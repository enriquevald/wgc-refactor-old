//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Promotion.cs
// 
//   DESCRIPTION: Class to manage Promotions
// 
//        AUTHOR: Agust� Poch
// 
// CREATION DATE: 15-SEP-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-SEP-2010 APB    First release.
// 24-NOV-2011 MPO    Fixed bug in IsFrequencyAllowed  --> The movements type 1 and 44  (before only 1) are count with the frequency promotion
// 18-JAN-2012 ACC    Calculate spent per provider
// 18-JAN-2012 MPO    Function to calculate spent per provider (AccountRemainingSpentPerProvider,AccountRemainingSpentPerSession,AccountUpdateSpentUsed)
// 23-MAY-2012 RCI    Fixed Bug #303: Read properly NR2 field.
// 31-MAY-2012 RCI    In a CashIn operation with "Bola Extra" (client is in session), don't allow to assign a Promotion. No matter the type of the terminal.
// 04-JUN-2012 SSC    Added CommonPromotionToRedeem method and moved common part of DB_PromotionRedeem in WSI.Cashier to UpdateAccountsPromotionToReedeemMoved method in WSI.Common
// 05-JUN-2012 DDM    Modified enumeration PROMOTION_TYPE
// 11-JUN-2012 JCM    Added GetPromoDate, auxiliary function that returns the earliest date that applies a promotion
// 18-JUN-2012 JAB    Added ReadPromotionDataByType function.
// 20-JUN-2012 SSC    Added routine UpdateSpentUsedInPromotion.
// 22-JUN-2012 JCM    Moved IsTimeInRange to Common.Schedule
// 06-JUL-2012 JAB    Not redeemable credit control.
// 30-JUL-2012 DDM    Modification ReadPromotionData for read credit_type of the Promotions table
// 02-AUG-2012 RRB    In routine IsFrequencyAllowed: Change query, now use ACCOUNT_OPERATIONS
// 13-AUG-2012 XIT    Added GetPromotionLists by date/s.
// 17-AUG-2012 DDM    Added field "must_expire" in TYPE_PROMOTION_DATA 
// 22-AUG-2012 MPO    Preassigned promotions: * CheckPromotionApplicable --> Retrieve preassigned award
//                                            * GetApplicablePromotionList --> Consider new types of promotion
// 02-OCT-2012 ANG    Add flag support
// 18-FEB-2013 ICS & RCI    Since 10-AUG-2012, it is possible to cash in even if the promotion has WonLock.
// 12-APR-2013 DHA    Added Expiration Limit
// 24-MAY-2013 HBB    The function IsLevelAllowed was moved to Misc.cs to make it accesible from other sites
// 01-OCT-2013 JBP    Fixed Bug WIG-230: Not show correct promotion limit. 
// 04-OCT-2013 JMM    Added GetWKTApplicableAvailablePromos to allow PromoBOX get applicable promotions
// 08-OCT-2013 FBA    Added new birthday_filter values and its control.
// 15-OCT-2013 RRR    Fixed Bug WIG-281: Cajero - No aparece promoci�n por m�nimo agotado
// 14-DEC-2013 JPJ    In TITO mode it shouldn't take into account the virtual accounts.
// 18-DEC-2013 JPJ    Virtual accounts don't take into account the daily and month limits.
// 23-DEC-2013 JBC    Fixed Bug WIG-929: Error on promotions without redeemable minimum
// 14-APR-2014 DLL    Added new functionality: VIP Account
// 08-MAY-2013 DLL    Fixed Bug WIG-912: Incorrect quantity in promotions. 
// 09-DEC-2014 JMM    Added by recharges promotions to the promotion list sent to the WKT
// 07-JAN-2015 JMM    Fixed Bug WIG-1911: A brand new account can't get a by recharges promotion on PromoBox
// 17-JUL-2015 YNM    Fixed Bug WIG-2330: Promobox collection error
// 11-AUG-2015 ETP    Fixed Bug 3633: Modified query GetLastUnpromotionedRecharges for avoiding refunds, added index and filters by date.
// 12-AUG-2015 ETP    Fixed Bug 3693: Need to save _total_recharges_amount between iterations, there are promotions that modify this value.
// 15-SEP-2015 JRC    Backlog Item #3697: Added new account creation date filter and its controls     
// 01-OCT-2015 AVZ    Backlog Item #4753: Edit filter by created account date - New requirements
// 07-OCT-2015 AVZ    Bug #4830: Birth date filter applies by working date
// 22-OCT-2015 SDS    Add promotion by played - Backlog Item 3856
// 13-JAN-2015 JML    Product Backlog Item 6559: Cashier draw (RE in kind)
// 18-JAN-2016 DLL    Fixed Bug 8501: Recharge with promotion error.
// 27-JAN-2016 JMV    Product Backlog Item 8257:Multiple buckets: Canje de buckets NR
// 29-JAN-2016 FOS    Product Backlog Item 8470:Floor Dual Currency: Promotions And Raffle
// 02-FOS-2016 FOS    Bug 8924:Floor Dual Currency: Promotions grouped by provider and iso_code
// 03-FOS-2016 FOS    Bug 8924:Floor Dual Currency: Promotions grouped by provider and iso_code
// 19-FEB-2016 ETP    Product Backlog Item 9748: Multiple buckets: Canje de buckets RE
// 18-FEB-2016 AVZ    Product Backlog Item 9363:Promo RE impuestos: Kernel: Promociones Redimibles: Aplicar impuestos
// 30-MAR-2016 AVZ    Product Backlog Item 10642:Pago de Premio: Pago de premios en efectivo
// 18-MAY-2017 FAV    Product Backlog Item 27515:[2021] Gaming table tournament - Tournament payout to winner (single player) � movements
// 03-AGO-2017 RLO    Add PromoGameId 
// 13-JUN-2018 EAV    Change Promotions Frecuency query
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;

namespace WSI.Common
{
  public class Promotion
  {
    public enum PROMOTION_STATUS
    {
      PROMOTION_OK = 0,
      PROMOTION_OK_MODIFIED_AMOUNTS = 1,
      PROMOTION_REDUCED_REWARD = 2,
      PROMOTION_GENERIC_ERROR = -1,
      PROMOTION_BELOW_MINIMUM = -2,
      PROMOTION_NOT_FOUND = -10,
      PROMOTION_DISABLED = -11,
      PROMOTION_OUT_OF_DATE = -12,
      PROMOTION_OUT_OF_WEEKDAY = -13,
      PROMOTION_OUT_OF_TIME = -14,
      PROMOTION_ACCT_NOT_FOUND = -20,
      PROMOTION_ACCT_ANONYMOUS = -21,
      PROMOTION_ACCT_BLOCKED = -22,
      PROMOTION_ACCT_NO_REDEEMABLE = -23,
      PROMOTION_ACCT_HAS_PRIZE = -24,
      PROMOTION_GENDER_FILTER = -30,
      PROMOTION_BIRTHDAY_FILTER = -31,
      PROMOTION_LEVEL_FILTER = -32,
      PROMOTION_LIMIT_EXHAUSTED = -33,
      PROMOTION_FREQUENCY_FILTER = -34,
      PROMOTION_BELOW_MINIMUM_SPENT = -35,
      PROMOTION_BELOW_SPENT = -36,
      PROMOTION_NOT_FOUND_PREASSIGNED = -37,
      PROMOTION_CREATED_ACCOUNT = -38,
    }

    public enum ACCOUNT_TYPE
    {
      ACCOUNT_TYPE_ANONYMOUS = 0,
      ACCOUNT_TYPE_PERSONAL = 1
    }

    public enum ACCOUNT_HOLDER_GENDER
    {
      ACCOUNT_HOLDER_GENDER_NOT_SET = 0,
      ACCOUNT_HOLDER_GENDER_MALE = 1,
      ACCOUNT_HOLDER_GENDER_FEMALE = 2
    }

    public enum PROMOTION_GENDER_FILTER
    {
      PROMOTION_GENDER_FILTER_NOT_SET = 0,
      PROMOTION_GENDER_FILTER_MEN = 1,
      PROMOTION_GENDER_FILTER_WOMEN = 2
    }

    public enum PROMOTION_BIRTHDAY_FILTER
    {
      PROMOTION_BIRTHDAY_FILTER_NOT_SET = 0,
      PROMOTION_BIRTHDAY_FILTER_DAY_ONLY = 1,
      PROMOTION_BIRTHDAY_FILTER_WHOLE_MONTH = 2,
      PROMOTION_BIRTHDAY_FILTER_AGE_INCLUDED = 3,
      PROMOTION_BIRTHDAY_FILTER_AGE_EXCLUDED = 4
    }

    // AVZ 01-OCT-2015, account creation date filter
    public enum PROMOTION_CREATED_ACCOUNT_FILTER
    {
      PROMOTION_CREATED_ACCOUNT_FILTER_NOT_SET = 0,
      PROMOTION_CREATED_ACCOUNT_FILTER_WORKING_DAY_ONLY = 1,
      PROMOTION_CREATED_ACCOUNT_FILTER_WORKING_DAYS_INCLUDED = 2,
      PROMOTION_CREATED_ACCOUNT_FILTER_ANNIVERSARY_WHOLE_MONTH = 3,
      PROMOTION_CREATED_ACCOUNT_FILTER_ANNIVERSARY_DAY_ONLY = 4
    }

    public enum PROMOTION_SPECIAL_PERMISSION
    {
      PROMOTION_SPECIAL_PERMISSION_NOT_SET = 0,
      PROMOTION_SPECIAL_PERMISSION_A = 1,
      PROMOTION_SPECIAL_PERMISSION_B = 2
    }

    public enum PROMOTION_LIMIT_TYPE
    {
      PROMOTION_DAILY_LIMIT = 0,
      PROMOTION_MONTHLY_LIMIT = 1,
      PROMOTION_GLOBAL_DAILY_LIMIT = 2,
      PROMOTION_GLOBAL_MONTHLY_LIMIT = 3,
      PROMOTION_GLOBAL_LIMIT = 4,
    }

    public enum PROMOTION_TYPE
    {
      UNKNOWN = -1,
      MANUAL = 0,

      PER_POINTS_NOT_REDEEMABLE = 1, // NR

      AUTOMATIC_PER_RECHARGE_COVER_COUPON = 2, // NR2
      AUTOMATIC_PER_PRIZE = 3, // NR1

      PERIODIC_PER_LEVEL = 4,
      PERIODIC_PER_LEVEL_AND_MIN_PLAYED = 5,
      PERIODIC_PER_BIRTHDAY = 6,

      AUTOMATIC_PER_RECHARGE_PRIZE_COUPON = 7, // PRIZE_COUPON REDEEMABLE

      PER_POINTS_REDEEMABLE = 8,

      PREASSIGNED = 9,

      EXTERNAL_NON_REDEEM = 10,    // RCI 19-DEC-2012: For the WSP Interface / Space Vouchers and can be used for other External promotions

      AUTOMATIC_PER_RECHARGE_WITH_CURRENCY_EXCHANGE = 11, // DLL 28-JUN-2013: For Currency Exchange promotion

      EXTERNAL_REDEEM = 12,    // ACC 14-AUG-2013: For Space Vouchers and can be used for other External promotions

      // DDM & AJQ, 05-SEP-2013 CashDeskDraw
      AUTOMATIC_CASH_DESK_DRAW_LOSER_NR = 13,
      AUTOMATIC_CASH_DESK_DRAW_LOSER_RE = 14,
      AUTOMATIC_CASH_DESK_DRAW_WINNER_NR = 15,
      AUTOMATIC_CASH_DESK_DRAW_WINNER_RE = 16,
      AUTOMATIC_CASH_DESK_DRAW_PLAYER_SAID_NO = 17, // Like Prize Coupon
      AUTOMATIC_CASH_DESK_DRAW_SERVER_DOWN = 18,    // Like Prize Coupon
      AUTOMATIC_CASH_DESK_DRAW_WINNER_NR2_IN_KIND = 19,     // Like NR2
      AUTOMATIC_CASH_DESK_DRAW_LOSER_NR2_IN_KIND = 20,     // Like NR2
      AUTOMATIC_CASH_DESK_DRAW_PLAY_SESSION_WINNER_NR_HIDDEN = 21,  // All NR credits must be associated to a promotion.
      // JML 11-SEP-2015
      AUTOMATIC_CASH_DESK_DRAW_WINNER_RE_IN_KIND = 22,    // Like UNR, but redeemable.
      // JMV 27-01-2016
      AUTOMATIC_BUCKET_NR_CREDIT = 23,
      AUTOMATIC_BUCKET_RE_CREDIT = 24,
      // 30-MAR-2016 AVZ
      AUTOMATIC_PRIZE_PAYOUT = 25,
      AUTOMATIC_PRIZE_PAYOUT_AND_RECHARGE = 26,

      // AJQ 14-FEB-2014, Promotion to allow SPACE restrict the machines where the NR credits can be played.
      EXTERNAL_NON_REDEEM_RESTRICTED_01 = 101,
      EXTERNAL_NON_REDEEM_RESTRICTED_02 = 102,
      EXTERNAL_NON_REDEEM_RESTRICTED_03 = 103,
      EXTERNAL_NON_REDEEM_RESTRICTED_04 = 104,
      EXTERNAL_NON_REDEEM_RESTRICTED_05 = 105,
      EXTERNAL_NON_REDEEM_RESTRICTED_06 = 106,
      EXTERNAL_NON_REDEEM_RESTRICTED_07 = 107,
      EXTERNAL_NON_REDEEM_RESTRICTED_08 = 108,
      EXTERNAL_NON_REDEEM_RESTRICTED_09 = 109,

    }

    public enum EXPIRATION_TYPE
    {
      EXPIRATION_TYPE_NOT_SET = 0,
      EXPIRATION_TYPE_DAYS = 1,
      EXPIRATION_TYPE_HOURS = 2
    }


    public struct PROMOTION_FREQUENCY_FILTER
    {
      public Int32 last_days;
      public Int32 min_days;
      public Currency min_cash_in;
    }

    public class TYPE_PROMOTION_DATA
    {

      public Int64 id;
      public String name;
      public Boolean enabled;
      public PROMOTION_SPECIAL_PERMISSION special_permission;
      public PROMOTION_TYPE type;

      public DateTime date_start;
      public DateTime date_finish;

      public Int32 schedule_weekdays;
      public Int32 schedule1_time_from;
      public Int32 schedule1_time_to;
      public Boolean schedule2_enabled;
      public Int32 schedule2_time_from;
      public Int32 schedule2_time_to;

      public PROMOTION_GENDER_FILTER gender_filter;
      public PROMOTION_BIRTHDAY_FILTER birthday_filter;
      public PROMOTION_BIRTHDAY_FILTER age_filter;
      public Int32 age_from;
      public Int32 age_to;
      public Int32 level_filter;
      public PROMOTION_FREQUENCY_FILTER frequency_filter;

      public Currency min_cash_in;
      public Currency min_cash_in_reward;

      public Boolean won_lock_enabled;
      public Currency won_lock;

      public Currency cash_in;
      public Currency cash_in_reward;

      public Int32 num_tokens;
      public String token_name;
      public Currency token_reward;

      public Currency daily_limit;
      public Currency monthly_limit;
      public Currency global_daily_limit;
      public Currency global_monthly_limit;
      public Currency global_limit;

      // AJQ 16-NOV-2010, Promotions on "Creditos Agotados"
      public Currency min_spent;
      public Currency min_spent_reward;
      public Currency spent;
      public Currency spent_reward;

      public Currency min_played;
      public Currency min_played_reward;
      public Currency played;
      public Currency played_reward;

      public DateTime last_executed;
      public DateTime next_execution;

      public ProviderList provider_list;
      public TerminalList terminal_list;

      public DrawOfferList draw_offer_list;

      public EXPIRATION_TYPE expiration_type;
      public Int32 expiration_value;

      public DateTime actual_promo_date;

      public ACCOUNT_PROMO_CREDIT_TYPE credit_type = ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN;

      public DateTime must_expire;

      public DataTable flags_awarded;
      public DataTable flags_required;

      // DHA 09-APR-2013: 'Expiration Limit' field added
      public DateTime expiration_limit;

      public Int64 points_to_credits_id;

      //DLL 11-APR-2014: is vip promotion
      public Boolean is_vip;

      // AVZ 01-OCT-2015, account creation date filter
      public PROMOTION_CREATED_ACCOUNT_FILTER created_account_filter;
      public Int32 working_days_included;
      public Int32 anniversary_year_from;
      public Int32 anniversary_year_to;

      // 18-FEB-2016 AVZ
      public Boolean apply_tax;

      // 03-AGO-2017
      public Int64 PromoGameId;

      public String PyramidalDist;
    };

    public class TYPE_ACCOUNT_DATA
    {
      public Int64 id;
      public ACCOUNT_TYPE user_type;
      public Boolean blocked;

      public ACCOUNT_HOLDER_GENDER holder_gender;
      public DateTime holder_birth_date;
      public Int32 holder_level;

      public int terminal_id;
      public Boolean is_logged_in;
      public Boolean is_virtual;

      public MultiPromos.AccountBalance balance = MultiPromos.AccountBalance.Zero;
      public Boolean is_vip;
      // JRC 15-SEP-2015, account creation date filter
      public DateTime created;
    };

    public class TYPE_PROMO_CREDIT_USED
    {
      public Decimal account_daily_credit_used;
      public Decimal account_monthly_credit_used;
      public Decimal global_daily_credit_used;
      public Decimal global_monthly_credit_used;
      public Decimal global_credit_used;
    };

    #region Constants

    public const Int16 COLUMN_PROVIDER_ID = 0;
    public const Int16 COLUMN_TERMINAL_ID = 0;
    public const Int16 COLUMN_SPENT_REMAINING = 1;
    public const Int16 COLUMN_PLAYED_AMOUNT = 2;
    public const Int16 COLUMN_PLAY_SESSION_ID = 3;
    public const Int16 COLUMN_DELTA_SPENT_USED = 4;

    #endregion


    //------------------------------------------------------------------------------
    // PURPOSE: Check if a promotion can be applied to an account (PROMOBOX VERSION)
    // 
    //  PARAMS:
    //      - INPUT:
    //        - SQLTransaction
    //        - PromotionId
    //        - AccountId
    //        - CheckAmounts: 
    //          - false: do not perform calculations on amounts, no value is return as Reward
    //          - true: Calculations will take into account provided CashIn and NumTokens to determine the reward amount
    //        - CashIn: amount player is going to pay
    //        - Spent: amount spent for player during the day to consider for the promotion
    //        - NumTokens: number of tokens player is about to deliver
    //		    - DateFrom : Date interval start where promotion must be applied
    //		    - DateTo : Date interval end where promotion must be applied
    //
    //      - OUTPUT:
    //        - CashIn: maximum amount that can be used in order not to exceed promotion limits
    //        - NumTokens: maximum number of tokens that can be used in order not to exceed promotion limits
    //        - Reward: amount of non-redeemable credits awarded using promotion, account and cash in values
    //        - SpentUsed: spent used for the promotion
    //
    // RETURNS:
    //    Successful checks
    //    - PROMOTION_OK: promotion can be applied with current amounts and data
    ////    - PROMOTION_OK_MODIFIED_AMOUNTS : promotion can be applied using returned amounts
    //    - PROMOTION_REDUCED_REWARD: calculated reward exceeds daily/monthly limit set for the promotion; reward is reduced
    //
    //    Failed checks
    //    - PROMOTION_GENERIC_ERROR: general error during check
    //    - PROMOTION_BELOW_MINIMUM: specified cash in/num tokens amounts do not reach minimum amounts set for the promotion
    //    - PROMOTION_NOT_FOUND: promotion not found
    //    - PROMOTION_DISABLED: promotion has been disabled
    //    - PROMOTION_OUT_OF_DATE: promotion is no longer or still not valid
    //    - PROMOTION_OUT_OF_WEEKDAY: current weekday is not allowed for the promotion
    //    - PROMOTION_OUT_OF_TIME: current time is out of the time interval set for the promotion
    //    - PROMOTION_ACCT_NOT_FOUND: account not found
    //    - PROMOTION_ACCT_ANONYMOUS: not a personal account, cannot have any promotio applied
    //    - PROMOTION_ACCT_BLOCKED: account is blocked
    //    - PROMOTION_ACCT_NO_REDEEMABLE: account has already some non-redeemable balance
    //    - PROMOTION_GENDER_FILTER: account holder gender is excluded from promotion conditions
    //    - PROMOTION_BIRTHDAY_FILTER: promotion cannot be applied based on account date of birth and current date
    //    - PROMOTION_LIMIT_EXHAUSTED: user has already exhausted the limit set for the promotion
    // 
    //   NOTES:
    public static PROMOTION_STATUS CheckPromotionApplicable(SqlTransaction SQLTransaction,
                                                             Int64 PromotionId,
                                                             Int64 AccountId,
                                                             Boolean CheckAmounts,
                                                             DataTable AccountFlags,
                                                             ref Currency CashIn,
                                                             Currency Spent,
                                                             Currency Played,
                                                             ref Int32 NumTokens,
                                                             ref TYPE_PROMOTION_DATA PromoData,
                                                             out Currency Reward,
                                                             out Currency WonLock,
                                                             out Currency SpentUsed,
                                                             DateTime Date)
    {
      // Call to new function's version that support the date interval
      Currency _remaining;

      return CheckPromotionApplicable(SQLTransaction,
                                       PromotionId,
                                       AccountId,
                                       CheckAmounts,
                                       AccountFlags,
                                       ref CashIn,
                                       Spent,
                                       Played,
                                       ref NumTokens,
                                       ref PromoData,
                                       out Reward,
                                       out WonLock,
                                       out SpentUsed,
                                       out _remaining,
                                       Date,
                                       Date);
    } // CheckPromotionApplicable

    public static PROMOTION_STATUS CheckPromotionApplicable(SqlTransaction SQLTransaction,
                                                             Int64 PromotionId,
                                                             Int64 AccountId,
                                                             Boolean CheckAmounts,
                                                             DataTable AccountFlags,
                                                             ref Currency CashIn,
                                                             Currency Spent,
                                                             Currency Played,
                                                             ref Int32 NumTokens,
                                                             ref TYPE_PROMOTION_DATA PromoData,
                                                             out Currency Reward,
                                                             out Currency WonLock,
                                                             out Currency SpentUsed,
                                                             DateTime DateFrom,
                                                             DateTime DateTo)
    {
      Currency _remaining;

      return CheckPromotionApplicable(SQLTransaction,
                                       PromotionId,
                                       AccountId,
                                       CheckAmounts,
                                       AccountFlags,
                                       ref CashIn,
                                       Spent,
                                       Played,
                                       ref NumTokens,
                                       ref PromoData,
                                       out Reward,
                                       out WonLock,
                                       out SpentUsed,
                                       out _remaining,
                                       DateFrom,
                                       DateTo);
    } // CheckPromotionApplicable

    public static PROMOTION_STATUS CheckPromotionApplicable(SqlTransaction SQLTransaction,
                                                             Int64 PromotionId,
                                                             Int64 AccountId,
                                                             Boolean CheckAmounts,
                                                             DataTable AccountFlags,
                                                             ref Currency CashIn,
                                                             Currency Spent,
                                                             Currency Played,
                                                             ref Int32 NumTokens,
                                                             ref TYPE_PROMOTION_DATA PromoData,
                                                             out Currency Reward,
                                                             out Currency WonLock,
                                                             out Currency SpentUsed,
                                                             out Currency Remaining,
                                                             DateTime DateFrom,
                                                             DateTime DateTo)
    {
      Int32 _quantity_for_min_cash_in;

      _quantity_for_min_cash_in = 1;
      return CheckPromotionApplicable(SQLTransaction,
                                     PromotionId,
                                     AccountId,
                                     CheckAmounts,
                                     AccountFlags,
                                     ref CashIn,
                                     Spent,
                                     Played,
                                     ref NumTokens,
                                     ref PromoData,
                                     out Reward,
                                     out WonLock,
                                     out SpentUsed,
                                     out Remaining,
                                     DateFrom,
                                     DateTo,
                                     ref _quantity_for_min_cash_in);
    }

    public static PROMOTION_STATUS CheckPromotionApplicable(SqlTransaction SQLTransaction,
                                                             Int64 PromotionId,
                                                             Int64 AccountId,
                                                             Boolean CheckAmounts,
                                                             DataTable AccountFlags,
                                                             ref Currency CashIn,
                                                             Currency Spent,
                                                             Currency Played,
                                                             ref Int32 NumTokens,
                                                             ref TYPE_PROMOTION_DATA PromoData,
                                                             out Currency Reward,
                                                             out Currency WonLock,
                                                             out Currency SpentUsed,
                                                             out Currency Remaining,
                                                             DateTime DateFrom,
                                                             DateTime DateTo,
                                                             ref Int32 QuantityForMinCashIn)
    {

      return CheckPromotionApplicable(SQLTransaction,
                                     PromotionId,
                                     AccountId,
                                     CheckAmounts,
                                     AccountFlags,
                                     ref CashIn,
                                     Spent,
                                     Played,
                                     ref NumTokens,
                                     ref PromoData,
                                     out Reward,
                                     out WonLock,
                                     out SpentUsed,
                                     out Remaining,
                                     DateFrom,
                                     DateTo,
                                     ref QuantityForMinCashIn,
                                     null);
    }

    //DHA 06-DEC-2013: Added QuantityForMinCashIn, this parameter is only to generate X promotions in TITO,
    //                 The operation is: min_cash_in_reward * QuantityForMinCashIn, only for promotion without recharge
    // SDS 20-10-2015 Added Played, only for promotion by played
    public static PROMOTION_STATUS CheckPromotionApplicable(SqlTransaction SQLTransaction,
                                                             Int64 PromotionId,
                                                             Int64 AccountId,
                                                             Boolean CheckAmounts,
                                                             DataTable AccountFlags,
                                                             ref Currency CashIn,
                                                             Currency Spent,
                                                             Currency Played,
                                                             ref Int32 NumTokens,
                                                             ref TYPE_PROMOTION_DATA PromoData,
                                                             out Currency Reward,
                                                             out Currency WonLock,
                                                             out Currency SpentUsed,
                                                             out Currency Remaining,
                                                             DateTime DateFrom,
                                                             DateTime DateTo,
                                                             ref Int32 QuantityForMinCashIn,
                                                             TYPE_ACCOUNT_DATA AccountData)
    {
      Boolean _rc;
      TYPE_PROMO_CREDIT_USED _promo_used_credit;
      Currency _remaining_daily_limit;
      Currency _remaining_monthly_limit;
      Currency _remaining_global_daily_limit;
      Currency _remaining_global_monthly_limit;
      Currency _remaining_global_limit;
      Int32 _closing_time;
      DayOfWeek _day_of_week;
      DateTime _aux_date_time;
      Int32 _num_days;
      Boolean _time_in_range;

      Reward = 0;
      WonLock = 0;
      SpentUsed = 0;
      Remaining = 0;
      _closing_time = 0;

      try
      {
        // When a promotion can be applied?
        //    - Enabled
        //    - Inside its datetime range
        //    - Matches any of its enabled weekdays
        //    - Match any of its defined time slots
        //    - Related account is ok

        //    - Enabled
        if (!PromoData.enabled)
        {
          // Disabled
          return PROMOTION_STATUS.PROMOTION_DISABLED;
        }

        //    - Inside its datetime range
        if (CheckDatePeriods(DateFrom, DateTo, PromoData.date_start, PromoData.date_finish) == 0)
        {
          // Promotion does not intersect with the given date-time period DateFrom-DateTo
          return PROMOTION_STATUS.PROMOTION_OUT_OF_DATE;
        }

        //    - Match any of its enabled weekdays
        Int32.TryParse(WSI.Common.Misc.ReadGeneralParams("WigosGUI", "ClosingTime"), out _closing_time);
        _num_days = 0;
        for (_aux_date_time = DateFrom; _aux_date_time.CompareTo(DateTo) <= 0; _aux_date_time = _aux_date_time.AddDays(1))
        {
          _day_of_week = GetWeekDay(_aux_date_time, _closing_time);

          if (ScheduleDay.WeekDayIsInSchedule(_day_of_week, PromoData.schedule_weekdays))
          {
            _num_days++;

            break;
          }
        }

        if (_num_days == 0)
        {
          // None of the days in [DateFrom, DateTo] match any of promo's weekdays
          return PROMOTION_STATUS.PROMOTION_OUT_OF_WEEKDAY;
        }

        //    - Match any of its defined time slots
        if (DateFrom == DateTo)
        {
          // No date interval, just checkout received datetime
          _aux_date_time = DateFrom;

          // Check time slot #1
          _time_in_range = ScheduleDay.IsTimeInRange(_aux_date_time.TimeOfDay, PromoData.schedule1_time_from, PromoData.schedule1_time_to);

          // Check time slot #2
          if (!_time_in_range && PromoData.schedule2_enabled)
          {
            _time_in_range = ScheduleDay.IsTimeInRange(_aux_date_time.TimeOfDay, PromoData.schedule2_time_from, PromoData.schedule2_time_to);
          }
        }
        else
        {
          TimeSpan _timespan_schedule_from;
          TimeSpan _timespan_schedule_to;

          // Seconds to TimeSpan
          _timespan_schedule_from = new TimeSpan(0, 0, PromoData.schedule1_time_from);
          _timespan_schedule_to = new TimeSpan(0, 0, PromoData.schedule1_time_to);

          // Check time slot #1
          _time_in_range = ScheduleDay.IsTimeInRange(_timespan_schedule_from, (int)DateFrom.TimeOfDay.TotalSeconds, (int)DateTo.TimeOfDay.TotalSeconds)
                        || ScheduleDay.IsTimeInRange(_timespan_schedule_to, (int)DateFrom.TimeOfDay.TotalSeconds, (int)DateTo.TimeOfDay.TotalSeconds);

          // Check time slot #2
          if (!_time_in_range && PromoData.schedule2_enabled)
          {
            // Seconds to TimeSpan
            _timespan_schedule_from = new TimeSpan(0, 0, PromoData.schedule2_time_from);
            _timespan_schedule_to = new TimeSpan(0, 0, PromoData.schedule2_time_to);

            _time_in_range = ScheduleDay.IsTimeInRange(_timespan_schedule_from, (int)DateFrom.TimeOfDay.TotalSeconds, (int)DateTo.TimeOfDay.TotalSeconds)
                          || ScheduleDay.IsTimeInRange(_timespan_schedule_to, (int)DateFrom.TimeOfDay.TotalSeconds, (int)DateTo.TimeOfDay.TotalSeconds);
          }
        }

        if (!_time_in_range)
        {
          return PROMOTION_STATUS.PROMOTION_OUT_OF_TIME;
        }

        _rc = true;
        if (AccountData == null)
        {
          _rc = ReadAccountData(SQLTransaction, AccountId, out AccountData);
        }

        if (!_rc)
        {
          Log.Error("CheckPromotionApplicable. Account not found: " + AccountId + ".");

          return PROMOTION_STATUS.PROMOTION_ACCT_NOT_FOUND;
        }

        //    - Related account is ok
        //        - Not Blocked
        //        - Avoid assign points to "anonymous" accounts

        //        - Not Blocked
        if (AccountData.blocked)
        {
          return PROMOTION_STATUS.PROMOTION_ACCT_BLOCKED;
        }

        // AJQ 22-AUG-2012
        //        - Avoid assign points to "anonymous" accounts
        if (PromoData.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.POINT)
        {
          if (AccountData.holder_level == 0)
          {
            return PROMOTION_STATUS.PROMOTION_ACCT_BLOCKED;
          }
        }

        //
        // If is preassigned, will check only the scheduler
        //
        if (PromoData.type == PROMOTION_TYPE.PREASSIGNED)
        {
          if (RetrievePreassignedReward(PromoData.id, AccountId, out Reward, SQLTransaction))
          {
            return PROMOTION_STATUS.PROMOTION_OK;
          }
          else
          {
            return PROMOTION_STATUS.PROMOTION_NOT_FOUND_PREASSIGNED;
          }

        }

        // RCI & AJQ 11-JUL-2012: Allow to assign Promotion even the client is playing.
        //                        Now that we have the 3 balances (RE, PRE, PNR) is not a problem.
        //if (_account_data.is_logged_in)
        //{
        //  return PROMOTION_STATUS.PROMOTION_DISABLED;
        //}

        //  - Non redeemable balance
        //  - AJQ 06-OCT-2010, An account is considered to have non-redeemable balance also when 
        //    it has an active promotion and it is in use

        // TODO: Maximum Manual Promotions 
        // - Read GP More Than Manual Promo allowed?
        // - Num Active Manual Promos
        // - If NumActive > MaxManual --> return PROMOTION_STATUS.PROMOTION_ACCT_NO_REDEEMABLE;

        // RCI 17-NOV-2010: Filter promos that have a minimum spent defined and user has spent less than that minimum.
        if (PromoData.min_spent > 0 && PromoData.min_spent > Spent)
        {
          return PROMOTION_STATUS.PROMOTION_BELOW_MINIMUM_SPENT;
        }

        // RCI 30-NOV-2010: Filter promos that have rewards for spent, but not rewards for cash-in or tokens,
        //                  and user has spent less than the necessary (RRR 15-OCT-2013: And min_spent_reward not defined)
        if (PromoData.spent > 0
          && PromoData.spent > Spent
          && PromoData.min_spent_reward == 0
          && PromoData.min_cash_in_reward == 0
          && PromoData.cash_in_reward == 0
          && PromoData.token_reward == 0)
        {
          return PROMOTION_STATUS.PROMOTION_BELOW_SPENT;
        }

        // Checks to be performed on promotion and account data
        //  - Level filter
        //  - Gender filter
        //  - Date of birth filter
        //  - Frequency filter

        //  - Level filter
        if (!Misc.IsLevelAllowed(AccountData.holder_level, PromoData.level_filter))
        {
          return PROMOTION_STATUS.PROMOTION_LEVEL_FILTER;
        }

        // JPJ 14-DEC-2013 There is no need to take into account non anonymous level promotions if it's a virtual account.
        if (WSI.Common.TITO.Utils.IsTitoMode())
        {
          if (AccountData.holder_level == 0 && !IsLevelSelected(4, PromoData.level_filter))
          {
            return PROMOTION_STATUS.PROMOTION_LEVEL_FILTER;
          }
        }
        //  - Gender filter
        if (PromoData.gender_filter != PROMOTION_GENDER_FILTER.PROMOTION_GENDER_FILTER_NOT_SET
          && !IsGenderAllowed(AccountData.holder_gender, PromoData.gender_filter))
        {
          return PROMOTION_STATUS.PROMOTION_GENDER_FILTER;
        }

        //  - Date of birth filter
        if (PromoData.birthday_filter != PROMOTION_BIRTHDAY_FILTER.PROMOTION_BIRTHDAY_FILTER_NOT_SET || PromoData.age_filter != PROMOTION_BIRTHDAY_FILTER.PROMOTION_BIRTHDAY_FILTER_NOT_SET)
        {
          // Does Promo consider Birthday in any way?
          _num_days = 0;

          for (_aux_date_time = DateFrom; _aux_date_time.CompareTo(DateTo) <= 0; _aux_date_time = _aux_date_time.AddDays(1))
          {
            if (IsDateBirthdayCompliant(_aux_date_time, AccountData.holder_birth_date, PromoData.birthday_filter, PromoData.age_filter, PromoData.age_from, PromoData.age_to))
            {
              // Player's birthday is in [DateFrom, DateTo]
              _num_days++;

              break;
            }
          }

          if (_num_days == 0)
          {
            // None of the days in [DateFrom, DateTo] match the Birthday
            return PROMOTION_STATUS.PROMOTION_BIRTHDAY_FILTER;
          }
        }

        // JRC 15-SEP-2015, account creation date filter
        // AVZ 01-OCT-2015
        if (PromoData.created_account_filter != PROMOTION_CREATED_ACCOUNT_FILTER.PROMOTION_CREATED_ACCOUNT_FILTER_NOT_SET)
        {
          _num_days = 0;
          for (_aux_date_time = DateFrom; _aux_date_time.CompareTo(DateTo) <= 0; _aux_date_time = _aux_date_time.AddDays(1))
          {
            if (IsCreatedAccountCompliant(_aux_date_time, AccountData.created, PromoData.created_account_filter, PromoData.working_days_included, PromoData.anniversary_year_from, PromoData.anniversary_year_to))
            {
              // Player's created account date is in [DateFrom, DateTo]
              _num_days++;

              break;
            }
          }

          if (_num_days == 0)
          {
            return PROMOTION_STATUS.PROMOTION_CREATED_ACCOUNT;
          }
        }

        // - Flags Required
        if (!Promotion.CheckAccountFlagsPromoFilter(AccountId, PromoData.flags_required, AccountFlags, SQLTransaction))
        {
          // We use that status to say "NOT APPLICABLE"
          return PROMOTION_STATUS.PROMOTION_LEVEL_FILTER;
        }

        //  - Frequency filter
        if (!IsFrequencyAllowed(AccountData.id, PromoData.frequency_filter, SQLTransaction))
        {
          return PROMOTION_STATUS.PROMOTION_FREQUENCY_FILTER;
        }

        // Check period limits day by day
        //  - Day limit
        //  - Month limit
        //  - Global Day limit
        //  - Global Month limit
        //  - Global limit

        _num_days = 0;

        _remaining_daily_limit = 0;
        _remaining_monthly_limit = 0;
        _remaining_global_daily_limit = 0;
        _remaining_global_monthly_limit = 0;
        _remaining_global_limit = 0;

        for (_aux_date_time = DateFrom; _aux_date_time.CompareTo(DateTo) <= 0; _aux_date_time = _aux_date_time.AddDays(1))
        {
          if (!GetUsedCreditPromotion(PromotionId, AccountId, AccountData.is_virtual, out _promo_used_credit, SQLTransaction))
          {
            return PROMOTION_STATUS.PROMOTION_GENERIC_ERROR;
          }

          _remaining_global_daily_limit = Math.Max(0, PromoData.global_daily_limit - _promo_used_credit.global_daily_credit_used);
          _remaining_global_monthly_limit = Math.Max(0, PromoData.global_monthly_limit - _promo_used_credit.global_monthly_credit_used);
          _remaining_global_limit = Math.Max(0, PromoData.global_limit - _promo_used_credit.global_credit_used);

          //Calculate remaining available limit promotion
          Remaining = Decimal.MaxValue;

          //JPJ 18-DEC-2013 Virtual accounts don't take into account the daily and month limits.
          if (AccountData.is_virtual)
          {
            _remaining_daily_limit = Int64.MaxValue;
            _remaining_monthly_limit = Int64.MaxValue;
          }
          else
          {
            _remaining_daily_limit = Math.Max(0, PromoData.daily_limit - _promo_used_credit.account_daily_credit_used);
            _remaining_monthly_limit = Math.Max(0, PromoData.monthly_limit - _promo_used_credit.account_monthly_credit_used);

            if (PromoData.daily_limit > 0)
            {
              if (_remaining_daily_limit <= 0)
              {
                // Promotion DAILY limit overflow for the specified account
                _num_days++;

                break;
              }
              else
              {
                Remaining = Math.Min(Remaining, _remaining_daily_limit);
              }
            }

            if (PromoData.monthly_limit > 0)
            {
              if (_remaining_monthly_limit <= 0)
              {
                // Promotion DAILY limit overflow for the specified account
                _num_days++;

                break;
              }
              else
              {
                Remaining = Math.Min(Remaining, _remaining_monthly_limit);
              }
            }
          }
          if (PromoData.global_daily_limit > 0)
          {
            if (_remaining_global_daily_limit <= 0)
            {
              // Promotion GLOBAL DAILY limit overflow for the specified account
              _num_days++;

              break;
            }
            else
            {
              Remaining = Math.Min(Remaining, _remaining_global_daily_limit);
            }
          }

          if (PromoData.global_monthly_limit > 0)
          {
            if (_remaining_global_monthly_limit <= 0)
            {
              // Promotion GLOBAL MONTHLY limit overflow for the specified account
              _num_days++;

              break;
            }
            else
            {
              Remaining = Math.Min(Remaining, _remaining_global_monthly_limit);
            }
          }

          // JBP: Fixed bug WIG-230: Not show correct promotion limit. 
          // JBP: Fixed bug WIG-243: Not show correct promotion without limit.
          if (PromoData.global_limit > 0)
          {
            if (_remaining_global_limit <= 0)
            {
              // Promotion GLOBAL limit overflow for the specified account
              _num_days++;

              break;
            }
            else
            {
              Remaining = Math.Min(Remaining, _remaining_global_limit);
            }
          }
          else
          {
            Remaining = Math.Min(Remaining, _remaining_global_limit);
          }
        }

        if (_num_days > 0)
        {
          // 
          return PROMOTION_STATUS.PROMOTION_LIMIT_EXHAUSTED;
        }

        // Final stage: calculate reward if requested
        //
        // CheckAmounts parameter set to TRUE makes only sense for 1-day period (e.g. DateFrom == DateTo)
        // Otherwise '_remaining_xxx' variables (x5) that were calculated above must be recalculated before 
        // being checked out for each day in the interval. If don't they will hold the values calculated 
        // for DateTo.
        //
        // Today the only place where CheckPromotionApplicable is called with DateFrom != DateTo has 
        // set CheckAmounts=FALSE

        if (CheckAmounts)
        {
          Currency _cash_in;
          Int32 _num_tokens;
          Int32 _num_times;

          // Check reward according to specified amounts
          _cash_in = CashIn;
          _num_tokens = NumTokens;
          if (!CalculateReward(ref _cash_in, ref _num_tokens, ref PromoData, out Reward, out WonLock, QuantityForMinCashIn))
          {
            // Provided amounts do not reach minimum requirements or exceed promotion limits
            CashIn = _cash_in;
            NumTokens = _num_tokens;

            return PROMOTION_STATUS.PROMOTION_BELOW_MINIMUM;
          }

          if (Spent >= PromoData.min_spent)
          {
            Currency _max_reward;

            // Find out the lowest minimum
            _max_reward = Decimal.MaxValue;
            if (PromoData.daily_limit > 0)
            {
              _max_reward = Math.Min(_max_reward, _remaining_daily_limit);
            }

            if (PromoData.monthly_limit > 0)
            {
              _max_reward = Math.Min(_max_reward, _remaining_monthly_limit);
            }

            if (PromoData.global_daily_limit > 0)
            {
              _max_reward = Math.Min(_max_reward, _remaining_global_daily_limit);
            }

            if (PromoData.global_monthly_limit > 0)
            {
              _max_reward = Math.Min(_max_reward, _remaining_global_monthly_limit);
            }

            if (PromoData.global_limit > 0)
            {
              _max_reward = Math.Min(_max_reward, _remaining_global_limit);
            }

            Reward += PromoData.min_spent_reward;
            SpentUsed = PromoData.min_spent;
            if (PromoData.spent > 0)
            {
              _num_times = (Int32)(Spent / PromoData.spent);

              //
              // ACC 02-FEB-2012 Calculate reward and spent used needed without exceed promotion limits...
              //
              //////Reward += _num_times * PromoData.spent_reward;
              //////SpentUsed = Math.Max(SpentUsed, _num_times * PromoData.spent);

              Int32 _idx;
              Currency _spent_used;

              _spent_used = 0;
              for (_idx = 0; _idx < _num_times; _idx++)
              {
                if (Reward >= _max_reward)
                {
                  Reward = _max_reward;

                  break;
                }

                Reward += PromoData.spent_reward;
                _spent_used += PromoData.spent;
              }

              SpentUsed = Math.Max(SpentUsed, _spent_used);
            }
          }

          //SDS 20-10-2015
          if (Played  >= PromoData.min_played )
          {
            Currency _max_reward;

            // Find out the lowest minimum
            _max_reward = Decimal.MaxValue;
            if (PromoData.daily_limit > 0)
            {
              _max_reward = Math.Min(_max_reward, _remaining_daily_limit);
            }

            if (PromoData.monthly_limit > 0)
            {
              _max_reward = Math.Min(_max_reward, _remaining_monthly_limit);
            }

            if (PromoData.global_daily_limit > 0)
            {
              _max_reward = Math.Min(_max_reward, _remaining_global_daily_limit);
            }

            if (PromoData.global_monthly_limit > 0)
            {
              _max_reward = Math.Min(_max_reward, _remaining_global_monthly_limit);
            }

            if (PromoData.global_limit > 0)
            {
              _max_reward = Math.Min(_max_reward, _remaining_global_limit);
            }

            Reward += PromoData.min_played_reward ;
            if (PromoData.played > 0)
            {
              _num_times = (Int32)(Played / PromoData.played);
              Int32 _idx;
              Currency _played_used;

              _played_used = 0;
              for (_idx = 0; _idx < _num_times; _idx++)
              {
                if (Reward >= _max_reward)
                {
                  Reward = _max_reward;

                  break;
                }

                Reward += PromoData.played_reward;
                _played_used += PromoData.played;
              }
              //   PlayedUsed = Math.Max(SpentUsed, _played_used);
            }
          }

          // Check calculated reward does not exceed promotion limits...
          if ((PromoData.daily_limit > 0 && Reward > _remaining_daily_limit)
            || (PromoData.monthly_limit > 0 && Reward > _remaining_monthly_limit)
            || (PromoData.global_daily_limit > 0 && Reward > _remaining_global_daily_limit)
            || (PromoData.global_monthly_limit > 0 && Reward > _remaining_global_monthly_limit)
            || (PromoData.global_limit > 0 && Reward > _remaining_global_limit))
          {
            Currency _min_limit;

            // Find out the lowest minimum
            _min_limit = Decimal.MaxValue;
            if (PromoData.daily_limit > 0)
            {
              _min_limit = Math.Min(_min_limit, _remaining_daily_limit);
            }
            if (PromoData.monthly_limit > 0)
            {
              _min_limit = Math.Min(_min_limit, _remaining_monthly_limit);
            }
            if (PromoData.global_daily_limit > 0)
            {
              _min_limit = Math.Min(_min_limit, _remaining_global_daily_limit);
            }
            if (PromoData.global_monthly_limit > 0)
            {
              _min_limit = Math.Min(_min_limit, _remaining_global_monthly_limit);
            }
            if (PromoData.global_limit > 0)
            {
              _min_limit = Math.Min(_min_limit, _remaining_global_limit);
            }

            Reward = _min_limit;

            if (PromoData.min_cash_in_reward > 0)
            {

              QuantityForMinCashIn = (Int32)Math.Ceiling(Reward / PromoData.min_cash_in_reward);
            }

            return PROMOTION_STATUS.PROMOTION_REDUCED_REWARD;
          }
        }

        return PROMOTION_STATUS.PROMOTION_OK;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return PROMOTION_STATUS.PROMOTION_GENERIC_ERROR;
      }
    } // CheckPromotionApplicable

    //------------------------------------------------------------------------------
    // PURPOSE : Requested level is selected?.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Level: Level to be asked for.
    //          - LevelFilter: Level mask number.
    //
    //      - OUTPUT :
    //
    // RETURNS : Boolenan: Requested level is selected?.
    //
    //   NOTES :
    //    
    private static Boolean IsLevelSelected(Int32 Level, Int32 LevelFilter)
    {
      String _str_binary;
      char[] bit_array;

      if (LevelFilter == 0)
      {

        return true;
      }
      _str_binary = Convert.ToString(LevelFilter, 2);
      _str_binary = new string('0', 5 - _str_binary.Length) + _str_binary;

      bit_array = _str_binary.ToCharArray();

      return (bit_array[Level] == '1') ? true : false;
    } // IsLevelSelected

    //------------------------------------------------------------------------------
    // PURPOSE : Get Best applicable promotion.
    //
    //  PARAMS :
    //      - INPUT :
    //          - AccountId
    //          - AmountToAdd
    //          - SqlTrx
    //
    //      - OUTPUT :
    //          - PromotionId
    //          - Reward
    //
    // RETURNS : DataTable: List of applicable promotion list
    //
    //   NOTES :
    //    
    public static void GetBestApplicablePromotion(Int64 AccountId,
                                                  Currency AmountToAdd,
                                                  out Int64 PromotionId,
                                                  out Currency Reward,
                                                  out Currency SpentUsed,
                                                  SqlTransaction SqlTrx)
    {
      Currency _cash_in;
      Int32 _num_tokens;
      Int64 _promo_id;
      Currency _reward;
      Currency _won_lock;
      Int64 _max_promo_id;
      Currency _max_reward;
      Boolean _rc;
      DataTable _dt;
      WSI.Common.Promotion.PROMOTION_STATUS _promo_status;
      TYPE_PROMOTION_DATA _promo_data;
      Currency _spent;
      Currency _played;
      Currency _spent_used;
      Currency _max_spent_used;
      DataTable dt_data_terminal;
      DataTable _account_flags;

      PromotionId = 0;
      Reward = 0;
      SpentUsed = 0;

      _num_tokens = 0;
      _reward = 0;
      _won_lock = 0;
      _max_promo_id = 0;
      _max_reward = 0;
      _max_spent_used = 0;

      _dt = Promotion.GetApplicablePromotionList(AccountId,
                                                 "None",
                                                 out _promo_status,
                                                 SqlTrx);

      if (_dt.Rows.Count == 0)
      {
        return;
      }

      // AMF 05-SEP-2013: Calculate spent per terminal
      dt_data_terminal = AccountRemainingDataPerTerminal(AccountId, SqlTrx);

      if (GetActiveFlags(AccountId, SqlTrx, out _account_flags))
      {
        return;
      }

      foreach (DataRow _row in _dt.Rows)
      {
        _promo_id = (Int64)_row["PM_PROMOTION_ID"];
        if (_promo_id == 0)
        {
          continue;
        }

        _cash_in = AmountToAdd;
        _num_tokens = 0;
        _reward = 0;
        _spent_used = 0;
        _won_lock = 0;

        _promo_data = new TYPE_PROMOTION_DATA();
        _rc = Promotion.ReadPromotionData(SqlTrx, _promo_id, _promo_data);
        if (!_rc)
        {
          // Log error
          Log.Error("GetBestApplicablePromotion. Promotion not found: " + _promo_id + ".");
          _promo_status = Promotion.PROMOTION_STATUS.PROMOTION_NOT_FOUND;
        }
        else
        {
          // RCI 16-NOV-2010: MB doesn't assing promotions with special permission.
          if (_promo_data.special_permission != PROMOTION_SPECIAL_PERMISSION.PROMOTION_SPECIAL_PERMISSION_NOT_SET)
          {
            continue;
          }

          // AMF 05-SEP-2013: Calculate spent per terminal
          // SDS 22-10-2015: calculate played & spent per terminal in one function only
          CaculateDataByTerminal(dt_data_terminal, _promo_data.terminal_list, _promo_id, out _spent, out _played, SqlTrx);

          _promo_status = Promotion.CheckPromotionApplicable(SqlTrx,
                                                             _promo_id,
                                                             AccountId,
                                                             true,
                                                             _account_flags,
                                                             ref _cash_in,
                                                             _spent,
                                                             _played,
                                                             ref _num_tokens,
                                                             ref _promo_data,
                                                             out _reward,
                                                             out _won_lock,
                                                             out _spent_used,
                                                             WGDB.Now);     // To Date
        }

        if (_promo_status == PROMOTION_STATUS.PROMOTION_OK
            || _promo_status == PROMOTION_STATUS.PROMOTION_REDUCED_REWARD)
        {
          // MB doesn't assing promotions with won lock.
          if (_won_lock == 0)
          {
            if (_max_reward < _reward)
            {
              _max_promo_id = _promo_id;
              _max_reward = _reward;
              _max_spent_used = _spent_used;
            }
          }
        }
      }

      PromotionId = _max_promo_id;
      Reward = _max_reward;
      SpentUsed = _max_spent_used;

    } // GetBestApplicablePromotion




    public static DataTable GetApplicablePromotionList(Int64 AccountId,
                                                      String NoPromotionStr,
                                                      out Promotion.PROMOTION_STATUS Status,
                                                      SqlTransaction SqlTrx)
    {
      Status = PROMOTION_STATUS.PROMOTION_OK;

      return GetApplicablePromotionList(AccountId, NoPromotionStr, null, out Status, SqlTrx);
    }
    //------------------------------------------------------------------------------
    // PURPOSE : Get applicable promotion list.
    //
    //  PARAMS :
    //      - INPUT :
    //          - AccountId
    //          - NoPromotionStr
    //          - SqlTrx
    //
    // RETURNS : DataTable: List of applicable promotion list
    //
    //   NOTES :
    public static DataTable GetApplicablePromotionList(Int64 AccountId,
                                                       String NoPromotionStr,
                                                       List<ACCOUNT_PROMO_CREDIT_TYPE> SelectedCreditType,
                                                       out Promotion.PROMOTION_STATUS Status,
                                                       SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      SqlCommand _sql_command;

      Status = PROMOTION_STATUS.PROMOTION_OK;

      try
      {
        _sb = new StringBuilder();

        // Manual promotions
        _sb.AppendLine("  SELECT   CAST(NULL AS VARBINARY) AS IMAGE ");
        _sb.AppendLine("         , PM_PROMOTION_ID ");
        _sb.AppendLine("         , PM_NAME ");
        _sb.AppendLine("         , CASE WHEN PM_WON_LOCK IS NULL THEN 0 ELSE 1 END ");
        _sb.AppendLine("    FROM   PROMOTIONS ");
        _sb.AppendLine("   WHERE   (PM_TYPE    = @pTypeManual ");
        _sb.AppendLine("      OR    PM_TYPE    = @pTypePreassigned) ");
        _sb.AppendLine("     AND   PM_ENABLED = 1 ");
        _sb.AppendLine("     AND   GETDATE() BETWEEN PM_DATE_START AND PM_DATE_FINISH ");
        if (SelectedCreditType != null)
        {
          String _str_credit_type = "";

          foreach (ACCOUNT_PROMO_CREDIT_TYPE _credit_type in SelectedCreditType)
          {
            _str_credit_type += (Int32)_credit_type + ", ";
          }
          _str_credit_type = _str_credit_type.TrimEnd(new Char[] { ' ', ',' });
          _sb.AppendLine("     AND   PM_CREDIT_TYPE IN( " + _str_credit_type + ")");
        }

        _sb.AppendLine("   UNION ALL ");

        _sb.AppendLine("  SELECT   CAST(NULL AS VARBINARY), 0, ' ', 0 ");

        _sb.AppendLine("ORDER BY   PM_NAME ");


        using (_sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          // Only manual promotions must be shown
          _sql_command.Parameters.Add("@pTypeManual", SqlDbType.Int).Value = (Int32)PROMOTION_TYPE.MANUAL;
          _sql_command.Parameters.Add("@pTypePreassigned", SqlDbType.Int).Value = (Int32)PROMOTION_TYPE.PREASSIGNED;

          using (DataSet _ds = new DataSet())
          {
            using (SqlDataAdapter _da = new SqlDataAdapter(_sql_command))
            {
              _da.Fill(_ds);

              return ProcessPromotions(AccountId, NoPromotionStr, ref Status, SqlTrx, _ds, WGDB.Now, WGDB.Now);
            }
          }
        }
      } // try
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return null;
      }

    } // GetApplicablePromotionList

    ////------------------------------------------------------------------------------
    //// PURPOSE : Get applicable promotion list available in specific date.
    ////
    ////  PARAMS :
    ////      - INPUT :
    ////          - AccountId
    ////          - NoPromotionStr
    ////          - SqlTrx
    ////          - Status
    ////          - Date of the calculation to obtain the promotions
    ////
    //// RETURNS : DataTable: List of applicable promotion list
    ////
    ////   NOTES :
    ////    
    //public static DataTable GetDateApplicablePromotionList (Int64 AccountId,
    //                                                        String NoPromotionStr,
    //                                                        out Promotion.PROMOTION_STATUS Status,
    //                                                        SqlTransaction SqlTrx, 
    //                                                        DateTime PromotionsDate)
    //{
    //  String _sql_query;
    //  SqlCommand _sql_command;
    //  DataSet _ds;
    //  SqlDataAdapter _da;

    //  _ds = null;
    //  _da = null;

    //  Status = PROMOTION_STATUS.PROMOTION_OK;

    //  try
    //  {
    //    _sql_query = "SELECT PM_PROMOTION_ID " +
    //                      ", ISNULL(PM_NAME, '') as PM_NAME" +
    //                      ", ISNULL(PM_LARGE_RESOURCE_ID, 0) as PM_LARGE_RESOURCE_ID " +
    //                      ", ISNULL(PM_SMALL_RESOURCE_ID, 0) as PM_SMALL_RESOURCE_ID " +
    //                      ", '' AS PM_DESCRIPTION " +
    //                   "FROM PROMOTIONS " +
    //                  "WHERE PM_ENABLED = 1 " +
    //                    "AND PM_TYPE = @PromoType " +
    //                    "AND @PromotionsDate BETWEEN PM_DATE_START AND PM_DATE_FINISH " +
    //               "ORDER BY PM_NAME";

    //    _sql_command = new SqlCommand (_sql_query, SqlTrx.Connection, SqlTrx);

    //    // Only manual promotions must be shown
    //    _sql_command.Parameters.Add ("@PromoType", SqlDbType.Int).Value = 0;
    //    _sql_command.Parameters.Add ("@PromotionsDate", SqlDbType.DateTime).Value = PromotionsDate;
    //    _ds = new DataSet ();
    //    _da = new SqlDataAdapter (_sql_command);
    //    _da.Fill (_ds);

    //    return ProcessPromotions (AccountId, NoPromotionStr, ref Status, SqlTrx, _ds, WGDB.Now);

    //  } // try
    //  catch ( Exception _ex )
    //  {
    //    Log.Exception (_ex);

    //    return null;
    //  }
    //  finally
    //  {
    //    // Close connection
    //    if ( _da != null )
    //    {
    //      _da.Dispose ();
    //      _da = null;
    //    }

    //    if ( _ds != null )
    //    {
    //      _ds.Dispose ();
    //      _ds = null;
    //    }
    //  } // finally

    //} // GetDateApplicablePromotionList

    ////------------------------------------------------------------------------------
    //// PURPOSE : Get applicable promotion list specifying the dates.
    ////
    ////  PARAMS :
    ////      - INPUT :
    ////          - AccountId
    ////          - NoPromotionStr
    ////          - SqlTrx
    ////          - Status of the promotion
    ////          - FromDate: Date Until the promotions don't have to be shown
    ////          - PromotionsDate: Date of calculation of promotions
    ////
    //// RETURNS : DataTable: List of applicable promotion list
    ////
    ////   NOTES :
    ////    
    //public static DataTable GetApplicablePromotionListByDates (Int64 AccountId,
    //                                                           String NoPromotionStr,
    //                                                           out Promotion.PROMOTION_STATUS Status,
    //                                                           SqlTransaction SqlTrx, 
    //                                                           DateTime PromotionsDate, 
    //                                                           DateTime FromDate)
    //{
    //  String _sql_query;
    //  SqlCommand _sql_command;
    //  DataSet _ds;
    //  SqlDataAdapter _da;

    //  _ds = null;
    //  _da = null;

    //  Status = PROMOTION_STATUS.PROMOTION_OK;

    //  try
    //  {
    //    _sql_query = "SELECT PM_PROMOTION_ID " +
    //                      ", ISNULL(PM_NAME, '') as PM_NAME" +
    //                      ", ISNULL(PM_LARGE_RESOURCE_ID, 0) as PM_LARGE_RESOURCE_ID " +
    //                      ", ISNULL(PM_SMALL_RESOURCE_ID, 0) as PM_SMALL_RESOURCE_ID " +
    //                      ", '' AS PM_DESCRIPTION " +
    //                   "FROM PROMOTIONS " +
    //                  "WHERE PM_ENABLED = 1 " +
    //                    "AND PM_TYPE = @PromoType " +
    //                    "AND @PromotionsDate BETWEEN PM_DATE_START AND PM_DATE_FINISH " +
    //                    "AND PM_PROMOTION_ID NOT IN( " +
    //                    "SELECT PM_PROMOTION_ID " +
    //                    "FROM PROMOTIONS " +
    //                    "WHERE PM_ENABLED = 1 " +
    //                    "AND PM_TYPE = @PromoType " +
    //                    "AND @FromDate BETWEEN PM_DATE_START AND PM_DATE_FINISH)" +

    //               "ORDER BY PM_NAME";

    //    _sql_command = new SqlCommand (_sql_query, SqlTrx.Connection, SqlTrx);

    //    // Only manual promotions must be shown
    //    _sql_command.Parameters.Add ("@PromoType", SqlDbType.Int).Value = 0;
    //    _sql_command.Parameters.Add ("@PromotionsDate", SqlDbType.DateTime).Value = PromotionsDate;
    //    _sql_command.Parameters.Add ("@FromDate", SqlDbType.DateTime).Value = FromDate;
    //    _ds = new DataSet ();
    //    _da = new SqlDataAdapter (_sql_command);
    //    _da.Fill (_ds);

    //    return ProcessPromotions (AccountId, NoPromotionStr, ref Status, SqlTrx, _ds, PromotionsDate);

    //  } // try
    //  catch ( Exception _ex )
    //  {
    //    Log.Exception (_ex);

    //    return null;
    //  }
    //  finally
    //  {
    //    // Close connection
    //    if ( _da != null )
    //    {
    //      _da.Dispose ();
    //      _da = null;
    //    }

    //    if ( _ds != null )
    //    {
    //      _ds.Dispose ();
    //      _ds = null;
    //    } 
    //  } // finally

    //}

    //------------------------------------------------------------------------------
    // PURPOSE : Get applicable promotion list available in specific date.
    //
    //  PARAMS :
    //      - INPUT :
    //          - AccountId
    //          - NoPromotionStr
    //          - WhichOnes: 1-current  2-next  3-future
    //          - SqlTrx
    //          - Status
    //
    // RETURNS : DataTable: List of applicable promotion list
    //
    //   NOTES :

    public static DataTable GetAvailablePromotions(Int64 AccountId,
                                                   String NoPromotionStr,
                                                   Int32 WhichOnes,
                                                   out Promotion.PROMOTION_STATUS Status,
                                                   SqlTransaction SqlTrx)
    {
      StringBuilder _sql_query;
      StringBuilder _sql_where;
      SqlCommand _sql_command;
      DataSet _ds;
      SqlDataAdapter _da;
      DateTime _today_opening;
      DateTime _today_closing;
      DateTime _tomorrow_closing;
      DateTime _date_from;
      DateTime _date_to;

      _ds = null;
      _da = null;

      Status = PROMOTION_STATUS.PROMOTION_OK;

      try
      {
        _today_opening = Misc.TodayOpening();
        _today_closing = _today_opening.AddDays(1);
        _tomorrow_closing = _today_closing.AddDays(1);

        _sql_query = new StringBuilder();
        _sql_where = new StringBuilder();

        _sql_where.AppendLine(" ");

        switch (WhichOnes)
        {
          case 1:
            _sql_where.AppendLine("AND GETDATE() BETWEEN PM_DATE_START AND PM_DATE_FINISH");
            break;

          case 2:
            _sql_where.AppendLine("   AND (   ( @NowPlusOneSecond BETWEEN PM_DATE_START AND PM_DATE_FINISH )");
            _sql_where.AppendLine("        OR ( @TodayClosing     BETWEEN PM_DATE_START AND PM_DATE_FINISH )");
            _sql_where.AppendLine("        OR (    ( PM_DATE_START  BETWEEN @NowPlusOneSecond AND @TodayClosing )");
            _sql_where.AppendLine("            AND ( PM_DATE_FINISH BETWEEN @NowPlusOneSecond AND @TodayClosing )");
            _sql_where.AppendLine("           )");
            _sql_where.AppendLine("       )");
            break;

          case 3:
            _sql_where.AppendLine("   AND (   ( @TodayClosing       BETWEEN PM_DATE_START AND PM_DATE_FINISH )");
            _sql_where.AppendLine("        OR ( @TomorrowClosing    BETWEEN PM_DATE_START AND PM_DATE_FINISH )");
            _sql_where.AppendLine("        OR (    ( PM_DATE_START  BETWEEN @TodayClosing AND @TomorrowClosing )");
            _sql_where.AppendLine("            AND ( PM_DATE_FINISH BETWEEN @TodayClosing AND @TomorrowClosing )");
            _sql_where.AppendLine("           )");
            _sql_where.AppendLine("       )");
            break;

          default:
            return null;
        }

        _sql_query.AppendLine("SELECT PM_PROMOTION_ID");
        _sql_query.AppendLine("     , ISNULL(PM_NAME, '') as PM_NAME");
        _sql_query.AppendLine("     , ISNULL(PM_LARGE_RESOURCE_ID, 0) as PM_LARGE_RESOURCE_ID");
        _sql_query.AppendLine("     , ISNULL(PM_SMALL_RESOURCE_ID, 0) as PM_SMALL_RESOURCE_ID");
        _sql_query.AppendLine("     , '' AS PM_DESCRIPTION");
        _sql_query.AppendLine("  FROM PROMOTIONS");
        _sql_query.AppendLine(" WHERE PM_ENABLED = 1");
        _sql_query.AppendLine("   AND PM_TYPE = @PromoType");
        _sql_query.AppendLine("   AND PM_VISIBLE_ON_PROMOBOX = 1");
        _sql_query.AppendLine(_sql_where.ToString());
        _sql_query.AppendLine(" ORDER BY PM_NAME");

        _sql_command = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx);

        // Only manual promotions must be shown
        _sql_command.Parameters.Add("@PromoType", SqlDbType.Int).Value = (Int32)PROMOTION_TYPE.MANUAL;

        switch (WhichOnes)
        {
          case 1:
            _date_from = WGDB.Now;
            _date_to = WGDB.Now;
            break;

          case 2:
            _date_from = WGDB.Now.AddSeconds(1);
            _date_to = _today_closing;

            _sql_command.Parameters.Add("@NowPlusOneSecond", SqlDbType.DateTime).Value = _date_from;
            _sql_command.Parameters.Add("@TodayClosing", SqlDbType.DateTime).Value = _date_to;
            break;

          case 3:
            _date_from = _today_closing.AddSeconds(1);
            _date_to = _tomorrow_closing;

            _sql_command.Parameters.Add("@TodayClosing", SqlDbType.DateTime).Value = _today_closing;
            _sql_command.Parameters.Add("@TomorrowClosing", SqlDbType.DateTime).Value = _tomorrow_closing;
            break;

          default:
            return null;
        }

        _ds = new DataSet();
        _da = new SqlDataAdapter(_sql_command);
        _da.Fill(_ds);

        return ProcessPromotions(AccountId, NoPromotionStr, ref Status, SqlTrx, _ds, _date_from, _date_to);

      } // try
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return null;
      }
      finally
      {
        // Close connection
        if (_da != null)
        {
          _da.Dispose();
          _da = null;
        }

        if (_ds != null)
        {
          _ds.Dispose();
          _ds = null;
        }
      } // finally

    } // GetAvailablePromotions

    public static Boolean GetAccountCanApplyRechargesPromotion(Int32 TerminalId, Int64 AccountId, Int32 NumHours, SqlTransaction SqlTrx)
    {
      Currency _total_recharges_amount;
      Currency _total_recharges_amount_aux;
      Currency _zero_recharges_amount;
      DataTable _dt_promotions;
      TYPE_PROMOTION_DATA _promo_data;
      long _promotion_id;
      DataRow[] _dr;
      DataTable _account_flags;
      DataTable dt_data_terminal;
      Currency _spent;
      Currency _played;
      DateTime _now;
      Currency _total_recharges_reward;
      Currency _zero_recharges_reward;
      Currency _won_lock;
      Currency _spent_used;
      Currency _available_limit;
      Int32 _num_tokens;
      Promotion.PROMOTION_STATUS _status;

      _status = PROMOTION_STATUS.PROMOTION_OK;
      _now = WGDB.Now;
      _num_tokens= 0;
      _zero_recharges_amount = 0;

      try
      {
        if (!Promotion.GetActiveFlags(AccountId, SqlTrx, out  _account_flags))
        {
          return false;
        }

        dt_data_terminal = Promotion.AccountRemainingDataPerTerminal(AccountId, SqlTrx);

        GetLastUnpromotionedRecharges(AccountId, NumHours, SqlTrx, out _total_recharges_amount);
        // ETP 12/08/2015 BUG-3693  Fixed. Saving value of _total_recharges_amount because CheckPromotionApplicable replace it.
        _total_recharges_amount_aux = _total_recharges_amount;

        if (_total_recharges_amount == 0)
        {
          return false;
        }

        _dt_promotions = GetCandidatePromotions(TerminalId , SqlTrx);

        foreach (DataRow _row in _dt_promotions.Rows)
        {
          _total_recharges_reward = 0;
          _zero_recharges_reward = 0;
          _zero_recharges_amount = 0;
          // ETP 12/08/2015 BUG-3693  Fixed. Getting value of _total_recharges_amount for next iterations.
          _total_recharges_amount = _total_recharges_amount_aux;

          _promotion_id = (long)_row["PM_PROMOTION_ID"];

          _promo_data = new TYPE_PROMOTION_DATA();

          if (!Promotion.ReadPromotionData(SqlTrx, _promotion_id, _promo_data))
          {
            // Log error
            Log.Error("GetAccountCanApplyRechargesPromotion. Promotion not found: " + _promotion_id + ".");

            _dr = _dt_promotions.Select("PM_PROMOTION_ID = " + _promotion_id);
            _dr[0].Delete();
          }
          // SDS 22-10-2015: calculate played & spent per terminal in one function only
          Promotion.CaculateDataByTerminal(dt_data_terminal,
                                             _promo_data.terminal_list,
                                             _promotion_id,
                                             out _spent,out _played,
                                             SqlTrx);


          // Call with zero recharges amount 
          _status = Promotion.CheckPromotionApplicable(SqlTrx,
                                                      _promotion_id,
                                                      AccountId,
                                                      true,
                                                      _account_flags,
                                                      ref _zero_recharges_amount,
                                                      _spent,
                                                      _played,
                                                      ref _num_tokens,
                                                      ref _promo_data,
                                                      out _zero_recharges_reward,
                                                      out _won_lock,
                                                      out _spent_used,
                                                      out _available_limit,
                                                      _now,
                                                      _now);

          _row["PM_REWARD"] = (Decimal)_zero_recharges_reward;

          _status = Promotion.CheckPromotionApplicable(SqlTrx,
                                                      _promotion_id,
                                                      AccountId,
                                                      true,
                                                      _account_flags,
                                                      ref _total_recharges_amount,
                                                      _spent,
                                                      _played ,
                                                      ref _num_tokens,
                                                      ref _promo_data,
                                                      out _total_recharges_reward,
                                                      out _won_lock,
                                                      out _spent_used,
                                                      out _available_limit,
                                                      _now,
                                                      _now);

          if (_total_recharges_reward - _zero_recharges_reward > 0)
          {
            return true;
          }
        } // foreach

        return false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get last recharges that haven't be used in any promotion.
    //
    //  PARAMS :
    //      - INPUT :
    //          - AccountId
    //          - NumDays    
    //          - SqlTrx
    //
    //      - OUTPUT :
    //          - TotalRechargesAmount
    //
    // RETURNS : DataTable: List of last unpromotioned recharges
    //
    //   NOTES :
    public static DataTable GetLastUnpromotionedRecharges(Int64 AccountId, Int32 NumHours, SqlTransaction SqlTrx, out Currency TotalRechargesAmount)
    {
      StringBuilder _sql_query;
      DateTime _date_from;
      DataTable _dt_recharges;

      TotalRechargesAmount = 0;

      try
      {
        _date_from = WGDB.Now.AddHours(NumHours * -1);

        // Get the last recharges total amount
        _sql_query = new StringBuilder();

        _sql_query.AppendLine("SELECT   AO_OPERATION_ID                                                                                  ");
        _sql_query.AppendLine("       , AO_DATETIME                                                                                      ");
        _sql_query.AppendLine("       , isnull (AO_AMOUNT, 0) AS AO_AMOUNT                                                               ");
        _sql_query.AppendLine("  FROM   ACCOUNT_OPERATIONS WITH (INDEX(IX_ao_code_date_account))                                         ");
        _sql_query.AppendLine(" WHERE   AO_ACCOUNT_ID = @pAccountID                                                                      ");
        _sql_query.AppendLine("   AND   AO_OPERATION_ID >= ( SELECT   MAX(OPERATION_ID)                                                  ");
        _sql_query.AppendLine("                                FROM  ( SELECT   ISNULL (MAX(ACP_OPERATION_ID), 0) AS OPERATION_ID        ");
        _sql_query.AppendLine("                                          FROM   ACCOUNT_PROMOTIONS  WITH (INDEX(IX_acp_account_status))  ");
        _sql_query.AppendLine("                                         WHERE   ACP_ACCOUNT_ID = @pAccountID                             ");
        _sql_query.AppendLine("                                           AND   ACP_PROMO_TYPE = 0                                       ");
        _sql_query.AppendLine("                                           AND   NOT ACP_CASH_IN IS NULL                                  ");
        _sql_query.AppendLine("                                           AND   ACP_CREATED >= @pFromDate                                ");
        _sql_query.AppendLine("                                         UNION                                                            ");
        _sql_query.AppendLine("                                        SELECT   ISNULL (MAX(AO_OPERATION_ID), 0)   AS OPERATION_ID       ");
        _sql_query.AppendLine("                                          FROM   ACCOUNT_OPERATIONS WITH (INDEX(IX_ao_code_date_account)) ");
        _sql_query.AppendLine("                                         WHERE   AO_ACCOUNT_ID = @pAccountID                              ");
        _sql_query.AppendLine("                                           AND   AO_CODE IN (   @pOperationcodeCashOut                    ");
        _sql_query.AppendLine("                                                              , @pOperationcodeTransferCreditOut          ");
        _sql_query.AppendLine("                                                            )                                             ");
        _sql_query.AppendLine("                                           AND   AO_DATETIME >= @pFromDate                                ");
        _sql_query.AppendLine("                                      ) AS MAX_OPERATION_ID                                               ");
        _sql_query.AppendLine("                             )                                                                            ");
        _sql_query.AppendLine("   AND   AO_CODE IN (   @pOperationCodeCashIn                                                             ");
        _sql_query.AppendLine("                      , @pOperationCodeMBCashIn                                                           ");
        _sql_query.AppendLine("                      , @pOperationcodeNACashIn)                                                          ");
        _sql_query.AppendLine("   AND   AO_DATETIME >= @pFromDate                                                                        ");

        using (SqlCommand _sql_command = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_command.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = AccountId;
          _sql_command.Parameters.Add("@pOperationcodeCashOut", SqlDbType.BigInt).Value = OperationCode.CASH_OUT;
          _sql_command.Parameters.Add("@pOperationcodeTransferCreditOut", SqlDbType.BigInt).Value = OperationCode.TRANSFER_CREDIT_OUT;
          _sql_command.Parameters.Add("@pOperationCodeCashIn", SqlDbType.BigInt).Value = OperationCode.CASH_IN;
          _sql_command.Parameters.Add("@pOperationCodeMBCashIn", SqlDbType.BigInt).Value = OperationCode.MB_CASH_IN;
          _sql_command.Parameters.Add("@pOperationcodeNACashIn", SqlDbType.BigInt).Value = OperationCode.NA_CASH_IN;
          _sql_command.Parameters.Add("@pFromDate", SqlDbType.DateTime).Value = _date_from;

          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_command))
          {
            using (DataSet _ds = new DataSet())
            {
              _da.Fill(_ds);

              _dt_recharges = _ds.Tables[0];
            }
          }

          if ( _dt_recharges.Rows.Count > 0 )
          {
            TotalRechargesAmount = (Decimal) _dt_recharges.Compute("Sum(AO_AMOUNT)", "");
          }
        }

        return _dt_recharges;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return null;

    } // GetLastUnpromotionedRecharges


    //------------------------------------------------------------------------------
    // PURPOSE : Queries currently applicable promotion DataTable list available according to last recharges.
    //
    //  PARAMS :
    //      - INPUT :
    //          - 
    //          - SqlTrx
    //          - Status
    //
    // RETURNS : DataTable: List of applicable promotion list
    //
    //   NOTES :
    private static DataTable GetCandidatePromotions(Int32 TerminalId, SqlTransaction SqlTrx)
    {
      StringBuilder _sql_query;
      DataTable _dt_promotions;
      Int32 _terminal_type;
      PROMOTION_AWARD_TARGET _flag_pm_award;

      _dt_promotions = null;

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand("SELECT [te_terminal_type] FROM [terminals] WHERE [te_terminal_id] = @pTerminalId", SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          int _result = 0;
          string _string_result = _sql_cmd.ExecuteScalar().ToString();
          Int32.TryParse(_string_result, out _result);
          if (_result == 0)
            return null;

          _terminal_type = _result;

          switch ((TerminalTypes)_terminal_type)
          {
            case TerminalTypes.SAS_HOST:
              _flag_pm_award = PROMOTION_AWARD_TARGET.INTOUCH;
              break;
            case TerminalTypes.PROMOBOX:
            case TerminalTypes.WIN_UP:
              _flag_pm_award = PROMOTION_AWARD_TARGET.PROMOBOX;
              break;
            default:
              return null;
          }
        }

        _sql_query = new StringBuilder();

        _sql_query.AppendLine("  SELECT   PM_PROMOTION_ID                                         ");
        _sql_query.AppendLine("         , PM_NAME                                                 ");
        _sql_query.AppendLine("         , PM_TEXT_ON_PROMOBOX                                     ");
        _sql_query.AppendLine("         , ISNULL(PM_LARGE_RESOURCE_ID, 0) as PM_LARGE_RESOURCE_ID ");
        _sql_query.AppendLine("         , ISNULL(PM_SMALL_RESOURCE_ID, 0) as PM_SMALL_RESOURCE_ID ");
        _sql_query.AppendLine("         , '' AS PM_DESCRIPTION                                    ");
        _sql_query.AppendLine("         , PM_CREDIT_TYPE                                          ");
        _sql_query.AppendLine("         , CAST(0.0 AS money) AS PM_REWARD                         ");
        _sql_query.AppendLine("         , CAST(0.0 AS money) AS PM_RECHARGES_REWARD               ");
        _sql_query.AppendLine("         , CAST(0.0 AS money) AS PM_AVAILABLE_LIMIT                ");
        _sql_query.AppendLine("    FROM   PROMOTIONS                                              ");
        _sql_query.AppendLine("   WHERE   ( PM_TYPE    = @pTypeManual                             ");
        _sql_query.AppendLine("          OR PM_TYPE    = @pTypePreassigned)                       ");
        _sql_query.AppendLine("     AND   PM_PERMISSION = 0                                       ");
        _sql_query.AppendLine("     AND   PM_ENABLED = 1                                          ");
        _sql_query.AppendLine("     AND   GETDATE() BETWEEN PM_DATE_START AND PM_DATE_FINISH      ");
        _sql_query.AppendLine("     AND   (PM_AWARD_ON_PROMOBOX & @_flag_pm_award) = @_flag_pm_award");


        if (TITO.Utils.IsTitoMode())
        {
          if (!(GeneralParam.GetBoolean("WigosKiosk", "Promotions.RE.PrintTITOTicket", false) && _flag_pm_award == PROMOTION_AWARD_TARGET.PROMOBOX))
          {
            _sql_query.AppendLine("   AND   PM_CREDIT_TYPE NOT IN (@pPromoRedeem, @pPromoRedeemInKind)         ");
          }
        }


        using (SqlCommand _sql_command = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
        {
          // Only manual & preassigned promotions must be shown
          _sql_command.Parameters.Add("@pTypeManual", SqlDbType.Int).Value = (Int32)PROMOTION_TYPE.MANUAL;
          _sql_command.Parameters.Add("@pTypePreassigned", SqlDbType.Int).Value = (Int32)PROMOTION_TYPE.PREASSIGNED;
          _sql_command.Parameters.Add("@pPromoRedeem", SqlDbType.Int).Value = (Int32)ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE;
          _sql_command.Parameters.Add("@pPromoRedeemInKind", SqlDbType.Int).Value = (Int32)ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE_IN_KIND;
          _sql_command.Parameters.Add("@_flag_pm_award", SqlDbType.Int).Value = (Int32)_flag_pm_award;


          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_command))
          {
            using (DataSet _ds = new DataSet())
            {
              _da.Fill(_ds);

              _dt_promotions = _ds.Tables[0];
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _dt_promotions;

    } // GetCandidatePromotions


    //------------------------------------------------------------------------------
    // PURPOSE : Gets applicable promotion list (by spent, by past recharges and for free) available to the account.
    //
    //  PARAMS :
    //      - INPUT :
    //          - AccountId
    //          - NumDays
    //          - NoPromotionStr
    //          - SqlTrx
    //          - Status
    //
    // RETURNS : DataTable: List of applicable promotion list
    //
    //   NOTES :
    public static DataRow[] GetAccountAvailablePromos(Int32 TerminalId,
                                                      Int64 AccountId,
                                                      Int32 NumHours,
                                                      SqlTransaction SqlTrx)
    {
      Currency _total_recharges_amount;
      Currency _total_recharges_amount_aux;
      Currency _zero_recharges_amount;
      DataTable _dt_promotions;
      TYPE_PROMOTION_DATA _promo_data;
      long _promotion_id;
      DataRow[] _dr;
      DataTable _account_flags;
      DataTable dt_data_terminal;
      Currency _spent;
      Currency _played;
      DateTime _now;
      Currency _total_recharges_reward;
      Currency _zero_recharges_reward;
      Currency _won_lock;
      Currency _spent_used;
      Currency _available_limit;
      Int32 _num_tokens;
      Promotion.PROMOTION_STATUS _status;

      _status = PROMOTION_STATUS.PROMOTION_OK;
      _now = WGDB.Now;
      _num_tokens= 0;
      _zero_recharges_amount = 0;

      try
      {
        if (!Promotion.GetActiveFlags(AccountId, SqlTrx, out  _account_flags))
        {
          return null;
        }

        dt_data_terminal = Promotion.AccountRemainingDataPerTerminal(AccountId, SqlTrx);

        GetLastUnpromotionedRecharges(AccountId, NumHours, SqlTrx, out _total_recharges_amount);
        // ETP 12/08/2015 BUG-3693  Fixed. Saving value of _total_recharges_amount because CheckPromotionApplicable replace it.
        _total_recharges_amount_aux = _total_recharges_amount;

        _dt_promotions = GetCandidatePromotions(TerminalId, SqlTrx);

        foreach (DataRow _row in _dt_promotions.Rows)
        {
          // ETP 12/08/2015 BUG-3693  Fixed. Getting value of _total_recharges_amount for next iterations.
          _total_recharges_amount = _total_recharges_amount_aux;
          _total_recharges_reward = 0;
          _zero_recharges_reward = 0;
          _zero_recharges_amount = 0;

          _promotion_id = (long)_row["PM_PROMOTION_ID"];

          _promo_data = new TYPE_PROMOTION_DATA();

          if (!Promotion.ReadPromotionData(SqlTrx, _promotion_id, _promo_data))
          {
            // Log error
            Log.Error("GetAvailableRechargesPromotions. Promotion not found: " + _promotion_id + ".");

            _dr = _dt_promotions.Select("PM_PROMOTION_ID = " + _promotion_id);
            _dr[0].Delete();
          }

          // SDS 22-10-2015: calculate played & spent per terminal in one function only
          Promotion.CaculateDataByTerminal(dt_data_terminal,
                                             _promo_data.terminal_list,
                                             _promotion_id,
                                             out _spent,out _played,
                                             SqlTrx);


          // Call with zero recharges amount 
          _status = Promotion.CheckPromotionApplicable(SqlTrx,
                                                       _promotion_id,
                                                       AccountId,
                                                       true,
                                                       _account_flags,
                                                       ref _zero_recharges_amount,
                                                       _spent,
                                                       _played,
                                                       ref _num_tokens,
                                                       ref _promo_data,
                                                       out _zero_recharges_reward,
                                                       out _won_lock,
                                                       out _spent_used,
                                                       out _available_limit,
                                                       _now,
                                                       _now);

          _row["PM_REWARD"] = (Decimal)_zero_recharges_reward;

          if (_total_recharges_amount > 0)
          {
            _status = Promotion.CheckPromotionApplicable(SqlTrx,
                                                         _promotion_id,
                                                         AccountId,
                                                         true,
                                                         _account_flags,
                                                         ref _total_recharges_amount,
                                                         _spent,
                                                         _played,
                                                         ref _num_tokens,
                                                         ref _promo_data,
                                                         out _total_recharges_reward,
                                                         out _won_lock,
                                                         out _spent_used,
                                                         out _available_limit,
                                                         _now,
                                                         _now);

            _row["PM_REWARD"] = (Decimal)_total_recharges_reward;
            _row["PM_RECHARGES_REWARD"] = (Decimal)_total_recharges_reward - _zero_recharges_reward;
          }

          if (_total_recharges_reward == 0 && _zero_recharges_reward == 0)
          {
            _row.Delete();
          }
        } // foreach

        _dt_promotions.AcceptChanges();

        return _dt_promotions.Select("", "PM_REWARD DESC, PM_TEXT_ON_PROMOBOX");

      } // try
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return null;
      }
      finally
      {

      } // finally

    } // GetAccountAvailablePromos

    private static DataTable ProcessPromotions(Int64 AccountId,
                                            String NoPromotionStr,
                                            ref Promotion.PROMOTION_STATUS Status,
                                            SqlTransaction SqlTrx,
                                            DataSet Promotions,
                                            DateTime DateFrom,
                                            DateTime DateTo)
    {
      return ProcessPromotions(AccountId,
                               NoPromotionStr,
                               ref Status,
                               SqlTrx,
                               Promotions,
                               DateFrom,
                               DateTo,
                               false);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Extract the invalid promotions from the list of received. (PROMOBOX VERSION)
    //
    //  PARAMS :
    //      - INPUT :
    //          - AccountId
    //          - NoPromotionStr
    //          - Status
    //          - SqlTrx 
    //          - _ds
    //  		- Date
    //
    // RETURNS : DataTable: List of applicable promotion list
    //
    //   NOTES :

    private static DataTable ProcessPromotions(Int64 AccountId,
                                               String NoPromotionStr,
                                               ref Promotion.PROMOTION_STATUS Status,
                                               SqlTransaction SqlTrx,
                                               DataSet Promotions,
                                               DateTime DateFrom,
                                               DateTime DateTo,
                                               Boolean OnlyPromotionsBySpentAndPreassigned)
    {
      WSI.Common.Promotion.PROMOTION_STATUS _promo_status;
      TYPE_PROMOTION_DATA _promo_data;
      Currency _spent_used_dummy;
      Currency _spent;
      Currency _played;
      DataTable dt_data_terminal;
      Boolean _not_redeemable_allowed;
      DataTable _account_flags;
      Boolean _reward_requested;
      Boolean _is_virtual_acccount;
      TYPE_ACCOUNT_DATA _account_data;

      _account_flags = null;

      // JAB 06-JUL-2012: Not redeemable credit control.
      _not_redeemable_allowed = true;
      Gift.GIFT_MSG _msg_gift = Gift.GIFT_MSG.UNKNOWN;
      _is_virtual_acccount = WSI.Common.TITO.Utils.IsVirtualAccountById(AccountId);

      // JPJ 14-DEC-2013 There is no need to take into account the virtual accounts.
      if (!WSI.Common.TITO.Utils.IsTitoMode() || !_is_virtual_acccount)
      {
        if (!Accounts.CheckAddNotRedeemable(AccountId, out _msg_gift))
        {
          _not_redeemable_allowed = false;
        }
      }

      // RCI 17-NOV-2010: Calculate spent.
      _spent = 0;
      dt_data_terminal = null;

      _reward_requested = (Promotions.Tables[0].Columns.Contains("PM_REWARD")
                         && Promotions.Tables[0].Columns.Contains("PM_AVAILABLE_LIMIT"));

      // JPJ 14-DEC-2013 There is no need to take into account the virtual accounts.
      if (!WSI.Common.TITO.Utils.IsTitoMode() || !_is_virtual_acccount)
      {
        if (Promotions.Tables[0].Rows.Count > 0)
        {
          // AMF 05-SEP-2013: Calculate spent per terminal
          dt_data_terminal = AccountRemainingDataPerTerminal(AccountId, SqlTrx);
        }
        if (!GetActiveFlags(AccountId, SqlTrx, out _account_flags))
        {
          Log.Warning("ProcessPromotions.GetActiveFlags  Error ocurred in reading from account_flags. AccountId:" + AccountId + ".");
        }
      }

      if (!ReadAccountData(SqlTrx, AccountId, out _account_data))
      {
        return new DataTable();
      }

      foreach (DataRow row in Promotions.Tables[0].Rows)
      {
        Int64 _promo_id;
        Currency _reward;
        Currency _won_lock;
        Currency _cash_in;
        Currency _available_limit;
        Int32 _num_tokens;
        Boolean _rc;
        Int32 _quantity_for_min_cash_in;

        _cash_in = 0;
        _num_tokens = 0;
        _promo_id = (Int64)row["PM_PROMOTION_ID"];
        _promo_status = PROMOTION_STATUS.PROMOTION_OK;

        if (_promo_id == 0)
        {
          row["PM_NAME"] = NoPromotionStr;
        }
        else
        {
          _promo_data = new TYPE_PROMOTION_DATA();
          _rc = Promotion.ReadPromotionData(SqlTrx, _promo_id, _promo_data, AccountId);
          if (!_rc)
          {
            // Log error
            Log.Error("PopulatePromotionsGrid. Promotion not found: " + _promo_id + ".");
            _promo_status = Promotion.PROMOTION_STATUS.PROMOTION_NOT_FOUND;
          }
          else
          {
            if (!Misc.IsVIP(_account_data.is_vip, _promo_data.is_vip))
            {
              row.Delete();

              continue;
            }

            if (_promo_data.won_lock_enabled && TITO.Utils.IsTitoMode())
            {//exclude lock promotions for virtual accounts
              row.Delete();

              continue;
            }
            if (_promo_data.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.NR1
                && !_not_redeemable_allowed)
            {
              _promo_status = PROMOTION_STATUS.PROMOTION_ACCT_NO_REDEEMABLE;
            }
            else
            {
              if (OnlyPromotionsBySpentAndPreassigned)
              {
                if (_promo_data.min_cash_in != 0)
                {
                  row.Delete();

                  continue;
                }
              }
              _played = 0;
              // JPJ 14-DEC-2013 There is no need to take into account the virtual accounts.
              if (!WSI.Common.TITO.Utils.IsTitoMode() || !_is_virtual_acccount)
              {
                // AMF 05-SEP-2013: Calculate spent per terminal
                // SDS 22-10-2015: calculate played & spent per terminal in one function only
                CaculateDataByTerminal(dt_data_terminal,
                                       _promo_data.terminal_list,
                                       _promo_id,
                                       out _spent,
                                       out _played,
                                       SqlTrx);

              }

              _quantity_for_min_cash_in = 1;
              _promo_status = Promotion.CheckPromotionApplicable(SqlTrx,
                                                                 _promo_id,
                                                                 AccountId,
                                                                 _reward_requested,
                                                                 _account_flags,
                                                                 ref _cash_in,
                                                                 _spent,
                                                                 _played,
                                                                 ref _num_tokens,
                                                                 ref _promo_data,
                                                                 out _reward,
                                                                 out _won_lock,
                                                                 out _spent_used_dummy,
                                                                 out _available_limit,
                                                                 DateFrom,    // From Date
                                                                 DateTo,     // To Date
                                                                 ref _quantity_for_min_cash_in,
                                                                 _account_data);

              if (_reward_requested)
              {
                if (_reward == 0)
                {
                  row.Delete();

                  continue;
                }
                row.BeginEdit();
                row["PM_REWARD"] = (Decimal)_reward;
                row["PM_AVAILABLE_LIMIT"] = (Decimal)_available_limit;
                row.EndEdit();
              }
            }
          }

          switch (_promo_status)
          {
            case Promotion.PROMOTION_STATUS.PROMOTION_OK:
            case Promotion.PROMOTION_STATUS.PROMOTION_OK_MODIFIED_AMOUNTS:
              break;

            case Promotion.PROMOTION_STATUS.PROMOTION_ACCT_ANONYMOUS:
              if (Status == PROMOTION_STATUS.PROMOTION_OK
                  || Status == PROMOTION_STATUS.PROMOTION_ACCT_HAS_PRIZE)
              {
                Status = _promo_status;
              }
              row.Delete();
              break;

            case Promotion.PROMOTION_STATUS.PROMOTION_ACCT_NO_REDEEMABLE:
              if (Status == PROMOTION_STATUS.PROMOTION_OK
                  || Status == PROMOTION_STATUS.PROMOTION_ACCT_HAS_PRIZE)
              {
                Status = _promo_status;
              }
              row.Delete();
              break;

            case Promotion.PROMOTION_STATUS.PROMOTION_ACCT_HAS_PRIZE:
              if (Status == PROMOTION_STATUS.PROMOTION_OK)
              {
                Status = _promo_status;
              }
              row.Delete();
              break;

            case Promotion.PROMOTION_STATUS.PROMOTION_REDUCED_REWARD:
              if (!_reward_requested)
              {
                row.Delete();
              }
              break;

            default:
              // Promotion cannot be applied, delete it from data set
              row.Delete();
              break;
          }
        }
      }

      Promotions.Tables[0].AcceptChanges();

      if (Promotions.Tables[0].Rows.Count > 1)
      {
        Status = PROMOTION_STATUS.PROMOTION_OK;
      }

      // Fill grid with remaining rows
      return Promotions.Tables[0];
    } // ProcessPromotions

    //------------------------------------------------------------------------------
    // PURPOSE : Get promotion id for player trackung.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlTrx
    //
    //      - OUTPUT :
    //          - PromotionId
    //
    // RETURNS : 
    //    - true: Successful 
    //    - false: Not successful
    //
    //   NOTES :
    //    
    public static Boolean GetPlayerTrackingPromotionId(out Int64 PromotionId, SqlTransaction Trx)
    {
      String _sql_query;
      SqlCommand _sql_command;
      Object _obj;

      PromotionId = 0;

      try
      {
        _sql_query = "SELECT TOP 1 PM_PROMOTION_ID " +
                       "FROM PROMOTIONS " +
                      "WHERE PM_TYPE = 1 ";

        _sql_command = new SqlCommand(_sql_query, Trx.Connection, Trx);

        _obj = _sql_command.ExecuteScalar();
        if (_obj != null)
        {
          PromotionId = (Int64)_obj;

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        // Not Found. Continue.
      }

      return false;

    } // GetPlayerTrackingPromotionId

    //------------------------------------------------------------------------------
    // PURPOSE : Check whether two date periods intersect or not
    //
    //  Condition 0       |--- Date Period 1 ---|  |--- Date Period 2 ---|
    //                 OR  
    //                    |--- Date Period 2 ---|  |--- Date Period 1 ---|
    //                    
    //
    //  Condition 1       |--- Date Period 1 ---|
    //                             |--- Date Period 2 ---|
    //
    //  Condition 2                |--- Date Period 1 ---|
    //                    |--- Date Period 2 ----|
    //
    //  Condition 3       |-------- Date Period 1 --------|
    //                         |--- Date Period 2 ---|
    //
    //  Condition 4            |--- Date Period 1 ---|
    //                    |-------- Date Period 2 --------|
    //
    //  PARAMS :
    //      - INPUT :
    //          - Period_1_Start
    //          - Period_1_End
    //          - Period_2_Start
    //          - Period_2_End
    //
    //      - OUTPUT :
    //
    // RETURNS : 
    //    - 0 : No intersection
    //    - 1 : Condition 1
    //    - 2 : Condition 2
    //    - 3 : Condition 3
    //    - 4 : Condition 4
    //
    //   NOTES :

    public static Int32 CheckDatePeriods(DateTime Period_1_Start,
                                          DateTime Period_1_End,
                                          DateTime Period_2_Start,
                                          DateTime Period_2_End)
    {
      if (Period_1_Start > Period_1_End
        || Period_2_Start > Period_2_End)
      {
        return 0;
      }

      if (Period_1_Start <= Period_2_Start)
      {
        if (Period_1_End >= Period_2_Start
          && Period_1_End <= Period_2_End)
        {
          return 1;   // Condition 1
        }

        if (Period_1_End >= Period_2_End)
        {
          return 3;   // Condition 3
        }
      }
      else
      {
        if (Period_2_End >= Period_1_Start
          && Period_2_End <= Period_1_End)
        {
          return 2;   // Condition 2
        }

        if (Period_2_End >= Period_1_End)
        {
          return 4;   // Condition 4
        }
      }

      return 0;
    } // CheckDatePeriods

    /// <summary>
    /// Obtain Expiration Date depending on expiration type (hours or days) and system closing time.
    /// </summary>
    /// <param name="SQLTransaction"></param>
    /// <param name="PromotionId"></param>
    /// <param name="ExpirationDate"></param>
    /// <returns></returns>
    public static Boolean GetExpirationDate(SqlTransaction SQLTransaction, Int64 PromotionId, out DateTime ExpirationDate)
    {
      EXPIRATION_TYPE _expiration_type;
      Int32 _expiration_value;
      Int32 _closing_time;
      DateTime _current_date;

      _current_date = WGDB.Now;
      ExpirationDate = _current_date;

      if (!ReadExpirationData(SQLTransaction, PromotionId, out _expiration_type, out _expiration_value))
      {
        return false;
      }

      switch (_expiration_type)
      {
        case EXPIRATION_TYPE.EXPIRATION_TYPE_HOURS:
          ExpirationDate = ExpirationDate.AddHours(_expiration_value);
          break;

        case EXPIRATION_TYPE.EXPIRATION_TYPE_DAYS:
          if (!Int32.TryParse(WSI.Common.Misc.ReadGeneralParams("WigosGUI", "ClosingTime"), out _closing_time))
          {
            return false;
          }

          ExpirationDate = new DateTime(_current_date.Year, _current_date.Month, _current_date.Day, _closing_time, 0, 0);
          if (_current_date.Hour < _closing_time)
          {
            ExpirationDate = ExpirationDate.AddDays(-1);
          }

          ExpirationDate = ExpirationDate.AddDays(_expiration_value);
          break;

        default:
          return false;
        //break;
      }

      return true;
    } // GetExpirationDate

    ////------------------------------------------------------------------------------
    //// PURPOSE: Returns the minimum number of tokens a promotion requires (if any) in
    ////          order to award some non-redeemable amount
    //// 
    ////  PARAMS:
    ////      - INPUT:
    ////        - SQLTransaction
    ////        - PromotionId
    ////
    ////      - OUTPUT:
    ////        - NumTokens
    ////        - TokenName
    ////
    //// RETURNS:
    ////    - true: Successful 
    ////    - false: Not successful
    //// 
    ////   NOTES:
    ////
    //public static Boolean AreTokensRequired(SqlTransaction SQLTransaction, Int64 PromotionId, out Int32 NumTokens, out String TokenName)
    //{
    //  TYPE_PROMOTION_DATA _promo_data;

    //  NumTokens = 0;
    //  TokenName = "";
    //  _promo_data = new TYPE_PROMOTION_DATA();
    //  if (!ReadPromotionData(SQLTransaction, PromotionId, ref _promo_data))
    //  {
    //    return false;
    //  }

    //  if (_promo_data.token_reward > 0 && _promo_data.num_tokens > 0)
    //  {
    //    NumTokens = _promo_data.num_tokens;
    //    TokenName = _promo_data.token_name;

    //    return true;
    //  }
    //  else
    //  {
    //    return false;
    //  }

    //} // AreTokensRequired

    //------------------------------------------------------------------------------
    // PURPOSE: Obtain promotion relevant data from database
    // 
    //  PARAMS:
    //      - INPUT:
    //        - SQLTransaction
    //        - PromotionId
    //
    //      - OUTPUT:
    //        - PromotionData
    //
    // RETURNS:
    //    - true: Successful 
    //    - false: Not successful
    // 
    //   NOTES:
    //
    public static Boolean ReadPromotionData(SqlTransaction SqlTrx, Int64 PromotionId, TYPE_PROMOTION_DATA PromotionData)
    {
      if (PromotionId == 0)
      {
        return false;
      }

      return ReadPromotionData(SqlTrx, PROMOTION_TYPE.UNKNOWN, PromotionId, PromotionData);
    } // ReadPromotionData
    public static Boolean ReadPromotionData(SqlTransaction SqlTrx, Int64 PromotionId, TYPE_PROMOTION_DATA PromotionData, Int64 AccountId)
    {
      if (PromotionId == 0)
      {
        return false;
    }

      return ReadPromotionData(SqlTrx, PROMOTION_TYPE.UNKNOWN, PromotionId, PromotionData, AccountId);
    } // ReadPromotionData
    /// <summary>
    /// Read promotion Data
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <param name="Type"></param>
    /// <param name="PromotionData"></param>
    /// <returns></returns>
    public static Boolean ReadPromotionData(SqlTransaction SqlTrx, PROMOTION_TYPE Type, out AccountPromotion PromotionData)
    {
      PromotionData = new AccountPromotion();

      return ReadPromotionData(SqlTrx, Type, 0, PromotionData, 0);
    } // ReadPromotionData
    private static Boolean ReadPromotionData(SqlTransaction SqlTrx, PROMOTION_TYPE PromoType, Int64 PromotionId, TYPE_PROMOTION_DATA PromotionData)
    {
      return ReadPromotionData(SqlTrx, PromoType, PromotionId, PromotionData, 0);
    } // ReadPromotionData
    private static Boolean ReadPromotionData(SqlTransaction SqlTrx, PROMOTION_TYPE PromoType, Int64 PromotionId, TYPE_PROMOTION_DATA PromotionData, Int64 AccountId)
    {
      String _xml;
      DateTime _now;


      StringBuilder _sb;

      _sb = new StringBuilder();

      if (AccountId == 0)
      {
        _sb.AppendLine(" DECLARE @pAccountId AS BIGINT                              ");
      }
      else
      {
        _sb.AppendLine(" IF EXISTS(                                               ");
        _sb.AppendLine("    (SELECT   TOP 1 ACP_PROMO_ID                          ");
        _sb.AppendLine("       FROM   ACCOUNT_PROMOTIONS                          ");
        _sb.AppendLine("      WHERE   ACP_PROMO_ID =  @pPromoId                   ");
        _sb.AppendLine("        AND   ACP_ACCOUNT_ID = @pAccountId                ");//Falta traerse el accountid
        _sb.AppendLine("        AND   ACP_CREATED >= @pOpeningTime                ");
        _sb.AppendLine("        AND   ACP_CREATED < @pClosingTime)                ");
        _sb.AppendLine(" ) AND                                                    ");
        _sb.AppendLine(" (                                                        ");
        _sb.AppendLine("   (SELECT PM_JOURNEY_LIMIT                               ");
        _sb.AppendLine("     FROM PROMOTIONS                                      ");
        _sb.AppendLine("    WHERE pm_promotion_id = @pPromoId ) = 1               ");
        _sb.AppendLine(" )                                                        ");
        _sb.AppendLine(" BEGIN                                                    ");
        _sb.AppendLine(" RETURN                                                   ");
        _sb.AppendLine(" END                                                      ");
      }

      _sb.AppendLine(" SELECT         PM_PROMOTION_ID                           ");
      _sb.AppendLine("              , ISNULL(PM_NAME, '')                       ");//1
      _sb.AppendLine("              , PM_ENABLED                                ");//2
      _sb.AppendLine("              , PM_TYPE                                   ");//3
      _sb.AppendLine("              , PM_DATE_START                             ");//4
      _sb.AppendLine("              , PM_DATE_FINISH                            ");//5
      _sb.AppendLine("              , PM_SCHEDULE_WEEKDAY                       ");//6
      _sb.AppendLine("              , PM_SCHEDULE1_TIME_FROM                    ");//7
      _sb.AppendLine("              , PM_SCHEDULE1_TIME_TO                      ");//8
      _sb.AppendLine("              , PM_SCHEDULE2_ENABLED                      ");//9
      _sb.AppendLine("              , PM_SCHEDULE2_TIME_FROM                    ");//10
      _sb.AppendLine("              , PM_SCHEDULE2_TIME_TO                      ");//11
      _sb.AppendLine("              , PM_GENDER_FILTER                          ");//12
      _sb.AppendLine("              , PM_BIRTHDAY_FILTER                        ");//13
      _sb.AppendLine("              , PM_WON_LOCK                               ");//14
      _sb.AppendLine("              , PM_MIN_CASH_IN                            ");//15
      _sb.AppendLine("              , PM_MIN_CASH_IN_REWARD                     ");//16
      _sb.AppendLine("              , PM_CASH_IN                                ");//17
      _sb.AppendLine("              , PM_CASH_IN_REWARD                         ");//18
      _sb.AppendLine("              , PM_NUM_TOKENS                             ");//19
      _sb.AppendLine("              , PM_TOKEN_REWARD                           ");//20
      _sb.AppendLine("              , PM_TOKEN_NAME                             ");//21
      _sb.AppendLine("              , PM_DAILY_LIMIT                            ");//22
      _sb.AppendLine("              , PM_MONTHLY_LIMIT                          ");//23
      _sb.AppendLine("              , PM_GLOBAL_DAILY_LIMIT                     ");//24
      _sb.AppendLine("              , PM_GLOBAL_MONTHLY_LIMIT                   ");//25
      _sb.AppendLine("              , PM_LEVEL_FILTER                           ");//26
      _sb.AppendLine("              , PM_PERMISSION                             ");//27
      _sb.AppendLine("              , ISNULL(PM_FREQ_FILTER_LAST_DAYS, 0)       ");//28
      _sb.AppendLine("              , ISNULL(PM_FREQ_FILTER_MIN_DAYS, 0)        ");//29
      _sb.AppendLine("              , ISNULL(PM_FREQ_FILTER_MIN_CASH_IN, 0)     ");//30
      _sb.AppendLine("              , ISNULL(PM_MIN_SPENT, 0)                   ");//31
      _sb.AppendLine("              , ISNULL(PM_MIN_SPENT_REWARD, 0)            ");//32
      _sb.AppendLine("              , ISNULL(PM_SPENT, 0)                       ");//33
      _sb.AppendLine("              , ISNULL(PM_SPENT_REWARD, 0)                ");//34
      _sb.AppendLine("              , PM_PROVIDER_LIST                          ");//35
      _sb.AppendLine("              , PM_OFFER_LIST                             ");//36
      _sb.AppendLine("              , ISNULL(PM_MIN_PLAYED, 0)                  ");//37
      _sb.AppendLine("              , ISNULL(PM_MIN_PLAYED_REWARD, 0)           ");//38
      _sb.AppendLine("              , ISNULL(PM_PLAYED, 0)                      ");//39
      _sb.AppendLine("              , ISNULL(PM_PLAYED_REWARD, 0)               ");//40
      _sb.AppendLine("              , PM_LAST_EXECUTED                          ");//41
      _sb.AppendLine("              , PM_NEXT_EXECUTION                         ");//42
      _sb.AppendLine("              , PM_EXPIRATION_TYPE                        ");//43
      _sb.AppendLine("              , PM_EXPIRATION_VALUE                       ");//44
      _sb.AppendLine("              , PM_CREDIT_TYPE                            ");//45
      _sb.AppendLine("              , PM_GLOBAL_LIMIT                           ");//46
      _sb.AppendLine("              , PM_EXPIRATION_LIMIT                       ");//47
      _sb.AppendLine("              , PM_PROVIDER_LIST                          ");//48
      _sb.AppendLine("              , ISNULL(PM_POINTS_TO_CREDITS_ID, 0)        ");//49
      _sb.AppendLine("              , ISNULL(PM_VIP, 0)                         ");//50
      _sb.AppendLine("              , ISNULL(PM_CREATED_ACCOUNT_FILTER, 0)      ");//51
      _sb.AppendLine("              , ISNULL(PM_APPLY_TAX, 0)                   ");//52
      //_sb.AppendLine("              , PM_PLAYED_TERMINAL_LIST                 ");//53
      _sb.AppendLine("              , PM_PROMOGAME_ID                           ");//54
      _sb.AppendLine("              , PM_PYRAMIDAL_DIST                         ");//55
      _sb.AppendLine("              FROM PROMOTIONS                             ");


      if (PromoType == PROMOTION_TYPE.UNKNOWN)
      {
        if (PromotionId == 0)
        {
          Log.Error("PromotionId not specified!");

          return false;
        }

        _sb.AppendLine("WHERE PM_PROMOTION_ID = @pPromoId");
      }
      else
      {
        StringBuilder _sql_sb;

        _sql_sb = new StringBuilder();
        _sql_sb.AppendLine("   SELECT   TOP 1 PM_PROMOTION_ID ");
        _sql_sb.AppendLine("     FROM   PROMOTIONS ");
        _sql_sb.AppendLine("    WHERE   PM_TYPE = @pPromoType ");
        _sql_sb.AppendLine(" ORDER BY   PM_PROMOTION_ID DESC ");

        _sb.AppendLine("WHERE PM_PROMOTION_ID = ( " + _sql_sb.ToString() + " )");
      }

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pPromoId", SqlDbType.BigInt).Value = PromotionId;

          if (AccountId != 0)
          {
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          }
          _sql_cmd.Parameters.Add("@pPromoType", SqlDbType.Int).Value = PromoType;
          _sql_cmd.Parameters.Add("@pOpeningTime", SqlDbType.DateTime).Value = Misc.TodayOpening();
          _sql_cmd.Parameters.Add("@pClosingTime", SqlDbType.DateTime).Value = Misc.TodayOpening().AddDays(1);

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            if (!_sql_reader.Read())
            {
              Log.Error("ReadPromotionData. Promo not found: " + PromotionId + ".");

              return false;
            }

            PromotionData.id = _sql_reader.GetInt64(0);
            PromotionData.name = _sql_reader.GetString(1);
            PromotionData.enabled = _sql_reader.GetBoolean(2);
            PromotionData.type = (PROMOTION_TYPE)_sql_reader.GetInt32(3);
            PromotionData.date_start = _sql_reader.GetDateTime(4);
            PromotionData.date_finish = _sql_reader.GetDateTime(5);
            PromotionData.schedule_weekdays = _sql_reader.GetInt32(6);
            PromotionData.schedule1_time_from = _sql_reader.GetInt32(7);
            PromotionData.schedule1_time_to = _sql_reader.GetInt32(8);
            PromotionData.schedule2_enabled = _sql_reader.GetBoolean(9);
            PromotionData.schedule2_time_from = _sql_reader.IsDBNull(10) ? 0 : _sql_reader.GetInt32(10);
            PromotionData.schedule2_time_to = _sql_reader.IsDBNull(11) ? 0 : _sql_reader.GetInt32(11);
            PromotionData.gender_filter = (PROMOTION_GENDER_FILTER)_sql_reader.GetInt32(12);
            // FBA 08-OCT-2013 Added Decode to implement new birthday_filter values in the same field
            PromotionData.birthday_filter = (PROMOTION_BIRTHDAY_FILTER)_sql_reader.GetInt32(13);
            if (PromotionData.birthday_filter.ToString().Length > 1)
            {
              DecodeBirthdayFilter(_sql_reader.GetInt32(13), out PromotionData.age_filter, out PromotionData.birthday_filter, out PromotionData.age_from, out PromotionData.age_to);
            }
            // AVZ 01-OCT-2015, account creation date filter
            PromotionData.created_account_filter = (PROMOTION_CREATED_ACCOUNT_FILTER)_sql_reader.GetInt32(51);
            if (PromotionData.created_account_filter.ToString().Length > 1)
            {
              DecodeCreatedAccountFilter(_sql_reader.GetInt32(51), out PromotionData.created_account_filter, out PromotionData.working_days_included, out PromotionData.anniversary_year_from, out PromotionData.anniversary_year_to);
            }

            PromotionData.won_lock_enabled = !_sql_reader.IsDBNull(14);
            PromotionData.won_lock = 0;
            if (PromotionData.won_lock_enabled)
            {
              PromotionData.won_lock = _sql_reader.GetDecimal(14);
            }
            PromotionData.min_cash_in = _sql_reader.GetDecimal(15);
            PromotionData.min_cash_in_reward = _sql_reader.GetDecimal(16);
            PromotionData.cash_in = _sql_reader.GetDecimal(17);
            PromotionData.cash_in_reward = _sql_reader.GetDecimal(18);
            PromotionData.num_tokens = _sql_reader.GetInt32(19);
            PromotionData.token_reward = _sql_reader.GetDecimal(20);
            PromotionData.token_name = _sql_reader.IsDBNull(21) ? "" : _sql_reader.GetString(21);
            PromotionData.daily_limit = _sql_reader.IsDBNull(22) ? 0 : _sql_reader.GetDecimal(22);
            PromotionData.monthly_limit = _sql_reader.IsDBNull(23) ? 0 : _sql_reader.GetDecimal(23);
            PromotionData.global_daily_limit = _sql_reader.IsDBNull(24) ? 0 : _sql_reader.GetDecimal(24);
            PromotionData.global_monthly_limit = _sql_reader.IsDBNull(25) ? 0 : _sql_reader.GetDecimal(25);
            PromotionData.level_filter = _sql_reader.GetInt32(26);
            PromotionData.special_permission = (PROMOTION_SPECIAL_PERMISSION)_sql_reader.GetInt32(27);
            PromotionData.frequency_filter.last_days = _sql_reader.GetInt32(28);
            PromotionData.frequency_filter.min_days = _sql_reader.GetInt32(29);
            PromotionData.frequency_filter.min_cash_in = _sql_reader.GetDecimal(30);

            // AJQ 16-NOV-2010, "Creditos Agotados"
            PromotionData.min_spent = _sql_reader.GetDecimal(31);
            PromotionData.min_spent_reward = _sql_reader.GetDecimal(32);
            PromotionData.spent = _sql_reader.GetDecimal(33);
            PromotionData.spent_reward = _sql_reader.GetDecimal(34);

            _xml = _sql_reader.IsDBNull(35) ? null : _sql_reader.GetString(35);
            PromotionData.provider_list = new ProviderList();
            PromotionData.provider_list.FromXml(_xml);

            _xml = _sql_reader.IsDBNull(36) ? null : _sql_reader.GetString(36);
            PromotionData.draw_offer_list = new DrawOfferList();
            PromotionData.draw_offer_list.FromXml(_xml);

            // JCM 08-JUN-2012 New Fields on TYPE_PROMOTION_DATA
            PromotionData.min_played = _sql_reader.GetDecimal(37);
            PromotionData.min_played_reward = _sql_reader.GetDecimal(38);
            PromotionData.played = _sql_reader.GetDecimal(39);
            PromotionData.played_reward = _sql_reader.GetDecimal(40);
            PromotionData.last_executed = _sql_reader.IsDBNull(41) ? DateTime.MinValue : _sql_reader.GetDateTime(41);
            PromotionData.next_execution = _sql_reader.IsDBNull(42) ? DateTime.MinValue : _sql_reader.GetDateTime(42);
            PromotionData.expiration_type = (EXPIRATION_TYPE)_sql_reader.GetInt32(43);
            PromotionData.expiration_value = _sql_reader.GetInt32(44);
            // DDM 30-JUL-2012: Read credit type of the Promotions table.
            PromotionData.credit_type = (ACCOUNT_PROMO_CREDIT_TYPE)_sql_reader.GetInt32(45);
            // AJQ 20-AUG-2012: Global limit for the promotion
            PromotionData.global_limit = _sql_reader.IsDBNull(46) ? 0 : _sql_reader.GetDecimal(46);
            // DHA 12-APR-2013: set value of 'Expiration limit' field
            PromotionData.expiration_limit = _sql_reader.IsDBNull(47) ? DateTime.MinValue : _sql_reader.GetDateTime(47);
            // AMF 05-SEP-2013: Terminal List
            //if (!_sql_reader.IsDBNull(53))
            //{
            //    _xml = _sql_reader.IsDBNull(53) ? null : _sql_reader.GetString(53);
            //    PromotionData.terminal_list = new TerminalList();
            //    PromotionData.terminal_list.FromXml(_xml);
            //}
            //else
            //{
            _xml = _sql_reader.IsDBNull(48) ? null : _sql_reader.GetString(48);
            PromotionData.terminal_list = new TerminalList();
            PromotionData.terminal_list.FromXml(_xml);
            //}
            //DDM 10-OCT-2013: Points to credits
            PromotionData.points_to_credits_id = _sql_reader.GetInt64(49);

            //DLL 11-APR-2014: is vip promotion
            PromotionData.is_vip = _sql_reader.GetBoolean(50);

            // 18-FEB-2016 AVZ
            PromotionData.apply_tax = _sql_reader.GetBoolean(52);

            _now = WGDB.Now;
            PromotionData.actual_promo_date = _now;
            PromotionData.PromoGameId = _sql_reader.IsDBNull(53) ? 0 : _sql_reader.GetInt64(53);  //TODO: Refactor, put const 
            PromotionData.PyramidalDist = _sql_reader.IsDBNull(54) ? String.Empty : _sql_reader.GetString(54);

            switch (PromotionData.type)
            {
              case PROMOTION_TYPE.MANUAL:
              case PROMOTION_TYPE.PER_POINTS_NOT_REDEEMABLE:
              case PROMOTION_TYPE.AUTOMATIC_PER_PRIZE:
              case PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON:
              case PROMOTION_TYPE.PER_POINTS_REDEEMABLE:
              case PROMOTION_TYPE.EXTERNAL_REDEEM:
              case PROMOTION_TYPE.EXTERNAL_NON_REDEEM:
              case PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_PRIZE_COUPON:

              // JBP 18-FEB-2014: Promotion Groups (ELP)
              case PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_01:
              case PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_02:
              case PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_03:
              case PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_04:
              case PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_05:
              case PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_06:
              case PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_07:
              case PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_08:
              case PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_09:

              // DDM & AJQ, 05-SEP-2013 CashDeskDraw
              case PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR:
              case PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_RE:
              case PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR:
              case PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE:
              case PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAYER_SAID_NO:
              case PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_SERVER_DOWN:
              case PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR2_IN_KIND:
              case PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR2_IN_KIND:
              case PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAY_SESSION_WINNER_NR_HIDDEN:

              case PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_WITH_CURRENCY_EXCHANGE:
              // JML 14-Dec-2015 
              case PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE_IN_KIND:
              // JMV & RCI 28-JAN-2016
              case PROMOTION_TYPE.AUTOMATIC_BUCKET_NR_CREDIT:
              case PROMOTION_TYPE.AUTOMATIC_BUCKET_RE_CREDIT:
              case PROMOTION_TYPE.AUTOMATIC_PRIZE_PAYOUT:
              case PROMOTION_TYPE.AUTOMATIC_PRIZE_PAYOUT_AND_RECHARGE:

                // DHA 12-APR-213: In case that 'expiration_limit' is not null, 'must_expire' is set with 'expiration_limit' value
                if (PromotionData.expiration_limit != DateTime.MinValue)
                {
                  PromotionData.must_expire = PromotionData.expiration_limit > PromotionData.date_finish ? PromotionData.expiration_limit : PromotionData.date_finish;
                }
                else
                {
                  PromotionData.must_expire = PromotionData.date_finish;
                }

                break;

              case PROMOTION_TYPE.PERIODIC_PER_LEVEL:
              case PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED:
              case PROMOTION_TYPE.PERIODIC_PER_BIRTHDAY:
                if (PromotionData.next_execution == DateTime.MinValue)
                {
                  PromotionData.actual_promo_date = ScheduleDay.GetScheduleDate(_now, PromotionData.schedule_weekdays);
                }
                else
                {
                  PromotionData.actual_promo_date = PromotionData.next_execution;
                }

                PromotionData.must_expire = DateTime.MaxValue;
                break;

              case PROMOTION_TYPE.PREASSIGNED:
                PromotionData.must_expire = DateTime.MaxValue;
                break;

              case PROMOTION_TYPE.UNKNOWN:
              default:
                Log.Warning("ReadPromotionData: Unknown promo type: " + PromotionData.type.ToString() + ", PromoId: " + PromotionId + ".");

                return false;
            }

          }

        }

        if (!ReadPromotionFlags(PromotionData.id, SqlTrx, out PromotionData.flags_awarded, out PromotionData.flags_required))
        {
          Log.Warning("ReadPromotionData: Error ocurred trying to get Flags dependencies from PromotionId " + PromotionData.id);

          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // ReadPromotionData





    //------------------------------------------------------------------------------
    // PURPOSE: Obtain ExpirationType and ExpirationValue from a PromotionId.
    // 
    //  PARAMS:
    //      - INPUT:
    //        - SQLTransaction
    //        - PromotionId
    //
    //      - OUTPUT:
    //        - ExpirationType
    //        - ExpirationValue
    //
    // RETURNS:
    //    - true: Successful 
    //    - false: Not successful
    // 
    //   NOTES:
    //
    private static Boolean ReadExpirationData(SqlTransaction SQLTransaction, Int64 PromotionId, out EXPIRATION_TYPE ExpirationType, out Int32 ExpirationValue)
    {
      SqlDataReader _sql_data_reader;
      SqlCommand _sql_command;
      String _sql_str;

      _sql_str = "SELECT PM_EXPIRATION_TYPE " +
                      ", PM_EXPIRATION_VALUE " +
                   "FROM PROMOTIONS " +
                  "WHERE PM_PROMOTION_ID = @pPromoId";

      ExpirationType = EXPIRATION_TYPE.EXPIRATION_TYPE_NOT_SET;
      ExpirationValue = 0;

      _sql_data_reader = null;

      try
      {
        _sql_command = new SqlCommand(_sql_str);
        _sql_command.Connection = SQLTransaction.Connection;
        _sql_command.Transaction = SQLTransaction;

        _sql_command.Parameters.Add("@pPromoId", SqlDbType.BigInt).Value = PromotionId;

        _sql_data_reader = _sql_command.ExecuteReader();

        if (_sql_data_reader.Read())
        {
          ExpirationType = (EXPIRATION_TYPE)_sql_data_reader.GetInt32(0);
          ExpirationValue = _sql_data_reader.GetInt32(1);

          return true;
        }
        else
        {
          Log.Error("GetExpirationDate. Promo not found: " + PromotionId + ".");

          return false;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return false;
      }
      finally
      {
        if (_sql_data_reader != null)
        {
          _sql_data_reader.Close();
        }
        _sql_command = null;
      }
    } // ReadExpirationData

    //------------------------------------------------------------------------------
    // PURPOSE: Obtain account relevant data from database
    // 
    //  PARAMS:
    //      - INPUT:
    //        - SQLTransaction
    //        - AccountId
    //
    //      - OUTPUT:
    //        - AccountData
    //
    // RETURNS:
    //    - true: Successful 
    //    - false: Not successful
    // 
    //   NOTES:
    //

    public static Boolean ReadAccountData(SqlTransaction Trx, Int64 AccountId, out TYPE_ACCOUNT_DATA AccountData)
    {
      StringBuilder _sb;

      AccountData = new TYPE_ACCOUNT_DATA();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   AC_USER_TYPE ");
        _sb.AppendLine("       , AC_HOLDER_LEVEL ");
        _sb.AppendLine("       , AC_BLOCKED ");
        _sb.AppendLine("       , AC_HOLDER_GENDER ");
        _sb.AppendLine("       , AC_HOLDER_BIRTH_DATE ");
        _sb.AppendLine("       , ISNULL (AC_CURRENT_TERMINAL_ID, 0)  ");
        _sb.AppendLine("       , AC_RE_BALANCE ");
        _sb.AppendLine("       , AC_PROMO_RE_BALANCE ");
        _sb.AppendLine("       , AC_PROMO_NR_BALANCE ");
        _sb.AppendLine("       , AC_TYPE ");
        _sb.AppendLine("       , ISNULL(AC_HOLDER_IS_VIP, 0) ");
        _sb.AppendLine("       , AC_CREATED ");
        _sb.AppendLine("  FROM   ACCOUNTS ");
        _sb.AppendLine(" WHERE   AC_ACCOUNT_ID = @AccountId");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = AccountId;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            if (!_sql_reader.Read())
            {
              Log.Error("ReadAccountData: Account Not Found. AccountId: " + AccountId.ToString());

              return false;
            }

            AccountData.id = AccountId;
            AccountData.user_type = _sql_reader.IsDBNull(0) ? ACCOUNT_TYPE.ACCOUNT_TYPE_ANONYMOUS : (ACCOUNT_TYPE)_sql_reader.GetInt32(0);
            AccountData.holder_level = _sql_reader.GetInt32(1);
            AccountData.blocked = _sql_reader.GetBoolean(2);
            // JRC 15-SEP-2015, account creation date filter
            AccountData.created = _sql_reader.GetDateTime(11);

            if (AccountData.user_type == ACCOUNT_TYPE.ACCOUNT_TYPE_ANONYMOUS || AccountData.holder_level == 0)
            {
              AccountData.user_type = ACCOUNT_TYPE.ACCOUNT_TYPE_ANONYMOUS;
              AccountData.holder_gender = ACCOUNT_HOLDER_GENDER.ACCOUNT_HOLDER_GENDER_NOT_SET;
              AccountData.holder_birth_date = DateTime.MinValue;
              AccountData.holder_level = 0;
            }
            else
            {
              AccountData.holder_gender = (ACCOUNT_HOLDER_GENDER)_sql_reader.GetInt32(3);

              if (!_sql_reader.IsDBNull(4))
              {
                AccountData.holder_birth_date = _sql_reader.GetDateTime(4);
              }
            }

            AccountData.terminal_id = _sql_reader.GetInt32(5);
            AccountData.is_logged_in = (AccountData.terminal_id > 0);
            if (_sql_reader.GetInt32(9) == (Int32)WSI.Common.AccountType.ACCOUNT_VIRTUAL_CASHIER ||
                _sql_reader.GetInt32(9) == (Int32)WSI.Common.AccountType.ACCOUNT_VIRTUAL_TERMINAL)
            {
              AccountData.is_virtual = true;
            }
            else
            {
              AccountData.is_virtual = false;
            }
            AccountData.balance = new MultiPromos.AccountBalance(_sql_reader.GetDecimal(6), _sql_reader.GetDecimal(7), _sql_reader.GetDecimal(8));

            AccountData.is_vip = _sql_reader.GetBoolean(10);
          }
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }


      return false;
    } // ReadAccountData

    //------------------------------------------------------------------------------
    // PURPOSE: Get Awarded flags from promotion
    // 
    //  PARAMS:
    //      - INPUT:
    //        - PromotionId
    //        - Transaction    
    //
    //      - OUTPUT:
    //        - Awarded Promotions
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public static Boolean ReadAwardedPromotionFlags(Int64 PromotionId, SqlTransaction Trx, out DataTable Awarded)
    {
      DataTable _dummy_required;

      return ReadPromotionFlags(PromotionId, Trx, false, out Awarded, out _dummy_required);
    } // ReadAwardedPromotionFlags

    //------------------------------------------------------------------------------
    // PURPOSE: Get flags ( Required and Awarded ) from promotion
    // 
    //  PARAMS:
    //      - INPUT:
    //        - PromotionId
    //        - Transaction    
    //
    //      - OUTPUT:
    //        - Awarded Promotions
    //        - Required Promotions
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public static Boolean ReadPromotionFlags(Int64 PromotionId, SqlTransaction Trx, out DataTable Awarded, out DataTable Required)
    {
      return ReadPromotionFlags(PromotionId, Trx, true, out Awarded, out Required);
    } // ReadPromotionFlags

    //------------------------------------------------------------------------------
    // PURPOSE: Get flags ( Required OR Awarded ) from promotion
    // 
    //  PARAMS:
    //      - INPUT:
    //        - PromotionId
    //        - Transaction
    //        - ReadRequired
    //
    //      - OUTPUT:
    //        - Awarded Promotions
    //        - Required Promotions
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public static Boolean ReadPromotionFlags(Int64 PromotionId, SqlTransaction Trx, Boolean ReadRequired, out DataTable Awarded, out DataTable Required)
    {
      StringBuilder _sb;
      DataSet _promotion_flags;

      _promotion_flags = new DataSet();
      Awarded = new DataTable();
      Required = new DataTable();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   PF_FLAG_ID                       ");
        _sb.AppendLine("       , PF_FLAG_COUNT                    ");
        _sb.AppendLine("  FROM   PROMOTION_FLAGS                  ");
        _sb.AppendLine("  WHERE  PF_PROMOTION_ID = @pPromotionId  ");
        _sb.AppendLine("    AND  PF_TYPE = @pAwarded              ");
        _sb.AppendLine("  ORDER  BY  PF_FLAG_ID                   "); // AWARDED

        if (ReadRequired)
        {
          _sb.AppendLine("SELECT   PF_FLAG_ID                       ");
          _sb.AppendLine("       , PF_FLAG_COUNT                    ");
          _sb.AppendLine("  FROM   PROMOTION_FLAGS                  ");
          _sb.AppendLine("  WHERE  PF_PROMOTION_ID = @pPromotionId  ");
          _sb.AppendLine("    AND  PF_TYPE = @pRequired             ");
          _sb.AppendLine("  ORDER  BY  PF_FLAG_ID                   "); // REQUIRED
        }


        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pPromotionId", SqlDbType.BigInt).Value = PromotionId;
          _sql_cmd.Parameters.Add("@pAwarded", SqlDbType.Int).Value = PROMOTION_FLAG_TYPE.AWARDED;
          _sql_cmd.Parameters.Add("@pRequired", SqlDbType.Int).Value = PROMOTION_FLAG_TYPE.REQUIRED;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(_promotion_flags);
          }

          Awarded = _promotion_flags.Tables[0];

          if (ReadRequired)
          {
            Required = _promotion_flags.Tables[1];
          }
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    } // ReadPromotionFlags


    // PURPOSE: Get Active Flags from Account
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Transaction
    //        - AccountId
    //
    //      - OUTPUT:
    //        - DataTable with FlagId , Number of flags
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public static Boolean GetActiveFlags(Int64 AccountId, SqlTransaction Trx, out DataTable AccountFlags)
    {

      StringBuilder _sb;
      AccountFlags = new DataTable();

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT   AF_FLAG_ID                                        ");
        _sb.AppendLine("       , COUNT(*)  AS AF_COUNT                             ");
        _sb.AppendLine("  FROM   ACCOUNT_FLAGS                                     ");
        _sb.AppendLine(" WHERE   AF_ACCOUNT_ID = @pAccountId                       ");
        _sb.AppendLine("   AND   AF_STATUS     = @pFlagStatusActive                ");
        _sb.AppendLine("   AND ( AF_VALID_FROM <= GETDATE() )                      ");
        _sb.AppendLine("   AND ( AF_VALID_TO IS NULL OR AF_VALID_TO > GETDATE() )  ");
        _sb.AppendLine(" GROUP BY AF_FLAG_ID                                       ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_cmd.Parameters.Add("@pFlagStatusActive", SqlDbType.Int).Value = ACCOUNT_FLAG_STATUS.ACTIVE;

          using (SqlDataReader _sql_dr = _sql_cmd.ExecuteReader())
          {
            AccountFlags.Load(_sql_dr);
          }
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Check if account has the required flags for a promo
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Transaction
    //        - AccountId
    //        - Promotion Data
    //
    //      - OUTPUT:
    //        - True if match the filter
    //
    // RETURNS:
    // 
    //   NOTES:
    //    TO DO : Require uncomment ReadPromotionFlags() call in ReadPromotion
    //
    public static Boolean CheckAccountFlagsPromoFilter(Int64 AccountId, DataTable Required, DataTable AccountFlags, SqlTransaction Trx)
    {
      DataRow[] _flag;
      Int32 _current_flags;
      Int32 _required_flags;

      _required_flags = 0;

      if (Required.Rows.Count == 0)
      {
        return true;
      }
      else
      {
        if (AccountFlags == null || AccountFlags.Rows.Count == 0)
      {
        return false;
      }
      }

      foreach (DataRow _required_flag in Required.Rows)
      {
        _flag = AccountFlags.Select("AF_FLAG_ID = " + _required_flag["PF_FLAG_ID"]);
        if (_flag.Length == 0)
        {
          return false; // Required flag not found!
        }

        _current_flags = (Int32)_flag[0][1];
        _required_flags = (Int32)_required_flag["PF_FLAG_COUNT"];

        if (_current_flags < _required_flags)
        {
          return false;
        }
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Save Promotion Flags
    //          Called by GUI to set relationship between Promotion and Flags
    //  PARAMS:
    //      - INPUT:
    //        - Transaction
    //        - PromotionId
    //        - DataTable with flags ( all flags 0 = Awarded , 1 = Required )
    //
    //      - OUTPUT:
    //        - True if match the filter
    //
    // RETURNS:
    // 
    //   NOTES:
    //

    //------------------------------------------------------------------------------
    // PURPOSE: Save Promotion Flags
    //          Called by GUI to set relationship between Promotion and Flags
    //  PARAMS:
    //      - INPUT:
    //        - Transaction
    //        - PromotionId
    //        - Required Flags
    //        - Awareded Flags
    //
    //      - OUTPUT:
    //        - True if match the filter
    //
    // RETURNS:
    // 
    //   NOTES:
    //

    public static Boolean SavePromotionFlags(Int64 PromotionId, DataTable Required, DataTable Awarded, SqlTransaction Trx)
    {

      DataTable _all_flags;
      StringBuilder _sb;
      Int32 _rows_affected;

      _all_flags = Required.Clone();
      _all_flags.Columns.Add(new DataColumn("PF_TYPE", typeof(PROMOTION_FLAG_TYPE)));
      _all_flags.PrimaryKey = new DataColumn[] { _all_flags.Columns["PF_FLAG_ID"], _all_flags.Columns["PF_TYPE"] };

      foreach (DataRow _row in Awarded.Rows)
      {
        _all_flags.Rows.Add(new Object[] { _row[0], _row[1], PROMOTION_FLAG_TYPE.AWARDED });
      }
      foreach (DataRow _row in Required.Rows)
      {
        _all_flags.Rows.Add(new Object[] { _row[0], _row[1], PROMOTION_FLAG_TYPE.REQUIRED });
      }


      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("DELETE FROM   PROMOTION_FLAGS                ");
        _sb.AppendLine("      WHERE   PF_PROMOTION_ID = @pPromotionId");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pPromotionId", SqlDbType.Int).Value = PromotionId;
          _sql_cmd.ExecuteNonQuery();
        }


        if (_all_flags.Rows.Count == 0)
        {
          return true;
        }


        _sb.Length = 0;

        _sb.AppendLine("INSERT INTO   PROMOTION_FLAGS                ");
        _sb.AppendLine("            ( PF_PROMOTION_ID                ");
        _sb.AppendLine("            , PF_TYPE                        ");
        _sb.AppendLine("            , PF_FLAG_ID                     ");
        _sb.AppendLine("            , PF_FLAG_COUNT                  ");
        _sb.AppendLine("            )                                ");
        _sb.AppendLine("     VALUES ( @pPromotionId                  ");
        _sb.AppendLine("          , @pPromotionType                  ");
        _sb.AppendLine("          , @pFlagId                         ");
        _sb.AppendLine("          , @pFlagCount                      ");
        _sb.AppendLine("          )                                  ");


        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pPromotionId", SqlDbType.Int).Value = PromotionId;
          _sql_cmd.Parameters.Add("@pPromotionType", SqlDbType.BigInt).SourceColumn = "PF_TYPE";
          _sql_cmd.Parameters.Add("@pFlagId", SqlDbType.BigInt).SourceColumn = "PF_FLAG_ID";
          _sql_cmd.Parameters.Add("@pFlagCount", SqlDbType.Int).SourceColumn = "PF_FLAG_COUNT";

          _sql_cmd.UpdatedRowSource = UpdateRowSource.OutputParameters;

          using (SqlDataAdapter _da = new SqlDataAdapter())
          {
            _da.InsertCommand = _sql_cmd;
            _rows_affected = _da.Update(_all_flags);

            if (_rows_affected == _all_flags.Rows.Count)
            {
              return true;
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Error(ex.Message);

      }

      return false;
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Awarded flags in Account.
    // 
    //  PARAMS:
    //      - INPUT:
    //        - AwardedFlags
    //        - AccountId
    //        - UserId
    //        - Trx
    //
    //      - OUTPUT:
    //        - NONE
    //
    // RETURNS:
    //        - True if inserted all account_flags. False Otherwise
    //   NOTES:
    //
    public static Boolean AddFlagsToAccount(DataTable AwardedFlags, Int64 AccountId, Int64 PromoId, Int64 UserId, SqlTransaction Trx)
    {
      DataTable _account_flags;

      try
      {
        if (AwardedFlags.Rows.Count == 0)
        {
          return true;
        }

        _account_flags = AccountFlag.CreateAccountFlagsTable();

        foreach (DataRow _awarded_flag in AwardedFlags.Rows)
        {
          for (Int32 _idx = 0; _idx < (Int32)_awarded_flag["PF_FLAG_COUNT"]; _idx++)
          {
            AccountFlag.AddFlagToAccount(AccountId, (Int64)_awarded_flag["PF_FLAG_ID"], ACCOUNT_FLAG_STATUS.ACTIVE, UserId, PromoId, _account_flags, false);
          }
        }

        return AccountFlag.InsertAccountFlags(_account_flags, Trx);

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Week day based on Closing time
    // 
    //  PARAMS:
    //      - INPUT:
    //        - WeekDay
    //        - Mask
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - true: provided weekday is included
    //    - false: provided weekday is not included
    // 
    //   NOTES:

    public static DayOfWeek GetWeekDay(DateTime ThisDate, Int32 ClosingTime)
    {
      DayOfWeek _weekday;
      int _hour;

      _weekday = ThisDate.DayOfWeek;
      _hour = ThisDate.Hour;

      if (_hour < ClosingTime)
      {
        _weekday = ThisDate.AddDays(-1).DayOfWeek;
      }

      return _weekday;
    } // GetWeekDay

    //------------------------------------------------------------------------------
    // PURPOSE: Checks whether provided account holder gender is allowed in the promotion
    // 
    //  PARAMS:
    //      - INPUT:
    //        - AcctGender
    //        - GenderMask
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - true: account holder gender is included
    //    - false: account holder gender is not included
    // 
    //   NOTES:
    //
    private static Boolean IsGenderAllowed(ACCOUNT_HOLDER_GENDER AcctGender, PROMOTION_GENDER_FILTER GenderMask)
    {
      Boolean _rc;

      _rc = false;

      switch (AcctGender)
      {
        case ACCOUNT_HOLDER_GENDER.ACCOUNT_HOLDER_GENDER_MALE:
          _rc = (GenderMask == PROMOTION_GENDER_FILTER.PROMOTION_GENDER_FILTER_NOT_SET) || (GenderMask == PROMOTION_GENDER_FILTER.PROMOTION_GENDER_FILTER_MEN);
          break;

        case ACCOUNT_HOLDER_GENDER.ACCOUNT_HOLDER_GENDER_FEMALE:
          _rc = (GenderMask == PROMOTION_GENDER_FILTER.PROMOTION_GENDER_FILTER_NOT_SET) || (GenderMask == PROMOTION_GENDER_FILTER.PROMOTION_GENDER_FILTER_WOMEN);
          break;

        case ACCOUNT_HOLDER_GENDER.ACCOUNT_HOLDER_GENDER_NOT_SET:
        default:
          _rc = false;
          break;
      }

      return _rc;
    } // IsGenderAllowed

    //------------------------------------------------------------------------------
    // PURPOSE: Checks whether the account holder qualifies for a birth date-based promotion
    // 
    //  PARAMS:
    //      - INPUT:
    //        - DateToTest: current date
    //        - BirthDate
    //        - BirthdayFilter: birthday filter set on the promotion
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - true: account holder qualifies for the promotion
    //    - false: account holder qualifies for the promotion
    // 
    //   NOTES:
    //    Special treatment for thos born on February 29th.
    //

    private static Boolean IsDateBirthdayCompliant(DateTime DateToTest, DateTime BirthDate, PROMOTION_BIRTHDAY_FILTER BirthdayFilter, PROMOTION_BIRTHDAY_FILTER AgeFilter, Int32 AgeFrom, Int32 AgeTo)
    {
      // AVZ 07-OCT-2015: set the correct working day
      DateToTest = Misc.Opening(DateToTest);

      Boolean _rc;
      Int32 _age;

      _rc = false;

      if (BirthDate == DateTime.MinValue)
      {
        // Date of birth not set
        _rc = false;
      }
      else
      {
        _age = WGDB.Now.Year - BirthDate.Year;
        if (BirthDate > WGDB.Now.AddYears(-_age)) _age--;

        switch (BirthdayFilter)
        {
          case PROMOTION_BIRTHDAY_FILTER.PROMOTION_BIRTHDAY_FILTER_NOT_SET:
            _rc = true;
            break;

          case PROMOTION_BIRTHDAY_FILTER.PROMOTION_BIRTHDAY_FILTER_DAY_ONLY:
            _rc = (DateToTest.Day == BirthDate.Day) && (DateToTest.Month == BirthDate.Month);
            // Special case: people born on 29-02 are allowed to play on 28-02 in non-leap years
            if (BirthDate.Day == 29 && BirthDate.Month == 2 && !DateTime.IsLeapYear(DateToTest.Year))
            {
              _rc = (DateToTest.Day == 28) && (DateToTest.Month == 2);
            }
            break;

          case PROMOTION_BIRTHDAY_FILTER.PROMOTION_BIRTHDAY_FILTER_WHOLE_MONTH:
            _rc = (DateToTest.Month == BirthDate.Month);
            break;

        }
        if (_rc)
        {
          switch (AgeFilter)
          {
            // FBA 08-OCT-2013 Added control of the new birthday_filter values 
            case PROMOTION_BIRTHDAY_FILTER.PROMOTION_BIRTHDAY_FILTER_AGE_INCLUDED:
              _rc = (_age >= AgeFrom && _age < AgeTo);
              break;

            case PROMOTION_BIRTHDAY_FILTER.PROMOTION_BIRTHDAY_FILTER_AGE_EXCLUDED:
              _rc = !(_age >= AgeFrom && _age < AgeTo);
              break;
          }
        }
      }

      return _rc;
    } // IsDateBirthdayCompliant



    // JRC 15-SEP-2015, account creation date filter
    // AVZ 01-OCT-2015
    //------------------------------------------------------------------------------
    // PURPOSE: Checks whether the account qualifies for a created account date-based promotion
    // 
    //  PARAMS:
    //      - INPUT:
    //        - DateToTest: current date
    //        - CreatedAccountDate: created account date
    //        - Filter: created account filter set on the promotion
    //        - DaysIncluded: offset days since account creation
    //        - AnniversaryYearFrom: anniversary year from
    //        - AnniversaryYearTo: anniversary year to
    //
    //      - OUTPUT: see Returns below
    //
    // RETURNS:
    //    - true: account qualifies for the promotion
    //    - false: account qualifies for the promotion
    // 

    private static Boolean IsCreatedAccountCompliant(DateTime DateToTest, DateTime CreatedAccountDate, PROMOTION_CREATED_ACCOUNT_FILTER Filter, Int32 DaysIncluded, Int32 AnniversaryYearFrom, Int32 AnniversaryYearTo)
    {
      Boolean _result = true;
      //Obtener la jornada en la que se cre� la cuenta
      DateTime _createdAccountWorkingDate = Misc.Opening(CreatedAccountDate);
      //Obtener la jornada de la fecha a testear
      DateTime _dateToTestWorkingDate = Misc.Opening(DateToTest);

      switch (Filter)
      {
        case PROMOTION_CREATED_ACCOUNT_FILTER.PROMOTION_CREATED_ACCOUNT_FILTER_WORKING_DAY_ONLY:
          _result = CreatedAccountFilterWorkingDayRangeCompare(_dateToTestWorkingDate, _createdAccountWorkingDate, 0);
          break;
        case PROMOTION_CREATED_ACCOUNT_FILTER.PROMOTION_CREATED_ACCOUNT_FILTER_WORKING_DAYS_INCLUDED:
          _result = CreatedAccountFilterWorkingDayRangeCompare(_dateToTestWorkingDate, _createdAccountWorkingDate, DaysIncluded);
          break;
        case PROMOTION_CREATED_ACCOUNT_FILTER.PROMOTION_CREATED_ACCOUNT_FILTER_ANNIVERSARY_DAY_ONLY:
          _result = CreatedAccountFilterAnniversaryRangeCompare(_dateToTestWorkingDate, _createdAccountWorkingDate, false, AnniversaryYearFrom, AnniversaryYearTo);
          break;
        case PROMOTION_CREATED_ACCOUNT_FILTER.PROMOTION_CREATED_ACCOUNT_FILTER_ANNIVERSARY_WHOLE_MONTH:
          _result = CreatedAccountFilterAnniversaryRangeCompare(_dateToTestWorkingDate, _createdAccountWorkingDate, true, AnniversaryYearFrom, AnniversaryYearTo);
          break;
      }
      return _result;
    } // IsCreatedAcountCompliant

    // JRC 15-SEP-2015, account creation date filter
    // AVZ 01-OCT-2015
    //------------------------------------------------------------------------------
    // PURPOSE: Checks the resulting working days range against now.
    // 
    //  PARAMS:
    //      - INPUT:
    //        - DateToTest: current date
    //        - StartDate: created account working date
    //        - DaysIncluded: offset days since account creation
    //
    //      - OUTPUT: returns true or false based on analysis
    //
    // RETURNS:
    //    - true: working days range contains now
    //    - false: working days range doesn't
    // 
    private static Boolean CreatedAccountFilterWorkingDayRangeCompare(DateTime DateToTest, DateTime StartDate, Int32 DaysIncluded)
    {
      Boolean _result = false;
      DateTime _endDate = StartDate.AddDays(DaysIncluded);

      //don't want to deal with the time part 
      DateToTest = DateToTest - DateToTest.TimeOfDay;
      StartDate = StartDate - StartDate.TimeOfDay;
      _endDate = _endDate - _endDate.TimeOfDay;

      _result = DateToTest >= StartDate && DateToTest <= _endDate;
      return _result;
    }

    // AVZ 01-OCT-2015, account creation date filter
    //------------------------------------------------------------------------------
    // PURPOSE: Checks the resulting anniversary range against now.
    // 
    //  PARAMS:
    //      - INPUT:
    //        - DateToTest: current date
    //        - StartDate: created account working date
    //        - AnniversaryWholeMonth: check the whole month or the working day only
    //        - AnniversaryYearFrom: anniversary year from
    //        - AnniversaryYearTo: anniversary year from
    //
    //      - OUTPUT: returns true or false based on analysis
    //
    // RETURNS:
    //    - true: range contains now
    //    - false: range doesn't
    // 
    private static Boolean CreatedAccountFilterAnniversaryRangeCompare(DateTime DateToTest, DateTime StartDate, bool AnniversaryWholeMonth, Int32 AnniversaryYearFrom, Int32 AnniversaryYearTo)
    {
      Boolean _result = false;
      bool _isInAnniversary = false;
      //don't want to deal with the time part 
      DateToTest = DateToTest - DateToTest.TimeOfDay;
      StartDate = StartDate - StartDate.TimeOfDay;

      //Check the anniversary (month or day only), if the year is the same it isn't an anniversary
      if (AnniversaryWholeMonth)
        _isInAnniversary = StartDate.Month == DateToTest.Month && StartDate.Year != DateToTest.Year;
      else
        _isInAnniversary = (StartDate.Day == DateToTest.Day && StartDate.Month == DateToTest.Month) && StartDate.Year != DateToTest.Year;

      _result = _isInAnniversary;

      //Check the anniversary years range (only if necessary)
      if( _isInAnniversary && AnniversaryYearFrom != 0)
      {
        int _yearFrom = StartDate.Year + AnniversaryYearFrom;
        int _yearTo = StartDate.Year + AnniversaryYearTo;
        int _yearToTest = DateToTest.Year;
        _result = _yearToTest >= _yearFrom && _yearToTest <= _yearTo;
      }

      return _result;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Checks whether the account holder qualifies for a frequency-based promotion
    // 
    //  PARAMS:
    //      - INPUT:
    //        - AccountData
    //        - FrequencyFilter
    //        - SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - true: account holder qualifies for the promotion
    //    - false: account holder qualifies for the promotion
    // 
    private static Boolean IsFrequencyAllowed(Int64 AccountId,
                                              PROMOTION_FREQUENCY_FILTER FrequencyFilter,
                                              SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int32 _sql_value;
      Object _obj;

      if (FrequencyFilter.last_days == 0)
      {
        // No frequency filter set.
        return true;
      }

      try
      {
        // RRB 02-AUG-2012: Change query, now use ACCOUNT_OPERATIONS
        _sb = new StringBuilder();
        /*
        _sb.AppendLine("SELECT CASE WHEN (COUNT(*) >= @pMinDays) THEN 1 ELSE 0 END ");
        _sb.AppendLine("FROM ");
        _sb.AppendLine("( ");
        _sb.AppendLine("  SELECT   SUM(AO_AMOUNT) DAYLY_CASHIN ");
        _sb.AppendLine("         , DATEDIFF (DAY, AO_DATETIME, @pTodayOpening) NDAYS ");
        _sb.AppendLine("    FROM   ACCOUNT_OPERATIONS with(index(IX_ao_code_date_account)) ");
        _sb.AppendLine("   WHERE   AO_CODE IN ( @pOpCashIn, @pOpCashInPromo, @pMBCashIn, @pMBCashInPromo, ");
        _sb.AppendLine("                        @pNACashIn, @pNACashInPromo ) ");
        _sb.AppendLine("     AND   AO_DATETIME   >= DATEADD (DAY, -@pLastDays, @pTodayOpening) ");
        _sb.AppendLine("     AND   AO_DATETIME   <  @pTodayOpening ");
        _sb.AppendLine("     AND   AO_ACCOUNT_ID =  @pAccountId ");
        _sb.AppendLine("  GROUP BY DATEDIFF (DAY, AO_DATETIME, @pTodayOpening) ");
        _sb.AppendLine(") as X ");
        _sb.AppendLine("WHERE DAYLY_CASHIN >= @pMinDailyCashIn ");
         * 
         * */

        // EAV 16-JUN-2018 change query, compute journey of operations and group by it
        _sb.AppendLine("SELECT CASE WHEN (COUNT(*) >= @pMinDays) THEN 1 ELSE 0 END ");
        _sb.AppendLine("FROM ");
        _sb.AppendLine("( ");
        _sb.AppendLine("  SELECT   SUM(AO_AMOUNT) DAYLY_CASHIN, ");
        _sb.AppendLine("   CASE WHEN (	AO_DATETIME < DATEADD(day, (DATEDIFF (day, AO_DATETIME,  @pTodayOpening) * -1),  @pTodayOpening))");
        _sb.AppendLine("   THEN");
        _sb.AppendLine("      DATEADD(day, -1, (DATEADD(day, (DATEDIFF (day, AO_DATETIME,  @pTodayOpening) * -1),  @pTodayOpening)))");
        _sb.AppendLine("   ELSE");
        _sb.AppendLine("      DATEADD(day, (DATEDIFF (day, AO_DATETIME,  @pTodayOpening) * -1),  @pTodayOpening)");
        _sb.AppendLine("   END AS NDAYS");
        _sb.AppendLine("   FROM   ACCOUNT_OPERATIONS with(index(IX_ao_code_date_account)) ");
        _sb.AppendLine("   WHERE   AO_CODE IN ( @pOpCashIn, @pOpCashInPromo, @pMBCashIn, @pMBCashInPromo, ");
        _sb.AppendLine("                        @pNACashIn, @pNACashInPromo ) ");
        _sb.AppendLine("     AND   AO_DATETIME   >= DATEADD (DAY, -@pLastDays, @pTodayOpening) ");
        _sb.AppendLine("     AND   AO_DATETIME   <  @pTodayOpening ");
        _sb.AppendLine("     AND   AO_ACCOUNT_ID =  @pAccountId ");
        _sb.AppendLine("  GROUP BY ");
        _sb.AppendLine("     CASE WHEN (AO_DATETIME < DATEADD(day, (DATEDIFF (day, AO_DATETIME,  @pTodayOpening) * -1),  @pTodayOpening))");
        _sb.AppendLine("     THEN");
        _sb.AppendLine("         DATEADD(day, -1, (DATEADD(day, (DATEDIFF (day, AO_DATETIME,  @pTodayOpening) * -1),  @pTodayOpening)))");
        _sb.AppendLine("     ELSE");
        _sb.AppendLine("         DATEADD(day, (DATEDIFF (day, AO_DATETIME,  @pTodayOpening) * -1),  @pTodayOpening)");
        _sb.AppendLine("     END");

        _sb.AppendLine(") as X ");
        _sb.AppendLine("WHERE DAYLY_CASHIN >= @pMinDailyCashIn ");


        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pMinDays", SqlDbType.Int).Value = FrequencyFilter.min_days;
          _cmd.Parameters.Add("@pLastDays", SqlDbType.Int).Value = FrequencyFilter.last_days;
          _cmd.Parameters.Add("@pMinDailyCashIn", SqlDbType.Money).Value = FrequencyFilter.min_cash_in.SqlMoney;
          _cmd.Parameters.Add("@pTodayOpening", SqlDbType.DateTime).Value = Misc.TodayOpening();

          _cmd.Parameters.Add("@pOpCashIn", SqlDbType.Int).Value = OperationCode.CASH_IN;
          _cmd.Parameters.Add("@pOpCashInPromo", SqlDbType.Int).Value = OperationCode.PROMOTION;
          _cmd.Parameters.Add("@pMBCashIn", SqlDbType.Int).Value = OperationCode.MB_CASH_IN;
          _cmd.Parameters.Add("@pMBCashInPromo", SqlDbType.Int).Value = OperationCode.MB_PROMOTION;
          _cmd.Parameters.Add("@pNACashIn", SqlDbType.Int).Value = OperationCode.NA_CASH_IN;
          _cmd.Parameters.Add("@pNACashInPromo", SqlDbType.Int).Value = OperationCode.NA_PROMOTION;

          _sql_value = 0;
          _obj = _cmd.ExecuteScalar();
          if (_obj != null && _obj != DBNull.Value)
          {
            _sql_value = (Int32)_obj;
          }

          return _sql_value == 1;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return false;
      }
    } // IsFrequencyAllowed

    //------------------------------------------------------------------------------
    // PURPOSE: Calculates the today spent  & Played per provider.
    // 
    //  PARAMS:
    //      - INPUT:
    //        - RemainingSpentPerProvider
    //        - ProviderList
    //
    //      - OUTPUT:
    //        - Spent
    //        -Played
    //
    // RETURNS:
    // 
    public static void CalculateDataByProvider(DataTable RemainingSpentPerProvider,
                                                ProviderList ProviderList,
                                                out Currency Spent,
                                                out Currency Played )
    {
      String _provider_id;
      Object _objSpent;
      Object _objPlayed;
      Spent = 0;
      Played = 0;

      if (ProviderList == null)
      {
        _objSpent = RemainingSpentPerProvider.Compute("SUM(SPENT_REMAINING)", "");
        if (_objSpent != null && _objSpent != DBNull.Value)
        {
          Spent = (Decimal)_objSpent;
        }
        _objPlayed = RemainingSpentPerProvider.Compute("SUM(PLAYED_AMOUNT)", "");
        if (_objPlayed != null && _objPlayed != DBNull.Value)
        {
          Played = (Decimal)_objPlayed;
        }

        return;
      }

      foreach (DataRow _provider in RemainingSpentPerProvider.Rows)
      {
        _provider_id = (String)_provider[COLUMN_PROVIDER_ID];
        if (!ProviderList.Contains(_provider_id.ToUpper()))
        {
          continue;
        }

        Spent += (Decimal)_provider[COLUMN_SPENT_REMAINING];
        Played += (Decimal)_provider[COLUMN_PLAYED_AMOUNT];
      }
    } // CalculateDataByProvider

    //------------------------------------------------------------------------------
    // PURPOSE: Calculates the today spent & played per terminal.
    // 
    //  PARAMS:
    //      - INPUT:
    //        - RemainingSpentPerTerminal
    //        - TerminalList
    //
    //      - OUTPUT:
    //        - Spent
    //       -Played
    //
    // RETURNS:
    // 
    //SDS 28-10-2015
    public static void CaculateDataByTerminal(DataTable RemainingSpentPerTerminal,
                                                TerminalList TerminalList,
                                                Int64 PromotionId,
                                                out Currency Spent,
                                                out Currency Played,
                                                SqlTransaction Trx)
    {
      String _terminal_id;
      Object _objSpent;
      Object _objPlayed;
      DataTable  _dt_terminal;
      SqlDataReader _dr_terminal;
      StringBuilder _sb;

      Spent = 0;
      Played = 0;
      if (TerminalList.ListType == TerminalList.TerminalListType.All)
      {
        _objSpent = RemainingSpentPerTerminal.Compute("SUM(SPENT_REMAINING)", "");
        if (_objSpent != null && _objSpent != DBNull.Value)
        {
          Spent = (Decimal)_objSpent;
        }

        _objPlayed = RemainingSpentPerTerminal.Compute("SUM(PLAYED_AMOUNT)", "");
        if (_objPlayed != null && _objPlayed != DBNull.Value)
        {
          Played = (Decimal)_objPlayed;
        }

        return;
      }

      _sb = new StringBuilder();
      _sb.AppendLine("SELECT  TG_ELEMENT_TYPE ");
      _sb.AppendLine("  FROM   TERMINAL_GROUPS ");
      _sb.AppendLine(" WHERE   TG_TERMINAL_ID = @TerminalId ");
      _sb.AppendLine("   AND   TG_ELEMENT_ID = @PromotionId ");
      _sb.AppendLine("   AND   TG_ELEMENT_TYPE IN (@ElementType1, @ElementType2)");


      try
      {
        foreach (DataRow _terminal in RemainingSpentPerTerminal.Rows)
        {
          _terminal_id = (String)_terminal[COLUMN_TERMINAL_ID];

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@TerminalId", SqlDbType.Int).Value = _terminal_id;
            _cmd.Parameters.Add("@PromotionId", SqlDbType.BigInt).Value = PromotionId;
            _cmd.Parameters.Add("@ElementType1", SqlDbType.Int).Value = EXPLOIT_ELEMENT_TYPE.PROMOTION;
            _cmd.Parameters.Add("@ElementType2", SqlDbType.Int).Value = EXPLOIT_ELEMENT_TYPE.PROMOTION_PLAYED;
            _dr_terminal = _cmd.ExecuteReader();
            _dt_terminal= new DataTable();
            _dt_terminal.Load(_dr_terminal);
          }

          if (_dt_terminal.Rows.Count  == 0)
          {
            continue;
          }

          foreach (DataRow row in _dt_terminal.Rows)
          {
            if (Convert.ToInt32(row[0]) == (int)EXPLOIT_ELEMENT_TYPE.PROMOTION)
              Spent += (Decimal)_terminal[COLUMN_SPENT_REMAINING];
            else
              Played += (Decimal)_terminal[COLUMN_PLAYED_AMOUNT];
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    } // CaculateDataByTerminal



    //------------------------------------------------------------------------------
    // PURPOSE: Calculates the reward corresponding to specified CashIn and NumTokens amounts
    //          using the provided promotion
    // 
    //  PARAMS:
    //      - INPUT:
    //        - CashIn
    //        - NumTokens
    //        - PromotionData
    //
    //      - OUTPUT:
    //        - Reward: Non-redeemable amount awarded
    //
    // RETURNS:
    // 
    //   NOTES:
    //

    private static Boolean CalculateReward(ref Currency CashIn, ref Int32 NumTokens, ref TYPE_PROMOTION_DATA PromotionData, out Currency Reward, out Currency WonLock, Int32 QuantityForMinCashIn)
    {
      Int32 _num_times;
      Currency _reward;

      _reward = 0;
      Reward = 0;
      WonLock = 0;

      if (CashIn < PromotionData.min_cash_in)
      {
        CashIn = PromotionData.min_cash_in;

        return false;
      }

      // Check minimum cash in reward
      if (PromotionData.min_cash_in_reward > 0)
      {
        if (CashIn >= PromotionData.min_cash_in)
        {
          _reward += PromotionData.min_cash_in_reward * QuantityForMinCashIn;
        }
      }

      // Check additional cash in reward
      if (PromotionData.cash_in_reward > 0 && PromotionData.cash_in > 0)
      {
        if (CashIn >= PromotionData.cash_in && CashIn >= PromotionData.min_cash_in)
        {
          // Integer calculation
          _num_times = (Int32)(CashIn / PromotionData.cash_in);
          _reward += PromotionData.cash_in_reward * _num_times;
        }
      }

      // Check token reward
      if (PromotionData.token_reward > 0 && PromotionData.num_tokens > 0)
      {
        if (NumTokens >= PromotionData.num_tokens && CashIn >= PromotionData.min_cash_in)
        {
          // Integer calculation
          _num_times = NumTokens / PromotionData.num_tokens;
          _reward += PromotionData.token_reward * _num_times;
        }
      }

      Reward = _reward;
      WonLock = 0;
      if (PromotionData.won_lock_enabled)
      {
        if (PromotionData.won_lock == 0)
        {
          WonLock = 0.0001m;
        }
        else if (PromotionData.won_lock < 0)
        {
          // TODO: Deprecated. There is a field that contains the credit type.
          WonLock = -0.0001m; // Old form of credit is not redeemable.
        }
        else
        {
          WonLock = PromotionData.won_lock * _reward;
        }
      }

      return true;
    } // CalculateReward


    private static Boolean RetrievePreassignedReward(Int64 PromoId, Int64 AccounId, out Currency Reward, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _reward;

      Reward = 0;
      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("  SELECT   TOP 1 ACP_BALANCE ");
        _sb.AppendLine("    FROM   ACCOUNT_PROMOTIONS ");
        _sb.AppendLine("   WHERE   ACP_PROMO_ID   = @pPromoId ");
        _sb.AppendLine("     AND   ACP_STATUS     = @pStatusPreassigned ");
        _sb.AppendLine("     AND   ACP_ACCOUNT_ID = @pAccountId ");
        _sb.AppendLine("     AND   GETDATE() < ISNULL(ACP_EXPIRATION,GETDATE()) ");
        _sb.AppendLine("ORDER BY   ACP_UNIQUE_ID ASC ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPromoId", SqlDbType.BigInt).Value = PromoId;
          _cmd.Parameters.Add("@pStatusPreassigned", SqlDbType.Int).Value = (Int32)ACCOUNT_PROMO_STATUS.PREASSIGNED;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccounId;

          _reward = _cmd.ExecuteScalar();

          if (_reward == null)
          {
            return false;
          }

          if (_reward == DBNull.Value)
          {
            return false;
          }

          Reward = (Decimal)_reward;

        }

        return true;
      }
      catch
      { ; }

      return false;
    } // RetrievePreassignedReward

    //------------------------------------------------------------------------------
    // PURPOSE: Tries to calculate alternate Cash In and Num Tokens values so that 
    //          the promotion reward does not exceed the promotion daily or monthly limits.
    // 
    //  PARAMS:
    //      - INPUT:
    //        - RemainingLimit
    //        - CashIn : original cash in amount 
    //        - NumTokens : original number of tokens user is delivering
    //        - PromotionData
    //
    //      - OUTPUT:
    //        - CashIn : modified cash in amount
    //        - NumTokens : modified number of tokens
    //        - NewReward : reward calculated using new cash in and num tokens values
    //
    // RETURNS:
    //    - True: amounts could be adjusted
    //    - False: amounts could not be adjusted
    //  
    //   NOTES:
    //
    private static Boolean AdjustAmountToLimit(Currency RemainingLimit, ref Currency CashIn, ref Int32 NumTokens, ref TYPE_PROMOTION_DATA PromotionData, out Currency NewReward)
    {
      Int32 _num_times;
      Int32 _aux_num_tokens;
      Currency _aux_reward;
      Currency _aux_cash_in;

      NewReward = 0;

      if (PromotionData.min_cash_in_reward > RemainingLimit)
      {
        return false;
      }

      _aux_reward = PromotionData.min_cash_in_reward;
      _aux_cash_in = PromotionData.min_cash_in;
      _aux_num_tokens = 0;

      // Try to reduce reward from tokens
      if (PromotionData.token_reward > 0 && NumTokens > 0)
      {
        _num_times = (Int32)((RemainingLimit - _aux_reward) / PromotionData.token_reward);
        _aux_reward += _num_times * PromotionData.token_reward;
        _aux_num_tokens = _num_times * PromotionData.num_tokens;
      }

      // Try to reduce reward from cash in
      if (PromotionData.cash_in_reward > 0)
      {
        _num_times = (Int32)((RemainingLimit - _aux_reward) / PromotionData.cash_in_reward);
        _aux_reward += _num_times * PromotionData.cash_in_reward;
        _aux_cash_in += _num_times * PromotionData.cash_in;
      }

      if (_aux_reward <= RemainingLimit)
      {
        // Adjusted incoming values to available limit, return new values
        NewReward = _aux_reward;
        CashIn = _aux_cash_in;
        NumTokens = _aux_num_tokens;

        return true;
      }
      else
      {
        // No way to adjust incoming values
        return false;
      }
    } // AdjustAmountToLimit

    //------------------------------------------------------------------------------
    // PURPOSE: Decode birthday filter and gets birthdafilter, AgeFrom and AgeTo
    // 
    //  PARAMS:
    //      - INPUT:
    //        - SQLBirthdayFilter
    //        
    //      - OUTPUT:
    //        - BirthdayFilter
    //        - AgeFrom
    //        - AgeTo
    //
    // RETURNS:
    //  
    //   NOTES:
    //
    public static void DecodeBirthdayFilter(Int32 SQLBirthdayFilter, out PROMOTION_BIRTHDAY_FILTER AgeFilter,
                                            out PROMOTION_BIRTHDAY_FILTER BirthdayFilter, out Int32 AgeFrom, out Int32 AgeTo)
    {
      AgeTo = 0;
      AgeFrom = 0;
      AgeFilter = PROMOTION_BIRTHDAY_FILTER.PROMOTION_BIRTHDAY_FILTER_NOT_SET;
      BirthdayFilter = (PROMOTION_BIRTHDAY_FILTER)SQLBirthdayFilter;
      if (SQLBirthdayFilter.ToString().Length > 1)
      {
        int _birthday_value;
        byte[] _bytes;

        _bytes = BitConverter.GetBytes(SQLBirthdayFilter);

        Int32.TryParse(_bytes[_bytes.Length - _bytes.Length].ToString(), out  _birthday_value);
        AgeFilter = (PROMOTION_BIRTHDAY_FILTER)_birthday_value;
        Int32.TryParse(_bytes[_bytes.Length - 1].ToString(), out  _birthday_value);
        BirthdayFilter = (PROMOTION_BIRTHDAY_FILTER)_birthday_value;
        Int32.TryParse(_bytes[_bytes.Length - 3].ToString(), out AgeFrom);
        Int32.TryParse(_bytes[_bytes.Length - 2].ToString(), out AgeTo);
      }
    }//DecodeBirthdayFilter

    //------------------------------------------------------------------------------
    // PURPOSE: Encode birthday filter from enum and age range
    // 
    //  PARAMS:
    //      - INPUT:
    //        - BirthdayFilter enum
    //        - AgeFrom
    //        - AgeTo
    //        
    //      - OUTPUT:
    //
    // RETURNS:
    //  
    //   NOTES:
    //
    public static Int32 EncodeBirthdayFilter(PROMOTION_BIRTHDAY_FILTER BirthdayFilter, PROMOTION_BIRTHDAY_FILTER BirthdayFilterByAge, Int32 AgeFrom, Int32 AgeTo)
    {
      byte[] _bytes;
      int _i;

      _bytes = new byte[] { (byte)BirthdayFilterByAge, (byte)AgeFrom, (byte)AgeTo, (byte)BirthdayFilter };
      _i = BitConverter.ToInt32(_bytes, 0);

      return _i;
    }//EncodeBirthdayFilter

    // JRC 15-SEP-2015, account creation date filter
    // AVZ 01-OCT-2015, new requirement
    //------------------------------------------------------------------------------
    // PURPOSE: Decode Account Creation filter and gets Creation account filter mode , working days included, and anniversary ranges (from-to)
    // 
    //  PARAMS:
    //      - INPUT:
    //        - SQLCreatedAccountFilter
    //        
    //      - OUTPUT:
    //        - CreatedAccountFilter
    //        - WorkingDaysIncluded
    //        - AnniversaryYearFrom
    //        - AnniversaryYearTo
    //
    // RETURNS:
    //  
    //   NOTES:
    //
    public static void DecodeCreatedAccountFilter(Int32 SQLCreatedAccountFilter, out PROMOTION_CREATED_ACCOUNT_FILTER CreatedAccountFilter,
                                            out Int32 WorkingDaysIncluded, out Int32 AnniversaryYearFrom, out Int32 AnniversaryYearTo)
    {
      WorkingDaysIncluded = 0;
      AnniversaryYearFrom = 0;
      AnniversaryYearTo = 0;
      CreatedAccountFilter = (PROMOTION_CREATED_ACCOUNT_FILTER)SQLCreatedAccountFilter;
      if (SQLCreatedAccountFilter.ToString().Length > 1)
      {
        int _filter_value;
        byte[] _bytes;

        _bytes = BitConverter.GetBytes(SQLCreatedAccountFilter);

        Int32.TryParse(_bytes[_bytes.Length - _bytes.Length].ToString(), out  _filter_value);
        CreatedAccountFilter = (PROMOTION_CREATED_ACCOUNT_FILTER)_filter_value;

        Int32.TryParse(_bytes[_bytes.Length - 3].ToString(), out WorkingDaysIncluded);
        Int32.TryParse(_bytes[_bytes.Length - 2].ToString(), out AnniversaryYearFrom);
        Int32.TryParse(_bytes[_bytes.Length - 1].ToString(), out AnniversaryYearTo);
      }
    }//DecodeCreatedAccountFilter

    // JRC 15-SEP-2015, account creation date filter
    // AVZ 01-OCT-2015, new requirement
    //------------------------------------------------------------------------------
    // PURPOSE: Encode Created Account filter from enum , working days included, and anniversary ranges (from-to)
    //
    //  PARAMS:
    //      - INPUT:
    //        - CreatedAccountFilter enum
    //        - WorkingDaysIncluded
    //        - AnniversaryYearFrom
    //        - AnniversaryYearTo
    //        
    //      - OUTPUT:
    //			
    // RETURNS: Int32
    //  
    //   NOTES:
    //
    public static Int32 EncodeCreatedAccountFilter(PROMOTION_CREATED_ACCOUNT_FILTER CreatedAccountFilter, Int32 WorkingDaysIncluded, Int32 AnniversaryYearFrom, Int32 AnniversaryYearTo)
    {
      byte[] _bytes;
      int _i;
      //byte[0] = radio button selection 0-1-2-3-4
      //byte[1] = working days included
      //byte[2] = anniversary ranges - from
      //byte[3] = anniversary ranges - to


      _bytes = new byte[] { (byte)CreatedAccountFilter, (byte)WorkingDaysIncluded, (byte)AnniversaryYearFrom, (byte)AnniversaryYearTo };
      _i = BitConverter.ToInt32(_bytes, 0);

      return _i;
    }//EncodeCreatedAccountFilter




    //------------------------------------------------------------------------------
    // PURPOSE: Make a DataTable with "providers id" and the accumulate of "spent remaining" columns
    // 
    //  PARAMS:
    //      - INPUT:
    //        - AccountId : Account id 
    //        - Trx : Transaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - DataTable: 
    //  
    //   NOTES:
    //
    public static DataTable AccountRemainingDataPerProvider(Int64 AccountId, SqlTransaction Trx)
    {
      DataTable _dt_spent_provider;

      SqlCommand _sql_cmd_select;
      SqlDataAdapter _da;
      String _sql_select;
      Int32 _hours_before;
      DateTime _lower_limit_datetime;
      String _national_currency;
      List<CurrencyExchange> _currency_exchange_list;
      CurrencyExchange _currency_exchange_auxiliar;
      CurrencyExchange _floor_dual_currency;


      _floor_dual_currency = new CurrencyExchange();
      _national_currency = String.Empty;

      _dt_spent_provider = new DataTable("SPENT_PER_PROVIDER");

      _dt_spent_provider.Columns.Add("PROVIDER_ID", Type.GetType("System.String"));       // COLUMN_PROVIDER_ID
      _dt_spent_provider.Columns.Add("SPENT_REMAINING", Type.GetType("System.Decimal"));  // COLUMN_SPENT_REMAINING
      _dt_spent_provider.Columns.Add("PLAYED_AMOUNT", Type.GetType("System.Decimal"));  // COLUMN_PLAYED_AMOUNT

      try
      {

        if (!Int32.TryParse(Misc.ReadGeneralParams("Cashier", "Promotions.HoursBeforeTodayOpening", Trx), out _hours_before))
          _hours_before = 0;

        _lower_limit_datetime = Misc.TodayOpening().AddHours(-_hours_before);

        // RCI 05-DEC-2012: Explicit use of the index IX_ps_account_id_finished.
        // SDS 19-10-2015: new Promotion by Played. Backlog Item 3856

        if (!WSI.Common.Misc.IsFloorDualCurrencyEnabled())
        {
          _sql_select = " SELECT   TE_PROVIDER_ID AS PROVIDER_ID" +
                        "  	     , SUM(PS_SPENT_REMAINING) AS SPENT_REMAINING" +
                        "  	     , SUM(PS_PLAYED_AMOUNT) AS PLAYED_AMOUNT" +
                        "   FROM   PLAY_SESSIONS WITH(INDEX(IX_ps_account_id_finished))" +
                        "  INNER   JOIN TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID" +
                        " WHERE    PS_ACCOUNT_ID  = @pAccountId " +
                        "    AND   PS_STATUS     <> 0 " +
                        "    AND   PS_FINISHED   >= @pLowerLimitDatetime " +
                        "    AND   PS_SPENT_REMAINING > 0 " +
                        " GROUP BY TE_PROVIDER_ID " +
                        " ORDER BY TE_PROVIDER_ID ";
        }
        else
        {

          _national_currency = CurrencyExchange.GetNationalCurrency();

          if (!CurrencyExchange.GetAllowedCurrencies(true, out _currency_exchange_auxiliar, out _currency_exchange_list))
          {
            return null;
          }

          foreach (CurrencyExchange _currency in _currency_exchange_list)
          {
            if (_currency.CurrencyCode != _national_currency)
            {
              _floor_dual_currency = _currency;
            }
          }

          if (_floor_dual_currency == null)
          {
            return null;
          }


          _sql_select = "  SELECT   TE_PROVIDER_ID       AS PROVIDER_ID                         " +
                        "         , SUM(SPENT_REMAINING) AS SPENT_REMAINING                     " +
                        "         , SUM(PLAYED_AMOUNT)   AS PLAYED_AMOUNT                       " +
                        "    FROM (                                                             " +
                        "  SELECT  TE_PROVIDER_ID AS TE_PROVIDER_ID                             " +
                        "        , CASE WHEN TE_ISO_CODE = @pCurrencyIsoCode                    " +
                        "  	        THEN ISNULL (SUM(PS_SPENT_REMAINING),0)                     " +
                        "           ELSE ISNULL (SUM(PS_SPENT_REMAINING) * @pExchangeValue , 0) " +
                        "           END SPENT_REMAINING                                         " +
                        "        , CASE WHEN TE_ISO_CODE = @pCurrencyIsoCode                    " +
                        "  	        THEN ISNULL (SUM(PS_PLAYED_AMOUNT),0)                       " +
                        "           ELSE ISNULL (SUM(PS_PLAYED_AMOUNT) * @pExchangeValue , 0)   " +
                        "          END PLAYED_AMOUNT                                            " +
                        "        , TE_ISO_CODE                                                  " +
                        "   FROM   PLAY_SESSIONS WITH(INDEX(IX_ps_account_id_finished))         " +
                        "  INNER   JOIN TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID            " +
                        " WHERE    PS_ACCOUNT_ID  = @pAccountId                                 " +
                        "    AND   PS_STATUS     <> 0                                           " +
                        "    AND   PS_FINISHED   >= @pLowerLimitDatetime                        " +
                        "    AND   PS_SPENT_REMAINING > 0                                       " +
                        " GROUP BY TE_PROVIDER_ID, TE_ISO_CODE                                  " +
                        "         ) AS X                                                        " +
                        "            GROUP BY  X.TE_PROVIDER_ID                                 " +
                        "            ORDER BY  X.TE_PROVIDER_ID                                 ";

        }

        _sql_cmd_select = new SqlCommand(_sql_select);
        _sql_cmd_select.Parameters.Add("@pLowerLimitDatetime", SqlDbType.DateTime).Value = _lower_limit_datetime;
        _sql_cmd_select.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

        if (WSI.Common.Misc.IsFloorDualCurrencyEnabled())
        {
          _sql_cmd_select.Parameters.Add("@pExchangeValue", SqlDbType.Decimal).Value = _floor_dual_currency.ChangeRate;
          _sql_cmd_select.Parameters.Add("@pCurrencyIsoCode", SqlDbType.NVarChar).Value = _national_currency;
        }


        _da = new SqlDataAdapter();
        _da.SelectCommand = _sql_cmd_select;
        _da.SelectCommand.Connection = Trx.Connection;
        _da.SelectCommand.Transaction = Trx;

        _da.Fill(_dt_spent_provider);

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
      }

      return _dt_spent_provider;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Make a DataTable with "terminal id" and the accumulate of "spent remaining" columns
    // 
    //  PARAMS:
    //      - INPUT:
    //        - AccountId : Account id 
    //        - Trx : Transaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - DataTable: 
    //  
    //   NOTES:
    //
    public static DataTable AccountRemainingDataPerTerminal(Int64 AccountId, SqlTransaction Trx)
    {
      DataTable _dt_spent_provider;

      SqlCommand _sql_cmd_select;
      SqlDataAdapter _da;
      String _sql_select;
      Int32 _hours_before;
      DateTime _lower_limit_datetime;
      String _national_currency;
      List<CurrencyExchange> _currency_exchange_list;
      CurrencyExchange _currency_exchange_auxiliar;
      CurrencyExchange _floor_dual_currency;



      _national_currency = String.Empty;
      _currency_exchange_list = null;
      _floor_dual_currency = null;
      _currency_exchange_auxiliar = null;

      _dt_spent_provider = new DataTable("SPENT_PER_TERMINAL");

      _dt_spent_provider.Columns.Add("TERMINAL_ID", Type.GetType("System.String"));       // COLUMN_TERMINAL_ID
      _dt_spent_provider.Columns.Add("SPENT_REMAINING", Type.GetType("System.Decimal"));  // COLUMN_SPENT_REMAINING
      _dt_spent_provider.Columns.Add("PLAYED_AMOUNT", Type.GetType("System.Decimal"));  // COLUMN_PLAYED_AMOUNT
      try
      {

        if (!Int32.TryParse(Misc.ReadGeneralParams("Cashier", "Promotions.HoursBeforeTodayOpening", Trx), out _hours_before))
          _hours_before = 0;

        _lower_limit_datetime = Misc.TodayOpening().AddHours(-_hours_before);



        if (!WSI.Common.Misc.IsFloorDualCurrencyEnabled())
        {
          // RCI 05-DEC-2012: Explicit use of the index IX_ps_account_id_finished.
          _sql_select = " SELECT   TE_TERMINAL_ID AS TERMINAL_ID" +
                        "  	     , SUM(PS_SPENT_REMAINING) AS SPENT_REMAINING" +
                        "  	     , SUM(PS_PLAYED_AMOUNT) AS PLAYED_AMOUNT" +
                        "   FROM   PLAY_SESSIONS WITH(INDEX(IX_ps_account_id_finished))" +
                        "  INNER   JOIN TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID" +
                        " WHERE    PS_ACCOUNT_ID  = @pAccountId " +
                        "    AND   PS_STATUS     <> 0 " +
                        "    AND   PS_FINISHED   >= @pLowerLimitDatetime " +
                        "    AND   PS_SPENT_REMAINING > 0 " +
                        " GROUP BY TE_TERMINAL_ID " +
                        " ORDER BY TE_TERMINAL_ID ";


        }
        else
        {
          _national_currency = CurrencyExchange.GetNationalCurrency();

          if (!CurrencyExchange.GetAllowedCurrencies(true, out _currency_exchange_auxiliar, out _currency_exchange_list))
          {
            return null;
          }

          foreach (CurrencyExchange _currency in _currency_exchange_list)
          {
            if (_currency.CurrencyCode != _national_currency)
            {
              _floor_dual_currency = _currency;
            }
          }

          if (_floor_dual_currency == null)
          {
            return null;
          }
          _sql_select = " SELECT   TE_TERMINAL_ID AS TERMINAL_ID" +
                        "        , CASE WHEN TE_ISO_CODE = @pCurrencyIsoCode    " +
                        "  	        THEN ISNULL (SUM(PS_SPENT_REMAINING),0) "+
                        "           ELSE ISNULL (SUM(PS_SPENT_REMAINING) * @pExchangeValue , 0) " +
                        "           END SPENT_REMAINING" +
                        "        , CASE WHEN TE_ISO_CODE = @pCurrencyIsoCode    " +
                        "  	        THEN ISNULL (SUM(PS_PLAYED_AMOUNT),0) "+
                        "           ELSE ISNULL (SUM(PS_PLAYED_AMOUNT) * @pExchangeValue , 0) " +
                        "          END PLAYED_AMOUNT" +
                        "   FROM   PLAY_SESSIONS WITH(INDEX(IX_ps_account_id_finished))" +
                        "  INNER   JOIN TERMINALS ON TE_TERMINAL_ID = PS_TERMINAL_ID" +
                        " WHERE    PS_ACCOUNT_ID  = @pAccountId " +
                        "    AND   PS_STATUS     <> 0 " +
                        "    AND   PS_FINISHED   >= @pLowerLimitDatetime " +
                        "    AND   PS_SPENT_REMAINING > 0 " +
                        " GROUP BY TE_TERMINAL_ID, TE_ISO_CODE " +
                        " ORDER BY TE_TERMINAL_ID ";
        }
        _sql_cmd_select = new SqlCommand(_sql_select);
        _sql_cmd_select.Parameters.Add("@pLowerLimitDatetime", SqlDbType.DateTime).Value = _lower_limit_datetime;
        _sql_cmd_select.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

        if (WSI.Common.Misc.IsFloorDualCurrencyEnabled())
        {
          _sql_cmd_select.Parameters.Add("@pExchangeValue", SqlDbType.Decimal).Value = _floor_dual_currency.ChangeRate;
          _sql_cmd_select.Parameters.Add("@pCurrencyIsoCode", SqlDbType.NVarChar).Value = _national_currency;
        }

        _da = new SqlDataAdapter();
        _da.SelectCommand = _sql_cmd_select;
        _da.SelectCommand.Connection = Trx.Connection;
        _da.SelectCommand.Transaction = Trx;

        _da.Fill(_dt_spent_provider);

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
      }

      return _dt_spent_provider;
    } // AccountRemainingSpentPerTerminal

    //------------------------------------------------------------------------------
    // PURPOSE: Make a DataTable with "providers id", the accumulate of "spent remaining" columns and play sessions 
    //          grouped by session. The DataTable contains the column "DELTA_SPENT_USED" to calculate a data to increment 
    //          the SPEND_USED columns of play_sessions.          
    //
    //  PARAMS:
    //      - INPUT:
    //        - AccountId : Account id 
    //        - Trx : Transaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - DataTable: 
    //  
    //   NOTES:
    //

    public static DataTable AccountRemainingDataPerSession(Int64 AccountId, SqlTransaction Trx)
    {
      DataTable _dt_spent_session;

      SqlCommand _sql_cmd_select;
      SqlDataAdapter _da;
      String _sql_select;
      Int32 _hours_before;
      DateTime _lower_limit_datetime;

      _dt_spent_session = new DataTable("SPENT_PER_SESSION");

      _dt_spent_session.Columns.Add("PROVIDER_ID", Type.GetType("System.String"));       // COLUMN_PROVIDER_ID
      _dt_spent_session.Columns.Add("SPENT_REMAINING", Type.GetType("System.Decimal"));  // COLUMN_SPENT_REMAINING
      _dt_spent_session.Columns.Add("PLAYED_AMOUNT", Type.GetType("System.Decimal"));  // COLUMN_PLAYED_AMOUNT
      _dt_spent_session.Columns.Add("PLAY_SESSION_ID", Type.GetType("System.Int64"));    // COLUMN_PLAY_SESSION_ID
      _dt_spent_session.Columns.Add("DELTA_SPENT_USED", Type.GetType("System.Decimal")); // COLUMN_DELTA_SPENT_USED

      try
      {

        if (!Int32.TryParse(Misc.ReadGeneralParams("Cashier", "Promotions.HoursBeforeTodayOpening", Trx), out _hours_before))
          _hours_before = 0;

        _lower_limit_datetime = Misc.TodayOpening().AddHours(-_hours_before);

        _sql_select = "   SELECT   TE_PROVIDER_ID AS PROVIDER_ID " +
                      "          , PS_SPENT_REMAINING AS SPENT_REMAINING" +
                      "  	     , PS_PLAYED_AMOUNT AS PLAYED_AMOUNT" +
                      "          , PS_PLAY_SESSION_ID AS PLAY_SESSION_ID" +
                      "          , CAST (0 AS MONEY) AS DELTA_SPENT_USED" +
                      "     FROM   PLAY_SESSIONS " +
                      "          , TERMINALS " +
                      "    WHERE   PS_ACCOUNT_ID  = @pAccountId " +
                      "      AND   PS_STATUS     <> 0 " +
                      "      AND   PS_FINISHED   >= @pLowerLimitDatetime " +
                      "      AND   PS_TERMINAL_ID = TE_TERMINAL_ID " +
                      "      AND   PS_SPENT_REMAINING > 0 " +
                      " ORDER BY   PS_PLAY_SESSION_ID ";

        _sql_cmd_select = new SqlCommand(_sql_select);
        _sql_cmd_select.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
        _sql_cmd_select.Parameters.Add("@pLowerLimitDatetime", SqlDbType.DateTime).Value = _lower_limit_datetime;

        _da = new SqlDataAdapter();
        _da.SelectCommand = _sql_cmd_select;
        _da.SelectCommand.Connection = Trx.Connection;
        _da.SelectCommand.Transaction = Trx;

        _da.Fill(_dt_spent_session);

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
      }

      return _dt_spent_session;

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Make a DataTable with "terminal_id", the accumulate of "spent remaining" columns and play sessions 
    //          grouped by session. The DataTable contains the column "DELTA_SPENT_USED" to calculate a data to increment 
    //          the SPEND_USED columns of play_sessions.          
    //
    //  PARAMS:
    //      - INPUT:
    //        - AccountId : Account id 
    //        - Trx : Transaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - DataTable: 
    //  
    //   NOTES:
    //

    public static DataTable AccountRemainingDataPerSessionByTerminal(Int64 AccountId, SqlTransaction Trx)
    {
      DataTable _dt_spent_session;

      SqlCommand _sql_cmd_select;
      SqlDataAdapter _da;
      String _sql_select;
      Int32 _hours_before;
      DateTime _lower_limit_datetime;

      _dt_spent_session = new DataTable("SPENT_PER_SESSION");

      _dt_spent_session.Columns.Add("TERMINAL_ID", Type.GetType("System.String"));       // COLUMN_TERMINAL_ID
      _dt_spent_session.Columns.Add("SPENT_REMAINING", Type.GetType("System.Decimal"));  // COLUMN_SPENT_REMAINING
      _dt_spent_session.Columns.Add("PLAYED_AMOUNT", Type.GetType("System.Decimal"));    // COLUMN_PLAYED_AMOUNT
      _dt_spent_session.Columns.Add("PLAY_SESSION_ID", Type.GetType("System.Int64"));    // COLUMN_PLAY_SESSION_ID
      _dt_spent_session.Columns.Add("DELTA_SPENT_USED", Type.GetType("System.Decimal")); // COLUMN_DELTA_SPENT_USED

      try
      {

        if (!Int32.TryParse(Misc.ReadGeneralParams("Cashier", "Promotions.HoursBeforeTodayOpening", Trx), out _hours_before))
          _hours_before = 0;

        _lower_limit_datetime = Misc.TodayOpening().AddHours(-_hours_before);

        _sql_select = "   SELECT   TE_TERMINAL_ID AS TERMINAL_ID " +
                      "          , PS_SPENT_REMAINING AS SPENT_REMAINING" +
                      "  	       , PS_PLAYED_AMOUNT AS PLAYED_AMOUNT" +
                      "          , PS_PLAY_SESSION_ID AS PLAY_SESSION_ID" +
                      "          , CAST (0 AS MONEY) AS DELTA_SPENT_USED" +
                      "     FROM   PLAY_SESSIONS " +
                      "          , TERMINALS " +
                      "    WHERE   PS_ACCOUNT_ID  = @pAccountId " +
                      "      AND   PS_STATUS     <> 0 " +
                      "      AND   PS_FINISHED   >= @pLowerLimitDatetime " +
                      "      AND   PS_TERMINAL_ID = TE_TERMINAL_ID " +
                      "      AND   PS_SPENT_REMAINING > 0 " +
                      " ORDER BY   PS_PLAY_SESSION_ID ";

        _sql_cmd_select = new SqlCommand(_sql_select);
        _sql_cmd_select.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
        _sql_cmd_select.Parameters.Add("@pLowerLimitDatetime", SqlDbType.DateTime).Value = _lower_limit_datetime;

        _da = new SqlDataAdapter();
        _da.SelectCommand = _sql_cmd_select;
        _da.SelectCommand.Connection = Trx.Connection;
        _da.SelectCommand.Transaction = Trx;

        _da.Fill(_dt_spent_session);

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
      }

      return _dt_spent_session;

    } // AccountRemainingSpentPerSessionByTerminal

    //------------------------------------------------------------------------------
    // PURPOSE: Update the PLAY_SESSIONS with the "delta spent used" calculated
    //
    //  PARAMS:
    //      - INPUT:
    //        - AccountId : Account id 
    //        - DeltaSpentUsed : Datatable with "delta spent"
    //        - Trx : Transaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - Boolean: Is it correct the update?
    //  
    //   NOTES:
    //
    public static Boolean AccountUpdateSpentUsed(Int64 AccountId, DataTable DeltaSpentUsed, SqlTransaction Trx)
    {
      SqlDataAdapter _da;
      StringBuilder _sql_txt;
      int _num_updated;
      int _num_modified;

      _num_modified = DeltaSpentUsed.Select("", "", DataViewRowState.ModifiedCurrent).Length;
      if (_num_modified == 0)
      {
        return true;
      }

      try
      {

        _da = new SqlDataAdapter();

        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("UPDATE PLAY_SESSIONS ");
        _sql_txt.AppendLine("   SET PS_SPENT_USED       = PS_SPENT_USED + @pDeltaSpentUsed");
        _sql_txt.AppendLine(" WHERE PS_ACCOUNT_ID       = @pAccountId ");
        _sql_txt.AppendLine("   AND PS_PLAY_SESSION_ID  = @pSessionId");
        _sql_txt.AppendLine("   AND PS_SPENT_REMAINING >= @pDeltaSpentUsed");

        _da.UpdateCommand = new SqlCommand(_sql_txt.ToString());
        _da.UpdateCommand.Connection = Trx.Connection;
        _da.UpdateCommand.Transaction = Trx;
        _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;

        _da.UpdateCommand.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
        _da.UpdateCommand.Parameters.Add("@pSessionId", SqlDbType.BigInt, 8, "PLAY_SESSION_ID");
        _da.UpdateCommand.Parameters.Add("@pDeltaSpentUsed", SqlDbType.Money, 8, "DELTA_SPENT_USED");

        _da.DeleteCommand = null;
        _da.SelectCommand = null;
        _da.InsertCommand = null;
        _da.UpdateBatchSize = 500;
        _da.ContinueUpdateOnError = true;

        _num_updated = _da.Update(DeltaSpentUsed);

        if (_num_modified == _num_updated)
        {
          return true;
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return false;
    }

    public static Boolean CommonPromotionToRedeem(CardData Account,
                                                  SqlTransaction Trx)
    {
      if (!Account.PromotionToRedeemableAllowed)
      {
        return false;
      }

      if (Account.IsLoggedIn)
      {
        Log.Error("CommonPromotionToRedeem.Error: " + Resource.String("STR_UC_CARD_IN_USE_ERROR"));

        return false;
      }

      return UpdateAccountsPromotionToReedeem(Account, Trx);

    } // CommonPromotionToRedeem


    public static Boolean UpdateAccountsPromotionToReedeem(CardData Account, SqlTransaction Trx)
    {
      String _sql;

      try
      {
        _sql = "";
        _sql += "UPDATE   ACCOUNTS";
        _sql += "   SET   AC_INITIAL_NOT_REDEEMABLE = 0 ";
        _sql += "       , AC_NR_WON_LOCK            = 0 ";
        _sql += "       , AC_CURRENT_PROMOTION_ID   = NULL ";
        _sql += "       , AC_LAST_ACTIVITY          = GETDATE() ";
        _sql += " WHERE   AC_ACCOUNT_ID             = @pAccountID ";
        _sql += "   AND   AC_BALANCE                = @pBalance   ";
        _sql += "   AND   AC_NR_EXPIRATION          > GETDATE ()  ";   // Not expired!

        using (SqlCommand _sql_cmd = new SqlCommand(_sql, Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pAccountID", SqlDbType.BigInt).Value = Account.AccountId;
          _sql_cmd.Parameters.Add("@pBalance", SqlDbType.Money).Value = Account.CurrentBalance.SqlMoney;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateAccountsPromotionToReedeem

    //------------------------------------------------------------------------------
    // PURPOSE: Calculate "delta spent used" and Update the PLAY_SESSIONS
    //
    //  PARAMS:
    //      - INPUT:
    //        - SpentUsed : Currency
    //        - AccountId : Account id 
    //        - PromoData : Promotion.TYPE_PROMOTION_DATA
    //        - Trx : Transaction
    //
    //      - OUTPUT:
    //        - StrMessage : String
    //
    // RETURNS:
    //    - Boolean: 
    //        - True: It's correct
    //        - False: It isn't correct
    //   NOTES:
    //
    public static Boolean UpdateSpentUsedInPromotion(Currency SpentUsed, Int64 AccountId, Promotion.TYPE_PROMOTION_DATA PromoData, SqlTransaction Trx, out String StrMessage)
    {
      DataTable _dt_remaining_spent_per_session;
      String _terminal_id;
      Currency _spent_used_aux;
      Currency _delta;
      StringBuilder _sb;
      Object _obj;

      StrMessage = "";

      _spent_used_aux = SpentUsed;

      _dt_remaining_spent_per_session = Promotion.AccountRemainingDataPerSessionByTerminal(AccountId, Trx);

      _sb = new StringBuilder();
      _sb.AppendLine("SELECT   1 ");
      _sb.AppendLine("  FROM   TERMINAL_GROUPS ");
      _sb.AppendLine(" WHERE   TG_TERMINAL_ID = @TerminalId ");
      _sb.AppendLine("   AND   TG_ELEMENT_ID = @PromotionId ");
      _sb.AppendLine("   AND   TG_ELEMENT_TYPE = @ElementType");

      foreach (DataRow _session in _dt_remaining_spent_per_session.Rows)
      {
        _terminal_id = (String)_session[Promotion.COLUMN_TERMINAL_ID];

        if (PromoData.terminal_list.ListType != TerminalList.TerminalListType.All)
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@TerminalId", SqlDbType.Int).Value = _terminal_id;
            _cmd.Parameters.Add("@PromotionId", SqlDbType.BigInt).Value = PromoData.id;
            _cmd.Parameters.Add("@ElementType", SqlDbType.Int).Value = EXPLOIT_ELEMENT_TYPE.PROMOTION;

            _obj = _cmd.ExecuteScalar();
          }

          if (_obj == null || _obj == DBNull.Value)
          {
            continue;
          }
        }

        _delta = Math.Min((Decimal)_session[Promotion.COLUMN_SPENT_REMAINING], _spent_used_aux);
        _session[Promotion.COLUMN_DELTA_SPENT_USED] = (Decimal)_delta;
        _spent_used_aux -= _delta;

        if (_spent_used_aux <= 0)
        {
          break;
        }
      }

      if (_spent_used_aux > 0)
      {
        StrMessage = "The Play Sessions spent used is not enough.";

        return false;
      }

      if (!Promotion.AccountUpdateSpentUsed(AccountId, _dt_remaining_spent_per_session, Trx))
      {
        StrMessage = "Update Play Sessions fails.";

        return false;
      }

      return true;
    } // UpdateSpentUsedInPromotion


    //------------------------------------------------------------------------------
    // PURPOSE: Calculate used credit for an promotion
    //
    //  PARAMS:
    //      - INPUT:
    //        - PromotionId : Promotion id
    //        - AccountId : Account id 
    //        - IsVirtual: Account is virtual?
    //        - Trx :  Sql Transaction
    //
    //      - OUTPUT:
    //        - UsedCreditInfo : Promotion.TYPE_PROMO_CREDIT_USED
    //
    // RETURNS:
    //    - Boolean: 
    //        - False: Errors occurred
    //        - True: Otherwise
    //   NOTES:
    //        ICS 27-MAR-2014 Virtual accounts don't take in mind the daily and monthly limits
    //
    public static Boolean GetUsedCreditPromotion(Int64 PromotionId, Int64 AccountId, Boolean IsVirtual, out TYPE_PROMO_CREDIT_USED UsedCreditInfo, SqlTransaction Trx)
    {
      StringBuilder _sb;
      DateTime _today;

      UsedCreditInfo = new TYPE_PROMO_CREDIT_USED();

      UsedCreditInfo.global_daily_credit_used = 0;
      UsedCreditInfo.global_monthly_credit_used = 0;
      UsedCreditInfo.global_credit_used = 0;
      UsedCreditInfo.account_daily_credit_used = 0;
      UsedCreditInfo.account_monthly_credit_used = 0;

      _today = Misc.TodayOpening();
      _sb = new StringBuilder();

      _sb.AppendLine(" SELECT   ISNULL(SUM(CASE WHEN ACP_PROMO_DATE >= @pTodayOpening ");
      _sb.AppendLine("                          THEN ACP_INI_BALANCE ELSE 0 END) ,0) AS DAILY ");
      _sb.AppendLine("        , ISNULL(SUM(CASE WHEN ACP_PROMO_DATE >= DATEADD(DAY, -DATEPART(DAY,@pTodayOpening) + 1, @pTodayOpening) ");
      _sb.AppendLine("                          THEN ACP_INI_BALANCE ELSE 0 END) ,0) AS MONTHLY ");
      _sb.AppendLine("        , ISNULL(SUM(ACP_INI_BALANCE),0) AS GLOBAL ");

      if (!IsVirtual)
      {
        _sb.AppendLine("        , ISNULL(SUM(CASE WHEN (ACP_PROMO_DATE >= @pTodayOpening AND   ACP_ACCOUNT_ID  = @pAccountId) ");
        _sb.AppendLine("                          THEN ACP_INI_BALANCE ELSE 0 END) ,0) AS DAILY_ACC ");
        _sb.AppendLine("        , ISNULL(SUM(CASE WHEN (ACP_PROMO_DATE >= DATEADD(DAY, -DATEPART(DAY,@pTodayOpening) + 1, @pTodayOpening) AND   ACP_ACCOUNT_ID  = @pAccountId) ");
        _sb.AppendLine("                          THEN ACP_INI_BALANCE ELSE 0 END) ,0) AS MONTHLY_ACC ");
      }
      _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(IX_acp_promo_id_account_date))");
      _sb.AppendLine("  WHERE   ACP_PROMO_ID = @pPromoId ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTodayOpening", SqlDbType.DateTime).Value = _today;
          _cmd.Parameters.Add("@pPromoId", SqlDbType.BigInt).Value = PromotionId;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            UsedCreditInfo.global_daily_credit_used = _reader.GetDecimal(0);
            UsedCreditInfo.global_monthly_credit_used = _reader.GetDecimal(1);
            UsedCreditInfo.global_credit_used = _reader.GetDecimal(2);

            if (!IsVirtual)
            {
              UsedCreditInfo.account_daily_credit_used = _reader.GetDecimal(3);
              UsedCreditInfo.account_monthly_credit_used = _reader.GetDecimal(4);
            }

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Add PlayPoint promotion
    /// </summary>
    /// <param name="OperationId"></param>
    /// <param name="AccountId"></param>
    /// <param name="AccountLevel"></param>
    /// <param name="TotalRecharged"></param>
    /// <param name="TotalPoints"></param>
    /// <param name="Awarded"></param>
    /// <param name="AwardedAmount"></param>
    /// <param name="Promotions"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal Boolean AddPlayPointPromotion(Int64 OperationId, Int64 AccountId, Int32 AccountLevel, Currency TotalRecharged, Points TotalPoints, Boolean Awarded, Currency AwardedAmount, DataTable Promotions, Int64 PromotionId, SqlTransaction SqlTrx)
    {
      AccountPromotion _promotion;

      _promotion = new AccountPromotion();

      try
      {
        // Read Promotion
        if (!Promotion.ReadPromotionData(SqlTrx, PromotionId, _promotion))
        {
          return false;
        }

        // Insert the Exchange promotion in DataTable promotions.
        if (!AccountPromotion.AddPromotionToAccount(OperationId, AccountId, AccountLevel, TotalRecharged, TotalPoints, Awarded, AwardedAmount, _promotion, Promotions))
        {
          return false;
        }

        // Apply promotion to account
        if (!AccountPromotion.InsertAccountPromotions(Promotions, false, true, SqlTrx))
        {
          return false;

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return true;
    } // AddPlayPointPromotion

  }
}