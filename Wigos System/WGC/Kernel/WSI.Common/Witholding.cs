//-------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   WitHolding.cs
// DESCRIPTION:   Class to manage the generation of Witholding documents
// AUTHOR:        Marcos Piedra Osuna
// CREATION DATE: 22-MAR-2012
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 22-MAR-2012  MPO    Initial version
// 02-MAY-2012  MPO    Generate the "ConstanciaEstatal"
// 09-AUG-2012  DDM    Modified funtion DB_CreateWitholdingDoc
// 29-SEP-2015  DHA    Product Backlog Item 3902: external withholding
// 20-JUN-2016  LTC    Product Backlog Item 13612,13613,13614: Witholding Evidence.
// 13-JUL-2016  LTC    Bug 15581: Witholding Documents: Change NLS for Codere
// 30-NOV-2016  RAB    PBI 21157: Withholding generation - Thread.
// 30-NOV-2016  JML    PBI 21149: Generation of Withholding - Direct printing
// 01-FEB-2017  FGB    PBI 23979: Generation of Withholding: If generate Excel then also generate PDF at the same time
// 01-MAR-2017  FGB    Bug 24126: Withholding generation: The Excel data is incorrect
// 16-MAR-2017  ATB    PBI 25737: Third TAX - Report columns
// 29-MAY-2017  DHA    Bug 27734: Background constancy generation error
// 29-MAY-2017  LTC    Bug 27738: Error witholding document - delete comma in "raz�n social" field
// 18-JUN-2017  FGB    Bug 27741: Error witholding document - "Folio" field does not show the required value
// -------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace WSI.Common
{
  public class Witholding
  {
    #region Constants
    public const Int32 WAIT_FOR_GENERATE_WITHHOLDING = 60;      // 1 minute
    #endregion

    #region Enum
    public enum WITHHOLDING_PRINT_MODE
    {
      BACKGROUND = 0,
      IMMEDIATE = 1
    }
    #endregion

    public Int64 OperationId; // LTC 20-JUN-2016

    //------------------------------------------------------------------------------
    // PURPOSE : Check whether the amount to cash includes a prize that requires 
    //           the generation of Witholding documents
    // 
    //  PARAMS :
    //      - INPUT :
    //          - AmountToCheck : card containing the prize amount to cash (if any) 
    //
    //      - OUTPUT :
    //          - WitholdingRequired : Whether Witholding document is required for any category (1,2,3)
    //
    // RETURNS : Whether any Witholding document is required
    // 
    //   NOTES :

    public static Boolean CheckWitholding(Currency PrizeAmount, out Boolean WithholdingRequired, out Boolean[] DocumentRequired)
    {
      Decimal _threshold_1;
      Decimal _threshold_2;

      WithholdingRequired = false;
      DocumentRequired = new Boolean[3];
      DocumentRequired[0] = false;
      DocumentRequired[1] = false;
      DocumentRequired[2] = false;

      try
      {
        if (PrizeAmount <= 0)
        {
          return true;
        }

        // Retrieve Witholding configuration
        if (!Witholding.Enabled())
        {
          return true;
        }

        // Is any Witholding document required?
        _threshold_1 = Witholding.GetOnPrizeGreaterThan1();
        DocumentRequired[0] = (PrizeAmount > _threshold_1);

        _threshold_2 = Witholding.GetOnPrizeGreaterThan2();
        DocumentRequired[1] = (PrizeAmount > _threshold_2);

        DocumentRequired[2] = false;

        WithholdingRequired = DocumentRequired[0] || DocumentRequired[1] || DocumentRequired[2];

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // CheckWitholding

    // LTC 20-JUN-2016
    //------------------------------------------------------------------------------
    // PURPOSE : Get Witholding Evidence List
    // 
    //  PARAMS :
    //      - INPUT :
    //          - OperationId : Id Operation fron DB 
    //          - OperationPrize : Amount to process
    //          - FederalDoc : Federal template id
    //          - StatalDoc : Statal template id
    //
    //      - OUTPUT :
    //          - List<CashierDocs.EVIDENCE_TYPE> : List of witholding documents
    //
    // RETURNS : Whether any Witholding document is required
    // 
    //   NOTES :
    public static List<CashierDocs.EVIDENCE_TYPE> GetWitholdingDocuments(Int64 OperationId, Currency OperationPrize, CashierDocs.EVIDENCE_TYPE FederalDoc, CashierDocs.EVIDENCE_TYPE StatalDoc)
    {
      Decimal _threshold_1;
      Decimal _threshold_2;

      List<CashierDocs.EVIDENCE_TYPE> _list_withholding_documents;

      Misc.WriteLog(String.Format("GetWitholdingDocuments BEGIN - OperationId: {0} Prize: {1}", OperationId, OperationPrize), Log.Type.Message);

      Misc.WriteLog(String.Format("GetWitholdingDocuments - FederalDoc: {0}", FederalDoc.ToString()), Log.Type.Message);
      Misc.WriteLog(String.Format("GetWitholdingDocuments - StatalDoc: {0}", StatalDoc.ToString()), Log.Type.Message);

      try
      {
        _list_withholding_documents = new List<CashierDocs.EVIDENCE_TYPE>();

        // Retrieve Witholding configuration
        if (!Witholding.Enabled())
        {
          Misc.WriteLog("GetWitholdingDocuments - Witholding not enabled", Log.Type.Message);
          return _list_withholding_documents;
        }

        // Is any Witholding document required?
        _threshold_1 = Witholding.GetOnPrizeGreaterThan1();
        Misc.WriteLog(String.Format("GetWitholdingDocuments - _threshold_1: {0}", _threshold_1), Log.Type.Message);

        Misc.WriteLog(String.Format("GetWitholdingDocuments - OperationPrize > _threshold_1: {0}", OperationPrize > _threshold_1), Log.Type.Message);
        if (OperationPrize > _threshold_1)
        {
          Misc.WriteLog("GetWitholdingDocuments - Add Federal Doc", Log.Type.Message);
          _list_withholding_documents.Add(FederalDoc);
        }

        _threshold_2 = Witholding.GetOnPrizeGreaterThan2();

        Misc.WriteLog(String.Format("GetWitholdingDocuments - _threshold_2: {0}", _threshold_2), Log.Type.Message);

        Misc.WriteLog(String.Format("GetWitholdingDocuments - OperationPrize > _threshold_2: {0}", OperationPrize > _threshold_1), Log.Type.Message);
        if (OperationPrize > _threshold_2)
        {
          if (StatalDoc != CashierDocs.EVIDENCE_TYPE.NONE)
          {
            Misc.WriteLog("GetWitholdingDocuments - Add Statal Doc", Log.Type.Message);
            _list_withholding_documents.Add(StatalDoc);
          }

          //FGB 2017-02-01 PBI 23979: Generation of Withholding: If generate Excel then also generate PDF at the same time
          if (StatalDoc == CashierDocs.EVIDENCE_TYPE.FEDERAL_CODERE)
          {
            Misc.WriteLog("GetWitholdingDocuments - Add CashierDocs.EVIDENCE_TYPE.ESTATAL", Log.Type.Message);
            _list_withholding_documents.Add(CashierDocs.EVIDENCE_TYPE.ESTATAL);
          }
        }

        Misc.WriteLog("GetWitholdingDocuments - END", Log.Type.Message);
        return _list_withholding_documents;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return new List<CashierDocs.EVIDENCE_TYPE>();
    }

    /// <summary>
    /// Is the whitholding enabled
    /// </summary>
    /// <returns></returns>
    public static Boolean Enabled()
    {
      return GeneralParam.GetBoolean("Witholding.Document", "Enabled", true);
    }

    /// <summary>
    /// Generates withholding document
    /// </summary>
    /// <returns></returns>
    public static Boolean GenerateWitholdingDocument()
    {
      return (Enabled() && (GetGenerateDocumentMode() != CashierDocs.EVIDENCE_TYPE.NONE));
    }

    /// <summary>
    /// Generates withholding document in excel
    /// </summary>
    /// <returns></returns>
    public static Boolean GenerateWitholdingDocumentExcel()
    {
      //FEDERAL_CODERE --> Generates Excel & PDF
      return (Enabled() && (GetGenerateDocumentMode() == CashierDocs.EVIDENCE_TYPE.FEDERAL_CODERE));
    }

    public static CashierDocs.EVIDENCE_TYPE GetGenerateDocumentMode()
    {
      return (CashierDocs.EVIDENCE_TYPE)(GeneralParam.GetInt32("Witholding.Document", "GenerateDocument", (Int32)CashierDocs.EVIDENCE_TYPE.NONE));
    }

    public static Int32 WitholdingDocumentThreadWaitSeconds()
    {
      return GeneralParam.GetInt32("Witholding.Document", "Thread.WaitSeconds", WAIT_FOR_GENERATE_WITHHOLDING);
    }

    public static Witholding.WITHHOLDING_PRINT_MODE WitholdingDocumentModeGenerate()
    {
      return (Witholding.WITHHOLDING_PRINT_MODE)GeneralParam.GetInt32("Witholding.Document", "PrintMode", (Int32)Witholding.WITHHOLDING_PRINT_MODE.BACKGROUND);
    }

    public static Decimal GetOnPrizeGreaterThan1()
    {
      return GeneralParam.GetDecimal("Witholding.Document", "OnPrizeGreaterThan.1", 0);
    }

    public static Decimal GetOnPrizeGreaterThan2()
    {
      return GeneralParam.GetDecimal("Witholding.Document", "OnPrizeGreaterThan.2", 0);
    }

    /// <summary>
    /// Generates the Witholding document related to the operation (Major Prize payment)
    /// </summary>
    /// <returns></returns>
    public static Boolean DB_CreateWitholdingDoc()
    {
      if (DB_CreateWitholdingDoc(0, null).Count <= 0)
      {
        return false;
      }

      return true;
    }

    // LTC 20-JUN-2016
    //------------------------------------------------------------------------------
    // PURPOSE : Generates the Witholding document related to the operation (Major 
    //           Prize payment)
    // 
    //  PARAMS :
    //      - INPUT :
    //          - OperationId : Id of the operation that identifies the major prize
    //          - SqlTx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :
    public static DocumentList DB_CreateWitholdingDoc(Int64 OperationId = 0,
                                                      SqlTransaction SqlTrx = null)
    {
      DB_TRX _db_trx;
      SqlTransaction _sql_Trx;
      String _sql_str;
      StringBuilder _sb;
      Int32 _num_rows_updated;
      DataSet _sql_dataset;

      Int64 _operation_id;
      Int64 _operation_prize;
      OperationCode _operation_code;

      CashierDocs.EVIDENCE_TYPE _state_template;

      Currency[] _witholding_tax;
      DateTime _prize_datetime;
      String _player_id1;
      String _player_id2;
      String _player_name1;
      String _player_name2;
      String _player_name3;
      String _player_full_name;
      Currency _prize;
      String _business_id1;
      String _business_id2;
      String _business_name;
      String _representative_id1;
      String _representative_id2;
      String _representative_name;
      Int64 _document_id;

      Byte[] _sign;

      MemoryStream _ms_pdf;
      DocumentList _list;
      DocumentList _doc_list;
      String _str;
      String _document_filename;

      String _ext;
      String _evidence_path;
      String _site_id;
      List<String> _operation_id_complete;

      Misc.WriteLog(String.Format("DB_CreateWitholdingDoc - BEGIN"), Log.Type.Message);

      List<CashierDocs.EVIDENCE_TYPE> _list_withholding_documents;
      _operation_id_complete = new List<string>();

      using (_db_trx = new DB_TRX())
      {
        _site_id = GetSiteId();

        Misc.WriteLog(String.Format("DB_CreateWitholdingDoc - SiteId {0}", _site_id), Log.Type.Message);

        _list = new DocumentList();

        try
        {
          _sql_dataset = new DataSet();

          _sql_Trx = (SqlTrx != null) ? SqlTrx : _db_trx.SqlTransaction;

          //Get Sql for major prizes
          _sql_str = GetMajorPrizesSql(OperationId);

          Misc.WriteLog(String.Format("DB_CreateWitholdingDoc - OperationId: {0}", OperationId), Log.Type.Message);

          //Fill dataset
          using (SqlCommand _sql_cmd = new SqlCommand(_sql_str, _sql_Trx.Connection, _sql_Trx))
          {
            using (SqlDataAdapter _sql_adapter = new SqlDataAdapter(_sql_cmd))
            {
              _sql_adapter.Fill(_sql_dataset);
            }
          }

          Misc.WriteLog(String.Format("DB_CreateWitholdingDoc - Table: TOTAL {0}", _sql_dataset.Tables.Count), Log.Type.Message);
          Misc.WriteLog(String.Format("DB_CreateWitholdingDoc - Rows: TOTAL {0}", _sql_dataset.Tables[0].Rows.Count), Log.Type.Message);

          foreach (DataRow _dr in _sql_dataset.Tables[0].Rows)
          {
            _operation_id = Convert.ToInt64(_dr["mpg_operation_id"]);
            _operation_prize = Convert.ToInt64(_dr["amp_prize"]);
            _state_template = GetGenerateDocumentMode();

            Misc.WriteLog(String.Format("DB_CreateWitholdingDoc - FOREACH OperationId: {0} Prize: {1}", _operation_id, _operation_prize), Log.Type.Message);

            //GetEvidenceDocumentTogenerate
            _list_withholding_documents = GetWitholdingDocuments(_operation_id, _operation_prize, CashierDocs.EVIDENCE_TYPE.FEDERAL, _state_template);

            Misc.WriteLog(String.Format("DB_CreateWitholdingDoc - FOREACH withholding documents count: {0}", _list_withholding_documents.Count), Log.Type.Message);

            if (_list_withholding_documents.Count == 0)
            {
              continue;
            }

            _ms_pdf = null;

            _witholding_tax = new Currency[3];
            _prize_datetime = DateTime.MinValue;
            _player_id1 = "";
            _player_id2 = "";
            _player_name1 = "";
            _player_name2 = "";
            _player_name3 = "";
            _player_full_name = "";
            _prize = 0;
            _business_id1 = "";
            _business_id2 = "";
            _business_name = "";
            _representative_id1 = "";
            _representative_id2 = "";
            _representative_name = "";
            _document_id = 0;

            _sign = null;

            _sb = new StringBuilder();
            _sb.AppendLine(" SELECT AMP_WITHOLDING_TAX1               ");  // 0
            _sb.AppendLine("      , AMP_WITHOLDING_TAX2               ");  // 1
            _sb.AppendLine("      , AMP_WITHOLDING_TAX3               ");  // 2
            _sb.AppendLine("      , AMP_DATETIME                      ");  // 3
            _sb.AppendLine("      , AMP_PLAYER_ID1                    ");  // 4
            _sb.AppendLine("      , AMP_PLAYER_ID2                    ");  // 5
            _sb.AppendLine("      , AMP_PLAYER_NAME                   ");  // 6
            _sb.AppendLine("      , AMP_PRIZE                         ");  // 7
            _sb.AppendLine("      , AMP_BUSINESS_ID1                  ");  // 8
            _sb.AppendLine("      , AMP_BUSINESS_ID2                  ");  // 9
            _sb.AppendLine("      , AMP_BUSINESS_NAME                 ");  // 10
            _sb.AppendLine("      , AMP_REPRESENTATIVE_ID1            ");  // 11
            _sb.AppendLine("      , AMP_REPRESENTATIVE_ID2            ");  // 12
            _sb.AppendLine("      , AMP_REPRESENTATIVE_NAME           ");  // 13
            _sb.AppendLine("      , AMP_DOCUMENT_ID1                  ");  // 14
            _sb.AppendLine("      , AMP_PLAYER_NAME1                  ");  // 15
            _sb.AppendLine("      , AMP_PLAYER_NAME2                  ");  // 16
            _sb.AppendLine("      , AMP_PLAYER_NAME3                  ");  // 17
            _sb.AppendLine("  FROM  ACCOUNT_MAJOR_PRIZES              ");
            _sb.AppendLine(" WHERE  AMP_OPERATION_ID = @pOperationId  ");

            Misc.WriteLog(String.Format("DB_CreateWitholdingDoc - BEGIN SELECT ACCOUNT_MAJOR_PRIZES"), Log.Type.Message);

            using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), _sql_Trx.Connection, _sql_Trx))
            {
              _sql_command.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = _operation_id;

              using (SqlDataReader _reader = _sql_command.ExecuteReader())
              {
                if (_reader.Read())
                {
                  _witholding_tax[0] = _reader.IsDBNull(0) ? 0 : (Currency)_reader.GetSqlMoney(0);
                  _witholding_tax[1] = _reader.IsDBNull(1) ? 0 : (Currency)_reader.GetSqlMoney(1);
                  _witholding_tax[2] = _reader.IsDBNull(2) ? 0 : (Currency)_reader.GetSqlMoney(2);
                  _prize_datetime = (DateTime)_reader.GetDateTime(3);
                  _player_id1 = (String)_reader.GetSqlString(4);
                  _player_id2 = (String)_reader.GetSqlString(5);
                  _player_name1 = _reader.IsDBNull(15) ? "" : (String)_reader.GetSqlString(15);
                  _player_name2 = _reader.IsDBNull(16) ? "" : (String)_reader.GetSqlString(16);
                  _player_name3 = _reader.IsDBNull(17) ? "" : (String)_reader.GetSqlString(17);
                  _player_full_name = CardData.FormatFullName(FULL_NAME_TYPE.WITHHOLDING, _player_name3, _player_name1, _player_name2);
                  _prize = (Currency)_reader.GetSqlMoney(7);
                  _business_id1 = _reader.IsDBNull(8) ? "" : (String)_reader.GetSqlString(8);
                  _business_id2 = _reader.IsDBNull(9) ? "" : (String)_reader.GetSqlString(9);
                  _business_name = _reader.IsDBNull(10) ? "" : (String)_reader.GetSqlString(10);
                  _representative_id1 = _reader.IsDBNull(11) ? "" : (String)_reader.GetSqlString(11);
                  _representative_id2 = _reader.IsDBNull(12) ? "" : (String)_reader.GetSqlString(12);
                  _representative_name = _reader.IsDBNull(13) ? "" : (String)_reader.GetSqlString(13);
                  _document_id = _reader.IsDBNull(14) ? 0 : (Int64)_reader.GetInt64(14);

                  // LTC 2017-MAY-29
                  if (_state_template == CashierDocs.EVIDENCE_TYPE.FEDERAL_CODERE)
                  {
                    _player_full_name = _player_full_name.ToString().Replace(",", "");
                  }
                }
              }
            }

            Misc.WriteLog(String.Format("DB_CreateWitholdingDoc - END SELECT ACCOUNT_MAJOR_PRIZES"), Log.Type.Message);

            _list = new DocumentList();

            TableDocuments.Load(DOCUMENT_TYPE.SIGNATURE, out _doc_list, _sql_Trx);
            if (_doc_list.Count > 0)
            {
              _sign = _doc_list[0].Content;

              Misc.WriteLog(String.Format("DB_CreateWitholdingDoc - _sign", _sign), Log.Type.Message);
            }

            foreach (CashierDocs.EVIDENCE_TYPE _item in _list_withholding_documents)
            {
              Operations.DB_GetOperationCode(_operation_id, out _operation_code, _sql_Trx);

              // Create a pdf file with tax#
              _ms_pdf = CashierDocs.CreateDocumentEvidence((CashierDocs.EVIDENCE_TYPE)_item,
                                                           _operation_id,
                                                           _operation_code,
                                                           _site_id,
                                                           GetTax(_item, _witholding_tax[0], _witholding_tax[1], _witholding_tax[2]),
                                                           _prize_datetime,
                                                           _player_id1,
                                                           _player_id2,
                                                           _player_full_name,
                                                           _player_name3,
                                                           _player_name1,
                                                           _player_name2,
                                                           _prize,
                                                           _business_id1,
                                                           _business_id2,
                                                           _business_name,
                                                           _representative_id1,
                                                           _representative_id2,
                                                           _representative_name,
                                                           _sign,
                                                           _document_id,
                                                           out _ext,
                                                           _sql_Trx);

              if (_ms_pdf == null)
              {
                _str = String.Format("DB_CreateWitholdingDoc.CreateDocumentEvidence Tax={0} OpId={1}", _item, _operation_id.ToString());
                Log.Error(_str);

                continue;
              }

              //Add document
              _document_filename = GetDocumentFilename(_item, _prize_datetime, _player_id1, _ext, _site_id);

              Misc.WriteLog(String.Format("DB_CreateWitholdingDoc - Add pdf document"), Log.Type.Message);
              _list.Add(new ReadOnlyDocument(_document_filename, _ms_pdf));

              Misc.WriteLog(String.Format("DB_CreateWitholdingDoc - Dispose"), Log.Type.Message);
              _ms_pdf.Dispose();
            }

            if (_list.Count == 0)
            {
              continue;
            }

            Misc.WriteLog(String.Format("DB_CreateWitholdingDoc - Save Documents"), Log.Type.Message);
            if (!TableDocuments.Save(ref _document_id, DOCUMENT_TYPE.WITHOLDING, _list, _sql_Trx))
            {
              Misc.WriteLog(String.Format("DB_CreateWitholdingDoc - Save Documents - Error"), Log.Type.Message);
              continue;
            }

            // Add operation to delete
            _operation_id_complete.Add(_operation_id.ToString());

            Misc.WriteLog(String.Format("DB_CreateWitholdingDoc - UPDATE   ACCOUNT_MAJOR_PRIZES"), Log.Type.Message);

            _sb = new StringBuilder();
            _sb.AppendLine("UPDATE   ACCOUNT_MAJOR_PRIZES               ");
            _sb.AppendLine("   SET   AMP_DOCUMENT_ID1 = @pDocumentId1   ");
            _sb.AppendLine(" WHERE   AMP_OPERATION_ID = @pOperationId   ");

            using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), _sql_Trx.Connection, _sql_Trx))
            {
              _sql_command.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = _operation_id;
              _sql_command.Parameters.Add("@pDocumentId1", SqlDbType.BigInt).Value = _document_id;

              _num_rows_updated = _sql_command.ExecuteNonQuery();
            }

            if (_num_rows_updated != 1)
            {
              Log.Error("DB_CreateWitholdingDoc.UPDATE OpId=" + _operation_id.ToString());

              continue;
            }

            Misc.WriteLog(String.Format("DB_CreateWitholdingDoc - UPDATE   ACCOUNT_MAJOR_PRIZES --> OK"), Log.Type.Message);

            //Save Documents to Disc
            _evidence_path = GeneralParam.GetString("Witholding.Document", "OutputPath", "");
            if (!String.IsNullOrEmpty(_evidence_path))
            {
              if (!_list.Save(_evidence_path))
              {
                Misc.WriteLog(String.Format("DB_CreateWitholdingDoc - Documents not saved to path: {0}", _evidence_path), Log.Type.Message);
              }
            }
          }

          //Delete 
          DB_DeleteMajorPrizes(_operation_id_complete, _sql_Trx);

          _db_trx.SqlTransaction.Commit();

          Misc.WriteLog(String.Format("DB_CreateWitholdingDoc - END - return document"), Log.Type.Message);

          return _list;
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }

      Misc.WriteLog(String.Format("DB_CreateWitholdingDoc - END - return empty document"), Log.Type.Message);

      return new DocumentList();
    }

    // Return Sql for major prizes
    private static String GetMajorPrizesSql(Int64 OperationId)
    {
      StringBuilder _sb;
      _sb = new StringBuilder();

      Misc.WriteLog(String.Format("GetMajorPrizesSql BEGIN - OperationId {0}", OperationId), Log.Type.Message);

      //Get operation requested
      if (OperationId == 0)
      {
        _sb.AppendLine(" SELECT MPG.mpg_operation_id,                       ");
        _sb.AppendLine("        AMP.amp_prize                               ");
        _sb.AppendLine("FROM MAJOR_PRIZES_TO_GENERATE MPG                   ");
        _sb.AppendLine(" INNER JOIN ACCOUNT_MAJOR_PRIZES AMP                ");
        _sb.AppendLine("   ON AMP.amp_operation_id = MPG.mpg_operation_id   ");
        _sb.AppendLine(" INNER JOIN ACCOUNTS AC                             ");
        _sb.AppendLine("   ON AC.ac_account_id = AMP.amp_account_id         ");
      }
      else
      {
        _sb.AppendLine(" SELECT AMP.amp_operation_id  as  mpg_operation_id, ");
        _sb.AppendLine("        AMP.amp_prize                               ");
        _sb.AppendLine(" FROM  ACCOUNT_MAJOR_PRIZES AMP                     ");
        _sb.AppendLine(" INNER JOIN ACCOUNTS AC                             ");
        _sb.AppendLine("     ON AC.ac_account_id = AMP.amp_account_id       ");
        _sb.AppendLine(" WHERE AMP.amp_operation_id = " + OperationId);
      }

      Misc.WriteLog(String.Format("GetMajorPrizesSql END"), Log.Type.Message);

      return _sb.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Generates the Witholding document related to the operation (Major 
    //           Prize payment)
    // 
    //  PARAMS :
    //      - INPUT :
    //          - OperationId : Id of the operation that identifies the major prize
    //          - SqlTx
    //          - ListWitholding : Get the list of witholding
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :
    public static Boolean DB_CreateWitholdingDoc(Int64 OperationId
                                               , SqlTransaction SqlTrx
                                               , ref DocumentList ListWitholding)
    {
      ListWitholding = DB_CreateWitholdingDoc(OperationId, SqlTrx);

      if (ListWitholding.Count <= 0)
      {
        return false;
      }

      return true;
    }

    // LTC 20-JUN-2016
    //------------------------------------------------------------------------------
    // PURPOSE : Delete Witholding request from table MAJOR_PRIZES_TO_GENERATE
    //
    // 
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :
    private static void DB_DeleteMajorPrizes(List<String> OperationIds, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      if (OperationIds != null && OperationIds.Count > 0)
      {
        _sb.AppendLine(" DELETE   FROM MAJOR_PRIZES_TO_GENERATE ");
        _sb.AppendLine("  WHERE  MPG_OPERATION_ID IN (" + String.Join(",", OperationIds.ToArray()) + ")");

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_command.ExecuteNonQuery();
        }
      }
    }

    /// <summary>
    /// Get tax depending of the wihtholding type
    /// </summary>
    /// <param name="WihtHoldingType"></param>
    /// <param name="Tax1"></param>
    /// <param name="Tax2"></param>
    /// <returns></returns>
    private static Currency GetTax(CashierDocs.EVIDENCE_TYPE WithHoldingType, Currency Tax1, Currency Tax2, Currency Tax3)
    {
      switch (WithHoldingType)
      {
        case CashierDocs.EVIDENCE_TYPE.FEDERAL:
        case CashierDocs.EVIDENCE_TYPE.FEDERAL_CODERE:
          return Tax1;

        case CashierDocs.EVIDENCE_TYPE.ESTATAL:
        case CashierDocs.EVIDENCE_TYPE.ESTATAL_CAMPECHE:
        case CashierDocs.EVIDENCE_TYPE.ESTATAL_YUCATAN:
          return Tax2;

        case CashierDocs.EVIDENCE_TYPE.COUNCIL:
          return Tax3;

        default:
          return Tax2;
      }
    }

    /// <summary>
    /// Get Site Id
    /// </summary>
    /// <returns></returns>
    private static String GetSiteId()
    {
      Int32 _site_id;

      if (!Int32.TryParse(Misc.ReadGeneralParams("Site", "Identifier"), out _site_id))
      {
        Log.Warning("Witholding.GetSiteId does not obtain an integer");
        return "XXXX";
      }

      return _site_id.ToString("0000");
    }

    /// <summary>
    /// Get Folio
    /// </summary>
    private static String GetFolio(String SiteId, String TipoOperacion, String NumeroRecibo)
    {
      return String.Format("{0}{1}{2}", SiteId, TipoOperacion, NumeroRecibo);
    }

    /// <summary>
    /// Get Withholding Document filename
    /// </summary>
    private static String GetDocumentFilename(CashierDocs.EVIDENCE_TYPE WihtHoldingType, DateTime PrizeDatetime, String PlayerId, String FileExtension, String SiteId)
    {
      Misc.WriteLog(String.Format("Witholding.GetDocumentFilename - BEGIN"), Log.Type.Message);
      String _document_filename;

      _document_filename = String.Format("Evidence_{0}_{1}_{2}{3}{4}{5}{6}_TAX_{7}.{8}",
                                         SiteId,
                                         PlayerId,
                                         PrizeDatetime.Year.ToString(), PrizeDatetime.Month.ToString("00"), PrizeDatetime.Day.ToString("00"),
                                         DateTime.Now.Minute.ToString("00"), DateTime.Now.Second.ToString("00"),
                                         WihtHoldingType,
                                         FileExtension);

      Misc.WriteLog(String.Format("Witholding.GetDocumentFilename - END Document_file_name: {0}", _document_filename), Log.Type.Message);
      return _document_filename;
    }
  }
}
