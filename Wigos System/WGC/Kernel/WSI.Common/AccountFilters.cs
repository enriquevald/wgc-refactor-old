﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ServiceForMGT.cs
// 
//   DESCRIPTION: Class used for get account data by filters 
//        AUTHOR: Enric Tomas
// 
// CREATION DATE: 01-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-DEC-2015 ETP    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common
{
  public class AccountFilters
  {
    #region atributes

    private int m_BirthDateYear;
    private int m_BirthDateMonth;
    private int m_BirthDateDay;

    private int m_WeddingDateYear;
    private int m_WeddingDateMonth;
    private int m_WeddingDateDay;
    
    #endregion //atributes

    #region properties

    public String FirstName { get; set; }

    public String LastName { get; set; }

    public String DocumentID { get; set; }

    public String PhoneNumber { get; set; }

    public String Email { get; set; }

    public int BirthDateYear 
    {
      get
      {
        return m_BirthDateYear;
      }

      set
      {
        m_BirthDateYear = value;
      }
    }

    public int BirthDateMonth
    {
      get
      {
        return m_BirthDateMonth;
      }

      set
      {
        m_BirthDateMonth = value;
      }
    }

    public int BirthDateDay
    {
      get
      {
        return m_BirthDateDay;
      }

      set
      {
        m_BirthDateDay = value;
      }
    }

   public DateTime BirthDate
    {
      set
      {
        m_BirthDateYear = value.Year;
        m_BirthDateMonth = value.Month;
        m_BirthDateDay = value.Day;
      }
    }

    public DateTime WeddingDate
    {
      set
      {
        m_WeddingDateYear = value.Year;
        m_WeddingDateMonth = value.Month;
        m_WeddingDateDay = value.Day;

      }
    }
    public int WeddingDateYear
    {
      get;
      set;
    }

    public int WeddingDateMonth
    {
      get;
      set;
    }

    public int WeddingDateDay
    {
      get;
      set;
    }

    public Boolean OnlyVipAccount { get; set; }

    public GENDER Gender { get; set; }



    #endregion //properties

    #region constructor

    //------------------------------------------------------------------------------
    // PURPOSE : constructor inicialize filters to default value
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    
    public AccountFilters()
    {
      ResetAllFilters();     
    }

    #endregion //constructor

    #region public functions
    
    //------------------------------------------------------------------------------
    // PURPOSE : ResetFilters for inicialize or new search
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //    
    public void ResetAllFilters()
    {
      
      FirstName = "";      
      LastName = "";      
      DocumentID = "";
      
      PhoneNumber = "";
      Email = "";
      
      BirthDateYear = 0;
      BirthDateMonth = 0;
      BirthDateDay = 0;
      
      WeddingDateYear = 0;
      WeddingDateMonth = 0;
      WeddingDateDay = 0;
      
      OnlyVipAccount = false;
      Gender = 0;
    
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Search Player data by filters setted.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //  DataTable: 
    //   NOTES : Use of procedure dbo.GetAccountsData 
    //    
    public DataTable GetPlayerData()
    {
     
      DataTable _table;
      StringBuilder _sb;

      _table = new DataTable();
      _sb = new StringBuilder();


      _sb.AppendLine("EXEC dbo.GetAccountsData @FirstName         ");
      _sb.AppendLine("                    ,    @LastName          ");
      _sb.AppendLine("                    ,    @DocumentID        ");
      _sb.AppendLine("                    ,    @YearBirthDate     ");
      _sb.AppendLine("                    ,    @MonthBirthDate    ");
      _sb.AppendLine("                    ,    @DayBirthDate      ");
      _sb.AppendLine("                    ,    @YearWeddingDate   ");
      _sb.AppendLine("                    ,    @MonthWeddingDate  ");
      _sb.AppendLine("                    ,    @DayWeddingDate    ");
      _sb.AppendLine("                    ,    @OnlyVipAccount    ");
      _sb.AppendLine("                    ,    @Phone             ");
      _sb.AppendLine("                    ,    @Email             ");
      _sb.AppendLine("                    ,    @Gender             ");



      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = FirstName;
            _sql_cmd.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = LastName;
            _sql_cmd.Parameters.Add("@DocumentID", SqlDbType.NVarChar).Value = DocumentID;

            _sql_cmd.Parameters.Add("@YearBirthDate", SqlDbType.Int).Value = BirthDateYear;
            _sql_cmd.Parameters.Add("@MonthBirthDate", SqlDbType.Int).Value = BirthDateMonth;
            _sql_cmd.Parameters.Add("@DayBirthDate", SqlDbType.Int).Value = BirthDateDay;

            _sql_cmd.Parameters.Add("@YearWeddingDate", SqlDbType.Int).Value = WeddingDateYear;
            _sql_cmd.Parameters.Add("@MonthWeddingDate", SqlDbType.Int).Value = WeddingDateMonth;
            _sql_cmd.Parameters.Add("@DayWeddingDate", SqlDbType.Int).Value = WeddingDateDay;

            _sql_cmd.Parameters.Add("@OnlyVipAccount", SqlDbType.Bit).Value = OnlyVipAccount;
            _sql_cmd.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = PhoneNumber;
            _sql_cmd.Parameters.Add("@Email", SqlDbType.NVarChar).Value = Email;

            _sql_cmd.Parameters.Add("@Gender", SqlDbType.Int).Value = Gender;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(_table);
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Error(ex.Message);
      }

      return _table;

    }

    #endregion //public functions


  }

  public static class PlayerDataFields
  {
  

    public static DataTable GetPlayerData(String Name,
                                                     String DocumentId,
                                                     Int32 YearhBirthDate, Int32 MonthBirthDate, Int32 DayBirthDate,
                                                     Int32 YearhWeddingDate, Int32 MonthWeddingDate, Int32 DayWeddingDate,
                                                     Boolean OnlyVipAccount,
                                                     String Phone, String Email, GENDER Gender)
    {
      DataTable _table;
      StringBuilder _sb;

      _table = new DataTable();
      _sb = new StringBuilder();


      _sb.AppendLine("EXEC dbo.GetAccountsData @Name              ");
      _sb.AppendLine("                    ,    @DocumentID        ");
      _sb.AppendLine("                    ,    @YearBirthDate     ");
      _sb.AppendLine("                    ,    @MonthBirthDate    ");
      _sb.AppendLine("                    ,    @DayBirthDate      ");
      _sb.AppendLine("                    ,    @YearWeddingDate   ");
      _sb.AppendLine("                    ,    @MonthWeddingDate  ");
      _sb.AppendLine("                    ,    @DayWeddingDate    ");
      _sb.AppendLine("                    ,    @OnlyVipAccount    ");
      _sb.AppendLine("                    ,    @Phone             ");
      _sb.AppendLine("                    ,    @Email             ");
      _sb.AppendLine("                    ,    @Gender             ");



      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Name;
            _sql_cmd.Parameters.Add("@DocumentID", SqlDbType.NVarChar).Value = DocumentId;

            _sql_cmd.Parameters.Add("@YearBirthDate", SqlDbType.Int).Value = YearhBirthDate;
            _sql_cmd.Parameters.Add("@MonthBirthDate", SqlDbType.Int).Value = MonthBirthDate;
            _sql_cmd.Parameters.Add("@DayBirthDate", SqlDbType.Int).Value = DayBirthDate;

            _sql_cmd.Parameters.Add("@YearWeddingDate", SqlDbType.Int).Value = YearhWeddingDate;
            _sql_cmd.Parameters.Add("@MonthWeddingDate", SqlDbType.Int).Value = MonthWeddingDate;
            _sql_cmd.Parameters.Add("@DayWeddingDate", SqlDbType.Int).Value = DayWeddingDate;

            _sql_cmd.Parameters.Add("@OnlyVipAccount", SqlDbType.Bit).Value = OnlyVipAccount;
            _sql_cmd.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = Phone;
            _sql_cmd.Parameters.Add("@Email", SqlDbType.NVarChar).Value = Email;

            _sql_cmd.Parameters.Add("@Gender", SqlDbType.Int).Value = Gender;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(_table);
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Error(ex.Message);
      }

      return _table;
    }

  }
}
