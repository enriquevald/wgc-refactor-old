//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TitoPrinter.cs
// 
//   DESCRIPTION: TitoPrinter static class
// 
//        AUTHOR: Daniel Dom�nguez Mart�n 
// 
// CREATION DATE: 22-Nov-2013
// 
// REVISION HISTORY:
// 
// Date        Author      Description
// ----------- --------    ----------------------------------------------------------
// 22-Nov-2013 DDM         First version.
// 07-Dec-2013 AJQ & DDM   Add Thread TitoPrinterStatusThread and Critical Section
// 13-Feb-2014 DHA         WIGOSTITO-1072: Read general params in PrintTicket(): Location, Address1 and Address2
// 14-JUL-2014 AMF         Regionalization
// 18-OCT-2016 FAV         PBI 20407: TITO Ticket: Calculated fields
// 05-DEC-2016 FJC         Fixed Bug 8061:Error de comunicaci�n con la impresora
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace WSI.Common
{
  public static class TitoPrinter
  {

    public enum TITO_PRINTER_PRINT_MODE
    {
      ASYNCHRONOUS = 0,
      REAL_TIME = 1
    }

    #region "Private members"
    private static ITitoPrinter m_printer_device;
    private static TicketInfo m_ticket_info;
    private static TitoTicketDefault m_ticket_template;

    private static ReaderWriterLock m_data_lock = new ReaderWriterLock();
    private static ReaderWriterLock m_device_lock = new ReaderWriterLock();
    private static AutoResetEvent m_ticket_printed = new AutoResetEvent(true);
    private static AutoResetEvent m_status_available = new AutoResetEvent(false);

    private static PrinterStatus m_device_status = PrinterStatus.Unknown;
    private static String m_device_status_text = String.Empty;
    private static Boolean m_device_is_ready = false;

    private static Decimal m_wait_printer_response_seconds = 5;

    #endregion

    #region Private Methods

    private static void TitoPrinterStatusThread()
    {
      while (true)
      {
        if (m_ticket_printed.WaitOne(30000))
        {
          Thread.Sleep(m_printer_device.PrintTime);
        }
        TitoPrinterGetStatus(false);
      }
    }

    #endregion

    #region "Methods"
    //------------------------------------------------------------------------------
    // PURPOSE: Get the printer status
    // 
    //  PARAMS:
    //      - INPUT:
    //          - WaitForReady
    //              True: the print process REAL_TIME
    //              False; the print process is ASYNCHRONOUS
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static void TitoPrinterGetStatus(Boolean WaitForReady)
    {
      PrinterStatus _status;
      String _status_txt;
      Boolean _is_ready;
      int _offset_tick;
      Boolean _in_path;
      Boolean _in_path_and_paper_low;

      // Wait while the ticket is printing, and the new status is updated
      _offset_tick = WSI.Common.Misc.GetTickCount();
      _in_path = false;
      _in_path_and_paper_low = false;

      // Wait time to retry to get the printer status: 5 sec
      // If the printer status is ticket in path, we need to retry to get the new printer status
      while (Misc.GetElapsedTicks(_offset_tick) < m_wait_printer_response_seconds * 1000 || m_device_status == PrinterStatus.TicketInPath)
      {
        Thread.Sleep(100);
        m_device_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);

        m_printer_device.GetStatus(out _is_ready, out _status, out _status_txt);

        // Save Status
        m_data_lock.AcquireWriterLock(Timeout.Infinite);
        m_device_status = _status;
        m_device_status_text = _status_txt;
        m_device_is_ready = _is_ready;
        m_data_lock.ReleaseWriterLock();

        m_device_lock.ReleaseWriterLock();

        // In this case the printer status is ready
        if (m_device_status == PrinterStatus.TicketInPath ||
            WaitForReady)
        {
          _in_path = true;
        }
        // In this case is necessary to retry to get the status printer, the next status can be 'out of ticket'
        if (m_device_status == PrinterStatus.TicketInPathAndPaperLow)
        {
          _in_path = true;
          _in_path_and_paper_low = true;
        }

        if (_in_path || !WaitForReady)
        {
          if (!_in_path_and_paper_low || !WaitForReady)
          {
            break;
          }
          // Retry for last time
          WaitForReady = true;
        }
      }
      
      // Indicates the status is available for reading
      m_status_available.Set();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Init TitoPrinter
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TicketInfo Ticket info
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      True , init ok 
    //      False otherwise.
    // 
    //   NOTES:
    //    
    public static Boolean Init(TitoTicketDefault TicketTemplate)
    {
      Thread _thread;

      m_ticket_template = TicketTemplate;


      if (Environment.GetEnvironmentVariable("LKS_VC_DEV") != null)
      {
        m_printer_device = new TitoPrinter_Null();

      }
      else
      {
        m_printer_device = new TitoPrinter_Epic950();
      }

      if (!m_printer_device.Init())
      {
        return false;
      }

      _thread = new Thread(TitoPrinterStatusThread);
      _thread.Name = "TitoPrinterStatusThread";
      _thread.Start();

      return true;
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE: Print ticket
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int64 Barcode
    //          - Int64 Sequence
    //          - ticketType kind of ticket.
    //          - Decimal Amount
    //          - String ticket title
    //          - DateTime PrintDate
    //          - DateTime Expiration
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      True if ticket printed
    //      False otherwise
    // 
    //   NOTES:
    //    
    public static Boolean PrintTicket(Int64 BarCode, Int32 Sequence, TITO_TICKET_TYPE Type, Decimal Amount, DateTime PrintDate, DateTime Expiration, String Address1, String Address2)
    {
      return PrintTicket(BarCode, Sequence, Type, Amount, PrintDate, Expiration, TITO_PRINTER_PRINT_MODE.ASYNCHRONOUS, Address1, Address2);
    }

    public static Boolean PrintTicket(Int64 BarCode, Int32 Sequence, TITO_TICKET_TYPE Type, Decimal Amount, DateTime PrintDate, DateTime Expiration, CashierSessionInfo CashierSessionInfo)
    {
      return PrintTicket(BarCode, Sequence, Type, Amount, PrintDate, Expiration, TITO_PRINTER_PRINT_MODE.ASYNCHRONOUS, CashierSessionInfo);
    }

    public static Boolean PrintTicket(Int64 BarCode, Int32 Sequence, TITO_TICKET_TYPE Type, Decimal Amount, DateTime PrintDate, DateTime Expiration, TITO_PRINTER_PRINT_MODE PrintMode, CashierSessionInfo CashierSessionInfo)
    {
      String _address_1;
      String _address_2;
      Terminal.TerminalInfo _terminal_info;

      _terminal_info = new Terminal.TerminalInfo();

      // Cashier: Has only termninaID and TerminalName
      _terminal_info.TerminalId = CashierSessionInfo.TerminalId;
      _terminal_info.Name = CashierSessionInfo.TerminalName;

      _address_1 = WSI.Common.TITO.Utils.GetValueAddres(_terminal_info, 1);
      _address_2 = WSI.Common.TITO.Utils.GetValueAddres(_terminal_info, 2);

      return PrintTicket(BarCode, Sequence, Type, Amount, PrintDate, Expiration, PrintMode, _address_1, _address_2);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Print ticket
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Int64 Barcode
    //          - Int64 Sequence
    //          - ticketType kind of ticket.
    //          - Decimal Amount
    //          - String ticket title
    //          - DateTime PrintDate
    //          - DateTime Expiration
    //          - Print Mode
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      True if ticket printed
    //      False otherwise
    // 
    //   NOTES:
    //    
    private static Boolean PrintTicket(Int64 BarCode, Int32 Sequence, TITO_TICKET_TYPE Type, Decimal Amount, DateTime PrintDate, DateTime Expiration, TITO_PRINTER_PRINT_MODE PrintMode, String Address1, String Address2)
    {
      try
      {
        String _format_date;
        String _format_hour;

        if (m_printer_device == null)
        {
          Log.Error("PrintTicket. Printer not initialized!");

          return false;
        }

        if (!IsReady())
        {
          Log.Error("Tito Printer is not ready. Status: " + m_device_status.ToString() + "Error: " + m_device_status_text);

          return false;
        }

        if (BarCode > 999999999999999999 || BarCode < 0)
        {
          Log.Error("PrintTicket. Error Barcode not valid. Barcode: " + BarCode.ToString());

          return false;
        }

        if (Expiration == DateTime.MinValue)
        {
          Log.Error("PrintTicket. Expiration date not defined.");

          return false;
        }

        m_ticket_info = new TicketInfo(m_ticket_template);

        // DHA 13-FEB-2014: read general params before printing the ticket
        m_ticket_info.StrLocation = GeneralParam.GetString("TITO", "Tickets.Location", "");
        m_ticket_info.StrAddress1 = Address1; //GeneralParam.GetString("TITO", "Tickets.Address1", "");
        m_ticket_info.StrAddress2 = Address2; //GeneralParam.GetString("TITO", "Tickets.Address2", "");
        m_ticket_info.LblSequence = GeneralParam.GetString("TITO", "TicketInfo.LblSequence", "TICKET# ");
        m_ticket_info.LblMachine = GeneralParam.GetString("TITO", "TicketInfo.LblMachine", "MACHINE# ");
        m_ticket_info.LblExpiration = GeneralParam.GetString("TITO", "TicketInfo.LblExpiration", "Ticket Void after ");
        m_ticket_info.LblValidationNumber = GeneralParam.GetString("TITO", "TicketInfo.LblValidation", "VALIDATION ");
        _format_date = GeneralParam.GetString("TITO", "TicketInfo.FormatDate", Format.DateTimeCustomFormatString(false));
        _format_hour = GeneralParam.GetString("TITO", "TicketInfo.FormatTime", Format.TimeCustomFormatString(true));
        m_ticket_info.StrTitle = "";
        m_ticket_info.StrSequence = Sequence.ToString("0000");
        m_ticket_info.TicketType = Type;
        m_ticket_info.StrTitle = m_ticket_info.GetNameTicketType();
        m_ticket_info.StrBarcodeNumber = BarCode.ToString("000000000000000000");
        m_ticket_info.StrValidationNumber = BarCode.ToString("00-0000-0000-0000-0000");
        m_ticket_info.Amount = Amount;
        m_ticket_info.StrAmount = Amount.ToString("C");
        m_ticket_info.StrAmountLetter = NumberToLetter.CurrencyString(Amount);

        m_ticket_info.Expiration = Expiration;
        m_ticket_info.StrExpiration = String.Format("{0:" + _format_date + "}", Expiration);
        m_ticket_info.Date = PrintDate;
        m_ticket_info.StrDate = String.Format("{0:" + _format_date + "}", PrintDate);
        m_ticket_info.StrTime = String.Format("{0:" + _format_hour + "}", PrintDate);

        try
        {
          m_device_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
          if (!IsReady())
          {
            return false;
          }
          if (!m_printer_device.PrintTicket(m_ticket_info))
          {
            Log.Error("PrintTicket. Error in print. TicketSequence:" + m_ticket_info.StrSequence);

            return false;
          }
        }
        finally
        {
          if (PrintMode == TITO_PRINTER_PRINT_MODE.REAL_TIME)
          {
            TitoPrinterGetStatus(true);
          }
          else
          {
            m_ticket_printed.Set();
          }
          m_device_lock.ReleaseWriterLock();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // PrintTicket

    //------------------------------------------------------------------------------
    // PURPOSE: Printer is ready 
    // 
    //  PARAMS:
    //      - INPUT:    
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      True , if printer is ready
    //      False otherwise.
    // 
    //   NOTES:
    // 
    public static Boolean IsReady()
    {
      try
      {
        m_data_lock.AcquireReaderLock(System.Threading.Timeout.Infinite);

        return m_device_is_ready;
      }
      finally
      {
        m_data_lock.ReleaseReaderLock();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Status and status description
    // 
    //  PARAMS:
    //      - INPUT:    
    //
    //      - OUTPUT:
    //          - Status
    //          - StatusText
    //
    // RETURNS:
    //
    //   NOTES:
    //    
    public static void GetStatus(out PrinterStatus Status, out String StatusText)
    {
      try
      {
        m_data_lock.AcquireReaderLock(System.Threading.Timeout.Infinite);

        Status = m_device_status;
        StatusText = m_device_status_text;
      }
      finally
      {
        m_data_lock.ReleaseReaderLock();
      }
    }

    public static String GetMsgError(PrinterStatus Status)
    {
      switch (Status)
      {
        case PrinterStatus.OutOfPaper:

          return Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_PRINTER_NO_PAPER");

        case PrinterStatus.NotOpenPort:

          return Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_PRINTER_LOST_CONNECTION");

        default:

          return Resource.String("STR_FRM_CONTAINER_EVENTS_DESC_PRINTER_ERROR");
      }
    }

    /// <summary>
    /// Set to N seconds for waiting IsReady Status Refreshed
    /// </summary>
    public static void WaitOneStatusAvailable(Int32 Milliseconds)
    {
      m_status_available.WaitOne(Milliseconds);
    } // SetWaitOneStatusAvailable

    /// <summary>
    /// Force to Execute a Thread (TitoPrinterStatusThread())
    /// </summary>
    public static void SetTicketPrinted()
    {
      m_ticket_printed.Set();
    } // SetTicketPrinted

    #endregion
  }
}
