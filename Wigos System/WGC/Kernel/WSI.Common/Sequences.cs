using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common
{
  public static class Sequences
  {
    public static Boolean GetValue(SqlTransaction SqlTrx, SequenceId SequenceId, out Int64 Value)
    {
      StringBuilder _sb;

      Value = 0;

      if (SequenceId == SequenceId.NotSet)
      {
        return false;
      }

      _sb = new StringBuilder();
      _sb.AppendLine("IF NOT EXISTS ( SELECT SEQ_NEXT_VALUE FROM SEQUENCES WHERE SEQ_ID = @pSeqId ) ");
      _sb.AppendLine("  INSERT INTO SEQUENCES VALUES (@pSeqId, 1); ");
      _sb.AppendLine(" ");
      _sb.AppendLine("UPDATE   SEQUENCES ");
      _sb.AppendLine("   SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1 ");
      _sb.AppendLine("OUTPUT   DELETED.SEQ_NEXT_VALUE ");
      _sb.AppendLine(" WHERE   SEQ_ID    = @pSeqId ");
      
      try 
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pSeqId", SqlDbType.Int).Value = SequenceId;
          
          Value = (Int64)_sql_cmd.ExecuteScalar();
          
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    } // GetValue

    public static Boolean GetSequenceFromPlaySession(SqlTransaction SqlTrx, Int64 PlaySession, out Int64 Value)
    {
      StringBuilder _sb;

      Value = 0;

      _sb = new StringBuilder();
      _sb.AppendLine("        SELECT   PS_SEQUENCE                        ");
      _sb.AppendLine("          FROM   PLAY_SESSIONS                      ");
      _sb.AppendLine("         WHERE   PS_PLAY_SESSION_ID = @pPlaySession ");

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pPlaySession", SqlDbType.Int).Value = PlaySession;

          Value = (Int64)_sql_cmd.ExecuteScalar();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    } // GetSequenceFromPlaySession
  }
}
