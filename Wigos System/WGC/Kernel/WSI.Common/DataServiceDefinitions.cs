﻿using System;
using System.Collections.Generic;
// using System.Linq;
using System.Text;
using System.Data;

namespace WSI.Common
{




  // JML 15-OCT-2012
  // RXM 23-OCT-2012
  public enum REPORT_STATUS
  {
    NO_STATUS = -1,
    NOT_RECEIVED = 0,
    RECEIVED = 1,
    VALIDATED_TO_GENERATE = 2,
    GENERATED_OK = 3,
    GENERATED_WARNING = 4,
    VALIDATED_TO_SEND = 5,
    SENT_SAT = 6,
    ACCEPTED_SAT = 7,
    REJECTED_SAT = 8,
    ERROR_GENERATING = 9,
    ERROR_SENDING = 10,
    VALIDATED_TO_SEND_MANUALLY = 11,
    RECEIVED_WITH_ERRORS = 12
  }

  //JCA 01-FEB-2013
  public enum SUBLINE_RECEIVED_BY
  {
    NOT_RECEIVED = 0,
    LINE = 1,
    SUBLINE = 2,
  }
  // JML 18-OCT-2012
  public enum TIPO_XML
  {
    CATALOGO = 1,
    REPORTE = 2,
    CONSTANCIA = 3
  }

  // JML 18-OCT-2012
  public enum TIPO_ENVIO
  {   
   NORMAL = 'N',
   SUSTITUTIVO = 'S'
  }



  // QMP 12-NOV-2012
  public enum MONITORING_STATUS
  {
    NOT_RECEIVED = 0,
    RECEIVED = 1
  }

  public enum MONITORING_TYPE
  {
    SESSION = 1,
    STATISTICS = 2
  }

  public enum SEND_STATUS_SAT
  {
    NoStatus = -1,
    NoEnviado = 0,
    Recibido = 1,
    Aceptado = 2,
    AceptadoEnTiempo = 3,
    AceptadoExtemporaneo = 4,
    Rechazado = 5
  }

  // QMP 19-NOV-2012
  public enum GAME_SESSION_STATUS
  {
    OPEN = 0,
    CLOSED = 1,
    ABANDONED = 2,
    MANUAL_CLOSED = 3,
    CANCELLED = 4,
    HANDPAY = 5,
    HANDPAY_CANCEL = 6
  }

  public enum CATALOG_LIFECYCLE_STATUS
  {
    NO_STATUS = -1,
    PENDING_NEW_CHANGES = 0,
    VALIDATED_TO_GENERATE = 1,
    GENERATED_OK = 2,
    GENERATED_WARNING = 3,
    ERROR_GENERATING = 4,
    VALIDATED_TO_SEND = 5,
    SENT_SAT = 6,
    ERROR_SENDING = 7,
    ACCEPTED_SAT = 8,
    REJECTED_SAT = 9,
    VALIDATED_TO_SEND_MANUALLY = 10,
  }

  //JCA 14-JAN-2013
  public enum DATA_WS_NET_MSG_LOG
  {
    NO_LOG = 0,
    ALL = 1,
    BY_LOGIN = 2,
    BY_LINE = 3
  }

  // JML 11-FEB-2013
  public enum CONSTANCIA_ERRORES
  { 
    NONE = 0,
    RFC = 1,
    CURP = 2,
    POSTALCODE = 4
  }

  // JCA 08-APR-2013
  public enum ALESIS_STATUS_SEND
  {
    NONE = 0,
    SUCCES = 1,
    ERROR_SEND = 2,
  }

}
