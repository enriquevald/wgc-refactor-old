//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MetersGroup.cs 
// 
//   DESCRIPTION: MetersGroup class
//  
//        AUTHOR: David Hern�ndez & Jos� Martinez
// 
// CREATION DATE: 27-APR-2015
// 
// REVISION HISTORY:
// 
// Date        Author      Description
// ----------- ----------- -----------------------------------------------------
// 27-APR-2015 DHA & JML   First version.
// 04-MAY-2015 DHA & JML   Fase 3.
// 07-SEP-2015 MPO         TFS ITEM 2194: SAS16: Estad�sticas multi-denominaci�n: WCP
// 14-SEP-2015 DHA         Product Backlog Item 3707: Added meter coins from terminals
// 19-NOV-2015 DHA         Product Backlog Item 6376: Modified meter coins type from terminals
// 02-MAR-2016 ETP         PBI 10190: Hide multi-denominations menu
// 04-MAR-2016 ETP         PBI 10190: Hide multi-denominations menu corrected.
// 02-MAR-2016 ETP         PBI 10190: Hide multi-denominations menu
// 29-OCT-2017 LTC         [WIGOS-6134] Multisite Terminal meter adjustment
// 02-May-2018 JGC         Bug 32500:[WIGOS-10553]: When trying to adjust meters, a controlled error is shown but an unhandled exception is reported in the WigosGUI log file.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common
{

  public static class TerminalMeterGroup
  {

    #region CONST

    private const String TABLE_NAME_METERS = "Meters";
    private const String TABLE_NAME_METERS_HISTORY = "Meters_Ajusments_History";
    private const String TABLE_TERMINAL_SAS_METERS_HISTORY = "terminal_sas_meters_history";
    private const Int32 DECIMAL_LENGTH = 2;
    private const Int32 NUM_HOURS_DAY = 24;

    #endregion

    #region ENUMS

    public enum MetersAdjustmentsType
    {
      Manual = 0,
      System = 1
    } // MetersAdjustmentsType

    public enum MetersAdjustmentsSubType
    {
      NotApply = 0,
      System_Machine_BigIncrement = 1,
      System_Machine_Reset = 2,
      System_Machine_Rollover = 3,
      System_Game_BigIncrement = 4,
      System_Game_Reset = 5,
      System_Game_Rollover = 6,
      System_SAS_BigIncrement = 7,
      System_SAS_Reset = 8,
      System_SAS_Rollover = 9
    } // MetersAdjustmentsSubType

    public enum MeterGroups
    {
      GroupStatistics = 0,      // Statistics Group.
      GroupElectronicsFundsTransfer = 1,      // Electronics Funds Transfer Group.
      GroupTickets = 2,
      GroupBills = 3,
      //GroupX = X,      // X Group.
    } // MeterGroups

    public enum MeterAjustmentReason
    {
      others = 0,
    } // MeterAjustmentReason

    public enum ResultCode
    {
      OK = 0,
      ERROR_GENERIC = 1,
      ERROR_NEVER_ACTIVITY = 2,
    } // ResultCode

    #endregion // ENUMS

    #region STRUCT

    public struct COL_METERS
    {
      public static readonly int TERMINAL_ID = 0;
      public static readonly int TERMINAL_NAME = 1;
      public static readonly int GAMING_DAY = 2;
      public static readonly int METER_CODE = 3;
      public static readonly int METER_NAME = 4;
      public static readonly int GAME_ID = 5;
      public static readonly int DENOMINATION = 6;
      public static readonly int TYPE = 7;
      public static readonly int SUB_TYPE = 8;
      public static readonly int INITIAL_VALUE = 9;
      public static readonly int FINAL_VALUE = 10;
      public static readonly int DELTA_SYSTEM = 11;
      public static readonly int DELTA_CALCULATE = 12;
      public static readonly int USER_ID = 13;
      public static readonly int REASON = 14;
      public static readonly int REMARKS = 15;
      public static readonly int METER_MAX_VALUE = 16;
      public static readonly int ROLL_OVER = 17;
      public static readonly int GRID_INDEX = 18;
      public static readonly int BIG_INCREMENT = 19;
      public static readonly int MAX_INCREMENT = 20;
      public static readonly int IS_NEW_ROW = 21;
      public static readonly int IS_VISIBLE = 22;
      public static readonly int ONLY_DELTA = 23;
      public static readonly int ORDER = 24;
    } // COL_METERS

    public struct COL_METERS_HISTORY
    {
      public static readonly int TERMINAL_ID = 0;
      public static readonly int TERMINAL_NAME = 1;
      public static readonly int GAMING_DAY = 2;
      public static readonly int METER_CODE = 3;
      public static readonly int GAME_ID = 4;
      public static readonly int DENOMINATION = 5;
      public static readonly int UNIQUE_ID = 6;
      public static readonly int DATETIME = 7;
      public static readonly int TYPE = 8;
      public static readonly int SUB_TYPE = 9;
      public static readonly int OLD_INITIAL_VALUE = 10;
      public static readonly int OLD_FINAL_VALUE = 11;
      public static readonly int OLD_DELTA_SYSTEM = 12;
      public static readonly int OLD_DELTA_CALCULATE = 13;
      public static readonly int NEW_INITIAL_VALUE = 14;
      public static readonly int NEW_FINAL_VALUE = 15;
      public static readonly int NEW_DELTA_SYSTEM = 16;
      public static readonly int NEW_DELTA_CALCULATE = 17;
      public static readonly int USER_ID = 18;
      public static readonly int USER_NAME = 19;
      public static readonly int REASON = 20;
      public static readonly int REMARKS = 21;
    } // COL_METERS_HISTORY 

    public struct COL_METERS_TO_UPDATE
    {
      public static readonly int TERMINAL_ID = 0;
      public static readonly int METER_CODE = 1;
      public static readonly int GAME_ID = 2;
      public static readonly int DENOMINATION = 3;
      public static readonly int TYPE = 4;
      public static readonly int DATETIME = 5;
      public static readonly int METER_INI_VALUE = 6;
      public static readonly int METER_FIN_VALUE = 7;
      public static readonly int METER_INCREMENT = 8;
    }  // COL_METERS_TO_UPDATE

    public struct COL_METERS_GENERIC
    {
      public static readonly int TERMINAL_ID = 0;
      public static readonly int GAME_ID = 1;
      public static readonly int DENOMINATION = 2;
      public static readonly int DATETIME = 3;
    } // COL_METERS_GENERIC

    #endregion  // STRUCT

    #region "MEMBERS"

    // LTC  29-OCT-2017
    public static String SiteId { get; set; }

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE: Get Meter 
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - Meter
    //
    public static Meter GetMeter(Int32 Code, List<Meter> Meters)
    {
      foreach (Meter _meter in Meters)
      {
        if (_meter.Code == Code)
        {
          return _meter;
        }
      }
      return null;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Create general meter DataTable
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - DataTable structure
    //
    public static DataTable CreateMetersGroupDataTable()
    {
      DataTable MetersGroupTable;

      MetersGroupTable = new DataTable();

      MetersGroupTable.TableName = TABLE_NAME_METERS;

      MetersGroupTable.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32")).AllowDBNull = false;                 //col 00
      MetersGroupTable.Columns.Add("TERMINAL_NAME", Type.GetType("System.String")).AllowDBNull = false;              //col 01
      MetersGroupTable.Columns.Add("GAMING_DAY", Type.GetType("System.DateTime")).AllowDBNull = false;               //col 02
      MetersGroupTable.Columns.Add("METER_CODE", Type.GetType("System.Int64")).AllowDBNull = false;                  //col 03
      MetersGroupTable.Columns.Add("METER_NAME", Type.GetType("System.String")).AllowDBNull = true;                  //col 04
      MetersGroupTable.Columns.Add("GAME_ID", Type.GetType("System.Int32")).AllowDBNull = false;                     //col 05
      MetersGroupTable.Columns.Add("DENOMINATION", Type.GetType("System.Decimal")).AllowDBNull = false;              //col 06
      MetersGroupTable.Columns.Add("TYPE", Type.GetType("System.Int32")).AllowDBNull = false;                        //col 07
      MetersGroupTable.Columns.Add("SUB_TYPE", Type.GetType("System.Int32")).AllowDBNull = false;                    //col 08
      MetersGroupTable.Columns.Add("INITIAL_VALUE", Type.GetType("System.Int64")).AllowDBNull = true;                //col 09
      MetersGroupTable.Columns.Add("FINAL_VALUE", Type.GetType("System.Int64")).AllowDBNull = true;                  //col 10
      MetersGroupTable.Columns.Add("DELTA_SYSTEM", Type.GetType("System.Int64")).AllowDBNull = true;                 //col 11
      MetersGroupTable.Columns.Add("DELTA_CALCULATE", Type.GetType("System.Int64")).AllowDBNull = true;              //col 12
      MetersGroupTable.Columns.Add("USER_ID", Type.GetType("System.Int32")).AllowDBNull = false;                     //col 13
      MetersGroupTable.Columns.Add("REASON", Type.GetType("System.Int32")).AllowDBNull = false;                      //col 14
      MetersGroupTable.Columns.Add("REMARKS", Type.GetType("System.String")).AllowDBNull = true;                     //col 15
      MetersGroupTable.Columns.Add("METER_MAX_VALUE", Type.GetType("System.Int64")).AllowDBNull = false;             //col 16
      MetersGroupTable.Columns.Add("ROLL_OVER", Type.GetType("System.Int32")).AllowDBNull = false;                   //col 17
      MetersGroupTable.Columns.Add("GRID_INDEX", Type.GetType("System.Int32")).AllowDBNull = false;                  //col 18
      MetersGroupTable.Columns.Add("IS_BIG_INCREMENT", Type.GetType("System.Int32")).AllowDBNull = false;            //col 19
      MetersGroupTable.Columns.Add("MAX_INCREMENT", Type.GetType("System.Int64")).AllowDBNull = false;               //col 20
      MetersGroupTable.Columns.Add("IS_NEW_ROW", Type.GetType("System.Int32")).AllowDBNull = false;                  //col 21
      MetersGroupTable.Columns.Add("IS_VISIBLE", Type.GetType("System.Int32")).AllowDBNull = false;                  //col 22
      MetersGroupTable.Columns.Add("ONLY_DELTA", Type.GetType("System.Int32")).AllowDBNull = false;                  //col 23
      MetersGroupTable.Columns.Add("ORDER", Type.GetType("System.Int32")).AllowDBNull = false;                       //col 24

      //MetersGroupTable.Columns["terminal_id"].DefaultValue = 0;                 //col 00
      //MetersGroupTable.Columns["terminal_name"].DefaultValue = "";              //col 01
      //MetersGroupTable.Columns["gaming_day"].DefaultValue = WGDB.Now;           //col 02
      //MetersGroupTable.Columns["meter_code"].DefaultValue = 0;                  //col 03
      //MetersGroupTable.Columns["meter_name"].DefaultValue = "";                 //col 04
      MetersGroupTable.Columns["game_id"].DefaultValue = 0;                     //col 05
      MetersGroupTable.Columns["denomination"].DefaultValue = 0;                //col 06  
      MetersGroupTable.Columns["type"].DefaultValue = 0;                        //col 07
      MetersGroupTable.Columns["sub_type"].DefaultValue = 0;                    //col 08
      MetersGroupTable.Columns["initial_value"].DefaultValue = 0;               //col 09
      MetersGroupTable.Columns["final_value"].DefaultValue = 0;                 //col 10
      MetersGroupTable.Columns["delta_system"].DefaultValue = 0;                //col 11
      MetersGroupTable.Columns["delta_calculate"].DefaultValue = 0;             //col 12
      MetersGroupTable.Columns["user_id"].DefaultValue = 0;                     //col 13
      MetersGroupTable.Columns["reason"].DefaultValue = 0;                      //col 14
      // MetersGroupTable.Columns["remarks"].DefaultValue = "";                   //col 15
      MetersGroupTable.Columns["meter_max_value"].DefaultValue = 0;             //col 16
      MetersGroupTable.Columns["roll_over"].DefaultValue = 0;                   //col 17
      MetersGroupTable.Columns["grid_index"].DefaultValue = 0;                  //col 18
      MetersGroupTable.Columns["is_big_increment"].DefaultValue = 0;            //col 19
      MetersGroupTable.Columns["max_increment"].DefaultValue = 0;               //col 20
      MetersGroupTable.Columns["is_visible"].DefaultValue = true;               //col 22
      MetersGroupTable.Columns["only_delta"].DefaultValue = false;              //col 23
      MetersGroupTable.Columns["ORDER"].DefaultValue = 0;                       //col 24

      return MetersGroupTable;
    } // CreateMetersGroupDataTable

    //------------------------------------------------------------------------------
    // PURPOSE: Create history meter DataTable
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - DataTable structure
    //
    public static DataTable CreateHistoryMetersGroupDataTable()
    {

      DataTable HistoryMetersGroupTable;

      HistoryMetersGroupTable = new DataTable();

      HistoryMetersGroupTable.TableName = TABLE_NAME_METERS_HISTORY;

      HistoryMetersGroupTable.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32")).AllowDBNull = false;                 //col 00
      HistoryMetersGroupTable.Columns.Add("TERMINAL_NAME", Type.GetType("System.String")).AllowDBNull = false;              //col 01
      HistoryMetersGroupTable.Columns.Add("GAMING_DAY", Type.GetType("System.DateTime")).AllowDBNull = false;               //col 02
      HistoryMetersGroupTable.Columns.Add("METER_CODE", Type.GetType("System.Int32")).AllowDBNull = false;                  //col 03
      HistoryMetersGroupTable.Columns.Add("GAME_ID", Type.GetType("System.Int32")).AllowDBNull = false;                     //col 04
      HistoryMetersGroupTable.Columns.Add("DENOMINATION", Type.GetType("System.Decimal")).AllowDBNull = false;              //col 05
      HistoryMetersGroupTable.Columns.Add("UNIQUE_ID", Type.GetType("System.Int64")).AllowDBNull = true;                    //col 06
      HistoryMetersGroupTable.Columns.Add("DATETIME", Type.GetType("System.DateTime")).AllowDBNull = false;                 //col 07
      HistoryMetersGroupTable.Columns.Add("TYPE", Type.GetType("System.Int32")).AllowDBNull = false;                        //col 08
      HistoryMetersGroupTable.Columns.Add("SUB_TYPE", Type.GetType("System.Int32")).AllowDBNull = false;                    //col 09
      HistoryMetersGroupTable.Columns.Add("OLD_INITIAL_VALUE", Type.GetType("System.Int64")).AllowDBNull = false;           //col 10
      HistoryMetersGroupTable.Columns.Add("OLD_FINAL_VALUE", Type.GetType("System.Int64")).AllowDBNull = false;             //col 11
      HistoryMetersGroupTable.Columns.Add("OLD_DELTA_SYSTEM", Type.GetType("System.Int64")).AllowDBNull = false;            //col 12
      HistoryMetersGroupTable.Columns.Add("OLD_DELTA_CALCULATE", Type.GetType("System.Int64")).AllowDBNull = false;         //col 13
      HistoryMetersGroupTable.Columns.Add("NEW_INITIAL_VALUE", Type.GetType("System.Int64")).AllowDBNull = false;           //col 14
      HistoryMetersGroupTable.Columns.Add("NEW_FINAL_VALUE", Type.GetType("System.Int64")).AllowDBNull = false;             //col 15
      HistoryMetersGroupTable.Columns.Add("NEW_DELTA_SYSTEM", Type.GetType("System.Int64")).AllowDBNull = false;            //col 16
      HistoryMetersGroupTable.Columns.Add("NEW_DELTA_CALCULATE", Type.GetType("System.Int64")).AllowDBNull = false;         //col 17
      HistoryMetersGroupTable.Columns.Add("USER_ID", Type.GetType("System.Int32")).AllowDBNull = false;                     //col 18
      HistoryMetersGroupTable.Columns.Add("USER_NAME", Type.GetType("System.String")).AllowDBNull = false;                  //col 19
      HistoryMetersGroupTable.Columns.Add("REASON", Type.GetType("System.Int32")).AllowDBNull = false;                      //col 20
      HistoryMetersGroupTable.Columns.Add("REMARKS", Type.GetType("System.String")).AllowDBNull = true;                     //col 21

      //HistoryMetersGroupTable.Columns["terminal_id"].DefaultValue = 0;                 //col 00
      //HistoryMetersGroupTable.Columns["terminal_name"].DefaultValue = "";              //col 01
      //HistoryMetersGroupTable.Columns["gaming_day"].DefaultValue = WGDB.Now;           //col 02
      //HistoryMetersGroupTable.Columns["meter_code"].DefaultValue = 0;                  //col 03
      //HistoryMetersGroupTable.Columns["game_id"].DefaultValue = 0;                     //col 04
      //HistoryMetersGroupTable.Columns["denomination"].DefaultValue = 0;                //col 05
      //HistoryMetersGroupTable.Columns["unique_id"].DefaultValue = 0;                   //col 06
      HistoryMetersGroupTable.Columns["datetime"].DefaultValue = WGDB.Now;             //col 07
      HistoryMetersGroupTable.Columns["type"].DefaultValue = 0;                        //col 08
      HistoryMetersGroupTable.Columns["sub_type"].DefaultValue = 0;                    //col 09
      HistoryMetersGroupTable.Columns["old_initial_value"].DefaultValue = 0;           //col 10
      HistoryMetersGroupTable.Columns["old_final_value"].DefaultValue = 0;             //col 11
      HistoryMetersGroupTable.Columns["old_delta_system"].DefaultValue = 0;            //col 12
      HistoryMetersGroupTable.Columns["old_delta_calculate"].DefaultValue = 0;         //col 13
      HistoryMetersGroupTable.Columns["new_initial_value"].DefaultValue = 0;           //col 14
      HistoryMetersGroupTable.Columns["new_final_value"].DefaultValue = 0;             //col 15
      HistoryMetersGroupTable.Columns["new_delta_system"].DefaultValue = 0;            //col 16
      HistoryMetersGroupTable.Columns["new_delta_calculate"].DefaultValue = 0;         //col 17
      HistoryMetersGroupTable.Columns["user_id"].DefaultValue = 0;                     //col 18
      HistoryMetersGroupTable.Columns["user_name"].DefaultValue = "";                  //col 19
      HistoryMetersGroupTable.Columns["reason"].DefaultValue = 0;                      //col 20
      // HistoryMetersGroupTable.Columns["remarks"].DefaultValue = "";                   //col 21


      return HistoryMetersGroupTable;
    } // CreateHistoryMetersGroupDataTable 

    //------------------------------------------------------------------------------
    // PURPOSE: Create terminal sas meter history DataTable
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - DataTable structure
    //
    public static DataTable CreateTerminalSasMeterHistory()
    {
      DataTable TerminalSasMeterHistory;

      TerminalSasMeterHistory = new DataTable();

      TerminalSasMeterHistory.TableName = TABLE_TERMINAL_SAS_METERS_HISTORY;

      TerminalSasMeterHistory.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32")).AllowDBNull = false;                //col 00
      TerminalSasMeterHistory.Columns.Add("METER_CODE", Type.GetType("System.Int32")).AllowDBNull = false;                 //col 01
      TerminalSasMeterHistory.Columns.Add("GAME_ID", Type.GetType("System.Int32")).AllowDBNull = false;                    //col 02
      TerminalSasMeterHistory.Columns.Add("DENOMINATION", Type.GetType("System.Decimal")).AllowDBNull = false;             //col 03
      TerminalSasMeterHistory.Columns.Add("TYPE", Type.GetType("System.Int32")).AllowDBNull = false;                       //col 04
      TerminalSasMeterHistory.Columns.Add("DATETIME", Type.GetType("System.DateTime")).AllowDBNull = false;                //col 05
      TerminalSasMeterHistory.Columns.Add("METER_INI_VALUE", Type.GetType("System.Int64")).AllowDBNull = true;             //col 06
      TerminalSasMeterHistory.Columns.Add("METER_FIN_VALUE", Type.GetType("System.Int64")).AllowDBNull = false;            //col 07
      TerminalSasMeterHistory.Columns.Add("METER_INCREMENT", Type.GetType("System.Int64")).AllowDBNull = false;            //col 08

      TerminalSasMeterHistory.Columns["TERMINAL_ID"].DefaultValue = 0;                 //col 00
      TerminalSasMeterHistory.Columns["METER_CODE"].DefaultValue = 0;                  //col 01
      TerminalSasMeterHistory.Columns["GAME_ID"].DefaultValue = 0;                     //col 02
      TerminalSasMeterHistory.Columns["DENOMINATION"].DefaultValue = 0;                //col 03
      TerminalSasMeterHistory.Columns["TYPE"].DefaultValue = 0;                        //col 04
      TerminalSasMeterHistory.Columns["DATETIME"].DefaultValue = WGDB.Now;             //col 05
      TerminalSasMeterHistory.Columns["METER_INI_VALUE"].DefaultValue = 0;             //col 06
      TerminalSasMeterHistory.Columns["METER_FIN_VALUE"].DefaultValue = 0;             //col 07
      TerminalSasMeterHistory.Columns["METER_INCREMENT"].DefaultValue = 0;             //col 08

      return TerminalSasMeterHistory;

    } // CreateTerminalSasMeterHistory

    //------------------------------------------------------------------------------
    // PURPOSE: Create generic meter DataTable
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - DataTable structure
    //
    public static DataTable CreateMeterTableGeneric(MeterRelationsBD MeterRelationsDB)
    {
      DataTable SasMetersUpdates;

      SasMetersUpdates = new DataTable();

      SasMetersUpdates.TableName = TABLE_TERMINAL_SAS_METERS_HISTORY;

      SasMetersUpdates.Columns.Add("TERMINAL_ID", Type.GetType("System.Int32")).AllowDBNull = false;                //col 00
      SasMetersUpdates.Columns.Add("GAME_ID", Type.GetType("System.Int32")).AllowDBNull = false;                    //col 01
      SasMetersUpdates.Columns.Add("DENOMINATION", Type.GetType("System.Decimal")).AllowDBNull = false;             //col 02
      SasMetersUpdates.Columns.Add("DATETIME", Type.GetType("System.DateTime")).AllowDBNull = false;                //col 03
      // for each meter of a list
      foreach (KeyValuePair<Int64, List<MeterRelationBD>> _meter_relations in MeterRelationsDB.MeterRelations)
      {
        foreach (MeterRelationBD _meter_relation in _meter_relations.Value)
        {
          SasMetersUpdates.Columns.Add(_meter_relation.FieldName, Type.GetType(_meter_relation.FieldType)).AllowDBNull = true;
        }
      }

      SasMetersUpdates.Columns["TERMINAL_ID"].DefaultValue = 0;                 //col 00
      SasMetersUpdates.Columns["GAME_ID"].DefaultValue = 0;                     //col 01
      SasMetersUpdates.Columns["DENOMINATION"].DefaultValue = 0;                //col 02

      return SasMetersUpdates;

    } // CreateMeterTableGeneric

    //------------------------------------------------------------------------------
    // PURPOSE: Return format meter
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    public static Decimal FormatMeterCodeValue(Meter Meter, Object Value, Type CurrentType)
    {
      if (CurrentType == typeof(decimal))
      {
        return FormatMeterCodeValue(Meter, (decimal)Value);
      }
      else if (CurrentType == typeof(Int64))
      {
        return FormatMeterCodeValue(Meter, (Int64)(Decimal)Value);
      }
      else
      {
        throw new Exception();
      }
    } // FormatMeterCodeValue

    private static Decimal FormatMeterCodeValue(Meter Meter, Int64 Value)
    {
      Decimal _converted_value;

      _converted_value = Value;

      if (Meter.type == typeof(Currency))
      {
        for (Int32 _iteration = 1; _iteration <= DECIMAL_LENGTH; _iteration++)
        {
          _converted_value = _converted_value / 10;
        }
      }
      else if (Meter.type == typeof(Int64))
      {
        // No apply conversion
      }
      else
      {
        throw new Exception();
      }

      return _converted_value;
    } // FormatMeterCodeValue

    private static Int64 FormatMeterCodeValue(Meter Meter, Decimal Value)
    {
      Decimal _converted_value;

      _converted_value = Value;

      if (Meter.type == typeof(Currency))
      {
        for (Int32 _iteration = 1; _iteration <= DECIMAL_LENGTH; _iteration++)
        {
          _converted_value = _converted_value * 10;
        }
      }
      else if (Meter.type == typeof(Int64))
      {
        // No apply conversion
      }
      else
      {
        throw new Exception();
      }

      return (Int64)_converted_value;
    } // FormatMeterCodeValue

    //------------------------------------------------------------------------------
    // PURPOSE: Read value meters for group 
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    internal static Boolean ReadMetersOfGroupByTerminalAndWorkday(Int64[] Terminals, DateTime Workday, List<Meter> Meters, out DataTable MetersGroup, String SitesId)
    {
      String _meters_codes_list;
      String _terminals_id_list;
      StringBuilder _sb;
      Int32 _first_terminal_id;

      MetersGroup = TerminalMeterGroup.CreateMetersGroupDataTable();
      _first_terminal_id = -1;

      try
      {
        _meters_codes_list = "";
        foreach (Meter _meter in Meters)
        {
          _meters_codes_list += _meter.Code.ToString() + ", ";
        }
        _meters_codes_list = _meters_codes_list.Substring(0, _meters_codes_list.Length - 2);
        //todo  ejemplo String.Join(",", _user_type_list.ToArray()

        _terminals_id_list = "";

        foreach (Int32 _terminal in Terminals)
        {
          _terminals_id_list += _terminal.ToString() + ", ";
          _first_terminal_id = _terminal; // Default value
        }
        _terminals_id_list = _terminals_id_list.Substring(0, _terminals_id_list.Length - 2);

        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   TE_TERMINAL_ID                   AS  TERMINAL_ID       ");   // col 0 
        _sb.AppendLine("        , TE_NAME                          AS  TERMINAL_NAME     ");   // col 1
        _sb.AppendLine("        , ISNULL(TSMH_DATETIME, @pWorkday) AS  GAMING_DAY        ");   // col 2
        _sb.AppendLine("        , SMC_METER_CODE                   AS  METER_CODE        ");   // col 3
        _sb.AppendLine("        , SMC_DESCRIPTION                  AS  METER_NAME        ");   // col 4
        _sb.AppendLine("        , 0                                AS  GAME_ID           ");   // col 5
        _sb.AppendLine("        , 0                                AS  DENOMINATION      ");   // col 6
        _sb.AppendLine("        , 0                                AS  TYPE              ");   // col 7
        _sb.AppendLine("        , 0                                AS  SUB_TYPE          ");   // col 8
        _sb.AppendLine("        , ISNULL(TSMH_METER_INI_VALUE, 0)  AS  INITIAL_VALUE     ");   // col 9
        _sb.AppendLine("        , ISNULL(TSMH_METER_FIN_VALUE, 0)  AS  FINAL_VALUE       ");   // col 10
        _sb.AppendLine("        , ISNULL(TSMH_METER_INCREMENT, 0)  AS  DELTA_SYSTEM      ");   // col 11
        _sb.AppendLine("        , 0                                AS  DELTA_CALCULATE   ");   // col 12
        _sb.AppendLine("        , 0                                AS  USER_ID           ");   // col 13
        _sb.AppendLine("        , 0                                AS  REASON            ");   // col 14
        _sb.AppendLine("        , ''                               AS  REMARKS           ");   // col 15
        _sb.AppendLine("        , ISNULL(TSM_METER_MAX_VALUE, 0)   AS  METER_MAX_VALUE   ");   // col 16
        _sb.AppendLine("        , 0                                AS  ROLL_OVER         ");   // col 17
        _sb.AppendLine("        , 0                                AS  GRID_INDEX        ");   // col 18
        _sb.AppendLine("        , 0                                AS  IS_BIG_INCREMENT  ");   // col 19
        _sb.AppendLine("        , 0                                AS  MAX_INCREMENT     ");   // col 20
        _sb.AppendLine("        , CASE WHEN (TSMH_TERMINAL_ID IS NULL) THEN 1 ELSE 0 END    AS IS_NEW_ROW       ");   // col 21
        _sb.AppendLine("   FROM   SAS_METERS_CATALOG        ");
        _sb.AppendFormat("   INNER   JOIN TERMINALS  ON TE_TERMINAL_ID IN ( {0} ) ", _terminals_id_list);
        _sb.AppendLine("");
        _sb.AppendLine("    LEFT   OUTER JOIN TERMINAL_SAS_METERS_HISTORY ON TSMH_TERMINAL_ID  = TE_TERMINAL_ID ");
        _sb.AppendLine("                                                 AND TSMH_METER_CODE   = SMC_METER_CODE ");
        _sb.AppendLine("                                                 AND TSMH_TYPE         = @pType         ");
        _sb.AppendLine("                                                 AND TSMH_DATETIME    = @pWorkday       ");
        _sb.AppendLine("                                                 AND TSMH_DENOMINATION     =    0       ");
        _sb.AppendLine("                                                 AND TSMH_GAME_ID     =     0           ");
        _sb.AppendLine("    LEFT   OUTER JOIN TERMINAL_SAS_METERS ON TSM_TERMINAL_ID  = TE_TERMINAL_ID ");
        _sb.AppendLine("                                         AND TSM_METER_CODE   = SMC_METER_CODE ");
        _sb.AppendLine("                                         AND TSM_DENOMINATION     =     0               ");
        _sb.AppendLine("                                         AND TSM_GAME_ID    =     0                     ");
        _sb.AppendFormat("  WHERE   SMC_METER_CODE  IN ( {0} ) ", _meters_codes_list);

        if (!String.IsNullOrEmpty(SitesId))
        {
          _sb.AppendFormat("    AND TE_SITE_ID IN ({0})", SitesId);
        }
        
        _sb.AppendLine("   ORDER   BY IS_NEW_ROW, TE_TERMINAL_ID, SMC_METER_CODE ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            //_sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = _terminals_id_list;
            _sql_cmd.Parameters.Add("@pWorkday", SqlDbType.DateTime).Value = Workday;
            _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = (Int32)ENUM_SAS_METER_HISTORY.TSMH_MINCETUR_DAILY_REPORT;
            //_sql_cmd.Parameters.Add("@pMetersGroup", SqlDbType.NVarChar).Value = _meters_codes_list;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(MetersGroup);
            }
          } // SqlCommand
          if (MetersGroup.Rows.Count > 0)
          {
            _first_terminal_id = (Int32)MetersGroup.Rows[0][0];
            FillCalculateData(_first_terminal_id, ref MetersGroup, Meters);
          }

          //FillGaps(_first_terminal_id, Workday, Meters, MetersGroup, _db_trx.SqlTransaction);
        } // DB_TRX

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // ReadMetersOfGroupByTerminalAndWorkday

    //------------------------------------------------------------------------------
    // PURPOSE: Fill calculate data fro read meters
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - DataTable structure
    //
    private static void FillCalculateData(Int32 FirstTerminalId, ref DataTable MetersGroup, List<Meter> Meters)
    {
      Int64 _delta_meter_value;
      Boolean _has_rollover;
      Int64 _max_units_increment;
      DataRow[] _rows_filter;
      Int32 _order;
     
      _order = 0;

      foreach (Meter _meter in Meters)
      {
        _rows_filter = MetersGroup.Select("METER_CODE = " + _meter.Code);

        if (_rows_filter.Length == 0)
        {
          continue;
        }

        //_meter_row = _rows_filter[0];

        foreach (DataRow _meter_data in _rows_filter)
        {
          if ((Int32)_meter_data[COL_METERS.TERMINAL_ID] == FirstTerminalId)
          {
            _meter_data[COL_METERS.BIG_INCREMENT] = GetDeltaMeter((Int32)_meter_data[COL_METERS.TERMINAL_ID]
                                                              , (Int32)(Int64)_meter_data[COL_METERS.METER_CODE]
                                                              , (String)_meter_data[COL_METERS.METER_NAME]
                                                              , (Int64)_meter_data[COL_METERS.INITIAL_VALUE]
                                                              , (Int64)_meter_data[COL_METERS.FINAL_VALUE]
                                                              , (Int64)_meter_data[COL_METERS.METER_MAX_VALUE]
                                                              , out _delta_meter_value
                                                              , out _has_rollover
                                                              , out _max_units_increment);

            _meter_data[COL_METERS.DELTA_CALCULATE] = _delta_meter_value;
            _meter_data[COL_METERS.ROLL_OVER] = _has_rollover;
            _meter_data[COL_METERS.MAX_INCREMENT] = _max_units_increment;
            _meter_data[COL_METERS.IS_VISIBLE] = _meter.IsVisible;
            _meter_data[COL_METERS.ONLY_DELTA] = _meter.OnlyDelta;
            _meter_data[COL_METERS.ORDER] = _order;

            _order += 1;

          }
          else
          {
            _meter_data.Delete();
          }
        }
      }
      DataView dv = MetersGroup.DefaultView;
      dv.Sort = "ORDER ASC";
      MetersGroup = dv.ToTable();

    } // FillCalculateData

    //------------------------------------------------------------------------------
    // PURPOSE: Process Format Meters Type
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    //private static void ProcessFormatMetersType(DataTable MetersData, List<Meter> Meters, Type CurrentType)
    //{
    //  foreach (DataRow _row in MetersData.Rows)
    //  {
    //    ProcessFormatMetersType(_row, Meters, CurrentType);
    //  }
    //} // ProcessFormatMetersType

    //------------------------------------------------------------------------------
    // PURPOSE: Process Format Meters Type
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    //private static void ProcessFormatMetersType(DataRow MetersData, List<Meter> Meters, Type CurrentType)
    //{
    //  Int32 _meter_code;
    //  Meter _meter;

    //  _meter_code = (Int32)MetersData[COL_METERS.METER_CODE];
    //  _meter = WSI.Common.TerminalMeterGroup.GetMeter(_meter_code, Meters);

    //  if (MetersData[COL_METERS.INITIAL_VALUE] != DBNull.Value)
    //  {
    //    MetersData[COL_METERS.INITIAL_VALUE] = FormatMeterCodeValue(_meter, MetersData[COL_METERS.INITIAL_VALUE], CurrentType);
    //  }

    //  if (MetersData[COL_METERS.FINAL_VALUE] != DBNull.Value)
    //  {
    //    MetersData[COL_METERS.FINAL_VALUE] = FormatMeterCodeValue(_meter, MetersData[COL_METERS.FINAL_VALUE], CurrentType);
    //  }

    //  if (MetersData[COL_METERS.DELTA_CALCULATE] != DBNull.Value)
    //  {
    //    MetersData[COL_METERS.DELTA_CALCULATE] = FormatMeterCodeValue(_meter, MetersData[COL_METERS.DELTA_CALCULATE], CurrentType);
    //  }

    //  if (MetersData[COL_METERS.DELTA_SYSTEM] != DBNull.Value)
    //  {
    //    MetersData[COL_METERS.DELTA_SYSTEM] = FormatMeterCodeValue(_meter, MetersData[COL_METERS.DELTA_SYSTEM], CurrentType);
    //  }

    //  if (MetersData[COL_METERS.MAX_INCREMENT] != DBNull.Value)
    //  {
    //    MetersData[COL_METERS.MAX_INCREMENT] = FormatMeterCodeValue(_meter, MetersData[COL_METERS.MAX_INCREMENT], CurrentType);
    //  }

    //  if (MetersData[COL_METERS.METER_MAX_VALUE] != DBNull.Value)
    //  {
    //    MetersData[COL_METERS.METER_MAX_VALUE] = FormatMeterCodeValue(_meter, MetersData[COL_METERS.METER_MAX_VALUE], CurrentType);
    //  }
    //} // ProcessFormatMetersType

    //------------------------------------------------------------------------------
    // PURPOSE: Process Format Meters Type
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    private static void ProcessFormatHistoryMetersType(DataTable MetersData, List<Meter> Meters, Type CurrentType)
    {
      foreach (DataRow _row in MetersData.Rows)
      {
        ProcessFormatHistoryMetersType(_row, Meters, CurrentType);
      }
    } // ProcessFormatHistoryMetersType

    //------------------------------------------------------------------------------
    // PURPOSE: Process Format Meters Type
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    private static void ProcessFormatHistoryMetersType(DataRow MetersData, List<Meter> Meters, Type CurrentType)
    {
      Int32 _meter_code;
      Meter _meter;

      _meter_code = (Int32)MetersData[COL_METERS.METER_CODE];
      _meter = WSI.Common.TerminalMeterGroup.GetMeter(_meter_code, Meters);

      if (MetersData[COL_METERS_HISTORY.OLD_INITIAL_VALUE] != DBNull.Value)
      {
        MetersData[COL_METERS_HISTORY.OLD_INITIAL_VALUE] = FormatMeterCodeValue(_meter, MetersData[COL_METERS_HISTORY.OLD_INITIAL_VALUE], CurrentType);
      }

      if (MetersData[COL_METERS_HISTORY.NEW_INITIAL_VALUE] != DBNull.Value)
      {
        MetersData[COL_METERS_HISTORY.NEW_INITIAL_VALUE] = FormatMeterCodeValue(_meter, MetersData[COL_METERS_HISTORY.NEW_INITIAL_VALUE], CurrentType);
      }

      if (MetersData[COL_METERS_HISTORY.OLD_FINAL_VALUE] != DBNull.Value)
      {
        MetersData[COL_METERS_HISTORY.OLD_FINAL_VALUE] = FormatMeterCodeValue(_meter, MetersData[COL_METERS_HISTORY.OLD_FINAL_VALUE], CurrentType);
      }

      if (MetersData[COL_METERS_HISTORY.NEW_FINAL_VALUE] != DBNull.Value)
      {
        MetersData[COL_METERS_HISTORY.NEW_FINAL_VALUE] = FormatMeterCodeValue(_meter, MetersData[COL_METERS_HISTORY.NEW_FINAL_VALUE], CurrentType);
      }

      if (MetersData[COL_METERS_HISTORY.OLD_DELTA_SYSTEM] != DBNull.Value)
      {
        MetersData[COL_METERS_HISTORY.OLD_DELTA_SYSTEM] = FormatMeterCodeValue(_meter, MetersData[COL_METERS_HISTORY.OLD_DELTA_SYSTEM], CurrentType);
      }

      if (MetersData[COL_METERS_HISTORY.NEW_DELTA_SYSTEM] != DBNull.Value)
      {
        MetersData[COL_METERS_HISTORY.NEW_DELTA_SYSTEM] = FormatMeterCodeValue(_meter, MetersData[COL_METERS_HISTORY.NEW_DELTA_SYSTEM], CurrentType);
      }

      if (MetersData[COL_METERS_HISTORY.OLD_DELTA_CALCULATE] != DBNull.Value)
      {
        MetersData[COL_METERS_HISTORY.OLD_DELTA_CALCULATE] = FormatMeterCodeValue(_meter, MetersData[COL_METERS_HISTORY.OLD_DELTA_CALCULATE], CurrentType);
      }

      if (MetersData[COL_METERS_HISTORY.NEW_DELTA_CALCULATE] != DBNull.Value)
      {
        MetersData[COL_METERS_HISTORY.NEW_DELTA_CALCULATE] = FormatMeterCodeValue(_meter, MetersData[COL_METERS_HISTORY.NEW_DELTA_CALCULATE], CurrentType);
      }
    } // ProcessFormatHistoryMetersType

    //------------------------------------------------------------------------------
    // PURPOSE: Read last meters updated. 
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int32    TerminalId
    //        - DateTime Workday
    //        - Int64    MetersGroup
    //
    //      - OUTPUT:
    //        - DataTable  DTHistory
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    internal static Boolean ReadHistory(Int64[] Terminals, DateTime Workday, List<Meter> Meters, out DataTable DTHistory)
    {
      StringBuilder _sb;
      String _meters_codes_list;
      String _terminals_id_list;

      DTHistory = TerminalMeterGroup.CreateHistoryMetersGroupDataTable();

      try
      {
        _meters_codes_list = "";
        _terminals_id_list = "";

        foreach (Meter _meter in Meters)
        {
          _meters_codes_list += _meter.Code.ToString() + ", ";
        }
        _meters_codes_list = _meters_codes_list.Substring(0, _meters_codes_list.Length - 2);

        foreach (Int32 _terminal in Terminals)
        {
          _terminals_id_list += _terminal.ToString() + ", ";
        }
        _terminals_id_list = _terminals_id_list.Substring(0, _terminals_id_list.Length - 2);

        _sb = new StringBuilder();

        _sb.AppendLine("SELECT   TMA_TERMINAL_ID        AS TERMINAL_ID         ");   // Col 01
        _sb.AppendLine("       , TE_NAME                AS TERMINAL_NAME       ");   // Col 02
        _sb.AppendLine("       , TMA_GAMING_DAY         AS GAMING_DAY          ");   // Col 03
        _sb.AppendLine("       , TMA_METER_CODE         AS METER_CODE          ");   // Col 04
        _sb.AppendLine("       , TMA_GAME_ID            AS GAME_ID             ");   // Col 05
        _sb.AppendLine("       , TMA_DENOMINATION       AS DENOMINATION        ");   // Col 06
        _sb.AppendLine("       , TMA_UNIQUE_ID          AS UNIQUE_ID           ");   // Col 07
        _sb.AppendLine("       , TMA_DATETIME           AS DATETIME            ");   // Col 08
        _sb.AppendLine("       , TMA_TYPE               AS TYPE                ");   // Col 09
        _sb.AppendLine("       , TMA_SUB_TYPE           AS SUB_TYPE            ");   // Col 10
        _sb.AppendLine("       , TMA_OLD_INITIAL_VALUE  AS OLD_INITIAL_VALUE   ");   // Col 11
        _sb.AppendLine("       , TMA_OLD_FINAL_VALUE    AS OLD_FINAL_VALUE     ");   // Col 12
        _sb.AppendLine("       , TMA_OLD_DELTA_VALUE    AS OLD_DELTA_SYSTEM    ");   // Col 13
        _sb.AppendLine("       , TMA_OLD_DELTA_VALUE    AS OLD_DELTA_CALCULATE ");   // Col 14
        _sb.AppendLine("       , TMA_NEW_INITIAL_VALUE  AS NEW_INITIAL_VALUE   ");   // Col 15
        _sb.AppendLine("       , TMA_NEW_FINAL_VALUE    AS NEW_FINAL_VALUE     ");   // Col 16
        _sb.AppendLine("       , TMA_NEW_DELTA_VALUE    AS NEW_DELTA_SYSTEM    ");   // Col 17
        _sb.AppendLine("       , TMA_NEW_DELTA_VALUE    AS NEW_DELTA_CALCULATE ");   // Col 18
        _sb.AppendLine("       , TMA_USER_ID            AS USER_ID             ");   // Col 19
        _sb.AppendLine("       , GU_USERNAME            AS USER_NAME           ");   // Col 20
        _sb.AppendLine("       , TMA_REASON             AS REASON              ");   // Col 21
        _sb.AppendLine("       , TMA_REMARKS            AS REMARKS             ");   // Col 22
        _sb.AppendLine("  FROM   SAS_METERS_ADJUSTMENTS  ");
        _sb.AppendLine(" INNER   JOIN TERMINALS ON TE_TERMINAL_ID = TMA_TERMINAL_ID ");
        _sb.AppendLine(" INNER   JOIN GUI_USERS ON GU_USER_ID = TMA_USER_ID         ");
        _sb.AppendFormat("    AND   TMA_TERMINAL_ID IN ( {0} )  ", _terminals_id_list);
        _sb.AppendLine("    AND   TMA_GAMING_DAY      = @pWorkday       ");
        _sb.AppendFormat("    AND   TMA_METER_CODE  IN ( {0} ) ", _meters_codes_list);
        _sb.AppendLine(" ORDER   BY TMA_UNIQUE_ID DESC   ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            //_sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = Terminals;
            _sql_cmd.Parameters.Add("@pWorkday", SqlDbType.DateTime).Value = Workday;
            //_sql_cmd.Parameters.Add("@pMetersGroup", SqlDbType.BigInt).Value = MetersGroup;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(DTHistory);
            }
          } // SqlCommand
        } // DB_TRX

        //////ProcessFormatHistoryMetersType(DTHistory, Meters, typeof(Int64));

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ReadHistory 

    //------------------------------------------------------------------------------
    // PURPOSE: Save update data
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    internal static TerminalMeterGroup.ResultCode SaveData(DataTable DTMeters, List<Meter> Meters, MeterRelationsBD MeterRelationDB)
    {
      DataTable _terminal_sas_meter_history;
      DataTable _meter_generic;
      TerminalMeterGroup.ResultCode _result_code;
      Int32 _game_id;

      _result_code = ResultCode.ERROR_GENERIC;
      System.Globalization.CultureInfo _old_ci = System.Threading.Thread.CurrentThread.CurrentCulture;
      System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-us");

      _terminal_sas_meter_history = CreateTerminalSasMeterHistory();
      _meter_generic = CreateMeterTableGeneric(MeterRelationDB);

      _game_id = 0;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // LTC  29-OCT-2017
          // If SiteId != "" IS A MULTISITE METERS EDITING
          if (SiteId != "")
          {
            _terminal_sas_meter_history.Columns.Add("SITE_ID", typeof(String), SiteId);

            foreach (DataRow _row in DTMeters.GetChanges().Rows)
            {
              if (!FillSasMeterToUpdate(_row, _terminal_sas_meter_history, _meter_generic, _game_id, MeterRelationDB))
              {
                return TerminalMeterGroup.ResultCode.ERROR_GENERIC;
              }

              if (!InsertSasMetersAdjustmentsMS((DateTime)_row[COL_METERS.GAMING_DAY]
                                               , SiteId
                                               , (Int32)_row[COL_METERS.TERMINAL_ID]
                                               , (Int32)(Int64)_row[COL_METERS.METER_CODE]
                                               , (Int32)_row[COL_METERS.GAME_ID]
                                               , (Decimal)_row[COL_METERS.DENOMINATION]
                                               , WGDB.Now
                                               , (MetersAdjustmentsType)_row[COL_METERS.TYPE]
                                               , (MetersAdjustmentsSubType)_row[COL_METERS.SUB_TYPE]
                                               , (Int64)_row[COL_METERS.INITIAL_VALUE, DataRowVersion.Original]
                                               , (Int64)_row[COL_METERS.FINAL_VALUE, DataRowVersion.Original]
                                               , (Int64)_row[COL_METERS.DELTA_SYSTEM, DataRowVersion.Original]
                                               , (Int64)_row[COL_METERS.INITIAL_VALUE, DataRowVersion.Current]
                                               , (Int64)_row[COL_METERS.FINAL_VALUE, DataRowVersion.Current]
                                               , (Int64)_row[COL_METERS.DELTA_SYSTEM, DataRowVersion.Current]
                                               , (Int32)_row[COL_METERS.USER_ID]
                                               , (Int32)_row[COL_METERS.REASON]
                                               , (String)_row[COL_METERS.REMARKS]
                                               , _db_trx.SqlTransaction))
              {
                return TerminalMeterGroup.ResultCode.ERROR_GENERIC;
              }
            }// foreach

            if (!UpdateTerminalSasMeterHistoryMS(_terminal_sas_meter_history, SiteId, _db_trx.SqlTransaction))
            {
              return TerminalMeterGroup.ResultCode.ERROR_GENERIC;
            }

            _result_code = ResultCode.OK;
            _db_trx.SqlTransaction.Commit();

          }
          else
          {

            _result_code = GetGameIdForTerminal(DTMeters, out _game_id, _db_trx.SqlTransaction);

            if (_result_code != TerminalMeterGroup.ResultCode.OK)
            {
              return _result_code;
            }

            foreach (DataRow _row in DTMeters.GetChanges().Rows)
            {
              if (!FillSasMeterToUpdate(_row, _terminal_sas_meter_history, _meter_generic, _game_id, MeterRelationDB))
              {
                return TerminalMeterGroup.ResultCode.ERROR_GENERIC;
              }

              if (!InsertSasMetersAdjustments((DateTime)_row[COL_METERS.GAMING_DAY]
                                               , (Int32)_row[COL_METERS.TERMINAL_ID]
                                               , (Int32)(Int64)_row[COL_METERS.METER_CODE]
                                               , (Int32)_row[COL_METERS.GAME_ID]
                                               , (Decimal)_row[COL_METERS.DENOMINATION]
                                               , WGDB.Now
                                               , (MetersAdjustmentsType)_row[COL_METERS.TYPE]
                                               , (MetersAdjustmentsSubType)_row[COL_METERS.SUB_TYPE]
                                               , (Int64)_row[COL_METERS.INITIAL_VALUE, DataRowVersion.Original]
                                               , (Int64)_row[COL_METERS.FINAL_VALUE, DataRowVersion.Original]
                                               , (Int64)_row[COL_METERS.DELTA_SYSTEM, DataRowVersion.Original]
                                               , (Int64)_row[COL_METERS.INITIAL_VALUE, DataRowVersion.Current]
                                               , (Int64)_row[COL_METERS.FINAL_VALUE, DataRowVersion.Current]
                                               , (Int64)_row[COL_METERS.DELTA_SYSTEM, DataRowVersion.Current]
                                               , (Int32)_row[COL_METERS.USER_ID]
                                               , (Int32)_row[COL_METERS.REASON]
                                               , (String)_row[COL_METERS.REMARKS]
                                               , _db_trx.SqlTransaction))
              {
                return TerminalMeterGroup.ResultCode.ERROR_GENERIC;
              }
            }// foreach

            if (!UpdateTerminalSasMeterHistory(_terminal_sas_meter_history, _db_trx.SqlTransaction))
            {
              return TerminalMeterGroup.ResultCode.ERROR_GENERIC;
            }

            _result_code = UpdateMetersGeneric(_meter_generic, MeterRelationDB, _db_trx.SqlTransaction);

            if (_result_code == TerminalMeterGroup.ResultCode.OK)
            {
              _result_code = ResultCode.OK;
              _db_trx.SqlTransaction.Commit();
            }
          }
        } // DB_TRX
      } // try
      catch (Exception _ex)
      {
        _result_code = ResultCode.ERROR_GENERIC;
        Log.Exception(_ex);
      }
      finally
      {
        System.Threading.Thread.CurrentThread.CurrentCulture = _old_ci;
      }
      return _result_code;

    }  // SaveAjustmentMeters

    //------------------------------------------------------------------------------
    // PURPOSE: Fill datatable for save update data
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    private static Boolean FillSasMeterToUpdate(DataRow Row, DataTable SasMeterToUpdate, DataTable MeterGeneric, Int32 GameId, MeterRelationsBD MeterRelationsDB)
    {
      DataRow _new_data_row;
      DataRow[] _rows_generic;
      Int64 _meter_code;
      List<MeterRelationBD> _meter_relations_db;

      _meter_relations_db = null;

      try
      {
        _meter_code = (Int64)Row[COL_METERS.METER_CODE];

        // Daily
        _new_data_row = SasMeterToUpdate.NewRow();
        _new_data_row[COL_METERS_TO_UPDATE.TERMINAL_ID] = Row[COL_METERS.TERMINAL_ID];          //col 00
        _new_data_row[COL_METERS_TO_UPDATE.METER_CODE] = Row[COL_METERS.METER_CODE];            //col 01
        _new_data_row[COL_METERS_TO_UPDATE.GAME_ID] = Row[COL_METERS.GAME_ID];                  //col 02  This is allways 0
        _new_data_row[COL_METERS_TO_UPDATE.DENOMINATION] = Row[COL_METERS.DENOMINATION];        //col 03
        _new_data_row[COL_METERS_TO_UPDATE.TYPE] = 20;                                          //col 04
        _new_data_row[COL_METERS_TO_UPDATE.DATETIME] = Row[COL_METERS.GAMING_DAY];              //col 05
        _new_data_row[COL_METERS_TO_UPDATE.METER_INI_VALUE] = Row[COL_METERS.INITIAL_VALUE];    //col 06
        _new_data_row[COL_METERS_TO_UPDATE.METER_FIN_VALUE] = Row[COL_METERS.FINAL_VALUE];      //col 07
        _new_data_row[COL_METERS_TO_UPDATE.METER_INCREMENT] = Row[COL_METERS.DELTA_SYSTEM];     //col 08
        SasMeterToUpdate.Rows.Add(_new_data_row);

        //  Hourly
        for (int _idx = 0; _idx < NUM_HOURS_DAY; _idx++)
        {
          _new_data_row = SasMeterToUpdate.NewRow();
          _new_data_row[COL_METERS_TO_UPDATE.TERMINAL_ID] = Row[COL_METERS.TERMINAL_ID];                        //col 00
          _new_data_row[COL_METERS_TO_UPDATE.METER_CODE] = Row[COL_METERS.METER_CODE];                          //col 01
          _new_data_row[COL_METERS_TO_UPDATE.GAME_ID] = 0;                                                      //col 02
          _new_data_row[COL_METERS_TO_UPDATE.DENOMINATION] = 0;                                                 //col 03
          _new_data_row[COL_METERS_TO_UPDATE.TYPE] = 1;                                                         //col 04
          _new_data_row[COL_METERS_TO_UPDATE.DATETIME] = ((DateTime)Row[COL_METERS.GAMING_DAY]).AddHours(_idx); //col 05
          if (_idx == 0)
          {
            _new_data_row[COL_METERS_TO_UPDATE.METER_INI_VALUE] = Row[COL_METERS.INITIAL_VALUE];    //col 06
            _new_data_row[COL_METERS_TO_UPDATE.METER_INCREMENT] = Row[COL_METERS.DELTA_SYSTEM];     //col 08
          }
          else
          {
            _new_data_row[COL_METERS_TO_UPDATE.METER_INI_VALUE] = Row[COL_METERS.FINAL_VALUE];      //col 06
            _new_data_row[COL_METERS_TO_UPDATE.METER_INCREMENT] = 0;                                //col 08
          }
          _new_data_row[COL_METERS_TO_UPDATE.METER_FIN_VALUE] = Row[COL_METERS.FINAL_VALUE];                   //col 07
          SasMeterToUpdate.Rows.Add(_new_data_row);


          if (Row[COL_METERS.DELTA_SYSTEM, DataRowVersion.Original] != Row[COL_METERS.DELTA_SYSTEM, DataRowVersion.Current])
          {
            MeterRelationsDB.MeterRelations.TryGetValue(_meter_code, out _meter_relations_db);

            if (_meter_relations_db != null)
            {
              _rows_generic = MeterGeneric.Select("TERMINAL_ID = " + (Int32)Row[COL_METERS.TERMINAL_ID] +
                                   " AND GAME_ID = " + (Int32)Row[COL_METERS.GAME_ID] +
                                   " AND DENOMINATION = " + (Decimal)Row[COL_METERS.DENOMINATION] +
                                   " AND DATETIME = '" + ((DateTime)Row[COL_METERS.GAMING_DAY]).AddHours(_idx) + "' ");
              if (_rows_generic.Length > 0)
              {
                _new_data_row = _rows_generic[0];
              }
              else
              {
                _new_data_row = MeterGeneric.NewRow();
                _new_data_row[COL_METERS_GENERIC.TERMINAL_ID] = Row[COL_METERS.TERMINAL_ID];                        //col 00
                _new_data_row[COL_METERS_GENERIC.GAME_ID] = GameId;                                                 //col 01 This is the game search for terminal
                _new_data_row[COL_METERS_GENERIC.DENOMINATION] = Row[COL_METERS.DENOMINATION];                      //col 02
                _new_data_row[COL_METERS_GENERIC.DATETIME] = ((DateTime)Row[COL_METERS.GAMING_DAY]).AddHours(_idx); //col 03

                MeterGeneric.Rows.Add(_new_data_row);

              }
              foreach (MeterRelationBD _meter_relation_db in _meter_relations_db)
              {
                if (_idx == 0)
                {
                  if (Type.GetType(_meter_relation_db.FieldType) == typeof(Decimal))
                  {
                    _new_data_row[_meter_relation_db.FieldName] = (Decimal)((Int64)Row[COL_METERS.DELTA_SYSTEM]) / 100;
                  }
                  else
                  {
                    _new_data_row[_meter_relation_db.FieldName] = Row[COL_METERS.DELTA_SYSTEM];
                  }
                }
                else
                {
                  _new_data_row[_meter_relation_db.FieldName] = 0;
                }
              }
            }
          }

        } // for Hourly
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;

    } // FillSasMeterToUpdate

    //------------------------------------------------------------------------------
    // PURPOSE: Upadate terminal sas meter history
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    private static Boolean UpdateTerminalSasMeterHistory(DataTable TerminalSasMeterHistory, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("EXECUTE Update_TerminalSasMeterHistory ");

        _sb.AppendLine("  @pTerminaId");                   // Int                     
        _sb.AppendLine(", @pMeterCode");                   // Int                     
        _sb.AppendLine(", @pGameId");                      // Int                         
        _sb.AppendLine(", @pDenomination");                // Money               
        _sb.AppendLine(", @pType");                        // Int                         
        _sb.AppendLine(", @pDateTime");                    // DateTime                
        _sb.AppendLine(", @pMeterIniValue");               // BigInt              
        _sb.AppendLine(", @pMeterFinValue");               // BigInt              
        _sb.AppendLine(", @pMeterIncrement");              // BigInt              

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {

          _sql_cmd.Parameters.Add("@pTerminaId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";                             //col 00
          _sql_cmd.Parameters.Add("@pMeterCode", SqlDbType.Int).SourceColumn = "METER_CODE";                              //col 01
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "GAME_ID";                                    //col 02
          _sql_cmd.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "DENOMINATION";                       //col 03
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "TYPE";                                         //col 04
          _sql_cmd.Parameters.Add("@pDateTime", SqlDbType.DateTime).SourceColumn = "DATETIME";                            //col 05
          _sql_cmd.Parameters.Add("@pMeterIniValue", SqlDbType.BigInt).SourceColumn = "METER_INI_VALUE";                  //col 06
          _sql_cmd.Parameters.Add("@pMeterFinValue", SqlDbType.BigInt).SourceColumn = "METER_FIN_VALUE";                  //col 07
          _sql_cmd.Parameters.Add("@pMeterIncrement", SqlDbType.BigInt).SourceColumn = "METER_INCREMENT";                 //col 08

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.InsertCommand = _sql_cmd;
            if (_sql_da.Update(TerminalSasMeterHistory) == TerminalSasMeterHistory.Rows.Count)
            {
              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateTerminalSasMeterHistory

    // LTC  29-OCT-2017
    private static Boolean UpdateTerminalSasMeterHistoryMS(DataTable TerminalSasMeterHistory, String Site, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("EXECUTE Update_TerminalSasMeterHistory ");
        _sb.AppendLine("  @pSiteId");                    // Int   
        _sb.AppendLine(", @pTerminaId");                   // Int                     
        _sb.AppendLine(", @pMeterCode");                   // Int                     
        _sb.AppendLine(", @pGameId");                      // Int                         
        _sb.AppendLine(", @pDenomination");                // Money               
        _sb.AppendLine(", @pType");                        // Int                         
        _sb.AppendLine(", @pDateTime");                    // DateTime                
        _sb.AppendLine(", @pMeterIniValue");               // BigInt              
        _sb.AppendLine(", @pMeterFinValue");               // BigInt              
        _sb.AppendLine(", @pMeterIncrement");              // BigInt  


        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = Site;
          _sql_cmd.Parameters.Add("@pTerminaId", SqlDbType.Int).SourceColumn = "TERMINAL_ID";                             //col 00
          _sql_cmd.Parameters.Add("@pMeterCode", SqlDbType.Int).SourceColumn = "METER_CODE";                              //col 01
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "GAME_ID";                                    //col 02
          _sql_cmd.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "DENOMINATION";                       //col 03
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "TYPE";                                         //col 04
          _sql_cmd.Parameters.Add("@pDateTime", SqlDbType.DateTime).SourceColumn = "DATETIME";                            //col 05
          _sql_cmd.Parameters.Add("@pMeterIniValue", SqlDbType.BigInt).SourceColumn = "METER_INI_VALUE";                  //col 06
          _sql_cmd.Parameters.Add("@pMeterFinValue", SqlDbType.BigInt).SourceColumn = "METER_FIN_VALUE";                  //col 07
          _sql_cmd.Parameters.Add("@pMeterIncrement", SqlDbType.BigInt).SourceColumn = "METER_INCREMENT";                 //col 08          

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.InsertCommand = _sql_cmd;
            if (_sql_da.Update(TerminalSasMeterHistory) == TerminalSasMeterHistory.Rows.Count)
            {
              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateTerminalSasMeterHistoryMS

    //------------------------------------------------------------------------------
    // PURPOSE: Get GameId for terminal
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    private static ResultCode GetGameIdForTerminal(DataTable DTMeters, out Int32 GameId, SqlTransaction SqlTrx)
    {
      ResultCode _result_code;
      StringBuilder _sb;
      Object _obj;

      GameId = 0;
      _result_code = ResultCode.ERROR_GENERIC;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("EXECUTE GetGameIdForTerminal ");

        _sb.AppendLine("  @pFromDate");                    // DateTime
        _sb.AppendLine(", @pTodate");                      // DateTime                         
        _sb.AppendLine(", @pTerminaId");                   // Int                     

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {

          _sql_cmd.Parameters.Add("@pFromDate", SqlDbType.DateTime).Value = Misc.Opening((DateTime)DTMeters.Rows[0][COL_METERS.GAMING_DAY]);
          _sql_cmd.Parameters.Add("@pTodate", SqlDbType.DateTime).Value = Misc.Opening((DateTime)DTMeters.Rows[0][COL_METERS.GAMING_DAY]).AddDays(1);
          _sql_cmd.Parameters.Add("@pTerminaId", SqlDbType.Int).Value = (Int32)DTMeters.Rows[0][COL_METERS.TERMINAL_ID];

          _obj = _sql_cmd.ExecuteScalar();
          if (_obj != null && _obj != DBNull.Value)
          {
            GameId = (Int32)_obj;
          }

          _result_code = ResultCode.OK;
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Class == 11)
        {
          _result_code = TerminalMeterGroup.ResultCode.ERROR_NEVER_ACTIVITY;
        }
        Log.Error(_sql_ex.Message);
      }
      catch (Exception _ex)
      {
        _result_code = TerminalMeterGroup.ResultCode.ERROR_GENERIC;
        Log.Exception(_ex);
      }

      return _result_code;


    } // UpdateTerminalSasMeterHistory


    //------------------------------------------------------------------------------
    // PURPOSE: Update meter group tables
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    private static TerminalMeterGroup.ResultCode UpdateMetersGeneric(DataTable MeterGenericDT, MeterRelationsBD MeterRelationsDB, SqlTransaction SqlTrx)
    {
      TerminalMeterGroup.ResultCode _return;
      Dictionary<String, List<MeterRelationBD>> _fields_by_tables;
      SqlCommand _sql_cmd;

      _return = TerminalMeterGroup.ResultCode.ERROR_GENERIC;

      try
      {
        _fields_by_tables = MeterRelationsBD.GetMetersFieldsByTable(MeterRelationsDB.MeterRelations);

        foreach (KeyValuePair<String, List<MeterRelationBD>> _fields_by_table in _fields_by_tables)
        {
          _sql_cmd = MeterRelationsBD.GetCommandMetersRelationBD(_fields_by_table);
          _sql_cmd.Transaction = SqlTrx;
          _sql_cmd.Connection = SqlTrx.Connection;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.InsertCommand = _sql_cmd;
            if (_sql_da.Update(MeterGenericDT) != MeterGenericDT.Rows.Count)
            {
              _return = TerminalMeterGroup.ResultCode.ERROR_GENERIC;
            }
          }
          foreach (DataRow _row in MeterGenericDT.Rows)
          {
            _row.SetAdded();
          }
        }

        _return = ResultCode.OK;

      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Class == 16)
        {
          _return = TerminalMeterGroup.ResultCode.ERROR_NEVER_ACTIVITY;
        }
        Log.Error(_sql_ex.Message);
      }
      catch (Exception _ex)
      {
        _return = TerminalMeterGroup.ResultCode.ERROR_GENERIC;
        Log.Exception(_ex);
      }
      return _return;
    } // UpdateMetersGeneric

    //------------------------------------------------------------------------------
    // PURPOSE: Save log for adjustment in SAS_METERS_ADJUSTMENTS
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    //     NOTE: Use in WCP for automatic adjustments
    public static Boolean InsertSasMetersAdjustments(DateTime GamingDay
                                                    , Int32 TerminalId
                                                    , Int32 MeterCode
                                                    , Int32 GameId
                                                    , Int32 Denomination
                                                    , DateTime DateTimeNow
                                                    , MetersAdjustmentsType AjustType
                                                    , MetersAdjustmentsSubType AjustSubType
                                                    , Int64 InitialValue
                                                    , Int64 FinalValue
                                                    , Int64 DeltaSystem
                                                    , String Remarks)
    {

      Int32 _user_id;
      String _user_name;
      Int64 _new_initial_value;
      Int64 _new_final_value;
      Int64 _new_delta_system;

      if (Denomination > 0)
      {
        return true;
      }

      _new_initial_value = InitialValue;
      _new_final_value = FinalValue;
      _new_delta_system = DeltaSystem;

      if (AjustSubType == MetersAdjustmentsSubType.System_Game_BigIncrement ||
          AjustSubType == MetersAdjustmentsSubType.System_Machine_BigIncrement ||
          AjustSubType == MetersAdjustmentsSubType.System_SAS_BigIncrement)
      {
        _new_delta_system = 0;
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          WSI.Common.Cashier.GetSystemUser(GU_USER_TYPE.SYS_SYSTEM, _db_trx.SqlTransaction, out _user_id, out _user_name);

          if (TerminalMeterGroup.InsertSasMetersAdjustments(GamingDay
                                                       , TerminalId
                                                       , MeterCode
                                                       , GameId
                                                       , Denomination
                                                       , DateTimeNow
                                                       , AjustType
                                                       , AjustSubType
                                                       , InitialValue
                                                       , FinalValue
                                                       , DeltaSystem
                                                       , _new_initial_value
                                                       , _new_final_value
                                                       , _new_delta_system
                                                       , _user_id
                                                       , (Int32)MeterAjustmentReason.others
                                                       , Remarks
                                                       , _db_trx.SqlTransaction))
          {
            _db_trx.Commit();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    } // InsertSasMetersAdjustments

    //------------------------------------------------------------------------------
    // PURPOSE: Save log for adjustment in SAS_METERS_ADJUSTMENTS
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    //     NOTE: Use in GUI for manual adjust
    public static Boolean InsertSasMetersAdjustments(DateTime GamingDay
                                                    , Int32 TerminalId
                                                    , Int32 MeterCode
                                                    , Int32 GameId
                                                    , Decimal Denomination
                                                    , DateTime DateTimeNow
                                                    , MetersAdjustmentsType AjustType
                                                    , MetersAdjustmentsSubType AjustSubType
                                                    , Int64 OldInitialValue
                                                    , Int64 OldFinalValue
                                                    , Int64 OldDeltaSystem
                                                    , Int64 NewInitialValue
                                                    , Int64 NewFinalValue
                                                    , Int64 NewDeltaSystem
                                                    , Int32 UserId
                                                    , Int32 Reason
                                                    , String Remarks
                                                    , SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      try
      {
        _sb.AppendLine("INSERT INTO SAS_METERS_ADJUSTMENTS ");
        _sb.AppendLine("           (TMA_GAMING_DAY         ");
        _sb.AppendLine("           ,TMA_TERMINAL_ID        ");
        _sb.AppendLine("           ,TMA_METER_CODE         ");
        _sb.AppendLine("           ,TMA_GAME_ID            ");
        _sb.AppendLine("           ,TMA_DENOMINATION       ");
        _sb.AppendLine("           ,TMA_DATETIME           ");
        _sb.AppendLine("           ,TMA_TYPE               ");
        _sb.AppendLine("           ,TMA_SUB_TYPE           ");
        _sb.AppendLine("           ,TMA_OLD_INITIAL_VALUE  ");
        _sb.AppendLine("           ,TMA_OLD_FINAL_VALUE    ");
        _sb.AppendLine("           ,TMA_OLD_DELTA_VALUE    ");
        _sb.AppendLine("           ,TMA_NEW_INITIAL_VALUE  ");
        _sb.AppendLine("           ,TMA_NEW_FINAL_VALUE    ");
        _sb.AppendLine("           ,TMA_NEW_DELTA_VALUE    ");
        _sb.AppendLine("           ,TMA_USER_ID            ");
        _sb.AppendLine("           ,TMA_REASON             ");
        _sb.AppendLine("           ,TMA_REMARKS)           ");
        _sb.AppendLine("     VALUES                        ");
        _sb.AppendLine("           (@pGamingDay            ");
        _sb.AppendLine("           ,@pTerminalId           ");
        _sb.AppendLine("           ,@pMeterCode            ");
        _sb.AppendLine("           ,@pGameId               ");
        _sb.AppendLine("           ,@pDenomination         ");
        _sb.AppendLine("           ,@pDateTime             ");
        _sb.AppendLine("           ,@pType                 ");
        _sb.AppendLine("           ,@pSubType              ");
        _sb.AppendLine("           ,@pOldInitialValue      ");
        _sb.AppendLine("           ,@pOldFinalValue        ");
        _sb.AppendLine("           ,@pOldDeltaValue        ");
        _sb.AppendLine("           ,@pNewInitialValue      ");
        _sb.AppendLine("           ,@pNewFinalValue        ");
        _sb.AppendLine("           ,@pNewDeltaValue        ");
        _sb.AppendLine("           ,@pUserId               ");
        _sb.AppendLine("           ,@pReason               ");
        _sb.AppendLine("           ,@pRemarks             )");
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pGamingDay", SqlDbType.DateTime).Value = GamingDay;
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pMeterCode", SqlDbType.Int).Value = MeterCode;
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int).Value = GameId;
          _sql_cmd.Parameters.Add("@pDenomination", SqlDbType.Decimal).Value = Denomination;
          _sql_cmd.Parameters.Add("@pDateTime", SqlDbType.DateTime).Value = DateTimeNow;
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = (Int32)AjustType;
          _sql_cmd.Parameters.Add("@pSubType", SqlDbType.Int).Value = (Int32)AjustSubType;
          _sql_cmd.Parameters.Add("@pOldInitialValue", SqlDbType.BigInt).Value = OldInitialValue;
          _sql_cmd.Parameters.Add("@pOldFinalValue", SqlDbType.BigInt).Value = OldFinalValue;
          _sql_cmd.Parameters.Add("@pOldDeltaValue", SqlDbType.BigInt).Value = OldDeltaSystem;
          _sql_cmd.Parameters.Add("@pNewInitialValue", SqlDbType.BigInt).Value = NewInitialValue;
          _sql_cmd.Parameters.Add("@pNewFinalValue", SqlDbType.BigInt).Value = NewFinalValue;
          _sql_cmd.Parameters.Add("@pNewDeltaValue", SqlDbType.BigInt).Value = NewDeltaSystem;
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;
          _sql_cmd.Parameters.Add("@pReason", SqlDbType.Int).Value = Reason;
          _sql_cmd.Parameters.Add("@pRemarks", SqlDbType.NVarChar, 512).Value = Remarks;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            //TODO JML: revisar..
            return true;
          }
        } // SqlCommand 
      } // try
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;

    } // InsertSasMetersAdjustments

    // LTC  29-OCT-2017
    public static Boolean InsertSasMetersAdjustmentsMS(DateTime GamingDay
                                                    , String SiteId
                                                    , Int32 TerminalId
                                                    , Int32 MeterCode
                                                    , Int32 GameId
                                                    , Decimal Denomination
                                                    , DateTime DateTimeNow
                                                    , MetersAdjustmentsType AjustType
                                                    , MetersAdjustmentsSubType AjustSubType
                                                    , Int64 OldInitialValue
                                                    , Int64 OldFinalValue
                                                    , Int64 OldDeltaSystem
                                                    , Int64 NewInitialValue
                                                    , Int64 NewFinalValue
                                                    , Int64 NewDeltaSystem
                                                    , Int32 UserId
                                                    , Int32 Reason
                                                    , String Remarks
                                                    , SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      try
      {
        _sb.AppendLine("INSERT INTO SAS_METERS_ADJUSTMENTS ");
        _sb.AppendLine("           (TMA_GAMING_DAY         ");
        _sb.AppendLine("           ,TMA_SITE_ID           ");
        _sb.AppendLine("           ,TMA_TERMINAL_ID        ");
        _sb.AppendLine("           ,TMA_METER_CODE         ");
        _sb.AppendLine("           ,TMA_GAME_ID            ");
        _sb.AppendLine("           ,TMA_DENOMINATION       ");
        _sb.AppendLine("           ,TMA_DATETIME           ");
        _sb.AppendLine("           ,TMA_TYPE               ");
        _sb.AppendLine("           ,TMA_SUB_TYPE           ");
        _sb.AppendLine("           ,TMA_OLD_INITIAL_VALUE  ");
        _sb.AppendLine("           ,TMA_OLD_FINAL_VALUE    ");
        _sb.AppendLine("           ,TMA_OLD_DELTA_VALUE    ");
        _sb.AppendLine("           ,TMA_NEW_INITIAL_VALUE  ");
        _sb.AppendLine("           ,TMA_NEW_FINAL_VALUE    ");
        _sb.AppendLine("           ,TMA_NEW_DELTA_VALUE    ");
        _sb.AppendLine("           ,TMA_USER_ID            ");
        _sb.AppendLine("           ,TMA_REASON             ");
        _sb.AppendLine("           ,TMA_REMARKS)           ");
        _sb.AppendLine("     VALUES                        ");
        _sb.AppendLine("           (@pGamingDay            ");
        _sb.AppendLine("           ,@pSiteId               ");
        _sb.AppendLine("           ,@pTerminalId           ");
        _sb.AppendLine("           ,@pMeterCode            ");
        _sb.AppendLine("           ,@pGameId               ");
        _sb.AppendLine("           ,@pDenomination         ");
        _sb.AppendLine("           ,@pDateTime             ");
        _sb.AppendLine("           ,@pType                 ");
        _sb.AppendLine("           ,@pSubType              ");
        _sb.AppendLine("           ,@pOldInitialValue      ");
        _sb.AppendLine("           ,@pOldFinalValue        ");
        _sb.AppendLine("           ,@pOldDeltaValue        ");
        _sb.AppendLine("           ,@pNewInitialValue      ");
        _sb.AppendLine("           ,@pNewFinalValue        ");
        _sb.AppendLine("           ,@pNewDeltaValue        ");
        _sb.AppendLine("           ,@pUserId               ");
        _sb.AppendLine("           ,@pReason               ");
        _sb.AppendLine("           ,@pRemarks             )");
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pGamingDay", SqlDbType.DateTime).Value = GamingDay;
          _sql_cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pMeterCode", SqlDbType.Int).Value = MeterCode;
          _sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int).Value = GameId;
          _sql_cmd.Parameters.Add("@pDenomination", SqlDbType.Decimal).Value = Denomination;
          _sql_cmd.Parameters.Add("@pDateTime", SqlDbType.DateTime).Value = DateTimeNow;
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = (Int32)AjustType;
          _sql_cmd.Parameters.Add("@pSubType", SqlDbType.Int).Value = (Int32)AjustSubType;
          _sql_cmd.Parameters.Add("@pOldInitialValue", SqlDbType.BigInt).Value = OldInitialValue;
          _sql_cmd.Parameters.Add("@pOldFinalValue", SqlDbType.BigInt).Value = OldFinalValue;
          _sql_cmd.Parameters.Add("@pOldDeltaValue", SqlDbType.BigInt).Value = OldDeltaSystem;
          _sql_cmd.Parameters.Add("@pNewInitialValue", SqlDbType.BigInt).Value = NewInitialValue;
          _sql_cmd.Parameters.Add("@pNewFinalValue", SqlDbType.BigInt).Value = NewFinalValue;
          _sql_cmd.Parameters.Add("@pNewDeltaValue", SqlDbType.BigInt).Value = NewDeltaSystem;
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = UserId;
          _sql_cmd.Parameters.Add("@pReason", SqlDbType.Int).Value = Reason;
          _sql_cmd.Parameters.Add("@pRemarks", SqlDbType.NVarChar, 512).Value = Remarks;

          if (_sql_cmd.ExecuteNonQuery() == 1)
          {
            //TODO JML: revisar..
            return true;
          }
        } // SqlCommand 
      } // try
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;

    } // InsertSasMetersAdjustmentsMS

    //------------------------------------------------------------------------------
    // PURPOSE: Get Meter Groups
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    public static Boolean GetMeterGroups(out List<MeterGroups> DTMeterGroups)
    {
      Boolean _is_tito;

      _is_tito = WSI.Common.TITO.Utils.IsTitoMode();

      DTMeterGroups = new List<MeterGroups>();

      try
      {
        foreach (MeterGroups _meter_group in Enum.GetValues(typeof(MeterGroups)))
        {
          // TODO mejorar forma de excluir grupos.
          if (_is_tito && _meter_group == MeterGroups.GroupElectronicsFundsTransfer
            || !_is_tito && _meter_group == MeterGroups.GroupTickets
            || !_is_tito && _meter_group == MeterGroups.GroupBills)
          {
            continue;
          }

          DTMeterGroups.Add(_meter_group);
        }
        return true;
      }
      catch
      {
      }
      return false;

    } // GetMeterGroups

    //------------------------------------------------------------------------------
    // PURPOSE: Get Meter Ajustment Reason
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    public static Boolean GetMeterAjustmentReason(out List<MeterAjustmentReason> AjustmentReasons)
    {
      AjustmentReasons = new List<MeterAjustmentReason>();

      try
      {
        foreach (MeterAjustmentReason _meter_ajustment_reason in Enum.GetValues(typeof(MeterAjustmentReason)))
        {
          AjustmentReasons.Add(_meter_ajustment_reason);
        }
        return true;
      }
      catch
      {
      }
      return false;

    } // GetMeterAjustmentReason

    //------------------------------------------------------------------------------
    // PURPOSE : Get Delta of one Machine Meter 
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - MachineMeterName
    //          - OldMachineMeter
    //          - NewMachineMeter
    //          - MachineMeterMaxValue
    //          - RollOverPct
    //
    //      - OUTPUT :
    //          - DeltaMachineMeter
    //
    // RETURNS :
    //
    //   NOTES : 
    //
    public static Boolean GetDeltaMeter(Int32 TerminalId,
                                       Int32 MeterCode,
                                       String MeterName,
                                       Int64 OldMeterValue,
                                       Int64 NewMeterValue,
                                       Int64 MeterMaxValue,
                                       out Int64 DeltaMeterValue,
                                       out Boolean HasRollover,
                                       out Int64 MaxUnitsIncrement)
    { //  for GUI Ajustment
      Boolean _big_increment;
      HasRollover = false;

      SAS_Meter.GetBigIncrement(MeterCode, out MaxUnitsIncrement, TerminalId);

      _big_increment = GetDeltaMeter(TerminalId, MeterName, OldMeterValue, NewMeterValue, -1, MeterMaxValue, MaxUnitsIncrement, out DeltaMeterValue);

      HasRollover = (OldMeterValue > NewMeterValue) || (MeterMaxValue < DeltaMeterValue);

      return _big_increment;
    } // GetDeltaMeter for GUI Ajustment

    public static Boolean GetDeltaMeter(Int32 TerminalId,
                                        String MeterName,
                                        Int64 OldMeterValue,
                                        Int64 NewMeterValue,
                                        Int64 MeterMaxValue,
                                        Int64 MaxUnitsIncrement,
                                        out Int64 DeltaMeterValue)
    {
      return GetDeltaMeter(TerminalId, MeterName, OldMeterValue, NewMeterValue, -1, MeterMaxValue, MaxUnitsIncrement, out DeltaMeterValue);
    } // GetDeltaMeter

    public static Boolean GetDeltaMeter(Int32 TerminalId,
                                        String MeterName,
                                        Int64 OldMeterValue,
                                        Int64 NewMeterValue,
                                        Int64 OldMeterMaxValue,
                                        Int64 NewMeterMaxValue,
                                        Int64 MaxUnitsIncrement,
                                        out Int64 DeltaMeterValue)
    {
      Boolean _big_increment;

      _big_increment = false;
      DeltaMeterValue = 0;

      if (NewMeterMaxValue == 0)
      {
        return false;
      }

      if (NewMeterMaxValue <= OldMeterValue)
      {
        return false;
      }

      if (OldMeterMaxValue != NewMeterMaxValue)
      {
        if (OldMeterMaxValue != -1 && NewMeterMaxValue != -1)
        {
          return false;
        }
      }

      if (NewMeterValue >= OldMeterValue)
      {
        DeltaMeterValue = NewMeterValue - OldMeterValue;
      }
      else
      {
        DeltaMeterValue = NewMeterValue + (NewMeterMaxValue - OldMeterValue);
      }

      if (DeltaMeterValue > MaxUnitsIncrement)
      {
        Log.Warning("BigIncrement. TerminalId: " + TerminalId.ToString() +
                                         ", MeterName: " + MeterName +
                                         ", New: " + NewMeterValue.ToString() +
                                         ", Old: " + OldMeterValue.ToString() +
                                         ", Max: " + NewMeterMaxValue.ToString() +
                                         ", BigIncrement: " + MaxUnitsIncrement.ToString());

        _big_increment = true;
      }

      return _big_increment;

    } // GetDeltaMeter

    //------------------------------------------------------------------------------
    // PURPOSE: Load Meter Data
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //
    public static ITerminalMeterGroup LoadMeterData(DateTime WorkDay, Int64[] Terminals, MeterGroups Group)
    {
      return LoadMeterData(WorkDay, Terminals, Group, String.Empty);
    }

    public static ITerminalMeterGroup LoadMeterData(DateTime WorkDay, Int64[] Terminals, MeterGroups Group, String SitesId)
    {
      ITerminalMeterGroup _terminal_meter_group;

      _terminal_meter_group = null;

      switch (Group)
      {
        case MeterGroups.GroupStatistics:
          _terminal_meter_group = new MeterGroup_Statistics(Terminals, WorkDay, SitesId);
          break;
        case MeterGroups.GroupElectronicsFundsTransfer:
          _terminal_meter_group = new MeterGroup_ElectronicsFundsTransfer(Terminals, WorkDay, SitesId);
          break;
        case MeterGroups.GroupTickets:
          _terminal_meter_group = new MeterGroup_Tickets(Terminals, WorkDay, SitesId);
          break;
        case MeterGroups.GroupBills:
          _terminal_meter_group = new MeterGroup_Bills(Terminals, WorkDay, SitesId);
          break;
      }
      return _terminal_meter_group;
    }


  } // class TerminalMeterGroup 

    

  public class MeterGroup_Statistics : ITerminalMeterGroup
  {
    private DataTable m_meters_terminal_workday;
    private DataTable m_history;
    private List<Meter> m_meters_of_stats;
    private MeterRelationsBD m_dic_meter_relation;
    private String m_site_id = "";

    public List<Meter> Meters
    {
      get
      {
        return m_meters_of_stats;
      }
    }

    public MeterRelationsBD MeterRelationDB
    {
      get
      {
        if (m_dic_meter_relation == null)
        {
          m_dic_meter_relation = MeterRelationsBD.GetMetersRelationsBD(this);
        }
        return m_dic_meter_relation;
      }
    }

    // constructor
    public MeterGroup_Statistics(Int64[] Terminals, DateTime Workday, String SitesId)
    {
      //MeterRelationsBD _meter_relations_BD;
      m_meters_of_stats = new List<Meter>();

      //  m_meters_descriptions.Select("SMC_METER_CODE = " + _meter)[0][1]

      m_meters_of_stats.Add(new Meter(0x0000, "xCoin in", typeof(Currency)));
      m_meters_of_stats.Add(new Meter(0x0001, "xCoin out", typeof(Currency)));
      m_meters_of_stats.Add(new Meter(0x0002, "xJackpot", typeof(Currency)));
      m_meters_of_stats.Add(new Meter(0x0005, "xPlayed", typeof(Int64)));
      m_meters_of_stats.Add(new Meter(0x0006, "xPlayed", typeof(Int64)));

      ReadMeters(Terminals, Workday, SitesId); // meters para jornada terminal..

      //m_dic_meter_relation = MeterRelationsBD.GetMetersRelationsBD(this);

      // relation..
      //m_dic_meter_relation = new List<MeterRelationsBD>();
      //_meter_relations_BD = new MeterRelationsBD(0x0000);
      //_meter_relations_BD.Add("MACHINE_STATS_PER_HOUR", "MSH_PLAYED_AMOUNT");
      //_meter_relations_BD.Add("SALES_PER_HOUR", "SPH_PLAYED_AMOUNT");
      //m_dic_meter_relation.Add(_meter_relations_BD);

      //_meter_relations_BD = new MeterRelationsBD(0x0001);
      //_meter_relations_BD.Add("MACHINE_STATS_PER_HOUR", "MSH_WON_AMOUNT");
      //_meter_relations_BD.Add("SALES_PER_HOUR", "SPH_WON_AMOUNT");
      //m_dic_meter_relation.Add(_meter_relations_BD);

      //_meter_relations_BD = new MeterRelationsBD(0x0002);
      //_meter_relations_BD.Add("MACHINE_STATS_PER_HOUR", "MSH_JACKPOT_AMOUNT");
      //_meter_relations_BD.Add("SALES_PER_HOUR", "SPH_JACKPOT_AMOUNT");
      //m_dic_meter_relation.Add(_meter_relations_BD);

      //_meter_relations_BD = new MeterRelationsBD(0x0005);
      //_meter_relations_BD.Add("MACHINE_STATS_PER_HOUR", "MSH_PLAYED_COUNT");
      //_meter_relations_BD.Add("SALES_PER_HOUR", "SPH_PLAYED_COUNT");
      //m_dic_meter_relation.Add(_meter_relations_BD);

      //_meter_relations_BD = new MeterRelationsBD(0x0006);
      //_meter_relations_BD.Add("MACHINE_STATS_PER_HOUR", "MSH_WON_COUNT");
      //_meter_relations_BD.Add("SALES_PER_HOUR", "SPH_WON_COUNT");
      //m_dic_meter_relation.Add(_meter_relations_BD);

    }

    public DataTable MetersData
    {
      get
      {
        return m_meters_terminal_workday;
      }
      set
      {
        m_meters_terminal_workday = value;
      }
    } // MetersData

    public DataTable HistoryMetersData
    {
      get
      {
        return m_history;
      }
      set
      {
        m_history = value;
      }
    } // HistoryMetersData

    // LTC  29-OCT-2017
    public String SiteId { get { return m_site_id; } set { m_site_id = value; } }

    public Boolean ReadMeters(Int64[] Terminals, DateTime Workday)
    {
      return ReadMeters(Terminals, Workday, String.Empty);
    }

    public Boolean ReadMeters(Int64[] Terminals, DateTime Workday, String SitesId)
    {
      // select return data table
      if (!TerminalMeterGroup.ReadMetersOfGroupByTerminalAndWorkday(Terminals, Workday, Meters, out m_meters_terminal_workday, SitesId))
      {
        return false;
      }
      m_meters_terminal_workday.AcceptChanges();

      m_history = TerminalMeterGroup.CreateHistoryMetersGroupDataTable();
      if (!TerminalMeterGroup.ReadHistory(Terminals, Workday, Meters, out m_history))
      {
        return false;
      }
      m_history.AcceptChanges();
      return true;
    } // ReadMeters

    public TerminalMeterGroup.ResultCode UpdateMeters()
    {
      StringBuilder _sb;
      TerminalMeterGroup.ResultCode _result_code;

      _result_code = TerminalMeterGroup.ResultCode.ERROR_GENERIC;

      _sb = new StringBuilder();
      try
      {
        TerminalMeterGroup.SiteId = m_site_id;
        _result_code = TerminalMeterGroup.SaveData(MetersData, Meters, MeterRelationDB);

        if (_result_code == TerminalMeterGroup.ResultCode.OK)
        {
          MetersData.AcceptChanges();
        }

      } // try
      catch (Exception _ex)
      {
        _result_code = TerminalMeterGroup.ResultCode.ERROR_GENERIC;
        Log.Exception(_ex);
      }
      return _result_code;

    } // UpdateMeters

    public Boolean Recalculation()
    {
      return true;
    } // Recalculation

  } // class MeterGroup_Statistics

  public class MeterGroup_ElectronicsFundsTransfer : ITerminalMeterGroup
  {
    private DataTable m_meters_terminal_workday;
    private DataTable m_history;
    private List<Meter> m_meters_of_electronics_funds_transfer;// meters que compone el grupo.
    private MeterRelationsBD m_meter_relation_db;
    private String m_site_id = "";

    public List<Meter> Meters
    {
      get
      {
        return m_meters_of_electronics_funds_transfer;
      }
    }

    public MeterRelationsBD MeterRelationDB
    {
      get
      {
        if (m_meter_relation_db == null)
        {
          m_meter_relation_db = MeterRelationsBD.GetMetersRelationsBD(this);
        }
        return m_meter_relation_db;
      }
    }

    public MeterGroup_ElectronicsFundsTransfer(Int64[] Terminals, DateTime Workday)
    {
      m_meters_of_electronics_funds_transfer = new List<Meter>();

      //  m_meters_descriptions.Select("SMC_METER_CODE = " + _meter)[0][1]

      m_meters_of_electronics_funds_transfer.Add(new Meter(0x00A0, "xCoin in", typeof(Currency)));
      m_meters_of_electronics_funds_transfer.Add(new Meter(0x00B8, "xCoin out", typeof(Currency)));
      m_meters_of_electronics_funds_transfer.Add(new Meter(0x0003, "xJackpot", typeof(Currency)));

      ReadMeters(Terminals, Workday);

      //m_dic_meter_relation = MeterRelationsBD.GetMetersRelationsBD(this);

      //// relation..
      //m_dic_meter_relation = new List<MeterRelationsBD>();
      //_meter_relations_BD = new MeterRelationsBD(0x00A0);
      //_meter_relations_BD.Add("MACHINE_STATS_PER_HOUR", "MSH_TO_GM_AMOUNT");
      //m_dic_meter_relation.Add(_meter_relations_BD);

      //_meter_relations_BD = new MeterRelationsBD(0x00B8);
      //_meter_relations_BD.Add("MACHINE_STATS_PER_HOUR", "MSH_TO_GM_COUNT");
      //m_dic_meter_relation.Add(_meter_relations_BD);

      //_meter_relations_BD = new MeterRelationsBD(0x0003);
      //_meter_relations_BD.Add("MACHINE_STATS_PER_HOUR", "MSH_HPC_HANDPAYS_AMOUNT");
      //m_dic_meter_relation.Add(_meter_relations_BD);
    }

    // Constructor
    public MeterGroup_ElectronicsFundsTransfer(Int64[] Terminals, DateTime Workday, String SitesId)
    {
      m_meters_of_electronics_funds_transfer = new List<Meter>();

      m_meters_of_electronics_funds_transfer.Add(new Meter(0x00A0, "xCoin in", typeof(Currency)));
      m_meters_of_electronics_funds_transfer.Add(new Meter(0x00B8, "xCoin out", typeof(Currency)));
      m_meters_of_electronics_funds_transfer.Add(new Meter(0x0003, "xJackpot", typeof(Currency)));

      ReadMeters(Terminals, Workday, SitesId);

      //m_dic_meter_relation = MeterRelationsBD.GetMetersRelationsBD(this);

      //// relation..
      //m_dic_meter_relation = new List<MeterRelationsBD>();
      //_meter_relations_BD = new MeterRelationsBD(0x00A0);
      //_meter_relations_BD.Add("MACHINE_STATS_PER_HOUR", "MSH_TO_GM_AMOUNT");
      //m_dic_meter_relation.Add(_meter_relations_BD);

      //_meter_relations_BD = new MeterRelationsBD(0x00B8);
      //_meter_relations_BD.Add("MACHINE_STATS_PER_HOUR", "MSH_TO_GM_COUNT");
      //m_dic_meter_relation.Add(_meter_relations_BD);

      //_meter_relations_BD = new MeterRelationsBD(0x0003);
      //_meter_relations_BD.Add("MACHINE_STATS_PER_HOUR", "MSH_HPC_HANDPAYS_AMOUNT");
      //m_dic_meter_relation.Add(_meter_relations_BD);

    }

    public DataTable MetersData
    {
      get
      {
        return m_meters_terminal_workday;
      }
      set
      {
        m_meters_terminal_workday = value;
      }
    } // MetersData

    public DataTable HistoryMetersData
    {
      get
      {
        return m_history;
      }
      set
      {
        m_history = value;
      }
    } // HistoryMetersData

    public String SiteId { get { return m_site_id; } set { m_site_id = value; } }

    public Boolean ReadMeters(Int64[] Terminals, DateTime Workday)
    {
      return ReadMeters(Terminals, Workday, String.Empty);
    }

    public Boolean ReadMeters(Int64[] Terminals, DateTime Workday, String SitesId)
    {
      // select return data table
      if (!TerminalMeterGroup.ReadMetersOfGroupByTerminalAndWorkday(Terminals, Workday, Meters, out m_meters_terminal_workday, SitesId))
      {
        return false;
      }
      m_meters_terminal_workday.AcceptChanges();

      m_history = TerminalMeterGroup.CreateHistoryMetersGroupDataTable();
      if (!TerminalMeterGroup.ReadHistory(Terminals, Workday, Meters, out m_history))
      {
        return false;
      }
      m_history.AcceptChanges();

      return true;
    } // ReadMeters

    public TerminalMeterGroup.ResultCode UpdateMeters()
    {
      StringBuilder _sb;
      TerminalMeterGroup.ResultCode _result_code;

      _result_code = TerminalMeterGroup.ResultCode.ERROR_GENERIC;

      _sb = new StringBuilder();
      try
      {
        TerminalMeterGroup.SiteId = m_site_id;
        _result_code = TerminalMeterGroup.SaveData(MetersData, Meters, MeterRelationDB);

        if (_result_code == TerminalMeterGroup.ResultCode.OK)
        {
          MetersData.AcceptChanges();
        }

      } // try
      catch (Exception _ex)
      {
        _result_code = TerminalMeterGroup.ResultCode.ERROR_GENERIC;
        Log.Exception(_ex);
      }
      return _result_code;

    } // UpdateMeters

    public Boolean Recalculation()
    {
      return true;
    } // Recalculation

  } //class MeterGroup_ElectronicsFundsTransfer 

  public class MeterGroup_Tickets : ITerminalMeterGroup
  {
    private DataTable m_meters_terminal_workday;
    private DataTable m_history;
    private List<Meter> m_meters_of_Tickets;  // meters que compone el grupo.
    private MeterRelationsBD m_meter_relation_db;
    private String m_site_id = "";

    public List<Meter> Meters
    {
      get
      {
        return m_meters_of_Tickets;
      }
    }

    public MeterRelationsBD MeterRelationDB
    {
      get
      {
        if (m_meter_relation_db == null)
        {
          m_meter_relation_db = MeterRelationsBD.GetMetersRelationsBD(this);
        }
        return m_meter_relation_db;
      }
    }

    // Constructor
    public MeterGroup_Tickets(Int64[] Terminals, DateTime Workday, String SitesId)
    {
      m_meters_of_Tickets = new List<Meter>();

      m_meters_of_Tickets.Add(new Meter(0x0080, "xTicket IN redimible (amt)", typeof(Currency)));
      m_meters_of_Tickets.Add(new Meter(0x0081, "xTicket IN redimible (qty)", typeof(Int64)));
      m_meters_of_Tickets.Add(new Meter(0x0082, "xTicket IN promo NO redimible (amt)", typeof(Currency)));
      m_meters_of_Tickets.Add(new Meter(0x0083, "xTicket IN promo NO redimible (qty)", typeof(Int64)));
      m_meters_of_Tickets.Add(new Meter(0x0084, "xTicket IN promo redimible (amt)", typeof(Currency)));
      m_meters_of_Tickets.Add(new Meter(0x0085, "xTicket IN promo redimible (qty)", typeof(Int64)));

      m_meters_of_Tickets.Add(new Meter(0x0086, "xTicket OUT redimible (amt)", typeof(Currency)));
      m_meters_of_Tickets.Add(new Meter(0x0087, "xTicket OUT redimible (qty)", typeof(Int64)));
      m_meters_of_Tickets.Add(new Meter(0x0088, "xTicket OUT promo NO redimible (amt)", typeof(Currency)));
      m_meters_of_Tickets.Add(new Meter(0x0089, "xTicket OUT promo NO redimible (qty)", typeof(Int64)));

      ReadMeters(Terminals, Workday, SitesId);

    }

    public DataTable MetersData
    {
      get
      {
        return m_meters_terminal_workday;
      }
      set
      {
        m_meters_terminal_workday = value;
      }
    } // MetersData

    public DataTable HistoryMetersData
    {
      get
      {
        return m_history;
      }
      set
      {
        m_history = value;
      }
    } // HistoryMetersData

    public String SiteId { get { return m_site_id; } set { m_site_id = value; } }

    public Boolean ReadMeters(Int64[] Terminals, DateTime Workday)
    {
      return ReadMeters(Terminals, Workday, String.Empty);
    }

    public Boolean ReadMeters(Int64[] Terminals, DateTime Workday, String SitesId)
    {
      // select return data table
      if (!TerminalMeterGroup.ReadMetersOfGroupByTerminalAndWorkday(Terminals, Workday, Meters, out m_meters_terminal_workday, SiteId))
      {
        return false;
      }
      m_meters_terminal_workday.AcceptChanges();

      m_history = TerminalMeterGroup.CreateHistoryMetersGroupDataTable();
      if (!TerminalMeterGroup.ReadHistory(Terminals, Workday, Meters, out m_history))
      {
        return false;
      }
      m_history.AcceptChanges();

      return true;
    } // ReadMeters

    public TerminalMeterGroup.ResultCode UpdateMeters()
    {
      StringBuilder _sb;
      TerminalMeterGroup.ResultCode _result_code;

      _result_code = TerminalMeterGroup.ResultCode.ERROR_GENERIC;

      _sb = new StringBuilder();
      try
      {
        TerminalMeterGroup.SiteId = m_site_id;
        _result_code = TerminalMeterGroup.SaveData(MetersData, Meters, MeterRelationDB);

        if (_result_code == TerminalMeterGroup.ResultCode.OK)
        {
          MetersData.AcceptChanges();
        }

      } // try
      catch (Exception _ex)
      {
        _result_code = TerminalMeterGroup.ResultCode.ERROR_GENERIC;
        Log.Exception(_ex);
      }
      return _result_code;

    } // UpdateMeters

    public Boolean Recalculation()
    {
      return true;
    } // Recalculation

  } //class MeterGroup_Tickets 

  public class MeterGroup_Bills : ITerminalMeterGroup
  {
    private DataTable m_meters_terminal_workday;
    private DataTable m_history;
    private List<Meter> m_meters_of_bills;    // meters que compone el grupo.
    private MeterRelationsBD m_meter_relation_db;
    private String m_site_id = "";

    public List<Meter> Meters
    {
      get
      {
        return m_meters_of_bills;
      }
    }

    public MeterRelationsBD MeterRelationDB
    {
      get
      {
        if (m_meter_relation_db == null)
        {
          m_meter_relation_db = MeterRelationsBD.GetMetersRelationsBD(this);
        }
        return m_meter_relation_db;
      }
    }

    // Constructor
    public MeterGroup_Bills(Int64[] Terminals, DateTime Workday, String SitesId)
    {
      m_meters_of_bills = new List<Meter>();
      List<Int64> _compute_meters;
      _compute_meters = new List<Int64>();

      if (!FillGroupMeters(Terminals, Workday))
      {
        return;
      }
      foreach (Meter _meter in m_meters_of_bills)
      {
        // Avoid to add coins meter to compute total bills
        if (_meter.Code != 0x0008)
        {
        _compute_meters.Add(_meter.Code);
      }
      }
      m_meters_of_bills.Add(new Meter(0x000B, "xTotal credits from bills accepted", typeof(Int64), false, false, MetersComputedOperations.OPERATION_SUM_BY_TOTAL_METER_DENOMINATION, _compute_meters));

      ReadMeters(Terminals, Workday);
    }

    private Boolean FillGroupMeters(Int64[] Terminals, DateTime Workday)
    {
      StringBuilder _sb;
      DataTable _temp_meters;
      String _terminals_id_list;

      _temp_meters = new DataTable();
      _terminals_id_list = "";

      foreach (Int32 _terminal in Terminals)
      {
        _terminals_id_list += _terminal.ToString() + ", ";
      }
      _terminals_id_list = _terminals_id_list.Substring(0, _terminals_id_list.Length - 2);
      //todo  ejemplo String.Join(",", _user_type_list.ToArray()

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" WITH #TEMP_METERS AS (");
        _sb.AppendLine("   SELECT  8   AS METER_CODE, 0 AS DENOMINATION UNION ALL");
        _sb.AppendLine("   SELECT 64,  1.00 UNION ALL      ");
        _sb.AppendLine("   SELECT 65,  2.00 UNION ALL      ");
        _sb.AppendLine("   SELECT 66,  5.00 UNION ALL      ");
        _sb.AppendLine("   SELECT 67,  10.00 UNION ALL     ");
        _sb.AppendLine("   SELECT 68,  20.00 UNION ALL     ");
        _sb.AppendLine("   SELECT 69,  25.00 UNION ALL     ");
        _sb.AppendLine("   SELECT 70,  50.00 UNION ALL     ");
        _sb.AppendLine("   SELECT 71,  100.00 UNION ALL    ");
        _sb.AppendLine("   SELECT 72,  200.00 UNION ALL    ");
        _sb.AppendLine("   SELECT 73,  250.00 UNION ALL    ");
        _sb.AppendLine("   SELECT 74,  500.00 UNION ALL    ");
        _sb.AppendLine("   SELECT 75,  1000.00 UNION ALL   ");
        _sb.AppendLine("   SELECT 76,  2000.00 UNION ALL   ");
        _sb.AppendLine("   SELECT 77,  2500.00 UNION ALL   ");
        _sb.AppendLine("   SELECT 78,  5000.00 UNION ALL   ");
        _sb.AppendLine("   SELECT 79,  10000.00 UNION ALL  ");
        _sb.AppendLine("   SELECT 80,  20000.00 UNION ALL  ");
        _sb.AppendLine("   SELECT 81,  25000.00 UNION ALL  ");
        _sb.AppendLine("   SELECT 82,  50000.00 UNION ALL  ");
        _sb.AppendLine("   SELECT 83,  100000.00 UNION ALL ");
        _sb.AppendLine("   SELECT 84,  200000.00 UNION ALL ");
        _sb.AppendLine("   SELECT 85,  250000.00 UNION ALL ");
        _sb.AppendLine("   SELECT 86,  500000.00 UNION ALL ");
        _sb.AppendLine("   SELECT 87,  1000000.00 )        ");
        _sb.AppendLine("");
        _sb.AppendLine(" SELECT   DISTINCT METER_CODE ");
        _sb.AppendLine("                 , METER_DESCRIPTION ");
        _sb.AppendLine("                 , DENOMINATION * 100 AS DENOMINATION_CENT ");
        _sb.AppendLine("   FROM ( SELECT   SMC_METER_CODE AS METER_CODE ");
        _sb.AppendLine("                 , SMC_DESCRIPTION AS METER_DESCRIPTION ");
        _sb.AppendLine("                 , DENOMINATION ");
        _sb.AppendLine("            FROM   SAS_METERS_CATALOG ");
        _sb.AppendLine("           INNER   JOIN #TEMP_METERS    ON METER_CODE = SMC_METER_CODE ");
        _sb.AppendLine("           INNER   JOIN CAGE_CURRENCIES ON CGC_ISO_CODE IN (SELECT GP_KEY_VALUE ");
        _sb.AppendLine("                                                              FROM GENERAL_PARAMS ");
        _sb.AppendLine("                                                             WHERE GP_GROUP_KEY = 'RegionalOptions' ");
        _sb.AppendLine("                                                               AND GP_SUBJECT_KEY = 'CurrencyISOCode') ");
        _sb.AppendLine("                                       AND CGC_DENOMINATION = DENOMINATION ");
        _sb.AppendLine("                                       AND CGC_DENOMINATION >= 1 ");
        _sb.AppendLine("                                       AND CGC_ALLOWED = 1 ");
        _sb.AppendLine("            UNION   ALL ");
        _sb.AppendLine("           SELECT   DISTINCT TSMH_METER_CODE ");
        _sb.AppendLine("                  , SMC_DESCRIPTION AS METER_DESCRIPTION ");
        _sb.AppendLine("                  , DENOMINATION ");
        _sb.AppendLine("             FROM   TERMINAL_SAS_METERS_HISTORY ");
        _sb.AppendLine("            INNER   JOIN SAS_METERS_CATALOG  ON TSMH_METER_CODE = SMC_METER_CODE ");
        _sb.AppendLine("            INNER   JOIN #TEMP_METERS        ON SMC_METER_CODE  = METER_CODE ");
        _sb.AppendLine("            WHERE   TSMH_DATETIME >= @pWorkdayFrom ");
        _sb.AppendLine("              AND   TSMH_DATETIME <  @pWorkdayTo ");
        _sb.AppendLine("              AND   TSMH_TYPE = 20 ");
        _sb.AppendFormat("            AND   TSMH_TERMINAL_ID IN ( {0} ) ", _terminals_id_list);
        _sb.AppendLine(" ");
        _sb.AppendLine("              AND   (TSMH_METER_CODE BETWEEN 64 AND 87 OR TSMH_METER_CODE = 8) ");
        _sb.AppendLine("              AND   TSMH_METER_INCREMENT <> 0 ");

        _sb.AppendLine("            UNION   ALL ");
        _sb.AppendLine("           SELECT   SMC_METER_CODE AS METER_CODE ");
        _sb.AppendLine("                  , SMC_DESCRIPTION AS METER_DESCRIPTION ");
        _sb.AppendLine("                  , DENOMINATION ");
        _sb.AppendLine("             FROM   SAS_METERS_CATALOG  ");
        _sb.AppendLine("            INNER   JOIN #TEMP_METERS    ON METER_CODE = SMC_METER_CODE ");
        _sb.AppendFormat("          INNER   JOIN TERMINALS ON TE_TERMINAL_ID IN ( {0} ) ", _terminals_id_list);
        _sb.AppendLine("            WHERE   TE_COIN_COLLECTION = 1 AND METER_CODE = 8 ");
        _sb.AppendLine("        ) AS A");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pWorkdayFrom", SqlDbType.DateTime).Value = Workday;
            _sql_cmd.Parameters.Add("@pWorkdayTo", SqlDbType.DateTime).Value = Workday.AddDays(1);

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(_temp_meters);
            }
          } // SqlCommand
        } // DB_TRX

        foreach (DataRow _row in _temp_meters.Rows)
        {
          if ((Int32)_row[0] == 0x0008)
          {
            m_meters_of_bills.Add(new Meter((Int32)_row[0], "x" + (String)_row[1], typeof(Currency), (Decimal)_row[2]));
          }
          else
          {
            m_meters_of_bills.Add(new Meter((Int32)_row[0], "x" + (String)_row[1], typeof(Int64), (Decimal)_row[2]));
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public DataTable MetersData
    {
      get
      {
        return m_meters_terminal_workday;
      }
      set
      {
        m_meters_terminal_workday = value;
      }
    } // MetersData

    public DataTable HistoryMetersData
    {
      get
      {
        return m_history;
      }
      set
      {
        m_history = value;
      }
    } // HistoryMetersData

    public String SiteId { get { return m_site_id; } set { m_site_id = value; } }

    public Boolean ReadMeters(Int64[] Terminals, DateTime Workday)
    {
      return ReadMeters(Terminals, Workday, String.Empty);
    }

    public Boolean ReadMeters(Int64[] Terminals, DateTime Workday, String SitesId)
    {
      // select return data table
      if (!TerminalMeterGroup.ReadMetersOfGroupByTerminalAndWorkday(Terminals, Workday, Meters, out m_meters_terminal_workday, SitesId))
      {
        return false;
      }
      m_meters_terminal_workday.AcceptChanges();

      m_history = TerminalMeterGroup.CreateHistoryMetersGroupDataTable();
      if (!TerminalMeterGroup.ReadHistory(Terminals, Workday, Meters, out m_history))
      {
        return false;
      }
      m_history.AcceptChanges();

      return true;
    } // ReadMeters

    public TerminalMeterGroup.ResultCode UpdateMeters()
    {
      StringBuilder _sb;
      TerminalMeterGroup.ResultCode _result_code;

      _result_code = TerminalMeterGroup.ResultCode.ERROR_GENERIC;

      _sb = new StringBuilder();
      try
      {
        if (!Recalculation())
        {
          return _result_code;
        }

        TerminalMeterGroup.SiteId = m_site_id;
        _result_code = TerminalMeterGroup.SaveData(MetersData, Meters, MeterRelationDB);

        if (_result_code == TerminalMeterGroup.ResultCode.OK)
        {
          MetersData.AcceptChanges();
        }

      } // try
      catch (Exception _ex)
      {
        _result_code = TerminalMeterGroup.ResultCode.ERROR_GENERIC;
        Log.Exception(_ex);
      }
      return _result_code;

    } // UpdateMeters

    public Boolean Recalculation()
    {
      foreach (Meter _meter in m_meters_of_bills)
      {
        if (_meter.IsComputed)
        {
          if (!MeterComputeColumns.ComputeMeterColumn(_meter.Code, m_meters_of_bills, MetersData, "METER_CODE", "DELTA_SYSTEM"))
          {
            return false;
          }
        }
      }
      return true;
    } // Recalculation

  } //class MeterGroup_Bills 

} // WSI.Common
