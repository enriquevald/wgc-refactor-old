//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: HtmlPrinter.cs
// 
//   DESCRIPTION: Classes to manage Html Printer
// 
//        AUTHOR: Alberto Cuesta 
// 
// CREATION DATE: 10-AUG-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-AUG-2012 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing.Printing;
using System.Management;

namespace WSI.Common
{
  public delegate void AddHtmlElementEventHandler();

  public static class HtmlPrinter
  {
    #region Members

    private const String EPS_PRINTER_NAME = "EPSON BA-T500 Receipt";

    private static Thread m_thread;
    private static ReaderWriterLock m_lock;
    private static WebBrowser m_web_browser = null;
    private static WaitableQueue m_queue;
    private static Int32 m_file_sequence;
    private static Form m_form;
    private static System.Windows.Forms.Timer m_timer_print_processed;
    private static Boolean m_processing = false;
    private static Int32 m_processed_tick;
    private static Boolean m_init = false;
    private static String m_vouchers_path;
    private static String m_printer_name = null;
    private static Boolean m_is_epson_bat500_model = false;
    private static Boolean m_queue_full = false;
    private static Int32 m_queue_full_tick = 0;

    #endregion

    #region Private Functions

    static public void TestPrinter()
    {
      PrinterSettings _printer_settings;
      String _desired_printer;

      // Desired Printer not equals to the WebBrowser Printer
      _printer_settings = new PrinterSettings();
      _desired_printer = _printer_settings.PrinterName;

      if (_desired_printer != m_printer_name
          || m_web_browser == null)
      {
        //
        // Printer Changed!
        // - Create a new web browser
        //
        if (m_web_browser != null)
        {
          m_form.Controls.Remove(m_web_browser);
        }

        m_web_browser = new WebBrowser();
        m_web_browser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(OnDocumentCompleted);
        m_form.Controls.Add(m_web_browser);

        m_printer_name = _desired_printer;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Main thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    static private void PrintingThread()
    {
      PrinterSettings _dummy_printer_settings;

      try
      {
        m_form = new Form();

        Log.Message("Initializating printer ...");

        // Configure extra delay for "EPSON BA-T500 Receipt" printer
        _dummy_printer_settings = new PrinterSettings();
        m_printer_name = _dummy_printer_settings.PrinterName;

        if (String.Compare(m_printer_name, EPS_PRINTER_NAME, true) == 0)
        {
          m_is_epson_bat500_model = true;
        }

        // Create a web browser linked to the default printer
        TestPrinter();

        Log.Message("Printer Initializated.");

        //
        // Create Timer Print Processed
        //
        m_timer_print_processed = new System.Windows.Forms.Timer();
        m_timer_print_processed.Enabled = false;
        m_timer_print_processed.Interval = 1000;
        m_timer_print_processed.Tick += new System.EventHandler(OnTimerPrintProcessedTick);

        m_form.Opacity = 0;
        m_form.Show();
        m_form.Hide();

        m_init = true;

        // Create a Standard application message loop on the current thread, without a form
        Application.Run();
        //Application.Run(m_form);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return;
      }

    } // PeriodicJobThread

    //------------------------------------------------------------------------------
    // PURPOSE: Event handler when Timer print processed Tick.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private static void OnTimerPrintProcessedTick(object Sender, EventArgs EventArgs)
    {
      try
      {
        m_timer_print_processed.Stop();

        // ACC 20-SEP-2012 For "EPSON BA-T500 Receipt" printer
        if (m_is_epson_bat500_model)
        {
          Int32 _printer_status;
          Int32 _printer_num_jobs_enqueued;

          if (Printer.GetStatus(out _printer_status, out _printer_num_jobs_enqueued))
          {
            _printer_num_jobs_enqueued -= EnqueuedItems();
            if (_printer_num_jobs_enqueued > 1)
            {
              if (!m_queue_full)
              {
                m_queue_full_tick = Misc.GetTickCount();
              }

              if (Misc.GetElapsedTicks(m_queue_full_tick) > 2000)
              {
                PrinterJobs.CancelHungPrintJob(m_printer_name);
              }

              m_queue_full = true;
              m_timer_print_processed.Interval = 250;
              m_timer_print_processed.Start();

              return;
            }
          }
        }

        m_queue_full = false;
        m_lock.AcquireWriterLock(Timeout.Infinite);
        m_timer_print_processed.Interval = 1000;
        m_processed_tick = Misc.GetTickCount();
        m_processing = false;
        m_lock.ReleaseWriterLock();

        m_form.BeginInvoke(new AddHtmlElementEventHandler(OnAddHtmlElement));
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return;
      }

    } // OnTimerPrintProcessedTick

    //------------------------------------------------------------------------------
    // PURPOSE: Event handler when Document is loaded completely.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Sender
    //          - EventArgs
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private static void OnDocumentCompleted(object Sender, WebBrowserDocumentCompletedEventArgs EventArgs)
    {
      try
      {
        WebBrowser _web_browser;

        _web_browser = (WebBrowser)Sender;
        // Print the document.
        _web_browser.Print();

        m_timer_print_processed.Start();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // OnDocumentCompleted

    //------------------------------------------------------------------------------
    // PURPOSE: Delete older temporary files ...
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private static void DeleteOldFiles(Boolean OnOldFolders)
    {
      String[] _folders;
      DirectoryInfo _dir;
      FileInfo[] _files;

      if (OnOldFolders)
      {
        _folders = new String[3];
        _folders[0] = m_vouchers_path;
        _folders[1] = Directory.GetCurrentDirectory();
        _folders[2] = Path.GetTempPath();
      }
      else
      {
        _folders = new String[1];
        _folders[0] = m_vouchers_path;
      }

      foreach (String _path in _folders)
      {
        try
        {
          _dir = new DirectoryInfo(_path);
          _files = _dir.GetFiles("HtmlPrinterVoucher*.html");

          foreach (FileInfo _file in _files)
          {
            try { File.Delete(Path.Combine(_dir.FullName, _file.Name)); }
            catch { }
          }
        }
        catch { }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Invoke Delegate Method.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private static void OnAddHtmlElement()
    {
      String _html_to_print;
      String _file_to_print;
      Object _obj;

      m_lock.AcquireWriterLock(Timeout.Infinite);
      try
      {
        if (m_processing)
        {
          return;
        }

        // Dequeue element
        if (!m_queue.Dequeue(out _obj, 0))
        {
          DeleteOldFiles(false);

          return;
        }

        if (Misc.GetElapsedTicks(m_processed_tick) > 10000)
        {
          m_timer_print_processed.Interval = 2000;
        }

        _html_to_print = (String)_obj;

        // Create file
        _file_to_print = SaveHTMLOnFile(_html_to_print);

        TestPrinter();

        // Load the document to print.
        m_web_browser.Navigate(_file_to_print);
        m_processing = true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        m_lock.ReleaseWriterLock();
      }

    } // OnAddHtmlElement

    //------------------------------------------------------------------------------
    // PURPOSE: Save HTML string into voucher file to be loaded afterwards.
    //
    //  PARAMS:
    //      - INPUT:
    //          - VoucherHTML
    //          - Index
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private static String SaveHTMLOnFile(String VoucherHTML)
    {
      FileStream _file_stream;
      StreamWriter _file_writer;
      String _voucher_filename;

      _file_writer = null;

      // Create a temporary file with voucher to print
      _voucher_filename = "HtmlPrinterVoucher" + m_file_sequence.ToString("0000") + ".html";
      _voucher_filename = Path.Combine(m_vouchers_path, _voucher_filename);
      m_file_sequence = (m_file_sequence + 1) % 10000;

      VoucherManager.CreateJSFile(m_vouchers_path);

      try
      {
        _file_stream = new FileStream(_voucher_filename, FileMode.Create, FileAccess.Write);
        _file_writer = new StreamWriter(_file_stream);

        // Write voucher
        _file_writer.BaseStream.Seek(0, SeekOrigin.Begin);
        _file_writer.Write(VoucherHTML);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        if (_file_writer != null)
        {
          _file_writer.Close();
        }
      }

      return _voucher_filename;
    } // SaveHTMLOnFile

    //------------------------------------------------------------------------------
    // PURPOSE: Invoke Delegate Method Stop.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private static void OnStop()
    {
      try
      {
        //Application.Exit();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

    } // OnStop

    #endregion

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Init
    //
    //  PARAMS :
    //      - INPUT: 
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : 
    //
    public static Boolean Init()
    {
      CustomWindow _cw;
      Int32 _started_thread_tick;

      if (m_init)
      {
        return true;
      }

      try
      {
        m_file_sequence = 1;

        try
        {
          // Start print delay hack
          _cw = new CustomWindow("PrintTray_Notify_WndClass");
        }
        catch { }

        // Init ReaderWriterLock
        m_lock = new ReaderWriterLock();

        // Create Queue 
        m_queue = new WaitableQueue();

        m_vouchers_path = Path.Combine(Directory.GetCurrentDirectory(), "Temp");
        m_vouchers_path = Path.Combine(m_vouchers_path, "HtmlPrinter");
        if (!Directory.Exists(m_vouchers_path))
        {
          try { Directory.CreateDirectory(m_vouchers_path); }
          catch { }
        }

        DeleteOldFiles(true);

        m_processed_tick = Misc.GetTickCount();

        // Create Thread
        m_thread = new Thread(PrintingThread);
        m_thread.Name = "HtmlPrinter.PrintingThread";
        m_thread.SetApartmentState(ApartmentState.STA);
        m_thread.Start();

        _started_thread_tick = Misc.GetTickCount();

        while (!m_init)
        {
          if (Misc.GetElapsedTicks(_started_thread_tick) > 10000)
          {
            // Not initialized correctly ...
            return false;
          }
            
          Thread.Sleep(100);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Init
    //
    //  PARAMS :
    //      - INPUT: 
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : 
    //
    public static void Stop()
    {
      try
      {
        m_form.BeginInvoke(new AddHtmlElementEventHandler(OnStop));
      }
      catch { }

    } // Stop

    //------------------------------------------------------------------------------
    // PURPOSE : Add new element into Queue
    //
    //  PARAMS :
    //      - INPUT: 
    //          - Html
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : 
    //
    public static void AddHtml(String Html)
    {
      if (!m_init)
      {
        return;
      }

      try
      {
        m_queue.Enqueue(Html);
        m_form.BeginInvoke(new AddHtmlElementEventHandler(OnAddHtmlElement));
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

    } // AddHtml

    //------------------------------------------------------------------------------
    // PURPOSE : Get enqueued elements
    //
    //  PARAMS :
    //      - INPUT: 
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Number of elements into queue
    //
    //   NOTES : 
    //
    public static Int32 EnqueuedItems()
    {
      Int32 _count_elements;

      if (!m_init)
      {
        return Int32.MaxValue;
      }

      try
      {
        _count_elements = m_queue.Count;

        if (m_processing)
        {
          _count_elements++;
        }

        return _count_elements;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return Int32.MaxValue;
    } // EnqueuedItems

    #endregion
  }

  #region CreatePrintClassWindow

  public class CustomWindow : IDisposable
  {
    private static WndProc _global_wnd_proc = new WndProc(CustomWndProc);

    #region Win32
    delegate IntPtr WndProc(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    struct WNDCLASS
    {
      public uint style;
      public WndProc lpfnWndProc;
      public int cbClsExtra;
      public int cbWndExtra;
      public IntPtr hInstance;
      public IntPtr hIcon;
      public IntPtr hCursor;
      public IntPtr hbrBackground;
      [MarshalAs(UnmanagedType.LPWStr)]
      public string lpszMenuName;
      [MarshalAs(UnmanagedType.LPWStr)]
      public string lpszClassName;
    }
    [DllImport("user32.dll", SetLastError = true)]
    static extern UInt16 RegisterClassW([In] ref WNDCLASS lpWndClass);
    [DllImport("user32.dll", SetLastError = true)]
    static extern IntPtr CreateWindowExW(UInt32 dwExStyle, [MarshalAs(UnmanagedType.LPWStr)]string lpClassName, [MarshalAs(UnmanagedType.LPWStr)]string lpWindowName, UInt32 dwStyle, Int32 x, Int32 y, Int32 nWidth, Int32 nHeight, IntPtr hWndParent, IntPtr hMenu, IntPtr hInstance, IntPtr lpParam);
    [DllImport("user32.dll", SetLastError = true)]
    static extern IntPtr DefWindowProcW(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);
    [DllImport("user32.dll", SetLastError = true)]
    static extern bool DestroyWindow(IntPtr hWnd);
    [DllImport("user32.dll", CharSet = CharSet.Unicode)]
    static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string lclassName, string windowTitle);

    const int ERROR_CLASS_ALREADY_EXISTS = 1410;
    #endregion

    public CustomWindow(string class_name)
    {
      if (FindWindowEx(IntPtr.Zero, IntPtr.Zero, class_name, String.Empty) != IntPtr.Zero)
      {
        return;
      }
      // Create WNDCLASS        
      WNDCLASS wind_class = new WNDCLASS();
      wind_class.lpszClassName = class_name;
      wind_class.lpfnWndProc += _global_wnd_proc;
      UInt16 class_atom = RegisterClassW(ref wind_class);
      int last_error = Marshal.GetLastWin32Error();
      if (class_atom == 0 && last_error != ERROR_CLASS_ALREADY_EXISTS)
      {
        throw new System.Exception("Could not register window class");
      }
      // Create window        
      CreateWindowExW(0, class_name, String.Empty, 0, 0, 0, 0, 0, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero);
    }
    private static IntPtr CustomWndProc(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam)
    {
      return DefWindowProcW(hWnd, msg, wParam, lParam);
    }
    public void Dispose()
    {
      throw new NotImplementedException();
    }
  }

  #endregion


  #region PrinterJobs

  public static class PrinterJobs
  {
    private const Int32 MAX_SENT_TO_PRINTER_TIME = 1000;  // Start counting after 5 secconds ==> 2 + 1 = 3 seconds

    private static Int32 m_last_sent_to_printer_job_id = 0;
    private static String m_last_sent_to_printer_document = "";
    private static Int32 m_last_sent_to_printer_tick = 0;

    public static void CancelHungPrintJob(string PrinterName)
    {
      String _search_query;
      ManagementObjectSearcher _search_print_jobs;
      ManagementObjectCollection _jobs;
      String _job_name;
      Char[] _split_arr;
      String _printer_name;
      int _job_id;
      String _document_name;
      String _job_status;
      String _status_mask = "";
      Int32 _status_mask_value;
      Boolean _first_job_in_queue;

      try
      {
        _search_query = "SELECT * FROM Win32_PrintJob";
        _search_print_jobs = new ManagementObjectSearcher(_search_query);
        _jobs = _search_print_jobs.Get();

        _first_job_in_queue = true;
        foreach (ManagementObject _job in _jobs)
        {
          _job_name = _job.Properties["Name"].Value.ToString();

          // Job name would be of the format [PrinterName], [Job ID]
          _split_arr = new char[1];
          _split_arr[0] = Convert.ToChar(",");

          _printer_name = _job_name.Split(_split_arr)[0];
          if (String.Compare(_printer_name, PrinterName, true) != 0)
          {
            continue;
          }

          _job_id = Convert.ToInt32(_job_name.Split(_split_arr)[1]);
          _document_name = _job.Properties["Document"].Value.ToString();
          _job_status = "";
          if (_job.Properties["JobStatus"].Value != null)
          {
            _job_status = _job.Properties["JobStatus"].Value.ToString();
          }
          _status_mask_value = 0;
          if (_job.Properties["StatusMask"].Value != null)
          {
            _status_mask = _job.Properties["StatusMask"].Value.ToString();
            if (!Int32.TryParse(_status_mask, out _status_mask_value))
            {
              _status_mask_value = 0;

            }
          }

          if (   _first_job_in_queue && _status_mask_value == 4096             // 4096 - JobCompleted
              && (   String.Compare(_job_status, "Sent to Printer", true) == 0
                  || _job_status == "") )
          {
            if (_job_id == m_last_sent_to_printer_job_id
                && String.Compare(_document_name, m_last_sent_to_printer_document, true) == 0)
            {
              if (Misc.GetElapsedTicks(m_last_sent_to_printer_tick) > MAX_SENT_TO_PRINTER_TIME)
              {
                // Performs a action similar to the cancel operation of windows print console.
                _job.Delete();

                m_last_sent_to_printer_job_id = 0;
                m_last_sent_to_printer_document = "";
              }
            }
            else
            {
              m_last_sent_to_printer_job_id = _job_id;
              m_last_sent_to_printer_document = _document_name;
              m_last_sent_to_printer_tick = Misc.GetTickCount();
            }

            break;
          }

          _first_job_in_queue = false;

        } // foreach

      } // try
      catch 
      {
        //MessageBox.Show("Exception: Step: *** " + _step + " *** Detail: " + _ex.Message);
      }

    } // CancelHungPrintJob

  } // class PrinterJobs

  #endregion

}
