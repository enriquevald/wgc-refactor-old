/////////////////////////////////////////////////////////////////////////////////////
//                                   CLASE OBSOLETA
/////////////////////////////////////////////////////////////////////////////////////


//------------------------------------------------------------------------------
// Copyright © 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ChipsAmount.cs
// 
//   DESCRIPTION: ChipsAmount class
// 
//        AUTHOR: David Lasdiez
// 
// CREATION DATE: 02-DEC-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-DEC-2013 DLL    First version.
// 09-JAN-2014 DLL    Moved function DB_ReadStock to Chips.cs && Set chips stock to MaxValue.
// 22-APR-2014 DLL    Added new functionality for print specific voucher for the Dealer.
// 06-JUN-2014 JAB    Added two decimal characters in denominations fields.
// 25-JUL-2014 DLL    Chips are moved to a separate table
// 28-AUG-2014 LEM & DLL    Fixed Bug WIG-1218: Cage stock mismatch with tips
// 11-DEC-2014 OPC    Fixed Bug WIG-1823: Show only chips denomination allowed.
// 09-JUN-2015 DLL    Fixed Bug WIG-2419: Chips sale with ticket mode and without chips distribution don't work
// 03-NOV-2015 ECP    Backlog Item 5638 - Task 5724 Modify parameter general: Autodistribuite Chips Denomination, for the moment does not apply
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 07-MAR-2016 LTC    Product Backlog Item 10227:TITA: TITO ticket exchange for gambling chips
//------------------------------------------------------------------------------

//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Data.SqlClient;
//using System.Data;
//using System.Drawing;
//using System.Resources;

//namespace WSI.Common
//{

  //public enum ChipsCalculateType
  //{
  //  NONE = 0,
  //  SMALL_CHIPS = 1,
  //  MEDIUM_CHIPS = 2,
  //  HIGH_CHIPS = 3,
  //  CUSTOM_CHIPS = 4
  //}

  //public enum ChipsOperation
  //{
  //  NONE = 0,
  //  SALE = 1,
  //  PURCHASE = 2,
  //  CHANGE_IN = 3,
  //  CHANGE_OUT = 4,
  //  SHOW_STOCK = 5,
  //}

  //public class LinkedGamingTable
  //{
  //  public Int32 TypeId;
  //  public String TypeName;
  //  public Int32 TableId;
  //  public String TableName;
  //  public Int64 CashierSession;
  //  public Int64 GamingTableSession;

  //  public static LinkedGamingTable UnknownSession()
  //  {
  //    LinkedGamingTable _unknown_session;

  //    _unknown_session = new LinkedGamingTable();

  //    _unknown_session.TableId = -1;
  //    _unknown_session.TypeId = -1;
  //    _unknown_session.GamingTableSession = -1;
  //    _unknown_session.CashierSession = -1;

  //    return _unknown_session;
  //  }
  //}

  //public class ChipsAmount
  //{
  //  Decimal[] WEIGHT_CHIPS = { 0.01m, 0.02m, 0.03m, 0.04m, 0.05m, 0.06m, 0.5m, 0.6m }; // PONDERACION (tanto por 1)

  //  const String DEFAULT_WEIGHING_HIGH = "70;15;10;5;0;0;0;0;0;0;0;0;0;";
  //  const String DEFAULT_WEIGHING_MEDIUM = "0;0;0;0;0;25;25;25;24;1;0;0;0";
  //  const String DEFAULT_WEIGHING_SMALL = "0;0;0;0;0;0;0;0;25;25;25;24;1";
  //  const String DEFAULT_WEIGHING_CUSTOM = "-1;-1;-1;-1;-1;40;-1;30;-1;20;8;-1;2";

  //  #region Members

  //  private Dictionary<Decimal, Int32> m_chips_stock;
  //  private Boolean m_with_stock;
  //  private Boolean m_stock_control;

  //  private Dictionary<Decimal, Int32> m_chips_denomination;
  //  private String m_iso_chip;
  //  private ChipsOperation m_operation_type;
  //  private CASHIER_MOVEMENT m_cashier_movement;
  //  private Currency m_initial_amount;
  //  private Currency m_delivered;
  //  private ChipsCalculateType m_chips_calculate;
  //  private LinkedGamingTable m_selected_gaming_table;
  //  private Dictionary<Decimal, Color> m_chip_color;
  //  private Boolean m_print_ticket;
  //  private Boolean m_chips_sale_mode;
  //  private Boolean m_is_chips_sale_register;
  //  private Boolean m_is_apply_taxes;
  //  private Int64 m_account_operation_reason_id;
  //  private String m_account_operation_comment;

  //  #endregion

  //  #region Properties


  //  public String IsoChip
  //  {
  //    get { return m_iso_chip; }
  //    set { m_iso_chip = value; }
  //  }

  //  public ChipsOperation OperationType
  //  {
  //    get { return m_operation_type; }
  //    set { m_operation_type = value; }
  //  }

  //  public ChipsCalculateType ChipsCalculate
  //  {
  //    get { return m_chips_calculate; }
  //    set { m_chips_calculate = value; }
  //  }

  //  public CASHIER_MOVEMENT MovementType
  //  {
  //    get { return m_cashier_movement; }
  //    set { m_cashier_movement = value; }
  //  }

  //  public Currency InitialAmount
  //  {
  //    get { return m_initial_amount; }
  //    set { m_initial_amount = value; }
  //  }

  //  public Dictionary<Decimal, Int32> ChipsDenomination
  //  {
  //    get { return m_chips_denomination; }
  //  }

  //  public Dictionary<Decimal, Int32> ChipsStock
  //  {
  //    get { return m_chips_stock; }
  //  }

  //  public Currency Delivered
  //  {
  //    get { return m_delivered; }
  //    set { m_delivered = value; }
  //  }

  //  public Currency Diference
  //  {
  //    get { return m_initial_amount - m_delivered; }
  //  }

  //  public Boolean WithStock
  //  {
  //    get { return m_with_stock; }
  //  }

  //  public LinkedGamingTable SelectedGamingTable
  //  {
  //    get { return m_selected_gaming_table; }
  //    set { m_selected_gaming_table = value; }
  //  }

  //  public Dictionary<Decimal, Color> ChipColor
  //  {
  //    get { return m_chip_color; }
  //  }

  //  public Boolean PrintTicket
  //  {
  //    get { return m_print_ticket; }
  //    set { m_print_ticket = value; }
  //  }

  //  public Boolean IsChipsSaleRegister
  //  {
  //    get { return m_is_chips_sale_register; }
  //    set { m_is_chips_sale_register = value; }
  //  }


  //  public Boolean IsApplyTaxes
  //  {
  //    get { return m_is_apply_taxes; }
  //    set { m_is_apply_taxes = value; }
  //  }

  //  public Int64 AccountOperationReasonId
  //  {
  //    get { return m_account_operation_reason_id ; }
  //    set { m_account_operation_reason_id = value; }
  //  }

  //  public String  AccountOperationComment
  //  {
  //    get { return m_account_operation_comment; }
  //    set { m_account_operation_comment  = value; }
  //  }

  //  #endregion   // Properties

  //  #region Private Methods

  //  // PURPOSE: Initialize Chip Denomination
  //  //
  //  // PARAMS:
  //  //     - INPUT:
  //  //
  //  //     - OUTPUT:
  //  //
  //  // RETURNS:
  //  //     - True 
  //  //     - False
  //  //
  //  private Boolean DB_ReadDenominations(ChipsOperation Operation, SqlTransaction Trx)
  //  {
  //    StringBuilder _sb;
  //    Decimal _denomination;
  //    Boolean _allowed;
  //    Int32 _color;

  //    try
  //    {
  //      _sb = new StringBuilder();

  //      // TODO: Utilizar enumerado CurrencyExchangeType.CASINOCHIP haciendo join a currency_exchange.

  //      _sb.AppendLine("SELECT   CH_DENOMINATION ");
  //      _sb.AppendLine("       , CH_ALLOWED ");
  //      _sb.AppendLine("       , ISNULL(CH_COLOR, 0) ");
  //      _sb.AppendLine("  FROM   CHIPS ");
  //      _sb.AppendLine(" WHERE   CH_DENOMINATION > 0 ");
  //      _sb.AppendLine("   AND   CH_ALLOWED = 1 ");
  //      _sb.AppendLine("   AND   CH_ISO_CODE = @pIsoChip ");
  //      _sb.AppendLine(" ORDER   BY CH_DENOMINATION DESC ");

  //      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
  //      {
  //        //DLL11111
  //        _cmd.Parameters.Add("@pIsoChip", SqlDbType.NVarChar).Value = Cage.CHIPS_ISO_CODE;

  //        using (SqlDataReader _reader = _cmd.ExecuteReader())
  //        {
  //          while (_reader.Read())
  //          {
  //            _denomination = _reader.GetDecimal(0);
  //            _allowed = _reader.GetBoolean(1);
  //            _color = _reader.GetInt32(2);

  //            if (_color != 0)
  //            {
  //              m_chip_color[_denomination] = System.Drawing.Color.FromArgb(_color);
  //            }

  //            // For Purchase operations, allow all denominations.
  //            if (_allowed || Operation == ChipsOperation.PURCHASE)
  //            {
  //              m_chips_denomination[_denomination] = 0;
  //            }

  //            m_chips_stock[_denomination] = 0;
  //            // DLL 13-JAN-2014: Set stock to MaxValue only when is not gaming table
  //            if ((Operation == ChipsOperation.SALE && m_chips_sale_mode) || !m_stock_control || GamingTablesSessions.GamingTableInfo.IsGamingTable)
  //            {
  //              m_chips_stock[_denomination] = Int32.MaxValue;
  //            }
  //          }
  //        }
  //      }

  //      return true;
  //    }
  //    catch (SqlException _sql_ex)
  //    {
  //      Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
  //    }
  //    catch (Exception _ex)
  //    {
  //      Log.Exception(_ex);
  //    }

  //    return false;
  //  }

  //  private Boolean ReadStock(Int64 SessionId, SqlTransaction Trx)
  //  {
  //    if (m_operation_type != ChipsOperation.SALE || !m_chips_sale_mode)
  //    {
  //      // DLL 13-JAN-2014: Only read stock when is not gaming table
  //      if (m_stock_control && !GamingTablesSessions.GamingTableInfo.IsGamingTable)
  //      {
  //        if (!Chips.GetStock(SessionId, ref m_chips_stock, Trx))
  //        {
  //          return false;
  //        }
  //      }
  //    }

  //    m_with_stock = false;
  //    foreach (KeyValuePair<Decimal, Int32> _stock in m_chips_stock)
  //    {
  //      if (_stock.Value > 0)
  //      {
  //        m_with_stock = true;
  //        break;
  //      }
  //    }

  //    return true;
  //  }

  //  public Boolean UpdateStock(Decimal Denomination, Int32 Amount)
  //  {
  //    if (!m_chips_stock.ContainsKey(Denomination))
  //    {
  //      if (Amount < 0)
  //      {
  //        // Amount < 0 means sale: Can't sale chips that are not in stock.
  //        return false;
  //      }

  //      m_chips_stock[Denomination] = 0;
  //    }

  //    m_chips_stock[Denomination] += Amount;

  //    return true;
  //  }

  //  private void OrderKeysByWeight(ref List<Decimal> Keys, ref List<String> Weighing, ChipsCalculateType Type)
  //  {
  //    // Variables
  //    Int32 _i;
  //    Int32 _j;

  //    Boolean _swap;
  //    Decimal _temp_weight;
  //    Decimal _temp_key;
  //    Decimal _temp_weight_a;
  //    Decimal _temp_weight_b;

  //    // N Pasadas
  //    for (_i = 0; _i < Keys.Count; _i++)
  //    {
  //      for (_j = 0; _j < Keys.Count - 1; _j++)
  //      {
  //        _swap = false;
  //        if (!Decimal.TryParse(Weighing[_j], out _temp_weight_a))
  //        {
  //          _temp_weight_a = 0;
  //        }
  //        if (!Decimal.TryParse(Weighing[_j + 1], out _temp_weight_b))
  //        {
  //          _temp_weight_b = 0;
  //        }

  //        if (_temp_weight_a < _temp_weight_b)
  //        {
  //          _swap = true;
  //        }
  //        else if (_temp_weight_a == _temp_weight_b)
  //        {
  //          switch (Type)
  //          {
  //            case ChipsCalculateType.SMALL_CHIPS:
  //              if (Keys[_j] > Keys[_j + 1])
  //              {
  //                _swap = true;
  //              }

  //              break;
  //            case ChipsCalculateType.NONE:
  //            case ChipsCalculateType.MEDIUM_CHIPS:
  //            case ChipsCalculateType.HIGH_CHIPS:
  //            default:
  //              if (Keys[_j] < Keys[_j + 1])
  //              {
  //                _swap = true;
  //              }

  //              break;
  //          } // switch
  //        } // if

  //        if (_swap)
  //        {
  //          // WEIGHT_CHIPS
  //          if (!Decimal.TryParse(Weighing[_j], out _temp_weight))
  //          {
  //            _temp_weight = 0;
  //          }
  //          Weighing[_j] = Weighing[_j + 1];
  //          Weighing[_j + 1] = _temp_weight.ToString();

  //          // DENOMINATION_CHIPS
  //          _temp_key = Keys[_j];
  //          Keys[_j] = Keys[_j + 1];
  //          Keys[_j + 1] = _temp_key;

  //        } // if
  //      } // for
  //    } // for
  //  } // OrderKeysByWeight

  //  private void AddChipToDistribution(Decimal Denomination, Int32 Units, ref Decimal AmountPending, ref Dictionary<Decimal, Int32> ChipsStock)
  //  {
  //    Decimal _amount_to_add;

  //    _amount_to_add = Denomination * Units;
  //    ChipsStock[Denomination] -= Units;
  //    if (!m_chips_sale_mode)
  //    {
  //      m_chips_denomination[Denomination] += Units;
  //    }
  //    _amount_to_add = Denomination * Units;
  //    AmountPending -= _amount_to_add;
  //    m_delivered += _amount_to_add;

  //  } // AddChipToDistribution

  //  public void AutomaticDistribution(Decimal Amount, ChipsCalculateType Type)
  //  {
  //    Dictionary<Decimal, Int32> _chips_stock_copy;
  //    List<Decimal> _chips_denominations;
  //    List<String> _weighing;
  //    Int32 _idx_weighing;
  //    Int32 _count;
  //    Int32 _chip_units;
  //    Int32 _chip_stock;
  //    String _weight_general_params;
  //    Decimal _chip_weight;
  //    Decimal _amount_pending;
  //    Decimal _amount_pending_old;
  //    Boolean _first_time;

  //    _chips_denominations = new List<Decimal>(m_chips_denomination.Keys);

  //    _chip_weight = 0;
  //    _count = _chips_denominations.Count;
  //    _weight_general_params = "";
  //    _amount_pending = Amount;
  //    _amount_pending_old = Decimal.MinValue;

  //    if (!Chips.IsAutoDistributeEnabled())
  //    {
  //      m_delivered = Amount;

  //      return;
  //    }

  //    //initialize denominations
  //    foreach (Decimal _key in _chips_denominations)
  //    {
  //      m_chips_denomination[_key] = 0;
  //    }
  //    m_delivered = 0;

  //    //DLL 09-JAN-2014: only set Default type when Type is not assigned
  //    if (Type == ChipsCalculateType.NONE)
  //    {
  //      Type = (ChipsCalculateType)GeneralParam.GetInt32("GamingTables", "AutoDistribute.DefaultType", 0);
  //    }

  //    switch (Type)
  //    {
  //      case ChipsCalculateType.HIGH_CHIPS:
  //      case ChipsCalculateType.NONE:
  //        _weight_general_params = GeneralParam.GetString("GamingTables", "Chips.Weighing.High", DEFAULT_WEIGHING_HIGH);
  //        break;

  //      case ChipsCalculateType.MEDIUM_CHIPS:
  //        _weight_general_params = GeneralParam.GetString("GamingTables", "Chips.Weighing.Medium", DEFAULT_WEIGHING_MEDIUM);
  //        break;

  //      case ChipsCalculateType.SMALL_CHIPS:
  //        _weight_general_params = GeneralParam.GetString("GamingTables", "Chips.Weighing.Small", DEFAULT_WEIGHING_SMALL);
  //        break;

  //      case ChipsCalculateType.CUSTOM_CHIPS:
  //        _weight_general_params = GeneralParam.GetString("GamingTables", "Chips.Weighing.Custom", DEFAULT_WEIGHING_CUSTOM);
  //        break;
  //    }

  //    _weighing = new List<String>(_weight_general_params.Split(new Char[] { ';' }, StringSplitOptions.RemoveEmptyEntries));

  //    if (_chips_denominations.Count > _weighing.Count)
  //    {
  //      Int32 _diff_denom_weight = _chips_denominations.Count - _weighing.Count;
  //      for (Int32 _i = 0; _i < _diff_denom_weight; _i++)
  //      {
  //        if (Type == ChipsCalculateType.CUSTOM_CHIPS)
  //        {
  //          _weighing.Add("-1");
  //        }
  //        else
  //        {
  //          _weighing.Add("0");
  //        }
  //      }
  //    }

  //    // Clone stock dictionary
  //    _chips_stock_copy = new Dictionary<Decimal, Int32>();
  //    _idx_weighing = 0;
  //    foreach (KeyValuePair<Decimal, Int32> _stock in m_chips_stock)
  //    {
  //      Int32 _stock_value;
  //      if (_weighing.Count <= _idx_weighing || Int32.Parse(_weighing[_idx_weighing]) < 0)
  //      {
  //        _stock_value = 0;
  //      }
  //      else
  //      {
  //        _stock_value = _stock.Value;
  //      }
  //      _chips_stock_copy.Add(_stock.Key, _stock_value);
  //      _idx_weighing++;
  //    }

  //    // Ordenar Keys segun su ponderación (de mayor a menor)
  //    OrderKeysByWeight(ref _chips_denominations, ref _weighing, Type);

  //    Boolean _finish_distribution = false;

  //    _first_time = true;
  //    while (!_finish_distribution)
  //    {
  //      _idx_weighing = -1;
  //      foreach (Decimal _chip_denomination in _chips_denominations)
  //      {
  //        _idx_weighing++;
  //        _chip_stock = _chips_stock_copy[_chip_denomination];
  //        if (_chip_stock <= 0)
  //        {
  //          continue;
  //        }
  //        Decimal.TryParse(_weighing[_idx_weighing], out _chip_weight);
  //        _chip_units = 0;
  //        // First time allways add 1 unit
  //        if (_first_time)
  //        {
  //          if (_chip_weight > 0 && _chip_denomination <= _amount_pending)
  //          {
  //            _chip_units = 1;
  //          }
  //        }
  //        else
  //        {
  //          _chip_weight = _chip_weight / 100;
  //          _chip_units = (Int32)((_amount_pending * (_chip_weight) / _chip_denomination));
  //        }

  //        if (_chip_units > _chip_stock)
  //        {
  //          _chip_units = _chip_stock;
  //        }

  //        AddChipToDistribution(_chip_denomination, _chip_units, ref _amount_pending, ref _chips_stock_copy);
  //      } // foreach

  //      if (_amount_pending == 0 || _amount_pending_old == _amount_pending)
  //      {
  //        _finish_distribution = true;
  //      }
  //      _amount_pending_old = _amount_pending;

  //      _first_time = false;
  //    }

  //    // Acabar de ajustar
  //    if (_amount_pending > 0)
  //    {
  //      //while (_amount_pending > 0 && _idx < DENOMINATION_CHIPS.Length)
  //      foreach (Decimal _chip_denomination in _chips_denominations)
  //      {
  //        if (_amount_pending == 0)
  //        {
  //          break;
  //        }
  //        _chip_stock = _chips_stock_copy[_chip_denomination];

  //        if (_chip_stock <= 0)
  //        {
  //          continue;
  //        }
  //        _chip_units = (Int32)(_amount_pending / _chip_denomination);
  //        if (_chip_units > _chip_stock)
  //        {
  //          _chip_units = _chip_stock;
  //        }

  //        AddChipToDistribution(_chip_denomination, _chip_units, ref _amount_pending, ref _chips_stock_copy);

  //      } // foreach
  //    } // if

  //    return;
  //  } // AutomaticDistribution

  //  #endregion  //Private Methods

  //  #region Public Methods

  //  public ChipsAmount(Int64 SessionId, CASHIER_MOVEMENT CashierMovement)
  //    : this(SessionId, 0, CashierMovement, ChipsCalculateType.NONE)
  //  {
  //  }

  //  public ChipsAmount(Int64 SessionId, Decimal Total, CASHIER_MOVEMENT CashierMovement, ChipsCalculateType CalculateType)
  //  {
  //    m_initial_amount = Total;
  //    m_delivered = 0;
  //    m_cashier_movement = CashierMovement;
  //    m_chips_calculate = CalculateType;

  //    m_stock_control = GeneralParam.GetBoolean("GamingTables", "ChipsStockControl", false);

  //    m_chips_sale_mode = GeneralParam.GetBoolean("GamingTables", "Cashier.Mode");

  //    switch (CashierMovement)
  //    {
  //      case CASHIER_MOVEMENT.CHIPS_SALE:
  //        m_operation_type = ChipsOperation.SALE;
  //        break;
  //      case CASHIER_MOVEMENT.CHIPS_PURCHASE:
  //        m_operation_type = ChipsOperation.PURCHASE;
  //        break;
  //      case CASHIER_MOVEMENT.CHIPS_CHANGE_IN:
  //        m_operation_type = ChipsOperation.CHANGE_IN;
  //        break;
  //      case CASHIER_MOVEMENT.CHIPS_CHANGE_OUT:
  //        m_operation_type = ChipsOperation.CHANGE_OUT;
  //        break;
  //      default:
  //        m_operation_type = ChipsOperation.SHOW_STOCK;
  //        break;
  //    }

  //    m_iso_chip = Cage.CHIPS_ISO_CODE;
  //    m_chips_denomination = new Dictionary<Decimal, Int32>();
  //    m_chip_color = new Dictionary<Decimal, Color>();
  //    m_chips_stock = new Dictionary<Decimal, Int32>();

  //    using (DB_TRX _db_trx = new DB_TRX())
  //    {
  //      if (!DB_ReadDenominations(m_operation_type, _db_trx.SqlTransaction))
  //      {
  //        throw new Exception(String.Format("ChipsAmount.DB_ReadDenominations error: Movement {0}, SessionId {1}, Amount {2}",
  //                                          CashierMovement, SessionId, Total));
  //      }

  //      // Stock and AutomaticDistribution are only necessary when sale.
  //      if (m_operation_type == ChipsOperation.SALE || m_operation_type == ChipsOperation.CHANGE_OUT || m_operation_type == ChipsOperation.SHOW_STOCK)
  //      {
  //        if (!ReadStock(SessionId, _db_trx.SqlTransaction))
  //        {
  //          throw new Exception(String.Format("ChipsAmount.DB_ReadStock error: Movement {0}, SessionId {1}, Amount {2}",
  //                                            CashierMovement, SessionId, Total));
  //        }
  //      }

  //      if (m_operation_type == ChipsOperation.SALE)
  //      {
  //        if (m_initial_amount > 0)
  //        {
  //          AutomaticDistribution(m_initial_amount, CalculateType);
  //        }
  //      }
  //    }

  //  } // ChipsAmount

  //  #endregion // Public Methods
  //}


  //public class CageAmountsDictionary : Dictionary<String, CageAmountsDictionaryItem>
  //{
  //  public new void Add(String Key, CageAmountsDictionaryItem Value)
  //  {
  //    base.Add(Key.Trim().ToUpper(), Value);
  //  }
  //}

  //public struct CageAmountsDictionaryItem
  //{
  //  public DataTable ItemAmounts;
  //  public Decimal TotalCurrency;
  //  public Decimal TotalTips;
  //  public Boolean AllowedFillOut;
  //  public Boolean HasTicketsTito;
  //}
//}
