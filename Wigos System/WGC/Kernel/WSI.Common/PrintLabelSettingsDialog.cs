﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Printing;
using WSI.Common;

namespace WSI.Labels
{
  internal partial class PrintLabelSettingsDialog : Form
  {
    private PrintLabelSettings m_read_settings;

    public PrintLabelSettingsDialog()
    {
      InitializeComponent();
    }

    private void PrintLabelSettingsDialog_Load(object sender, EventArgs e)
    {
      PrintLabelSettings _settings;

      _settings = PrintLabelSettings.DefaultSettings;
      m_read_settings = _settings;

      SetScreenData(_settings);
    }

    private void SetScreenData(PrintLabelSettings Settings)
    {
      String _printer;
      String _source;
      String _size;

      this.cmb_printer.SelectedIndexChanged -= new System.EventHandler(this.cmb_printer_SelectedIndexChanged);
      this.cmb_paper_size.SelectedIndexChanged -= new System.EventHandler(this.cmb_paper_size_SelectedIndexChanged);
      this.UD_cols.ValueChanged -= new System.EventHandler(this.UD_cols_ValueChanged);
      this.UD_rows.ValueChanged -= new System.EventHandler(this.UD_rows_ValueChanged);

      _printer = Settings.PrinterName;
      _source = Settings.PrinterPaperSource;
      _size = Settings.PaperSize;

      this.cmb_printer.Items.Clear();
      this.cmb_paper_source.Items.Clear();
      this.cmb_paper_size.Items.Clear();

      if (WSI.Common.Misc.IsNullOrWhiteSpace(_printer))
      {
        try
        {
          PrintDocument _doc;
          _doc = new PrintDocument();
          _printer = _doc.PrinterSettings.PrinterName;
        }
        catch
        { }
      }

      if (WSI.Common.Misc.IsNullOrWhiteSpace(_printer))
      {
        _printer = "";
      }

      foreach (String _installed_printer in PrinterSettings.InstalledPrinters)
      {
        this.cmb_printer.Items.Add(_installed_printer);
        if (_installed_printer == _printer)
        {
          this.cmb_printer.SelectedIndex = this.cmb_printer.Items.Count - 1;
        }
      }

      if (!WSI.Common.Misc.IsNullOrWhiteSpace(_printer))
      {
        PrinterSettings _p;
        _p = new PrinterSettings();

        _p.PrinterName = _printer;

        // Add all paper sources
        foreach (PaperSource _paper_source in _p.PaperSources)
        {
          this.cmb_paper_source.Items.Add(_paper_source.SourceName);
          if (_paper_source.SourceName == _source)
          {
            this.cmb_paper_source.SelectedIndex = this.cmb_paper_source.Items.Count - 1;
          }
        }

        if (this.cmb_paper_source.Items.Count > 0)
        {
          if (this.cmb_paper_source.SelectedIndex < 0)
          {
            this.cmb_paper_source.SelectedIndex = 0;
          }
        }

        // Add all paper size
        foreach (PaperSize _paper_size in _p.PaperSizes)
        {
          this.cmb_paper_size.Items.Add(_paper_size.PaperName);
          if (_paper_size.PaperName == _size)
          {
            this.cmb_paper_size.SelectedIndex = this.cmb_paper_size.Items.Count - 1;
          }
        }

        if (this.cmb_paper_size.Items.Count > 0)
        {
          if (this.cmb_paper_size.SelectedIndex < 0)
          {
            this.cmb_paper_size.SelectedIndex = 0;
          }
        }
      }

      UD_rows.Value = Settings.Rows;
      UD_cols.Value = Settings.Colums;
      UD_lbl_width.Value = (decimal)Settings.LabelSize.Width;
      UD_lbl_height.Value = (decimal)Settings.LabelSize.Height;
      UD_padding.Value = (decimal)Settings.Padding;

      UD_ox.Value = (decimal)Settings.PrintOrigin.X;
      UD_oy.Value = (decimal)Settings.PrintOrigin.Y;

      UD_copies.Value = (int)Settings.Copies;

      this.cmb_printer.SelectedIndexChanged += new System.EventHandler(this.cmb_printer_SelectedIndexChanged);
      this.cmb_paper_size.SelectedIndexChanged += new System.EventHandler(this.cmb_paper_size_SelectedIndexChanged);
      this.UD_cols.ValueChanged += new System.EventHandler(this.UD_cols_ValueChanged);
      this.UD_rows.ValueChanged += new System.EventHandler(this.UD_rows_ValueChanged);
    }

    private void btn_accept_Click(object sender, EventArgs e)
    {
      SaveScreenData();
      this.Close();
    }



    private void SaveScreenData()
    {
      PrintLabelSettings _settings;

      _settings = new PrintLabelSettings();
      _settings.SetLayout((int)this.UD_rows.Value, (int)this.UD_cols.Value);
      _settings.LabelSize = new SizeF((float)this.UD_lbl_width.Value, (float)this.UD_lbl_height.Value);
      _settings.Padding = (float)this.UD_padding.Value;
      _settings.PrinterName = (cmb_printer.SelectedIndex < 0) ? "" : (String)cmb_printer.Items[cmb_printer.SelectedIndex];
      _settings.PrinterPaperSource = (cmb_paper_source.SelectedIndex < 0) ? "" : (String)cmb_paper_source.Items[cmb_paper_source.SelectedIndex];
      _settings.PaperSize = (cmb_paper_size.SelectedIndex < 0) ? "" : (String)cmb_paper_size.Items[cmb_paper_size.SelectedIndex];
      _settings.PrintOrigin = new PointF((float)this.UD_ox.Value, (float)this.UD_oy.Value);
      _settings.Copies = (int)this.UD_copies.Value;

      if (_settings.Rows == m_read_settings.Rows
         && _settings.Colums == m_read_settings.Colums)
      {
        // LabelUsed
        for (int i = 0; i < _settings.Rows; i++)
        {
          for (int j = 0; j < _settings.Colums; j++)
          {
            _settings.LabelUsed[i, j] = m_read_settings.LabelUsed[i, j];
          }
        }
      }

      _settings.Save();
    }


    private void cmb_printer_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (cmb_printer.SelectedIndex >= 0)
      {
        PrinterSettings _p;
        String _source;

        _source = cmb_paper_source.Text;

        _p = new PrinterSettings();
        _p.PrinterName = cmb_printer.Text;
        this.cmb_paper_source.Items.Clear();

        foreach (PaperSource _paper_source in _p.PaperSources)
        {
          this.cmb_paper_source.Items.Add(_paper_source.SourceName);
          if (_paper_source.SourceName == _source)
          {
            this.cmb_paper_source.SelectedIndex = this.cmb_paper_source.Items.Count - 1;
          }
        }

        this.cmb_printer.Enabled = this.cmb_printer.Items.Count > 0;
        if (!this.cmb_printer.Enabled)
        {
          this.cmb_printer.Text = "There is not any printer installed.";
        }

        if (this.cmb_paper_source.Items.Count > 0)
        {
          if (this.cmb_paper_source.SelectedIndex < 0)
          {
            this.cmb_paper_source.SelectedIndex = 0;
          }
          this.cmb_paper_source.Enabled = true;
        }
        else
        {
          this.cmb_paper_source.Text = "";
          this.cmb_paper_source.Enabled = false;
        }

      }


    }

    private void btn_cancel_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void btn_default_Click(object sender, EventArgs e)
    {
      PrintLabelSettings _settings;
      _settings = PrintLabelSettings.InitialSettings;
      _settings.Save();
      SetScreenData(_settings);
    }

    private void UpdateLabelSize(object sender, EventArgs e)
    {
      PrintDocument _p;
      _p = new PrintDocument();
      foreach (PaperSize _size in _p.PrinterSettings.PaperSizes)
      {
        if (_size.PaperName == (String) this.cmb_paper_size.SelectedItem)
        {
          try
          {
            UD_lbl_width.Value = (_size.Width / UD_cols.Value) / 100m * 25.4m; // mm
          }
          catch
          { }
          try
          {
            UD_lbl_height.Value = (_size.Height / UD_rows.Value) / 100m * 25.4m; // mm
          }
          catch
          { }

          break;
        }
      
      }
    }



    private void UD_cols_ValueChanged(object sender, EventArgs e)
    {
      this.UpdateLabelSize(sender, e);

    }

    private void UD_rows_ValueChanged(object sender, EventArgs e)
    {
      this.UpdateLabelSize(sender, e);

    }

    private void cmb_paper_size_SelectedIndexChanged(object sender, EventArgs e)
    {
      this.UpdateLabelSize(sender, e);
    }


  }
}
