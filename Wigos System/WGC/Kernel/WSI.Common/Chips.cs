/////////////////////////////////////////////////////////////////////////////////////
//                                   CLASE OBSOLETA
/////////////////////////////////////////////////////////////////////////////////////


//------------------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Chips.cs  
// 
//   DESCRIPTION: Chips class
//
//        AUTHOR: Sergi Mart�nez
// 
// CREATION DATE: 13-DEC-2013
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ---------- ------------------------------------------------------
// 23-DIC-2013 SMN        First release.
// 28-JAN-2014 DLL        Change GetStock function. Change table cashier_session for cashier_movements_grouped_by_session_id.
// 24-MAR-2014 DLL        Fixed Bug WIG-762: game profit can be negative.
// 08-APR-2014 DLL        Fixed Bug WIG-799: When game profit is zero in Close don't create movement GAMING_TABLE_GAME_NETWIN_CLOSE.
// 08-APR-2014 SMN        Fixed Bug WIG-809: AC_LAST_ACTIVITY is not updated.
// 22-APR-2014 DLL        Added new functionality for print specific voucher for the Dealer.
// 06-MAY-2014 RCI        Added GP to show/hide "Mesas de Juego" functionality.
// 06-JUN-2014 JAB        Added two decimal characters in denominations fields.
// 25-JUL-2014 DLL        Chips are moved to a separate table
// 22-AUG-2014 DRV        Fixed Bug WIG-1202 - In integrated mode, account final balance in account movements is negative
// 29-OCT-2014 DHA        Added IsFeaturesGamingTablesEnabled validation
// 30-OCT-2015 RGR        Added Enum ChipType
// 03-NOV-2015 ECP        Backlog Item 5638 - Task 5724 Modify parameter general: Autodistribuite Chips Denomination, for the moment does not apply
// 04-APR-2016 DHA        Product Backlog Item 9756: added chips (RE, NR and Color)
// 13-APR-2016 DHA        Product Backlog Item 9950: added chips operation (sales and purchases)
//------------------------------------------------------------------------------ 

//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Data.SqlClient;
//using System.Data;
//using System.Collections;

//namespace WSI.Common
//{
//  public class Chips
//  {     
   
//    //public static Boolean IsAutoDistributeEnabled()
//    //{
//    //    Boolean _autodistribute_enabled = false;
//    //    //_autodistribute_enabled = GeneralParam.GetBoolean("GamingTables", "AutoDistribute", false);
//    //    return _autodistribute_enabled;
//    //}

//    //------------------------------------------------------------------------------
//    // PURPOSE : Get stock for each denomination
//    //
//    //  PARAMS :
//    //      - INPUT : 
//    //                SessionId
//    //      - OUTPUT : 
//    //                Stock
//    //
//    // RETURNS :
//    //
//    //   NOTES :
//    //
//    //public static Boolean GetStock(long SessionId, ref Dictionary<Decimal, Int32> Stock, SqlTransaction Trx)
//    //{
//    //  return GetStock(SessionId, 0, ref Stock, Trx);
//    //}

//    //public static Boolean GetStock(long SessionId, Int64 OperationId, ref Dictionary<Decimal, Int32> Stock, SqlTransaction Trx)
//    //{
//    //  StringBuilder _sb;
//    //  DataTable _movements;
//    //  Decimal _denomination;
//    //  Int32 _units;
//    //  CASHIER_MOVEMENT _cashier_movement;
//    //  Boolean _chips_sale_mode;

//    //  _chips_sale_mode = GeneralParam.GetBoolean("GamingTables", "Cashier.Mode");

//    //  if (Stock == null)
//    //  {
//    //    Stock = new Dictionary<Decimal, Int32>();
//    //  }
//    //  try
//    //  {
//    //    _sb = new StringBuilder();
       
//    //    _sb.AppendLine("   SELECT   CM.CM_TYPE ");
//    //    _sb.AppendLine("          , CM.CM_CURRENCY_DENOMINATION ");
//    //    _sb.AppendLine("          , CASE ISNULL(CM.ADD_AMOUNT, 0) ");
//    //    _sb.AppendLine("            WHEN 0 THEN (CM.SUB_AMOUNT / CM.CM_CURRENCY_DENOMINATION) ");
//    //    _sb.AppendLine("            ELSE (CM.ADD_AMOUNT / CM.CM_CURRENCY_DENOMINATION) ");
//    //    _sb.AppendLine("            END AS UNITS ");
//    //    _sb.AppendLine("     FROM ( ");
//    //    _sb.AppendLine("            SELECT   SUM(CM_ADD_AMOUNT) AS ADD_AMOUNT ");
//    //    _sb.AppendLine("                   , SUM(CM_SUB_AMOUNT) AS SUB_AMOUNT ");
//    //    _sb.AppendLine("                   , SUM(CM_INITIAL_BALANCE) AS INITIAL_BALANCE ");
//    //    _sb.AppendLine("                   , CM_TYPE ");
//    //    _sb.AppendLine("                   , CM_CURRENCY_DENOMINATION ");

//    //    if (OperationId > 0)
//    //    {
//    //      _sb.AppendLine("              FROM   CASHIER_MOVEMENTS WITH(INDEX(IX_cm_operation_id)) ");
//    //    }
//    //    else
//    //    {
//    //      _sb.AppendLine("              FROM   CASHIER_MOVEMENTS WITH(INDEX(IX_cm_session_id)) ");
//    //    }

//    //    _sb.AppendLine("             INNER   JOIN CHIPS ON  CH_ISO_CODE = CM_CURRENCY_ISO_CODE ");

//    //    if (OperationId > 0)
//    //    {
//    //      _sb.AppendLine("             WHERE   CM_OPERATION_ID      =  @pOperationId ");
//    //      _sb.AppendLine("               AND   CM_SESSION_ID        = @pSessionId ");
//    //    }
//    //    else
//    //    {
//    //      _sb.AppendLine("             WHERE   CM_SESSION_ID        = @pSessionId ");
//    //    }

//    //    _sb.AppendLine("               AND   CM_CURRENCY_ISO_CODE = @pIsoChip ");
//    //    _sb.AppendLine("               AND   CH_DENOMINATION      = CM_CURRENCY_DENOMINATION ");
//    //    _sb.AppendLine("               AND   CH_ALLOWED           = 1 ");
//    //    _sb.AppendLine("               AND   CM_TYPE              IN  ( @pFillIn, @pChipsPurchase, @pChipsChangeIn, @pFillOut, @pChipsSale, @pChipsChangeOut) ");

//    //    _sb.AppendLine("            GROUP BY CM_TYPE, CM_CURRENCY_DENOMINATION ");
//    //    _sb.AppendLine("          ) AS CM ");

//    //    using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
//    //    {
//    //      _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionId;
//    //      _cmd.Parameters.Add("@pIsoChip", SqlDbType.VarChar).Value = Cage.CHIPS_ISO_CODE;
//    //      _cmd.Parameters.Add("@pFillIn", SqlDbType.Int).Value = CASHIER_MOVEMENT.CAGE_FILLER_IN;
//    //      _cmd.Parameters.Add("@pChipsPurchase", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_PURCHASE;
//    //      _cmd.Parameters.Add("@pChipsChangeIn", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_CHANGE_IN;
//    //      _cmd.Parameters.Add("@pFillOut", SqlDbType.Int).Value = CASHIER_MOVEMENT.CAGE_FILLER_OUT;
//    //      _cmd.Parameters.Add("@pChipsSale", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_SALE;
//    //      _cmd.Parameters.Add("@pChipsChangeOut", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_CHANGE_OUT;
//    //      if (OperationId > 0)
//    //      {
//    //        _cmd.Parameters.Add("@pOperationId", SqlDbType.Int).Value = OperationId;
//    //      }

//    //      using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
//    //      {
//    //        _movements = new DataTable();
//    //        _sql_da.Fill(_movements);
//    //      }
//    //    }

//    //    foreach (DataRow _row in _movements.Rows)
//    //    {
//    //      _cashier_movement = (CASHIER_MOVEMENT)_row[0];
//    //      _denomination = (Decimal)_row[1];
//    //      _units = (Int32)(Decimal)_row[2];

//    //      if (!Stock.ContainsKey(_denomination))
//    //      {
//    //        Stock[_denomination] = 0;
//    //      }

//    //      switch (_cashier_movement)
//    //      {
//    //        case CASHIER_MOVEMENT.CAGE_FILLER_IN:
//    //        case CASHIER_MOVEMENT.CHIPS_PURCHASE:
//    //        case CASHIER_MOVEMENT.CHIPS_CHANGE_IN:
//    //          Stock[_denomination] += _units;
//    //          break;

//    //        case CASHIER_MOVEMENT.CAGE_FILLER_OUT:
//    //        case CASHIER_MOVEMENT.CHIPS_CHANGE_OUT:
//    //          Stock[_denomination] -= _units;
//    //          break;

//    //        case CASHIER_MOVEMENT.CHIPS_SALE:
//    //          if (!_chips_sale_mode)
//    //          {
//    //            Stock[_denomination] -= _units;
//    //          }
//    //          break;

//    //        default:
//    //          break;
//    //      }
//    //    }

//    //    return true;
//    //  }
//    //  catch (SqlException _sql_ex)
//    //  {
//    //    Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
//    //  }
//    //  catch (Exception _ex)
//    //  {
//    //    Log.Exception(_ex);
//    //  }

//    //  return false;
//    //}

//    //------------------------------------------------------------------------------
//    // PURPOSE: Check if is integrated chip and credit operation
//    //          Condition: if TITO always false
//    //                     else true if GP = 1 or is virtual account 
//    //                               else false
//    // 
//    //  PARAMS:
//    //      - INPUT:
//    //             IsVirtualCard
//    //
//    //      - OUTPUT:
//    //

//  }
        //}
