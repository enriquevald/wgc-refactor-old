//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AccountFlag.cs
// 
//   DESCRIPTION: Class to manage Account Flag
// 
//        AUTHOR: Daniel Dom�nguez
// 
// CREATION DATE: 28-SEP-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-SEP-2012 DDM    First release.
//
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common
{
  public class AccountFlag
  {
    #region CONSTANTS

    public const int SQL_COLUMN_AF_UNIQUE_ID = 0;
    public const int SQL_COLUMN_AF_ACCOUNT_ID = 1;
    public const int SQL_COLUMN_AF_FLAG_ID = 2;
    public const int SQL_COLUMN_AF_STATUS = 3;
    public const int SQL_COLUMN_AF_VALID_FROM = 4;
    public const int SQL_COLUMN_AF_VALID_TO = 5;
    public const int SQL_COLUMN_AF_ADDED_DATETIME = 6;
    public const int SQL_COLUMN_AF_ADDED_BY_USER_ID = 7;
    public const int SQL_COLUMN_AF_ADDED_BY_PROMOTION_ID = 8;
    public const int SQL_COLUMN_AF_CANCELLED_DATETIME = 9;
    public const int SQL_COLUMN_AF_CANCELLED_USER_ID = 10;
    public const int SQL_COLUMN_AF_ADDED_BY_OPERATION_ID = 11;

    #endregion

    #region ENUMS

    private enum ActionToPerform
    {
      Expire = 1,
      Cancel = 2,
    }

    public enum FlagTypes
    {
      Manual = 0,
      Automatic = 1
    }

    public enum RelatedTypes
    {
      NONE = 0,
      FLYER = 1,
      JACKPOT = 2
    }

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE :  Create schema to the ACCOUNT_FLAGS table 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS : Return datatable with schema
    //
    public static DataTable CreateAccountFlagsTable()
    {
      DataTable _dt;
      DataColumn _col;

      _dt = new DataTable();

      _col = _dt.Columns.Add("AF_UNIQUE_ID", Type.GetType("System.Int64"));
      _col.AutoIncrement = true;
      _col.AutoIncrementSeed = -1;
      _col.AutoIncrementStep = -1;

      _dt.Columns.Add("AF_ACCOUNT_ID", typeof(long));
      _dt.Columns.Add("AF_FLAG_ID", typeof(long));
      _dt.Columns.Add("AF_STATUS", typeof(int));
      _dt.Columns.Add("AF_VALID_FROM", typeof(DateTime));
      _dt.Columns.Add("AF_VALID_TO", typeof(DateTime));
      _dt.Columns.Add("AF_ADDED_DATETIME", typeof(DateTime));
      _dt.Columns.Add("AF_ADDED_BY_USER_ID", typeof(long));
      _dt.Columns.Add("AF_ADDED_BY_PROMOTION_ID", typeof(long));
      _dt.Columns.Add("AF_CANCELLED_DATETIME", typeof(DateTime));
      _dt.Columns.Add("AF_CANCELLED_USER_ID", typeof(long));
      _dt.Columns.Add("AF_ADDED_BY_OPERATION_ID", typeof(long));

      _dt.PrimaryKey = new DataColumn[] { _dt.Columns["AF_UNIQUE_ID"] };

      return _dt;

    } // CreateAccountFlagTable

    //------------------------------------------------------------------------------
    // PURPOSE : Add row values
    //
    //  PARAMS :
    //      - INPUT: 
    //            - AccountId
    //            - FlagId
    //            - Status
    //            - UserId
    //            - AccountsFlags
    //            - InsertFirst
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static Boolean AddFlagToAccount(Int64 AccountId,
                                           Int64 FlagId,
                                           ACCOUNT_FLAG_STATUS Status,
                                           Int64 UserId,
                                           Int64 PromoId,
                                           DataTable AccountsFlags,
                                           Boolean InsertFirst,
                                           Int64 OperationId = -1)
    {
      DataRow _account_flag;

      try
      {
        _account_flag = AccountsFlags.NewRow();

        _account_flag[SQL_COLUMN_AF_ACCOUNT_ID] = AccountId;
        _account_flag[SQL_COLUMN_AF_FLAG_ID] = FlagId;
        _account_flag[SQL_COLUMN_AF_STATUS] = Status;

        if (UserId < 0)
        {
          _account_flag[SQL_COLUMN_AF_ADDED_BY_USER_ID] = DBNull.Value;
        }
        else
        {
          _account_flag[SQL_COLUMN_AF_ADDED_BY_USER_ID] = UserId;
        }

        if (PromoId <= 0)
        {
          _account_flag[SQL_COLUMN_AF_ADDED_BY_PROMOTION_ID] = DBNull.Value;
        }
        else
        {
          _account_flag[SQL_COLUMN_AF_ADDED_BY_PROMOTION_ID] = PromoId;
        }

        if (OperationId < 0)
        {
          _account_flag[SQL_COLUMN_AF_ADDED_BY_OPERATION_ID] = DBNull.Value;
        }
        else
        {
          _account_flag[SQL_COLUMN_AF_ADDED_BY_OPERATION_ID] = OperationId;
        }
        
        if (InsertFirst)
        {
          AccountsFlags.Rows.InsertAt(_account_flag, 0);
        }
        else
        {
          AccountsFlags.Rows.Add(_account_flag);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // AddFlagToAccount
  
    //------------------------------------------------------------------------------
    // PURPOSE : Insert Account_flags
    //
    //  PARAMS :
    //      - INPUT:
    //            AccountFlags
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Inserted ok. False: Otherwise..
    public static Boolean InsertAccountFlags(DataTable AccountFlags)
    {
      Boolean _success;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        _success = InsertAccountFlags(AccountFlags, _db_trx.SqlTransaction);

        if (_success)
        {
          _db_trx.Commit();
        }
      }
      return _success;
    } // InsertAccountFlags


    //------------------------------------------------------------------------------
    // PURPOSE : Insert Account_flags
    //
    //  PARAMS :
    //      - INPUT:
    //            AccountFlags
    //            SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: Inserted ok. False: Otherwise.
    public static Boolean InsertAccountFlags(DataTable AccountFlags, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int32 _nr;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" INSERT INTO   ACCOUNT_FLAGS                                                           ");
        _sb.AppendLine("             ( AF_ACCOUNT_ID                                                           ");
        _sb.AppendLine("             , AF_FLAG_ID                                                              ");
        _sb.AppendLine("             , AF_STATUS                                                               ");
        _sb.AppendLine("             , AF_VALID_FROM                                                           ");
        _sb.AppendLine("             , AF_VALID_TO                                                             ");
        _sb.AppendLine("             , AF_ADDED_DATETIME                                                       ");
        _sb.AppendLine("             , AF_ADDED_BY_USER_ID                                                     ");
        _sb.AppendLine("             , AF_ADDED_BY_PROMOTION_ID                                                ");
        _sb.AppendLine("             , AF_ADDED_BY_OPERATION_ID )                                              ");
        _sb.AppendLine("      SELECT                                                                           ");
        _sb.AppendLine("              @pAccountID                                                              ");
        _sb.AppendLine("             , @pFlagId                                                                ");
        _sb.AppendLine("             , @pStatus                                                                ");
        _sb.AppendLine("             , GETDATE()                                                               ");
        _sb.AppendLine("             , ( CASE FL_EXPIRATION_TYPE                                               ");
        _sb.AppendLine("                      WHEN 1  THEN NULL                                                ");
        _sb.AppendLine("                      WHEN 2  THEN FL_EXPIRATION_DATE                                  ");
        _sb.AppendLine("                      WHEN 11 THEN DATEADD(MINUTE, FL_EXPIRATION_INTERVAL,  GETDATE()) ");
        _sb.AppendLine("                      WHEN 12 THEN DATEADD(HOUR,   FL_EXPIRATION_INTERVAL,  GETDATE()) ");
        _sb.AppendLine("                      WHEN 13 THEN DATEADD(DAY,    FL_EXPIRATION_INTERVAL,  GETDATE()) ");
        _sb.AppendLine("                      WHEN 14 THEN DATEADD(MONTH,  FL_EXPIRATION_INTERVAL,  GETDATE()) ");
        _sb.AppendLine("                      ELSE '---'                                                       ");
        _sb.AppendLine("                      END )                                                            ");
        _sb.AppendLine("             , GETDATE()                                                               ");
        _sb.AppendLine("             , @pCurrentuserId                                                         ");
        _sb.AppendLine("             , @pPromotionId                                                           ");
        _sb.AppendLine("             , @pOperationId                                                           ");
        _sb.AppendLine("        FROM   FLAGS                                                                   ");
        _sb.AppendLine("       WHERE   FL_FLAG_ID =   @pFlagId                                                 ");
        _sb.AppendLine(" SET @pUniqueId = SCOPE_IDENTITY()                                                     ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          SqlParameter _param;

          _sql_cmd.Parameters.Add("@pAccountID", SqlDbType.BigInt).SourceColumn = "AF_ACCOUNT_ID";
          _sql_cmd.Parameters.Add("@pFlagId", SqlDbType.BigInt).SourceColumn = "AF_FLAG_ID";
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).SourceColumn = "AF_STATUS";
          _sql_cmd.Parameters.Add("@pCurrentuserId", SqlDbType.BigInt).SourceColumn = "AF_ADDED_BY_USER_ID";
          _sql_cmd.Parameters.Add("@pPromotionId", SqlDbType.BigInt).SourceColumn = "AF_ADDED_BY_PROMOTION_ID";
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).SourceColumn = "AF_ADDED_BY_OPERATION_ID";

          _param = _sql_cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt);
          _param.SourceColumn = "AF_UNIQUE_ID";
          _param.Direction = ParameterDirection.Output;

          _sql_cmd.UpdatedRowSource = UpdateRowSource.OutputParameters;
          using (SqlDataAdapter _sql_da = new SqlDataAdapter())
          {
            _sql_da.InsertCommand = _sql_cmd;
            _nr = _sql_da.Update(AccountFlags);

            if (_nr == AccountFlags.Rows.Count)
            {
              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertAccountFlags

    //------------------------------------------------------------------------------
    // PURPOSE : Accounts flags expiration management
    //           
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      -  True if all indicated flags have been expired, false otherwise.
    public static Boolean AccountsFlagsExpiration()
    {
      StringBuilder _sb;
      DataTable _account_flags;
      DataTable _dummy_account_flags;
      try
      {

        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   AF_UNIQUE_ID ");
        _sb.AppendLine("        , AF_ACCOUNT_ID  ");
        _sb.AppendLine("        , AF_STATUS  ");
        _sb.AppendLine("   FROM   ACCOUNT_FLAGS ");
        _sb.AppendLine("  WHERE   AF_VALID_TO < GETDATE() ");
        _sb.AppendLine("    AND   AF_STATUS =  @pStatusActive");


        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_FLAG_STATUS.ACTIVE;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _account_flags = new DataTable();
              _db_trx.Fill(_sql_da, _account_flags);
            }
          }
        }

        return (ChangeStatusFlags(_account_flags, ACCOUNT_FLAG_STATUS.EXPIRED, 0, out _dummy_account_flags) == _account_flags.Rows.Count);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // AccountsFlagsExpiration   


    //------------------------------------------------------------------------------
    // PURPOSE : Change the status from flags
    //
    //  PARAMS :
    //      - INPUT :    
    //          - AccountFlags must contain at least account_id, unique_id and status.
    //          - NewStatus
    //          - CancelledUserId
    //
    //      - OUTPUT :
    //          - AccountFlagsUpdated
    //          
    // RETURNS :
    //      - Datatable with account_flags updated
    //  
    public static Int32 ChangeStatusFlags(DataTable AccountFlags,
                                          ACCOUNT_FLAG_STATUS NewStatus,
                                          Int64 CancelledUserId,
                                          out DataTable AccountFlagsUpdated)
    {
      StringBuilder _sb;
      StringBuilder _sb_error;
      Int32 _num_changed;

      _num_changed = 0;
      AccountFlagsUpdated = CreateAccountFlagsTable();

      try
      {
        if (AccountFlags.Rows.Count == 0)
        {
          return _num_changed;
        }
        _sb_error = new StringBuilder();
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   ACCOUNT_FLAGS");
        _sb.AppendLine("    SET   AF_STATUS    = @pNewStatus");

        if (NewStatus == ACCOUNT_FLAG_STATUS.CANCELLED)
        {
          _sb.AppendLine("        , AF_CANCELLED_DATETIME = GETDATE()");
          _sb.AppendLine("        , AF_CANCELLED_USER_ID = @pCancelledUserId");
        }

        _sb.AppendLine("   FROM   ACCOUNT_FLAGS WITH(INDEX(IX_af_flag_status)) ");
        _sb.AppendLine("  WHERE   AF_UNIQUE_ID = @pUniqueId ");
        _sb.AppendLine("    AND   AF_STATUS    = @pOldStatus");

        foreach (DataRow _flag in AccountFlags.Rows)
        {
          try
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
              {
                _cmd.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = NewStatus;
                _cmd.Parameters.Add("@pOldStatus", SqlDbType.Int).Value = _flag["AF_STATUS"];
                _cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt).Value = _flag["AF_UNIQUE_ID"];
                if (NewStatus == ACCOUNT_FLAG_STATUS.CANCELLED)
                {
                  _cmd.Parameters.Add("@pCancelledUserId", SqlDbType.BigInt).Value = CancelledUserId;
                }

                if (_cmd.ExecuteNonQuery() != 1)
                {
                  _sb_error.Length = 0;
                  _sb_error.AppendFormat("Flag not changed status - Detected on ChangeStatusFlag ");
                  _sb_error.AppendLine();
                  _sb_error.AppendFormat(" - FlagUniqueId: {0} ", _flag["AF_UNIQUE_ID"]);
                  _sb_error.AppendLine();
                  _sb_error.AppendFormat(" - AccountId: {0}, ", _flag["AF_ACCOUNT_ID"]);
                  _sb_error.AppendLine();
                  _sb_error.AppendFormat(" - Status: {0} ", _flag["AF_STATUS"]);
                  _sb_error.AppendLine();

                  Log.Warning(_sb_error.ToString());

                  continue;
                }

              }// SqlCommand

              _db_trx.Commit();

              AccountFlagsUpdated.Rows.Add(_flag.ItemArray);
              _num_changed++;
            }// DB_TRX
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }
        }// foreach
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return _num_changed;
    } // ChangeStatusFlags   
   
  }


}

