//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Peru_Frame.cs
// 
//   DESCRIPTION: Classes to manage Peru Frame
// 
//        AUTHOR: Jos� Mart�nez 
// 
// CREATION DATE: 29-JUN-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-JUN-2012 JML    First release.
// 25-JUL-2013 AJQ    Meter Rollover should be reported as a new event (before it was reported as a Meter Reset)
// 05-SEP-2013 RCI & AJQ    Fixed Bug WIG-191: E-Frames not send when new terminal
// 18-SEP-2013 XCD    Added SetTerminalStatusReport and RepeatTerminalStatusReport
// 19-SEP-2013 XCD    Added TechnicalFrame with Int64 for Event, CreateTechnicalFrame
// 07-OCT-2013 RRR    Added WXP SAS Meters mailing: Provider and TerminalName is needed.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common
{
  public enum PERU_FRAME_TYPE
  {
    TYPE_ECONOMICAL = 1,
    TYPE_TECHNICAL = 2,
  }

  public enum PERU_FRAME_STATUS
  {
    STATUS_PENDING = 0,
    STATUS_SENT = 1,
    STATUS_ERROR = 2
  }

  //------------------------------------------------------------------------------
  // PURPOSE: Class to manage peru frame row
  // 
  // NOTES:
  //
  //------------------------------------------------------------------------------
  public class PeruFrameRow
  {
    DataRow m_row;


    public PeruFrameRow(DataRow Row)
    {
      m_row = Row;
    }

    public PERU_FRAME_STATUS Status
    {
      get
      {
        return (PERU_FRAME_STATUS)m_row["WXM_STATUS"];
      }
      set
      {
        m_row["WXM_STATUS"] = (PERU_FRAME_STATUS)value;
      }
    }
    public Byte[] Data
    {
      get
      {
        return (Byte[])m_row["WXM_DATA"];
      }
      set
      {
        m_row["WXM_DATA"] = value;
      }
    }
  }

  //------------------------------------------------------------------------------
  // PURPOSE: Class to manage peru frames table
  // 
  // NOTES:
  //
  //------------------------------------------------------------------------------
  public class PeruFramesTable : DataTable
  {

    #region CONSTANTS

    private const int COLUMN_ID = 0;
    private const int COLUMN_DATETIME = 1;
    private const int COLUMN_TYPE = 2;
    private const int COLUMN_STATUS = 3;
    private const int COLUMN_TERMINAL_ID = 4;
    private const int COLUMN_DATA = 5;

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE :  Create schema to the Peru_frame table 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS : Return datatable with schema
    //
    public static PeruFramesTable CreatePeruFrameTable()
    {
      PeruFramesTable _dt;
      DataColumn _col;
      Byte[] _get_type = new Byte[128];

      _dt = new PeruFramesTable();

      _dt.Columns.Add("WXM_ID", Type.GetType("System.Int64")).AllowDBNull = true;
      _dt.Columns.Add("WXM_DATETIME", Type.GetType("System.DateTime")).AllowDBNull = true;
      _dt.Columns.Add("WXM_TYPE", Type.GetType("System.Int32")).AllowDBNull = true;
      _col = _dt.Columns.Add("WXM_STATUS", Type.GetType("System.Int32"));
      _col.AllowDBNull = false;
      _col.DefaultValue = (Int32)PERU_FRAME_STATUS.STATUS_PENDING;
      _dt.Columns.Add("WXM_TERMINAL_ID", Type.GetType("System.Int32")).AllowDBNull = true;
      _col = _dt.Columns.Add("WXM_DATA", _get_type.GetType());
      _col.AllowDBNull = true;

      return _dt;

    } // CreatePeruFrameTable


    //------------------------------------------------------------------------------
    // PURPOSE :  Create schema to the Peru_frame table 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS : Return datatable with schema
    //
    public Boolean ReadPendingToSend(PERU_FRAME_TYPE FrameType, SqlTransaction Trx)
    {

      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   TOP 2000              ");
        _sb.AppendLine("         WXM_ID                ");
        _sb.AppendLine("       , WXM_STATUS            ");
        _sb.AppendLine("       , WXM_DATETIME          ");
        _sb.AppendLine("       , WXM_TYPE              ");
        _sb.AppendLine("       , WXM_TERMINAL_ID       ");
        _sb.AppendLine("       , WXM_DATA              ");
        _sb.AppendLine("       , WXM_STATUS_CHANGED    ");
        _sb.AppendLine("  FROM   WXP_001_MESSAGES WITH (INDEX (IX_WXM_TYPE_STATUS)) ");
        _sb.AppendLine(" WHERE   WXM_TYPE   = @pType   ");
        _sb.AppendLine("   AND   WXM_STATUS = @pStatus ");
        _sb.AppendLine("ORDER BY WXM_ID ASC            ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = PERU_FRAME_STATUS.STATUS_PENDING;
          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = FrameType;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(this);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // ReadPendingSent


    //------------------------------------------------------------------------------
    // PURPOSE : Add row values
    //
    //  PARAMS :
    //      - INPUT: 
    //            - WXM_DATETIME
    //            - WXM_TYPE
    //            - WXM_TERMINAL_ID
    //            - WXM_DATA
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : True if add row, false otherwise
    public Boolean AddNewFrame(Int32 TerminalId, PERU_FRAME_TYPE Type, Byte[] FrameData, DateTime When)
    {
      DataRow _row;

      try
      {
        _row = this.NewRow();

        _row[COLUMN_TERMINAL_ID] = TerminalId;
        _row[COLUMN_TYPE] = Type;
        _row[COLUMN_DATA] = FrameData;

        if (When != DateTime.MinValue)
        {
          _row[COLUMN_DATETIME] = When;
        }

        this.Rows.Add(_row);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // AddNewFrame


    //------------------------------------------------------------------------------
    // PURPOSE : Add row values
    //
    //  PARAMS :
    //      - INPUT: 
    //            - WXM_TYPE
    //            - WXM_TERMINAL_ID
    //            - WXM_DATA
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : True if add row, false otherwise
    public Boolean AddNewFrame(Int32 TerminalId, PERU_FRAME_TYPE Type, Byte[] FrameData)
    {
      return AddNewFrame(TerminalId, Type, FrameData, DateTime.MinValue);

    } // AddNewFrame


    //------------------------------------------------------------------------------
    // PURPOSE : Change Status
    //
    //  PARAMS :
    //      - INPUT: 
    //            - WXM_ID
    //            - WXM_STATUS (new)
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : True if add row, false otherwise
    public Boolean ChangeStatus(Int64 FrameId, PERU_FRAME_STATUS NewStatus)
    {
      DataRow _row;

      try
      {
        _row = this.NewRow();

        _row["WXM_ID"] = FrameId;
        _row["WXM_STATUS"] = NewStatus;


        this.Rows.Add(_row);
        _row.AcceptChanges();
        _row.SetModified();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // AddFrame

    //------------------------------------------------------------------------------
    // PURPOSE : Save Data Table
    //
    //  PARAMS :
    //      - INPUT: 
    //            - SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : True if inserts & updates ok, false otherwise
    public Boolean Save(SqlTransaction SqlTrx)
    {
      StringBuilder _sql_update;
      StringBuilder _sql_insert;
      SqlCommand _sql_cmd_upd;
      SqlCommand _sql_cmd_ins;
      Int32 _nr;
      Int32 _num_rows_inserted;
      Int32 _num_rows_updated;

      try
      {
        _sql_insert = new StringBuilder();
        _sql_insert.AppendLine("INSERT INTO   WXP_001_MESSAGES                     ");
        _sql_insert.AppendLine("            ( WXM_DATETIME                     ");
        _sql_insert.AppendLine("            , WXM_TYPE                         ");
        _sql_insert.AppendLine("            , WXM_TERMINAL_ID                  ");
        _sql_insert.AppendLine("            , WXM_DATA                         ");
        _sql_insert.AppendLine("            , WXM_STATUS                       ");
        _sql_insert.AppendLine("            , WXM_STATUS_CHANGED             ) ");
        _sql_insert.AppendLine(" VALUES     ( ISNULL(@pDatetime, GETDATE())   ");
        _sql_insert.AppendLine("            , @pType                          ");
        _sql_insert.AppendLine("            , @pTerminalId                    ");
        _sql_insert.AppendLine("            , @pData                          ");
        _sql_insert.AppendLine("            , @pStatus                        ");
        _sql_insert.AppendLine("            , GETDATE ()                    ) ");

        _sql_cmd_ins = new SqlCommand(_sql_insert.ToString(), SqlTrx.Connection, SqlTrx);
        _sql_cmd_ins.Parameters.Add("@pDatetime", SqlDbType.DateTime).SourceColumn = "WXM_DATETIME";
        _sql_cmd_ins.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "WXM_TYPE";
        _sql_cmd_ins.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "WXM_TERMINAL_ID";
        _sql_cmd_ins.Parameters.Add("@pData", SqlDbType.VarBinary, 128).SourceColumn = "WXM_DATA";
        _sql_cmd_ins.Parameters.Add("@pStatus", SqlDbType.Int).SourceColumn = "WXM_STATUS";


        _sql_update = new StringBuilder();
        _sql_update.AppendLine(" UPDATE   WXP_001_MESSAGES ");
        _sql_update.AppendLine("    SET   WXM_STATUS         = @pNewstatus ");
        _sql_update.AppendLine("        , WXM_STATUS_CHANGED = CASE WHEN WXM_STATUS = @pNewStatus THEN WXM_STATUS_CHANGED ELSE GETDATE() END ");
        _sql_update.AppendLine("  WHERE   WXM_ID = @pUniqueId ");

        _sql_cmd_upd = new SqlCommand(_sql_update.ToString(), SqlTrx.Connection, SqlTrx);
        _sql_cmd_upd.Parameters.Add("@pUniqueId", SqlDbType.BigInt).SourceColumn = "WXM_ID";
        _sql_cmd_upd.Parameters.Add("@pNewstatus", SqlDbType.Int).SourceColumn = "WXM_STATUS";

        using (SqlDataAdapter _sql_da = new SqlDataAdapter())
        {
          _sql_da.UpdateCommand = _sql_cmd_upd;
          _sql_da.InsertCommand = _sql_cmd_ins;

          DataRow[] _eframes;

          _eframes = this.Select("WXM_TYPE = 1", "", DataViewRowState.Added);

          _num_rows_inserted = this.Select("", "", DataViewRowState.Added).Length;
          _num_rows_updated = this.Select("", "", DataViewRowState.ModifiedCurrent).Length;

          _nr = _sql_da.Update(this);

          if (_nr == _num_rows_inserted + _num_rows_updated)
          {
            try
            {
              if (_eframes.Length > 0)
              {
                foreach (DataRow _row in _eframes)
                {
                  _row.SetModified();
                }

                _sql_update = new StringBuilder();
                _sql_update.AppendLine(" UPDATE   TERMINALS ");
                _sql_update.AppendLine("    SET   TE_WXP_REPORTED = 1 ");
                _sql_update.AppendLine("  WHERE   TE_TERMINAL_ID  = @pTerminalId");

                _sql_cmd_upd = new SqlCommand(_sql_update.ToString(), SqlTrx.Connection, SqlTrx);
                _sql_cmd_upd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "WXM_TERMINAL_ID";

                _sql_da.UpdateCommand = _sql_cmd_upd;
                _sql_da.InsertCommand = null;

                _nr = _sql_da.Update(this);
              }
            }
            catch (Exception _ex)
            {
              // Log exception but continue ...
              Log.Exception(_ex);
            }

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Save
  }

  //------------------------------------------------------------------------------
  // PURPOSE: Class to manage peru events
  // 
  // NOTES:
  //
  //------------------------------------------------------------------------------
  static public class PeruEvents
  {
    public enum ENUM_TECHNICAL_EVENTS
    {
      PERU_TECHNICAL_EVENT_NONE = 0,
      PERU_TECHNICAL_EVENT_SAS_MACHINE_DISCONNECTED = 1,
      PERU_TECHNICAL_EVENT_SAS_HOST_DISCONNECTED = 2,
      PERU_TECHNICAL_EVENT_SAS_MACHINE_RESTORING_ON_POWER_UP = 3,
      PERU_TECHNICAL_EVENT_SAS_MACHINE_LOW_BATTERY = 4,
      PERU_TECHNICAL_EVENT_SAS_MACHINE_CASHBOX_DOOR_OPENED = 5,
      PERU_TECHNICAL_EVENT_SAS_HOST_MEMORY_FAIL = 6,
      PERU_TECHNICAL_EVENT_SAS_MACHINE_MEMORY_FAIL = 7,
      PERU_TECHNICAL_EVENT_SAS_MACHINE_RESET_MEMORY = 8,
      PERU_TECHNICAL_EVENT_SAS_MACHINE_METER_ROLLOVER = 9,

      PERU_TECHNICAL_EVENT_MACHINE_METER_FIRST_TIME_ON_REGISTER = -1, // Internal event -- Not reported to Mincetur

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Insert Peru Technical event.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TerminalId
    //          - TechnicalEvent
    //          - EventDateTime
    //          - SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True if ok, false otherwise
    //
    public static Boolean InsertTechnicalEvent(Int32 TerminalId, ENUM_TECHNICAL_EVENTS TechnicalEvent, DateTime EventDateTime)
    {
      return InsertTechnicalEvent(TerminalId, true, TechnicalEvent, EventDateTime);
    }
    public static Boolean InsertTechnicalEvent(Int32 TerminalId, Boolean Activated, ENUM_TECHNICAL_EVENTS TechnicalEvent, DateTime EventDateTime)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (InsertTechnicalEvent(TerminalId, Activated, TechnicalEvent, EventDateTime, _db_trx.SqlTransaction))
        {
          _db_trx.Commit();

          return true;
        }
      }

      return false;
    } // InsertTechnicalEvent

    public static Boolean InsertTechnicalEvent(Int32 TerminalId, ENUM_TECHNICAL_EVENTS TechnicalEvent, DateTime EventDateTime, SqlTransaction SqlTrx)
    {
      return InsertTechnicalEvent(TerminalId, true, TechnicalEvent, EventDateTime, SqlTrx);
    }
    public static Boolean InsertTechnicalEvent(Int32 TerminalId, Boolean Activated, ENUM_TECHNICAL_EVENTS TechnicalEvent, DateTime EventDateTime, SqlTransaction SqlTrx)
    {
      Int64 _set_bit;
      Int64 _reset_bit;

      if (Activated)
      {
        _set_bit = (1 << (int)TechnicalEvent);
        _reset_bit = 0;
      }
      else
      {
        _set_bit = 0;
        _reset_bit = (1 << (int)TechnicalEvent);
      }

      return InsertTechnicalEvent(TerminalId, _set_bit, _reset_bit, EventDateTime, SqlTrx);
    }

    public static Boolean InsertTechnicalEvent(Int32 TerminalId, Int64 SetFlags, Int64 ResetFlags, DateTime EventDateTime, SqlTransaction SqlTrx)
    {
      int _external_protocol_enabled;
      PeruFramesTable _peru_frames;
      String _site_register;
      String _machine_register;
      String _ebox_mac_address;
      byte[] _frame;
      Boolean _has_eframes;
      Boolean _report_flags;
      Int64 _new_flags;

      _external_protocol_enabled = 0;
      if (!int.TryParse(Misc.ReadGeneralParams("ExternalProtocol", "Enabled"), out _external_protocol_enabled))
      {
        _external_protocol_enabled = 0;
      }

      if (_external_protocol_enabled == 0)
      {
        return true;
      }

      // AJQ 22-AUG-2013, Avoid reporting "duplicated events". We consider same event if it happened on the last minute
      if (EventDateTime == DateTime.MinValue)
      {
        EventDateTime = WGDB.Now;
      }
      // Ensure Millisecond is zero
      EventDateTime = EventDateTime.AddMilliseconds(-EventDateTime.Millisecond);

      if (!PeruTerminalInfo.Get(TerminalId, out _site_register, out _machine_register, out _ebox_mac_address, out _has_eframes, SqlTrx))
      {
        Log.Error("PeruTerminalInfo.Get: return false. ");

        return false;
      }

      if (String.IsNullOrEmpty(_machine_register)
          || String.IsNullOrEmpty(_ebox_mac_address))
      {
        return true;
      }

      if (!GetTechnicalEventFlagsToBeReported(TerminalId, SetFlags, ResetFlags, out _report_flags, out EventDateTime, out _new_flags, SqlTrx))
      {
        Log.Error("GetTechnicalEventFlagsToBeReported failed, TerminalId " + TerminalId + " Set/Reset: " + SetFlags.ToString() + "/" + ResetFlags.ToString() );

        return false;
      }
      if (!_report_flags)
      {
        return true;
      }

      _peru_frames = PeruFramesTable.CreatePeruFrameTable();

      //
      // Add & Insert technical event
      //
      _frame = PeruFrameCreator.TechnicalFrame(_site_register, _machine_register, _ebox_mac_address, EventDateTime, _new_flags);
      _peru_frames.AddNewFrame(TerminalId, PERU_FRAME_TYPE.TYPE_TECHNICAL, _frame);

      if (!_peru_frames.Save(SqlTrx))
      {
        Log.Error("_peru_frames.Save: return false. ");

        return false;
      }

      return true;

    } // InsertTechnicalEvent

    //------------------------------------------------------------------------------
    // PURPOSE: Insert Peru Technical event.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TerminalId
    //          - NewMachineMeters
    //          - OldMachineMeters
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True if ok, false otherwise
    //
    public static Boolean InsertMachineMetersEvent(Int32 TerminalId, MachineMeters OldMachineMeters, MachineMeters NewMachineMeters)
    {
      int _external_protocol_enabled;
      PeruFramesTable _peru_frames;
      String _site_register;
      String _machine_register;
      String _ebox_mac_address;
      byte[] _frame;
      Boolean _new_is_zero;
      Boolean _old_is_zero;
      ENUM_TECHNICAL_EVENTS _event;
      DateTime _event_time;
      Boolean _previously_reported;
      Int64 _new_flags;

      if (!NewMachineMeters.PlayWonAvalaible)
      {
        return true;
      }

      _external_protocol_enabled = 0;
      if (!int.TryParse(Misc.ReadGeneralParams("ExternalProtocol", "Enabled"), out _external_protocol_enabled))
      {
        _external_protocol_enabled = 0;
      }

      if (_external_protocol_enabled == 0)
      {
        return true;
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!PeruTerminalInfo.Get(TerminalId, out _site_register, out _machine_register, out _ebox_mac_address, out _previously_reported, _db_trx.SqlTransaction))
          {
            Log.Error("PeruTerminalInfo.Get: return false. ");

            return false;
          }

          if (String.IsNullOrEmpty(_machine_register)
              || String.IsNullOrEmpty(_ebox_mac_address))
          {
            return true;
          }

          _event = ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_NONE;

          if (!_previously_reported)
          {
            _event = ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_MACHINE_METER_FIRST_TIME_ON_REGISTER; // Use this event as a mark for "FIRST TIME" connection.
          }
          else
          {
            _new_is_zero = (NewMachineMeters.PlayedCents == 0
                            && NewMachineMeters.WonCents == 0
                            && NewMachineMeters.JackpotCents == 0);

            _old_is_zero = (OldMachineMeters.PlayedCents == 0
                            && OldMachineMeters.WonCents == 0
                            && OldMachineMeters.JackpotCents == 0);

            if (_new_is_zero && !_old_is_zero)
            {
              _event = ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_RESET_MEMORY;
            }
            else
            {
              if (NewMachineMeters.PlayedCents < OldMachineMeters.PlayedCents
                  || NewMachineMeters.WonCents < OldMachineMeters.WonCents
                  || NewMachineMeters.JackpotCents < OldMachineMeters.JackpotCents)
              {
                _event = ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_METER_ROLLOVER;
              }
            }
          }

          if (_event == ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_NONE)
          {
            return true;
          }

          {
            Int64 _set_bit;
            Boolean _to_report; // dummy

            switch (_event)
            { 
              case ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_MACHINE_METER_FIRST_TIME_ON_REGISTER:
                _set_bit = 0;
                break;

              case ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_METER_ROLLOVER:
                _set_bit = 1L << (int)ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_METER_ROLLOVER;
                break;

              case ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_RESET_MEMORY:
                _set_bit = 1L << (int)ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_RESET_MEMORY;
                break;

              default:
                _set_bit = 0;
                Log.Error("Unexpected Event: " + _event.ToString());
                return false;
            }

            if (!GetTechnicalEventFlagsToBeReported(TerminalId, _set_bit, 0, out _to_report, out _event_time, out _new_flags, _db_trx.SqlTransaction))
            {
              return false;
            }
          }



          _peru_frames = PeruFramesTable.CreatePeruFrameTable();

          // AJQ, 22-AUG-2013, Use "EventTime" -1s, +0s, +1s to let MINCETUR order the frames by the datetime field of frame (Yuri Guerra dixit)
          _event_time = _event_time.AddSeconds(-1);
          if (_event != ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_MACHINE_METER_FIRST_TIME_ON_REGISTER)
          {
            //
            // Add & Insert ECONOMIC event with old machine meters (before Reset or RollOver) 
            //
            _frame = PeruFrameCreator.EconomicalFrame(_site_register, _machine_register, _event_time, ((Decimal)OldMachineMeters.PlayedCents) / 100,
                                                                                                      ((Decimal)OldMachineMeters.WonCents) / 100,
                                                                                                      ((Decimal)OldMachineMeters.JackpotCents) / 100);
            _peru_frames.AddNewFrame(TerminalId, PERU_FRAME_TYPE.TYPE_ECONOMICAL, _frame);

            //
            // Add & Insert TECHNICAL event
            //
            _event_time = _event_time.AddSeconds(+1);
            _frame = PeruFrameCreator.TechnicalFrame(_site_register, _machine_register, _ebox_mac_address, _event_time, _new_flags);
            _peru_frames.AddNewFrame(TerminalId, PERU_FRAME_TYPE.TYPE_TECHNICAL, _frame);
            if (_event == ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_RESET_MEMORY)
            {
              _frame = PeruFrameCreator.EconomicalFrame(_site_register, _machine_register, _event_time, 0, 0, 0);
              _peru_frames.AddNewFrame(TerminalId, PERU_FRAME_TYPE.TYPE_ECONOMICAL, _frame);
            }
          }

          //
          // Add & Insert ECONOMIC event with new machine meters (Reset: Zero, RollOver: New Meters)
          //
          _event_time = _event_time.AddSeconds(+1);
          _frame = PeruFrameCreator.EconomicalFrame(_site_register, _machine_register, _event_time, ((Decimal)NewMachineMeters.PlayedCents) / 100,
                                                                                                    ((Decimal)NewMachineMeters.WonCents) / 100,
                                                                                                    ((Decimal)NewMachineMeters.JackpotCents) / 100);
          _peru_frames.AddNewFrame(TerminalId, PERU_FRAME_TYPE.TYPE_ECONOMICAL, _frame);

          if (!_peru_frames.Save(_db_trx.SqlTransaction))
          {
            Log.Error("_peru_frames.Save: return false. ");

            return false;
          }

          _db_trx.Commit();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertMachineMetersEvent

    //------------------------------------------------------------------------------
    // PURPOSE: Update terminal report status
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TerminalId
    //          - SetBitMask
    //          - SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True if ok, false otherwise
    //
    public static Boolean GetTechnicalEventFlagsToBeReported(Int32 TerminalId,
                                                              Int64 SetBitMask,
                                                              Int64 ResetBitMask,
                                                              out Boolean ToBeReported,
                                                              out DateTime TechEventDateTime,
                                                              out Int64 TechEventFlags,
                                                              SqlTransaction SqlTrx)
    {
      const Int64 AUTORESET_BIT_MASK = (1 << (Int32)ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_RESTORING_ON_POWER_UP)
                                     | (1 << (Int32)ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_LOW_BATTERY)
                                     | (1 << (Int32)ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_HOST_MEMORY_FAIL)
                                     | (1 << (Int32)ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_MEMORY_FAIL)
                                     | (1 << (Int32)ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_RESET_MEMORY)
                                     | (1 << (Int32)ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_METER_ROLLOVER);

      const Int64 ALWAYS_REPORT_BIT_MASK = (1 << (Int32)ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_RESET_MEMORY)
                                         | (1 << (Int32)ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_METER_ROLLOVER);

      StringBuilder _sb;
      Int64 _reported_status;
      DateTime _reported_status_datetime;
      Int64 _old_status;
      Int64 _new_status;
      DateTime _now;
      Int32 _seconds_between_events;
      TimeSpan _elapsed;
      Int64 _never_report;

      _sb = new StringBuilder();

      _now = WGDB.Now; _now.AddMilliseconds(-_now.Millisecond);

      ToBeReported = false;
      TechEventFlags = 0;
      TechEventDateTime = _now;

      _sb.AppendLine("SELECT   TE_WXP_REPORTED_STATUS           ");
      _sb.AppendLine("       , TE_WXP_REPORTED_STATUS_DATETIME  ");
      _sb.AppendLine("  FROM   TERMINALS ");
      _sb.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId ");

      // Get last reported status
      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            _reported_status = (Int64)_reader.GetInt64(0); // TE_WXP_REPORTED_STATUS
            _reported_status_datetime = _reader.GetDateTime(1);// TE_WXP_REPORTED_STATUS_DATETIME 
            _reported_status_datetime = _reported_status_datetime.AddMilliseconds(-_reported_status_datetime.Millisecond);
            _elapsed = _now - _reported_status_datetime;

            if (_elapsed.TotalMilliseconds > 0 && _elapsed.TotalSeconds < 1)
            {
              _now = _reported_status_datetime.AddSeconds(1);
              _elapsed = _now - _reported_status_datetime;
              TechEventDateTime = _now;
            }
          }
        }

        // Save current reported status
        TechEventFlags = _reported_status;

        _never_report = GeneralParam.GetInt64("ExternalProtocol", "TechnicalEventIgnoreMask", 0);
        if (_never_report != 0)
        {
          _never_report = _never_report & ~ALWAYS_REPORT_BIT_MASK;
          SetBitMask   &= ~_never_report;
          ResetBitMask |= _never_report;
        }

        // Reset autoreset flags and set the new bit mask
        _old_status = _reported_status & ~AUTORESET_BIT_MASK;
        _new_status = (_old_status & ~ResetBitMask) | SetBitMask ;

        // TODO XCD Add GP to DB
        // Default 60 seconds, minimum 10 seconds, maximum 68 years :D
        _seconds_between_events = GeneralParam.GetInt32("ExternalProtocol", "RepeatedTechnicalEventIgnoreSeconds", 60, 1, Int32.MaxValue);
        if (_new_status != _reported_status
          || (_new_status != 0 && _elapsed.TotalSeconds >= _seconds_between_events)
          || ((_new_status & (~ALWAYS_REPORT_BIT_MASK)) > 0))
        {
          _sb.Length = 0;

          _sb.AppendLine(" UPDATE   TERMINALS                                      ");
          _sb.AppendLine("    SET   TE_WXP_REPORTED_STATUS          = @pNewStatus  ");
          _sb.AppendLine("        , TE_WXP_REPORTED_STATUS_DATETIME = @pNow        ");
          _sb.AppendLine("  WHERE   TE_TERMINAL_ID                  = @pTerminalId ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pNewStatus", SqlDbType.BigInt).Value = _new_status;
            _sql_cmd.Parameters.Add("@pNow", SqlDbType.DateTime).Value = _now;
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

            if (_sql_cmd.ExecuteNonQuery() != 1)
            {
              return false;
            }
          }// end using

          TechEventFlags = _new_status;
          ToBeReported = true; // technical frame must be inserted
          TechEventDateTime = _now;

        }// end if
      }// end try
      catch (Exception _e)
      {
        Log.Exception(_e);
        return false;
      }

      return true;
    }// SetTerminalStatusReport

    //------------------------------------------------------------------------------
    // PURPOSE: Repeat some technical events when elapsed time is greater than 1 hour
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True if ok, false otherwise
    //
    public static Boolean RepeatTerminalStatusReport()
    {
      const Int64 REPEAT_BIT_MASK = (1 << (Int32)ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_DISCONNECTED)
                                  | (1 << (Int32)ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_HOST_DISCONNECTED)
                                  | (1 << (Int32)ENUM_TECHNICAL_EVENTS.PERU_TECHNICAL_EVENT_SAS_MACHINE_CASHBOX_DOOR_OPENED);

      StringBuilder _sb;
      Int64 _old_reported_status;
      Int32 _terminal_id;
      DataTable _dt_terminals_report_status;

      _sb = new StringBuilder();
      _dt_terminals_report_status = new DataTable();

      // Get terminals with repeated mask and reported time is 1 hour ago.
      _sb.AppendLine(" SELECT   TOP 1000 TE_TERMINAL_ID, (TE_WXP_REPORTED_STATUS & @pRepeatBitMask)  ");
      _sb.AppendLine("   FROM   TERMINALS                                                           ");
      _sb.AppendLine("  WHERE   (TE_WXP_REPORTED_STATUS & @pRepeatBitMask) <> 0                     ");
      _sb.AppendLine("    AND   DATEDIFF (MINUTE, TE_WXP_REPORTED_STATUS_DATETIME, GETDATE()) >= 60 ");
      _sb.AppendLine("    AND   TE_STATUS = 0");
      _sb.AppendLine("    AND   TE_TERMINAL_TYPE IN ( " + Misc.GamingTerminalTypeListToString() + " ) ");
      _sb.AppendLine("  ORDER BY TE_WXP_REPORTED_STATUS_DATETIME DESC ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pRepeatBitMask", SqlDbType.BigInt).Value = REPEAT_BIT_MASK;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(_dt_terminals_report_status);
            }
          }// end using
        }// end using

        foreach (DataRow _row in _dt_terminals_report_status.Rows)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _terminal_id = (Int32)_row[0];         //TE_TERMINAL_ID
            _old_reported_status = (Int64)_row[1]; //TE_WXP_REPORTED_STATUS

            if ( !InsertTechnicalEvent(_terminal_id, _old_reported_status, 0, WGDB.Now, _db_trx.SqlTransaction) )
            {
              Log.Error("RepeatTerminalStatusReport.InsertTechnicalEvent (" + _terminal_id.ToString() + ", " + _old_reported_status.ToString());

              continue;
            }

            _db_trx.Commit();

          }// end using
        }// end foreach
      }// end try
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    }// RepeatTerminalStatusReport
    
  } // class PeruEvents

  //------------------------------------------------------------------------------
  // PURPOSE: Class to manage terminal information for frames
  // 
  // NOTES:
  //
  //------------------------------------------------------------------------------
  public static class PeruTerminalInfo
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Get Terminal Resister Info for Peru frames
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TerminalId
    //          - Trx
    //
    //      - OUTPUT:
    //          - SiteRegister
    //          - MachineRegister
    //          - EBoxMacAddress
    //
    // RETURNS:
    //      - True if ok, false otherwise
    //
    public static Boolean Get(Int32 TerminalId, out String SiteRegister, out String MachineRegister, out String EBoxMacAddress, out Boolean PreviouslyReported, SqlTransaction Trx)
    {
      StringBuilder _sb;


      MachineRegister = "";
      EBoxMacAddress = "";
      PreviouslyReported = false;
      SiteRegister = Misc.ReadGeneralParams("ExternalProtocol", "SiteRegistrationCode");
      SiteRegister = SiteRegister.Trim();
      if (String.IsNullOrEmpty(SiteRegister))
      {
        Log.Error("ExternalProtocol.SiteRegistrationCode is not defined.");

        return false;
      }

      _sb = new StringBuilder();
      _sb.AppendLine("SELECT   ISNULL (TE_EXTERNAL_ID, '')       ");
      _sb.AppendLine("       , ISNULL (TE_REGISTRATION_CODE, '') ");
      _sb.AppendLine("       , TE_WXP_REPORTED                   ");
      _sb.AppendLine("  FROM   TERMINALS ");
      _sb.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId ");

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }
            EBoxMacAddress = _reader.GetString(0);
            MachineRegister = _reader.GetString(1);
            PreviouslyReported = _reader.IsDBNull(2) ? false : _reader.GetBoolean(2);
          }
        }

        MachineRegister = MachineRegister.Trim();
        EBoxMacAddress = EBoxMacAddress.Trim();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Get

    //------------------------------------------------------------------------------
    // PURPOSE : Get Machine Meters from Database
    //
    //  PARAMS :
    //      - INPUT: 
    //            - TerminalId
    //            - Trx
    //
    //      - OUTPUT :
    //            - MachineMeters
    //
    // RETURNS :
    //      - True: sent ok,
    //      - False: otherwise
    //
    //   NOTES : 
    //
    public static Boolean GetMachineMeters(Int32 TerminalId, out DataTable MachineMeters, SqlTransaction Trx)
    {
      StringBuilder _sql_sb;

      _sql_sb = new StringBuilder();

      MachineMeters = new DataTable();

      try
      {
        _sql_sb.AppendLine(" SELECT   TE_TERMINAL_ID               ");
        _sql_sb.AppendLine("        , TE_PROVIDER_ID         ");
        _sql_sb.AppendLine("        , TE_NAME         ");
        _sql_sb.AppendLine("        , TE_REGISTRATION_CODE         ");
        _sql_sb.AppendLine("        , ISNULL(MM_PLAYED_AMOUNT,  0) MM_PLAYED_AMOUNT  ");
        _sql_sb.AppendLine("        , ISNULL(MM_WON_AMOUNT,     0) MM_WON_AMOUNT     ");
        _sql_sb.AppendLine("        , ISNULL(MM_JACKPOT_AMOUNT, 0) MM_JACKPOT_AMOUNT ");
        _sql_sb.AppendLine("   FROM   TERMINALS LEFT JOIN MACHINE_METERS ON TE_TERMINAL_ID = MM_TERMINAL_ID ");
        _sql_sb.AppendLine("  WHERE  (        TE_STATUS = @pStatusActive ");
        _sql_sb.AppendLine("              OR  TE_STATUS = @pOutOfService ");
        _sql_sb.AppendLine("              OR (TE_STATUS = @pStatusRetired AND DATEDIFF(DAY, TE_RETIREMENT_DATE, GETDATE()) <= @pNumDays)) ");
        _sql_sb.AppendLine("    AND   TE_REGISTRATION_CODE IS NOT NULL ");
        if (TerminalId > 0)
        {
          _sql_sb.AppendLine("    AND   MM_TERMINAL_ID = @pTerminalId ");
        }
        _sql_sb.AppendLine("  ORDER BY  TE_PROVIDER_ID, TE_NAME ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_sb.ToString(), Trx.Connection, Trx))
        {
          if (TerminalId > 0)
          {
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          }

          _sql_cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = (Int32)TerminalStatus.ACTIVE;
          _sql_cmd.Parameters.Add("@pOutOfService", SqlDbType.Int).Value = (Int32)TerminalStatus.OUT_OF_SERVICE;
          _sql_cmd.Parameters.Add("@pStatusRetired", SqlDbType.Int).Value = (Int32)TerminalStatus.RETIRED;
          _sql_cmd.Parameters.Add("@pNumDays", SqlDbType.Int).Value = 5;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(MachineMeters);
          }
        }

        return true;

      }
      catch (Exception _ex)
      {
        MachineMeters = new DataTable();

        Log.Exception(_ex);
      }

      return false;

    } // GetMachineMeters

    public static Boolean GetMachineMeters(out DataTable MachineMeters, SqlTransaction Trx)
    {

      return GetMachineMeters(0, out MachineMeters, Trx);

    } // GetMachineMeters

  } // class PeruTerminalInfo

  //------------------------------------------------------------------------------
  // PURPOSE: Class to create Frame Item
  // 
  // NOTES:
  //
  //------------------------------------------------------------------------------
  class PeruFrameItem
  {
    internal String m_contenido;
    internal String m_abreviatura;
    internal int m_longitud;

    //------------------------------------------------------------------------------
    // PURPOSE: PeruFrameItem
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Contenido
    //          - Abreviatura
    //          - Longitud
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    internal PeruFrameItem(String Contenido, String Abreviatura, int Longitud)
    {
      m_contenido = Contenido;
      m_abreviatura = Abreviatura;
      m_longitud = Longitud;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: PeruFrameItem
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Contenido
    //          - Longitud
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    internal PeruFrameItem(String Contenido, int Longitud)
    {
      m_contenido = Contenido;
      m_abreviatura = "";
      m_longitud = Longitud;
    }
  }

  //------------------------------------------------------------------------------
  // PURPOSE: Class to create Frame Item List
  // 
  // NOTES:
  //
  //------------------------------------------------------------------------------
  class PeruFrameItemList : List<PeruFrameItem>
  {
    private int m_header_length = 0;

    //------------------------------------------------------------------------------
    // PURPOSE: HeaderEnd
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public void HeaderEnd()
    {
      m_header_length = 0;
      foreach (PeruFrameItem _item in this)
      {
        m_header_length += _item.m_longitud;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: HeaderLength
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public int HeaderLength
    {
      get
      {
        return m_header_length;
      }
    }
  }

  //------------------------------------------------------------------------------
  // PURPOSE: Class to create frame
  // 
  // NOTES:
  //
  //------------------------------------------------------------------------------
  public static class PeruFrameCreator
  {
    const String METER_FORMAT = "0000000000";
    const String DATE_FORMAT = "yyMMddHHmmss";

    private static void ComputeCRC(Byte[] Frame)
    {
      const int OffsetToData = 3;
      Crc16Ccitt _crc;
      short _crc_value;
      Byte[] _aux;

      if (OffsetToData >= Frame.Length)
      {
        return;
      }
      _crc = new Crc16Ccitt(InitialCrcValue.NonZero1);
      Frame[1] = 0x00;
      Frame[2] = 0x00;

      _aux = new byte[Frame.Length - OffsetToData];
      Array.Copy(Frame, OffsetToData, _aux, 0, _aux.Length);

      // CRC --> HostToNetwork
      _crc_value = System.Net.IPAddress.HostToNetworkOrder((short)_crc.ComputeChecksum(_aux));
      _aux = BitConverter.GetBytes(_crc_value);
      Frame[1] = _aux[0];
      Frame[2] = _aux[1];
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Create Technical Frame
    // 
    //  PARAMS:
    //      - INPUT:
    //          - SiteRegister
    //          - MachineRegister
    //          - EBoxMacAddress
    //          - EventDateTime
    //          - Event
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Frame
    //
    public static Byte[] TechnicalFrame(String SiteRegister, String MachineRegister, String EboxMacAddress, DateTime EventDateTime, PeruEvents.ENUM_TECHNICAL_EVENTS Event)
    {
      return TechnicalFrame(SiteRegister, MachineRegister, EboxMacAddress, EventDateTime, (1 << (Int32)Event));
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Create Technical Frame
    // 
    //  PARAMS:
    //      - INPUT:
    //          - SiteRegister
    //          - MachineRegister
    //          - EBoxMacAddress
    //          - EventDateTime
    //          - Event
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Frame
    //
    public static Byte[] TechnicalFrame(String SiteRegister, String MachineRegister, String EboxMacAddress, DateTime EventDateTime, Int64 TechnicalEventFlags)
    {
      StringBuilder _sb;
      Byte[] _frame;
      String _str;
      String _id_collector;
      String _bits;
      Int64 _bit_test;

      _str = (SiteRegister == null) ? "" : SiteRegister;
      SiteRegister = _str.Trim().PadLeft(9, '0').Substring(0, 9);

      _str = (MachineRegister == null) ? "" : MachineRegister;
      MachineRegister = _str.Trim().PadLeft(8, '0').Substring(0, 8);

      _str = (EboxMacAddress == null) ? "" : EboxMacAddress;
      _str = _str.Replace("MAC", "");
      _str = _str.Replace("-", "");
      _str = _str.Replace(" ", "");
      _id_collector = _str.Trim().PadRight(12, ' ').Substring(0, 12);

      _sb = new StringBuilder();
      _sb.Append("T");                                    // Cabecera
      _sb.Append("00");                                   // CRC 
      _sb.Append(SiteRegister);                           // RSDGJCMT
      _sb.Append(MachineRegister);                        // RMT
      _sb.Append(_id_collector);                          // ID Collector
      _sb.Append(EventDateTime.ToString(DATE_FORMAT));    // FECHA

      // Datos
      //  P�rdida Comunicaci�n entre Colector y M�quina
      //  P�rdida Comunicaci�n entre Colector y Sala
      //  Restauraci�n por cortes de energ�a
      //  Error en M�quina por Bater�a Baja en RAM
      //  Apertura de Puerta de Caja L�gica
      //  Falla de Memoria del Colector
      //  Falla de la RAM de la M�quina
      //  Cereo (borrado del contenido de la memoria)
      //  Meter Roll Over // AJQ 25-JUL-2013 The flag Reserva 1 indicates the Meter Roll Over
      //  Reserva 2

      _bits = "";
      for (int _flag = 1; _flag <= 10; _flag++)
      {
        _bit_test = (1 << _flag);
        if ((TechnicalEventFlags & _bit_test) == _bit_test)
        { _bits += "1"; }
        else
        { _bits += "0"; }
      }

      _sb.Append(_bits);

      _frame = Encoding.ASCII.GetBytes(_sb.ToString());
      ComputeCRC(_frame);

      return _frame;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Create Economical Frame
    // 
    //  PARAMS:
    //      - INPUT:
    //          - SiteRegister
    //          - MachineRegister
    //          - EventDateTime
    //          - CoinIn
    //          - CoinOut
    //          - Handpay
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Frame
    //
    public static Byte[] EconomicalFrame(String SiteRegister, String MachineRegister, DateTime EventDateTime, Decimal CoinIn, Decimal CoinOut, Decimal Jackpot)
    {
      StringBuilder _sb;
      Int64 _meter_value;
      Byte[] _frame;
      String _str;

      _str = (SiteRegister == null) ? "" : SiteRegister;
      SiteRegister = _str.Trim().PadLeft(9, '0').Substring(0, 9);

      _str = (MachineRegister == null) ? "" : MachineRegister;
      MachineRegister = _str.Trim().PadLeft(8, '0').Substring(0, 8);

      _sb = new StringBuilder();
      _sb.Append("E");                                    // Cabecera
      _sb.Append("00");                                   // CRC 
      _sb.Append(SiteRegister);                           // RSDGJCMT
      _sb.Append(MachineRegister);                        // RMT
      _sb.Append(EventDateTime.ToString(DATE_FORMAT));    // FECHA
      // Datos
      _sb.Append(1.ToString("0"));                        // ID Moneda (1=soles, 2=d�lares)
      _sb.Append(1.ToString("0000"));                     // Denominaci�n

      _meter_value = (Int64)(CoinIn * 100); _sb.Append(_meter_value.ToString(METER_FORMAT));   // CIF
      _meter_value = (Int64)(CoinOut * 100); _sb.Append(_meter_value.ToString(METER_FORMAT));  // COF
      _meter_value = (Int64)(Jackpot * 100); _sb.Append(_meter_value.ToString(METER_FORMAT));  // PMF

      _sb.Append(0.ToString(METER_FORMAT));               // OPF
      _sb.Append(1000.ToString("00000"));                 // TC 1 -- x 1000 --> 01000
      _sb.Append(0.ToString(METER_FORMAT));               // Reserva

      _frame = Encoding.ASCII.GetBytes(_sb.ToString());
      ComputeCRC(_frame);

      return _frame;
    }
  }

  //------------------------------------------------------------------------------
  // PURPOSE: Class to manage Frames
  // 
  // NOTES:
  //
  //------------------------------------------------------------------------------
  public class PeruFrameViewer
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Economical Frame
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Economical Frame
    //
    private static PeruFrameItemList EFrame()
    {
      PeruFrameItemList _frame = new PeruFrameItemList();

      _frame.Add(new PeruFrameItem("Cabecera", "E", 1));
      _frame.Add(new PeruFrameItem("Check de Redundancia C�clica", "CRC", 2));
      _frame.Add(new PeruFrameItem("Registro de Sala DGJCMT", "RSDGJCMT", 9));
      _frame.Add(new PeruFrameItem("Registro M�quina DGJCMT", "RMT", 8));
      _frame.Add(new PeruFrameItem("FECHA", 12));
      _frame.HeaderEnd();

      // Datos
      _frame.Add(new PeruFrameItem("ID Moneda (1=soles, 2=d�lares)", 1));
      _frame.Add(new PeruFrameItem("Denominaci�n", "D", 4));
      _frame.Add(new PeruFrameItem("COIN IN FINAL", "CIF", 10));
      _frame.Add(new PeruFrameItem("COIN OUT FINAL", "COF", 10));
      _frame.Add(new PeruFrameItem("PMA FINAL", "PMF", 10));
      _frame.Add(new PeruFrameItem("OTRO CONTADOR FINAL", "OPF", 10));
      _frame.Add(new PeruFrameItem("TIPO DE CAMBIO", "TC", 5));
      _frame.Add(new PeruFrameItem("Reserva 1", 10));

      return _frame;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Technical Frame
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Technical Frame
    //
    private static PeruFrameItemList TFrame()
    {
      PeruFrameItemList _frame = new PeruFrameItemList();

      _frame.Add(new PeruFrameItem("Cabecera", "T", 1));
      _frame.Add(new PeruFrameItem("Check de Redundancia C�clica", "CRC", 2));
      _frame.Add(new PeruFrameItem("Registro de Sala DGJCMT", "RSDGJCMT", 9));
      _frame.Add(new PeruFrameItem("Registro M�quina DGJCMT", "RMT", 8));
      _frame.Add(new PeruFrameItem("ID Colector", 12));
      _frame.Add(new PeruFrameItem("FECHA", 12));
      _frame.HeaderEnd();

      // Datos
      _frame.Add(new PeruFrameItem("P�rdida Comunicaci�n entre Colector y M�quina", 1));
      _frame.Add(new PeruFrameItem("P�rdida Comunicaci�n entre Colector y Sala", 1));
      _frame.Add(new PeruFrameItem("Restauraci�n por cortes de energ�a", 1));
      _frame.Add(new PeruFrameItem("Error en M�quina por Bater�a Baja en RAM", 1));
      _frame.Add(new PeruFrameItem("Apertura de Puerta de Caja L�gica", 1));
      _frame.Add(new PeruFrameItem("Falla de Memoria del Colector", 1));
      _frame.Add(new PeruFrameItem("Falla de la RAM de la M�quina", 1));
      _frame.Add(new PeruFrameItem("Meters Reset / RAM Clear", 1));
      _frame.Add(new PeruFrameItem("Meters Rollover", 1));
      _frame.Add(new PeruFrameItem("Reserva 2", 1));

      return _frame;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: ToBase16
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String in Hexa
    //
    private static String ToBase16(Byte[] Item, int Start, int Count)
    {
      return BitConverter.ToString(Item, Start, Count).Replace("-", " ");
    }

    //------------------------------------------------------------------------------
    // PURPOSE: ToBase02
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String in Binary
    //
    private static String ToBase02(Byte[] Item, int Start, int Count)
    {
      String _base16;
      String[] _bits = new String[] { "0000", "0001", "0010", "0011",
                                      "0100", "0101", "0110", "0111",
                                      "1000", "1001", "1010", "1011",
                                      "1100", "1101", "1110", "1111"};
      Byte[] _nibbles;
      Byte _zero;
      Byte _character_A;
      StringBuilder _sb;

      _base16 = ToBase16(Item, Start, Count);
      _nibbles = Encoding.ASCII.GetBytes(_base16.Replace(" ", ""));
      _zero = Encoding.ASCII.GetBytes("0")[0];
      _character_A = Encoding.ASCII.GetBytes("A")[0];

      _sb = new StringBuilder();
      int _idx_nibble;
      _idx_nibble = 0;
      foreach (Byte _nibble in _nibbles)
      {
        int _idx;

        _idx = _nibble - _zero;
        if (_idx >= 0 && _idx < 10)
        {
          _sb.Append(_bits[_idx]);
          _idx_nibble++;
        }

        _idx = _nibble - _character_A + 10;
        if (_idx >= 10 && _idx < 16)
        {
          _sb.Append(_bits[_idx]);
          _idx_nibble++;
        }

        if (_idx_nibble % 2 == 0)
        {
          _sb.Append(" ");
        }
      }

      return _sb.ToString().TrimEnd();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Data from Frame
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Frame Data
    //
    private static Byte[] Data(Byte[] Frame)
    {
      PeruFrameItemList _items;

      if (Frame[0] == 'E')
      {
        _items = EFrame();
      }
      else if (Frame[0] == 'T')
      {
        _items = TFrame();
      }
      else
      {
        return new Byte[0];
      }

      if (Frame.Length < _items.HeaderLength)
      {
        return new Byte[0];
      }

      Byte[] _data;

      _data = new Byte[Frame.Length - _items.HeaderLength];

      Array.Copy(Frame, _items.HeaderLength, _data, 0, _data.Length);

      return _data;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Frame to String
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String Frame
    //
    public static String ToString(Byte[] Frame, Boolean Hexadecimal, Boolean Binary)
    {
      PeruFrameItemList _items;
      StringBuilder _sb;

      int _idx_read;

      _sb = new StringBuilder();

      if (Frame[0] == 'E')
      {
        _items = EFrame();
      }
      else if (Frame[0] == 'T')
      {
        _items = TFrame();
      }
      else
      {
        _sb.AppendLine(ToBase16(Frame, 0, Frame.Length));

        return _sb.ToString();
      }

      _idx_read = 0;
      foreach (PeruFrameItem _item in _items)
      {
        String _text;
        String _b16;
        String _b02;


        _text = Encoding.ASCII.GetString(Frame, _idx_read, _item.m_longitud);
        _b16 = ToBase16(Frame, _idx_read, _item.m_longitud);
        _b02 = ToBase02(Frame, _idx_read, _item.m_longitud);
        _idx_read += _item.m_longitud;

        if (_item.m_abreviatura == "CRC")
        {
          _text = "[" + _b16.Replace(" ", "") + "]";
        }

        _sb.Append(_item.m_contenido.PadRight(50, ' ') + " " + _item.m_abreviatura.PadRight(10, ' ') + " L" + _item.m_longitud.ToString("00") + "     " + _text.PadRight(12));
        if (Hexadecimal) _sb.Append(" [Hexa: " + _b16.Replace(" ", "").PadRight(24) + "]");
        if (Binary) _sb.Append(" [Bin: " + _b02 + "]");
        _sb.AppendLine();
      }

      return _sb.ToString();
    }

    public static List<string> ToStringArray(Byte[] Frame)
    {
      PeruFrameItemList _items;
      List<string> _array;
      int _idx_read;

      _array = new List<string>();

      if (Frame[0] == 'E')
      {
        _items = EFrame();
      }
      else if (Frame[0] == 'T')
      {
        _items = TFrame();
      }
      else
      {
        _array.Add(ToBase16(Frame, 0, Frame.Length));

        return _array;
      }

      _idx_read = 0;
      foreach (PeruFrameItem _item in _items)
      {
        String _text;
        String _b16;

        _text = Encoding.ASCII.GetString(Frame, _idx_read, _item.m_longitud);
        _b16 = ToBase16(Frame, _idx_read, _item.m_longitud);

        _idx_read += _item.m_longitud;

        if (_item.m_abreviatura == "CRC")
        {
          _text = "[" + _b16.Replace(" ", "") + "]";
        }

        _array.Add(_text);
      }

      return _array;
    }
  }

  public enum InitialCrcValue { Zeros, NonZero1 = 0xffff, NonZero2 = 0x1D0F }

  public class Crc16Ccitt
  {
    const ushort poly = 4129;
    ushort[] table = new ushort[256];
    ushort initialValue = 0;

    public ushort ComputeChecksum(byte[] bytes)
    {
      ushort crc = this.initialValue;
      for (int i = 0; i < bytes.Length; ++i)
      {
        crc = (ushort)((crc << 8) ^ table[((crc >> 8) ^ (0xff & bytes[i]))]);
      }
      return crc;
    }

    public byte[] ComputeChecksumBytes(byte[] bytes)
    {
      ushort crc = ComputeChecksum(bytes);
      return BitConverter.GetBytes(crc);
    }

    public Crc16Ccitt(InitialCrcValue initialValue)
    {
      this.initialValue = (ushort)initialValue;
      ushort temp, a;
      for (int i = 0; i < table.Length; ++i)
      {
        temp = 0;
        a = (ushort)(i << 8);
        for (int j = 0; j < 8; ++j)
        {
          if (((temp ^ a) & 0x8000) != 0)
          {
            temp = (ushort)((temp << 1) ^ poly);
          }
          else
          {
            temp <<= 1;
          }
          a <<= 1;
        }
        table[i] = temp;
      }
    }
  }
}
