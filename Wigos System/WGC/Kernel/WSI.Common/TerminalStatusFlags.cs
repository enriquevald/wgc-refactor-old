//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : TerminalStatusFlags.cs
// 
//   DESCRIPTION : Class to contain information regarding status of a terminal
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-JAN-2014 ICS    First release.
// 17-JAN-2014 ICS    Added new flags
// 07-FEB-2014 ICS    Fixed Bug WIG-1053: Error in WCP log: Invalid column name 'TS_CALL_ATTENDANT'
// 12-FEB-2014 MPO    Change TS_STACKER_COUNTER is not null.
// 08-APR-2014 AMF    Added validation software
// 20-OCT-2015 GMV    TFS 5277 : HighRollers Alarm
// 11-NOV-2015 GMV    TFS 5275 : HighRollers Extended definition
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common
{
  public static class TerminalStatusFlags
  {
    #region Enums

    public enum WCP_TerminalEvent
    {
      BillIn = 0,
      TicketOut = 1,
      TicketIn = 2,
      ChangeStacker = 3,
      CoinsIn = 4,
    }

    public enum STACKER_STATUS
    {
      OK = 0,
      ALMOST_FULL = 1,
      FULL = 2,
    }

    public enum BITMASK_TYPE
    {
      None = 0,
      Door = 1,
      Bill = 2,
      Printer = 3,
      EGM = 4,
      Plays = 5,
      Jackpot = 6,
      Sas_host_error = 7,
      Call_attendant = 8,
      Stacker_status = 9,
      Machine_status = 10,
      Coin = 11,
    }

    [Flags]
    public enum DOOR_FLAG
    {
      NONE = 0,                       // 000000
      BELLY_DOOR = 1,                 // 000001
      CARD_CAGE = 2,                  // 000010
      CASHBOX_DOOR = 4,               // 000100
      DROP_DOOR = 8,                  // 001000
      SLOT_DOOR = 16,                 // 010000
    }

    [Flags]
    public enum BILL_FLAG
    {
      NONE = 0,                       // 000000
      BILL_JAM = 1,                   // 000001
      HARD_FAILURE = 2,               // 000010
      COUNTERFEIT_EXCEEDED = 4,       // 000100
    }

    [Flags]
    public enum COIN_FLAG
    {
      NONE = 0,                       // 000000
      COIN_IN_TILT = 1,               // 000001
      REVERSE_COIN = 2,               // 000010
      COIN_IN_LOCKOUT = 4,            // 000100
    }

    [Flags]
    public enum PRINTER_FLAG
    {
      NONE = 0,                       // 000000
      CARRIAGE_JAM = 1,               // 000001
      COMM_ERROR = 2,                 // 000010
      PAPER_LOW = 4,                  // 000100
      PAPER_OUT_ERROR = 8,            // 001000
      POWER_ERROR = 16,               // 010000
      REPLACE_RIBBON = 32,            // 100000
    }

    [Flags]
    public enum EGM_FLAG
    {
      NONE = 0,                       // 00000000000
      CMOS_RAM_DATA_RECOVERED = 1,    // 00000000001
      CMOS_RAM_NO_DATA_RECOVERED = 2, // 00000000010
      CMOS_RAM_BAD_DEVICE = 4,        // 00000000100
      EPROM_DATA_ERROR = 8,           // 00000001000
      EPROM_BAD_DEVICE = 16,          // 00000010000
      EPROM_DIFF_CHECKSUM = 32,       // 00000100000
      EPROM_BAD_CHECKSUM = 64,        // 00001000000
      PART_EPROM_DIFF_CHECKSUM = 128, // 00010000000
      PART_EPROM_BAD_CHECKSUM = 256,  // 00100000000
      LOW_BACKUP_BATTERY = 512,       // 01000000000
      WITH_TECHNICIAN = 1024          // 10000000000     
    }

    [Flags]
    public enum PLAYS_FLAG
    {
      NONE = 0,                       // 000000
      PLAYED = 1,                     // 000001
      WON = 2,                        // 000010 
      CURRENT_PAYOUT = 4,             // 000100
      WITHOUT_PLAYS = 8,              // 001000
      HIGHROLLER = 16,                // 010000
      HIGHROLLER_ANONYMOUS = 32,      // 100000
    }

    [Flags]
    public enum JACKPOT_FLAG
    {
      NONE = 0,                       // 000000
      JACKPOTS = 1,                   // 000001
      CANCELED_CREDIT = 2,            // 000010
    }

    [Flags]
    public enum MACHINE_FLAGS
    {
      NON_BLOCKED = 0,
      BLOCKED_NO_LICENSE = 1,
      BLOCKED_BY_SIGNATURE = 2,
      BLOCKED_MANUALLY = 4,
      BLOCKED_AUTOMATICALLY = 8,
      ALWAYS_BLOCKED = 16,
      ALWAYS_UNBLOCKED = 32,
      BLOCKED_INTRUSION = 64,
    }

    #endregion

    #region Private Methods

    // PURPOSE: Count set bits in a bitmask
    //
    // PARAMS:
    //     - INPUT:
    //           - Bitmask
    //     - OUTPUT:
    //           - None
    //
    // RETURNS:
    //     - None
    //
    public static UInt32 CountSetBits(UInt32 Bitmask)
    {
      UInt32 _count;
      _count = 0;

      while (Bitmask > 0)
      {
        Bitmask = Bitmask & (Bitmask - 1);
        _count += 1;
      }

      return _count;
    }

    // PURPOSE : Set flag value in DB
    //
    //  PARAMS :
    //      - INPUT :
    //          - DB_column
    //          - TerminalId
    //          - Flag
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //          - Bitmask
    //
    // RETURNS :
    //      - True: If it could set the value successfully
    //      - False: Otherwise
    //
    private static Boolean DB_SetFlag(String DBColumn, Int32 TerminalId, Int32 Flag, SqlTransaction Trx)
    {
      StringBuilder _sb;
      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" IF EXISTS (  SELECT   1  ");
        _sb.AppendLine("              FROM   TERMINAL_STATUS ");
        _sb.AppendLine("              WHERE   TS_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("           )  ");
        _sb.AppendLine("      UPDATE   TERMINAL_STATUS ");
        _sb.AppendLine("         SET   " + DBColumn + " = " + DBColumn + " | @pFlag ");
        _sb.AppendLine("       WHERE   TS_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("        ELSE  ");
        _sb.AppendLine(" INSERT INTO TERMINAL_STATUS ");
        _sb.AppendLine("            ( TS_TERMINAL_ID ");
        _sb.AppendLine("            , " + DBColumn);
        _sb.AppendLine("            ) ");
        _sb.AppendLine("      VALUES ");
        _sb.AppendLine("            ( @pTerminalId ");
        _sb.AppendLine("            , @pFlag ");
        _sb.AppendLine("            ) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pFlag", SqlDbType.Int).Value = Flag;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("DB_SetFlag: Error setting " + DBColumn + " status of Terminal: " + TerminalId);

            return false;
          }
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("DB_SetFlag: Error setting " + DBColumn + " status of Terminal: " + TerminalId);
        Log.Exception(_ex);

        return false;
      }
    }

    // PURPOSE : Unset flag value in DB
    //
    //  PARAMS :
    //      - INPUT :
    //          - DB_column
    //          - TerminalId
    //          - Flag
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //          - Bitmask
    //
    // RETURNS :
    //      - True: If it could set the value successfully
    //      - False: Otherwise
    //
    private static Boolean DB_UnsetFlag(String DBColumn, Int32 TerminalId, Int32 Flag, SqlTransaction Trx)
    {
      StringBuilder _sb;
      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" IF EXISTS (  SELECT   1  ");
        _sb.AppendLine("              FROM   TERMINAL_STATUS ");
        _sb.AppendLine("              WHERE   TS_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("           )  ");
        _sb.AppendLine("      UPDATE   TERMINAL_STATUS ");
        _sb.AppendLine("         SET   " + DBColumn + " = " + DBColumn + " & (~@pFlag) ");
        _sb.AppendLine("       WHERE   TS_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("        ELSE  ");
        _sb.AppendLine(" INSERT INTO TERMINAL_STATUS ");
        _sb.AppendLine("            ( TS_TERMINAL_ID ");
        _sb.AppendLine("            , " + DBColumn);
        _sb.AppendLine("            ) ");
        _sb.AppendLine("      VALUES ");
        _sb.AppendLine("            ( @pTerminalId ");
        _sb.AppendLine("            , @pFlag ");
        _sb.AppendLine("            ) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pFlag", SqlDbType.Int).Value = Flag;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("DB_UnsetFlag: Error setting " + DBColumn + " status of Terminal: " + TerminalId);

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("DB_UnsetFlag: Error setting " + DBColumn + " status of Terminal: " + TerminalId);
        Log.Exception(_ex);

        return false;
      }
    }

    // PURPOSE : Get a bitmask value from DB
    //
    //  PARAMS :
    //      - INPUT :
    //          - DB_column
    //          - TerminalId
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //          - Bitmask
    //
    // RETURNS :
    //      - True: If it could read the value successfully
    //      - False: Otherwise
    //
    private static Boolean DB_GetBitmask(String DBColumn, Int32 TerminalId, out Int32 Bitmask, SqlTransaction Trx)
    {
      {
        StringBuilder _sb;

        Bitmask = 0;

        try
        {
          _sb = new StringBuilder();
          _sb.AppendLine(" SELECT   ISNULL(" + DBColumn + ", 0) ");
          _sb.AppendLine("   FROM   TERMINAL_STATUS ");
          _sb.AppendLine("  WHERE   TS_TERMINAL_ID = @pTerminalId ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                Log.Error("DB_GetBitmask: Error getting " + DBColumn + " status of Terminal: " + TerminalId);

                return false;
              }

              Bitmask = _reader.GetInt32(0);
            }
          }
          return true;
        }
        catch (Exception _ex)
        {
          Log.Error("DB_GetBitmask: Error getting " + DBColumn + " status of Terminal: " + TerminalId);
          Log.Exception(_ex);

          return false;
        }
      }
    }

    // PURPOSE : Increase terminal stacker counter
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - ResetCounter: If it's set, the counter is reset to zero
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          - True: If it has been updated successfully.
    //          - False: Otherwise
    //
    private static Boolean IncreaseTerminalStacker(Int32 TerminalId, Boolean ResetCounter, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        if (ResetCounter && !TerminalStatusFlags.DB_SetValue(BITMASK_TYPE.Stacker_status, TerminalId, (Int32)STACKER_STATUS.OK, Trx))
        {
          Log.Error("UpdateTerminalStacker: Error resetting stacker status of Terminal: " + TerminalId);
          return false;
        }

        _sb = new StringBuilder();
        _sb.AppendLine(" IF EXISTS (  SELECT   1  ");
        _sb.AppendLine("              FROM   TERMINAL_STATUS ");
        _sb.AppendLine("              WHERE   TS_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("           )  ");
        _sb.AppendLine("      UPDATE  TERMINAL_STATUS  ");
        if (ResetCounter)
        {
          _sb.AppendLine("       SET  TS_STACKER_COUNTER = 0");
        }
        else
        {
          _sb.AppendLine("         SET  TS_STACKER_COUNTER = TS_STACKER_COUNTER + 1");
        }
        _sb.AppendLine("       WHERE  TS_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("        ELSE  ");
        _sb.AppendLine(" INSERT INTO TERMINAL_STATUS ");
        _sb.AppendLine("            ( TS_TERMINAL_ID ");
        _sb.AppendLine("            , TS_STACKER_COUNTER ");
        _sb.AppendLine("            ) ");
        _sb.AppendLine("      VALUES ");
        _sb.AppendLine("            ( @pTerminalId ");
        if (ResetCounter)
        {
          _sb.AppendLine("          , 0");
        }
        else
        {
          _sb.AppendLine("          , 1");
        }
        _sb.AppendLine("            ) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("UpdateTerminalStacker: Error updating Stacker counter of Terminal: " + TerminalId);
            return false;
          }
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("UpdateTerminalStacker: Error updating Stacker counter of Terminal: " + TerminalId);
        Log.Exception(_ex);

        return false;
      }
    } // IncreaseTerminalStacker

    // PURPOSE : Get a bitmask value from DB
    //
    //  PARAMS :
    //      - INPUT :
    //          - Bitmask Type
    //          - TerminalId
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //          - Bitmask
    //
    // RETURNS :
    //      - True: If it could read the value successfully
    //      - False: Otherwise
    //
    private static Boolean DB_GetBitmask(BITMASK_TYPE Type, Int32 TerminalId, out Int32 Bitmask, SqlTransaction Trx)
    {
      Bitmask = 0;

      switch (Type)
      {
        case BITMASK_TYPE.Door:
          return DB_GetBitmask("TS_DOOR_FLAGS", TerminalId, out Bitmask, Trx);
        case BITMASK_TYPE.Bill:
          return DB_GetBitmask("TS_BILL_FLAGS", TerminalId, out Bitmask, Trx);
        case BITMASK_TYPE.Printer:
          return DB_GetBitmask("TS_PRINTER_FLAGS", TerminalId, out Bitmask, Trx);
        case BITMASK_TYPE.EGM:
          return DB_GetBitmask("TS_EGM_FLAGS", TerminalId, out Bitmask, Trx);
        case BITMASK_TYPE.Plays:
          return DB_GetBitmask("TS_PLAYED_WON_FLAGS", TerminalId, out Bitmask, Trx);
        case BITMASK_TYPE.Jackpot:
          return DB_GetBitmask("TS_JACKPOT_FLAGS", TerminalId, out Bitmask, Trx);
        case BITMASK_TYPE.Machine_status:
          return DB_GetBitmask("TS_MACHINE_FLAGS", TerminalId, out Bitmask, Trx);
        default:
          return false;
      }
    }



    #endregion

    #region Public Methods

    // PURPOSE : Set flag value in DB
    //
    //  PARAMS :
    //      - INPUT :
    //          - Bitmask Type
    //          - TerminalId
    //          - Flag
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //          - Bitmask
    //
    // RETURNS :
    //      - True: If it could set the value successfully
    //      - False: Otherwise
    //
    public static Boolean DB_SetFlag(BITMASK_TYPE Type, Int32 TerminalId, Int32 Flag, SqlTransaction Trx)
    {
      switch (Type)
      {
        case BITMASK_TYPE.Door:
          return DB_SetFlag("TS_DOOR_FLAGS", TerminalId, Flag, Trx);
        case BITMASK_TYPE.Bill:
          return DB_SetFlag("TS_BILL_FLAGS", TerminalId, Flag, Trx);
        case BITMASK_TYPE.Coin:
          return DB_SetFlag("TS_COIN_FLAGS", TerminalId, Flag, Trx);
        case BITMASK_TYPE.Printer:
          return DB_SetFlag("TS_PRINTER_FLAGS", TerminalId, Flag, Trx);
        case BITMASK_TYPE.EGM:
          return DB_SetFlag("TS_EGM_FLAGS", TerminalId, Flag, Trx);
        case BITMASK_TYPE.Plays:
          return DB_SetFlag("TS_PLAYED_WON_FLAGS", TerminalId, Flag, Trx);
        case BITMASK_TYPE.Jackpot:
          return DB_SetFlag("TS_JACKPOT_FLAGS", TerminalId, Flag, Trx);
        case BITMASK_TYPE.Machine_status:
          return DB_SetFlag("TS_MACHINE_FLAGS", TerminalId, Flag, Trx);
        default:
          return false;
      }
    } // DB_SetFlag

    // PURPOSE : Unset flag value in DB
    //
    //  PARAMS :
    //      - INPUT :
    //          - Bitmask Type
    //          - TerminalId
    //          - Flag
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //          - Bitmask
    //
    // RETURNS :
    //      - True: If it could set the value successfully
    //      - False: Otherwise
    //
    public static Boolean DB_UnsetFlag(BITMASK_TYPE Type, Int32 TerminalId, Int32 Flag, SqlTransaction Trx)
    {
      switch (Type)
      {
        case BITMASK_TYPE.Door:
          return DB_UnsetFlag("TS_DOOR_FLAGS", TerminalId, Flag, Trx);
        case BITMASK_TYPE.Bill:
          return DB_UnsetFlag("TS_BILL_FLAGS", TerminalId, Flag, Trx);
        case BITMASK_TYPE.Coin:
          return DB_UnsetFlag("TS_COIN_FLAGS", TerminalId, Flag, Trx);
        case BITMASK_TYPE.Printer:
          return DB_UnsetFlag("TS_PRINTER_FLAGS", TerminalId, Flag, Trx);
        case BITMASK_TYPE.EGM:
          return DB_UnsetFlag("TS_EGM_FLAGS", TerminalId, Flag, Trx);
        case BITMASK_TYPE.Plays:
          return DB_UnsetFlag("TS_PLAYED_WON_FLAGS", TerminalId, Flag, Trx);
        case BITMASK_TYPE.Jackpot:
          return DB_UnsetFlag("TS_JACKPOT_FLAGS", TerminalId, Flag, Trx);
        case BITMASK_TYPE.Machine_status:
          return DB_UnsetFlag("TS_MACHINE_FLAGS", TerminalId, Flag, Trx);
        default:
          return false;
      }
    } // DB_UnsetFlag

    // PURPOSE : Set value value in DB
    //
    //  PARAMS :
    //      - INPUT :
    //          - BITMASK_TYPE
    //          - TerminalId
    //          - Value
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: If it could set the value successfully
    //      - False: Otherwise
    //
    public static Boolean DB_SetValue(BITMASK_TYPE Type, Int32 TerminalId, Int32 Value, SqlTransaction Trx)
    {
      StringBuilder _sb;
      String _str_column;

      switch (Type)
      {
        case BITMASK_TYPE.Sas_host_error:
          _str_column = "TS_SAS_HOST_ERROR";
          break;  
        case BITMASK_TYPE.Call_attendant:
          _str_column = "TS_CALL_ATTENDANT_FLAGS";
          break;
        case BITMASK_TYPE.Stacker_status:
          _str_column = "TS_STACKER_STATUS";
          break;
        case BITMASK_TYPE.Machine_status:
          _str_column = "TS_MACHINE_FLAGS";
          break;
        case BITMASK_TYPE.Jackpot:
          _str_column = "TS_JACKPOT_FLAGS";
          break;
        case BITMASK_TYPE.Bill:
          _str_column = "TS_BILL_FLAGS";
          break;
        case BITMASK_TYPE.Plays:
          _str_column = "TS_PLAYED_WON_FLAGS";
          break;
        default:
          return false;
      }
      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" IF EXISTS (  SELECT   1  ");
        _sb.AppendLine("              FROM   TERMINAL_STATUS ");
        _sb.AppendLine("              WHERE   TS_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("           )  ");
        _sb.AppendLine("      UPDATE   TERMINAL_STATUS ");
        _sb.AppendLine("         SET   " + _str_column + " = @pValue ");
        _sb.AppendLine("       WHERE   TS_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("        ELSE  ");
        _sb.AppendLine(" INSERT INTO TERMINAL_STATUS ");
        _sb.AppendLine("            ( TS_TERMINAL_ID ");
        _sb.AppendLine("            , " + _str_column);
        _sb.AppendLine("            ) ");
        _sb.AppendLine("      VALUES ");
        _sb.AppendLine("            ( @pTerminalId ");
        _sb.AppendLine("            , @pValue ");
        _sb.AppendLine("            ) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pValue", SqlDbType.Int).Value = Value;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("DB_SetValue: Error setting " + _str_column + " status of Terminal: " + TerminalId);
            return false;
          }
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("DB_SetValue: Error setting " + _str_column + " status of Terminal: " + TerminalId);
        Log.Exception(_ex);

        return false;
      }
    } // DB_SetValue

    // PURPOSE : Set value value in DB
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - Value
    //          - Terminal Status Flag
    //          - AlarmId
    //          - SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: If it could set the value successfully
    //      - False: Otherwise
    //
    public static Boolean DB_SetValue(CustomAlarms.CustomAlarmsTypes AlarmType, Int32 TerminalId, Int32 Value, Int64 AlarmId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      String _str_column;
      String _str_column_alarm;

      switch (AlarmType)
      {
        case CustomAlarms.CustomAlarmsTypes.Payout:
          _str_column = "TS_PLAYED_WON_FLAGS";
          _str_column_alarm = "";
          break;

        case CustomAlarms.CustomAlarmsTypes.Jackpot:
          _str_column = "TS_JACKPOT_FLAGS";
          _str_column_alarm = "TS_JACKPOT_ALARM_ID";
          break;

        case CustomAlarms.CustomAlarmsTypes.CancelledCredits:
          _str_column = "TS_JACKPOT_FLAGS";
          _str_column_alarm = "TS_CANCELED_CREDIT_ALARM_ID";
          break;

        case CustomAlarms.CustomAlarmsTypes.Counterfeit:
          _str_column = "TS_BILL_FLAGS";
          _str_column_alarm = "TS_COUNTERFEIT_ALARM_ID";
          break;

        case CustomAlarms.CustomAlarmsTypes.Played:
          _str_column = "TS_PLAYED_WON_FLAGS";
          _str_column_alarm = "TS_PLAYED_ALARM_ID";
          break;

        case CustomAlarms.CustomAlarmsTypes.Won:
          _str_column = "TS_PLAYED_WON_FLAGS";
          _str_column_alarm = "TS_WON_ALARM_ID";
          break;

        case CustomAlarms.CustomAlarmsTypes.Highroller:
          _str_column = "TS_PLAYED_WON_FLAGS";
          _str_column_alarm = "TS_HIGHROLLER_ALARM_ID";
          break;

        case CustomAlarms.CustomAlarmsTypes.HighrollerAnonymous:
          _str_column = "TS_PLAYED_WON_FLAGS";
          _str_column_alarm = "TS_HIGHROLLER_ANONYMOUS_ALARM_ID";
          break;
        default:
          return false;
      }

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" IF EXISTS (  SELECT   1  ");
        _sb.AppendLine("              FROM   TERMINAL_STATUS ");
        _sb.AppendLine("              WHERE   TS_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("           )  ");
        _sb.AppendLine("      UPDATE   TERMINAL_STATUS ");
        _sb.AppendLine("         SET   " + _str_column + " = @pValue ");
        if (!String.IsNullOrEmpty(_str_column_alarm))
        {
          _sb.AppendLine("           , " + _str_column_alarm + " = @pValueAlarm ");
        }
        _sb.AppendLine("       WHERE   TS_TERMINAL_ID = @pTerminalId ");
        _sb.AppendLine("        ELSE  ");
        _sb.AppendLine(" INSERT INTO TERMINAL_STATUS ");
        _sb.AppendLine("            ( TS_TERMINAL_ID ");
        _sb.AppendLine("            , " + _str_column);
        if (!String.IsNullOrEmpty(_str_column_alarm))
        {
          _sb.AppendLine("            , " + _str_column_alarm);
        }
        _sb.AppendLine("            ) ");
        _sb.AppendLine("      VALUES ");
        _sb.AppendLine("            ( @pTerminalId ");
        _sb.AppendLine("            , @pValue ");
        if (!String.IsNullOrEmpty(_str_column_alarm))
        {
          _sb.AppendLine("            , @pValueAlarm ");
        }
        _sb.AppendLine("            ) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pValue", SqlDbType.Int).Value = Value;
          if (!String.IsNullOrEmpty(_str_column_alarm))
          {
            _cmd.Parameters.Add("@pValueAlarm", SqlDbType.BigInt).Value = AlarmId;
          }

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("DB_SetValue: Error setting " + _str_column + " status of Terminal: " + TerminalId);
            return false;
          }
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("DB_SetValue: Error setting " + _str_column + " status of Terminal: " + TerminalId);
        Log.Exception(_ex);

        return false;
      }
    } // DB_SetValue

    // PURPOSE : Check if a flag is set in bitmask
    //
    //  PARAMS :
    //      - INPUT :
    //          - Current bitmask
    //          - Flag
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: If the flag is activated
    //      - False: If the flag is not activated
    //
    public static Boolean IsFlagActived(Int32 CurrentBitmask, Int32 Flag)
    {
      return ((CurrentBitmask & Flag) == Flag);
    }

    // PURPOSE : Update a bitmask value
    //
    //  PARAMS :
    //      - INPUT :
    //          - Current bitmask
    //          - Flag
    //          - New flag status
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - New bitmask value
    //
    public static Int32 UpdateBitmask(Int32 CurrentBitmask, Int32 Flag, Boolean FlagStatus)
    {
      if (FlagStatus)
      {
        return (CurrentBitmask | Flag);
      }
      else
      {
        return (CurrentBitmask & (~Flag));
      }
    }

    // PURPOSE : Sets terminal status values in terminal_status table
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - TerminalEvent
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          - True: If it has been updated successfully.
    //          - False: Otherwise
    //
    public static Boolean SetTerminalStatus(Int32 TerminalId, WCP_TerminalEvent Event, SqlTransaction Trx)
    {
      String _column;
      Int32 _flag;

      _column = String.Empty;
      _flag = 0;

      switch (Event)
      {
        case WCP_TerminalEvent.BillIn:
          _flag += (Int32)TerminalStatusFlags.BILL_FLAG.HARD_FAILURE;
          _flag += (Int32)TerminalStatusFlags.BILL_FLAG.BILL_JAM;
          if (!DB_UnsetFlag(BITMASK_TYPE.Bill, TerminalId, _flag, Trx))
          {
            Log.Error("SetTerminalStatus: Error unsetting bitmask for WCP_TerminalEvent: " + Event);
            return false;
          }
          if (!IncreaseTerminalStacker(TerminalId, false, Trx))
          {
            return false;
          }
          break;

        case WCP_TerminalEvent.TicketOut:
          _flag += (Int32)TerminalStatusFlags.PRINTER_FLAG.COMM_ERROR;
          _flag += (Int32)TerminalStatusFlags.PRINTER_FLAG.CARRIAGE_JAM;
          _flag += (Int32)TerminalStatusFlags.PRINTER_FLAG.PAPER_OUT_ERROR;
          _flag += (Int32)TerminalStatusFlags.PRINTER_FLAG.REPLACE_RIBBON;
          if (!DB_UnsetFlag(BITMASK_TYPE.Printer, TerminalId, _flag, Trx))
          {
            Log.Error("SetTerminalStatus: Error unsetting bitmask for WCP_TerminalEvent: " + Event);
            return false;
          }
          break;

        case WCP_TerminalEvent.TicketIn:
          return IncreaseTerminalStacker(TerminalId, false, Trx);

        case WCP_TerminalEvent.ChangeStacker:
          return IncreaseTerminalStacker(TerminalId, true, Trx);

        case WCP_TerminalEvent.CoinsIn:
          _flag += (Int32)TerminalStatusFlags.COIN_FLAG.COIN_IN_TILT;
          _flag += (Int32)TerminalStatusFlags.COIN_FLAG.REVERSE_COIN;
          _flag += (Int32)TerminalStatusFlags.COIN_FLAG.COIN_IN_LOCKOUT;

          if (!DB_UnsetFlag(BITMASK_TYPE.Coin, TerminalId, _flag, Trx))
          {
            Log.Error("SetTerminalStatus: Error unsetting bitmask for WCP_TerminalEvent: " + Event);
            return false;
          }

          if (!IncreaseTerminalStacker(TerminalId, false, Trx))
          {
            return false;
          }
          break;

        default:
          Log.Error("SetTerminalStatus: Unknown WCP_TerminalEvent: " + Event);
          return false;

      } // switch

      return true;
    } // SetTerminalStatus

    // PURPOSE : Set Terminal Status.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - SasEventCode
    //          - SqlTransaction Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //          - True: If the status has been updated successfully.
    //          - False: Otherwise.
    //
    public static Boolean SetTerminalStatus(Int32 TerminalId, UInt16 SasEventCode, SqlTransaction Trx)
    {
      switch (SasEventCode)
      {
        // DOOR EVENTS
        case 0x0011: // Slot door was opened
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.Door, TerminalId, (Int32)TerminalStatusFlags.DOOR_FLAG.SLOT_DOOR, Trx);
        case 0x0013: // Drop door was opened
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.Door, TerminalId, (Int32)TerminalStatusFlags.DOOR_FLAG.DROP_DOOR, Trx);
        case 0x0015: // Card cage was opened
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.Door, TerminalId, (Int32)TerminalStatusFlags.DOOR_FLAG.CARD_CAGE, Trx);
        case 0x0019: // Cashbox door was opened
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.Door, TerminalId, (Int32)TerminalStatusFlags.DOOR_FLAG.CASHBOX_DOOR, Trx);
        case 0x001D: // Belly door was opened
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.Door, TerminalId, (Int32)TerminalStatusFlags.DOOR_FLAG.BELLY_DOOR, Trx);
        case 0x0012: // Slot door was closed        
          return TerminalStatusFlags.DB_UnsetFlag(BITMASK_TYPE.Door, TerminalId, (Int32)TerminalStatusFlags.DOOR_FLAG.SLOT_DOOR, Trx);
        case 0x0014: // Drop door was closed
          return TerminalStatusFlags.DB_UnsetFlag(BITMASK_TYPE.Door, TerminalId, (Int32)TerminalStatusFlags.DOOR_FLAG.DROP_DOOR, Trx);
        case 0x0016: // Card cage was closed
          return TerminalStatusFlags.DB_UnsetFlag(BITMASK_TYPE.Door, TerminalId, (Int32)TerminalStatusFlags.DOOR_FLAG.CARD_CAGE, Trx);
        case 0x001A: // Cashbox door was closed
          return TerminalStatusFlags.DB_UnsetFlag(BITMASK_TYPE.Door, TerminalId, (Int32)TerminalStatusFlags.DOOR_FLAG.CASHBOX_DOOR, Trx);
        case 0x001E: // Belly door was closed
          return TerminalStatusFlags.DB_UnsetFlag(BITMASK_TYPE.Door, TerminalId, (Int32)TerminalStatusFlags.DOOR_FLAG.BELLY_DOOR, Trx);

        // BILL EVENTS
        case 0x0028: // Bill jam
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.Bill, TerminalId, (Int32)TerminalStatusFlags.BILL_FLAG.BILL_JAM, Trx);
        case 0x0029: // Bill acceptor hardware failure          
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.Bill, TerminalId, (Int32)TerminalStatusFlags.BILL_FLAG.HARD_FAILURE, Trx);

        // COIN EVENTS
        case 0x0021: // Coin in tilt
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.Coin, TerminalId, (Int32)TerminalStatusFlags.COIN_FLAG.COIN_IN_TILT, Trx);
        case 0x002D: // Reverse coin in detected
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.Coin, TerminalId, (Int32)TerminalStatusFlags.COIN_FLAG.REVERSE_COIN, Trx);
        case 0x0079: // Coin in lockout malfunction
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.Coin, TerminalId, (Int32)TerminalStatusFlags.COIN_FLAG.COIN_IN_LOCKOUT, Trx);

        // PRINTER EVENTS
        case 0x0060: // Printer communication error
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.Printer, TerminalId, (Int32)TerminalStatusFlags.PRINTER_FLAG.COMM_ERROR, Trx);
        case 0x0061: // Printer paper out error
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.Printer, TerminalId, (Int32)TerminalStatusFlags.PRINTER_FLAG.PAPER_OUT_ERROR, Trx);
        case 0x0075: // Printer power off
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.Printer, TerminalId, (Int32)TerminalStatusFlags.PRINTER_FLAG.POWER_ERROR, Trx);
        case 0x0078: // Printer carriage jammed
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.Printer, TerminalId, (Int32)TerminalStatusFlags.PRINTER_FLAG.CARRIAGE_JAM, Trx);
        case 0x0076: // Printer power on
          return TerminalStatusFlags.DB_UnsetFlag(BITMASK_TYPE.Printer, TerminalId, (Int32)TerminalStatusFlags.PRINTER_FLAG.POWER_ERROR, Trx);
        case 0x0074: // Printer paper low
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.Printer, TerminalId, (Int32)TerminalStatusFlags.PRINTER_FLAG.PAPER_LOW, Trx);
        case 0x0077: // Printer preplace printer ribbon
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.Printer, TerminalId, (Int32)TerminalStatusFlags.PRINTER_FLAG.REPLACE_RIBBON, Trx);

        case 0x0101: // SAS disconnected
          return TerminalStatusFlags.DB_SetValue(BITMASK_TYPE.Sas_host_error, TerminalId, 1, Trx);
        case 0x0102: // SAS connected
          return TerminalStatusFlags.DB_SetValue(BITMASK_TYPE.Sas_host_error, TerminalId, 0, Trx);

        // EGM EVENTS
        case 0x0031: // CMOS RAM error (data recovered from EEPROM)
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.EGM, TerminalId, (Int32)TerminalStatusFlags.EGM_FLAG.CMOS_RAM_DATA_RECOVERED, Trx);
        case 0x0032: // CMOS RAM error (no data recovered from EEPROM)
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.EGM, TerminalId, (Int32)TerminalStatusFlags.EGM_FLAG.CMOS_RAM_NO_DATA_RECOVERED, Trx);
        case 0x0033: // CMOS RAM error (bad device)
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.EGM, TerminalId, (Int32)TerminalStatusFlags.EGM_FLAG.CMOS_RAM_BAD_DEVICE, Trx);
        case 0x0034: // EEPROM error (data error)
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.EGM, TerminalId, (Int32)TerminalStatusFlags.EGM_FLAG.EPROM_DATA_ERROR, Trx);
        case 0x0035: // EEPROM error (bad device)
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.EGM, TerminalId, (Int32)TerminalStatusFlags.EGM_FLAG.EPROM_BAD_DEVICE, Trx);
        case 0x0036: // EPROM error (different checksum - version changed)
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.EGM, TerminalId, (Int32)TerminalStatusFlags.EGM_FLAG.EPROM_DIFF_CHECKSUM, Trx);
        case 0x0037: // EPROM error (bad checksum compare)
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.EGM, TerminalId, (Int32)TerminalStatusFlags.EGM_FLAG.EPROM_BAD_CHECKSUM, Trx);
        case 0x0038: // Partitioned EPROM error (checksum - versoin changed)
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.EGM, TerminalId, (Int32)TerminalStatusFlags.EGM_FLAG.PART_EPROM_DIFF_CHECKSUM, Trx);
        case 0x0039: // Partitioned EPROM error (bad checksum compare)
          return TerminalStatusFlags.DB_SetFlag(BITMASK_TYPE.EGM, TerminalId, (Int32)TerminalStatusFlags.EGM_FLAG.PART_EPROM_BAD_CHECKSUM, Trx);

        // CALL ATTENDANT EVENTS
        case 0x0071:   // Change Lamp On
          return TerminalStatusFlags.DB_SetValue(BITMASK_TYPE.Call_attendant, TerminalId, 1, Trx);
        case 0x0072:   // Change Lamp Off
          return TerminalStatusFlags.DB_SetValue(BITMASK_TYPE.Call_attendant, TerminalId, 0, Trx);

        // STACKER EVENTS
        case 0x002E:   // CASHBOX near full detected
          return TerminalStatusFlags.DB_SetValue(BITMASK_TYPE.Stacker_status, TerminalId, (Int32)STACKER_STATUS.ALMOST_FULL, Trx);
        case 0x0027:   // CASHBOX full detected
          return TerminalStatusFlags.DB_SetValue(BITMASK_TYPE.Stacker_status, TerminalId, (Int32)STACKER_STATUS.FULL, Trx);

        default:
          Log.Error("SetTerminalStatus: Unknown SasEventCode: " + SasEventCode.ToString("X"));
          return false;
      } // end switch

    } // SetTerminalStatus

    // PURPOSE : Set Block Status (terminal_status table) for All Terminals of the Site.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32 Flag
    //          - Boolean SetFlag
    //          - SqlTransaction Trx
    //      - OUTPUT :
    //
    // RETURNS :
    //          - True: If the status has been updated successfully.
    //          - False: Otherwise.
    //
    public static Boolean SetAllTerminalsBlockStatus(Int32 Flag, Boolean SetFlag, SqlTransaction Trx)
    {
      StringBuilder _sb;
      try
      {
        _sb = new StringBuilder();
        
        //First: Update Terminals has data on TERMINAL_STATUS table'
        _sb.AppendLine("    UPDATE  TERMINAL_STATUS                                                           ");
        if (SetFlag)
        {
          _sb.AppendLine("       SET  TS_MACHINE_FLAGS = TS_MACHINE_FLAGS | @pFlag                            ");
        }
        else
        {
          _sb.AppendLine("       SET  TS_MACHINE_FLAGS = TS_MACHINE_FLAGS & (~@pFlag)                         ");
        }
        _sb.AppendLine("      FROM  TERMINALS                                                                 ");
        _sb.AppendLine("INNER JOIN  TERMINAL_STATUS                                                           ");
        _sb.AppendLine("        ON  TE_TERMINAL_ID = TS_TERMINAL_ID                                           ");

        //Second: Insert into TERMINAL_STATUS from Terminals has'nt data on TERMINAL_STATUS table'
        _sb.AppendLine("    INSERT INTO   TERMINAL_STATUS (TS_TERMINAL_ID, TS_MACHINE_FLAGS)                  ");
        _sb.AppendLine("         SELECT   TE_TERMINAL_ID, @pFlag                                              ");
        _sb.AppendLine("           FROM   TERMINALS                                                           ");
        _sb.AppendLine("          WHERE   TE_TERMINAL_ID NOT IN (SELECT TS_TERMINAL_ID FROM TERMINAL_STATUS)  ");
        
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pFlag", SqlDbType.Int).Value = Flag;

          _cmd.ExecuteNonQuery();
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("SetAllTerminalsStatus: Error.");
        Log.Exception(_ex);

        return false;
      }

    } // SetAllTerminalsBlockStatus
    #endregion
  }
}
