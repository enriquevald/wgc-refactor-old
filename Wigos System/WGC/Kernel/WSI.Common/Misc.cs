//------------------------------------------------------------------------------
// Copyright � 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Misc.cs
// 
//   DESCRIPTION: Miscellaneous utilities for WCP and WC2 service
// 
//        AUTHOR: Agust� Poch (Header only)
// 
// CREATION DATE: 28-APR-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-APR-2009 APB    First release (Header).
// 23-MAR-2012 RCI    Allow to check license by only valid date (routine CheckLicense).
// 07-JUN-2012 JCM    Add filter function to filter holder with birth today 
//                    Add filter function to filter holders who accomplish a promotion level
// 11-JUN-2012 DDM    Modified the function BirthDayFilter
// 20-JUN-2012 SSC    Added GetTerminalName function.
// 02-JUL-2012 ACC    Added Peru External Events.
// 07-SEP-2012 DDM    Added functions WCPTerminalTypes, WCPTerminalTypes and AllTerminalTypes.
// 15-MAY-2013 HBB    The function IsLevelAllowed was brought from Promotion.cs to make it accesible from other sites
// 23-MAY-2013 HBB    Added the function IsGenderAllowed
// 22-JUL-2013 LEM    Created DateSQLFilter to build where sentence using dates from, to.
// 20-NOV-2013 ANG    Add method to sort DataTable columns by column name lists.
// 26-NOV-2013 JRM    Added method to fetch allowRedemption from terminal. If it is null, it is fetched from general params
// 28-NOV-2013 JML    Added function: InstallExternalComponent()
// 29-NOV-2013 NMR    Loading more terminal parameters
// 05-DIC-2013 NMR    Amount won't be converted to cents
// 09-JAN-2014 DHA    Added method to get System Users
// 14-APR-2014 DLL    Added method to filter VIP Account
// 18-MAY-2015 YNM    Added method to get GP 'Terminal.UseStackerId' - Bug WIG-2362
// 18-MAY-2015 JML    Multisite-Multicurrency permisions (add funtions: IsPartOfAMultisiteGroup(), IsMultiCurrency()
// 17-JUL-2015 YNM    Fixed Bug WIG-2330: Promobox collection error 
// 19-OCT-2015 ETP    Product Backlog Item 4690 reemplaced TITO.TITOMode for Site.SystemMode.
// 17-NOV-2015 DLL    Backlog Item 6505: Apply tax to all withdrawal. Without Devolution.
// 04-DEC-2015 JCA    Task 7370:Crear rutina: IsGameGatewayEnabled
// 04-JAN-2016 DDS    Fixed Bug 7795: Added check 'AntiMoneyLaundering.Enabled'
// 17-NOV-2015  DLL   Backlog Item 6505: Apply tax to all withdrawal. Without Devolution.
// 05-JAN-2016 DDS    Addem method 'IsFloorDualCurrencyEnabled' to encapsulate behavior
// 13-JAN-2016 YNM    Bug 8346:Error on Visits Report Add Function IsReceptionEnabled
// 09-FEB-2015 JBC    PBI 7147: SmartFloor license. Now the system allow to check license features.
// 19-FEB-2016  ETP    Product Backlog Item 9752: Dual Currency: Configurar montos transferencia cr�dito NR
// 17-NOV-2015  DLL    Backlog Item 6505: Apply tax to all withdrawal. Without Devolution.
// 01-MAR-2016 DHA    Product Backlog Item 10083: Winpot - Provisioning Tax
// 13-APR-2016 RGR    Product Backlog Item 11295: Codere - Custody Tax: Implement tax
// 19-APR-2016  RGR   Product Backlog Item 11297: Codere - Tax Custody: Reports and cash movements and account
// 21-APR-2016 LTC    Product Backlog Item 11296:Codere - Tax Custody: Redeem Tax Implementation
// 11-MAY-2016 LTC    Product Backlog Item 13097:UNPLANNED - Diferent Issues Winions - Mex Sprint 24
// 11-APR-2016 EOR    Bug 11221 Handpays balance report error in the filters
// 02-MAY-2016 ETP    Product Backlog Item 12652: Get GP value for MultiCurrencyExchange.
// 15-JUN-2016 ETP    Product Backlog Item 14340: Get GP value for PinPadEnabled.
// 01-AUG-2016 RAB    Product Backlog Item 15614: Get GP value for BankCard.Undo.Mode.
// 05-AGO-2016 LTC    Product Backlog Item 16198:TPV Televisa: Closing Cash
// 02-AUG-2016 EOR    Product Backlog Item 16149: Codere - Tax Custody: Add tax custody concept in ticket recharge
// 18-AGO-2016 FJC    Bug 16841:CountR: La sesi�n de CountR se muestra en las sesiones de caja abierta
// 06-SEP-2016 LTC    Bug 17354: Wrong information is shown in cashier resume about vouchers
// 06-SEP-2016 ETP    Fixed Bug 17128: Error enable PinPad
// 27-SEP-2016 ESE    Bug 17833: Televisa voucher, status cash
// 04-OCT-2016 EOR    Product Backlog Item 15335: TPV Televisa: Cash Summary
// 04-OCT-2016 RGR    PBI 17447: Televisa - Draw Cash (Cash prize) - Recharge Operation
// 04-OCT-2016 JBP    PBI 18464:PariPlay - Ajustar pantallas de BonoPlay
// 07-OCT-2016 LTC    PBI 17960: Televisa - Draw Cash for gaming tables - Configuration
// 11-OCT-2016 ESE    Bug 18892: Always regiters COM dll for PinPad
// 04-OCT-2016 EOR    Product Backlog Item 15335: TPV Televisa: Cash Summary
// 04-OCT-2016 RGR    PBI 17447: Televisa - Draw Cash (Cash prize) - Recharge Operation
// 07-OCT-2016 LTC    PBI 17960: Televisa - Draw Cash for gaming tables - Configuration
// 11-NOV-2016 FGB    Bug 20222: Exped: Connection with WCP server
// 22-NOV-2016 JBP    PBI 20566:Cage Str Team 1 - Cambios Review Sprint 32
// 24-NOV-2016 EOR    PBI 19870: R.Franco: Nuevo reporte de m�quinas y juegos
// 19-DEC-2016 ETP    Bug 21793: Incorrect client data needed.
// 11-JAN-2016  ETP   Fixed Bug 22543 Prize_coupon not aplicable if cashdesk enabled.
// 16-FEB-2017 ATB    PBI 24870: Add logging system to the shutdowns to control it
// 13-FEB-2017 JML    Fixed Bug 24422: Cashier: Incorrect withdrawal and closing with bank card
// 24-FEB-2017 FAV    Bug 25131: The Cashier memory is growing
// 13-MAR-2016 ATB    PBI 25262: Exped - Payment authorisation
// 15-MAR-2017 FAV    PBI 25262: Exped - Payment authorisation
// 27-MAR-2017 XGJ    PBI 26247:Sorteo de m�quina - Habilitar / Deshablitar la funcionalidad de sorteo de m�quina
// 05-APR-2017 JML    Bug 26568:Custodia - add new general param to configure tax custody for gaming tables operations
// 11-JAN-2016 ETP    Fixed Bug 22543 Prize_coupon not aplicable if cashdesk enabled.
// 13-JUN-2017 RGR    Fixed Bug 28010: WIGOS-2576 Cashier duplicate session allowed
// 22-JUN-2017 FJC    WIGOS-3142 WS2S - Set / Retrieve Reserved Credit on the Cashier
// 12-JUL-2017 LTC    Bug 28623:WIGOS-3453 Error "resumen de caja" "retiros con tarjeta bancaria" ID7 is not displaying
// 18-JUL-2017 ETP    WIGOS-3515 New Jackpots option menu appears in wrong place and with wrong description.
// 24-JUL-2017 ATB    PBI 28710: EGM Reserve � Settings
// 19-SEP-2017 ETP    PBI 29621: AGG Data transfer [WIGOS-4458]
// 29-OCT-2017 LTC    [WIGOS-6134] Multisite Terminal meter adjustment
// 23-OCT-2017  MS    PBI 29628:FBM Filipinas
// 04-DIC-2017 FJC    WIGOS-6985 Terminal Draw when "Loser" type does not add the "Courtesy prize" to player account (TFS: Bug 31036:WIGOS-6985 Terminal Draw when "Loser" type does not add the "Courtesy prize" to player account)
// 15-FEB-2018 DHA    Bug 31537:WIGOS-8103 [Ticket #11369] Sessiones de sorteos - Sorteo Perdedor
// 21-FEB-2018 DHA    Bug 31619:WIGOS-8109 [Ticket #12335] Custodia
// 07-FEB-2018 AGS    Fixed Bug 31619:WIGOS-8109 [Ticket #12335] Custodia
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Net;
using System.Runtime.InteropServices;
using System.Management;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Data.SqlClient;
using System.IO.Compression;
using System.Diagnostics;
using System.Data.SqlTypes;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.Win32;
using WSI.Common.Entities.General;
using WSI.Common.PinPad;
using WSI.Common.ExternalPaymentAndSaleValidation;
using System.Globalization;
using System.ComponentModel;
using System.Xml.Serialization;

namespace WSI.Common
{

  public class GetInfo
  {

    public string GetHardDriveSerial()
    {
      //ManagementScope x;
      ConnectionOptions opt;

      opt = new ConnectionOptions();

      opt.Username = @"DPIDC\Administrator";
      opt.Password = "stocking";


      ManagementScope scope =
            new ManagementScope(
            "\\\\CARLES\\root\\cimv2", opt);
      scope.Connect();

      //x = new ManagementScope ("RONALD");

      ManagementObjectSearcher searcher;

      searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMedia");
      searcher = new ManagementObjectSearcher("SELECT * FROM win32_logicaldisk");
      searcher.Scope = scope;


      foreach (ManagementObject wmi_HD in searcher.Get())
      {
        if (wmi_HD["VolumeSerialNumber"] != null)
        {
          return wmi_HD["VolumeSerialNumber"].ToString();
        }
        //if ( wmi_HD["SerialNumber"] != null )
        //{
        //  return wmi_HD["SerialNumber"].ToString ();
        //}

      }

      return "";

    }

    /// <summary>
    /// return Volume Serial Number from hard drive
    /// </summary>
    /// <param name="strDriveLetter">[optional] Drive letter</param>
    /// <returns>[string] VolumeSerialNumber</returns>
    public string GetVolumeSerial(string strDriveLetter)
    {
      if (strDriveLetter == "" || strDriveLetter == null) strDriveLetter = "C";
      ManagementObject disk =
          new ManagementObject("win32_logicaldisk.deviceid=\"" + strDriveLetter + ":\"");
      disk.Get();
      return disk["VolumeSerialNumber"].ToString();
    }

    /// <summary>
    /// Returns MAC Address from first Network Card in Computer
    /// </summary>
    /// <returns>[string] MAC Address</returns>
    public string GetMACAddress()
    {
      ManagementObjectSearcher _obj_query = null;
      ManagementObjectCollection _query_collection = null;
      String _mac_address;

      try
      {
        _obj_query = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration");
        _query_collection = _obj_query.Get();
        foreach (ManagementObject _m_object in _query_collection)
        {
          if (_m_object["MacAddress"] == null)
          {
            continue;
          }

          if (_m_object["IPAddress"] == null)
          {
            continue;
          }

          if (!((Boolean)_m_object["IPEnabled"]))
          {
            continue;
          }

          if (_m_object["MacAddress"] != null)
          {
            _mac_address = _m_object["MacAddress"].ToString().Replace(":", "-");

            return _mac_address;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        Log.Error("GetMacAddress: not found.");
      }

      return "";
    }

    /// <summary>
    /// Check if @mac is present
    /// </summary>
    /// <param name="MacToCheck"></param>
    /// <returns></returns>
    public Boolean IsMACAddressPresent(string MacToCheck)
    {
      ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
      ManagementObjectCollection moc = mc.GetInstances();

      foreach (ManagementObject mo in moc)
      {
        try
        {
          if (mo["MacAddress"].ToString().Replace(":", "-") == MacToCheck)
          {
            return true;
          }
        }
        catch
        {
        }
      }

      return false;
    }

    /// <summary>
    /// Return processorId from first CPU in machine
    /// </summary>
    /// <returns>[string] ProcessorId</returns>
    public string GetCPUId()
    {
      string cpuInfo = String.Empty;
      string temp = String.Empty;
      ManagementClass mc = new ManagementClass("Win32_Processor");
      ManagementObjectCollection moc = mc.GetInstances();
      foreach (ManagementObject mo in moc)
      {
        if (cpuInfo == String.Empty)
        {// only return cpuInfo from first CPU
          cpuInfo = mo.Properties["ProcessorId"].Value.ToString();
        }
      }
      return cpuInfo;
    }
  }


  static public class Performance
  {
    private static Stopwatch m_stop_watch = Stopwatch.StartNew();

    public static TimeSpan GetTickCount()
    {
      return m_stop_watch.Elapsed;
    }

    public static TimeSpan GetElapsedTicks(TimeSpan FirstSample)
    {
      return GetElapsedTicks(FirstSample, m_stop_watch.Elapsed);
    }

    public static TimeSpan GetElapsedTicks(TimeSpan FirstSample, TimeSpan LastSample)
    {
      ulong _t0;
      ulong _t1;
      ulong _elapsed;

      _t0 = (ulong)(FirstSample.Ticks);
      _t1 = (ulong)(LastSample.Ticks);
      _elapsed = _t1;
      if (_t1 < _t0)
      {
        _elapsed += ulong.MaxValue;
        _elapsed += 1;
      }
      _elapsed -= _t0;

      if (_elapsed > long.MaxValue)
      {
        _elapsed = long.MaxValue;
      }

      return new TimeSpan((long)_elapsed);
    }
  }

  static public class PromoBOX
  {
    // Must match with wkt_functionalities
    public enum Functionality
    {
      ALL = 0,
      DRAWS = 1,
      PROMOTIONS = 2,
      ADVERTISEMENTS = 3,
      ACTIVITY_ONLY_POINTS = 4,
      SCHEDULE_GIFT_REQUEST = 5,
      SCHEDULE_RECHARGES = 6,
      PRINT_RECHARGES_TICKET = 7,
      SCHEDULE_DRAW_NUMBERS = 8,
      DRAW_NUMBERS = 9,
      SCHEDULE_PROMOTIONS = 10
    }

    /// <summary>
    /// Obtains the list of PromoBOX functionalities
    /// </summary>
    /// <returns>List of PromoBOX functionalities</returns>
    public static List<PromoBOX.Functionality> ReadPromoBoxFunc(PromoBOX.Functionality WhichOne,
                                                                 Boolean OnlyEnabled,
                                                                 SqlTransaction SqlTrx)
    {
      List<PromoBOX.Functionality> _list;
      StringBuilder _str;
      String _str_where;

      _list = new List<PromoBOX.Functionality>();

      _str = new StringBuilder();
      _str.AppendLine(" SELECT FUN_FUNCTION_ID");
      _str.AppendLine("   FROM WKT_FUNCTIONALITIES");

      _str_where = "";

      if (WhichOne != Functionality.ALL)
      {
        _str_where += (_str_where == "") ? " WHERE" : " AND";
        _str_where += " FUN_FUNCTION_ID = " + Convert.ToInt32(WhichOne);
      }

      if (OnlyEnabled)
      {
        _str_where += (_str_where == "") ? " WHERE" : " AND";
        _str_where += " FUN_ENABLED = 1";
      }

      _str.AppendLine(_str_where);

      using (SqlCommand _sql_cmd = new SqlCommand(_str.ToString(), SqlTrx.Connection, SqlTrx))
      {
        using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
        {
          while (_reader.Read())
          {
            _list.Add((PromoBOX.Functionality)_reader[0]);
          }
        }
      }

      return _list;
    } // ReadPromoBoxFunc
  } // PromoBOX


  static public class Misc
  {
    /// <summary>
    ///  Stamp DateTime on Photo
    /// </summary>
    /// <param name="ImageStamp"></param>
    /// <param name="DtStamp"></param>
    /// <param name="Format"></param>
    public static void StampDateOnImage(Image ImageStamp, DateTime DtStamp, string Format)
    {
      string _stamp_string;
      Graphics _graphics;
      GraphicsPath _graphics_path;
      Matrix _translate_matrix;
      Matrix _translate_matrix2;

      try
      {
        if (!((ImageStamp.Width == 640 || ImageStamp.Width == 320) &&
            (ImageStamp.Height == 480 || ImageStamp.Height == 240)))
          return;

        _graphics_path = new GraphicsPath();
        _stamp_string = !string.IsNullOrEmpty(Format) ? DtStamp.ToString(Format) : DtStamp.ToString();
        _graphics = Graphics.FromImage(ImageStamp);
        _translate_matrix = new Matrix();
        _translate_matrix2 = new Matrix();

        _graphics_path.AddString(_stamp_string.Trim(), FontFamily.GenericSansSerif,
                                   (int)FontStyle.Bold, _graphics.DpiY * 60f / 72f,
                                  new Point(320, 0), new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center });

        _translate_matrix.Scale(ImageStamp.Width / 640f, ImageStamp.Height / 480f);
        _graphics_path.Transform(_translate_matrix);
        _graphics_path.Flatten();
        _translate_matrix2.Translate(0,
          ImageStamp.Height - (_graphics_path.GetBounds().Size.Height));
        _graphics_path.Transform(_translate_matrix2);

        _graphics.SmoothingMode = SmoothingMode.AntiAlias;
        _graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
        _graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
        _graphics.DrawPath(new Pen(Color.Black, 5), _graphics_path);
        _graphics.FillPath(new SolidBrush(Color.White), _graphics_path);
        //        _graphics.DrawString(_stamp_string, new Font("Tahoma",32), Brushes.Black, rectf);

        _graphics.Flush();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    public static String GetDateTimeCurrentCulture(DateTime DateTime)
    {
      CultureInfo _culture_info;
      String _date_format;

      _culture_info = new CultureInfo(CultureInfo.CurrentCulture.Name);
      _date_format = _culture_info.DateTimeFormat.ShortDatePattern;

      return DateTime.ToString(_date_format + " HH:mm:ss");
    }
    // <summary>
    /// 
    /// </summary>
    /// <param name="HolderBirthdayDate"></param>
    /// <returns></returns>
    public static HolderHappyBirthDayNotification GetHolderBirthDayNotification(Int64 AccountId, String HolderName, DateTime? HolderBirthdayDate)
    {
      DateTime _now_date;
      TimeSpan _tspam;
      DateTime _birth_date;

      // FJC 11-MAY-2016: IMPORTANT!!!!!! If make changes in this function, we have to make changes in UcCard.cs (//RCI 19-AUG-2010: Happy Birthday!) 

      //JBC 30-01-2015 BirthDay alarm feature
      if (!Alarm.CheckBirthdayAlarm(AccountId, HolderName,
        CommonCashierInformation.CashierSessionInfo().TerminalId + " - " + CommonCashierInformation.CashierSessionInfo().TerminalName, TerminalTypes.UNKNOWN))
      {
        Log.Error("GetHolderBirthDayNotification(): Error checking Birthday Alarm.");
      }

      _now_date = new DateTime(WGDB.Now.Year, WGDB.Now.Month, WGDB.Now.Day);
      _birth_date = HolderBirthdayDate ?? default(DateTime);

      // MBF 26-OCT-2010 - People born in 29 of Feb have birthdays too.
      if (_birth_date.Month == 2
        && _birth_date.Day == 29
        && !DateTime.IsLeapYear(WGDB.Now.Year))
      {
        _birth_date = new DateTime(WGDB.Now.Year, _birth_date.Month, _birth_date.AddDays(-1).Day);
      }
      else
      {
        _birth_date = new DateTime(WGDB.Now.Year, _birth_date.Month, _birth_date.Day);
      }

      _tspam = (_birth_date - _now_date);

      if (_tspam.TotalDays > 0 && _tspam.TotalDays <= GeneralParam.GetInt32("Accounts", "Player.BirthdayWarningDays", 7))
      {
        return HolderHappyBirthDayNotification.HAPPY_BIRTHDAY_INCOMMING;
      }

      if (_tspam.TotalDays == 0)
      {
        return HolderHappyBirthDayNotification.HAPPY_BIRTHDAY_TODAY;
      }

      return HolderHappyBirthDayNotification.NONE;
    }

    private const String LOCAL_IP_CONF_SETTING = "LocalIP";
    private const String EMPTY_IP = "0.0.0.0";

    //----------------------------------------------------------------------------
    // PURPOSE: Get NLS strings for Gender values
    //
    // PARAMS:
    //   - INPUT: Value = 1 - Male | 2 - Female
    //   - OUTPUT: None
    //
    // RETURNS:
    //     - NLS String
    //
    public static String GenderString(Int32 Value)
    {
      switch (Value)
      {
        case 1:
          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_MALE");

        case 2:
          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_FEMALE");

        default:
          return "";
      }
    }

    //----------------------------------------------------------------------------
    // PURPOSE: Get NLS strings for ACCOUNT_MARITAL_STATUS values
    //
    // PARAMS:
    //   - INPUT: ACCOUNT_MARITAL_STATUS
    //   - OUTPUT: None
    //
    // RETURNS:
    //     - NLS String
    //
    public static String MaritalStatusString(ACCOUNT_MARITAL_STATUS Value)
    {
      switch (Value)
      {
        case ACCOUNT_MARITAL_STATUS.SINGLE:
          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_SINGLE");
        case ACCOUNT_MARITAL_STATUS.MARRIED:
          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_MARRIED");
        case ACCOUNT_MARITAL_STATUS.DIVORCED:
          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_DIVORCED");
        case ACCOUNT_MARITAL_STATUS.WIDOWED:
          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_WIDOWED");
        case ACCOUNT_MARITAL_STATUS.UNMARRIED:
          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_UNMARRIED");
        case ACCOUNT_MARITAL_STATUS.OTHER:
          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_OTHER");
        case ACCOUNT_MARITAL_STATUS.UNSPECIFIED:
          return Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_UNSPECIFIED");
        default:
          return "";
      }
    }

    // LTC  29-OCT-2017
    public static Boolean IsMultisiteCenter()
    {
      return GeneralParam.GetBoolean("MultiSite", "IsCenter", false);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get enabled value for GameGateway (BonoPlay)
    //
    // RETURNS:
    //      - Boolean: GameGateway enabled.
    //
    public static Boolean IsGameGatewayMenuEnabled()
    {
      // DHA 12-NOV-2014 Added Not TITO validation
      // JBP 04-OCT-2016 Added PariPlay validatin
      return (IsGameGatewayEnabled() || IsPariPlayEnabled());
    } // IsGameGatewayMenuEnabled

    //------------------------------------------------------------------------------
    // PURPOSE: Get enabled value for GameGateway (BonoPLUS)
    //
    // RETURNS:
    //      - Boolean: GameGateway enabled.
    //
    public static Boolean IsGameGatewayEnabled()
    {
      // DHA 12-NOV-2014 Added Not TITO validation
      return (GeneralParam.GetBoolean("GameGateway", "Enabled", false));
    } // IsGameGatewayEnabled

    //------------------------------------------------------------------------------
    // PURPOSE: Get enabled value for PariPlay
    //
    // RETURNS:
    //      - Boolean: PariPlay enabled.
    //
    public static Boolean IsPariPlayEnabled()
    {
      // JBP 04-OCT-2016 Added PariPlay validatin
      return (GeneralParam.GetBoolean("PariPlay", "Enabled", false));
    } // IsPariPlayEnabled

    //------------------------------------------------------------------------------
    // PURPOSE: Get enabled value for ColJuegos
    //
    // RETURNS:
    //      - Boolean: ColJuegos enabled.
    //
    public static Boolean IsColJuegosEnabled()
    {
      // DHA 12-NOV-2014 Added Not TITO validation
      return GeneralParam.GetBoolean("Interface.Coljuegos", "Enabled", false);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get an Object (Int32 / Datetime) with value of Grace Period
    //
    // RETURNS:
    //      - Object: Null/Int32/Datetime
    //
    public static Object GetTITOPaymentGracePeriod()
    {
      String _str_gp_date_mmdd;
      DateTime _grace_period_date;

      try
      {
        switch (GeneralParam.GetInt32("TITO", "Payment.GracePeriod.Mode"))
        {
          case 1:
            return GeneralParam.GetInt32("TITO", "Payment.GracePeriod.Days");

          case 2:
            _str_gp_date_mmdd = GeneralParam.GetString("TITO", "Payment.GracePeriod.ExclusiveDateMMDD");

            if (_str_gp_date_mmdd.Length != 4)
            {
              throw new Exception("Incorrect format in GP TITO.Payment.GracePeriod.ExclusiveDateMMDD. Current value: " + _str_gp_date_mmdd);
            }

              if (!DateTime.TryParseExact(_str_gp_date_mmdd.Insert(2, "/"), "MM/dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out _grace_period_date))
              {
                throw new Exception("Incorrect format in GP TITO.Payment.GracePeriod.ExclusiveDateMMDD. Current value: " + _str_gp_date_mmdd);
              }

            return _grace_period_date;

          default:
            break;
        }

        return null;
      }
      catch (Exception _ex)
      {
        Log.Message(_ex.Message);
      }

      return null;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get enabled value for Note Acceptor
    //
    // RETURNS:
    //      - Boolean: NoteAcceptor enabled.
    //
    public static Boolean IsNoteAcceptorEnabled()
    {
      // DHA 12-NOV-2014 Added Not TITO validation
      return GeneralParam.GetBoolean("NoteAcceptor", "Enabled") && Cage.IsCageEnabled() && !TITO.Utils.IsTitoMode();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get enabled value for Gaming Day General Param
    //
    // RETURNS:
    //      - Boolean: GamingDay enabled.
    //
    public static Boolean IsGamingDayEnabled()
    {
      return GeneralParam.GetBoolean("GamingDay", "Enabled") && Cage.IsCageEnabled() && !Cage.IsCageAutoMode();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get enabled value when Amount Player Card goes to company B
    //
    // RETURNS:
    //      - Boolean: Amount To Company B enabled.
    //
    public static Boolean IsCardPlayerToCompanyB()
    {
      return GeneralParam.GetBoolean("Cashier", "Split.B.Enabled") && GeneralParam.GetBoolean("Cashier.PlayerCard", "AmountToCompanyB");
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get name value for concept Player Card
    //
    // RETURNS:
    //      - String: Player Card Name
    //
    public static String GetCardPlayerConceptName()
    {
      return GeneralParam.GetString("Cashier.PlayerCard", "ConceptName", Resource.String("STR_FORMAT_GENERAL_NAME_CARD"));
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get value for Terminal.UseStarckerID
    //
    // RETURNS:
    //      - Boolean: UseStackerId value.
    //
    public static Boolean UseStackerId()
    {
      return GeneralParam.GetBoolean("WCP", "Terminal.UseStackerId") && TITO.Utils.IsTitoMode();
    }


    //------------------------------------------------------------------------------
    // PURPOSE: check if withdraw always price
    //
    // RETURNS:
    //      - Boolean: NoteAcceptor enabled.
    //
    public static Boolean PrizeComputationWithoutDevolution()
    {
      return ((PrizeComputationMode)GeneralParam.GetInt32("Cashier", "PrizeComputationMode") == PrizeComputationMode.WithoutDevolution);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: check if Provisioning Tax is enabled
    //
    // RETURNS:
    //      - Boolean: Provisioning Tax enabled.
    //
    public static Boolean IsTaxProvisionsEnabled()
    {
      return GeneralParam.GetBoolean("Cashier.TaxProvisions", "Enabled");
    }

    // PURPOSE: check if MultiCurrencyExchange is enabled
    //
    // RETURNS:
    //      - Boolean: Multicurrency exchange enabled.
    //
    public static Boolean IsMultiCurrencyExchangeEnabled()
    {
      return GeneralParam.GetBoolean("MultiCurrencyExchange", "Enabled");
    }

    //------------------------------------------------------------------------------
    // PURPOSE: returns Provisioning Tax Pct
    //
    // RETURNS:
    //      - Decimal: Provisioning Tax pct
    //
    public static Decimal TaxProvisionsPct()
    {
      return GeneralParam.GetDecimal("Cashier.TaxProvisions", "Pct");
    }

    //------------------------------------------------------------------------------
    // PURPOSE: check if Custody Tax is enabled
    //
    // RETURNS:
    //      - Boolean: Custody Tax enabled.
    //
    public static Boolean IsTaxCustodyEnabled()
    {
      return (GeneralParam.GetInt32("Cashier.TaxCustody", "Mode", 0) > 0);
    }

    /// <summary>
    /// Check if configuration applies Custody and returns the ratio type to be calculated
    /// </summary>
    /// <param name="IsChipsOperation"></param>
    /// <returns></returns>
    public static TaxCustodyMode ApplyCustodyConfigurationCalculateRatio(Boolean IsChipsOperation)
    {
      if (Misc.IsTaxCustodyEnabled()
        && ((Misc.GetTaxCustodyType() == (Int32)Misc.TaxCustodyType.Both) ||
            (Misc.GetTaxCustodyType() == (Int32)Misc.TaxCustodyType.OnlyGamingTables && IsChipsOperation) ||
            (Misc.GetTaxCustodyType() == (Int32)Misc.TaxCustodyType.OnlyMachines && !IsChipsOperation)))
      {
        return Misc.GetCustodyMode();
      }

      return TaxCustodyMode.None;
    }

    /// <summary>
    /// Check if configuration applies Custody
    /// </summary>
    /// <param name="IsChipsOperation"></param>
    /// <returns></returns>
    public static Boolean ApplyCustodyConfiguration(Boolean IsChipsOperation)
    {
      if (Misc.IsTaxCustodyEnabled()
        && ((Misc.GetTaxCustodyType() == (Int32)Misc.TaxCustodyType.Both) ||
            (Misc.GetTaxCustodyType() == (Int32)Misc.TaxCustodyType.OnlyGamingTables && IsChipsOperation) ||
            (Misc.GetTaxCustodyType() == (Int32)Misc.TaxCustodyType.OnlyMachines && !IsChipsOperation)))
      {
        return true;
      }

      return false;
    }

    // LTC 07-OCT-2016
    //------------------------------------------------------------------------------
    // PURPOSE: Get tax custody type 
    //
    // RETURNS:
    //      - Int: Custody Type.
    //        0 => Both
    //        1 => Only Machines
    //        2 => Only Gaming Tables
    //
    public static Int32 GetTaxCustodyType()
    {
      return (GeneralParam.GetInt32("Cashier.TaxCustody", "Type", 0));
    }

    /// <summary>
    ///       TaxOnSplit1  = 1 : CodereMode: Tax no included 80%/20% and 10% Custody--> 72 Deposit,      20 Company B, 8 Custody 8      
    ///       TaxOnDeposit = 2 : WinMode:    Tax included 80%/20% and 10% Custody   --> 72.73 Deposit A, 20 Company B, 7.27 Custody 8
    /// </summary>
    public enum TaxCustodyMode
    {
      None = 0,
      TaxOnSplit1 = 1, // CodereMode: Tax no included 80%/20% and 10% Custody--> 72 Deposit,      20 Company B, 8 Custody 8
      TaxOnDeposit = 2 // WinMode:    Tax included 80%/20% and 10% Custody   --> 72.73 Deposit A, 20 Company B, 7.27 Custody 8
    }

    // LTC 07-OCT-2016
    /// <summary>
    ///       Both  = 0 :           Apply tax custody for both Machines and gamins tables
    ///       OnlyMachines = 1      Apply tax custody only for Machines
    ///       OnlyGamingTables = 2  Apply tax custody only for Gaming tables
    /// </summary>
    public enum TaxCustodyType
    {
      Both = 0,
      OnlyMachines = 1,
      OnlyGamingTables = 2
    }

    /// <summary>
    ///    Get the tax custody mode :
    ///      - TaxOnSplit1 = 1, // CodereMode: Tax no included 80%/20% and 10% Custody--> 72 Deposit,      20 Company B, 8 Custody 8
    ///      - TaxOnDeposit = 2 // WinMode:    Tax included 80%/20% and 10% Custody   --> 72.73 Deposit A, 20 Company B, 7.27 Custody 8
    /// 
    /// </summary>
    /// <returns></returns>
    public static TaxCustodyMode GetCustodyMode()
    {
      Int32 _gp_custodymode;
      TaxCustodyMode _custody_mode;

      _gp_custodymode = GeneralParam.GetInt32("Cashier.TaxCustody", "Mode", 0);

      //If GP is 0  --> unexpected.
      //   GP is >2 --> Wrong configuration

      if (_gp_custodymode > 2)
      {
        _gp_custodymode = 0;
      }

      _custody_mode = (TaxCustodyMode)_gp_custodymode;

      return _custody_mode;
    }

    // EOR 02-AUG-2016
    /// <summary>
    ///       CustodyVocuher  = 0 : Generate Voucher for Tax Custody      
    ///       CustodyConcept = 1 : Not Generate Voucher for Tax Custody, generate concept custody in Voucher Recharge
    /// </summary>
    public enum TaxCustodyVoucher
    {
      CustodyVoucher = 0, // Generate Voucher for Tax Custody
      CustodyConcept = 1, // Not Generate Voucher for Tax Custody, generate concept custody in Voucher Recharge
    }

    // EOR 02-AUG-2016
    /// <summary>
    ///    Get the tax custody voucher :
    ///      - CustodyVocuher  = 0 : Generate Voucher for Tax Custody      
    ///      - CustodyConcept = 1 : Not Generate Voucher for Tax Custody, generate concept custody in Voucher Recharge
    /// </summary>
    /// <returns></returns>
    public static TaxCustodyVoucher GetTaxCustodyVoucher()
    {
      Int32 _gp_custodyvoucher;
      TaxCustodyVoucher _custody_voucher;

      _gp_custodyvoucher = GeneralParam.GetInt32("Cashier.TaxCustody", "Voucher.ShowTaxRechargeVoucher", 0);

      if (_gp_custodyvoucher > 1)
      {
        _gp_custodyvoucher = 0;
      }

      _custody_voucher = (TaxCustodyVoucher)_gp_custodyvoucher;

      return _custody_voucher;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: returns Custody Tax Pct
    //
    // RETURNS:
    //      - Decimal: Custody Tax pct
    //
    public static Decimal TaxCustodyPct()
    {
      return GeneralParam.GetDecimal("Cashier.TaxCustody", "Pct");
    }

    //------------------------------------------------------------------------------
    // PURPOSE: returns Custody Tax Pct for Gaming tables
    //
    // RETURNS:
    //      - Decimal: Custody Tax pct for Gaming tables
    //
    public static Decimal GamingTablesTaxCustodyPct()
    {
      return GeneralParam.GetDecimal("GamingTables.TaxCustody", "Pct", (GeneralParam.GetDecimal("Cashier.TaxCustody", "Pct")));
    }

    //------------------------------------------------------------------------------
    // PURPOSE: returns Custody Tax Name
    //
    // RETURNS:
    //      - string: Custody Tax Name
    //
    public static string TaxCustodyName()
    {
      return GeneralParam.GetString("Cashier.TaxCustody", "Voucher.Title", Resource.String("STR_VOUCHER_TAX_CUSTODY_CONCEPT")); // LTC 11-MAY-2016 
    }

    // 21-APR-2016 LTC
    //------------------------------------------------------------------------------
    // PURPOSE: returns Custody Tax Concept
    //
    // RETURNS:
    //      - string: Custody Tax Concept
    //
    public static string TaxCustodyConcept()
    {
      return GeneralParam.GetString("Cashier.TaxCustody", "Voucher.Name", Resource.String("STR_CUSTODY_TAX_NAME")); // LTC 11-MAY-2016 
    }

    // 21-APR-2016 LTC
    //------------------------------------------------------------------------------
    // PURPOSE: returns Custody Refund Tax Name
    //
    // RETURNS:
    //      - string: Custody Refund Tax Name
    //
    public static string TaxCustodyRefundName()
    {
      return GeneralParam.GetString("Cashier.TaxCustody", "Voucher.RefundName", Resource.String("STR_VOUCHER_TAX_DEV_CUSTODY_TITLE"));
    }

    // 04-OCT-2016 EOR
    //------------------------------------------------------------------------------
    // PURPOSE: returns voucher mode
    //
    // RETURNS:
    //      - Int32: Voucher Mode
    //
    public static Int32 GetVoucherMode()
    {
      return GeneralParam.GetInt32("Cashier.Voucher", "Mode", 0);
    }

    // 04-OCT-2016 RGR
    //------------------------------------------------------------------------------
    // PURPOSE: returns true if is mode 8
    //
    // RETURNS:
    //      - bool: true or false
    //
    public static bool IsVoucherModeTV()
    {
      return (GeneralParam.GetInt32("Cashier.Voucher", "Mode", 0) == 8);
    }

    // 11-JAN-2017 ETP
    //------------------------------------------------------------------------------
    // PURPOSE: returns true if is mode 3 -- CODERE
    //
    // RETURNS:
    //      - bool: true or false
    //
    public static bool IsVoucherModeCodere()
    {
      return (GeneralParam.GetInt32("Cashier.Voucher", "Mode", 0) == 3);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get the ProviderId from the terminal with the terminal_type SITE.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - ProviderId
    //
    public static String GetSiteProviderId()
    {
      StringBuilder _sql_str;
      String _provider_id;

      _provider_id = String.Empty;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sql_str = new StringBuilder();
          _sql_str.AppendLine("  SELECT   TOP 1 TE_PROVIDER_ID                  ");
          _sql_str.AppendLine("    FROM   TERMINALS                             ");
          _sql_str.AppendLine("   WHERE   TE_TERMINAL_TYPE = @pTerminalTypeSite ");
          _sql_str.AppendLine("ORDER BY   TE_TERMINAL_ID DESC                   ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_str.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
          _sql_cmd.Parameters.Add("@pTerminalTypeSite", SqlDbType.SmallInt).Value = (Int16)TerminalTypes.SITE;

            _provider_id = (String)_sql_cmd.ExecuteScalar();

          if (_provider_id == null)
          {
              _provider_id = String.Empty;
          }
        }
      }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _provider_id;
    } // GetSiteProviderId

    //------------------------------------------------------------------------------
    // PURPOSE: Returns Terminal Transfer Amount by Terminal currency ISO CODE
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Amounts
    //          - String IsoCode
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String Amount by IsoCode
    //
    public static String GetTerminalTransferAmount(String AllAmounts, String IsoCode)
    {
      String[] _Amounts;
      _Amounts = AllAmounts.Split('|');

      foreach (String _str_AmountByCurrency in _Amounts)
      {
        String[] _AmountsByCurrency;
        _AmountsByCurrency = _str_AmountByCurrency.Split(';');

        if (_AmountsByCurrency[0].Equals(IsoCode))
        {
          return _str_AmountByCurrency;
        }
      }

      return "MXN;5;25;50;75;100";
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Concat the provider and the terminal with a specific separator.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Provider
    //          - String Terminal
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    //
    public static String ConcatProviderAndTerminal(String Provider, String Terminal)
    {
      return Provider + " - " + Terminal;
    } // ConcatProviderAndTerminal

    //------------------------------------------------------------------------------
    // PURPOSE:  Type of terminals used in command
    //           All terminals type, except:  3gs, mobile_bank_imb, istats.
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - List of TerminalTypes
    //
    public static TerminalTypes[] WCPTerminalTypes()
    {
      //TerminalTypes[] _terminal_types;
      List<TerminalTypes> _terminal_types;
      TerminalTypes[] _all_terminal_types;

      _terminal_types = new List<TerminalTypes>();
      //Int32 _idx_terminal_type;

      //_idx_terminal_type = 0;
      _all_terminal_types = AllTerminalTypes();
      //_terminal_types = new TerminalTypes[_all_terminal_types.Length - 3];

      //foreach (TerminalTypes _type in _all_terminal_types)
      //{
      //  if (_type != TerminalTypes.T3GS && _type != TerminalTypes.ISTATS && _type != TerminalTypes.MOBILE_BANK_IMB)
      //  {
      //    _terminal_types[_idx_terminal_type] = _type;
      //    _idx_terminal_type++;
      //  }
      //}

      _terminal_types.AddRange(Array.FindAll(_all_terminal_types, _type => _type != TerminalTypes.T3GS && _type != TerminalTypes.ISTATS && _type != TerminalTypes.MOBILE_BANK_IMB));

      return _terminal_types.ToArray();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: All terminals types, expcept 3GS.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - List of TerminalTypes
    //
    public static TerminalTypes[] AllTerminalTypesExcept3GS()
    {
      //TerminalTypes[] _terminal_types;
      List<TerminalTypes> _terminal_types;
      TerminalTypes[] _all_terminal_types;
      //Int32 _index;

      _terminal_types = new List<TerminalTypes>();
      //_index = 0;

      _all_terminal_types = AllTerminalTypes();
      //_terminal_types = new TerminalTypes[_all_terminal_types.Length - 1];

      _terminal_types.AddRange(Array.FindAll(_all_terminal_types, _type => _type != TerminalTypes.T3GS));

      //foreach (TerminalTypes _type in _all_terminal_types)
      //{
      //  if (_type != TerminalTypes.T3GS)
      //  {
      //    _terminal_types[_index] = _type;
      //    _index++;
      //  }
      //}

      return _terminal_types.ToArray();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: List of gaming terminal types
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - List of TerminalTypes
    //
    public static TerminalTypes[] AcceptorTerminalTypeList()
    {
      if (GeneralParam.GetBoolean("WS2S", "Stacker.Enabled", false))
      {
        return new TerminalTypes[] { TerminalTypes.SAS_HOST, TerminalTypes.PROMOBOX, TerminalTypes.T3GS };
      }
      else
      {
        return new TerminalTypes[] { TerminalTypes.SAS_HOST, TerminalTypes.PROMOBOX };
      }
    } // AcceptorTerminalTypeList

    public static Boolean CurrencyDirectExchange(Decimal AmountToExchange, string FromIsoCode, string ToIsoCode, out Decimal ExchangedAmount)
    {
      StringBuilder _sb;

      ExchangedAmount = 0;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine("SELECT dbo.ApplyExchange2(@Amount, @FromIsoCode, @ToIsoCode)");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@Amount", SqlDbType.Decimal).Value = AmountToExchange;
            _cmd.Parameters.Add("@FromIsoCode", SqlDbType.NVarChar).Value = FromIsoCode;
            _cmd.Parameters.Add("@ToIsoCode", SqlDbType.NVarChar).Value = ToIsoCode;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (_reader.Read())
              {
                ExchangedAmount = _reader.GetDecimal(0);
              }
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("ApplyExchange2 ");
        Log.Exception(_ex);
      }

        return false;
      }

    //------------------------------------------------------------------------------
    // PURPOSE: All terminals types.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - List of TerminalTypes
    //
    // NOTES:
    //    IMPORTANT! If you add a new terminal type, increment GUI --> TYPE_TERMINAL_SEL_PARAMS.NUM_CHECKBOXES.
    public static TerminalTypes[] AllTerminalTypes()
    {
      return new TerminalTypes[] { TerminalTypes.WIN, TerminalTypes.T3GS, TerminalTypes.SAS_HOST, TerminalTypes.SITE_JACKPOT, 
                                   TerminalTypes.MOBILE_BANK, TerminalTypes.MOBILE_BANK_IMB, TerminalTypes.ISTATS, TerminalTypes.PROMOBOX,
                                   TerminalTypes.CASHDESK_DRAW, TerminalTypes.OFFLINE, TerminalTypes.GAMEGATEWAY, TerminalTypes.PARIPLAY };
    } // AllTerminalTypes

    //------------------------------------------------------------------------------
    // PURPOSE: All terminals types Sas Host.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - List of TerminalTypes
    //
    // NOTES:
    //        
    public static TerminalTypes[] AllTerminalTypeSasHost()
    {
      return new TerminalTypes[] { TerminalTypes.SAS_HOST };
    } // AllTerminalTypeSasHost

    //------------------------------------------------------------------------------
    // PURPOSE: List of gaming terminal types
    // 
    //  PARAMS:
    //      - INPUT: ShowGames - Include Terminal Games in Type List
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - List of TerminalTypes
    //
    public static TerminalTypes[] GamingTerminalTypeList(Boolean ShowGames = true)
    {
      if (ShowGames)
      {
        return new TerminalTypes[] {TerminalTypes.WIN, TerminalTypes.T3GS, TerminalTypes.SAS_HOST, 
                                   TerminalTypes.CASHDESK_DRAW, TerminalTypes.TERMINAL_DRAW, 
                                   TerminalTypes.OFFLINE, TerminalTypes.GAMEGATEWAY, TerminalTypes.PARIPLAY};
      }
      else
      {
        return new TerminalTypes[] {TerminalTypes.WIN, TerminalTypes.T3GS, TerminalTypes.SAS_HOST, 
                                   TerminalTypes.CASHDESK_DRAW, TerminalTypes.TERMINAL_DRAW, TerminalTypes.OFFLINE};
      }
    } // GamingTerminalTypeList

    //------------------------------------------------------------------------------
    // PURPOSE: List of gaming terminal types as String
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String of list of TerminalTypes
    //
    public static String TerminalTypeListToString(TerminalTypes[] TerminalList)
    {
      String _str_in;

      _str_in = "";
      foreach (TerminalTypes _type in TerminalList)
      {
        _str_in += (Int16)_type + ", ";
      }
      if (_str_in.Length > 1)
      {
        _str_in = _str_in.Substring(0, _str_in.Length - 2);
      }

      return _str_in;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: List of gaming terminal types as String
    // 
    //  PARAMS:
    //      - INPUT: Showgames: include Terminal Games in StringList 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String of list of TerminalTypes
    //
    public static String GamingTerminalTypeListToString(Boolean ShowGames = true)
    {
      return TerminalTypeListToString(GamingTerminalTypeList(ShowGames));
    } // GamingTerminalTypeListToString

    //------------------------------------------------------------------------------
    // PURPOSE: List of System Users
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - List of GU_USER_TYPE
    //
    public static GU_USER_TYPE[] SystemUsersList()
    {
      return new GU_USER_TYPE[] { GU_USER_TYPE.SYS_ACCEPTOR, GU_USER_TYPE.SYS_GAMING_TABLE, GU_USER_TYPE.SYS_PROMOBOX,
                                  GU_USER_TYPE.SYS_SYSTEM, GU_USER_TYPE.SYS_TITO, GU_USER_TYPE.SUPERUSER, GU_USER_TYPE.SYS_REDEMPTION};
    } // SystemUsersList

    //------------------------------------------------------------------------------
    // PURPOSE: List of System Users as String
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String of list of System Users
    //
    public static String SystemUsersListListToString()
    {
      String _str_in;

      _str_in = "";
      foreach (GU_USER_TYPE _type in SystemUsersList())
      {
        _str_in += (Int16)_type + ", ";
      }

      if (_str_in.Length > 1)
      {
        _str_in = _str_in.Substring(0, _str_in.Length - 2);
      }

      return _str_in;
    } // SystemUsersListListToString

    //------------------------------------------------------------------------------
    // PURPOSE: Generates and return the visible trackdata
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String in this format "*****************1234"
    //      - Where 1234 is de visible part of the trackdata
    // 
    //   NOTES:
    //    
    public static String VisibleTrackdata(String TrackData)
    {
      Int32 _trackdata_length;
      Int32 _visible_length;
      String _trackdata_visible;

      if (TrackData == null)
      {
        return "";
      }

      _trackdata_length = TrackData.Length;
      _visible_length = _trackdata_length;

      try
      {
        _visible_length = Int32.Parse(WSI.Common.Misc.ReadGeneralParams("Cashier", "TrackDataVisibleChars"));
        _visible_length = Math.Max(_visible_length, 0);
        _visible_length = Math.Min(_visible_length, _trackdata_length);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      _trackdata_visible = new string('*', _trackdata_length - _visible_length);
      _trackdata_visible = _trackdata_visible + TrackData.Substring(_trackdata_length - _visible_length);

      return _trackdata_visible;
    } // VisibleTrackdata

    /// <summary>
    /// Get a general parameters value
    /// </summary>
    /// <returns>Site id.</returns>
    public static String ReadGeneralParams(String Group, String Subject)
    {
      return GeneralParam.Value(Group, Subject);
    } // ReadGeneralParams

    /// <summary>
    /// Get a general parameters value
    /// </summary>
    /// <returns>Site id.</returns>
    public static String ReadGeneralParams(String Group, String Subject, SqlTransaction SqlTrx)
    {
      return GeneralParam.Value(Group, Subject);
    } // ReadGeneralParams

    static public long GetElapsedTicks(int FirstTick, int LastTick)
    {
      long _elapsed;

      _elapsed = LastTick;
      if (LastTick < FirstTick)
      {
        _elapsed += UInt32.MaxValue;
        _elapsed += 1;
      }
      _elapsed -= FirstTick;

      if (_elapsed < 0)
      {
        Log.Error("*** Computed elapsed time is less than zero.");
        _elapsed = 0;
      }

      return _elapsed;
    }

    static public int GetTickCount()
    {
      return Environment.TickCount;
    }

    static public long GetElapsedTicks(int FirstTick)
    {
      return GetElapsedTicks(FirstTick, GetTickCount());
    }

    static public void WaitSeconds(int Seconds, Boolean WaitConnected)
    {
      DateTime _now;
      TimeSpan _ts;
      int _wait;
      int _ms;

      while (true)
      {
        _ms = 1000 * Seconds;
        _now = WGDB.Now;
        _ts = _now - _now.Date;
        _wait = _ms - ((int)_ts.TotalMilliseconds % _ms);

        if (_wait > 0)
        {
          System.Threading.Thread.Sleep(_wait);
        }

        if (!WaitConnected
            || WGDB.ConnectionState == ConnectionState.Open)
        {
          return;
        }

        System.Threading.Thread.Sleep(100);
      }
    }

    /// <summary>
    /// Converts the given string to an IPEndPoint
    /// </summary>
    /// <param name="IPAddressPort"></param>
    /// <returns></returns>
    static public IPEndPoint IPEndPointParse(String IPAddressPort)
    {
      int idx;
      IPAddress ip;
      int port;

      idx = IPAddressPort.IndexOf(':');
      ip = IPAddress.Parse(IPAddressPort.Substring(0, idx));
      port = int.Parse(IPAddressPort.Substring(1 + idx));

      return new IPEndPoint(ip, port);
    }

    /// <summary>
    /// Return IP and port
    /// </summary>
    /// <param name="IPAddressWithoutPort"></param>
    /// <param name="Port"></param>
    /// <returns></returns>
    public static String GetIPAndPort(String IPAddressWithoutPort, String Port)
    {
      String _ip;
      String _port;
      String _ip_and_port;

      _ip = IPAddressWithoutPort.Trim();
      _port = Port.Trim();

      //If needed set start to http://
      if (!_ip.StartsWith("http://"))
      {
        _ip = "http://" + _ip;
      }

      //If needed set ':' between ip and port
      if ((!_ip.EndsWith(":")) && (!_port.StartsWith(":")))
      {
        _port = ":" + _port;
      }

      _ip_and_port = _ip + _port;

      //If needed set '/' at the end
      if (!_ip_and_port.EndsWith("/"))
      {
        _ip_and_port = _ip_and_port + "/";
      }

      return _ip_and_port;
    }
    static public UInt32 Checksum(Byte[] Data)
    {
      const UInt16 C1 = 52845;
      const UInt16 C2 = 22719;
      const UInt16 C3 = 55665;

      Byte cipher;
      UInt16 r;
      UInt32 checksum;

      r = C3;
      checksum = 0;

      for (int i = 0; i < Data.Length; i++)
      {
        cipher = (Byte)(Data[i] ^ (Byte)(r >> 8));
        r = (UInt16)((cipher + r) * C1 + C2);
        checksum += cipher;
      }

      return checksum;
    } // Checksum

    /// <summary>
    /// Delete log files of the log directory
    /// </summary>
    /// <param name="Path"></param>
    /// <param name="MaskFile"></param>
    /// <param name="NumDays"></param>
    static public void ClearHistoryLogFiles(String LogPath, String MaskFile, Int32 NumDays)
    {
      DirectoryInfo dir;

      try
      {
        dir = new DirectoryInfo(LogPath);

        // Search all files in directory
        foreach (FileInfo file_info in Array.FindAll(dir.GetFiles(MaskFile), file_info => file_info.LastWriteTime.AddDays(NumDays) < DateTime.Now))
        {
            file_info.Delete();
          }
        }
      catch
      {
        ;
      }
    } // ClearHistoryLogFiles

    public static Boolean VerifyXmlDoc(XmlDocument Doc, RSA Key)
    {
      // Create a new SignedXml object and pass it
      // the XML document class.
      SignedXml signedXml = new SignedXml(Doc);

      // Find the "Signature" node and create a new
      // XmlNodeList object.
      XmlNodeList nodeList = Doc.GetElementsByTagName("Signature");

      if (nodeList.Count != 1)
      {
        return false;
      }

      // Load the signature node.
      signedXml.LoadXml((XmlElement)nodeList[0]);

      // Check the signature and return the result.
      return signedXml.CheckSignature(Key);
    }

    #region Events

    // For notify License status changes.
    public delegate void LicenseStatusChangedDelegate(LicenseStatus LicenseStatus);
    private static event LicenseStatusChangedDelegate OnLicenseStatusChanged;

    #endregion

    public class LicenseParameters
    {
      #region Private properties
      private Boolean m_check_only_valid_date;
      private List<string> m_features;
      #endregion

      #region Public properties
      public Boolean CheckOnlyValidDate
      {
        get { return m_check_only_valid_date; }
        set { m_check_only_valid_date = value; }
      }

      public List<string> Features
      {
        get { return m_features; }
        set { m_features = value; }
      }
      #endregion

      #region Public methods
      public LicenseParameters()
      {
        m_check_only_valid_date = false;
        m_features = new List<string>();
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Add a feature into feature list.
      //
      //  PARAMS :
      //      - INPUT :
      //          
      //      - OUTPUT :
      //            
      //
      // RETURNS :
      //      - Boolean value
      //
      //   NOTES :
      //------------------------------------------------------------------------------
      public void AddFeature(String StrFeature)
      {
        if (!m_features.Contains(StrFeature))
        {
          m_features.Add(StrFeature);
        }
      }
      #endregion
    }

    public static void StartLicenseChecker(LicenseStatusChangedDelegate LicenseStatusChangedHandler)
    {
      StartLicenseChecker(LicenseStatusChangedHandler, false, new List<string>());
    }

    public static void StartLicenseChecker(LicenseStatusChangedDelegate LicenseStatusChangedHandler, Boolean CheckOnlyValidDate, List<string> Features)
    {
      System.Threading.Thread _thread;
      LicenseParameters _license_params = new LicenseParameters();

      _license_params.CheckOnlyValidDate = CheckOnlyValidDate;
      _license_params.Features = Features;

      OnLicenseStatusChanged += LicenseStatusChangedHandler;
      _thread = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(LicenseCheckerThread));
      _thread.Name = "LicenseCheckerThread";
      _thread.Start(_license_params);
    }

    private static void LicenseCheckerThread(Object LisenceParams)
    {
      Boolean _check_only_valid_date;
      DateTime _not_valid_after;
      TimeSpan _left;
      TimeSpan _wait_hint;
      DateTime _now;
      LicenseStatus _license_status;
      LicenseStatus _previous_license_status;
      LicenseParameters _lisence_params;
      List<string> _features;

      _lisence_params = (LicenseParameters)LisenceParams;

      _check_only_valid_date = _lisence_params.CheckOnlyValidDate;
      _features = _lisence_params.Features;

      _license_status = LicenseStatus.NOT_CHECKED;

      _wait_hint = new TimeSpan();

      while (true)
      {
        System.Threading.Thread.Sleep(_wait_hint);
        _wait_hint = new TimeSpan(0, 1, 0);

        _now = WGDB.Now;

        _previous_license_status = _license_status;
        _license_status = Misc.CheckLicense(_now, _check_only_valid_date, _features, out _not_valid_after);

        // Error reading the license
        if (_license_status == LicenseStatus.NOT_CHECKED)
        {
          continue;
        }

        if (_license_status != _previous_license_status)
        {
          OnLicenseStatusChanged(_license_status);
        }

        if (_license_status != LicenseStatus.VALID && _license_status != LicenseStatus.VALID_EXPIRES_SOON)
        {
          continue;
        }

        _left = _not_valid_after.AddDays(1).Subtract(_now);
        if (_left.Ticks > 0)
        {
          if (_left.TotalDays > 1)
          {
            _wait_hint = new TimeSpan(1, 0, 0, 0);
          }
          else
          {
            _wait_hint = new TimeSpan(_left.Ticks / 2);
          }
        }
      }
    }

    private static LicenseStatus CheckLicense(DateTime CurrentDateTime, Boolean CheckOnlyValidDate, List<string> Features, out DateTime LicenseNotValidAfter)
    {
      string xml_public_key = "<RSAKeyValue><Modulus>1jZt5vR4VWWog3xZkGdyNjI2i+1F8zvAirwpR4aSVN1qSiMi1uRk2fuVYsgKYlYUDqzaaKjAuGr6DmKYeTTZUhSQS+vkL/6iLxnR52+5adJCD40AgHGIwm05lUegH8oWasVdNQXlIWEsVgiDSLLajA8clWUOXdFrdTLa07urppc=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";

      XmlDocument doc;
      RSACryptoServiceProvider Key;

      String sql_str;

      String aux_text;
      StringReader str_reader;
      XmlReader xml_reader;

      LicenseStatus _license_status;

      _license_status = LicenseStatus.INVALID;
      LicenseNotValidAfter = DateTime.MinValue;

      try
      {
        Key = new RSACryptoServiceProvider();
        Key.FromXmlString(xml_public_key);

        //
        // Read Licence from database
        //
        sql_str = "  SELECT TOP (1) WL_LICENCE " +
                  "    FROM LICENCES " +
                  "ORDER BY WL_INSERTION_DATE DESC";

        using (SqlConnection sql_conn = WGDB.Connection())
        {
          using (SqlCommand sql_command = new SqlCommand(sql_str, sql_conn))
          {
            using (SqlDataReader sql_reader = sql_command.ExecuteReader())
            {
            if (sql_reader.HasRows)
            {
              sql_reader.Read();
              aux_text = sql_reader.GetString(0);

              str_reader = new StringReader(aux_text);
              xml_reader = XmlReader.Create(str_reader);
            }
            else
            {
              Log.Message("*** License: DB Table not found or empty");

              return _license_status;
            }
            }

            doc = new XmlDocument();
            try
            {
              //doc.Load("WSI.License.xml");
              doc.Load(xml_reader);
            }
            catch
            {
              Log.Message("*** License: NOT FOUND");
            }

            if (!VerifyXmlDoc(doc, Key))
            {
              Log.Message("*** License: NOT VALID");

              return _license_status;
            }
          }
        }

        _license_status = LicenseStatus.NOT_AUTHORIZED;

        XmlNode site_license;
        XmlNode xml_node;

        if (CheckOnlyValidDate)
        {
          Int32 _gp_site_id;
          Int32 _xml_site_id;
          XmlNodeList _sites = doc.GetElementsByTagName("SiteLicense");

          if (!Int32.TryParse(Misc.ReadGeneralParams("Site", "Identifier"), out _gp_site_id))
          {
            Log.Message("*** License: General Parameter 'Site.Identifier' not set.");

            return _license_status;
          }

          site_license = null;
          foreach (XmlNode _site in _sites)
          {
            XmlNode _site_id;
            _site_id = _site.Attributes.GetNamedItem("SiteId");
            if (!Int32.TryParse(_site_id.Value, out _xml_site_id))
            {
              continue;
            }

            if (_xml_site_id == _gp_site_id)
            {
              site_license = _site;

              break;
            }
          }

          if (site_license == null)
          {
            Log.Message("*** License: SITE NOT AUTHORIZED (Not found)");

            return _license_status;
          }
        }
        else
        {
          XmlNodeList servers = doc.GetElementsByTagName("Server");
          XmlNode site_server;

          site_license = null;
          site_server = null;
          foreach (XmlNode server in servers)
          {
            XmlNode server_name;
            server_name = server.Attributes.GetNamedItem("Name");
            if (server_name.Value == Environment.MachineName)
            {
              site_server = server;
              site_license = server.ParentNode.ParentNode;

              break;
            }
          }

          if (site_license == null
              || site_server == null)
          {
            Log.Message("*** License: SERVER NOT AUTHORIZED (Not found)");

            return _license_status;
          }

          // Check Volume Serial Number
          string serial_number;
          GetInfo get_info;

          get_info = new GetInfo();

          xml_node = site_server.Attributes.GetNamedItem("VolumeSerialNumber");
          if (xml_node.Value != "ANY")
          {
            serial_number = get_info.GetVolumeSerial("C");
            if (serial_number != xml_node.Value.Replace("-", ""))
            {
              Log.Message("*** License: SERVER NOT AUTHORIZED (Volume Serial Number)");

              return _license_status;
            }
          }

          // Check @mac
          xml_node = site_server.Attributes.GetNamedItem("MacAddress");
          if (xml_node.Value != "ANY")
          {
            if (!get_info.IsMACAddressPresent(xml_node.Value))
            {
              Log.Message("*** License: SERVER NOT AUTHORIZED (MAC Address)");

              return _license_status;
            }
          }
        }

        _license_status = LicenseStatus.EXPIRED;

        // Check Date
        xml_node = site_license.Attributes.GetNamedItem("LicenseNotValidAfter");
        LicenseNotValidAfter = DateTime.Parse(xml_node.Value);
        if (CurrentDateTime >= LicenseNotValidAfter.AddDays(1))
        {
          Log.Message("*** License: EXPIRED");

          return LicenseStatus.VALID;
        }

        //Check Features
        if (Features.Count > 0)
        {
          foreach (string _feature in Features)
          {
            XmlNodeList _xml_features = site_license.SelectNodes("Features");

            if (_xml_features.Count != 1)
            {
              Log.Message("*** License: NO FEATURES INSTALLED");

              return _license_status;
            }

            foreach (XmlNode _xml_feature_node in _xml_features[0])
            {
              Boolean _feature_enabled = false;

              if (_xml_feature_node.Attributes.GetNamedItem("Name").Value == _feature)
              {
                if (!Boolean.TryParse(_xml_feature_node.Attributes.GetNamedItem("Enabled").Value, out _feature_enabled))
                {
                  Log.Message("*** License Enabled Parameter: not set");

                }

                if (!_feature_enabled)
                {
                  Log.Message("*** License: DISABLED");

                  return _license_status;
                }
              }
            }
          }
        }

        TimeSpan ts = LicenseNotValidAfter.AddDays(1).Subtract(CurrentDateTime);
        int days_left = ((int)ts.TotalDays);

        if (days_left < 30)
        {
          string msg;
          msg = "Your license expires in " + days_left.ToString() + " days.";

          Log.Message("*** License: " + msg);
        }

        _license_status = LicenseStatus.VALID;

        if (days_left < 7)
        {
          _license_status = LicenseStatus.VALID_EXPIRES_SOON;
        }
      }
      catch (SqlException _sql_ex)
      {
        if (_sql_ex.Number == -2) // Connection timeout
        {
          Log.Warning(" *** Sql Database Timeout. License not validated.");

          return LicenseStatus.NOT_CHECKED;
        }

          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
      catch (Exception ex)
      {
        Log.Message("*** License: Validation failed");
        Log.Exception(ex);
      }

      return _license_status;
    } // CheckLicense

    public static Boolean CheckLicenseFromFile(String LicenceFile, out String LicenceText, out DateTime LicenseNotValidAfter)
    {
      string xml_public_key = "<RSAKeyValue><Modulus>1jZt5vR4VWWog3xZkGdyNjI2i+1F8zvAirwpR4aSVN1qSiMi1uRk2fuVYsgKYlYUDqzaaKjAuGr6DmKYeTTZUhSQS+vkL/6iLxnR52+5adJCD40AgHGIwm05lUegH8oWasVdNQXlIWEsVgiDSLLajA8clWUOXdFrdTLa07urppc=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";

      Boolean is_valid;
      XmlDocument doc;
      RSACryptoServiceProvider Key;

      is_valid = false;
      LicenseNotValidAfter = WGDB.Now.Date;
      LicenceText = "";

      try
      {
        Key = new RSACryptoServiceProvider();
        Key.FromXmlString(xml_public_key);

        doc = new XmlDocument();
        try
        {
          doc.Load(LicenceFile);
          //doc.Load(xml_reader);
        }
        catch
        {
          Log.Message("*** License: NOT FOUND");
        }

        if (!VerifyXmlDoc(doc, Key))
        {
          Log.Message("*** License: NOT VALID");

          return false;
        }

        XmlNode xml_node;
        XmlNode site_server;
        XmlNode site_licence;
        XmlNodeList servers = doc.GetElementsByTagName("Server");

        site_server = null;
        site_licence = null;
        foreach (XmlNode server in servers)
        {
          site_server = server;
          site_licence = server.ParentNode.ParentNode;

          // Check Date
          xml_node = site_licence.Attributes.GetNamedItem("LicenseNotValidAfter");
          LicenseNotValidAfter = DateTime.Parse(xml_node.Value);
        }

        LicenceText = doc.InnerXml;

        is_valid = true;
      }

      catch (Exception ex)
      {
        Log.Message("*** License: Validation failed");
        Log.Exception(ex);
      }

      return is_valid;
    } // CheckLicenseFromFile

    //------------------------------------------------------------------------------
    // PURPOSE : Deflate String
    //
    //  PARAMS :
    //      - INPUT :
    //          - StringToDeflate
    //
    //      - OUTPUT :
    //          - DeflatedString
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void DeflateString(String StringToDeflate, out String DeflatedString)
    {
      MemoryStream _mem_stream;
      DeflateStream _compress;
      Byte[] _logger;

      _logger = Encoding.UTF8.GetBytes(StringToDeflate);

      _mem_stream = new MemoryStream();

      // Compressed
      _compress = new DeflateStream(_mem_stream, CompressionMode.Compress, true);
      _compress.Write(_logger, 0, _logger.Length);
      _compress.Close();
      _compress = null;

      DeflatedString = Convert.ToBase64String(_mem_stream.GetBuffer(), 0, (int)_mem_stream.Length);

    } // DeflateString

    //------------------------------------------------------------------------------
    // PURPOSE : Deflate String
    //
    //  PARAMS :
    //      - INPUT :
    //          - StringToInflate
    //
    //      - OUTPUT :
    //          - InflatedString
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void InflateString(String StringToInflate, out String InflatedString)
    {
      MemoryStream _mem_stream;
      DeflateStream _compress;
      Byte[] _logger;
      int _len;

      _logger = Convert.FromBase64String(StringToInflate);
      _mem_stream = new MemoryStream(_logger);

      // Decompressed
      _logger = new Byte[(64 * 1024)];
      _compress = new DeflateStream(_mem_stream, CompressionMode.Decompress, true);
      _len = _compress.Read(_logger, 0, _logger.Length);
      _compress.Close();
      _compress = null;

      InflatedString = Encoding.UTF8.GetString(_logger, 0, _len);

    } // InflateString


    /// <summary>
    /// Returns true if the given terminal name exists in the database
    /// </summary>
    /// <param name="ExternalTerminalId"></param>
    /// <param name="Type"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean GetTerminalInfo(String ExternalTerminalId,
                                          int Type,
                                          out Int32 TerminalId,
                                          out String ProviderId,
                                          out String TerminalName,
                                          out TerminalStatus Status,
                                          out String ServerExternalId,
                                          out Currency SasAcountDenomCents,
                                          SqlTransaction Trx)
    {
      StringBuilder _sql_str;

      TerminalId = 0;
      ProviderId = "";
      TerminalName = "";
      Status = 0;
      ServerExternalId = "";
      SasAcountDenomCents = 0;

      if (ExternalTerminalId.Length > 40)
      {
        ExternalTerminalId = ExternalTerminalId.Substring(0, 40);
      }

      // 
      // Get Terminal Id
      //
      _sql_str = new StringBuilder();
      _sql_str.AppendLine("SELECT   TE_TERMINAL_ID ");
      _sql_str.AppendLine("       , TE_PROVIDER_ID ");
      _sql_str.AppendLine("       , TE_NAME        ");
      _sql_str.AppendLine("       , TE_STATUS      ");
      _sql_str.AppendLine("       , (SELECT TE_EXTERNAL_ID FROM TERMINALS WHERE TE_TERMINAL_ID = T.TE_SERVER_ID) ");
      _sql_str.AppendLine("       , TE_SAS_ACCOUNTING_DENOM ");
      _sql_str.AppendLine("  FROM   TERMINALS T   ");
      _sql_str.AppendLine(" WHERE   TE_TYPE        = @pType ");
      _sql_str.AppendLine("   AND   TE_EXTERNAL_ID = @pExternalId ");

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sql_str.ToString(), Trx.Connection, Trx))
        {
      _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).Value = Type;
      _sql_cmd.Parameters.Add("@pExternalId", SqlDbType.NVarChar).Value = ExternalTerminalId;

          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
      {
        if (_sql_reader.Read())
        {
          TerminalId = _sql_reader.GetInt32(0);
          ProviderId = _sql_reader.IsDBNull(1) ? "" : _sql_reader.GetString(1);
          TerminalName = _sql_reader.IsDBNull(2) ? "" : _sql_reader.GetString(2);
          Status = (TerminalStatus)_sql_reader.GetInt32(3);
          ServerExternalId = _sql_reader.IsDBNull(4) ? "" : _sql_reader.GetString(4);
          SasAcountDenomCents = _sql_reader.IsDBNull(5) ? 0 : _sql_reader.GetDecimal(5);

          return true;
        }
      }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetTerminalInfo

    /// <summary>
    /// Check if exist pendign terminal draws
    /// </summary>
    /// <param name="AccountId"></param>
    /// <returns></returns>
    public static Boolean Common_CheckPendingTerminalDrawsRecharges(Int64 AccountId)
    {
      List<CancellableOperation> _cancellable_operations;

      if (!Misc.IsTerminalDrawEnabled())
      {
        return false;
      }

      return Common_GetPendingTerminalDrawsRechargesToCancel(AccountId, out _cancellable_operations);
    } // Common_CheckPendingTerminalDrawsRecharges

    /// <summary>
    /// Get pendign amount of terminal draws
    /// </summary>
    /// <param name="AccountId"></param>
    /// <returns></returns>
    public static Currency Common_GetPendingAmountTerminalDrawsRecharges(Int64 AccountId)
    {
      List<CancellableOperation> _cancellable_operations;

      _cancellable_operations = new List<CancellableOperation>();

      if (Misc.IsTerminalDrawEnabled())
      {
        Common_GetPendingTerminalDrawsRechargesToCancel(AccountId, out _cancellable_operations);
      }

      return Common_GetPendingAmountTerminalDrawsRecharges(_cancellable_operations);
    } // Common_GetPendingAmountTerminalDrawsRecharges

    public static Currency Common_GetPendingAmountTerminalDrawsRecharges(List<CancellableOperation> CancellableOperations)
    {
      Currency _total_amount;

      _total_amount = 0;

      if (Misc.IsTerminalDrawEnabled())
      {
        foreach (CancellableOperation _operation in CancellableOperations)
        {
          _total_amount += _operation.OperationAmount;
        }
      }

      return _total_amount;
    } // Common_GetPendingAmountTerminalDrawsRecharges

    /// <summary>
    /// Get pending terminal draws recharges to cancel
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="OperationId"></param>
    /// <param name="Amount"></param>
    /// <returns></returns>
    public static Boolean Common_GetPendingTerminalDrawsRechargesToCancel(Int64 AccountId, out List<CancellableOperation> CancellableOperations)
    {
      StringBuilder _sb;
      Currency _amount;
      Int64 _operation_id;

      CancellableOperations = new List<CancellableOperation>();
      _amount = new Currency();
      _operation_id = 0;

      if (!Misc.IsTerminalDrawEnabled())
      {
        return false;
      }

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("   SELECT   TDR_OPERATION_ID                              ");
        _sb.AppendLine("          , TDR_TOTAL_CASH_IN - TDR_RE_BET RECHARGE_DIFF  ");
        _sb.AppendLine("     FROM   TERMINAL_DRAWS_RECHARGES                      ");
        _sb.AppendLine("    WHERE   TDR_ACCOUNT_ID  = @AccountId                  ");
        _sb.AppendLine("      AND   TDR_STATUS      = @Status                     ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@AccountId", SqlDbType.BigInt).Value = AccountId;
            _cmd.Parameters.Add("@Status", SqlDbType.Int).Value = TerminalDraw.TerminalDraw.StatusTerminalDraw.PENDING;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              while (_reader.Read())
              {
                _operation_id = _reader.GetInt64(0);
                _amount = _reader.GetDecimal(1);

                CancellableOperations.Add(SetTerminalDrawCancellableOperation(_operation_id, _amount));
              }

              return (CancellableOperations.Count > 0);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Common_GetPendingTerminalDrawsRechargesToCancel

    /// <summary>
    /// Set terminal draw cancellable operations
    /// </summary>
    /// <param name="OperationId"></param>
    /// <param name="Amount"></param>
    /// <returns></returns>
    private static CancellableOperation SetTerminalDrawCancellableOperation(Int64 OperationId, Currency Amount)
    {
      return new CancellableOperation()
      {
        OperationId = OperationId,
        Balance = new MultiPromos.AccountBalance(0, Amount, 0),
        OperationAmount = Amount,
        CashInTaxAmount = 0,
        IsTerminalDraw = true
      };

    } // SetTerminalDrawCancellableOperation

    //------------------------------------------------------------------------------
    // PURPOSE: Get Terminal name by TerminalId
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TerminalId
    //          - SqlTrx
    //
    //      - OUTPUT:
    //          - TerminalName
    //
    // RETURNS:
    //    - Boolean:
    //
    public static Boolean GetTerminalName(Int32 TerminalId, out String TerminalName, SqlTransaction SqlTrx)
    {
      Int64 _v_account_id;
      String _v_track_data;

      return GetTerminalNameAndVirtualAccount(TerminalId, out TerminalName, out _v_account_id, out _v_track_data, SqlTrx);
    }

    public static Boolean GetTerminalNameAndVirtualAccount(Int32 TerminalId,
                                                           out String TerminalName, out Int64 VirtualAccountId, out String VirtualTrackData,
                                                           SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      TerminalName = "";
      VirtualAccountId = 0;
      VirtualTrackData = "";

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   TE_NAME ");
        _sb.AppendLine("       , ISNULL(TE_VIRTUAL_ACCOUNT_ID, 0) ");
        _sb.AppendLine("  FROM   TERMINALS ");
        _sb.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            TerminalName = _reader.GetString(0);
            VirtualAccountId = _reader.GetInt64(1);
            VirtualTrackData = "00000000000000000000" + CardData.VIRTUAL_FLAG_TRACK_DATA + VirtualAccountId.ToString();

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //GetTerminalName

    //------------------------------------------------------------------------------
    // PURPOSE: Returns the Opening DateTime according to the DateTime, OpeningHour and OpeningMinutes parameters
    // 
    //  PARAMS:
    //      - INPUT:
    //          - DateTime
    //          - OpeningHour
    //          - OpeningMinutes
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - DateTime
    //
    public static DateTime Opening(DateTime DateTime, Int32 OpeningHour, Int32 OpeningMinutes)
    {
      DateTime _today_opening;
      DateTime _now;

      _now = DateTime;
      _today_opening = _now;

      _today_opening = new DateTime(_today_opening.Year, _today_opening.Month, _today_opening.Day,
                                   OpeningHour, OpeningMinutes, 0);
      if (_today_opening > _now)
      {
        _today_opening = _today_opening.AddDays(-1);
      }

      return _today_opening;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Returns the Opening DateTime according to the DateTime parameter
    // 
    //  PARAMS:
    //      - INPUT:
    //          - DateTime
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - DateTime
    //
    public static DateTime Opening(DateTime DateTime)
    {
      Int32 _opening_hour;
      Int32 _opening_minutes;

      Int32.TryParse(Misc.ReadGeneralParams("WigosGUI", "ClosingTime"), out _opening_hour);
      Int32.TryParse(Misc.ReadGeneralParams("WigosGUI", "ClosingTimeMinutes"), out _opening_minutes);

      return Opening(DateTime, _opening_hour, _opening_minutes);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Returns the Opening DateTime
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - DateTime
    //
    public static DateTime TodayOpening()
    {
      return Opening(WGDB.Now);
    } // OpeningDateTime

    //------------------------------------------------------------------------------
    // PURPOSE: Returns the Opening DateTime In Function Name ReportTool
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - DateTime
    //
    // EOR 24-NOV-2016
    public static DateTime GetValueDateTimeReportTool(string value)
    {
      DateTime _date;
      Double _days;

      try
      {
        if (DateTime.TryParse(value, out _date))
        {
          return _date;
        }

        if (value.Trim() == "TodayOpening()")
        {
          return Opening(WGDB.Now);
        }

        if (value.Contains("TodayOpening()"))
        {
          if (Double.TryParse(value.Replace("TodayOpening()", "").Trim().Replace("+", "").Replace(" ", ""), out _days))
          {
            return Opening(WGDB.Now).AddDays(_days);
          }
        }

        return Opening(WGDB.Now);
      }
      catch
      {
        return Opening(WGDB.Now);
      }
    } // OpeningDateTime


    //------------------------------------------------------------------------------
    // PURPOSE: Converts an hex string representation to the string
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Hex
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - String
    //
    public static String HexToString(String Hex)
    {
      String _one_char;
      String _str_out;
      //

      _str_out = "";

      if (Hex.Length % 2 != 0)
      {
        Hex = "0" + Hex;
      }

      while (Hex.Length > 0)
      {
        // First take two Hex value using Substring.
        // Then convert Hex value into ascii.
        // Then convert ascii value into character.
        _one_char = Convert.ToChar(Convert.ToUInt32(Hex.Substring(0, 2), 16)).ToString();

        _str_out += _one_char;
        Hex = Hex.Substring(2, Hex.Length - 2);
      }

      return _str_out;
    } // HexToString

    //------------------------------------------------------------------------------
    // PURPOSE: Converts an string to its hex representation
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Data
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - String
    //
    public static String StringToHex(String Data)
    {
      String _one_char;
      String _hex_out;

      _hex_out = "";

      // First take each character using ToCharArray.
      // Then convert character into ascii.
      // Then convert ascii value into Hex format.
      foreach (Char c in Data.ToCharArray())
      {
        _one_char = String.Format("{0:X}", Convert.ToUInt32(c));
        _hex_out += _one_char;
      }

      return _hex_out;
    } // StringToHex

    //------------------------------------------------------------------------------
    // PURPOSE: Retunrs if the given string IsNull or White spaces 
    // 
    //  PARAMS:
    //      - INPUT:
    //          - string
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - Boolean
    //
    public static Boolean IsNullOrWhiteSpace(String Value)
    {
      if (Value == null)
      {
        return true;
      }

      return (Value.Trim() == String.Empty);
    } // IsNullOrWhiteSpace

    //------------------------------------------------------------------------------
    // PURPOSE : DataSet to Xml
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet DataSet
    //          - XmlWriteMode WriteMode
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public static String DataSetToXml(DataSet DataSet, XmlWriteMode WriteMode)
    {
      String _xml;

      _xml = "";
      using (StringWriter _sw = new StringWriter())
      {
        DataSet.WriteXml(_sw, WriteMode);
        _xml = _sw.ToString();
      }

      return _xml;
    } // DataSetToXml

    //------------------------------------------------------------------------------
    // PURPOSE : DataSet to Xml
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet DataSet
    //          - XmlWriteMode WriteMode
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public static void DataSetFromXml(DataSet DataSet, String Xml, XmlReadMode ReadMode)
    {
      using (StringReader _sr = new StringReader(Xml))
      {
        DataSet.ReadXml(_sr, ReadMode);
      }

    } // DataSetFromXml

    /// <summary>
    /// Class to xml
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <param name="XML"></param>
    /// <returns></returns>
    public static Boolean ClassToXml<T>(Object value, out String XML)
    {
      StringBuilder _sb;
      XmlSerializer xmlserializer;

      XML = String.Empty;

      try
      {
        xmlserializer = new XmlSerializer(typeof(T));
        _sb = new StringBuilder();

        using (XmlWriter _writer = XmlWriter.Create(_sb))
        {
          xmlserializer.Serialize(_writer, value);
        }

        XML = _sb.ToString();

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Error("Misc.ToXML");
      }

      return false;
    } // ClassToXml

    //------------------------------------------------------------------------------
    // PURPOSE : Calculate the Adler32 of an array of bytes
    //
    //  PARAMS :
    //      - INPUT :
    //          - Byte[] Bytes
    //          - Int32 Length
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Int32
    //
    private static Int32 Adler32(Byte[] Buffer, Int32 Length)
    {
      const UInt32 _a32mod = 65521;
      UInt32 _s1 = 1;
      UInt32 _s2 = 0;

      for (Int32 _idx_buf = 0; _idx_buf < Length; _idx_buf++)
      {
        _s1 = (_s1 + Buffer[_idx_buf]) % _a32mod;
        _s2 = (_s2 + _s1) % _a32mod;
      }

      return unchecked((Int32)((_s2 << 16) + _s1));
    } // Adler32

    //------------------------------------------------------------------------------
    // PURPOSE : DataSet to Xml
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet DataSet
    //          - DataSetRawFormat Format
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public static String DataSetToBase64(DataSet DataSet, DataSetRawFormat Format)
    {
      MemoryStream _ms_inflated;
      MemoryStream _ms_deflated;
      DeflateStream _df;
      String _xml64;

      if (DataSet == null)
      {
        return "";
      }

      _xml64 = "";

      _ms_inflated = new MemoryStream();
      _ms_deflated = new MemoryStream();
      _df = new DeflateStream(_ms_deflated, CompressionMode.Compress, true);

      switch (Format)
      {
        case DataSetRawFormat.BIN_DEFLATE_BASE64:
          {
            BinaryFormatter _formatter;
            SerializationFormat _sformat;

            // Serialize BinXml
            _formatter = new BinaryFormatter();
            _sformat = DataSet.RemotingFormat;
            DataSet.RemotingFormat = SerializationFormat.Binary;
            _formatter.Serialize(_ms_inflated, DataSet);
            DataSet.RemotingFormat = _sformat;

            // Compress
            _df.Write(_ms_inflated.GetBuffer(), 0, (Int32)_ms_inflated.Position);
            _df.Close();
          }
          break;

        case DataSetRawFormat.XML_ZLIB_BASE64:
          {
            Byte[] _buffer;
            Int32 _adler32;

            DataSet.WriteXml(_ms_inflated, XmlWriteMode.WriteSchema);

            // Compress
            _ms_deflated.WriteByte(0x58);
            _ms_deflated.WriteByte(0x85);
            _buffer = _ms_inflated.GetBuffer();

            _df.Write(_buffer, 0, (Int32)_ms_inflated.Position);
            _df.Close();

            _adler32 = Adler32(_buffer, (Int32)_ms_inflated.Position);
            _adler32 = IPAddress.HostToNetworkOrder(_adler32);
            _ms_deflated.Write(BitConverter.GetBytes(_adler32), 0, sizeof(Int32));
          }
          break;
      }

      _xml64 = Convert.ToBase64String(_ms_deflated.GetBuffer(), 0, (Int32)_ms_deflated.Position, Base64FormattingOptions.InsertLineBreaks);

      _df.Dispose();
      _ms_deflated.Dispose();
      _ms_inflated.Dispose();

      return _xml64;
    } // DataSetToBase64

    //------------------------------------------------------------------------------
    // PURPOSE : Returns if commission to company b and company b is enabled
    //
    // PARAMS :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean IsCommissionToCompanyBEnabled()
    {

      return GeneralParam.GetBoolean("Cashier", "Split.B.Enabled") && GeneralParam.GetBoolean("Cashier.PaymentMethod", "CommissionToCompanyB.Enabled");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Returns if has beneficiary and not AML external control
    //
    // PARAMS :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean HasBeneficiaryAndNotAMLExternalControl()
    {
      Boolean _antimoney_enabled;

      _antimoney_enabled = GeneralParam.GetBoolean("AntiMoneyLaundering", "Enabled", false);

      return _antimoney_enabled 
               && GeneralParam.GetBoolean("AntiMoneyLaundering", "HasBeneficiary", false)
                                                    && !GeneralParam.GetBoolean("AntiMoneyLaundering", "ExternalControl.Enabled", false);
    } // HasBeneficiaryAndNotAMLExternalControl

    //------------------------------------------------------------------------------
    // PURPOSE : Returns if terminals can use dual currency  
    //
    // PARAMS :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean IsFloorDualCurrencyEnabled()
    {
      return GeneralParam.GetBoolean("FloorDualCurrency", "Enabled", false) && TITO.Utils.IsTitoMode();
    } // IsFloorDualCurrencyEnabled

    //------------------------------------------------------------------------------
    // PURPOSE : Returns if MachineAndGameReport has been to shown
    //
    // PARAMS :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean ShowMachineAndGameReport()
    {
      return !GeneralParam.GetBoolean("WigosGUI", "HideMachineAndGameReport", true) && !WSI.Common.Misc.IsCashlessMode() && !IsFloorDualCurrencyEnabled();
    } // HideMachineAndGameReport

    //------------------------------------------------------------------------------
    // PURPOSE : Xml to DataSet
    //
    //  PARAMS :
    //      - INPUT :
    //          - String Base64
    //          - DataSetRawFormat Format
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public static DataSet DataSetFromBase64(String Base64, DataSetRawFormat Format)
    {
      MemoryStream _ms_inflated;
      MemoryStream _ms_deflated;
      DeflateStream _df;
      Byte[] _aux;
      Int32 _num_read;
      DataSet _ds;

      if (String.IsNullOrEmpty(Base64))
      {
        return null;
      }

      _ds = null;

      _aux = Convert.FromBase64String(Base64);

      _ms_inflated = new MemoryStream();
      if (Format == DataSetRawFormat.XML_ZLIB_BASE64)
      {
        // Skip the Zlib header (2 bytes)
        // Lenght without Zlib header (2) and CRC (4).
        _ms_deflated = new MemoryStream(_aux, 2, _aux.Length - (2 + 4));
      }
      else
      {
        _ms_deflated = new MemoryStream(_aux);
      }

      _df = new DeflateStream(_ms_deflated, CompressionMode.Decompress, true);

      _aux = new Byte[8 * 1024];

      do
      {
        _num_read = _df.Read(_aux, 0, _aux.Length);
        _ms_inflated.Write(_aux, 0, _num_read);

      } while (_num_read > 0);

      _ms_inflated.Position = 0;

      switch (Format)
      {
        case DataSetRawFormat.BIN_DEFLATE_BASE64:
          {
            BinaryFormatter _formatter;

            _formatter = new BinaryFormatter();
            _ds = (DataSet)_formatter.Deserialize(_ms_inflated);
          }
          break;

        case DataSetRawFormat.XML_ZLIB_BASE64:
          {
            _ds = new DataSet();
            _ds.ReadXml(_ms_inflated);
          }
          break;
      }

      _df.Dispose();
      _ms_deflated.Dispose();
      _ms_inflated.Dispose();

      return _ds;
    } // DataSetFromBase64

    //------------------------------------------------------------------------------
    // PURPOSE : Sort Datatable columns by list of column names
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable DataTable to sort
    //          - List of column names
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public static void SortDataTableColumns(ref DataTable Table, String[] ColumnNames)
    {
      UInt16 _col_idx;

      for (_col_idx = 0; _col_idx < ColumnNames.Length; _col_idx++)
      {
        Table.Columns[ColumnNames[_col_idx]].SetOrdinal(_col_idx);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Write a String to a file
    //
    //  PARAMS :
    //      - INPUT :
    //          - String Filename
    //          - String TextToWrite
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean WriteStringToFile(String Filename, String TextToWrite)
    {
      try
      {
        using (StreamWriter _file_writer = new StreamWriter(Filename))
        {
        _file_writer.BaseStream.Seek(0, SeekOrigin.Begin);
        _file_writer.Write(TextToWrite);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("WriteStringToFile. Error writing file: " + Filename + ".");
        Log.Exception(_ex);

        return false;
      }
      }

    public static class ObjectManager
    {
      private static Hashtable m_ht = new Hashtable();

      public static void Register(String Name, DataSet DataSet)
      {
        lock (m_ht)
        {
          if (m_ht.Contains(Name))
          {
            m_ht.Remove(Name);
          }

          m_ht.Add(Name, DataSet);
        }
      } // Register

      public static void Unregister(String Name)
      {
        lock (m_ht)
        {
          if (m_ht.Contains(Name))
          {
            m_ht.Remove(Name);
          }
        }
      } // Unregister

      public static DataSet GetObject(String Name, int TableIndex, int RowIndex, int NumRows)
      {
        lock (m_ht)
        {
          if (m_ht.Contains(Name))
          {
            DataSet _ds;
            DataTable _dt;
            int _idx0;
            int _idx1;
            _ds = (DataSet)m_ht[Name];

            _dt = _ds.Tables[TableIndex];

            _dt.AcceptChanges();
            _idx0 = Math.Min(RowIndex, _dt.Rows.Count);
            _idx1 = Math.Min(_idx0 + NumRows, _dt.Rows.Count);
            for (int _idx = _idx0; _idx < _idx1; _idx++)
            {
              _dt.Rows[_idx].SetModified();
            }
            _dt.AcceptChanges();
            _dt = _dt.GetChanges(DataRowState.Modified);
            _ds = new DataSet();
            _ds.Tables.Add(_dt);

            return _ds;
          }
        }

        return null;
      } // GetObject
    } // ObjectManager

    //------------------------------------------------------------------------------
    // PURPOSE : Calculate the age from birth date
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime Birthdate
    //          - String TextToWrite
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Int32 CalculateAge(DateTime Birthdate)
    {
      Int32 _age;
      DateTime _now;

      _now = WGDB.Now;

      // Get the difference in years.
      _age = _now.Year - Birthdate.Year;
      // Subtract another year if we're before the birth day in the current year.
      if (Birthdate > _now.AddYears(-_age))
      {
        _age--;
      }

      return _age;
    } // CalculateAge

    //------------------------------------------------------------------------------
    // PURPOSE : Check if is today is the Client birth day
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime Birthdate
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean: True. Today is birth day
    //                 False. Today isn't birth day
    public static Boolean IsClientBirthday(DateTime Today, DateTime Birthday)
    {
      // If your birthday is 29/02 and the current year is leap, 
      //          we must consider birth as if it was the 28/02
      if (Birthday.Day == 29 && Birthday.Month == 2 && !DateTime.IsLeapYear(Today.Year))
      {
        Birthday = new DateTime(Birthday.Year, Birthday.Month, 28);
      }

      return (Today.Month == Birthday.Month && Today.Day == Birthday.Day);

    } // IsClientBirthday

    //------------------------------------------------------------------------------
    // PURPOSE : Return the SQL Filter of witholders the level account mask promotion
    //                    
    //  PARAMS :
    //      - INPUT :
    //          - ColumnName: Name of column is filtered, usually [AC_HOLDER_LEVEL]
    //
    // RETURNS :
    //      - String:  
    //
    public static String LevelFilter(Int32 LevelMask, String ColumnName)
    {
      String _filter;

      _filter = "";

      // Any level allowed 
      if (LevelMask == 0)
      {
        //LevelMask = 31; //Binary: 11111 Decimal = 31
        return " " + ColumnName + " = 0 " + " OR " + ColumnName + " = 1 " + " OR " + ColumnName + " = 2 " + " OR " + ColumnName + " = 3 " + " OR " + ColumnName + " = 4 ";
      }

      for (int AcctLevel = 0; AcctLevel < 5; AcctLevel++)
      {
        if ((LevelMask & (1 << AcctLevel)) != 0)
        {
          if (_filter.Length != 0)
          {
            _filter += " OR";
          }
          _filter += " " + ColumnName + " = " + AcctLevel;
        }
      }

      return _filter;
    } // LevelFilter

    //------------------------------------------------------------------------------
    // PURPOSE: Checks whether provided account holder level is allowed in the promotion
    // 
    //  PARAMS:
    //      - INPUT:
    //        - AcctLevel
    //        - LevelMask
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - true: account holder level is included
    //    - false: account holder level is not included
    public static Boolean IsLevelAllowed(Int32 AcctLevel, Int32 LevelMask)
    {
      if (LevelMask == 0)
      {
        // Any level allowed
        return true;
      }

      return (LevelMask & (1 << AcctLevel)) != 0;
    } // IsLevelAllowed

    //------------------------------------------------------------------------------
    // PURPOSE: Checks whether provided account gender is allowed in the promotion
    // 
    //  PARAMS:
    //      - INPUT:
    //        - AcctLevel
    //        - LevelMask
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - true: account holder gender is included
    //    - false: account holder gender is not included
    public static Boolean IsGenderAllowed(Int32 AccountGenderFilter, DrawGenderFilter DrawGenderFilter)
    {
      switch (DrawGenderFilter)
      {
        case DrawGenderFilter.ALL:
          return true;

        case DrawGenderFilter.MEN_ONLY:
          if (AccountGenderFilter != 1)
          {
            return false;
          }
          break;

        case DrawGenderFilter.WOMEN_ONLY:
          if (AccountGenderFilter != 2)
          {
            return false;
          }
          break;
      }
      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Checks whether provided if is VIP account is allowed in the promotion/draws/gift
    // 

    //  PARAMS:
    //      - INPUT:
    //        - IsVIPAccount
    //        - VIPMask
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - true: if promotion/draws/gift is applicable
    //    - false: account holder gender is not included
    public static Boolean IsVIP(Boolean IsVIPAccount, Boolean VIPMask)
    {
      if (!VIPMask)
      {
        return true;
      }

      return VIPMask && IsVIPAccount;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get gu_user_type for promobox
    //
    // RETURNS:
    //      - GU_USER_TYPE
    //
    public static GU_USER_TYPE GetUserTypePromobox()
    {
      if (TITO.Utils.IsTitoMode())
      {

        return GU_USER_TYPE.SYS_SYSTEM;
      }

      return GU_USER_TYPE.SYS_PROMOBOX;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get the SQL date filter to insert on a query
    // 
    //  PARAMS:
    //      - INPUT:
    //          - FieldName: Referenced date column name
    //          - DateFrom: String date from; if "" is omited in where
    //          - DateTo: String date to; if "" is omited in where
    //          - BeginWhere: If true result begin with WHERE else begin with AND
    //
    //      - OUTPUT:
    //
    // RETURNS: A SQL WHERE clause to filter a date field; If not dates returns "".
    public static String DateSQLFilter(String FieldName, String DBDateFrom, String DBDateTo, Boolean BeginWhere)
    {
      String _date_where;
      String _from_part;
      String _to_part;

      _date_where = "";
      _from_part = "";
      _to_part = "";

      if (DBDateFrom != "")
      {
        _from_part = FieldName + " >= " + DBDateFrom;
      }

      if (DBDateTo != "")
      {
        if (_from_part != "")
        {
          _to_part = " AND ";
        }
        _to_part = _to_part + FieldName + " < " + DBDateTo;
      }

      if (_from_part != "" || _to_part != "")
      {
        _date_where = (BeginWhere ? " WHERE " : " AND ") + _from_part + _to_part;
      }

      return _date_where;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Register component
    // 
    //  PARAMS:
    //      - INPUT:
    //          - OcxFile: File that will be register
    //
    //      - OUTPUT:
    //
    // RETURNS: True if register has ended in less of one second, false otherwise.
    public static Boolean InstallExternalComponent(String OcxFile)
    {
      String _str_dir;
      Process _pr;

      try
      {
        //TODO: - Check if you have completed the registration
        //      - Change  the function to pass as parameter the path
        _str_dir = " /s " + (Char)(34) + System.IO.Path.Combine(Environment.CurrentDirectory, OcxFile) + (Char)(34);
        _pr = Process.Start("regsvr32", _str_dir);
        _pr.WaitForExit(5000);

        if (_pr.HasExited)
        {
          return true;
        }

        Log.Warning("Not installed, file name: " + OcxFile);
      }
      catch (Exception _ex)
      {
        Log.Error("InstallExternalComponent. Not installed ocx, file name: " + OcxFile);
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get system mode
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - SYSTEM_MODE
    //
    public static SYSTEM_MODE SystemMode()
    {
      Int32 _gp_tito_mode;

      _gp_tito_mode = GeneralParam.GetInt32("Site", "SystemMode", -1, 0, 4);

      //ETP 20-OCT-2015 Added compativility for old version.
      if (_gp_tito_mode < 0)
      {
        _gp_tito_mode = GeneralParam.GetInt32("TITO", "TITOMode", 0, 0, 2);
      }

      // If Cage is disabled --> TITO mode is changed by Cashless mode.
      if (_gp_tito_mode == 1 && !Cage.IsCageEnabled())
      {
        _gp_tito_mode = 0;
      }

      return (SYSTEM_MODE)_gp_tito_mode;

    } // SystemMode

    //------------------------------------------------------------------------------
    // PURPOSE : Get if system mode is Cashless
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean IsCashlessMode()
    {
      return (Misc.SystemMode() == SYSTEM_MODE.CASHLESS);
    } // IsCashlessMode

    //------------------------------------------------------------------------------
    // PURPOSE : Get if system mode is Mico2
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean IsMico2Mode()
    {
      return (Misc.SystemMode() == SYSTEM_MODE.MICO2);
    } // IsMico2Mode

    //------------------------------------------------------------------------------
    // PURPOSE : Get if system mode is GamingHall
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean IsGamingHallMode()
    {
      return (Misc.SystemMode() == SYSTEM_MODE.GAMINGHALL);
    } // IsGamingHallMode

    //------------------------------------------------------------------------------
    // PURPOSE : Get if system mode is Wass
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean IsWassMode()
    {
      return (Misc.SystemMode() == SYSTEM_MODE.WASS);
    } // IsWassMode

    //------------------------------------------------------------------------------
    // PURPOSE : Get if is AGG enabled
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean IsAGGEnabled()
    {
      return GeneralParam.GetBoolean("AGG", "Buffering.Enabled", false);
    } // IsAGGEnabled

    /// <summary>
    /// Check if the form has been hide
    /// </summary>
    /// <returns></returns>
    public static Boolean IsHideTerminalsTimeDisconnected()
    {
      return GeneralParam.GetBoolean("WigosGUI", "HideTerminalsTimeDisconnected", true);
    } // IsHideTerminalsTimeDisconnected

    //------------------------------------------------------------------------------
    // PURPOSE : Is part of a Multisite group (Center or Site)
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean IsPartOfAMultisiteGroup()
    {
      Boolean _multisite_is_center;
      Boolean _site_multisite_site_member;

      _multisite_is_center = IsCenter();
      _site_multisite_site_member = IsMultiSiteMember();

      return _multisite_is_center || _site_multisite_site_member;

    } // IsPartOfAMultisiteGroup

    public static Boolean IsCenter()
    {
      return GeneralParam.GetBoolean("MultiSite", "IsCenter", false);
    }

    public static Boolean IsMultiSiteMember()
    {
      return GeneralParam.GetBoolean("MultiSite", "MultiSiteMember", false);
    }

    public static String SiteName()
    {
      return GeneralParam.GetString("Site", "Name");
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get enabled value for SmartFloor
    //
    // RETURNS:
    //      - Boolean: SmartFloor enabled.
    //
    public static Boolean IsIntelliaEnabled()
    {
      return GeneralParam.GetBoolean("Intellia", "Enabled", false);
    }

    public static Boolean IsRaffleBlankMinFieldsAllowed()
    {
      return GeneralParam.GetBoolean("Cashier.DrawTicket", "AllowBlankSpentPlayed", false);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get enabled value for reception
    //
    // RETURNS:
    //      - Boolean: Reception enabled.
    //
    public static Boolean IsReceptionEnabled()
    {
      return GeneralParam.GetBoolean("Reception", "Enabled", false);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get enabled value for reception
    //
    // RETURNS:
    //      - Boolean: Reception enabled.
    //
    public static Int32 GetReceptionCashierOpenAutomaticallyMode()
    {
      return GeneralParam.GetInt32("Reception", "OpenCashierAutomatically", 0);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get enabled value for CountR Terminals
    //
    // RETURNS:
    //      - Boolean: CountR enabled.
    //
    public static Boolean IsCountREnabled()
    {
      return TITO.Utils.IsTitoMode() && GeneralParam.GetBoolean("CountR", "Enabled", false);
    }

    /// <summary>
    /// Get enabled value for PinPad Terminals.
    /// </summary>
    /// <returns>PinPad Enabled</returns>
    public static Boolean IsPinPadEnabled()
    {
      return GeneralParam.GetBoolean("PinPad", "Enabled", false);
    }

    /// <summary>
    /// Detects if framework 4 or 4.x is installed
    /// </summary>
    /// <returns>True if framework 4 or 4.x is installed</returns>
    public static Boolean IsFramework4Installed()
    {
      // Opens the registry key for the .NET Framework entry.
      using (RegistryKey ndpKey =
          RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, "").
          OpenSubKey(@"SOFTWARE\Microsoft\NET Framework Setup\NDP\"))
      {
        // As an alternative, if you know the computers you will query are running .NET Framework 4.5 
        // or later, you can use:
        // using (RegistryKey ndpKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, 
        // RegistryView.Registry32).OpenSubKey(@"SOFTWARE\Microsoft\NET Framework Setup\NDP\"))
        foreach (string versionKeyName in ndpKey.GetSubKeyNames())
        {
          if (versionKeyName == "v4" || versionKeyName == "v4.0")
          {
            return true;
          }
        }
      }

      return false;
    } //IsFramework4Installed

    /// <summary>
    /// Get enabled value for CDFI WebService.
    /// </summary>
    public static Boolean IsCDFIEnabled()
    {
      return GeneralParam.GetBoolean("CFDI", "Enabled", false);
    } // IsCDFIEnabled

    /// <summary>
    /// Get Active value for CDFI WebService.
    /// </summary>
    public static Boolean IsCDFIActive()
    {
      return GeneralParam.GetBoolean("CFDI", "Active", false);
    } // IsCDFIActive

    public static String GetControlNumberBySequence(String HexSequence, Int64 Try = 0)
    {
      String _control_number;

      _control_number = GeneralParam.GetString("PinPad", "ControlNumber", "{0}");

      _control_number = String.Format(_control_number, HexSequence);

      _control_number = _control_number + (Try == 0 ? "" : "_" + Try);

      return _control_number;
    }
    /// <summary>Bank card undo mode value:
    ///          0: Amount is subtracted from card concept
    ///          1: Amount is subtracted from amount concept
    /// </summary>

    public enum BankCardUndoMode
    {
      FROM_CARD_CONCEPT = 0,
      FROM_AMOUNT_CONCEPT = 1
    }

    /// <summary>
    /// Get mode of bank card undo
    /// </summary>
    /// <returns>Bank card undo mode value
    /// </returns>
    public static BankCardUndoMode GetBankCardUndoMode()
    {
      return (BankCardUndoMode)GeneralParam.GetInt32("Cashier.Undo", "BankCard.Undo.Mode");
    }

    //LTC 05-AGO-2016
    //------------------------------------------------------------------------------
    // PURPOSE: check if Cards Withdrawal is enabled
    //
    // RETURNS:
    //      - Boolean: Cards Withdrawal enabled.
    //
    public static Boolean IsBankCardWithdrawalEnabled()
    {
      return GeneralParam.GetBoolean("Cashier.Withdrawal", "BankCard.Conciliate", false) && IsBankCardCloseCashEnabled();
    }

    /// <summary>
    /// Returns if the BankCard.Conciliate
    /// </summary>
    /// <returns></returns>
    public static Boolean IsBankCardConciliateOnCloseCash()
    {
      return (GeneralParam.GetBoolean("Cashier.CloseCash", "BankCard.Conciliate", false));
    }

    //LTC 05-AGO-2016
    //------------------------------------------------------------------------------
    // PURPOSE: check if Cards Withdrawal is enabled
    //
    // RETURNS:
    //      - Boolean: Cards Withdrawal enabled.
    //
    public static Boolean IsBankCardCloseCashEnabled()
    {
      return (IsBankCardConciliateOnCloseCash() && IsPinPadEnabled() && (Cashier.ReadPinPadEnabled() || IsGUIRquesting));      // LTC 2017-JUL-12
    }

    /// <summary>
    /// Has Terminal the p�npad enabled?
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <returns></returns>
    public static Boolean HasTerminalPinPadEnabled(Int32 TerminalId)
    {
      PinPadCashierTerminals _pinpad_cashier_terminals;

      _pinpad_cashier_terminals = new PinPadCashierTerminals();
      _pinpad_cashier_terminals.CashierId = TerminalId;

      return _pinpad_cashier_terminals.DB_CheckSomeEnabledByCashierId();
    }  // HasTerminalPinPadEnabled

    /// <summary>
    /// Has to hide shorts or overs on bank card with pinpad
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <returns></returns>
    public static Boolean HasToHideShortOrOverOnBankCardWithPinpadAndConciliation(Int32 TerminalId)
    {
      return (IsBankCardConciliateOnCloseCash() && IsPinPadEnabled() && HasTerminalPinPadEnabled(TerminalId));
    }

    /// <summary>
    /// Has to hide shorts or overs on bank card with pinpad
    /// </summary>
    /// <param name="ParametersOverview"></param>
    /// <returns></returns>
    public static Boolean HasToHideShortOrOverOnBankCardWithPinpadAndConciliation(VoucherCashDeskOverview.ParametersCashDeskOverview ParametersOverview)
    {
      Int32 _terminal_id;

      if (ParametersOverview == null)
      {
        return true;
      }

      //Get terminal id
      _terminal_id = GetTerminalIdFromCashierSession(ParametersOverview);

      return Misc.HasToHideShortOrOverOnBankCardWithPinpadAndConciliation(_terminal_id);
    }

    /// <summary>
    /// Returns the terminal id from the cashier session
    /// </summary>
    /// <param name="ParametersOverview"></param>
    /// <returns></returns>
    private static Int32 GetTerminalIdFromCashierSession(VoucherCashDeskOverview.ParametersCashDeskOverview ParametersOverview)
    {
      CashierSessionInfo _cashier_session_info;
      Int32 _terminal_id;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        //Get cashier session info
        Cashier.GetCashierSessionById(ParametersOverview.CashierSessionId, out _cashier_session_info, _db_trx.SqlTransaction);
      }

      //Get terminal from cashier session info
      _terminal_id = _cashier_session_info.TerminalId;

      return _terminal_id;
    }

    //LTC 06-SEP-2016
    //------------------------------------------------------------------------------
    // PURPOSE: Indicate if is a close cash operation
    //
    // RETURNS:
    //      - Boolean: Close cash operation.
    //
    public static Boolean PrintBankCardSection = false;

    //ESE 27-SEP-2016
    //------------------------------------------------------------------------------
    // PURPOSE: Indicate if is a status cash operation
    //
    // RETURNS:
    //      - Boolean: Status cash operation.
    //
    public static Boolean PrintBankCardSection_StatusCash = false;

    //LTC 12-JUL-2017
    //------------------------------------------------------------------------------
    // PURPOSE: Indicate if is GUI requesting cashier resume
    //
    // RETURNS:
    //      - Boolean: If is GUI requesting
    //
    public static Boolean IsGUIRquesting = false;

    /// <summary>
    /// Registers com dll 
    /// </summary>
    /// <param name="File">Dll file name</param>
    /// <param name="Class">Class name</param>
    /// <returns></returns>
    public static Boolean RegisterComDll(String File, String Class, Boolean RegisterAlways)
    {
      try
      {

        String _path_dll;
        _path_dll = AppDomain.CurrentDomain.BaseDirectory + File;
        Type T = Type.GetTypeFromProgID(Class);

        if (RegisterAlways || T == null)
        {
          String _result = RunShellCommand(@"%SystemRoot%\Microsoft.NET\Framework\v4.0.30319\regasm.exe /codebase """ + _path_dll + @"""");

          if (_result.Contains("successfully") || _result.Contains("correctamente"))
          {
            Log.Message(string.Format("The '{0}' was registered successfully.", File));
            return true;
          }
          else
          {
            Log.Error(string.Format("Error registering {0}. {1}", File, _result));
            return false;
          }
        }
        else
        {
          //// The DLL already registered previously
          //Log.Message(string.Format("The {0} already registered previously", T.FullName));
          return true;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        return false;
      }
    }

    /// <summary>
    /// Runs a cmd command without showing console.
    /// </summary>
    /// <param name="command">Command </param>
    /// <returns></returns>
    public static String RunShellCommand(String command)
    {
      System.Diagnostics.ProcessStartInfo procStartInfo = new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command);

      procStartInfo.RedirectStandardOutput = true;
      procStartInfo.UseShellExecute = false;
      procStartInfo.CreateNoWindow = true;

      procStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
      System.Diagnostics.Process proc = new System.Diagnostics.Process();
      proc.StartInfo = procStartInfo;
      proc.Start();

      String result = proc.StandardOutput.ReadToEnd();
      return result;
    }

    private static bool? m_is_ws2s_enabled = null;

    public static bool IsWS2SEnabled()
    {
      if (!m_is_ws2s_enabled.HasValue)
      {
        m_is_ws2s_enabled = GeneralParam.GetBoolean("WS2S", "Listener.Enabled", false);
      }

      return m_is_ws2s_enabled.Value;
    }

    public static bool IsWS2SFBMEnabled()
    {
      return !String.IsNullOrEmpty(GeneralParam.GetString("WS2S", "VendorId", String.Empty));
    }

    /// <summary>
    /// Check if OpenCloseLoop is enabled (OpenCloseLoop = C2Go)
    /// </summary>
    /// <returns></returns>
    public static bool IsOpenCloseLoopEnabled()
    {
      return GeneralParam.GetBoolean("OpenCloseLoop", "Enabled", false);
    }

    /// <summary>
    /// Returns if the Exped is enabled
    /// </summary>
    /// <returns></returns>
    public static bool IsExpedEnabled()
    {
      return (ExternalPaymentAndSaleValidationCommon.ExternalPaymentValidationEnabled());
    }

    public static bool IsAccRequiredByCurrency(CurrencyExchange Exchange, Currency? Amount)
    {
      String _gp_currency;
      String _gp_type;
      String _iso_code;
      CurrencyExchangeType _currency_type;

      try
      {
        _iso_code = Exchange == null ? CurrencyExchange.GetNationalCurrency() : Exchange.CurrencyCode;
        _currency_type = Exchange == null ? CurrencyExchangeType.CURRENCY : Exchange.Type;

        if (_iso_code == CurrencyExchange.GetNationalCurrency())
        {
          _gp_currency = "NC";
        }
        else
        {
          _gp_currency = "FC";
        }

        switch (_currency_type)
        {
          case CurrencyExchangeType.CURRENCY:
            _gp_type = "Cash";
            break;
          case CurrencyExchangeType.CARD:
            _gp_type = "Card";
            break;
          case CurrencyExchangeType.CHECK:
            _gp_type = "Check";
            break;
          default:
            _gp_type = "Cash";
            break;
        }

        return Amount >= GeneralParam.GetInt64("IdentifyCustomer", _gp_currency + "." + _gp_type + ".GreatherThan", 999999999999);
      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);
      }

      return false;
    }

    /// <summary>
    /// Is FB enabled?
    /// </summary>
    /// <returns></returns>
    public static Boolean IsFBEnabled()
    {
      return GeneralParam.GetBoolean("FB", "Enabled", false);
    } // IsFBEnabled

    public static int DefaultDocumentTypeId()
    {
      return GeneralParam.GetInt32("Account.DefaultValues", "DocumentType", -1);
    }

    // XGJ 27-MAR-2017
    public static bool IsFeatureTerminalDrawEnabled()
    {
      return GeneralParam.GetBoolean("Features", "CashDesk.Draw.02", false);
    }
    public static bool IsTerminalDrawEnabled()
    {
      return GeneralParam.GetBoolean("CashDesk.Draw.02", "Enabled", false);
    }

    public static Boolean IsPromoGamesEnabled()
    {
      return IsTerminalDrawEnabled() && GeneralParam.GetBoolean("DisplayTouch", "Games.Enabled", false);
    }
    public static bool IsTableDrawEnabled()
    {
      return GeneralParam.GetBoolean("CashDesk.Draw.01", "Enabled", false);
    }
    public static bool IsCashDeskDrawEnabled()
    {
      return GeneralParam.GetBoolean("CashDesk.Draw", "Enabled", false);
    }
    public static bool IsInTouchGamesEnabled()
    {
      return GeneralParam.GetBoolean("DisplayTouch", "Games.Enabled", false);
    }
    public static string GetLocalIpAddress()
    {
      string _ipaddr = ConfigurationFile.GetSetting(LOCAL_IP_CONF_SETTING);

      if (IsAValidIP(_ipaddr))
      {
        return _ipaddr;
      }

      var _host = Dns.GetHostEntry(Dns.GetHostName());
      IPAddress[] _internetwork_addresses = Array.FindAll(_host.AddressList,
        _ip => _ip.AddressFamily == AddressFamily.InterNetwork);
      return _internetwork_addresses.Length > 0 ? _internetwork_addresses[0].ToString() : EMPTY_IP;
    }

    /// <summary>
    /// Is a valid IP?
    /// </summary>
    /// <param name="IP"></param>
    /// <returns></returns>
    public static Boolean IsAValidIP(String IP)
    {
      if (String.IsNullOrEmpty(IP))
      {
        return false;
      }

      if (IP.Equals(EMPTY_IP))
      {
        return false;
      }

      return true;
    }

    public static string ReplaceUrlParam(string Url)
    {
      return Url.Contains("@WCP") ? Url.Replace("@WCP", GetLocalIpAddress()) : Url;
    }
    // ATB 16-FEB-2017
    /// <summary>
    /// Checks if the WriteLog.Message general param is active
    /// </summary>
    /// <returns>General param's value</returns>
    private static bool IsWriteLogEnabled()
    {
      if (!WGDB.Initialized)
      {
        return false;
      }

      return GeneralParam.GetBoolean("Log", "WriteLog.Message", false);
    }
    // ATB 16-FEB-2017
    /// <summary>
    /// Writes a log, supported types are Error, Message and Warning
    /// </summary>
    /// <param name="text">Log message to write</param>
    /// <param name="logType">Log type, in enum type format</param>
    public static void WriteLog(string text, Log.Type logType)
    {
      switch (logType)
      {
        case Log.Type.Error:
          Log.Error(text);
          break;
        case Log.Type.Message:
          if (IsWriteLogEnabled())
          {
            Log.Message(text);
          }
          break;
        case Log.Type.Warning:
          Log.Warning(text);
          break;
        default:
          if (IsWriteLogEnabled())
          {
            Log.Message(text);
          }
          break;
      }
    }

    /// <summary>
    /// Returns the terminal reservation status
    /// </summary>
    /// <returns></returns>
    public static bool IsTerminalReservationEnabled()
    {
      return GeneralParam.GetBoolean("Terminal", "Reservation.Enabled", false);
    }


    public static class ExternalWS
    {
      #region Exped

      public static ExternalValidationInputParameters InputParams;
      public static ExternalValidationOutputParameters OutputParams;
      public static Boolean ExpedRequestAuthorization(Int64 AccountId, Currency NetAmount, OperationCode OpCode)
      {
        ExternalPaymentAndSaleValidation_Exped expedWS;
        InputParams = new ExternalValidationInputParameters(AccountId
                                                            , NetAmount            //NetAmount
                                                            , 0                 //GrossAmount
                                                            , 0 //OperationId
                                                            , OpCode);
        OutputParams = new ExternalValidationOutputParameters();
        expedWS = new ExternalPaymentAndSaleValidation_Exped();
        return expedWS.RequestAuthorization(InputParams, out OutputParams);
      }

      #endregion
    }

    public static List<MaritalStatus> MaritalStatusList()
    {
      return new List<MaritalStatus>
    {
        new MaritalStatus {Id = 0, Name = ""},
        new MaritalStatus {Id = 1, Name = Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_SINGLE")},
        new MaritalStatus {Id = 2, Name = Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_MARRIED")},
        new MaritalStatus {Id = 3, Name = Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_DIVORCED")},
        new MaritalStatus {Id = 4, Name = Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_WIDOWED")},
        new MaritalStatus {Id = 5, Name = Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_UNMARRIED")},
        new MaritalStatus {Id = 6, Name = Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_OTHER")},
        new MaritalStatus {Id = 7, Name = Resource.String("STR_FRM_PLAYER_TRACKING_CB_MARITAL_UNSPECIFIED")}
      };
    }

    public static List<Gender> GenderList()
    {
      return new List<Gender>
      {
          new Gender {Id = 0, Name = ""},
          new Gender {Id = 1, Name = Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_MALE")},
          new Gender {Id = 2, Name = Resource.String("STR_FRM_PLAYER_TRACKING_CB_GENDER_FEMALE")}
        };
      }

    /// <summary>
    /// Convert objects list to datatable
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="data"></param>
    /// <returns></returns>
    public static DataTable ListToDataTable<T>(this IList<T> data)
    {
      PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
      DataTable table = new DataTable();
      foreach (PropertyDescriptor prop in properties)
        table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
      foreach (T item in data)
      {
        DataRow row = table.NewRow();
        foreach (PropertyDescriptor prop in properties)
          row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
        table.Rows.Add(row);
    }
      return table;
    } // ToDataTable

  } // Misc

  public static class FlagsHelper
{
    public static bool IsSet<T>(T flags, T flag) where T : struct
    {
        int flagsValue = (int)(object)flags;
        int flagValue = (int)(object)flag;

        return (flagsValue & flagValue) != 0;
    } // IsSet

    public static void Set<T>(ref T flags, T flag) where T : struct
    {
        int flagsValue = (int)(object)flags;
        int flagValue = (int)(object)flag;

        flags = (T)(object)(flagsValue | flagValue);
    } // Set

    public static void Unset<T>(ref T flags, T flag) where T : struct
    {
        int flagsValue = (int)(object)flags;
        int flagValue = (int)(object)flag;

        flags = (T)(object)(flagsValue & (~flagValue));
    } // Unset
} // FlagsHelper

  public static class Development
  {
    private static Boolean m_init = false;

    public static Boolean Enabled
    {
      get
      {
        Boolean _development;

        _development = (Environment.GetEnvironmentVariable("LKS_VC_DEV") != null);

        if (!m_init)
        {
          if (_development)
          {
            Log.Message("Development variable defined!");
          }
          m_init = true;
        }

        return _development;
      }
    } // Enabled

    public static void DumpVariables()
    {
      IDictionary _dictionary;
      StringBuilder _sb;
      String _value;
      String[] _keys;

      _sb = new StringBuilder();

      _dictionary = Environment.GetEnvironmentVariables();
      _keys = new String[_dictionary.Keys.Count];
      _dictionary.Keys.CopyTo(_keys, 0);
      Array.Sort(_keys);

      _sb.AppendLine();
      _sb.AppendLine("<ENVIRONMENT>");

      String _tab;
      _tab = "    ";
      foreach (String _key in _keys)
      {
        _value = (String)_dictionary[_key];
        _sb.AppendLine(_tab + _key + "=" + _value);
      }
      _sb.AppendLine("</ENVIRONMENT>");

      Log.Message(_sb.ToString());
    } // DumpVariables


    private static Int32 GetDevScreen(String EnvironmentVariableName)
    {
      String _env_var_dev_monitor;
      Int32 _dev_monitor;

      _dev_monitor = 0;
      _env_var_dev_monitor = Environment.GetEnvironmentVariable(EnvironmentVariableName);

      if (!String.IsNullOrEmpty(_env_var_dev_monitor))
      {
        if (!Int32.TryParse(_env_var_dev_monitor, out _dev_monitor))
        {
          _dev_monitor = 0;
        }
      }

      return _dev_monitor;
    }

    /// <summary>
    /// Returns the screen to show the GUI
    /// </summary>
    /// <returns></returns>
    public static Int32 GetDevMonitorGUI()
    {
      return GetDevScreen("LKS_VC_DEV_SCREEN_GUI");
    }

    /// <summary>
    /// Returns the screen to show the Cashier
    /// </summary>
    /// <returns></returns>
    public static Int32 GetDevMonitorCashier()
    {
      return GetDevScreen("LKS_VC_DEV_SCREEN_CASHIER");
    }

    /// <summary>
    /// Sets the form on the Screen
    /// </summary>
    /// <param name="FormToRelocate"></param>
    /// <param name="ShowOnMonitorNumber"></param>
    public static void SetFormOnMonitor(Form FormToRelocate, int ShowOnMonitorNumber)
    {
      Screen[] _screens_list;
      Boolean _form_was_maximised;

      if (FormToRelocate == null)
      {
        return;
      }

      _screens_list = Screen.AllScreens;

      if ((ShowOnMonitorNumber < 0) || (ShowOnMonitorNumber >= _screens_list.Length))
      {
        ShowOnMonitorNumber = 0;
      }

      _form_was_maximised = false;
      if (FormToRelocate.WindowState == FormWindowState.Maximized)
      {
        FormToRelocate.WindowState = FormWindowState.Normal;
        _form_was_maximised = true;
      }

      FormToRelocate.StartPosition = FormStartPosition.Manual;
      FormToRelocate.Location = _screens_list[ShowOnMonitorNumber].WorkingArea.Location;

      if (_form_was_maximised)
      {
        FormToRelocate.WindowState = FormWindowState.Maximized;
      }
    }
  } // Development

} // WSI.Common
