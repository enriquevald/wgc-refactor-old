﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: WCPPlayerRequest.cs
// 
//      DESCRIPTION: Class to process the player request gift
// 
//           AUTHOR: Ferran Ortner
// 
//    CREATION DATE: 31-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-OCT-2017 FOS    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections;
using System.Text;
using System.Xml;

namespace WSI.Common.InTouch
{
  public class WCPPlayerRequest
  {
    #region " Properties "
    public Int64 GiftId { get; set; }
    public String TrackData { get; set; }
    public String PlayerPin { get; set; }
    public Decimal GiftPrice { get; set; }
    public Int64 GiftNumUnits { get; set; }
    public Int64 AccountId { get; set; }
    public String TerminalName { get; set; }
    public Int64 DrawId { get; set; }
    public ArrayList VoucherList { get; set; }
    public Ticket Ticket { get; set; }
    public Boolean TitoPrinterIsReady { get; set; }

    public MultiPromos.PromoBalance PromoBalance = new MultiPromos.PromoBalance();
    #endregion

    #region " Constructor "
    public WCPPlayerRequest()
    {
    } // WCPPlayerRequest

    public WCPPlayerRequest(  Int64 PlayPointGiftId
                         , String AccountTrackData 
                         , String  AccountPin 
                         , Decimal GiftPrice 
                         , Int64 GiftUnits
                         , Int64 AccountId
                         , String TerminalName
                         , Int64 DrawId
                         , ArrayList VoucherList)
    {
      this.GiftId = PlayPointGiftId;
      this.TrackData = AccountTrackData;
      this.PlayerPin = AccountPin;
      this.GiftPrice = GiftPrice;
      this.GiftNumUnits = GiftUnits;
      this.AccountId = AccountId;
      this.TerminalName = TerminalName;
      this.DrawId = DrawId;
      this.VoucherList = VoucherList;
    } // WCPPlayerRequest
    #endregion

    #region " Public funtions "
    public Boolean ProcessPlayerRequestGift(WCPPlayerRequest Player, out ArrayList OutVoucherList, out Ticket OutTickets, out MultiPromos.PromoBalance OutPromo, out Int32 ResponseCode)
    {
      Int64 _gift_id;
      Int64 _draw_id;
      String _trackdata;
      String _pin;
      Decimal _gift_points;
      Int32 _gift_num_units;

      Decimal _player_points;
      Decimal _player_balance;
      ArrayList _vouchers_list;
    
      Int32 _rsp_code;
      Int64 _account_id;

      OutVoucherList = new ArrayList();

      OutTickets = null;

      OutPromo = null;
     
      try
      {
        _gift_id = Player.GiftId;
        Gift.GIFT_MSG _errmsg;
        _trackdata = Player.TrackData;
        _pin = Player.PlayerPin;
        _gift_points = Player.GiftPrice / 100;
        _gift_num_units = (int)Player.GiftNumUnits;

        _draw_id = Player.DrawId;
        //Login Ok?
        if (!new WCPPlayerRequestLogin().WKT_PlayerLogin(_trackdata, _pin, out _account_id, out _rsp_code))
        {
          ResponseCode = _rsp_code;
          return false;
        }

        //Check current time is in schedule
        if (!new WCPPlayerRequestFuncionalitySchedule().WKT_FunctionalitySchedule(PromoBOX.Functionality.SCHEDULE_GIFT_REQUEST))
        {
          ResponseCode = (Int32)WCPPlayerRequestResponseCodes.WCP_RC_WKT_FUNCTIONALITY_OFF_SCHEDULE;
          return false;
        }


        using (DB_TRX _db_trx = new DB_TRX())
        {
          String _terminal_name;
          String _terminal_external_id;
          Int32 _terminal_id;
          TerminalTypes _terminal_type;
          MultiPromos.AccountBalance _balance;
          FlagBucketId _flags_buckets;

          Gift _gift_request;
          Boolean _gp_print_redeemable;
          Boolean _gp_print_no_redeemable;
          Ticket _ticket_tito;


          WSI.Common.Terminal.TerminalInfo _terminal_info;

          //_terminal_external_id = WktRequest.MsgHeader.TerminalId;
          _terminal_external_id = Player.TerminalName;

          // 1 --> WCP_TerminalType.GamingTerminal
          if (!WSI.Common.Terminal.Trx_GetTerminalInfoByExternalId(_terminal_external_id, out _terminal_info, _db_trx.SqlTransaction))
          {
            ResponseCode = (Int32)WCPPlayerRequestResponseCodes.WCP_RC_WKT_ERROR;

            return false;
          }

          _terminal_id = _terminal_info.TerminalId;
          _terminal_name = _terminal_info.Name;
          _terminal_type = _terminal_info.TerminalType;
          _ticket_tito = null;
          //Gift Request
          if (!GiftRequest.ProcessRequestGift(_gift_id, _draw_id, _account_id, _gift_points, _gift_num_units, _terminal_id, _db_trx.SqlTransaction,
                                              out _player_points, out _player_balance, out _vouchers_list, out _errmsg, _terminal_name, _terminal_type, out _ticket_tito))
          {
            ResponseCode =(Int32)GetResponseCode(_errmsg);

            return false;
          }

          if (_ticket_tito != null)
          {
            _ticket_tito.Address1 = WSI.Common.TITO.Utils.GetValueAddres(_terminal_info, 1);
            _ticket_tito.Address2 = WSI.Common.TITO.Utils.GetValueAddres(_terminal_info, 2);
             OutTickets  = _ticket_tito;
          }


          if (_terminal_type == TerminalTypes.PROMOBOX)
          {
            OutVoucherList = _vouchers_list;
            _gift_request = new Gift();

            if (!_gift_request.DB_GiftData(_gift_id, _account_id))
            {
              ResponseCode = (Int32)WCPPlayerRequestResponseCodes.WCP_RC_WKT_TITO_PRINTER_ERROR;

              return false;
            }

            if (WSI.Common.TITO.Utils.IsTitoMode())
            {

              _gp_print_redeemable = _gift_request.Type == GIFT_TYPE.REDEEMABLE_CREDIT
                          && GeneralParam.GetBoolean("WigosKiosk", "Promotions.RE.PrintTITOTicket", false);

              _gp_print_no_redeemable = _gift_request.Type == GIFT_TYPE.NOT_REDEEMABLE_CREDIT
                                     && GeneralParam.GetBoolean("WigosKiosk", "Promotions.NR.PrintTITOTicket", false);

              if (_gp_print_redeemable || _gp_print_no_redeemable)
              {
                if (!Player.TitoPrinterIsReady)
                {
                  ResponseCode = (Int32)WCPPlayerRequestResponseCodes.WCP_RC_WKT_TITO_PRINTER_ERROR;

                  return false;
                }


              }
            }
          }
          _flags_buckets = FlagBucketId.NR;

          if (!WSI.Common.TITO.Utils.IsTitoMode())
          {
            _flags_buckets = _flags_buckets | FlagBucketId.RE;
          }

          if (!MultiPromos.Trx_UpdateAccountBalance(_account_id, MultiPromos.AccountBalance.Zero, 0, _flags_buckets, out _balance, out _player_points, _db_trx.SqlTransaction))
          {
            ResponseCode = (Int32)WCPPlayerRequestResponseCodes.WCP_RC_WKT_ERROR;

            return false;
          }

          _db_trx.Commit();

          _balance = CurrencyExchange.GetExchange(_balance, CurrencyExchange.GetNationalCurrency(), _terminal_info.CurrencyCode, _db_trx.SqlTransaction);

          OutPromo = new MultiPromos.PromoBalance(_balance, _player_points);

          ResponseCode = (Int32)WCPPlayerRequestResponseCodes.WCP_RC_WKT_OK;
          
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        ResponseCode = (Int32)WCPPlayerRequestResponseCodes.WCP_RC_WKT_ERROR;
      }

      return false;
    } // ProcessPlayerRequestGift

    private static WCPPlayerRequestResponseCodes GetResponseCode(Gift.GIFT_MSG _errmsg)
    {
      String _msg = "WKT_";
      WCPPlayerRequestResponseCodes _response;

      _response = WCPPlayerRequestResponseCodes.WCP_RC_ERROR;
      try
      {
        _msg += _errmsg.ToString();
        _response = (WCPPlayerRequestResponseCodes)Enum.Parse(typeof(WCPPlayerRequestResponseCodes), _msg);

      }
      catch (Exception _ex)
      {
      
        Log.Exception(_ex);
      }

      return _response;
    }
   #endregion
  }
}
