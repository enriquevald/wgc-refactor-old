//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MachineMeters.cs
// 
//   DESCRIPTION: MachineMeters class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 02-JUL-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-JUL-2012 ACC    MOve class from WCP.
// 24-JUL-2012 ACC    Extended Meters: Add maximum meter values. Delta meters is zero if any maximum changed.
// 07-AUG-2014 ACC    Add Progressive Jackpot Cents into Meters
// 25-JAN-2018 FGB    WIGOS-7569: AGG SITE-Terminal analyse's screen: error identifiying the the anomaly "change of denomination"
// 25-JAN-2017 FGB    Add method Copy
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common
{
  //------------------------------------------------------------------------------
  // PURPOSE : MachineMeters class 
  //
  //   NOTES :
  //
  public class MachineMeters
  {
    // PLAYED WON METERS
    public Boolean PlayWonAvalaible = false;
    public Int64 PlayedCents = 0;
    public Int64 PlayedMaxValueCents = 0;
    public Int64 PlayedQuantity = 0;
    public Int64 PlayedMaxValueQuantity = 0;
    public Int64 WonCents = 0;
    public Int64 WonMaxValueCents = 0;
    public Int64 WonQuantity = 0;
    public Int64 WonMaxValueQuantity = 0;
    // ACC 15-JUN-2012 Add Jackpot Cents into Machine Meters
    public Int64 JackpotCents = 0;
    public Int64 JackpotMaxValueCents = 0;
    // ACC 04-AUG-2014 Add Jackpot Progressive Cents into Machine Meters
    public Int64 ProgressiveJackpotCents = 0;
    public Int64 ProgressiveJackpotMaxValueCents = 0;
    // AFT FUND TRANSFER METERS
    public Boolean AftFundTransferAvalaible = false;
    public Int64 AftToGmCents = 0;
    public Int64 AftToGmMaxValueCents = 0;
    public Int64 AftToGmQuantity = 0;
    public Int64 AftToGmMaxValueQuantity = 0;
    public Int64 AftFromGmCents = 0;
    public Int64 AftFromGmMaxValueCents = 0;
    public Int64 AftFromGmQuantity = 0;
    public Int64 AftFromGmMaxValueQuantity = 0;
    // EFT FUND TRANSFER METERS
    public Boolean EftFundTransferAvalaible = false;
    public Int64 EftToGmCents = 0;
    public Int64 EftToGmMaxValueCents = 0;
    public Int64 EftToGmQuantity = 0;
    public Int64 EftToGmMaxValueQuantity = 0;
    public Int64 EftFromGmCents = 0;
    public Int64 EftFromGmMaxValueCents = 0;
    public Int64 EftFromGmQuantity = 0;
    public Int64 EftFromGmMaxValueQuantity = 0;

    #region Operators Overloading
    public Boolean IsEqual(MachineMeters MM2)
    {
      if (this.PlayWonAvalaible)
      {
        if (this.PlayWonAvalaible != MM2.PlayWonAvalaible ||
             this.PlayedCents != MM2.PlayedCents ||
             this.PlayedQuantity != MM2.PlayedQuantity ||
             this.WonCents != MM2.WonCents ||
             this.WonQuantity != MM2.WonQuantity ||
             this.JackpotCents != MM2.JackpotCents ||
             this.PlayedMaxValueCents != MM2.PlayedMaxValueCents ||
             this.PlayedMaxValueQuantity != MM2.PlayedMaxValueQuantity ||
             this.WonMaxValueCents != MM2.WonMaxValueCents ||
             this.WonMaxValueQuantity != MM2.WonMaxValueQuantity ||
             this.JackpotMaxValueCents != MM2.JackpotMaxValueCents ||
             this.ProgressiveJackpotCents != MM2.ProgressiveJackpotCents ||
             this.ProgressiveJackpotMaxValueCents != MM2.ProgressiveJackpotMaxValueCents)
        {
          return false;
        }
      }

      if (this.AftFundTransferAvalaible)
      {
        if (this.AftFundTransferAvalaible != MM2.AftFundTransferAvalaible ||
           this.AftToGmCents != MM2.AftToGmCents ||
           this.AftToGmQuantity != MM2.AftToGmQuantity ||
           this.AftFromGmCents != MM2.AftFromGmCents ||
           this.AftFromGmQuantity != MM2.AftFromGmQuantity ||
           this.AftToGmMaxValueCents != MM2.AftToGmMaxValueCents ||
           this.AftToGmMaxValueQuantity != MM2.AftToGmMaxValueQuantity ||
           this.AftFromGmMaxValueCents != MM2.AftFromGmMaxValueCents ||
           this.AftFromGmMaxValueQuantity != MM2.AftFromGmMaxValueQuantity)
        {
          return false;
        }
      }

      if (this.EftFundTransferAvalaible)
      {
        if (this.EftFundTransferAvalaible != MM2.EftFundTransferAvalaible ||
           this.EftToGmCents != MM2.EftToGmCents ||
           this.EftToGmQuantity != MM2.EftToGmQuantity ||
           this.EftFromGmCents != MM2.EftFromGmCents ||
           this.EftFromGmQuantity != MM2.EftFromGmQuantity ||
           this.EftToGmMaxValueCents != MM2.EftToGmMaxValueCents ||
           this.EftToGmMaxValueQuantity != MM2.EftToGmMaxValueQuantity ||
           this.EftFromGmMaxValueCents != MM2.EftFromGmMaxValueCents ||
           this.EftFromGmMaxValueQuantity != MM2.EftFromGmMaxValueQuantity)
        {
          return false;
        }
      }

      return true;
    }
    #endregion

    #region Public Methods
    /// <summary>
    /// Return a copy of MachineMeters
    /// </summary>
    /// <returns></returns>
    public static MachineMeters Copy(MachineMeters OriginMeters)
    {
      MachineMeters _result;

      _result = new MachineMeters();

      if (OriginMeters != null)
      {
        _result.PlayWonAvalaible = OriginMeters.PlayWonAvalaible;
        _result.PlayedCents = OriginMeters.PlayedCents;
        _result.PlayedMaxValueCents = OriginMeters.PlayedMaxValueCents;
        _result.PlayedQuantity = OriginMeters.PlayedQuantity;
        _result.PlayedMaxValueQuantity = OriginMeters.PlayedMaxValueQuantity;
        _result.WonCents = OriginMeters.WonCents;
        _result.WonMaxValueCents = OriginMeters.WonMaxValueCents;
        _result.WonQuantity = OriginMeters.WonQuantity;
        _result.WonMaxValueQuantity = OriginMeters.WonMaxValueQuantity;
        _result.JackpotCents = OriginMeters.JackpotCents;
        _result.JackpotMaxValueCents = OriginMeters.JackpotMaxValueCents;
        _result.ProgressiveJackpotCents = OriginMeters.ProgressiveJackpotCents;
        _result.ProgressiveJackpotMaxValueCents = OriginMeters.ProgressiveJackpotMaxValueCents;
        _result.AftFundTransferAvalaible = OriginMeters.AftFundTransferAvalaible;
        _result.AftToGmCents = OriginMeters.AftToGmCents;
        _result.AftToGmMaxValueCents = OriginMeters.AftToGmMaxValueCents;
        _result.AftToGmQuantity = OriginMeters.AftToGmQuantity;
        _result.AftToGmMaxValueQuantity = OriginMeters.AftToGmMaxValueQuantity;
        _result.AftFromGmCents = OriginMeters.AftFromGmCents;
        _result.AftFromGmMaxValueCents = OriginMeters.AftFromGmMaxValueCents;
        _result.AftFromGmQuantity = OriginMeters.AftFromGmQuantity;
        _result.AftFromGmMaxValueQuantity = OriginMeters.AftFromGmMaxValueQuantity;
        _result.EftFundTransferAvalaible = OriginMeters.EftFundTransferAvalaible;
        _result.EftToGmCents = OriginMeters.EftToGmCents;
        _result.EftToGmMaxValueCents = OriginMeters.EftToGmMaxValueCents;
        _result.EftToGmQuantity = OriginMeters.EftToGmQuantity;
        _result.EftToGmMaxValueQuantity = OriginMeters.EftToGmMaxValueQuantity;
        _result.EftFromGmCents = OriginMeters.EftFromGmCents;
        _result.EftFromGmMaxValueCents = OriginMeters.EftFromGmMaxValueCents;
        _result.EftFromGmQuantity = OriginMeters.EftFromGmQuantity;
        _result.EftFromGmMaxValueQuantity = OriginMeters.EftFromGmMaxValueQuantity;
      }

      return _result;
    }
    #endregion
  }
}
