//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: SoftwareValidations.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: DDM
// 
// CREATION DATE: 03-APR-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-APR-2014 DDM     First release.
// 11-APR-2014 JCOR    Added ReadSoftwareValidationByID.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common
{
  public static class SoftwareValidations
  {

    //------------------------------------------------------------------------------
    // PURPOSE: Read signature validation
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int32: TerminalId
    //        - SqlTransaction: SqlTrx
    //
    //      - OUTPUT:
    //        - DataTable: SignatureValidation
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean ReadSignatureValidation(Int32 TerminalId, out DataTable SignatureValidation, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      SignatureValidation = new DataTable("TERMINALS");

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   TE_AUTHENTICATION_METHOD ");
        _sb.AppendLine("        , TE_AUTHENTICATION_SEED  ");
        _sb.AppendLine("        , TE_AUTHENTICATION_SIGNATURE  ");
        _sb.AppendLine("   FROM   TERMINALS ");
        _sb.AppendLine("  WHERE   TE_TERMINAL_ID = @pTerminalId");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(SignatureValidation);

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("ReadSignatureValidation. Exception thrown -> " + _ex.Message);
      }

      return false;

    } // ReadSignatureValidation

    public static String GenerateRandomSeed()
    {
      Int32 _random_seed;
      Random _random;

      _random = new Random(DateTime.Now.Millisecond);
      _random_seed = _random.Next(65535);

      return _random_seed.ToString("X4");
    }
    //------------------------------------------------------------------------------
    // PURPOSE: Insert software validations
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int32: TerminalId
    //        - SqlTransaction: Trx
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean InsertSoftwareValidation(Int32 TerminalId, SqlTransaction Trx)
    {
      DataTable _dt_sval;

      _dt_sval = new DataTable();
      _dt_sval.Columns.Add("SVAL_TERMINAL_ID", Type.GetType("System.Int32"));
      _dt_sval.Columns.Add("SVAL_RANDOM_SEED", Type.GetType("System.String"));
      _dt_sval.Columns.Add("SVAL_VALIDATION_ID", Type.GetType("System.Int64"));

      _dt_sval.Rows.Add();
      _dt_sval.Rows[0]["SVAL_TERMINAL_ID"] = TerminalId;
      _dt_sval.Rows[0]["SVAL_RANDOM_SEED"] = GenerateRandomSeed();

      return InsertSoftwareValidation(_dt_sval, Trx);
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Insert software validations
    // 
    //  PARAMS:
    //      - INPUT:
    //        - SoftwareValidation: Datatable with SVAL_VALIDATION_ID, SVAL_TERMINAL_ID and SVAL_RANDOM_SEED columns
    //        - SqlTransaction: Trx
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean InsertSoftwareValidation(DataTable SoftwareValidation, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _nr;
      SqlParameter _param;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" INSERT INTO   SOFTWARE_VALIDATIONS     ");
        _sb.AppendLine("             ( SVAL_INSERTED            ");
        _sb.AppendLine("             , SVAL_TERMINAL_ID         ");
        _sb.AppendLine("             , SVAL_STATUS              ");
        _sb.AppendLine("             , SVAL_METHOD              ");
        _sb.AppendLine("             , SVAL_SEED                ");
        _sb.AppendLine("             , SVAL_EXPECTED_SIGNATURE  ");
        _sb.AppendLine("             , SVAL_LAST_REQUEST)       ");
        _sb.AppendLine("      SELECT   GETDATE()                                    ");
        _sb.AppendLine("             , @pTerminalId                                 ");
        _sb.AppendLine("             , @pStatus                                     ");
        _sb.AppendLine("             , TE_AUTHENTICATION_METHOD                     ");
        _sb.AppendLine("             , ISNULL(TE_AUTHENTICATION_SEED, @pRandomSeed) ");
        _sb.AppendLine("             , TE_AUTHENTICATION_SIGNATURE                  ");
        _sb.AppendLine("             , GETDATE()                                    ");
        _sb.AppendLine("        FROM   TERMINALS                                    ");
        _sb.AppendLine("       WHERE   TE_TERMINAL_ID = @pTerminalId                ");
        _sb.AppendLine("");
        _sb.AppendLine("SET @pUniqueId = SCOPE_IDENTITY()                           ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.UpdatedRowSource = UpdateRowSource.OutputParameters;

          _param = _cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt);
          _param.SourceColumn = "SVAL_VALIDATION_ID";
          _param.Direction = ParameterDirection.Output;

          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "SVAL_TERMINAL_ID";
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = SoftwareValidationStatus.Pending;
          _cmd.Parameters.Add("@pRandomSeed", SqlDbType.VarChar, 50, "SVAL_RANDOM_SEED");

          using (SqlDataAdapter _da = new SqlDataAdapter())
          {
            _da.InsertCommand = _cmd;

            _nr = _da.Update(SoftwareValidation);
            if (_nr == SoftwareValidation.Rows.Count)
            {
              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("InsertSoftwareValidations. Exception thrown -> " + _ex.Message);
        return false;
      }

      return false;
    } // InsertSoftwareValidations

    //------------------------------------------------------------------------------
    // PURPOSE: Read Authentication & Machine status
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int32: TerminalId
    //        - SqlTransaction: SqlTrx
    //
    //      - OUTPUT:
    //        - ENUM_AUTHENTICATION_STATUS: AuthenticationStatus
    //        - Int32: MachineStatus
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean ReadAuthenticationMachineStatus(Int32 TerminalId, out ENUM_AUTHENTICATION_STATUS AuthenticationStatus, out Int32 MachineStatus, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      AuthenticationStatus = ENUM_AUTHENTICATION_STATUS.UNKNOWN;
      MachineStatus = 0;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("    SELECT   TE_AUTHENTICATION_STATUS ");
        _sb.AppendLine("           , TS_MACHINE_FLAGS ");
        _sb.AppendLine("      FROM   TERMINALS ");
        _sb.AppendLine(" LEFT JOIN   TERMINAL_STATUS ON TS_TERMINAL_ID = TE_TERMINAL_ID ");
        _sb.AppendLine("     WHERE   TE_TERMINAL_ID = @pTerminalId");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              AuthenticationStatus = _reader.IsDBNull(0) ? ENUM_AUTHENTICATION_STATUS.UNKNOWN : (ENUM_AUTHENTICATION_STATUS)_reader.GetInt32(0);
              MachineStatus = _reader.IsDBNull(1) ? 0 : _reader.GetInt32(1);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(" ReadAuthenticationStatus. Exception thrown -> " + _ex.Message);
      }

      return false;
    } // ReadAuthenticationMachineStatus

    //------------------------------------------------------------------------------
    // PURPOSE: Save Authentication status
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int32: TerminalId
    //        - ENUM_AUTHENTICATION_STATUS: AuthenticationStatus
    //        - SqlTransaction: SqlTrx
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean SaveAuthenticationStatus(Int32 TerminalId, ENUM_AUTHENTICATION_STATUS AuthenticationStatus, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   TERMINALS ");
        _sb.AppendLine("    SET   TE_AUTHENTICATION_STATUS = @pAuthenticationStatus ");
        _sb.AppendLine("        , TE_AUTHENTICATION_LAST_CHECKED = GETDATE() ");
        _sb.AppendLine("  WHERE   TE_TERMINAL_ID = @pTerminalId");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pAuthenticationStatus", SqlDbType.Int).Value = AuthenticationStatus;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("SaveAuthenticationStatus. TerminalId: " + TerminalId);
            return false;
          }

          // Changes BLOCKED_BY_SIGNATURE (Bit 2)of MachineStatus
          if (AuthenticationStatus == ENUM_AUTHENTICATION_STATUS.OK)
          {
            if (!TerminalStatusFlags.DB_UnsetFlag(TerminalStatusFlags.BITMASK_TYPE.Machine_status, TerminalId, (Int32)TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_BY_SIGNATURE, SqlTrx))
            {
              return false;
            }
          }
          if (AuthenticationStatus == ENUM_AUTHENTICATION_STATUS.ERROR)
          {
            if (!TerminalStatusFlags.DB_SetFlag(TerminalStatusFlags.BITMASK_TYPE.Machine_status, TerminalId, (Int32)TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_BY_SIGNATURE, SqlTrx))
            {
              return false;
            }
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error("SaveAuthenticationStatus. Exception thrown -> " + _ex.Message);
      }

      return false;
    } // SaveAuthenticationStatus

    //------------------------------------------------------------------------------
    // PURPOSE: Update software validations
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int64: ValidationId
    //        - Int32: TerminalId
    //        - Int32: MachineStatus
    //        - String: Seed
    //        - String: Signature
    //        - SoftwareValidationStatus: Status
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    // TODO: remove if not used
    public static Boolean SaveSoftwareValidation(DataRow[] SWValidations)
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (SaveSoftwareValidation(SWValidations, _db_trx.SqlTransaction))
          {
            _db_trx.Commit();
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("UpdateSoftwareValidations. Exception thrown -> " + _ex.Message);
      }
      return false;
    }// UpdateSoftwareValidations

    //------------------------------------------------------------------------------
    // PURPOSE: Update software validations
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int32: TerminalId
    //        - Int32: MachineStatus
    //        - String: Seed
    //        - String: Signature
    //        - SoftwareValidationStatus: Status
    //        - SqlTransaction: SqlTrx
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean SaveSoftwareValidation(DataRow[] SWValidations, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      int _num_updated;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   SOFTWARE_VALIDATIONS                          ");
        _sb.AppendLine("    SET   SVAL_SEED               = @pSeed              ");
        _sb.AppendLine("        , SVAL_EXPECTED_SIGNATURE = @pSignature         ");
        _sb.AppendLine("        , SVAL_STATUS             = @pStatus1           ");
        _sb.AppendLine("        , SVAL_LAST_REQUEST       = GETDATE()           ");
        _sb.AppendLine("        , SVAL_RECEIVED_STATUS    = @pReceivedStatus    ");
        _sb.AppendLine("        , SVAL_RECEIVED_SIGNATURE = @pReceivedSignature ");
        _sb.AppendLine("        , SVAL_RECEIVED_DATETIME  = case when SVAL_RECEIVED_SIGNATURE = @pSignature ");
        _sb.AppendLine("                                           OR @pStatus1               = @pStatusTimeOut ");
        _sb.AppendLine("                                         then SVAL_RECEIVED_DATETIME else GETDATE() end");
        _sb.AppendLine("  WHERE   SVAL_VALIDATION_ID      = @pValidationId      ");
        _sb.AppendLine("    AND   SVAL_STATUS             = @pStatus0           ");

        using (SqlDataAdapter _da = new SqlDataAdapter())
        {
          using (SqlCommand _cmd_update = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            SqlParameter _param;

            _param = new SqlParameter("@pStatus0", SqlDbType.Int);
            _param.SourceColumn = "SVAL_STATUS";
            _param.SourceVersion = DataRowVersion.Original;
            _cmd_update.Parameters.Add(_param);

            _param = new SqlParameter("@pStatus1", SqlDbType.Int);
            _param.SourceColumn = "SVAL_STATUS";
            _param.SourceVersion = DataRowVersion.Current;
            _cmd_update.Parameters.Add(_param);

            _param = new SqlParameter("@pSeed", SqlDbType.NVarChar);
            _param.SourceColumn = "SVAL_SEED";
            _param.SourceVersion = DataRowVersion.Current;
            _cmd_update.Parameters.Add(_param);

            _param = new SqlParameter("@pSignature", SqlDbType.NVarChar);
            _param.SourceColumn = "SVAL_EXPECTED_SIGNATURE";
            _param.SourceVersion = DataRowVersion.Current;
            _cmd_update.Parameters.Add(_param);

            _param = new SqlParameter("@pValidationId", SqlDbType.BigInt);
            _param.SourceColumn = "SVAL_VALIDATION_ID";
            _param.SourceVersion = DataRowVersion.Original;
            _cmd_update.Parameters.Add(_param);

            _param = new SqlParameter("@pReceivedStatus", SqlDbType.Int);
            _param.SourceColumn = "SVAL_RECEIVED_STATUS";
            _param.SourceVersion = DataRowVersion.Current;
            _cmd_update.Parameters.Add(_param);

            _param = new SqlParameter("@pReceivedSignature", SqlDbType.NVarChar);
            _param.SourceColumn = "SVAL_RECEIVED_SIGNATURE";
            _param.SourceVersion = DataRowVersion.Current;
            _cmd_update.Parameters.Add(_param);

            _cmd_update.Parameters.Add("@pStatusTimeOut", SqlDbType.Int).Value = SoftwareValidationStatus.CanceledTimeout;

            _da.UpdateCommand = _cmd_update;
            _num_updated = _da.Update(SWValidations);

            if (_num_updated == SWValidations.Length)
            {
              return true;
            }

            Log.Warning(String.Format("UpdateSoftwareValidation ROLLBACK, NumModified: {0}, NumUpdated: {1}", SWValidations.Length.ToString(), _num_updated.ToString()));

            return false;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateSoftwareValidations

    //------------------------------------------------------------------------------
    // PURPOSE: Read software validation by Status
    //      - INPUT:
    //        - SoftwareValidationStatus: SoftwareValidationStatus
    //
    //      - OUTPUT:
    //        - DataTable: SoftwareValidations
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    // TODO: remove if not used
    public static Boolean ReadSoftwareValidationByStatus(SoftwareValidationStatus SoftwareValidationStatus, out DataTable SoftwareValidations)
    {
      StringBuilder _sb;

      SoftwareValidations = new DataTable("SOFTWARE_VALIDATIONS");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();

          _sb.AppendLine(" SELECT   SVAL_VALIDATION_ID ");
          _sb.AppendLine("        , SVAL_INSERTED ");
          _sb.AppendLine("        , SVAL_TERMINAL_ID ");
          _sb.AppendLine("        , SVAL_METHOD ");
          _sb.AppendLine("        , isnull (SVAL_SEED, '0') as SVAL_SEED ");
          _sb.AppendLine("        , SVAL_EXPECTED_SIGNATURE ");
          _sb.AppendLine("        , SVAL_STATUS ");
          _sb.AppendLine("        , SVAL_LAST_REQUEST ");
          _sb.AppendLine("        , SVAL_RECEIVED_STATUS ");
          _sb.AppendLine("        , SVAL_RECEIVED_SIGNATURE ");
          _sb.AppendLine("        , SVAL_RECEIVED_DATETIME ");
          _sb.AppendLine("        , DATEDIFF (SECOND, isnull (SVAL_LAST_REQUEST, SVAL_INSERTED),  GETDATE()) SVAL_ELAPSED ");
          _sb.AppendLine("   FROM   SOFTWARE_VALIDATIONS ");
          _sb.AppendLine("  WHERE   SVAL_STATUS = @pStatus");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (int)SoftwareValidationStatus;

            using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
            {
              _da.Fill(SoftwareValidations);

              return true;
            }
          } // sqlCommand
        }// db_trx
      }
      catch (Exception _ex)
      {
        Log.Error("ReadSoftwareValidationByStatus. Exception thrown -> " + _ex.Message);
      }

      return false;
    }// ReadSoftwareValidationByStatus

    //------------------------------------------------------------------------------
    // PURPOSE: Read software validation by Validation ID
    // 
    //  PARAMS:
    //      - INPUT:
    //        - ValidationID
    //        - SqlTransaction: SqlTrx
    //
    //      - OUTPUT:
    //        - DataTable: SoftwareValidations
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean ReadSoftwareValidationByID(Int64 ValidationID, out DataTable SoftwareValidations, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      SoftwareValidations = new DataTable("SOFTWARE_VALIDATIONS");

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   SVAL_VALIDATION_ID ");
        _sb.AppendLine("        , SVAL_INSERTED ");
        _sb.AppendLine("        , SVAL_TERMINAL_ID ");
        _sb.AppendLine("        , SVAL_METHOD ");
        _sb.AppendLine("        , isnull (SVAL_SEED, '0') as SVAL_SEED ");
        _sb.AppendLine("        , SVAL_EXPECTED_SIGNATURE ");
        _sb.AppendLine("        , SVAL_STATUS ");
        _sb.AppendLine("        , SVAL_LAST_REQUEST ");
        _sb.AppendLine("        , SVAL_RECEIVED_STATUS ");
        _sb.AppendLine("        , SVAL_RECEIVED_SIGNATURE ");
        _sb.AppendLine("        , SVAL_RECEIVED_DATETIME ");
        _sb.AppendLine("        , DATEDIFF (SECOND, isnull (SVAL_LAST_REQUEST, SVAL_INSERTED),  GETDATE()) SVAL_ELAPSED ");
        _sb.AppendLine("   FROM   SOFTWARE_VALIDATIONS ");
        _sb.AppendLine("  WHERE   SVAL_VALIDATION_ID = @pValidationID");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pValidationID", SqlDbType.BigInt).Value = ValidationID;

          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(SoftwareValidations);

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("ReadSoftwareValidationByID. Exception thrown -> " + _ex.Message);
      }

      return false;

    } // ReadSoftwareValidationByID
  } //SoftwareValidations
}
