﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ExpedClientWS.cs
// 
//   DESCRIPTION: Miscellaneous utilities for External Validation Operation
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 01-SEP-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-SEP-2016 FGB    First release (Header).
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 10-NOV-2016 FGB    Bug 20222: Exped: Connection with WCP service
// 15-NOV-2016 FGB    BUG 20636: Exped: Faltan informar campos al WS
// 06-MAR-2016 ATB    PBI 25262: Exped - Payment authorisation
// 07-MAR-2017 FAV    PBI 25265: Exped - Transaction authorization
// 07-MAR-2016 ATB    PBI 25262: Exped - Payment authorisation
// 24-MAR-2017 FAV    PBI 25265: Exped - Transaction authorization
//------------------------------------------------------------------------------
//
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using System.Xml.Serialization;
using WSI.Common.Exceptions;
using WSI.Common.Utilities;
using WSI.Common.Utilities.Extensions;

namespace WSI.Common.ExternalPaymentAndSaleValidation
{
  /// <summary>
  /// Input data for Exped WS
  /// </summary>
  public class ExpedClientWSDataInput
  {
    public Int64 AccountId;
    public String AuthorizationCode;
    public EnumExternalPaymentAndSaleValidationOperationMethod TypeOfRequest;
  }

  /// <summary>
  /// Output data for Exped WS
  /// </summary>
  public class ExpedClientWSDataOutput
  {
    public String ErrorMessage;
    public ExpedClientResponse ExpedResponse;
  }

  /// <summary>
  /// Class to perform actions (authorization/registration) on Exped WS
  /// </summary>
  public class ExpedClientWS
  {
    #region "Members"
    private String m_url;
    private String m_domain;
    private String m_username;
    private String m_pwd;
    private Int32 m_timeout_in_milliseconds;
    #endregion

    #region "Constants"
    private const String GET_AUTHORIZATION_METHOD = "GetAuthorization";
    private const String REGISTER_TRANSACTION_RECHARGE_METHOD = "RegisterTransactionRecharge";
    private const String REGISTER_TRANSACTION_REFUND_METHOD = "RegisterTransactionRefund";

    private const String SERVICE_RUNNING_EXPED_WS = "WCP";
    #endregion

    #region "Constructor"
    /// <summary>
    /// Constructor without parameters
    /// </summary>
    public ExpedClientWS()
    {
      String _url;
      String _domain;
      String _username;
      String _pwd;
      Int32 _timeout_in_milliseconds;

      //Search parameters
      GetWSConnectionParams(out _url, out _domain, out _username, out _pwd, out _timeout_in_milliseconds);

      Initialize(_url, _domain, _username, _pwd, _timeout_in_milliseconds);
    }
    #endregion

    /// <summary>
    /// Initializes the members
    /// </summary>
    /// <param name="Url"></param>
    /// <param name="Domain"></param>
    /// <param name="UserName"></param>
    /// <param name="Password"></param>
    /// <param name="TimeOutInMilliseconds"></param>
    private void Initialize(String Url, String Domain, String UserName, String Password, Int32 TimeOutInMilliseconds)
    {
      System.Net.ServicePointManager.ServerCertificateValidationCallback += ValidateRemoteCertificate;

      m_url = Url;
      m_domain = Domain;
      m_username = UserName;
      m_pwd = Password;
      m_timeout_in_milliseconds = TimeOutInMilliseconds;
    }

    /// <summary>
    /// Returns the connection params for the WS
    /// </summary>
    /// <param name="Url"></param>
    /// <param name="Domain"></param>
    /// <param name="Username"></param>
    /// <param name="Pwd"></param>
    /// <param name="TimeOutInMilliseconds"></param>
    private void GetWSConnectionParams(out String Url, out String Domain, out String Username, out String Pwd, out Int32 TimeOutInMilliseconds)
    {
      Url = ExternalPaymentAndSaleValidationCommon.ExpedServerAddress1;
      Domain = ExternalPaymentAndSaleValidationCommon.ExpedDomain;
      Username = ExternalPaymentAndSaleValidationCommon.ExpedUsername;
      Pwd = ExternalPaymentAndSaleValidationCommon.ExpedPassword;
      TimeOutInMilliseconds = ExternalPaymentAndSaleValidationCommon.ExpedTimeOutInMilliseconds;
    }

    /// <summary>
    /// Call the Service
    /// </summary>
    /// <param name="UriAddress"></param>
    /// <param name="Document"></param>
    /// <returns></returns>
    private XmlDocument CallService(String UriAddress, XmlDocument Document)
    {
      var _web_request = WebRequestHelper.CreateWebRequestPOST(UriAddress, Document);

      using (var _web_response = (System.Net.HttpWebResponse)_web_request.GetResponse())
      {
        return _web_response.Read();
      }
    }

    /// <summary>
    /// Serialize
    /// </summary>
    /// <param name="ObjectResponse"></param>
    /// <returns></returns>
    private String Serialize(Object ObjectResponse)
    {
      XmlSerializer serializer = new XmlSerializer(ObjectResponse.GetType());
      XmlSerializerNamespaces _name_spaces = new XmlSerializerNamespaces();

      try
      {
        using (StringWriter writer = new Utf8StringWriter())
        {
          _name_spaces.Add("", "");
          serializer.Serialize(writer, ObjectResponse, _name_spaces);

          return writer.ToString();
        }
      }
      catch (Exception)
      {
        //Log.Exception(_ex);
      }

      return String.Empty;
    }
    #region "Public Methods"
    /// <summary>
    /// Gets the Authorization for the Account 
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="AuthorizationCode"></param>
    /// <param name="ErrorMessage"></param>
    /// <param name="IsServerConnected"></param>
    /// <returns></returns>
    public Boolean GetAuthorization(Int64 AccountId, out String AuthorizationCode, out String ErrorMessage, out Boolean IsServerConnected)
    {
      ExpedClientRequest _request_object;
      ExpedClientWSDataInput _data_input;
      ExpedClientWSDataOutput _data_output;

      AuthorizationCode = String.Empty;
      ErrorMessage = String.Empty;
      IsServerConnected = false;

      try
      {
        //Set 'GetAuthorization' petition values
        _request_object = MapGetAuthorizationRequest(AccountId);

        //Generate XML for authorization petition
        var _request = new XmlDocument();
        _request.LoadXml(XmlSerializationHelper.Serialize(_request_object));

        //Input
        _data_input = new ExpedClientWSDataInput();
        _data_input.AccountId = AccountId;
        _data_input.TypeOfRequest = EnumExternalPaymentAndSaleValidationOperationMethod.METHOD_GET_AUTHORIZATION;

        //Output
        _data_output = new ExpedClientWSDataOutput();

        //GetAuthorization
        if (!SendRequest(_data_input, _request, ref _data_output, out IsServerConnected))
        {
          if (_data_output != null)
          {
            LogError("ExpedClientWS.GetAuthorization: **ERROR** for AccountId: {0} - {1}", AccountId, _data_output.ErrorMessage);
          }

          return false;
        }

        AuthorizationCode = _data_output.ExpedResponse.CodeAuthorization;
        ErrorMessage = _data_output.ExpedResponse.ErrorDescription;

        if (_data_output.ExpedResponse.OperationResult != EnumExternalPaymentAndSaleValidationOperationResult.OPERATION_RESULT_OK)
        {
          LogError("ExpedClientWS.GetAuthorization: OperationResult {0}, for AccountId: {1} - {2}", _data_output.ExpedResponse.OperationResult, AccountId, ErrorMessage);
      }
        else
        {
          return true;
        }
      }
      catch (Exception _ex)
      {
        LogError("ExpedClientWS.GetAuthorization: **ERROR** for AccountId: {0} - {1}", AccountId, _ex.Message);

        ErrorMessage = Resource.String("STR_EXTERNAL_PAYMENT_AND_SALE_VALIDATION_GET_AUTHORIZATION_ERROR", AccountId);
      }

      return false;
    }

    /// <summary>
    /// Registers the Transaction in the WS
    /// </summary>
    /// <param name="OperationData"></param>
    /// <param name="DataOutput"></param>
    /// <returns></returns>
    public Boolean RegisterTransaction(ExternalValidationOperationData OperationData
                                     , ref ExpedClientWSDataOutput DataOutput)
    {
      ExpedClientRequest _request_object;
      ExpedClientWSDataInput _data_input;
      Boolean _is_server_connected;

      try
      {
        //Set 'RegisterTransaction' registration values
        _request_object = MapRegisterTransactionRequest(OperationData);

        //Generate XML for transaction registration 
        var _request = new XmlDocument();
        //Input values
        _data_input = new ExpedClientWSDataInput();

        if (OperationData.OperationType == TipoOperacion.Venta)
        {
          _data_input.TypeOfRequest = EnumExternalPaymentAndSaleValidationOperationMethod.METHOD_REGISTER_TRANSACTION_RECHARGE;
          _request_object.Type = REGISTER_TRANSACTION_RECHARGE_METHOD;
        }
        else
        {
          _data_input.TypeOfRequest = EnumExternalPaymentAndSaleValidationOperationMethod.METHOD_REGISTER_TRANSACTION_REFUND;
          _request_object.Type = REGISTER_TRANSACTION_REFUND_METHOD;
        }

        _request.LoadXml(XmlSerializationHelper.Serialize(_request_object));
        _data_input.AccountId = _request_object.AccountId;
        _data_input.AuthorizationCode = _request_object.AuthorizationCode;

        //Send request to WS
        return SendRequest(_data_input, _request, ref DataOutput, out _is_server_connected);
      }
      catch (Exception _ex)
      {
        LogError("ExpedClientWS.RegisterTransaction: **ERROR** for AccountId: {0} - AuthorizationCode: {1} - {2}", OperationData.AccountId, OperationData.AuthorizationCode, _ex.Message);

        DataOutput.ErrorMessage = Resource.String("STR_EXTERNAL_PAYMENT_AND_SALE_VALIDATION_REGISTER_TRANSACTION_ERROR", OperationData.AccountId);
      }

      return false;
    }
    #endregion

    #region "Private Methods"
    /// <summary>
    /// Logs the connection message if is enabled
    /// </summary>
    /// <param name="MsgFormat"></param>
    /// <param name="MsgArgs"></param>
    private void LogMessage(String MsgFormat, params object[] MsgArgs)
    {
      if (ExternalPaymentAndSaleValidationCommon.LogConnectionsToWS())
      {
        Log.Message(String.Format(MsgFormat, MsgArgs));
      }
    }

    /// <summary>
    /// Logs an error if the connection message if is enabled
    /// </summary>
    /// <param name="MsgFormat"></param>
    /// <param name="MsgArgs"></param>
    private void LogError(String MsgFormat, params object[] MsgArgs)
    {
      if (ExternalPaymentAndSaleValidationCommon.LogConnectionsToWS())
    {
        Log.Error(String.Format(MsgFormat, MsgArgs));
      }
    }

    /// <summary>
    /// Log phase message 
    /// </summary>
    /// <param name="Phase"></param>
    /// <param name="Message"></param>
    /// <param name="DataInput"></param>
    /// <param name="IPAndPort"></param>
    /// <param name="ErrorMessage"></param>
    private void LogMessagePhase(EnumExternalPaymentAndSaleValidationOperationPhase Phase, String Message, ExpedClientWSDataInput DataInput, String IPAndPort, String ErrorMessage)
    {
      String _request_type;
      String _request_phase;
      String _log_message;

      _request_type = GetRequestDescription(DataInput);
      _request_phase = GetRequestPhaseDescription(Phase);

      switch (DataInput.TypeOfRequest)
      {
        case EnumExternalPaymentAndSaleValidationOperationMethod.METHOD_GET_AUTHORIZATION:
          _log_message = String.Format("{0} {1} '{2}' request for AccountId: {3} to IP: {4}", Message, _request_phase, _request_type, DataInput.AccountId, IPAndPort);

          break;
        case EnumExternalPaymentAndSaleValidationOperationMethod.METHOD_REGISTER_TRANSACTION_RECHARGE:
          _log_message = String.Format("{0} {1} '{2}' request recharge for AccountId: {3} - AuthorizationCode: {4} to IP: {5}", Message, _request_phase, _request_type, DataInput.AccountId, DataInput.AuthorizationCode, IPAndPort);
          break;
        case EnumExternalPaymentAndSaleValidationOperationMethod.METHOD_REGISTER_TRANSACTION_REFUND:
          _log_message = String.Format("{0} {1} '{2}' request refund for AccountId: {3} - AuthorizationCode: {4} to IP: {5}", Message, _request_phase, _request_type, DataInput.AccountId, DataInput.AuthorizationCode, IPAndPort);
          break;
        default:

          throw new Exception(" GetRequestPhase. Invalid Request type value ");
      }

      if (!String.IsNullOrEmpty(ErrorMessage))
      {
        _log_message += " - error: " + ErrorMessage;
      }

      LogMessage(_log_message);
    }

    /// <summary>
    /// Get request description
    /// </summary>
    /// <param name="DataInput"></param>
    /// <returns></returns>
    private String GetRequestDescription(ExpedClientWSDataInput DataInput)
    {
      switch (DataInput.TypeOfRequest)
      {
        case EnumExternalPaymentAndSaleValidationOperationMethod.METHOD_GET_AUTHORIZATION:
          return "GetAuthorization";
        case EnumExternalPaymentAndSaleValidationOperationMethod.METHOD_REGISTER_TRANSACTION_RECHARGE:
          return "RegisterTransactionRecharge";
        case EnumExternalPaymentAndSaleValidationOperationMethod.METHOD_REGISTER_TRANSACTION_REFUND:
          return "RegisterTransactionRefund";
        default:

          throw new Exception(" GetRequestType. Invalid Request type value ");
      }
    }

    /// <summary>
    /// Get request resource name
    /// </summary>
    /// <param name="DataInput"></param>
    /// <returns></returns>
    private String GetRequestResourceName(ExpedClientWSDataInput DataInput)
    {
      switch (DataInput.TypeOfRequest)
      {
        case EnumExternalPaymentAndSaleValidationOperationMethod.METHOD_GET_AUTHORIZATION:
          return "STR_EXTERNAL_PAYMENT_AND_SALE_VALIDATION_GET_AUTHORIZATION_ERROR";
        case EnumExternalPaymentAndSaleValidationOperationMethod.METHOD_REGISTER_TRANSACTION_RECHARGE:
          return "STR_EXTERNAL_PAYMENT_AND_SALE_VALIDATION_REGISTER_TRANSACTION_ERROR";
        case EnumExternalPaymentAndSaleValidationOperationMethod.METHOD_REGISTER_TRANSACTION_REFUND:
          return "STR_EXTERNAL_PAYMENT_AND_SALE_VALIDATION_REGISTER_TRANSACTION_ERROR";
        default:
          throw new Exception(" GetRequestResourceName. Invalid Request type value ");
      }
    }

    /// <summary>
    /// Get operation phase description
    /// </summary>
    /// <param name="Phase"></param>
    /// <returns></returns>
    private String GetRequestPhaseDescription(EnumExternalPaymentAndSaleValidationOperationPhase Phase)
    {
      switch (Phase)
      {
        case EnumExternalPaymentAndSaleValidationOperationPhase.PHASE_SENT:

          return "Sending";
        case EnumExternalPaymentAndSaleValidationOperationPhase.PHASE_RECEIVED:

          return "Reply received";
        case EnumExternalPaymentAndSaleValidationOperationPhase.PHASE_ERROR:

          return "** ERROR **";
        default:

          throw new Exception(" GetRequestPhase. Invalid Operation phase value ");
      }
    }

    /// <summary>
    /// Set connection values
    /// </summary>
    /// <param name="RequestObject"></param>
    private void SetConnectionParams(ExpedClientRequest RequestObject)
    {
      RequestObject.ServerAddress = ExternalPaymentAndSaleValidationCommon.ExpedServerAddress1;
      RequestObject.Domain = m_domain;
      RequestObject.User = m_username;
      RequestObject.Password = m_pwd;
      RequestObject.TimeOutInMilliseconds = m_timeout_in_milliseconds;
    }

    /// <summary>
    /// Map ExpedClientRequest with the AccountId for Authorization
    /// </summary>
    /// <param name="AccountId"></param>
    /// <returns></returns>
    private ExpedClientRequest MapGetAuthorizationRequest(Int64 AccountId)
    {
      ExpedClientRequest _request;

      //Map fields
      _request = new ExpedClientRequest();
      _request.Type = GET_AUTHORIZATION_METHOD; //Type -> Name of the method 'GetAuthorization' in ExpedClientService.cs at ExpedBridge.dll 
      _request.AccountId = AccountId;

      //Set connection values
      SetConnectionParams(_request);

      return _request;
    }

    /// <summary>
    /// Map fields from OperationData to ExpedClientRequest
    /// </summary>
    /// <param name="OperationData"></param>
    /// <returns></returns>
    private ExpedClientRequest MapRegisterTransactionRequest(ExternalValidationOperationData OperationData)
    {
      ExpedClientRequest _request;

      //Map fields
      _request = new ExpedClientRequest();
      _request.Type = REGISTER_TRANSACTION_RECHARGE_METHOD; //Type -> Name of the method 'RegisterTransaction' in ExpedClientService.cs at ExpedBridge.dll 
      _request.Type = REGISTER_TRANSACTION_REFUND_METHOD; //Type -> Name of the method 'RegisterTransaction' in ExpedClientService.cs at ExpedBridge.dll 
      _request.AuthorizationCode = OperationData.AuthorizationCode;

      _request.OperationId = OperationData.OperationId;
      _request.AccountId = OperationData.AccountId;
      _request.NetAmount = OperationData.NetAmount;
      _request.NetAmountSplitA = OperationData.NetAmountSplitA;
      _request.NetAmountSplitB = OperationData.NetAmountSplitB;

      _request.AccountName = OperationData.AccountName;
      _request.OperationDate = OperationData.OperationDate;

      _request.SiteId = OperationData.SiteId;
      _request.BusinessLineType = (TipoLineaNegocio)OperationData.BusinessLineType;
      _request.OperationType = (TipoOperacion)OperationData.OperationType;

      _request.HtmlTicket = OperationData.HtmlTicket;
      _request.TicketNumber = OperationData.TicketNumber;

      _request.CheckNumber = OperationData.CheckNumber;
      _request.CheckAmount = OperationData.CheckAmount;

      //Set connection values
      SetConnectionParams(_request);

      return _request;
    }

    /// <summary>
    /// Needed to trust any certificate
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="certificate"></param>
    /// <param name="chain"></param>
    /// <param name="sslPolicyErrors"></param>
    /// <returns></returns>
    private Boolean ValidateRemoteCertificate(Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
      // trust any certificate!!!
      return true;
    }

    /// <summary>
    /// Gets the Port to connect to WCP
    /// </summary>
    /// <returns></returns>
    private String GetWCPPortToConnect()
    {
      return ExternalPaymentAndSaleValidationCommon.ExpedWCPPort;
    }

    /// <summary>
    /// Gets the IP and the Port to be able to connect to WCP
    /// </summary>
    /// <param name="IPWithoutPort"></param>
    /// <returns></returns>
    private String GetIPAndPortFromWCPToConnect(String IPWithoutPort)
    {
      String _port;
      String _ip_and_port;

      _port = GetWCPPortToConnect().Trim();

      _ip_and_port = Misc.GetIPAndPort(IPWithoutPort, _port);

      return _ip_and_port;
    }

    /// <summary>
    /// Send request to WS
    /// </summary>
    /// <param name="DataInput"></param>
    /// <param name="Request"></param>
    /// <param name="DataOutput"></param>
    /// <param name="IsServerConnected"></param>
    /// <returns></returns>
    private Boolean SendRequest(ExpedClientWSDataInput DataInput, XmlDocument Request, ref ExpedClientWSDataOutput DataOutput, out Boolean IsServerConnected)
    {
      //If 'fixed IP' then ONLY try to connect to that IP, and don't search or try other IP
      if (!String.IsNullOrEmpty(ExternalPaymentAndSaleValidationCommon.ExpedWCPFixedIP))
      {
        return SendRequestToWS(ExternalPaymentAndSaleValidationCommon.ExpedWCPFixedIP, DataInput, Request, ref DataOutput, out IsServerConnected);
      }

      //If 'last IP' then try connecting to that IP, and if it does not work try with the list of available services
      if (!String.IsNullOrEmpty(ExternalPaymentAndSaleValidationCommon.ExpedWCPLastActiveIP))
      {
        if (SendRequestToWS(ExternalPaymentAndSaleValidationCommon.ExpedWCPLastActiveIP, DataInput, Request, ref DataOutput, out IsServerConnected))
        {
          return true;
        }
      }

      //Send request to the list of available services
      return SendRequestToAvailableWS(DataInput, Request, ref DataOutput, out IsServerConnected);
    }

    /// <summary>
    /// Send request to WS
    /// </summary>
    /// <param name="IP"></param>
    /// <param name="DataInput"></param>
    /// <param name="Request"></param>
    /// <param name="DataOutput"></param>
    /// <param name="IsServerConnected"></param>
    /// <returns></returns>
    private Boolean SendRequestToWS(String IP, ExpedClientWSDataInput DataInput, XmlDocument Request, ref ExpedClientWSDataOutput DataOutput, out Boolean IsServerConnected)
    {
      const String MESSAGE_TEXT = "ExpedClientWS.SendRequestToWS:";

      String _ip_and_port;
      ExpedClientResponse _exped_client_response;

      IsServerConnected = false;


      //Get IP and port
      _ip_and_port = GetIPAndPortFromWCPToConnect(IP);

      try
      {
        //Get authorization from WS bridge
        LogMessagePhase(EnumExternalPaymentAndSaleValidationOperationPhase.PHASE_SENT, MESSAGE_TEXT, DataInput, _ip_and_port, String.Empty);
        var _xml_result = CallService(_ip_and_port, Request);
        LogMessagePhase(EnumExternalPaymentAndSaleValidationOperationPhase.PHASE_RECEIVED, MESSAGE_TEXT, DataInput, _ip_and_port, String.Empty);

        //Deserialize result from WS
        _exped_client_response = XmlSerializationHelper.Deserialize<ExpedClientResponse>(_xml_result.InnerXml);

        //Get output
        DataOutput.ExpedResponse = _exped_client_response;
        DataOutput.ErrorMessage = String.Empty;

        IsServerConnected = true;
        return true;
      }
      catch (WebException _ex)
      {
        IsServerConnected = (_ex.Status == WebExceptionStatus.Success);
        Log.Error(String.Format("The Exped Web service has not responde. Status: {0}. IsServerConnected: {1}.", _ex.Status.ToString(), IsServerConnected));
        DataOutput.ErrorMessage = Resource.String(GetRequestResourceName(DataInput), DataInput.AccountId);
      }
      catch (Exception _ex)
      {
        DataOutput.ErrorMessage = Resource.String(GetRequestResourceName(DataInput), DataInput.AccountId);
        Log.Exception(_ex);
        DataOutput.ErrorMessage = Resource.String(GetRequestResourceName(DataInput), DataInput.AccountId);
      }

      return false;
    }

    /// <summary>
    /// Send request to every active WS until it found one that works
    /// </summary>
    /// <param name="DataInput"></param>
    /// <param name="Request"></param>
    /// <param name="DataOutput"></param>
    /// <param name="IsServerConnected"></param>
    /// <returns></returns>
    private Boolean SendRequestToAvailableWS(ExpedClientWSDataInput DataInput, XmlDocument Request, ref ExpedClientWSDataOutput DataOutput, out Boolean IsServerConnected)
    {
      List<String> _list_of_running_services_IP;

      IsServerConnected = false;

      //Get list of running services (available)
      if (!GetListOfRunningServicesIP(out _list_of_running_services_IP))
      {
        return false;
      }

      //Send request to the list of running services,
      foreach (String _IP_from_WCP_running in _list_of_running_services_IP)
      {
        //If it went ok then exit
        if (SendRequestToWS(_IP_from_WCP_running, DataInput, Request, ref DataOutput, out IsServerConnected))
        {
          //Save IP for next try
          ExternalPaymentAndSaleValidationCommon.ExpedWCPLastActiveIP = _IP_from_WCP_running;

          return true;
        }
      }

      return false;
    }

    /// <summary>
    /// Get list of running services IP
    /// </summary>
    /// <param name="ListOfRunningServicesIP"></param>
    /// <returns></returns>
    private Boolean GetListOfRunningServicesIP(out List<String> ListOfRunningServicesIP)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!Services.DB_GetRunningServices(SERVICE_RUNNING_EXPED_WS, _db_trx.SqlTransaction, out ListOfRunningServicesIP))
        {
          LogMessage("GetListOfRunningServicesIP: Not running services found for: {0}", SERVICE_RUNNING_EXPED_WS);

          return false;
        }
      }

      return true;
    }

    #endregion
  }

  /// <summary>
  /// Class that contains the info for a Request 
  /// </summary>    
  [Serializable, XmlRoot(ElementName = "ExpedClientRequest")]
  public class ExpedClientRequest
  {
    [XmlElementAttribute(ElementName = "type")]
    public String Type { get; set; }

    [XmlElementAttribute(ElementName = "ServerAddress")]
    public String ServerAddress { get; set; }

    [XmlElementAttribute(ElementName = "Domain")]
    public String Domain { get; set; }

    [XmlElementAttribute(ElementName = "User")]
    public String User { get; set; }

    [XmlElementAttribute(ElementName = "Password")]
    public String Password { get; set; }

    [XmlElementAttribute(ElementName = "TimeOutInMilliseconds")]
    public Int32 TimeOutInMilliseconds { get; set; }

    [XmlElementAttribute(ElementName = "OperationId")]
    public Int64 OperationId { get; set; }

    [XmlElementAttribute(ElementName = "AccountId")]
    public Int64 AccountId { get; set; }

    [XmlElementAttribute(ElementName = "NetAmount")]
    public Decimal NetAmount;

    [XmlElementAttribute(ElementName = "NetAmountSplitA")]
    public Decimal NetAmountSplitA;

    [XmlElementAttribute(ElementName = "NetAmountSplitB")]
    public Decimal NetAmountSplitB;

    [XmlElementAttribute(ElementName = "OperationType")]
    public TipoOperacion OperationType;

    [XmlElementAttribute(ElementName = "BusinessLineType")]
    public TipoLineaNegocio BusinessLineType;

    [XmlElementAttribute(ElementName = "SiteId")]
    public Int32 SiteId;

    [XmlElementAttribute(ElementName = "AuthorizationCode")]
    public String AuthorizationCode;

    [XmlElementAttribute(ElementName = "OperationDate")]
    public DateTime OperationDate;

    [XmlElementAttribute(ElementName = "AccountName")]
    public String AccountName;

    [XmlElementAttribute(ElementName = "TicketNumber")]
    public Int64 TicketNumber;

    [XmlElementAttribute(ElementName = "HtmlTicket")]
    public String HtmlTicket;

    [XmlElementAttribute(ElementName = "CheckNumber")]
    public String CheckNumber;

    [XmlElementAttribute(ElementName = "CheckAmount")]
    public Decimal? CheckAmount;
  }

  /// <summary>
  /// Class that contains the info of a Response
  /// <summary>    
  [Serializable, XmlRoot(ElementName = "ExpedClientResponse")]
  public class ExpedClientResponse
  {
    [XmlElementAttribute(ElementName = "type")]
    public String Type { get; set; }

    [XmlElementAttribute(ElementName = "CodeAuthorization")]
    public String CodeAuthorization { get; set; }

    [XmlElementAttribute(ElementName = "TransactionId")]
    public String TransactionId { get; set; }

    [XmlElementAttribute(ElementName = "ErrorCode")]
    public String ErrorCode { get; set; }

    [XmlElementAttribute(ElementName = "ErrorDescription")]
    public String ErrorDescription { get; set; }

    [XmlElementAttribute(ElementName = "OperationResult")]
    public EnumExternalPaymentAndSaleValidationOperationResult OperationResult { get; set; }
  }
}
