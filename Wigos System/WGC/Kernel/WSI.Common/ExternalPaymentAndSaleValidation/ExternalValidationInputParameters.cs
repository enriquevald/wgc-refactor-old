﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ExternalValidationInputParameters.cs
// 
//   DESCRIPTION: Input parameters for External Validation Operation
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 19-SEP-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-SEP-2016 FGB    First release (Header).
// 10-NOV-2016 FGB    Bug 20222: Exped: Connection with WCP service
// 09-MAR-2016 ATB    PBI 25262: Exped - Payment authorisation
// 13-MAR-2017 FAV    PBI 25265:Exped - Transaction authorization
//------------------------------------------------------------------------------
//
using System;

namespace WSI.Common.ExternalPaymentAndSaleValidation
{
  /// <summary>
  /// Class that holds the validation input parameters 
  /// </summary>
  public class ExternalValidationInputParameters
  {
    #region Attributes
    public Int64 AccountId;
    public Decimal NetAmount;
    public Decimal NetAmountSplitA;
    public Decimal NetAmountSplitB;
    public Decimal GrossAmount;
    public Int64 OperationId;
    public OperationCode OperationCode;
    public decimal BankCheckAmount;
    public string BankCheckNumber;
    public string AuthorizationCode;
    #endregion

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="NetAmount"></param>
    /// <param name="GrossAmount"></param>
    /// <param name="OperationId"></param>
    /// <param name="OperationCode"></param>
    public ExternalValidationInputParameters(Int64 AccountId, Decimal NetAmount, Decimal GrossAmount, Int64 OperationId, OperationCode OperationCode)
    {
      decimal _cash_in_split1 = 0;
      decimal _cash_in_split2 = 0;
      TYPE_SPLITS _splits;

      if (Split.ReadSplitParameters(out _splits) && NetAmount > 0)
      {
        // Calculate cash in splits
        if (OperationCode == OperationCode.CHIPS_SALE_WITH_RECHARGE)
        {

          _cash_in_split1 = Math.Round(NetAmount * _splits.company_a.chips_sale_pct / 100, 2, MidpointRounding.AwayFromZero);
          _cash_in_split2 = NetAmount - _cash_in_split1;
        }
        else
        {
          _cash_in_split1 = Math.Round(NetAmount * _splits.company_a.cashin_pct / 100, 2, MidpointRounding.AwayFromZero);
          _cash_in_split2 = NetAmount - _cash_in_split1;
        }
      }

      this.AccountId = AccountId;

      this.NetAmount = NetAmount;
      this.NetAmountSplitA = _cash_in_split1;
      this.NetAmountSplitB = _cash_in_split2;
      this.GrossAmount = GrossAmount;

      this.OperationId = OperationId;
      this.OperationCode = OperationCode;
    }

    /// <summary>
    /// Gets the Operation type (payment/sale) from the Operation Code
    /// </summary>
    /// <param name="OperationCode"></param>
    /// <returns></returns>
    public TipoOperacion GetOperationType()
    {
      return ExternalPaymentAndSaleValidationCommon.GetOperationTypeFromOperationCode(this.OperationCode);
    }

    /// <summary>
    /// Gets the Business Line type (terminals/gaming tables) from the Operation Code
    /// </summary>
    /// <param name="OperationCode"></param>
    /// <returns></returns>
    public TipoLineaNegocio GetBusinessLineType()
    {
      return ExternalPaymentAndSaleValidationCommon.GetBusinessLineTypeFromOperationCode(this.OperationCode);
    }
  }
}