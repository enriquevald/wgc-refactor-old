﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ExternalValidationOperationData.cs
// 
//   DESCRIPTION: Miscellaneous utilities for External Validation Operation
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 01-SEP-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-SEP-2016 FGB    First release (Header).
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 15-NOV-2016 FGB    BUG 20636: Exped: Faltan informar campos al WS
// 07-MAR-2017 FAV    PBI 25265: Exped - Transaction authorization
// 07-MAR-2016 ATB    PBI 25262: Exped - Payment authorisation
//------------------------------------------------------------------------------
//
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.ExternalPaymentAndSaleValidation
{
  public class ExternalValidationOperationData
  {
    public Int64 EvoId;
    public Int64 OperationId;
    public Int64 AccountId;
    public Decimal NetAmount;
    public Decimal NetAmountSplitA;
    public Decimal NetAmountSplitB;

    public TipoOperacion OperationType;
    public TipoLineaNegocio BusinessLineType;
    public EnumExternalPaymentAndSaleValidationStatus Status;
    public Int32 SiteId;

    public String AuthorizationCode;
    public DateTime OperationDate;
    public String AccountName;

    public Int64 TicketNumber;
    public String HtmlTicket;
    public String CheckNumber;
    public Decimal? CheckAmount;

    /// <summary>
    /// Load operation from DB
    /// </summary>
    /// <param name="EvoId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public Boolean LoadFromDB(Int64 EvoId, SqlTransaction Trx)
    {
      DataTable _dt_operation_data;
      DataRow _dr_evo;

      if (!ReadOperationsFromDB(EvoId, out _dt_operation_data, Trx))
      {
        return false;
      }

      if (_dt_operation_data == null)
      {
        return false;
      }

      if (_dt_operation_data.Rows.Count != 1)
      {
        return false;
      }

      try
      {
        _dr_evo = _dt_operation_data.Rows[0];

        this.EvoId = (Int64)_dr_evo["EVO_ID"];
        this.OperationId = (Int64)_dr_evo["EVO_OPERATION_ID"];
        this.AccountId = (Int64)_dr_evo["EVO_ACCOUNT_ID"];

        this.OperationType = (TipoOperacion)(Int32)_dr_evo["EVO_OPERATION_TYPE"];
        this.BusinessLineType = (TipoLineaNegocio)(Int32)_dr_evo["EVO_BUSINESS_LINE_TYPE"];
        
        this.Status = (EnumExternalPaymentAndSaleValidationStatus)(Int32)_dr_evo["EVO_STATUS"];
        this.SiteId = (Int32)_dr_evo["EVO_SITE_ID"];

        this.AuthorizationCode = _dr_evo["EVO_AUTHORIZATION_CODE"].ToString();

        this.OperationDate = (DateTime)_dr_evo["OPERATION_DATE"];
        this.AccountName = _dr_evo["ACCOUNT_NAME"].ToString();

        if (this.OperationType == TipoOperacion.Pago)
        {
          this.NetAmount = (Decimal)_dr_evo["EVO_NET_AMOUNT"];
          this.NetAmountSplitA = 0;
          this.NetAmountSplitB = 0;

          // Check fields
          this.CheckNumber = _dr_evo.IsNull("CHECK_NUMBER") ? String.Empty : _dr_evo["CHECK_NUMBER"].ToString();
          this.CheckAmount = _dr_evo.IsNull("CHECK_NUMBER") ? (Decimal?)null : (Decimal)_dr_evo["CHECK_AMOUNT"];
        }
        else if (this.OperationType == TipoOperacion.Venta)
        {
          this.NetAmount = 0;
          this.NetAmountSplitA = _dr_evo.IsNull("EVO_NET_AMOUNT_SPLIT_A") ? 0 : (Decimal)_dr_evo["EVO_NET_AMOUNT_SPLIT_A"];
          this.NetAmountSplitB = _dr_evo.IsNull("EVO_NET_AMOUNT_SPLIT_B") ? 0 : (Decimal)_dr_evo["EVO_NET_AMOUNT_SPLIT_B"];

          // Check fields
        this.CheckNumber = String.Empty;
        this.CheckAmount = null;
        }

        //Voucher fields
        this.TicketNumber = (Int64)_dr_evo["VOUCHER_ID"];
        this.HtmlTicket = String.Empty;
        if (!_dr_evo.IsNull("VOUCHER_HTML"))
        {
          this.HtmlTicket = String.Format("<![CDATA[{0}]]", _dr_evo["VOUCHER_HTML"].ToString());
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Read operation data from DB
    /// </summary>
    /// <param name="EvoId"></param>
    /// <param name="DtOperationData"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private Boolean ReadOperationsFromDB(Int64 EvoId, out DataTable DtOperationData, SqlTransaction Trx)
    {
      StringBuilder _sb;

      DtOperationData = new DataTable();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("          SELECT   EVO_ID                                                     ");
        _sb.AppendLine("               ,   EVO_OPERATION_ID                                           ");
        _sb.AppendLine("               ,   EVO_ACCOUNT_ID                                             ");

        _sb.AppendLine("               ,   EVO_NET_AMOUNT                                             ");
        _sb.AppendLine("               ,   EVO_GROSS_AMOUNT                                           ");

        _sb.AppendLine("               ,   EVO_NET_AMOUNT_SPLIT_A                                     ");
        _sb.AppendLine("               ,   EVO_NET_AMOUNT_SPLIT_B                                     ");

        _sb.AppendLine("               ,   EVO_OPERATION_TYPE                                         ");
        _sb.AppendLine("               ,   EVO_BUSINESS_LINE_TYPE                                     ");
        _sb.AppendLine("               ,   EVO_SITE_ID                                                ");

        _sb.AppendLine("               ,   EVO_AUTHORIZATION_CODE                                     ");
        _sb.AppendLine("               ,   EVO_STATUS                                                 ");

        //Check fields
        _sb.AppendLine("               ,   EVO_BANKCHECK_NUMBER as  CHECK_NUMBER                      ");
        _sb.AppendLine("               ,   EVO_BANKCHECK_AMOUNT as  CHECK_AMOUNT                      ");

        //Operations
        _sb.AppendLine("               ,   AO_DATETIME          as  OPERATION_DATE                    ");

        //Accounts
        _sb.AppendLine("               ,   AC_HOLDER_NAME       as  ACCOUNT_NAME                      ");

        //Voucher fields
        //Min ID from vouchers
        _sb.AppendLine("               ,   (SELECT  MIN(ST1.CV_VOUCHER_ID)                            ");
        _sb.AppendLine("                      FROM  CASHIER_VOUCHERS ST1                              ");
        _sb.AppendLine("                     WHERE  ST1.CV_OPERATION_ID = EVO_OPERATION_ID            ");
        _sb.AppendLine("                       AND  ST1.CV_HTML IS NOT NULL                           ");
        _sb.AppendLine("                   )                    as  VOUCHER_ID                        ");
        //Concatenade HTML from vouchers
        _sb.AppendLine("               ,   (SELECT  ST1.cv_html  AS [text()]                          ");
        _sb.AppendLine("                      FROM  CASHIER_VOUCHERS ST1                              ");
        _sb.AppendLine("                     WHERE  ST1.CV_OPERATION_ID = EVO_OPERATION_ID            ");
        _sb.AppendLine("                       AND  ST1.CV_HTML IS NOT NULL                           ");
        _sb.AppendLine("                  ORDER BY  ST1.cv_voucher_id                                 ");
        _sb.AppendLine("                   FOR XML  PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'        ");
        _sb.AppendLine("                   )                    as  VOUCHER_HTML                      ");

        _sb.AppendLine("            FROM   EXTERNAL_VALIDATION_OPERATION                              ");
        _sb.AppendLine(" LEFT OUTER JOIN   ACCOUNT_OPERATIONS   on AO_OPERATION_ID = EVO_OPERATION_ID ");
        _sb.AppendLine(" LEFT OUTER JOIN   ACCOUNTS             on AC_ACCOUNT_ID = EVO_ACCOUNT_ID     ");
        _sb.AppendLine("           WHERE   EVO_ID = @pEvoId                                           ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pEvoId", SqlDbType.BigInt).Value = EvoId;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(DtOperationData);

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }
  }
}