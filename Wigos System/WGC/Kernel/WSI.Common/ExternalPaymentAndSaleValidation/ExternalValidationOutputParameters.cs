﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ExternalValidationOutputParameters.cs
// 
//   DESCRIPTION: Output parameters for External Validation Operation
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 19-SEP-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-SEP-2016 FGB    First release (Header).
// 10-NOV-2016 FGB    Bug 20222: Exped: Connection with WCP service
//------------------------------------------------------------------------------
//
using System;

namespace WSI.Common.ExternalPaymentAndSaleValidation
{
  /// <summary>
  /// Class that holds the validation output parameters 
  /// </summary>
  public class ExternalValidationOutputParameters
  {
    #region Attributes
    public EnumExternalPaymentAndSaleValidationResult ValidationResult;
    public String ValidationMessage;
    public String AuthorizationCode;
    public Int64? ExternalValidationOperationId;
    public String TransactionId;
    public EnumExternalPaymentAndSaleValidationStatus Status;
    #endregion

    #region Constructor
    public ExternalValidationOutputParameters(EnumExternalPaymentAndSaleValidationResult ValidationResult, String Message)
    {
      this.ValidationResult = ValidationResult;
      this.ValidationMessage = Message;
      this.AuthorizationCode = String.Empty;
      this.ExternalValidationOperationId = null;
      this.TransactionId = String.Empty;
      this.Status = EnumExternalPaymentAndSaleValidationStatus.STATUS_UNKNOWN;
    }

    public ExternalValidationOutputParameters(EnumExternalPaymentAndSaleValidationResult ValidationResult)
      : this(ValidationResult, String.Empty)
    {
    }

    public ExternalValidationOutputParameters()
      : this(EnumExternalPaymentAndSaleValidationResult.RESULT_UNKNOWN, String.Empty)
    {
    }
    #endregion
  }
}
