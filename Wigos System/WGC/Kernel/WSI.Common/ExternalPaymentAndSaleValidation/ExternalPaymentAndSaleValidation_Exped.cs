﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ExternalPaymentAndSaleValidation_Exped.cs
// 
//   DESCRIPTION: Miscellaneous utilities for External Validation Operation
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 01-SEP-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-SEP-2016 FGB    First release (Header).
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 10-NOV-2016 FGB    Bug 20222: Exped: Connection with WCP service
// 07-MAR-2016 ATB    PBI 25262: Exped - Payment authorisation
// 09-MAR-2016 ATB    PBI 25262: Exped - Payment authorisation
// 13-MAR-2016 ATB    PBI 25262: Exped - Payment authorisation
// 15-MAR-2017 ATB    PBI 25262: Exped - Payment authorisation
// 24-MAR-2017 FAV    PBI 25265: Exped - Transaction authorization
// 05-APR-2017 FAV    PBI 25265: Exped - Transaction authorization
//------------------------------------------------------------------------------
//
using System;
using System.Data;
using System.Data.SqlClient;


namespace WSI.Common.ExternalPaymentAndSaleValidation
{
  public class ExternalPaymentAndSaleValidation_Exped : IExternalPaymentAndSaleValidation
  {
    #region "Public Methods"
    /// <summary>
    /// Validates Account at the Exped WebService
    /// </summary>
    /// <param name="InputParams"></param>
    /// <param name="OutputParams"></param>
    /// <returns></returns>
    public Boolean RequestAuthorization(ExternalValidationInputParameters InputParams
                                       , out ExternalValidationOutputParameters OutputParams)
    {
      Int64 _account_id;
      String _validation_message;
      String _authorization_code;
      Boolean _ws_connected;
      ExpedClientWS _exped_client_WS;
      EnumExternalPaymentAndSaleValidationResult _validation_result;

      OutputParams = new ExternalValidationOutputParameters();
      _authorization_code = String.Empty;
      _validation_message = String.Empty;


      try
      {
        _account_id = InputParams.AccountId;

        _exped_client_WS = new ExpedClientWS();

        if (!_exped_client_WS.GetAuthorization(_account_id, out _authorization_code, out _validation_message, out _ws_connected))
        {
          if (_ws_connected)
          {
            if (_validation_message == ExternalPaymentAndSaleValidationCommon.VALIDATION_MESSAGE_ACCOUNT_NOT_AUTHORIZED
            || _validation_message == ExternalPaymentAndSaleValidationCommon.VALIDATION_MESSAGE_ACCOUNT_WIN_NOT_VALID)
            {
              _validation_result = EnumExternalPaymentAndSaleValidationResult.RESULT_NOT_AUTHORIZED;
            }
            else
            {
              _validation_result = EnumExternalPaymentAndSaleValidationResult.RESULT_UNKNOWN;
            }
          }
          else
          {
            _validation_message = "The Exped Web service has not responded";
            _validation_result = EnumExternalPaymentAndSaleValidationResult.RESULT_ERROR_CONNECTION;
          }
        }
        else
        {
          _validation_result = EnumExternalPaymentAndSaleValidationResult.RESULT_AUTHORIZED;
          OutputParams.Status = EnumExternalPaymentAndSaleValidationStatus.STATUS_AUTHORIZED;
        }
      }
      catch (Exception _ex)
      {
        _validation_message = _ex.Message;
        _validation_result = EnumExternalPaymentAndSaleValidationResult.RESULT_UNKNOWN;
        Log.Exception(_ex);
      }

      OutputParams.AuthorizationCode = _authorization_code;
      OutputParams.ValidationMessage = _validation_message;
      OutputParams.ValidationResult = _validation_result;

      Misc.ExternalWS.InputParams = InputParams;
      Misc.ExternalWS.OutputParams = OutputParams;

      return (OutputParams.ValidationResult == EnumExternalPaymentAndSaleValidationResult.RESULT_AUTHORIZED);
    }

    /// <summary>
    /// Saves at database the params's info (an internal log)
    /// </summary>
    /// <param name="InputParams"></param>
    /// <param name="OutputParams"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public Boolean ExternalPaymentAndSaleValidation_GetAuthorization(ExternalValidationInputParameters InputParams, ExternalValidationOutputParameters OutputParams, SqlTransaction Trx)
    {
      Boolean _operation_validated;

      // Only in case of recharge, we have to request authorization to Exped
      if (InputParams.OperationCode != OperationCode.CASH_OUT)
      {
        //Get Account Authorization from WS
        _operation_validated = RequestAuthorization(InputParams
                                                  , out OutputParams);
      }
      else
      {
        _operation_validated = true;
      }
      //Get status fromn result
      if (_operation_validated)
      {
        //Insert Log
        ExternalValidationOperationLog _external_validation_operation;
        _external_validation_operation = new ExternalValidationOperationLog();
        if (!_external_validation_operation.InsertLog(InputParams, OutputParams, Trx))
        {
          return false;
        }
      }
      return _operation_validated;
    }

    /// <summary>
    /// Process the pending transactions
    /// </summary>
    /// <param name="NumberOfRecordsToProcess"></param>
    /// <returns></returns>
    public Boolean ExternalPaymentAndSaleValidation_ProcessPendingTransactions(Int32 NumberOfRecordsToProcess)
    {
      DataTable _dt_pending_transactions;

      try
      {
        if (!GetPendingOperations(NumberOfRecordsToProcess, out _dt_pending_transactions))
        {
          return false;
        }

        ProcessPendingOperations(_dt_pending_transactions);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("ExternalPaymentAndSaleValidation_Exped.ExternalPaymentAndSaleValidation_ProcessPendingTransactions: error {0}"
                               , _ex.Message));
      }

      return false;
    }
    #endregion

    #region "Private Methods"
    /// <summary>
    /// Returns a DataTable with the pending operations
    /// </summary>
    /// <param name="NumberOfRecordsToProcess"></param>
    /// <param name="DtPendingOperations"></param>
    /// <returns></returns>
    private Boolean GetPendingOperations(Int32 NumberOfRecordsToProcess, out DataTable DtPendingOperations)
    {
      ExternalValidationOperationLog _epv_log;

      DtPendingOperations = new DataTable();

      try
      {
        _epv_log = new ExternalValidationOperationLog();

        _epv_log.ReadOperationsToConfirm(NumberOfRecordsToProcess, out DtPendingOperations);

        return (DtPendingOperations.Rows.Count != 0);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Process the pending operations 
    /// </summary>
    /// <param name="DtPendingOperations"></param>
    private void ProcessPendingOperations(DataTable DtPendingOperations)
    {
      foreach (DataRow _row in DtPendingOperations.Rows)
      {
        ProcessExpedRegistration(_row);
      }
    }

    /// <summary>
    /// Process a pending operation
    /// </summary>
    /// <param name="RowPendingOperation"></param>
    /// <returns></returns>
    private Boolean ProcessExpedRegistration(DataRow RowPendingOperation)
    {
      EnumExternalPaymentAndSaleValidationOperationResult _registration_result;
      Int64 _evo_id;
      String _transaction_id;
      String _error_code;
      String _error_description;
      EnumExternalPaymentAndSaleValidationStatus _new_status;

      try
      {
        _evo_id = (Int64)RowPendingOperation[ExternalValidationOperationLog.GetEvoIdColumnName()];

        using (DB_TRX _db_trx = new DB_TRX())
        {
          //Register operation
          _registration_result = RegisterOperation(_evo_id, out _transaction_id, out _error_code, out _error_description, _db_trx.SqlTransaction);

          if (_registration_result == EnumExternalPaymentAndSaleValidationOperationResult.OPERATION_RESULT_ERROR)
          {
            //Normally it is a connection error, has to retry later
            return false;
          }

          //Get new status from OperationResult
          _new_status = GetOperationResultNewStatus(_registration_result);

          if (!UpdateStatusOperation(_evo_id, _transaction_id, _new_status, _error_code, _error_description, _db_trx.SqlTransaction))
          {
            return false;
          }

          _db_trx.Commit();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Returns new status from OperationResult
    /// </summary>
    /// <param name="OperationResult"></param>
    /// <returns></returns>
    private EnumExternalPaymentAndSaleValidationStatus GetOperationResultNewStatus(EnumExternalPaymentAndSaleValidationOperationResult OperationResult)
    {
      EnumExternalPaymentAndSaleValidationStatus _new_status;

      switch (OperationResult)
      {
        case EnumExternalPaymentAndSaleValidationOperationResult.OPERATION_RESULT_OK:
          //It is Ok, set to confirmed
          _new_status = EnumExternalPaymentAndSaleValidationStatus.STATUS_CONFIRMED;

          break;
        case EnumExternalPaymentAndSaleValidationOperationResult.OPERATION_RESULT_FAULT:
          //Fault, it can not be registered, set to ErrorAuthorized, not to be processed again.
          _new_status = EnumExternalPaymentAndSaleValidationStatus.STATUS_ERROR_AUTHORIZED;

          break;
        case EnumExternalPaymentAndSaleValidationOperationResult.OPERATION_RESULT_ERROR:
        default:

          throw new ArgumentOutOfRangeException();
      }

      return _new_status;
    }

    /// <summary>
    /// Registers the transaction (at the Exped WS) of the pending operation 
    /// </summary>
    /// <param name="EvoId"></param>
    /// <param name="TransactionId"></param>
    /// <param name="ErrorCode"></param>
    /// <param name="ErrorDescription"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private EnumExternalPaymentAndSaleValidationOperationResult RegisterOperation(Int64 EvoId, out String TransactionId, out String ErrorCode, out String ErrorDescription, SqlTransaction Trx)
    {
      Boolean _result;
      ExternalValidationOperationData _evo_data;
      ExpedClientWS _exped_client_WS;
      ExpedClientWSDataOutput _data_output;
      String _error_message;

      TransactionId = String.Empty;
      ErrorCode = String.Empty;
      ErrorDescription = String.Empty;
      _error_message = String.Empty;

      try
      {
        _evo_data = new ExternalValidationOperationData();

        //Get data to register
        if (!_evo_data.LoadFromDB(EvoId, Trx))
        {
          Log.Error(String.Format("ExternalPaymentAndSaleValidation_Exped.RegisterOperation: Could not load data from DB for EvoId {0}", EvoId));

          return EnumExternalPaymentAndSaleValidationOperationResult.OPERATION_RESULT_ERROR;
        }

        //Register transaction in WS
        _exped_client_WS = new ExpedClientWS();

        //Output
        _data_output = new ExpedClientWSDataOutput();

        //Register Transaction
        _result = _exped_client_WS.RegisterTransaction(_evo_data, ref _data_output);

        TransactionId = _data_output.ExpedResponse.TransactionId;
        ErrorCode = _data_output.ExpedResponse.ErrorCode;
        ErrorDescription = _data_output.ExpedResponse.ErrorDescription;
        _error_message = _data_output.ErrorMessage;

        if (!_result)
        {
          return EnumExternalPaymentAndSaleValidationOperationResult.OPERATION_RESULT_ERROR;
        }

        if (_data_output.ExpedResponse.OperationResult != EnumExternalPaymentAndSaleValidationOperationResult.OPERATION_RESULT_OK)
        {
          if (String.IsNullOrEmpty(_error_message))
          {
            _error_message = ErrorDescription;
          }

          Log.Error(String.Format("ExternalPaymentAndSaleValidation_Exped.RegisterOperation: Could not register EvoId {0} - {1}", EvoId, _error_message));
        }

        return _data_output.ExpedResponse.OperationResult;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return EnumExternalPaymentAndSaleValidationOperationResult.OPERATION_RESULT_ERROR;
    }

    /// <summary>
    /// Updates the operation status from pending to confirmed
    /// </summary>
    /// <param name="EvoId"></param>
    /// <param name="TransactionId"></param>
    /// <param name="NewStatus"></param>
    /// <param name="ErrorCode"></param>
    /// <param name="ErrorDescription"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private Boolean UpdateStatusOperation(Int64 EvoId, String TransactionId, EnumExternalPaymentAndSaleValidationStatus NewStatus, String ErrorCode, String ErrorDescription, SqlTransaction Trx)
    {
      ExternalValidationOperationLog _epv_log;
      _epv_log = new ExternalValidationOperationLog();

      return _epv_log.SetOperationStatus(EvoId
                                       , NewStatus
                                       , EnumExternalPaymentAndSaleValidationStatus.STATUS_AUTHORIZED  //Old status
                                       , TransactionId
                                       , ErrorCode
                                       , ErrorDescription
                                       , Trx);
    }

    #endregion
  }
}