﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ExternalValidationOperationLog.cs
// 
//   DESCRIPTION: Miscellaneous utilities for External Validation Operation
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 01-SEP-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-SEP-2016 FGB    First release (Header).
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 10-NOV-2016 FGB    Bug 20222: Exped: Connection with WCP service
// 09-MAR-2016 ATB    PBI 25262: Exped - Payment authorisation
// 13-MAR-2017 FAV    PBI 25265: Exped - Transaction authorization
// 15-MAR-2017 FAV    PBI 25262: Exped - Payment authorisation
//------------------------------------------------------------------------------
//
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common.ExternalPaymentAndSaleValidation
{
  public class ExternalValidationOperationLog
  {
    private const String EVO_ID_COLUMN = "EVO_ID";

    // EVO COLUMNS
    public static String GetEvoIdColumnName()
    {
      return EVO_ID_COLUMN;
    }

    /// <summary>
    /// Insert Log in ExternalValidationOperation
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="OperationId"></param>
    /// <param name="NetAmount"></param>
    /// <param name="GrossAmount"></param>
    /// <param name="AuthorizationCode"></param>
    /// <param name="Status"></param>
    /// <param name="OperationType"></param>
    /// <param name="BusinessLineType"></param>
    /// <param name="SiteId"></param>
    /// <param name="EvoId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public Boolean InsertLog(Int64 AccountId
                           , Int64 OperationId
                           , Decimal NetAmount
                           , Decimal GrossAmount
                           , String AuthorizationCode
                           , EnumExternalPaymentAndSaleValidationStatus Status
                           , TipoOperacion OperationType
                           , TipoLineaNegocio BusinessLineType
                           , Int32 SiteId
                           , Int64? EvoId
                           , SqlTransaction Trx
                           , Decimal NetAmountSplitA
                           , Decimal NetAmountSplitB
                           , decimal BankCheckAmount
                           , string BankCheckNumber)
    {
      StringBuilder _sb;
      SqlParameter _output_param;

      EvoId = -1;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO   EXTERNAL_VALIDATION_OPERATION  ");
        _sb.AppendLine("             (                                ");
        _sb.AppendLine("               EVO_OPERATION_ID               ");
        _sb.AppendLine("             , EVO_ACCOUNT_ID                 ");
        _sb.AppendLine("             , EVO_NET_AMOUNT                 ");
        _sb.AppendLine("             , EVO_GROSS_AMOUNT               ");

        _sb.AppendLine("             , EVO_OPERATION_TYPE             ");
        _sb.AppendLine("             , EVO_BUSINESS_LINE_TYPE         ");
        _sb.AppendLine("             , EVO_SITE_ID                    ");

        _sb.AppendLine("             , EVO_AUTHORIZATION_CODE         ");

        _sb.AppendLine("             , EVO_STATUS                     ");
        _sb.AppendLine("             , EVO_CREATED_DATE               ");

        _sb.AppendLine("             , EVO_NET_AMOUNT_SPLIT_A         ");
        _sb.AppendLine("             , EVO_NET_AMOUNT_SPLIT_B         ");

        _sb.AppendLine("             , EVO_BANKCHECK_AMOUNT           ");
        _sb.AppendLine("             , EVO_BANKCHECK_NUMBER           ");

        _sb.AppendLine("             )                                ");
        _sb.AppendLine("      VALUES                                  ");
        _sb.AppendLine("             (                                ");
        _sb.AppendLine("               @pOperationId                  ");
        _sb.AppendLine("             , @pAccountId                    ");
        _sb.AppendLine("             , @pNetAmount                    ");
        _sb.AppendLine("             , @pGrossAmount                  ");

        _sb.AppendLine("             , @pOperationType                ");
        _sb.AppendLine("             , @pBusinessLineType             ");
        _sb.AppendLine("             , @pSiteId                       ");

        _sb.AppendLine("             , @pAuthorizationCode            ");

        _sb.AppendLine("             , @pStatus                       ");
        _sb.AppendLine("             , GETDATE()                      ");
        _sb.AppendLine("             , @pNetAmountSplitA              ");
        _sb.AppendLine("             , @pNetAmountSplitB              ");
        _sb.AppendLine("             , @pBankCheckAmount              ");
        _sb.AppendLine("             , @pBankCheckNumber              ");

        _sb.AppendLine("             )                                ");
        _sb.AppendLine("                                              ");
        _sb.AppendLine(" SET @pEVOId = SCOPE_IDENTITY()               ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pOperationId", SqlDbType.Int).Value = OperationId;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          if (OperationType == TipoOperacion.Pago)
          {
          _cmd.Parameters.Add("@pNetAmount", SqlDbType.Decimal).Value = NetAmount;
            _cmd.Parameters.Add("@pNetAmountSplitA", SqlDbType.Decimal).Value = DBNull.Value;
            _cmd.Parameters.Add("@pNetAmountSplitB", SqlDbType.Decimal).Value = DBNull.Value;

            _cmd.Parameters.Add("@pBankCheckAmount", SqlDbType.Money).Value = BankCheckAmount;

            if (string.IsNullOrEmpty(BankCheckNumber))
            {
              _cmd.Parameters.Add("@pBankCheckNumber", SqlDbType.NVarChar).Value = string.Empty;
            }
            else
            {
              _cmd.Parameters.Add("@pBankCheckNumber", SqlDbType.NVarChar).Value = BankCheckNumber;
            }
          }
          else
          {
            _cmd.Parameters.Add("@pNetAmount", SqlDbType.Decimal).Value = 0;
            _cmd.Parameters.Add("@pNetAmountSplitA", SqlDbType.Decimal).Value = NetAmountSplitA;
            _cmd.Parameters.Add("@pNetAmountSplitB", SqlDbType.Decimal).Value = NetAmountSplitB;

            _cmd.Parameters.Add("@pBankCheckAmount", SqlDbType.Money).Value = DBNull.Value;
            _cmd.Parameters.Add("@pBankCheckNumber", SqlDbType.NVarChar).Value = DBNull.Value;
          }

          _cmd.Parameters.Add("@pGrossAmount", SqlDbType.Decimal).Value = GrossAmount;
          _cmd.Parameters.Add("@pOperationType", SqlDbType.Int).Value = (TipoOperacion)(Int32)OperationType;
          _cmd.Parameters.Add("@pBusinessLineType", SqlDbType.Int).Value = (TipoLineaNegocio)(Int32)BusinessLineType;
          _cmd.Parameters.Add("@pSiteId", SqlDbType.Int).Value = SiteId;
          _cmd.Parameters.Add("@pAuthorizationCode", SqlDbType.NVarChar).Value = AuthorizationCode;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Status;

          //Output parameter
          _output_param = _cmd.Parameters.Add("@pEVOId", SqlDbType.BigInt);
          _output_param.Direction = ParameterDirection.Output;

          //Get results of the insert
          if (_cmd.ExecuteNonQuery() == 1)
          {
            if (_output_param.Value != DBNull.Value)
            {
              EvoId = (Int64)_output_param.Value; //Get ID from inserted row 
            }

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("ExternalValidationOperationLog.InsertLog: AccountId: {0} - OperationId: {1} - NetAmount: {2}", AccountId, OperationId, NetAmount));
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Insert Log in ExternalValidationOperation    
    /// </summary>
    /// <param name="InputParams"></param>
    /// <param name="OutputParams"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public Boolean InsertLog(ExternalValidationInputParameters InputParams
                           , ExternalValidationOutputParameters OutputParams
                           , SqlTransaction Trx)
    {
      Int64 _account_id;
      Int64 _operation_id;
      Int32 _site_id;
      Decimal _net_amount;
      Decimal _net_amountSplitA;
      Decimal _net_amountSplitB;
      Decimal _gross_amount;
      Decimal _bank_check_amount;
      String _bank_check_number;

      TipoOperacion _operation_type;
      TipoLineaNegocio _business_line_type;

      _account_id = InputParams.AccountId;
      _operation_id = InputParams.OperationId;

      _net_amount = InputParams.NetAmount;
      _net_amountSplitA = InputParams.NetAmountSplitA;
      _net_amountSplitB = InputParams.NetAmountSplitB;
      _gross_amount = InputParams.GrossAmount;

      _bank_check_amount = InputParams.BankCheckAmount;
      _bank_check_number = InputParams.BankCheckNumber;

      _operation_type = InputParams.GetOperationType();
      _business_line_type = InputParams.GetBusinessLineType();
      Int32.TryParse(Misc.ReadGeneralParams("Site", "Identifier"), out _site_id);

      return InsertLog(_account_id
                     , _operation_id
                     , _net_amount
                     , _gross_amount
                     , OutputParams.AuthorizationCode
                     , OutputParams.Status
                     , _operation_type
                     , _business_line_type
                     , _site_id
                     , OutputParams.ExternalValidationOperationId
                     , Trx
                     , _net_amountSplitA
                     , _net_amountSplitB
                     , _bank_check_amount
                     , _bank_check_number);
    }

    /// <summary>
    /// Update Log in EXTERNAL_VALIDATION_OPERATION table and sets the AuthorizationCode
    /// </summary>
    /// <param name="EvoId"></param>
    /// <param name="NewStatus"></param>
    /// <param name="OldStatus"></param>
    /// <param name="TransactionId"></param>
    /// <param name="ErrorCode"></param>
    /// <param name="ErrorDescription"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public Boolean SetOperationStatus(Int64 EvoId
                                    , EnumExternalPaymentAndSaleValidationStatus NewStatus
                                    , EnumExternalPaymentAndSaleValidationStatus OldStatus
                                    , String TransactionId
                                    , String ErrorCode
                                    , String ErrorDescription
                                    , SqlTransaction Trx)
    {
      StringBuilder _sb;
      String _transaction_id;
      Boolean _has_transaction;

      try
      {
        _transaction_id = TransactionId ?? String.Empty;
        _transaction_id = _transaction_id.Trim();
        _has_transaction = (!String.IsNullOrEmpty(_transaction_id));

        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE  EXTERNAL_VALIDATION_OPERATION                                  ");
        _sb.AppendLine("    SET  EVO_STATUS = @pNewStatus                                       ");

        if (_has_transaction)
        {
          _sb.AppendLine("       , EVO_TRANSACTION_ID = @pTransactionId                           ");
        }
        else
        {
          _sb.AppendLine("       , EVO_TRANSACTION_ID = NULL                                      ");
        }

        _sb.AppendLine("       , EVO_REGISTER_ERROR_CODE = @pRegisterErrorCode                  ");
        _sb.AppendLine("       , EVO_REGISTER_ERROR_DESCRIPTION = @pRegisterErrorDescription    ");
        _sb.AppendLine("       , EVO_UPDATED_DATE = GETDATE()                                   ");
        _sb.AppendLine("  WHERE  EVO_ID = @pEVOId                                               ");
        _sb.AppendLine("    AND  EVO_STATUS = @pOldStatus                                       ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pEVOId", SqlDbType.BigInt).Value = EvoId;         //PK parameter
          _cmd.Parameters.Add("@pOldStatus", SqlDbType.Int).Value = OldStatus;    //To ensure it has not been modified by another process

          _cmd.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = NewStatus;

          if (_has_transaction)
          {
            _cmd.Parameters.Add("@pTransactionId", SqlDbType.NVarChar).Value = TransactionId;
          }

          _cmd.Parameters.Add("@pRegisterErrorCode", SqlDbType.NVarChar).Value = ErrorCode;
          _cmd.Parameters.Add("@pRegisterErrorDescription", SqlDbType.NVarChar).Value = ErrorDescription;

          //Get results of the update
          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("ExternalValidationOperation.SetOperationStatus -> EvoId: {0} - TransactionId: {1}", EvoId, TransactionId));
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Get the Operations to confirm
    /// </summary>
    /// <param name="NumberOfRecordsToProcess"></param>
    /// <param name="DtOperationsToConfirms"></param>
    /// <returns></returns>
    public Boolean ReadOperationsToConfirm(Int32 NumberOfRecordsToProcess, out DataTable DtOperationsToConfirms)
    {
      StringBuilder _sb;

      DtOperationsToConfirms = new DataTable();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("   SELECT   TOP  " + NumberOfRecordsToProcess.ToString());
        _sb.AppendLine("            " + GetEvoIdColumnName());
        _sb.AppendLine("     FROM   EXTERNAL_VALIDATION_OPERATION");
        _sb.AppendLine("    WHERE   EVO_STATUS = @pStatusAuthorized");
        _sb.AppendLine(" ORDER BY   " + GetEvoIdColumnName() + " ASC ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pStatusAuthorized", SqlDbType.Int).Value = EnumExternalPaymentAndSaleValidationStatus.STATUS_AUTHORIZED;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(DtOperationsToConfirms);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }
  }
}