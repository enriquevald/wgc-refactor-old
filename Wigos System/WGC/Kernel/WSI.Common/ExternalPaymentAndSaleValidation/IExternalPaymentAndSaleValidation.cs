﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: IExternalPaymentAndSaleValidation.cs
// 
//   DESCRIPTION: Miscellaneous utilities for External Validation Operation
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 01-SEP-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-SEP-2016 FGB    First release (Header).
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 09-MAR-2016 ATB    PBI 25262: Exped - Payment authorisation
//------------------------------------------------------------------------------
//
using System;
using System.Data.SqlClient;

namespace WSI.Common.ExternalPaymentAndSaleValidation
{
  /// <summary>
  /// Interface for the External Validation
  /// </summary>
  public interface IExternalPaymentAndSaleValidation
  {
    Boolean ExternalPaymentAndSaleValidation_GetAuthorization(ExternalValidationInputParameters InputParams, ExternalValidationOutputParameters OutputParams, SqlTransaction Trx);
    Boolean ExternalPaymentAndSaleValidation_ProcessPendingTransactions(Int32 NumberOfRecordsToProcess);
  }
}