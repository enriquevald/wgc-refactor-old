﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ExternalValidationEnums.cs
// 
//   DESCRIPTION: Miscellaneous utilities for External Validation Operation
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 01-SEP-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-SEP-2016 FGB    First release (Header).
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 10-NOV-2016 FGB    Bug 20222: Exped: Connection with WCP service
// 06-MAR-2016 ATB    PBI 25262: Exped - Payment authorisation
//------------------------------------------------------------------------------
//
using System;
using System.Data.SqlClient;
using WSI.Common;

namespace WSI.Common.ExternalPaymentAndSaleValidation
{
  public enum TipoOperacion
  {
    Venta,
    Pago,
  }

  public enum TipoLineaNegocio
  {
    Terminales,
    Mesas,
  }

  /// <summary>
  /// Enumeration of result types of the external validation
  /// </summary>
  public enum EnumExternalPaymentAndSaleValidationResult
  {
    RESULT_UNKNOWN = -1,
    //Result
    RESULT_AUTHORIZED = 0,
    RESULT_NOT_AUTHORIZED = 1,
    //Validation not needed
    RESULT_VALIDATION_DISABLED = 10000,
    RESULT_AMOUNT_DOES_NOT_REACH_LIMIT = 10001,
    RESULT_CASINO_MODE_IS_NOT_CASHLESS = 10002,
    RESULT_CASINO_MODE_OPERATION_CODE_NOT_TO_VALIDATE = 10003,
    //Error
    RESULT_ERROR_CONNECTION = 10005,
  }

  /// <summary>
  /// Enumeration of Validation Modes
  /// </summary>
  public enum EnumExternalPaymentAndSaleValidationMode
  {
    MODE_UNKNOWN = -1,
    MODE_DISABLED = 0,
    MODE_EXPED = 1
  }
 
  /// <summary>
  /// Enumeration of type of Validation Status
  /// </summary>
  public enum EnumExternalPaymentAndSaleValidationStatus
  {
    STATUS_UNKNOWN = 0,
    STATUS_AUTHORIZED = 1,
    STATUS_CONFIRMED = 2,
    STATUS_ERROR_AUTHORIZED = -1
  }

  /// <summary>
  /// Enumeration of type of Log modes
  /// </summary>
  public enum EnumExternalPaymentAndSaleValidationLogMode
  {
    LOG_MODE_DISABLED = 0,
    LOG_MODE_ENABLED = 1
  }

  /// <summary>
  /// Enumeration of type Operation result
  /// </summary>
  public enum EnumExternalPaymentAndSaleValidationOperationResult
  {
    OPERATION_RESULT_OK = 0,
    OPERATION_RESULT_FAULT = 1,
    OPERATION_RESULT_ERROR = 2
  }

  /// <summary>
  /// Enumeration of type Operation method
  /// </summary>
  public enum EnumExternalPaymentAndSaleValidationOperationMethod
  {
    METHOD_GET_AUTHORIZATION = 0,
    METHOD_REGISTER_TRANSACTION_RECHARGE = 1,
    METHOD_REGISTER_TRANSACTION_REFUND = 2,
  }

  /// <summary>
  /// Enumeration of Operation phase
  /// </summary>
  public enum EnumExternalPaymentAndSaleValidationOperationPhase
  {
    PHASE_SENT = 0,
    PHASE_RECEIVED = 1,
    PHASE_ERROR = 2,
  }
}