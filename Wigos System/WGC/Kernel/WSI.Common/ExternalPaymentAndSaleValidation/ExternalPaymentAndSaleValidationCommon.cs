﻿//------------------------------------------------------------------------------
// Copyright © 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ExternalPaymentAndSaleValidationCommon.cs
// 
//   DESCRIPTION: Procedures for System Exped: Check if the payment/sale can be done
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 19-SEP-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-SEP-2016 FGB    First release (Header).
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 10-NOV-2016 FGB    Bug 20222: Exped: Connection with WCP service
// 07-MAR-2017 FGB    PBI 25262: Exped - Payment authorisation
// 24-MAR-2017 ATB    PBI 25262: Exped - Payment authorisation
// 05-APR-2017 FAV    PBI 25265: Exped - Transaction authorization
//------------------------------------------------------------------------------
//
using System;
using System.Data.SqlClient;
using WSI.Common.ExternalPaymentAndSaleValidation;

namespace WSI.Common
{
  public static class ExternalPaymentAndSaleValidationCommon
  {
    #region "Constants"
    private const String GP_GROUP_KEY_EXTERNAL_PAYMENT_VALIDATION = "ExternalPaymentAndSaleValidation";

    private const String GP_SUBJECT_EPV_MODE = "Mode";
    private const String GP_SUBJECT_EPV_SERVER_ADDRESS1 = "ServerAddress1";
    private const String GP_SUBJECT_EPV_SERVER_ADDRESS2 = "ServerAddress2";
    private const String GP_SUBJECT_EPV_TIMEOUT_IN_MILLISECONDS = "TimeOutInMilliseconds";
    private const String GP_SUBJECT_EPV_DOMAIN = "Domain";
    private const String GP_SUBJECT_EPV_USERNAME = "Username";
    private const String GP_SUBJECT_EPV_PASSWORD = "Password";
    private const String GP_SUBJECT_EPV_CASHIER_PAYMENT_LIMIT = "Cashier.Payment.Limit";
    private const String GP_SUBJECT_EPV_CASHIER_PAYMENT_CASHLIMIT = "Cashier.Payment.CashLimit";
    private const String GP_SUBJECT_EPV_CASHIER_SALE_LIMIT = "Cashier.Sale.Limit";
    private const String GP_SUBJECT_EPV_WCP_FIXED_IP = "WCP.IP";
    private const String GP_SUBJECT_EPV_WCP_PORT = "WCP.Port";
    private const String GP_SUBJECT_EPV_LOG_MODE = "Log.Mode";

    private const String GP_SUBJECT_EPV_THREAD_ENABLED = "Thread.Enabled";
    private const String GP_SUBJECT_EPV_THREAD_WAIT_SECONDS = "Thread.WaitSeconds";
    private const String GP_SUBJECT_EPV_THREAD_NUMBER_OPERATIONS = "Thread.NumberOperationsToProcess";

    private const Int32 DEFAULT_EVP_MODE = (Int32)EnumExternalPaymentAndSaleValidationMode.MODE_DISABLED;
    private const String DEFAULT_EVP_SERVER_ADDRESS1 = "https://cumplimientomexico.com.mx/srvexpedwin_dev/ServiceReference.svc";
    private const String DEFAULT_EVP_SERVER_ADDRESS2 = "https://cumplimientomexico.com.mx/srvexpedwin_dev/ServiceReference.svc";
    private const Int32 DEFAULT_EVP_TIMEOUT_IN_MILLISECONDS = 10000;
    private const String DEFAULT_EVP_DOMAIN = "";
    private const String DEFAULT_EVP_USERNAME = "";
    private const String DEFAULT_EVP_PASSWORD = "";
    private const Decimal DEFAULT_EVP_CASHIER_PAYMENT_LIMIT = 0;
    private const Decimal DEFAULT_EVP_CASHIER_PAYMENT_CASHLIMIT = 0;
    private const Decimal DEFAULT_EVP_CASHIER_SALE_LIMIT = 0;
    private const String DEFAULT_EVP_WCP_FIXED_IP = "";
    private const String DEFAULT_EVP_WCP_PORT = "8085";
    private const Int32 DEFAULT_EVP_LOG_MODE = (Int32)EnumExternalPaymentAndSaleValidationLogMode.LOG_MODE_DISABLED;

    private const Boolean DEFAULT_EVP_THREAD_ENABLED = false;

    //Number of records to process (Thread)
    private const Int32 DEFAULT_EVP_NUMBER_OF_RECORDS_TO_PROCESS_DEFAULT = 500;
    private const Int32 DEFAULT_EVP_NUMBER_OF_RECORDS_TO_PROCESS_MIN = 1;
    private const Int32 DEFAULT_EVP_NUMBER_OF_RECORDS_TO_PROCESS_MAX = 10000;

    //Wait seconds (Thread)
    private const Int32 DEFAULT_EVP_WAIT_SECONDS_DEFAULT = 20;
    private const Int32 DEFAULT_EVP_WAIT_SECONDS_MIN = 10;
    private const Int32 DEFAULT_EVP_WAIT_SECONDS_MAX = 30;

    public const String VALIDATION_MESSAGE_ACCOUNT_NOT_AUTHORIZED = "EXPEDIENTE INCOMPLETO";
    public const String VALIDATION_MESSAGE_ACCOUNT_WIN_NOT_VALID = "CUENTA WIN NO EXISTE";

    #endregion

    #region "Properties"
    public static String ExpedServerGroupKey
    {
      get { return GP_GROUP_KEY_EXTERNAL_PAYMENT_VALIDATION; }
    }

    public static EnumExternalPaymentAndSaleValidationMode ExpedMode
    {
      get { return GetExternalPaymentValidationMode(); }
    }

    public static String ExpedServerAddress1
    {
      get { return GeneralParam.GetString(GP_GROUP_KEY_EXTERNAL_PAYMENT_VALIDATION, GP_SUBJECT_EPV_SERVER_ADDRESS1, DEFAULT_EVP_SERVER_ADDRESS1); }
    }

    public static String ExpedServerAddress2
    {
      get { return GeneralParam.GetString(GP_GROUP_KEY_EXTERNAL_PAYMENT_VALIDATION, GP_SUBJECT_EPV_SERVER_ADDRESS2, DEFAULT_EVP_SERVER_ADDRESS2); }
    }

    public static Int32 ExpedTimeOutInMilliseconds
    {
      get { return GeneralParam.GetInt32(GP_GROUP_KEY_EXTERNAL_PAYMENT_VALIDATION, GP_SUBJECT_EPV_TIMEOUT_IN_MILLISECONDS, DEFAULT_EVP_TIMEOUT_IN_MILLISECONDS); }
    }

    public static String ExpedDomain
    {
      get { return GeneralParam.GetString(GP_GROUP_KEY_EXTERNAL_PAYMENT_VALIDATION, GP_SUBJECT_EPV_DOMAIN, DEFAULT_EVP_DOMAIN); }
    }

    public static String ExpedUsername
    {
      get { return GeneralParam.GetString(GP_GROUP_KEY_EXTERNAL_PAYMENT_VALIDATION, GP_SUBJECT_EPV_USERNAME, DEFAULT_EVP_USERNAME); }
    }

    public static String ExpedPassword
    {
      get { return GeneralParam.GetString(GP_GROUP_KEY_EXTERNAL_PAYMENT_VALIDATION, GP_SUBJECT_EPV_PASSWORD, DEFAULT_EVP_PASSWORD); }
    }

    public static Decimal ExpedCashierPaymentLimit
    {
      get { return GeneralParam.GetDecimal(GP_GROUP_KEY_EXTERNAL_PAYMENT_VALIDATION, GP_SUBJECT_EPV_CASHIER_PAYMENT_LIMIT, DEFAULT_EVP_CASHIER_PAYMENT_LIMIT); }
    }

    public static Decimal ExpedCashierPaymentInCashLimit
    {
      get { return GeneralParam.GetDecimal(GP_GROUP_KEY_EXTERNAL_PAYMENT_VALIDATION, GP_SUBJECT_EPV_CASHIER_PAYMENT_CASHLIMIT, DEFAULT_EVP_CASHIER_PAYMENT_CASHLIMIT); }
    }

    public static Decimal ExpedCashierSaleLimit
    {
      get { return GeneralParam.GetDecimal(GP_GROUP_KEY_EXTERNAL_PAYMENT_VALIDATION, GP_SUBJECT_EPV_CASHIER_SALE_LIMIT, DEFAULT_EVP_CASHIER_SALE_LIMIT); }
    }

    /// <summary>
    /// Fixed IP to connect to Exped WCP
    /// </summary>
    public static String ExpedWCPFixedIP
    {
      get { return GeneralParam.GetString(GP_GROUP_KEY_EXTERNAL_PAYMENT_VALIDATION, GP_SUBJECT_EPV_WCP_FIXED_IP, DEFAULT_EVP_WCP_FIXED_IP); }
    }

    /// <summary>
    /// Port to connect to Exped WCP
    /// </summary>
    public static String ExpedWCPPort
    {
      get { return GeneralParam.GetString(GP_GROUP_KEY_EXTERNAL_PAYMENT_VALIDATION, GP_SUBJECT_EPV_WCP_PORT, DEFAULT_EVP_WCP_PORT); }
    }

    /// <summary>
    /// IP of last active Exped WCP 
    /// </summary>
    public static String ExpedWCPLastActiveIP
    {
      get;
      set;
    }

    /// <summary>
    /// Is enabled thread for registering transactions 
    /// </summary>
    public static Boolean ExpedThreadEnabled
    {
      get { return GeneralParam.GetBoolean(GP_GROUP_KEY_EXTERNAL_PAYMENT_VALIDATION, GP_SUBJECT_EPV_THREAD_ENABLED, DEFAULT_EVP_THREAD_ENABLED); }
    }

    public static Int32 ExpedThreadWaitSeconds
    {
      get { return GeneralParam.GetInt32(GP_GROUP_KEY_EXTERNAL_PAYMENT_VALIDATION, GP_SUBJECT_EPV_THREAD_WAIT_SECONDS, DEFAULT_EVP_WAIT_SECONDS_DEFAULT, DEFAULT_EVP_WAIT_SECONDS_MIN, DEFAULT_EVP_WAIT_SECONDS_MAX); }
    }

    public static Int32 ExpedThreadWaitSecondsDefault
    {
      get { return DEFAULT_EVP_WAIT_SECONDS_DEFAULT; }
    }

    /// <summary>
    /// Number of records to process can't be negative or 0
    /// </summary>
    public static Int32 ExpedThreadNumberOfRecordsToProcess
    {
      get { return GeneralParam.GetInt32(GP_GROUP_KEY_EXTERNAL_PAYMENT_VALIDATION, GP_SUBJECT_EPV_THREAD_NUMBER_OPERATIONS, DEFAULT_EVP_NUMBER_OF_RECORDS_TO_PROCESS_DEFAULT, DEFAULT_EVP_NUMBER_OF_RECORDS_TO_PROCESS_MIN, DEFAULT_EVP_NUMBER_OF_RECORDS_TO_PROCESS_MAX); }
    }
    #endregion

    #region "Public Methods"
    /// <summary>
    /// Returns if the External Payment gives authorization to process the operation with AccountId
    /// </summary>
    /// <param name="InputParams"></param>
    /// <param name="OutputParams"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean ExternalPaymentValidationGetAuthorization(ExternalValidationInputParameters InputParams
                                                                  , ExternalValidationOutputParameters OutputParams
                                                                  , SqlTransaction Trx)
    {
      EnumExternalPaymentAndSaleValidationMode _validation_mode;
      Boolean _result;

      _validation_mode = GetExternalPaymentValidationMode();

      switch (_validation_mode)
      {
        //If ExternalPaymentService Disabled then do not validate
        case EnumExternalPaymentAndSaleValidationMode.MODE_DISABLED:
          OutputParams = new ExternalValidationOutputParameters(EnumExternalPaymentAndSaleValidationResult.RESULT_VALIDATION_DISABLED);

          return true;

        //Exped ValidationMode -> Validate with Exped WebService
        case EnumExternalPaymentAndSaleValidationMode.MODE_EXPED:
          _result = ExternalPaymentValidation_Exped_GetAuthorization(InputParams, OutputParams, Trx);
          if (_result)
          {
            OutputParams.Status = EnumExternalPaymentAndSaleValidationStatus.STATUS_AUTHORIZED;
          }

          return _result;
        default:
          Log.Error("ExternalPaymentAndSaleValidationCommon.ExternalPaymentValidationGetAuthorization: ValidationMode unknown: " + _validation_mode.ToString());

          throw new ArgumentOutOfRangeException("ValidationMode unknown");
      }
    }

    /// <summary>
    /// Process the pending transactions
    /// </summary>
    /// <param name="NumberOfRecordsToProcess"></param>
    /// <returns></returns>
    public static Boolean ExternalPaymentValidationProcessPendingTransactions(Int32 NumberOfRecordsToProcess)
    {
      EnumExternalPaymentAndSaleValidationMode _validation_mode;
      Boolean _result;

      _validation_mode = GetExternalPaymentValidationMode();

      switch (_validation_mode)
      {
        //If ExternalPaymentService Disabled then do not process
        case EnumExternalPaymentAndSaleValidationMode.MODE_DISABLED:

          return false;
        //Exped ValidationMode -> Process pending with Exped WebService
        case EnumExternalPaymentAndSaleValidationMode.MODE_EXPED:
          _result = ExternalPaymentValidation_Exped_ProcessPendingTransactions(NumberOfRecordsToProcess);

          return _result;
        default:
          Log.Error("ExternalPaymentAndSaleValidationCommon.ExternalPaymentValidationProcessPendingTransactions: ValidationMode unknown: " + _validation_mode.ToString());

          throw new ArgumentOutOfRangeException("ValidationMode unknown");
      }
    }

    /// <summary>
    /// Returns if the External Validation is Enabled
    /// </summary>
    /// <returns></returns>
    public static Boolean ExternalPaymentValidationEnabled()
    {
      EnumExternalPaymentAndSaleValidationMode _validation_mode;
      _validation_mode = GetExternalPaymentValidationMode();

      switch (_validation_mode)
      {
        //Disabled
        case EnumExternalPaymentAndSaleValidationMode.MODE_DISABLED:

          return false;
        //Mode: Exped 
        case EnumExternalPaymentAndSaleValidationMode.MODE_EXPED:

          return true;
        default:
          Log.Error("ExternalPaymentAndSaleValidationCommon.ExternalPaymentValidationEnabled: ValidationMode unknown: " + _validation_mode.ToString());

          throw new ArgumentOutOfRangeException("ValidationMode unknown");
      }
    }

    /// <summary>
    /// Gets the Operation type (payment/sale) from the Operation Code
    /// </summary>
    /// <param name="OperationCode"></param>
    /// <returns></returns>
    public static TipoOperacion GetOperationTypeFromOperationCode(OperationCode OperationCode)
    {
      TipoOperacion _operation_type;

      switch (OperationCode)
      {
        case OperationCode.CASH_IN:
        case OperationCode.MB_CASH_IN:
        case OperationCode.NA_CASH_IN:
        case OperationCode.CHIPS_SALE:
        case OperationCode.CHIPS_SALE_WITH_RECHARGE:
          _operation_type = TipoOperacion.Venta;
          break;

        case OperationCode.CASH_OUT:
        case OperationCode.CHIPS_PURCHASE:
        case OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT:
        case OperationCode.CASH_ADVANCE:
        case OperationCode.PRIZE_PAYOUT:
        case OperationCode.PRIZE_PAYOUT_AND_RECHARGE:
          _operation_type = TipoOperacion.Pago;
          break;

        default:
          Log.Error("ExternalPaymentAndSaleValidationCommon.GetOperationTypeFromOperationCode: OperationCode unknown: " + OperationCode.ToString());

          throw new ArgumentOutOfRangeException("OperationCode unknown");
      }

      return _operation_type;
    }

    /// <summary>
    /// Get the operation amount to validate
    /// </summary>
    /// <param name="InputParams"></param>
    /// <returns></returns>
    public static Decimal GetOperationAmountToValidate(ExternalValidationInputParameters InputParams)
    {
      Decimal _amount_to_validate;
      OperationCode _operation_code;

      _operation_code = InputParams.OperationCode;

      switch (_operation_code)
      {
        case OperationCode.PRIZE_PAYOUT:
        case OperationCode.PRIZE_PAYOUT_AND_RECHARGE:
          _amount_to_validate = InputParams.NetAmount;
          break;

        default:
          _amount_to_validate = InputParams.NetAmount;
          break;
      }

      return _amount_to_validate;
    }

    /// <summary>
    /// Gets the Business Line type (terminals/gaming tables) from the Operation Code
    /// </summary>
    /// <param name="OperationCode"></param>
    /// <returns></returns>
    public static TipoLineaNegocio GetBusinessLineTypeFromOperationCode(OperationCode OperationCode)
    {
      TipoLineaNegocio _business_line_type;

      switch (OperationCode)
      {
        case OperationCode.CASH_IN:
        case OperationCode.MB_CASH_IN:
        case OperationCode.NA_CASH_IN:

        case OperationCode.CASH_OUT:
        case OperationCode.CASH_ADVANCE:
        case OperationCode.PRIZE_PAYOUT:
        case OperationCode.PRIZE_PAYOUT_AND_RECHARGE:
          _business_line_type = TipoLineaNegocio.Terminales;
          break;

        case OperationCode.CHIPS_SALE:
        case OperationCode.CHIPS_SALE_WITH_RECHARGE:
        case OperationCode.CHIPS_PURCHASE:
        case OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT:
          _business_line_type = TipoLineaNegocio.Mesas;
          break;

        default:
          Log.Error("ExternalPaymentAndSaleValidationCommon.GetBusinessLineTypeFromOperationCode: OperationCode unknown: " + OperationCode.ToString());

          throw new ArgumentOutOfRangeException("OperationCode unknown");
      }

      return _business_line_type;
    }

    /// <summary>
    /// Returns if we must log the connections to the WS
    /// </summary>
    /// <returns></returns>
    public static Boolean LogConnectionsToWS()
    {
      //Search if logging the connections is enabled
      return (GeneralParam.GetInt32(GP_GROUP_KEY_EXTERNAL_PAYMENT_VALIDATION
                                  , GP_SUBJECT_EPV_LOG_MODE
                                  , DEFAULT_EVP_LOG_MODE) == (Int32)EnumExternalPaymentAndSaleValidationLogMode.LOG_MODE_ENABLED);
    }

    /// <summary>
    /// Check the second threshold (refund with check) if the amount >= limit
    /// </summary>
    /// <param name="InputParams"></param>
    /// <returns></returns>
    public static Boolean HasToValidateSecondThresholdAmount(ExternalValidationInputParameters InputParams)
    {
      Decimal _payment_limit;
      Decimal _amount_to_validate;

      //Get limit
      _payment_limit = ExpedCashierPaymentInCashLimit;

      //Get amount to validate
      _amount_to_validate = GetOperationAmountToValidate(InputParams);

      //When the amount is greater or equal than the limit, we must check the account
      return (_amount_to_validate >= _payment_limit);
    }

    /// <summary>
    /// Returns the error message to show it in a popup for External WS
    /// </summary>
    /// <returns></returns>
    /// <remarks>When exists more than one provider, this function must be refactorized</remarks>
    public static string GetErrorMessageFromExternalProvider(long accountId, String holderName, Currency amountToSubstract)
    {
      string _error_message = string.Empty;
      switch (Misc.ExternalWS.OutputParams.ValidationResult)
      {
        case EnumExternalPaymentAndSaleValidationResult.RESULT_NOT_AUTHORIZED:
          _error_message = Misc.ExternalWS.OutputParams.ValidationMessage;
          if (!String.IsNullOrEmpty(_error_message))
          {
            _error_message += "\r\n" + "\r\n";
          }

          if (Misc.ExternalWS.OutputParams.ValidationMessage == VALIDATION_MESSAGE_ACCOUNT_NOT_AUTHORIZED
            || Misc.ExternalWS.OutputParams.ValidationMessage ==VALIDATION_MESSAGE_ACCOUNT_WIN_NOT_VALID)
          {
            _error_message += Resource.String("STR_FRM_CASHIERBUSINESSLOGIC_INFO_ERROR_AUTHORIZATION", accountId, holderName, amountToSubstract);
          }
          else
          {
            _error_message += Resource.String("STR_UC_CARD_RECHARGE_ERROR", accountId, holderName, amountToSubstract);
          }
          break;
        case EnumExternalPaymentAndSaleValidationResult.RESULT_ERROR_CONNECTION:
          _error_message += Resource.String("STR_FRM_CASHIERBUSINESSLOGIC_INFO_ERROR_CONNECTION", accountId, holderName, amountToSubstract);
          break;
        case EnumExternalPaymentAndSaleValidationResult.RESULT_AMOUNT_DOES_NOT_REACH_LIMIT:
        case EnumExternalPaymentAndSaleValidationResult.RESULT_CASINO_MODE_IS_NOT_CASHLESS:
        case EnumExternalPaymentAndSaleValidationResult.RESULT_CASINO_MODE_OPERATION_CODE_NOT_TO_VALIDATE:
        case EnumExternalPaymentAndSaleValidationResult.RESULT_VALIDATION_DISABLED:
        case EnumExternalPaymentAndSaleValidationResult.RESULT_UNKNOWN:
          _error_message += Resource.String("STR_ERROR_GENERIC_SERVICE_EXTERNAL_PROVIDER", accountId, holderName, amountToSubstract);
          break;
        case EnumExternalPaymentAndSaleValidationResult.RESULT_AUTHORIZED:
          throw new Exception("Exped: There is not a error message for a valid result");
        default:
          throw new Exception(String.Format("Exped: Invalid error status [{0}]", Misc.ExternalWS.OutputParams.ValidationResult.ToString()));
      }

      return _error_message.Replace("\\r\\n", "\r\n");
    }
    #endregion

    #region "Private Methods"
    /// <summary>
    /// Returns the validation mode
    /// </summary>
    /// <returns></returns>
    private static EnumExternalPaymentAndSaleValidationMode GetExternalPaymentValidationMode()
    {
      EnumExternalPaymentAndSaleValidationMode _validation_mode;
      Int64 _external_payment_validation_mode;

      try
      {
        _external_payment_validation_mode = GeneralParam.GetInt32(GP_GROUP_KEY_EXTERNAL_PAYMENT_VALIDATION
                                                                , GP_SUBJECT_EPV_MODE
                                                                , DEFAULT_EVP_MODE);
        _validation_mode = (EnumExternalPaymentAndSaleValidationMode)_external_payment_validation_mode;
      }
      catch (Exception)
      {
        _validation_mode = EnumExternalPaymentAndSaleValidationMode.MODE_UNKNOWN;
      }

      return _validation_mode;
    }

    /// <summary>
    /// Has to validate the operation
    /// </summary>
    /// <param name="InputParams"></param>
    /// <param name="OutputParams"></param>
    /// <returns></returns>
    public static Boolean HasToValidate(ExternalValidationInputParameters InputParams, out ExternalValidationOutputParameters OutputParams)
    {
      if (!HasToValidateOperationCode(InputParams.OperationCode))
      {
        //Is not an Payment/Sale operation
        OutputParams = new ExternalValidationOutputParameters(EnumExternalPaymentAndSaleValidationResult.RESULT_CASINO_MODE_OPERATION_CODE_NOT_TO_VALIDATE);

        return false;
      }

      if (!HasToValidateSystemMode())
      {
        //Is not CashLess
        OutputParams = new ExternalValidationOutputParameters(EnumExternalPaymentAndSaleValidationResult.RESULT_CASINO_MODE_IS_NOT_CASHLESS);

        return false;
      }

      if (!HasToValidateAmount(InputParams))
      {
        //We don't have to validate because the amount is less than the limit.
        OutputParams = new ExternalValidationOutputParameters(EnumExternalPaymentAndSaleValidationResult.RESULT_AMOUNT_DOES_NOT_REACH_LIMIT);

        return false;
      }

      OutputParams = new ExternalValidationOutputParameters(EnumExternalPaymentAndSaleValidationResult.RESULT_UNKNOWN);

      return true;
    }

    /// <summary>
    /// Has to validate the amount only if CashLess
    /// </summary>
    /// <returns></returns>
    private static Boolean HasToValidateSystemMode()
    {
      return (WSI.Common.Misc.IsCashlessMode());
    }

    /// <summary>
    /// Check if amount >= limit
    /// </summary>
    /// <param name="InputParams"></param>
    /// <returns></returns>
    private static Boolean HasToValidateAmount(ExternalValidationInputParameters InputParams)
    {
      Decimal _exped_limit;
      Decimal _amount_to_validate;
      TipoOperacion _operation_type;

      //Get limit
      _exped_limit = GetOperationLimit(InputParams.OperationCode);

      //Get amount to validate
      _amount_to_validate = GetOperationAmountToValidate(InputParams);

      _operation_type = GetOperationTypeFromOperationCode(InputParams.OperationCode);

      if (_operation_type == TipoOperacion.Pago)
      {
        //When the amount is greater or equal than the limit, we must check the account
        return (_amount_to_validate >= _exped_limit);
      }
      else
      {
        return HasToValidateSplitedValues(InputParams.OperationCode, _amount_to_validate, _exped_limit);
      }
    }

    /// <summary>
    /// Do the splited values have to be validated by Exped
    /// </summary>
    /// <param name="OpCode"></param>
    /// <param name="AmountToValidate"></param>
    /// <param name="PaymentLimit"></param>
    /// <returns></returns>
    private static Boolean HasToValidateSplitedValues(OperationCode OpCode, Decimal AmountToValidate, Decimal PaymentLimit)
    {
      Decimal _ammount_split_a;
      Decimal _ammount_split_b;

      GetSplitAmounts(OpCode, AmountToValidate, out _ammount_split_a, out _ammount_split_b);

      //Is either amount greater or equal to PaymentLimit
      return ((_ammount_split_a >= PaymentLimit) || (_ammount_split_b >= PaymentLimit));
    }

    /// <summary>
    /// Get Split percentage for company A
    /// </summary>
    /// <param name="OpCode"></param>
    /// <returns></returns>
    private static Decimal GetSplitPercentageForCompanyA(OperationCode OpCode)
    {
      TYPE_SPLITS _splits;
      Decimal _percentage_company_a;
      const Decimal TOTAL_PERCENTAGE = 100;

      //Get split parameters
      if (!Split.ReadSplitParameters(out _splits))
      {
        return TOTAL_PERCENTAGE;
      }

      if (OpCode == OperationCode.CHIPS_SALE_WITH_RECHARGE)
      {
        _percentage_company_a = _splits.company_a.chips_sale_pct;
      }
      else
      {
        _percentage_company_a = _splits.company_a.cashin_pct;
      }

      return _percentage_company_a;
    }

    private static void GetSplitAmounts(OperationCode OpCode, Decimal AmountToValidate, out Decimal AmountSplitA, out Decimal AmountSplitB)
    {
      Decimal _percentage_company_a;
      _percentage_company_a = GetSplitPercentageForCompanyA(OpCode);

      //Split amounts
      AmountSplitA = Math.Round((AmountToValidate * _percentage_company_a / 100), 2, MidpointRounding.AwayFromZero);
      AmountSplitB = AmountToValidate - AmountSplitA;
    }

    /// <summary>
    /// Returns if the Operation Code needs to be validated
    /// </summary>
    /// <param name="OperationCode"></param>
    /// <returns></returns>
    private static Boolean HasToValidateOperationCode(OperationCode OperationCode)
    {
      Boolean _has_to_be_validated;

      switch (OperationCode)
      {
        case OperationCode.PRIZE_PAYOUT_AND_RECHARGE:
          _has_to_be_validated = false;
          break;
        case OperationCode.CASH_IN:
        case OperationCode.CASH_OUT:
        case OperationCode.MB_CASH_IN:
        case OperationCode.CHIPS_SALE:
        case OperationCode.CHIPS_SALE_WITH_RECHARGE:
        case OperationCode.CHIPS_PURCHASE:
        case OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT:
        case OperationCode.CASH_ADVANCE:
        case OperationCode.NA_CASH_IN:
        case OperationCode.PRIZE_PAYOUT:
          _has_to_be_validated = true;
          break;
        default:
          _has_to_be_validated = false;
          break;
      }

      return _has_to_be_validated;
    }

    /// <summary>
    /// Returns the limit of the External Payment Validation
    /// </summary>
    /// <param name="OperationCode"></param>
    /// <returns></returns>
    private static Decimal GetOperationLimit(OperationCode OperationCode)
    {
      Decimal _operation_limit;
      TipoOperacion _operation_type;

      //Get the operation type (payment/sale), to next get its limit.
      _operation_type = GetOperationTypeFromOperationCode(OperationCode);

      switch (_operation_type)
      {
        case TipoOperacion.Pago:
          _operation_limit = ExpedCashierPaymentLimit;

          break;
        case TipoOperacion.Venta:
          _operation_limit = ExpedCashierSaleLimit;

          break;
        default:
          Log.Error("ExternalPaymentAndSaleValidationCommon.GetOperationLimit: OperationType unknown: " + _operation_type.ToString());

          throw new ArgumentOutOfRangeException("OperationType unknown");
      }

      return _operation_limit;
    }

    /// <summary>
    /// Returns if the Exped webservice validates the Payment/Sale operation
    /// </summary>
    /// <param name="InputParams"></param>
    /// <param name="OutputParams"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean ExternalPaymentValidation_Exped_GetAuthorization(ExternalValidationInputParameters InputParams
                                                                          , ExternalValidationOutputParameters OutputParams
                                                                          , SqlTransaction Trx)
    {
      IExternalPaymentAndSaleValidation _external_payment_validation;

      if (string.IsNullOrEmpty(OutputParams.AuthorizationCode) && !HasToValidate(InputParams, out OutputParams))
      {
        return true;
      }

      //ExternalPaymentService check
      _external_payment_validation = new ExternalPaymentAndSaleValidation_Exped();

      return _external_payment_validation.ExternalPaymentAndSaleValidation_GetAuthorization(InputParams, OutputParams, Trx);
    }

    /// <summary>
    /// Registers the pending transactions 
    /// </summary>
    /// <param name="NumberOfRecordsToProcess">Number of records to process by the thread</param>
    /// <returns></returns>
    private static Boolean ExternalPaymentValidation_Exped_ProcessPendingTransactions(Int32 NumberOfRecordsToProcess)
    {
      IExternalPaymentAndSaleValidation _external_payment_validation;

      //ExternalPaymentService check
      _external_payment_validation = new ExternalPaymentAndSaleValidation_Exped();

      return _external_payment_validation.ExternalPaymentAndSaleValidation_ProcessPendingTransactions(NumberOfRecordsToProcess);
    }
    #endregion
  }
}