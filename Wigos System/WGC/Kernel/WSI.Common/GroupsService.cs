//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GroupsService.cs
// 
//   DESCRIPTION: Class to manage terminal-groups relations service
// 
//        AUTHOR: Rub�n Brea
// 
// CREATION DATE: 25-JUN-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-JUL-2013 RBG    Initial version
// 10-DEC-2013 AMF    Change query of GetActivePromotions
// 12-MAR-2014 AMF    Fixed Bug WIG-722: Implement TerminalListType All
// 27-MAR-2014 MPO    Performance improved for terminal generations
// 15-MAY-2014 AMF    Patterns
// 02-JUN-2014 JBC    Fixed Bug WIG-986: Now method to migration of jackpot providers works.
// 20-AUG-2014 JBC    Fixed Bug WIG-1174: Now time 00:00 to 00:00 does the same work as validation period unchecked.
// 27-MAR-2014 DCS    Update exploit with the new development of cloned terminals
// 03-NOV-2015 AVZ    Added game gateway terminals 
// 25-AUG-2017 ATB    PBI 28710: EGM Reserve � Settings (WIGOS-4697)
// 03-AUG-2018 MS     Bug 33790:[WIGOS-13891] [Ticket #16007] Fallo � Multiplicador � Sesiones de Mesas no funciona version Vo3.08.0027
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using System.IO;
using System.Diagnostics;

namespace WSI.Common
{
  public static class GroupsService
  {
    #region Class TimeProcess

    // Time process information
    public class TimeProcess
    {
      #region Members

      public String Name = String.Empty;               // Process name
      public Stopwatch Stopwatch = null;               // 
      public Boolean Ended = false;                    // Indicates if the process has ended
      public String Information = String.Empty;        // It can store aditional general information
      public Int32 LogLevel = 1;                       // Log level associated in the process
      public Double MaxSeconds = 0;
      public String PrintName = String.Empty;
      public Boolean PrintAsWarning = false;

      public Boolean IsPrintable(Int32 LogLevelDefined)
      {
        Boolean _print;

        _print = false;
        if (this.MaxSeconds > 0 && this.Stopwatch.Elapsed.TotalSeconds > this.MaxSeconds)
        {
          _print = true;
          this.PrintAsWarning = true;
        }

        if (this.LogLevel <= LogLevelDefined)
        {
          _print = true;
        }

        if (_print)
        {
          StringBuilder _sb;

          _sb = new StringBuilder();
          _sb.Append(this.Name);
          if (!String.IsNullOrEmpty(this.Information))
          {
            _sb.Append(" ");
            _sb.Append(this.Information);
          }
          this.PrintName = _sb.ToString();
        }

        return _print;
      }

      #endregion
    }

    // Performance Management
    public class TimeProcessControl
    {
      #region Members

      private Dictionary<String, TimeProcess> m_process_times = new Dictionary<String, TimeProcess>();    // Contains all the process timings
      private Int32 m_log_level = 0;                            // Should Logs be printed?

      public Int32 LogLevel
      {
        set
        {
          m_log_level = value;
        }
      }

      #endregion

      #region Constructor

      #endregion

      #region Public

      public void IniProcess(String ProcessName)
      {
        this.IniProcess(ProcessName, 0, 1);
      }

      public void IniProcess(String ProcessName, Double MaxSeconds)
      {
        this.IniProcess(ProcessName, MaxSeconds, 1);
      }

      public void IniProcess(String ProcessName, Int32 LogLevel)
      {
        this.IniProcess(ProcessName, 0, LogLevel);
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Start supervising a process.
      //
      //  PARAMS :
      //      - INPUT :
      //           - Process
      //
      //      - OUTPUT :
      //
      // RETURNS :
      //
      private void IniProcess(String ProcessName, Double MaxSeconds, Int32 LogLevel)
      {
        TimeProcess _process;

        if (m_log_level == 0)
        {
          return;
        }

        if (m_process_times.ContainsKey(ProcessName))
        {
          _process = m_process_times[ProcessName];
        }
        else
        {
          _process = new TimeProcess();
          _process.Stopwatch = new Stopwatch();
          _process.Name = ProcessName;
          _process.LogLevel = LogLevel;
          _process.MaxSeconds = MaxSeconds;

          m_process_times.Add(ProcessName, _process);
        }

        _process.Stopwatch.Start();
        _process.Ended = false;

        return;
      }  // IniProcess

      //------------------------------------------------------------------------------
      // PURPOSE : Stops process from being supervised.
      //
      //  PARAMS :
      //      - INPUT :
      //           - Process
      //
      //      - OUTPUT :
      //
      // RETURNS :
      //      - Boolean
      //
      public void EndProcess(String Process)  // EndProcess
      {
        if (m_log_level == 0)
        {
          return;
        }

        if (m_process_times.ContainsKey(Process))
        {
          m_process_times[Process].Stopwatch.Stop();
          m_process_times[Process].Ended = true;
        }
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Prints all process and totalize all finished process
      //
      //  PARAMS :
      //      - INPUT :
      //           - Process
      //
      //      - OUTPUT :
      //           
      // RETURNS :
      //      - Boolean 
      //
      public void PrintProcessTimes(String InitialText)
      {
        if (m_log_level == 0)
        {
          return;
        }

        if (m_process_times.Count == 0)
        {
          return;
        }

        // Preprocess
        Int32 _max_length;
        List<TimeProcess> _processes_to_print;

        _max_length = 0;
        _processes_to_print = new List<TimeProcess>();

        foreach (TimeProcess _process in m_process_times.Values)
        {
          if (_process.IsPrintable(m_log_level))
          {
            // Warning => Length 7
            _max_length = Math.Max(_max_length, _process.PrintName.Length + (_process.PrintAsWarning ? 8 : 0));

            _processes_to_print.Add(_process);
          }
        }

        if (_processes_to_print.Count == 0)
        {
          return;
        }

        Log.Message("--" + InitialText);

        // Print message
        foreach (TimeProcess _process in _processes_to_print)
        {
          if (_process.PrintAsWarning)
          {
            // Warning => Length 7
            Log.Warning(_process.PrintName.Trim().PadRight(_max_length - 7, ' ') + _process.Stopwatch.Elapsed.ToString());
          }
          else
          {
            Log.Message(_process.PrintName.Trim().PadRight(_max_length + 1, ' ') + _process.Stopwatch.Elapsed.ToString());
          }
        }

        Log.Message("--");

        return;
      }  // PrintProcessTimes

      //------------------------------------------------------------------------------
      // PURPOSE : Set process aditional information
      //
      //  PARAMS :
      //      - INPUT :
      //           - Process
      //
      //      - OUTPUT :
      //           
      // RETURNS :
      //      - Boolean 
      //
      public void SetInformation(String Process, String Information)  // SetInformation
      {
        if (m_log_level == 0)
        {
          return;
        }

        if (m_process_times.ContainsKey(Process))
        {
          m_process_times[Process].Information = Information;
        }

        return;
      }

      //------------------------------------------------------------------------------
      // PURPOSE : Clears all the elemens from the list.
      //
      //  PARAMS :
      //      - INPUT :
      //
      //      - OUTPUT :
      //
      // RETURNS :
      //
      public void Clear()
      {
        m_log_level = 0;
        m_process_times.Clear();
      }  // Clear

      #endregion
    }

    #endregion

    #region Enums

    public enum ELEMENT_STATUS
    {
      Unchanged = 0,
      Added = 1,
      Deleted = 2,
    }

    #endregion

    #region CONSTANTS

    public const String COLUMN_EXPLOIT_MASTER_ID = "MASTER_ID";
    public const String COLUMN_EXPLOIT_TER_ID = "TERMINAL_ID";
    public const String COLUMN_EXPLOIT_ELEMENT_ID = "ELEMENT_ID";
    public const String COLUMN_EXPLOIT_TYPE = "ELEMENT_TYPE";
    public const String COLUMN_EXPLOIT_STATUS = "STATUS"; // Enum ELEMENT_STATUS

    public const String COLUMN_ELEMENT_ID = "ELEMENT_ID";
    public const String COLUMN_ELEMENT_LIST = "ELEMENT_LIST";

    #endregion

    #region MEMBERS

    private static DataTable m_terminals_group = null;
    private static DataTable m_all_terminals = null;

    private static List<Int64> m_ls_expanded_groups = null;
    private static DateTime m_expanded_group;

    private static TimeProcessControl m_time_control = new TimeProcessControl();

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Exploit Terminals
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :  Result Succesfully
    //
    //   NOTES :
    public static Boolean GenerateTerminals()
    {
      SqlTransaction _trx;
      DataTable _dt;

      try
      {
        m_time_control.Clear();
        m_time_control.LogLevel = GeneralParam.GetInt32("WCP", "Groups.GenerateLog", 0, 0, 3);

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _trx = _db_trx.SqlTransaction;

          // 2 minutes for generate log
          m_time_control.IniProcess("Total", 120.00);

          m_time_control.IniProcess("Load", 2);
          if (!Load(_trx))
          {
            return false;
          }
          m_time_control.EndProcess("Load");

          // Groups
          m_time_control.IniProcess("Groups", 2);
          if (!GenerateTerminalsFromGroups(_trx))
          {
            return false;
          }
          m_time_control.EndProcess("Groups");

          // Promotions
          m_time_control.IniProcess("Promos", 2);
          if (!GetActivePromotions(EXPLOIT_ELEMENT_TYPE.PROMOTION, out _dt, _trx))
          {
            return false;
          }
          m_time_control.SetInformation("Promos", "[" + _dt.Rows.Count + "]");
          if (!GenerateTerminalFromTerminalList(_dt, EXPLOIT_ELEMENT_TYPE.PROMOTION, _trx))
          {
            return false;
          }
          m_time_control.EndProcess("Promos");

          //////// Promotion by Played
          //////m_time_control.IniProcess("PromosByPlayed", 2);
          //////if (!GetActivePromotions(EXPLOIT_ELEMENT_TYPE.PROMOTION_PLAYED, out _dt, _trx))
          //////{
          //////  return false;
          //////}
          //////m_time_control.SetInformation("PromosByPlayed", "[" + _dt.Rows.Count + "]");
          //////if (!GenerateTerminalFromTerminalList(_dt, EXPLOIT_ELEMENT_TYPE.PROMOTION_PLAYED, _trx))
          //////{
          //////  return false;
          //////}
          //////m_time_control.EndProcess("PromosByPlayed");

          // Promotions restricted
          m_time_control.IniProcess("PromosRestricted", 2);
          if (!GetActivePromotions(EXPLOIT_ELEMENT_TYPE.PROMOTION_RESTRICTED, out _dt, _trx))
          {
            return false;
          }
          m_time_control.SetInformation("PromosRestricted", "[" + _dt.Rows.Count + "]");
          if (!GenerateTerminalFromTerminalList(_dt, EXPLOIT_ELEMENT_TYPE.PROMOTION_RESTRICTED, _trx))
          {
            return false;
          }
          m_time_control.EndProcess("PromosRestricted");

          m_time_control.IniProcess("Jackpot", 2);
          if (!GetActiveSiteJackpot(out _dt, _trx))
          {
            return false;
          }
          m_time_control.SetInformation("Jackpot", "[" + _dt.Rows.Count + "]");
          if (!GenerateTerminalFromTerminalList(_dt, EXPLOIT_ELEMENT_TYPE.JACKPOT, _trx))
          {
            return false;
          }
          m_time_control.EndProcess("Jackpot");

          m_time_control.IniProcess("Patterns", 2);
          if (!GetActivePatterns(out _dt, _trx))
          {
            return false;
          }
          m_time_control.SetInformation("Patterns", "[" + _dt.Rows.Count + "]");
          if (!GenerateTerminalFromTerminalList(_dt, EXPLOIT_ELEMENT_TYPE.PATTERNS, _trx))
          {
            return false;
          }
          m_time_control.EndProcess("Patterns");

          m_time_control.IniProcess("Progressives", 2);
          if (!GetActiveProgressives(out _dt, _trx))
          {
            return false;
          }
          m_time_control.SetInformation("Progressives", "[" + _dt.Rows.Count + "]");
          if (!GenerateTerminalFromTerminalList(_dt, EXPLOIT_ELEMENT_TYPE.PROGRESSIVE, _trx))
          {
            return false;
          }
          m_time_control.EndProcess("Progressives");

          //
          m_time_control.IniProcess("LCD_Message", 2);
          if (!GetActiveLCDMessage(out _dt, _trx))
          {
            return false;
          }
          m_time_control.SetInformation("LCD_Message", "[" + _dt.Rows.Count + "]");
          if (!GenerateTerminalFromTerminalList(_dt, EXPLOIT_ELEMENT_TYPE.LCD_MESSAGE, _trx))
          {
            return false;
          }
          m_time_control.EndProcess("LCD_Message");
          //

          // AVZ 03-11-2015
          m_time_control.IniProcess("Game_Gateway", 2);
          if (!GetActiveGameGateway(out _dt, _trx))
          {
            return false;
          }
          m_time_control.SetInformation("Game_Gateway", "[" + _dt.Rows.Count + "]");
          if (!GenerateTerminalFromTerminalList(_dt, EXPLOIT_ELEMENT_TYPE.GAME_GATEWAY, _trx))
          {
            return false;
          }
          m_time_control.EndProcess("Game_Gateway");
          //

          // XGJ 28-AGU-2016
          // Buckets
          m_time_control.IniProcess("Buckets_Multiplier", 2);
          if (!GetActiveBucketsMultiplier(out _dt, _trx))
          {
            return false;
          }
          m_time_control.SetInformation("Buckets_Multiplier", "[" + _dt.Rows.Count + "]");
          if (!GenerateTerminalFromTerminalList(_dt, EXPLOIT_ELEMENT_TYPE.BUCKETS_MULTIPLIER, _trx))
          {
            return false;
          }
          m_time_control.EndProcess("Buckets_Multiplier");
          //

          // CCG 20-APR-2017
          // Jackpot Contribution group
          m_time_control.IniProcess("Jackpot_contribution_group", 2);
          if (!GetActiveJackpotContributionTerminals(out _dt, _trx))
          {
            return false;
          }
          m_time_control.SetInformation("Jackpot_contribution_group", "[" + _dt.Rows.Count + "]");
          if (!GenerateTerminalFromTerminalList(_dt, EXPLOIT_ELEMENT_TYPE.JACKPOT_CONTRIBUTIONS, _trx))
          {
            return false;
          }
          m_time_control.EndProcess("Jackpot_contribution_group");
          //

          // Jackpot award terminals
          m_time_control.IniProcess("Jackpot_award_terminals", 2);
          if (!GetActiveJackpotAwardTerminalsList(out _dt, _trx))
          {
            return false;
          }
          m_time_control.SetInformation("Jackpot_award_terminals", "[" + _dt.Rows.Count + "]");
          if (!GenerateTerminalFromTerminalList(_dt, EXPLOIT_ELEMENT_TYPE.JACKPOT_AWARDS, _trx))
          {
            return false;
          }
          m_time_control.EndProcess("Jackpot_award_terminals");
          //

          // ATB 24-AUG-2017
          // Reserved terminals
          m_time_control.IniProcess("Reserved_Terminals", 2);
          if (!GetActiveReservedTerminals(out _dt, _trx))
          {
            return false;
          }
          m_time_control.SetInformation("Reserved_Terminals", "[" + _dt.Rows.Count + "]");
          if (!GenerateTerminalFromTerminalList(_dt, EXPLOIT_ELEMENT_TYPE.RESERVED_TERMINALS, _trx))
          {
            return false;
          }
          m_time_control.EndProcess("Reserved_Terminals");
          //

          m_time_control.IniProcess("Save", 2);
          if (!Save(_trx))
          {
            return false;
          }
          m_time_control.EndProcess("Save");

          _trx.Commit();

          m_time_control.EndProcess("Total");

          m_time_control.PrintProcessTimes("Groups Log");

          return true;
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Exception(_sql_ex);
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;

    } //  ExploitTerminals

    //------------------------------------------------------------------------------
    // PURPOSE : Provider data migration
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :  Result Succesfully
    //
    //   NOTES :
    public static Boolean UpdateJackpotTerminalsFromProviders()
    {
      StringBuilder _sb;
      DataTable _dt_providers;
      TerminalList _tl;
      String _xml;
      Object _obj;
      SqlTransaction _trx;

      _sb = new StringBuilder();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _trx = _db_trx.SqlTransaction;

          _sb.Length = 0;
          _sb.AppendLine("SELECT SJP_TERMINAL_LIST FROM SITE_JACKPOT_PARAMETERS");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _trx.Connection, _trx))
          {
            _obj = _cmd.ExecuteScalar();

            if (_obj != null && _obj != DBNull.Value)
            {
              return true;
            }
          }

          _dt_providers = new DataTable();
          _tl = new TerminalList();

          _sb.Length = 0;
          _sb.AppendLine(" SELECT   PV_ID   AS ELEMENT_ID ");
          _sb.AppendLine("        , PV_NAME AS ELEMENT_NAME ");
          _sb.AppendLine("   FROM   PROVIDERS ");
          _sb.AppendLine("  WHERE   PV_SITE_JACKPOT = 1 ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _trx.Connection, _trx))
          {
            using (SqlDataAdapter _sda = new SqlDataAdapter(_cmd))
            {
              _sda.Fill(_dt_providers);
            }
          }

          _tl.ListType = TerminalList.TerminalListType.Listed;

          foreach (DataRow _dr in _dt_providers.Rows)
          {
            _tl.AddElement((String)_dr["ELEMENT_NAME"], (Int32)_dr["ELEMENT_ID"], GROUP_ELEMENT_TYPE.PROV);
          }
          _xml = _tl.ToXml();

          _sb.Length = 0;
          _sb.AppendLine(" UPDATE SITE_JACKPOT_PARAMETERS SET SJP_TERMINAL_LIST = @pXml ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _trx.Connection, _trx))
          {
            _cmd.Parameters.Add("@pXml", SqlDbType.Xml).Value = _xml;
            if (_cmd.ExecuteNonQuery() != 1)
            {

              return false;
            }
          }

          _trx.Commit();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error("Error on UpdateJackpotTerminalsFromProviders");

        Log.Exception(_ex);
      }

      return false;
    } // GetJackPotTerminalsFromProviders

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Return terminal list from XML
    //
    //  PARAMS :
    //      - INPUT :
    //          Xml:            Contains the to generate the terminal list
    //    			TerminalsList:  DataTable
    //          ElementType:    EXPLOIT_ELEMENT_TYPE
    //          Trx:            SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    static public Boolean GetTerminalListFromXml(String Xml, out DataTable TerminalsList, EXPLOIT_ELEMENT_TYPE ElementType)
    {
      DB_TRX _db_trx;

      TerminalsList = new DataTable();
      _db_trx = new DB_TRX();

      try
      {

        TerminalList _list = new TerminalList();

        Load(_db_trx.SqlTransaction);
        _list.FromXml(Xml);

        if (!GetTerminalsFromTerminalList(_list, true, out TerminalsList, _db_trx.SqlTransaction))
        {
          return false;
        }

        //switch (_list.ListType)
        //{
        //  case TerminalList.TerminalListType.Listed:

        //  case TerminalList.TerminalListType.NotListed:

        //  case TerminalList.TerminalListType.All:
        //  default:
        //    break;
        //}

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Exception: GroupService.GetTerminalListFromXml: Type - " + ElementType.ToString());
        Log.Exception(_ex);
      }
      _db_trx.Dispose();

      return false;
    } // GetTerminalListFromXml

    //------------------------------------------------------------------------------
    // PURPOSE : Return terminal complete list from XML
    //
    //  PARAMS :
    //      - INPUT :
    //          Xml:            Contains the to generate the terminal list
    //    			TerminalsList:  DataTable
    //          Trx:            SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    static public Boolean GetCompleteTerminalListFromXml(String Xml, TerminalList TerminalList, out DataTable DtTerminalsList)
    {

      DtTerminalsList = new DataTable();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          Load(_db_trx.SqlTransaction);
          if (GetTerminalsFromTerminalList(TerminalList, false, out DtTerminalsList, _db_trx.SqlTransaction))
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("Exception: GroupService.GetCompleteTerminalListFromXml");
        Log.Exception(_ex);
      }

      return false;
    } // GetCompleteTerminalListFromXml

    //------------------------------------------------------------------------------
    // PURPOSE : Return terminal list from XML
    //
    //  PARAMS :
    //      - INPUT :
    //          Xml:            Contains the to generate the terminal list
    //    			TerminalsList:  DataTable
    //          ElementType:    EXPLOIT_ELEMENT_TYPE
    //          Trx:            SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    static public Boolean GetTerminalListFromXml2(String Xml1, String Xml2, out DataTable TerminalsList1, out DataTable TerminalsList2, EXPLOIT_ELEMENT_TYPE ElementType)
    {
      DB_TRX _db_trx;

      TerminalsList1 = new DataTable();
      TerminalsList2 = new DataTable();
      _db_trx = new DB_TRX();

      try
      {
        TerminalList _list = new TerminalList();

        Load(_db_trx.SqlTransaction);

        _list.FromXml(Xml1);
        if (!GetTerminalsFromTerminalList(_list, true, out TerminalsList1, _db_trx.SqlTransaction))
        {
          return false;
        }

        _list = new TerminalList();
        _list.FromXml(Xml2);
        if (!GetTerminalsFromTerminalList(_list, true, out TerminalsList2, _db_trx.SqlTransaction))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Exception: GroupService.GetTerminalListFromXml: Type - " + ElementType.ToString());
        Log.Exception(_ex);
      }
      _db_trx.Dispose();

      return false;
    } // GetTerminalListFromXml2

    //------------------------------------------------------------------------------
    // PURPOSE : Generate the terminals related to a terminal list
    //
    //  PARAMS :
    //      - INPUT :
    //          ElementsListToGenerateTerminals: Contains the terminals list to generate
    //    			ElementType: EXPLOIT_ELEMENT_TYPE
    //          Trx: SqlTransaction
    //      - OUTPUT :
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    static private Boolean GenerateTerminalFromTerminalList(DataTable ElementsListToGenerateTerminals, EXPLOIT_ELEMENT_TYPE ElementType, SqlTransaction Trx)
    {
      String _xml;
      Int64 _element_id;
      DataTable _terminals;
      Int64 _te_id_to_add;
      Int64 _te_master_id_to_add;

      try
      {
        // For each element type exploit all its elements and mark it as Unchanged or Added
        foreach (DataRow _element_list in ElementsListToGenerateTerminals.Rows)
        {
          if (_element_list[COLUMN_ELEMENT_LIST] == DBNull.Value)
          {

            continue;
          }

          _element_id = (Int64)_element_list[COLUMN_ELEMENT_ID];

          _xml = (String)_element_list[COLUMN_ELEMENT_LIST];
          TerminalList _list = new TerminalList();
          _list.FromXml(_xml);

          switch (_list.ListType)
          {
            case TerminalList.TerminalListType.Listed:

              if (!GetTerminalsFromTerminalList(_list, true, out _terminals, Trx))
              {
                return false;
              }

              foreach (DataRow _te_to_add in _terminals.Rows)
              {
                AddTerminalIfNotE((Int64)_te_to_add[Groups.ELEMENTS_COLUMN_ID], (Int64)_te_to_add[Groups.ELEMENTS_COLUMN_MASTER_ID], _element_id, (Int32)ElementType);
              }

              break;

            case TerminalList.TerminalListType.NotListed:

              if (!GetTerminalsFromTerminalList(_list, true, out _terminals, Trx))
              {
                return false;
              }

              foreach (DataRow _te_to_add in m_all_terminals.Rows)
              {
                _te_id_to_add = Convert.ToInt64(_te_to_add["TE_TERMINAL_ID"]); // Exclude terminal
                _te_master_id_to_add = Convert.ToInt64(_te_to_add["TE_MASTER_ID"]); // Exclude terminal

                if (_terminals.Rows.Find(new Object[] { _te_master_id_to_add, _te_id_to_add, (Int32)GROUP_ELEMENT_TYPE.TERM, 0 }) == null)
                {
                  AddTerminalIfNotE(_te_id_to_add, _te_master_id_to_add, _element_id, (Int32)ElementType);
                }
              }

              break;

            case TerminalList.TerminalListType.All:

              foreach (DataRow _te_to_add in m_all_terminals.Rows)
              {
                _te_id_to_add = Convert.ToInt64(_te_to_add["TE_TERMINAL_ID"]);
                _te_master_id_to_add = Convert.ToInt64(_te_to_add["TE_MASTER_ID"]); // Exclude terminal

                AddTerminalIfNotE(_te_id_to_add, _te_master_id_to_add, _element_id, (Int32)ElementType);
              }

              break;

            default:
              break;

          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Exception: GroupService.GenerateTerminalFromTerminalList: Type - " + ElementType.ToString());

        Log.Exception(_ex);
      }

      return false;
    } // GenerateTerminalFromTerminalList

    //------------------------------------------------------------------------------
    // PURPOSE : Generate terminals related to modified groups
    //
    //  PARAMS :
    //      - INPUT :
    //          Trx: SqlTransaction
    //      - OUTPUT :
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    static private Boolean GenerateTerminalsFromGroups(SqlTransaction Trx)
    {
      DataTable _terminals;
      Int64 _group_modified;
      Groups.INPUT_GROUP_PARAMS _params;

      m_ls_expanded_groups = new List<Int64>();
      m_expanded_group = WGDB.Now;

      try
      {
        // Get list of terminals ordered by groups that has been updated
        _params = new Groups.INPUT_GROUP_PARAMS();
        _params.ElementId = 0;
        _params.ElementType = GROUP_ELEMENT_TYPE.GROUP;
        _params.ParentId = 0;
        _terminals = Groups.CreateElementTable(true);
        if (!GetTerminalsFromGroups(_params, ref _terminals, Trx))
        {
          return false;
        }

        foreach (DataRow _row in _terminals.Rows)
        {
          _group_modified = (Int64)_row[Groups.ELEMENTS_COLUMN_PARENT_ID];
          if ((Boolean)_row[Groups.ELEMENTS_COLUMN_ENABLED])
          {
            AddTerminalIfNotE((Int64)_row[Groups.ELEMENTS_COLUMN_ID], (Int64)_row[Groups.ELEMENTS_COLUMN_MASTER_ID], _group_modified, (Int32)EXPLOIT_ELEMENT_TYPE.GROUP);
          }
          else
          {
            DelTerminalIfNotE((Int64)_row[Groups.ELEMENTS_COLUMN_ID], (Int64)_row[Groups.ELEMENTS_COLUMN_MASTER_ID], _group_modified, (Int32)EXPLOIT_ELEMENT_TYPE.GROUP);
          }
        }

        String _select;
        DataRow[] _drs;

        _select = COLUMN_EXPLOIT_TYPE + " = " + (Int32)EXPLOIT_ELEMENT_TYPE.GROUP;
        _drs = m_terminals_group.Select(_select);

        foreach (DataRow _dr in _drs)
        {
          if (!m_ls_expanded_groups.Contains((Int64)_dr[COLUMN_ELEMENT_ID]))
          {
            _dr["STATUS"] = ELEMENT_STATUS.Unchanged;
          }
        }

        m_time_control.SetInformation("Groups", "[" + m_ls_expanded_groups.Count + "]");

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on GroupService.GenerateTerminalsFromGroups");

        Log.Exception(_ex);
      }

      return false;
    } // GenerateTerminalsFromGroups

    //------------------------------------------------------------------------------
    // PURPOSE : Check if exist the terminal in memory. If not exists will add and if exists the status is "modified"
    //
    //  PARAMS :
    //      - INPUT :
    //          TerminaId
    //          ElementId
    //          ElementType
    //      - OUTPUT :
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    static private Boolean AddTerminalIfNotE(Int64 TerminaId, Int64 MasterId, Int64 ElementId, Int32 ElementType)
    {
      DataRow _terminal_group;
      Object[] _row_to_find;

      _row_to_find = new Object[] { TerminaId, ElementId, ElementType };
      _terminal_group = m_terminals_group.Rows.Find(_row_to_find);
      if (_terminal_group == null)
      {
        _row_to_find = new Object[] { TerminaId, MasterId, ElementId, ElementType, (Int32)ELEMENT_STATUS.Added };
        m_terminals_group.Rows.Add(_row_to_find);
      }
      else
      {
        if ((ELEMENT_STATUS)_terminal_group[COLUMN_EXPLOIT_STATUS] == ELEMENT_STATUS.Deleted)
        {
          _terminal_group[COLUMN_EXPLOIT_STATUS] = (Int32)ELEMENT_STATUS.Unchanged;
        }
      }

      return true;
    } // AddTerminalIfNotE

    //------------------------------------------------------------------------------
    // PURPOSE : Check if exist the terminal in memory. If not exists will add and if exists the status is "deleted"
    //
    //  PARAMS :
    //      - INPUT :
    //          TerminaId
    //          ElementId
    //          ElementType
    //      - OUTPUT :
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    static private Boolean DelTerminalIfNotE(Int64 TerminaId, Int64 MasterId, Int64 ElementId, Int32 ElementType)
    {
      DataRow _terminal_group;
      Object[] _row_to_find;

      _row_to_find = new Object[] { TerminaId, ElementId, ElementType };
      _terminal_group = m_terminals_group.Rows.Find(_row_to_find);
      if (_terminal_group == null)
      {
        _row_to_find = new Object[] { TerminaId, MasterId, ElementId, ElementType, (Int32)ELEMENT_STATUS.Deleted };
        m_terminals_group.Rows.Add(_row_to_find);
      }
      else
      {
        _terminal_group[COLUMN_EXPLOIT_STATUS] = (Int32)ELEMENT_STATUS.Deleted;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Generate terminals related to modified groups. This process can be recursive.
    //
    //  PARAMS :
    //      - INPUT :
    //       Params: Contain information about the group to generate
    //          Trx: SqlTransaction
    //      - OUTPUT :
    //    Terminals: Terminals generated
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    static private Boolean GetTerminalsFromGroups(Groups.INPUT_GROUP_PARAMS Params, ref DataTable Terminals, SqlTransaction Trx)
    {
      StringBuilder _sb;
      DataTable _elements;
      Groups.INPUT_GROUP_PARAMS _params;

      _sb = new StringBuilder();
      _sb.AppendLine("    SELECT   GR_ID           AS GROUP_ID ");
      _sb.AppendLine("           , GR_ENABLED      AS GROUP_ENABLED ");
      _sb.AppendLine("           , GE_ELEMENT_ID   AS ELEMENT_ID ");
      _sb.AppendLine("           , GE_ELEMENT_TYPE AS ELEMENT_TYPE ");
      _sb.AppendLine("      FROM   GROUPS ");
      _sb.AppendLine(" LEFT JOIN   GROUP_ELEMENTS ");
      _sb.AppendLine("        ON   GR_ID = GE_GROUP_ID ");

      if (Params.ElementId == 0)
      {
        _sb.AppendLine("     WHERE ( GR_EXPANDED IS NULL ");
        _sb.AppendLine("        OR   GR_UPDATED > GR_EXPANDED ");
        _sb.AppendLine("        OR   DATEDIFF(MINUTE,GETDATE(),GR_EXPANDED) > @pAutoExpandAfterMinutes ) ");
      }
      else
      {
        _sb.AppendLine("     WHERE   GR_ID = @pGroupId ");
      }
      _sb.AppendLine("  ORDER BY   GR_ID DESC, GE_ELEMENT_TYPE ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAutoExpandAfterMinutes", SqlDbType.Int).Value = GeneralParam.GetInt32("WCP", "Groups.AutoExpandAfterMinutes", 30);
          _cmd.Parameters.Add("@pGroupId", SqlDbType.BigInt).Value = Params.ElementId;

          _elements = new DataTable();

          m_time_control.IniProcess("*Accessing DB");
          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            m_time_control.EndProcess("*Accessing DB");
            _elements.Load(_reader);
          }

          foreach (DataRow _dr in _elements.Rows)
          {
            _params = new Groups.INPUT_GROUP_PARAMS();
            _params.ElementId = (Int64)_dr["ELEMENT_ID"];
            _params.ElementType = (GROUP_ELEMENT_TYPE)_dr["ELEMENT_TYPE"];
            _params.ElementEnabled = (Boolean)_dr["GROUP_ENABLED"];

            if (Params.ParentId == 0)
            {
              _params.ParentId = (Int64)_dr["GROUP_ID"];
              _params.ParentType = GROUP_ELEMENT_TYPE.GROUP;
              if (!m_ls_expanded_groups.Contains(_params.ParentId))
              {
                // Note expanded groups for update GR_EXPANDED field
                m_ls_expanded_groups.Add(_params.ParentId);
              }
            }
            else
            {
              _params.ParentId = Params.ParentId;
              _params.ParentType = Params.ParentType;
            }

            if (_params.ElementType == GROUP_ELEMENT_TYPE.GROUP)
            {
              if (!GetTerminalsFromGroups(_params, ref Terminals, Trx))
              {
                return false;
              }
            }
            else
            {
              if (!GetTerminalsFromMemory(_params, ref Terminals, Trx))
              {
                return false;
              }
            }

          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on GetTerminalsFromGroups");

        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Generate terminals from terminals list
    //
    //  PARAMS :
    //      - INPUT : 
    //        List : TerminalList
    //        Trx  : SqlTransaction
    //      - OUTPUT :
    //   Terminals : Terminals generated
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    static private Boolean GetTerminalsFromTerminalList(TerminalList List, bool FilterByGroupDeletedStatus, out DataTable Terminals, SqlTransaction Trx)
    {
      DataTable _elements;
      Groups.INPUT_GROUP_PARAMS _params;
      List<GROUP_ELEMENT_TYPE> _ls_types;

      Terminals = Groups.CreateElementTable(true);

      _ls_types = new List<GROUP_ELEMENT_TYPE>();
      _ls_types.Add(GROUP_ELEMENT_TYPE.PROV);
      _ls_types.Add(GROUP_ELEMENT_TYPE.ZONE);
      _ls_types.Add(GROUP_ELEMENT_TYPE.AREA);
      _ls_types.Add(GROUP_ELEMENT_TYPE.BANK);
      _ls_types.Add(GROUP_ELEMENT_TYPE.TERM);
      _ls_types.Add(GROUP_ELEMENT_TYPE.GROUP);

      foreach (GROUP_ELEMENT_TYPE _element_type in _ls_types)
      {
        _elements = List.GetElementsList(_element_type);

        foreach (DataRow _row_elements in _elements.Rows)
        {
          _params = new Groups.INPUT_GROUP_PARAMS();
          _params.ElementId = (Int64)_row_elements[TerminalList.COLUMN_ID];
          _params.ElementType = _element_type;
          _params.ElementEnabled = true;

          _params.ParentId = 0;
          _params.ParentType = 0;

          if (_element_type == GROUP_ELEMENT_TYPE.GROUP)
          {
            if (!GetTerminalsGroupsFromMemory(_params, FilterByGroupDeletedStatus, ref Terminals, Trx))
            {
              return false;
            }
          }
          else
          {
            if (!GetTerminalsFromMemory(_params, ref Terminals, Trx))
            {
              return false;
            }
          }

        }

      }

      return true;
    } // GetTerminalsFromTerminalList

    //------------------------------------------------------------------------------
    // PURPOSE : Load the terminal_group from database and all information about terminals
    //
    //  PARAMS :
    //      - INPUT :
    //          - Trx: SqlTransaction
    //      - OUTPUT :
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    static private Boolean Load(SqlTransaction Trx)
    {
      StringBuilder _sb;

      TerminalTypes[] GamingSeat;

      GamingSeat = new TerminalTypes[] { TerminalTypes.GAMING_TABLE_SEAT };

      try
      {
        m_terminals_group = CreateExploitTable();

        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   TE_TERMINAL_ID   AS " + COLUMN_EXPLOIT_TER_ID);
        _sb.AppendLine("      ,   TE_MASTER_ID  AS " + COLUMN_EXPLOIT_MASTER_ID);
        _sb.AppendLine("      ,   TG_ELEMENT_ID   AS " + COLUMN_EXPLOIT_ELEMENT_ID);
        _sb.AppendLine("      ,   TG_ELEMENT_TYPE AS " + COLUMN_EXPLOIT_TYPE);
        _sb.AppendLine("      ,   @pDeleted       AS " + COLUMN_EXPLOIT_STATUS);
        _sb.AppendLine("   FROM   TERMINAL_GROUPS");
        _sb.AppendLine(" INNER    JOIN   TERMINALS");
        _sb.AppendLine("    ON    TG_TERMINAL_ID = TE_TERMINAL_ID");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pDeleted", SqlDbType.Int).Value = (Int32)ELEMENT_STATUS.Deleted;
          _sql_cmd.Parameters.Add("@pTypeGroup", SqlDbType.Int).Value = (Int32)GROUP_ELEMENT_TYPE.GROUP;
          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            m_time_control.IniProcess("*Accessing DB");
            _da.Fill(m_terminals_group);
            m_time_control.EndProcess("*Accessing DB");
          }
        }

        m_all_terminals = new DataTable();

        _sb.Length = 0;
        _sb.AppendLine("   SELECT   TE_TERMINAL_ID ");
        _sb.AppendLine("          , TE_MASTER_ID ");
        _sb.AppendLine("          , TE_PROV_ID ");
        _sb.AppendLine("          , TE_BANK_ID ");
        _sb.AppendLine("          , BK_AREA_ID ");
        _sb.AppendLine("          , AR_SMOKING ");
        _sb.AppendLine("     FROM   TERMINALS ");
        _sb.AppendLine("LEFT JOIN   BANKS");
        _sb.AppendLine("       ON   BK_BANK_ID = TE_BANK_ID ");
        _sb.AppendLine("LEFT JOIN   AREAS");
        _sb.AppendLine("       ON   AR_AREA_ID = BK_AREA_ID ");
        // Vaklidar = vista o sin vista
        //_sb.AppendLine("INNER JOIN  TERMINALS_LAST_CHANGED ");
        //_sb.AppendLine("       ON   TLC_TERMINAL_ID = TE_TERMINAL_ID ");
        _sb.AppendLine("    WHERE   TE_TYPE = 1 ");
        _sb.AppendLine("      AND   ISNULL(TE_SERVER_ID, 0) =  0");
        _sb.AppendLine("      AND   TE_BLOCKED = 0");
        _sb.AppendLine("      AND   TE_STATUS IN (0, 1) ");
        _sb.AppendLine("      AND   TE_TERMINAL_TYPE IN ( " + Misc.GamingTerminalTypeListToString(false) +
                                                          " , " + Misc.TerminalTypeListToString(GamingSeat) + " ) ");


        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            m_time_control.IniProcess("*Accessing DB");
            _da.Fill(m_all_terminals);
            m_time_control.EndProcess("*Accessing DB");
          }
        }
        m_all_terminals.PrimaryKey = new DataColumn[] { m_all_terminals.Columns[0] };

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Exception: GroupService.Load");

        Log.Exception(_ex);
      }

      return false;
    } // Load

    //------------------------------------------------------------------------------
    // PURPOSE : Save into terminals_groups database
    //
    //  PARAMS :
    //      - INPUT :
    //        - ExpandedElements: List<Int64>
    //        - Trx: SqlTransaction
    //      - OUTPUT :
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    static private Boolean Save(SqlTransaction Trx)
    {
      StringBuilder _sb;
      SqlParameter _param;
      DataRow[] _rows_for_update;
      Int32 _rows_updated;

      _sb = new StringBuilder();

      try
      {
        _rows_for_update = m_terminals_group.Select("STATUS > 0", "", DataViewRowState.Unchanged);
        foreach (DataRow _dr in _rows_for_update)
        {
          _dr.SetModified();
        }

        _rows_for_update = m_terminals_group.Select("STATUS > 0");
        if (_rows_for_update.Length != 0)
        {
          _sb.AppendLine("IF @pElementStatus = @pAdded");
          _sb.AppendLine("   INSERT INTO TERMINAL_GROUPS (TG_TERMINAL_ID,TG_ELEMENT_ID , TG_ELEMENT_TYPE) VALUES (@pTerminalId, @pElementId, @pElementType)");
          _sb.AppendLine("ELSE IF @pElementStatus = @pDeleted");
          _sb.AppendLine("   DELETE TERMINAL_GROUPS WHERE TG_TERMINAL_ID = @pTerminalId AND TG_ELEMENT_ID = @pElementId AND TG_ELEMENT_TYPE = @pElementType");

          using (SqlDataAdapter _da = new SqlDataAdapter())
          {
            SqlCommand _cmd = new SqlCommand(_sb.ToString());
            _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = COLUMN_EXPLOIT_TER_ID;
            _cmd.Parameters.Add("@pElementId", SqlDbType.BigInt).SourceColumn = COLUMN_EXPLOIT_ELEMENT_ID;
            _cmd.Parameters.Add("@pElementType", SqlDbType.Int).SourceColumn = COLUMN_EXPLOIT_TYPE;
            _cmd.Parameters.Add("@pElementStatus", SqlDbType.Int).SourceColumn = COLUMN_EXPLOIT_STATUS;
            _cmd.Parameters.Add("@pAdded", SqlDbType.Int).Value = (Int32)ELEMENT_STATUS.Added;
            _cmd.Parameters.Add("@pDeleted", SqlDbType.Int).Value = (Int32)ELEMENT_STATUS.Deleted;

            _cmd.Connection = Trx.Connection;
            _cmd.Transaction = Trx;
            _cmd.UpdatedRowSource = UpdateRowSource.None;

            _da.UpdateCommand = _cmd;
            _da.InsertCommand = _cmd.Clone();

            _da.ContinueUpdateOnError = true;
            _da.UpdateBatchSize = 1000;

            m_time_control.IniProcess("*Accessing DB");
            _rows_updated = _da.Update(_rows_for_update);
            m_time_control.EndProcess("*Accessing DB");
          }

          //if (_rows_updated != _rows_for_update.Length)
          //{
          //  Log.Error("Error on GroupService.Save: Regs to update: " + _rows_for_update.Length + ", Num. updated " + _rows_updated);

          //  return false;
          //}

        }

        _sb.Length = 0;
        _sb.AppendLine("UPDATE GROUPS SET GR_EXPANDED = @pGroupExpanded WHERE GR_ID = @pGroupId ");
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _param = _sql_cmd.Parameters.Add("@pGroupId", SqlDbType.BigInt);
          _sql_cmd.Parameters.Add("@pGroupExpanded", SqlDbType.DateTime).Value = m_expanded_group;
          foreach (Int64 _id in m_ls_expanded_groups)
          {
            _param.Value = _id;
            _sql_cmd.ExecuteNonQuery();

            if (!ForceGenerationForParentsGroup(m_expanded_group, _id, Trx))
            {
              return false;
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Exception: GroupService.Save");

        Log.Exception(_ex);
      }

      return false;
    } // Save

    //------------------------------------------------------------------------------
    // PURPOSE: Update groups table to force exploit of direct parents of the group
    //
    //  PARAMS:
    //      - INPUT:
    //          - Date: DateTime
    //          - GroupId: Int64
    //          - Trx: SqlTransaction
    //      - OUTPUT:
    // 
    // RETURNS :
    //      True: successful otherwise False
    //
    public static Boolean ForceGenerationForParentsGroup(DateTime Date, Int64 GroupId, SqlTransaction Trx)
    {
      StringBuilder _sb_groups;
      StringBuilder _sb_update;
      Int64 _current_group;
      List<Int64> _parents_to_exploit;

      _sb_groups = new StringBuilder();
      _sb_update = new StringBuilder();
      _parents_to_exploit = new List<Int64>();

      try
      {
        _sb_groups.AppendLine(" SELECT   GE_GROUP_ID ");
        _sb_groups.AppendLine("   FROM   GROUP_ELEMENTS");
        _sb_groups.AppendLine("  WHERE   GE_ELEMENT_ID = @pGroupId ");
        _sb_groups.AppendLine("    AND   GE_ELEMENT_TYPE = @pGroupType ");

        using (SqlCommand _cmd = new SqlCommand(_sb_groups.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pGroupId", SqlDbType.BigInt).Value = GroupId;
          _cmd.Parameters.Add("@pGroupType", SqlDbType.Int).Value = GROUP_ELEMENT_TYPE.GROUP;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              _current_group = _reader.GetInt64(0);
              _parents_to_exploit.Add(_current_group);
            }
          }
        }

        _sb_update.AppendLine("UPDATE   GROUPS                  ");
        _sb_update.AppendLine("   SET   GR_UPDATED = @pDate ");
        _sb_update.AppendLine(" WHERE   GR_ID      = @pGroupId ");
        _sb_update.AppendLine("   AND   GR_ENABLED = 1 ");

        foreach (Int64 _group in _parents_to_exploit)
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb_update.ToString(), Trx.Connection, Trx))
          {
            _sql_cmd.Parameters.Add("@pDate", SqlDbType.DateTime).Value = Date;
            _sql_cmd.Parameters.Add("@pGroupId", SqlDbType.BigInt).Value = _group;
            _sql_cmd.ExecuteNonQuery();
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Exception: ForceParentsExploit, Group Id: " + GroupId);

        Log.Exception(_ex);
      }

      return false;
    } // ForceGenerationForParentsGroup

    //------------------------------------------------------------------------------
    // PURPOSE : Get all promotions list of elements
    //
    //  PARAMS :
    //      - INPUT :
    //        -  ElementType: EXPLOIT_ELEMENT_TYPE
    //        -  Trx: SqlTransaction
    //
    //      - OUTPUT :
    //        -  DtPromotions: DataTable
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    public static Boolean GetActivePromotions(EXPLOIT_ELEMENT_TYPE ElementType, out DataTable DtPromotions, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      DtPromotions = CreateElementsTable();

      try
      {
        _sb.AppendLine("SELECT   PM_PROMOTION_ID AS ELEMENT_ID");

        switch (ElementType)
        {
          case EXPLOIT_ELEMENT_TYPE.PROMOTION:
            _sb.AppendLine("     ,   PM_PROVIDER_LIST AS ELEMENT_LIST");
            break;

          //SDS 27-10-2015
          //case EXPLOIT_ELEMENT_TYPE.PROMOTION_PLAYED:
          //  _sb.AppendLine("     ,   PM_PLAYED_TERMINAL_LIST AS ELEMENT_LIST");
          //  break;

          case EXPLOIT_ELEMENT_TYPE.PROMOTION_RESTRICTED:
            _sb.AppendLine("     ,   PM_RESTRICTED_TO_TERMINAL_LIST AS ELEMENT_LIST");
            break;

          default:
            _sb.AppendLine("     ,   PM_PROVIDER_LIST AS ELEMENT_LIST");
            break;
        }

        _sb.AppendLine("  FROM   PROMOTIONS");
        _sb.AppendLine(" WHERE   PM_ENABLED = 1");
        _sb.AppendLine("   AND   GETDATE() >= PM_DATE_START ");
        _sb.AppendLine("   AND   GETDATE() < PM_DATE_FINISH");

        //SDS 28-10-2015 
        if (ElementType != EXPLOIT_ELEMENT_TYPE.PROMOTION_PLAYED)
          _sb.AppendLine("   AND   PM_CREDIT_TYPE IN (@pTypeNR1, @pTypeNR2) ");

        switch (ElementType)
        {
          case EXPLOIT_ELEMENT_TYPE.PROMOTION:
            _sb.AppendLine("   AND   PM_PROVIDER_LIST IS NOT NULL");
            break;

          case EXPLOIT_ELEMENT_TYPE.PROMOTION_RESTRICTED:
            _sb.AppendLine("   AND   PM_RESTRICTED_TO_TERMINAL_LIST IS NOT NULL");
            break;
          //SDS 27-10-2015
          //case EXPLOIT_ELEMENT_TYPE.PROMOTION_PLAYED:
          //  _sb.AppendLine("   AND   PM_PLAYED_TERMINAL_LIST IS NOT NULL");
          //  break;

          default:
            _sb.AppendLine("   AND   PM_PROVIDER_LIST IS NOT NULL");
            break;
        }

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          //SDS 28-10-2015 
          if (ElementType != EXPLOIT_ELEMENT_TYPE.PROMOTION_PLAYED)
          {
            _sql_cmd.Parameters.Add("@pTypeNR1", SqlDbType.Int).Value = (Int32)ACCOUNT_PROMO_CREDIT_TYPE.NR1;
            _sql_cmd.Parameters.Add("@pTypeNR2", SqlDbType.Int).Value = (Int32)ACCOUNT_PROMO_CREDIT_TYPE.NR2;
          }
          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(DtPromotions);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Exception: GroupService.GetActivePromotions");

        Log.Exception(_ex);
      }

      return false;
    } // GetActivePromotions

    //------------------------------------------------------------------------------
    // PURPOSE : Get all site jackpot list of elements
    //
    //  PARAMS :
    //      - INPUT :
    //        -  Trx: SqlTransaction
    //
    //      - OUTPUT :
    //        -  DtJackPot: DataTable
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    public static Boolean GetActiveSiteJackpot(out DataTable DtJackPot, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      DtJackPot = CreateElementsTable();
      try
      {
        _sb.AppendLine("SELECT   1 AS ELEMENT_ID");
        _sb.AppendLine("       , SJP_TERMINAL_LIST AS ELEMENT_LIST");
        _sb.AppendLine("  FROM   SITE_JACKPOT_PARAMETERS");
        _sb.AppendLine(" WHERE   SJP_TERMINAL_LIST IS NOT NULL");
        _sb.AppendLine("   AND   SJP_ENABLED = 1");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(DtJackPot);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Exception: GroupService.GetActiveJackPot");

        Log.Exception(_ex);
      }

      return false;
    } // GetActiveSiteJackpot


    //------------------------------------------------------------------------------
    // PURPOSE : Get all patterns list of elements
    //
    //  PARAMS :
    //      - INPUT :
    //        -  Trx: SqlTransaction
    //
    //      - OUTPUT :
    //        -  DtPatterns: DataTable
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    public static Boolean GetActivePatterns(out DataTable DtPatterns, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      DtPatterns = CreateElementsTable();
      try
      {
        _sb.AppendLine("SELECT   PT_ID AS ELEMENT_ID");
        _sb.AppendLine("       , PT_RESTRICTED_TO_TERMINAL_LIST AS ELEMENT_LIST");
        _sb.AppendLine("  FROM   PATTERNS");
        _sb.AppendLine(" WHERE   PT_ACTIVE = 1");
        _sb.AppendLine("   AND   (ISNULL(PT_SCHEDULE_TIME_FROM, 0) = 0 OR DATEADD(S, PT_SCHEDULE_TIME_FROM, CONVERT(DATETIME,CONVERT(VARCHAR(10), GETDATE(), 103),103)) <= GETDATE())");
        _sb.AppendLine("   AND   (ISNULL(PT_SCHEDULE_TIME_TO,   0) = 0 OR DATEADD(S, PT_SCHEDULE_TIME_TO,   CONVERT(DATETIME,CONVERT(VARCHAR(10), GETDATE(), 103),103)) >  GETDATE())");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(DtPatterns);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Exception: GroupService.GetActivePatterns");

        Log.Exception(_ex);
      }

      return false;
    } // GetActivePatterns

    //------------------------------------------------------------------------------
    // PURPOSE : Get active progressives list of elements
    //
    //  PARAMS :
    //      - INPUT :
    //        -  Trx: SqlTransaction
    //
    //      - OUTPUT :
    //        -  DtProgressives: DataTable
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    public static Boolean GetActiveProgressives(out DataTable DtProgressives, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      DtProgressives = CreateElementsTable();
      try
      {
        _sb.AppendLine("SELECT   PGS_PROGRESSIVE_ID   AS ELEMENT_ID ");
        _sb.AppendLine("       , PGS_TERMINAL_LIST    AS ELEMENT_LIST ");
        _sb.AppendLine("  FROM   PROGRESSIVES ");
        _sb.AppendLine(" WHERE   PGS_STATUS = @pStatus ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (Int32)Progressives.PROGRESSIVE_STATUS.ACTIVE;

          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(DtProgressives);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Exception: GroupService.GetActiveProgressives");

        Log.Exception(_ex);
      }

      return false;
    } // GetActiveProgressives

    //------------------------------------------------------------------------------
    // PURPOSE : Get active progressives list of elements
    //
    //  PARAMS :
    //      - INPUT :
    //        -  Trx: SqlTransaction
    //
    //      - OUTPUT :
    //        -  DtLCDMessages: DataTable
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    public static Boolean GetActiveLCDMessage(out DataTable DtLCDMessages, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      DtLCDMessages = CreateElementsTable();

      try
      {
        _sb.AppendLine(" SELECT   MSG_UNIQUE_ID     AS ELEMENT_ID ");
        _sb.AppendLine("        , ISNULL(MSG_TERMINAL_LIST, '') AS ELEMENT_LIST  ");
        _sb.AppendLine("   FROM   LCD_MESSAGES ");
        _sb.AppendLine("  WHERE   MSG_ENABLED = 1 ");

        if (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") == 0)
        {
          _sb.AppendLine("    AND   MSG_TYPE <> @pMsgType ");
        }

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          if (GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode") == 0)
          {
            _sql_cmd.Parameters.Add("@pMsgType", SqlDbType.Int).Value = LCDMessages.ENUM_TYPE_MESSAGE.CANCELED_ACCOUNT;
          }

          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(DtLCDMessages);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Exception: GroupService.GetActiveLCDMessage");

        Log.Exception(_ex);
      }

      return false;
    } // GetActiveLCDMessage

    //------------------------------------------------------------------------------
    // PURPOSE : Get active buckets multiplier list of elements
    //
    //  PARAMS :
    //      - INPUT :
    //        -  Trx: SqlTransaction
    //
    //      - OUTPUT :
    //        -  DtBucketsMultiplier: DataTable
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    public static Boolean GetActiveBucketsMultiplier(out DataTable DtBucketsMultiplier, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      DtBucketsMultiplier = CreateElementsTable();

      try
      {
        _sb.AppendLine(" SELECT   BM_BUCKET_ID     AS ELEMENT_ID ");
        _sb.AppendLine("        , ISNULL(BM_BUCKET_TERMINALS_LIST, '') AS ELEMENT_LIST  ");
        _sb.AppendLine("   FROM   BUCKETS_MULTIPLIER_SCHEDULE ");
        _sb.AppendLine("  WHERE   BM_BUCKET_ENABLED = 1 ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(DtBucketsMultiplier);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Exception: GroupService.GetActiveBucketsMultiplier");

        Log.Exception(_ex);
      }

      return false;
    } // GetActiveBucketsMultiplier

    //------------------------------------------------------------------------------
    // PURPOSE : Get active GameGateway list of elements
    //
    //  PARAMS :
    //      - INPUT :
    //        -  Trx: SqlTransaction
    //
    //      - OUTPUT :
    //        -  DtGameGateway: DataTable
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    public static Boolean GetActiveGameGateway(out DataTable DtGameGateway, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      DtGameGateway = CreateElementsTable();
      try
      {
        _sb.AppendLine("SELECT   1                    AS ELEMENT_ID ");
        _sb.AppendLine("       , GTL_TERMINAL_LIST    AS ELEMENT_LIST ");
        _sb.AppendLine("  FROM   GAMEGATEWAY_TERMINAL_LIST ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(DtGameGateway);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Exception: GroupService.GetActiveGameGateway");

        Log.Exception(_ex);
      }

      return false;
    } // GetActiveGameGateway


    //------------------------------------------------------------------------------
    // PURPOSE : Get active terminals in jackpot contributions group
    //
    //  PARAMS :
    //      - INPUT :
    //        -  Trx: SqlTransaction
    //
    //      - OUTPUT :
    //        -  DtJackpotContributionTerminals: DataTable
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    public static Boolean GetActiveJackpotContributionTerminals(out DataTable DtJackpotContributionTerminals, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      DtJackpotContributionTerminals = CreateElementsTable();

      try
      {
        _sb.AppendLine(" SELECT   JSC_ID                      AS ELEMENT_ID ");
        _sb.AppendLine("        , ISNULL(JSC_TERMINALS, '')   AS ELEMENT_LIST ");
        _sb.AppendLine("   FROM   JACKPOTS_SETTINGS_CONTRIBUTION ");
        //_sb.AppendLine("  WHERE   BM_BUCKET_ENABLED = 1 ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(DtJackpotContributionTerminals);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Exception: GroupService.GetActiveJackpotContributionTerminals");
        Log.Exception(_ex);
      }

      return false;
    } // GetActiveJackpotContributionTerminals

    //------------------------------------------------------------------------------
    // PURPOSE : Get active terminals in jackpot contributions group
    //
    //  PARAMS :
    //      - INPUT :
    //        -  Trx: SqlTransaction
    //
    //      - OUTPUT :
    //        -  DtJackpotContributionTerminals: DataTable
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    public static Boolean GetActiveJackpotAwardTerminalsList(out DataTable DtJackpotAwardTerminals, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      DtJackpotAwardTerminals = CreateElementsTable();

      try
      {
        
        _sb.AppendLine(" SELECT   JAF_ID AS ELEMENT_ID                         ");
        _sb.AppendLine("        , ISNULL(JAF_SELECTED_EGM, '') AS ELEMENT_LIST ");
        _sb.AppendLine("  FROM    JACKPOTS_AWARD_FILTERS                       ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(DtJackpotAwardTerminals);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Exception: GroupService.GetActiveJackpotAwardTerminalsList");
        Log.Exception(_ex);
      }

      return false;
    } // GetActiveJackpotAwardTerminalsList

    /// <summary>
    /// Gets the list of active terminals
    /// </summary>
    /// <param name="DtReservedTerminals"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean GetActiveReservedTerminals(out DataTable DtReservedTerminals, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      DtReservedTerminals = CreateElementsTable();

      try
      {
        _sb.AppendLine(" SELECT   RTC_HOLDER_LEVEL     AS ELEMENT_ID ");
        _sb.AppendLine("        , ISNULL(RTC_TERMINAL_LIST, '') AS ELEMENT_LIST  ");
        _sb.AppendLine("   FROM   RESERVED_TERMINAL_CONFIGURATION ");
        _sb.AppendLine("  WHERE   RTC_ENABLED = 1 ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(DtReservedTerminals);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Exception: GroupService.GetActiveReservedTerminals");

        Log.Exception(_ex);
      }

      return false;
    } // GetActiveBucketsMultiplier

    //------------------------------------------------------------------------------
    // PURPOSE: Create the relation terminal-group table structure.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      DataTable
    //
    public static DataTable CreateElementsTable()
    {
      DataTable DtInfo = new DataTable();

      DtInfo.Columns.Add(COLUMN_ELEMENT_ID, Type.GetType("System.Int64"));
      DtInfo.Columns.Add(COLUMN_ELEMENT_LIST, Type.GetType("System.String"));

      DtInfo.PrimaryKey = new DataColumn[1] { DtInfo.Columns[COLUMN_ELEMENT_ID] };

      return DtInfo;
    } // CreateElementsTable

    //------------------------------------------------------------------------------
    // PURPOSE: Create the relation terminal-elements table structure.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      DataTable
    //
    private static DataTable CreateExploitTable()
    {
      DataTable DtInfo = new DataTable();


      DtInfo.Columns.Add(COLUMN_EXPLOIT_TER_ID, Type.GetType("System.Int64"));
      DtInfo.Columns.Add(COLUMN_EXPLOIT_MASTER_ID, Type.GetType("System.Int64"));
      DtInfo.Columns.Add(COLUMN_EXPLOIT_ELEMENT_ID, Type.GetType("System.Int64"));
      DtInfo.Columns.Add(COLUMN_EXPLOIT_TYPE, Type.GetType("System.Int32"));
      DtInfo.Columns.Add(COLUMN_EXPLOIT_STATUS, Type.GetType("System.Int32"));


      DtInfo.PrimaryKey = new DataColumn[3] { DtInfo.Columns[COLUMN_EXPLOIT_TER_ID]
                                            , DtInfo.Columns[COLUMN_EXPLOIT_ELEMENT_ID]
                                            , DtInfo.Columns[COLUMN_EXPLOIT_TYPE] };

      return DtInfo;
    } // CreateExploitTable

    //------------------------------------------------------------------------------
    // PURPOSE: Get a list of terminals with parent information from memory
    //
    //  PARAMS:
    //      - INPUT:
    //          - Params INPUT_GROUP_PARAMS: : List of information of the terminals
    //          - Trx: SqlTransaction
    //      - OUTPUT:
    //          - DtTerminals: DataTable
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    public static Boolean GetTerminalsFromMemory(Groups.INPUT_GROUP_PARAMS Params, ref DataTable DtTerminals, SqlTransaction Trx)
    {
      StringBuilder _sb;
      DataRow[] _drs;
      DataRow _row_to_add;

      try
      {
        _sb = new StringBuilder();

        switch (Params.ElementType)
        {
          case GROUP_ELEMENT_TYPE.ALL:
            _sb.Length = 0;
            break;
          case GROUP_ELEMENT_TYPE.PROV:
            _sb.AppendLine("TE_PROV_ID = @pElementId");
            break;
          case GROUP_ELEMENT_TYPE.ZONE:
            _sb.AppendLine("AR_SMOKING = @pElementId");
            break;
          case GROUP_ELEMENT_TYPE.AREA:
            _sb.AppendLine("BK_AREA_ID = @pElementId");
            break;
          case GROUP_ELEMENT_TYPE.BANK:
            _sb.AppendLine("TE_BANK_ID = @pElementId");
            break;
          case GROUP_ELEMENT_TYPE.TERM:
            _sb.AppendLine("TE_MASTER_ID = @pElementId");
            break;
          case GROUP_ELEMENT_TYPE.QUERY:
            break;
          default:
            return false;
        }

        _sb = _sb.Replace("@pElementId", Params.ElementId.ToString());

        _drs = m_all_terminals.Select(_sb.ToString());
        foreach (DataRow _dr in _drs)
        {
          if (DtTerminals.Rows.Find(new Object[] { _dr["TE_MASTER_ID"], _dr["TE_TERMINAL_ID"], (Int32)GROUP_ELEMENT_TYPE.TERM, Params.ParentId }) != null)
          {
            continue;
          }

          _row_to_add = DtTerminals.NewRow();
          _row_to_add[Groups.ELEMENTS_COLUMN_PARENT_ID] = Params.ParentId;
          _row_to_add[Groups.ELEMENTS_COLUMN_PARENT_TYPE] = Params.ParentType;
          _row_to_add[Groups.ELEMENTS_COLUMN_ENABLED] = Params.ElementEnabled;
          _row_to_add[Groups.ELEMENTS_COLUMN_TYPE] = (Int32)GROUP_ELEMENT_TYPE.TERM;
          _row_to_add[Groups.ELEMENTS_COLUMN_ID] = _dr["TE_TERMINAL_ID"];
          _row_to_add[Groups.ELEMENTS_COLUMN_MASTER_ID] = _dr["TE_MASTER_ID"];

          DtTerminals.Rows.Add(_row_to_add);
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Error("Error on GetTerminalsFromMemory: Element type " + Params.ElementType + ", Element Id " + Params.ElementId);

        Log.Exception(_ex);
      }

      return false;
    } // GetTerminalsFromMemory

    //------------------------------------------------------------------------------
    // PURPOSE: Get a list of terminals belong to a group.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Params INPUT_GROUP_PARAMS: : List of information of the terminals
    //          - Trx: SqlTransaction
    //      - OUTPUT:
    //          - DtTerminals: DataTable
    //
    // RETURNS :
    //      True: successful otherwise False
    //
    public static Boolean GetTerminalsGroupsFromMemory(Groups.INPUT_GROUP_PARAMS Params, bool FilterByDeletedStatus, ref DataTable DtTerminals, SqlTransaction Trx)
    {
      DataRow[] _drs;
      DataRow _row_to_add;
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.Append(COLUMN_EXPLOIT_ELEMENT_ID + " = " + Params.ElementId.ToString());
        _sb.Append(" AND " + COLUMN_EXPLOIT_TYPE + " = " + (Int32)EXPLOIT_ELEMENT_TYPE.GROUP);
        if (FilterByDeletedStatus)
        {
          _sb.Append(" AND " + COLUMN_EXPLOIT_STATUS + " <> " + (Int32)ELEMENT_STATUS.Deleted);
        }
        _drs = m_terminals_group.Select(_sb.ToString());

        foreach (DataRow _dr in _drs)
        {
          if (DtTerminals.Rows.Find(new Object[] { _dr[COLUMN_EXPLOIT_MASTER_ID], _dr[COLUMN_EXPLOIT_TER_ID], (Int32)GROUP_ELEMENT_TYPE.TERM, Params.ParentId }) != null)
          {
            continue;
          }

          _row_to_add = DtTerminals.NewRow();
          _row_to_add[Groups.ELEMENTS_COLUMN_PARENT_ID] = Params.ParentId;
          _row_to_add[Groups.ELEMENTS_COLUMN_PARENT_TYPE] = Params.ParentType;
          _row_to_add[Groups.ELEMENTS_COLUMN_ENABLED] = Params.ElementEnabled;
          _row_to_add[Groups.ELEMENTS_COLUMN_TYPE] = (Int32)GROUP_ELEMENT_TYPE.TERM;
          _row_to_add[Groups.ELEMENTS_COLUMN_ID] = _dr[COLUMN_EXPLOIT_TER_ID];
          _row_to_add[Groups.ELEMENTS_COLUMN_MASTER_ID] = _dr[COLUMN_EXPLOIT_MASTER_ID];

          DtTerminals.Rows.Add(_row_to_add);
        }

        return true;

      }
      catch (Exception _ex)
      {
        Log.Error("Error on GetTerminalsGroupsFromMemory: Element id " + Params.ElementId);

        Log.Exception(_ex);
      }

      return false;
    } // GetTerminalsGroupsFromMemory


    #endregion // Private Methods
  }
}

