//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Template.cs
// 
//   DESCRIPTION: Class base for template
// 
//        AUTHOR: Marcos Piedra
// 
// CREATION DATE: 31-JAN-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-JAN-2012 MPO    First release.
// 02-MAY-2012 MPO    New feature, set an image.
// 20-JUN-2016 LTC    Product Backlog Item 13612,13613,13614: Witholding Evidence: Campeche.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using WSI.Common;
using System.Drawing;

namespace WSI.Common
{

  public abstract class Template
  {

    public enum ACTION_IMG
    { 
      NONE = 0,
      SCALE_TO_FIT = 1,
    }

    protected class ImageInfo
    {
      private RectangleF m_rect;
      private Image m_image;
      private ACTION_IMG m_action_img;

      public RectangleF Rect
      {
        get { return m_rect; }
        set { m_rect = value; }
      }     
      public Image Image
      {
        get { return m_image; }
        set { m_image = value; }
      }
      public Byte[] BytesImg
      {
        get 
        {
          using (MemoryStream _mm = new MemoryStream())
          {
            m_image.Save(_mm, m_image.RawFormat);
            return _mm.GetBuffer(); 
          }          
        }
      }
      public ACTION_IMG ActionImg
      {
        get { return m_action_img; }
        set { m_action_img = value; }
      }  

    }

    #region Members

    protected Dictionary<String, String> m_fields;
    protected DataTable m_images;
    protected Stream m_stream_target;
    protected Stream m_stream_template;
    protected Boolean m_changed;
    protected String m_path_template;
    public String m_template_ext;

    #endregion // Members

    #region Enums

    public enum TYPE_TEMPLATE
    {
      PDF = 0,
      WORD = 1,
      EXCEL = 2 // LTC 20-JUN-2016
    }

    #endregion // Enums

    #region Constructor

    //------------------------------------------------------------------------------
    // PURPOSE: Constructor. Initialize properties
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public Template()
    {

      m_stream_template = null;
      m_stream_target = null;
      m_fields = new Dictionary<String, String>();
      m_images = new DataTable();
      m_images.Columns.Add("NumPage", Type.GetType("System.Int32"));
      m_images.Columns.Add("Image", Type.GetType("System.Object"));
      m_images.Columns.Add("Action", Type.GetType("System.Int16")).DefaultValue = (Int16)ACTION_IMG.NONE;
      m_changed = false;
      m_path_template = "";

    }

    #endregion // Constructor

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Set field to dictionary
    //
    //  PARAMS:
    //      - INPUT:
    //          - FieldName: Key of dictionary
    //          - FieldValue: Value of dictionary
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public void SetField(String FieldName, String FieldValue)
    {
      m_changed = true;
      m_fields[FieldName] = FieldValue;
    } // SetField

    //------------------------------------------------------------------------------
    // PURPOSE: Set a Image to list
    //
    //  PARAMS:
    //      - INPUT:
    //          - Image
    //          - Rectangle
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public void SetImage(Byte[] ImageBytes, Rectangle Rect)
    {
      ImageInfo _img_info;

      _img_info = new ImageInfo();
      _img_info.Image = System.Drawing.Image.FromStream(new MemoryStream(ImageBytes));
      _img_info.Rect = Rect;

      m_changed = true;
      m_images.Rows.Add(new Object[] { 1, _img_info });
    } // SetImage

    //------------------------------------------------------------------------------
    // PURPOSE: Set a Image to list
    //
    //  PARAMS:
    //      - INPUT:
    //          - Image
    //          - Rectangle
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public void SetScaleImage(Byte[] ImageBytes, RectangleF Rect)
    {
      ImageInfo _img_info;

      _img_info = new ImageInfo();
      _img_info.Image = System.Drawing.Image.FromStream(new MemoryStream(ImageBytes));
      _img_info.Rect = Rect;
      _img_info.ActionImg = ACTION_IMG.SCALE_TO_FIT;

      m_changed = true;
      m_images.Rows.Add(new Object[] { 1, _img_info });
    } // SetImage

    //------------------------------------------------------------------------------
    // PURPOSE: Show a file 
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public virtual void Show()
    {
    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE: Print a template file.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public virtual void Print()
    {
    } // Print

    //------------------------------------------------------------------------------
    // PURPOSE: Save the file
    //
    //  PARAMS:
    //      - INPUT:
    //          - FileName: File where the template is saved
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public virtual void SaveAs(String FileName)
    {
    } // SaveAs

    //------------------------------------------------------------------------------
    // PURPOSE: Close the corresponding objects
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public virtual void Close()
    {
    } // Close

    //------------------------------------------------------------------------------
    // PURPOSE: Filling the template that contains the fields and images
    //
    //  PARAMS:
    //      - INPUT:
    //          - FileName: Template that contains the fields and images
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Boolean: Is correct the process of filling?
    // 
    //   NOTES:
    // 
    public virtual Boolean Fill()
    {

      return false;
    } // Fill

    //------------------------------------------------------------------------------
    // PURPOSE: Get a list of field name 
    //
    //  PARAMS:
    //      - INPUT:
    //          - FileName: File to get field name
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - List<String>: List of field name
    // 
    //   NOTES:
    //    
    public virtual List<String> Fields()
    {

      return null;
    } // GetListOfField

    //------------------------------------------------------------------------------
    // PURPOSE: Get the current stream that contain the template has filled.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Stream:
    // 
    //   NOTES:
    // 
    public virtual Stream GetStream()
    {

      return null;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Create a generic template
    //
    //  PARAMS:
    //      - INPUT:
    //          - FileName: Path of template file  
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Template: Templates
    // 
    //   NOTES:
    //    
    public static Template Create(String FileNameTemplate)
    {

      Template _template;
      String _extension_file_name;

      _extension_file_name = Path.GetExtension(FileNameTemplate);

      switch (_extension_file_name.ToUpper())
      {

        case ".PDF":

          _template = new TemplatePDF(FileNameTemplate);

          return _template;

        case ".DOTX":
        case ".DOT":

          _template = new TemplateWord(FileNameTemplate);

          return null;

        default:

          return null;

      }

    } // Build

    //------------------------------------------------------------------------------
    // PURPOSE: Create a generic template from a MemoryStream
    //
    //  PARAMS:
    //      - INPUT:
    //          - FileName: Path of template file  
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Template: Templates
    // 
    //   NOTES:
    //    
    public static Template Create(MemoryStream MemoryStream, TYPE_TEMPLATE Type)
    {

      Template _template;

      switch (Type)
      {

        case TYPE_TEMPLATE.PDF:

          _template = new TemplatePDF(MemoryStream);

          return _template;

        case TYPE_TEMPLATE.WORD:

          _template = new TemplateWord(MemoryStream);
          _template = null;

          return _template;

        // LTC 20-JUN-2016
        case TYPE_TEMPLATE.EXCEL:

          _template = new TemplateExcel(MemoryStream);
         
          return _template;

        default:

          return null;

      }

    } // Build        

    //------------------------------------------------------------------------------
    // PURPOSE: Save the memory stream into a file
    //
    //  PARAMS:
    //      - INPUT:
    //          - MemoryStreamFile: The memory stream object
    //          - FileName: Path of the target file
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static void Save(Stream StreamFile, String FileName)
    {

      FileStream _output;
      Byte[] _buffer;
      Int32 _bytes_read;

      _buffer = new Byte[8192];
      _output = File.OpenWrite(FileName);

      StreamFile.Position = 0;

      while ((_bytes_read = StreamFile.Read(_buffer, 0, _buffer.Length)) > 0)
      {
        _output.Write(_buffer, 0, _bytes_read);
      }

      _output.Flush();
      _output.Close();

    }

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Set the template stream
    //
    //  PARAMS:
    //      - INPUT:
    //          - TemplateStream: Input a stream
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    protected void SetMemoryStreamTemplate(Stream TemplateStream)
    {

      m_stream_template = TemplateStream;

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set the file name of template 
    //
    //  PARAMS:
    //      - INPUT:
    //          - TemplateFileName: 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    protected void SetFileNameTemplate(String TemplateFileName)
    {

      m_path_template = TemplateFileName;

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Copy the generic stream into a MemoryStream and return this
    //
    //  PARAMS:
    //      - INPUT:
    //          - Input: Input a stream
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      MemoryStream: The copy of stream
    // 
    //   NOTES:
    // 
    public static MemoryStream CopyToMemory(Stream Input)
    {
      MemoryStream _stream;
      Byte[] _buffer;
      Int32 _bytes_read;

      _buffer = new Byte[8192];
      _stream = new MemoryStream();

      Input.Position = 0;

      while ((_bytes_read = Input.Read(_buffer, 0, _buffer.Length)) > 0)
      {
        _stream.Write(_buffer, 0, _bytes_read);
      }

      _stream.Position = 0;

      return _stream;
    }

    #endregion

  } // Template
} // Wrapper
