//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: DrawBingoFormat.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Raul Cervera
// 
// CREATION DATE: 21-DEC-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-DEC-2011 RCI    First release.
// 
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common
{
  public static class DrawBingoFormat
  {
    private static Boolean m_is_random = true;
    private static Boolean m_first_time = true;
    private static Random m_random;

    public static String NumberToBingo(Int64 Number, Int64 MaxNumber)
    {
      String _str_number;
      Char[] _chars;
      Char[] _chars_f1;
      Char[] _chars_f2;
      String _bingo;
      Int32 _fake_number;
      String _format;
      Int32 _max_random_number;

      MaxNumber = Math.Min(MaxNumber, 999999);
      if (Number > MaxNumber)
      {
        return "";
      }

      if (m_first_time)
      {
        m_first_time = false;
        m_random = new Random(Misc.GetTickCount());
      }

      // MaxNumber is used to define the digits to format the numbers.
      if (MaxNumber > 99999)
      {
        _format = "000000";
        _max_random_number = 1000000;
      }
      else
      {
        _format = "00000";
        _max_random_number = 100000;
      }

      if (m_is_random)
      {
        _fake_number = m_random.Next(_max_random_number);
        _str_number = _fake_number.ToString(_format);
        _chars_f1 = _str_number.ToCharArray();

        _fake_number = m_random.Next(_max_random_number);
        _str_number = _fake_number.ToString(_format);
        _chars_f2 = _str_number.ToCharArray();
      }
      else
      {
        _chars_f1 = _format.ToCharArray();
        _chars_f2 = _format.ToCharArray();
      }

      _str_number = Number.ToString(_format);
      _chars = _str_number.ToCharArray();

      _bingo = "<table style = 'border-collapse: collapse;border: none;font-size: 150%' cellpadding='0' cellspacing='0'>" +
               "  <tr>" +
               "    <td style='padding-left: 10px; padding-right: 10px; border: solid 1px white'><p style='font-size: 80%'>A</p></td>" +
               "    <td class='padded'>" + _chars[0] + "</td>" +
               "    <td class='padded'>" + _chars[1] + "</td>" +
               "    <td class='padded'>" + _chars[2] + "</td>" +
               "    <td class='padded'>" + _chars[3] + "</td>" +
               "    <td class='padded'>" + _chars[4] + "</td>" +
               (_chars.Length < 6 ? "" : "    <td class='padded'>" + _chars[5] + "</td>") +
               "  </tr>" +
               "  <tr>" +
               "    <td style='padding-left: 10px; padding-right: 10px; border: solid 1px white'><p style='font-size: 80%'>B</p></td>" +
               "    <td class='padded'>" + _chars_f1[0] + "</td>" +
               "    <td class='padded'>" + _chars_f1[1] + "</td>" +
               "    <td class='padded'>" + _chars_f1[2] + "</td>" +
               "    <td class='padded'>" + _chars_f1[3] + "</td>" +
               "    <td class='padded'>" + _chars_f1[4] + "</td>" +
               (_chars.Length < 6 ? "" : "    <td class='padded'>" + _chars_f1[5] + "</td>") +
               "  </tr>" +
               "  <tr>" +
               "    <td style='padding-left: 10px; padding-right: 10px; border: solid 1px white'><p style='font-size: 80%'>C</p></td>" +
               "    <td class='padded'>" + _chars_f2[0] + "</td>" +
               "    <td class='padded'>" + _chars_f2[1] + "</td>" +
               "    <td class='padded'>" + _chars_f2[2] + "</td>" +
               "    <td class='padded'>" + _chars_f2[3] + "</td>" +
               "    <td class='padded'>" + _chars_f2[4] + "</td>" +
               (_chars.Length < 6 ? "" : "    <td class='padded'>" + _chars_f2[5] + "</td>") +
               "  </tr>" +
               "</table>";

      return _bingo;
    } // NumberToBingo

  } // DrawBingoFormat

} // WSI.Common
