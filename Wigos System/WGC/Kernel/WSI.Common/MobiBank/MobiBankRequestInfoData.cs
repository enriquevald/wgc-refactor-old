﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MobiBankRechargeInfoData.cs
// 
//   DESCRIPTION: Class to hold data form Mobile bank request
// 
//        AUTHOR: Francis Gretz
// 
// CREATION DATE: 23-FEB-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-FEB-2018 PDM    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.MobiBank
{
  public class MobiBankRequestInfoData
  {
    #region "Properties"

    public Int64 RequestId { get; set; }

    public Int32 TerminalId { get; set; }

    public Int64 AccountId { get; set; }

    public MobileBankStatusRequestRechare StatusId { get; set; }

    public Decimal? Amount { get; set; }

    public DateTime? TimeRequest { get; set; }

    #endregion

  }
}
