﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MobiBankRecharge.cs
// 
//   DESCRIPTION: MobiBankRecharge class
// 
//        AUTHOR: Fernando Jiménez
// 
// CREATION DATE: 28-NOV-2017
// 
// REVISION HISTORY:
// 
// Date        Author           Description
// ----------- ------           ----------------------------------------------------------
// 28-NOV-2017 FJC              First version.
//----------------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common.MobiBank
{
  public class MobiBankRecharge
  {
    /// <summary>
    /// Inserts a new Request of MobiBank
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="AccountId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DB_MobiBankInsertRecharge(Int32 TerminalId, Int64 AccountId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Terminal.TerminalInfo _terminal_info;

      _sb = new StringBuilder();
      _terminal_info = new Terminal.TerminalInfo();

      try
      {

        if (!Terminal.Trx_GetTerminalInfo(TerminalId, out _terminal_info, SqlTrx))
        {
          Log.Error(string.Format("DB_MobiBankInsertRecharge() ==> Trx_GetTerminalInfo() failed: TerminalId: {0}; AccountId: {1} ", TerminalId, AccountId));

          return false;
        }

        _sb.AppendLine(" INSERT INTO   MOBIBANK_REQUEST         ");
        _sb.AppendLine("             ( MBR_TERMINAL_ID          ");
        _sb.AppendLine("             , MBR_ACCOUNT_ID           ");
        _sb.AppendLine("             , MBR_TIME_REQUEST         ");
        _sb.AppendLine("             , MBR_STATUS_ID            ");
        _sb.AppendLine("             , MBR_BANK_ID              ");
        _sb.AppendLine("             , MBR_AREA_ID              ");
        _sb.AppendLine("             , MBR_ELAPSED_TIME_REQUEST ");
        _sb.AppendLine("             )                          ");
        _sb.AppendLine("      VALUES (  @pTerminalId            ");
        _sb.AppendLine("              , @pAccountId             ");
        _sb.AppendLine("              , GETDATE()               ");
        _sb.AppendLine("              , @pStatus                ");
        _sb.AppendLine("              , @pBankId                ");
        _sb.AppendLine("              , @pBankAreaId            ");
        _sb.AppendLine("              , GETDATE()               ");
        _sb.AppendLine("             )                          ");

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_command.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_command.Parameters.Add("@pStatus", SqlDbType.Int).Value = MobileBankStatusRequestRechare.New;
          _sql_command.Parameters.Add("@pBankId", SqlDbType.Int).Value = _terminal_info.BankId;
          _sql_command.Parameters.Add("@pBankAreaId", SqlDbType.Int).Value = _terminal_info.BankAreaId;


          if (_sql_command.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_MobiBankInsertRecharge


    /// <summary>
    /// Get mobile bank request info
    /// </summary>
    /// <param name="RequestId"></param>
    /// <param name="MBRequestInfoData"></param>
    /// <returns></returns>
    public Boolean DB_GetMobiBankRequestInfo(Int64 RequestId, out MobiBankRequestInfoData MBRequestInfoData)
    {

      MBRequestInfoData = new MobiBankRequestInfoData();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {

          using (SqlCommand _sql_cmd = new SqlCommand(GetMobileBankRequestInfoDataQuery(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pId", SqlDbType.BigInt).Value = RequestId;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                return false;
              }

              MBRequestInfoData.RequestId = (_reader[0] == DBNull.Value) ? 0 : Convert.ToInt64(_reader[0]);
              MBRequestInfoData.TerminalId = (_reader[1] == DBNull.Value) ? 0 : Convert.ToInt32(_reader[1]);
              MBRequestInfoData.AccountId = (_reader[2] == DBNull.Value) ? 0 : Convert.ToInt64(_reader[2]);
              MBRequestInfoData.TimeRequest = (_reader[3] == DBNull.Value) ? DateTime.Now : Convert.ToDateTime(_reader[3]);
              MBRequestInfoData.StatusId = (MobileBankStatusRequestRechare)((_reader[4] == DBNull.Value) ? 0 : Convert.ToInt32(_reader[4]));
              MBRequestInfoData.Amount = (_reader[5] == DBNull.Value) ? 0 : Convert.ToDecimal(_reader[5]);

            }


            return true;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }//DB_GetMobiBankRequestInfo

    /// <summary>
    /// Update the Status of Requests of MobiBank
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="AccountId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DB_MobiBankCancelRecharges(Int64 AccountId, MobileBankStatusCancelReason CancelReason, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" UPDATE   MOBIBANK_REQUEST                                      ");
        _sb.AppendLine("    SET   MBR_STATUS_ID   =   @pNewStatus                       ");
        _sb.AppendLine("        , MBR_REASON      =   @pReason                          ");
        _sb.AppendLine("   WHERE  MBR_ACCOUNT_ID  =   @pAccountId                       ");
        _sb.AppendLine("     AND  MBR_STATUS_ID   IN  (@pStatusWhere1, @pStatusWhere2)  ");

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_command.Parameters.Add("@pReason", SqlDbType.Int).Value = CancelReason;
          _sql_command.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = MobileBankStatusRequestRechare.Void;
          _sql_command.Parameters.Add("@pStatusWhere1", SqlDbType.Int).Value = MobileBankStatusRequestRechare.New;
          _sql_command.Parameters.Add("@pStatusWhere2", SqlDbType.Int).Value = MobileBankStatusRequestRechare.InProgress;


          _sql_command.ExecuteNonQuery();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_MobiBankUpdateRecharges


    /// <summary>
    /// Update the Status of Requests of MobiBank
    /// </summary>
    /// <param name="RequestId">ID Request to be updated</param>
    /// <param name="Status">New Status</param>
    /// <param name="SqlTrx">Open Transaction</param>
    /// <returns></returns>
    public Boolean DB_MobiBankUpdateStatus(Int64 RequestId, MobileBankStatusRequestRechare Status, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" UPDATE   MOBIBANK_REQUEST                                      ");
        _sb.AppendLine("    SET   MBR_STATUS_ID   =   @pNewStatus                       ");
        _sb.AppendLine("   WHERE  MBR_ID  =   @pId                       ");

        using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_command.Parameters.Add("@pId", SqlDbType.BigInt).Value = RequestId;
          _sql_command.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = Status;
          _sql_command.ExecuteNonQuery();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_MobiBankUpdateStatus

    /// <summary>
    /// Insert WCP_MsgMobiBankRechargeNotification command in WCP and update the status in the request
    /// </summary>
    /// <param name="RequestId"></param>
    /// <param name="Status"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean NotifyRechargeAndUpdateRequestStatus(Int64 RequestId, MobileBankRechargeintoEGMStatus Status, SqlTransaction SqlTrx)
    {

      MobiBankRequestInfoData MBRequestInfoData;
      Boolean result = false;
      var Parameter = new StringBuilder();
      string amount = "0";

      try
      {

        if (!DB_GetMobiBankRequestInfo(RequestId, out MBRequestInfoData))
        {
          Log.Error("NotifyRechargeAndUpdateRequestStatus. Can´t get request info.");
          return false;
        }

        Parameter.Append("<WCP_MsgMobiBankRechargeNotification>");
        Parameter.Append("<AccountId>{0}</AccountId>");
        Parameter.Append("<Parameters>");
        Parameter.Append("<TransferType>{1}</TransferType>");
        Parameter.Append("<TransferAmount>{2}</TransferAmount>");
        Parameter.Append("</Parameters>");
        Parameter.Append("</WCP_MsgMobiBankRechargeNotification>");

        if (MBRequestInfoData.Amount.HasValue)
        {
          var amountX100 = Decimal.ToInt32(MBRequestInfoData.Amount.Value * 100);
          amount = amountX100.ToString();
        }

        string xml = String.Format(Parameter.ToString(), MBRequestInfoData.AccountId.ToString(), (int)Status, amount);

        result = WcpCommands.InsertWcpCommand(MBRequestInfoData.TerminalId, WCP_CommandCode.MobiBankRechargeNotification, xml, SqlTrx);

        if (result)
        {
          if (Status == MobileBankRechargeintoEGMStatus.RechargeInEgmOK)
          {
            DB_MobiBankUpdateStatus(RequestId, MobileBankStatusRequestRechare.FinishedInEgm, SqlTrx);
          }
        }
        else
        {
          Log.Error("NotifyRechargeAndUpdateRequestStatus. Can´t insert WCP_MsgMobiBankRechargeNotification command in WCP.");
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        return false;
      }

      return result;

    }


    /// <summary>
    /// Query to Get mobile bank request info from DB
    /// </summary>
    /// <returns>Query String</returns>
    private String GetMobileBankRequestInfoDataQuery()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("          SELECT   MBR_ID                 ");
      _sb.AppendLine("                 , MBR_TERMINAL_ID        ");
      _sb.AppendLine("                 , MBR_ACCOUNT_ID         ");
      _sb.AppendLine("                 , MBR_TIME_REQUEST       ");
      _sb.AppendLine("                 , MBR_STATUS_ID          ");
      _sb.AppendLine("                 , MBR_AMOUNT             ");
      _sb.AppendLine("            FROM   MOBIBANK_REQUEST       ");
      _sb.AppendLine("           WHERE   MBR_ID  =  @pId        ");

      return _sb.ToString();
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Send WCP command to notify the request cancelation
    // 
    //  PARAMS:
    //      - INPUT:
    //        - terminal Id
    //        - account Id
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - true: if the oparations was success
    //    - false: if the oparations was error
    public static bool InsertWCPCommand_CancelRequest(int terminalId, long accountId)
    {

      Boolean result = false;
      var XML = new StringBuilder();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {

          result = InsertWCPCommand_CancelRequest(terminalId, accountId, _db_trx.SqlTransaction);

          if (result)
          {
            _db_trx.Commit();
          }
          else
          {
            Log.Warning("WigosMobileBankService.InsertWCPCommand_CancelRequest: Can't cancel request. TerminalId:" + terminalId.ToString());
          }

        }

        return result;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("WigosMobileBankService.InsertWCPCommand_CancelRequest: Error to send WCP Command (Cancel request).");
        return result;
      }
    }//InsertWCPCommand_CancelRequest

    //------------------------------------------------------------------------------
    // PURPOSE: Send WCP command to notify the request cancelation
    // 
    //  PARAMS:
    //      - INPUT:
    //        - terminal Id
    //        - account Id
    //        - transaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - true: if the oparations was success
    //    - false: if the oparations was error
    public static bool InsertWCPCommand_CancelRequest(int terminalId, long accountId, SqlTransaction Trx)
    {

      Boolean result = false;
      var XML = new StringBuilder();

      try
      {

        XML.Append("<WCP_MsgMobiBankCancelRequest>");
        XML.Append("<AccountId>" + accountId.ToString() + "</AccountId>");
        XML.Append("<Parameters>");
        XML.Append("<CancelReason>" + ((int)MobileBankStatusCancelReason.CancelledByRunner).ToString() + "</CancelReason>");
        XML.Append("</Parameters>");
        XML.Append("</WCP_MsgMobiBankCancelRequest>");

        result = WcpCommands.InsertWcpCommand(terminalId, WCP_CommandCode.MobiBankCancelRequest, XML.ToString(), Trx);

        return result;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Warning("WigosMobileBankService.InsertWCPCommand_CancelRequest: Error to send WCP Command (Cancel request).");
        return result;
      }
    }//InsertWCPCommand_CancelRequest

  }
}
