//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TerminalSignatureValidation.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: DDM
// 
// CREATION DATE: 03-APR-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-APR-2014 DDM     First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common
{
  public static class TerminalSignatureValidation
  {

    //------------------------------------------------------------------------------
    // PURPOSE: Get signature validation
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int32: TerminalId
    //
    //      - OUTPUT:
    //        - ENUM_AUTHENTICATION_METHOD: AuthenticationMethod
    //        - String: Seed
    //        - String: Signature
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean GetSignatureValidation(Int32 TerminalId, out ENUM_AUTHENTICATION_METHOD AuthenticationMethod, out String Seed, out String Signature)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetSignatureValidation(TerminalId, out AuthenticationMethod, out Seed, out Signature, _db_trx.SqlTransaction);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get signature validation
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int32: TerminalId
    //        - SqlTransaction: SqlTrx
    //
    //      - OUTPUT:
    //        - ENUM_AUTHENTICATION_METHOD: AuthenticationMethod
    //        - String: Seed
    //        - String: Signature
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean GetSignatureValidation(Int32 TerminalId, out ENUM_AUTHENTICATION_METHOD AuthenticationMethod, out String Seed, out String Signature, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      AuthenticationMethod = ENUM_AUTHENTICATION_METHOD.NONE;
      Seed = "";
      Signature = "";

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   TE_AUTHENTICATION_METHOD ");
        _sb.AppendLine("        , TE_AUTHENTICATION_SEED  ");
        _sb.AppendLine("        , TE_AUTHENTICATION_SIGNATURE  ");
        _sb.AppendLine("   FROM   TERMINALS ");
        _sb.AppendLine("  WHERE   TE_TERMINAL_ID = @pTerminalId");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              AuthenticationMethod = _reader.IsDBNull(0) ? ENUM_AUTHENTICATION_METHOD.NONE : (ENUM_AUTHENTICATION_METHOD)_reader.GetInt32(0);
              Seed = _reader.IsDBNull(1) ? "" : _reader.GetString(1);
              Signature = _reader.IsDBNull(2) ? "" : _reader.GetString(2);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("GetSignatureValidation. Exception thrown -> " + _ex.Message);
      }

      return false;

    } // GetSignatureValidation

    //------------------------------------------------------------------------------
    // PURPOSE: Get Authentication & Machine status
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int32: TerminalId
    //
    //      - OUTPUT:
    //        - ENUM_AUTHENTICATION_STATUS: AuthenticationStatus
    //        - Int32: MachineStatus
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean GetAuthenticationMachineStatus(Int32 TerminalId, out ENUM_AUTHENTICATION_STATUS AuthenticationStatus, out Int32 MachineStatus)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetAuthenticationMachineStatus(TerminalId, out AuthenticationStatus, out MachineStatus, _db_trx.SqlTransaction);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Authentication & Machine status
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int32: TerminalId
    //        - SqlTransaction: SqlTrx
    //
    //      - OUTPUT:
    //        - ENUM_AUTHENTICATION_STATUS: AuthenticationStatus
    //        - Int32: MachineStatus
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean GetAuthenticationMachineStatus(Int32 TerminalId, out ENUM_AUTHENTICATION_STATUS AuthenticationStatus, out Int32 MachineStatus, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      AuthenticationStatus = ENUM_AUTHENTICATION_STATUS.UNKNOWN;
      MachineStatus = 0;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   TE_AUTHENTICATION_STATUS ");
        _sb.AppendLine("        , TE_MACHINE_STATUS ");
        _sb.AppendLine("   FROM   TERMINALS ");
        _sb.AppendLine("  WHERE   TE_TERMINAL_ID = @pTerminalId");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              AuthenticationStatus = _reader.IsDBNull(0) ? ENUM_AUTHENTICATION_STATUS.UNKNOWN : (ENUM_AUTHENTICATION_STATUS)_reader.GetInt32(0);
              MachineStatus = _reader.GetInt32(1);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(" GetAuthenticationStatus. Exception thrown -> " + _ex.Message);
      }

      return false;
    } // GetAuthenticationStatus

    //------------------------------------------------------------------------------
    // PURPOSE: Set Authentication status
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int32: TerminalId
    //        - ENUM_AUTHENTICATION_STATUS: AuthenticationStatus
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean SetAuthenticationStatus(Int32 TerminalId, ENUM_AUTHENTICATION_STATUS AuthenticationStatus)
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (SetAuthenticationStatus(TerminalId, AuthenticationStatus, _db_trx.SqlTransaction))
          {
            _db_trx.Commit();
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("SetAuthenticationStatus. Exception thrown -> " + _ex.Message);
      }
      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set Authentication status
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int32: TerminalId
    //        - ENUM_AUTHENTICATION_STATUS: AuthenticationStatus
    //        - SqlTransaction: SqlTrx
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean SetAuthenticationStatus(Int32 TerminalId, ENUM_AUTHENTICATION_STATUS AuthenticationStatus, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   TERMINALS ");
        _sb.AppendLine("    SET   TE_AUTHENTICATION_STATUS = @pAuthenticationStatus ");
        _sb.AppendLine("  WHERE   TE_TERMINAL_ID = @pTerminalId");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pAuthenticationStatus", SqlDbType.Int).Value = AuthenticationStatus;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("SetAuthenticationStatus. TerminalId: " + TerminalId);

            return false;
          }

          SqlTrx.Commit();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error("SetAuthenticationStatus. Exception thrown -> " + _ex.Message);
      }

      return false;
    } // SetAuthenticationStatus

    //------------------------------------------------------------------------------
    // PURPOSE: Set Machine status
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int32: TerminalId
    //        - Int32: MachineStatus
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean SetMachineStatus(Int32 TerminalId, Int32 MachineStatus)
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (SetMachineStatus(TerminalId, MachineStatus, _db_trx.SqlTransaction))
          {
            _db_trx.Commit();
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("SetMachineStatus. Exception thrown -> " + _ex.Message);
      }
      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set Machine status
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int32: TerminalId
    //        - Int32: MachineStatus
    //        - SqlTransaction: SqlTrx
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean SetMachineStatus(Int32 TerminalId, Int32 MachineStatus, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   TERMINALS ");
        _sb.AppendLine("    SET   TE_MACHINE_STATUS = @pMachineStatus ");
        _sb.AppendLine("  WHERE   TE_TERMINAL_ID = @pTerminalId");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pMachineStatus", SqlDbType.Int).Value = MachineStatus;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("SetMachineStatus. TerminalId: " + TerminalId);

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error("SetMachineStatus. Exception thrown -> " + _ex.Message);
      }

      return false;
    } // SetMachineStatus
  }
}
