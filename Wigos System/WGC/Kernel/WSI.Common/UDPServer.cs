//------------------------------------------------------------------------------
// Copyright (c) 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: UDPServer.cs
// 
//   DESCRIPTION: Abstract UDP Server
// 
//        AUTHOR: Andreu Julia
// 
// CREATION DATE: 12-SEP-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-SEP-2008 AJQ    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Net.NetworkInformation;

namespace WSI.Common
{
  /// <summary>
  /// Abstract UDP Server
  /// </summary>
  public abstract class UdpServer
  {
    #region CONSTANTS
    // ------------------------------------------------------------------------- CONSTANTS
    #endregion // CONSTANTS

    #region MEMBERS
    // ------------------------------------------------------------------------- MEMBERS
    private Encoding m_encoding = Encoding.UTF8;
    private int m_udp_port = 0;
    private Thread m_thread = null;


    private ManualResetEvent m_ev_stop = new ManualResetEvent(false);
    private ManualResetEvent m_ev_continue = new ManualResetEvent(true);


    private UdpClient m_udp_client = null;
    private IAsyncResult m_iar_recv = null;

    #endregion // MEMBERS

    #region PUBLIC METHODS
    // ------------------------------------------------------------------------- PUBLIC METHODS


    private void Sleep(int Timeout)
    {
      m_ev_stop.WaitOne(Timeout);
    }

    private Boolean WaitForIncomingData(IAsyncResult IAR)
    {
      WaitHandle[] _handles;

      _handles = new WaitHandle[2];
      _handles[0] = m_ev_stop;
      _handles[1] = IAR.AsyncWaitHandle;

      return (WaitHandle.WaitAny(_handles) == 1);
    }
    private Boolean WaitForContinue()
    {
      WaitHandle[] _handles;

      _handles = new WaitHandle[2];
      _handles[0] = m_ev_stop;
      _handles[1] = m_ev_continue;
      return (WaitHandle.WaitAny(_handles) == 1);
    }



    protected Boolean StopRequested
    {
      get
      {
        return m_ev_stop.WaitOne(0);
      }
    }
    protected Boolean Paused
    {
      get
      {
        return !m_ev_continue.WaitOne(0);
      }
    }


    public void Pause()
    {
      m_ev_continue.Reset();
    }


    public void Continue()
    {
      m_ev_continue.Set();
    }

    public void Start()
    {
      m_thread.Start();
    }

    public void Stop()
    {
      // Request pause ...
      Pause();
      // Signal 'Stop event'
      m_ev_stop.Set();

    } // Stop






    /// <summary>
    /// Creates a new instance of a UPD server that will be listening on the given port number.
    /// </summary>
    /// <param name="PortNumber">Port Number where to listen</param>
    public UdpServer(int PortNumber)
    {
      m_udp_port = PortNumber;
      m_thread = new Thread(new ThreadStart(this.UDP_ListenerThread));

    } // UdpServer

    /// <summary>
    /// Starts a thread that listen the UDP information.
    /// </summary>
    private void UDP_ListenerThread()
    {
      int _wait_hint;
      int _tick_log;

      _wait_hint = 0;
      _tick_log = Misc.GetTickCount();

      // Create Listener ...
      while (!StopRequested)
      {
        Sleep(_wait_hint);
        _wait_hint = 30000;

        try
        {
          m_udp_client = new UdpClient(m_udp_port);
          m_udp_client.EnableBroadcast = true;
          m_iar_recv = null;
          //
          // Receive loop ...
          //
          while (!StopRequested)
          {
            m_iar_recv = m_udp_client.BeginReceive(new AsyncCallback(Dummy), null);
            if (WaitForIncomingData(m_iar_recv))
            {
              ProcessIncomingData(m_iar_recv);
            }
          }
        }
        catch (SocketException _se)
        {
          if (_se.ErrorCode == 10048)
          {
            Log.Message("UDP Port " + m_udp_port + " already in use.");
            Log.Close();

            Environment.Exit(0);
          }
          else
          {
            // Log.Exception(_se);
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
        finally
        {
          if (m_udp_client != null)
          {
            try { m_udp_client.Close(); }
            catch { };
          }
        }
      } // while (!StopRequested )
    }

    private void Dummy(IAsyncResult IAR)
    {
    }

    private void ProcessIncomingData(IAsyncResult IAR)
    {
      IAsyncResult _iar;
      _iar = IAR;
      m_iar_recv = null;
      if (_iar != null)
      {
        DataReceivedCallback(_iar);
      }
    }


    /// <summary>
    /// Sends a string to an IP and port.
    /// </summary>
    /// <param name="TargetIPEndPoint">The target IP endpoint.</param>
    /// <param name="DataToSend">The data to send.</param>
    public void Send(IPEndPoint TargetIPEndPoint, String DataToSend)
    {
      byte[] datagram;  // Datagram to send

      if (TargetIPEndPoint == null)
      {
        return;
      }
      if (String.IsNullOrEmpty(DataToSend))
      {
        return;
      }

      try
      {
        // Create the datagram: convert the string to a byte array
        datagram = this.Encoding.GetBytes(DataToSend);
        if (datagram.Length == 0)
        {
          return;
        }
        // Write data to the network
        if (m_udp_client != null)
        {
          m_udp_client.Send(datagram, datagram.Length, TargetIPEndPoint);
        }
      }
      catch (SocketException ex)
      {
        switch (ex.ErrorCode)
        {
          case 10065: // WSAEHOSTUNREACH
            // Ignore error
            break;

          default:
            Log.Exception(ex);
            Log.Warning("Send failed. WSAError=" + ex.ErrorCode.ToString() + " TargetIP=" + TargetIPEndPoint.ToString());
            break;
        } // switch ( ex.ErrorCode )
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Send failed. TargetIP=" + TargetIPEndPoint.ToString());
      }
    } // Send

    #endregion // PUBLIC METHODS

    #region PRIVATE METHODS
    // ------------------------------------------------------------------------- PRIVATE METHODS

    /// <summary>
    /// Callback, called when data is received
    /// </summary>
    /// <param name="AsyncResult"></param>
    private void DataReceivedCallback(IAsyncResult AsyncResult)
    {
      IPEndPoint _remote_ep;
      Byte[] _udp_packet;
      IPAddress _ip_filter;
      IPAddress _ip_remote;
      Boolean _filter;

      _remote_ep = null;
      _udp_packet = null;
      _ip_filter = IPAddress.Parse("255.255.255.255");

      try
      {
        _udp_packet = m_udp_client.EndReceive(AsyncResult, ref _remote_ep);
        if (Paused)
        {
          return;
        }
        if (_udp_packet == null)
        {
          return;
        }
        if (_udp_packet.Length <= 0)
        {
          return;
        }
        if (_remote_ep == null)
        {
          return;
        }
        // AJQ 16-OCT-2008, Ignore messages comming from Loopback IP address: 
        //   This happens when the terminal has no IP and sends a message indicating that its IP is the Loopback one.
        //   The service must avoid sending a message to its Loopback!!
        _ip_remote = _remote_ep.Address;
        if (IPAddress.IsLoopback(_ip_remote))
        {
          return;
        }

        _filter = false;
        try
        {
          string str_ip_filter;

          str_ip_filter = null;
          str_ip_filter = Environment.GetEnvironmentVariable("DIRECTORY_SERVICE_IP_FILTER");
          if (str_ip_filter != null)
          {
            _ip_filter = IPAddress.Parse(str_ip_filter);
            _filter = true;
          }
        }
        catch (Exception)
        {
          // Do nothing: variable not defined
        }

        if (_filter)
        {
          byte[] bytes_ip_remote;
          byte[] bytes_ip_filter;
          IPAddress ip_filtered;

          bytes_ip_remote = _ip_remote.GetAddressBytes();
          bytes_ip_filter = _ip_filter.GetAddressBytes();

          for (int idx = 0; idx < bytes_ip_remote.Length; idx++)
          {
            bytes_ip_filter[idx] = (byte)(bytes_ip_filter[idx] & bytes_ip_remote[idx]);
          }
          ip_filtered = new IPAddress(bytes_ip_filter);
          if (!_ip_remote.Equals(ip_filtered))
          {
            return;
          }
        }
        // Process data received
        ProcessDataReceived(_remote_ep, _udp_packet);
      }
      catch
      {
      }
      finally
      {
      }
    } // DataReceivedCallback

    #endregion // PRIVATE METHODS

    #region PROTECTED METHODS
    // ------------------------------------------------------------------------- PROTECTED METHODS

    /// <summary>
    /// Sets or gets the encoding for the string messages
    /// </summary>
    protected Encoding Encoding
    {
      get { return m_encoding; }
      set { m_encoding = value; }
    } // Encoding

    #endregion // PROTECTED METHODS

    /// <summary>
    /// Process the received data
    /// </summary>
    /// <param name="RemoteIPEndPoint">Source IP of the received datagram</param>
    /// <param name="ReceivedData">The received datagram</param>
    protected abstract void ProcessDataReceived(IPEndPoint RemoteIPEndPoint, Byte[] ReceivedData);

  } // class UdpServer

  public abstract class UDP_QueryService
  {
    protected abstract int RequestToPortNumber();
    protected abstract String ProtocolName();
    protected abstract int ServicePortNumber();

    private void DataReceivedCallback(IAsyncResult AsyncResult)
    {
      // Do nothing

    } // DataReceivedCallback

    protected abstract Byte[] GetRequestDatagram(IPEndPoint UdpListener);
    protected abstract IPEndPoint GetResponseIPEndPoint(Byte[] Response);



    private IPEndPoint GetServiceIPEndPointFromDns()
    {
      IPInterfaceProperties _ip_properties;
      Random _rng;
      int _idx_ip;
      String _str_ip;
      IPAddress _dns_ip;
      List<IPAddress> _ip_list;

      try
      {
        try
        {
          _str_ip = Environment.GetEnvironmentVariable("LKT_" + ProtocolName() + "_SERVER_IP");
          if (String.IsNullOrEmpty(_str_ip))
          {
            _str_ip = Environment.GetEnvironmentVariable("LKT_REMOTE_SERVER_IP");
          }

          if (!String.IsNullOrEmpty(_str_ip))
          {
            try
            {
              // We have an environment variable that indicates where we have to connect ...
              IPAddress[] _dns_ip_list;

              _dns_ip_list = Dns.GetHostAddresses(_str_ip);

              if (_dns_ip_list.Length > 0)
              {
                _rng = new Random(Environment.TickCount);
                _idx_ip = _rng.Next(0, _dns_ip_list.Length);

                for (int _test = 0; _test < _dns_ip_list.Length; _test++)
                {
                  _dns_ip = _dns_ip_list[_idx_ip];
                  if (_dns_ip.AddressFamily == AddressFamily.InterNetwork)
                  {
                    return new IPEndPoint(_dns_ip, ServicePortNumber());
                  }
                  _idx_ip = (_idx_ip + 1) % _dns_ip_list.Length;
                }
              }
            }
            catch
            { }

            return null;
          }
        }
        catch
        { }


        _ip_list = new List<IPAddress>();
        int _num_up;

        do
        {
          _num_up = 0;
          foreach (NetworkInterface _net_interface in NetworkInterface.GetAllNetworkInterfaces())
          {
            if (_net_interface.NetworkInterfaceType == NetworkInterfaceType.Loopback)
            {
              continue;
            }
            if (_net_interface.OperationalStatus != OperationalStatus.Up)
            {
              continue;
            }

            _num_up++;

            _ip_properties = _net_interface.GetIPProperties();
            foreach (IPAddress _ip in _ip_properties.DnsAddresses)
            {
              if (_ip.AddressFamily != System.Net.Sockets.AddressFamily.InterNetwork) continue;

              _ip_list.Add(_ip);
            }
          }

          if (_num_up <= 0)
          {
            System.Threading.Thread.Sleep(1000);
          }

        } while (_num_up <= 0);

        if (_ip_list.Count == 0)
        {
          return null;
        }


        _rng = new Random(Environment.TickCount);

        _idx_ip = _rng.Next(0, _ip_list.Count);
        _dns_ip = _ip_list[_idx_ip];

        return new IPEndPoint(_dns_ip, ServicePortNumber());
      }
      catch
      {
      }

      return null;
    } // GetServiceIPEndPointFromDns

    public IPEndPoint QueryService(int RequestPeriod)
    {
      UdpClient _udp_client;
      IPEndPoint _ipe_remote;
      IPEndPoint _ipe_service;
      Byte[] _dgram_request;
      Byte[] _dgram_response;
      IPEndPoint _ipe_listener;
      IPAddress _ip_bcast;
      string _str_ip_bcast;
      int _idx_try;

      _ipe_service = null;
      _udp_client = null;

      try
      {
        try
        {
          _ipe_service = GetServiceIPEndPointFromDns();
          if (_ipe_service != null)
          {
            return _ipe_service;
          }
        }
        catch
        {
        }

        // Create the UDP client
        _udp_client = new UdpClient();
        _udp_client.EnableBroadcast = true;
        _udp_client.Client.ReceiveTimeout = RequestPeriod;
        _udp_client.Client.SendTimeout = 1000;

        // Listen on "Any" IP
        _udp_client.Client.Bind(new IPEndPoint(IPAddress.Any, 0));
        _ipe_listener = (IPEndPoint)_udp_client.Client.LocalEndPoint;

        // Broadcast IP
        _ip_bcast = IPAddress.Broadcast;
        try
        {
          _str_ip_bcast = null;
          _str_ip_bcast = Environment.GetEnvironmentVariable("LKT_BCAST_IP");
          if (_str_ip_bcast != null)
          {
            _ip_bcast = IPAddress.Parse(_str_ip_bcast);
          }
        }
        catch
        {
          _ip_bcast = IPAddress.Broadcast;
        }

        for (_idx_try = 0; _idx_try < 3; _idx_try++)
        {
          if (_idx_try > 0)
          {
            System.Threading.Thread.Sleep(1000 + 100 * _idx_try);
          }
          // Get the request datagram
          _dgram_request = this.GetRequestDatagram(_ipe_listener);
          // Send the request
          _udp_client.Send(_dgram_request, _dgram_request.Length, new IPEndPoint(_ip_bcast, RequestToPortNumber()));

          try
          {
            // Get the any incoming datagram
            _ipe_remote = null;
            _dgram_response = _udp_client.Receive(ref _ipe_remote);
            // Get the service IP address and port
            _ipe_service = GetResponseIPEndPoint(_dgram_response);
            // Return the received IP address and port
            return _ipe_service;
          }
          catch (SocketException _ex)
          {
            switch (_ex.ErrorCode)
            {
              case 10060: // Timeout
                continue;

              default:
                Log.Exception(_ex);
                System.Threading.Thread.Sleep(100);
                break;
            }
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
            System.Threading.Thread.Sleep(100);
          }
        } // for ( _idx_try )
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        System.Threading.Thread.Sleep(100);
      }
      finally
      {
        // Close the UDP connection
        if (_udp_client != null)
        {
          try
          {
            _udp_client.Client.Close();
          }
          catch
          { }
          try
          {
            _udp_client.Close();
          }
          catch
          { }
          _udp_client = null;
        }
      }

      return _ipe_service;
    } // QueryService
  }

} // namespace WSI.Common
