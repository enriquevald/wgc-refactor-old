//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GeneralParams.cs
// 
//   DESCRIPTION: Class to manage General Parameters
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 27-MAR-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-MAR-2012 AJQ    First release.
// 18-APR-2013 RCI    Added alias in the dictionary.
// 02-JUL-2013 RCI    Fixed Bug WIG-30: If system (Cashier, GUI, WCP, ...) is on, if only are new GPs (no changes in existing GPs), they are not detected.
// 17-OCT-2013 ANG    Add DefaultValue Parameter in GeneralParam.GetString method.
// 08-AUG-2014 AC & FJC Create function 'GetDecimalRange'
// 14-FEB-2015 DRV    Add BigIncrementsData class. Item 981
// 26-OCT-2015 JML & DDM Fixed Bug 5581: Only multisite center. (for cash sumary..)
// 14-OCT-2016 RAB    PBI 18093: Add update frequency at GP
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Threading;
using System.Diagnostics;
using System.Data;
using System.Runtime.CompilerServices;

namespace WSI.Common
{
  public static class Computer
  {
    public static String ShortAlias(String ComputerName)
    {
      String _value;

      if (String.IsNullOrEmpty(ComputerName))
      {
        return ComputerName;
      }

      // Alias only used for Cashier machines. The APP_NAME is fixed to CASHIER.
      _value = GeneralParam.Value(GeneralParam.Dictionary.ALIAS_PREFIX + "." + "CASHIER", ComputerName.ToUpper());
      if (String.IsNullOrEmpty(_value)
         || String.Compare(_value, ComputerName, StringComparison.InvariantCultureIgnoreCase) == 0)
      {
        _value = ComputerName;
      }

      return _value;
    } // ShortAlias

    public static String Alias(String ComputerName)
    {
      String _value;

      _value = ShortAlias(ComputerName);

      if (_value != ComputerName)
      {
        _value = _value + " [" + ComputerName + "]";
      }

      return _value;
    } // Alias
  }

  public static partial class GeneralParam
  {
    private const Int32 REFRESH_PERIOD = 2 * 60 * 1000; // 2 minutes
    private const Int32 ERROR_WAIT_HINT = 10 * 1000;    // 10 seconds
    private const Int32 NUM_COLUMNS = 3; // Group, Subject, Value
    private const Int32 MIN_REFRESH_PERIOD = 5 * 1000; // 5 seconds

    private static ManualResetEvent m_ev_data_available = new ManualResetEvent(false);
    private static Dictionary m_params = new Dictionary();
    private static ReaderWriterLock m_rw_lock = new ReaderWriterLock();
    private static Boolean m_init = GeneralParam.Init();
    private static Boolean m_current_process_LKT_Terminal = false;

    public enum COLUMN_NAME
    {
      Group = 0,
      Subject = 1,
      Value = 2
    }

    public static T Get<T>(String GroupKey, String SubjectKey, T DefaultValue)
    {
      try
      {
        var _type = typeof(T);
        if (_type == typeof(bool) || _type == typeof(Boolean))
        {
          return (T)(object)GetBoolean(GroupKey, SubjectKey, (bool)(object)DefaultValue);
        }
        if (_type == typeof(string) || _type == typeof(String))
        {
          return (T)(object)GetString(GroupKey, SubjectKey, (string)(object)DefaultValue);
        }
        if (_type == typeof(int) || _type == typeof(Int32))
        {
          return (T)(object)GetInt32(GroupKey, SubjectKey, (int)(object)DefaultValue);
        }
        if (_type == typeof(Int64) || _type == typeof(long))
        {
          return (T)(object)GetInt64(GroupKey, SubjectKey, (long)(object)DefaultValue);
        }
        if (_type == typeof(Decimal))
        {
          return (T)(object)GetDecimal(GroupKey, SubjectKey, (Decimal)(object)DefaultValue);
        }
        if (_type == typeof(Currency))
        {
          return (T)(object)GetCurrency(GroupKey, SubjectKey, (Currency)(object)DefaultValue);
        }
        if (_type == typeof(Currency))
        {
          return (T)(object)GetCurrency(GroupKey, SubjectKey, (Currency)(object)DefaultValue);
        }
      }
      catch
      {
        //swallow
      }
      return DefaultValue;
    }

    public static String[] AddToList(String GroupKey, String SubjectKey, String Value)
    {
      String[] _general_param_array = new String[NUM_COLUMNS];

      _general_param_array[(int)COLUMN_NAME.Group] = GroupKey;
      _general_param_array[(int)COLUMN_NAME.Subject] = SubjectKey;
      _general_param_array[(int)COLUMN_NAME.Value] = Value;

      return _general_param_array;
    }

    public partial class Dictionary : Dictionary<String, String>
    {
      // Make it public because WKT_Common wants to access it
      public Dictionary()
        : base()
      {
      }

      public static Dictionary Create()
      {
        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            return Create(_db_trx.SqlTransaction);
          }
        }
        catch
        {
          return null;
        }
      }
    }

    public static Dictionary GetDictionary()
    {
      Dictionary _dic;

      try
      {
        // Wait till the values have been retrieved from the DB
        m_ev_data_available.WaitOne();
        m_rw_lock.AcquireReaderLock(Timeout.Infinite);

        _dic = new Dictionary();
        foreach (String _key in m_params.Keys)
        {
          _dic.Add(_key, m_params[_key]);
        }
        return _dic;
      }
      finally
      {
        m_rw_lock.ReleaseReaderLock();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Creates the auto-refresh thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : This function is "automatically" called on the first call to GeneralParam
    //
    private static Boolean Init()
    {
      Thread _thread = null;

      // AJQ 16-APR-2012, Avoid waiting for the event in design mode (the DB is not available)
      if (!String.IsNullOrEmpty(Environment.GetEnvironmentVariable("LKS_VC_DEV")))
      {
        if (System.Diagnostics.Process.GetCurrentProcess().ProcessName == "devenv")
        {
          m_ev_data_available.Set();

          return true;
        }
      }

      // SGB 17-NOV-2015 Check if process from Promobox
      m_current_process_LKT_Terminal = (System.Diagnostics.Process.GetCurrentProcess().ProcessName == "LKT_Terminal");

      if (!m_current_process_LKT_Terminal)
      {
        _thread = new Thread(new ThreadStart(GeneralParamsThread));
        _thread.Name = "GeneralParamsThread";
        _thread.Start();
      }

      return true;
    } // Init

    public static Boolean GenerateGeneralParam(String GroupKey, String SubjectKey, String Value, SqlTransaction SqlTrans)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("IF EXISTS (SELECT  1                                ");
        _sb.AppendLine("             FROM  GENERAL_PARAMS                   ");
        _sb.AppendLine("            WHERE  GP_GROUP_KEY   = @pGKey          ");
        _sb.AppendLine("              AND  GP_SUBJECT_KEY = @pKey)          ");

        _sb.AppendLine("  UPDATE GENERAL_PARAMS                             ");
        _sb.AppendLine("     SET GP_KEY_VALUE    = @pValue                  ");
        _sb.AppendLine("   WHERE GP_GROUP_KEY    = @pGKey                   ");
        _sb.AppendLine("     AND GP_SUBJECT_KEY  = @pKey                    ");

        _sb.AppendLine("ELSE                                                ");
        
        _sb.AppendLine("  INSERT INTO GENERAL_PARAMS                        ");
        _sb.AppendLine("              (GP_GROUP_KEY                         ");
        _sb.AppendLine("             , GP_SUBJECT_KEY                       ");
        _sb.AppendLine("             , GP_KEY_VALUE)                        ");
        _sb.AppendLine("         VALUES                                     ");
        _sb.AppendLine("              (@pGKey                               ");
        _sb.AppendLine("             , @pKey                                ");
        _sb.AppendLine("             , @pValue)                             ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrans.Connection, SqlTrans))
        {
          _sql_cmd.Parameters.Add("@pGKey", SqlDbType.NVarChar).Value = GroupKey;
          _sql_cmd.Parameters.Add("@pKey", SqlDbType.NVarChar).Value = SubjectKey;
          _sql_cmd.Parameters.Add("@pValue", SqlDbType.NVarChar).Value = Value;

          return _sql_cmd.ExecuteNonQuery() == 1;
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        return false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    } //GenerateGeneralParam

    //------------------------------------------------------------------------------
    // PURPOSE : Automatic reload of the General Params
    //
    //  PARAMS :
    //      - INPUT :
    //          - GroupKey
    //          - SubjectKey
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //        The value of the selected General Param
    //
    //   NOTES :
    //
    private static void GeneralParamsThread()
    {
      Dictionary _new_params;
      Dictionary _new_params_copy;
      Boolean _changed;
      StringBuilder _sb;
      Int32 _wait_hint;

      _sb = new StringBuilder();
      _wait_hint = 0;

      while (true)
      {
        try
        {
          Thread.Sleep(_wait_hint);
          _wait_hint = REFRESH_PERIOD;

          if (!WGDB.Initialized || WGDB.ConnectionState != ConnectionState.Open)
          {
            _wait_hint = 500;

            continue;            
          }

          if (!LoadGeneralParams(_sb, out _wait_hint, out _new_params, out _new_params_copy, out _changed))
          {
            continue;
          }
          
          if (_changed)
          {
            try
            {
              m_rw_lock.AcquireWriterLock(Timeout.Infinite);

              m_params = _new_params;

              if (_sb.Length > 0)
              {
                Log.Warning(String.Format("General Params Changed!. Details:{0}", _sb.ToString()));
              }
            }
            finally
            {
              m_rw_lock.ReleaseWriterLock();
            }
          }

          m_ev_data_available.Set();

          //MultiSite	IsCenter	1
          if (!m_params.GetBoolean("MultiSite", "IsCenter", false))
          {
            if (!BigIncrementsData.LoadBigIncrementsData())
            {
              Log.Error("Error reading BigIncrementsData from DB. Used values from General Params");
            }
          }
          // 26-OCT-2015 JML & DDM Fixed Bug 5581: Only multisite center. (for cash sumary..)
          if (!OperationVoucherParams.LoadOperationVoucherParameters())
          {
            continue;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          _wait_hint = ERROR_WAIT_HINT;
        }
      }

    }

    private static Boolean LoadGeneralParams(StringBuilder _sb, out Int32 _wait_hint, out Dictionary _new_params, out Dictionary _new_params_copy, out Boolean _changed)
    {
      _new_params = Dictionary.Create();
      _new_params_copy = new Dictionary();
      _changed = false;

      if (_new_params == null)
      {
        _wait_hint = ERROR_WAIT_HINT;

        Log.Warning("Create GeneralParamDictionary failed!");

        return false;
      }

      if (_new_params.Count == 0)
      {
        _wait_hint = MIN_REFRESH_PERIOD;

        Log.Warning("Read GeneralParams.Count = 0");

        return false;
      }

      // Copy the new params to another dictionary. Used for detect really new params in the system.
      _new_params_copy = new Dictionary();
      foreach (String _key in _new_params.Keys)
      {
        _new_params_copy.Add(_key, _new_params[_key]);
      }

      _wait_hint = _new_params_copy.GetInt32("GeneralParams", "RefreshRateInMilliseconds", REFRESH_PERIOD);
      
      if(_wait_hint < MIN_REFRESH_PERIOD)
      {
        _wait_hint = MIN_REFRESH_PERIOD;
      }

      _changed = false;
      if (m_params.Count == 0)
      {
        _changed = true;
      }

      _sb.Length = 0;
      foreach (String _key in m_params.Keys)
      {
        if (_new_params_copy.ContainsKey(_key))
        {
          if (_new_params_copy[_key] != m_params[_key])
          {
            _changed = true;
            _sb.AppendLine();
            _sb.AppendFormat("- Key Changed: {0}, Value: {1}, Previous: {2}", _key, _new_params_copy[_key], m_params[_key]);
          }

          _new_params_copy.Remove(_key);
        }
        else
        {
          _changed = true;
          _sb.AppendLine();
          _sb.AppendFormat("- Key Deleted: {0}, Value: {1}", _key, m_params[_key]);
        }
      }

      if (m_params.Count > 0 && _new_params_copy.Count > 0)
      {
        _changed = true;
        foreach (String _key in _new_params_copy.Keys)
        {
          _sb.AppendLine();
          _sb.AppendFormat("- Key Added: {0}, Value: {1}", _key, _new_params_copy[_key]);
        }
      }
      return true;
    } // GeneralParamsRefreshThread


#if DEBUG

    public static void ReloadGP()
    {
      Dictionary _new_params;

      _new_params = Dictionary.Create();
      try
      {
        m_rw_lock.AcquireWriterLock(Timeout.Infinite);

        m_params = _new_params;
      }
      finally
      {
        m_rw_lock.ReleaseWriterLock();
      }
    }

#endif

    public static void ReloadGPFromPromobox(Dictionary DictionaryGP)
    {
      try
      {
        m_rw_lock.AcquireWriterLock(Timeout.Infinite);

        m_params = DictionaryGP;
      }
      finally
      {
        m_rw_lock.ReleaseWriterLock();
      }
    } // ReloadGPFromPromobox

    public static void SetDataAvaiable()
    {
      m_ev_data_available.Set();
    } // SetManualResetEvent

    //------------------------------------------------------------------------------
    // PURPOSE : Return the value from the General Params (in memory)
    //
    //  PARAMS :
    //      - INPUT :
    //          - GroupKey
    //          - SubjectKey
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //        The value of the selected General Param
    //
    //   NOTES :
    //
    public static String Value(String GroupKey, String SubjectKey)
    {
      String _key;
      String _value;

      // Wait till the values have been retrieved from the DB
      m_ev_data_available.WaitOne();

      // Build the Key
      _key = GroupKey + "." + SubjectKey;

      // Get the value
      try
      {
        m_rw_lock.AcquireReaderLock(Timeout.Infinite);
        if (!m_params.TryGetValue(_key, out _value))
        {
          _value = String.Empty;
        }
      }
      finally
      {
        m_rw_lock.ReleaseReaderLock();
      }

      return _value;
    } // Value

    public static Int32 GetInt32(String GroupKey, String SubjectKey, Int32 DefaultValue, Int32 MinValue, Int32 MaxValue)
    {
      String _str_value;

      Int32 _value;

      _str_value = GeneralParam.Value(GroupKey, SubjectKey);

      if (String.IsNullOrEmpty(_str_value))
      {
        return DefaultValue;
      }

      if (!Int32.TryParse(_str_value, out _value))
      {
        return DefaultValue;
      }
      if (_value < MinValue)
      {
        _value = MinValue;
      }
      if (_value > MaxValue)
      {
        _value = MaxValue;
      }
      return _value;
    }

    public static Int32 GetInt32(String GroupKey, String SubjectKey)
    {
      return GetInt32(GroupKey, SubjectKey, 0, Int32.MinValue, Int32.MaxValue);
    }

    public static Int32 GetInt32(String GroupKey, String SubjectKey, Int32 DefaultValue)
    {
      return GetInt32(GroupKey, SubjectKey, DefaultValue, Int32.MinValue, Int32.MaxValue);
    }

    public static Boolean GetBoolean(String GroupKey, String SubjectKey, Boolean DefaultValue)
    {
      return GetInt32(GroupKey, SubjectKey, DefaultValue ? 1 : 0) != 0;
    }

    public static Boolean GetBoolean(String GroupKey, String SubjectKey)
    {
      return GetBoolean(GroupKey, SubjectKey, false);
    }


    public static Decimal GetDecimal(String GroupKey, String SubjectKey, Decimal DefaultValue)
    {
      String _str_value;
      Decimal _value;

      _str_value = GeneralParam.Value(GroupKey, SubjectKey);

      if (String.IsNullOrEmpty(_str_value))
      {
        return DefaultValue;
      }

      if (!Decimal.TryParse(_str_value,
                            System.Globalization.NumberStyles.Any,
                            System.Globalization.CultureInfo.GetCultureInfo("en-US").NumberFormat,
                            out _value))
      {
        return DefaultValue;
      }

      return _value;
    }

    public static Decimal GetDecimal(String GroupKey, String SubjectKey)
    {
      return GetDecimal(GroupKey, SubjectKey, 0);
    }

    public static Currency GetCurrency(String GroupKey, String SubjectKey, Currency DefaultValue)
    {
      return GetDecimal(GroupKey, SubjectKey, DefaultValue);
    }

    public static Currency GetCurrency(String GroupKey, String SubjectKey)
    {
      return GetDecimal(GroupKey, SubjectKey, 0);
    }

    public static String GetString(String GroupKey, String SubjectKey)
    {
      return GetString(GroupKey, SubjectKey, String.Empty);
    }

    public static String GetString(String GroupKey, String SubjectKey, String DefaultValue)
    {
      String _value;

      _value = GeneralParam.Value(GroupKey, SubjectKey);

      if (String.IsNullOrEmpty(_value))
      {
        _value = DefaultValue;
      }

      return _value;
    }

    public static Int64 GetInt64(String GroupKey, String SubjectKey, Int64 DefaultValue)
    {
      String _str_value;
      Int64 _value;

      _str_value = GeneralParam.Value(GroupKey, SubjectKey);

      if (String.IsNullOrEmpty(_str_value))
      {
        return DefaultValue;
      }

      if (!Int64.TryParse(_str_value, out _value))
      {
        return DefaultValue;
      }

      return _value;
    }

    public static Int64 GetInt64(String GroupKey, String SubjectKey)
    {
      return GetInt64(GroupKey, SubjectKey, 0);
    }


    public static UInt64 GetUInt64(String GroupKey, String SubjectKey, UInt64 DefaultValue)
    {
      String _str_value;
      UInt64 _value;

      _str_value = GeneralParam.Value(GroupKey, SubjectKey);

      if (String.IsNullOrEmpty(_str_value))
      {
        return DefaultValue;
      }

      if (!UInt64.TryParse(_str_value, out _value))
      {
        return DefaultValue;
      }

      return _value;
    }

    public static UInt64 GetUInt64(String GroupKey, String SubjectKey)
    {
      return GetUInt64(GroupKey, SubjectKey, 0);
    }


    //------------------------------------------------------------------------------
    // PURPOSE :  Return the value from the General Params (in memory).
    //            This Value is splitted to obtain two decimal values
    //
    //  PARAMS :
    //      - INPUT : 
    //          - GroupKey
    //          - SubjectKey
    //          - DefaultValue1
    //          - DefaultValue2
    //
    //      - OUTPUT : 
    //          - Value1
    //          - Value2
    //
    // RETURNS :  The two values of the selected General Param (values separated with char ';')
    //
    //   NOTES : 
    //
    public static void GetDecimalRange(String GroupKey, String SubjectKey, out Decimal Value1, out Decimal Value2, Decimal DefaultValue1, Decimal DefaultValue2)
    {
      String _str_value = string.Empty;
      String[] _str_value_split;
      Decimal[] _decimal_value_split;
      Decimal[] _decimal_default_value_split;
      String _str_split_value1 = string.Empty;
      String _str_split_value2 = string.Empty;

      _str_value = GetString(GroupKey, SubjectKey);

      _str_value = _str_value.Replace("%", "");
      _str_value_split = _str_value.Split(';');
      _decimal_value_split = new Decimal[_str_value_split.Length];
      _decimal_default_value_split = new Decimal[_str_value_split.Length];
      _decimal_default_value_split[0] = DefaultValue1;
      _decimal_default_value_split[1] = DefaultValue2;

      for (int _idx = 0; _idx < _str_value_split.Length; _idx++)
      {
        try
        {
          if (String.IsNullOrEmpty(_str_value_split[_idx]))
          {
            _decimal_value_split[_idx] = _decimal_default_value_split[_idx];

            continue;
          }

          if (!Decimal.TryParse(_str_value_split[_idx],
                                System.Globalization.NumberStyles.Any,
                                System.Globalization.CultureInfo.GetCultureInfo("en-US").NumberFormat,
                                out _decimal_value_split[_idx]))
          {
            _decimal_value_split[_idx] = _decimal_default_value_split[_idx];
          }
        }
        catch
        {
          _decimal_value_split[_idx] = _decimal_default_value_split[_idx];
        }
      }

      Value1 = _decimal_value_split[0];
      Value2 = _decimal_value_split[1];

    } // GetDecimalRange

    public static void GetDecimalRange(String GroupKey, String SubjectKey, out Decimal Value1, out Decimal Value2)
    {
      GetDecimalRange(GroupKey, SubjectKey, out Value1, out Value2, 0, 0);
    }
  }
}
