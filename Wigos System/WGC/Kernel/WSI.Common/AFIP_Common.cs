﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AFIP_Common.cs
// 
//   DESCRIPTION: Common Procedures to the AFIP Service
// 
//        AUTHOR: Ferran Ortner
// 
// CREATION DATE: 06-May-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 06-MAY-2016 FOS    First release 
// 12-JUL-2016 JPJ    New parameters 
// 23-JAN-2017 FGB    Bug 23207: AFIP_Client: Envio de jornadas desde Wigos no coincide con lo solicitado por Jaza Local y se muestra la jornada anterior.
// ----------- ------ ----------------------------------------------------------

using System;
using System.Data;

namespace WSI.Common.AFIP_Common
{
  public static class AFIP_Common
  {
    #region "Constants"
    public const String GP_AFIPClient_GROUP = "AFIPClient";
    private const Boolean DefaultIsEnabled = false;
    private const Int32 DefaultFrequencyToGetPendingTasks = 5;
    private const String DefaultServerAddress = "http://localhost/JAZALOCALWS/Service.asmx";
    private const Boolean DefaultEnableMessageTrace = false;
    #endregion

    #region "Properties"
    /// <summary>
    /// Returns if the AFIP Client is enabled
    /// </summary>
    public static Boolean IsEnabled
    {
      get { return GeneralParam.GetBoolean(GP_AFIPClient_GROUP, "Enabled", DefaultIsEnabled); }
    }

    /// <summary>
    /// Returns the frequency (in seconds) for looking for pending tasks
    /// </summary>
    public static Int32 FrequencyToGetPendingTasks
    {
      get { return GeneralParam.GetInt32(GP_AFIPClient_GROUP, "FrequencyToGetPendingTasks", DefaultFrequencyToGetPendingTasks); }
    }

    /// <summary>
    /// Returns the closing time hour
    /// </summary>
    private static Int32 AFIPClosingTime
    {
      get { return GeneralParam.GetInt32(GP_AFIPClient_GROUP, "ClosingTime", 0, 0, 23); }
    }

    /// <summary>
    /// Returns the closing time minutes
    /// </summary>
    private static Int32 AFIPClosingTimeMinutes
    {
      get { return GeneralParam.GetInt32(GP_AFIPClient_GROUP, "ClosingTimeMinutes", 0, 0, 59); }
    }

    /// <summary>
    /// Returns the minutes that has to pass between the closing time and when we can send the meters
    /// It is to give enough time to correct the meters.
    /// </summary>
    private static Int32 AFIPTimeOffsetMinutesToSendMeters
    {
      get { return GeneralParam.GetInt32(GP_AFIPClient_GROUP, "TimeOffsetMinutesToSendMeters", 0); }
    }

    /// <summary>
    /// Returns the first Jaza Local application URL
    /// </summary>
    public static String AFIPServerAddress
    {
      get { return GeneralParam.GetString(GP_AFIPClient_GROUP, "ServerAddress", DefaultServerAddress); }
    }

    /// <summary>
    /// Returns the second (redundant) Jaza Local application URL
    /// </summary>
    public static String AFIPServerAddress2
    {
      get { return GeneralParam.GetString(GP_AFIPClient_GROUP, "ServerAddress2", DefaultServerAddress); }
    }

    /// <summary>
    /// Returns if the message trace functionality is active
    /// </summary>
    public static Boolean AFIPEnableMessageTrace
    {
      get { return GeneralParam.GetBoolean(GP_AFIPClient_GROUP, "EnableMessageTrace", DefaultEnableMessageTrace); }
    }
    #endregion

    #region "Public Methods"
    /// <summary>
    /// For a SessionDate returns the FromDate and ToDate
    /// </summary>
    /// <param name="SessionDate"></param>
    /// <param name="SessionFrom"></param>
    /// <param name="SessionTo"></param>
    /// <returns></returns>
    public static Boolean GetSessionStartAndClosingTime(DateTime SessionDate, out DateTime SessionFrom, out DateTime SessionTo, Boolean IsRequest)
    {
      Int32 _offset_minutes;

      SessionFrom = DateTime.MinValue;
      SessionTo = DateTime.MaxValue;

      try
      {
        //Get start of the session
        if (!GetSessionStartTime(SessionDate, out SessionFrom))
        {
          return false;
        }

        //Get end of the session
        SessionTo = GetEndOfSession(SessionDate, SessionFrom, IsRequest);

        //If is a request then SessionDate could be NOW
        if (!IsRequest)
        {
          //Session has not been open
          if (SessionFrom > WGDB.Now)
          {
            Log.Error(String.Format("GetSessionStartAndClosingTime - Session: {0} has not been opened", SessionDate.ToString("yyyy-MM-dd")));
            return false;
          }

          //Session has not been closed
          if (SessionTo > WGDB.Now)
          {
            Log.Error(String.Format("GetSessionStartAndClosingTime - Session: {0} is still open", SessionDate.ToString("yyyy-MM-dd")));
            return false;
          }

          //It has not passed the time to correct the session meters
          _offset_minutes = AFIPTimeOffsetMinutesToSendMeters;
          if (SessionTo.AddMinutes(_offset_minutes) > WGDB.Now)
          {
            Log.Error(String.Format("GetSessionStartAndClosingTime - Session: {0} is not ready to be sent", SessionDate.ToString("yyyy-MM-dd")));
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Error at GetSessionStartAndClosingTime - SessionDate: {0} - {1}", SessionDate.ToString("yyyy-MM-dd"), _ex.Message));
      }

      return false;
    }

    /// <summary>
    /// Get end of session
    /// </summary>
    /// <param name="SessionDate"></param>
    /// <param name="SessionFrom"></param>
    /// <param name="IsRequest"></param>
    /// <returns></returns>
    private static DateTime GetEndOfSession(DateTime SessionDate, DateTime SessionFrom, Boolean IsRequest)
    {
      DateTime SessionTo;

      if (IsRequest)
      {
        SessionTo = SessionDate;
      }
      else
      {
        SessionTo = SessionFrom.AddDays(1);
      }

      return SessionTo;
    }

    /// <summary>
    /// For a SessionDate returns the date of start
    /// </summary>
    /// <param name="SessionDate"></param>
    /// <param name="SessionFrom"></param>
    /// <returns></returns>
    public static Boolean GetSessionStartTime(DateTime SessionDate, out DateTime SessionFrom)
    {
      Int32 _closing_HH;
      Int32 _closing_MM;
      Int32 _closing_SS;

      SessionFrom = DateTime.MinValue;

      try
      {
        _closing_HH = AFIPClosingTime;
        _closing_MM = AFIPClosingTimeMinutes;
        _closing_SS = 0;

        SessionFrom = new DateTime(SessionDate.Year, SessionDate.Month, SessionDate.Day,
                                   _closing_HH, _closing_MM, _closing_SS);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("Error at GetSessionStartTime - SessionDate: {0} - {1}", SessionDate.ToString("yyyy-MM-dd"), _ex.Message));
      }

      return false;
    }

    /// <summary>
    /// Generates XML for DataTable
    /// </summary>
    /// <param name="MetersDataTable"></param>
    /// <param name="XMLTableName"></param>
    /// <param name="XMLString"></param>
    /// <param name="ErrorMessage"></param>
    /// <returns></returns> 
    public static Boolean GenerateXMLFromDatatable(DataTable MetersDataTable, String XMLTableName, out String XMLString, out String ErrorMessage)
    {
      XMLString = String.Empty;
      ErrorMessage = String.Empty;

      try
      {
        if (MetersDataTable.Rows.Count > 0)
        {
          MetersDataTable.TableName = XMLTableName;
          using (System.IO.StringWriter _xml_element = new System.IO.StringWriter())
          {
            MetersDataTable.WriteXml(_xml_element, XmlWriteMode.IgnoreSchema);
            XMLString = _xml_element.ToString();
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        ErrorMessage = _ex.Message;
      }

      return false;
    }
    #endregion

    #region "Enums"
    public enum PendingType
    {
      Terminals = 1,
      Bingo = 2,
      GamingTables = 3,
      Request = 4
    }
    #endregion
  }
}
