using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Threading;

namespace WSI.Common
{
  /// <summary>
  /// This class is used to manage the different service status
  /// </summary>
  public class ServiceStatus
  {
    /// <summary>
    /// Inits the service and deppending on the StandBy variable the service will wait until the 
    /// </summary>
    /// <param name="Protocol"></param>
    /// <param name="service_ip_address"></param>
    /// <param name="Connection"></param>
    /// <param name="StandBy"></param>
    static public bool InitService(WaitHandle StopEvent, String Protocol, String IpAddress, Boolean StandBy, String ServiceVersion)
    {
      SqlTransaction sql_trx;
      SqlConnection conn;
      int num_runnings;
      Boolean _running;
      Int32 _wait_hint;
      
      conn = null;
      sql_trx = null;
      _running = false;

      try
      {
        conn = WGDB.Connection();

        sql_trx = conn.BeginTransaction();

        if (!Services.DB_UpdateService(Protocol, IpAddress, "UNKNOWN", ServiceVersion,  sql_trx))
        {
          if (!Services.DB_InsertService(Protocol, IpAddress, "UNKNOWN", ServiceVersion, sql_trx))
          {
            Log.Error(" Error updating service");
            sql_trx.Rollback();
            System.Threading.Thread.Sleep(5000);
            Environment.Exit(0);
          }
        }
        sql_trx.Commit();

        conn.Close();
        conn = null;

        if (!StandBy)
        {
          conn = WGDB.Connection();
          sql_trx = conn.BeginTransaction();
          Services.DB_UpdateService(Protocol, IpAddress, "RUNNING", ServiceVersion, sql_trx);
          sql_trx.Commit();
          _running = true;
        }
        else 
        {
          _wait_hint = 0;
          while (!StopEvent.WaitOne(_wait_hint))
          {
            _wait_hint = Services.CHECK_TIMEOUT;
            try
            {
              conn = WGDB.Connection();

              sql_trx = conn.BeginTransaction();
              Services.DB_UpdateService(Protocol, IpAddress, "STANDBY", ServiceVersion, sql_trx);
              sql_trx.Commit();

              sql_trx = conn.BeginTransaction();
              Services.DB_UpdateTimeOuts(Protocol, sql_trx);
              num_runnings = Services.DB_GetRunnings(Protocol, sql_trx);
              sql_trx.Commit();

              if (num_runnings == 0)
              {
                sql_trx = conn.BeginTransaction();
                
                if ( Services.DB_LockServices (Protocol, sql_trx) )
                {
                  Services.DB_UpdateService(Protocol, IpAddress, "RUNNING", ServiceVersion, sql_trx);
                  num_runnings = Services.DB_GetRunnings(Protocol, sql_trx);
                  if (num_runnings == 1)
                  {
                    sql_trx.Commit();
                    
                    _running = true;
                    
                    break;
                  }
                }
                sql_trx.Rollback();
              }
            }
            catch (Exception ex)
            {
              Log.Error(" Error Initializing service status manager.");
              Log.Exception(ex);
            }
            finally
            {
              if (sql_trx != null)
              {
                if (sql_trx.Connection != null)
                {
                  sql_trx.Rollback();
                }
              }

              if (conn != null)
              {
                conn.Close();
                conn = null;
              }
            } // try
          } // While
        } //Stand by
      }
      catch (Exception ex)
      {
        Log.Error(" Error Initializing service status manager.");
        Log.Exception(ex);
      }
      finally
      {
        if (sql_trx != null)
        {
          if (sql_trx.Connection != null)
          {
            try { sql_trx.Rollback(); }
            catch { }
          }
          sql_trx = null;
        }
        if (conn != null)
        {
          try { conn.Close(); }
          catch { }
          conn = null;
        }
      }
      
      return _running;
    }

    /// <summary>
    /// Inits the service and deppending on the StandBy variable the service will wait until the 
    /// </summary>
    /// <param name="Protocol"></param>
    /// <param name="service_ip_address"></param>
    /// <param name="Connection"></param>
    /// <param name="StandBy"></param>
    static public void SetServiceStatus(String Protocol, String IpAddress, Boolean StandBy, String ServiceVersion, String NewStatus)
    {
      SqlTransaction _sql_trx;
      SqlConnection _sql_conn;

      _sql_conn = null;
      _sql_trx = null;

      try
      {
        _sql_conn = WGDB.Connection();
        _sql_trx = _sql_conn.BeginTransaction();
        Services.DB_UpdateService(Protocol, IpAddress, NewStatus, ServiceVersion, _sql_trx);
        _sql_trx.Commit();
        _sql_conn.Close();
      }
      catch 
      {
      }
      finally
      {
        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            try { _sql_trx.Rollback(); }
            catch { }
          }
          _sql_trx = null;
        }
        if (_sql_conn != null)
        {
          try { _sql_conn.Close(); }
          catch { }
          _sql_conn = null;
        }
      }
    }



    /// <summary>
    /// Sets the status to running continuously, and checks for a possible restart of the service.
    /// </summary>
    /// <param name="Protocol"></param>
    /// <param name="service_ip_address"></param>
    /// <param name="conn"></param>
    /// <param name="restart"></param>
    static public void RunningService(String Protocol, String IpAddress, Boolean Restart, String ServiceVersion)
    {
      ManualResetEvent _ev_stop;
      _ev_stop = new ManualResetEvent (false);
      RunningService(_ev_stop,Protocol, IpAddress, Restart, ServiceVersion);
    }
    
    static public void RunningService(WaitHandle EvStop, String Protocol, String IpAddress, Boolean Restart, String ServiceVersion)
    {
      SqlTransaction sql_trx;
      SqlConnection conn;
      int _wait_timeout;
      int _tick_last_check;

      conn = null;
      sql_trx = null;

      _wait_timeout    = 0;
      _tick_last_check = Misc.GetTickCount() - Services.CHECK_RUNNING;
      
      while (!EvStop.WaitOne(0))
      {
        _wait_timeout = Math.Max(0, Services.CHECK_RUNNING - (int)Misc.GetElapsedTicks(_tick_last_check));
        if ( EvStop.WaitOne(_wait_timeout) )
        {
          // Stop requested
          break;
        }
        
        _tick_last_check = Misc.GetTickCount();
        
        try
        {
          conn = WGDB.Connection();
          
          // This uses two different transactions to distinct between updating to running and updating to timeout.
          sql_trx = conn.BeginTransaction();
          Services.DB_UpdateService(Protocol, IpAddress, "RUNNING", ServiceVersion,  sql_trx);
          sql_trx.Commit();

          sql_trx = conn.BeginTransaction();
          Services.DB_UpdateTimeOuts(Protocol, sql_trx);
          sql_trx.Commit();

          if (Restart)
          {
            sql_trx = conn.BeginTransaction();

            if (Services.DB_GetStatus(Protocol, sql_trx) != "RUNNING")
            {
              Log.Error(" Service not RUNNING! Restarting ...");

              Services.DB_UpdateService(Protocol, IpAddress, "UNKNOWN", ServiceVersion, sql_trx);
              sql_trx.Commit();
              
              // Suicide
              

              System.Threading.Thread.Sleep(5000);
              Environment.Exit(0);
            }

            if (Services.DB_CheckLivingService(Protocol, sql_trx))
            {
              Log.Error(" Found other RUNNING service! Restarting ...");

              Services.DB_UpdateService(Protocol, IpAddress, "UNKNOWN", ServiceVersion, sql_trx);
              sql_trx.Commit();
              
              System.Threading.Thread.Sleep(5000);
              Environment.Exit(0);
            }

            sql_trx.Commit();
          }
        }
        catch (Exception ex)
        {
          Log.Error(" Error Running service status manager.");
          Log.Exception(ex);
        }
        finally
        {
          if (sql_trx != null)
          {
            if (sql_trx.Connection != null)
            {
              sql_trx.Rollback();
            }
          }

          if (conn != null)
          {
            conn.Close();
            conn = null;
          }
        }
      }
    }
  }
}
