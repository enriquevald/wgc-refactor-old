//-------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   OperationsScheduleService.cs
// DESCRIPTION:   Operations schedule manager
// AUTHOR:        Marcos Piedra & Quim Morales
// CREATION DATE: 01-MAR-2013
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 01-MAR-2013  MPO & QMP    Initial version
// 26-MAR-2013  QMP    Fixed Bug #668: error when first and last email overlap. Event detection must include PromoBOX and Acceptor cash movements.
// 16-APR-2013  QMP    Added ServiceName parameter to Init()
// -------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;

namespace WSI.Common
{
  public static class OperationsScheduleService
  {

    private static Thread m_thread;

    private enum OPERATIONS_ALLOWED_STATUS
    {
      UNKNOWN = -1,
      NOT_ALLOWED = 0,
      ALLOWED = 1,
    }

    [Flags]
    private enum EVENT_TYPE
    {
      NONE = 0,                       // 000000
      LOGIN = 1,                      // 000001
      GUI_OPERATIONS = 2,             // 000010
      PLAY_SESSION = 4,               // 000100
      CASH_MOVEMENT = 8,              // 001000
      SAS_EVENT_OPERATOR_ACCESS = 16, // 010000
      SAS_EVENT_PHYSICAL_ACCESS = 32, // 100000
    }

    private const Int32 EVENT_CHECK_WAIT_MINUTES = 5;
    private const Int32 EVENT_CHECK_BACKWARDS_MINUTES = 10;

    private static StringBuilder m_str_sql_login;
    private static StringBuilder m_str_sql_gui_operation;
    private static StringBuilder m_str_sql_play_session;
    private static StringBuilder m_str_sql_cash_movement;
    private static StringBuilder m_str_sql_sas_event_operator_access;
    private static StringBuilder m_str_sql_sas_event_physical_access;

    private static String m_service_name;

    //------------------------------------------------------------------------------
    // PURPOSE : Initializes OperationsSchedule and Starts PeriodicJobThread thread 
    //
    //  PARAMS :
    //      - INPUT : String ServiceName
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static public void Init(String ServiceName)
    {
      m_service_name = ServiceName;

      m_thread = new Thread(PeriodicJobThread);
      m_thread.Name = "OperationsScheduleThread";
      m_thread.Start();

    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Periodic thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static private void PeriodicJobThread()
    {
      Int32 _wait_hint;
      OPERATIONS_ALLOWED_STATUS _operations_allowed;
      OPERATIONS_ALLOWED_STATUS _previous_operations_allowed;
      EVENT_TYPE _event_mask;
      EVENT_TYPE _new_event_mask;
      EVENT_TYPE _first_email_event_mask;
      DateTime _now;
      DateTime _next_event_check;
      DateTime _check_date_from;
      DateTime _record_date_from;
      DateTime _last_schedule_update_time;
      DateTime _workday;
      DateTime _new_workday;
      Boolean _update_events;
      Boolean _update_sent;
      Boolean _update_first_event_detected;
      Boolean _last_check;
      Boolean _terminals_enabled;

      //Initialize SQL queries
      BuildQueries();

      //Initialize variables
      _record_date_from = DateTime.MinValue;
      _check_date_from = DateTime.MinValue;
      _next_event_check = DateTime.MinValue;
      _workday = DateTime.MinValue;
      _new_workday = DateTime.MinValue;
      _operations_allowed = OPERATIONS_ALLOWED_STATUS.UNKNOWN;
      _previous_operations_allowed = OPERATIONS_ALLOWED_STATUS.UNKNOWN;
      _event_mask = EVENT_TYPE.NONE;
      _first_email_event_mask = EVENT_TYPE.NONE;

      while (true)
      {
        _now = WGDB.Now;
        _wait_hint = 60000 - _now.Second * 1000 - _now.Millisecond;
        Thread.Sleep(_wait_hint);

        //Round NOW to nearest minute
        _now = _now.AddMilliseconds(_wait_hint);
        _now = new DateTime(_now.Year, _now.Month, _now.Day, _now.Hour, _now.Minute, 0);

        _last_check = false;

        //If the service was down, check the missing intervals
        if (_operations_allowed == OPERATIONS_ALLOWED_STATUS.UNKNOWN)
        {
          if (!GetLastScheduleUpdate(out _last_schedule_update_time))
          {
            Log.Message("OperationsScheduleService: Exception in function GetLastScheduleUpdate");
            continue;
          }

          if (!ProcessMissingIntervals(_last_schedule_update_time, _now, out _operations_allowed, out _event_mask, out _first_email_event_mask, out _record_date_from))
          {
            Log.Message("OperationsScheduleService: Exception in function ProcessMissingIntervals");
            _operations_allowed = OPERATIONS_ALLOWED_STATUS.UNKNOWN;
            continue;
          }

          _previous_operations_allowed = _operations_allowed;

          //Set next event check time
          if (_operations_allowed == OPERATIONS_ALLOWED_STATUS.NOT_ALLOWED)
          {
            _next_event_check = _now.AddMinutes(EVENT_CHECK_WAIT_MINUTES);
          }

          //Force an update of OPERATIONS_SCHEDULE_STATUS
          if (!ForceUpdateStatus(_operations_allowed, _record_date_from))
          {
            Log.Message("OperationsScheduleService: Exception in function ForceUpdateStatus");
            _operations_allowed = OPERATIONS_ALLOWED_STATUS.UNKNOWN;
            continue;
          }

          //Clean old schedule exceptions from OPERATIONS_SCHEDULE
          if (!CleanOperationsSchedule(_now))
          {
            Log.Message("OperationsScheduleService: Exception in function CleanOperationsSchedule");
            _operations_allowed = OPERATIONS_ALLOWED_STATUS.UNKNOWN;
            continue;
          }

          //Get the current workday
          _workday = Misc.Opening(_now);
          _new_workday = _workday;
        }
        else
        {
          //Update operations schedule status
          if (!UpdateStatus(_now, out _terminals_enabled, out _operations_allowed))
          {
            Log.Message("OperationsScheduleService: Exception in function UpdateStatus");
            _operations_allowed = OPERATIONS_ALLOWED_STATUS.UNKNOWN;
            continue;
          }

          //Entering or leaving an out of hours interval
          if (_operations_allowed != _previous_operations_allowed)
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              switch (_operations_allowed)
              {
                case OPERATIONS_ALLOWED_STATUS.NOT_ALLOWED:
                  //Insert a new record in table OPERATIONS_AFTER_HOURS
                  if (!NewRecord(_now, _db_trx.SqlTransaction))
                  {
                    Log.Message("OperationsScheduleService: Exception in function NewRecord");
                    _operations_allowed = OPERATIONS_ALLOWED_STATUS.UNKNOWN;
                    continue;
                  }

                  _record_date_from = _now;
                  _event_mask = EVENT_TYPE.NONE;
                  _first_email_event_mask = EVENT_TYPE.NONE;
                  _next_event_check = _now.AddMinutes(EVENT_CHECK_WAIT_MINUTES);

                  break;
                case OPERATIONS_ALLOWED_STATUS.ALLOWED:
                  //Set LAST CHECK flag
                  _last_check = true;

                  //Close the open record in table OPERATIONS_AFTER_HOURS
                  if (!CloseRecord(_record_date_from, _now, _db_trx.SqlTransaction))
                  {
                    Log.Message("OperationsScheduleService: Exception in function CloseRecord");
                    _operations_allowed = OPERATIONS_ALLOWED_STATUS.UNKNOWN;
                    continue;
                  }

                  break;
              }

              _db_trx.Commit();
            }

            _previous_operations_allowed = _operations_allowed;
          }

          //If it's a new workday, clean OPERATIONS_SCHEDULE table
          _new_workday = Misc.Opening(_now);

          if (_new_workday != _workday)
          {
            //Update OPERATIONS_SCHEDULE_STATUS if operations are allowed
            if (_operations_allowed == OPERATIONS_ALLOWED_STATUS.ALLOWED)
            {
              if (!ForceUpdateStatus(_operations_allowed, _now))
              {
                Log.Message("OperationsScheduleService: Exception in function ForceUpdateStatus");
                _operations_allowed = OPERATIONS_ALLOWED_STATUS.UNKNOWN;
                continue;
              }
            }

            if (!CleanOperationsSchedule(_now))
            {
              Log.Message("OperationsScheduleService: Exception in function CleanOperationsSchedule");
              _operations_allowed = OPERATIONS_ALLOWED_STATUS.UNKNOWN;
              continue;
            }

            _workday = _new_workday;
          }

          //Check events out of hours
          //Range to check
          if (_now.AddMinutes(-EVENT_CHECK_BACKWARDS_MINUTES) < _record_date_from)
          {
            _check_date_from = _record_date_from;
          }
          else
          {
            _check_date_from = _now.AddMinutes(-EVENT_CHECK_BACKWARDS_MINUTES);
          }

          //Only check if we are outside of Operations Schedule or in Last Check
          if ((_operations_allowed == OPERATIONS_ALLOWED_STATUS.NOT_ALLOWED) || _last_check)
          {
            //Perform check only when it's time or if it's last check
            if ((_now >= _next_event_check) || _last_check)
            {
              using (DB_TRX _db_trx = new DB_TRX())
              {
                //Get mask for new detected events
                if (!CheckEvents(_check_date_from, _now, _event_mask, out _new_event_mask, _db_trx.SqlTransaction))
                {
                  Log.Message("OperationsScheduleService: Exception in function CheckEvents");
                  _operations_allowed = OPERATIONS_ALLOWED_STATUS.UNKNOWN;
                  continue;
                }
              }

              _update_events = false;
              _update_sent = false;
              _update_first_event_detected = false;

              //Check if new events were detected
              if (_new_event_mask != _event_mask)
              {
                _update_events = true;

                //Send first e-mail if necessary
                if (_event_mask == EVENT_TYPE.NONE)
                {
                  if (!_last_check)
                  {
                    if (!SendEmail(_new_event_mask, _check_date_from, _now, true))
                    {
                      Log.Message("OperationsScheduleService: Exception in function SendEmail");
                      _operations_allowed = OPERATIONS_ALLOWED_STATUS.UNKNOWN;
                      continue;
                    }

                    _first_email_event_mask = _new_event_mask;
                    _update_sent = true;
                  }

                  _update_first_event_detected = true;
                }
              }

              //Send last e-mail if necessary
              if (_last_check && _new_event_mask != _first_email_event_mask)
              {
                if (!SendEmail(_new_event_mask, _record_date_from, _now, false))
                {
                  Log.Message("OperationsScheduleService: Exception in function SendEmail");
                  _operations_allowed = OPERATIONS_ALLOWED_STATUS.UNKNOWN;
                  continue;
                }

                _update_events = true;
                _update_sent = true;
              }

              //Update OPERATIONS_AFTER_HOURS record
              using (DB_TRX _db_trx = new DB_TRX())
              {
                if (!UpdateRecord(_new_event_mask, _record_date_from, _now, _update_events, _update_sent, _update_first_event_detected, _db_trx.SqlTransaction))
                {
                  Log.Message("OperationsScheduleService: Exception in function UpdateRecord");
                  _operations_allowed = OPERATIONS_ALLOWED_STATUS.UNKNOWN;
                  continue;
                }

                _db_trx.Commit();

              }

              //Update in-memory mask and next event check time
              _event_mask = _new_event_mask;
              _next_event_check = _next_event_check.AddMinutes(EVENT_CHECK_WAIT_MINUTES);
            }
          }
        }
      } // while (true)
    } // PeriodicJobThread

    //------------------------------------------------------------------------------
    // PURPOSE : Build the SQL queries needed for the service.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void BuildQueries()
    {
      List<String> _alarm_codes_operator_access_list;
      List<String> _alarm_codes_physical_access_list;
      List<String> _user_type_list;
      String _alarm_codes_operator_access_string;
      String _alarm_codes_physical_access_string;
      String _user_type_string;

      //Logins
      m_str_sql_login = new StringBuilder();
      m_str_sql_login.AppendLine(" SELECT   TOP 1 GA_AUDIT_ID                               ");
      m_str_sql_login.AppendLine("   FROM   GUI_AUDIT WITH(INDEX(IX_gui_audit_ga_datetime)) ");
      m_str_sql_login.AppendLine("  WHERE   GA_AUDIT_CODE = 4                               "); //Access control (login, logout, failed attempt)
      m_str_sql_login.AppendLine("    AND   GA_DATETIME  >= @pDateFrom                      ");
      m_str_sql_login.AppendLine("    AND   GA_DATETIME  <  @pDateTo                        ");

      //GUI Operations
      m_str_sql_gui_operation = new StringBuilder();
      m_str_sql_gui_operation.AppendLine(" SELECT   TOP 1 GA_AUDIT_ID                               ");
      m_str_sql_gui_operation.AppendLine("   FROM   GUI_AUDIT WITH(INDEX(IX_gui_audit_ga_datetime)) ");
      m_str_sql_gui_operation.AppendLine("  WHERE   GA_AUDIT_CODE <> 4                              "); //GUI operations
      m_str_sql_gui_operation.AppendLine("    AND   GA_DATETIME   >= @pDateFrom                     ");
      m_str_sql_gui_operation.AppendLine("    AND   GA_DATETIME   <  @pDateTo                       ");

      //Play Sessions
      m_str_sql_play_session = new StringBuilder();
      m_str_sql_play_session.AppendLine(" SELECT   TOP 1 PS_PLAY_SESSION_ID                         ");
      m_str_sql_play_session.AppendLine("   FROM   PLAY_SESSIONS WITH(INDEX(IX_ps_finished_status)) ");
      m_str_sql_play_session.AppendLine("  WHERE   PS_STATUS IN (0, 1, 3, 4)                        "); //Open, closed, manually closed, cancelled
      m_str_sql_play_session.AppendLine("    AND   PS_FINISHED >= @pDateFrom                        ");
      m_str_sql_play_session.AppendLine("    AND   PS_FINISHED <  @pDateTo                          ");

      //Cash Movements
      _user_type_list = new List<String>();
      _user_type_list.Add(((Int32)GU_USER_TYPE.USER).ToString());
      _user_type_list.Add(((Int32)GU_USER_TYPE.SYS_ACCEPTOR).ToString());
      _user_type_list.Add(((Int32)GU_USER_TYPE.SYS_PROMOBOX).ToString());
      _user_type_list.Add(((Int32)GU_USER_TYPE.SUPERUSER).ToString());
      _user_type_string = String.Join(",", _user_type_list.ToArray());

      m_str_sql_cash_movement = new StringBuilder();
      m_str_sql_cash_movement.AppendLine(" SELECT   TOP 1 CM_MOVEMENT_ID                           ");
      m_str_sql_cash_movement.AppendLine("   FROM   CASHIER_MOVEMENTS WITH(INDEX(IX_cm_date_type)) ");
      m_str_sql_cash_movement.AppendLine("  INNER   JOIN GUI_USERS ON CM_USER_ID = GU_USER_ID      ");
      m_str_sql_cash_movement.AppendLine("  WHERE   CM_DATE      >= @pDateFrom                     ");
      m_str_sql_cash_movement.AppendLine("    AND   CM_DATE      <  @pDateTo                       ");
      m_str_sql_cash_movement.AppendLine("    AND   GU_USER_TYPE IN (" + _user_type_string + ")    "); // User, Acceptor, PromoBOX and SU

      //SAS Events - Operator Access
      _alarm_codes_operator_access_list = new List<String>();
      _alarm_codes_operator_access_list.Add(Convert.ToInt32("0x00010082", 16).ToString()); //Display meters or attendant menu has been entered
      _alarm_codes_operator_access_list.Add(Convert.ToInt32("0x00010083", 16).ToString()); //Display meters or attendant menu has been exited
      _alarm_codes_operator_access_list.Add(Convert.ToInt32("0x00010084", 16).ToString()); //Self test or operator menu has been entered
      _alarm_codes_operator_access_list.Add(Convert.ToInt32("0x00010085", 16).ToString()); //Self test or operator menu has been exited
      _alarm_codes_operator_access_list.Add(Convert.ToInt32("0x00010086", 16).ToString()); //Gaming machine is out of service (by attendant)
      _alarm_codes_operator_access_list.Add(Convert.ToInt32("0x0001003C", 16).ToString()); //Operator changed options
      _alarm_codes_operator_access_list.Add(Convert.ToInt32("0x00060013", 16).ToString()); //WCP_OPERATION_CODE_TEST_MODE_ENTER
      _alarm_codes_operator_access_list.Add(Convert.ToInt32("0x00060014", 16).ToString()); //WCP_OPERATION_CODE_TEST_MODE_LEAVE
      _alarm_codes_operator_access_list.Add(Convert.ToInt32("0x00060016", 16).ToString()); //WCP_OPERATION_CODE_ATTENDANT_MENU_ENTER
      _alarm_codes_operator_access_list.Add(Convert.ToInt32("0x00060017", 16).ToString()); //WCP_OPERATION_CODE_ATTENDANT_MENU_EXIT
      _alarm_codes_operator_access_list.Add(Convert.ToInt32("0x00060018", 16).ToString()); //WCP_OPERATION_CODE_OPERATOR_MENU_ENTER
      _alarm_codes_operator_access_list.Add(Convert.ToInt32("0x00060019", 16).ToString()); //WCP_OPERATION_CODE_OPERATOR_MENU_EXIT
      _alarm_codes_operator_access_list.Add(Convert.ToInt32("0x0006001B", 16).ToString()); //WCP_OPERATION_CODE_HANDPAY_REQUESTED
      _alarm_codes_operator_access_list.Add(Convert.ToInt32("0x0006001C", 16).ToString()); //WCP_OPERATION_CODE_HANDPAY_RESET
      _alarm_codes_operator_access_list.Add(Convert.ToInt32("0x0006001A", 16).ToString()); //WCP_OPERATION_CODE_CONFIG_SETTINGS_MODIFIED
      _alarm_codes_operator_access_string = String.Join(",", _alarm_codes_operator_access_list.ToArray());

      m_str_sql_sas_event_operator_access = new StringBuilder();
      m_str_sql_sas_event_operator_access.AppendLine(" SELECT   TOP 1 AL_ALARM_ID                                               ");
      m_str_sql_sas_event_operator_access.AppendLine("   FROM   ALARMS WITH(INDEX(IX_alarms_datetime_severity_source))          ");
      m_str_sql_sas_event_operator_access.AppendLine("  WHERE   AL_SOURCE_CODE IN (4, 5, 7)                                     "); //SAS Host Terminal, SAS Machine Terminal, WCP Terminal
      m_str_sql_sas_event_operator_access.AppendLine("    AND   AL_SEVERITY    IN (1, 2, 3)                                     ");
      m_str_sql_sas_event_operator_access.AppendLine("    AND   AL_DATETIME    >= @pDateFrom                                    ");
      m_str_sql_sas_event_operator_access.AppendLine("    AND   AL_DATETIME    <  @pDateTo                                      ");
      m_str_sql_sas_event_operator_access.AppendLine("    AND   AL_ALARM_CODE  IN (" + _alarm_codes_operator_access_string + ") ");

      //SAS Events - Physical Access
      _alarm_codes_physical_access_list = new List<String>();
      _alarm_codes_physical_access_list.Add(Convert.ToInt32("0x00010011", 16).ToString()); //Slot door was opened
      _alarm_codes_physical_access_list.Add(Convert.ToInt32("0x00010012", 16).ToString()); //Slot door was closed
      _alarm_codes_physical_access_list.Add(Convert.ToInt32("0x00010013", 16).ToString()); //Drop door was opened
      _alarm_codes_physical_access_list.Add(Convert.ToInt32("0x00010014", 16).ToString()); //Drop door was closed
      _alarm_codes_physical_access_list.Add(Convert.ToInt32("0x00010015", 16).ToString()); //Card cage was opened
      _alarm_codes_physical_access_list.Add(Convert.ToInt32("0x00010016", 16).ToString()); //Card cage was closed
      _alarm_codes_physical_access_list.Add(Convert.ToInt32("0x00010019", 16).ToString()); //Cashbox door was opened
      _alarm_codes_physical_access_list.Add(Convert.ToInt32("0x0001001A", 16).ToString()); //Cashbox door was closed
      _alarm_codes_physical_access_list.Add(Convert.ToInt32("0x0001001B", 16).ToString()); //Cashbox was removed
      _alarm_codes_physical_access_list.Add(Convert.ToInt32("0x0001001C", 16).ToString()); //Cashbox was installed
      _alarm_codes_physical_access_list.Add(Convert.ToInt32("0x0001001D", 16).ToString()); //Belly door was opened
      _alarm_codes_physical_access_list.Add(Convert.ToInt32("0x0001001E", 16).ToString()); //Belly door was closed
      _alarm_codes_physical_access_list.Add(Convert.ToInt32("0x0006000C", 16).ToString()); //WCP_OPERATION_CODE_DOOR_OPENED
      _alarm_codes_physical_access_list.Add(Convert.ToInt32("0x0006000D", 16).ToString()); //WCP_OPERATION_CODE_DOOR_CLOSED
      _alarm_codes_physical_access_list.Add(Convert.ToInt32("0x00060021", 16).ToString()); //WCP_OPERATION_CODE_CHANGE_STACKER
      _alarm_codes_physical_access_string = String.Join(",", _alarm_codes_physical_access_list.ToArray());

      m_str_sql_sas_event_physical_access = new StringBuilder();
      m_str_sql_sas_event_physical_access.AppendLine(" SELECT   TOP 1 AL_ALARM_ID                                               ");
      m_str_sql_sas_event_physical_access.AppendLine("   FROM   ALARMS WITH(INDEX(IX_alarms_datetime_severity_source))          ");
      m_str_sql_sas_event_physical_access.AppendLine("  WHERE   AL_SOURCE_CODE IN (4, 5, 7)                                     "); //SAS Host Terminal, SAS Machine Terminal, WCP Terminal
      m_str_sql_sas_event_physical_access.AppendLine("    AND   AL_SEVERITY    IN (1, 2, 3)                                     ");
      m_str_sql_sas_event_physical_access.AppendLine("    AND   AL_DATETIME    >= @pDateFrom                                    ");
      m_str_sql_sas_event_physical_access.AppendLine("    AND   AL_DATETIME    <  @pDateTo                                      ");
      m_str_sql_sas_event_physical_access.AppendLine("    AND   AL_ALARM_CODE  IN (" + _alarm_codes_physical_access_string + ") ");

    } // BuildQueries

    //------------------------------------------------------------------------------
    // PURPOSE : Gets the current operations schedule status and updates the table
    //           OPERATIONS_SCHEDULE_STATUS if it has changed.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime Now
    //
    //      - OUTPUT : 
    //          - OPERATIONS_ALLOWED_STATUS OperationsAllowedStatus
    //
    // RETURNS :
    //      - Boolean: TRUE if operations executed successfully
    //                 FALSE if any errors occurred
    //
    private static Boolean UpdateStatus(DateTime Now, out Boolean UpdatedTerminalsEnabled, out OPERATIONS_ALLOWED_STATUS OperationsAllowedStatus)
    {
      StringBuilder _str_sql;
      Boolean _operations_allowed;

      OperationsAllowedStatus = OPERATIONS_ALLOWED_STATUS.UNKNOWN;
      _operations_allowed = true;
      UpdatedTerminalsEnabled = true;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!OperationsSchedule.OperationsAllowed(Now, out _operations_allowed, out UpdatedTerminalsEnabled, _db_trx.SqlTransaction))
          {
            return false;
          }

          _str_sql = new StringBuilder();
          _str_sql.AppendLine(" UPDATE   OPERATIONS_SCHEDULE_STATUS             ");
          _str_sql.AppendLine("    SET   OSS_ALLOWED      = @pOperationsAllowed ");
          _str_sql.AppendLine("        , OSS_LAST_UPDATED = @pDateNow           ");
          _str_sql.AppendLine("  WHERE   OSS_ALLOWED     <> @pOperationsAllowed ");

          using (SqlCommand _cmd = new SqlCommand(_str_sql.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pOperationsAllowed", System.Data.SqlDbType.Bit).Value = _operations_allowed;
            _cmd.Parameters.Add("@pDateNow", System.Data.SqlDbType.DateTime).Value = Now;

            if (_cmd.ExecuteNonQuery() > 1)
            {
              return false;
            }
          }

          _str_sql = new StringBuilder();
          _str_sql.AppendLine(" UPDATE   TERMINAL_STATUS ");
          if (!UpdatedTerminalsEnabled)
          {
            _str_sql.AppendLine("    SET   TS_MACHINE_FLAGS = TS_MACHINE_FLAGS | @pFlag ");
          }
          else
          {
            _str_sql.AppendLine("    SET   TS_MACHINE_FLAGS = TS_MACHINE_FLAGS & (~@pFlag) ");
          }

          using (SqlCommand _cmd = new SqlCommand(_str_sql.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pFlag", System.Data.SqlDbType.Int).Value = TerminalStatusFlags.MACHINE_FLAGS.BLOCKED_AUTOMATICALLY;

            _cmd.ExecuteNonQuery();
          }
          _db_trx.Commit();


          OperationsAllowedStatus = _operations_allowed ? OPERATIONS_ALLOWED_STATUS.ALLOWED : OPERATIONS_ALLOWED_STATUS.NOT_ALLOWED;

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateStatus

    //------------------------------------------------------------------------------
    // PURPOSE : Inserts a new record in the table OPERATIONS_AFTER_HOURS.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime       DateFrom (OAH_FROM column)
    //          - SqlTransaction Trx
    //
    //      - OUTPUT : 
    //
    // RETURNS :
    //      - Boolean: TRUE if operations executed successfully
    //                 FALSE if any errors occurred
    //
    private static Boolean NewRecord(DateTime DateFrom, SqlTransaction Trx)
    {
      StringBuilder _str_sql;

      try
      {
        _str_sql = new StringBuilder();
        _str_sql.AppendLine("  INSERT INTO   OPERATIONS_AFTER_HOURS                ");
        _str_sql.AppendLine("              ( OAH_FROM                              ");
        _str_sql.AppendLine("              , OAH_EVENT_TYPE                        ");
        _str_sql.AppendLine("              , OAH_EVENT_TYPE_SENT                   ");
        _str_sql.AppendLine("              , OAH_CHANGES                           ");
        _str_sql.AppendLine("              , OAH_CHANGES_SENT                      ");
        _str_sql.AppendLine("              )                                       ");
        _str_sql.AppendLine("              VALUES                                  ");
        _str_sql.AppendLine("              ( @pDateFrom                            ");
        _str_sql.AppendLine("              , @pEventType                           ");
        _str_sql.AppendLine("              , @pEventType                           ");
        _str_sql.AppendLine("              , @pChanges                             ");
        _str_sql.AppendLine("              , @pChanges                             ");
        _str_sql.AppendLine("              )                                       ");

        using (SqlCommand _cmd = new SqlCommand(_str_sql.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pDateFrom", System.Data.SqlDbType.DateTime).Value = DateFrom;
          _cmd.Parameters.Add("@pEventType", System.Data.SqlDbType.BigInt).Value = 0;
          _cmd.Parameters.Add("@pChanges", System.Data.SqlDbType.Int).Value = 0;

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // NewRecord

    //------------------------------------------------------------------------------
    // PURPOSE : Updates a record in the table OPERATIONS_AFTER_HOURS.
    //
    //  PARAMS :
    //      - INPUT :
    //          - EVENT_TYPE     EventMask    (OAH_EVENT_TYPE column)
    //          - DateTime       DateFrom     (OAH_FROM column)
    //          - DateTime       LastChecked  (OAH_LAST_CHECKED column)
    //          - Boolean        UpdateEvents (update OAH_EVENT_TYPE and OAH_CHANGES columns)
    //          - Boolean        UpdateSent   (update OAH_EVENT_TYPE_SENT and OAH_CHANGES_SENT columns)
    //          - SqlTransaction Trx
    //
    //      - OUTPUT : 
    //
    // RETURNS :
    //      - Boolean: TRUE if operations executed successfully
    //                 FALSE if any errors occurred
    //
    private static Boolean UpdateRecord(EVENT_TYPE EventMask,
                                        DateTime DateFrom,
                                        DateTime LastChecked,
                                        Boolean UpdateEvents,
                                        Boolean UpdateSent,
                                        Boolean UpdateFirstEventDetected,
                                        SqlTransaction Trx)
    {
      StringBuilder _str_sql;

      _str_sql = new StringBuilder();

      if (UpdateEvents)
      {
        _str_sql.AppendLine(" UPDATE   OPERATIONS_AFTER_HOURS                     ");
        _str_sql.AppendLine("    SET   OAH_EVENT_TYPE           = @pEventMask     ");
        _str_sql.AppendLine("        , OAH_CHANGES              = OAH_CHANGES + 1 ");
        _str_sql.AppendLine("        , OAH_LAST_CHECKED         = @pLastChecked   ");

        if (UpdateSent)
        {
          _str_sql.AppendLine("      , OAH_EVENT_TYPE_SENT      = @pEventMask     ");
          _str_sql.AppendLine("      , OAH_CHANGES_SENT         = OAH_CHANGES + 1 ");
        }

        if (UpdateFirstEventDetected)
        {
          _str_sql.AppendLine("      , OAH_FIRST_EVENT_DETECTED = @pLastChecked   ");
        }

        _str_sql.AppendLine("  WHERE   OAH_FROM                 = @pDateFrom      ");
      }
      else
      {
        _str_sql.AppendLine(" UPDATE   OPERATIONS_AFTER_HOURS                     ");
        _str_sql.AppendLine("    SET   OAH_LAST_CHECKED         = @pLastChecked   ");
        _str_sql.AppendLine("  WHERE   OAH_FROM                 = @pDateFrom      ");
      }

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_str_sql.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pEventMask", System.Data.SqlDbType.BigInt).Value = (Int64)EventMask;
          _cmd.Parameters.Add("@pLastChecked", System.Data.SqlDbType.DateTime).Value = LastChecked;
          _cmd.Parameters.Add("@pDateFrom", System.Data.SqlDbType.DateTime).Value = DateFrom;

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // UpdateRecord

    //------------------------------------------------------------------------------
    // PURPOSE : Closes a record in the table OPERATIONS_AFTER_HOURS (sets OAH_TO column).
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime       DateFrom (OAH_FROM column)
    //          - DateTime       DateTo   (OAH_TO column)
    //          - SqlTransaction Trx
    //
    //      - OUTPUT : 
    //
    // RETURNS :
    //      - Boolean: TRUE if operations executed successfully
    //                 FALSE if any errors occurred
    //
    private static Boolean CloseRecord(DateTime DateFrom,
                                       DateTime DateTo,
                                       SqlTransaction Trx)
    {
      StringBuilder _str_sql;

      try
      {
        _str_sql = new StringBuilder();
        _str_sql.AppendLine("  UPDATE   OPERATIONS_AFTER_HOURS ");
        _str_sql.AppendLine("     SET   OAH_TO   = @pDateTo    ");
        _str_sql.AppendLine("   WHERE   OAH_FROM = @pDateFrom  ");

        using (SqlCommand _cmd = new SqlCommand(_str_sql.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pDateTo", System.Data.SqlDbType.DateTime).Value = DateTo;
          _cmd.Parameters.Add("@pDateFrom", System.Data.SqlDbType.DateTime).Value = DateFrom;

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // CloseRecord

    //------------------------------------------------------------------------------
    // PURPOSE : Check if any events occurred during an interval.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime       DateFrom    (interval start)
    //          - DateTime       DateTo      (interval end)
    //          - EVENT_TYPE     EventMask   (events found last time)
    //          - SqlTransaction Trx
    //
    //      - OUTPUT : 
    //          - EVENT_TYPE NewEventMask (events found now)
    //
    // RETURNS :
    //      - Boolean: TRUE if operations executed successfully
    //                 FALSE if any errors occurred
    //
    private static Boolean CheckEvents(DateTime DateFrom,
                                       DateTime DateTo,
                                       EVENT_TYPE EventMask,
                                       out EVENT_TYPE NewEventMask,
                                       SqlTransaction Trx)
    {
      Boolean _all_event_types_checked;
      Boolean _event_found;

      NewEventMask = EventMask;
      _event_found = false;

      //If all event types are already detected, do nothing
      _all_event_types_checked = true;

      foreach (EVENT_TYPE _type in Enum.GetValues(typeof(EVENT_TYPE)))
      {
        if (((EventMask & _type) == EVENT_TYPE.NONE) && _type != EVENT_TYPE.NONE)
        {
          _all_event_types_checked = false;
        }
      }

      if (_all_event_types_checked)
      {
        NewEventMask = EventMask;

        return true;
      }

      //Execute the appropriate queries, depending on EventMask
      //Logins
      if ((EventMask & EVENT_TYPE.LOGIN) == EVENT_TYPE.NONE)
      {
        if (!ExecuteQuery(DateFrom, DateTo, m_str_sql_login.ToString(), out _event_found, Trx))
        {

          return false;
        }

        if (_event_found)
        {
          NewEventMask = NewEventMask | EVENT_TYPE.LOGIN;
        }
      }

      //GUI Operations
      if ((EventMask & EVENT_TYPE.GUI_OPERATIONS) == EVENT_TYPE.NONE)
      {
        if (!ExecuteQuery(DateFrom, DateTo, m_str_sql_gui_operation.ToString(), out _event_found, Trx))
        {

          return false;
        }

        if (_event_found)
        {
          NewEventMask = NewEventMask | EVENT_TYPE.GUI_OPERATIONS;
        }
      }

      //Play Sessions
      if ((EventMask & EVENT_TYPE.PLAY_SESSION) == EVENT_TYPE.NONE)
      {
        if (!ExecuteQuery(DateFrom, DateTo, m_str_sql_play_session.ToString(), out _event_found, Trx))
        {

          return false;
        }

        if (_event_found)
        {
          NewEventMask = NewEventMask | EVENT_TYPE.PLAY_SESSION;
        }
      }

      //Cash Movements
      if ((EventMask & EVENT_TYPE.CASH_MOVEMENT) == EVENT_TYPE.NONE)
      {
        if (!ExecuteQuery(DateFrom, DateTo, m_str_sql_cash_movement.ToString(), out _event_found, Trx))
        {

          return false;
        }

        if (_event_found)
        {
          NewEventMask = NewEventMask | EVENT_TYPE.CASH_MOVEMENT;
        }
      }

      //SAS Events - Operator Access
      if ((EventMask & EVENT_TYPE.SAS_EVENT_OPERATOR_ACCESS) == EVENT_TYPE.NONE)
      {
        if (!ExecuteQuery(DateFrom, DateTo, m_str_sql_sas_event_operator_access.ToString(), out _event_found, Trx))
        {

          return false;
        }

        if (_event_found)
        {
          NewEventMask = NewEventMask | EVENT_TYPE.SAS_EVENT_OPERATOR_ACCESS;
        }
      }

      //SAS Events - Physical Access
      if ((EventMask & EVENT_TYPE.SAS_EVENT_PHYSICAL_ACCESS) == EVENT_TYPE.NONE)
      {
        if (!ExecuteQuery(DateFrom, DateTo, m_str_sql_sas_event_physical_access.ToString(), out _event_found, Trx))
        {

          return false;
        }

        if (_event_found)
        {
          NewEventMask = NewEventMask | EVENT_TYPE.SAS_EVENT_PHYSICAL_ACCESS;
        }
      }

      return true;
    } // CheckEvents

    //------------------------------------------------------------------------------
    // PURPOSE : Execute a query to check for a specific type of event.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime       DateFrom    (interval start)
    //          - DateTime       DateTo      (interval end)
    //          - String         SQLQuery    (query to execute)
    //          - SqlTransaction Trx
    //
    //      - OUTPUT : 
    //          - Boolean        EventFound  (TRUE if event was detected)
    //
    // RETURNS :
    //      - Boolean: TRUE if operations executed successfully
    //                 FALSE if any errors occurred
    //
    private static Boolean ExecuteQuery(DateTime DateFrom,
                                        DateTime DateTo,
                                        String SqlQuery,
                                        out Boolean EventFound,
                                        SqlTransaction Trx)
    {
      Object _obj;

      EventFound = false;

      try
      {
        using (SqlCommand _cmd = new SqlCommand(SqlQuery, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pDateFrom", System.Data.SqlDbType.DateTime).Value = DateFrom;
          _cmd.Parameters.Add("@pDateTo", System.Data.SqlDbType.DateTime).Value = DateTo;

          _obj = _cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            EventFound = true;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // ExecuteQuery

    //------------------------------------------------------------------------------
    // PURPOSE : Send an event notification e-mail.
    //
    //  PARAMS :
    //      - INPUT :
    //          - EVENT_TYPE EventMask  (events to notify)
    //          - DateTime   DateFrom   (interval start, just for notification purposes)
    //          - DateTime   DateTo     (interval end, just for notification purposes)
    //          - Boolean    FirstEmail (first notification -shows only some info-)
    //
    //      - OUTPUT : 
    //
    // RETURNS :
    //      - Boolean: TRUE if operations executed successfully
    //                 FALSE if any errors occurred
    //
    private static Boolean SendEmail(EVENT_TYPE EventMask,
                                     DateTime DateFrom,
                                     DateTime DateTo,
                                     Boolean FirstEmail)
    {
      Boolean _notifications_enabled;
      String _subject;
      String _recipients;
      String _site_name;
      String _prog_name;
      String _alarm_source;
      Int32 _site_id;
      StringBuilder _body;
      StringBuilder _alarm_description;

      if (EventMask == EVENT_TYPE.NONE)
      {

        return true;
      }

      _subject = GeneralParam.GetString("OperationsSchedule", "Subject");
      _recipients = GeneralParam.GetString("OperationsSchedule", "MailingList");
      _site_name = GeneralParam.GetString("Site", "Name");
      _site_id = GeneralParam.GetInt32("Site", "Identifier");

      //Build message body
      _body = new StringBuilder();
      _body.AppendLine(Resource.String("STR_OPERATIONS_SCHEDULE_MAIL_INTRO", _site_id.ToString("000") + " - " + _site_name));
      _body.AppendLine();

      if (!FirstEmail)
      {
        _body.AppendLine(Resource.String("STR_OPERATIONS_SCHEDULE_TIME_RANGE", DateFrom, DateTo));
        _body.AppendLine();
        _body.AppendLine(Resource.String("STR_OPERATIONS_SCHEDULE_ACTIVITY"));
      }
      else
      {
        _body.AppendLine(Resource.String("STR_OPERATIONS_SCHEDULE_ACTIVITY_AT", Format.CustomFormatDateTime(DateTo, false), DateTo.ToString(Format.TimeCustomFormatString(false))));
      }

      //Logins
      if ((EventMask & EVENT_TYPE.LOGIN) != EVENT_TYPE.NONE)
      {
        _body.AppendLine("  -" + Resource.String("STR_OPERATIONS_SCHEDULE_LOGINS"));
      }

      //GUI Operations
      if ((EventMask & EVENT_TYPE.GUI_OPERATIONS) != EVENT_TYPE.NONE)
      {
        _body.AppendLine("  -" + Resource.String("STR_OPERATIONS_SCHEDULE_GUI_OPERATIONS"));
      }

      //Play Sessions
      if ((EventMask & EVENT_TYPE.PLAY_SESSION) != EVENT_TYPE.NONE)
      {
        _body.AppendLine("  -" + Resource.String("STR_OPERATIONS_SCHEDULE_PLAY_SESSIONS"));
      }

      //Cash Movements
      if ((EventMask & EVENT_TYPE.CASH_MOVEMENT) != EVENT_TYPE.NONE)
      {
        _body.AppendLine("  -" + Resource.String("STR_OPERATIONS_SCHEDULE_CASH_MOVEMENTS"));
      }

      //SAS Events - Operator Access
      if ((EventMask & EVENT_TYPE.SAS_EVENT_OPERATOR_ACCESS) != EVENT_TYPE.NONE)
      {
        _body.AppendLine("  -" + Resource.String("STR_OPERATIONS_SCHEDULE_SAS_EVENTS_OPERATOR_ACCESS"));
      }

      //SAS Events - Physical Access
      if ((EventMask & EVENT_TYPE.SAS_EVENT_PHYSICAL_ACCESS) != EVENT_TYPE.NONE)
      {
        _body.AppendLine("  -" + Resource.String("STR_OPERATIONS_SCHEDULE_SAS_EVENTS_PHYSICAL_ACCESS"));
      }

      _body = _body.Replace("\\r\\n", "\r\n");

      // Build alarm description
      _prog_name = Resource.String("STR_OPERATIONS_SCHEDULE_PROG_NAME");
      _alarm_description = new StringBuilder();
      _alarm_description.Append(_prog_name + " - ");

      if (FirstEmail)
      {
        _alarm_description.AppendLine(Resource.String("STR_OPERATIONS_SCHEDULE_FIRST_ALARM"));
      }
      else
      {
        _alarm_description.AppendLine(Resource.String("STR_OPERATIONS_SCHEDULE_SECOND_ALARM"));
      }

      _alarm_description.AppendLine();
      _alarm_description.AppendLine(_body.ToString());

      _alarm_source = Environment.MachineName + @"\\" + m_service_name;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Generate alarm
          if (!Alarm.Register(AlarmSourceCode.System,
                              0,
                              _alarm_source,
                              (UInt32)AlarmCode.Service_OperationsAfterHours,
                              _alarm_description.ToString().TrimEnd(),
                              AlarmSeverity.Warning,
                              DateTime.MinValue,
                              _db_trx.SqlTransaction))
          {
            return false;
          }

          // Check if mailing notifications are enabled
          _notifications_enabled = GeneralParam.GetBoolean("OperationsSchedule", "MailingEnabled");

          if (_notifications_enabled)
          {
            // Insert message into MAILING_INSTANCES. It will be sent by Mailing service.
            if (!Mailing.DB_InsertInstance(DateTo,
                                           _prog_name,
                                           MAILING_PROGRAMMING_TYPE.GENERAL_TEXT,
                                           _recipients,
                                           _subject,
                                           _body.ToString().TrimEnd(),
                                           MAILING_INSTANCES_STATUS.READY,
                                           _db_trx.SqlTransaction))
            {
              return false;
            }
          }

          _db_trx.Commit();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // SendEmail

    //------------------------------------------------------------------------------
    // PURPOSE : Get the OSS_LAST_UPDATED column from the table OPERATIONS_SCHEDULE_STATUS.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //          - DateTime LastScheduleUpdate
    //
    // RETURNS :
    //      - Boolean: TRUE if operations executed successfully
    //                 FALSE if any errors occurred
    //
    private static Boolean GetLastScheduleUpdate(out DateTime LastScheduleUpdate)
    {
      StringBuilder _sb;
      Object _obj;

      LastScheduleUpdate = DateTime.MinValue;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine(" SELECT   OSS_LAST_UPDATED           ");
          _sb.AppendLine("   FROM   OPERATIONS_SCHEDULE_STATUS ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
          {
            _obj = _db_trx.ExecuteScalar(_cmd);

            if (_obj != null && _obj != DBNull.Value)
            {
              LastScheduleUpdate = (DateTime)_obj;

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetLastScheduleUpdate

    //------------------------------------------------------------------------------
    // PURPOSE : If the service was down, process the intervals to detect any activity
    //           out of schedule and notify accordingly.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime DateFrom (interval start)
    //          - DateTime DateTo   (interval end)
    //
    //      - OUTPUT : 
    //          - OPERATIONS_ALLOWED_STATUS OperationsAllowedStatus (status at the end of the interval)
    //          - EVENT_TYPE                EventMask               (events detected at the end of the interval)
    //          - EVENT_TYPE                FirstEmailEventMask     (events sent in the first e-mail notification)
    //          - DateTime                  RecordDateFrom          (OAH_FROM of the last interval)
    //
    // RETURNS :
    //      - Boolean: TRUE if operations executed successfully
    //                 FALSE if any errors occurred
    //
    private static Boolean ProcessMissingIntervals(DateTime DateFrom,
                                                   DateTime DateTo,
                                                   out OPERATIONS_ALLOWED_STATUS OperationsAllowedStatus,
                                                   out EVENT_TYPE EventMask,
                                                   out EVENT_TYPE FirstEmailEventMask,
                                                   out DateTime RecordDateFrom)
    {
      Boolean _operations_allowed;
      Boolean _new_operations_allowed;
      DateTime _interval_start;
      DateTime _interval_end;
      DateTime _last_check;
      Int32 _closing_hour;
      OperationsSchedule.TYPE_OPERATIONS_SCHEDULE_ITEM _schedule;

      _interval_start = DateFrom;
      _interval_end = DateFrom;
      _last_check = DateTime.MinValue;
      _operations_allowed = true;

      OperationsAllowedStatus = OPERATIONS_ALLOWED_STATUS.ALLOWED;
      EventMask = EVENT_TYPE.NONE;
      FirstEmailEventMask = EVENT_TYPE.NONE;
      RecordDateFrom = DateTime.MinValue;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        //Get initial schedule
        if (!OperationsSchedule.GetOperationsSchedule(DateFrom, out _schedule, _db_trx.SqlTransaction))
        {
          return false;
        }

        //Get closing hour
        _closing_hour = GeneralParam.GetInt32("WigosGUI", "ClosingTime", -1);

        if (_closing_hour == -1)
        {
          return false;
        }

        //Get initial status
        if (!OperationsSchedule.OperationsAllowed(DateFrom, _schedule, out _operations_allowed, _db_trx.SqlTransaction))
        {
          return false;
        }

        _new_operations_allowed = _operations_allowed;

        while (_interval_end < DateTo)
        {
          //Locate the next schedule change
          while (_new_operations_allowed == _operations_allowed && _interval_end < DateTo)
          {
            _interval_end = _interval_end.AddMinutes(1);

            //If workday changes, get the new schedule
            if (_interval_end.TimeOfDay == DateTime.MinValue.AddHours(_closing_hour).TimeOfDay)
            {
              if (!OperationsSchedule.GetOperationsSchedule(_interval_end, out _schedule, _db_trx.SqlTransaction))
              {
                return false;
              }
            }

            //Check operations allowed status
            if (!OperationsSchedule.OperationsAllowed(_interval_end, _schedule, out _new_operations_allowed, _db_trx.SqlTransaction))
            {
              return false;
            }

          }

          if (_interval_end < DateTo)
          {
            //If schedule was NOT ALLOWED
            if (!_operations_allowed)
            {
              //Process the interval
              if (!ProcessMissingRecord(_interval_start, _interval_end, false, out EventMask, out FirstEmailEventMask, _db_trx.SqlTransaction))
              {
                return false;
              }
            }

            _operations_allowed = _new_operations_allowed;
            _interval_start = _interval_end;
          }
        }

        if (!_operations_allowed)
        {
          if (!ProcessMissingRecord(_interval_start, DateTo, true, out EventMask, out FirstEmailEventMask, _db_trx.SqlTransaction))
          {
            return false;
          }
        }

        _db_trx.Commit();
      }

      OperationsAllowedStatus = _operations_allowed ? OPERATIONS_ALLOWED_STATUS.ALLOWED : OPERATIONS_ALLOWED_STATUS.NOT_ALLOWED;
      RecordDateFrom = _interval_start;

      return true;
    } // ProcessMissingIntervals

    //------------------------------------------------------------------------------
    // PURPOSE : If the service was down, process a missing record in OPERATIONS_AFTER_HOURS.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime       DateFrom     (interval start)
    //          - DateTime       DateTo       (interval end)
    //          - Boolean        IsOpenRecord (TRUE if the interval has not yet ended)
    //          - SqlTransaction Trx
    //
    //      - OUTPUT : 
    //          - EVENT_TYPE EventMask           (events detected at the end of the interval)
    //          - EVENT_TYPE FirstEmailEventMask (events sent in the first e-mail notification)
    //
    // RETURNS :
    //      - Boolean: TRUE if operations executed successfully
    //                 FALSE if any errors occurred
    //
    private static Boolean ProcessMissingRecord(DateTime DateFrom,
                                                DateTime DateTo,
                                                Boolean IsOpenRecord,
                                                out EVENT_TYPE EventMask,
                                                out EVENT_TYPE FirstEmailEventMask,
                                                SqlTransaction Trx)
    {
      StringBuilder _str_sql;
      Object _obj;
      EVENT_TYPE _events_to_report;
      Boolean _update_events;
      Boolean _update_sent;
      Boolean _update_first_event_detected;

      EventMask = EVENT_TYPE.NONE;
      FirstEmailEventMask = EVENT_TYPE.NONE;

      _update_events = false;
      _update_sent = false;
      _update_first_event_detected = false;

      try
      {
        _str_sql = new StringBuilder();
        _str_sql.AppendLine(" SELECT   OAH_EVENT_TYPE_SENT    ");
        _str_sql.AppendLine("   FROM   OPERATIONS_AFTER_HOURS ");
        _str_sql.AppendLine("  WHERE   OAH_FROM = @pDateFrom  ");

        using (SqlCommand _cmd = new SqlCommand(_str_sql.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pDateFrom", System.Data.SqlDbType.DateTime).Value = DateFrom;

          _obj = _cmd.ExecuteScalar();

          if (_obj == null || _obj == DBNull.Value)
          {
            //If there's no record, create it
            if (!NewRecord(DateFrom, Trx))
            {
              return false;
            }
          }
          else
          {
            FirstEmailEventMask = (EVENT_TYPE)(Int64)_obj;
          }

          //Close the record
          if (!IsOpenRecord)
          {
            if (!CloseRecord(DateFrom, DateTo, Trx))
            {
              return false;
            }
          }

          //Check events in the interval
          if (!CheckEvents(DateFrom, DateTo, EVENT_TYPE.NONE, out EventMask, Trx))
          {
            return false;
          }

          //Send e-mail if necessary
          if (EventMask != FirstEmailEventMask)
          {
            _update_events = true;
            _update_first_event_detected = true;

            //If it's an open record, send first e-mail
            if (IsOpenRecord && FirstEmailEventMask == EVENT_TYPE.NONE)
            {
              _update_sent = true;
              _events_to_report = EventMask;

              if (!SendEmail(_events_to_report, DateFrom, DateTo, true))
              {
                return false;
              }
            }

            //If it's a closed record, send second e-mail
            if (!IsOpenRecord)
            {
              _update_sent = true;
              _events_to_report = EventMask & ~FirstEmailEventMask;

              if (!SendEmail(_events_to_report, DateFrom, DateTo, false))
              {
                return false;
              }
            }
          }

          //Update record
          if (!UpdateRecord(EventMask, DateFrom, DateTo, _update_events, _update_sent, _update_first_event_detected, Trx))
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ProcessMissingRecord

    //------------------------------------------------------------------------------
    // PURPOSE : Forces an update of the table OPERATIONS_SCHEDULE_STATUS.
    //
    //  PARAMS :
    //      - INPUT :
    //          - OPERATIONS_ALLOWED_STATUS OperationsAllowed (OSS_ALLOWED column)
    //          - DateTime                  UpdateDate        (OSS_LAST_UPDATED column)
    //
    //      - OUTPUT : 
    //
    // RETURNS :
    //      - Boolean: TRUE if operations executed successfully
    //                 FALSE if any errors occurred
    //
    private static Boolean ForceUpdateStatus(OPERATIONS_ALLOWED_STATUS OperationsAllowed, DateTime LastUpdated)
    {
      StringBuilder _sb;
      Boolean _operations_allowed;

      switch (OperationsAllowed)
      {
        case OPERATIONS_ALLOWED_STATUS.ALLOWED:
          _operations_allowed = true;
          break;
        case OPERATIONS_ALLOWED_STATUS.NOT_ALLOWED:
          _operations_allowed = false;
          break;
        case OPERATIONS_ALLOWED_STATUS.UNKNOWN:
        default:
          return false;
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine(" UPDATE   OPERATIONS_SCHEDULE_STATUS             ");
          _sb.AppendLine("    SET   OSS_ALLOWED      = @pOperationsAllowed ");
          _sb.AppendLine("        , OSS_LAST_UPDATED = @pLastUpdated       ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
          {
            _cmd.Parameters.Add("@pOperationsAllowed", System.Data.SqlDbType.Bit).Value = _operations_allowed;
            _cmd.Parameters.Add("@pLastUpdated", System.Data.SqlDbType.DateTime).Value = LastUpdated;

            if (_db_trx.ExecuteNonQuery(_cmd) == 1)
            {
              _db_trx.Commit();

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ForceUpdateStatus

    //------------------------------------------------------------------------------
    // PURPOSE : Deletes old exceptions from table OPERATIONS_SCHEDULE.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DateTime Now
    //
    //      - OUTPUT : 
    //
    // RETURNS :
    //      - Boolean: TRUE if operations executed successfully
    //                 FALSE if any errors occurred
    //
    private static Boolean CleanOperationsSchedule(DateTime Now)
    {
      Int32 _closing_hour;
      DateTime _working_day;
      StringBuilder _sb;

      //Get closing hour
      _closing_hour = GeneralParam.GetInt32("WigosGUI", "ClosingTime", -1);

      if (_closing_hour == -1)
      {
        return false;
      }

      //Determine the right working day, depending on closing time
      _working_day = Misc.Opening(Now, _closing_hour, 0);

      //Clean the OPERATIONS_SCHEDULE table
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine(" DELETE FROM   OPERATIONS_SCHEDULE          ");
          _sb.AppendLine("       WHERE   OS_TYPE    = @pTypeDateRange ");
          _sb.AppendLine("         AND   OS_DATE_TO < @pDateTo        ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
          {
            _cmd.Parameters.Add("@pTypeDateRange", System.Data.SqlDbType.Int).Value = OPERATIONS_SCHEDULE_TYPE.DATE_RANGE;
            _cmd.Parameters.Add("@pDateTo", System.Data.SqlDbType.DateTime).Value = _working_day.Date;

            _db_trx.ExecuteNonQuery(_cmd);
          }

          _db_trx.Commit();

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // CleanOperationsSchedule

  }
}
