//------------------------------------------------------------------------------
// Copyright � 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Withdrawal.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Luis Tenorio
// 
// CREATION DATE: 05-AGO-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-AGO-2016 LTC    First release. Product Backlog Item 14990:TPV Televisa: Cashier Withdrawal
// 23-AGO-2016 LTC    Product Backlog Item 16742:TPV Televisa: Reconciliation modification of cashier vouchers
// 27-SEP-2016 ESE    Bug 17833: Televisa voucher, status cash
// 21-OCT-2016 ETP    Bug 19314: PinPad Card Payment Refactorized
// 07-NOV-2016 ATB    Bug 20036:Cajero: Error en el log al cerrar caja en el cajero.
// 20-JUN-2017 EOR    Bug 28292: WIGOS-2979 The report "Resumen de sesion de caja" is not displaying the field "Report con tarjeta bancaria" 
// 03-AUG-2017 EOR    Bug 29187: WIGOS-3944 Is not possible close a cash session by bankcard once the session was reopened
// 01-SEP-2017 ETP    WIGOS-3817 The cancellation of a withdrawal with credit card is not working properly
// 16-OCT-2017 LTC    WIGOS-5738 Metepec: Diference error bank card collection
// 29-NOV-2017 RAB    Bug 30942:WIGOS-6509 PinPad: wrong cashier movements when some vouchers are withdrawn
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;

namespace WSI.Common
{
  public static class Withdrawal
  {
    public static CurrencyExchangeType Type;
    public static Int32 Count;
    public static Int32 CountDelivered;   
    public static Decimal AmountTotal;
    public static DataTable DtPinpad;
    public static DataTable _dt;
    public static Boolean Reconciliated = false;
    public static Int32 ToRecon;
    public static Int32 CountDeliveredCutOff;
    public static Int32 CountNoDeliveredCutOff;

    //EOR 20-JUN-2017
    //LTC 16-OCT-2017
    public static void GetPinpanMovementsSummaryCashDesk(long CashierSesssionId, DateTime DateFrom, DateTime DateTo, ref Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData)
    {
      StringBuilder _sb;


      _sb = new StringBuilder();

      _sb.AppendLine("SELECT  (");
      _sb.AppendLine("          SELECT  COUNT(*)                                                      ");
      _sb.AppendLine("          FROM    pinpad_transactions                                           ");
      _sb.AppendLine("          INNER JOIN gui_users ON gu_user_id = pt_user_id                       ");
      _sb.AppendLine("          INNER JOIN account_operations ON ao_operation_id = pt_operation_id    ");
      _sb.AppendLine("          INNER JOIN pinpad_transactions_reconciliation ON ptc_id = pt_id       ");
      _sb.AppendLine("          WHERE pt_status = @pPinPadTransactionsStatus                          ");
      
      _sb.AppendLine("            AND ptc_reconciliate = 1                                            ");

      if (CashierSesssionId > 0)
      {
        _sb.AppendLine("          AND ao_cashier_session_id = @pCashierSesssionId                     ");
      }
      else
      {
        _sb.AppendLine("          AND pt_created >= @pDateFrom                                        ");
        _sb.AppendLine("          AND pt_created < @pDateTo                                           ");
      }

      _sb.AppendLine("        ) delivered,                                                            ");
      _sb.AppendLine("        (                                                                       ");
      _sb.AppendLine("          SELECT SUM(pt_total_amount)                                           ");
      _sb.AppendLine("          FROM pinpad_transactions                                              ");
      _sb.AppendLine("          INNER JOIN gui_users ON gu_user_id = pt_user_id                       ");
      _sb.AppendLine("          INNER JOIN account_operations ON ao_operation_id = pt_operation_id    ");
      _sb.AppendLine("          INNER JOIN pinpad_transactions_reconciliation ON ptc_id = pt_id       ");
      _sb.AppendLine("          WHERE pt_status = @pPinPadTransactionsStatus                          ");
      
      _sb.AppendLine("            AND ptc_reconciliate = 1                                            ");

      if (CashierSesssionId > 0)
      {
        _sb.AppendLine("          AND ao_cashier_session_id = @pCashierSesssionId                     ");
      }
      else
      {
        _sb.AppendLine("          AND pt_created >= @pDateFrom                                        ");
        _sb.AppendLine("          AND pt_created < @pDateTo                                           ");
      }

      _sb.AppendLine("        ) delivered_amount,                                                     ");
      _sb.AppendLine("        (                                                                       ");
      _sb.AppendLine("          SELECT COUNT(*)                                                       ");
      _sb.AppendLine("          FROM pinpad_transactions                                              ");
      _sb.AppendLine("          INNER JOIN gui_users ON gu_user_id = pt_user_id                       ");
      _sb.AppendLine("          INNER JOIN account_operations ON ao_operation_id = pt_operation_id    ");
      _sb.AppendLine("          INNER JOIN pinpad_transactions_reconciliation ON ptc_id = pt_id       ");
      _sb.AppendLine("          WHERE pt_status = @pPinPadTransactionsStatus                          ");
      
      _sb.AppendLine("            AND ptc_reconciliate = 0                                            ");

      if (CashierSesssionId > 0)
      {
        _sb.AppendLine("          AND ao_cashier_session_id = @pCashierSesssionId                     ");
      }
      else
      {
        _sb.AppendLine("          AND pt_created >= @pDateFrom                                        ");
        _sb.AppendLine("          AND pt_created < @pDateTo                                           ");
      }

      _sb.AppendLine("        ) +                                                                     ");
      _sb.AppendLine("        (                                                                       ");
      _sb.AppendLine("          SELECT COUNT(*)                                                       ");
      _sb.AppendLine("          FROM pinpad_transactions                                              ");
      _sb.AppendLine("          INNER JOIN gui_users ON gu_user_id = pt_user_id                       ");
      _sb.AppendLine("          INNER JOIN account_operations ON ao_operation_id = pt_operation_id    ");
      _sb.AppendLine("          WHERE pt_status = @pPinPadTransactionsStatus                          ");
      
      _sb.AppendLine("            AND NOT EXISTS(SELECT 1                                             ");
      _sb.AppendLine("                          FROM pinpad_transactions_reconciliation               ");
      _sb.AppendLine("                          WHERE ptc_id = pt_id)                                 ");

      if (CashierSesssionId > 0)
      {
        _sb.AppendLine("          AND ao_cashier_session_id = @pCashierSesssionId                     ");
      }
      else
      {
        _sb.AppendLine("          AND pt_created >= @pDateFrom                                        ");
        _sb.AppendLine("          AND pt_created < @pDateTo                                           ");
      }

      _sb.AppendLine("        ) no_delivered,                                                         ");
      _sb.AppendLine("        (                                                                       ");
      _sb.AppendLine("          SELECT ISNULL(SUM(pt_total_amount),0)                                 ");
      _sb.AppendLine("          FROM pinpad_transactions                                              ");
      _sb.AppendLine("          INNER JOIN gui_users ON gu_user_id = pt_user_id                       ");
      _sb.AppendLine("          INNER JOIN account_operations ON ao_operation_id = pt_operation_id    ");
      _sb.AppendLine("          INNER JOIN pinpad_transactions_reconciliation ON ptc_id = pt_id       ");
      _sb.AppendLine("          WHERE pt_status = @pPinPadTransactionsStatus                          ");
      
      _sb.AppendLine("            AND ptc_reconciliate = 0                                            ");

      if (CashierSesssionId > 0)
      {
        _sb.AppendLine("          AND ao_cashier_session_id = @pCashierSesssionId                     ");
      }
      else
      {
        _sb.AppendLine("          AND pt_created >= @pDateFrom                                        ");
        _sb.AppendLine("          AND pt_created < @pDateTo                                           ");
      }

      _sb.AppendLine("        ) +                                                                     ");
      _sb.AppendLine("        (                                                                       ");
      _sb.AppendLine("          SELECT ISNULL(SUM(pt_total_amount),0)                                 ");
      _sb.AppendLine("          FROM pinpad_transactions                                              ");
      _sb.AppendLine("          INNER JOIN gui_users ON gu_user_id = pt_user_id                       ");
      _sb.AppendLine("          INNER JOIN account_operations ON ao_operation_id = pt_operation_id    ");
      _sb.AppendLine("          WHERE pt_status = @pPinPadTransactionsStatus                          ");
      
      _sb.AppendLine("            AND NOT EXISTS(SELECT 1                                             ");
      _sb.AppendLine("                          FROM pinpad_transactions_reconciliation               ");
      _sb.AppendLine("                          WHERE ptc_id = pt_id)                                 ");

      if (CashierSesssionId > 0)
      {
        _sb.AppendLine("          AND ao_cashier_session_id = @pCashierSesssionId                     ");
      }
      else
      {
        _sb.AppendLine("          AND pt_created >= @pDateFrom                                        ");
        _sb.AppendLine("          AND pt_created < @pDateTo                                           ");
      }

      _sb.AppendLine("        ) no_delivered_amount                                                   ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_command.Parameters.Add("@pPinPadTransactionsStatus", SqlDbType.Int).Value = PinPad.PinPadTransactions.STATUS.APPROVED;


            if (CashierSesssionId > 0)
            {
              _sql_command.Parameters.Add("@pCashierSesssionId", SqlDbType.BigInt).Value = CashierSesssionId;
            }
            else
            {
              _sql_command.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = DateFrom;
              _sql_command.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = DateTo;
            }

            using (SqlDataReader _reader = _sql_command.ExecuteReader())
            {
              if (_reader.Read())
              {
                CashierSessionData.vouchers_delivered = _reader[0] == DBNull.Value ? 0 : (Int32)_reader[0];
                CashierSessionData.vouchers_no_delivered = _reader[2] == DBNull.Value ? 0 : (Int32)_reader[2];

                if(CashierSessionData.m_cashier_session_has_pinpad_operations)
                {
                  CashierSessionData.vouchers_delivered_amount = 0;
                  CashierSessionData.vouchers_no_delivered_amount = 0;
                }
                else
                {
                  CashierSessionData.vouchers_delivered_amount = _reader[1] == DBNull.Value ? 0 : Convert.ToDecimal(_reader[1]);
                  CashierSessionData.vouchers_no_delivered_amount = _reader[3] == DBNull.Value ? 0 : Convert.ToDecimal(_reader[3]);
                }
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    public static void GetPinpanMovements(long CashierSesssionId)
    {
      StringBuilder _sb;
      DataTable _dt;

      Count = 0;
      AmountTotal = 0;
      Withdrawal.DtPinpad = null;
      Withdrawal.CountDelivered = 0;

      try
      {

        _dt = new DataTable();

        _sb = new StringBuilder();
        _sb.AppendLine("     SELECT                                                                     ");
        _sb.AppendLine("          T.pt_created,                                                         ");
        _sb.AppendLine("          T.pt_id,                                                              ");
        _sb.AppendLine("          T.pt_operation_id,                                                    ");
        _sb.AppendLine("          T.pt_total_amount,                                                    ");
        _sb.AppendLine("          T.pt_card_number,                                                     ");
        _sb.AppendLine("          U.gu_full_name,                                                       ");
        _sb.AppendLine("          U.gu_user_id                                                          ");
        _sb.AppendLine("     FROM pinpad_transactions T                                                 ");
        _sb.AppendLine("     INNER JOIN gui_users U                                                     ");
        _sb.AppendLine("          ON U.gu_user_id = T.pt_user_id                                        ");
        _sb.AppendLine("     INNER JOIN account_operations A                                            ");
        _sb.AppendLine("          ON A.ao_operation_id = T.pt_operation_id                              ");
        _sb.AppendLine("     WHERE T.pt_status = " + WSI.Common.PinPad.PinPadTransactions.STATUS.APPROVED.GetHashCode().ToString() + "");
        _sb.AppendLine("     AND A.ao_cashier_session_id = " + CashierSesssionId);
        _sb.AppendLine("     AND NOT EXISTS(                                                            ");
        _sb.AppendLine("        SELECT 1 FROM pinpad_transactions_reconciliation WHERE ptc_id = T.pt_id ");
        _sb.AppendLine("     )                                                                          ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            using (SqlDataAdapter _da = new SqlDataAdapter(_sql_command))
            {
              _da.Fill(_dt);
            }
          }
        }

        // ATB 07-NOV-2016 Bug 20036:Cajero: Error en el log al cerrar caja en el cajero.
        // Avoids a log error when the system tries to converto a dbnulled value
        if (_dt != null && _dt.Compute("SUM(pt_total_amount)", "") != DBNull.Value)
        {
          Count = _dt.Rows.Count;
          AmountTotal = Convert.ToDecimal(_dt.Compute("SUM(pt_total_amount)", ""));
          DtPinpad = _dt;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    //LTC 16-OCT-2017
    public static void GetPinpadMovementsNoReconciliated(long CashierSesssionId)
    {
      StringBuilder _sb;
      DataTable _dt;
      Withdrawal.ToRecon = 0;

      try
      {

        _dt = new DataTable();

        _sb = new StringBuilder();
        _sb.AppendLine("     SELECT                                                                     ");
        _sb.AppendLine("          COUNT(*)                                                              ");
        _sb.AppendLine("     FROM pinpad_transactions T                                                 ");
        _sb.AppendLine("     INNER JOIN gui_users U                                                     ");
        _sb.AppendLine("          ON U.gu_user_id = T.pt_user_id                                        ");
        _sb.AppendLine("     INNER JOIN account_operations A                                            ");
        _sb.AppendLine("          ON A.ao_operation_id = T.pt_operation_id                              ");
        _sb.AppendLine("     INNER JOIN pinpad_transactions_reconciliation R                            ");
        _sb.AppendLine("          ON R.ptc_id = T.pt_id                                                 ");
        _sb.AppendLine("     WHERE T.pt_status = " + WSI.Common.PinPad.PinPadTransactions.STATUS.APPROVED.GetHashCode().ToString() + "");
        
        _sb.AppendLine("     AND R.ptc_reconciliate = 0                                                 ");
        _sb.AppendLine("     AND R.ptc_drawal_status = 0                                                ");     //LTC 23-AGO-2016
        _sb.AppendLine("     AND A.ao_cashier_session_id = " + CashierSesssionId                         );     

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            Withdrawal.ToRecon = (Int32)_sql_command.ExecuteScalar();
          }
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    /// <summary>
    /// Insert Reconciliation Movements
    /// </summary>
    /// <param name="OperationId"></param>
    /// <returns></returns>
    public static Boolean InsertReconciliationMovements(Int64 OperationId) //03-AUG-2017
    {
      StringBuilder _sb;
      DateTime _date;
      Int32 _nr;

      Count = 0;
      AmountTotal = 0;
      _date = DateTime.Now;

      try
      {
        _sb = new StringBuilder();

        if (Withdrawal.DtPinpad == null || DtPinpad.Rows.Count == 0)
        {
          return true;
        }

        foreach (DataRow _row in DtPinpad.Rows)
        {
          _row.SetAdded();
        }

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb.AppendLine(" INSERT INTO   PINPAD_TRANSACTIONS_RECONCILIATION ");
          _sb.AppendLine("             ( PTC_DRAWAL                         ");
          _sb.AppendLine("             , PTC_USER_ID                        ");
          _sb.AppendLine("             , PTC_ID                             ");
          _sb.AppendLine("             , PTC_RECONCILIATE                   ");
          _sb.AppendLine("             , PTC_DRAWAL_STATUS                  ");
          _sb.AppendLine("             , PTC_CODE                           ");
          _sb.AppendLine("             )                                    ");
          _sb.AppendLine("      VALUES                                      ");
          _sb.AppendLine("             (    @pDate                          ");
          _sb.AppendLine("             ,    @pUserID                        ");
          _sb.AppendLine("             ,    @pTransactionId                 ");
          _sb.AppendLine("             ,    @pReconciliate                  ");
          _sb.AppendLine("             ,    @pDrawalStatus                  ");
          _sb.AppendLine("             ,    @pOperationID                   ");
          _sb.AppendLine("             )                                    ");


          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {

            _sql_cmd.Parameters.Add("@pUserID", SqlDbType.Int).SourceColumn = "GU_USER_ID";
            _sql_cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).SourceColumn = "PT_ID";

            _sql_cmd.Parameters.Add("@pDate", SqlDbType.DateTime).Value = WGDB.Now;
            _sql_cmd.Parameters.Add("@pReconciliate", SqlDbType.Bit).Value = Withdrawal.Reconciliated;
            _sql_cmd.Parameters.Add("@pDrawalStatus", SqlDbType.Bit).Value = Withdrawal.Reconciliated;
            _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter())
            {
              _sql_da.InsertCommand = _sql_cmd;
              _sql_da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
              _sql_da.UpdateBatchSize = 500;
              _sql_da.ContinueUpdateOnError = true;

              _nr = _sql_da.Update(DtPinpad);

              if (_nr == DtPinpad.Rows.Count)
              {
                _db_trx.Commit();
                return true;
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertReconciliationMovements

    /// <summary>
    /// Delete Reconciliation Movements
    /// </summary>
    /// <param name="OperationCode"></param>
    /// <param name="TrxOk"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static bool DeleteReconciliationMovements(Int64 OperationCode, Boolean TrxOk, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      if (!TrxOk)
      {
        return false;
      }

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("  DELETE pinpad_transactions_reconciliation ");
        _sb.AppendLine("  WHERE ptc_code = @pOperationCode          ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pOperationCode", SqlDbType.BigInt).Value = OperationCode;

          _cmd.ExecuteNonQuery();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }

      return true;
    } // DeleteReconciliationMovements

    //LTC 16-OCT-2017
    public static void GetPinpanMovementsCutOff(long CashierSesssionId)
    {
      StringBuilder _sb;
      DataTable _dt;

      Withdrawal.CountDeliveredCutOff = 0;
      Withdrawal.CountNoDeliveredCutOff = 0;

      try
      {

        _dt = new DataTable();

        _sb = new StringBuilder();

        String _Query = @"SELECT
                      (  
                     --// ENTREGADOS CORTE
                     SELECT                                                                     
                          COUNT(*)
                     FROM pinpad_transactions T                                                 
                      INNER JOIN gui_users U                                                     
                        ON U.gu_user_id = T.pt_user_id                                        
                      INNER JOIN account_operations A                                            
                        ON A.ao_operation_id = T.pt_operation_id                              
                      INNER JOIN pinpad_transactions_reconciliation R                            
                        ON R.ptc_id = T.pt_id                                                 
                     WHERE T.pt_status = " + WSI.Common.PinPad.PinPadTransactions.STATUS.APPROVED.GetHashCode().ToString() + " " +
                       
                       @" AND R.ptc_reconciliate = 1 " +
                       @" AND A.ao_cashier_session_id = " + CashierSesssionId + " " +
                    @" )Given, 
                  (
                     --// NO ENTREGADOS CORTE
                     SELECT                                                                     
                          COUNT(*) AS A
                     FROM pinpad_transactions T                                                 
                      INNER JOIN gui_users U                                                     
                        ON U.gu_user_id = T.pt_user_id                                        
                      INNER JOIN account_operations A                                            
                        ON A.ao_operation_id = T.pt_operation_id                              
                      INNER JOIN pinpad_transactions_reconciliation R                            
                        ON R.ptc_id = T.pt_id                                                 
                     WHERE T.pt_status = " + WSI.Common.PinPad.PinPadTransactions.STATUS.APPROVED.GetHashCode().ToString() + " " +
                       
                       @" AND R.ptc_reconciliate = 0 " +
                       @" AND A.ao_cashier_session_id = " + CashierSesssionId + " " +
                    @" ) +
                  (
                     --// NUEVOS MOVIMIENTOS
                     SELECT                                                                     
                          COUNT(*) AS B
                     FROM pinpad_transactions T                                                 
                      INNER JOIN gui_users U                                                     
                        ON U.gu_user_id = T.pt_user_id                                        
                      INNER JOIN account_operations A                                            
                        ON A.ao_operation_id = T.pt_operation_id                                                                    
                     WHERE T.pt_status = " + WSI.Common.PinPad.PinPadTransactions.STATUS.APPROVED.GetHashCode().ToString() + " " +
                       
                       @" AND A.ao_cashier_session_id = " + CashierSesssionId + " " +
                       @" AND NOT EXISTS(                                                           
                        SELECT 1 FROM pinpad_transactions_reconciliation WHERE ptc_id = T.pt_id
                      )   
                  )NoGiven ";

        _sb.Append(_Query);

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            using (SqlDataAdapter _da = new SqlDataAdapter(_sql_command))
            {
              _da.Fill(_dt);
            }
          }
        }

        if (_dt != null)
        {
          Withdrawal.CountDeliveredCutOff = Convert.ToInt32(_dt.Rows[0]["Given"].ToString().Trim());
          Withdrawal.CountNoDeliveredCutOff = Convert.ToInt32(_dt.Rows[0]["NoGiven"].ToString().Trim());
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }


  }

} // WSI.Common
