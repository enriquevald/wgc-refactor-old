﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AccountOperation.cs
// 
//   DESCRIPTION: AccountOperation class
// 
//        AUTHOR: Ferran Ortner
// 
// CREATION DATE: 10-NOV-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-NOV-2015 FOS    First version.
// 19-JAN-2016 FOS    Backlog Item 7969: Apply Taxes in handpay authorization
// ----------- ------ ----------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common
{
  public class AccountOperations
  {

    #region "Property"

    private int cashier_session_id;
    private int user_id;
    private int authorized_user_id;
    private long reason_id;
    private string comments;

    public string Comments
    {
      get { return comments; }
      set { comments = value; }
    }


    public Int64 ReasonId
    {
      get { return reason_id; }
      set { reason_id = value; }
    }


    public int AuthorizedUserId
    {
      get { return authorized_user_id; }
      set { authorized_user_id = value; }
    }


    public int UserId
    {
      get { return user_id; }
      set { user_id = value; }
    }


    public int CashierSessionId
    {
      get { return cashier_session_id; }
      set { cashier_session_id = value; }
    }
    #endregion

    #region "Constructor"


    public AccountOperations()
    {
      this.CashierSessionId = 0;
      this.UserId = 0;
      this.AuthorizedUserId = 0;
    }

    public AccountOperations(int UserId, int AuthorizedUserId, int CashierSessionId, long ReasonId, string Comments)
    {
      this.CashierSessionId = CashierSessionId;
      this.UserId = UserId;
      this.AuthorizedUserId = AuthorizedUserId;
      this.ReasonId = ReasonId;
      this.Comments = Comments;
    }
    #endregion

    #region "Methods"

   
    public static bool GetReasonAndComment(Int64 AccountOperationId, out Int64  AccountOperationReasonId, out String AccountOperationReasonComment)
    {

      Boolean _get_reason_and_comment;
      StringBuilder _sb;

      AccountOperationReasonComment = string.Empty;
      AccountOperationReasonId = 0;
      _get_reason_and_comment = false;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine("   SELECT    AO_REASON_ID ");
          _sb.AppendLine("            ,AO_COMMENT   ");
          _sb.AppendLine("     FROM    ACCOUNT_OPERATIONS");
          _sb.AppendLine("    WHERE    AO_OPERATION_ID  = @pOperationId");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = AccountOperationId;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              AccountOperations _account_operations = new AccountOperations();
              if (_reader.Read())
              {
                AccountOperationReasonId = _reader[0] == DBNull.Value ? 0 : (Int64)_reader[0];
                AccountOperationReasonComment = _reader[1] == DBNull.Value ? "" : (String)_reader[1];
                _get_reason_and_comment = true;
              }
            }
      
          }
          _get_reason_and_comment = true;
        }
      }
     
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return _get_reason_and_comment;
    }

    #endregion

    #region "Old class Account Operation - moved from class Operation"
    public class Operation
    {
      public Int64 OperationId;
      public Int64 AccountId;
      public OperationCode Code;
      public Int64 CashierSessionId;
      public Int64 MbAccountId;
      public Currency Amount;
      public Currency Redeemable;
      public Currency NonRedemable;
      public Currency NonRedemable2;
      public Currency WonLock;
      public Int64 PromoId;
      public Currency PromoRedeemable;
      public Currency PromoNonRedeemable;
      public Int64 OperationData;
      public Boolean AutomaticallyPrinted;
      public Int64 UserId;
      public UndoStatus UndoStatus;
      public Int64 UndoOperationId;

      public Operation()
      {
        UndoStatus = UndoStatus.None;
        UndoOperationId = -1;
        OperationId = -1;
        AccountId = -1;
        CashierSessionId = -1;
        Amount = 0;
        PromoRedeemable = 0;
        PromoNonRedeemable = 0;
      }
    }
    #endregion

  }
}
