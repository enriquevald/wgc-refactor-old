﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AutoPrintVouchersManager.cs
// 
//   DESCRIPTION: Common Class for AutoPrintVouchersManager - Manager of AutoPrint threads and logic
//        AUTHOR: David Perelló
// 
// CREATION DATE: 04-MAY-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2018 JGC    First release.
//------------------------------------------------------------------------------

using WSI.Common.AutoPrint.BusinessLogic;

namespace WSI.Common.AutoPrint
{
  public class AutoPrintVouchersManager
  {
    #region Members
    private AutoPrintPendingPrintVouchersBL m_auto_print_pending_print_vouchers_BL;
    private AutoPrintCashOutReportBL m_auto_print_cash_out_report_BL;
    private AutoPrintPendingGenerationVouchersBL m_auto_print_pending_generation_vouchers_BL;
    #endregion // Members

    #region Properties
    public AutoPrintPendingPrintVouchersBL PendingPrintVouchers 
    {
      get 
      {
        if (m_auto_print_pending_print_vouchers_BL == null)
        {
          m_auto_print_pending_print_vouchers_BL = new AutoPrintPendingPrintVouchersBL();
        }

        return m_auto_print_pending_print_vouchers_BL;
      }
    }

    public AutoPrintCashOutReportBL CashOutReport
    {
      get
      {
        if (m_auto_print_cash_out_report_BL == null)
        {
          m_auto_print_cash_out_report_BL = new AutoPrintCashOutReportBL();
        }

        return m_auto_print_cash_out_report_BL;
      }
    }
    public AutoPrintPendingGenerationVouchersBL PendingGenerationVouchers
    {
      get
      {
        if (m_auto_print_pending_generation_vouchers_BL == null)
        {
          m_auto_print_pending_generation_vouchers_BL = new AutoPrintPendingGenerationVouchersBL();
        }

        return m_auto_print_pending_generation_vouchers_BL;
      }
    }
    #endregion // Properties

  } // AutoPrintVouchersManager
}
