﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AutoPrintCashOutReportDAO.cs
// 
//   DESCRIPTION: Common Class for AutoPrintCashOutReportDAO - DAO to access AUTO_PRINT_CASH_OUT_RECEIPT_REPORT
//        AUTHOR: David Perelló
// 
// CREATION DATE: 04-MAY-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2018 DPC    First release.
// 12-JUN-2018 MS     PBI 32946: WIGOS-10137 Philippines - Create Cashout Receipt Report
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common.AutoPrint.BusinessLogic;
using WSI.Common.AutoPrint.Entities;
using WSI.Common.AutoPrint.SQL;

namespace WSI.Common.AutoPrint.DAO
{
  public class AutoPrintCashOutReportDAO
  {
    public Boolean InsertCashOutReport(eCashOutReceiptData Voucher, Int64 VoucherId, Int64 PlaySessionId, AutoPrintConfig.AutoPrintStatus Status, SqlTransaction SqlTrx)
    {

      Int64 ProviderId;
      DateTime? VoucherDate;
      String VoucherDateTime;

      try
      {

        if (!GetCashOutRelatedFields(PlaySessionId, out ProviderId, SqlTrx))
        {
          return false;
        }


        using (SqlCommand _cmd = new SqlCommand(AutoPrintCashOutReportSQL.GetSQLInsertCashOutReport(), SqlTrx.Connection, SqlTrx))
        {

          VoucherDateTime = Voucher.Date + " " + Voucher.Time;
          VoucherDate = Convert.ToDateTime(VoucherDateTime);

          if (!MapperToBD(_cmd, VoucherId, PlaySessionId, Status, VoucherDate,  Voucher.Provider,  Voucher.TerminalName,  Voucher.PlayerName,  Voucher.ReceiptValue,
                          Voucher.TransactionNumber,  Voucher.ReceiptNumber,  ProviderId,  Voucher.AccountId,  Voucher.TerminalID))
          {

            return false;
          }

          if (_cmd.ExecuteNonQuery() > 1)
          {
            Log.Error(string.Format("Error in InsertCashOutReport: Cannot insert VoucherId: {0}", VoucherId));

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // InsertCashOutReport

    public Boolean UpdatePrintStatus(Int64 VoucherId, AutoPrintConfig.AutoPrintStatus Status, SqlTransaction SqlTrx)
    {


      try
      {
        using (SqlCommand _cmd = new SqlCommand(AutoPrintCashOutReportSQL.GetSQLUpdatePrintStatus(), SqlTrx.Connection, SqlTrx))
        {
          if (!MapperToBD(_cmd, VoucherId, -1, Status, null, null, null, null, -1, -1, -1, -1, -1, -1))
          {

            return false;
          }

          if (_cmd.ExecuteNonQuery() > 1)
          {
            Log.Error(string.Format("Error in UpdatePrintStatus: Cannot update VoucherId: {0}", VoucherId));

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // UpdatePrintStatus

    #region Private methods

    /// <summary>
    /// Mapper object to BD
    /// </summary>
    /// <param name="SqlCmd"></param>
    /// <param name="VoucherType"></param>
    /// <returns></returns>
    private Boolean MapperToBD(SqlCommand SqlCmd, Int64 VoucherId, Int64 PlaySessionId, AutoPrintConfig.AutoPrintStatus Status,
                               DateTime? VoucherDate, String Provider, String Egm, String AccountHolder, Decimal Amount,
                               Int64 TransactionNumber, Int64 ReceiptNumber, Int64 ProviderId, Int64 AccountId, Int64 TerminalId)
    {
      try
      {
        if (VoucherId > -1)
        {
          SqlCmd.Parameters.Add("@pVoucherId", SqlDbType.BigInt).Value = VoucherId;
        }

        if (PlaySessionId > -1)
        {
          SqlCmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;
        }

        if (Status != null)
        {
          SqlCmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Status;
        }

        if (VoucherDate != null)
        {
          SqlCmd.Parameters.Add("@pVoucherDate", SqlDbType.DateTime).Value = VoucherDate;
        }

        if (Provider != null)
        {
          SqlCmd.Parameters.Add("@pProvider", SqlDbType.NVarChar).Value = Provider;
        }

        if (Egm != null)
        {
          SqlCmd.Parameters.Add("@pEgm", SqlDbType.NVarChar).Value = Egm;
        }

        if (AccountHolder != null)
        {
          SqlCmd.Parameters.Add("@pAccountHolder", SqlDbType.NVarChar).Value = AccountHolder;
        }

        if (Amount != null)
        {
          SqlCmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = Amount;
        }

        if (TransactionNumber > -1)
        {
          SqlCmd.Parameters.Add("@pTransactionNumber", SqlDbType.BigInt).Value = TransactionNumber;
        }

        if (ReceiptNumber > -1)
        {
          SqlCmd.Parameters.Add("@pReceiptNumber", SqlDbType.BigInt).Value = ReceiptNumber;
        }

        if (ProviderId > -1)
        {
          SqlCmd.Parameters.Add("@pProviderId", SqlDbType.BigInt).Value = ProviderId;
        }

        if (AccountId > -1)
        {
          SqlCmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
        }

        if (TerminalId > -1)
        {
          SqlCmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;
        }


        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // MapperToBD

    private Boolean GetCashOutRelatedFields(Int64 PlaySessionId, out Int64 ProviderId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _table;

      ProviderId = -1;



      _table = new DataTable();
      _sb = new StringBuilder();

      _sb.AppendLine("  SELECT TE_PROV_ID AS PROVIDER                    ");
      _sb.AppendLine("    FROM [DBO].[PLAY_SESSIONS]                         ");                       
      _sb.AppendLine("  INNER JOIN [DBO].[TERMINALS]                         ");
      _sb.AppendLine("    ON PS_TERMINAL_ID = TE_TERMINAL_ID                 ");                             
      _sb.AppendLine("  WHERE PS_PLAY_SESSION_ID = @pPlaySessionId           ");


      using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _sql_cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

        using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
        {
          _sql_da.Fill(_table);
        }
      }

      foreach (DataRow _row in _table.Rows)
      {
        ProviderId = (Int32)_row["PROVIDER"];
      }

      return true;

    }
    #endregion // Private methods

  } // AutoPrintCashOutReportDAO
}
