﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AutoPrintPendingPrintVouchersDAO.cs
// 
//   DESCRIPTION: Common Class for AutoPrintPendingPrintVouchersDAO - DAO to access AUTO_PRINT_PENDING_PRINT_VOUCHER
//        AUTHOR: David Perelló
// 
// CREATION DATE: 04-MAY-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2018 DPC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WSI.Common.AutoPrint.BusinessLogic;
using WSI.Common.AutoPrint.Entities;
using WSI.Common.AutoPrint.SQL;

namespace WSI.Common.AutoPrint.DAO
{
  public class AutoPrintPendingPrintVouchersDAO
  {
    #region Constants
    private const int SQL_COLUMN_APPPV_VOUCHER_ID = 0;
    private const int SQL_COLUMN_APPPV_TYPE = 1;
    private const int SQL_COLUMN_HTML = 2;
    #endregion // Constants

    #region Public methods
    /// <summary>
    /// Get block of voucher pending print
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <param name="ListPrintVouchers"></param>
    /// <returns></returns>
    public Boolean GetVouchersToPrint(AutoPrintConfig.VoucherType Type, SqlTransaction SqlTrx, out List<ePendingPrintVoucher> ListPrintVouchers)
    {
      ePendingPrintVoucher AutoPrintPendingPrintVoucher;

      try
      {
        ListPrintVouchers = new List<ePendingPrintVoucher>();

        using (SqlCommand _cmd = new SqlCommand(AutoPrintPendingPrintVouchersSQL.GetSQLAutoPrintPendingPrintVouchers(), SqlTrx.Connection, SqlTrx))
        {
          if (!MapperToBD(_cmd, -1, Type))
          {
            Log.Error("Error in GetVouchersToPrint: Cannot get vouchers");

            return false;
          }

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              if (!MapperToObject(_reader, out AutoPrintPendingPrintVoucher))
              {
                return false;
              }

              ListPrintVouchers.Add(AutoPrintPendingPrintVoucher);
            }

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      ListPrintVouchers = null;

      return false;

    } // GetVouchersToPrint

    /// <summary>
    /// Delete a voucher from print queue in AUTO_PRINT_PENDING_PRINT_VOUCHER
    /// </summary>
    /// <param name="VoucherId"></param>
    /// <param name="Type"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DeleteVouchersToPrint(Int64 VoucherId, AutoPrintConfig.VoucherType Type, SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand(AutoPrintPendingPrintVouchersSQL.GetSQLDeleteVouchersToPrint(), SqlTrx.Connection, SqlTrx))
        {
          if (!MapperToBD(_cmd, VoucherId, Type))
          {

            return false;
          }

          if (_cmd.ExecuteNonQuery() > 1)
          {
            Log.Error(string.Format("Error in DeleteVouchersToPrint: Cannot delete VoucherId: {0}", VoucherId));

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetVouchersToPrint

    /// <summary>
    /// Insert voucher in AUTO_PRINT_PENDING_PRINT_VOUCHER for print queue
    /// </summary>
    /// <param name="VoucherId"></param>
    /// <param name="Type"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean InsertVouchersToPrint(Int64 VoucherId, AutoPrintConfig.VoucherType Type, SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand(AutoPrintPendingPrintVouchersSQL.GetSQLInsertVouchersToPrint(), SqlTrx.Connection, SqlTrx))
        {
          if (!MapperToBD(_cmd, VoucherId, Type))
          {

            return false;
          }

          if (_cmd.ExecuteNonQuery() > 1)
          {
            Log.Error(string.Format("Error in InsertVouchersToPrint: Cannot insert VoucherId: {0}", VoucherId));

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetVouchersToPrint
    #endregion // Public methods

    #region Private mehtods
    /// <summary>
    /// Mapper object to BD
    /// </summary>
    /// <param name="SqlCmd"></param>
    /// <param name="VoucherType"></param>
    /// <returns></returns>
    private Boolean MapperToBD(SqlCommand SqlCmd, Int64 VoucherId, AutoPrintConfig.VoucherType VoucherType)
    {
      try
      {
        if (VoucherId > -1)
        {
          SqlCmd.Parameters.Add("@pVoucherId", SqlDbType.BigInt).Value = VoucherId;
        }

        if (VoucherType != null)
        {
          SqlCmd.Parameters.Add("@pType", SqlDbType.Int).Value = VoucherType;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // MapperToBD

    /// <summary>
    /// Mapper BD to object
    /// </summary>
    /// <param name="Reader"></param>
    /// <param name="PrintVouchers"></param>
    /// <returns></returns>
    private Boolean MapperToObject(SqlDataReader Reader, out ePendingPrintVoucher PrintVouchers)
    {
      try
      {
        PrintVouchers = new ePendingPrintVoucher()
        {
          VoucherId = (Int64)Reader[SQL_COLUMN_APPPV_VOUCHER_ID],
          Type = (AutoPrintConfig.VoucherType)Reader[SQL_COLUMN_APPPV_TYPE],
          Status = AutoPrintConfig.AutoPrintStatus.PendingPrint,
          Html = (String)Reader[SQL_COLUMN_HTML]
        };

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      PrintVouchers = null;

      return false;

    } // MapperToObject
    #endregion // Private mehtods
  } // AutoPrintPendingPrintVouchersDAO
}
