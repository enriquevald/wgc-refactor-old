﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AutoPrintPendingPrintVouchersDAO.cs
// 
//   DESCRIPTION: Common Class for AutoPrintPendingPrintVouchersDAO - DAO to access AUTO_PRINT_PENDING_GENERATION_VOUCHER
//        AUTHOR: Jesús García
// 
// CREATION DATE: 04-MAY-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2018 DPC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WSI.Common.AutoPrint.BusinessLogic;
using WSI.Common.AutoPrint.Entities;
using WSI.Common.AutoPrint.SQL;

namespace WSI.Common.AutoPrint.DAO
{
  public class AutoPrintPendingPrintGenerationVouchersDAO
  {
    #region Constants
    private const int SQL_COLUMN_APPGV_SESSION_ID = 0;
    private const int SQL_COLUMN_APPGV_TYPE = 1;
    private const int SQL_COLUMN_PS_FINISHED = 2;
    private const int SQL_COLUMN_PS_FINAL_BALANCE = 3;
    private const int SQL_COLUMN_TE_PROVIDER_ID = 4;
    private const int SQL_COLUMN_TE_SERIAL_NUMBER = 5;
    private const int SQL_COLUMN_AC_HOLDER_NAME = 6;
    private const int SQL_COLUMN_TE_TERMINAL_ID = 7;
    private const int SQL_COLUMN_TE_TERMINAL_NAME = 8;
    private const int SQL_COLUMN_AC_ACCOUNT_ID = 9;
    #endregion // Constants

    #region Public methods

    public Boolean InsertSessionId(long SessionId, AutoPrintConfig.VoucherType VoucherType, SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand(AutoPrintPendingGenerationVouchersSQL.GetSQLInsertAutoPrintPendingPrintGenerationVouchers(), SqlTrx.Connection, SqlTrx))
        {
          if (!MapperToBD(_cmd, VoucherType, SessionId))
          {

            return false;
          }

          if (_cmd.ExecuteNonQuery() > 1)
          {
            Log.Error(string.Format("Error in InsertSessionId: Cannot insert SessionId: {0}", SessionId));

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertSessionId

    public Boolean DeleteSessionId(long SessionId, SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand(AutoPrintPendingGenerationVouchersSQL.GetSQLDeleteAutoPrintPendingPrintGenerationVouchers(), SqlTrx.Connection, SqlTrx))
        {
          if (!MapperToBD(_cmd, null, SessionId))
          {

            return false;
          }

          if (_cmd.ExecuteNonQuery() > 1)
          {
            Log.Error(string.Format("Error in DeleteSessionId: Cannot delete SessionId: {0}", SessionId));

            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DeleteSessionId

    public Boolean GetSessionIds(AutoPrintConfig.VoucherType VoucherType, SqlTransaction SqlTrx, out List<eCashOutReceiptData> ListReceipts)
    {
      eCashOutReceiptData CashOutReceipt;

      try
      {
        ListReceipts = new List<eCashOutReceiptData>();

        using (SqlCommand _cmd = new SqlCommand(AutoPrintPendingGenerationVouchersSQL.GetSQLGetAutoPrintPendingPrintGenerationVouchers(), SqlTrx.Connection, SqlTrx))
        {
          if (!MapperToBD(_cmd, VoucherType, -1))
          {

            return false;
          }

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              if (!MapperToObject(_reader, out CashOutReceipt))
              {
                return false;
              }

              ListReceipts.Add(CashOutReceipt);
            }

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      ListReceipts = null;

      return false;
    } // GetSessionIds

    #endregion // Public methods

    #region Private mehtods
    /// <summary>
    /// Mapper object to BD
    /// </summary>
    /// <param name="SqlCmd"></param>
    /// <param name="VoucherType"></param>
    /// <returns></returns>
    private Boolean MapperToBD(SqlCommand SqlCmd, AutoPrintConfig.VoucherType? VoucherType, Int64 SessionId)
    {
      try
      {
        if (SessionId > -1)
        {
          SqlCmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = SessionId;
        }

        if (VoucherType != null)
        {
          SqlCmd.Parameters.Add("@pVoucherType", SqlDbType.Int).Value = (int)VoucherType;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // MapperToBD

    /// <summary>
    /// Mapper BD to object
    /// </summary>
    /// <param name="Reader"></param>
    /// <param name="PrintVouchers"></param>
    /// <returns></returns>
    private Boolean MapperToObject(SqlDataReader Reader, out eCashOutReceiptData CashOutReceipts)
    {
      try
      {
        CashOutReceipts = new eCashOutReceiptData()
        {
          SessionId = (Int64)Reader[SQL_COLUMN_APPGV_SESSION_ID],
          Type = (Int32)Reader[SQL_COLUMN_APPGV_TYPE],
          Date = ((DateTime)Reader[SQL_COLUMN_PS_FINISHED]).ToString("dd/MM/yyyy"),
          Time = ((DateTime)Reader[SQL_COLUMN_PS_FINISHED]).ToString("HH:mm"),
          ReceiptValue = (Currency)(Decimal)Reader[SQL_COLUMN_PS_FINAL_BALANCE],
          Provider = (String)Reader[SQL_COLUMN_TE_PROVIDER_ID],
          EGM = (String)Reader[SQL_COLUMN_TE_SERIAL_NUMBER],
          PlayerName = (Reader[SQL_COLUMN_AC_HOLDER_NAME] == DBNull.Value ? string.Empty : (string)Reader[SQL_COLUMN_AC_HOLDER_NAME]),
          TerminalID = (Int32)Reader[SQL_COLUMN_TE_TERMINAL_ID],
          TerminalName = (String)Reader[SQL_COLUMN_TE_TERMINAL_NAME],
          AccountId = (Int64)Reader[SQL_COLUMN_AC_ACCOUNT_ID],
          Header = AutoPrintConfig.CashoutVoucherHeader,
          Footer = AutoPrintConfig.CashoutVoucherFooter,
          Title = AutoPrintConfig.CashoutVoucherTitle,
          CompanyName = AutoPrintConfig.CashoutVoucherCompanyName,
          SiteName = AutoPrintConfig.CashoutVoucherSiteName,
          CashOutReceipt = AutoPrintConfig.CashoutVoucherReceipt,
          WorkStation = AutoPrintConfig.CashoutVoucherWorkStation
        };

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      CashOutReceipts = null;

      return false;

    } // MapperToObject
    #endregion // Private methods
  } // AutoPrintPendingPrintGenerationVouchersDAO
}
