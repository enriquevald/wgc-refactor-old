﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: eThreadKiller.cs
//                 
//   DESCRIPTION: Common Class for eThreadKiller - Entity
//        AUTHOR: David Perelló
// 
// CREATION DATE: 04-MAY-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2018 DPC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace WSI.Common.AutoPrint.Entities
{
  public class eThreadKiller
  {
    public Thread Printer_thread;
    public Boolean Kill;
  } // eThreadKiller
}
