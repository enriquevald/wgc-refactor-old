﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: eCashOutReceiptData.cs
// 
//   DESCRIPTION: Common Class for eCashOutReceiptData - Entity
//        AUTHOR: Jesús García
// 
// CREATION DATE: 04-MAY-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2018 JGC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.AutoPrint.Entities
{
  public class eCashOutReceiptData
  {
    public Int64 SessionId { get; set; }
    public Int32 Type { get; set; }
    public String Header { get; set; }
    public String Title { get; set; }
    public String CompanyName { get; set; }
    public String SiteName { get; set; }
    public String CashOutReceipt { get; set; }
    public String Date { get; set; }
    public String Time { get; set; }
    public Int64 ReceiptNumber { get; set; }
    public Int64 TransactionNumber { get; set; }
    public String Provider { get; set; }
    public String EGM { get; set; }
    public Currency ReceiptValue { get; set; }
    public String PlayerName { get; set; }
    public String WorkStation { get; set; }
    public String Footer { get; set; }
    public Int32 TerminalID { get; set; }
    public String TerminalName { get; set; }
    public Int64 AccountId { get; set; }
  } // eCashOutReceiptData
}
