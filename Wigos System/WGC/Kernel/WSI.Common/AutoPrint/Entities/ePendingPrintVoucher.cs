﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ePendingPrintVoucher.cs
// 
//   DESCRIPTION: Common Class for ePendingPrintVoucher - Entity
//        AUTHOR: David Perelló
// 
// CREATION DATE: 04-MAY-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2018 DPC    First release.
//------------------------------------------------------------------------------

using System;
using WSI.Common.AutoPrint.BusinessLogic;

namespace WSI.Common.AutoPrint.Entities
{
  public class ePendingPrintVoucher
  {
    public Int64 VoucherId { get; set; }
    public AutoPrintConfig.VoucherType Type { get; set; }

    public AutoPrintConfig.AutoPrintStatus Status { get; set; }

    public string Html { get; set; }

    public ePendingPrintVoucher()
    {
      this.Status = AutoPrintConfig.AutoPrintStatus.PendingPrint;
    }
  } // ePendingPrintVocuher
}
