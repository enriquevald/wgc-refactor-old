﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AutoPrintPendingPrintVouchersSQL.cs
// 
//   DESCRIPTION: Common Class for AutoPrintPendingPrintVouchersSQL - SQL queries for pending print vouchers
//        AUTHOR: David Perelló
// 
// CREATION DATE: 04-MAY-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2018 DPC    First release.
//------------------------------------------------------------------------------

using System.Text;
using WSI.Common.AutoPrint.BusinessLogic;

namespace WSI.Common.AutoPrint.SQL
{
  public class AutoPrintPendingPrintVouchersSQL
  {
    public static string GetSQLAutoPrintPendingPrintVouchers()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(string.Format("     SELECT   TOP {0} APPPV_VOUCHER_ID		                                      ", AutoPrintConfig.CashoutBlockTop));
      _sb.AppendLine("                      		, APPPV_TYPE								                                        ");
      _sb.AppendLine("                       		, CV_HTML										                                        ");
      _sb.AppendLine("                     FROM   AUTO_PRINT_PENDING_PRINT_VOUCHER APPPV						                ");
      _sb.AppendLine("               INNER JOIN   CASHIER_VOUCHERS CV ON CV.CV_VOUCHER_ID = APPPV.APPPV_VOUCHER_ID	");
      _sb.AppendLine("                    WHERE   APPPV_TYPE = @pType											                          ");
      _sb.AppendLine("                 ORDER BY   APPPV_VOUCHER_ID ASC										                          ");

      return _sb.ToString();
    } // GetSQLAutoPrintPendingPrintVouchers

    public static string GetSQLDeleteVouchersToPrint()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" DELETE FROM   AUTO_PRINT_PENDING_PRINT_VOUCHER ");
      _sb.AppendLine("       WHERE   APPPV_VOUCHER_ID = @pVoucherId   ");
      _sb.AppendLine("         AND   APPPV_TYPE	      = @pType        ");

      return _sb.ToString();
    } // GetSQLDeleteVouchersToPrint

    public static string GetSQLInsertVouchersToPrint()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" INSERT INTO   AUTO_PRINT_PENDING_PRINT_VOUCHER ");
      _sb.AppendLine("             ( APPPV_VOUCHER_ID                 ");
      _sb.AppendLine("             , APPPV_TYPE )                     ");
      _sb.AppendLine("      VALUES                                    ");
      _sb.AppendLine("             ( @pVoucherId                      ");
      _sb.AppendLine("             , @pType )                         ");

      return _sb.ToString();
    } // GetSQLInsertVouchersToPrint
  } // AutoPrintPendingPrintVouchersSQL
}
