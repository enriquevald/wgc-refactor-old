﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AutoPrintPendingGenerationVouchersSQL.cs
// 
//   DESCRIPTION: Common Class for AutoPrintPendingGenerationVouchersSQL - SQL queries for pending generation vouchers
//        AUTHOR: Jesús García
// 
// CREATION DATE: 04-MAY-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2018 JGC    First release.
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data;
using WSI.Common.AutoPrint.Entities;
using WSI.Common.AutoPrint.BusinessLogic;

namespace WSI.Common.AutoPrint.SQL
{
  public class AutoPrintPendingGenerationVouchersSQL
  {
    public static String GetSQLInsertAutoPrintPendingPrintGenerationVouchers()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" INSERT INTO   AUTO_PRINT_PENDING_GENERATION_VOUCHER ");
      _sb.AppendLine("             ( APPGV_SESSION_ID                      ");
      _sb.AppendLine("             , APPGV_TYPE )                          ");
      _sb.AppendLine("    VALUES   (                                       ");
      _sb.AppendLine("                @pSessionId                          ");
      _sb.AppendLine("             ,  @pVoucherType )                      ");
                                                                                                     
      return _sb.ToString();                                                                         
    } // GetSQLInsertAutoPrintPendingPrintGenerationVouchers                                                                                 
                                                                                                     
    public static String GetSQLDeleteAutoPrintPendingPrintGenerationVouchers()                       
    {                                                                                                
      StringBuilder _sb;                                                                             
                                                                                                     
      _sb = new StringBuilder();                                                                     
                                                                                                     
      _sb.AppendLine(" DELETE FROM   AUTO_PRINT_PENDING_GENERATION_VOUCHER ");
      _sb.AppendLine("       WHERE   APPGV_SESSION_ID = @pSessionId        ");
                                                                                                  
      return _sb.ToString();                                                                      
    } // GetSQLDeleteAutoPrintPendingPrintGenerationVouchers                                                                              
                                                                                                  
    public static String GetSQLGetAutoPrintPendingPrintGenerationVouchers()                       
    {                                                                                             
      StringBuilder _sb;                                                                          
                                                                                                  
      _sb = new StringBuilder();                                                                  
                                                                                                  
      _sb.AppendLine("        SELECT   APPGV.APPGV_SESSION_ID                                        ");
      _sb.AppendLine("               , APPGV.APPGV_TYPE                                              ");
      _sb.AppendLine("               , PS.PS_FINISHED                                                ");
      _sb.AppendLine("               , PS.PS_FINAL_BALANCE                                           ");
      _sb.AppendLine("               , TE.TE_PROVIDER_ID                                             ");
      _sb.AppendLine("               , TE.TE_SERIAL_NUMBER                                           ");      
      _sb.AppendLine("               , AC.AC_HOLDER_NAME                                             ");
      _sb.AppendLine("               , TE.TE_TERMINAL_ID                                             ");
      _sb.AppendLine("               , TE.TE_NAME                                                    ");
      _sb.AppendLine("               , AC.AC_ACCOUNT_ID                                              ");
      _sb.AppendLine("          FROM   AUTO_PRINT_PENDING_GENERATION_VOUCHER APPGV                   ");
      _sb.AppendLine("    INNER JOIN   PLAY_SESSIONS PS ON PS.PS_PLAY_SESSION_ID = APPGV_SESSION_ID  ");
      _sb.AppendLine("    INNER JOIN   TERMINALS TE ON TE.TE_TERMINAL_ID = PS.PS_TERMINAL_ID         ");
      _sb.AppendLine("    INNER JOIN   ACCOUNTS AC ON AC_ACCOUNT_ID = PS.PS_ACCOUNT_ID               ");
      _sb.AppendLine("         WHERE   APPGV.APPGV_TYPE = @pVoucherType                              ");
      _sb.AppendLine("      ORDER BY   APPGV.APPGV_SESSION_ID ASC 					                         ");

      return _sb.ToString();
    } // GetSQLGetAutoPrintPendingPrintGenerationVouchers
  } // AutoPrintPendingGenerationVouchersSQL
}