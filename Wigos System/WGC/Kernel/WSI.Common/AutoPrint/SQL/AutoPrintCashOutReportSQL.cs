﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AutoPrintCashOutReportSQL.cs
// 
//   DESCRIPTION: Common Class for AutoPrintCashOutReportSQL - SQL queries for cash out report
//        AUTHOR: David Perelló
// 
// CREATION DATE: 04-MAY-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2018 DPC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.AutoPrint.SQL
{
  public class AutoPrintCashOutReportSQL
  {
    public static string GetSQLInsertCashOutReport()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" INSERT INTO [DBO].[AUTO_PRINT_CASH_OUT_RECEIPT_REPORT] ");
      _sb.AppendLine("            ([APCURR_VOUCHER_ID]                        ");
      _sb.AppendLine("            ,[APCURR_PLAY_SESSION_ID]                   ");
      _sb.AppendLine("            ,[APCURR_STATUS]                            ");
      _sb.AppendLine("            ,[APCURR_DATETIME]                          ");
      _sb.AppendLine("            ,[APCURR_PROVIDER]                          ");
      _sb.AppendLine("            ,[APCURR_EGM]                               ");
      _sb.AppendLine("            ,[APCURR_TRANSACTION_NO]                    ");
      _sb.AppendLine("            ,[APCURR_RECEIPT_NO]                        ");
      _sb.AppendLine("            ,[APCURR_AMOUNT]                            ");
      _sb.AppendLine("            ,[APCURR_PROV_ID]                           ");
      _sb.AppendLine("            ,[APCURR_ACCOUNT_ID]                        ");
      _sb.AppendLine("            ,[APCURR_ACCOUNT_HOLDER]                    ");
      _sb.AppendLine("            ,[APCURR_TERMINAL_ID])                      ");
      _sb.AppendLine("     VALUES (                                           ");
      _sb.AppendLine(" 		         @pVoucherId                                ");
      _sb.AppendLine("            ,@pPlaySessionId                            ");
      _sb.AppendLine("            ,@pStatus                                   ");
      _sb.AppendLine("            ,@pVoucherDate                              ");
      _sb.AppendLine("            ,@pProvider                                 ");
      _sb.AppendLine("            ,@pEgm                                      ");
      _sb.AppendLine("            ,@pTransactionNumber                        ");
      _sb.AppendLine("            ,@pReceiptNumber                            ");
      _sb.AppendLine("            ,@pAmount                                   ");
      _sb.AppendLine("            ,@pProviderId                               ");
      _sb.AppendLine("            ,@pAccountId                                ");
      _sb.AppendLine("            ,@pAccountHolder                            ");
      _sb.AppendLine("            ,@pTerminalId )								              ");

      return _sb.ToString();
    } // GetSQLInsertPrintStatus

    public static string GetSQLUpdatePrintStatus()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine(" UPDATE   AUTO_PRINT_CASH_OUT_RECEIPT_REPORT ");
      _sb.AppendLine("    SET   APCURR_STATUS     = @pStatus      ");
      _sb.AppendLine("  WHERE   APCURR_VOUCHER_ID = @pVoucherId   ");

      return _sb.ToString();
    } // GetSQLUpdatePrintStatus
  } // AutoPrintCashOutReportSQL
}
