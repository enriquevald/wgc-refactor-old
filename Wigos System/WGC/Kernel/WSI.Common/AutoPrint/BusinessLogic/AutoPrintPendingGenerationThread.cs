﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AutoPrintPendingGenerationThread.cs
// 
//   DESCRIPTION: Common Class for AutoPrintPendingGenerationThread - Thread to generate voucher of cash outs
//        AUTHOR: Jesús García
// 
// CREATION DATE: 04-MAY-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2018 JGC    First release.
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;
using System.Diagnostics;
using WSI.Common.AutoPrint.Entities;

namespace WSI.Common.AutoPrint.BusinessLogic
{
  public class AutoPrintPendingGenerationThread
  {

    #region Members
    private static AutoPrintVouchersManager m_manager;
    private static Thread m_thread;
    #endregion // Members

    #region Constants
    private const String EXCEPTION_WCP_AUTOPRINT_TASK = "Exception into class AutoPrintPendingGeneration.";
    private const String EXCEPTION_MESSAGE = ". Message: ";
    private const Int32 WAIT_FOR_NEXT_TRY = (1 * 60 * 1000);
    #endregion // Constants

    #region Properties
    private static AutoPrintVouchersManager managerBL
    {
      get
      {
        if (m_manager == null)
        {
          m_manager = new AutoPrintVouchersManager();
        }

        return m_manager;
      }
    }
    #endregion // Properties

    #region Public methods
    /// <summary>
    /// Method to start AutoPrintPendingGeneration threads
    /// </summary>
    public static void Init()
    {
      m_thread = new Thread(AutoPrintPendingGenerationMainThread);
      m_thread.Name = "AutoPrintPendingGenerationThread";

      m_thread.Start();
    } // Init

    /// <summary>
    /// Method that executes thread and do the Business Logic to generate vouchers of cash outs
    /// </summary>
    private static void AutoPrintPendingGenerationMainThread()
    {
      Int32 _wait_hint;

      _wait_hint = 0;

      while (true)
      {
        try
        {
          Thread.Sleep(_wait_hint);

          if (!AutoPrintConfig.CashoutGenerationEnabled || !AutoPrintConfig.TypesEnabled.Contains(AutoPrintConfig.VoucherType.CashOutReceipt))
          {
            _wait_hint = 60000;

            continue;
          }

          if (!Services.IsPrincipal("WCP"))
          {
            _wait_hint = 1000;

            continue;
          }

          _wait_hint = 1000 * AutoPrintConfig.CashoutTimeSecondsPeriodForGetTickets;

          if (!managerBL.PendingGenerationVouchers.GeneratePendingPrintVouchers())
          {
            Log.Error(String.Format("AutoPrintPendingGeneration Error: Error in GeneratePendingPrintVouchers()"));

            continue;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          Log.Warning(String.Format(EXCEPTION_WCP_AUTOPRINT_TASK + "AutoPrintPendingGenerationThread" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
        }
      } // while 
    } // AutoPrintPendingGenerationMainThread
    #endregion // Public methods
  } // AutoPrintPendingGenerationThread
}