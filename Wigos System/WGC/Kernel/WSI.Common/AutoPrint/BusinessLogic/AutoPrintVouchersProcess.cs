﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AutoPrintVouchersProcess.cs
// 
//   DESCRIPTION: Common Class for AutoPrintVouchersProcess
//        AUTHOR: David Perelló
// 
// CREATION DATE: 04-MAY-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2018 DPC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using WSI.Common.AutoPrint.Entities;

namespace WSI.Common.AutoPrint.BusinessLogic
{
  public class AutoPrintVouchersProcess
  {
    #region Constants
    private const String DT_COLUMN_ACCOUNT_ID = "ACCOUNT_ID";
    private const String DT_COLUMN_AUTO_PRINT_STATUS_PENDING = "AUTO_PRINT_STATUS_PENDING";
    private const String DT_COLUMN_AUTO_PRINT_STATUS_NONE = "AUTO_PRINT_STATUS_NONE";

    private const String EXCEPTION_WCP_PROCESS_AUTOPRINT_STATUS_TASK = "Exception into class AutoPrintDrawProcessTickets.";
    private const String EXCEPTION_WCP_AUTOPRINT_TASK = "Exception into class AutoPrintDrawTickets.";
    private const String EXCEPTION_MESSAGE = ". Message: ";
    #endregion // Constants

    #region Members
    private static Thread m_thread_create_tickets;
    #endregion // Members

    #region Public methods
    /// <summary>
    /// Init Method for Create and Start the thread
    /// </summary>
    public static void Init()
    {
      try
      {
        m_thread_create_tickets = new Thread(MainProcessCreateTickets);
        m_thread_create_tickets.SetApartmentState(ApartmentState.STA);
        m_thread_create_tickets.Name = "DrawAutoPrintProcessTicketsThread";
        m_thread_create_tickets.Start();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // Init
    #endregion // Public methods

    #region Main Thread
    /// <summary>
    /// Thread for Creation Tickets
    /// </summary>
    private static void MainProcessCreateTickets()
    {
      Int32 _wait_hint = 0;

      while (true)
      {
        try
        {
          Thread.Sleep(_wait_hint);

          if (!AutoPrintConfig.CashoutPrintEnabled)
          {
            _wait_hint = 60000;

            continue;
          }

          if (!Services.IsPrincipal("WCP"))
          {
            _wait_hint = 1000;

            continue;
          }

          _wait_hint = 1000 * AutoPrintConfig.CashoutTimeSecondsPeriodForGetTickets;
          
          foreach (var _type in AutoPrintConfig.TypesEnabled)
          {
            if (!EnqueueVouchers(_type))
            {
              Log.Error(String.Format("AutoPrintVouchersProcess Error: Error in EnqueueCashOutVouchers()"));

              continue;
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          Log.Warning(String.Format(EXCEPTION_WCP_PROCESS_AUTOPRINT_STATUS_TASK + "DrawAutoPrintProcessTicketsThread" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
        }
      }
    }
    #endregion // Maint Thread

    #region "Private methods
    /// <summary>
    /// Enqueue a list of DrawTickets
    /// </summary>
    /// <param name="AccountsProcessedWithTickets"></param>
    /// <returns></returns>
    private static Boolean EnqueueVouchers(AutoPrintConfig.VoucherType Type)
    {
      AutoPrintPendingPrintVouchersBL _pending_print;
      List<ePendingPrintVoucher> _list_pending_vouchers;

      try
      {
        _pending_print = new AutoPrintPendingPrintVouchersBL();

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!_pending_print.GetVouchersToPrint(Type, _db_trx.SqlTransaction, out _list_pending_vouchers))
          {
            return false;
          }
        }

        if (!AutoPrintVouchers.m_dic_queues.ContainsKey(Type))
        {
          AutoPrintVouchers.m_dic_queues.Add(Type, new WaitableQueue());
        }

        foreach (ePendingPrintVoucher _voucher in _list_pending_vouchers)
        {
          if (!AutoPrintVouchers.m_dic_queues[Type].Contains(_voucher, "VoucherId"))
          {
            AutoPrintVouchers.m_dic_queues[Type].Enqueue(_voucher);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }
    #endregion // Private methods
  } // AutoPrintVouchersProcess
}
