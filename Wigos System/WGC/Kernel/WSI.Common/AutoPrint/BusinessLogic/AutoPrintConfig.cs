﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AutoPrintConfig.cs
//                 
//   DESCRIPTION: Common Class for AutoPrintConfig - AutoPrint configuration
//        AUTHOR: David Perelló
// 
// CREATION DATE: 04-MAY-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2018 DPC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.AutoPrint.BusinessLogic
{
  public class AutoPrintConfig
  {
    #region Enums
    public enum VoucherType
    {
      None = 0,
      CashOutReceipt = 1
    }

    public enum AutoPrintStatus
    {
      None = 0,
      PendingPrint = 1,
      Printing = 2,
      Printed = 3
    }

    public enum PrinterStatus
    {
      Other = 0,
      Unknown = 2,
      Idle = 3,
      Printing = 4,
      Warmup = 5,
      StoppedPrinting = 6,
      Offline = 7
    }
    #endregion // Enums

    #region Public methods
    public static Boolean CashoutGenerationEnabled
    {
      get
      {
        return GeneralParam.GetBoolean("AutoPrintVoucher", "CashOut.Generation.Enabled", false);
      }
    } // CashoutGenerationEnabled

    public static Boolean CashoutPrintEnabled
    {
      get
      {
        return GeneralParam.GetBoolean("AutoPrintVoucher", "CashOut.Print.Enabled", false);
      }
    } // CashoutPrintEnabled

    public static List<string> CashoutEnabledPrinters
    {
      get
      {
        return new List<string>(GeneralParam.GetString("AutoPrintVoucher", "CashOut.EnabledPrinters", string.Empty).Split('|'));
      }
    } // CashoutEnabledPrinters

    public static Int32 CashoutSecondsOfSleepTimeBetweenPrints
    {
      get
      {
        return GeneralParam.GetInt32("AutoPrintVoucher", "CashOut.SecondsOfSleepTimeBetweenPrints", 1, 1, 30);
      }
    } // CashoutSecondsOfSleepTimeBetweenPrints

    public static Int32 CashoutTimeSecondsPeriodForGetTickets
    {
      get
      {
        return GeneralParam.GetInt32("AutoPrintVoucher", "CashOut.TimeSecondsPeriodForGetTickets", 30, 15, 300);
      }
    } // CashoutTimeSecondsPeriodForGetTickets

    public static String CashoutVoucherHeader
    {
      get
      {
        return GeneralParam.GetString("AutoPrintVoucher", "CashOut.Voucher.Header", string.Empty);
      }
    } // CashoutVoucherHeader

    public static String CashoutVoucherFooter
    {
      get
      {
        return GeneralParam.GetString("AutoPrintVoucher", "CashOut.Voucher.Footer", string.Empty);
      }
    } // CashoutVoucherFooter

    public static String CashoutVoucherTitle
    {
      get
      {
        return GeneralParam.GetString("AutoPrintVoucher", "CashOut.Voucher.Title", string.Empty);
    }
    } // CashoutVoucherPagcor

    public static String CashoutVoucherCompanyName
    {
      get
      {
        return GeneralParam.GetString("AutoPrintVoucher", "CashOut.Voucher.CompanyName", string.Empty);
      }
    } // CashoutVoucherCompanyName

    public static String CashoutVoucherSiteName
    {
      get
      {
        return GeneralParam.GetString("Site", "Name", string.Empty);
      }
    } // CashoutVoucherSiteName

    public static String CashoutVoucherReceipt
    {
      get
      {
        return GeneralParam.GetString("AutoPrintVoucher", "CashOut.Voucher.CashOutReceipt", string.Empty);
      }
    } // CashoutVoucherReceipt

    public static String CashoutVoucherWorkStation
    {
      get
      {
        return GeneralParam.GetString("AutoPrintVoucher", "CashOut.Voucher.WorkStation", string.Empty);
      }
    } // CashoutVoucherWorkStation

    public static Int32 CashoutBlockTop
    {
      get
      {
        return GeneralParam.GetInt32("AutoPrintVoucher", "CashOut.BlockTop", 10, 1, 100);
      }
    } // CashoutBlockTop

    public static List<AutoPrintConfig.VoucherType> TypesEnabled
    {
      get
      {
        return new List<AutoPrintConfig.VoucherType>(Array.ConvertAll(GeneralParam.GetString("AutoPrintVoucher", "Types.Enabled", "0").Split(','), delegate(string x)
        {
          return (AutoPrintConfig.VoucherType)Enum.Parse(typeof(AutoPrintConfig.VoucherType), x);
        }));
      }
    } // TypesEnabled

    public static List<string> GetPrintersByTypeVoucher(AutoPrintConfig.VoucherType Type)
    {
      switch (Type)
      {
        case AutoPrintConfig.VoucherType.CashOutReceipt:
          
          return AutoPrintConfig.CashoutEnabledPrinters;
      }

      return new List<string>();
    } // GetPrintersByTypeVoucher
    #endregion // Public methods
  } // AutoPrintConfig
}
