﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AutoPrintPendingGenerationVouchersBL.cs
// 
//   DESCRIPTION: Common Class for AutoPrintPendingGenerationVouchersBL - Business Logic to generate every cash out voucher
//        AUTHOR: Jesús García
// 
// CREATION DATE: 04-MAY-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2018 JGC    First release.
// 12-JUN-2018 MS     PBI 32946: WIGOS-10137 Philippines - Create Cashout Receipt Report
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;
using System.Diagnostics;
using WSI.Common.AutoPrint.SQL;
using WSI.Common.AutoPrint.Entities;
using WSI.Common.AutoPrint.DAO;
using System.Data.SqlClient;

namespace WSI.Common.AutoPrint.BusinessLogic
{
  public class AutoPrintPendingGenerationVouchersBL
  {
    #region Members
    private AutoPrintPendingPrintGenerationVouchersDAO m_pending_print_generation_vouchers_dao;
    private AutoPrintPendingPrintVouchersBL m_auto_print_pending_print_vouchers_BL;
    private AutoPrintCashOutReportBL m_auto_print_cash_out_report_BL;
    #endregion // Members

    #region Properties
    public AutoPrintPendingPrintGenerationVouchersDAO PendingPrintGenerationVouchersDAO
    {
      get
      {
        if (m_pending_print_generation_vouchers_dao == null)
        {
          m_pending_print_generation_vouchers_dao = new AutoPrintPendingPrintGenerationVouchersDAO();
        }

        return m_pending_print_generation_vouchers_dao;
      }
    }

    public AutoPrintPendingPrintVouchersBL PendingPrintVouchersBL
    {
      get
      {
        if (m_auto_print_pending_print_vouchers_BL == null)
        {
          m_auto_print_pending_print_vouchers_BL = new AutoPrintPendingPrintVouchersBL();
        }

        return m_auto_print_pending_print_vouchers_BL;
      }
    }

    public AutoPrintCashOutReportBL CashOutReportBL
    {
      get
      {
        if (m_auto_print_cash_out_report_BL == null)
        {
          m_auto_print_cash_out_report_BL = new AutoPrintCashOutReportBL();
        }

        return m_auto_print_cash_out_report_BL;
      }
    }

    public AutoPrintConfig.VoucherType Type { get; set; }
    #endregion // Properties

    #region Public methods
    /// <summary>
    /// Save SessionId in AUTO_PRINT_PENDING_GENERATION_VOUCHER for cash outs
    /// </summary>
    /// <param name="SessionId"></param>
    /// <param name="Type"></param>
    /// <returns></returns>
    public Boolean SaveEndSession(long SessionId, AutoPrintConfig.VoucherType Type)
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!this.PendingPrintGenerationVouchersDAO.InsertSessionId(SessionId, Type, _db_trx.SqlTransaction))
          {
            return false;
          }

          _db_trx.Commit();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // SaveEndSession

    /// <summary>
    /// Delete a SessionId from AUTO_PRINT_PENDING_GENERATION_VOUCHER
    /// </summary>
    /// <param name="SessionId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean DeleteEndSession(long SessionId, SqlTransaction SqlTrx)
    {
      try
      {
        if (!this.PendingPrintGenerationVouchersDAO.DeleteSessionId(SessionId, SqlTrx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DeleteEndSession

    /// <summary>
    /// Get all SessionIds from AUTO_PRINT_PENDING_GENERATION_VOUCHER
    /// </summary>
    /// <param name="Type"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="ListReceipts"></param>
    /// <returns></returns>
    public Boolean GetEndSessions(AutoPrintConfig.VoucherType Type, SqlTransaction SqlTrx, out List<eCashOutReceiptData> ListReceipts)
    {
      ListReceipts = new List<eCashOutReceiptData>();

      try
      {
        if (!this.PendingPrintGenerationVouchersDAO.GetSessionIds(Type, SqlTrx, out ListReceipts))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetEndSessions

    /// <summary>
    /// Generate vouchers for SessionIds in AUTO_PRINT_PENDING_GENERATION_VOUCHER, and insert in AUTO_PRINT_CASH_OUT_RECEIPT_REPORT for report, and AUTO_PRINT_PENDING_PRINT_VOUCHER for enqueue voucher to get print
    /// </summary>
    /// <returns></returns>
    public Boolean GeneratePendingPrintVouchers()
    {
      Int64 _sequence;
      Int64 _voucher_id;
      Int64 _operation_id;
      Int32 _system_user_id;
      String _system_user_name;
      CashierSessionInfo _cashier_session_info;
      List<eCashOutReceiptData> _list_pending_generate_vouchers;

      try
      {
        _operation_id = 0;

        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (!this.GetEndSessions(AutoPrintConfig.VoucherType.CashOutReceipt, _db_trx.SqlTransaction, out _list_pending_generate_vouchers))
          {
            return false;
          }

          if (!Cashier.GetSystemUser(GU_USER_TYPE.SYS_SYSTEM, _db_trx.SqlTransaction, out _system_user_id, out _system_user_name))
          {
            Log.Error("ReleaseInactiveSessions: GetSystemUser SYS_SYSTEM failed.");
          }
        }

        foreach (eCashOutReceiptData _voucher in _list_pending_generate_vouchers)
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            try
            {
              if (!WSI.Common.Cashier.GetCashierSessionFromTerminal(_voucher.TerminalID, (Int32)GU_USER_TYPE.SYS_TITO, CASHIER_SESSION_STATUS.OPEN, out _cashier_session_info, _db_trx.SqlTransaction))
              {
                continue;
              }

              WSI.Common.CommonCashierInformation.SetCashierInformation(_cashier_session_info.CashierSessionId, (Int32)GU_USER_TYPE.SYS_SYSTEM, _system_user_name, _voucher.TerminalID, _voucher.TerminalName);

              if (!Operations.DB_InsertOperation(OperationCode.AUTO_PRINT_VOUCHER_CASH_OUT, _voucher.AccountId, _cashier_session_info.CashierSessionId, 0, 0,
                                                _voucher.ReceiptValue, 0, 0, 0, "", out _operation_id, _db_trx.SqlTransaction))
              {
                continue;
              }

              _voucher.TransactionNumber = _operation_id;

              Sequences.GetSequenceFromPlaySession(_db_trx.SqlTransaction, _voucher.SessionId, out _sequence);
              _voucher.ReceiptNumber = _sequence;

              if (!VoucherBuilder.GetVoucherCashOutReceipt(new System.Collections.ArrayList(), _voucher, _operation_id, PrintMode.Print, _db_trx.SqlTransaction, out _voucher_id))
              {
                Log.Error(string.Format("Cannot generate voucher for SessionId: {0}", _voucher.SessionId));

                continue;
              }

              // Insert in report DB
              if (!this.CashOutReportBL.InsertCashOutReport(_voucher, _voucher_id, _voucher.SessionId, AutoPrintConfig.AutoPrintStatus.PendingPrint, _db_trx.SqlTransaction))
              {
                Log.Error(string.Format("Cannot insert in report DB for SessionId: {0}", _voucher.SessionId));

                continue;
              }

              if (AutoPrintConfig.CashoutPrintEnabled)
              {
                if (!this.PendingPrintVouchersBL.InsertVouchersToPrint(_voucher_id, AutoPrintConfig.VoucherType.CashOutReceipt, _db_trx.SqlTransaction))
                {
                  Log.Error(string.Format("Cannot insert voucher in queue for print DB for SessionId: {0}", _voucher.SessionId));

                  continue;
                }
              }

              if (!this.DeleteEndSession(_voucher.SessionId, _db_trx.SqlTransaction))
              {
                Log.Error(string.Format("Cannot delete from pending generation voucher DB for SessionId: {0}", _voucher.SessionId));

                continue;
              }

              _db_trx.Commit();

            }
            catch (Exception _ex)
            {
              Log.Exception(_ex);

              _db_trx.Rollback();
            }
          }

        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GeneratePendingPrintVouchers
    #endregion // Public methods
  }
} // AutoPrintPendingGenerationVouchersBL