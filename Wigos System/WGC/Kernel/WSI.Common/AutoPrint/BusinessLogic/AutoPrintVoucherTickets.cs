﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AutoPrintVoucherTickets.cs
// 
//   DESCRIPTION: Common Class for AutoPrintVoucherTickets
//        AUTHOR: David Perelló
// 
// CREATION DATE: 04-MAY-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2018 DPC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.AutoPrint.BusinessLogic
{
  public class AutoPrintVoucherTickets : IEquatable<AutoPrintVoucherTickets>
  {
    #region Members
    private Int64 m_draw_ticket_id;
    private Int64 m_account_id;
    private String m_voucher_html;
    private AutoPrintConfig.AutoPrintStatus m_draw_ticket_status;
    #endregion // Members

    #region Properties
    public Int64 DrawTicketId
    {
      get { return m_draw_ticket_id; }
      set { m_draw_ticket_id = value; }
    }
    public Int64 AccountId
    {
      get { return m_account_id; }
      set { m_account_id = value; }
    }
    public String VoucherHtml
    {
      get { return m_voucher_html; }
      set { m_voucher_html = value; }
    }
    public AutoPrintConfig.AutoPrintStatus DrawTicketStatus
    {
      get { return m_draw_ticket_status; }
      set { m_draw_ticket_status = value; }
    }
    #endregion // Properties

    #region Constructor
    public AutoPrintVoucherTickets(Int64 DrawTicketId, Int64 AccountId, AutoPrintConfig.AutoPrintStatus DrawTicketStatus, String VoucherHtml)
    {
      m_draw_ticket_id = DrawTicketId;
      m_voucher_html = VoucherHtml;
      m_account_id = AccountId;
      m_draw_ticket_status = DrawTicketStatus;
    }
    #endregion // Constructor

    #region Public methods
    public bool Equals(AutoPrintVoucherTickets ObjectCompare)
    {
      if (ObjectCompare == null)
      {
        return false;
      }

      if (ReferenceEquals(this, ObjectCompare))
      {
        return true;
      }

      if (ObjectCompare.DrawTicketId == this.DrawTicketId)
      {
        return true;
      }

      return false;
    }
    #endregion // Public methods

    #region Overrides
    public override bool Equals(Object Obj)
    {
      var other = Obj as AutoPrintVoucherTickets;

      if (other == null)
      {
        return false;
      }

      return this.Equals(other);
    }
    #endregion // Overrides
  } // AutoPrintVoucherTickets
}
