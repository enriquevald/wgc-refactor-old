﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AutoPrintPrintingVouchers.cs
// 
//   DESCRIPTION: Common Class for AutoPrintPrintingVouchers
//        AUTHOR: David Perelló
// 
// CREATION DATE: 04-MAY-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2018 DPC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Drawing.Text;
using System.Text;
using System.Threading;
using WSI.Common.AutoPrint.Entities;

namespace WSI.Common.AutoPrint.BusinessLogic
{
  public class AutoPrintPrintingVouchers
  {
    #region Members
    private Thread m_thread_process_pending_tickets_queue;
    private object m_locker = new object[] { };
    private AutoPrintVouchersManager m_manager;
    #endregion // Members

    #region Properties
    public string Instance_Printer
    {
      get;
      set;
    }

    private AutoPrintVouchersManager managerBL
    {
      get
      {
        if (m_manager == null)
        {
          m_manager = new AutoPrintVouchersManager();
        }

        return m_manager;
      }
    }

    public AutoPrintConfig.VoucherType Type { get; set; }
    #endregion // Properties

    #region Constructor
    public AutoPrintPrintingVouchers(string Printer, AutoPrintConfig.VoucherType Type)
    {
      this.Instance_Printer = Printer;
      this.Type = Type;
    }
    #endregion // Constructor

    #region Public methods
    /// <summary>
    /// Starts a new thread to print vouchers cash outs
    /// </summary>
    public void Init()
    {
      try
      {
        m_thread_process_pending_tickets_queue = new Thread(ProcessPendingPrintTickets);
        m_thread_process_pending_tickets_queue.SetApartmentState(ApartmentState.STA);
        m_thread_process_pending_tickets_queue.Name = string.Format("PendingPrintSearchThread_{0}", Instance_Printer);
        AutoPrintVouchers.m_dicc_running_threads.Add(Instance_Printer,
          new eThreadKiller
          {
            Printer_thread = m_thread_process_pending_tickets_queue,
            Kill = false //Insert Thread with NO_KILL_FLAG
          });
        m_thread_process_pending_tickets_queue.Start();

        Log.Message(string.Format("AutoPrintVouchers :: {0} :: Printer thread '{1}' has been created.", System.Reflection.MethodBase.GetCurrentMethod().Name, Instance_Printer));
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // Init
    #endregion // Public methods

    #region Private methods
    /// <summary>
    ///  Principal Method witch triggers the thread's Start().
    //   If contains the Sleep configurated via General Param.
    //   Queue locking to protect multiple reads.
    //   Printing method call.
    /// </summary>
    private void ProcessPendingPrintTickets()
    {
      Int32 _wait_hint = 0;

      try
      {
        while (true)
        {
          if (AutoPrintVouchers.m_dicc_running_threads[Instance_Printer].Kill)
          {
            Log.Warning(string.Format("{0} :: Thread of printer '{1}' is going to be killed.", System.Reflection.MethodBase.GetCurrentMethod().Name, Instance_Printer));
            //After that, remove entrance from dictionary to syncronize current number of active threads with dictionary entrances.
            AutoPrintVouchers.m_dicc_running_threads.Remove(Instance_Printer);
            break;
          }

          Thread.Sleep(_wait_hint);

          if (!Services.IsPrincipal("WCP"))
          {
            _wait_hint = 1000;

            continue;
          }
          _wait_hint = 1000 * AutoPrintConfig.CashoutSecondsOfSleepTimeBetweenPrints; // 1 Seconds (min 1 seg, max 15 seg)

          if (AutoPrintVouchers.m_dic_queues.ContainsKey(this.Type) && AutoPrintVouchers.m_dic_queues[this.Type].Count > 0)
          {
            lock (m_locker)
            {
              SendToPrintTicket(); // <-- Strictly printing process.
            }
          }
        } // while
        Log.Warning(string.Format("{0} :: Thread of printer '{1}' has been exited.", System.Reflection.MethodBase.GetCurrentMethod().Name, Instance_Printer));
      }
      catch (Exception ex)
      {
        Log.Error(string.Format("{0} :: {1}.", System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message));
      }
    } // ProcessPendingPrintTickets

    /// <summary>
    /// Method to Dequeue and process each ticket.
    //           Process relates to: 
    //              1-. Dequeue ticket.
    //              2-. Print ticket.
    //              1-. Mark ticket as Printed in DB.
    /// </summary>
    private void SendToPrintTicket()
    {
      ePendingPrintVoucher _current_ticket;
      using (DB_TRX _db_trx = new DB_TRX())
      {
        try
        {
          // 1-. Dequeue ticket from queue.
          _current_ticket = (ePendingPrintVoucher)((WaitableQueue)AutoPrintVouchers.m_dic_queues[this.Type]).Dequeue();

          if (_current_ticket.Status.Equals(AutoPrintConfig.AutoPrintStatus.PendingPrint))
          {
            // 2-. Print the ticket.
            PrintHtml(m_thread_process_pending_tickets_queue, Instance_Printer, _current_ticket.Html);
            // 3-. Mark as PRINTED in DB.
            if (!this.managerBL.PendingPrintVouchers.DeleteVouchersToPrint(_current_ticket.VoucherId, _current_ticket.Type, _db_trx.SqlTransaction))
            {
              Log.Error(string.Format("Cannot delete voucher in DB for VoucherId: {0}", _current_ticket.VoucherId));
            }

            if (!this.managerBL.CashOutReport.UpdatePrintStatus(_current_ticket.VoucherId, AutoPrintConfig.AutoPrintStatus.Printed, _db_trx.SqlTransaction))
            {
              Log.Error(string.Format("Cannot update print status in DB for VoucherId: {0}", _current_ticket.VoucherId));
            }
          }
          _db_trx.Commit();
        }
        catch (Exception ex)
        {
          _db_trx.Rollback();
          Log.Error(string.Format("{0} :: {1}.", System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message));
        }
      }
    } // SendToPrintTicket
    #endregion // Private methods

    #region Static methods
    /// <summary>
    /// Method to explicitly print a ticket.
    /// </summary>
    /// <param name="T_thread"></param>
    /// <param name="Printer"></param>
    /// <param name="Html"></param>
    private static void PrintHtml(Thread T_thread, string Printer, string Html)
    {
      try
      {
        int _img_min_width = 300;
        int _img_max_width = 350;
        int _paper_width;

        int _img_min_height = 200;
        int _img_max_height = 1000;
        int _paper_height;
        using (PrintDocument _pd_document = new PrintDocument())
        {
          using (System.Drawing.Image _image = TheArtOfDev.HtmlRenderer.WinForms.HtmlRender.RenderToImageGdiPlus(Html, new System.Drawing.Size(_img_min_width, _img_min_height), new System.Drawing.Size(_img_max_width, _img_max_height), TextRenderingHint.AntiAliasGridFit))
          {
            //Set Paper Dimensions = Generated Image Size.
            _paper_width = _image.Width;
            _paper_height = _image.Height;

            //Setup printer Settings.
            _pd_document.DefaultPageSettings.PaperSize = new PaperSize() { Height = _paper_height, Width = _paper_width, PaperName = "Custom" };
            _pd_document.DocumentName = String.Format("{0}_{1}.html", Printer, Guid.NewGuid().ToString());
            _pd_document.DefaultPageSettings.Margins.Top = 0;
            _pd_document.DefaultPageSettings.Margins.Bottom = 0;
            _pd_document.DefaultPageSettings.Margins.Left = 0;
            _pd_document.DefaultPageSettings.Margins.Right = 0;
            _pd_document.PrinterSettings.DefaultPageSettings.Margins.Top = 0;
            _pd_document.PrinterSettings.DefaultPageSettings.Margins.Bottom = 0;
            _pd_document.PrinterSettings.DefaultPageSettings.Margins.Left = 0;
            _pd_document.PrinterSettings.DefaultPageSettings.Margins.Right = 0;
            _pd_document.PrinterSettings.PrinterName = Printer;

            //PRINT PAGE :: Draw page at printing moment.
            _pd_document.PrintPage += new PrintPageEventHandler((object sendr, PrintPageEventArgs evt) =>
            {
              try
              {
                //Set image as drawn, but resized to fit to the paper.
                evt.Graphics.DrawImageUnscaled(ResizeImage(_image, _img_min_width, _image.Height), 0, 0, _paper_width, _paper_height);
                //Log.Message(string.Format("{0} :: PRINTER: {1} |||| {2} ||||.", "PrintDocument.PrintPage", Printer, "Document is being printed!"));
              }
              catch (Exception ex)
              {
                // Thread will be aborted to avoid throw draws against a hanged printer.
                T_thread.Abort();
                Log.Error(string.Format("{0} :: {1}.", "PrintDocument.PrintPage", ex.Message));
              }
            });

            //BEGIN PRINTING PAGE :: Pre-actions.
            _pd_document.BeginPrint += (object sendr, PrintEventArgs evt) =>
            {
              try
              {
                //Log.Message(string.Format("{0} :: PRINTER: {1} |||| {2} ||||.", "PrintDocument.BeginPrint", Printer, "Document started to print!"));
              }
              catch (Exception ex)
              {
                // Thread will be aborted to avoid throw draws against a hanged printer.
                T_thread.Abort();
                Log.Error(string.Format("{0} :: PRINTER: {1} |||| {2} ||||.", "PrintDocument.BeginPrint", Printer, ex.Message));
              }
            };

            //ENDING PRINTING PAGE :: Post-actions.
            _pd_document.EndPrint += (object sendr, PrintEventArgs evt) =>
            {
              try
              {
                //Log.Message(string.Format("{0} :: PRINTER: {1} |||| {2} ||||.", "PrintDocument.EndPrint", Printer, "Document has been printed!"));
              }
              catch (Exception ex)
              {
                // Thread will be aborted to avoid throw draws against a hanged printer.
                T_thread.Abort();
                Log.Error(string.Format("{0} :: PRINTER: {1} |||| {2} ||||.", "PrintDocument.EndPrint", Printer, ex.Message));
              }
            };

            //Call 'Print()' method if setup is valid for the current printer.
            if (_pd_document.PrinterSettings.IsValid)
            {
              _pd_document.Print();
            }
            else
            {
              Log.Error(string.Format("{0} :: Established PrinterSettings Not Supported for printer: '{1}' ||||.", System.Reflection.MethodBase.GetCurrentMethod().Name, Printer));
            }
          } //using
        } //using
      }
      catch (Exception ex)
      {
        Log.Error(string.Format("{0} :: {1}.", System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message));
      }
    } //PrintHtml

    /// <summary>
    /// Method to Resize an Image to an specified size.
    /// </summary>
    /// <param name="image"></param>
    /// <param name="width"></param>
    /// <param name="height"></param>
    /// <returns></returns>
    public static Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
    {
      var destRect = new System.Drawing.Rectangle(0, 0, width, height);
      var destImage = new System.Drawing.Bitmap(width, height);

      destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

      using (System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(destImage))
      {
        graphics.CompositingMode = CompositingMode.SourceCopy;
        graphics.CompositingQuality = CompositingQuality.HighQuality;
        graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
        graphics.SmoothingMode = SmoothingMode.HighQuality;
        graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

        using (var wrapMode = new ImageAttributes())
        {
          wrapMode.SetWrapMode(WrapMode.TileFlipXY);
          graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
        }
      }

      return destImage;
    } //ResizeImage
    #endregion // Static methods
  } // AutoPrintPrintingVouchers
}
