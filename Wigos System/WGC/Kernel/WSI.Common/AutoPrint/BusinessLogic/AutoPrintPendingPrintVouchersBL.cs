﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AutoPrintPendingPrintVouchersBL.cs
// 
//   DESCRIPTION: Common Class for AutoPrintPendingPrintVouchersBL - Business Logic to print every cash out voucher
//        AUTHOR: David Perelló
// 
// CREATION DATE: 04-MAY-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2018 DPC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;
using WSI.Common.AutoPrint.DAO;
using WSI.Common.AutoPrint.Entities;

namespace WSI.Common.AutoPrint.BusinessLogic
{
  public class AutoPrintPendingPrintVouchersBL
  {
    #region Members
    private AutoPrintPendingPrintVouchersDAO m_pending_print_vouchers_dao;
    #endregion // Members
    
    #region Properties
    private AutoPrintPendingPrintVouchersDAO PendingPrintVouchersDAO
    {
      get
      {
        if (m_pending_print_vouchers_dao == null)
        {
          m_pending_print_vouchers_dao = new AutoPrintPendingPrintVouchersDAO();
        }

        return m_pending_print_vouchers_dao;
      }
    }
    #endregion // Properties

    #region Public methods
    public Boolean GetVouchersToPrint(AutoPrintConfig.VoucherType Type, SqlTransaction SqlTrx, out List<ePendingPrintVoucher> ListPrintVouchers)
    {
      return this.PendingPrintVouchersDAO.GetVouchersToPrint(Type, SqlTrx, out ListPrintVouchers);
    } // GetVouchersToPrint

    public Boolean DeleteVouchersToPrint(Int64 VoucherId, AutoPrintConfig.VoucherType Type, SqlTransaction SqlTrx)
    {
      return this.PendingPrintVouchersDAO.DeleteVouchersToPrint(VoucherId, Type, SqlTrx);
    } // DeleteVouchersToPrint

    public Boolean InsertVouchersToPrint(Int64 VoucherId, AutoPrintConfig.VoucherType Type, SqlTransaction SqlTrx)
    {
      return this.PendingPrintVouchersDAO.InsertVouchersToPrint(VoucherId, Type, SqlTrx);
    } // InsertVouchersToPrint
    #endregion // Public methods
  } // AutoPrintPendingPrintVouchersBL
}
