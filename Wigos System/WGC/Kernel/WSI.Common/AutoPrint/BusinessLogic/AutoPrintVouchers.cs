﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AutoPrintVouchers.cs
// 
//   DESCRIPTION: Common Class for AutoPrintVouchers
//        AUTHOR: David Perelló
// 
// CREATION DATE: 04-MAY-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2018 DPC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Management;
using System.Text;
using System.Threading;
using WSI.Common.AutoPrint.Entities;

namespace WSI.Common.AutoPrint.BusinessLogic
{
  public class AutoPrintVouchers
  {
    #region Constants
    private const String EXCEPTION_WCP_AUTOPRINT_TASK = "Exception into class AutoPrintVouchers.";
    private const String EXCEPTION_MESSAGE = ". Message: ";
    #endregion // Constants

    #region Members
    public static WaitableQueue m_queue_vouchers = new WaitableQueue();
    public static Dictionary<AutoPrintConfig.VoucherType, WaitableQueue> m_dic_queues = new Dictionary<AutoPrintConfig.VoucherType, WaitableQueue>();
    public static Dictionary<string, eThreadKiller> m_dicc_running_threads = new Dictionary<string, eThreadKiller>();
    private static Thread m_thread_process_init;
    #endregion // Members

    #region Public methods
    /// <summary>
    /// Method to start AutoPrintDraw Threads and to check if keep them alive, create new ones or kill them.
    /// </summary>
    /// <returns></returns>
    /// 
    public static void Init()
    {
      m_thread_process_init = new Thread(AutoPrintVouchersStartThread);
      m_thread_process_init.SetApartmentState(ApartmentState.STA);
      m_thread_process_init.Name = string.Format("AutoPrintVouchersStart");
      m_thread_process_init.Start();
    } // Init
    #endregion // Public methods

    #region Private methods
    /// <summary>
    /// Method that executes thread and do the Business Logic to print vouchers of cash outs
    /// </summary>
    private static void AutoPrintVouchersStartThread()
    {
      Boolean _is_valid;
      Int32 _wait_hint;
      AutoPrintPrintingVouchers _cls_auto_print_printing_vouchers;

      _wait_hint = 0;
      _is_valid = false;

      AutoPrintVouchersProcess.Init();

      while (true)
      {
        try
        {
          Thread.Sleep(_wait_hint);

          if (!AutoPrintConfig.CashoutPrintEnabled)
          {
            _wait_hint = 60000;

            continue;
          }

          // Check if at least 1 informed printer name is a server installed printer.
          if (!AutoPrintVouchers.IsPrinterListInformed())
          {
            Log.Warning(string.Format("AutoPrintVouchers :: {0} :: NOT DETECTED PRINTERS.", "AutoPrintVouchersStartThread()"));

            _wait_hint = 1000 * 60 * 2;

            continue;
          }

          _wait_hint = 1000 * 1;

          foreach (var _type in AutoPrintConfig.TypesEnabled)
          {
            foreach (string _printer in AutoPrintConfig.GetPrintersByTypeVoucher(_type))
            {
              _is_valid = false;
              IsValidPrinter(ref _is_valid, _printer, true);

              if (_is_valid && !m_dicc_running_threads.ContainsKey(_printer))
              {
                _cls_auto_print_printing_vouchers = new AutoPrintPrintingVouchers(_printer, _type);
                _cls_auto_print_printing_vouchers.Init();
              }
            }
          }
          //After everything check list against General Param to kill (if they're orphan).
          KillOrphanPrinterThreads();
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          Log.Warning(String.Format(EXCEPTION_WCP_AUTOPRINT_TASK + "AutoPrintVouchersStartThread" + EXCEPTION_MESSAGE + "{0} - {1} .", _ex.Message, WGDB.Now));
        }
      } // while 
    } // AutoPrintVouchersStartThread

    /// <summary>
    /// Method to check if threads have to be killed or keep them alive.
    /// </summary>
    /// <returns></returns>
    private static void KillOrphanPrinterThreads()
    {
      Boolean _printer_found = false;

      try
      {
        foreach (string _printer_gp in m_dicc_running_threads.Keys)
        {
          foreach (string _configured_printer in AutoPrintConfig.CashoutEnabledPrinters)
          {
            if (_configured_printer.Equals(_printer_gp))
            {
              _printer_found = true;

              break;
            }
          }
          if (!_printer_found)
          {
            m_dicc_running_threads[_printer_gp].Kill = true;
          }
        }
      }
      catch (Exception ex)
      {
        Log.Error(string.Format("{0} :: {1}.", System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message));
      }
    }

    /// <summary>
    /// Validates that 'EnabledPrinters' General Param has at least 1 printer name informed.
    /// </summary>
    /// <returns></returns>
    private static Boolean IsPrinterListInformed()
    {
      Boolean _is_valid = false;
      string[] _printer_list = AutoPrintConfig.CashoutEnabledPrinters.ToArray();
      if (_printer_list.Length != 0)
      {
        foreach (
          string _printer in _printer_list)
        {
          _is_valid = false;
          IsValidPrinter(ref _is_valid, _printer);
          if (_is_valid.Equals(true))
            break;
        }
      }
      else
      {
        //Está vacía.
        _is_valid = false;
      }
      return _is_valid;
    } // IsPrinterListInformed

    /// <summary>
    /// Check if a printer is connected /// OBSOLETE: Status printers is not working correctly
    /// </summary>
    /// <param name="PrinterName"></param>
    /// <returns></returns>
    private static Boolean IsConnectedPrinter(string PrinterName)
    {
      string _query;

      try
      {
        _query = string.Format("SELECT * from Win32_Printer WHERE Name LIKE '%{0}'", PrinterName);

        using (ManagementObjectSearcher searcher = new ManagementObjectSearcher(_query))
        using (ManagementObjectCollection coll = searcher.Get())
        {
          try
          {
            foreach (ManagementObject printer in coll)
            {
              foreach (PropertyData property in printer.Properties)
              {
                if (property.Name.Equals("PrinterStatus"))
                {
                  if (!property.Value.Equals((UInt16)AutoPrintConfig.PrinterStatus.Idle) && !property.Value.Equals((UInt16)AutoPrintConfig.PrinterStatus.Printing))
                  {
                    return false;
                  }
                }
              }
            }
          }
          catch (ManagementException _ex)
          {
            // Log ?
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        // log
      }

      return false;
    } // IsConnectedPrinter

    /// <summary>
    /// Checks that every printer_name informed is valid, this means that it's in the server's installed printers list.
    /// </summary>
    /// <returns></returns>
    private static void IsValidPrinter(ref Boolean Is_valid, string Printer, Boolean Logging = false)
    {
      if (!Printer.Equals(string.Empty))
      {
        foreach (string _current_installed_printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
        {
          if (Printer.Equals(_current_installed_printer))
          {
            // Status printers is not working correctly
            //if (IsConnectedPrinter(_current_installed_printer))
            //{
              Is_valid = true;
              if (Logging && !m_dicc_running_threads.ContainsKey(Printer))
                Log.Message(string.Format("AutoPrintVouchers :: {0} :: Valid printer detected: '{1}'.", System.Reflection.MethodBase.GetCurrentMethod().Name, Printer));
              break;
            //}
          }
        }
        if (!Is_valid && Logging)
          Log.Warning(string.Format("AutoPrintVouchers :: {0} :: NOT valid printer detected: '{1}'.", System.Reflection.MethodBase.GetCurrentMethod().Name, Printer));
      }
    } // IsValidPrinter
    #endregion
  } // AutoPrintVouchers
}
