﻿//------------------------------------------------------------------------------
// Copyright © 2018 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AutoPrintCashOutReportBL.cs
//                 
//   DESCRIPTION: Common Class for AutoPrintCashOutReportBL - Business Logic to report every cash out voucher
//        AUTHOR: David Perelló
// 
// CREATION DATE: 04-MAY-2018
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-MAY-2018 DPC    First release.
// 12-JUN-2018 MS     PBI 32946: WIGOS-10137 Philippines - Create Cashout Receipt Report
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using WSI.Common.AutoPrint.Entities;
using WSI.Common.AutoPrint.DAO;

namespace WSI.Common.AutoPrint.BusinessLogic
{
  public class AutoPrintCashOutReportBL
  {
    #region Members
    private AutoPrintCashOutReportDAO m_cash_out_report_DAO;
    #endregion // Members

    #region Properties
    private AutoPrintCashOutReportDAO CashOutReportDAO
    {
      get
      {
        if (m_cash_out_report_DAO == null)
        {
          m_cash_out_report_DAO = new AutoPrintCashOutReportDAO();
        }

        return m_cash_out_report_DAO;
      }
    }
    #endregion // Properties

    #region Public methods
    /// <summary>
    /// Insert a register for a cash out in AUTO_PRINT_CASH_OUT_RECEIPT_REPORT
    /// </summary>
    /// <param name="VoucherId"></param>
    /// <param name="PlaySessionId"></param>
    /// <param name="Status"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean InsertCashOutReport(eCashOutReceiptData Voucher, Int64 VoucherId, Int64 PlaySessionId, AutoPrintConfig.AutoPrintStatus Status, SqlTransaction SqlTrx)
    {
      return this.CashOutReportDAO.InsertCashOutReport(Voucher, VoucherId, PlaySessionId, Status, SqlTrx);
    } // InsertCashOutReport

    /// <summary>
    /// Update print status for a cash out in AUTO_PRINT_CASH_OUT_RECEIPT_REPORT
    /// </summary>
    /// <param name="VoucherId"></param>
    /// <param name="Status"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public Boolean UpdatePrintStatus(Int64 VoucherId, AutoPrintConfig.AutoPrintStatus Status, SqlTransaction SqlTrx)
    {
      return this.CashOutReportDAO.UpdatePrintStatus(VoucherId, Status, SqlTrx);
    } // UpdatePrintStatus
    #endregion // Public methods
  } // AutoPrintCashOutReportBL
}
