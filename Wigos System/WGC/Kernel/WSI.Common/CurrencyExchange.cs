﻿
//------------------------------------------------------------------------------
// Copyright © 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CurrencyExchange.cs
// 
//   DESCRIPTION: CurrencyExchange class
// 
//        AUTHOR: Ignasi Carreño
// 
// CREATION DATE: 12-JUN-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-JUN-2013 ICS    First version.
// 02-JUL-2013 DLL    Correct SQL column names
// 05-JUL-2013 DLL    New function to filter currency exchange
// 26-JUL-2013 ICS    Added nuevo sol peruano currency
// 12-AUG-2013 DLL    With national bank card add commission to InAmount
// 22-AUG-2013 DLL    Added methods to read active currencies for an specified cashier session
// 10-SEP-2013 DLL    Added Check currency
// 20-SEP-2013 DLL    Added Bank Card and Check in Withdraws and CloseCash operations
// 27-SEP-2013 ICS    Changed IEqualityComparer to IComparer in CurrecnyIsoType class
// 04-OCT-2013 DLL    WIG-265: Wrong calculation in commission
// 29-OCT-2013 ICS    Added new method to obtain historic change rate
// 11-NOV-2013 ICS    WIG-401: Currency collection in cash closing is not working properly when there are Euro currencies
// 19-FEB-2014 JAB    Fixed bug WIG-655: Without gaming tables, chips are showed.
// 03-MAR-2014 CCG    Added new methods to obtain the national currency symbol and pattern type.
// 14-APR-2014 JCA    Added default Iso Code to use with unknown currencies
// 14-JUL-2014 XIT    Added MultiLanguage support
// 25-JUL-2014 DLL    Chips are moved to a separate table
// 07-AUG-2014 RMS & MPO & DRV Fixed Bug WIG-1166: incorrect currency title label on credit card and check.
// 08-AUG-2014 RMS    Added supoort for Specific Bank Card Types
// 18-SEP-2014 DLL    Added new method for cash advance
// 08-OCT-2014 JBC    Fixed Bug WIG-1437: CashAdvance mode in ApplyExchange
// 14-NOV-2014 MPO    Added nordway CurrencyExchange and flags
// 10-DEC-2014 DRV    Decimal currency formatting: DUT 201411 - Tito Chile - Currency formatting without decimals
// 05-FEB-2015 JPJ    Fixed Bug WIG-2011:GrossAmount formula changed
// 29-JUN-2015 DHA    Added comissions on card casino payment
// 26-AUG-2015 DHA    Initialize parameters by default
// 09-SEP-2015 ETP    Product Backlog Item 4181 - Added new currencies: Uruguay (UYU) and Trinidad & Tobago (TTD)
// 11-NOV-2015 DLL    Product Backlog Item 6379 - Error when create new denomination
// 09-NOV-2015 FOS    Product Backlog Item 5850:Apply new design in form
// 05-JAN-2016 DDS    product Backlog Item 7885 - Floor Dual Currency: Method to return Currencies
// 17-FEB-2016 YNM    Product Backlog Item 9278:Regionalization of Phillipines
// 04-APR-2016 DHA    Product Backlog Item 9756: added chips (RE, NR and Color)
// 23-MAR-2016 JML    Product Backlog Item 9755:Gaming Tables (Fase 1): Cage
// 13-APR-2016 DHA    Product Backlog Item 9950: added chips operation (sales and purchases)
// 30-MAR-2016 RAB    Product Backlog Item 10789: Floor Dual Currency: manual input manual payments
// 05-MAY-2016 ETP    PBI 12691: Added Multidivisa order.
// 05-MAY-2016 ETP    MultiCurrencyExchange: Flags incorrectos.
// 20-JUN-2016 ETP    Fixed Bug : MultiCurrencyExchange: Added flag for national currency.
// 06-JUL-2016 DHA    Product Backlog Item 15064: multicurrency chips sale/purchase
// 08-JUL-2016 RAB    GamingTables - MultiCurrency (Phase 4): Adapt them reports of table to MultiCurrency
// 05-AGO-2016 LTC    Product Backlog Item 14990:TPV Televisa: Cashier Withdrawal
// 12-AUG-2016 ETP    Fixed Bug 16726: MultiCurrency: Added properties for NIO currency.
// 17-AUG-2016 RAB    Product Backlog Item 15914: Gaming tables (Phase 5): Order summary box and voucher currency
// 24-AUG-2016 EOR    Fixed Bug 13785: Currency Norwegian: NLS in English
// 29-AGU-2016 DHA    Fixed Bug 17116: Gaming table - Error closing fixed bank on gaming table
// 12-DEC-2016 ATB    Bug 20523:Codere - Wrong NLS in Buy/sell Operations (fix)
// 21-MAR-2017 ETP    PBI 25788: Credit Line - Get a Marker
// 30-AUG-2017 AMF    Bug 29495:[WIGOS-4667] The screen "Cerrar caja" is displaying currency exchanges that haven't any balance
// 15-SEP-2017 JML    Bug 29790:WIGOS-5165 [Ticket #8881] Release 03.005.105 - POS Integrado a Mesa - Cierre de mesa - Debe aparecer fichas inicialmente
// 03-NOV-2017 RGR    Bug 30558:WIGOS-6158 Exception when the user proceed with a "Devolucion de divisa"
// 21-NOV-2017 DPC    PBI 30852:[WIGOS-4055]: Payment threshold registration - Threshold movements report
// 22-DEC-2017 EOR    Bug 31053:WIGOS-6903 [Ticket #10771] Mty I: Error in screen "Histórico de transacciones bancarias", no appear the movement "Sustitución tarjeta - Pago con tarjeta bancaria" 
// 17-JUN-2018 LQB    Bug 33107:WIGOS-13012 Gambling tables: Close cash session error with gaming table with integrated cashier when there are operations with check and credit card.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using System.Resources;
using WSI.Common.PinPad;

namespace WSI.Common
{
  public static class LanguageResource
  {

    #region Members

    private static CurrencyExchange m_currency_exchange;

    #endregion

    #region Properties

    public static CurrencyExchange NationalCurrencyData
    {
      get
      {
        if (m_currency_exchange == null)
        {
          CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CURRENCY, CurrencyExchange.GetNationalCurrency(), out m_currency_exchange);
        }

        return m_currency_exchange;
      }
      set
      {
        m_currency_exchange = value;
      }
    }

    #endregion

  } // LanguageResource

  public class LanguageResources
  {
    #region Members

    private String m_plural_currency;
    private String m_singular_currency;
    private String m_plural_cents;
    private String m_singular_cents;

    #endregion

    #region Properties

    public String PluralCurrency
    {
      get { return m_plural_currency; }
      set { m_plural_currency = value; }
    }

    public String SingularCurrency
    {
      get { return m_singular_currency; }
      set { m_singular_currency = value; }
    }

    public String PluralCents
    {
      get { return m_plural_cents; }
      set { m_plural_cents = value; }
    }

    public String SingularCents
    {
      get { return m_singular_cents; }
      set { m_singular_cents = value; }
    }

    #endregion
  } // LanguageResources

  public class CurrencyExchange
  {
    #region Constants

    public const Int32 DEFAULT_FORMATTED_DECIMALS = 2;

    public const String CONFIGURATION_TABLE_NAME = "data";

    #endregion
    #region Members

    private CurrencyExchangeType m_type;
    private CurrencyExchangeSubType m_subtype = CurrencyExchangeSubType.NONE;
    private String m_currency_code;
    private String m_description;
    private Decimal? m_change_rate;
    private Int32? m_decimals;
    private Decimal? m_variable_comission;
    private Decimal? m_fixed_comission;
    private Decimal? m_variable_NR2;
    private Decimal? m_fixed_NR2;
    private Boolean m_status;
    private Int32 m_formatted_decimals;
    private LanguageResources m_language_resource;
    private bank_card m_bank_card;

    #endregion

    #region Properties

    public String CurrencyCode
    {
      get { return m_currency_code; }
      set { m_currency_code = value; }
    }

    public CurrencyExchangeType Type
    {
      get { return m_type; }
      set { m_type = value; }
    }

    public CurrencyExchangeSubType SubType
    {
      get { return m_subtype; }
      set { m_subtype = value; }
    }

    public String Description
    {
      get { return m_description; }
      set { m_description = value; }
    }

    public Decimal? ChangeRate
    {
      get { return m_change_rate; }
      set { m_change_rate = value; }
    }

    public Int32? Decimals
    {
      get { return m_decimals; }
      set { m_decimals = value; }
    }

    public Decimal? VariableComission
    {
      get { return m_variable_comission; }
      set { m_variable_comission = value; }
    }

    public Decimal? FixedComission
    {
      get { return m_fixed_comission; }
      set { m_fixed_comission = value; }
    }

    public Decimal? VariableNR2
    {
      get { return m_variable_NR2; }
      set { m_variable_NR2 = value; }
    }

    public Decimal? FixedNR2
    {
      get { return m_fixed_NR2; }
      set { m_fixed_NR2 = value; }
    }

    public Boolean Status
    {
      get { return m_status; }
      set { m_status = value; }
    }

    public Int32 FormattedDecimals
    {
      get { return m_formatted_decimals; }
      set { m_formatted_decimals = value; }
    }

    public LanguageResources LanguageResource
    {
      get { return m_language_resource; }
      set { m_language_resource = value; }
    }

    public bank_card BankCard
    {
      get { return m_bank_card; }
      set { m_bank_card = value; }
    }

    #endregion

    #region Private Methods

    // PURPOSE: Truncate value with specific decimals
    //
    // PARAMS:
    //     - INPUT:
    //           - Value
    //           - Decimals
    //
    // RETURNS:
    //     - Truncated number
    //
    private Decimal TruncateWithDecimals(Decimal Value, Int32 Decimals)
    {
      // DLL 04-OCT-2013
      Int32 _pow;

      _pow = (Int32)Math.Pow(10, Decimals);

      return (Decimal)(Math.Truncate(Value * _pow) / _pow);

    }

    /// <summary>
    /// Get Dataset To Load and Generate XML 
    /// </summary>
    /// <returns></returns>
    private static DataSet GetCurrencyConfigurationDataSet()
    {

      DataSet _configuration_exchange;
      DataTable _data;
      _configuration_exchange = new DataSet("ConfigurationExchange");

      _data = new DataTable(CONFIGURATION_TABLE_NAME);
      _data.Columns.Add("type", System.Type.GetType("System.Int32"));
      _data.Columns.Add("scope", System.Type.GetType("System.Int32"));

      _data.Columns.Add("fixed_commission", System.Type.GetType("System.Decimal"));
      _data.Columns.Add("variable_commission", System.Type.GetType("System.Decimal"));
      _data.Columns.Add("fixed_nr2", System.Type.GetType("System.Decimal"));
      _data.Columns.Add("variable_nr2", System.Type.GetType("System.Decimal"));

      _configuration_exchange.Tables.Add(_data);

      return _configuration_exchange;

    }
    /// <summary>
    /// Load XML to CurrencyExchange
    /// </summary>
    /// <param name="xml"></param>
    /// <returns></returns>
    private static DataSet CurrencyConfigurationFromXml(String xml)
    {
      DataSet _configuration_exchange;

      _configuration_exchange = GetCurrencyConfigurationDataSet();

      Misc.DataSetFromXml(_configuration_exchange, xml, XmlReadMode.IgnoreSchema);
      return _configuration_exchange;
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Order of currencies balance according to order established in the form 'operations with card / check and currency' in 'Wigos GUI'
    /// </summary>
    /// <param name="CurrenciesBalance">Sorted dictionary contains currencies balances. The order is alphabetical.</param>
    /// <returns>Sorted dictionary from currency configuration</returns>
    public static Dictionary<CurrencyIsoType, Decimal> OrderCurrenciesFromCurrencyConfiguration(SortedDictionary<CurrencyIsoType, Decimal> CurrenciesBalance)
    {
      Dictionary<CurrencyIsoType, Decimal> _currencies_balance_sorted;
      DataTable _currency_configuration;
      KeyValuePair<CurrencyIsoType, Decimal> _currency_balance_chip_color;
      Boolean _found_chip_color;

      _currencies_balance_sorted = new Dictionary<CurrencyIsoType, Decimal>();
      _currency_configuration = new DataTable();
      _currency_balance_chip_color = new KeyValuePair<CurrencyIsoType, Decimal>();
      _found_chip_color = false;

      ReadCurrencyExchangeFilter(false, out _currency_configuration);

      foreach (DataRow _row_currency_config in _currency_configuration.Rows)
      {
        foreach (KeyValuePair<CurrencyIsoType, Decimal> _currency_balance in CurrenciesBalance)
        {
          if ((String)_row_currency_config["ISO_CODE"] == _currency_balance.Key.IsoCode)
          {
            _currencies_balance_sorted.Add(_currency_balance.Key, _currency_balance.Value);
          }

          if (_currency_balance.Key.IsoCode == Cage.CHIPS_COLOR.ToString() && !_found_chip_color)
          {
            _currency_balance_chip_color = new KeyValuePair<CurrencyIsoType, Decimal>(_currency_balance.Key, _currency_balance.Value);
            _found_chip_color = true;
          }
        }
      }

      //Add chips color to currencies balance dictionary
      if (_currency_balance_chip_color.Key != null)
      {
        _currencies_balance_sorted.Add(_currency_balance_chip_color.Key, _currency_balance_chip_color.Value);
      }

      return _currencies_balance_sorted;
    }

    /// <summary>
    /// Order of currencies balance according to order established in the form 'operations with card / check and currency' in 'Wigos GUI'
    /// </summary>
    /// <param name="CurrenciesBalance">Sorted dictionary contains currencies balances. The order is alphabetical.</param>
    /// <returns>Sorted dictionary from currency configuration</returns>
    public static Dictionary<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> OrderCurrenciesFromCurrencyConfiguration(CageDenominationsItems.CageDenominationsDictionary CurrenciesBalance)
    {
      Dictionary<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _currencies_balance_sorted;
      _currencies_balance_sorted = new Dictionary<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem>();

      if (CurrenciesBalance.Count != 0)
      {
        DataTable _currency_configuration;
        KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _currency_balance_chip_color;
        Boolean _found_chip_color;

        _currency_configuration = new DataTable();
        _currency_balance_chip_color = new KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem>();
        _found_chip_color = false;

        ReadCurrencyExchangeFilter(false, out _currency_configuration);

        foreach (DataRow _row_currency_config in _currency_configuration.Rows)
        {
          foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _currency_balance in CurrenciesBalance)
          {
            if ((String)_row_currency_config["ISO_CODE"] == _currency_balance.Key.ISOCode)
            {
              _currencies_balance_sorted.Add(_currency_balance.Key, _currency_balance.Value);
            }

            if (_currency_balance.Key.ISOCode == Cage.CHIPS_COLOR.ToString() && !_found_chip_color)
            {
              _currency_balance_chip_color = new KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem>(_currency_balance.Key, _currency_balance.Value);
              _found_chip_color = true;
            }
          }
        }

        //Add chips color to currencies balance dictionary
        if (!String.IsNullOrEmpty(_currency_balance_chip_color.Key.ISOCode))
        {
          _currencies_balance_sorted.Add(_currency_balance_chip_color.Key, _currency_balance_chip_color.Value);
        }
      }

      return _currencies_balance_sorted;
    }

    public CurrencyExchange Copy()
    {
      return (CurrencyExchange)this.MemberwiseClone();
    } // Copy

    // PURPOSE: Get national currency
    //
    // PARAMS:
    //     - INPUT:
    //        - None
    //     - OUTPUT:
    //        - None
    //
    // RETURNS:
    //     - String with the ISO code of the national currency
    //
    public static String GetNationalCurrency()
    {
      return GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");
    }

    // PURPOSE: Check if cash advance is enabled
    //
    // PARAMS:
    //     - INPUT:
    //        - None
    //     - OUTPUT:
    //        - None
    //
    // RETURNS:
    //     - Boolean
    //
    public static Boolean IsCashAdvanceEnabled()
    {
      List<CurrencyExchange> _allowed_currencies;
      List<CurrencyExchangeType> _list_types;
      CurrencyExchange _national_currency;

      _list_types = new List<CurrencyExchangeType>();
      _list_types.Add(CurrencyExchangeType.CARD);
      _list_types.Add(CurrencyExchangeType.CHECK);

      if (!CurrencyExchange.GetAllowedCurrencies(true, _list_types, false, out _national_currency, out _allowed_currencies))
      {
        return false;
      }

      return _allowed_currencies.Count > 0;
    }

    // PURPOSE: Get national currency and allowed currencies
    //
    // PARAMS:
    //     - INPUT:
    //           - OnlyEnabled: If its set only return enabled currencies
    //     - OUTPUT:
    //           - NationalCurrency
    //           - AllowedCurrencies
    // RETURNS:
    //     - True: if the process was successful
    //     - False: Otherwise
    //
    public static Boolean GetAllowedCurrencies(Boolean OnlyEnabled, out CurrencyExchange NationalCurrency, out List<CurrencyExchange> AllowedCurrencies)
    {
      return GetAllowedCurrencies(OnlyEnabled, null, false, out NationalCurrency, out AllowedCurrencies);
    }

    // PURPOSE: Get national currency and allowed currencies
    //
    // PARAMS:
    //     - INPUT:
    //           - OnlyEnabled: If its set only return enabled currencies
    //           - RefreshParams: Force to refresh general params
    //     - OUTPUT:
    //           - NationalCurrency
    //           - AllowedCurrencies
    // RETURNS:
    //     - True: if the process was successful
    //     - False: Otherwise
    //
    public static Boolean GetAllowedCurrencies(Boolean OnlyEnabled, Boolean RefreshParams, out CurrencyExchange NationalCurrency, out List<CurrencyExchange> AllowedCurrencies)
    {
      return GetAllowedCurrencies(OnlyEnabled, null, RefreshParams, out NationalCurrency, out AllowedCurrencies);
    }

    public static Boolean GetAllowedCurrencies(Boolean OnlyEnabled, List<CurrencyExchangeType> Type, Boolean RefreshParams,
                                               out CurrencyExchange NationalCurrency, out List<CurrencyExchange> AllowedCurrencies)
    {
      return GetAllowedCurrencies(OnlyEnabled, Type, RefreshParams, false, out NationalCurrency, out AllowedCurrencies);
    }



    public static Boolean GetAllowedCurrencies(Boolean OnlyEnabled, List<CurrencyExchangeType> Type, Boolean RefreshParams, Boolean AllowCardBankTypes,
                                               out CurrencyExchange NationalCurrency, out List<CurrencyExchange> AllowedCurrencies)
    {

      return GetAllowedCurrencies(OnlyEnabled, Type, RefreshParams, AllowCardBankTypes, false, out NationalCurrency, out AllowedCurrencies);
    }

    public static Boolean GetAllowedCurrencies(Boolean OnlyEnabled, List<CurrencyExchangeType> Type, Boolean RefreshParams, Boolean AllowCardBankTypes, Boolean OnlyNational,
                                           out CurrencyExchange NationalCurrency, out List<CurrencyExchange> AllowedCurrencies)
    {
      return GetAllowedCurrencies(OnlyEnabled, Type, RefreshParams, AllowCardBankTypes, OnlyNational, true, out  NationalCurrency, out AllowedCurrencies);
    }

    // PURPOSE: Get national currency and allowed currencies
    //
    // PARAMS:
    //     - INPUT:
    //           - OnlyEnabled: If its set only return enabled currencies
    //           - Type
    //           - RefreshParams: Force to refresh general params
    //           - OnlyNational: 
    //     - OUTPUT:
    //           - NationalCurrency
    //           - AllowedCurrencies
    // RETURNS:
    //     - True: if the process was successful
    //     - False: Otherwise
    //
    public static Boolean GetAllowedCurrencies(Boolean OnlyEnabled, List<CurrencyExchangeType> Type, Boolean RefreshParams, Boolean AllowCardBankTypes, Boolean OnlyNational, Boolean AddAllowedChipsTypes,
                                               out CurrencyExchange NationalCurrency, out List<CurrencyExchange> AllowedCurrencies)
    {
      StringBuilder _sb;
      CurrencyExchange _currency_exchange;
      String[] _currencies_accepted;
      Int32 _counter;
      String _national_currency;
      String _currency_param;
      String _accepted_gp_currencies;
      String _description;
      String _types;
      CurrencyExchangeType _first_type;
      DataSet _configuration_exchange;
      List<CurrencyExchange> _chips_currency_exchange;
      List<CurrencyExchange> _others_currency_exchange;

      AllowedCurrencies = null;
      NationalCurrency = null;
      _others_currency_exchange = null;
      _first_type = CurrencyExchangeType.CURRENCY;

      if (RefreshParams)
      {
        GeneralParam.Dictionary _gp_dictionary;

        _gp_dictionary = GeneralParam.Dictionary.Create();
        _national_currency = _gp_dictionary.GetString("RegionalOptions", "CurrencyISOCode");
        _accepted_gp_currencies = _gp_dictionary.GetString("RegionalOptions", "CurrenciesAccepted");
      }
      else
      {
        _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");
        _accepted_gp_currencies = GeneralParam.GetString("RegionalOptions", "CurrenciesAccepted");
      }

      if (String.IsNullOrEmpty(_accepted_gp_currencies))
      {
        return false;
      }

      _currencies_accepted = _accepted_gp_currencies.Split(new Char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

      if (_currencies_accepted.Length == 0)
      {
        return false;
      }

      // Set Allowed Currencies
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand())
          {

            _sb = new StringBuilder();
            _others_currency_exchange = new List<CurrencyExchange>();

            _sb.AppendLine("SELECT   CE_TYPE                   ");
            _sb.AppendLine("       , CE_CURRENCY_ISO_CODE      ");
            _sb.AppendLine("       , ISNULL(CE_DESCRIPTION,'') AS CE_DESCRIPTION ");
            _sb.AppendLine("       , CE_CHANGE                 ");
            _sb.AppendLine("       , CE_NUM_DECIMALS           ");
            _sb.AppendLine("       , CE_VARIABLE_COMMISSION    ");
            _sb.AppendLine("       , CE_FIXED_COMMISSION       ");
            _sb.AppendLine("       , CE_VARIABLE_NR2           ");
            _sb.AppendLine("       , CE_FIXED_NR2              ");
            _sb.AppendLine("       , CE_STATUS                 ");
            _sb.AppendLine("       , CE_FORMATTED_DECIMALS     ");
            _sb.AppendLine("       , CE_CONFIGURATION          ");
            _sb.AppendLine("  FROM   CURRENCY_EXCHANGE         ");
            _sb.AppendLine(" WHERE   CE_CURRENCY_ISO_CODE IN ( ");

            _counter = 0;

            if (OnlyNational)
            {
              _currency_param = String.Format("@pCurrency{0}", _counter);
              _cmd.Parameters.Add(_currency_param, SqlDbType.NVarChar).Value = _national_currency.Trim();
              _sb.AppendLine(_currency_param);
              _counter++;
            }
            else
            {
              foreach (String _currency in _currencies_accepted)
              {
                _currency_param = String.Format("@pCurrency{0}", _counter);
                _cmd.Parameters.Add(_currency_param, SqlDbType.NVarChar).Value = _currency.Trim();

                if (_counter > 0)
                {
                  _sb.Append(", ");
                }
                _sb.AppendLine(_currency_param);

                _counter++;
              }
            }

            _sb.AppendLine(" ) ");

            if (OnlyEnabled)
            {
              _sb.AppendLine(" AND   CE_STATUS = 1 ");
            }

            if (Type != null)
            {
              _types = "";
              foreach (CurrencyExchangeType _cet in Type)
              {
                if (_types == "")
                {
                  _first_type = _cet;
                }
                _types += ", " + (Int32)_cet;
              }
              _types = _types.TrimStart(',');
              _sb.AppendLine(" AND   CE_TYPE IN (" + _types + ") ");
            }
            _sb.AppendLine(" ORDER BY CE_CURRENCY_ORDER, CE_CURRENCY_ISO_CODE");

            _cmd.CommandText = _sb.ToString();

            using (SqlDataReader _reader = _db_trx.ExecuteReader(_cmd))
            {
              while (_reader.Read())
              {
                _currency_exchange = new CurrencyExchange();

                _currency_exchange.Type = (CurrencyExchangeType)_reader.GetInt32(0);
                _currency_exchange.CurrencyCode = _reader.GetString(1);
                _description = _reader.GetString(2);
                _currency_exchange.Description = GetDescription(_currency_exchange.Type, _currency_exchange.CurrencyCode, _description);

                if (_reader.IsDBNull(3))
                {
                  _currency_exchange.ChangeRate = null;
                }
                else
                {
                  _currency_exchange.ChangeRate = _reader.GetDecimal(3);
                }

                if (_reader.IsDBNull(4))
                {
                  _currency_exchange.Decimals = null;
                }
                else
                {
                  _currency_exchange.Decimals = _reader.GetInt32(4);
                }

                _currency_exchange.VariableComission = null;
                if (GeneralParam.GetInt32("Cashier.PaymentMethod", "DifferentiateComission", 0) == 0 || _currency_exchange.Type != CurrencyExchangeType.CARD)
                {
                  if (!_reader.IsDBNull(11) && !String.IsNullOrEmpty(_reader.GetString(11)))
                  {
                    _configuration_exchange = CurrencyConfigurationFromXml(_reader.GetString(11));
                    if (_configuration_exchange.Tables[CONFIGURATION_TABLE_NAME].Rows.Count > 0)
                    {
                      _currency_exchange.VariableComission = Convert.ToDecimal(_configuration_exchange.Tables[CONFIGURATION_TABLE_NAME].Rows[0]["variable_commission"]);
                      _currency_exchange.FixedComission = Convert.ToDecimal(_configuration_exchange.Tables[CONFIGURATION_TABLE_NAME].Rows[0]["fixed_commission"]);
                      _currency_exchange.VariableNR2 = Convert.ToDecimal(_configuration_exchange.Tables[CONFIGURATION_TABLE_NAME].Rows[0]["variable_nr2"]);
                      _currency_exchange.FixedNR2 = Convert.ToDecimal(_configuration_exchange.Tables[CONFIGURATION_TABLE_NAME].Rows[0]["fixed_nr2"]);
                      _currency_exchange.m_bank_card.card_type = (CARD_TYPE)_configuration_exchange.Tables[CONFIGURATION_TABLE_NAME].Rows[0]["type"];
                      _currency_exchange.m_bank_card.card_scope = (CARD_SCOPE)_configuration_exchange.Tables[CONFIGURATION_TABLE_NAME].Rows[0]["scope"];
                    }
                  }
                }

                if (_currency_exchange.VariableComission == null)
                {
                  _currency_exchange.FixedComission = null;
                  _currency_exchange.VariableNR2 = null;
                  _currency_exchange.FixedNR2 = null;
                  _currency_exchange.m_bank_card.card_type = CARD_TYPE.NONE;
                  _currency_exchange.m_bank_card.card_scope = CARD_SCOPE.NONE;
                }


                //if (_reader.IsDBNull(5))
                //{
                //  _currency_exchange.VariableComission = null;
                //}
                //else
                //{
                //  _currency_exchange.VariableComission = _reader.GetDecimal(5);
                //}

                //if (_reader.IsDBNull(6))
                //{
                //  _currency_exchange.FixedComission = null;
                //}
                //else
                //{
                //  _currency_exchange.FixedComission = _reader.GetDecimal(6);
                //}

                //if (_reader.IsDBNull(7))
                //{
                //  _currency_exchange.VariableNR2 = null;
                //}
                //else
                //{
                //  _currency_exchange.VariableNR2 = _reader.GetDecimal(7);
                //}

                //if (_reader.IsDBNull(8))
                //{
                //  _currency_exchange.FixedNR2 = null;
                //}
                //else
                //{
                //  _currency_exchange.FixedNR2 = _reader.GetDecimal(8);
                //}

                _currency_exchange.Status = _reader.GetBoolean(9);

                if (_reader.IsDBNull(10))
                {
                  _currency_exchange.FormattedDecimals = DEFAULT_FORMATTED_DECIMALS;
                }
                else
                {
                  _currency_exchange.FormattedDecimals = _reader.GetInt32(10);
                }

                if (_currency_exchange.CurrencyCode == _national_currency && _currency_exchange.Type == CurrencyExchangeType.CURRENCY)
                {
                  // Set National Currency and put in the first position
                  NationalCurrency = _currency_exchange;
                  _others_currency_exchange.Insert(0, _currency_exchange);
                }
                else
                {
                  // RMS 08-AUG-2014
                  if (_currency_exchange.Type == CurrencyExchangeType.CARD && AllowCardBankTypes && GeneralParam.GetInt32("Cashier.PaymentMethod", "DifferentiateComission", 0) == 0)
                  {
                    CurrencyExchange _bank_credit_card_currency;
                    CurrencyExchange _bank_debit_card_currency;

                    _bank_credit_card_currency = _currency_exchange.Copy();
                    _bank_credit_card_currency.SubType = CurrencyExchangeSubType.CREDIT_CARD;
                    _bank_credit_card_currency.Description = Resource.String("STR_FRM_AMOUNT_BANK_CREDIT_CARD");

                    _bank_debit_card_currency = _currency_exchange.Copy();
                    _bank_debit_card_currency.SubType = CurrencyExchangeSubType.DEBIT_CARD;
                    _bank_debit_card_currency.Description = Resource.String("STR_FRM_AMOUNT_BANK_DEBIT_CARD");

                    _others_currency_exchange.Add(_bank_credit_card_currency);
                    _others_currency_exchange.Add(_bank_debit_card_currency);
                  }
                  else
                  {
                    _others_currency_exchange.Add(_currency_exchange);
                  }
                }
              }
            }
          }// _cmd
        }// DB_TRX

        if (NationalCurrency == null)
        {
          NationalCurrency = new CurrencyExchange();
          NationalCurrency.Type = CurrencyExchangeType.CURRENCY;
          NationalCurrency.CurrencyCode = _national_currency;
        }

        AllowedCurrencies = new List<CurrencyExchange>();

        if (_first_type != CurrencyExchangeType.CASINOCHIP)
        {
          AllowedCurrencies.AddRange(_others_currency_exchange);
        }
        // DHA 09-MAR-2016: added allowed chips
        if (AddAllowedChipsTypes)
        {
          _chips_currency_exchange = GetAllowedChipsTypes(_others_currency_exchange);
          AllowedCurrencies.AddRange(_chips_currency_exchange);
        }

        if (_first_type == CurrencyExchangeType.CASINOCHIP)
        {
          AllowedCurrencies.AddRange(_others_currency_exchange);
        }

        return true;
      }
      catch (SqlException _sql_ex)
      {
        Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        AllowedCurrencies = new List<CurrencyExchange>();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        AllowedCurrencies = new List<CurrencyExchange>();
      }

      return false;
    } // GetAllowedCurrencies


    // PURPOSE: check is if correct the currency exchange configuration
    public static bool IsValidCurrencyExchange()
    {
      List<CurrencyExchangeType> _types;
      CurrencyExchange _national;
      List<CurrencyExchange> _currencies;

      _currencies = null;
      _types = new List<CurrencyExchangeType>();

      _types.Add(CurrencyExchangeType.CURRENCY);

      CurrencyExchange.GetAllowedCurrencies(true, _types, false, false, false, false, out _national, out _currencies);
      _currencies.Remove(_national);


      return _currencies.Count > 0 ? true : false;

    }

    // PURPOSE: Get national currency and allowed currencies for the specified cashier session
    //
    // PARAMS:
    //     - INPUT:
    //           - CashierSessionId
    //     - OUTPUT:
    //           - NationalCurrency
    //           - AllowedCurrencies
    // RETURNS:
    //     - True: if the process was successful
    //     - False: Otherwise
    //
    public static Boolean GetAllowedCurrenciesForCashierSession(Int64 CashierSessionId, Boolean AllSessionCurrenciesTypes, out CurrencyExchange NationalCurrency, out List<CurrencyExchange> AllowedCurrenciesSession)
    {
      StringBuilder _sb;
      CurrencyExchange _currency_exchange;
      String _national_currency;
      String _description;
      DataSet _configuration_exchange;
      ClosingStocks _closing_stock;
      CashierSessionInfo _cashier_session_info;

      NationalCurrency = null;
      AllowedCurrenciesSession = null;

      _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");

      // Set Allowed Currencies in session
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          Cashier.GetCashierSessionById(CashierSessionId, out _cashier_session_info, _db_trx.SqlTransaction);
          _closing_stock = new ClosingStocks(_cashier_session_info.TerminalId, _db_trx.SqlTransaction);

          _sb = new StringBuilder();
          AllowedCurrenciesSession = new List<CurrencyExchange>();

          _sb.AppendLine("SELECT   CE_TYPE ");
          _sb.AppendLine("       , CE_CURRENCY_ISO_CODE ");
          _sb.AppendLine("       , ISNULL(CE_DESCRIPTION,'') AS CE_DESCRIPTION  ");
          _sb.AppendLine("       , CE_CHANGE ");
          _sb.AppendLine("       , CE_NUM_DECIMALS ");
          _sb.AppendLine("       , CE_VARIABLE_COMMISSION ");
          _sb.AppendLine("       , CE_FIXED_COMMISSION ");
          _sb.AppendLine("       , CE_VARIABLE_NR2 ");
          _sb.AppendLine("       , CE_FIXED_NR2 ");
          _sb.AppendLine("       , CE_STATUS ");
          _sb.AppendLine("       , CAST(CE_CONFIGURATION AS NVARCHAR(MAX) ) AS CE_CONFIGURATION ");
          _sb.AppendLine("  FROM   CURRENCY_EXCHANGE ");
          _sb.AppendLine(" WHERE ( ( CE_CURRENCY_ISO_CODE = @pNationalCurrency  ");
          _sb.AppendLine("   AND   CE_TYPE  = @pTypeCurrency ) ");

          // LTC 05-AGO-2016
          if (Misc.IsBankCardWithdrawalEnabled() || (GetBankEnabled() && _closing_stock.SleepsOnTable))
          {
            _sb.AppendLine("     OR  ( CE_CURRENCY_ISO_CODE = @pNationalCurrency ");
            _sb.AppendLine("     AND   CE_TYPE  IN (@pTypeCard) ) ");
          }

          _sb.AppendLine("       ) ");
          _sb.AppendLine("    OR   EXISTS ");
          _sb.AppendLine("         (SELECT   1 ");
          _sb.AppendLine("             FROM  CASHIER_SESSIONS_BY_CURRENCY   ");
          _sb.AppendLine("            WHERE  CSC_SESSION_ID       = @pSessionId ");
          if (!AllSessionCurrenciesTypes)
          {
            _sb.AppendLine("            AND  CSC_TYPE             IN (@pTypeCurrency, @pTypeCheck) ");
          }
          _sb.AppendLine("              AND  CE_TYPE              = CSC_TYPE ");
          _sb.AppendLine("              AND  CSC_BALANCE          > 0 ");
          _sb.AppendLine("              AND  CE_CURRENCY_ISO_CODE = CSC_ISO_CODE ) ");

          // Add chips RE & NR
          _sb.AppendLine(" UNION ");
          _sb.AppendLine("SELECT   CSC_TYPE AS CE_TYPE ");
          _sb.AppendLine("       , CE_CURRENCY_ISO_CODE ");
          _sb.AppendLine("       , ISNULL('Fichas ' + CE_CURRENCY_ISO_CODE + '-' + CONVERT(varchar(12),CSC_TYPE, 101),'') AS CE_DESCRIPTION   ");
          _sb.AppendLine("       , CE_CHANGE ");
          _sb.AppendLine("       , CE_NUM_DECIMALS ");
          _sb.AppendLine("       , CE_VARIABLE_COMMISSION ");
          _sb.AppendLine("       , CE_FIXED_COMMISSION ");
          _sb.AppendLine("       , CE_VARIABLE_NR2 ");
          _sb.AppendLine("       , CE_FIXED_NR2 ");
          _sb.AppendLine("       , CE_STATUS ");
          _sb.AppendLine("       , CAST(CE_CONFIGURATION AS NVARCHAR(MAX) ) AS CE_CONFIGURATION ");
          _sb.AppendLine("  FROM   CASHIER_SESSIONS_BY_CURRENCY ");
          _sb.AppendLine(" INNER   JOIN CURRENCY_EXCHANGE ON CE_CURRENCY_ISO_CODE = CSC_ISO_CODE AND CE_TYPE = @pTypeCurrency ");
          _sb.AppendLine(" WHERE   CSC_SESSION_ID = @pSessionId ");
          _sb.AppendLine("         AND  CSC_TYPE IN (@pTypeChipsRE, @pTypeChipsNR) ");

          // Add chips Color
          _sb.AppendLine(" UNION ");
          _sb.AppendLine("SELECT   CSC_TYPE AS CE_TYPE ");
          _sb.AppendLine("       , 'X02' AS CE_CURRENCY_ISO_CODE ");
          _sb.AppendLine("       , ISNULL('Fichas Color','') AS CE_DESCRIPTION  ");
          _sb.AppendLine("       , 1 AS CE_CHANGE ");
          _sb.AppendLine("       , 0 AS CE_NUM_DECIMALS ");
          _sb.AppendLine("       , 0 AS CE_VARIABLE_COMMISSION ");
          _sb.AppendLine("       , 0 AS CE_FIXED_COMMISSION ");
          _sb.AppendLine("       , 0 AS CE_VARIABLE_NR2 ");
          _sb.AppendLine("       , 0 AS CE_FIXED_NR2 ");
          _sb.AppendLine("       , CAST(1 As bit) AS CE_STATUS ");
          _sb.AppendLine("       , NULL AS CE_CONFIGURATION ");
          _sb.AppendLine("  FROM   CASHIER_SESSIONS_BY_CURRENCY  ");
          _sb.AppendLine(" WHERE   CSC_SESSION_ID = @pSessionId ");
          _sb.AppendLine("         AND  CSC_TYPE IN (@pTypeChipsColor) ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pNationalCurrency", SqlDbType.VarChar).Value = _national_currency;
            _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CashierSessionId;
            _cmd.Parameters.Add("@pTypeCurrency", SqlDbType.Int).Value = CurrencyExchangeType.CURRENCY;
            _cmd.Parameters.Add("@pTypeChipsRE", SqlDbType.Int).Value = CurrencyExchangeType.CASINO_CHIP_RE;
            _cmd.Parameters.Add("@pTypeChipsNR", SqlDbType.Int).Value = CurrencyExchangeType.CASINO_CHIP_NRE;
            _cmd.Parameters.Add("@pTypeChipsColor", SqlDbType.Int).Value = CurrencyExchangeType.CASINO_CHIP_COLOR;

            if (!AllSessionCurrenciesTypes)
            {
              _cmd.Parameters.Add("@pTypeCheck", SqlDbType.Int).Value = CurrencyExchangeType.CHECK;
            }
            // LTC 05-AGO-2016
            if (Misc.IsBankCardWithdrawalEnabled() || (GetBankEnabled() && _closing_stock.SleepsOnTable))
            {
              _cmd.Parameters.Add("@pTypeCard", SqlDbType.Int).Value = CurrencyExchangeType.CARD;
            }

            using (SqlDataReader _reader = _db_trx.ExecuteReader(_cmd))
            {
              while (_reader.Read())
              {
                _currency_exchange = new CurrencyExchange();

                _currency_exchange.Type = (CurrencyExchangeType)_reader.GetInt32(0);
                _currency_exchange.CurrencyCode = _reader.GetString(1);
                _description = _reader.GetString(2);
                _currency_exchange.Description = GetDescription(_currency_exchange.Type, _currency_exchange.CurrencyCode, _description);

                if (_reader.IsDBNull(3))
                {
                  _currency_exchange.ChangeRate = null;
                }
                else
                {
                  _currency_exchange.ChangeRate = _reader.GetDecimal(3);
                }

                if (_reader.IsDBNull(4))
                {
                  _currency_exchange.Decimals = null;
                }
                else
                {
                  _currency_exchange.Decimals = _reader.GetInt32(4);
                }

                //if (_reader.IsDBNull(5))
                //{
                //  _currency_exchange.VariableComission = null;
                //}
                //else
                //{
                //  _currency_exchange.VariableComission = _reader.GetDecimal(5);
                //}

                //if (_reader.IsDBNull(6))
                //{
                //  _currency_exchange.FixedComission = null;
                //}
                //else
                //{
                //  _currency_exchange.FixedComission = _reader.GetDecimal(6);
                //}

                //if (_reader.IsDBNull(7))
                //{
                //  _currency_exchange.VariableNR2 = null;
                //}
                //else
                //{
                //  _currency_exchange.VariableNR2 = _reader.GetDecimal(7);
                //}

                //if (_reader.IsDBNull(8))
                //{
                //  _currency_exchange.FixedNR2 = null;
                //}
                //else
                //{
                //  _currency_exchange.FixedNR2 = _reader.GetDecimal(8);
                //}

                _currency_exchange.VariableComission = null;
                if (GeneralParam.GetInt32("Cashier.PaymentMethod", "DifferentiateComission", 0) == 0 || _currency_exchange.Type != CurrencyExchangeType.CARD)
                {
                  if (!_reader.IsDBNull(10) && !String.IsNullOrEmpty(_reader.GetString(10)))
                  {
                    _configuration_exchange = CurrencyConfigurationFromXml(_reader.GetString(10));
                    if (_configuration_exchange.Tables[CONFIGURATION_TABLE_NAME].Rows.Count > 0)
                    {
                      _currency_exchange.VariableComission = Convert.ToDecimal(_configuration_exchange.Tables[CONFIGURATION_TABLE_NAME].Rows[0]["variable_commission"]);
                      _currency_exchange.FixedComission = Convert.ToDecimal(_configuration_exchange.Tables[CONFIGURATION_TABLE_NAME].Rows[0]["fixed_commission"]);
                      _currency_exchange.VariableNR2 = Convert.ToDecimal(_configuration_exchange.Tables[CONFIGURATION_TABLE_NAME].Rows[0]["variable_nr2"]);
                      _currency_exchange.FixedNR2 = Convert.ToDecimal(_configuration_exchange.Tables[CONFIGURATION_TABLE_NAME].Rows[0]["fixed_nr2"]);
                      _currency_exchange.m_bank_card.card_type = (CARD_TYPE)_configuration_exchange.Tables[CONFIGURATION_TABLE_NAME].Rows[0]["type"];
                      _currency_exchange.m_bank_card.card_scope = (CARD_SCOPE)_configuration_exchange.Tables[CONFIGURATION_TABLE_NAME].Rows[0]["scope"];
                    }
                  }

                }
                if (_currency_exchange.VariableComission == null)
                {
                  _currency_exchange.FixedComission = null;
                  _currency_exchange.VariableNR2 = null;
                  _currency_exchange.FixedNR2 = null;
                  _currency_exchange.m_bank_card.card_type = CARD_TYPE.NONE;
                  _currency_exchange.m_bank_card.card_scope = CARD_SCOPE.NONE;
                }


                _currency_exchange.Status = _reader.GetBoolean(9);

                if (_currency_exchange.CurrencyCode == _national_currency && _currency_exchange.Type == CurrencyExchangeType.CURRENCY)
                {
                  // Set National Currency and put in the first position
                  NationalCurrency = _currency_exchange;
                  AllowedCurrenciesSession.Insert(0, _currency_exchange);
                }
                else
                {
                  AllowedCurrenciesSession.Add(_currency_exchange);
                }

              }
            }
          }
        }// DB_TRX

        if (NationalCurrency == null)
        {
          NationalCurrency = new CurrencyExchange();
          NationalCurrency.Type = CurrencyExchangeType.CURRENCY;
          NationalCurrency.CurrencyCode = _national_currency;
        }

        foreach (CurrencyExchange _item in AllowedCurrenciesSession)
        {
          if (FeatureChips.IsChipsType(_item.Type))
          {
            _item.Description = FeatureChips.GetChipTypeDescription(_item.Type, _item.m_currency_code);
          }
        }

        return true;
      }
      catch (SqlException _sql_ex)
      {
        Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        AllowedCurrenciesSession = new List<CurrencyExchange>();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        AllowedCurrenciesSession = new List<CurrencyExchange>();
      }

      return false;
    } // GetAllowedCurrenciesForCashierSession

    /// <summary>
    /// Get value of BankCard.Enabled
    /// </summary>
    /// <returns></returns>
    private static Boolean GetBankEnabled()
    {
      CurrencyExchange m_currency_exchange;
      //If the function returns a true, bankcard enabled is true
      return (CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CARD, CurrencyExchange.GetNationalCurrency(), out m_currency_exchange) ||
              CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CHECK, CurrencyExchange.GetNationalCurrency(), out m_currency_exchange));       
    }

    // PURPOSE: Add allowed chips
    //
    // PARAMS:
    //     - INPUT:
    //           - AllowedCurrencies
    //
    // RETURNS:
    //

    private static List<CurrencyExchange> GetAllowedChipsTypes(List<CurrencyExchange> AllowedCurrencies)
    {
      List<CurrencyExchange> _chips_currency_exchange;
      CurrencyExchange _currency_exchange;

      _currency_exchange = null;
      _chips_currency_exchange = new List<CurrencyExchange>();

      if (AllowedCurrencies == null)
      {
        return null;
      }

      if (FeatureChips.IsFeatureChipsEnabled)
      {
        // Add Chips
        foreach (CurrencyExchange _item in AllowedCurrencies)
        {
          if (_item.Type == CurrencyExchangeType.CURRENCY)
          {

            // Add value chips
            if (FeatureChips.IsChipsValueTypeEnabled)
            {
              _currency_exchange = _item.Copy();
              _currency_exchange.Type = CurrencyExchangeType.CASINO_CHIP_RE;
              _currency_exchange.Description = FeatureChips.GetChipTypeDescription(_currency_exchange.Type, _currency_exchange.m_currency_code);

              _chips_currency_exchange.Add(_currency_exchange);
            }

            // Add no redeemable chips
            if (FeatureChips.IsChipsNonRedeemableTypeEnabled)
            {
              _currency_exchange = _item.Copy();
              _currency_exchange.Type = CurrencyExchangeType.CASINO_CHIP_NRE;
              _currency_exchange.Description = FeatureChips.GetChipTypeDescription(_currency_exchange.Type, _currency_exchange.m_currency_code);

              _chips_currency_exchange.Add(_currency_exchange);
            }

            // Add color chips
            if (FeatureChips.IsChipsColorTypeEnabled && _item.m_currency_code == CurrencyExchange.GetNationalCurrency())
            {
              _currency_exchange = _item.Copy();
              _currency_exchange.Type = CurrencyExchangeType.CASINO_CHIP_COLOR;
              _currency_exchange.m_currency_code = Cage.CHIPS_COLOR;
              _currency_exchange.Description = FeatureChips.GetChipTypeDescription(_currency_exchange.Type, _currency_exchange.m_currency_code);

              _chips_currency_exchange.Add(_currency_exchange);
            }
          }
        }
      }

      return _chips_currency_exchange;
    }

    // PURPOSE: Get national currency and foreign currency to fill a Combo
    //          Each DataRow has a CURRENCY_ID (RowNumber on database) and CE_CURRENCY_ISO_CODE
    //          First DataRow is the National Currency
    // PARAMS:
    //     - INPUT:
    //     - OUTPUT:
    //            - out DataTable
    // RETURNS:
    //     - True: if the process was successful
    //     - False: Otherwise
    //
    public static Boolean GetFloorDualCurrenciesForCombo(out DataTable Table)
    {
      StringBuilder _sb;
      SqlCommand _sql_cmd;
      DataRow _national_currency_row;
      Boolean _error;
      String _national_currency;

      Table = new DataTable();
      _error = true;
      _national_currency = CurrencyExchange.GetNationalCurrency();
      try
      {
        if (WSI.Common.Misc.IsFloorDualCurrencyEnabled())
        {
          // Get foreign currency
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _sb = new StringBuilder();
            _sb.AppendLine("  SELECT (ROW_NUMBER() OVER(ORDER BY CE_CURRENCY_ISO_CODE)) as CURRENCY_ID ");
            _sb.AppendLine("       , CE_CURRENCY_ISO_CODE");
            _sb.AppendLine("  FROM CURRENCY_EXCHANGE");
            _sb.AppendLine("  WHERE CE_TYPE = @pCurrencyType");
            _sb.AppendLine("       AND CE_STATUS = 1");
            _sb.AppendLine("       AND CE_CURRENCY_ISO_CODE <> @pNationalCurrencyIso");
            _sb.AppendLine("  ORDER BY CE_CURRENCY_ISO_CODE ASC");

            using (_sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _sql_cmd.Parameters.Add("@pCurrencyType", SqlDbType.Int).Value = (Int32)CurrencyExchangeType.CURRENCY;
              _sql_cmd.Parameters.Add("@pNationalCurrencyIso", SqlDbType.NVarChar).Value = _national_currency;
              using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
              {
                _sql_da.Fill(Table);
              }
            }
          }
        }
        else
        {
          DataColumn _col_id = new DataColumn("CURRENCY_ID", typeof(Int32));
          DataColumn _col_code = new DataColumn("CE_CURRENCY_ISO_CODE", typeof(String));
          Table.Columns.Add(_col_id);
          Table.Columns.Add(_col_code);
        }
        _error = false;
      }
      catch (SqlException _sql_ex)
      {
        Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      // Insert national currency at first row
      _national_currency_row = Table.NewRow();
      _national_currency_row["CURRENCY_ID"] = 0;
      _national_currency_row["CE_CURRENCY_ISO_CODE"] = _national_currency;
      Table.Rows.InsertAt(_national_currency_row, 0);

      return _error;
    }

    // PURPOSE: Check if there are enabled currencies
    //
    // PARAMS:
    //     - INPUT:
    //          - OnlyCurrencies: Only currencies, not credit card
    //
    //     - OUTPUT:
    //
    // RETURNS:
    //     - True: if there is any enabled currency other national currency
    //     - False: Otherwise
    //
    public static Boolean AreCurrenciesEnabled(Boolean OnlyCurrencies)
    {
      StringBuilder _sb;
      String[] _currencies_accepted;
      Int32 _counter;
      String _national_currency;
      String _accepted_gp_currencies;
      Int32 _count_currencies;

      _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");
      _accepted_gp_currencies = GeneralParam.GetString("RegionalOptions", "CurrenciesAccepted");

      if (String.IsNullOrEmpty(_accepted_gp_currencies))
      {
        return false;
      }

      _currencies_accepted = _accepted_gp_currencies.Split(new Char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

      if (_currencies_accepted.Length == 0)
      {
        return false;
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand())
          {
            _sb = new StringBuilder();
            _sb.AppendLine("SELECT   COUNT(*) ");
            _sb.AppendLine("  FROM   CURRENCY_EXCHANGE  ");
            _sb.AppendLine(" WHERE   CE_STATUS = @pStatus  ");
            if (OnlyCurrencies)
            {
              _sb.AppendLine(" AND   CE_TYPE IN (@pCurrency)  ");
            }
            _sb.AppendLine("   AND   CE_CURRENCY_ISO_CODE IN (                ");

            _counter = 0;
            foreach (String _currency in _currencies_accepted)
            {
              if (_counter > 0)
              {
                _sb.Append(", ");
              }
              _sb.Append(String.Format("@pCurrency{0}", _counter));
              _cmd.Parameters.Add(String.Format("@pCurrency{0}", _counter), SqlDbType.NVarChar).Value = _currency;

              _counter++;
            }

            _sb.AppendLine(" ) ");

            _cmd.Parameters.Add("@pCurrency", SqlDbType.Int).Value = CurrencyExchangeType.CURRENCY;
            _cmd.Parameters.Add("@pCasinoChip", SqlDbType.Int).Value = CurrencyExchangeType.CASINOCHIP;
            _cmd.Parameters.Add("@pStatus", SqlDbType.Bit).Value = true;

            _cmd.CommandText = _sb.ToString();

            _count_currencies = (Int32)_db_trx.ExecuteScalar(_cmd);
          }

          // DHA 09-MAR-2016: count chips type
          foreach (String _currency in _currencies_accepted)
          {
            if (FeatureChips.IsChipsValueTypeEnabled)
            {
              _count_currencies++;
            }

            if (FeatureChips.IsChipsNonRedeemableTypeEnabled)
            {
              _count_currencies++;
            }
          }

          if (FeatureChips.IsChipsColorTypeEnabled)
          {
            _count_currencies++;
          }

          //Only national currency enabled
          return (_count_currencies > 1);
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }//AreCurrenciesEnabled

    // PURPOSE: Check if there are enabled currencies for the specified casher session
    //
    // PARAMS:
    //     - INPUT:
    //          - CashierSessionId
    //
    //     - OUTPUT:
    //
    // RETURNS:
    //     - True: if there is any enabled currency other national currency
    //     - False: Otherwise
    //
    public static Boolean AreCurrenciesEnabledForCashierSession(Int64 CashierSessionId)
    {
      StringBuilder _sb;
      Int32 _count_currencies;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine("SELECT   COUNT(*) ");
          _sb.AppendLine("  FROM   CASHIER_SESSIONS_BY_CURRENCY ");
          _sb.AppendLine(" WHERE   CSC_SESSION_ID = @pSessionId ");
          _sb.AppendLine("   AND   CSC_TYPE       = @pType ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
          {
            _cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = CashierSessionId;
            _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = CurrencyExchangeType.CURRENCY;

            _count_currencies = (Int32)_db_trx.ExecuteScalar(_cmd);
          }

          return (_count_currencies > 0);
        }
      }
      catch (SqlException _sql_ex)
      {
        Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }//AreCurrenciesEnabledForCashierSession


    // PURPOSE: Get only one currency
    //
    // PARAMS:
    //     - INPUT:
    //           - Type: Currency type
    //           - CurrencyCode: Iso Code
    //     - OUTPUT:
    //           - CurrencyExchange
    //
    // RETURNS:
    //     - True: if the process was successful
    //     - False: Otherwise
    //
    public static Boolean ReadCurrencyExchange(CurrencyExchangeType Type, String CurrencyCode, out CurrencyExchange CurrencyExchange)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return ReadCurrencyExchange(Type, CurrencyCode, out CurrencyExchange, _db_trx.SqlTransaction);
      }
    }

    public static Boolean ReadCurrencyExchange(CurrencyExchangeType Type, String CurrencyCode, out CurrencyExchange CurrencyExchange, SqlTransaction Trx)
    {
      bank_card _bank_card;
      _bank_card = new bank_card();
      _bank_card.Initialize(CARD_TYPE.NONE, CARD_SCOPE.NONE);

      return ReadCurrencyExchange(Type, CurrencyCode, _bank_card, out CurrencyExchange, Trx);
    }

    public static Boolean ReadCurrencyExchange(CurrencyExchangeType Type, String CurrencyCode, bank_card CardType, out CurrencyExchange CurrencyExchange)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return ReadCurrencyExchange(Type, CurrencyCode, CardType, out CurrencyExchange, _db_trx.SqlTransaction);
      }
    }



    public static Boolean ReadCurrencyExchange(CurrencyExchangeType ExchangeType, String CurrencyCode, bank_card CardType, out CurrencyExchange CurrencyExchange, SqlTransaction Trx)
    {
      StringBuilder _sb;
      String _description;
      String _table_name;
      DataSet _language_exchange;
      DataSet _configuration_exchange;

      CurrencyExchange = new CurrencyExchange();
      CurrencyExchange.LanguageResource = new LanguageResources();
      _language_exchange = new DataSet();
      _configuration_exchange = new DataSet();
      _table_name = String.Empty;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   ISNULL(CE_DESCRIPTION,'') AS CE_DESCRIPTION     ");
        _sb.AppendLine("       , CE_CHANGE                                       ");
        _sb.AppendLine("       , CE_NUM_DECIMALS                                 ");
        _sb.AppendLine("       , CE_VARIABLE_COMMISSION                          ");
        _sb.AppendLine("       , CE_FIXED_COMMISSION                             ");
        _sb.AppendLine("       , CE_VARIABLE_NR2                                 ");
        _sb.AppendLine("       , CE_FIXED_NR2                                    ");
        _sb.AppendLine("       , CE_STATUS                                       ");
        _sb.AppendLine("       , ISNULL(CE_LANGUAGE_RESOURCES,'')                ");
        _sb.AppendLine("       , CE_CONFIGURATION                                ");
        _sb.AppendLine("  FROM   CURRENCY_EXCHANGE                               ");
        _sb.AppendLine(" WHERE   CE_TYPE = @pType                                ");
        _sb.AppendLine("   AND   CE_CURRENCY_ISO_CODE = @pCode                   ");



        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = ExchangeType;
          _cmd.Parameters.Add("@pCode", SqlDbType.NVarChar).Value = CurrencyCode;
          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              // there are not currency exchanges! 
              return false;
            }

            CurrencyExchange.Status = _reader.GetBoolean(7);

            if (!_reader.IsDBNull(9) && !String.IsNullOrEmpty(_reader.GetString(9)))
            {
              String _condition;
              DataRow[] _selection;
              _configuration_exchange = CurrencyConfigurationFromXml(_reader.GetString(9));


              _condition = String.Format("type = '{0}' and scope = '{1}'", (Int32)CardType.card_type, (Int32)CardType.card_scope);
              _selection = _configuration_exchange.Tables[CONFIGURATION_TABLE_NAME].Select(_condition);

              if (_selection.Length == 0)
              {
                Log.Warning("ReadCurrencyExchange: Charged default configuration: Needs to configure Credit Card Data.");
                _selection = _configuration_exchange.Tables[CONFIGURATION_TABLE_NAME].Select();
                CurrencyExchange.Status = false;
                if (_selection.Length == 0)
                {
                  return false;
                }
              }
              CurrencyExchange.VariableComission = Convert.ToDecimal(_selection[0]["variable_commission"]);
              CurrencyExchange.FixedComission = Convert.ToDecimal(_selection[0]["fixed_commission"]);
              CurrencyExchange.VariableNR2 = Convert.ToDecimal(_selection[0]["variable_nr2"]);
              CurrencyExchange.FixedNR2 = Convert.ToDecimal(_selection[0]["fixed_nr2"]);
              CurrencyExchange.m_bank_card.card_type = CardType.card_type;
              CurrencyExchange.m_bank_card.card_scope = CardType.card_scope;
            }
            CurrencyExchange.Type = ExchangeType;
            CurrencyExchange.CurrencyCode = CurrencyCode;
            _description = _reader.GetString(0);
            CurrencyExchange.Description = CurrencyExchange.GetDescription(CurrencyExchange.Type, CurrencyExchange.CurrencyCode, _description);
            CurrencyExchange.ChangeRate = _reader.GetDecimal(1);
            CurrencyExchange.Decimals = _reader.GetInt32(2);
            //CurrencyExchange.VariableComission = _reader.GetDecimal(3);
            //CurrencyExchange.FixedComission = _reader.GetDecimal(4);
            //CurrencyExchange.VariableNR2 = _reader.GetDecimal(5);
            //CurrencyExchange.FixedNR2 = _reader.GetDecimal(6);


            if (!_reader.IsDBNull(8) && !String.IsNullOrEmpty(_reader.GetString(8)))
            {
              Misc.DataSetFromXml(_language_exchange, _reader.GetString(8), XmlReadMode.Auto);

              _table_name = "NLS" + Resource.LanguageId.ToString("00"); // "NLS10" : "NLS09"
              CurrencyExchange.LanguageResource.PluralCurrency = _language_exchange.Tables[_table_name].Rows[0]["PluralCurrency"].ToString();
              CurrencyExchange.LanguageResource.SingularCurrency = _language_exchange.Tables[_table_name].Rows[0]["SingularCurrency"].ToString();
              CurrencyExchange.LanguageResource.PluralCents = _language_exchange.Tables[_table_name].Rows[0]["PluralCents"].ToString();
              CurrencyExchange.LanguageResource.SingularCents = _language_exchange.Tables[_table_name].Rows[0]["SingularCents"].ToString();
            }

          }
        }

        return true;
      }
      catch (SqlException _sql_ex)
      {
        Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // ReadCurrencyExchange




    // PURPOSE: Sets currency exchange description for a Datatable. If exists NLS use it, if not use the description provided by the DB
    //
    // PARAMS:
    //     - INPUT:
    //          - Currencies: Datatable with rows to set the description
    //          - IsCageAmount: If the call comes from CageAmount
    //
    //     - OUTPUT:
    //          - Currencies: the content of the datatable is updated with the correct description
    //
    // RETURNS:
    //
    public static void GetTableDescription(DataTable Currencies, Boolean IsCageAmount)
    {
      CurrencyExchangeType _type;
      String _iso_code;
      String _description;

      foreach (DataRow _idrow in Currencies.Rows)
      {
        _type = ((CurrencyExchangeType)_idrow["CE_TYPE"]);
        if (IsCageAmount)
        {
          _iso_code = (String)_idrow["CUR_ISO_CODE"];
          _description = (String)_idrow["CUR_NAME"];
          _idrow["CUR_NAME"] = GetDescription(_type, _iso_code, _description);
        }
        else
        {
          _iso_code = (String)_idrow["ISO_CODE"];
          _description = (String)_idrow["CE_DESCRIPTION"];
          _idrow["CE_DESCRIPTION"] = GetDescription(_type, _iso_code, _description);
        }
      }

    } //GetTableDescription

    // PURPOSE: Sets currency exchange description. If exists NLS use it, if not use the description provided by the DB
    //
    // PARAMS:
    //     - INPUT:
    //          - CurrencyExchangeType Type
    //          - String IsoCode
    //          - String Description
    //     - OUTPUT:
    //
    // RETURNS:
    //          - The Description to use
    //
    public static String GetDescription(CurrencyExchangeType Type, String IsoCode, String Description)
    {
      switch (Type)
      {
        case CurrencyExchangeType.CURRENCY:
          switch (IsoCode)
          {
            case "MXN":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_MXN");
            case "USD":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_USD");
            case "PEN":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_PEN");
            case "EUR":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_EUR");
            case "PAB":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_PAB");
            case "CRC":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_CRC");
            case "BSD":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_BSD");
            case "COP":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_COP");
            case "CLP":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_CLP");
            case "DOP":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_DOP");
            case "TTD":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_TTD");
            case "UYU":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_UYU");
            case "NIC":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_NIC");
            case "X01":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_X01");
            //EOR 24-AUG-2016
            case "NOK":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_NOK");
            case "GBP":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_GBP");
            case "SEK":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_SEK");
            case "CHF":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_CHF");
            case "JPY":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_JPY");
            case "DKK":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_DKK");
            case "CAD":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_CAD");
            case "PHP":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_PHP");
            case "ARS":
              return Resource.String("STR_CURRENCY_TYPE_0_ISO_ARS");

            default:
              return Description;
          }

        case CurrencyExchangeType.CARD:
          return Resource.String("STR_CURRENCY_TYPE_1");

        case CurrencyExchangeType.CHECK:
          return Resource.String("STR_CURRENCY_TYPE_2");

        case CurrencyExchangeType.CASINOCHIP:
          switch (IsoCode)
          {
            case "X01":
              return Resource.String("STR_CURRENCY_TYPE_3_ISO_X01");
            case "X02":
              return Resource.String("STR_CURRENCY_TYPE_3_ISO_X02");
            default:
              return Description;
          }

        case CurrencyExchangeType.CASINO_CHIP_NRE:
        case CurrencyExchangeType.CASINO_CHIP_RE:
        case CurrencyExchangeType.CASINO_CHIP_COLOR:
          // ATB 12-DEC-2016
          // Setting the () for redeemable and not redeemable chips
          String _chip_type_description;
          String _redeemable_string;
          String _not_redeemable_string;
          _redeemable_string = GeneralParam.GetString("FeatureChips", "ValueType.Name", Resource.String("STR_CURRENCY_TYPE_3_ISO_X01"));
          _not_redeemable_string = GeneralParam.GetString("FeatureChips", "NonRedeemableType.Name", Resource.String("STR_CURRENCY_TYPE_1002_NON_REDEEMABLE"));
          _chip_type_description = FeatureChips.GetChipTypeDescription(Type, IsoCode);
          if (_chip_type_description.Contains(_redeemable_string))
          {
            _chip_type_description = _chip_type_description.Replace(_redeemable_string, "(" + _redeemable_string + ")");
          }
          else if (_chip_type_description.Contains(_not_redeemable_string))
          {
            _chip_type_description = _chip_type_description.Replace(_not_redeemable_string, "(" + _not_redeemable_string + ")");
          }
          return _chip_type_description;
        case CurrencyExchangeType.CREDITLINE:
          return Resource.String("STR_CURRENCY_TYPE_3");
        default:
          return Description;
      }

    } //GetDescription

    public static String GetDescriptionType(CurrencyExchangeType Type)
    {
        String _description;

        _description = string.Empty;

        switch (Type)
        {
            case CurrencyExchangeType.CURRENCY:

                _description =  Resource.String("STR_FRM_AMOUNT_INPUT_CASH_PAYMENT");

                break;
            case CurrencyExchangeType.CARD:
                _description =  Resource.String("STR_CURRENCY_TYPE_1");

                break;
            case CurrencyExchangeType.CHECK:
                _description =  Resource.String("STR_CURRENCY_TYPE_2");

                break;
        }

        return _description;
    }

    public static String GetDescriptionSubType(CurrencyExchangeSubType SubType)
    {
        String _description;

        _description = string.Empty;

        switch (SubType)
        {
            case CurrencyExchangeSubType.NONE:

                _description = String.Empty;

                break;
            case CurrencyExchangeSubType.CHECK:

                _description = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_CHECK_PAYMENT");

                break;
            case CurrencyExchangeSubType.BANK_CARD:

                _description = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_BANK_CARD");

                break;
            case CurrencyExchangeSubType.CREDIT_CARD:

                _description = Resource.String("STR_CURRENCY_TYPE_1");

                break;
            case CurrencyExchangeSubType.DEBIT_CARD:

                _description = Resource.String("STR_FRM_AMOUNT_BANK_DEBIT_CARD");

                break;
            case CurrencyExchangeSubType.CARD2GO:
                break;
            case CurrencyExchangeSubType.BEARER_CHECK:

                _description = Resource.String("STR_BANK_TRANSACTION_CHECK_TYPE_BEARER_CHECK");

                break;
            case CurrencyExchangeSubType.NOMINAL_CHECK:

                _description = Resource.String("STR_BANK_TRANSACTION_CHECK_TYPE_NOMINAL_CHECK");

                break;
        }

        return _description;
    }

    // PURPOSE: Get all currency exchange
    //
    // PARAMS:
    //     - INPUT:
    //           - OnlyEnabled: True -> return only enabled; False: all
    //     - OUTPUT:
    //           - DtCurrencyExchange
    //
    // RETURNS:
    //     - True: if the process was successful
    //     - False: Otherwise
    //
    public static Boolean ReadCurrencyExchangeFilter(Boolean OnlyEnabled, out DataTable DtCurrencyExchange)
    {
      StringBuilder _sb;

      DtCurrencyExchange = new DataTable();
      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("    SELECT   CE_CURRENCY_ISO_CODE AS ISO_CODE                                ");
        _sb.AppendLine("           , '' AS DESCRIPTION                                               ");
        _sb.AppendLine("           , CE_TYPE                                                         ");
        _sb.AppendLine("           , ISNULL(CE_DESCRIPTION, '') AS CE_DESCRIPTION                    ");
        _sb.AppendLine("      FROM   CURRENCY_EXCHANGE AS A                                          ");
        _sb.AppendLine("     WHERE   NOT EXISTS                                                      ");
        //Get first type defined
        _sb.AppendLine("          (  SELECT   1                                                      ");
        _sb.AppendLine("               FROM   CURRENCY_EXCHANGE AS B                                 ");
        _sb.AppendLine("              WHERE   A.CE_CURRENCY_ISO_CODE = B.CE_CURRENCY_ISO_CODE        ");
        _sb.AppendLine("                AND   B.CE_TYPE < A.CE_TYPE  )                               ");

        if (OnlyEnabled)
        {
          _sb.AppendLine("     AND   CE_STATUS = 1                                                   ");
        }

        _sb.AppendLine("                ORDER BY CE_CURRENCY_ORDER, CE_CURRENCY_ISO_CODE             ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
          {
            using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
            {
              _db_trx.Fill(_da, DtCurrencyExchange);
              GetTableDescription(DtCurrencyExchange, false);
              foreach (DataRow _idx_row in DtCurrencyExchange.Rows)
              {
                _idx_row["DESCRIPTION"] = (String)_idx_row["ISO_CODE"] + " - " + (String)_idx_row["CE_DESCRIPTION"];
              }
            }
          }
        } // DB_TRX

        return true;
      }
      catch (SqlException _sql_ex)
      {
        Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // ReadCurrencyExchangeFilter

    /// <summary>
    /// Returns the number of currencies used in chips
    /// </summary>
    /// <param name="OnlyEnabled"></param>
    /// <returns></returns>
    public static Int32 GetNumberOfCurrenciesUsedInChips(Boolean OnlyEnabled)
    {
      DataTable _dt_currencies_in_chips;
      Int32 _number_of_currencies_in_chips;

      //Gets the datatable with the currencies used in chips
      ReadCurrenciesUsedInChips(OnlyEnabled, out _dt_currencies_in_chips);

      if (_dt_currencies_in_chips == null)
      {
        _number_of_currencies_in_chips = 0;
      }
      else
      {
        _number_of_currencies_in_chips = _dt_currencies_in_chips.Rows.Count;
      }

      return _number_of_currencies_in_chips;
    }

    /// <summary>
    /// Returns the currencies used in chips
    /// </summary>
    /// <param name="OnlyEnabled"></param>
    /// <param name="DtCurrenciesInChips"></param>
    /// <returns></returns>
    public static Boolean ReadCurrenciesUsedInChips(Boolean OnlyEnabled, out DataTable DtCurrenciesInChips)
    {
      StringBuilder _sb;

      DtCurrenciesInChips = new DataTable();

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("    SELECT   CE_CURRENCY_ISO_CODE     AS  ISO_CODE                                 ");
        _sb.AppendLine("           , CE_CURRENCY_ORDER        AS  ORDEN                                    ");
        _sb.AppendLine("      FROM   CURRENCY_EXCHANGE AS A                                                ");
        _sb.AppendLine("     WHERE   NOT EXISTS (                                                          ");
        //Get first type defined
        _sb.AppendLine("                          SELECT   1                                               ");
        _sb.AppendLine("                            FROM   CURRENCY_EXCHANGE AS B                          ");
        _sb.AppendLine("                           WHERE   A.CE_CURRENCY_ISO_CODE = B.CE_CURRENCY_ISO_CODE ");
        _sb.AppendLine("                             AND   B.CE_TYPE < A.CE_TYPE                           ");
        _sb.AppendLine("                         )                                                         ");
        //Used in chips
        _sb.AppendLine("       AND   A.CE_CURRENCY_ISO_CODE IN (                                           ");
        _sb.AppendLine("                                            SELECT DISTINCT CHS_ISO_CODE           ");
        _sb.AppendLine("                                            FROM CHIPS_SETS A                      ");
        _sb.AppendLine("                                       )                                           ");

        if (OnlyEnabled)
        {
          _sb.AppendLine("     AND   CE_STATUS = 1                                                         ");
        }

        _sb.AppendLine("  ORDER BY   CE_CURRENCY_ORDER, CE_CURRENCY_ISO_CODE                               ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
          {
            using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
            {
              _db_trx.Fill(_da, DtCurrenciesInChips);
            }
          }
        } // DB_TRX

        return true;
      }
      catch (SqlException _sql_ex)
      {
        Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //ReadCurrenciesUsedInChips

    // PURPOSE: Apply exchange
    //
    // PARAMS:
    //     - INPUT:
    //           - InAmount
    //     - OUTPUT:
    //           - Exchanged: operation result
    //
    // RETURNS:
    //     - True: if the process was successful
    //     - False: Otherwise
    //
    public Boolean ApplyExchange(Decimal InAmount, out CurrencyExchangeResult Exchanged)
    {
      return ApplyExchange(InAmount, out Exchanged, false);
    }

    public Boolean SimpleApplyExchange(Decimal InAmount, out Decimal GrossAmount)
    {
      Decimal _change_rate;

      if (m_change_rate.HasValue)
      {
        _change_rate = m_change_rate.Value;
      }
      else
      {
        _change_rate = 1;
      }

      if (m_decimals.HasValue)
      {
        GrossAmount = TruncateWithDecimals(InAmount * _change_rate, m_decimals.Value);

        return true;
      }

      GrossAmount = InAmount;

      Log.Message("Error on SimpleApplyExchange: Currency Exchange not defined");

      return false;
    }

    // PURPOSE: Apply exchange
    //
    // PARAMS:
    //     - INPUT:
    //           - InAmount
    //           - CashAvance
    //     - OUTPUT:
    //           - Exchanged: operation result
    //
    // RETURNS:
    //     - True: if the process was successful
    //     - False: Otherwise
    //
    public Boolean ApplyExchange(Decimal InAmount, out CurrencyExchangeResult Exchanged, Boolean CashAdvance)
    {
      return ApplyExchange(InAmount, out Exchanged, CashAdvance, 0, true);
    }

    // PURPOSE: Apply inverse exchange
    //
    // PARAMS:
    //     - INPUT:
    //           - InAmount
    //           - CashAvance
    //     - OUTPUT:
    //           - Exchanged: operation result
    //
    // RETURNS:
    //     - True: if the process was successful
    //     - False: Otherwise
    //
    public Boolean ApplyInverseExchange(Decimal InAmount, out CurrencyExchangeResult Exchanged, Boolean CashAdvance, Currency CardPrice, bool ApplyComission)
    {
      String _national_currency;
      TYPE_SPLITS _splits;
      Decimal _net_amount;

      Exchanged = new CurrencyExchangeResult();

      try
      {
        Exchanged.InAmount = InAmount;
        Exchanged.InType = m_type;

        _national_currency = GetNationalCurrency();
        Exchanged.InCurrencyCode = _national_currency;
        Exchanged.WasForeingCurrency = !(m_currency_code != null && m_currency_code.Equals(_national_currency));

        // DHA 30-JUN-2015: set card price
        if (Misc.IsVoucherModeTV()) //EOR 08-DEC-2017
        {
          Exchanged.Comission = CardPrice;
        }
        else
        {
          Exchanged.CardPrice = CardPrice;
        }

        if (m_change_rate.HasValue)
        {
          Exchanged.ChangeRate = m_change_rate.Value;
        }
        else
        {
          Exchanged.ChangeRate = 1;
        }

        if (m_decimals.HasValue)
        {
          Exchanged.Decimals = m_decimals.Value;
        }

        Exchanged.GrossAmount = TruncateWithDecimals(InAmount / Exchanged.ChangeRate, Exchanged.Decimals);

        if (ApplyComission)
        {
          if (m_variable_comission.HasValue && m_fixed_comission.HasValue)
          {
            Exchanged.Comission = TruncateWithDecimals((Exchanged.GrossAmount * m_variable_comission.Value) / 100 + m_fixed_comission.Value, 2);
          }

          // DHA 29-JUN-2015: calculate card comissions
          if (m_variable_comission.HasValue)
          {
            Exchanged.CardComissions = TruncateWithDecimals((Exchanged.CardPrice * m_variable_comission.Value) / 100, 2);
          }
        }

        if ((Exchanged.InType == CurrencyExchangeType.CARD || CashAdvance) && Exchanged.InCurrencyCode == _national_currency)
        {
          _net_amount = Exchanged.GrossAmount;
        }
        else
        {
          _net_amount = Exchanged.GrossAmount - Exchanged.Comission;
        }

        // DHA 30-JUN-2015: check and foreing currency payment must to include the price of the card
        if (Exchanged.InType == CurrencyExchangeType.CHECK || Exchanged.InType == CurrencyExchangeType.CURRENCY && Exchanged.WasForeingCurrency)
        {
          _net_amount -= Exchanged.CardPrice;
        }

        // Check that there is still amount after the exchange
        if (_net_amount <= 0)
        {
          return false;
        }

        // Check for enough balance to subtract and make maths
        if (!Split.ReadSplitParameters(out _splits))
        {
          return false;
        }

        Exchanged.NetAmountSplit1 = Math.Round(_net_amount * _splits.company_a.cashin_pct / 100, 2, MidpointRounding.AwayFromZero);
        Exchanged.NetAmountSplit2 = _net_amount - Exchanged.NetAmountSplit1;

        if (m_variable_NR2.HasValue && m_fixed_NR2.HasValue)
        {
          Exchanged.NR2Amount = TruncateWithDecimals((Exchanged.GrossAmount * m_variable_NR2.Value) / 100 + m_fixed_NR2.Value, 2);
        }

        Exchanged.ArePromotionsEnabled = false;
        if ((m_variable_NR2.HasValue && m_variable_NR2 != 0)
          || (m_fixed_NR2.HasValue && m_fixed_NR2 != 0))
        {
          Exchanged.ArePromotionsEnabled = true;
        }

        if ((Exchanged.InType == CurrencyExchangeType.CARD || CashAdvance) && Exchanged.InCurrencyCode == _national_currency)
        {
          // DHA 29-JUN-2015
          Exchanged.Comission += Exchanged.CardComissions;
          Exchanged.InAmount += Exchanged.Comission;
          Exchanged.InAmount += Exchanged.CardPrice;
          Exchanged.GrossAmount += Exchanged.Comission;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // ApplyInverseExchange



    // PURPOSE: Apply exchange
    //
    // PARAMS:
    //     - INPUT:
    //           - InAmount
    //           - CashAvance
    //     - OUTPUT:
    //           - Exchanged: operation result
    //
    // RETURNS:
    //     - True: if the process was successful
    //     - False: Otherwise
    //
    public Boolean ApplyExchange(Decimal InAmount, out CurrencyExchangeResult Exchanged, Boolean CashAdvance, Currency CardPrice, bool ApplyComission)
    {
      String _national_currency;
      TYPE_SPLITS _splits;
      Decimal _net_amount;

      Exchanged = new CurrencyExchangeResult();

      try
      {
        Exchanged.InAmount = InAmount;
        Exchanged.InCurrencyCode = m_currency_code;

        Exchanged.InType = m_type;

        _national_currency = GetNationalCurrency();
        Exchanged.WasForeingCurrency = !(m_currency_code != null && m_currency_code.Equals(_national_currency));

        // DHA 30-JUN-2015: set card price
        Exchanged.CardPrice = CardPrice;

        if (m_change_rate.HasValue)
        {
          Exchanged.ChangeRate = m_change_rate.Value;
        }
        else
        {
          Exchanged.ChangeRate = 1;
        }

        if (m_decimals.HasValue)
        {
          Exchanged.Decimals = m_decimals.Value;
        }

        Exchanged.GrossAmount = TruncateWithDecimals(InAmount * Exchanged.ChangeRate, Exchanged.Decimals);

        if (ApplyComission)
        {

          if (m_variable_comission.HasValue && m_fixed_comission.HasValue)
          {
            Exchanged.Comission = TruncateWithDecimals((Exchanged.GrossAmount * m_variable_comission.Value) / 100 + m_fixed_comission.Value, 2);
          }

          // DHA 29-JUN-2015: calculate card comissions
          if (m_variable_comission.HasValue)
          {
            Exchanged.CardComissions = TruncateWithDecimals((Exchanged.CardPrice * m_variable_comission.Value) / 100, 2);
          }
        }

        if ((Exchanged.InType == CurrencyExchangeType.CARD || CashAdvance) && Exchanged.InCurrencyCode == _national_currency)
        {
          _net_amount = Exchanged.GrossAmount;
        }
        else
        {
          _net_amount = Exchanged.GrossAmount - Exchanged.Comission;
        }

        // DHA 30-JUN-2015: check and foreing currency payment must to include the price of the card
        if (Exchanged.InType == CurrencyExchangeType.CHECK || Exchanged.InType == CurrencyExchangeType.CURRENCY && Exchanged.WasForeingCurrency)
        {
          _net_amount -= Exchanged.CardPrice;
        }

        // Check that there is still amount after the exchange
        if (_net_amount <= 0)
        {
          return false;
        }

        // Check for enough balance to subtract and make maths
        if (!Split.ReadSplitParameters(out _splits))
        {
          return false;
        }

        Exchanged.NetAmountSplit1 = Math.Round(_net_amount * _splits.company_a.cashin_pct / 100, 2, MidpointRounding.AwayFromZero);
        Exchanged.NetAmountSplit2 = _net_amount - Exchanged.NetAmountSplit1;

        if (m_variable_NR2.HasValue && m_fixed_NR2.HasValue)
        {
          Exchanged.NR2Amount = TruncateWithDecimals((Exchanged.GrossAmount * m_variable_NR2.Value) / 100 + m_fixed_NR2.Value, 2);
        }

        Exchanged.ArePromotionsEnabled = false;
        if ((m_variable_NR2.HasValue && m_variable_NR2 != 0)
          || (m_fixed_NR2.HasValue && m_fixed_NR2 != 0))
        {
          Exchanged.ArePromotionsEnabled = true;
        }

        if ((Exchanged.InType == CurrencyExchangeType.CARD || CashAdvance) && Exchanged.InCurrencyCode == _national_currency)
        {
          // DHA 29-JUN-2015
          Exchanged.Comission += Exchanged.CardComissions;
          Exchanged.InAmount += Exchanged.Comission;
          Exchanged.InAmount += Exchanged.CardPrice;
          Exchanged.GrossAmount += Exchanged.Comission;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // ApplyExchange

    // PURPOSE: Inverse Apply exchange for cash advance
    //
    // PARAMS:
    //     - INPUT:
    //           - InAmount
    //     - OUTPUT:
    //           - OutAmount
    //
    // RETURNS:
    //     - True: if the process was successful
    //     - False: Otherwise
    //
    public Boolean InverseApplyExchangeForCashAdvance(Decimal GrossAmount, out CurrencyExchangeResult Exchanged)
    {
      String _national_currency;
      Decimal _net_amount;

      Exchanged = new CurrencyExchangeResult();

      try
      {
        Exchanged.InAmount = GrossAmount;
        Exchanged.GrossAmount = GrossAmount;
        Exchanged.InCurrencyCode = m_currency_code;

        Exchanged.InType = m_type;

        _national_currency = GetNationalCurrency();
        Exchanged.WasForeingCurrency = !(m_currency_code != null && m_currency_code.Equals(_national_currency));

        if (m_change_rate.HasValue)
        {
          Exchanged.ChangeRate = m_change_rate.Value;
        }
        else
        {
          Exchanged.ChangeRate = 1;
        }

        if (m_decimals.HasValue)
        {
          Exchanged.Decimals = m_decimals.Value;
        }

        if (m_variable_comission.HasValue && m_fixed_comission.HasValue)
        {
          // 05-FEB-2015 JPJ GrossAmount formula changed
          _net_amount = (Exchanged.GrossAmount - m_fixed_comission.Value) * 100 / (100 + m_variable_comission.Value);
          _net_amount = TruncateWithDecimals(_net_amount, Exchanged.Decimals);
          Exchanged.Comission = Exchanged.GrossAmount - _net_amount;
        }
        else
        {
          _net_amount = Exchanged.GrossAmount;
        }

        // CashAdvance has no B part, so everithing goes to split1.
        Exchanged.NetAmountSplit1 = _net_amount;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // InverseApplyExchangeForCashAdvance

    // PURPOSE: Format change rate
    //
    // PARAMS:
    //     - INPUT:
    //           - Value
    //
    // RETURNS:
    //     - Formatted number
    //
    public static String ChangeRateFormat(Decimal Value)
    {
      return Value.ToString("#,#0.00######");
    }

    // PURPOSE: Get historic change rate
    //
    // PARAMS:
    //     - INPUT:
    //           - IsoCode
    //           - Type
    //           - Date
    //
    //     - OUTPUT:
    //           - ChangeRate
    //
    // RETURNS:
    //     - True if change rate has been found.
    //
    public static Boolean GetHistoricChangeRate(String IsoCode, CurrencyExchangeType Type, DateTime Date,
                                                out Decimal ChangeRate, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;

      ChangeRate = 1;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   TOP 1 CEA_NEW_CHANGE                ");
        _sb.AppendLine("    FROM   CURRENCY_EXCHANGE_AUDIT             ");
        _sb.AppendLine("   WHERE   CEA_DATETIME          <= @pDateTime ");
        _sb.AppendLine("     AND   CEA_CURRENCY_ISO_CODE  = @pIsoCode  ");
        _sb.AppendLine("     AND   CEA_TYPE               = @pType     ");
        _sb.AppendLine("ORDER BY   CEA_DATETIME DESC                   ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pDateTime", SqlDbType.DateTime).Value = Date;
          _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = IsoCode;
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = Type;

          _obj = _cmd.ExecuteScalar();
          if (_obj != null && _obj != DBNull.Value)
          {
            ChangeRate = (Decimal)_obj;

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    // PURPOSE: Get Allowed Chips
    //
    // PARAMS:
    //     - INPUT:
    //        - Trx
    //     - OUTPUT:
    //        - Chips
    //
    // RETURNS:
    //     - Boolean
    //
    public static Boolean GetAllowedChips(out DataTable Chips, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Chips = new DataTable();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT    CH_CHIP_ID   ");
        _sb.AppendLine("         , CH_DENOMINATION   ");
        _sb.AppendLine("         , CH_ISO_CODE   ");
        _sb.AppendLine("         , CH_CHIP_TYPE   ");
        _sb.AppendLine("         , CH_COLOR   ");
        _sb.AppendLine("         , CH_NAME   ");
        _sb.AppendLine("         , CH_DRAWING   ");
        _sb.AppendLine("   FROM    CHIPS ");
        _sb.AppendLine("  WHERE    CH_ALLOWED = @pAllowed ");
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAllowed", SqlDbType.Bit).Value = true;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(Chips);

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    // PURPOSE: Get exchange in Currency
    //
    // PARAMS:
    //     - INPUT:
    //           - Amount
    //           - FromISO
    //           - ToISO
    //     - OUTPUT:
    //
    // RETURNS:
    //     - Exchanged: operation result value
    //
    public static Currency GetExchange(Currency Amount, String FromISO, String ToISO, SqlTransaction SqlTrx)
    {
      Currency _amount_to_exchange;
      Decimal _gross_amount;
      CurrencyExchange _target_currency;
      CurrencyExchange _source_currency;

      _amount_to_exchange = Amount;
      if (_amount_to_exchange == 0 || ToISO.Equals(FromISO))
      {
        return _amount_to_exchange;
      }

      _target_currency = new CurrencyExchange();
      _source_currency = new CurrencyExchange();

      // Read target exchange config
      CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CURRENCY, ToISO, out _target_currency, SqlTrx);

      if (FromISO != CurrencyExchange.GetNationalCurrency())
      {
        // Read source exchange config
        CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CURRENCY, FromISO, out _source_currency, SqlTrx);

        // Set exchange value due to mantein config from iso. In ce_change field (BD) value is the conversion to N.C.
        _target_currency.ChangeRate = _source_currency.ChangeRate.Value;

      }
      else
      {
        // Set exchange value, in this case do the inverse
        _target_currency.ChangeRate = 1 / _target_currency.ChangeRate;
      }

      _target_currency.SimpleApplyExchange((Decimal)_amount_to_exchange, out _gross_amount);

      return (Currency)_gross_amount;
    }  // GetExchange

    // PURPOSE: Get exchange in MultiPromos.AccountBalance
    //
    // PARAMS:
    //     - INPUT:
    //           - Amount
    //           - FromISO
    //           - ToISO
    //     - OUTPUT:
    //
    // RETURNS:
    //     - Exchanged: operation result value
    //
    public static MultiPromos.AccountBalance GetExchange(MultiPromos.AccountBalance Amount, String FromISO, String ToISO, SqlTransaction SqlTrx)
    {
      MultiPromos.AccountBalance _amount_to_exchange;
      MultiPromos.AccountBalance _gross_amount;
      CurrencyExchange _target_currency;
      CurrencyExchange _source_currency;

      if (Amount.TotalBalance == 0 || ToISO.Equals(FromISO))
      {
        return Amount;
      }

      _target_currency = new CurrencyExchange();
      _source_currency = new CurrencyExchange();
      _amount_to_exchange = new MultiPromos.AccountBalance(Amount);

      // Read target exchange config
      CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CURRENCY, ToISO, out _target_currency, SqlTrx);

      if (FromISO != CurrencyExchange.GetNationalCurrency())
      {
        // Read source exchange config
        CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CURRENCY, FromISO, out _source_currency, SqlTrx);

        // Set exchange value due to mantein config from iso. In ce_change field (BD) value is the conversion to N.C.
        _target_currency.ChangeRate = _source_currency.ChangeRate.Value;

      }
      else
      {
        // Set exchange value, in this case do the inverse
        _target_currency.ChangeRate = 1 / _target_currency.ChangeRate;
      }

      _gross_amount = new MultiPromos.AccountBalance();
      _target_currency.SimpleApplyExchange((Decimal)_amount_to_exchange.Redeemable, out _gross_amount.Redeemable);
      _target_currency.SimpleApplyExchange((Decimal)_amount_to_exchange.PromoRedeemable, out _gross_amount.PromoRedeemable);
      _target_currency.SimpleApplyExchange((Decimal)_amount_to_exchange.PromoNotRedeemable, out _gross_amount.PromoNotRedeemable);

      return _gross_amount;
    }

    // PURPOSE: Get IsoCode From DB to TerminalId
    //
    // PARAMS:
    //     - INPUT:
    //          - TerminalId
    //     - OUTPUT:
    //          - IsoCode
    //
    // RETURNS:
    //     - Boolean according to exit function
    //
    public static Boolean GetIsoCodeFromTerminalID(Int32 TerminalId, out String IsoCode)
    {
      StringBuilder _sb;

      IsoCode = "";

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine("  SELECT   TE_ISO_CODE                   ");
          _sb.AppendLine("    FROM   TERMINALS                     ");
          _sb.AppendLine("   WHERE   TE_TERMINAL_ID = @pTerminalID ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = TerminalId;

            using (SqlDataReader _reader = _db_trx.ExecuteReader(_cmd))
            {
              while (_reader.Read())
              {
                IsoCode = _reader.GetString(0);
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }

      return true;
    } // GetIsoCodeFromTerminalID

    // PURPOSE: Add currency symbol or iso code to imput amount
    //
    // PARAMS:
    //     - INPUT:
    //          - Amount
    //          - Iso_code_terminal
    //     - OUTPUT:
    //          - Amount_currency
    //
    // RETURNS:
    //     - Boolean to according to exit function
    //
    public static Boolean AddCurrencyToAmount(Decimal Amount, String IsoCodeTerminal, out String AmountCurrency)
    {
      AmountCurrency = "";
      try
      {
        //AmountCurrency = IsoCodeTerminal == CurrencyExchange.GetNationalCurrency().ToString() ? ((Currency)Amount).ToString() : Currency.Format(Amount, IsoCodeTerminal);
        AmountCurrency = Currency.Format(Amount, IsoCodeTerminal);
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
    } // ConvertAmounCurrencyTerminal

    /// <summary>
    /// Save currencyConfigurationToXml 
    /// </summary>
    /// <param name="Currency_Exchange_List"></param>
    /// <returns></returns>
    public static String CurrencyConfigurationToXML(List<CurrencyExchange> Currency_Exchange_List)
    {
      DataSet _currency_exchange;
      DataTable _currency_table;
      DataRow _newCurrencyRow;

      _currency_exchange = GetCurrencyConfigurationDataSet();
      _currency_table = _currency_exchange.Tables[CONFIGURATION_TABLE_NAME];

      foreach (CurrencyExchange _cur in Currency_Exchange_List)
      {
        _newCurrencyRow = _currency_table.NewRow();

        _newCurrencyRow["type"] = (Int32)_cur.BankCard.card_type;
        _newCurrencyRow["scope"] = (Int32)_cur.BankCard.card_scope;
        _newCurrencyRow["fixed_commission"] = _cur.FixedComission;
        _newCurrencyRow["variable_commission"] = _cur.VariableComission;
        _newCurrencyRow["fixed_nr2"] = _cur.FixedNR2;
        _newCurrencyRow["variable_nr2"] = _cur.VariableNR2;

        _currency_table.Rows.Add(_newCurrencyRow);

      }

      return Misc.DataSetToXml(_currency_exchange, XmlWriteMode.IgnoreSchema);

    }

    #endregion

  } // CurrencyExchange

  //------------------------------------------------------------------------------
  // CLASS : CurrencyExchangeResult
  // 
  // Moved to SharedDefinitions.cs because it's needed by SharedCashierMovements.


  //------------------------------------------------------------------------------
  // CLASS : CurrencyExchangeProperties
  // 
  // NOTES :

  public class CurrencyExchangeProperties
  {
    #region Enums

    private enum PatternType
    {
      Left = 0,
      Right = 1,
      LeftSpace = 2,
      SpaceRight = 3
    }

    private enum FlagImage
    {
      UNKNOWN = -1,
      MXN = 0,
      USD = 1,
      EUR = 2,
      PEN = 3,
      X01 = 4,
      PAB = 5,
      CRC = 6,
      BSD = 7,
      COP = 8,
      CLP = 9,
      DOP = 10,
      NOK = 11,
      UYU = 12,
      TTD = 13,
      GBP = 14,
      SEK = 15,
      CHF = 16,
      JPY = 17,
      DKK = 18,
      CAD = 19,
      PHP = 20,
      ARS = 21,
      NIO = 22,
    }

    #endregion

    #region Members

    private String m_iso_code;
    private String m_symbol;
    private PatternType m_pattern;
    private FlagImage m_flag;

    private static Dictionary<String, CurrencyExchangeProperties> m_properties = Create();
    private const String m_default_currency_iso_code = "UNKNOWN";
    #endregion

    #region Properties

    public String Symbol
    {
      get { return m_symbol; }
    }

    #endregion

    #region Constructors

    private CurrencyExchangeProperties(String ISOCode, String Symbol, PatternType Pattern, FlagImage Flag)
    {
      m_iso_code = ISOCode;
      m_symbol = Symbol;
      m_pattern = Pattern;
      m_flag = Flag;
    }

    #endregion

    #region Public Methods

    public static String GetCurrencySymbol(String CurrencyValue)
    {
      String _currency_symbol;

      _currency_symbol = String.Empty;

      foreach (KeyValuePair<String, CurrencyExchangeProperties> _property in m_properties)
      {
        CurrencyExchangeProperties _currency_exchange_aux;

        _currency_exchange_aux = _property.Value;

        if (CurrencyValue.Contains(_currency_exchange_aux.Symbol))
        {
          _currency_symbol = _currency_exchange_aux.Symbol;
          break;
        }
      }

      return (_currency_symbol);
    } // GetCurrencySymbol

    public static CurrencyExchangeProperties GetProperties(String CurrencyIsoCode)
    {
      CurrencyExchangeProperties _prop;

      _prop = null;

      if (m_properties.ContainsKey(CurrencyIsoCode))
      {
        _prop = m_properties[CurrencyIsoCode];
      }
      else
      {
        _prop = m_properties[m_default_currency_iso_code];
      }
      return _prop;
    }

    // PURPOSE: Return the national currency symbol
    //
    // PARAMS:
    //     - INPUT:
    //
    // RETURNS:
    //     - String with national currency symbol
    //
    public static String GetNationalCurrencySymbol()
    {
      String _symbol;
      String _iso_code;
      CurrencyExchangeProperties _prop;

      _symbol = "$";
      _iso_code = CurrencyExchange.GetNationalCurrency();

      if (m_properties.ContainsKey(_iso_code))
      {
        _prop = m_properties[_iso_code];
        _symbol = _prop.Symbol;
      }

      return _symbol;
    } // GetNationalCurrencySymbol

    // PURPOSE: Return the national currency pattern type
    //
    // PARAMS:
    //     - INPUT:
    //
    // RETURNS:
    //     - String with national pattern type
    //
    public static Int32 GetNationalCurrencyPattern()
    {
      Int32 _pattern;
      String _iso_code;
      CurrencyExchangeProperties _prop;

      _pattern = 0;
      _iso_code = CurrencyExchange.GetNationalCurrency();

      if (m_properties.ContainsKey(_iso_code))
      {
        _prop = m_properties[_iso_code];
        _pattern = (Int32)_prop.m_pattern;
      }

      return _pattern;
    } // GetNationalCurrencyPattern

    // PURPOSE: Format number with specific currency property
    //
    // PARAMS:
    //     - INPUT:
    //           - Number
    //
    // RETURNS:
    //     - String with formated number
    //
    public Decimal ParseCurrency(String Number)
    {
      Decimal _value;
      String _number;
      Boolean _is_numeric;
      Int32 _symbol_len;

      _symbol_len = m_symbol.Length;
      _number = Number;

      if (_symbol_len > 0)
      {
        if (m_pattern == PatternType.LeftSpace || m_pattern == PatternType.SpaceRight)
        {
          _symbol_len += 1;
        }

        switch (m_pattern)
        {
          case PatternType.Left:       //$n 
          case PatternType.LeftSpace:  //$ n
          default:
            _number = Number.Remove(0, _symbol_len);
            break;
          case PatternType.Right:      //n$
          case PatternType.SpaceRight: //n $
            _number = Number.Remove(Number.Length - _symbol_len, _symbol_len);
            break;
        }
      }

      _is_numeric = Decimal.TryParse(_number, out _value);

      if (!_is_numeric)
        _value = 0;

      return _value;

    }

    // PURPOSE: Format number with specific currency property
    //
    // PARAMS:
    //     - INPUT:
    //           - Number
    //
    // RETURNS:
    //     - String with formated number
    //
    public String FormatCurrency(Decimal Number)
    {
      Currency _value;

      _value = ((Currency)Number);
      _value.CurrencyIsoCode = m_iso_code;

      switch (m_pattern)
      {
        case PatternType.Left:       //$n 
        default:
          return m_symbol + _value.ToString(false);
        case PatternType.Right:      //n$
          return _value.ToString(false) + m_symbol;
        case PatternType.LeftSpace:  //$ n
          return m_symbol + " " + _value.ToString(false);
        case PatternType.SpaceRight: //n $
          return _value.ToString(false) + " " + m_symbol;
      }
    }

    // PURPOSE: Get currency flag in 32x32 size
    //
    // PARAMS:
    //     - INPUT:
    //
    // RETURNS:
    //     - Bitmap of the flag
    //
    public Bitmap GetFlag32x32()
    {
      return GetFlag(32);
    }

    // PURPOSE: Get currency flag in YxY size
    //
    // PARAMS:
    //     - INPUT:
    //            - Size
    //
    // RETURNS:
    //     - Bitmap of the flag
    //
    public Bitmap GetFlag(Int32 Size)
    {
      return GetFlag(Size, Size);
    }

    // PURPOSE: Get currency flag in YxZ size
    //
    // PARAMS:
    //     - INPUT:
    //            - Width
    //            - Height
    //
    // RETURNS:
    //     - Bitmap of the flag
    //
    public Bitmap GetFlag(Int32 Width, Int32 Height)
    {
      Bitmap b;

      b = (Bitmap)GetFlag();

      if (b != null)
      {
        Decimal ratioX = Math.Max(b.Height, b.Width);

        return (Bitmap)b.GetThumbnailImage((Int32)(Width * b.Width / ratioX), (Int32)(Height * b.Height / ratioX), null, new IntPtr(0));
      }

      return null;
    }

    // PURPOSE: Get currency flag
    //
    // PARAMS:
    //     - INPUT:
    //
    // RETURNS:
    //     - Bitmap of the flag
    //
    public Bitmap GetFlag()
    {
      return GetFlag(false);
    }
    public Bitmap GetFlag(Boolean WhiteColor)
    {
      try
      {
        switch (m_flag)
        {
          case FlagImage.MXN:
            return Resource.Image("flag_MXN");

          case FlagImage.USD:
            return Resource.Image("flag_USD");

          case FlagImage.EUR:
            return Resource.Image("flag_EUR");

          case FlagImage.PEN:
            return Resource.Image("flag_PEN");

          case FlagImage.PAB:
            return Resource.Image("flag_PAB");

          case FlagImage.CRC:
            return Resource.Image("flag_CRC");

          case FlagImage.BSD:
            return Resource.Image("flag_BSD");

          case FlagImage.COP:
            return Resource.Image("flag_COP");

          case FlagImage.CLP:
            return Resource.Image("flag_CLP");

          case FlagImage.DOP:
            return Resource.Image("flag_DOP");

          case FlagImage.NOK:
            return Resource.Image("flag_NOK");

          case FlagImage.UYU:
            return Resource.Image("flag_UYU");

          case FlagImage.TTD:
            return Resource.Image("flag_TTD");

          case FlagImage.GBP:
            return Resource.Image("flag_GBP");

          case FlagImage.SEK:
            return Resource.Image("flag_SEK");

          case FlagImage.CHF:
            return Resource.Image("flag_CHF");

          case FlagImage.JPY:
            return Resource.Image("flag_JPY");

          case FlagImage.DKK:
            return Resource.Image("flag_DKK");

          case FlagImage.CAD:
            return Resource.Image("flag_CAD");

          case FlagImage.ARS:
            return Resource.Image("flag_ARS");

          case FlagImage.NIO:
            return Resource.Image("flag_NIC");

          //EOR 24-AUG-2016
          case FlagImage.PHP:
            return Resource.Image("flag_PHP");

          case FlagImage.X01:
            if (WhiteColor)
            {
              return Resource.Image("flag_X01_white");
            }
            return Resource.Image("flag_X01_RE");

          default:
            if (WhiteColor)
            {
              return Resource.Image("money_white");
            }
            return Resource.Image("money");
        }
      }
      catch
      {
        return null;
      }
    }

    // PURPOSE: Get Icon flag
    //
    // PARAMS:
    //     - INPUT:
    //
    // RETURNS:
    //     - Bitmap of the flag
    //
    public Bitmap GetIcon(CurrencyExchangeType Type, Boolean WhiteColor)
    {
      String _image_name;

      try
      {
        switch (Type)
        {
          case CurrencyExchangeType.CURRENCY:
            return GetFlag(WhiteColor);

          // RMS 08-AUG-2014
          case CurrencyExchangeType.CARD:
            if (WhiteColor)
            {
              _image_name = "card_white";
            }
            else
            {
              _image_name = "card_black";
            }
            return Resource.Image(_image_name);


          case CurrencyExchangeType.CHECK:
            if (WhiteColor)
            {
              _image_name = "check_white";
            }
            else
            {
              _image_name = "check_black";
            }
            return Resource.Image(_image_name);

          case CurrencyExchangeType.FREE:
            return Resource.Image("no_money");

          case CurrencyExchangeType.CASINOCHIP:
          case CurrencyExchangeType.CASINO_CHIP_RE:
            if (WhiteColor)
            {
              return Resource.Image("flag_X01_white");
            }
            return Resource.Image("flag_X01");

          case CurrencyExchangeType.CASINO_CHIP_NRE:
            return Resource.Image("Chips_NR");

          case CurrencyExchangeType.CASINO_CHIP_COLOR:
            return Resource.Image("Chips_Color");

          case CurrencyExchangeType.CREDITLINE:
            if (WhiteColor)
            {
              _image_name = "credit_line_white";
            }
            else
            {
              _image_name = "credit_line_black";
            }

            return Resource.Image(_image_name);

          default:
            return System.Drawing.SystemIcons.Application.ToBitmap();
        }
      }
      catch
      {
        return null;
      }
    }

    // PURPOSE: Get currency Icon in YxZ size
    //
    // PARAMS:
    //     - INPUT:
    //            - Width
    //            - Height
    //
    // RETURNS:
    //     - Bitmap of the icon
    //
    public Bitmap GetIcon(CurrencyExchangeType Type, Int32 Width, Int32 Height)
    {
      return GetIcon(Type, Width, Height, false);
    }

    public Bitmap GetIcon(CurrencyExchangeType Type, Int32 Width, Int32 Height, Boolean IsSelected)
    {
      Bitmap b;

      b = (Bitmap)GetIcon(Type, IsSelected);

      if (b != null)
      {
        Decimal ratioX = Math.Max(b.Height, b.Width);

        return (Bitmap)b.GetThumbnailImage((Int32)(Width * b.Width / ratioX), (Int32)(Height * b.Height / ratioX), null, new IntPtr(0));
      }

      return null;
    }

    #endregion

    #region Private Methods

    private static Dictionary<String, CurrencyExchangeProperties> Create()
    {
      m_properties = new Dictionary<String, CurrencyExchangeProperties>();

      m_properties.Add("MXN", new CurrencyExchangeProperties("MXN", "$", PatternType.Left, FlagImage.MXN));
      m_properties.Add("USD", new CurrencyExchangeProperties("USD", "$", PatternType.Left, FlagImage.USD));
      m_properties.Add("EUR", new CurrencyExchangeProperties("EUR", "€", PatternType.SpaceRight, FlagImage.EUR));
      m_properties.Add("PEN", new CurrencyExchangeProperties("PEN", "S/.", PatternType.LeftSpace, FlagImage.PEN));
      m_properties.Add("PAB", new CurrencyExchangeProperties("PAB", "B/.", PatternType.LeftSpace, FlagImage.PAB));
      m_properties.Add("CRC", new CurrencyExchangeProperties("CRC", "¢", PatternType.Left, FlagImage.CRC));
      m_properties.Add("BSD", new CurrencyExchangeProperties("BSD", "B$", PatternType.Left, FlagImage.BSD));
      m_properties.Add("COP", new CurrencyExchangeProperties("COP", "$", PatternType.Left, FlagImage.COP));
      m_properties.Add("CLP", new CurrencyExchangeProperties("CLP", "$", PatternType.LeftSpace, FlagImage.CLP));
      m_properties.Add("DOP", new CurrencyExchangeProperties("DOP", "RD$", PatternType.Left, FlagImage.DOP));
      m_properties.Add("NOK", new CurrencyExchangeProperties("NOK", "kr", PatternType.LeftSpace, FlagImage.NOK));
      m_properties.Add("UYU", new CurrencyExchangeProperties("UYU", "$U", PatternType.LeftSpace, FlagImage.UYU));
      m_properties.Add("TTD", new CurrencyExchangeProperties("TTD", "$", PatternType.Left, FlagImage.TTD));
      m_properties.Add("PHP", new CurrencyExchangeProperties("PHP", "PhP", PatternType.Left, FlagImage.PHP));
      m_properties.Add("GBP", new CurrencyExchangeProperties("GBP", "£", PatternType.Left, FlagImage.GBP));
      m_properties.Add("SEK", new CurrencyExchangeProperties("SEK", "kr", PatternType.LeftSpace, FlagImage.SEK));
      m_properties.Add("CHF", new CurrencyExchangeProperties("CHF", "Fr", PatternType.LeftSpace, FlagImage.CHF));
      m_properties.Add("JPY", new CurrencyExchangeProperties("JPY", "¥", PatternType.Left, FlagImage.JPY));
      m_properties.Add("DKK", new CurrencyExchangeProperties("DKK", "kr", PatternType.LeftSpace, FlagImage.DKK));
      m_properties.Add("CAD", new CurrencyExchangeProperties("CAD", "C$", PatternType.Left, FlagImage.CAD));
      m_properties.Add("ARS", new CurrencyExchangeProperties("ARS", "$", PatternType.Left, FlagImage.ARS));
      m_properties.Add("NIO", new CurrencyExchangeProperties("NIO", "C$", PatternType.Left, FlagImage.NIO));

      // Currencies that use the national currency properties need to be at the end of the property dictionary
      // to ensure that the national currency has been declared.
      m_properties.Add("X01", new CurrencyExchangeProperties("X01", CurrencyExchangeProperties.GetNationalCurrencySymbol()
                                                                  , (PatternType)CurrencyExchangeProperties.GetNationalCurrencyPattern()
                                                                  , FlagImage.X01));

      m_properties.Add("UNKNOWN", new CurrencyExchangeProperties("---", "", PatternType.Left, FlagImage.UNKNOWN));

      return m_properties;

    }

    #endregion

  } // CurrencyExchangeProperties

  [Serializable]
  public class CurrencyIsoType
  {
    public String IsoCode;
    public CurrencyExchangeType Type;

    public CurrencyIsoType()
    {
    }

    public CurrencyIsoType(String IsoCode, CurrencyExchangeType Type)
    {
      this.IsoCode = IsoCode;
      this.Type = Type;
    }

    public override int GetHashCode()
    {
      if (IsoCode == null) return 0;
      return IsoCode.GetHashCode() ^ Type.GetHashCode();
    }

    public override bool Equals(object obj)
    {
      return (IsoCode == ((CurrencyIsoType)obj).IsoCode) && (Type == ((CurrencyIsoType)obj).Type);
    }

    public bool IsCasinoChip
    {
      get
      {
        return (this.IsoCode.Equals(Cage.CHIPS_COLOR) || this.Type.Equals(CurrencyExchangeType.CASINOCHIP)
          || this.Type.Equals(CurrencyExchangeType.CASINO_CHIP_RE) || this.Type.Equals(CurrencyExchangeType.CASINO_CHIP_NRE) || this.Type.Equals(CurrencyExchangeType.CASINO_CHIP_COLOR));
      }
    }

    /// <summary>
    /// Returns if it is National currency
    /// </summary>
    /// <param name="NationalCurrency"></param>
    public bool IsNationalCurrency(String NationalCurrency)
    {
      return (this.Type.Equals(CurrencyExchangeType.CURRENCY))
                && ((this.IsoCode.Equals(NationalCurrency) || String.IsNullOrEmpty(this.IsoCode)));
    }

    public class SortComparer : IComparer<CurrencyIsoType>
    {
      // PURPOSE: Implements Compare method to sort CurrencyIsoTypes objects
      //
      // PARAMS:
      //     - INPUT:
      //           - CurrencyIsoType x: First element to compare
      //           - CurrencyIsoType y: Second element to compare
      //
      // RETURNS:
      //       0: If both objects are equals
      //      -1: If x is less than y
      //       1: If x is greater than y
      //
      public Int32 Compare(CurrencyIsoType x, CurrencyIsoType y)
      {
        Int32 _compare_iso_code;
        String _iso_code;

        _iso_code = CurrencyExchange.GetNationalCurrency();

        _compare_iso_code = x.IsoCode.CompareTo(y.IsoCode);
        if (_compare_iso_code == 0)
        {
          return x.Type.CompareTo(y.Type);
        }

        if (x.IsoCode == _iso_code)
        {
          return -1;
        }

        if (y.IsoCode == _iso_code)
        {
          return 1;
        }

        return _compare_iso_code;
      }
    }
  }


  public class CurrencyExchangeVoucher
  {
    public CASHIER_MOVEMENT CashierMovement { get; set; }
    public Decimal ChangeRate { get; set; }
    public Decimal Amount { get; set; }
    public Decimal NRAmount { get; set; }
    public Decimal NetAmountDevolution { get; set; }
    public Decimal EquivalChange { get; set; }
    public Decimal Comission { get; set; }
    public Decimal TotalAmount { get; set; }
    public String InIsoCode { get; set; }
    public String OutIsoCode { get; set; }
    public String[] AccountInfo { get; set; }

    #region constructor
    //------------------------------------------------------------------------------
    // PURPOSE : constructor inicialize filters to default value
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    public CurrencyExchangeVoucher(String[] AccountInfo, CASHIER_MOVEMENT CashierMovement, CurrencyExchangeResult CurrencyExchangeResult)
    //Decimal ChangeRate, 
    //                            Decimal Comission, Decimal Amount, Decimal EquivalChange, Decimal TotalAmount,
    //                          Decimal NRAmount, String InIsoCode, String OutIsoCode)
    {
      this.AccountInfo = AccountInfo;
      this.CashierMovement = CashierMovement;
      this.ChangeRate = CurrencyExchangeResult.ChangeRate;
      this.Comission = CurrencyExchangeResult.Comission;
      this.Amount = CurrencyExchangeResult.InAmount;
      this.NetAmountDevolution = CurrencyExchangeResult.NetAmountDevolution;
      this.EquivalChange = CurrencyExchangeResult.GrossAmount;
      this.TotalAmount = CurrencyExchangeResult.NetAmount;
      this.NRAmount = CurrencyExchangeResult.NR2Amount;
      this.InIsoCode = CurrencyExchangeResult.InCurrencyCode;
      this.OutIsoCode = CurrencyExchangeResult.OutCurrencyCode;
    }

    #endregion //constructor
  }
}