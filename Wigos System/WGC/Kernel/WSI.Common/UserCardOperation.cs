﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common
{
  public class UserCardOperation
  {

    public Boolean CardLinkedToAnotherUsers(String TrackData, Int64 NewLinkedID, out List<UserCard> CurrentUsersLinked)
    {
      StringBuilder _sb;
      CurrentUsersLinked = new List<UserCard>();

      try
      {
        _sb = new StringBuilder();

        _sb.Append("   SELECT   CA_LINKED_TYPE                                                                   ");
        _sb.Append("          , CA_LINKED_ID                                                                     ");
        _sb.Append("          , CASE                                                                             ");
        _sb.Append("              WHEN CA_LINKED_TYPE = @pTypePlayer THEN                                        ");
        _sb.Append("                AC_HOLDER_NAME                                                               ");
        _sb.Append("              WHEN CA_LINKED_TYPE =  @pTypeMobileBank THEN                                   ");
        _sb.Append("                MB_HOLDER_NAME                                                               ");
        _sb.Append("              WHEN CA_LINKED_TYPE IN (@pTypeChangeStacker, @pTypeTech, @pTypeEmployee) THEN  ");
        _sb.Append("                GU_FULL_NAME                                                                 ");
        _sb.Append("            END AS HOLDER_NAME                                                               ");
        _sb.Append("     FROM   CARDS C                                                                          ");
        _sb.Append("LEFT JOIN   ACCOUNTS AC                                                                      ");
        _sb.Append("       ON   C.CA_LINKED_ID = AC.AC_ACCOUNT_ID                                                ");
        _sb.Append("LEFT JOIN   MOBILE_BANKS MB                                                                  ");

        _sb.Append("       ON   C.CA_LINKED_ID = MB.MB_ACCOUNT_ID                                                ");
        _sb.Append("LEFT JOIN   GUI_USERS GU                                                                     ");
        _sb.Append("       ON   C.CA_LINKED_ID = GU.GU_USER_ID                                                   ");
        _sb.Append("    WHERE   C.CA_TRACKDATA = @pTrackData                                                     ");
        _sb.Append("      AND   C.CA_LINKED_ID <> @pCurrentLinkedID                                              ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pTrackData", SqlDbType.Char, 20).Value = TrackData;
            _sql_cmd.Parameters.Add("@pCurrentLinkedID", SqlDbType.BigInt).Value = NewLinkedID;
            _sql_cmd.Parameters.Add("@pTypePlayer", SqlDbType.Int).Value = WCP_CardTypes.CARD_TYPE_PLAYER;
            _sql_cmd.Parameters.Add("@pTypeMobileBank", SqlDbType.Int).Value = WCP_CardTypes.CARD_TYPE_MOBILE_BANK;
            _sql_cmd.Parameters.Add("@pTypeChangeStacker", SqlDbType.Int).Value = WCP_CardTypes.CARD_TYPE_CHANGE_STACKER;
            _sql_cmd.Parameters.Add("@pTypeTech", SqlDbType.Int).Value = WCP_CardTypes.CARD_TYPE_TECH;
            _sql_cmd.Parameters.Add("@pTypeEmployee", SqlDbType.Int).Value = WCP_CardTypes.CARD_TYPE_EMPLOYEE;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              while (_reader.Read())
              {
                if (!_reader.IsDBNull(1))
                {
                  WCP_CardTypes _linked_type = (WCP_CardTypes)_reader.GetInt32(0);
                  Int32 _user_id =  Convert.ToInt32(_reader.GetInt64(1));

                  UserCard _card = new UserCard(_user_id, TrackData, _linked_type);
                  _card.HolderName = _reader.IsDBNull(2) ? String.Empty : _reader.GetString(2);

                  CurrentUsersLinked.Add(_card);
                }
              }
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

  }
}
