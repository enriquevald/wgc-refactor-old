//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: XML.cs
// 
//   DESCRIPTION: Some XML utils
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 12-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAR-2007 AJQ    First release.
// 20-JAN-2016 ACC    Product Backlog Item 8580: iMB: Rechazar recargas retrasadas
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Collections;
using System.Threading;
using System.Xml;

using WSI.Common;

namespace WSI.Common
{
  public interface IXmlSink
  {
    void Process(Object Listener,
                  Object Client,
                  String Xml);
  }

  public class AuthenticateTask : Task
  {
    SecureTcpClient client;

    public AuthenticateTask(SecureTcpClient Client)
    {
      client = Client;
    }

    public override void Execute()
    {
      SecureTcpClient.SecureTcpClientAuthenticateServer(client);
    }
  }

  /// <summary>
  /// Secure TCP client
  /// </summary>
  public class SecureTcpClient
  {
    private static long m_sequence_id = 0;

    // SENDING-STATS
    //////private static UInt64   m_snd_num = 0;
    //////private static TimeSpan m_snd_sum = new TimeSpan(0);
    //////private static TimeSpan m_snd_max = new TimeSpan(0);
    //////private static TimeSpan m_lck_sum = new TimeSpan(0);
    //////private static TimeSpan m_lck_max = new TimeSpan(0);
    //////private static ReaderWriterLock m_snd_lock = new ReaderWriterLock();

    private Object m_lock = new Object();

    internal Int64 m_id;
    internal SecureTcpServer m_server;
    private TcpClient m_tcp_client;
    private NetworkStream m_tcp_stream;
    private SslStream m_ssl_stream;
    internal byte[] m_rcv_buffer;

    //private string            m_str_buffer;
    private StringBuilder m_str_builder = new StringBuilder();
    IPEndPoint m_end_point;
    private String m_friendly_name;
    private int m_internal_id;
    internal Int64 m_session_id;
    internal TerminalTypes m_terminal_type;

    internal Queue m_msg_rcvd = Queue.Synchronized(new Queue());
    internal Int32 m_tick_last_activity = Misc.GetTickCount();

    // ACC 20-JAN-2016
    int m_diff_datetime_milliseconds;

    //////private  Boolean          m_sending = false;
    //////private  Queue            m_queue   = Queue.Synchronized(new Queue ());



    public Int64 UniqueID
    {
      get
      {
        return m_id;
      }
    }

    public String Identity
    {
      get
      {
        return m_friendly_name;
      }
      set
      {
        if (m_friendly_name.Equals(value))
        {
          return;
        }

        NetLog.Add(NetEvent.IdentityChange, m_friendly_name, "from now on: " + value);

        m_friendly_name = value;
      }
    }



    public int InternalId
    {
      get
      {
        return m_internal_id;
      }
      set
      {
        if (m_internal_id == value)
        {
          return;
        }

        m_internal_id = value;

        // AJQ 02-APR-2012, Close all the previous clients (same terminal_id, older id)
        if (m_internal_id != 0 && m_server != null)
        {
          m_server.CloseOldSessions(m_internal_id, m_id);
        }
      }
    }

    public TerminalTypes TerminalType
    {
      get
      {
        return m_terminal_type;
      }
      set
      {
        m_terminal_type = value;
      }
    }

    public bool IsConnected
    {
      get
      {
        try
        {
          if (m_tcp_client == null) return false;
          if (m_tcp_client.Client == null) return false;

          return m_tcp_client.Client.Connected;
        }
        catch
        {
          return false;
        }
      }
    }

    public Int64 SessionId
    {
      get
      {
        return m_session_id;
      }
      set
      {
        if (m_session_id.Equals(value))
        {
          return;
        }

        m_session_id = value;
      }
    }

    public String AppendString(String Value)
    {
      m_str_builder.Append(Value);

      return m_str_builder.ToString();
    }

    public String StringBuffer
    {
      get
      {
        return m_str_builder.ToString();
      }
      set
      {
        m_str_builder.Length = 0;
        m_str_builder.Append(value);
      }
    }

    public IPEndPoint RemoteEndPoint
    {
      get
      {
        return (IPEndPoint)m_tcp_client.Client.RemoteEndPoint;
      }
    }

    public int DiffDatetimeMilliSeconds
    {
      get
      {
        return m_diff_datetime_milliseconds;
      }
      set
      {
        m_diff_datetime_milliseconds = value;
      }
    }


    /// <summary>
    /// Creates a secure client
    /// </summary>
    /// <param name="Server">The Server</param>
    /// <param name="TcpClient">The TCP client to secure</param>
    internal SecureTcpClient(SecureTcpServer Server, TcpClient TcpClient)
    {
      // Unique ID
      m_id = Interlocked.Increment(ref m_sequence_id);

      m_server = Server;
      m_tcp_client = TcpClient;
      m_tcp_stream = m_tcp_client.GetStream();
      m_ssl_stream = new SslStream(m_tcp_stream, false);

      // Set TCP parameters
      //  - Delay
      m_tcp_client.NoDelay = false;
      //  - Receive parameters
      m_tcp_client.ReceiveTimeout = m_server.ReceiveTimeout;
      m_tcp_client.ReceiveBufferSize = m_server.ReceiveBufferSize;
      //  - Send parameters
      m_tcp_client.SendTimeout = m_server.SendTimeout;
      m_tcp_client.SendBufferSize = m_server.SendBufferSize;

      //  - Remote Info
      m_end_point = (IPEndPoint)m_tcp_client.Client.RemoteEndPoint;
      m_friendly_name = m_end_point.ToString();

      //  - Receive Buffer
      m_rcv_buffer = new byte[m_server.ApplicationBufferSize];
    }

    /// <summary>
    /// Starts the client
    /// </summary>
    internal void Start()
    {
      m_server.AuthenticateClient(this);
      //m_thread.Start(this);
    }

    private int MaxToBeRead
    {
      get
      {
        //////int _size;

        //////_size = Math.Max(1, m_tcp_client.Available);
        //////_size = Math.Min(_size, m_rcv_buffer.Length);

        return m_rcv_buffer.Length;
      }
    }




    /// <summary>
    /// 
    /// </summary>
    /// <param name="AsyncResult"></param>
    private static void ReadCallback(IAsyncResult AsyncResult)
    {
      SecureTcpClient _client;
      int _num_read_bytes;
      Boolean _disconnected;
      SecureTcpServer _server;

      _client = (SecureTcpClient)AsyncResult.AsyncState;
      _server = _client.m_server;

      // SSL Connection
      _num_read_bytes = 0;
      _disconnected = true;

      try
      {
        lock (_client.m_lock)
        {
          if (_client.m_ssl_stream == null)
          {
            return;
          }
          _num_read_bytes = _client.m_ssl_stream.EndRead(AsyncResult);
        }

        if (_num_read_bytes <= 0)
        {
          return;
        }

        _client.m_tick_last_activity = Misc.GetTickCount();

        _server.DataReceived(_client, _num_read_bytes, _client.m_rcv_buffer);

        lock (_client.m_lock)
        {
          if (_client.m_ssl_stream == null)
          {
            return;
          }
          _client.m_ssl_stream.BeginRead(_client.m_rcv_buffer,
                                         0,
                                         _client.MaxToBeRead,
                                         new AsyncCallback(ReadCallback),
                                         _client);

          _disconnected = false;
        }
      }
      catch { }
      finally
      {
        if (_disconnected)
        {
          _server.ClientDisconnected(_client);
          _server.Remove(_client);
          _client.Close();
        }
      }
    }

    /// <summary>
    /// Secure TCP client thread: Authentification.
    /// </summary>
    /// <param name="SecureTcpClient"></param>
    internal static void SecureTcpClientAuthenticateServer(Object SecureTcpClient)
    {
      SecureTcpClient _client;
      SecureTcpServer _server;
      SslStream _ssl_stream;
      Boolean _connected;

      _client = (SecureTcpClient)SecureTcpClient;
      _server = _client.m_server;
      lock (_client.m_lock)
      {
        _ssl_stream = _client.m_ssl_stream;
      }

      _connected = false;

      try
      {
        if (_ssl_stream == null)
        {
          return;
        }

        // Authenticate the server 
        _ssl_stream.AuthenticateAsServer(_server.m_certificate,  // X509 Certificate
                                          false,                 // Client certificate requiered
                                          SslProtocols.Default,  // Protocols
                                          false);                // Check revocation

        if (_ssl_stream.IsAuthenticated)
        {
          _ssl_stream.ReadTimeout = _client.m_tcp_client.ReceiveTimeout;
          _ssl_stream.WriteTimeout = _client.m_tcp_client.SendTimeout;
          _ssl_stream.BeginRead(_client.m_rcv_buffer,
                                0,
                                _client.MaxToBeRead,
                                new AsyncCallback(ReadCallback),
                                _client);

          _server.ClientConnected(_client);

          _connected = true;
        }
      }
      catch { }
      finally
      {
        if (!_connected)
        {
          // Close connection
          _server.ClientDisconnected(_client);
          _server.Remove(_client);
          _client.Close();
        }
      }
    }

    public bool Send(String Xml)
    {
      try
      {
        Byte[] _msg;

        _msg = Encoding.UTF8.GetBytes(Xml);

        return this.Send(_msg, 0, _msg.Length);
      }
      catch
      { }

      return false;
    }

    /// <summary>
    /// Send data
    /// </summary>
    /// <param name="Buffer"></param>
    /// <param name="Offset"></param>
    /// <param name="Count"></param>
    protected internal bool Send(Byte[] Buffer, int Offset, int Count)
    {
      try
      {
        lock (m_lock)
        {
          if (m_ssl_stream == null)
          {
            return false;
          }
          m_ssl_stream.Write(Buffer, Offset, Count);
          m_tick_last_activity = Misc.GetTickCount();

          return true;
        }
      }
      catch { }

      return false;
    }

    internal void Close()
    {
      try
      {
        lock (m_lock)
        {
          if (m_ssl_stream != null)
          {
            m_ssl_stream.Close();
            m_ssl_stream.Dispose();
            m_ssl_stream = null;
          }
          if (m_tcp_stream != null)
          {
            m_tcp_stream.Close();
            m_tcp_stream.Dispose();
            m_tcp_stream = null;
          }
          if (m_tcp_client != null)
          {
            m_tcp_client.Close();
            m_tcp_client = null;
          }
        }
      }
      catch {}
    }
  }

  /// <summary>
  /// The Secure Server
  /// </summary>
  public class SecureTcpServer
  {
    // TCP parameters
    private const int RCV_BUFFER_SIZE = 32 * 1024;
    private const int SND_BUFFER_SIZE = 32 * 1024;
    private const int APP_BUFFER_SIZE = 8 * 1024;
    private const int RCV_TIMEOUT = 2 * 60 * 1000;
    private const int SND_TIMEOUT = 2 * 60 * 1000;
    private const int CONNECTION_TIMEOUT = SecureTcpClientConnection.Timeout;

    internal TcpListener m_tcp_listener;
    internal ArrayList m_connections;
    private ReaderWriterLock m_connections_lock;
    internal X509Certificate2 m_certificate;
    private ArrayList clients;
    private ReaderWriterLock clients_lock;
    WorkerPool m_authenticate_pool;

    private int m_last_connection_timeout_tick = Misc.GetTickCount();

    // XML Server
    private Boolean m_xml_server = false;
    private String m_xml_str_tag = "";
    private String m_xml_end_tag = "";
    private int m_xml_max_len = 0;
    private Encoding m_xml_encoding = null;

    private AutoResetEvent m_xml_rcvd_event = new AutoResetEvent(false);


    public int ReceiveBufferSize
    {
      get { return RCV_BUFFER_SIZE; }
    }
    public int SendBufferSize
    {
      get { return SND_BUFFER_SIZE; }
    }
    public int ApplicationBufferSize
    {
      get { return APP_BUFFER_SIZE; }
    }
    public int ReceiveTimeout
    {
      get { return RCV_TIMEOUT; }
    }
    public int SendTimeout
    {
      get { return SND_TIMEOUT; }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Check inactive clients
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void TimeoutConnections()
    {
      if (Misc.GetElapsedTicks(m_last_connection_timeout_tick) <= CONNECTION_TIMEOUT / 10)
      {
        return;
      }

      try
      {
        ArrayList _timeout = new ArrayList();

        m_connections_lock.AcquireReaderLock(Timeout.Infinite);
        try
        {
          foreach (SecureTcpClient _client in m_connections)
          {
            if (Misc.GetElapsedTicks(_client.m_tick_last_activity) >= CONNECTION_TIMEOUT)
            {
              // Timeout 
              _timeout.Add(_client);
            }
          }
        }
        catch
        {
        }
        finally
        {
          m_connections_lock.ReleaseReaderLock();
        }

        foreach (SecureTcpClient _client in _timeout)
        {
          _client.Close();
        }

        _timeout.Clear();
      }
      catch
      {
      }
      finally
      {
        m_last_connection_timeout_tick = Misc.GetTickCount();
      }
    } // TimeoutClients

    //------------------------------------------------------------------------------
    // PURPOSE : Close previous sessions of the given client (InternalId)
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //

    internal void CloseOldSessions(int InternalId, Int64 UniqueID)
    {
      ArrayList _old_clients = new ArrayList();

      try
      {
        clients_lock.AcquireReaderLock(Timeout.Infinite);
        foreach (SecureTcpClient _client in clients)
        {
          if (_client.InternalId == InternalId && _client.m_id < UniqueID)
          {
            _old_clients.Add(_client);
          }
        }
      }
      finally
      {
        clients_lock.ReleaseReaderLock();
      }

      foreach (SecureTcpClient _client in _old_clients)
      {
        _client.Close();
      }
    } // CloseOldSessions

    private void XmlMsgReceivedThread()
    {
      String _xml_msg;
      long _num_pending;

      while (true)
      {
        try
        {
          // AJQ 03-APR-2012, Check Timeout 
          TimeoutConnections();

          if (!m_xml_rcvd_event.WaitOne(1000))
          {
            continue;
          }

          _num_pending = 0;
          clients_lock.AcquireReaderLock(Timeout.Infinite);
          try
          {
            foreach (SecureTcpClient _client in clients)
            {
              if (_client.m_msg_rcvd.Count == 0)
              {
                continue;
              }
              try
              {
                _xml_msg = (String)_client.m_msg_rcvd.Dequeue();
                //Log.Message(_xml_msg);
                XmlMesageReceived(_client, _xml_msg);
                //Log.Message("");
              }
              catch (Exception _ex)
              {
                Log.Exception(_ex);
              }
              _num_pending += _client.m_msg_rcvd.Count;
            }
          }
          finally
          {
            clients_lock.ReleaseReaderLock();
          }
          if (_num_pending > 0)
          {
            m_xml_rcvd_event.Set();
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      } // while ( true )
    }





    protected void SetXmlServerParameters(String XmlStartTag, String XmlEndTag, int XmlMaxLength, Encoding Encoding)
    {
      m_xml_server = true;
      m_xml_str_tag = XmlStartTag;
      m_xml_end_tag = XmlEndTag;
      m_xml_max_len = XmlMaxLength;
      m_xml_encoding = Encoding;
    }

    public void AuthenticateClient(SecureTcpClient Client)
    {
      m_authenticate_pool.EnqueueTask(new AuthenticateTask(Client));
    }

    public ArrayList Clients
    {
      get
      {
        return clients;
      }
    }

    public ReaderWriterLock m_clients_rw_lock
    {
      get
      {
        return clients_lock;
      }
    }

    public bool IsConnected(SecureTcpClient SecureClient)
    {
      try
      {
        clients_lock.AcquireReaderLock(Timeout.Infinite);
        if (clients.Contains(SecureClient))
        {
          return SecureClient.IsConnected;
        }
        return false;
      }
      finally
      {
        clients_lock.ReleaseReaderLock();
      }
    }

    public virtual void DataReceived(SecureTcpClient SecureClient, int DataLength, byte[] Data)
    {
      if (m_xml_server)
      {
        XmlProcess(SecureClient, DataLength, Data);

        return;
      }
      return;
    }

    public virtual void XmlMesageReceived(SecureTcpClient SecureClient, String XmlMessage)
    {
      return;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="SecureTcpClient"></param>
    /// <param name="DataLength"></param>
    /// <param name="Data"></param>
    protected void XmlProcess(SecureTcpClient SecureTcpClient, int DataLength, byte[] Data)
    {
      String _xml_msg;
      String _xml_buffer;
      int _idx_stx;
      int _end_stx;
      int _idx_etx;
      int _end_etx;

      _xml_buffer = SecureTcpClient.AppendString(m_xml_encoding.GetString(Data, 0, DataLength));

      try
      {
        while (true)
        {
          // Index of starting tag
          _idx_stx = _xml_buffer.IndexOf(m_xml_str_tag);
          if (_idx_stx < 0)
          {
            break;
          }

          _end_stx = _idx_stx + m_xml_str_tag.Length;

          // Index of ending tag
          _idx_etx = _xml_buffer.IndexOf(m_xml_end_tag, _end_stx, _xml_buffer.Length - _end_stx);
          if (_idx_etx < 0)
          {
            if (_idx_stx > 0)
            {
              // - Remove char before STX
              _xml_buffer = _xml_buffer.Substring(_idx_stx);
              _idx_stx = 0;
            }
            break;
          }
          _end_etx = _idx_etx + m_xml_end_tag.Length;

          // Both STX/ETX exist
          // - Get 1st Message
          _xml_msg = _xml_buffer.Substring(_idx_stx, _end_etx - _idx_stx);
          // - Remove 1st Message
          _xml_buffer = _xml_buffer.Substring(_end_etx);

          // Fire event
          SecureTcpClient.m_msg_rcvd.Enqueue(_xml_msg);
          SecureTcpClient.m_server.m_xml_rcvd_event.Set();

          // XmlMesageReceived (SecureTcpClient, _xml_msg);
        } // while ( true )      
      }
      finally
      {
        if (_xml_buffer.Length >= m_xml_max_len)
        {
          Log.Warning("Message-OVERFLOW " + _xml_buffer.Length + " ---> " + _xml_buffer);
          _xml_buffer = "";
        }

        SecureTcpClient.StringBuffer = _xml_buffer;
      }

    } // XmlProcess

    public virtual void ClientConnected(SecureTcpClient SecureClient)
    {
      clients_lock.AcquireWriterLock(Timeout.Infinite);
      try
      {
        clients.Add(SecureClient);
      }
      finally
      {
        clients_lock.ReleaseWriterLock();
      }
    }

    public virtual void ClientDisconnected(SecureTcpClient SecureClient)
    {
      clients_lock.AcquireWriterLock(Timeout.Infinite);
      try
      {
        clients.Remove(SecureClient);
      }
      finally
      {
        clients_lock.ReleaseWriterLock();
      }
    }

    public IPEndPoint LocalEndpoint
    {
      get
      {
        return (IPEndPoint)m_tcp_listener.LocalEndpoint;
      }
    }

    /// <summary>
    /// Initialize secure server
    /// </summary>
    /// <param name="IPEndPoint"></param>
    protected SecureTcpServer(IPEndPoint IPEndPoint)
    {
      // Instantiate an instance of the TcpListener
      m_tcp_listener = new TcpListener(IPEndPoint);
      m_connections = new ArrayList(100);

      // Init ReaderWriterLock
      m_connections_lock = new ReaderWriterLock();

      try
      {
        m_certificate = new X509Certificate2(@"WCP_ServerCertificate.pfx", "xixero08");
      }
      catch (Exception ex)
      {
        Log.Error(" Security Certificate file was not found");
        Log.Exception(ex);
      }

      clients = new ArrayList();
      // Init ReaderWriterLock
      clients_lock = new ReaderWriterLock();

      //authenticate_queue  = new WaitableQueue ();
      //authenticate_worker = new Worker ("SecureTcpServer", authenticate_queue);

      int _num_workers;

      _num_workers = 10;
      if (Environment.GetEnvironmentVariable("SSL_WORKERS") != null)
      {
        int.TryParse(Environment.GetEnvironmentVariable("SSL_WORKERS"), out _num_workers);
      }
      m_authenticate_pool = new WorkerPool("SSL-Auth", _num_workers);


      Thread _thread;
      _thread = new Thread(new ThreadStart(this.XmlMsgReceivedThread));
      _thread.Start();
    }

    /// <summary>
    /// Start listening
    /// </summary>
    public void Start()
    {
      // Start listening for incoming connections.
      try
      {
        m_tcp_listener.Start();
      }
      catch (Exception ex)
      {
        Log.Message(" Configuration file does not have a valid Vendor IP address.");
        Log.Exception(ex);

        return;
      }

      NetLog.Add(NetEvent.ListenerStarted, m_tcp_listener.LocalEndpoint.ToString(), "");

      // Accept connections asynchronously.
      this.m_tcp_listener.BeginAcceptTcpClient(new AsyncCallback(AcceptTcpClient), this);
    }

    public void Stop()
    {
      try
      {
        m_tcp_listener.Stop();

        NetLog.Add(NetEvent.ListenerStopped, m_tcp_listener.LocalEndpoint.ToString(), "");

        m_connections_lock.AcquireReaderLock(Timeout.Infinite);
        try
        {
          // Close all the connections
          foreach (SecureTcpClient _client in m_connections)
          {
            _client.Close();
          }
        }
        finally
        {
          m_connections_lock.ReleaseReaderLock();
        }

      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return;
      }
      finally
      {
        m_tcp_listener = null;
      }
    }

    internal void Remove(SecureTcpClient SecureTcpClient)
    {
      m_connections_lock.AcquireWriterLock(Timeout.Infinite);
      try
      {
        m_connections.Remove(SecureTcpClient);
      }
      finally
      {
        m_connections_lock.ReleaseWriterLock();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="AsyncResult"></param>
    private static void AcceptTcpClient(IAsyncResult AsyncResult)
    {
      SecureTcpServer server;
      SecureTcpClient client;
      TcpClient tcp_client;

      server = (SecureTcpServer)AsyncResult.AsyncState;

      try
      {
        // End the asynchronous operation
        tcp_client = server.m_tcp_listener.EndAcceptTcpClient(AsyncResult);

        // Create the secure client
        client = new SecureTcpClient(server, tcp_client);

        // Add to server
        server.m_connections_lock.AcquireWriterLock(Timeout.Infinite);
        try
        {
          server.m_connections.Add(client);
        }
        finally
        {
          server.m_connections_lock.ReleaseWriterLock();
        }
        // Start Client
        client.Start();
      }
      catch (Exception ex)
      {
        NetLog.Add(NetEvent.ListenerError, server.m_tcp_listener.LocalEndpoint.ToString(), "");
        Log.Exception(ex);

        return;
      }
      finally
      {
        tcp_client = null;
        client = null;

        try
        {
          int _delay;

          // 23-APR-2010, Force a delay before accepting a new client
          _delay = 0; // Default delay 
          if (Environment.GetEnvironmentVariable("SSL_ACCEPT_DELAY") != null)
          {
            int.TryParse(Environment.GetEnvironmentVariable("SSL_ACCEPT_DELAY"), out _delay);
          }
          if (_delay > 0)
          {
            System.Threading.Thread.Sleep(_delay);
          }
        }
        catch
        {
          ;
        }
        // Continue acception connections
        server.m_tcp_listener.BeginAcceptTcpClient(new AsyncCallback(AcceptTcpClient), server);

        server = null;
      }
    }
  }
}

