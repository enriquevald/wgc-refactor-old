﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: WCPPlayerRequestMessage.cs
// 
//      DESCRIPTION: Class to create and manage message with player request
// 
//           AUTHOR: Ferran Ortner
// 
//    CREATION DATE: 31-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-OCT-2017 FOS    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Xml;

namespace WSI.Common
{
  public class WCPPlayerRequestMessage
  {
    public int m_created_tick;

    public WCPPlayerRequestMsgHeader MsgHeader;
    public IPlayerRequestXml MsgContent;

    public Int64 TransactionId
    {
      get
      {
        switch ( MsgHeader.MsgType )
        {
          case WCPPlayerRequestMsgTypes.WCP_MsgReportPlay:
            return ( (WCPPlayerRequestMsgReportPlay) MsgContent ).TransactionId;

          case WCPPlayerRequestMsgTypes.WCP_MsgAddCredit:
            return ( (WCPPlayerRequestMsgAddCredit) MsgContent ).TransactionId;

          case WCPPlayerRequestMsgTypes.WCP_MsgMobileBankCardTransfer:
            return ((WCPPlayerRequestMsgMobileBankCardTransfer)MsgContent).TransactionId;

          case WCPPlayerRequestMsgTypes.WCP_MsgStartCardSession:
            return ((WCPPlayerRequestMsgStartCardSession)MsgContent).GetTransactionId();

          case WCPPlayerRequestMsgTypes.WCP_MsgCancelStartSession:
            return ((WCPPlayerRequestMsgCancelStartSession)MsgContent).TransactionId;

          case WCPPlayerRequestMsgTypes.WCP_MsgEndSession:
            return ((WCPPlayerRequestMsgEndSession)MsgContent).TransactionId;

          case WCPPlayerRequestMsgTypes.WCP_MsgMoneyEvent:
            return ((WCPPlayerRequestMsgMoneyEvent)MsgContent).TransactionId;

          case WCPPlayerRequestMsgTypes.WCP_TITO_MsgCreateTicket:
            return ((WCPPlayerRequestTITO_MsgCreateTicket)MsgContent).TransactionId;
 
          default:
            return 0;
        }
      }
    }

    private WCPPlayerRequestMessage()
    {
      m_created_tick = Misc.GetTickCount();
    }


    public static WCPPlayerRequestMessage CreateMessage(String Xml)
    {
      WCPPlayerRequestMessage _wcp_message;
      XmlReader   _xml_reader;
      System.IO.StringReader _txt_reader;
      bool _content_found;
      WCPPlayerRequestMsgHeader _wcp_header;
      

      _wcp_message = null;
      _xml_reader = null;
      _txt_reader = null;
      _content_found = false;


      try
      {
        _wcp_header = new WCPPlayerRequestMsgHeader();
        _txt_reader = new System.IO.StringReader(Xml);
        _xml_reader = XmlReader.Create(_txt_reader);
        
        //Loads the message header
        _wcp_header.LoadXml(_xml_reader);
        if (_wcp_header.MsgType == WCPPlayerRequestMsgTypes.WCP_MsgUnknown)
        {
          return null;
        }
        //Creates the message if the message header is correct.
        _wcp_message = CreateMessage(_wcp_header);

        //Put the xmlreader at the beginning of the Message Content
        while (_xml_reader.Read() && !_content_found)
        {
          if ((_xml_reader.Name == "WCP_MsgContent") && (_xml_reader.NodeType.Equals(XmlNodeType.Element)))
          {
            _content_found = true;
          }
        }
        if (_content_found)
        {
          _wcp_message.MsgContent.LoadXml(_xml_reader);
          if (_wcp_message.MsgContent == null)
          {
            throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag not found: " + _wcp_message.MsgHeader.MsgType.ToString());
          }
        }
        else
        {
          throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag not found: " + _wcp_message.MsgHeader.MsgType.ToString());
        }
      }
      catch (WCPPlayerRequestException wcp_ex)
      {
        throw wcp_ex;
      }
      catch (Exception ex)
      {
        // Format Error in message
        throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, ex.Message, ex);
      }
      finally 
      {
        if ( _xml_reader != null )
        {
          _xml_reader.Close ();
        }
        if ( _txt_reader != null )
        {
          _txt_reader.Close ();
          _txt_reader.Dispose ();
        }
      }
      return _wcp_message;
    }

    private static WCPPlayerRequestMessage CreateMessage(WCPPlayerRequestMsgHeader Header)
    {
      WCPPlayerRequestMessage _wcp_msg;
      IPlayerRequestXml _xml_content;
      Assembly _assembly;
      String _wcp_msg_type_name;
      Type _target;
      WCPPlayerRequestMsgTypes _msg_type;


      _msg_type = Header.MsgType;

      _wcp_msg = new WCPPlayerRequestMessage();
      _wcp_msg.MsgHeader = Header;

      if (_msg_type == WCPPlayerRequestMsgTypes.WCP_MsgReportPlay)
      {
        _wcp_msg.MsgContent = new WCPPlayerRequestMsgReportPlay();

        return _wcp_msg;
      }

      // The Power of Reflection
      _xml_content = null;

      _wcp_msg_type_name = "WSI.WCP." + _msg_type.ToString();

      _assembly = Assembly.GetExecutingAssembly();

      _target = _assembly.GetType(_wcp_msg_type_name);
      if (_target == null)
      {
        if ((_msg_type == WCPPlayerRequestMsgTypes.WCP_MsgSasMeters || _msg_type == WCPPlayerRequestMsgTypes.WCP_MsgSasMetersReply) ||
            (_msg_type == WCPPlayerRequestMsgTypes.WCP_MsgPlaySessionMeters || _msg_type == WCPPlayerRequestMsgTypes.WCP_MsgPlaySessionMetersReply))
        {
          _wcp_msg_type_name = "WSI.WCP.WCP_Messages.Meters." + _msg_type.ToString();
          _target = _assembly.GetType(_wcp_msg_type_name);
        }
      }

      Type[] _wcp_msg_types = new Type[0];
      ConstructorInfo _constructor = _target.GetConstructor(_wcp_msg_types);
      _xml_content = (IPlayerRequestXml)_constructor.Invoke(null);

      _wcp_msg.MsgContent = _xml_content;

      return _wcp_msg;
    }


    public static WCPPlayerRequestMessage CreateMessage(WCPPlayerRequestMsgTypes MsgType)
    {
      WCPPlayerRequestMsgHeader _wcp_header;

      _wcp_header = new WCPPlayerRequestMsgHeader();
      _wcp_header.MsgType = MsgType;
      
      return CreateMessage (_wcp_header);
    }



    public String ToXml ()
    {
      String xml;
      String xml_header;
      String xml_content;

      xml_header = MsgHeader.ToXml ();
      xml_content = MsgContent.ToXml ();

      xml = "";
      xml += "<WCP_Message>";
      xml += xml_header;
      xml += "<WCP_MsgContent>";
      xml += xml_content;
      xml += "</WCP_MsgContent>";
      xml += "</WCP_Message>";



      return xml;
    }
  }

  #region " Interface "
  public interface IPlayerRequestXml
  {
    String ToXml();
    void LoadXml(XmlReader Xml);
  }
  #endregion

  #region " Enums "
  public enum WCPPlayerRequestMsgTypes
  {
    // Request                          // Reply
    WCP_MsgRequestService,
    WCP_MsgRequestServiceReply, 
    WCP_MsgEnrollServer,
    WCP_MsgEnrollServerReply, 
    WCP_MsgEnrollTerminal,
    WCP_MsgEnrollTerminalReply, 
    WCP_MsgGetParameters,
    WCP_MsgGetParametersReply, 
    WCP_MsgUnenrollTerminal,
    WCP_MsgUnenrollTerminalReply, 
    WCP_MsgUnenrollServer,
    WCP_MsgUnenrollServerReply, 
    WCP_MsgGetCardType,
    WCP_MsgGetCardTypeReply, 
    WCP_MsgLockCard,
    WCP_MsgLockCardReply, 
    WCP_MsgStartCardSession,
    WCP_MsgStartCardSessionReply, 
    WCP_MsgPlayConditions,
    WCP_MsgPlayConditionsReply, 
    WCP_MsgReportPlay,
    WCP_MsgReportPlayReply, 
    WCP_MsgAddCredit,
    WCP_MsgAddCreditReply, 
    WCP_MsgEndSession,
    WCP_MsgEndSessionReply, 
    WCP_MsgReportEvent,
    WCP_MsgReportEventReply, 
    WCP_MsgExecuteCommand,
    WCP_MsgExecuteCommandReply, 
    WCP_MsgReportMeters,
    WCP_MsgReportMetersReply, 
    WCP_MsgIntegrityCheck,
    WCP_MsgIntegrityCheckReply, 
    WCP_MsgKeepAlive,
    WCP_MsgKeepAliveReply, 
    WCP_MsgReportEventList,
    WCP_MsgReportEventListReply, 
    WCP_MsgMobileBankCardCheck,
    WCP_MsgMobileBankCardCheckReply, 
    WCP_MsgMobileBankCardTransfer,
    WCP_MsgMobileBankCardTransferReply,
    WCP_MsgError,
    WCP_MsgUnknown, 
    WCP_MsgReportGameMeters,
    WCP_MsgReportGameMetersReply, 
    WCP_MsgGetLogger,
    WCP_MsgGetLoggerReply, 
    WCP_MsgReportHPCMeter,
    WCP_MsgReportHPCMeterReply, 
    WCP_MsgUpdateCardSession,
    WCP_MsgUpdateCardSessionReply, 
    WCP_MsgCardInformation,
    WCP_MsgCardInformationReply,   // Mobile bank: iPOD.
    WCP_MsgReportMachineMeters,
    WCP_MsgReportMachineMetersReply, 
    WCP_MsgMoneyEvent,
    WCP_MsgMoneyEventReply, 
    WCP_MsgCancelStartSession,
    WCP_MsgCancelStartSessionReply, 
    WCP_MsgCashSessionPendingClosing,
    WCP_MsgCashSessionPendingClosingReply, 
    WCP_MsgMBInformation,
    WCP_MsgMBInformationReply,    // Mobile bank: iPOD.
    // generic messages related with TITO development
    WCP_MsgSasMeters,
    WCP_MsgSasMetersReply, 
    WCP_MsgPlaySessionMeters,
    WCP_MsgPlaySessionMetersReply, 
    WCP_MsgEGMHandpays,
    WCP_MsgEGMHandpaysReply, 
    WCP_MsgBonusTransferStatus,
    WCP_MsgBonusTransferStatusReply, 
    WCP_MsgVersionSignature,
    WCP_MsgVersionSignatureReply, 
    WCP_MsgCheckTicket,
    WCP_MsgCheckTicketReply, // Mobile bank: iPOD.
    WCP_MsgPayTicket,
    WCP_MsgPayTicketReply,
    //JMM 03-JUL-2014: PIN validation message and its reply
    WCP_MsgPlayerCheckPin,
    WCP_MsgPlayerCheckPinReply,
    //XCD 30-APR-2014: Smib protocol parameters to send
    WCP_MsgGetProtocolParameters,
    WCP_MsgGetProtocolParametersReply,
    //JMM 4-AUG-2015 Multi-denom
    WCP_MsgMachineMultiDenomInfo,
    WCP_MsgMachineMultiDenomInfoReply,
    //FJC 02-JAN-2016 (Feature 22239:Sorteo de máquina)
    WCP_MsgTerminalDrawGetPendingDraw,
    WCP_MsgTerminalDrawGetPendingDrawReply, 
    WCP_MsgTerminalDrawProcessDraw,
    WCP_MsgTerminalDrawProcessDrawReply,
    //JMM 01-FEB-2017 Reserve Terminal
    WCP_MsgReserveTerminal,
    WCP_MsgReserveTerminalReply,
    //FJC 
    WCP_MsgTerminalDrawGameTimeElapsed,
    WCP_MsgTerminalDrawGameTimeElapsedReply,

    //FJC 27-11-2017: WIGOS-3592 MobiBank: InTouch - Recharge request
    WCP_MsgMobiBankRecharge,
    WCP_MsgMobiBankRechargeReply,
    WCP_MsgMobiBankCancelRequest,
    WCP_MsgMobiBankCancelRequestReply,
    
    //
    // AJQ 18-MAY-2012, WKT Messages
    // 
    // Reserved range: 1000..1999
    WCP_WKT_MsgGetParameters = 10001,
    WCP_WKT_MsgGetParametersReply = 10002, 
    WCP_WKT_MsgGetResource = 10003,
    WCP_WKT_MsgGetResourceReply = 10004, 
    WCP_WKT_MsgGetNextAdvStep = 10005,
    WCP_WKT_MsgGetNextAdvStepReply = 10006, 
    WCP_WKT_MsgGetPlayerGifts = 10007,
    WCP_WKT_MsgGetPlayerGiftsReply = 10008, 
    WCP_WKT_MsgPlayerRequestGift = 10009,
    WCP_WKT_MsgPlayerRequestGiftReply = 10010, 
    WCP_WKT_MsgSetPlayerFields = 10011,
    WCP_WKT_MsgSetPlayerFieldsReply = 10012, 
    WCP_WKT_MsgGetPlayerInfo = 10013,
    WCP_WKT_MsgGetPlayerInfoReply = 10014, 
    WCP_WKT_MsgGetPlayerPromos = 10015,
    WCP_WKT_MsgGetPlayerPromosReply = 10016, 
    WCP_WKT_MsgGetPlayerActivity = 10017,
    WCP_WKT_MsgGetPlayerActivityReply = 10018, 
    WCP_WKT_MsgPrintRechargesList = 10019,
    WCP_WKT_MsgPrintRechargesListReply = 10020, 
    WCP_WKT_MsgGetPlayerDraws = 10021,
    WCP_WKT_MsgGetPlayerDrawsReply = 10022, 
    WCP_WKT_MsgRequestPlayerDrawNumbers = 10023,
    WCP_WKT_MsgRequestPlayerDrawNumbersReply = 10024, 
    WCP_WKT_MsgPlayerRequestPromo = 10025,
    WCP_WKT_MsgPlayerRequestPromoReply = 10026,
    // TITO Messages. 20000..20300 Reserved
    WCP_TITO_MsgIsValidTicket = 20000,
    WCP_TITO_MsgIsValidTicketReply = 20001, 
    WCP_TITO_MsgCancelTicket = 20002,
    WCP_TITO_MsgCancelTicketReply = 20003, 
    WCP_TITO_MsgCreateTicket = 20004,
    WCP_TITO_MsgCreateTicketReply = 20005, 
    WCP_TITO_MsgEgmParameters = 20006,
    WCP_TITO_MsgEgmParametersReply = 20007, 
    WCP_TITO_MsgValidationNumber = 20008,
    WCP_TITO_MsgValidationNumberReply = 20009,
    // LCD Display Messages 21000..22000
    WCP_LCD_MsgGetParameters = 21001,
    WCP_LCD_MsgGetParametersReply = 21002, 
    WCP_LCD_MsgGetPromoBalance = 21003,
    WCP_LCD_MsgGetPromoBalanceReply = 21004,
    // GAMEGATEWAY Messages 22000..23000
    // FJC 06-OCT-2015 (Product Bakclog 4704)
    WCP_MsgSubsMachinePartialCredit = 22000,
    WCP_MsgSubsMachinePartialCreditReply = 22001, 
    WCP_MsgGameGatewayGetCredit = 22002,
    WCP_MsgGameGatewayGetCreditReply = 22003,
    // FJC 10-MAR-2016 (PBI 9105:BonoPlay: LCD: Changes; Task 10362: BonoPlay LCD: Refactorizar mensajería.)
    WCP_MsgGameGatewayProcessMsg = 22004, 
    WCP_MsgGameGatewayProcessMsgReply = 22005

  }

  public enum WCPPlayerRequestResponseCodes
  {
    WCP_RC_OK = 0,
    WCP_RC_ERROR = 1,
    WCP_RC_DATABASE_OFFLINE = 2,
    WCP_RC_SERVICE_NOT_AVAILABLE = 3,
    WCP_RC_SERVICE_NOT_FOUND = 4,
    WCP_RC_SERVER_NOT_AUTHORIZED = 5,
    WCP_RC_SERVER_SESSION_NOT_VALID = 6,
    WCP_RC_TERMINAL_NOT_AUTHORIZED = 7,
    WCP_RC_TERMINAL_SESSION_NOT_VALID = 8,
    WCP_RC_CARD_NOT_VALID = 9,
    WCP_RC_CARD_BLOCKED = 10,
    WCP_RC_CARD_EXPIRED = 11,
    WCP_RC_CARD_ALREADY_IN_USE = 12,
    WCP_RC_CARD_NOT_VALIDATED = 13,
    WCP_RC_CARD_BALANCE_MISMATCH = 14,
    WCP_RC_CARD_REMOTE_BALANCE_MISMATCH = 15,
    WCP_RC_CARD_SESSION_NOT_CLOSED = 16,
    WCP_RC_CARD_SESSION_NOT_VALID = 17,
    WCP_RC_CARD_WRONG_PIN = 18,
    WCP_RC_INVALID_SESSION_ID = 19,
    WCP_RC_UNKNOWN_GAME_ID = 20,
    WCP_RC_INVALID_AMOUNT = 21,
    WCP_RC_EVENT_NOT_VALID = 22,
    WCP_RC_CRC_MISMATCH = 23,
    WCP_RC_NOT_ENOUGH_PLAYERS = 24,

    WCP_RC_MSG_FORMAT_ERROR = 25,
    WCP_RC_INVALID_MSG_TYPE = 26,

    WCP_RC_INVALID_PROTOCOL_VERSION = 27,
    WCP_RC_INVALID_SEQUENCE_ID = 28,
    WCP_RC_INVALID_SERVER_ID = 29,
    WCP_RC_INVALID_TERMINAL_ID = 30,
    WCP_RC_INVALID_SERVER_SESSION_ID = 31,
    WCP_RC_INVALID_TERMINAL_SESSION_ID = 32,
    WCP_RC_INVALID_COMPRESSION_METHOD = 33,
    WCP_RC_INVALID_RESPONSE_CODE = 34,

    WCP_RC_UNKNOWN = 35,
    WCP_RC_LAST = 36,

    WCP_RC_SESSION_TIMEOUT = 37,

    // ACC 12-AUG-2013
    WCP_RC_RECHARGE_NOT_AUTHORIZED = 38,

    // JML 14-NOV-2014
    WCP_RC_MB_EXCEEDED_TOTAL_RECHARGES_LIMIT = 39,
    WCP_RC_MB_EXCEEDED_RECHARGE_LIMIT = 40,
    WCP_RC_MB_EXCEEDED_RECHARGES_NUMBER_LIMIT = 41,
    // JML 14-NOV-2014
    WCP_RC_EXCEEDED_MAX_ALLOWED_CASH_IN = 42,
    WCP_RC_EXCEEDED_MAX_ALLOWED_DAILY_CASH_IN = 43,
    WCP_RC_ML_RECHARGE_NOT_AUTHORIZED = 44,

    // JML 21-NOV-2014 
    WCP_RC_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED = 45,
    WCP_RC_ACCOUNT_GLOBAL_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED = 46,
    WCP_RC_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_COUNT_EXCEEDED = 47,
    WCP_RC_ACCOUNT_DAILY_MAX_ALLOWED_CASH_IN_AMOUNT_EXCEEDED = 48,

    WCP_RC_MB_SESSION_CLOSED = 49,

    // ACC 27-APR-2015 
    WCP_RC_STACKER_ID_NOT_FOUND = 50,
    WCP_RC_STACKER_ID_ALREADY_IN_USE = 51,

    // JBC 11-MAY-2015
    WCP_RC_GAMING_DAY_EXPIRED = 52,

    // YNM 28-JUL-2015
    WCP_RC_ACCOUNT_MIN_ALLOWED_CASH_IN_AMOUNT_FALL_SHORT = 53,

    // SGB 16-NOV-2015
    WCP_RC_GENERAL_PARAM_DUPLICATED = 54,

    // JMM 08-FEB-2017
    WCP_RC_TERMINAL_RESERVED = 55,
    WCP_RC_ANOTHER_TERMINAL_RESERVED = 56,

    // AJQ 18-MAY-2012, WKT Response code
    WCP_RC_WKT_OK = 10000,  // OK
    WCP_RC_WKT_DISABLED = 10001,  // Kiosk disabled         / Kiosco deshabilitado
    WCP_RC_WKT_ERROR = 10002,  // Error                  / Error
    WCP_RC_WKT_MESSAGE_NOT_SUPPORTED = 10003,  // Message Not Supported  / Mensaje no soportado
    WCP_RC_WKT_RESOURCE_NOT_FOUND = 10004,  // Resource Not found     / Recurso no encontrado

    // Login
    WCP_RC_WKT_CARD_NOT_VALID = 10010,  // Card Not Valid     / Tarjeta no válida
    WCP_RC_WKT_ACCOUNT_BLOCKED = 10011,  // Acccount Blocked   / Cuenta bloqueada
    WCP_RC_WKT_ACCOUNT_NO_PIN = 10012,  // PIN not configured / PIN sin configurar
    WCP_RC_WKT_WRONG_PIN = 10013,  // Wrong PIN          / PIN erróneo

    //GetRequest
    WKT_UNKNOWN = 10014,
    WKT_CARD_ERROR = 10015,
    WKT_NOT_ENOUGH_POINTS = 10016,
    WKT_NOT_AVAILABLE = 10017,
    WKT_NOT_ENOUGH_STOCK = 10018,
    WKT_CARD_HAS_NON_REDEEMABLE = 10019,
    WCP_RC_WKT_FUNCTIONALITY_OFF_SCHEDULE = 10020,
    WKT_PLAYER_NOT_ALLOWED_TO_REDEEM_POINTS = 10021,
    WCP_RC_WKT_FUNCTIONALITY_NOT_ENABLED = 10022,

    //TITO GetRequest
    WCP_RC_TITO_NOT_ALLOWED_ISSUANCE = 10023,
    WCP_RC_TITO_TICKET_ERROR = 10024,
    WCP_RC_TITO_MESSAGE_NOT_SUPPORTED = 10025,
    WCP_RC_TITO_COULD_NOT_CANCEL_TICKET = 10026,

    WKT_PROMO_NOT_CURRENTLY_APPLICABLE = 10027,
    WCP_RC_TITO_INVALID_VALIDATION_NUMBER = 10028,
    WCP_RC_TITO_TICKET_EMISSION_NOT_ALLOWED = 10029,
    WCP_RC_TITO_TICKET_OUT_AMOUNT_EXCEEDED = 10030,

    //WKT & TITO Errors
    WCP_RC_WKT_TITO_PRINTER_ERROR = 10031
  }

  #endregion 

  #region " Public auxiliar class "
  public class WCPPlayerRequestMsgHeader
  {
    #region CONSTANTS

    public const string PROTOCOL_VERSION = "1.0.0.6";
    const string COMPRESSION_METHOD = "NONE";

    #endregion // CONSTANTS

    #region MEMBERS

    private string ProtocolVersion;
    public string CompressionMethod;
    private DateTime SystemTime = DateTime.MinValue;
    private Int32 TimeZone;
    public string ServerId;
    public string TerminalId;
    public Int64 ServerSessionId;
    public Int64 TerminalSessionId;
    public WCPPlayerRequestMsgTypes MsgType;
    public Int64 SequenceId;
    public int MsgContentSize;
    public WCPPlayerRequestResponseCodes ResponseCode;
    public string ResponseCodeText;
    //////public static Int64 SequenceIdResponse = 0;

    #endregion // MEMBERS

    #region PUBLIC

    public enum WCPPlayerRequestCompressionMethod
    {
      NONE
    }

    /// <summary>
    ///   Gets the Protocol Version
    /// </summary>
    public string GetProtocolVersion
    {
      get { return ProtocolVersion; }
    }

    /// <summary>
    ///   Gets the Compression Method
    /// </summary>
    WCPPlayerRequestCompressionMethod GetCompressionMethod
    {
      get { return WCPPlayerRequestCompressionMethod.NONE; }
    }

    /// <summary>
    /// Gets the System Time
    /// </summary>
    public DateTime GetSystemTime
    {
      get { return SystemTime; }
    }

    /// <summary>
    /// Get time Zone
    /// </summary>
    public Int32 GetTimeZone
    {
      get { return TimeZone; }
    }

    /// <summary>
    /// Creates the reply header.
    /// The following fields are copied from the request:
    /// - ServerId
    /// - TerminalId
    /// - ServerSessionId
    /// - TerminalSessionId
    /// - SequenceId
    /// </summary>
    /// <param name="ResponseCode">Response Code</param>
    /// <returns>The reply header</returns>
    public WCPPlayerRequestMsgHeader ReplyHeader(WCPPlayerRequestResponseCodes ResponseCode)
    {
      WCPPlayerRequestMsgHeader reply;

      reply = new WCPPlayerRequestMsgHeader();
      reply.ServerId = ServerId;
      reply.TerminalId = TerminalId;
      reply.ServerSessionId = ServerSessionId;
      reply.TerminalSessionId = TerminalSessionId;
      reply.MsgType = MsgType;
      reply.MsgType++;
      reply.SequenceId = SequenceId;
      reply.ResponseCode = ResponseCode;
      reply.ResponseCodeText = "";

      return reply;
    }

    public void PrepareReply(WCPPlayerRequestMsgHeader RequestHeader)
    {
      ServerId = RequestHeader.ServerId;
      TerminalId = RequestHeader.TerminalId;
      ServerSessionId = RequestHeader.ServerSessionId;
      TerminalSessionId = RequestHeader.TerminalSessionId;
      SequenceId = RequestHeader.SequenceId;

      if (MsgType == WCPPlayerRequestMsgTypes.WCP_MsgUnknown)
      {
        MsgType = RequestHeader.MsgType + 1;
      }
    }


    /// <summary>
    /// Extracts an enum object value from a 
    /// Enum Object giving the string name of 
    /// the value and getting the index.
    /// </summary>
    /// <param name="ResponseCode">Response Code</param>
    /// <returns>The reply header</returns>
    public static Object Decode(String str, Object defaultValue)
    {
      foreach (String s in System.Enum.GetNames(defaultValue.GetType()))
      {
        if (s.Equals(str))
        {
          return System.Enum.Parse(defaultValue.GetType(), s);
        }
      }
      return defaultValue;
    }

    public void LoadXml(XmlReader reader)
    {
      ProtocolVersion = XML.GetValue(reader, "ProtocolVersion");
      CompressionMethod = XML.GetValue(reader, "CompressionMethod");

      string time;
      string time_zone_hours;
      string time_zone_minutes;
      Int32 minutes;

      time = XML.GetValue(reader, "SystemTime");

      DateTime.TryParse(time, out SystemTime);

      time_zone_hours = time.Substring(time.Length - 6, 3);
      time_zone_minutes = time.Substring(time.Length - 2, 2);

      Int32.TryParse(time_zone_hours, out TimeZone);
      Int32.TryParse(time_zone_minutes, out minutes);
      TimeZone *= 60;
      if (TimeZone >= 0)
      {
        TimeZone += minutes;
      }
      else
      {
        TimeZone -= minutes;
      }

      ServerId = XML.GetValue(reader, "ServerId");
      TerminalId = XML.GetValue(reader, "TerminalId");
      long.TryParse(XML.GetValue(reader, "ServerSessionId"), out ServerSessionId);
      long.TryParse(XML.GetValue(reader, "TerminalSessionId"), out TerminalSessionId);
      MsgType = (WCPPlayerRequestMsgTypes)Decode(XML.GetValue(reader, "MsgType"), WCPPlayerRequestMsgTypes.WCP_MsgUnknown);
      long.TryParse(XML.GetValue(reader, "SequenceId"), out SequenceId);
      int.TryParse(XML.GetValue(reader, "MsgContentSize"), out MsgContentSize);
      ResponseCode = (WCPPlayerRequestResponseCodes)Decode(XML.GetValue(reader, "ResponseCode"), WCPPlayerRequestResponseCodes.WCP_RC_UNKNOWN);
      ResponseCodeText = XML.GetValue(reader, "ResponseCodeText");
    }

    public String ToXml()
    {

      String _str_date;
      StringBuilder _strtemp = new StringBuilder(1024);

      if (SystemTime == DateTime.MinValue)
      {
        SystemTime = DateTime.Now;
      }

      _str_date = XML.XmlDateTimeString(SystemTime);

      _strtemp.Append("<WCP_MsgHeader><ProtocolVersion>"); //WCPPlayerRequestMsgHeader FOS
      _strtemp.Append(ProtocolVersion);
      _strtemp.Append("</ProtocolVersion><CompressionMethod>");
      _strtemp.Append(CompressionMethod);
      _strtemp.Append("</CompressionMethod><SystemTime>");
      _strtemp.Append(_str_date);
      _strtemp.Append("</SystemTime><ServerId>");
      _strtemp.Append(XML.StringToXmlValue(ServerId));
      _strtemp.Append("</ServerId><TerminalId>");
      _strtemp.Append(XML.StringToXmlValue(TerminalId));
      _strtemp.Append("</TerminalId><ServerSessionId>");
      _strtemp.Append(ServerSessionId);
      _strtemp.Append("</ServerSessionId><TerminalSessionId>");
      _strtemp.Append(TerminalSessionId);
      _strtemp.Append("</TerminalSessionId><MsgType>");
      _strtemp.Append(MsgType);
      _strtemp.Append("</MsgType><SequenceId>");
      _strtemp.Append(SequenceId);
      _strtemp.Append("</SequenceId><MsgContentSize>");
      _strtemp.Append(MsgContentSize);
      _strtemp.Append("</MsgContentSize><ResponseCode>");
      _strtemp.Append(ResponseCode);
      _strtemp.Append("</ResponseCode><ResponseCodeText>");
      _strtemp.Append(XML.StringToXmlValue(ResponseCodeText));
      _strtemp.Append("</ResponseCodeText></WCP_MsgHeader>"); //WCPPlayerRequestMsgHeader 

      return _strtemp.ToString();
    }
    /// <summary>
    /// Adds the header to the given Xml WCP message
    /// </summary>
    /// <param name="XmlWcpMessage">The message</param>
    public void ToXml(XmlDocument XmlWcpMessage)
    {
      XmlNode root;
      XmlNode header;
      XmlNode old_header;
      DateTime now;
      string str;

      now = DateTime.Now;
      str = XML.XmlDateTimeString(now);

      root = XmlWcpMessage.SelectSingleNode("WCP_Message");
      header = XmlWcpMessage.CreateNode(XmlNodeType.Element, "", "WCP_MsgHeader", ""); //WCPPlayerRequestMsgHeader

      XML.AppendChild(header, "ProtocolVersion", ProtocolVersion);
      XML.AppendChild(header, "CompressionMethod", CompressionMethod);
      XML.AppendChild(header, "SystemTime", str);
      XML.AppendChild(header, "ServerId", ServerId);
      XML.AppendChild(header, "TerminalId", TerminalId);
      XML.AppendChild(header, "ServerSessionId", ServerSessionId.ToString());
      XML.AppendChild(header, "TerminalSessionId", TerminalSessionId.ToString());
      XML.AppendChild(header, "MsgType", MsgType.ToString());
      XML.AppendChild(header, "SequenceId", SequenceId.ToString());
      XML.AppendChild(header, "MsgContentSize", MsgContentSize.ToString());
      XML.AppendChild(header, "ResponseCode", ResponseCode.ToString());
      XML.AppendChild(header, "ResponseCodeText", ResponseCodeText);

      old_header = root.SelectSingleNode("WCP_MsgHeader"); //WCPPlayerRequestMsgHeader
      if (old_header != null)
      {
        root.RemoveChild(old_header);
      }

      if (root.FirstChild != null)
      {
        root.InsertBefore(header, root.FirstChild);
      }
      else
      {
        root.AppendChild(header);
      }
    }

    /// <summary>
    /// Creates a header
    /// </summary>
    public WCPPlayerRequestMsgHeader()
    {
      Init();
    }

    #endregion // PUBLIC

    #region PRIVATE

    /// <summary>
    /// Initializes the header
    /// </summary>
    private void Init()
    {
      ProtocolVersion = PROTOCOL_VERSION;
      CompressionMethod = COMPRESSION_METHOD;
      SystemTime = System.DateTime.Now;
      ServerId = "";
      TerminalId = "";
      ServerSessionId = 0;
      TerminalSessionId = 0;
      MsgType = WCPPlayerRequestMsgTypes.WCP_MsgUnknown;
      SequenceId = 0;
      MsgContentSize = 0;
      ResponseCode = WCPPlayerRequestResponseCodes.WCP_RC_OK;
      ResponseCodeText = "";
    }


    #endregion // PRIVATE
  } // WCPPlayerRequestMsgHeader

  public class WCPPlayerRequestException : Exception
  {
    WCPPlayerRequestResponseCodes response_code;
    String response_code_text;

    public WCPPlayerRequestResponseCodes ResponseCode
    {
      get
      {
        return response_code;
      }
    }
    public String ResponseCodeText
    {
      get
      {
        return response_code_text;
      }
    }

    public WCPPlayerRequestException(WCPPlayerRequestResponseCodes Value)
    {
      response_code = Value;
    }

    public WCPPlayerRequestException(WCPPlayerRequestResponseCodes Code, String Text)
      : base(Text)
    {
      response_code = Code;
      response_code_text = Text;
    }

    public WCPPlayerRequestException(WCPPlayerRequestResponseCodes Code, String Text, Exception InnerException)
      : base(Text, InnerException)
    {
      response_code = Code;
      response_code_text = Text;
    }
  } // WCPPlayerRequestException

  public class WCPPlayerRequestMsgReportPlay : IPlayerRequestXml
  {
    public Int64 TransactionId;
    public Int64 CardSessionId;
    public Int64 SessionBalanceBeforePlay;
    public Int64 SessionBalanceAfterPlay;
    public String GameId;
    public int Denomination;
    public Int64 PlayedAmount;
    public Int64 WonAmount;
    public double PayoutPercentage;

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgReportPlay", "");
      xml.AppendChild(node);

      XML.AppendChild(node, "TransactionId", TransactionId.ToString());
      XML.AppendChild(node, "CardSessionId", CardSessionId.ToString());
      XML.AppendChild(node, "SessionBalanceBeforePlay", SessionBalanceBeforePlay.ToString());
      XML.AppendChild(node, "SessionBalanceAfterPlay", SessionBalanceAfterPlay.ToString());
      XML.AppendChild(node, "GameId", GameId);
      XML.AppendChild(node, "Denomination", Denomination.ToString());
      XML.AppendChild(node, "PlayedAmount", PlayedAmount.ToString());
      XML.AppendChild(node, "WonAmount", WonAmount.ToString());


      XML.AppendChild(node, "PayoutPercentage", PayoutPercentage.ToString());

      // AJQ - 04-DES-2014, New format without decimal.

      //// New Format
      //int _x10000;
      //_x10000 = (int)(PayoutPercentage * 100);
      //_x10000 = Math.Max(0, _x10000);
      //_x10000 = Math.Min(10000, _x10000);
      //XML.AppendChild(node, "PayoutPercentage", _x10000.ToString("0000"));

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgReportPlay")
      {
        throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgReportPlay not found in content area");
      }
      else
      {
        TransactionId = Int64.Parse(XML.GetValue(reader, "TransactionId"));
        CardSessionId = Int64.Parse(XML.GetValue(reader, "CardSessionId"));
        SessionBalanceBeforePlay = Int64.Parse(XML.GetValue(reader, "SessionBalanceBeforePlay"));
        SessionBalanceAfterPlay = Int64.Parse(XML.GetValue(reader, "SessionBalanceAfterPlay"));
        GameId = XML.GetValue(reader, "GameId");
        Denomination = Int32.Parse(XML.GetValue(reader, "Denomination"));
        PlayedAmount = Int64.Parse(XML.GetValue(reader, "PlayedAmount"));
        WonAmount = Int64.Parse(XML.GetValue(reader, "WonAmount"));


        String _payout;
        Boolean _ok;

        PayoutPercentage = 0;
        _payout = XML.GetValue(reader, "PayoutPercentage");
        if (!String.IsNullOrEmpty(_payout))
        {
          if (_payout.Length == 4 && _payout.IndexOf('.') < 0)
          {
            // New format
            if (double.TryParse(_payout, out PayoutPercentage))
            {
              PayoutPercentage /= 100;
            }
            else
            {
              PayoutPercentage = 0;
            }
          }
          else
          {
            // Old format
            _ok = double.TryParse(_payout, out PayoutPercentage);
            if (PayoutPercentage > 100 || PayoutPercentage < 0)
            {
              _ok = false;
              PayoutPercentage = 0;
            }

            if (!_ok)
            {
              if (_payout.IndexOf('.') > 0)
              {
                _payout = _payout + "0000"; // Ensure at least 4 decimals 
                _payout = _payout.Substring(0, _payout.IndexOf('.') + 3);
                _payout = _payout.Replace(".", "");
                _ok = double.TryParse(_payout, out PayoutPercentage);
                if (_ok)
                {
                  PayoutPercentage = PayoutPercentage / 100;
                }
              }
            }

            PayoutPercentage = Math.Max(0, PayoutPercentage);
            PayoutPercentage = Math.Min(100, PayoutPercentage);
          }
        }
      }
    }
  } // WCPPlayerRequestMsgReportPlay

  public class WCPPlayerRequestMsgReportPlayReply : IPlayerRequestXml
  {
    public Int64 SessionBalanceAfterPlay;

    public String ToXml()
    {
      return "<WCP_MsgReportPlayReply><SessionBalanceAfterPlay>" + SessionBalanceAfterPlay.ToString() + "</SessionBalanceAfterPlay></WCP_MsgReportPlayReply>";

      ////XmlDocument xml;
      ////XmlNode node;

      ////xml = new XmlDocument();

      ////node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgReportPlayReply", "");
      ////xml.AppendChild(node);

      ////XML.AppendChild(node, "SessionBalanceAfterPlay", SessionBalanceAfterPlay.ToString());

      ////return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgReportPlayReply")
      {
        throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgReportPlayReply not found in content area");
      }
      else
      {
        SessionBalanceAfterPlay = Int64.Parse(XML.GetValue(reader, "SessionBalanceAfterPlay"));
      }
    }
  } // WCPPlayerRequestMsgReportPlayReply

  public class WCPPlayerRequestTITO_MsgCreateTicket : IPlayerRequestXml
  {
    public Int64 TransactionId;
    public Int32 ValidationType;
    public DateTime Date;
    public Int64 ValidationNumberSequence;
    public Int32 MachineTicketNumber;
    public Int64 AmountCents;
    public Int32 SystemId;
    public DateTime ExpirationDate;
    public Int64 PoolId;
    public Int64 PlaySessionId;

    // Helper variables not in original VO
    public String TerminalID;
    public Int32 TerminalType;
    public Int64 CreatedAccountID;
    public Int32 TicketType;

    // date will limit MAX ticket expiration date
    protected static DateTime MIN_VALID_DATE_EXPIRATION = new DateTime(2010, 1, 1);

    public WCPPlayerRequestTITO_MsgCreateTicket()
    {

    }

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();
      xml.LoadXml("<WCP_TITO_MsgCreateTicket></WCP_TITO_MsgCreateTicket>");

      node = xml.SelectSingleNode("WCP_TITO_MsgCreateTicket");

      XML.AppendChild(node, "TransactionId", TransactionId.ToString());
      XML.AppendChild(node, "ValidationType", ValidationType.ToString());
      XML.AppendChild(node, "Date", XML.XmlDateTimeString(Date));
      XML.AppendChild(node, "ValidationNumberSequence", ValidationNumberSequence.ToString());
      XML.AppendChild(node, "MachineTicketNumber", MachineTicketNumber.ToString());
      XML.AppendChild(node, "AmountCents", AmountCents.ToString());
      XML.AppendChild(node, "SystemId", SystemId.ToString());
      XML.AppendChild(node, "ExpirationDate", XML.XmlDateString(ExpirationDate));
      XML.AppendChild(node, "PoolId", PoolId.ToString());
      XML.AppendChild(node, "TerminalType", TerminalType.ToString());
      XML.AppendChild(node, "TicketType", TicketType.ToString());
      XML.AppendChild(node, "PlaySessionId", PlaySessionId.ToString());

      return xml.OuterXml;
    }

    public void SetDatetime(Int64 FileTime)
    {
      Date = DateTime.FromFileTime(FileTime);
    }

    public void SetExpirationDatetime(Int64 FileTime)
    {
      if (FileTime > 0)
      {
        ExpirationDate = DateTime.FromFileTime(FileTime);
      }
      else
      {
        ExpirationDate = DateTime.MinValue;
      }
    }

    public void LoadXml(XmlReader Reader)
    {
      if (Reader.Name != "WCP_TITO_MsgCreateTicket")
      {
        throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_TITO_MsgCreateTicket not found in content area");
      }
      else
      {
        TransactionId = XML.GetAsInt64(Reader, "TransactionId");
        ValidationType = Int32.Parse(XML.GetValue(Reader, "ValidationType"));
        Date = XML.GetAsDateTime(Reader, "Date");
        ValidationNumberSequence = Int64.Parse(XML.GetValue(Reader, "ValidationNumberSequence"));
        MachineTicketNumber = Int32.Parse(XML.GetValue(Reader, "MachineTicketNumber"));
        AmountCents = Int64.Parse(XML.GetValue(Reader, "AmountCents"));
        SystemId = Int32.Parse(XML.GetValue(Reader, "SystemId"));
        ExpirationDate = XML.GetAsDate(Reader, "ExpirationDate");
        if (ExpirationDate < MIN_VALID_DATE_EXPIRATION)
        {
          ExpirationDate = DateTime.MinValue;
        }
        PoolId = Int64.Parse(XML.GetValue(Reader, "PoolId"));
        TerminalType = Int32.Parse(XML.GetValue(Reader, "TerminalType"));
        TicketType = Int32.Parse(XML.GetValue(Reader, "TicketType"));
        PlaySessionId = XML.GetAsInt64(Reader, "PlaySessionId");

        Reader.Close();
      }
    }
  } // WCPPlayerRequestTITO_MsgCreateTicket

  public class WCPPlayerRequestTITO_MsgCreateTicketReply : IPlayerRequestXml
  {
    public WCPPlayerRequestTITO_MsgCreateTicketReply()
    {

    }

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("<WCP_TITO_MsgCreateTicketReply>");
      _sb.AppendLine("</WCP_TITO_MsgCreateTicketReply>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_TITO_MsgCreateTicketReply")
      {
        throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_TITO_MsgCreateTicketReply not found in content area");
      }
    }
  } // WCPPlayerRequestTITO_MsgCreateTicketReply

  public class WCPPlayerRequestMsgMoneyEvent : IPlayerRequestXml
  {
    public Int32 LktMoneyEventId;
    public DateTime LocalDatetime;
    public ENUM_MONEY_SOURCE Source;
    public ENUM_MONEY_TARGET Target;
    public Int64 Amount;

    public ENUM_ACCEPTED_MONEY_STATUS Status;
    public Int64 TransactionId;
    public Int64 CardSessionId;
    public Int64 StackerId;

    private ArrayList Track;


    public void SetLocalDatetime(Int64 FileTime)
    {
      LocalDatetime = DateTime.FromFileTime(FileTime);
    }

    public WCPPlayerRequestMsgMoneyEvent()
    {
      Track = new ArrayList();
      LocalDatetime = DateTime.Now;
    } // WCP_MsgMoneyEvent

    public void SetTrackData(int TrackIndex, String TrackData)
    {
      while (Track.Count <= TrackIndex)
      {
        Track.Add("");
      }
      Track[TrackIndex] = TrackData;
    } // SetTrackData

    public String GetTrackData(int TrackIndex)
    {
      if (Track.Count <= TrackIndex)
      {
        return "";
      }

      return Track[TrackIndex].ToString();
    } // GetTrackData

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;
      XmlNode _card_node;
      XmlNode _track;
      int _track_idx;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgMoneyEvent", "");
      xml.AppendChild(node);

      XML.AppendChild(node, "LktMoneyEventId", LktMoneyEventId.ToString());
      XML.AppendChild(node, "LocalDatetime", XML.XmlDateTimeString(LocalDatetime));
      XML.AppendChild(node, "Source", Source.ToString());
      XML.AppendChild(node, "Target", Target.ToString());
      XML.AppendChild(node, "Amount", Amount.ToString());
      XML.AppendChild(node, "Status", Status.ToString());
      XML.AppendChild(node, "TransactionId", TransactionId.ToString());
      XML.AppendChild(node, "CardSessionId", CardSessionId.ToString());
      XML.AppendChild(node, "StackerId", StackerId.ToString());

      // Create Card node and append.
      _card_node = xml.CreateNode(XmlNodeType.Element, "Card", "");
      node.AppendChild(_card_node);

      // Load card trackdata tracks on node
      for (_track_idx = 0; _track_idx < Track.Count; _track_idx++)
      {
        _track = xml.CreateNode(XmlNodeType.Element, "Track", "");
        _card_node.AppendChild(_track);

        XML.AppendChild(_track, "TrackIndex", _track_idx.ToString());
        XML.AppendChild(_track, "TrackData", Track[_track_idx].ToString());
      }

      return xml.OuterXml;
    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      int _track_index;
      String _track_data;
      bool _end_card;
      String time;
      String _str;

      if (reader.Name != "WCP_MsgMoneyEvent")
      {
        throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMoneyEvent not found in content area");
      }


      LktMoneyEventId = Int32.Parse(XML.GetValue(reader, "LktMoneyEventId"));

      time = XML.GetValue(reader, "LocalDatetime");
      DateTime.TryParse(time, out LocalDatetime);


      _str = XML.GetValue(reader, "Source");
      Source = (ENUM_MONEY_SOURCE)Enum.Parse(typeof(ENUM_MONEY_SOURCE), _str, true);

      _str = XML.GetValue(reader, "Target");
      Target = (ENUM_MONEY_TARGET)Enum.Parse(typeof(ENUM_MONEY_TARGET), _str, true);

      Amount = Int64.Parse(XML.GetValue(reader, "Amount"));

      _str = XML.GetValue(reader, "Status");
      Status = (ENUM_ACCEPTED_MONEY_STATUS)Enum.Parse(typeof(ENUM_ACCEPTED_MONEY_STATUS), _str, true);

      TransactionId = Int64.Parse(XML.GetValue(reader, "TransactionId"));
      CardSessionId = Int64.Parse(XML.GetValue(reader, "CardSessionId"));
      StackerId = Int64.Parse(XML.GetValue(reader, "StackerId"));


      _end_card = false;

      while (reader.Name != "Card")
      {
        if (!reader.Read())
        {
          throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMoneyEvent Card element missing");
        }
      }

      while (!_end_card)
      {
        _track_index = int.Parse(XML.GetValue(reader, "TrackIndex"));
        _track_data = XML.GetValue(reader, "TrackData");
        SetTrackData(_track_index, _track_data);

        //advance until </track> element 
        while (reader.Name != "Track")
        {
          if (!reader.Read())
          {
            throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMoneyEvent Track element missing");
          }
        }

        if ((reader.NodeType.Equals(XmlNodeType.EndElement)))
        {
          reader.Read();
          if ((reader.Name == "Card") && (reader.NodeType.Equals(XmlNodeType.EndElement)))
          {
            _end_card = true;
          } // if
        } // if
      }// while

    } // LoadXml
  } // WCPPlayerRequestMsgMoneyEvent

  public class WCPPlayerRequestMsgMoneyEventReply : IPlayerRequestXml
  {

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgMoneyEventReply", "");
      xml.AppendChild(node);

      return xml.OuterXml;
    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgMoneyEventReply")
      {
        throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMoneyEventReply not found in content area");
      }
    } // LoadXml

  } // WCPPlayerRequestMsgMoneyEventReply

  public class WCPPlayerRequestMsgEndSession : IPlayerRequestXml
  {
    public Int64 TransactionId = 0;
    public Int64 CardSessionId;
    public Int64 CardBalance;

    public Int64 PlayedCount = 0;
    public Int64 PlayedCents = 0;
    public Int64 WonCount = 0;
    public Int64 WonCents = 0;
    public Int64 JackpotCents = 0;

    public Int64 PlayedReedemable = 0;
    public Int64 PlayedNonRestrictedAmount = 0;
    public Int64 PlayedRestrictedAmount = 0;

    public Int64 HandpaysAmountCents = 0;

    public Boolean HasCounters = false;
    public MultiPromos.AccountBalance Balance = MultiPromos.AccountBalance.Zero;

    public Boolean OnlyAddBalances = false;

    public MultiPromos.EndSessionInput EndSessionInput()
    {
      MultiPromos.EndSessionInput _input;

      _input = new MultiPromos.EndSessionInput();

      _input.TransactionId = this.TransactionId;
      _input.PlaySessionId = this.CardSessionId;
      _input.BalanceFromGM = ((Decimal)CardBalance) / 100m;

      _input.HasMeters = this.HasCounters;
      if (this.HasCounters)
      {
        _input.Meters.PlayedCount = this.PlayedCount;
        _input.Meters.PlayedAmount = ((Decimal)this.PlayedCents) / 100m;
        _input.Meters.WonCount = this.WonCount;
        _input.Meters.WonAmount = ((Decimal)this.WonCents) / 100m;
        _input.Meters.JackpotAmount = ((Decimal)this.JackpotCents) / 100m;
        _input.Meters.PlayedReedemable = ((Decimal)this.PlayedReedemable) / 100.00m;
        _input.Meters.PlayedNonRestrictedAmount = ((Decimal)this.PlayedNonRestrictedAmount) / 100.00m;
        _input.Meters.PlayedRestrictedAmount = ((Decimal)this.PlayedRestrictedAmount) / 100.00m;


        _input.Meters.HandpaysAmount = ((Decimal)this.HandpaysAmountCents) / 100m;
      }

      return _input;
    } // EndSessionInput


    #region IXml Members

    //------------------------------------------------------------------------------
    // PURPOSE : Convert the properties of this class to a XML String representation.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgEndSession>");

      // RCI 09-AUG-2010: Added optional field TransactionId.
      if (TransactionId > 0)
      {
        _sb.AppendLine("<TransactionId>" + TransactionId.ToString() + "</TransactionId>");
      }

      _sb.AppendLine("<CardSessionId>" + CardSessionId.ToString() + "</CardSessionId>");
      _sb.AppendLine("<CardBalance>" + CardBalance.ToString() + "</CardBalance>");

      // ACC 01-AUG-2012 Added Card Balance in parts (Redeemable, PromoRedeemable and PromoNotRedeemable)
      _sb.AppendLine("<Balance>");
      _sb.AppendLine(Balance.ToXml());
      _sb.AppendLine("</Balance>");

      _sb.AppendLine("<OnlyAddBalances>" + (OnlyAddBalances ? "1" : "0") + "</OnlyAddBalances>");

      // RCI 10-AUG-2010: Added optional fields for counters.
      if (HasCounters)
      {
        _sb.AppendLine("<PlayedCount>" + PlayedCount.ToString() + "</PlayedCount>");
        _sb.AppendLine("<PlayedCents>" + PlayedCents.ToString() + "</PlayedCents>");
        _sb.AppendLine("<WonCount>" + WonCount.ToString() + "</WonCount>");
        _sb.AppendLine("<WonCents>" + WonCents.ToString() + "</WonCents>");
        _sb.AppendLine("<JackpotCents>" + JackpotCents.ToString() + "</JackpotCents>");
        _sb.AppendLine("<HandpaysAmountCents>" + HandpaysAmountCents.ToString() + "</HandpaysAmountCents>");
      }

      _sb.AppendLine("</WCP_MsgEndSession>");

      return _sb.ToString();

    } // ToXml

    //------------------------------------------------------------------------------
    // PURPOSE : Parse a XML Document and save values into the properties of this class.
    //
    //  PARAMS :
    //      - INPUT :
    //          - XmlReader Reader
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(XmlReader Reader)
    {
      Int64 _transaction_id;
      Int64 _played_count;
      Int64 _played_cents;
      Int64 _won_count;
      Int64 _won_cents;
      Int64 _jackpot_cents;
      Int32 _only_add_balances;
      Int64 _handpays_amount_cents;

      if (Reader.Name != "WCP_MsgEndSession")
      {
        throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgEndSession not found in content area");
      }
      else
      {
        // RCI 09-AUG-2010: Added optional field TransactionId.
        if (!Reader.Read())
        {
          throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgEndSession CardSessionId element missing");
        }

        //
        // AJQ ACC 19-SEP-2012 The message created with the StringBuilder contains an empty item, skip it!
        //
        if (Reader.Name == "")
        {
          if (!Reader.Read())
          {
            throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgEndSession CardSessionId element missing");
          }
        }

        if (Reader.Name == "TransactionId" && Reader.NodeType.Equals(XmlNodeType.Element))
        {
          if (!Int64.TryParse(XML.GetValue(Reader, "TransactionId"), out _transaction_id))
          {
            throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgEndSession TransactionId: Can't parse value");
          }
          TransactionId = _transaction_id;
        }

        CardSessionId = Int64.Parse(XML.GetValue(Reader, "CardSessionId"));
        CardBalance = Int64.Parse(XML.GetValue(Reader, "CardBalance"));

        // ACC 01-AUG-2012 Added Card Balance in parts (Redeemable, PromoRedeemable and PromoNotRedeemable)
        while (Reader.Name != "Balance" && Reader.Name != "PlayedCount" && Reader.Name != "WCP_MsgEndSession")
        {
          if (!Reader.Read())
          {
            throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgEndSession Balance or PlayedCount element missing");
          }
        }

        if (Reader.Name == "Balance")
        {
          Balance.LoadXml(Reader);
        }

        Int32.TryParse(XML.ReadTagValue(Reader, "OnlyAddBalances", true), out _only_add_balances);
        OnlyAddBalances = _only_add_balances > 0 ? true : false;

        // RCI 10-AUG-2010: Added optional fields for counters.
        HasCounters = false;
        PlayedCount = 0;
        PlayedCents = 0;
        WonCount = 0;
        WonCents = 0;
        JackpotCents = 0;
        HandpaysAmountCents = 0;

        _handpays_amount_cents = 0;

        if (!Int64.TryParse(XML.GetValue(Reader, "PlayedCount", true), out _played_count))
        {
          return;
        }
        if (!Int64.TryParse(XML.GetValue(Reader, "PlayedCents", true), out _played_cents))
        {
          return;
        }
        if (!Int64.TryParse(XML.GetValue(Reader, "WonCount", true), out _won_count))
        {
          return;
        }
        if (!Int64.TryParse(XML.GetValue(Reader, "WonCents", true), out _won_cents))
        {
          return;
        }
        if (!Int64.TryParse(XML.GetValue(Reader, "JackpotCents", true), out _jackpot_cents))
        {
          return;
        }

        //Read HandpaysAmountCents.  If terminal has old version, there is no a HandpaysAmountCents tag
        while (Reader.Name != "HandpaysAmountCents" && !Reader.NodeType.Equals(XmlNodeType.Element) && !Reader.EOF)
        {
          Reader.Read();
        }

        if (Reader.Name == "HandpaysAmountCents" && Reader.NodeType.Equals(XmlNodeType.Element))
        {
          if (!Int64.TryParse(XML.GetValue(Reader, "HandpaysAmountCents", true), out _handpays_amount_cents))
          {
            return;
          }
        }

        HasCounters = true;
        PlayedCount = _played_count;
        PlayedCents = _played_cents;
        WonCount = _won_count;
        WonCents = _won_cents;
        JackpotCents = _jackpot_cents;
        HandpaysAmountCents = _handpays_amount_cents;
      }
    } // LoadXml

    #endregion // IXml Members

  } // WCPPlayerRequestMsgEndSession

  public class WCPPlayerRequestMsgEndSessionReply : IPlayerRequestXml
  {
    public Int64 CardBalance;
    public String HolderName;
    public Int32 Points;

    public MultiPromos.AccountBalance Balance = MultiPromos.AccountBalance.Zero;

    #region IXml Members

    //------------------------------------------------------------------------------
    // PURPOSE : Convert the properties of this class to a XML String representation.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgEndSessionReply>");
      _sb.AppendLine("<CardBalance>" + CardBalance.ToString() + "</CardBalance>");
      _sb.AppendLine("<HolderName>" + XML.StringToXmlValue(HolderName) + "</HolderName>");
      _sb.AppendLine("<Points>" + Points.ToString() + "</Points>");

      // ACC 01-AUG-2012 Added Card Balance in parts (Redeemable, PromoRedeemable and PromoNotRedeemable)
      _sb.AppendLine("<Balance>");
      _sb.AppendLine(Balance.ToXml());
      _sb.AppendLine("</Balance>");

      _sb.AppendLine("</WCP_MsgEndSessionReply>");

      return _sb.ToString();

    } // ToXml

    //------------------------------------------------------------------------------
    // PURPOSE : Parse a XML Document and save values into the properties of this class.
    //
    //  PARAMS :
    //      - INPUT :
    //          - XmlReader Reader
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void LoadXml(XmlReader Reader)
    {
      if (Reader.Name != "WCP_MsgEndSessionReply")
      {
        throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgEndSessionReply not found in content area");
      }
      else
      {
        CardBalance = Int64.Parse(XML.GetValue(Reader, "CardBalance"));

        // RCI 05-NOV-2010: Added optional fields for holder name and points balance.
        HolderName = XML.GetValue(Reader, "HolderName", true);
        Int32.TryParse(XML.GetValue(Reader, "Points", true), out Points);

        // ACC 01-AUG-2012 Added Card Balance in parts (Redeemable, PromoRedeemable and PromoNotRedeemable)
        Balance.LoadXml(Reader);
      }
    } // LoadXml

    #endregion // IXml Members

  } // WCPPlayerRequestMsgEndSessionReply

  public class WCPPlayerRequestMsgCancelStartSession : IPlayerRequestXml
  {
    public Int64 TransactionId = 0;
    ArrayList Track;
    MultiPromos.AccountBalance CancelledBalance = MultiPromos.AccountBalance.Zero;

    public WCPPlayerRequestMsgCancelStartSession()
    {
      Track = new ArrayList();
    }

    public void SetTrackData(int TrackIndex, String TrackData)
    {
      while (Track.Count <= TrackIndex)
      {
        Track.Add("");
      }
      Track[TrackIndex] = TrackData;
    }

    public String GetTrackData(int TrackIndex)
    {
      if (Track.Count <= TrackIndex)
      {
        return "";
      }

      return Track[TrackIndex].ToString();
    }

    public MultiPromos.AccountBalance GetCancelledBalance()
    {
      return CancelledBalance;
    }

    public void SetCancelledBalance(MultiPromos.AccountBalance ValueCancelledBalance)
    {
      CancelledBalance = ValueCancelledBalance;
    }

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;
      XmlNode parent_node;
      XmlNode track;

      xml = new XmlDocument();
      xml.LoadXml("<WCP_MsgCancelStartSession><Card></Card></WCP_MsgCancelStartSession>");

      node = xml.SelectSingleNode("WCP_MsgCancelStartSession/Card");

      if (TransactionId > 0)
      {
        parent_node = xml.SelectSingleNode("WCP_MsgCancelStartSession");
        XML.InsertBefore(parent_node, node, "TransactionId", TransactionId.ToString());
      }

      for (int track_idx = 0; track_idx < Track.Count; track_idx++)
      {
        track = xml.CreateNode(XmlNodeType.Element, "Track", "");

        node.AppendChild(track);

        XML.AppendChild(track, "TrackIndex", track_idx.ToString());
        XML.AppendChild(track, "TrackData", Track[track_idx].ToString());
      }
      node = xml.SelectSingleNode("WCP_MsgCancelStartSession");

      parent_node = XML.AppendChild(node, "CancelledBalance", "");
      XML.AppendChild(parent_node, "Redeemable", ((Int64)(CancelledBalance.Redeemable * 100)).ToString());
      XML.AppendChild(parent_node, "PromoRedeemable", ((Int64)(CancelledBalance.PromoRedeemable * 100)).ToString());
      XML.AppendChild(parent_node, "PromoNotRedeemable", ((Int64)(CancelledBalance.PromoNotRedeemable * 100)).ToString());

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader Xmlreader)
    {
      bool endCard;
      Int64 _transaction_id;

      if (Xmlreader.Name == "WCP_MsgCancelStartSession")
      {
        // RCI 09-AUG-2010: Added optional field TransactionId.
        if (!Xmlreader.Read())
        {
          throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCancelStartSession Card element missing");
        }
        if (Xmlreader.Name == "TransactionId" && Xmlreader.NodeType.Equals(XmlNodeType.Element))
        {
          if (!Int64.TryParse(XML.GetValue(Xmlreader, "TransactionId"), out _transaction_id))
          {
            throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCancelStartSession TransactionId: Can't parse value");
          }
          TransactionId = _transaction_id;
        }

        endCard = false;
        while (Xmlreader.Name != "Card" || (!Xmlreader.NodeType.Equals(XmlNodeType.Element)))
        {
          if (!Xmlreader.Read())
          {
            throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCancelStartSession Card element missing");
          }
        }
        //Now, the xmlreader is at the start of the Card tag.
        //and it must advance until the end Card tag.
        //the new GetValue method moves the XmlReader until the end of the searched tag
        // so:
        while (!endCard)
        {
          if (!Xmlreader.Read())
          {
            throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCancelStartSession Track element missing");
          }
          else
          {
            int track_index;
            String track_data;

            track_index = int.Parse(XML.GetValue(Xmlreader, "TrackIndex"));
            track_data = XML.GetValue(Xmlreader, "TrackData");
            SetTrackData(track_index, track_data);
            //advance until </track_data> element
            Xmlreader.Read();
            //advance until </track> element 
            Xmlreader.Read();
            if ((Xmlreader.Name == "Track") && (Xmlreader.NodeType.Equals(XmlNodeType.EndElement)))
            {
              Xmlreader.Read();
              if ((Xmlreader.Name == "Card") && (Xmlreader.NodeType.Equals(XmlNodeType.EndElement)))
              {
                endCard = true;
              }
            }
          }
        }

        CancelledBalance.LoadXml(Xmlreader);
      }
      else
      {
        throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCancelStartSession not found in content area");
      }
    }
  } // WCPPlayerRequestMsgCancelStartSession

  public class WCPPlayerRequestMsgCancelStartSessionReply : IPlayerRequestXml
  {
    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgCancelStartSessionReply", "");
      xml.AppendChild(node);

      return xml.OuterXml;
    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgCancelStartSessionReply")
      {
        throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgCancelStartSessionReply not found in content area");
      }
    } // LoadXml
  } // WCPPlayerRequestMsgCancelStartSessionReply

  public class WCPPlayerRequestMsgStartCardSession : IPlayerRequestXml
  {
    // RCI 09-AUG-2010: Value 0 indicates TransactionId is NOT present
    Int64 TransactionId = 0;
    ArrayList Track;
    public String Pin = "";
    Int32 TransferMode = 0;
    MultiPromos.AccountBalance AccountBalanceToTranfer = MultiPromos.AccountBalance.Zero;
    Boolean TransferWithBuckets = false;

    public WCPPlayerRequestMsgStartCardSession()
    {
      Track = new ArrayList();
    }

    public void SetTransactionId(Int64 Value)
    {
      TransactionId = Value;
    }

    public void SetPin(String Value)
    {
      Pin = Value;
    }

    public void SetTrackData(int TrackIndex, String TrackData)
    {
      while (Track.Count <= TrackIndex)
      {
        Track.Add("");
      }
      Track[TrackIndex] = TrackData;
    }

    public Int64 GetTransactionId()
    {
      return TransactionId;
    }

    public String GetTrackData(int TrackIndex)
    {
      if (Track.Count <= TrackIndex)
      {
        return "";
      }

      return Track[TrackIndex].ToString();
    }

    public Int32 GetTranferMode()
    {
      return TransferMode;
    }

    public void SetTranferMode(Int32 Value)
    {
      TransferMode = Value;
    }

    public MultiPromos.AccountBalance GetBalanceToTransfer()
    {
      return AccountBalanceToTranfer;
    }

    public void SetBalanceToTransfer(MultiPromos.AccountBalance ValueAccountBalanceToTranfer)
    {
      AccountBalanceToTranfer = ValueAccountBalanceToTranfer;
    }

    public Boolean IsTransferWithBuckets()
    {
      return TransferWithBuckets;
    }

    public void SetTransferWithBuckets(Boolean ValueTransferWithBuckets)
    {
      TransferWithBuckets = ValueTransferWithBuckets;
    }

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;
      XmlNode parent_node;
      XmlNode track;

      xml = new XmlDocument();
      xml.LoadXml("<WCP_MsgStartCardSession><Card></Card></WCP_MsgStartCardSession>");

      node = xml.SelectSingleNode("WCP_MsgStartCardSession/Card");

      // RCI 09-AUG-2010: Added optional field TransactionId.
      if (TransactionId > 0)
      {
        parent_node = xml.SelectSingleNode("WCP_MsgStartCardSession");
        XML.InsertBefore(parent_node, node, "TransactionId", TransactionId.ToString());
      }

      for (int track_idx = 0; track_idx < Track.Count; track_idx++)
      {
        track = xml.CreateNode(XmlNodeType.Element, "Track", "");

        node.AppendChild(track);

        XML.AppendChild(track, "TrackIndex", track_idx.ToString());
        XML.AppendChild(track, "TrackData", Track[track_idx].ToString());
      }
      node = xml.SelectSingleNode("WCP_MsgStartCardSession");
      XML.AppendChild(node, "Pin", Pin);

      XML.AppendChild(node, "TransferMode", TransferMode.ToString());
      if (TransferMode != 0)
      {
        parent_node = XML.AppendChild(node, "BalanceToTransfer", "");
        XML.AppendChild(parent_node, "Redeemable", ((Int64)(AccountBalanceToTranfer.Redeemable * 100)).ToString());
        XML.AppendChild(parent_node, "PromoRedeemable", ((Int64)(AccountBalanceToTranfer.PromoRedeemable * 100)).ToString());
        XML.AppendChild(parent_node, "PromoNotRedeemable", ((Int64)(AccountBalanceToTranfer.PromoNotRedeemable * 100)).ToString());
      }

      XML.AppendChild(node, "TransferWithBuckets", TransferWithBuckets.ToString());

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader Xmlreader)
    {
      bool endCard;
      Int64 _transaction_id;
      Int32 _aux_value;

      if (Xmlreader.Name == "WCP_MsgStartCardSession")
      {
        // RCI 09-AUG-2010: Added optional field TransactionId.
        if (!Xmlreader.Read())
        {
          throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgStartCardSession Card element missing");
        }
        if (Xmlreader.Name == "TransactionId" && Xmlreader.NodeType.Equals(XmlNodeType.Element))
        {
          if (!Int64.TryParse(XML.GetValue(Xmlreader, "TransactionId"), out _transaction_id))
          {
            throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgStartCardSession TransactionId: Can't parse value");
          }
          SetTransactionId(_transaction_id);
        }

        endCard = false;
        while (Xmlreader.Name != "Card" || (!Xmlreader.NodeType.Equals(XmlNodeType.Element)))
        {
          if (!Xmlreader.Read())
          {
            throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgStartCardSession Card element missing");
          }
        }
        //Now, the xmlreader is at the start of the Card tag.
        //and it must advance until the end Card tag.
        //the new GetValue method moves the XmlReader until the end of the searched tag
        // so:
        while (!endCard)
        {
          if (!Xmlreader.Read())
          {
            throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgStartCardSession Track element missing");
          }
          else
          {
            int track_index;
            String track_data;

            track_index = int.Parse(XML.GetValue(Xmlreader, "TrackIndex"));
            track_data = XML.GetValue(Xmlreader, "TrackData");
            SetTrackData(track_index, track_data);
            //advance until </track_data> element
            Xmlreader.Read();
            //advance until </track> element 
            Xmlreader.Read();
            if ((Xmlreader.Name == "Track") && (Xmlreader.NodeType.Equals(XmlNodeType.EndElement)))
            {
              Xmlreader.Read();
              if ((Xmlreader.Name == "Card") && (Xmlreader.NodeType.Equals(XmlNodeType.EndElement)))
              {
                endCard = true;
              }
            }
          }
        }

        Pin = XML.GetValue(Xmlreader, "Pin", true);

        if (Int32.TryParse(XML.GetValue(Xmlreader, "TransferMode", true), out _aux_value))
        {
          TransferMode = _aux_value;
        }

        if (TransferMode != 0)
        {
          AccountBalanceToTranfer.LoadXml(Xmlreader);
        }

        if (!Boolean.TryParse(XML.GetValue(Xmlreader, "TransferWithBuckets", true), out TransferWithBuckets))
        {
          TransferWithBuckets = false;
        }
      }
      else
      {
        throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgStartCardSession not found in content area");
      }
    }
  } // WCPPlayerRequestMsgStartCardSession

  public class WCPPlayerRequestMsgStartCardSessionReply : IPlayerRequestXml
  {
    public Int64 CardSessionId;
    public Int64 CardBalance;
    public String HolderName;
    public Int32 Points;
    public Boolean AllowCashIn = false;
    public MultiPromos.AccountBalance Balance = MultiPromos.AccountBalance.Zero;

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgStartCardSessionReply>");
      _sb.AppendLine("<CardSessionId>" + CardSessionId.ToString() + "</CardSessionId>");
      _sb.AppendLine("<CardBalance>" + CardBalance.ToString() + "</CardBalance>");
      _sb.AppendLine("<HolderName>" + XML.StringToXmlValue(HolderName) + "</HolderName>");
      // RCI 05-NOV-2010: Added optional field for points balance.
      _sb.AppendLine("<Points>" + Points.ToString() + "</Points>");
      _sb.AppendLine("<AllowCashIn>" + AllowCashIn.ToString() + "</AllowCashIn>");
      // ACC 01-AUG-2012 Added Card Balance in parts (Redeemable, PromoRedeemable and PromoNotRedeemable)
      _sb.AppendLine("<Balance>");
      _sb.AppendLine(Balance.ToXml());
      _sb.AppendLine("</Balance>");
      _sb.AppendLine("</WCP_MsgStartCardSessionReply>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgStartCardSessionReply")
      {
        throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgStartCardSessionReply not found in content area");
      }
      else
      {
        CardSessionId = Int64.Parse(XML.GetValue(reader, "CardSessionId"));
        CardBalance = Int64.Parse(XML.GetValue(reader, "CardBalance"));
        HolderName = XML.GetValue(reader, "HolderName");

        // RCI 05-NOV-2010: Added optional field for points balance.
        Int32.TryParse(XML.GetValue(reader, "Points", true), out Points);
        if (!Boolean.TryParse(XML.GetValue(reader, "AllowCashIn", true), out AllowCashIn))
        {
          AllowCashIn = false;
        }

        // ACC 01-AUG-2012 Added Card Balance in parts (Redeemable, PromoRedeemable and PromoNotRedeemable)
        Balance.LoadXml(reader);
      }
    }
  } // WCPPlayerRequestMsgStartCardSessionReply

  public class WCPPlayerRequestMsgMobileBankCardTransfer : IPlayerRequestXml
  {
    public Int64 TransactionId;
    ArrayList MobileBankCard;
    public String Pin;
    ArrayList PlayerCard;
    public Int64 AmountToTransferx100;

    public WCPPlayerRequestMsgMobileBankCardTransfer()
    {
      MobileBankCard = new ArrayList();
      PlayerCard = new ArrayList();
    }

    public void PlayerSetTrackData(int TrackIndex, String TrackData)
    {
      while (PlayerCard.Count <= TrackIndex)
      {
        PlayerCard.Add("");
      }
      PlayerCard[TrackIndex] = TrackData;
    }

    public String PlayerGetTrackData(int TrackIndex)
    {
      if (PlayerCard.Count <= TrackIndex)
      {
        return "";
      }

      return PlayerCard[TrackIndex].ToString();
    }

    public void MobileBankSetTrackData(int TrackIndex, String TrackData)
    {
      while (MobileBankCard.Count <= TrackIndex)
      {
        MobileBankCard.Add("");
      }
      MobileBankCard[TrackIndex] = TrackData;
    }

    public String MobileBankGetTrackData(int TrackIndex)
    {
      if (MobileBankCard.Count <= TrackIndex)
      {
        return "";
      }

      return MobileBankCard[TrackIndex].ToString();
    }

    public void MBSetPin(String Value)
    {
      Pin = Value;
    }

    public void SetAmountToTransfer(Int64 Value)
    {
      AmountToTransferx100 = Value;
    }

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;
      XmlNode node_card;
      XmlNode track;

      xml = new XmlDocument();
      xml.LoadXml("<WCP_MsgMobileBankCardTransfer></WCP_MsgMobileBankCardTransfer>");

      // Mobile Bank Track Data
      node = xml.SelectSingleNode("WCP_MsgMobileBankCardTransfer");

      XML.AppendChild(node, "TransactionId", TransactionId.ToString());

      node_card = xml.CreateNode(XmlNodeType.Element, "MobileBankCard", "");
      for (int track_idx = 0; track_idx < MobileBankCard.Count; track_idx++)
      {
        track = xml.CreateNode(XmlNodeType.Element, "Track", "");

        node_card.AppendChild(track);

        XML.AppendChild(track, "TrackIndex", track_idx.ToString());
        XML.AppendChild(track, "TrackData", MobileBankCard[track_idx].ToString());
      }
      node.AppendChild(node_card);

      XML.AppendChild(node, "Pin", Pin);

      // Player Track Data
      node_card = xml.CreateNode(XmlNodeType.Element, "PlayerCard", "");
      for (int track_idx = 0; track_idx < PlayerCard.Count; track_idx++)
      {
        track = xml.CreateNode(XmlNodeType.Element, "Track", "");

        node_card.AppendChild(track);

        XML.AppendChild(track, "TrackIndex", track_idx.ToString());
        XML.AppendChild(track, "TrackData", PlayerCard[track_idx].ToString());
      }
      node.AppendChild(node_card);

      XML.AppendChild(node, "AmountToTransferx100", AmountToTransferx100.ToString());

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      Boolean end_card;

      if (reader.Name != "WCP_MsgMobileBankCardTransfer")
      {
        throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobileBankCardTransfer not found in content area");
      }
      else
      {
        //
        // Read TransactionId
        //
        TransactionId = Int64.Parse(XML.GetValue(reader, "TransactionId"));

        //
        // Read MobileBankCard
        //
        end_card = false;
        while (reader.Name != "MobileBankCard" && (reader.NodeType.Equals(XmlNodeType.Element)))
        {
          if (!reader.Read())
          {
            throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobileBankCardTransfer MobileBankCard element missing");
          }
        }
        //Now, the xmlreader is at the start of the MobileBankCard tag.
        //and it must advance until the end MobileBankCard tag.
        //the new GetValue method moves the XmlReader until the end of the searched tag
        // so:
        while (!end_card)
        {
          if (!reader.Read())
          {
            throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobileBankCardTransfer/MobileBankCard Track element missing");
          }
          else
          {
            int track_index;
            String track_data;

            track_index = int.Parse(XML.GetValue(reader, "TrackIndex"));
            track_data = XML.GetValue(reader, "TrackData");
            MobileBankSetTrackData(track_index, track_data);
            //advance until </TrackData> element
            reader.Read();
            //advance until </Track> element 
            reader.Read();
            if ((reader.Name == "Track") && (reader.NodeType.Equals(XmlNodeType.EndElement)))
            {
              reader.Read();
              if ((reader.Name == "MobileBankCard") && (reader.NodeType.Equals(XmlNodeType.EndElement)))
              {
                end_card = true;
              }
            }
          }
        }

        //
        // Read Pin
        //
        Pin = XML.GetValue(reader, "Pin");

        //
        // Read PlayerCard
        //
        end_card = false;
        while (reader.Name != "PlayerCard" && (reader.NodeType.Equals(XmlNodeType.Element)))
        {
          if (!reader.Read())
          {
            throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobileBankCardTransfer PlayerCard element missing");
          }
        }
        //Now, the xmlreader is at the start of the PlayerCard tag.
        //and it must advance until the end PlayerCard tag.
        //the new GetValue method moves the XmlReader until the end of the searched tag
        // so:
        while (!end_card)
        {
          if (!reader.Read())
          {
            throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobileBankCardTransfer/PlayerCard Track element missing");
          }
          else
          {
            int track_index;
            String track_data;

            track_index = int.Parse(XML.GetValue(reader, "TrackIndex"));
            track_data = XML.GetValue(reader, "TrackData");
            PlayerSetTrackData(track_index, track_data);
            //advance until </TrackData> element
            reader.Read();
            //advance until </Track> element 
            reader.Read();
            if ((reader.Name == "Track") && (reader.NodeType.Equals(XmlNodeType.EndElement)))
            {
              reader.Read();
              if ((reader.Name == "PlayerCard") && (reader.NodeType.Equals(XmlNodeType.EndElement)))
              {
                end_card = true;
              }
            }
          }
        }

        //
        // Read AmountToTransferx100
        //
        AmountToTransferx100 = Int64.Parse(XML.GetValue(reader, "AmountToTransferx100"));
      }
    }
  } // WCPPlayerRequestMsgMobileBankCardTransfer

  public class WCPPlayerRequestMsgMobileBankCardTransferReply : IPlayerRequestXml
  {
    public Int64 PlayerPreviousCardBalancex100;
    public Int64 PlayerCurrentCardBalancex100;

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgMobileBankCardTransferReply", "");
      xml.AppendChild(node);

      XML.AppendChild(node, "PlayerPreviousCardBalancex100", PlayerPreviousCardBalancex100.ToString());
      XML.AppendChild(node, "PlayerCurrentCardBalancex100", PlayerCurrentCardBalancex100.ToString());

      return xml.OuterXml;
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WCP_MsgMobileBankCardTransferReply")
      {
        throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgMobileBankCardTransferReply not found in content area");
      }
      else
      {
        PlayerPreviousCardBalancex100 = Int64.Parse(XML.GetValue(reader, "PlayerPreviousCardBalancex100"));
        PlayerCurrentCardBalancex100 = Int64.Parse(XML.GetValue(reader, "PlayerCurrentCardBalancex100"));
      }
    }
  } // WCPPlayerRequestMsgMobileBankCardTransferReply

  public class WCPPlayerRequestMsgAddCredit : IPlayerRequestXml
  {
    public Int64 TransactionId;
    public Int64 CardSessionId;
    public Int64 AmountToAdd;
    public String Currency;
    public DateTime AcceptedTime;
    public Int64 StackerId;

    ArrayList Track;

    public WCPPlayerRequestMsgAddCredit()
    {
      Track = new ArrayList();
      Currency = "";
    } // WCP_MsgAddCredit

    public void SetTrackData(int TrackIndex, String TrackData)
    {
      while (Track.Count <= TrackIndex)
      {
        Track.Add("");
      }
      Track[TrackIndex] = TrackData;
    } // SetTrackData

    public String GetTrackData(int TrackIndex)
    {
      if (Track.Count <= TrackIndex)
      {
        return "";
      }

      return Track[TrackIndex].ToString();
    } // GetTrackData

    public void SetAcceptedTime(Int64 FileTime)
    {
      AcceptedTime = DateTime.FromFileTime(FileTime);
    }

    public String ToXml()
    {
      XmlDocument xml;
      XmlNode node;
      XmlNode _card_node;
      XmlNode _track;
      int _track_idx;

      xml = new XmlDocument();

      node = xml.CreateNode(XmlNodeType.Element, "WCP_MsgAddCredit", "");
      xml.AppendChild(node);

      XML.AppendChild(node, "TransactionId", TransactionId.ToString());

      // MBF 26-JAN-2012 Trackdata information

      // Create Card node and append.
      _card_node = xml.CreateNode(XmlNodeType.Element, "Card", "");
      node.AppendChild(_card_node);

      // Load card trackdata tracks on node
      for (_track_idx = 0; _track_idx < Track.Count; _track_idx++)
      {
        _track = xml.CreateNode(XmlNodeType.Element, "Track", "");
        _card_node.AppendChild(_track);

        XML.AppendChild(_track, "TrackIndex", _track_idx.ToString());
        XML.AppendChild(_track, "TrackData", Track[_track_idx].ToString());
      }

      XML.AppendChild(node, "CardSessionId", CardSessionId.ToString());
      XML.AppendChild(node, "AmountToAdd", AmountToAdd.ToString());
      XML.AppendChild(node, "Currency", Currency);
      XML.AppendChild(node, "AcceptedTime", XML.XmlDateTimeString(AcceptedTime));
      XML.AppendChild(node, "StackerId", StackerId.ToString());

      return xml.OuterXml;
    } // ToXml

    public void LoadXml(XmlReader reader)
    {
      int _track_index;
      String _track_data;
      bool _end_card;
      String _time;

      if (reader.Name != "WCP_MsgAddCredit")
      {
        throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgAddCredit not found in content area");
      }
      else
      {
        TransactionId = Int64.Parse(XML.GetValue(reader, "TransactionId"));

        // MBF 26-JAN-2012 Trackdata information
        _end_card = false;

        while (reader.Name != "Card")
        {
          if (!reader.Read())
          {
            throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgAddCredit Card element missing");
          }
        }
        //Now, the xmlreader is at the start of the Card tag.
        //and it must advance until the end Card tag.
        //the new GetValue method moves the XmlReader until the end of the searched tag
        // so:
        while (!_end_card)
        {
          _track_index = int.Parse(XML.GetValue(reader, "TrackIndex"));
          _track_data = XML.GetValue(reader, "TrackData");
          SetTrackData(_track_index, _track_data);

          //advance until </track> element 
          while (reader.Name != "Track")
          {
            if (!reader.Read())
            {
              throw new WCPPlayerRequestException(WCPPlayerRequestResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgAddCredit Track element missing");
            }
          }

          if ((reader.NodeType.Equals(XmlNodeType.EndElement)))
          {
            reader.Read();
            if ((reader.Name == "Card") && (reader.NodeType.Equals(XmlNodeType.EndElement)))
            {
              _end_card = true;
            } // if
          } // if
        }// while

        CardSessionId = Int64.Parse(XML.GetValue(reader, "CardSessionId"));
        AmountToAdd = Int64.Parse(XML.GetValue(reader, "AmountToAdd"));
        Currency = XML.GetValue(reader, "Currency");

        _time = XML.GetValue(reader, "AcceptedTime");
        DateTime.TryParse(_time, out AcceptedTime);

        StackerId = Int64.Parse(XML.GetValue(reader, "StackerId"));

      } // else
    } // LoadXml
  }  // WCPPlayerRequestMsgAddCredit

  /*
  public class WCPPlayerRequestMsgAddCreditReply : IXml, WCP_WKT.IVouchers
  {
    public Int64 SessionBalanceAfterAdd;
    public Boolean AccountAlreadyHasPromo;
    public Boolean BillInGivesPromo;

    private ArrayList _voucherList;

    private String _deflated_voucher;
    private String _inflated_voucher;

    #region IVoucher Members

    public ArrayList VoucherList
    {
      get
      {
        return _voucherList;
      }
      set
      {
        this._voucherList = value;
      }
    }

    #endregion

    public String ToXml()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("<WCP_MsgAddCreditReply>");
      _sb.Append("<SessionBalanceAfterAdd>" + SessionBalanceAfterAdd.ToString() + "</SessionBalanceAfterAdd>");
      _sb.Append("<AccountAlreadyHasPromo>" + (AccountAlreadyHasPromo ? "1" : "0") + "</AccountAlreadyHasPromo>");
      _sb.Append("<BillInGivesPromo>" + (BillInGivesPromo ? "1" : "0") + "</BillInGivesPromo>");

      _sb.AppendLine("<Vouchers>");
      foreach (Voucher _voucher in VoucherList)
      {
        Misc.DeflateString(_voucher.VoucherHTML.ToString(), out _deflated_voucher);
        _sb.Append("<Voucher>" + XML.StringToXmlValue(_deflated_voucher) + "</Voucher>");
      }
      _sb.AppendLine("</Vouchers>");
      _sb.AppendLine("</WCP_MsgAddCreditReply>");

      return _sb.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      VoucherList = new ArrayList();

      if (reader.Name != "WCP_MsgAddCreditReply")
      {
        throw new WCP_Exception(WCP_ResponseCodes.WCP_RC_MSG_FORMAT_ERROR, "Tag WCP_MsgAddCreditReply not found in content area");
      }
      else
      {
        SessionBalanceAfterAdd = Int64.Parse(XML.GetValue(reader, "SessionBalanceAfterAdd"));

        reader.Read();
        reader.Read();
        if (reader.Name == "AccountAlreadyHasPromo")
        {
          AccountAlreadyHasPromo = XML.ReadTagValue(reader, "AccountAlreadyHasPromo") == "1";
        }

        reader.Read();
        if (reader.Name == "BillInGivesPromo")
        {
          BillInGivesPromo = XML.ReadTagValue(reader, "BillInGivesPromo") == "1";
        }

        while ((!(reader.Name == "Vouchers" && reader.NodeType == XmlNodeType.EndElement)) && (!(reader.Name == "WCP_MsgAddCreditReply" && reader.NodeType == XmlNodeType.EndElement)))
        {
          reader.Read();
          if ((reader.Name == "Voucher") && (reader.NodeType != XmlNodeType.EndElement))
          {
            _deflated_voucher = XML.GetValue(reader, "Voucher");
            Misc.InflateString(_deflated_voucher, out _inflated_voucher);
            VoucherList.Add(_inflated_voucher);
          }
        }
      }
    }
  }
  */
  #endregion

}
