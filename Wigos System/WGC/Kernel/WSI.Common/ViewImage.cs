//-------------------------------------------------------------------
// Copyright � 2007-2013 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   cls_view_image
// DESCRIPTION:   Class to show images in fullscreen mode
// AUTHOR:        Pau Guillamon Jaime
// CREATION DATE: 28-PGJ-2013
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 28-JUN-2013  PGJ    Initial version
//--------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace WSI.Common
{
  public static class ViewImage
  {
    // PURPOSE: Shows a full screen form with a image using the image passed as parameter
    //
    //  PARAMS:
    //     - INPUT:
    //           - ImageId_1: Image 1
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    public static bool ViewFullScreenImage(Image Image)
    {
      Panel _pnl;
      Panel [] _images;

      if (Image == null)
      {
        return false;
      }

      _images = new Panel[1];

      // Create the panel and set its properties
      _pnl = new Panel();
      _pnl.Dock = DockStyle.Fill;
      _pnl.BackgroundImage = Image;
      _pnl.BackColor = Color.Black;
      _pnl.BackgroundImageLayout = ImageLayout.Zoom;
      _pnl.Cursor = Cursors.Hand;

      // Add the panel into an array and shows a full screen form with it
      _images[0] = _pnl;
      ViewFullScreenImages(_images);

      return true;
    } // ViewFullScreenImage


    // PURPOSE: Shows a full screen form with two images using the images passed as parameters
    //
    //  PARAMS:
    //     - INPUT:
    //           - ImageId_1: Image 1
    //           - ImageId_2: Image 2
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    public static bool ViewFullScreenImage(Image Image_1, Image Image_2)
    {
      Panel _pnl;
      Panel _pnl2;
      Panel [] _images;

      if (Image_1 == null && Image_2 == null)
      {
        return false;
      }

      if (Image_1 == null)
      {
        return ViewImage.ViewFullScreenImage(Image_2);
      }

      if (Image_2 == null)
      {
        return ViewImage.ViewFullScreenImage(Image_1);
      }

      _images = new Panel[2];

      // Create the panel and set its properties
      _pnl = new Panel();
      _pnl.Dock = DockStyle.None;
      _pnl.Left = 0;
      _pnl.Width = Screen.PrimaryScreen.Bounds.Width / 2;
      _pnl.Height = Screen.PrimaryScreen.Bounds.Height;
      _pnl.BackgroundImage = Image_1;
      _pnl.BackColor = Color.Black;
      _pnl.BackgroundImageLayout = ImageLayout.Zoom;
      _pnl.Cursor = Cursors.Hand;

      // Create panel 2 and set its properties
      _pnl2 = new Panel();
      _pnl2.Dock = DockStyle.None;
      _pnl2.Left = Screen.PrimaryScreen.Bounds.Width / 2;
      _pnl2.Width = Screen.PrimaryScreen.Bounds.Width / 2;
      _pnl2.Height = Screen.PrimaryScreen.Bounds.Height;
      _pnl2.BackgroundImage = Image_2;
      _pnl2.BackColor = Color.Black;
      _pnl2.BackgroundImageLayout = ImageLayout.Zoom;
      _pnl2.Cursor = Cursors.Hand;

      // Add both panels into an array and shows a full screen form with them
      _images[0] = _pnl;
      _images[1] = _pnl2;
      ViewFullScreenImages(_images);

      return true;
    } // ViewFullScreenImage

    // PURPOSE: Creates and show a form with the panels passed as parameter
    //
    //  PARAMS:
    //     - INPUT:
    //           - Images: an array of the Panels to be shown
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    private static void ViewFullScreenImages(Panel [] Images)
    {
      Form _frm;

      _frm = new Form();

      // Add the panels to the form and add its click handler
      foreach (Panel _current_panel in Images)
      {
        if(_current_panel == null)
        {
          continue;
        }

        _frm.Controls.Add(_current_panel);
        _current_panel.Click += new EventHandler(ViewImage.DoCloseFormInPanel);
      }

      // Sets the form properties
      _frm.Left = Screen.PrimaryScreen.Bounds.Width / 2;
      _frm.BackColor = System.Drawing.Color.Black;
      _frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      _frm.KeyPreview = true;
      _frm.WindowState = System.Windows.Forms.FormWindowState.Maximized;
      _frm.TopMost = true;
      _frm.ShowInTaskbar = false;

      // Add the form click handler
      _frm.KeyDown += new KeyEventHandler(ViewImage.DoCloseForm);

      _frm.Show();

    } // ViewFullScreenImages


    // PURPOSE: Close the full screen
    //
    //  PARAMS:
    //     - INPUT:
    //           - Obj:
    //           - e:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    private static void DoCloseFormInPanel(object sender, EventArgs e)
    {
      ((Panel)sender).FindForm().Close();
    }

    // PURPOSE: Close the full screen
    //
    //  PARAMS:
    //     - INPUT:
    //           - Obj:
    //           - e:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    private static void DoCloseForm(object sender, EventArgs e)
    {
      ((Form)sender).Close();
    }
  }
}
