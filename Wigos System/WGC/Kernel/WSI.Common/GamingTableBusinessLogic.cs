//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GamingTables.cs
//  
//   DESCRIPTION: 
// 
//        AUTHOR: Dani Dom�nguez
// 
// CREATION DATE: 27-AUG-2014
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ------     ----------------------------------------------------------
// 27-AUG-2014 DDM        First release.
// 23-OCT-2014 DHA & DDM  Fixed Bug WIG-1567 :
// 28-OCT-2014 DCS        Fixed Bug #WIG-1586: Awarded points was lost when user was paused because inserted in another table and then close sesion from GUI
// 26-NOV-2014 DCS        Fixed Bug #WIG-1545: Remove select within the insert
// 30-MAR-2015 DHA        Added changes for clone terminals feature
// 11-JUN-2015 DCS        Fixed Bug #WIG-2350: Incorrect values in the table report
// 23-JUN-2015 DHA        Added IsEnableManualConciliation method
// 30-SEP-2015 ETP        Fixed Bug-3841 Added Case for null or empty values in currency at Gamming table Summary.
// 06-JUL-2016 DHA        Product Backlog Item 15064: multicurrency chips sale/purchase
// 11-JUL-2016 JML        Product Backlog Item 15065: Gaming tables (Fase 4): Player tracking. Multicurrency
// 13-JUL-2016 SMN        Fixed Bug 14837:Mesa de juego (Cashless,TITO): No se pueden sacar a los jugadores cuando est�n jugando
// 13-JUL-2016 SMN        Fixed Bug 14837:Mesa de juego (Cashless,TITO): No se pueden sacar a los jugadores cuando est�n jugando
// 28-JUN-2016 DHA        Product Backlog Item 15903: Gaming tables (Fase 5): Drop box collection on Cashier
// 10-AUG-2016 RAB        Product Backlog Item 15172: GamingTables (Phase 4): Cashier - Adapt report screen in Gaming Tables
// 29-AGU-2016 DHA        Fixed Bug 17113: error showing first fill in gaming tables (Fixed and chips sleeps on table)
// 08-SEP-2016 FOS        PBI 16567: Tables: Split PlayerTracking to GamingTables (Phase 5)
// 15-SEP-2016 FOS        Fixed Bug 17682: Tables: Error object reference in log
// 15-SEP-2016 RAB        Bug 16904: GamingTables - GUI: Displayed time in the latest client in WigosGUI activity isn't correct
// 21-SEP-2016 FOS        Fixed Bug 17882: Tables: Exception when exist a gamingtable without seats in design
// 22-SEP-2016 JML        Fixed Bug 17571: Gaming tables - Integrated Cashier: saling chips - wrong error message
// 04-OCT-2016 FJC        Fixed Bug 18264:Buckets: en movimientos de cuenta el valor inicial est� a 0
// 16-NOV-2016 LTC        Bug 18293: Tables: Cage movement to gaming tables with player tracking dosabled doesn't update closed table icon. 
// 11-JAN-2017 RAB        Bug 21956:CASHIER: Application Exception When Table Opened
// 04-ABR-2017 JML & DHA  PBI 26401:MES10 Ticket validation - Ticket status change to cancelled
// 26-APR-2017 JML        PBI 26808: MES10 Ticket validation - Ticket conciliation
// 26-APR-2017 JML        PBI 26810: MES10 Ticket validation - Ticket conciliation action (without gaming table validation)
// 08-MAY-2017 RAB        PBI 27177: Win / loss - Access to introduction screen
// 10-MAY-2017 JML        PBI 27288: Win / loss - Create automatically a null value for unreported hours
// 15-JUN-2017 DHA        PBI 27996:WIGOS-2482 - Dealer copy ticket - Validation in gaming table
// 15-JUN-2017 JML        PBI 27998: WIGOS-2695 - Rounding - Apply rounding according to the minimum chip
// 07-AUG-2017 RAB        Bug 29201:WIGOS-4245 The cashier is not applying properly the conversion between currency exchanges once the user introduce a new currency in a openened gambling table session
// 18-AUG-2017 DHA        PBI 29401:WIGOS-4554 Enable backward compatibility of tables chips
// 04-SEP-2017 JML        PBI 29533: WIGOS-3413 MES16 - Points assignment: gaming table proposal
// 07-SEP-2017 RAB        PBI 28944: WIGOS-3414 MES16 - Points assignment: Gaming table session edition
// 16-OCT-2017 DHA        Bug 30253:WIGOS-5432 [Ticket #9123] Accounts movements - Points in tables v03.006.0023
// 25-OCT-2017 RAB        Bug 30416: WIGOS-6074 Linked gaming table: the terminal linked to gaming table allows to validate a dealer copy ticket to another gaming table not linked in only one sale movement
// 06-NOV-2017 RAB        Bug 30537:WIGOS-6337 Gaming tables point assignment: Computed and assigned points are calculated and saved to anonymous user in gaming table session editor
// 28-FEB-2018 JML        Fixed Bug 31758:WIGOS-8398 [Ticket #12336] Mesas
// 14-MAR-2018 RGR        Bug 31934:WIGOS-8743 [Ticket #12993] Notificaci�n de Cliente Activo en Mesa de Juego V03.06.0035 
// 03-AUG-2018 MS         Bug 33790:[WIGOS-13891] [Ticket #16007] Fallo � Multiplicador � Sesiones de Mesas no funciona version Vo3.08.0027
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Xml;
using System.Globalization;
using WSI.Common.Junkets;

namespace WSI.Common
{
  public static partial class GamingTableBusinessLogic
  {
    public enum GT_RESPONSE_CODE
    {
      OK = 0,
      ERROR_GENERIC = 1,
    }

    public enum GT_SHOW_MESSAGE
    {
      HIDE = 0,
      MESSAGE_BOX = 1,
      LABEL = 2,
    }

    public enum GT_READ_TABLES_MODE
    {
      TABLE_ID = 0,
      SEAT_ID = 1,
      BANK_ID = 2,
    }


    public enum GT_MODE
    {
      DISABLED = 0,
      GUI_AND_CASHIER = 1, // Mesas activadas + gesti�n de mesa (gui y cajero) + Con playertracking
      GUI = 2
    }

    public enum GT_COPY_DEALER_VALIDATED_SOURCE
    {
      NONE = 0,
      GAMING_TABLE = 1,
      GUI = 2,
    }

    public enum GT_WIN_LOSS_MODE
    {
      NONE = 0,
      BY_TOTALS = 1,
      BY_DENOMINATION = 2,
    }

    public enum ASSIGNMENT_POINTS_MODE
    {
      AUTOMATIC = 0,
      MANUAL = 1,
    }

    public static Boolean IsGamingTablesEnabledAndPlayerTracking()
    {
      return IsFeaturesGamingTablesEnabled() && (GamingTablesMode() != GT_MODE.DISABLED) && IsEnabledGTPlayerTracking();
    }

    public static Boolean IsGamingTablesEnabled()
    {
      return IsFeaturesGamingTablesEnabled() && (GamingTablesMode() != GT_MODE.DISABLED);
    }

    public static GT_MODE GamingTablesMode()
    {
      if (!IsFeaturesGamingTablesEnabled())
      {
        return GT_MODE.DISABLED;
      }

      return (GT_MODE)GeneralParam.GetInt32("GamingTables", "Mode", 0);
    }

    public static Boolean IsFeaturesGamingTablesEnabled()
    {
      return (GeneralParam.GetBoolean("Features", "GamingTables", false) && Cage.IsCageEnabled());
    }

    public static GamingTableDropBoxCollectionCount GamingTableDropBoxCollectionCountMode()
    {
      try
      {
        return (GamingTableDropBoxCollectionCount)GeneralParam.GetInt32("GamingTables", "Dropbox.CollectionMode", 0);
      }
      catch
      {
        return GamingTableDropBoxCollectionCount.Cage;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Player tracking on gaming table are enabled or dissabled
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: Player tracking on gaming table are enabled. False: Otherwise.
    // 
    //   NOTES:
    //
    public static Boolean IsEnabledGTPlayerTracking()
    {
      return GeneralParam.GetBoolean("GamingTable.PlayerTracking", "Enabled", false);
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Get Gaming table win/loss are enabled or dissabled
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: Gaming table win/loss are enabled. False: Otherwise.
    // 
    //   NOTES:
    //
    public static Boolean IsEnabledGTWinLoss()
    {
      return GeneralParam.GetBoolean("GamingTable.PlayerTracking", "WinLoss.Mode", false);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Gaming table win/loss are enabled or dissabled
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: Gaming table win/loss are enabled. False: Otherwise.
    // 
    //   NOTES:
    //
    public static GT_WIN_LOSS_MODE GetGTWinLossMode()
    {
      return (GT_WIN_LOSS_MODE)GeneralParam.GetInt32("GamingTable.PlayerTracking", "WinLoss.Mode", 0, 0, 2);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get WinLoss: Margin minutes to modify
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT: 
    //
    // RETURNS: Minutes: WinLoss: Margin to modify
    // 
    //   NOTES:
    //
    public static Int32  GetGTWinLossMinutes()
    {
      return GeneralParam.GetInt32("GamingTable.PlayerTracking", "WinLoss.MarginMinutesToModify", 5, 1, 29);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get General param for Player Tracking
    // 
    // RETURNS: Value of general param
    // 
    //          GetDefaultTimeOfAHand : Default duration of one hand in seconds
    //          GetPlayerTrackingAutomaticSave : If true save every time that change of player, 
    //                                           else user should push "save button" to save the changes.
    //          GetShowConfirmationExitMsg : If true show confirmation of end game for player,
    //                                       else don�t show.
    //          GetShowConfirmationChangePlayerMsg : If true show confirmation for change player without save,
    //                                               else don�t show.
    //
    public static Int32 GetDefaultTimeOfAHand()
    {
      // Min value 10 seconds!
      return GeneralParam.GetInt32("GamingTable.PlayerTracking", "DefaultTimeOfAHand", 300, 10, Int32.MaxValue);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get General param for Player Tracking
    // 
    // RETURNS: Value of general param
    // 
    //          If true show message when validate values
    //                  else don�t show.
    //
    public static GamingTableBusinessLogic.GT_SHOW_MESSAGE GetShowInformationMessages()
    {
      return (GamingTableBusinessLogic.GT_SHOW_MESSAGE)GeneralParam.GetInt32("GamingTable.PlayerTracking", "ShowInformationMessages", 2);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get general param for Restart in Swap Seats
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: Restart in Swap Seats are enabled. False: Otherwise.
    // 
    //   NOTES:
    //
    public static Boolean IsEnabledRestartInSwapSeats()
    {
      return GeneralParam.GetBoolean("GamingTable.PlayerTracking", "RestartPlayerTrackingInSwapSeats", false);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get  Visual effects on Player tracking are enabled or dissabled
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: Visual effects on Player tracking are enabled. False: Otherwise.
    // 
    //   NOTES: 
    //
    public static Boolean IsEnabledTransitionEffects()
    {
      return GeneralParam.GetBoolean("GamingTable.PlayerTracking", "Cashier.TransitionEffects", true);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get the manual conciliation
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: Visual effects on Player tracking are enabled. False: Otherwise.
    // 
    //   NOTES: 
    public static Boolean IsEnableManualConciliation()
    {
      return GeneralParam.GetBoolean("GamingTables", "Cashier.Mode", false);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get if is enable "Cash desk dealer tickets validation"
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT: 
    //
    // RETURNS: True: "Cash desk dealer tickets validation" are enabled. False: Otherwise.
    // 
    //   NOTES: 
    public static Boolean IsEnableCashDeskDealerTicketsValidation()
    {
      return GeneralParam.GetBoolean("GamingTable.PlayerTracking", "BuyIn.Mode", false) && !TITO.Utils.IsTitoMode();
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Dealer Copy Tickets Expiration Days
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT: 
    //
    // RETURNS: (Int32) Days: Dealer Copy Tickets Expiration Days
    // 
    //   NOTES: 
    public static Int32 GetDealerCopyTicketsExpirationDays()
    {
      return GeneralParam.GetInt32("GamingTables", "DealerCopyTickets.ExpirationDays", 0, 0, 99999999);
    }
          
    /// <summary>
    /// Check if the account must be validated when a copy dealer is validate at table
    /// </summary>
    /// <returns></returns>
    public static Boolean CheckAccountValidationTicketValidation()
    {
      return GeneralParam.GetBoolean("GamingTable.PlayerTracking", "TicketValidation.CheckAccount", false);
    }

    /// <summary>
    ///   Get Assignment Points Mode
    /// </summary>
    /// <returns>
    ///   AUTOMATIC = 0,
    ///   MANUAL = 1,
    /// </returns>
    public static ASSIGNMENT_POINTS_MODE GetAssignmentPointsMode()
    {
      return (ASSIGNMENT_POINTS_MODE)GeneralParam.GetInt32("GamingTables", "PointsAssignment.Mode", 0, 0, 1);
    }

    /// <summary>
    /// Get days to edit assignment points in gambling table session edit report
    /// </summary>
    /// <returns></returns>
    public static Int32 PointsAssignmentDays()
    {
      return GeneralParam.GetInt32("GamingTables", "PointsAssignment.Mode", 0);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Read Class Gaming Table. 
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int32: Gaming Table Id
    //
    //      - OUTPUT:
    //        - GamingTable: Information about play sessions to the gaming table
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean ReadTable(Int32 GamingTableId, out GamingTable GamingTable)
    {
      StringBuilder _sb;
      Seat _seat;
      CardData _card_data = new CardData();
      GamingTablesSessions _gt_session;

      TableLimitsDictionary _table_acepted_iso_codes;

      GamingTable = new GamingTable();
      if (GamingTable.Seats == null)
      {
        GamingTable.Seats = new Dictionary<Int64, Seat>();
      }

      try
      {
        _sb = new StringBuilder();

        GetSqlQueryForReadTable(_sb, GT_READ_TABLES_MODE.TABLE_ID);

        using (DB_TRX _db_trx = new DB_TRX())
        {
          // Read gaming table session
          if (!GamingTablesSessions.GetOpenedSession(GamingTableId, out _gt_session, _db_trx.SqlTransaction))
          {
            return false;
          }

          GamingTable.GamingTableSession = _gt_session;

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = GamingTableId;
            _sql_cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = true;
            _sql_cmd.Parameters.Add("@TablePlaySessionStatusOpen", SqlDbType.Int).Value = GTPlaySessionStatus.Opened;
            _sql_cmd.Parameters.Add("@TablePlaySessionStatusAway", SqlDbType.Int).Value = GTPlaySessionStatus.Away;
            _sql_cmd.Parameters.Add("@pUnknown", SqlDbType.Int).Value = GTPlayerType.Unknown;
            _sql_cmd.Parameters.Add("@pAnonymousWithoutCard", SqlDbType.Int).Value = GTPlayerType.AnonymousWithoutCard;
            if (_gt_session != null)
            {
              _sql_cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = _gt_session.GamingTableSessionId;
            }
            else
            {
              //The gambling table not have open session!
              _sql_cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = DBNull.Value;
            }

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              while (_reader.Read())
              {
                if (GamingTable.GamingTableId == 0)
                {
                  // Read GamingTable
                  if (!SetTableDataFromDB(_reader, GamingTable))
                  {
                    return false;
                  }
                }

                _seat = new Seat();

                // Read Seat
                if (SetSeatDataFromDB(_reader, _seat))
                {
                  if (_gt_session != null)
                  {
                    _seat.GamingTableSessionId = _gt_session.GamingTableSessionId;
                  }
                  GamingTable.Seats.Add(_seat.SeatId, _seat);
                }

              }
            }// Reader
          } // SqlCommand

          _table_acepted_iso_codes = new TableLimitsDictionary(GamingTable.GamingTableId, _db_trx.SqlTransaction);

          GamingTable.TableAceptedIsoCode = _table_acepted_iso_codes;

        } // DB_TRX

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //  

    //------------------------------------------------------------------------------
    // PURPOSE: Read Tables for bank. 
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int64: Bank Id
    //
    //      - OUTPUT:
    //        - GamingTables: Dictionary with Gaming tables 
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean ReadTables(Int64 BankId, out Dictionary<Int32, GamingTable> GamingTables)
    {
      StringBuilder _sb;
      Seat _seat;
      CardData _card_data = new CardData();
      GamingTablesSessions _gt_session;
      GamingTable GamingTable;

      TableLimitsDictionary _table_acepted_iso_codes;

      GamingTables = new Dictionary<Int32, GamingTable>();
      GamingTable = new GamingTable();

      try
      {
        _sb = new StringBuilder();

        GetSqlQueryForReadTable(_sb, GT_READ_TABLES_MODE.BANK_ID);

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pTableEnable", SqlDbType.Bit).Value = true;
            _sql_cmd.Parameters.Add("@TablePlaySessionStatusOpen", SqlDbType.Int).Value = GTPlaySessionStatus.Opened;
            _sql_cmd.Parameters.Add("@TablePlaySessionStatusAway", SqlDbType.Int).Value = GTPlaySessionStatus.Away;
            _sql_cmd.Parameters.Add("@pUnknown", SqlDbType.Int).Value = GTPlayerType.Unknown;
            _sql_cmd.Parameters.Add("@pAnonymousWithoutCard", SqlDbType.Int).Value = GTPlayerType.AnonymousWithoutCard;
            _sql_cmd.Parameters.Add("@pBankId", SqlDbType.Int).Value = BankId;
            _sql_cmd.Parameters.Add("@pSessionOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              while (_reader.Read())
              {
                if (GamingTable.GamingTableId == 0 || GamingTable.GamingTableId != _reader.GetInt32(0))
                {
                  GamingTable = new GamingTable();

                  // Read GamingTable
                  if (!SetTableDataFromDB(_reader, GamingTable))
                  {
                    return false;
                  }

                  GamingTable.Seats = new Dictionary<Int64, Seat>();
                  GamingTables.Add(_reader.GetInt32(0), GamingTable);
                }

                _seat = new Seat();

                // Read Seat
                if (SetSeatDataFromDB(_reader, _seat))
                {
                  GamingTable.Seats.Add(_seat.SeatId, _seat);
                }
              }
            }// Reader
          } // SqlCommand

          // Read gaming table session
          foreach (KeyValuePair<Int32, GamingTable> _gt_table in GamingTables)
          {
            if (!GamingTablesSessions.GetOpenedSession(_gt_table.Value.GamingTableId, out _gt_session, _db_trx.SqlTransaction))
            {
              return false;
            }

            _gt_table.Value.GamingTableSession = _gt_session;

            if (_gt_session != null)
            {
              foreach (KeyValuePair<Int64, Seat> _seat_aux in _gt_table.Value.Seats)
              {
                _seat_aux.Value.GamingTableSessionId = _gt_session.GamingTableSessionId;
              }
            }

            //Currencies by table
            _table_acepted_iso_codes = new TableLimitsDictionary(_gt_table.Value.GamingTableId, _db_trx.SqlTransaction);
            _gt_table.Value.TableAceptedIsoCode = _table_acepted_iso_codes;

          }

        } // DB_TRX

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } //  

    //------------------------------------------------------------------------------
    // PURPOSE: Update Gaming Table
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Gaming Table 
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - GT_RESPONSE_CODE 
    //
    public static GT_RESPONSE_CODE UpdateTable(ref GamingTable GamingTable, Int32 CashierUserId, Int64 CashierTerminalId)
    {
      StringBuilder _sb;
      GT_RESPONSE_CODE _error_code;
      PlayerTrackingMovements _PT_movements;
      GTPlayerTrackingSpeed _old_table_speed;
      Boolean _old_table_play;
      Int64 _sequence;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   GAMING_TABLES                               ");
        _sb.AppendLine("    SET   GT_TABLE_SPEED_SELECTED  =  @pSpeedSelected ");
        _sb.AppendLine("        , GT_PLAYER_TRACKING_PAUSE = @pGtStatus       ");
        _sb.AppendLine(" OUTPUT   DELETED.GT_TABLE_SPEED_SELECTED 'OldTableSpeed' ");
        _sb.AppendLine("        , DELETED.GT_PLAYER_TRACKING_PAUSE 'OldTablePlay' ");
        _sb.AppendLine("        , CAST(INSERTED.GT_TIMESTAMP AS BIGINT) 'Sequence'");
        _sb.AppendLine("  WHERE   GT_GAMING_TABLE_ID =       @pGamingTableID  ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = GamingTable.GamingTableId;
            _sql_cmd.Parameters.Add("@pSpeedSelected", SqlDbType.Int).Value = (Int32)GamingTable.SelectedSpeed;
            _sql_cmd.Parameters.Add("@pGtStatus", SqlDbType.Bit).Value = GamingTable.GamingTablePaused;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                _db_trx.Rollback();
                _error_code = GT_RESPONSE_CODE.ERROR_GENERIC;

                return _error_code;
              }
              _old_table_speed = (GTPlayerTrackingSpeed)_reader.GetInt32(0);
              _old_table_play = _reader.GetBoolean(1);
              _sequence = _reader.GetInt64(2);
            }

            if (GamingTable.GamingTablePaused)
            {
              _error_code = RestartTableCounters(GamingTable.GamingTableId, false, _db_trx.SqlTransaction);

              if (_error_code == GT_RESPONSE_CODE.ERROR_GENERIC)
              {
                _db_trx.Rollback();
                return _error_code;
              }
            }

            // Add movements
            _PT_movements = new PlayerTrackingMovements(CashierUserId, CashierTerminalId, GamingTable.GamingTableId, GamingTable.GamingTableSession.GamingTableSessionId);
            if (_old_table_speed != GamingTable.SelectedSpeed)
            {
              _PT_movements.Add(PlayerTrackingMovementType.TableSpeed, (decimal)_old_table_speed, (decimal)GamingTable.SelectedSpeed, "");
            }
            if (_old_table_play != GamingTable.GamingTablePaused)
            {
              if (GamingTable.GamingTablePaused)
              {
                _PT_movements.Add(PlayerTrackingMovementType.PauseTablePlay, 0, 1, "");
              }
              else
              {
                _PT_movements.Add(PlayerTrackingMovementType.ContinueTablePlay, 1, 0, "");
              }
            }

            if (!_PT_movements.Save(_db_trx.SqlTransaction))
            {
              _error_code = GT_RESPONSE_CODE.ERROR_GENERIC;
              return _error_code;
            }

            _db_trx.Commit();

            _error_code = GT_RESPONSE_CODE.OK;
            GamingTable.Sequence = _sequence;

          } // SqlCommand
        } // DB_TRX
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        _error_code = GT_RESPONSE_CODE.ERROR_GENERIC;
      }

      return _error_code;
    } // UpdateTable

    //------------------------------------------------------------------------------
    // PURPOSE: Read Class Seat. 
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int32: Gaming Table Id
    //        - Int64: Seat Id
    //
    //      - OUTPUT:
    //        - Seat: Information about play sessions to the seat
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean ReadSeat(Int32 GamingTableId, Int64 SeatId, Int64 GamingTableSessionId, out Seat Seat)
    {
      Seat = new Seat();
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          return ReadSeat(GamingTableId, SeatId, GamingTableSessionId, _db_trx.SqlTransaction, out Seat);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Read Class Seat. 
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int32: Gaming Table Id
    //        - Int64: Seat Id
    //
    //      - OUTPUT:
    //        - Seat: Information about play sessions to the seat
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean ReadSeat(Int32 GamingTableId, Int64 SeatId, Int64 GamingTableSessionId, SqlTransaction Trx, out Seat Seat)
    {
      StringBuilder _sb;
      Seat _seat;

      Seat = new Seat();

      try
      {
        _sb = new StringBuilder();
        GetSqlQueryForReadTable(_sb, GT_READ_TABLES_MODE.SEAT_ID);

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = GamingTableId;
          _sql_cmd.Parameters.Add("@pSeatId", SqlDbType.BigInt).Value = SeatId;
          _sql_cmd.Parameters.Add("@pEnabled", SqlDbType.Bit).Value = true;
          _sql_cmd.Parameters.Add("@TablePlaySessionStatusOpen", SqlDbType.Int).Value = GTPlaySessionStatus.Opened;
          _sql_cmd.Parameters.Add("@TablePlaySessionStatusAway", SqlDbType.Int).Value = GTPlaySessionStatus.Away;
          _sql_cmd.Parameters.Add("@pUnknown", SqlDbType.Int).Value = GTPlayerType.Unknown;
          _sql_cmd.Parameters.Add("@pAnonymousWithoutCard", SqlDbType.Int).Value = GTPlayerType.AnonymousWithoutCard;

          if (GamingTableSessionId > 0)
          {
            _sql_cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = GamingTableSessionId;
          }
          else
          {
            //The gambling table not have open session!
            _sql_cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = DBNull.Value;
          }

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              _seat = new Seat();

              if (SetSeatDataFromDB(_reader, _seat))
              {
                Seat = _seat;
                Seat.GamingTableSessionId = GamingTableSessionId;
              }
            }
          }// Reader
        } // SqlCommand

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ReadSeat

    //------------------------------------------------------------------------------
    // PURPOSE: Get sql string for gambling table or seat read
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //        - StringBuilder: Select to execute
    //        - GT_READ_TABLES_MODE: read by GamingTableID, BankId or SeatId
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    private static void GetSqlQueryForReadTable(StringBuilder SB, GT_READ_TABLES_MODE GamingTableReadMode)
    {
      SB.AppendLine("SELECT   GT_GAMING_TABLE_ID");      // 0 
      SB.AppendLine("       , GT_NAME");                 // 1
      SB.AppendLine("       , GT_TYPE_ID");              // 2

      SB.AppendLine("       , GTS_SEAT_ID");             // 3

      if (GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
      {
        SB.AppendLine("       , ISNULL(GTPS_ACCOUNT_ID, GTS_ACCOUNT_ID)");          // 4
      }
      else
      {
        SB.AppendLine("       , ''"); //4
      }

      SB.AppendLine("       , GTS_TERMINAL_ID");         // 5

      if (GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
      {
        SB.AppendLine("       , GTPS_PLAY_SESSION_ID");    // 6
        SB.AppendLine("       , GTPS_ACCOUNT_ID");         // 7
        SB.AppendLine("       , GTPS_STARTED");            // 8
        SB.AppendLine("       , GTPS_PLAYED_AMOUNT");      // 9
        SB.AppendLine("       , GTPS_BET_MIN");         // 10
        SB.AppendLine("       , GTPS_BET_MAX");         // 11
        SB.AppendLine("       , GTPS_PLAYED_AVERAGE");     // 12
        SB.AppendLine("       , GTPS_WALK      ");         // 13

        SB.AppendLine("       , CASE WHEN GTPS_ACCOUNT_ID IS NULL THEN @pUnknown                             ");         // 14
        SB.AppendLine("         ELSE CASE WHEN GTPS_ACCOUNT_ID = GTS_ACCOUNT_ID THEN @pAnonymousWithoutCard  ");         // 14
        SB.AppendLine("         ELSE AC_USER_TYPE END END AS PLAYERTYPE                                      ");         // 14

        SB.AppendLine("       , TE_BASE_NAME  ");         // 15
        SB.AppendLine("       , GTPS_NETWIN ");        // 16
        SB.AppendLine("       , GTPS_PLAYS");       // 17
        SB.AppendLine("       , GTPS_POINTS ");            // 18
        SB.AppendLine("       , GTPS_PLAYER_SKILL ");      // 19
        SB.AppendLine("       , GTPS_PLAYER_SPEED ");      // 20
        SB.AppendLine("       , GTPS_START_WALK   ");      // 21
        SB.AppendLine("       , GTPS_COMPUTED_POINTS ");   // 22
      }
      else
      {
        SB.AppendLine("       , ''"); //6
        SB.AppendLine("       , ''"); //7
        SB.AppendLine("       , ''"); //8
        SB.AppendLine("       , ''"); //9
        SB.AppendLine("       , ''"); //10
        SB.AppendLine("       , ''"); //11
        SB.AppendLine("       , ''"); //12
        SB.AppendLine("       , ''"); //13

        SB.AppendLine("       , ''"); //14

        SB.AppendLine("       , ''"); //15
        SB.AppendLine("       , ''"); //16
        SB.AppendLine("       , ''"); //17
        SB.AppendLine("       , ''"); //18
        SB.AppendLine("       , ''"); //19
        SB.AppendLine("       , ''"); //20
        SB.AppendLine("       , ''"); //21
        SB.AppendLine("       , ''"); //22
      }

      SB.AppendLine("       , GT_TABLE_SPEED_NORMAL ");         // 23

      if (GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
      {
        SB.AppendLine("       , CAST(GTPS_TIMESTAMP AS BIGINT) "); // 24
      }
      else
      {
        SB.AppendLine("       , ''"); //24
      }

      SB.AppendLine("       , GTS_POSITION    ");        // 25
      SB.AppendLine("       , GT_BET_MIN    ");          // 26
      SB.AppendLine("       , GT_BET_MAX    ");          // 27

      if (GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
      {
        SB.AppendLine("       , GTPS_CURRENT_BET    ");   // 28
        SB.AppendLine("       , GTPS_TOTAL_SELL_CHIPS    "); // 39
        SB.AppendLine("       , GTPS_CHIPS_OUT    ");        // 30
        SB.AppendLine("       , GTPS_CHIPS_IN    ");         // 31
        SB.AppendLine("       , GTPS_UPDATED    ");          // 32

        SB.AppendLine("       , GTPS_IN_SESSION_LAST_PLAYED_AMOUNT        ");// 33
        SB.AppendLine("       , GTPS_IN_SESSION_LAST_PLAY    ");      // 34
        SB.AppendLine("       , GTPS_UPDATED_PLAYED_CURRENT  ");  // 35
      }
      else
      {
        SB.AppendLine("       , ''"); //28
        SB.AppendLine("       , ''"); //29
        SB.AppendLine("       , ''"); //30
        SB.AppendLine("       , ''"); //31
        SB.AppendLine("       , ''"); //32

        SB.AppendLine("       , ''"); //33
        SB.AppendLine("       , ''"); //34
        SB.AppendLine("       , ''"); //35
      }

      SB.AppendLine("       , GT_TABLE_SPEED_SLOW                       ");         // 36
      SB.AppendLine("       , GT_TABLE_SPEED_FAST                       ");         // 37
      SB.AppendLine("       , GT_TABLE_SPEED_SELECTED                   ");         // 38
      SB.AppendLine("       , GT_PLAYER_TRACKING_PAUSE                  ");         // 39
      SB.AppendLine("       , GT_PLAYS_COUNT");           // 40
      SB.AppendLine("       , CAST(GT_TIMESTAMP AS BIGINT)");           // 41
      SB.AppendLine("       , GT_CASHIER_ID "); // 42
      SB.AppendLine("       , GT_TABLE_IDLE_PLAYS "); // 43
      SB.AppendLine("       , GT_HAS_INTEGRATED_CASHIER "); // 44
      SB.AppendLine("       , ISNULL(GT_THEORIC_HOLD,0) "); // 45

      if (GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
      {
        SB.AppendLine("       , GTPS_ISO_CODE AS ISO_CODE "); // 46
      }
      else
      {
        SB.AppendLine("       , ''"); // 46
      }

      SB.AppendLine("  FROM   GAMING_TABLES ");
      SB.AppendLine(" INNER   JOIN GT_SEATS ON GT_GAMING_TABLE_ID = GTS_GAMING_TABLE_ID ");

      if (GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
      {
        SB.AppendLine("  LEFT   JOIN GT_PLAY_SESSIONS  ON GTS_CURRENT_PLAY_SESSION_ID = GTPS_PLAY_SESSION_ID  ");

        SB.AppendLine("  LEFT   JOIN ACCOUNTS ON AC_ACCOUNT_ID = ISNULL(GTPS_ACCOUNT_ID, GTS_ACCOUNT_ID) ");

        SB.AppendLine(" INNER   JOIN TERMINALS ON TE_TERMINAL_ID = GTS_TERMINAL_ID ");
      }

      if (GamingTableReadMode == GT_READ_TABLES_MODE.TABLE_ID || GamingTableReadMode == GT_READ_TABLES_MODE.SEAT_ID)
      {
        SB.AppendLine(" WHERE   GT_GAMING_TABLE_ID = @pGamingTableId");
        SB.AppendLine("   AND   GTS_ENABLED = @pEnabled");

        if (GamingTableReadMode == GT_READ_TABLES_MODE.SEAT_ID)
        {
          SB.AppendLine("   AND   GTS_SEAT_ID = @pSeatId");
        }
      }
      else
      {
        SB.AppendLine(" WHERE   GT_ENABLED = @pTableEnable ");
        SB.AppendLine("   AND   GT_BANK_ID = @pBankId");
        SB.AppendLine("   AND   GT_NUM_SEATS > 0 ");
        SB.AppendLine("   AND   GTS_ENABLED = 1");
        SB.AppendLine(" ORDER BY  GT_GAMING_TABLE_ID ASC ");
      }

    } // GetSqlQueryForReadTable

    //------------------------------------------------------------------------------
    // PURPOSE: Set table data from reader
    // 
    //  PARAMS:
    //      - INPUT:
    //        - SqlDataReader: 
    //        - Seat: 
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    private static Boolean SetTableDataFromDB(SqlDataReader _reader, GamingTable GamingTable)
    {
      if (_reader.IsDBNull(0))
      {
        GamingTable = null;
      }

      try
      {
        GamingTable.GamingTableId = _reader.GetInt32(0);
        GamingTable.GamingTableName = _reader.GetString(1);
        GamingTable.GamingTableType = _reader.GetInt32(2);
        GamingTable.BetMin = _reader.IsDBNull(26) ? 0 : _reader.GetDecimal(26);
        GamingTable.BetMax = _reader.IsDBNull(27) ? 0 : _reader.GetDecimal(27);
        GamingTable.TheoricHold = _reader.GetDecimal(45);

        //Speeds
        GamingTable.TableSpeeds = new Dictionary<GTPlayerTrackingSpeed, Int32>();
        GamingTable.TableSpeeds.Add(GTPlayerTrackingSpeed.Slow, _reader.IsDBNull(36) ? 0 : _reader.GetInt32(36));
        GamingTable.TableSpeeds.Add(GTPlayerTrackingSpeed.Medium, _reader.IsDBNull(23) ? 0 : _reader.GetInt32(23));
        GamingTable.TableSpeeds.Add(GTPlayerTrackingSpeed.Fast, _reader.IsDBNull(37) ? 0 : _reader.GetInt32(37));
        GamingTable.SelectedSpeed = _reader.IsDBNull(38) ? 0 : (GTPlayerTrackingSpeed)_reader.GetInt32(38);

        if (_reader.IsDBNull(43) || _reader.GetInt32(43) == 0)
        {
          GamingTable.TableIdlePlays = GeneralParam.GetInt32("GamingTable.PlayerTracking", "CurrentBetWarningElapsedPlays", 5);
        }
        else
        {
          GamingTable.TableIdlePlays = _reader.GetInt32(43);
        }

        // Paused table
        if (_reader.IsDBNull(39))
        {
          GamingTable.GamingTablePaused = false;
        }
        else
        {
          if (_reader.GetBoolean(39))
          {
            GamingTable.GamingTablePaused = true;
          }
          else
          {
            GamingTable.GamingTablePaused = false;
          }
        }

        GamingTable.CashierId = _reader.IsDBNull(42) ? 0 : _reader.GetInt32(42);
        GamingTable.Sequence = _reader.IsDBNull(41) ? 0 : _reader.GetInt64(41);
        GamingTable.IsIntegrated = _reader.IsDBNull(44) ? false : _reader.GetBoolean(44);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

    } // SetSeatDataFromDB

    //------------------------------------------------------------------------------
    // PURPOSE: Set seat data from reader
    // 
    //  PARAMS:
    //      - INPUT:
    //        - SqlDataReader: 
    //        - Seat: 
    //
    //      - OUTPUT:
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    private static Boolean SetSeatDataFromDB(SqlDataReader _reader, Seat Seat)
    {
      Int64 _seat_id;
      CardData _card_data = new CardData();
      TablePlaySession _table_play_session;
      String _national_currency;

      if (_reader.IsDBNull(3))
      {
        Seat = null;
      }

      _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");

      try
      {
        // Not have play sessiones with opened status   
        _seat_id = _reader.GetInt64(3);
        Seat.SeatId = _seat_id;
        Seat.VirtualTerminalId = _reader.GetInt32(5);

        if (GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
        {
          Seat.AccountId = _reader.GetInt64(4);
          Seat.PlayerType = (GTPlayerType)_reader.GetInt32(14);
          Seat.VirtualTerminalName = _reader.GetString(15);
        }

        Seat.GamingTableId = _reader.GetInt32(0);
        Seat.TheoricHold = _reader.GetDecimal(45);

        if (!CardData.DB_CardGetAllData(Seat.AccountId, _card_data, false))
        {
          Log.Error("LoadCardByAccountId. DB_GetCardAllData: Error reading card.");
        }
        else
        {
          Seat.AccountId = _card_data.AccountId;
          Seat.HolderGender = _card_data.PlayerTracking.HolderGender;
          Seat.HolderLevel = _card_data.PlayerTracking.CardLevel;

          if (_card_data.PlayerTracking.HolderName == "")
          {
            if (Seat.PlayerType == GTPlayerType.Unknown)
            {
              Seat.HolderName = "";
              Seat.FirstName = "";
            }
            else if (Seat.PlayerType == GTPlayerType.AnonymousWithoutCard)
            {
              Seat.HolderName = Resource.String("STR_CASHIER_GAMING_TABLE_PLAYER_UNKNOWN_BUTTON");
              Seat.FirstName = Seat.HolderName;
            }
            else
            {
              Seat.HolderName = Resource.String("STR_UC_CARD_CARD_HOLDER_ANONYMOUS");
              Seat.FirstName = Seat.HolderName;
            }
          }
          else
          {
            Seat.HolderName = _card_data.PlayerTracking.HolderName;
            Seat.FirstName = _card_data.PlayerTracking.HolderName3;
          }

          Seat.Points = _card_data.PlayerTracking.TruncatedPoints;

          if (_card_data.IsRecycled)
          {
            Seat.TrackData = CardData.RECYCLED_TRACK_DATA;
          }
          else
          {
            Seat.TrackData = _card_data.VisibleTrackdata();
          }
        }

        _table_play_session = new TablePlaySession();
        Seat.SeatPosition = _reader.IsDBNull(25) ? 0 : _reader.GetInt32(25);


        if (GamingTableBusinessLogic.IsEnabledGTPlayerTracking())
        {
          _table_play_session.GamingTableId = Seat.GamingTableId;
          _table_play_session.SeatId = Seat.SeatId;
          _table_play_session.TerminalId = Seat.VirtualTerminalId;
          _table_play_session.PlaySessionId = _reader.IsDBNull(6) ? 0 : _reader.GetInt64(6);
          _table_play_session.AccountId = _reader.IsDBNull(7) ? 0 : _reader.GetInt64(7);
          if (!_reader.IsDBNull(8))
          {
            _table_play_session.Started = _reader.GetDateTime(8);
          }
          _table_play_session.PlayedAmount = _reader.IsDBNull(9) ? 0 : _reader.GetDecimal(9);
          _table_play_session.PlayedMin = _reader.IsDBNull(10) ? 0 : _reader.GetDecimal(10);
          _table_play_session.PlayedMax = _reader.IsDBNull(11) ? 0 : _reader.GetDecimal(11);
          _table_play_session.PlayedAvg = _reader.IsDBNull(12) ? 0 : _reader.GetDecimal(12);
          _table_play_session.Walk = _reader.IsDBNull(13) ? 0 : _reader.GetInt64(13);

          _table_play_session.NetWin = _reader.IsDBNull(16) ? 0 : _reader.GetDecimal(16);
          _table_play_session.AwardedPoints = _reader.IsDBNull(18) ? 0 : _reader.GetDecimal(18);
          _table_play_session.AwardedPointsCalculated = _reader.IsDBNull(22) ? 0 : _reader.GetDecimal(22);
          _table_play_session.PlayerSkill = _reader.IsDBNull(19) ? GTPlaySessionPlayerSkill.Average : (GTPlaySessionPlayerSkill)_reader.GetInt32(19);
          _table_play_session.PlayerSpeed = _reader.IsDBNull(20) ? GTPlayerTrackingSpeed.Medium : (GTPlayerTrackingSpeed)_reader.GetInt32(20);
          if (!_reader.IsDBNull(21))
          {
            _table_play_session.Started_walk = _reader.GetDateTime(21);
          }
          else
          {
            _table_play_session.Started_walk = null;
          }

          _table_play_session.Sequence = _reader.IsDBNull(24) ? 0 : _reader.GetInt64(24);

          _table_play_session.CurrentBet = _reader.IsDBNull(28) ? 0 : _reader.GetDecimal(28);
          _table_play_session.TotalSellChips = _reader.IsDBNull(29) ? 0 : _reader.GetDecimal(29);
          _table_play_session.ChipsOut = _reader.IsDBNull(30) ? 0 : _reader.GetDecimal(30);
          _table_play_session.ChipsIn = _reader.IsDBNull(31) ? 0 : _reader.GetDecimal(31);

          if (!_reader.IsDBNull(32))
          {
            _table_play_session.Updated = _reader.GetDateTime(32);
          }
          else
          {
            _table_play_session.Updated = null;
          }

          _table_play_session.LastPlayedAmount = _reader.IsDBNull(33) ? 0 : _reader.GetDecimal(33);
          _table_play_session.LastPlaysCount = _reader.IsDBNull(34) ? 0 : _reader.GetInt32(34);

          if (!_reader.IsDBNull(35))
          {
            _table_play_session.UpdatedPlayedCurrent = _reader.GetDateTime(35);
          }
          else
          {
            _table_play_session.UpdatedPlayedCurrent = null;
          }

          _table_play_session.PlaysCount = _reader.IsDBNull(17) ? 0 : _reader.GetInt32(17);
          _table_play_session.PlayerIsoCode = _reader.IsDBNull(46) ? _national_currency : _reader.GetString(46);

        }
        _table_play_session.CurrentTableSpeed = _reader.IsDBNull(23) || _reader.GetInt32(23) <= 0 ? GetDefaultTimeOfAHand() : _reader.GetInt32(23);

        Seat.PlaySession = _table_play_session;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

    } // SetSeatDataFromDB

    //------------------------------------------------------------------------------
    // PURPOSE : Sell chips operation
    //
    //  PARAMS :
    //      - INPUT :
    //          - TablePlaySession
    //          - Amount
    //          - UserId
    //          - TerminalId
    //          - OperationId
    //          - DbTrx
    //      - OUTPUT :
    //
    // RETURNS :  
    //      - Boolean
    //

    private static Boolean PlayerTrackingRegister_Sell(TablePlaySession PlaySession, Decimal Amount, Int32 UserId, Int64 TerminalId, Int64 GamingTableSessionId, Int64 TicketTransactionId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      PlayerTrackingMovements _pt_movements;
      Boolean _is_enable_cash_desk_dealer_tickets_validation;

      _is_enable_cash_desk_dealer_tickets_validation = IsEnableCashDeskDealerTicketsValidation();

      try
      {
        _sb = new StringBuilder();

        //1 - update total buy and sel in playsesions.
        _sb.AppendLine("UPDATE   GT_PLAY_SESSIONS");
        _sb.AppendLine("   SET   GTPS_TOTAL_SELL_CHIPS = GTPS_TOTAL_SELL_CHIPS + @pTotalSellChips");
        _sb.AppendLine("       , GTPS_UPDATED = GETDATE()");
        _sb.AppendLine("       , GTPS_NETWIN = GTPS_NETWIN  + @pTotalSellChips");
        _sb.AppendLine(" WHERE   GTPS_PLAY_SESSION_ID = @pPlaySessionId");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTotalSellChips", SqlDbType.Decimal).Value = Amount;
          _sql_cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySession.PlaySessionId;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("PlayerTrackingRegister_Sell. Update Total Buy Chips CashIn. PlaySessionId: " + PlaySession.PlaySessionId.ToString());
          }
        }

        //2 - Generate playertracking movements
        _pt_movements = new PlayerTrackingMovements(UserId, TerminalId, PlaySession.GamingTableId, GamingTableSessionId);

        if (_is_enable_cash_desk_dealer_tickets_validation)
        {
          _pt_movements.Add(PlayerTrackingMovementType.DealerCopyValidate, PlaySession.SeatId, PlaySession.PlaySessionId, PlaySession.TerminalId, PlaySession.AccountId, 0, Amount, PlaySession.PlayerIsoCode, null, null);
        }
        else
        {
          _pt_movements.Add(PlayerTrackingMovementType.SaleChips, PlaySession.SeatId, PlaySession.PlaySessionId, PlaySession.TerminalId, PlaySession.AccountId, 0, Amount, PlaySession.PlayerIsoCode, null, null);
        }

        if (!_pt_movements.Save(Trx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // PlayerTrackingRegister_Sell

    /// <summary>
    /// Insert Copy Dealer Validated data on DB
    /// </summary>
    /// <param name="GamingTableSessionId"></param>
    /// <param name="TicketId"></param>
    /// <param name="CopyDealerValidatedSource"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean DB_InsertCopyDealerValidated(Int64 GamingTableSessionId, Int64 TicketId, GT_COPY_DEALER_VALIDATED_SOURCE CopyDealerValidatedSource, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {

        _sb = new StringBuilder();

        _sb.AppendLine("INSERT INTO      GT_COPY_DEALER_VALIDATED       ");
        _sb.AppendLine("                 (                              ");
        _sb.AppendLine("                 GTCD_GAMING_TABLE_SESSION_ID   ");
        _sb.AppendLine("             ,   GTCD_TICKET_ID                 ");
        _sb.AppendLine("             ,   GTCD_VALIDATION_SOURCE         ");
        _sb.AppendLine("             ,   GTCD_VALIDATION_DATETIME       ");
        _sb.AppendLine("                 )                              ");
        _sb.AppendLine("    VALUES                                      ");
        _sb.AppendLine("                 (                              ");
        _sb.AppendLine("                 @pGamingTableSessionId         ");
        _sb.AppendLine("             ,   @pTicketId                     ");
        _sb.AppendLine("             ,   @pValidationSource             ");
        _sb.AppendLine("             ,   GETDATE()                      ");
        _sb.AppendLine("                 )                              ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = GamingTableSessionId;
          _cmd.Parameters.Add("@pTicketId", SqlDbType.BigInt).Value = TicketId;
          _cmd.Parameters.Add("@pValidationSource", SqlDbType.Int).Value = CopyDealerValidatedSource;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Read Gaming Table Design
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int32: Gaming Table Id
    //
    //      - OUTPUT:
    //        - String: Xml design
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean ReadTableDesign(Int32 GamingTableId, out String GamingTableDesign, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _xml_design;

      GamingTableDesign = String.Empty;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT   GT_DESIGN_XML");
        _sb.AppendLine("  FROM   GAMING_TABLES ");
        _sb.AppendLine(" WHERE   GT_GAMING_TABLE_ID = @pGamingTableId");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = GamingTableId;

          _xml_design = _sql_cmd.ExecuteScalar();

          if (_xml_design == null || _xml_design == DBNull.Value)
          {
            return false;
          }
          GamingTableDesign = _xml_design.ToString();

        } // SqlCommand

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ReadTableDesign

    //------------------------------------------------------------------------------
    // PURPOSE : Get gaming table seats
    //
    //  PARAMS :
    //      - INPUT :
    //          - GamingTableId
    //          - Trx
    //      - OUTPUT :
    //          - NumSeats
    //
    // RETURNS :  
    //
    //   NOTES :
    public static Boolean GetTableSeats(Int64 GamingTableId, out List<Seat> Seats, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Seat _seat;

      Seats = new List<Seat>();
      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("SELECT   GTS_SEAT_ID          ");
        _sb.AppendLine("       , GTS_GAMING_TABLE_ID  ");
        _sb.AppendLine("       , GTS_ENABLED          ");
        _sb.AppendLine("       , GTS_TERMINAL_ID      ");
        _sb.AppendLine("       , GTS_ACCOUNT_ID       ");
        _sb.AppendLine("       , GTS_ENABLED_DATE     ");
        _sb.AppendLine("       , GTS_DISABLED_DATE    ");

        _sb.AppendLine("  FROM   GT_SEATS             ");

        _sb.AppendLine(" WHERE GTS_GAMING_TABLE_ID = @pGamingTableId  ");
        _sb.AppendLine(" ORDER BY GTS_SEAT_ID ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pGamingTableId", SqlDbType.BigInt).Value = GamingTableId;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              _seat = new Seat();
              _seat.SeatId = _reader.GetInt64(0);
              _seat.GamingTableId = _reader.GetInt32(1);
              _seat.Enabled = _reader.GetBoolean(2);
              _seat.VirtualTerminalId = _reader.GetInt32(3);
              _seat.AccountId = _reader.GetInt64(4);

              if (!_reader.IsDBNull(5))
              {
                _seat.EnabledDate = _reader.GetDateTime(5);
              }

              if (!_reader.IsDBNull(6))
              {
                _seat.DisabledDate = _reader.GetDateTime(6);
              }

              Seats.Add(_seat);
            }

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }  // GetTableSeats

    //------------------------------------------------------------------------------
    // PURPOSE : Insert/Update the number of seats
    //
    //  PARAMS :
    //      - INPUT :
    //          - GamingTableId
    //          - NumSeats
    //          - DesignXml
    //          - ProviderID (nullable
    //          - Trx
    //      - OUTPUT :
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    //   NOTES :
    public static Boolean InsertUpdateTableSeats(Int32 GamingTableId, Int32 NumSeats, String DesignXml, Int32? ProviderID, Int32 BankId, String FloorId, SqlTransaction Trx)
    {
      List<Seat> _seats;
      XmlDocument _xml_design;
      XmlNodeList _xml_nodes;
      List<Boolean> _seats_status;
      Int32 _current_seat;

      _current_seat = 0;
      _xml_design = new XmlDocument();
      _seats_status = new List<bool>();

      if (!GetTableSeats(GamingTableId, out _seats, Trx))
      {
        return false;
      }

      if (DesignXml != null && !String.IsNullOrEmpty(DesignXml))
      {
        // Load gaming table
        _xml_design.LoadXml(DesignXml);

        // Get Seats Nodes
        _xml_nodes = _xml_design.GetElementsByTagName("GamingSeatProperties");

        foreach (XmlElement _seat in _xml_nodes)
        {
          if (_current_seat >= _seats.Count)
          {
            _seats.Add(new Seat());
          }
          _seats[_current_seat].Enabled = Boolean.Parse(_seat.GetElementsByTagName("Enabled")[0].FirstChild.Value);

          // Set seat position with the next value
          if (_seat.GetElementsByTagName("SeatPosition").Count > 0)
          {
            _seats[_current_seat].SeatPosition = Int32.Parse(_seat.GetElementsByTagName("SeatPosition")[0].FirstChild.Value);
          }
          else
          {
            _seats[_current_seat].SeatPosition = _current_seat + 1;
          }
          _current_seat++;
        }

        if (_current_seat < _seats.Count)
        {
          for (Int32 _idx_seat = _current_seat; _idx_seat < _seats.Count; _idx_seat++)
          {
            _seats[_idx_seat].Enabled = false;
            _seats[_idx_seat].SeatPosition = null;
          }
        }
      }

      return InsertUpdateTableSeats(GamingTableId, _seats, ProviderID, BankId, FloorId, Trx);
    } // InsertUpdateTableSeats

    //------------------------------------------------------------------------------
    // PURPOSE : Insert/Update the number of seats
    //
    //  PARAMS :
    //      - INPUT :
    //          - GamingTableId
    //          - Seats
    //          - NumSeats
    //          - Trx
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    public static Boolean InsertUpdateTableSeats(Int32 GamingTableId, List<Seat> Seats, Int32? ProviderID, Int32 BankId, String FloorId, SqlTransaction Trx)
    {
      StringBuilder _sb_insert;
      StringBuilder _sb_update;
      Int32 _terminal_id;
      Int32 _current_seat;
      String _query;

      _sb_insert = new StringBuilder();
      _sb_update = new StringBuilder();
      _terminal_id = 0;
      _current_seat = 0;

      try
      {
        // Insert
        _sb_insert.AppendLine("INSERT INTO   GT_SEATS             ");
        _sb_insert.AppendLine("            ( GTS_GAMING_TABLE_ID  ");
        _sb_insert.AppendLine("            , GTS_ENABLED          ");
        _sb_insert.AppendLine("            , GTS_TERMINAL_ID      ");
        _sb_insert.AppendLine("            , GTS_ACCOUNT_ID       ");
        _sb_insert.AppendLine("            , GTS_ENABLED_DATE     ");
        _sb_insert.AppendLine("            , GTS_POSITION      )  ");
        _sb_insert.AppendLine("     VALUES                        ");
        _sb_insert.AppendLine("            ( @pGamingTableId      ");
        _sb_insert.AppendLine("            , @pEnabled            ");
        _sb_insert.AppendLine("            , @pTerminalId         ");
        _sb_insert.AppendLine("            , @pAccountId          ");
        _sb_insert.AppendLine("            , Getdate()            ");
        _sb_insert.AppendLine("            , @pSeatPosition   )   ");

        // Update
        _sb_update.AppendLine("UPDATE   GT_SEATS                              ");
        _sb_update.AppendLine("   SET   GTS_GAMING_TABLE_ID = @pGamingTableId ");
        _sb_update.AppendLine("       , GTS_ENABLED      = @pEnabled          ");
        _sb_update.AppendLine("       , GTS_TERMINAL_ID  = @pTerminalId       ");
        _sb_update.AppendLine("       , GTS_ACCOUNT_ID   = @pAccountId        ");
        _sb_update.AppendLine("       , GTS_ENABLED_DATE = CASE  WHEN GTS_ENABLED = 0 AND @pEnabled = 1 THEN GETDATE() ELSE GTS_ENABLED_DATE END ");
        _sb_update.AppendLine("       , GTS_DISABLED_DATE= CASE  WHEN GTS_ENABLED = 1 AND @pEnabled = 0 THEN GETDATE() ELSE GTS_DISABLED_DATE END ");
        _sb_update.AppendLine("       , GTS_POSITION = @pSeatPosition         ");
        _sb_update.AppendLine(" WHERE   GTS_SEAT_ID = @pSeatId                ");

        foreach (Seat _seat in Seats)
        {
          _current_seat++;

          if (_seat.SeatId > 0)
          {
            _query = _sb_update.ToString();
          }
          else
          {
            _query = _sb_insert.ToString();
          }

          // Create Terminal
          if (_seat.VirtualTerminalId == 0)
          {
            if (!InsertTerminalTableSeat(String.Format("GT{0}-Seat{1}", GamingTableId.ToString("D3"), _current_seat.ToString("D2")), ProviderID, BankId, FloorId, out _terminal_id, Trx))
            {
              return false;
            }

            _seat.VirtualTerminalId = _terminal_id;
          }

          // Create VirtualAccount
          if (_seat.AccountId == 0)
          {
            _seat.AccountId = Accounts.GetOrCreateVirtualAccount(_terminal_id, Trx);

            if (_seat.AccountId == 0)
            {
              return false;
            }
          }

          // Enable/disable terminal
          if (!DB_VirtualTerminal_Update(_seat.VirtualTerminalId, _seat.Enabled, ProviderID, BankId, FloorId, Trx))
          {
            return false;
          }

          using (SqlCommand _sql_cmd = new SqlCommand(_query, Trx.Connection, Trx))
          {
            _sql_cmd.Parameters.Add("@pGamingTableId", SqlDbType.BigInt).Value = GamingTableId;
            _sql_cmd.Parameters.Add("@pEnabled", SqlDbType.Int).Value = _seat.Enabled;
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = _seat.VirtualTerminalId;
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = _seat.AccountId;

            if (_seat.SeatPosition == null)
            {
              _sql_cmd.Parameters.Add("@pSeatPosition", SqlDbType.Int).Value = DBNull.Value;
            }
            else
            {
              _sql_cmd.Parameters.Add("@pSeatPosition", SqlDbType.Int).Value = _seat.SeatPosition;
            }

            if (_seat.SeatId > 0)
            {
              _sql_cmd.Parameters.Add("@pSeatId", SqlDbType.BigInt).Value = _seat.SeatId;
            }

            if (_sql_cmd.ExecuteNonQuery() != 1)
            {
              return false;
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }  // InsertUpdateTableSeats

    //------------------------------------------------------------------------------
    // PURPOSE : Insert terminal gaming table seat
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalName
    //          - ProviderName
    //          - Trx
    //      - OUTPUT :
    //          - TerminalId
    //
    // RETURNS :  
    //
    //   NOTES :
    private static Boolean InsertTerminalTableSeat(String TerminalName, Int32? ProviderID, Int32 BankId, String FloorId, out Int32 TerminalId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      SqlParameter _param;
      Int32 _provider_id;

      TerminalId = 0;
      _sb = new StringBuilder();

      try
      {
        // 26-NOV-2014 DCS        Fixed Bug #WIG-1545: Remove select within the insert
        _sb.AppendLine(" DECLARE @pvName AS NVARCHAR(50) ");
        _sb.AppendLine(" SET @pvName = (SELECT PV_NAME FROM PROVIDERS WHERE PV_ID = @pProviderID) ");
        _sb.AppendLine(" INSERT INTO   TERMINALS           ");
        _sb.AppendLine("             ( TE_TYPE             ");
        _sb.AppendLine(",              TE_BASE_NAME        ");
        _sb.AppendLine(",              TE_EXTERNAL_ID      ");
        _sb.AppendLine(",              TE_BLOCKED          ");
        _sb.AppendLine(",              TE_ACTIVE           ");
        _sb.AppendLine(",              TE_PROVIDER_ID      ");
        _sb.AppendLine(",              TE_BANK_ID          ");
        _sb.AppendLine(",              TE_FLOOR_ID         ");
        _sb.AppendLine(",              TE_TERMINAL_TYPE    ");
        _sb.AppendLine(",              TE_STATUS           ");
        _sb.AppendLine(",              TE_CHANGE_ID        ");
        _sb.AppendLine(",              TE_MASTER_ID        ");
        _sb.AppendLine(",              TE_ISO_CODE         ");
        _sb.AppendLine(",              TE_CREATION_DATE) ");
        _sb.AppendLine("     VALUES   (                    ");
        _sb.AppendLine("               1                   ");
        _sb.AppendLine(",              @pTerminalName      ");
        _sb.AppendLine(",              @pTerminalName      ");
        _sb.AppendLine(",              0                   ");
        _sb.AppendLine(",              1                   ");
        _sb.AppendLine(",              @pvName             ");
        _sb.AppendLine(",              @pBankId            ");
        _sb.AppendLine(",              @pFloorId           ");
        _sb.AppendLine(",              @pTerminalType      ");
        _sb.AppendLine(",              0                   ");
        _sb.AppendLine(",              0                   ");
        _sb.AppendLine(",              0                   ");
        _sb.AppendLine(",              @pIsoCode           ");
        _sb.AppendLine(",              @pDateTimeNow)      ");
        _sb.AppendLine(" SET @pTerminalId = SCOPE_IDENTITY() ");

        _sb.AppendLine("      UPDATE   TERMINALS ");
        _sb.AppendLine("         SET   TE_MASTER_ID = @pTerminalId ");
        _sb.AppendLine("       WHERE   TE_TERMINAL_ID = @pTerminalId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar).Value = TerminalName;
          _sql_cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = TerminalTypes.GAMING_TABLE_SEAT;

          if (ProviderID.HasValue)
          {
            _provider_id = ProviderID.Value;
          }
          else
          {
            Groups.GetProviderId("UNKNOWN", out _provider_id, Trx);
          }

          _sql_cmd.Parameters.Add("@pProviderId", SqlDbType.Int).Value = _provider_id;
          _sql_cmd.Parameters.Add("@pBankId", SqlDbType.Int).Value = BankId;
          _sql_cmd.Parameters.Add("@pFloorId", SqlDbType.NVarChar).Value = FloorId;
          _sql_cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = CurrencyExchange.GetNationalCurrency();
          _sql_cmd.Parameters.Add("@pDateTimeNow", SqlDbType.DateTime).Value = WGDB.Now;
          _param = _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int);
          _param.Direction = ParameterDirection.Output;

          if (_sql_cmd.ExecuteNonQuery() >= 1)
          {
            TerminalId = (Int32)_param.Value;

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // InsertTerminalTableSeat

    //------------------------------------------------------------------------------
    // PURPOSE : Set gaming table terminal blocked/active
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalName
    //          - Active
    //          - Trx
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    private static Boolean DB_VirtualTerminal_Update(Int32 TerminalId, Boolean Active, Int32? ProviderId, Int32 BankId, String FloorId, SqlTransaction Trx)
    {
      // TODO DHA:  Pasar la rutina que est� en el GUI (CLASS_CASHDESK_DRAWS_CONFIGURATION) a la common.
      //            Esta se tiene que pasar a un sitio mas comun.
      StringBuilder _sb;
      Int32 _provider_id;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE  TERMINALS ");
        _sb.AppendLine("   SET   TE_STATUS         = @pStatus        ");
        _sb.AppendLine("       , TE_BLOCKED        = @pBlocked       ");
        _sb.AppendLine("       , TE_ACTIVE         = @pActive        ");
        _sb.AppendLine("       , TE_PROVIDER_ID    = (SELECT PV_NAME FROM PROVIDERS WHERE PV_ID = @pProviderId) ");
        _sb.AppendLine("       , TE_BANK_ID        = @pBankId        ");
        _sb.AppendLine("       , TE_FLOOR_ID       = @pFloorId       ");

        if (Active)
        {
          _sb.AppendLine("        , TE_ACTIVATION_DATE = GETDATE()           ");
        }
        else
        {
          _sb.AppendLine("        , TE_RETIREMENT_DATE        = GETDATE ()   ");
          _sb.AppendLine("        , TE_RETIREMENT_REQUESTED   = GETDATE ()   ");
        }

        _sb.AppendLine(" WHERE   TE_TERMINAL_TYPE          = @pTerminalType  ");
        _sb.AppendLine("   AND   TE_TERMINAL_ID            = @pTerminalId    ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          if (Active)
          {
            _cmd.Parameters.Add("@pBlocked", SqlDbType.Bit).Value = false;
            _cmd.Parameters.Add("@pActive", SqlDbType.Bit).Value = true;
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = TerminalStatus.ACTIVE;
          }
          else
          {
            _cmd.Parameters.Add("@pBlocked", SqlDbType.Bit).Value = true;
            _cmd.Parameters.Add("@pActive", SqlDbType.Bit).Value = false;
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = TerminalStatus.RETIRED;
          }
          _cmd.Parameters.Add("@pTerminalType", SqlDbType.SmallInt).Value = TerminalTypes.GAMING_TABLE_SEAT;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;

          if (ProviderId.HasValue)
          {
            _provider_id = ProviderId.Value;
          }
          else
          {
            Groups.GetProviderId("UNKNOWN", out _provider_id, Trx);
          }

          _cmd.Parameters.Add("@pProviderId", SqlDbType.Int).Value = _provider_id;
          _cmd.Parameters.Add("@pBankId", SqlDbType.Int).Value = BankId;
          _cmd.Parameters.Add("@pFloorId", SqlDbType.NVarChar).Value = FloorId;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }

          return true;

        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // DB_VirtualTerminal_Update

    //------------------------------------------------------------------------------
    // PURPOSE : Start gaming table seat play session
    //
    //  PARAMS :
    //      - INPUT :
    //          - SeatId
    //          - AccountId
    //          - Trx
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    public static GT_RESPONSE_CODE StartGTPlaySession(Seat Seat, Int32 CashierUserId, Int64 CashierTerminalId)
    {
      StringBuilder _sb;
      PlayerTrackingMovements _PT_movements;
      Seat _seat;
      GT_RESPONSE_CODE _error_code;

      _seat = Seat;
      _error_code = GT_RESPONSE_CODE.ERROR_GENERIC;

      if(_seat.PlaySession == null || _seat.PlaySession.PlayerIsoCode == null)
      {
        _seat.PlaySession.PlayerIsoCode = CurrencyExchange.GetNationalCurrency();
      }

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("DECLARE   @pPlayedMax     AS MONEY  ");
        _sb.AppendLine("DECLARE   @pPlayedMin     AS MONEY  ");
        _sb.AppendLine("DECLARE   @pPlayedAvg     AS MONEY  ");
        _sb.AppendLine("DECLARE   @pPlayerSkill   AS INT    ");
        _sb.AppendLine("DECLARE   @pPlayerSpeed   AS INT    ");
        _sb.AppendLine("DECLARE   @pPlaySessionId AS BIGINT ");
        _sb.AppendLine("DECLARE   @IdPlaySession  AS BIGINT ");

        _sb.AppendLine(" IF NOT EXISTS ( SELECT   1 ");
        _sb.AppendLine("                  FROM   GT_SEATS");
        _sb.AppendLine("                 WHERE   GTS_SEAT_ID = @pSeatId AND GTS_CURRENT_PLAY_SESSION_ID IS NOT NULL ) ");
        _sb.AppendLine(" BEGIN ");
        //DCS 18-AUG-2014 Only recover data for Customized Account
        if (Seat.PlayerType == GTPlayerType.Customized)
        {
          _sb.AppendLine("      SELECT   @pPlayedMax = GTPS_BET_MAX           ");
          _sb.AppendLine("             , @pPlayedMin = GTPS_BET_MIN           ");
          _sb.AppendLine("             , @pPlayedAvg = GTPS_PLAYED_AVERAGE    ");
          _sb.AppendLine("             , @pPlayerSkill = GTPS_PLAYER_SKILL    ");
          _sb.AppendLine("             , @pPlayerSpeed = GTPS_PLAYER_SPEED    ");
          _sb.AppendLine("       FROM ( SELECT   MAX (GTPS_PLAY_SESSION_ID) AS GTPS_PLAY_SESSION_ID  ");
          _sb.AppendLine("                FROM   GT_PLAY_SESSIONS                                    ");
          _sb.AppendLine("               WHERE   GTPS_ACCOUNT_ID      = @pAccountId                  ");
          _sb.AppendLine("                 AND   GTPS_STATUS          = @pStatusClosed               ");
          _sb.AppendLine("                 AND   GTPS_GAMING_TABLE_ID = @pGamingTableId              ");
          _sb.AppendLine("            ) AS B                                                         ");
          _sb.AppendLine(" INNER JOIN   GT_PLAY_SESSIONS AS A                           ");
          _sb.AppendLine("         ON   A.GTPS_PLAY_SESSION_ID = B.GTPS_PLAY_SESSION_ID ");
        }

        _sb.AppendLine("INSERT INTO   GT_PLAY_SESSIONS                      ");
        _sb.AppendLine("            ( GTPS_GAMING_TABLE_ID                  ");
        _sb.AppendLine("            , GTPS_GAMING_TABLE_SESSION_ID          ");
        _sb.AppendLine("            , GTPS_STARTED                          ");
        _sb.AppendLine("            , GTPS_ACCOUNT_ID                       ");
        _sb.AppendLine("            , GTPS_SEAT_ID                          ");
        _sb.AppendLine("            , GTPS_STATUS                           ");
        _sb.AppendLine("            , GTPS_PLAYED_AMOUNT                    ");
        _sb.AppendLine("            , GTPS_BET_MAX                          ");
        _sb.AppendLine("            , GTPS_BET_MIN                          ");
        _sb.AppendLine("            , GTPS_PLAYED_AVERAGE                   ");
        _sb.AppendLine("            , GTPS_NETWIN                           ");
        _sb.AppendLine("            , GTPS_CURRENT_BET                      ");
        _sb.AppendLine("            , GTPS_TOTAL_SELL_CHIPS                 ");
        _sb.AppendLine("            , GTPS_CHIPS_OUT                        ");
        _sb.AppendLine("            , GTPS_CHIPS_IN                         ");
        _sb.AppendLine("            , GTPS_ISO_CODE                         ");
        _sb.AppendLine("            , GTPS_START_WALK                       ");
        _sb.AppendLine("            , GTPS_IN_SESSION_LAST_PLAYED_AMOUNT )  ");
        _sb.AppendLine("     VALUES ( @pGamingTableId                       ");
        _sb.AppendLine("            , @pGamingTableSessionId                ");
        _sb.AppendLine("            , GETDATE()                             ");
        _sb.AppendLine("            , @pAccountId                           ");
        _sb.AppendLine("            , @pSeatId                              ");
        _sb.AppendLine("            , CASE WHEN @pStartWalk=1 THEN @pStatusAway ELSE @pStatusOpened END ");
        _sb.AppendLine("            , 0                                     ");
        _sb.AppendLine("            , 0                                     ");
        _sb.AppendLine("            , 0                                     ");
        _sb.AppendLine("            , 0                                     ");
        _sb.AppendLine("            , 0                                     ");
        _sb.AppendLine("            , 0                                     ");
        _sb.AppendLine("            , 0                                     ");
        _sb.AppendLine("            , 0                                     ");
        _sb.AppendLine("            , 0                                     ");
        _sb.AppendLine("            , @pPlayerIsoCode                       ");
        _sb.AppendLine("            , CASE WHEN @pStartWalk=1 THEN GETDATE() ELSE NULL END ");
        _sb.AppendLine("            , 0 )                                   ");
        _sb.AppendLine("SET @IdPlaySession = SCOPE_IDENTITY() ");

        // Update Seat
        _sb.AppendLine(" UPDATE   GT_SEATS                                     ");
        _sb.AppendLine("    SET   GTS_CURRENT_PLAY_SESSION_ID = @IdPlaySession ");
        _sb.AppendLine("  WHERE   GTS_SEAT_ID = @pSeatId                       ");
        _sb.AppendLine(" END ");

        _sb.AppendLine(" SELECT   GTPS_PLAY_SESSION_ID           ");
        _sb.AppendLine("        , CAST(GTPS_TIMESTAMP AS BIGINT) ");
        _sb.AppendLine("        , @pPlayedMax                    ");
        _sb.AppendLine("        , @pPlayedMin                    ");
        _sb.AppendLine("        , @pPlayedAvg                    ");
        _sb.AppendLine("        , @pPlayerSkill                  ");
        _sb.AppendLine("        , @pPlayerSpeed                  ");
        _sb.AppendLine("        , GTPS_UPDATED                   ");
        _sb.AppendLine("        , GTPS_UPDATED_PLAYED_CURRENT    ");
        _sb.AppendLine("   FROM   GT_PLAY_SESSIONS               ");
        _sb.AppendLine("  INNER   JOIN GT_SEATS ON GTS_CURRENT_PLAY_SESSION_ID = GTPS_PLAY_SESSION_ID ");
        _sb.AppendLine("  WHERE   GTS_SEAT_ID = @pSeatId ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = _seat.AccountId;
            _cmd.Parameters.Add("@pStatusOpened", SqlDbType.Int).Value = GTPlaySessionStatus.Opened;
            _cmd.Parameters.Add("@pStatusAway", SqlDbType.Int).Value = GTPlaySessionStatus.Away;
            _cmd.Parameters.Add("@pStatusClosed", SqlDbType.Int).Value = GTPlaySessionStatus.Closed;
            _cmd.Parameters.Add("@pSeatId", SqlDbType.Int).Value = _seat.SeatId;
            _cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = _seat.GamingTableId;
            _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = _seat.GamingTableSessionId;
            _cmd.Parameters.Add("@pStartWalk", SqlDbType.Bit).Value = _seat.PlaySession.Started_walk == null ? false : true;
            _cmd.Parameters.Add("@pPlayerIsoCode", SqlDbType.NVarChar).Value = _seat.PlaySession.PlayerIsoCode;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                _error_code = GT_RESPONSE_CODE.ERROR_GENERIC;

                return _error_code;
              }
              _seat.PlaySession.PlaySessionId = _reader.GetInt64(0);
              _seat.PlaySession.AccountId = _seat.AccountId;
              _seat.PlaySession.Started = WGDB.Now;
              _seat.PlaySession.Sequence = _reader.GetInt64(1);
              _seat.PlaySession.PlayedAvg = _reader.IsDBNull(4) ? 0 : _reader.GetDecimal(4);
              _seat.PlaySession.PlayerSkill = _reader.IsDBNull(5) ? GTPlaySessionPlayerSkill.Average : (GTPlaySessionPlayerSkill)_reader.GetInt32(5);
              _seat.PlaySession.PlayerSpeed = _reader.IsDBNull(6) ? GTPlayerTrackingSpeed.Medium : (GTPlayerTrackingSpeed)_reader.GetInt32(6);
              _seat.PlaySession.Updated = _reader.GetDateTime(7);
              _seat.PlaySession.UpdatedPlayedCurrent = _reader.GetDateTime(8);
              _seat.PlaySession.PlayerIsoCode = _seat.PlaySession.PlayerIsoCode;
            }
          }

          // Add movements
          _PT_movements = new PlayerTrackingMovements(CashierUserId, CashierTerminalId, _seat.GamingTableId, Seat.GamingTableSessionId);

          _PT_movements.Add(PlayerTrackingMovementType.PlayerIn, _seat.SeatId, _seat.PlaySession.PlaySessionId, _seat.VirtualTerminalId, _seat.AccountId, 0, 0, "", null, null);

          if (!_PT_movements.Save(_db_trx.SqlTransaction))
          {
            return _error_code;
          }

          _db_trx.Commit();

          Seat = _seat;

          _error_code = GT_RESPONSE_CODE.OK;

          return _error_code;
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _error_code;
    } // StartGTPlaySession

    //------------------------------------------------------------------------------
    // PURPOSE : Update gaming table seat play session
    //
    //  PARAMS :
    //      - INPUT :
    //          - Seat object
    //          - Int32 UserId
    //          - Int64 CashierId
    //          - Trx
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    public static GT_RESPONSE_CODE UpdateGTPlaySession(Seat Seat, Int32 CashierUserId, Int64 CashierTerminalId)
    {
      StringBuilder _sb;
      DateTime _updated;
      DateTime _updated_played_current;
      Int64 _sequence;
      GT_RESPONSE_CODE _error_code;

      _error_code = GT_RESPONSE_CODE.ERROR_GENERIC;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   GT_PLAY_SESSIONS                               ");
        _sb.AppendLine("    SET   GTPS_BET_MAX                = @pPlayedMax      ");
        _sb.AppendLine("        , GTPS_BET_MIN                = @pPlayedMin      ");
        _sb.AppendLine("        , GTPS_NETWIN                 = @pNetWin         ");
        _sb.AppendLine("        , GTPS_PLAYER_SKILL           = @pPlayerSkill    ");
        _sb.AppendLine("        , GTPS_PLAYER_SPEED           = @pPlayerSpeed    ");
        _sb.AppendLine("        , GTPS_POINTS                 = @pPoints         ");
        _sb.AppendLine("        , GTPS_COMPUTED_POINTS        = @pComputedPoints ");
        _sb.AppendLine("        , GTPS_GAME_TIME              = @pGameTime       ");
        _sb.AppendLine("        , GTPS_UPDATED_USER_ID        = @pUserId         ");
        _sb.AppendLine("        , GTPS_CURRENT_BET            = @pCurrentBet     ");
        _sb.AppendLine("        , GTPS_CHIPS_OUT              = @pChipsOut       ");
        _sb.AppendLine("        , GTPS_CHIPS_IN               = @pChipsIn        ");
        _sb.AppendLine("        , GTPS_IN_SESSION_LAST_PLAYED_AMOUNT = @pLastPlayedAmount ");
        _sb.AppendLine("        , GTPS_IN_SESSION_LAST_PLAY   = @pLastPlaysCount ");
        _sb.AppendLine("        , GTPS_UPDATED                = GETDATE()        ");
        _sb.AppendLine("        , GTPS_UPDATED_PLAYED_CURRENT = (CASE WHEN GTPS_CURRENT_BET <> @pCurrentBet THEN GETDATE() ELSE GTPS_UPDATED_PLAYED_CURRENT END ) ");
        _sb.AppendLine("        , GTPS_ISO_CODE               = @pPlayerIsoCode  ");

        _sb.AppendLine(" OUTPUT   INSERTED.GTPS_UPDATED                          ");
        _sb.AppendLine("        , INSERTED.GTPS_UPDATED_PLAYED_CURRENT           ");
        _sb.AppendLine("        , CAST(INSERTED.GTPS_TIMESTAMP AS BIGINT)        ");
        _sb.AppendLine("  WHERE   GTPS_PLAY_SESSION_ID        = @pPlaySessionId  ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pFinished", SqlDbType.DateTime).Value = WGDB.Now;
            _cmd.Parameters.Add("@pPlayedMax", SqlDbType.Money).Value = Seat.PlaySession.PlayedMax.SqlMoney;
            _cmd.Parameters.Add("@pPlayedMin", SqlDbType.Money).Value = Seat.PlaySession.PlayedMin.SqlMoney;
            _cmd.Parameters.Add("@pNetWin", SqlDbType.Money).Value = Seat.PlaySession.NetWin.SqlMoney;
            _cmd.Parameters.Add("@pPlayerSkill", SqlDbType.Int).Value = Seat.PlaySession.PlayerSkill;
            _cmd.Parameters.Add("@pPlayerSpeed", SqlDbType.Int).Value = Seat.PlaySession.PlayerSpeed;
            _cmd.Parameters.Add("@pPoints", SqlDbType.Money).Value = Seat.PlaySession.AwardedPoints.SqlMoney;
            _cmd.Parameters.Add("@pComputedPoints", SqlDbType.Money).Value = Seat.PlaySession.AwardedPointsCalculated.SqlMoney;
            _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = Seat.PlaySession.PlaySessionId;
            _cmd.Parameters.Add("@pSequence", SqlDbType.BigInt).Value = Seat.PlaySession.Sequence;
            _cmd.Parameters.Add("@pGameTime", SqlDbType.BigInt).Value = Seat.PlaySession.GameTime;
            _cmd.Parameters.Add("@pCurrentBet", SqlDbType.Money).Value = Seat.PlaySession.CurrentBet.SqlMoney;
            _cmd.Parameters.Add("@pChipsOut", SqlDbType.Money).Value = Seat.PlaySession.ChipsOut.SqlMoney;
            _cmd.Parameters.Add("@pChipsIn", SqlDbType.Money).Value = Seat.PlaySession.ChipsIn.SqlMoney;
            _cmd.Parameters.Add("@pLastPlayedAmount", SqlDbType.Money).Value = Seat.PlaySession.LastPlayedAmount.SqlMoney;
            _cmd.Parameters.Add("@pLastPlaysCount", SqlDbType.Int).Value = Seat.PlaySession.LastPlaysCount;
            _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = CashierUserId;
            _cmd.Parameters.Add("@pPlayerIsoCode", SqlDbType.NVarChar).Value = Seat.PlaySession.PlayerIsoCode;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                _error_code = GT_RESPONSE_CODE.ERROR_GENERIC;

                return _error_code;
              }

              _updated = _reader.GetDateTime(0);
              _updated_played_current = _reader.GetDateTime(1);
              _sequence = _reader.GetInt64(2);
            }

            if (!AddLastMovemets(Seat, CashierUserId, CashierTerminalId, _db_trx.SqlTransaction))
            {
              return _error_code;
            }

            if (!MultiPromos.Trx_SetActivity(Seat.PlaySession.AccountId, _db_trx.SqlTransaction))
            {
              return _error_code;
            }

            _error_code = GT_RESPONSE_CODE.OK;

            _db_trx.Commit();

            Seat.PlaySession.Updated = _updated;
            Seat.PlaySession.UpdatedPlayedCurrent = _updated_played_current;
            Seat.PlaySession.Sequence = _sequence;

            return _error_code;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _error_code;
    } // UpdateGTPlaySession

    //------------------------------------------------------------------------------
    // PURPOSE : End gaming table seat play session
    //
    //  PARAMS :
    //      - INPUT :
    //          - Seat object
    //          - Int32 UserId
    //          - Int64 CashierId
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    public static GT_RESPONSE_CODE EndGTPlaySession(Seat Seat, Int32 CashierUserId, Int64 CashierTerminalId, String CashierTerminalName)
    {
      GT_RESPONSE_CODE _error_code;

      try
      {
        _error_code = GT_RESPONSE_CODE.ERROR_GENERIC;

        using (DB_TRX _db_trx = new DB_TRX())
        {
          _error_code = EndGTPlaySession(Seat, CashierUserId, CashierTerminalId, CashierTerminalName, _db_trx.SqlTransaction);

          if (_error_code == GT_RESPONSE_CODE.OK)
          {
            _db_trx.Commit();
          }
        }//DB_TRX
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        _error_code = GT_RESPONSE_CODE.ERROR_GENERIC;
      }

      return _error_code;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : End gaming table seat play session
    //
    //  PARAMS :
    //      - INPUT :
    //          - Seat object
    //          - Int32 UserId
    //          - Int64 CashierId
    //          - Trx
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    public static GT_RESPONSE_CODE EndGTPlaySession(Seat Seat, Int32 CashierUserId, Int64 CashierTerminalId, String CashierTerminalName, SqlTransaction Trx) // 
    {
      StringBuilder _sb;
      Decimal _computed_points;
      PlayerTrackingMovements _PT_movements;
      Int64 _sequence;
      DateTime _updated;
      GT_RESPONSE_CODE _error_code;
      GamingTable _gaming_table;
      WonBucketDict _bucketList;

      _computed_points = 0;
      _error_code = GT_RESPONSE_CODE.ERROR_GENERIC;
      _bucketList = new WonBucketDict();

      try
      {
        if (Seat.PlayerType == GTPlayerType.AnonymousWithCard || Seat.PlayerType == GTPlayerType.Customized)
        {
          if (Get_CalculateBuckets(Seat, ref _bucketList) == GT_RESPONSE_CODE.ERROR_GENERIC)
          {
            return _error_code;
          }

          if (_bucketList.Dict.ContainsKey(Buckets.BucketId.RedemptionPoints))
          {
            _computed_points = _bucketList.Dict[Buckets.BucketId.RedemptionPoints].WonValue;
          }
        }

        if (Seat.PlayerType == GTPlayerType.Customized)
        {
          Seat.PlaySession.AwardedPointsCalculated = _computed_points;
        }
        else
        {
          Seat.PlaySession.AwardedPointsCalculated = 0;
        }        

        if (!Seat.PlaySession.Started_walk.HasValue)
        {
          // Only in case close session by thread. From interface can not close session without be away
          // In this case, points are calculated
          Seat.PlaySession.AwardedPoints = _computed_points;
        }
        _sb = new StringBuilder();

        _sb.AppendLine(" UPDATE   GT_PLAY_SESSIONS                               ");

        _sb.AppendLine("    SET   GTPS_FINISHED               = @pFinished       ");
        _sb.AppendLine("        , GTPS_STATUS                 = @pStatus         ");
        _sb.AppendLine("        , GTPS_START_WALK             = @pStartWalk      ");
        _sb.AppendLine("        , GTPS_WALK                   = @pWalk           ");
        _sb.AppendLine("        , GTPS_GAME_TIME              = @pGameTime       ");
        _sb.AppendLine("        , GTPS_FINISHED_USER_ID       = @pUserId         ");
        _sb.AppendLine("        , GTPS_POINTS                 = @pPoints         ");
        _sb.AppendLine("        , GTPS_COMPUTED_POINTS        = @pComputedPoints ");

        _sb.AppendLine("        , GTPS_NETWIN                 = @pNetWin         ");
        _sb.AppendLine("        , GTPS_UPDATED_USER_ID        = @pUserId         ");
        _sb.AppendLine("        , GTPS_CURRENT_BET            = @pCurrentBet     ");
        _sb.AppendLine("        , GTPS_PLAYER_SKILL           = @pPlayerSkill    ");
        _sb.AppendLine("        , GTPS_PLAYER_SPEED           = @pPlayerSpeed    ");
        _sb.AppendLine("        , GTPS_CHIPS_OUT              = @pChipsOut       ");
        _sb.AppendLine("        , GTPS_CHIPS_IN               = @pChipsIn        ");
        _sb.AppendLine("        , GTPS_IN_SESSION_LAST_PLAYED_AMOUNT = @pLastPlayedAmount ");
        _sb.AppendLine("        , GTPS_IN_SESSION_LAST_PLAY   = @pLastPlaysCount ");
        _sb.AppendLine("        , GTPS_UPDATED                = GETDATE()        ");
        _sb.AppendLine("        , GTPS_UPDATED_PLAYED_CURRENT = (CASE WHEN GTPS_CURRENT_BET <> @pCurrentBet THEN GETDATE() ELSE GTPS_UPDATED_PLAYED_CURRENT END ) ");
        _sb.AppendLine("        , GTPS_ISO_CODE               = @pIsoCode        ");

        _sb.AppendLine(" OUTPUT   CAST(INSERTED.GTPS_TIMESTAMP AS BIGINT) 'Sequence' ");
        _sb.AppendLine("        , INSERTED.GTPS_UPDATED ");

        _sb.AppendLine("  WHERE   GTPS_PLAY_SESSION_ID        = @pPlaySessionId  ");

        // Update Seat
        _sb.AppendLine(" UPDATE   GT_SEATS                      ");
        _sb.AppendLine("    SET   GTS_CURRENT_PLAY_SESSION_ID = NULL ");
        _sb.AppendLine("  WHERE   GTS_SEAT_ID = @pSeatId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = GTPlaySessionStatus.Closed;

          if (Seat.PlaySession.Started_walk == null)
          {
            _cmd.Parameters.Add("@pStartWalk", SqlDbType.DateTime).Value = DBNull.Value;
            _cmd.Parameters.Add("@pFinished", SqlDbType.DateTime).Value = WGDB.Now;
          }
          else
          {
            _cmd.Parameters.Add("@pStartWalk", SqlDbType.DateTime).Value = Seat.PlaySession.Started_walk.Value;
            _cmd.Parameters.Add("@pFinished", SqlDbType.DateTime).Value = Seat.PlaySession.Started_walk.Value;
          }

          _cmd.Parameters.Add("@pWalk", SqlDbType.BigInt).Value = Seat.PlaySession.Walk;
          _cmd.Parameters.Add("@pGameTime", SqlDbType.BigInt).Value = Seat.PlaySession.GameTime;
          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = CashierUserId;
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = Seat.PlaySession.PlaySessionId;
          _cmd.Parameters.Add("@pSequence", SqlDbType.BigInt).Value = Seat.PlaySession.Sequence;

          _cmd.Parameters.Add("@pPoints", SqlDbType.Money).Value = Seat.PlaySession.AwardedPoints.SqlMoney;
          _cmd.Parameters.Add("@pComputedPoints", SqlDbType.Money).Value = Seat.PlaySession.AwardedPointsCalculated.SqlMoney;

          _cmd.Parameters.Add("@pNetWin", SqlDbType.Money).Value = Seat.PlaySession.NetWin.SqlMoney;
          _cmd.Parameters.Add("@pPlayerSkill", SqlDbType.Int).Value = Seat.PlaySession.PlayerSkill;
          _cmd.Parameters.Add("@pPlayerSpeed", SqlDbType.Int).Value = Seat.PlaySession.PlayerSpeed;
          _cmd.Parameters.Add("@pCurrentBet", SqlDbType.Money).Value = Seat.PlaySession.CurrentBet.SqlMoney;
          _cmd.Parameters.Add("@pChipsOut", SqlDbType.Money).Value = Seat.PlaySession.ChipsOut.SqlMoney;
          _cmd.Parameters.Add("@pChipsIn", SqlDbType.Money).Value = Seat.PlaySession.ChipsIn.SqlMoney;
          _cmd.Parameters.Add("@pLastPlayedAmount", SqlDbType.Money).Value = Seat.PlaySession.LastPlayedAmount.SqlMoney;
          _cmd.Parameters.Add("@pLastPlaysCount", SqlDbType.Int).Value = Seat.PlaySession.LastPlaysCount;
          _cmd.Parameters.Add("@pSeatId", SqlDbType.Int).Value = Seat.SeatId;
          _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = Seat.PlaySession.PlayerIsoCode;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              Log.Error("End gt_play_session failed.");
              _error_code = GT_RESPONSE_CODE.ERROR_GENERIC;

              return _error_code;
            }

            _sequence = _reader.GetInt64(0);
            _updated = _reader.GetDateTime(1);
          }

          _PT_movements = new PlayerTrackingMovements(CashierUserId, CashierTerminalId, Seat.GamingTableId, Seat.GamingTableSessionId);
          _PT_movements.Add(PlayerTrackingMovementType.PlayerOut, Seat.SeatId, Seat.PlaySession.PlaySessionId, Seat.VirtualTerminalId, Seat.AccountId, 0, 0, "", null, null);

          // Add points awared movement
          if (Seat.PlaySession.AwardedPoints != 0)
          {
            _PT_movements.Add(PlayerTrackingMovementType.PointsWon, Seat.SeatId, Seat.PlaySession.PlaySessionId, Seat.VirtualTerminalId, Seat.AccountId,
                               Seat.Points, Seat.Points + Seat.PlaySession.AwardedPoints, "", null, null);
          }

          if (!_PT_movements.Save(Trx))
          {
            Log.Error("PlayerTrackingMovements.Save failed!");
            return _error_code;
          }

          if (Seat.PlaySession.AwardedPoints != 0 && (Seat.PlayerType == GTPlayerType.AnonymousWithCard || Seat.PlayerType == GTPlayerType.Customized))
          {
            if (!ReadTable(Seat.GamingTableId, out _gaming_table))
            {
              return _error_code;
            }

            foreach (KeyValuePair<Buckets.BucketId, WonBucket> _key_value in _bucketList.Dict)
            {
              _key_value.Value.AccountId = Seat.AccountId;
              _key_value.Value.TerminalId = Seat.VirtualTerminalId;
              _key_value.Value.WonValue = Seat.PlaySession.AwardedPoints;
              _key_value.Value.TerminalName = WSI.Common.CommonCashierInformation.AuthorizedByUserName + "@" + _gaming_table.GamingTableName;
            }


            if (!UpdateAccountBuckets(Seat.AccountId
                                    , WSI.Common.CommonCashierInformation.AuthorizedByUserName + "/" + _gaming_table.GamingTableName + " - " + Resource.String("STR_GTPS_SEAT", Seat.SeatPosition)
                                    , _bucketList
                                    , Trx))
            {
              return _error_code;
            }
          }

          Seat.PlaySession.Sequence = _sequence;
          Seat.PlaySession.Updated = _updated;

          if (RestartTableCounters(Seat.GamingTableId, true, Trx) == GT_RESPONSE_CODE.ERROR_GENERIC)
          {
            return _error_code;
          }

          if (MultiPromos.Trx_SetActivity(Seat.PlaySession.AccountId, Trx))
          {
            return GT_RESPONSE_CODE.OK;
          }

          return _error_code;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _error_code;
    } // EndGTPlaySession

    // PURPOSE: Updates Account
    //
    //  PARAMS:
    //     - INPUT :
    //         AccountId
    //         AccountName
    //         AwardedPoints
    //         NegativeAcountsList
    //         TerminalNameDoChange
    //         SqlTransaction
    //
    //     - OUTPUT :
    //
    // RETURNS:
    //
    public static Boolean UpdateAccountPoints(Int64 AccountId,
                                             String AccountName,
                                            Decimal PointsUpdate,
                                       List<String> NegativeAccounts,
                                             String TerminalName,
                                             String Details,
                                     SqlTransaction SqlTrans)
    {
      return UpdateAccountPoints(0,
                                 AccountId,
                                 AccountName,
                                 PointsUpdate,
                                 NegativeAccounts,
                                 TerminalName,
                                 Details,
                                 MovementType.ManuallyAddedPointsForLevel,
                                 SqlTrans);
    }

    // PURPOSE: Updates Account
    //
    //  PARAMS:
    //     - INPUT :
    //         OperationId
    //         AccountId
    //         AccountName
    //         AwardedPoints
    //         NegativeAcountsList
    //         TerminalNameDoChange
    //         SqlTransaction
    //
    //     - OUTPUT :
    //
    // RETURNS:
    //
    public static Boolean UpdateAccountPoints(Int64 OperationId,
                                              Int64 AccountId,
                                             String AccountName,
                                            Decimal PointsUpdate,
                                       List<String> NegativeAccounts,
                                             String TerminalName,
                                             String Details,
                                       MovementType TypeMovement,
                                     SqlTransaction SqlTrans)
    {
      AccountMovementsTable _am_table;
      Decimal _fin_points;
      CashierSessionInfo _cashier_info;
      Decimal _ini_points_for_level;

      // Create Table
      _cashier_info = new CashierSessionInfo();
      if (String.IsNullOrEmpty(TerminalName))
      {
        _cashier_info.TerminalName = "@" + Environment.MachineName;
      }
      else
      {
        _cashier_info.TerminalName = TerminalName;
      }

      // Lock Account
      if (!AccountImportPoints.DB_LockAccount(AccountId, SqlTrans))
      {
        return false;
      }

      //points for level
      if (!AccountImportPoints.DB_GetAccountBucket(AccountId, Buckets.BucketId.RankingLevelPoints, out _ini_points_for_level, SqlTrans))
      {
        throw new Exception();
      }

      // Update account
      if (!WSI.Common.MultiPromos.Trx_UpdateAccountPoints(AccountId, PointsUpdate, true, out _fin_points, SqlTrans))
      {
        return false;
      }

      // Account has activity
      if (!WSI.Common.MultiPromos.Trx_SetActivity(AccountId, SqlTrans))
      {
        return false;
      }

      _am_table = new AccountMovementsTable(_cashier_info);

      // Add movement
      if (!_am_table.Add(OperationId, AccountId, TypeMovement, _ini_points_for_level, 0, PointsUpdate, _fin_points, Details))
      {
        return false;
      }

      // Save
      if (!_am_table.Save(SqlTrans))
      {
        return false;
      }

      String _account;
      _account = AccountId.ToString() + " - " + AccountName;

      if (_fin_points < 0 && NegativeAccounts != null)
      {
        if (!NegativeAccounts.Contains(_account))
        {
          NegativeAccounts.Add(_account);
        }
        else
        {
          if (NegativeAccounts.Contains(_account))
          {
            NegativeAccounts.Remove(_account);
          }
        }
      }

      return true;

    } // UpdateAccount

    // PURPOSE: Updates Account Buckets
    //
    //  PARAMS:
    //     - INPUT :
    //         AccountId
    //         TerminalName
    //         WonBuckets
    //         SqlTrans
    //
    //     - OUTPUT :
    //
    // RETURNS:
    //
    public static Boolean UpdateAccountBuckets(Int64 AccountId,
                                              String TerminalName,
                                       WonBucketDict WonBuckets,
                                      SqlTransaction SqlTrans)
    {
      DataTable _dt_buckets;

      if (WonBuckets == null)
      {
        return false;
      }

      //AccountMovementsTable _am_table;
      CashierSessionInfo _cashier_info;

      // Create Table
      _cashier_info = new CashierSessionInfo();
      if (String.IsNullOrEmpty(TerminalName))
      {
        _cashier_info.TerminalName = "@" + Environment.MachineName;
      }
      else
      {
        _cashier_info.TerminalName = TerminalName;
      }

      // Update Buckets
      _dt_buckets = BucketsUpdate.FromBucketDictToTable(WonBuckets);

      if (!BucketsUpdate.BatchUpdateBuckets(_dt_buckets, SqlTrans))
      {
        return false;
      }

      if (!BucketsUpdate.CreateMovements(OperationCode.MULTIPLE_BUCKETS_ENDSESSION, MovementType.MULTIPLE_BUCKETS_EndSession, CASHIER_MOVEMENT.MULTIPLE_BUCKETS_END_SESSION, _dt_buckets, _cashier_info, SqlTrans))
      {
        return false;
      }

      // Account has activity
      if (!WSI.Common.MultiPromos.Trx_SetActivity(AccountId, SqlTrans))
      {
        return false;
      }

      return true;

    } // UpdateAccount

    //------------------------------------------------------------------------------
    // PURPOSE: get points calculates for this player
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public static GT_RESPONSE_CODE Get_CalculateBuckets(Seat Seat, ref WonBucketDict WonBucketList)
    {
      String _national_currency;
      StringBuilder _sb;
      Object _obj;
      Currency _currency_exchange;
      Currency _played_amount;
      GT_RESPONSE_CODE _response_code;

      _national_currency = CurrencyExchange.GetNationalCurrency();
      _sb = new StringBuilder();
      _currency_exchange = 0;
      _played_amount = Seat.PlaySession.PlayedAmount;
      _response_code = GT_RESPONSE_CODE.ERROR_GENERIC;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          if (Seat.PlaySession.PlayerIsoCode != _national_currency)
          {
            _sb.AppendLine(" SELECT   CE_CHANGE                        ");
            _sb.AppendLine("   FROM   CURRENCY_EXCHANGE                ");
            _sb.AppendLine("  WHERE   CE_CURRENCY_ISO_CODE = @pIsoCode ");
            _sb.AppendLine("    AND   CE_TYPE = 0                      ");

            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _sql_cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = Seat.PlaySession.PlayerIsoCode;
              _obj = _sql_cmd.ExecuteScalar();
            }

            if (_obj != null && _obj != DBNull.Value)
            {
              _currency_exchange = (Decimal)_obj;
            }

            if (_currency_exchange != 0)
            {
              _played_amount *= _currency_exchange;
            }
          }

          //PBI 33790: Seat.PlaySession.PlaySessionId used instead of 0 so that Bucket Multiplier can be located correctly
          _response_code = Get_CalculateBuckets(Seat.VirtualTerminalId, Seat.AccountId, _played_amount,
                                                Seat.TheoricHold, Seat.PlaySession.PlaySessionId,
                                   ref WonBucketList, _db_trx.SqlTransaction);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _response_code;

    }  // Get_CalculateBuckets

    //------------------------------------------------------------------------------
    // PURPOSE: get points calculates for this player
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //
    public static GT_RESPONSE_CODE Get_CalculateBuckets(Int64 VirtualTerminalId, Int64 AccountId, Currency PlayedAmount, Decimal TheoricHold, Int64 PlaySessionId, ref WonBucketDict WonBucketList)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return Get_CalculateBuckets(VirtualTerminalId, AccountId, PlayedAmount, TheoricHold, PlaySessionId,
                                   ref WonBucketList, _db_trx.SqlTransaction);
      }
    }

    public static GT_RESPONSE_CODE Get_CalculateBuckets(Int64 VirtualTerminalId, Int64 AccountId, Currency PlayedAmount, Decimal TheoricHold, Int64 PlaySessionId, ref WonBucketDict WonBucketList, SqlTransaction Trx)
    {
      Terminal.TerminalInfo _terminal_info;
      Decimal _theoretica_payout;

      _theoretica_payout = 1 - TheoricHold;

      if (!Terminal.Trx_GetTerminalInfo((Int32)VirtualTerminalId, out _terminal_info, Trx))
      {
        return GT_RESPONSE_CODE.ERROR_GENERIC;
      }

      if (!BucketsUpdate.ComputeWonBuckets(AccountId, _terminal_info, PlayedAmount, _theoretica_payout, PlaySessionId, ref WonBucketList, Trx))
      {
        Log.Error("WCP_BucketsUpdate.ComputeWonBuckets failed.");

        return GT_RESPONSE_CODE.ERROR_GENERIC;
      }

      return GT_RESPONSE_CODE.OK;
    }  // Get_CalculatePoints

    //------------------------------------------------------------------------------
    // PURPOSE : set customer is out of gaming table seat
    //
    //  PARAMS :
    //      - INPUT :
    //          - SeatId
    //          - AccountId
    //          - Trx
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    public static GT_RESPONSE_CODE UpdateSeatWalk(Seat Seat, Int32 CashierUserId, Int64 CashierTerminalId)
    {
      StringBuilder _sb;
      PlayerTrackingMovements _PT_movements;
      Int64 _sequence;
      DateTime _updated;
      GT_RESPONSE_CODE _error_code;
      Int64 _old_walk;
      PlayerTrackingMovementType _pt_movement;
      Decimal _computed_points;
      WonBucketDict _computed_buckets = new WonBucketDict();

      _computed_points = 0;
      _error_code = GT_RESPONSE_CODE.ERROR_GENERIC;

      if (Get_CalculateBuckets(Seat, ref _computed_buckets) == GT_RESPONSE_CODE.ERROR_GENERIC)
      {
        return _error_code;
      }

      if (_computed_buckets.Dict.ContainsKey(Buckets.BucketId.RedemptionPoints))
      {
        _computed_points = _computed_buckets.Dict[Buckets.BucketId.RedemptionPoints].WonValue;
      }

      if(Seat.PlayerType == GTPlayerType.Customized)
      {
        Seat.PlaySession.AwardedPointsCalculated = _computed_points;
      }
      else
      {
        Seat.PlaySession.AwardedPointsCalculated = 0;
      }      

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("IF EXISTS ( SELECT   *                                                                      ");
        _sb.AppendLine("              FROM   GT_PLAY_SESSIONS                                                       ");
        _sb.AppendLine("             WHERE   GTPS_PLAY_SESSION_ID = @pPlaySessionId                                 ");
        _sb.AppendLine("               AND   GTPS_START_WALK IS NULL )                                              ");
        // client walk
        _sb.AppendLine("  BEGIN                                                                                     ");
        _sb.AppendLine("    UPDATE   GT_PLAY_SESSIONS                                                               ");
        _sb.AppendLine("       SET   GTPS_START_WALK = GETDATE()                                                    ");
        _sb.AppendLine("           , GTPS_GAME_TIME = @pGameTime                                                    ");
        _sb.AppendLine("           , GTPS_STATUS = @pStatusAway                                                     ");
        _sb.AppendLine("           , GTPS_UPDATED = GETDATE()                                                       ");
        _sb.AppendLine("           , GTPS_POINTS = @pAwardedPoints                                                  ");
        _sb.AppendLine("           , GTPS_COMPUTED_POINTS = @pComputedPoints                                        ");
        _sb.AppendLine("    OUTPUT   INSERTED.GTPS_START_WALK                                                       ");
        _sb.AppendLine("           , ISNULL(INSERTED.GTPS_WALK, 0)                                                  ");
        _sb.AppendLine("           , CAST(INSERTED.GTPS_TIMESTAMP AS BIGINT)                                        ");
        _sb.AppendLine("           , INSERTED.GTPS_UPDATED                                                          ");
        _sb.AppendLine("     WHERE   GTPS_PLAY_SESSION_ID = @pPlaySessionId                                         ");
        _sb.AppendLine("       AND   GTPS_STATUS <> @pStatusClose                                                   ");
        _sb.AppendLine("  END                                                                                       ");
        _sb.AppendLine("ELSE                                                                                        ");
        // client running
        _sb.AppendLine("  BEGIN                                                                                     ");
        _sb.AppendLine("    UPDATE   GT_PLAY_SESSIONS                                                               ");
        _sb.AppendLine("       SET   GTPS_WALK = ISNULL(GTPS_WALK, 0) + DATEDIFF(second,GTPS_START_WALK,GETDATE())  ");
        _sb.AppendLine("           , GTPS_START_WALK = NULL                                                         ");
        _sb.AppendLine("           , GTPS_GAME_TIME = @pGameTime                                                    ");
        _sb.AppendLine("           , GTPS_STATUS = @pStatusOpen                                                     ");
        _sb.AppendLine("           , GTPS_UPDATED = GETDATE()                                                       ");
        _sb.AppendLine("           , GTPS_IN_SESSION_PLAYS_TABLE = GT_PLAYS_COUNT - ISNULL(GTPS_PLAYS,0)            ");
        _sb.AppendLine("           , GTPS_POINTS = @pAwardedPoints                                                  ");
        _sb.AppendLine("           , GTPS_COMPUTED_POINTS = @pComputedPoints                                        ");
        _sb.AppendLine("    OUTPUT   INSERTED.GTPS_START_WALK                                                       ");
        _sb.AppendLine("           , ISNULL(INSERTED.GTPS_WALK, 0)                                                  ");
        _sb.AppendLine("           , CAST(INSERTED.GTPS_TIMESTAMP AS BIGINT)                                        ");
        _sb.AppendLine("           , INSERTED.GTPS_UPDATED                                                          ");
        _sb.AppendLine("      FROM   GT_PLAY_SESSIONS GTPS                                                          ");
        _sb.AppendLine("     INNER   JOIN  GAMING_TABLES ON  GTPS_GAMING_TABLE_ID = GT_GAMING_TABLE_ID              ");
        _sb.AppendLine("     WHERE   GTPS_PLAY_SESSION_ID = @pPlaySessionId                                         ");
        _sb.AppendLine("       AND   GTPS_STATUS <> @pStatusClose                                                   ");
        _sb.AppendLine("  END                                                                                       ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = Seat.PlaySession.PlaySessionId;
            _cmd.Parameters.Add("@pSequence", SqlDbType.BigInt).Value = Seat.PlaySession.Sequence;
            _cmd.Parameters.Add("@pGameTime", SqlDbType.BigInt).Value = Seat.PlaySession.GameTime;
            _cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = GTPlaySessionStatus.Opened;
            _cmd.Parameters.Add("@pStatusAway", SqlDbType.Int).Value = GTPlaySessionStatus.Away;
            _cmd.Parameters.Add("@pStatusClose", SqlDbType.Int).Value = GTPlaySessionStatus.Closed;
            // If seat change status from play to pause awarded points is computed points
            if (!Seat.PlaySession.Started_walk.HasValue)
            {
              _cmd.Parameters.Add("@pAwardedPoints", SqlDbType.Money).Value = Seat.PlaySession.AwardedPointsCalculated.SqlMoney;
            }
            else
            {
              _cmd.Parameters.Add("@pAwardedPoints", SqlDbType.Money).Value = Seat.PlaySession.AwardedPoints.SqlMoney;
            }
            _cmd.Parameters.Add("@pComputedPoints", SqlDbType.Money).Value = Seat.PlaySession.AwardedPointsCalculated.SqlMoney;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                Log.Warning("Error reading UpdateSeatWalk");

                _error_code = GT_RESPONSE_CODE.ERROR_GENERIC;

                return _error_code;
              }

              _sequence = _reader.GetInt64(2);
              _updated = _reader.GetDateTime(3);

              _PT_movements = new PlayerTrackingMovements(CashierUserId, CashierTerminalId, Seat.GamingTableId, Seat.GamingTableSessionId);

              if (!_reader.IsDBNull(0))
              {
                // in pause
                _pt_movement = PlayerTrackingMovementType.PausePlay;
                Seat.PlaySession.Started_walk = _reader.GetDateTime(0);
                _old_walk = _reader.GetInt64(1);

              }
              else
              {
                // continue
                _pt_movement = PlayerTrackingMovementType.ContinuePlay;
                Seat.PlaySession.Started_walk = null;
                _old_walk = Seat.PlaySession.Walk;

              }
              Seat.PlaySession.Walk = _reader.GetInt64(1);
            }

            _PT_movements.Add(_pt_movement, Seat.SeatId, Seat.PlaySession.PlaySessionId, Seat.VirtualTerminalId, Seat.AccountId, _old_walk, Seat.PlaySession.Walk, Seat.PlaySession.PlayerIsoCode, null, null);

            if (!_PT_movements.Save(_db_trx.SqlTransaction))
            {
              return _error_code;
            }

            _db_trx.Commit();

            Seat.PlaySession.Sequence = _sequence;
            Seat.PlaySession.Updated = _updated;
            _error_code = GT_RESPONSE_CODE.OK;

            return _error_code;

          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _error_code;

    } // UpdateSeatWalk

    //------------------------------------------------------------------------------
    // PURPOSE : Swap players seat
    //
    //  PARAMS :
    //      - INPUT :
    //          - Two seat objects
    //      - OUTPUT :
    //
    // RETURNS :  
    //
    //   NOTES :
    public static GT_RESPONSE_CODE SwapSeats(Seat OneSeat, Seat OtherSeat, Int32 CashierUserId, Int64 CashierTerminalId)
    {
      Int64 _old_one_seat_id;
      StringBuilder _sb;
      SqlParameter _param_seat_id;
      SqlParameter _param_player_type;
      SqlParameter _param_play_session_id;
      SqlParameter _param_play_session_status;
      Int64 _sequence_one_seat;
      Int64 _sequence_other_seat;
      DateTime? _updated_one_seat;
      DateTime? _updated_other_seat;
      GT_RESPONSE_CODE _error_code;
      PlayerTrackingMovements _PT_movements;

      _error_code = GT_RESPONSE_CODE.ERROR_GENERIC;
      _sequence_one_seat = 0;
      _sequence_other_seat = 0;
      _updated_one_seat = null;
      _updated_other_seat = null;

      if (OneSeat == null
          || OtherSeat == null
          || OneSeat.SeatId == OtherSeat.SeatId
          || OneSeat.GamingTableId != OtherSeat.GamingTableId)
      {
        return _error_code;
      }

      _error_code = GT_RESPONSE_CODE.ERROR_GENERIC;

      _sb = new StringBuilder();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb.AppendLine(" UPDATE   GT_PLAY_SESSIONS             ");
          _sb.AppendLine("    SET   GTPS_SEAT_ID     = @pSeatId  ");
          _sb.AppendLine("        , GTPS_ACCOUNT_ID  = (SELECT CASE @pPlayerType WHEN 1 THEN GTS_ACCOUNT_ID ELSE GTPS_ACCOUNT_ID END FROM GT_SEATS WHERE GTS_SEAT_ID = @pSeatId ) ");
          _sb.AppendLine("        , GTPS_UPDATED = GETDATE()                ");
          _sb.AppendLine(" OUTPUT   CAST(DELETED.GTPS_TIMESTAMP AS BIGINT)  ");
          _sb.AppendLine("        , CAST(INSERTED.GTPS_TIMESTAMP AS BIGINT) ");
          _sb.AppendLine("        , INSERTED.GTPS_UPDATED                   ");
          _sb.AppendLine("  WHERE   GTPS_PLAY_SESSION_ID = @pPlaySessionId  ");
          _sb.AppendLine("    AND   GTPS_STATUS <> @pStatusClosed           ");

          // Update Seat
          _sb.AppendLine("IF ( @@ROWCOUNT > 0 OR @pPlaySessionId = 0 ) ");
          _sb.AppendLine("BEGIN ");
          _sb.AppendLine("  UPDATE   GT_SEATS               ");
          _sb.AppendLine("     SET   GTS_CURRENT_PLAY_SESSION_ID = CASE WHEN @pPlaySessionId = 0 THEN NULL ElSE @pPlaySessionId END ");
          _sb.AppendLine("   WHERE   GTS_SEAT_ID = @pSeatId ");
          _sb.AppendLine("END ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(),
                                                  _db_trx.SqlTransaction.Connection,
                                                  _db_trx.SqlTransaction))
          {
            _param_seat_id = _cmd.Parameters.Add("@pSeatId", SqlDbType.BigInt);
            _param_player_type = _cmd.Parameters.Add("@pPlayerType", SqlDbType.Bit);
            _param_play_session_id = _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt);
            _param_play_session_status = _cmd.Parameters.Add("@pStatusClosed", SqlDbType.Int);

            _param_seat_id.Value = OneSeat.SeatId;
            _param_player_type.Value = (OtherSeat.PlayerType == GTPlayerType.AnonymousWithoutCard);
            _param_play_session_id.Value = OtherSeat.PlaySession.PlaySessionId;
            _param_play_session_status.Value = GTPlaySessionStatus.Closed;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (_reader.Read())
              {
                _sequence_one_seat = _reader.GetInt64(1);
                _updated_one_seat = _reader.GetDateTime(2);
              }
              //else
              //{
              //  return _error_code;
              //}
            }

            _param_seat_id.Value = OtherSeat.SeatId;
            _param_player_type.Value = (OneSeat.PlayerType == GTPlayerType.AnonymousWithoutCard);
            _param_play_session_id.Value = OneSeat.PlaySession.PlaySessionId;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (_reader.Read())
              {
                _sequence_other_seat = _reader.GetInt64(1);
                _updated_other_seat = _reader.GetDateTime(2);
              }
              else
              {
                return _error_code;
              }
            }

            _PT_movements = new PlayerTrackingMovements(CashierUserId, CashierTerminalId, OneSeat.GamingTableId, OneSeat.GamingTableSessionId);
            if (OneSeat.PlaySession.PlaySessionId > 0)
            {
              _PT_movements.Add(PlayerTrackingMovementType.SwapSeat, OneSeat.SeatId, OneSeat.PlaySession.PlaySessionId, OneSeat.VirtualTerminalId, OneSeat.AccountId,
                               (Decimal)OneSeat.SeatPosition, (Decimal)OtherSeat.SeatPosition, "", null, null);
            }
            if (OtherSeat.PlaySession.PlaySessionId > 0)
            {
              _PT_movements.Add(PlayerTrackingMovementType.SwapSeat, OtherSeat.SeatId, OtherSeat.PlaySession.PlaySessionId, OtherSeat.VirtualTerminalId, OtherSeat.AccountId,
                                (Decimal)OtherSeat.SeatPosition, (Decimal)OneSeat.SeatPosition, "", null, null);
            }

            if (!_PT_movements.Save(_db_trx.SqlTransaction))
            {
              return _error_code;
            }


            if (_db_trx.Commit())
            {
              // change position
              _old_one_seat_id = OneSeat.SeatId;
              OneSeat.SeatId = OtherSeat.SeatId;
              OtherSeat.SeatId = _old_one_seat_id;

              OneSeat.PlaySession.Sequence = _sequence_one_seat;
              OneSeat.PlaySession.Updated = _updated_one_seat;

              OtherSeat.PlaySession.Sequence = _sequence_other_seat;
              OtherSeat.PlaySession.Updated = _updated_other_seat;

              _error_code = GT_RESPONSE_CODE.OK;

              return _error_code;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _error_code;
    } // SwapSeats

    //------------------------------------------------------------------------------
    // PURPOSE: Read Gaming Tables. 
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //        - GamingTables: gaming tables
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean ReadTables(Int32 BankId, Int32 Cashier_id, out DataTable GamingTables, out Int32 GamingTableIdSelected)
    {
      StringBuilder _sb;
      DataRow[] _table;

      GamingTables = new DataTable();
      GamingTableIdSelected = 0;
      try
      {
        _sb = new StringBuilder();

        // TODO: revisar para optimizar
        _sb.AppendLine("SELECT   GT_GAMING_TABLE_ID");      // 0
        _sb.AppendLine("       , GT_NAME");                 // 1
        _sb.AppendLine("       , GT_TYPE_ID");              // 2
        _sb.AppendLine("       , GT_CASHIER_ID");           // 3
        _sb.AppendLine("       , GT_PLAYER_TRACKING_PAUSE");// 4
        _sb.AppendLine("       , CASE WHEN (SELECT   COUNT(*) ");
        _sb.AppendLine("              FROM   GAMING_TABLES_SESSIONS ");
        _sb.AppendLine("             INNER   JOIN CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID  ");
        _sb.AppendLine("             WHERE   GTS_GAMING_TABLE_ID    = GT_GAMING_TABLE_ID ");
        _sb.AppendLine("               AND   CS_STATUS = 0 ) > 0 THEN 0 ");
        _sb.AppendLine("                                         ELSE 1 ");
        _sb.AppendLine("         END AS GT_SESSION_STATUS "); // 5
        _sb.AppendLine("  FROM   GAMING_TABLES ");
        _sb.AppendLine(" WHERE   GT_ENABLED = @pTableEnable ");
        _sb.AppendLine("   AND   GT_BANK_ID = @pBankId");
        _sb.AppendLine("   AND   GT_NUM_SEATS > 0 ");
        _sb.AppendLine(" ORDER   BY GT_NAME ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pTableEnable", SqlDbType.Int).Value = 1;
            _sql_cmd.Parameters.Add("@pBankId", SqlDbType.Int).Value = BankId;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(GamingTables);
            }
          } // SqlCommand
        } // DB_TRX

        if (GamingTables.Rows.Count > 0)
        {
          _table = GamingTables.Select("GT_CASHIER_ID=" + Cashier_id.ToString());
          if (_table.Length > 0)
          {
            GamingTableIdSelected = (Int32)_table[0]["GT_GAMING_TABLE_ID"];
          }
          else
          {
            GamingTableIdSelected = (Int32)GamingTables.Rows[0]["GT_GAMING_TABLE_ID"];
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ReadTables

    //------------------------------------------------------------------------------
    // PURPOSE: Get Gaming tables situation (area and banks)
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //        - Database: gaming_table name, area name, bank name
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean GetGamingTablesSituation(out DataTable Banks)
    {
      StringBuilder _sb;

      Banks = new DataTable();
      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT GT_NAME, AR_NAME AREA_NAME, BK_NAME BANK_NAME FROM GAMING_TABLES LEFT JOIN AREAS ON GT_AREA_ID = AR_AREA_ID LEFT JOIN BANKS ON GT_BANK_ID = BK_BANK_ID");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(Banks);
            }
          } // SqlCommand
        } // DB_TRX

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ReadTables


    //------------------------------------------------------------------------------
    // PURPOSE: Read Gaming Tables. 
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //        - GamingTables: gaming tables
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean ReadBanksWithTablesAssociated(Int32 CashierId, out DataTable Banks, out Int32 BankId)
    {
      // TODO DDM: Falta la l�gica de asociar dispositivo-cajero.
      //           Hasta que este hecho, devolvera el primer Id de isla.
      StringBuilder _sb;
      Object _obj;

      Banks = new DataTable();
      BankId = -1;
      _obj = null;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT   BK_BANK_ID ");
        _sb.AppendLine("       , AR_NAME + '/' +BK_NAME as BK_NAME ");
        _sb.AppendLine("  FROM   GAMING_TABLES ");
        _sb.AppendLine(" INNER   JOIN BANKS ON GT_AREA_ID = BK_AREA_ID AND GT_BANK_ID = BK_BANK_ID");
        _sb.AppendLine(" INNER   JOIN AREAS ON BK_AREA_ID = AR_AREA_ID ");
        _sb.AppendLine("WHERE    GT_ENABLED = @pTableEnable ");
        _sb.AppendLine("  AND    GT_NUM_SEATS > 0 ");
        _sb.AppendLine(" GROUP   BY BK_NAME, AR_NAME,BK_BANK_ID ");
        _sb.AppendLine(" ORDER   BY AR_NAME ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pTableEnable", SqlDbType.Int).Value = 1;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(Banks);
            }
          } // SqlCommand

          if (Banks.Rows.Count > 0)
          {
            _sb.Length = 0;
            _sb.AppendLine("SELECT   GT_BANK_ID");
            _sb.AppendLine("  FROM   GAMING_TABLES");
            _sb.AppendLine(" WHERE   (GT_CASHIER_ID = @pCashierId OR GT_LINKED_CASHIER_ID = @pCashierId) ");
            _sb.AppendLine("   AND    GT_ENABLED = @pTableEnable ");
            _sb.AppendLine("   AND    GT_NUM_SEATS > 0 ");

            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = CashierId;
              _sql_cmd.Parameters.Add("@pTableEnable", SqlDbType.Int).Value = 1;

              _obj = _sql_cmd.ExecuteScalar();
            }
            BankId = (Int32)Banks.Rows[0]["BK_BANK_ID"];
            if (_obj != null && _obj != DBNull.Value)
            {
              BankId = (Int32)_obj;
            }
          }
        } // DB_TRX

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // ReadTables

    //------------------------------------------------------------------------------
    // PURPOSE: Add movements with last changes. 
    // 
    //  PARAMS:
    //      - INPUT:
    //        - Int32: Gaming Table Id
    //        - Int64: Seat Id
    //
    //      - OUTPUT:
    //        - Seat: Information about play sessions to the seat
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean AddLastMovemets(Seat Seat, Int32 CashierUserId, Int64 CashierTerminalId, SqlTransaction Trx)
    {
      PlayerTrackingMovements _PT_movements;
      Seat _last_seat;
      //TODO DDM Revisar..

      ReadSeat(Seat.GamingTableId, Seat.SeatId, Seat.GamingTableSessionId, out _last_seat);

      _PT_movements = new PlayerTrackingMovements(CashierUserId, CashierTerminalId, Seat.GamingTableId, Seat.GamingTableSessionId);

      if (Seat.PlaySession.PlayerSpeed != _last_seat.PlaySession.PlayerSpeed)
      {
        _PT_movements.Add(PlayerTrackingMovementType.Speed, Seat.SeatId, Seat.PlaySession.PlaySessionId, Seat.VirtualTerminalId, Seat.AccountId,
                           (decimal)_last_seat.PlaySession.PlayerSpeed, (decimal)Seat.PlaySession.PlayerSpeed, "", null, null);
      }
      if (Seat.PlaySession.PlayerSkill != _last_seat.PlaySession.PlayerSkill)
      {
        _PT_movements.Add(PlayerTrackingMovementType.Skill, Seat.SeatId, Seat.PlaySession.PlaySessionId, Seat.VirtualTerminalId, Seat.AccountId,
                           (decimal)_last_seat.PlaySession.PlayerSkill, (decimal)Seat.PlaySession.PlayerSkill, "", null, null);
      }
      if (Seat.PlaySession.CurrentBet != _last_seat.PlaySession.CurrentBet)
      {
        _PT_movements.Add(PlayerTrackingMovementType.CurrentPlayed, Seat.SeatId, Seat.PlaySession.PlaySessionId, Seat.VirtualTerminalId, Seat.AccountId,
                           (decimal)_last_seat.PlaySession.CurrentBet, (decimal)Seat.PlaySession.CurrentBet, Seat.PlaySession.PlayerIsoCode, null, null);
      }
      if (Seat.PlaySession.ChipsIn != _last_seat.PlaySession.ChipsIn)
      {
        _PT_movements.Add(PlayerTrackingMovementType.ChipsIn, Seat.SeatId, Seat.PlaySession.PlaySessionId, Seat.VirtualTerminalId, Seat.AccountId,
                           (decimal)_last_seat.PlaySession.ChipsIn, (decimal)Seat.PlaySession.ChipsIn, Seat.PlaySession.PlayerIsoCode, null, null);
      }
      if (Seat.PlaySession.ChipsOut != _last_seat.PlaySession.ChipsOut)
      {
        _PT_movements.Add(PlayerTrackingMovementType.ChipsOut, Seat.SeatId, Seat.PlaySession.PlaySessionId, Seat.VirtualTerminalId, Seat.AccountId,
                           (decimal)_last_seat.PlaySession.ChipsOut, (decimal)Seat.PlaySession.ChipsOut, Seat.PlaySession.PlayerIsoCode, null, null);
      }

      if (!_PT_movements.Save(Trx))
      {
        return false;
      }

      return true;
    } // AddLastMovemets

    //------------------------------------------------------------------------------
    // PURPOSE: Get status gaming tables. 
    // 
    //  PARAMS:
    //      - INPUT:
    //        - List<Int32>: Gaming Table Ids
    //
    //      - OUTPUT:
    //        - DataTable: Gaming table status
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean GetGamingTablesStatus(List<Int32> GamingTablesIds, out DataTable GamingTableStatus)
    {
      StringBuilder _sb;
      List<String> _gaming_tables_ids;

      _sb = new StringBuilder();
      GamingTableStatus = new DataTable();
      _gaming_tables_ids = new List<string>();

      if (GamingTablesIds != null && GamingTablesIds.Count > 0)
      {
        _gaming_tables_ids = GamingTablesIds.ConvertAll<String>(delegate(Int32 i) { return i.ToString(); });
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb.AppendLine("SELECT   GT_GAMING_TABLE_ID AS GT_GAMING_TABLE_ID ");
          _sb.AppendLine("       , MAX(CASE WHEN GTPS_STATUS = 0 ");
          _sb.AppendLine("                  THEN 1 ");
          _sb.AppendLine("                  ELSE 0 ");
          _sb.AppendLine("                  END) AS IS_OPEN ");
          _sb.AppendLine("  FROM   GAMING_TABLES ");
          _sb.AppendLine("  LEFT   JOIN GT_PLAY_SESSIONS ON GT_GAMING_TABLE_ID = GTPS_GAMING_TABLE_ID ");

          if (_gaming_tables_ids != null && _gaming_tables_ids.Count > 0)
          {
            _sb.AppendLine(" WHERE   GT_GAMING_TABLE_ID IN (" + String.Join(", ", _gaming_tables_ids.ToArray()) + ") ");
          }

          _sb.AppendLine("  GROUP BY GT_GAMING_TABLE_ID ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(GamingTableStatus);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Sale chips operation 
    //
    //  PARAMS :
    //      - INPUT :
    //          - Ticket
    //          - Seat
    //          - UserId
    //          - TerminalId
    //          - Amount
    //          - DbTrx
    //      - OUTPUT :
    //
    // RETURNS :  
    //      - Boolean
    //
    public static Boolean PlayerTracking_SaleChips(Seat Seat, Int32 UserId, Int64 TerminalId, Decimal Amount, DB_TRX _db_trx)
    {
      if (!GamingTableBusinessLogic.PlayerTrackingRegister_Sell(Seat.PlaySession, Amount, UserId,
                                                               TerminalId, Seat.GamingTableSessionId, 0, _db_trx.SqlTransaction))
      {
        return false;
      }

      if (!JunketsBusinessLogic.ProcessCommissionBuyIn(Seat.AccountId, Amount, _db_trx.SqlTransaction))
      {
        return false;
      }

      return true;
    }

    public static Boolean PlayerTracking_SaleChips(Ticket Ticket, Seat Seat, Int32 UserId, Int64 TerminalId, DB_TRX _db_trx)
    {
      GamingTablesSessions _gt_session;

      if (Ticket != null)
      {
        Ticket.Status = TITO_TICKET_STATUS.CANCELED;
        Ticket.LastActionTerminalID = Seat.VirtualTerminalId;
        Ticket.LastActionDateTime = WGDB.Now;
        Ticket.LastActionAccountID = Seat.AccountId;
        Ticket.LastActionTerminalType = Convert.ToInt32(TerminalTypes.GAMING_TABLE_SEAT);
        Ticket.Amt_1 = Ticket.Amt_0;
        Ticket.Cur_1 = Ticket.Cur_0;

        // Update Ticket TITO Data
        if (!Ticket.DB_UpdateTicket(Ticket, _db_trx.SqlTransaction))
        {
          return false;
        }

        // Get gaming table session
        if (!GamingTablesSessions.GetSession(out _gt_session, Seat.GamingTableSessionId, _db_trx.SqlTransaction))
        {
          return false;
        }

        // Update gaming table session values
        if (!_gt_session.UpdateSession(GTS_UPDATE_TYPE.ValidatedCopyDealerAmount, Ticket.Amt_0, new CurrencyIsoType(Ticket.Cur_0, CurrencyExchangeType.CASINO_CHIP_RE), _db_trx.SqlTransaction))
        {
          return false;
        }

        // Insert Copy Dealer ticket
        if (!DB_InsertCopyDealerValidated(Seat.GamingTableSessionId, Ticket.TicketID, GT_COPY_DEALER_VALIDATED_SOURCE.GAMING_TABLE, _db_trx.SqlTransaction))
        {
          return false;
        }

        if (!PlayerTrackingRegister_Sell(Seat.PlaySession, Ticket.Amount, UserId,
                                                                 TerminalId, Seat.GamingTableSessionId, Ticket.TransactionId, _db_trx.SqlTransaction))
        {
          return false;
        }

        if (!JunketsBusinessLogic.ProcessCommissionBuyIn(Seat.AccountId, Ticket.Amount, _db_trx.SqlTransaction))
        {
          return false;
        }

      }
      else
      {
        return false;
      }
      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Check chips balance to sale chips
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    public static Boolean HasChipsBalance(Int64 SessionId, Boolean ChipsSaleMode)
    {
      Decimal _total_chips;

      if (GamingTableBusinessLogic.IsGamingTablesEnabledAndPlayerTracking())
      {
        //DLL 22-APR-2014: when Cashier.Mode == 1 don't ask for chips balance
        if (ChipsSaleMode)
        {
          return true;
        }

        _total_chips = 0;

        //DLL 14-FEB-2014
        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _total_chips = WSI.Common.Cashier.UpdateSessionBalance(_db_trx.SqlTransaction, SessionId, 0, Cage.CHIPS_ISO_CODE, CurrencyExchangeType.CASINOCHIP);
          }
        }
        catch { }

        return (_total_chips > 0);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Restart Gaming table player counters
    //
    //  PARAMS :
    //      - INPUT :
    //          - Gaming Table Id
    //          - Restart Plays Boolean
    //      - OUTPUT :
    //
    // RETURNS :  
    //      - GT_RESPONSE_CODE
    //   NOTES :    
    public static GT_RESPONSE_CODE RestartTableCounters(Int32 GamingTableId, Boolean RestartPlays, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;

      try
      {

        // Reset player counters if last player in the table left
        _sb = new StringBuilder();
        _obj = null;

        _sb.AppendLine("  SELECT   COUNT(*)                               ");
        _sb.AppendLine("    FROM   GT_PLAY_SESSIONS                       ");
        _sb.AppendLine("   WHERE   GTPS_GAMING_TABLE_ID = @pGamingTableID ");
        _sb.AppendLine("     AND   GTPS_STATUS <> @pStatus                ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pGamingTableID", SqlDbType.BigInt).Value = GamingTableId;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = GTPlaySessionStatus.Closed;

          _obj = _cmd.ExecuteScalar();

          if (_obj == null && _obj == DBNull.Value)
          {
            return GT_RESPONSE_CODE.ERROR_GENERIC;
          }

          if ((Int32)_obj == 0 || !RestartPlays)
          {
            // Update counters to NULL value
            _sb = new StringBuilder();

            _sb.AppendLine(" UPDATE   GAMING_TABLES                        ");
            _sb.AppendLine("    SET   GT_LAST_TICK_PLAYS_COUNT = NULL      ");
            _sb.AppendLine("        , GT_LAST_TICK_DATE = NULL             ");

            if (RestartPlays)
            {
              _sb.AppendLine("        , GT_PLAYS_COUNT = NULL                ");
            }

            _sb.AppendLine("  WHERE   GT_GAMING_TABLE_ID = @pGamingTableId ");

            using (SqlCommand _cmdUpdate = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
            {
              _cmdUpdate.Parameters.Add("@pGamingTableID", SqlDbType.BigInt).Value = GamingTableId;
              if (_cmdUpdate.ExecuteNonQuery() == 0)
              {
                return GT_RESPONSE_CODE.ERROR_GENERIC;
              }
            }
          }
        }

        return GT_RESPONSE_CODE.OK;

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return GT_RESPONSE_CODE.ERROR_GENERIC;
    } // RestartPlaysCounter

    //------------------------------------------------------------------------------
    // PURPOSE : Restart Gaming Table Idle for Seats
    //
    //  PARAMS :
    //      - INPUT :
    //          - GamingTable 
    //      - OUTPUT :
    //
    // RETURNS :  
    //      - GT_RESPONSE_CODE
    //   NOTES :
    public static GT_RESPONSE_CODE ResetGamingTableIdle(GamingTable GamingTable)
    {
      StringBuilder _sb;
      GT_RESPONSE_CODE _error_code;


      try
      {
        if (GamingTable == null)
        {
          return GT_RESPONSE_CODE.ERROR_GENERIC;
        }
        // TODO DDM: Revisar Response_code
        _error_code = GT_RESPONSE_CODE.OK;


        // For update "idle" or "inactivity time" is necessary updating the last play and the last amount played
        // The "inactivity time"  is by number of play..        
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   GT_PLAY_SESSIONS                                   ");
        _sb.AppendLine("    SET   GTPS_IN_SESSION_LAST_PLAY           = GTPS_PLAYS   ");
        _sb.AppendLine("        , GTPS_IN_SESSION_LAST_PLAYED_AMOUNT  = GTPS_PLAYED_AMOUNT ");// DHA & DDM 23-OCT-2014: Fixed Bug WIG-1567
        _sb.AppendLine("        , GTPS_UPDATED                = GETDATE()            ");
        _sb.AppendLine("  WHERE   GTPS_PLAY_SESSION_ID        = @pPlaySessionId      ");
        _sb.AppendLine("    AND   GTPS_STATUS                 = @pStatusOpened       ");


        foreach (KeyValuePair<Int64, Seat> _seat in GamingTable.Seats)
        {
          if (_seat.Value.PlaySession == null
            || _seat.Value.PlaySession.PlaySessionId == 0)
          {
            continue;
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
            {
              _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = _seat.Value.PlaySession.PlaySessionId;
              _cmd.Parameters.Add("@pStatusOpened", SqlDbType.Int).Value = GTPlaySessionStatus.Opened;

              if (_cmd.ExecuteNonQuery() == 1)
              {
                _db_trx.Commit();
              }
            }
          } // DB_TRX
        } // foreach seat
      }
      catch (Exception _ex)
      {
        _error_code = GT_RESPONSE_CODE.ERROR_GENERIC;
        Log.Exception(_ex);
      }

      return _error_code;
    }// ResetGamingTableIdle

    //------------------------------------------------------------------------------
    // PURPOSE : Get two datatables, first with all movements table, second with a summary calculated which that movements table
    //
    //  PARAMS :
    //      - INPUT :
    //          - GamingTable 
    //      - OUTPUT :
    //          - Movements Table 
    //          - Summary Table 
    //          - Drop 
    //
    // RETURNS :  
    //      - GT_RESPONSE_CODE
    //   NOTES :
    public static Boolean GetSummaryAndCashierMovementsGamingTable(GamingTable GamingTable, out DataTable MovementsTable, out DataTable SummaryTable, out Currency Drop)
    {
      StringBuilder _sb;
      SqlParameter _param;
      DataSet _ds;

      MovementsTable = new DataTable();
      SummaryTable = new DataTable();
      Drop = 0;
      _ds = new DataSet();

      try
      {
        if (GamingTable == null)
        {
          return false;
        }
        _sb = new StringBuilder();

        //_sb.AppendLine("   DECLARE @Columns AS VARCHAR(MAX)	                                                                          ");
        //_sb.AppendLine("   DECLARE @NationalCurrency as VARCHAR(5)   ");
        //_sb.AppendLine("	 SET @NationalCurrency = (SELECT GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'RegionalOptions' AND GP_SUBJECT_KEY = 'CurrencyISOCode')");
        //_sb.AppendLine("   	                                                                                                          ");
        //_sb.AppendLine("      SELECT   MAX(CM_DATE)							      AS DATE_MOV                                                     ");
        //_sb.AppendLine("             , CM_TYPE									      AS TYPE_MOV                                                     ");
        //_sb.AppendLine("             , ISNULL(CCMR.CGM_MOVEMENT_ID,0)	AS ID_MOV                                                       ");
        //_sb.AppendLine("             , SUM(CM_SUB_AMOUNT)						  AS SUB_AMOUNT                                                   ");
        //_sb.AppendLine("             , SUM(CM_ADD_AMOUNT)						  AS ADD_AMOUNT                                                   ");
        //_sb.AppendLine("             , SUM(CM_ADD_AMOUNT) - SUM(CM_SUB_AMOUNT) AS CALCULATED_AMOUNT                                   ");
        //_sb.AppendLine("             ,  CASE ISNULL(CM_CURRENCY_ISO_CODE,'') WHEN '' THEN @NationalCurrency                           ");
        //_sb.AppendLine("                WHEN '" + Cage.CHIPS_COLOR + "' THEN ''                                                       ");
        //_sb.AppendLine("                ELSE CM.CM_CURRENCY_ISO_CODE END AS CURRENCY_ISO_CODE                                         ");
        //_sb.AppendLine("             ,  CM.CM_CAGE_CURRENCY_TYPE AS CURRENCY_TYPE                                                     ");
        //_sb.AppendLine("             ,  0 OPENER_MOV                                                                                  ");

        //_sb.AppendLine("        INTO   #TABLE_MOVEMENTS	                                                                              ");
        //_sb.AppendLine("        FROM   GAMING_TABLES_SESSIONS        	                                                                ");
        //_sb.AppendLine("   LEFT JOIN   CASHIER_MOVEMENTS AS CM WITH(INDEX(IX_CM_SESSION_ID)) ON CM_SESSION_ID = GTS_CASHIER_SESSION_ID");
        //_sb.AppendLine("   LEFT JOIN   CAGE_CASHIER_MOVEMENT_RELATION AS CCMR ON CCMR.CM_MOVEMENT_ID = CM.CM_MOVEMENT_ID              ");
        //_sb.AppendLine("       WHERE   GTS_GAMING_TABLE_SESSION_ID =  @pGtSessionId                                                  	");
        //_sb.AppendLine("         AND   CM_TYPE IN (@pCmOpen,                                                                          ");
        //_sb.AppendLine("   						             @pCmOpenFillIn,                                                                    ");
        //_sb.AppendLine("   						             @pCmFillIn,                                                                        ");
        //_sb.AppendLine("   						             @pCmFillOut,	                                                                      ");
        //_sb.AppendLine("   						             @pCmClose	                                                                        ");
        //_sb.AppendLine("                          )      	                                                                            ");
        //_sb.AppendLine("    GROUP BY   CM_TYPE                         	                                                              ");
        //_sb.AppendLine("             , CCMR.CGM_MOVEMENT_ID                          	                                                ");
        //_sb.AppendLine("             , CM.CM_CURRENCY_ISO_CODE	                                                                      ");
        //_sb.AppendLine("             , CM.CM_CAGE_CURRENCY_TYPE	                                                                      ");
        //_sb.AppendLine("    ORDER BY   MAX(CM_DATE) DESC    	                                                                        ");
        //_sb.AppendLine("    	                                                                                                        ");
        //_sb.AppendLine("      SELECT @Columns = COALESCE(@Columns + ',', '') +  '[' + CURRENCY_TYPE + ']'                             ");
        //_sb.AppendLine("        FROM   (SELECT   DISTINCT CURRENCY_ISO_CODE, CASE WHEN CURRENCY_TYPE IN ('" + Convert.ToInt32(CurrencyExchangeType.CASINO_CHIP_RE).ToString() + "'");
        //_sb.AppendLine("                                                                                ,'" + Convert.ToInt32(CurrencyExchangeType.CASINO_CHIP_NRE).ToString() + "'");
        //_sb.AppendLine("                                                                                ,'" + Convert.ToInt32(CurrencyExchangeType.CASINO_CHIP_COLOR).ToString() + "'" + ')');
        //_sb.AppendLine("                                                          THEN '.' + CASE WHEN CURRENCY_ISO_CODE = '' THEN '" + Cage.CHIPS_COLOR + "'                              ");
        //_sb.AppendLine("                                                                          ELSE CURRENCY_ISO_CODE END                                                               ");
        //_sb.AppendLine("                                                          ELSE CURRENCY_ISO_CODE END + ' ' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_TYPE                  ");
        //_sb.AppendLine("                  FROM   #TABLE_MOVEMENTS WHERE CURRENCY_ISO_CODE IS NOT NULL	                                ");
        //_sb.AppendLine("               ) AS COLS                                                                                      ");
        //_sb.AppendLine("             	 ORDER BY CURRENCY_TYPE ASC                                                                     ");
        //_sb.AppendLine("             	                                                                                                ");
        //_sb.AppendLine("    SET   @Columns =  ISNULL(@Columns,COALESCE('[' + @NationalCurrency + ']',''))                             ");
        //_sb.AppendLine("   	                                                                                                          ");
        //_sb.AppendLine("   IF EXISTS (SELECT * FROM #TABLE_MOVEMENTS WHERE TYPE_MOV = @pCmOpenFillIn)	                                ");
        //_sb.AppendLine("   	  UPDATE   #TABLE_MOVEMENTS                                                                               ");
        //_sb.AppendLine("   	     SET   OPENER_MOV = 1                                                                                 ");
        //_sb.AppendLine("   	   WHERE   ID_MOV = (SELECT MIN(ID_MOV)                                                                   ");
        //_sb.AppendLine("   	                       FROM #TABLE_MOVEMENTS                                                              ");
        //_sb.AppendLine("   	                      WHERE TYPE_MOV IN (@pCmOpenFillIn))                                                 ");
        //_sb.AppendLine("   ELSE                                                                                                       ");
        //_sb.AppendLine("   	  UPDATE   #TABLE_MOVEMENTS                                                                               ");
        //_sb.AppendLine("         SET   OPENER_MOV = 1                                                                                 ");
        //_sb.AppendLine("   	   WHERE   ID_MOV = (SELECT MIN(ID_MOV)                                                                   ");
        //_sb.AppendLine("   	                       FROM #TABLE_MOVEMENTS                                                              ");
        //_sb.AppendLine("   	                      WHERE TYPE_MOV != 0)                                                                ");
        //_sb.AppendLine("   	                                                                                                          ");
        //_sb.AppendLine("   DELETE   #TABLE_MOVEMENTS                                                                                  ");
        //_sb.AppendLine("    WHERE   TYPE_MOV IN (@pCmOpenFillIn)                                                                      ");
        //_sb.AppendLine("   	                                                                                                          ");

        //_sb.AppendLine("   SELECT   *        	                                                                                        ");
        //_sb.AppendLine("     FROM   #TABLE_MOVEMENTS	                                                                                ");
        //_sb.AppendLine("   						                                                                                                ");
        //_sb.AppendLine("   EXEC 	                                                                                                    ");
        //_sb.AppendLine("   ('	                                                                                                        ");
        //_sb.AppendLine("   -- INITIAL AMOUNT	                                                                                        ");
        //_sb.AppendLine("   		 SELECT   ''INITIAL_AMOUNT''  AS NAME_MOV	                                                              ");
        //_sb.AppendLine("   		         , * 	                                                                                          ");
        //_sb.AppendLine("   		   FROM   (SELECT   ADD_AMOUNT AS AMOUNT	                                                              ");
        //_sb.AppendLine("                , CASE WHEN CURRENCY_TYPE IN (" + Convert.ToInt32(CurrencyExchangeType.CASINO_CHIP_RE).ToString());
        //_sb.AppendLine("                                             ," + Convert.ToInt32(CurrencyExchangeType.CASINO_CHIP_NRE).ToString());
        //_sb.AppendLine("                                             ," + Convert.ToInt32(CurrencyExchangeType.CASINO_CHIP_COLOR).ToString());
        //_sb.AppendLine("                     ) THEN ''.'' + CASE WHEN CURRENCY_ISO_CODE = '''' THEN ''" + Cage.CHIPS_COLOR + "''                        ");
        //_sb.AppendLine("                                         ELSE CURRENCY_ISO_CODE END                                                             ");
        //_sb.AppendLine("                       ELSE CURRENCY_ISO_CODE END + '' '' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_ISO_CODE            ");
        //_sb.AppendLine("   					 FROM   #TABLE_MOVEMENTS	                                                                        ");
        //_sb.AppendLine("   					WHERE   OPENER_MOV = 1                                                                            ");
        //_sb.AppendLine("   				   ) AS T1	                                                                                        ");
        //_sb.AppendLine("   		 PIVOT 	                                                                                                ");
        //_sb.AppendLine("   			(	                                                                                                    ");
        //_sb.AppendLine("   					SUM (AMOUNT)	                                                                                    ");
        //_sb.AppendLine("   					FOR CURRENCY_ISO_CODE IN (' + @Columns+ ')	                                                      ");
        //_sb.AppendLine("   			) AS PVT	                                                                                            ");
        //_sb.AppendLine("   UNION ALL	                                                                                                ");
        //_sb.AppendLine("   -- TOTAL FILL IN 	                                                                                        ");
        //_sb.AppendLine("   		 SELECT   ''TOTAL_FILL_IN''  AS NAME_MOV	                                                              ");
        //_sb.AppendLine("   		        , * 	                                                                                          ");
        //_sb.AppendLine("   		   FROM   (SELECT   ADD_AMOUNT AS AMOUNT	                                                              ");
        //_sb.AppendLine("                , CASE WHEN CURRENCY_TYPE IN (" + Convert.ToInt32(CurrencyExchangeType.CASINO_CHIP_RE).ToString());
        //_sb.AppendLine("                                             ," + Convert.ToInt32(CurrencyExchangeType.CASINO_CHIP_NRE).ToString());
        //_sb.AppendLine("                                             ," + Convert.ToInt32(CurrencyExchangeType.CASINO_CHIP_COLOR).ToString());
        //_sb.AppendLine("                     ) THEN ''.'' + CASE WHEN CURRENCY_ISO_CODE = '''' THEN ''" + Cage.CHIPS_COLOR + "''                        ");
        //_sb.AppendLine("                                         ELSE CURRENCY_ISO_CODE END                                                             ");
        //_sb.AppendLine("                       ELSE CURRENCY_ISO_CODE END + '' '' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_ISO_CODE            ");
        //_sb.AppendLine("   					 FROM   #TABLE_MOVEMENTS	                                                                        ");
        //_sb.AppendLine("   					WHERE   OPENER_MOV <> 1                                                                           ");
        //_sb.AppendLine("   				   ) AS T1	                                                                                        ");
        //_sb.AppendLine("   		 PIVOT 	                                                                                                ");
        //_sb.AppendLine("   			(	                                                                                                    ");
        //_sb.AppendLine("   					SUM (AMOUNT)	                                                                                    ");
        //_sb.AppendLine("   					FOR CURRENCY_ISO_CODE IN (' + @Columns+ ')	                                                      ");
        //_sb.AppendLine("   			) AS PVT		                                                                                          ");
        //_sb.AppendLine("   UNION ALL	                                                                                                ");
        //_sb.AppendLine("   -- TOTAL WITHDRAW	                                                                                        ");
        //_sb.AppendLine("   		 SELECT   ''TOTAL_WITHDRAW'' AS NAME_MOV 	                                                              ");
        //_sb.AppendLine("   		        , * 	                                                                                          ");
        //_sb.AppendLine("   		   FROM   (SELECT   SUB_AMOUNT AS AMOUNT	                                                              ");
        //_sb.AppendLine("                , CASE WHEN CURRENCY_TYPE IN (" + Convert.ToInt32(CurrencyExchangeType.CASINO_CHIP_RE).ToString());
        //_sb.AppendLine("                                             ," + Convert.ToInt32(CurrencyExchangeType.CASINO_CHIP_NRE).ToString());
        //_sb.AppendLine("                                             ," + Convert.ToInt32(CurrencyExchangeType.CASINO_CHIP_COLOR).ToString());
        //_sb.AppendLine("                     ) THEN ''.'' + CASE WHEN CURRENCY_ISO_CODE = '''' THEN ''" + Cage.CHIPS_COLOR + "''                        ");
        //_sb.AppendLine("                                         ELSE CURRENCY_ISO_CODE END                                                             ");
        //_sb.AppendLine("                       ELSE CURRENCY_ISO_CODE END + '' '' + CONVERT(NVARCHAR(4), CURRENCY_TYPE) AS CURRENCY_ISO_CODE            ");
        //_sb.AppendLine("   					 FROM   #TABLE_MOVEMENTS	                                                                        ");
        //_sb.AppendLine("   				   ) AS T1	                                                                                        ");
        //_sb.AppendLine("   		 PIVOT 	                                                                                                ");
        //_sb.AppendLine("   			(	                                                                                                    ");
        //_sb.AppendLine("   					SUM (AMOUNT)	                                                                                    ");
        //_sb.AppendLine("   					FOR CURRENCY_ISO_CODE IN (' + @Columns+ ')	                                                      ");
        //_sb.AppendLine("   			) AS PVT		                                                                                          ");
        //_sb.AppendLine("   ')	                                                                                                        ");
        //_sb.AppendLine("   	                                                                                                          ");
        //_sb.AppendLine("   DROP TABLE #TABLE_MOVEMENTS	                                                                              ");
        //_sb.AppendLine(" SELECT @pDrop = dbo.GT_Calculate_DROP(@pOwnSalesAmount, @pExternalSalesAmount, @pCollectedAmount, @pIsIntegratedCashier) ");

        _sb.AppendLine("EXEC dbo.GT_Cashier_Movements_And_Summary_By_Session ");
        _sb.AppendLine("          @pGtSessionId                ");
        _sb.AppendLine("        , @pCageChipColor              ");
        _sb.AppendLine("        , @pOpeningCash                ");
        _sb.AppendLine("        , @pCageCloseSession           ");
        _sb.AppendLine("        , @pCageFillerIn               ");
        _sb.AppendLine("        , @pCageFillerOut              ");
        _sb.AppendLine("        , @pChipsSale                  ");
        _sb.AppendLine("        , @pChipsPurchase              ");
        _sb.AppendLine("        , @pChipsSaleDevolutionForTito ");
        _sb.AppendLine("        , @pChipsSaleWithCashIn        ");
        _sb.AppendLine("        , @pChipsSaleRegisterTotal     ");
        _sb.AppendLine("        , @pFillerInClosingStock       ");
        _sb.AppendLine("        , @pFillerOutClosingStock      ");
        _sb.AppendLine("        , @pReopenCashier              ");
        _sb.AppendLine("        , @pOwnSalesAmount             ");
        _sb.AppendLine("        , @pExternalSalesAmount        ");
        _sb.AppendLine("        , @pCollectedAmount            ");
        _sb.AppendLine("        , @pIsIntegratedCashier        ");
        _sb.AppendLine("        , @pDrop                OUTPUT ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pGtSessionId", SqlDbType.BigInt).Value = GamingTable.GamingTableSession.GamingTableSessionId;

            _cmd.Parameters.Add("@pCageChipColor", SqlDbType.NVarChar).Value = Cage.CHIPS_COLOR;
            _cmd.Parameters.Add("@pOpeningCash", SqlDbType.Int).Value = CASHIER_MOVEMENT.OPEN_SESSION;
            _cmd.Parameters.Add("@pCageCloseSession", SqlDbType.Int).Value = CASHIER_MOVEMENT.CAGE_CLOSE_SESSION;
            _cmd.Parameters.Add("@pCageFillerIn", SqlDbType.Int).Value = CASHIER_MOVEMENT.CAGE_FILLER_IN;
            _cmd.Parameters.Add("@pCageFillerOut", SqlDbType.Int).Value = CASHIER_MOVEMENT.CAGE_FILLER_OUT;
            _cmd.Parameters.Add("@pChipsSale", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_SALE;
            _cmd.Parameters.Add("@pChipsPurchase", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_PURCHASE;
            _cmd.Parameters.Add("@pChipsSaleDevolutionForTito", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_SALE_DEVOLUTION_FOR_TITO;
            _cmd.Parameters.Add("@pChipsSaleWithCashIn", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_SALE_WITH_CASH_IN;
            _cmd.Parameters.Add("@pChipsSaleRegisterTotal", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHIPS_SALE_REGISTER_TOTAL;
            _cmd.Parameters.Add("@pFillerInClosingStock", SqlDbType.Int).Value = CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK;
            _cmd.Parameters.Add("@pFillerOutClosingStock", SqlDbType.Int).Value = CASHIER_MOVEMENT.FILLER_OUT_CLOSING_STOCK;
            _cmd.Parameters.Add("@pReopenCashier", SqlDbType.Int).Value = CASHIER_MOVEMENT.REOPEN_CASHIER;

            _cmd.Parameters.Add("@pOwnSalesAmount", SqlDbType.Money).Value = GamingTable.GamingTableSession.OwnSalesAmount.SqlMoney;
            _cmd.Parameters.Add("@pExternalSalesAmount", SqlDbType.Money).Value = GamingTable.GamingTableSession.ExternalSalesAmount.SqlMoney;
            _cmd.Parameters.Add("@pCollectedAmount", SqlDbType.Money).Value = GamingTable.GamingTableSession.CollectedAmount.SqlMoney;
            _cmd.Parameters.Add("@pIsIntegratedCashier", SqlDbType.Bit).Value = GamingTable.IsIntegrated;

            _param = _cmd.Parameters.Add("@pDrop", SqlDbType.Money);
            _param.Direction = ParameterDirection.Output;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(_ds);

              if (_ds.Tables.Count != 2)
              {
                return false;
              }

              if (_ds.Tables[0].Rows.Count > 0)
              {
                _ds.Tables[0].TableName = "MOVEMENTS";
                MovementsTable = _ds.Tables[0];
              }

              if (_ds.Tables[1].Rows.Count > 0)
              {
                _ds.Tables[1].TableName = "SUMMARY";
                SummaryTable = _ds.Tables[1];
              }

              if (_param == null && _param.Value == DBNull.Value)
              {
                Drop = (Currency)0;
              }
              else
              {
                Drop = (Currency)(Decimal)_param.Value;
              }
            }
          }
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }

    } // GetSummaryAndCashierMovementsGamingTable

    //------------------------------------------------------------------------------
    // PURPOSE : Fill Cashier movements table in the gaming table
    //           Also calculate Totals
    //
    //  PARAMS :
    //      - INPUT :
    //          - GamingTable 
    //      - OUTPUT :
    //
    // RETURNS :  
    //      - GT_RESPONSE_CODE
    //   NOTES :
    public static GT_RESPONSE_CODE RefreshGamingTableCashierSessionInfo(GamingTable GamingTable)
    {
      DataTable _movements_table;
      DataTable _summary_table;
      Currency _drop;

      try
      {
        if (GamingTable == null || GamingTable.GamingTableSession == null)
        {
          return GT_RESPONSE_CODE.ERROR_GENERIC;
        }

        _movements_table = new DataTable();
        _summary_table = new DataTable();

        if (!GetSummaryAndCashierMovementsGamingTable(GamingTable, out _movements_table, out _summary_table, out _drop))
        {
          return GT_RESPONSE_CODE.ERROR_GENERIC;
        }
        else
        {
          if (_movements_table.Rows.Count < 1 || _summary_table.Rows.Count < 1)
          {
            // Always exist at least 1 movement -> Open
            return GT_RESPONSE_CODE.ERROR_GENERIC;
          }
          else
          {
            _movements_table.DefaultView.Sort = "[DATE_MOV] ASC";
            GamingTable.CashierSessionDrop = _drop;
            GamingTable.CashierSessionMovements = _movements_table.DefaultView.ToTable();
            GamingTable.CashierSessionSummary = _summary_table;

            if (GamingTable.CashierSessionMovements.Rows.Count > 1)
            {
              GamingTable.CashierSessionOpening = (DateTime)GamingTable.CashierSessionMovements.Compute("MIN(DATE_MOV)", "ID_MOV = '" + GamingTable.CashierSessionMovements.Compute("MIN(ID_MOV)", "") + "'");
            }
            else
            {
              GamingTable.CashierSessionOpening = (DateTime)GamingTable.CashierSessionMovements.Compute("MIN(DATE_MOV)", "");
            }
          }
        }

        return GT_RESPONSE_CODE.OK;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return GT_RESPONSE_CODE.ERROR_GENERIC;
      }

    } // RefreshCashierSessionMovements

    //------------------------------------------------------------------------------
    // PURPOSE : Returns value for max allowed sales for iso code
    //
    //  PARAMS :
    //      - INPUT :
    //          - String: IsoCode 
    //      - OUTPUT :
    //
    // RETURNS :  
    //      - Decimal: Max allowed Sales
    //   NOTES :
    public static Int32 GetGamingTablesMaxAllowedSales(String IsoCode)
    {
      StringBuilder _sb;
      Object _obj;
      Int32 _max_allowed_sales;

      _max_allowed_sales = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   GTC_MAX_ALLOWED_SALES");
        _sb.AppendLine("   FROM   GAMING_TABLE_CONFIGURATION");
        _sb.AppendLine("  WHERE   GTC_ISO_CODE = @pIsoCode");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = IsoCode == null ? CurrencyExchange.GetNationalCurrency() : IsoCode;

            _obj = _cmd.ExecuteScalar();

            if (_obj != null && _obj != DBNull.Value)
            {
              _max_allowed_sales = Decimal.ToInt32((Decimal)_obj);
            }
          }
        } // DB_TRX
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _max_allowed_sales;

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Returns value for max allowed purchase for iso code
    //
    //  PARAMS :
    //      - INPUT :
    //          - String: IsoCode 
    //      - OUTPUT :
    //
    // RETURNS :  
    //      - Decimal: Max allowed Purchase
    //   NOTES :
    public static Decimal GetGamingTablesMaxAllowedPurchase(String IsoCode)
    {
      StringBuilder _sb;
      Object _obj;
      Decimal _max_allowed_purchase;

      _max_allowed_purchase = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   GTC_MAX_ALLOWED_PURCHASE");
        _sb.AppendLine("   FROM   GAMING_TABLE_CONFIGURATION");
        _sb.AppendLine("  WHERE   GTC_ISO_CODE = @pIsoCode");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = IsoCode;

            _obj = _cmd.ExecuteScalar();

            if (_obj != null && _obj != DBNull.Value)
            {
              _max_allowed_purchase = (Decimal)_obj;
            }
          }
        } // DB_TRX
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _max_allowed_purchase;

    }

    // LTC 16-NOV-2016
    //------------------------------------------------------------------------------
    // PURPOSE: Get status gaming tables from GUI cage movement. 
    // 
    //  PARAMS:
    //      - INPUT:
    //        - List<Int32>: Gaming Table Ids
    //
    //      - OUTPUT:
    //        - DataTable: Gaming table status
    //
    //  RETURNS:
    //      - True: All went OK; False: otherwise
    //
    public static Boolean GetGamingTablesOpenedFromGUI(List<Int32> GamingTablesIds, out DataTable GamingTableStatus)
    {
      StringBuilder _sb;
      List<String> _gaming_tables_ids;

      _sb = new StringBuilder();
      GamingTableStatus = new DataTable();
      _gaming_tables_ids = new List<string>();

      if (GamingTablesIds != null && GamingTablesIds.Count > 0)
      {
        _gaming_tables_ids = GamingTablesIds.ConvertAll<String>(delegate(Int32 i) { return i.ToString(); });
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb.AppendLine("SELECT GT_GAMING_TABLE_ID AS TABLE_ID ");
          _sb.AppendLine("FROM GAMING_TABLES GT ");
          _sb.AppendLine("  INNER JOIN GAMING_TABLES_SESSIONS GTS ");
          _sb.AppendLine("    ON GTS.GTS_GAMING_TABLE_ID = GT.GT_GAMING_TABLE_ID ");
          _sb.AppendLine("  INNER JOIN CASHIER_SESSIONS CS ");
          _sb.AppendLine("    ON CS.CS_SESSION_ID = GTS.GTS_CASHIER_SESSION_ID ");

          if (_gaming_tables_ids != null && _gaming_tables_ids.Count > 0)
          {
            _sb.AppendLine(" WHERE  CS.CS_STATUS = 0 AND GT.GT_GAMING_TABLE_ID IN (" + String.Join(", ", _gaming_tables_ids.ToArray()) + ") ");
          }

          _sb.AppendLine("  ORDER BY 1  ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(GamingTableStatus);

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }


    /// <summary>
    /// Get if is enable "Rounding in the sale of chips with recharge"
    /// </summary>
    /// <returns>GP value</returns>
    public static Boolean IsEnableRoundingSaleChips()
    {
      return !TITO.Utils.IsTitoMode() && GeneralParam.GetBoolean("GamingTables", "Chips.Sales.RoundingToMinimumDenomination", false);
    }

    /// <summary>
    /// Get value of "Minimum chip value" for rounding in the sale of chips with recharge
    /// </summary>
    /// <returns>GP value</returns>
    public static Decimal GetDefaultMinimumDenominationForRoundingSaleChips()
    {
      return GeneralParam.GetDecimal("GamingTables", "Chips.Sales.DefaultMinimumDenominationForRounding", 1.00M);
    }

    /// <summary>
    /// Close all GTP Play Session in the Gaming Table
    /// </summary>
    /// <param name="GamingTableID"></param>
    /// <param name="Session"></param>
    /// <param name="SqlTrans"></param>
    public static void CloseAllGTPlaySession(int GamingTableID, CashierSessionInfo Session, SqlTransaction SqlTrans)
    {
      GamingTable _gaming_table;
      IList<Seat> _seats;

      _seats = new List<Seat>();
      GamingTableBusinessLogic.ReadTable(GamingTableID, out _gaming_table);

      foreach (var _item in _gaming_table.Seats)
      {
        if (_item.Value.PlayerType != GTPlayerType.Unknown)
        {
          _seats.Add(_item.Value);
        }
      }

      foreach (var _seat in _seats)
      {
        GamingTableBusinessLogic.EndGTPlaySession(_seat, Session.UserId, Session.TerminalId, Session.TerminalName, SqlTrans);
      }

    }  
  }
}
