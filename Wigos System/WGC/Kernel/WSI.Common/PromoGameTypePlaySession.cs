﻿
//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: PromoGameTypePlaySession.cs
// 
//      DESCRIPTION: PromogameType Play Session
// 
//           AUTHOR: Rubén Lama
// 
//    CREATION DATE: 02-NOV-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-NOV-2017 RLO    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using WSI.Common;

namespace WSI.Common
{
  public class PromoGameTypePlaySession : ICloneable
  {

    #region " Constants "

    #endregion

    #region " Members "

    #endregion

    #region " Properties "

    public Int64 PlaySessionId { get; set; }
    public PromoGame.GameType GameType { get; set; }

    #endregion

    #region " Public Methods "

    #region " Constructors "

    public PromoGameTypePlaySession()
    {
      this.Initialize();
    } // InTouchAccount

    #endregion

    /// <summary>
    /// Clone
    /// </summary>
    /// <returns></returns>
    public object Clone()
    {
      return this.MemberwiseClone();
    } // Clone

    /// <summary>
    /// Public method for insert to DB
    /// </summary>
    /// <returns></returns>
    public Boolean Insert()
    {
      return this.DB_Insert();
    }
    #endregion

    #region " Private Methods "

    /// <summary>
    /// Initialize InTouchAccount
    /// </summary>
    private void Initialize()
    {
      PlaySessionId = 0;
      GameType = PromoGame.GameType.Unknown;

    } // Initialize

    /// <summary>
    /// Insert into DB
    /// </summary>
    /// <returns></returns>
    private Boolean DB_Insert()
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {

        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT   PROMOGAME_TYPE_PLAY_SESSION       ");
        _sb.AppendLine(" VALUES   (@pPlaySessionID, @pGameType)     ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pPlaySessionID"       , SqlDbType.BigInt).Value = this.PlaySessionId;
            _cmd.Parameters.Add("@pGameType"            , SqlDbType.BigInt).Value = (Int64)this.GameType;

            if (_cmd.ExecuteNonQuery() == 1)
            {
              _db_trx.Commit();
              return true;
            }

            return false;
          }
        }

      }
      catch (Exception _ex)
      {
        Log.Error("PromoGameTypePlaySession Error: DB_Insert()");
        Log.Exception(_ex);
      }

      return false;

    } // DB_Insert

    #endregion
  }
}