//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MultiSiteRequests.cs
// 
//   DESCRIPTION: MultiSiteRequests functions.
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 23-APR-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-APR-2013 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using System.Threading;

namespace WSI.Common
{
  public enum WWP_MultiSiteRequestType
  {
    MULTISITE_REQUEST_TYPE_UNKNOWN = 0
  , MULTISITE_REQUEST_TYPE_ELP01 = 1
  }

   public enum WWP_MSRequestStatus
  {
    MS_REQUEST_STATUS_PENDING = 1
  , MS_REQUEST_STATUS_INPROGRESS = 2
  , MS_REQUEST_STATUS_OK = 3
  , MS_REQUEST_STATUS_ERROR = 4
  , MS_REQUEST_STATUS_TIMEOUT = 5
  }

  public static class MultiSiteRequests
  {
    #region Public Methods

    public static Boolean AddRequest(WWP_MultiSiteRequestType RequestType, String InputData, Int32 NumTries, Int32 Priority, out Int64 UniqueId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      SqlParameter _parameter;
      Int32 _tick0;
      Int32 _nr;
      String _process_name;

      // Initialize parameters
      _nr = 0;
      _tick0 = Environment.TickCount;
      UniqueId = -1;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" INSERT INTO   MULTISITE_REQUESTS   ");
        _sb.AppendLine("             ( MR_REQUEST_TYPE      ");
        _sb.AppendLine("             , MR_PRIORITY          ");
        _sb.AppendLine("             , MR_INPUT_DATA        ");
        _sb.AppendLine("             , MR_NUM_TRIES         ");
        _sb.AppendLine("             , MR_STATUS)           ");
        _sb.AppendLine("      VALUES ( @pMrRequestType      ");
        _sb.AppendLine("             , @pMrPriority         ");
        _sb.AppendLine("             , @pMrInputData        ");
        _sb.AppendLine("             , @pMrNumTries         ");
        _sb.AppendLine("             , @pMrStatus)          ");

        _sb.AppendLine(" SET     @pMrUniqueId = SCOPE_IDENTITY() ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pMrRequestType", SqlDbType.Int).Value = RequestType;
          _sql_cmd.Parameters.Add("@pMrPriority", SqlDbType.Int).Value = Priority;
          _sql_cmd.Parameters.Add("@pMrInputData", SqlDbType.NVarChar).Value = InputData;
          _sql_cmd.Parameters.Add("@pMrNumTries", SqlDbType.Int).Value = NumTries;
          _sql_cmd.Parameters.Add("@pMrStatus", SqlDbType.Int).Value = WWP_MSRequestStatus.MS_REQUEST_STATUS_PENDING;
          _parameter = _sql_cmd.Parameters.Add("@pMrUniqueId", SqlDbType.BigInt);
          _parameter.Direction = ParameterDirection.Output;

          _nr = _sql_cmd.ExecuteNonQuery();

          if(_parameter.Value != null)
          {
            UniqueId = (Int64)_parameter.Value;
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        //throw _ex;
      }
      finally
      {
        _process_name = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name; // = ClassName.MethodName
        Log.Warning(_process_name, 10000, _nr, _tick0);
      }

      return false;
    } // AddRequest

    // PURPOSE : Wait for the Request Status is not processing (not Pending and not InProgres)
    //
    //  PARAMS :
    //      - INPUT:
    //            - UniqueId : Request Id for wait
    //
    //      - OUTPUT :
    //            - Status
    //            - OutputData
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    public static WWP_MSRequestStatus WaitRequest(Int64 UniqueId, out String OutputData)
    {
      WWP_MSRequestStatus _status;
      Boolean _is_request_processing;
      Int32 _wait_seconds;
      Int64 _ellapsed;
      Int32 _tick0;

      // Initialize parameters
      OutputData    = "";
      _status       = WWP_MSRequestStatus.MS_REQUEST_STATUS_OK;
      _wait_seconds = GeneralParam.GetInt32("MultiSite", "RequestTimeOut", 30);
      _tick0        = Environment.TickCount;
      _ellapsed     = Misc.GetElapsedTicks(_tick0);

      _is_request_processing = true;
      while (_ellapsed < (_wait_seconds * 1000) && _is_request_processing)
      {
        if (!GetResponse(UniqueId, out OutputData, out _status))
        {
          return WWP_MSRequestStatus.MS_REQUEST_STATUS_ERROR;
        }
        else if ( _status != WWP_MSRequestStatus.MS_REQUEST_STATUS_PENDING
               && _status != WWP_MSRequestStatus.MS_REQUEST_STATUS_INPROGRESS)
        {
          _is_request_processing = false;
        }
        Thread.Sleep(100);
        _ellapsed = Misc.GetElapsedTicks(_tick0);
      }

      if (_is_request_processing)
      {
        _status = WWP_MSRequestStatus.MS_REQUEST_STATUS_TIMEOUT;
      }

      return _status;
    } // WaitRequest

    // PURPOSE : Delete a Request
    //
    //  PARAMS :
    //      - INPUT:
    //            - UniqueId : Request Id for delete
    //
    //      - OUTPUT :
    //            - None
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    public static Boolean DeleteRequest(Int64 UniqueId)
    {
      StringBuilder _sb;
      Int32 _tick0;
      Int32 _nr;
      String _process_name;

      // Initialize parameters
      _nr = 0;
      _tick0 = Environment.TickCount;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" DELETE FROM    MULTISITE_REQUESTS             ");
        _sb.AppendLine("       WHERE    MR_UNIQUE_ID = @pMrUniqueId    ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pMrUniqueId", SqlDbType.BigInt).Value = UniqueId;

            _nr = _db_trx.ExecuteNonQuery(_sql_cmd);

          } //SqlCommand _sql_cmd

          _db_trx.Commit();
          
          return true;

        } //DB_TRX _db_trx
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        //throw _ex;
      }
      finally
      {
        _process_name = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name; // = ClassName.MethodName
        Log.Warning(_process_name, 10000, _nr, _tick0);
      }

      return false;
    } // DeleteRequest

    #endregion // Public Methods

    #region Private Methods

    // PURPOSE : Obtain response of a Request
    //
    //  PARAMS :
    //      - INPUT:
    //            - UniqueId : Request Id of a query
    //
    //      - OUTPUT :
    //            - OutputData : Response of the Request
    //            - CodeStatus : Status of the Request
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    private static Boolean GetResponse(Int64 UniqueId, out String OutputData, out WWP_MSRequestStatus CodeStatus)
    {
      StringBuilder _sb;
      DataTable _multiSite_requests;
      Int32 _tick0;
      Int32 _nr;
      String _process_name;

      // Initialize parameters
      OutputData = "";
      CodeStatus = WWP_MSRequestStatus.MS_REQUEST_STATUS_ERROR;
      _nr = 0;
      _tick0 = Environment.TickCount;

      _multiSite_requests = new DataTable();

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT    MR_OUTPUT_DATA                 ");
        _sb.AppendLine("         , MR_STATUS                      ");
        _sb.AppendLine("   FROM    MULTISITE_REQUESTS             ");
        _sb.AppendLine("  WHERE    MR_UNIQUE_ID = @pMrUniqueId    ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pMrUniqueId", SqlDbType.BigInt).Value = UniqueId;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _nr = _sql_da.Fill(_multiSite_requests);
            }
          } //SqlCommand _sql_cmd
        } //DB_TRX _db_trx

        if (_multiSite_requests.Rows[0]["MR_OUTPUT_DATA"] != DBNull.Value)
        {
          OutputData = _multiSite_requests.Rows[0]["MR_OUTPUT_DATA"].ToString();
        }
        CodeStatus = (WWP_MSRequestStatus)_multiSite_requests.Rows[0]["MR_STATUS"];

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        _process_name = System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.Name + "." + System.Reflection.MethodBase.GetCurrentMethod().Name; // = ClassName.MethodName
        Log.Warning(_process_name, 10000, _nr, _tick0);
      }

      OutputData = "";
      CodeStatus = WWP_MSRequestStatus.MS_REQUEST_STATUS_ERROR;

      return false;
    } // GetResponse

    #endregion // Private Methods

  } // static class 
} // namespace
