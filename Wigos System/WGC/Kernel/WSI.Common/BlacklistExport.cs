﻿using SpreadsheetGear;
//------------------------------------------------------------------------------
// Copyright © 2007-2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME:
// 
//   DESCRIPTION: Export blacklist to excel documents.
//
//        AUTHOR: Pablo Molina  
// 
// CREATION DATE: 29-JUL-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 29-JUL-2016 PDM   First release
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using WSI.Common.Entrances.Enums;
using WSI.Common.Utilities;

namespace WSI.Common
{
  public class BlacklistExport
  {

    private static Dictionary<String, String> m_columns = new Dictionary<string, string>();

    static BlacklistExport()
    {

      m_columns.Add("blkf_reference", Resource.String("STR_FRM_BLACKLIST_IMPORT_REFERENCE"));
      m_columns.Add("blkf_name", Resource.String("STR_FRM_BLACKLIST_IMPORT_NAME"));
      m_columns.Add("blkf_middle_name", Resource.String("STR_FRM_BLACKLIST_IMPORT_MIDDLENAME"));
      m_columns.Add("blkf_lastname_1", Resource.String("STR_FRM_BLACKLIST_IMPORT_LASTNAME1"));
      m_columns.Add("blkf_lastname_2", Resource.String("STR_FRM_BLACKLIST_IMPORT_LASTNAME2"));
      m_columns.Add("blkf_document_type", Resource.String("STR_FRM_BLACKLIST_IMPORT_DOCUMENT_TYPE"));
      m_columns.Add("blkf_document", Resource.String("STR_FRM_BLACKLIST_IMPORT_DOCUMENT_NUMBER"));
      m_columns.Add("bklf_exclusion_date", Resource.String("STR_FRM_BLACKLIST_IMPORT_EXCLUSION_DATE"));
      m_columns.Add("bklf_reason_type", Resource.String("STR_FRM_BLACKLIST_IMPORT_REASON_TYPE"));
      m_columns.Add("bklf_reason_description", Resource.String("STR_FRM_BLACKLIST_IMPORT_REASON_EXCLUSION"));
      m_columns.Add("bklf_exclusion_duration", Resource.String("STR_FRM_BLACKLIST_IMPORT_EXCLUSION_DURATION"));

    }

    public static void Export(DataTable Table, Boolean OpenDocument = false)
    {
      CultureInfo _old_ci;
      String _filename;
      Microsoft.Office.Interop.Excel.Application _excel;
      IWorkbook _book;
      IWorksheet _sheet;
      Int32 _index_row;
      IRange _range;

      _old_ci = Thread.CurrentThread.CurrentCulture;
      _excel = null;
      _book = null;
      _sheet = null;
      _filename = String.Format("{0}{1}", "wigos_blacklist_export_", DateTime.Now.ToString("yyyyMMddHHmmss"));

      try
      {
        // Excel needs the running thread with Culture as en-US.
        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", true);

        // Restore DateTime and Number Format.
        Thread.CurrentThread.CurrentCulture.DateTimeFormat = _old_ci.DateTimeFormat;
        Thread.CurrentThread.CurrentCulture.NumberFormat = _old_ci.NumberFormat;

        _excel = null;
        try
        {
          _excel = new Microsoft.Office.Interop.Excel.ApplicationClass();
          _excel.DisplayAlerts = false;
        }
        catch
        {
          _excel = null;
        }

        ExcelConversion.SetFileExtensionAndMaxRows(_excel, ref _filename);

        // Initilize excel spreadsheetgear.
        _book = Factory.GetWorkbook();
        _sheet = _book.Worksheets[0];

        //First the header
        SetHeader(_sheet);

        //Save the data
        _index_row = 1;

        foreach (DataRow dt in Table.Rows)
        {

          _range = _sheet.Cells[_index_row, 0];
          _range.Value = dt["blkf_reference"] == DBNull.Value ? "" : dt["blkf_reference"];

          _range = _sheet.Cells[_index_row, 1];
          _range.Value = dt["blkf_name"] == DBNull.Value ? "" : dt["blkf_name"];

          _range = _sheet.Cells[_index_row, 2];
          _range.Value = dt["blkf_middle_name"] == DBNull.Value ? "" : dt["blkf_middle_name"];

          _range = _sheet.Cells[_index_row, 3];
          _range.Value = dt["blkf_lastname_1"] == DBNull.Value ? "" : dt["blkf_lastname_1"];

          _range = _sheet.Cells[_index_row, 4];
          _range.Value = dt["blkf_lastname_2"] == DBNull.Value ? "" : dt["blkf_lastname_2"];

          _range = _sheet.Cells[_index_row, 5];
          _range.Value = GetDocumentType(dt["blkf_document_type"]);

          _range = _sheet.Cells[_index_row, 6];
          _range.Value = dt["blkf_document"] == DBNull.Value ? "" : dt["blkf_document"];

          _range = _sheet.Cells[_index_row, 7];
          _range.Value = dt["bklf_exclusion_date"] == DBNull.Value ? new DateTime(1900, 1, 1) : dt["bklf_exclusion_date"];

          _range = _sheet.Cells[_index_row, 8];
          _range.Value = GetBlockReasonType(dt["bklf_reason_type"]);

          _range = _sheet.Cells[_index_row, 9];
          _range.Value = dt["bklf_reason_description"] == DBNull.Value ? "" : dt["bklf_reason_description"];

          _range = _sheet.Cells[_index_row, 10];
          _range.Value = dt["bklf_exclusion_duration"] == DBNull.Value ? 0 : dt["bklf_exclusion_duration"];

          _index_row++;
        }

        // Save excel file.
        FileStream _file = File.Create(_filename);
        switch (ExcelConversion.ExcelVersion)
        {
          case WSI.Common.ExcelConversion.EXCEL_VERSION.AFTER_2007:
            _book.SaveToStream(_file, FileFormat.OpenXMLWorkbook);
            break;

          case WSI.Common.ExcelConversion.EXCEL_VERSION.BEFORE_2007:
            _book.SaveToStream(_file, FileFormat.Excel8);
            break;
        }
        _file.Close();

        if (OpenDocument && System.IO.File.Exists(_file.Name))
        {
          System.Diagnostics.Process.Start(_file.Name);
        }

      }
      catch (Exception _ex)
      {
        throw new Exception(_ex.Message, _ex);
      }
    }

    private static void SetHeader(IWorksheet Sheet)
    {

      IRange _range;


      _range = Sheet.Cells[0, 0];
      _range.Value = m_columns["blkf_reference"];
      _range.ColumnWidth = 25d;
      _range.Interior.Color = SpreadsheetGear.Colors.Aquamarine;

      _range = Sheet.Cells[0, 1];
      _range.Value = m_columns["blkf_name"];
      _range.ColumnWidth = 25d;
      _range.Interior.Color = SpreadsheetGear.Colors.Aquamarine;

      _range = Sheet.Cells[0, 2];
      _range.Value = m_columns["blkf_middle_name"];
      _range.ColumnWidth = 25d;
      _range.Interior.Color = SpreadsheetGear.Colors.Aquamarine;

      _range = Sheet.Cells[0, 3];
      _range.Value = m_columns["blkf_lastname_1"];
      _range.ColumnWidth = 25d;
      _range.Interior.Color = SpreadsheetGear.Colors.Aquamarine;

      _range = Sheet.Cells[0, 4];
      _range.Value = m_columns["blkf_lastname_2"];
      _range.ColumnWidth = 25d;
      _range.Interior.Color = SpreadsheetGear.Colors.Aquamarine;

      _range = Sheet.Cells[0, 5];
      _range.Value = m_columns["blkf_document_type"];
      _range.ColumnWidth = 20d;
      _range.Interior.Color = SpreadsheetGear.Colors.Aquamarine;

      _range = Sheet.Cells[0, 6];
      _range.Value = m_columns["blkf_document"];
      _range.ColumnWidth = 25d;
      _range.Interior.Color = SpreadsheetGear.Colors.Aquamarine;

      _range = Sheet.Cells[0, 7];
      _range.Value = m_columns["bklf_exclusion_date"];
      _range.ColumnWidth = 20d;
      _range.Interior.Color = SpreadsheetGear.Colors.Aquamarine;

      _range = Sheet.Cells[0, 8];
      _range.Value = m_columns["bklf_reason_type"];
      _range.ColumnWidth = 25d;
      _range.Interior.Color = SpreadsheetGear.Colors.Aquamarine;

      _range = Sheet.Cells[0, 9];
      _range.Value = m_columns["bklf_reason_description"];
      _range.ColumnWidth = 25d;
      _range.Interior.Color = SpreadsheetGear.Colors.Aquamarine;

      _range = Sheet.Cells[0, 10];
      _range.Value = m_columns["bklf_exclusion_duration"];
      _range.ColumnWidth = 20d;
      _range.Interior.Color = SpreadsheetGear.Colors.Aquamarine;

    }

    private static String GetDocumentType(object DocType)
    {
      try
      {
        return IdentificationTypes.DocIdTypeString(Convert.ToInt32(DocType));
      }
      catch (Exception)
      {
        return "";
      }
    }

    private static String GetBlockReasonType(object ReasonType)
    {
      ENUM_BLACKLIST_REASON _reason;

      try
      {
        Dictionary<Enum, String> dict = EnumHelper.ToDictionary2<ENUM_BLACKLIST_REASON>();
        _reason = ConvertObjectToEnum<ENUM_BLACKLIST_REASON>(ReasonType);
        return dict[_reason];
      }
      catch (Exception)
      {
        return Resource.String("STR_ENUM_BLACKLIST_REASON_OTHER");
      }
    }

    private static T ConvertObjectToEnum<T>(object Value)
    {
      T _enumVal = (T)Enum.Parse(typeof(T), Value.ToString());
      return _enumVal;
    }

  }
}
