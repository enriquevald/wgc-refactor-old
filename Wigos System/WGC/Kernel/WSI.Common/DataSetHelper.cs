//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: DataSetHelper.cs
// 
//   DESCRIPTION: Class to create a DataTable object that contains aggregate data from another DataTable object.
//                Obtained from http://support.microsoft.com/default.aspx?scid=kb;en-us;326145
// 
//        AUTHOR: Raul Cervera
// 
// CREATION DATE: 01-FEB-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-FEB-2011 RCI    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace WSI.Common
{
  public class DataSetHelper
  {
    private System.Collections.ArrayList m_field_info;
    private System.Collections.ArrayList m_group_by_field_info;
    private string m_field_list;
    private string m_group_by_field_list;

    private class FieldInfo
    {
      public string RelationName;
      public string FieldName;	//source table field name
      public string FieldAlias;	//destination table field name
      public string Aggregate;
    }

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Creates a table based on aggregates of fields of another table.
    //
    //  PARAMS :
    //      - INPUT :
    //          - string TableName
    //          - DataTable SourceTable
    //          - string FieldList
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    public DataTable CreateGroupByTable(string TableName,
                                        DataTable SourceTable,
                                        string FieldList)
    {
      DataTable _dt;
      DataColumn _dc;

      //  FieldList syntax: fieldname[ alias]|aggregatefunction(fieldname)[ alias], ...
      if (FieldList == null)
      {
        throw new ArgumentException("You must specify at least one field in the field list.");
      }

      ParseGroupByFieldList(FieldList);

      _dt = new DataTable(TableName);

      foreach (FieldInfo Field in m_group_by_field_info)
      {
        _dc = SourceTable.Columns[Field.FieldName];
        if (Field.Aggregate == null)
        {
          _dt.Columns.Add(Field.FieldAlias, _dc.DataType, _dc.Expression);
        }
        else
        {
          _dt.Columns.Add(Field.FieldAlias, _dc.DataType);
        }
      }

      return _dt;
    } // CreateGroupByTable

    //------------------------------------------------------------------------------
    // PURPOSE : Copies the selected rows and columns from SourceTable and inserts them into DestTable.
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable DestTable
    //          - DataTable SourceTable
    //          - string FieldList
    //          - string RowFilter
    //          - string GroupBy
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void InsertGroupByInto(DataTable DestTable,
                                  DataTable SourceTable,
                                  string FieldList,
                                  string RowFilter,
                                  string GroupBy)
    {
      DataRow[] _rows;
      DataRow _last_source_row;
      DataRow _dest_row;
      bool _same_row;
      int _row_count;

      // RowFilter affects rows before GroupBy operation. No "Having" support
      // though this can be emulated by subsequent filtering of the table that results.
      
      // FieldList has same format as CreateGroupByTable
      if (FieldList == null)
      {
        throw new ArgumentException("You must specify at least one field in the field list.");
      }

      ParseGroupByFieldList(FieldList);	//parse field list
      ParseFieldList(GroupBy, false);			//parse field names to Group By into an arraylist

      _rows = SourceTable.Select(RowFilter, GroupBy);
      _last_source_row = null;
      _dest_row = null;
      _row_count = 0;

      foreach (DataRow SourceRow in _rows)
      {
        _same_row = false;
        if (_last_source_row != null)
        {
          _same_row = true;
          foreach (FieldInfo Field in m_field_info)
          {
            if (!ColumnEqual(_last_source_row[Field.FieldName], SourceRow[Field.FieldName]))
            {
              _same_row = false;
              break;
            }
          }
          if (!_same_row)
          {
            DestTable.Rows.Add(_dest_row);
          }
        }
        if (!_same_row)
        {
          _dest_row = DestTable.NewRow();
          _row_count = 0;
        }

        _row_count += 1;
        foreach (FieldInfo Field in m_group_by_field_info)
        {
          switch (Field.Aggregate)    //this test is case-sensitive
          {
            case null:        //implicit last
            case "":        //implicit last
            case "last":
              _dest_row[Field.FieldAlias] = SourceRow[Field.FieldName];
              break;

            case "first":
              if (_row_count == 1)
                _dest_row[Field.FieldAlias] = SourceRow[Field.FieldName];
              break;

            case "count":
              _dest_row[Field.FieldAlias] = _row_count;
              break;

            case "sum":
              _dest_row[Field.FieldAlias] = Add(_dest_row[Field.FieldAlias], SourceRow[Field.FieldName]);
              break;

            case "max":
              _dest_row[Field.FieldAlias] = Max(_dest_row[Field.FieldAlias], SourceRow[Field.FieldName]);
              break;

            case "min":
              if (_row_count == 1)
                _dest_row[Field.FieldAlias] = SourceRow[Field.FieldName];
              else
                _dest_row[Field.FieldAlias] = Min(_dest_row[Field.FieldAlias], SourceRow[Field.FieldName]);
              break;
          }
        }

        _last_source_row = SourceRow;
      }

      if (_dest_row != null)
      {
        DestTable.Rows.Add(_dest_row);
      }
    } // InsertGroupByInto

    //------------------------------------------------------------------------------
    // PURPOSE : Selects data from one DataTable to another and performs various aggregate functions
    //           along the way. See InsertGroupByInto and ParseGroupByFieldList for supported aggregate functions.
    //
    //  PARAMS :
    //      - INPUT :
    //          - string TableName
    //          - DataTable SourceTable
    //          - string FieldList
    //          - string RowFilter
    //          - string GroupBy
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - DataTable
    //
    public DataTable SelectGroupByInto(string TableName,
                                       DataTable SourceTable,
                                       string FieldList,
                                       string RowFilter,
                                       string GroupBy)
    {
      DataTable _dt;

      _dt = CreateGroupByTable(TableName, SourceTable, FieldList);
      InsertGroupByInto(_dt, SourceTable, FieldList, RowFilter, GroupBy);

      return _dt;
    } // SelectGroupByInto

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : Parse FieldList into FieldInfo objects and then 
    //           adds them to the m_FieldInfo private member.
    //
    //           FieldList systax:  [relationname.]fieldname[ alias], ...
    //  PARAMS :
    //      - INPUT :
    //          - string FieldList
    //          - bool AllowRelation
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void ParseFieldList(string FieldList,
                                bool AllowRelation)
    {
      int i;
      FieldInfo Field;
      string[] FieldParts;
      string[] Fields;

      if (m_field_list == FieldList)
      {
        return;
      }

      m_field_info = new System.Collections.ArrayList();
      m_field_list = FieldList;

      Fields = FieldList.Split(',');

      for (i = 0; i <= Fields.Length - 1; i++)
      {
        Field = new FieldInfo();
        //parse FieldAlias
        FieldParts = Fields[i].Trim().Split(' ');

        switch (FieldParts.Length)
        {
          case 1:
            //to be set at the end of the loop
            break;

          case 2:
            Field.FieldAlias = FieldParts[1];
            break;

          default:
            throw new Exception("Too many spaces in field definition: '" + Fields[i] + "'.");
        }

        //parse FieldName and RelationName
        FieldParts = FieldParts[0].Split('.');

        switch (FieldParts.Length)
        {
          case 1:
            Field.FieldName = FieldParts[0];
            break;

          case 2:
            if (AllowRelation == false)
            {
              throw new Exception("Relation specifiers not permitted in field list: '" + Fields[i] + "'.");
            }
            Field.RelationName = FieldParts[0].Trim();
            Field.FieldName = FieldParts[1].Trim();
            break;

          default:
            throw new Exception("Invalid field definition: " + Fields[i] + "'.");
        }

        if (Field.FieldAlias == null)
        {
          Field.FieldAlias = Field.FieldName;
        }
        m_field_info.Add(Field);
      }
    } // ParseFieldList

    //------------------------------------------------------------------------------
    // PURPOSE : Parses FieldList into FieldInfo objects and adds them to the GroupByFieldInfo private member
    //
    //           FieldList syntax: fieldname[ alias]|operatorname(fieldname)[ alias],...
    //
    //           Supported Operators: count, sum, max, min, first, last
    //  PARAMS :
    //      - INPUT :
    //          - string FieldList
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private void ParseGroupByFieldList(string FieldList)
    {
      FieldInfo Field;
      string[] FieldParts;
      string[] Fields;

      if (m_group_by_field_list == FieldList)
      {
        return;
      }

      m_group_by_field_info = new System.Collections.ArrayList();
      
      Fields = FieldList.Split(',');

      for (int i = 0; i <= Fields.Length - 1; i++)
      {
        Field = new FieldInfo();
        //Parse FieldAlias
        FieldParts = Fields[i].Trim().Split(' ');

        switch (FieldParts.Length)
        {
          case 1:
            //to be set at the end of the loop
            break;

          case 2:
            Field.FieldAlias = FieldParts[1];
            break;

          default:
            throw new ArgumentException("Too many spaces in field definition: '" + Fields[i] + "'.");
        }

        //Parse FieldName and Aggregate
        FieldParts = FieldParts[0].Split('(');

        switch (FieldParts.Length)
        {
          case 1:
            Field.FieldName = FieldParts[0];
            break;

          case 2:
            Field.Aggregate = FieldParts[0].Trim().ToLower();    //we're doing a case-sensitive comparison later
            Field.FieldName = FieldParts[1].Trim(' ', ')');
            break;

          default:
            throw new ArgumentException("Invalid field definition: '" + Fields[i] + "'.");
        }

        if (Field.FieldAlias == null)
        {
          if (Field.Aggregate == null)
          {
            Field.FieldAlias = Field.FieldName;
          }
          else
          {
            Field.FieldAlias = Field.Aggregate + "of" + Field.FieldName;
          }
        }
        m_group_by_field_info.Add(Field);
      }
      m_group_by_field_list = FieldList;
    } // ParseGroupByFieldList

    //------------------------------------------------------------------------------
    // PURPOSE : Compares two values to see if they are equal. Also compares DBNULL.Value.
    //
    //  PARAMS :
    //      - INPUT :
    //          - object Obj1
    //          - object Obj2
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - bool
    //
    private bool ColumnEqual(object Obj1, object Obj2)
    {
      if ((Obj1 is DBNull) && (Obj2 is DBNull))
      {
        return true;    //both are null
      }
      if ((Obj1 is DBNull) || (Obj2 is DBNull))
      {
        return false;    //only one is null
      }
      if (Obj1 is string && Obj2 is string)
      {
        return (String.Compare((string)Obj1, (string)Obj2, true) == 0);
      }

      return Obj1.Equals(Obj2);    //value type standard comparison
    } // ColumnEqual

    //------------------------------------------------------------------------------
    // PURPOSE : Returns MIN of two values - DBNull is less than all others.
    //
    //  PARAMS :
    //      - INPUT :
    //          - object Obj1
    //          - object Obj2
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - object
    //
    private object Min(object Obj1, object Obj2)
    {
      if ((Obj1 is DBNull) || (Obj2 is DBNull))
      {
        return DBNull.Value;
      }
      if (((IComparable)Obj1).CompareTo(Obj2) == -1)
      {
        return Obj1;
      }

      return Obj2;
    } // Min

    //------------------------------------------------------------------------------
    // PURPOSE : Returns MAX of two values - DBNull is less than all others.
    //
    //  PARAMS :
    //      - INPUT :
    //          - object Obj1
    //          - object Obj2
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - object
    //
    private object Max(object Obj1, object Obj2)
    {
      if (Obj1 is DBNull)
      {
        return Obj2;
      }
      if (Obj2 is DBNull)
      {
        return Obj1;
      }
      if (((IComparable)Obj1).CompareTo(Obj2) == 1)
      {
        return Obj1;
      }

      return Obj2;
    } // Max

    //------------------------------------------------------------------------------
    // PURPOSE : Adds two values - if one is DBNull, then returns the other.
    //
    //  PARAMS :
    //      - INPUT :
    //          - object Obj1
    //          - object Obj2
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - object
    //
    private object Add(object Obj1, object Obj2)
    {
      decimal _obj1_value;
      decimal _obj2_value;

      if (Obj1 is DBNull)
      {
        return Obj2;
      }
      if (Obj2 is DBNull)
      {
        return Obj1;
      }

      decimal.TryParse(Obj1.ToString(), out _obj1_value);
      decimal.TryParse(Obj2.ToString(), out _obj2_value);

      return (_obj1_value + _obj2_value);
    } // Add

    #endregion // Private Methods

  } // DataSetHelper

} // WSI.Common
