﻿using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common.Entities.General;

namespace WSI.Common.Entrances
{
  public interface ICustomerNameAndId
  {
    long? AccountId { get; set; }//
    string HolderName1 { get; } //
    string HolderName2 { get; } //
    string HolderName3 { get; } //
    string HolderName4 { get; } //  
    string HolderDocument { get; } //
    DocumentType HolderDocumentType { get; }
    DateTime? HolderBirthDate { get; set; }
    Dictionary<int, string> IDDocuments { get; set; }

//

  }
}
