﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : CustomerAddress.cs
// 
//   DESCRIPTION : Transport Class used between the UI and Common
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-DIC-2015 AVZ    Product Backlog Item 6646:Visitas / Recepción: Expedientes
//------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using WSI.Common.Entrances.Enums;
using WSI.Common.Utilities;
using WSI.Common.Utilities.Extensions;

namespace WSI.Common.Entrances.Entities
{
  [Serializable, XmlRoot(ElementName = "address")]
  public class CustomerAddress
  {
    [XmlIgnore]
    public ENUM_ADDRESS AddressType
    {
      get;
      set;
    }

    [XmlElementAttribute(ElementName = "type")]
    public int LegacyAddressType
    {
      get
      {
        return (int) this.AddressType;
      }
      set
      {
        this.AddressType = (ENUM_ADDRESS)value;
      }
    }

    [XmlElementAttribute(ElementName = "street")]
    public String Street
    {
      get;
      set;
    }

    [XmlElementAttribute(ElementName = "cp")]
    public String Cp
    {
      get;
      set;
    }

    [XmlElementAttribute(ElementName = "number")]
    public String Number
    {
      get;
      set;
    }

    [XmlElementAttribute(ElementName = "colony")]
    public String Colony
    {
      get;
      set;
    }

    [XmlElementAttribute(ElementName = "delegation")]
    public String Delegation
    {
      get;
      set;
    }

    [XmlElementAttribute(ElementName = "city")]
    public String City
    {
      get;
      set;
    }

    [XmlElementAttribute(ElementName = "country")]
    public Int32 CountryAddress
    {
      get;
      set;
    }

    [XmlElementAttribute(ElementName = "federalentity")]
    public Int32 FederalEntity
    {
      get;
      set;
    }

    public string ToXML()
    {
      return XmlSerializationHelper.Serialize<CustomerAddress>(this);
    }
  }
}
