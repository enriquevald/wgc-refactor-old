﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : CustomerRecordDetail.cs
// 
//   DESCRIPTION : Class CustomerRecordDetail - Persistence
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-DIC-2015 AVZ    Product Backlog Item 6646:Visitas / Recepción: Expedientes
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common.Entrances.Enums;

namespace WSI.Common.Entrances.Entities.Persistence
{
  public class CustomerRecordDetail
  {
    public Int64 RecordDetailId { get; set; }
    public Int64 RecordId { get; set; }
    public Boolean Deleted { get; set; }
    public DateTime Created { get; set; }
    public Nullable<DateTime> Expiration { get; set; }
    public ENUM_TYPE_DETAIL Type { get; set; }
    public String Data { get; set; }
    public byte[] Image { get; set; }
  }
}
