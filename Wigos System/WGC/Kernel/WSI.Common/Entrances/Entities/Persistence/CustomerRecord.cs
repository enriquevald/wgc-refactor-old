﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : CustomerRecord.cs
// 
//   DESCRIPTION : Class CustomerRecord - Persistence
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-DIC-2015 AVZ    Product Backlog Item 6646:Visitas / Recepción: Expedientes
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.Entrances.Entities.Persistence
{
  public class CustomerRecord
  {
    public Int64 RecordId { get; set; }
    public Int64 CustomerId { get; set; }
    public Boolean Deleted { get; set; }
    public DateTime Created { get; set; }
    public Nullable<DateTime> Expiration { get; set; }
  }
}
