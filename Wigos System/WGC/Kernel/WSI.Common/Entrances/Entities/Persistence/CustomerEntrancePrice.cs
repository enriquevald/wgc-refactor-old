﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CustomerEntrancePrice.cs
// 
//   DESCRIPTION: Class to manage customer entrance price
//
//        AUTHOR: YNM
// 
// CREATION DATE: 19-JAN-2016
//  
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-JAN-2016  YNM    First release. PBI-6656
//
//
//
using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.Entrances.Entities.Persistence
{
  public class CustomerEntrancePrice
  {
    public Int64 EntrancePriceId { get; set; }
    public String Description { get; set; }
    public Currency Price { get; set; }
    public Int16 CustomerLevel { get; set; }
  }
}
