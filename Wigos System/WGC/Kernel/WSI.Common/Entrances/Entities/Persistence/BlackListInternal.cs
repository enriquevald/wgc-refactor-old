﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;
using WSI.Common.Entrances.Enums;
using WSI.Common.Utilities.Extensions;
using WSI.Entrances.BlackList.Model.DTO;
using WSI.Entrances.BlackList.Model.Interfaces;


namespace WSI.Common.Entrances.Entities.Persistence
{
  public class BlackListInternal
  {
    public Int64 BlackListInternalId { get; set; }
    public Int64 AccountId { get; set; }
    public DateTime InclusionDate { get; set; }
    public Int16 Reason { get; set; }
    public string ReasonDescription { get; set; }
  }
  public class BlackList : BlackListConnector
  {
    #region CONSTANTS

    private static string TABLE_NAME = "blacklist_internal_block_list";
    private static string TABLE_COLUMN_ACCOUNT_ID = "bkl_account_id";
    private static string TABLE_COLUMN_INCLUSION_DATE = "bkl_inclusion_date";
    private static string TABLE_COLUMN_REASON = "bkl_reason";
    private static string TABLE_COLUMN_REASON_DESCRIP = "bkl_reason_description";


    #endregion

    #region PROPERTIES

    //private static readonly object m_locker = new object();

    //private static bool m_cache_needs_initilization = true;

    //private static DataTable m_dt;

    //private static long m_last_id;

    private static readonly string[] m_fields_array = new[] { TABLE_COLUMN_ACCOUNT_ID, TABLE_COLUMN_INCLUSION_DATE, TABLE_COLUMN_REASON, TABLE_COLUMN_REASON_DESCRIP };

    private static readonly string m_fields = string.Join(",", m_fields_array);

    /// <summary>
    /// refresh thread
    /// </summary>
    //private static readonly System.Threading.Timer m_refresh_thread = new System.Threading.Timer(NeedRefresh);

    #endregion

    #region CONSTRUCTOR

    /// <summary>
    /// Constructor, initializes cache
    /// </summary>
    //public BlackList()
    //{
    //  lock (m_locker)
    //  {
    //    if (m_cache_needs_initilization)
    //    {
    //      InitializeCache();
    //    }
    //  }
    //  m_refresh_thread.Change(TimeSpan.FromMinutes(5), TimeSpan.FromMilliseconds(-1));

    //}

    #endregion

    #region PUBLIC FUNCTIONS

    /// <summary>
    /// The calling order of this DLL
    /// </summary>
    public override int CallOrder
    {
      get { return 1; }
    }

    /// <summary>
    /// Indicates the source of data
    /// </summary>
    public override int SourceType
    {
      get { return (int)ENUM_BLACKLIST_SOURCE.LOCAL; }
    }

    /// <summary>
    /// Method that checks if the given documents exist in the cache
    /// </summary>
    /// <param name="Documents"></param>
    /// <returns></returns>
    public override BlackListServiceResult AreAnyDocumentsBlackListed(IDictionary<int, string> Documents)
    {
      CardData _card_data = new CardData();
      //lock (m_locker)
      //{
        long _int;

        if (Documents.ContainsKey((int)ENUM_BLACKLIST_DOC_TYPE.ACCOUNT) &&
            !Documents[(int)ENUM_BLACKLIST_DOC_TYPE.ACCOUNT].IsNullOrWhiteSpace() &&
             long.TryParse(Documents[(int)ENUM_BLACKLIST_DOC_TYPE.ACCOUNT], out _int) &&
             CardData.DB_CardGetPersonalData(_int, _card_data))
        {
          BlackListServiceResult _result = GetBlackListResult(_card_data.AccountId);
          if (_result.FoundResults)
            return _result;
        }
      //}
      return new BlackListServiceResult { FoundResults = false };
    }

    /// <summary>
    /// allows refresh cache whenever is needed
    /// </summary>
    public override void RefreshCache()
    {
      //lock (m_locker)
      //{
      //  InitializeCache();
      //}
      //m_refresh_thread.Change(TimeSpan.FromMinutes(5), TimeSpan.FromMilliseconds(-1));
    }

    /// <summary>
    /// Dispose class
    /// </summary>
    public override void Dispose()
    {
      //lock (m_locker)
      //{
        //m_cache_needs_initilization = true;
        //m_dt = null;
        //m_last_id = default(long);
      //}
    }

    #endregion

    #region PRIVATE FUNCTIONS

    /// <summary>
    /// Initialize the cache
    /// </summary>
    //private static void InitializeCache()
    //{

    //  m_dt = new DataTable();

    //  using (DB_TRX _trans = new DB_TRX())
    //  {
    //    using (
    //      SqlDataAdapter _rdr = new SqlDataAdapter(string.Format("SELECT {0} FROM {1}", m_fields, TABLE_NAME),
    //        _trans.SqlTransaction.Connection))
    //    {
    //      _rdr.SelectCommand.Transaction = _trans.SqlTransaction;
    //      _rdr.Fill(m_dt);
    //    }
    //    m_last_id = GetChangeMonitorValue(_trans);

    //  }
    //  m_cache_needs_initilization = false;

    //}

    /// <summary>
    /// Periodically checks if a cache refresh is needed
    /// </summary>
    /// <param name="State"></param>
    //private static void NeedRefresh(object State)
    //{
    //  long _last_id;
    //  using (DB_TRX _trans = new DB_TRX())
    //  {
    //    _last_id = GetChangeMonitorValue(_trans);
    //  }
    //  if (_last_id != m_last_id)
    //    lock (m_locker)
    //    {
    //      InitializeCache();
    //    }
    //  m_refresh_thread.Change(TimeSpan.FromMinutes(5), TimeSpan.FromMilliseconds(-1));
    //}

    /// <summary>
    /// Get monitored cache dirty value from database
    /// </summary>
    /// <param name="Trans"></param>
    /// <returns></returns>
    //private static long GetChangeMonitorValue(DB_TRX Trans)
    //{

    //  using (
    //    SqlCommand _cmd = new SqlCommand(string.Format("SELECT COUNT({0}) FROM {1}", m_fields_array[0], TABLE_NAME)))
    //  {
    //    var _res = Trans.ExecuteScalar(_cmd);
    //    return Convert.ToInt64(_res);
    //  }
    //}

    /// <summary>
    /// Form a BlackListServiceResult object from the given data row
    /// </summary>
    /// <param name="ResultList"> Array of results founded </param>
    /// <returns>BlackListServiceResult</returns>
    private static BlackListServiceResult BuildBlackListServiceResult(DataRow[] ResultList)
    {
      int _i = 0;
      BlackListServiceResult _result = new BlackListServiceResult { FoundResults = true };
      _result.Results = new CustomersResult[ResultList.Length];
      foreach (DataRow _row in ResultList)
      {
        CustomersResult _customer = new CustomersResult
        {
          BlackListSource = (int)ENUM_BLACKLIST_SOURCE.LOCAL,
          ExclusionDate = (DateTime)_row.ItemArray[Array.IndexOf(m_fields_array, TABLE_COLUMN_INCLUSION_DATE)],
          ReasonType = (Int16)_row.ItemArray[Array.IndexOf(m_fields_array, TABLE_COLUMN_REASON)],
          ReasonDescription = (string)_row.ItemArray[Array.IndexOf(m_fields_array, TABLE_COLUMN_REASON_DESCRIP)]

        };

        _result.Results[_i] = _customer;
        _i++;
      }
      _result.Count = _result.Results.Length;

      return _result;
    }

    /// <summary>
    /// Method that checks the cache for the account id
    /// </summary>
    /// <param name="AccountId"></param>
    /// <returns></returns>
    private BlackListServiceResult GetBlackListResult(Int64 AccountId)
    {
      DataRow[] _row;
      DataTable _table;
      _table = new DataTable();

      using (DB_TRX _trans = new DB_TRX())
      {
        using (
          SqlDataAdapter _rdr = new SqlDataAdapter(string.Format("SELECT {0} FROM {1} WHERE {2}= " + AccountId.ToString(), m_fields, TABLE_NAME, m_fields_array[0]), _trans.SqlTransaction.Connection))
        {
          _rdr.SelectCommand.Transaction = _trans.SqlTransaction;
          _rdr.Fill(_table);
        }
        _row = _table.Select(string.Format("{0}=" + AccountId.ToString(), m_fields_array[0]));
      }
      return _row.Length > 0 ? BuildBlackListServiceResult(_row) : new BlackListServiceResult { FoundResults = false };

    }

    /// <summary>
    /// Validates characters on strings recived
    /// </summary>
    /// <param name="Value"></param>
    /// <returns></returns>
    private string EscapeLikeValue(string Value)
    {
      StringBuilder sb = new StringBuilder(Value.Length);
      for (int i = 0; i < Value.Length; i++)
      {
        char c = Value[i];
        switch (c)
        {
          case ']':
          case '[':
          case '%':
          case '*':
            sb.Append("[").Append(c).Append("]");
            break;
          case '\'':
            sb.Append("''");
            break;
          default:
            sb.Append(c);
            break;
        }
      }
      return sb.ToString();
    }

    #endregion

  }

}
