﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : CustomerEntrance.cs
// 
//   DESCRIPTION : Class CustomerEntrance - Persistence
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-DIC-2015 AVZ    Product Backlog Item 6646:Visitas / Recepción: Expedientes
// 20-SEP-2016 OVS    Product Backlog Item 
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common.Entrances.Enums;

namespace WSI.Common.Entrances.Entities.Persistence
{
  public class CustomerEntrance
  {
    public DateTime EntranceDate { get; set; }
    public Int64 VisitId { get; set; }
    public Nullable<Int64> CashierSessionId { get; set; }
    public Nullable<Int64> CashierUserId { get; set; }
    public Nullable<Int64> TerminalId { get; set; }
    public Nullable<Int32> EntranceBlockReason { get; set; }
    public Nullable<Int32> EntranceBlockReason2 { get; set; }
    public String EntranceBlockDescription { get; set; }
    public String Remarks { get; set; }
    public Nullable<Int64> TicketEntryId { get; set; }
    public Nullable<Decimal> TicketPriceReal { get; set; }
    public Nullable<Decimal> TicketPricePaid { get; set; }
    public Nullable<Decimal> TicketPriceDiff { get; set; }
    public Nullable<int> DocumentType { get; set; }
    public String DocumentNumber { get; set; }
    public Nullable<Int64> VoucherId { get; set; }
    public String Coupon { get; set; }
    public Nullable<Int64> VoucherSeq { get; set; }
    public DateTime ExpirationDate { get; set; }

		public BlackListResult InBlackList { get; set; }

		public bool OpAcknowledgedBlackList { get; set; }

    public String PriceDescription { get; set; }
	}
}
