﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : CustomerVisit.cs
// 
//   DESCRIPTION : Class CustomerVisit - Persistence
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-DIC-2015 AVZ    Product Backlog Item 6646:Visitas / Recepción: Expedientes
// 06-MAY-2016 SJA    Product Backlog Item 12929:Visitas / Recepción: BlackList - Ampliar funcionalidad
//------------------------------------------------------------------------------

using System;

namespace WSI.Common.Entrances.Entities.Persistence
{
  public class CustomerVisit
  {
    public Int64 VisitId { get; set; }
    public Int32 GamingDay { get; set; }
    public Int64 CustomerId { get; set; }
    public Int32 Level { get; set; }
    public Boolean IsVip { get; set; }
    public sbyte Age { get; set; }
    public sbyte Gender { get; set; }
  }
}
