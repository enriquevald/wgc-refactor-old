﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : AccountsReception.cs
// 
//   DESCRIPTION : Transport Class used between the UI and Common
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-DIC-2015 AVZ    Product Backlog Item 6646:Visitas / Recepción: Expedientes
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common.Entrances.Enums;
using WSI.Common.Junkets;

namespace WSI.Common.Entrances.Entities
{
  public class AccountsReception
  {

    public JunketsBusinessLogic JunketsBusinessLogic
    {
      get;
      set;
    }
    public String FirstName
    {
      get;
      set;
    }

    public String SecondName
    {
      get;
      set;
    }

    public String SecondSurname
    {
      get;
      set;
    }

    public String FirstSurname
    {
      get;
      set;
    }

    public Nullable<DateTime> BirthDate
    {
      get;
      set;
    }

    private CustomerContact contact;
    public CustomerContact Contact
    {
      get
      {
        if (contact == null)
          contact = new CustomerContact();
        return contact;
      }
      set
      {
        contact = value;
      }
    }

    public String Document
    {
      get;
      set;
    }

    public int Gender
    {
      get;
      set;
    }

    public Int64 Nacionality
    {
      get;
      set;
    }

    public Int32 Country
    {
      get;
      set;
    }

    public Int64 AccountId
    {
      get;
      set;
    }

    private List<CustomerAddress> addresses;
    public List<CustomerAddress> Addresses
    {
      get
      {
        if (addresses == null)
          addresses = new List<CustomerAddress>();

        return addresses;
      }
      set
      {
        addresses = value;
      }
    }

    private List<CustomerScannedDocument> scannedDocuments;
    public List<CustomerScannedDocument> ScannedDocuments
    {
      get
      {
        if (scannedDocuments == null)
          scannedDocuments = new List<CustomerScannedDocument>();

        return scannedDocuments;
      }
      set
      {
        scannedDocuments = value;
      }
    }

    public int DocumentType
    {
      get;
      set;
    }

    public String Comments
    {
      get;
      set;
    }

    public Boolean AceptedAgreement
    {
      get;
      set;
    }

    public DateTime AceptedAgreementDate
    {
      get;
      set;
    } 

    public Nullable<DateTime> RecordExpiration
    {
      get;
      set;
    }

    public DateTime? CreationDate { get; set; }

		public BlackListResult InBlackList { get; set; }

		public bool OpAcknowledgedBlackList { get; set; }
    public Dictionary<int,string> DocumentTypes { get; set; }
  }
}
