﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : CustomerScannedDocument.cs
// 
//   DESCRIPTION : Transport Class used between the UI and Common
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-DIC-2015 AVZ    Product Backlog Item 6646:Visitas / Recepción: Expedientes
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Xml.Serialization;
using WSI.Common.Entrances.Enums;
using WSI.Common.Utilities;

namespace WSI.Common.Entrances.Entities
{
  [Serializable, XmlRoot(ElementName = "document")]
  public class CustomerScannedDocument
  {
    [XmlElementAttribute(ElementName = "type")]
    public int DocumentTypeId
    {
      get;
      set;
    }

    [XmlElementAttribute(ElementName = "type_description")]
    public string DocumentTypeDescription
    {
      get;
      set;
    }

    [XmlIgnore]
    public DateTime ScannedDate
    {
      get;
      set;
    }

    [XmlElementAttribute(ElementName = "scanned_date")]
    public string LegacyScannedDate
    {
      get
      {
        return this.ScannedDate.ToString("yyyyMMdd");
      }
      set
      {
        this.ScannedDate = DateTime.ParseExact(value, "yyyyMMdd", CultureInfo.InvariantCulture);
      }
    }

    [XmlIgnore]
    public byte[] Image
    {
      get;
      set;
    }

    [XmlIgnore]
    public Nullable<DateTime> Expiration
    {
      get;
      set;
    }

    public string ToXML()
    {
      return XmlSerializationHelper.Serialize<CustomerScannedDocument>(this);
    }
  }
}
