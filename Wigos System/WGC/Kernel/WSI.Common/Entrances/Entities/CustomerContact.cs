﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : CustomerContact.cs
// 
//   DESCRIPTION : Transport Class used between the UI and Common
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-DIC-2015 AVZ    Product Backlog Item 6646:Visitas / Recepción: Expedientes
//------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.Entrances.Entities
{
  public class CustomerContact
  {
    public String FirstTelephone
    {
      get;
      set;
    }

    public String Email
    {
      get;
      set;
    }

    public String SecondTelephone
    {
      get;
      set;
    }

    public String Twitter
    {
      get;
      set;
    }

    public String Facebook
    {
      get;
      set;
    }
  }
}
