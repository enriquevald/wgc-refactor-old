﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : Entrance.cs
// 
//   DESCRIPTION : Class to manage visits and entrance into the casino
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-DIC-2015  AVZ    Product Backlog Item  6646:Visitas / Recepción: Expedientes
// 27-ENE-2016  AVZ    Product Backlog Item  8673:Visitas / Recepción: Configuración de Modo de Recepción
// 09-MAY-2015  JBP    Product Backlog Item 12931:Visitas / Recepción: 2º Ticket LOPD
// 10-MAY-2015  JBP    Product Backlog Item 12927:Visitas / Recepción: Caducidad de las entradas variable
// 20-MAY-2015  FJC    Fixed Bug 13382:Recepción - versión Junio. -Error al registrar una entrada-
// 09-NOV-2016  SMN    Fixed Bug 20329:Recepción - Error al registrar entrada con Mode = 0
// 16-JAN-2017  ETP    Fixed Bug 22747: Multivisa: needed valid visid.
// 04-MAY-2017  JCA    Fixed Bug 27225:Pop-up de cobro de segunda entrada
// 06-OCT-2017  MS     PBI 30006: Reception - Register customer output
// 10-OCT-2017  DPC    [WIGOS-5525]: Reception - Register customer output - Cashier button - Exit logic
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common.Entrances.Entities;
using WSI.Common.Entrances.Entities.Persistence;
using WSI.Common.Entrances.Enums;
using WSI.Common.Junkets;

namespace WSI.Common.Entrances
{
  public class Entrance
  {
    #region Constants

    public const string GP_RECEPTION_GROUP = "Reception";
    public const string GP_RECEPTION_MODE = "Mode";
    const double DAYS_IN_YEAR = 365.25;

    #endregion

    /// <summary>
    /// Register customer entry to the casino
    /// </summary>
    /// <param name="AccountsReceptionInfo">Customer information</param>
    /// <param name="Ticket">Entrance ticket</param>
    /// <param name="DocumentType">Document type used to enter</param>
    /// <param name="DocumentNumber">Document number used to enter</param>
    /// <returns>Entrance data</returns>
    public static Boolean RecordEntry(AccountsReception AccountsReceptionInfo, TicketData Ticket, out CustomerEntrance _entrance, SqlTransaction _trx)
    {
      try
      {
        Int64 _customer_id = AccountsReceptionInfo.AccountId;
        int DocumentType = AccountsReceptionInfo.DocumentType;
        string DocumentNumber = AccountsReceptionInfo.Document;
        Decimal _diff;

        _diff = 0;

        //Buscar Visita por CustomerId y fecha actual
        CustomerVisit _visit = DB_GetVisitByDate(_customer_id, Misc.TodayOpening());

        //TODO: Ver los datos faltantes!!!!
        _entrance = new CustomerEntrance();
        _entrance.EntranceDate = WGDB.Now;
        _entrance.DocumentType = DocumentType;
        _entrance.DocumentNumber = DocumentNumber;
        _entrance.CashierSessionId = WSI.Common.CommonCashierInformation.SessionId;
        _entrance.CashierUserId = WSI.Common.CommonCashierInformation.UserId;
        _entrance.TerminalId = WSI.Common.CommonCashierInformation.TerminalId;
        _entrance.EntranceBlockDescription = string.Empty; //Me lo pasan por cajero
        _entrance.EntranceBlockReason = null; //Me lo pasan por cajero
        _entrance.EntranceBlockReason2 = null; //Me lo pasan por cajero
        _entrance.EntranceBlockDescription = string.Empty; //Me lo pasan por cajero
        _entrance.Remarks = string.Empty; //Me lo pasan por cajero
        _entrance.TicketEntryId = Ticket.EntryId;
        _entrance.TicketPriceReal = Ticket.PriceReal;
        _entrance.TicketPricePaid = Ticket.PricePaid;
        _entrance.PriceDescription = Ticket.priceDescription;

        //VALIDATE NULL
        _diff = (Decimal)_entrance.TicketPriceReal - (Decimal)_entrance.TicketPricePaid;

        _entrance.TicketPriceDiff = Math.Abs(_diff);
        _entrance.VoucherId = Ticket.VoucherId;
        _entrance.Coupon = Ticket.Coupon;
        _entrance.VoucherSeq = Ticket.Sequence;
        _entrance.ExpirationDate = Entrance.GetExpirationDate(Ticket.ValidGamingDays);

        //Si no existe el cliente, crearlo
        if (!Customers.ExistCustomer(_customer_id, _trx))
        {
          if (!Customers.CreateCustomer(AccountsReceptionInfo, _trx))
            throw new Exception("CreateCustomer: Can't create customer");
        }
        else
        {
          if (AccountsReceptionInfo.AceptedAgreement && AccountsReceptionInfo.AceptedAgreementDate == DateTime.MinValue)
          {
            Customers.SaveAgreement(AccountsReceptionInfo.AccountId, _trx);
          }
        }

        _entrance.InBlackList = AccountsReceptionInfo.InBlackList;
        _entrance.OpAcknowledgedBlackList = AccountsReceptionInfo.OpAcknowledgedBlackList;

        if (_visit != null)
        {
          //Si el cliente ya ingresó en la jornada actual, solamente crear la entrada.
          _entrance.VisitId = _visit.VisitId;
          if (!DB_InsertEntrance(_entrance, _trx))
            throw new Exception("DB_InsertEntrance: Can't insert entrance");
        }
        else
        {

          ////Si el cliente NO ingresó en la jornada actual, crear la visita y la entrada.
          CardData _card_data = new CardData();
          if (!CardData.DB_CardGetPersonalData(_customer_id, _card_data, _trx))
            throw new Exception("DB_CardGetPersonalData: Can't get account data");

          _visit = new CustomerVisit
          {
            CustomerId = _customer_id,
            GamingDay = Int32.Parse(Misc.TodayOpening().ToString("yyyyMMdd")),
            IsVip = _card_data.PlayerTracking.HolderIsVIP,
            Level = (Int16)_card_data.PlayerTracking.CardLevel,
            Gender =(sbyte)_card_data.PlayerTracking.HolderGender
          };

          if (!CreateVisit(_visit, _entrance, _trx))
            throw new Exception("DB_InsertVisit: Can't insert visit");
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        throw _ex;
      }
    }

    public static Boolean RecordExit(AccountsReception AccountsReceptionInfo, SqlTransaction _trx, out Int32 VisitsUpdated)
    {
      CustomerVisit _visit;

      VisitsUpdated = 0;

      try
      {
        _visit = DB_GetVisitByDate(AccountsReceptionInfo.AccountId, Misc.TodayOpening());

        if (_visit == null)
        {
          return true;
        }

        if (!DB_UpdateEntrance(_visit.VisitId, _trx, out VisitsUpdated))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Get visit by customer and date
    /// </summary>
    /// <param name="CustomerId"></param>
    /// <param name="Date"></param>
    /// <param name="SqlTrx"></param>
    /// <returns>Visit information</returns>
    private static CustomerVisit DB_GetVisitByDate(Int64 CustomerId, DateTime Date, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      CustomerVisit _visit_data = null;

      _sb = new StringBuilder();
      _sb.AppendLine("  SELECT  CUT_VISIT_ID, ");
      _sb.AppendLine("          CUT_GAMING_DAY, ");
      _sb.AppendLine("          CUT_CUSTOMER_ID, ");
      _sb.AppendLine("          CUT_LEVEL, ");
      _sb.AppendLine("          CUT_IS_VIP ");
      _sb.AppendLine("  FROM    CUSTOMER_VISITS  ");
      _sb.AppendLine("  WHERE   CUT_CUSTOMER_ID = @pCustomerId  ");
      _sb.AppendLine("    AND   CUT_GAMING_DAY  = @pDate  ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCustomerId", SqlDbType.BigInt).Value = CustomerId;
          _cmd.Parameters.Add("@pDate", SqlDbType.Int).Value = Int32.Parse(Date.ToString("yyyyMMdd"));

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read() && !_reader.IsDBNull(0))
            {
              _visit_data = BuildVisitData(_reader);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        throw _ex;
      }

      return _visit_data;
    }

    /// <summary>
    /// Get visit by customer and date
    /// </summary>
    /// <param name="CustomerId"></param>
    /// <param name="Date"></param>
    /// <returns>Visit information</returns>
    private static CustomerVisit DB_GetVisitByDate(Int64 CustomerId, DateTime Date)
    {
      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          return DB_GetVisitByDate(CustomerId, Date, _trx.SqlTransaction);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        throw _ex;
      }
    }

    private static CustomerVisit BuildVisitData(SqlDataReader Reader)
    {
      CustomerVisit _data = new CustomerVisit();
      _data.VisitId = (Int64)Reader["CUT_VISIT_ID"];
      _data.GamingDay = (Int32)Reader["CUT_GAMING_DAY"];
      _data.CustomerId = (Int64)Reader["CUT_CUSTOMER_ID"];
      _data.Level = (Int32)Reader["CUT_LEVEL"];
      _data.IsVip = (Boolean)Reader["CUT_IS_VIP"];
      return _data;
    }

    /// <summary>
    /// Insert the entrance into the database
    /// </summary>
    /// <param name="EntranceInfo">Entrance information</param>
    /// <param name="SqlTrx"></param>
    /// <returns>Success/Fail</returns>
    private static Boolean DB_InsertEntrance(CustomerEntrance EntranceInfo,
                                             SqlTransaction SqlTrx)
    {
      Int32 _visit_updated;

      //Fill in Exit date if null in previous records for today
      if (!DB_UpdateEntrance(EntranceInfo.VisitId, SqlTrx, out _visit_updated))
      {
        return false;
      }

      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("  INSERT INTO   CUSTOMER_ENTRANCES                ");
      _sb.AppendLine("              ( CUE_ENTRANCE_DATETIME             ");
      _sb.AppendLine("              , CUE_VISIT_ID                      ");
      _sb.AppendLine("              , CUE_CASHIER_SESSION_ID            ");
      _sb.AppendLine("              , CUE_CASHIER_USER_ID               ");
      _sb.AppendLine("              , CUE_CASHIER_TERMINAL_ID           ");
      _sb.AppendLine("              , CUE_ENTRANCE_BLOCK_REASON         ");
      _sb.AppendLine("              , CUE_ENTRANCE_BLOCK_REASON2        ");
      _sb.AppendLine("              , CUE_ENTRANCE_BLOCK_DESCRIPTION    ");
      _sb.AppendLine("              , CUE_REMARKS                       ");
      _sb.AppendLine("              , CUE_TICKET_ENTRY_ID               ");
      _sb.AppendLine("              , CUE_TICKET_ENTRY_PRICE_REAL       ");
      _sb.AppendLine("              , CUE_DOCUMENT_TYPE                 ");
      _sb.AppendLine("              , CUE_DOCUMENT_NUMBER               ");
      _sb.AppendLine("              , CUE_VOUCHER_ID                    ");
      _sb.AppendLine("              , CUE_COUPON                        ");
      _sb.AppendLine("              , CUE_VOUCHER_SEQUENCE              ");
      _sb.AppendLine("              , CUE_ENTRANCE_EXPIRATION           ");
      _sb.AppendLine("              , CUE_TICKET_ENTRY_PRICE_PAID       ");
      _sb.AppendLine("              , CUE_TICKET_ENTRY_PRICE_DIFFERENCE ");
      _sb.AppendLine("              , CUT_INCLUDED_IN_BLACK_LIST        ");
      _sb.AppendLine("              , CUT_OP_ACKNOWLEDGED_BLACK_LIST    ");
      _sb.AppendLine("              , CUE_PRICE_DESCRIPTION  )          ");
      _sb.AppendLine("      VALUES                                      ");
      _sb.AppendLine("              ( @pEntranceDatetime                ");
      _sb.AppendLine("              , @pVisitId                         ");
      _sb.AppendLine("              , @pCashierSessionId                ");
      _sb.AppendLine("              , @pCashierUserId                   ");
      _sb.AppendLine("              , @pCashierTerminalId               ");
      _sb.AppendLine("              , @pEntranceBlockReason             ");
      _sb.AppendLine("              , @pEntranceBlockReason2            ");
      _sb.AppendLine("              , @pEntranceBlockReasonDescription  ");
      _sb.AppendLine("              , @pRemarks                         ");
      _sb.AppendLine("              , @pTicketEntryId                   ");
      _sb.AppendLine("              , @pTicketPriceReal                 ");
      _sb.AppendLine("              , @pDocumentType                    ");
      _sb.AppendLine("              , @pDocumentNumber                  ");
      _sb.AppendLine("              , @pVoucherId                       ");
      _sb.AppendLine("              , @pCoupon                          ");
      _sb.AppendLine("              , @pVoucherSeq                      ");
      _sb.AppendLine("              , @pEntranceExpiration              ");
      _sb.AppendLine("              , @pTicketPricePaid                 ");
      _sb.AppendLine("              , @pTicketPriceDifference	          ");
      _sb.AppendLine("              , @pIsInBlackList                   ");
      _sb.AppendLine("              , @pHasReadBlackList                ");
      _sb.AppendLine("              , @pPriceDescription )              ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pEntranceDatetime", SqlDbType.DateTime).Value = EntranceInfo.EntranceDate;
          _cmd.Parameters.Add("@pVisitId", SqlDbType.BigInt).Value = EntranceInfo.VisitId;
          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = EntranceInfo.CashierSessionId.HasValue ? EntranceInfo.CashierSessionId : (object)DBNull.Value;
          _cmd.Parameters.Add("@pCashierUserId", SqlDbType.BigInt).Value = EntranceInfo.CashierUserId.HasValue ? EntranceInfo.CashierUserId : (object)DBNull.Value;
          _cmd.Parameters.Add("@pCashierTerminalId", SqlDbType.BigInt).Value = EntranceInfo.TerminalId.HasValue ? EntranceInfo.TerminalId : (object)DBNull.Value;
          _cmd.Parameters.Add("@pEntranceBlockReason", SqlDbType.Int).Value = EntranceInfo.EntranceBlockReason.HasValue ? EntranceInfo.EntranceBlockReason : (object)DBNull.Value;
          _cmd.Parameters.Add("@pEntranceBlockReason2", SqlDbType.Int).Value = EntranceInfo.EntranceBlockReason2.HasValue ? EntranceInfo.EntranceBlockReason2 : (object)DBNull.Value;
          _cmd.Parameters.Add("@pEntranceBlockReasonDescription", SqlDbType.VarChar).Value = EntranceInfo.EntranceBlockDescription;
          _cmd.Parameters.Add("@pRemarks", SqlDbType.VarChar).Value = EntranceInfo.Remarks;
          _cmd.Parameters.Add("@pTicketEntryId", SqlDbType.BigInt).Value = EntranceInfo.TicketEntryId.HasValue ? EntranceInfo.TicketEntryId : (object)DBNull.Value;
          _cmd.Parameters.Add("@pTicketPriceReal", SqlDbType.Decimal).Value = EntranceInfo.TicketPriceReal.HasValue ? EntranceInfo.TicketPriceReal : (object)DBNull.Value;
          _cmd.Parameters.Add("@pDocumentType", SqlDbType.Int).Value = EntranceInfo.DocumentType.HasValue ? EntranceInfo.DocumentType : (object)DBNull.Value;
          _cmd.Parameters.Add("@pDocumentNumber", SqlDbType.VarChar).Value = EntranceInfo.DocumentNumber;
          _cmd.Parameters.Add("@pVoucherId", SqlDbType.BigInt).Value = EntranceInfo.VoucherId.HasValue ? EntranceInfo.VoucherId : (object)DBNull.Value;
          _cmd.Parameters.Add("@pCoupon", SqlDbType.VarChar).Value = EntranceInfo.Coupon;
          _cmd.Parameters.Add("@pVoucherSeq", SqlDbType.BigInt).Value = EntranceInfo.VoucherSeq.HasValue ? EntranceInfo.VoucherSeq : (object)DBNull.Value; ;
          _cmd.Parameters.Add("@pTicketPricePaid", SqlDbType.Decimal).Value = EntranceInfo.TicketPricePaid.HasValue ? EntranceInfo.TicketPricePaid : (object)DBNull.Value;
          _cmd.Parameters.Add("@pTicketPriceDifference", SqlDbType.Decimal).Value = EntranceInfo.TicketPriceDiff.HasValue ? EntranceInfo.TicketPriceDiff : (object)DBNull.Value;
          _cmd.Parameters.Add("@pIsInBlackList", SqlDbType.Int).Value = EntranceInfo.InBlackList;
          _cmd.Parameters.Add("@pHasReadBlackList", SqlDbType.Bit).Value = EntranceInfo.OpAcknowledgedBlackList;

          if (String.IsNullOrEmpty(EntranceInfo.PriceDescription))
          {
            _cmd.Parameters.Add("@pPriceDescription", SqlDbType.VarChar).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pPriceDescription", SqlDbType.VarChar).Value = EntranceInfo.PriceDescription;
          }

          if (EntranceInfo.ExpirationDate == DateTime.MinValue)
          {
            _cmd.Parameters.Add("@pEntranceExpiration", SqlDbType.DateTime).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pEntranceExpiration", SqlDbType.DateTime).Value = EntranceInfo.ExpirationDate;          
          }

          
          _cmd.ExecuteNonQuery();
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }

      return true;
    }

    /// <summary>
    /// Set a exit date and aexit reason
    /// </summary>
    /// <param name="VisitID"></param>
    /// <param name="SqlTrx"></param>
    /// <param name="VisitUpdated"></param>
    /// <returns></returns>
    private static Boolean DB_UpdateEntrance(Int64 VisitID, SqlTransaction SqlTrx, out Int32 VisitUpdated)
    {

      StringBuilder _sb;

      VisitUpdated = 0;

      try
      {

        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   CUSTOMER_ENTRANCES			   ");
        _sb.AppendLine("    SET   CUE_EXIT_DATETIME = @pNow	 ");
        _sb.AppendLine("        , CUE_EXIT_REASON = @pReason ");
        _sb.AppendLine("  WHERE   CUE_EXIT_DATETIME IS NULL	 ");
        _sb.AppendLine("    AND   CUE_VISIT_ID = @pVisitID	 ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pNow", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pReason", SqlDbType.Int).Value = ENUM_EXIT_REASON.RECEPTION;
          _cmd.Parameters.Add("@pVisitID", SqlDbType.BigInt).Value = VisitID;
          
         VisitUpdated =  _cmd.ExecuteNonQuery();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);        
      }

      return false;
    }

    /// <summary>
    /// Create visit and entrance
    /// </summary>
    /// <param name="VisitInfo">Visit information</param>
    /// <param name="EntranceInfo">Entrance information</param>
    /// <param name="SqlTrx"></param>
    /// <returns>Success/Fail</returns>
    private static Boolean CreateVisit(CustomerVisit VisitInfo, CustomerEntrance EntranceInfo, SqlTransaction SqlTrx)
    {
      if (!DB_InsertVisit(VisitInfo, SqlTrx))
        return false;

      EntranceInfo.VisitId = VisitInfo.VisitId;
      if (!DB_InsertEntrance(EntranceInfo, SqlTrx))
        return false;

      return true;
    }

    /// <summary>
    /// Insert the visit into the database
    /// </summary>
    /// <param name="VisitInfo">Visit information</param>
    /// <param name="SqlTrx">/param>
    /// <returns>Success/Fail</returns>
    private static Boolean DB_InsertVisit(CustomerVisit VisitInfo,
                                                  SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("  INSERT INTO  CUSTOMER_VISITS      ");
      _sb.AppendLine("          ( CUT_GAMING_DAY          ");
      _sb.AppendLine("          , CUT_CUSTOMER_ID         ");
      _sb.AppendLine("          , CUT_LEVEL               ");
      _sb.AppendLine("          , CUT_IS_VIP              ");
      _sb.AppendLine("          , CUT_AGE                 ");
      _sb.AppendLine("          , CUT_GENDER )            ");
      _sb.AppendLine("  VALUES                            ");
      _sb.AppendLine("          ( @pGamingDay             ");
      _sb.AppendLine("          , @pCustomerId            ");
      _sb.AppendLine("          , @pLevel                 ");
      _sb.AppendLine("          , @pIsVip                 ");
      _sb.AppendLine("          , @pAge                   ");
      _sb.AppendLine("          , @pGender )              ");
      _sb.AppendLine("  SET @pOutputId = SCOPE_IDENTITY() ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pGamingDay", SqlDbType.Int).Value = VisitInfo.GamingDay;
          _cmd.Parameters.Add("@pCustomerId", SqlDbType.BigInt).Value = VisitInfo.CustomerId;
          _cmd.Parameters.Add("@pLevel", SqlDbType.Int).Value = VisitInfo.Level;
          _cmd.Parameters.Add("@pIsVip", SqlDbType.Bit).Value = VisitInfo.IsVip;
          _cmd.Parameters.Add("@pAge", SqlDbType.TinyInt).Value = VisitInfo.Age;
          _cmd.Parameters.Add("@pGender", SqlDbType.Int).Value = VisitInfo.Gender;
          SqlParameter pOutputId = _cmd.Parameters.Add("@pOutputId", SqlDbType.BigInt, 8, "CUT_VISIT_ID");
          pOutputId.Direction = ParameterDirection.Output;

          _cmd.ExecuteNonQuery();

          VisitInfo.VisitId = (Int64)pOutputId.Value;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        return false;
      }

      return true;
    }
    /// <summary>
    /// Check if customer has a visit with a voucher in this working date 
    /// </summary>
    /// <param name="CustomerId"></param>
    /// <param name="VoucherId"></param>
    /// <returns></returns>
    public static Boolean CustomerHasValidVisitWithVoucher(Int64 CustomerId, out Int64 VoucherId, out DateTime EntraceExpiration)
    {
      StringBuilder _sb;
      _sb = new StringBuilder();
      VoucherId = 0;
      EntraceExpiration = DateTime.MinValue;

      DateTime _working_date = Misc.TodayOpening();

      _sb.AppendLine("    SELECT   TOP 1 CUE_VOUCHER_ID              ");
      _sb.AppendLine("           , CUE_ENTRANCE_EXPIRATION           ");
      _sb.AppendLine("      FROM   CUSTOMER_ENTRANCES                ");
      _sb.AppendLine("INNER JOIN   CUSTOMER_VISITS                   ");
      _sb.AppendLine("        ON   CUT_VISIT_ID = CUE_VISIT_ID       ");
      _sb.AppendLine("     WHERE   CUE_ENTRANCE_EXPIRATION > @pdate  ");
      _sb.AppendLine("       AND   CUT_CUSTOMER_ID = @pCustomerId    ");
      _sb.AppendLine("       AND   ( CUE_VOUCHER_ID IS NOT NULL      ");
      _sb.AppendLine("               AND   CUE_VOUCHER_ID <> 0)      ");

      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _trx.SqlTransaction.Connection, _trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pCustomerId", SqlDbType.BigInt).Value = CustomerId;
            _cmd.Parameters.Add("@pDate", SqlDbType.DateTime).Value = _working_date;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (_reader.Read() && !_reader.IsDBNull(0))
              {
                VoucherId = (Int64)_reader[0];
                EntraceExpiration = _reader.IsDBNull(1) ? DateTime.MinValue : (DateTime)_reader[1];
                return true;
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }
    /// <summary>
    /// Check if Customer has visited today.
    /// </summary>
    /// <param name="CustomerId"></param>
    /// <returns></returns>
    public static Boolean CustomerHasVisitedToday(Int64 CustomerId)
    {
      StringBuilder _sb;

      DateTime _date;
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          //Find if exists a previuos visit the same day 
          _sb = new StringBuilder();
          _sb.AppendLine("SELECT TOP 1  CUT_VISIT_ID                    ");
          _sb.AppendLine("  FROM        CUSTOMER_VISITS                 ");
          _sb.AppendLine(" WHERE        CUT_CUSTOMER_ID = @pCustomerId  ");
          _sb.AppendLine("   AND        CUT_GAMING_DAY  = @pGamingDay   ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pCustomerId", SqlDbType.BigInt).Value = CustomerId;
            _date =  Misc.TodayOpening().Date;
            _cmd.Parameters.Add("@pGamingDay", SqlDbType.Int).Value = Int32.Parse(Misc.TodayOpening().ToString("yyyyMMdd"));

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (_reader.Read())
              {
                return true;
              }
            }
          }
        }
      }
      catch (Exception Ex)
      {
        string test = Ex.Message;
        Log.Exception(Ex);
      }
      return false;

    }//CustomerHasVisitedToday


    /// <summary>
    /// Get last entrance voucher to reprint it
    /// </summary>
    /// <param name="VoucherId"></param>
    /// <param name="Voucher"></param>
    public static void GetLastEntranceVoucher(Int64 VoucherId, out Voucher Voucher)
    {
      DataTable _dt;

      Voucher = null;

      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          Voucher = new Voucher(PrintMode.Print, CashierVoucherType.CustomerEntrance, _trx.SqlTransaction);
          _dt = VoucherManager.GetVouchersFromId(VoucherSource.FROM_VOUCHER, VoucherId, _trx.SqlTransaction);
          if (_dt != null)
          {
            foreach (DataRow _row in _dt.Rows)
            {
              Voucher.VoucherId = (Int64)_row[0];
              Voucher.VoucherHTML = (String)_row[1];
              Voucher.VoucherSeqNumber = (long)_row[2];
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }

    /// <summary>
    /// Get Expiration date
    /// </summary>
    /// <returns></returns>
    public static DateTime GetExpirationDate(Int32 ValidGamingDays)
    {
      return Misc.TodayOpening().AddDays((double)(ValidGamingDays));
    }

    /// <summary>
    /// Get configured reception mode
    /// </summary>
    /// <returns></returns>
    public static ENUM_RECEPTION_MODE GetReceptionMode()
    {
      return (ENUM_RECEPTION_MODE)GeneralParam.GetInt32(GP_RECEPTION_GROUP, GP_RECEPTION_MODE, (int)ENUM_RECEPTION_MODE.Lite);
    }

    /// <summary>
    /// Get if exist customer
    /// </summary>
    /// <param name="CustomerId"></param>
    /// <returns></returns>
    public static Boolean IsNewCustomer(Int64? CustomerId)
    {
      StringBuilder _sb;
      _sb = new StringBuilder();

      if (!CustomerId.HasValue)
      {
        return true;
      }

      _sb.AppendLine("  SELECT  TOP 1 CUT_CUSTOMER_ID                 ");
      _sb.AppendLine("   FROM         CUSTOMER_VISITS                 ");
      _sb.AppendLine("  WHERE         CUT_CUSTOMER_ID = @pCustomerId  ");

      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _trx.SqlTransaction.Connection, _trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pCustomerId", SqlDbType.BigInt).Value = CustomerId;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (_reader.Read() && !_reader.IsDBNull(0))
              {
                return false;
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return true;
    }
  }

  public class TicketData
  {
    public Int64 EntryId;
    public Decimal PriceReal;
    public Decimal PricePaid;
    public Int64 VoucherId;
    public String Coupon;
    public Boolean Agreement;
    public Int32 ValidGamingDays;
    public DateTime ExpireDate;
    public Int64 Sequence;
    public CurrencyExchangeResult ExchangeResult;
    public String priceDescription;
    public JunketsBusinessLogic junketInfo;
  }
}
