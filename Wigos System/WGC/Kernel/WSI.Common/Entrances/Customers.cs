﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : Customer.cs
// 
//   DESCRIPTION : Class to manage customers
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 03-DIC-2015  AVZ    Product Backlog Item  6646:Visitas / Recepción: Expedientes
// 11-FEB-2015  AVZ    Product Backlog Item  9046:Visitas / Recepción: BlackLists
// 09-MAY-2015  JBP    Product Backlog Item 12931:Visitas / Recepción: 2º Ticket LOPD
// 10-MAY-2015  JBP    Product Backlog Item 12927:Visitas / Recepción: Caducidad de las entradas variable
// 06-JUN-2016  PDM    Product Backlog Item 13828:Mejora Recepción - Controlar Log Errores 
// 14-JUL-2016  PDM    PBI 15448:Visitas / Recepción: MEJORAS - Cajero & GUI - Nuevo motivo de bloqueo
// 03-AGO-2016  FJC    Bug 16072:Recepción: Errores Varios
// 12-JUL-2017  DPC    WIGOS-3564: [Ticket #7077] Incidencia en San Sebastián. No se puede registrar clientes "Lista de Prohibidos deniega acceso"
// 21-SEP-2017  AMF    Bug 29878:[WIGOS-5065][WIGOS-4646]: Exception when the user do any modification in the "Account data editor"
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using System.Xml;
using WSI.Common.Utilities;
using WSI.Entrances.BlackList.Model.DTO;
using WSI.Entrances.BlackList.Model;
using WSI.Common.Utilities.Extensions;
using WSI.Common.Entrances.Entities.Persistence;
using WSI.Common.Entrances.Enums;
using WSI.Common.Entrances.Entities;
using System.Globalization;
using System.Windows.Forms;
using WSI.Common.Exceptions;

namespace WSI.Common.Entrances
{
  public delegate void BlackListResultHandler(BlackListResultInfo Event);
  public delegate void ExpiredDocumentsResultHandler(ExpiredDocumentsResultInfo Event);

  public class BlackListResultInfo
  {
    public BlackListResultInfo()
    {
      CustomerResults = new BlackListResultCustomer[] { };
    }
    public BlackListResult Result
    {
      get
      {
        if (Processing) return BlackListResult.Processing;
        if (Error) return BlackListResult.Error;
        switch (CustomerResults.Length)
        {
          case 0:
            return BlackListResult.NotInBlackList;
          case 1:
            return BlackListResult.InBlackList;
          default:
            return BlackListResult.WithMatches;

        }
      }
    }
    public int CustomerCount
    {
      get { return CustomerResults.Length; }
    }
    public BlackListResultCustomer[] CustomerResults { get; set; }
    public Int64 AccountId { get; set; }

    public bool Processing { get; set; }
    public bool Error { get; set; }
  }

  public class BlackListResultCustomer
  {

    public String CustomerFullName { get; set; }
    public ACCOUNT_HOLDER_ID_TYPE CustomerDocType { get; set; }
    public String CustumerDocNmbr { get; set; }
    public DateTime? InclusionDate { get; set; }
    public ENUM_BLACKLIST_SOURCE Source { get; set; }
    public ENUM_BLACKLIST_REASON ReasonType { get; set; }
    public string ReasonDescription { get; set; }
    public bool NotPermitedToEnter { get; set; }
    public string[] Messages { get; set; }
    public string[] Lists { get; set; }
  }

  public enum BlackListResult
  {
    None = 0,
    Error = 1,
    Processing = 2,
    WithMatches = 3,
    InBlackList = 4,
    NotInBlackList = 5
  }

  public class ExpiredDocumentsResultInfo
  {
    public ExpiredDocumentsResult Result { get; set; }
  }

  public enum ExpiredDocumentsResult
  {
    None = 0,
    Error = 1,
    Processing = 2,
    HasExpiredDocuments = 3,
    DoesNotHaveExpiredDocuments = 4
  }

  public static class Customers
  {
    #region Constants

    public const string GP_RECEPTION_GROUP = "Reception";
    public const string GP_EXPIRATION_DAYS = "Record.ExpirationDays";
    public const string GP_BLACKLIST_SERVICE_GROUP = "BlackListService";
    public const string GP_BLACKLIST_SERVICE_IP = "WCP.FixedIP";
    public const string GP_BLACKLIST_SERVICE_PORT = "WCP.Port";
    public const int ELAPSED_LOG_TICK = 300000; // 5 minutes

    private static int m_stats_log_tick = 0;

    #endregion

    private static String m_last_active_IP;
    private static Control m_remote_control_blacklist;
    private static BlackListResultHandler m_remote_handler_blacklist;

    private static Control m_remote_control_expired_docs;
    private static ExpiredDocumentsResultHandler m_remote_handler_expired_docs;


    /// <summary>
    /// Register handler
    /// </summary>
    /// <param name="Ctrl"></param>
    /// <param name="Handler"></param>
    public static void RegisterHandler(Control Ctrl, BlackListResultHandler Handler)
    {
      if (Handler == null)
      {
        // Unregister
        m_remote_control_blacklist = null;
        m_remote_handler_blacklist = null;
      }
      else
      {
        m_remote_handler_blacklist = Handler;
        m_remote_control_blacklist = Ctrl;
      }
    }

    /// <summary>
    /// Register handler - expired documents
    /// </summary>
    /// <param name="Ctrl"></param>
    /// <param name="Handler"></param>
    public static void RegisterHandlerExpiredDocuments(Control Ctrl, ExpiredDocumentsResultHandler Handler)
    {
      if (Handler == null)
      {
        // Unregister
        m_remote_control_expired_docs = null;
        m_remote_handler_expired_docs = null;
      }
      else
      {
        m_remote_handler_expired_docs = Handler;
        m_remote_control_expired_docs = Ctrl;
      }
    }

    /// <summary>
    /// Notify changes to remote objects - blacklist
    /// </summary>
    /// <param name="Event"></param>
    private static void NotifyToRemoteBlackList(BlackListResultInfo Event)
    {
      try
      {
        Control _ctrl;
        BlackListResultHandler _hdlr;

        _ctrl = m_remote_control_blacklist;
        _hdlr = m_remote_handler_blacklist;

        if (_ctrl == null) return;
        if (_hdlr == null) return;

        if (_ctrl.InvokeRequired)
        {
          _ctrl.BeginInvoke(new BlackListResultHandler(NotifyToRemoteBlackList), Event);

          return;
        }

        _hdlr(Event);
      }
      catch
      {
        //
      }
    }

    /// <summary>
    /// Notify changes to remote objects - expired documents
    /// </summary>
    /// <param name="Event"></param>
    private static void NotifyToRemoteExpiredDocuments(ExpiredDocumentsResultInfo Event)
    {
      try
      {
        Control _ctrl;
        ExpiredDocumentsResultHandler _hdlr;

        _ctrl = m_remote_control_expired_docs;
        _hdlr = m_remote_handler_expired_docs;

        if (_ctrl == null)
          return;
        if (_hdlr == null)
          return;

        if (_ctrl.InvokeRequired)
        {
          _ctrl.BeginInvoke(new ExpiredDocumentsResultHandler(NotifyToRemoteExpiredDocuments), Event);

          return;
        }

        _hdlr(Event);
      }
      catch
      {
        //
      }

    }

    /// <summary>
    /// Save customer and documents
    /// </summary>
    /// <param name="AccountsReceptionInfo">Customer information</param>
    /// <param name="SqlTrx"></param>
    /// <returns>Success/Fail</returns>
    public static Boolean SaveCustomer(AccountsReception AccountsReceptionInfo,
                                                  SqlTransaction SqlTrx)
    {
      return ExistCustomer(AccountsReceptionInfo.AccountId, SqlTrx) ? UpdateCustomer(AccountsReceptionInfo, SqlTrx) : CreateCustomer(AccountsReceptionInfo, SqlTrx);
    }

    /// <summary>
    /// Chek if customer exist
    /// </summary>
    /// <param name="CustomerId"></param>
    /// <returns></returns>
    public static Boolean ExistCustomer(Int64 CustomerId)
    {
      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          return ExistCustomer(CustomerId, _trx.SqlTransaction);
        }
      }
      catch (Exception _ex)
      {
        Log.Error("DB_SaveCustomer: There was an error saving record");
        Log.Exception(_ex);
        throw;
      }
    }

    /// <summary>
    /// Chek if customer exist
    /// </summary>
    /// <param name="CustomerId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean ExistCustomer(Int64 CustomerId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("  SELECT  COUNT(CUS_CUSTOMER_ID) AS COUNT ");
      _sb.AppendLine("  FROM    CUSTOMERS  ");
      _sb.AppendLine("  WHERE   CUS_CUSTOMER_ID = @pCustomerId  ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCustomerId", SqlDbType.BigInt).Value = CustomerId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            _reader.Read();
            return (Int32)_reader["COUNT"] > 0;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("ExistCustomer: There was an error finding a custormer");
        Log.Exception(_ex);
        throw;
      }
    }

    /// <summary>
    /// Create customer and documents
    /// </summary>
    /// <param name="AccountsReceptionInfo">Customer information</param>
    /// <param name="SqlTrx"></param>
    /// <returns>Success/Fail</returns>
    public static Boolean CreateCustomer(AccountsReception AccountsReceptionInfo,
                                                  SqlTransaction SqlTrx)
    {
      try
      {
        DateTime _now = WGDB.Now;
        DateTime _record_expiration = GetRecordExpiration(AccountsReceptionInfo.ScannedDocuments);

        //Crear el customer
        Customer _customer = new Customer { CustomerId = AccountsReceptionInfo.AccountId };
        if (!DB_InsertCustomer(_customer, SqlTrx))
        {
          return false;
        }

        //Crear el expediente
        if (AccountsReceptionInfo.RecordExpiration != null)
        {
          _record_expiration = AccountsReceptionInfo.RecordExpiration.Value;
        }
        CustomerRecord _record = new CustomerRecord
        {
          CustomerId = _customer.CustomerId,
          Created = _now,
          Deleted = false,
          Expiration = _record_expiration
        };
        if (!DB_InsertRecord(_record, SqlTrx))
        {
          return false;
        }

        //Crear los documentos asociados al expediente-cliente
        List<CustomerRecordDetail> _record_details = BuildRecordDetails(AccountsReceptionInfo);
        foreach (CustomerRecordDetail _record_detail in _record_details)
        {
          _record_detail.RecordId = _record.RecordId;
          if (!DB_InsertRecordDetail(_record_detail, SqlTrx))
          {
            return false;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("CreateCustomer: There was an error creating new record");
        Log.Exception(_ex);
        return false;
      }

      return true;
    }

    /// <summary>
    /// Update customer and documents
    /// </summary>
    /// <param name="AccountsReceptionInfo">Customer information</param>
    /// <param name="SqlTrx"></param>
    /// <returns>Success/Fail</returns>
    public static Boolean UpdateCustomer(AccountsReception AccountsReceptionInfo,
                                                  SqlTransaction SqlTrx)
    {
      try
      {
        DateTime _record_expiration = GetRecordExpiration(AccountsReceptionInfo.ScannedDocuments);

        CustomerRecord _record = DB_GetRecordByCustomer(AccountsReceptionInfo.AccountId, SqlTrx);
        if (_record == null)
        {
          CreateCustomer(AccountsReceptionInfo,
            SqlTrx);
          _record = DB_GetRecordByCustomer(AccountsReceptionInfo.AccountId, SqlTrx);
        }
        //Pasar a un histórico el expediente y sus documentos
        if (!DB_SaveRecordHistory(_record.RecordId, SqlTrx))
        {
          return false;
        }

        //Actualizo la fecha de expiración del expediente
        if (AccountsReceptionInfo.RecordExpiration != null)
        {
          _record_expiration = AccountsReceptionInfo.RecordExpiration.Value;
        }
        if (!DB_UpdateRecordExpiration(_record.RecordId, _record_expiration, SqlTrx))
        {
          return false;
        }

        //Borrar los documentos actuales del cliente
        if (!DB_DeleteRecordDetailsByRecord(_record.RecordId, SqlTrx))
        {
          return false;
        }

        //Inserto los documentos nuevamente
        List<CustomerRecordDetail> _details_to_save = BuildRecordDetails(AccountsReceptionInfo);
        foreach (CustomerRecordDetail _detail in _details_to_save)
        {
          _detail.RecordId = _record.RecordId;
          if (!DB_InsertRecordDetail(_detail, SqlTrx))
          {
            return false;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("UpdateCustomer: An error occurred while updating customer");
        Log.Exception(_ex);
        return false;
      }

      return true;
    }

    /// <summary>
    /// Save record and documents to historical tables
    /// </summary>
    /// <param name="RecordId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean DB_SaveRecordHistory(Int64 RecordId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("  DECLARE @Id bigint; ");
      _sb.AppendLine("  INSERT INTO  CUSTOMER_RECORDS_HISTORY ");
      _sb.AppendLine("          ( CURH_RECORD_ID ");
      _sb.AppendLine("          , CURH_CUSTOMER_ID ");
      _sb.AppendLine("          , CURH_DELETED ");
      _sb.AppendLine("          , CURH_CREATED ");
      _sb.AppendLine("          , CURH_EXPIRATION ");
      _sb.AppendLine("          , CURH_LOGDATE ) ");
      _sb.AppendLine("  SELECT ");
      _sb.AppendLine("            CUR_RECORD_ID ");
      _sb.AppendLine("          , CUR_CUSTOMER_ID ");
      _sb.AppendLine("          , CUR_DELETED ");
      _sb.AppendLine("          , CUR_CREATED ");
      _sb.AppendLine("          , CUR_EXPIRATION ");
      _sb.AppendLine("          , @pLogDate ");
      _sb.AppendLine("      FROM    CUSTOMER_RECORDS ");
      _sb.AppendLine("      WHERE   CUR_RECORD_ID = @pRecordId; ");

      _sb.AppendLine("  SET @Id = SCOPE_IDENTITY(); ");

      _sb.AppendLine("  INSERT INTO  CUSTOMER_RECORD_DETAILS_HISTORY ");
      _sb.AppendLine("          ( CURDH_RECORD_ID ");
      _sb.AppendLine("          , CURDH_DELETED ");
      _sb.AppendLine("          , CURDH_TYPE ");
      _sb.AppendLine("          , CURDH_DATA ");
      _sb.AppendLine("          , CURDH_CREATED ");
      _sb.AppendLine("          , CURDH_IMAGE ");
      _sb.AppendLine("          , CURDH_EXPIRATION ");
      _sb.AppendLine("          , CURDH_LOGDATE ) ");
      _sb.AppendLine("  SELECT ");
      _sb.AppendLine("            @Id ");
      _sb.AppendLine("          , CURD_DELETED ");
      _sb.AppendLine("          , CURD_TYPE ");
      _sb.AppendLine("          , CURD_DATA ");
      _sb.AppendLine("          , CURD_CREATED ");
      _sb.AppendLine("          , CURD_IMAGE ");
      _sb.AppendLine("          , CURD_EXPIRATION ");
      _sb.AppendLine("          , @pLogDate ");
      _sb.AppendLine("      FROM    CUSTOMER_RECORD_DETAILS ");
      _sb.AppendLine("      WHERE   CURD_RECORD_ID = @pRecordId  ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pRecordId", SqlDbType.BigInt).Value = RecordId;
          _cmd.Parameters.Add("@pLogDate", SqlDbType.DateTime).Value = WGDB.Now;

          _cmd.ExecuteNonQuery();
        }
      }
      catch (Exception _ex)
      {
        Log.Error("DB_SaveRecordHistory: An error occurred while saving record history");
        Log.Exception(_ex);
        return false;
      }

      return true;
    }

    /// <summary>
    /// Get record expiration
    /// </summary>
    /// <param name="ScannedDocuments"></param>
    /// <returns></returns>
    private static DateTime GetRecordExpiration(List<CustomerScannedDocument> ScannedDocuments)
    {
      DateTime _min_document_date = GetDefaultExpirationDate();

      foreach (CustomerScannedDocument _scanned_document in ScannedDocuments)
      {
        if (_scanned_document.Expiration.HasValue && _scanned_document.Expiration.Value < _min_document_date)
        {
          _min_document_date = _scanned_document.Expiration.Value;
        }
      }

      return _min_document_date;
    }

    /// <summary>
    /// Get default expiration date (General Param)
    /// </summary>
    /// <returns></returns>
    public static DateTime GetDefaultExpirationDate()
    {
      Int32 _expiration_days = GeneralParam.GetInt32(GP_RECEPTION_GROUP, GP_EXPIRATION_DAYS, 90);
      return WGDB.Now.AddDays(_expiration_days);
    }

    /// <summary>
    /// Add account to internal blacklist
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="InclusionDate"></param>
    /// <param name="Reason"></param>
    /// <param name="ReasonDescription"></param>
    public static void AddToBlacklist(CardData CardInfo, CashierSessionInfo SessionInfo, DateTime InclusionDate, ENUM_BLACKLIST_REASON Reason, string ReasonDescription)
    {
      if (!Misc.IsReceptionEnabled()) return;
      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          AddToBlacklist(CardInfo, SessionInfo, InclusionDate, Reason, ReasonDescription, _trx.SqlTransaction);

          _trx.Commit();
        }

        //RefreshBlackListCache(ENUM_BLACKLIST_SOURCE.LOCAL);

      }
      catch (Exception _ex)
      {
        Log.Error("AddToBlacklist: There was an error.");
        Log.Exception(_ex);
        throw;
      }
    }

    /// <summary>
    /// Add account to internal blacklist
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="InclusionDate"></param>
    /// <param name="Reason"></param>
    /// <param name="ReasonDescription"></param>
    public static void AddToBlacklist(CardData CardInfo, CashierSessionInfo SessionInfo, DateTime InclusionDate, ENUM_BLACKLIST_REASON Reason, string ReasonDescription, SqlTransaction SqlTransaction)
    {
      if (!Misc.IsReceptionEnabled()) return;
      try
      {
        AccountMovementsTable _account_movements;

        BlackListInternal _blacklist_info = new BlackListInternal
        {
          AccountId = CardInfo.AccountId,
          InclusionDate = InclusionDate,
          Reason = (Int16)Reason,
          ReasonDescription = ReasonDescription
        };

        if (!DB_InsertBlackListInternal(_blacklist_info, SqlTransaction))
          throw new Exception("DB_InsertBlackListInternal: Can't insert blacklist internal");

        // Set account movement
        _account_movements = new AccountMovementsTable(SessionInfo);

        // Add account movement
        _account_movements.Add(0, CardInfo.AccountId, MovementType.AccountInBlacklist,
                               CardInfo.CurrentBalance, 0, 0, CardInfo.CurrentBalance, ReasonDescription);

        // Save accounts and cashier movements
        if (!_account_movements.Save(SqlTransaction))
        {
          throw new Exception("DB_InsertBlackListInternal: Can't save account movement blacklist internal");
        }

      }
      catch (Exception _ex)
      {
        Log.Error("AddToBlacklist: There was an error.");
        Log.Exception(_ex);
        throw;
      }
    }

    /// <summary>
    /// Remove account from internal blacklist
    /// </summary>
    /// <param name="AccountId"></param>
    public static void RemoveFromBlacklist(CardData CardInfo, CashierSessionInfo SessionInfo)
    {
      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          RemoveFromBlacklist(CardInfo, SessionInfo, _trx.SqlTransaction);

          _trx.Commit();
        }

        //RefreshBlackListCache(ENUM_BLACKLIST_SOURCE.LOCAL);
      }
      catch (Exception _ex)
      {
        Log.Error("RemoveFromBlacklist: There was an error removing client form Blacklist");
        Log.Exception(_ex);

        throw;
      }
    }

    /// <summary>
    /// Remove account from internal blacklist
    /// </summary>
    /// <param name="AccountId"></param>
    public static void RemoveFromBlacklist(CardData CardInfo, CashierSessionInfo SessionInfo, SqlTransaction SqlTransaction)
    {
      try
      {

        AccountMovementsTable _account_movements;

        if (!DB_RemoveFromBlackListInternal(CardInfo.AccountId, SqlTransaction))
          throw new Exception("DB_RemoveFromBlackListInternal: Can't remove account from internal blacklist");


        // Set account movement
        _account_movements = new AccountMovementsTable(SessionInfo);

        // Add account movement
        _account_movements.Add(0, CardInfo.AccountId, MovementType.AccountNotInBlacklist,
                               CardInfo.CurrentBalance, 0, 0, CardInfo.CurrentBalance, "");

        // Save accounts and cashier movements
        if (!_account_movements.Save(SqlTransaction))
        {
          throw new Exception("DB_RemoveFromBlackListInternal: Can't save account movement not blacklist internal");
        }

        //RefreshBlackListCache(ENUM_BLACKLIST_SOURCE.LOCAL);
      }
      catch (Exception _ex)
      {
        Log.Error("RemoveFromBlacklist: There was an error removing client form Blacklist");
        Log.Exception(_ex);

        throw;
      }
    }

    /// <summary>
    /// Update record expiration with default expiration date
    /// </summary>
    /// <param name="CustomerId"></param>
    /// <returns></returns>
    public static DateTime? UpdateRecordExpiration(Int64 CustomerId)
    {
      DateTime _record_expiration;
      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          _record_expiration = GetDefaultExpirationDate();

          CustomerRecord _record = DB_GetRecordByCustomer(CustomerId, _trx.SqlTransaction);
          //Actualizo la fecha de expiración del expediente
          if (!DB_UpdateRecordExpiration(_record.RecordId, _record_expiration, _trx.SqlTransaction))
          {
            return null;
          }

          _trx.Commit();
        }
      }
      catch (Exception _ex)
      {
        Log.Error("UpdateRecordExpiration: There was an error updating record expiration.");
        Log.Exception(_ex);
        return null;
      }

      return _record_expiration;
    }

    private static List<CustomerRecordDetail> BuildRecordDetails(AccountsReception AccountsReceptionInfo)
    {
      List<CustomerRecordDetail> _details = new List<CustomerRecordDetail>();

      if (!string.IsNullOrEmpty(AccountsReceptionInfo.FirstName))
        _details.Add(NewCustomerRecordDetail(ENUM_TYPE_DETAIL.FIRST_NAME, AccountsReceptionInfo.FirstName.Trim()));

      if (!string.IsNullOrEmpty(AccountsReceptionInfo.SecondName))
        _details.Add(NewCustomerRecordDetail(ENUM_TYPE_DETAIL.SECOND_NAME, AccountsReceptionInfo.SecondName.Trim()));

      if (!string.IsNullOrEmpty(AccountsReceptionInfo.FirstSurname))
        _details.Add(NewCustomerRecordDetail(ENUM_TYPE_DETAIL.FIRST_SURNAME, AccountsReceptionInfo.FirstSurname.Trim()));

      if (!string.IsNullOrEmpty(AccountsReceptionInfo.SecondSurname))
        _details.Add(NewCustomerRecordDetail(ENUM_TYPE_DETAIL.SECOND_SURNAME, AccountsReceptionInfo.SecondSurname.Trim()));

      _details.Add(NewCustomerRecordDetail(ENUM_TYPE_DETAIL.NATIONALITY, AccountsReceptionInfo.Nacionality.ToString()));
      _details.Add(NewCustomerRecordDetail(ENUM_TYPE_DETAIL.GENDER, AccountsReceptionInfo.Gender.ToString()));
      _details.Add(NewCustomerRecordDetail(ENUM_TYPE_DETAIL.DOCUMENT_TYPE, AccountsReceptionInfo.DocumentType.ToString()));

      if (!string.IsNullOrEmpty(AccountsReceptionInfo.Document))
        _details.Add(NewCustomerRecordDetail(ENUM_TYPE_DETAIL.DOCUMENT, AccountsReceptionInfo.Document.Trim()));

      _details.Add(NewCustomerRecordDetail(ENUM_TYPE_DETAIL.COUNTRY, AccountsReceptionInfo.Country.ToString()));

      if (!string.IsNullOrEmpty(AccountsReceptionInfo.Comments))
        _details.Add(NewCustomerRecordDetail(ENUM_TYPE_DETAIL.COMMENTS, AccountsReceptionInfo.Comments.Trim()));

      if (AccountsReceptionInfo.BirthDate.HasValue)
        _details.Add(NewCustomerRecordDetail(ENUM_TYPE_DETAIL.BIRTH_DATE, AccountsReceptionInfo.BirthDate.Value.ToString("yyyyMMdd")));

      if (!string.IsNullOrEmpty(AccountsReceptionInfo.Contact.Email))
        _details.Add(NewCustomerRecordDetail(ENUM_TYPE_DETAIL.EMAIL, AccountsReceptionInfo.Contact.Email.Trim()));

      if (!string.IsNullOrEmpty(AccountsReceptionInfo.Contact.FirstTelephone))
        _details.Add(NewCustomerRecordDetail(ENUM_TYPE_DETAIL.FIRST_TEL_NUMBER, AccountsReceptionInfo.Contact.FirstTelephone.Trim()));

      if (!string.IsNullOrEmpty(AccountsReceptionInfo.Contact.SecondTelephone))
        _details.Add(NewCustomerRecordDetail(ENUM_TYPE_DETAIL.SECOND_TEL_NUMBER, AccountsReceptionInfo.Contact.SecondTelephone.Trim()));

      if (!string.IsNullOrEmpty(AccountsReceptionInfo.Contact.Twitter))
        _details.Add(NewCustomerRecordDetail(ENUM_TYPE_DETAIL.TWITTER, AccountsReceptionInfo.Contact.Twitter.Trim()));

      if (!string.IsNullOrEmpty(AccountsReceptionInfo.Contact.Facebook))
        _details.Add(NewCustomerRecordDetail(ENUM_TYPE_DETAIL.FACEBOOK, AccountsReceptionInfo.Contact.Facebook.Trim()));

      // Update date when AceptedAgreementDate have default
      if (AccountsReceptionInfo.AceptedAgreement)
      {
        AccountsReceptionInfo.AceptedAgreementDate = (AccountsReceptionInfo.AceptedAgreementDate == default(DateTime)) ? WGDB.Now
                                                                                                                       : AccountsReceptionInfo.AceptedAgreementDate;
        _details.Add(NewCustomerRecordDetail(ENUM_TYPE_DETAIL.ACEPTED_AGREEMENT, AccountsReceptionInfo.AceptedAgreementDate.ToString(CultureInfo.InvariantCulture)));
      }
      else
      {
        _details.Add(NewCustomerRecordDetail(ENUM_TYPE_DETAIL.ACEPTED_AGREEMENT, string.Empty));
      }

      foreach (CustomerAddress _address in AccountsReceptionInfo.Addresses)
      {
        _details.Add(NewCustomerRecordDetail(ENUM_TYPE_DETAIL.XML_ADDRESS, _address.ToXML()));
      }

      foreach (CustomerScannedDocument _scanned_document in AccountsReceptionInfo.ScannedDocuments)
      {
        _details.Add(NewCustomerRecordDetail(ENUM_TYPE_DETAIL.XML_SCANNED_DOCUMENT, _scanned_document.ToXML(), _scanned_document.Image, _scanned_document.Expiration));
      }

      foreach (var v in AccountsReceptionInfo.DocumentTypes)
      {
        _details.Add(NewCustomerRecordDetail(v.Key + 6000, v.Value));

      }

      return _details;
    }

    public static Boolean SaveAgreement(Int64 AccountId, SqlTransaction SqlTrx)
    {
      CustomerRecordDetail _crd;

      _crd = NewCustomerRecordDetail(ENUM_TYPE_DETAIL.ACEPTED_AGREEMENT, WGDB.Now.ToString(CultureInfo.InvariantCulture));

      CustomerRecord _record = DB_GetRecordByCustomer(AccountId, SqlTrx);
      _crd.RecordId = _record.RecordId;

      if (!DB_InsertRecordDetail(_crd, SqlTrx))
      {
        return false;
      }

      return true;
    }

    private static CustomerRecordDetail NewCustomerRecordDetail(ENUM_TYPE_DETAIL Type, string Data, byte[] Image, DateTime? DocumentExpiration = null)
    {
      return new CustomerRecordDetail
      {
        Created = WGDB.Now,
        Data = Data,
        Image = Image,
        Deleted = false,
        Expiration = DocumentExpiration,
        Type = Type
      };
    }
    private static CustomerRecordDetail NewCustomerRecordDetail(int Type, string Data, byte[] Image, DateTime? DocumentExpiration = null)
    {
      return new CustomerRecordDetail
      {
        Created = WGDB.Now,
        Data = Data,
        Image = Image,
        Deleted = false,
        Expiration = DocumentExpiration,
        Type = (ENUM_TYPE_DETAIL)Type
      };
    }

    private static CustomerRecordDetail NewCustomerRecordDetail(ENUM_TYPE_DETAIL Type, string Data, DateTime? DocumentExpiration = null)
    {
      return NewCustomerRecordDetail(Type, Data, null, DocumentExpiration);
    }
    private static CustomerRecordDetail NewCustomerRecordDetail(int Type, string Data, DateTime? DocumentExpiration = null)
    {
      return NewCustomerRecordDetail(Type, Data, null, DocumentExpiration);
    }

    private static AccountsReception BuildAccountsReception(CustomerRecord Record, List<CustomerRecordDetail> RecordDetails)
    {
      AccountsReception _accounts_reception = new AccountsReception
      {
        AccountId = Record.CustomerId,
        RecordExpiration = Record.Expiration,
        CreationDate = Record.Created
      };
      if (_accounts_reception.DocumentTypes == null)
        _accounts_reception.DocumentTypes = new Dictionary<int, string>();
      else
        _accounts_reception.DocumentTypes.Clear();

      foreach (CustomerRecordDetail _detail in RecordDetails)
      {
        if (string.IsNullOrEmpty(_detail.Data))
          continue;

        switch (_detail.Type)
        {
          case ENUM_TYPE_DETAIL.BIRTH_DATE:
            _accounts_reception.BirthDate = DateTime.ParseExact(_detail.Data, "yyyyMMdd", CultureInfo.InvariantCulture);
            break;
          case ENUM_TYPE_DETAIL.COMMENTS:
            _accounts_reception.Comments = _detail.Data;
            break;
          case ENUM_TYPE_DETAIL.COUNTRY:
            _accounts_reception.Country = int.Parse(_detail.Data);
            break;
          case ENUM_TYPE_DETAIL.DOCUMENT:
            _accounts_reception.Document = _detail.Data;
            break;
          case ENUM_TYPE_DETAIL.DOCUMENT_TYPE:
            _accounts_reception.DocumentType = int.Parse(_detail.Data);
            break;
          case ENUM_TYPE_DETAIL.EMAIL:
            _accounts_reception.Contact.Email = _detail.Data;
            break;
          case ENUM_TYPE_DETAIL.FIRST_NAME:
            _accounts_reception.FirstName = _detail.Data;
            break;
          case ENUM_TYPE_DETAIL.FIRST_SURNAME:
            _accounts_reception.FirstSurname = _detail.Data;
            break;
          case ENUM_TYPE_DETAIL.SECOND_TEL_NUMBER:
            _accounts_reception.Contact.SecondTelephone = _detail.Data;
            break;
          case ENUM_TYPE_DETAIL.GENDER:
            _accounts_reception.Gender = int.Parse(_detail.Data);
            break;
          case ENUM_TYPE_DETAIL.NATIONALITY:
            _accounts_reception.Nacionality = int.Parse(_detail.Data);
            break;
          case ENUM_TYPE_DETAIL.SECOND_NAME:
            _accounts_reception.SecondName = _detail.Data;
            break;
          case ENUM_TYPE_DETAIL.SECOND_SURNAME:
            _accounts_reception.SecondSurname = _detail.Data;
            break;
          case ENUM_TYPE_DETAIL.FIRST_TEL_NUMBER:
            _accounts_reception.Contact.FirstTelephone = _detail.Data;
            break;
          case ENUM_TYPE_DETAIL.TWITTER:
            _accounts_reception.Contact.Twitter = _detail.Data;
            break;
          case ENUM_TYPE_DETAIL.FACEBOOK:
            _accounts_reception.Contact.Facebook = _detail.Data;
            break;
          case ENUM_TYPE_DETAIL.ACEPTED_AGREEMENT:
            _accounts_reception.AceptedAgreement = true;
            _accounts_reception.AceptedAgreementDate = _detail.Created;
            break;
          case ENUM_TYPE_DETAIL.XML_ADDRESS:
            _accounts_reception.Addresses.Add(XmlSerializationHelper.Deserialize<CustomerAddress>(_detail.Data));
            break;
          case ENUM_TYPE_DETAIL.XML_SCANNED_DOCUMENT:
            CustomerScannedDocument _document = XmlSerializationHelper.Deserialize<CustomerScannedDocument>(_detail.Data);
            _document.Expiration = _detail.Expiration;
            _document.Image = _detail.Image;
            _accounts_reception.ScannedDocuments.Add(_document);
            break;
          default:
            if ((int)_detail.Type > 5999 && (int)_detail.Type < 7000)
            {
              int _dt = (int)_detail.Type - 6000;
              if (_accounts_reception.DocumentTypes.ContainsKey(_dt))
              {
                _accounts_reception.DocumentTypes[_dt] = _detail.Data;
              }
              else
              {
                _accounts_reception.DocumentTypes.Add(_dt, _detail.Data);
              }
            }
            break;
        }
      }

      return _accounts_reception;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public static CustomerAddress GetCustomerAddress(String Data)
    {
      try
      {
        return XmlSerializationHelper.Deserialize<CustomerAddress>(Data);
      }
      catch (Exception)
      {
        return null;
      }
    }

    /// <summary>
    /// Get customer and current record documents
    /// </summary>
    /// <param name="CustomerId"></param>
    /// <returns></returns> 
    public static AccountsReception GetCustomerData(Int64 CustomerId)
    {
      try
      {
        CustomerRecord _record = DB_GetRecordByCustomer(CustomerId);

        if (_record == null)
          return null;

        List<CustomerRecordDetail> _record_details = DB_GetActiveRecordDetailsWithoutScanDocuments(_record.RecordId);

        return BuildAccountsReception(_record, _record_details);
      }
      catch (Exception _ex)
      {
        Log.Error("GetCustomerData: There was an error looking for customer data");
        Log.Exception(_ex);
        throw;
      }
    }

    /// <summary>
    /// Get scanned documents by customer
    /// </summary>
    /// <param name="CustomerId"></param>
    /// <returns></returns> 
    public static List<CustomerScannedDocument> GetScannedDocuments(Int64 CustomerId)
    {
      try
      {
        List<CustomerScannedDocument> _documents = new List<CustomerScannedDocument>();

        CustomerRecord _record = DB_GetRecordByCustomer(CustomerId);

        if (_record == null)
          return _documents;

        List<CustomerRecordDetail> _record_details = DB_GetActiveRecordDetailsOnlyScannedDocuments(_record.RecordId);

        CustomerScannedDocument _document;
        foreach (CustomerRecordDetail _detail in _record_details)
        {
          _document = XmlSerializationHelper.Deserialize<CustomerScannedDocument>(_detail.Data);
          _document.Expiration = _detail.Expiration;
          _document.Image = _detail.Image;
          _documents.Add(_document);
        }

        return _documents;
      }
      catch (Exception _ex)
      {
        Log.Error("GetScannedDocuments: There was an error getting scanned documents");
        Log.Exception(_ex);
        throw;
      }
    }

    /// <summary>
    /// Check if customer has expired documents
    /// </summary>
    /// <param name="CustomerId"></param>
    /// <returns></returns>
    public static void CheckCustomerHasExpiredDocuments(Int64 CustomerId)
    {
      try
      {
        NotifyToRemoteExpiredDocuments(new ExpiredDocumentsResultInfo() { Result = ExpiredDocumentsResult.Processing });

        //Chequear que el expediente del cliente no esté caducado
        CustomerRecord _record = DB_GetRecordByCustomer(CustomerId);
        if (_record.Expiration != null && _record.Expiration.Value.Date < WGDB.Now.Date)
        {
          NotifyToRemoteExpiredDocuments(new ExpiredDocumentsResultInfo() { Result = ExpiredDocumentsResult.HasExpiredDocuments });
          return;
        }

        //Chequear que ningún documento adjunto al expediente esté caducado
        List<CustomerRecordDetail> _record_details = DB_GetActiveRecordDetails(_record.RecordId);
        bool _has_expired_docs = false;
        foreach (CustomerRecordDetail _detail in _record_details)
        {

          if (!_detail.Expiration.HasValue)
            continue;

          if (_detail.Expiration < WGDB.Now.Date.AddDays(-1))
          {
            _has_expired_docs = true;
          }
        }
        NotifyToRemoteExpiredDocuments(_has_expired_docs
          ? new ExpiredDocumentsResultInfo() { Result = ExpiredDocumentsResult.HasExpiredDocuments }
          : new ExpiredDocumentsResultInfo() { Result = ExpiredDocumentsResult.DoesNotHaveExpiredDocuments });
      }

      catch (Exception _ex)
      {
        Log.Error("DB_SaveRecordHistory: There was an error saving record");
        Log.Exception(_ex);
        NotifyToRemoteExpiredDocuments(new ExpiredDocumentsResultInfo() { Result = ExpiredDocumentsResult.Error });
      }
    }


    /// <summary>
    /// Return a list of running WCP services
    /// </summary>
    /// <param name="Protocol">Name of service</param>
    /// <param name="ListOfRunningServicesIP">result</param>
    /// <returns></returns>
    private static Boolean GetListOfRunningServicesIP(String Protocol, out List<String> ListOfRunningServicesIP)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!Services.DB_GetRunningServices(Protocol, _db_trx.SqlTransaction, out ListOfRunningServicesIP))
        {
          Log.Message("GetListOfRunningServicesIP: Not running services found for:" + Protocol);

          return false;
        }
      }

      return true;
    } // GetListOfRunningServicesIP


    /// <summary>
    /// Send request to WS using the list of availables services (WCP)
    /// </summary>
    /// <param name="Request"></param>
    /// <returns>response of WS: XmlDocument</returns>
    private static XmlDocument SendRequestToAvailableWS(XmlDocument Request)
    {

      List<String> _list_of_running_services_IP;
      String _port;

      //Get list of running services (available)
      if (!GetListOfRunningServicesIP("WCP", out _list_of_running_services_IP))
      {
        throw new Exception("Error: GetListOfRunningServicesIP");
      }

      _port = GeneralParam.GetString(GP_BLACKLIST_SERVICE_GROUP, GP_BLACKLIST_SERVICE_PORT, "7778");

      //Send request to the list of running services,
      foreach (String _IP_from_WCP_running in _list_of_running_services_IP)
      {
        try
        {
          //Save IP for next try
          m_last_active_IP = _IP_from_WCP_running;

          //If it went ok then exit
          return CallService(Misc.GetIPAndPort(m_last_active_IP, _port)
                            , Request);

        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      } // foreach

      throw new Exception("Error SendRequestToAvailableWS");
    } // SendRequestToAvailableWS


    /// <summary>
    /// Send request to WS. Try 1: using fixed ip; try 2: using last active IP; try 3: using the list of available services
    /// </summary>
    /// <param name="Request"></param>
    /// <returns>request of WS: XmlDocument</returns>
    private static XmlDocument SendRequest(XmlDocument Request)
    {
      String _fixed_ip;
      String _port;

      _fixed_ip = GeneralParam.GetString(GP_BLACKLIST_SERVICE_GROUP, GP_BLACKLIST_SERVICE_IP, "");
      _port = GeneralParam.GetString(GP_BLACKLIST_SERVICE_GROUP, GP_BLACKLIST_SERVICE_PORT, "7778");

      //If 'fixed IP' then ONLY try to connect to that IP, and don't search or try other IP
      if (!String.IsNullOrEmpty(_fixed_ip))
      {

        return CallService(Misc.GetIPAndPort(_fixed_ip, _port)
                          , Request);
      }

      //If 'last IP' then try connecting to that IP, and if it does not work try with the list of available services
      if (!String.IsNullOrEmpty(m_last_active_IP))
      {
        try
        {
          return CallService(Misc.GetIPAndPort(m_last_active_IP, _port)
                  , Request);
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }

      //Send request to the list of available services
      return SendRequestToAvailableWS(Request);
    } // SendRequest

    /// <summary>
    /// Check if customer is in a black list
    /// </summary>
    /// <param name="CustomerDetails"></param>
    /// <returns></returns>
    public static BlackListResultInfo CheckAccountInBlackList(ICustomerNameAndId CustomerDetails)
    {
      BlackListResultInfo _result = new BlackListResultInfo { AccountId = CustomerDetails.AccountId ?? -1 };
      BlackListServiceResult _blacklist_service_result;
      BlackList _blackList;
      Boolean _service_error;

      _service_error = false;
      _blackList = new BlackList();

      try
      {
        NotifyToRemoteBlackList(new BlackListResultInfo()
        {
          Processing = true,
          AccountId = CustomerDetails.AccountId ?? -1
        });

        BlackListDocuments _documents;
        Dictionary<int, string> _documents_dictionary = new Dictionary<int, string>();
        XmlDocument _request;
        XmlDocument _response;
        foreach (KeyValuePair<int, string> _docs in CustomerDetails.IDDocuments)
        {
          _documents_dictionary.Add((int)ENUM_BLACKLIST_DOC_TYPE.DOC_TYPE + _docs.Key, _docs.Key.ToString());
          _documents_dictionary.Add((int)ENUM_BLACKLIST_DOC_TYPE.DOC_NUMBER + _docs.Key, _docs.Value);
        }

        if (CustomerDetails.HolderBirthDate.HasValue && CustomerDetails.HolderBirthDate != DateTime.MinValue)
        {
          _documents_dictionary.Add((int)ENUM_BLACKLIST_DOC_TYPE.DATEOFBIRTH,
            XmlConvert.ToString(CustomerDetails.HolderBirthDate.Value, XmlDateTimeSerializationMode.Utc));
        }


        if (CustomerDetails.AccountId.HasValue && CustomerDetails.AccountId.Value > 0)
          _documents_dictionary.Add((int)ENUM_BLACKLIST_DOC_TYPE.ACCOUNT, CustomerDetails.AccountId.Value.ToString());
        if (GeneralParam.GetBoolean("Account.VisibleField", "Name3", true))
          _documents_dictionary.Add((int)ENUM_BLACKLIST_DOC_TYPE.FIRST_NAME, CustomerDetails.HolderName3);
        if (GeneralParam.GetBoolean("Account.VisibleField", "Name4", true))
          _documents_dictionary.Add((int)ENUM_BLACKLIST_DOC_TYPE.MIDDLE_NAME, CustomerDetails.HolderName4);
        if (GeneralParam.GetBoolean("Account.VisibleField", "Name1", true))
          _documents_dictionary.Add((int)ENUM_BLACKLIST_DOC_TYPE.LASTNAME_1, CustomerDetails.HolderName1);
        if (GeneralParam.GetBoolean("Account.VisibleField", "Name2", true))
          _documents_dictionary.Add((int)ENUM_BLACKLIST_DOC_TYPE.LASTNAME_2, CustomerDetails.HolderName2);

        _documents = _documents_dictionary.ToBlackListDocuments();
        _documents.Type = "CheckDocumentsAreNotBlackListed";

        _request = new XmlDocument();
        _request.LoadXml(XmlSerializationHelper.Serialize(_documents));
        _blacklist_service_result = new BlackListServiceResult();

        try
        {
          _response = SendRequest(_request); //OK
          _blacklist_service_result = XmlSerializationHelper.Deserialize<BlackListServiceResult>(_response.OuterXml);
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);

          _service_error = true;
        }        

        if (_blacklist_service_result.FoundResults && !_service_error)
        {
          _result.CustomerResults = ConcatCustomerResults(_result.CustomerResults,
            ReadCustomerResults(_blacklist_service_result.Results));
        }

        _blacklist_service_result = _blackList.AreAnyDocumentsBlackListed(_documents_dictionary);
        if (_blacklist_service_result.FoundResults)
        {
          _result.CustomerResults = ConcatCustomerResults(_result.CustomerResults,
            ReadCustomerResults(_blacklist_service_result.Results));
        }
        if (_service_error && (_result.CustomerResults == null || _result.CustomerResults.Length == 0))
        {
          _result = new BlackListResultInfo() { Error = true, AccountId = CustomerDetails.AccountId ?? -1 };
        }
      }
      catch (WebServiceConnectionException _ex)
      {
        if (Misc.GetElapsedTicks(m_stats_log_tick) > ELAPSED_LOG_TICK)
        {
          Log.Error(_ex.Message);
          m_stats_log_tick = Misc.GetTickCount();
        }

        _result = new BlackListResultInfo() { Error = true, AccountId = CustomerDetails.AccountId ?? -1 };
      }
      catch (Exception _ex)
      {
        Log.Error(
          String.Format("CheckAccountInBlackList: There was an error checking account in blacklist. Details: {0}",
            _ex.Message));
        _result = new BlackListResultInfo() { Error = true, AccountId = CustomerDetails.AccountId ?? -1 };
      }
      NotifyToRemoteBlackList(_result);
      return _result;
    }

    private static BlackListResultCustomer[] ConcatCustomerResults(BlackListResultCustomer[] array1, BlackListResultCustomer[] array2)
    {
      int array1OriginalLength = array1.Length;
      Array.Resize(ref array1, array1OriginalLength + array2.Length);
      Array.Copy(array2, 0, array1, array1OriginalLength, array2.Length);
      return array1;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="CustomersResult"></param>
    /// <returns></returns>
    private static BlackListResultCustomer[] ReadCustomerResults(CustomersResult[] CustomersResult)
    {
      int i = 0;
      BlackListResultCustomer[] _blrcs = new BlackListResultCustomer[CustomersResult.Length];

      foreach (CustomersResult _cr in CustomersResult)
      {
        _blrcs[i] = new BlackListResultCustomer
        {
          CustomerFullName = _cr.CustomerFullName,
          CustomerDocType = (ACCOUNT_HOLDER_ID_TYPE)_cr.CustumerDocType,
          CustumerDocNmbr = _cr.CustumerDocNmbr,
          InclusionDate = _cr.ExclusionDate,
          ReasonType = (ENUM_BLACKLIST_REASON)_cr.ReasonType,
          ReasonDescription = _cr.ReasonDescription,
          Source = (ENUM_BLACKLIST_SOURCE)_cr.BlackListSource,
          Lists = (_cr.ListName ?? "").Split('\\'),
          Messages = (_cr.Message ?? "").Split('\\'),
          NotPermitedToEnter = _cr.NotPermittedToEnter


        };

        i++;
      }

      return _blrcs;
    }

    /// <summary>
    /// Refresh Cache from BlacklistService from a specific source
    /// </summary>
    /// <param name="Source">ENUM_BLACKLIST_SOURCE Source of Blacklist</param>
    public static void RefreshBlackListCache(ENUM_BLACKLIST_SOURCE Source)
    {
      BlackListDocuments _documents;
      XmlDocument _request;

      try
      {

        _documents = new BlackListDocuments();
        _documents.Source = (int)Source;
        _documents.Type = "RefreshCache";

        _request = new XmlDocument();
        _request.LoadXml(XmlSerializationHelper.Serialize(_documents));
        SendRequest(_request);

      }
      catch (WebServiceConnectionException _ex)
      {
        if (Misc.GetElapsedTicks(m_stats_log_tick) > ELAPSED_LOG_TICK)
        {
          Log.Error(_ex.Message);
          m_stats_log_tick = Misc.GetTickCount();
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("RefreshBlackListCache: There was an error checking account in blacklist. Details: {0}", _ex.Message));
      }
    }


    private static XmlDocument CallService(string UriAddress, XmlDocument Document)
    {
      try
      {
        var _web_request = WebRequestHelper.CreateWebRequestPOST(UriAddress, Document);
        using (var _web_response = (HttpWebResponse)_web_request.GetResponse())
        {
          return _web_response.Read();
        }
      }
      catch (Exception _ex)
      {
        throw new WebServiceConnectionException(string.Format("There was an error calling the service '{0}' in CallService Method", UriAddress), _ex);
      }
    }

    /// <summary>
    /// Get record by customer
    /// </summary>
    /// <param name="CustomerId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static CustomerRecord DB_GetRecordByCustomer(Int64 CustomerId,
                                                  SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      CustomerRecord _record = null;

      _sb = new StringBuilder();
      _sb.AppendLine("  SELECT  CUR_RECORD_ID, ");
      _sb.AppendLine("          CUR_CUSTOMER_ID, ");
      _sb.AppendLine("          CUR_DELETED, ");
      _sb.AppendLine("          CUR_CREATED, ");
      _sb.AppendLine("          CUR_EXPIRATION ");
      _sb.AppendLine("  FROM    CUSTOMER_RECORDS  ");
      _sb.AppendLine("  WHERE   CUR_CUSTOMER_ID = @pCustomerId  ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCustomerId", SqlDbType.BigInt).Value = CustomerId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read() && !_reader.IsDBNull(0))
            {
              _record = BuildRecordData(_reader);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("DB_GetRecordByCustomer: There was an error getting record");
        Log.Exception(_ex);
        throw;
      }

      return _record;
    }

    /// <summary>
    /// Get record by customer
    /// </summary>
    /// <param name="CustomerId"></param>
    /// <returns></returns>
    private static CustomerRecord DB_GetRecordByCustomer(Int64 CustomerId)
    {
      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          return DB_GetRecordByCustomer(CustomerId, _trx.SqlTransaction);
        }
      }
      catch (Exception _ex)
      {
        Log.Error("DB_SaveRecordHistory: There was an error saving record");
        Log.Exception(_ex);
        throw;
      }
    }

    /// <summary>
    /// Get active documents by record
    /// </summary>
    /// <param name="RecordId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>

    private static List<CustomerRecordDetail> DB_GetActiveRecordDetails(Int64 RecordId,
                                                  SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      List<CustomerRecordDetail> _list = new List<CustomerRecordDetail>();

      _sb = new StringBuilder();
      _sb.AppendLine("  SELECT  CURD_DETAIL_ID, ");
      _sb.AppendLine("          CURD_RECORD_ID, ");
      _sb.AppendLine("          CURD_DELETED, ");
      _sb.AppendLine("          CURD_TYPE, ");
      _sb.AppendLine("          CURD_DATA, ");
      _sb.AppendLine("          CURD_CREATED, ");
      _sb.AppendLine("          CURD_EXPIRATION, ");
      _sb.AppendLine("          CURD_IMAGE ");
      _sb.AppendLine("  FROM    CUSTOMER_RECORD_DETAILS  ");
      _sb.AppendLine("  WHERE   CURD_RECORD_ID = @pRecordId  ");
      _sb.AppendLine("      AND CURD_DELETED = 0  ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pRecordId", SqlDbType.BigInt).Value = RecordId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              _list.Add(BuildRecordDetailData(_reader));
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("DB_GetActiveRecordDetails: There was an error getting active records details from customer");
        Log.Exception(_ex);
        throw;
      }

      return _list;
    }

    private static List<CustomerRecordDetail> DB_GetActiveRecordDetailsWithoutScanDocuments(Int64 RecordId,
                                              SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      List<CustomerRecordDetail> _list = new List<CustomerRecordDetail>();

      _sb = new StringBuilder();
      _sb.AppendLine("  SELECT  CURD_DETAIL_ID, ");
      _sb.AppendLine("          CURD_RECORD_ID, ");
      _sb.AppendLine("          CURD_DELETED, ");
      _sb.AppendLine("          CURD_TYPE, ");
      _sb.AppendLine("          CURD_DATA, ");
      _sb.AppendLine("          CURD_CREATED, ");
      _sb.AppendLine("          CURD_EXPIRATION, ");
      _sb.AppendLine("          CURD_IMAGE ");
      _sb.AppendLine("  FROM    CUSTOMER_RECORD_DETAILS  ");
      _sb.AppendLine("  WHERE   CURD_RECORD_ID = @pRecordId  ");
      _sb.AppendLine("      AND CURD_DELETED = 0  ");
      _sb.AppendLine("      AND CURD_TYPE <> @pScanDocumentType ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pRecordId", SqlDbType.BigInt).Value = RecordId;
          _cmd.Parameters.Add("@pScanDocumentType", SqlDbType.Int).Value = (int)ENUM_TYPE_DETAIL.XML_SCANNED_DOCUMENT;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              _list.Add(BuildRecordDetailData(_reader));
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("DB_GetActiveRecordDetailsWithoutScanDocuments: There was an error getting active records details");
        Log.Exception(_ex);
        throw;
      }

      return _list;
    }

    /// <summary>
    /// Get active documents by record
    /// </summary>
    /// <param name="RecordId"></param>
    /// <returns></returns>
    private static List<CustomerRecordDetail> DB_GetActiveRecordDetails(Int64 RecordId)
    {
      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          return DB_GetActiveRecordDetails(RecordId, _trx.SqlTransaction);
        }
      }
      catch (Exception _ex)
      {
        Log.Error("DB_SaveRecordHistory: There was an error getting data");
        Log.Exception(_ex);
        throw;
      }
    }

    /// <summary>
    /// Get active documents by record - without scan documents
    /// </summary>
    /// <param name="RecordId"></param>
    /// <returns></returns>
    private static List<CustomerRecordDetail> DB_GetActiveRecordDetailsWithoutScanDocuments(Int64 RecordId)
    {
      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          return DB_GetActiveRecordDetailsWithoutScanDocuments(RecordId, _trx.SqlTransaction);
        }
      }
      catch (Exception _ex)
      {
        Log.Error("DB_GetActiveRecordDetailsWithoutScanDocuments: There was an error getting data");
        Log.Exception(_ex);
        throw;
      }
    }

    /// <summary>
    /// active documents by record - only scan documents
    /// </summary>
    /// <param name="RecordId"></param>
    /// <returns></returns>
    private static List<CustomerRecordDetail> DB_GetActiveRecordDetailsOnlyScannedDocuments(Int64 RecordId)
    {
      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          return DB_GetActiveRecordDetailsOnlyScannedDocuments(RecordId, _trx.SqlTransaction);
        }
      }
      catch (Exception _ex)
      {
        Log.Error("DB_GetActiveRecordDetailsOnlyScannedDocuments: There was an error getting data");
        Log.Exception(_ex);
        throw;
      }
    }

    /// <summary>
    /// active documents by record - only scan documents
    /// </summary>
    /// <param name="RecordId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static List<CustomerRecordDetail> DB_GetActiveRecordDetailsOnlyScannedDocuments(Int64 RecordId,
                                              SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      List<CustomerRecordDetail> _list = new List<CustomerRecordDetail>();

      _sb = new StringBuilder();
      _sb.AppendLine("  SELECT  CURD_DETAIL_ID, ");
      _sb.AppendLine("          CURD_RECORD_ID, ");
      _sb.AppendLine("          CURD_DELETED, ");
      _sb.AppendLine("          CURD_TYPE, ");
      _sb.AppendLine("          CURD_DATA, ");
      _sb.AppendLine("          CURD_CREATED, ");
      _sb.AppendLine("          CURD_EXPIRATION, ");
      _sb.AppendLine("          CURD_IMAGE ");
      _sb.AppendLine("  FROM    CUSTOMER_RECORD_DETAILS  ");
      _sb.AppendLine("  WHERE   CURD_RECORD_ID = @pRecordId  ");
      _sb.AppendLine("      AND CURD_DELETED = 0  ");
      _sb.AppendLine("      AND CURD_TYPE = @pScanDocumentType ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pRecordId", SqlDbType.BigInt).Value = RecordId;
          _cmd.Parameters.Add("@pScanDocumentType", SqlDbType.Int).Value = (int)ENUM_TYPE_DETAIL.XML_SCANNED_DOCUMENT;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              _list.Add(BuildRecordDetailData(_reader));
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error("DB_GetActiveRecordDetailsOnlyScannedDocuments: There was an error getting data");
        Log.Exception(_ex);
        throw;
      }

      return _list;
    }

    /// <summary>
    /// Insert the customer into the database
    /// </summary>
    /// <param name="CustomerInfo">Customer Information</param>
    /// <param name="SqlTrx"></param>
    /// <returns>Success/Fail</returns>
    private static Boolean DB_InsertCustomer(Customer CustomerInfo, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine(" IF NOT EXISTS ( SELECT   1                                ");
      _sb.AppendLine("                   FROM   CUSTOMERS                        ");
      _sb.AppendLine("                  WHERE   CUS_CUSTOMER_ID = @pCustomerId ) ");
      _sb.AppendLine("            INSERT INTO   CUSTOMERS                        ");
      _sb.AppendLine("                        ( CUS_CUSTOMER_ID )                ");
      _sb.AppendLine("                 VALUES                                    ");
      _sb.AppendLine("                        ( @pCustomerId )                   ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCustomerId", SqlDbType.BigInt).Value = CustomerInfo.CustomerId;

          _cmd.ExecuteNonQuery();
        }
      }
      catch (Exception _ex)
      {
        Log.Error("DB_InsertCustomer: There was an error inserting record");
        Log.Exception(_ex);

        return false;
      }

      return true;
    }

    /// <summary>
    /// Update the record expiration
    /// </summary>
    /// <param name="CustomerRecordId">Record Id</param>
    /// <param name="Expiration">New Expiration</param>
    /// <param name="SqlTrx"></param>
    /// <returns>Success/Fail</returns>
    private static Boolean DB_UpdateRecordExpiration(Int64 CustomerRecordId,
                                                  DateTime Expiration,
                                                  SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("  UPDATE CUSTOMER_RECORDS SET ");
      _sb.AppendLine("        CUR_EXPIRATION = @pExpiration ");
      _sb.AppendLine("  WHERE   CUR_RECORD_ID = @pRecordId  ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pExpiration", SqlDbType.DateTime).Value = Expiration;
          _cmd.Parameters.Add("@pRecordId", SqlDbType.BigInt).Value = CustomerRecordId;

          _cmd.ExecuteNonQuery();
        }
      }
      catch (Exception _ex)
      {
        Log.Error("DB_UpdateRecordExpiration: There was an error updating record");
        Log.Exception(_ex);
        return false;
      }

      return true;
    }

    /// <summary>
    /// Delete record details by record
    /// </summary>
    /// <param name="CustomerRecordId">Record Id</param>
    /// <param name="SqlTrx"></param>
    /// <returns>Success/Fail</returns>
    private static Boolean DB_DeleteRecordDetailsByRecord(Int64 CustomerRecordId,
                                                  SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("  DELETE FROM CUSTOMER_RECORD_DETAILS ");
      _sb.AppendLine("  WHERE   CURD_RECORD_ID = @pRecordId  ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pRecordId", SqlDbType.BigInt).Value = CustomerRecordId;
          _cmd.ExecuteNonQuery();
        }
      }
      catch (Exception _ex)
      {
        Log.Error("DB_DeleteRecordDetailsByRecord: There was an error deleting record details");
        Log.Exception(_ex);
        return false;
      }

      return true;
    }

    /// <summary>
    /// Insert the record into the database
    /// </summary>
    /// <param name="RecordInfo">Record information</param>
    /// <param name="SqlTrx"></param>
    /// <returns>Success/Fail</returns>
    private static Boolean DB_InsertRecord(CustomerRecord RecordInfo,
                                                  SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("  INSERT INTO  CUSTOMER_RECORDS ");
      _sb.AppendLine("          ( CUR_CUSTOMER_ID ");
      _sb.AppendLine("          , CUR_DELETED ");
      _sb.AppendLine("          , CUR_CREATED ");
      _sb.AppendLine("          , CUR_EXPIRATION ) ");
      _sb.AppendLine("  VALUES ");
      _sb.AppendLine("          ( @pCustomerId ");
      _sb.AppendLine("          , @pDeleted ");
      _sb.AppendLine("          , @pCreated ");
      _sb.AppendLine("          , @pExpiration ) ");
      _sb.AppendLine("  SET @pOutputId = SCOPE_IDENTITY() ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCustomerId", SqlDbType.BigInt).Value = RecordInfo.CustomerId;
          _cmd.Parameters.Add("@pDeleted", SqlDbType.Bit).Value = RecordInfo.Deleted;
          _cmd.Parameters.Add("@pCreated", SqlDbType.DateTime).Value = RecordInfo.Created;
          _cmd.Parameters.Add("@pExpiration", SqlDbType.DateTime).Value = RecordInfo.Expiration;
          SqlParameter _p_output_id = _cmd.Parameters.Add("@pOutputId", SqlDbType.BigInt, 8, "CUR_RECORD_ID");
          _p_output_id.Direction = ParameterDirection.Output;

          _cmd.ExecuteNonQuery();

          RecordInfo.RecordId = (Int64)_p_output_id.Value;
        }
      }
      catch (Exception _ex)
      {
        Log.Error("DB_InsertRecord: There was an error inserting new record");
        Log.Exception(_ex);
        return false;
      }

      return true;
    }

    /// <summary>
    /// Insert the record detail into the database
    /// </summary>
    /// <param name="RecordDetailInfo">Record detail information</param>
    /// <param name="SqlTrx"></param>
    /// <returns>Success/Fail</returns>
    private static Boolean DB_InsertRecordDetail(CustomerRecordDetail RecordDetailInfo,
                                                  SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("  INSERT INTO  CUSTOMER_RECORD_DETAILS ");
      _sb.AppendLine("          ( CURD_RECORD_ID ");
      _sb.AppendLine("          , CURD_DELETED ");
      _sb.AppendLine("          , CURD_CREATED ");
      _sb.AppendLine("          , CURD_EXPIRATION ");
      _sb.AppendLine("          , CURD_TYPE ");
      _sb.AppendLine("          , CURD_IMAGE ");
      _sb.AppendLine("          , CURD_DATA ) ");
      _sb.AppendLine("  VALUES ");
      _sb.AppendLine("          ( @pRecordId ");
      _sb.AppendLine("          , @pDeleted ");
      _sb.AppendLine("          , @pCreated ");
      _sb.AppendLine("          , @pExpiration ");
      _sb.AppendLine("          , @pType ");
      _sb.AppendLine("          , @pImage ");
      _sb.AppendLine("          , @pData ) ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pRecordId", SqlDbType.BigInt).Value = RecordDetailInfo.RecordId;
          _cmd.Parameters.Add("@pDeleted", SqlDbType.Bit).Value = RecordDetailInfo.Deleted;
          _cmd.Parameters.Add("@pCreated", SqlDbType.DateTime).Value = RecordDetailInfo.Created;
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = RecordDetailInfo.Type;

          if (RecordDetailInfo.Expiration.HasValue)
            _cmd.Parameters.Add("@pExpiration", SqlDbType.DateTime).Value = RecordDetailInfo.Expiration;
          else
            _cmd.Parameters.Add("@pExpiration", SqlDbType.DateTime).Value = DBNull.Value;

          if (RecordDetailInfo.Image != null)
            _cmd.Parameters.Add("@pImage", SqlDbType.Image).Value = RecordDetailInfo.Image;
          else
            _cmd.Parameters.Add("@pImage", SqlDbType.Image).Value = DBNull.Value;

          _cmd.Parameters.Add("@pData", SqlDbType.VarChar).Value = RecordDetailInfo.Data;

          _cmd.ExecuteNonQuery();
        }
      }
      catch (Exception _ex)
      {
        Log.Error("DB_InsertRecordDetail: There was an error inserting new details");
        Log.Exception(_ex);
        return false;
      }

      return true;
    }

    /// <summary>
    /// Insert the internal blacklist data
    /// </summary>
    /// <param name="BlackListInfo">Blacklist information</param>
    /// <param name="SqlTrx"></param>
    /// <returns>Success/Fail</returns>
    private static Boolean DB_InsertBlackListInternal(BlackListInternal BlackListInfo,
                                                  SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("  INSERT INTO  BLACKLIST_INTERNAL_BLOCK_LIST ");
      _sb.AppendLine("          ( BKL_ACCOUNT_ID ");
      _sb.AppendLine("          , BKL_INCLUSION_DATE ");
      _sb.AppendLine("          , BKL_REASON ");
      _sb.AppendLine("          , BKL_REASON_DESCRIPTION ) ");
      _sb.AppendLine("  VALUES ");
      _sb.AppendLine("          ( @pAccountId ");
      _sb.AppendLine("          , @pInclusionDate ");
      _sb.AppendLine("          , @pReason ");
      _sb.AppendLine("          , @pReasonDescription ) ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = BlackListInfo.AccountId;
          _cmd.Parameters.Add("@pInclusionDate", SqlDbType.DateTime).Value = BlackListInfo.InclusionDate;
          _cmd.Parameters.Add("@pReason", SqlDbType.SmallInt).Value = BlackListInfo.Reason;
          _cmd.Parameters.Add("@pReasonDescription", SqlDbType.NVarChar).Value = BlackListInfo.ReasonDescription;

          _cmd.ExecuteNonQuery();
        }
      }
      catch (Exception _ex)
      {
        Log.Error("DB_InsertBlackListInternal: There was an error inserting internal blacklist data");
        Log.Exception(_ex);
        return false;
      }

      return true;
    }

    /// <summary>
    /// Remove account from internal black list
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns>Success/Fail</returns>
    private static Boolean DB_RemoveFromBlackListInternal(Int64 AccountId,
                                                  SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("  DELETE FROM BLACKLIST_INTERNAL_BLOCK_LIST ");
      _sb.AppendLine("  WHERE BKL_ACCOUNT_ID = @pAccountId ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.ExecuteNonQuery();
        }
      }
      catch (Exception _ex)
      {

        Log.Error("DB_RemoveFromBlackListInternal: There was an error removig data from Blacklist");
        Log.Exception(_ex);
        return false;
      }

      return true;
    }

    private static CustomerRecord BuildRecordData(SqlDataReader Reader)
    {
      CustomerRecord _data = new CustomerRecord
      {
        RecordId = (Int64)Reader["CUR_RECORD_ID"],
        CustomerId = (Int64)Reader["CUR_CUSTOMER_ID"],
        Created = (DateTime)Reader["CUR_CREATED"],
        Deleted = (Boolean)Reader["CUR_DELETED"],
        Expiration = (DateTime)Reader["CUR_EXPIRATION"]
      };
      return _data;
    }

    private static CustomerRecordDetail BuildRecordDetailData(SqlDataReader Reader)
    {
      CustomerRecordDetail _data = new CustomerRecordDetail
      {
        RecordDetailId = (Int64)Reader["CURD_DETAIL_ID"],
        RecordId = (Int64)Reader["CURD_RECORD_ID"],
        Created = (DateTime)Reader["CURD_CREATED"],
        Deleted = (Boolean)Reader["CURD_DELETED"]
      };
      if (!Reader.IsDBNull(Reader.GetOrdinal("CURD_EXPIRATION")))
        _data.Expiration = (DateTime)Reader["CURD_EXPIRATION"];
      _data.Type = (ENUM_TYPE_DETAIL)Reader["CURD_TYPE"];
      _data.Data = (String)Reader["CURD_DATA"];
      if (!Reader.IsDBNull(Reader.GetOrdinal("CURD_IMAGE")))
        _data.Image = (byte[])Reader["CURD_IMAGE"];
      return _data;
    }

    public static void UnregisterHandlers(Form ViewForm, BlackListResultHandler handler1, ExpiredDocumentsResultHandler handler2)
    {
      m_remote_control_blacklist = null;
      m_remote_handler_blacklist -= handler1;
      m_remote_control_expired_docs = null;
      m_remote_handler_expired_docs -= handler2;
    }
  }
}
