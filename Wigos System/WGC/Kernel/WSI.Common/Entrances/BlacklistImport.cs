﻿//------------------------------------------------------------------------------
// Copyright © 2007-2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME:
// 
//   DESCRIPTION: Imports blacklist from excel documents.
//
//        AUTHOR: Scott Adamson  
// 
// CREATION DATE: 16-FEB-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 16-FEB-2016 SJA   First release
// 24-JUN-2016 PDM   PBI 14776: Visitas / Recepción: Blacklist - mejorar importación Excel
// 31-OCT-2016 XGJ   Bug 19815:MALTA-Recepción: Al importar un fichero a la lista de prohibidos el campo middle name no puede estar vacío
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using WSI.Common.Entrances.Enums;
using WSI.Common.Utilities;

namespace WSI.Common.Entrances
{
  public class BlacklistImport : ImportFromExcel
  {
    public BlacklistImport(string Fileout)
      : base(Fileout)
    {
    }
    public BlacklistImport()
      : base()
    {
    }

    private static DataTable _dtDocTypes;
    public override string TableName
    {
      get { return "blacklist_file_imported"; }
    }

    public override ColumnDictionary ColumnMatches
    {
      get
      {
        return new ColumnDictionary()
          {
            {Resource.String("STR_FRM_BLACKLIST_IMPORT_REFERENCE"), new ColumnDefinition() {ColumnName = "blkf_reference", Alias =  Resource.String("STR_FRM_BLACKLIST_IMPORT_REFERENCE"), ColumnType = SqlDbType.NVarChar, Required =false }},
            {Resource.String("STR_FRM_BLACKLIST_IMPORT_NAME"), new ColumnDefinition() {ColumnName = "blkf_name", Alias =  Resource.String("STR_FRM_BLACKLIST_IMPORT_NAME"), ColumnType = SqlDbType.NVarChar, Required =GeneralParam.GetBoolean("Account.RequestedField", "Name1",true), MaxLength=50}},
            {Resource.String("STR_FRM_BLACKLIST_IMPORT_MIDDLENAME"), new ColumnDefinition() {ColumnName = "blkf_middle_name", Alias =  Resource.String("STR_FRM_BLACKLIST_IMPORT_MIDDLENAME"), ColumnType = SqlDbType.NVarChar,Required =GeneralParam.GetBoolean("Account.RequestedField", "Name4",true), MaxLength=50}},
            {Resource.String("STR_FRM_BLACKLIST_IMPORT_LASTNAME1"), new ColumnDefinition() {ColumnName = "blkf_lastname_1", Alias =  Resource.String("STR_FRM_BLACKLIST_IMPORT_LASTNAME1"), ColumnType = SqlDbType.NVarChar, Required =GeneralParam.GetBoolean("Account.RequestedField", "Name3",true), MaxLength=50}},
            {Resource.String("STR_FRM_BLACKLIST_IMPORT_LASTNAME2"), new ColumnDefinition() {ColumnName = "blkf_lastname_2", Alias =  Resource.String("STR_FRM_BLACKLIST_IMPORT_LASTNAME2"), ColumnType = SqlDbType.NVarChar, Required =GeneralParam.GetBoolean("Account.RequestedField", "Name2",true), MaxLength=50}},
            {Resource.String("STR_FRM_BLACKLIST_IMPORT_DOCUMENT_TYPE"),
              new ColumnDefinition()
                {
                  ColumnName = "blkf_document_type",
                  Alias =  Resource.String("STR_FRM_BLACKLIST_IMPORT_DOCUMENT_TYPE"), 
                  ColumnType = SqlDbType.SmallInt,
                  EnumTranslation = (X => GetDocTypeFor(X)),
                  DefaultValue="Other"
                }
            },
            {Resource.String("STR_FRM_BLACKLIST_IMPORT_DOCUMENT_NUMBER"), new ColumnDefinition() {ColumnName = "blkf_document", Alias =  Resource.String("STR_FRM_BLACKLIST_IMPORT_DOCUMENT_NUMBER"), ColumnType = SqlDbType.NVarChar, Required =GeneralParam.GetBoolean("Account.RequestedField", "Document",true), MaxLength=50}},
            {Resource.String("STR_FRM_BLACKLIST_IMPORT_EXCLUSION_DATE"),
              new ColumnDefinition() {ColumnName = "bklf_exclusion_date", Alias =  Resource.String("STR_FRM_BLACKLIST_IMPORT_EXCLUSION_DATE"), ColumnType = SqlDbType.DateTime, Required =true}
            },
            {Resource.String("STR_FRM_BLACKLIST_IMPORT_REASON_TYPE"),
              new ColumnDefinition()
                {
                  ColumnName = "bklf_reason_type",
                  Alias =  Resource.String("STR_FRM_BLACKLIST_IMPORT_REASON_TYPE"),
                  ColumnType = SqlDbType.SmallInt,
                  EnumTranslation = (X => GetInclusionReasonFor(X)),
                  Required=false,
                  DefaultValue=ENUM_BLACKLIST_REASON.OTHER 
                }
            },
            {Resource.String("STR_FRM_BLACKLIST_IMPORT_REASON_EXCLUSION"),
              new ColumnDefinition() {ColumnName = "bklf_reason_description",Alias =  Resource.String("STR_FRM_BLACKLIST_IMPORT_REASON_EXCLUSION"), ColumnType = SqlDbType.NVarChar,Required =true, MaxLength=50}
            },
            {Resource.String("STR_FRM_BLACKLIST_IMPORT_EXCLUSION_DURATION"),
              new ColumnDefinition() {ColumnName = "bklf_exclusion_duration", Alias =  Resource.String("STR_FRM_BLACKLIST_IMPORT_EXCLUSION_DURATION"),ColumnType = SqlDbType.Int,Required=false}
            },
            {Resource.String("STR_FRM_BLACKLIST_IMPORT_EXCLUSION_ACTION") ,
              new ColumnDefinition() {ColumnName = "", ColumnType = SqlDbType.VarChar}
            }
          };
      }
    }

    Dictionary<string, Enum> dict = EnumHelper.ToDictionary<ENUM_BLACKLIST_REASON>();

    private int GetInclusionReasonFor(string InclusionReasonText)
    {
      try
      {
        InclusionReasonText = InclusionReasonText.ToUpperInvariant();
        if (dict.ContainsKey(InclusionReasonText))
        {
          return (int)((ENUM_BLACKLIST_REASON)dict[InclusionReasonText]);
        }
        else
        {
          return -1;
        }
      }
      catch (Exception)
      {
        return (int)default(ENUM_BLACKLIST_REASON);
      }
    }

    /// <summary>
    /// transform document type from string to int
    /// </summary>
    /// <param name="DocTypeString"></param>
    /// <returns></returns>
    private int GetDocTypeFor(string DocTypeString)
    {
      if (_dtDocTypes == null)
      {
        DataRow _dr_empty_row;
        _dtDocTypes = IdentificationTypes.GetIdentificationTypes(true);
        _dr_empty_row = _dtDocTypes.NewRow();
        _dr_empty_row[0] = -1;
        _dr_empty_row[1] = "";
        _dtDocTypes.Rows.InsertAt(_dr_empty_row, 0);
      }
      foreach (DataRow _dt_doc_type in _dtDocTypes.Rows)
      {
        var _x = _dt_doc_type[1].ToString().Trim().ToUpperInvariant();
        if (!_x.Equals(DocTypeString.Trim().ToUpperInvariant()))
          continue;
        return (int)_dt_doc_type[0];
      }
      return -1;
    }
  }
}


