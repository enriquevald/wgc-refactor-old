﻿using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common.Utilities.Attributes.Enums;

namespace WSI.Common.Entrances.Enums
{
  public enum ENUM_BLACKLIST_SOURCE
  {
    [DescriptionResource("STR_ENUM_BLACKLIST_SOURCE_LOCAL")]
    LOCAL = 0,
    [DescriptionResource("STR_ENUM_BLACKLIST_SOURCE_EXTERNAL_FILE")]
    EXTERNAL_FILE = 1,
    [DescriptionResource("STR_ENUM_BLACKLIST_SOURCE_EXTERNAL_WS")]
    EXTERNAL_WS = 2
  }
}
