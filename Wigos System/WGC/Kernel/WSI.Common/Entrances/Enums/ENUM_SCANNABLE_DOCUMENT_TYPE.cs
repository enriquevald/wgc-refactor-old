﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.Entrances.Enums
{
  public enum ENUM_SCANNABLE_DOCUMENT_TYPE
  {
    PHOTO = 0,
    DNI = 1,
    PASSPORT = 2,
    SS = 3,
    DRIVE_LICENSE = 4,
    OTHER = 5
  }
}
