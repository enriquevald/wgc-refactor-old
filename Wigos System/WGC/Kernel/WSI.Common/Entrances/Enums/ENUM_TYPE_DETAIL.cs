﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.Entrances.Enums
{
  public enum ENUM_TYPE_DETAIL
  {
    ACCOUNT_ID = 1,
    FIRST_NAME = 2,
    SECOND_NAME = 3,
    DOCUMENT = 4,
    BIRTH_DATE = 5,
    GENDER = 6,
    NATIONALITY = 7,
    COUNTRY = 8,
    FIRST_SURNAME = 9,
    SECOND_SURNAME = 10,
    DOCUMENT_TYPE = 11,
    EMAIL = 12,
    SECOND_TEL_NUMBER = 13,
    FIRST_TEL_NUMBER = 14,
    COMMENTS = 15,
    TWITTER = 16,
    FACEBOOK = 17,
    ACEPTED_AGREEMENT = 18,
    XML_ADDRESS = 500,
    XML_SCANNED_DOCUMENT = 501
    /*
     SJA 15/09/2016 aprx.
     
     dynamic value of 5999 + identification_type IDT_ID = specific id type saved in db
        lowest value of idt_id is 1, therefore range is 6000 +
           capped at 7000
     */
  }
}
