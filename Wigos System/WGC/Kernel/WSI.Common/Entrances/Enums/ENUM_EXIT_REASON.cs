﻿//------------------------------------------------------------------------------
// Copyright © 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AccountAddCredit.cs
// 
//   DESCRIPTION: enum exit reason table CUSTOMERS_ENTRANCES
// 
//        AUTHOR: Alberto Marcos
// 
// CREATION DATE: 09-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-OCT-2017 AMF    Initial version
// ----------- ------ ----------------------------------------------------------

namespace WSI.Common.Entrances.Enums
{
  public enum ENUM_EXIT_REASON
  {
    NONE = 0,
    RECEPTION = 1,
    DISABLED = 2
  }
}
