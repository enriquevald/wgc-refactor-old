﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common.Entrances.Enums
{
  public enum ENUM_RECEPTION_MODE
  {
    Lite = 0,
    TicketOnly = 1,
    TicketWithPrice = 2
  }
}
