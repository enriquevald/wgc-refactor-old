﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;
using WSI.Common.Utilities;
using WSI.Common.Utilities.Attributes.Enums;

namespace WSI.Common.Entrances.Enums
{
  public enum ENUM_BLACKLIST_REASON
  {
    [DescriptionResource("STR_ENUM_BLACKLIST_REASON_UNKNOWN")]
    UNKNOWN = -1,
    [DescriptionResource("STR_ENUM_BLACKLIST_REASON_ALCOHOLIC")]
    ALCOHOLIC = 0,
    [DescriptionResource("STR_ENUM_BLACKLIST_REASON_COMPULSIVE_GAMBLER")]
    COMPULSIVE_GAMBLER = 1,
    [DescriptionResource("STR_ENUM_BLACKLIST_REASON_IN_DEBT")]
    IN_DEBT = 2,
    [DescriptionResource("STR_ENUM_BLACKLIST_REASON_OTHER")]
    OTHER = 3,
    [DescriptionResource("STR_ENUM_BLACKLIST_REASON_BLOCK")]
    BLOCK = 4,
    GENERIC_ERROR = 999,
  }

}
