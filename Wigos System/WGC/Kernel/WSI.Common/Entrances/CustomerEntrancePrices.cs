﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CustomerEntrancePrices.cs
// 
//   DESCRIPTION: Class to manage customer entrance price
//
//        AUTHOR: YNM
// 
// CREATION DATE: 19-JAN-2016
//  
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-JAN-2016  YNM    First release.
// 10-MAY-2015  JBP    Product Backlog Item 12931:Visitas / Recepción: 2º Ticket LOPD
// -----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common.Entrances.Entities.Persistence;

namespace WSI.Common.Entrances
{
  public static class CustomerEntrancePrices
  {
    /// <summary>
    /// Save new CustomerEntrancePrice
    /// </summary>
    /// <param name="CEPrice">Record to save</param>
    /// <param name="SqlTrx">SQL Trasaction</param>
    /// <returns>if CustomerEntrancePrice was saved</returns>
    public static Boolean Save(CustomerEntrancePrice CEPrice,
                                                  SqlTransaction SqlTrx)
    { return true; }

    /// <summary>
    /// Get a list of differents CustomerEntrancePrice filtered by level
    /// </summary>
    /// <param name="Level">Highest level to find</param>
    /// <param name="SqlTrx">SQL Trasaction</param>
    /// <returns></returns>
    public static Boolean GetCEPricesByLevel(int Level, out DataTable DTCuep)
    {
      StringBuilder _sb;
      
      DTCuep = new DataTable("CUSTOMER_ENTRANCES_PRICES");

      _sb = new StringBuilder();
      
      _sb.AppendLine("  SELECT  CUEP_PRICE_ID                                    ");
      _sb.AppendLine("        , CUEP_DESCRIPTION                                 ");
      _sb.AppendLine("        , CAST(CUEP_PRICE AS DECIMAL(12,2)) AS CUEP_PRICE  "); 
      _sb.AppendLine("        , CUEP_CUSTOMER_LEVEL                              ");
      _sb.AppendLine("        , CUEP_DEFAULT                                     ");
      _sb.AppendLine("        , CUEP_VALID_GAMING_DAYS                           ");
      _sb.AppendLine("    FROM  CUSTOMER_ENTRANCES_PRICES                        ");
      if (Level >= 0)
      {
        _sb.AppendLine(" WHERE  CUEP_CUSTOMER_LEVEL <= @pLevel                   ");
      }
      _sb.AppendLine("ORDER BY  CUEP_CUSTOMER_LEVEL DESC                         ");   
    
      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _trx.SqlTransaction.Connection, _trx.SqlTransaction))
          {
            if (Level >= 0)
            {
            _cmd.Parameters.Add("@pLevel", SqlDbType.Int).Value = Level;
            }

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(DTCuep);
            }                               
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return true;
    }     

    /// <summary>
    /// Get CustomerEntrancePrice by Id
    /// </summary>
    /// <param name="CEPriceId">ID</param>
    /// <param name="SqlTrx">SQL Trasaction</param>
    /// <returns></returns>
    public static CustomerEntrancePrice GetCEPricesById(Int64 CEPriceId,
                                                  SqlTransaction SqlTrx)
    {
       CustomerEntrancePrice _cuep;

      _cuep = new CustomerEntrancePrice();

      return _cuep;
    
    }

    /// <summary>
    /// Get a list of differents CustomerEntrancePrice filtered by level and lower prices
    /// </summary>
    /// <param name="Level">Highest level to find</param>
    /// <param name="SqlTrx">SQL Trasaction</param>
    /// <returns></returns>
    public static Boolean GetCEPricesToCashier(int Level, out DataTable DTCuep)
    {
      StringBuilder _sb;

      DTCuep = new DataTable("CUSTOMER_ENTRANCES_PRICES");

      _sb = new StringBuilder();

      //_sb.AppendLine("DECLARE @MAX_AMOUNT AS MONEY");
      //_sb.AppendLine("SET @MAX_AMOUNT = (SELECT MAX(CUEP_PRICE)");
      //_sb.AppendLine("                   FROM CUSTOMER_ENTRANCES_PRICES  ");
      //_sb.AppendLine("                   WHERE CUEP_CUSTOMER_LEVEL = @pLevel) ");
      //_sb.AppendLine(" ");
      _sb.AppendLine("  SELECT CUEP_PRICE_ID            ");
      _sb.AppendLine("       , CUEP_DESCRIPTION         ");
      _sb.AppendLine("       , CUEP_PRICE               ");
      _sb.AppendLine("       , CUEP_CUSTOMER_LEVEL      ");
      _sb.AppendLine("       , CUEP_DEFAULT             ");
      _sb.AppendLine("       , CUEP_VALID_GAMING_DAYS   ");
      _sb.AppendLine("  FROM CUSTOMER_ENTRANCES_PRICES  ");
      _sb.AppendLine("  WHERE CUEP_CUSTOMER_LEVEL = @pLevel   ");
      //_sb.AppendLine("     OR CUEP_PRICE >= @MAX_AMOUNT       ");
      _sb.AppendLine("     OR CUEP_DEFAULT= 1                 ");
      _sb.AppendLine("  ORDER BY CUEP_CUSTOMER_LEVEL DESC, CUEP_PRICE, CUEP_VALID_GAMING_DAYS DESC ");

      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _trx.SqlTransaction.Connection, _trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pLevel", SqlDbType.Int).Value = Level;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
            {
              _sql_da.Fill(DTCuep);
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return true;
    }     

    /// <summary>
    /// Update data for CustomerEntrancePrice
    /// </summary>
    /// <param name="CEPrice"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public static Boolean DBUpdate(CustomerEntrancePrice CEPrice,
                                                  SqlTransaction SqlTrx)
    { 

      return true;
    }


    /// <summary>
    /// BatchUpdate for CUSTOMER_ENTRANCES_PRICES (Insert, Update or Delete)
    /// </summary>
    /// <param name="CustomerEntrancePrices"></param>
    /// <param name="_sql_trx"></param>
    /// <returns></returns>
    public static Boolean InsertCustomerEntrancePrices(DataTable CustomerEntrancePrices, SqlTransaction _sql_trx)
    {
      StringBuilder _sb_update;
      SqlDataAdapter _da_customer_entrance_price;
      SqlCommand _sql_command_customer_entrance_price;

      try
      {
        _da_customer_entrance_price = new SqlDataAdapter();

        // SELECT COMMAND
        _sb_update = new StringBuilder();
        _sb_update.AppendLine(" SELECT * FROM CUSTOMER_ENTRANCES_PRICES WHERE CUEP_PRICE_ID = @pCuepPriceId");
        _sql_command_customer_entrance_price = new SqlCommand(_sb_update.ToString(), _sql_trx.Connection, _sql_trx);
        _sql_command_customer_entrance_price.Parameters.Add("@pCuepPriceId", SqlDbType.BigInt);
        _da_customer_entrance_price.SelectCommand = _sql_command_customer_entrance_price;

        // INSERT COMMAND
        _sb_update = new StringBuilder();
        _sb_update.AppendLine(" INSERT INTO   CUSTOMER_ENTRANCES_PRICES   ");
        _sb_update.AppendLine("             ( CUEP_DESCRIPTION            ");
        _sb_update.AppendLine("             , CUEP_PRICE                  ");
        _sb_update.AppendLine("             , CUEP_CUSTOMER_LEVEL         ");
        _sb_update.AppendLine("             , CUEP_DEFAULT                ");
        _sb_update.AppendLine("             , CUEP_VALID_GAMING_DAYS      ");
        _sb_update.AppendLine("             )                             ");
        _sb_update.AppendLine("      VALUES ( @CuepDescription            ");
        _sb_update.AppendLine("             , @CuepPrice                  ");
        _sb_update.AppendLine("             , @CuepCustomerLevel          ");
        _sb_update.AppendLine("             , @CuepDefault                ");
        _sb_update.AppendLine("             , @CuepValidGamingDays        ");
        _sb_update.AppendLine("             )                             ");
        _sql_command_customer_entrance_price = new SqlCommand(_sb_update.ToString(), _sql_trx.Connection, _sql_trx);
        _sql_command_customer_entrance_price.Parameters.Add("@CuepDescription", SqlDbType.NVarChar, 50, "cuep_description");
        _sql_command_customer_entrance_price.Parameters.Add("@CuepPrice", SqlDbType.Money, 8, "cuep_price");
        _sql_command_customer_entrance_price.Parameters.Add("@CuepCustomerLevel", SqlDbType.Int, System.Int32.MaxValue, "cuep_customer_level");
        _sql_command_customer_entrance_price.Parameters.Add("@CuepDefault", SqlDbType.Bit, 1, "cuep_default");
        _sql_command_customer_entrance_price.Parameters.Add("@CuepValidGamingDays", SqlDbType.Int, System.Int32.MaxValue, "cuep_valid_gaming_days");

        _da_customer_entrance_price.InsertCommand = _sql_command_customer_entrance_price;

        // UPDATE COMMAND
        _sb_update = new StringBuilder();
        _sb_update.AppendLine(" UPDATE    CUSTOMER_ENTRANCES_PRICES                       ");
        _sb_update.AppendLine("    SET    CUEP_DESCRIPTION        =  @CuepDescription     ");
        _sb_update.AppendLine("        ,  CUEP_PRICE              =  @CuepPrice           ");
        _sb_update.AppendLine("        ,  CUEP_CUSTOMER_LEVEL     =  @CuepCustomerLevel   ");
        _sb_update.AppendLine("        ,  CUEP_DEFAULT            =  @CuepDefault         ");
        _sb_update.AppendLine("        ,  CUEP_VALID_GAMING_DAYS  =  @CuepValidGamingDays ");
        _sb_update.AppendLine("  WHERE    CUEP_PRICE_ID           = @pCuepPriceId         ");
        _sql_command_customer_entrance_price = new SqlCommand(_sb_update.ToString(), _sql_trx.Connection, _sql_trx);
        _sql_command_customer_entrance_price.Parameters.Add("@pCuepPriceId", SqlDbType.BigInt, System.Int32.MaxValue, "CUEP_PRICE_ID");
        _sql_command_customer_entrance_price.Parameters.Add("@CuepDescription", SqlDbType.NVarChar, 50, "cuep_description");
        _sql_command_customer_entrance_price.Parameters.Add("@CuepPrice", SqlDbType.Money, 8, "cuep_price");
        _sql_command_customer_entrance_price.Parameters.Add("@CuepCustomerLevel", SqlDbType.Int, System.Int32.MaxValue, "cuep_customer_level");
        _sql_command_customer_entrance_price.Parameters.Add("@CuepDefault", SqlDbType.Bit, 1, "cuep_default");
        _sql_command_customer_entrance_price.Parameters.Add("@CuepValidGamingDays", SqlDbType.Int, System.Int32.MaxValue, "cuep_valid_gaming_days");

        _da_customer_entrance_price.UpdateCommand = _sql_command_customer_entrance_price;

        // DELETE COMMAND
        _sb_update = new StringBuilder();
        _sb_update.AppendLine(" DELETE                                ");
        _sb_update.AppendLine("   FROM  CUSTOMER_ENTRANCES_PRICES     ");  
        _sb_update.AppendLine("  WHERE  CUEP_PRICE_ID = @pCuepPriceId ");
        _sql_command_customer_entrance_price = new SqlCommand(_sb_update.ToString(), _sql_trx.Connection, _sql_trx);
        _sql_command_customer_entrance_price.Parameters.Add("@pCuepPriceId", SqlDbType.BigInt, System.Int32.MaxValue, "CUEP_PRICE_ID");
        _da_customer_entrance_price.DeleteCommand = _sql_command_customer_entrance_price;

        _da_customer_entrance_price.Update(CustomerEntrancePrices);

      return true;
    }
      catch (DBConcurrencyException _ex)
      {
        Log.Exception(_ex);
        return false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      
    }
  }
}
