//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : TerminalStatus.cs
// 
//   DESCRIPTION : Class to contain information regarding status of a terminal
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-JAN-2014 ICS    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common
{
  class TerminalBitStatus
  {
    #region Members
    
    // Bitmaps
    private Int32 m_bm_doors;
    private Int32 m_bm_bills;
    private Int32 m_bm_printer;

    // Flags - Doors
    private Boolean m_fl_slot_door;
    private Boolean m_fl_drop_door;
    private Boolean m_fl_card_cage;
    private Boolean m_fl_belly_door;
    private Boolean m_fl_cashbox_door;

    // Flags - Bills
    private Boolean m_fl_bill_jam;
    private Boolean m_fl_hardware_failure;

    // Flags - Printer
    private Boolean m_fl_print_comm_error;
    private Boolean m_fl_print_paper_out_error;
    private Boolean m_fl_print_paper_low;
    private Boolean m_fl_print_carriage_jam;
    private Boolean m_fl_print_power_error;

    #endregion

    #region Properties

    #endregion

    #region Private Methods
    #endregion

    #region Public Methods
    #endregion
  }
}
