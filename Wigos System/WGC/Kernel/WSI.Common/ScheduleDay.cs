//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: ScheduleDay.cs
// 
//   DESCRIPTION: ScheduleDay class
// 
//        AUTHOR: Joaquim Cid 
// 
// CREATION DATE: 15-JUN-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-JUN-2012 JCM    First version.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common
{
  public static class ScheduleDay
  {
    public enum SCHEDULE_DAY_MASK
    {
      TYPE_MASK     = 0x7F000000,
      TYPE_WEEKDAY  = 0x00000000,
      TYPE_MONTH    = 0x01000000,
      TYPE_BIRTH    = 0x02000000,
      TYPE_NONE     = 0x7F000000,

      MONTHDAY_MASK = 0x000000FF,
      WEEKDAY_MASK  = 0x000000FF,

      MONTH_END     = 0x00000020,

      MONDAY        = 0x00000001,
      TUESDAY       = 0x00000002,
      WEDNESDAY     = 0x00000004,
      THURSDAY      = 0x00000008,
      FRIDAY        = 0x00000010,
      SATURDAY      = 0x00000020,
      SUNDAY        = 0x00000040, 

      BIRTH_DAY     = 0x00000001,
      BIRTH_MONTH   = 0x00000002

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Return the SCHEDULE_DAY_MASK of DayOfWeek
    //
    //  PARAMS:
    //      - INPUT:
    //        - Weekday : 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - SCHEDULE_DAY_MASK: Returns the Mask for WeekDay
    //
    //   NOTES:
    //
    public static SCHEDULE_DAY_MASK DayOfWeekMask(DayOfWeek WeekDay)
    {
      switch (WeekDay)
      {
        case DayOfWeek.Monday:
          return SCHEDULE_DAY_MASK.MONDAY;
        case DayOfWeek.Tuesday:
          return SCHEDULE_DAY_MASK.TUESDAY;
        case DayOfWeek.Wednesday:
          return SCHEDULE_DAY_MASK.WEDNESDAY;
        case DayOfWeek.Thursday:
          return SCHEDULE_DAY_MASK.THURSDAY;
        case DayOfWeek.Friday:
          return SCHEDULE_DAY_MASK.FRIDAY;
        case DayOfWeek.Saturday:
          return SCHEDULE_DAY_MASK.SATURDAY;
        case DayOfWeek.Sunday:
          return SCHEDULE_DAY_MASK.SUNDAY;
      }

      return SCHEDULE_DAY_MASK.TYPE_NONE;

    }                  // WeekDayMask

    //------------------------------------------------------------------------------
    // PURPOSE: Return the Periode Schedule type is defined in ScheduleWeekDays mask
    //
    //  PARAMS:
    //      - INPUT:
    //        - ScheduleDays : Schedule Mask  
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - SCHEDULE_DAY_MASK: Schedule type (Week, Month, Birth)
    //
    //   NOTES:
    //
    public static SCHEDULE_DAY_MASK GetTypePeriod(Int32 ScheduleDays)
    {
      switch (ScheduleDays & (Int32)SCHEDULE_DAY_MASK.TYPE_MASK)
      {
        case (Int32)SCHEDULE_DAY_MASK.TYPE_WEEKDAY:
        case (Int32)SCHEDULE_DAY_MASK.TYPE_MONTH:
        case (Int32)SCHEDULE_DAY_MASK.TYPE_BIRTH:
          return (SCHEDULE_DAY_MASK)(ScheduleDays & (Int32)SCHEDULE_DAY_MASK.TYPE_MASK);

        default:
          return SCHEDULE_DAY_MASK.TYPE_NONE;

      }
    }                // GetTypePeriod
    
    //------------------------------------------------------------------------------
    // PURPOSE: Check if WeekDay is in ScheduleDays  
    //
    //  PARAMS:
    //      - INPUT:
    //        - WeekDay :   Day of week to check
    //        - ScheduleDays : Schedule Mask  
    //      - OUTPUT:
    //
    // RETURNS:
    //    - Boolean : True if Type Mask is scheduled Weekly and WeekDay is in ScheduleDays  
    //                False otherwise
    //   NOTES:
    //
    public static Boolean WeekDayIsInSchedule(DayOfWeek WeekDay, Int32 ScheduleDays)
    {
      if (GetTypePeriod(ScheduleDays) == SCHEDULE_DAY_MASK.TYPE_WEEKDAY)
      {
        return (ScheduleDays & (Int32)DayOfWeekMask(WeekDay)) != 0;
      }

      return false;
    } // WeekDayIsInSchedule

    //------------------------------------------------------------------------------
    // PURPOSE: Return the nearest date of BaseDate that is in ScheduleWeekDays 
    //          Time is set on Open Time
    //  PARAMS:
    //      - INPUT:
    //        - BaseDate : Start date to look
    //        - Now
    //        - ScheduleWeekdays : Mask of days 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - DateTime: If Mask is WEEKLY: The DayOfWeek is in ScheduleWeekDays and is nearest of BaseDate.
    //                                   Looks forward from BaseDate
    //                                   If not found, return DateTime.MinValue
    //                          MONTHLY: The day seted on ScheduleDays with Month and Year of BaseDate 
    //   NOTES:
    //

    public static DateTime GetScheduleDate(DateTime Now, Int32 ScheduleDays)
    {
      DateTime _today;
      Int32 _day;
      Int32 _last_day;
      Int32 _num_days;

      _today = Misc.Opening(Now);
      _num_days = 0;

      switch (GetTypePeriod(ScheduleDays))
      {
        case SCHEDULE_DAY_MASK.TYPE_WEEKDAY:

          while (_num_days < 7)
          {
            if ((ScheduleDays & (Int32)DayOfWeekMask(_today.DayOfWeek)) != 0)
            {
              return _today;
            }

            _today = _today.AddDays(-1);
            _num_days++;
          }

          Log.Warning("GetScheduleDate: Can't obtain date of TYPE_WEEKDAY. Now: " + Now.ToString() + ", ScheduleDays: " + ScheduleDays.ToString() + ".");
          break;

        case SCHEDULE_DAY_MASK.TYPE_MONTH:

          _day = ScheduleDays & (Int32)SCHEDULE_DAY_MASK.MONTHDAY_MASK;

          if (_day <= 0 || _day > 32)
          {
            return DateTime.MinValue;
          }

          while (_num_days < 366)
          {
            if (_day == _today.Day)
            {
              return _today;
            }

            if (_day == (Int32)SCHEDULE_DAY_MASK.MONTH_END)
            {
              _last_day = DateTime.DaysInMonth(_today.Year, _today.Month);
              if (_day == _last_day)
              {
                return _today;
              }
            }

            _today = _today.AddDays(-1);
            _num_days++;
          }

          Log.Warning("GetScheduleDate: Can't obtain date of TYPE_MONTH. Now: " + Now.ToString() + ", ScheduleDays: " + ScheduleDays.ToString() + ".");
          break;

        case SCHEDULE_DAY_MASK.TYPE_BIRTH:
          return _today;

        default:
          break;
      }

      return DateTime.MinValue;
    } // GetScheduleDate

    //------------------------------------------------------------------------------
    // PURPOSE: Get the PM_SCHEDULE_WEEKDAY value according to that user has input
    //
    //  PARAMS:
    //      - INPUT:
    //        - Weekday : 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - SCHEDULE_DAY_MASK: Returns the Mask for WeekDay
    //
    //   NOTES:
    //
    public static Int32 GetScheduleDayValue (Int32 MonthsDay, Promotion.PROMOTION_TYPE PromoType)
    {
      switch (PromoType)
      {
        case Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED:
        case Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL:

          return (Int32)SCHEDULE_DAY_MASK.TYPE_MONTH + MonthsDay;

        case Promotion.PROMOTION_TYPE.PERIODIC_PER_BIRTHDAY:

          return (Int32)SCHEDULE_DAY_MASK.TYPE_BIRTH;
      }

      return (Int32)SCHEDULE_DAY_MASK.TYPE_NONE;
    }   // GetScheduleDayValue
  
    //------------------------------------------------------------------------------
    // PURPOSE: Gets the month's day according to the PM_SCHEDULE_WEEKDAY value
    //
    //  PARAMS:
    //      - INPUT:
    //        - ScheduleWeekdayValue : 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //    - SCHEDULE_DAY_MASK: Returns the Mask for WeekDay
    //
    //   NOTES:
    //
    public static Int32 GetMonthDay (Int32 ScheduleWeekDayValue) 
    {
      if (GetTypePeriod(ScheduleWeekDayValue) == SCHEDULE_DAY_MASK.TYPE_MONTH)
      {
        return ScheduleWeekDayValue & (Int32) SCHEDULE_DAY_MASK.MONTHDAY_MASK;
      }
      else 
      {
        return (Int32)SCHEDULE_DAY_MASK.TYPE_NONE;
      }
    } // GetMonthDay

    //------------------------------------------------------------------------------
    // PURPOSE: Indicates if TimeToTest is between IntervalStart and IntervalEnd.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - TimeSpan TimeToTest
    //          - Int32 IntervalStart
    //          - Int32 IntervalEnd
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Boolean

    public static Boolean IsTimeInRange(TimeSpan  TimeToTest, 
                                        Int32     IntervalStart, 
                                        Int32     IntervalEnd)
    {
      Int32 _time_of_day;

      if ( IntervalStart == IntervalEnd )
      {
        // Intervals with the same value in both ends are used to indicate
        // promotions that are valid all day long
        return true;
      }

      _time_of_day = (Int32)TimeToTest.TotalSeconds;

      // Behavior varies if intervals spans over two days or not
      if ( IntervalStart < IntervalEnd )
      {
        // Interval to check is contained in one day
        return (IntervalStart <= _time_of_day && _time_of_day < IntervalEnd);
      }
      else
      {
        // Interval to check spans over two days
        return (IntervalStart <= _time_of_day || _time_of_day < IntervalEnd);
      }
    } // IsTimeInRange

    //------------------------------------------------------------------------------
    // PURPOSE : Check whether a time range (TimeRangeStart,TimeRangeEnd) intersects 
    //           another time range (IntervalStart,IntervalEnd)
    //
    //  PARAMS :
    //      - INPUT :
    //          - TimeRangeStart : expressed as amount of seconds from day start
    //          - TimeRangeEnd : expressed as amount of seconds from day start
    //          - IntervalStart : expressed as amount of seconds from day start
    //          - IntervalEnd : expressed as amount of seconds from day start
    //
    //      - OUTPUT :
    //
    // RETURNS : 
    //
    //   NOTES :

    public static Boolean IsTimeRangeInRange (Int32     TimeRangeStart,
                                              Int32     TimeRangeEnd,
                                              Int32     IntervalStart,
                                              Int32     IntervalEnd)
    {
      TimeSpan _timespan;

      //  Condition 1                |--- Time Range ---|
      //                    |--- Interval ---|

      _timespan = new TimeSpan (0, 0, (int) TimeRangeStart);

      if ( ScheduleDay.IsTimeInRange (_timespan, IntervalStart, IntervalEnd) )
      {
        return true;
      }

      //  Condition 2       |--- Time Range ---|
      //                             |--- Interval ---|
      
      _timespan = new TimeSpan (0, 0, (int) TimeRangeEnd);

      if ( ScheduleDay.IsTimeInRange(_timespan, IntervalStart, IntervalEnd) )
      {
        return true;
      }

      //  Condition 3                |--- Interval ---|
      //                    |--- Time Range ---|

      _timespan = new TimeSpan (0, 0, (int) IntervalStart);

      if ( ScheduleDay.IsTimeInRange(_timespan, (Int32)TimeRangeStart, (Int32)TimeRangeEnd) )
      {
        return true;
      }

      //  Condition 4       |--- Interval ---|
      //                             |--- Time Range ---|

      _timespan = new TimeSpan (0, 0, (int) IntervalEnd);

      if ( ScheduleDay.IsTimeInRange (_timespan, (Int32)TimeRangeStart, (Int32)TimeRangeEnd) )
      {
        return true;
      }

      return false;

    } // CheckDatePeriods
  }
}
