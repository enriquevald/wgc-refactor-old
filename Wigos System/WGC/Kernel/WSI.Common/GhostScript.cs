//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: GhostScript.cs
// 
//   DESCRIPTION: Methods to print pdf files with ghostscript.
//                Documentation: http://ghostscript.com/doc/current/Readme.htm
//
//                Documentation gsprint: http://pages.cs.wisc.edu/~ghost/gsview/gsprint.htm
//                Parameters aviables for gsprint.exe:
//                  -help:Print usage summary
//                  -mono:Render in monochrome as 1bit/pixel
//                  -grey or -gray:Render in greyscale as 8bits/pixel
//                  -colour or -color:Render in colour as 24bits/pixel
//                  -query:Show printer setup dialog
//                  -noquery:Don't show printer setup dialog
//                  -printer "name":Print to the specified printer
//                  -noprinter:Use default printer
//                  -port "name":Print to the specified printer port, instead of the default for the printer
//                  -ghostscript "name":Path and filename of command line Ghostscript
//                  -config "name":Read options from this file, one argument per line.
//                  -odd:Print only odd pages
//                  -even:Print only even pages
//                  -all:Print all pages
//                  -from NN:First page to print is NN
//                  -to NN:Last page to print is NN
//                  -twoup:Two pages per sheet
//                  -portrait:Portrait orientation
//                  -landscape:Landscape orientation
//                  -duplex_vertical:Duplex (for long edge binding)
//                  -duplex_horizontal:Duplex (for short edge binding)
//                  -copies NN:Print NN copies (if supported by Windows printer driver)
//                  "filename":The PostScript/PDF file to print
//
//        AUTHOR: MPO
// 
// CREATION DATE: 20-FEB-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-FEB-2012 MPO    First release.
// 02-MAY-2012 MPO    Change default printer GRAY to COLOUR 
// 02-FEB-2015 MPO    WIG-1969: System crash when printing "constancias"
// 05-OCT-2015 FAV    Fixed Bug 5011: Some document using a network printer doesn't print 
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing.Printing;
using System.IO;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Collections;
using System.Windows.Forms;
using System.Diagnostics;

namespace WSI.Common
{
  public class GhostScript
  {

    #region Imports

    [DllImport("gsdll32.dll", EntryPoint = "gsapi_new_instance")]
    private static extern int CreateAPIInstance(out IntPtr pinstance, IntPtr caller_handle);

    [DllImport("gsdll32.dll", EntryPoint = "gsapi_init_with_args")]
    private static extern int InitAPI(IntPtr instance, int argc, string[] argv);

    [DllImport("gsdll32.dll", EntryPoint = "gsapi_exit")]
    private static extern int ExitAPI(IntPtr instance);

    [DllImport("gsdll32.dll", EntryPoint = "gsapi_delete_instance")]
    private static extern void DeleteAPIInstance(IntPtr instance);

    #endregion

    #region Members

    private static Object m_resource_lock = new object();
    private static Int16 m_copies;
    private static Int16 m_current_page;
    private static List<String> m_list_img;

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Use the API GhostScript for convert the pdf file to a image and  then print the image.
    //
    //  PARAMS:
    //      - INPUT: Params: The parameteres aviables.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Boolean: true if is printed
    // 
    //   NOTES:
    //   

    public static Boolean Print(GhostScriptParameter PrintParams)
    {
      IntPtr _gs_instance_ptr; // Get a pointer to an instance of the Ghostscript API and run the API with the current arguments
      String _tmp_filename;
      String _base_filename;
      String _base_directory;
      int _result;
      String[] _params;
      String[] _list_images;
      PrintDocument _pd;
      PrintDialog _print_dialog;
      DialogResult _dialog_result;
      Boolean _get_files;

      _gs_instance_ptr = IntPtr.Zero;
      _tmp_filename = "";
      _get_files = false;
      _base_directory = "";
      _base_filename ="";

      lock (m_resource_lock)
      {
        CreateAPIInstance(out _gs_instance_ptr, IntPtr.Zero);
        try
        {

          m_list_img = new List<String>();
          if (String.IsNullOrEmpty(PrintParams.OutputFile))
          {
            _tmp_filename = Path.GetTempFileName();            
            PrintParams.OutputFile = _tmp_filename + ".%d.png";
            _base_filename = Path.GetFileName(_tmp_filename);
            _base_directory = Path.GetDirectoryName(_tmp_filename);
            _get_files = true;
          }
          else
          {
            m_list_img.Add(PrintParams.OutputFile);
          }          

          _params = PrintParams.GetParametersGhostScript();
          _result = InitAPI(_gs_instance_ptr, _params.Length, _params);
 
          if (_result < 0)
          {
            Cleanup(_gs_instance_ptr);
            throw new ExternalException("Ghostscript conversion error", _result);
          }

          Cleanup(_gs_instance_ptr);

          if (_get_files)
          {
            _list_images = Directory.GetFiles(_base_directory, _base_filename + "*.png");
            m_list_img.AddRange(_list_images);
          }           

          _pd = new PrintDocument();
          _pd.PrinterSettings.Copies = PrintParams.Copies;
          _pd.PrinterSettings.PrinterName = PrintParams.PrinterName;

          if (PrintParams.PrinterDialog)
          {
            _print_dialog = new PrintDialog();
            try
            {
              _print_dialog.PrinterSettings = _pd.PrinterSettings;
              _print_dialog.UseEXDialog = true;
              _dialog_result = _print_dialog.ShowDialog();

              if (_dialog_result != DialogResult.OK)
              {

                return false;
              }

              _pd.PrinterSettings = _print_dialog.PrinterSettings;
            }
            finally
            {
              // RCI & ICS 16-APR-2014 Free the object
              _print_dialog.Dispose();
            }
          }

          m_copies = _pd.PrinterSettings.Copies;
          m_current_page = 0;
          _pd.PrintController = new StandardPrintController();          
          _pd.PrintPage += new PrintPageEventHandler(PrintPage);
          _pd.EndPrint += new PrintEventHandler(EndPrint);
          _pd.Print();

          return true;
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);

          return false;
        }
        finally
        {
        }
      }

    } // Print

    //------------------------------------------------------------------------------
    // PURPOSE: Check if exists the library "gsdll32.dll"
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Boolean: true if check is correct
    // 
    //   NOTES:
    //   

    static public Boolean ApplicationAvailable()
    {
      String _path_app;

      _path_app = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
      if (!File.Exists(Path.Combine(_path_app, "gsdll32.dll")))
      {
        return false;
      }

      return true;
    } //ApplicationAvailable

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Call the API for clean the instance
    //
    //  PARAMS:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //  

    private static void Cleanup(IntPtr InstancePtr)
    {
      ExitAPI(InstancePtr);
      DeleteAPIInstance(InstancePtr);
    } // Cleanup

    //------------------------------------------------------------------------------
    // PURPOSE:  Specific event for each page to print.
    //
    //  PARAMS:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //  

    private static void PrintPage(object o, PrintPageEventArgs e)
    {
      String _img_path;

      _img_path = m_list_img[m_current_page];

      if (File.Exists(_img_path))
      {
        Image _img;
        FileStream _img_fs;
        Point _loc;

        _img_fs = new FileStream(_img_path, FileMode.Open);

        _img = Image.FromStream(_img_fs);
        _img_fs.Close();
        _loc = new Point(0, 0);

        e.Graphics.DrawImageUnscaled(_img, _loc);

      }

      if (m_current_page < m_list_img.Count-1)
      {
        e.HasMorePages = true;
        m_current_page++;
      }
      else
      {
        e.HasMorePages = ((m_copies--) > 1);
        m_current_page = 0;
      }      

    } // PrintPage

    //------------------------------------------------------------------------------
    // PURPOSE:  specific event at the end of the printing.
    //
    //  PARAMS:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //  

    private static void EndPrint(object o, PrintEventArgs e)
    {

      foreach (String _img_path in m_list_img)
      {

        if (File.Exists(_img_path))
        {
          try
          {
            File.Delete(_img_path);
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }
        }

      }

    } // EndPrint

    #endregion

  }



  public class GhostScriptParameter
  {

    #region Enums

    public enum DEEP_COLOR
    {
      MONO = 0,
      GREY = 1,
      COLOUR = 2
    }
    public enum ORIENTATION
    {
      DEFAULT = -1,
      PORTRAIT = 0,
      LANDSCAPE = 1
    }

    #endregion

    #region Members

    private DEEP_COLOR m_deep_color = DEEP_COLOR.COLOUR;
    private Boolean m_printer_dialog = false;
    private String m_printer = null;
    private Int16 m_from_page = -1;
    private Int16 m_to_page = -1;
    private ORIENTATION m_orientation = ORIENTATION.DEFAULT;
    private Int16 m_copies = 1;
    private String m_file_name = null;
    private String m_output_file = "";
    private PaperSize m_paper_size = null;
    private Int32 m_res = 600;

    #endregion

    #region Properties

    public DEEP_COLOR DeepColor
    {
      get { return DEEP_COLOR.COLOUR; }
      set { m_deep_color = value; }
    }
    public Boolean PrinterDialog
    {
      get { return m_printer_dialog; }
      set { m_printer_dialog = value; }
    }
    public String PrinterName
    {
      get { return m_printer; }
      set { m_printer = value; }
    }
    public Int16 FromPage
    {
      get { return m_from_page; }
      set { m_from_page = value; }
    }
    public Int16 ToPage
    {
      get { return m_to_page; }
      set { m_to_page = value; }
    }
    public ORIENTATION Orientation
    {
      get { return m_orientation; }
      set { m_orientation = value; }
    }
    public Int16 Copies
    {
      get { return m_copies; }
      set { m_copies = value; }
    }
    public String FileName
    {
      get { return m_file_name; }
      set { m_file_name = value; }
    }
    public String OutputFile
    {
      get { return m_output_file; }
      set { m_output_file = value; }
    }
    public PaperSize Paper
    {
      get { return m_paper_size; }
      set { m_paper_size = value; }
    }
    public Int32 Resolution
    {
      get { return m_res; }
      set { m_res = value; }
    }

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Return the parameters for the API
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      String: The parameters for gsprinter.exe
    // 
    //   NOTES:
    // 
    public string[] GetParametersGhostScript()
    {
      ArrayList _args;

      _args = new ArrayList();

      _args.Add("-q");
      _args.Add("-dQUIET");

      _args.Add("-dPARANOIDSAFER"); 								              // Run this command in safe mode
      _args.Add("-dBATCH"); 										                  // Keep gs from going into interactive mode
      _args.Add("-dNOPAUSE"); 									                  // Do not prompt and pause for each page
      _args.Add("-dNOPROMPT"); 									                  // Disable prompts for user interaction
      _args.Add(String.Format("-dMaxBitmap={0}", 500000000)); 		// Set high for better performance
      _args.Add(String.Format("-dNumRenderingThreads={0}", 4)); 	// Multi-core, come-on!

      // Configure the output anti-aliasing, resolution, etc
      _args.Add(String.Format("-dAlignToPixels={0}", 0));
      _args.Add(String.Format("-dGridFitTT={0}", 0));
      _args.Add(String.Format("-dTextAlphaBits={0}", 4));
      _args.Add(String.Format("-dGraphicsAlphaBits={0}", 4));

      switch (m_deep_color)
      {
        case DEEP_COLOR.MONO:
          _args.Add(String.Format("-sDEVICE={0}", "pngmono"));
          break;
        case DEEP_COLOR.GREY:
          _args.Add(String.Format("-sDEVICE={0}", "pnggray"));
          break;
        case DEEP_COLOR.COLOUR:
          _args.Add(String.Format("-sDEVICE={0}", "png16m"));
          break;
        default:
          _args.Add(String.Format("-sDEVICE={0}", "pnggray"));
          break;
      }

      if (String.IsNullOrEmpty(m_printer))
        SetDefaultPrinter();

      if (m_from_page != -1)
        _args.Add(String.Format("-dFirstPage={0}", m_from_page));

      if (m_to_page != -1)
        _args.Add(String.Format("-dLastPage={0}", m_to_page));

      _args.Add(String.Format("-r{0}", 300));

      _args.Add(String.Format("-sOutputFile={0}", m_output_file));

      //_args.Add("margin.ps");

      if (!String.IsNullOrEmpty(m_file_name))
        _args.Add(String.Format("{0}", m_file_name));

      return (string[])_args.ToArray(typeof(string));

    }//GetParametersGhostScript

    //------------------------------------------------------------------------------
    // PURPOSE: Return the parameters for gsprinter.exe
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      String: The parameters for gsprinter.exe
    // 
    //   NOTES:
    // 
    public String GetParametersGSPrint()
    {
      List<String> _args;

      _args = new List<String>();

      switch (m_deep_color)
      {
        case DEEP_COLOR.MONO:
          _args.Add("-mono");
          break;
        case DEEP_COLOR.GREY:
          _args.Add("-grey");
          break;
        case DEEP_COLOR.COLOUR:
          _args.Add("-colour");
          break;
      }

      if (m_printer_dialog)
        _args.Add("-query");

      if (String.IsNullOrEmpty(m_printer))
        SetDefaultPrinter();

      _args.Add(String.Format("-printer \"{0}\"", m_printer));

      if (m_from_page != -1)
        _args.Add(String.Format("-from {0}", m_from_page));

      if (m_to_page != -1)
        _args.Add(String.Format("-to {0}", m_to_page));

      switch (m_orientation)
      {
        case ORIENTATION.PORTRAIT:
          _args.Add("-portrait");
          break;
        case ORIENTATION.LANDSCAPE:
          _args.Add("-landscape");
          break;
      }

      _args.Add(String.Format("-copies {0}", m_copies));

      _args.Add(String.Format("-ghostscript \"{0}\"", "gswin32c.exe"));

      //_args.Add(String.Format("-r{0}", m_res));      
      
      if (m_paper_size != null)
      {
        _args.Add(String.Format("-g{0}x{1}", m_paper_size.Width/100 * m_res, m_paper_size.Height/100 * m_res));
      }      

      if (!String.IsNullOrEmpty(m_file_name))
        _args.Add(String.Format("\"{0}\"", m_file_name));

      return String.Join(" ", _args.ToArray());

    }//GetParametersGSPrint

    //------------------------------------------------------------------------------
    // PURPOSE: Constructor. Initialize properties.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public GhostScriptParameter(String FileName)
    {
      m_file_name = FileName;
      SetDefaultPrinter();
    }//GSPrintParameter

    //------------------------------------------------------------------------------
    // PURPOSE: Constructor. Initialize properties.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public GhostScriptParameter(String FileName, Int16 Copies)
    {
      m_file_name = FileName;
      m_copies = Copies;
      SetDefaultPrinter();
    }//GSPrintParameter

    //------------------------------------------------------------------------------
    // PURPOSE: Constructor. Initialize properties.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public GhostScriptParameter(String FileName, Int16 Copies, String Printer)
    {
      m_file_name = FileName;
      m_copies = Copies;
      m_printer = Printer;
    }//GSPrintParameter

    //------------------------------------------------------------------------------
    // PURPOSE: Constructor. Initialize properties.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public GhostScriptParameter(String FileName, Int16 Copies, PaperSize Paper ,String Printer)
    {
      m_file_name = FileName;
      m_copies = Copies;
      m_printer = Printer;
      m_paper_size = Paper;
    }//GSPrintParameter

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Set the default printer of SO
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    private void SetDefaultPrinter()
    {

      PrintDocument _print_document;
      _print_document = new PrintDocument();
      m_printer = _print_document.PrinterSettings.PrinterName;
      _print_document.Dispose();

    }//SetDefaultPrinter

    #endregion
  }

  public class GSPrint
  {

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Check if exists the library "gsdll32.dll" and "gsprint.exe"
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Boolean: true if check is correct
    // 
    //   NOTES:
    //   
    static public Boolean ApplicationAvailable()
    {
      String _path_app;

      _path_app = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
      if (!File.Exists(Path.Combine(_path_app, "gsprint.exe")))
      {
        return false;
      }

      if (!File.Exists(Path.Combine(_path_app, "gsdll32.dll")))
      {
        return false;
      }

      return true;
    } //ApplicationAvailable

    //------------------------------------------------------------------------------
    // PURPOSE: Execute the "gsprint.exe" with the specified parameters
    //
    //  PARAMS:
    //      - INPUT: Params: The parameteres aviables.
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Boolean: true if is printed
    // 
    //   NOTES:
    //   
    static public Boolean Print(GhostScriptParameter Params)
    {

      ProcessStartInfo _start_info;
      Process _process;

      _process = new Process();
      _start_info = new ProcessStartInfo();

      _start_info.CreateNoWindow = true;
      _start_info.WindowStyle = ProcessWindowStyle.Hidden;
      _start_info.Arguments = Params.GetParametersGSPrint();
      _start_info.FileName = "gsprint.exe";
      _start_info.UseShellExecute = false;

      _process.StartInfo = _start_info;

      try
      {

        _process.Start();

        try
        {
          _process.WaitForExit(10000);
        }
        catch
        { }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }


    } //Print

    //------------------------------------------------------------------------------
    // PURPOSE : Print the witholding documents
    // 
    //  PARAMS:
    //      - INPUT :
    //          - OperationId: 
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:

    public static Boolean PrintDialog(String Path)
    {
      DialogResult _dialog;
      PrinterSettings _ps_selected;
      PrinterSettings _ps_default;
      GhostScriptParameter _print_params;

      _ps_default = new PrinterSettings();
      _ps_default.Copies = 1;
       
      _dialog = SelectPrinterDlg(_ps_default, out _ps_selected);

      if (_dialog == DialogResult.OK)
      {
        _print_params = new GhostScriptParameter(Path, _ps_selected.Copies, _ps_selected.PrinterName);
        
        return GSPrint.Print(_print_params); 
      }

      return true;
    } // PrintDialogWitholdingDocuments

    //------------------------------------------------------------------------------
    // PURPOSE : Printer dialog
    // 
    //  PARAMS:
    //      - INPUT :
    //          - Card : Card Data
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:

    private static DialogResult SelectPrinterDlg(PrinterSettings DefaultPrinterSetting, out PrinterSettings PrinterSettingsResults)
    {
      PrintDialog _print_dialog;
      DialogResult _dialog_result;

      _print_dialog = new PrintDialog();

      _print_dialog.PrinterSettings = DefaultPrinterSetting;
      _print_dialog.UseEXDialog = true;
      _print_dialog.PrintToFile = false;
      _print_dialog.AllowPrintToFile = false;
      _print_dialog.AllowSomePages = false;
      _dialog_result = _print_dialog.ShowDialog();
      PrinterSettingsResults = _print_dialog.PrinterSettings;

      // RCI & ICS 16-APR-2014 Free the object
      _print_dialog.Dispose();

      return _dialog_result;

    } // SelectPrinterDlg

    #endregion

  }

}

