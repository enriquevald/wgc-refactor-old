//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PatternList.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 08-MAY-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ------------------------------------------------
// 08-MAY-2014 JMM    Initial draft.
// 04-JUN-2014 JPJ    Fixed Bug WIG-1004: Terminal restriction was not included
// 05-JUN-2014 JPJ    Fixed Bug WIG-1004: Terminal restriction was not included (It was reopened due to assigning groups without terminals)
// 06-JUN-2014 JPJ    Fixed Bug WIG-1016: Some patterns don't show up when pattern is consecutive or no consecutive with order
// 10-JUN-2014 JPJ    Fixed Bug WIG-1026: The scheduled time when the pattern is active
// 31-JUL-2014 JPJ    Fixed Bug WIG-1135: Some consecutive patterns where not deleted
// 12-SEP-2014 JPJ    Mailing alarm functionality
// 22-SEP-2014 JPJ    Fixed Bug WIG-1252: Null values in elements
// 25-NOV-2014 JPJ    Fixed Bug WIG-1733: First pattern runs through all the alarm table
// 27-JAN-2015 JPJ    Fixed Bug WIG-1733: First pattern runs through all the alarm table
// 11-JUN-2015 JPJ    Added new information in the Mailing body when type is terminal
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using System.Threading;

namespace WSI.Common
{
  public class PatternList
  {
    public enum PatternListType
    {
      Unknown = 0,
      Alarms = 1,
    }

    PatternListType m_patternlist_type;

    DataTable m_elements = null;

    DataSet m_ds = null;

    #region Constants

    public const String COLUMN_ID = "Id";

    #endregion

    # region Constructors

    //------------------------------------------------------------------------------
    // PURPOSE : Constructor for a PatternList instance
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public PatternList()
    {
      Init();
    } // PatternList

    #endregion // Constructors

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Get the type of the PatternList.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - PatternListType Type
    // 
    public PatternListType ListType
    {
      get
      {
        return m_patternlist_type;
      }
      set
      {
        m_patternlist_type = value;
        CleanList();
      }
    } // ListType

    public DataTable Elements
    {
      get
      {
        return m_elements;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get the modifications of the type.
    // 
    //  PARAMS:
    //      - INPUT:
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    public String AuditString()
    {
      StringBuilder _list;

      _list = new StringBuilder();

      try
      {
        foreach (DataRow _dr in m_elements.Rows)
        {
          if (_list.Length > 0)
          {
            _list.Append(", ");
          }

          _list.Append(String.Format("{0}", _dr[0]).ToUpper());
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _list.ToString();

    } // AuditString

    //------------------------------------------------------------------------------
    // PURPOSE: Add a new element.
    // 
    //  PARAMS:
    //      - INPUT:    
    //          - Int32 Id
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void AddElement(Int32 ElementId, PatternListType Type)
    {
      DataRow _dr;
      DataTable _dt;

      switch (Type)
      {
        case PatternListType.Alarms:
          _dt = m_elements;
          break;
        default:
          _dt = null;
          break;
      }

      if (_dt != null)
      {
        _dr = _dt.NewRow();
        _dr[0] = ElementId;
        _dt.Rows.Add(_dr);
      }
    } // AddElement

    //------------------------------------------------------------------------------
    // PURPOSE: Remove a element.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Terminal
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void RemoveElement(Int32 Element, PatternListType Type)
    {
      DataRow _dr;

      _dr = SeekByType(Element, Type);

      if (_dr != null)
      {
        _dr.Delete();

        switch (Type)
        {
          case PatternListType.Alarms:
            m_elements.AcceptChanges();
            break;
          default:
            break;
        }
      }
    } // RemoveElement

    //------------------------------------------------------------------------------
    // PURPOSE: Remove all elements.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Terminal
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void CleanList()
    {
      m_elements.Clear();

    } // CleanList

    //------------------------------------------------------------------------------
    // PURPOSE: Indicates if a Element is in the TerminalList.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Element
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True: The Element is in the list. False: Otherwise.
    // 
    public Boolean Contains(Int32 ElementId, PatternListType Type)
    {
      return (SeekByType(ElementId, Type) != null);

    } // Contains

    //------------------------------------------------------------------------------
    // PURPOSE: Indicates if exists an element of type Type.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - GROUP_ELEMENT_TYPE Type
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True: The element is in the corresponding list. False: Otherwise.
    // 
    public Boolean ExistsElements(PatternListType Type)
    {
      switch (Type)
      {
        case PatternListType.Alarms:
          return m_elements.Rows.Count > 0;
        default:
          return false;
      }
    } // ExistsElements

    //------------------------------------------------------------------------------
    // PURPOSE: Get the list of elements of type Type.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Element
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - True: The Element is in the list. False: Otherwise.
    // 
    public DataTable GetElementsList()
    {

      return m_elements;
    } // Contains

    //------------------------------------------------------------------------------
    // PURPOSE: Get a XML representation of the element list.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - String
    // 
    public String ToXml()
    {
      return DataSetToXml(m_ds, XmlWriteMode.IgnoreSchema);
    } // ToXml

    //------------------------------------------------------------------------------
    // PURPOSE: Read a XML representation of the element list and store it into the DataSet.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public void FromXml(String Xml)
    {
      if (String.IsNullOrEmpty(Xml))
      {
        //TODO JMM 09-MAY-2014: XML Empty.  No elements on the pattern!
        return;
      }

      CleanList();
      DataSetFromXml(m_ds, Xml, XmlReadMode.IgnoreSchema);

    } // FromXml

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Init the properties of the class.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private void Init()
    {
      m_ds = new DataSet("Pattern");

      m_elements = new DataTable("Element");
      m_elements.Columns.Add(COLUMN_ID, Type.GetType("System.Int64"));

      m_ds.Tables.Add(m_elements);
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE: Search for the element in the type list.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - String Key
    //          - GROUP_ELEMENT_TYPE Type
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - DataRow: If found return the DataRow.
    // 
    private DataRow SeekByType(Int32 ElementId, PatternListType Type)
    {
      DataTable _search;
      Object[] _search_key;
      DataRow _dr_result;

      switch (Type)
      {
        case PatternListType.Alarms:
          _search = m_elements;
          break;
        default:
          return null;
      }

      _search_key = new Object[] { ElementId };
      _dr_result = _search.Rows.Find(_search_key);

      return _dr_result;
    } // SeekByType

    //------------------------------------------------------------------------------
    // PURPOSE : DataSet to Xml
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet DataSet
    //          - XmlWriteMode WriteMode
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    public String DataSetToXml(DataSet DataSet, XmlWriteMode WriteMode)
    {
      String _xml;

      _xml = "";
      using (StringWriter _sw = new StringWriter())
      {
        DataSet.WriteXml(_sw, WriteMode);
        _xml = _sw.ToString();
      }

      return _xml;
    } // DataSetToXml

    #endregion // Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE : DataSet to Xml
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataSet DataSet
    //          - XmlWriteMode WriteMode
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - String
    //
    private void DataSetFromXml(DataSet DataSet, String Xml, XmlReadMode ReadMode)
    {
      using (StringReader _sr = new StringReader(Xml))
      {
        DataSet.ReadXml(_sr, ReadMode);
      }
    } // DataSetFromXml
  } // class PatternList

  [Serializable]
  public class PendingPattern
  {
    private const String DATASET_NAME = "Alarms";
    private const String DATATABLE_NAME = "Alarm";
    private const String DATATABLE_COLUMN_ID = "Id";

    public String Key;                                   // Indentifier inside the dictionary
    public Int64 pattern_id;                             // Indicates which pattern is analyzed
    public Int64 alarm_id;                               // Indicates id of the alarm table
    public Int64 element_identifier;                     // The Terminal is being analyzed
    public String element_identifier_name;               // The name of the Terminal is being analyzed
    //PATTERN_TYPE 
    public Int32 pattern_type;                           // How the pattern is processed
    public Int32 alarm_code;                             // Identificaction code of the alarm to generate when pattern is found
    public String alarm_name;                            // Name of the alarm to generate when pattern is found
    public String alarm_description;                     // Descripction of the found pattern
    //AlarmSeverity
    public Int32 alarm_severity;                         // severity of the alarm
    public DateTime creation_time;                       // Current time when the first element of the pattern was created 
    public DateTime first_element_time;                  // Time when first element of the pattern was detected (not current time)
    public DateTime last_element_time;                   // Time time when the last element of the pattern was detected (not current time) 
    [System.Xml.Serialization.XmlArray(ElementName = "Pattern_Elements")]
    [System.Xml.Serialization.XmlArrayItem(typeof(Int64), ElementName = "Element")]
    public Int64[] pattern_elements;                     // Pattern template
    [System.Xml.Serialization.XmlArray(ElementName = "Pattern_Values")]
    [System.Xml.Serialization.XmlArrayItem(typeof(Int64), ElementName = "Value")]
    public Int64[] element_values;                       // Values of the elements in the template 
    // PATTERN_STATE
    public Int32 State;                                  // State of the pending pattern 0: Created, 1:Closed, 2:Delete 

    // Ignored elements
    [System.Xml.Serialization.XmlIgnore]
    public Int64 temp_alarm_id;                          // Temp alarm_id value until is inserted
    public Boolean alarm_mailing_enabled;                // Indicates if mailing is enabled (alarm should be sent?)
    public String alarm_mailing_name;                    // Alarm mailing name
    public String alarm_mailing_address;                 // Mailing addresses
    public String alarm_mailing_subject;                 // Mailing subject 
    public String alarm_mailing_body;                    // Information regarding the mail body 

    //Funcions
    // Creates a key definition for the dictionary element
    public String CreateKey(Int64 PatternId, Int64 Indentifier, Int64 FirstValue)
    {
      Key = PatternId.ToString() + "-" + Indentifier.ToString() + "-" + FirstValue;

      return Key;
    }

    // Generates the array of element values into a XML
    public String ElementValuesToXML()
    {
      DataSet _ds;
      DataTable _dt;
      String _xml;

      try
      {
        using (_ds = new DataSet(DATASET_NAME))
        {
          _dt = new DataTable(DATATABLE_NAME);
          _dt.Columns.Add(DATATABLE_COLUMN_ID, Type.GetType("System.Int64"));
          _ds.Tables.Add(_dt);
          _xml = "";

          foreach (Int64 _value in element_values)
          {
            DataRow _row = _dt.NewRow();
            _row[DATATABLE_COLUMN_ID] = _value;
            _dt.Rows.Add(_row);
          }

          using (System.IO.StringWriter _xmlelement = new System.IO.StringWriter())
          {
            _ds.WriteXml(_xmlelement, XmlWriteMode.IgnoreSchema);
            _xml = _xmlelement.ToString();
          }
        }

        return _xml;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return String.Empty;
    }
  }  // class PendingPattern

  [Serializable]
  public class PatternTemplate
  {
    public Int64 pattern_id;                             // Indicates which pattern is
    public String pattern_name;                          // Indicates the pattern name
    public Int64[] pattern_elements;                     // Pattern template
    public Int32 alarm_code;                             // Identificaction code of the alarm to generate when pattern is found
    public String alarm_name;                            // Name of the alarm to generate when pattern is found
    public String alarm_description;                     // Descripction of the found pattern
    public AlarmSeverity alarm_severity;                 // severity of the alarm
    public Patterns.PATTERN_TYPE pattern_type;           // How the pattern is processed
    public Int32 life_time;                              // Time that the pattern will active 
    public DataTable terminal_list;                      // Pattern can process only these Terminals
    public Int32 time_from;                              // Pattern start time
    public Int32 time_to;                                // Pattern end time
    public Int32 terminal_list_type;                     // Type of the terminal list
    public Boolean mailing_enabled;                      // Mailing functionality enabled
    public String mailing_address;                       // List of address to send the mail
    public String mailing_subject;                       // Information to be included in the subject of the mail

    //------------------------------------------------------------------------------
    // PURPOSE : Transforms a given xml into an array
    //
    //  PARAMS :
    //      - INPUT:
    //            - String
    //
    // RETURNS :
    //      - Int64[]
    //
    public Int64[] toArray(String Xml)
    {
      DataSet _ds;
      Int64[] _my_array;
      Int32 _idx_array;

      try
      {
        _my_array = null;
        _idx_array = 0;
        _ds = new DataSet();

        using (System.IO.StringReader _sr = new System.IO.StringReader(Xml))
        {
          _ds.ReadXml(_sr, XmlReadMode.Auto);
          if (_ds.Tables.Count == 0)
          {

            return null;
          }
          _my_array = new Int64[_ds.Tables[0].Rows.Count];
          foreach (DataRow _row in _ds.Tables[0].Rows)
          {
            _my_array[_idx_array] = Convert.ToInt64(_row[0]);

            _idx_array++;
          }
        }

        return _my_array;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return null;
    } // toArray

    public PatternTemplate()
    {
      terminal_list = new DataTable();
    } // PatternTemplate

  } // class PatternTemplate

  public struct AlarmInformation
  {
    public Int64 al_source_id;
    public String al_source_name;
    public String al_alarm_description;
    public Int32 al_source_code;
  } // struct ALARM_INFORMATION

  public class Patterns
  {

    #region Enums

    public enum PATTERN_TYPE
    {
      UNKNOWN = 0,
      CONSECUTIVE = 1,
      NO_CONSECUTIVE_WITH_ORDER = 2,
      NO_ORDER = 3,
      NO_ORDER_WITH_REPETITION = 4,
    }

    public enum PATTERN_STATE
    {
      CREATED = 0,
      CLOSED = 1,
      DELETE = 2,
      PENDING = 3
    }

    #endregion

    #region Members

    private const Int32 NUMBER_OF_ALARMS_TO_PROCESS = 500;
    private const Int32 NUMBER_OF_ALARMS_TO_PROCESS_MIN = 1;
    private const Int32 NUMBER_OF_ALARMS_TO_PROCESS_MAX = 10000;

    public const Int32 PATTERN_LIFE_TIME = 5;                              // 5 min.
    public const Int32 PATTERN_LIFE_TIME_MIN = 1;                          // 1 min.
    public const Int32 PATTERN_LIFE_TIME_MAX = 60;                         // 1 hour

    private const Int32 PATTERN_WAIT_SECONDS = 20;                         // 20 seg.
    private const Int32 PATTERN_WAIT_SECONDS_MIN = 10;                     // 10 seg. 
    private const Int32 PATTERN_WAIT_SECONDS_MAX = 30;                     // 30 seg.

    // ALARM COLUMNS
    private const String ALARM_COLUMN_ID = "ALARM_ID";
    private const String ALARM_COLUMN_IDENTIFIER = "ELEMENT_IDENTIFIER";
    private const String ALARM_COLUMN_IDENTIFIER_NAME = "ELEMENT_IDENTIFIER_NAME";
    private const String ALARM_COLUMN_ALARM_CODE = "ALARM_CODE";
    private const String ALARM_COLUMN_ALARM_NAME = "ALARM_NAME";
    private const String ALARM_COLUMN_ALARM_CREATION_TIME = "ALARM_REPORTED";
    private const String ALARM_COLUMN_ALARM_DETECTION_TIME = "ALARM_DATETIME";

    // PATTERN COLUMNS
    private const String PATTERN_COLUMN_ID = "PATTERN_ID";
    private const String PATTERN_COLUMN_NAME = "PATTERN_NAME";
    private const String PATTERN_COLUMN_ELEMENTS = "PATTERN_ELEMENTS";
    private const String PATTERN_COLUMN_ALARM_CODE = "ALARM_CODE";
    private const String PATTERN_COLUMN_ALARM_NAME = "ALARM_NAME";
    private const String PATTERN_COLUMN_ALARM_DESCRIPTION = "ALARM_DESCRIPTION";
    private const String PATTERN_COLUMN_ALARM_SEVERITY = "ALARM_SEVERITY";
    private const String PATTERN_COLUMN_ALARM_TYPE = "ALARM_TYPE";
    private const String PATTERN_COLUMN_LIFE_TIME = "PATTERN_LIFE_TIME";
    private const String PATTERN_COLUMN_TIME_FROM = "PATTERN_COLUMN_TIME_FROM";
    private const String PATTERN_COLUMN_TIME_TO = "PATTERN_COLUMN_TIME_TO";
    private const String PATTERN_COLUMN_TERMINALS = "PATTERN_COLUMN_TERMINALS";
    private const String PATTERN_COLUMN_MAILING_ACTIVE = "PATTERN_COLUMN_MAILING_ACTIVE";
    private const String PATTERN_COLUMN_MAILING_ADDRESS = "PATTERN_COLUMN_MAILING_ADDRESS";
    private const String PATTERN_COLUMN_MAILING_SUBJECT = "PATTERN_COLUMN_MAILING_SUBJECT";

    // TERMINAL_GROUP COLUMNS
    public const String TERMINAL_COLUMN_PATTERN_ID = "PATTERN_ID";
    public const String TERMINAL_COLUMN_TERMINAL_ID = "TERMINAL_ID";

    private const int START_RANGE_GAME_ALARMS = 135169;
    private const int END_RANGE_GAME_ALARMS = 139263;

    //Mailing
    private const String MAILING_NAME = "Alarm mailing pattern";
    //MAILING COLUMNS
    private const String MAILING_COLUMN_UNIQUE = "AM_UNIQUE_ID";
    private const String MAILING_COLUMN_PROG_NAME = "AM_PROG_NAME";
    private const String MAILING_COLUMN_ADDRESS_LIST = "AM_ADDRESS_LIST";
    private const String MAILING_COLUMN_SUBJECT = "AM_SUBJECT";
    private const String MAILING_COLUMN_BODY = "AM_BODY";
    private const String MAILING_COLUMN_SCHEDULED = "AM_SCHEDULED";
    private const String MAILING_COLUMN_ALARM_ID = "AM_ALARM_ID";

    private static Thread m_thread;

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE : Starts a new thread to search for patterns
    //
    //  PARAMS :
    //      - INPUT:
    //            - SqlTrx
    //
    // RETURNS :
    //      - Boolean
    //
    public static void Init()
    {
      m_thread = new Thread(PatternSearchThread);
      m_thread.Name = "PatternSearchThread";

      m_thread.Start();
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Search for patterns
    //
    //  PARAMS :
    //      - INPUT:
    //
    // RETURNS :
    //      - Boolean
    //
    private static void PatternSearchThread()
    {
      DataTable _dtalarms;
      Int64 _block_id;
      Int32 _wait_time;
      Dictionary<String, PendingPattern> _pattern_list;      // Contains the list of open patterns and also the patterns that should be registered
      Dictionary<Int64, PatternTemplate> _pattern_template;  // Contains the list of patterns templates  
      Stack<String> _ordered_list;
      DataTable _dtmailing_alarms;

      _wait_time = 1000 * PATTERN_WAIT_SECONDS;

      while (true)
      {
        Thread.Sleep(_wait_time);

        // Set default wait time
        _wait_time = 1000 * GeneralParam.GetInt32("Pattern", "Thread.WaitSeconds",
                                                  PATTERN_WAIT_SECONDS,
                                                  PATTERN_WAIT_SECONDS_MIN,
                                                  PATTERN_WAIT_SECONDS_MAX);

        if (!Services.IsPrincipal("WCP"))
        {

          continue;
        }

        if (!GeneralParam.GetBoolean("Pattern", "Enabled", true))
        { // If the patterns functionality is not enabled we don't continue

          continue;
        }

        if (!GeneralParam.GetBoolean("Pattern", "Thread.Enabled", true))
        { // If the patterns search thread is not enabled we don't continue

          continue;
        }

        try
        {
          //Initializing
          _block_id = 0;
          _pattern_list = new Dictionary<String, PendingPattern>();
          _pattern_template = new Dictionary<Int64, PatternTemplate>();

          using (DB_TRX _db_trx = new DB_TRX())
          {
            //Get the available pattern and definition
            if (!GetPatterns(_db_trx.SqlTransaction, ref _pattern_template))
            {

              // If there are no patterns to analyze then we can exit
              continue;
            }

            //Get pending patterns
            if (!GetPendingPatterns(_db_trx.SqlTransaction, out _block_id, ref _pattern_list))
            {

              // If there is an error getting the data
              continue;
            }

            // If it's the first time patterns are active
            if (_block_id == 0)
            {
              if (!GetLastAlarmID(_db_trx.SqlTransaction, out _block_id))
              {
                // If there is an error getting the data
                continue;
              }
              List<PendingPattern> _dummy_pending_patterns;

              _dummy_pending_patterns = new List<PendingPattern>();

              // Save pending patterns
              SavePatternInformation(_block_id, _dummy_pending_patterns, _db_trx.SqlTransaction);
              _db_trx.Commit();
            }

            // Get next alarm block to process
            if (!GetAlarmBlock(_block_id, _db_trx.SqlTransaction, out _dtalarms))
            {

              // There is no more information to process
              continue;
            }
          } // Using DB_TRX

          _wait_time = 0;

          // Patterns lifetime process
          LifeTimePendingPatterns(_pattern_template, ref _pattern_list);

          // Search for patterns in the block
          _ordered_list = new Stack<String>();
          PatternLookUp(_dtalarms, _pattern_template, ref _pattern_list, ref _ordered_list);

          // Getting the last element of the processed block
          _block_id = (Int64)_dtalarms.Compute("MAX(" + ALARM_COLUMN_ID + ")", String.Empty);
          using (DB_TRX _db_trx = new DB_TRX())
          {
            // Save found patterns and pending patterns
            _dtmailing_alarms = GetMailDataTable();
            SavePatterns(_block_id, ref _pattern_list, _ordered_list, _db_trx.SqlTransaction, ref _dtmailing_alarms);

            if (GeneralParam.GetBoolean("Pattern", "MailingAlarm.Enabled", true))
            { // Processes the alarms that have to be sent by mail
              AlarmsToMail(_dtmailing_alarms, _db_trx.SqlTransaction);
            }
            _db_trx.Commit();
          } // Using DB_TRX


          // Clears the list of elements
          _pattern_list.Clear();
          _pattern_template.Clear();
          _ordered_list.Clear();
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        } // try
      } // while

    } // PatternSearchThread

    //------------------------------------------------------------------------------
    // PURPOSE : Erase all pending patterns whose life time have expired
    //
    //  PARAMS :
    //      - INPUT:
    //            - Dictionary<Int64, PatternTemplate>
    //            - Dictionary<String, PendingPattern>
    //
    //      - OUTPUT:
    //            - Dictionary<String, PendingPattern>
    //
    // RETURNS :
    //
    private static void LifeTimePendingPatterns(Dictionary<Int64, PatternTemplate> PatternTemplate,
                                                ref Dictionary<String, PendingPattern> PatternList)
    {
      List<String> _list_delete_pendings;

      _list_delete_pendings = new List<String>();

      try
      {
        foreach (PendingPattern _pattern_element in PatternList.Values)
        {
          // We erase the deleted patterns
          if ((PATTERN_STATE)_pattern_element.State == PATTERN_STATE.DELETE)
          {
            _list_delete_pendings.Add(_pattern_element.Key);

            continue;
          }

          // If the pattern isn't in the list pending pattern must be erased it means patterns is no longer active
          if (!PatternTemplate.ContainsKey(_pattern_element.pattern_id))
          {
            _list_delete_pendings.Add(_pattern_element.Key);

            continue;
          }
        }

        // Remove patterns marked for deletion
        foreach (String _key in _list_delete_pendings)
        {
          PatternList.Remove(_key);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

    } // LifeTimePendingPatterns

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the patterns to process
    //
    //  PARAMS :
    //      - INPUT:
    //            - SqlTrx
    //            - Dictionary<Int64, PatternTemplate>
    //
    //      - OUTPUT :
    //            - Dictionary<Int64, PatternTemplate>
    //
    // RETURNS :
    //      - Boolean
    //
    private static Boolean GetPatterns(SqlTransaction SqlTrx,
                                       ref Dictionary<Int64, PatternTemplate> PatternTemplate)
    {
      StringBuilder _sb;
      PatternTemplate _pattern;
      String _select;
      DataTable _dt;
      Int32 _life_time;
      TerminalList _terminals;

      try
      {
        _sb = new StringBuilder();
        _dt = new DataTable();

        // We get the terminals of the actual active patterns
        GetPatternTerminals(SqlTrx, out _dt);

        _life_time = GeneralParam.GetInt32("Pattern", "Pattern.LifeTime",
                                           PATTERN_LIFE_TIME,
                                           PATTERN_LIFE_TIME_MIN,
                                           PATTERN_LIFE_TIME_MAX);

        _sb.AppendLine(" SELECT    PT_ID AS " + PATTERN_COLUMN_ID);
        _sb.AppendLine("         , PT_NAME AS " + PATTERN_COLUMN_NAME);
        _sb.AppendLine("         , PT_PATTERN AS " + PATTERN_COLUMN_ELEMENTS);
        _sb.AppendLine("         , PT_AL_CODE AS " + PATTERN_COLUMN_ALARM_CODE);
        _sb.AppendLine("         , PT_AL_NAME AS " + PATTERN_COLUMN_ALARM_NAME);
        _sb.AppendLine("         , PT_AL_DESCRIPTION AS " + PATTERN_COLUMN_ALARM_DESCRIPTION);
        _sb.AppendLine("         , PT_AL_SEVERITY AS " + PATTERN_COLUMN_ALARM_SEVERITY);
        _sb.AppendLine("         , PT_TYPE AS " + PATTERN_COLUMN_ALARM_TYPE);
        _sb.AppendLine("         , PT_LIFE_TIME AS " + PATTERN_COLUMN_LIFE_TIME);
        _sb.AppendLine("         , PT_SCHEDULE_TIME_FROM AS " + PATTERN_COLUMN_TIME_FROM);
        _sb.AppendLine("         , PT_SCHEDULE_TIME_TO AS " + PATTERN_COLUMN_TIME_TO);
        _sb.AppendLine("         , PT_RESTRICTED_TO_TERMINAL_LIST AS " + PATTERN_COLUMN_TERMINALS);
        _sb.AppendLine("         , PT_ACTIVE_MAILING AS " + PATTERN_COLUMN_MAILING_ACTIVE);
        _sb.AppendLine("         , PT_ADDRESS_LIST AS " + PATTERN_COLUMN_MAILING_ADDRESS);
        _sb.AppendLine("         , PT_SUBJECT AS " + PATTERN_COLUMN_MAILING_SUBJECT);
        _sb.AppendLine("   FROM  PATTERNS");
        _sb.AppendLine("  WHERE  PT_ACTIVE = 1");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            // If there are no patterns to analyze then we can exit
            if (!_reader.HasRows)
            {

              return false;
            }

            while (_reader.Read())
            {
              _pattern = new PatternTemplate();
              _pattern.pattern_id = _reader.GetInt64(0);
              _pattern.pattern_name = _reader.GetString(1);
              if (!_reader.IsDBNull(2))
              {
                _pattern.pattern_elements = _pattern.toArray(_reader.GetString(2));
              }

              if (_pattern.pattern_elements == null || _pattern.pattern_elements.Length == 0)
              {

                continue;
              }
              _pattern.alarm_code = _reader.GetInt32(3);
              _pattern.alarm_name = _reader.GetString(4);
              _pattern.alarm_description = _reader.GetString(5);
              _pattern.alarm_severity = (AlarmSeverity)_reader.GetInt32(6);
              _pattern.pattern_type = (PATTERN_TYPE)_reader.GetInt32(7);
              if (!_reader.IsDBNull(8))
              {
                _pattern.life_time = (Int32)_reader.GetInt32(8);
              }
              else
              {
                _pattern.life_time = _life_time;
              }

              if (_pattern.life_time > _life_time)
              {
                _pattern.life_time = _life_time;
              }
              _pattern.time_from = 0;
              if (!_reader.IsDBNull(9))
              {
                _pattern.time_from = _reader.GetInt32(9);
              }
              _pattern.time_to = 0;
              if (!_reader.IsDBNull(10))
              {
                _pattern.time_to = _reader.GetInt32(10);
              }
              // 05-JUN-2014 JPJ    Fixed Bug WIG-1004: Terminal restriction was not included (It was reopened due to assigning groups without terminals)
              _pattern.terminal_list_type = 0; // All terminals
              if (!_reader.IsDBNull(11))
              {
                _terminals = new TerminalList();
                _terminals.FromXml(_reader.GetString(11));
                _pattern.terminal_list_type = (Int32)_terminals.ListType;
              }

              _pattern.mailing_enabled = false;
              _pattern.mailing_address = String.Empty;
              _pattern.mailing_subject = String.Empty;
              // 22-SEP-2014 JPJ Fixed Bug WIG-1252: Null values in elements
              _pattern.mailing_enabled = _reader.GetBoolean(12);
              if (!_reader.IsDBNull(13))
              {
                _pattern.mailing_address = _reader.GetString(13);
              }
              if (!_reader.IsDBNull(14))
              {
                _pattern.mailing_subject = _reader.GetString(14);
              }

              // Terminals are associated with the pattern
              // Filter and Sort expressions
              _select = TERMINAL_COLUMN_PATTERN_ID + " = " + _pattern.pattern_id.ToString();
              // Create a DataView using the table as its source and the filter and sort expressions and convert the DataView to a DataTable
              // 04-JUN-2014 JPJ    Fixed Bug WIG-1004: Terminal restriction was not included
              _pattern.terminal_list = new DataView(_dt, _select, TERMINAL_COLUMN_TERMINAL_ID + " ASC", DataViewRowState.CurrentRows).ToTable();

              // Pattern is added to the list
              PatternTemplate.Add(_pattern.pattern_id, _pattern);
            }

            return true;
          }
        }
      }  // Using SqlCommand
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetPatterns

    //------------------------------------------------------------------------------
    // PURPOSE : Returns a portion of the data of the Alarms table
    //
    //  PARAMS :
    //      - INPUT:
    //            - SqlTransaction
    //            - Int64
    //            - Dictionary<String, PendingPattern>
    //
    //      - OUTPUT :
    //            - Int64
    //            - Dictionary<String, PendingPattern>
    //
    // RETURNS :
    //      - Boolean
    //
    private static Boolean GetPendingPatterns(SqlTransaction SqlTrx,
                                              out Int64 InitialAlarm,
                                              ref Dictionary<String, PendingPattern> PendingPatterns)
    {
      StringBuilder _sb;

      InitialAlarm = 0;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("   SELECT   TOP  1");
        _sb.AppendLine("            PTI_SEQUENCE, PTI_OPEN_PATTERNS");
        _sb.AppendLine("     FROM   PATTERNS_INFORMATION");
        _sb.AppendLine(" ORDER BY   PTI_ID DESC");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {

              return true;
            }

            InitialAlarm = _reader.GetInt64(0);
            if (!_reader.IsDBNull(1))
            {
              List<PendingPattern> _list = PatternsListFromXML(_reader.GetString(1));
              PendingPatterns = PatternsListToDictionary(_list);
            }

            return true;
          }
        }
      }  // Using SqlCommand
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetPendingPatterns

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the last alarm id of the Alarms table
    //
    //  PARAMS :
    //      - INPUT:
    //            - SqlTransaction
    //            - Int64
    //
    //      - OUTPUT :
    //            - Int64
    //
    // RETURNS :
    //      - Boolean
    //
    private static Boolean GetLastAlarmID(SqlTransaction SqlTrx,
                                          out Int64 InitialAlarm)
    {
      StringBuilder _sb;

      InitialAlarm = 0;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("   SELECT   TOP  1");
        _sb.AppendLine("            AL_ALARM_ID");
        _sb.AppendLine("     FROM   ALARMS");
        _sb.AppendLine(" ORDER BY   AL_ALARM_ID DESC");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {

              return true;
            }

            InitialAlarm = _reader.GetInt64(0);

            return true;
          }
        }
      }  // Using SqlCommand
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetLastAlarmID

    //------------------------------------------------------------------------------
    // PURPOSE : Returns a portion of the data of the Alarms table
    //
    //  PARAMS :
    //      - INPUT:
    //            - SqlTrx
    //
    //      - OUTPUT :
    //            - DataTable
    //
    // RETURNS :
    //      - Boolean
    //
    private static Boolean GetAlarmBlock(Int64 InitialAlarm, SqlTransaction SqlTrx, out DataTable dtAlarms)
    {
      StringBuilder _sb;
      Int32 _number_of_alarms_to_process;

      dtAlarms = new DataTable();

      try
      {
        // Number of alarms to process can't be negative or 0
        _number_of_alarms_to_process = GeneralParam.GetInt32("Pattern", "Thread.NumberAlarmsToProcess",
                                                             NUMBER_OF_ALARMS_TO_PROCESS,
                                                             NUMBER_OF_ALARMS_TO_PROCESS_MIN,
                                                             NUMBER_OF_ALARMS_TO_PROCESS_MAX);

        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   TOP  " + _number_of_alarms_to_process);
        _sb.AppendLine("            AL_ALARM_ID AS " + ALARM_COLUMN_ID);
        _sb.AppendLine("          , AL_SOURCE_ID AS " + ALARM_COLUMN_IDENTIFIER);
        _sb.AppendLine("          , AL_SOURCE_NAME AS " + ALARM_COLUMN_IDENTIFIER_NAME);
        _sb.AppendLine("          , AL_ALARM_CODE AS " + ALARM_COLUMN_ALARM_CODE);
        _sb.AppendLine("          , AL_ALARM_NAME AS " + ALARM_COLUMN_ALARM_NAME);
        _sb.AppendLine("          , AL_REPORTED AS " + ALARM_COLUMN_ALARM_CREATION_TIME);
        _sb.AppendLine("          , AL_DATETIME AS " + ALARM_COLUMN_ALARM_DETECTION_TIME);
        _sb.AppendLine("   FROM   ALARMS");
        _sb.AppendLine("  WHERE   AL_ALARM_ID > @pInitialAlarm");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pInitialAlarm", SqlDbType.BigInt).Value = InitialAlarm;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {

            _sql_da.Fill(dtAlarms);

            if (dtAlarms.Rows.Count == 0)
            {

              return false;
            }

            return true;
          }
        }
      }  // Using SqlCommand
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetAlarmBlock

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the terminals associated with the pattern
    //
    //  PARAMS :
    //      - INPUT:
    //            - SqlTrx
    //
    //      - OUTPUT :
    //            - DataTable
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean GetPatternTerminals(SqlTransaction Trx, out DataTable PatternTerminals)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      PatternTerminals = new DataTable();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   TG.TG_TERMINAL_ID AS " + TERMINAL_COLUMN_TERMINAL_ID);
        _sb.AppendLine("        , PT.PT_ID AS " + TERMINAL_COLUMN_PATTERN_ID);
        _sb.AppendLine("   FROM   TERMINAL_GROUPS AS TG INNER JOIN PATTERNS AS PT");
        _sb.AppendLine("          ON TG.TG_ELEMENT_ID = PT.PT_ID");
        _sb.AppendLine("  WHERE   TG.TG_ELEMENT_TYPE = @pElementType");
        _sb.AppendLine("    AND   PT.PT_ACTIVE = 1");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pElementType", SqlDbType.Int).Value = EXPLOIT_ELEMENT_TYPE.PATTERNS;

          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(PatternTerminals);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetPatternTerminals

    //------------------------------------------------------------------------------
    // PURPOSE : Process the data to identify patterns
    //
    //  PARAMS :
    //      - INPUT:
    //            - DataTable
    //            - Dictionary<Int64, PatternTemplate>
    //            - Dictionary<String, PendingPattern>
    //            - Stack<String>
    //
    //      - OUTPUT :
    //            - Dictionary<String, PendingPattern> PatternList,
    //            - Stack<String>
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean PatternLookUp(DataTable dtAlarms,
                                        Dictionary<Int64, PatternTemplate> PatternTemplate,
                                        ref Dictionary<String, PendingPattern> PatternList,
                                        ref Stack<String> OrderedList)
    {
      Int32 _code;
      Int64 _code_value;
      Int64 _identifier;
      String _identifier_name;
      DateTime _alarm_detection_time;
      Int64 _temp_alarm_id;
      List<String> _list_delete_pendings;
      Dictionary<String, PendingPattern> _new_patterns;
      SortedList<Int64, String> _ordered_values;

      _temp_alarm_id = 0;
      _ordered_values = new SortedList<Int64, String>();
      OrderedList = new Stack<String>();

      try
      {
        foreach (DataRow _row in dtAlarms.Rows)
        {
          // Gather information
          _code = (Int32)_row[ALARM_COLUMN_ALARM_CODE];

          if (_code >= START_RANGE_GAME_ALARMS && _code <= END_RANGE_GAME_ALARMS)
          {
            _code = START_RANGE_GAME_ALARMS;
          }

          _code_value = (Int64)_row[ALARM_COLUMN_ID];
          _identifier = (Int64)_row[ALARM_COLUMN_IDENTIFIER];
          _identifier_name = (String)_row[ALARM_COLUMN_IDENTIFIER_NAME];
          _alarm_detection_time = (DateTime)_row[ALARM_COLUMN_ALARM_DETECTION_TIME];

          // Clear information
          _list_delete_pendings = new List<String>();
          _new_patterns = new Dictionary<String, PendingPattern>();

          // Process open patterns
          ProcessOpenPatterns(_code, _code_value, _identifier, _alarm_detection_time, PatternTemplate,
                              ref PatternList, ref _temp_alarm_id, ref _list_delete_pendings, ref _new_patterns);
          // Create new patterns
          ProcessNewPatterns(_code, _code_value, _identifier, _identifier_name, _alarm_detection_time, PatternTemplate,
                             ref PatternList, ref _temp_alarm_id, ref _list_delete_pendings, ref _new_patterns);

          // Add new patterns
          foreach (PendingPattern _pending_pattern in _new_patterns.Values)
          {
            if (!PatternList.ContainsKey(_pending_pattern.Key))
            {
              PatternList.Add(_pending_pattern.Key, _pending_pattern);
            }
          }

          // Remove patterns marked for deletion
          foreach (String _key in _list_delete_pendings)
          {
            if (PatternList.ContainsKey(_key))
            {
              PatternList.Remove(_key);
            }
          }

          // Change the state from pending to closed
          foreach (PendingPattern _pending_pattern in PatternList.Values)
          {
            // We don't process the closed, pending and deleted patterns
            if ((PATTERN_STATE)_pending_pattern.State == PATTERN_STATE.PENDING)
            {
              _pending_pattern.State = (Int32)PATTERN_STATE.CLOSED;
            }
          }
        } // foreach

        // Dictionary is copied into a List and sorted
        foreach (PendingPattern _pending_pattern in PatternList.Values)
        {
          if (_pending_pattern.temp_alarm_id != 0)
          {
            _ordered_values.Add(_pending_pattern.temp_alarm_id, _pending_pattern.Key);
          }
        }

        // Sorted list is copied into a stack
        foreach (KeyValuePair<Int64, String> _order in _ordered_values)
        {
          OrderedList.Push(_order.Value);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // PatternLookUp

    //------------------------------------------------------------------------------
    // PURPOSE : Get list of patterns 
    //
    //  PARAMS :
    //      - INPUT:
    //            - DataTable
    //
    //      - OUTPUT :
    //            - DataTable
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean GetPatternsList(out DataTable DtPatterns)
    {
      DtPatterns = new DataTable();

      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          if (!Trx_GetPatternList(_trx.SqlTransaction, out DtPatterns))
          {

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }   //GetPatternsList

    //------------------------------------------------------------------------------
    // PURPOSE : Trx to get the list of patterns
    //
    //  PARAMS :
    //      - INPUT:
    //            - SqlTransaction
    //            - DataTable
    //
    //      - OUTPUT :
    //            - DataTable
    //
    // RETURNS :
    //      - Boolean
    //
    private static Boolean Trx_GetPatternList(SqlTransaction Trx, out DataTable dtPatterns)
    {
      dtPatterns = new DataTable();
      StringBuilder _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("  WITH ALARM_CATALOG_VALUES AS                                              ");
        _sb.AppendLine("        (                                                                   ");
        _sb.AppendLine("      SELECT	ALCG_ALARM_CODE                                               ");
        _sb.AppendLine("        FROM	ALARM_CATALOG                                                 ");
        _sb.AppendLine("        WHERE	ALCG_TYPE = 1                                                 ");
        _sb.AppendLine("          AND ALCG_LANGUAGE_ID  = @pLanguageID                              ");
        _sb.AppendLine("  )                                                                         ");
        _sb.AppendLine("      SELECT  DISTINCT PT_AL_CODE,                                          ");
        _sb.AppendLine("              p.value('(./Id)[1]', 'VARCHAR(80)') AS ChildAlarmCodes        ");
        _sb.AppendLine("        FROM                                                                ");
        _sb.AppendLine("      (                                                                     ");
        _sb.AppendLine("        SELECT	PT_AL_CODE,PT_PATTERN                                       ");
        _sb.AppendLine("    FROM   PATTERNS                                                         ");
        _sb.AppendLine("         WHERE	PT_PATTERN.value('(Pattern/Element/Id)[1]', 'VARCHAR(80)')  ");
        _sb.AppendLine("            IN	(SELECT * FROM ALARM_CATALOG_VALUES)                        ");
        _sb.AppendLine("      )     AS  t                                                           ");
        _sb.AppendLine("   CROSS APPLY  PT_PATTERN.nodes('/Pattern/Element') pt (p)                 ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pLanguageID", SqlDbType.Int).Value = WSI.Common.Resource.LanguageId;
          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(dtPatterns);
          }
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return true;

    } //Trx_GetPatternList

    //------------------------------------------------------------------------------
    // PURPOSE : Gets the list of patterns that will not displayed on the form
    //
    //  PARAMS :
    //      - INPUT:
    //            - Int64
    //            - DataTable
    //
    //      - OUTPUT :
    //            - DataTable
    //
    // RETURNS :
    //      - DataTable
    //
    public static Boolean GetForbiddenPatterns(Int64 Id, out DataTable dtForbidden)
    {
      Stack<Int64> _st;
      Int64 _to_process;
      Int64 _id_finded;
      DataRow[] _finded;
      DataTable _dtPatterns;

      dtForbidden = new DataTable();

      try
      {
        _st = new Stack<Int64>();
        _dtPatterns = new DataTable();

        dtForbidden.Columns.Add("Id", Type.GetType("System.Int64"));
        dtForbidden.PrimaryKey = new DataColumn[] { dtForbidden.Columns[0] };

        GetPatternsList(out _dtPatterns);

        _st.Push(Id);

        while (_st.Count > 0)
        {
          _to_process = _st.Pop();
          if (dtForbidden.Rows.Find(_to_process) != null)
          {
            continue;
          }
          dtForbidden.Rows.Add(_to_process);

          _finded = _dtPatterns.Select("ChildAlarmCodes = " + _to_process);
          foreach (DataRow _dr in _finded)
          {
            _id_finded = (Int32)_dr["PT_AL_CODE"];
            if (dtForbidden.Rows.Find(_id_finded) == null)
            {
              _st.Push(_id_finded);
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {

        Log.Exception(_ex);
      }

      return false;
    } //GetForbiddenPatterns

    //------------------------------------------------------------------------------
    // PURPOSE : Process the data to identify new patterns
    //
    //  PARAMS :
    //      - INPUT:
    //            - Int32
    //            - Int64
    //            - Int64
    //            - String
    //            - DateTime
    //            - Dictionary<Int64, PatternTemplate>
    //            - Dictionary<String, PendingPattern>
    //            - Int64
    //            - List<String> DeletePendings,
    //            - Dictionary<String, PendingPattern>
    //
    //      - OUTPUT :
    //            - Dictionary<String, PendingPattern>
    //            - Int64
    //            - List<String>
    //            - Dictionary<String, PendingPattern>
    //
    // RETURNS :
    //
    private static void ProcessNewPatterns(Int32 Code,
                                           Int64 CodeValue,
                                           Int64 Identifier,
                                           String IdentifierName,
                                           DateTime DetectionTime,
                                           Dictionary<Int64, PatternTemplate> PatternTemplate,
                                           ref Dictionary<String, PendingPattern> PatternList,
                                           ref Int64 TempAlarmId,
                                           ref List<String> DeletePendings,
                                           ref Dictionary<String, PendingPattern> NewPatterns)
    {
      PendingPattern _pending_pattern;
      Int32 _idx_element;

      _idx_element = 0;

      try
      {
        foreach (PatternTemplate _pattern_element in PatternTemplate.Values)
        {
          // Terminal should be processed by the pattern?
          if (!TerminalInPattern(Identifier, _pattern_element.terminal_list, _pattern_element.terminal_list_type))
          {

            continue;
          }

          // Pattern is active when the event ocurred?
          if (!ScheduleDay.IsTimeInRange(DetectionTime.TimeOfDay, _pattern_element.time_from, _pattern_element.time_to))
          {

            continue;
          }

          switch (_pattern_element.pattern_type)
          {
            case PATTERN_TYPE.UNKNOWN:
              // By default goes to next pattern

              continue;
            case PATTERN_TYPE.CONSECUTIVE:
            case PATTERN_TYPE.NO_CONSECUTIVE_WITH_ORDER:
              // Element doesn't match first element of the pattern
              if (_pattern_element.pattern_elements[0] != Code)
              {

                continue;
              }

              _idx_element = 0;

              break;
            case PATTERN_TYPE.NO_ORDER_WITH_REPETITION:
            case PATTERN_TYPE.NO_ORDER:
              _idx_element = Array.IndexOf(_pattern_element.pattern_elements, (Int64)Code);

              // Element doesn't match any of the elements in the pattern
              if (_idx_element == -1)
              {

                continue;
              }

              break;
            default:

              // By default goes to next pattern
              continue;
          }

          // Creating a new pending pattern
          _pending_pattern = new PendingPattern();
          _pending_pattern.pattern_id = _pattern_element.pattern_id;
          _pending_pattern.alarm_code = _pattern_element.alarm_code;
          _pending_pattern.alarm_name = _pattern_element.alarm_name;
          _pending_pattern.alarm_description = _pattern_element.alarm_description;
          _pending_pattern.alarm_severity = (Int32)_pattern_element.alarm_severity;
          _pending_pattern.pattern_elements = _pattern_element.pattern_elements;
          _pending_pattern.State = (Int32)PATTERN_STATE.CREATED;
          _pending_pattern.element_identifier = Identifier;
          _pending_pattern.element_identifier_name = IdentifierName;
          _pending_pattern.element_values = new Int64[_pattern_element.pattern_elements.Length];
          _pending_pattern.element_values.SetValue(CodeValue, _idx_element);
          _pending_pattern.pattern_type = (Int32)_pattern_element.pattern_type;
          _pending_pattern.creation_time = WGDB.Now;
          _pending_pattern.first_element_time = DetectionTime;
          _pending_pattern.last_element_time = DetectionTime;
          _pending_pattern.alarm_mailing_enabled = _pattern_element.mailing_enabled;
          _pending_pattern.alarm_mailing_address = _pattern_element.mailing_address;
          _pending_pattern.alarm_mailing_subject = _pattern_element.mailing_subject;

          _pending_pattern.CreateKey(_pattern_element.pattern_id, Identifier, CodeValue);

          if (_pattern_element.pattern_type == PATTERN_TYPE.NO_ORDER)
          {
            foreach (PendingPattern _pattern in PatternList.Values)
            {
              if ((_pattern.State == (Int32)PATTERN_STATE.CREATED || _pattern.State == (Int32)PATTERN_STATE.PENDING) &&
                   _pattern.pattern_id == _pending_pattern.pattern_id &&
                   _pattern.element_identifier == _pending_pattern.element_identifier)
              {

                return;
              }
            }
          }

          if (!NewPatterns.ContainsKey(_pending_pattern.Key))
          {
            NewPatterns.Add(_pending_pattern.Key, _pending_pattern);
          }

          //Should we close the pending pattern? --> It's a one element pattern.
          _idx_element = Array.IndexOf(_pending_pattern.element_values, (Int64)0);
          if (_idx_element == -1)
          {
            if (_pattern_element.pattern_type == PATTERN_TYPE.NO_ORDER)
            {
              _pending_pattern.State = (Int32)PATTERN_STATE.PENDING;
            }
            else
            {
              _pending_pattern.State = (Int32)PATTERN_STATE.CLOSED;
            }

            // If pattern closes then child patterns should be verified (looking to close patterns of patterns)
            TempAlarmId--;
            _pending_pattern.temp_alarm_id = TempAlarmId;
            ProcessOpenPatterns(_pending_pattern.alarm_code, _pending_pattern.temp_alarm_id, Identifier, DetectionTime,
                                PatternTemplate, ref PatternList, ref TempAlarmId, ref DeletePendings, ref NewPatterns);
            ProcessNewPatterns(_pending_pattern.alarm_code, _pending_pattern.temp_alarm_id, Identifier, IdentifierName,
                               DetectionTime, PatternTemplate, ref PatternList, ref TempAlarmId, ref DeletePendings, ref NewPatterns);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

    } // ProcessNewPatterns

    //------------------------------------------------------------------------------
    // PURPOSE : Searches a Terminal into a given datatable
    //
    //  PARAMS :
    //      - INPUT:
    //            - Int64
    //            - DataTable
    //            - Int32
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    private static Boolean TerminalInPattern(Int64 TerminalID, DataTable TerminalTable, Int32 TerminalType)
    {
      Boolean _found;
      DataRow[] _dr;

      _found = false;

      if (TerminalType == 0)
      {
        // We assume that if the Terminal table is empty means it accepts all the Terminals

        return true;
      }

      if (TerminalTable.Rows.Count == 0)
      {
        // We assume that if the Terminal table is empty means it doesn't accept any Terminals

        return false;
      }

      _dr = TerminalTable.Select(TERMINAL_COLUMN_TERMINAL_ID + " = " + TerminalID);

      if (_dr != null)
      {
        if (_dr.Length > 0)
        {
          _found = true;
        }
      }

      return _found;
    } // TerminalInPattern

    //------------------------------------------------------------------------------
    // PURPOSE : Process the data to identify already open patterns
    //
    //  PARAMS :
    //      - INPUT:
    //            - Int32
    //            - Int64
    //            - Int64
    //            - DateTime
    //            - Dictionary<Int64, PatternTemplate>
    //            - Dictionary<String, PendingPattern>
    //            - Int64
    //            - List<String>
    //            - Dictionary<String, PendingPattern>
    //
    //      - OUTPUT :
    //            - Dictionary<String, PendingPattern>
    //            - Int64
    //            - List<String>
    //            - Dictionary<String, PendingPattern>
    //
    // RETURNS :
    //
    private static void ProcessOpenPatterns(Int32 Code,
                                            Int64 CodeValue,
                                            Int64 Identifier,
                                            DateTime DetectionTime,
                                            Dictionary<Int64, PatternTemplate> PatternTemplate,
                                            ref Dictionary<String, PendingPattern> PatternList,
                                            ref Int64 TempAlarmId,
                                            ref List<String> DeletePendings,
                                            ref Dictionary<String, PendingPattern> NewPatterns)
    {
      Int32 _idx_element;
      Int64[] _found_elements;

      _idx_element = -1;

      try
      {
        foreach (PendingPattern _pending_pattern in PatternList.Values)
        {
          // We don't process the closed, pending and deleted patterns
          if ((PATTERN_STATE)_pending_pattern.State != PATTERN_STATE.CREATED)
          {

            continue;
          }

          // We expire all open patterns whose times are not in the schedule template. Pattern was active when the event ocurred?
          // 10-JUN-2014 JPJ    Fixed Bug WIG-1026: The scheduled time when the pattern is active
          if (!(PatternTemplate[_pending_pattern.pattern_id].time_from == 0 &&
                PatternTemplate[_pending_pattern.pattern_id].time_to == 0))
          {
            if (!ScheduleDay.IsTimeInRange(DetectionTime.TimeOfDay,
                                           PatternTemplate[_pending_pattern.pattern_id].time_from,
                                           PatternTemplate[_pending_pattern.pattern_id].time_to))
            {
              _pending_pattern.State = (Int32)PATTERN_STATE.DELETE;
              DeletePendings.Add(_pending_pattern.Key);

              continue;
            }
          }
          // Should we expire the pending pattern?
          if (DetectionTime > _pending_pattern.first_element_time.AddMinutes(PatternTemplate[_pending_pattern.pattern_id].life_time))
          {
            // Pending pattern is expired
            _pending_pattern.State = (Int32)PATTERN_STATE.DELETE;
            DeletePendings.Add(_pending_pattern.Key);

            continue;
          }

          // If identifier (TerminalID) doesn't match with the identifier in the pending pattern we don't continue
          if (_pending_pattern.element_identifier != Identifier)
          {

            continue;
          }

          PATTERN_TYPE _type = (PATTERN_TYPE)_pending_pattern.pattern_type;
          switch (_type)
          {
            case PATTERN_TYPE.UNKNOWN:

              break;
            case PATTERN_TYPE.CONSECUTIVE:
            case PATTERN_TYPE.NO_CONSECUTIVE_WITH_ORDER:
              // 06-JUN-2014 JPJ    Fixed Bug WIG-1016: Some patterns don't show up when pattern is consecutive or no consecutive with order
              // 31-JUL-2014 JPJ    Fixed Bug WIG-1135: Some consecutive patterns where not deleted
              for (int _idx = 0; _idx <= _pending_pattern.element_values.GetUpperBound(0); _idx++)
              {
                if (_pending_pattern.element_values[_idx] == 0)
                {
                  if (Code != _pending_pattern.pattern_elements[_idx])
                  {
                    // Element doesn't match the next element of the pattern template
                    if (_type == PATTERN_TYPE.CONSECUTIVE)
                    {
                      // Pending pattern doesn't match any more
                      _pending_pattern.State = (Int32)PATTERN_STATE.DELETE;
                      DeletePendings.Add(_pending_pattern.Key);
                    }

                    continue;
                  }

                  break;
                }
              }

              _found_elements = new Int64[0];
              // We look the element in the pattern and compare with the pattern template
              _found_elements = FindAllInArray(_pending_pattern.pattern_elements, Code);
              // Element doesn't match any of the elements in the pattern
              if (_found_elements.Length == 0)
              {

                continue;
              }

              foreach (Int64 _element_value in _found_elements)
              {
                // Position in the pending pattern is already used?
                if (_pending_pattern.element_values[_element_value] == 0)
                { // NO
                  _idx_element = (Int32)_element_value;
                  //Add element in the pending pattern
                  _pending_pattern.element_values[_idx_element] = CodeValue;

                  break;
                }
              }

              break;
            case PATTERN_TYPE.NO_ORDER:
            case PATTERN_TYPE.NO_ORDER_WITH_REPETITION:
              _found_elements = new Int64[0];
              // We look the element in the pattern and compare with the pattern template
              _found_elements = FindAllInArray(_pending_pattern.pattern_elements, Code);
              // Element doesn't match any of the elements in the pattern
              if (_found_elements.Length == 0)
              {

                continue;
              }

              foreach (Int64 _element_value in _found_elements)
              {
                // Position in the pending pattern is already used?
                if (_pending_pattern.element_values[_element_value] == 0)
                { // NO
                  _idx_element = (Int32)_element_value;
                  //Add element in the pending pattern
                  _pending_pattern.element_values[_idx_element] = CodeValue;

                  break;
                }
              }

              break;
            default:

              break;
          }

          // Should we close the pending pattern?
          _idx_element = Array.IndexOf(_pending_pattern.element_values, (Int64)0);
          if (_idx_element == -1)
          {
            if (_pending_pattern.pattern_type == (Int32)PATTERN_TYPE.NO_ORDER)
            {
              _pending_pattern.State = (Int32)PATTERN_STATE.PENDING;
            }
            else
            {
              _pending_pattern.State = (Int32)PATTERN_STATE.CLOSED;
            }
            _pending_pattern.last_element_time = DetectionTime;
            TempAlarmId--;

            _pending_pattern.temp_alarm_id = TempAlarmId;
            // If pattern closes then child patterns should be verified
            ProcessOpenPatterns(_pending_pattern.alarm_code, _pending_pattern.temp_alarm_id, Identifier, DetectionTime,
                           PatternTemplate, ref PatternList, ref TempAlarmId, ref DeletePendings, ref NewPatterns);
            ProcessNewPatterns(_pending_pattern.alarm_code, _pending_pattern.temp_alarm_id, Identifier, _pending_pattern.element_identifier_name,
                                    DetectionTime, PatternTemplate, ref PatternList, ref TempAlarmId, ref DeletePendings, ref NewPatterns);

          }
        } // End foreach

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return;
    }  // ProcessOpenPatterns

    //------------------------------------------------------------------------------
    // PURPOSE : Finds Value in a given array and returns all its matches into another array 
    //
    //  PARAMS :
    //      - INPUT:
    //            - Array
    //            - Int64
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Int64[]
    //
    private static Int64[] FindAllInArray(Array MyArray, Int64 SearchValue)
    {
      Int64[] _found_elements;
      Int32 _idx_array_position;

      _idx_array_position = 0;
      _found_elements = new Int64[0];

      foreach (Int64 _element in MyArray)
      {
        if (_element == SearchValue)
        {
          Array.Resize(ref _found_elements, _found_elements.Length + 1);
          _found_elements.SetValue(_idx_array_position, _found_elements.GetUpperBound(0));
        }
        _idx_array_position++;
      }

      return _found_elements;
    } // FindAllInArray

    //------------------------------------------------------------------------------
    // PURPOSE : Saves founded patterns, pending patterns and historical
    //
    //  PARAMS :
    //      - INPUT:
    //            - Int64
    //            - Dictionary<String, PendingPattern>
    //            - Stack<String>
    //            - SqlTransaction
    //
    //      - OUTPUT :
    //            - Dictionary<String, PendingPattern>
    //
    // RETURNS :
    //      - Boolean
    //
    private static Boolean SavePatterns(Int64 FinalSequence,
                                        ref Dictionary<String, PendingPattern> PatternList,
                                        Stack<String> OrderedList,
                                        SqlTransaction SqlTrx,
                                        ref DataTable MailingAlarms)
    {
      PendingPattern _pending_pattern;
      List<PendingPattern> _list_pending_patterns;
      Dictionary<Int64, Int64> _number_of_patterns;
      List<String> _list_delete_pendings;

      _number_of_patterns = new Dictionary<Int64, Int64>();
      _list_pending_patterns = new List<PendingPattern>();
      _list_delete_pendings = new List<String>();

      foreach (KeyValuePair<String, PendingPattern> _pattern in PatternList)
      {
        _pending_pattern = _pattern.Value;
        // Mark for deletion are not taken into account
        if ((PATTERN_STATE)_pending_pattern.State == PATTERN_STATE.DELETE)
        {
          _list_delete_pendings.Add(_pending_pattern.Key);
        }

        // Pending patterns are acumulated to save them for next iteration
        if ((PATTERN_STATE)_pending_pattern.State == PATTERN_STATE.CREATED)
        {
          _list_pending_patterns.Add(_pending_pattern);
        }
      }

      // Remove patterns marked for deletion
      foreach (String _key in _list_delete_pendings)
      {
        if (PatternList.ContainsKey(_key))
        {
          PatternList.Remove(_key);
        }
      }

      foreach (String _element in OrderedList)
      {
        _pending_pattern = PatternList[_element];

        // We count how many times a pattern is found per loop
        if (_number_of_patterns.ContainsKey(_pending_pattern.pattern_id))
        {
          _number_of_patterns[_pending_pattern.pattern_id]++;
        }
        else
        {
          _number_of_patterns.Add(_pending_pattern.pattern_id, 1);
        }

        // Register Alarms - Saving the alarm using the Alarm.Register
        Alarm.Register(AlarmSourceCode.Pattern, _pending_pattern.element_identifier,
                       _pending_pattern.element_identifier_name, (uint)_pending_pattern.alarm_code,
                       _pending_pattern.alarm_description, (AlarmSeverity)_pending_pattern.alarm_severity,
                       _pending_pattern.last_element_time, false, _pending_pattern.last_element_time,
                       out _pending_pattern.alarm_id, SqlTrx);

        // Saving the alarm so it can be sent
        if (_pending_pattern.alarm_mailing_enabled == true)
        {
          DataRow _row;

          _pending_pattern.alarm_mailing_body = String.Empty;

          _pending_pattern.alarm_mailing_body = CreateMailingBody(_pending_pattern, SqlTrx);
          _row = MailingAlarms.NewRow();

          _row[MAILING_COLUMN_PROG_NAME] = MAILING_NAME;
          _row[MAILING_COLUMN_ADDRESS_LIST] = _pending_pattern.alarm_mailing_address;
          _row[MAILING_COLUMN_SUBJECT] = _pending_pattern.alarm_mailing_subject;
          _row[MAILING_COLUMN_BODY] = _pending_pattern.alarm_mailing_body;
          _row[MAILING_COLUMN_SCHEDULED] = _pending_pattern.last_element_time;
          _row[MAILING_COLUMN_ALARM_ID] = _pending_pattern.alarm_id;

          // alarm is added
          MailingAlarms.Rows.Add(_row);
        }

        // Pattern History
        SavePatternHistory(_pending_pattern, SqlTrx);

        // Update child patterns with the new id if needed
        UpdateChildPatterns(_pending_pattern, ref PatternList);

        // Pattern is deleted because it has been already processed
        PatternList.Remove(_pending_pattern.Key);
      }

      // Save how many times a pattern is found
      SavePatternTimes(_number_of_patterns, SqlTrx);

      // Save pending patterns
      SavePatternInformation(FinalSequence, _list_pending_patterns, SqlTrx);

      // Clears the list of elements
      _list_pending_patterns.Clear();
      _number_of_patterns.Clear();
      _list_delete_pendings.Clear();

      return true;

    } // SavePatterns

    //------------------------------------------------------------------------------
    // PURPOSE : Searches for child patterns and updates them with the new alarm id
    //
    //  PARAMS :
    //      - INPUT:
    //            - PendingPattern
    //            - Dictionary
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    private static Boolean UpdateChildPatterns(PendingPattern ClosedPattern,
                                               ref Dictionary<String, PendingPattern> PatternList)
    {
      Int64[] _found_elements;

      foreach (PendingPattern _pending_pattern in PatternList.Values)
      {
        // Is the same element identifier?
        if (ClosedPattern.element_identifier != _pending_pattern.element_identifier)
        {

          continue;
        }

        _found_elements = new Int64[0];
        // We look the element in the pattern and compare with the pattern template
        _found_elements = FindAllInArray(_pending_pattern.element_values, ClosedPattern.temp_alarm_id);
        // Element doesn't match any of the elements in the pattern
        if (_found_elements.Length == 0)
        {

          continue;
        }

        foreach (Int64 _element_value in _found_elements)
        {
          //Modify temp element in the pending pattern for the correct value
          _pending_pattern.element_values[_element_value] = ClosedPattern.alarm_id;
        }
      }

      return true;
    } // UpdateChildPatterns

    //------------------------------------------------------------------------------
    // PURPOSE : Saves pending patterns and the last item to be processed
    //
    //  PARAMS :
    //      - INPUT :
    //                - PendingPattern
    //                - SqlTransaction
    //
    // RETURNS :
    //            - True:  pending pattern created correctly
    //            - False: pending pattern not created
    //
    private static Boolean SavePatternHistory(PendingPattern Pattern, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO   HISTORICAL_GENERATED_PATTERNS");
        _sb.AppendLine("              ( hgp_al_id");
        _sb.AppendLine("              , hgp_pattern_id");
        _sb.AppendLine("              , hgp_pattern_values");
        _sb.AppendLine("              , hgp_date");
        _sb.AppendLine("              , hgp_element_id");
        _sb.AppendLine("              , hgp_alarm_code");
        _sb.AppendLine("              , hgp_alarm_name");
        _sb.AppendLine("              , hgp_alarm_description");
        _sb.AppendLine("              )");
        _sb.AppendLine("      VALUES (");
        _sb.AppendLine("                @pAlarmID");
        _sb.AppendLine("              , @pPatternID");
        _sb.AppendLine("              , @pPatternValues");
        _sb.AppendLine("              , @pCreationDate");
        _sb.AppendLine("              , @pElementID");
        _sb.AppendLine("              , @pAlarmCode");
        _sb.AppendLine("              , @pAlarmName");
        _sb.AppendLine("              , @pAlarmDescription");
        _sb.AppendLine("             )");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAlarmID", SqlDbType.BigInt).Value = (Int64)Pattern.alarm_id;
          _cmd.Parameters.Add("@pPatternID", SqlDbType.BigInt).Value = (Int64)Pattern.pattern_id;
          _cmd.Parameters.Add("@pPatternValues", SqlDbType.Xml).Value = Pattern.ElementValuesToXML();
          _cmd.Parameters.Add("@pCreationDate", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pElementID", SqlDbType.BigInt).Value = Pattern.element_identifier;
          _cmd.Parameters.Add("@pAlarmCode", SqlDbType.NVarChar).Value = Pattern.alarm_code;
          _cmd.Parameters.Add("@pAlarmName", SqlDbType.NVarChar).Value = Pattern.alarm_name;
          _cmd.Parameters.Add("@pAlarmDescription", SqlDbType.NVarChar).Value = Pattern.alarm_description;
          if (_cmd.ExecuteNonQuery() == 1)
          {

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // PatternsListToDictionary

    //------------------------------------------------------------------------------
    // PURPOSE : Saves pending patterns and the last item to be processed
    //
    //  PARAMS :
    //      - INPUT :
    //                - Int64
    //                - List<PendingPattern>
    //                - SqlTransaction
    //
    // RETURNS :
    //            - True:  pending pattern created correctly
    //            - False: pending pattern not created
    //
    private static Boolean SavePatternInformation(Int64 FinalSequence,
                                                  List<PendingPattern> PendingPatterns,
                                                  SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        // We can choose how many registers we want to keep in the table
        _sb.AppendLine("  DELETE      PATTERNS_INFORMATION");
        _sb.AppendLine("   WHERE      PTI_ID NOT IN");
        _sb.AppendLine("              (");
        _sb.AppendLine("                 SELECT TOP 1 PTI_ID");
        _sb.AppendLine("                   FROM PATTERNS_INFORMATION ");
        _sb.AppendLine("                   ORDER BY PTI_ID DESC");
        _sb.AppendLine("              )");
        _sb.AppendLine();
        _sb.AppendLine(" INSERT INTO  PATTERNS_INFORMATION");
        _sb.AppendLine("              ( PTI_STATE");
        _sb.AppendLine("              , PTI_OPEN_PATTERNS");
        _sb.AppendLine("              , PTI_SEQUENCE");
        _sb.AppendLine("              , PTI_CREATION");
        _sb.AppendLine("              )");
        _sb.AppendLine("      VALUES (");
        _sb.AppendLine("                @pState");
        _sb.AppendLine("              , @pOpenPatterns");
        _sb.AppendLine("              , @pSequence");
        _sb.AppendLine("              , @pCreationtionDate");
        _sb.AppendLine("             )");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pState", SqlDbType.Int).Value = 1;

          if (PendingPatterns.Count >= 1)
          {
            _cmd.Parameters.Add("@pOpenPatterns", SqlDbType.Xml).Value = PatternsListToXML(PendingPatterns);
          }
          else
          {
            _cmd.Parameters.Add("@pOpenPatterns", SqlDbType.Xml).Value = DBNull.Value;
          }
          _cmd.Parameters.Add("@pSequence", SqlDbType.Int).Value = FinalSequence;
          _cmd.Parameters.Add("@pCreationtionDate", SqlDbType.DateTime).Value = WGDB.Now;

          if (_cmd.ExecuteNonQuery() == 1)
          {

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // SavePatternInformation

    //------------------------------------------------------------------------------
    // PURPOSE : Saves the number of detecctions and the time of when was last find
    //
    //  PARAMS :
    //      - INPUT :
    //                - Dictionary<Int64, Int64>
    //                - SqlTransaction
    //
    // RETURNS :
    //                - Boolean
    //
    private static Boolean SavePatternTimes(Dictionary<Int64, Int64> PatternDetecctions,
                                            SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        foreach (KeyValuePair<Int64, Int64> _pattern_detecction in PatternDetecctions)
        {
          _sb.Length = 0;
          // We can choose how many registers we want to keep in the table
          _sb.AppendLine(" UPDATE Patterns SET PT_DETECTIONS = PT_DETECTIONS + @pDetections,");
          _sb.AppendLine("                     PT_LAST_FIND  = @pLastFind");
          _sb.AppendLine("  WHERE PT_ID = @pPatternId");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pPatternId", SqlDbType.BigInt).Value = _pattern_detecction.Key;
            _cmd.Parameters.Add("@pDetections", SqlDbType.Int).Value = _pattern_detecction.Value;
            _cmd.Parameters.Add("@pLastFind", SqlDbType.DateTime).Value = WGDB.Now;

            _cmd.ExecuteNonQuery();
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // SavePatternTimes

    //------------------------------------------------------------------------------
    // PURPOSE : Generates the pending pattern object into a XML
    //
    //  PARAMS :
    //      - INPUT :
    //            - List<PendingPattern>
    //
    // RETURNS :
    //            - String
    //
    private static String PatternsListToXML(List<PendingPattern> Patterns)
    {
      String _xml;

      _xml = "";

      System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(Patterns.GetType());
      using (System.IO.StringWriter _xmlelement = new System.IO.StringWriter())
      {
        serializer.Serialize(_xmlelement, Patterns);
        _xml = _xmlelement.ToString();
      }

      return _xml.ToString();
    } // PatternsListToXML

    //------------------------------------------------------------------------------
    // PURPOSE : Generates the pending pattern object from a XML
    //
    //  PARAMS :
    //      - INPUT :
    //            - String
    //
    // RETURNS :
    //            - List<PendingPattern>
    //
    private static List<PendingPattern> PatternsListFromXML(String Xml)
    {
      List<PendingPattern> _list_pending_patterns;

      _list_pending_patterns = new List<PendingPattern>();

      try
      {
        System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(_list_pending_patterns.GetType());
        using (System.IO.StringReader _xmlelement = new System.IO.StringReader(Xml))
        {
          Object _o = serializer.Deserialize(_xmlelement);
          _list_pending_patterns = (List<PendingPattern>)_o;
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _list_pending_patterns;
    }  // PatternsListFromXML

    //------------------------------------------------------------------------------
    // PURPOSE : Transforms a list of patterns into a pattern dictionary
    //
    //  PARAMS :
    //      - INPUT :
    //            - List<PendingPattern>
    //
    // RETURNS :
    //            - Dictionary<String, PendingPattern>
    //
    private static Dictionary<String, PendingPattern> PatternsListToDictionary(List<PendingPattern> PendingPatterns)
    {
      Dictionary<String, PendingPattern> _pending_patterns;

      _pending_patterns = new Dictionary<String, PendingPattern>();

      foreach (PendingPattern _element in PendingPatterns)
      {
        _pending_patterns.Add(_element.Key, _element);
      }

      return _pending_patterns;
    } // PatternsListToDictionary

    //------------------------------------------------------------------------------
    // PURPOSE :  Creates AlarmMailing schema table 
    //
    //  PARAMS :
    //      - INPUT :
    //
    // RETURNS :
    //
    //      - DataTable
    //
    private static DataTable GetMailDataTable()
    {
      DataTable _dt;
      DataColumn _col;

      _dt = new DataTable();

      _col = _dt.Columns.Add("AM_UNIQUE_ID", Type.GetType("System.Int64"));
      _col.AutoIncrement = true;
      _col.AutoIncrementSeed = -1;
      _col.AutoIncrementStep = -1;

      _dt.Columns.Add(MAILING_COLUMN_PROG_NAME, Type.GetType("System.String"));
      _dt.Columns.Add(MAILING_COLUMN_ADDRESS_LIST, Type.GetType("System.String"));
      _dt.Columns.Add(MAILING_COLUMN_SUBJECT, Type.GetType("System.String"));
      _dt.Columns.Add(MAILING_COLUMN_BODY, Type.GetType("System.String"));
      _dt.Columns.Add(MAILING_COLUMN_SCHEDULED, Type.GetType("System.DateTime"));
      _dt.Columns.Add(MAILING_COLUMN_ALARM_ID, Type.GetType("System.Int64"));

      _dt.PrimaryKey = new DataColumn[] { _dt.Columns["AF_UNIQUE_ID"] };

      return _dt;
    } // GetMailDataTable

    //------------------------------------------------------------------------------
    // PURPOSE : Processes the alarms that have to be sent by email.
    //
    //  PARAMS :
    //      - INPUT :
    //
    // RETURNS :
    //
    //      - DataTable
    //
    private static void AlarmsToMail(DataTable dtMailingAlarms, SqlTransaction Trx)
    {
      String _prog_name;
      String _address_list;
      String _subject;
      String _body;
      Int64 _prog_data;

      foreach (DataRow _row in dtMailingAlarms.Rows)
      {
        // Obtain mail information
        _prog_name = (String)_row[MAILING_COLUMN_PROG_NAME];
        _address_list = (String)_row[MAILING_COLUMN_ADDRESS_LIST];
        _subject = (String)_row[MAILING_COLUMN_SUBJECT];
        _body = (String)_row[MAILING_COLUMN_BODY];
        _prog_data = (Int64)_row[MAILING_COLUMN_ALARM_ID];

        // Send alamarm to queue up and get send by mail
        Mailing.DB_InsertInstance(WGDB.Now, _prog_name, MAILING_PROGRAMMING_TYPE.PATTERNS,
                                  _address_list, _subject, _body,
                                  MAILING_INSTANCES_STATUS.PENDING, _prog_data, Trx);
      }

      return;
    } // AlarmsToMail

    private static String CreateMailingBody(PendingPattern Pattern, SqlTransaction Trx)
    {
      StringBuilder _body;
      Int32 _alarm_position;
      AlarmInformation _alarm_information;
      Int64 _alarm_id;

      _alarm_position = -1;
      _alarm_id = -1;

      _body = new StringBuilder();
      //Alarm '{0}' was generated on {1} at {2}.
      _body.AppendLine(String.Format(Resource.String("STR_PATTERN_ALARM_CREATION"), Pattern.alarm_name, Format.CustomFormatDateTime(Pattern.last_element_time, false), Pattern.last_element_time.ToString(Format.TimeCustomFormatString(false))));

      // Element doesn't match any of the elements in the pattern
      _alarm_position = Array.IndexOf(Pattern.pattern_elements, (Int64)AlarmCode.CustomAlarm_Player_Birthday);
      if (_alarm_position != -1)
      {
        _alarm_id = Pattern.element_values[_alarm_position];
      }

      _alarm_position = Array.IndexOf(Pattern.pattern_elements, (Int64)AlarmCode.CustomAlarm_Soon_Player_Birthday);
      if (_alarm_position != -1)
      {
        _alarm_id = Pattern.element_values[_alarm_position];
      }

      if (_alarm_id != -1)
      {
        if (GetAlarmInformation(_alarm_id, Trx, out _alarm_information))
        {
          //Cuenta: {0} \r\n\r\n
          //Nombre: {1}
          //Origen: {2}
          _body.AppendLine(String.Format(Resource.String("STR_PATTERN_ALARM_CREATION_ACCOUNT"), _alarm_information.al_source_id, _alarm_information.al_source_name, _alarm_information.al_alarm_description));
        }
      }
      else
      {
        _alarm_id = Pattern.element_values[0];
        if (GetAlarmInformation(_alarm_id, Trx, out _alarm_information))
        {
          switch ((AlarmSourceCode)_alarm_information.al_source_code)
          {
            case AlarmSourceCode.GameTerminal:
            case AlarmSourceCode.Terminal3GS:
            case AlarmSourceCode.TerminalSASHost:
            case AlarmSourceCode.TerminalSASMachine:
            case AlarmSourceCode.TerminalSystem:
            case AlarmSourceCode.TerminalWCP:
              {

                WSI.Common.Terminal.TerminalInfo _terminal_info;
                WSI.Common.Terminal.Trx_GetTerminalInfo((Int32)Pattern.element_identifier, out _terminal_info, Trx);
                //Terminal: {0}. \r\n Nombre: {1}. \r\n Id en planta: {2}. \r\n Isla: {3}. \r\n Nombre del proveedor: {4}. \r\n Direcci�n MAC: {5}.

                _body.AppendLine(String.Format(Resource.String("STR_PATTERN_ALARM_CREATION_TERMINAL"),
                                 _terminal_info.TerminalId, _terminal_info.Name, _terminal_info.FloorId,
                                 _terminal_info.BankName, _terminal_info.ProviderName, _terminal_info.ExternalId));
                
                break;
              }
            default:
              
              break;
          }
        }
      }

      return _body.ToString();
    } // CreateMailingBody

    //------------------------------------------------------------------------------
    // PURPOSE : Returns a row of the Alarms table
    //
    //  PARAMS :
    //      - INPUT:
    //            - Alarm_id
    //            - SqlTrx
    //
    //      - OUTPUT :
    //            - DataTable
    //
    // RETURNS :
    //      - Boolean
    //
    private static Boolean GetAlarmInformation(Int64 AlarmId, SqlTransaction Trx, out AlarmInformation AlarmInformationRow)
    {
      StringBuilder _sb;

      AlarmInformationRow = new AlarmInformation();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT     AL_SOURCE_ID ");
        _sb.AppendLine("          , AL_SOURCE_NAME ");
        _sb.AppendLine("          , AL_ALARM_DESCRIPTION ");
        _sb.AppendLine("          , AL_SOURCE_CODE ");
        _sb.AppendLine("   FROM   ALARMS");
        _sb.AppendLine("  WHERE   AL_ALARM_ID = @pAlarmId");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pAlarmId", SqlDbType.BigInt).Value = AlarmId;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            // If there are no patterns to analyze then we can exit
            if (!_reader.Read())
            {
              Log.Warning("GetAlarmInformation: Alarm not found.  AlarmId: " + AlarmId.ToString());

              return false;
            }

            AlarmInformationRow.al_source_id = _reader.GetInt64(0);
            AlarmInformationRow.al_source_name = _reader.GetString(1);
            if (!_reader.IsDBNull(2))
            {
              AlarmInformationRow.al_alarm_description = _reader.GetString(2);
            }

            AlarmInformationRow.al_source_code = _reader.GetInt32(3);

            return true;
          }
        }
      }  // Using SqlCommand
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetAlarmInformation

  } // class Patterns

}  // WSI.Common