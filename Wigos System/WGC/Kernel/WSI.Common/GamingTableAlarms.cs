﻿//------------------------------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------------------------------
// 
//   MODULE NAME: GamingTableAlarms.cs
//  
//   DESCRIPTION: 
// 
//        AUTHOR: José Martínez
// 
// CREATION DATE: 18-SEP-2017
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ---------- -------------------------------------------------------------------------------
// 18-SEP-2017 JML        First release.
// 21-SEP-2017 RAB        PBI 29318 - WIGOS-3699 MES13 Specific alarms - Gaming table bets over
// 21-SEP-2017 JML        PBI 29317:WIGOS-3409: MES13 Specific alarms: Gaming table day loss
// 26-SEP-2017 JML        PBI 29319:MES13 Specific alarms: Purchase: Card
// 28-SEP-2017 RAB        PBI 29842: WIGOS-3757 MES13 Specific alarms - Gaming table player sitting time
// 29-SEP-2017 RAB        PBI 29841: WIGOS-3756 MES13 Specific alarms - Gaming table bet deviation
// 10-OCT-2017 RAB        Bug 30200: WIGOS-5683  Gambling tables alarms: TITO. Error has been registered on cashier log when is performed a buy chips without sell in a customized user
// 02-NOV-2017 RAB        Bug 30532: WIGOS-6253 Gambling tables alarms: "PaymentDoneByPlayer.UnderMinutes" alarm is not triggered when the sale chips is with foreign currency
//------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Xml;
using System.Globalization;

namespace WSI.Common
{
  public static partial class GamingTableAlarms
  {
    #region Enums
    #endregion

    #region Public
    /// <summary>
    /// Get GP value of Gaming table with session loss for $ XX,XXX.XX
    /// </summary>
    /// <returns></returns>
    public static Currency GetSessionLossFor()
    {
      return GeneralParam.GetCurrency("Alarms.GamingTables", "SessionLossFor");
    }

    /// <summary>
    /// Get GP value of Gaming table with 3 days loss for $ XX,XXX.XX
    /// </summary>
    /// <returns></returns>
    public static Currency GetGamblingTable3DayLost()
    {
      return GeneralParam.GetCurrency("Alarms.GamingTables", "GamingTable3DayLost");
    }

    /// <summary>
    /// Get GP value of bets over hand by customer alarm
    /// </summary>
    /// <returns></returns>
    public static Currency GetBetsOverHandByCustomer()
    {
      return GeneralParam.GetCurrency("Alarms.GamingTables", "BetsOver.HandByCustomer", 0);
    }
    
    /// <summary>
    /// Get GP value of 
    /// </summary>
    /// <returns></returns>
    public static Currency GetInvestmentsMadeWithCardEqualOrOver()
    {
      return GeneralParam.GetCurrency("Alarms.GamingTables", "InvestmentsWithCreditDebitCard.EqualOrOver");
    }

    /// <summary>
    /// Get GP value of Customer gets paid without a Drop (purchase)
    /// </summary>
    /// <returns></returns>
    public static Boolean GetCustomerGetsPaidWithoutADrop()
    {
      return GeneralParam.GetBoolean("Alarms.GamingTables", "CustomerGetsPaidWithoutAPurchase");
    }
    
    /// <summary>
    /// Get GP value of Payment Done By Player Under Minutes
    /// </summary>
    /// <returns></returns>
    public static Int32 GetPaymentDoneByPlayerUnderMinutes()
    {
      return GeneralParam.GetInt32("Alarms.GamingTables", "PaymentDoneByPlayer.UnderMinutes", 0, 0, 180);
    }

    /// <summary>
    /// Get GP value of Amount purchased in one operation
    /// </summary>
    /// <returns></returns>
    public static Currency GetAmountPurchaseInOneOperation()
    {
      return GeneralParam.GetCurrency("Alarms.GamingTables", "AmountPurchaseInOneOperation");
    }

    public static Currency GetAmountPaidInOneOperation()
    {
      return GeneralParam.GetCurrency("Alarms.GamingTables", "AmountPaidInOneOperation");
    }
   

    /// <summary>
    /// Get session loss / win
    /// </summary>
    /// <param name="CashierSessionInfo"></param>
    /// <param name="SessionAlarmAmount"></param>
    /// <param name="SessionWinLossAmount"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean CheckForAlarm_SessionLossFor(CashierSessionInfo CashierSessionInfo, Currency SessionAlarmAmount, out Currency SessionWinLossAmount, SqlTransaction Trx)
    {
      StringBuilder _sb;
      String _regional_currency_iso_code;

      _sb = new StringBuilder();
      SessionWinLossAmount = 0;
      _regional_currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");
 
      try
      {
        _sb.AppendLine("   SELECT   CM_CAGE_CURRENCY_TYPE                               ");
        _sb.AppendLine("          , ISNULL(CM_CURRENCY_ISO_CODE, @pRegionalIsoCode)     ");
        _sb.AppendLine("          , SUM(CM_ADD_AMOUNT - CM_SUB_AMOUNT)                  ");
        _sb.AppendLine("     FROM   CASHIER_MOVEMENTS  WITH (INDEX (IX_cm_session_id))  ");
        _sb.AppendLine("    WHERE   CM_SESSION_ID = @pCashierSessionId                  ");
        _sb.AppendLine("      AND   CM_TYPE IN (@pCashClosingShort,                     "); // CASH_CLOSING_SHORT            
        _sb.AppendLine("                        @pCashClosingOver,                      "); // CASH_CLOSING_OVER             
        _sb.AppendLine("                        @pGamingTableGameNetwin,                "); // GAMING_TABLE_GAME_NETWIN      
        _sb.AppendLine("                        @pGamingTableGameNetwinClose)           "); // GAMING_TABLE_GAME_NETWIN_CLOSE
        _sb.AppendLine("      AND   CM_CAGE_CURRENCY_TYPE NOT IN (@pColorChips)         "); // No color chips 
        _sb.AppendLine(" GROUP BY   CM_CAGE_CURRENCY_TYPE                               ");
        _sb.AppendLine("          , CM_CURRENCY_ISO_CODE                                ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = CashierSessionInfo.CashierSessionId;

          _cmd.Parameters.Add("@pCashClosingShort", SqlDbType.Int).Value = CASHIER_MOVEMENT.CASH_CLOSING_SHORT;
          _cmd.Parameters.Add("@pCashClosingOver", SqlDbType.Int).Value = CASHIER_MOVEMENT.CASH_CLOSING_OVER;
          _cmd.Parameters.Add("@pGamingTableGameNetwin", SqlDbType.Int).Value = CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN;
          _cmd.Parameters.Add("@pGamingTableGameNetwinClose", SqlDbType.Int).Value = CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN_CLOSE;
          _cmd.Parameters.Add("@pColorChips", SqlDbType.Int).Value = CageCurrencyType.ChipsColor;
          _cmd.Parameters.Add("@pRegionalIsoCode", SqlDbType.NVarChar, 3).Value = _regional_currency_iso_code;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              using (DB_TRX _db_trx_2 = new DB_TRX())
              {
                SessionWinLossAmount += CurrencyExchange.GetExchange(_reader.GetDecimal(2), _reader.GetString(1), _regional_currency_iso_code, _db_trx_2.SqlTransaction);
              }
            }
          }
        }

        return true;

      } //try
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

     /// <summary>
    /// Get if customer has chips purchases and when 
    /// </summary>
    /// <param name="AccountId"></param>
    /// <param name="MinutesFromLastChipsSale"></param>
    /// <param name="Trx"></param>
    /// <returns>if customer has chips purchases</returns>
    public static Boolean HasCustomerChipsPurchases(Int64 AccountId, out Int32 MinutesFromLastChipsSale, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Boolean _has_customer_chips_purchases;

      _sb = new StringBuilder();

      MinutesFromLastChipsSale = Int32.MaxValue;
      _has_customer_chips_purchases = false;

      try
      {
        _sb.AppendLine("SELECT   DATEDIFF(SECOND, MAX(ao_datetime), GETDATE()) / 60 AS MINUTES ");
        _sb.AppendLine("  FROM   ACCOUNT_OPERATIONS                                            ");
        _sb.AppendLine(" WHERE   AO_CODE IN (@pChipsSale, @pChipsSaleWithRecharge)             ");
        _sb.AppendLine("   AND   AO_DATETIME >= dbo.TodayOpening(0)                            ");
        _sb.AppendLine("   AND   AO_ACCOUNT_ID = @pAccountId                                   ");
        _sb.AppendLine("   AND   AO_REDEEMABLE > 0                                             ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pChipsSale", SqlDbType.Int).Value = OperationCode.CHIPS_SALE;
          _cmd.Parameters.Add("@pChipsSaleWithRecharge", SqlDbType.Int).Value = OperationCode.CHIPS_SALE_WITH_RECHARGE;

          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              MinutesFromLastChipsSale = _reader.IsDBNull(0) ? Int32.MaxValue : _reader.GetInt32(0);
              _has_customer_chips_purchases = _reader.IsDBNull(0) ? false : true;
              return _has_customer_chips_purchases;
            }
          }
        }

      } //try
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Get gambling table 3 days, loss / win
    /// </summary>
    /// <param name="CashierSessionInfo"></param>
    /// <param name="SessionAlarmAmount"></param>
    /// <param name="SessionWinLossAmount"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean CheckForAlarm_GamblingTable3DaysLossFor(CashierSessionInfo CashierSessionInfo, Currency SessionAlarmAmount, out Currency SessionWinLossAmount, SqlTransaction Trx)
    {
      StringBuilder _sb;
      String _regional_currency_iso_code;

      _sb = new StringBuilder();
      SessionWinLossAmount = 0;
      _regional_currency_iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");

      try
      {
        _sb.AppendLine("   SELECT   CM_CAGE_CURRENCY_TYPE                               ");
        _sb.AppendLine("          , ISNULL(CM_CURRENCY_ISO_CODE, @pRegionalIsoCode)     ");
        _sb.AppendLine("          , SUM(CM_ADD_AMOUNT - CM_SUB_AMOUNT)                  ");
        _sb.AppendLine("     FROM   CASHIER_MOVEMENTS  WITH (INDEX (IX_cm_date_type))   ");
        _sb.AppendLine(" WHERE   CM_SESSION_ID IN (SELECT   DISTINCT TOP 3 CM_SESSION_ID                       "); // OPENING 3 SESSIONS AGO
        _sb.AppendLine("                             FROM   CASHIER_MOVEMENTS  WITH (INDEX (IX_cm_date_type))  "); // OPENING 3 DAYS AGO
        _sb.AppendLine("                            WHERE   CM_CASHIER_ID = @pCashierId                        "); // OPENING 3 DAYS AGO
        _sb.AppendLine("                         ORDER BY   CM_SESSION_ID DESC)                                "); // OPENING 3 DAYS AGO
        _sb.AppendLine("      AND   CM_TYPE IN (@pCashClosingShort,                     "); // CASH_CLOSING_SHORT            
        _sb.AppendLine("                        @pCashClosingOver,                      "); // CASH_CLOSING_OVER             
        _sb.AppendLine("                        @pGamingTableGameNetwin,                "); // GAMING_TABLE_GAME_NETWIN      
        _sb.AppendLine("                        @pGamingTableGameNetwinClose)           "); // GAMING_TABLE_GAME_NETWIN_CLOSE
        _sb.AppendLine("      AND   CM_CASHIER_ID = @pCashierId                         ");
        _sb.AppendLine("      AND   CM_CAGE_CURRENCY_TYPE NOT IN (@pColorChips)         "); // No color chips 
        _sb.AppendLine(" GROUP BY   CM_CAGE_CURRENCY_TYPE                               ");
        _sb.AppendLine("          , CM_CURRENCY_ISO_CODE                                ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pCashierId", SqlDbType.BigInt).Value = CashierSessionInfo.TerminalId;

          _cmd.Parameters.Add("@pCashClosingShort", SqlDbType.Int).Value = CASHIER_MOVEMENT.CASH_CLOSING_SHORT;
          _cmd.Parameters.Add("@pCashClosingOver", SqlDbType.Int).Value = CASHIER_MOVEMENT.CASH_CLOSING_OVER;
          _cmd.Parameters.Add("@pGamingTableGameNetwin", SqlDbType.Int).Value = CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN;
          _cmd.Parameters.Add("@pGamingTableGameNetwinClose", SqlDbType.Int).Value = CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN_CLOSE;
          _cmd.Parameters.Add("@pColorChips", SqlDbType.Int).Value = CageCurrencyType.ChipsColor;
          _cmd.Parameters.Add("@pRegionalIsoCode", SqlDbType.NVarChar, 3).Value = _regional_currency_iso_code;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              using (DB_TRX _db_trx_2 = new DB_TRX())
              {
                SessionWinLossAmount += CurrencyExchange.GetExchange(_reader.GetDecimal(2), _reader.GetString(1), _regional_currency_iso_code, _db_trx_2.SqlTransaction);
              }
            }
          }
        }

        return true;

      } //try
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Get variation of customer bet per session in a given period general param.
    /// </summary>
    /// <returns></returns>
    public static Decimal GetBetsVariationCustomerPerSession()
    {
      return GeneralParam.GetDecimal("Alarms.GamingTables", "BetsVariation.CustomerPerSession", 0);
    }


    /// <summary>
    /// Get player sitting time general param
    /// </summary>
    /// <returns></returns>
    public static Int32 GetPlayerSittingTime()
    {
      return GeneralParam.GetInt32("Alarms.GamingTables", "PlayerSittingTimeMinutes", 0);
    }

    #endregion

    #region Private
    #endregion

  }
}
