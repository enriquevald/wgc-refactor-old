//-------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   BankMobile.cs
// DESCRIPTION:   Bank mobile operations
// AUTHOR:        Marcos Piedra Osuna
// CREATION DATE: 22-FEB-2013
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 22-FEB-2013  MPO    Initial version
// 22-FEB-2013  ICS    Moved from CashierBusinessLogic.cs
//                     - MBCardData
//                     - DB_IsClosedSesionMB
//                     - DB_MBBalanceLost
//                     - The body of DB_InsertMBCardMovement
//                     - The body of DB_MBCloseCashSession
//                     - The body of UpdateSessionBalance
//                     - The body of DB_MBCardGetAllData
//                     - The body of DB_MB_SetNewSalesLimit
//                     - Added new function to close all mobile sessions 
//                       of a cashier session
// 22-NOV-2013  SMN    Call SP CashierMovementsHistory when insert a new DB_InsertMBCardMovement
// 10-FEB-2014  RCI & RMS    The historicize by hour will be made in a separate thread
// 06-MAR-2014  RCI    Fixed Bug WIG-700: Close MB Session while a MB recharge is begin made
// 03-OCT-2014  SGB    Modified call to VoucherMBCardDepositCash for new footer voucher
// 04-NOV-2014  JBC    Fixed Bug WIG-1635: MB Close Session Amount Excess
// 27-NOV-2014  JBC    Fixed Bug WIG-1762: Close MB Session
// 09-DEC-2014  JBC    Fixed Bug WIG-1817: Close MB Session in a new cashier session.
// 18-DEC-2014  JPJ    Fixed Bug WIG-1869: While closing MB and parameter reopen is 1.
// 23-DEC-2014  JBC    Fixed Bug WIG-1865: Error closing BM Session
// 23-DEC-2014  JBC    Fixed Bug WIG-1895: Error closing BM Session
// 05-ENE-2014  JBC    DB_InsertMBCardMovement now return true if transaction ok/false if transaction fails
// 21-JAN-2015  JPJ    Added new operation on mobile bank deposit
// 10-APR-2015  YNM    Fixed Bug WIG-2126: Error OnClose Cashier Session when MB has Shortfall or OverCash and ReopenMode=1.
// 28-APR-2015  YNM    Fixed Bug WIG-2252: System allows recharged with closed session
// 21-MAR-2016  DHA    Product Backlog Item 10051: add CageCurrencyType to historification
// 30-AUG-2017  DPC    [WIGOS-4779]: [Ticket #8418]Issue: Reports of Missing and surplus BM (Cancellation Deposits) V03.06.0023
// 19-GEN-2018  XGJ    WIGOS-6671 MobiBank: GUI management - Mobibank edition: Lock
// 09-MAY-2018  FJC    WIGOS-10352 Cancel Request button is display when recharge was voided
// 31-MAY-2018  JML    Fixed Bug 32858:WIGOS-12369 [Ticket #14610] Fallo - Error al Anular Dep�sitos en Caja (Fillbank)
// 09-MAY-2018  FJC    WIGOS-10352 Cancel Request button is display when recharge was voided
// -------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;

namespace WSI.Common
{
  public enum MobileBankLinkDeviceMode
  {
    CardMode = 0,
    DeviceWithLCDInTouch = 1,
    DeviceWithoutLCDInTouch = 2,
  }

  public enum MobileBankDataInput
  {
    CardMode = 0,
    UserId = 1,
  }

  public enum MobileBankStatusRequestRechare
  {
    None = 0,
    New = 1,
    InProgress = 2,
    Void = 3,
    Finished = 4,
    FinishedInEgm = 5
  }

  public enum MobileBankStatusCancelReason
  {
    None = 0,
    CancelledByUser_Request = 1,
    CancelledByUser_CardOut = 2,
    CancelledByRunner = 3
  }

  public enum MobileBankStatusExpiredReason
  {
    None = 0,
    Expired = 1
  }

  public enum MobileBankUserType
  {
    MobileBankCard = 0,
    UserDevice = 1
  }

  public enum MobileBankRechargeType
  {
    None = 0,
    RechargeInEgm = 1,
    RechargeInAccount = 2
  }

  public enum MobileBankRechargeintoEGMStatus
  {
    None = 0,
    RechargeInEgmOK = 1,
    RechargeInEgmERROR = 2
  }

  public static class MobileBankConfig
  {
    private static readonly String GP_GROUP_KEY_MOBILE_BANK_LINK_DEVICE = "Movibank";
    private static readonly String GP_SUBJECT_MOBILE_BANK_LINK_DEVICE = "Device";
    private static readonly MobileBankLinkDeviceMode DEFAULT_LINK_DEVICE_MODE = MobileBankLinkDeviceMode.CardMode;
    public static readonly AccountType MOBILE_BANK_NEW_ACCOUNT_TYPE = AccountType.ACCOUNT_UNKNOWN;
    private static readonly String VirtualCardTrackdataToShow = "---";

    #region "Public Methods"
    public static MobileBankLinkDeviceMode MobileBankLinkDeviceMode
    {
      get
      {
        return GetInternalMobileBankLinkDeviceMode();
      }
    }

    public static Boolean IsMobileBankLinkedToADevice
    {
      get
      {
        MobileBankLinkDeviceMode _mode_link;
        _mode_link = GetInternalMobileBankLinkDeviceMode();

        return (_mode_link == MobileBankLinkDeviceMode.DeviceWithLCDInTouch ||
                _mode_link == MobileBankLinkDeviceMode.DeviceWithoutLCDInTouch);
      }
    }

    /// <summary>
    /// Get trackdata to show from virtual card
    /// </summary>
    /// <returns></returns>
    public static String GetVirtualCardTrackdataToShow()
    {
      return VirtualCardTrackdataToShow;
    }

    #endregion

    #region "Private Methods"
    private static MobileBankLinkDeviceMode GetInternalMobileBankLinkDeviceMode()
    {
      MobileBankLinkDeviceMode _validation_mode;
      Int32 _link_validation_mode;

      try
      {
        _link_validation_mode = GeneralParam.GetInt32(GP_GROUP_KEY_MOBILE_BANK_LINK_DEVICE,
                                                      GP_SUBJECT_MOBILE_BANK_LINK_DEVICE,
                                                      (int)DEFAULT_LINK_DEVICE_MODE);
        _validation_mode = (MobileBankLinkDeviceMode)_link_validation_mode;
      }
      catch (Exception)
      {
        _validation_mode = MobileBankLinkDeviceMode.CardMode;
      }

      return _validation_mode;
    }
    #endregion
  }

  public static class MobileBank
  {
    #region Class Atributes

    public static ReopenMode m_reopen_mode;

    #endregion

    //------------------------------------------------------------------------------
    // PURPOSE: Insert MB card movement into MB_movements table
    // 
    //  PARAMS:
    //      - INPUT:
    //          - MBMovementInfo
    //          - SqlTrx
    //
    //      - OUTPUT:
    //          - MovementId
    //
    // RETURNS:
    //      - True
    //      - False
    // 
    //   NOTES:
    //    
    public static Boolean DB_InsertMBCardMovement(MBMovementData MBMovementInfo,
                                                  out Int64 MovementId,
                                                  SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int32 _num_rows_inserted;
      SqlParameter _out_param;

      MovementId = 0;

      _num_rows_inserted = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" DECLARE @_datetime AS DATETIME    ");
        _sb.AppendLine(" SET @_datetime = GETDATE()        ");

        _sb.AppendLine(" INSERT INTO MB_MOVEMENTS          ");
        _sb.AppendLine("     (                             ");
        _sb.AppendLine("       MBM_TYPE                    ");
        _sb.AppendLine("     , MBM_DATETIME                ");
        _sb.AppendLine("     , MBM_CASHIER_SESSION_ID      ");
        _sb.AppendLine("     , MBM_MB_ID                   ");
        _sb.AppendLine("     , MBM_CASHIER_ID              ");
        _sb.AppendLine("     , MBM_CASHIER_NAME            ");
        _sb.AppendLine("     , MBM_INITIAL_BALANCE         ");
        _sb.AppendLine("     , MBM_ADD_AMOUNT              ");
        _sb.AppendLine("     , MBM_SUB_AMOUNT              ");
        _sb.AppendLine("     , MBM_FINAL_BALANCE           ");
        _sb.AppendLine("     , MBM_OPERATION_ID            ");
        _sb.AppendLine("     , MBM_AMOUNT_01               ");
        _sb.AppendLine("     , MBM_AMOUNT_02               ");
        _sb.AppendLine("     )                             ");
        _sb.AppendLine("VALUES                             ");
        _sb.AppendLine("     (                             ");
        _sb.AppendLine("       @p1, @_datetime, @p2, @p3   ");
        _sb.AppendLine("     , @p4, @p5, @p6, @p7, @p8     ");
        _sb.AppendLine("     , @p9, @p10, @p11, @p12       ");
        _sb.AppendLine("     )                             ");

        _sb.AppendLine("SET @p13 = SCOPE_IDENTITY()        ");
          // RCI & RMS 10-FEB-2014: The historicize by hour will be made in a separate thread.
          //                        The movements to historicize are in table CASHIER_MOVEMENTS_PENDING_HISTORY.
        _sb.AppendLine("EXEC dbo.CashierMovementsHistory @p2, @p13, @p1, 1, 0, @p7, @p8, 0, @pCurrencyCode, 0, 0, 0  ");

        using (SqlCommand sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
        sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "MBM_TYPE").Value = (Int32)MBMovementInfo.Type;
        sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "MBM_CASHIER_SESSION_ID").Value = MBMovementInfo.CashierSessionId;
        sql_command.Parameters.Add("@p3", SqlDbType.BigInt, 8, "MBM_MB_ID").Value = MBMovementInfo.CardId;
        sql_command.Parameters.Add("@p4", SqlDbType.Int, 4, "MBM_CASHIER_ID").Value = MBMovementInfo.CashierId;

        if (String.IsNullOrEmpty(MBMovementInfo.CashierName))
        {
          sql_command.Parameters.Add("@p5", SqlDbType.NVarChar, 50, "MBM_CASHIER_NAME").Value = DBNull.Value;
        }
        else
        {
          sql_command.Parameters.Add("@p5", SqlDbType.NVarChar, 50, "MBM_CASHIER_NAME").Value = MBMovementInfo.CashierName; // cashier_name;
        }

        sql_command.Parameters.Add("@p6", SqlDbType.Money, 8, "MBM_INITIAL_BALANCE").Value = MBMovementInfo.InitialBalance.SqlMoney;
        sql_command.Parameters.Add("@p7", SqlDbType.Money, 8, "MBM_ADD_AMOUNT").Value = MBMovementInfo.AddAmount.SqlMoney;
        sql_command.Parameters.Add("@p8", SqlDbType.Money, 8, "MBM_SUB_AMOUNT").Value = MBMovementInfo.SubAmount.SqlMoney;
        sql_command.Parameters.Add("@p9", SqlDbType.Money, 8, "MBM_FINAL_BALANCE").Value = MBMovementInfo.FinalBalance.SqlMoney;

        if (MBMovementInfo.OperationId == 0)
        {
          sql_command.Parameters.Add("@p10", SqlDbType.BigInt, 8, "MBM_OPERATION_ID").Value = DBNull.Value;
        }
        else
        {
            sql_command.Parameters.Add("@p10", SqlDbType.BigInt, 8, "MBM_OPERATION_ID").Value = MBMovementInfo.OperationId; // Operation from accounts_operation;
        }

        if (MBMovementInfo.Amount_01 == 0)
        {
          sql_command.Parameters.Add("@p11", SqlDbType.Money, 8, "MBM_AMOUNT_01").Value = DBNull.Value;
        }
        else
        {
            sql_command.Parameters.Add("@p11", SqlDbType.Money, 8, "MBM_AMOUNT_01").Value = MBMovementInfo.Amount_01.SqlMoney; // Operation from accounts_operation;
        }

        if (MBMovementInfo.Amount_02 == 0)
        {
          sql_command.Parameters.Add("@p12", SqlDbType.Money, 8, "MBM_AMOUNT_02").Value = DBNull.Value;
        }
        else
        {
            sql_command.Parameters.Add("@p12", SqlDbType.Money, 8, "MBM_AMOUNT_02").Value = MBMovementInfo.Amount_02.SqlMoney; // Operation from accounts_operation;
        }

        sql_command.Parameters.Add("@pCurrencyCode", SqlDbType.NVarChar).Value = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");

          _out_param = sql_command.Parameters.Add("@p13", SqlDbType.BigInt, 8, "MBM_MOVEMENT_ID");
          _out_param.Direction = ParameterDirection.Output;

          //Get results
          _num_rows_inserted = sql_command.ExecuteNonQuery();
        }

        MovementId = (Int64)_out_param.Value;

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      if (_num_rows_inserted < 1)
      {
        Log.Error("DB_InsertMBCardMovement. Movement not inserted.");
      }

      return false;
    } // DB_InsertMBCardMovement

    //------------------------------------------------------------------------------
    // PURPOSE: Sets a new Sales Limit for a MB
    // 
    //  PARAMS:
    //      - INPUT:
    //          - MBCard
    //          - Operation
    //          - AmountToAdd
    //          - Trx
    //          - SessionId
    //          - Session
    //
    //      - OUTPUT:
    //          - SalesLimitReached
    //          - RemainingSalesLimit
    //          - MBSalesLimit
    //
    // RETURNS:
    //      - True
    //      - False
    //
    public static Boolean DB_MB_SetNewSalesLimit(MBCardData MBCard,
                                             MB_SALES_LIMIT_OPERATION Operation,
                                             Currency AmountToAdd,
                                             CashierSessionInfo Session,
                                             Int32 CashierUserId,
                                             out Boolean SalesLimitReached,
                                             out Currency RemainingSalesLimit,
                                             out Currency MBSalesLimit,
                                             SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int64 _mov_id;

      Int32 _gp_value;                         // GP 
      Boolean _sales_limit_enabled;            // GP Flag: Sales Limit is Enabled
      Boolean _auto_increase;                  // GP Flag: Auto Increase MB Limit on Deposits

      Currency _cs_remaining_sales_limit;      // CS Remain Credit to deal on all MB with same Cashier Session
      Currency _mb_sales_limit_extension;      // MB Increase for Manual Extensions Counter: MOBILE_BANKS.MB_EXTENSION

      CashierMovementsTable _cashier_movements;
      MBMovementData _mb_movement_info;

      _cs_remaining_sales_limit = 0;
      _mb_sales_limit_extension = 0;
      _auto_increase = false;

      SalesLimitReached = false;
      RemainingSalesLimit = 0;
      MBSalesLimit = 0;

      // Steps:
      // 1. Update card data.
      // 2. Insert movements.
      // 3. Insert voucher.            

      try
      {
        if (!Int32.TryParse(GeneralParam.Value("MobileBank", "SalesLimit.Enabled"), out _gp_value))
        {
          _gp_value = 0;
        }

        _sales_limit_enabled = (_gp_value == 1);

        _cs_remaining_sales_limit = Decimal.MaxValue;
        if (_sales_limit_enabled)
        {
          // Read Sales Limit and Remain Sale Credit. 
          if (!Cashier.GetRemainingSalesLimit(CashierUserId, Session.CashierSessionId, Cashier.SalesLimitType.MobileBank,
                                                         out _cs_remaining_sales_limit))
          {
            Log.Error("DB_MB_SetNewSalesLimit. Error reading Cashier Sesion Limit Sales. Mobile Bank: " + MBCard.CardId.ToString());

            return false;
          }
        }

        // Get Limit Extension depending if mobile banks sales are limited 
        switch (Operation)
        {
          case MB_SALES_LIMIT_OPERATION.EXTEND_LIMIT:
            if (AmountToAdd > _cs_remaining_sales_limit)
            {
              SalesLimitReached = true;
              RemainingSalesLimit = _cs_remaining_sales_limit;
              MBSalesLimit = MBCard.SalesLimit;

              return true;
            }

            _mb_sales_limit_extension = Math.Min(AmountToAdd, _cs_remaining_sales_limit);
            MBSalesLimit = MBCard.SalesLimit + _mb_sales_limit_extension;

            if (_sales_limit_enabled)
            {
              RemainingSalesLimit = _cs_remaining_sales_limit - _mb_sales_limit_extension;
            }
            else
            {
              RemainingSalesLimit = Decimal.MaxValue;
            }

            break;

          case MB_SALES_LIMIT_OPERATION.CHANGE_LIMIT:
            if (AmountToAdd - MBCard.SalesLimit > _cs_remaining_sales_limit)
            {
              SalesLimitReached = true;
              RemainingSalesLimit = _cs_remaining_sales_limit + MBCard.SalesLimit;
              MBSalesLimit = MBCard.SalesLimit;

              return true;
            }

            _mb_sales_limit_extension = AmountToAdd - MBCard.SalesLimit;
            MBSalesLimit = AmountToAdd;

            if (_sales_limit_enabled)
            {
              RemainingSalesLimit = _cs_remaining_sales_limit - _mb_sales_limit_extension;
            }
            else
            {
              RemainingSalesLimit = Decimal.MaxValue;
            }

            break;

          case MB_SALES_LIMIT_OPERATION.DEPOSIT:
            if (!Int32.TryParse(GeneralParam.Value("MobileBank", "AutomaticRenewLimitAfterDeposit"), out _gp_value))
            {
              _gp_value = 1;
            }
            _auto_increase = (_gp_value == 1);

            if (!_auto_increase)
            {
              MBSalesLimit = MBCard.SalesLimit;

              return true;
            }

            AmountToAdd = Math.Min(AmountToAdd, _cs_remaining_sales_limit);
            _mb_sales_limit_extension = 0;
            MBSalesLimit = MBCard.SalesLimit + AmountToAdd;

            if (_sales_limit_enabled)
            {
              RemainingSalesLimit = _cs_remaining_sales_limit - AmountToAdd;
            }
            else
            {
              RemainingSalesLimit = Decimal.MaxValue;
            }

            // If there are no MB Limit Extension, return;
            if (AmountToAdd == 0)
            {
              return true;
            }

            break;

          case MB_SALES_LIMIT_OPERATION.CLOSE_SESSION:
            MBSalesLimit = 0;
            _mb_sales_limit_extension = 0;

            break;

          default:
            Log.Error("Unexpected Operation: " + Operation.ToString());

            return false;
        } // switch (Operation)

        if (Operation != MB_SALES_LIMIT_OPERATION.CLOSE_SESSION)
        {
          //Linking cashier session with MB session
          if (!DB_MBLinkCashierSession(Session.CashierSessionId, MBCard.CardId, SqlTrx))
          {
            Log.Error("DB_MB_SetNewSalesLimit. Card not updated. Mobile Bank: " + MBCard.CardId.ToString());

            return false;
          }

          _sb = new StringBuilder();
          _sb.AppendLine("UPDATE   MOBILE_BANKS                                                               ");
          _sb.AppendLine("   SET   MB_BALANCE =  @pNewSalesLimit                                              ");
          _sb.AppendLine("       , MB_INITIAL_LIMIT = (CASE WHEN (MB_CASHIER_SESSION_ID IS NULL)              ");
          _sb.AppendLine("                                    THEN @pAmountToAdd                              ");
          _sb.AppendLine("                                    ELSE MB_INITIAL_LIMIT                           ");
          _sb.AppendLine("                             END)                                                   ");
          _sb.AppendLine("       , MB_EXTENSION = MB_EXTENSION + (CASE WHEN (MB_CASHIER_SESSION_ID IS NULL)   ");
          _sb.AppendLine("                                               THEN 0                               ");
          _sb.AppendLine("                                               ELSE @pExtension                     ");
          _sb.AppendLine("                                        END)                                        ");
          _sb.AppendLine("       , MB_LAST_ACTIVITY = GETDATE()                                               ");
          _sb.AppendLine("       , MB_SESSION_STATUS = (CASE WHEN (MB_SESSION_STATUS = 0)                     ");
          _sb.AppendLine("                                     THEN 1                                         ");
          _sb.AppendLine("                                     ELSE MB_SESSION_STATUS                         ");
          _sb.AppendLine("                              END)                                                  ");
          _sb.AppendLine(" WHERE   MB_ACCOUNT_ID = @pCardId                                                   ");

          if (MobileBank.m_reopen_mode == ReopenMode.NoReopen || MobileBank.m_reopen_mode == ReopenMode.ReopenInThisSession)
          {
            _sb.AppendLine("   AND   ((MB_CASHIER_SESSION_ID = @pCSId)  OR  (MB_CASHIER_SESSION_ID IS NULL))    ");
          }

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _cmd.Parameters.Add("@pNewSalesLimit", SqlDbType.Money).Value = MBSalesLimit.SqlMoney;
            _cmd.Parameters.Add("@pAmountToAdd", SqlDbType.Money).Value = AmountToAdd.SqlMoney;
            _cmd.Parameters.Add("@pCSId", SqlDbType.BigInt).Value = Session.CashierSessionId;
            _cmd.Parameters.Add("@pCardId", SqlDbType.BigInt).Value = MBCard.CardId;
            _cmd.Parameters.Add("@pExtension", SqlDbType.Money).Value = _mb_sales_limit_extension.SqlMoney;

            if (_cmd.ExecuteNonQuery() != 1)
            {
              Log.Error("DB_MB_SetNewSalesLimit. Card not updated. Mobile Bank: " + MBCard.CardId.ToString());

              return false;
            }
          }
        }

        // 2. Insert movements.
        _cashier_movements = new CashierMovementsTable(Session);
        _mb_movement_info = new MBMovementData();
        switch (Operation)
        {
          case MB_SALES_LIMIT_OPERATION.EXTEND_LIMIT:
            _mb_movement_info.CashierId = Session.TerminalId;
            _mb_movement_info.CardId = MBCard.CardId;
            _mb_movement_info.Type = MBMovementType.ManualChangeLimit;
            _mb_movement_info.CashierSessionId = Session.CashierSessionId;
            _mb_movement_info.InitialBalance = MBCard.SalesLimit;
            _mb_movement_info.SubAmount = 0;
            _mb_movement_info.AddAmount = AmountToAdd;
            _mb_movement_info.FinalBalance = MBSalesLimit;
            _mb_movement_info.CashierName = Session.TerminalName;
            _mb_movement_info.OperationId = 0;

            if (!DB_InsertMBCardMovement(_mb_movement_info, out _mov_id, SqlTrx))
            {
              Log.Error("DB_MB_SetNewSalesLimit. Card not updated. Mobile Bank: " + MBCard.CardId.ToString());

              return false;
            }

            //CreateCashierMovement(Trx, CASHIER_MOVEMENT.MB_MANUAL_CHANGE_LIMIT, MBSalesLimit - MBCard.SalesLimit,
            //                      SessionId, MBCard.TrackData, MBCard.CardId, _mov_id);
            _cashier_movements.Add(0, CASHIER_MOVEMENT.MB_MANUAL_CHANGE_LIMIT, MBSalesLimit - MBCard.SalesLimit,
                                    MBCard.CardId, MBCard.TrackData);

            break;

          case MB_SALES_LIMIT_OPERATION.CHANGE_LIMIT:
            _mb_movement_info.CashierId = Session.TerminalId;
            _mb_movement_info.CardId = MBCard.CardId;
            _mb_movement_info.Type = MBMovementType.ManualChangeLimit;
            _mb_movement_info.CashierSessionId = Session.CashierSessionId;
            _mb_movement_info.InitialBalance = MBCard.SalesLimit;
            _mb_movement_info.SubAmount = MBCard.SalesLimit;
            _mb_movement_info.AddAmount = MBSalesLimit;
            _mb_movement_info.FinalBalance = MBSalesLimit;
            _mb_movement_info.CashierName = Session.TerminalName;
            _mb_movement_info.OperationId = 0;

            if (!DB_InsertMBCardMovement(_mb_movement_info, out _mov_id, SqlTrx))
            {
              Log.Error("DB_MB_SetNewSalesLimit. Card not updated. Mobile Bank: " + MBCard.CardId.ToString());

              return false;
            }

            //CreateCashierMovement(Trx, CASHIER_MOVEMENT.MB_MANUAL_CHANGE_LIMIT, MBSalesLimit - MBCard.SalesLimit,
            //                      Cashier.SessionId, MBCard.TrackData, MBCard.CardId, _mov_id);
            _cashier_movements.Add(0, CASHIER_MOVEMENT.MB_MANUAL_CHANGE_LIMIT, MBSalesLimit - MBCard.SalesLimit,
                                   MBCard.CardId, MBCard.TrackData);

            break;

          case MB_SALES_LIMIT_OPERATION.DEPOSIT:
            // This case insert outside function, after deposit movement
            //DB_InsertMBCardMovement(Cashier.TerminalId, MBCard.CardId, MBMovementType.AutomaticChangeLimit, Cashier.SessionId,
            //                        MBCard.SalesLimit, 0, AmountToAdd, MBSalesLimit, out _mov_id, Trx);

            break;

          case MB_SALES_LIMIT_OPERATION.CLOSE_SESSION:
            // This case no insert Sales Limit Movement
            //DB_InsertMBCardMovement(Cashier.TerminalId, MBCard.CardId, MBMovementType.AutomaticChangeLimit, Cashier.SessionId,
            //                        MBCard.SalesLimit, MBCard.SalesLimit, 0, 0, out _mov_id, Trx);
            //CreateCashierMovement(Trx, CASHIER_MOVEMENT.MB_MANUAL_CHANGE_LIMIT, MBSalesLimit - MBCard.SalesLimit,
            //                      Cashier.SessionId, MBCard.TrackData, MBCard.CardId, _mov_id);
            break;
        }

        return (_cashier_movements.Save(SqlTrx));
        }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }  // DB_MB_SetNewSalesLimit

    //------------------------------------------------------------------------------
    // PURPOSE: Link the current session of cashier with a MB
    // 
    //  PARAMS:
    //      - INPUT:
    //          - MBCard
    //          - SessionId
    //          - Trx
    //
    // RETURNS:
    //      - true: operation completed successfully
    //      - false: operation failed
    // 
    public static Boolean DB_MBLinkCashierSession(Int64 CashierUserId, Int64 CardId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   MOBILE_BANKS                   ");
        _sb.AppendLine("    SET   MB_CASHIER_SESSION_ID = @pCSId ");
        _sb.AppendLine("  WHERE   MB_ACCOUNT_ID = @pCardId       ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCSId", SqlDbType.BigInt).Value = CashierUserId;
          _cmd.Parameters.Add("@pCardId", SqlDbType.BigInt).Value = CardId;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Error("DB_MBLinkCashierSession. Card not updated. Mobile Bank: " + CardId.ToString());

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("DB_MBLinkCashierSession. Card not updated. Mobile Bank: " + CardId.ToString());
        Log.Exception(_ex);
      }

        return false;
    } //DB_MBLinkCashierSession

    //------------------------------------------------------------------------------
    // PURPOSE: Close Cash Session of MB on Cashier
    // 
    //  PARAMS:
    //      - INPUT:
    //          - MBCard
    //          - Session
    //          - SessionId
    //          - Trx
    //
    //      - OUTPUT:
    //          - Voucher
    //
    // RETURNS:
    //      - true: operation completed successfully
    //      - false: operation failed
    // 
    //   NOTES:
    //   CurrentCashierSessionId is set to NULL:
    //        When cashier session is closed and Reopen Mode is "No Reopen" or "Reopen in this session"
    //        When mobile bank session is closed and Reopen Mode is "Reopen in other session" or "Reopen always" 
    //   When mobile bank session is closed and Reopen Mode is "Reopen in other session" or "Reopen always" all other MB card session data is also set to 0.
    //    
    public static Boolean DB_MBCloseCashSession(MBCardData MBCard,
                                                CashierSessionInfo Session,
                                                Int32 CashierUserId,
                                                out VoucherMBCardDepositCash Voucher,
                                                Boolean CashierClosing,
                                                SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      Int32 _num_rows;
      Boolean _voucher_saved;
      Currency _new_mb_sales_limit;
      Boolean _dummy_sales_limit_reached;
      Currency _dummy_remaining_sales_limit;
      CashierMovementsTable _cashier_movements;
      Currency _total_pending_cash;
      MBMovementData _mb_movement_info;

      _voucher_saved = false;
      Voucher = null;

      _sb = new StringBuilder();

      // Set 0 Sales Limit
      _sb.AppendLine("UPDATE   MOBILE_BANKS ");
      _sb.AppendLine("   SET   MB_PENDING_CASH               = 0     ");
      _sb.AppendLine("       , MB_OVER_CASH_SESSION          = 0     ");
      _sb.AppendLine("       , MB_SESSION_STATUS             = 0     ");
      _sb.AppendLine("       , MB_SHORTFALL_CASH_SESSION     = 0     ");

      if (!CashierClosing && (m_reopen_mode == ReopenMode.ReopenInThisSession) && (MBCard.PendingCash > 0))
      {
        MBCard.ShortFallCash = MBCard.ShortFallCash + MBCard.PendingCash;
        _sb.AppendLine("       , MB_SHORTFALL_CASH             = @pSFC ");
      }

      if (CashierClosing || (m_reopen_mode == ReopenMode.ReopenInOtherSession || m_reopen_mode == ReopenMode.ReopenAlways))
      {
        // & delete all balance from de last session
        _sb.AppendLine("       , MB_BALANCE                    = 0     ");
        _sb.AppendLine("       , MB_INITIAL_LIMIT              = 0     ");
        _sb.AppendLine("       , MB_CASH_IN                    = 0     ");
        _sb.AppendLine("       , MB_DEPOSIT                    = 0     ");
        _sb.AppendLine("       , MB_OVER_CASH                  = 0     ");
        _sb.AppendLine("       , MB_EXTENSION                  = 0     ");
        _sb.AppendLine("       , MB_SHORTFALL_CASH             = 0     ");
        _sb.AppendLine("       , MB_ACTUAL_NUMBER_OF_RECHARGES = 0     ");
        _sb.AppendLine("       , MB_CASHIER_SESSION_ID         = 0     ");

        if (CashierClosing)
        {
          _sb.AppendLine("       , MB_LAST_ACTIVITY              = NULL  ");
          _sb.AppendLine("       , MB_LAST_TERMINAL_ID           = NULL  ");
          _sb.AppendLine("       , MB_LAST_TERMINAL_NAME         = NULL  ");
        }
      }

      _sb.AppendLine(" WHERE   MB_ACCOUNT_ID            =  @pCardId               ");
      _sb.AppendLine("   AND   MB_CASHIER_SESSION_ID    =  @pCSId                 ");
      _sb.AppendLine("   AND   MB_PENDING_CASH          =  @pPreviousPendingCash  ");

      using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
      _sql_command.Parameters.Add("@pCardId", SqlDbType.BigInt).Value = MBCard.CardId;
      _sql_command.Parameters.Add("@pCSId", SqlDbType.BigInt).Value = Session.CashierSessionId;
      _sql_command.Parameters.Add("@pPreviousPendingCash", SqlDbType.Money).Value = MBCard.PendingCash.SqlMoney;
      _sql_command.Parameters.Add("@pSFC", SqlDbType.Money).Value = MBCard.ShortFallCash.SqlMoney;

      _num_rows = _sql_command.ExecuteNonQuery();
      }

      if (_num_rows != 1)
      {
        Log.Error("DB_MBCardCloseCashSession. MBCard not updated. Mobile Bank: " + MBCard.CardId);

        return false;
      }

      if (MBCard.SessionClosed)
      {
        return true;
      }

      // Insert GUI_AUDIT if delivered Cash Mismatch
      if (MBCard.PendingCash != 0 || MBCard.ShortFallCashSession != 0)
      {
        String _source_name;
        String _description;
        String _mb_name;
        Currency _pending_cash;

        if (String.IsNullOrEmpty(MBCard.HolderName))
        {
          _source_name = MBCard.CardId.ToString() + "@" + Session.TerminalName;
          _mb_name = MBCard.CardId.ToString();
        }
        else
        {
          _source_name = MBCard.HolderName + "@" + Session.TerminalName;
          _mb_name = MBCard.CardId.ToString() + "-" + MBCard.HolderName;
        }

        if (MBCard.PendingCash != 0)
        {
          _pending_cash = MBCard.PendingCash;
        }
        else
        {
          _pending_cash = MBCard.ShortFallCashSession;
        }

        _description = Resource.String(Alarm.ResourceId(AlarmCode.User_WrongDelivered), (-_pending_cash).ToString());
        _description = _description + ", " + Resource.String("STR_FORMAT_GENERAL_NAME_MB") + ": " + _mb_name;

        //Recording the alarm using the Alarm.Register
        Alarm.Register(AlarmSourceCode.User, Session.UserId, _source_name, (UInt32)AlarmCode.User_WrongDelivered,
                       _description, AlarmSeverity.Warning, DateTime.MinValue, SqlTrx);
      }

      //
      // Insert Movements Cashier and Mobile Bank
      //
      Int64 movement_id = 0;

      // Insert shortfall movement with a current value of pending.
      if (GP_GetRegisterShortfallOnDeposit())
      {
        if (MBCard.PendingCash > 0)
        {
          _mb_movement_info = new MBMovementData();
          _mb_movement_info.CashierId = Session.TerminalId;
          _mb_movement_info.CardId = MBCard.CardId;
          _mb_movement_info.Type = MBMovementType.CashShortFall;
          _mb_movement_info.CashierSessionId = Session.CashierSessionId;
          _mb_movement_info.InitialBalance = MBCard.SalesLimit;
          _mb_movement_info.SubAmount = MBCard.PendingCash;
          _mb_movement_info.AddAmount = 0;
          _mb_movement_info.FinalBalance = MBCard.SalesLimit;
          _mb_movement_info.CashierName = Session.TerminalName;
          _mb_movement_info.OperationId = 0;

          if (!DB_InsertMBCardMovement(_mb_movement_info, out movement_id, SqlTrx))
          {
            Log.Error("DB_MBCloseCashSession. MBMovement not inserted. Mobile Bank: " + MBCard.CardId.ToString());

            return false;
          }
        }
      }

      // Current Pending Cash + Partial Pending Cash
      _total_pending_cash = MBCard.ShortFallCashSession + MBCard.PendingCash;

      // Insert Balance Mismatch if remain Pending Deposits 
      if (_total_pending_cash != 0)
      {
        _mb_movement_info = new MBMovementData();
        _mb_movement_info.CashierId = Session.TerminalId;
        _mb_movement_info.CardId = MBCard.CardId;
        _mb_movement_info.Type = MBMovementType.CloseSessionWithCashLost;
        _mb_movement_info.CashierSessionId = Session.CashierSessionId;
        _mb_movement_info.InitialBalance = 0;
        _mb_movement_info.SubAmount = _total_pending_cash;
        _mb_movement_info.AddAmount = 0;
        _mb_movement_info.FinalBalance = 0;
        _mb_movement_info.CashierName = Session.TerminalName;
        _mb_movement_info.OperationId = 0;

        if (!DB_InsertMBCardMovement(_mb_movement_info, out movement_id, SqlTrx))
        {
          Log.Error("DB_MBCloseCashSession. MBMovement not inserted. Mobile Bank: " + MBCard.CardId.ToString());

          return false;
        }

        _cashier_movements = new CashierMovementsTable(Session);
        _cashier_movements.Add(0, CASHIER_MOVEMENT.MB_CASH_LOST, _total_pending_cash, MBCard.CardId, MBCard.TrackData);

        if (!_cashier_movements.Save(SqlTrx))
        {
          return false;
        }
      }

      // Insert overcash close session
      if (MBCard.OverCashSession != 0)
      {
        String _source_name;
        String _description;
        String _mb_name;

        if (String.IsNullOrEmpty(MBCard.HolderName))
        {
          _source_name = MBCard.CardId.ToString() + "@" + Session.TerminalName;
          _mb_name = MBCard.CardId.ToString();
        }
        else
        {
          _source_name = MBCard.HolderName + "@" + Session.TerminalName;
          _mb_name = MBCard.CardId.ToString() + "-" + MBCard.HolderName;
        }

        _mb_movement_info = new MBMovementData();
        _mb_movement_info.CashierId = Session.TerminalId;
        _mb_movement_info.CardId = MBCard.CardId;
        _mb_movement_info.Type = MBMovementType.CloseSessionWithExcess;
        _mb_movement_info.CashierSessionId = Session.CashierSessionId;
        _mb_movement_info.InitialBalance = 0;
        _mb_movement_info.SubAmount = 0;
        _mb_movement_info.AddAmount = MBCard.OverCashSession;
        _mb_movement_info.FinalBalance = 0;
        _mb_movement_info.CashierName = Session.TerminalName;
        _mb_movement_info.OperationId = 0;

        if (!DB_InsertMBCardMovement(_mb_movement_info, out movement_id, SqlTrx))
        {
          Log.Error("DB_MBCloseCashSession. MBMovement not inserted. Mobile Bank: " + MBCard.CardId.ToString());

          return false;
        }

        _description = Resource.String(Alarm.ResourceId(AlarmCode.User_WrongDelivered), (MBCard.OverCashSession).ToString());
        _description = _description + ", " + Resource.String("STR_FORMAT_GENERAL_NAME_MB") + ": " + _mb_name;

        //Recording the alarm using the Alarm.Register
        Alarm.Register(AlarmSourceCode.User, Session.UserId, _source_name, (UInt32)AlarmCode.User_WrongDelivered,
                       _description, AlarmSeverity.Warning, DateTime.MinValue, SqlTrx);

        _cashier_movements = new CashierMovementsTable(Session);
        _cashier_movements.Add(0, CASHIER_MOVEMENT.MB_EXCESS, MBCard.OverCashSession, MBCard.CardId, MBCard.TrackData);

        if (!_cashier_movements.Save(SqlTrx))
        {
          return false;
        }
      }

      // Insert Close MB Movement
      _mb_movement_info = new MBMovementData();
      _mb_movement_info.CashierId = Session.TerminalId;
      _mb_movement_info.CardId = MBCard.CardId;
      _mb_movement_info.Type = MBMovementType.CloseSession;
      _mb_movement_info.CashierSessionId = Session.CashierSessionId;
      _mb_movement_info.InitialBalance = MBCard.SalesLimit;  //mb_balance
      _mb_movement_info.SubAmount = MBCard.SalesLimit;  //mb_balance
      _mb_movement_info.AddAmount = 0;
      _mb_movement_info.FinalBalance = 0;
      _mb_movement_info.CashierName = Session.TerminalName;
      _mb_movement_info.OperationId = 0;

      if (!DB_InsertMBCardMovement(_mb_movement_info, out movement_id, SqlTrx))
      {
        Log.Error("DB_MBCloseCashSession. MBMovement not inserted. Mobile Bank: " + MBCard.CardId.ToString());

        return false;
      }

      // Update MB Sales of CASHIER Session
      if (!Cashier.UpdateSessionTotalSold(Session.CashierSessionId, CashierUserId, WSI.Common.Cashier.SalesLimitType.MobileBank,
                                              _total_pending_cash, SqlTrx))
      {
        Log.Error("DB_MBCardCloseCashSession. MBCard Sales not updated. Mobile Bank: " + MBCard.CardId.ToString());

        return false;
      }

      // Update MB Sales LIMIT of CASHIER Session
      if (!DB_MB_SetNewSalesLimit(MBCard, MB_SALES_LIMIT_OPERATION.CLOSE_SESSION, _total_pending_cash, Session, CashierUserId,
                                  out _dummy_sales_limit_reached, out _dummy_remaining_sales_limit, out _new_mb_sales_limit, SqlTrx))
      {
        Log.Error("DB_MBCardCloseCashSession: Error extension cash. Mobile Bank: " + MBCard.CardId.ToString());

        return false;
      }

      //Get voucher
      _voucher_saved = GenerateVoucherForClosingSession(MBCard, out Voucher, _total_pending_cash, SqlTrx);

      if (!_voucher_saved)
      {
        Log.Error("DB_MBCardCloseCashSession. Error saving voucher. Card Id: " + MBCard.CardId.ToString());

        return false;
      }

      return true;
    } // DB_MBCloseCashSession

    /// <summary>
    /// Generate voucher when we close the session
    /// </summary>
    /// <param name="MBCard"></param>
    /// <param name="Voucher"></param>
    /// <param name="TotalPendingCash"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean GenerateVoucherForClosingSession(MBCardData MBCard, out VoucherMBCardDepositCash Voucher, Currency TotalPendingCash, SqlTransaction SqlTrx)
    {
      // 
      // Voucher
      // 
      Voucher = new VoucherMBCardDepositCash(PrintMode.Print, CashierVoucherType.MBClosing, MBCard, SqlTrx);

      // TITLE
      Voucher.AddString("STR_VOUCHER_MBCARD_CASH_DEPOSIT_TITLE", Resource.String("STR_UC_CARD_MB_CLOSE_SESSION_TITLE"));

      // Recharges
      Voucher.AddString("STR_VOUCHER_MBCARD_CASH_DEPOSIT_AMOUNT", ((String)Resource.String("STR_UC_CARD_RECHARGES") + ":"));
      Voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_AMOUNT", ((Currency)MBCard.DepositCash + TotalPendingCash) - MBCard.OverCash);

      // Deposits
      Voucher.AddString("STR_VOUCHER_MBCARD_CASH_DEPOSIT_PENDING", ((String)Resource.String("STR_UC_CARD_BALANCE_INITIAL_CASH_IN") + ":"));
      Voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_PENDING", MBCard.DepositCash);

      // Sales Limit detail 
      Voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_INITIAL", MBCard.SalesLimit);
      Voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_FINAL", ((Currency)0));

      // Excess
      Voucher.AddString("STR_VOUCHER_MBCARD_CASH_DEPOSIT_EXCESS", ((String)Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA") + ":"));
      Voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_EXCESS", MBCard.OverCash);

      // Total title
      Voucher.AddString("STR_VOUCHER_MBCARD_CASH_DEPOSIT_CASH", ((String)Resource.String("STR_UC_CARD_PENDING_DEPOSITS") + ":"));
      Voucher.AddCurrency("VOUCHER_MBCARD_CASH_DEPOSIT_CASH", TotalPendingCash);

      // Save voucher
      return Voucher.Save(SqlTrx);
      }

    public static Boolean GP_GetRegisterShortfallOnDeposit()
    {
      return GeneralParam.GetBoolean("MobileBank", "RegisterShortfallOnDeposit", false);
    }

    public static Boolean DB_IsClosedSesionMB(Int64 MobileBankId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;
      Int16 _mb_status;

      _sb = new StringBuilder();
      _sb.AppendLine("  SELECT  MB_SESSION_STATUS         ");
      _sb.AppendLine("  FROM    MOBILE_BANKS              ");
      _sb.AppendLine("  WHERE   MB_ACCOUNT_ID = @pMBId    ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pMBId", SqlDbType.BigInt).Value = MobileBankId;

        _obj = _cmd.ExecuteScalar();
      }

      if (_obj == null || _obj == DBNull.Value)
      {
        return false;
      }

      _mb_status = (Int16)_obj;

      return ((MbSessionStatus)_mb_status == MbSessionStatus.CLOSED);
      }

    public static Boolean DB_HasClosedSesionMB(Int64 CashierSessionId, Int64 MobileBankId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;

      _sb = new StringBuilder();
      _sb.AppendLine("  SELECT   COUNT(*)                                                     ");
      _sb.AppendLine("    FROM   MB_MOVEMENTS                                                 ");
      _sb.AppendLine("   WHERE   MBM_CASHIER_SESSION_ID = @pCSId                              ");
      _sb.AppendLine("     AND   MBM_MB_ID = @pMBId                                           ");
      _sb.AppendLine("     AND   MBM_TYPE = @pMovType                                         ");
      _sb.AppendLine("     AND   MBM_MOVEMENT_ID = (SELECT   MAX(MBM_MOVEMENT_ID)             ");
      _sb.AppendLine("                                FROM   MB_MOVEMENTS                     ");
      _sb.AppendLine("                               WHERE   MBM_CASHIER_SESSION_ID = @pCSId  ");
      _sb.AppendLine("                                 AND   MBM_MB_ID = @pMBId )             ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pCSId", SqlDbType.BigInt).Value = CashierSessionId;
        _cmd.Parameters.Add("@pMBId", SqlDbType.BigInt).Value = MobileBankId;
        _cmd.Parameters.Add("@pMovType", SqlDbType.SmallInt).Value = (Int16)MBMovementType.CloseSession;

        _obj = _cmd.ExecuteScalar();
      }

      if (_obj != null && _obj != DBNull.Value)
      {
        return ((Int32)_obj > 0);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: know if the card is in use
    //------------------------------------------------------------------------------
    public static Boolean CardInUse(String trackdata)
    {
      Boolean _rc;
      StringBuilder _sb;
      String _internal_card_id;
      Int32 _card_type;

      // Obtain internal card identifier from track data
      _rc = CardNumber.TrackDataToInternal(trackdata, out _internal_card_id, out _card_type);

      _sb = new StringBuilder();
      _sb.AppendLine("  SELECT   MB_ACCOUNT_ID                      ");
      _sb.AppendLine("    FROM   MOBILE_BANKS                       ");
      _sb.AppendLine("   WHERE   MB_TRACK_DATA = @pTrackdata        ");
      _sb.AppendLine("   AND   MB_USER_ID   IS NOT NULL        ");

      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _trx.SqlTransaction.Connection, _trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pTrackdata", SqlDbType.NVarChar).Value = _internal_card_id;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              return (_reader.Read() && !_reader.IsDBNull(0));
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }
    /// <summary>
    /// Get Card All Data from Card Id.
    /// </summary>
    /// <param name="CardId"></param>
    /// <param name="CardInfo"></param>
    /// <param name="Session"></param>
    /// <returns></returns>
    public static Boolean DB_MBCardGetAllData(Int64 CardId,
                                              MBCardData CardInfo,
                                              CashierSessionInfo Session)
    {
      StringBuilder _sb;
      Int64 _current_cashier_session_id;
      Int32 _last_terminal_id;
      String _last_terminal_name;
      Boolean _data_is_ok;

      // Initialize mobile bank card data
      InitializeMBCardData(CardId, CardInfo);

      // Initialize variables
      _last_terminal_id = 0;
      _last_terminal_name = "";
      _current_cashier_session_id = 0;

      _data_is_ok = false;

      // Get card data
      _sb = new StringBuilder();
      _sb.AppendLine("SELECT   MB_ACCOUNT_ID                              ");
      _sb.AppendLine("     ,   MB_ACCOUNT_TYPE                            ");
      _sb.AppendLine("     ,   ISNULL(MB_HOLDER_NAME, '')                 ");
      _sb.AppendLine("     ,   MB_BLOCKED                                 ");
      _sb.AppendLine("     ,   MB_BALANCE                                 ");
      _sb.AppendLine("     ,   MB_INITIAL_LIMIT                           ");
      _sb.AppendLine("     ,   MB_CASH_IN                                 ");
      _sb.AppendLine("     ,   MB_DEPOSIT                                 ");
      _sb.AppendLine("     ,   MB_EXTENSION                               ");
      _sb.AppendLine("     ,   MB_PENDING_CASH                            ");
      //_sb.AppendLine("     ,   MB_PIN                                     ");
      _sb.AppendLine("     ,   (CASE WHEN MB_PIN = '0' THEN '' ELSE MB_PIN END) as MB_PIN     "); // EAV 09-05-2018
      _sb.AppendLine("     ,   MB_CASHIER_SESSION_ID                      ");
      _sb.AppendLine("     ,   MB_LAST_ACTIVITY                           ");
      _sb.AppendLine("     ,   MB_LAST_TERMINAL_ID                        ");
      _sb.AppendLine("     ,   ISNULL(MB_LAST_TERMINAL_NAME, '')          ");
      _sb.AppendLine("     ,   CAST(ISNULL(MB_TIMESTAMP, 0) AS BIGINT)    ");   // RCI & AJQ 24-FEB-2011
      _sb.AppendLine("     ,   MB_OVER_CASH                               ");   // JBC 21-OCT-2014
      _sb.AppendLine("     ,   MB_TOTAL_LIMIT                             ");
      _sb.AppendLine("     ,   MB_RECHARGE_LIMIT                          ");
      _sb.AppendLine("     ,   MB_NUMBER_OF_RECHARGES_LIMIT               ");
      _sb.AppendLine("     ,   MB_EMPLOYEE_CODE                           ");
      _sb.AppendLine("     ,   MB_SHORTFALL_CASH                          ");   // JBC 21-OCT-2014
      _sb.AppendLine("     ,   MB_OVER_CASH_SESSION                       ");
      _sb.AppendLine("     ,   MB_SHORTFALL_CASH_SESSION                  ");
      _sb.AppendLine("     ,   MB_SESSION_STATUS                          ");
      _sb.AppendLine("     ,   MB_USER_ID                                 ");
      _sb.AppendLine("  FROM   MOBILE_BANKS                               ");
      _sb.AppendLine(" WHERE   MB_ACCOUNT_ID = @pCardId                   ");

      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _trx.SqlTransaction.Connection, _trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pCardId", SqlDbType.BigInt).Value = CardId;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (_reader.Read() && !_reader.IsDBNull(0))
              {
                _data_is_ok = true;

                if (_data_is_ok)
                {
                  CardInfo.HolderName = (String)_reader[2];
                  CardInfo.Blocked = (Boolean)_reader[3];
                  CardInfo.SalesLimit = _reader.GetSqlMoney(4);
                  CardInfo.InitialLimit = _reader.GetSqlMoney(5);
                  CardInfo.DisposedCash = _reader.GetSqlMoney(6);
                  CardInfo.DepositCash = _reader.GetSqlMoney(7);
                  CardInfo.LimitExtensions = _reader.GetSqlMoney(8);
                  CardInfo.PendingCash = _reader.GetSqlMoney(9);
                  CardInfo.Pin = (String)_reader[10];

                  _current_cashier_session_id = (_reader.IsDBNull(11)) ? 0 : (Int64)_reader[11];
                  _last_terminal_id = (_reader.IsDBNull(13)) ? 0 : (Int32)_reader[13];
                  _last_terminal_name = (String)_reader[14];

                  CardInfo.LastActivity = (_reader.IsDBNull(12)) ? DateTime.MinValue : (DateTime)_reader[12];
                  CardInfo.Timestamp = _reader.GetInt64(15);
                  CardInfo.OverCash = _reader.GetSqlMoney(16);
                  CardInfo.TotalLimit = (_reader.IsDBNull(17) ? 0 : _reader.GetSqlMoney(17));
                  CardInfo.RechargeLimit = (_reader.IsDBNull(18) ? 0 : _reader.GetSqlMoney(18));
                  CardInfo.NumberOfRechargesLimit = (_reader.IsDBNull(19) ? 0 : _reader.GetInt32(19));
                  CardInfo.EmployeeCode = _reader.IsDBNull(20) ? String.Empty : _reader.GetString(20);
                  CardInfo.ShortFallCash = _reader.GetSqlMoney(21);
                  CardInfo.OverCashSession = _reader.GetSqlMoney(22);
                  CardInfo.ShortFallCashSession = _reader.GetSqlMoney(23);
                  CardInfo.SessionClosed = ((MbSessionStatus)_reader.GetInt16(24) == MbSessionStatus.CLOSED);
                  CardInfo.UserId = (_reader.IsDBNull(25)) ? 0 : (Int32)_reader[25];
                  CardInfo.SiteId = GeneralParam.GetInt32("Site", "Identifier", 0);
                }
              }
            } //using SQLDataReader

            if (_data_is_ok)
            {
              //If data is ok, then retrieve trackdata
              CardInfo.TrackData = DB_GetMBExternalTrackdata(CardId, _trx.SqlTransaction);
          }

            //If trackdata empty or not exists then is a virtual card
            CardInfo.IsVirtualCard = String.IsNullOrEmpty(CardInfo.TrackData);
          }

          //
          // Session Info
          //
          {
            Int64 _info_cashier_session_id;

            _info_cashier_session_id = 0;
            if (Session != null)
            {
              _info_cashier_session_id = Session.CashierSessionId;
            }
            else if (_current_cashier_session_id != 0)
            {
              _info_cashier_session_id = _current_cashier_session_id;
            }

            if (_info_cashier_session_id != 0 && _current_cashier_session_id == 0)
            {             
              CardInfo.SessionClosed = DB_HasClosedSesionMB(_info_cashier_session_id, CardId, _trx.SqlTransaction);
            }
          }

          _trx.Commit();
        }
      } // try
      catch (Exception ex)
      {
        Log.Exception(ex);

        return false;
      }

      if (_last_terminal_id > 0)
      {
        CardInfo.LastTerminalId = _last_terminal_id;
        CardInfo.LastTerminalName = _last_terminal_name;
      }

      if (_current_cashier_session_id != 0)
      {
        CardInfo.CurrentCashierSessionId = _current_cashier_session_id;
      }

      return _data_is_ok;
    } // DB_MBCardGetAllData

    /// <summary>
    /// Initialize mobile bank card data
    /// </summary>
    /// <param name="CardId"></param>
    /// <param name="CardInfo"></param>
    private static void InitializeMBCardData(Int64 CardId, MBCardData CardInfo)
      {
      // Initialize card data
      CardInfo.CardId = CardId;
      CardInfo.HolderName = String.Empty;
      CardInfo.Blocked = false;
      CardInfo.SalesLimit = 0;
      CardInfo.InitialLimit = 0;
      CardInfo.DisposedCash = 0;
      CardInfo.DepositCash = 0;
      CardInfo.LimitExtensions = 0;
      CardInfo.PendingCash = 0;
      CardInfo.Pin = String.Empty;
      CardInfo.TrackData = String.Empty;
      CardInfo.TotalLimit = 0;
      CardInfo.RechargeLimit = 0;
      CardInfo.NumberOfRechargesLimit = 0;
      CardInfo.EmployeeCode = String.Empty;
      CardInfo.Timestamp = 0;
      CardInfo.UserId = 0;
      CardInfo.IsVirtualCard = false;
      }

    //------------------------------------------------------------------------------
    // PURPOSE: Close All MB Cash Sessions of a Cashier session
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Session
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true: operation completed successfully
    //      - false: operation failed
    // 
    //   NOTES:
    //    
    public static Boolean DB_MBCloseAllCashSessions(CashierSessionInfo Session,
                                                    Int32 CashierUserId,
                                                    SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      List<Int64> _mb_ids;
      MBCardData _mb_card_data;
      VoucherMBCardDepositCash _voucher;

      // Initializations
      _mb_card_data = new MBCardData();
      _mb_ids = new List<Int64>();

      _sb = new StringBuilder();
      _sb.AppendLine(" SELECT   MB_ACCOUNT_ID                   ");
      _sb.AppendLine("   FROM   MOBILE_BANKS                    ");
      _sb.AppendLine("  WHERE   MB_CASHIER_SESSION_ID = @pCSId  ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pCSId", SqlDbType.BigInt).Value = Session.CashierSessionId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              _mb_ids.Add(Convert.ToInt64(_reader["MB_ACCOUNT_ID"]));
            }
          }
        }

        foreach (Int64 _id in _mb_ids)
        {
          if (DB_MBCardGetAllData(_id, _mb_card_data, Session))
          {
            if (!DB_MBCloseCashSession(_mb_card_data, Session, CashierUserId, out _voucher, true, SqlTrx))
            {
              return false;
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_MBCloseCashSession

    public static ReopenMode GetReopenMode()
    {
      Int32 _reopen_mode;
      _reopen_mode = GeneralParam.GetInt32("MobileBank", "ReopenMode", 0);

      if (_reopen_mode < 0 || _reopen_mode > 3)
      {
        _reopen_mode = 0;   //No reopen
      }

      return (ReopenMode)_reopen_mode;
    }

    /// <summary>
    /// Get mobile bank user type
    /// </summary>
    /// <param name="MBCard"></param>
    /// <returns></returns>
    public static MobileBankUserType GetMobileBankUserType(MBCardData MBCard)
    {
      if (!MobileBankConfig.IsMobileBankLinkedToADevice)
      {
        return MobileBankUserType.MobileBankCard;
      }

      if ((MBCard != null) && (!MBCard.IsVirtualCard))
      {
        return MobileBankUserType.MobileBankCard;
      }

      return MobileBankUserType.UserDevice;
    }

    /// <summary>
    /// Get mobile bank user type
    /// </summary>
    /// <param name="CardId"></param>
    /// <returns></returns>
    public static MobileBankUserType GetMobileBankUserType(Int64 CardId)
    {
      MBCardData _MB_card_info;
      CashierSessionInfo _session;

      _MB_card_info = new MBCardData();
      _session = null;

      //Get MB card data
      DB_MBCardGetAllData(CardId, _MB_card_info, _session);

      return (GetMobileBankUserType(_MB_card_info));
    }

    /// <summary>
    /// Get card trackdata for voucher
    /// </summary>
    /// <param name="MBCard"></param>
    /// <returns></returns>
    public static String GetCardTrackDataForVoucher(MBCardData MBCard)
    {
      if (GetMobileBankUserType(MBCard) == MobileBankUserType.MobileBankCard)
      {
        return MBCard.TrackData.ToString();
      }

      return MobileBankConfig.GetVirtualCardTrackdataToShow();
    }

    /// <summary>
    /// Create main fields in mobile bank (Info User)
    /// </summary>
    /// <param name="MBCard"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean DB_CreateEmptyMobileBank(MBCardData MBCard, SqlTransaction Trx)
    {
      StringBuilder _sb;

      SqlParameter _param_account_id;
      SqlParameter _param_pin;
      SqlParameter _param_track_data;

      Int32 _num_rows_inserted;

      if (MBCard == null || (MBCard.CardId != 0))
      {
        return false;
      }

      MBCard.CardId = 0;
      MBCard.TrackData = String.Empty;

      _sb = new StringBuilder();

      _sb.AppendLine("SET   @pPin = ROUND((9999 - 1000) * RAND() + 1000, 0)   ");

      _sb.AppendLine("INSERT   INTO MOBILE_BANKS                              ");
      _sb.AppendLine("(                                                       ");
      _sb.AppendLine("         MB_ACCOUNT_TYPE                                ");
      _sb.AppendLine("       , MB_HOLDER_NAME                                 ");
      _sb.AppendLine("       , MB_USER_ID                                     ");
      _sb.AppendLine("       , MB_PIN                                         ");
      _sb.AppendLine("       , MB_TRACK_DATA                                  ");
      _sb.AppendLine("       , MB_BLOCKED                                     ");
      _sb.AppendLine("       , MB_BALANCE                                     ");
      _sb.AppendLine(")                                                       ");
      _sb.AppendLine("VALUES                                                  ");
      _sb.AppendLine("(                                                       ");
      _sb.AppendLine("         @pAccountType                                  ");
      _sb.AppendLine("       , @pHolderName                                   ");
      _sb.AppendLine("       , @pUserId                                       ");
      _sb.AppendLine("       , @pPin                                          ");
      _sb.AppendLine("       , @pInternal                                     ");
      _sb.AppendLine("       , 0                                              ");
      _sb.AppendLine("       , 0                                              ");
      _sb.AppendLine(")                                                       ");

      _sb.AppendLine("SET   @pAccountId  = SCOPE_IDENTITY()                   ");
      _sb.AppendLine("SET   @pTrackData  = RIGHT('0000000000000' + CAST(@pAccountId AS NVARCHAR), 13) ");

      _sb.AppendLine("UPDATE   MOBILE_BANKS                                   ");
      _sb.AppendLine("   SET   MB_TRACK_DATA = @pTrackData                    ");
      _sb.AppendLine(" WHERE   MB_ACCOUNT_ID = @pAccountId                    ");

      //LinkCard to associate (in this case deassociate) mobile bank and card
      //    @pExternalTrackData = null  --> We want to deassociate mobile bank and card
      _sb.AppendLine("  EXEC   dbo.LinkCard  NULL, @pLinkedType, @pAccountId  ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        try
        {
          _cmd.Parameters.Add("@pAccountType", SqlDbType.Int).Value = (Int32)MobileBankConfig.MOBILE_BANK_NEW_ACCOUNT_TYPE;
          _cmd.Parameters.Add("@pHolderName", SqlDbType.NVarChar, 50).Value = MBCard.HolderName;
          _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = MBCard.UserId;
          _cmd.Parameters.Add("@pLinkedType", SqlDbType.Int).Value = GetMBLinkedType();

          //Temporal trackdata, it will be changed in the update that follows the insert
          _cmd.Parameters.Add("@pInternal", SqlDbType.NVarChar, 50).Value = DateTime.Now.ToString("yyMMddHHmmss");

          //Output params
          _param_account_id = _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt);
          _param_account_id.Direction = ParameterDirection.Output;

          _param_pin = _cmd.Parameters.Add("@pPin", SqlDbType.NVarChar, 12);
          _param_pin.Direction = ParameterDirection.Output;

          _param_track_data = _cmd.Parameters.Add("@pTrackData", SqlDbType.NVarChar, 50);
          _param_track_data.Direction = ParameterDirection.Output;

          _num_rows_inserted = _cmd.ExecuteNonQuery();

          if (_num_rows_inserted != 2)  //Insert + Update
          {
            Log.Error("DB_CreateEmptyMobileBank. Card could not be inserted.");
            return false;
          }

          //Get values of output params
          if (_param_account_id.Value != null && _param_account_id.Value != DBNull.Value)
          {
            MBCard.CardId = (Int64)_param_account_id.Value;
          }

          if (_param_pin.Value != null && _param_pin.Value != DBNull.Value)
          {
            MBCard.Pin = _param_pin.Value.ToString();
          }

          if (_param_track_data.Value != null && _param_track_data.Value != DBNull.Value)
          {
            String _internal_trackdata;
            String _external_trackdata;
            Int32 _card_type;

            _internal_trackdata = _param_track_data.Value.ToString();
            _card_type = (Int32)MobileBankConfig.MOBILE_BANK_NEW_ACCOUNT_TYPE;

            //Get external track data
            if (!CardNumber.TrackDataToExternal(out _external_trackdata, _internal_trackdata, _card_type))
            {
              Log.Error("DB_CreateEmptyMobileBank. Card could not be linked.");
              return false;
            }

            MBCard.TrackData = _external_trackdata;
            MBCard.IsVirtualCard = true;

            return true;
          }

          //It dit not return a trackdata
          return false;
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Create main fields in mobile bank (Info User and l�mits)
    // 
    //  PARAMS:
    //      - INPUT:
    //        
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true: operation completed successfully
    //      - false: operation failed
    // 
    //   NOTES:
    //
    public static Boolean DB_InsertMobileBank(MBCardData MBCard)
    {
      StringBuilder _sb;
      Int32 _num_rows_affected;
      Boolean _rc;
      String _internal_card_id;
      int _card_type;

      // Obtain internal card identifier from track data
      _rc = CardNumber.TrackDataToInternal(MBCard.TrackData, out _internal_card_id, out _card_type);

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb = new StringBuilder();
          _sb.AppendLine("DECLARE @v_AccountId        AS  BIGINT                                                  ");
          _sb.AppendLine("DECLARE @v_VirtualInternal  AS  NVARCHAR(20)                                            ");

          _sb.AppendLine("INSERT   INTO MOBILE_BANKS                                                              ");
          _sb.AppendLine("       (                                                                                ");
          _sb.AppendLine("         MB_TRACK_DATA                                                                  ");
          _sb.AppendLine("       , MB_BLOCKED                                                                     ");
          _sb.AppendLine("       , MB_BALANCE                                                                     ");
          _sb.AppendLine("       , MB_HOLDER_NAME                                                                 ");
          _sb.AppendLine("       , MB_EMPLOYEE_CODE                                                               ");
          _sb.AppendLine("       , MB_PIN                                                                         ");
          _sb.AppendLine("       , MB_TOTAL_LIMIT                                                                 ");
          _sb.AppendLine("       , MB_RECHARGE_LIMIT                                                              ");
          _sb.AppendLine("       , MB_NUMBER_OF_RECHARGES_LIMIT                                                   ");
          _sb.AppendLine("       , MB_USER_ID                                                                     ");
          _sb.AppendLine("       )                                                                                ");
          _sb.AppendLine("VALUES                                                                                  ");
          _sb.AppendLine("       (                                                                                ");
          _sb.AppendLine("         @pInternalTrackdata                                                            ");
          _sb.AppendLine("       , @pBlocked                                                                      ");
          _sb.AppendLine("       , 0                                                                              ");
          _sb.AppendLine("       , @pHolderName                                                                   ");
          _sb.AppendLine("       , @pEmployeeCode                                                                 ");
          _sb.AppendLine("       , @pPin                                                                          ");
          _sb.AppendLine("       , @pTotalLimit                                                                   ");
          _sb.AppendLine("       , @pRechargeLimit                                                                ");
          _sb.AppendLine("       , @pNumberOfRechargesLimit                                                       ");
          _sb.AppendLine("       , @pUserId                                                                       ");
          _sb.AppendLine("       )                                                                                ");

          _sb.AppendLine(" SET @v_AccountId = SCOPE_IDENTITY()                                                    ");
          _sb.AppendLine(" SET @v_VirtualInternal = RIGHT('0000000000000' + CAST(@v_AccountId AS NVARCHAR), 13)   ");

          _sb.AppendLine("IF @pExternalTrackdata <> '0'                                                           ");
          _sb.AppendLine("BEGIN                                                                                   ");

          _sb.AppendLine("    EXEC dbo.LinkCard @pExternalTrackdata, @pLinkedType, @v_AccountId                   ");

          _sb.AppendLine("    UPDATE   MOBILE_BANKS                                                               ");
          _sb.AppendLine("      SET   MB_TRACK_DATA  =  @v_VirtualInternal                                        ");
          _sb.AppendLine("    WHERE   MB_ACCOUNT_ID  =  @v_AccountId                                              ");

          _sb.AppendLine("END                                                                                      ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
          {
            _cmd.Parameters.Add("@pInternalTrackdata", SqlDbType.NVarChar).Value = (_rc) ? (object)_internal_card_id : DBNull.Value;
            _cmd.Parameters.Add("@pExternalTrackdata", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(MBCard.TrackData) ? "0" : MBCard.TrackData;
            _cmd.Parameters.Add("@pPin", SqlDbType.NVarChar).Value = string.IsNullOrEmpty(MBCard.Pin) ? "0" : MBCard.Pin;
            _cmd.Parameters.Add("@pLinkedType", SqlDbType.Int).Value = GetMBLinkedType();
            _cmd.Parameters.Add("@pUserId", SqlDbType.Int).Value = MBCard.UserId;
            _cmd.Parameters.Add("@pTotalLimit", SqlDbType.Money).Value = MBCard.TotalLimit.SqlMoney;
            _cmd.Parameters.Add("@pRechargeLimit", SqlDbType.Money).Value = MBCard.RechargeLimit.SqlMoney;
            _cmd.Parameters.Add("@pNumberOfRechargesLimit", SqlDbType.Int).Value = MBCard.NumberOfRechargesLimit;
            _cmd.Parameters.Add("@pHolderName", SqlDbType.NVarChar).Value = MBCard.HolderName;
            _cmd.Parameters.Add("@pBlocked", SqlDbType.Bit).Value = MBCard.Blocked;
            _cmd.Parameters.Add("@pEmployeeCode", SqlDbType.NVarChar).Value = String.IsNullOrEmpty(MBCard.EmployeeCode) ? String.Empty : (MBCard.EmployeeCode);

            _num_rows_affected = _db_trx.ExecuteNonQuery(_cmd);
          }

          if (_num_rows_affected != 2)  //Insert + Update
          {
            Log.Error("DB_InsertMobileBank. Card could not be inserted.");
          }

          _db_trx.Commit();
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }

    /// <summary>
    /// Delete Mobile Bank
    /// </summary>
    /// <param name="MBCard"></param>
    /// <returns></returns>
    public static Boolean DB_DeleteMobileBank(MBCardData MBCard)
    {
      if (MBCard == null || (MBCard.CardId == 0))
      {
        return true;
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          //Unlink card
          if (!Internal_MobileBankLinkCard(MBCard, true, _db_trx.SqlTransaction))
          {
            Log.Error("DB_DeleteMobileBank. Error updating LinkCard.");
            return false;
          }

          //Delete MobileBank
          if (!Internal_DeleteMobileBank(MBCard, _db_trx.SqlTransaction))
          {
            Log.Error("DB_DeleteMobileBank. Error deleting MobileBank.");
            return false;
          }

          _db_trx.Commit();

          return true;
        } // end using trx
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Update main fields in mobile bank (Info User and l�mits)
    // 
    //  PARAMS:
    //      - INPUT:
    //          - MBCard
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true: operation completed successfully
    //      - false: operation failed
    // 
    //   NOTES:
    //    
    public static Boolean DB_UpdateMobileBank(MBCardData MBCard)
    {
      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          //Update mobile bank
          if (!Internal_UpdateMobileBank(MBCard, _trx.SqlTransaction))
          {
            Log.Error("DB_UpdateMobileBank. Error updating MobileBank.");
            return false;
          }

          //Link card
          if (!Internal_MobileBankLinkCard(MBCard, false, _trx.SqlTransaction))
          {
            Log.Error("DB_UpdateMobileBank. Error updating LinkCard.");
            return false;
          }

          _trx.Commit();

          return true;
        } // end using trx
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }

    /// <summary>
    /// Update mobile bank
    /// </summary>
    /// <param name="MBCard"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean Internal_UpdateMobileBank(MBCardData MBCard, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _num_rows_updated;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   MOBILE_BANKS                                         ");
        _sb.AppendLine("    SET   MB_HOLDER_NAME                 = @pHolderName        ");
        _sb.AppendLine("        , MB_EMPLOYEE_CODE               = @pEmployeeCode      ");
        _sb.AppendLine("        , MB_BLOCKED                     = @pBlocked           ");
        _sb.AppendLine("        , MB_PIN                         = @pPin               ");
        _sb.AppendLine("        , MB_RECHARGE_LIMIT              = @pRechargeLimit     ");
        _sb.AppendLine("        , MB_NUMBER_OF_RECHARGES_LIMIT   = @pNumRechargesLimit ");
        _sb.AppendLine("        , MB_TOTAL_LIMIT                 = @pBySessionLimit    ");

        if (String.IsNullOrEmpty(MBCard.TrackData))
        {
          _sb.AppendLine("        , MB_TRACK_DATA                  = RIGHT('0000000000000' + CAST(@pAccountId AS NVARCHAR), 13) ");
        }
        else
        {
          _sb.AppendLine("        , MB_TRACK_DATA                  = @pTrackData         ");
        }

        _sb.AppendLine("  WHERE   MB_ACCOUNT_ID  =  @pAccountId                        ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pHolderName", SqlDbType.NVarChar).Value = MBCard.HolderName;
            _cmd.Parameters.Add("@pEmployeeCode", SqlDbType.NVarChar).Value = MBCard.EmployeeCode;
            _cmd.Parameters.Add("@pBlocked", SqlDbType.Bit).Value = MBCard.Blocked;
            _cmd.Parameters.Add("@pPin", SqlDbType.NVarChar).Value = MBCard.Pin;
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = MBCard.CardId;

          if (!String.IsNullOrEmpty(MBCard.TrackData))
          {
            string _internal_card_id;
            int typeCard;

            Boolean _rc;

            // Obtain internal card identifier from track data
            _rc = CardNumber.TrackDataToInternal(MBCard.TrackData, out _internal_card_id, out typeCard);

            _cmd.Parameters.Add("@pTrackData", SqlDbType.NVarChar).Value = (_rc) ? (object)_internal_card_id : DBNull.Value;

          }

            if (MBCard.RechargeLimit.SqlMoney == 0)
            {
              _cmd.Parameters.Add("@pRechargeLimit", SqlDbType.Money).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pRechargeLimit", SqlDbType.Money).Value = MBCard.RechargeLimit.SqlMoney;
            }

            if (MBCard.NumberOfRechargesLimit == 0)
            {
              _cmd.Parameters.Add("@pNumRechargesLimit", SqlDbType.Int).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pNumRechargesLimit", SqlDbType.Int).Value = MBCard.NumberOfRechargesLimit;
            }

            if (MBCard.TotalLimit.SqlMoney == 0)
            {
              _cmd.Parameters.Add("@pBySessionLimit", SqlDbType.Money).Value = DBNull.Value;
            }
            else
            {
              _cmd.Parameters.Add("@pBySessionLimit", SqlDbType.Money).Value = MBCard.TotalLimit.SqlMoney;
            }

            _num_rows_updated = _cmd.ExecuteNonQuery();
          } // end using cmd

        return (_num_rows_updated == 1);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }

    /// <summary>
    /// Delete mobile bank
    /// </summary>
    /// <param name="MBCard"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean Internal_DeleteMobileBank(MBCardData MBCard, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _num_rows_affected;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("DELETE   FROM MOBILE_BANKS         ");
        _sb.AppendLine(" WHERE   MB_ACCOUNT_ID  =  @pMBId  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pMBId", SqlDbType.Int).Value = MBCard.CardId;

          _num_rows_affected = _cmd.ExecuteNonQuery();
        }

        if (_num_rows_affected != 1)  //Delete
          {
          Log.Error("Internal_DeleteMobileBank. Card could not be deleted.");
          }

        return (_num_rows_affected == 1);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }

    /// <summary>
    /// Call LinkCard for MobileBank
    /// </summary>
    /// <param name="MBCard"></param>
    /// <param name="ForceUnlink">Use when we are deleteing a MB, and want do deassociate it first</param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean Internal_MobileBankLinkCard(MBCardData MBCard, Boolean ForceUnlink, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        //LinkCard to associate/deassociate mobile bank and card
        _sb = new StringBuilder();
        _sb.AppendLine("  EXEC   dbo.LinkCard  @pLinkTrackData, @pLinkedType, @pLinkedId  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          //LinkCard
          if (ForceUnlink || String.IsNullOrEmpty(MBCard.TrackData))
          {
            _cmd.Parameters.Add("@pLinkTrackData", SqlDbType.NVarChar).Value = DBNull.Value;      //Deassociate
          }
          else
          {
            _cmd.Parameters.Add("@pLinkTrackData", SqlDbType.NVarChar).Value = MBCard.TrackData;  //Associate
          }

          _cmd.Parameters.Add("@pLinkedType", SqlDbType.Int).Value = GetMBLinkedType();
          _cmd.Parameters.Add("@pLinkedId", SqlDbType.BigInt).Value = MBCard.CardId;

          _cmd.ExecuteNonQuery();
        }

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      return false;
    }

    /// <summary>
    /// Update amount cash session undo deposit
    /// </summary>
    /// <param name="MBCard"></param>
    /// <param name="Session"></param>
    /// <param name="Amount"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean DB_MBUndoDeposit(MBCardData MBCard, CashierSessionInfo Session, Decimal Amount, MBMovementType UndoType, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   MOBILE_BANKS                                                        ");
        switch (UndoType)
        {
          case MBMovementType.CashShortFall:
               _sb.AppendLine("    SET   MB_SHORTFALL_CASH_SESSION = (MB_SHORTFALL_CASH_SESSION + @pAmount)  "); 

            break;
          case MBMovementType.CashExcess:
               _sb.AppendLine("    SET   MB_OVER_CASH_SESSION = (MB_OVER_CASH_SESSION + @pAmount)  "); 

            break;
          default:
            // No update
            return true;
        }

        _sb.AppendLine("  WHERE   MB_ACCOUNT_ID             = @pAccountId                             ");
        _sb.AppendLine("    AND   MB_CASHIER_SESSION_ID     = @pCSId                                  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = MBCard.CardId;
          _cmd.Parameters.Add("@pCSId", SqlDbType.BigInt).Value = Session.CashierSessionId;
          _cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = Amount;

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_MBUndoDeposit

    /// <summary>
    /// Method for the new type of device mobile bank, the purpose is get the CardId with the user Id (runner)
    /// </summary>
    /// <param name="guiUserId"></param>
    /// <returns></returns>
    public static Int64 DB_FindMBId(Int64 guiUserId)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   MB_ACCOUNT_ID           ");
        _sb.AppendLine("    FROM   MOBILE_BANKS            ");
        _sb.AppendLine("   WHERE   MB_USER_ID = @pUserID   ");

        using (DB_TRX _trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _trx.SqlTransaction.Connection, _trx.SqlTransaction))
          {
            _cmd.Parameters.Add("@pUserID", SqlDbType.BigInt).Value = guiUserId;

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (_reader.Read() && !_reader.IsDBNull(0))
              {
                return (Int64)_reader[0];
              }
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return 0;
    }

    /// <summary>
    /// Get linked type for MB
    /// </summary>
    /// <returns></returns>
    public static Int32 GetMBLinkedType()
    {
      return (Int32)WCP_CardTypes.CARD_TYPE_MOBILE_BANK;
    }

    /// <summary>
    /// Get MB external trackdata
    /// </summary>
    /// <param name="MBAccountId"></param>
    /// <returns></returns>
    public static String DB_GetMBExternalTrackdata(Int64 MBAccountId, SqlTransaction Trx)
    {
      Object _obj;
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("   SELECT   TOP 1   CA_TRACKDATA             ");
        _sb.AppendLine("     FROM   CARDS                            ");
        _sb.AppendLine("    WHERE   CA_LINKED_TYPE  =  @pLinkedType  ");
        _sb.AppendLine("      AND   CA_LINKED_ID    =  @pLinkedId    ");
        _sb.AppendLine(" ORDER BY   CA_LINKED_ID                     ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pLinkedType", SqlDbType.Int).Value = GetMBLinkedType();
          _cmd.Parameters.Add("@pLinkedId", SqlDbType.BigInt).Value = MBAccountId;

          _obj = _cmd.ExecuteScalar();

          if ((_obj != null) && (_obj != DBNull.Value))
          {
            return ((String)_obj);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return String.Empty;
    }

    /// <summary>
    /// Has MB a virtual card?
    /// </summary>
    /// <param name="MBAccountId"></param>
    /// <returns></returns>
    public static Boolean DB_HasMBVirtualCard(Int64 MBAccountId, SqlTransaction Trx)
    {
      return (String.IsNullOrEmpty(DB_GetMBExternalTrackdata(MBAccountId, Trx)));
    }
  }

  public class MBCardData
  {
    public Int64 CardId;
    public String HolderName;
    public Boolean Blocked;
    public String  TrackData;

    public CardTrackData CardTrackData;
    public Currency SalesLimit;
    public Currency InitialLimit;
    public Currency DisposedCash;
    public Currency DepositCash;
    public Currency LimitExtensions;
    public Currency PendingCash;
    public Currency BalanceLost;
    public Boolean SessionClosed;
    public String Pin;
    public Currency TotalLimit;
    public Currency RechargeLimit;
    public Int32 NumberOfRechargesLimit;
    public String EmployeeCode;

    // RCI & AJQ 24-FEB-2011
    public Int64 Timestamp;

    // Site id.
    public Int32 SiteId;

    public Int32 LastTerminalId;
    public String LastTerminalName;
    public DateTime LastActivity;
    public Int64 CurrentCashierSessionId;

    public Boolean HadMovementsInThisCashierSession;

    //JBC 21-OCT-2014
    public Currency OverCash;

    //JBC 12-NOV-2014
    public Currency ShortFallCash;

    public Currency OverCashSession;
    public Currency ShortFallCashSession;

    //FGB 20-NOV-2017: MobiBank
    public Int32 UserId;
    public Boolean IsVirtualCard;
  } // MBCardData

  public class MBMovementData
  {
    public Int64 CashierId;
    public Int64 CardId;
    public MBMovementType Type;
    public Int64 CashierSessionId;
    public Currency InitialBalance;
    public Currency SubAmount;
    public Currency AddAmount;
    public Currency FinalBalance;
    public String CashierName;
    public Int64 OperationId;
    public Int64 TerminalId;
    public String TerminalName;
    public Currency Amount_01;
    public Currency Amount_02;
  } // MBMovementData

  public class MBUserData
  {
    public Int32 UserId;
    public String UserName;
    public String FullName;

    public Int64? MobileBankId;
  }

  public class MobileBankFormConfig
  {
    public MobileBankDataInput DataInput { get; set; }

    public MBCardData CardData { get; set; }

    public MBUserData UserData { get; set; }

    public MobileBankFormConfig()
    {
      DataInput = MobileBankDataInput.CardMode;
      CardData = new MBCardData();
      UserData = new MBUserData();
    }
  }
}
