﻿//------------------------------------------------------------------------------
// Copyright © 2007-2009 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AntiMoneyLaundering.cs
//  
//   DESCRIPTION: Check Anti Money Laundering
//                Class: AntiMoneyLaundering (Anti Money Laundering Operations)
//
// REVISION HISTORY:
// 
// Date        Author  Description
// ----------- ------- ----------------------------------------------------------
// 23-JAN-2017 JML     First release.
// 23-JAN-2017 JML     Fixed Bug 21868: Allows to perform Buy-In exceeding the number configured in the external PLD.
// 24-JAN-2017 JML     Fixed Bug 23372: Can not do recharges in an environment without anti-money laundering.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace WSI.Common
{

  public class AntiMoneyLaunderingInputParameters
  {
    public Int64 m_account_id;
    public ENUM_CREDITS_DIRECTION m_credits_direction;
    public Currency m_amount;
  }

  public class AntiMoneyLaunderingOutputParameters
  {
    public ENUM_ANTI_MONEY_LAUNDERING_LEVEL m_anti_m_l_level;
    public Boolean m_threshold_crossed;
    public Boolean m_player_edit_optional;
    public StringBuilder m_sb_msg;
    public Boolean m_player_decides_if_continue;
    public Boolean m_complete_process;
  }

  public static class AntiMoneyLaundering
  {

    public static Boolean CheckAntiMoneyLaundering(AntiMoneyLaunderingInputParameters InputParameters, out AntiMoneyLaunderingOutputParameters OutputParameters)
    {
      Boolean _account_already_registered;
      String _gp_prefix;

      OutputParameters = new AntiMoneyLaunderingOutputParameters();

      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (!Accounts.CheckAntiMoneyLaunderingFilters(InputParameters.m_account_id, InputParameters.m_credits_direction, InputParameters.m_amount, _db_trx.SqlTransaction,
                                                      out OutputParameters.m_anti_m_l_level, out OutputParameters.m_threshold_crossed, out _account_already_registered))
        {
          return false;
        }
      }

      if (OutputParameters.m_anti_m_l_level == ENUM_ANTI_MONEY_LAUNDERING_LEVEL.None)
      {
        return true;
      }

      switch (InputParameters.m_credits_direction)
      {
        case ENUM_CREDITS_DIRECTION.Recharge:
          _gp_prefix = "Recharge";
          break;
        case ENUM_CREDITS_DIRECTION.Redeem:
        default:
          _gp_prefix = "Prize";
          break;
      }

      OutputParameters.m_player_edit_optional = false;
      OutputParameters.m_player_decides_if_continue = false;
      OutputParameters.m_complete_process = true;

      OutputParameters.m_sb_msg = new StringBuilder();

      switch (OutputParameters.m_anti_m_l_level)
      {
        case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.IdentificationWarning:
          // 310 ==> "El cliente ha superado el límite 310 SMVGFD, en breve tendrá que identificarse."
          // 310 ==> "Por la ley antilavado de dinero, el cliente deberá identificarse en próximas recargas/retiros."
          if (!_account_already_registered)
          {
            if (OutputParameters.m_threshold_crossed)
            {
              OutputParameters.m_player_decides_if_continue = true;
              OutputParameters.m_sb_msg.AppendLine(GeneralParam.GetString("AntiMoneyLaundering", _gp_prefix + ".Identification.Warning.Message"));
              OutputParameters.m_sb_msg.AppendLine("");
              OutputParameters.m_sb_msg.AppendLine(Resource.String("STR_MSG_ANTIMONEYLAUNDERING_03")); //¿Continuar con la operación?";
            }
          }
          break;

        case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.Identification:
          // 325 ==> "El cliente ha superado el límite 325 SMVGFD. Por la ley antilavado de dinero, debe identificarse."
          if ((!_account_already_registered) || (OutputParameters.m_threshold_crossed && GeneralParam.GetBoolean("AntiMoneyLaundering", "ExternalControl.Enabled")))
          {
            OutputParameters.m_player_edit_optional = true;
            OutputParameters.m_complete_process = false;
            // La operación de recarga/retiro no puede realizarse.
            OutputParameters.m_sb_msg.AppendLine(Resource.String(_gp_prefix == "Recharge" ? "STR_MSG_ANTIMONEYLAUNDERING_04" : "STR_MSG_ANTIMONEYLAUNDERING_05"));
            OutputParameters.m_sb_msg.AppendLine("");
            OutputParameters.m_sb_msg.AppendLine(GeneralParam.GetString("AntiMoneyLaundering", _gp_prefix + ".Identification.Message"));
            OutputParameters.m_sb_msg.AppendLine("");
            if (!GeneralParam.GetBoolean("AntiMoneyLaundering", "ExternalControl.Enabled"))
            {
              OutputParameters.m_sb_msg.AppendLine(Resource.String("STR_MSG_ANTIMONEYLAUNDERING_02")); //¿Identificar ahora?";
            }
          }
          break;

        case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.ReportWarning:
          // 620 ==> "El cliente superará en breve el límite 625 SMVGFD para ser reportado al SAT."
          // 620 ==> "Por la ley antilavado de dinero, el cliente será reportado al SAT en próximas recargas/retiros."
          if (!_account_already_registered)
          {
            OutputParameters.m_player_edit_optional = true;
            OutputParameters.m_complete_process = false;
            // La operación de recarga/retiro no puede realizarse.
            OutputParameters.m_sb_msg.AppendLine(Resource.String(_gp_prefix == "Recharge" ? "STR_MSG_ANTIMONEYLAUNDERING_04" : "STR_MSG_ANTIMONEYLAUNDERING_05"));
            OutputParameters.m_sb_msg.AppendLine("");
            OutputParameters.m_sb_msg.AppendLine(Resource.String("STR_MSG_ANTIMONEYLAUNDERING_01")); //El cliente debe identificarse para realizar la operación.";
            OutputParameters.m_sb_msg.AppendLine(GeneralParam.GetString("AntiMoneyLaundering", _gp_prefix + ".Report.Warning.Message"));
            OutputParameters.m_sb_msg.AppendLine("");
            OutputParameters.m_sb_msg.AppendLine(Resource.String("STR_MSG_ANTIMONEYLAUNDERING_02")); //¿Identificar ahora?";
          }
          else
          {
            if (OutputParameters.m_threshold_crossed)
            {
              OutputParameters.m_player_decides_if_continue = true;
              OutputParameters.m_sb_msg.AppendLine(GeneralParam.GetString("AntiMoneyLaundering", _gp_prefix + ".Report.Warning.Message"));
              OutputParameters.m_sb_msg.AppendLine("");
              OutputParameters.m_sb_msg.AppendLine(Resource.String("STR_MSG_ANTIMONEYLAUNDERING_03")); //¿Continuar con la operación?";
            }
          }
          break;

        case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.Report:
          Boolean _show_report_message;

          _show_report_message = GeneralParam.GetBoolean("AntiMoneyLaundering", _gp_prefix + ".Report.ShowMessage");

          // 645 ==> "Por la ley antilavado de dinero, el cliente está a punto de ser incluído en un reporte para el SAT."
          if (!_account_already_registered)
          {
            OutputParameters.m_player_edit_optional = true;
            OutputParameters.m_complete_process = false;
            // La operación de recarga/retiro no puede realizarse.
            OutputParameters.m_sb_msg.AppendLine(Resource.String(_gp_prefix == "Recharge" ? "STR_MSG_ANTIMONEYLAUNDERING_04" : "STR_MSG_ANTIMONEYLAUNDERING_05"));
            OutputParameters.m_sb_msg.AppendLine("");
            OutputParameters.m_sb_msg.AppendLine(Resource.String("STR_MSG_ANTIMONEYLAUNDERING_01"));//El cliente debe identificarse para realizar la operación.";
            OutputParameters.m_sb_msg.AppendLine("");
            OutputParameters.m_sb_msg.AppendLine(GeneralParam.GetString("AntiMoneyLaundering", _gp_prefix + ".Report.Message"));
            OutputParameters.m_sb_msg.AppendLine("");
            OutputParameters.m_sb_msg.AppendLine(Resource.String("STR_MSG_ANTIMONEYLAUNDERING_02"));//¿Identificar ahora?";
          }
          else
          {
            if (OutputParameters.m_threshold_crossed)
            {
              if (_show_report_message)
              {
                OutputParameters.m_player_decides_if_continue = true;
                OutputParameters.m_sb_msg.AppendLine(GeneralParam.GetString("AntiMoneyLaundering", _gp_prefix + ".Report.Message"));
                OutputParameters.m_sb_msg.AppendLine("");
                OutputParameters.m_sb_msg.AppendLine(Resource.String("STR_MSG_ANTIMONEYLAUNDERING_03")); //¿Continuar con la operación?";
              }
            }
          }
          break;
        case ENUM_ANTI_MONEY_LAUNDERING_LEVEL.MaxAllowed:

          if (!_account_already_registered)
          {
            OutputParameters.m_player_edit_optional = true;
            OutputParameters.m_complete_process = false;
            // La operación de recarga/retiro no puede realizarse.
            OutputParameters.m_sb_msg.AppendLine(Resource.String(_gp_prefix == "Recharge" ? "STR_MSG_ANTIMONEYLAUNDERING_04" : "STR_MSG_ANTIMONEYLAUNDERING_05"));
            OutputParameters.m_sb_msg.AppendLine("");
            OutputParameters.m_sb_msg.AppendLine(Resource.String("STR_MSG_ANTIMONEYLAUNDERING_01"));//El cliente debe identificarse para realizar la operación.";
            OutputParameters.m_sb_msg.AppendLine("");
            OutputParameters.m_sb_msg.AppendLine(GeneralParam.GetString("AntiMoneyLaundering", _gp_prefix + ".Report.Message"));
            OutputParameters.m_sb_msg.AppendLine("");
            OutputParameters.m_sb_msg.AppendLine(Resource.String("STR_MSG_ANTIMONEYLAUNDERING_02"));//¿Identificar ahora?";
          }
          else
          {
            OutputParameters.m_player_decides_if_continue = true;
            OutputParameters.m_sb_msg.AppendLine("");
            OutputParameters.m_sb_msg.AppendLine(GeneralParam.GetString("AntiMoneyLaundering", _gp_prefix + ".MaxAllowed.Message"));
            OutputParameters.m_sb_msg.AppendLine("");
            OutputParameters.m_sb_msg.AppendLine(Resource.String("STR_MSG_ANTIMONEYLAUNDERING_03")); //¿Continuar con la operación?";
          }

          break;

        default:
          Log.Error("CheckAntiMoneyLaundering: Account " + InputParameters.m_account_id.ToString() + ". Anti MoneyLaundering level unknown: " + OutputParameters.m_anti_m_l_level.ToString() + ".");

          return false;
      }

      return true;
    }
  }
}
