//------------------------------------------------------------------------------
// Copyright � 2014 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Progressives.cs
// 
//   DESCRIPTION: Class to manage operations
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 11-AUG-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-AUG-2014 JMM    First release.
// 19-AUG-2014 FJC    Add Function "GetProgressives"
// 04-SEP-2014 JCO    Added ProgressiveJackpotPaid() and RegisterProgressiveJackpotAwarded().
// 21-OCT-2014 JMM    Fixed bug WIG-1551: Error on loading levels when there are no records on TERMINAL_SAS_METERS
// 16-FEB-2015 MPO    WIG-2066: It Shouldn't provisioning when the level doesn't exists
// 09-MAR-2015 FJC    Fixed Bug: WIG-2144
// 22-ABR-2016 JML    Product Backlog Item 9758:Gaming Tables (Fase 1): General cage report
// 22-NOV-2016 JMM    PBI 19744:Pago autom�tico de handpays
// 25-MAY-2017 DPC    Bug 21257:Error de c�lculo tito netwin
// 04-JUL-2017 DHA    Bug 28544:WIGOS-3367 Sometimes cashier does not accept cashing a Jackpot handpay
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common
{
  public static class Progressives
  {
    public class ProgressiveProvision
    {
      private Int64 m_provision_id;
      private Int64 m_cage_session_id;
      private Int64 m_progressive_id;
      private DateTime m_creation_date;
      private DateTime m_hour_from;
      private DateTime m_hour_to;
      private Decimal m_amount;
      private Decimal m_theoretical_amount;
      private DataTable m_levels;
      private DataTable m_terminals;
      private Decimal m_total_cage_provision;
      private Decimal m_total_terminals_provision;
      private DateTime m_progressive_last_provisioned_hour;
      private Decimal m_total_terminals_played;
      private Int32 m_status;
      private Int32 m_gui_user_id;
      private Decimal m_current_amount;

      public Int64 ProvisionId
      {
        get { return m_provision_id; }
        set { m_provision_id = value; }
      }

      public Int64 CageSessionId
      {
        get { return m_cage_session_id; }
        set { m_cage_session_id = value; }
      }

      public Int64 ProgressiveId
      {
        get { return m_progressive_id; }
        set { m_progressive_id = value; }
      }

      public DateTime CreationDate
      {
        get { return m_creation_date; }
        set { m_creation_date = value; }
      }

      public DateTime HourFrom
      {
        get { return m_hour_from; }
        set { m_hour_from = value; }
      }

      public DateTime HourTo
      {
        get { return m_hour_to; }
        set { m_hour_to = value; }
      }

      public Decimal Amount
      {
        get { return m_amount; }
        set { m_amount = value; }
      }

      public Decimal TheoreticalAmount
      {
        get { return m_theoretical_amount; }
        set { m_theoretical_amount = value; }
      }

      public DataTable Levels
      {
        get { return m_levels; }
        set { m_levels = value; }
      }

      public DataTable Terminals
      {
        get { return m_terminals; }
        set { m_terminals = value; }
      }

      public Decimal TotalCageProvision
      {
        get { return m_total_cage_provision; }
        set { m_total_cage_provision = value; }
      }

      public Decimal TotalTerminalsProvision
      {
        get { return m_total_terminals_provision; }
        set { m_total_terminals_provision = value; }
      }

      public DateTime ProgressiveLastProvisionedHour
      {
        get { return m_progressive_last_provisioned_hour; }
        set { m_progressive_last_provisioned_hour = value; }
      }

      public Decimal TotalTerminalsPlayed
      {
        get { return m_total_terminals_played; }
        set { m_total_terminals_played = value; }
      }

      public Int32 Status
      {
        get { return m_status; }
        set { m_status = value; }
      }

      public Int32 GuiUserId
      {
        get { return m_gui_user_id; }
        set { m_gui_user_id = value; }
      }

      public Decimal CurrentAmount
      {
        get { return m_current_amount; }
        set { m_current_amount = value; }
      }
    }

    #region Enums

    public enum PROGRESSIVE_STATUS
    {
      NOT_ACTIVE = 0,
      ACTIVE = 1
    }

    public enum PROGRESSIVE_PROVISION_STATUS
    {
      ACTIVE = 0,
      CANCELED = 1
    }

    public enum PROGRESSIVE_PROVISION_INPUT_MODE
    {
      COUNTER_READING = 0,
      THEORETICAL = 1,
    }

    public enum PROGRESSIVE_PROVISION_DISTRIBUTION_MODE
    {
      NONE = 0,
      EQUALLY = 1,
      THEORETICAL = 2,
    }

    public enum PROGRESSIVE_PROVISION_GOES_TO_CAGE
    {
      NOT = 0,
      YES = 1,
    }

    public enum SITE_JACKPOT_PROVISION_MODE
    {
      NONE = 0,
      PROVISION_WITHOUT_CAGE_METER_INCREMENT = 1,
      PROVISION_WITH_CAGE_METER_INCREMENT = 2
    }

    #endregion // Enums

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE : get previous amount for progressive.
    //
    //  PARAMS :
    //      - INPUT:
    //          - ProgressiveId
    //
    //      - OUTPUT :
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - Decimal
    //
    private static Decimal GetPreviousAmount(Int64 ProgressiveId)
    {

      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetPreviousAmount(ProgressiveId, _db_trx.SqlTransaction);

      } // DB_TRX
    } // GetPreviousAmount

    //------------------------------------------------------------------------------
    // PURPOSE : get previous amount for progressive.
    //
    //  PARAMS :
    //      - INPUT:
    //          - ProgressiveId
    //          - Trx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Decimal
    //
    public static Decimal GetPreviousAmount(Int64 ProgressiveId, SqlTransaction SqlTrx)
    {
      SqlCommand _sql_cmd;
      StringBuilder _sql_str;
      Object _previous_amount;

      try
      {
        //Search the last provided hour
        _sql_str = new StringBuilder();
        _sql_str.AppendLine("   SELECT   TOP 1 PGP_CURRENT_AMOUNT             ");
        _sql_str.AppendLine("     FROM   PROGRESSIVES_PROVISIONS              ");
        _sql_str.AppendLine("    WHERE   PGP_PROGRESSIVE_ID = @pProgressiveId ");
        _sql_str.AppendLine(" ORDER BY   PGP_PROVISION_ID DESC                ");

        _sql_cmd = new SqlCommand(_sql_str.ToString());
        _sql_cmd.Connection = SqlTrx.Connection;
        _sql_cmd.Transaction = SqlTrx;
        _sql_cmd.Parameters.Add("@pProgressiveId", SqlDbType.BigInt).Value = ProgressiveId;

        _previous_amount = _sql_cmd.ExecuteScalar();
        if (_previous_amount == DBNull.Value)
        {
          return 0;
        }

        return (Decimal)_previous_amount;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return 0;
      }
    } // GetPreviousAmount

    //------------------------------------------------------------------------------
    // PURPOSE : Creates the handpay in table Handpay.
    //
    //  PARAMS :
    //      - INPUT:  
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    private static Boolean DB_InsertProgressiveSpecialHandpay(Int64 ProgressiveId, Int32 ProgLevel, Decimal ProgAmount, Int32 TerminalId, out Int64 HandpayId, SqlTransaction SqlTrx)
    {
      Int32 _num_rows_inserted;
      SqlParameter _param_handpay_id;

      String _egm_iso;
      Decimal _egm_amt;
      Decimal _sys_amt;

      HandpayId = 0;

      String _sql_query = "INSERT INTO HANDPAYS(HP_TERMINAL_ID " +
                                            " , HP_DATETIME " +
                                            " , HP_AMOUNT " +
                                            " , HP_TE_NAME " +
                                            " , HP_TE_PROVIDER_ID " +
                                            " , HP_TYPE " +
                                            " , HP_LEVEL " +
                                            " , HP_PROGRESSIVE_ID " +
                                            " , HP_AMT0 " +
                                            " , HP_CUR0) " +
                                      " VALUES (@TerminalId " +
                                            " , @DateTime " +
                                            " , @Amount " +
                                            " , @TermName " +
                                            " , @ProviderId " +
                                            " , @HandpayType " +
                                            " , @pLevel " +
                                            " , @pProgressiveID " +
                                            " , @pAmount0 " +
                                            " , @pCurren0) " +
                                            " SET @pHandPayId = SCOPE_IDENTITY() ";

      try
      {
        // Get info terminal        
        Terminal.TerminalInfo _terminal_info;
        Terminal.Trx_GetTerminalInfo(TerminalId, out _terminal_info, SqlTrx);
        TITO.HandPay.GetFloorDualCurrency(ProgAmount, TerminalId, out _sys_amt, out _egm_amt, out _egm_iso, SqlTrx);

        using (SqlCommand _sql_command = new SqlCommand(_sql_query, SqlTrx.Connection, SqlTrx))
        {
          _sql_command.Parameters.Add("@TerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_command.Parameters.Add("@DateTime", SqlDbType.DateTime).Value = WGDB.Now;
          _sql_command.Parameters.Add("@Amount", SqlDbType.Money).Value = _sys_amt;
          _sql_command.Parameters.Add("@TermName", SqlDbType.NVarChar).Value = _terminal_info.Name;
          _sql_command.Parameters.Add("@ProviderId", SqlDbType.NVarChar).Value = _terminal_info.ProviderName;
          _sql_command.Parameters.Add("@HandpayType", SqlDbType.Int).Value = HANDPAY_TYPE.SPECIAL_PROGRESSIVE;
          _sql_command.Parameters.Add("@pLevel", SqlDbType.Int).Value = ProgLevel;
          _sql_command.Parameters.Add("@pProgressiveID", SqlDbType.Int).Value = ProgressiveId;
          _sql_command.Parameters.Add("@pAmount0", SqlDbType.Money).Value = (Decimal)_egm_amt;
          _sql_command.Parameters.Add("@pCurren0", SqlDbType.NVarChar, 3).Value = _egm_iso;
          _param_handpay_id = _sql_command.Parameters.Add("@pHandPayId", SqlDbType.BigInt);
          _param_handpay_id.Direction = ParameterDirection.Output;

          _num_rows_inserted = _sql_command.ExecuteNonQuery();

          HandpayId = (Int64)_param_handpay_id.Value;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return _num_rows_inserted == 1;

    } // DB_InsertProgressiveSpecialHandpay

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the date corresponding to the last provision made to the given ProgressiveId
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveId    
    //
    //      - OUTPUT :
    //            - LastProvisionDate
    //
    // RETURNS :
    //      - Boolean
    //
    private static Boolean GetLastProvisionDate(Int64 ProgressiveId, out DateTime LastProvisionDate)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetLastProvisionDate(ProgressiveId, out LastProvisionDate, _db_trx.SqlTransaction);

      } // DB_TRX

    } // GetLastProvisionDate

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the date corresponding to the last provision made to the given ProgressiveId
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveId
    //            - Trx
    //
    //      - OUTPUT :
    //            - LastProvisionDate
    //
    // RETURNS :
    //      - Boolean
    //
    private static Boolean GetLastProvisionDate(Int64 ProgressiveId, out DateTime LastProvisionDate, SqlTransaction SqlTrx)
    {
      SqlCommand _sql_cmd;
      StringBuilder _sql_str;
      Object _last_provision_date;

      LastProvisionDate = DateTime.MinValue;

      try
      {
        //Search the last provided hour
        _sql_str = new StringBuilder();
        _sql_str.AppendLine("SELECT   MAX (PGP_HOUR_TO)                   ");
        _sql_str.AppendLine("  FROM   PROGRESSIVES_PROVISIONS             ");
        _sql_str.AppendLine(" WHERE   PGP_PROGRESSIVE_ID = @pProgressiveId");
        _sql_str.AppendLine("   AND   PGP_STATUS = @pStatus               ");

        _sql_cmd = new SqlCommand(_sql_str.ToString());
        _sql_cmd.Connection = SqlTrx.Connection;
        _sql_cmd.Transaction = SqlTrx;
        _sql_cmd.Parameters.Add("@pProgressiveId", SqlDbType.BigInt).Value = ProgressiveId;
        _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = PROGRESSIVE_PROVISION_STATUS.ACTIVE;

        _last_provision_date = _sql_cmd.ExecuteScalar();
        if (_last_provision_date == DBNull.Value)
        {
          //No previous provision
          return true;
        }

        LastProvisionDate = (DateTime)_last_provision_date;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // GetLastProvisionDate

    //------------------------------------------------------------------------------
    // PURPOSE : Checks the consistency of the provision amounts.
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveId
    //            - Trx
    //
    //      - OUTPUT :
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    private static Boolean CheckProvision(Int64 ProvisionId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Decimal _provision_amount;
      Decimal _levels_amount;
      Decimal _terminals_amount;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   PGP_AMOUNT                      ");
        _sb.AppendLine("   FROM   PROGRESSIVES_PROVISIONS         ");
        _sb.AppendLine("  WHERE   PGP_PROVISION_ID = @pProvisionId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pProvisionId", SqlDbType.BigInt).Value = ProvisionId;

          _provision_amount = (Decimal)_cmd.ExecuteScalar();
        }

        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   SUM (PPL_AMOUNT)                  ");
        _sb.AppendLine("   FROM   PROGRESSIVES_PROVISIONS_LEVELS    ");
        _sb.AppendLine("  WHERE   PPL_PROVISION_ID = @pProvisionId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pProvisionId", SqlDbType.BigInt).Value = ProvisionId;

          _levels_amount = (Decimal)_cmd.ExecuteScalar();
        }

        if (_provision_amount != _levels_amount)
        {
          Log.Message("Inconsistency found on provision amounts: ");
          Log.Message(String.Format(" - Provision amount: {0}", Currency.Format(_provision_amount, CurrencyExchange.GetNationalCurrency())));
          Log.Message(String.Format(" - Levels amount: {0}", Currency.Format(_levels_amount, CurrencyExchange.GetNationalCurrency())));

          return false;
        }

        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   SUM (PPT_AMOUNT)                  ");
        _sb.AppendLine("   FROM   PROGRESSIVES_PROVISIONS_TERMINALS ");
        _sb.AppendLine("  WHERE   PPT_PROVISION_ID = @pProvisionId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pProvisionId", SqlDbType.BigInt).Value = ProvisionId;

          _terminals_amount = (Decimal)_cmd.ExecuteScalar();
        }

        if (_provision_amount != _terminals_amount)
        {
          Log.Message("Inconsistency found on provision amounts: ");
          Log.Message(String.Format(" - Provision amount: {0}", Currency.Format(_provision_amount, CurrencyExchange.GetNationalCurrency())));
          Log.Message(String.Format(" - Terminals amount: {0}", Currency.Format(_terminals_amount, CurrencyExchange.GetNationalCurrency())));

          return false;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      if ((_provision_amount == _levels_amount) && (_provision_amount == _terminals_amount))
      {
        return true;
      }

      Log.Message("Inconsistency found on provision amounts.");

      return false;

    } // CheckProvision

    //------------------------------------------------------------------------------
    // PURPOSE : Actions to do when we have to pay a progressive jackpot.
    //
    //  PARAMS :
    //      - INPUT:
    //
    //      - OUTPUT :
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //    
    public static Boolean SubstractProgressiveAmount(Int64 ProgressiveId, Int32 ProgLevel, Decimal ProgAmount, Int32 TerminalId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Object _obj;
      Boolean _has_level = false;

      try
      {
        // Gets if the progressive has that level.
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   PGL_PROGRESSIVE_ID                    ");
        _sb.AppendLine("   FROM   PROGRESSIVES_LEVELS                   ");
        _sb.AppendLine("  WHERE   PGL_PROGRESSIVE_ID = @pProgressiveId  ");
        _sb.AppendLine("    AND   PGL_LEVEL_ID = @pProgressiveLevelId   ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pProgressiveId", SqlDbType.BigInt).Value = ProgressiveId;
          _sql_cmd.Parameters.Add("@pProgressiveLevelId", SqlDbType.BigInt).Value = ProgLevel;

          _obj = _sql_cmd.ExecuteScalar();
          if (_obj != null)
          {
            _has_level = true;
          }
        }

        if (!_has_level)
        {
          return true;
        }

        // Update progressive amount.
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   PROGRESSIVES                         ");
        _sb.AppendLine("   SET   PGS_AMOUNT = PGS_AMOUNT - @pAmount   ");
        _sb.AppendLine(" WHERE   PGS_PROGRESSIVE_ID = @pProgressiveID ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pProgressiveID", SqlDbType.BigInt).Value = ProgressiveId;
          _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = ProgAmount;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Message("An error occurred updating PROGRESSIVES: Rows affected <> 1.");

            return false;
          }
        }

        // Updates the level's amount of the progressive.
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   PROGRESSIVES_LEVELS                    ");
        _sb.AppendLine("   SET   PGL_AMOUNT = PGL_AMOUNT - @pAmount     ");
        _sb.AppendLine(" WHERE   PGL_PROGRESSIVE_ID = @pProgressiveId   ");
        _sb.AppendLine("   AND   PGL_LEVEL_ID = @pProgressiveLevelId    ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pProgressiveId", SqlDbType.BigInt).Value = ProgressiveId;
          _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = ProgAmount;
          _sql_cmd.Parameters.Add("@pProgressiveLevelId", SqlDbType.BigInt).Value = ProgLevel;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Message("An error occurred updating PROGRESSIVES_LEVELS: Rows affected <> 1.");

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // SubstractProgressiveAmount

    #endregion // Private Functions

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Return true if exist a provision on indicated date
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveId
    //            - ProvisionDate
    //
    //      - OUTPUT :
    //            - LastProvisionDate
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean ExistsProvisionOnDate(Int64 ProgressiveId, DateTime ProvisionDate)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return ExistsProvisionOnDate(ProgressiveId, ProvisionDate, _db_trx.SqlTransaction);

      } // DB_TRX

    } // ExistsProvisionOnDate

    //------------------------------------------------------------------------------
    // PURPOSE : Return true if exist a provision on indicated date
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveId
    //            - ProvisionDate
    //            - Trx
    //
    //      - OUTPUT :
    //            - LastProvisionDate
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean ExistsProvisionOnDate(Int64 ProgressiveId, DateTime ProvisionDate, SqlTransaction SqlTrx)
    {
      SqlCommand _sql_cmd;
      StringBuilder _sql_str;
      Object _exists_provision;

      try
      {
        //Search the last provided hour
        _sql_str = new StringBuilder();
        _sql_str.AppendLine("SELECT   SUM (1)                              ");
        _sql_str.AppendLine("  FROM   PROGRESSIVES_PROVISIONS              ");
        _sql_str.AppendLine(" WHERE   PGP_PROGRESSIVE_ID = @pProgressiveId ");
        _sql_str.AppendLine("   AND   PGP_HOUR_FROM     <= @pProvisionDate ");
        _sql_str.AppendLine("   AND   PGP_HOUR_TO       >= @pProvisionDate ");

        _sql_cmd = new SqlCommand(_sql_str.ToString());
        _sql_cmd.Connection = SqlTrx.Connection;
        _sql_cmd.Transaction = SqlTrx;
        _sql_cmd.Parameters.Add("@pProgressiveId", SqlDbType.BigInt).Value = ProgressiveId;
        _sql_cmd.Parameters.Add("@pProvisionDate", SqlDbType.DateTime).Value = ProvisionDate;

        _exists_provision = _sql_cmd.ExecuteScalar();
        if (_exists_provision == DBNull.Value)
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // ExistsProvisionOnDate

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the dates corresponding to the next possible provision period
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveId
    //
    //      - OUTPUT :
    //            - HourTo
    //            - HourFrom
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean GetDefaultPeriod(Int64 ProgressiveId, out DateTime HourFrom, out DateTime HourTo, out DateTime TopLimitHour)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetDefaultPeriod(ProgressiveId, out HourFrom, out HourTo, out TopLimitHour, _db_trx.SqlTransaction);

      } // DB_TRX

    } // GetPeriod

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the dates corresponding to the next possible provision period
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveId    
    //            - Trx 
    //
    //      - OUTPUT :
    //            - HourTo
    //            - HourFrom
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean GetDefaultPeriod(Int64 ProgressiveId, out DateTime HourFrom, out DateTime HourTo, out DateTime TopLimitHour, SqlTransaction SqlTrx)
    {
      Int32 _hour;
      Int32 _minutes;

      //From time part, only get the hour.  Dismiss anything else      
      TopLimitHour = WGDB.Now;
      _hour = TopLimitHour.Hour;
      _minutes = TopLimitHour.Minute;
      TopLimitHour = TopLimitHour.Subtract(TopLimitHour.TimeOfDay);
      TopLimitHour = TopLimitHour.AddHours(_hour);

      // Avoid provision the last finished hour until a quarter after to ensure statistics have the proper played amount
      if (_minutes < 15)
      {
        TopLimitHour = TopLimitHour.AddHours(-1);
      }

      // Default Hour to: Today's opening
      HourTo = Misc.TodayOpening();

      HourFrom = DateTime.MinValue;

      if (!GetLastProvisionDate(ProgressiveId, out HourFrom, SqlTrx))
      {
        return false;
      }

      if (HourFrom == DateTime.MinValue)
      {
        // There is no previous provision, so we take previous opening as date from
        HourFrom = Misc.TodayOpening().AddDays(-1);
      }

      // If there is a provision latter than today's opening, default hour to will be last complete hour
      if (HourTo <= HourFrom)
      {
        HourTo = TopLimitHour;
      }

      return true;
    } // GetPeriod

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the levels from a progressive and its current amounts
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveId    
    //            - HourFrom 
    //            - HourTo 
    //
    //      - OUTPUT :
    //            - DtProgressiveLevels
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean GetProgressiveLevels(Int64 ProgressiveId, DateTime HourFrom, DateTime HourTo, out DataTable DtProgressiveLevels)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetProgressiveLevels(ProgressiveId, HourFrom, HourTo, out DtProgressiveLevels, _db_trx.SqlTransaction);
      }
    } // GetProgressiveLevels

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the levels from a progressive and its current amounts
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveId    
    //            - HourFrom 
    //            - HourTo 
    //
    //      - OUTPUT :
    //            - DtProgressiveLevels
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean GetProgressiveLevels(Int64 ProgressiveId, DateTime HourFrom, DateTime HourTo, out DataTable DtProgressiveLevels, SqlTransaction SqlTrx)
    {
      StringBuilder _sql_str;

      DtProgressiveLevels = new DataTable("PROGRESSIVES_LEVELS");

      try
      {
        //Get all progressive levels
        _sql_str = new StringBuilder();
        _sql_str.AppendLine("    SELECT   PGL_LEVEL_ID                                    AS LEVEL_ID                   ");
        _sql_str.AppendLine("           , PGL_NAME                                        AS LEVEL_NAME                 ");
        _sql_str.AppendLine("           , PGL_CONTRIBUTION_PCT                            AS CONTRIBUTION_PCT           ");
        _sql_str.AppendLine("           , PGL_AMOUNT                                      AS LEVEL_AMOUNT               ");
        _sql_str.AppendLine("           , isnull (ROUND                                                                         ");
        _sql_str.AppendLine("            ( ROUND((   SELECT   sum (TSMH_METER_INCREMENT)                                ");
        _sql_str.AppendLine("                          FROM   TERMINAL_SAS_METERS_HISTORY                               ");
        _sql_str.AppendLine("                    INNER JOIN   TERMINAL_GROUPS ON TSMH_TERMINAL_ID = TG_TERMINAL_ID      ");
        _sql_str.AppendLine("                         WHERE   TSMH_METER_CODE = 0 AND TSMH_TYPE = @pTsmhType            ");
        _sql_str.AppendLine("                           AND   TG_ELEMENT_TYPE = @pElementType                           ");
        _sql_str.AppendLine("                           AND   TG_ELEMENT_ID   = @pProgressiveId                         ");
        _sql_str.AppendLine("                           AND   TSMH_DATETIME >= @pDateFrom AND TSMH_DATETIME < @pDateTo) ");
        _sql_str.AppendLine("              * (PGS_CONTRIBUTION_PCT / 100) / 100, 2)                                     ");
        _sql_str.AppendLine("            * (PGL_CONTRIBUTION_PCT / 100), 2), 0) as THEORETICAL_AMOUNT                       ");
        _sql_str.AppendLine("           , 0.0                                             AS PROVISION_AMOUNT           ");
        _sql_str.AppendLine("      FROM   PROGRESSIVES_LEVELS                                                           ");
        _sql_str.AppendLine("INNER JOIN   PROGRESSIVES ON PGS_PROGRESSIVE_ID = PGL_PROGRESSIVE_ID                       ");
        _sql_str.AppendLine("     WHERE   PGL_PROGRESSIVE_ID = @pProgressiveId                                          ");

        //using (SqlCommand _sql_cmd = new SqlCommand(_sql_str.ToString(), SqlTrx.Connection, SqlTrx))
        {
          SqlCommand _sql_cmd;

          _sql_cmd = new SqlCommand();
          _sql_cmd.CommandText = _sql_str.ToString();
          _sql_cmd.Connection = SqlTrx.Connection;
          _sql_cmd.Transaction = SqlTrx;

          _sql_cmd.Parameters.Add("@pProgressiveId", SqlDbType.BigInt).Value = ProgressiveId;
          _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = HourFrom;
          _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = HourTo;
          _sql_cmd.Parameters.Add("@pTsmhType", SqlDbType.Int).Value = (Int32)ENUM_SAS_METER_HISTORY.TSMH_HOURLY;
          _sql_cmd.Parameters.Add("@pElementType", SqlDbType.Int).Value = (Int32)EXPLOIT_ELEMENT_TYPE.PROGRESSIVE;

          // To optimize the query
          _sql_cmd = WGDB.ParametersToVariables(_sql_cmd);

          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(DtProgressiveLevels);
          }
        }

        //JMM 03-OCT-2014: Chapuza para implementar V2
        if (GetProvisionInputMode() == Progressives.PROGRESSIVE_PROVISION_INPUT_MODE.THEORETICAL)
        {
          foreach (DataRow _row in DtProgressiveLevels.Rows)
          {
            _row["PROVISION_AMOUNT"] = _row["THEORETICAL_AMOUNT"];
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetProgressiveLevels

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the terminals associated with the PROGRESSIVES
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveId
    //
    //      - OUTPUT :
    //            - DtProgressiveTerminals
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean GetProgressiveTerminals(Int64 ProgressiveId, out DataTable DtProgressiveTerminals)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetProgressiveTerminals(ProgressiveId, out DtProgressiveTerminals, _db_trx.SqlTransaction);
      }
    } // GetProgressiveTerminals

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the terminals associated with the PROGRESSIVES
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveId
    //            - SqlTrx
    //
    //      - OUTPUT :
    //            - DtProgressiveTerminals (DataTable)
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean GetProgressiveTerminals(Int64 ProgressiveId, out DataTable DtProgressiveTerminals, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      DtProgressiveTerminals = new DataTable();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   TG_TERMINAL_ID AS TERMINAL_ID ");
        _sb.AppendLine("   FROM   TERMINAL_GROUPS  ");
        _sb.AppendLine("  WHERE   TG_ELEMENT_TYPE = @pElementType ");
        _sb.AppendLine("    AND   TG_ELEMENT_ID   = @pProgressiveId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pElementType", SqlDbType.Int).Value = (Int32)EXPLOIT_ELEMENT_TYPE.PROGRESSIVE;
          _sql_cmd.Parameters.Add("@pProgressiveId", SqlDbType.BigInt).Value = ProgressiveId;

          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(DtProgressiveTerminals);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetProgressiveTerminals

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the string terminals list associated with the PROGRESSIVES
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveId
    //            - SqlTrx
    //
    //      - OUTPUT :
    //            - TerminalsList
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean GetProgressiveTerminalsStrList(Int64 ProgressiveId, out String TerminalsList, SqlTransaction SqlTrx)
    {
      DataTable _dt_terminals;
      TerminalsList = "";

      try
      {
        if (!GetProgressiveTerminals(ProgressiveId, out _dt_terminals, SqlTrx))
        {
          return false;
        }

        foreach (DataRow _dr in _dt_terminals.Rows)
        {
          if (TerminalsList.Length == 0)
          {
            TerminalsList += ((Int32)_dr["TERMINAL_ID"]).ToString();
          }
          else
          {
            TerminalsList += ", " + ((Int32)_dr["TERMINAL_ID"]).ToString();
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetProgressiveTerminalsStrList

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the progressive from terminal
    //
    //  PARAMS :
    //      - INPUT:
    //            - TerminalId
    //            - SqlTrx
    //            - LevelId
    //
    //      - OUTPUT :
    //            - ProgressiveId
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean GetProgressiveFromTerminal(Int64 TerminalId, out Int64 ProgressiveId, SqlTransaction SqlTrx, Int32 LevelId)
    {
      StringBuilder _sb;
      Object _obj;

      ProgressiveId = 0;

      _sb = new StringBuilder();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   TOP 1   TG_ELEMENT_ID                       ");
        _sb.AppendLine("           FROM   TERMINAL_GROUPS                     ");
        if (LevelId > 0)
        {
          _sb.AppendLine("   INNER JOIN   PROGRESSIVES_LEVELS                 ");
          _sb.AppendLine("           ON   PGL_PROGRESSIVE_ID = TG_ELEMENT_ID  ");
        }
        _sb.AppendLine("          WHERE   TG_ELEMENT_TYPE = @pElementType     ");
        _sb.AppendLine("            AND   TG_TERMINAL_ID  = @pTerminalId      ");
        if (LevelId > 0)
        {
          _sb.AppendLine("          AND   PGL_LEVEL_ID    = @pLevelId         ");
        }

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pElementType", SqlDbType.Int).Value = (Int32)EXPLOIT_ELEMENT_TYPE.PROGRESSIVE;
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;

          if (LevelId > 0)
          {
            _sql_cmd.Parameters.Add("@pLevelId", SqlDbType.Int).Value = LevelId;
          }

          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _obj = _sql_cmd.ExecuteScalar();
            if (_obj != null)
            {
              ProgressiveId = (Int64)_obj;

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetProgressiveFromTerminal

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the progressive from terminal
    //
    //  PARAMS :
    //      - INPUT:
    //            - TerminalId
    //            - SqlTrx
    //
    //      - OUTPUT :
    //            - ProgressiveId
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean GetProgressiveFromTerminal(Int64 TerminalId, out Int64 ProgressiveId, SqlTransaction SqlTrx)
    {
      return GetProgressiveFromTerminal(TerminalId, out ProgressiveId, SqlTrx, 0);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the progressive from terminal
    //
    //  PARAMS :
    //      - INPUT:
    //            - TerminalId
    //            - SqlTrx
    //
    //      - OUTPUT :
    //            - ProgressiveId
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean GetProgressiveNameFromTerminal(Int64 TerminalId, out Int64 ProgressiveId, out String Name, out Int32 NumLevels, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      Name = String.Empty;
      ProgressiveId = 0;
      NumLevels = 1;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("    SELECT   TOP 1 ");
        _sb.AppendLine("             PGS_NAME ");
        _sb.AppendLine("           , PGS_PROGRESSIVE_ID ");
        _sb.AppendLine("           , PGS_NUM_LEVELS ");
        _sb.AppendLine("      FROM   TERMINAL_GROUPS ");
        _sb.AppendLine("INNER JOIN   PROGRESSIVES ON PGS_PROGRESSIVE_ID = TG_ELEMENT_ID ");
        _sb.AppendLine("     WHERE   TG_ELEMENT_TYPE = @pElementType ");
        _sb.AppendLine("       AND   TG_TERMINAL_ID = @pTerminalId ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pElementType", SqlDbType.Int).Value = (Int32)EXPLOIT_ELEMENT_TYPE.PROGRESSIVE;
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return true;
            }

            Name = _reader.GetString(0);
            ProgressiveId = _reader.GetInt64(1);
            NumLevels = _reader.GetInt32(2);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetProgressiveFromTerminal

    //------------------------------------------------------------------------------
    // PURPOSE : Return all the progressives 
    //
    //  PARAMS :
    //      - INPUT:
    //
    //      - OUTPUT :
    //            - DtProgressives (DataTable)
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean GetProgressives(out DataTable DtProgressives)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetProgressives(out DtProgressives, _db_trx.SqlTransaction);
      }

    } // GetProgressives

    //------------------------------------------------------------------------------
    // PURPOSE : Return all the progressives 
    //
    //  PARAMS :
    //      - INPUT:
    //
    //      - OUTPUT :
    //            - DtProgressives (DataTable)
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean GetProgressives(out DataTable DtProgressives, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      DtProgressives = new DataTable();

      try
      {
        //SELECT pgs_progressive_id, pgs_name FROM progressives ORDER BY pgs_name
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   PGS_PROGRESSIVE_ID as PROGRESSIVE_ID   ");
        _sb.AppendLine("         , PGS_NAME           as PROGRESSIVE_NAME ");
        _sb.AppendLine("    FROM   PROGRESSIVES                           ");
        _sb.AppendLine("   WHERE   PGS_STATUS = @pProgressiveStatus       ");
        _sb.AppendLine("ORDER BY   PGS_NAME                               ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pProgressiveStatus", SqlDbType.Int).Value = PROGRESSIVE_STATUS.ACTIVE;

          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(DtProgressives);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetProgressives

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the terminals associated with the PROGRESSIVES and their played amount on a given period
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveId
    //            - HourFrom
    //            - HourTo
    //
    //      - OUTPUT :
    //            - DtProgressiveTerminals
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean GetPlayedFromProgressiveTerminals(Int64 ProgressiveId, DateTime HourFrom, DateTime HourTo, out DataTable DtProgressiveTerminals)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetPlayedFromProgressiveTerminals(ProgressiveId, HourFrom, HourTo, out DtProgressiveTerminals, _db_trx.SqlTransaction, false);
      }
    } // GetPlayedFromProgressiveTerminals

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the terminals associated with the PROGRESSIVES and their played amount on a given period
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveId
    //            - HourFrom
    //            - HourTo
    //
    //      - OUTPUT :
    //            - DtProgressiveTerminals
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean GetPlayedFromProgressiveTerminals(Int64 ProgressiveId, DateTime HourFrom, DateTime HourTo, out DataTable DtProgressiveTerminals, Boolean IsSiteJackPot)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetPlayedFromProgressiveTerminals(ProgressiveId, HourFrom, HourTo, out DtProgressiveTerminals, _db_trx.SqlTransaction, IsSiteJackPot);
      }
    } // GetPlayedFromProgressiveTerminals

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the terminals associated with the PROGRESSIVES and their played amount on a given period
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveId
    //            - HourFrom
    //            - HourTo
    //            - SqlTrx
    //      - OUTPUT :
    //            - DtProgressiveTerminals
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean GetPlayedFromProgressiveTerminals(Int64 ProgressiveId, DateTime HourFrom, DateTime HourTo, out DataTable DtProgressiveTerminals, SqlTransaction SqlTrx)
    {
      return GetPlayedFromProgressiveTerminals(ProgressiveId, HourFrom, HourTo, out DtProgressiveTerminals, SqlTrx, false);
    } // GetPlayedFromProgressiveTerminals

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the terminals associated with the PROGRESSIVES and their played amount on a given period
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveId
    //            - HourFrom
    //            - HourTo    
    //            - Trx
    //            - IsSiteJackPot
    //      - OUTPUT :
    //            - DtProgressiveTerminals (DataTable)
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean GetPlayedFromProgressiveTerminals(Int64 ProgressiveId, DateTime HourFrom, DateTime HourTo, out DataTable DtProgressiveTerminals, SqlTransaction SqlTrx, Boolean IsSiteJackPot)
    {
      StringBuilder _sql_str;
      PROGRESSIVE_PROVISION_DISTRIBUTION_MODE _distribution_mode;

      _sql_str = new StringBuilder();
      DtProgressiveTerminals = new DataTable();

      try
      {
        _sql_str = new StringBuilder();
        _sql_str.AppendLine("    SELECT   TE_TERMINAL_ID                                                  AS TERMINAL_ID            ");
        _sql_str.AppendLine("           , PV_NAME                                                         AS PROVIDER_NAME          ");
        _sql_str.AppendLine("           , TE_NAME                                                         AS TERMINAL_NAME          ");
        _sql_str.AppendLine("           , ISNULL(convert (money, sum (TSMH_METER_INCREMENT)) / 100, 0.0)  AS TERMINAL_PLAYED_AMOUNT ");
        _sql_str.AppendLine("           , 0.0                                                             AS EQUALLY_AMOUNT         ");
        _sql_str.AppendLine("           , 0.0                                                             AS AS_PLAYED_AMOUNT       ");
        _sql_str.AppendLine("           , 0.0                                                             AS PROVISION_AMOUNT       ");
        _sql_str.AppendLine("           , 0.0                                                             AS EQUALLY_DIFF_AMOUNT    ");
        _sql_str.AppendLine("           , 0.0                                                             AS AS_PLAYED_DIFF_AMOUNT  ");
        _sql_str.AppendLine("           , 0.0                                                             AS DIFF_AMOUNT            ");
        _sql_str.AppendLine("      FROM   TERMINAL_GROUPS                                                                           ");
        _sql_str.AppendLine("INNER JOIN   TERMINALS ON TG_TERMINAL_ID = TE_TERMINAL_ID                                              ");
        _sql_str.AppendLine("INNER JOIN   PROVIDERS ON TE_PROV_ID = PV_ID                                                           ");
        _sql_str.AppendLine(" LEFT JOIN   TERMINAL_SAS_METERS_HISTORY                                                               ");
        _sql_str.AppendLine("                 ON  TSMH_TERMINAL_ID = TG_TERMINAL_ID                                                 ");
        _sql_str.AppendLine("                 AND TSMH_METER_CODE = 0 AND TSMH_TYPE = @pTsmhType                                    ");
        _sql_str.AppendLine("                 AND TSMH_DATETIME >= @pDateFrom AND TSMH_DATETIME < @pDateTo                          ");
        _sql_str.AppendLine("     WHERE   TG_ELEMENT_TYPE = @pElementType                                                           ");
        if (!IsSiteJackPot)
        {
          _sql_str.AppendLine("     AND   TG_ELEMENT_ID   = @pProgressiveId                                                         ");
        }
        _sql_str.AppendLine("  GROUP BY   TE_TERMINAL_ID, PV_NAME, TE_NAME                                                          ");
        _sql_str.AppendLine("  ORDER BY   PV_NAME, TE_NAME                                                                          ");

        // using (SqlCommand _sql_cmd = new SqlCommand(_sql_str.ToString(), SqlTrx.Connection, SqlTrx))
        {
          SqlCommand _sql_cmd;

          _sql_cmd = new SqlCommand();
          _sql_cmd.CommandText = _sql_str.ToString();
          _sql_cmd.Connection = SqlTrx.Connection;
          _sql_cmd.Transaction = SqlTrx;
          if (!IsSiteJackPot)
          {
            _sql_cmd.Parameters.Add("@pElementType", SqlDbType.Int).Value = (Int32)EXPLOIT_ELEMENT_TYPE.PROGRESSIVE;
            _sql_cmd.Parameters.Add("@pProgressiveId", SqlDbType.BigInt).Value = ProgressiveId;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pElementType", SqlDbType.Int).Value = (Int32)EXPLOIT_ELEMENT_TYPE.JACKPOT;
          }
          _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = HourFrom;
          _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = HourTo;
          _sql_cmd.Parameters.Add("@pTsmhType", SqlDbType.Int).Value = (Int32)ENUM_SAS_METER_HISTORY.TSMH_HOURLY;

          // To optimize the query
          _sql_cmd = WGDB.ParametersToVariables(_sql_cmd);

          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(DtProgressiveTerminals);
          }
        }

        //JMM 03-OCT-2014: Chapuza para implementar V2
        if (!IsSiteJackPot)
        {
          _distribution_mode = GetProvisionDistributionMode();
        }
        else
        {
          _distribution_mode = GetSiteJackPotProvisionDistributionMode();
        }

        if (_distribution_mode == PROGRESSIVE_PROVISION_DISTRIBUTION_MODE.NONE)
        {
          return true;
        }

        foreach (DataRow _row in DtProgressiveTerminals.Rows)
        {
          switch (_distribution_mode)
          {
            case PROGRESSIVE_PROVISION_DISTRIBUTION_MODE.EQUALLY:
              _row["PROVISION_AMOUNT"] = _row["EQUALLY_AMOUNT"];
              break;

            case PROGRESSIVE_PROVISION_DISTRIBUTION_MODE.THEORETICAL:
              _row["PROVISION_AMOUNT"] = _row["AS_PLAYED_AMOUNT"];
              break;

            default:
              return true;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetPlayedFromProgressiveTerminals

    //------------------------------------------------------------------------------
    // PURPOSE : For each terminal the total provision is distributed according to its played amount.
    //
    //  PARAMS :
    //      - INPUT:
    //            - TotalProvision
    //            - ProgressiveId
    //            - TotalAmount
    //            - DtTerminalList
    //
    //      - OUTPUT :
    //            - TerminalsTotalPlayed
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean ProvisionDistribute(Decimal TotalProvision,Int64 ProgressiveId, Decimal TotalAmount, ref DataTable DtTerminalList, out Decimal TerminalsTotalPlayed)
    {
      Decimal _provision_amount_equally;
      Decimal _provision_amount_as_played;
      Decimal _provisioned_equally_amount;
      Decimal _provisioned_as_played_amount;
      Decimal _num_terminals;
      Decimal _total_played_amount;
      Boolean _distribute_equally;
      Decimal _previous_amount;
      Decimal _diff_amount_equally;
      Decimal _diff_amount_as_played;

      TerminalsTotalPlayed = 0;
      _provisioned_equally_amount = 0;
      _provisioned_as_played_amount = 0;
      _diff_amount_equally = 0;

      if (DtTerminalList.Rows.Count == 0)
      {
        Log.Message("ProvisionDistribute: No rows to distribute provision on DtTerminalList.");

        return false;
      }

      try
      {
        //Get total played
        foreach (DataRow _dr in DtTerminalList.Rows)
        {
          if (!_dr.IsNull("TERMINAL_PLAYED_AMOUNT"))
          {
            TerminalsTotalPlayed = TerminalsTotalPlayed + (Decimal)_dr["TERMINAL_PLAYED_AMOUNT"];
          }
        }

        _num_terminals = (Decimal)DtTerminalList.Rows.Count;
        _provision_amount_equally = TotalProvision;
        _provision_amount_as_played = TotalProvision;
        _total_played_amount = TerminalsTotalPlayed;
        _previous_amount = GetPreviousAmount(ProgressiveId);
        _diff_amount_equally = TotalAmount - _previous_amount;
        _diff_amount_as_played = TotalAmount - _previous_amount;

        //If there is no played amount on any terminal, distribute the provision equally
        _distribute_equally = (_total_played_amount == 0);

        //Calc the new values
        foreach (DataRow _dr in DtTerminalList.Rows)
        {
          //
          // EQUALLY
          //
          _dr["EQUALLY_AMOUNT"] = Math.Round(_provision_amount_equally / _num_terminals, 2, MidpointRounding.AwayFromZero);
          _provision_amount_equally -= (Decimal)_dr["EQUALLY_AMOUNT"];

          _dr["EQUALLY_DIFF_AMOUNT"] = Math.Round(_diff_amount_equally / _num_terminals, 2, MidpointRounding.AwayFromZero);
          _diff_amount_equally -= (Decimal)_dr["EQUALLY_DIFF_AMOUNT"];

          _num_terminals--;

          _provisioned_equally_amount += (Decimal)_dr["EQUALLY_AMOUNT"];

          //
          // AS PLAYED
          //
          if (_total_played_amount > 0)
          {
            _dr["AS_PLAYED_AMOUNT"] = Math.Round((_provision_amount_as_played / _total_played_amount) * (Decimal)_dr["TERMINAL_PLAYED_AMOUNT"], 2, MidpointRounding.AwayFromZero);
            _provision_amount_as_played -= (Decimal)_dr["AS_PLAYED_AMOUNT"];

            _dr["AS_PLAYED_DIFF_AMOUNT"] = Math.Round((_diff_amount_as_played / _total_played_amount) * (Decimal)_dr["TERMINAL_PLAYED_AMOUNT"], 2, MidpointRounding.AwayFromZero);
            _diff_amount_as_played -= (Decimal)_dr["AS_PLAYED_DIFF_AMOUNT"];

            _total_played_amount -= (Decimal)_dr["TERMINAL_PLAYED_AMOUNT"];
          }
          else
          {
            _dr["AS_PLAYED_AMOUNT"] = 0;
            _dr["AS_PLAYED_DIFF_AMOUNT"] = 0;
          }

          _provisioned_as_played_amount += (Decimal)_dr["AS_PLAYED_AMOUNT"];

          switch (GetProvisionDistributionMode())
          {
            case PROGRESSIVE_PROVISION_DISTRIBUTION_MODE.NONE:
              break;
            case PROGRESSIVE_PROVISION_DISTRIBUTION_MODE.THEORETICAL:
              _dr["PROVISION_AMOUNT"] = (Decimal)(_distribute_equally ? _dr["EQUALLY_AMOUNT"] : _dr["AS_PLAYED_AMOUNT"]);
              _dr["DIFF_AMOUNT"] = (Decimal)(_distribute_equally ? _dr["EQUALLY_DIFF_AMOUNT"] : _dr["AS_PLAYED_DIFF_AMOUNT"]);
              break;
            case PROGRESSIVE_PROVISION_DISTRIBUTION_MODE.EQUALLY:
              _dr["PROVISION_AMOUNT"] = (Decimal)_dr["EQUALLY_AMOUNT"];
              _dr["DIFF_AMOUNT"] = (Decimal)_dr["EQUALLY_DIFF_AMOUNT"];
              break;
          }
        }

        if (_provisioned_equally_amount != TotalProvision
            || (_provisioned_as_played_amount != TotalProvision && TerminalsTotalPlayed > 0))
        {
          Log.Message("ProvisionDistribute: Provision amounts don't match.");

          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // ProvisionDistribute


    //------------------------------------------------------------------------------
    // PURPOSE : For each terminal the total provision is distributed according to its played amount.
    //
    //  PARAMS :
    //      - INPUT:
    //            - TotalProvision
    //            - DtTerminalList
    //
    //      - OUTPUT :
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean GenerateProvision(ProgressiveProvision ProgressiveProvision, SqlTransaction SqlTrx, out List<String> ErrorOnTerminals)
    {
      StringBuilder _sql_query;
      SqlParameter _output_param;
      Int64 _provision_id;
      Int64 _cage_session_id;

      ErrorOnTerminals = new List<string>();

      try
      {
        _cage_session_id = 0;

        if (ProvisionGoesToCage() == PROGRESSIVE_PROVISION_GOES_TO_CAGE.YES && Cage.IsCageEnabled())
        {
          // Check cage amount matches provision amount
          if (ProgressiveProvision.Amount != ProgressiveProvision.TotalCageProvision)
          {
            Log.Message("Cage provision amout doesn't match provision amount.  Provision can't be made.");

            return false;
          }

          // Get Cage Session Id
          if (!Cage.GetCageDefaultSessionId(out _cage_session_id, SqlTrx))
          {
            Log.Message("An error occurred getting cage default session id.");

            return false;
          }
        }

        // Insert the provision record on PROGRESSIVES_PROVISIONS
        _sql_query = new StringBuilder();
        _sql_query.AppendLine("INSERT INTO   PROGRESSIVES_PROVISIONS");
        _sql_query.AppendLine("            ( PGP_PROGRESSIVE_ID     ");
        _sql_query.AppendLine("            , PGP_CREATED            ");
        _sql_query.AppendLine("            , PGP_HOUR_FROM          ");
        _sql_query.AppendLine("            , PGP_HOUR_TO            ");
        _sql_query.AppendLine("            , PGP_AMOUNT             ");
        _sql_query.AppendLine("            , PGP_THEORETICAL_AMOUNT ");
        _sql_query.AppendLine("            , PGP_CAGE_SESSION_ID    ");
        _sql_query.AppendLine("            , PGP_CAGE_AMOUNT        ");
        _sql_query.AppendLine("            , PGP_GUI_USER_ID        ");
        _sql_query.AppendLine("            , PGP_CURRENT_AMOUNT     ");
        _sql_query.AppendLine("            )                        ");
        _sql_query.AppendLine("     VALUES ( @pProgressiveID        ");
        _sql_query.AppendLine("            , GetDate()              ");
        _sql_query.AppendLine("            , @pHourFrom             ");
        _sql_query.AppendLine("            , @pHourTo               ");
        _sql_query.AppendLine("            , @pAmount               ");
        _sql_query.AppendLine("            , @pTheoreticalAmount    ");
        _sql_query.AppendLine("            , @pCageSessionID        ");
        _sql_query.AppendLine("            , @pCageAmount           ");
        _sql_query.AppendLine("            , @pGuiUserId            ");
        _sql_query.AppendLine("            , @pCurrentAmount        ");
        _sql_query.AppendLine("            )                        ");
        _sql_query.AppendLine(" SET @pPatternId = SCOPE_IDENTITY()  ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pProgressiveID", SqlDbType.BigInt).Value = ProgressiveProvision.ProgressiveId;
          _sql_cmd.Parameters.Add("@pHourFrom", SqlDbType.DateTime).Value = ProgressiveProvision.HourFrom;
          _sql_cmd.Parameters.Add("@pHourTo", SqlDbType.DateTime).Value = ProgressiveProvision.HourTo;
          _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = ProgressiveProvision.Amount;
          _sql_cmd.Parameters.Add("@pTheoreticalAmount", SqlDbType.Money).Value = ProgressiveProvision.TheoreticalAmount;
          _sql_cmd.Parameters.Add("@pCageSessionID", SqlDbType.BigInt).Value = _cage_session_id;
          _sql_cmd.Parameters.Add("@pCageAmount", SqlDbType.Money).Value = ProgressiveProvision.TotalCageProvision;
          _sql_cmd.Parameters.Add("@pGuiUserId", SqlDbType.Int).Value = ProgressiveProvision.GuiUserId;
          _sql_cmd.Parameters.Add("@pCurrentAmount", SqlDbType.Money).Value = ProgressiveProvision.CurrentAmount + ProgressiveProvision.Amount;

          _output_param = new SqlParameter("@pPatternId", SqlDbType.BigInt);
          _output_param.Direction = ParameterDirection.Output;
          _sql_cmd.Parameters.Add(_output_param);

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Message("An error occurred inserting into PROGRESSIVES_PROVISIONS table: Rows affected <> 1.");

            return false;
          }

          if (_output_param.Value == DBNull.Value)
          {
            Log.Message("An error occurred inserting into PROGRESSIVES_PROVISIONS table: No provision id returned.");

            return false;
          }

          _provision_id = (Int64)_output_param.Value;
        }

        // Update progressives table
        _sql_query = new StringBuilder();
        _sql_query.AppendLine("UPDATE   PROGRESSIVES                                              ");
        _sql_query.AppendLine("   SET   PGS_AMOUNT = PGS_AMOUNT + @pProvisionAmount               ");
        _sql_query.AppendLine("       , PGS_LAST_PROVISIONED_HOUR = @pLastProvisionedHour         ");
        _sql_query.AppendLine(" WHERE   PGS_PROGRESSIVE_ID = @pProgressiveID                      ");

        if (ProgressiveProvision.ProgressiveLastProvisionedHour == DateTime.MinValue)
        {
          _sql_query.AppendLine("   AND   PGS_LAST_PROVISIONED_HOUR IS NULL");
        }
        else
        {
          _sql_query.AppendLine("   AND   PGS_LAST_PROVISIONED_HOUR = @pPreviousLastProvisionedHour");
        }

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pProgressiveID", SqlDbType.BigInt).Value = ProgressiveProvision.ProgressiveId;
          _sql_cmd.Parameters.Add("@pProvisionAmount", SqlDbType.Money).Value = ProgressiveProvision.TotalTerminalsProvision;
          _sql_cmd.Parameters.Add("@pLastProvisionedHour", SqlDbType.DateTime).Value = ProgressiveProvision.HourTo;

          if (ProgressiveProvision.ProgressiveLastProvisionedHour != DateTime.MinValue)
          {
            _sql_cmd.Parameters.Add("@pPreviousLastProvisionedHour", SqlDbType.DateTime).Value = ProgressiveProvision.ProgressiveLastProvisionedHour;
          }

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Message("An error occurred updating PROGRESSIVES table: Rows affected <> 1.");

            return false;
          }
        }

        foreach (DataRow _dr in ProgressiveProvision.Levels.Rows)
        {
          // Update progressives levels table
          _sql_query = new StringBuilder();
          _sql_query.AppendLine("UPDATE   PROGRESSIVES_LEVELS                             ");
          _sql_query.AppendLine("   SET   PGL_AMOUNT = PGL_AMOUNT + @pLevelProvisionAmount");
          _sql_query.AppendLine(" WHERE   PGL_PROGRESSIVE_ID = @pProgressiveID            ");
          _sql_query.AppendLine("   AND   PGL_LEVEL_ID = @pLevelID                        ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pProgressiveID", SqlDbType.BigInt).Value = ProgressiveProvision.ProgressiveId;
            _sql_cmd.Parameters.Add("@pLevelID", SqlDbType.BigInt).Value = _dr["LEVEL_ID"];
            _sql_cmd.Parameters.Add("@pLevelProvisionAmount", SqlDbType.Money).Value = _dr["PROVISION_AMOUNT"];

            if (_sql_cmd.ExecuteNonQuery() != 1)
            {
              Log.Message("An error occurred updating PROGRESSIVES_LEVELS table: Rows affected <> 1.");

              return false;
            }
          }

          // Provision levels
          _sql_query = new StringBuilder();
          _sql_query.AppendLine("INSERT INTO   PROGRESSIVES_PROVISIONS_LEVELS ");
          _sql_query.AppendLine("            ( PPL_PROVISION_ID               ");
          _sql_query.AppendLine("            , PPL_LEVEL_ID                   ");
          _sql_query.AppendLine("            , PPL_AMOUNT                     ");
          _sql_query.AppendLine("            , PPL_THEORETICAL_AMOUNT         ");
          _sql_query.AppendLine("            , PPL_CURRENT_AMOUNT             ");
          _sql_query.AppendLine("            )                                ");
          _sql_query.AppendLine("     VALUES ( @pProvisionID                  ");
          _sql_query.AppendLine("            , @pLevelID                      ");
          _sql_query.AppendLine("            , @pAmount                       ");
          _sql_query.AppendLine("            , @pTheoreticalAmount            ");
          _sql_query.AppendLine("            , @pCurrentAmount                ");
          _sql_query.AppendLine("            )                                ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pProvisionID", SqlDbType.BigInt).Value = _provision_id;
            _sql_cmd.Parameters.Add("@pLevelID", SqlDbType.Int).Value = _dr["LEVEL_ID"];
            _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = _dr["PROVISION_AMOUNT"];
            _sql_cmd.Parameters.Add("@pTheoreticalAmount", SqlDbType.Money).Value = _dr["THEORETICAL_AMOUNT"];
            _sql_cmd.Parameters.Add("@pCurrentAmount", SqlDbType.Money).Value = Convert.ToDouble(_dr["LEVEL_AMOUNT"]) + Convert.ToDouble(_dr["PROVISION_AMOUNT"]);

            if (_sql_cmd.ExecuteNonQuery() != 1)
            {
              Log.Message("An error occurred inserting into PROGRESSIVES_PROVISIONS_LEVELS table: Rows affected <> 1.");

              return false;
            }
          }
        }

        foreach (DataRow _dr in ProgressiveProvision.Terminals.Rows)
        {
          // Provision terminals
          _sql_query = new StringBuilder();
          _sql_query.AppendLine("INSERT INTO   PROGRESSIVES_PROVISIONS_TERMINALS");
          _sql_query.AppendLine("            ( PPT_PROVISION_ID                 ");
          _sql_query.AppendLine("            , PPT_TERMINAL_ID                  ");
          _sql_query.AppendLine("            , PPT_PROGRESSIVE_ID               ");
          _sql_query.AppendLine("            , PPT_AMOUNT                       ");
          _sql_query.AppendLine("            , PPT_DIFF_AMOUNT                  ");
          _sql_query.AppendLine("            )                                  ");
          _sql_query.AppendLine("     VALUES ( @pProvisionID                    ");
          _sql_query.AppendLine("            , @pTerminalID                     ");
          _sql_query.AppendLine("            , @pProgressiveID                  ");
          _sql_query.AppendLine("            , @pProvisionAmount                ");
          _sql_query.AppendLine("            , @pDiffAmount                     ");
          _sql_query.AppendLine("            )                                  ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pProvisionID", SqlDbType.BigInt).Value = _provision_id;
            _sql_cmd.Parameters.Add("@pTerminalID", SqlDbType.Int).Value = _dr["TERMINAL_ID"];
            _sql_cmd.Parameters.Add("@pProgressiveID", SqlDbType.BigInt).Value = ProgressiveProvision.ProgressiveId;
            _sql_cmd.Parameters.Add("@pProvisionAmount", SqlDbType.Money).Value = _dr["PROVISION_AMOUNT"];
            _sql_cmd.Parameters.Add("@pDiffAmount", SqlDbType.Money).Value = _dr["DIFF_AMOUNT"];

            if (_sql_cmd.ExecuteNonQuery() != 1)
            {
              Log.Message("An error occurred inserting into PROGRESSIVES_PROVISIONS_TERMINALS table: Rows affected <> 1.");

              return false;
            }
          }
        }

        //Update Statistics
        if (!Statistics_InsertUpdateProvision(ProgressiveProvision, SqlTrx, out ErrorOnTerminals))
        {
          return false;
        }

        //Insert provision's play session
        if (!PlaySessions_InsertProvision(ProgressiveProvision, SqlTrx))
        {
          return false;
        }

        // Cage Provision
        if (ProvisionGoesToCage() == PROGRESSIVE_PROVISION_GOES_TO_CAGE.YES && Cage.IsCageEnabled())
        {
          if (!CageProgressivesProvisionIncrement(Cage.CageOperationType.ProgressiveProvision, _provision_id, 0, ProgressiveProvision.TotalCageProvision, _cage_session_id, SqlTrx))
          {
            return false;
          }

          // Everything went ok.  Update provision object
          ProgressiveProvision.CageSessionId = _cage_session_id;
        }

        if (!CheckProvision(_provision_id, SqlTrx))
        {
          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Updates and inserts statistics data from a progressive provision
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveProvision
    //            - SqlTrx
    //
    //      - OUTPUT :
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean Statistics_InsertUpdateProvision(ProgressiveProvision ProgressiveProvision, SqlTransaction SqlTrx, out List<String> ErrorOnTerminals)
    {
      return Statistics_InsertUpdateProvision(ProgressiveProvision, false, SqlTrx, out ErrorOnTerminals);
    } // Statistics_InsertUpdateProvision

    //------------------------------------------------------------------------------
    // PURPOSE : Updates and inserts statistics data from a progressive provision
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveProvision
    //            - SqlTrx
    //
    //      - OUTPUT :
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean Statistics_InsertUpdateProvision(ProgressiveProvision ProgressiveProvision, Boolean CancelProvision, SqlTransaction SqlTrx, out List<String> ErrorOnTerminals)
    {
      StringBuilder _sql_query;
      DataTable _dt_sales;
      DataTable _dt_aux_sales;
      DataTable _dt_machine_stats;
      DataRow _dr_aux_sales;
      Int32 _num_updated;
      Decimal _total_terminal_played;
      Decimal _total_terminal_provision;
      String _terminal_name;
      Int32 _game_id;
      String _game_name;

      ErrorOnTerminals = new List<string>();

      try
      {
        // Update SPH_PROGRESSIVE_PROVISION_AMOUNT
        foreach (DataRow _dr_terminals in ProgressiveProvision.Terminals.Rows)
        {
          _dt_sales = new DataTable();

          _total_terminal_provision = (Decimal)_dr_terminals["PROVISION_AMOUNT"];

          _sql_query = new StringBuilder();
          _sql_query.AppendLine("SELECT   SPH_TERMINAL_ID                                         ");
          _sql_query.AppendLine("       , SPH_PLAYED_AMOUNT                                       ");
          _sql_query.AppendLine("       , SPH_BASE_HOUR                                           ");
          _sql_query.AppendLine("       , SPH_GAME_ID                                             ");
          _sql_query.AppendLine("       , SPH_PROGRESSIVE_PROVISION_AMOUNT                        ");
          _sql_query.AppendLine("  FROM   SALES_PER_HOUR                                          ");
          _sql_query.AppendLine(" WHERE   SPH_TERMINAL_ID = @pTerminalId                          ");
          _sql_query.AppendLine("   AND   SPH_BASE_HOUR >= @pDateFrom AND SPH_BASE_HOUR < @pDateTo");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = _dr_terminals["TERMINAL_ID"];
            _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = ProgressiveProvision.HourFrom;
            _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = ProgressiveProvision.HourTo;

            using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
            {
              _da.Fill(_dt_sales);
            }
          }

          if (_dt_sales.Rows.Count == 0)
          {
            //No SALES_PER_HOUR record.  Insert a dummy one
            //  - Get last played game
            //  - Insert dummy record on SALES_PER_HOUR

            //  - Get last played game

            _dt_aux_sales = new DataTable();

            _sql_query = new StringBuilder();
            _sql_query.AppendLine("SELECT TOP (1)   SPH_TERMINAL_NAME   AS TERMINAL_NAME");
            _sql_query.AppendLine("               , SPH_GAME_ID         AS GAME_ID      ");
            _sql_query.AppendLine("               , SPH_GAME_NAME       AS GAME_NAME    ");
            _sql_query.AppendLine("          FROM   SALES_PER_HOUR                      ");
            _sql_query.AppendLine("         WHERE   SPH_TERMINAL_ID = @pTerminalId      ");
            _sql_query.AppendLine("      ORDER BY   SPH_BASE_HOUR DESC                  ");

            using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
            {
              _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = _dr_terminals["TERMINAL_ID"];

              using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
              {
                _da.Fill(_dt_aux_sales);

                if (_dt_aux_sales.Rows.Count == 0)
                {
                  _sql_query = new StringBuilder();
                  _sql_query.AppendLine("SELECT TOP (1)   TE_NAME               AS TERMINAL_NAME      ");
                  _sql_query.AppendLine("               , TGT_SOURCE_GAME_ID    AS GAME_ID            ");
                  _sql_query.AppendLine("               , GM_NAME               AS GAME_NAME          ");
                  _sql_query.AppendLine("          FROM   TERMINAL_GAME_TRANSLATION, GAMES, TERMINALS ");
                  _sql_query.AppendLine("         WHERE   TGT_TERMINAL_ID = @pTerminalId              ");
                  _sql_query.AppendLine("           AND   TGT_TERMINAL_ID = TE_TERMINAL_ID            ");
                  _sql_query.AppendLine("           AND   TGT_SOURCE_GAME_ID = GM_GAME_ID             ");

                  _da.SelectCommand.CommandText = _sql_query.ToString();

                  _da.Fill(_dt_aux_sales);
                }
              }

              if (_dt_aux_sales.Rows.Count == 0)
              {
                Log.Message(String.Format("WARNING: No game related to terminal {0}.  Provision can't be generated.", _dr_terminals["TERMINAL_ID"]));

                ErrorOnTerminals.Add((String)_dr_terminals["TERMINAL_NAME"]);

                continue;
              }

              _dr_aux_sales = _dt_aux_sales.Rows[0];

              _terminal_name = (String)_dr_aux_sales["TERMINAL_NAME"];
              _game_id = (Int32)_dr_aux_sales["GAME_ID"];
              _game_name = (String)_dr_aux_sales["GAME_NAME"];
            }

            //  - Insert dummy record on SALES_PER_HOUR
            _sql_query = new StringBuilder();
            _sql_query.AppendLine("INSERT INTO   SALES_PER_HOUR                   ");
            _sql_query.AppendLine("             ( SPH_BASE_HOUR                   ");
            _sql_query.AppendLine("             , SPH_TERMINAL_ID                 ");
            _sql_query.AppendLine("             , SPH_TERMINAL_NAME               ");
            _sql_query.AppendLine("             , SPH_GAME_ID                     ");
            _sql_query.AppendLine("             , SPH_GAME_NAME                   ");
            _sql_query.AppendLine("             , SPH_PLAYED_COUNT                ");
            _sql_query.AppendLine("             , SPH_PLAYED_AMOUNT               ");
            _sql_query.AppendLine("             , SPH_WON_COUNT                   ");
            _sql_query.AppendLine("             , SPH_WON_AMOUNT                  ");
            _sql_query.AppendLine("             , SPH_NUM_ACTIVE_TERMINALS        ");
            _sql_query.AppendLine("             , SPH_LAST_PLAY_ID                ");
            _sql_query.AppendLine("             , SPH_PROGRESSIVE_PROVISION_AMOUNT");
            _sql_query.AppendLine("             )                                 ");
            _sql_query.AppendLine("      VALUES ( @pBaseHour                      ");
            _sql_query.AppendLine("             , @pTerminalId                    ");
            _sql_query.AppendLine("             , @pTerminalName                  ");
            _sql_query.AppendLine("             , @pGameId                        ");
            _sql_query.AppendLine("             , @pGameName                      ");
            _sql_query.AppendLine("             , 0                               ");
            _sql_query.AppendLine("             , 0.0                             ");
            _sql_query.AppendLine("             , 0                               ");
            _sql_query.AppendLine("             , 0.0                             ");
            _sql_query.AppendLine("             , 0                               ");
            _sql_query.AppendLine("             , 0                               ");
            _sql_query.AppendLine("             , @pProvisionAmount               ");
            _sql_query.AppendLine("             )                                 ");

            using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
            {
              _sql_cmd.Parameters.Add("@pBaseHour", SqlDbType.DateTime).Value = ProgressiveProvision.HourFrom;
              _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = _dr_terminals["TERMINAL_ID"];
              _sql_cmd.Parameters.Add("@pTerminalName", SqlDbType.VarChar, 50).Value = _terminal_name;
              _sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int).Value = _game_id;
              _sql_cmd.Parameters.Add("@pGameName", SqlDbType.VarChar, 50).Value = _game_name;
              _sql_cmd.Parameters.Add("@pProvisionAmount", SqlDbType.Money).Value = CancelProvision ? ((Decimal)_dr_terminals["PROVISION_AMOUNT"] * -1) : _dr_terminals["PROVISION_AMOUNT"];

              _num_updated = _sql_cmd.ExecuteNonQuery();

              if (_num_updated != 1)
              {
                Log.Message("An error occurred inserting into SALES_PER_HOUR: Rows affected <> 1.");

                return false;
              }
            }
          }
          else
          {
            // Update SPH_PROGRESSIVE_PROVISION_AMOUNT            
            _total_terminal_played = (Decimal)_dt_sales.Compute("SUM(SPH_PLAYED_AMOUNT)", "");

            foreach (DataRow _dr_sales in _dt_sales.Rows)
            {
              if (_total_terminal_played != 0)
              {
                _dr_sales["SPH_PROGRESSIVE_PROVISION_AMOUNT"] = (Decimal)_dr_sales["SPH_PLAYED_AMOUNT"] * _total_terminal_provision / _total_terminal_played;
              }
              else
              {
                _dr_sales["SPH_PROGRESSIVE_PROVISION_AMOUNT"] = _total_terminal_provision / _dt_sales.Rows.Count;
              }

              if (CancelProvision)
              {
                _dr_sales["SPH_PROGRESSIVE_PROVISION_AMOUNT"] = ((Decimal)_dr_sales["SPH_PROGRESSIVE_PROVISION_AMOUNT"] * -1);
              }

            } // foreach

            _sql_query = new StringBuilder();
            _sql_query.AppendLine("UPDATE   SALES_PER_HOUR                                                                                      ");
            _sql_query.AppendLine("   SET   SPH_PROGRESSIVE_PROVISION_AMOUNT = isnull (SPH_PROGRESSIVE_PROVISION_AMOUNT, 0) + @pProvisionAmount ");
            _sql_query.AppendLine(" WHERE   SPH_TERMINAL_ID = @pTerminalId                                                                      ");
            _sql_query.AppendLine("   AND   SPH_BASE_HOUR = @pBaseHour                                                                          ");
            _sql_query.AppendLine("   AND   SPH_GAME_ID = @pGameId                                                                              ");

            using (SqlDataAdapter _da = new SqlDataAdapter())
            {
              using (SqlCommand _cmd_update = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
              {
                _cmd_update.Parameters.Add("@pProvisionAmount", SqlDbType.Money).SourceColumn = "SPH_PROGRESSIVE_PROVISION_AMOUNT";
                _cmd_update.Parameters.Add("@pTerminalId", SqlDbType.BigInt).SourceColumn = "SPH_TERMINAL_ID";
                _cmd_update.Parameters.Add("@pBaseHour", SqlDbType.DateTime).SourceColumn = "SPH_BASE_HOUR";
                _cmd_update.Parameters.Add("@pGameId", SqlDbType.Int).SourceColumn = "SPH_GAME_ID";

                _da.UpdateCommand = _cmd_update;

                _num_updated = _da.Update(_dt_sales);

                if (_num_updated != _dt_sales.Rows.Count)
                {
                  Log.Message("An error occurred updating SALES_PER_HOUR: Rows affected <> _dt_sales.Rows.Count.");

                  return false;
                }
              }
            }
          }

          // Update MSH_PROGRESSIVE_PROVISION_AMOUNT on MACHINE_STATS_PER_HOUR 
          _dt_machine_stats = new DataTable();

          _sql_query = new StringBuilder();
          _sql_query.AppendLine("    SELECT   MSH_BASE_HOUR                                           ");
          _sql_query.AppendLine("           , MSH_TERMINAL_ID                                         ");
          _sql_query.AppendLine("           , MSH_PLAYED_AMOUNT                                       ");
          _sql_query.AppendLine("           , MSH_PROGRESSIVE_PROVISION_AMOUNT                        ");
          _sql_query.AppendLine("      FROM   MACHINE_STATS_PER_HOUR                                  ");
          _sql_query.AppendLine("     WHERE   MSH_TERMINAL_ID = @pTerminalId                          ");
          _sql_query.AppendLine("       AND   MSH_BASE_HOUR >= @pDateFrom AND MSH_BASE_HOUR < @pDateTo");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = _dr_terminals["TERMINAL_ID"];
            _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = ProgressiveProvision.HourFrom;
            _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = ProgressiveProvision.HourTo;

            using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
            {
              _da.Fill(_dt_machine_stats);
            }
          }

          if (_dt_machine_stats.Rows.Count == 0)
          {
            // Insert dummy record on MACHINE_STATS_PER_HOUR
            _sql_query = new StringBuilder();
            _sql_query.AppendLine("INSERT INTO   MACHINE_STATS_PER_HOUR           ");
            _sql_query.AppendLine("             ( MSH_BASE_HOUR                   ");
            _sql_query.AppendLine("             , MSH_TERMINAL_ID                 ");
            _sql_query.AppendLine("             , MSH_PLAYED_COUNT                ");
            _sql_query.AppendLine("             , MSH_PLAYED_AMOUNT               ");
            _sql_query.AppendLine("             , MSH_WON_COUNT                   ");
            _sql_query.AppendLine("             , MSH_WON_AMOUNT                  ");
            _sql_query.AppendLine("             , MSH_TO_GM_COUNT                 ");
            _sql_query.AppendLine("             , MSH_TO_GM_AMOUNT                ");
            _sql_query.AppendLine("             , MSH_FROM_GM_COUNT               ");
            _sql_query.AppendLine("             , MSH_FROM_GM_AMOUNT              ");
            _sql_query.AppendLine("             , MSH_PROGRESSIVE_PROVISION_AMOUNT");
            _sql_query.AppendLine("             )                                 ");
            _sql_query.AppendLine("      VALUES ( @pBaseHour                      ");
            _sql_query.AppendLine("             , @pTerminalId                    ");
            _sql_query.AppendLine("             , 0                               ");
            _sql_query.AppendLine("             , 0.0                             ");
            _sql_query.AppendLine("             , 0                               ");
            _sql_query.AppendLine("             , 0.0                             ");
            _sql_query.AppendLine("             , 0                               ");
            _sql_query.AppendLine("             , 0.0                             ");
            _sql_query.AppendLine("             , 0                               ");
            _sql_query.AppendLine("             , 0.0                             ");
            _sql_query.AppendLine("             , @pProvisionAmount               ");
            _sql_query.AppendLine("             )                                 ");

            using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
            {
              _sql_cmd.Parameters.Add("@pBaseHour", SqlDbType.DateTime).Value = ProgressiveProvision.HourFrom;
              _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = _dr_terminals["TERMINAL_ID"];
              _sql_cmd.Parameters.Add("@pProvisionAmount", SqlDbType.Money).Value = CancelProvision ? ((Decimal)_dr_terminals["PROVISION_AMOUNT"] * -1) : _dr_terminals["PROVISION_AMOUNT"];

              _num_updated = _sql_cmd.ExecuteNonQuery();

              if (_num_updated != 1)
              {
                Log.Message("An error occurred inserting into MACHINE_STATS_PER_HOUR: Rows affected <> -1.");

                return false;
              }
            }
          }
          else
          {
            _total_terminal_played = (Decimal)_dt_machine_stats.Compute("SUM(MSH_PLAYED_AMOUNT)", "");

            foreach (DataRow _dr_machine_stats in _dt_machine_stats.Rows)
            {
              if (_total_terminal_played != 0)
              {
                _dr_machine_stats["MSH_PROGRESSIVE_PROVISION_AMOUNT"] = (Decimal)_dr_machine_stats["MSH_PLAYED_AMOUNT"] * _total_terminal_provision / _total_terminal_played;
              }
              else
              {
                _dr_machine_stats["MSH_PROGRESSIVE_PROVISION_AMOUNT"] = _total_terminal_provision / _dt_machine_stats.Rows.Count;
              }

              if (CancelProvision)
              {
                _dr_machine_stats["MSH_PROGRESSIVE_PROVISION_AMOUNT"] = ((Decimal)_dr_machine_stats["MSH_PROGRESSIVE_PROVISION_AMOUNT"] * -1);
              }
            } // foreach

            _sql_query = new StringBuilder();
            _sql_query.AppendLine("UPDATE   MACHINE_STATS_PER_HOUR                                                                              ");
            _sql_query.AppendLine("   SET   MSH_PROGRESSIVE_PROVISION_AMOUNT = isnull (MSH_PROGRESSIVE_PROVISION_AMOUNT, 0) + @pProvisionAmount ");
            _sql_query.AppendLine(" WHERE   MSH_TERMINAL_ID = @pTerminalId                                                                      ");
            _sql_query.AppendLine("   AND   MSH_BASE_HOUR = @pBaseHour                                                                          ");

            using (SqlDataAdapter _da = new SqlDataAdapter())
            {
              using (SqlCommand _cmd_update = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
              {
                _cmd_update.Parameters.Add("@pProvisionAmount", SqlDbType.Money).SourceColumn = "MSH_PROGRESSIVE_PROVISION_AMOUNT";
                _cmd_update.Parameters.Add("@pTerminalId", SqlDbType.BigInt).SourceColumn = "MSH_TERMINAL_ID";
                _cmd_update.Parameters.Add("@pBaseHour", SqlDbType.DateTime).SourceColumn = "MSH_BASE_HOUR";

                _da.UpdateCommand = _cmd_update;

                _num_updated = _da.Update(_dt_machine_stats);

                if (_num_updated != _dt_machine_stats.Rows.Count)
                {
                  Log.Message("An error occurred updating MACHINE_STATS_PER_HOUR: Rows affected <> _dt_machine_stats.Rows.Count.");

                  return false;
                }
              }
            }
          }
        } // foreach        

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Updates and inserts statistics data from a progressive provision
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveProvision
    //            - SqlTrx
    //
    //      - OUTPUT :
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean Statistics_InsertUpdateJackpotAmount(Int64 TerminalId, Decimal JackpotAmount, SqlTransaction SqlTrx)
    {
      StringBuilder _sql_query;
      DataTable _dt_sales;
      DataTable _dt_aux_sales;
      DataTable _dt_machine_stats;
      DataRow _dr_aux_sales;
      Int32 _num_updated;
      String _terminal_name;
      Int32 _game_id;
      String _game_name;
      DateTime _base_hour;

      try
      {
        _base_hour = WGDB.Now;

        //Dismiss minutes, seconds and milliseconds
        _base_hour = _base_hour.AddMinutes(_base_hour.Minute * -1);
        _base_hour = _base_hour.AddSeconds(_base_hour.Second * -1);
        _base_hour = _base_hour.AddMilliseconds(_base_hour.Millisecond * -1);

        // Update SPH_PROGRESSIVE_JACKPOT_AMOUNT & SPH_PROGRESSIVE_JACKPOT_AMOUNT_0
        _dt_sales = new DataTable();

        _sql_query = new StringBuilder();
        _sql_query.AppendLine("SELECT   SPH_TERMINAL_ID                 ");
        _sql_query.AppendLine("       , SPH_PLAYED_AMOUNT               ");
        _sql_query.AppendLine("       , SPH_BASE_HOUR                   ");
        _sql_query.AppendLine("       , SPH_GAME_ID                     ");
        _sql_query.AppendLine("       , SPH_PROGRESSIVE_JACKPOT_AMOUNT  ");
        _sql_query.AppendLine("       , SPH_PROGRESSIVE_JACKPOT_AMOUNT_0");
        _sql_query.AppendLine("  FROM   SALES_PER_HOUR                  ");
        _sql_query.AppendLine(" WHERE   SPH_TERMINAL_ID = @pTerminalId  ");
        _sql_query.AppendLine("   AND   SPH_BASE_HOUR = @pBaseHour      ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pBaseHour", SqlDbType.DateTime).Value = _base_hour;

          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(_dt_sales);
          }
        }

        if (_dt_sales.Rows.Count == 0)
        {
          //No SALES_PER_HOUR record.  Insert a dummy one
          //  - Get last played game
          //  - Insert dummy record on SALES_PER_HOUR

          //  - Get last played game

          _dt_aux_sales = new DataTable();

          _sql_query = new StringBuilder();
          _sql_query.AppendLine("SELECT TOP (1)   SPH_TERMINAL_NAME   AS TERMINAL_NAME");
          _sql_query.AppendLine("               , SPH_GAME_ID         AS GAME_ID      ");
          _sql_query.AppendLine("               , SPH_GAME_NAME       AS GAME_NAME    ");
          _sql_query.AppendLine("          FROM   SALES_PER_HOUR                      ");
          _sql_query.AppendLine("         WHERE   SPH_TERMINAL_ID = @pTerminalId      ");
          _sql_query.AppendLine("      ORDER BY   SPH_BASE_HOUR DESC                  ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;

            using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
            {
              _da.Fill(_dt_aux_sales);

              if (_dt_aux_sales.Rows.Count == 0)
              {
                _sql_query = new StringBuilder();
                _sql_query.AppendLine("SELECT TOP (1)   TE_NAME               AS TERMINAL_NAME      ");
                _sql_query.AppendLine("               , TGT_SOURCE_GAME_ID    AS GAME_ID            ");
                _sql_query.AppendLine("               , GM_NAME               AS GAME_NAME          ");
                _sql_query.AppendLine("          FROM   TERMINAL_GAME_TRANSLATION, GAMES, TERMINALS ");
                _sql_query.AppendLine("         WHERE   TGT_TERMINAL_ID = @pTerminalId              ");
                _sql_query.AppendLine("           AND   TGT_TERMINAL_ID = TE_TERMINAL_ID            ");
                _sql_query.AppendLine("           AND   TGT_SOURCE_GAME_ID = GM_GAME_ID             ");

                _da.SelectCommand.CommandText = _sql_query.ToString();

                _da.Fill(_dt_aux_sales);
              }
            }

            if (_dt_aux_sales.Rows.Count == 0)
            {
              Log.Message(String.Format("WARNING: No game related to terminal {0}.  Progressive Jackpot amount can't be updated.", TerminalId));

              return true;
            }

            _dr_aux_sales = _dt_aux_sales.Rows[0];

            _terminal_name = (String)_dr_aux_sales["TERMINAL_NAME"];
            _game_id = (Int32)_dr_aux_sales["GAME_ID"];
            _game_name = (String)_dr_aux_sales["GAME_NAME"];
          }

          //  - Insert dummy record on SALES_PER_HOUR
          _sql_query = new StringBuilder();
          _sql_query.AppendLine("INSERT INTO   SALES_PER_HOUR                    ");
          _sql_query.AppendLine("             ( SPH_BASE_HOUR                    ");
          _sql_query.AppendLine("             , SPH_TERMINAL_ID                  ");
          _sql_query.AppendLine("             , SPH_TERMINAL_NAME                ");
          _sql_query.AppendLine("             , SPH_GAME_ID                      ");
          _sql_query.AppendLine("             , SPH_GAME_NAME                    ");
          _sql_query.AppendLine("             , SPH_PLAYED_COUNT                 ");
          _sql_query.AppendLine("             , SPH_PLAYED_AMOUNT                ");
          _sql_query.AppendLine("             , SPH_WON_COUNT                    ");
          _sql_query.AppendLine("             , SPH_WON_AMOUNT                   ");
          _sql_query.AppendLine("             , SPH_NUM_ACTIVE_TERMINALS         ");
          _sql_query.AppendLine("             , SPH_LAST_PLAY_ID                 ");
          _sql_query.AppendLine("             , SPH_PROGRESSIVE_JACKPOT_AMOUNT   ");
          _sql_query.AppendLine("             , SPH_PROGRESSIVE_JACKPOT_AMOUNT_0 ");
          _sql_query.AppendLine("             , SPH_JACKPOT_AMOUNT               ");
          _sql_query.AppendLine("             )                                  ");
          _sql_query.AppendLine("      VALUES ( @pBaseHour                       ");
          _sql_query.AppendLine("             , @pTerminalId                     ");
          _sql_query.AppendLine("             , @pTerminalName                   ");
          _sql_query.AppendLine("             , @pGameId                         ");
          _sql_query.AppendLine("             , @pGameName                       ");
          _sql_query.AppendLine("             , 0                                ");
          _sql_query.AppendLine("             , 0.0                              ");
          _sql_query.AppendLine("             , 0                                ");
          _sql_query.AppendLine("             , 0.0                              ");
          _sql_query.AppendLine("             , 0                                ");
          _sql_query.AppendLine("             , 0                                ");
          _sql_query.AppendLine("             , @pProgJackpotAmount              ");
          _sql_query.AppendLine("             , @pProgJackpotAmount              ");
          _sql_query.AppendLine("             , @pProgJackpotAmount              ");
          _sql_query.AppendLine("             )                                  ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pBaseHour", SqlDbType.DateTime).Value = _base_hour;
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;
            _sql_cmd.Parameters.Add("@pTerminalName", SqlDbType.VarChar, 50).Value = _terminal_name;
            _sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int).Value = _game_id;
            _sql_cmd.Parameters.Add("@pGameName", SqlDbType.VarChar, 50).Value = _game_name;
            _sql_cmd.Parameters.Add("@pProgJackpotAmount", SqlDbType.Money).Value = JackpotAmount;

            _num_updated = _sql_cmd.ExecuteNonQuery();

            if (_num_updated != 1)
            {
              Log.Message("An error occurred inserting into SALES_PER_HOUR: Rows affected <> 1.");

              return false;
            }
          }
        }
        else
        {
          _game_id = (Int32)_dt_sales.Rows[0]["SPH_GAME_ID"];

          // Update SPH_PROGRESSIVE_JACKPOT_AMOUNT & SPH_PROGRESSIVE_JACKPOT_AMOUNT_0
          _sql_query = new StringBuilder();
          _sql_query.AppendLine("UPDATE   SALES_PER_HOUR                                                                                    ");
          _sql_query.AppendLine("   SET   SPH_PROGRESSIVE_JACKPOT_AMOUNT = isnull (SPH_PROGRESSIVE_JACKPOT_AMOUNT, 0) + @pJackpotAmount     ");
          _sql_query.AppendLine("       , SPH_PROGRESSIVE_JACKPOT_AMOUNT_0 = isnull (SPH_PROGRESSIVE_JACKPOT_AMOUNT_0, 0) + @pJackpotAmount ");
          _sql_query.AppendLine("       , SPH_JACKPOT_AMOUNT = isnull (SPH_JACKPOT_AMOUNT, 0) + @pJackpotAmount                             ");
          _sql_query.AppendLine(" WHERE   SPH_TERMINAL_ID = @pTerminalId                                                                    ");
          _sql_query.AppendLine("   AND   SPH_BASE_HOUR = @pBaseHour                                                                        ");
          _sql_query.AppendLine("   AND   SPH_GAME_ID = @pGameId                                                                            ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pJackpotAmount", SqlDbType.Money).Value = JackpotAmount;
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;
            _sql_cmd.Parameters.Add("@pBaseHour", SqlDbType.DateTime).Value = _base_hour;
            _sql_cmd.Parameters.Add("@pGameId", SqlDbType.Int).Value = _game_id;

            _num_updated = _sql_cmd.ExecuteNonQuery();

            if (_num_updated != 1)
            {
              Log.Message("An error occurred updating SALES_PER_HOUR: Rows affected <> _dt_sales.Rows.Count.");

              return false;
            }
          }
        }

        // Update MSH_PROGRESSIVE_JACKPOT_AMOUNT & MSH_PROGRESSIVE_JACKPOT_AMOUNT_0 on MACHINE_STATS_PER_HOUR 
        _dt_machine_stats = new DataTable();

        _sql_query = new StringBuilder();
        _sql_query.AppendLine("    SELECT   MSH_BASE_HOUR                   ");
        _sql_query.AppendLine("           , MSH_TERMINAL_ID                 ");
        _sql_query.AppendLine("           , MSH_PLAYED_AMOUNT               ");
        _sql_query.AppendLine("           , MSH_PROGRESSIVE_JACKPOT_AMOUNT  ");
        _sql_query.AppendLine("           , MSH_PROGRESSIVE_JACKPOT_AMOUNT_0");
        _sql_query.AppendLine("      FROM   MACHINE_STATS_PER_HOUR          ");
        _sql_query.AppendLine("     WHERE   MSH_TERMINAL_ID = @pTerminalId  ");
        _sql_query.AppendLine("       AND   MSH_BASE_HOUR = @pBaseHour      ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pBaseHour", SqlDbType.DateTime).Value = _base_hour;

          using (SqlDataAdapter _da = new SqlDataAdapter(_sql_cmd))
          {
            _da.Fill(_dt_machine_stats);
          }
        }

        if (_dt_machine_stats.Rows.Count == 0)
        {
          // Insert dummy record on MACHINE_STATS_PER_HOUR
          _sql_query = new StringBuilder();
          _sql_query.AppendLine("INSERT INTO   MACHINE_STATS_PER_HOUR           ");
          _sql_query.AppendLine("             ( MSH_BASE_HOUR                   ");
          _sql_query.AppendLine("             , MSH_TERMINAL_ID                 ");
          _sql_query.AppendLine("             , MSH_PLAYED_COUNT                ");
          _sql_query.AppendLine("             , MSH_PLAYED_AMOUNT               ");
          _sql_query.AppendLine("             , MSH_WON_COUNT                   ");
          _sql_query.AppendLine("             , MSH_WON_AMOUNT                  ");
          _sql_query.AppendLine("             , MSH_TO_GM_COUNT                 ");
          _sql_query.AppendLine("             , MSH_TO_GM_AMOUNT                ");
          _sql_query.AppendLine("             , MSH_FROM_GM_COUNT               ");
          _sql_query.AppendLine("             , MSH_FROM_GM_AMOUNT              ");
          _sql_query.AppendLine("             , MSH_PROGRESSIVE_JACKPOT_AMOUNT  ");
          _sql_query.AppendLine("             , MSH_PROGRESSIVE_JACKPOT_AMOUNT_0");
          _sql_query.AppendLine("             )                                 ");
          _sql_query.AppendLine("      VALUES ( @pBaseHour                      ");
          _sql_query.AppendLine("             , @pTerminalId                    ");
          _sql_query.AppendLine("             , 0                               ");
          _sql_query.AppendLine("             , 0.0                             ");
          _sql_query.AppendLine("             , 0                               ");
          _sql_query.AppendLine("             , 0.0                             ");
          _sql_query.AppendLine("             , 0                               ");
          _sql_query.AppendLine("             , 0.0                             ");
          _sql_query.AppendLine("             , 0                               ");
          _sql_query.AppendLine("             , 0.0                             ");
          _sql_query.AppendLine("             , @pJackpotAmount                 ");
          _sql_query.AppendLine("             , @pJackpotAmount                 ");
          _sql_query.AppendLine("             )                                 ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pBaseHour", SqlDbType.DateTime).Value = _base_hour;
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;
            _sql_cmd.Parameters.Add("@pJackpotAmount", SqlDbType.Money).Value = JackpotAmount;

            _num_updated = _sql_cmd.ExecuteNonQuery();

            if (_num_updated != 1)
            {
              Log.Message("An error occurred inserting into MACHINE_STATS_PER_HOUR: Rows affected <> -1.");

              return false;
            }
          }
        }
        else
        {
          _sql_query = new StringBuilder();
          _sql_query.AppendLine("UPDATE   MACHINE_STATS_PER_HOUR                                                                            ");
          _sql_query.AppendLine("   SET   MSH_PROGRESSIVE_JACKPOT_AMOUNT = isnull (MSH_PROGRESSIVE_JACKPOT_AMOUNT, 0) + @pJackpotAmount     ");
          _sql_query.AppendLine("       , MSH_PROGRESSIVE_JACKPOT_AMOUNT_0 = isnull (MSH_PROGRESSIVE_JACKPOT_AMOUNT_0, 0) + @pJackpotAmount ");
          _sql_query.AppendLine(" WHERE   MSH_TERMINAL_ID = @pTerminalId                                                                    ");
          _sql_query.AppendLine("   AND   MSH_BASE_HOUR = @pBaseHour                                                                        ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pJackpotAmount", SqlDbType.Money).Value = JackpotAmount;
            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;
            _sql_cmd.Parameters.Add("@pBaseHour", SqlDbType.DateTime).Value = _base_hour;

            _num_updated = _sql_cmd.ExecuteNonQuery();

            if (_num_updated != _dt_machine_stats.Rows.Count)
            {
              Log.Message("An error occurred updating MACHINE_STATS_PER_HOUR: Rows affected <> _dt_machine_stats.Rows.Count.");

              return false;
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Statistics_InsertUpdateJackpotAmount

    //------------------------------------------------------------------------------
    // PURPOSE : Inserts the play sessions related to the progressive provision.
    //
    //  PARAMS :
    //      - INPUT:
    //            - HourFrom
    //            - HourTo
    //            - DtTerminalList
    //            - SqlTrx
    //
    //      - OUTPUT :
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean PlaySessions_InsertProvision(ProgressiveProvision ProgressiveProvision, SqlTransaction SqlTrx)
    {
      return PlaySessions_InsertProvision(ProgressiveProvision, false, SqlTrx);
    } // PlaySessions_InsertProvision

    //------------------------------------------------------------------------------
    // PURPOSE : Inserts the play sessions related to the progressive provision.
    //
    //  PARAMS :
    //      - INPUT:
    //            - HourFrom
    //            - HourTo
    //            - DtTerminalList
    //            - SqlTrx
    //
    //      - OUTPUT :
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean PlaySessions_InsertProvision(ProgressiveProvision ProgressiveProvision, Boolean CancelProvision, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      SqlParameter _param_play_session;
      Int64 _accound_id;
      Int32 _terminal_id;
      Int64 _play_session_id;

      try
      {
        foreach (DataRow _dr_terminals in ProgressiveProvision.Terminals.Rows)
        {
          _play_session_id = -1;

          _terminal_id = (Int32)_dr_terminals["TERMINAL_ID"];
          _accound_id = Accounts.GetOrCreateVirtualAccount(_terminal_id, AccountType.ACCOUNT_VIRTUAL_TERMINAL, SqlTrx);

          if (_accound_id == 0)
          {
            Log.Message("An error occurred calling Accounts.GetOrCreateVirtualAccount:  Returned AccountId = 0.");

            return false;
          }
          _sb = new StringBuilder();
          _sb.AppendLine("INSERT INTO PLAY_SESSIONS ( PS_ACCOUNT_ID ");
          _sb.AppendLine("                          , PS_TERMINAL_ID ");
          _sb.AppendLine("                          , PS_TYPE ");
          _sb.AppendLine("                          , PS_INITIAL_BALANCE ");
          _sb.AppendLine("                          , PS_FINAL_BALANCE ");
          _sb.AppendLine("                          , PS_PLAYED_COUNT ");
          _sb.AppendLine("                          , PS_PLAYED_AMOUNT ");
          _sb.AppendLine("                          , PS_WON_COUNT ");
          _sb.AppendLine("                          , PS_WON_AMOUNT ");
          _sb.AppendLine("                          , PS_CASH_IN ");
          _sb.AppendLine("                          , PS_CASH_OUT ");
          _sb.AppendLine("                          , PS_STATUS ");
          _sb.AppendLine("                          , PS_STARTED ");
          _sb.AppendLine("                          , PS_FINISHED ");
          _sb.AppendLine("                          , PS_STAND_ALONE ");
          _sb.AppendLine("                          , PS_PROMO ");
          _sb.AppendLine("                          ) ");
          _sb.AppendLine("                   VALUES ( @pAccountId ");      // PS_ACCOUNT_ID
          _sb.AppendLine("                          , @pTerminalId ");     // PS_TERMINAL_ID
          _sb.AppendLine("                          , @pType ");           // PS_TYPE
          _sb.AppendLine("                          , @pInitialBalance "); // PS_INITIAL_BALANCE
          _sb.AppendLine("                          , @pFinalBalance ");   // PS_FINAL_BALANCE
          _sb.AppendLine("                          , 0 ");                // PS_PLAYED_COUNT
          _sb.AppendLine("                          , 0 ");                // PS_PLAYED_AMOUNT
          _sb.AppendLine("                          , 0 ");                // PS_WON_COUNT
          _sb.AppendLine("                          , 0 ");                // PS_WON_AMOUNT
          _sb.AppendLine("                          , 0 ");                // PS_CASH_IN
          _sb.AppendLine("                          , 0 ");                // PS_CASH_OUT
          _sb.AppendLine("                          , @pStatus ");         // PS_STATUS
          _sb.AppendLine("                          , @pStarted ");        // PS_STARTED
          _sb.AppendLine("                          , @pFinished ");       // PS_FINISHED
          _sb.AppendLine("                          , 1 ");                // PS_STAND_ALONE
          _sb.AppendLine("                          , 0 ");                // PS_PROMO
          _sb.AppendLine("                          ) ");
          _sb.AppendLine("                           SET @pPlaySessionId = SCOPE_IDENTITY() ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = _accound_id;
            _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = _terminal_id;
            _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = PlaySessionType.PROGRESSIVE_PROVISION;
            _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = PlaySessionStatus.ProgressiveProvision;

            if (CancelProvision)
            {
              _cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).Value = (Decimal)_dr_terminals["PROVISION_AMOUNT"];
              _cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).Value = 0;
            }
            else
            {
              _cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).Value = 0;
              _cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).Value = (Decimal)_dr_terminals["PROVISION_AMOUNT"];
            }

            _cmd.Parameters.Add("@pStarted", SqlDbType.DateTime).Value = ProgressiveProvision.HourFrom;
            _cmd.Parameters.Add("@pFinished", SqlDbType.DateTime).Value = ProgressiveProvision.HourFrom;

            _param_play_session = _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt);
            _param_play_session.Direction = ParameterDirection.Output;

            if (_cmd.ExecuteNonQuery() != 1)
            {
              Log.Message("An error occurred inserting into PLAY_SESSIONS: Rows affected <> -1.");

              return false;
            }

            _play_session_id = (Int64)_param_play_session.Value;

          }

          MultiPromos.AccountBalance _progressive_amount;
          _progressive_amount = new MultiPromos.AccountBalance((Decimal)_dr_terminals["PROVISION_AMOUNT"], 0, 0);

          if (!MultiPromos.Trx_PlaySessionSetMeters(_play_session_id,
                                                    CancelProvision ? _progressive_amount : MultiPromos.AccountBalance.Zero,
                                                    MultiPromos.AccountBalance.Zero,
                                                    MultiPromos.AccountBalance.Zero,
                                                    CancelProvision ? MultiPromos.AccountBalance.Zero : _progressive_amount,
                                                    0, 
                                                    SqlTrx))
          {
            Log.Message("An error occurred calling MultiPromos.Trx_PlaySessionSetMeters.");

            return false;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;

    } // PlaySessions_InsertProvision

    //------------------------------------------------------------------------------
    // PURPOSE : Insert Provision Cage Movement
    //
    //  PARAMS :
    //      - INPUT :
    //                - CageSessionId
    //                - Type
    //                - SourceTargetId
    //                - Trx
    //      - OUTPUT :
    //                - MovementId
    //
    // RETURNS :
    //            - True: If cage movement has been created correctly
    //            - False: Otherwise
    //
    public static Boolean InsertProvisionCageMovement(Int64 CageSessionId,
                                                      Cage.CageOperationType Type,
                                                      Int64 RelatedId,
                                                      Int64 TerminalId,
                                                      out Int64 MovementId,
                                                      SqlTransaction Trx)
    {
      StringBuilder _sb_movement;
      SqlParameter _param;

      MovementId = 0;

      try
      {
        _sb_movement = new StringBuilder();
        _sb_movement.AppendLine(" INSERT INTO   CAGE_MOVEMENTS                ");
        _sb_movement.AppendLine("              ( CGM_CAGE_SESSION_ID          ");
        _sb_movement.AppendLine("             ,  CGM_TYPE                     "); // before: CGM_OPERATION_ID
        _sb_movement.AppendLine("             ,  CGM_STATUS                   ");
        _sb_movement.AppendLine("             ,  CGM_TERMINAL_CASHIER_ID      ");
        _sb_movement.AppendLine("             ,  CGM_USER_CASHIER_ID          ");
        _sb_movement.AppendLine("             ,  CGM_USER_CAGE_ID             ");
        _sb_movement.AppendLine("             ,  CGM_CASHIER_SESSION_ID       ");
        _sb_movement.AppendLine("             ,  CGM_MOVEMENT_DATETIME        ");
        _sb_movement.AppendLine("             ,  CGM_RELATED_ID               ");
        _sb_movement.AppendLine("             )                               ");
        _sb_movement.AppendLine("                VALUES (                     ");
        _sb_movement.AppendLine("                @pCageSessionId              ");
        _sb_movement.AppendLine("             ,  @pType                       ");
        _sb_movement.AppendLine("             ,  @pStatus                     ");
        _sb_movement.AppendLine("             ,  @pTerminalCashier            ");
        _sb_movement.AppendLine("             ,  @pUserCashier                ");
        _sb_movement.AppendLine("             ,  @pUserCageId                 ");
        _sb_movement.AppendLine("             ,  @pCashierSessionId           ");
        _sb_movement.AppendLine("             ,  GETDATE()                    ");
        _sb_movement.AppendLine("             ,  @pRelatedId                  ");
        _sb_movement.AppendLine("             )                               ");

        _sb_movement.AppendLine("SET @pMovementId = SCOPE_IDENTITY()");

        using (SqlCommand _cmd = new SqlCommand(_sb_movement.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pCageSessionId", SqlDbType.BigInt).Value = CageSessionId;
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = (Int32)Type;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = (Int32)Cage.CageStatus.OK;
          if (TerminalId == 0)
          {
            _cmd.Parameters.Add("@pTerminalCashier", SqlDbType.BigInt).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pTerminalCashier", SqlDbType.BigInt).Value = TerminalId;
          }

          _cmd.Parameters.Add("@pUserCashier", SqlDbType.BigInt).Value = DBNull.Value;
          _cmd.Parameters.Add("@pUserCageId", SqlDbType.BigInt).Value = DBNull.Value;
          _cmd.Parameters.Add("@pCashierSessionId", SqlDbType.BigInt).Value = DBNull.Value;

          if (RelatedId == 0)
          {
            _cmd.Parameters.Add("@pRelatedId", SqlDbType.BigInt).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pRelatedId", SqlDbType.BigInt).Value = RelatedId;
          }


          _param = _cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt);
          _param.Direction = ParameterDirection.Output;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            MovementId = (Int64)_param.Value;

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // InsertProvisionCageMovement

    //------------------------------------------------------------------------------
    // PURPOSE : Provision Cage Increment
    //
    //  PARAMS :
    //      - INPUT :
    //                - CageOperationType
    //                - SourceId
    //                - TerminalId
    //                - Amount
    //                - Trx
    //      - OUTPUT :
    //
    // RETURNS :
    //            - True: If cage increment has been done correctly
    //            - False: Otherwise
    //
    public static Boolean CageProgressivesProvisionIncrement(Cage.CageOperationType CageOperationType,
                                                             Int64 SourceId,     // ProvisionId or HandpayId
                                                             Int64 TerminalId,
                                                             Decimal Amount,
                                                             Int64 CageSessionId,
                                                             SqlTransaction Trx)
    {
      Int64 _movement_id;
      Decimal _value_in;
      Decimal _value_out;
      String _iso_code;
      Decimal _result_meter;
      Decimal _result_session_meter;

      _iso_code = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");

      _value_in = 0;
      _value_out = 0;

      switch (CageOperationType)
      {
        case Cage.CageOperationType.ProgressiveProvision:
        case Cage.CageOperationType.ProgressiveCancelAwarded:
          _value_in = Amount;
          break;

        case Cage.CageOperationType.ProgressiveAwarded:
        case Cage.CageOperationType.ProgressiveCancelProvision:
          _value_out = Amount;
          break;

        default:
          return false;
        //break;
      }

      //
      // Call Cage Meter Increment: Cage.UpdateCageMeters   
      //
      if (!CageMeters.UpdateCageMeters(_value_in
                                    , _value_out
                                    , CageSessionId
                                    , (Int64)CageMeters.CageSystemSourceTarget.System
                                    , (Int64)CageMeters.CageConceptId.ProgresiveProvision
                                    , _iso_code
                                    , 0 
                                    , CageMeters.UpdateCageMetersOperationType.Increment
                                    , CageMeters.UpdateCageMetersgetGetSessionMode.FromParameter
                                    , out _result_meter, out _result_session_meter, Trx))
      {
        Log.Message("An error occurred calling CageMeters.UpdateCageMeters.");

        return false;
      }

      if (!Progressives.InsertProvisionCageMovement(CageSessionId, CageOperationType, SourceId, TerminalId, out _movement_id, Trx))
      {
        Log.Message("An error occurred calling Progressives.InsertProvisionCageMovement.");

        return false;
      }

      return true;
    } // CageProgressivesProvisionIncrement

    //------------------------------------------------------------------------------
    // PURPOSE : Actions to do when we have to pay a progressive jackpot.
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveId
    //            - ProgLevel
    //            - ProgAmount
    //            - TerminalId
    //            - SqlTrx
    //
    //      - OUTPUT :
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean ProgressiveReserveInsertPlaySession(Int32 TerminalId,
                                                               Decimal ProgAmount,
                                                               SqlTransaction Trx,
                                                               out Int64 PlaySessionId)
    {
      StringBuilder _sb;
      SqlParameter _param_play_session;
      Int64 _accound_id;

      PlaySessionId = -1;

      _accound_id = Accounts.GetOrCreateVirtualAccount(TerminalId, AccountType.ACCOUNT_VIRTUAL_TERMINAL, Trx);

      if (_accound_id == 0)
      {
        Log.Message("An error occurred calling Accounts.GetOrCreateVirtualAccount:  Returned AccountId = 0.");

        return false;
      }

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("INSERT INTO PLAY_SESSIONS ( PS_ACCOUNT_ID ");
        _sb.AppendLine("                          , PS_TERMINAL_ID ");
        _sb.AppendLine("                          , PS_TYPE ");
        _sb.AppendLine("                          , PS_INITIAL_BALANCE ");
        _sb.AppendLine("                          , PS_FINAL_BALANCE ");
        _sb.AppendLine("                          , PS_PLAYED_COUNT ");
        _sb.AppendLine("                          , PS_PLAYED_AMOUNT ");
        _sb.AppendLine("                          , PS_WON_COUNT ");
        _sb.AppendLine("                          , PS_WON_AMOUNT ");
        _sb.AppendLine("                          , PS_CASH_IN ");
        _sb.AppendLine("                          , PS_CASH_OUT ");
        _sb.AppendLine("                          , PS_STATUS ");
        _sb.AppendLine("                          , PS_STARTED ");
        _sb.AppendLine("                          , PS_FINISHED ");
        _sb.AppendLine("                          , PS_STAND_ALONE ");
        _sb.AppendLine("                          , PS_PROMO ");
        _sb.AppendLine("                          ) ");
        _sb.AppendLine("                   VALUES ( @pAccountId ");      // PS_ACCOUNT_ID
        _sb.AppendLine("                          , @pTerminalId ");     // PS_TERMINAL_ID
        _sb.AppendLine("                          , @pType ");           // PS_TYPE
        _sb.AppendLine("                          , @pInitialBalance "); // PS_INITIAL_BALANCE
        _sb.AppendLine("                          , @pFinalBalance ");   // PS_FINAL_BALANCE
        _sb.AppendLine("                          , 0 ");                // PS_PLAYED_COUNT
        _sb.AppendLine("                          , 0 ");                // PS_PLAYED_AMOUNT
        _sb.AppendLine("                          , 0 ");                // PS_WON_COUNT
        _sb.AppendLine("                          , 0 ");                // PS_WON_AMOUNT
        _sb.AppendLine("                          , 0 ");                // PS_CASH_IN
        _sb.AppendLine("                          , 0 ");                // PS_CASH_OUT
        _sb.AppendLine("                          , @pStatus ");         // PS_STATUS
        _sb.AppendLine("                          , GETDATE() ");        // PS_STARTED
        _sb.AppendLine("                          , GETDATE() ");        // PS_FINISHED
        _sb.AppendLine("                          , 1 ");                // PS_STAND_ALONE
        _sb.AppendLine("                          , 0 ");                // PS_PROMO
        _sb.AppendLine("                          ) ");
        _sb.AppendLine("                           SET @pPlaySessionId = SCOPE_IDENTITY() ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = _accound_id;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = PlaySessionType.PROGRESSIVE_RESERVE;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = PlaySessionStatus.ProgressiveReserve;

          if (ProgAmount > 0)
          {
            _cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).Value = ProgAmount;
            _cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).Value = 0;
          }
          else
          {
            _cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).Value = 0;
            _cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).Value = (ProgAmount * -1);
          }

          _param_play_session = _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt);
          _param_play_session.Direction = ParameterDirection.Output;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            Log.Message("An error occurred inserting into PLAY_SESSIONS:  Rows affected <> 1.");

            return false;
          }

          PlaySessionId = (Int64)_param_play_session.Value;

        }
        return true;
      }
      catch
      {
        return false;
      }

    } // ProgressiveReserveInsertPlaySession


    //------------------------------------------------------------------------------
    // PURPOSE : Actions to do when we have to pay a progressive jackpot.
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveId
    //            - ProgLevel
    //            - ProgAmount
    //            - TerminalId
    //            - SqlTrx
    //
    //      - OUTPUT :
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean ProgressiveJackpotPaid(Int64 ProgressiveId, Int32 ProgLevel, Decimal ProgAmount, Int32 TerminalId, Int64 HandpayId, SqlTransaction SqlTrx)
    {
      return ProgressiveJackpotPaid(ProgressiveId, ProgLevel, ProgAmount, TerminalId, HandpayId, false, SqlTrx);
    }
    public static Boolean ProgressiveJackpotPaid(Int64 ProgressiveId, Int32 ProgLevel, Decimal ProgAmount, Int32 TerminalId, Int64 HandpayId, Boolean Cancel, SqlTransaction SqlTrx)
    {
      Int64 _playsession_id;
      Int64 _cage_session_id;

      if (!SubstractProgressiveAmount(ProgressiveId, ProgLevel, (Cancel ? ProgAmount * -1 : ProgAmount), TerminalId, SqlTrx))
      {
        return false;
      }

      if (!ProgressiveReserveInsertPlaySession(TerminalId, (Cancel ? ProgAmount * -1 : ProgAmount), SqlTrx, out _playsession_id))
      {
        return false;
      }

      MultiPromos.AccountBalance _progressive_amount;
      _progressive_amount = new MultiPromos.AccountBalance(ProgAmount, 0, 0);

      if (!MultiPromos.Trx_PlaySessionSetMeters(_playsession_id,
                                                (Cancel ? MultiPromos.AccountBalance.Zero : _progressive_amount),
                                                MultiPromos.AccountBalance.Zero,
                                                MultiPromos.AccountBalance.Zero,
                                                (Cancel ? _progressive_amount : MultiPromos.AccountBalance.Zero),
                                                0, 
                                                SqlTrx))
      {
        Log.Message("An error occurred calling MultiPromos.Trx_PlaySessionSetMeters.");

        return false;
      }

      // Get Cage Session Id
      if (!Cage.GetCageDefaultSessionId(out _cage_session_id, SqlTrx))
      {
        Log.Message("An error occurred calling Cage.GetCageDefaultSessionId.");

        return false;
      }

      // Cage Provision Increment: Awarded --> Decrement
      if (!CageProgressivesProvisionIncrement((Cancel ? Cage.CageOperationType.ProgressiveCancelAwarded : Cage.CageOperationType.ProgressiveAwarded),
                                              HandpayId,
                                              TerminalId,
                                              ProgAmount,
                                              _cage_session_id,
                                              SqlTrx))
      {
        return false;
      }

      if (!Statistics_InsertUpdateJackpotAmount(TerminalId, (Cancel ? ProgAmount * -1 : ProgAmount), SqlTrx))
      {
        return false;
      }

      return true;

    } // ProgressiveJackpotPaid


    //------------------------------------------------------------------------------
    // PURPOSE : Actions to do when we have to pay a progressive jackpot.
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveId
    //            - ProgLevel
    //            - ProgAmount
    //            - TerminalId
    //            - SqlTrx
    //
    //      - OUTPUT :
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean RegisterProgressiveJackpotAwarded(Int64 ProgressiveId, Int32 ProgLevel, Decimal ProgAmount, Int32 TerminalId, SqlTransaction SqlTrx)
    {
      Int64 _handpay_id;

      if (!DB_InsertProgressiveSpecialHandpay(ProgressiveId, ProgLevel, ProgAmount, TerminalId, out _handpay_id, SqlTrx))
      {
        Log.Message("An error occurred calling DB_InsertProgressiveSpecialHandpay.");

        return false;
      }

      if (!ProgressiveJackpotPaid(ProgressiveId, ProgLevel, ProgAmount, TerminalId, _handpay_id, SqlTrx))
      {
        Log.Message("An error occurred calling ProgressiveJackpotPaid.");

        return false;
      }

      return true;
    } // RegisterProgressiveJackpotAwarded 

    //------------------------------------------------------------------------------
    // PURPOSE : Cancels a provision.
    //
    //  PARAMS :
    //      - INPUT:
    //            - ProgressiveProvision
    //            - SqlTrx
    //
    //      - OUTPUT :
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - Boolean
    //
    public static Boolean CancelProvision(ProgressiveProvision ProgressiveProvision, SqlTransaction SqlTrx, out List<String> ErrorOnTerminals)
    {
      StringBuilder _sql_query;
      DateTime _last_provision_date;
      Int64 _cage_session_id;

      ErrorOnTerminals = new List<string>();

      try
      {
        // Steps to cancel a progressive jackpot provision:
        //    1. Update progressive tables (PROGRESSIVES, PROGRESSIVES_LEVELS)
        //    2. Update provision as canceled (PROGRESSIVES_PROVISIONS)
        //    3. Update LAST_PROVISION_DATE from PROGRESSIVES table
        //    4. Update Statistics
        //    5. Insert provision's play session
        //    6. Update Cage


        //    Step 1: Update progressive tables (PROGRESSIVES, PROGRESSIVES_LEVELS)
        //              1.1 Subtract from each progressive level the amount provided
        //              1.2 Subtract from global progressive amount the amount provided

        // 1.1 Subtract from each progressive level the amount provided
        foreach (DataRow _dr in ProgressiveProvision.Levels.Rows)
        {
          // Update progressives levels table
          _sql_query = new StringBuilder();
          _sql_query.AppendLine("UPDATE   PROGRESSIVES_LEVELS                             ");
          _sql_query.AppendLine("   SET   PGL_AMOUNT = PGL_AMOUNT - @pLevelProvisionAmount");
          _sql_query.AppendLine(" WHERE   PGL_PROGRESSIVE_ID = @pProgressiveID            ");
          _sql_query.AppendLine("   AND   PGL_LEVEL_ID = @pLevelID                        ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
          {
            _sql_cmd.Parameters.Add("@pProgressiveID", SqlDbType.BigInt).Value = ProgressiveProvision.ProgressiveId;
            _sql_cmd.Parameters.Add("@pLevelID", SqlDbType.BigInt).Value = _dr["LEVEL_ID"];
            _sql_cmd.Parameters.Add("@pLevelProvisionAmount", SqlDbType.Money).Value = _dr["PROVISION_AMOUNT"];

            if (_sql_cmd.ExecuteNonQuery() != 1)
            {
              Log.Message("An error occurred updating PROGRESSIVES_LEVELS: Rows affected <> 1.");

              return false;
            }
          }
        }

        // 1.2 Subtract from global progressive amount the amount provided
        _sql_query = new StringBuilder();
        _sql_query.AppendLine("UPDATE   PROGRESSIVES                                              ");
        _sql_query.AppendLine("   SET   PGS_AMOUNT = PGS_AMOUNT - @pProvisionAmount               ");
        _sql_query.AppendLine(" WHERE   PGS_PROGRESSIVE_ID = @pProgressiveID                      ");
        _sql_query.AppendLine("   AND   PGS_LAST_PROVISIONED_HOUR = @pPreviousLastProvisionedHour ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pProgressiveID", SqlDbType.BigInt).Value = ProgressiveProvision.ProgressiveId;
          _sql_cmd.Parameters.Add("@pProvisionAmount", SqlDbType.Money).Value = ProgressiveProvision.Amount;
          _sql_cmd.Parameters.Add("@pPreviousLastProvisionedHour", SqlDbType.DateTime).Value = ProgressiveProvision.ProgressiveLastProvisionedHour;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Message("An error occurred updating PROGRESSIVES: Rows affected <> 1.");

            return false;
          }
        }

        // 2. Update provision as canceled (PROGRESSIVES_PROVISIONS)
        _sql_query = new StringBuilder();
        _sql_query.AppendLine("UPDATE   PROGRESSIVES_PROVISIONS             ");
        _sql_query.AppendLine("   SET   PGP_STATUS = @pNewStatus            ");
        _sql_query.AppendLine(" WHERE   PGP_PROVISION_ID = @pProvisionID    ");
        _sql_query.AppendLine("   AND   PGP_PROGRESSIVE_ID = @pProgressiveID");
        _sql_query.AppendLine("   AND   PGP_STATUS = @pOldStatus            ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = PROGRESSIVE_PROVISION_STATUS.CANCELED;
          _sql_cmd.Parameters.Add("@pProvisionID", SqlDbType.BigInt).Value = ProgressiveProvision.ProvisionId;
          _sql_cmd.Parameters.Add("@pProgressiveID", SqlDbType.BigInt).Value = ProgressiveProvision.ProgressiveId;
          _sql_cmd.Parameters.Add("@pOldStatus", SqlDbType.Int).Value = ProgressiveProvision.Status;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Message("An error occurred updating PROGRESSIVES_PROVISIONS: Rows affected <> 1.");

            return false;
          }
        }

        // 3. Update LAST_PROVISION_DATE from PROGRESSIVES table
        if (!GetLastProvisionDate(ProgressiveProvision.ProgressiveId, out _last_provision_date, SqlTrx))
        {
          Log.Message("An error occurred calling GetLastProvisionDate.");

          return false;
        }

        _sql_query = new StringBuilder();
        _sql_query.AppendLine("UPDATE   PROGRESSIVES ");

        if (_last_provision_date != DateTime.MinValue)
        {
          _sql_query.AppendLine("   SET   PGS_LAST_PROVISIONED_HOUR = @pLastProvisionedHour ");
        }
        else
        {
          _sql_query.AppendLine("   SET   PGS_LAST_PROVISIONED_HOUR = NULL ");
        }

        _sql_query.AppendLine(" WHERE   PGS_PROGRESSIVE_ID = @pProgressiveID");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_query.ToString(), SqlTrx.Connection, SqlTrx))
        {
          if (_last_provision_date != DateTime.MinValue)
          {
            _sql_cmd.Parameters.Add("@pLastProvisionedHour", SqlDbType.DateTime).Value = _last_provision_date;
          }

          _sql_cmd.Parameters.Add("@pProgressiveID", SqlDbType.BigInt).Value = ProgressiveProvision.ProgressiveId;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            Log.Message("An error occurred updating PROGRESSIVES: Rows affected <> 1.");

            return false;
          }
        }

        // 4. Update Statistics
        if (!Statistics_InsertUpdateProvision(ProgressiveProvision, true, SqlTrx, out ErrorOnTerminals))
        {
          return false;
        }

        // 5. Insert provision's play session
        if (!PlaySessions_InsertProvision(ProgressiveProvision, true, SqlTrx))
        {
          return false;
        }

        // 6. Update Cage
        if (Cage.IsCageEnabled())
        {
          // Get Cage Session Id
          if (!Cage.GetCageDefaultSessionId(out _cage_session_id, SqlTrx))
          {
            Log.Message("An error occurred getting cage default session id.");

            return false;
          }

          if (!CageProgressivesProvisionIncrement(Cage.CageOperationType.ProgressiveCancelProvision,
                                                  ProgressiveProvision.ProvisionId,
                                                  0,
                                                  ProgressiveProvision.TotalCageProvision,
                                                  _cage_session_id,
                                                  SqlTrx))
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Gets the configuration for provisioning input mode
    //
    //  PARAMS :
    //      - INPUT:
    //
    //      - OUTPUT :
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - PROGRESSIVE_PROVISION_INPUT_MODE
    //
    public static PROGRESSIVE_PROVISION_INPUT_MODE GetProvisionInputMode()
    {
      return (PROGRESSIVE_PROVISION_INPUT_MODE)GeneralParam.GetInt32("Progressives", "Provision.InputMode", (Int32)PROGRESSIVE_PROVISION_INPUT_MODE.COUNTER_READING);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Gets the configuration for provisioning distribution mode
    //
    //  PARAMS :
    //      - INPUT:
    //
    //      - OUTPUT :
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - PROGRESSIVE_PROVISION_DISTRIBUTION_MODE
    //
    public static PROGRESSIVE_PROVISION_DISTRIBUTION_MODE GetProvisionDistributionMode()
    {
      return (PROGRESSIVE_PROVISION_DISTRIBUTION_MODE)GeneralParam.GetInt32("Progressives", "Provision.DistributionMode", (Int32)PROGRESSIVE_PROVISION_DISTRIBUTION_MODE.THEORETICAL);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Gets the configuration for provisioning to cage
    //
    //  PARAMS :
    //      - INPUT:
    //
    //      - OUTPUT :
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - PROGRESSIVE_PROVISION_GOES_TO_CAGE
    //
    public static PROGRESSIVE_PROVISION_GOES_TO_CAGE ProvisionGoesToCage()
    {
      return (PROGRESSIVE_PROVISION_GOES_TO_CAGE)GeneralParam.GetInt32("Progressives", "Provision.GoesToCage", (Int32)PROGRESSIVE_PROVISION_GOES_TO_CAGE.YES);
    }



    //***** PROGRESIVE SITE JACKPOT FUNCTIONS (NOT USED, NOT USE) ******//
    //******************************************************************//

    //------------------------------------------------------------------------------
    // PURPOSE : Provisioning SiteJackPot (NOT CALL; NOT USED)
    //
    //  PARAMS :
    //      - INPUT:
    //          - SqlTrx
    //      - OUTPUT :
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - bool
    //
    public static Boolean GenerateSiteJackpotProvision(SqlTransaction SqlTrx)
    {
      DateTime _hour_from;
      DateTime _hour_to;
      DataTable _site_jackpot_terminals;
      Decimal _total_amount_provision;
      Decimal _terminal_total_played;
      Int64 _cage_session_id;
      List<string> ErrorOnTerminals;

      SITE_JACKPOT_PROVISION_MODE _site_jackpot_provision_mode; 
      ProgressiveProvision _provisions_site_jackpot;

      //Inititalize 
      _cage_session_id = 0;
      _total_amount_provision = 200;
      _hour_from = new DateTime();
      _hour_to = new DateTime();
      _provisions_site_jackpot = new ProgressiveProvision();
      ErrorOnTerminals = new List<string>();
      
      // Process
      // 1. Check GP (Bonusing.Provision.Mode) 
      _site_jackpot_provision_mode = GetSiteJackPotBonusingProvisionMode();
      if ((Handpays.SiteJackpotPaymentMode == HANDPAY_PAYMENT_MODE.BONUSING) &&
         (_site_jackpot_provision_mode == SITE_JACKPOT_PROVISION_MODE.PROVISION_WITH_CAGE_METER_INCREMENT ||
         _site_jackpot_provision_mode == SITE_JACKPOT_PROVISION_MODE.PROVISION_WITHOUT_CAGE_METER_INCREMENT))
      {


        // 1.1. Get Amount for provisioning (Pending AC, AJ, RC)
        //
        //
        //
        //Pending (AC)
        _hour_from = WSI.Common.Misc.TodayOpening().AddDays(-1);       // Dia Anterior desde inicio FROM 
        _hour_to = WSI.Common.Misc.TodayOpening().AddDays(0);          // Dia Anterior desde inicio TO
        _provisions_site_jackpot.HourFrom = _hour_from;
        _provisions_site_jackpot.HourTo = _hour_to;


        // 1.2. Get Site JackPot Terminals  (if there no terminals return false?)
        if (!Progressives.GetPlayedFromProgressiveTerminals(0, _hour_from, _hour_to, out _site_jackpot_terminals, true))
        {
          //Pending Write Log Error

          return false;
        }
        // 1.3. Distribute Amount (punto 1.1) to Terminals (get in "punto 1.2") 
        // El tercer par�metro es pasado como el monto del �ltimo nivel. Preguntar a AC
        if (!Progressives.ProvisionDistribute(_total_amount_provision, 0, _total_amount_provision, ref _site_jackpot_terminals, out _terminal_total_played))
        {
          //Pending Write Log Error

          return false;
        }

        // 1.4 Generar provision (Pending)
          // Insertar Progressive_Provisions



        // 1.5 Update Statistics
        if (!Statistics_InsertUpdateProvision(_provisions_site_jackpot, SqlTrx, out ErrorOnTerminals))
        {
          //Pending Write Log Error

          return false;
        }
        
        // 1.6 Insertar play Session con los datos anteriores. 
        _provisions_site_jackpot.Terminals = _site_jackpot_terminals;
        if (!Progressives.PlaySessions_InsertProvision(_provisions_site_jackpot, SqlTrx))
        {
          //Pending Write Log Error
          
          return false;
        }

        // 1.7 Actualizar contador de boveda segun GP (PENDING AC)
        // Si se quiere actualizar el contador de b�veda [Provisi�n progresivos]. Sumar al provisionar y restar cuando toca. 
        // Cage Provision (Revisar GP y preguntar si controlar si b�veda activado)

        if (Cage.IsCageEnabled() && _site_jackpot_provision_mode == SITE_JACKPOT_PROVISION_MODE.PROVISION_WITH_CAGE_METER_INCREMENT)
        {

          // Get Cage Session Id
          if (!Cage.GetCageDefaultSessionId(out _cage_session_id, SqlTrx))
          {
            Log.Message("An error occurred getting cage default session id.");

            return false;
          }

          if (!CageProgressivesProvisionIncrement(Cage.CageOperationType.ProgressiveProvision, 0, 0, _total_amount_provision, _cage_session_id, SqlTrx))
          {
            //Pending Write Log Error

            return false; 
          }
          // Everything went ok.  Update provision object
          //ProgressiveProvision.CageSessionId = _cage_session_id;
        }
      }
      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Compensate SiteJackPot (NOT CALL; NOT USED)
    //
    //  PARAMS :
    //      - INPUT:
    //          - SqlTrx
    //      - OUTPUT :
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - bool
    //
    public static Boolean GenerateSiteJackpotCompensation(Decimal         Amount,  
                                                          Int32           TerminalId, 
                                                          Int64           HandpayId, 
                                                          SqlTransaction  SqlTrx)
    {

      Int64 _playsession_id;
      Int64 _cage_session_id;
      SITE_JACKPOT_PROVISION_MODE _site_jackpot_provision_mode; 

      // Process
      // 1. Check GP (Bonusing.Provision.Mode) 
      _site_jackpot_provision_mode = GetSiteJackPotBonusingProvisionMode();

      if  ((Handpays.SiteJackpotPaymentMode == HANDPAY_PAYMENT_MODE.BONUSING) && 
          (_site_jackpot_provision_mode == SITE_JACKPOT_PROVISION_MODE.PROVISION_WITH_CAGE_METER_INCREMENT ||
          _site_jackpot_provision_mode == SITE_JACKPOT_PROVISION_MODE.PROVISION_WITHOUT_CAGE_METER_INCREMENT))
      {
        
        // 2. Insertar play session (ProgressiveReserve) para el terminal que ha tocado el jackpot de sala 
        //if (!ProgressiveReserveInsertPlaySession(TerminalId, (Cancel ? Amount * -1 : Amount), SqlTrx, out _playsession_id))
        if (!ProgressiveReserveInsertPlaySession(TerminalId, Amount, SqlTrx, out _playsession_id))
        {
          //Pending Write Log Error

          return false;
        }

        // 3. Cage counter Decrement
        if (Cage.IsCageEnabled() && _site_jackpot_provision_mode == SITE_JACKPOT_PROVISION_MODE.PROVISION_WITH_CAGE_METER_INCREMENT)
        {
          // Get Cage Session Id
          if (!Cage.GetCageDefaultSessionId(out _cage_session_id, SqlTrx))
          {
            Log.Message("An error occurred calling Cage.GetCageDefaultSessionId.");

            return false;
          }

          // Cage Provision Increment: Awarded --> Decrement (Penfing AC) (check GP)
          //if (!CageProgressivesProvisionIncrement((Cancel ? Cage.CageOperationType.ProgressiveCancelAwarded : Cage.CageOperationType.ProgressiveAwarded),
          if (!CageProgressivesProvisionIncrement((Cage.CageOperationType.ProgressiveAwarded),
                                                  HandpayId,
                                                  TerminalId,
                                                  Amount,
                                                  _cage_session_id,
                                                  SqlTrx))
          {
            //Pending Write Log Error

            return false;
          }
        }

        // 4. Update Statistics
        //if (!Statistics_InsertUpdateJackpotAmount(TerminalId, (Cancel ? Amount * -1 : Amount), SqlTrx))
        if (!Statistics_InsertUpdateJackpotAmount(TerminalId, Amount, SqlTrx))
        {
          //Pending Write Log Error

          return false;
        }
      }

       return true;
    }


    //------------------------------------------------------------------------------
    // PURPOSE : Gets the configuration for SiteJackPot provisioning distribution mode
    //
    //  PARAMS :
    //      - INPUT:
    //
    //      - OUTPUT :
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - PROGRESSIVE_PROVISION_DISTRIBUTION_MODE
    //
    public static SITE_JACKPOT_PROVISION_MODE GetSiteJackPotBonusingProvisionMode()
    {
      return (SITE_JACKPOT_PROVISION_MODE)GeneralParam.GetInt32("SiteJackpot", "Bonusing.Provision.Mode", (Int32)SITE_JACKPOT_PROVISION_MODE.NONE);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Gets the configuration for SiteJackPot provisioning distribution mode
    //
    //  PARAMS :
    //      - INPUT:
    //
    //      - OUTPUT :
    //
    //      - INPUT/OUTPUT :
    //
    // RETURNS :
    //      - PROGRESSIVE_PROVISION_DISTRIBUTION_MODE
    //
    public static PROGRESSIVE_PROVISION_DISTRIBUTION_MODE GetSiteJackPotProvisionDistributionMode()
    {
      return (PROGRESSIVE_PROVISION_DISTRIBUTION_MODE)GeneralParam.GetInt32("SiteJackpot", "Bonusing.Provision.DistributionMode", (Int32)PROGRESSIVE_PROVISION_DISTRIBUTION_MODE.THEORETICAL);
    }

    //***** PROGRESIVE SITE JACKPOT FUNCTIONS (NOT USED, NOT USE) ******
    //******************************************************************

    #endregion // Public Functions
  }
}
