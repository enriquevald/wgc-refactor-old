//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: XMLReports.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Javier Molina
// 
// CREATION DATE: 13-JAN-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-JAN-2012 JMM    First release.
// 02-FEB-2012 JMM    Clean empty tags on XML report.
// 03-FEB-2012 JMM    Query enhancement: select from CASHIER_MOVEMENTS 
//                    instead of ACCOUNT_MOVEMENTS.
// 03-FEB-2012 JMM    Remove milliseconds from date.
// 09-FEB-2012 JMM    Ensure RFC & CURP on Accounts datatable contain same value 
//                    that last movement.
// 14-FEB-2012 JMM    Add DocumentID from the evidence to the movements datatable.
// 10-AUG-2012 DDM    Modified function AccountMovements
// 20-SEP-2012 RRB    Add function AddCashierSessionsTable
// 03-SEP-2013 RCI    Fixed Bug WIG-183: Recharge for points: invalid cashier movements
// 01-OCT-2013 ANG    Add PrmioEspecie column to output AccountMovements() datatable.
// 25-NOV-2013 ANG    Fix bug WIG-428
// 04-DEC-2013 DHA    Fixed bug WIG-455: ISR2 now is reported
// 20-JAN-2015 JML    Product Backlog Item 202:Codere M�xico - Changes for gamming table activation
// 27-FEB-2015 JML & RCI & DDM Fixed bug WIG-2121: Send money to tables
// 06-AUG-2015 DDM    TFS-3569: Botched Job for AlesisWinpot.
// 13-JAN-2015 JML    Product Backlog Item 6559: Cashier draw (RE in kind)
// 03-MAR-2016 DDM    Product Backlog Item 10292: PSAClient-Alesis-Palacio: Get operations of external database 
//                    and modifications (print xml, constancias LastDayAsNextMonth)
// 10-MAR-2016 DDS    Fixed Bug 10564:Ref.16443: Added optional filter by accountId on SQL Query � Sala Jubile
// 26-APR-2016 EOR    Product Backlog Item 11298: Codere - Tax Custody: Reports GUI 
// 08-JUN-2016 DLL    Bug 14292: Prize retention error.
// 19-SEP-2016 EOR    PBI 17468: Televisa - Service Charge: Cashier Movements
// 21-OCT-2016 FGB    PBI 10324: PSA Client: obtaining data for the withhold (evidence)
// 23-NOV-2016 ETP    Fixed Bug 20352: Cashdesk draw with cash prize is not reported.
// 16-MAR-2017 ATB    PBI 25737:Third TAX - Report columns
// 21-MAR-2017 FAV    PBI 25735:Third TAX - Movement reports
// 01-JUN-2017 DHA    Bug 27844:Descuadre en reporte de PSAClient con Sorteo de Caja en v3.004 en salas Crown City
// 11-JAN-2018 JML    Fixed Bug 31238:WIGOS-7384 [Ticket #11446] Anulaci�n de Venta Fichas,no se anula en reportes
// 10-APR-2018 JML    Feature 32223:WIGOS-9856 Parametrizaci�n de la inclusi�n del impuesto sobre erogaciones en el c�lculo del monto diario de salidas de caja que se informa en los balances diarios de caja reportados al PSA
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common
{
  public class XMLReport
  {
    #region Enums
    public enum PSAClient_EspecialMode
    {
      Default = 0,
      AlesisWinpot = 1, // especial mode for Winpot...
    }

    public enum EnumAccountMovementsReport
    {
      Default = 1,
      CashierSessionsTable = 2,
      RetreatsReport = 3,
    }
    #endregion

    #region Constants
    // Enum for an CashierMovements datatable.
    private const string TABLE_NAME_CASHIER_MOVEMENTS = "Movimiento";

    public struct COL_CASHIER_MOVS
    {
      public const int OPERATION_ID = 0;
      public const int DATETIME = 1;
      public const int ACCOUNT_ID = 2;
      public const int CASHIN_AMOUNT = 3;
      public const int DEVOLUTION_AMOUNT = 4;
      public const int PRIZE_AMOUNT = 5;
      public const int TAX_ON_PRIZE1_AMOUNT = 6;
      public const int TAX_ON_PRIZE2_AMOUNT = 7;
      public const int TAX_ON_PRIZE3_AMOUNT = 8;
      public const int HOLDER_RFC = 9;
      public const int HOLDER_CURP = 10;
      public const int HOLDER_NAME = 11;
      public const int HOLDER_ADDRESS = 12;
      public const int HOLDER_CITY = 13;
      public const int HOLDER_ZIP = 14;
      public const int DOCUMENT_ID = 15;
      public const int EVIDENCE = 16;
      public const int EVIDENCE_HOLDER_RFC = 17;
      public const int EVIDENCE_HOLDER_CURP = 18;
      public const int EVIDENCE_HOLDER_NAME = 19;
      public const int EVIDENCE_ADDRESS = 20;
      public const int EVIDENCE_CITY = 21;
      public const int EVIDENCE_ZIP = 22;
      public const int EVIDENCE_DOCUMENT_ID = 23;
      public const int EVIDENCE_NO_DOCUMENT_ID = 24;
      public const int TAX_RETURNING = 25;
      public const int SERVICE_CHARGE = 26;
      public const int DECIMAL_ROUNDING = 27;
      public const int CASHIER_SESSION = 28;
      public const int PRIZE_IN_KIND_AMOUNT = 29;

      public const int PRIZE_RE_IN_KIND_AMOUNT = 30;  //LA 19-FEB-2016
      public const int TAX_CUSTODY = 31;              //EOR 26-APR-2016

      // Specific for XmlReportsPSA
      public const int CASHIER_NAME = 32;
      public const int PRIZE_EXPIRED = 33;
      public const int VOUCHER_ID = 34;
      public const int FOLIO = 35;
      public const int PROMOTIONAL_BALANCE = 36;
      public const int PAYMENT_TYPE_KEY = 37;
      public const int PAYMENT_METHOD = 38;
      public const int RECHARGE_FOR_POINTS_PRIZE = 39;
      public const int TOTAL_DEVOLUTION_A = 40;
      public const int TOTAL_DEVOLUTION_B = 41;

      // FGB 21-OCT-2016: Additional fields for withholding (constancia)
      public const int EVIDENCE_CALLE = 42;
      public const int EVIDENCE_MUNICIPIO = 43;
      public const int EVIDENCE_ESTADO = 44;
      public const int EVIDENCE_PAIS = 45;
      public const int EVIDENCE_NO_EXTERIOR = 46;
      public const int EVIDENCE_NO_INTERIOR = 47;
      public const int EVIDENCE_COLONIA = 48;
      public const int EVIDENCE_REFERENCIA = 49;
      public const int EVIDENCE_CODIGO_POSTAL = 50;
      public const int EVIDENCE_LOCALIDAD = 51;
    }

    // Enum for AccountMovements datatable
    private const string TABLE_NAME_ACCOUNT_MOVEMENTS = "Cuenta";

    private struct COL_SELECT_MOVS
    {
      public const int OPERATION_ID = 0;
      public const int DATETIME = 1;
      public const int TYPE = 2;
      public const int ADD_AMOUNT = 3;
      public const int SUB_AMOUNT = 4;
      public const int ACCOUNT_ID = 5;
      public const int HOLDER_NAME = 6;
      public const int HOLDER_RFC = 7;
      public const int HOLDER_CURP = 8;
      public const int HOLDER_ADDRESS = 9;
      public const int HOLDER_CITY = 10;
      public const int HOLDER_ZIP = 11;
      public const int HOLDER_TRACK_DATA = 12;
      public const int DOCUMENT_ID = 13;
      public const int EVIDENCE = 14;
      public const int EVIDENCE_HOLDER_NAME = 15;
      public const int EVIDENCE_HOLDER_RFC = 16;
      public const int EVIDENCE_HOLDER_CURP = 17;
      public const int EVIDENCE_DOCUMENT_ID = 18;
      public const int EVIDENCE_NO_DOCUMENT_ID = 19;
      public const int EVIDENCE_PLAYER_NAME1 = 20;
      public const int EVIDENCE_PLAYER_NAME2 = 21;
      public const int EVIDENCE_PLAYER_NAME3 = 22;
      public const int CASHIER_SESSION = 23;
      public const int CASHIER_NAME = 24;

      // FGB 21-OCT-2016: Additional fields for withholding (constancia)
      public const int HOLDER_ADDRESS_01 = 25;
      public const int HOLDER_ADDRESS_02 = 26;
      public const int HOLDER_ADDRESS_03 = 27;
      public const int HOLDER_STATE_ID = 28;
      public const int HOLDER_STATE_DESC = 29;
      public const int HOLDER_COUNTRY_ID = 30;
      public const int HOLDER_COUNTRY_DESC = 31;
      public const int EVIDENCE_PLAYER_BIRTH_DATE = 32;
      public const int EVIDENCE_EXT_NUM = 33;
      public const int EVIDENCE_INT_NUM = 34;
      public const int EVIDENCE_REFERENCE = 35;
      public const int EVIDENCE_LOCALITY = 36;
    }

    private const string DATASET_NAME = "MovimientosCuenta";
    private const string TABLE_NAME_TOTAL_MOVEMENTS = "TotalMovimientos";

    private const int COL_ACCS_ACCOUNT_ID = 0;
    private const int COL_ACCS_TOTAL_CASHIN = 1;
    private const int COL_ACCS_TOTAL_DEVOLUTION = 2;
    private const int COL_ACCS_TOTAL_PRIZE = 3;
    private const int COL_ACCS_TOTAL_ISR1 = 4;
    private const int COL_ACCS_TOTAL_ISR2 = 5;
    private const int COL_ACCS_TOTAL_ISR3 = 6;
    private const int COL_ACCS_RFC = 7;
    private const int COL_ACCS_CURP = 8;
    private const int COL_ACCS_NAME = 9;
    private const int COL_ACCS_ADDRESS = 10;
    private const int COL_ACCS_CITY = 11;
    private const int COL_ACCS_ZIP = 12;
    private const int COL_ACCS_TRACK_DATA = 13;
    private const int COL_ACCS_TOTAL_TAX_RETURNING = 14;
    private const int COL_ACCS_TOTAL_SERVICE_CHARGE = 15;
    private const int COL_ACCS_TOTAL_DECIMAL_ROUNDING = 16;
    private const int COL_ACCS_TOTAL_PRIZE_IN_KIND = 17;
    private const int COL_ACCS_TOTAL_PRIZE_RE_IN_KIND = 18;
    private const int COL_ACCS_TOTAL_CUSTODY = 19;    // EOR 26-APR-2016

    private const string TABLE_NAME_CASHIER_SESSIONS = "SesionesCaja";

    // Set default value for Tipo Pago.
    private const String TIPO_PAGO = "Efectivo";
    #endregion

    #region Public Functions
    public static PSAClient_EspecialMode GetPSAClient_EspecialMode()
    {
      PSAClient_EspecialMode _especial_mode;
      PSA_TYPE _psa_type;

      _especial_mode = (XMLReport.PSAClient_EspecialMode)GeneralParam.GetInt32("PSAClient", "SpecialMode", 0);

      if (_especial_mode == PSAClient_EspecialMode.Default)
      {
        return _especial_mode;
      }

      _psa_type = (PSA_TYPE)GeneralParam.GetInt32("PSAClient", "PSAType", (int)PSA_TYPE.WINPSA);

      if (_especial_mode == PSAClient_EspecialMode.AlesisWinpot && _psa_type == PSA_TYPE.WINPSA)
      {
        _especial_mode = PSAClient_EspecialMode.Default;
      }

      return _especial_mode;
    }

    // PURPOSE: Generate the Caja Unica report from a single day
    // 
    //  PARAMS:
    //      - INPUT:
    //            - DateTime DateToReport
    //            - Boolean TwentyFourHoursDays
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      DataSet containing all the report data
    //     
    public static DataSet CajaUnicaOneDayReport(DateTime DateToReport)
    {
      DateTime _from_date;
      DateTime _to_date;

      return CajaUnicaOneDayReport(DateToReport, out _from_date, out _to_date);
    } // CajaUnicaOneDayReport

    // PURPOSE: Generate the Caja Unica report from a single day
    // 
    //  PARAMS:
    //      - INPUT:
    //            - DateTime DateToReport
    //            - Boolean TwentyFourHoursDays
    //
    //      - OUTPUT:
    //            - DateTime FromDate
    //            - DateTime ToDate
    //
    // RETURNS: 
    //      DataSet containing all the report data
    //     
    public static DataSet CajaUnicaOneDayReport(DateTime DateToReport, out DateTime FromDate, out DateTime ToDate)
    {
      DataSet _ds;

      //Get date from/to from DateToReport
      DateToReport = GetDateFromDateToReport(DateToReport, out FromDate, out ToDate);

      _ds = AccountMovements(FromDate, ToDate);

      // RRB 18-SEP-2012 Remove Columns
      RemoveUnusedColumns(_ds);

      return _ds;
    } // CajaUnicaOneDayReport

    //------------------------------------------------------------------------------
    // PURPOSE: Reports all the prizes greater than PrizeAbove value between two dates for all the accounts
    // 
    //  PARAMS:
    //      - INPUT:
    //            - FromDate
    //            - ToDate
    //            - PrizeAbove
    //            - AccountId
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      DataSet containing all the report data
    public static DataSet AccountPrizes(DateTime FromDate, DateTime ToDate, Decimal PrizeAbove, Boolean EvidencesOnly, Int64? AccountId = null)
    {
      DataSet _ds;
      DataSet _ds_ret;
      DataRow[] _row_array;
      DataRow[] _prize_totals;
      DataTable _dt_prizes;
      DataTable _dt_accs;
      DataRow _acc_row;
      String _filter;

      // Get all the operations related to the selected time interval 
      _ds = AccountMovements(FromDate, ToDate, AccountId);

      // RRB 18-SEP-2012 Remove Columns
      RemoveUnusedColumns(_ds);

      _dt_prizes = _ds.Tables[TABLE_NAME_CASHIER_MOVEMENTS].Clone();
      _dt_accs = _ds.Tables[TABLE_NAME_ACCOUNT_MOVEMENTS].Clone();

      //_filter = "Premio > " + PrizeAbove;

      _filter = String.Format("( Premio > {0} Or PremioEspecie > {0} Or PremioEspecieRE > {0} or Premio < -{0} Or PremioEspecie < -{0} Or PremioEspecieRE < -{0}  )", PrizeAbove);

      if (EvidencesOnly)
      {
        _filter += " AND Constancia = True ";
      }

      // Select just those whose Prize amount is greater than PrizeAbove
      _row_array = _ds.Tables[TABLE_NAME_CASHIER_MOVEMENTS].Select(_filter, "IdOperacion");

      // Copy the selected ones into the output datatable _dt_prizes
      foreach (DataRow _row in _row_array)
      {
        // Browse the selected prizes and recalculate the prize related data based on PrizeAbove filter 
        //    - Operation's movements
        //    - Account prize totals

        //    - Operation's movements
        _dt_prizes.ImportRow(_row);

        //    - Account prize totals
        _prize_totals = _dt_accs.Select("NoCuenta = " + _row["NoCuenta"]);

        if (_prize_totals.Length == 0)
        {
          // Account not found => add it to the output list
          //    - Update RFC & CURP
          //    - Prize amount
          //    - Tax1
          //    - Tax2

          _acc_row = _ds.Tables[TABLE_NAME_ACCOUNT_MOVEMENTS].Select("NoCuenta = " + _row["NoCuenta"])[0];

          //    JMM 09-FEB-2012
          //    - Update RFC & CURP to ensure that on Accounts datatable RFC & CURP contain same value 
          //      that last movement.
          if (_row[COL_CASHIER_MOVS.EVIDENCE_HOLDER_RFC] != DBNull.Value)
          {
            _acc_row[COL_ACCS_RFC] = _row[COL_CASHIER_MOVS.EVIDENCE_HOLDER_RFC];
          }

          if (_row[COL_CASHIER_MOVS.EVIDENCE_HOLDER_CURP] != DBNull.Value)
          {
            _acc_row[COL_ACCS_CURP] = _row[COL_CASHIER_MOVS.EVIDENCE_HOLDER_CURP];
          }

          // Just prizes => remove cash in and devolution values
          _acc_row[COL_ACCS_TOTAL_CASHIN] = 0;
          _acc_row[COL_ACCS_TOTAL_DEVOLUTION] = 0;

          //    - Prize amount
          if (_row[COL_CASHIER_MOVS.PRIZE_AMOUNT] != DBNull.Value)
          {
            _acc_row[COL_ACCS_TOTAL_PRIZE] = (Decimal)_row[COL_CASHIER_MOVS.PRIZE_AMOUNT];
          }
          else
          {
            _acc_row[COL_ACCS_TOTAL_PRIZE] = 0;
          }

          //    - Tax1
          if (_row[COL_CASHIER_MOVS.TAX_ON_PRIZE1_AMOUNT] != DBNull.Value)
          {
            _acc_row[COL_ACCS_TOTAL_ISR1] = (Decimal)_row[COL_CASHIER_MOVS.TAX_ON_PRIZE1_AMOUNT];
          }
          else
          {
            _acc_row[COL_ACCS_TOTAL_ISR1] = 0;
          }

          //    - Tax2
          if (_row[COL_CASHIER_MOVS.TAX_ON_PRIZE2_AMOUNT] != DBNull.Value)
          {
            _acc_row[COL_ACCS_TOTAL_ISR2] = (Decimal)_row[COL_CASHIER_MOVS.TAX_ON_PRIZE2_AMOUNT];
          }
          else
          {
            _acc_row[COL_ACCS_TOTAL_ISR2] = 0;
          }

          //    - Tax3
          if (_row[COL_CASHIER_MOVS.TAX_ON_PRIZE3_AMOUNT] != DBNull.Value)
          {
            _acc_row[COL_ACCS_TOTAL_ISR3] = (Decimal)_row[COL_CASHIER_MOVS.TAX_ON_PRIZE3_AMOUNT];
          }
          else
          {
            _acc_row[COL_ACCS_TOTAL_ISR3] = 0;
          }

          _dt_accs.ImportRow(_acc_row);
        }
        else
        {
          // Account found => accumulate current values to the account totals
          //    - Update RFC & CURP
          //    - Prize amount
          //    - Tax1
          //    - Tax2

          _acc_row = _prize_totals[0];

          _acc_row.BeginEdit();

          //    JMM 09-FEB-2012
          //    - Update RFC & CURP to ensure that on Accounts datatable RFC & CURP contain same value 
          //      that last movement.
          if (_row[COL_CASHIER_MOVS.EVIDENCE_HOLDER_RFC] != DBNull.Value)
          {
            _acc_row[COL_ACCS_RFC] = _row[COL_CASHIER_MOVS.EVIDENCE_HOLDER_RFC];
          }

          if (_row[COL_CASHIER_MOVS.EVIDENCE_HOLDER_CURP] != DBNull.Value)
          {
            _acc_row[COL_ACCS_CURP] = _row[COL_CASHIER_MOVS.EVIDENCE_HOLDER_CURP];
          }

          //    - Prize amount
          if (_row[COL_CASHIER_MOVS.PRIZE_AMOUNT] != DBNull.Value)
          {
            _acc_row[COL_ACCS_TOTAL_PRIZE] = (Decimal)_acc_row[COL_ACCS_TOTAL_PRIZE]
                                           + (Decimal)_row[COL_CASHIER_MOVS.PRIZE_AMOUNT];
          }

          //    - Tax1
          if (_row[COL_CASHIER_MOVS.TAX_ON_PRIZE1_AMOUNT] != DBNull.Value)
          {
            _acc_row[COL_ACCS_TOTAL_ISR1] = (Decimal)_acc_row[COL_ACCS_TOTAL_ISR1]
                                          + (Decimal)_row[COL_CASHIER_MOVS.TAX_ON_PRIZE1_AMOUNT];
          }

          //    - Tax2
          if (_row[COL_CASHIER_MOVS.TAX_ON_PRIZE2_AMOUNT] != DBNull.Value)
          {
            _acc_row[COL_ACCS_TOTAL_ISR2] = (Decimal)_acc_row[COL_ACCS_TOTAL_ISR2]
                                          + (Decimal)_row[COL_CASHIER_MOVS.TAX_ON_PRIZE2_AMOUNT];
          }

          //    - Tax3
          if (_row[COL_CASHIER_MOVS.TAX_ON_PRIZE3_AMOUNT] != DBNull.Value)
          {
            _acc_row[COL_ACCS_TOTAL_ISR3] = (Decimal)_acc_row[COL_ACCS_TOTAL_ISR3]
                                          + (Decimal)_row[COL_CASHIER_MOVS.TAX_ON_PRIZE3_AMOUNT];
          }

          _acc_row.EndEdit();
        }
      }

      _ds_ret = new DataSet(_ds.DataSetName);

      // Publish the produced data 
      _ds_ret.Tables.Add(_dt_prizes);
      _ds_ret.Tables.Add(_dt_accs);
      _ds_ret.Tables.Add(_ds.Tables[TABLE_NAME_TOTAL_MOVEMENTS].Copy());

      return _ds_ret;
    } // AccountPrizes

    //------------------------------------------------------------------------------
    // PURPOSE: Add a DataTable with all Cashier Sessions in a range of
    // Min and Max from DataTable "Movimiento" in the DataSet passed
    // 
    //  PARAMS:
    //      - INPUT:
    //            - DataSet
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      Boolean: - True => OK
    //               - False => Error
    //  
    public static Boolean AddCashierSessionsTable(DataSet DataSetAMXMLReports)
    {
      StringBuilder _sb;
      DataTable _dt_cashier_sessions;
      Int64 _min_session_id;
      Int64 _max_session_id;

      _dt_cashier_sessions = new DataTable();

      _dt_cashier_sessions.TableName = TABLE_NAME_CASHIER_SESSIONS;

      if (DataSetAMXMLReports.Tables[TABLE_NAME_CASHIER_MOVEMENTS].Rows.Count == 0)
      {
        _min_session_id = Int64.MaxValue;
        _max_session_id = Int64.MinValue;
      }
      else
      {
        _min_session_id = (Int64)DataSetAMXMLReports.Tables[TABLE_NAME_CASHIER_MOVEMENTS].Compute("MIN(SesionCaja) ", "");
        _max_session_id = (Int64)DataSetAMXMLReports.Tables[TABLE_NAME_CASHIER_MOVEMENTS].Compute("MAX(SesionCaja) ", "");
      }

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   * ");
        _sb.AppendLine("   FROM   CASHIER_SESSIONS ");
        _sb.AppendLine("  WHERE   CS_SESSION_ID >= @pMinSessionId ");
        _sb.AppendLine("    AND   CS_SESSION_ID <= @pMaxSessionId ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pMinSessionId", SqlDbType.BigInt).Value = _min_session_id;
            _sql_cmd.Parameters.Add("@pMaxSessionId", SqlDbType.BigInt).Value = _max_session_id;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _sql_da.Fill(_dt_cashier_sessions);

              DataSetAMXMLReports.Tables.Add(_dt_cashier_sessions);

              DataSetAMXMLReports.AcceptChanges();

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } //AddCashierSessionsTable

    //------------------------------------------------------------------------------
    // PURPOSE: Generate the Caja Unica report from a two dates range
    // 
    //  PARAMS:
    //      - INPUT:
    //            - DateTime FromDate
    //            - DateTime ToDate
    //            - AccountId
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      DataSet containing all the report data
    //    
    public static DataSet AccountMovements(DateTime FromDate, DateTime ToDate, Int64? AccountId = null)
    {
      return AccountMovements(FromDate, ToDate, EnumAccountMovementsReport.Default, AccountId);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Generate the Caja Unica report from a two dates range
    // 
    //  PARAMS:
    //      - INPUT:
    //            - DateTime FromDate
    //            - DateTime ToDate
    //            - EnumAccountMovementsReport
    //            - AccountId
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      DataSet containing all the report data
    //    
    public static DataSet AccountMovements(DateTime FromDate, DateTime ToDate, EnumAccountMovementsReport Report, Int64? AccountId = null)
    {
      DataSet _dataset;
      DataTable _dt_cashier_movements;
      DataTable _dt_account_movements;
      DataTable _dt_total_records;
      DataRow _row_cashier_mov;
      DataRow _row_account_mov;
      StringBuilder _str_sql;
      Int64 _last_operation_id;
      Int32 _num_movs;
      Boolean _log_error_written;
      Int64 _account_id;
      Decimal _devolution;
      String _evidence_name;
      String _evidence_last_name1;
      String _evidence_last_name2;
      DateTime _evidence_birth_date;
      Int64 _am_operation_id;
      Int64 _cashier_session_id;
      Int64 _document_id;
      Int32 _amount_col;

      try
      {
        _dataset = new DataSet();
        _dt_cashier_movements = new DataTable();
        _dt_account_movements = new DataTable();
        _dt_total_records = new DataTable();

        _row_cashier_mov = null;
        _row_account_mov = null;

        _last_operation_id = -1;
        _log_error_written = false;
        _devolution = 0;

        using (DB_TRX _trx = new DB_TRX())
        {
          using (SqlCommand _sql_command = new SqlCommand())
          {
            _sql_command.Connection = _trx.SqlTransaction.Connection;
            _sql_command.Transaction = _trx.SqlTransaction;

            CreateCashierMovementsColumns(ref _dt_cashier_movements);

            CreateAccountMovementsColumns(ref _dt_account_movements);

            CreateTotalRecordsColumns(ref _dt_total_records);

            // Gather all required data
            //    - Number of movements (Cash In/Out + Prizes) that occurred after the initial data
            //    - List of movements (Cash In/Out + Prizes) that occurred after the initial data

            //    - Number of movements (Cash In/Out + Prizes) that occurred after the initial data
            //      With the only purpose to implement a future progression bar.

            ////////////_str_sql = new StringBuilder();
            ////////////_str_sql.AppendLine("SELECT   count(*)");   //col 0
            ////////////_str_sql.AppendLine("  FROM   CASHIER_MOVEMENTS with (index (IX_cm_date_type))");
            ////////////_str_sql.AppendLine("         INNER JOIN ACCOUNTS              ON CM_ACCOUNT_ID   = AC_ACCOUNT_ID  ");
            ////////////_str_sql.AppendLine("         LEFT  JOIN ACCOUNT_MAJOR_PRIZES  ON CM_OPERATION_ID = AMP_OPERATION_ID");
            ////////////_str_sql.AppendLine(" WHERE   CM_DATE >= @pFromDate AND CM_DATE < @pToDate ");
            ////////////_str_sql.AppendLine("   AND   CM_TYPE IN (@pTypeDevolutionSplit1, @pTypeCancelSplit1, @pTypeCashInSplit1, ");
            ////////////_str_sql.AppendLine("         @pTypeCashInMBSplit1, @pTypeCashInNASplit1, @pTypePrizes, @pTypeTaxOnPrize1, @pTypeTaxOnPrize2, ");
            ////////////_str_sql.AppendLine("         @pTypeTaxReturning, @pTypeServiceCharge, @pTypeDecimalRounding ) ");

            _sql_command.Parameters.Add("@pFromDate", SqlDbType.DateTime).Value = FromDate;
            _sql_command.Parameters.Add("@pToDate", SqlDbType.DateTime).Value = ToDate;
            _sql_command.Parameters.Add("@pTypeDevolutionSplit1", SqlDbType.Int).Value = CASHIER_MOVEMENT.DEV_SPLIT1;
            _sql_command.Parameters.Add("@pTypeCancelSplit1", SqlDbType.Int).Value = CASHIER_MOVEMENT.CANCEL_SPLIT1;
            _sql_command.Parameters.Add("@pTypeCashInSplit1", SqlDbType.Int).Value = CASHIER_MOVEMENT.CASH_IN_SPLIT1;
            _sql_command.Parameters.Add("@pTypeCashInMBSplit1", SqlDbType.Int).Value = CASHIER_MOVEMENT.MB_CASH_IN_SPLIT1;
            _sql_command.Parameters.Add("@pTypeCashInNASplit1", SqlDbType.Int).Value = CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1;
            _sql_command.Parameters.Add("@pTypeRechargeForPointsSplit1", SqlDbType.Int).Value = CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT1;
            _sql_command.Parameters.Add("@pTypePrizes", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZES;
            _sql_command.Parameters.Add("@pTypeTaxOnPrize1", SqlDbType.Int).Value = CASHIER_MOVEMENT.TAX_ON_PRIZE1;
            _sql_command.Parameters.Add("@pTypeTaxOnPrize2", SqlDbType.Int).Value = CASHIER_MOVEMENT.TAX_ON_PRIZE2;
            _sql_command.Parameters.Add("@pTypeTaxOnPrize3", SqlDbType.Int).Value = CASHIER_MOVEMENT.TAX_ON_PRIZE3;

            // RRB 18-SEP-2012: TaxReturning, ServiceCharge, DecimalRounding
            _sql_command.Parameters.Add("@pTypeTaxReturning", SqlDbType.Int).Value = CASHIER_MOVEMENT.TAX_RETURNING_ON_PRIZE_COUPON;
            _sql_command.Parameters.Add("@pPrizeDrawGrossOther", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_DRAW_GROSS_OTHER;

            _sql_command.Parameters.Add("@pTypeServiceCharge", SqlDbType.Int).Value = CASHIER_MOVEMENT.SERVICE_CHARGE;
            _sql_command.Parameters.Add("@pTypeServiceChargeA", SqlDbType.Int).Value = CASHIER_MOVEMENT.SERVICE_CHARGE_ON_COMPANY_A;    // EOR 21-SEP-2016
            _sql_command.Parameters.Add("@pTypeServiceChargeGamingTable", SqlDbType.Int).Value = CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE;
            _sql_command.Parameters.Add("@pTypeServiceChargeGamingTableA", SqlDbType.Int).Value = CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE_ON_COMPANY_A;
            _sql_command.Parameters.Add("@pTypeDecimalRounding", SqlDbType.Int).Value = CASHIER_MOVEMENT.DECIMAL_ROUNDING;

            // ANG 01-OCT-2013   PrizeInKind support
            _sql_command.Parameters.Add("@pTypePrizeInKind1Gross", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND1_GROSS;
            _sql_command.Parameters.Add("@pTypeTax1OnPrizeKind1", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX1;
            _sql_command.Parameters.Add("@pTypeTax2OnPrizeKind1", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX2;
            _sql_command.Parameters.Add("@pTypeTax3OnPrizeKind1", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX3;

            _sql_command.Parameters.Add("@pTypePrizeInKind2Gross", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND2_GROSS;
            _sql_command.Parameters.Add("@pTypeTax1OnPrizeKind2", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX1;
            _sql_command.Parameters.Add("@pTypeTax2OnPrizeKind2", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX2;
            _sql_command.Parameters.Add("@pTypeTax3OnPrizeKind2", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX3;

            //EOR 26-APR-2016
            _sql_command.Parameters.Add("@pTypeTaxCustody", SqlDbType.Int).Value = CASHIER_MOVEMENT.TAX_RETURNING_CUSTODY;

            // DDS 10-MAR-2016: Ooptionally filter by AccountId
            if (AccountId.HasValue)
            {
              _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId.Value;
            }

            // JML 22-DEC-2015   PrizeReInKind support
            _sql_command.Parameters.Add("@pTypePrizeReInKind1Gross", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_GROSS;
            _sql_command.Parameters.Add("@pTypeTax1OnPrizeReKind1", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX1;
            _sql_command.Parameters.Add("@pTypeTax2OnPrizeReKind1", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX2;
            _sql_command.Parameters.Add("@pTypeTax3OnPrizeReKind1", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX3;

            _sql_command.Parameters.Add("@pTypePrizeReInKind2Gross", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_GROSS;
            _sql_command.Parameters.Add("@pTypeTax1OnPrizeReKind2", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX1;
            _sql_command.Parameters.Add("@pTypeTax2OnPrizeReKind2", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX2;
            _sql_command.Parameters.Add("@pTypeTax3OnPrizeReKind2", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX3;

            _sql_command.Parameters.Add("@pTypePromoReFederalTax", SqlDbType.Int).Value = CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_FEDERAL_TAX;
            _sql_command.Parameters.Add("@pTypePromoReStateTax", SqlDbType.Int).Value = CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_STATE_TAX;
            _sql_command.Parameters.Add("@pTypePromoReCouncilTax", SqlDbType.Int).Value = CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_COUNCIL_TAX;

            //    - List of movements (Cash In/Out + Prizes) that occurred after the initial data
            _str_sql = new StringBuilder();

            _str_sql.AppendLine("SELECT    CM_OPERATION_ID ");                                                                                   //col 0
            _str_sql.AppendLine("        , CM_DATE ");                                                                                           //col 1
            _str_sql.AppendLine("        , CM_TYPE ");                                                                                           //col 2
            _str_sql.AppendLine("        , CM_ADD_AMOUNT ");                                                                                     //col 3
            _str_sql.AppendLine("        , CM_SUB_AMOUNT ");                                                                                     //col 4
            _str_sql.AppendLine("        , CM_ACCOUNT_ID                                                       -- Cuenta ");                     //col 5
            _str_sql.AppendLine("        , AC_HOLDER_NAME                                                      -- Nombre ");                     //col 6
            _str_sql.AppendLine("        , ISNULL(AC_HOLDER_ID1, AC_HOLDER_ID)                                 -- RFC ");                        //col 7
            _str_sql.AppendLine("        , AC_HOLDER_ID2                                                       -- CURP ");                       //col 8
            _str_sql.AppendLine("        , AC_HOLDER_ADDRESS_01 + ' ' + AC_HOLDER_ADDRESS_02 + ' ' + AC_HOLDER_ADDRESS_03 ");                    //col 9
            _str_sql.AppendLine("        , AC_HOLDER_CITY ");                                                                                    //col 10
            _str_sql.AppendLine("        , AC_HOLDER_ZIP ");                                                                                     //col 11
            _str_sql.AppendLine("        , AC_TRACK_DATA ");                                                                                     //col 12
            _str_sql.AppendLine("        , AMP_DOCUMENT_ID1                                                    -- DocumentID 37-A ISR1 ");       //col 13 DocumentID (PDF) generated by witholding
            _str_sql.AppendLine("        -- EVIDENCE --");
            _str_sql.AppendLine("        , CONVERT(bit, case when AMP_OPERATION_ID is null then 0 else 1 end)  -- EVIDENCE ");                   //col 14
            _str_sql.AppendLine("        , ISNULL(AMP_PLAYER_NAME, AC_HOLDER_NAME)                             -- Constancia.Nombre ");          //col 15
            _str_sql.AppendLine("        , ISNULL(AMP_PLAYER_ID1, ISNULL(AC_HOLDER_ID1, AC_HOLDER_ID))         -- Constancia.RFC ");             //col 16
            _str_sql.AppendLine("        , ISNULL(AMP_PLAYER_ID2, AC_HOLDER_ID2)                               -- Constancia.CURP ");            //col 17
            _str_sql.AppendLine("        , AMP_PLAYER_ID1                                                      -- Constancia.DocumentoID ");     //col 18
            _str_sql.AppendLine("        , NULL                                                                -- Constancia.NoDocumentoID ");   //col 19
            // DDM 09-AUG-2012: Added new fields.
            _str_sql.AppendLine("        , ISNULL(AMP_PLAYER_NAME1, '') ");                                                                      //col 20
            _str_sql.AppendLine("        , ISNULL(AMP_PLAYER_NAME2, '') ");                                                                      //col 21
            _str_sql.AppendLine("        , ISNULL(AMP_PLAYER_NAME3, '') ");                                                                      //col 22
            _str_sql.AppendLine("        , CM_SESSION_ID ");                                                                                     //col 23
            _str_sql.AppendLine("        , CM_CASHIER_NAME");                                                                                    //col 24

            // FGB 21-OCT-2016: Added Additional fields for withholding (constancia)
            _str_sql.AppendLine("        , AC_HOLDER_ADDRESS_01                                                -- Constancia.Calle ");           //col 25
            _str_sql.AppendLine("        , AC_HOLDER_ADDRESS_02                                                -- Constancia.Colonia ");         //col 26
            _str_sql.AppendLine("        , AC_HOLDER_ADDRESS_03                                                -- Constancia.Municipio ");       //col 27 Municipio/Delegacion
            _str_sql.AppendLine("        , AC_HOLDER_FED_ENTITY                                                -- Constancia.Estado ID ");       //col 28
            _str_sql.AppendLine("        , ISNULL(AC_HOLDER_STATE, AC_HOLDER_STATE_ALT)                        -- Constancia.Estado Desc ");     //col 29
            _str_sql.AppendLine("        , AC_HOLDER_ADDRESS_COUNTRY                                           -- Constancia.Pais   ID ");       //col 30
            _str_sql.AppendLine("        , ISNULL(AC_HOLDER_COUNTRY, AC_HOLDER_COUNTRY_ALT)                    -- Constancia.Pais   Desc ");     //col 31
            _str_sql.AppendLine("        , AMP_PLAYER_BIRTH_DATE                                               -- Constancia.FechaNacimiento "); //col 32
            _str_sql.AppendLine("        , AC_HOLDER_EXT_NUM                                                   -- Constancia.noExterior ");      //col 33
            _str_sql.AppendLine("        , NULL                                                                -- Constancia.noInterior ");      //col 34      //TODO:ZZZZ - (FGB): 2016-10-24: Check if correct
            _str_sql.AppendLine("        , NULL                                                                -- Constancia.Referencia ");      //col 35      //TODO:ZZZZ - (FGB): 2016-10-24: Check if correct
            _str_sql.AppendLine("        , NULL                                                                -- Constancia.localidad ");       //col 36      //TODO:ZZZZ - (FGB): 2016-10-24: Check if correct

            _str_sql.AppendLine("  FROM    CASHIER_MOVEMENTS with (index (IX_cm_date_type))");
            _str_sql.AppendLine("          INNER JOIN ACCOUNTS              ON CM_ACCOUNT_ID   = AC_ACCOUNT_ID      ");
            _str_sql.AppendLine("          LEFT  JOIN ACCOUNT_MAJOR_PRIZES  ON CM_OPERATION_ID = AMP_OPERATION_ID   ");
            _str_sql.AppendLine(" WHERE    CM_DATE >= @pFromDate ");
            _str_sql.AppendLine("   AND    CM_DATE < @pToDate ");

            // DDS 10-MAR-2016: Optionally filter by AccountId
            if (AccountId.HasValue)
            {
              _str_sql.AppendLine("   AND    AC_ACCOUNT_ID = @pAccountId ");
            }

            _str_sql.AppendLine("   AND   CM_TYPE IN ( @pTypeDevolutionSplit1, @pTypeCancelSplit1, @pTypePrizes, @pTypeTaxOnPrize1 ");
            _str_sql.AppendLine("                    , @pTypeTaxOnPrize2, @pTypeTaxOnPrize3, @pTypeTaxReturning, @pTypeServiceCharge, @pTypeDecimalRounding ");

            _str_sql.AppendLine("                    , @pTypeServiceChargeA, @pTypeServiceChargeGamingTable, @pTypeServiceChargeGamingTableA "); // EOR 21-SEP-2016

            // ANG 01-OCT-2013 PrizeInKind Movements
            _str_sql.AppendLine("         , @pTypePrizeInKind1Gross, @pTypeTax1OnPrizeKind1, @pTypeTax2OnPrizeKind1, @pTypeTax3OnPrizeKind1 ");
            _str_sql.AppendLine("         , @pTypePrizeInKind2Gross, @pTypeTax1OnPrizeKind2, @pTypeTax2OnPrizeKind2, @pTypeTax3OnPrizeKind2 ");

            // JML 22-DEC-2015 PrizeReInKind Movements
            _str_sql.AppendLine("         , @pTypePrizeReInKind1Gross, @pTypeTax1OnPrizeReKind1, @pTypeTax2OnPrizeReKind1, @pTypeTax3OnPrizeReKind1 ");
            _str_sql.AppendLine("         , @pTypePrizeReInKind2Gross, @pTypeTax1OnPrizeReKind2, @pTypeTax2OnPrizeReKind2, @pTypeTax3OnPrizeReKind2");
            _str_sql.AppendLine("         , @pTypePromoReFederalTax, @pTypePromoReStateTax, @pTypePromoReCouncilTax ");

            // EOR 26-APR-2016
            _str_sql.AppendLine("         ,@pTypeTaxCustody");
            _str_sql.AppendLine("         ,@pPrizeDrawGrossOther");

            // Payment types.
            //_str_sql.AppendLine("         , @pTypeBankCardPayment, @pTypeCheckPayment, @pTypeCheckPaymentRefund) ");

            // Exclude from 'Retreats Report' this movements.
            if (Report != EnumAccountMovementsReport.RetreatsReport)
            {
              _str_sql.AppendLine("         , @pTypeCashInSplit1, @pTypeCashInMBSplit1, @pTypeCashInNASplit1, @pTypeRechargeForPointsSplit1 ");
            }

            _str_sql.AppendLine(")");

            _str_sql.AppendLine(" ORDER BY CM_OPERATION_ID ");

            _sql_command.CommandText = _str_sql.ToString();

            _num_movs = 0;

            using (SqlDataReader _sql_reader = _sql_command.ExecuteReader())
            {
              // Browse the gathered account movements to produce the required data operation by operation
              while (_sql_reader.Read())
              {
                _num_movs++;

                // Group all the movements related to the current operation
                _am_operation_id = (_sql_reader.IsDBNull(COL_SELECT_MOVS.OPERATION_ID) ? 0 : _sql_reader.GetInt64(COL_SELECT_MOVS.OPERATION_ID));
                _cashier_session_id = (_sql_reader.IsDBNull(COL_SELECT_MOVS.CASHIER_SESSION) ? 0 : _sql_reader.GetInt64(COL_SELECT_MOVS.CASHIER_SESSION));
                _document_id = (_sql_reader.IsDBNull(COL_SELECT_MOVS.DOCUMENT_ID) ? 0 : _sql_reader.GetInt64(COL_SELECT_MOVS.DOCUMENT_ID));

                if (_last_operation_id != _am_operation_id)
                {
                  // Save the devolution value before get new operation data
                  if (_devolution != 0)
                  {
                    _row_cashier_mov[COL_CASHIER_MOVS.DEVOLUTION_AMOUNT] = _devolution;
                  }

                  // New operation

                  _account_id = _sql_reader.GetInt64(COL_SELECT_MOVS.ACCOUNT_ID);

                  // Was this operation already handled by this loop? 
                  // If so it should be present in _dt_movement_accounts (output datatable)
                  _row_account_mov = _dt_account_movements.Rows.Find(_account_id);

                  if (_row_account_mov == null)
                  {
                    // Operation was not handled yet              

                    //  - Add the account data and set the account totals to 0
                    _row_account_mov = _dt_account_movements.NewRow();

                    FillAccountRowWithSqlReaderData(_sql_reader, _row_account_mov, _account_id);

                    CheckEmptyFields(_row_account_mov);

                    _dt_account_movements.Rows.Add(_row_account_mov);
                  }

                  // DDM 09-AUG-2012 Now full name is composed from  AMP_PLAYER_NAME1, AMP_PLAYER_NAME2 and AMP_PLAYER_NAME3. 
                  GetHolderData(_sql_reader, out _evidence_name, out _evidence_last_name1, out _evidence_last_name2, out _evidence_birth_date);

                  _row_cashier_mov = _dt_cashier_movements.NewRow();

                  _row_cashier_mov[COL_CASHIER_MOVS.OPERATION_ID] = _am_operation_id;
                  _row_cashier_mov[COL_CASHIER_MOVS.DOCUMENT_ID] = _document_id;

                  //Fill fields
                  SetDefaultFields(_sql_reader, _row_cashier_mov, _account_id, _evidence_name, _evidence_last_name1, _evidence_last_name2, _cashier_session_id);

                  _devolution = 0;

                  CheckEmptyFields(_row_cashier_mov);

                  _dt_cashier_movements.Rows.Add(_row_cashier_mov);

                  _last_operation_id = _am_operation_id;
                } // if

                // Accumulate the account movement data into the operation 
                //    - Operation's movements
                //    - Account totals

                switch ((CASHIER_MOVEMENT)_sql_reader[COL_SELECT_MOVS.TYPE])
                {
                  case CASHIER_MOVEMENT.CASH_IN_SPLIT1:
                  case CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1:
                  case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT1:
                  case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT1:

                    if (_row_cashier_mov[COL_CASHIER_MOVS.CASHIN_AMOUNT] == DBNull.Value)
                    {
                      _row_cashier_mov[COL_CASHIER_MOVS.CASHIN_AMOUNT] = (Decimal)_sql_reader[COL_SELECT_MOVS.ADD_AMOUNT];
                    }
                    else
                    {
                      _row_cashier_mov[COL_CASHIER_MOVS.CASHIN_AMOUNT] = (Decimal)_sql_reader[COL_SELECT_MOVS.ADD_AMOUNT]
                                                                      + (Decimal)_row_cashier_mov[COL_CASHIER_MOVS.CASHIN_AMOUNT];
                    }

                    _row_account_mov[COL_ACCS_TOTAL_CASHIN] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_CASHIN]
                                                            + (Decimal)_sql_reader[COL_SELECT_MOVS.ADD_AMOUNT];

                    break;

                  case CASHIER_MOVEMENT.PRIZES:
                  case CASHIER_MOVEMENT.PRIZE_DRAW_GROSS_OTHER:
                    _row_cashier_mov[COL_CASHIER_MOVS.PRIZE_AMOUNT] = _sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    _row_account_mov[COL_ACCS_TOTAL_PRIZE] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_PRIZE]
                                                           + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    break;

                  case CASHIER_MOVEMENT.DEV_SPLIT1:
                    _devolution = _devolution + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    _row_account_mov[COL_ACCS_TOTAL_DEVOLUTION] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_DEVOLUTION]
                                                                + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    break;

                  case CASHIER_MOVEMENT.CANCEL_SPLIT1:
                    _devolution = _devolution + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    _row_account_mov[COL_ACCS_TOTAL_DEVOLUTION] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_DEVOLUTION]
                                                                + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    break;

                  case CASHIER_MOVEMENT.TAX_ON_PRIZE1:                      // FEDERAL TAX
                  case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX1:                // Premio Sorteo de Caja en Especie   - Impuesto Federal (UNR)
                  case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX1:                // Premio Sorteo de Caja en Promoci�n - Impuesto Federal (UNR)
                  case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX1:             // Premio Sorteo de Caja en Especie   - Impuesto Federal (RE in Kind)
                  case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX1:             // Premio Sorteo de Caja en Promoci�n - Impuesto Federal (RE in Kind)
                  case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_FEDERAL_TAX:
                    {
                      if ((CASHIER_MOVEMENT)_sql_reader[COL_SELECT_MOVS.TYPE] == CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX1
                        || (CASHIER_MOVEMENT)_sql_reader[COL_SELECT_MOVS.TYPE] == CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX1)
                      {
                        _amount_col = COL_SELECT_MOVS.SUB_AMOUNT;
                      }
                      else
                      {
                        _amount_col = COL_SELECT_MOVS.ADD_AMOUNT;
                      }

                      if (_row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE1_AMOUNT] == DBNull.Value)
                      {
                        _row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE1_AMOUNT] = (Decimal)_sql_reader[_amount_col];
                      }
                      else
                      {
                        _row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE1_AMOUNT] = (Decimal)_row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE1_AMOUNT]
                                                                      + (Decimal)_sql_reader[_amount_col];
                      }

                      _row_account_mov[COL_ACCS_TOTAL_ISR1] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_ISR1]
                                                        + (Decimal)_sql_reader[_amount_col];

                      break;
                    }
                  case CASHIER_MOVEMENT.TAX_ON_PRIZE2:                      // STATE TAXES
                  case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX2:                // Premio Sorteo de Caja en Especie   - Impuesto Estatal (UNR)
                  case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX2:                // Premio Sorteo de Caja en Promoci�n - Impuesto Estatal (UNR)
                  case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX2:             // Premio Sorteo de Caja en Especie   - Impuesto Estatal (RE in Kind)
                  case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX2:             // Premio Sorteo de Caja en Promoci�n - Impuesto Estatal (RE in KInd)
                  case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_STATE_TAX:
                    {
                      if ((CASHIER_MOVEMENT)_sql_reader[COL_SELECT_MOVS.TYPE] == CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX2
                        || (CASHIER_MOVEMENT)_sql_reader[COL_SELECT_MOVS.TYPE] == CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX2)
                      {
                        _amount_col = COL_SELECT_MOVS.SUB_AMOUNT;
                      }
                      else
                      {
                        _amount_col = COL_SELECT_MOVS.ADD_AMOUNT;
                      }
                      if (_row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE2_AMOUNT] == DBNull.Value)
                      {
                        _row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE2_AMOUNT] = (Decimal)_sql_reader[_amount_col];
                      }
                      else
                      {
                        _row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE2_AMOUNT] = (Decimal)_row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE2_AMOUNT]
                                                                      + (Decimal)_sql_reader[_amount_col];
                      }

                      _row_account_mov[COL_ACCS_TOTAL_ISR2] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_ISR2]
                                                        + (Decimal)_sql_reader[_amount_col];

                      break;
                    }

                  case CASHIER_MOVEMENT.TAX_ON_PRIZE3:                      // STATE TAXES
                  case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX3:                // Premio Sorteo de Caja en Especie   - Impuesto Estatal (UNR)
                  case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX3:                // Premio Sorteo de Caja en Promoci�n - Impuesto Estatal (UNR)
                  case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX3:             // Premio Sorteo de Caja en Especie   - Impuesto Estatal (RE in Kind)
                  case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX3:             // Premio Sorteo de Caja en Promoci�n - Impuesto Estatal (RE in KInd)
                  case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_COUNCIL_TAX:
                    {
                      if ((CASHIER_MOVEMENT)_sql_reader[COL_SELECT_MOVS.TYPE] == CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX3
                        || (CASHIER_MOVEMENT)_sql_reader[COL_SELECT_MOVS.TYPE] == CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX3)
                      {
                        _amount_col = COL_SELECT_MOVS.SUB_AMOUNT;
                      }
                      else
                      {
                        _amount_col = COL_SELECT_MOVS.ADD_AMOUNT;
                      }
                      if (_row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE3_AMOUNT] == DBNull.Value)
                      {
                        _row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE3_AMOUNT] = (Decimal)_sql_reader[_amount_col];
                      }
                      else
                      {
                        _row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE3_AMOUNT] = (Decimal)_row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE3_AMOUNT]
                                                                      + (Decimal)_sql_reader[_amount_col];
                      }

                      _row_account_mov[COL_ACCS_TOTAL_ISR3] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_ISR3]
                                                        + (Decimal)_sql_reader[_amount_col];

                      break;
                    }
                  // RRB 18-SEP-2012: TAX RETURNING, SERVICE CHARGE and DECIMAL ROUNDING
                  case CASHIER_MOVEMENT.TAX_RETURNING_ON_PRIZE_COUPON:
                    _row_cashier_mov[COL_CASHIER_MOVS.TAX_RETURNING] = _sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    _row_account_mov[COL_ACCS_TOTAL_TAX_RETURNING] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_TAX_RETURNING]
                                                                   + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    break;

                  case CASHIER_MOVEMENT.SERVICE_CHARGE:
                  case CASHIER_MOVEMENT.SERVICE_CHARGE_ON_COMPANY_A:  //EOR 19-SEP-2016
                  case CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE:
                  case CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE_ON_COMPANY_A:
                    _row_cashier_mov[COL_CASHIER_MOVS.SERVICE_CHARGE] = _sql_reader[COL_SELECT_MOVS.ADD_AMOUNT];
                    _row_account_mov[COL_ACCS_TOTAL_SERVICE_CHARGE] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_SERVICE_CHARGE]
                                                                    + (Decimal)_sql_reader[COL_SELECT_MOVS.ADD_AMOUNT];
                    break;

                  case CASHIER_MOVEMENT.DECIMAL_ROUNDING:
                    _row_cashier_mov[COL_CASHIER_MOVS.DECIMAL_ROUNDING] = _sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    _row_account_mov[COL_ACCS_TOTAL_DECIMAL_ROUNDING] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_DECIMAL_ROUNDING]
                                                                      + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    break;

                  //EOR 26-APR-2016
                  case CASHIER_MOVEMENT.TAX_RETURNING_CUSTODY:
                    _row_cashier_mov[COL_CASHIER_MOVS.TAX_CUSTODY] = _sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    _row_account_mov[COL_ACCS_TOTAL_CUSTODY] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_CUSTODY]
                                                             + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    break;

                  case CASHIER_MOVEMENT.PRIZE_IN_KIND1_GROSS:       // Premio Sorteo de Caja en Especie
                  case CASHIER_MOVEMENT.PRIZE_IN_KIND2_GROSS:       // Premio Sorteo de Caja en Promoci�n
                    if (_row_cashier_mov[COL_CASHIER_MOVS.PRIZE_IN_KIND_AMOUNT] == DBNull.Value)
                    {
                      _row_cashier_mov[COL_CASHIER_MOVS.PRIZE_IN_KIND_AMOUNT] = (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    }
                    else
                    {
                      _row_cashier_mov[COL_CASHIER_MOVS.PRIZE_IN_KIND_AMOUNT] = (Decimal)_row_cashier_mov[COL_CASHIER_MOVS.PRIZE_IN_KIND_AMOUNT]
                                                                              + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    }

                    _row_account_mov[COL_ACCS_TOTAL_PRIZE_IN_KIND] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_PRIZE_IN_KIND]
                                                                   + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];

                    break;

                  case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_GROSS:    // Premio Sorteo de Caja en Especie   (RE in Kind)
                  case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_GROSS:    // Premio Sorteo de Caja en Promoci�n (RE in Kind)
                    if (_row_cashier_mov[COL_CASHIER_MOVS.PRIZE_IN_KIND_AMOUNT] == DBNull.Value)
                    {
                      _row_cashier_mov[COL_CASHIER_MOVS.PRIZE_RE_IN_KIND_AMOUNT] = (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    }
                    else
                    {
                      _row_cashier_mov[COL_CASHIER_MOVS.PRIZE_RE_IN_KIND_AMOUNT] = (Decimal)_row_cashier_mov[COL_CASHIER_MOVS.PRIZE_RE_IN_KIND_AMOUNT]
                                                                                 + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    }

                    _row_account_mov[COL_ACCS_TOTAL_PRIZE_RE_IN_KIND] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_PRIZE_RE_IN_KIND]
                                                                      + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];

                    break;

                  default:
                    if (!_log_error_written)
                    {
                      Log.Error("XMLReport.AccountMovements. Unexpected movement type.  Operation_id: " + _sql_reader.GetInt64(COL_SELECT_MOVS.OPERATION_ID)
                              + ". Type:" + _sql_reader[COL_SELECT_MOVS.TYPE]);
                      _log_error_written = true;
                    }

                    break;
                } // switch
              } // while

              _sql_reader.Close();

              // Save the devolution value before end the process
              if (_devolution != 0)
              {
                _row_cashier_mov[COL_CASHIER_MOVS.DEVOLUTION_AMOUNT] = _devolution;
              }

            } // using SqlReader

            // Add the number of movements (Cash In/Out + Prizes) that occurred after the initial data
            _dt_total_records.Rows.Add(_num_movs);

            // Publish the produced data
            _dataset.Tables.Add(_dt_cashier_movements);
            _dataset.Tables.Add(_dt_account_movements);
            _dataset.Tables.Add(_dt_total_records);

            // Name the dataset (XML purpouses)
            _dataset.DataSetName = DATASET_NAME;

            // Commit the changes
            _dataset.AcceptChanges();

            if (Report == EnumAccountMovementsReport.CashierSessionsTable)
            {
              return (AddCashierSessionsTable(_dataset) ? _dataset : null);
            }

            return _dataset;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return null;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Generate report from a two dates range for WigosGUI Accounting->Export AccountMovements ( SAT )
    // 
    //  PARAMS:
    //      - INPUT:
    //            - DateTime FromDate
    //            - DateTime ToDate
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      DataSet containing all the report data
    // 
    public static DataSet AccountMovementsPSA(DateTime FromDate, DateTime ToDate)
    {
      Decimal _initial_balance_discarded;

      return AccountMovementsPSA(FromDate, ToDate, false, WGDB.Connection(), out _initial_balance_discarded);
    }

    /// <summary>
    /// Returns the table name of Cashier movements
    /// </summary>
    /// <returns></returns>
    public static String GetTableNameCashierMovements()
    {
      return TABLE_NAME_CASHIER_MOVEMENTS;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Generate report from a two dates range for PSA Client
    // 
    //  PARAMS:
    //      - INPUT:
    //            - DateTime FromDate
    //            - DateTime ToDate
    //            - AddOperationsExternalsOfGamingTables 
    //      - OUTPUT:
    //            - InitialBalance
    //
    // RETURNS: 
    //      DataSet containing all the report data
    //    
    public static DataSet AccountMovementsPSA(DateTime FromDate, DateTime ToDate, out Decimal InitialBalance)
    {
      DataSet _account_movements_PSA;
      DataSet _external_accountMovementsPSA;
      String _connection_string;

      InitialBalance = 0;

      _account_movements_PSA = null;

      try
      {
          _account_movements_PSA = AccountMovementsPSA(FromDate, ToDate, false, WGDB.Connection(), out InitialBalance);

        if (!GeneralParam.GetBoolean("PSAClient", "ExternalServer.Enabled", false))
        {
          return _account_movements_PSA;
        }

        // DDM 12-APR-2016: Report gaming tables
        _connection_string = GeneralParam.GetString("PSAClient", "ExternalServer.ConnectionString", "");
        
        if (String.IsNullOrEmpty(_connection_string))
        {
          Log.Error("PSACLient - ExternalTablesServerDB.ConnectionString not defined. \r\n");
          _account_movements_PSA = null;

          return _account_movements_PSA;
        }

        using (SqlConnection _sql_connection = new SqlConnection(_connection_string))
        {
          _sql_connection.Open();
          _external_accountMovementsPSA = AccountMovementsPSA(FromDate, ToDate, true, _sql_connection, out InitialBalance);
          _account_movements_PSA.Merge(_external_accountMovementsPSA);
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        _account_movements_PSA = null;
      }

      return _account_movements_PSA;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Generate report from a two dates range for PSA Client
    // 
    //  PARAMS:
    //      - INPUT:
    //            - DateTime FromDate
    //            - DateTime ToDate
    //            - AddOperationsExternalsOfGamingTables 
    //            - SqlConnection
    //      - OUTPUT:
    //            - InitialBalance
    // RETURNS: 
    //      DataSet containing all the report data
    //    
    public static DataSet AccountMovementsPSA(DateTime FromDate, DateTime ToDate, Boolean AddOperationsExternalsOfGamingTables, SqlConnection SqlConnection, out Decimal InitialBalance)
    {
      DataSet _dataset;
      DataTable _dt_cashier_movements;
      DataTable _dt_account_movements;
      DataTable _dt_total_records;
      DataRow _row_cashier_mov;
      DataRow _row_account_mov;
      StringBuilder _str_sql;
      Int64 _last_operation_id;
      Int32 _num_movs;
      Boolean _log_error_written;
      Int64 _account_id;
      Decimal _devolution;
      String _evidence_name;
      String _evidence_last_name1;
      String _evidence_last_name2;
      DateTime _evidence_birth_date;
      Int64 _cm_operation_id;
      Int64 _cashier_session_id;
      Int64 _document_id;
      DataTable _cashier_voucher;

      String _clave_tipo_pago_default_value;
      String _clave_tipo_pago_val;

      String _forma_pago_default_value;
      String _forma_pago_val;
      Boolean _is_reporting_redeemable;

      Boolean _operation_has_unr_credit;
      DataRow _unr_row;
      Boolean _skip_undone_operations;
      Int32 _amount_column;

      String _holder_state_desc;
      String _holder_country_desc;

      Boolean _add_operations_of_gaming_tables;

      InitialBalance = 0;

      try
      {
        _dataset = new DataSet();
        _dt_cashier_movements = new DataTable();
        _dt_account_movements = new DataTable();
        _dt_total_records = new DataTable();

        _row_cashier_mov = null;
        _row_account_mov = null;
        _last_operation_id = Int64.MinValue;
        _log_error_written = false;
        _devolution = 0;
        _operation_has_unr_credit = false;

        _clave_tipo_pago_default_value = String.Empty;
        _forma_pago_default_value = String.Empty;

        _is_reporting_redeemable = GeneralParam.GetBoolean("PSAClient", "ReportRedeemablePromotions", true);
        _skip_undone_operations = GeneralParam.GetBoolean("PSAClient", "SkipUndoneOperations", false);
        _add_operations_of_gaming_tables = GeneralParam.GetBoolean("PSAClient", "AddOperationsOfGamingTables", false);

        using (SqlTransaction _trx = SqlConnection.BeginTransaction())
        {
          using (SqlCommand _sql_command = new SqlCommand())
          {
            _sql_command.Connection = _trx.Connection;
            _sql_command.Transaction = _trx;

            if (!GetVoucherFromCashierMovement(FromDate, ToDate, out _cashier_voucher, _trx))
            {
              return null;
            }

            //Create the datatables
            CreateCashierMovementsColumns(ref _dt_cashier_movements, true);

            CreateAccountMovementsColumns(ref _dt_account_movements);

            CreateTotalRecordsColumns(ref _dt_total_records);

            // Gather all required data
            //    - Number of movements (Cash In/Out + Prizes) that occurred after the initial data
            //    - List of movements (Cash In/Out + Prizes) that occurred after the initial data

            //    - Number of movements (Cash In/Out + Prizes) that occurred after the initial data
            //      With the only purpose to implement a future progression bar.

            ////////////_str_sql.AppendLine("SELECT   count(*)");   //col 0
            ////////////_str_sql.AppendLine("  FROM   CASHIER_MOVEMENTS with (index (IX_cm_date_type))");
            ////////////_str_sql.AppendLine("         INNER JOIN ACCOUNTS              ON CM_ACCOUNT_ID   = AC_ACCOUNT_ID  ");
            ////////////_str_sql.AppendLine("         LEFT  JOIN ACCOUNT_MAJOR_PRIZES  ON CM_OPERATION_ID = AMP_OPERATION_ID");
            ////////////_str_sql.AppendLine(" WHERE   CM_DATE >= @pFromDate AND CM_DATE < @pToDate ");
            ////////////_str_sql.AppendLine("   AND   CM_TYPE IN (@pTypeDevolutionSplit1, @pTypeCancelSplit1, @pTypeCashInSplit1, ");
            ////////////_str_sql.AppendLine("         @pTypeCashInMBSplit1, @pTypeCashInNASplit1, @pTypePrizes, @pTypeTaxOnPrize1, @pTypeTaxOnPrize2, ");
            ////////////_str_sql.AppendLine("         @pTypeTaxReturning, @pTypeServiceCharge, @pTypeDecimalRounding ) ");

            _sql_command.Parameters.Add("@pFromDate", SqlDbType.DateTime).Value = FromDate;
            _sql_command.Parameters.Add("@pToDate", SqlDbType.DateTime).Value = ToDate;
            _sql_command.Parameters.Add("@pTypeDevolutionSplit1", SqlDbType.Int).Value = CASHIER_MOVEMENT.DEV_SPLIT1;
            _sql_command.Parameters.Add("@pTypeCancelSplit1", SqlDbType.Int).Value = CASHIER_MOVEMENT.CANCEL_SPLIT1;

            if (GeneralParam.GetBoolean("PSAClient", "Salidas.IncluirCancelacionesEmpresaB"))
            {
              _sql_command.Parameters.Add("@pTypeCancelSplit2", SqlDbType.Int).Value = CASHIER_MOVEMENT.CANCEL_SPLIT2;
            }

            _sql_command.Parameters.Add("@pTypeCashInSplit1", SqlDbType.Int).Value = CASHIER_MOVEMENT.CASH_IN_SPLIT1;
            _sql_command.Parameters.Add("@pTypeCashInMBSplit1", SqlDbType.Int).Value = CASHIER_MOVEMENT.MB_CASH_IN_SPLIT1;
            _sql_command.Parameters.Add("@pTypeCashInNASplit1", SqlDbType.Int).Value = CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1;
            _sql_command.Parameters.Add("@pTypeRechargeForPointsSplit1", SqlDbType.Int).Value = CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT1;
            _sql_command.Parameters.Add("@pTypePrizes", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZES;
            _sql_command.Parameters.Add("@pTypeTaxOnPrize1", SqlDbType.Int).Value = CASHIER_MOVEMENT.TAX_ON_PRIZE1;
            _sql_command.Parameters.Add("@pTypeTaxOnPrize2", SqlDbType.Int).Value = CASHIER_MOVEMENT.TAX_ON_PRIZE2;
            _sql_command.Parameters.Add("@pTypeTaxOnPrize3", SqlDbType.Int).Value = CASHIER_MOVEMENT.TAX_ON_PRIZE3;

            _sql_command.Parameters.Add("@pTypePrizeInKind1Gross", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND1_GROSS;
            _sql_command.Parameters.Add("@pTypeTax1OnPrizeKind1", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX1;
            _sql_command.Parameters.Add("@pTypeTax2OnPrizeKind1", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX2;
            _sql_command.Parameters.Add("@pTypeTax3OnPrizeKind1", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX3;

            _sql_command.Parameters.Add("@pTypePrizeInKind2Gross", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND2_GROSS;
            _sql_command.Parameters.Add("@pTypeTax1OnPrizeKind2", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX1;
            _sql_command.Parameters.Add("@pTypeTax2OnPrizeKind2", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX2;
            _sql_command.Parameters.Add("@pTypeTax3OnPrizeKind2", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX3;

            _sql_command.Parameters.Add("@pTypePrizeReInKind1Gross", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_GROSS;
            _sql_command.Parameters.Add("@pTypeTax1OnPrizeReKind1", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX1;
            _sql_command.Parameters.Add("@pTypeTax2OnPrizeReKind1", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX2;
            _sql_command.Parameters.Add("@pTypeTax3OnPrizeReKind1", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX3;

            _sql_command.Parameters.Add("@pTypePrizeReInKind2Gross", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_GROSS;
            _sql_command.Parameters.Add("@pTypeTax1OnPrizeReKind2", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX1;
            _sql_command.Parameters.Add("@pTypeTax2OnPrizeReKind2", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX2;
            _sql_command.Parameters.Add("@pTypeTax3OnPrizeReKind2", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX3;

            _sql_command.Parameters.Add("@pTypePromoReFederalTax", SqlDbType.Int).Value = CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_FEDERAL_TAX;
            _sql_command.Parameters.Add("@pTypePromoReStateTax", SqlDbType.Int).Value = CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_STATE_TAX;
            _sql_command.Parameters.Add("@pTypePromoReCouncilTax", SqlDbType.Int).Value = CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_COUNCIL_TAX;

            // Input Payments types : 
            _sql_command.Parameters.Add("@pTypeBankCardPayment", SqlDbType.Int).Value = CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1;
            _sql_command.Parameters.Add("@pTypeCheckPayment", SqlDbType.Int).Value = CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1;

            // Output Payments types :
            _sql_command.Parameters.Add("@pTypeCheckPaymentRefund", SqlDbType.Int).Value = CASHIER_MOVEMENT.CHECK_PAYMENT;

            // RRB 18-SEP-2012: TaxReturning, ServiceCharge, DecimalRounding

            // AJQ 05-JUN-2014, Include TaxReturning
            _sql_command.Parameters.Add("@pTypeTaxReturning", SqlDbType.Int).Value = CASHIER_MOVEMENT.TAX_RETURNING_ON_PRIZE_COUPON;

            //_sql_command.Parameters.Add("@pTypeServiceCharge", SqlDbType.Int).Value = CASHIER_MOVEMENT.SERVICE_CHARGE;

            // DDM 28-MAY-2014: Add decimal rounding 
            _sql_command.Parameters.Add("@pTypeDecimalRounding", SqlDbType.Int).Value = CASHIER_MOVEMENT.DECIMAL_ROUNDING;

            //DHA 29-05-2014: 
            _sql_command.Parameters.Add("@pTypeRechargeForPointsPrize", SqlDbType.Int).Value = CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_PRIZE;

            // ANG : Parameters by Compute Initial Balance
            _sql_command.Parameters.Add("@pTypeOpenSession", SqlDbType.Int).Value = CASHIER_MOVEMENT.OPEN_SESSION;
            _sql_command.Parameters.Add("@pTypeFillerIn", SqlDbType.Int).Value = CASHIER_MOVEMENT.FILLER_IN;

            _sql_command.Parameters.Add("@pTypeRedemPrizeExpired", SqlDbType.Int).Value = CASHIER_MOVEMENT.REDEEMABLE_PRIZE_EXPIRED;
            //DDM 05-12-2012: 
            _sql_command.Parameters.Add("@pTypePromotionRedeemable", SqlDbType.Int).Value = CASHIER_MOVEMENT.PROMOTION_REDEEMABLE;

            //_sql_command.Parameters.Add("@pTypePrizeExpired",SqlDbType.Int).Value = ACCOUNT_MOVE

            _sql_command.Parameters.Add("@pOperationUndoNone", SqlDbType.Int).Value = UndoStatus.None;

            // JML 19-JAN-2015 Add parameters for filter tables operations
            _sql_command.Parameters.Add("@pOCodeChipsSale", SqlDbType.Int).Value = OperationCode.CHIPS_SALE;
            _sql_command.Parameters.Add("@pOCodeChipsSaleWithRecharge", SqlDbType.Int).Value = OperationCode.CHIPS_SALE_WITH_RECHARGE;
            _sql_command.Parameters.Add("@pOCodeChipsPurchase", SqlDbType.Int).Value = OperationCode.CHIPS_PURCHASE;
            _sql_command.Parameters.Add("@pOCodeChipsPurchaseWithCashOut", SqlDbType.Int).Value = OperationCode.CHIPS_PURCHASE_WITH_CASH_OUT;

            _sql_command.Parameters.Add("@pPrizeDrawGrossOther", SqlDbType.Int).Value = CASHIER_MOVEMENT.PRIZE_DRAW_GROSS_OTHER;

            //    - List of movements (Cash In/Out + Prizes) that occurred after the initial data
            _str_sql = new StringBuilder();

            _str_sql.AppendLine("SELECT    CM_OPERATION_ID ");                                                                                   //col 0
            _str_sql.AppendLine("        , CM_DATE ");                                                                                           //col 1
            _str_sql.AppendLine("        , CM_TYPE ");                                                                                           //col 2
            _str_sql.AppendLine("        , CM_ADD_AMOUNT ");                                                                                     //col 3
            _str_sql.AppendLine("        , CM_SUB_AMOUNT ");                                                                                     //col 4
            _str_sql.AppendLine("        , CM_ACCOUNT_ID                                                       -- Cuenta ");                     //col 5
            _str_sql.AppendLine("        , AC_HOLDER_NAME                                                      -- Nombre ");                     //col 6
            _str_sql.AppendLine("        , ISNULL(AC_HOLDER_ID1, AC_HOLDER_ID)                                 -- RFC ");                        //col 7
            _str_sql.AppendLine("        , AC_HOLDER_ID2                                                       -- CURP ");                       //col 8
            _str_sql.AppendLine("        , AC_HOLDER_ADDRESS_01 + ' ' + AC_HOLDER_ADDRESS_02 + ' ' + AC_HOLDER_ADDRESS_03 ");                    //col 9
            _str_sql.AppendLine("        , AC_HOLDER_CITY ");                                                                                    //col 10
            _str_sql.AppendLine("        , AC_HOLDER_ZIP ");                                                                                     //col 11
            _str_sql.AppendLine("        , AC_TRACK_DATA ");                                                                                     //col 12
            _str_sql.AppendLine("        , AMP_DOCUMENT_ID1                                                    -- DocumentID 37-A ISR1 ");       //col 13 DocumentID (PDF) generated by witholding
            _str_sql.AppendLine("        -- EVIDENCE --");
            _str_sql.AppendLine("        , CONVERT(bit, case when AMP_OPERATION_ID is null then 0 else 1 end)  -- EVIDENCE ");                   //col 14
            _str_sql.AppendLine("        , ISNULL(AMP_PLAYER_NAME, AC_HOLDER_NAME)                             -- Constancia.Nombre ");          //col 15
            _str_sql.AppendLine("        , ISNULL(AMP_PLAYER_ID1, ISNULL(AC_HOLDER_ID1, AC_HOLDER_ID))         -- Constancia.RFC ");             //col 16
            _str_sql.AppendLine("        , ISNULL(AMP_PLAYER_ID2, AC_HOLDER_ID2)                               -- Constancia.CURP ");            //col 17
            _str_sql.AppendLine("        , AMP_PLAYER_ID1                                                      -- Constancia.DocumentoID ");     //col 18
            _str_sql.AppendLine("        , NULL                                                                -- Constancia.NoDocumentoID ");   //col 19
            // DDM 09-AUG-2012: Added new fields.
            _str_sql.AppendLine("        , ISNULL(AMP_PLAYER_NAME1, '') ");                                                                      //col 20
            _str_sql.AppendLine("        , ISNULL(AMP_PLAYER_NAME2, '') ");                                                                      //col 21
            _str_sql.AppendLine("        , ISNULL(AMP_PLAYER_NAME3, '') ");                                                                      //col 22
            _str_sql.AppendLine("        , CM_SESSION_ID ");                                                                                     //col 23
            _str_sql.AppendLine("        , CM_CASHIER_NAME");                                                                                    //col 24

            // FGB 21-OCT-2016: Added Additional fields for withholding (constancia)
            _str_sql.AppendLine("        , AC_HOLDER_ADDRESS_01                                                -- Constancia.Calle ");           //col 25
            _str_sql.AppendLine("        , AC_HOLDER_ADDRESS_02                                                -- Constancia.Colonia ");         //col 26
            _str_sql.AppendLine("        , AC_HOLDER_ADDRESS_03                                                -- Constancia.Municipio ");       //col 27 Municipio/Delegacion
            _str_sql.AppendLine("        , AC_HOLDER_FED_ENTITY                                                -- Constancia.Estado ID ");       //col 28
            _str_sql.AppendLine("        , ISNULL(AC_HOLDER_STATE, AC_HOLDER_STATE_ALT)                        -- Constancia.Estado Desc ");     //col 29
            _str_sql.AppendLine("        , AC_HOLDER_ADDRESS_COUNTRY                                           -- Constancia.Pais   ID ");       //col 30
            _str_sql.AppendLine("        , ISNULL(AC_HOLDER_COUNTRY, AC_HOLDER_COUNTRY_ALT)                    -- Constancia.Pais   Desc ");     //col 31
            _str_sql.AppendLine("        , AMP_PLAYER_BIRTH_DATE                                               -- Constancia.FechaNacimiento "); //col 32
            _str_sql.AppendLine("        , AC_HOLDER_EXT_NUM                                                   -- Constancia.noExterior ");      //col 33
            _str_sql.AppendLine("        , NULL                                                                -- Constancia.noInterior ");      //col 34      //TODO:ZZZZ - (FGB): 2016-10-24: Check if correct
            _str_sql.AppendLine("        , NULL                                                                -- Constancia.Referencia ");      //col 35      //TODO:ZZZZ - (FGB): 2016-10-24: Check if correct
            _str_sql.AppendLine("        , NULL                                                                -- Constancia.localidad ");       //col 36      //TODO:ZZZZ - (FGB): 2016-10-24: Check if correct5

            _str_sql.AppendLine("  FROM    CASHIER_MOVEMENTS with (index (IX_cm_date_type))");
            _str_sql.AppendLine("          INNER JOIN ACCOUNTS              ON CM_ACCOUNT_ID   = AC_ACCOUNT_ID  ");
            _str_sql.AppendLine("          LEFT  JOIN ACCOUNT_MAJOR_PRIZES  ON CM_OPERATION_ID = AMP_OPERATION_ID ");
            _str_sql.AppendLine("          LEFT  JOIN ACCOUNT_OPERATIONS    ON CM_OPERATION_ID = AO_OPERATION_ID ");
            _str_sql.AppendLine(" WHERE    CM_DATE >= @pFromDate ");
            _str_sql.AppendLine("   AND    CM_DATE < @pToDate ");

            // ANG Reject undone operations and undo operation.
            if (_skip_undone_operations)
            {
              _str_sql.AppendLine("   AND    (CM_UNDO_STATUS IS NULL OR CM_UNDO_STATUS = @pOperationUndoNone) ");
            }

            _str_sql.AppendLine("   AND    CM_TYPE IN( @pTypeDevolutionSplit1, @pTypeCancelSplit1 ");

            //SGB
            if (GeneralParam.GetBoolean("PSAClient", "Salidas.IncluirCancelacionesEmpresaB"))
            {
              _str_sql.AppendLine("                   , @pTypeCancelSplit2 ");
            }

            _str_sql.AppendLine("                   , @pTypePrizes, @pTypeTaxOnPrize1, @pTypeTaxOnPrize2, @pTypeTaxOnPrize3 ");

            // ANG : Added movements to report DrawCash
            _str_sql.AppendLine("         , @pTypePrizeInKind1Gross, @pTypeTax1OnPrizeKind1, @pTypeTax2OnPrizeKind1, @pTypeTax3OnPrizeKind1 ");
            _str_sql.AppendLine("         , @pTypePrizeInKind2Gross, @pTypeTax1OnPrizeKind2, @pTypeTax2OnPrizeKind2, @PTypeTax3OnPrizeKind1 ");

            _str_sql.AppendLine("         , @pTypePrizeReInKind1Gross, @pTypeTax1OnPrizeReKind1, @pTypeTax2OnPrizeReKind1, @pTypeTax3OnPrizeReKind1 ");
            _str_sql.AppendLine("         , @pTypePrizeReInKind2Gross, @pTypeTax1OnPrizeReKind2, @pTypeTax2OnPrizeReKind2, @pTypeTax3OnPrizeReKind2 ");

            _str_sql.AppendLine("         , @pTypePromoReFederalTax, @pTypePromoReStateTax, @pTypePromoReCouncilTax ");

            _str_sql.AppendLine("         ,@pPrizeDrawGrossOther");

            // Payment types.
            _str_sql.AppendLine("         , @pTypeBankCardPayment, @pTypeCheckPayment, @pTypeCheckPaymentRefund ");
            // Redeemable Prize Expired
            _str_sql.AppendLine("         , @pTypeRedemPrizeExpired ");
            // Decimal Rounding
            _str_sql.AppendLine("         , @pTypeDecimalRounding ");
            // DHA 29-MAY-2014
            _str_sql.AppendLine("         , @pTypeRechargeForPointsPrize ");

            //if (Report == WSI.Common.XMLReport.EnumAccountMovementsReport.RetreatsReport)
            //{
            //  _str_sql.AppendLine("          , @pTypeTaxOnPrize2, @pTypeTaxReturning, @pTypeServiceCharge ");
            //}
            //else
            //{
            _str_sql.AppendLine("         , @pTypeCashInSplit1, @pTypeCashInMBSplit1, @pTypeCashInNASplit1 ");
            _str_sql.AppendLine("         , @pTypeRechargeForPointsSplit1, @pTypePromotionRedeemable ");

            // AJQ 05-JUN-2014, Include TaxReturning
            _str_sql.AppendLine("         , @pTypeTaxReturning ");

            //_str_sql.AppendLine("       , @pTypeTaxReturning, @pTypeServiceCharge ");
            //}
            _str_sql.AppendLine(")");

            // DDM 12-APR-2016: Bug 11742:
            if (!AddOperationsExternalsOfGamingTables && !_add_operations_of_gaming_tables)
            {
              // 20-JAN-2015 JML    Product Backlog Item 202:Codere M�xico - Changes for table gamming activation
              _str_sql.AppendLine("   AND    ISNULL(AO_CODE, 0) NOT IN ( @pOCodeChipsSale,     @pOCodeChipsSaleWithRecharge, ");
              _str_sql.AppendLine("                                      @pOCodeChipsPurchase, @pOCodeChipsPurchaseWithCashOut ) ");
            }

            _str_sql.AppendLine(" ORDER BY CM_OPERATION_ID ");
            _sql_command.CommandText = _str_sql.ToString();

            _num_movs = 0;

            using (SqlDataReader _sql_reader = _sql_command.ExecuteReader())
            {
              // Browse the gathered account movements to produce the required data operation by operation
              while (_sql_reader.Read())
              {
                // AJQ 05-FEB-2018, SkipPrizeInKind2
                if (WSI.Common.GeneralParam.GetBoolean("PSAClient", "RegistroL007.SkipPrizeInKind2", false))
                {
                  bool _skip_mov = false;
                  switch ((CASHIER_MOVEMENT)_sql_reader[COL_SELECT_MOVS.TYPE])
                  {
                    case CASHIER_MOVEMENT.PRIZE_IN_KIND2_GROSS:       // Premio Sorteo de Caja en Promoci�n  (UNR)
                    case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_GROSS:    // Premio Sorteo de Caja en Promoci�n  (RE in Kind)
                    case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX2:        // Premio Sorteo de Caja en Promoci�n - Impuesto Estatal (UNR)
                    case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX2:     // Premio Sorteo de Caja en Promoci�n - Impuesto Estatal (RE in Kind)
                      _skip_mov = true;
                      break;
                    default:
                      break;
                  } // swith MovType

                  if (_skip_mov)
                  {
                    continue; // Continue with next register
                  }
                }

                _num_movs++;

                // Group all the movements related to the current operation
                _cm_operation_id = (_sql_reader.IsDBNull(COL_SELECT_MOVS.OPERATION_ID) ? 0 : _sql_reader.GetInt64(COL_SELECT_MOVS.OPERATION_ID));
                _cashier_session_id = (_sql_reader.IsDBNull(COL_SELECT_MOVS.CASHIER_SESSION) ? 0 : _sql_reader.GetInt64(COL_SELECT_MOVS.CASHIER_SESSION));
                _document_id = (_sql_reader.IsDBNull(COL_SELECT_MOVS.DOCUMENT_ID) ? 0 : _sql_reader.GetInt64(COL_SELECT_MOVS.DOCUMENT_ID));

                if (_last_operation_id != _cm_operation_id)
                {
                  // If Last operation has UNR Credit
                  // Will split register in two, one for recharge and other for UNR Prize
                  if (_last_operation_id != Int64.MinValue
                            && _operation_has_unr_credit)
                  {
                    _unr_row = CreateRowForUNRCredit(_row_cashier_mov);
                    _dt_cashier_movements.Rows.Add(_unr_row);
                  } //if (  _operation_has_unr_credit )

                  // Save the devolution value before get new operation data
                  if (_devolution != 0)
                  {
                    _row_cashier_mov[COL_CASHIER_MOVS.DEVOLUTION_AMOUNT] = _devolution;
                  }

                  // New operation
                  _account_id = _sql_reader.GetInt64(COL_SELECT_MOVS.ACCOUNT_ID);

                  // Was this operation already handled by this loop? 
                  // If so it should be present in _dt_movement_accounts (output datatable)
                  _row_account_mov = _dt_account_movements.Rows.Find(_account_id);

                  if (_row_account_mov == null)
                  {
                    // Operation was not handled yet              

                    //  - Add the account data and set the account totals to 0
                    _row_account_mov = _dt_account_movements.NewRow();

                    FillAccountRowWithSqlReaderData(_sql_reader, _row_account_mov, _account_id);

                    CheckEmptyFields(_row_account_mov);

                    _dt_account_movements.Rows.Add(_row_account_mov);
                  }

                  // DDM 09-AUG-2012 Now full name is composed from  AMP_PLAYER_NAME1, AMP_PLAYER_NAME2 and AMP_PLAYER_NAME3.
                  GetHolderData(_sql_reader, out _evidence_name, out _evidence_last_name1, out _evidence_last_name2, out _evidence_birth_date);

                  _row_cashier_mov = _dt_cashier_movements.NewRow();

                  _row_cashier_mov[COL_CASHIER_MOVS.OPERATION_ID] = _cm_operation_id;

                  if (AddOperationsExternalsOfGamingTables)
                  {
                    _row_cashier_mov[COL_CASHIER_MOVS.OPERATION_ID] = (Int64)_row_cashier_mov[COL_CASHIER_MOVS.OPERATION_ID] + 100000000;
                  }

                  _row_cashier_mov[COL_CASHIER_MOVS.DOCUMENT_ID] = _document_id;

                  //Fill fields
                  SetDefaultFields(_sql_reader, _row_cashier_mov, _account_id, _evidence_name, _evidence_last_name1, _evidence_last_name2, _cashier_session_id);

                  // ANG : Added CashierId
                  _row_cashier_mov[COL_CASHIER_MOVS.CASHIER_NAME] = GetColumnValue(_sql_reader, COL_SELECT_MOVS.CASHIER_NAME);

                  _row_cashier_mov[COL_CASHIER_MOVS.RECHARGE_FOR_POINTS_PRIZE] = 0;

                  // Set 'Efectivo' as default payment type key and as default Payment method.
                  // When examining operation movements change to apropiate if exist BankCard or Check movement.

                  _forma_pago_default_value = WSI.Common.GeneralParam.GetString("PSAClient", "FormaDePago.Efectivo", TIPO_PAGO);
                  _row_cashier_mov[COL_CASHIER_MOVS.PAYMENT_METHOD] = _forma_pago_default_value;

                  _clave_tipo_pago_default_value = WSI.Common.GeneralParam.GetString("PSAClient", "ClaveTipoPago.Efectivo");
                  if (String.IsNullOrEmpty(_clave_tipo_pago_default_value))
                  {
                    _clave_tipo_pago_default_value = WSI.Common.GeneralParam.GetString("PSAClient", "ClaveTipoPago");
                  }
                  _row_cashier_mov[COL_CASHIER_MOVS.PAYMENT_TYPE_KEY] = _clave_tipo_pago_default_value;

                  // FGB 21-OCT-2016: Additional fields for withholding (constancia)
                  if (HasToReportMajorPrizeAdditionalFields())
                  {
                    //If the Holder is same person as the Holding person, then report additional fields
                    if (IsSamePerson(_evidence_name, _evidence_last_name1, _evidence_last_name2, _evidence_birth_date, _row_cashier_mov))
                    {
                      //State
                      GetStateDesc(_sql_reader, out _holder_state_desc);

                      //Country
                      GetCountryDesc(_sql_reader, out _holder_country_desc);

                      //Set Additional fields for withholding
                      SetMajorPrizeAdditionalFields(_sql_reader, _row_cashier_mov, _holder_state_desc, _holder_country_desc);
                    }
                  }

                  String _filter_foucher;
                  DataRow[] _rows_voucher;

                  // Look for Voucher ( If exist )
                  _filter_foucher = String.Format("CV_SESSION_ID = {0} and CV_ACCOUNT_ID = {1} and CV_OPERATION_ID = {2}",
                                          _cashier_session_id,
                                          _account_id,
                                          _cm_operation_id);

                  _rows_voucher = _cashier_voucher.Select(_filter_foucher);

                  if (_rows_voucher != null && _rows_voucher.Length > 0)
                  {
                    if (_rows_voucher[0]["CV_VOUCHER_ID"] != DBNull.Value)
                    {
                      _row_cashier_mov[COL_CASHIER_MOVS.VOUCHER_ID] = (Int64)_rows_voucher[0]["CV_VOUCHER_ID"];
                    }

                    if (_rows_voucher[0]["CV_SEQUENCE"] != DBNull.Value)
                    {
                      _row_cashier_mov[COL_CASHIER_MOVS.FOLIO] = (Int64)_rows_voucher[0]["CV_SEQUENCE"];
                    }
                  }

                  _devolution = 0;

                  CheckEmptyFields(_row_cashier_mov);
                  _dt_cashier_movements.Rows.Add(_row_cashier_mov);

                  _last_operation_id = _cm_operation_id;

                  // Reset operation has unr credit flag.
                  _operation_has_unr_credit = false;

                } // if _last_operation_id != _am_operation_id

                // Accumulate the account movement data into the operation 
                //    - Operation's movements
                //    - Account totals

                switch ((CASHIER_MOVEMENT)_sql_reader[COL_SELECT_MOVS.TYPE])
                {
                  case CASHIER_MOVEMENT.CASH_IN_SPLIT1:
                  case CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1:
                  case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT1:
                  case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT1:

                    if (_row_cashier_mov[COL_CASHIER_MOVS.CASHIN_AMOUNT] == DBNull.Value)
                    {
                      _row_cashier_mov[COL_CASHIER_MOVS.CASHIN_AMOUNT] = (Decimal)_sql_reader[COL_SELECT_MOVS.ADD_AMOUNT];
                    }
                    else
                    {
                      _row_cashier_mov[COL_CASHIER_MOVS.CASHIN_AMOUNT] = (Decimal)_sql_reader[COL_SELECT_MOVS.ADD_AMOUNT]
                                                                      + (Decimal)_row_cashier_mov[COL_CASHIER_MOVS.CASHIN_AMOUNT];
                    }

                    _row_account_mov[COL_ACCS_TOTAL_CASHIN] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_CASHIN]
                                                            + (Decimal)_sql_reader[COL_SELECT_MOVS.ADD_AMOUNT];
                    break;

                  case CASHIER_MOVEMENT.PRIZES:
                  case CASHIER_MOVEMENT.PRIZE_DRAW_GROSS_OTHER:

                    if (_row_cashier_mov[COL_CASHIER_MOVS.PRIZE_AMOUNT] == DBNull.Value)
                    {
                      _row_cashier_mov[COL_CASHIER_MOVS.PRIZE_AMOUNT] = (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    }
                    else
                    {
                      _row_cashier_mov[COL_CASHIER_MOVS.PRIZE_AMOUNT] = (Decimal)_row_cashier_mov[COL_CASHIER_MOVS.PRIZE_AMOUNT] 
                                                                      + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                                                                   
                    }
                    _row_account_mov[COL_ACCS_TOTAL_PRIZE] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_PRIZE]
                                                           + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    break;

                  case CASHIER_MOVEMENT.PRIZE_IN_KIND1_GROSS:       // Premio Sorteo de Caja en Especie    (UNR)
                  case CASHIER_MOVEMENT.PRIZE_IN_KIND2_GROSS:       // Premio Sorteo de Caja en Promoci�n  (UNR)
                  case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_GROSS:    // Premio Sorteo de Caja en Especie    (RE in Kind)
                  case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_GROSS:    // Premio Sorteo de Caja en Promoci�n  (RE in Kind)

                    if (_row_cashier_mov[COL_CASHIER_MOVS.PRIZE_IN_KIND_AMOUNT] == DBNull.Value)
                    {
                      _row_cashier_mov[COL_CASHIER_MOVS.PRIZE_IN_KIND_AMOUNT] = (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    }
                    else
                    {
                      _row_cashier_mov[COL_CASHIER_MOVS.PRIZE_IN_KIND_AMOUNT] = (Decimal)_row_cashier_mov[COL_CASHIER_MOVS.PRIZE_IN_KIND_AMOUNT]
                                                                              + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    }

                    _row_account_mov[COL_ACCS_TOTAL_PRIZE_IN_KIND] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_PRIZE_IN_KIND]
                                                                   + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];

                    _operation_has_unr_credit = true;
                    break;

                  case CASHIER_MOVEMENT.DEV_SPLIT1:
                    _devolution = _devolution + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    _row_account_mov[COL_ACCS_TOTAL_DEVOLUTION] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_DEVOLUTION]
                                                                + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    break;

                  case CASHIER_MOVEMENT.CANCEL_SPLIT1:
                    _devolution = _devolution + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    _row_account_mov[COL_ACCS_TOTAL_DEVOLUTION] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_DEVOLUTION]
                                                                + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];

                    // SGB 17-MAR-2015: Cancelation controls
                    if (_row_cashier_mov[COL_CASHIER_MOVS.TOTAL_DEVOLUTION_A] == DBNull.Value)
                    {
                      _row_cashier_mov[COL_CASHIER_MOVS.TOTAL_DEVOLUTION_A] = (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    }
                    else
                    {
                      _row_cashier_mov[COL_CASHIER_MOVS.TOTAL_DEVOLUTION_A] = (Decimal)_row_cashier_mov[COL_CASHIER_MOVS.TOTAL_DEVOLUTION_A]
                                                                            + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    }
                    break;

                  case CASHIER_MOVEMENT.CANCEL_SPLIT2:
                    if (_row_cashier_mov[COL_CASHIER_MOVS.TOTAL_DEVOLUTION_B] == DBNull.Value)
                    {
                      _row_cashier_mov[COL_CASHIER_MOVS.TOTAL_DEVOLUTION_B] = (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    }
                    else
                    {
                      _row_cashier_mov[COL_CASHIER_MOVS.TOTAL_DEVOLUTION_B] = (Decimal)_row_cashier_mov[COL_CASHIER_MOVS.TOTAL_DEVOLUTION_B]
                                                                            + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    }
                    break;

                  case CASHIER_MOVEMENT.TAX_ON_PRIZE1:                      // FEDERAL TAXES
                  case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX1:                // Premio Sorteo de Caja en Especie   - Impuesto Federal (UNR)
                  case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX1:                // Premio Sorteo de Caja en Promoci�n - Impuesto Federal (UNR)
                  case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX1:             // Premio Sorteo de Caja en Especie   - Impuesto Federal (RE in Kind)
                  case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX1:             // Premio Sorteo de Caja en Promoci�n - Impuesto Federal (RE in Kind)
                  case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_FEDERAL_TAX:
                    {
                      if ((CASHIER_MOVEMENT)_sql_reader[COL_SELECT_MOVS.TYPE] == CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX1
                        || (CASHIER_MOVEMENT)_sql_reader[COL_SELECT_MOVS.TYPE] == CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX1
                        || (CASHIER_MOVEMENT)_sql_reader[COL_SELECT_MOVS.TYPE] == CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_FEDERAL_TAX)
                      {
                        _amount_column = COL_SELECT_MOVS.SUB_AMOUNT;
                      }
                      else
                      {
                        _amount_column = COL_SELECT_MOVS.ADD_AMOUNT;
                      }

                      if (_row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE1_AMOUNT] == DBNull.Value)
                      {
                        _row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE1_AMOUNT] = (Decimal)_sql_reader[_amount_column];
                      }
                      else
                      {
                        _row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE1_AMOUNT] = (Decimal)_row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE1_AMOUNT]
                                                                      + (Decimal)_sql_reader[_amount_column];
                      }

                      _row_account_mov[COL_ACCS_TOTAL_ISR1] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_ISR1]
                                                        + (Decimal)_sql_reader[_amount_column];

                      //PSAClient_EspecialMode.AlesisWinpot 
                      // Only CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX1 It must be added tax to the "PremioEspecial"
                      // Additional information: PRIZE_IN_KIND1_GROSS = PremioEspecie + Tax --> Gross
                      //                         PRIZE_IN_KIND2_GROSS = Premio Especie      --> Net
                      if (GetPSAClient_EspecialMode() == PSAClient_EspecialMode.AlesisWinpot
                        && ((CASHIER_MOVEMENT)_sql_reader[COL_SELECT_MOVS.TYPE] == CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX1
                        || (CASHIER_MOVEMENT)_sql_reader[COL_SELECT_MOVS.TYPE] == CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX1))
                      {
                        if (_row_cashier_mov[COL_CASHIER_MOVS.PRIZE_IN_KIND_AMOUNT] == DBNull.Value)
                        {
                          _row_cashier_mov[COL_CASHIER_MOVS.PRIZE_IN_KIND_AMOUNT] = (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT]; // COL_AM.SUB_AMOUNT is correct,  not use _amount_column.
                        }
                        else
                        {
                          _row_cashier_mov[COL_CASHIER_MOVS.PRIZE_IN_KIND_AMOUNT] = (Decimal)_row_cashier_mov[COL_CASHIER_MOVS.PRIZE_IN_KIND_AMOUNT]
                                                                                  + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                        }

                        _row_account_mov[COL_ACCS_TOTAL_PRIZE_IN_KIND] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_PRIZE_IN_KIND]
                                                                       + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                      }

                      break;
                    }
                  case CASHIER_MOVEMENT.TAX_ON_PRIZE2:                      // STATE TAXES
                  case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX2:                // Premio Sorteo de Caja en Especie   - Impuesto Estatal (UNR)
                  case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX2:                // Premio Sorteo de Caja en Promoci�n - Impuesto Estatal (UNR)
                  case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX2:             // Premio Sorteo de Caja en Especie   - Impuesto Estatal (RE in Kind)
                  case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX2:             // Premio Sorteo de Caja en Promoci�n - Impuesto Estatal (RE in Kind)
                  case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_STATE_TAX:
                    {
                      if ((CASHIER_MOVEMENT)_sql_reader[COL_SELECT_MOVS.TYPE] == CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX2
                        || (CASHIER_MOVEMENT)_sql_reader[COL_SELECT_MOVS.TYPE] == CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX2
                        || (CASHIER_MOVEMENT)_sql_reader[COL_SELECT_MOVS.TYPE] == CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_STATE_TAX)
                      {
                        _amount_column = COL_SELECT_MOVS.SUB_AMOUNT;
                      }
                      else
                      {
                        _amount_column = COL_SELECT_MOVS.ADD_AMOUNT;
                      }

                      if (_row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE2_AMOUNT] == DBNull.Value)
                      {
                        _row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE2_AMOUNT] = (Decimal)_sql_reader[_amount_column];
                      }
                      else
                      {
                        _row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE2_AMOUNT] = (Decimal)_row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE2_AMOUNT]
                                                                      + (Decimal)_sql_reader[_amount_column];
                      }

                      _row_account_mov[COL_ACCS_TOTAL_ISR2] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_ISR2]
                                                        + (Decimal)_sql_reader[_amount_column];

                      break;
                    }

                  case CASHIER_MOVEMENT.TAX_ON_PRIZE3:                      // COUNCIL TAXES
                  case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX3:                // Premio Sorteo de Caja en Especie   - Impuesto Municipal (UNR)
                  case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX3:                // Premio Sorteo de Caja en Promoci�n - Impuesto Municipal (UNR)
                  case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX3:             // Premio Sorteo de Caja en Especie   - Impuesto Municipal (RE in Kind)
                  case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX3:             // Premio Sorteo de Caja en Promoci�n - Impuesto Municipal (RE in Kind)
                  case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_COUNCIL_TAX:
                    {
                      if ((CASHIER_MOVEMENT)_sql_reader[COL_SELECT_MOVS.TYPE] == CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX3
                        || (CASHIER_MOVEMENT)_sql_reader[COL_SELECT_MOVS.TYPE] == CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX3
                        || (CASHIER_MOVEMENT)_sql_reader[COL_SELECT_MOVS.TYPE] == CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_COUNCIL_TAX)
                      {
                        _amount_column = COL_SELECT_MOVS.SUB_AMOUNT;
                      }
                      else
                      {
                        _amount_column = COL_SELECT_MOVS.ADD_AMOUNT;
                      }

                      if (_row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE3_AMOUNT] == DBNull.Value)
                      {
                        _row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE3_AMOUNT] = (Decimal)_sql_reader[_amount_column];
                      }
                      else
                      {
                        _row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE3_AMOUNT] = (Decimal)_row_cashier_mov[COL_CASHIER_MOVS.TAX_ON_PRIZE3_AMOUNT]
                                                                      + (Decimal)_sql_reader[_amount_column];
                      }

                      _row_account_mov[COL_ACCS_TOTAL_ISR3] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_ISR3]
                                                        + (Decimal)_sql_reader[_amount_column];

                      break;
                    }
                  // RRB 18-SEP-2012: TAX RETURNING, SERVICE CHARGE and DECIMAL ROUNDING
                  case CASHIER_MOVEMENT.TAX_RETURNING_ON_PRIZE_COUPON:
                    _row_cashier_mov[COL_CASHIER_MOVS.TAX_RETURNING] = _sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    _row_account_mov[COL_ACCS_TOTAL_TAX_RETURNING] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_TAX_RETURNING]
                                                                   + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    break;

                  case CASHIER_MOVEMENT.SERVICE_CHARGE:
                  case CASHIER_MOVEMENT.SERVICE_CHARGE_ON_COMPANY_A:   //EOR 19-SEP-2016
                  case CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE:
                  case CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE_ON_COMPANY_A:
                    _row_cashier_mov[COL_CASHIER_MOVS.SERVICE_CHARGE] = _sql_reader[COL_SELECT_MOVS.ADD_AMOUNT];
                    _row_account_mov[COL_ACCS_TOTAL_SERVICE_CHARGE] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_SERVICE_CHARGE]
                                                                    + (Decimal)_sql_reader[COL_SELECT_MOVS.ADD_AMOUNT];
                    break;

                  case CASHIER_MOVEMENT.DECIMAL_ROUNDING:
                    _row_cashier_mov[COL_CASHIER_MOVS.DECIMAL_ROUNDING] = _sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    _row_account_mov[COL_ACCS_TOTAL_DECIMAL_ROUNDING] = (Decimal)_row_account_mov[COL_ACCS_TOTAL_DECIMAL_ROUNDING]
                                                                      + (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];
                    break;
                  // DDM 05-12-2012
                  case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE:

                    if (!_is_reporting_redeemable)
                    {
                      // Not report promotional redeemable credit.
                      break;
                    }

                    if (_row_cashier_mov[COL_CASHIER_MOVS.PROMOTIONAL_BALANCE] != DBNull.Value)
                    {
                      _row_cashier_mov[COL_CASHIER_MOVS.PROMOTIONAL_BALANCE] = (Decimal)_row_cashier_mov[COL_CASHIER_MOVS.PROMOTIONAL_BALANCE] + (Decimal)_sql_reader[COL_SELECT_MOVS.ADD_AMOUNT];
                    }
                    else
                    {
                      _row_cashier_mov[COL_CASHIER_MOVS.PROMOTIONAL_BALANCE] = (Decimal)_sql_reader[COL_SELECT_MOVS.ADD_AMOUNT];
                    }

                    break;

                  // ANG : Added to handle PrizeExpired ( DEVOLUTION )
                  case CASHIER_MOVEMENT.REDEEMABLE_PRIZE_EXPIRED:

                    _row_cashier_mov[COL_CASHIER_MOVS.PRIZE_EXPIRED] = _sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];

                    break;

                  case CASHIER_MOVEMENT.CHECK_PAYMENT:
                  case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1:

                    _clave_tipo_pago_val = GeneralParam.GetString("PSAClient", "ClaveTipoPago.Cheque", _clave_tipo_pago_default_value);
                    _row_cashier_mov[COL_CASHIER_MOVS.PAYMENT_TYPE_KEY] = _clave_tipo_pago_val;

                    _forma_pago_val = WSI.Common.GeneralParam.GetString("PSAClient", "FormaDePago.Cheque", _forma_pago_default_value);
                    _row_cashier_mov[COL_CASHIER_MOVS.PAYMENT_METHOD] = _forma_pago_val;

                    break;

                  case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1:

                    _clave_tipo_pago_val = WSI.Common.GeneralParam.GetString("PSAClient", "ClaveTipoPago.Tarjeta", _clave_tipo_pago_default_value);
                    _row_cashier_mov[COL_CASHIER_MOVS.PAYMENT_TYPE_KEY] = _clave_tipo_pago_val;

                    _forma_pago_val = WSI.Common.GeneralParam.GetString("PSAClient", "FormaDePago.Tarjeta", _forma_pago_default_value);
                    _row_cashier_mov[COL_CASHIER_MOVS.PAYMENT_METHOD] = _forma_pago_val;

                    break;

                  // DHA 29-MAY-2014
                  case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_PRIZE:
                    _row_cashier_mov[COL_CASHIER_MOVS.RECHARGE_FOR_POINTS_PRIZE] = (Decimal)_sql_reader[COL_SELECT_MOVS.SUB_AMOUNT];

                    break;

                  default:
                    if (!_log_error_written)
                    {
                      Log.Error("DB_PSAReportesDiarios.AccountMovements. Unexpected movement type.  Operation_id: " + _sql_reader.GetInt64(COL_SELECT_MOVS.OPERATION_ID)
                              + ". Type:" + _sql_reader[COL_SELECT_MOVS.TYPE]);
                      _log_error_written = true;
                    }
                    break;
                } // switch

              } // while

              _sql_reader.Close();

              // Save the devolution value before end the process
              if (_devolution != 0)
              {
                _row_cashier_mov[COL_CASHIER_MOVS.DEVOLUTION_AMOUNT] = _devolution;
              }

              // If last operation has unr_row will create row for this.
              if (_operation_has_unr_credit
                 && _last_operation_id != Int64.MinValue)
              {
                _unr_row = CreateRowForUNRCredit(_row_cashier_mov);
                _dt_cashier_movements.Rows.Add(_unr_row);
              } //if (  _operation_has_unr_credit )

              // Output parameter are not avaliable until reader not closed!
              //InitialBalance = (Decimal)_pInitialBalance.Value;
            }

            // Compute Initial Balance with CASHIER_MOVEMENT.OPEN_SESSION 
            // and CASHIER_MOVMENT.FILLER_IN in Operations
            _str_sql.Length = 0;
            _str_sql.AppendLine("SELECT   ISNULL(SUM (CM_ADD_AMOUNT), 0)                   ");
            _str_sql.AppendLine("  FROM   CASHIER_MOVEMENTS with (index (IX_cm_date_type)) ");
            _str_sql.AppendLine(" WHERE   CM_TYPE IN (@pTypeOpenSession                    ");
            _str_sql.AppendLine("                   , @pTypeFillerIn )                     ");
            _str_sql.AppendLine("   AND   CM_DATE >= @pFromDate                            ");
            _str_sql.AppendLine("   AND   CM_DATE < @pToDate                               ");
            // RCI & DDM 27-FEB-2015: In L007 don't take in account FillerIn to gaming tables (non integrated).
            _str_sql.AppendLine("   AND   ISNULL(CM_GAMING_TABLE_SESSION_ID, 0) = 0        ");

            // ANG 
            // To ignore all open sessions or cashin operations in foreign currency.
            // Initial Balance isn't relevant value, the relevant value is ( Income - Outputs )
            _str_sql.AppendLine("   AND   ( CM_CURRENCY_ISO_CODE IS NULL                   ");
            _str_sql.AppendLine("            OR CM_CURRENCY_ISO_CODE = 'MXN' )             ");

            _sql_command.CommandText = _str_sql.ToString();

            InitialBalance = (Decimal)_sql_command.ExecuteScalar();
          }
        }

        // Add the number of movements (Cash In/Out + Prizes) that occurred after the initial data
        _dt_total_records.Rows.Add(_num_movs);

        // Name the output datatables
        _dt_total_records.TableName = TABLE_NAME_TOTAL_MOVEMENTS;

        // Publish the produced data
        _dataset.Tables.Add(_dt_cashier_movements);
        _dataset.Tables.Add(_dt_account_movements);
        _dataset.Tables.Add(_dt_total_records);

        // Name the dataset (XML purpouses)
        _dataset.DataSetName = DATASET_NAME;

        // Commit the changes
        _dataset.AcceptChanges();

        //if (Report == WSI.Common.XMLReport.EnumAccountMovementsReport.CashierSessionsTable)
        //{
        //  return (WSI.Common.XMLReport.AddCashierSessionsTable(_dataset) ? _dataset : null);
        //}

        return _dataset;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return null;
      }
    }
    # endregion

    #region Private Functions
    /// <summary>
    /// Remove unused columns
    /// </summary>
    /// <param name="Dataset"></param>
    private static void RemoveUnusedColumns(DataSet Dataset)
    {
      Dataset.Tables[TABLE_NAME_CASHIER_MOVEMENTS].Columns.Remove("RedondeoPorRetencion");
      Dataset.Tables[TABLE_NAME_CASHIER_MOVEMENTS].Columns.Remove("CargoPorServicio");
      Dataset.Tables[TABLE_NAME_CASHIER_MOVEMENTS].Columns.Remove("RedondeoPorDecimales");

      Dataset.Tables[TABLE_NAME_ACCOUNT_MOVEMENTS].Columns.Remove("TotalRetencion");
      Dataset.Tables[TABLE_NAME_ACCOUNT_MOVEMENTS].Columns.Remove("TotalServicio");
      Dataset.Tables[TABLE_NAME_ACCOUNT_MOVEMENTS].Columns.Remove("TotalDecimales");

      Dataset.Tables[TABLE_NAME_CASHIER_MOVEMENTS].Columns.Remove("SesionCaja");
    }

    /// <summary>
    /// return the dates from the date to report
    /// </summary>
    /// <param name="DateToReport"></param>
    /// <param name="FromDate"></param>
    /// <param name="ToDate"></param>
    /// <returns></returns>
    private static DateTime GetDateFromDateToReport(DateTime DateToReport, out DateTime FromDate, out DateTime ToDate)
    {
      DateTime _date_from;
      DateTime _date_to;

      // Eliminate the Minutes & hour part.
      DateToReport = DateToReport.AddMinutes(-DateToReport.Minute);
      DateToReport = DateToReport.AddHours(-DateToReport.Hour);

      _date_from = Misc.Opening(DateToReport);

      // Eliminate the Minutes & hour part.
      _date_from = _date_from.AddMinutes(-_date_from.Minute);
      _date_from = _date_from.AddHours(-_date_from.Hour);

      _date_to = _date_from.AddDays(1);

      FromDate = _date_from;
      ToDate = _date_to;

      return DateToReport;
    }

    /// <summary>
    /// Return the reader column value_sql_reader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <param name="ColumnIndex"></param>
    /// <returns></returns>
    private static String GetColumnValue(SqlDataReader SqlReader, int ColumnIndex)
    {
      return SqlReader.IsDBNull(ColumnIndex) ? null : SqlReader.GetString(ColumnIndex).Trim();
    }

    /// <summary>
    /// Set default fields values
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <param name="RowCashierMov"></param>
    /// <param name="AccountId"></param>
    /// <param name="EvidenceName"></param>
    /// <param name="EvidenceLastName1"></param>
    /// <param name="EvidenceLastName2"></param>
    /// <param name="CashierSessionId"></param>
    private static void SetDefaultFields(SqlDataReader SqlReader, DataRow RowCashierMov, Int64 AccountId, String EvidenceName, String EvidenceLastName1, String EvidenceLastName2, Int64 CashierSessionId)
    {
      DateTime _aux_date;
      _aux_date = SqlReader.GetDateTime(COL_SELECT_MOVS.DATETIME);

      RowCashierMov[COL_CASHIER_MOVS.DATETIME] = new DateTime(_aux_date.Year,
                                                              _aux_date.Month,
                                                              _aux_date.Day,
                                                              _aux_date.Hour,
                                                              _aux_date.Minute,
                                                              _aux_date.Second);

      RowCashierMov[COL_CASHIER_MOVS.ACCOUNT_ID] = AccountId;
      RowCashierMov[COL_CASHIER_MOVS.HOLDER_NAME] = GetColumnValue(SqlReader, COL_SELECT_MOVS.HOLDER_NAME);
      RowCashierMov[COL_CASHIER_MOVS.HOLDER_RFC] = GetColumnValue(SqlReader, COL_SELECT_MOVS.HOLDER_RFC);
      RowCashierMov[COL_CASHIER_MOVS.HOLDER_CURP] = GetColumnValue(SqlReader, COL_SELECT_MOVS.HOLDER_CURP);
      RowCashierMov[COL_CASHIER_MOVS.HOLDER_ADDRESS] = GetColumnValue(SqlReader, COL_SELECT_MOVS.HOLDER_ADDRESS);
      RowCashierMov[COL_CASHIER_MOVS.HOLDER_CITY] = GetColumnValue(SqlReader, COL_SELECT_MOVS.HOLDER_CITY);
      RowCashierMov[COL_CASHIER_MOVS.HOLDER_ZIP] = GetColumnValue(SqlReader, COL_SELECT_MOVS.HOLDER_ZIP);

      //EVIDENCE (CONSTANCIA) FIELDS
      RowCashierMov[COL_CASHIER_MOVS.EVIDENCE_HOLDER_NAME] = CardData.FormatFullName(FULL_NAME_TYPE.WITHHOLDING, EvidenceName, EvidenceLastName1, EvidenceLastName2);
      RowCashierMov[COL_CASHIER_MOVS.EVIDENCE_HOLDER_RFC] = GetColumnValue(SqlReader, COL_SELECT_MOVS.EVIDENCE_HOLDER_RFC);
      RowCashierMov[COL_CASHIER_MOVS.EVIDENCE_HOLDER_CURP] = GetColumnValue(SqlReader, COL_SELECT_MOVS.EVIDENCE_HOLDER_CURP);

      //TODO JMM 01-FEB-2012:  Read address fields from Evidence (ACCOUNT_MAJOR_PRIZES table)
      //_row_movement[COL_CASHIER_MOVS.EVIDENCE_ADDRESS] = _sql_reader.IsDBNull() ? null : _sql_reader.GetString().Trim();
      //_row_movement[COL_CASHIER_MOVS.EVIDENCE_CITY] = _sql_reader.IsDBNull() ? null : _sql_reader.GetString().Trim();
      //_row_movement[COL_CASHIER_MOVS.EVIDENCE_ZIP] = _sql_reader.IsDBNull() ? null : _sql_reader.GetString().Trim();

      RowCashierMov[COL_CASHIER_MOVS.EVIDENCE] = SqlReader.IsDBNull(COL_SELECT_MOVS.EVIDENCE) ? false : SqlReader.GetBoolean(COL_SELECT_MOVS.EVIDENCE);
      RowCashierMov[COL_CASHIER_MOVS.EVIDENCE_DOCUMENT_ID] = GetColumnValue(SqlReader, COL_SELECT_MOVS.EVIDENCE_DOCUMENT_ID);
      RowCashierMov[COL_CASHIER_MOVS.EVIDENCE_NO_DOCUMENT_ID] = GetColumnValue(SqlReader, COL_SELECT_MOVS.EVIDENCE_NO_DOCUMENT_ID);

      RowCashierMov[COL_CASHIER_MOVS.CASHIER_SESSION] = CashierSessionId;
    }

    /// <summary>
    /// Set major prize additional fields values
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <param name="RowCashierMov"></param>
    /// <param name="HolderStateDesc"></param>
    /// <param name="HolderCountryDesc"></param>
    private static void SetMajorPrizeAdditionalFields(SqlDataReader SqlReader, DataRow RowCashierMov, String HolderStateDesc, String HolderCountryDesc)
    {
      RowCashierMov[COL_CASHIER_MOVS.EVIDENCE_CALLE] = GetColumnValue(SqlReader, COL_SELECT_MOVS.HOLDER_ADDRESS_01);
      RowCashierMov[COL_CASHIER_MOVS.EVIDENCE_MUNICIPIO] = GetColumnValue(SqlReader, COL_SELECT_MOVS.HOLDER_CITY);
      RowCashierMov[COL_CASHIER_MOVS.EVIDENCE_ESTADO] = HolderStateDesc;
      RowCashierMov[COL_CASHIER_MOVS.EVIDENCE_PAIS] = HolderCountryDesc;
      RowCashierMov[COL_CASHIER_MOVS.EVIDENCE_NO_EXTERIOR] = GetColumnValue(SqlReader, COL_SELECT_MOVS.EVIDENCE_EXT_NUM);
      RowCashierMov[COL_CASHIER_MOVS.EVIDENCE_NO_INTERIOR] = GetColumnValue(SqlReader, COL_SELECT_MOVS.EVIDENCE_INT_NUM);
      RowCashierMov[COL_CASHIER_MOVS.EVIDENCE_COLONIA] = GetColumnValue(SqlReader, COL_SELECT_MOVS.HOLDER_ADDRESS_02);
      RowCashierMov[COL_CASHIER_MOVS.EVIDENCE_REFERENCIA] = GetColumnValue(SqlReader, COL_SELECT_MOVS.EVIDENCE_REFERENCE);
      RowCashierMov[COL_CASHIER_MOVS.EVIDENCE_CODIGO_POSTAL] = GetColumnValue(SqlReader, COL_SELECT_MOVS.HOLDER_ZIP);
      RowCashierMov[COL_CASHIER_MOVS.EVIDENCE_LOCALIDAD] = GetColumnValue(SqlReader, COL_SELECT_MOVS.EVIDENCE_LOCALITY);
    }

    /// <summary>
    /// Get holder data fields
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <param name="EvidenceName"></param>
    /// <param name="EvidenceLastName1"></param>
    /// <param name="EvidenceLastName2"></param>
    /// <param name="EvidenceBirthDate"></param>
    private static void GetHolderData(SqlDataReader SqlReader, out String EvidenceName, out String EvidenceLastName1, out String EvidenceLastName2, out DateTime EvidenceBirthDate)
    {
      EvidenceName = GetColumnValue(SqlReader, COL_SELECT_MOVS.EVIDENCE_PLAYER_NAME3);
      EvidenceLastName1 = GetColumnValue(SqlReader, COL_SELECT_MOVS.EVIDENCE_PLAYER_NAME1);
      EvidenceLastName2 = GetColumnValue(SqlReader, COL_SELECT_MOVS.EVIDENCE_PLAYER_NAME2);
      EvidenceBirthDate = SqlReader.IsDBNull(COL_SELECT_MOVS.EVIDENCE_PLAYER_BIRTH_DATE) ? DateTime.MinValue : SqlReader.GetDateTime(COL_SELECT_MOVS.EVIDENCE_PLAYER_BIRTH_DATE);
    }

    /// <summary>
    /// Fill the DataRow with the SQLReader data
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <param name="AccountRow"></param>
    /// <param name="AccountID"></param>
    private static void FillAccountRowWithSqlReaderData(SqlDataReader SqlReader, DataRow AccountRow, Int64 AccountID)
    {
      AccountRow[COL_ACCS_ACCOUNT_ID] = AccountID;
      AccountRow[COL_ACCS_NAME] = GetColumnValue(SqlReader, COL_SELECT_MOVS.HOLDER_NAME);
      AccountRow[COL_ACCS_RFC] = GetColumnValue(SqlReader, COL_SELECT_MOVS.HOLDER_RFC);
      AccountRow[COL_ACCS_CURP] = GetColumnValue(SqlReader, COL_SELECT_MOVS.HOLDER_CURP);
      AccountRow[COL_ACCS_ADDRESS] = GetColumnValue(SqlReader, COL_SELECT_MOVS.HOLDER_ADDRESS);
      AccountRow[COL_ACCS_CITY] = GetColumnValue(SqlReader, COL_SELECT_MOVS.HOLDER_CITY);
      AccountRow[COL_ACCS_ZIP] = GetColumnValue(SqlReader, COL_SELECT_MOVS.HOLDER_ZIP);
      AccountRow[COL_ACCS_TRACK_DATA] = GetColumnValue(SqlReader, COL_SELECT_MOVS.HOLDER_TRACK_DATA);
      AccountRow[COL_ACCS_TOTAL_CASHIN] = 0;
      AccountRow[COL_ACCS_TOTAL_DEVOLUTION] = 0;
      AccountRow[COL_ACCS_TOTAL_PRIZE] = 0;
      AccountRow[COL_ACCS_TOTAL_ISR1] = 0;
      AccountRow[COL_ACCS_TOTAL_ISR2] = 0;
      AccountRow[COL_ACCS_TOTAL_ISR3] = 0;
      AccountRow[COL_ACCS_TOTAL_TAX_RETURNING] = 0;
      AccountRow[COL_ACCS_TOTAL_SERVICE_CHARGE] = 0;
      AccountRow[COL_ACCS_TOTAL_DECIMAL_ROUNDING] = 0;
      AccountRow[COL_ACCS_TOTAL_PRIZE_IN_KIND] = 0;
      AccountRow[COL_ACCS_TOTAL_PRIZE_RE_IN_KIND] = 0;
      AccountRow[COL_ACCS_TOTAL_CUSTODY] = 0;   //EOR 26-APR-2016
    }

    /// <summary>
    /// Returns the description of the Country
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <param name="CountryDesc"></param>
    private static void GetCountryDesc(SqlDataReader SqlReader, out String CountryDesc)
    {
      Int32 _holder_country_id;

      CountryDesc = String.Empty;
      //If holder country ID has value search its name, else get the holder country description
      if (!SqlReader.IsDBNull(COL_SELECT_MOVS.HOLDER_COUNTRY_ID))
      {
        _holder_country_id = SqlReader.GetInt32(COL_SELECT_MOVS.HOLDER_COUNTRY_ID);
        CountryDesc = GetCountryName(_holder_country_id);
      }

      if (CountryDesc == String.Empty)
      {
        CountryDesc = GetColumnValue(SqlReader, COL_SELECT_MOVS.HOLDER_COUNTRY_DESC);
      }
    }

    /// <summary>
    /// Returns the description of the State
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <param name="StateDesc"></param>
    private static void GetStateDesc(SqlDataReader SqlReader, out String StateDesc)
    {
      Int32 _holder_state_id;

      //If holder state ID has value search its name, else get the holder state description
      StateDesc = String.Empty;
      if (!SqlReader.IsDBNull(COL_SELECT_MOVS.HOLDER_STATE_ID))
      {
        _holder_state_id = SqlReader.GetInt32(COL_SELECT_MOVS.HOLDER_STATE_ID);
        if (_holder_state_id != -1)
        {
          StateDesc = GetStateName(_holder_state_id);
        }
      }

      if (StateDesc == String.Empty)
      {
        StateDesc = GetColumnValue(SqlReader, COL_SELECT_MOVS.HOLDER_STATE_DESC);
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Cleans the empty string replacing it with null values
    // 
    //  PARAMS:
    //      - INPUT:
    //            - DataRow Row      
    //
    //      - OUTPUT:
    //
    // RETURNS:       
    // 
    private static void CheckEmptyFields(DataRow Row)
    {
      foreach (DataColumn _col in Row.Table.Columns)
      {
        if (_col.AllowDBNull && (Row[_col] != DBNull.Value))
        {
          if (Row[_col] is String)
          {
            if (String.IsNullOrEmpty(((String)Row[_col]).Trim()))
            {
              Row[_col] = null;
            }
          }
        }
      }
    }

    /// <summary>
    /// Create AccountMovementsTable columns
    /// </summary>
    /// <param name="AccountMovementsTable"></param>
    private static void CreateAccountMovementsColumns(ref DataTable AccountMovementsTable)
    {
      // COL_AM structure for indexs
      AccountMovementsTable.TableName = TABLE_NAME_ACCOUNT_MOVEMENTS;

      AccountMovementsTable.Columns.Add("NoCuenta", Type.GetType("System.Int64")).AllowDBNull = false;                //col 0
      AccountMovementsTable.Columns.Add("TotalDeposito", Type.GetType("System.Decimal")).AllowDBNull = true;          //col 1
      AccountMovementsTable.Columns.Add("TotalDevolucion", Type.GetType("System.Decimal")).AllowDBNull = true;        //col 2
      AccountMovementsTable.Columns.Add("TotalPremio", Type.GetType("System.Decimal")).AllowDBNull = true;            //col 3
      AccountMovementsTable.Columns.Add("TotalISR1", Type.GetType("System.Decimal")).AllowDBNull = true;              //col 4
      AccountMovementsTable.Columns.Add("TotalISR2", Type.GetType("System.Decimal")).AllowDBNull = true;              //col 5
      AccountMovementsTable.Columns.Add("TotalISR3", Type.GetType("System.Decimal")).AllowDBNull = true;              //col 6 ATB 15-MAR-2017
      AccountMovementsTable.Columns.Add("RFC", Type.GetType("System.String")).AllowDBNull = true;                     //col 7
      AccountMovementsTable.Columns.Add("CURP", Type.GetType("System.String")).AllowDBNull = true;                    //col 8
      AccountMovementsTable.Columns.Add("Nombre", Type.GetType("System.String")).AllowDBNull = true;                  //col 9
      AccountMovementsTable.Columns.Add("Domicilio", Type.GetType("System.String")).AllowDBNull = true;               //col 10
      AccountMovementsTable.Columns.Add("Localidad", Type.GetType("System.String")).AllowDBNull = true;               //col 11
      AccountMovementsTable.Columns.Add("CodigoPostal", Type.GetType("System.String")).AllowDBNull = true;            //col 12
      AccountMovementsTable.Columns.Add("TrackData", Type.GetType("System.String")).AllowDBNull = true;               //col 13
      AccountMovementsTable.Columns.Add("TotalRetencion", Type.GetType("System.Decimal")).AllowDBNull = true;         //col 14
      AccountMovementsTable.Columns.Add("TotalServicio", Type.GetType("System.Decimal")).AllowDBNull = true;          //col 15
      AccountMovementsTable.Columns.Add("TotalDecimales", Type.GetType("System.Decimal")).AllowDBNull = true;         //col 16
      AccountMovementsTable.Columns.Add("TotalPremioEspecie", Type.GetType("System.Decimal")).AllowDBNull = true;     //col 17
      AccountMovementsTable.Columns.Add("TotalPremioEspecieRe", Type.GetType("System.Decimal")).AllowDBNull = true;   //col 18
      AccountMovementsTable.Columns.Add("TotalImpuestoCustodia", Type.GetType("System.Decimal")).AllowDBNull = true;  //col 19 EOR 26-APR-2016

      // Primary Key
      AccountMovementsTable.PrimaryKey = new DataColumn[] { AccountMovementsTable.Columns["NoCuenta"] };
    }

    /// <summary>
    /// Create CashierMovementsTable columns
    /// </summary>
    /// <param name="CashierMovementsTable"></param>
    private static void CreateCashierMovementsColumns(ref DataTable CashierMovementsTable)
    {
      CreateCashierMovementsColumns(ref CashierMovementsTable, false);
    }

    /// <summary>
    /// Create CashierMovementsTable columns
    /// </summary>
    /// <param name="CashierMovementsTable"></param>
    /// <param name="IsForPSA"></param>
    /// <returns></returns>
    private static DataTable CreateCashierMovementsColumns(ref DataTable CashierMovementsTable, Boolean IsForPSA)
    {
      /**************************************************************************
       * JMM 25-JAN-2012
       * 
       * Columns names match the XML fields exported to the XML requiered by SAT
       * Please, DON'T CHANGE COLUMN NAMES
       * TODO: Load from NLS
       * COL_MOVS structure has columns indexes
       * ************************************************************************/
      CashierMovementsTable.TableName = TABLE_NAME_CASHIER_MOVEMENTS;

      // AJQ 05-JUN-2014, The OperationId must be a Int64
      CashierMovementsTable.Columns.Add("IdOperacion", Type.GetType("System.Int64")).AllowDBNull = false;                //col 0
      CashierMovementsTable.Columns.Add("FechaHora", Type.GetType("System.DateTime")).AllowDBNull = false;               //col 1
      CashierMovementsTable.Columns.Add("NoCuenta", Type.GetType("System.Int64")).AllowDBNull = false;                   //col 2
      CashierMovementsTable.Columns.Add("Deposito", Type.GetType("System.Decimal")).AllowDBNull = true;                  //col 3
      CashierMovementsTable.Columns.Add("Devolucion", Type.GetType("System.Decimal")).AllowDBNull = true;                //col 4
      CashierMovementsTable.Columns.Add("Premio", Type.GetType("System.Decimal")).AllowDBNull = true;                    //col 5
      CashierMovementsTable.Columns.Add("ISR1", Type.GetType("System.Decimal")).AllowDBNull = true;                      //col 6
      CashierMovementsTable.Columns.Add("ISR2", Type.GetType("System.Decimal")).AllowDBNull = true;                      //col 7 
      CashierMovementsTable.Columns.Add("ISR3", Type.GetType("System.Decimal")).AllowDBNull = true;                      //col 8        
      CashierMovementsTable.Columns.Add("RFC", Type.GetType("System.String")).AllowDBNull = true;                        //col 9
      CashierMovementsTable.Columns.Add("CURP", Type.GetType("System.String")).AllowDBNull = true;                       //col 10
      CashierMovementsTable.Columns.Add("Nombre", Type.GetType("System.String")).AllowDBNull = true;                     //col 11
      CashierMovementsTable.Columns.Add("Domicilio", Type.GetType("System.String")).AllowDBNull = true;                  //col 12
      CashierMovementsTable.Columns.Add("Localidad", Type.GetType("System.String")).AllowDBNull = true;                  //col 13
      CashierMovementsTable.Columns.Add("CodigoPostal", Type.GetType("System.String")).AllowDBNull = true;               //col 14        
      CashierMovementsTable.Columns.Add("DocumentID", Type.GetType("System.Int64")).AllowDBNull = true;                  //col 15
      CashierMovementsTable.Columns.Add("Constancia", Type.GetType("System.Boolean")).AllowDBNull = true;                //col 16
      CashierMovementsTable.Columns.Add("Constancia.RFC", Type.GetType("System.String")).AllowDBNull = true;             //col 17
      CashierMovementsTable.Columns.Add("Constancia.CURP", Type.GetType("System.String")).AllowDBNull = true;            //col 18
      CashierMovementsTable.Columns.Add("Constancia.Nombre", Type.GetType("System.String")).AllowDBNull = true;          //col 19
      CashierMovementsTable.Columns.Add("Constancia.Domicilio", Type.GetType("System.String")).AllowDBNull = true;       //col 20
      CashierMovementsTable.Columns.Add("Constancia.Localidad", Type.GetType("System.String")).AllowDBNull = true;       //col 21
      CashierMovementsTable.Columns.Add("Constancia.CodigoPostal", Type.GetType("System.String")).AllowDBNull = true;    //col 22
      CashierMovementsTable.Columns.Add("Constancia.DocumentoID", Type.GetType("System.String")).AllowDBNull = true;     //col 23
      CashierMovementsTable.Columns.Add("Constancia.NoDocumentoID", Type.GetType("System.String")).AllowDBNull = true;   //col 24
      CashierMovementsTable.Columns.Add("RedondeoPorRetencion", Type.GetType("System.Decimal")).AllowDBNull = true;      //col 25
      CashierMovementsTable.Columns.Add("CargoPorServicio", Type.GetType("System.Decimal")).AllowDBNull = true;          //col 26
      CashierMovementsTable.Columns.Add("RedondeoPorDecimales", Type.GetType("System.Decimal")).AllowDBNull = true;      //col 27
      CashierMovementsTable.Columns.Add("SesionCaja", Type.GetType("System.Int64")).AllowDBNull = false;                 //col 28

      CashierMovementsTable.Columns.Add("PremioEspecie", Type.GetType("System.Decimal")).AllowDBNull = true;             //col 29
      CashierMovementsTable.Columns.Add("PremioEspecieRE", Type.GetType("System.Decimal")).AllowDBNull = true;           //col 30

      //EOR 26-APR-2016
      CashierMovementsTable.Columns.Add("ImpuestoCustodia", Type.GetType("System.Decimal")).AllowDBNull = true;          //col 30

      // Specific for PSA
      if (IsForPSA)
      {
        //TODO: Not match with RegistroLinea007, In PSA Client is NoRegistroCaja
        CashierMovementsTable.Columns.Add("CashierName", Type.GetType("System.String")).AllowDBNull = false;             //col 31

        //TODO: Not match with RegistroLinea007, Not in use..
        CashierMovementsTable.Columns.Add("PremioCaducado", Type.GetType("System.Decimal")).AllowDBNull = true;          //col 32

        // TODO: Not match with RegistroLinea007, In PSA Client is  NoComprobante
        CashierMovementsTable.Columns.Add("VoucherId", Type.GetType("System.Int64")).AllowDBNull = true;                 //col 33
        CashierMovementsTable.Columns.Add("Folio", Type.GetType("System.Int64")).AllowDBNull = true;                     //col 34

        CashierMovementsTable.Columns.Add("SaldoPromocional", Type.GetType("System.Decimal")).AllowDBNull = true;        //col 35
        CashierMovementsTable.Columns.Add("ClaveTipoPago", Type.GetType("System.String")).AllowDBNull = true;            //col 36
        CashierMovementsTable.Columns.Add("FormaPago", Type.GetType("System.String")).AllowDBNull = true;                //col 37
        // DHA 29-MAY-2014
        CashierMovementsTable.Columns.Add("RecargaPorPuntosPremio", Type.GetType("System.Decimal")).AllowDBNull = true;  //col 38
        // SGB 16-MAR-2015
        CashierMovementsTable.Columns.Add("CancelacionEmpresaA", Type.GetType("System.Decimal")).AllowDBNull = true;     //col 39
        CashierMovementsTable.Columns.Add("CancelacionEmpresaB", Type.GetType("System.Decimal")).AllowDBNull = true;     //col 40

        if (HasToReportMajorPrizeAdditionalFields())
        {
          // FGB 21-OCT-2016: Additional fields for withholding (constancia)
        CashierMovementsTable.Columns.Add("Constancia.calle", Type.GetType("System.String")).AllowDBNull = true;         //col 41
        CashierMovementsTable.Columns.Add("Constancia.municipio", Type.GetType("System.String")).AllowDBNull = true;     //col 42
        CashierMovementsTable.Columns.Add("Constancia.estado", Type.GetType("System.String")).AllowDBNull = true;        //col 43
        CashierMovementsTable.Columns.Add("Constancia.pais", Type.GetType("System.String")).AllowDBNull = true;          //col 44
        CashierMovementsTable.Columns.Add("Constancia.noExterior", Type.GetType("System.String")).AllowDBNull = true;    //col 45
        CashierMovementsTable.Columns.Add("Constancia.noInterior", Type.GetType("System.String")).AllowDBNull = true;    //col 46
        CashierMovementsTable.Columns.Add("Constancia.colonia", Type.GetType("System.String")).AllowDBNull = true;       //col 47
        CashierMovementsTable.Columns.Add("Constancia.referencia", Type.GetType("System.String")).AllowDBNull = true;    //col 48
        CashierMovementsTable.Columns.Add("Constancia.codigoPostal", Type.GetType("System.String")).AllowDBNull = true;  //col 49
        CashierMovementsTable.Columns.Add("Constancia.localidad", Type.GetType("System.String")).AllowDBNull = true;     //col 50
        }
      }

      return CashierMovementsTable;
    }

    /// <summary>
    /// Create TotalRecordsTable columns
    /// </summary>
    /// <param name="TotalRecordsTable"></param>
    private static void CreateTotalRecordsColumns(ref DataTable TotalRecordsTable)
    {
      // Name the output datatable
      TotalRecordsTable.TableName = TABLE_NAME_TOTAL_MOVEMENTS;

      TotalRecordsTable.Columns.Add("TotalMovimientos", Type.GetType("System.Int32")).AllowDBNull = false;       //col 0
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Create RegistroL007 Row for UNR
    //          If the operation is a UNR credit movement, add a new record for the UNR part.
    //          
    //  PARAMS:
    //      - INPUT:
    //            - DataRow , Row with movement.
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      DataSet containing all the report data
    //    
    private static DataRow CreateRowForUNRCredit(DataRow RowMovement)
    {
      String _clave_tipo_pago_val;
      String _forma_pago_val;
      DataRow _unr_row;

      _unr_row = RowMovement.Table.NewRow();

      // Clone row and set balances to 0 except PrizeInKind
      _unr_row.ItemArray = (Object[])RowMovement.ItemArray.Clone();

      _unr_row[COL_CASHIER_MOVS.CASHIN_AMOUNT] = 0;
      _unr_row[COL_CASHIER_MOVS.DEVOLUTION_AMOUNT] = 0;
      _unr_row[COL_CASHIER_MOVS.PRIZE_AMOUNT] = 0;
      _unr_row[COL_CASHIER_MOVS.TAX_ON_PRIZE1_AMOUNT] = 0;
      _unr_row[COL_CASHIER_MOVS.TAX_ON_PRIZE2_AMOUNT] = 0;
      _unr_row[COL_CASHIER_MOVS.TAX_RETURNING] = 0;
      _unr_row[COL_CASHIER_MOVS.SERVICE_CHARGE] = 0;
      _unr_row[COL_CASHIER_MOVS.DECIMAL_ROUNDING] = 0;
      _unr_row[COL_CASHIER_MOVS.PRIZE_EXPIRED] = 0;
      _unr_row[COL_CASHIER_MOVS.PROMOTIONAL_BALANCE] = 0;
      _unr_row[COL_CASHIER_MOVS.PRIZE_IN_KIND_AMOUNT] = RowMovement[COL_CASHIER_MOVS.PRIZE_IN_KIND_AMOUNT];
      _unr_row[COL_CASHIER_MOVS.RECHARGE_FOR_POINTS_PRIZE] = 0;
      RowMovement[COL_CASHIER_MOVS.PRIZE_IN_KIND_AMOUNT] = 0;

      // Set ClaveTipoPago and Payment Method for UNR Register.
      _clave_tipo_pago_val = WSI.Common.GeneralParam.GetString("PSAClient", "ClaveTipoPago.EnEspecie"
                                                              , "003");
      _unr_row[COL_CASHIER_MOVS.PAYMENT_TYPE_KEY] = _clave_tipo_pago_val;

      _forma_pago_val = WSI.Common.GeneralParam.GetString("PSAClient", "FormaDePago.EnEspecie"
                                                          , "En Especie");
      _unr_row[COL_CASHIER_MOVS.PAYMENT_METHOD] = _forma_pago_val;

      if (GetPSAClient_EspecialMode() == PSAClient_EspecialMode.AlesisWinpot)
      {
        // registroL007.SaldoPromocion
        _unr_row[COL_CASHIER_MOVS.PROMOTIONAL_BALANCE] = _unr_row[COL_CASHIER_MOVS.PRIZE_IN_KIND_AMOUNT];

        // registroL007.SaldoInicial
        _unr_row[COL_CASHIER_MOVS.CASHIN_AMOUNT] = RowMovement.IsNull(COL_CASHIER_MOVS.TAX_ON_PRIZE1_AMOUNT) ? 0 : ((Decimal)RowMovement[COL_CASHIER_MOVS.TAX_ON_PRIZE1_AMOUNT]) * -1;

        _unr_row[COL_CASHIER_MOVS.TAX_ON_PRIZE1_AMOUNT] = RowMovement[COL_CASHIER_MOVS.TAX_ON_PRIZE1_AMOUNT];
        // delete tax because it gets in _unr_row and could affect BalanceLinea....
        RowMovement[COL_CASHIER_MOVS.TAX_ON_PRIZE1_AMOUNT] = 0;

        // Promotion without Recharge in this case, only one register
        if (RowMovement.IsNull(COL_CASHIER_MOVS.CASHIN_AMOUNT) || (Decimal)RowMovement[COL_CASHIER_MOVS.CASHIN_AMOUNT] == 0)
        {
          // Promotion without Recharge: In this case only one register! 
          RowMovement.Table.Rows.RemoveAt(RowMovement.Table.Rows.Count - 1);
        }
      }

      return _unr_row;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get Cashier Voucher for a date range
    //
    //  PARAMS:
    //      - INPUT: 
    //          - DateFrom    
    //          - DateTo
    //          - Trx
    //
    //      - OUTPUT:
    //          - CashierVoucher 
    //
    // RETURNS:
    //          - True: correctly executed 
    //          - False: otherwise
    //
    //   NOTES:
    //
    private static Boolean GetVoucherFromCashierMovement(DateTime DateFrom, DateTime DateTo, out DataTable CashierVoucher, SqlTransaction Trx)
    {
      CashierVoucher = new DataTable();
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT  CV_VOUCHER_ID   ");
        _sb.AppendLine("        , CV_SEQUENCE     ");
        _sb.AppendLine("        , CV_SESSION_ID   ");
        _sb.AppendLine("        , CV_ACCOUNT_ID   ");
        _sb.AppendLine("        , CV_OPERATION_ID ");
        _sb.AppendLine("   FROM   CASHIER_VOUCHERS WITH (INDEX(IX_datetime)) ");
        _sb.AppendLine("  WHERE   CV_DATETIME >= @pDateFrom  ");
        _sb.AppendLine("    AND   CV_DATETIME <  @pDateTo    ");
        _sb.AppendLine("    AND   CV_TYPE IN (@pVoucherType_CashInA, @pVoucherType_CashOutA) ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pDateFrom", SqlDbType.DateTime).Value = DateFrom;
          _sql_cmd.Parameters.Add("@pDateTo", SqlDbType.DateTime).Value = DateTo;
          _sql_cmd.Parameters.Add("@pVoucherType_CashInA", SqlDbType.Int).Value = CashierVoucherType.CashIn_A;
          _sql_cmd.Parameters.Add("@pVoucherType_CashOutA", SqlDbType.Int).Value = CashierVoucherType.CashOut_A;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(CashierVoucher);
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // GetVoucherFromCashierMovement

    /// <summary>
    /// Returns the country name
    /// </summary>
    /// <param name="CountryId">ID of the country</param>
    /// <returns></returns>
    private static String GetCountryName(Int32 CountryId)
    {
      String _country_name;
      String _adj;
      String _iso2;

      Countries.GetCountryInfo(CountryId, out _country_name, out _adj, out _iso2);

      return _country_name;
    }

    /// <summary>
    /// Returns the state name
    /// </summary>
    /// <param name="StateId">ID of the state</param>
    /// <returns></returns>
    private static String GetStateName(Int32 StateId)
    {
      return States.StateName(StateId);
    }

    /// <summary>
    /// Returns if we must report the Additional Fields for Major Prizes.
    /// </summary>
    /// <returns></returns>
    public static Boolean HasToReportMajorPrizeAdditionalFields()
    {
      return GeneralParam.GetBoolean("PSAClient", "ReportMajorPrizeAdditionalFields", false);
    }

    /// <summary>
    /// Check if the AccountHolder and the EvidenceHolder are the same person
    /// </summary>
    /// <param name="EvidenceName"></param>
    /// <param name="EvidenceLastName1"></param>
    /// <param name="EvidenceLastName2"></param>
    /// <param name="EvidenceBirthDate"></param>
    /// <param name="RowMovement"></param>
    /// <returns></returns>
    public static Boolean IsSamePerson(String EvidenceName, String EvidenceLastName1, String EvidenceLastName2, DateTime EvidenceBirthDate, DataRow RowMovement)
    {
      String _evidence_rfc;
      String _evidence_full_name;

      String _holder_rfc;
      String _holder_full_name;

      //Check Evidence RFC is not empty or generic
      _evidence_rfc = RowMovement[COL_CASHIER_MOVS.EVIDENCE_HOLDER_RFC].ToString();
      if (String.IsNullOrEmpty(_evidence_rfc) || ValidateFormat.IsGenericRFC(_evidence_rfc))
      {
        return false;
      }

      //Check Holder RFC is not empty or generic
      _holder_rfc = RowMovement[COL_CASHIER_MOVS.HOLDER_RFC].ToString();
      if (String.IsNullOrEmpty(_holder_rfc) || ValidateFormat.IsGenericRFC(_holder_rfc))
      {
        return false;
      }

      //Check RFC are the same
      if (_holder_rfc != _evidence_rfc)
      {
        return false;
      }

      //Compare full names
      _evidence_full_name = CardData.FormatFullName(FULL_NAME_TYPE.ACCOUNT, EvidenceName, EvidenceLastName1, EvidenceLastName2);
      if (String.IsNullOrEmpty(_evidence_full_name))
      {
        return false;
      }

      _holder_full_name = RowMovement[COL_CASHIER_MOVS.HOLDER_NAME].ToString();
      if (_evidence_full_name != _holder_full_name)
      {
        return false;
      }

      return true;
    }
    #endregion
  }
}