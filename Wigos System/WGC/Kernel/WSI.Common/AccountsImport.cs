//------------------------------------------------------------------------------
// Copyright � 2007-2009 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME:
// 
//   DESCRIPTION: Imports accounts with an external ID (ac_external_reference) from excel documents.
//
//        AUTHOR: Rafael Xandri
// 
// CREATION DATE: 23-JUL-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 23-JUL-2012 RXM    First release.
// 27-AUG-2012 MPO    InsertForPreassignedPromotion: Holder level passed.
// 27-AUG-2012 MPO    ImportForPreassignedPromotion: If has a general error, It won't return
// 01-OCT-2012 RXM    Added new fields to import
// 14-MAR-2013 RXM    Changed the Insert statement to EXECUTE CreateAccount strored procedure call
//                    Code for verification of duplicated ids improved
//                    Ensure that Excel is closed when process ends
// 18-MAR-2013 RXM    SQL timeout set to 60 seconds
// 09-APR-2013 RXM    Added movements for account creation and points update
// 09-APR-2013 RXM    Isert accounts processed with 3 retries decreasing batchupdatesize (1/2) to avoid timeouts
// 17-MAY-2013 MPO    Added to import column ID Site (ac_ms_created_on_site_id)
// 14-JUN-2013 RRR    Fixed Bug #855: Convert decimal to formatted data base string when regional configuration is not mexican
// 01-JUL-2013 DHA & MPO    Fixed Bug #894: Log shows negative values on Account Import
// 26-AUG-2013 JPJ & DDM    Fixed Bug WIG-160.     
// 29-AUG-2013 JAB    Implemented necessaries changes to generate excel file with the new library (SpreadSheetGear2012).
// 17-SEP-2013 ACM    Added AccountsImportPoints class
// 30-SEP-2013 FBA    Fixed non-controlled exception
// 22-OCT-2013 LEM    Unify saving account and cashier movements using SharedMovementsTables clases
// 07-JAN-2014 LEM    Fixed Bug WIG-504.
// 12-FEB-2014 JBC    Fixed Bug WIG-504: Timeout increased. Fixed inserting movements
// 12-FEB-2014 FBA    Added account holder id type control via CardData class IdentificationTypes
// 12-AUG-2014 RCI    Fixed Bug WIG-1178: Regionalization support
// 28-OCT-2014 SMN     Added new functionality: BLOCK_REASON field can have more than one block reason.
// 04-MAY-2015 YNM    Fixed Bug TFS-1160: Add birth Country and nationality on account import
// 27-OCT-2015 ECP    Fixed when import process is canceled by user, show appropaite message
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 23-FEB-2016 FGB    Fixed method AccountsImport for MultiSite Center
// 01-APR-2016 FGB    Fixed method AddImportedPoints, updated the Level Points bucket with Redemption Point value
// 02-AGO-2016 JRC    PBI 16257:Buckets - RankingLevelPoints_Generated - RankingLevelPoints_Discretional
// 19-SEP-2016 FGB    PBI 15985: Exped System: Cashier: Check if the payment/sale can be done
// 20-DEC-2016 XGJ    Bug 14917:Importaci�n de cuentas: Una vez importadas correctamente se muestran en min�sculas
// 13-OCT-2017 DHA    Bug 30232:WIGOS-5646 [Ticket #8958] Error on expiration date for preassigned promo
// 16-JUL-2018 AGS    Fixed Bug 33441:WIGOS-12444 [Ticket #14705] Bajar categor�a socio
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Office.Interop.Excel;
using System.IO;
using System.Reflection;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Common;
using Data = System.Data;
using Excel = Microsoft.Office.Interop.Excel;
using System.Diagnostics;
using System.Threading;
using SpreadsheetGear;
using WSI.Common.Utilities.Extensions;

namespace WSI.Common
{
  public class AccountsImport
  {

    #region ENUMS

    public enum IMPORT_STATES
    {
      ReadingDocument = 0,
      DocumentReaded = 1,
      UpdatingDB = 2,
      DBUpdated = 3,
      UpdatingBalances = 4,
      BalancesUpdated = 5,
      Aborted = 6

    }

    public enum REPORT_MODE
    {
      SUMMARY = 0,
      DETAILS = 1,
      ONLY_ERRORS = 3,
    }


    #endregion

    #region CONSTANTS

    public const String PROMO_NR_NAME = "Importaci�n - No Redimible";
    public const String PROMO_RE_NAME = "Importaci�n - Redimible";
    public const String RECYCLED_FLAG_TRACK_DATA = "-RECYCLED-";
    private const String ACCOUNTS_TABLE_NAME = "ACCOUNTS";
    private const String SCHEMA_ACCOUNTS_TABLE_NAME = "ACCOUNTS";
    private const int FIRST_DATA_ROW = 1;
    private const int COLUMN_NAMES_ROW = 0;

    private const int NUMBER_RETRIES_BATCH_DECREASE = 2;
    private const double DECREASE_VALUE_BATCH = 0.1;

    // RXM Field to compare before updating account or level data
    private string[] ACCOUNT_FIELDS = new string[] {"ac_holder_name", "ac_blocked", "ac_holder_id", "ac_holder_address_01", "ac_holder_address_02",
                                                    "ac_holder_address_03", "ac_holder_city", "ac_holder_zip", "ac_holder_email_01",
                                                    "ac_holder_email_02", "ac_holder_phone_number_01", "ac_holder_phone_number_02", 
                                                    "ac_holder_comments", "ac_holder_gender", "ac_holder_marital_status", "ac_holder_birth_date",
                                                    "ac_pin", "ac_holder_id1", "ac_holder_id2", "ac_holder_document_id1",
                                                    "ac_holder_document_id2", "ac_holder_name1", "ac_holder_name2", "ac_holder_name3",
                                                    "ac_holder_id_type", "ac_holder_is_vip", "ac_holder_title", "ac_holder_name4",
                                                    "ac_holder_wedding_date", "ac_holder_phone_type_01", "ac_holder_phone_type_02",
                                                    "ac_holder_state", "ac_holder_nationality", "ac_holder_address_country", "ac_holder_birth_country", "ac_holder_address_01_alt", "ac_holder_address_02_alt",
                                                    "ac_holder_address_03_alt", "ac_holder_city_alt", "ac_holder_zip_alt", "ac_holder_state_alt",
                                                    "ac_holder_country_alt", "ac_holder_is_smoker", "ac_holder_nickname", "ac_holder_credit_limit",
                                                    "ac_holder_request_credit_limit", "ac_pin_last_modified", "ac_last_activity_site_id",
                                                    "ac_creation_site_id", "ac_last_update_site_id"};

    private string[] LEVEL_FIELDS = new string[] { "ac_type", "ac_holder_level", "ac_holder_level_expiration", "ac_holder_level_entered" };

    #endregion

    #region members

    private Int32 m_retry;
    private String m_user;
    private Int32 m_no_action_accounts;
    private Int32 m_clients_count;
    private Int32 m_pending_clients;
    private Int32 m_batch_tick0;
    private Int32 m_process_tick0;
    public String m_file_path;
    public String m_file_out;
    public String[] m_Excel_cols;
    public String[] m_DB_cols;
    public Int32 m_not_new_accounts;
    ColumnDictionary m_column_matches;

    public class ColumnDictionary : Dictionary<String, String>
    {
      public new void Add(String Key, String Value)
      {
        base.Add(Key.Trim().ToUpper(), Value.Trim());
      }
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="LogFile"></param>
    /// <param name="ColumnMatches"></param>
    public AccountsImport(String LogFile, ColumnDictionary ColumnMatches)
    {
      this.m_file_out = LogFile;
      m_column_matches = ColumnMatches;
    }

    public AccountsImport(String LogFile)
    {
      this.m_file_out = LogFile;

      m_column_matches = new ColumnDictionary();
      m_column_matches.Add("Referencia", "ac_external_reference");
      m_column_matches.Add("Fecha Alta", "ac_created");
      m_column_matches.Add("�ltima Actividad", "ac_last_activity");
      m_column_matches.Add("Redimible", "ac_re_balance");
      m_column_matches.Add("Promo Redimible", "ac_promo_re_balance");
      m_column_matches.Add("Promo No Redimible", "ac_promo_nr_balance");
      m_column_matches.Add("Nivel", "ac_holder_level");
      m_column_matches.Add("VIP", "ac_holder_is_vip");
      m_column_matches.Add("Puntos", "cbu_value");
      m_column_matches.Add("Fecha Entrada", "ac_holder_level_entered");
      m_column_matches.Add("Fecha Caducidad", "ac_holder_level_expiration");
      m_column_matches.Add("T�tulo", "ac_holder_title");
      m_column_matches.Add("Nombre", "ac_holder_name3");
      m_column_matches.Add("Segundo Nombre", "ac_holder_name4");
      m_column_matches.Add("Apellido Paterno", "ac_holder_name1");
      m_column_matches.Add("Apellido Materno", "ac_holder_name2");
      m_column_matches.Add("Tipo Documento", "ac_holder_id_type");
      m_column_matches.Add("Documento", "ac_holder_id");
      m_column_matches.Add("Fecha Nacimiento", "ac_holder_birth_date");
      m_column_matches.Add("Sexo", "ac_holder_gender");
      m_column_matches.Add("Bloqueada", "ac_blocked");
      m_column_matches.Add("Estado Civil", "ac_holder_marital_status");
      m_column_matches.Add("Fecha Boda", "ac_holder_wedding_date");
      m_column_matches.Add("Tipo Tel�fono 1", "ac_holder_phone_type_01");
      m_column_matches.Add("Tel�fono 1", "ac_holder_phone_number_01");
      m_column_matches.Add("Tipo Tel�fono 2", "ac_holder_phone_type_02");
      m_column_matches.Add("Tel�fono 2", "ac_holder_phone_number_02");
      m_column_matches.Add("E-Mail 1", "ac_holder_email_01");
      m_column_matches.Add("E-Mail 2", "ac_holder_email_02");
      m_column_matches.Add("Calle", "ac_holder_address_01");
      m_column_matches.Add("Colonia", "ac_holder_address_02");
      m_column_matches.Add("Delegaci�n", "ac_holder_address_03");
      m_column_matches.Add("Municipio", "ac_holder_city");
      m_column_matches.Add("C�digo Postal", "ac_holder_zip");
      m_column_matches.Add("Estado", "ac_holder_state");
      m_column_matches.Add("Pa�s", "ac_holder_address_country");
      m_column_matches.Add("Pa�s Nacimiento", "ac_holder_birth_country");
      m_column_matches.Add("Calle Alternativa", "ac_holder_address_01_alt");
      m_column_matches.Add("Colonia Alternativa", "ac_holder_address_02_alt");
      m_column_matches.Add("Municipio Alternativo", "ac_holder_city_alt");
      m_column_matches.Add("C�digo Postal Alternativo", "ac_holder_zip_alt");
      m_column_matches.Add("Estado Alternativo", "ac_holder_state_alt");
      m_column_matches.Add("Pa�s Alternativo", "ac_holder_country_alt");
      m_column_matches.Add("Fumador", "ac_holder_is_smoker");
      m_column_matches.Add("Apodo", "ac_holder_nickname");
      m_column_matches.Add("L�mite Cr�dito", "ac_holder_credit_limit");
      m_column_matches.Add("L�mite Solicitud Cr�dito", "ac_holder_request_credit_limit");
      m_column_matches.Add("�ltima Sala con Actividad", "ac_last_activity_site_id");
      m_column_matches.Add("PIN", "ac_pin");
      m_column_matches.Add("�ltima Modificaci�n PIN", "ac_pin_last_modified");
      m_column_matches.Add("Casino Alta Tarjeta", "ac_creation_site_id");
      m_column_matches.Add("Casino �ltima Modificaci�n Tarjeta", "ac_last_update_site_id");

      if (GeneralParam.GetBoolean("MultiSite", "IsCenter", false))
      {
        m_column_matches.Add("ID Site", "ac_ms_created_on_site_id");
      }
    }

    # region private methods

    /// <summary>
    /// return the db column name that matches in the dicctionary m_column_matches
    /// </summary>
    /// <param name="ExcelColName"></param>
    /// <returns></returns>
    private String GetAccountsColumName(String ExcelColName)
    {
      String _account_col_name;
      _account_col_name = "";

      try
      {
        String _key;
        _key = ExcelColName.Trim().ToUpper();
        if (m_column_matches.ContainsKey(_key))
        {
          _account_col_name = m_column_matches[_key];
        }
      }
      catch
      {
        return "";
      }

      return _account_col_name;
    }

    /// <summary>
    /// Searches the Excel column name that matches with the DB Column Name
    /// </summary>
    /// <param name="DBColName"></param>
    /// <returns></returns>
    private String GetExcelColName(String DBColName)
    {
      String _excel_col_name;
      _excel_col_name = "";

      try
      {
        foreach (KeyValuePair<String, String> _key_pair in m_column_matches)
        {
          if (DBColName.Trim().ToUpper() == _key_pair.Value.Trim().ToUpper())
          {
            _excel_col_name = _key_pair.Key;
            break;
          }
        }
      }
      catch
      {
        return "";
      }

      return _excel_col_name;
    }

    /// <summary>
    /// gets schema from table accounts an default values using a SELECT FROM INFORMATION_SCHEMA.COLUMNS
    /// returns an empty datatable whith its schema
    /// </summary>
    /// <param name="AccountSchema"></param>
    /// <returns></returns>
    private Boolean GetAccountsTable(out System.Data.DataTable AccountsTable)
    {
      System.Data.DataTable _dt_accounts_def_values;
      _dt_accounts_def_values = new System.Data.DataTable();
      AccountsTable = new System.Data.DataTable("accounts");
      try
      {
        // get schema 
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand("SELECT * FROM " + ACCOUNTS_TABLE_NAME, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
            {

              _da.FillSchema(AccountsTable, SchemaType.Source);

            }
          }
        }
      }
      catch
      {
        // Log.Exception(_ex);
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _cmd = new SqlCommand("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + SCHEMA_ACCOUNTS_TABLE_NAME + "'", _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
            {
              _da.Fill(_dt_accounts_def_values);
            }
          }
        }
      }
      catch
      {
        // Log.Exception(_ex);
      }

      try
      {

        foreach (DataColumn col in AccountsTable.Columns)
        {
          if (col.ColumnName.ToUpper() == "AC_ACCOUNT_ID")
          {
            col.AutoIncrement = true;
          }

          if (col.ColumnName.ToUpper() == "AC_HOLDER_LEVEL_NOTIFY")
          {
            col.DefaultValue = 0;
          }

          object _defvalue = (_dt_accounts_def_values.Select("column_name = '" + col.ColumnName + "'"))[0]["column_default"];

          if (_defvalue != System.DBNull.Value)
          {
            if (col.DataType.ToString() == "System.DateTime")
            {
              col.DefaultValue = WGDB.Now;
            }
            else
            {
              try
              {
                col.DefaultValue = _defvalue.ToString().Trim('(').Trim(')');
              }
              catch
              {
              }
            }
          }
        }

        AccountsTable.Columns.Add("ROW_PROCESSED", Type.GetType("System.Boolean")).DefaultValue = false;
        AccountsTable.Columns.Add("CBU_VALUE", Type.GetType("System.Decimal"));
        AccountsTable.Columns.Add("EXCEL_ROW", Type.GetType("System.Int32"));
      }
      catch (Exception)
      { }

      return false;
    }

    /// <summary>
    /// Gets a DataColumn from a DataTable
    /// </summary>
    /// <param name="Table"></param>
    /// <param name="TableName"></param>
    /// <param name="ColumnName"></param>
    /// <param name="Warning"></param>
    /// <param name="WarningMessage"></param>
    /// <returns></returns>
    private DataColumn GetTableColumn(System.Data.DataTable Table, String TableName, String ColumnName, out Boolean Warning, out String WarningMessage)
    {
      DataColumn _data_column = null;

      Warning = false;
      WarningMessage = String.Empty;

      if (Table == null)
      {
        Warning = true;
        WarningMessage = String.Format("DataTable '{0}' not found", TableName);
        return _data_column;
      }

      _data_column = Table.Columns[ColumnName];

      if (_data_column == null)
      {
        Warning = true;
        WarningMessage = String.Format("Column '{0}' not found in table '{1}'", ColumnName, TableName);
      }

      return _data_column;
    }

    /// <summary>
    /// returns a formated cell value depending on the column datatype and def values
    /// </summary>
    /// <param name="CellValue"></param>
    /// <param name="Column"></param>
    /// <param name="Warning"></param>
    /// <param name="WarningMessage"></param>
    /// <returns></returns>
    private object CheckCell(object CellValue, DataColumn Column, out Boolean Warning, out String WarningMessage)
    {
      Warning = false;
      WarningMessage = "";
      String _str_value;
      String _upper_case_value;

      try
      {
        _str_value = "";
        if (CellValue != null)
        {
          _str_value = CellValue.ToString().Trim();
        }
        _upper_case_value = _str_value.ToUpper();

        if (String.IsNullOrEmpty(_str_value))
        {
          CellValue = DBNull.Value;

          if (Column.ColumnName == "ac_blocked")
          {
            CellValue = false;
          }
        }
        else if (CellValue != DBNull.Value)// !((CellValue == null) || (CellValue.ToString().Trim() == ""))
        {
          switch (Column.DataType.ToString())
          {
            case "System.String":
              {
                Warning = (_str_value.Length > Column.MaxLength);
                if (Warning)
                {
                  CellValue = (_str_value.Substring(0, Column.MaxLength));
                  WarningMessage += "\r\nWARNING: " + GetExcelColName(Column.ColumnName) + Resource.String("STR_AC_IMPORT_TRUNCATED");
                }
                else
                {
                  CellValue = (_str_value);
                }
                break;
              }
            case "System.DateTime":
              {
                CellValue = DateTime.FromOADate((Double)CellValue);
                break;
              }
            case "System.Int32":
              {
                if (Column.ColumnName == "ac_holder_gender")
                {
                  if (_upper_case_value.StartsWith("H"))
                  {
                    CellValue = GENDER.MALE;
                  }
                  else if (_upper_case_value.StartsWith("M"))
                  {
                    CellValue = GENDER.FEMALE;
                  }
                  else
                  {
                    CellValue = GENDER.MALE;
                  }
                }
                else if ((Column.ColumnName == "ac_holder_phone_type_01") || (Column.ColumnName == "ac_holder_phone_type_02"))
                {
                  if (_upper_case_value.StartsWith("HOM"))
                  {
                    CellValue = PHONE_TYPE.HOME_PHONE;
                  }
                  else if (_upper_case_value.StartsWith("CEL"))
                  {
                    CellValue = PHONE_TYPE.MOBILE_PHONE;
                  }
                  else if (_upper_case_value.StartsWith("BUS"))
                  {
                    CellValue = PHONE_TYPE.BUSINESS_PHONE;
                  }
                  else if (_upper_case_value.StartsWith("RAD"))
                  {
                    CellValue = PHONE_TYPE.RADIO;
                  }
                  else if (_upper_case_value.StartsWith("ALT"))
                  {
                    CellValue = PHONE_TYPE.MESSAGE_PHONE;
                  }
                  else if (_upper_case_value.StartsWith("FAX"))
                  {
                    CellValue = PHONE_TYPE.FAX;
                  }
                  else
                  {
                    CellValue = PHONE_TYPE.UNDEFINED_PHONE;
                  }
                }
                else if (Column.ColumnName == "ac_holder_id_type")
                {
                  Int32 _cell_value;
                  _cell_value = IdentificationTypes.GetIdentificationID(_upper_case_value);
                  CellValue = _cell_value;
                  if (_cell_value == 0)
                  {
                    WarningMessage += "\r\nWARNING: " + Resource.String("STR_AC_IMPORT_HOLDER_ID_TYPE_UNKNOWN",
                                                                        GetExcelColName(Column.ColumnName), _upper_case_value);
                    Warning = true;
                  }
                }
                else if (Column.ColumnName == "ac_holder_marital_status")
                {

                  // SINGLE = 0, MARRIED = 1, DIVORCED = 2, WIDOWED = 3, UNMARRIED = 4, OTHER = 5, UNSPECIFIED = 6
                  switch (_upper_case_value)
                  {
                    case "SOLTERO":
                      CellValue = ACCOUNT_MARITAL_STATUS.SINGLE;
                      break;
                    case "DIVORCIADO":
                      CellValue = ACCOUNT_MARITAL_STATUS.DIVORCED;
                      break;
                    case "CASADO":
                      CellValue = ACCOUNT_MARITAL_STATUS.MARRIED;
                      break;

                    case "UNI�N LIBRE":
                    case "UNION LIBRE":
                    case "UNI�N_LIBRE":
                    case "UNION_LIBRE":
                      CellValue = ACCOUNT_MARITAL_STATUS.UNMARRIED;
                      break;
                    case "SIN_ESPECIFICAR":
                    case "SIN ESPECIFICAR":
                      CellValue = ACCOUNT_MARITAL_STATUS.UNSPECIFIED;
                      break;
                    case "VIUDO":
                      CellValue = ACCOUNT_MARITAL_STATUS.WIDOWED;
                      break;
                    case "OTROS":
                    default:
                      CellValue = ACCOUNT_MARITAL_STATUS.OTHER;
                      break;
                  }

                  // RCI 13-AUG-2014: MaritalStatus in DB is saved plus 1!!!
                  CellValue = (Int32)CellValue + 1;
                }
                else if (Column.ColumnName == "ac_holder_birth_country")
                {
                  System.Data.DataTable _dt;
                  _dt = WSI.Common.CardData.GetCountriesList();
                  CellValue = (_dt.Select("CO_ISO2 = '" + CellValue.ToString() + "'"))[0]["CO_COUNTRY_ID"];
                  CellValue = (Int32)CellValue;
                  _dt.Dispose();
                }
                else if (Column.ColumnName == "ac_holder_address_country")
                {
                  System.Data.DataTable _dt;
                  _dt = WSI.Common.CardData.GetCountriesList();
                  CellValue = (_dt.Select("CO_ISO2 = '" + CellValue.ToString() + "'"))[0]["CO_COUNTRY_ID"];
                  CellValue = (Int32)CellValue;
                  _dt.Dispose();
                }
                else
                {

                  if (CellValue is Double)
                  {
                    CellValue = Convert.ToInt32((Double)CellValue);
                  }
                  else if (CellValue is Decimal)
                  {
                    CellValue = Convert.ToInt32((Decimal)CellValue);
                  }
                  else if (CellValue is Int64)
                  {
                    CellValue = Convert.ToInt32((Int64)CellValue);
                  }
                  else
                  {
                    CellValue = (Int32)CellValue;
                  }

                  if (Column.ColumnName == "ac_holder_level")
                  {
                    if ((Int32)CellValue > 4 || (Int32)CellValue < 0)
                    {
                      CellValue = (_str_value.Substring(0, Column.MaxLength));
                      WarningMessage += "\r\nWARNING: " + Resource.String("STR_AC_IMPORT_NOT_EXPECTED", GetExcelColName(Column.ColumnName));
                      Warning = true;
                    }
                  }
                  else if (Column.ColumnName == "ac_ms_created_on_site_id")
                  {

                    if (Convert.ToInt32(CellValue) > 999 || Convert.ToInt32(CellValue) < 1)
                    {
                      WarningMessage += "\r\nWARNING: " + Resource.String("STR_AC_IMPORT_NOT_EXPECTED", GetExcelColName(Column.ColumnName));
                      Warning = true;
                    }
                  }

                  break;
                }
              }

              break;

            case "System.Boolean":
              {
                if (Column.ColumnName == "ac_holder_is_vip")
                {
                  CellValue = (_upper_case_value == "VIP");
                }
                else if (Column.ColumnName == "ac_holder_is_smoker")
                {
                  CellValue = (_upper_case_value == "F");
                }
                else if (Column.ColumnName == "ac_blocked")
                {
                  CellValue = (_upper_case_value == "B");
                }


                if ((Boolean)CellValue == false)
                {
                  WarningMessage += "\r\nWARNING: " + Resource.String("STR_AC_IMPORT_NOT_EXPECTED", GetExcelColName(Column.ColumnName));
                  Warning = true;
                }
              }
              break;
            case "System.Decimal":
              if (CellValue is Double)
              {
                CellValue = (Double)CellValue;
              }
              else
              {
                CellValue = (Decimal)CellValue;
              }
              break;

            default:
              break;
          }
        }
      }
      catch (Exception)
      {
        throw new Exception(Resource.String("STR_AC_IMPORT_DATA_FORMAT") + " - " + GetExcelColName(Column.ColumnName));
      }
      return CellValue;
    }

    /// <summary>
    /// assigns  ac_user_type value from holder_level
    /// check if an account contains minimal values depending on if it's an anonymous account or a custom one
    /// assigns some default values for imported clients
    /// </summary>
    /// <param name="Row"></param>
    /// <param name="Columns"></param>
    /// <param name="RowNumber"></param>
    /// <returns></returns>
    private Boolean CheckRow(ref DataRow Account, DataColumnCollection Columns, int RowNumber, System.Data.DataTable ErrorsTable)
    {
      Account["EXCEL_ROW"] = RowNumber;

      Boolean _is_anonymous;

      try
      {
        _is_anonymous = (((Account["ac_holder_name3"] == DBNull.Value) || (Account["ac_holder_name3"].ToString().Trim() == ""))
            && ((Account["ac_holder_name1"] == DBNull.Value) || (Account["ac_holder_name1"].ToString().Trim() == ""))
            && ((Account["ac_holder_name2"] == DBNull.Value) || (Account["ac_holder_name2"].ToString().Trim() == ""))
            && ((Account["ac_holder_id_type"] == DBNull.Value) || (Account["ac_holder_id_type"].ToString().Trim() == ""))
            && ((Account["ac_holder_id"] == DBNull.Value) || (Account["ac_holder_id"].ToString().Trim() == ""))
            && ((Account["ac_holder_birth_date"] == DBNull.Value) || (Account["ac_holder_birth_date"].ToString().Trim() == ""))
            && ((Account["ac_holder_gender"] == DBNull.Value) || (Account["ac_holder_gender"].ToString().Trim() == "")));


        if ((Account["ac_holder_level"] == DBNull.Value) || (Account["ac_holder_level"].ToString().Trim() == ""))
        {
          Account["ac_holder_level"] = 0;
          if (!_is_anonymous)
          {
            Account["ac_holder_level"] = 1;
            Account["ac_user_type"] = 1;

          }
        }

        if ((!_is_anonymous) && (Account["ac_holder_gender"] == DBNull.Value))
        {
          Account["ac_holder_gender"] = 0;
        }

        if (Account["ac_ms_created_on_site_id"] != DBNull.Value)
        {

          if (Convert.ToInt32((Int32)Account["ac_ms_created_on_site_id"]) > 999 || Convert.ToInt32((Int32)Account["ac_ms_created_on_site_id"]) < 1)
          {
            return false;
          }

        }

        if ((Account["ac_external_reference"] == DBNull.Value)) // minimal
        {
          int _values_count;
          _values_count = 0;
          foreach (Object _account_cell in Account.ItemArray)
          {
            if (_account_cell != DBNull.Value)
            {
              _values_count++;
            }
          }

          if (_values_count > 4)
          {
            ErrorsTable.Rows.Add(RowNumber, Resource.String("STR_AC_IMPORT_REQUIRED_FIELD") + GetExcelColName("ac_external_reference"));
            return false;
          }
          else
          {
            // linea en blanco
            return false;

          }
        }

        if (Account["ac_blocked"] != DBNull.Value)
        {
          if ((Boolean)Account["ac_blocked"])
          {
            Account["ac_block_reason"] = (Int32)AccountBlockReason.IMPORT;
          }
        }

        if ((int)Account["ac_holder_level"] > 0)
        {
          if (Account["ac_holder_level_entered"] == DBNull.Value)
          {
            Account["ac_holder_level_entered"] = WGDB.Now;
          }

          if (Account["ac_holder_level_expiration"] == DBNull.Value)
          {
            Account["ac_holder_level_expiration"] = WGDB.Now + new TimeSpan(30, 0, 0, 0); // + 30 days
          }

          Account["ac_user_type"] = 1; // custom account
          Boolean _error_required_fields;
          StringBuilder _required_fields;
          _error_required_fields = false;

          _required_fields = new StringBuilder();

          if (Account["ac_holder_name1"] == DBNull.Value)
          {
            _error_required_fields = true;
            if (_required_fields.Length > 0) _required_fields.Append(", ");
            _required_fields.Append(GetExcelColName("ac_holder_name1"));
          }

          if (Account["ac_holder_name2"] == DBNull.Value)
          {
            if (GeneralParam.GetBoolean("Account.VisibleField", "Name2", true) &&
                GeneralParam.GetBoolean("Account.RequestedField", "Name2", true))
            {
              _error_required_fields = true;
              if (_required_fields.Length > 0) _required_fields.Append(", ");
              _required_fields.Append(GetExcelColName("ac_holder_name2"));
            }
          }

          if (Account["ac_holder_name3"] == DBNull.Value)
          {
            _error_required_fields = true;
            if (_required_fields.Length > 0) _required_fields.Append(", ");
            _required_fields.Append(GetExcelColName("ac_holder_name3"));
          }

          if (Account["ac_holder_name4"] == DBNull.Value)
          {
            if (GeneralParam.GetBoolean("Account.VisibleField", "Name4", true) &&
                GeneralParam.GetBoolean("Account.RequestedField", "Name4", true))
            {
              _error_required_fields = true;
              if (_required_fields.Length > 0) _required_fields.Append(", ");
              _required_fields.Append(GetExcelColName("ac_holder_name4"));
            }
          }

          if (Account["ac_holder_id_type"] == DBNull.Value)
          {
            _error_required_fields = true;
            if (_required_fields.Length > 0) _required_fields.Append(", ");
            _required_fields.Append(GetExcelColName("ac_holder_id_type"));
          }

          if (Account["ac_holder_id"] == DBNull.Value)
          {
            _error_required_fields = true;
            if (_required_fields.Length > 0) _required_fields.Append(", ");
            _required_fields.Append(GetExcelColName("ac_holder_id"));
          }

          if (Account["ac_holder_gender"] == DBNull.Value)
          {
            _error_required_fields = true;
            if (_required_fields.Length > 0) _required_fields.Append(", ");
            _required_fields.Append(GetExcelColName("ac_holder_gender"));
          }

          if (Account["ac_holder_birth_date"] == DBNull.Value)
          {
            _error_required_fields = true;
            if (_required_fields.Length > 0) _required_fields.Append(", ");
            _required_fields.Append(GetExcelColName("ac_holder_birth_date"));
          }

          if (_error_required_fields)
          {
            ErrorsTable.Rows.Add(RowNumber, Resource.String("STR_AC_IMPORT_REQUIRED_FIELD") + _required_fields);
            return false;
          }
          else
          {
            String _holder_name2;
            String _holder_name4;

            _holder_name2 = (Account["ac_holder_name2"] == DBNull.Value) ? "" : (String)Account["ac_holder_name2"];
            _holder_name4 = (Account["ac_holder_name4"] == DBNull.Value) ? "" : (String)Account["ac_holder_name4"];

            Account["ac_holder_name"] = CardData.FormatFullName(FULL_NAME_TYPE.ACCOUNT,
                                                                (String)Account["ac_holder_name3"], _holder_name4,
                                                                (String)Account["ac_holder_name1"], _holder_name2);
          }
        }
        else
        {
          Account["ac_user_type"] = 0; // anonymous account
          if ((Account["ac_holder_name"] != DBNull.Value) || (Account["ac_holder_id"] != DBNull.Value) || (Account["ac_holder_birth_date"] != DBNull.Value)
              || (Account["ac_holder_gender"] != DBNull.Value) || (Account["cbu_value"] != DBNull.Value) || (Account["ac_holder_level_entered"] != DBNull.Value)
              || (Account["ac_holder_level_expiration"] != DBNull.Value))
          {
            ErrorsTable.Rows.Add(RowNumber, Resource.String("STR_AC_IMPORT_ANONYMOUS_WRONG"));
            return false;

            //    Account["ac_user_type"] = 1;
            //    Account["ac_holder_level"] = 1;
          }
        }

        Int32 _site_id;
        Int32.TryParse(Misc.ReadGeneralParams("Site", "Identifier"), out _site_id);

        Account["ac_track_data"] = _site_id.ToString("0000") + "-IMPORT-" + WGDB.Now.ToString("yyyyMMddHHmmss") + "-ROW-" + RowNumber + RECYCLED_FLAG_TRACK_DATA;
        Account["ac_type"] = 2;
        Account["ac_card_paid"] = 1;

        foreach (DataColumn _column in Columns)
        {
          if (!_column.AllowDBNull)
          {

            if (Account[_column.ColumnName] == DBNull.Value)
            {
              if (_column.DefaultValue != DBNull.Value)
              {
                Account[_column.ColumnName] = _column.DefaultValue;
              }
              else
              {
                switch (_column.DataType.ToString())
                {
                  case "System.DateTime": Account[_column.ColumnName] = WGDB.Now;
                    break;
                  case "System.String": Account[_column.ColumnName] = "";
                    break;
                  case "System.Boolean": Account[_column.ColumnName] = 0;
                    break;
                  case "System.Int64": Account[_column.ColumnName] = 0;
                    break;
                  case "System.Int32": Account[_column.ColumnName] = 0;
                    break;
                  case "System.Decimal": Account[_column.ColumnName] = 0;
                    break;
                  default:
                    break;
                }
              }
            }
          }
        }
        Account["ac_balance"] = (decimal)Account["AC_RE_BALANCE"] + (decimal)Account["AC_PROMO_RE_BALANCE"] + (decimal)Account["AC_PROMO_NR_BALANCE"];

      }
      catch (Exception _RowException)
      {

        throw _RowException;

      }


      Account["ac_card_paid"] = true;


      return true;
    }

    /// <summary>
    /// Compare two accounts to know if an update is needed
    /// </summary>
    /// <param name="DbAccount"></param>
    /// <param name="ExcelAccount"></param>
    /// <param name="UpdatePoints"></param>
    /// <param name="UpdateClient"></param>
    /// <param name="UpdateLevel"></param>
    /// <returns></returns>
    private bool HaveChanges(DataRow DbAccount, DataRow ExcelAccount, bool UpdatePoints, bool UpdateClient, bool UpdateLevel)
    {
      List<string> _db_compare_fields;

      _db_compare_fields = new List<string>();

      if (UpdateClient)
      {
        _db_compare_fields.AddRange(ACCOUNT_FIELDS);
      }

      if (UpdateLevel)
      {
        _db_compare_fields.AddRange(LEVEL_FIELDS);
      }

      foreach (string _name in _db_compare_fields)
      {
        if (!DbAccount[_name].Equals(ExcelAccount[_name]))
        {
          return true;
        }
      }

      return false;
    }

    /// <summary>
    /// to avoid processing accounts when none update option is choosed 
    /// </summary>
    /// <param name="DBAccounts"></param>
    /// <param name="ExcelAccounts"></param>
    /// <param name="ImportNewAccounts"></param>
    private void CheckExistingAccountsByExtRef(System.Data.DataTable DBAccounts, System.Data.DataTable ExcelAccounts, bool ImportNewAccounts)
    {
      int _not_new_accounts;
      DataRow[] _existing_accounts;

      _not_new_accounts = 0;

      foreach (DataRow _account in ExcelAccounts.Rows)
      {
        _existing_accounts = DBAccounts.Select("ac_external_reference = '" + _account["ac_external_reference"].ToString() + "'");
        if (_existing_accounts.Length > 0)
        {
          _account.AcceptChanges();
          _not_new_accounts++;
        }
      }

      m_not_new_accounts = _not_new_accounts;

      if ((_not_new_accounts > 0) && (OnRowUpdated != null))
      {
        AccountImportEventArgs _rowupdateEvent = new AccountImportEventArgs();
        _rowupdateEvent.Message = "\r\n" + _not_new_accounts + " " + Resource.String("STR_AC_IMPORT_ALREADY_EXIST");
        // _not_new_accounts + " already exist and won't be updated.";
        _rowupdateEvent.Row_count = m_clients_count;

        if (ImportNewAccounts)
        {
          m_pending_clients = m_clients_count - _not_new_accounts;
        }
        else
        {
          m_pending_clients = 0;
        }

        _rowupdateEvent.Pending_rows = m_pending_clients;
        _rowupdateEvent.State = IMPORT_STATES.UpdatingDB;
        OnRowUpdated(this, _rowupdateEvent);
      }
    }

    private Boolean AvoidNewAccounts(System.Data.DataTable DBAccounts, System.Data.DataTable ExcelAccounts)
    {
      try
      {
        int _not_new_accounts;
        DataRow[] _existing_accounts;

        _not_new_accounts = 0;

        foreach (DataRow _account in ExcelAccounts.Rows)
        {
          _existing_accounts = DBAccounts.Select("ac_external_reference = '" + _account["ac_external_reference"].ToString() + "'");
          if (_existing_accounts.Length == 0)
          {
            _account.AcceptChanges();
            _not_new_accounts++;
          }
        } // end foreach ExcelAccounts.Rows

        if (OnRowUpdated != null)
        {
          AccountImportEventArgs _rowupdateEvent = new AccountImportEventArgs();
          _rowupdateEvent.Message = " \r\n" + _not_new_accounts + " " + Resource.String("STR_AC_IMPORT_DONT_EXIST");
          //_not_new_accounts + " don't exist and won't be created.";
          _rowupdateEvent.Row_count = m_clients_count;
          m_pending_clients = m_clients_count - _not_new_accounts;
          _rowupdateEvent.Pending_rows = m_pending_clients;
          _rowupdateEvent.State = IMPORT_STATES.UpdatingDB;
          OnRowUpdated(this, _rowupdateEvent);
        }

        return true;
      }
      catch (Exception _ex)
      {
        WSI.Common.Log.Exception(_ex);

        return false;
      }
    }

    /// <summary>
    /// avoid work with non existing accounts as its not posible to add accounts setting account_id values 
    /// </summary>
    /// <param name="DBAccounts"></param>
    /// <param name="ExcelAccounts"></param>
    /// <returns></returns>
    private System.Data.DataTable CompareAccountsByID(System.Data.DataTable DBAccounts, System.Data.DataTable ExcelAccounts, bool UpdatePoints, bool UpdateClient, bool UpdateLevel, bool ImportNewAccounts)
    {
      int _no_change_accounts;
      int _new_accounts;
      DataRow[] _existing_accounts;

      _new_accounts = 0;
      _no_change_accounts = 0;

      foreach (DataRow _excel_account in ExcelAccounts.Rows)
      {
        try
        {
          _existing_accounts = DBAccounts.Select("ac_account_id = '" + _excel_account["ac_external_reference"].ToString() + "'");
        }
        catch
        {
          _existing_accounts = new DataRow[0];
        }

        if (_existing_accounts.Length > 0)
        {

          if (!HaveChanges(_existing_accounts[0], _excel_account, UpdatePoints, UpdateClient, UpdateLevel))   //comparar datos, si no hay cambios avoid work
          {
            _excel_account.AcceptChanges();
            _no_change_accounts++;
          }
        }
        else
        {
          _excel_account.AcceptChanges(); // avoid work with this non existing account
          _new_accounts++;
        }
      } // end foreach ExcelAccounts.Rows

      if (OnRowUpdated != null)
      {
        AccountImportEventArgs _rowupdateEvent = new AccountImportEventArgs();
        _rowupdateEvent.Message = " \r\n" + _new_accounts + Resource.String("STR_AC_IMPORT_DONT_EXIST");
        _rowupdateEvent.Message += " \r\n" + _no_change_accounts + Resource.String("STR_AC_IMPORT_NO_CHANGE");
        m_no_action_accounts = _no_change_accounts;

        _rowupdateEvent.Row_count = m_clients_count;

        if ((ImportNewAccounts) || (UpdatePoints || UpdateClient || UpdateLevel))
        {
          m_pending_clients = m_clients_count - _new_accounts;
        }
        else
        {
          m_pending_clients = 0;
        }

        _rowupdateEvent.Pending_rows = m_pending_clients;
        _rowupdateEvent.State = IMPORT_STATES.UpdatingDB;
        OnRowUpdated(this, _rowupdateEvent);
      }
      return ExcelAccounts;
    }

    /// <summary>
    /// check if there are existing accounts with the same ac_holder_id and diferent external reference (only customized accounts)
    /// </summary>
    /// <param name="DBAccounts"></param>
    /// <param name="ExcelAccounts"></param>
    /// <returns></returns>
    private void CheckRepeatedHolder_Id(System.Data.DataTable DBAccounts, System.Data.DataTable ExcelAccounts, out bool Error)
    {
      string _repeated_holder_ids;
      Error = false;

      int _match_accounts;
      DataRow[] _existing_accounts;

      _repeated_holder_ids = "";
      _match_accounts = 0;

      foreach (DataRow _excel_account in ExcelAccounts.Rows)
      {
        _existing_accounts = DBAccounts.Select("ac_holder_id = '" + _excel_account["ac_holder_id"].ToString() + "'");
        if (_existing_accounts.Length > 0) // existen accounts con el mismo ac_holder_id
        {
          foreach (DataRow _db_account in _existing_accounts)
          {

            if ((int)_excel_account["ac_holder_level"] > 0)
            {
              if (_db_account["ac_external_reference"].ToString().Trim().ToUpper() != _excel_account["ac_external_reference"].ToString().Trim().ToUpper())      //error mismo documento diferente ext_ref en cuenta personalizada
              {
                _excel_account.AcceptChanges(); // no procesar
                _match_accounts++;
                _repeated_holder_ids += " " + _excel_account["ac_holder_id"];
                Error = true;

              }
            }
          } // end foerach existing_accounts
        }
      } // end foreach ExcelAccounts.Rows

      if (Error == true && OnRowUpdated != null)
      {
        AccountImportEventArgs _rowupdateEvent = new AccountImportEventArgs();
        _rowupdateEvent.Message = Resource.String("STR_AC_IMPORT_HOLDER_ID_EXISTS") + "Documento (" + _repeated_holder_ids + ").";
        _rowupdateEvent.Message += "\r\n" + Resource.String("STR_AC_IMPORT_PROCESS_STOP");
        _rowupdateEvent.Row_count = m_clients_count;
        m_pending_clients = m_clients_count - _match_accounts;
        _rowupdateEvent.Pending_rows = 0;
        _rowupdateEvent.State = IMPORT_STATES.Aborted;
        OnRowUpdated(this, _rowupdateEvent);
        this.WriteLog(_rowupdateEvent.Message);
      }



    }

    /// <summary>
    /// Avoid work with existing accounts that not have changes to do 
    /// </summary>
    /// <param name="DBAccounts"></param>
    /// <param name="ExcelAccounts"></param>
    /// <returns></returns>
    private System.Data.DataTable CompareAccountsByExtRef(System.Data.DataTable DBAccounts, System.Data.DataTable ExcelAccounts, bool UpdatePoints, bool UpdateClient, bool UpdateLevel, bool ImportNewAccounts)
    {

      int _no_change_accounts;
      DataRow[] _existing_accounts;

      _no_change_accounts = 0;

      foreach (DataRow _excel_account in ExcelAccounts.Rows)
      {
        _existing_accounts = DBAccounts.Select("ac_external_reference = '" + _excel_account["ac_external_reference"].ToString() + "'");
        if (_existing_accounts.Length > 0) // existen accounts con el mismo external reference
        {
          if (!HaveChanges(_existing_accounts[0], _excel_account, UpdatePoints, UpdateClient, UpdateLevel))// comparar datos, si no hay cambios avoid work
          {
            _excel_account.AcceptChanges();
            _no_change_accounts++;
          }
        }
      }

      if (OnRowUpdated != null && _no_change_accounts > 0)
      {
        AccountImportEventArgs _rowupdateEvent = new AccountImportEventArgs();
        _rowupdateEvent.Message = "\r\n" + _no_change_accounts + Resource.String("STR_AC_IMPORT_NO_CHANGE");
        m_no_action_accounts = _no_change_accounts;
        _rowupdateEvent.Row_count = m_clients_count;

        if ((ImportNewAccounts) || (UpdatePoints || UpdateClient || UpdateLevel))
        {
          m_pending_clients = m_clients_count - _no_change_accounts;
        }
        else
        {
          m_pending_clients = 0;
        }

        _rowupdateEvent.Pending_rows = m_pending_clients;
        _rowupdateEvent.State = IMPORT_STATES.UpdatingDB;
        OnRowUpdated(this, _rowupdateEvent);
      }
      return ExcelAccounts;
    }

    /// <summary>
    /// add a promotion if it doen't exist
    /// </summary>
    /// <param name="PromoName"></param>
    /// <param name="CreditType"></param>
    /// <returns></returns>
    private Boolean AddPromotion(String PromoName, ACCOUNT_PROMO_CREDIT_TYPE CreditType)
    {

      StringBuilder _sb;
      _sb = new StringBuilder();

      _sb.Length = 0;
      _sb.AppendLine("IF NOT EXISTS (SELECT * ");
      _sb.AppendLine("   FROM  PROMOTIONS ");
      _sb.AppendLine("  WHERE  PM_NAME = @p_PromoName ");
      _sb.AppendLine("    AND  PM_CREDIT_TYPE = @p_Credit_type) ");
      _sb.AppendLine("BEGIN ");
      _sb.AppendLine(" INSERT INTO DBO.PROMOTIONS ");
      _sb.AppendLine(" ( PM_NAME ");
      _sb.AppendLine(" , PM_ENABLED ");
      _sb.AppendLine(" , PM_TYPE ");
      _sb.AppendLine(" , PM_DATE_START ");
      _sb.AppendLine(" , PM_DATE_FINISH ");
      _sb.AppendLine(" , PM_SCHEDULE_WEEKDAY ");
      _sb.AppendLine(" , PM_SCHEDULE1_TIME_FROM ");
      _sb.AppendLine(" , PM_SCHEDULE1_TIME_TO ");
      _sb.AppendLine(" , PM_SCHEDULE2_ENABLED ");
      _sb.AppendLine(" , PM_GENDER_FILTER ");
      _sb.AppendLine(" , PM_BIRTHDAY_FILTER ");
      _sb.AppendLine(" , PM_EXPIRATION_TYPE ");
      _sb.AppendLine(" , PM_EXPIRATION_VALUE ");
      _sb.AppendLine(" , PM_MIN_CASH_IN ");
      _sb.AppendLine(" , PM_MIN_CASH_IN_REWARD ");
      _sb.AppendLine(" , PM_CASH_IN ");
      _sb.AppendLine(" , PM_CASH_IN_REWARD ");
      _sb.AppendLine(" , PM_NUM_TOKENS ");
      _sb.AppendLine(" , PM_TOKEN_REWARD ");
      _sb.AppendLine(" , PM_LEVEL_FILTER ");
      _sb.AppendLine(" , PM_PERMISSION ");
      _sb.AppendLine(" , PM_MIN_SPENT ");
      _sb.AppendLine(" , PM_MIN_SPENT_REWARD ");
      _sb.AppendLine(" , PM_SPENT ");
      _sb.AppendLine(" , PM_SPENT_REWARD ");
      _sb.AppendLine(" , PM_MIN_PLAYED ");
      _sb.AppendLine(" , PM_MIN_PLAYED_REWARD ");
      _sb.AppendLine(" , PM_PLAYED ");
      _sb.AppendLine(" , PM_PLAYED_REWARD ");
      _sb.AppendLine(" , PM_PLAY_RESTRICTED_TO_PROVIDER_LIST ");
      _sb.AppendLine(" , PM_CREDIT_TYPE) ");
      _sb.AppendLine(" VALUES ");
      _sb.AppendLine(" ( @p_PromoName ");
      _sb.AppendLine(" , 0 ");
      _sb.AppendLine(" , 0 ");
      _sb.AppendLine(" , '2012-08-01T00:00:00' ");
      _sb.AppendLine(" , '2100-01-01T00:00:00' ");
      _sb.AppendLine(" , 127 ");
      _sb.AppendLine(" , 0 ");
      _sb.AppendLine(" , 0 ");
      _sb.AppendLine(" , 0 ");
      _sb.AppendLine(" , 0 ");
      _sb.AppendLine(" , 0 ");
      _sb.AppendLine(" , 1 ");
      _sb.AppendLine(" , 30 ");
      _sb.AppendLine(" , 0.00 ");
      _sb.AppendLine(" , 0.00 ");
      _sb.AppendLine(" , 0.00 ");
      _sb.AppendLine(" , 0.00 ");
      _sb.AppendLine(" , 0 ");
      _sb.AppendLine(" , 0.00 ");
      _sb.AppendLine(" , 0 ");
      _sb.AppendLine(" , 0 ");
      _sb.AppendLine(" , 0.00 ");
      _sb.AppendLine(" , 0.00 ");
      _sb.AppendLine(" , 0.00 ");
      _sb.AppendLine(" , 0.00 ");
      _sb.AppendLine(" , 0.00 ");
      _sb.AppendLine(" , 0.00 ");
      _sb.AppendLine(" , 0.00 ");
      _sb.AppendLine(" , 0.00 ");
      _sb.AppendLine(" , 0 ");
      _sb.AppendLine(" , @p_Credit_type) ");
      _sb.AppendLine("END ");

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString()))
          {
            _sql_cmd.Parameters.Add("p_PromoName", SqlDbType.NVarChar).Value = PromoName;
            _sql_cmd.Parameters.Add("p_Credit_type", SqlDbType.Int).Value = CreditType;

            _db_trx.ExecuteNonQuery(_sql_cmd);
            _db_trx.Commit();

            return true;

          }
        }
      }
      catch
      {
        return false;

      }
    }


    #endregion

    # region public methods

    public Boolean ExcelGetAccounts(String FilePath, out System.Data.DataSet Accounts)
    {
      Accounts = null;

      m_file_path = FilePath;
      WriteLog(Resource.String("STR_AC_IMPORT_SEPARATOR") + "\r\n" + Resource.String("STR_AC_IMPORT_START2") + WGDB.Now.ToString());

      if (!File.Exists(FilePath))
      {
        return false;
      }

      String _exec_path;

      SpreadsheetGear.IWorkbook _excel_book = null;
      SpreadsheetGear.IWorksheet _excel_sheet = null;
      SpreadsheetGear.IRange _excel_range = null;

      object[,] _range_values;

      System.Data.DataTable _dt_errors;
      System.Data.DataTable _dt_duplicate;
      System.Data.DataTable _dt_accounts;

      DataRow _row;
      DataRow _account;

      int _num_rows;
      int _num_cols;
      String _down_address;
      Int32 _correct_accounts;
      Int32 _error_accounts;
      Int32 _excel_row_index;

      _dt_accounts = new System.Data.DataTable("accounts");
      _dt_errors = new System.Data.DataTable("errors");
      _dt_duplicate = new System.Data.DataTable("duplicate");
      _dt_errors.Columns.Add("row", typeof(int));
      _dt_errors.Columns.Add("message", typeof(String));
      _dt_duplicate.Columns.Add("row", typeof(int));
      _dt_duplicate.Columns.Add("message", typeof(String));

      System.Globalization.CultureInfo _old_ci = System.Threading.Thread.CurrentThread.CurrentCulture;
      System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-us");

      try
      {
        _exec_path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
        _excel_book = Factory.GetWorkbook(FilePath);

        _excel_sheet = _excel_book.Worksheets[0];
        _excel_range = _excel_sheet.UsedRange.CurrentRegion;

        // get the address of the bottom, right cell
        _down_address = _excel_range.GetAddress(false,
                                                false,
                                                ReferenceStyle.A1,
                                                false,
                                                null);

        // Get the range, then values from a1
        _excel_range = _excel_sheet.Range[_down_address];
        _range_values = (object[,])_excel_range.Value;
        _num_rows = _range_values.GetLength(0) - 1;
        _num_cols = _range_values.GetLength(1) - 1;

        // get schema 
        GetAccountsTable(out _dt_accounts);

        for (int _idx_row = FIRST_DATA_ROW; _idx_row <= _num_rows; _idx_row++)
        {
          _excel_row_index = _idx_row + 1;

          if (OnRowUpdated != null) // call event
          {
            if ((_idx_row == _num_rows) || (_idx_row % 2000 == 0))
            {
              AccountImportEventArgs _rowupdateEvent;

              _rowupdateEvent = new AccountImportEventArgs();
              _rowupdateEvent.Message = "\r\n" + Resource.String("STR_AC_IMPORT_PROCESSING") + Resource.String("STR_AC_IMPORT_READ_ACCOUNTS") + _excel_row_index.ToString();
              if (_idx_row != _num_rows)
              {
                _rowupdateEvent.Message += "\r\n" + Resource.String("STR_AC_IMPORT_PROCESSING") + Resource.String("STR_AC_IMPORT_READ_ERRORS_FOUND") + _dt_errors.Rows.Count;
              }
              _rowupdateEvent.Row_count = _num_rows;
              _rowupdateEvent.Pending_rows = _num_rows - _idx_row;
              if (_rowupdateEvent.Pending_rows <= 0)
              {
                _rowupdateEvent.State = IMPORT_STATES.ReadingDocument;
              }
              else
              {
                _rowupdateEvent.State = IMPORT_STATES.ReadingDocument;
              }
              WriteLog(_rowupdateEvent.Message);
              OnRowUpdated(this, _rowupdateEvent);
            }
          } // end if OnRowUpdated != null

          try
          {
            _row = _dt_accounts.NewRow();

            for (int _idx_col = 0; _idx_col <= _num_cols; _idx_col++)
            {
              String _col_name;
              Object _cell_value;
              String _warning_message;
              Boolean _warning;
              DataColumn _data_column;

              _col_name = String.Empty;
              if (_range_values[COLUMN_NAMES_ROW, _idx_col] != null)
              {
                //Gets the Accounts.ColumnName for the Excel.ColumnName
                _col_name = GetAccountsColumName((_range_values[COLUMN_NAMES_ROW, _idx_col]).ToString());
              }

              if (String.IsNullOrEmpty(_col_name))
              {
                continue;
              }

              _cell_value = (_range_values[_idx_row, _idx_col]); // From Excel.Cell(_idx_row, _idx_col)

              if (_col_name.Contains("name"))
              {
                if (_cell_value != null) _cell_value = _cell_value.ToString().ToUpper();
              }

              _data_column = GetTableColumn(_dt_accounts, "Accounts", _col_name, out _warning, out _warning_message);

              if (_data_column != null)
              {
                _row[_col_name] = CheckCell(_cell_value, _data_column, out _warning, out _warning_message);
              }

              if (_warning)
              {
                _dt_errors.Rows.Add(_excel_row_index, _warning_message);

                if (OnRowUpdated != null) // call event
                {
                  AccountImportEventArgs _rowupdateEvent;
                  _rowupdateEvent = new AccountImportEventArgs();
                  _rowupdateEvent.Message = "\r\n" + Resource.String("STR_AC_IMPORT_ROW") + _excel_row_index.ToString() + " " + _warning_message;
                  _rowupdateEvent.Row_count = _num_rows;
                  _rowupdateEvent.Pending_rows = _num_rows - _idx_row;
                  if (_rowupdateEvent.Pending_rows <= 0)
                  {
                    _rowupdateEvent.State = IMPORT_STATES.ReadingDocument;
                  }
                  else
                  {
                    _rowupdateEvent.State = IMPORT_STATES.ReadingDocument;
                  }
                  WriteLog(_rowupdateEvent.Message);
                  OnRowUpdated(this, _rowupdateEvent);

                } // end if OnRowUpdated != null
              }
            } // for (int _idx_col 

            if (CheckRow(ref _row, _dt_accounts.Columns, _excel_row_index, _dt_errors))
            {
              _dt_accounts.Rows.Add(_row);
            }
            else
            {
              continue;
            }
          }
          catch (Exception ex)   // row exception
          {
            if (ex.Message.ToUpper().Contains("Input string was not in a correct format".ToUpper()))
            {
              _dt_errors.Rows.Add(_excel_row_index, Resource.String("STR_AC_IMPORT_DATA_FORMAT"));
            }
            else
            {
              _dt_errors.Rows.Add(_excel_row_index, ex.Message);
            }

            continue;
          }

        } // for (int _idx_row

      } // end for

      catch (Exception)
      {
        CloseBookObjects(ref _excel_book, ref _excel_sheet, ref _excel_range);
        return false;
      }
      finally
      {
        CloseBookObjects(ref _excel_book, ref _excel_sheet, ref _excel_range);
      }

      // set previous cultureinfo
      System.Threading.Thread.CurrentThread.CurrentCulture = _old_ci;

      Accounts = new DataSet();
      Accounts.Tables.Add(_dt_accounts);
      Accounts.Tables.Add(_dt_errors);
      Accounts.Tables.Add(_dt_duplicate);
      _correct_accounts = 0;
      _error_accounts = 0;
      DataRow[] _repeatedrows;
      for (Int32 _accounts_index = 0; _accounts_index < Accounts.Tables["accounts"].Rows.Count; _accounts_index++)
      {
        _account = Accounts.Tables["accounts"].Rows[_accounts_index];

        _repeatedrows = null;
        _repeatedrows = Accounts.Tables["accounts"].Select("ac_external_reference = '" + _account["ac_external_reference"].ToString() + "'");
        if (_repeatedrows.Length > 1)
        {
          //  Accounts.Tables["duplicate"].Rows.Add(_acount_row[0], Resource.String("STR_AC_IMPORT_REPEATED_FIELD") + "Referencia (" + _acount_row["ac_external_reference"] + ").");
          Accounts.Tables["errors"].Rows.Add(_account["EXCEL_ROW"], Resource.String("STR_AC_IMPORT_REPEATED_FIELD") + GetExcelColName("ac_external_reference"));
          _account.AcceptChanges();
        }
        _repeatedrows = null;
        _repeatedrows = Accounts.Tables["accounts"].Select("ac_holder_id = '" + _account["ac_holder_id"].ToString() + "'"); //check repeated ac_holder_ids on document (only customized accounts)
        if (_repeatedrows.Length > 1)
        {
          if ((int)_account["ac_holder_level"] > 0)
          {
            // Accounts.Tables["duplicate"].Rows.Add(_acount_row[0], Resource.String("STR_AC_IMPORT_REPEATED_FIELD") + "Documento (" + _acount_row["ac_holder_id"] + ").");
            Accounts.Tables["errors"].Rows.Add(_account["EXCEL_ROW"], Resource.String("STR_AC_IMPORT_REPEATED_FIELD") + GetExcelColName("ac_holder_id"));
            _account.AcceptChanges();
          }
        }

        if (OnRowUpdated != null)
        {
          if (_account.RowState != DataRowState.Unchanged)
          {
            _correct_accounts++;
          }
          else
          {
            _error_accounts++;
          }
        }
      }

      AccountImportEventArgs _rowupdate_event;
      _rowupdate_event = new AccountImportEventArgs();
      _rowupdate_event.Message = "\r\n" + Resource.String("STR_AC_IMPORT_PROCESSING") + Resource.String("STR_AC_IMPORT_CORRECT_ACCOUNTS") + _correct_accounts +
        //"\r\n" + Resource.String("STR_AC_IMPORT_INCORRECT_ACCOUNTS") + ((Int32)Accounts.Tables["errors"].Rows.Count - _error_accounts) +
              "\r\n" + Resource.String("STR_AC_IMPORT_INCORRECT_ACCOUNTS") + _error_accounts +
              "\r\n" + Resource.String("STR_AC_IMPORT_ERRORS_FOUND") + (Int32)Accounts.Tables["errors"].Rows.Count;

      _rowupdate_event.State = IMPORT_STATES.DocumentReaded;
      _rowupdate_event.Row_count = m_clients_count;
      _rowupdate_event.Pending_rows = m_pending_clients;

      WriteLog(_rowupdate_event.Message);
      OnRowUpdated(this, _rowupdate_event);

      return true;
    }

    /// <summary>
    /// Closes all the book objects
    /// </summary>
    /// <param name="_excel_book"></param>
    /// <param name="_excel_sheet"></param>
    /// <param name="_excel_range"></param>
    private static void CloseBookObjects(ref SpreadsheetGear.IWorkbook _excel_book, ref SpreadsheetGear.IWorksheet _excel_sheet, ref SpreadsheetGear.IRange _excel_range)
    {
      if (_excel_range != null)
      {
        _excel_range = null;
      }

      if (_excel_sheet != null)
      {
        _excel_sheet = null;
      }

      if (_excel_book != null)
      {
        _excel_book.Close();
        _excel_book = null;
      }
    }

    public Boolean AddAccounts(System.Data.DataTable Accounts,
                               int UpdateBatchSize,
                               bool ByExtRef,
                               bool ImportNewAccounts,
                               bool UpdateClient,
                               bool UpdateLevel,
                               bool ResetBalances,
                               bool UpdateBalances,
                               bool ResetPoints,
                               bool UpdatePoints,
                               String User
                               )
    {
      Boolean _any_updates;
      Boolean _error;
      Boolean _is_center;
      int _t0;

      StringBuilder _sql_insert_query;
      SqlCommand _sql_insert_cmd;
      SqlParameter _parameter;

      //WriteLog("Starting Update:");
      m_user = User;
      m_no_action_accounts = 0;
      _t0 = 0;
      _error = false;

      _any_updates = (UpdateClient || UpdateLevel);

      //Is it MultiSite Center?
      _is_center = GeneralParam.GetBoolean("MultiSite", "IsCenter", false);

      if (Accounts.Rows.Count == 0)
      {
        throw new Exception(Resource.String("STR_AC_IMPORT_NO_ACCOUNTS ")); //There are 0 accounts to add or update, process won't start
      }

      #region insert_query

      _sql_insert_query = new StringBuilder();
      _sql_insert_query.AppendLine(" SET @pRowProcessed = 0 ");

      if (ByExtRef && ImportNewAccounts)
      {
        _sql_insert_query.AppendLine(" IF NOT EXISTS ( ");
        _sql_insert_query.AppendLine("    SELECT 1 ");
        _sql_insert_query.AppendLine("    FROM " + ACCOUNTS_TABLE_NAME);
        _sql_insert_query.AppendLine("    WHERE ac_external_reference = @p_ac_external_reference ");
        _sql_insert_query.AppendLine(" ) ");
        _sql_insert_query.AppendLine(" BEGIN");
        _sql_insert_query.AppendLine(" DECLARE @pAccountId AS BIGINT");
        //Crea la cuenta
        _sql_insert_query.AppendLine(" EXECUTE CreateAccount @pAccountId OUTPUT");
        //Modificamos la cuenta que acabamos de crear
        _sql_insert_query.AppendLine(" UPDATE " + ACCOUNTS_TABLE_NAME);
        _sql_insert_query.AppendLine(" SET AC_TYPE = @p_ac_type ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_NAME = @p_ac_holder_name ");
        _sql_insert_query.AppendLine(" , AC_BLOCKED = @p_ac_blocked");
        _sql_insert_query.AppendLine(" , AC_TRACK_DATA = @p_ac_track_data");
        _sql_insert_query.AppendLine(" , AC_TOTAL_CASH_IN = @p_ac_total_cash_in ");
        _sql_insert_query.AppendLine(" , AC_TOTAL_CASH_OUT = @p_ac_total_cash_out ");
        _sql_insert_query.AppendLine(" , AC_INITIAL_CASH_IN = @p_ac_initial_cash_in ");
        _sql_insert_query.AppendLine(" , AC_ACTIVATED = @p_ac_activated");
        _sql_insert_query.AppendLine(" , AC_DEPOSIT = @p_ac_deposit ");
        _sql_insert_query.AppendLine(" , AC_INITIAL_NOT_REDEEMABLE = @p_ac_initial_not_redeemable ");
        _sql_insert_query.AppendLine(" , AC_CREATED = @p_ac_created ");
        _sql_insert_query.AppendLine(" , AC_PROMO_LIMIT = @p_ac_promo_limit ");
        _sql_insert_query.AppendLine(" , AC_PROMO_CREATION = @p_ac_promo_creation ");
        _sql_insert_query.AppendLine(" , AC_PROMO_EXPIRATION = @p_ac_promo_expiration ");
        _sql_insert_query.AppendLine(" , AC_LAST_ACTIVITY = @p_ac_last_activity ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_ID = @p_ac_holder_id");
        _sql_insert_query.AppendLine(" , AC_HOLDER_ADDRESS_01 = @p_ac_holder_address_01 ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_ADDRESS_02 = @p_ac_holder_address_02 ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_ADDRESS_03 = @p_ac_holder_address_03 ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_CITY = @p_ac_holder_city ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_ZIP = @p_ac_holder_zip ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_EMAIL_01 = @p_ac_holder_email_01 ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_EMAIL_02 = @p_ac_holder_email_02 ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_PHONE_NUMBER_01 = @p_ac_holder_phone_number_01 ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_PHONE_NUMBER_02 = @p_ac_holder_phone_number_02 ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_COMMENTS = @p_ac_holder_comments ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_GENDER = @p_ac_holder_gender ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_MARITAL_STATUS = @p_ac_holder_marital_status ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_BIRTH_DATE = @p_ac_holder_birth_date ");
        _sql_insert_query.AppendLine(" , AC_DRAW_LAST_PLAY_SESSION_ID = @p_ac_draw_last_play_session_id ");
        _sql_insert_query.AppendLine(" , AC_DRAW_LAST_PLAY_SESSION_REMAINDER = @p_ac_draw_last_play_session_remainder ");
        _sql_insert_query.AppendLine(" , AC_NR_WON_LOCK = @p_ac_nr_won_lock ");
        _sql_insert_query.AppendLine(" , AC_NR_EXPIRATION = @p_ac_nr_expiration ");
        _sql_insert_query.AppendLine(" , AC_CASHIN_WHILE_PLAYING = @p_ac_cashin_while_playing ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_LEVEL = @p_ac_holder_level ");
        _sql_insert_query.AppendLine(" , AC_CARD_PAID = @p_ac_card_paid");
        _sql_insert_query.AppendLine(" , AC_CANCELLABLE_OPERATION_ID = @p_ac_cancellable_operation_id ");
        _sql_insert_query.AppendLine(" , AC_CURRENT_PROMOTION_ID = @p_ac_current_promotion_id");
        _sql_insert_query.AppendLine(" , AC_BLOCK_REASON = dbo.BlockReason_ToNewEnumerate(AC_BLOCK_REASON) | @p_ac_block_reason ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_LEVEL_EXPIRATION = @p_ac_holder_level_expiration ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_LEVEL_ENTERED = @p_ac_holder_level_entered ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_LEVEL_NOTIFY = ISNULL(@p_ac_holder_level_notify,0) ");
        _sql_insert_query.AppendLine(" , AC_PIN = @p_ac_pin");
        _sql_insert_query.AppendLine(" , AC_PIN_FAILURES = @p_ac_pin_failures ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_ID1 = @p_ac_holder_id1 ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_ID2 = @p_ac_holder_id2 ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_DOCUMENT_ID1 = @p_ac_holder_document_id1 ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_DOCUMENT_ID2 = @p_ac_holder_document_id2 ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_NAME1 = @p_ac_holder_name1 ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_NAME2 = @p_ac_holder_name2 ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_NAME3 = @p_ac_holder_name3 ");
        _sql_insert_query.AppendLine(" , AC_NR2_EXPIRATION = @p_ac_nr2_expiration ");
        _sql_insert_query.AppendLine(" , AC_RECOMMENDED_BY = @p_ac_recommended_by");
        _sql_insert_query.AppendLine(" , AC_EXTERNAL_REFERENCE = @p_ac_external_reference ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_IS_VIP = @p_ac_holder_is_vip ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_TITLE = @p_ac_holder_title ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_NAME4 = @p_ac_holder_name4 ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_WEDDING_DATE = @p_ac_holder_wedding_date ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_PHONE_TYPE_01 = @p_ac_holder_phone_type_01 ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_PHONE_TYPE_02 = @p_ac_holder_phone_type_02 ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_STATE = @p_ac_holder_state ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_NATIONALITY = @p_ac_holder_birth_country ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_ADDRESS_COUNTRY = @p_ac_holder_address_country ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_BIRTH_COUNTRY = @p_ac_holder_birth_country ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_ADDRESS_01_ALT = @p_ac_holder_address_01_alt ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_ADDRESS_02_ALT = @p_ac_holder_address_02_alt ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_CITY_ALT = @p_ac_holder_city_alt ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_ZIP_ALT = @p_ac_holder_zip_alt ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_STATE_ALT = @p_ac_holder_state_alt ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_COUNTRY_ALT = @p_ac_holder_country_alt ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_IS_SMOKER = @p_ac_holder_is_smoker ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_NICKNAME = @p_ac_holder_nickname ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_CREDIT_LIMIT = @p_ac_holder_credit_limit ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_REQUEST_CREDIT_LIMIT = @p_ac_holder_request_credit_limit ");
        _sql_insert_query.AppendLine(" , AC_LAST_ACTIVITY_SITE_ID = @p_ac_last_activity_site_id ");
        _sql_insert_query.AppendLine(" , AC_PIN_LAST_MODIFIED = @p_ac_pin_last_modified ");
        _sql_insert_query.AppendLine(" , AC_CREATION_SITE_ID = @p_ac_creation_site_id ");
        _sql_insert_query.AppendLine(" , AC_MS_CREATED_ON_SITE_ID = @p_ac_ms_created_on_site_id ");
        _sql_insert_query.AppendLine(" , AC_LAST_UPDATE_SITE_ID = @p_ac_last_update_site_id ");
        _sql_insert_query.AppendLine(" , AC_HOLDER_ID_TYPE = @p_ac_holder_id_type");
        _sql_insert_query.AppendLine(" , AC_USER_TYPE = @p_ac_user_type");
        _sql_insert_query.AppendLine(" WHERE AC_ACCOUNT_ID = @pAccountId");

        if (_is_center)
        {
          _sql_insert_query.AppendLine(" DECLARE @pSeqMovementId AS BIGINT ");

          _sql_insert_query.AppendLine(" UPDATE   SEQUENCES ");
          _sql_insert_query.AppendLine("    SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1 ");
          _sql_insert_query.AppendLine("  WHERE   SEQ_ID         = 12 ");

          _sql_insert_query.AppendLine(" IF @@ROWCOUNT = 0 ");
          _sql_insert_query.AppendLine(" BEGIN ");
          _sql_insert_query.AppendLine("   RAISERROR ('Sequence #12 does not exist!', 20, 0) WITH LOG ");
          _sql_insert_query.AppendLine(" END ");

          _sql_insert_query.AppendLine(" SELECT @pSeqMovementId = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID  = 12 ");
        }

        //Account Movements
        _sql_insert_query.AppendLine(" INSERT INTO ACCOUNT_MOVEMENTS (");
        _sql_insert_query.AppendLine("   AM_ACCOUNT_ID ");
        _sql_insert_query.AppendLine(" , AM_TYPE ");
        _sql_insert_query.AppendLine(" , AM_INITIAL_BALANCE ");
        _sql_insert_query.AppendLine(" , AM_SUB_AMOUNT ");
        _sql_insert_query.AppendLine(" , AM_ADD_AMOUNT ");
        _sql_insert_query.AppendLine(" , AM_FINAL_BALANCE ");
        _sql_insert_query.AppendLine(" , AM_DATETIME ");
        _sql_insert_query.AppendLine(" , AM_CASHIER_NAME ");

        if (_is_center)
        {
          _sql_insert_query.AppendLine(" , AM_SITE_ID ");
          _sql_insert_query.AppendLine(" , AM_MOVEMENT_ID ");
        }

        _sql_insert_query.AppendLine(" ) ");
        _sql_insert_query.AppendLine(" VALUES ( ");
        _sql_insert_query.AppendLine("   @pAccountId ");
        _sql_insert_query.AppendLine(" , 70");
        _sql_insert_query.AppendLine(" , 0");
        _sql_insert_query.AppendLine(" , 0");
        _sql_insert_query.AppendLine(" , 0");
        _sql_insert_query.AppendLine(" , 0");
        _sql_insert_query.AppendLine(" , GETDATE() ");
        _sql_insert_query.AppendLine(" , @cashier_name ");

        if (_is_center)
        {
          _sql_insert_query.AppendLine(" , 0 ");
          _sql_insert_query.AppendLine(" , @pSeqMovementId ");
        }

        _sql_insert_query.AppendLine(" ) ");
        _sql_insert_query.AppendLine(" END ");

        if (_any_updates)
        {
          _sql_insert_query.AppendLine(" ELSE ");  // Else del IF NOT EXISTS inicial
        }
      }  // if (ByExtRef and importnewaccounts

      if (_any_updates)
      {
        _sql_insert_query.AppendLine(" BEGIN ");
        _sql_insert_query.AppendLine(" UPDATE " + ACCOUNTS_TABLE_NAME);
        _sql_insert_query.AppendLine(" SET ac_external_reference = ac_external_reference");

        if (UpdateLevel)
        {
          _sql_insert_query.AppendLine(" , ac_holder_level = @p_ac_holder_level");
          _sql_insert_query.AppendLine(" , ac_holder_level_expiration = @p_ac_holder_level_expiration");
          _sql_insert_query.AppendLine(" , ac_holder_level_entered = @p_ac_holder_level_entered");
          _sql_insert_query.AppendLine(" , ac_user_type = @p_ac_user_type");
        }

        if (UpdateClient)
        {
          _sql_insert_query.AppendLine(" , ac_blocked = @p_ac_blocked");
          _sql_insert_query.AppendLine(" , ac_block_reason = dbo.BlockReason_ToNewEnumerate(ac_block_reason) | @p_ac_block_reason");
          _sql_insert_query.AppendLine(" , ac_holder_is_vip = @p_ac_holder_is_vip ");
          _sql_insert_query.AppendLine(" , ac_holder_title = @p_ac_holder_title ");
          _sql_insert_query.AppendLine(" , ac_holder_name4 = @p_ac_holder_name4 ");
          _sql_insert_query.AppendLine(" , ac_holder_wedding_date = @p_ac_holder_wedding_date ");
          _sql_insert_query.AppendLine(" , ac_holder_phone_type_01 = @p_ac_holder_phone_type_01 ");
          _sql_insert_query.AppendLine(" , ac_holder_phone_type_02 = @p_ac_holder_phone_type_02 ");
          _sql_insert_query.AppendLine(" , ac_holder_state = @p_ac_holder_state ");
          _sql_insert_query.AppendLine(" , ac_holder_nationality = @p_ac_holder_birth_country ");
          _sql_insert_query.AppendLine(" , ac_holder_address_country = @p_ac_holder_address_country ");
          _sql_insert_query.AppendLine(" , ac_holder_birth_country = @p_ac_holder_birth_country ");
          _sql_insert_query.AppendLine(" , ac_holder_address_01_alt = @p_ac_holder_address_01_alt ");
          _sql_insert_query.AppendLine(" , ac_holder_address_02_alt = @p_ac_holder_address_02_alt ");
          _sql_insert_query.AppendLine(" , ac_holder_city_alt = @p_ac_holder_city_alt ");
          _sql_insert_query.AppendLine(" , ac_holder_zip_alt = @p_ac_holder_zip_alt ");
          _sql_insert_query.AppendLine(" , ac_holder_state_alt = @p_ac_holder_state_alt ");
          _sql_insert_query.AppendLine(" , ac_holder_country_alt = @p_ac_holder_country_alt ");
          _sql_insert_query.AppendLine(" , ac_holder_is_smoker = @p_ac_holder_is_smoker ");
          _sql_insert_query.AppendLine(" , ac_holder_nickname = @p_ac_holder_nickname ");
          _sql_insert_query.AppendLine(" , ac_holder_credit_limit = @p_ac_holder_credit_limit ");
          _sql_insert_query.AppendLine(" , ac_holder_request_credit_limit = @p_ac_holder_request_credit_limit ");
          _sql_insert_query.AppendLine(" , ac_last_activity_site_id = @p_ac_last_activity_site_id ");
          _sql_insert_query.AppendLine(" , ac_pin_last_modified = @p_ac_pin_last_modified ");
          _sql_insert_query.AppendLine(" , ac_creation_site_id = @p_ac_creation_site_id ");
          _sql_insert_query.AppendLine(" , ac_ms_created_on_site_id = @p_ac_ms_created_on_site_id ");
          _sql_insert_query.AppendLine(" , ac_last_update_site_id = @p_ac_last_update_site_id ");
          _sql_insert_query.AppendLine(" , ac_pin = @p_ac_pin ");

          _sql_insert_query.AppendLine(" , ac_holder_name = @p_ac_holder_name");
          _sql_insert_query.AppendLine(" , ac_holder_id = @p_ac_holder_id");
          _sql_insert_query.AppendLine(" , ac_holder_address_01 = @p_ac_holder_address_01");
          _sql_insert_query.AppendLine(" , ac_holder_address_02 = @p_ac_holder_address_02");
          _sql_insert_query.AppendLine(" , ac_holder_address_03 = @p_ac_holder_address_03");
          _sql_insert_query.AppendLine(" , ac_holder_city = @p_ac_holder_city");
          _sql_insert_query.AppendLine(" , ac_holder_zip = @p_ac_holder_zip");
          _sql_insert_query.AppendLine(" , ac_holder_email_01 = @p_ac_holder_email_01");
          _sql_insert_query.AppendLine(" , ac_holder_email_02 = @p_ac_holder_email_02");
          _sql_insert_query.AppendLine(" , ac_holder_phone_number_01 = @p_ac_holder_phone_number_01");
          _sql_insert_query.AppendLine(" , ac_holder_phone_number_02 = @p_ac_holder_phone_number_02");
          _sql_insert_query.AppendLine(" , ac_holder_comments = @p_ac_holder_comments");
          _sql_insert_query.AppendLine(" , ac_holder_gender = @p_ac_holder_gender");
          _sql_insert_query.AppendLine(" , ac_holder_marital_status = @p_ac_holder_marital_status");
          _sql_insert_query.AppendLine(" , ac_holder_birth_date = @p_ac_holder_birth_date");
          _sql_insert_query.AppendLine(" , ac_holder_id1 = @p_ac_holder_id1");
          _sql_insert_query.AppendLine(" , ac_holder_id2 = @p_ac_holder_id2");
          _sql_insert_query.AppendLine(" , ac_holder_document_id1 = @p_ac_holder_document_id1");
          _sql_insert_query.AppendLine(" , ac_holder_document_id2 = @p_ac_holder_document_id2");
          _sql_insert_query.AppendLine(" , ac_holder_name1 = @p_ac_holder_name1");
          _sql_insert_query.AppendLine(" , ac_holder_name2 = @p_ac_holder_name2");
          _sql_insert_query.AppendLine(" , ac_holder_name3 = @p_ac_holder_name3");
          _sql_insert_query.AppendLine(" , ac_holder_level_notify = ISNULL(@p_ac_holder_level_notify,0)");
          _sql_insert_query.AppendLine(" , ac_holder_id_type = @p_ac_holder_id_type");
        }

        if (ByExtRef)
        {
          _sql_insert_query.AppendLine(" WHERE ac_external_reference = @p_ac_external_reference");
        }
        else // by ac_account_id
        {
          _sql_insert_query.AppendLine(" WHERE ac_account_id = @p_ac_external_reference");
        }

        _sql_insert_query.AppendLine(" END ");
      } // end if (_any_updates) 
      else
      {
        // do nothing 
      }

      _sql_insert_query.AppendLine(" SET @pRowProcessed = 1 ");

      # endregion

      # region insert_params
      _sql_insert_cmd = new SqlCommand(_sql_insert_query.ToString());
      _sql_insert_cmd.Parameters.Add("@p_ac_type", SqlDbType.Int).SourceColumn = "ac_type";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_name", SqlDbType.NVarChar).SourceColumn = "ac_holder_name";
      _sql_insert_cmd.Parameters.Add("@p_ac_blocked", SqlDbType.Bit).SourceColumn = "ac_blocked";
      _sql_insert_cmd.Parameters.Add("@p_ac_not_valid_before", SqlDbType.DateTime).SourceColumn = "ac_not_valid_before";
      _sql_insert_cmd.Parameters.Add("@p_ac_not_valid_after", SqlDbType.DateTime).SourceColumn = "ac_not_valid_after";
      _sql_insert_cmd.Parameters.Add("@p_ac_balance", SqlDbType.Money).SourceColumn = "ac_balance";
      _sql_insert_cmd.Parameters.Add("@p_ac_cash_in", SqlDbType.Money).SourceColumn = "ac_cash_in";
      _sql_insert_cmd.Parameters.Add("@p_ac_cash_won", SqlDbType.Money).SourceColumn = "ac_cash_won";
      _sql_insert_cmd.Parameters.Add("@p_ac_not_redeemable", SqlDbType.Money).SourceColumn = "ac_not_redeemable";
      _sql_insert_cmd.Parameters.Add("@p_ac_track_data", SqlDbType.NVarChar).SourceColumn = "ac_track_data";
      _sql_insert_cmd.Parameters.Add("@p_ac_total_cash_in", SqlDbType.Money).SourceColumn = "ac_total_cash_in";
      _sql_insert_cmd.Parameters.Add("@p_ac_total_cash_out", SqlDbType.Money).SourceColumn = "ac_total_cash_out";
      _sql_insert_cmd.Parameters.Add("@p_ac_initial_cash_in", SqlDbType.Money).SourceColumn = "ac_initial_cash_in";
      _sql_insert_cmd.Parameters.Add("@p_ac_activated", SqlDbType.Bit).SourceColumn = "ac_activated";
      _sql_insert_cmd.Parameters.Add("@p_ac_deposit", SqlDbType.Money).SourceColumn = "ac_deposit";
      _sql_insert_cmd.Parameters.Add("@p_ac_current_terminal_id", SqlDbType.Int).SourceColumn = "ac_current_terminal_id";
      _sql_insert_cmd.Parameters.Add("@p_ac_current_terminal_name", SqlDbType.NVarChar).SourceColumn = "ac_current_terminal_name";
      _sql_insert_cmd.Parameters.Add("@p_ac_current_play_session_id", SqlDbType.BigInt).SourceColumn = "ac_current_play_session_id";
      _sql_insert_cmd.Parameters.Add("@p_ac_last_terminal_id", SqlDbType.Int).SourceColumn = "ac_last_terminal_id";
      _sql_insert_cmd.Parameters.Add("@p_ac_last_terminal_name", SqlDbType.NVarChar).SourceColumn = "ac_last_terminal_name";
      _sql_insert_cmd.Parameters.Add("@p_ac_last_play_session_id", SqlDbType.BigInt).SourceColumn = "ac_last_play_session_id";
      _sql_insert_cmd.Parameters.Add("@p_ac_user_type", SqlDbType.Int).SourceColumn = "ac_user_type";
      _sql_insert_cmd.Parameters.Add("@p_ac_initial_not_redeemable", SqlDbType.Money).SourceColumn = "ac_initial_not_redeemable";
      _sql_insert_cmd.Parameters.Add("@p_ac_created", SqlDbType.DateTime).SourceColumn = "ac_created";
      _sql_insert_cmd.Parameters.Add("@p_ac_promo_balance", SqlDbType.Money).SourceColumn = "ac_promo_balance";
      _sql_insert_cmd.Parameters.Add("@p_ac_promo_limit", SqlDbType.Money).SourceColumn = "ac_promo_limit";
      _sql_insert_cmd.Parameters.Add("@p_ac_promo_creation", SqlDbType.DateTime).SourceColumn = "ac_promo_creation";
      _sql_insert_cmd.Parameters.Add("@p_ac_promo_expiration", SqlDbType.DateTime).SourceColumn = "ac_promo_expiration";
      _sql_insert_cmd.Parameters.Add("@p_ac_last_activity", SqlDbType.DateTime).SourceColumn = "ac_last_activity";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_id", SqlDbType.NVarChar).SourceColumn = "ac_holder_id";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_address_01", SqlDbType.NVarChar).SourceColumn = "ac_holder_address_01";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_address_02", SqlDbType.NVarChar).SourceColumn = "ac_holder_address_02";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_address_03", SqlDbType.NVarChar).SourceColumn = "ac_holder_address_03";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_city", SqlDbType.NVarChar).SourceColumn = "ac_holder_city";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_zip", SqlDbType.NVarChar).SourceColumn = "ac_holder_zip";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_email_01", SqlDbType.NVarChar).SourceColumn = "ac_holder_email_01";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_email_02", SqlDbType.NVarChar).SourceColumn = "ac_holder_email_02";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_phone_number_01", SqlDbType.NVarChar).SourceColumn = "ac_holder_phone_number_01";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_phone_number_02", SqlDbType.NVarChar).SourceColumn = "ac_holder_phone_number_02";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_comments", SqlDbType.NVarChar).SourceColumn = "ac_holder_comments";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_gender", SqlDbType.Int).SourceColumn = "ac_holder_gender";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_marital_status", SqlDbType.Int).SourceColumn = "ac_holder_marital_status";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_birth_date", SqlDbType.DateTime).SourceColumn = "ac_holder_birth_date";
      _sql_insert_cmd.Parameters.Add("@p_ac_draw_last_play_session_id", SqlDbType.BigInt).SourceColumn = "ac_draw_last_play_session_id";
      _sql_insert_cmd.Parameters.Add("@p_ac_draw_last_play_session_remainder", SqlDbType.Money).SourceColumn = "ac_draw_last_play_session_remainder";
      _sql_insert_cmd.Parameters.Add("@p_ac_nr_won_lock", SqlDbType.Money).SourceColumn = "ac_nr_won_lock";
      _sql_insert_cmd.Parameters.Add("@p_ac_nr_expiration", SqlDbType.DateTime).SourceColumn = "ac_nr_expiration";
      _sql_insert_cmd.Parameters.Add("@p_ac_cashin_while_playing", SqlDbType.Money).SourceColumn = "ac_cashin_while_playing";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_level", SqlDbType.Int).SourceColumn = "ac_holder_level";
      _sql_insert_cmd.Parameters.Add("@p_ac_card_paid", SqlDbType.Bit).SourceColumn = "ac_card_paid";
      _sql_insert_cmd.Parameters.Add("@p_ac_cancellable_operation_id", SqlDbType.BigInt).SourceColumn = "ac_cancellable_operation_id";
      _sql_insert_cmd.Parameters.Add("@p_ac_current_promotion_id", SqlDbType.BigInt).SourceColumn = "ac_current_promotion_id";
      _sql_insert_cmd.Parameters.Add("@p_ac_block_reason", SqlDbType.Int).SourceColumn = "ac_block_reason";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_level_expiration", SqlDbType.DateTime).SourceColumn = "ac_holder_level_expiration";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_level_entered", SqlDbType.DateTime).SourceColumn = "ac_holder_level_entered";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_level_notify", SqlDbType.Int).SourceColumn = "ac_holder_level_notify";
      _sql_insert_cmd.Parameters.Add("@p_ac_pin_failures", SqlDbType.Int).SourceColumn = "ac_pin_failures";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_id1", SqlDbType.NVarChar).SourceColumn = "ac_holder_id1";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_id2", SqlDbType.NVarChar).SourceColumn = "ac_holder_id2";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_document_id1", SqlDbType.BigInt).SourceColumn = "ac_holder_document_id1";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_document_id2", SqlDbType.BigInt).SourceColumn = "ac_holder_document_id2";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_name1", SqlDbType.NVarChar).SourceColumn = "ac_holder_name1";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_name2", SqlDbType.NVarChar).SourceColumn = "ac_holder_name2";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_name3", SqlDbType.NVarChar).SourceColumn = "ac_holder_name3";
      _sql_insert_cmd.Parameters.Add("@p_ac_nr2_expiration", SqlDbType.DateTime).SourceColumn = "ac_nr2_expiration";
      _sql_insert_cmd.Parameters.Add("@p_ac_recommended_by", SqlDbType.BigInt).SourceColumn = "ac_recommended_by";
      _sql_insert_cmd.Parameters.Add("@p_ac_external_reference", SqlDbType.NVarChar).SourceColumn = "ac_external_reference";

      // RXM addded 28-SEP-2012
      _sql_insert_cmd.Parameters.Add("@p_ac_pin", SqlDbType.NVarChar).SourceColumn = "ac_pin";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_is_vip", SqlDbType.Bit).SourceColumn = "ac_holder_is_vip";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_title", SqlDbType.NVarChar).SourceColumn = "ac_holder_title";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_name4", SqlDbType.NVarChar).SourceColumn = "ac_holder_name4";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_wedding_date", SqlDbType.DateTime).SourceColumn = "ac_holder_wedding_date";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_phone_type_01", SqlDbType.Int).SourceColumn = "ac_holder_phone_type_01";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_phone_type_02", SqlDbType.Int).SourceColumn = "ac_holder_phone_type_02";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_state", SqlDbType.NVarChar).SourceColumn = "ac_holder_state";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_address_country", SqlDbType.Int).SourceColumn = "ac_holder_address_country";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_birth_country", SqlDbType.NVarChar).SourceColumn = "ac_holder_birth_country";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_address_01_alt", SqlDbType.NVarChar).SourceColumn = "ac_holder_address_01_alt";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_address_02_alt", SqlDbType.NVarChar).SourceColumn = "ac_holder_address_02_alt";

      _sql_insert_cmd.Parameters.Add("@p_ac_holder_city_alt", SqlDbType.NVarChar).SourceColumn = "ac_holder_city_alt";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_zip_alt", SqlDbType.NVarChar).SourceColumn = "ac_holder_zip_alt";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_state_alt", SqlDbType.NVarChar).SourceColumn = "ac_holder_state_alt";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_country_alt", SqlDbType.NVarChar).SourceColumn = "ac_holder_country_alt";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_is_smoker", SqlDbType.Bit).SourceColumn = "ac_holder_is_smoker";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_nickname", SqlDbType.NVarChar).SourceColumn = "ac_holder_nickname";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_credit_limit", SqlDbType.Money).SourceColumn = "ac_holder_credit_limit";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_request_credit_limit", SqlDbType.Money).SourceColumn = "ac_holder_request_credit_limit";
      _sql_insert_cmd.Parameters.Add("@p_ac_last_activity_site_id", SqlDbType.NVarChar).SourceColumn = "ac_last_activity_site_id";
      _sql_insert_cmd.Parameters.Add("@p_ac_pin_last_modified", SqlDbType.DateTime).SourceColumn = "ac_pin_last_modified";
      _sql_insert_cmd.Parameters.Add("@p_ac_creation_site_id", SqlDbType.NVarChar).SourceColumn = "ac_creation_site_id";

      if (_is_center)
      {
        _sql_insert_cmd.Parameters.Add("@p_ac_ms_created_on_site_id", SqlDbType.Int).SourceColumn = "ac_ms_created_on_site_id";
      }
      else
      {
        if ((GeneralParam.GetInt32("Site", "Identifier", 0)) != 0)
        {
          _sql_insert_cmd.Parameters.Add("@p_ac_ms_created_on_site_id", SqlDbType.Int).Value = GeneralParam.GetInt32("Site", "Identifier");
        }
        else
        {
          _sql_insert_cmd.Parameters.Add("@p_ac_ms_created_on_site_id", SqlDbType.Int).SourceColumn = "ac_ms_created_on_site_id";
        }
      }

      _sql_insert_cmd.Parameters.Add("@p_ac_last_update_site_id", SqlDbType.NVarChar).SourceColumn = "ac_last_update_site_id";
      _sql_insert_cmd.Parameters.Add("@p_ac_holder_id_type", SqlDbType.NVarChar).SourceColumn = "ac_holder_id_type";
      _sql_insert_cmd.Parameters.Add("@cashier_name", SqlDbType.NVarChar).Value = m_user;

      _parameter = _sql_insert_cmd.Parameters.Add("@pRowProcessed", SqlDbType.Bit);
      _parameter.SourceColumn = "ROW_PROCESSED";
      _parameter.Direction = ParameterDirection.Output;

      # endregion

      Int32 _correct_accounts;

      _correct_accounts = 0;

      foreach (DataRow _account_row in Accounts.Rows)
      {
        if (_account_row.RowState != DataRowState.Unchanged)
        {
          _correct_accounts++;
        }
      }

      m_clients_count = _correct_accounts;
      m_pending_clients = _correct_accounts;

      StringBuilder _sb_select;

      _sb_select = new StringBuilder();

      if ((_any_updates) || ByExtRef)
      {
        try
        {
          using (SqlConnection _conn = WGDB.Connection())
          {
            _sb_select.Append("SELECT ");
            _sb_select.Append("   ac_external_reference ");
            _sb_select.Append(" , ac_holder_id ");

            if (_any_updates)
            {
              // ACCOUNT_FIELDS
              _sb_select.Append(" , ac_holder_name ");
              _sb_select.Append(" , ac_blocked ");
              _sb_select.Append(" , ac_holder_address_01 ");
              _sb_select.Append(" , ac_holder_address_02 ");
              _sb_select.Append(" , ac_holder_address_03 ");
              _sb_select.Append(" , ac_holder_city ");
              _sb_select.Append(" , ac_holder_zip ");
              _sb_select.Append(" , ac_holder_email_01 ");
              _sb_select.Append(" , ac_holder_email_02 ");
              _sb_select.Append(" , ac_holder_phone_number_01 ");
              _sb_select.Append(" , ac_holder_phone_number_02 ");
              _sb_select.Append(" , ac_holder_comments ");
              _sb_select.Append(" , ac_holder_gender ");
              _sb_select.Append(" , ac_holder_marital_status ");
              _sb_select.Append(" , ac_holder_birth_date ");
              _sb_select.Append(" , ac_holder_level_notify ");
              _sb_select.Append(" , ac_pin ");
              _sb_select.Append(" , ac_holder_id1 ");
              _sb_select.Append(" , ac_holder_id2 ");
              _sb_select.Append(" , ac_holder_document_id1 ");
              _sb_select.Append(" , ac_holder_document_id2 ");
              _sb_select.Append(" , ac_holder_name1 ");
              _sb_select.Append(" , ac_holder_name2 ");
              _sb_select.Append(" , ac_holder_name3 ");
              _sb_select.Append(" , ac_holder_id_type ");
              _sb_select.Append(" , ac_holder_is_vip ");
              _sb_select.Append(" , ac_holder_title ");
              _sb_select.Append(" , ac_holder_name4 ");
              _sb_select.Append(" , ac_holder_wedding_date ");
              _sb_select.Append(" , ac_holder_phone_type_01 ");
              _sb_select.Append(" , ac_holder_phone_type_02 ");
              _sb_select.Append(" , ac_holder_state ");
              _sb_select.Append(" , ac_holder_nationality ");
              _sb_select.Append(" , ac_holder_address_country ");
              _sb_select.Append(" , ac_holder_birth_country ");
              _sb_select.Append(" , ac_holder_address_01_alt ");
              _sb_select.Append(" , ac_holder_address_02_alt ");
              _sb_select.Append(" , ac_holder_address_03_alt ");
              _sb_select.Append(" , ac_holder_city_alt ");
              _sb_select.Append(" , ac_holder_zip_alt ");
              _sb_select.Append(" , ac_holder_state_alt ");
              _sb_select.Append(" , ac_holder_country_alt ");
              _sb_select.Append(" , ac_holder_is_smoker ");
              _sb_select.Append(" , ac_holder_nickname ");
              _sb_select.Append(" , ac_holder_credit_limit ");
              _sb_select.Append(" , ac_holder_request_credit_limit ");
              _sb_select.Append(" , ac_pin_last_modified ");
              _sb_select.Append(" , ac_last_activity_site_id ");
              _sb_select.Append(" , ac_creation_site_id ");
              _sb_select.Append(" , ac_last_update_site_id ");
              // LEVEL_FIELDS
              _sb_select.Append(" , ac_type ");
              _sb_select.Append(" , ac_holder_level ");
              _sb_select.Append(" , ac_holder_level_expiration ");
              _sb_select.Append(" , ac_holder_level_entered ");
            }

            _sb_select.AppendLine("FROM " + ACCOUNTS_TABLE_NAME);

            using (SqlCommand _select_cmd = new SqlCommand(_sb_select.ToString(), _conn))
            {
              using (SqlDataAdapter _da = new SqlDataAdapter(_select_cmd))
              {
                DataSet _ds_accounts;
                _ds_accounts = new DataSet();
                _sql_insert_cmd.CommandTimeout = 60;
                _da.InsertCommand = WGDB.DownsizeCommandText(_sql_insert_cmd);
                _da.InsertCommand.Connection = _conn;
                _da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;

                _t0 = Environment.TickCount;

                _da.UpdateBatchSize = UpdateBatchSize;
                _da.ContinueUpdateOnError = false;

                if (OnRowUpdated != null)
                {
                  _da.RowUpdated += new SqlRowUpdatedEventHandler(AccountsRowUpdated);
                }

                GC.Collect();
                ThreadPool.QueueUserWorkItem(delegate
                {
                  GC.WaitForPendingFinalizers();
                  GC.Collect();
                });

                _da.Fill(_ds_accounts);

                if (!ImportNewAccounts && ByExtRef)
                {
                  AvoidNewAccounts(_ds_accounts.Tables[0], Accounts);
                }

                if (!_any_updates)
                {
                  if (ByExtRef)
                  {
                    CheckExistingAccountsByExtRef(_ds_accounts.Tables[0], Accounts, ImportNewAccounts);
                  }
                }
                else // 'some updates'
                {
                  if (ByExtRef) // avoid work with no change accounts
                  {
                    CompareAccountsByExtRef(_ds_accounts.Tables[0], Accounts, UpdatePoints, UpdateClient, UpdateLevel, ImportNewAccounts);
                  }
                  else // by account_id --> avoid work with no change or non existing accounts
                  {
                    CompareAccountsByID(_ds_accounts.Tables[0], Accounts, UpdatePoints, UpdateClient, UpdateLevel, ImportNewAccounts);
                  }
                }

                if (ByExtRef && ImportNewAccounts) // check for repeated ac_holder_ids... should stop process...
                {
                  CheckRepeatedHolder_Id(_ds_accounts.Tables[0], Accounts, out _error);
                }

                if (!_error)
                {
                  m_batch_tick0 = Environment.TickCount; // set tick0 to get first interval delay
                  m_process_tick0 = m_batch_tick0;
                  m_retry = 0;

                  while (true)
                  {
                    try
                    {
                      _da.Update(Accounts);
                    }
                    catch (SqlException ex)
                    {
                      // Timeout
                      if (ex.Number == -2)
                      {
                        m_retry += 1;
                        if (m_retry % NUMBER_RETRIES_BATCH_DECREASE == 0)
                        {
                          _da.UpdateBatchSize = Math.Max(30, Convert.ToInt32(_da.UpdateBatchSize * (1 - DECREASE_VALUE_BATCH)));
                        }
                        System.Threading.Thread.Sleep(1000);

                        continue;
                      }
                      else
                      {
                        WriteLog(ex.Message);
                      }
                    }
                    catch (Exception ex)
                    {
                      WriteLog(ex.Message);
                    }
                    break;
                  }
                }
                else // ERROR
                {
                  return false;
                }
              }
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        // WriteLog(Resource.String("STR_AC_IMPORT_READ_DONE")); lectura finalizada

        // WriteLog("\r\n" + Resource.String("STR_AC_IMPORT_SEPARATOR"));
      }
      else
      {
      }// endif ((_someupdates) && ByExtRef)

      if ((ResetBalances || UpdateBalances || ResetPoints || UpdatePoints) && !_error)
      {
        if (!AddBalances(Accounts, ByExtRef, ResetBalances, UpdateBalances, ResetPoints, UpdatePoints))
        {
          return false;
        }
      }

      return true;
    }

    public Boolean AddBalances(System.Data.DataTable Accounts, bool ByExtRef, bool ResetBalances, bool UpdateBalances, bool ResetPoints, bool UpdatePoints)
    {
      int _updated_balance_count;
      int _non_existing_accounts;
      int _ms_x_balance_interval;
      int _ms_x_balance_interval0;

      System.Data.DataTable _db_accounts;
      System.Data.DataTable _db_promotions;
      System.Data.DataTable _account_promotions;

      _non_existing_accounts = 0;
      AccountPromotion _nr_accountpromotion;
      AccountPromotion _re_accountpromotion;
      StringBuilder _sb;
      MultiPromos.AccountBalance _account_balance;
      Decimal _points;
      long _old_id;
      Int32 _old_holder_level;
      Decimal _old_points;
      Decimal _new_points;
      Decimal _old_re_balance;
      Decimal _old_promo_re_balance;
      Decimal _old_promo_nr_balance;
      Decimal _new_re_balance;
      Decimal _new_promo_re_balance;
      Decimal _new_promo_nr_balance;
      //Int64 _movement_id;
      Boolean _is_center;
      StringBuilder _sb_select;

      CashierSessionInfo _cashier_info;
      AccountMovementsTable _account_mov_table;

      try
      {
        _is_center = GeneralParam.GetBoolean("MultiSite", "IsCenter", false);

        if (!_is_center)
        {
          // add promotions if don't exist
          if (!AddPromotion(PROMO_NR_NAME, ACCOUNT_PROMO_CREDIT_TYPE.NR1) || !AddPromotion(PROMO_RE_NAME, ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE))
          {
            throw new Exception("Error while checking promotions");
          }
        }

        _sb = new StringBuilder();

        DataRow _nr_promotion;
        DataRow _re_promotion;

        _db_accounts = new System.Data.DataTable();
        GC.Collect();
        ThreadPool.QueueUserWorkItem(delegate
        {
          GC.WaitForPendingFinalizers();
          GC.Collect();
        });

        // get existing accounts
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _sb_select = new StringBuilder();
          _sb_select.AppendLine(" SELECT ac_external_reference ");
          _sb_select.AppendLine("      , ac_account_id ");
          _sb_select.AppendLine("      , ac_holder_level ");
          _sb_select.AppendLine("      , ac_re_balance ");
          _sb_select.AppendLine("      , ac_promo_re_balance ");
          _sb_select.AppendLine("      , ac_promo_nr_balance ");
          _sb_select.AppendLine("      , ISNULL(cbu_value, 0) as cbu_value ");

          _sb_select.AppendLine(" FROM " + ACCOUNTS_TABLE_NAME);

          _sb_select.AppendLine(" LEFT JOIN CUSTOMER_BUCKET ON (AC_ACCOUNT_ID = CBU_CUSTOMER_ID) AND (CBU_BUCKET_ID = @pBucketId_PT) ");

          using (SqlCommand _cmd = new SqlCommand(_sb_select.ToString()))
          {
            _cmd.Parameters.Add("@pBucketId_PT", SqlDbType.Int).Value = Buckets.BucketId.RedemptionPoints;

            using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
            {
              _db_trx.Fill(_da, _db_accounts);
            }
          }
        }

        if (!_is_center)
        {
          // get related promotions
          using (DB_TRX _db_trx = new DB_TRX())
          {
            _sb.Length = 0;
            _sb.AppendLine("SELECT   PM_PROMOTION_ID, PM_NAME    ");
            _sb.AppendLine("  FROM   PROMOTIONS                  ");
            _sb.AppendLine("  WHERE  (PM_NAME = @p_PROMO_NR_NAME ");
            _sb.AppendLine("      AND PM_CREDIT_TYPE = 1        )");
            _sb.AppendLine("  OR     (PM_NAME = @p_PROMO_RE_NAME ");
            _sb.AppendLine("      AND PM_CREDIT_TYPE = 3        )");

            using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
            {
              _cmd.Parameters.Add("@p_PROMO_NR_NAME", SqlDbType.NVarChar).Value = PROMO_NR_NAME;
              _cmd.Parameters.Add("@p_PROMO_RE_NAME", SqlDbType.NVarChar).Value = PROMO_RE_NAME;

              using (SqlDataAdapter _da = new SqlDataAdapter(_cmd))
              {
                _db_promotions = new System.Data.DataTable();
                _db_trx.Fill(_da, _db_promotions);
              }
            }

            _nr_promotion = _db_promotions.Select("PM_NAME = '" + PROMO_NR_NAME + "'")[0];
            _re_promotion = _db_promotions.Select("PM_NAME = '" + PROMO_RE_NAME + "'")[0];

            _nr_accountpromotion = new AccountPromotion();
            _re_accountpromotion = new AccountPromotion();

            Promotion.ReadPromotionData(_db_trx.SqlTransaction, (long)_nr_promotion["PM_PROMOTION_ID"], _nr_accountpromotion);
            Promotion.ReadPromotionData(_db_trx.SqlTransaction, (long)_re_promotion["PM_PROMOTION_ID"], _re_accountpromotion);
          }
        } // !_is_center
        else
        {
          _re_accountpromotion = new AccountPromotion();
          _nr_accountpromotion = new AccountPromotion();
        }

        _updated_balance_count = 0;

        _ms_x_balance_interval0 = Environment.TickCount;

        _cashier_info = new CashierSessionInfo();
        _cashier_info.TerminalName = m_user;
        _account_mov_table = new AccountMovementsTable(_cashier_info);

        foreach (DataRow _account_new in Accounts.Rows)
        {
          try
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              DataRow[] _account_old;

              if (ByExtRef)
              {
                _account_old = _db_accounts.Select("ac_external_reference = '" + _account_new["ac_external_reference"].ToString() + "'");
              }
              else
              {
                try
                {
                  _account_old = _db_accounts.Select("ac_account_id = '" + _account_new["ac_external_reference"].ToString() + "'");
                }
                catch
                {
                  // format exception.. account_id must be int64
                  _account_old = new DataRow[0];
                  // treat as if this account doesn't exist
                }
              }

              if (_account_old.Length > 0)
              {
                _old_id = (long)_account_old[0]["ac_account_id"];
                _old_holder_level = (int)_account_old[0]["ac_holder_level"];
                _old_points = (decimal)_account_old[0]["cbu_value"];

                _old_re_balance = (Decimal)_account_old[0]["ac_re_balance"];
                _old_promo_re_balance = (Decimal)_account_old[0]["ac_promo_re_balance"];
                _old_promo_nr_balance = (Decimal)_account_old[0]["ac_promo_nr_balance"];

                _new_re_balance = (Decimal)_account_new["ac_re_balance"];
                _new_promo_re_balance = (Decimal)_account_new["ac_promo_re_balance"];
                _new_promo_nr_balance = (Decimal)_account_new["ac_promo_nr_balance"];

                if ((Int32)_account_new["ac_holder_level"] > 0)
                {
                  _new_points = (decimal)_account_new["cbu_value"];
                }
                else
                {
                  _new_points = 0;
                }

                if (ResetPoints && (_old_holder_level > 0))
                {
                  // pon puntos a 0
                  if (_old_points != 0)
                  {
                    _account_balance = new MultiPromos.AccountBalance(0, 0, 0);
                    MultiPromos.Trx_UpdateAccountBalance(_old_id, _account_balance, (_old_points * -1), out _account_balance, out _points, _db_trx.SqlTransaction);

                    _account_mov_table.Add(0, _old_id, MovementType.ImportedPointsOnlyForRedeem, _old_points, _old_points, 0, 0);
                  }
                }

                if (!_is_center)
                {
                  if (ResetBalances)
                  {
                    // pon balances a 0
                    _account_balance = new MultiPromos.AccountBalance(_old_re_balance * -1, _old_promo_re_balance * -1, _old_promo_nr_balance * -1);
                    MultiPromos.Trx_UpdateAccountBalance(_old_id, _account_balance, 0, out _account_balance, out _points, _db_trx.SqlTransaction);

                    // borra account promotions
                    _sb.Length = 0;
                    _sb.AppendLine("DELETE FROM  ACCOUNT_PROMOTIONS ");
                    _sb.AppendLine("WHERE   ACP_ACCOUNT_ID = @pAccountId ");
                    _sb.AppendLine("  AND ( ACP_PROMO_ID = @pPromoReId ");
                    _sb.AppendLine("  OR    ACP_PROMO_ID = @pPromoNrId ) ");

                    using (SqlCommand _cmd = new SqlCommand(_sb.ToString()))
                    {
                      _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = _old_id;
                      _cmd.Parameters.Add("@pPromoReId", SqlDbType.BigInt).Value = _re_accountpromotion.id;
                      _cmd.Parameters.Add("@pPromoNrId", SqlDbType.BigInt).Value = _nr_accountpromotion.id;

                      _db_trx.ExecuteNonQuery(_cmd);
                    }
                  }
                } // !_is_center

                // add points
                if (UpdatePoints && _old_holder_level > 0)
                {
                  // JPJ & DDM 26-AUG-2013: Defect WIG-160
                  if (!MultiPromos.Trx_UpdateAccountBalance(_old_id, MultiPromos.AccountBalance.Zero,
                                                            _new_points, out _account_balance, out _points, _db_trx.SqlTransaction))
                  {
                    throw new Exception();
                  }

                  _account_mov_table.Add(0, _old_id, MovementType.ImportedPointsOnlyForRedeem, (ResetPoints) ? 0 : _old_points, 0, _new_points, _points);
                }

                if (!_is_center)
                {
                  // add balance
                  if (UpdateBalances)
                  {
                    _account_balance = new MultiPromos.AccountBalance(_new_re_balance, 0, 0);
                    if (!MultiPromos.Trx_UpdateAccountBalance(_old_id, _account_balance,
                                                              0, out _account_balance,
                                                              out _points, _db_trx.SqlTransaction))
                    {
                      throw new Exception();
                    }

                    // add promotions
                    _account_promotions = AccountPromotion.CreateAccountPromotionTable();
                    if (!AccountPromotion.AddPromotionToAccount(0, _old_id,
                                                                _old_holder_level,
                                                                0, 0, true, _new_promo_re_balance,
                                                                _re_accountpromotion, _account_promotions))
                    {
                      throw new Exception();
                    }
                    if (!AccountPromotion.AddPromotionToAccount(0, _old_id,
                                                                _old_holder_level,
                                                                0, 0, true, _new_promo_nr_balance,
                                                                _nr_accountpromotion, _account_promotions))
                    {
                      throw new Exception();
                    }

                    if (!AccountPromotion.InsertAccountPromotions(_account_promotions, false, _db_trx.SqlTransaction))
                    {
                      throw new Exception();
                    }

                    if (!AccountPromotion.Trx_ActivatePromotionsOnAccount(_account_promotions, null, null, _db_trx.SqlTransaction))
                    {
                      throw new Exception();
                    }
                  }
                }
              }
              else //  if (_db_account_old.Length > 0)  //// la cuenta no existe ////
              {
                _non_existing_accounts++;
                _account_new.SetColumnError("ac_external_reference", "Not Exist");
                //  WriteLog(Resource.String("STR_AC_IMPORT_BALANCE_NOT_PROCESED") + _account_new["ac_external_reference"]);
              } //end if (_db_account_old.Length > 0)

              if (!_account_mov_table.Save(_db_trx.SqlTransaction))
              {
                throw new Exception();
              }

              _db_trx.Commit();

              _account_mov_table.Clear();

            } // end using (DB_TRX _db_trx = new DB_TRX())

            if (OnRowUpdated != null) // call event
            {
              _updated_balance_count++;

              if ((_updated_balance_count == Accounts.Rows.Count) || (_updated_balance_count % 200 == 0))
              {
                AccountImportEventArgs _rowupdateEvent;
                _ms_x_balance_interval = Environment.TickCount - _ms_x_balance_interval0;
                _ms_x_balance_interval0 = Environment.TickCount;
                _rowupdateEvent = new AccountImportEventArgs();

                _rowupdateEvent.Message = "\r\n\r\n-- " + DateTime.Now + " --" +
                    "\r\n" + Resource.String("STR_AC_IMPORT_BALANCE_PROCESED") + (_updated_balance_count - _non_existing_accounts) +
                    "\r\n" + Resource.String("STR_AC_IMPORT_ELAPSED") + FormatTimeSpan(new TimeSpan(((Int64)_ms_x_balance_interval) * 10000)) +
                    "\r\n" + Resource.String("STR_AC_IMPORT_ACCOUNTS_LEFT") + (Accounts.Rows.Count - _updated_balance_count) +
                    "\r\n" + Resource.String("STR_AC_IMPORT_TIME_LEFT") + FormatTimeSpan(new TimeSpan(((((Int64)_ms_x_balance_interval * (Int64)(Accounts.Rows.Count - _updated_balance_count) / 200) * 10000)))) + "";

                if (_non_existing_accounts > 0)
                {
                  _rowupdateEvent.Message += "\r\n" + Resource.String("STR_AC_IMPORT_BALANCE_NOT_PROCESED2") + _non_existing_accounts;
                }

                _rowupdateEvent.Row_count = Accounts.Rows.Count;
                _rowupdateEvent.Pending_rows = Accounts.Rows.Count - _updated_balance_count;
                
                if (_rowupdateEvent.Pending_rows <= 0)
                {
                  _rowupdateEvent.State = IMPORT_STATES.BalancesUpdated;
                  _rowupdateEvent.Message += "\r\n" + Resource.String("STR_AC_IMPORT_END");
                }
                else
                {
                  _rowupdateEvent.State = IMPORT_STATES.UpdatingBalances;
                }

                WriteLog(_rowupdateEvent.Message);
                OnRowUpdated(this, _rowupdateEvent);
              }
            }// end if OnRowUpdated != null
          }
          catch
          {
            if (OnRowUpdated != null) // call event
            {
              _updated_balance_count++;

              if ((_updated_balance_count == Accounts.Rows.Count))
              {
                AccountImportEventArgs _rowupdateEvent;
                _ms_x_balance_interval = Environment.TickCount - _ms_x_balance_interval0;
                _ms_x_balance_interval0 = Environment.TickCount;
                _rowupdateEvent = new AccountImportEventArgs();
                _rowupdateEvent.Message = Resource.String("STR_AC_IMPORT_BALANCES_ERROR");
                _rowupdateEvent.Row_count = Accounts.Rows.Count;
                _rowupdateEvent.Pending_rows = Accounts.Rows.Count - _updated_balance_count;

                if (_rowupdateEvent.Pending_rows <= 0)
                {
                  _rowupdateEvent.State = IMPORT_STATES.BalancesUpdated;
                  _rowupdateEvent.Message += "" + Resource.String("STR_AC_IMPORT_END");
                  _rowupdateEvent.Message += "\r\n" + Resource.String("STR_AC_IMPORT_SEPARATOR");
                }
                else
                {
                  _rowupdateEvent.State = IMPORT_STATES.UpdatingBalances;
                }
                
                WriteLog(_rowupdateEvent.Message);
                OnRowUpdated(this, _rowupdateEvent);
              }
            }// end if OnRowUpdated != null

          }
        } //end foreach (DataRow _account_new in Accounts.Rows)
      }
      catch
      {

      }

      return true;
    }

    public bool WriteLog(String LogLine)
    {
      FileStream _fs_log;
      StreamWriter _sw_log;
      _fs_log = new FileStream(m_file_out, FileMode.Append, FileAccess.Write);
      _sw_log = new StreamWriter(_fs_log);

      try
      {
        _sw_log.WriteLine("" + LogLine);
        _sw_log.Close();
        _fs_log.Close();

        return true;
      }
      catch
      {
        _sw_log.Close();
        _fs_log.Close();

        return false;
      }
    }

    #endregion

    #region events
    public delegate void RowUpdatedEvent(object sender, AccountImportEventArgs e);
    public event RowUpdatedEvent OnRowUpdated;

    void AccountsRowUpdated(object sender, SqlRowUpdatedEventArgs e)
    {
      int _msxbatch;
      AccountImportEventArgs _rowupdateEvent;
      _msxbatch = Environment.TickCount - m_batch_tick0;

      m_batch_tick0 = Environment.TickCount;
      _rowupdateEvent = new AccountImportEventArgs();
      if (m_pending_clients <= 0)
      {
        _rowupdateEvent.State = IMPORT_STATES.DBUpdated;
      }
      else
      {
        _rowupdateEvent.State = IMPORT_STATES.UpdatingDB;
      }

      if (e.Errors == null)
      {
        // Resource.String("STR_FRM_PLAYER_SEARCH_001");
        m_pending_clients -= e.RowCount;
        _rowupdateEvent.Message = "\r\n\r\n-- " + DateTime.Now + " --" +
            "\r\n" + e.RowCount.ToString() + Resource.String("STR_AC_IMPORT_PROCESED_IN") + FormatTimeSpan(new TimeSpan((Int64)_msxbatch * 10000)) +
            "\r\n" + Resource.String("STR_AC_IMPORT_ELAPSED") + FormatTimeSpan(new TimeSpan(((Int64)(Environment.TickCount) - (Int64)m_process_tick0) * 10000)) +
            "\r\n" + Resource.String("STR_AC_IMPORT_ACCOUNTS_TOTAL") + m_clients_count.ToString() +
            "\r\n" + Resource.String("STR_AC_IMPORT_PROCESED") + (m_clients_count - m_pending_clients - m_no_action_accounts).ToString() +
            "\r\n" + Resource.String("STR_AC_IMPORT_ACCOUNTS_LEFT") + Math.Max(m_pending_clients, 0).ToString() +
            "\r\n" + Resource.String("STR_AC_IMPORT_TIME_LEFT") + FormatTimeSpan(new TimeSpan(((((Int64)_msxbatch * (Int64)m_pending_clients / e.RowCount) * 10000))));
        _rowupdateEvent.Row_count = m_clients_count;
        _rowupdateEvent.Pending_rows = m_pending_clients;
        WriteLog(_rowupdateEvent.Message);
      }
      else
      {
        Int32 _processed_rows_before_update;
        Int32 _processed_rows_after_update;

        _processed_rows_before_update = m_clients_count - m_pending_clients - m_no_action_accounts;
        _processed_rows_after_update = e.Row.Table.Select("ROW_PROCESSED=1").Length + m_not_new_accounts;
        m_pending_clients -= Math.Max(_processed_rows_after_update - _processed_rows_before_update, 0);

        _rowupdateEvent.Message = "\r\n\r\n-- " + DateTime.Now + " --" +
            "\r\n" + Math.Max(_processed_rows_after_update - _processed_rows_before_update, 0) + Resource.String("STR_AC_IMPORT_PROCESED_IN") + FormatTimeSpan(new TimeSpan((Int64)_msxbatch * 10000)) +
            "\r\n" + Resource.String("STR_AC_IMPORT_ELAPSED") + FormatTimeSpan(new TimeSpan(((Int64)(Environment.TickCount) - (Int64)m_process_tick0) * 10000)) +
            "\r\n" + Resource.String("STR_AC_IMPORT_ACCOUNTS_TOTAL") + m_clients_count.ToString() +
            "\r\n" + Resource.String("STR_AC_IMPORT_PROCESED") + (m_clients_count - m_pending_clients - m_no_action_accounts).ToString() +
            "\r\n" + Resource.String("STR_AC_IMPORT_ACCOUNTS_LEFT") + Math.Max(m_pending_clients, 0).ToString() +
            "\r\n" + Resource.String("STR_AC_IMPORT_TIME_LEFT") + FormatTimeSpan(new TimeSpan(((((Int64)_msxbatch * (Int64)m_pending_clients / e.RowCount) * 10000)))) +
            "\r\n" + Resource.String("STR_AC_IMPORT_RETRY_NUMBER") + (m_retry + 1).ToString();

        _rowupdateEvent.Row_count = m_clients_count;
        _rowupdateEvent.Pending_rows = m_pending_clients;
        WriteLog(_rowupdateEvent.Message);
        WriteLog(e.Errors.Message);

        _rowupdateEvent.Message += Environment.NewLine + Resource.String("STR_AC_IMPORT_RETRY");// "\r\nSe ha producido un error. \r\nReintentando...";
        _rowupdateEvent.State = IMPORT_STATES.UpdatingDB;
        WriteLog(Environment.NewLine + "Retry" + (m_retry + 1).ToString());
      }

      OnRowUpdated(this, _rowupdateEvent);
    }

    private string FormatTimeSpan(TimeSpan Time)
    {
      String _formated_time;
      _formated_time = string.Empty;

      if (Time.Hours > 0)
      {
        _formated_time = "> " + Time.Hours.ToString() + "h " + Time.Minutes + "m ";
      }
      else
      {
        if (Time.Minutes > 0)
        {
          _formated_time = "> " + Time.Minutes.ToString() + "m";
        }
        else
        {
          _formated_time = "< 1m";
        }
      }

      return _formated_time;

    }

    # endregion

    #region static methods

    public static Boolean ImportForAccountSummary(String FilePath, out Data.DataTable DtAccount, out Data.DataTable DtError, ProgressBarEventHandler Progress)
    {
      if (!File.Exists(FilePath))
      {
        DtAccount = null;
        DtError = null;
        return false;
      }

      SpreadsheetGear.IWorkbook _excel_book = null;
      SpreadsheetGear.IWorksheet _excel_sheet = null;
      SpreadsheetGear.IRange _excel_range = null;

      Object[,] _range_value;
      Int32 _num_rows;
      String _down_address;
      System.Globalization.CultureInfo _current_culture_info;

      Int64 _account_id;
      String _holden_name;

      DtAccount = new System.Data.DataTable("accounts");
      DtAccount.Columns.Add("account_id", typeof(Int64));
      DtAccount.Columns.Add("account_holder_name", typeof(String));

      DtError = new System.Data.DataTable("errors");
      DtError.Columns.Add("row", typeof(Int32));
      DtError.Columns.Add("message", typeof(String));

      // Microsoft.Office.Interop.Excel just work properly under a en-us cultureinfo
      _current_culture_info = System.Threading.Thread.CurrentThread.CurrentCulture;
      System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-us");

      try
      {
        _excel_book = Factory.GetWorkbook(FilePath);

        _excel_sheet = _excel_book.Worksheets[0];
        _excel_range = _excel_sheet.UsedRange.CurrentRegion;

        // get the address of the bottom, right cell
        _down_address = _excel_range.GetAddress(false,
                                               false,
                                               ReferenceStyle.A1,
                                               false,
                                               null);

        // Get the range, then values from a1
        _excel_range = _excel_sheet.UsedRange; //.Range[_down_address];

        _range_value = (Object[,])_excel_range.Value;

        _num_rows = _range_value.GetLength(0) - 1;

        using (DB_TRX _db_trx = new DB_TRX())
        {
          for (int _idx_row = 6; _idx_row <= _num_rows; _idx_row++)
          {
            try
            {
              object value = _range_value[_idx_row, 0];

              if (value.IsNumber())
              {

                _account_id = Convert.ToInt64(value);
                _holden_name = Convert.ToString(_range_value[_idx_row, 2]);

                // 1 - AccountId
                // 2 - Name
                DtAccount.Rows.Add(_account_id,
                                  _holden_name);

              }

              if (Progress != null)
              {
                Progress(_idx_row, _num_rows);
              }
            }
            catch (Exception _ex)
            {
              DtError.Rows.Add(_idx_row, _ex.Message);
              continue;
            }
          }
        }

        return true;
      }

      catch (Exception _ex)
      {
        DtError.Rows.Add(-1, _ex.Message);
      }
      finally
      {
        try
        {
          CloseBookObjects(ref _excel_book, ref _excel_sheet, ref _excel_range);
        }
        finally
        {
          // set previous cultureinfo
          System.Threading.Thread.CurrentThread.CurrentCulture = _current_culture_info;

          CloseBookObjects(ref _excel_book, ref _excel_sheet, ref _excel_range);
        }
      }

      return false;
    }





    public static Boolean ImportForPreassignedPromotion(String FilePath, out Data.DataTable DtAccount, out Data.DataTable DtError, ProgressBarEventHandler Progress)
    {
      if (!File.Exists(FilePath))
      {
        DtAccount = null;
        DtError = null;

        return false;
      }

      SpreadsheetGear.IWorkbook _excel_book = null;
      SpreadsheetGear.IWorksheet _excel_sheet = null;
      SpreadsheetGear.IRange _excel_range = null;

      Object[,] _range_value;
      Int32 _num_rows;
      Int32 _holder_level;
      String _down_address;
      System.Globalization.CultureInfo _current_culture_info;
      String _message;

      DtAccount = new System.Data.DataTable("accounts");
      DtAccount.Columns.Add("account_id", typeof(Int64));
      DtAccount.Columns.Add("reward", typeof(Decimal));
      DtAccount.Columns.Add("account_holder_level", typeof(Int32));

      DtError = new System.Data.DataTable("errors");
      DtError.Columns.Add("row", typeof(Int32));
      DtError.Columns.Add("message", typeof(String));

      // Microsoft.Office.Interop.Excel just work properly under a en-us cultureinfo
      _current_culture_info = System.Threading.Thread.CurrentThread.CurrentCulture;
      System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-us");

      try
      {
        _excel_book = Factory.GetWorkbook(FilePath);

        _excel_sheet = _excel_book.Worksheets[0];
        _excel_range = _excel_sheet.UsedRange.CurrentRegion;

        // get the address of the bottom, right cell
        _down_address = _excel_range.GetAddress(false,
                                               false,
                                               ReferenceStyle.A1,
                                               false,
                                               null);

        // Get the range, then values from a1
        _excel_range = _excel_sheet.Range[_down_address];
        _range_value = (Object[,])_excel_range.Value;
        _num_rows = _range_value.GetLength(0) - 1;

        using (DB_TRX _db_trx = new DB_TRX())
        {
          for (int _idx_row = 0; _idx_row <= _num_rows; _idx_row++)
          {
            try
            {
              if (!CheckAccountForPreassigned(Convert.ToInt64(_range_value[_idx_row, 0]),
                                              Convert.ToDecimal(_range_value[_idx_row, 1]),
                                              out _message,
                                              out _holder_level,
                                              _db_trx.SqlTransaction))
              {
                throw new Exception(_message);
              }

              // 1 - AccountId
              // 2 - Reward
              // 3 - HolderLevel
              DtAccount.Rows.Add(_range_value[_idx_row, 0],
                                 _range_value[_idx_row, 1],
                                 _holder_level);

              if (Progress != null)
              {
                Progress(_idx_row, _num_rows);
              }
            }
            catch (Exception _ex)
            {
              DtError.Rows.Add(_idx_row, _ex.Message);
              continue;
            }
          }
        }

        return true;
      }

      catch (Exception _ex)
      {
        DtError.Rows.Add(-1, _ex.Message);
      }
      finally
      {
        try
        {
          CloseBookObjects(ref _excel_book, ref _excel_sheet, ref _excel_range);
        }
        finally
        {
          // set previous cultureinfo
          System.Threading.Thread.CurrentThread.CurrentCulture = _current_culture_info;

          CloseBookObjects(ref _excel_book, ref _excel_sheet, ref _excel_range);
        }
      }

      return false;
    }

    private static Boolean CheckAccountForPreassigned(Int64 AccountId,
                                                      Decimal Reward,
                                                      out String Message,
                                                      out Int32 HolderLevel,
                                                      SqlTransaction Trx)
    {
      Object _obj;
      String _str;

      HolderLevel = 0;
      Message = "";

      if (Reward <= 0)
      {
        _str = Resource.String("STR_AC_PREASSIGNED_INCORRECT_VALUE"); // Valor incorrecto, 
        Message = _str + Reward;
        return false;
      }

      using (SqlCommand _cmd = new SqlCommand("SELECT AC_HOLDER_LEVEL FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId", Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
        _obj = _cmd.ExecuteScalar();
      }

      if (_obj != null && _obj != DBNull.Value)
      {
        HolderLevel = (Int32)_obj;

        return true;
      }
      else
      {
        _str = Resource.String("STR_AC_PREASSIGNED_ACC_NOT_FOUND"); // Cuenta no encontrada,
        Message = _str + AccountId;
      }

      return false;
    }

    public static void ReportForPreassignedPromotion(Data.DataTable DtAccount,
                                                     Data.DataTable DtError,
                                                     ACCOUNT_PROMO_CREDIT_TYPE CreditType,
                                                     REPORT_MODE Mode,
                                                     out StringBuilder Report)
    {
      Int64 _acc_prev;
      Int32 _total_accounts;
      Int32 _total_rows;
      String _str;
      String _str1;
      Decimal _decimal_output;
      Object _decimal;

      _acc_prev = -1;
      _total_accounts = 0;
      _total_rows = 0;

      Report = new StringBuilder();

      // FBA 30-SEP-2013 if it's null exits the function to avoid throwing an exception
      if (DtAccount == null)
      {
        return;
      }

      try
      {
        Report.AppendLine("");
        Report.AppendLine("");
        foreach (DataRow _dr_next in DtAccount.Select(String.Empty, "account_id"))
        {
          if (!_dr_next[0].Equals(_acc_prev))
          {
            _total_accounts++;
          }
          _acc_prev = (Int64)_dr_next[0];
          _total_rows++;
        }

        _str = Resource.String("STR_AC_PREASSIGNED_TOTAL_PREASSIGNED"); // Total de preasginaciones: 
        Report.AppendLine(_str + _total_rows);
        _str = Resource.String("STR_AC_PREASSIGNED_TOTAL_NUM"); // N�mero total de cuentas: 
        Report.AppendLine(_str + _total_accounts);
        _str = Resource.String("STR_AC_PREASSIGNED_TOTAL_AWARDED"); // Total otorgado: 
        _decimal = DtAccount.Compute("SUM(reward)", String.Empty);
        // FBA 30-SEP-2013 sets values to 0 if compute returns null and cannot convert to decimal
        if (Decimal.TryParse(_decimal.ToString(), out _decimal_output))
        {
          Report.AppendLine(_str + ToSymbol(CreditType, _decimal_output));
        }
        else
        {
          Report.AppendLine(_str + ToSymbol(CreditType, 0));
        }
        Report.AppendLine("");

        if (Mode == REPORT_MODE.SUMMARY)
        {
          Decimal _min_reward;
          Decimal _max_reward;
          Decimal _total_interval;
          Int32 _num_accounts;
          Int32 _interval;
          Decimal _from, _to;
          Int32 _count_interval;
          String _filter_compute;

          _str = Resource.String("STR_AC_PREASSIGNED_SUMMARY"); // Resumen de importaci�n:
          Report.Insert(0, _str);

          _num_accounts = DtAccount.Rows.Count;
          _decimal = DtAccount.Compute("MAX(reward)", String.Empty);
          if (Decimal.TryParse(_decimal.ToString(), out _decimal_output))
          {
            _max_reward = _decimal_output;
          }
          else
          {
            _max_reward = 0;
          }

          _decimal = DtAccount.Compute("MIN(reward)", String.Empty);
          if (Decimal.TryParse(_decimal.ToString(), out _decimal_output))
          {
            _min_reward = _decimal_output;
          }
          else
          {
            _min_reward = 0;
          }

          _interval = (Int32)Math.Ceiling((_max_reward - _min_reward) / 10);

          for (Decimal _int = 0; _int < 10; _int++)
          {
            _from = _int * _interval;
            if (_int == 9)
            {
              _to = _max_reward + 1;
            }
            else
            {
              _to = (_int + 1) * _interval;
            }

            _filter_compute = "reward>=" + Format.LocalNumberToDBNumber(_from.ToString()) + " AND reward<" + Format.LocalNumberToDBNumber(_to.ToString());

            _count_interval = (Int32)DtAccount.Compute("COUNT(account_id)", _filter_compute);
            if (_count_interval > 0)
            {
              _total_interval = (Decimal)DtAccount.Compute("SUM(reward)", _filter_compute);
              Report.AppendLine(ToSymbol(CreditType, _from) + " - " + ToSymbol(CreditType, _to) + " = " + _count_interval + ", total = " + ToSymbol(CreditType, _total_interval));
            }
          }
        }

        if (Mode == REPORT_MODE.DETAILS)
        {
          _str = Resource.String("STR_AC_PREASSIGNED_DETAIL_OF_ACCOUNTS"); // Detalle de cuentas:
          Report.Insert(0, _str);

          foreach (DataRow _dr in DtAccount.Rows)
          {
            _str = Resource.String("STR_AC_PREASSIGNED_ACCOUNT"); // Cuenta: 
            _str1 = Resource.String("STR_AC_PREASSIGNED_AWARDED"); // , otorgado: 
            Report.AppendLine(_str + _dr["account_id"] + _str1 + ToSymbol(CreditType, (Decimal)_dr["reward"]));
          }
        }

        if (DtError != null && DtError.Rows.Count > 0)
        {
          Report.AppendLine("");
          _str = Resource.String("STR_AC_PREASSIGNED_IMPORT_ERRORS"); // Errores en la importaci�n:
          Report.AppendLine(_str);
          foreach (DataRow _dr in DtError.Rows)
          {
            _str = Resource.String("STR_AC_PREASSIGNED_ROW"); // Fila: 
            _str1 = Resource.String("STR_AC_PREASSIGNED_MSG"); // , mensaje: 
            Report.AppendLine("Fila: " + _dr["row"] + ", mensaje: " + _dr["message"]);
          }
        }
      }
      catch
      {
        Report.Length = 0;
      }
    }

    private static String ToSymbol(ACCOUNT_PROMO_CREDIT_TYPE CreditType, Decimal Amount)
    {
      if (CreditType == ACCOUNT_PROMO_CREDIT_TYPE.POINT)
      {
        return ((Points)Amount).ToString() + " PT ";
      }
      else
      {
        return ((Currency)(Amount)).ToString();
      }
    }

    public static Boolean InsertForPreassignedPromotion(Data.DataTable DtAccount, Int64 PromotionId, SqlTransaction Trx)
    {
      Data.DataTable _account_promotions;
      AccountPromotion _promotion;

      _promotion = new AccountPromotion();

      if (!Promotion.ReadPromotionData(Trx, PromotionId, _promotion))
      {
        return false;
      }

      _account_promotions = AccountPromotion.CreateAccountPromotionTable();

      try
      {
        foreach (Data.DataRow _dr_acc in DtAccount.Rows)
        {
          if ((Int32)_dr_acc[2] == 0 && _promotion.credit_type == ACCOUNT_PROMO_CREDIT_TYPE.POINT)
          {
            // Holder level 0 (anonymous) and credit type point
            continue;
          }

          if (!AccountPromotion.AddPromotionPreassignedToAccount((Int64)_dr_acc[0],
                                                                 (Int32)_dr_acc[2],
                                                                 (Decimal)_dr_acc[1],
                                                                 _promotion,
                                                                 _account_promotions))
          {
            return false;
          }

          if(!AccountPromotion.SetExpirationLimitPreAsignedPromoOnCreation(_account_promotions, _promotion.date_finish))
          {
            return false;
          }
        }

        if (!AccountPromotion.InsertAccountPromotions(_account_promotions, true, _promotion.PromoGameId > 0, Trx))
        {
          return false;
        }

        return true;

      }
      catch
      { ; }

      return false;
    }

    public static Boolean LoadPreassignedAccounts(Int64 PromotionId, out Data.DataTable Accounts, SqlTransaction Trx)
    {
      StringBuilder _sb;

      Accounts = null;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  SELECT   ACP_ACCOUNT_ID AS ACCOUNT_ID ");
        _sb.AppendLine("         , ACP_BALANCE    AS REWARD ");
        _sb.AppendLine("         , ACP_ACCOUNT_LEVEL AS HOLDER_LEVEL ");
        _sb.AppendLine("    FROM   ACCOUNT_PROMOTIONS ");
        _sb.AppendLine("   WHERE   ACP_PROMO_ID = @pPromoId ");
        _sb.AppendLine("ORDER BY   ACP_UNIQUE_ID ASC ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPromoId", SqlDbType.BigInt).Value = PromotionId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            Accounts = new Data.DataTable();
            Accounts.Load(_reader);
          }
        }

        return true;
      }
      catch
      { ; }

      return false;

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Release a COM object.
    //
    //  PARAMS :
    //      - INPUT :
    //          - object Obj
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void ReleaseComObject(object Obj)
    {
      Int32 _num_ref;
      try
      {
        if (Obj != null)
        {
          do
          {
            _num_ref = System.Runtime.InteropServices.Marshal.ReleaseComObject(Obj);
          } while (_num_ref > 0);
        }
        Obj = null;
      }
      catch
      {
        Obj = null;
      }
    } // ReleaseComObject

    #endregion

  }

  public class AccountImportEventArgs : EventArgs
  {
    private int _row_count;

    private int _pending_rows;

    private String _message;

    private AccountsImport.IMPORT_STATES _state;

    public AccountsImport.IMPORT_STATES State
    {
      get { return _state; }
      set { _state = value; }
    }

    public int Row_count
    {
      get { return _row_count; }
      set { _row_count = value; }
    }

    public int Pending_rows
    {
      get { return _pending_rows; }
      set { _pending_rows = value; }
    }

    public String Message
    {
      get { return _message; }
      set { _message = value; }
    }
  }

  public class AccountPointsImportEventArgs : EventArgs
  {
    private int _row_count;

    private int _pending_rows;

    private String _message;

    private AccountImportPoints.IMPORT_STATES _state;

    public AccountImportPoints.IMPORT_STATES State
    {
      get { return _state; }
      set { _state = value; }
    }

    public int Row_count
    {
      get { return _row_count; }
      set { _row_count = value; }
    }

    public int Pending_rows
    {
      get { return _pending_rows; }
      set { _pending_rows = value; }
    }

    public String Message
    {
      get { return _message; }
      set { _message = value; }
    }
  }

  public class AccountImportPoints
  {
    #region ENUMS

    public enum IMPORT_STATES
    {
      ReadingDocument = 0,
      DocumentReaded = 1,
      UpdatingDB = 2,
      DBUpdated = 3,
      UpdatingBalances = 4,
      BalancesUpdated = 5,
      Aborted = 6
    }

    public enum REPORT_MODE
    {
      SUMMARY = 0,
      DETAILS = 1,
      ONLY_ERRORS = 3,
    }

    #endregion

    #region Constants

    private const decimal MAX_POINTS = 1000000; //Max of points that can import is 1.000.000
    private const decimal ROWS_FOR_EVENTS = 2000;
    private const int FIRST_DATA_ROW = 2;

    #endregion

    #region Events
    public delegate void RowUpdatedEvent(object sender, AccountPointsImportEventArgs e);
    public event RowUpdatedEvent OnRowUpdated;

    #endregion

    # region PrivateFunctions

    //------------------------------------------------------------------------------
    // PURPOSE : Sends the last Event
    //  PARAMS :
    //      - INPUT :
    //
    // RETURNS :     Name of column or "" if there's any error.
    //
    public void setLastEvent()
    {
      AccountPointsImportEventArgs _rowupdateEvent;

      _rowupdateEvent = new AccountPointsImportEventArgs();
      _rowupdateEvent.Message = Resource.String("STR_GTPS_IMPORT_CANCELED").Replace("\\r\\n", "\r\n");

      _rowupdateEvent.Row_count = 0;
      _rowupdateEvent.Pending_rows = 0;

      OnRowUpdated(this, _rowupdateEvent);

    }//setLastEvent

    //------------------------------------------------------------------------------
    // PURPOSE : Generate an error message event
    //  PARAMS :
    //      - INPUT :
    //
    // RETURNS :     STR_AC_IMPORT_POINTS_READ_ERROR
    //
    public void setErrorEvent()
    {
      AccountPointsImportEventArgs _rowupdateEvent;

      _rowupdateEvent = new AccountPointsImportEventArgs();
      _rowupdateEvent.Message = Resource.String("STR_AC_IMPORT_POINTS_READ_ERROR").Replace("\\r\\n", "\r\n");

      _rowupdateEvent.Row_count = 0;
      _rowupdateEvent.Pending_rows = 0;

      OnRowUpdated(this, _rowupdateEvent);

    }//setLastEvent


    #endregion

    #region PublicFunctions

    //------------------------------------------------------------------------------
    // PURPOSE : Open and check a DataTable with accounts points to import. Then, fill a DataSet with correct accounts
    //           and errors.
    //  PARAMS :
    //      - INPUT :
    //          - DataTable: PointsToImport
    //          - Boolean: AllowNegativePoints
    //
    //      - OUTPUT : 
    //          - DataSet: Accounts
    //
    // RETURNS :     True if the process ends, false if there's an error.
    //
    public Boolean GetPoints(System.Data.DataTable PointsToImport, Boolean AllowNegativePoints, out System.Data.DataSet Accounts)
    {
      Accounts = null;

      if (PointsToImport.Rows.Count == 0)
      {
        return false;
      }

      // Tables from DataSet
      System.Data.DataTable _dt_errors;
      System.Data.DataTable _dt_accounts;

      AccountPointsImportEventArgs _row_update_event;
      Int32 _correct_accounts;
      DataRow _new_row;
      Boolean _insert_row;

      // Create and configure DataTables from DataSet
      _dt_accounts = new System.Data.DataTable("AccountsPoints");
      _dt_accounts.Columns.Add("AccountId", typeof(Int64));
      _dt_accounts.Columns.Add("PointsForLevel", typeof(Decimal));
      _dt_accounts.Columns.Add("PointsOnlyForRedeem", typeof(Decimal));

      _dt_errors = new System.Data.DataTable("Errors");
      _dt_errors.Columns.Add("row", typeof(Int32));
      _dt_errors.Columns.Add("message", typeof(String));

      //Configure thread
      System.Globalization.CultureInfo _old_ci = System.Threading.Thread.CurrentThread.CurrentCulture;
      System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-us");
      _correct_accounts = 0;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          foreach (DataRow _row in PointsToImport.Rows)
          {
            _insert_row = true;
            _new_row = _dt_accounts.NewRow();


            if (OnRowUpdated != null && ((PointsToImport.Rows.IndexOf(_row) == PointsToImport.Rows.Count) || ((PointsToImport.Rows.IndexOf(_row) % ROWS_FOR_EVENTS) == 0)))// Create an event, and fill with the current info.
            {
              _row_update_event = new AccountPointsImportEventArgs();
              _row_update_event.Message = "\r\n" + Resource.String("STR_AC_IMPORT_PROCESSING") + Resource.String("STR_AC_IMPORT_READ_ACCOUNTS") + (PointsToImport.Rows.IndexOf(_row) + 1);

              if (PointsToImport.Rows.IndexOf(_row) != PointsToImport.Rows.Count)
              {
                _row_update_event.Message += "\r\n" + Resource.String("STR_AC_IMPORT_PROCESSING") + Resource.String("STR_AC_IMPORT_READ_ERRORS_FOUND") + _dt_errors.Rows.Count;
              }

              _row_update_event.Row_count = PointsToImport.Rows.Count;
              _row_update_event.Pending_rows = PointsToImport.Rows.Count - PointsToImport.Rows.IndexOf(_row);
              if (_row_update_event.Pending_rows <= 0)
              {
                _row_update_event.State = IMPORT_STATES.DocumentReaded;
              }
              else
              {
                _row_update_event.State = IMPORT_STATES.ReadingDocument;
              }
              OnRowUpdated(this, _row_update_event); //Call event, handled byGUI [AddHandler m_Accounts_Points.OnRowUpdated, AddressOf UpdateProcess]

            } // if (OnRowUpdated != null)

            foreach (DataColumn _column in PointsToImport.Columns)
            {
              String _warning_message;
              Boolean _warning;
              Decimal _points;

              _warning = false;
              _warning_message = String.Empty;

              switch (PointsToImport.Columns.IndexOf(_column))
              {
                case 0: // Account Id

                  if (CheckAccountId(_row[_column], _db_trx.SqlTransaction, out _warning, out _warning_message))
                  {
                    _new_row[0] = _row[_column];
                  }

                  break;

                case 1: // Level Points

                  if (CheckPoints(_row[_column], 1, AllowNegativePoints, out _points, out _warning, out _warning_message))
                  {
                    _new_row[1] = _points;
                  }

                  break;

                case 2: // Change Points

                  if (CheckPoints(_row[_column], 2, AllowNegativePoints, out _points, out _warning, out _warning_message))
                  {
                    _new_row[2] = _points;
                  }

                  break;

                default:
                  break;
              }

              // If error, add to Errors table
              if (_warning)
              {
                _dt_errors.Rows.Add(PointsToImport.Rows.IndexOf(_row) + FIRST_DATA_ROW, _warning_message);
                _insert_row = false;
                break;
              }
            } // For each Column

            // Insert Row
            if (_insert_row)
            {
              _dt_accounts.Rows.Add(_new_row);
              _correct_accounts++;
            }

          } // For each Row
        }
      }
      catch (Exception)
      {
        return false;
      }

      System.Threading.Thread.CurrentThread.CurrentCulture = _old_ci;

      Accounts = new DataSet();
      Accounts.Tables.Add(_dt_accounts);
      Accounts.Tables.Add(_dt_errors);

      _row_update_event = new AccountPointsImportEventArgs();
      _row_update_event.Message = Resource.String("STR_AC_IMPORT_POINTS_READ_FINISHED").Replace("\\r\\n", "\r\n");
      _row_update_event.Message += "\r\n" + Resource.String("STR_AC_IMPORT_CORRECT_ACCOUNTS") + _correct_accounts;
      _row_update_event.State = IMPORT_STATES.DocumentReaded;
      _row_update_event.Row_count = PointsToImport.Rows.Count;
      _row_update_event.Pending_rows = 0;
      OnRowUpdated(this, _row_update_event);

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Returns True or False if the Account: not exists, is blocked or is anonymous.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Account: DataRow that we're checking.
    //          - SqlTrx: DataBase Transaction object
    //      - OUT:
    //          - Warning: True if there's a warning.
    //          - WarningMessage: String message.
    //
    // RETURNS:
    //      - Object: CellValue.
    //
    private Boolean CheckAccountId(Object Account, SqlTransaction SqlTrx, out Boolean Warning, out String WarningMessage)
    {
      Object _obj;
      Int64 _account;

      Warning = false;
      WarningMessage = String.Empty;

      try
      {
        //Is null?
        Warning = (Account == DBNull.Value);
        if (Warning)
        {
          WarningMessage = Resource.String("STR_AC_IMPORT_POINTS_FORMAT_NOT_CORRECT_1");

          return false;
        }

        _account = (Int64)Account;

        // Is anonymous?
        using (SqlCommand _cmd = new SqlCommand("SELECT AC_HOLDER_LEVEL FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId AND AC_HOLDER_LEVEL > 0", SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = _account;
          _obj = _cmd.ExecuteScalar();
        }

        if (_obj == null || _obj == DBNull.Value)
        {
          Warning = true;
          WarningMessage = Resource.String("STR_AC_IMPORT_POINTS_NOT_VALID");

          return false;
        }

        return true;
      }
      catch (Exception)
      {
        Warning = true;
        WarningMessage = Resource.String("STR_AC_IMPORT_POINTS_NOT_VALID");

        return false;
      }
    } // CheckAccountId

    //------------------------------------------------------------------------------
    // PURPOSE: Lock an Account record in BBDDD.
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //          - Trx
    //      - OUTPUT:
    //          - Points
    //
    // RETURNS:
    //      - Boolean: If operation has been set correctly.
    //
    public static Boolean DB_LockAccount(Int64 AccountId, SqlTransaction Trx)
    {
      StringBuilder _sql_txt;
      SqlCommand _sql_command;
      Int32 _num_rows_updated;

      try
      {
        _sql_txt = new StringBuilder();
        _sql_txt.AppendLine("UPDATE ACCOUNTS");
        _sql_txt.AppendLine("   SET AC_BALANCE = AC_BALANCE + 0");
        _sql_txt.AppendLine(" WHERE AC_ACCOUNT_ID = @pAccountId ");

        using (_sql_command = new SqlCommand(_sql_txt.ToString()))
        {
          _sql_command.Connection = Trx.Connection;
          _sql_command.Transaction = Trx;
          _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          _num_rows_updated = _sql_command.ExecuteNonQuery();
        }

        if (_num_rows_updated != 1)
        {
          Log.Error(String.Format("DB_LockAccount. AccountId: {0}", AccountId));

          return false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

    }// DB_LockAccount


    //------------------------------------------------------------------------------
    // PURPOSE: Returns the Value for bucket for the selected account
    //
    //  PARAMS:
    //      - INPUT:
    //          - BucketId
    //          - AccountId
    //          - Trx
    //      - OUTPUT:
    //          - PointsForLevel
    //
    // RETURNS:
    //      - Boolean: If operation has been set correctly.
    //
    public static Boolean DB_GetAccountBucket(Int64 AccountId, Buckets.BucketId BucketId, out Decimal Value, SqlTransaction Trx)
    {
      StringBuilder _sql_txt;
      Object _obj;
      SqlCommand _sql_command;

      Value = 0;

      try
      {
        _sql_txt = new StringBuilder();

        _sql_txt.AppendLine("DECLARE @pValue MONEY");
        _sql_txt.AppendLine("SELECT @pValue = CBU_VALUE");
        _sql_txt.AppendLine("  FROM CUSTOMER_BUCKET");
        _sql_txt.AppendLine(" WHERE CBU_CUSTOMER_ID = @pAccountId ");
        _sql_txt.AppendLine("   AND CBU_BUCKET_ID = @pBucketId");

        _sql_txt.AppendLine("SET @pValue = ISNULL(@pValue,0)");
        _sql_txt.AppendLine("SELECT @pValue");

        using (_sql_command = new SqlCommand(_sql_txt.ToString()))
        {
          _sql_command.Connection = Trx.Connection;
          _sql_command.Transaction = Trx;
          _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _sql_command.Parameters.Add("@pBucketId", SqlDbType.BigInt).Value = (int)BucketId;

          _obj = _sql_command.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            Value = (Decimal)_obj;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

    }// DB_GetAccountBucket


    private Boolean CheckPoints(Object PointsFromDataRow, Int16 _column_index, Boolean AllowNegativePoints, out Decimal Points, out Boolean Warning, out String WarningMessage)
    {
      Warning = false;
      WarningMessage = String.Empty;
      _column_index++;

      // Null
      if (PointsFromDataRow == DBNull.Value)
      {
        Points = 0;

        return true;
      }

      Points = (Decimal)PointsFromDataRow;

      // Negative Points
      Warning = ((Points < 0 && !AllowNegativePoints) || Points > Decimal.MaxValue);
      if (Warning)
      {
        WarningMessage = Resource.String(String.Format("STR_AC_IMPORT_POINTS_FORMAT_NOT_CORRECT_{0}", _column_index.ToString()));

        return false;
      }

      // Maximum Points  
      Warning = (Points > MAX_POINTS);
      if (Warning)
      {
        WarningMessage = Resource.String("STR_AC_IMPORT_POINTS_EXCEED_MAXIMUM");

        return false;
      }

      return true;
    } // CheckPoints

    //------------------------------------------------------------------------------
    // PURPOSE : Add points into users account. Points can be to level change or redeemeables.
    //           Add the movements types: ImportedPointsForLevel and ImportedPointsOnlyForRedeem.
    //  PARAMS :
    //      - INPUT :
    //          - DataTable with three columns AccountId, PointsForLevel and PointsOnlyForRedeem
    //
    //      - OUTPUT :
    //
    // RETURNS : True if operations were successful
    //
    public void AddImportedPoints(System.Data.DataTable Accounts)
    {
      Int64 _operation_id;
      AccountMovementsTable _ac_mov_table;
      CashierSessionInfo _session_info;
      AccountPointsImportEventArgs _events;
      Int64 _account_id;
      Decimal _points_for_level;
      Decimal _points_only_for_redeem;
      Decimal _initial_points_for_level;
      Decimal _initial_points_for_redeem;
      Decimal _final_points;
      Int32 _total_ap_imported;
      Int32 _total_ap_error;
      StringBuilder _accounts_points_error;
      StringBuilder _accounts_points_negative;
      Dictionary<Int64, Decimal> _accounts_negative_balance;
      String _error_msg;

      _events = new AccountPointsImportEventArgs();
      _events.State = IMPORT_STATES.UpdatingDB;

      if (GeneralParam.GetBoolean("Site", "MultiSiteMember") && CommonMultiSite.GetPlayerTrackingMode() != PlayerTracking_Mode.Site) // MultiSiteMember
      {
        _events.Message = Resource.String("STR_AC_IMPORT_POINTS_OPERATION_ERROR").Replace("\\r\\n", "\r\n");
        OnRowUpdated(this, _events);
        return;
      }

      _events.Row_count = Accounts.Rows.Count;
      _events.Pending_rows = Accounts.Rows.Count;
      _total_ap_imported = 0;
      _total_ap_error = 0;
      _accounts_points_error = new StringBuilder();
      _accounts_points_negative = new StringBuilder();
      _accounts_negative_balance = new Dictionary<Int64, Decimal>();

      // Insert Operation
      if (GeneralParam.GetBoolean("MultiSite", "IsCenter")) // MultiSite Center
      {
        _operation_id = 0; //There is not account_operations in MultiSite Center.
      }
      else // NOT a MultiSite Center and NOT a MultiSite Member.
      {
        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (!Operations.DB_InsertOperation(OperationCode.IMPORT_POINTS,
                                               0, // account_id = 0 because it would be the same operationId for different accounts.
                                               0, // Cashier session id
                                               0, // Mb Account id
                                               0, // promo id
                                               0, // currency ammount
                                               0, // currency NR
                                               0, // Operation Data
                                               0,
                                               string.Empty,
                                               out _operation_id,
                                               out _error_msg,
                                               _db_trx.SqlTransaction))
            {
              throw new Exception();
            }

            if (!_db_trx.Commit())
            {
              throw new Exception();
            }
          }
        }
        catch (Exception)
        {
          _events.Message = Resource.String("STR_AC_IMPORT_POINTS_OPERATION_ERROR").Replace("\\r\\n", "\r\n");
          OnRowUpdated(this, _events);
          return;
        }
      }

      // Update Accounts and Insert Movements
      try
      {
        _session_info = new CashierSessionInfo();
        _session_info.AuthorizedByUserName = String.Empty;
        _session_info.TerminalId = 0;
        _session_info.TerminalName = String.Empty;
        _session_info.CashierSessionId = 0;

        _events.Message = Resource.String("STR_AC_IMPORT_POINTS_INSERTED").Replace("\\r\\n", "\r\n");
        OnRowUpdated(this, _events);
        _events.Message = String.Empty;

        foreach (DataRow _dr_account in Accounts.Rows)
        {
          _account_id = (Int64)_dr_account["AccountId"];
          _points_for_level = (Decimal)_dr_account["PointsForLevel"];
          _points_only_for_redeem = (Decimal)_dr_account["PointsOnlyForRedeem"];
          _ac_mov_table = new AccountMovementsTable(_session_info);

          try
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              // Lock Account
              if (!DB_LockAccount(_account_id, _db_trx.SqlTransaction))
              {
                throw new Exception();
              }

              _final_points = 0;

              // PointsForLevel
              if (_points_for_level != 0)
              {
                if (!DB_GetAccountBucket(_account_id, Buckets.BucketId.RankingLevelPoints, out _initial_points_for_level, _db_trx.SqlTransaction))
                {
                  throw new Exception();
                }

                _final_points = _initial_points_for_level + _points_for_level;

                // Insert Movement
                if (!_ac_mov_table.Add(_operation_id,
                                       _account_id,
                                       MovementType.ImportedPointsForLevel,
                                       _initial_points_for_level,
                                       0,
                                       _points_for_level,
                                       _final_points))
                {
                  throw new Exception();
                }
              }

              // PointsOnlyForRedeem
              if (_points_only_for_redeem != 0)
              {
                if (!DB_GetAccountBucket(_account_id, Buckets.BucketId.RedemptionPoints, out _initial_points_for_redeem, _db_trx.SqlTransaction))
                {
                  throw new Exception();
                }

                _final_points = _initial_points_for_redeem + _points_only_for_redeem;

                // Insert Movement
                if (!_ac_mov_table.Add(_operation_id,
                                       _account_id,
                                       MovementType.ImportedPointsOnlyForRedeem,
                                       _initial_points_for_redeem,
                                       0,
                                       _points_only_for_redeem,
                                       _final_points))
                {
                  throw new Exception();
                }
              }

              if (_accounts_negative_balance.ContainsKey(_account_id))
              {
                _accounts_negative_balance.Remove(_account_id);
              }
              // Negative Balance
              if (_final_points < 0)
              {
                _accounts_negative_balance.Add(_account_id, _final_points);
              }
              // Save movements
              if (!_ac_mov_table.Save(_db_trx.SqlTransaction))
              {
                throw new Exception();
              }

              if (!BucketsUpdate.UpdateCustomerBucket(_account_id, Buckets.BucketId.RankingLevelPoints, null, _points_for_level, Buckets.BucketOperation.ADD_DISCRETIONAL, true, true, _db_trx.SqlTransaction))
              {
                throw new Exception();
              }

              if (!BucketsUpdate.UpdateCustomerBucket(_account_id, Buckets.BucketId.RedemptionPoints, null, _points_only_for_redeem, Buckets.BucketOperation.ADD_GENERATED, true, true, _db_trx.SqlTransaction))
              {
                throw new Exception();
              }

              // Commit Transaction
              if (!_db_trx.Commit())
              {
                throw new Exception();
              }
            }// End _db_trx

            _events.Pending_rows--;
            _total_ap_imported++;
            if ((_total_ap_imported % ROWS_FOR_EVENTS) == 0) //Every 2000 updated accounts, throw a message.
            {
              _events.Message = Resource.String("STR_AC_IMPORT_POINTS_INSERTED_2", _total_ap_imported, _events.Pending_rows).Replace("\\r\\n", "\r\n");
            }
            OnRowUpdated(this, _events);
            _events.Message = String.Empty;

          }
          catch (Exception)
          {
            //Throw a message with the account info and continues with the next one.
            _total_ap_error++;
            _accounts_points_error.AppendLine(Resource.String("STR_AC_IMPORT_POINTS_ACCOUNT", _account_id));
            _events.Pending_rows--;
            OnRowUpdated(this, _events);
          }
        }// End For

        _events.Message = Resource.String("STR_AC_IMPORT_POINTS_PROCESS_FINISHED").Replace("\\r\\n", "\r\n");
        OnRowUpdated(this, _events);
      }
      catch (Exception Ex)
      {
        Log.Message("AddImportedPoints() - Error updating imported points in DB");
        Log.Exception(Ex);
        return;
      }
      finally // Summary
      {
        // Total imported
        _events.Message = Resource.String("STR_AC_IMPORT_POINTS_TOTAL_SUCCESS", _total_ap_imported).Replace("\\r\\n", "\r\n");

        // Negative Balance
        if (_accounts_negative_balance.Count > 0)
        {
          _events.Message += Resource.String("STR_AC_IMPORT_POINTS_TOTAL_NEGATIVES", _accounts_negative_balance.Count, _total_ap_imported).Replace("\\r\\n", "\r\n"); ;
          foreach (KeyValuePair<Int64, Decimal> _account in _accounts_negative_balance)
          {
            _accounts_points_negative.AppendLine("   " + Resource.String("STR_AC_IMPORT_POINTS_ACCOUNT", _account.Key) + " " +
                                Resource.String("STR_AC_IMPORT_POINTS_POINTS", string.Format("{0:C}", _account.Value)));
          }

          _events.Message += _accounts_points_negative.ToString();
        }

        // Import errors        
        if (_total_ap_error > 0)
        {
          _events.Message += Resource.String("STR_AC_IMPORT_POINTS_TOTAL_ERROR", _total_ap_error).Replace("\\r\\n", "\r\n");
          _events.Message += _accounts_points_error.ToString();
        }

        _events.State = IMPORT_STATES.DBUpdated;
        _events.Row_count = 0;
        _events.Pending_rows = 0;

        OnRowUpdated(this, _events);
      }

    }// AddImportedPoints

    #endregion

  }// AccountImportPoints
}