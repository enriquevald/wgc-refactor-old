using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Microsoft.Win32;
using System.IO;

namespace WSI.Common
{
  public static class VisualStudioRuntime
  {
    const string VC_2010_REDIST_X86 = "{196BB40D-1578-3D01-B289-BEFC77A11A1E}";
    const string VC_2010_REDIST_X64 = "{DA5E371C-6333-3D8A-93A4-6FD5B20BCC6E}";
    const string VC_2010_REDIST_IA64 = "{C1A35166-4301-38E9-BA67-02823AD72A1B}";
    const string VC_2010_SP1_REDIST_X86 = "{F0C3E5D1-1ADE-321E-8167-68EF0DE699A5}";
    const string VC_2010_SP1_REDIST_X64 = "{1D8E6291-B0D5-35EC-8441-6616F567A0F7}";
    const string VC_2010_SP1_REDIST_IA64 = "{88C73C1C-2DE5-3B01-AFB8-B46EF4AB41CD}";

    static String[] _vs2010x86 = new string[] { VC_2010_REDIST_X86, VC_2010_SP1_REDIST_X86 };

    private static Boolean m_install_running = false;
    private static List<String> m_installed_versions = new List<string>();
    private static Object m_lock = new object();


    private static Boolean IsInstalled(String Version)
    {
      try
      {
        RegistryKey _regkey;
        List<String> _keys;
        List<String> _versions;

        if (m_installed_versions.Contains(Version)) return true;

        // Not checked previously

        _regkey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall");
        _keys = new List<string>(_regkey.GetSubKeyNames());

        _versions = null;
        if (Version == "2010")
        {
          _versions = new List<string>(_vs2010x86);
        }

        if (_versions == null)
        {
          return false;
        }

        foreach (String _key in _keys)
        {
          foreach (String _version in _versions)
          {
            if (!_key.Contains(_version)) continue;

            m_installed_versions.Add(Version);

            return true;
          }
        }
      }
      catch
      { }

      return false;
    }




    public static void Install(String Version)
    {
      lock (m_lock)
      {
        if (IsInstalled(Version))
        {
          return;
        }
        if (m_install_running)
        {
          return;
        }

        // Install
        ProcessStartInfo _start_info;
        Process _install;
        String _filename;
        String _arguments;

        if (Version != "2010")
        {
          return;
        }
        _filename = "VC_2010_Redistributable_Setup.EXE";
        _arguments = "";

        _start_info = new System.Diagnostics.ProcessStartInfo();
        _start_info.Arguments = _arguments;
        _start_info.FileName = _filename;
        _start_info.CreateNoWindow = true;
        _start_info.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
        _start_info.RedirectStandardOutput = true;
        _start_info.UseShellExecute = false;
        _start_info.RedirectStandardOutput = true;

        _install = new System.Diagnostics.Process();
        _install.StartInfo = _start_info;
        _install.Exited += new EventHandler(InstallExited);
        _install.EnableRaisingEvents = true;
        if (_install.Start())
        {
          m_install_running = true;
        }
      }
    }

    private static void InstallExited(object sender, EventArgs e)
    {
      lock (m_lock)
      {
        m_install_running = false;

        try
        {
          ((Process)sender).Dispose();
        }
        catch
        { }
      }
    }
  }



}



//  [Files]
//Source: "vcredist_x86.exe"; DestDir: {tmp}; Flags: deleteafterinstall

//[Run]
//; add the Parameters, WorkingDir and StatusMsg as you wish, just keep here
//; the conditional installation Check
//Filename: "{tmp}\vcredist_x86.exe"; Check: VCRedistNeedsInstall

//[Code]
//#IFDEF UNICODE
//  #DEFINE AW "W"
//#ELSE
//  #DEFINE AW "A"
//#ENDIF
//type
//  INSTALLSTATE = Longint;
//const
//  INSTALLSTATE_INVALIDARG = -2;  // An invalid parameter was passed to the function.
//  INSTALLSTATE_UNKNOWN = -1;     // The product is neither advertised or installed.
//  INSTALLSTATE_ADVERTISED = 1;   // The product is advertised but not installed.
//  INSTALLSTATE_ABSENT = 2;       // The product is installed for a different user.
//  INSTALLSTATE_DEFAULT = 5;      // The product is installed for the current user.

//  VC_2005_REDIST_X86 = '{A49F249F-0C91-497F-86DF-B2585E8E76B7}';
//  VC_2005_REDIST_X64 = '{6E8E85E8-CE4B-4FF5-91F7-04999C9FAE6A}';
//  VC_2005_REDIST_IA64 = '{03ED71EA-F531-4927-AABD-1C31BCE8E187}';
//  VC_2005_SP1_REDIST_X86 = '{7299052B-02A4-4627-81F2-1818DA5D550D}';
//  VC_2005_SP1_REDIST_X64 = '{071C9B48-7C32-4621-A0AC-3F809523288F}';
//  VC_2005_SP1_REDIST_IA64 = '{0F8FB34E-675E-42ED-850B-29D98C2ECE08}';
//  VC_2005_SP1_ATL_SEC_UPD_REDIST_X86 = '{837B34E3-7C30-493C-8F6A-2B0F04E2912C}';
//  VC_2005_SP1_ATL_SEC_UPD_REDIST_X64 = '{6CE5BAE9-D3CA-4B99-891A-1DC6C118A5FC}';
//  VC_2005_SP1_ATL_SEC_UPD_REDIST_IA64 = '{85025851-A784-46D8-950D-05CB3CA43A13}';

//  VC_2008_REDIST_X86 = '{FF66E9F6-83E7-3A3E-AF14-8DE9A809A6A4}';
//  VC_2008_REDIST_X64 = '{350AA351-21FA-3270-8B7A-835434E766AD}';
//  VC_2008_REDIST_IA64 = '{2B547B43-DB50-3139-9EBE-37D419E0F5FA}';
//  VC_2008_SP1_REDIST_X86 = '{9A25302D-30C0-39D9-BD6F-21E6EC160475}';
//  VC_2008_SP1_REDIST_X64 = '{8220EEFE-38CD-377E-8595-13398D740ACE}';
//  VC_2008_SP1_REDIST_IA64 = '{5827ECE1-AEB0-328E-B813-6FC68622C1F9}';
//  VC_2008_SP1_ATL_SEC_UPD_REDIST_X86 = '{1F1C2DFC-2D24-3E06-BCB8-725134ADF989}';
//  VC_2008_SP1_ATL_SEC_UPD_REDIST_X64 = '{4B6C7001-C7D6-3710-913E-5BC23FCE91E6}';
//  VC_2008_SP1_ATL_SEC_UPD_REDIST_IA64 = '{977AD349-C2A8-39DD-9273-285C08987C7B}';
//  VC_2008_SP1_MFC_SEC_UPD_REDIST_X86 = '{9BE518E6-ECC6-35A9-88E4-87755C07200F}';
//  VC_2008_SP1_MFC_SEC_UPD_REDIST_X64 = '{5FCE6D76-F5DC-37AB-B2B8-22AB8CEDB1D4}';
//  VC_2008_SP1_MFC_SEC_UPD_REDIST_IA64 = '{515643D1-4E9E-342F-A75A-D1F16448DC04}';

//  VC_2010_REDIST_X86 = '{196BB40D-1578-3D01-B289-BEFC77A11A1E}';
//  VC_2010_REDIST_X64 = '{DA5E371C-6333-3D8A-93A4-6FD5B20BCC6E}';
//  VC_2010_REDIST_IA64 = '{C1A35166-4301-38E9-BA67-02823AD72A1B}';
//  VC_2010_SP1_REDIST_X86 = '{F0C3E5D1-1ADE-321E-8167-68EF0DE699A5}';
//  VC_2010_SP1_REDIST_X64 = '{1D8E6291-B0D5-35EC-8441-6616F567A0F7}';
//  VC_2010_SP1_REDIST_IA64 = '{88C73C1C-2DE5-3B01-AFB8-B46EF4AB41CD}';

//  // Microsoft Visual C++ 2012 x86 Minimum Runtime - 11.0.61030.0 (Update 4) 
//  VC_2012_REDIST_MIN_UPD4_X86 = '{BD95A8CD-1D9F-35AD-981A-3E7925026EBB}';
//  VC_2012_REDIST_MIN_UPD4_X64 = '{CF2BEA3C-26EA-32F8-AA9B-331F7E34BA97}';
//  // Microsoft Visual C++ 2012 x86 Additional Runtime - 11.0.61030.0 (Update 4) 
//  VC_2012_REDIST_ADD_UPD4_X86 = '{B175520C-86A2-35A7-8619-86DC379688B9}';
//  VC_2012_REDIST_ADD_UPD4_X64 = '{37B8F9C7-03FB-3253-8781-2517C99D7C00}';

//function MsiQueryProductState(szProduct: string): INSTALLSTATE; 
//  external 'MsiQueryProductState{#AW}@msi.dll stdcall';

//function VCVersionInstalled(const ProductID: string): Boolean;
//begin
//  Result := MsiQueryProductState(ProductID) = INSTALLSTATE_DEFAULT;
//end;

//function VCRedistNeedsInstall: Boolean;
//begin
//  // here the Result must be True when you need to install your VCRedist
//  // or False when you don't need to, so now it's upon you how you build
//  // this statement, the following won't install your VC redist only when
//  // the Visual C++ 2010 Redist (x86) and Visual C++ 2010 SP1 Redist(x86)
//  // are installed for the current user
//  Result := not (VCVersionInstalled(VC_2010_REDIST_X86) and 
//    VCVersionInstalled(VC_2010_SP1_REDIST_X86));
//end;