//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TemplatePDF.cs
// 
//   DESCRIPTION: Handle a PDF file 
// 
//        AUTHOR: Marcos Piedra
// 
// CREATION DATE: 31-JAN-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-JAN-2012 MPO    First release.
// 02-MAY-2012 MPO    New feature, set an image and it filled into PDF.
// 28-SEP-2017 RGR    Bug 29898:WIGOS-4921 Accounting - Generic report Monthly report: the design is not the same than the story and the footer signings are missing
// 13-NOV-2017 LTC    Bug 30632:WIGOS-6159 WigosGui: Error occurred when the customer data is printed from customer base form
// 23-NOV-2017 LTC    Bug 30632:WIGOS-6159 WigosGui: Error occurred when the customer data is printed from customer base form
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using pdf = iTextSharp.text.pdf;
using draw = System.Drawing;
using System.Collections;
using System.IO;
using WSI.Common;
using System.Data;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Drawing;

namespace WSI.Common
{
  public class TemplatePDF : Template
  {

    public const float CM_TO_POINTS = 28.3464567F;
    public const float CM_TO_INCH = 0.393700787F;
    public const float INCH_TO_POINTS = 72;

    #region Members

    String m_current_pdf;

    #endregion // Members

    #region Constructor

    //------------------------------------------------------------------------------
    // PURPOSE: Constructor. Initialize properties
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public TemplatePDF(String TemplatePDF)
    {
      FileStream _file_stream;
      Stream _stream;

      _file_stream = new FileStream(TemplatePDF, FileMode.Open, FileAccess.Read);
      _stream = CopyToMemory(_file_stream);
      SetMemoryStreamTemplate(_stream);
      _file_stream.Close();

      m_current_pdf = "";

    } //TemplatePDF

    //------------------------------------------------------------------------------
    // PURPOSE: Constructor. Initialize properties
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public TemplatePDF(MemoryStream TemplatePDF)
    {

      SetMemoryStreamTemplate(TemplatePDF);
      m_current_pdf = "";

    } //TemplatePDF

    #endregion // Constructor

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Show a PDF file using the default application to open PDF files
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public override void Show()
    {

      CheckCurrentPdf();

      if (String.IsNullOrEmpty(m_current_pdf))
      {

        return;
      }

      System.Diagnostics.Process _process;

      _process = new System.Diagnostics.Process();
      _process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Maximized;
      _process.StartInfo.FileName = m_current_pdf;
      _process.Start();

    } // Show

    //------------------------------------------------------------------------------
    // PURPOSE: Print a PDF file using the default application to open PDF files.
    //          It waits for print and close the application.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public override void Print()
    {

      CheckCurrentPdf();

      if (String.IsNullOrEmpty(m_current_pdf))
      {

        return;
      }

      System.Diagnostics.ProcessStartInfo _start_info = new System.Diagnostics.ProcessStartInfo();
      _start_info.Verb = "Print";
      _start_info.FileName = m_current_pdf;
      _start_info.CreateNoWindow = true;
      _start_info.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;

      System.Diagnostics.Process _process = new System.Diagnostics.Process();
      _process.StartInfo = _start_info;

      _process.Start();

      // Wait for the process to start.
      System.Threading.Thread.Sleep(1000);

      if (!_process.HasExited)
      {
        _process.WaitForInputIdle();
        _process.CloseMainWindow();
        _process.Close();
      }

    } // Print

    //------------------------------------------------------------------------------
    // PURPOSE: Save the PDF file
    //
    //  PARAMS:
    //      - INPUT:
    //          - FileName: File where the PDF document is saved
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public override void SaveAs(String FileName)
    {
      Boolean _is_filled;

      _is_filled = true;
      if (m_changed)
      {
        _is_filled = Fill();
      }

      if (_is_filled)
      {
        WSI.Common.Template.Save(m_stream_target, FileName);
        m_current_pdf = FileName;
        m_changed = false;
      }

    } // SaveAs

    //------------------------------------------------------------------------------
    // PURPOSE: Filling the template PDF that contains the fields and the images
    //
    //  PARAMS:
    //      - INPUT:
    //          - FileName: Template PDF that contains the fields
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Boolean: Is correct the process of filling?
    // 
    //   NOTES:
    // 
    public override Boolean Fill()
    {

      pdf.AcroFields _acro_fields;
      pdf.PdfReader _reader;
      pdf.PdfStamper _stamper;
      pdf.PdfContentByte _byte;
      iTextSharp.text.Image _itext_image;
      DataRow[] _img_rows;
      Int32 _last_num_page;
      ImageInfo _image_info;
      String _field;
      String[] _splitted_field;

      _reader = null;
      _stamper = null;

      try
      {

        m_stream_template.Position = 0;
        _reader = new pdf.PdfReader(m_stream_template);


        //if (m_stream_target != null)
        //  m_stream_target.Close();

        m_stream_target = new MemoryStream();
        _stamper = new pdf.PdfStamper(_reader, m_stream_target);
        // LTC 2017-NOV-23    
        _stamper.AcroFields.GenerateAppearances = true;

        _acro_fields = _stamper.AcroFields;

        foreach (String _key in m_fields.Keys)
        {
          _field = "";
          _splitted_field = m_fields[_key].Split(new string[] { "\r\n", "\n", "\\n" }, StringSplitOptions.None);

          foreach (String _aux in _splitted_field)
          {
            _field += _aux + (_splitted_field.Length == 1 ? "" : System.Environment.NewLine);
          }


          _acro_fields.SetField(_key, _field);
        }

        _last_num_page = 0;
        _img_rows = m_images.Select("", "NumPage");

        _byte = null;
        foreach (DataRow _dr in _img_rows)
        {
          if (!_last_num_page.Equals(_dr["NumPage"]))
          {
            _last_num_page = (Int32)_dr["NumPage"];
            _byte = _stamper.GetOverContent(_last_num_page);
          }

          _image_info = (ImageInfo)_dr["Image"];

          switch (_image_info.ActionImg)
          {
            case ACTION_IMG.NONE:
              _itext_image = iTextSharp.text.Image.GetInstance(_image_info.BytesImg);
              _itext_image.ScaleToFit(_image_info.Rect.Width, _image_info.Rect.Height);
              //_itext_image.ScaleAbsolute(_image_info.Rect.Width, _image_info.Rect.Height);
              _itext_image.SetAbsolutePosition(_image_info.Rect.X + (_image_info.Rect.Width - _itext_image.ScaledWidth) / 2, _image_info.Rect.Y + (_image_info.Rect.Height - _itext_image.ScaledHeight) / 2);
              break;

            case ACTION_IMG.SCALE_TO_FIT:
              float _width;
              float _height;
              float _scale_w;
              float _scale_h;
              float _scale;

              Int32 _dpix;
              Int32 _dpiy;

              _itext_image = iTextSharp.text.Image.GetInstance(_image_info.BytesImg);

              _dpix = _itext_image.DpiX;
              if (_dpix == 0)
              {
                _dpix = (Int32)_image_info.Image.HorizontalResolution;
              }

              _dpiy = _itext_image.DpiY;
              if (_dpiy == 0)
              {
                _dpiy = (Int32)_image_info.Image.VerticalResolution;
              }

              _width = _itext_image.Width / _dpix * INCH_TO_POINTS;
              _height = _itext_image.Height / _dpiy * INCH_TO_POINTS;

              _scale_w = Math.Min(1, (_image_info.Rect.Width * CM_TO_POINTS) / _width);
              _scale_h = Math.Min(1, (_image_info.Rect.Height * CM_TO_POINTS) / _height);
              _scale = Math.Min(_scale_h, _scale_w);

              _itext_image.ScaleToFit(_width * _scale, _height * _scale);
              _itext_image.SetAbsolutePosition((float)(_image_info.Rect.X * CM_TO_POINTS) + ((_image_info.Rect.Width * CM_TO_POINTS) - (_width * _scale)) / 2,
                                               (float)(_image_info.Rect.Y * CM_TO_POINTS) + (_image_info.Rect.Height * CM_TO_POINTS) - (_height * _scale));

              break;

            default:
              return false;
          }

          _byte.AddImage(_itext_image);

          //_byte.Rectangle(_image_info.Rect.X, _image_info.Rect.Y, _image_info.Rect.Width, _image_info.Rect.Height);
          //_byte.SetColorStroke(Color.BLACK);
          //_byte.Fill();

        }

        _stamper.FormFlattening = true;
        _stamper.Writer.CloseStream = false;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {

        if (_stamper != null)
        {
          _stamper.Close();
        }

        if (_reader != null)
        {
          _reader.Close();
        }

        m_changed = !m_changed; 
      }

    } // FillField

    //------------------------------------------------------------------------------
    // PURPOSE: Get a list of field name of PDF
    //
    //  PARAMS:
    //      - INPUT:
    //          - FileName: Path of PDF to get field name
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - List<String>: List of field name
    // 
    //   NOTES:
    //    
    public override List<String> Fields()
    {
      List<String> _list;
      pdf.PdfReader _reader;

      _reader = null;

      try
      {
        m_stream_template.Position = 0;
        _reader = new pdf.PdfReader(m_stream_template);
        _list = new List<String>();

        foreach (var _de in _reader.AcroFields.Fields)
        {
          _list.Add((String)_de.Key);
        }

        return _list;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return null;
      }
      finally
      {
        if (_reader != null)
        {
          _reader.Close();
        }

      }
    } // GetListOfField

    //------------------------------------------------------------------------------
    // PURPOSE: Get the current stream that contain the template has filled.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Stream:
    // 
    //   NOTES:
    // 
    public override Stream GetStream()
    {
      Boolean _is_filled;
      MemoryStream _copied;

      _is_filled = true;
      _copied = null;

      if (m_changed)
      {
        _is_filled = Fill();
      }

      if (_is_filled)
      {
        _copied = CopyToMemory(m_stream_target);
      }

      return _copied;

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Close the corresponding objects
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public override void Close()
    {

      if (m_stream_target != null) m_stream_target.Close();
      if (m_stream_template != null) m_stream_template.Close();

    } // Close

    //------------------------------------------------------------------------------
    // PURPOSE: Check if exists the library "itextsharp.dll" and 
    //          an application that open the PDF document
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Boolean: true if check is correct
    // 
    //   NOTES:
    //    
    public static Boolean ApplicationAvailable()
    {
      String _path_itext;

      _path_itext = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
      _path_itext = System.IO.Path.Combine(_path_itext, "itextsharp.dll");

      if (!System.IO.File.Exists(_path_itext))
      {

        return false;
      }

      try
      {

        if (!IsAssociated(".pdf"))
        {

          return false;
        }

        return true;
      }
      catch
      {

        return false;
      }

    } // ApplicationAvailable

    public void RotateAllPage()
    {
      PdfReader _pdf_reader;
      PdfStamper _pdf_stamper;
      PdfDictionary _page_dict;
      Int32 _desired_rot;
      MemoryStream _mm_output;

      try
      {
        m_stream_target.Position = 0;
        _pdf_reader = new PdfReader(m_stream_target);
        _mm_output = new MemoryStream();
        _pdf_stamper = new PdfStamper(_pdf_reader, _mm_output);
        _page_dict = _pdf_reader.GetPageN(1);
        _desired_rot = 90; // 90 degrees clockwise from what it is now
        PdfNumber _rotation = _page_dict.GetAsNumber(PdfName.ROTATE);
        if (_rotation != null)
        {
          _desired_rot += _rotation.IntValue;
          _desired_rot %= 360; // must be 0, 90, 180, or 270
        }
        _page_dict.Put(PdfName.ROTATE, new PdfNumber(_desired_rot));

        _pdf_stamper.FormFlattening = true;
        _pdf_stamper.Writer.CloseStream = false;
        _pdf_stamper.Close();

        m_stream_target = new MemoryStream(((MemoryStream)_mm_output).ToArray());
        m_changed = false;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

    }

    public Boolean Merge(MemoryStream MemoryStream)
    {
      List<Byte[]> _docs_to_merge = new List<Byte[]>();

      try
      {
        PdfReader _pdf_reader = null;
        Document _doc;
        PdfWriter _pdf_writer;
        PdfImportedPage _pdf_imported_page;

        _docs_to_merge.Add(((MemoryStream)m_stream_target).ToArray());
        _docs_to_merge.Add(MemoryStream.ToArray());

        using (MemoryStream _ms_output = new MemoryStream())
        {
          _doc = new Document();
          _pdf_writer = new PdfSmartCopy(_doc, _ms_output);
          _doc.Open();

          for (int _idx_doc = 0; _idx_doc < _docs_to_merge.Count; _idx_doc++)
          {
            _pdf_reader = new PdfReader(_docs_to_merge[_idx_doc]);
            for (int _idx_page = 1; _idx_page < _pdf_reader.NumberOfPages + 1; _idx_page++)
            {
              _pdf_imported_page = _pdf_writer.GetImportedPage(_pdf_reader, _idx_page);
              ((PdfSmartCopy)_pdf_writer).AddPage(_pdf_imported_page);
              _pdf_writer.FreeReader(_pdf_reader);
            }
          }

          if (_pdf_reader != null) _pdf_reader.Close();
          _pdf_writer.Close();
          _doc.Close();
          _docs_to_merge.Clear();

          m_stream_target.Dispose();
          m_stream_target = new MemoryStream(_ms_output.ToArray());
          m_changed = false;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    /// <summary>
    /// Reset m_changed
    /// </summary>
    // LTC 13-NOV-2017
    public void SetIsChangedOff()
    {
      m_changed = false;
    }

    #endregion // Public Methods

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Check the current path that target pdf file
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    private void CheckCurrentPdf()
    {

      if (m_changed)
      {

        Fill();
        m_current_pdf = Path.Combine(Path.GetTempPath(), Path.GetTempFileName() + ".pdf");
        Template.Save(m_stream_target, m_current_pdf);
        m_changed = false;

      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE: It is registered an extension of file in the Win32 System for an application
    //
    //  PARAMS:
    //      - INPUT:
    //          - Extension: The extension
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Boolean: Is registered
    // 
    //   NOTES:
    // 
    static private Boolean IsAssociated(String Extension)
    {

      try
      {

        return (Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(Extension, false) != null);
      }
      catch (Exception)
      {

        return false;
      }

    }

    #endregion

  } // TemplatePDF

} // Wrapper
