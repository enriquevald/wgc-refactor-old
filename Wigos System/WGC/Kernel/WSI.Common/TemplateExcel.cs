﻿//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TemplateExcel.cs
// 
//   DESCRIPTION: Handle excel application
//                Reference code: 
// 
//        AUTHOR: Luis Tenorio
// 
// CREATION DATE: 20-JUN-2016
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-JUN-2016 LTC    First release.
// 20-JUN-2016 LTC    Product Backlog Item 13612,13613,13614: Witholding Evidence: Campeche.
// 13-JUL-2016 LTC    Bug 15020:Wigos GUI: Unhandled exception generating a Microsoft Excel Sheet 2003
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.IO;
using System.Threading;
using WSI.Common;
using System.Xml;
using System.Windows.Forms;
using SpreadsheetGear;
using System.Data;

namespace WSI.Common
{
  public class TemplateExcel : Template
  {

    #region Members

    IWorkbook m_workbook;
    IWorksheet m_worksheet;
    String m_current_xls;

    #endregion

    #region Constructor

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize properties.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public TemplateExcel(MemoryStream TemplateExcel)
    {
      SetMemoryStreamTemplate(TemplateExcel);
      m_current_xls = "";
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize properties.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public TemplateExcel()
    {
      m_current_xls = "";
    }


    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Filling the template excel that contains the fields
    //
    //  PARAMS:
    //      - INPUT:
    //          - FileName: Template excel that contains the fields
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Boolean: Is correct the process of filling?
    // 
    //   NOTES:
    // 
    public override Boolean Fill()
    {
      String _field;
      String[] _splitted_field;
      String _file_name;
      String _file_path;
      DataRow[] _img_rows;
      ImageInfo _image_info;

      SpreadsheetGear.IRange _range;
      SpreadsheetGear.IWorksheetWindowInfo _windowInfo;

      try
      {

        _file_name = Path.Combine(Path.GetTempPath(), Path.GetTempFileName() + this.m_template_ext);
        WSI.Common.Template.Save(m_stream_template, _file_name);

        if (m_workbook != null) Dispose();
        m_workbook = SpreadsheetGear.Factory.GetWorkbook(_file_name);
        m_worksheet = m_workbook.Worksheets[0];

        _windowInfo = m_worksheet.WindowInfo;
        double _left = _windowInfo.ColumnToPoints(1.5);
        double _top = _windowInfo.RowToPoints(1.5);
        

        if (m_worksheet == null)
          return false;

        // Replace the fields in the document
        foreach (String _key in m_fields.Keys)
        {
          _field = "";
          _splitted_field = m_fields[_key].Split(new string[] { "\r\n", "\n", "\\n" }, StringSplitOptions.None);

          foreach (String _aux in _splitted_field)
          {
            _field += _aux + (_splitted_field.Length == 1 ? "" : System.Environment.NewLine);
          }

          _range = m_worksheet.Cells[_key];
          _range.Value = _field;

        }

        _img_rows = m_images.Select("", "NumPage");
        foreach (DataRow _dr in _img_rows)
        {

          _image_info = (ImageInfo)_dr["Image"];

          switch (_image_info.ActionImg)
          {
            case ACTION_IMG.NONE:
              m_worksheet.Shapes.AddPicture(_image_info.BytesImg, _image_info.Rect.X, _image_info.Rect.Y, _image_info.Rect.Width, _image_info.Rect.Height); 
              break;

            default:
              return false;
          }

        }

        //Create the output folder if it does not exist.
        _file_path = Path.GetTempPath();
        if (!Directory.Exists(_file_path))
        {
          Directory.CreateDirectory(_file_path);
        }

        m_workbook.Save();
        m_workbook.Close();

        m_stream_target = File.OpenRead(_file_name);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE: Get the current stream that contain the template has filled.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //          - Stream:
    // 
    //   NOTES:
    // 
    public override Stream GetStream()
    {
      Boolean _is_filled;
      MemoryStream _copied;

      _is_filled = true;
      _copied = null;

      if (m_changed)
      {
        _is_filled = Fill();
      }

      if (_is_filled)
      {
        _copied = CopyToMemory(m_stream_target);
      }

      return _copied;

    }  

    #endregion

    #region Private Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Check the current path that target xls file
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    private void CheckCurrentXls()
    {

      if (m_changed)
      {

        Fill();
        m_current_xls = Path.Combine(Path.GetTempPath(), Path.GetTempFileName() + ".xlsx");
        Template.Save(m_stream_target, m_current_xls);
        m_changed = false;

      }

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Release a COM object.
    //
    //  PARAMS :
    //      - INPUT :
    //          - object Obj
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static private void ReleaseComObject(Object Obj)
    {
      Int32 _num_ref;
      try
      {
        if (Obj != null)
        {
          do
          {
            _num_ref = System.Runtime.InteropServices.Marshal.ReleaseComObject(Obj);
          } while (_num_ref > 0);
        }
        Obj = null;
      }
      catch
      {
        Obj = null;
      }
    } 

    //------------------------------------------------------------------------------
    // PURPOSE: Check if it have changed and filled the fields if is necessary
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    private void CheckCurrentDocument()
    {
      if (m_changed)
      {
        Fill();
        m_changed = false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Dispose Excel objects
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public void Dispose()
    {
      System.Threading.Thread.CurrentThread.CurrentCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
      try
      {

        if (m_workbook != null)
        {
          m_workbook.Close();
        }
      }
      catch
      {
        throw;
      }
      finally
      {
        m_worksheet = null;
        m_workbook = null;
      }
    }

    #endregion

  }
}
