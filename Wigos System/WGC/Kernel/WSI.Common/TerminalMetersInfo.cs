﻿//------------------------------------------------------------------------------
// Copyright © 2007-2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: TerminalMetersInfo.cs
//  
//   DESCRIPTION: Implements the class TerminalMetersInfo (Get Terminal meters.)
// 
//        AUTHOR Daniel Dubé
// 
// CREATION DATE: 22-DEC-2015
// 
// REVISION HISTORY:
// 
// Date        Author     Description
//------------------------------------------------------------------------------
// 22-DEC-2015 DDS        Inital Draft.
// 08-APR-2016 ETP        Fixed Bug 11600: Needs to show / Save all Meters not only 3.
// 11-APR-2016 ETP        Fixed Bug 11600: Only for PCD terminals
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common
{
    public static class TerminalMetersInfo
    {
        #region Constants

        private const int CONNECTION_EXCEPTION_TIME_OUT = -2;
        public const int NOT_READED_METER_VALUE = -1;

        #endregion

        #region Public methods

        //------------------------------------------------------------------------------
        // PURPOSE : Read PCD Config and related children items
        //
        //  PARAMS :
        //      - INPUT : SqlTransaction transaction, PCDConfiguration Id
        //
        //      - OUTPUT :
        //
        // RETURNS : Filled DataSet
        //
        //   NOTES :
        //  
        public static DataSet ReadPCDConfiguration(SqlTransaction Trx, Int64 PCDConfigurationId)
        {
            StringBuilder _str_bld;
            DataSet _pcd_configuration;

            _pcd_configuration = new DataSet();
            try
            {
                _str_bld = new StringBuilder();

                _str_bld.AppendLine("SELECT   SC_CONFIGURATION_ID ");
                _str_bld.AppendLine("       , SC_NAME ");
                _str_bld.AppendLine("       , SC_DESCRIPTION ");
                _str_bld.AppendLine("  FROM   SMIB_CONFIGURATION ");
                _str_bld.AppendLine(" WHERE   SC_CONFIGURATION_ID = @pPCDConfigurationId ");
                _str_bld.AppendLine("   AND   SC_COMM_TYPE = @pCommType ");

                _str_bld.AppendLine(";");

                _str_bld.AppendLine("SELECT   PMT_CONFIGURATION_ID ");
                _str_bld.AppendLine("       , PMT_PCD_IO_NUMBER ");
                _str_bld.AppendLine("       , PMT_PCD_IO_TYPE ");
                _str_bld.AppendLine("       , PMT_EGM_NUMBER ");
                _str_bld.AppendLine("       , PMT_EGM_NUMBER_MULTIPLIER ");
                _str_bld.AppendLine("       , PMT_OUPUT_TIME_PULSE_DOWN ");
                _str_bld.AppendLine("       , PMT_OUPUT_TIME_PULSE_UP ");
                _str_bld.AppendLine("  FROM   PCD_METERS_TRANSLATION ");
                _str_bld.AppendLine(" WHERE   PMT_CONFIGURATION_ID = @pPCDConfigurationId ");
                _str_bld.AppendLine(" ORDER BY PMT_EGM_NUMBER ");

                using (SqlCommand _sql_cmd = new SqlCommand(_str_bld.ToString(), Trx.Connection, Trx))
                {

                    _sql_cmd.Parameters.Add("@pPCDConfigurationId", SqlDbType.BigInt).Value = PCDConfigurationId;
                    _sql_cmd.Parameters.Add("@pCommType", SqlDbType.Int).Value = SMIB_COMMUNICATION_TYPE.PULSES;
                    using (SqlDataAdapter _adapter = new SqlDataAdapter(_sql_cmd))
                    {
                        _adapter.TableMappings.Add("SMIB_CONFIGURATION", "SMIB_CONFIGURATION");
                        _adapter.TableMappings.Add("PCD_METERS_TRANSLATION", "PCD_METERS_TRANSLATION");
                        _adapter.Fill(_pcd_configuration);
                    }
                }
            }
            catch (Exception _ex)
            {
                Log.Message(_ex.Message);
            }

            return _pcd_configuration; ;
        }

        //------------------------------------------------------------------------------
        // PURPOSE : Read SAS Meters
        //
        //  PARAMS :
        //      - INPUT : SqlTransaction transaction, String MetersSelected filter by Code's list
        //
        //      - OUTPUT :
        //
        // RETURNS : Filled Datatable
        //
        //   NOTES :
        //  
        public static DataTable ReadSASMeters(SqlTransaction Trx, String MetersSelected)
        {
            StringBuilder _str_sql;
            DataTable _meters_catalog;

            _meters_catalog = new DataTable();
            try
            {
                _str_sql = new StringBuilder();

                _str_sql.AppendLine("  SELECT   SMCG_GROUP_ID                                               ");
                _str_sql.AppendLine("         , SMCG_METER_CODE                                             ");
                _str_sql.AppendLine("         , SMC_DESCRIPTION                                             ");
                _str_sql.AppendLine("         , SMG_NAME                                                    ");
                _str_sql.AppendLine("         , ISNULL(SMC_NAME, '') AS CATALOG_NAME                        ");
                _str_sql.AppendLine("    FROM   SAS_METERS_CATALOG_PER_GROUP                                ");
                _str_sql.AppendLine("   INNER   JOIN SAS_METERS_CATALOG ON SMC_METER_CODE = SMCG_METER_CODE ");
                _str_sql.AppendLine("   INNER   JOIN SAS_METERS_GROUPS ON SMCG_GROUP_ID = SMG_GROUP_ID      ");
                _str_sql.AppendLine("   WHERE   SMCG_GROUP_ID = 3                                           ");

                if (!String.IsNullOrEmpty(MetersSelected))
                {
                    _str_sql.AppendLine("   AND   SMCG_METER_CODE IN (" + MetersSelected + " )");
                }
                _str_sql.AppendLine("ORDER BY   SMCG_GROUP_ID ");

                using (SqlCommand _sql_cmd = new SqlCommand(_str_sql.ToString(), Trx.Connection, Trx))
                {
                    using (SqlDataAdapter _adapter = new SqlDataAdapter(_sql_cmd))
                    {
                        _adapter.Fill(_meters_catalog);
                    }
                }
            }
            catch (Exception _ex)
            {
                Log.Message(_ex.Message);
            }

            return _meters_catalog; ;
        }

        //------------------------------------------------------------------------------
        // PURPOSE: Get meters of Terminal
        //
        //  PARAMS:
        //      - INPUT:
        //        - TerminalId
        //        - OutBillsCode
        //        - OutCoinsCode
        //        - OutCentsCode
        //        - Trx : SQL Transaction
        //      - OUTPUT:
        //        - TerminalMeters
        //
        // RETURNS :
        //      - True:  Successfully
        //      - False: Otherwise
        //
        //   NOTES:
        //
        public static Boolean ReadTerminalMeters(Int32 TerminalId, String MeterCodes, out List<TerminalMeter> TerminalMeters, SqlTransaction Trx)
        {
            StringBuilder _sb;

            TerminalMeters = new List<TerminalMeter>();

            try
            {
                _sb = new StringBuilder();
                _sb.AppendLine("SELECT   TSM_TERMINAL_ID                                                                            ");
                _sb.AppendLine("        ,TSM_LAST_REPORTED                                                                          ");
                _sb.AppendLine("        ,TSM_LAST_MODIFIED                                                                          ");
                _sb.AppendLine("        ,SMCG_METER_CODE                                                                            ");
                _sb.AppendLine("        ,SMG_NAME                                                                                   ");
                _sb.AppendLine("        ,SMC_DESCRIPTION                                                                            ");
                _sb.AppendLine("        ,TSM_METER_VALUE                                                                            ");
                _sb.AppendLine("        ,TSM_METER_MAX_VALUE                                                                        ");
                _sb.AppendLine("        ,TSM_SAS_ACCOUNTING_DENOM                                                                   ");
                // SMIB_COMMUNICATION_TYPE.PULSES(3) OR SMIB_COMMUNICATION_TYPE.SAS_IGT_POKER(4) == PCD
                // SMIB_COMMUNICATION_TYPE.SAS(2), SMIB_COMMUNICATION_TYPE.WCP(1), SMIB_COMMUNICATION_TYPE.NONE(0)
                //_sb.AppendLine("        ,TE_SMIB2EGM_COMM_TYPE                                                                      ");
                _sb.AppendLine("FROM    [SAS_METERS_CATALOG_PER_GROUP]                                                              ");
                _sb.AppendLine("        INNER JOIN SAS_METERS_GROUPS ON SMG_GROUP_ID = SMCG_GROUP_ID                                ");
                _sb.AppendLine("        INNER JOIN SAS_METERS_CATALOG ON SMC_METER_CODE = SMCG_METER_CODE                           ");
                _sb.AppendLine("        INNER JOIN TERMINAL_SAS_METERS ON TSM_METER_CODE = SMCG_METER_CODE AND TSM_DENOMINATION = 0 "); // SUMA DE DISTINTAS DENOMINACIONES
                _sb.AppendLine("        INNER JOIN TERMINALS ON TE_TERMINAL_ID = TSM_TERMINAL_ID                                    ");

                _sb.AppendLine("WHERE   SMCG_GROUP_ID = @pMeterGroupId                                                              "); // SAS_METERS_GROUPS.IDX_SMG_METERS_REPORTED
                _sb.AppendLine("        AND TERMINAL_SAS_METERS.TSM_TERMINAL_ID = @pTerminalId                                      ");
                _sb.AppendFormat("        AND SMCG_METER_CODE IN ({0})                                                               ", MeterCodes);
                _sb.AppendLine("                                                                                                    ");

                using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
                {
                    _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
                    _cmd.Parameters.Add("@pMeterGroupId", SqlDbType.Int).Value = (Int32)SAS_METERS_GROUPS.IDX_SMG_METERS_REPORTED;

                    using (SqlDataReader _reader = _cmd.ExecuteReader())
                    {
                        while (_reader.Read())
                        {
                            TerminalMeter _meter = new TerminalMeter();

                            _meter.TerminalId = _reader.GetInt32(0);
                            _meter.LastReported = _reader.GetDateTime(1);
                            _meter.LastModified = _reader.IsDBNull(2) ? (DateTime?)null : _reader.GetDateTime(2);
                            _meter.MeterCode = _reader.GetInt32(3);
                            _meter.GroupName = _reader.IsDBNull(4) ? null : _reader.GetString(4);
                            _meter.CatalogDescription = _reader.IsDBNull(5) ? null : _reader.GetString(5);
                            _meter.MeterValue = _reader.GetInt64(6);
                            _meter.MeterMaxValue = _reader.GetInt64(7);
                            _meter.MeterSasAccountDenom = _reader.GetInt64(8);

                            TerminalMeters.Add(_meter);
                        }

                        return true;
                    }
                }
            }
            catch (SqlException _sql_ex)
            {
                if (_sql_ex.Number != CONNECTION_EXCEPTION_TIME_OUT)
                {
                    Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
                }
            }
            catch (Exception _ex)
            {
                Log.Exception(_ex);
            }

            return false;
        }

        public static string GetMeterXml(Object Code, Object Value, Object Action)
        {
            StringBuilder _sb;
            String _value;

            _value = Value.ToString();

            if (Value == null || String.IsNullOrEmpty(Value.ToString()))
                _value = TerminalMetersInfo.NOT_READED_METER_VALUE.ToString();

            if (Action != null && (Byte)Action == (Byte)TerminalMeterAction.ENUM_PCD_UPDATE_METERS_ACTION_RESET)
                _value = string.Empty;


            _sb = new StringBuilder(String.Format("<Meter Code=\"{0}\" Value=\"{1}\"", Code, _value));

            if (Action != null)
                _sb.AppendFormat(" Action=\"{0}\"", Action);

            _sb.Append(" />");

            return _sb.ToString();
        }

        public static string GetMeterXml(TerminalMeter TerminalMeter)
        {
            return GetMeterXml(TerminalMeter.MeterCode, TerminalMeter.MeterValue, null);
        }

        public static string GetMetersXml(IEnumerable<TerminalMeter> TerminalMeters)
        {
            StringBuilder _sb;

            _sb = new StringBuilder("<Meters>");

            foreach (TerminalMeter _meter in TerminalMeters)
            {
                _sb.Append(GetMeterXml(_meter));
            }
            _sb.Append("</Meters>");
            return _sb.ToString();
        }

        #endregion // Public methods
    }

    public class TerminalMeter
    {
        #region Properties

        public Int32 TerminalId { get; set; }
        public DateTime LastReported { get; set; }
        public DateTime? LastModified { get; set; }
        public Int32 MeterCode { get; set; }
        public String GroupName { get; set; }
        public String CatalogDescription { get; set; }
        public Int64 MeterValue { get; set; }
        public Int64 MeterMaxValue { get; set; }
        public Decimal MeterSasAccountDenom { get; set; }

        #endregion // Properties
    }


    // Copied from Service (CPP)
    public enum TerminalMeterAction : byte
    {
        ENUM_PCD_UPDATE_METERS_ACTION_SET = 0,
        ENUM_PCD_UPDATE_METERS_ACTION_FIX = 1,
        ENUM_PCD_UPDATE_METERS_ACTION_RESET = 2,
    };
}
