﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: FBMWebService.cs
// 
//   DESCRIPTION: Proxy of web service of FBM 
// 
//        AUTHOR: David Perelló
// 
// CREATION DATE: 19-APR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-APR-2017 DPC    First release (Header).
// 02-OCT-2017 DPC    Update proxy
// 23-OCT-2017 AMF    Update proxy
//------------------------------------------------------------------------------

namespace FBM {
    using System.Xml.Serialization;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Diagnostics;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="FBMWebServiceSoap", Namespace="http://tempuri.org/")]
    public partial class FBMWebService : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback FS2S_SessionStatusOperationCompleted;
        
        private System.Threading.SendOrPostCallback FS2S_OpenSessionOperationCompleted;
        
        private System.Threading.SendOrPostCallback FS2S_UnblockTerminalsOperationCompleted;
        
        private System.Threading.SendOrPostCallback FS2S_BlockTerminalsOperationCompleted;
        
        private System.Threading.SendOrPostCallback FS2S_CloseSessionOperationCompleted;
        
        private System.Threading.SendOrPostCallback FS2S_HandPaymentPOSOperationCompleted;
        
        private System.Threading.SendOrPostCallback FS2S_CloseSessionConfirmationOperationCompleted;
        
        private System.Threading.SendOrPostCallback FS2S_ExpiredAccountsOperationCompleted;
        
        /// <remarks/>
        public FBMWebService() {
            this.Url = "http://25.81.82.197/WebService/FBMWService/FBMWebService.asmx";
        }
        
        /// <remarks/>
        public event FS2S_SessionStatusCompletedEventHandler FS2S_SessionStatusCompleted;
        
        /// <remarks/>
        public event FS2S_OpenSessionCompletedEventHandler FS2S_OpenSessionCompleted;
        
        /// <remarks/>
        public event FS2S_UnblockTerminalsCompletedEventHandler FS2S_UnblockTerminalsCompleted;
        
        /// <remarks/>
        public event FS2S_BlockTerminalsCompletedEventHandler FS2S_BlockTerminalsCompleted;
        
        /// <remarks/>
        public event FS2S_CloseSessionCompletedEventHandler FS2S_CloseSessionCompleted;
        
        /// <remarks/>
        public event FS2S_HandPaymentPOSCompletedEventHandler FS2S_HandPaymentPOSCompleted;
        
        /// <remarks/>
        public event FS2S_CloseSessionConfirmationCompletedEventHandler FS2S_CloseSessionConfirmationCompleted;
        
        /// <remarks/>
        public event FS2S_ExpiredAccountsCompletedEventHandler FS2S_ExpiredAccountsCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/FS2S_SessionStatus", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public SessionStatusResp FS2S_SessionStatus(string FBMEstablishmentCode) {
            object[] results = this.Invoke("FS2S_SessionStatus", new object[] {
                        FBMEstablishmentCode});
            return ((SessionStatusResp)(results[0]));
        }
        
        /// <remarks/>
        public System.IAsyncResult BeginFS2S_SessionStatus(string FBMEstablishmentCode, System.AsyncCallback callback, object asyncState) {
            return this.BeginInvoke("FS2S_SessionStatus", new object[] {
                        FBMEstablishmentCode}, callback, asyncState);
        }
        
        /// <remarks/>
        public SessionStatusResp EndFS2S_SessionStatus(System.IAsyncResult asyncResult) {
            object[] results = this.EndInvoke(asyncResult);
            return ((SessionStatusResp)(results[0]));
        }
        
        /// <remarks/>
        public void FS2S_SessionStatusAsync(string FBMEstablishmentCode) {
            this.FS2S_SessionStatusAsync(FBMEstablishmentCode, null);
        }
        
        /// <remarks/>
        public void FS2S_SessionStatusAsync(string FBMEstablishmentCode, object userState) {
            if ((this.FS2S_SessionStatusOperationCompleted == null)) {
                this.FS2S_SessionStatusOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFS2S_SessionStatusOperationCompleted);
            }
            this.InvokeAsync("FS2S_SessionStatus", new object[] {
                        FBMEstablishmentCode}, this.FS2S_SessionStatusOperationCompleted, userState);
        }
        
        private void OnFS2S_SessionStatusOperationCompleted(object arg) {
            if ((this.FS2S_SessionStatusCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FS2S_SessionStatusCompleted(this, new FS2S_SessionStatusCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/FS2S_OpenSession", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public OpenSessionResp FS2S_OpenSession(string FBMEstablishmentCode, string DateTime, decimal TotalValueAccounts, decimal TotalValueAccountsPromo) {
            object[] results = this.Invoke("FS2S_OpenSession", new object[] {
                        FBMEstablishmentCode,
                        DateTime,
                        TotalValueAccounts,
                        TotalValueAccountsPromo});
            return ((OpenSessionResp)(results[0]));
        }
        
        /// <remarks/>
        public System.IAsyncResult BeginFS2S_OpenSession(string FBMEstablishmentCode, string DateTime, decimal TotalValueAccounts, decimal TotalValueAccountsPromo, System.AsyncCallback callback, object asyncState) {
            return this.BeginInvoke("FS2S_OpenSession", new object[] {
                        FBMEstablishmentCode,
                        DateTime,
                        TotalValueAccounts,
                        TotalValueAccountsPromo}, callback, asyncState);
        }
        
        /// <remarks/>
        public OpenSessionResp EndFS2S_OpenSession(System.IAsyncResult asyncResult) {
            object[] results = this.EndInvoke(asyncResult);
            return ((OpenSessionResp)(results[0]));
        }
        
        /// <remarks/>
        public void FS2S_OpenSessionAsync(string FBMEstablishmentCode, string DateTime, decimal TotalValueAccounts, decimal TotalValueAccountsPromo) {
            this.FS2S_OpenSessionAsync(FBMEstablishmentCode, DateTime, TotalValueAccounts, TotalValueAccountsPromo, null);
        }
        
        /// <remarks/>
        public void FS2S_OpenSessionAsync(string FBMEstablishmentCode, string DateTime, decimal TotalValueAccounts, decimal TotalValueAccountsPromo, object userState) {
            if ((this.FS2S_OpenSessionOperationCompleted == null)) {
                this.FS2S_OpenSessionOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFS2S_OpenSessionOperationCompleted);
            }
            this.InvokeAsync("FS2S_OpenSession", new object[] {
                        FBMEstablishmentCode,
                        DateTime,
                        TotalValueAccounts,
                        TotalValueAccountsPromo}, this.FS2S_OpenSessionOperationCompleted, userState);
        }
        
        private void OnFS2S_OpenSessionOperationCompleted(object arg) {
            if ((this.FS2S_OpenSessionCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FS2S_OpenSessionCompleted(this, new FS2S_OpenSessionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/FS2S_UnblockTerminals", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public UnblockTerminalsResp FS2S_UnblockTerminals(string FBMEstablishmentCode, long FBMServerId) {
            object[] results = this.Invoke("FS2S_UnblockTerminals", new object[] {
                        FBMEstablishmentCode,
                        FBMServerId});
            return ((UnblockTerminalsResp)(results[0]));
        }
        
        /// <remarks/>
        public System.IAsyncResult BeginFS2S_UnblockTerminals(string FBMEstablishmentCode, long FBMServerId, System.AsyncCallback callback, object asyncState) {
            return this.BeginInvoke("FS2S_UnblockTerminals", new object[] {
                        FBMEstablishmentCode,
                        FBMServerId}, callback, asyncState);
        }
        
        /// <remarks/>
        public UnblockTerminalsResp EndFS2S_UnblockTerminals(System.IAsyncResult asyncResult) {
            object[] results = this.EndInvoke(asyncResult);
            return ((UnblockTerminalsResp)(results[0]));
        }
        
        /// <remarks/>
        public void FS2S_UnblockTerminalsAsync(string FBMEstablishmentCode, long FBMServerId) {
            this.FS2S_UnblockTerminalsAsync(FBMEstablishmentCode, FBMServerId, null);
        }
        
        /// <remarks/>
        public void FS2S_UnblockTerminalsAsync(string FBMEstablishmentCode, long FBMServerId, object userState) {
            if ((this.FS2S_UnblockTerminalsOperationCompleted == null)) {
                this.FS2S_UnblockTerminalsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFS2S_UnblockTerminalsOperationCompleted);
            }
            this.InvokeAsync("FS2S_UnblockTerminals", new object[] {
                        FBMEstablishmentCode,
                        FBMServerId}, this.FS2S_UnblockTerminalsOperationCompleted, userState);
        }
        
        private void OnFS2S_UnblockTerminalsOperationCompleted(object arg) {
            if ((this.FS2S_UnblockTerminalsCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FS2S_UnblockTerminalsCompleted(this, new FS2S_UnblockTerminalsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/FS2S_BlockTerminals", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public BlockTerminalsResp FS2S_BlockTerminals(string FBMEstablishmentCode, long FBMServerId) {
            object[] results = this.Invoke("FS2S_BlockTerminals", new object[] {
                        FBMEstablishmentCode,
                        FBMServerId});
            return ((BlockTerminalsResp)(results[0]));
        }
        
        /// <remarks/>
        public System.IAsyncResult BeginFS2S_BlockTerminals(string FBMEstablishmentCode, long FBMServerId, System.AsyncCallback callback, object asyncState) {
            return this.BeginInvoke("FS2S_BlockTerminals", new object[] {
                        FBMEstablishmentCode,
                        FBMServerId}, callback, asyncState);
        }
        
        /// <remarks/>
        public BlockTerminalsResp EndFS2S_BlockTerminals(System.IAsyncResult asyncResult) {
            object[] results = this.EndInvoke(asyncResult);
            return ((BlockTerminalsResp)(results[0]));
        }
        
        /// <remarks/>
        public void FS2S_BlockTerminalsAsync(string FBMEstablishmentCode, long FBMServerId) {
            this.FS2S_BlockTerminalsAsync(FBMEstablishmentCode, FBMServerId, null);
        }
        
        /// <remarks/>
        public void FS2S_BlockTerminalsAsync(string FBMEstablishmentCode, long FBMServerId, object userState) {
            if ((this.FS2S_BlockTerminalsOperationCompleted == null)) {
                this.FS2S_BlockTerminalsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFS2S_BlockTerminalsOperationCompleted);
            }
            this.InvokeAsync("FS2S_BlockTerminals", new object[] {
                        FBMEstablishmentCode,
                        FBMServerId}, this.FS2S_BlockTerminalsOperationCompleted, userState);
        }
        
        private void OnFS2S_BlockTerminalsOperationCompleted(object arg) {
            if ((this.FS2S_BlockTerminalsCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FS2S_BlockTerminalsCompleted(this, new FS2S_BlockTerminalsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/FS2S_CloseSession", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public CloseSessionResp FS2S_CloseSession(
                    string FBMEstablishmentCode, 
                    long FBMServerId, 
                    decimal TotalValueAccounts, 
                    decimal TotalValueAccountsPromo, 
                    decimal TotalSessionExpiredAccounts, 
                    decimal TotalSessionExpiredAccountsPromo, 
                    decimal TotalSessionSalesPOS, 
                    decimal TotalSessionPromoInPos, 
                    decimal TotalSessionPaymentsPOS, 
                    decimal TotalSessionPromoOutPOS, 
                    decimal TotalSessionJackpotPaymentPOS, 
                    decimal TotalSessionEletronicTransferIn, 
                    decimal TotalSessionEletronicTransferInPromo, 
                    decimal TotalSessionEletronicTransferOut, 
                    decimal TotalSessionEletronicTransferOutPromo, 
                    decimal TotalSessionHandPay, 
                    decimal TotalSessionCoinIn, 
                    decimal TotalSessionCoinOut, 
                    decimal TotalSessionJackpot, 
                    decimal TotalSessionNet) {
            object[] results = this.Invoke("FS2S_CloseSession", new object[] {
                        FBMEstablishmentCode,
                        FBMServerId,
                        TotalValueAccounts,
                        TotalValueAccountsPromo,
                        TotalSessionExpiredAccounts,
                        TotalSessionExpiredAccountsPromo,
                        TotalSessionSalesPOS,
                        TotalSessionPromoInPos,
                        TotalSessionPaymentsPOS,
                        TotalSessionPromoOutPOS,
                        TotalSessionJackpotPaymentPOS,
                        TotalSessionEletronicTransferIn,
                        TotalSessionEletronicTransferInPromo,
                        TotalSessionEletronicTransferOut,
                        TotalSessionEletronicTransferOutPromo,
                        TotalSessionHandPay,
                        TotalSessionCoinIn,
                        TotalSessionCoinOut,
                        TotalSessionJackpot,
                        TotalSessionNet});
            return ((CloseSessionResp)(results[0]));
        }
        
        /// <remarks/>
        public System.IAsyncResult BeginFS2S_CloseSession(
                    string FBMEstablishmentCode, 
                    long FBMServerId, 
                    decimal TotalValueAccounts, 
                    decimal TotalValueAccountsPromo, 
                    decimal TotalSessionExpiredAccounts, 
                    decimal TotalSessionExpiredAccountsPromo, 
                    decimal TotalSessionSalesPOS, 
                    decimal TotalSessionPromoInPos, 
                    decimal TotalSessionPaymentsPOS, 
                    decimal TotalSessionPromoOutPOS, 
                    decimal TotalSessionJackpotPaymentPOS, 
                    decimal TotalSessionEletronicTransferIn, 
                    decimal TotalSessionEletronicTransferInPromo, 
                    decimal TotalSessionEletronicTransferOut, 
                    decimal TotalSessionEletronicTransferOutPromo, 
                    decimal TotalSessionHandPay, 
                    decimal TotalSessionCoinIn, 
                    decimal TotalSessionCoinOut, 
                    decimal TotalSessionJackpot, 
                    decimal TotalSessionNet, 
                    System.AsyncCallback callback, 
                    object asyncState) {
            return this.BeginInvoke("FS2S_CloseSession", new object[] {
                        FBMEstablishmentCode,
                        FBMServerId,
                        TotalValueAccounts,
                        TotalValueAccountsPromo,
                        TotalSessionExpiredAccounts,
                        TotalSessionExpiredAccountsPromo,
                        TotalSessionSalesPOS,
                        TotalSessionPromoInPos,
                        TotalSessionPaymentsPOS,
                        TotalSessionPromoOutPOS,
                        TotalSessionJackpotPaymentPOS,
                        TotalSessionEletronicTransferIn,
                        TotalSessionEletronicTransferInPromo,
                        TotalSessionEletronicTransferOut,
                        TotalSessionEletronicTransferOutPromo,
                        TotalSessionHandPay,
                        TotalSessionCoinIn,
                        TotalSessionCoinOut,
                        TotalSessionJackpot,
                        TotalSessionNet}, callback, asyncState);
        }
        
        /// <remarks/>
        public CloseSessionResp EndFS2S_CloseSession(System.IAsyncResult asyncResult) {
            object[] results = this.EndInvoke(asyncResult);
            return ((CloseSessionResp)(results[0]));
        }
        
        /// <remarks/>
        public void FS2S_CloseSessionAsync(
                    string FBMEstablishmentCode, 
                    long FBMServerId, 
                    decimal TotalValueAccounts, 
                    decimal TotalValueAccountsPromo, 
                    decimal TotalSessionExpiredAccounts, 
                    decimal TotalSessionExpiredAccountsPromo, 
                    decimal TotalSessionSalesPOS, 
                    decimal TotalSessionPromoInPos, 
                    decimal TotalSessionPaymentsPOS, 
                    decimal TotalSessionPromoOutPOS, 
                    decimal TotalSessionJackpotPaymentPOS, 
                    decimal TotalSessionEletronicTransferIn, 
                    decimal TotalSessionEletronicTransferInPromo, 
                    decimal TotalSessionEletronicTransferOut, 
                    decimal TotalSessionEletronicTransferOutPromo, 
                    decimal TotalSessionHandPay, 
                    decimal TotalSessionCoinIn, 
                    decimal TotalSessionCoinOut, 
                    decimal TotalSessionJackpot, 
                    decimal TotalSessionNet) {
            this.FS2S_CloseSessionAsync(FBMEstablishmentCode, FBMServerId, TotalValueAccounts, TotalValueAccountsPromo, TotalSessionExpiredAccounts, TotalSessionExpiredAccountsPromo, TotalSessionSalesPOS, TotalSessionPromoInPos, TotalSessionPaymentsPOS, TotalSessionPromoOutPOS, TotalSessionJackpotPaymentPOS, TotalSessionEletronicTransferIn, TotalSessionEletronicTransferInPromo, TotalSessionEletronicTransferOut, TotalSessionEletronicTransferOutPromo, TotalSessionHandPay, TotalSessionCoinIn, TotalSessionCoinOut, TotalSessionJackpot, TotalSessionNet, null);
        }
        
        /// <remarks/>
        public void FS2S_CloseSessionAsync(
                    string FBMEstablishmentCode, 
                    long FBMServerId, 
                    decimal TotalValueAccounts, 
                    decimal TotalValueAccountsPromo, 
                    decimal TotalSessionExpiredAccounts, 
                    decimal TotalSessionExpiredAccountsPromo, 
                    decimal TotalSessionSalesPOS, 
                    decimal TotalSessionPromoInPos, 
                    decimal TotalSessionPaymentsPOS, 
                    decimal TotalSessionPromoOutPOS, 
                    decimal TotalSessionJackpotPaymentPOS, 
                    decimal TotalSessionEletronicTransferIn, 
                    decimal TotalSessionEletronicTransferInPromo, 
                    decimal TotalSessionEletronicTransferOut, 
                    decimal TotalSessionEletronicTransferOutPromo, 
                    decimal TotalSessionHandPay, 
                    decimal TotalSessionCoinIn, 
                    decimal TotalSessionCoinOut, 
                    decimal TotalSessionJackpot, 
                    decimal TotalSessionNet, 
                    object userState) {
            if ((this.FS2S_CloseSessionOperationCompleted == null)) {
                this.FS2S_CloseSessionOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFS2S_CloseSessionOperationCompleted);
            }
            this.InvokeAsync("FS2S_CloseSession", new object[] {
                        FBMEstablishmentCode,
                        FBMServerId,
                        TotalValueAccounts,
                        TotalValueAccountsPromo,
                        TotalSessionExpiredAccounts,
                        TotalSessionExpiredAccountsPromo,
                        TotalSessionSalesPOS,
                        TotalSessionPromoInPos,
                        TotalSessionPaymentsPOS,
                        TotalSessionPromoOutPOS,
                        TotalSessionJackpotPaymentPOS,
                        TotalSessionEletronicTransferIn,
                        TotalSessionEletronicTransferInPromo,
                        TotalSessionEletronicTransferOut,
                        TotalSessionEletronicTransferOutPromo,
                        TotalSessionHandPay,
                        TotalSessionCoinIn,
                        TotalSessionCoinOut,
                        TotalSessionJackpot,
                        TotalSessionNet}, this.FS2S_CloseSessionOperationCompleted, userState);
        }
        
        private void OnFS2S_CloseSessionOperationCompleted(object arg) {
            if ((this.FS2S_CloseSessionCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FS2S_CloseSessionCompleted(this, new FS2S_CloseSessionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/FS2S_HandPaymentPOS", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public HandPaymentPOSResp FS2S_HandPaymentPOS(string FBMEstablishmentCode, long FBMServerId, long SessionId, decimal ManualPaymentValue) {
            object[] results = this.Invoke("FS2S_HandPaymentPOS", new object[] {
                        FBMEstablishmentCode,
                        FBMServerId,
                        SessionId,
                        ManualPaymentValue});
            return ((HandPaymentPOSResp)(results[0]));
        }
        
        /// <remarks/>
        public System.IAsyncResult BeginFS2S_HandPaymentPOS(string FBMEstablishmentCode, long FBMServerId, long SessionId, decimal ManualPaymentValue, System.AsyncCallback callback, object asyncState) {
            return this.BeginInvoke("FS2S_HandPaymentPOS", new object[] {
                        FBMEstablishmentCode,
                        FBMServerId,
                        SessionId,
                        ManualPaymentValue}, callback, asyncState);
        }
        
        /// <remarks/>
        public HandPaymentPOSResp EndFS2S_HandPaymentPOS(System.IAsyncResult asyncResult) {
            object[] results = this.EndInvoke(asyncResult);
            return ((HandPaymentPOSResp)(results[0]));
        }
        
        /// <remarks/>
        public void FS2S_HandPaymentPOSAsync(string FBMEstablishmentCode, long FBMServerId, long SessionId, decimal ManualPaymentValue) {
            this.FS2S_HandPaymentPOSAsync(FBMEstablishmentCode, FBMServerId, SessionId, ManualPaymentValue, null);
        }
        
        /// <remarks/>
        public void FS2S_HandPaymentPOSAsync(string FBMEstablishmentCode, long FBMServerId, long SessionId, decimal ManualPaymentValue, object userState) {
            if ((this.FS2S_HandPaymentPOSOperationCompleted == null)) {
                this.FS2S_HandPaymentPOSOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFS2S_HandPaymentPOSOperationCompleted);
            }
            this.InvokeAsync("FS2S_HandPaymentPOS", new object[] {
                        FBMEstablishmentCode,
                        FBMServerId,
                        SessionId,
                        ManualPaymentValue}, this.FS2S_HandPaymentPOSOperationCompleted, userState);
        }
        
        private void OnFS2S_HandPaymentPOSOperationCompleted(object arg) {
            if ((this.FS2S_HandPaymentPOSCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FS2S_HandPaymentPOSCompleted(this, new FS2S_HandPaymentPOSCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/FS2S_CloseSessionConfirmation", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public CloseSessionConfirmationResp FS2S_CloseSessionConfirmation(string FBMEstablishmentCode, long FBMServerId) {
            object[] results = this.Invoke("FS2S_CloseSessionConfirmation", new object[] {
                        FBMEstablishmentCode,
                        FBMServerId});
            return ((CloseSessionConfirmationResp)(results[0]));
        }
        
        /// <remarks/>
        public System.IAsyncResult BeginFS2S_CloseSessionConfirmation(string FBMEstablishmentCode, long FBMServerId, System.AsyncCallback callback, object asyncState) {
            return this.BeginInvoke("FS2S_CloseSessionConfirmation", new object[] {
                        FBMEstablishmentCode,
                        FBMServerId}, callback, asyncState);
        }
        
        /// <remarks/>
        public CloseSessionConfirmationResp EndFS2S_CloseSessionConfirmation(System.IAsyncResult asyncResult) {
            object[] results = this.EndInvoke(asyncResult);
            return ((CloseSessionConfirmationResp)(results[0]));
        }
        
        /// <remarks/>
        public void FS2S_CloseSessionConfirmationAsync(string FBMEstablishmentCode, long FBMServerId) {
            this.FS2S_CloseSessionConfirmationAsync(FBMEstablishmentCode, FBMServerId, null);
        }
        
        /// <remarks/>
        public void FS2S_CloseSessionConfirmationAsync(string FBMEstablishmentCode, long FBMServerId, object userState) {
            if ((this.FS2S_CloseSessionConfirmationOperationCompleted == null)) {
                this.FS2S_CloseSessionConfirmationOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFS2S_CloseSessionConfirmationOperationCompleted);
            }
            this.InvokeAsync("FS2S_CloseSessionConfirmation", new object[] {
                        FBMEstablishmentCode,
                        FBMServerId}, this.FS2S_CloseSessionConfirmationOperationCompleted, userState);
        }
        
        private void OnFS2S_CloseSessionConfirmationOperationCompleted(object arg) {
            if ((this.FS2S_CloseSessionConfirmationCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FS2S_CloseSessionConfirmationCompleted(this, new FS2S_CloseSessionConfirmationCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/FS2S_ExpiredAccounts", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public ExpiredAccountsResp FS2S_ExpiredAccounts(string FBMEstablishmentCode, long FBMServerId, ExpiredAccounts[] xmlExp) {
            object[] results = this.Invoke("FS2S_ExpiredAccounts", new object[] {
                        FBMEstablishmentCode,
                        FBMServerId,
                        xmlExp});
            return ((ExpiredAccountsResp)(results[0]));
        }
        
        /// <remarks/>
        public System.IAsyncResult BeginFS2S_ExpiredAccounts(string FBMEstablishmentCode, long FBMServerId, ExpiredAccounts[] xmlExp, System.AsyncCallback callback, object asyncState) {
            return this.BeginInvoke("FS2S_ExpiredAccounts", new object[] {
                        FBMEstablishmentCode,
                        FBMServerId,
                        xmlExp}, callback, asyncState);
        }
        
        /// <remarks/>
        public ExpiredAccountsResp EndFS2S_ExpiredAccounts(System.IAsyncResult asyncResult) {
            object[] results = this.EndInvoke(asyncResult);
            return ((ExpiredAccountsResp)(results[0]));
        }
        
        /// <remarks/>
        public void FS2S_ExpiredAccountsAsync(string FBMEstablishmentCode, long FBMServerId, ExpiredAccounts[] xmlExp) {
            this.FS2S_ExpiredAccountsAsync(FBMEstablishmentCode, FBMServerId, xmlExp, null);
        }
        
        /// <remarks/>
        public void FS2S_ExpiredAccountsAsync(string FBMEstablishmentCode, long FBMServerId, ExpiredAccounts[] xmlExp, object userState) {
            if ((this.FS2S_ExpiredAccountsOperationCompleted == null)) {
                this.FS2S_ExpiredAccountsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFS2S_ExpiredAccountsOperationCompleted);
            }
            this.InvokeAsync("FS2S_ExpiredAccounts", new object[] {
                        FBMEstablishmentCode,
                        FBMServerId,
                        xmlExp}, this.FS2S_ExpiredAccountsOperationCompleted, userState);
        }
        
        private void OnFS2S_ExpiredAccountsOperationCompleted(object arg) {
            if ((this.FS2S_ExpiredAccountsCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FS2S_ExpiredAccountsCompleted(this, new FS2S_ExpiredAccountsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
    }
    
    /// <comentarios/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class SessionStatusResp {
        
        private int statusCodeField;
        
        private string statusTextField;
        
        private int sessionStatusField;
        
        private int blockStatusField;
        
        private long fBMSessionIdField;
        
        /// <comentarios/>
        public int StatusCode {
            get {
                return this.statusCodeField;
            }
            set {
                this.statusCodeField = value;
            }
        }
        
        /// <comentarios/>
        public string StatusText {
            get {
                return this.statusTextField;
            }
            set {
                this.statusTextField = value;
            }
        }
        
        /// <comentarios/>
        public int SessionStatus {
            get {
                return this.sessionStatusField;
            }
            set {
                this.sessionStatusField = value;
            }
        }
        
        /// <comentarios/>
        public int BlockStatus {
            get {
                return this.blockStatusField;
            }
            set {
                this.blockStatusField = value;
            }
        }
        
        /// <comentarios/>
        public long FBMSessionId {
            get {
                return this.fBMSessionIdField;
            }
            set {
                this.fBMSessionIdField = value;
            }
        }
    }
    
    /// <comentarios/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class ExpiredAccountsResp {
        
        private int statusCodeField;
        
        private string statusTextField;
        
        /// <comentarios/>
        public int StatusCode {
            get {
                return this.statusCodeField;
            }
            set {
                this.statusCodeField = value;
            }
        }
        
        /// <comentarios/>
        public string StatusText {
            get {
                return this.statusTextField;
            }
            set {
                this.statusTextField = value;
            }
        }
    }
    
    /// <comentarios/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class ExpiredAccounts {
        
        private string trackDataField;
        
        private decimal valueField;
        
        /// <comentarios/>
        public string trackData {
            get {
                return this.trackDataField;
            }
            set {
                this.trackDataField = value;
            }
        }
        
        /// <comentarios/>
        public decimal value {
            get {
                return this.valueField;
            }
            set {
                this.valueField = value;
            }
        }
    }
    
    /// <comentarios/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class CloseSessionConfirmationResp {
        
        private int statusCodeField;
        
        private string statusTextField;
        
        /// <comentarios/>
        public int StatusCode {
            get {
                return this.statusCodeField;
            }
            set {
                this.statusCodeField = value;
            }
        }
        
        /// <comentarios/>
        public string StatusText {
            get {
                return this.statusTextField;
            }
            set {
                this.statusTextField = value;
            }
        }
    }
    
    /// <comentarios/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class HandPaymentPOSResp {
        
        private int statusCodeField;
        
        private string statusTextField;
        
        /// <comentarios/>
        public int StatusCode {
            get {
                return this.statusCodeField;
            }
            set {
                this.statusCodeField = value;
            }
        }
        
        /// <comentarios/>
        public string StatusText {
            get {
                return this.statusTextField;
            }
            set {
                this.statusTextField = value;
            }
        }
    }
    
    /// <comentarios/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class CloseSessionResp {
        
        private int statusCodeField;
        
        private bool errorValidationAccountsField;
        
        private bool errorValidationPromoAccountsField;
        
        private bool erroValidationExpiredAccountField;
        
        private bool errorValidationNETField;
        
        private string statusTextField;
        
        /// <comentarios/>
        public int StatusCode {
            get {
                return this.statusCodeField;
            }
            set {
                this.statusCodeField = value;
            }
        }
        
        /// <comentarios/>
        public bool ErrorValidationAccounts {
            get {
                return this.errorValidationAccountsField;
            }
            set {
                this.errorValidationAccountsField = value;
            }
        }
        
        /// <comentarios/>
        public bool ErrorValidationPromoAccounts {
            get {
                return this.errorValidationPromoAccountsField;
            }
            set {
                this.errorValidationPromoAccountsField = value;
            }
        }
        
        /// <comentarios/>
        public bool ErroValidationExpiredAccount {
            get {
                return this.erroValidationExpiredAccountField;
            }
            set {
                this.erroValidationExpiredAccountField = value;
            }
        }
        
        /// <comentarios/>
        public bool ErrorValidationNET {
            get {
                return this.errorValidationNETField;
            }
            set {
                this.errorValidationNETField = value;
            }
        }
        
        /// <comentarios/>
        public string StatusText {
            get {
                return this.statusTextField;
            }
            set {
                this.statusTextField = value;
            }
        }
    }
    
    /// <comentarios/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class BlockTerminalsResp {
        
        private int statusCodeField;
        
        private string statusTextField;
        
        /// <comentarios/>
        public int StatusCode {
            get {
                return this.statusCodeField;
            }
            set {
                this.statusCodeField = value;
            }
        }
        
        /// <comentarios/>
        public string StatusText {
            get {
                return this.statusTextField;
            }
            set {
                this.statusTextField = value;
            }
        }
    }
    
    /// <comentarios/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class UnblockTerminalsResp {
        
        private int statusCodeField;
        
        private string statusTextField;
        
        /// <comentarios/>
        public int StatusCode {
            get {
                return this.statusCodeField;
            }
            set {
                this.statusCodeField = value;
            }
        }
        
        /// <comentarios/>
        public string StatusText {
            get {
                return this.statusTextField;
            }
            set {
                this.statusTextField = value;
            }
        }
    }
    
    /// <comentarios/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class OpenSessionResp {
        
        private int statusCodeField;
        
        private string statusTextField;
        
        private long fBMSessionIdField;
        
        /// <comentarios/>
        public int StatusCode {
            get {
                return this.statusCodeField;
            }
            set {
                this.statusCodeField = value;
            }
        }
        
        /// <comentarios/>
        public string StatusText {
            get {
                return this.statusTextField;
            }
            set {
                this.statusTextField = value;
            }
        }
        
        /// <comentarios/>
        public long FBMSessionId {
            get {
                return this.fBMSessionIdField;
            }
            set {
                this.fBMSessionIdField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void FS2S_SessionStatusCompletedEventHandler(object sender, FS2S_SessionStatusCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FS2S_SessionStatusCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal FS2S_SessionStatusCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public SessionStatusResp Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((SessionStatusResp)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void FS2S_OpenSessionCompletedEventHandler(object sender, FS2S_OpenSessionCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FS2S_OpenSessionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal FS2S_OpenSessionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public OpenSessionResp Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((OpenSessionResp)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void FS2S_UnblockTerminalsCompletedEventHandler(object sender, FS2S_UnblockTerminalsCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FS2S_UnblockTerminalsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal FS2S_UnblockTerminalsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public UnblockTerminalsResp Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((UnblockTerminalsResp)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void FS2S_BlockTerminalsCompletedEventHandler(object sender, FS2S_BlockTerminalsCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FS2S_BlockTerminalsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal FS2S_BlockTerminalsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public BlockTerminalsResp Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((BlockTerminalsResp)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void FS2S_CloseSessionCompletedEventHandler(object sender, FS2S_CloseSessionCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FS2S_CloseSessionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal FS2S_CloseSessionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public CloseSessionResp Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((CloseSessionResp)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void FS2S_HandPaymentPOSCompletedEventHandler(object sender, FS2S_HandPaymentPOSCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FS2S_HandPaymentPOSCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal FS2S_HandPaymentPOSCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public HandPaymentPOSResp Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((HandPaymentPOSResp)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void FS2S_CloseSessionConfirmationCompletedEventHandler(object sender, FS2S_CloseSessionConfirmationCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FS2S_CloseSessionConfirmationCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal FS2S_CloseSessionConfirmationCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public CloseSessionConfirmationResp Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((CloseSessionConfirmationResp)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    public delegate void FS2S_ExpiredAccountsCompletedEventHandler(object sender, FS2S_ExpiredAccountsCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.3038")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FS2S_ExpiredAccountsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal FS2S_ExpiredAccountsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public ExpiredAccountsResp Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((ExpiredAccountsResp)(this.results[0]));
            }
        }
    }
}
