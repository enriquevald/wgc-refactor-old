﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: FBMWebService.cs
// 
//   DESCRIPTION: Proxy of web service of FBM 
// 
//        AUTHOR: David Perelló
// 
// CREATION DATE: 19-APR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-APR-2017 DPC    First release (Header).
//------------------------------------------------------------------------------

namespace FBM
{
    public partial class FBMWebService
    {
      public FBMWebService(string sUrl)
        : base()
        {
            this.Url = sUrl;
        }
    }
}

