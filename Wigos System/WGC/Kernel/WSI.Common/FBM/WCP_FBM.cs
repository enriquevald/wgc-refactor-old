//------------------------------------------------------------------------------
// Copyright � 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WCP_FBM.cs
// 
//   DESCRIPTION: Open/Close working day FBM
// 
//        AUTHOR: Alberto Marcos
// 
// CREATION DATE: 12-APR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-APR-2017 AMF    First release.
// 05-OCT-2017 DPC    [WIGOS-5481]: FBM - WS - CloseSessionConfirmation
//------------------------------------------------------------------------------
using System;
using System.Threading;
using WSI.Common;
using FBM;
using WSI.Common.FBM;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using WSI.Common.FBM.Classes;

namespace WSI.WCP
{
  public static class WCP_FBM
  {

    #region  " Enum "

    enum SessionStatusCode
    {
      Ok = 0,
      AccessDenied = 1
    }

    enum OpenSessionStatusCode
    {
      Ok = 0,
      ExistsSessionOpen = 1,
      ErrorOpenSession = 2,
      AccessDenied = 3
    }

    enum CloseSessionStatusCode
    {
      Ok = 0,
      ExistUnblockTerminals = 1,
      NotExistsSessionOpen = 2,
      InvalidFBMSessionId = 3,
      AccessDenied = 4,
      CloseError = 5,
      ClosingAlreadyBeingDone = 6
    }

    enum BlockUnblockStatusCode
    {
      Ok = 0,
      Error = 1,
      InvalidFBMSessionId = 2,
      AccessDenied = 3,
      GeneralError = 4
    }

    public enum SessionStatusFBM
    {
      Close = 0,
      Open = 1
    }

    public enum BlockStatusFBM
    {
      Unblocked = 0,
      Blocked = 1
    }

    public enum LogStatus
    {
      Close = 0,
      Open = 1,
      Blocked = 2,
      Unblocked = 3,
    }

    public enum LogType
    {
      CloseSession = 0,
      OpenSession = 1,
      BlockMachine = 2,
      UnblockMachine = 3,
      CloseSessionConfirmation = 4
    }

    public enum CloseSessionConfirmationStatusCode
    {
      Ok = 0,
      NoCloseSessionInProgress = 1,
      Retry = 2,
      InvalidFBMSessionId = 3,
      InvalidFBMEstablishmentCode = 4,
      Error = 5
    }

    public enum ExpiredSessionCode
    {
      Ok = 0,
      FBMSessionIdError = 1,
      FBMEstablishmentError = 2,
      WigosSessionIdError = 3,
    }

    #endregion " Enums "

    #region " Atributes "

    private static FBMWebService m_ws_fbm;
    private static String m_establishment_code;
    private static Int64 m_fbm_session_id;

    #endregion " Atributes "

    #region " Properties "

    private static FBMWebService WsFBM
    {
      get
      {
        if (m_ws_fbm == null) m_ws_fbm = new FBMWebService(GeneralParam.GetString("FBM", "Uri"));

        return m_ws_fbm;
      }
    }

    private static String EstablishmentCode
    {
      get
      {
        if (m_establishment_code == null) m_establishment_code = GeneralParam.GetString("FBM", "EstablishmentCode", "tws");

        return m_establishment_code;
      }
    }

    private static Int64 FBMSessionId
    {
      get { return m_fbm_session_id; }
      set { m_fbm_session_id = value; }
    }

    #endregion " Properties "

    #region " Public Methotds "

    /// <summary>
    /// Initializes FBM thread
    /// </summary>
    public static void Init()
    {
      Thread _thread;

      _thread = new Thread(OpenCloseWorkingDayFBM);
      _thread.Name = "OpenCloseWorkingDayFBM";
      _thread.Start();

    } // Init

    /// <summary>
    /// Get the expected status of FBM machine
    /// </summary>
    /// <param name="ExpectedStatus"></param>
    /// <param name="ExpectedBlock"></param>
    /// <returns></returns>
    public static Boolean GetExpectedStatus(out SessionStatusFBM ExpectedStatus, out BlockStatusFBM ExpectedBlock)
    {
      StringBuilder _sb;
      ExpectedStatus = SessionStatusFBM.Close;
      ExpectedBlock = BlockStatusFBM.Blocked;
      LogStatus _session;
      LogStatus _machine;

      try
      {
        _sb = new StringBuilder();
        _session = LogStatus.Close;
        _machine = LogStatus.Blocked;

        _sb.AppendLine(" SELECT ( SELECT TOP 1   FL_STATUS                           ");
        _sb.AppendLine("                  FROM   FBM_LOG                             ");
        _sb.AppendLine("                 WHERE   FL_TYPE in (@pOpen,@pClose)         ");
        _sb.AppendLine("              ORDER BY   FL_CREATION desc) as SessionStatus, ");
        _sb.AppendLine("        ( SELECT TOP 1   FL_STATUS                           ");
        _sb.AppendLine("                  FROM   FBM_LOG                             ");
        _sb.AppendLine("                 WHERE   FL_TYPE IN (@pBlock,@pUnblock)      ");
        _sb.AppendLine("              ORDER BY   FL_CREATION desc) AS MachineStatus  ");

        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pOpen", SqlDbType.Int).Value = LogStatus.Open;
            _sql_cmd.Parameters.Add("@pClose", SqlDbType.Int).Value = LogStatus.Close;
            _sql_cmd.Parameters.Add("@pBlock", SqlDbType.Int).Value = LogStatus.Blocked;
            _sql_cmd.Parameters.Add("@pUnblock", SqlDbType.Int).Value = LogStatus.Unblocked;

            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              if (_reader.Read())
              {
                _session = (_reader.IsDBNull(_reader.GetOrdinal("SessionStatus"))) ? LogStatus.Close : (LogStatus)_reader["SessionStatus"];
                _machine = (_reader.IsDBNull(_reader.GetOrdinal("MachineStatus"))) ? LogStatus.Blocked : (LogStatus)_reader["MachineStatus"];

                switch (_session) // LogStatus To Session Status FBM
                {
                  case LogStatus.Close:
                    ExpectedStatus = SessionStatusFBM.Close;
                    break;
                  case LogStatus.Open:
                    ExpectedStatus = SessionStatusFBM.Open;
                    break;
                  default:
                    break;
                }

                switch (_machine) // LogStatus To Block Status FBM
                {
                  case LogStatus.Blocked:
                    ExpectedBlock = BlockStatusFBM.Blocked;
                    break;
                  case LogStatus.Unblocked:
                    ExpectedBlock = BlockStatusFBM.Unblocked;
                    break;
                  default:
                    break;
                }

              }
            }
          }
        } // DB_TRX

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Error("WCP_FBM.GetExpectedStatus");
      }
      return false;
    } // GetExpectedStatus

    /// <summary>
    /// Update Expected Status
    /// </summary>
    /// <param name="User"></param>
    /// <param name="ExpectedStatus"></param>
    /// <param name="ExpectedBlock"></param>
    /// <returns></returns>
    public static Boolean UpdateExpectedStatus(String User, out SessionStatusFBM ExpectedStatus, out BlockStatusFBM ExpectedBlock)
    {
      SessionStatusFBM _current_status;
      BlockStatusFBM _current_block;
      Boolean _return;
      _return = true;
      ExpectedStatus = SessionStatusFBM.Close;
      ExpectedBlock = BlockStatusFBM.Blocked;

      if (!GetExpectedStatus(out ExpectedStatus, out ExpectedBlock))
      {
        return false;
      }

      if (!GetCurrentStatus(out _current_status, out _current_block))
      {
        return false;
      }

      if (ExpectedStatus != _current_status)
      {
        switch (_current_status)
        {
          case SessionStatusFBM.Close:
            _return = UpdateFBMLog(LogStatus.Close, LogType.CloseSession, User);
            break;
          case SessionStatusFBM.Open:
            _return = UpdateFBMLog(LogStatus.Open, LogType.OpenSession, User);
            break;
        }

        ExpectedStatus = _current_status;
      }

      if (_return && ExpectedBlock != _current_block)
      {
        switch (_current_block)
        {
          case BlockStatusFBM.Blocked:
            _return = UpdateFBMLog(LogStatus.Blocked, LogType.BlockMachine, User);
            break;
          case BlockStatusFBM.Unblocked:
            _return = UpdateFBMLog(LogStatus.Unblocked, LogType.UnblockMachine, User);
            break;
        }

        ExpectedBlock = _current_block;
      }

      return _return;
    } // UpdateExpectedStatus

    /// <summary>
    /// Get the current status of FBM machine
    /// </summary>
    /// <param name="CurrentStatus"></param>
    /// <param name="CurrentBlock"></param>
    /// <returns></returns>
    public static Boolean GetCurrentStatus(out SessionStatusFBM CurrentStatus, out BlockStatusFBM CurrentBlock)
    {
      SessionStatusResp _resp;

      CurrentStatus = SessionStatusFBM.Open;
      CurrentBlock = BlockStatusFBM.Unblocked;
      FBMSessionId = 0;

      try
      {

        _resp = WsFBM.FS2S_SessionStatus(EstablishmentCode);

        if ((SessionStatusCode)_resp.StatusCode != SessionStatusCode.Ok)
        {
          Log.Warning(String.Format("WCP_FBM.FS2S_SessionStatus: Code: {0}. Description: {1}", _resp.StatusCode, _resp.StatusText));

          return false;
        }

        CurrentStatus = (SessionStatusFBM)_resp.SessionStatus;
        CurrentBlock = (BlockStatusFBM)_resp.BlockStatus;
        FBMSessionId = _resp.FBMSessionId;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Warning("WCP_FBM.GetCurrentStatus: Exception");
        Log.Exception(_ex);
      }

      return false;
    } // GetCurrentStatus

    #endregion " Public Methotds "

    #region " Private Methotds "

    /// <summary>
    /// Open/Close working day FBM
    /// </summary>
    private static void OpenCloseWorkingDayFBM()
    {
      Int32 _sleep;
      Boolean _show_run_message;
      Boolean _current_run_status;
      Boolean _handled_error;
      SessionStatusFBM _expected_status;
      SessionStatusFBM _current_status;
      BlockStatusFBM _expected_block;
      BlockStatusFBM _current_block;

      _sleep = 0;
      _show_run_message = true;
      _current_run_status = !GeneralParam.GetBoolean("FBM", "Enabled", false);

      while (true)
      {
        try
        {
          Thread.Sleep(_sleep);

          if (!GeneralParam.GetBoolean("FBM", "Enabled", false))
          {
            if (_show_run_message || _current_run_status) Log.Message("WCP_FBM.Running: False");

            _show_run_message = false;
            _current_run_status = false;
            _sleep = 60 * 1000;

            continue;
          }
          else if (!_current_run_status || _show_run_message)
          {
            Log.Message("WCP_FBM.Running: True");
            _current_run_status = true;
            _show_run_message = false;
          }

          // Default: 60 sec. Min: 10 sec. Max: 5 min
          _sleep = GeneralParam.GetInt32("FBM", "CheckIntervalSeconds ", 60, 10, 300) * 1000;

          // Only the service running as 'Principal' will execute the tasks.
          if (!Services.IsPrincipal("WCP"))
          {
            continue;
          }

          if (!GetExpectedStatus(out _expected_status, out _expected_block))
          {
            // On error, wait 10 sec
            _sleep = 10000;

            continue;
          }

          if (!GetCurrentStatus(out _current_status, out _current_block))
          {
            // On error, wait 10 sec
            _sleep = 10000;

            continue;
          }

          if (_expected_status == _current_status && _expected_block == _current_block)
          {
            continue;
          }

          switch (_expected_status)
          {
            case SessionStatusFBM.Open:
              if (!OpenSessionFBM(_current_status, _current_block, ""))
              {
                // On error, wait 10 sec
                _sleep = 10000;
              }
              break;

            case SessionStatusFBM.Close:
              if (!CloseSessionFBM(_current_status, _current_block, "", out _handled_error))
              {
                Log.Error("WCP_FBM.OpenCloseWorkingDayFBM: Unhandled exception");

              }

              if (_handled_error)
              {
                Log.Error("WCP_FBM.OpenCloseWorkingDayFBM: Handled exception ");
              }

              // On error, wait 10 sec
              _sleep = 10000;

              break;
          }
        }
        catch (Exception _ex)
        {
          Log.Warning("WCP_FBM.OpenCloseWorkingDayFBM: Exception");
          Log.Exception(_ex);
        }
      } // while
    } // OpenCloseWorkingDayFBM

    /// <summary>
    /// Expire Accounts FBM
    /// </summary>
    /// <param name="CurrentStatus"></param>
    /// <param name="CurrentBlock"></param>
    /// <param name="User"></param>
    /// <returns></returns>
    public static Boolean ExpireAccountsFBM(SessionStatusFBM CurrentStatus, BlockStatusFBM CurrentBlock, String User)
    {
      FBM_ExpiredAccountRequest _expired_req;
      ExpiredAccountsResp _exp_resp;
      String _error;

      _error = String.Empty;

      if (CurrentStatus == SessionStatusFBM.Close)
      {
        return true;
      }

      _expired_req = new FBM_ExpiredAccountRequest();

      _expired_req.GetExpiredAccounts(EstablishmentCode, FBMSessionId);

      _exp_resp = WsFBM.FS2S_ExpiredAccounts(_expired_req.FBMEstablishmentCode
                                            , _expired_req.FBMServerId
                                            , _expired_req.Expireds);

      if ((ExpiredSessionCode)_exp_resp.StatusCode == ExpiredSessionCode.Ok)
      {
        Log.Message(String.Format("WCP_FBM.FS2S_ExpiredAccounts: FBMSessionId: {0}, AccountsUpdated", FBMSessionId));
      }
      else
      {
        switch ((ExpiredSessionCode)_exp_resp.StatusCode)
        {
          case ExpiredSessionCode.FBMSessionIdError:
            _error = String.Format("WCP_FBM.FS2S_ExpiredAccounts: FBMSessionId: {0}, Error: Verify FBMSsessionId. Description: {1}", FBMSessionId, _exp_resp.StatusText);
            break;

          case ExpiredSessionCode.FBMEstablishmentError:
            _error = String.Format("WCP_FBM.FS2S_ExpiredAccounts: FBMSessionId: {0},  Error Verify FBMEstablishmentCode. Description: {1}", FBMSessionId, _exp_resp.StatusText);
            break;

          case ExpiredSessionCode.WigosSessionIdError:
            _error = String.Format("WCP_FBM.FS2S_ExpiredAccounts: FBMSessionId: {0}, Error: Invalid Wigos Session id. Description: {1}", FBMSessionId, _exp_resp.StatusText);
            break;
        }
        Log.Warning(_error);
        return false;
      }

      return true;
    } // ExpireAccountsFBM

    /// <summary>
    /// block the FBM machines and close session
    /// </summary>
    /// <param name="CurrentStatus"></param>
    /// <param name="CurrentBlock"></param>
    /// <returns></returns>
    public static Boolean CloseSessionFBM(SessionStatusFBM CurrentStatus, BlockStatusFBM CurrentBlock, String User, out Boolean HandledError)
    {
      CloseSessionResp _close_resp;
      BlockTerminalsResp _block_resp;
      String _error;
      FBM_CloseSessionRequest _close_session_request;
      FBM_BlockUnblockRequest _block_machine_request;
      String _xml_request;
      String _xml_response;

      _error = String.Empty;

      HandledError = true;

      try
      {
        if (CurrentBlock == BlockStatusFBM.Unblocked)
        {
          _block_machine_request = new FBM_BlockUnblockRequest
          {
            FBMEstablishmentCode = EstablishmentCode,
            FBMServerId = FBMSessionId
          };

          _block_resp = WsFBM.FS2S_BlockTerminals(_block_machine_request.FBMEstablishmentCode, _block_machine_request.FBMServerId);

          Misc.ClassToXml<FBM_BlockUnblockRequest>(_block_machine_request, out _xml_request);
          Misc.ClassToXml<BlockTerminalsResp>(_block_resp, out _xml_response);

          if ((BlockUnblockStatusCode)_block_resp.StatusCode == BlockUnblockStatusCode.Ok)
          {
            Log.Message(String.Format("WCP_FBM.FS2S_BlockTerminals: FBMSessionId: {0}, Block", FBMSessionId));
            UpdateFBMLog(LogStatus.Blocked, LogType.BlockMachine, _xml_request, _xml_response, User);
          }
          else
          {
            switch ((BlockUnblockStatusCode)_block_resp.StatusCode)
            {
              case BlockUnblockStatusCode.Error:
                _error = String.Format("WCP_FBM.FS2S_BlockTerminals: FBMSessionId: {0}, Error: Some terminals not blocked. Description: {1}", FBMSessionId, _block_resp.StatusText);
                break;

              case BlockUnblockStatusCode.InvalidFBMSessionId:
                _error = String.Format("WCP_FBM.FS2S_BlockTerminals: FBMSessionId: {0}, Error: Invalid FBMSessionId. Description: {1}", FBMSessionId, _block_resp.StatusText);
                break;

              case BlockUnblockStatusCode.AccessDenied:
                _error = String.Format("WCP_FBM.FS2S_BlockTerminals: FBMSessionId: {0}, Error: Access denied. Description: {1}", FBMSessionId, _block_resp.StatusText);
                break;

              case BlockUnblockStatusCode.GeneralError:
                _error = String.Format("WCP_FBM.FS2S_BlockTerminals: FBMSessionId: {0}, Error: Block error. Description: {1}", FBMSessionId, _block_resp.StatusText);
                break;
            }

            Log.Warning(_error);
            UpdateFBMLog(LogStatus.Unblocked, LogType.BlockMachine, _xml_request, _xml_response, User);

            return false;
          }
        }

        if (CurrentStatus == SessionStatusFBM.Open)
        {
          // Expired accounts
          if (!ExpireAccountsFBM(CurrentStatus, CurrentBlock, User))
          {
            HandledError = false;

            return false;
          }

          // Close session
          _close_session_request = new FBM_CloseSessionRequest();
          _close_session_request.GetCloseSessionData(EstablishmentCode, FBMSessionId); // FBMServerId = FBMSessionId

          _close_resp = WsFBM.FS2S_CloseSession(_close_session_request.FBMEstablishmentCode                  // FBMEstablishmentCode
                                               , _close_session_request.FBMServerId                           // FBMServerId
                                               , _close_session_request.TotalValueAccounts                    // TotalValueAccounts
                                               , _close_session_request.TotalValueAccountsPromo               // TotalValueAccountsPromo
                                               , _close_session_request.TotalSessionExpiredAccounts           // TotalSessionExpiredAccounts
                                               , _close_session_request.TotalSessionExpiredAccountsPromo      // TotalSessionExpiredAccountsPromo
                                               , _close_session_request.TotalSessionSalesPOS                  // TotalSessionSalesPOS
                                               , _close_session_request.TotalSessionPromoInPOS                // TotalSessionPromoInPOS
                                               , _close_session_request.TotalSessionPaymentsPOS               // TotalSessionPaymentsPOS
                                               , _close_session_request.TotalSessionPromoOutPOS               // TotalSessionPromoOutPOS
                                               , _close_session_request.TotalSessionJackpotPaymentPOS         // TotalSessionJackpotPaymentPOS
                                               , _close_session_request.TotalSessionEletronicTransferIn       // TotalSessionEletronicTransferIn
                                               , _close_session_request.TotalSessionEletronicTransferInPromo  // TotalSessionEletronicTransferInPromo
                                               , _close_session_request.TotalSessionEletronicTransferOut      // TotalSessionEletronicTransferOut
                                               , _close_session_request.TotalSessionEletronicTransferOutPromo // TotalSessionEletronicTransferOutPromo
                                               , _close_session_request.TotalSessionHandPay                   // TotalSessionHandPay
                                               , _close_session_request.TotalSessionCoinIn                    // TotalSessionCoinIn 
                                               , _close_session_request.TotalSessionCoinOut                   // TotalSessionCoinOut 
                                               , _close_session_request.TotalSessionJackpot                   // TotalSessionJackpot 
                                               , _close_session_request.TotalSessionNet);                     // TotalSessionNet 

          Misc.ClassToXml<FBM_CloseSessionRequest>(_close_session_request, out _xml_request);
          Misc.ClassToXml<CloseSessionResp>(_close_resp, out _xml_response);

          if ((CloseSessionStatusCode)_close_resp.StatusCode == CloseSessionStatusCode.Ok)
          {
            Log.Message(String.Format("WCP_FBM.FS2S_CloseSession: FBMSessionId: {0}, Closing started successfully", FBMSessionId));
            UpdateFBMLog(LogStatus.Close, LogType.CloseSession, _xml_request, _xml_response, User);
          }
          else if ((CloseSessionStatusCode)_close_resp.StatusCode == CloseSessionStatusCode.ClosingAlreadyBeingDone)
          {
            Log.Message(String.Format("WCP_FBM.FS2S_CloseSession: FBMSessionId: {0}, Warning: Closing is already being done", FBMSessionId));
            UpdateFBMLog(LogStatus.Close, LogType.CloseSession, _xml_request, _xml_response, User);
          }
          else
          {
            switch ((CloseSessionStatusCode)_close_resp.StatusCode)
            {
              case CloseSessionStatusCode.ExistUnblockTerminals:
                _error = String.Format("WCP_FBM.FS2S_CloseSession: FBMSessionId: {0}, Error: Exist unblocked terminals. Description: {1}", FBMSessionId, _close_resp.StatusText);
                break;

              case CloseSessionStatusCode.NotExistsSessionOpen:
                _error = String.Format("WCP_FBM.FS2S_CloseSession: FBMSessionId: {0}, Error: Not exists session open. Description: {1}", FBMSessionId, _close_resp.StatusText);
                break;

              case CloseSessionStatusCode.InvalidFBMSessionId:
                _error = String.Format("WCP_FBM.FS2S_CloseSession: FBMSessionId: {0}, Error: Invalid FBMSessionId. Description: {1}", FBMSessionId, _close_resp.StatusText);
                break;

              case CloseSessionStatusCode.AccessDenied:
                _error = String.Format("WCP_FBM.FS2S_CloseSession: FBMSessionId: {0}, Error: Access denied. Description: {1}", FBMSessionId, _close_resp.StatusText);
                break;

              case CloseSessionStatusCode.CloseError:
                _error = String.Format("WCP_FBM.FS2S_CloseSession: FBMSessionId: {0}, Error: Close error. Description: {1}", FBMSessionId, _close_resp.StatusText);
                break;
            }

            UpdateFBMLog(LogStatus.Open, LogType.CloseSession, _xml_request, _xml_response, User);
            Log.Warning(_error);

          }
        }

        HandledError = false;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Warning("WCP_FBM.CloseSessionFBM: Exception");
        Log.Exception(_ex);
      }

      HandledError = false;

      return false;
    } // CloseSessionFBM

    /// <summary>
    /// Open session and unblock the FBM machines
    /// </summary>
    /// <param name="CurrentStatus"></param>
    /// <param name="CurrentBlock"></param>
    /// <returns></returns>
    public static Boolean OpenSessionFBM(SessionStatusFBM CurrentStatus, BlockStatusFBM CurrentBlock, String User)
    {
      OpenSessionResp _open_resp;
      UnblockTerminalsResp _unblock_resp;
      FBM_OpenSessionRequest _open_session_request;
      FBM_BlockUnblockRequest _unblock_machine_request;
      String _error;
      String _xml_request;
      String _xml_response;

      _error = String.Empty;

      try
      {
        if (CurrentStatus == SessionStatusFBM.Close)
        {
          _open_session_request = new FBM_OpenSessionRequest();
          _open_session_request.GetOpenSessionData(EstablishmentCode, WGDB.Now);

          _open_resp = WsFBM.FS2S_OpenSession(_open_session_request.FBMEstablishmentCode
                                            , _open_session_request.DateTime.ToString("yyyyMMddHHmmss")
                                            , _open_session_request.TotalValueAccounts
                                            , _open_session_request.TotalValueAccountsPromo);

          Misc.ClassToXml<FBM_OpenSessionRequest>(_open_session_request, out _xml_request);
          Misc.ClassToXml<OpenSessionResp>(_open_resp, out _xml_response);

          if ((OpenSessionStatusCode)_open_resp.StatusCode == OpenSessionStatusCode.Ok)
          {
            FBMSessionId = _open_resp.FBMSessionId;
            Log.Message(String.Format("WCP_FBM.FS2S_OpenSession: FBMSessionId: {0}, Open", FBMSessionId));
            UpdateFBMLog(LogStatus.Open, LogType.OpenSession, _xml_request, _xml_response, User);
          }
          else
          {
            switch ((OpenSessionStatusCode)_open_resp.StatusCode)
            {
              case OpenSessionStatusCode.ExistsSessionOpen:
                _error = String.Format("WCP_FBM.FS2S_OpenSession: FBMSessionId: {0}, Error: Exists session open. Description: {1}", FBMSessionId, _open_resp.StatusText);
                break;

              case OpenSessionStatusCode.ErrorOpenSession:
                _error = String.Format("WCP_FBM.FS2S_OpenSession: FBMSessionId: {0}, Error: Cant open a session. Description: {1}", FBMSessionId, _open_resp.StatusText);
                break;

              case OpenSessionStatusCode.AccessDenied:
                _error = String.Format("WCP_FBM.FS2S_OpenSession: FBMSessionId: {0}, Error: Access denied. Description: {1}", FBMSessionId, _open_resp.StatusText);
                break;
            }

            UpdateFBMLog(LogStatus.Close, LogType.OpenSession, _xml_request, _xml_response, User);
            Log.Warning(_error);

            return false;
          }
        }

        if (CurrentBlock == BlockStatusFBM.Blocked)
        {
          _unblock_machine_request = new FBM_BlockUnblockRequest
          {
            FBMEstablishmentCode = EstablishmentCode,
            FBMServerId = FBMSessionId
          };

          _unblock_resp = WsFBM.FS2S_UnblockTerminals(_unblock_machine_request.FBMEstablishmentCode, _unblock_machine_request.FBMServerId);

          Misc.ClassToXml<FBM_BlockUnblockRequest>(_unblock_machine_request, out _xml_request);
          Misc.ClassToXml<UnblockTerminalsResp>(_unblock_resp, out _xml_response);

          if ((BlockUnblockStatusCode)_unblock_resp.StatusCode == BlockUnblockStatusCode.Ok)
          {
            Log.Message(String.Format("WCP_FBM.FS2S_UnblockTerminals: FBMSessionId: {0}, Unblock", FBMSessionId));
            UpdateFBMLog(LogStatus.Unblocked, LogType.UnblockMachine, _xml_request, _xml_response, User);
          }
          else
          {
            switch ((BlockUnblockStatusCode)_unblock_resp.StatusCode)
            {
              case BlockUnblockStatusCode.Error:
                _error = String.Format("WCP_FBM.FS2S_UnblockTerminals: FBMSessionId: {0}, Error: Some terminals not unblocked. Description: {1}", FBMSessionId, _unblock_resp.StatusText);
                break;

              case BlockUnblockStatusCode.InvalidFBMSessionId:
                _error = String.Format("WCP_FBM.FS2S_UnblockTerminals: FBMSessionId: {0}, Error: Invalid FBMSessionId. Description: {1}", FBMSessionId, _unblock_resp.StatusText);
                break;

              case BlockUnblockStatusCode.AccessDenied:
                _error = String.Format("WCP_FBM.FS2S_UnblockTerminals: FBMSessionId: {0}, Error: Access denied. Description: {1}", FBMSessionId, _unblock_resp.StatusText);
                break;

              case BlockUnblockStatusCode.GeneralError:
                _error = String.Format("WCP_FBM.FS2S_UnblockTerminals: FBMSessionId: {0}, Error: Unblock error. Description: {1}", FBMSessionId, _unblock_resp.StatusText);
                break;
            }

            Log.Warning(_error);
            UpdateFBMLog(LogStatus.Blocked, LogType.UnblockMachine, _xml_request, _xml_response, User);

            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Warning("WCP_FBM.OpenSessionFBM: Exception");
        Log.Exception(_ex);
      }

      return false;
    } // OpenSessionFBM


    public static Boolean CloseSessionConfirmationFBM(String User, out CloseSessionConfirmationResp CloseSessionConfirmationResponse)
    {

      String _response;
      String _xml_request;
      String _xml_response;
      FBM_CloseSessionConfirmationRequest _close_cession_confirmation_request;

      CloseSessionConfirmationResponse = new CloseSessionConfirmationResp();

      try
      {
        _close_cession_confirmation_request = new FBM_CloseSessionConfirmationRequest
        {
          FBMEstablishmentCode = EstablishmentCode,
          FBMServerId = FBMSessionId
        };

        CloseSessionConfirmationResponse = WsFBM.FS2S_CloseSessionConfirmation(_close_cession_confirmation_request.FBMEstablishmentCode,
                                                                               _close_cession_confirmation_request.FBMServerId);

        Misc.ClassToXml<FBM_CloseSessionConfirmationRequest>(_close_cession_confirmation_request, out _xml_request);
        Misc.ClassToXml<CloseSessionConfirmationResp>(CloseSessionConfirmationResponse, out _xml_response);

        _response = String.Format("WCP_FBM.FS2S_CloseSessionConfirmation | Request > FBMEstablishmentCode: {0}, FBMSessionId: {1} |",
                                  _close_cession_confirmation_request.FBMEstablishmentCode, _close_cession_confirmation_request.FBMServerId);

        if ((CloseSessionConfirmationStatusCode)CloseSessionConfirmationResponse.StatusCode == CloseSessionConfirmationStatusCode.Ok)
        {
          _response = String.Format("{0} Response > StatudCode: {1} , StatusText: {2} ", _response, CloseSessionConfirmationResponse.StatusCode, CloseSessionConfirmationResponse.StatusText);

          Log.Message(_response);

          UpdateFBMLog(LogStatus.Close, LogType.CloseSessionConfirmation, _xml_request, _xml_response, User);
        }
        else
        {
          switch ((CloseSessionConfirmationStatusCode)CloseSessionConfirmationResponse.StatusCode)
          {
            case CloseSessionConfirmationStatusCode.NoCloseSessionInProgress:
              _response = String.Format("{0} Error: Response > StatudCode: {1} , StatusText: {2} ", _response, CloseSessionConfirmationResponse.StatusCode, CloseSessionConfirmationResponse.StatusText);
              break;
            case CloseSessionConfirmationStatusCode.Retry:
              _response = String.Format("{0} Error: Response > StatudCode: {1} , StatusText: {2} ", _response, CloseSessionConfirmationResponse.StatusCode, CloseSessionConfirmationResponse.StatusText);
              break;
            case CloseSessionConfirmationStatusCode.InvalidFBMSessionId:
              _response = String.Format("{0} Error: Response > StatudCode: {1} , StatusText: {2} ", _response, CloseSessionConfirmationResponse.StatusCode, CloseSessionConfirmationResponse.StatusText);
              break;
            case CloseSessionConfirmationStatusCode.InvalidFBMEstablishmentCode:
              _response = String.Format("{0} Error: Response > StatudCode: {1} , StatusText: {2} ", _response, CloseSessionConfirmationResponse.StatusCode, CloseSessionConfirmationResponse.StatusText);
              break;
            case CloseSessionConfirmationStatusCode.Error:
              _response = String.Format("{0} Error: Response > StatudCode: {1} , StatusText: {2} ", _response, CloseSessionConfirmationResponse.StatusCode, CloseSessionConfirmationResponse.StatusText);
              break;
          }

          Log.Warning(_response);

          UpdateFBMLog(LogStatus.Close, LogType.CloseSessionConfirmation, _xml_request, _xml_response, User);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Warning("WCP_FBM.CloseSessionConfirmationFBM: Exception");
        Log.Exception(_ex);
      }

      return false;

    } // CloseSessionConfirmationFBM

    /// <summary>
    /// Get date of working day for FBM machines
    /// </summary>
    /// <param name="DateNow"></param>
    /// <param name="Time"></param>
    /// <param name="GPDate"></param>
    /// <returns></returns>
    private static Boolean GetDate(DateTime DateNow, String Time, out DateTime GPDate)
    {
      Int32 _hour;
      Int32 _minute;

      GPDate = DateNow;

      try
      {
        if (!Int32.TryParse(Time.Substring(0, 2), out _hour))
        {
          return false;
        }
        if (!Int32.TryParse(Time.Substring(3, 2), out _minute))
        {
          return false;
        }

        GPDate = new DateTime(DateNow.Year, DateNow.Month, DateNow.Day, _hour, _minute, 0);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // GetDate

    /// <summary>
    /// UpdateFBMLog for recalculate
    /// </summary>
    /// <param name="Status"></param>
    /// <param name="Type"></param>
    /// <param name="User"></param>
    /// <returns></returns>
    private static Boolean UpdateFBMLog(LogStatus Status, LogType Type, String User)
    {
      String _xml;

      _xml = String.Format("<Recalculated>{0}</Recalculated>", Status);

      return UpdateFBMLog(Status, Type, _xml, _xml, User);
    } // UpdateFBMLog

    /// <summary>
    /// UpdateFBMLog for opening or close.
    /// </summary>
    /// <param name="Status"></param>
    /// <param name="Type"></param>
    /// <param name="Request"></param>
    /// <param name="Response"></param>
    /// <param name="User"></param>
    /// <returns></returns>
    private static Boolean UpdateFBMLog(LogStatus Status, LogType Type, String Request, String Response, String User)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (UpdateFBMLog(Status, Type, Request, Response, User, _db_trx.SqlTransaction))
        {
          _db_trx.Commit();

          return true;
        }
      }

      return false;
    } // UpdateFBMLog

    /// <summary>
    /// Update fbm log transaction
    /// </summary>
    /// <param name="Status"></param>
    /// <param name="Type"></param>
    /// <param name="Request"></param>
    /// <param name="Response"></param>
    /// <param name="User"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private static Boolean UpdateFBMLog(LogStatus Status, LogType Type, String Request, String Response, String User, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("INSERT INTO   FBM_LOG            ");
        _sb.AppendLine("            ( FL_STATUS          ");
        _sb.AppendLine("            , FL_TYPE            ");
        _sb.AppendLine("            , FL_REQUEST         ");
        _sb.AppendLine("            , FL_RESPONSE        ");
        _sb.AppendLine("            , FL_CREATION        ");
        _sb.AppendLine("            , FL_CREATION_USER ) ");
        _sb.AppendLine("     VALUES                      ");
        _sb.AppendLine("           (   @pStatus          ");
        _sb.AppendLine("           ,   @pType            ");
        _sb.AppendLine("           ,   @pRequest         ");
        _sb.AppendLine("           ,   @pResponse        ");
        _sb.AppendLine("           ,   @pCreation        ");
        _sb.AppendLine("           ,   @pUser   )        ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = Status;
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = Type;

          if (Request == null)
          {
            _cmd.Parameters.Add("@pRequest", SqlDbType.Xml).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pRequest", SqlDbType.Xml).Value = Request;
          }

          if (Response == null)
          {
            _cmd.Parameters.Add("@pResponse", SqlDbType.Xml).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pResponse", SqlDbType.Xml).Value = Response;
          }

          _cmd.Parameters.Add("@pCreation", SqlDbType.DateTime).Value = WGDB.Now;
          _cmd.Parameters.Add("@pUser", SqlDbType.NVarChar).Value = User;

          _cmd.ExecuteNonQuery();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Error("UpdateFBMLog: Error inserting fbm log.");
      }

      return false;
    } // UpdateFBMLog


    #endregion " Private Methotds "

  }
}
