﻿// 
//   MODULE NAME: FBM_Request.cs
// 
//   DESCRIPTION: Dummy Classes for  request
// 
//        AUTHOR: Enric Tomas
// 
// CREATION DATE: 07-SEP-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-SEP-2017 ETP    First release.
// 05-OCT-2017 DPC    [WIGOS-5481]: FBM - WS - CloseSessionConfirmation
// ----------- ------ ----------------------------------------------------------
using System;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common.FBM
{
  #region " FBM_OpenSessionRequest "

  public class FBM_OpenSessionRequest
  {
    #region " properties "

    public String FBMEstablishmentCode { get; set; }
    public DateTime DateTime { get; set; }
    public Decimal TotalValueAccounts { get; set; }
    public Decimal TotalValueAccountsPromo { get; set; }

    #endregion " properties "

    #region " public methods "

    /// <summary>
    /// Get Open Session Data by SP
    /// </summary>
    /// <param name="EstablishmentCode"></param>
    /// <param name="ServerId"></param>
    /// <returns></returns>
    public Boolean GetOpenSessionData(String EstablishmentCode, DateTime DateTime)
    {
      this.FBMEstablishmentCode = EstablishmentCode;
      this.DateTime = DateTime;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetOpenSessionData(_db_trx);
      }
    } // GetCloseSessionData

    /// <summary>
    /// Get Open Session Data by SP
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean GetOpenSessionData(DB_TRX SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand("SP_FS2S_OpenSession", SqlTrx.SqlTransaction.Connection, SqlTrx.SqlTransaction))
        {
          _cmd.CommandType = CommandType.StoredProcedure;

          using (SqlDataReader _reader = SqlTrx.ExecuteReader(_cmd))
          {
            if (_reader.Read())
            {
              this.TotalValueAccounts = (_reader.IsDBNull(_reader.GetOrdinal("TotalValueAccounts"))) ? 0 : (Decimal)_reader["TotalValueAccounts"];
              this.TotalValueAccountsPromo = (_reader.IsDBNull(_reader.GetOrdinal("TotalValueAccountsPromo"))) ? 0 : (Decimal)_reader["TotalValueAccountsPromo"];

              return true;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Error("FBM.GetOpenSessionData");
      }

      return false;
    } // GetOpenSessionData

    #endregion " public methods "
  } // FBM_OpenSessionRequest

  #endregion " FBM_OpenSessionRequest "

  #region " FBM_BlockUnblockRequest "

  public class FBM_BlockUnblockRequest
  {
    #region " properties "

    public String FBMEstablishmentCode { get; set; }
    public Int64 FBMServerId { get; set; }
    
    #endregion " properties "
    
  } // FBM_BlockUnblockRequest

  #endregion " FBM_BlockUnblockRequest "


  #region " FBM_CloseSessionConfirmationRequest "

  public class FBM_CloseSessionConfirmationRequest
  {
    #region " properties "

    public String FBMEstablishmentCode { get; set; }
    public Int64 FBMServerId { get; set; }

    #endregion " properties "

  } // FBM_CloseSessionConfirmationRequest

  #endregion " FBM_CloseSessionConfirmationRequest "

}
