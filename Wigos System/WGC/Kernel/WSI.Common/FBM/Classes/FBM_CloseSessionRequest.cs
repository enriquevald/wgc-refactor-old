﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: FBM_CloseSessionRequest.cs
// 
//   DESCRIPTION: Close Session request
// 
//        AUTHOR: Enric Tomas
// 
// CREATION DATE: 07-SEP-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-SEP-2017 ETP    First release.
// ----------- ------ ----------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common.FBM
{
  public class FBM_CloseSessionRequest
  {

    #region " properties "

    public String FBMEstablishmentCode { get; set; }
    public Int64 FBMServerId { get; set; }
    public Decimal TotalValueAccounts { get; set; }
    public Decimal TotalValueAccountsPromo { get; set; }
    public Decimal TotalSessionExpiredAccounts { get; set; }
    public Decimal TotalSessionExpiredAccountsPromo { get; set; }
    public Decimal TotalSessionSalesPOS { get; set; }
    public Decimal TotalSessionPromoInPOS { get; set; }
    public Decimal TotalSessionPaymentsPOS { get; set; }
    public Decimal TotalSessionPromoOutPOS { get; set; }
    public Decimal TotalSessionJackpotPaymentPOS { get; set; }
    public Decimal TotalSessionEletronicTransferIn { get; set; }
    public Decimal TotalSessionEletronicTransferInPromo { get; set; }
    public Decimal TotalSessionEletronicTransferOut { get; set; }
    public Decimal TotalSessionEletronicTransferOutPromo { get; set; }
    public Decimal TotalSessionHandPay { get; set; }
    public Decimal TotalSessionCoinIn { get; set; }
    public Decimal TotalSessionCoinOut { get; set; }
    public Decimal TotalSessionJackpot { get; set; }
    public Decimal TotalSessionNet { get; set; }

    #endregion " properties "

    #region " public methods "

    /// <summary>
    /// Get Close Session Data by SP
    /// </summary>
    /// <param name="EstablishmentCode"></param>
    /// <param name="ServerId"></param>
    /// <returns></returns>
    public Boolean GetCloseSessionData(String EstablishmentCode, Int64 ServerId)
    {
      this.FBMEstablishmentCode = EstablishmentCode;
      this.FBMServerId = ServerId;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetCloseSessionData(_db_trx);
      }
    } // GetCloseSessionData

    /// <summary>
    /// Get Close Session Data by SP
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean GetCloseSessionData(DB_TRX SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand("SP_FS2S_CloseSession", SqlTrx.SqlTransaction.Connection, SqlTrx.SqlTransaction))
        {
          _cmd.CommandType = CommandType.StoredProcedure;

          using (SqlDataReader _reader = SqlTrx.ExecuteReader(_cmd))
          {
            if (_reader.Read())
            {
              this.TotalValueAccounts = (_reader.IsDBNull(_reader.GetOrdinal("TotalValueAccounts"))) ? 0 : (Decimal)_reader["TotalValueAccounts"];
              this.TotalValueAccountsPromo = (_reader.IsDBNull(_reader.GetOrdinal("TotalValueAccountsPromo"))) ? 0 : (Decimal)_reader["TotalValueAccountsPromo"];
              this.TotalSessionExpiredAccounts = (_reader.IsDBNull(_reader.GetOrdinal("TotalSessionExpiredAccounts"))) ? 0 : (Decimal)_reader["TotalSessionExpiredAccounts"];
              this.TotalSessionExpiredAccountsPromo = (_reader.IsDBNull(_reader.GetOrdinal("TotalSessionExpiredAccountsPromo"))) ? 0 : (Decimal)_reader["TotalSessionExpiredAccountsPromo"];
              this.TotalSessionSalesPOS = (_reader.IsDBNull(_reader.GetOrdinal("TotalSessionSalesPOS"))) ? 0 : (Decimal)_reader["TotalSessionSalesPOS"];
              this.TotalSessionPromoInPOS = (_reader.IsDBNull(_reader.GetOrdinal("TotalSessionPromoInPOS"))) ? 0 : (Decimal)_reader["TotalSessionPromoInPOS"];
              this.TotalSessionPaymentsPOS = (_reader.IsDBNull(_reader.GetOrdinal("TotalSessionPaymentsPOS"))) ? 0 : (Decimal)_reader["TotalSessionPaymentsPOS"];
              this.TotalSessionPromoOutPOS = (_reader.IsDBNull(_reader.GetOrdinal("TotalSessionPromoOutPOS"))) ? 0 : (Decimal)_reader["TotalSessionPromoOutPOS"];
              this.TotalSessionJackpotPaymentPOS = (_reader.IsDBNull(_reader.GetOrdinal("TotalSessionJackpotPaymentPOS"))) ? 0 : (Decimal)_reader["TotalSessionJackpotPaymentPOS"];
              this.TotalSessionEletronicTransferIn = (_reader.IsDBNull(_reader.GetOrdinal("TotalSessionEletronicTransferIn"))) ? 0 : (Decimal)_reader["TotalSessionEletronicTransferIn"];
              this.TotalSessionEletronicTransferInPromo = (_reader.IsDBNull(_reader.GetOrdinal("TotalSessionEletronicTransferInPromo"))) ? 0 : (Decimal)_reader["TotalSessionEletronicTransferInPromo"];
              this.TotalSessionEletronicTransferOut = (_reader.IsDBNull(_reader.GetOrdinal("TotalSessionEletronicTransferOut"))) ? 0 : (Decimal)_reader["TotalSessionEletronicTransferOut"];
              this.TotalSessionEletronicTransferOutPromo = (_reader.IsDBNull(_reader.GetOrdinal("TotalSessionEletronicTransferOutPromo"))) ? 0 : (Decimal)_reader["TotalSessionEletronicTransferOutPromo"];
              this.TotalSessionHandPay = (_reader.IsDBNull(_reader.GetOrdinal("TotalSessionHandPay"))) ? 0 : (Decimal)_reader["TotalSessionHandPay"];
              this.TotalSessionCoinIn = (_reader.IsDBNull(_reader.GetOrdinal("TotalSessionCoinIn"))) ? 0 : (Decimal)_reader["TotalSessionCoinIn"];
              this.TotalSessionCoinOut = (_reader.IsDBNull(_reader.GetOrdinal("TotalSessionCoinOut"))) ? 0 : (Decimal)_reader["TotalSessionCoinOut"];
              this.TotalSessionJackpot = (_reader.IsDBNull(_reader.GetOrdinal("TotalSessionJackpot"))) ? 0 : (Decimal)_reader["TotalSessionJackpot"];
              this.TotalSessionNet = (_reader.IsDBNull(_reader.GetOrdinal("TotalSessionNet"))) ? 0 : (Decimal)_reader["TotalSessionNet"];

              return true;             
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Error("FBM.GetCloseSessionData");
      }

      return false;
    } // GetCloseSessionData

    #endregion " public methods "
  
  }
}
