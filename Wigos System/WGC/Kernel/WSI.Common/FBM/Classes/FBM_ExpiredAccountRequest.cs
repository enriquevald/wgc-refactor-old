﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: FBM_ExpiredAccountRequest.cs
// 
//   DESCRIPTION: Expired Account Request
// 
//        AUTHOR: Enric Tomas
// 
// CREATION DATE: 02-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-OCT-2017 ETP    First release.
// ----------- ------ ----------------------------------------------------------

using FBM;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Xml;

namespace WSI.Common.FBM.Classes
{
  public class FBM_ExpiredAccountRequest
  {
    #region " properties "

    public String FBMEstablishmentCode { get; set; }
    public Int64 FBMServerId { get; set; }
    public ExpiredAccounts[] Expireds { get; set; }

    #endregion " properties "

    #region " public methods "

    /// <summary>
    /// Get Expired Account
    /// </summary>
    /// <param name="EstablishmentCode"></param>
    /// <param name="ServerId"></param>
    /// <returns></returns>
    public Boolean GetExpiredAccounts(String EstablishmentCode, Int64 ServerId)
    {
      this.FBMEstablishmentCode = EstablishmentCode;
      this.FBMServerId = ServerId;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        return GetExpiredAccounts(_db_trx);
      }
    } // GetExpiredAccounts

    /// <summary>
    /// Get Expired Account by SP
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean GetExpiredAccounts(DB_TRX SqlTrx)
    {
      DataTable _expired_accounts;

      try
      {
        using (SqlCommand _cmd = new SqlCommand("SP_FS2S_ExpiredAccount", SqlTrx.SqlTransaction.Connection, SqlTrx.SqlTransaction))
        {
          _cmd.CommandType = CommandType.StoredProcedure;


          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _expired_accounts = new DataTable();
            SqlTrx.Fill(_sql_da, _expired_accounts);

            Expireds = DataTableToExpiredAccounts(_expired_accounts);

            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        Log.Error("FBM.GetCloseSessionData");
      }

      return false;
    } // GetExpiredAccounts

    #endregion " public methods "

    #region " private methods "

    /// <summary>
    /// DataTable to ExpiredAccounts[]
    /// </summary>
    /// <param name="DtExpiredAccounts"></param>
    /// <returns></returns>
    private ExpiredAccounts[] DataTableToExpiredAccounts(DataTable DtExpiredAccounts)
    {
      List<ExpiredAccounts> _expired_accounts;

      _expired_accounts = new List<ExpiredAccounts>();

      foreach (DataRow _account in DtExpiredAccounts.Rows)
      {
        _expired_accounts.Add(new ExpiredAccounts
          {
            trackData = (String)(_account.IsNull("Trackdata") ? "" : _account["Trackdata"]),
            value = (Decimal)_account["Value"]
          });
      }

      return _expired_accounts.ToArray();

    } // DataTableToExpiredAccounts

    #endregion " private methods "

  }
}
