using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO.Ports;
using System.Threading;

namespace WSI.Common
{
  public delegate void KbdMsgFilterHandler(KbdMsgEvent Event, Object Data);


  public enum KbdMsgEvent
  {
    EventNone = 0,
    EventBarcode = 1,
    EventBankCard = 2,
    EventTrackData = 3,
    EventFlyer = 4,

    EventReading0 = 100,
    EventReading1 = 101,
    EventReadEnd = 102,
    EventNotValid = 103,
  }

  public class KeyboardMessageFilter : IMessageFilter
  {
    private static KeyboardMessageFilter m_base = null;
    private static BarcodeReader m_reader = null;
    
    public const int FLYER_LENGTH = 6;

    public static void Init()
    {
      if (m_base == null)
      {
        int _extra;

        _extra = 1;

        try
        {
          int.TryParse(Environment.GetEnvironmentVariable("LKS_KEYBOARD_FILTER_KEYUP"), out _extra);
          _extra = Math.Max(1, _extra);
        }
        catch
        { ; }

        m_num_extra_keyups = Math.Max(1, _extra);

        m_base = new KeyboardMessageFilter();
        Application.AddMessageFilter(m_base);

        try
        {
          int.TryParse(Environment.GetEnvironmentVariable("LKS_WITEK_BARCODE_READER"), out _extra);
          if (_extra > 0)
          {
            m_reader = new BarcodeReader();
            m_reader.Init(m_base.NotifyBarcodeRead);

      }
          _extra = Math.Max(1, _extra);
    }
        catch
        { ; }


      }
    }

    public static void RegisterHandler(Control Ctrl, BarcodeType FilterType, KbdMsgFilterHandler Handler)
    {
      if (m_base == null)
      {
        return;
      }
      m_base.PushHandler(new KbdMsgFilterListener(Ctrl, FilterType, Handler, true));
    }
    public static void UnregisterHandler(Control Ctrl, KbdMsgFilterHandler Handler)
    {
      if (m_base == null)
      {
        return;
      }
      m_base.PopHandler(Ctrl);
    }

    private class KbdMsgFilterListener
    {
      internal Control m_ctrl;
      internal KbdMsgFilterHandler m_handler;
      internal Boolean m_listening;
      internal BarcodeType m_filter;

      internal KbdMsgFilterListener(Control Ctrl, BarcodeType FilterType, KbdMsgFilterHandler Handler, Boolean Listening)
      {
        m_ctrl = Ctrl;
        m_handler = Handler;
        m_listening = Listening;
        m_filter = FilterType;
      }

      internal void Notify(KbdMsgEvent Type, Object Barcode)
      {
        if (!m_listening)
        {
          return;
        }
        if (m_handler == null)
        {
          return;
        }
        if (m_ctrl == null)
        {
          m_handler(Type, Barcode);
        }
        else
        {
          Object[] _params;
          _params = new Object[2] { Type, Barcode };
          m_ctrl.BeginInvoke(m_handler, _params);
        }
      }
    }

    private class KbdMsgFilterListenerStack : List<KbdMsgFilterListener>
    { }


    #region IMessageFilter Members

    [DllImport("user32.dll")]
    private static extern IntPtr PostMessage(IntPtr hWnd, int msg, IntPtr wp, IntPtr lp);

    private IntPtr m_window = IntPtr.Zero;
    private Queue<Message> m_queue = new Queue<Message>();
    private int m_num_key_ups = 0;
    private static int m_num_extra_keyups = 1;

    private Queue<String> m_code = new Queue<String>();
    private Object m_sync = new object();


    private Queue<Message> m_repost_queue = new Queue<Message>();
    private System.Windows.Forms.Timer m_repost_timer = new System.Windows.Forms.Timer();
    private static int m_tick_start = 0;
    private static int m_tick_reading = 0;
    private static KbdMsgEvent m_last_reading = KbdMsgEvent.EventReading1;

    public const int WM_KB_WEDGE_REPOST_EVENT = 0x04F0;

    private KbdMsgFilterListenerStack m_listeners = new KbdMsgFilterListenerStack();

    private Boolean m_enabled = true;

    private KeyboardMessageFilter()
    {
      m_repost_timer = new System.Windows.Forms.Timer();
      m_repost_timer.Interval = 35;
      m_repost_timer.Tick += new EventHandler(RepostTimerTick);
    }


    private void PushHandler(KbdMsgFilterListener Listener)
    {
      lock (m_sync)
      {
        PopHandler(Listener.m_ctrl);
        m_listeners.Add(Listener);
      }
    }
    private void PopHandler(Control Ctrl)
    {
      lock (m_sync)
      {
        int _idx_w;
        int _idx_r;

        _idx_w = 0;
        for (_idx_r = 0; _idx_r < m_listeners.Count; _idx_r++)
        {
          if (m_listeners[_idx_r].m_ctrl == Ctrl)
          {
            // Found
            continue;
          }
          if (m_listeners[_idx_r].m_ctrl.IsDisposed)
          {
            // Remove from list
            continue;
          }
          m_listeners[_idx_w++] = m_listeners[_idx_r];
        }
        while (m_listeners.Count > _idx_w)
        {
          m_listeners.RemoveAt(m_listeners.Count - 1);
        }
      }
    }

    void RepostMessages()
    {
      Message _msg;

      lock (m_sync)
      {
        m_repost_queue.Clear();

        while (m_queue.Count > 0)
        {
          _msg = m_queue.Dequeue();
          m_repost_queue.Enqueue(_msg);
          PostMessage(_msg.HWnd, _msg.Msg, _msg.WParam, _msg.LParam);
        }
      }
    }


    void NotifyReading()
    {
      KbdMsgFilterListener _listener;

      lock (m_sync)
      {
        if (m_queue.Count <= 25) return;
        if (Misc.GetElapsedTicks(m_tick_reading) < 100) return;

        m_tick_reading = Environment.TickCount;
        if (m_last_reading == KbdMsgEvent.EventReading0)
        {
          m_last_reading = KbdMsgEvent.EventReading1;
        }
        else
        {
          m_last_reading = KbdMsgEvent.EventReading0;
        }

        _listener = GetListener();
        if (_listener == null || !_listener.m_listening)
        {
          return;
        }

        _listener.Notify(m_last_reading, null);
      }
    }

    KbdMsgFilterListener GetListener()
    {
      try
      {
        lock (m_sync)
        {
          for (int _idx = m_listeners.Count - 1; _idx >= 0; _idx--)
          {
            if (IsFocused(m_listeners[_idx].m_ctrl))
            {
              return m_listeners[_idx];
            }
          }
        }
      }
      catch
      {
        ;
      }

      return null;
    }

    private Boolean IsFocused(Control Ctrl)
    {
      try
      {
        Control _prev;

        if (Ctrl.IsDisposed)
          return false;

        if (!Ctrl.Visible)
          return false;

        if (Ctrl.Focused)
          return true;

        _prev = Ctrl;
        while (_prev.Parent != null)
        {
          _prev = _prev.Parent;

          if (_prev.IsDisposed)
            continue;

          if (!_prev.Visible)
            continue;

          if (_prev.Focused)
            return true;
        }

        if (Form.ActiveForm != null)
        {
          if (Form.ActiveForm == _prev)
          {
            return true;
          }
        }
        return false;
      }
      catch
      {
        ;
      }

      return false;
    }

    void NotifyBarcodeRead(String ReadBarcode)
    {
      KbdMsgFilterListener _listener;

      lock (m_sync)
      {
        _listener = GetListener();
        if (_listener == null || !_listener.m_listening)
        {
          return;
        }

        Barcode _barcode;
        if (Barcode.TryParse(ReadBarcode, out _barcode))
        {
          Boolean _notify;

          switch (_listener.m_filter)
          {
            case BarcodeType.FilterAnyBarcode:
            case BarcodeType.FilterAny:
              _notify = true;
              break;

            case BarcodeType.FilterAnyTito:
              _notify = false;
              _notify |= (_barcode.Type == BarcodeType.TitoStandardValidation);
              _notify |= (_barcode.Type == BarcodeType.TitoSecureEnhancedValidation);
              _notify |= (_barcode.Type == BarcodeType.TitoSystemValidation);
              break;

            default:
              _notify = (_listener.m_filter == _barcode.Type);
              break;
          }

          if (_notify)
          {
            _listener.Notify(KbdMsgEvent.EventBarcode, _barcode);
          }

          return;

        }
      }
    }



    void NotifyDataRead()
    {
      String _external_code;
      Message _msg;
      int _char;
      KbdMsgFilterListener _listener;

      Console.WriteLine(String.Format("Reading: {0} ms.", Common.Misc.GetElapsedTicks(m_tick_start)));

      lock (m_sync)
      {
        _listener = GetListener();
        if (_listener == null || !_listener.m_listening)
        {
          m_queue.Clear();

          return;
        }

        try
        {
          // Listening
          _external_code = "";
          while (m_queue.Count > 0)
          {
            _msg = m_queue.Dequeue();
            _char = _msg.WParam.ToInt32();
            _external_code = _external_code + (Char)(_char);
          }

          _external_code = _external_code.Replace("\r", "");
          _external_code = _external_code.ToUpper();

          // Deambiguate
          String[] _tracks;
          _tracks = MagneticStripe.SplitTracks(_external_code);
          if (_tracks.Length > 0)
          {
            if (_listener.m_filter == BarcodeType.FilterTrackData)
            {
              _listener.Notify(KbdMsgEvent.EventTrackData, _tracks[0]);

              return;
            }

            if (_listener.m_filter == BarcodeType.FilterBankCard)
            {
              MagneticStripeBankCard _ms_card;

              if (MagneticStripeBankCard.TryParse(_external_code, out _ms_card))
              {
                _listener.Notify(KbdMsgEvent.EventBankCard, _ms_card);
              }

              return;
            }

            if (_listener.m_filter == BarcodeType.Flyer)
            {
              if (_external_code.Length == FLYER_LENGTH)
              {
                _listener.Notify(KbdMsgEvent.EventFlyer, _external_code);
              }
              return;
            }

            _external_code = _tracks[0];
          }

          Barcode _barcode;
          if (Barcode.TryParse(_external_code, out _barcode))
          {
            Boolean _notify;

            switch (_listener.m_filter)
            {
              case BarcodeType.FilterAnyBarcode:
              case BarcodeType.FilterAny:
                _notify = true;
                break;

              case BarcodeType.FilterAnyTito:
                _notify = false;
                _notify |= (_barcode.Type == BarcodeType.TitoStandardValidation);
                _notify |= (_barcode.Type == BarcodeType.TitoSecureEnhancedValidation);
                _notify |= (_barcode.Type == BarcodeType.TitoSystemValidation);
                break;

              case BarcodeType.Account:
                _notify = false;                
                if (CardTrackData.AdmitAnyData() && CardTrackData.IsTrackData(_external_code))
                {
                  _barcode = new Barcode(new ExternalTrackData(_external_code));
                  _notify = true;
                }
                else
                {
                  _notify = (_listener.m_filter == _barcode.Type);
                }
                break;

              default:
                _notify = (_listener.m_filter == _barcode.Type);
                break;
            }

            if (_notify)
            {
              _listener.Notify(KbdMsgEvent.EventBarcode, _barcode);
            }

            return;
          }
          else
          {
            if (_listener.m_filter == BarcodeType.FilterAny)
            {
              _listener.Notify(KbdMsgEvent.EventNotValid, _external_code);
              return;
            }
          }
        }
        finally
        {
          _listener.Notify(KbdMsgEvent.EventReadEnd, null);
        }
      }
    }

    void NotifyOrRepost()
    {
      lock (m_sync)
      {
        m_repost_timer.Stop();

        if (m_queue.Count <= 5 || m_num_key_ups < m_queue.Count / 2)
        {
          PostMessage(IntPtr.Zero, WM_KB_WEDGE_REPOST_EVENT, IntPtr.Zero, IntPtr.Zero);

          return;
        }

        NotifyDataRead();
      }
    }

    void RepostTimerTick(object sender, EventArgs e)
    {
      NotifyOrRepost();
    }



    private bool FilterMessage(ref Message Message)
    {
      lock (m_sync)
      {
        if (!m_enabled)
        {
          return false;
        }

        switch (Message.Msg)
        {
          default:
            {
              return false;
            }

          case 0x0100: // WM_KEY_DOWN
            {
              if (Message.WParam.ToInt32() == 0x000D) // ENTER
              {
                if (m_queue.Count >= 5)
                {
                  NotifyOrRepost();

                  return true;
                }
              }

              return false;
            }

          case 0x0101: // WM_KEY_UP
            {
              m_num_key_ups++;

              return false;
            }

          case WM_KB_WEDGE_REPOST_EVENT:
            {
              RepostMessages();

              return true;
            }

          case 0x0102: // WM_CHAR
            {
              break;
            }

        } // switch (Message.Msg)

        m_repost_timer.Stop();

        if (m_repost_queue.Count > 0)
        {
          Message _prev;

          _prev = m_repost_queue.Peek();

          if (_prev.HWnd == Message.HWnd
              && _prev.Msg == Message.Msg
              && _prev.WParam == Message.WParam
              && _prev.LParam == Message.LParam)
          {
            _prev = m_repost_queue.Dequeue();

            return false;
          }
        }

        Message _msg;

        _msg = new Message();
        _msg.HWnd = Message.HWnd;
        _msg.Msg = Message.Msg;
        _msg.WParam = Message.WParam;
        _msg.LParam = Message.LParam;
        _msg.Result = Message.Result;

        if (m_queue.Count == 0)
        {
          m_num_key_ups = 0;
          m_tick_start = Environment.TickCount;
          m_tick_reading = Environment.TickCount;
        }
        m_queue.Enqueue(_msg);

        if (m_queue.Count > (m_num_key_ups + m_num_extra_keyups)) // Avoid Keyboard 'repeat'
        {
          RepostMessages();

          return true;
        }

        m_repost_timer.Start();

        NotifyReading();

        return true;
      } // lock
    }

    #endregion

    #region IMessageFilter Members

    bool IMessageFilter.PreFilterMessage(ref Message Message)
    {
      return this.FilterMessage(ref Message);
    }

    #endregion
  }




  public enum KbdSource
  {
    Unknown = 0,
    KbdType = 1,
    KbdPaste = 2,

    MagneticCardReader = 10,
    BarcodeReader = 20,
  }

  public class MagneticStripe
  {
    private static String[] TRACK_BEGIN = { "%", ";", "�" };
    private static String[] TRACK_END = { "?", "_" };
    private static String[] TRACK_FIELD = { "&", "^", "=", "�" };

    public static Boolean StartsWithTrackBeginMark(String Stripe)
    {
      if (String.IsNullOrEmpty(Stripe))
      {
        return false;
      }

      if (Stripe.Length < 1)
      {
        return false;
      }

      foreach (String _str in TRACK_BEGIN)
      {
        if (Stripe.Substring(0, 1) == _str.ToUpper())
        {
          return true;
        }
      }

      return false;
    }

    public static Boolean EndsWithTrackEndMark(String Stripe)
    {
      if (String.IsNullOrEmpty(Stripe))
      {
        return false;
      }

      if (Stripe.Length < 1)
      {
        return false;
      }

      foreach (String _str in TRACK_END)
      {
        if (Stripe.Substring(Stripe.Length - 1, 1) == _str)
        {
          return true;
        }
      }

      return false;
    }

    public static String[] SplitTracks(String Stripe)
    {
      String[] _tracks;

      try
      {
        _tracks = Stripe.Split(TRACK_END, StringSplitOptions.RemoveEmptyEntries);
        for (int _idx = 0; _idx < _tracks.Length; _idx++)
        {
          if (MagneticStripe.StartsWithTrackBeginMark(_tracks[_idx]))
          {
            _tracks[_idx] = _tracks[_idx].Substring(1);
          }
        }

        return _tracks;
      }
      catch
      {
        _tracks = new String[0];
        return _tracks;
      }
    }

    public static String[] SplitFields(String StripeTrack)
    {
      String[] _fields;

      try
      {
        _fields = StripeTrack.Split(TRACK_FIELD, StringSplitOptions.RemoveEmptyEntries);
        for (int _idx = 0; _idx < _fields.Length; _idx++)
        {
          _fields[_idx] = _fields[_idx].Trim();
        }
        return _fields;
      }
      catch
      {
        _fields = new String[0];
        return _fields;
      }
    }
  }

  public class MagneticStripeBankCard
  {
    private String m_stripe = String.Empty;
    private String[] m_tracks = new String[] { };
    private String m_number = String.Empty;
    private String m_holder = String.Empty;
    private String m_expiration = String.Empty;


    public String CardNumber
    {
      get
      {
        return m_number;
      }
    }
    public String HolderName
    {
      get
      {
        return m_holder;
      }
    }
    public String ExpirationYYMM
    {
      get
      {
        return m_expiration;
      }
    }
    public String Stripe
    {
      get
      {
        return m_stripe;
      }
    }

    public String DecoratedCardNumber
    {
      get
      {
        return DecorateCardNumber(m_number);
      }
    }


    static public String UndecorateCardNumber(String CardNumber)
    {
      String _number;

      try
      {
        if (String.IsNullOrEmpty(CardNumber))
        {
          return String.Empty;
        }

        _number = "";
        foreach (Char _d in CardNumber)
        {
          if (Char.IsDigit(_d))
          {
            _number = _number + _d;
          }
        }

        return _number;
      }
      catch
      { ;}
      return CardNumber;
    }


    static public String DecorateCardNumber(String CardNumber)
    {
      String _number;
      String _format_list;
      String[] _formats;
      String _formated;
      String _original;
      String[] _groups;
      String _aux;

      int _idx;
      int _n;

      _original = CardNumber;

      try
      {
        CardNumber = UndecorateCardNumber(CardNumber);
        if (String.IsNullOrEmpty(CardNumber))
        {
          return String.Empty;
        }

        _format_list = GeneralParam.GetString("BankCard", "Formats", "16:4-4-4-4, 15:4-6-5");

        _formats = _format_list.Split(new String[] { "," }, StringSplitOptions.RemoveEmptyEntries);
        foreach (String _format in _formats)
        {
          _idx = _format.IndexOf(":");
          if (_idx <= 0)
          {
            continue;
          }
          if (!int.TryParse(_format.Substring(0, _idx), out _n))
          {
            continue;
          }
          if (_n != CardNumber.Length)
          {
            continue;
          }

          _aux = _format.Substring(_idx + 1);
          _aux = _aux.Trim();
          if (String.IsNullOrEmpty(_aux))
          {
            continue;
          }

          _number = CardNumber;
          _formated = "";
          _groups = _aux.Split(new String[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
          foreach (String _g in _groups)
          {
            if (!int.TryParse(_g, out _n))
            {
              continue;
            }

            if (_formated.Length > 0)
            {
              _formated = _formated + "-";
            }
            _formated = _formated + _number.Substring(0, _n);
            _number = _number.Substring(_n);
          }

          if (_formated.Length == CardNumber.Length + _groups.Length - 1)
          {
            return _formated;
          }
        }
      }
      catch
      { ;}

      return _original;
    }

    public static Boolean TryParse(String Stripe, out MagneticStripeBankCard MSBank)
    {
      String[] _tracks;
      String[] _fields;
      String _stripe;
      String _track1;
      String _aux;

      MSBank = new MagneticStripeBankCard();

      try
      {
        _stripe = Stripe.ToUpper();
        _tracks = MagneticStripe.SplitTracks(_stripe);
        if (_tracks.Length <= 0)
        {
          return false;
        }
        _track1 = _tracks[0];
        if (_track1.Length <= 0)
        {
          return false;
        }
        if (_track1[0] != 'B')
        {
          return false;
        }

        MSBank.m_stripe = _stripe;
        MSBank.m_tracks = _tracks;

        _track1 = _track1.Substring(1);
        _fields = MagneticStripe.SplitFields(_track1);
        for (int _idx_fld = 0; _idx_fld < _fields.Length; _idx_fld++)
        {
          _aux = _fields[_idx_fld].Trim();
          // Fields
          // - PAN
          // - Holder
          // - Expiration
          if (_idx_fld == 0)
          {
            MSBank.m_number = _aux;

            continue;
          }

          if (_idx_fld == 1)
          {
            _aux = _aux.Replace("-", ", ");
            _aux = _aux.Replace("/", ", ");
            MSBank.m_holder = _aux;

            continue;
          }

          if (_idx_fld == 2)
          {
            if (_aux.Length >= 4)
            {
              MSBank.m_expiration = _aux.Substring(0, 4);
            }
            continue;
          }

          break;
        }

        if (String.IsNullOrEmpty(MSBank.m_number)) return false;

        return true;
      }
      catch
      { ;}

      return false;
    }
  }

  class BarcodeReader
  {
    System.IO.Ports.SerialPort m_sp;
    BarcodeRead m_handler = null;

    public delegate void BarcodeRead(String Barcode);

    public void Init(BarcodeRead Handler)
    {
      Thread _thread;

      m_handler = Handler;
      _thread = new Thread(this.Loop);
      _thread.Start();
    }

    public void Loop()
    {
      String _barcode;
      String _read;
      Byte[] _buffer;
      int _len;
      int _wait_hint;

      m_sp = null;
      _buffer = new byte[100];

      _wait_hint = 0;

      while (true)
      {
        Thread.Sleep(_wait_hint);
        _wait_hint = 5000;

        try
        {
          if (m_sp == null)
          {
            m_sp = new SerialPort("COM3", 9600, Parity.None, 8, StopBits.One);
            m_sp.ParityReplace = 0;
            continue;
          }

          if (!m_sp.IsOpen)
          {
            m_sp.Open();
            continue;
          }

          _barcode = String.Empty;

          while (true)
          {
            if (_barcode.Length == 0)
            {
              m_sp.ReadTimeout = int.MaxValue;
            }
            else
            {
              m_sp.ReadTimeout = 50;
            }

            try
            {
              _len = m_sp.Read(_buffer, 0, _buffer.Length);
              _read = Encoding.ASCII.GetString(_buffer, 0, _len);
              foreach (char _c in _read)
              {
                if (Char.IsDigit(_c))
                {
                  _barcode += _c;
                }
              }
              _wait_hint = 0;
            }
            catch (TimeoutException)
            {
              if (_barcode.Length > 0)
              {
                // Notify Barcode Read!
                try
                {
                  BarcodeRead _handler;
                  _handler = m_handler;
                  if (_handler != null)
                  {
                    _handler(_barcode);
                  }
                }
                catch
                { ;}
              }

              _barcode = String.Empty;
              continue;
            }
          }
        }
        catch
        {
          _wait_hint = 30000;
        }

      }
    }

  }

}
