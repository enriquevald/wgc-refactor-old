//-------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//---------------- ---------------------------------------------------
//
// MODULE NAME:   MultiPromos.cs
// DESCRIPTION:   
// AUTHOR:        Marcos Piedra Osuna
// CREATION DATE: 05-JUN-2012
//
// REVISION HISTORY:
// 
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 05-JUN-2012  MPO    Initial version
// 16-AUG-2012  RCI    Check if System has disabled new PlaySessions
// 16-AUG-2012  RCI    Release inactive play sessions and block related accounts.
// 19-SEP-2012  DDM    Added function Trx_SetInitialREPromotionInAccounts.
// 21-JAN-2013  DDM    Fixed Bug #516.
// 19-JUL-2013  RCI & AJQ    Fixed Bug WIG-83: ManualEndSession continues the process even the play session is already closed.
// 27-SEP-2013  JPJ    Added new call to logger.
// 07-OCT-2013  RCI    Fixed Bug WIG-266: Can't remove NR2 Promotion
// 25-OCT-2013  AMF    Fixed Bug WIG-310: Can't close play session
// 14-NOV-2013  JML & RCI    WIG-409: Reset promotion cost when aren't active promotions.
// 21-NOV-2013  AMF     New comment when close session.
// 28-NOV-2013  NMR     Clear some account columns when is TITO mode
// 06-DEC-2013  RCI & QMP    Access column AC_VIRTUAL_TERMINAL only in TITO mode
// 07-JAN-2014  AMF    Fixed Bug WIGOSTITO-947: close play session
// 15-JAN-2014  JML    Fixed Bug WIGOSTITO-982: Computation of played NR
// 25-MAR-2014  DDM & RCI & MPO    Fixed Bug WIGOSTITO-1169: Calculate PLAY_SESSIONS meters correctly (Redeemable and Non-Redeemable parts)
// 19-JUL-2013  RCI & AJQ     Fixed Bug WIG-939: Manual EndSession when already closed, duplicates account balance
// 06-AUG-2014  MPO    WIGOSTITO-1249: TITO mode when AutoCloseSession. Trx_ReleaseInactivePlaySession Modified.
// 28-OCT-2014 SMN     Added new functionality: BLOCK_REASON field can have more than one block reason.
// 30-DEC-2014  SGB    Fixed Bug WIG-1906: Can't close play session
// 28-Jan-2015  JML    Reset cost promotions when recharging in the following cases: extra ball or the amount NR less than GP
// 27-JAN-2015  RCI    WIG-1954: Play sessions abandoned when cashout a NR ticket
// 30-JAN-2015  JBC    BirthDay alarm feature
// 05-FEB-2015  XCD    TITO: Added functionality to transfer NR credit from player account to terminal (using LCD Touch)
// 08-JUN-2015  MPO    WIG-2411: When cancel PromoNR is needed update TE_ACCOUNT_PROMO
// 07-JUL-2015  DRV    WIG-2548: Abandoned play sessions are not beeing notified to SPACE
// 21-JUL-2015  MPO    WIG-2599: Wrong behavior when there are promotions restricted
// 14-SEP-2015  DHA    Product Backlog Item 3705: Added coins from terminals
// 04-NOV-2015  JCA    Product Backlog Item 6145: BonoPLUS: Reserved Credit
// 04-DEC-2015  MPO    Fixed Bug 7060:Ref. 15201: Incorrect initial balance when cancel session
// 07-DIC-2015  FJC    Fixed BUG: 7360 (Added Cashier Movement when we reserve credit)
// 11-DIC-2015  JCA & FJC  BackLog Item 7311: Bonoplay=> Mixed Mode.
// 16-DIC-2015  JCA & FJC Fixed Bug 7779:Al insertar una tarjeta con cr�ditos No redimibles no se transfiere al terminal
// 22-DIC-2015  FJC    Fixed BUG 7564: When cancel last recharge, appear reserve credit in negative.
// 22-DIC-2015  FJC    Fixed BUG 7365: When cancel last recharge, doesn't reset the value of reserved
// 22-DIC-2015  FJC    Fixed BUG 7870: When cancel last Manual Pay, doesn't substract from reserved
// 22-DIC-2015  FJC    Fixed BUG 7869: When well chipps to Player, doesn't substract from reserved
// 22-DIC-2015  FJC    Fixed BUG 7867: When cancel Promo Redimible, doesn't reset the value of reserved
// 11-JAN-2016  SGB    Backlog Item 7910: Change column AC_POINTS to bucket 1.
// 19-JAN-2016  JRC    Product Backlog Item 7909:Multiple buckets: Insert into PENDING_PLAY_SESSIONS_TO_PLAYER_TRACKING
// 27-JAN-2016  JMV    Product Backlog Item 8257:Multiple buckets: Canje de buckets NR
// 01-FEB-2016  FJC    Bug 8855:BonoPlay: No se recupera correctamente el cr�dito reservado al insertar la tarjeta.
// 16-FEB-2016  SMN    Error en Updatar el Bucket RN
// 23-FEB-2016  ETP    Added AccountBlockReason.INACTIVE_PLAY_SESSION and block movements.
// 23-FEB-2016  FGB    Fixed method Trx_UpdateAccountBalance for MultiSite Center
// 26-FEB-2016  ETP    Product Backlog Item 9748: Multiple buckets: Canje de buckets RE.
// 26-FEB-2016  FGB    Product Backlog Item 9781: Multiple Buckets: Refactorizaci�n
// 01-MAR-2016  FGB    Product Backlog Item 9781: Multiple Buckets: Refactorizaci�n
// 03-MAR-2016  ETP    Fixed BUG 9957: Incorrect block reason.
// 03-MAR-2016  RAB    Bug 7608: The amounts reported to generate alarms from HighRollers are not correct
// 22-FEB-2016  FJC    Product Backlog Item 9434:PimPamGo: Otorgar Premios
// 18-MAR-2016  SMN    Product Backlog Item 10792:Multiple Buckets: Desactivar Acumulaci�n de Buckets si SPACE is enabled
// 23-MAR-2016  FGB    Bug 10955:Multiple Buckets: Does not have to compute buckets for anonymous or virtual accounts
// 30-MAR-2016  SMN    Bug 11202:Buckets: No se generan puntos en Tito
// 06-APR-2016  FJC    Fixed Bug 11201:Lcd Touch: Cuando el terminal rechaza cr�ditos Nr, se crea un start session
// 02-MAY-2016  ETP    Fixed Bug 11426: Sas host: IsAccountAnonymousOrVirtual: Error cuando la cuenta es virtual.
// 06-JUN-2016  JCA    Bug 14240:PimPamGo: Al jugar no genera puntos
// 09-JUN-2016  ETP    Bug 14343: Buckets: Redondeo al transferir promociones redimibles y No redimibles
// 13-JUN-2016  ETP    Bug 14467:Buckets: se transfiere de la cuenta al LCD unicamente los promocionales No redimibles
// 02-AGO-2016  JRC     PBI 16257:Buckets - RankingLevelPoints_Generated - RankingLevelPoints_Discretional
// 20-AGO-2016  PDM    PBI 16148:WebService FBM - Adaptar l�gica a los m�todos de FBM
// 22-NOV-2016  JMM    PBI 19744:Pago autom�tico de handpays
// 26-JAN-2016  DHA    BUG 23532: NR credit not added on account
// 27-JAN-2016  JML    Fixed Bug 23692: Cashless - Sale of chips: error in integrated mode with promotion
// 21-MAR-2017  JMM    Fixed Bug 25819: MICO2: No se puede iniciar sesi�n mediante tarjeta.
// 15-MAR-2017  ETP    WIGOS- 109: Add marker information to cashier forms.
// 20-APR-2017  JML    Fixed Bug 26807: Custody - Incorrect custody amount to be refunded
// 24-APR-2017  JMM    Bug 26301: MICO2 - Descuadre en Sesion de juego - inicio en Saldo Cero
// 22-JUN-2016  OM     PBI 28331: WS2S - Create Bucket for one VendorId
// 23-JUN-2017  FJC    WIGOS-3133 WS2S_StartCardSession
// 26-JUN-2017  OMC    WIGOS-3141 WS2S_EndCardSession
// 28-APR-2017  AMF    WIGOS-1105: Jackpots Contribution - Accumulation amounts
// 19-JUN-2017  JML    PBI 27999:WIGOS-2699 - Rounding - Accumulate the remaining amount
// 05-JUL-2017  RGR    Fixed Bug 28571:WIGOS-2994 Custody Ticket
// 06-JUL-2017  JML    PBI 28422: WIGOS-2702 Rounding - Consume the remaining amount
// 14-JUL-2017  DPC    PBI 16148: WebService FBM - Adaptar l�gica a los m�todos de FBM
// 18-JUL-2017  ETP    WIGOS-3515 New Jackpots option menu appears in wrong place and with wrong description.
// 01-SEP-2017  JML    PBI 29479: WIGOS-4700 Promotional credit reported as cashable after card out/in
// 28-SEP-2017  DPC    WIGOS-5446: Mico2: Error when play session inactive
// 02-NOV-2017  JMM    WIGOS-5042: [Ticket #611] Ver logger sesiones descuadradas
// 29-NOV-2017  JML    Bug 30948:WIGOS-6888 [Ticket #10804] Error en la acumulaci�n de puntos
// 14-DEC-2017  RAB    Bug 31085:WIGOS-5741 Abandoned play session does not report amounts for Total columns
// 04-ENE-2018  FJC    Fixed Bug: Bug 31190: WIGOS-7454 Marina del Sol -Error when we are transfering money to Machine-
// 11-ENE-2018  DHA&JML    Bug 31085:WIGOS-5741 Abandoned play session does not report amounts for Total columns
// 01-JAN-2018  DHA    Bug 31392:WIGOS-7261 Cashier - Prize payment operation doesn't work. Warning mesage "No es posible realizar la operaci�n" is displayed.
// 01-JAN-2018  DHA    Bug 31393:WIGOS-7793 Service charge: wrong calculation with Terminal draw
// 09-APR-2018  AGS    Bug 32255:WIGOS-9826 Play session information is incorrect (played and won amounts)
// 28-JUN-2018  ACC    Bug 32947:WIGOS-12628/12954 [Ticket #14872] sesiones descuadradas en jugado Colonial 612  --->   Add to Initial Balance Bills+Coins+Tickets
// -------------------------------------------------------------------

using System;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Xml;

namespace WSI.Common
{
  public static class MultiPromos
  {

    #region Class

    public const Int64 HANDPAY_VIRTUAL_PLAY_SESSION_ID = Int64.MinValue;

    public enum StartSessionStatus
    {
      Ok = 0,
      // Generic Error
      Error = 1,
      // Account Error
      AccountUnknown = 10,
      AccountBlocked = 11,
      AccountInSession = 12,
      // Terminal Error
      TerminalUnknown = 20,
      TerminalBlocked = 21,
    }

    public enum StartSessionTransferMode
    {
      Default = 0,
      Partial = 1,
      All = 2,
      IncludeReserved = 3,// 04-NOV-2015  JCA    Product Backlog Item 6145: BonoPLUS: Reserved Credit
      Mico2 = 4
    }


    public class StartSessionInput
    {
      public Int64 TransactionId;
      public int TerminalId;
      public String TrackData;
      public Boolean IsTitoMode;
      public StartSessionTransferMode TransferMode = StartSessionTransferMode.Default;
      public AccountBalance AccountBalanceToTransfer = AccountBalance.Zero;
      public Boolean TransferWithBuckets = false;
    }

    public class StartSessionOutput
    {
      public Int64 PlaySessionId = 0;
      public Boolean IsNewPlaySession = false;

      public AccountBalance IniBalance = AccountBalance.Zero;
      public AccountBalance PlayableBalance = AccountBalance.Zero;
      public AccountBalance FinBalance = AccountBalance.Zero;

      public AccountBalance TotalToGMBalance = AccountBalance.Zero;
      public Decimal Points = 0;
      public String HolderName;


      public StartSessionStatus StatusCode = StartSessionStatus.Error;
      public StringBuilder ErrorMsg = new StringBuilder();

      public void SetError(StartSessionStatus Error, String Message)
      {
        StatusCode = Error;
        if (!String.IsNullOrEmpty(Message))
        {
          ErrorMsg.AppendLine(Message);
        }
      }

      public void Warning(String Message)
      {
        if (!String.IsNullOrEmpty(Message))
        {
          ErrorMsg.AppendLine("Warning: " + Message);
        }
      }

      public void SetSuccess()
      {
        StatusCode = StartSessionStatus.Ok;
      }

      public String Message
      {
        get
        {
          return ErrorMsg.ToString();
        }
      }
    }

    public class EndSessionInput
    {
      public MovementType MovementType = MovementType.EndCardSession;
      public String CashierName = ""; // Used for manual close in Cashier and GUI.

      public Int64 AccountId = -1;
      public Int64 PlaySessionId = -1;
      public Int64 TransactionId = 0;

      public Boolean FinalBalanceSelectedByUserAfterMismatch = false;
      public Decimal BalanceFromGM = 0;

      public Boolean HasMeters = false;
      public PlayedWonMeters Meters = new PlayedWonMeters();

      public Boolean IsTITO = false;
      public Int64 VirtualAccountId = 0;
      public TITOSessionMeters TitoSessionMeters = new TITOSessionMeters();

      public String CloseSessionComment = "";

      public Boolean IsMico2 = false;
      public MultiPromos.AccountBalance BalanceToAccount = MultiPromos.AccountBalance.Zero;
      public MultiPromos.AccountBalance DeltaBalanceFoundInSession = MultiPromos.AccountBalance.Zero;
      public MultiPromos.AccountBalance RemainingInEgm = MultiPromos.AccountBalance.Zero;
      public MultiPromos.AccountBalance FoundInEgm = MultiPromos.AccountBalance.Zero;
    }

    public class PlayedWonMeters
    {
      public Int64 PlayedCount = 0;
      public Decimal PlayedAmount = 0;
      public Int64 WonCount = 0;
      public Decimal WonAmount = 0;
      public Decimal JackpotAmount = 0;
      public Decimal BillInAmount = 0;

      public Decimal PlayedReedemable = 0;
      public Decimal PlayedRestrictedAmount = 0;
      public Decimal PlayedNonRestrictedAmount = 0;

      public Decimal HandpaysAmount = 0;
    }

    // TITO Session Meters
    public class TITOSessionMeters
    {
      public Decimal TicketInCashable;
      public Decimal TicketInPromoNr;
      public Decimal TicketInPromoRe;
      public Decimal TicketOutCashable;
      public Decimal TicketOutPromoNr;
      public TITOSessionMetersCashIn CashIn = new TITOSessionMetersCashIn();

      public Decimal RedeemableCashIn
      {
        get
        {
          return TicketInCashable + CashIn.CashInTotal;
        }
      }

      public Decimal PromoReCashIn
      {
        get
        {
          return TicketInPromoRe;
        }
      }

      public Decimal PromoNrCashIn
      {
        get
        {
          return TicketInPromoNr;
        }
      }

      public Decimal TotalCashIn
      {
        get
        {
          return RedeemableCashIn + PromoReCashIn + PromoNrCashIn;
        }
      }

      public Decimal RedeemableCashOut
      {
        get
        {
          return TicketOutCashable;
        }
      }

      public Decimal PromoNrCashOut
      {
        get
        {
          return TicketOutPromoNr;
        }
      }

      public Decimal TotalCashOut
      {
        get
        {
          return RedeemableCashOut + PromoNrCashOut;
        }
      }

    };

    // TITO Session Meters Cash In
    public class TITOSessionMetersCashIn
    {
      public Decimal CashInCoins;
      public Decimal CashInBills;
      public Decimal CashInTotal;
    }
    public enum EndSessionStatus
    {
      Ok = 0,
      ErrorRetry = 1,
      FatalError = 2,
      NotOpened = 3,
      BalanceMismatch = 4,
      // Account Error
      AccountUnknown = 10,
      AccountBlocked = 11,
      AccountInSession = 12,
      InvalidPlaySession = 13,
      // Terminal Error
      TerminalUnknown = 20,
      TerminalBlocked = 21,
    }

    public class EndSessionOutput
    {
      public AccountBalance PlaySessionFinalBalance = AccountBalance.Zero;

      public EndSessionStatus StatusCode = EndSessionStatus.ErrorRetry;
      public StringBuilder ErrorMsg = new StringBuilder();

      public void SetError(EndSessionStatus Error, String Message)
      {
        StatusCode = Error;
        if (!String.IsNullOrEmpty(Message))
        {
          ErrorMsg.AppendLine(Message);
        }
      }

      public void Warning(String Message)
      {
        if (!String.IsNullOrEmpty(Message))
        {
          ErrorMsg.AppendLine("Warning: " + Message);
        }
      }

      public void SetSuccess()
      {
        StatusCode = EndSessionStatus.Ok;
      }

      public String Message
      {
        get
        {
          return ErrorMsg.ToString();
        }
      }
    }

    public class InSessionParameters
    {
      public Int64 PlaySessionId = 0;
      public Int64 AccountId = 0;
      public DataTable AccountPromotion = null;

      public Decimal DeltaPlayed = 0;
      public Decimal DeltaWon = 0;

      // In Session
      public Decimal DeltaPlayedRe = 0;
      public Decimal DeltaPlayedNr = 0;
      public Decimal DeltaWonRe = 0;
      public Decimal DeltaWonNr = 0;

      public AccountBalance Balance = AccountBalance.Zero;

      public AccountBalance Played = AccountBalance.Zero;
      public AccountBalance Won = AccountBalance.Zero;

      public Boolean IsMico2 = false;
      public AccountBalance BalanceToAccount = AccountBalance.Zero;
      public AccountBalance DeltaBalanceFoundInSession = AccountBalance.Zero;
    }

    public class PlaySessionCloseParameters
    {
      public MovementType MovementType = MovementType.EndCardSession;
      public String CashierName = ""; // Used for manual close in Cashier and GUI.

      public Int64 PlaySessionId = 0;
      public Int64 AccountId = 0;
      public Int32 TerminalId = 0;
      public String TerminalName = "";

      public Boolean BalanceMismatch = true;
      public AccountBalance BalanceFromGMToAdd = AccountBalance.Zero;
      public StringBuilder ErrorMsg = new StringBuilder();
      public Int64 NumPlayed = 0;
      public AccountBalance Played = AccountBalance.Zero;
      public Int64 NumWon = 0;
      public AccountBalance Won = AccountBalance.Zero;

      public Boolean IsTITO = false;
      public Int64 VirtualAccountId = 0;
      public TITOSessionMeters TitoSessionMeters = new TITOSessionMeters();

      public string CloseSessionComment = "";
      public Boolean IsMico2 = false;

      public Decimal HandpaysCents = 0;
    }

    public class AccountBalance : ICloneable
    {
      public Decimal Redeemable;
      public Decimal PromoRedeemable;
      public Decimal PromoNotRedeemable;
      public Decimal Reserved;
      public Boolean ReservedDiscounted;
      public Boolean ModeReserved;
      public Boolean ModeReservedChanged;
      public Boolean GetOnlyReserved;
      public Decimal BucketNRCredit;
      public Decimal BucketRECredit;
      public Decimal CustodyProvision;
      public Decimal Operation_ChipsSalesRemainingAmount;
      public Decimal Account_ChipsSalesRemainingAmount;

      public override String ToString()
      {
        return String.Format("{0} <RE: {1}, PR: {2}, NR: {3}>", TotalBalance.ToString(), Redeemable.ToString(), PromoRedeemable.ToString(), PromoNotRedeemable.ToString());
      }

      public AccountBalance()
      {
        this.ReservedDiscounted = false;
        this.Operation_ChipsSalesRemainingAmount = 0;
        this.GetOnlyReserved = false;
      } // AccountBalance

      public AccountBalance(AccountBalance Value)
      {
        this.Redeemable = Value.Redeemable;
        this.PromoRedeemable = Value.PromoRedeemable;
        this.PromoNotRedeemable = Value.PromoNotRedeemable;
        this.Reserved = Value.Reserved;
        this.ReservedDiscounted = Value.ReservedDiscounted;
        this.ModeReserved = Value.ModeReserved;
        this.ModeReservedChanged = Value.ModeReservedChanged;
        this.GetOnlyReserved = Value.GetOnlyReserved;
        this.BucketNRCredit = Value.BucketNRCredit;
        this.BucketRECredit = Value.BucketRECredit;
        this.CustodyProvision = Value.CustodyProvision;
      } // AccountBalance

      public AccountBalance(Decimal Redeemable, Decimal PromoRedeemable, Decimal PromoNotRedeemable)
      {
        this.Redeemable = Redeemable;
        this.PromoRedeemable = PromoRedeemable;
        this.PromoNotRedeemable = PromoNotRedeemable;
        this.CustodyProvision = 0;
        this.Reserved = 0;
        this.ReservedDiscounted = false;
        this.ModeReserved = false;
        this.ModeReservedChanged = false;
        this.GetOnlyReserved = false;
        this.BucketNRCredit = 0;
        this.BucketRECredit = 0;
      } // AccountBalance

      public AccountBalance(Decimal Redeemable, Decimal PromoRedeemable, Decimal PromoNotRedeemable, Decimal CustodyProvision)
        : this(Redeemable, PromoRedeemable, PromoNotRedeemable)
      {
        this.CustodyProvision = CustodyProvision;
      } // AccountBalance

      public AccountBalance(Decimal Redeemable, Decimal PromoRedeemable, Decimal PromoNotRedeemable, Decimal Reserved, Boolean ModeReserved)
        : this(Redeemable, PromoRedeemable, PromoNotRedeemable)
      {
        this.Reserved = Reserved;
        this.ModeReserved = ModeReserved;
        this.ModeReservedChanged = true;
      }

      public AccountBalance(Decimal Redeemable, Decimal PromoRedeemable, Decimal PromoNotRedeemable, Decimal Reserved, Boolean ModeReserved, Decimal BucketNRCredit, Decimal BucketRECredit)
        : this(Redeemable, PromoRedeemable, PromoNotRedeemable, Reserved, ModeReserved)
      {
        this.PromoNotRedeemable += BucketNRCredit;
        this.BucketNRCredit = BucketNRCredit;
        this.PromoRedeemable += BucketRECredit;
        this.BucketRECredit = BucketRECredit;
      }

      public AccountBalance(Decimal Redeemable, Decimal PromoRedeemable, Decimal PromoNotRedeemable, Decimal Reserved, Boolean ModeReserved, Decimal BucketNRCredit, Decimal BucketRECredit, Decimal CustodyProvision, Decimal Account_ChipsSalesRemainingAmount, Boolean IsEnableRoundingSaleChips)
        : this(Redeemable, PromoRedeemable, PromoNotRedeemable, Reserved, ModeReserved, BucketNRCredit, BucketRECredit)
      {
        this.CustodyProvision += CustodyProvision;
        if (IsEnableRoundingSaleChips)
        {
          this.Account_ChipsSalesRemainingAmount += Account_ChipsSalesRemainingAmount;
        }
        else
        {
          this.Account_ChipsSalesRemainingAmount = 0;
        }
      }

      public Decimal TotalRedeemable
      {
        get
        {
          return Redeemable + PromoRedeemable;
        }
      }

      public Decimal TotalNotRedeemable
      {
        get
        {
          return PromoNotRedeemable;
        }
      }

      public Decimal TotalBalance
      {
        get
        {
          return Redeemable + PromoRedeemable + PromoNotRedeemable;
        }
      }

      static public AccountBalance Zero
      {
        get
        {
          return new AccountBalance();
        }
      }

      // 04-NOV-2015  JCA    Product Backlog Item 6145: BonoPLUS: Reserved Credit
      public AccountBalance BalanceEGM
      {
        get
        {
          Decimal _reserved;

          if (this.ReservedDiscounted == true || Reserved == 0)
          {
            return this;
          }

          //IF no enough Redeemable 
          if (TotalRedeemable < Reserved)
          {
            Redeemable = 0;
            PromoRedeemable = 0;

            return this;
          }

          _reserved = Reserved;

          if (Redeemable >= _reserved)
          {
            Redeemable -= _reserved;
            _reserved = 0;
            this.ReservedDiscounted = true;
          }
          else
          {
            _reserved -= Redeemable;
            Redeemable = 0;
          }

          if (_reserved > 0 && PromoRedeemable >= _reserved)
          {
            PromoRedeemable -= _reserved;
            this.ReservedDiscounted = true;
          }

          this.BucketNRCredit = 0;

          return this;
        }
      }//BalanceEGM

      public AccountBalance BalanceEGMReservedFBM
      {
        get
        {
          Decimal _reserved;

          this.GetOnlyReserved = true;
          PromoNotRedeemable = 0;

          //IF no enough Redeemable or there's no Reserved
          if (TotalRedeemable < Reserved || Reserved == 0)
          {
            Redeemable = 0;
            PromoRedeemable = 0;

            return this;
          }

          _reserved = Reserved;

          if (Redeemable >= _reserved)
          {
            Redeemable = _reserved;
            _reserved = 0;
            PromoRedeemable = 0;
          }
          else
          {
            _reserved -= Redeemable;
          }

          if (_reserved > 0 && PromoRedeemable >= _reserved)
          {
            PromoRedeemable = _reserved;
          }

          this.BucketNRCredit = 0;

          return this;
        }
      }//BalanceEGM

      
      public static AccountBalance operator +(AccountBalance Bal1, AccountBalance Bal2)
      {
        return new AccountBalance(Bal1.Redeemable + Bal2.Redeemable,
                                  Bal1.PromoRedeemable + Bal2.PromoRedeemable,
                                  Bal1.PromoNotRedeemable + Bal2.PromoNotRedeemable,
                                  Bal1.CustodyProvision + Bal2.CustodyProvision);
      }

      public static AccountBalance operator -(AccountBalance Bal1, AccountBalance Bal2)
      {
        return new AccountBalance(Bal1.Redeemable - Bal2.Redeemable,
                                  Bal1.PromoRedeemable - Bal2.PromoRedeemable,
                                  Bal1.PromoNotRedeemable - Bal2.PromoNotRedeemable,
                                  Bal1.CustodyProvision - Bal2.CustodyProvision);
      }

      public static Boolean operator ==(AccountBalance Bal1, AccountBalance Bal2)
      {
        return (Bal1.Redeemable == Bal2.Redeemable
              && Bal1.PromoRedeemable == Bal2.PromoRedeemable
              && Bal1.PromoNotRedeemable == Bal2.PromoNotRedeemable);
      }

      public static Boolean operator !=(AccountBalance Bal1, AccountBalance Bal2)
      {
        return !(Bal1 == Bal2);
      }

      public override bool Equals(object obj)
      {
        return (this == (AccountBalance)obj);
      }

      public override int GetHashCode()
      {
        return base.GetHashCode();
      }

      static public AccountBalance Negate(AccountBalance Bal1)
      {
        return Negate(Bal1, false);
      } // Negate

      static public AccountBalance Negate(AccountBalance Bal1, Boolean NegateReserved)
      {
        if (NegateReserved)
        {
          return new AccountBalance(-Bal1.Redeemable, -Bal1.PromoRedeemable, -Bal1.PromoNotRedeemable, -Bal1.Reserved, Bal1.ModeReserved);
        }
        else
        {
          return new AccountBalance(-Bal1.Redeemable, -Bal1.PromoRedeemable, -Bal1.PromoNotRedeemable, -Bal1.CustodyProvision);
        }
      } // Negate




#if !SQL_BUSINESS_LOGIC
      // For messages propose
      public String ToXml()
      {
        StringBuilder _sb;

        _sb = new StringBuilder();
        _sb.AppendLine("<Redeemable>" + ((Int64)(Redeemable * 100)).ToString() + "</Redeemable>");
        _sb.AppendLine("<PromoRedeemable>" + ((Int64)(PromoRedeemable * 100)).ToString() + "</PromoRedeemable>");
        // Only Append (the caller set end of line if is necessary).
        _sb.Append("<PromoNotRedeemable>" + ((Int64)(PromoNotRedeemable * 100)).ToString() + "</PromoNotRedeemable>");
        _sb.AppendLine("<Reserved>" + ((Int64)(Reserved * 100)).ToString() + "</Reserved>"); // 04-NOV-2015  JCA        Product Backlog Item 6145: BonoPLUS: Reserved Credit
        _sb.AppendLine("<ModeReserved>" + Convert.ToInt32(ModeReserved).ToString() + "</ModeReserved>"); // 30-NOV-2015  FJC  Product Backlog Item 6145: BonoPLUS: Mode Reserved by ACCOUNT
        return _sb.ToString();
      }

      public void LoadXml(XmlReader XmlReader)
      {
        Int64 _aux_int64;
        Int32 _aux_int32;

        if (Int64.TryParse(XML.ReadTagValue(XmlReader, "Redeemable", true), out _aux_int64))
        {
          Redeemable = ((Decimal)_aux_int64) / 100;
        }
        if (Int64.TryParse(XML.ReadTagValue(XmlReader, "PromoRedeemable", true), out _aux_int64))
        {
          PromoRedeemable = ((Decimal)_aux_int64) / 100;
        }
        if (Int64.TryParse(XML.ReadTagValue(XmlReader, "PromoNotRedeemable", true), out _aux_int64))
        {
          PromoNotRedeemable = ((Decimal)_aux_int64) / 100;
        }
        if (Int64.TryParse(XML.ReadTagValue(XmlReader, "Reserved", true), out _aux_int64)) // 04-NOV-2015  JCA    Product Backlog Item 6145: BonoPLUS: Reserved Credit
        {
          Reserved = ((Decimal)_aux_int64) / 100;
        }
        if (Int32.TryParse(XML.ReadTagValue(XmlReader, "ModeReserved", true), out _aux_int32)) // 30-NOV-2015  FJC  Product Backlog Item 6145: BonoPLUS: Mode Reserved by ACCOUNT
        {
          ModeReserved = _aux_int32 == 1 ? true : false;
        }

      }
#endif




      public object Clone()
      {
        AccountBalance _retval = new AccountBalance();
        _retval.Redeemable = Redeemable;
        _retval.PromoRedeemable = PromoRedeemable;
        _retval.PromoNotRedeemable = PromoNotRedeemable;
        _retval.Reserved = Reserved;
        _retval.ReservedDiscounted = ReservedDiscounted;
        _retval.ModeReserved = ModeReserved;
        _retval.ModeReservedChanged = ModeReservedChanged;
        _retval.GetOnlyReserved = GetOnlyReserved;

        return _retval;
      }
    } // AccountBalance

    public class PromoBalance
    {
      public AccountBalance Balance;
      public Decimal Points;

      public override String ToString()
      {
        return String.Format("{0}, PT: {1}", Balance.ToString(), Points.ToString());
      }

      public PromoBalance()
      {
        Balance = AccountBalance.Zero;
        Points = 0;
      } // PromoBalance

      public PromoBalance(PromoBalance PromoBalance)
      {
        this.Balance = new AccountBalance(PromoBalance.Balance);
        this.Points = PromoBalance.Points;
      } // PromoBalance

      public PromoBalance(AccountBalance Balance, Decimal Points)
      {
        this.Balance = new AccountBalance(Balance);
        this.Points = Points;
      } // PromoBalance

      static public PromoBalance Zero
      {
        get
        {
          return new PromoBalance();
        }
      }

      public static PromoBalance operator +(PromoBalance Bal1, PromoBalance Bal2)
      {
        return new PromoBalance(Bal1.Balance + Bal2.Balance, Bal1.Points + Bal2.Points);
      }

      public static PromoBalance operator -(PromoBalance Bal1, PromoBalance Bal2)
      {
        return new PromoBalance(Bal1.Balance - Bal2.Balance, Bal1.Points - Bal2.Points);
      }

      public static Boolean operator ==(PromoBalance Bal1, PromoBalance Bal2)
      {
        return (Bal1.Balance == Bal2.Balance && Bal1.Points == Bal2.Points);
      }

      public static Boolean operator !=(PromoBalance Bal1, PromoBalance Bal2)
      {
        return !(Bal1 == Bal2);
      }

      public override bool Equals(object obj)
      {
        return (this == (PromoBalance)obj);
      }

      public override int GetHashCode()
      {
        return base.GetHashCode();
      }
    } // PromoBalance

    public class AccountInfo
    {
      public Int64 AccountId = 0;

      public Boolean Blocked = true;
      public Int32 BlockReason = 0;

      public Int32 HolderLevel = 0;
      public Int32 HolderGender = 0;
      public String HolderName = "";
      public AccountType AcType;

      public Int64 CancellableOperationId = 0;

      public Int64 CurrentPlaySessionId = 0;
      public Int32 CurrentTerminalId = 0;
      public PlaySessionStatus CurrentPlaySessionStatus = PlaySessionStatus.Closed;
      public Boolean CurrentPlaySessionBalanceMismatch = true;
      public Decimal CurrentPlaySessionInitialBalance = 0;
      public Decimal CurrentPlaySessionCashInAmount = 0;
      public Int64 CancellableTrxId = 0;
      public AccountBalance CancellableBalance = AccountBalance.Zero;

      public AccountBalance FinalBalance = AccountBalance.Zero;
      public Decimal FinalPoints = 0;

      public String ErrorMessage = "";

    }

    #endregion

    #region Public Methods

    public enum AccountPromotionsAction
    {
      CancelByPlayer = 1,
      NotRedeemableToRedeemable = 2,
    }

    public enum ALARM_BIRTHDAY
    {
      DO_NOTHING = 0,
      SEND_BIRTHDATE_ALARM = 1,
      SEND_BIRTHDATE_IS_NEAR = 2
    }

    public enum REQUEST_TYPE
    {
      REPORT_RESERVED_CREDIT = 0,
      CANCEL_RESERVED_CREDIT = 1,
      MODIFY_RESERVED_CREDIT = 2,
      REPORT_GET_CREDIT = 3
    }

    public enum REQUEST_TYPE_RESPONSE
    {
      REQUEST_TYPE_RESPONSE_OK = 0,
      REQUEST_TYPE_RESPONSE_ERROR = 1,
      REQUEST_TYPE_RESPONSE_NOT_ENOUGH_CREDIT = 2,
    }

    public enum GAME_GATEWAY_RESERVED_MODE
    {
      GAME_GATEWAY_RESERVED_MODE_WITHOUT_BUCKETS = 0,
      GAME_GATEWAY_RESERVED_MODE_WITH_BUCKETS = 1,
      GAME_GATEWAY_RESERVED_MODE_MIXED = 2,
    }

    public enum ENUM_GAME_GATEWAY_PROMO_BALANCE_ACTION
    {
      GAME_GATEWAY_PROMO_BALANCE_ACTION_NONE = 0,
      GAME_GATEWAY_PROMO_BALANCE_ACTION_COLLECT_PRIZE = 1,
      GAME_GATEWAY_PROMO_BALANCE_ACTION_WITHOUT_PIN = 2

    };

    // PURPOSE: Change the status to redeem of the promotions and then return the sum
    // PURPOSE: Change the status to cancelled and return the sum
    //
    //  PARAMS:
    //     - INPUT:
    //           - Action:
    //           - AccountId:
    //           - AffectedBalance:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if is updated successfully, false otherwise.

    public static Boolean Trx_AccountPromotionsAction(AccountPromotionsAction Action,
                                                      Int64 AccountId,
                                                      out AccountBalance AffectedBalance,
                                                      out Int32 NumPromos,
                                                      SqlTransaction Trx)
    {
      StringBuilder _sb;
      DataTable _promos;
      AccountBalance _ini_balance;
      String _sql_where;
      ACCOUNT_PROMO_STATUS _status;
      Int64 _previous_play_session_id;

      AffectedBalance = AccountBalance.Zero;
      NumPromos = 0;

      try
      {
        if (!MultiPromos.Trx_UpdateAccountBalance(AccountId, MultiPromos.AccountBalance.Zero, out _ini_balance, out _previous_play_session_id, Trx))
        {
          return false;
        }

        if (_previous_play_session_id != 0)
        {
          return false;
        }

        _sb = new StringBuilder();

        switch (Action)
        {
          case AccountPromotionsAction.CancelByPlayer:
            _status = ACCOUNT_PROMO_STATUS.CANCELLED_BY_PLAYER;
            _sb.Length = 0;
            _sb.AppendLine("          ACP_ACCOUNT_ID       = @pAccountId ");
            _sb.AppendLine("    AND   ACP_STATUS           = @pStatusActive ");
            // The account is the master and it's not in session. If any promotion is in session, it's an error: will be cancelled.
            //_sb.AppendLine("    AND   ACP_PLAY_SESSION_ID    IS NULL ");
            _sb.AppendLine("    AND   ACP_CREDIT_TYPE      IN ( @pCreditTypeNR1, @pCreditTypeNR2 ) ");
            _sb.AppendLine("    AND   ACP_PROMO_TYPE       <> @pPromoCoverCoupon ");
            _sb.AppendLine("    AND   ACP_BALANCE          >= 0 ");

            _sql_where = _sb.ToString();

            _sb.Length = 0;
            _sb.AppendLine("IF (SELECT COUNT(*)      FROM ACCOUNT_PROMOTIONS WITH(INDEX(IX_ACP_ACCOUNT_STATUS)) WHERE ACP_WONLOCK IS NOT NULL AND " + _sql_where + ") > 0 ");
            _sb.AppendLine("    SELECT ACP_UNIQUE_ID FROM ACCOUNT_PROMOTIONS WITH(INDEX(IX_ACP_ACCOUNT_STATUS)) WHERE ACP_WONLOCK IS NOT NULL AND " + _sql_where);
            _sb.AppendLine("ELSE ");
            _sb.AppendLine("  IF (SELECT COUNT(*)      FROM ACCOUNT_PROMOTIONS WITH(INDEX(IX_ACP_ACCOUNT_STATUS)) WHERE ACP_WONLOCK IS NULL AND " + _sql_where + ") > 0 ");
            _sb.AppendLine("      SELECT ACP_UNIQUE_ID FROM ACCOUNT_PROMOTIONS WITH(INDEX(IX_ACP_ACCOUNT_STATUS)) WHERE ACP_WONLOCK IS NULL AND " + _sql_where);
            _sb.AppendLine("  ELSE ");
            _sb.AppendLine("      SELECT CAST (-1 AS BIGINT) ");

            break;

          case AccountPromotionsAction.NotRedeemableToRedeemable:
            _status = ACCOUNT_PROMO_STATUS.REDEEMED;
            _sb.Length = 0;
            _sb.AppendLine("          ACP_ACCOUNT_ID       = @pAccountId ");
            _sb.AppendLine("    AND   ACP_STATUS           = @pStatusActive ");
            // When it's going to convert NR to RE, it's necessary to check that the promotions are not in session.
            _sb.AppendLine("    AND   ACP_PLAY_SESSION_ID IS NULL ");
            _sb.AppendLine("    AND   ACP_CREDIT_TYPE      = @pCreditTypeNR1 ");
            _sb.AppendLine("    AND   ACP_BALANCE         >= ACP_WONLOCK ");
            _sb.AppendLine("    AND   ACP_WONLOCK         IS NOT NULL ");

            _sql_where = _sb.ToString();

            _sb.Length = 0;
            _sb.AppendLine(" SELECT   ACP_UNIQUE_ID ");
            _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(IX_acp_account_status))");
            _sb.AppendLine("  WHERE   " + _sql_where);

            break;

          default:
            return false;
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
          _cmd.Parameters.Add("@pCreditTypeNR1", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR1;
          _cmd.Parameters.Add("@pCreditTypeNR2", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR2;
          _cmd.Parameters.Add("@pPromoCoverCoupon", SqlDbType.Int).Value = 2; // RCI & DDM 08-OCT-2013: WSI.Common.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _promos = new DataTable();
            _sql_da.Fill(_promos);
          }
        }

        if (_promos.Rows.Count <= 0)
        {
          return true;
        }

        _sb.Length = 0;
        _sb.AppendLine(" UPDATE   ACCOUNT_PROMOTIONS ");
        _sb.AppendLine("    SET   ACP_STATUS           = @pNewStatus ");
        _sb.AppendLine(" OUTPUT   DELETED.ACP_BALANCE ");
        _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions)) ");
        _sb.AppendLine("  WHERE   ACP_UNIQUE_ID        = @pUniqueId ");
        _sb.AppendLine("    AND   " + _sql_where);

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          SqlParameter _unique_id;

          _unique_id = _cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt);
          _cmd.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = _status;
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
          _cmd.Parameters.Add("@pCreditTypeNR1", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR1;
          _cmd.Parameters.Add("@pCreditTypeNR2", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR2;
          _cmd.Parameters.Add("@pPromoCoverCoupon", SqlDbType.Int).Value = 2; // RCI & DDM 08-OCT-2013: WSI.Common.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON;

          foreach (DataRow _promo in _promos.Rows)
          {
            _unique_id.Value = _promo[0];

            if ((Int64)_promo[0] == -1) // Fix any mismatch Account.NR > 0, Promos.NR = 0
            {
              NumPromos = 0;
              AffectedBalance.PromoNotRedeemable = _ini_balance.PromoNotRedeemable;

              break;
            }

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                // Not an error, conditions changed ...
                continue;
              }

              NumPromos++;
              AffectedBalance.PromoNotRedeemable += _reader.GetDecimal(0);
            }
          }
        }

        AffectedBalance.Redeemable = Math.Min(AffectedBalance.Redeemable, _ini_balance.Redeemable);
        AffectedBalance.PromoRedeemable = Math.Min(AffectedBalance.PromoRedeemable, _ini_balance.PromoRedeemable);
        AffectedBalance.PromoNotRedeemable = Math.Min(AffectedBalance.PromoNotRedeemable, _ini_balance.PromoNotRedeemable);

        return true;
      }
      catch
      { ; }

      return false;

    } // Trx_AccountPromotionsAction



    public static MultiPromos.StartSessionOutput Trx_WcpStartCardSession(MultiPromos.StartSessionInput Input,
                                                                         SqlTransaction Trx)
    {
      MultiPromos.StartSessionOutput _output;
      Terminal.TerminalInfo _terminal;
      MultiPromos.AccountInfo _account;

      _output = new MultiPromos.StartSessionOutput();

      if (Input.IsTitoMode)
      {
#if !SQL_BUSINESS_LOGIC

        Int64 AccountId;

        if (!Terminal.Trx_GetTerminalInfo(Input.TerminalId, out _terminal, Trx))
        {
          _output.SetError(MultiPromos.StartSessionStatus.TerminalUnknown, "");

          return _output;
        }

        AccountId = Accounts.GetOrCreateVirtualAccount(_terminal.TerminalId, Trx);
        if (AccountId == 0)
        {
          _output.SetError(MultiPromos.StartSessionStatus.AccountUnknown, "");

          return _output;
        }

        CashierSessionInfo _cs_info; // cashier session info
        Int64 _mc_id; // money collection id

        _cs_info = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_TITO, Trx, _terminal.Name);
        if (_cs_info == null)
        {
          _output.SetError(MultiPromos.StartSessionStatus.Error, "Cashier session not found for terminal:" + _terminal.TerminalId);

          return _output;
        }

        if (!WSI.Common.TITO.MoneyCollection.GetOrCreateTerminalMoneyCollectionId(_cs_info.CashierSessionId, Input.TerminalId, out _mc_id, Trx))
        {
          _output.SetError(MultiPromos.StartSessionStatus.Error, "Money collection not created");

          return _output;
        }

        if (Input.TransferMode == StartSessionTransferMode.Mico2)
        {
          Object _obj;
          String _internal_track_data;
          Int32 _card_type;

          using (SqlCommand _cmd = new SqlCommand("SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pInternal", Trx.Connection, Trx))
          {
            CardNumber.TrackDataToInternal(Input.TrackData, out _internal_track_data, out _card_type);

            _cmd.Parameters.Add("@pInternal", SqlDbType.NVarChar, 50).Value = _internal_track_data;
            _obj = _cmd.ExecuteScalar();

            if (_obj != null && _obj != DBNull.Value)
            {
              AccountId = (Int64)_obj;
            }
          }
        }

        if (!MultiPromos.Trx_UpdateAccountBalance(AccountId, String.Empty, MultiPromos.AccountBalance.Zero, 0, false, out _account, Trx))
        {
          _output.SetError(MultiPromos.StartSessionStatus.AccountUnknown, "");

          return _output;
        }

#else
        _output.SetError(MultiPromos.StartSessionStatus.Error, "SQL_BUSINESS_LOGIC doesn't support TITO Mode");

        return _output;
#endif
      }
      else
      {
        if (!MultiPromos.Trx_UpdateAccountBalance(0, Input.TrackData, MultiPromos.AccountBalance.Zero, 0, false, out _account, Trx))
        {
          _output.SetError(MultiPromos.StartSessionStatus.AccountUnknown, "");

          return _output;
        }

        if (!Terminal.Trx_GetTerminalInfo(Input.TerminalId, out _terminal, Trx))
        {
          _output.SetError(MultiPromos.StartSessionStatus.TerminalUnknown, "");

          return _output;
        }
      }

      return Trx_CommonStartCardSession(Input.TransactionId,
                                        _terminal,
                                        _account,
                                        Input.IsTitoMode,
                                        Input.TransferMode,
                                        Input.AccountBalanceToTransfer,
                                        Input.TransferWithBuckets,
                                        Trx);
    }

    // PURPOSE: 1) Block account.
    //          2) Retrieve the play session (if is necesarry insert new session).
    //          3) Compute Redeemable / NotRedeemable to be trasferred
    //          4) Substract the amount of the account to be transferred
    //          5) Create the StartCardSession movement
    //          6) Set activity
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - TerminalId:
    //           - TerminalType:
    //           - TerminalName:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if all the process was correct, false otherwise.

    public static StartSessionOutput Trx_CommonStartCardSession(Int64 TransactionId,
                                                                Terminal.TerminalInfo Terminal,
                                                                AccountInfo Account,
                                                                Boolean TITOMode,
                                                                String VendorId,
                                                                SqlTransaction Trx)
    {
      return Trx_CommonStartCardSession(TransactionId,
                                        Terminal,
                                        Account,
                                        TITOMode,
                                        StartSessionTransferMode.Default,
                                        AccountBalance.Zero,
                                        false,
                                        VendorId,
                                        Trx);
    }

    // PURPOSE: 1) Block account.
    //          2) Retrieve the play session (if is necesarry insert new session).
    //          3) Compute Redeemable / NotRedeemable to be trasferred
    //          4) Substract the amount of the account to be transferred
    //          5) Create the StartCardSession movement
    //          6) Set activity
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - TerminalId:
    //           - TerminalType:
    //           - TerminalName:
    //           - TransferMode
    //           - BalanceToTransfer
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if all the process was correct, false otherwise.
    public static StartSessionOutput Trx_CommonStartCardSession(Int64 TransactionId,
                                                                Terminal.TerminalInfo Terminal,
                                                                AccountInfo Account,
                                                                Boolean TITOMode,
                                                                StartSessionTransferMode TransferMode,
                                                                AccountBalance BalanceToTransfer,
                                                                Boolean TransferWithBuckets,
                                                                String VendorId,
                                                                SqlTransaction Trx)
    {
      StartSessionOutput _output;
      Int32 _disable_new_sessions;

      _output = new StartSessionOutput();

      //
      // RCI 16-AUG-2012: Check if the system has disabled new play sessions.
      //
      Int32.TryParse(ReadGeneralParams("Site", "DisableNewSessions", Trx), out _disable_new_sessions);
      if (_disable_new_sessions != 0)
      {
        _output.SetError(MultiPromos.StartSessionStatus.TerminalBlocked, "System has disabled new PlaySessions.");

        return _output;
      }

      //
      // Check Terminal
      //
      if (Terminal == null || Terminal.TerminalId == 0)
      {
        _output.SetError(MultiPromos.StartSessionStatus.TerminalUnknown, "");

        return _output;
      }

      if (Terminal.Blocked || Terminal.Status != TerminalStatus.ACTIVE)
      {
        _output.SetError(MultiPromos.StartSessionStatus.TerminalBlocked, "");

        return _output;
      }

      //
      // Check Account
      //
      if (Account == null || Account.AccountId == 0)
      {
        _output.SetError(MultiPromos.StartSessionStatus.AccountUnknown, "");

        return _output;
      }

      if (Account.Blocked)
      {
        _output.SetError(MultiPromos.StartSessionStatus.AccountBlocked, "");

        return _output;
      }

      if (!TITOMode)
      {
        if (Account.CurrentPlaySessionId != 0)
        {
          // In Session
          if (Terminal.TerminalType != TerminalTypes.SAS_HOST
              || Account.CurrentTerminalId != Terminal.TerminalId
              || Account.CurrentPlaySessionStatus != PlaySessionStatus.Opened
              || Account.CurrentPlaySessionBalanceMismatch)
          {
            _output.PlaySessionId = Account.CurrentPlaySessionId;
            _output.SetError(MultiPromos.StartSessionStatus.AccountInSession, "");

            return _output;
          }
        }
      }

      Trx_OnStartCardSession(Account, TransactionId,
                             Terminal.TerminalId, Terminal.TerminalType,
                             Terminal.Name, TITOMode,
                             TransferMode, BalanceToTransfer, TransferWithBuckets, VendorId, out _output, Trx);

      // Save the holder name.
      _output.HolderName = Account.HolderName;

      return _output;
    }

    private static Boolean Trx_OnStartCardSession(AccountInfo Account,
                                                  Int64 TransactionId,
                                                  Int32 TerminalId,
                                                  TerminalTypes TerminalType,
                                                  String TerminalName,
                                                  Boolean TITOMode,
                                                  StartSessionTransferMode TransferMode,
                                                  AccountBalance BalanceToTransfer,
                                                  Boolean TransferWithBuckets,
                                                  String VendorId,
                                                  out StartSessionOutput Output,
                                                  SqlTransaction Trx)
    {
      Int64 _play_session_id;
      AccountBalance _ini_balance;
      AccountBalance _fin_balance;
      AccountBalance _playable_balance;
      AccountBalance _total_to_gm_balance;
      AccountBalance _cancel_balance;
      AccountMovementsTable _am_table;
      InSessionAction _action;
      Decimal _ini_points;
      Decimal _fin_points;
      Boolean _is_new_session;
      Int64 _prev_play_session_id;
      AccountBalance _dummy_bal1;
      AccountBalance _dummy_bal2;
      StringBuilder _sb;
      Int64 _account_id;
      AccountType _terminal_account_type;
      Boolean _tito_is_transfer;
      Boolean _cashless_is_transfer;
      Boolean _is_transfer;
      AccountInfo _acc_info;
      Decimal _bucket_nr_credit;
      Decimal _bucket_re_credit;
      FlagBucketId _flag_buckets;
      AccountBalance _play_session_balance;
      Boolean _mico2_is_transfer;
      String _te_iso_code;
      Int64 _virtual_account;
      Boolean _tito_mode;
      Boolean _mico2_enabled;
      PlaySessionStatus _play_session_status;

      _flag_buckets = FlagBucketId.NONE;
      Output = new StartSessionOutput();
      Output.SetError(StartSessionStatus.Error, "");

      _account_id = Account.AccountId;
      _play_session_status = Account.CurrentPlaySessionStatus;

      try
      {
        _tito_is_transfer = false;
        _cashless_is_transfer = false;
        _mico2_is_transfer = false;
        _tito_mode = TITOMode;
        _mico2_enabled = ReadGeneralParams("Site", "SystemMode", Trx).Equals("4"); // MICO2

        if (_tito_mode)
        {
          _tito_is_transfer =
               TransferMode == StartSessionTransferMode.All
            || (TransferMode == StartSessionTransferMode.Partial && BalanceToTransfer.TotalBalance != 0);

          _mico2_is_transfer = TransferMode == StartSessionTransferMode.Mico2 && _mico2_enabled;
        }
        else
        {
          _cashless_is_transfer = TransferWithBuckets;
        }

        if (_mico2_is_transfer || (_mico2_enabled && _tito_is_transfer))
        {
          _tito_mode = false;
        }

        _is_transfer = _tito_is_transfer || _cashless_is_transfer;

        if (_tito_is_transfer)
        {
          _flag_buckets = FlagBucketId.NR;
        }
        if (_cashless_is_transfer)
        {
          _flag_buckets |= FlagBucketId.NR;
          _flag_buckets |= FlagBucketId.RE;
        }

        _virtual_account = 0;
        _te_iso_code = ReadGeneralParams("RegionalOptions", "CurrencyISOCode", Trx);
        using (SqlCommand _cmd = new SqlCommand("SELECT TE_ISO_CODE, TE_VIRTUAL_ACCOUNT_ID FROM TERMINALS WHERE TE_TERMINAL_ID = @pTerminalId", Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.NVarChar, 50).Value = TerminalId;
          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              Output.SetError(StartSessionStatus.Error, "Trx_UpdateAccountBalance Get terminal info failed");

              return false;
            }

            _te_iso_code = _reader.GetString(0);
            _virtual_account = _reader.IsDBNull(1) ? 0 : _reader.GetInt64(1);
          }
        }
        
        if (_tito_is_transfer)
        {
          _terminal_account_type = AccountType.ACCOUNT_VIRTUAL_TERMINAL;
          _account_id = 0;

          _sb = new StringBuilder();
          _sb.AppendLine("    SELECT   PS_ACCOUNT_ID                   ");
          _sb.AppendLine("           , AC_TYPE                         ");
          _sb.AppendLine("      FROM   PLAY_SESSIONS                   ");
          _sb.AppendLine(" LEFT JOIN   ACCOUNTS                        ");
          _sb.AppendLine("        ON   AC_ACCOUNT_ID  = PS_ACCOUNT_ID  ");
          _sb.AppendLine("     WHERE   PS_TERMINAL_ID = @pTerminalId   ");
          _sb.AppendLine("       AND   PS_STATUS      = @pStatus       ");

          using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _sql_command.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
            _sql_command.Parameters.Add("@pStatus", SqlDbType.Int).Value = PlaySessionStatus.Opened;

            using (SqlDataReader _sql_rd = _sql_command.ExecuteReader())
            {
              if (_sql_rd.Read())
              {
                // _account_id is the player account id
                _account_id = _sql_rd.IsDBNull(0) ? 0 : _sql_rd.GetInt64(0);
                _terminal_account_type = _sql_rd.IsDBNull(1) ? AccountType.ACCOUNT_UNKNOWN : (AccountType)_sql_rd.GetInt32(1);
              }
            }
          }
          //// TODO XCD Revise if for lcd or cashier
          //if (_terminal_account_type == AccountType.ACCOUNT_VIRTUAL_TERMINAL
          //  || _account_id == 0)
          //{
          //  // playsession not updated.
          //  Output.SetError(StartSessionStatus.AccountUnknown, "Promotional credit transfer: Playsession not updated: Need player account_id. Can't continue...");

          //  return false;
          //}
        }

        // Get account balance
        // 28-JAN-2016 RCI & JMV: Include NR Bucket in NR balance for LCD transfer
        if (!Trx_UpdateAccountBalance(_account_id, String.Empty, MultiPromos.AccountBalance.Zero, 0, false, _flag_buckets, out _acc_info, Trx))
        {
          Output.SetError(StartSessionStatus.Error, "Trx_UpdateAccountBalance LOCK account failed.");

          return false;
        }

        _ini_balance = _acc_info.FinalBalance;
        _ini_points = _acc_info.FinalPoints;
        _prev_play_session_id = _acc_info.CurrentPlaySessionId;
        _cancel_balance = _acc_info.CancellableBalance;

        // Calculate amount for the NR bucket account_promotion
        if (_cashless_is_transfer)
        {
          _bucket_nr_credit = Math.Max(0, _ini_balance.BucketNRCredit);
          _bucket_re_credit = Math.Max(0, _ini_balance.BucketRECredit);
        }
        else
        {
          _bucket_re_credit = 0;
          _bucket_nr_credit = Math.Max(0, BalanceToTransfer.PromoNotRedeemable - (_ini_balance.PromoNotRedeemable - _ini_balance.BucketNRCredit));
        }
        _bucket_nr_credit = Math.Truncate(_bucket_nr_credit * 100) / 100;
        _bucket_re_credit = Math.Truncate(_bucket_re_credit * 100) / 100;

        if (TransferMode == StartSessionTransferMode.Partial)
        {
          // Set ini_balance as desired to transfer
          _ini_balance.PromoNotRedeemable = Math.Min(_ini_balance.PromoNotRedeemable, BalanceToTransfer.PromoNotRedeemable);
          _ini_balance.PromoRedeemable = Math.Min(_ini_balance.PromoRedeemable, BalanceToTransfer.PromoRedeemable);
          _ini_balance.Redeemable = Math.Min(_ini_balance.Redeemable, BalanceToTransfer.Redeemable);
        }

        Int64 _account_id_for_get_ps;
        _account_id_for_get_ps = Account.AccountId;
        if (_mico2_is_transfer && _play_session_status != PlaySessionStatus.Opened)
        {
          _account_id_for_get_ps = _virtual_account;
        }

        // For TITO: AccountId is the virtual account id
        // For Cashless: AccountId is the player account id
        if (!Trx_GetPlaySessionId(_account_id_for_get_ps, TerminalId, TerminalType, _tito_mode, TransferMode, BalanceToTransfer, out _play_session_id, out _action, Trx))
        {
          Output.SetError(StartSessionStatus.Error, "Trx_GetPlaySessionId failed.");

          return false;
        }

        // On MICO2, on the transfer call, we need to unlink the virtual account, to properly link the 
        if (_mico2_is_transfer)
        {
          if (!Trx_UnlinkAccountTerminal(_virtual_account, TerminalId, _play_session_id, Trx))
          {
            Output.SetError(StartSessionStatus.Error, "Trx_UnlinkAccountTerminal failed.");

            return false;
          }

          if (!Trx_LinkAccountTerminal(Account.AccountId, TerminalId, _play_session_id, 
                                       (TransferMode == StartSessionTransferMode.Mico2 && _play_session_status == PlaySessionStatus.Opened) ? false : true, Trx))
          {
            Output.SetError(StartSessionStatus.Error, "Trx_LinkAccountTerminal failed.");

            return false;
          }
        }

        // Is a LCD transfer in Cashless or TITO.
        if (_is_transfer)
        {
#if !SQL_BUSINESS_LOGIC

          if (!Buckets.InsertPromotionToAccount(_acc_info, _bucket_nr_credit, _ini_balance.BucketNRCredit, Buckets.BucketId.Credit_NR, TerminalId, TerminalName, TerminalType, Trx))
          {
            Output.SetError(StartSessionStatus.Error, String.Format("Buckets.InsertPromotionToAccount failed. Account {0} NR Credit {1}.", _acc_info.AccountId, _bucket_nr_credit));

            return false;
          }

          if (!Buckets.InsertPromotionToAccount(_acc_info, _bucket_re_credit, _ini_balance.BucketRECredit, Buckets.BucketId.Credit_RE, TerminalId, TerminalName, TerminalType, Trx))
          {
            Output.SetError(StartSessionStatus.Error, String.Format("Buckets.InsertPromotionToAccount failed. Account {0} NR Credit {1}.", _acc_info.AccountId, _bucket_nr_credit));

            return false;
          }
#else
          Output.SetError(StartSessionStatus.Error, "LCD Transfer in 3GS not supported!!");

          return false;
#endif
        }

        if (_tito_is_transfer)
        {
          DataTable _active_promos;

          _is_new_session = false;

          // Compute Redeemable / NotRedeemable to be trasferred
          //if (!Trx_GetPlayableBalance(_account_id, TerminalId, _play_session_id, TransactionId, _ini_balance, !_mico2_enabled,
          //                            out _playable_balance, out _active_promos,  Trx))
          if (!Trx_GetPlayableBalance(_account_id, TerminalId, _play_session_id, TransactionId, _ini_balance, _tito_is_transfer,
                                      out _playable_balance, out _active_promos, Trx))
          {
            Output.SetError(StartSessionStatus.Error, "Trx_GetPlayableBalance failed.");

            return false;
          }

          if (_playable_balance.TotalBalance <= 0)
          {
            Output.SetError(StartSessionStatus.Error, "Without promotions to transfer: Balance ActivePromos = " + _playable_balance.TotalBalance.ToString() + ", BalanceToTransfer = " + BalanceToTransfer.TotalBalance.ToString() + ", TerminalId = " + TerminalId.ToString());

            return false;
          }

#if !SQL_BUSINESS_LOGIC
          if (_playable_balance.TotalBalance != BalanceToTransfer.TotalBalance && TransferMode != StartSessionTransferMode.All)
          {
            Log.Message("Balance from ActivePromos is different from BalanceToTransfer: Balance ActivePromos = " + _playable_balance.TotalBalance.ToString() + ", BalanceToTransfer = " + BalanceToTransfer.TotalBalance.ToString() + ", TerminalId = " + TerminalId.ToString());
          }
#endif

          if (!_mico2_enabled)
          {
            if (!Trx_TITO_UpdateAccountPromotionBalance(_account_id, _playable_balance, TerminalId, _active_promos, Trx))
            {
              Output.SetError(StartSessionStatus.Error, "Trx_TitoUpdateAccountPromotionBalance failed.");

              return false;
            }
          }

#if !SQL_BUSINESS_LOGIC
          //Set the terminal transfer status to TRANSFERRING
          if (!TerminalFundsTransfer.DB_UpdateTerminalFundsTransferStatus(TerminalId,
                                                                          TerminalFundsTransfer.TRANSFER_STATUS.TRANSFERRING,
                                                                          Trx))
          {
            Output.SetError(StartSessionStatus.Error, "DB_UpdateTerminalFundsTransferStatus failed.");

            return false;
          }
#endif
        }
        else
        {
          //Default StartCardsession

          _is_new_session = (_prev_play_session_id == 0);

          if (_is_new_session || _tito_mode)
          {
            Decimal _dummy1;
            Decimal _dummy2;

            if (!Trx_ResetAccountInSession(_account_id, 0, out _dummy1, out _dummy2, out _dummy_bal1, Trx))
            {
              Output.SetError(StartSessionStatus.Error, "Trx_ResetAccountInSession failed.");

              return false;
            }
          }

          // Compute Redeemable / NotRedeemable to be trasferred
          if (!Trx_GetPlayableBalance(_account_id, TerminalId, _play_session_id, TransactionId, _ini_balance, VendorId, out _playable_balance, Trx))
          {
            Output.SetError(StartSessionStatus.Error, "Trx_GetPlayableBalance failed.");

            return false;
          }
        }
        
        _playable_balance.PromoRedeemable = Math.Truncate(_playable_balance.PromoRedeemable * 100) / 100;
        _playable_balance.PromoNotRedeemable = Math.Truncate(_playable_balance.PromoNotRedeemable * 100) / 100;

        // Substract the amount to be transferred
        if (!Trx_UpdateAccountBalance(_account_id, AccountBalance.Negate(_playable_balance, _playable_balance.GetOnlyReserved),
                                      0, out _fin_balance, out _fin_points, out _prev_play_session_id, out _cancel_balance, Trx))
        {
          Output.SetError(StartSessionStatus.Error, "Trx_UpdateAccountBalance failed. PlayableBalance: " + _playable_balance.ToString());

          return false;
        }

        // Common Part
        // Create the StartCardSession movement
        _am_table = new AccountMovementsTable(TerminalId, TerminalName, _play_session_id);

        if (!_am_table.Add(0, _account_id, MovementType.StartCardSession,
                           _ini_balance.TotalBalance, _playable_balance.TotalBalance, 0, _fin_balance.TotalBalance))
        {
          Output.SetError(StartSessionStatus.Error, "AccountMovementsTable.Add(StartCardSession) failed.");

          return false;
        }

        if (!_am_table.Save(Trx))
        {
          Output.SetError(StartSessionStatus.Error, "AccountMovementsTable.Save failed.");

          return false;
        }

        // Account has activity
        if (!Trx_SetActivity(_account_id, Trx))
        {
          Output.SetError(StartSessionStatus.Error, "Trx_SetActivity failed.");

          return false;
        }

        String _currency_ISO_code;
        _currency_ISO_code = ReadGeneralParams("RegionalOptions", "CurrencyISOCode", Trx);

        if (!Trx_GetExchange(_playable_balance, _currency_ISO_code, _te_iso_code, out _play_session_balance, Trx))
        {
          Output.SetError(StartSessionStatus.Error, "PlaySessionId: " + _play_session_id.ToString() +
                                                    ", PlayableBalance: " + _playable_balance.ToString());

          return false;
        }

        if (!Trx_BalanceToPlaySession(_is_new_session, _play_session_id, _play_session_balance, _tito_mode, _mico2_enabled, _mico2_is_transfer, Trx))
        {
          Output.SetError(StartSessionStatus.Error, "Trx_BalanceToPlaySession failed: NewSession: " + _is_new_session.ToString() +
                          ", PlaySessionId: " + _play_session_id.ToString() + ", PlayableBalance: " + _playable_balance.ToString());

          return false;
        }

        _total_to_gm_balance = new AccountBalance();
        if (!_tito_mode)
        {
          if (!Trx_UpdateAccountInSession(_action, _account_id, TransactionId, _playable_balance,
                                          out _total_to_gm_balance, out _dummy_bal1, out _dummy_bal2, Trx))
          {
            Output.SetError(StartSessionStatus.Error, "Trx_UpdateAccountInSession failed. PlayableBalance: " + _playable_balance.ToString());

            return false;
          }
        }

        Output.PlaySessionId = _play_session_id;
        Output.IsNewPlaySession = _is_new_session;
        Output.IniBalance = _ini_balance;
        Output.PlayableBalance = _playable_balance;
        Output.TotalToGMBalance = _total_to_gm_balance;
        Output.FinBalance = _fin_balance;
        Output.Points = _fin_points;

        Output.SetSuccess();

        return true;
      }
      catch (Exception _ex)
      {
        Output.SetError(StartSessionStatus.Error, _ex.Message);
      }

      return false;
    } // Trx_OnStartCardSession


    public static Boolean Trx_GetExchange(Decimal Amount, String FromISO, String ToISO, out Decimal Exchanged, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      Exchanged = Amount;
      if (FromISO.Equals(ToISO))
      {
        return true;
      }

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("   SELECT     dbo.ApplyExchange2(  @pAmount,   ");
        _sb.AppendLine("                                   @pFromIso,  ");
        _sb.AppendLine("                                   @pToIso     ");
        _sb.AppendLine("              )  AS Exchanged                  ");



        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = Amount;
          _sql_cmd.Parameters.Add("@pFromIso", SqlDbType.NVarChar).Value = FromISO;
          _sql_cmd.Parameters.Add("@pToIso", SqlDbType.NVarChar).Value = ToISO;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              Exchanged = _reader.GetDecimal(_reader.GetOrdinal("Exchanged"));
              return true;
            }
          }
        }
      }
      catch (Exception)
      {
        return false;
      }
      return false;

    }

    // PURPOSE: 1) Block account.
    //          2) Retrieve the play session (if is necesarry insert new session).
    //          3) Compute Redeemable / NotRedeemable to be trasferred
    //          4) Substract the amount of the account to be transferred
    //          5) Create the StartCardSession movement
    //          6) Set activity
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - TerminalId:
    //           - TerminalType:
    //           - TerminalName:
    //           - TransferMode
    //           - BalanceToTransfer
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if all the process was correct, false otherwise.
    public static StartSessionOutput Trx_CommonStartCardSession(Int64 TransactionId,
                                                                Terminal.TerminalInfo Terminal,
                                                                AccountInfo Account,
                                                                Boolean TITOMode,
                                                                StartSessionTransferMode TransferMode,
                                                                AccountBalance BalanceToTransfer,
                                                                Boolean TransferWithBuckets,
                                                                SqlTransaction Trx)
    {
      return Trx_CommonStartCardSession(TransactionId,
                                        Terminal,
                                        Account,
                                        TITOMode,
                                        TransferMode,
                                        BalanceToTransfer,
                                        TransferWithBuckets,
                                        String.Empty,
                                        Trx);
    }

    public static Boolean Trx_GetExchange(MultiPromos.AccountBalance Amount, String FromISO, String ToISO, out MultiPromos.AccountBalance Exchanged, SqlTransaction SqlTrx)
    {
      Boolean _result;

      Exchanged = Amount;

      if (FromISO.Equals(ToISO))
      {
        return true;
      }

      _result = Trx_GetExchange(Amount.Redeemable, FromISO, ToISO, out Exchanged.Redeemable, SqlTrx);
      _result = _result && Trx_GetExchange(Amount.PromoRedeemable, FromISO, ToISO, out Exchanged.PromoRedeemable, SqlTrx);
      _result = _result && Trx_GetExchange(Amount.PromoNotRedeemable, FromISO, ToISO, out Exchanged.PromoNotRedeemable, SqlTrx);
      _result = _result && Trx_GetExchange(Amount.BucketNRCredit, FromISO, ToISO, out Exchanged.BucketNRCredit, SqlTrx);
      _result = _result && Trx_GetExchange(Amount.BucketRECredit, FromISO, ToISO, out Exchanged.BucketRECredit, SqlTrx);

      return _result;
    }


    //------------------------------------------------------------------------------
    // PURPOSE: Link the account with the play session and the terminal id.
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - TerminalId:
    //           - PlaySessionId:
    //           - EnsureNotInSession:
    //           - UseVirtualTerminal: in mode TITO, flag to indicates that AC_VIRTUAL_TERMINAL will filled
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if the update was correct , false otherwise.
    //
    public static Boolean Trx_LinkAccountTerminal(Int64 AccountId,
                                                  Int32 TerminalId,
                                                  Int64 PlaySessionId,
                                                  Boolean EnsureNotInSession,
                                                  SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE ACCOUNTS SET   AC_CURRENT_TERMINAL_ID     = @pTerminalId ");
        _sb.AppendLine("                    , AC_CURRENT_TERMINAL_NAME   = (SELECT TE_NAME FROM TERMINALS WHERE TE_TERMINAL_ID = @pTerminalId) ");
        _sb.AppendLine("                    , AC_CURRENT_PLAY_SESSION_ID = @pPlaySessionId ");
        _sb.AppendLine("              WHERE   AC_ACCOUNT_ID              = @pAccountId ");

        if (EnsureNotInSession)
        {
          _sb.AppendLine("                AND   AC_CURRENT_TERMINAL_ID     IS NULL ");
          _sb.AppendLine("                AND   AC_CURRENT_TERMINAL_NAME   IS NULL ");
          _sb.AppendLine("                AND   AC_CURRENT_PLAY_SESSION_ID IS NULL ");
        }

        _sb.AppendLine(" ; ");

        _sb.AppendLine("UPDATE TERMINALS SET  TE_CURRENT_ACCOUNT_ID      = @pAccountId ");
        _sb.AppendLine("                    , TE_CURRENT_PLAY_SESSION_ID = @pPlaySessionId ");
        _sb.AppendLine("              WHERE   TE_TERMINAL_ID             = @pTerminalId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

          // ExecuteNonQuery() returns the number of rows affected. If everything ok, it will be 2.
          return (_cmd.ExecuteNonQuery() == 2);
        }
      }
      catch
      { ; }

      return false;
    } // Trx_LinkAccountTerminal

    //------------------------------------------------------------------------------
    // PURPOSE: Unlink the account with the play session and the terminal id.
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - TerminalId:
    //           - PlaySessionId:
    //           - UseVirtualTerminal: in mode TITO, flag to indicates that AC_VIRTUAL_TERMINAL will filled
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if the update was correct , false otherwise.

    public static Boolean Trx_UnlinkAccountTerminal(Int64 AccountId,
                                                    Int32 TerminalId,
                                                    Int64 PlaySessionId,
                                                    SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE ACCOUNTS SET   AC_LAST_TERMINAL_ID        = AC_CURRENT_TERMINAL_ID ");
        _sb.AppendLine("                     , AC_LAST_TERMINAL_NAME      = AC_CURRENT_TERMINAL_NAME ");
        _sb.AppendLine("                     , AC_LAST_PLAY_SESSION_ID    = AC_CURRENT_PLAY_SESSION_ID ");
        _sb.AppendLine("                     , AC_CURRENT_TERMINAL_ID     = NULL ");
        _sb.AppendLine("                     , AC_CURRENT_TERMINAL_NAME   = NULL ");
        _sb.AppendLine("                     , AC_CURRENT_PLAY_SESSION_ID = NULL ");
        _sb.AppendLine("               WHERE   AC_ACCOUNT_ID              = @pAccountId ");
        _sb.AppendLine("                 AND   AC_CURRENT_TERMINAL_ID     = @pTerminalId ");
        _sb.AppendLine("                 AND   AC_CURRENT_PLAY_SESSION_ID = @pPlaySessionId ");

        _sb.AppendLine(" ; ");

        _sb.AppendLine(" UPDATE TERMINALS SET  TE_CURRENT_ACCOUNT_ID      = NULL ");
        _sb.AppendLine("                     , TE_CURRENT_PLAY_SESSION_ID = NULL ");
        _sb.AppendLine("               WHERE   TE_TERMINAL_ID             = @pTerminalId ");
        _sb.AppendLine("                 AND   TE_CURRENT_ACCOUNT_ID      = @pAccountId ");
        _sb.AppendLine("                 AND   TE_CURRENT_PLAY_SESSION_ID = @pPlaySessionId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

          _cmd.ExecuteNonQuery();
          // It doesn't matter if updated or not.

          return true;
        }
      }
      catch
      { ; }

      return false;
    } // Trx_UnlinkAccountTerminal


    public static Boolean Trx_CheckTerminalLink(Int64 AccountId,
                                                Int32 TerminalId,
                                                SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _num_rows;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   Count(*)                                                    ");
        _sb.AppendLine("   FROM   ACCOUNTS                                                    ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId                                 ");
        _sb.AppendLine("    AND   AC_CURRENT_TERMINAL_ID     IS NOT NULL                      ");
        _sb.AppendLine("    AND   AC_CURRENT_PLAY_SESSION_ID IS NOT NULL                      ");
        _sb.AppendLine("    AND   AC_ACCOUNT_ID <> ( SELECT   TE_VIRTUAL_ACCOUNT_ID           ");
        _sb.AppendLine("                               FROM   TERMINALS                       ");
        _sb.AppendLine("                              WHERE   TE_TERMINAL_ID = @pTerminalId ) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          _num_rows = (Int32)_cmd.ExecuteScalar();

          if (_num_rows == 0)
          {
            return true;
          }
        }

#if !SQL_BUSINESS_LOGIC
        // Check there is only one account linked to the terminal.  If there are more than one, unlink them.
        Int32 _rows_affected;

        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   Count(*)                              ");
        _sb.AppendLine("   FROM   ACCOUNTS                              ");
        _sb.AppendLine("  WHERE   AC_CURRENT_TERMINAL_ID = @pTerminalId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          _num_rows = (Int32)_cmd.ExecuteScalar();

          if (_num_rows > 1)
          {
            // There are more than one account linked to the terminal.  Unlink them!
            using (DB_TRX _db_trx = new DB_TRX())
            {
              _sb = new StringBuilder();
              _sb.AppendLine(" UPDATE ACCOUNTS SET   AC_LAST_TERMINAL_ID        = AC_CURRENT_TERMINAL_ID    ");
              _sb.AppendLine("                     , AC_LAST_TERMINAL_NAME      = AC_CURRENT_TERMINAL_NAME  ");
              _sb.AppendLine("                     , AC_LAST_PLAY_SESSION_ID    = AC_CURRENT_PLAY_SESSION_ID");
              _sb.AppendLine("                     , AC_CURRENT_TERMINAL_ID     = NULL                      ");
              _sb.AppendLine("                     , AC_CURRENT_TERMINAL_NAME   = NULL                      ");
              _sb.AppendLine("                     , AC_CURRENT_PLAY_SESSION_ID = NULL                      ");
              _sb.AppendLine("               WHERE   AC_CURRENT_TERMINAL_ID     = @pTerminalId              ");

              _sb.AppendLine(" ;                                                                            ");

              _sb.AppendLine(" UPDATE TERMINALS SET  TE_CURRENT_ACCOUNT_ID      = NULL                      ");
              _sb.AppendLine("                     , TE_CURRENT_PLAY_SESSION_ID = NULL                      ");
              _sb.AppendLine("               WHERE   TE_TERMINAL_ID             = @pTerminalId              ");

              using (SqlCommand _cmd_update = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
              {
                _cmd_update.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

                _rows_affected = _cmd_update.ExecuteNonQuery();

                Log.Message(String.Format("Mico2 -> Clean linked accounts to the terminal {0}.  Rows Affected: {1}", TerminalId, _rows_affected));

                _db_trx.Commit();
              }
            }
          }
        }
#endif
      }
#if !SQL_BUSINESS_LOGIC
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
#else
      catch
      { ; }
#endif

      return false;

    } // Trx_CheckTerminalLink

    // PURPOSE: Get a new session play session or the current play session of the account.
    //          * Link the play session id with the account if is necessary.
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - TerminalId:
    //           - TerminalType:
    //           - PlaySessionId:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if all the process was correct, false otherwise.

    public static Boolean Trx_GetPlaySessionId(Int64 AccountId,
                                               Int32 TerminalId,
                                               TerminalTypes TerminalType,
                                               Boolean TITOMode,
                                               StartSessionTransferMode TransferMode,
                                               AccountBalance BalanceToTransfer,
                                               out Int64 PlaySessionId,
                                               out InSessionAction Action,
                                               SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _current_terminal_id;
      Int64 _current_play_session_id;
      AccountType _ac_type;
      PlaySessionStatus _ps_status;
      String _xml_type_data;
      String _card_track_data;
      Boolean _rc;
      Boolean _balance_mismatch;

      PlaySessionStatus _old_status;
      PlaySessionStatus _new_status;
      TimeSpan _play_timespan;

      Int32 _ps_won_count;
      Int32 _ps_played_count;
      Decimal _ps_re_ticket_in;
      Decimal _ps_promo_re_ticket_in;
      Decimal _ps_promo_nr_ticket_in;
      Decimal _ps_re_ticket_out;
      Decimal _ps_promo_nr_ticket_out;

      PlaySessionId = 0;
      Action = InSessionAction.Unknown;

      _current_terminal_id = 0;
      _current_play_session_id = 0;
      _ac_type = 0;
      _ps_status = 0;
      _xml_type_data = "";

      _ps_won_count = 0;
      _ps_played_count = 0;
      _ps_re_ticket_in = 0;
      _ps_promo_re_ticket_in = 0;
      _ps_promo_nr_ticket_in = 0;
      _ps_re_ticket_out = 0;
      _ps_promo_nr_ticket_out = 0;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" SELECT   AC_CURRENT_TERMINAL_ID ");
        _sb.AppendLine("        , AC_CURRENT_PLAY_SESSION_ID ");
        _sb.AppendLine("        , AC_TYPE ");
        _sb.AppendLine("        , PS_STATUS ");
        _sb.AppendLine("        , AC_TRACK_DATA ");
        _sb.AppendLine("        , PS_BALANCE_MISMATCH ");
        _sb.AppendLine("        , ISNULL(PS_WON_COUNT,0) ");
        _sb.AppendLine("        , ISNULL(PS_PLAYED_COUNT,0) ");
        _sb.AppendLine("        , ISNULL(PS_RE_TICKET_IN,0) ");
        _sb.AppendLine("        , ISNULL(PS_PROMO_RE_TICKET_IN,0) ");
        _sb.AppendLine("        , ISNULL(PS_PROMO_NR_TICKET_IN,0) ");
        _sb.AppendLine("        , ISNULL(PS_RE_TICKET_OUT,0) ");
        _sb.AppendLine("        , ISNULL(PS_PROMO_NR_TICKET_OUT,0) ");
        _sb.AppendLine("   FROM   ACCOUNTS LEFT JOIN PLAY_SESSIONS ON AC_CURRENT_PLAY_SESSION_ID = PS_PLAY_SESSION_ID ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            _current_terminal_id = _reader.IsDBNull(0) ? 0 : (Int32)_reader.GetInt32(0);
            _current_play_session_id = _reader.IsDBNull(1) ? 0 : (Int64)_reader.GetInt64(1);
            _ac_type = _reader.IsDBNull(2) ? AccountType.ACCOUNT_UNKNOWN : (AccountType)_reader.GetInt32(2);
            _ps_status = _reader.IsDBNull(3) ? PlaySessionStatus.Closed : (PlaySessionStatus)_reader.GetInt32(3);
            if (!TITOMode)
            {
              if (!_reader.IsDBNull(4))
              {
                _rc = CardNumber.TrackDataToExternal(out _card_track_data, _reader.GetString(4), 0);
                if (_rc)
                {
                  _xml_type_data = _card_track_data;
                }
              }
            }
            _balance_mismatch = _reader.IsDBNull(5) ? false : _reader.GetBoolean(5);

            _ps_won_count = _reader.GetInt32(6);
            _ps_played_count = _reader.GetInt32(7);
            _ps_re_ticket_in = _reader.GetDecimal(8);
            _ps_promo_re_ticket_in = _reader.GetDecimal(9);
            _ps_promo_nr_ticket_in = _reader.GetDecimal(10);
            _ps_re_ticket_out = _reader.GetDecimal(11);
            _ps_promo_nr_ticket_out = _reader.GetDecimal(12);
          }
        }

        //TransferMode
        //  Default = 0,
        //  Partial = 1,
        //  All = 2,
        //  IncludeReserved = 3,// 04-NOV-2015  JCA Product Backlog Item 6145: BonoPLUS: Reserved Credit
        if (TITOMode && !((Int32)TransferMode > 0))
        {
          if ((_ps_won_count > 0)
            || (_ps_played_count > 0)
            || (_ps_re_ticket_in > 0)
            || (_ps_promo_re_ticket_in > 0)
            || (_ps_promo_nr_ticket_in > 0)
            || (_ps_re_ticket_out > 0)
            || (_ps_promo_nr_ticket_out > 0))
          {
            if (!Trx_PlaySessionChangeStatus(PlaySessionStatus.Abandoned, _current_play_session_id, out _old_status, out _new_status, out _play_timespan, Trx))
            {
              return false;
            }
            if (_old_status == PlaySessionStatus.Opened && _new_status != PlaySessionStatus.Opened)
            {
              // Llenar la tabla  MS_PENDING_GAME_PLAY_SESSIONS.
              if (!Trx_InsertAgroupGamePlaySession(_current_play_session_id, Trx))
              {
                return false;
              }
            }


            if (!Trx_InsertPlaySession(AccountId, _ac_type, _xml_type_data, TerminalId, TerminalType, out _current_play_session_id, Trx))
            {
              return false;
            }

            if (!Trx_LinkAccountTerminal(AccountId, TerminalId, _current_play_session_id, false, Trx))
            {
              return false;
            }

            Action = InSessionAction.StartSession;
            PlaySessionId = _current_play_session_id;

            return true;
          }
        }

        // If doesn't have current play session in the account, it insert a new play session
        // On TITO OR On MICO2, insert a new play session when we got a closed one
        if (_current_play_session_id == 0
          || ((TITOMode || ReadGeneralParams("Site", "SystemMode", Trx).Equals("4")) && _ps_status == PlaySessionStatus.Closed))
        {
          if (!Trx_InsertPlaySession(AccountId, _ac_type, _xml_type_data, TerminalId, TerminalType, out _current_play_session_id, Trx))
          {
            return false;
          }

          if (!Trx_LinkAccountTerminal(AccountId, TerminalId, _current_play_session_id, false, Trx))
          {
            return false;
          }

          Action = InSessionAction.StartSession;
          PlaySessionId = _current_play_session_id;

          return true;
        }

        // In Session
        if (TerminalId != _current_terminal_id
            || _ps_status != PlaySessionStatus.Opened
            || TerminalType != TerminalTypes.SAS_HOST
            || _balance_mismatch)
        {
          return false;
        }

        // This is a SAS-HOST 'asking more money'
        Action = InSessionAction.ReStartSession;
        PlaySessionId = _current_play_session_id;

        return true;
      }
      catch
      { ; }

      return false;

    } // Trx_GetPlaySessionId

    // PURPOSE: 1) Update state of all the sessions of the terminal to abandoned.
    //          2) Insert the new play session an return the id.
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - AccountType:
    //           - TypeData:
    //           - TerminalId:
    //           - PlaySessionId:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if all updated was successfully , false otherwise.

    public static Boolean Trx_InsertPlaySession(Int64 AccountId,
                                                AccountType AccountType,
                                                String TypeData,
                                                Int32 TerminalId,
                                                TerminalTypes TerminalType,
                                                out Int64 PlaySessionId,
                                                SqlTransaction Trx)
    {
      PlaySessionType _ps_type;
      StringBuilder _sb;
      SqlParameter _param_play_sesion_id;

      PlaySessionId = 0;

      _sb = new StringBuilder();
      _ps_type = PlaySessionType.NONE;

      switch (AccountType)
      {
        case AccountType.ACCOUNT_CADILLAC_JACK:
          _ps_type = PlaySessionType.CADILLAC_JACK;
          break;
        case AccountType.ACCOUNT_VIRTUAL_TERMINAL:
        case AccountType.ACCOUNT_WIN:
          _ps_type = PlaySessionType.WIN;
          break;
        case AccountType.ACCOUNT_ALESIS:
          _ps_type = PlaySessionType.ALESIS;
          break;
        default:
          _ps_type = PlaySessionType.NONE;
          break;
      }

      try
      {
        _sb.Length = 0;
        _sb.AppendLine(" DECLARE @ActivityDueToCardIn AS INTEGER ");
        _sb.AppendLine(" ");
        _sb.AppendLine(" SELECT   @ActivityDueToCardIn = CAST (GP_KEY_VALUE AS INT) ");
        _sb.AppendLine("   FROM   GENERAL_PARAMS                                    ");
        _sb.AppendLine("  WHERE   GP_GROUP_KEY   = 'Play'                           ");
        _sb.AppendLine("    AND   GP_SUBJECT_KEY = 'ActivityDueToCardIn'            ");
        _sb.AppendLine(" ");
        _sb.AppendLine(" IF ( @ActivityDueToCardIn IS NULL ) ");
        _sb.AppendLine(" BEGIN                               ");
        _sb.AppendLine("   SET @ActivityDueToCardIn = 0      ");
        _sb.AppendLine(" END                                 ");
        _sb.AppendLine(" ");
        _sb.AppendLine(" INSERT INTO PLAY_SESSIONS (PS_ACCOUNT_ID       ");
        _sb.AppendLine("                          , PS_TERMINAL_ID      ");
        _sb.AppendLine("                          , PS_TYPE             ");
        _sb.AppendLine("                          , PS_TYPE_DATA        ");
        _sb.AppendLine("                          , PS_INITIAL_BALANCE  ");
        _sb.AppendLine("                          , PS_FINAL_BALANCE    ");
        _sb.AppendLine("                          , PS_FINISHED         ");
        _sb.AppendLine("                          , PS_STAND_ALONE     )");
        _sb.AppendLine("                    VALUES (@pAccountId         ");
        _sb.AppendLine("                          , @pTerminalId        ");
        _sb.AppendLine("                          , @pTypeWin           ");
        _sb.AppendLine("                          , @pTypeData          ");
        _sb.AppendLine("                          , @pDefaultBalance    ");
        _sb.AppendLine("                          , @pDefaultBalance    ");
        _sb.AppendLine("                          , CASE WHEN (@ActivityDueToCardIn = 1) THEN GETDATE() ELSE NULL END ");
        _sb.AppendLine("                          , @pStandAlone        )");
        _sb.AppendLine(" ");
        _sb.AppendLine(" SET @pPlaySessionId = SCOPE_IDENTITY() ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pTerminalId ", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pTypeWin", SqlDbType.Int).Value = _ps_type;
          _cmd.Parameters.Add("@pDefaultBalance", SqlDbType.Money).Value = 0;
          _cmd.Parameters.Add("@pTypeData", SqlDbType.Xml).Value = String.IsNullOrEmpty(TypeData) ? (Object)DBNull.Value : (Object)TypeData;
          _cmd.Parameters.Add("@pStandAlone", SqlDbType.Bit).Value = (TerminalType == TerminalTypes.SAS_HOST);
          _param_play_sesion_id = _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt);
          _param_play_sesion_id.Direction = ParameterDirection.Output;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }

          if (_param_play_sesion_id.Value == DBNull.Value)
          {
            return false;
          }

          PlaySessionId = (Int64)_param_play_sesion_id.Value;

        }

        return true;

      }
      catch
      { ; }

      return false;

    } // Trx_InsertPlaySession


    public static Boolean Trx_UpdatePsAccountId(Int64 PlaySessionId, Int64 AccountId, SqlTransaction Trx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand("UPDATE PLAY_SESSIONS SET PS_ACCOUNT_ID = @pAccountId WHERE PS_PLAY_SESSION_ID = @pPlaySessionId", Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pPlaySessionId ", SqlDbType.Int).Value = PlaySessionId;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
        }

        return true;

      }
      catch
      { ; }

      return false;
    } // Trx_UpdatePsAccountID

    public static Boolean Trx_HandpayInsertPlaySession(HANDPAY_TYPE Type,
                                                       Int64 AccountId,
                                                       Int32 TerminalId,
                                                       TerminalTypes TerminalType,
                                                       AccountBalance PlaySessionInitialBalance,
                                                       AccountBalance HandpayAmount,
                                                       out Int64 PlaySessionId,
                                                       SqlTransaction Trx)
    {
      StringBuilder _sb;
      SqlParameter _param_play_session;
      PlaySessionType _ps_type;
      PlaySessionStatus _ps_status;
      Decimal _won_amount;

      PlaySessionId = -1;
      _sb = new StringBuilder();

      if (HandpayAmount.TotalBalance > 0)
      {
        _ps_status = PlaySessionStatus.HandpayPayment;
      }
      else
      {
        _ps_status = PlaySessionStatus.HandpayCancelPayment;
      }

      _won_amount = 0;
      switch (Type)
      {
        case HANDPAY_TYPE.CANCELLED_CREDITS:
          _ps_type = PlaySessionType.HANDPAY_CANCELLED_CREDITS;
          break;

        case HANDPAY_TYPE.JACKPOT:
          _ps_type = PlaySessionType.HANDPAY_JACKPOT;
          _won_amount = HandpayAmount.TotalBalance;
          break;

        case HANDPAY_TYPE.ORPHAN_CREDITS:
          _ps_type = PlaySessionType.HANDPAY_ORPHAN_CREDITS;
          break;

        case HANDPAY_TYPE.MANUAL_CANCELLED_CREDITS:
        case HANDPAY_TYPE.MANUAL_JACKPOT:
        case HANDPAY_TYPE.MANUAL_OTHERS:
        case HANDPAY_TYPE.SPECIAL_PROGRESSIVE:
        case HANDPAY_TYPE.MANUAL:
          _ps_type = PlaySessionType.HANDPAY_MANUAL;
          break;

        case HANDPAY_TYPE.SITE_JACKPOT:
          _ps_type = PlaySessionType.HANDPAY_SITE_JACKPOT;
          _won_amount = HandpayAmount.TotalBalance;
          break;

        case HANDPAY_TYPE.UNKNOWN:
        default:
          return false;
      }

      try
      {
        _sb.AppendLine("INSERT INTO PLAY_SESSIONS ( PS_ACCOUNT_ID ");
        _sb.AppendLine("                          , PS_TERMINAL_ID ");
        _sb.AppendLine("                          , PS_TYPE ");
        _sb.AppendLine("                          , PS_INITIAL_BALANCE ");
        _sb.AppendLine("                          , PS_FINAL_BALANCE ");
        _sb.AppendLine("                          , PS_PLAYED_COUNT ");
        _sb.AppendLine("                          , PS_PLAYED_AMOUNT ");
        _sb.AppendLine("                          , PS_WON_COUNT ");
        _sb.AppendLine("                          , PS_WON_AMOUNT ");
        _sb.AppendLine("                          , PS_CASH_IN ");
        _sb.AppendLine("                          , PS_CASH_OUT ");
        _sb.AppendLine("                          , PS_STATUS ");
        _sb.AppendLine("                          , PS_STARTED ");
        _sb.AppendLine("                          , PS_FINISHED ");
        _sb.AppendLine("                          , PS_STAND_ALONE ");
        _sb.AppendLine("                          , PS_PROMO ");
        _sb.AppendLine("                          ) ");
        _sb.AppendLine("                   VALUES ( @pAccountId ");      // PS_ACCOUNT_ID
        _sb.AppendLine("                          , @pTerminalId ");     // PS_TERMINAL_ID
        _sb.AppendLine("                          , @pType ");           // PS_TYPE
        _sb.AppendLine("                          , @pInitialBalance "); // PS_INITIAL_BALANCE
        _sb.AppendLine("                          , @pFinalBalance ");   // PS_FINAL_BALANCE
        _sb.AppendLine("                          , 0 ");                // PS_PLAYED_COUNT
        _sb.AppendLine("                          , 0 ");                // PS_PLAYED_AMOUNT
        _sb.AppendLine("                          , 0 ");                // PS_WON_COUNT
        _sb.AppendLine("                          , @pWonAmount ");      // PS_WON_AMOUNT
        _sb.AppendLine("                          , 0 ");                // PS_CASH_IN
        _sb.AppendLine("                          , 0 ");                // PS_CASH_OUT
        _sb.AppendLine("                          , @pStatus ");   // PS_STATUS
        _sb.AppendLine("                          , GETDATE() ");        // PS_STARTED
        _sb.AppendLine("                          , GETDATE() ");        // PS_FINISHED
        _sb.AppendLine("                          , @pStandAlone ");     // PS_STAND_ALONE
        _sb.AppendLine("                          , 0 ");                // PS_PROMO
        _sb.AppendLine("                          ) ");
        _sb.AppendLine("                           SET @pPlaySessionId = SCOPE_IDENTITY() ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pType", SqlDbType.Int).Value = _ps_type;
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = _ps_status;
          _cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).Value = (Decimal)PlaySessionInitialBalance.TotalBalance;
          _cmd.Parameters.Add("@pWonAmount", SqlDbType.Money).Value = _won_amount;
          _cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).Value = (Decimal)(PlaySessionInitialBalance.TotalBalance + HandpayAmount.TotalBalance);
          _cmd.Parameters.Add("@pStandAlone", SqlDbType.Bit).Value = (TerminalType == TerminalTypes.SAS_HOST);
          _param_play_session = _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt);
          _param_play_session.Direction = ParameterDirection.Output;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }

          PlaySessionId = (Int64)_param_play_session.Value;

        }

        return true;

      }
      catch
      {
        return false;
      }

    } // Trx_HandpayInsertPlaySession

    //------------------------------------------------------------------------------
    // PURPOSE : Update Card Session Counters received to the DB.
    //
    //  PARAMS :
    //      - INPUT :
    //          - CardSessionId
    //          - PlayedCount
    //          - PlayedAmount
    //          - WonCount
    //          - WonAmount
    //          - JackpotAmount
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - true: Card session counters updated.
    //      - false: error.
    //
    public static Boolean Trx_UpdatePlaySessionPlayedWon(TerminalTypes TerminalType, Int64 PlaySessionId, PlayedWonMeters PlaySessionMeters,
                                                         out PlayedWonMeters Delta,
                                                         SqlTransaction Trx)
    {
      StringBuilder _sb;

      Delta = new PlayedWonMeters();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   PLAY_SESSIONS ");

        if (TerminalType == TerminalTypes.T3GS)
        {
          _sb.AppendLine("   SET   PS_PLAYED_COUNT    = CASE WHEN (@pPlayedCount  > PS_PLAYED_COUNT)  THEN @pPlayedCount  ELSE PS_PLAYED_COUNT  END ");
          _sb.AppendLine("       , PS_PLAYED_AMOUNT   = @pPlayedAmount  ");
          _sb.AppendLine("       , PS_WON_COUNT       = CASE WHEN (@pWonCount     > PS_WON_COUNT)     THEN @pWonCount     ELSE PS_WON_COUNT     END ");
          _sb.AppendLine("       , PS_WON_AMOUNT      = @pWonAmount     ");
          _sb.AppendLine("       , ps_cash_in      = CASE WHEN (@pBillsIn     > isnull(ps_cash_in,0))     THEN @pBillsIn     ELSE isnull(ps_cash_in,0)     END ");
        }
        else
        {
          _sb.AppendLine("   SET   PS_PLAYED_COUNT    = CASE WHEN (@pPlayedCount  > PS_PLAYED_COUNT)  THEN @pPlayedCount  ELSE PS_PLAYED_COUNT  END ");
          _sb.AppendLine("       , PS_PLAYED_AMOUNT   = CASE WHEN (@pPlayedAmount > PS_PLAYED_AMOUNT) THEN @pPlayedAmount ELSE PS_PLAYED_AMOUNT END ");
          _sb.AppendLine("       , PS_WON_COUNT       = CASE WHEN (@pWonCount     > PS_WON_COUNT)     THEN @pWonCount     ELSE PS_WON_COUNT     END ");
          _sb.AppendLine("       , PS_WON_AMOUNT      = CASE WHEN (@pWonAmount    > PS_WON_AMOUNT)    THEN @pWonAmount    ELSE PS_WON_AMOUNT    END ");
        }

        _sb.AppendLine("       , PS_LOCKED          = NULL ");
        _sb.AppendLine("       , PS_FINISHED        = GETDATE() ");
        _sb.AppendLine("OUTPUT   INSERTED.PS_PLAYED_COUNT  - DELETED.PS_PLAYED_COUNT  ");
        _sb.AppendLine("       , INSERTED.PS_PLAYED_AMOUNT - DELETED.PS_PLAYED_AMOUNT ");
        _sb.AppendLine("       , INSERTED.PS_WON_COUNT     - DELETED.PS_WON_COUNT     ");
        _sb.AppendLine("       , INSERTED.PS_WON_AMOUNT    - DELETED.PS_WON_AMOUNT    ");
        if (TerminalType == TerminalTypes.T3GS)
        {
          _sb.AppendLine("       , isnull(INSERTED.ps_cash_in,0)    - isnull(DELETED.ps_cash_in,0)    ");
        }
        _sb.AppendLine(" WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;
          _cmd.Parameters.Add("@pPlayedCount", SqlDbType.BigInt).Value = Math.Max(0, PlaySessionMeters.PlayedCount);
          _cmd.Parameters.Add("@pPlayedAmount", SqlDbType.Money).Value = Math.Max(0, PlaySessionMeters.PlayedAmount);
          _cmd.Parameters.Add("@pWonCount", SqlDbType.BigInt).Value = Math.Max(0, PlaySessionMeters.WonCount);
          _cmd.Parameters.Add("@pWonAmount", SqlDbType.Money).Value = Math.Max(0, PlaySessionMeters.WonAmount);
          if (TerminalType == TerminalTypes.T3GS)
          {
            _cmd.Parameters.Add("@pBillsIn", SqlDbType.Money).Value = Math.Max(0, PlaySessionMeters.BillInAmount);
          }
          // AJQ & ACC & XI 25-AUG-2010
          // JackpotAmount will be added when the handpay is paid.

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }
            Delta.PlayedCount = _reader.GetInt32(0);
            Delta.PlayedAmount = _reader.GetDecimal(1);
            Delta.WonCount = _reader.GetInt32(2);
            Delta.WonAmount = _reader.GetDecimal(3);
            Delta.JackpotAmount = 0;
            if (TerminalType == TerminalTypes.T3GS)
            {
              Delta.BillInAmount = _reader.GetDecimal(4);
            }
          }

          return true;
        }
      }
      catch
      {
      }

      return false;
    } // Trx_UpdatePlaySessionPlayedWon

    public static Boolean Trx_OnEndCardSession(Int32 TerminalId,
                                               TerminalTypes TerminalType,
                                               String TerminalName,
                                               MultiPromos.EndSessionInput Input,
                                               out MultiPromos.EndSessionOutput Output,
                                               SqlTransaction Trx)
    {
      return Trx_OnEndCardSession(TerminalId, TerminalType, TerminalName, Input, String.Empty, out  Output, Trx);
    }

    public static Boolean Trx_OnEndCardSession(Int32 TerminalId,
                                               TerminalTypes TerminalType,
                                               String TerminalName,
                                               MultiPromos.EndSessionInput Input,
                                               String VendorId,
                                               out MultiPromos.EndSessionOutput Output,
                                               SqlTransaction Trx)
    {
      AccountBalance _balance_from_gm_calculated;
      AccountBalance _balance_from_gm_to_add;
      Boolean _big_mismatch;
      Boolean _mismatch;
      PlaySessionCloseParameters _psc_params;
      Decimal _balance_from_gm;
      InSessionParameters _params;
      InSessionParameters _final_params;
      Boolean _is_tito;
      Boolean _is_mico2;
      String _fbm_vendorid;
      Int32 _close_session_to_zero;
      Boolean _delta_not_computed;

      Output = new EndSessionOutput();
      Output.SetError(EndSessionStatus.ErrorRetry, "");

      _is_tito = Input.IsTITO;
      _is_mico2 = Input.IsMico2;

      _delta_not_computed = false;

      // If SystemMode is Tito,  _is_tito must be true
      if (_is_mico2 && Input.AccountId != 0 && !ReadGeneralParams("Site", "SystemMode", Trx).Equals("1"))
      {
        // WHEN MICO2, CASHSLESS BY DEFAULT
        _is_tito = false;
      }

      try
      {
        if (Input.PlaySessionId <= 0 || Input.TransactionId < 0)
        {
          Output.SetError(EndSessionStatus.FatalError, "Unexpected PlaySessionId/TransactionId: " + Input.PlaySessionId.ToString() + ", " + Input.TransactionId.ToString());

          return false;
        }

        if (_is_tito)
        {
          // MPO 02-JAN-2014
          // In TITO mode: initial_balance - final_balance == played - won
          // Is not necessary check the negative.
          _balance_from_gm = Input.BalanceFromGM;
        }
        else
        {
          _balance_from_gm = Math.Max(0, Input.BalanceFromGM);
        }

        if (_balance_from_gm != Input.BalanceFromGM)
        {
          Output.Warning("BalanceFromGM changed from " + Input.BalanceFromGM.ToString() + ", to " + _balance_from_gm.ToString());
        }

        switch (TerminalType)
        {
          case TerminalTypes.WIN:       // LKT
            break;

          case TerminalTypes.SAS_HOST:  // SAS-HOST
            if (Input.TransactionId == 0 || !Input.HasMeters)
            {
              if (Input.MovementType != MovementType.ManualEndSession)
              {
                Output.Warning("Session Meters not received. PlaySessionId: " + Input.PlaySessionId.ToString() + " TransactionId: " + Input.TransactionId.ToString() + ", HasCounters: " + Input.HasMeters.ToString());
              }
            }
            break;

          case TerminalTypes.T3GS:      // 3GS
            break;

          default:
            Output.SetError(EndSessionStatus.FatalError, "Unexpected TerminalType: " + TerminalType.ToString());

            return false;
        }


        if (Input.MovementType == MovementType.EndCardSession)
        {
          PlayedWonMeters _dummy;
          if (!Trx_UpdatePlaySessionPlayedWon(TerminalType, Input.PlaySessionId, Input.Meters, out _dummy, Trx))
          {
            Output.SetError(EndSessionStatus.ErrorRetry, "Trx_UpdatePlaySessionPlayedWon failed.");

            return false;
          }
        }

        Trx.Save("CalculatePlayedAndWon");

        if (_is_tito)
        {

          _params = TITO_GetDeltaPlayedWon(Input);

        }
        else
        {
          if (TerminalType == TerminalTypes.T3GS)
          {
            if (!Trx_GetDeltaPlayedWon3GS(Input.PlaySessionId, out _params, Trx))
            {
              Output.SetError(EndSessionStatus.ErrorRetry, "Trx_GetDeltaPlayedWon failed.");

              return false;
            }
          }
          else
          {
            if (!Trx_GetDeltaPlayedWon(Input.PlaySessionId, out _params, Trx))
            {
              Output.SetError(EndSessionStatus.ErrorRetry, "Trx_GetDeltaPlayedWon failed.");

              return false;
            }
          }

          if (_is_mico2)
          {
            _params.IsMico2 = true;

            // ACC 28-JUN-2018 Add to Initial Balance Bills+Coins+Tickets
            _params.Balance.Redeemable += (Input.TitoSessionMeters.RedeemableCashIn);
            _params.Balance.PromoRedeemable += (Input.TitoSessionMeters.PromoReCashIn);
            _params.Balance.PromoNotRedeemable += (Input.TitoSessionMeters.PromoNrCashIn);

            _params.BalanceToAccount = Input.BalanceToAccount;

            _params.DeltaPlayed = Input.Meters.PlayedAmount;
            //_params.DeltaPlayedRe = Input.Meters.PlayedReedemable + Input.Meters.PlayedNonRestrictedAmount;
            //_params.DeltaPlayedNr = Input.Meters.PlayedRestrictedAmount;

            _params.DeltaWon = Input.Meters.WonAmount;
            //_params.DeltaWonRe = Input.Meters.WonAmount;
            //_params.DeltaWonNr = 0;

            //_params.Played = new AccountBalance(_params.DeltaPlayedRe, 0, _params.DeltaPlayedNr);
            //_params.Won = new AccountBalance(_params.DeltaWonRe, 0, _params.DeltaWonNr);

            _params.DeltaBalanceFoundInSession = new AccountBalance(Input.DeltaBalanceFoundInSession);
          }

          if (!Trx_UpdatePlayedAndWon(_params, Output.ErrorMsg, Trx))
          {
            Output.SetError(EndSessionStatus.ErrorRetry, "Trx_UpdatePlayedAndWon failed.");

            return false;
          }

          if (_is_mico2)
          {
#if !SQL_BUSINESS_LOGIC
            Decimal _played_calculated;
            Decimal _won_calculated;

            _played_calculated = _params.Played.TotalBalance;
            _won_calculated = _params.Won.TotalBalance;
#endif
            _params.Played = new AccountBalance(Input.Meters.PlayedReedemable + Input.Meters.PlayedNonRestrictedAmount, 0, Input.Meters.PlayedRestrictedAmount);
            _params.Won = new AccountBalance(Input.Meters.WonAmount, 0, 0);

#if !SQL_BUSINESS_LOGIC
            if (_params.Played.TotalBalance != _played_calculated || _params.Won.TotalBalance != _won_calculated)
            {
              Log.Message(String.Format("*** MISMATCH Trx_UpdatePlayedAndWon PlaySession: {0}, PlayedCalculate: {1}, WonCalculate: {2}, InputPlayed: {3}, InputWon: {4}", Input.PlaySessionId, _played_calculated.ToString(), _won_calculated.ToString(), _params.Played.TotalBalance.ToString(), _params.Won.TotalBalance));
          }
#endif
          }

          // Niether all delta played or all delta won is computed!!!  There should be a mismatch.  Request for the logger
          if (_params.DeltaPlayed > (Decimal)0.0
              || _params.DeltaWon > (Decimal)0.0)
          {
            _delta_not_computed = true;
          }
        }

        // Played & Won taken from here, the mismatch is not taken into account to award points.
        _final_params = _params;

        _balance_from_gm_calculated = new AccountBalance(_params.Balance);

        _balance_from_gm_to_add = new AccountBalance(_balance_from_gm_calculated);

        switch (TerminalType)
        {
          case TerminalTypes.WIN:       // LKT
            _balance_from_gm = _balance_from_gm_calculated.TotalBalance;  // Always calculated
            break;

          case TerminalTypes.SAS_HOST:  // SAS-HOST
            break;

          case TerminalTypes.T3GS:      // 3GS
            if (Input.MovementType == MovementType.ManualEndSession)
            {
              if (!Input.FinalBalanceSelectedByUserAfterMismatch)
              {
                _balance_from_gm = _balance_from_gm_calculated.TotalBalance;
              }

              Int32.TryParse(ReadGeneralParams("WS2S", "CloseSessionToZero", Trx), out _close_session_to_zero);

              if (_close_session_to_zero != 0)
              {
                _balance_from_gm = 0;
              }
            }
            break;

          default:
            Output.SetError(EndSessionStatus.FatalError, "Unexpected TerminalType: " + TerminalType.ToString());

            return false;
        }        

        if (_is_tito)
        {
            _mismatch = (Input.FoundInEgm.TotalBalance + Input.TitoSessionMeters.TotalCashIn + Input.Meters.WonAmount - Input.Meters.PlayedAmount - Input.TitoSessionMeters.TotalCashOut) != 0;
        }
        else
        {
            _mismatch = (_balance_from_gm_calculated.TotalBalance != _balance_from_gm);
        }
        
        _big_mismatch = _mismatch;

        // Check Balance Mismatch
        if (_mismatch || _delta_not_computed)
        {
          Decimal _diff;
          Decimal _maximum_balance_error;

          _balance_from_gm_to_add = new AccountBalance(_balance_from_gm_calculated);
          _diff = _balance_from_gm - _balance_from_gm_calculated.TotalBalance;

          _big_mismatch = false;

          // if BIG return
          if (TerminalType == TerminalTypes.SAS_HOST || TerminalType == TerminalTypes.WIN)
          {
            Decimal.TryParse(ReadGeneralParams("SasHost", "MaximumBalanceErrorCents", Trx), out _maximum_balance_error);
            _maximum_balance_error = _maximum_balance_error / 100;

            if (_diff > _maximum_balance_error)
            {
              _big_mismatch = true;
            }
          }

          if (TerminalType == TerminalTypes.T3GS)
          {
            Decimal.TryParse(ReadGeneralParams("Interface3GS", "MaximumBalanceErrorCents", Trx), out _maximum_balance_error);
            _maximum_balance_error = _maximum_balance_error / 100;

            if (Math.Abs(_diff) > _maximum_balance_error)
            {
              _big_mismatch = true;
            }
          }

          // TODO: Simulate Played/Won 
          InSessionParameters _tmp_params;

          _tmp_params = new InSessionParameters();

          _tmp_params.PlaySessionId = Input.PlaySessionId;
          _tmp_params.AccountId = _params.AccountId;

          _tmp_params.Balance = new AccountBalance(_balance_from_gm_calculated);

          if (_diff > 0)
          {
            _tmp_params.DeltaPlayed = 0;
            _tmp_params.DeltaWon = _diff;
          }
          else
          {
            _tmp_params.DeltaPlayed = Math.Abs(_diff);
            _tmp_params.DeltaWon = 0;
          }

          MultiPromos.AccountBalance _reported_balance;

          if (_is_tito)
          {
            // It shouldn't add balance 
            _balance_from_gm_to_add = new AccountBalance();
            _reported_balance = new AccountBalance();
          }
          else
          {
            if (!Trx_UpdatePlayedAndWon(_tmp_params, Output.ErrorMsg, Trx))
            {
              Output.SetError(EndSessionStatus.ErrorRetry, "Trx_UpdatePlayedAndWon failed.");

              return false;
            }
            _reported_balance = _tmp_params.Balance;
            // Set final = reported
            _balance_from_gm_to_add = new AccountBalance(_reported_balance);
          }

          if (Input.MovementType == MovementType.ManualEndSession)
          {
            _mismatch = false;
            _big_mismatch = false;
          }

          if (_big_mismatch)
          {
            // Set final = Min(reported, calculated)
            if (_reported_balance.TotalBalance > _balance_from_gm_calculated.TotalBalance)
            {
              _balance_from_gm_to_add = new AccountBalance(_balance_from_gm_calculated);
            }

            Trx.Rollback("CalculatePlayedAndWon");
          }

          //27-SEPT-2013 JPJ  Added new call to logger.
          if (TerminalType == TerminalTypes.SAS_HOST || TerminalType == TerminalTypes.WIN)
          {
            if (!InsertWcpCommand(TerminalId, Input.PlaySessionId, Trx))
            {
              Output.SetError(EndSessionStatus.ErrorRetry, "Calling InsertWcpCommand: TerminalId: " + TerminalId.ToString() + " PlaySessionId: " + Input.PlaySessionId.ToString());
            }
          }
        }

        if (!_is_tito)
        {
          // Set Mismatch
          if (!Trx_PlaySessionSetFinalBalance(Input.PlaySessionId,
                                              _balance_from_gm_to_add.TotalBalance,
                                              _balance_from_gm,
                                              _mismatch, Input.MovementType, Trx))
          {
            Output.SetError(EndSessionStatus.ErrorRetry, "Trx_PlaySessionSetFinalBalance failed.");

            return false;
          }

        }

        if (Input.FoundInEgm.TotalBalance > 0 || Input.RemainingInEgm.TotalBalance > 0)
        {
          if (!Trx_UpdateFoundAndRemainingInEgm(Input.PlaySessionId, Input.FoundInEgm, Input.RemainingInEgm, Trx))
          {
            Output.SetError(EndSessionStatus.ErrorRetry, "Trx_UpdateFoundAndRemainingInEgm failed.");

            return false;

          }
        }

        if (_big_mismatch)
        {
          Output.SetError(EndSessionStatus.BalanceMismatch,
                          String.Format("Balance Mismatch: Reported={0}, Calculated={1}, Final={2}",
                          Input.BalanceFromGM, _balance_from_gm_calculated.ToString(), _balance_from_gm_to_add.ToString()));

          Output.PlaySessionFinalBalance = new AccountBalance(_balance_from_gm_to_add);

          if (!_is_tito)
          {
            return true;
          }
        }

        // Close PlaySession
        _psc_params = new PlaySessionCloseParameters();
        _psc_params.MovementType = Input.MovementType;
        _psc_params.CashierName = Input.CashierName;
        _psc_params.PlaySessionId = Input.PlaySessionId;
        _psc_params.AccountId = Input.AccountId;
        _psc_params.TerminalId = TerminalId;
        _psc_params.TerminalName = TerminalName;

        _psc_params.BalanceMismatch = _mismatch;
        _psc_params.BalanceFromGMToAdd = new AccountBalance(_balance_from_gm_to_add);
        _psc_params.ErrorMsg = Output.ErrorMsg;

        _psc_params.NumPlayed = Input.Meters.PlayedCount;
        _psc_params.Played = _final_params.Played;
        _psc_params.NumWon = Input.Meters.WonCount;
        _psc_params.Won = _final_params.Won;
        _psc_params.CloseSessionComment = Input.CloseSessionComment;
        _psc_params.IsMico2 = Input.IsMico2;
        _psc_params.HandpaysCents = Input.Meters.HandpaysAmount;

        if (_is_tito || _is_mico2)
        {
          _psc_params.IsTITO = true;
          _psc_params.VirtualAccountId = Input.VirtualAccountId;
          _psc_params.TitoSessionMeters = Input.TitoSessionMeters;

          if (!Trx_PlaySessionTITOSetMeters(Input.PlaySessionId,
                                            Input.TitoSessionMeters.TicketInPromoNr,
                                            Input.TitoSessionMeters.TicketInPromoRe,
                                            Input.TitoSessionMeters.TicketInCashable,
                                            Input.TitoSessionMeters.TicketOutCashable,
                                            Input.TitoSessionMeters.TicketOutPromoNr,
                                            Input.TitoSessionMeters.CashIn.CashInTotal,
                                            Trx))
          {
            Output.SetError(EndSessionStatus.ErrorRetry, "Trx_PlaySessionTITOSetMeters failed.");

            return false;
          }
        }

        if (!Trx_PlaySessionClose(_psc_params, Output, Input.Meters.PlayedAmount, Trx))
        {
          Output.SetError(EndSessionStatus.ErrorRetry, "Trx_PlaySessionClose failed.");

          return false;
        }

        Output.PlaySessionFinalBalance = new AccountBalance(_balance_from_gm_to_add);
        if (TerminalType == TerminalTypes.SAS_HOST || TerminalType == TerminalTypes.T3GS)
        {
          REQUEST_TYPE_RESPONSE _status;
          decimal _reserved_initial_balance;
          if (MultiPromos.Trx_GetFBMReservedCreditEnabled(out _fbm_vendorid, Trx))
          {
            if (_fbm_vendorid == VendorId)
            {
              if (!MultiPromos.Trx_GameGateway_Reserve_Credit(Input.AccountId, Input.BalanceFromGM, REQUEST_TYPE.MODIFY_RESERVED_CREDIT, true, out _status, out _reserved_initial_balance, Trx))
              {
                Output.SetError(EndSessionStatus.ErrorRetry, String.Format("Trx_GameGateway_Reserve_Credit failed!!! - AccountId: {1}", Input.AccountId));

                return false;
              }
            }
          }
        }
        Output.SetSuccess();

        return true;
      }
      catch (Exception _ex)
      {
        Output.SetError(EndSessionStatus.ErrorRetry, _ex.Message);
      }

      return false;
    }

    private static Boolean Trx_UpdateFoundAndRemainingInEgm(Int64 PlaySessionId, AccountBalance Found, AccountBalance Remaining, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("UPDATE   PLAY_SESSIONS");
      _sb.AppendLine("   SET   PS_RE_FOUND_IN_EGM     = @pREFoundInEgm");
      _sb.AppendLine("       , PS_NR_FOUND_IN_EGM     = @pNRFoundInEgm ");
      _sb.AppendLine("       , PS_RE_REMAINING_IN_EGM = @pRERemainingInEgm ");
      _sb.AppendLine("       , PS_NR_REMAINING_IN_EGM = @pNRRemainingInEgm ");
      _sb.AppendLine(" WHERE   PS_PLAY_SESSION_ID     = @pPlaySessionID ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPlaySessionID", SqlDbType.BigInt).Value = PlaySessionId;
          _cmd.Parameters.Add("@pREFoundInEgm", SqlDbType.Money).Value = Math.Max(Found.Redeemable, 0);
          _cmd.Parameters.Add("@pNRFoundInEgm", SqlDbType.Money).Value = Math.Max(Found.TotalNotRedeemable, 0);
          _cmd.Parameters.Add("@pRERemainingInEgm", SqlDbType.Money).Value = Math.Max(Remaining.Redeemable, 0);
          _cmd.Parameters.Add("@pNRRemainingInEgm", SqlDbType.Money).Value = Math.Max(Remaining.TotalNotRedeemable, 0);

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
#if !SQL_BUSINESS_LOGIC

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
#else
      catch
      { ; }
#endif

      return false;
    } // Trx_OnEndCardSession

    /// <summary>
    /// Returns (in the output parameter) if the Account is Anonymous or Virtual
    /// </summary>
    /// <param name="AccountId">Account to verify</param>
    /// <param name="IsAnonymousOrVirtual">Output param that shows if a Account is Anonymous or Virtual</param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean IsAccountAnonymousOrVirtual(Int64 AccountId, out Boolean IsAnonymousOrVirtual, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _holder_level;
      Object _obj;

      IsAnonymousOrVirtual = false;
      _holder_level = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   CASE WHEN AC_TYPE = @pAccountVirtualTerminal ");
        _sb.AppendLine("                 THEN 0                                ");
        _sb.AppendLine("                 ELSE ISNULL(AC_HOLDER_LEVEL, 0)       ");
        _sb.AppendLine("          END                                          ");
        _sb.AppendLine("   FROM   ACCOUNTS                                     ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId                  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pAccountVirtualTerminal", SqlDbType.Int).Value = AccountType.ACCOUNT_VIRTUAL_TERMINAL;


          _obj = _cmd.ExecuteScalar();

        }


        if (_obj != null && _obj != DBNull.Value)
        {
          _holder_level = (Int32)_obj;
        }

        // Is Account anonymous OR Virtual 
        IsAnonymousOrVirtual = (_holder_level == 0);
      }
#if !SQL_BUSINESS_LOGIC
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return false;
      }
#else
      catch
      {
        return false;
      }
#endif

      return true;
    }

    // PURPOSE: Close PlaySession
    //
    //  PARAMS:
    //     - INPUT:
    //           - Params:
    //           - Trx:
    //
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if was successfully , false otherwise.
    //
    private static Boolean Trx_PlaySessionClose(PlaySessionCloseParameters Params, EndSessionOutput Output, Decimal PlayedAmount, SqlTransaction Trx)
    {
      AccountBalance _ini_balance;
      AccountBalance _fin_balance;

      AccountBalance _to_gm;
      AccountBalance _from_gm;
      AccountBalance _ps_balance;

      AccountMovementsTable _am_table;
      PlaySessionStatus _desired_status;
      PlaySessionStatus _old_status;
      PlaySessionStatus _new_status;
      Int32 _terminal_id;

      Terminal.TerminalInfo _terminal_info;
      TimeSpan _play_session_duration;
      Boolean _is_tito;
      Int64 _default_account_id;
      Int64 _unlink_account_id;
      Int32 _elp_mode;
      Boolean _is_anonymous_or_virtual;
      Int64 _sequence;
      Int32 _generation_cash_out_voucher_enabled;

      _is_tito = Params.IsTITO;
      if (Params.IsMico2 && Params.AccountId != 0)
      {
        _is_tito = false;
      }

      if (!_is_tito)
      {
        _default_account_id = Params.AccountId;
        _unlink_account_id = Params.AccountId;
      }
      else
      {
        _default_account_id = Params.AccountId != 0 ? Params.AccountId : Params.VirtualAccountId;
        _unlink_account_id = Params.VirtualAccountId;
      }

      _ini_balance = new AccountBalance();
      _fin_balance = new AccountBalance();

      if (!Terminal.Trx_GetTerminalInfo(Params.TerminalId, out _terminal_info, Trx))
      {
        Output.SetError(EndSessionStatus.ErrorRetry, "Trx_GetTerminalInfo failed.");

        return false;
      }

      if (!_is_tito)
      {
        if (!Trx_UpdateAccountBalance(Params.AccountId, Params.BalanceFromGMToAdd, out _fin_balance, Trx))
        {
          Output.SetError(EndSessionStatus.ErrorRetry, "Trx_UpdateAccountBalance failed.");

          return false;
        }
        _ini_balance = _fin_balance - Params.BalanceFromGMToAdd;
      }

      // Account has activity
      if (!Trx_SetActivity(_default_account_id, Trx))
      {
        Output.SetError(EndSessionStatus.ErrorRetry, "Trx_SetActivity failed.");

        return false;
      }

      if (!_is_tito)
      {
        if (!Trx_UpdateAccountInSession(InSessionAction.EndSession, Params.AccountId, 0, Params.BalanceFromGMToAdd,
                                        out _to_gm, out _from_gm, out _ps_balance, Trx))
        {
          Output.SetError(EndSessionStatus.ErrorRetry, "Trx_UpdateAccountInSession failed.");

          return false;
        }

        if (!Trx_UnlinkPromotionsInSession(Params.AccountId, Params.PlaySessionId, 0, Trx))
        {
          Output.SetError(EndSessionStatus.ErrorRetry, "Trx_UnlinkPromotionsInSession failed.");

          return false;
        }

        if (Params.IsMico2)
        {
          _to_gm.Redeemable += Params.TitoSessionMeters.RedeemableCashIn;
          _to_gm.PromoRedeemable += Params.TitoSessionMeters.PromoReCashIn;
          _to_gm.PromoNotRedeemable += Params.TitoSessionMeters.PromoNrCashIn;

          _from_gm.Redeemable += Params.TitoSessionMeters.RedeemableCashOut;
          _from_gm.PromoRedeemable += 0;
          _from_gm.PromoNotRedeemable += Params.TitoSessionMeters.PromoNrCashOut;
        }
      }
      else
      {
        _to_gm = new AccountBalance(Params.TitoSessionMeters.RedeemableCashIn, Params.TitoSessionMeters.PromoReCashIn, Params.TitoSessionMeters.PromoNrCashIn);
        _from_gm = new AccountBalance(Params.TitoSessionMeters.RedeemableCashOut, 0, Params.TitoSessionMeters.PromoNrCashOut);

        // XCD 05-FEB-2015: Unlink is done in StartCardSession. If anytime has to be done in EndCardSession, this is the place.
        //
        ////////// Unlink promos assigned with lcd
        ////////if (!Trx_UnlinkPromotionsInSession(Params.AccountId, Params.PlaySessionId, 0, Trx))
        ////////{
        ////////  Output.SetError(EndSessionStatus.ErrorRetry, "Trx_UnlinkPromotionsInSession failed.");

        ////////  return false;
        ////////}
      }

      switch (Params.MovementType)
      {
        case MovementType.EndCardSession:
          _desired_status = PlaySessionStatus.Closed;
          _terminal_id = Params.TerminalId;
          break;

        case MovementType.ManualEndSession:
          _desired_status = PlaySessionStatus.ManualClosed;
          // For ManualEndSession, _terminal_id = 0 indicates that, for AccountMovements,
          // the important thing is CashierName not TerminalName.
          _terminal_id = 0;
          break;

        default:
          Output.SetError(EndSessionStatus.FatalError, "Unexpected MovementType: " + Params.MovementType.ToString());

          return false;
      }

      // PsIniBalance
      // PsFinBalance
      // PsDiffBalance --> Agotado
      // PsPlayed
      // PsWon
      //JCA 29-DEC-2017  Se comenta la siguiente l�nea pq se intent� solucionar el WIGOS-5741 y ocasion� el WIGOS-7418
      //Params.Played.Redeemable = PlayedAmount;

      if (!Trx_PlaySessionSetMeters(Params.PlaySessionId, _to_gm, Params.Played, Params.Won, _from_gm, Params.HandpaysCents, Trx))
      {
        Output.SetError(EndSessionStatus.ErrorRetry, "Trx_PlaySessionSetMeters failed.");

        return false;
      }

      if (_desired_status == PlaySessionStatus.Closed || _desired_status == PlaySessionStatus.ManualClosed)
      {
        Decimal _dummy1;
        Decimal _dummy2;
        AccountBalance _dummy_bal1;

        if (_terminal_info.CurrentPlaySessionId == Params.PlaySessionId)
        {
          if (!Trx_ResetAccountInSession(_default_account_id, 0, out _dummy1, out _dummy2, out _dummy_bal1, Trx))
          {
            Output.SetError(EndSessionStatus.ErrorRetry, "Trx_PlaySessionClose: Trx_ResetAccountInSession failed. Account_id: " + Params.AccountId.ToString());

            return false;
          }
        }
      }

      if (!Trx_PlaySessionChangeStatus(_desired_status, Params.PlaySessionId, out _old_status, out _new_status, out _play_session_duration, Trx)) // to add parameter true
      {
        Output.SetError(EndSessionStatus.ErrorRetry, "Trx_PlaySessionChangeStatus failed.");

        return false;
      }

      Int32.TryParse(ReadGeneralParams("AutoPrintVoucher", "CashOut.Generation.Enabled", Trx), out _generation_cash_out_voucher_enabled);

      if (_generation_cash_out_voucher_enabled == 1)
      {

        if (!Trx_SequenceGetValue(Trx, SequenceId.AutoPrintVoucherCashOut, out _sequence))
        {
          Output.SetError(EndSessionStatus.ErrorRetry, "Trx_SequenceGetValue failed.");

          return false;
        }

        if (!Trx_PlaySessionUpdateSequence(Params.PlaySessionId, _sequence, Trx))
        {
          return false;
        }
      }

      if ((_old_status == PlaySessionStatus.Opened || _old_status == PlaySessionStatus.Abandoned) && _new_status != PlaySessionStatus.Opened)
      {
        // Llenar la tabla  MS_PENDING_GAME_PLAY_SESSIONS.
        if (!Trx_InsertAgroupGamePlaySession(Params.PlaySessionId, Trx))
        {
          Output.SetError(EndSessionStatus.ErrorRetry, "Trx_InsertAgroupGamePlaySession failed.");

          return false;
        }
      }

      if (!Trx_UnlinkAccountTerminal(_unlink_account_id, Params.TerminalId, Params.PlaySessionId, Trx))
      {
        Output.SetError(EndSessionStatus.ErrorRetry, "Trx_UnlinkAccountTerminal failed.");

        return false;
      }

      if (_is_tito)
      {
        if (!Trx_UpdatePsAccountId(Params.PlaySessionId, _default_account_id, Trx))
        {
          Output.SetError(EndSessionStatus.ErrorRetry, "Trx_UpdatePsAccountId failed.");

          return false;
        }
      }

      if (Params.Played.TotalBalance > 0)
      {
        if (!Trx_ResetCancellableOperation(_default_account_id, Trx))
        {
          Output.SetError(EndSessionStatus.ErrorRetry, "Trx_ResetCancellableOperation failed.");

          return false;
        }

        if (!Params.BalanceMismatch)
        {
          if (!Trx_SiteJackpotContribution(_terminal_info, Params.Played, Trx))
          {
            Output.SetError(EndSessionStatus.ErrorRetry, "Trx_SiteJackpotContribution failed.");

            return false;
          }
        }

        if (!Trx_JackpotAmountTerminal(_terminal_info, Params.Played, Trx))
        {
          Output.SetError(EndSessionStatus.ErrorRetry, "Trx_JackpotAmountTerminal failed.");

          return false;
        }

        if (Params.MovementType == MovementType.EndCardSession && !Params.BalanceMismatch)
        {
          if (_is_tito)
          {
            if (!MultiPromos.Trx_TITO_UpdatePromoNRCost(Params.PlaySessionId, Params.TitoSessionMeters.TicketOutCashable, Params.TitoSessionMeters.RedeemableCashIn, Trx))
            {
              Output.SetError(EndSessionStatus.ErrorRetry, "Trx_TITO_CalculatePromoCost failed.");

              return false;
            }
          }
          else
          {
            if (!MultiPromos.Trx_PromoMarkAsExhausted(Params.AccountId, Trx))
            {
              Output.SetError(EndSessionStatus.ErrorRetry, "Trx_PromoMarkAsExhausted failed.");

              return false;
            }
          }
        }
      }

      // Create the EndCardSession movement
      _am_table = new AccountMovementsTable(_terminal_id, Params.TerminalName, Params.PlaySessionId, Params.CashierName);

      Int64 _account_id;
      _account_id = Params.AccountId;
      if (_is_tito)
      {
        _account_id = Params.VirtualAccountId;
      }
      if (!_am_table.Add(0, _account_id, Params.MovementType,
                         _ini_balance.TotalBalance, 0, Params.BalanceFromGMToAdd.TotalBalance, _fin_balance.TotalBalance, Params.CloseSessionComment))
      {
        Output.SetError(EndSessionStatus.ErrorRetry, "AccountMovementsTable.Add(" + Params.MovementType.ToString() + ") failed.");

        return false;
      }
      if (!_am_table.Save(Trx))
      {
        Output.SetError(EndSessionStatus.ErrorRetry, "AccountMovementsTable.Save failed.");

        return false;
      }

      // 18-MAR-2016  SMN
      if (!Trx_GetExternalLoyaltyProgramMode(out _elp_mode, Trx))
      {
        _elp_mode = 0;
      }

      if (_elp_mode == 0) // ELP disabled
      {
        if (Params.Played.TotalRedeemable > 0)
        {
          // 23-MAR-2016 FGB Bug 10955:Multiple Buckets: Does not have to compute buckets for anonymous or virtual accounts
          if (!IsAccountAnonymousOrVirtual(Params.AccountId, out _is_anonymous_or_virtual, Trx))
          {
            _is_anonymous_or_virtual = false;
          }

          if (!_is_anonymous_or_virtual)
          {
            if (!Trx_PlaySessionToPending(_play_session_duration, Params, Output, _terminal_info.TerminalType, Trx))
            {
              Output.SetError(EndSessionStatus.ErrorRetry, "Trx_PlaySessionToPending failed.");

              return false;
            }
          }
        }
      }

#if !SQL_BUSINESS_LOGIC

      // RAB 03-MAR-2016: Reset HighRoller and HighRoller Anonymous to end play session.
      if (!Trx_ResetStatusBitmaskHighRoller(Trx, _terminal_info.TerminalId))
      {
        Output.SetError(EndSessionStatus.ErrorRetry, "Trx_ResetStatusBitmaskHighRoller failed.");
        return false;
      }
#endif
      return true;
    }

#if !SQL_BUSINESS_LOGIC
    // RAB 03-MAR-2016
    public static Boolean Trx_ResetStatusBitmaskHighRoller(SqlTransaction SqlTrx, Int32 TerminalId)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("     UPDATE TERMINAL_STATUS ");
        _sb.AppendLine("     SET TS_PLAYED_WON_FLAGS = TS_PLAYED_WON_FLAGS & ~(" + ((Int32)TerminalStatusFlags.PLAYS_FLAG.HIGHROLLER + (Int32)TerminalStatusFlags.PLAYS_FLAG.HIGHROLLER_ANONYMOUS).ToString() + ") ");

        // RAB 03-MAR-2016
        _sb.AppendLine("     WHERE TS_TERMINAL_ID = @pTerminalId");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          if (_sql_cmd.ExecuteNonQuery() != -1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Trx_ResetStatusBitmaskHighRoller
#endif

    public static bool Trx_AddBalancesToAccount(Int64 AccountId, Int64 PlaySessionId, Int32 TerminalId, AccountBalance BalanceToAdd, SqlTransaction Trx)
    {
      AccountBalance _fin_balance;
      AccountBalance _ini_balance;
      InSessionParameters _in_sessions_param;
      StringBuilder _error_msg;
      AccountBalance _balance_from_gm_calculated;
      AccountBalance _balance_from_gm_to_add;
      Boolean _mismatch;
      Terminal.TerminalInfo _terminal_info;

      AccountBalance _to_gm;
      AccountBalance _from_gm;
      AccountBalance _ps_balance;

      _error_msg = new StringBuilder();

      if (!Trx_GetDeltaPlayedWon(PlaySessionId, out _in_sessions_param, Trx))
      {
#if !SQL_BUSINESS_LOGIC
        Log.Error("Trx_AddBalancesToAccount: Trx_GetDeltaPlayedWon failed. AccountId " + AccountId.ToString() + " PSId" + PlaySessionId.ToString());
#endif
        return false;
      }

      if (!Trx_UpdatePlayedAndWon(_in_sessions_param, _error_msg, Trx))
      {
#if !SQL_BUSINESS_LOGIC
        Log.Error("Trx_AddBalancesToAccount: Trx_UpdatePlayedAndWon failed." + _error_msg.ToString());
#endif

        return false;
      }

      _balance_from_gm_calculated = new AccountBalance(_in_sessions_param.Balance);
      _balance_from_gm_to_add = new AccountBalance(_balance_from_gm_calculated);
      _mismatch = (_balance_from_gm_calculated.TotalBalance != BalanceToAdd.TotalBalance);

      if (_mismatch)
      {
#if !SQL_BUSINESS_LOGIC
        Log.Error("Trx_AddBalancesToAccount: mismatch  balance." + _balance_from_gm_calculated.ToString() + " / " + BalanceToAdd.ToString());
#endif
      }

      if (!Trx_PlaySessionSetFinalBalance(PlaySessionId,
                                          _balance_from_gm_to_add.TotalBalance,
                                          BalanceToAdd.TotalBalance,
                                          _mismatch, MovementType.EndCardSession, Trx))
      {
#if !SQL_BUSINESS_LOGIC
        Log.Error("Trx_AddBalancesToAccount: Trx_PlaySessionSetFinalBalance.");
#endif

        return false;
      }

      if (!Trx_UpdateAccountBalance(AccountId, _balance_from_gm_to_add, out _fin_balance, Trx))
      {
#if !SQL_BUSINESS_LOGIC
        Log.Error("Trx_AddBalancesToAccount: Trx_UpdateAccountBalance.");
#endif

        return false;
      }
      _ini_balance = _fin_balance - _balance_from_gm_to_add;

      if (!Trx_UpdateAccountInSession(InSessionAction.EndSession, AccountId, 0, _balance_from_gm_to_add, out _to_gm, out _from_gm, out _ps_balance, Trx))
      {
#if !SQL_BUSINESS_LOGIC
        Log.Error("Trx_AddBalancesToAccount: Trx_UpdateAccountInSession.");
#endif

        return false;
      }

      if (!Trx_UnlinkPromotionsInSession(AccountId, PlaySessionId, 0, Trx))
      {
#if !SQL_BUSINESS_LOGIC
        Log.Error("Trx_AddBalancesToAccount: Trx_UnlinkPromotionsInSession.");
#endif

        return false;
      }

      // PsIniBalance
      // PsFinBalance
      // PsDiffBalance --> Agotado
      // PsPlayed
      // PsWon
      if (!Trx_PlaySessionSetMeters(PlaySessionId, _to_gm, _in_sessions_param.Played, _in_sessions_param.Won, _from_gm, 0, Trx))
      {
#if !SQL_BUSINESS_LOGIC
        Log.Error("Trx_AddBalancesToAccount: Trx_PlaySessionSetMeters failed.");
#endif

        return false;
      }

      if (!Trx_UnlinkAccountTerminal(AccountId, TerminalId, PlaySessionId, Trx))
      {
#if !SQL_BUSINESS_LOGIC
        Log.Error("Trx_AddBalancesToAccount: Trx_UnlinkAccountTerminal failed.");
#endif

        return false;
      }

      if (_in_sessions_param.Played.TotalBalance > 0)
      {
        if (!Trx_ResetCancellableOperation(AccountId, Trx))
        {
#if !SQL_BUSINESS_LOGIC
          Log.Error("Trx_AddBalancesToAccount: Trx_ResetCancellableOperation failed.");
#endif

          return false;
        }

        if (!Terminal.Trx_GetTerminalInfo(TerminalId, out _terminal_info, Trx))
        {
#if !SQL_BUSINESS_LOGIC
          Log.Error("Trx_AddBalancesToAccount: Trx_GetTerminalInfo failed.");
#endif

          return false;
        }

        if (!Trx_SiteJackpotContribution(_terminal_info, _in_sessions_param.Played, Trx))
        {
#if !SQL_BUSINESS_LOGIC
          Log.Error("Trx_AddBalancesToAccount: Trx_SiteJackpotContribution failed.");
#endif

          return false;
        }

        if (!MultiPromos.Trx_PromoMarkAsExhausted(AccountId, Trx))
        {
#if !SQL_BUSINESS_LOGIC
          Log.Error("Trx_AddBalancesToAccount: Trx_PromoMarkAsExhausted failed.");
#endif
          return false;
        }
      }

      return true;
    }

    public static bool Trx_PlaySessionToPending(TimeSpan Play_Session_duration
                                               , PlaySessionCloseParameters Params
                                               , EndSessionOutput Output
                                               , TerminalTypes TerminalType
                                               , SqlTransaction Trx)
    {
      StringBuilder _sb;
      try
      {

        //
        // Update Site Jackpot Played amounts
        // 
        _sb = new StringBuilder();
        _sb.AppendLine("  INSERT INTO PENDING_PLAY_SESSIONS_TO_PLAYER_TRACKING ");
        _sb.AppendLine("  (                                                    ");
        _sb.AppendLine("       PPS_SESSION_ID,                                 ");
        _sb.AppendLine("       PPS_ACCOUNT_ID,                                 ");
        _sb.AppendLine("       PPS_COIN_IN,                                    ");
        _sb.AppendLine("       PPS_TERMINAL_ID,                                ");

        _sb.AppendLine("       PPS_DURATION_TOTAL_MINUTES,                     ");
        _sb.AppendLine("       PPS_PARAMS_NUMPLAYED,                           ");
        _sb.AppendLine("       PPS_PARAMS_BALANCE_MISMATCH,                    ");
        _sb.AppendLine("       PPS_TERMINAL_TYPE,                              ");

        _sb.AppendLine("       PPS_DATETIMECREATED                             ");

        _sb.AppendLine("  ) VALUES (                                           ");
        _sb.AppendLine("       @pps_session_id,                                ");
        _sb.AppendLine("       @pps_account_id,                                ");
        _sb.AppendLine("       @pps_coin_in,                                   ");
        _sb.AppendLine("       @pps_terminal_id,                               ");
        _sb.AppendLine("       @pps_duration_total_minutes,                    ");
        _sb.AppendLine("       @pps_params_numplayed,                          ");
        _sb.AppendLine("       @pps_params_balance_mismatch,                   ");
        _sb.AppendLine("       @pps_terminal_type,                             ");
        _sb.AppendLine("       getdate()                                       ");
        _sb.AppendLine("  )                                                    ");


        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pps_session_id", SqlDbType.BigInt).Value = Params.PlaySessionId;
          _cmd.Parameters.Add("@pps_account_id", SqlDbType.BigInt).Value = Params.AccountId;
          _cmd.Parameters.Add("@pps_coin_in", SqlDbType.Money).Value = Params.Played.TotalRedeemable;
          _cmd.Parameters.Add("@pps_terminal_id", SqlDbType.Int).Value = Params.TerminalId;

          _cmd.Parameters.Add("@pps_duration_total_minutes", SqlDbType.Int).Value = Play_Session_duration.TotalMinutes;
          _cmd.Parameters.Add("@pps_params_numplayed", SqlDbType.BigInt).Value = Params.NumPlayed;
          _cmd.Parameters.Add("@pps_params_balance_mismatch", SqlDbType.Bit).Value = Params.BalanceMismatch;
          _cmd.Parameters.Add("@pps_terminal_type", SqlDbType.Int).Value = TerminalType;

          _cmd.ExecuteNonQuery();
        }

        return true;
      }
      catch
      { ; }

      return false;
    }



    private static bool Trx_SiteJackpotContribution(Terminal.TerminalInfo TerminalInfo, AccountBalance Played, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        if (!TerminalInfo.PvSiteJackpot)
        {
          return true;
        }

        Decimal _max_played;

        Decimal.TryParse(ReadGeneralParams("SiteJackpot", "ToJackpotMaxPlayedOnSession", Trx), out _max_played);

        if (_max_played > 0 && (Played.TotalBalance >= _max_played
                             || Played.TotalRedeemable >= _max_played))
        {
#if !SQL_BUSINESS_LOGIC
        StringBuilder sb;
        sb = new StringBuilder();
        sb.AppendLine("Trx_SiteJackpotContribution *** Played exceeded *** " + string.Format("PlaySessionId: {0} ", TerminalInfo.CurrentPlaySessionId.ToString()));
        sb.AppendLine(string.Format("Played TotalBalance:{0}, TotalRedeemable:{1}", Played.TotalBalance.ToString(), Played.TotalRedeemable.ToString()));
        Log.Message("WARNING: " + sb.ToString());
#endif
          return true;
        }



        //
        // Update Site Jackpot Played amounts
        // 
        _sb = new StringBuilder();
        _sb.AppendLine("  UPDATE SITE_JACKPOT_PARAMETERS");
        _sb.AppendLine("     SET SJP_PLAYED  = SJP_PLAYED + CASE WHEN (SJP_ONLY_REDEEMABLE = 1) THEN @pTotalRedeemablePlayed ELSE @pTotalPlayed END");
        _sb.AppendLine("   WHERE SJP_ENABLED = 1");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTotalRedeemablePlayed", SqlDbType.Money).Value = Played.TotalRedeemable;
          _cmd.Parameters.Add("@pTotalPlayed", SqlDbType.Money).Value = Played.TotalBalance;

          _cmd.ExecuteNonQuery();
        }

        return true;
      }
      catch
      { ; }

      return false;
    }

    /// <summary>
    /// Accumulate amount into jackpots_amount_terminals
    /// </summary>
    /// <param name="TerminalInfo"></param>
    /// <param name="Played"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean Trx_JackpotAmountTerminal(Terminal.TerminalInfo TerminalInfo, AccountBalance Played, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _enabled;

      try
      {
        Int32.TryParse(ReadGeneralParams("Jackpots", "Enabled", Trx), out _enabled);
        if (_enabled != 1)
        {
          return true;
        }

        _sb = new StringBuilder();
        _sb.AppendLine(" IF EXISTS (SELECT   1                               ");
        _sb.AppendLine("              FROM   JACKPOTS_AMOUNT_TERMINALS       ");
        _sb.AppendLine("             WHERE   JAT_TERMINAL_ID = @pTerminalId) ");
        _sb.AppendLine(" BEGIN                                               ");
        _sb.AppendLine("   UPDATE   JACKPOTS_AMOUNT_TERMINALS                ");
        _sb.AppendLine("      SET   JAT_AMOUNT = JAT_AMOUNT + @pAmount       ");
        _sb.AppendLine("    WHERE   JAT_TERMINAL_ID = @pTerminalId           ");
        _sb.AppendLine(" END                                                 ");
        _sb.AppendLine(" ELSE                                                ");
        _sb.AppendLine(" BEGIN                                               ");
        _sb.AppendLine("   INSERT INTO   JACKPOTS_AMOUNT_TERMINALS           ");
        _sb.AppendLine("               ( JAT_TERMINAL_ID                     ");
        _sb.AppendLine("               , JAT_AMOUNT)                         ");
        _sb.AppendLine("        VALUES ( @pTerminalId                        ");
        _sb.AppendLine("               , @pAmount)                           ");
        _sb.AppendLine(" END                                                 ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalInfo.TerminalId;
          _cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = Played.TotalRedeemable;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch
      { ; }

      return false;
    } // Trx_JackpotAmountTerminal

    public static Boolean Trx_PlaySessionSetMeters(Int64 PlaySessionId,
                                                   AccountBalance CashIn, AccountBalance Played, AccountBalance Won, AccountBalance CashOut,
                                                   Decimal HandpaysAmount,
                                                   SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.Length = 0;

        // Update PlaySession table (Redeemable & NonRedeemable amounts)

        _sb.AppendLine(" UPDATE   PLAY_SESSIONS ");
        // CASH IN 
        _sb.AppendLine("    SET   PS_NON_REDEEMABLE_CASH_IN    = @pNonRedeemableCashIn  ");
        _sb.AppendLine("        , PS_REDEEMABLE_CASH_IN        = @pTotalReCashIn ");
        _sb.AppendLine("        , PS_RE_CASH_IN                = @pReCashIn ");
        _sb.AppendLine("        , PS_PROMO_RE_CASH_IN          = @pPromoReCashIn ");
        // CASH OUT 
        _sb.AppendLine("        , PS_NON_REDEEMABLE_CASH_OUT   = @pNonRedeemableCashOut ");
        _sb.AppendLine("        , PS_REDEEMABLE_CASH_OUT       = @pTotalReCashOut ");
        _sb.AppendLine("        , PS_RE_CASH_OUT               = @pReCashOut ");
        _sb.AppendLine("        , PS_PROMO_RE_CASH_OUT         = @pPromoReCashOut ");
        // PLAYED
        _sb.AppendLine("        , PS_NON_REDEEMABLE_PLAYED     = @pNonRedeemablePlayed ");
        _sb.AppendLine("        , PS_REDEEMABLE_PLAYED         = @pRedeemablePlayed ");
        _sb.AppendLine("        , PS_PLAYED_AMOUNT             = @pTotalPlayed ");
        // WON
        _sb.AppendLine("        , PS_NON_REDEEMABLE_WON        = @pNonRedeemableWon ");
        _sb.AppendLine("        , PS_REDEEMABLE_WON            = @pRedeemableWon ");
        _sb.AppendLine("        , PS_WON_AMOUNT                = @pTotalWon ");
        // HANDPAYS
        _sb.AppendLine("        , PS_HANDPAYS_AMOUNT           = @pHandpaysAmount");
        //
        _sb.AppendLine("  WHERE   PS_PLAY_SESSION_ID           = @pPlaySessionId  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          // CASH IN
          _cmd.Parameters.Add("@pNonRedeemableCashIn", SqlDbType.Money).Value = CashIn.TotalNotRedeemable;
          _cmd.Parameters.Add("@pTotalReCashIn", SqlDbType.Money).Value = CashIn.TotalRedeemable;
          _cmd.Parameters.Add("@pReCashIn", SqlDbType.Money).Value = CashIn.Redeemable;
          _cmd.Parameters.Add("@pPromoReCashIn", SqlDbType.Money).Value = CashIn.PromoRedeemable;
          // CASH OUT
          _cmd.Parameters.Add("@pNonRedeemableCashOut", SqlDbType.Money).Value = CashOut.TotalNotRedeemable;
          _cmd.Parameters.Add("@pTotalReCashOut", SqlDbType.Money).Value = CashOut.TotalRedeemable;
          _cmd.Parameters.Add("@pReCashOut", SqlDbType.Money).Value = CashOut.Redeemable;
          _cmd.Parameters.Add("@pPromoReCashOut", SqlDbType.Money).Value = CashOut.PromoRedeemable;
          // PLAYED
          _cmd.Parameters.Add("@pNonRedeemablePlayed", SqlDbType.Money).Value = Played.TotalNotRedeemable;
          _cmd.Parameters.Add("@pRedeemablePlayed", SqlDbType.Money).Value = Played.TotalRedeemable;
          _cmd.Parameters.Add("@pTotalPlayed", SqlDbType.Money).Value = Played.TotalBalance;
          // WON
          _cmd.Parameters.Add("@pNonRedeemableWon", SqlDbType.Money).Value = Won.TotalNotRedeemable;
          _cmd.Parameters.Add("@pRedeemableWon", SqlDbType.Money).Value = Won.TotalRedeemable;
          _cmd.Parameters.Add("@pTotalWon", SqlDbType.Money).Value = Won.TotalBalance;
          // HANDPAYS
          _cmd.Parameters.Add("@pHandpaysAmount", SqlDbType.Money).Value = HandpaysAmount;
          //
          _cmd.Parameters.Add("@pPlaySessionId ", SqlDbType.BigInt).Value = PlaySessionId;

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
#if !SQL_BUSINESS_LOGIC
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
#else
      catch
      {
        ;
      }
#endif

      return false;

    } // Trx_PlaySessionClose


    public static Boolean Trx_PlaySessionTITOSetMeters(Int64 PlaySessionId,
                                                       Decimal PromoNrTicketIn,
                                                       Decimal PromoReTicketIn,
                                                       Decimal ReTicketIn,
                                                       Decimal ReTicketOut,
                                                       Decimal PromoNrTicketOut,
                                                       Decimal CashInAmount,
                                                       SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.Length = 0;

        _sb.AppendLine(" UPDATE   PLAY_SESSIONS ");
        // 
        _sb.AppendLine("    SET   PS_PROMO_NR_TICKET_IN  = @pPromoNrTicketIn ");
        _sb.AppendLine("        , PS_PROMO_RE_TICKET_IN  = @pPromoReTicketIn ");
        _sb.AppendLine("        , PS_RE_TICKET_IN        = @pReTicketIn ");
        _sb.AppendLine("        , PS_PROMO_NR_TICKET_OUT = @pPromoNrTicketOut ");
        _sb.AppendLine("        , PS_RE_TICKET_OUT       = @pReTicketOut ");
        _sb.AppendLine("        , PS_CASH_IN             = @pCashInAmount ");
        //
        _sb.AppendLine("  WHERE   PS_PLAY_SESSION_ID     = @pPlaySessionId  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPromoNrTicketIn", SqlDbType.Money).Value = PromoNrTicketIn;
          _cmd.Parameters.Add("@pPromoReTicketIn", SqlDbType.Money).Value = PromoReTicketIn;
          _cmd.Parameters.Add("@pReTicketIn", SqlDbType.Money).Value = ReTicketIn;
          _cmd.Parameters.Add("@pPromoNrTicketOut", SqlDbType.Money).Value = PromoNrTicketOut;
          _cmd.Parameters.Add("@pReTicketOut", SqlDbType.Money).Value = ReTicketOut;
          _cmd.Parameters.Add("@pCashInAmount", SqlDbType.Money).Value = CashInAmount;
          _cmd.Parameters.Add("@pPlaySessionId ", SqlDbType.BigInt).Value = PlaySessionId;

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch
      { ; }

      return false;
    } // Trx_PlaySessionClose

    public static Boolean Trx_ResetCancellableOperation(Int64 AccountId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   ACCOUNTS ");
        _sb.AppendLine("    SET   AC_CANCELLABLE_OPERATION_ID = NULL ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID               = @pAccountId   ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch
      { ; }

      return false;

    } // Trx_ResetCancellableOperation



    // PURPOSE: Initialize the fields "in session" of the account and play session of the account promotions
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - PlaySessionId:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if updated successfully. false otherwise.

    public static Boolean Trx_ResetAccountInSession(Int64 AccountId,
                                                    Int64 PlaySessionId,
                                                    out Decimal InSessionPlayed,
                                                    out Decimal InSessionWon,
                                                    out AccountBalance InSessionBalance,
                                                    SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      InSessionPlayed = 0;
      InSessionWon = 0;
      InSessionBalance = AccountBalance.Zero;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("DECLARE @OutputTable TABLE ( AC_IN_SESSION_PLAYED           MONEY");
        _sb.AppendLine("                           , AC_IN_SESSION_WON              MONEY");
        _sb.AppendLine("                           , AC_IN_SESSION_RE_BALANCE       MONEY");
        _sb.AppendLine("                           , AC_IN_SESSION_PROMO_RE_BALANCE MONEY");
        _sb.AppendLine("                           , AC_IN_SESSION_PROMO_NR_BALANCE MONEY);");
        _sb.AppendLine();
        _sb.AppendLine(" UPDATE   ACCOUNTS ");
        _sb.AppendLine("    SET   AC_IN_SESSION_PLAYED     = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_WON        = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_RE_PLAYED  = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_NR_PLAYED  = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_RE_WON     = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_NR_WON     = 0 ");
        //
        _sb.AppendLine("        , AC_IN_SESSION_RE_BALANCE       = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_BALANCE = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_BALANCE = 0 ");
        //
        _sb.AppendLine("        , AC_IN_SESSION_RE_TO_GM         = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_TO_GM   = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_TO_GM   = 0 ");
        //
        _sb.AppendLine("        , AC_IN_SESSION_RE_FROM_GM       = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_FROM_GM = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_FROM_GM = 0 ");
        //
        _sb.AppendLine("        , AC_IN_SESSION_CANCELLABLE_TRANSACTION_ID = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_RE_CANCELLABLE             = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_CANCELLABLE       = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_CANCELLABLE       = 0 ");
        //
        _sb.AppendLine(" OUTPUT   DELETED.AC_IN_SESSION_PLAYED ");
        _sb.AppendLine("        , DELETED.AC_IN_SESSION_WON ");
        _sb.AppendLine("        , DELETED.AC_IN_SESSION_RE_BALANCE ");
        _sb.AppendLine("        , DELETED.AC_IN_SESSION_PROMO_RE_BALANCE ");
        _sb.AppendLine("        , DELETED.AC_IN_SESSION_PROMO_NR_BALANCE ");
        //
        _sb.AppendLine("   INTO   @OutputTable ");

        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId ");

        if (PlaySessionId != 0)
        {
          _sb.AppendLine("    AND   AC_CURRENT_PLAY_SESSION_ID = @pPlaySessionId ");
        }

        _sb.AppendLine("IF ( @@ROWCOUNT = 1 )");
        _sb.AppendLine("   SELECT * FROM @OutputTable");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            InSessionPlayed = _reader.GetDecimal(0);
            InSessionWon = _reader.GetDecimal(1);
            InSessionBalance = new AccountBalance(_reader.GetDecimal(2), _reader.GetDecimal(3), _reader.GetDecimal(4));
          }

        }

        if (!Trx_UnlinkPromotionsInSession(AccountId, PlaySessionId, 0, SqlTrx))
        {
          return false;
        }

        return true;

      }
      catch
      { ; }

      return false;

    } // Trx_ResetAccountInSession


    public static Boolean Trx_UnlinkPromotionsInSession(Int64 AccountId,
                                                        Int64 PlaySessionId,
                                                        Int64 TransactionId,
                                                        SqlTransaction SqlTrx)
    {

      StringBuilder _sb;
      DataTable _promos;
      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   ACP_UNIQUE_ID ");
        _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(IX_acp_account_status)) ");
        _sb.AppendLine("  WHERE   ACP_ACCOUNT_ID       = @pAccountId ");
        _sb.AppendLine("    AND   ACP_STATUS           = @pStatusActive ");
        if (PlaySessionId != 0)
        {
          _sb.AppendLine("    AND   ACP_PLAY_SESSION_ID = @pPlaySessionId ");

          if (TransactionId != 0)
          {
            _sb.AppendLine("    AND   ACP_TRANSACTION_ID  = @pTransactionId ");
          }
        }
        else
        {
          // No play session indicated, but, at least, check that the play session is not null.
          _sb.AppendLine("    AND   ACP_PLAY_SESSION_ID IS NOT NULL ");
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;
          _cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = TransactionId;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _promos = new DataTable();
            _sql_da.Fill(_promos);
          }
        }

        if (_promos.Rows.Count == 0)
        {
          return true;
        }

        _sb.Length = 0;
        _sb.AppendLine(" UPDATE   ACCOUNT_PROMOTIONS ");
        _sb.AppendLine("    SET   ACP_PLAY_SESSION_ID = NULL ");
        _sb.AppendLine("        , ACP_TRANSACTION_ID  = NULL ");
        _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions)) ");
        _sb.AppendLine("  WHERE   ACP_UNIQUE_ID       = @pUniqueId ");
        _sb.AppendLine("    AND   ACP_ACCOUNT_ID      = @pAccountId ");
        _sb.AppendLine("    AND   ACP_STATUS          = @pStatusActive ");

        if (PlaySessionId != 0)
        {
          _sb.AppendLine("    AND   ACP_PLAY_SESSION_ID = @pPlaySessionId ");

          if (TransactionId != 0)
          {
            _sb.AppendLine("    AND   ACP_TRANSACTION_ID  = @pTransactionId ");
          }
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          SqlParameter _unique_id;

          _unique_id = _cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt);
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;
          _cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = TransactionId;
          foreach (DataRow _promo in _promos.Rows)
          {
            _unique_id.Value = _promo[0];

            _cmd.ExecuteNonQuery();
            // It doesn't matter if updated or not.
          }
        }

        return true;
      }
      catch
      { ; }

      return false;

    } // Trx_UnlinkPromotionsInSession

    // PURPOSE: Update the status and withhold of account promotions. If There isn't balance then the state is EXHAUSTED.
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Is correct the process?

    public static Boolean Trx_PromoRecomputeWithholdOnRecharge(Int64 AccountId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _promos;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   ACP_UNIQUE_ID ");
        _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(IX_acp_account_status)) ");
        _sb.AppendLine("  WHERE   ACP_ACCOUNT_ID  = @pAccountId ");
        _sb.AppendLine("    AND   ACP_STATUS      = @pStatusActive ");
        _sb.AppendLine("    AND   ACP_WITHHOLD    > 0 ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _promos = new DataTable();
            _sql_da.Fill(_promos);
          }
        }

        if (_promos.Rows.Count == 0)
        {
          return true;
        }

        _sb.Length = 0;
        _sb.AppendLine(" UPDATE   ACCOUNT_PROMOTIONS ");
        _sb.AppendLine("    SET   ACP_WITHHOLD    = CASE WHEN ACP_BALANCE < ACP_WITHHOLD THEN ACP_BALANCE ELSE ACP_WITHHOLD END ");
        _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions)) ");
        _sb.AppendLine("  WHERE   ACP_UNIQUE_ID   = @pUniqueId ");
        _sb.AppendLine("    AND   ACP_ACCOUNT_ID  = @pAccountId ");
        _sb.AppendLine("    AND   ACP_STATUS      = @pStatusActive ");
        _sb.AppendLine("    AND   ACP_WITHHOLD    > 0 ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          SqlParameter _unique_id;

          _unique_id = _cmd.Parameters.Add("pUniqueId", SqlDbType.BigInt);
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;

          foreach (DataRow _promo in _promos.Rows)
          {
            _unique_id.Value = _promo[0];

            _cmd.ExecuteNonQuery();
            // It doesn't matter if updated or not.
          }
        }

        return true;
      }
      catch
      { ; }

      return false;

    } // Trx_PromoRecomputeWithholdOnRecharge

    // PURPOSE: Update the status and withhold of account promotions. If There isn't balance then the state is EXHAUSTED.
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Is correct the process?

    public static Boolean Trx_PromoMarkAsExhausted(Int64 AccountId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      DataTable _promos;
      Boolean _error;
      Int32 _num_updated;

      _error = false;
      _promos = new DataTable();
      _num_updated = -1;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   ACP_UNIQUE_ID ");
        _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(IX_acp_account_status)) ");
        _sb.AppendLine("  WHERE   ACP_ACCOUNT_ID  = @pAccountId ");
        _sb.AppendLine("    AND   ACP_STATUS      = @pStatusActive ");
        // Retrieve all active promotions to decide if Cost must be reset.
        //_sb.AppendLine("    AND   ACP_BALANCE     = 0 ");
        _sb.AppendLine("    AND   ACP_PLAY_SESSION_ID IS NULL ");
        _sb.AppendLine("    AND   ACP_CREDIT_TYPE IN (@pCreditTypeNR1, @pCreditTypeNR2) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pCreditTypeNR1", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR1;
          _cmd.Parameters.Add("@pCreditTypeNR2", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR2;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
          _cmd.Parameters.Add("@pStatusExhausted", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.EXHAUSTED;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_cmd))
          {
            _sql_da.Fill(_promos);
          }
        }

        _num_updated = 0;

        if (_promos.Rows.Count == 0)
        {
          return true;
        }

        _sb.Length = 0;
        _sb.AppendLine(" UPDATE   ACCOUNT_PROMOTIONS ");
        _sb.AppendLine("    SET   ACP_STATUS      = @pStatusExhausted");
        _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions)) ");
        _sb.AppendLine("  WHERE   ACP_UNIQUE_ID   = @pUniqueId ");
        _sb.AppendLine("    AND   ACP_ACCOUNT_ID  = @pAccountId ");
        _sb.AppendLine("    AND   ACP_STATUS      = @pStatusActive ");
        _sb.AppendLine("    AND   ACP_BALANCE     = 0 ");
        _sb.AppendLine("    AND   ACP_PLAY_SESSION_ID IS NULL ");
        _sb.AppendLine("    AND   ACP_CREDIT_TYPE IN (@pCreditTypeNR1, @pCreditTypeNR2) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          SqlParameter _unique_id;

          _unique_id = _cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt);
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pCreditTypeNR1", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR1;
          _cmd.Parameters.Add("@pCreditTypeNR2", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR2;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
          _cmd.Parameters.Add("@pStatusExhausted", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.EXHAUSTED;

          foreach (DataRow _promo in _promos.Rows)
          {
            _unique_id.Value = _promo[0];

            _num_updated += _cmd.ExecuteNonQuery();
          }
        }

        return true;
      }
      catch
      {
        _error = true;
      }
      finally
      {
        if (!_error && _promos.Rows.Count == _num_updated)
        {
          // JML 13-NOV-2013 WIG-409: Cost must be reset, because are not active promotions.
          MultiPromos.Trx_AccountPromotionCost(AccountId, ACCOUNTS_PROMO_COST.RESET_ON_REDEEM, 0, SqlTrx);

        }
      }

      return false;

    } // Trx_PromoMarkAsExhausted

    // PURPOSE: Update the cost of account promotions. 
    //
    //  PARAMS:
    //     - INPUT:
    //           - PlaySessionId:
    //           - TicketOutRe:
    //           - CashInRe:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - Is correct the process?
    public static Boolean Trx_TITO_UpdatePromoNRCost(Int64 PlaySessionId, Decimal TicketOutRe, Decimal CashInRe, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Object _obj;
      Int64 _account_promotion_id;

      _account_promotion_id = 0;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("   SELECT   TOP 1 TI_ACCOUNT_PROMOTION ");
        _sb.AppendLine("     FROM   TICKETS ");
        _sb.AppendLine("    WHERE   TI_CANCELED_PLAY_SESSION_ID = @pPlaySessionId ");
        _sb.AppendLine("      AND   TI_TYPE_ID                  = @pTicketType ");
        _sb.AppendLine(" ORDER BY   TI_LAST_ACTION_DATETIME DESC ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;
          _cmd.Parameters.Add("@pTicketType", SqlDbType.Int).Value = 2; // TITO_TICKET_TYPE.PROMO_NONREDEEM;

          _obj = _cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            _account_promotion_id = (Int64)_obj;
          }
        }

        if (_account_promotion_id == 0)
        {
          return true;
        }

        return MultiPromos.Trx_UpdateAccountPromotionCost(TicketOutRe - CashInRe, _account_promotion_id, SqlTrx);
      }
      catch
      {
      }

      return false;
    } // Trx_TITO_UpdatePromoNRCost

    public static Boolean Trx_UpdateAccountPromotionCost(Decimal Amount, Int64 AccountPromotionId, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("  UPDATE   ACCOUNT_PROMOTIONS ");
      _sb.AppendLine("     SET   ACP_REDEEMABLE_COST = CASE WHEN   ACP_REDEEMABLE_COST IS NULL ");
      _sb.AppendLine("                                      THEN   @pAmount ");
      _sb.AppendLine("                                      ELSE   ACP_REDEEMABLE_COST + @pAmount END ");
      _sb.AppendLine("   WHERE   ACP_UNIQUE_ID = @pAccountPromotionId ");
      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountPromotionId", SqlDbType.BigInt).Value = AccountPromotionId;
          _cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = Amount;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch
      {
      }

      return false;
    }

    // PURPOSE: Know the "Not Redeemable" of the account promotions and actualize the play session 
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - TerminalId:
    //           - PlaySessionId:
    //           - NotRedeemable:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    public static Boolean Trx_GetPlayableBalance(Int64 AccountId,
                                             Int64 TerminalId,
                                             Int64 PlaySessionId,
                                             Int64 TransactionId,
                                             AccountBalance Current,
                                             out AccountBalance Playable,
                                             SqlTransaction SqlTrx)
    {
      DataTable _promos;
      String _vendor_dummy;

      _vendor_dummy = String.Empty;

      return Trx_GetPlayableBalance(AccountId, TerminalId, PlaySessionId, TransactionId, Current, false, _vendor_dummy, out Playable, out _promos, SqlTrx);
    }

    // PURPOSE: Know the "Not Redeemable" of the account promotions and actualize the play session 
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - TerminalId:
    //           - PlaySessionId:
    //           - NotRedeemable:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    public static Boolean Trx_GetPlayableBalance(Int64 AccountId,
                                                 Int64 TerminalId,
                                                 Int64 PlaySessionId,
                                                 Int64 TransactionId,
                                                 AccountBalance Current,
                                                 String VendorId,
                                             out AccountBalance Playable,
                                             SqlTransaction SqlTrx)
    {
      DataTable _promos;

      return Trx_GetPlayableBalance(AccountId, TerminalId, PlaySessionId, TransactionId, Current, false, VendorId, out Playable, out _promos, SqlTrx);
    }

    // PURPOSE: Know the "Not Redeemable" of the account promotions and actualize the play session 
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - TerminalId:
    //           - PlaySessionId:
    //           - NotRedeemable:
    //           - SqlTrx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none
    public static Boolean Trx_GetPlayableBalance(Int64 AccountId,
                                                 Int64 TerminalId,
                                                 Int64 PlaySessionId,
                                                 Int64 TransactionId,
                                                 AccountBalance Current,
                                                 Boolean TITOIsTransfer,
                                                 out AccountBalance Playable,
                                                 out DataTable ActivePromos,
                                                 SqlTransaction SqlTrx)
    {
      String _vendor_id_dummy;

      _vendor_id_dummy = String.Empty;

      return Trx_GetPlayableBalance(AccountId, TerminalId, PlaySessionId, TransactionId, Current, TITOIsTransfer, _vendor_id_dummy, out Playable, out ActivePromos, SqlTrx);
    }

    //------------------------------------------------------------------------------
    // PURPOSE :  Get General Param "GameGateway"."Reserved.Enabled"
    //
    //  PARAMS :
    //      - INPUT :
    //          - Trx
    //
    //      - OUTPUT :
    //          - Mode
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    //
    private static Boolean Trx_GetGameGatewayReservedEnabled(out Int32 Mode, Int64 AccountId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      Mode = 0;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" DECLARE @ModeReserved AS INT                                                     ");
        _sb.AppendLine("                                                                                  ");
        _sb.AppendLine(" SET @ModeReserved = (SELECT ISNULL((SELECT   CAST (GP_KEY_VALUE AS INT)          ");
        _sb.AppendLine("                                       FROM   GENERAL_PARAMS                      ");
        _sb.AppendLine("                                      WHERE   GP_GROUP_KEY   = 'GameGateway'      ");
        _sb.AppendLine("                                        AND   GP_SUBJECT_KEY = 'Enabled'          ");
        _sb.AppendLine("                                        AND   ISNUMERIC(GP_KEY_VALUE) = 1 ), 0))  ");
        _sb.AppendLine("                                                                                  ");
        _sb.AppendLine(" IF (@ModeReserved = 1)                                                           ");
        _sb.AppendLine(" SET @ModeReserved = (SELECT ISNULL((SELECT   CAST (GP_KEY_VALUE AS INT)          ");
        _sb.AppendLine("                                       FROM   GENERAL_PARAMS                      ");
        _sb.AppendLine("                                      WHERE   GP_GROUP_KEY   = 'GameGateway'      ");
        _sb.AppendLine("                                        AND   GP_SUBJECT_KEY = 'Reserved.Enabled' ");
        _sb.AppendLine("                                        AND   (ISNUMERIC(GP_KEY_VALUE) = 1        ");
        _sb.AppendLine("                                            OR ISNUMERIC(GP_KEY_VALUE) = 2) ), 0))");
        _sb.AppendLine("                                                                                  ");
        _sb.AppendLine(" IF (@ModeReserved = 1)                                                           ");
        _sb.AppendLine("   SET @ModeReserved = (SELECT  CAST(AC_MODE_RESERVED AS INT)                     ");
        _sb.AppendLine("                          FROM   ACCOUNTS                                         ");
        _sb.AppendLine("                         WHERE   AC_ACCOUNT_ID = @pAccountId)                     ");
        _sb.AppendLine("                                                                                  ");
        _sb.AppendLine(" SELECT @ModeReserved                                                             ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          Mode = (Int32)_cmd.ExecuteScalar();
        }

        return true;
      }
      catch
      {

      }

      return false;

    } // Trx_GetExternalLoyatyProgramMode

    public static Boolean Trx_GetPlayableBalance(Int64 AccountId,
                                                 Int64 TerminalId,
                                                 Int64 PlaySessionId,
                                                 Int64 TransactionId,
                                                 AccountBalance Current,
                                                 Boolean TITOIsTransfer,
                                                 String VendorId, 
                                                 out AccountBalance Playable,
                                                 out DataTable ActivePromos,
                                                 SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      ACCOUNT_PROMO_CREDIT_TYPE _credit_type;
      Decimal _promo_balance;
      Boolean _only_redeemable;
      String _provider;
      DataRow _row;
      Int32 _mode;
      String _fbm_provider;

      ActivePromos = new DataTable();
      _fbm_provider = String.Empty;

      // All the cashable credits are playable
      // 04-NOV-2015  JCA    Product Backlog Item 6145: BonoPLUS: Reserved Credit

      Playable = new AccountBalance(Math.Max(0, Current.Redeemable),
                                       Math.Max(0, Current.PromoRedeemable),
                                       0);  // Zero, NotRedeemable will be added promotion by promotion

      
      Trx_GetFBMReservedCreditEnabled(out _fbm_provider, SqlTrx);

      if ((Trx_GetGameGatewayReservedEnabled(out _mode, AccountId, SqlTrx)
          && (_mode == (Int16)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_WITH_BUCKETS
              || _mode == (Int16)MultiPromos.GAME_GATEWAY_RESERVED_MODE.GAME_GATEWAY_RESERVED_MODE_MIXED))
          || (!String.IsNullOrEmpty(_fbm_provider) && !String.IsNullOrEmpty(VendorId) && _fbm_provider != VendorId))
        {
          //JCA & FJC Fixed Bug 7779:Al insertar una tarjeta con cr�ditos No redimibles no se transfiere al terminal
          Playable = new AccountBalance(Current.BalanceEGM);
          Playable.PromoNotRedeemable = 0;// Zero, NotRedeemable will be added promotion by promotion
          Playable.CustodyProvision = 0; //  Zero, Not substract only in the refund
        }
      else
      {
        if (!String.IsNullOrEmpty(_fbm_provider) && _fbm_provider == VendorId) //FBM
        {
          Playable = new AccountBalance(Current.BalanceEGMReservedFBM);
          Playable.CustodyProvision = 0; //  Zero, Not substract only in the refund
        }
      }

      if (PlaySessionId != HANDPAY_VIRTUAL_PLAY_SESSION_ID)
      {
        // Non Cashable Exists?
        // NR Promotions can have balance 0, but with withhold. Don't exit here, continue to mark them.
        if (Current.PromoNotRedeemable <= 0)
        {
          return true;
        }
      }

      _provider = "";
      _only_redeemable = false;
      _sb = new StringBuilder();

      try
      {
        _sb.Length = 0;
        _sb.AppendLine("SELECT   PV_NAME ");
        _sb.AppendLine("       , PV_ONLY_REDEEMABLE ");
        _sb.AppendLine("  FROM   PROVIDERS INNER JOIN TERMINALS ON TE_PROV_ID = PV_ID ");
        _sb.AppendLine(" WHERE   TE_TERMINAL_ID = @pTerminalId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.BigInt).Value = TerminalId;
          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }
            _provider = (String)_reader.GetString(0);
            _only_redeemable = (Boolean)_reader.GetBoolean(1);
          }
        }

        if (_only_redeemable)
        {
          return true;
        }

        // Working Table
        ActivePromos.Columns.Add("ACP_UNIQUE_ID", Type.GetType("System.Int64"));
        ActivePromos.Columns.Add("ACP_CREDIT_TYPE", Type.GetType("System.Int32"));
        ActivePromos.Columns.Add("ACP_BALANCE", Type.GetType("System.Decimal"));

        // Get Promotions
        _sb.Length = 0;
        _sb.AppendLine(" SELECT   ACP_UNIQUE_ID ");
        _sb.AppendLine("        , CASE WHEN PM_RESTRICTED_TO_TERMINAL_LIST IS NOT NULL THEN ");
        _sb.AppendLine("                 ISNULL((SELECT 1 FROM TERMINAL_GROUPS WHERE TG_TERMINAL_ID = @pTerminalId AND TG_ELEMENT_ID = ACP_PROMO_ID AND TG_ELEMENT_TYPE = @pElementTypePromotion), 0) ");
        _sb.AppendLine("               ELSE 1 END ");
        _sb.AppendLine("        , ACP_BALANCE ");
        //
        _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(IX_acp_account_status)) ");
        _sb.AppendLine("   INNER  JOIN PROMOTIONS ON PM_PROMOTION_ID = ACP_PROMO_ID     ");
        //
        _sb.AppendLine("  WHERE   ACP_ACCOUNT_ID       = @pAccountId ");        // IX_ACP_ACCOUNT_STATUS.ACP_ACCOUNT_ID
        _sb.AppendLine("    AND   ACP_STATUS           = @pStatusActive ");     // IX_ACP_ACCOUNT_STATUS.ACP_STATUS
        //
        _sb.AppendLine("    AND   ACP_CREDIT_TYPE IN   (@pTypeNR1, @pTypeNR2) ");
        // Retenido
        _sb.AppendLine("    AND ( ( ACP_BALANCE > 0 ) OR ( ACP_BALANCE = 0 AND ACP_WITHHOLD > 0 )) ");
        // Not In Session
        _sb.AppendLine("    AND   ACP_PLAY_SESSION_ID IS NULL ");
        _sb.AppendLine("  ORDER BY ACP_UNIQUE_ID ASC ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
          _cmd.Parameters.Add("@pTypeNR1", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR1;
          _cmd.Parameters.Add("@pTypeNR2", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR2;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pElementTypePromotion", SqlDbType.Int).Value = EXPLOIT_ELEMENT_TYPE.PROMOTION_RESTRICTED;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            while (_reader.Read())
            {

              if (_reader.GetInt32(1) == 0)
              {
                continue;
              }

              _row = ActivePromos.NewRow();
              _row[0] = _reader.GetInt64(0);
              _row[2] = _reader.GetDecimal(2);
              ActivePromos.Rows.Add(_row);
            }
          }
        }

        _sb.Length = 0;
        _sb.AppendLine("UPDATE   ACCOUNT_PROMOTIONS ");
        _sb.AppendLine("   SET   ACP_PLAY_SESSION_ID = @pPlaySessionId ");
        _sb.AppendLine("       , ACP_TRANSACTION_ID  = @pTransactionId ");
        _sb.AppendLine("OUTPUT   DELETED.ACP_CREDIT_TYPE ");
        _sb.AppendLine("       , DELETED.ACP_BALANCE     ");
        _sb.AppendLine("  FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions)) ");
        _sb.AppendLine(" WHERE   ACP_UNIQUE_ID       = @pUniqueId ");   // PK
        _sb.AppendLine("   AND   ACP_ACCOUNT_ID      = @pAccountId ");
        _sb.AppendLine("   AND   ACP_STATUS          = @pStatusActive ");
        _sb.AppendLine("   AND   ACP_CREDIT_TYPE IN   (@pTypeNR1, @pTypeNR2) ");
        _sb.AppendLine("   AND ( ( ACP_BALANCE > 0 ) OR ( ACP_BALANCE = 0 AND ACP_WITHHOLD > 0 )) ");
        _sb.AppendLine("   AND   ACP_PLAY_SESSION_ID IS NULL ");


        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          SqlParameter _unique_id;

          _unique_id = _cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt);
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
          _cmd.Parameters.Add("@pTypeNR1", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR1;
          _cmd.Parameters.Add("@pTypeNR2", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR2;

          if (TITOIsTransfer)
          {
            _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = DBNull.Value;
            _cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = DBNull.Value;
          }
          else
          {
            _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;
            _cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = TransactionId;
          }

          foreach (DataRow _promo in ActivePromos.Rows)
          {
            _unique_id.Value = _promo[0];

            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                // Not an error, conditions changed ...
                continue;
              }

              _credit_type = (ACCOUNT_PROMO_CREDIT_TYPE)_reader.GetInt32(0);
              _promo_balance = _reader.GetDecimal(1);
              _promo_balance = Math.Max(0, _promo_balance);

              switch (_credit_type)
              {
                case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
                case ACCOUNT_PROMO_CREDIT_TYPE.NR2:
                  Playable.PromoNotRedeemable += _promo_balance;
                  break;

                case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
                case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
                default:
                  break;
              }
            }
          }
        }

        Playable.PromoNotRedeemable = Math.Max(0, Math.Min(Playable.PromoNotRedeemable, Current.PromoNotRedeemable));

        return true;
      }
      catch
      { ; }

      return false;

    } // Trx_GetPlayableBalance

    // PURPOSE: 
    //
    //  PARAMS:
    //     - INPUT:
    //           - PlaySessionId:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    public static bool Trx_UpdatePlayedAndWon(Int64 PlaySessionId, out AccountBalance InSessionBalance, SqlTransaction Trx)
    {
      InSessionParameters _params;
      StringBuilder _err_msg;

      _err_msg = new StringBuilder();
      InSessionBalance = AccountBalance.Zero;

      if (PlaySessionId == 0)
      {
        return false;
      }

      if (!Trx_GetDeltaPlayedWon(PlaySessionId, out _params, Trx))
      {
        return false;
      }

      if (!Trx_UpdatePlayedAndWon(_params, _err_msg, Trx))
      {
        return false;
      }

      InSessionBalance = _params.Balance;

      return true;
    } // Trx_UpdatePlayedAndWon

    private static Boolean Trx_GetCreditsPlayMode(out Terminal.CreditsPlayMode PlayMode, SqlTransaction Trx)
    {
      StringBuilder _sb;

      PlayMode = Terminal.CreditsPlayMode.NotRedeemableLast;

      try
      {
        _sb = new StringBuilder();
        _sb.Length = 0;
        _sb.AppendLine("SELECT ISNULL ( ( SELECT CAST (GP_KEY_VALUE AS INT) FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'Credits' AND GP_SUBJECT_KEY = 'PlayMode' AND ISNUMERIC(GP_KEY_VALUE) = 1 ), 0 ) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          PlayMode = (Terminal.CreditsPlayMode)((Int32)_cmd.ExecuteScalar());
        }

        switch (PlayMode)
        {
          case Terminal.CreditsPlayMode.NotRedeemableLast:
          case Terminal.CreditsPlayMode.PromotionalsFirst:
            break;

          default:
            PlayMode = Terminal.CreditsPlayMode.NotRedeemableLast;
            break;
        }

        return true;
      }
      catch
      {
      }
      return false;
    }

    // PURPOSE: Compute the played and won and update the account and promotion.
    //
    //  PARAMS:
    //     - INPUT:
    //           - InSessionParameters:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if computed and update was successfully. false otherwise.

    public static bool Trx_UpdatePlayedAndWon(InSessionParameters InSessionParameters, StringBuilder ErrorMsg, SqlTransaction Trx)
    {
      return Trx_UpdatePlayedAndWon(InSessionParameters, ErrorMsg, false, Trx);
    }

    public static bool Trx_UpdatePlayedAndWon(InSessionParameters InSessionParameters, StringBuilder ErrorMsg, Boolean AddInSession, SqlTransaction Trx)
    {
      StringBuilder _sb;
      DataRow[] _modified_rows;
      Terminal.CreditsPlayMode _credits_play_mode;
      Boolean _update_promos;

      _update_promos = InSessionParameters.DeltaPlayed != 0 || InSessionParameters.DeltaWon != 0;
      if (InSessionParameters.IsMico2)
      {
        _update_promos = _update_promos || InSessionParameters.DeltaBalanceFoundInSession.PromoNotRedeemable > 0;
        _update_promos = _update_promos || InSessionParameters.DeltaBalanceFoundInSession.PromoRedeemable > 0;
      }

      try
      {
        _sb = new StringBuilder();

        if (_update_promos)
        {
          if (!Trx_GetCreditsPlayMode(out _credits_play_mode, Trx))
          {
            ErrorMsg.AppendLine("Trx_GetCreditsPlayMode failed.");

            return false;
          }

          if (InSessionParameters.IsMico2)
          {
            _credits_play_mode = Terminal.CreditsPlayMode.PromotionalsFirst;
          }

          _sb.Length = 0;
          _sb.AppendLine("   SELECT   ACP_BALANCE ");
          _sb.AppendLine("          , ACP_WITHHOLD ");
          _sb.AppendLine("          , ACP_WONLOCK ");
          _sb.AppendLine("          , ACP_PLAYED ");
          _sb.AppendLine("          , ACP_WON ");
          _sb.AppendLine("          , ACP_CREDIT_TYPE ");
          _sb.AppendLine("          , ACP_UNIQUE_ID ");
          _sb.AppendLine("     FROM   ACCOUNT_PROMOTIONS WITH (INDEX(IX_acp_account_status)) ");
          _sb.AppendLine("    WHERE   ACP_ACCOUNT_ID      = @pAccountId ");
          _sb.AppendLine("      AND   ACP_STATUS          = @pStatusActive ");
          _sb.AppendLine("      AND   ACP_PLAY_SESSION_ID = @pPlaySessionId ");
          _sb.AppendLine(" ORDER BY   ACP_UNIQUE_ID ASC ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = InSessionParameters.PlaySessionId;
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = InSessionParameters.AccountId;
            _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = (Int32)ACCOUNT_PROMO_STATUS.ACTIVE;

            InSessionParameters.AccountPromotion = new DataTable();
            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              InSessionParameters.AccountPromotion.Load(_reader);
            }
          }

          //
          // Simulate Played/Won by System
          //
          switch (_credits_play_mode)
          {
            case Terminal.CreditsPlayMode.NotRedeemableLast: // Wigos Default
              Decimal _delta_played;
              Decimal _delta_won;
              Decimal _nr_ratio;
              Decimal _nr_replay;

              _nr_ratio = 0;
              if (InSessionParameters.Balance.TotalBalance > 0 && InSessionParameters.Balance.TotalNotRedeemable > 0)
              {
                _nr_ratio = InSessionParameters.Balance.TotalNotRedeemable / InSessionParameters.Balance.TotalBalance;
              }

              // 1.- PLAY the initial balance
              // 2.- When (OnlyNR) Replay NR
              // 3.- WON 
              // 4.- PLAY

              // 1.- PLAY the initial balance
              // PLAYED:   NR2 -> PromoRE -> RE -> NR1
              PlayPromoPrizeOnly(ACCOUNT_PROMO_CREDIT_TYPE.NR1, InSessionParameters);
              PlayPromoNotRedeemableBalance(ACCOUNT_PROMO_CREDIT_TYPE.NR2, InSessionParameters);
              PlayRedeemable(InSessionParameters);
              // Don't play NR1 here, it will be played later ... 
              // PlayPromoNotRedeemableBalance(ACCOUNT_PROMO_CREDIT_TYPE.NR1, InSessionParameters);

              // 2.- When (NR present) Replay NR
              // Compute the "Rejuego"
              _nr_replay = Math.Max(0, Math.Min(InSessionParameters.DeltaPlayed, InSessionParameters.DeltaWon));
              _nr_replay = Math.Round(_nr_replay * _nr_ratio, 2, MidpointRounding.AwayFromZero);

              if (_nr_replay > 0)
              {
                // Save 'delta' without the 'Rejuego'
                _delta_played = InSessionParameters.DeltaPlayed - _nr_replay;
                _delta_won = InSessionParameters.DeltaWon - _nr_replay;

                InSessionParameters.DeltaPlayed = _nr_replay;
                InSessionParameters.DeltaWon = _nr_replay;
                WonPromoNotRedeemable(InSessionParameters, true);
                PlayPromoNotRedeemableBalance(ACCOUNT_PROMO_CREDIT_TYPE.NR1, InSessionParameters);

                // Restore the previously computed 'delta'
                // Note: Delta MUST be computed as NewDelta = OldDelta + 'SavedDelta', 
                //       because it is possible that 'Delta' has some remaining value
                InSessionParameters.DeltaPlayed = InSessionParameters.DeltaPlayed + _delta_played;
                InSessionParameters.DeltaWon = InSessionParameters.DeltaWon + _delta_won;
              }

              // 3.- WON 
              WonPromoNotRedeemable(InSessionParameters, false);
              WonRedeemable(InSessionParameters);
              // 4.- PLAY
              // PLAYED:   NR2 -> PromoRE -> RE -> NR1
              PlayPromoPrizeOnly(ACCOUNT_PROMO_CREDIT_TYPE.NR1, InSessionParameters);
              PlayPromoNotRedeemableBalance(ACCOUNT_PROMO_CREDIT_TYPE.NR2, InSessionParameters);
              PlayRedeemable(InSessionParameters);
              PlayPromoNotRedeemableBalance(ACCOUNT_PROMO_CREDIT_TYPE.NR1, InSessionParameters);

              break;

            case Terminal.CreditsPlayMode.PromotionalsFirst: // Like SAS
              // WON
              WonRedeemable(InSessionParameters);
              // PLAYED: NR2 -> NR1 -> PromoRE -> RE
              PlayPromoNotRedeemableBalance(ACCOUNT_PROMO_CREDIT_TYPE.NR2, InSessionParameters);
              PlayPromoNotRedeemableBalance(ACCOUNT_PROMO_CREDIT_TYPE.NR1, InSessionParameters);
              PlayRedeemable(InSessionParameters);

              if (InSessionParameters.IsMico2)
              {
                SpentRemainingInSessionPromoNR(ACCOUNT_PROMO_CREDIT_TYPE.NR2, InSessionParameters);
                SpentRemainingInSessionPromoNR(ACCOUNT_PROMO_CREDIT_TYPE.NR1, InSessionParameters);
                SpentRemainingInSessionPromoRE(ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE, InSessionParameters);
              }

              break;

            default:
              ErrorMsg.AppendLine(String.Format("Unknown CreditPlayMode: {0}", _credits_play_mode));

              return false;
          }

          // DeltaPlayed & DeltaWon contain the "error" amounts 
          InSessionParameters.DeltaPlayed = Math.Max(0, InSessionParameters.DeltaPlayed);
          InSessionParameters.DeltaWon = Math.Max(0, InSessionParameters.DeltaWon);

          // UPDATE NOT REDEEMABLE IN SESSION
          _modified_rows = InSessionParameters.AccountPromotion.Select("", "", DataViewRowState.ModifiedCurrent);
          if (_modified_rows.Length > 0)
          {
            _sb.Length = 0;
            _sb.AppendLine("UPDATE   ACCOUNT_PROMOTIONS ");
            _sb.AppendLine("   SET   ACP_BALANCE         = @pBalance ");
            _sb.AppendLine("       , ACP_WON             = @pWon ");
            _sb.AppendLine("       , ACP_PLAYED          = @pPlayed ");
            _sb.AppendLine("  FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions)) ");
            _sb.AppendLine(" WHERE   ACP_UNIQUE_ID       = @pUniqueId ");
            _sb.AppendLine("   AND   ACP_ACCOUNT_ID      = @pAccountId ");
            _sb.AppendLine("   AND   ACP_STATUS          = @pStatusActive ");
            _sb.AppendLine("   AND   ACP_PLAY_SESSION_ID = @pPlaySessionId ");

            using (SqlDataAdapter _da = new SqlDataAdapter())
            {
              _da.UpdateCommand = new SqlCommand(_sb.ToString(), Trx.Connection, Trx);
              _da.UpdateCommand.Parameters.Add("@pBalance", SqlDbType.Money).SourceColumn = "ACP_BALANCE";
              _da.UpdateCommand.Parameters.Add("@pWon", SqlDbType.Money).SourceColumn = "ACP_WON";
              _da.UpdateCommand.Parameters.Add("@pPlayed", SqlDbType.Money).SourceColumn = "ACP_PLAYED";
              _da.UpdateCommand.Parameters.Add("@pUniqueId", SqlDbType.BigInt).SourceColumn = "ACP_UNIQUE_ID";
              _da.UpdateCommand.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = InSessionParameters.AccountId;
              _da.UpdateCommand.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = (Int32)ACCOUNT_PROMO_STATUS.ACTIVE;
              _da.UpdateCommand.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = InSessionParameters.PlaySessionId;

              _da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
              _da.UpdateBatchSize = 1;
              _da.ContinueUpdateOnError = true;

              int _num_updated;

              _num_updated = _da.Update(_modified_rows);
              if (_num_updated != _modified_rows.Length)
              {
                ErrorMsg.AppendLine(String.Format("Can't update account promotions. Updated {0} of {1}", _num_updated, _modified_rows.Length));
                foreach (DataRow _row in _modified_rows)
                {
                  ErrorMsg.AppendLine(String.Format("Promotion UniqueId: {0}, CreditType: {1}, Balance: {2}, Played: {3}, Won: {4}",
                                                    _row["ACP_UNIQUE_ID"], _row["ACP_CREDIT_TYPE"], _row["ACP_BALANCE"], _row["ACP_PLAYED"], _row["ACP_WON"]));


                  if (_row.HasErrors)
                  {
                    ErrorMsg.AppendLine("Error: " + _row.RowError);
                  }
                }
                ErrorMsg.AppendLine("Query.CommandText: " + _da.UpdateCommand.CommandText);
                foreach (SqlParameter _p in _da.UpdateCommand.Parameters)
                {
                  ErrorMsg.AppendLine(String.Format("Parameter {0} = {1}", _p.ParameterName, _p.Value));
                }


                return false;
              }
            }

          } // if (_modified_rows.Length

        } // if (InSessionParameters.DeltaPlayed != 0 || InSessionParameters.DeltaWon != 0)

        if (InSessionParameters.IsMico2)
        {
          InSessionParameters.Balance = InSessionParameters.BalanceToAccount;
        }

        // UPDATE REDEEMABLE IN SESSION
        _sb.Length = 0;
        _sb.AppendLine("DECLARE @OutputTable TABLE ( AC_IN_SESSION_RE_PLAYED MONEY");
        _sb.AppendLine("                           , AC_IN_SESSION_NR_PLAYED MONEY");
        _sb.AppendLine("                           , AC_IN_SESSION_RE_WON    MONEY");
        _sb.AppendLine("                           , AC_IN_SESSION_NR_WON    MONEY );");
        _sb.AppendLine();

        _sb.AppendLine(" UPDATE   ACCOUNTS ");

        if (AddInSession)
        {
          _sb.AppendLine("    SET   AC_IN_SESSION_RE_BALANCE       = AC_IN_SESSION_RE_BALANCE       + @pInSessionReBalance      ");
          _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_BALANCE = AC_IN_SESSION_PROMO_RE_BALANCE + @pInSessionPromoReBalance ");
          _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_BALANCE = AC_IN_SESSION_PROMO_NR_BALANCE + @pInSessionPromoNrBalance ");

        }
        else
        {
          _sb.AppendLine("    SET   AC_IN_SESSION_RE_BALANCE       = @pInSessionReBalance      ");
          _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_BALANCE = @pInSessionPromoReBalance ");
          _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_BALANCE = @pInSessionPromoNrBalance ");
        }

        _sb.AppendLine("        , AC_IN_SESSION_RE_PLAYED        = AC_IN_SESSION_RE_PLAYED + @pInSessionRePlayed ");
        _sb.AppendLine("        , AC_IN_SESSION_NR_PLAYED        = AC_IN_SESSION_NR_PLAYED + @pInSessionNrPlayed ");
        _sb.AppendLine("        , AC_IN_SESSION_RE_WON           = AC_IN_SESSION_RE_WON    + @pInSessionReWon ");
        _sb.AppendLine("        , AC_IN_SESSION_NR_WON           = AC_IN_SESSION_NR_WON    + @pInSessionNrWon ");
        _sb.AppendLine("        , AC_IN_SESSION_PLAYED           = AC_IN_SESSION_PLAYED    - @pInSessionPlayed ");
        _sb.AppendLine("        , AC_IN_SESSION_WON              = AC_IN_SESSION_WON       - @pInSessionWon ");

        _sb.AppendLine(" OUTPUT   INSERTED.AC_IN_SESSION_RE_PLAYED  ");
        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_NR_PLAYED  ");
        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_RE_WON     ");
        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_NR_WON     ");

        //
        _sb.AppendLine("   INTO   @OutputTable ");

        _sb.AppendLine("   FROM   ACCOUNTS ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId ");

        _sb.AppendLine("IF ( @@ROWCOUNT = 1 )");
        _sb.AppendLine("   SELECT * FROM @OutputTable");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = InSessionParameters.AccountId;
          _cmd.Parameters.Add("@pInSessionReBalance", SqlDbType.Money).Value = InSessionParameters.Balance.Redeemable;
          _cmd.Parameters.Add("@pInSessionPromoReBalance", SqlDbType.Money).Value = InSessionParameters.Balance.PromoRedeemable;
          _cmd.Parameters.Add("@pInSessionPromoNrBalance", SqlDbType.Money).Value = InSessionParameters.Balance.PromoNotRedeemable;
          _cmd.Parameters.Add("@pInSessionRePlayed", SqlDbType.Money).Value = InSessionParameters.DeltaPlayedRe;
          _cmd.Parameters.Add("@pInSessionNrPlayed", SqlDbType.Money).Value = InSessionParameters.DeltaPlayedNr;
          _cmd.Parameters.Add("@pInSessionReWon", SqlDbType.Money).Value = InSessionParameters.DeltaWonRe;
          _cmd.Parameters.Add("@pInSessionNrWon", SqlDbType.Money).Value = InSessionParameters.DeltaWonNr;
          // DeltaPlayed & DeltaWon contain the "error" amounts 
          _cmd.Parameters.Add("@pInSessionPlayed", SqlDbType.Money).Value = InSessionParameters.DeltaPlayed;
          _cmd.Parameters.Add("@pInSessionWon", SqlDbType.Money).Value = InSessionParameters.DeltaWon;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              ErrorMsg.AppendLine(String.Format("Can't update 'in session' account, Id: {0}", InSessionParameters.AccountId));

              return false;
            }

            InSessionParameters.Played = new AccountBalance(_reader.GetDecimal(0), 0, _reader.GetDecimal(1));
            InSessionParameters.Won = new AccountBalance(_reader.GetDecimal(2), 0, _reader.GetDecimal(3));
          }
        }

        return true;

      }
      catch (Exception _ex)
      {
        ErrorMsg.AppendLine(_ex.Message);
      }

      return false;

    } // Trx_UpdatePlayedAndWon

    // PURPOSE: 
    //
    //  PARAMS:
    //     - INPUT:
    //           - PlaySessionId:
    //           - InSessionParameters:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    public static Boolean Trx_GetDeltaPlayedWon(Int64 PlaySessionId, out InSessionParameters InSessionParameters, SqlTransaction Trx)
    {
      StringBuilder _sb;

      InSessionParameters = new InSessionParameters();

      _sb = new StringBuilder();
      _sb.AppendLine("DECLARE @OutputTable TABLE ( DELTA_PLAYED                   MONEY");
      _sb.AppendLine("                           , DELTA_WON                      MONEY");
      _sb.AppendLine("                           , AC_IN_SESSION_RE_BALANCE       MONEY");
      _sb.AppendLine("                           , AC_IN_SESSION_PROMO_RE_BALANCE MONEY");
      _sb.AppendLine("                           , AC_IN_SESSION_PROMO_NR_BALANCE MONEY");
      _sb.AppendLine("                           , AC_ACCOUNT_ID                  BIGINT);");
      _sb.AppendLine();
      _sb.AppendLine(" UPDATE   ACCOUNTS ");
      _sb.AppendLine("    SET   AC_IN_SESSION_PLAYED = CASE WHEN (PS_PLAYED_AMOUNT > AC_IN_SESSION_PLAYED) THEN PS_PLAYED_AMOUNT ELSE AC_IN_SESSION_PLAYED END ");
      _sb.AppendLine("        , AC_IN_SESSION_WON    = CASE WHEN (PS_WON_AMOUNT    > AC_IN_SESSION_WON)    THEN PS_WON_AMOUNT    ELSE AC_IN_SESSION_WON    END ");
      _sb.AppendLine(" OUTPUT   INSERTED.AC_IN_SESSION_PLAYED - DELETED.AC_IN_SESSION_PLAYED AS DELTA_PLAYED ");
      _sb.AppendLine("        , INSERTED.AC_IN_SESSION_WON    - DELETED.AC_IN_SESSION_WON    AS DELTA_WON    ");
      _sb.AppendLine("        , DELETED.AC_IN_SESSION_RE_BALANCE ");
      _sb.AppendLine("        , DELETED.AC_IN_SESSION_PROMO_RE_BALANCE ");
      _sb.AppendLine("        , DELETED.AC_IN_SESSION_PROMO_NR_BALANCE ");
      _sb.AppendLine("        , DELETED.AC_ACCOUNT_ID ");
      //
      _sb.AppendLine("   INTO   @OutputTable ");
      _sb.AppendLine("   FROM   PLAY_SESSIONS AS PS INNER JOIN ACCOUNTS AS ACC ON PS.PS_ACCOUNT_ID = ACC.AC_ACCOUNT_ID ");
      _sb.AppendLine("  WHERE   PS.PS_PLAY_SESSION_ID = @pPlaySessionId ");
      _sb.AppendLine();
      _sb.AppendLine("IF ( @@ROWCOUNT = 1 )");
      _sb.AppendLine("   SELECT * FROM @OutputTable");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }
            InSessionParameters = new InSessionParameters();
            InSessionParameters.DeltaPlayed = _reader.GetDecimal(0);
            InSessionParameters.DeltaWon = _reader.GetDecimal(1);
            InSessionParameters.Balance = new AccountBalance(_reader.GetDecimal(2), _reader.GetDecimal(3), _reader.GetDecimal(4));
            InSessionParameters.AccountId = _reader.GetInt64(5);
            InSessionParameters.PlaySessionId = PlaySessionId;

            return true;
          }
        }

      }
      catch
      {
        ;
      }

      return false;

    } // Trx_GetInSessionParameters
    public static Boolean Trx_GetDeltaPlayedWon3GS(Int64 PlaySessionId, out InSessionParameters InSessionParameters, SqlTransaction Trx)
    {
      StringBuilder _sb;

      InSessionParameters = new InSessionParameters();

      _sb = new StringBuilder();
      _sb.AppendLine("DECLARE @OutputTable TABLE ( DELTA_PLAYED                   MONEY");
      _sb.AppendLine("                           , DELTA_WON                      MONEY");
      _sb.AppendLine("                           , AC_IN_SESSION_RE_BALANCE       MONEY");
      _sb.AppendLine("                           , AC_IN_SESSION_PROMO_RE_BALANCE MONEY");
      _sb.AppendLine("                           , AC_IN_SESSION_PROMO_NR_BALANCE MONEY");
      _sb.AppendLine("                           , BILL_IN                        MONEY");
      _sb.AppendLine("                           , AC_ACCOUNT_ID                  BIGINT);");
      _sb.AppendLine();
      _sb.AppendLine(" UPDATE   ACCOUNTS ");
      _sb.AppendLine("    SET   AC_IN_SESSION_PLAYED = CASE WHEN (PS_PLAYED_AMOUNT > AC_IN_SESSION_PLAYED) THEN PS_PLAYED_AMOUNT ELSE AC_IN_SESSION_PLAYED END ");
      _sb.AppendLine("        , AC_IN_SESSION_WON    = CASE WHEN (PS_WON_AMOUNT    > AC_IN_SESSION_WON)    THEN PS_WON_AMOUNT    ELSE AC_IN_SESSION_WON    END ");
      _sb.AppendLine(" OUTPUT   INSERTED.AC_IN_SESSION_PLAYED - DELETED.AC_IN_SESSION_PLAYED AS DELTA_PLAYED ");
      _sb.AppendLine("        , INSERTED.AC_IN_SESSION_WON    - DELETED.AC_IN_SESSION_WON    AS DELTA_WON    ");
      _sb.AppendLine("        , DELETED.AC_IN_SESSION_RE_BALANCE ");
      _sb.AppendLine("        , DELETED.AC_IN_SESSION_PROMO_RE_BALANCE ");
      _sb.AppendLine("        , DELETED.AC_IN_SESSION_PROMO_NR_BALANCE ");
      _sb.AppendLine("        , ps.ps_cash_in ");
      _sb.AppendLine("        , DELETED.AC_ACCOUNT_ID ");
      //
      _sb.AppendLine("   INTO   @OutputTable ");
      _sb.AppendLine("   FROM   PLAY_SESSIONS AS PS INNER JOIN ACCOUNTS AS ACC ON PS.PS_ACCOUNT_ID = ACC.AC_ACCOUNT_ID ");
      _sb.AppendLine("  WHERE   PS.PS_PLAY_SESSION_ID = @pPlaySessionId ");
      _sb.AppendLine();
      _sb.AppendLine("IF ( @@ROWCOUNT = 1 )");
      _sb.AppendLine("   SELECT * FROM @OutputTable");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }
            InSessionParameters = new InSessionParameters();
            InSessionParameters.DeltaPlayed = _reader.GetDecimal(0);
            InSessionParameters.DeltaWon = _reader.GetDecimal(1);
            InSessionParameters.Balance = new AccountBalance(_reader.GetDecimal(2) + _reader.GetDecimal(5), _reader.GetDecimal(3), _reader.GetDecimal(4));
            InSessionParameters.AccountId = _reader.GetInt64(6);
            InSessionParameters.PlaySessionId = PlaySessionId;

            return true;
          }
        }

      }
      catch
      {
        ;
      }

      return false;

    } // Trx_GetInSessionParameters

    public static InSessionParameters TITO_GetDeltaPlayedWon(EndSessionInput Input)
    {
      InSessionParameters _params;

      _params = new InSessionParameters();
      _params.AccountId = Input.AccountId;

      _params.DeltaPlayed = Input.Meters.PlayedAmount;
      _params.DeltaPlayedRe = Input.Meters.PlayedReedemable + Input.Meters.PlayedNonRestrictedAmount;
      _params.DeltaPlayedNr = Input.Meters.PlayedRestrictedAmount;

      _params.DeltaWon = Input.Meters.WonAmount;
      _params.DeltaWonRe = Input.Meters.WonAmount;
      _params.DeltaWonNr = 0;

      _params.Played = new AccountBalance(_params.DeltaPlayedRe, 0, _params.DeltaPlayedNr);
      _params.Won = new AccountBalance(_params.DeltaWonRe, 0, _params.DeltaWonNr);

      return _params;

    } // TITO_GetDeltaPlayedWon

    // PURPOSE: Update the balance, redeemable and not redeemable of the account.
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - RedimibleBalance: The redimible to aggregate in the balance
    //           - NotRedeemableToAdd: The not redimible to aggregate in the balance
    //           - IsActivity: Update the last activity of account
    //           - Trx:
    //     - OUTPUT:
    //           - RedeemableFinalBalance: The redeemable in the account after update.
    //           - NotRedeemableFinalBalance: The not redeemable in the account after update.
    //           - PlaySessionId:  Current play session
    //
    // RETURNS:
    //     - Is correct the process?

    public static Boolean Trx_UpdateAccountPoints(Int64 AccountId,
                                               Decimal PointsToAdd,
                                               out Decimal PointsToAddFinalBalance,
                                               SqlTransaction Trx)
    {
      AccountBalance _final;
      AccountBalance _cancel;
      Int64 _play_session_id;

      return Trx_UpdateAccountBalance(AccountId, AccountBalance.Zero, PointsToAdd, out _final, out PointsToAddFinalBalance, out _play_session_id, out _cancel, Trx);

    } // Trx_UpdateAccountPoints

    // PURPOSE: Update the balance, redeemable and not redeemable of the account.
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - RedimibleBalance: The redimible to aggregate in the balance
    //           - NotRedeemableToAdd: The not redimible to aggregate in the balance
    //           - IsActivity: Update the last activity of account
    //           - Trx:
    //           - AllowNegativePoints: Allow negative points
    //     - OUTPUT:
    //           - RedeemableFinalBalance: The redeemable in the account after update.
    //           - NotRedeemableFinalBalance: The not redeemable in the account after update.
    //           - PlaySessionId:  Current play session
    //
    // RETURNS:
    //     - Is correct the process?
    public static Boolean Trx_UpdateAccountPoints(Int64 AccountId,
                                                  Decimal PointsToAdd,
                                                  Boolean AllowNegativePoints,
                                                  out Decimal PointsToAddFinalBalance,
                                                  SqlTransaction Trx)
    {
      AccountBalance _final;
      AccountBalance _cancel;
      Int64 _play_session_id;

      return Trx_UpdateAccountBalance(AccountId, AccountBalance.Zero, PointsToAdd, AllowNegativePoints, out _final, out PointsToAddFinalBalance, out _play_session_id, out _cancel, Trx);
    } // Trx_UpdateAccountPoints

    public static Boolean Trx_UpdateAccountBalance(Int64 AccountId,
                                                   AccountBalance ToAdd,
                                                   Decimal PointsToAdd,
                                                   out AccountBalance Final,
                                                   out Decimal FinalPoints,
                                                   SqlTransaction Trx)
    {
      AccountBalance _cancel;
      Int64 _play_session_id;

      return Trx_UpdateAccountBalance(AccountId, ToAdd, PointsToAdd, out Final, out FinalPoints, out _play_session_id, out _cancel, Trx);
    } // Trx_UpdateAccountBalance

    public static Boolean Trx_UpdateAccountBalance(Int64 AccountId,
                                                   AccountBalance ToAdd,
                                                   Decimal PointsToAdd,
                                                   FlagBucketId IncludeBucket,
                                                   out AccountBalance Final,
                                                   out Decimal FinalPoints,
                                                   SqlTransaction Trx)
    {
      AccountInfo _acc_info;

      Final = null;
      FinalPoints = 0;

      if (MultiPromos.Trx_UpdateAccountBalance(AccountId, String.Empty, ToAdd, PointsToAdd, false, IncludeBucket, out _acc_info, Trx))
      {
        Final = _acc_info.FinalBalance;
        FinalPoints = _acc_info.FinalPoints;
        return true;
      }

      return false;
    } // Trx_UpdateAccountBalance


    public static Boolean Trx_UpdateAccountBalance(Int64 AccountId,
                                               AccountBalance ToAdd,
                                               out AccountBalance Final,
                                               SqlTransaction Trx)
    {
      AccountBalance _cancel;
      Decimal _fin_points;
      Int64 _play_session_id;

      return Trx_UpdateAccountBalance(AccountId, ToAdd, 0, out Final, out _fin_points, out _play_session_id, out _cancel, Trx);
    } // Trx_UpdateAccountBalance

    public static Boolean Trx_UpdateAccountBalance(Int64 AccountId,
                                                   AccountBalance ToAdd,
                                                   out AccountBalance Final,
                                                   out Int64 PlaySessionId,
                                                   SqlTransaction Trx)
    {
      Decimal _final_points;
      AccountBalance _cancel;

      return Trx_UpdateAccountBalance(AccountId, ToAdd, 0, out Final, out _final_points, out PlaySessionId, out _cancel, Trx);
    } // Trx_UpdateAccountBalance

    public static Boolean Trx_UpdateAccountBalance(Int64 AccountId,
                                                   AccountBalance ToAdd,
                                                   out AccountBalance Final,
                                                   out Decimal FinalPoints,
                                                   out Int64 PlaySessionId,
                                                   SqlTransaction Trx)
    {
      AccountBalance _cancel;

      return Trx_UpdateAccountBalance(AccountId, ToAdd, 0, out Final, out FinalPoints, out PlaySessionId, out _cancel, Trx);
    } // Trx_UpdateAccountBalance

    public static Boolean Trx_UpdateAccountBalance(Int64 AccountId,
                                                   AccountBalance ToAdd,
                                                   out AccountBalance Final,
                                                   out Int64 PlaySessionId,
                                                   out AccountBalance Cancellable,
                                                   SqlTransaction Trx)
    {
      Decimal _final_points;

      return Trx_UpdateAccountBalance(AccountId, ToAdd, 0, out Final, out _final_points, out PlaySessionId, out Cancellable, Trx);
    } // Trx_UpdateAccountBalance

    public static Boolean Trx_UpdateAccountBalance(Int64 AccountId,
                                                   AccountBalance ToAdd,
                                                   Decimal PointsToAdd,
                                                   out AccountBalance Final,
                                                   out Decimal FinalPoints,
                                                   out Int64 PlaySessionId,
                                                   out AccountBalance Cancellable,
                                                   SqlTransaction Trx)
    {
      AccountInfo _acc_info;

      Final = AccountBalance.Zero;
      FinalPoints = 0;
      PlaySessionId = 0;
      Cancellable = AccountBalance.Zero;

      if (!Trx_UpdateAccountBalance(AccountId, String.Empty, ToAdd, PointsToAdd, false, out _acc_info, Trx))
      {
        return false;
      }

      Final = _acc_info.FinalBalance;
      FinalPoints = _acc_info.FinalPoints;
      PlaySessionId = _acc_info.CurrentPlaySessionId;
      Cancellable = _acc_info.CancellableBalance;

      return true;
    } // Trx_UpdateAccountBalance

    public static Boolean Trx_UpdateAccountBalance(Int64 AccountId,
                                                   AccountBalance ToAdd,
                                                   Decimal PointsToAdd,
                                                   Boolean AllowNegativePoints,
                                                   out AccountBalance Final,
                                                   out Decimal FinalPoints,
                                                   out Int64 PlaySessionId,
                                                   out AccountBalance Cancellable,
                                                   SqlTransaction Trx)
    {
      AccountInfo _acc_info;

      Final = AccountBalance.Zero;
      FinalPoints = 0;
      PlaySessionId = 0;
      Cancellable = AccountBalance.Zero;

      if (!Trx_UpdateAccountBalance(AccountId, String.Empty, ToAdd, PointsToAdd, AllowNegativePoints, out _acc_info, Trx))
      {
        return false;
      }

      Final = _acc_info.FinalBalance;
      FinalPoints = _acc_info.FinalPoints;
      PlaySessionId = _acc_info.CurrentPlaySessionId;
      Cancellable = _acc_info.CancellableBalance;

      return true;
    } // Trx_UpdateAccountBalance

    // PURPOSE: Update balance account and points.
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - ExternalTrackData:
    //           - ToAdd:
    //           - PointsToAdd:
    //           - AccountInfo:
    //           - Trx:
    //
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - true if updated succefully. else otherwise.

    public static Boolean Trx_UpdateAccountBalance(Int64 AccountId,
                                                   String ExternalTrackData,
                                                   AccountBalance ToAdd,
                                                   Decimal PointsToAdd,
                                                   Boolean AllowNegativePoints,
                                                   out AccountInfo AccountInfo,
                                                   SqlTransaction Trx)
    {
      return Trx_UpdateAccountBalance(AccountId, ExternalTrackData, ToAdd, PointsToAdd, AllowNegativePoints, FlagBucketId.NONE, out AccountInfo, Trx);
    }

    public static Boolean Trx_UpdateAccountBalance(Int64 AccountId,
                                                   String ExternalTrackData,
                                                   AccountBalance ToAdd,
                                                   Decimal PointsToAdd,
                                                   Boolean AllowNegativePoints,
                                                   FlagBucketId IncludeBucket,
                                                   out AccountInfo AccountInfo,
                                                   SqlTransaction Trx)
    {
      StringBuilder _sb;
      String _win_track_data;
      Boolean IsEnableRoundingSaleChips;

#if !SQL_BUSINESS_LOGIC
      Int32 _bucket_id_PT = (int)Buckets.BucketId.RedemptionPoints;
      Int32 _bucket_id_NR = (int)Buckets.BucketId.Credit_NR;
      Int32 _bucket_id_RE = (int)Buckets.BucketId.Credit_RE;
#else
      Int32 _bucket_id_PT = 1;
      Int32 _bucket_id_NR = 3;
      Int32 _bucket_id_RE = 4;
#endif



      Int32 _value_site;
      Boolean _is_center;

      _win_track_data = "";
      AccountInfo = new AccountInfo();

      Boolean _IncludeNRBucket;
      Boolean _IncludeREBucket;

      _IncludeNRBucket = (IncludeBucket & FlagBucketId.NR) != FlagBucketId.NONE;
      _IncludeREBucket = (IncludeBucket & FlagBucketId.RE) != FlagBucketId.NONE;

      //Is it MultiSite Center?
      //MultiSite Center does not have Buckets DB tables
      Int32.TryParse(ReadGeneralParams("MultiSite", "IsCenter", Trx), out _value_site);
      IsEnableRoundingSaleChips = ReadGeneralParams("GamingTables", "Chips.Sales.RoundingToMinimumDenomination", Trx).Equals("1");

      _is_center = (_value_site == 1);

      _sb = new StringBuilder();
      _sb.AppendLine("DECLARE @AccountId_toUpdate BIGINT ");
      _sb.AppendLine("DECLARE @OutputTable TABLE ( AC_ACCOUNT_ID BIGINT");
      // 
      _sb.AppendLine("        , AC_BLOCKED            BIT");
      _sb.AppendLine("        , AC_BLOCK_REASON       INT");
      //
      _sb.AppendLine("        , AC_HOLDER_LEVEL       INT");
      _sb.AppendLine("        , AC_HOLDER_GENDER      INT");
      _sb.AppendLine("        , AC_HOLDER_NAME        NVARCHAR(200)");
      //
      _sb.AppendLine("        , AC_RE_BALANCE         MONEY");
      _sb.AppendLine("        , AC_PROMO_RE_BALANCE   MONEY");
      _sb.AppendLine("        , AC_PROMO_NR_BALANCE   MONEY");
      _sb.AppendLine("        , CBU_VALUE             MONEY");
      //
      _sb.AppendLine("        , AC_CURRENT_PLAY_SESSION_ID    BIGINT");
      _sb.AppendLine("        , AC_CURRENT_TERMINAL_ID        INT");
      //
      _sb.AppendLine("        , AC_IN_SESSION_CANCELLABLE_TRANSACTION_ID    BIGINT");
      _sb.AppendLine("        , AC_IN_SESSION_RE_CANCELLABLE                MONEY");
      _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_CANCELLABLE          MONEY");
      _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_CANCELLABLE          MONEY");
      //
      _sb.AppendLine("        , AC_CANCELLABLE_OPERATION_ID   BIGINT   ");

      _sb.AppendLine("        , AC_RE_RESERVED       MONEY            "); // 04-NOV-2015  JCA    Product Backlog Item 6145: BonoPLUS: Reserved Credit
      _sb.AppendLine("        , AC_MODE_RESERVED     BIT              "); // 26-NOV-2015  AMF    Product Backlog Item 7079
      _sb.AppendLine("        , BU_NR_CREDIT         MONEY            "); // 28-JAN-2016  RCI & JMV Product Backlog Item 8257: Multiple buckets
      _sb.AppendLine("        , BU_RE_CREDIT         MONEY            "); // 10-FEB-2016 ETP Multiple buckets RE CREDIT.
      _sb.AppendLine("        , AC_TYPE              INT              ");
      _sb.AppendLine("        , AC_PROVISION         MONEY            "); // 19-APR-2016  JML    WIGOS-1069 - Custody - Incorrect custody amount to be refunded 
      _sb.AppendLine("        , AC_SALES_CHIPS_ROUNDING_AMOUNT  MONEY "); // 05-JUL-2017  JML    WIGOS-2702 - Rounding - Consume the remaining amount
      _sb.AppendLine("       );  ");

      _sb.AppendLine();

      // Search the Account
      _sb.AppendLine("SET @AccountId_toUpdate = NULL");

      _sb.AppendLine("SELECT @AccountId_toUpdate = AC_ACCOUNT_ID");
      _sb.AppendLine("FROM ACCOUNTS");

      if ((PointsToAdd < 0) && !AllowNegativePoints)
      {
        _sb.AppendLine("LEFT JOIN CUSTOMER_BUCKET ON (AC_ACCOUNT_ID = CBU_CUSTOMER_ID) AND (CBU_BUCKET_ID = @pBucketId_PT) ");
      }

      if (AccountId != 0)
      {
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID   = @pAccountId ");
      }
      else
      {
        _sb.AppendLine("  WHERE  (   ( AC_TYPE  =  3 AND AC_TRACK_DATA   = @pAlesisTrackData ) ");
        _sb.AppendLine("          OR ( AC_TYPE <>  3 AND AC_TRACK_DATA   = dbo.TrackDataToInternal (@pWinTrackData) ) ) ");
      }

      _sb.AppendLine("    AND   (AC_RE_BALANCE        + @pReBalance     ) >= 0 ");
      _sb.AppendLine("    AND   (AC_PROMO_RE_BALANCE  + @pPromoReBalance) >= 0 ");
      _sb.AppendLine("    AND   (AC_PROMO_NR_BALANCE  + @pPromoNrBalance) >= 0 ");

      // AJQ 07-MAR-2013 
      // - The final points balance could be a nevative number, only check it when substracting
      if ((PointsToAdd < 0) && !AllowNegativePoints)
      {
        // - The final points balance must be greater or equal to 0 when substracting
        _sb.AppendLine("    AND   (ISNULL(CBU_VALUE, 0) + @pPoints        ) >= 0 ");
      }

      //Update points to bucket
      _sb.AppendLine("    UPDATE   CUSTOMER_BUCKET                               "); // 12-JAN-2016  SGB  Backlog Item 7910
      _sb.AppendLine("       SET   CBU_VALUE = ISNULL(CBU_VALUE, 0) + @pPoints   ");
      _sb.AppendLine("     WHERE   CBU_CUSTOMER_ID = ISNULL(@AccountId_toUpdate, 0)");
      _sb.AppendLine("       AND   CBU_BUCKET_ID = @pBucketId_PT ");
      //Insert to bucket (if not exists)
      _sb.AppendLine(" IF @@ROWCOUNT = 0 AND ISNULL(@AccountId_toUpdate, 0) <> 0");
      _sb.AppendLine(" BEGIN                                          ");
      _sb.AppendLine("   INSERT INTO CUSTOMER_BUCKET (CBU_CUSTOMER_ID ");
      _sb.AppendLine("                  , CBU_BUCKET_ID               ");
      _sb.AppendLine("                  , CBU_VALUE                   ");
      _sb.AppendLine("                  , CBU_UPDATED                 ");
      _sb.AppendLine("                  )                             ");
      _sb.AppendLine("              VALUES (@AccountId_toUpdate       ");
      _sb.AppendLine("                  , @pBucketId_PT               ");
      _sb.AppendLine("                  , @pPoints                    ");
      _sb.AppendLine("                  , GETDATE()                   ");
      _sb.AppendLine("                  )                             ");
      _sb.AppendLine(" END                                            ");

      //Update Account
      _sb.AppendLine(" UPDATE   ACCOUNTS ");
      _sb.AppendLine("    SET   AC_BALANCE           = AC_RE_BALANCE + AC_PROMO_RE_BALANCE + AC_PROMO_NR_BALANCE + @pReBalance + @pPromoReBalance + @pPromoNrBalance ");
      _sb.AppendLine("        , AC_RE_BALANCE        = AC_RE_BALANCE        + @pReBalance ");
      _sb.AppendLine("        , AC_PROMO_RE_BALANCE  = AC_PROMO_RE_BALANCE  + @pPromoReBalance ");
      _sb.AppendLine("        , AC_PROMO_NR_BALANCE  = AC_PROMO_NR_BALANCE  + @pPromoNrBalance ");
      _sb.AppendLine("        , AC_PROVISION         = AC_PROVISION         + @pCustodyProvision ");

      // 04-NOV-2015  JCA    Product Backlog Item 6145: BonoPLUS: Reserved Credit
      // 22-DIC-2015  FJC    Fixed BUG 7564: When cancel last recharge, appear reserve credit in negative.
      // 22-DIC-2015  FJC    Fixed BUG 7365: When cancel last recharge, doesn't reset the value of reserved
      // 22-DIC-2015  FJC    Fixed BUG 7870: When cancel last Manual Pay, doesn't substract from reserved
      // 22-DIC-2015  FJC    Fixed BUG 7869: When well chipps to Player, doesn't substract from reserved
      // 22-DIC-2015  FJC    Fixed BUG 7867: When cancel Promo Redimible, doesn't reset the value of reserved
      if (!_is_center)
      {
        _sb.AppendLine("        , AC_RE_RESERVED       = CASE WHEN AC_RE_RESERVED  + @pReserved >                                                                   ");
        _sb.AppendLine("                                           AC_RE_BALANCE   + AC_PROMO_RE_BALANCE + @pReBalance + @pPromoReBalance                           ");
        _sb.AppendLine("                                 THEN AC_RE_BALANCE  + AC_PROMO_RE_BALANCE + @pReBalance + @pPromoReBalance                                 ");
        _sb.AppendLine("                                 ELSE AC_RE_RESERVED + @pReserved END                                                                       ");

        if (ToAdd.ModeReservedChanged)
        {
          _sb.AppendLine("        , AC_MODE_RESERVED     = @pModeReserved ");                    // 26-NOV-2015  AMF    Product Backlog Item 7079
        }
        else
        {
          _sb.AppendLine("        , AC_MODE_RESERVED     = AC_MODE_RESERVED ");                  // 26-NOV-2015  AMF    Product Backlog Item 7079
        }
        _sb.AppendLine("        , AC_SALES_CHIPS_ROUNDING_AMOUNT  = AC_SALES_CHIPS_ROUNDING_AMOUNT + @pChipsSalesRoundingAmount                                      ");
      }
      _sb.AppendLine(" OUTPUT   INSERTED.AC_ACCOUNT_ID ");
      //
      _sb.AppendLine("        , INSERTED.AC_BLOCKED       ");
      _sb.AppendLine("        , INSERTED.AC_BLOCK_REASON  ");
      //
      _sb.AppendLine("        , INSERTED.AC_HOLDER_LEVEL  ");
      _sb.AppendLine("        , ISNULL(INSERTED.AC_HOLDER_GENDER, 0) ");
      _sb.AppendLine("        , ISNULL(INSERTED.AC_HOLDER_NAME,  '') ");
      //
      _sb.AppendLine("        , INSERTED.AC_RE_BALANCE       ");
      _sb.AppendLine("        , INSERTED.AC_PROMO_RE_BALANCE ");
      _sb.AppendLine("        , INSERTED.AC_PROMO_NR_BALANCE ");

      _sb.AppendLine("        , ISNULL(CB_PT.CBU_VALUE, 0)"); // 12-JAN-2016  SGB  Backlog Item 7910
      _sb.AppendLine("        , INSERTED.AC_CURRENT_PLAY_SESSION_ID ");
      _sb.AppendLine("        , INSERTED.AC_CURRENT_TERMINAL_ID     ");
      //
      _sb.AppendLine("        , INSERTED.AC_IN_SESSION_CANCELLABLE_TRANSACTION_ID ");
      _sb.AppendLine("        , INSERTED.AC_IN_SESSION_RE_CANCELLABLE ");
      _sb.AppendLine("        , INSERTED.AC_IN_SESSION_PROMO_RE_CANCELLABLE ");
      _sb.AppendLine("        , INSERTED.AC_IN_SESSION_PROMO_NR_CANCELLABLE ");
      //
      _sb.AppendLine("        , ISNULL(INSERTED.AC_CANCELLABLE_OPERATION_ID, 0) ");

      if (!_is_center) //23-FEB-2016  FGB Center does not have bucket tables
      {
        _sb.AppendLine("        , ISNULL(INSERTED.AC_RE_RESERVED, 0)   "); // 04-NOV-2015  JCA    Product Backlog Item 6145: BonoPLUS: Reserved Credit
        _sb.AppendLine("        , INSERTED.AC_MODE_RESERVED            "); // 26-NOV-2015  AMF    Product Backlog Item 7079
      }
      else
      {
        _sb.AppendLine("        , 0 ");
        _sb.AppendLine("        , 0 ");
      }

      if (_IncludeNRBucket) // JMV 26-JAN-2016
      {
        _sb.AppendLine("        , ISNULL(CB_NR.CBU_VALUE, 0)");
      }
      else
      {
        _sb.AppendLine("        , 0 ");
      }

      if (_IncludeREBucket) // ETP 25-FEB-2016
      {
        _sb.AppendLine("      , ISNULL(CB_RE.CBU_VALUE, 0)");
      }
      else
      {
        _sb.AppendLine("      , 0 ");
      }
      _sb.AppendLine("        , INSERTED.AC_TYPE ");
      _sb.AppendLine("        , INSERTED.AC_PROVISION ");
      _sb.AppendLine("        , INSERTED.AC_SALES_CHIPS_ROUNDING_AMOUNT ");

      _sb.AppendLine("   INTO   @OutputTable ");
      _sb.AppendLine("      FROM   ACCOUNTS");

      // use this inner join 
      _sb.AppendLine("INNER JOIN   CUSTOMER_BUCKET CB_PT ON  CB_PT.CBU_CUSTOMER_ID = AC_ACCOUNT_ID  AND  CB_PT.CBU_BUCKET_ID = @pBucketId_PT");
      if (_IncludeNRBucket) // JMV 26-JAN-2016
      {
        _sb.AppendLine("LEFT  JOIN   CUSTOMER_BUCKET CB_NR ON  CB_NR.CBU_CUSTOMER_ID = AC_ACCOUNT_ID  AND  CB_NR.CBU_BUCKET_ID = @pBucketId_NR");
      }

      if (_IncludeREBucket) // ETP 25-FEB-2016
      {
        _sb.AppendLine("LEFT  JOIN   CUSTOMER_BUCKET CB_RE ON   CB_RE.CBU_CUSTOMER_ID = AC_ACCOUNT_ID  AND   CB_RE.CBU_BUCKET_ID = @pBucketId_RE");
      }
      _sb.AppendLine("     WHERE   AC_ACCOUNT_ID = ISNULL(@AccountId_toUpdate, 0)");

      _sb.AppendLine();
      _sb.AppendLine("IF ( @@ROWCOUNT = 1 )");
      _sb.AppendLine("   SELECT * FROM @OutputTable");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          if (AccountId == 0)
          {
            _win_track_data = ExternalTrackData.Trim().PadLeft(20, '0');
            _cmd.Parameters.Add("@pAlesisTrackData", SqlDbType.NVarChar, 50).Value = ExternalTrackData;
            _cmd.Parameters.Add("@pWinTrackData", SqlDbType.NVarChar, 50).Value = _win_track_data;
          }
          else
          {
            _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          }

          _cmd.Parameters.Add("@pReBalance", SqlDbType.Money).Value = ToAdd.Redeemable;
          _cmd.Parameters.Add("@pPromoReBalance", SqlDbType.Money).Value = ToAdd.PromoRedeemable;
          _cmd.Parameters.Add("@pPromoNrBalance", SqlDbType.Money).Value = ToAdd.PromoNotRedeemable;
          _cmd.Parameters.Add("@pCustodyProvision", SqlDbType.Money).Value = ToAdd.CustodyProvision;
          _cmd.Parameters.Add("@pChipsSalesRoundingAmount", SqlDbType.Money).Value = ToAdd.Operation_ChipsSalesRemainingAmount;
          _cmd.Parameters.Add("@pPoints", SqlDbType.Money).Value = PointsToAdd;

          if (!_is_center)
          {
            _cmd.Parameters.Add("@pReserved", SqlDbType.Money).Value = ToAdd.Reserved; // 04-NOV-2015  JCA    Product Backlog Item 6145: BonoPLUS: Reserved Credit
            _cmd.Parameters.Add("@pModeReserved", SqlDbType.Bit).Value = ToAdd.ModeReserved; // 26-NOV-2015  AMF    Product Backlog Item 7079
          }
          _cmd.Parameters.Add("@pBucketId_PT", SqlDbType.Int).Value = _bucket_id_PT;

          if (_IncludeNRBucket) // JMV 26-JAN-2016
          {
            _cmd.Parameters.Add("@pBucketId_NR", SqlDbType.Int).Value = _bucket_id_NR;
          }

          if (_IncludeREBucket) // JMV 26-JAN-2016
          {
            _cmd.Parameters.Add("@pBucketId_RE", SqlDbType.Int).Value = _bucket_id_RE;
          }

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              if (AccountId == 0)
              {
                AccountInfo.ErrorMessage = "ExternalTrackData: '" + ExternalTrackData.Trim() + "' --> Account Not Found.";
              }
              else
              {
                AccountInfo.ErrorMessage = "AccountId: " + AccountId.ToString() + " --> Account Not Found."; ;
              }

              return false;
            }

            AccountInfo.AccountId = _reader.GetInt64(0);

            AccountInfo.Blocked = _reader.GetBoolean(1);
            AccountInfo.BlockReason = _reader.GetInt32(2);

            AccountInfo.HolderLevel = _reader.GetInt32(3);
            if (AccountInfo.HolderLevel > 0)
            {
              AccountInfo.HolderGender = _reader.GetInt32(4);
              AccountInfo.HolderName = _reader.GetString(5);
            }

            // JMV 28-JAN-2016: Product Backlog Item 8257
            AccountInfo.FinalBalance = new AccountBalance(_reader.GetDecimal(6), _reader.GetDecimal(7), _reader.GetDecimal(8),
                                                          _reader.GetDecimal(17), _reader.GetBoolean(18), _reader.GetDecimal(19),
                                                          _reader.GetDecimal(20), _reader.GetDecimal(22), _reader.GetDecimal(23), 
                                                          IsEnableRoundingSaleChips);
            AccountInfo.FinalPoints = _reader.GetDecimal(9); //CBU_VALUE

            // InSession?
            AccountInfo.CurrentPlaySessionId = _reader.IsDBNull(10) ? 0 : _reader.GetInt64(10);
            AccountInfo.CurrentTerminalId = _reader.IsDBNull(11) ? 0 : _reader.GetInt32(11);

            if (AccountInfo.CurrentPlaySessionId != 0)
            {
              AccountInfo.CancellableTrxId = _reader.IsDBNull(12) ? 0 : _reader.GetInt64(12);
              if (AccountInfo.CancellableTrxId != 0)
              {
                AccountInfo.CancellableBalance = new AccountBalance(_reader.GetDecimal(13), _reader.GetDecimal(14), _reader.GetDecimal(15), _reader.GetDecimal(17), _reader.GetBoolean(18));
              }
            }

            AccountInfo.CancellableOperationId = _reader.IsDBNull(16) ? 0 : _reader.GetInt64(16);
            AccountInfo.AcType = (AccountType)_reader.GetInt32(21);
          }
        }

#if !SQL_BUSINESS_LOGIC
        // IMPORTANT: It is only updating CustomerBucketByGamingDay TALBE . NOT CustomerBucket TABLE
        if (!BucketsUpdate.UpdateCustomerBucket(AccountId, (Buckets.BucketId)_bucket_id_PT, null, PointsToAdd, Buckets.BucketOperation.ADD_GENERATED, false, true, Trx))
        {
          return false;
        }
#endif

        if (AccountInfo.CurrentPlaySessionId != 0)
        {
          _sb.Length = 0;
          _sb.AppendLine(" SELECT   PS_STATUS ");
          _sb.AppendLine("        , ISNULL(PS_BALANCE_MISMATCH, 0) ");
          _sb.AppendLine("        , PS_INITIAL_BALANCE ");
          _sb.AppendLine("        , PS_CASH_IN ");
          _sb.AppendLine("   FROM   PLAY_SESSIONS ");
          _sb.AppendLine("  WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId ");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = AccountInfo.CurrentPlaySessionId;
            using (SqlDataReader _reader = _cmd.ExecuteReader())
            {
              if (!_reader.Read())
              {
                return false;
              }

              AccountInfo.CurrentPlaySessionStatus = (PlaySessionStatus)_reader.GetInt32(0);
              AccountInfo.CurrentPlaySessionBalanceMismatch = _reader.GetBoolean(1);

              //AccountInfo.CurrentPlaySessionInitialBalance = _reader.GetDecimal(2) + _reader.GetDecimal(3);

              AccountInfo.CurrentPlaySessionInitialBalance = _reader.GetDecimal(2);
              AccountInfo.CurrentPlaySessionCashInAmount = _reader.GetDecimal(3);
              // Current playsessionintial balance should never be + cash in or it is not the session initial balance
              // only used inthese 2 places, safe to change to support FBM
              // \WGC\Kernel\WSI.Common\Shared\Shared3GS.cs(471):      _calculated = Math.Max(0, Account.CurrentPlaySessionInitialBalance - PlaySessionMeters.PlayedAmount + PlaySessionMeters.WonAmount + PlaySessionMeters.BillInAmount);
              // \WGC\Kernel\WSI.Common\Shared\SharedMultiPromos.cs(5689):              AccountInfo.CurrentPlaySessionInitialBalance = _reader.GetDecimal(2) + _reader.GetDecimal(3);

            }
          }
        }

        return true;

      }
      catch (Exception _ex)
      {
        if (AccountId == 0)
        {
          AccountInfo.ErrorMessage = "ExternalTrackData: '" + ExternalTrackData.Trim() + "' --> Exception: " + _ex.Message;
        }
        else
        {
          AccountInfo.ErrorMessage = "AccountId: " + AccountId.ToString() + " --> Exception: " + _ex.Message;
        }
      }

      return false;
    } // Trx_UpdateAccountBalance

    // PURPOSE: Set activity in the account
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    public static Boolean Trx_SetActivity(Int64 AccountId,
                                          SqlTransaction Trx)
    {
      StringBuilder _sb;
      _sb = new StringBuilder();
      _sb.AppendLine("UPDATE   ACCOUNTS ");
      _sb.AppendLine("   SET   AC_LAST_ACTIVITY  = GETDATE() ");
      _sb.AppendLine(" WHERE   AC_ACCOUNT_ID  = @pAccountId ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          return (_cmd.ExecuteNonQuery() == 1);

        }
      }
      catch
      { ; }

      return false;

    } // Trx_SetActivity

    public static Boolean Trx_SetInitialCashIn(Int64 AccountId, Decimal AmountToSub,
                                          SqlTransaction Trx)
    {
      StringBuilder _sb;
      _sb = new StringBuilder();
      _sb.AppendLine("UPDATE   ACCOUNTS ");
      _sb.AppendLine("   SET   AC_INITIAL_CASH_IN  = case WHen AC_INITIAL_CASH_IN - @pSubAmount < 0 THEN 0 ELSE AC_INITIAL_CASH_IN - @pSubAmount END ");
      _sb.AppendLine(" WHERE   AC_ACCOUNT_ID  = @pAccountId ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pSubAmount", SqlDbType.Money).Value = AmountToSub;

          return (_cmd.ExecuteNonQuery() == 1);

        }
      }
      catch
      { ; }

      return false;

    } // Trx_SetInitialCashIn

    //------------------------------------------------------------------------------
    // PURPOSE: Mark Account as Blocked indicating the reason why.
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountID
    //          - BlockReason
    //          - SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        
    //   NOTES :
    //
    public static Boolean Trx_SetAccountBlocked(Int64 AccountId,
                                                AccountBlockReason BlockReason,
                                                SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      // Don't do try/catch !!!!
      // The routine DB_SetAccountBlocked() needs to do it itself.
      // If you call this routine, do a try/catch.

      _sb = new StringBuilder();
      _sb.AppendLine("UPDATE   ACCOUNTS ");
      _sb.AppendLine("   SET   AC_BLOCKED      = 1 ");
      _sb.AppendLine("       , AC_BLOCK_REASON = dbo.BlockReason_ToNewEnumerate(AC_BLOCK_REASON) | @pBlockReason ");
      _sb.AppendLine("       , AC_BLOCK_DESCRIPTION = NULL");
      _sb.AppendLine(" WHERE   AC_ACCOUNT_ID   = @pAccountId ");
      _sb.AppendLine("   AND   AC_TYPE NOT IN (@pAccountVirtualCashier, @pAccountVirtualTerminal) ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        _cmd.Parameters.Add("@pBlockReason", SqlDbType.Int).Value = BlockReason;
        _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
        _cmd.Parameters.Add("@pAccountVirtualCashier", SqlDbType.Int).Value = AccountType.ACCOUNT_VIRTUAL_CASHIER;
        _cmd.Parameters.Add("@pAccountVirtualTerminal", SqlDbType.Int).Value = AccountType.ACCOUNT_VIRTUAL_TERMINAL;

        return (_cmd.ExecuteNonQuery() <= 1);
      }
    } // Trx_SetAccountBlocked

    public enum InSessionAction
    {
      Unknown = 0,
      StartSession = 1,
      ReStartSession = 2,
      EndSession = 3,
      StartSessionCancelled = 4,
    }

    // PURPOSE: Add a value to fields in session redeemable and not redeemable and update the game meters of the account.
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - InSessionReDelta:
    //           - InSessionNrDelta:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    public static Boolean Trx_CancelStartCardSession(Int32 TerminalId,
                                                     Int64 TransactionId,
                                                     out Boolean Cancelled,
                                                     out Int64 ClosedPlaySessionId,
                                                     SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int64 _account_id;
      Int64 _play_session_id;
      String _terminal_name;
      AccountBalance _cancelled;
      AccountBalance _to_gm0;
      AccountBalance _to_gm1;
      AccountBalance _dummy_bal1;
      AccountBalance _dummy_bal2;
      AccountBalance _ini_balance;
      AccountBalance _fin_balance;
      Boolean _rollback;
      PlaySessionStatus _old_status;
      PlaySessionStatus _new_status;
      Boolean _was_first_start_session;
      TimeSpan _play_timespan;
      Decimal _ps_initial_balance;
      Decimal _ps_cash_in;

      Cancelled = false;
      ClosedPlaySessionId = 0;
      _was_first_start_session = true;
      _ps_initial_balance = 0;
      _ps_cash_in = 0;

      _rollback = true;

      try
      {
        Trx.Save("CancelStartCardSession");

        _sb = new StringBuilder();
        _sb.AppendLine("DECLARE @OutputTable TABLE ( ");
        _sb.AppendLine("          AC_ACCOUNT_ID                      BIGINT");
        _sb.AppendLine("        , AC_CURRENT_PLAY_SESSION_ID         BIGINT");
        _sb.AppendLine("        , AC_IN_SESSION_RE_CANCELLABLE       MONEY");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_CANCELLABLE MONEY");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_CANCELLABLE MONEY");
        _sb.AppendLine("        , AC_IN_SESSION_RE_TO_GM             MONEY");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_TO_GM       MONEY");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_TO_GM       MONEY");
        _sb.AppendLine("        , PS_INITIAL_BALANCE                 MONEY");
        _sb.AppendLine("        , PS_CASH_IN                         MONEY");
        _sb.AppendLine("        , TE_NAME                            NVARCHAR(50));");
        _sb.AppendLine();

        _sb.AppendLine(" UPDATE   ACCOUNTS ");
        _sb.AppendLine("    SET   AC_IN_SESSION_CANCELLABLE_TRANSACTION_ID = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_RE_CANCELLABLE             = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_CANCELLABLE       = 0 ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_CANCELLABLE       = 0 ");
        //
        _sb.AppendLine(" OUTPUT   DELETED.AC_ACCOUNT_ID                      ");
        _sb.AppendLine("        , DELETED.AC_CURRENT_PLAY_SESSION_ID         ");
        _sb.AppendLine("        , DELETED.AC_IN_SESSION_RE_CANCELLABLE       ");
        _sb.AppendLine("        , DELETED.AC_IN_SESSION_PROMO_RE_CANCELLABLE ");
        _sb.AppendLine("        , DELETED.AC_IN_SESSION_PROMO_NR_CANCELLABLE ");
        _sb.AppendLine("        , DELETED.AC_IN_SESSION_RE_TO_GM ");
        _sb.AppendLine("        , DELETED.AC_IN_SESSION_PROMO_RE_TO_GM ");
        _sb.AppendLine("        , DELETED.AC_IN_SESSION_PROMO_NR_TO_GM ");
        _sb.AppendLine("        , PS.PS_INITIAL_BALANCE ");
        _sb.AppendLine("        , PS.PS_CASH_IN ");
        _sb.AppendLine("        , TE.TE_NAME ");
        //
        _sb.AppendLine("   INTO   @OutputTable ");
        //
        _sb.AppendLine("   FROM   ACCOUNTS ");
        _sb.AppendLine("  INNER   JOIN TERMINALS     AS TE ON AC_CURRENT_TERMINAL_ID     = TE.TE_TERMINAL_ID             ");
        _sb.AppendLine("  INNER   JOIN PLAY_SESSIONS AS PS ON AC_CURRENT_PLAY_SESSION_ID = PS.PS_PLAY_SESSION_ID         ");

        _sb.AppendLine("  WHERE   TE.TE_TERMINAL_ID             = @pTerminalId");
        _sb.AppendLine("    AND   TE.TE_CURRENT_PLAY_SESSION_ID = AC_CURRENT_PLAY_SESSION_ID    ");
        _sb.AppendLine("    AND   PS.PS_PLAY_SESSION_ID         = TE.TE_CURRENT_PLAY_SESSION_ID ");
        _sb.AppendLine("    AND   PS.PS_TERMINAL_ID             = @pTerminalId                  ");
        _sb.AppendLine("    AND   AC_ACCOUNT_ID                 = TE.TE_CURRENT_ACCOUNT_ID      ");
        _sb.AppendLine("    AND   AC_CURRENT_TERMINAL_ID        = @pTerminalId                  ");
        _sb.AppendLine("    AND   AC_CURRENT_PLAY_SESSION_ID    = TE.TE_CURRENT_PLAY_SESSION_ID ");

        _sb.AppendLine("    AND   AC_IN_SESSION_CANCELLABLE_TRANSACTION_ID = @pTransactionId ");
        _sb.AppendLine("    AND   AC_CURRENT_PLAY_SESSION_ID IS NOT NULL ");
        _sb.AppendLine("    AND   PS.PS_STATUS = @pStatusOpened ");


        _sb.AppendLine("IF ( @@ROWCOUNT = 1 )");
        _sb.AppendLine("   SELECT * FROM @OutputTable");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = TransactionId;
          _cmd.Parameters.Add("@pStatusOpened", SqlDbType.Int).Value = PlaySessionStatus.Opened;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              // Not Cancellable
              return true;
            }

            _account_id = _reader.GetInt64(0);
            _play_session_id = _reader.GetInt64(1);
            _cancelled = new AccountBalance(_reader.GetDecimal(2), _reader.GetDecimal(3), _reader.GetDecimal(4));
            _to_gm0 = new AccountBalance(_reader.GetDecimal(5), _reader.GetDecimal(6), _reader.GetDecimal(7));
            _ps_initial_balance = _reader.GetDecimal(8);
            _ps_cash_in = _reader.GetDecimal(9);
            _terminal_name = _reader.GetString(10);
          }
        }

        _was_first_start_session = (_cancelled == _to_gm0);

        // Decrease 'TotalInitialBalance'
        if (_was_first_start_session && _ps_initial_balance == 0 && _ps_cash_in > 0)
        {
          // When is SYS-Aceptor:
          //      1- Start Card session with initial balance = 0 (to gm)
          //      2- Restart card session with cash in = XX (to gm)
          //  In this case _was_first_start_session is false
          _was_first_start_session = false;
        }

        if (!Trx_UpdateAccountInSession(InSessionAction.StartSessionCancelled, _account_id, TransactionId, _cancelled,
                                        out _to_gm1, out _dummy_bal1, out _dummy_bal2, Trx))
        {
          return false;
        }

        if (!Trx_UpdateAccountBalance(_account_id, _cancelled, out _fin_balance, Trx))
        {
          return false;
        }

        _ini_balance = _fin_balance - _cancelled;

        if (!Trx_BalanceToPlaySession(_was_first_start_session, _play_session_id, AccountBalance.Negate(_cancelled), false, Trx))
        {
          return false;
        }

        if (_was_first_start_session)
        {
          // Close 
          if (!Trx_PlaySessionChangeStatus(PlaySessionStatus.Cancelled, _play_session_id, out _old_status, out _new_status, out _play_timespan, Trx))
          {
            return false;
          }

          ClosedPlaySessionId = _play_session_id;

          if (!Trx_UnlinkAccountTerminal(_account_id, TerminalId, _play_session_id, Trx))
          {
            return false;
          }
        }

        if (!Trx_UnlinkPromotionsInSession(_account_id, _play_session_id, TransactionId, Trx))
        {
          return false;
        }

        AccountMovementsTable _am_table;
        _am_table = new AccountMovementsTable(TerminalId, _terminal_name, _play_session_id);
        if (!_am_table.Add(0, _account_id, MovementType.CancelStartSession,
                           _ini_balance.TotalBalance, 0, _cancelled.TotalBalance, _fin_balance.TotalBalance))
        {
          return false;
        }

        if (!_am_table.Save(Trx))
        {
          return false;
        }

        Cancelled = true;
        _rollback = false;

        return true;
      }
#if !SQL_BUSINESS_LOGIC
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
#else
      catch
      { ; }
#endif
      finally
      {
        if (_rollback)
        {
          try
          {
            Trx.Rollback("CancelStartCardSession");
          }
          catch
          { }
        }
      }

      return false;
    } // Trx_CancelStartCardSession

    public static Boolean Trx_UpdateAccountInSession(InSessionAction Action,
                                                     Int64 AccountId,
                                                     Int64 TransactionId,
                                                     AccountBalance PlayableBalance,
                                                     out AccountBalance TotalToGMBalance,
                                                     out AccountBalance TotalFromGMBalance,
                                                     out AccountBalance SessionBalance,
                                                     SqlTransaction Trx)
    {
      StringBuilder _sb;

      AccountBalance _cancel;
      AccountBalance _to_gm;
      AccountBalance _from_gm;
      AccountBalance _to_add;
      Int64 _trx_id;

      TotalToGMBalance = AccountBalance.Zero;
      TotalFromGMBalance = AccountBalance.Zero;
      SessionBalance = AccountBalance.Zero;

      // All parts must be greater than 0
      if (PlayableBalance.Redeemable < 0
          || PlayableBalance.PromoRedeemable < 0
          || PlayableBalance.PromoNotRedeemable < 0)
      {
        return false;
      }

      try
      {
        switch (Action)
        {
          case InSessionAction.StartSession:
          case InSessionAction.ReStartSession:
            _to_add = PlayableBalance;
            _to_gm = PlayableBalance;
            _from_gm = AccountBalance.Zero;
            _cancel = PlayableBalance;
            _trx_id = TransactionId;
            break;

          case InSessionAction.EndSession:
            _to_add = AccountBalance.Negate(PlayableBalance);
            _to_gm = AccountBalance.Zero;
            _from_gm = PlayableBalance;
            _cancel = AccountBalance.Zero;
            _trx_id = 0;
            break;

          case InSessionAction.StartSessionCancelled:
            _to_add = AccountBalance.Negate(PlayableBalance);
            _to_gm = AccountBalance.Negate(PlayableBalance);
            _from_gm = AccountBalance.Zero;
            _cancel = AccountBalance.Zero;
            _trx_id = 0;
            break;

          default:
            return false;
        }

        _sb = new StringBuilder();

        _sb.AppendLine("DECLARE @OutputTable TABLE ( ");
        _sb.AppendLine("          AC_IN_SESSION_RE_TO_GM         MONEY");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_TO_GM   MONEY");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_TO_GM   MONEY");
        _sb.AppendLine("        , AC_IN_SESSION_RE_FROM_GM       MONEY");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_FROM_GM MONEY");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_FROM_GM MONEY");
        _sb.AppendLine("        , AC_IN_SESSION_RE_BALANCE       MONEY");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_BALANCE MONEY");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_BALANCE MONEY);");
        _sb.AppendLine();

        _sb.AppendLine(" UPDATE   ACCOUNTS ");

        _sb.AppendLine("    SET   AC_IN_SESSION_RE_BALANCE       = AC_IN_SESSION_RE_BALANCE       + @pRedeemable ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_BALANCE = AC_IN_SESSION_PROMO_RE_BALANCE + @pPromoRe ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_BALANCE = AC_IN_SESSION_PROMO_NR_BALANCE + @pPromoNr ");
        //
        _sb.AppendLine("        , AC_IN_SESSION_RE_TO_GM         = AC_IN_SESSION_RE_TO_GM         + @pToGmRedeemable ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_TO_GM   = AC_IN_SESSION_PROMO_RE_TO_GM   + @pToGmPromoRe ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_TO_GM   = AC_IN_SESSION_PROMO_NR_TO_GM   + @pToGmPromoNr ");
        //
        _sb.AppendLine("        , AC_IN_SESSION_RE_FROM_GM       = AC_IN_SESSION_RE_FROM_GM       + @pFromGmRedeemable ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_FROM_GM = AC_IN_SESSION_PROMO_RE_FROM_GM + @pFromGmPromoRe ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_FROM_GM = AC_IN_SESSION_PROMO_NR_FROM_GM + @pFromGmPromoNr ");
        //
        _sb.AppendLine("        , AC_IN_SESSION_CANCELLABLE_TRANSACTION_ID = @pCancelTransactionId ");
        _sb.AppendLine("        , AC_IN_SESSION_RE_CANCELLABLE             = @pCancelRedeemable ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_RE_CANCELLABLE       = @pCancelPromoRe ");
        _sb.AppendLine("        , AC_IN_SESSION_PROMO_NR_CANCELLABLE       = @pCancelPromoNr ");
        //
        _sb.AppendLine(" OUTPUT   INSERTED.AC_IN_SESSION_RE_TO_GM ");
        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_PROMO_RE_TO_GM ");
        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_PROMO_NR_TO_GM ");

        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_RE_FROM_GM ");
        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_PROMO_RE_FROM_GM ");
        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_PROMO_NR_FROM_GM ");

        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_RE_BALANCE ");
        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_PROMO_RE_BALANCE ");
        _sb.AppendLine("        , INSERTED.AC_IN_SESSION_PROMO_NR_BALANCE ");
        //
        _sb.AppendLine("   INTO   @OutputTable ");
        //
        _sb.AppendLine("   FROM   ACCOUNTS ");
        _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId ");

        _sb.AppendLine();
        _sb.AppendLine("IF ( @@ROWCOUNT = 1 )");
        _sb.AppendLine("   SELECT * FROM @OutputTable");


        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

          _cmd.Parameters.Add("@pRedeemable", SqlDbType.Decimal).Value = _to_add.Redeemable;
          _cmd.Parameters.Add("@pPromoRe", SqlDbType.Decimal).Value = _to_add.PromoRedeemable;
          _cmd.Parameters.Add("@pPromoNr", SqlDbType.Decimal).Value = _to_add.PromoNotRedeemable;

          _cmd.Parameters.Add("@pToGmRedeemable", SqlDbType.Decimal).Value = _to_gm.Redeemable;
          _cmd.Parameters.Add("@pToGmPromoRe", SqlDbType.Decimal).Value = _to_gm.PromoRedeemable;
          _cmd.Parameters.Add("@pToGmPromoNr", SqlDbType.Decimal).Value = _to_gm.PromoNotRedeemable;

          _cmd.Parameters.Add("@pFromGmRedeemable", SqlDbType.Decimal).Value = _from_gm.Redeemable;
          _cmd.Parameters.Add("@pFromGmPromoRe", SqlDbType.Decimal).Value = _from_gm.PromoRedeemable;
          _cmd.Parameters.Add("@pFromGmPromoNr", SqlDbType.Decimal).Value = _from_gm.PromoNotRedeemable;

          _cmd.Parameters.Add("@pCancelTransactionId", SqlDbType.BigInt).Value = _trx_id;
          _cmd.Parameters.Add("@pCancelRedeemable", SqlDbType.Decimal).Value = _cancel.Redeemable;
          _cmd.Parameters.Add("@pCancelPromoRe", SqlDbType.Decimal).Value = _cancel.PromoRedeemable;
          _cmd.Parameters.Add("@pCancelPromoNr", SqlDbType.Decimal).Value = _cancel.PromoNotRedeemable;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            TotalToGMBalance = new AccountBalance(_reader.GetDecimal(0), _reader.GetDecimal(1), _reader.GetDecimal(2));
            TotalFromGMBalance = new AccountBalance(_reader.GetDecimal(3), _reader.GetDecimal(4), _reader.GetDecimal(5));
            SessionBalance = new AccountBalance(_reader.GetDecimal(6), _reader.GetDecimal(7), _reader.GetDecimal(8));

            return true;
          }
        }
      }
      catch
      { ; }

      return false;

    } // Trx_UpdateAccountInSessionBalance

    public static Boolean Trx_BalanceToPlaySession(Boolean IsFirstTransfer,
                                                   Int64 PlaySessionId,
                                                   AccountBalance PlayableBalance,
                                                   Boolean TITOMode,
                                                   SqlTransaction Trx)
    {
      return Trx_BalanceToPlaySession(IsFirstTransfer, PlaySessionId, PlayableBalance, TITOMode, false, false, Trx);
    }

    // PURPOSE: Update the initial balance or cash in of the play sessions.
    //
    //  PARAMS:
    //     - INPUT:
    //           - PlaySessionId:
    //           - Redeemable:
    //           - NotRedeemable:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    public static Boolean Trx_BalanceToPlaySession(Boolean IsFirstTransfer,
                                                   Int64 PlaySessionId,
                                                   AccountBalance PlayableBalance,
                                                   Boolean TITOMode,
                                                   Boolean IsMico2,
                                                   Boolean IsMico2Transfer,
                                                   SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  UPDATE   PLAY_SESSIONS ");

          _sb.AppendLine("     SET   PS_INITIAL_BALANCE = CASE WHEN (@pIsFirstTransfer = 1) THEN PS_INITIAL_BALANCE + @pPlayableBalance ELSE PS_INITIAL_BALANCE END ");

        if (TITOMode)
        {
          _sb.AppendLine("          , PS_AUX_FT_NR_CASH_IN = ISNULL(PS_AUX_FT_NR_CASH_IN, 0) + @pNRBalance");
          _sb.AppendLine("          , PS_AUX_FT_RE_CASH_IN = ISNULL(PS_AUX_FT_RE_CASH_IN, 0) + @pREBalance");
        }
        else if (IsMico2)        
        {
          if (IsMico2Transfer)
        {
            _sb.AppendLine("          , PS_AUX_FT_NR_CASH_IN = CASE WHEN (@pIsFirstTransfer = 1) THEN 0 ELSE ISNULL(PS_AUX_FT_NR_CASH_IN, 0) + @pNRBalance END ");
            _sb.AppendLine("          , PS_AUX_FT_RE_CASH_IN = CASE WHEN (@pIsFirstTransfer = 1) THEN 0 ELSE ISNULL(PS_AUX_FT_RE_CASH_IN, 0) + @pREBalance END ");
        }
          _sb.AppendLine("          , PS_REDEEMABLE_CASH_IN = CASE WHEN (@pIsFirstTransfer = 1) THEN @pPlayableBalance ELSE PS_REDEEMABLE_CASH_IN + @pPlayableBalance END ");
          }
          else
          {
          _sb.AppendLine("          , PS_CASH_IN = CASE WHEN (@pIsFirstTransfer = 1) THEN 0 ELSE PS_CASH_IN + @pPlayableBalance END ");
        }


        _sb.AppendLine("   WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pIsFirstTransfer", SqlDbType.Bit).Value = IsFirstTransfer;
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;
          _cmd.Parameters.Add("@pPlayableBalance", SqlDbType.Decimal).Value = PlayableBalance.TotalBalance;
          _cmd.Parameters.Add("@pNRBalance", SqlDbType.Decimal).Value = PlayableBalance.TotalNotRedeemable;
          _cmd.Parameters.Add("@pREBalance", SqlDbType.Decimal).Value = PlayableBalance.TotalRedeemable;

          return (_cmd.ExecuteNonQuery() == 1);
        }

      }
      catch
      { ; }

      return false;

    } // Trx_BalanceToPlaySession

    //------------------------------------------------------------------------------
    // PURPOSE :  Calculate Award Points for account
    //
    //  PARAMS :
    //      - INPUT :
    //          - AccountId
    //          - TerminalId
    //          - InSessionRePlayed
    //          - InSessionReSpent
    //          - InSessionPlayed
    //          - Trx
    //
    //      - OUTPUT :
    //          - TotalPoints
    //          - GenerateMovement
    // RETURNS :
    //      - True:  If Points have been computed correctly
    //      - False: Otherwhise.
    //

    public static Boolean DEPRECATED_Trx_ComputeWonPoints(Int64 AccountId,
                                               Terminal.TerminalInfo TerminalInfo,
                                               Decimal InSessionReSpent,
                                               Decimal InSessionRePlayed,
                                               Decimal InSessionPlayed,
                                               out Decimal WonPoints,
                                               out Boolean PointHaveToBeAwarded,
                                               SqlTransaction Trx)
    {
      Decimal _redeemable_spent_to_1_point;
      Decimal _redeemable_played_to_1_point;
      Decimal _total_played_to_1_point;
      StringBuilder _sb;
      Int32 _holder_level;
      Int32 _mode;

      PointHaveToBeAwarded = false;
      WonPoints = 0;

      if (Trx_GetExternalLoyaltyProgramMode(out _mode, Trx))
      {
        if (_mode != 0)
        {
          return true;
        }
      }

      //
      // IF THE ACCOUNT IS VIRTUAL
      //      WON'T RETRIEVE THE HOLDER LEVEL BECAUSE WON'T COMPUTE POINTS
      //
      _sb = new StringBuilder();
      _sb.AppendLine(" SELECT   CASE WHEN AC_TYPE = @pAccountVirtualTerminal THEN 0 ELSE ISNULL(AC_HOLDER_LEVEL, 0) END  ");
      _sb.AppendLine("   FROM   ACCOUNTS ");
      _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pAccountVirtualTerminal", SqlDbType.Int).Value = AccountType.ACCOUNT_VIRTUAL_TERMINAL;

          _holder_level = (Int32)_cmd.ExecuteScalar();
        }

        if (_holder_level == 0)
        {
          // Is anonymous OR Is Virtual Account
          return true;
        }

        _sb.Length = 0;
        _sb.AppendLine(" SELECT   GP_KEY_VALUE ");
        _sb.AppendLine("   FROM   GENERAL_PARAMS ");
        _sb.AppendLine("  WHERE   GP_GROUP_KEY  = 'PlayerTracking' ");
        _sb.AppendLine("    AND   GP_SUBJECT_KEY = 'LevelXX.RedeemablePlayedTo1Point'");
        _sb.AppendLine(" ;");
        _sb.AppendLine(" SELECT   GP_KEY_VALUE ");
        _sb.AppendLine("   FROM   GENERAL_PARAMS ");
        _sb.AppendLine("  WHERE   GP_GROUP_KEY  = 'PlayerTracking' ");
        _sb.AppendLine("    AND   GP_SUBJECT_KEY = 'LevelXX.RedeemableSpentTo1Point'");
        _sb.AppendLine(" ;");
        _sb.AppendLine(" SELECT   GP_KEY_VALUE ");
        _sb.AppendLine("   FROM   GENERAL_PARAMS ");
        _sb.AppendLine("  WHERE   GP_GROUP_KEY  = 'PlayerTracking' ");
        _sb.AppendLine("    AND   GP_SUBJECT_KEY = 'LevelXX.TotalPlayedTo1Point' ");
        _sb.AppendLine(" ;");

        _sb.Replace("XX", _holder_level.ToString("00"));

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            // Read Points and Points multiplier
            if (!_reader.Read())
            {
              return false;
            }
            _redeemable_played_to_1_point = Convert.ToDecimal(_reader.GetString(0));

            if (!_reader.NextResult())
            {
              return false;
            }
            if (!_reader.Read())
            {
              return false;
            }
            _redeemable_spent_to_1_point = Convert.ToDecimal(_reader.GetString(0));

            if (!_reader.NextResult())
            {
              return false;
            }
            if (!_reader.Read())
            {
              return false;
            }
            _total_played_to_1_point = Convert.ToDecimal(_reader.GetString(0));

          }
        }

        if (TerminalInfo.PvPointsMultiplier == 0)
        {
          // Provider doesn't generate points --> Player won 0 points
          PointHaveToBeAwarded = true;
          WonPoints = 0;

          return true;
        }

        _redeemable_played_to_1_point = Math.Max(_redeemable_played_to_1_point, 0);
        _redeemable_spent_to_1_point = Math.Max(_redeemable_spent_to_1_point, 0);
        _total_played_to_1_point = Math.Max(_total_played_to_1_point, 0);

        // Compute points
        if (_redeemable_played_to_1_point > 0)
        {
          PointHaveToBeAwarded = true;
          WonPoints += Math.Max(0, InSessionRePlayed) / _redeemable_played_to_1_point;
        }
        if (_redeemable_spent_to_1_point > 0)
        {
          PointHaveToBeAwarded = true;
          WonPoints += Math.Max(0, InSessionReSpent) / _redeemable_spent_to_1_point;
        }
        if (_total_played_to_1_point > 0)
        {
          PointHaveToBeAwarded = true;
          WonPoints += Math.Max(0, InSessionPlayed) / _total_played_to_1_point;
        }

        WonPoints = PointHaveToBeAwarded ? Math.Round(WonPoints * TerminalInfo.PvPointsMultiplier, 4) : 0;

        return true;
      }
      catch
      { ;}

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE :  Get General Param "PlayerTracking.ExternalLoyaltyProgram"."Mode"
    //
    //  PARAMS :
    //      - INPUT :
    //          - Trx
    //
    //      - OUTPUT :
    //          - Mode
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    //
    private static Boolean Trx_GetExternalLoyaltyProgramMode(out Int32 Mode, SqlTransaction Trx)
    {
      StringBuilder _sb;

      Mode = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.Length = 0;
        _sb.AppendLine("SELECT ISNULL ( ( SELECT   CAST (GP_KEY_VALUE AS INT) ");
        _sb.AppendLine("                    FROM   GENERAL_PARAMS ");
        _sb.AppendLine("                   WHERE   GP_GROUP_KEY   = 'PlayerTracking.ExternalLoyaltyProgram' ");
        _sb.AppendLine("                     AND   GP_SUBJECT_KEY = 'Mode' ");
        _sb.AppendLine("                     AND   ISNUMERIC(GP_KEY_VALUE) = 1 ), 0 ) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          Mode = (Int32)_cmd.ExecuteScalar();
        }

        return true;
      }
      catch
      {
      }

      return false;

    } // Trx_GetExternalLoyatyProgramMode

    //------------------------------------------------------------------------------
    // PURPOSE :  Check if today is client's Birthday or soon (using Stored Procedure)
    //
    //  PARAMS :
    //      - INPUT :
    //          - AccountId
    //          - HolderName
    //          - Trx
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    //
    public static Boolean CheckBirthdayAlarm(Int64 AccountId, String HolderName, String TerminalName, TerminalTypes TerminalType, SqlTransaction Trx)
    {
      ALARM_BIRTHDAY _alarm;
      Int64 _alarm_code;
      String _alarm_description;

      _alarm = ALARM_BIRTHDAY.DO_NOTHING;
      _alarm_code = 0;//AlarmCode.Unknown;
      _alarm_description = string.Empty;

      try
      {
        using (SqlCommand _cmd = new SqlCommand("BirthDayAlarm", Trx.Connection, Trx))
        {
          _cmd.CommandType = CommandType.StoredProcedure;
          SqlParameter _parm1 = new SqlParameter("@pAccountId", SqlDbType.BigInt);
          _parm1.Value = AccountId;
          _parm1.Direction = ParameterDirection.Input;
          _cmd.Parameters.Add(_parm1);

          SqlParameter _parm2 = new SqlParameter("@pAlarmType", SqlDbType.Int);
          _parm2.Direction = ParameterDirection.Output;
          _cmd.Parameters.Add(_parm2);

          _cmd.ExecuteNonQuery();

          _alarm = (ALARM_BIRTHDAY)_cmd.Parameters["@pAlarmType"].Value;
        }

#if !SQL_BUSINESS_LOGIC
        switch (TerminalType)
        {
          case TerminalTypes.UNKNOWN:
            _alarm_description = Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_TITLE") + ": ";
            break;

          case TerminalTypes.SAS_HOST:
            _alarm_description = Resource.String("STR_FRM_CONTAINER_EVENTS_DATAGRID_HEADER_TERMINAL") + ": ";
            break;

          default:
            break;
        }
#else
        _alarm_description = "Terminal: ";
#endif

        if (_alarm != ALARM_BIRTHDAY.DO_NOTHING)
        {
          switch (_alarm)
          {
            case ALARM_BIRTHDAY.SEND_BIRTHDATE_ALARM:
              _alarm_code = 2097160;// AlarmCode.CustomAlarm_Player_Birthday;
              _alarm_description += TerminalName;
              break;

            case ALARM_BIRTHDAY.SEND_BIRTHDATE_IS_NEAR:
              _alarm_code = 2097161;// AlarmCode.CustomAlarm_Soon_Player_Birthday;
              _alarm_description += TerminalName;
              break;

            default:
              break;
          }

#if !SQL_BUSINESS_LOGIC
          Alarm.Register(AlarmSourceCode.User, AccountId, HolderName, (UInt32)_alarm_code, _alarm_description, AlarmSeverity.Info);
#else
          MultiPromos.Register(3, AccountId, HolderName, (UInt32)_alarm_code, _alarm_description, 1, Trx);
#endif
        }

        return true;
      }
#if !SQL_BUSINESS_LOGIC
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
#else
      catch
      { ; }
#endif
      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE :  Same function as Alarm.Register. Used to 3GS BirthDay feature.
    //
    //  PARAMS :
    //      - INPUT :
    //          - SourceCode,
    //          - SourceId,
    //          - SourceName,
    //          - AlarmCode,
    //          - Description,
    //          - Severity,
    //          - SqlTrx)
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    //
    public static Boolean Register(Int64 SourceCode,
                               long SourceId,
                               String SourceName,
                               UInt32 AlarmCode,
                               String Description,
                               Int32 Severity,
                               SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" INSERT INTO  ALARMS (AL_SOURCE_CODE       ");
        _sb.AppendLine("                    , AL_SOURCE_ID         ");
        _sb.AppendLine("                    , AL_SOURCE_NAME       ");
        _sb.AppendLine("                    , AL_ALARM_CODE        ");
        _sb.AppendLine("                    , AL_ALARM_NAME        ");
        _sb.AppendLine("                    , AL_ALARM_DESCRIPTION ");
        _sb.AppendLine("                    , AL_SEVERITY          ");
        _sb.AppendLine("                    , AL_REPORTED          ");
        _sb.AppendLine("                    , AL_DATETIME          ");
        _sb.AppendLine("                     )                     ");
        _sb.AppendLine("              VALUES (@pSourceCode         ");
        _sb.AppendLine("                    , @pSourceId           ");
        _sb.AppendLine("                    , @pSourceName         ");
        _sb.AppendLine("                    , @pAlarmCode          ");
        _sb.AppendLine("                    , @pAlarmName          ");
        _sb.AppendLine("                    , @pAlarmDescription   ");
        _sb.AppendLine("                    , @pSeverity           ");
        _sb.AppendLine("                    , GETDATE ()           ");
        _sb.AppendLine("                    , GETDATE ()           ");
        _sb.AppendLine("                     )                     ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pSourceCode", SqlDbType.Int).Value = SourceCode;
          _sql_cmd.Parameters.Add("@pSourceId", SqlDbType.BigInt).Value = SourceId;
          _sql_cmd.Parameters.Add("@pSourceName", SqlDbType.NVarChar).Value = SourceName;
          _sql_cmd.Parameters.Add("@pAlarmCode", SqlDbType.Int).Value = AlarmCode;
          _sql_cmd.Parameters.Add("@pAlarmName", SqlDbType.NVarChar).Value = "User";
          _sql_cmd.Parameters.Add("@pAlarmDescription", SqlDbType.NVarChar).Value = Description;
          _sql_cmd.Parameters.Add("@pSeverity", SqlDbType.Int).Value = Severity;

          _sql_cmd.ExecuteNonQuery();

          return true;
        }
      }

      catch
      { ; }

      return false;
    } // Register

    //////////public static Boolean Trx_GetAccountInfo(Int64 AccountId, out AccountInfo AccountInfo, SqlTransaction Trx)
    //////////{
    //////////  StringBuilder _sb;

    //////////  AccountInfo = new AccountInfo();
    //////////  _sb = new StringBuilder();

    //////////  try
    //////////  {
    //////////    _sb.AppendLine(" SELECT   AC_ACCOUNT_ID              ");
    //////////    _sb.AppendLine("        , AC_BLOCKED                 ");
    //////////    _sb.AppendLine("        , ISNULL(AC_HOLDER_NAME, '') ");
    //////////    _sb.AppendLine("        , AC_HOLDER_LEVEL            ");
    //////////    _sb.AppendLine("        , AC_HOLDER_GENDER           ");
    //////////    _sb.AppendLine("   FROM   ACCOUNTS ");
    //////////    _sb.AppendLine("  WHERE   AC_ACCOUNT_ID = @pAccountId ");

    //////////    using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
    //////////    {
    //////////      _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;

    //////////      using (SqlDataReader _reader = _cmd.ExecuteReader())
    //////////      {
    //////////        if (!_reader.Read())
    //////////        {
    //////////          return false;
    //////////        }

    //////////        AccountInfo.AccountId = _reader.GetInt64(0);
    //////////        AccountInfo.Blocked = _reader.GetBoolean(1);
    //////////        AccountInfo.HolderName = _reader.GetString(2);
    //////////        AccountInfo.HolderLevel = _reader.GetInt32(3);
    //////////        AccountInfo.HolderGender = _reader.GetInt32(4);

    //////////      }
    //////////    }

    //////////    return true;
    //////////  }
    //////////  catch
    //////////  { ; }

    //////////  return false;
    //////////} // Trx_GetAccountInfo

    //////////public static Boolean Trx_GetAccountInfo(String ExternalTrackData, out AccountInfo AccountInfo, SqlTransaction Trx)
    //////////{
    //////////  String _internal_track_data;
    //////////  Int32 _card_type;
    //////////  Object _obj;
    //////////  Int64 _account_id;

    //////////  AccountInfo = new AccountInfo();

    //////////  if (!CardNumber.TrackDataToCardNumber(ExternalTrackData, out _internal_track_data, out _card_type))
    //////////  {
    //////////    return false;
    //////////  }

    //////////  try
    //////////  {
    //////////    using (SqlCommand _cmd = new SqlCommand("SELECT AC_ACCOUNT_ID FROM ACCOUNTS WHERE AC_TRACK_DATA = @pInternal", Trx.Connection, Trx))
    //////////    {
    //////////      _cmd.Parameters.Add("@pInternal", SqlDbType.NVarChar, 50).Value = _internal_track_data.Trim();
    //////////      _obj = _cmd.ExecuteScalar();

    //////////      if (_obj == null || _obj == DBNull.Value)
    //////////      {
    //////////        return false;
    //////////      }

    //////////      _account_id = (Int64)_obj;
    //////////    }

    //////////    return Trx_GetAccountInfo(_account_id, out AccountInfo, Trx);
    //////////  }
    //////////  catch
    //////////  { ; }

    //////////  return false;
    //////////}

    //------------------------------------------------------------------------------
    // PURPOSE :  Update account promotion with balance trasferred to machine.
    //
    //  PARAMS :
    //      - INPUT :
    //          - AccountId
    //          - BalanceToAdd
    //          - SqlTrx
    //
    // RETURNS :
    //      - True:  OK
    //      - False: Otherwhise.
    //
    private static Boolean Trx_TITO_UpdateAccountPromotionBalance(Int64 AccountId,
                                                                  AccountBalance BalanceToTransfer,
                                                                  Int64 TerminalId,
                                                                  DataTable PromosToConsume,
                                                                  SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int64 _last_promo_id_redeemed;
      Decimal _balance_rest;
      Decimal _promo_balance;

      try
      {
        _last_promo_id_redeemed = 0;

        if (PromosToConsume.Rows.Count == 0
          || BalanceToTransfer.TotalNotRedeemable == 0)
        {
          return true;
        }

        // Update Account promotions with result

        _sb = new StringBuilder();
        _sb.AppendLine(" UPDATE   ACCOUNT_PROMOTIONS ");
        _sb.AppendLine("    SET   ACP_BALANCE = @pBalanceRest ");
        _sb.AppendLine("        , ACP_STATUS  = @pNewStatus ");
        _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS WITH(INDEX(PK_account_promotions)) ");
        _sb.AppendLine("  WHERE   ACP_UNIQUE_ID       = @pUniqueId ");
        _sb.AppendLine("    AND   ACP_ACCOUNT_ID      = @pAccountId ");
        _sb.AppendLine("    AND   ACP_STATUS          = @pStatusActive ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          SqlParameter _unique_id;
          SqlParameter _balance;
          SqlParameter _new_status;

          _unique_id = _cmd.Parameters.Add("@pUniqueId", SqlDbType.BigInt);
          _balance = _cmd.Parameters.Add("@pBalanceRest", SqlDbType.Money);
          _new_status = _cmd.Parameters.Add("@pNewStatus", SqlDbType.Int);

          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;

          _balance_rest = BalanceToTransfer.TotalNotRedeemable;
          foreach (DataRow _promo in PromosToConsume.Rows)
          {
            // to set in table terminals
            _last_promo_id_redeemed = (Int64)_promo[0];

            _unique_id.Value = _last_promo_id_redeemed;
            _promo_balance = (Decimal)_promo[2];

            if (_promo_balance >= _balance_rest)
            {
              _balance.Value = _promo_balance - _balance_rest;
              if (_promo_balance == _balance_rest)
              {
                _new_status.Value = ACCOUNT_PROMO_STATUS.EXHAUSTED;
              }
              else
              {
                _new_status.Value = ACCOUNT_PROMO_STATUS.ACTIVE;
              }

              if (_cmd.ExecuteNonQuery() != 1)
              {
                return false;
              }

              break;
            }
            else
            {
              _balance_rest -= _promo_balance;
              _balance.Value = 0;
              // with value 0 mark promo as exhausted
              _new_status.Value = ACCOUNT_PROMO_STATUS.EXHAUSTED;

              if (_cmd.ExecuteNonQuery() != 1)
              {
                return false;
              }
            }
          }
        }

        if (_last_promo_id_redeemed > 0)
        {
          DB_UpdateTerminalAccountPromotion(TerminalId, _last_promo_id_redeemed, SqlTrx);
        }

        return true;
      }
#if !SQL_BUSINESS_LOGIC
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
#else
      catch
      { ; }
#endif

      return false;

    }

    public static Boolean DB_UpdateTerminalAccountPromotion(Int64 TerminalId, Int64 PromoId, SqlTransaction SqlTrx)
    {
      // MPO 08-JUN-2015: Create this method  
      // Update terminals with last promotion applied
      // http://vstfs:8080/tfs/Gaming%20Products/Wigos%20Project/_workitems#_a=edit&id=249 
      try
      {
        using (SqlCommand _cmd = new SqlCommand("UPDATE TERMINALS SET TE_ACCOUNT_PROMOTION_ID = @pPromoId WHERE TE_TERMINAL_ID = @pTerminalId", SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pPromoId", SqlDbType.BigInt).Value = PromoId;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
        }

        return true;
      }
#if !SQL_BUSINESS_LOGIC
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
#else
      catch
      { ; }
#endif

      return false;
    } // DB_UpdateTerminalAccountPromotion

    // PURPOSE: Cancel an start card session launched from a transfer credit in a LCD touch
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - InSessionReDelta:
    //           - InSessionNrDelta:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    public static Boolean Trx_TITO_TransferFromAccount_CancelStartCardSession(Int32 TerminalId,
                                                                                  Int64 TransactionId,
                                                                                  AccountBalance BalanceCancelled,
                                                                                  out Boolean Cancelled,
                                                                                  out Int64 ClosedPlaySessionId,
                                                                                  SqlTransaction Trx)
    {
      StringBuilder _sb;
      AccountBalance _final_balance;
      Int64 _player_account_id;
      Int64 _account_promotion_id;
      Int64 _play_session_id;
      String _terminal_name;
      Boolean _rollback;
      Decimal _initial_balance;
      AccountMovementsTable _am_table;

      _rollback = true;
      Cancelled = false;
      // Session isn't closed
      ClosedPlaySessionId = 0;

      try
      {
        _final_balance = new AccountBalance();

        _sb = new StringBuilder();
        _sb.AppendLine("    SELECT   AC_ACCOUNT_ID                                                    ");
        _sb.AppendLine("           , ISNULL(TE_ACCOUNT_PROMOTION_ID, 0)                               ");
        _sb.AppendLine("           , PS_PLAY_SESSION_ID                                               ");
        _sb.AppendLine("           , TE_NAME                                                          ");
        _sb.AppendLine("      FROM   TERMINALS                                                        ");
        _sb.AppendLine(" LEFT JOIN   PLAY_SESSIONS ON PS_PLAY_SESSION_ID = TE_CURRENT_PLAY_SESSION_ID ");
        _sb.AppendLine(" LEFT JOIN   ACCOUNTS      ON AC_ACCOUNT_ID      = PS_ACCOUNT_ID              ");
        _sb.AppendLine("     WHERE   TE_TERMINAL_ID = @pTerminalId                                    ");
        _sb.AppendLine("       AND   (   AC_TYPE        = @pAccountType                               ");
        _sb.AppendLine("              OR AC_TYPE        = @pVirtualAccountType )                      ");
        _sb.AppendLine("       AND   PS_STATUS      = @pStatus                                        ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _sql_cmd.Parameters.Add("@pAccountType", SqlDbType.Int).Value = AccountType.ACCOUNT_WIN;
          _sql_cmd.Parameters.Add("@pVirtualAccountType", SqlDbType.Int).Value = AccountType.ACCOUNT_VIRTUAL_TERMINAL;
          _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = PlaySessionStatus.Opened;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              _player_account_id = _reader.GetInt64(0);
              _account_promotion_id = _reader.GetInt64(1);
              _play_session_id = _reader.GetInt64(2);
              _terminal_name = _reader.GetString(3);
            }
            else
            {
              // Not found
#if !SQL_BUSINESS_LOGIC
              Log.Error(String.Format("Trx_TITO_CancelStartCardSession not found. Terminal {0} - Balance {1}.", TerminalId.ToString(), BalanceCancelled.ToString()));
#endif
              return true;
            }
          }
        }

        Trx.Save("CancelStartCardSession");

        // Update account balance
        if (!Trx_UpdateAccountBalance(_player_account_id, BalanceCancelled, out _final_balance, Trx))
        {
          return false;
        }

        // Decrease 'TotalInitialBalance'
        if (!Trx_BalanceToPlaySession(false, _play_session_id, AccountBalance.Negate(BalanceCancelled), true, Trx))
        {
          return false;
        }

        //XCD 15-MAY-2015 do it only with promotional credit
        if ((BalanceCancelled.PromoRedeemable + BalanceCancelled.PromoNotRedeemable) > 0)
        {
          // Add credit to last promotion awarded and set status to active
          _sb.Length = 0;
          _sb.AppendLine(" UPDATE   ACCOUNT_PROMOTIONS ");
          _sb.AppendLine("    SET   ACP_BALANCE    = ACP_BALANCE + @pNRBalance ");
          _sb.AppendLine("        , ACP_STATUS     = @pStatusActive ");
          _sb.AppendLine("  WHERE   ACP_UNIQUE_ID  = @pAccountPromotionId ");
          _sb.AppendLine("    AND   ACP_ACCOUNT_ID = @pAccountId ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            _sql_cmd.Parameters.Add("@pNRBalance", SqlDbType.Money).Value = BalanceCancelled.TotalBalance;
            _sql_cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;
            _sql_cmd.Parameters.Add("@pAccountPromotionId", SqlDbType.BigInt).Value = _account_promotion_id;
            _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = _player_account_id;

            if (_sql_cmd.ExecuteNonQuery() != 1)
            {
              return false;
            }
          }
        }

        // 06-APR-2016 FJC Fixed Bug 11201:Lcd Touch: Cuando el terminal rechaza cr�ditos Nr, se crea un start session
        _initial_balance = _final_balance.TotalBalance - BalanceCancelled.TotalBalance;
        _am_table = new AccountMovementsTable(TerminalId, _terminal_name, _play_session_id);
        if (!_am_table.Add(0, _player_account_id, MovementType.CancelStartSession,
                           _initial_balance, 0, BalanceCancelled.TotalBalance, _final_balance.TotalBalance))
        {
          return false;
        }

        if (!_am_table.Save(Trx))
        {
          return false;
        }

        _rollback = false;
        Cancelled = true;

        return true;
      }
      catch
      { ; }
      finally
      {
        if (_rollback)
        {
          try
          {
            Trx.Rollback("CancelStartCardSession");
          }
          catch
          { }
        }
      }

      return false;
    } // Trx_CancelStartCardSession

    // PURPOSE: Update Reserved Credit from GameGateway
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - RequestAmount:
    //           - RequestType:
    //     - OUTPUT:
    //           - Status
    //
    // RETURNS:
    //     - TRUE or FALSE
    public static Boolean Trx_GameGateway_Reserve_Credit(Int64 AccountId,
                                                         Decimal RequestAmount,
                                                         REQUEST_TYPE RequestType,
                                                         out REQUEST_TYPE_RESPONSE Status)
    {
      return Trx_GameGateway_Reserve_Credit(AccountId,
                                            RequestAmount,
                                            RequestType,
                                            true,
                                            out Status);
    } // Trx_GameGateway_Reserve_Credit


    // PURPOSE: Update Reserved Credit from GameGateway
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId:
    //           - RequestAmount:
    //           - RequestType:
    //           - ModeReserved:
    //     - OUTPUT:
    //           - Status
    //
    // RETURNS:
    //     - TRUE or FALSE
    public static Boolean Trx_GameGateway_Reserve_Credit(Int64 AccountId,
                                                         Decimal RequestAmount,
                                                         REQUEST_TYPE RequestType,
                                                         Boolean ModeReserved,
                                                         out REQUEST_TYPE_RESPONSE Status)
    {

      Status = REQUEST_TYPE_RESPONSE.REQUEST_TYPE_RESPONSE_ERROR; //error
#if !SQL_BUSINESS_LOGIC

      Decimal _dumy_reserved_credit;
      _dumy_reserved_credit = 0;

      try
      {

        using (DB_TRX _trx = new DB_TRX())
        {
          if (Trx_GameGateway_Reserve_Credit(AccountId,
                                            RequestAmount,
                                            RequestType,
                                            ModeReserved,
                                            out Status,
                                            out _dumy_reserved_credit,
                                            _trx.SqlTransaction))
          {
            _trx.Commit();

            return true;
          }
        }
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
#endif
      return false; // ERROR
    } // Trx_GameGateway_Reserve_Credit

    // PURPOSE: Update Reserved Credit from GameGateway
    //
    //  PARAMS:
    //     - INPUT:
    //           - AccountId
    //           - RequestAmount
    //           - RequestType
    //           - ModeReserved
    //           - Trx:
    //     - OUTPUT:
    //           - Status
    //
    // RETURNS:
    //     - TRUE or FALSE
    public static Boolean Trx_GameGateway_Reserve_Credit(Int64 AccountId,
                                                     Decimal RequestAmount,
                                                     REQUEST_TYPE RequestType,
                                                     Boolean ModeReserved,
                                                     out REQUEST_TYPE_RESPONSE Status,
                                                     out Decimal ReservedInitialBalance,
                                                     SqlTransaction Trx)
    {
      MultiPromos.AccountBalance _target_initial_balance;
      Int64 _play_session_id;
      StringBuilder _sb;

      ReservedInitialBalance = 0;
      Status = REQUEST_TYPE_RESPONSE.REQUEST_TYPE_RESPONSE_ERROR; //ERROR

      try
      {
        if (RequestAmount != 0 || RequestType == REQUEST_TYPE.MODIFY_RESERVED_CREDIT)
        {
          _sb = new StringBuilder();

          // Obtain Target Account Balance
          if (MultiPromos.Trx_UpdateAccountBalance(AccountId, MultiPromos.AccountBalance.Zero, out _target_initial_balance, out _play_session_id, Trx))
          {
            ReservedInitialBalance = _target_initial_balance.Reserved;
            switch (RequestType)
            {
              case REQUEST_TYPE.REPORT_RESERVED_CREDIT:
                if (RequestAmount < 0) //SAS-HOST send negative number when need to reserve all credit
                {
                  //Reserve Credit (Reserve all credit available)
                  RequestAmount = Math.Min(_target_initial_balance.TotalRedeemable - _target_initial_balance.Reserved, _target_initial_balance.TotalRedeemable);
                }
                else
                {
                  //Reserve Credit (Reserve X credit )
                  RequestAmount = Math.Min(_target_initial_balance.TotalRedeemable - _target_initial_balance.Reserved, RequestAmount);
                }

                break;

              case REQUEST_TYPE.CANCEL_RESERVED_CREDIT:
                if (RequestAmount < 0) //SAS-HOST send negative number when need to cancel all credit
                {
                  RequestAmount = _target_initial_balance.Reserved * -1;
                }
                else
                {
                  RequestAmount = Math.Min(_target_initial_balance.Reserved, RequestAmount) * -1;
                }

                break;

              case REQUEST_TYPE.MODIFY_RESERVED_CREDIT:
                if (RequestAmount > _target_initial_balance.TotalRedeemable)
                {
                  Status = REQUEST_TYPE_RESPONSE.REQUEST_TYPE_RESPONSE_NOT_ENOUGH_CREDIT;

                  return false;
                }

                break;

              default:
                return false;
            }

            _sb = new StringBuilder();

            _sb.AppendLine(" UPDATE   ACCOUNTS                                   ");
            if (RequestType == REQUEST_TYPE.MODIFY_RESERVED_CREDIT)
            {
              _sb.AppendLine("    SET   AC_RE_RESERVED = @pAmount                  ");
              _sb.AppendLine("        , AC_MODE_RESERVED = @pModeReserved          ");
            }
            else
            {
              _sb.AppendLine("    SET   AC_RE_RESERVED = AC_RE_RESERVED + @pAmount ");
            }

            _sb.AppendLine("  WHERE   AC_ACCOUNT_ID  = @pAccountId               ");

            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
            {
              _sql_cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = RequestAmount;
              _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
              if (RequestType == REQUEST_TYPE.MODIFY_RESERVED_CREDIT)
              {
                _sql_cmd.Parameters.Add("@pModeReserved", SqlDbType.Bit).Value = ModeReserved;
              }

              if (_sql_cmd.ExecuteNonQuery() == 1)
              {
                Status = 0;

                return true; // ALL WENT OK
              }
            }
          }
        }//if (_amount != 0)
      }
#if !SQL_BUSINESS_LOGIC
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
#else
      catch
      { ; }
#endif

      return false; // ERROR
    } // Trx_GameGateway_Reserve_Credit


    // PURPOSE: Returns if the funcionality - FBM reserved credit -  is enable
    //
    //  PARAMS:
    //     - INPUT:
    //           - SqlTrx:.
    //     - OUTPUT:
    //           - FBMProvider:.
    //
    // RETURNS:
    //     - TRUE or FALSE
    public static Boolean Trx_GetFBMReservedCreditEnabled(out String FBMProvider, SqlTransaction SqlTrx)
    {
      FBMProvider = String.Empty;
      try
      {
        FBMProvider = ReadGeneralParams("WS2S", "VendorId", SqlTrx);
      }
      catch
      {
      }
      return !String.IsNullOrEmpty(FBMProvider);
    }//Trx_GetFBMReservedCreditEnabled

    #endregion

    #region Private Methods

    // PURPOSE: Compute not redemeeble, delta won and delta won not redeemable and update the new balance and won.
    //
    //  PARAMS:
    //     - INPUT:
    //           - Params:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    private static void WonPromoNotRedeemable(InSessionParameters Params, Boolean SimulateWonLock)
    {
      Decimal _promo_delta_won;

      foreach (DataRow _promo in Params.AccountPromotion.Rows)
      {
        if (Params.DeltaWon <= 0)
        {
          break;
        }

        if ((ACCOUNT_PROMO_CREDIT_TYPE)_promo["ACP_CREDIT_TYPE"] != ACCOUNT_PROMO_CREDIT_TYPE.NR1)
        {
          continue;
        }

        if (!_promo.IsNull("ACP_WONLOCK") || SimulateWonLock)
        {
          _promo_delta_won = Params.DeltaWon;
        }
        else
        {
          _promo_delta_won = (Decimal)_promo["ACP_WITHHOLD"] - Math.Min((Decimal)_promo["ACP_BALANCE"], (Decimal)_promo["ACP_WITHHOLD"]);
          _promo_delta_won = Math.Min(_promo_delta_won, Params.DeltaWon);
        }

        if (_promo_delta_won <= 0)
        {
          continue;
        }

        _promo["ACP_BALANCE"] = (Decimal)_promo["ACP_BALANCE"] + _promo_delta_won;
        _promo["ACP_WON"] = (Decimal)_promo["ACP_WON"] + _promo_delta_won;
        Params.DeltaWon -= _promo_delta_won;
        Params.DeltaWonNr += _promo_delta_won;
        Params.Balance.PromoNotRedeemable += _promo_delta_won;

      }
    } // WonPromoNotRedeemable

    // PURPOSE: Comouthe redeemable, delta won and delta won redeemble.
    //
    //  PARAMS:
    //     - INPUT:
    //           - Params:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    private static void WonRedeemable(InSessionParameters Params)
    {
      Decimal _promo_delta_won;

      if (Params.DeltaWon <= 0)
      {
        return;
      }

      _promo_delta_won = Params.DeltaWon;
      Params.Balance.Redeemable += _promo_delta_won;
      Params.DeltaWonRe += _promo_delta_won;
      Params.DeltaWon -= _promo_delta_won;
    } // WonRedeemable

    // PURPOSE: Compute not redeemable, delta played not redeemable and delta played. Update the balance and played.
    //                                                                               
    //  PARAMS:
    //     - INPUT:
    //           - CreditType:
    //           - Params:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    private static void PlayPromoNotRedeemableBalance(ACCOUNT_PROMO_CREDIT_TYPE CreditType, InSessionParameters Params)
    {
      Decimal _promo_delta_played;

      foreach (DataRow _promo in Params.AccountPromotion.Rows)
      {
        if (Params.DeltaPlayed <= 0)
        {
          break;
        }

        if ((ACCOUNT_PROMO_CREDIT_TYPE)_promo["ACP_CREDIT_TYPE"] != CreditType)
        {
          continue;
        }

        _promo_delta_played = Math.Min((Decimal)_promo["ACP_BALANCE"], Params.DeltaPlayed);
        _promo_delta_played = Math.Min(_promo_delta_played, Params.Balance.PromoNotRedeemable);

        if (_promo_delta_played <= 0)
        {
          continue;
        }

        _promo["ACP_BALANCE"] = (Decimal)_promo["ACP_BALANCE"] - _promo_delta_played;
        _promo["ACP_PLAYED"] = (Decimal)_promo["ACP_PLAYED"] + _promo_delta_played;
        Params.DeltaPlayedNr += _promo_delta_played;
        Params.DeltaPlayed -= _promo_delta_played;
        Params.Balance.PromoNotRedeemable -= _promo_delta_played;
      }
    } // PlayPromoBalance

    // PURPOSE: Compute not redeemable, delta played not redeemable and delta played. Update the balance and played.
    //                                                                               
    //  PARAMS:
    //     - INPUT:
    //           - CreditType:
    //           - Params:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    private static void SpentRemainingInSessionPromoNR(ACCOUNT_PROMO_CREDIT_TYPE CreditType, InSessionParameters Params)
    {
      Decimal _promo_delta;

      foreach (DataRow _promo in Params.AccountPromotion.Rows)
      {
        if (Params.DeltaBalanceFoundInSession.PromoNotRedeemable <= 0)
        {
          break;
        }

        if ((ACCOUNT_PROMO_CREDIT_TYPE)_promo["ACP_CREDIT_TYPE"] != CreditType)
        {
          continue;
        }

        _promo_delta = Math.Min((Decimal)_promo["ACP_BALANCE"], Params.DeltaBalanceFoundInSession.PromoNotRedeemable);

        if (_promo_delta <= 0)
        {
          continue;
        }

        _promo["ACP_BALANCE"] = (Decimal)_promo["ACP_BALANCE"] - _promo_delta;
        _promo["ACP_PLAYED"] = (Decimal)_promo["ACP_PLAYED"] + _promo_delta;
        Params.DeltaBalanceFoundInSession.PromoNotRedeemable -= _promo_delta;
      }
    } // PlayPromoBalance

    private static void SpentRemainingInSessionPromoRE(ACCOUNT_PROMO_CREDIT_TYPE CreditType, InSessionParameters Params)
    {
      Decimal _promo_delta;

      foreach (DataRow _promo in Params.AccountPromotion.Rows)
      {
        if (Params.DeltaBalanceFoundInSession.PromoRedeemable <= 0)
        {
          break;
        }

        if ((ACCOUNT_PROMO_CREDIT_TYPE)_promo["ACP_CREDIT_TYPE"] != CreditType)
        {
          continue;
        }

        _promo_delta = Math.Min((Decimal)_promo["ACP_BALANCE"], Params.DeltaBalanceFoundInSession.PromoRedeemable);

        if (_promo_delta <= 0)
        {
          continue;
        }

        _promo["ACP_BALANCE"] = (Decimal)_promo["ACP_BALANCE"] - _promo_delta;
        _promo["ACP_PLAYED"] = (Decimal)_promo["ACP_PLAYED"] + _promo_delta;
        Params.DeltaBalanceFoundInSession.PromoRedeemable -= _promo_delta;
      }
    } // PlayPromoBalance

    // PURPOSE: Compute the delta played not redeemed and played. Update the new balance and played.
    //
    //  PARAMS:
    //     - INPUT:
    //           - CreditType: NR1 or NR1
    //           - Params: 
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    private static void PlayPromoPrizeOnly(ACCOUNT_PROMO_CREDIT_TYPE CreditType, InSessionParameters Params)
    {
      Decimal _promo_delta_played;
      Decimal _promo_prize;

      foreach (DataRow _promo in Params.AccountPromotion.Rows)
      {
        if (Params.DeltaPlayed <= 0)
        {
          break;
        }

        if ((ACCOUNT_PROMO_CREDIT_TYPE)_promo["ACP_CREDIT_TYPE"] != CreditType)
        {
          continue;
        }

        if (_promo.IsNull("ACP_WONLOCK"))
        {
          continue;
        }

        _promo_prize = Math.Max(0, (Decimal)_promo["ACP_BALANCE"] - (Decimal)_promo["ACP_WITHHOLD"]);
        _promo_delta_played = Math.Min(Params.DeltaPlayed, _promo_prize);
        _promo_delta_played = Math.Min(Params.Balance.PromoNotRedeemable, _promo_delta_played);

        if (_promo_delta_played <= 0)
        {
          continue;
        }

        _promo["ACP_BALANCE"] = (Decimal)_promo["ACP_BALANCE"] - _promo_delta_played;
        _promo["ACP_PLAYED"] = (Decimal)_promo["ACP_PLAYED"] + _promo_delta_played;
        Params.DeltaPlayedNr += _promo_delta_played;
        Params.DeltaPlayed -= _promo_delta_played;
        Params.Balance.PromoNotRedeemable -= _promo_delta_played;
      }

    } // PlayPromoPrizeOnly

    // PURPOSE: Compute the redeemable played and played.
    //
    //  PARAMS:
    //     - INPUT:
    //           - Params:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //     - none

    private static void PlayRedeemable(InSessionParameters Params)
    {
      Decimal _delta_played;


      _delta_played = Math.Min(Params.Balance.PromoRedeemable, Params.DeltaPlayed);
      if (_delta_played > 0)
      {
        Params.Balance.PromoRedeemable -= _delta_played;
        Params.DeltaPlayedRe += _delta_played;
        Params.DeltaPlayed -= _delta_played;
      }

      _delta_played = Math.Min(Params.Balance.Redeemable, Params.DeltaPlayed);
      if (_delta_played > 0)
      {
        Params.Balance.Redeemable -= _delta_played;
        Params.DeltaPlayedRe += _delta_played;
        Params.DeltaPlayed -= _delta_played;
      }

    } // PlayRedeemable

    //------------------------------------------------------------------------------
    // PURPOSE: Closes a Play Session
    //
    //  PARAMS:
    //      - INPUT:
    //          - PlaySessionId
    //          - AccountID
    //          - FinalBalance
    //          - IsPromotion
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //      - Nothing
    //        
    //   NOTES :
    //
    private static Boolean Trx_PlaySessionChangeStatus(PlaySessionStatus DesiredStatus, Int64 PlaySessionId, 
                                                       out PlaySessionStatus OldStatus, out PlaySessionStatus NewStatus, out TimeSpan PlayTimeSpan,
                                                       SqlTransaction Trx)
    {
      StringBuilder _sb;
      DateTime _play_time_started;
      DateTime _play_time_finished;

      OldStatus = PlaySessionStatus.Opened;
      NewStatus = PlaySessionStatus.Opened;
      _play_time_started = new DateTime();
      _play_time_finished = new DateTime();
      PlayTimeSpan = new TimeSpan();

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("UPDATE   PLAY_SESSIONS ");
        _sb.AppendLine("   SET   PS_STATUS          = CASE WHEN PS_STATUS IN (@pStatusOpened) THEN @pNewStatus ELSE PS_STATUS   END ");
        _sb.AppendLine("       , PS_LOCKED          = CASE WHEN PS_STATUS IN (@pStatusOpened) THEN NULL        ELSE PS_LOCKED   END ");
        _sb.AppendLine("       , PS_FINISHED        = CASE WHEN PS_STATUS IN (@pStatusOpened) THEN GETDATE()   ELSE PS_FINISHED END ");
        _sb.AppendLine("OUTPUT   DELETED.PS_STATUS ");
        _sb.AppendLine("       , INSERTED.PS_STATUS ");
        _sb.AppendLine("       , DELETED.PS_STARTED ");
        _sb.AppendLine("       , ISNULL(DELETED.PS_FINISHED, GETDATE()) ");
        _sb.AppendLine(" WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;
          _cmd.Parameters.Add("@pStatusOpened", SqlDbType.Int).Value = PlaySessionStatus.Opened;
          _cmd.Parameters.Add("@pNewStatus", SqlDbType.Int).Value = DesiredStatus;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }
            OldStatus = (PlaySessionStatus)_reader.GetInt32(0);
            NewStatus = (PlaySessionStatus)_reader.GetInt32(1);
            _play_time_started = _reader.GetDateTime(2);
            _play_time_finished = _reader.GetDateTime(3);

            PlayTimeSpan = _play_time_finished.Subtract(_play_time_started);

            return true;
          }
        }
      }
      catch
      {
      }

      return false;
    } // Trx_PlaySessionChangeStatus

    //------------------------------------------------------------------------------
    // PURPOSE : Set ps_balance_mismatch flag and ps_final_balance into play session 
    //
    //  PARAMS :
    //      - INPUT :
    //          - PlaySessionId
    //          - EfectiveFinalBalance
    //          - ReportedFinalBalance
    //          - BalanceMismatch
    //          - MovementType
    //          - Trx
    //
    //      - OUTPUT :
    //          - None
    //
    // RETURNS :
    //      - True: Saved ok. False: Otherwise.
    //
    public static Boolean Trx_PlaySessionSetFinalBalance(Int64 PlaySessionId,
                                                          Decimal EfectiveFinalBalance,
                                                          Decimal ReportedFinalBalance,
                                                          Boolean BalanceMismatch,
                                                          MovementType MovementType,
                                                          SqlTransaction Trx)
    {
      String _sql_txt;

      // if CreditBalance and CreditBalanceReported is negative we assign 0.
      EfectiveFinalBalance = Math.Max(0, EfectiveFinalBalance);
      ReportedFinalBalance = Math.Max(0, ReportedFinalBalance);

      // Add the BalanceMismatch and CreditBalance to the play session       
      _sql_txt = " UPDATE   PLAY_SESSIONS";
      _sql_txt += "   SET   PS_REPORTED_BALANCE_MISMATCH = CASE WHEN (@pBalanceMismatch = 1) THEN @pReportedBalance ELSE PS_REPORTED_BALANCE_MISMATCH END ";
      _sql_txt += "       , PS_FINAL_BALANCE             = @pFinalBalance ";
      _sql_txt += "       , PS_BALANCE_MISMATCH          = @pBalanceMismatch ";
      _sql_txt += " WHERE   PS_PLAY_SESSION_ID           = @pPlaySessionID ";

      // RCI & AJQ 30-MAY-2014: Fixed Bug WIG-939: For Manual EndSessions, Status can be Opened or Abandoned.
      if (MovementType == MovementType.ManualEndSession)
      {
        _sql_txt += "   AND   PS_STATUS                    IN ( @pStatusOpened, @pStatusAbandoned ) ";
      }
      else
      {
        _sql_txt += "   AND   PS_STATUS                    = @pStatusOpened ";
      }

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sql_txt, Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPlaySessionID", SqlDbType.BigInt).Value = PlaySessionId;
          _cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).Value = EfectiveFinalBalance;
          _cmd.Parameters.Add("@pReportedBalance", SqlDbType.Money).Value = ReportedFinalBalance;
          _cmd.Parameters.Add("@pBalanceMismatch", SqlDbType.Bit).Value = BalanceMismatch;
          _cmd.Parameters.Add("@pStatusOpened", SqlDbType.Int).Value = (Int32)PlaySessionStatus.Opened;
          _cmd.Parameters.Add("@pStatusAbandoned", SqlDbType.Int).Value = (Int32)PlaySessionStatus.Abandoned;

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch
      { }

      return false;
    } // Trx_PlaySessionSetFinalBalance


    private static Boolean Trx_PlaySessionMarkAsAbandoned(Int64 PlaySessionId, StringBuilder ErrorMsg, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("   UPDATE   PLAY_SESSIONS                          ");
        _sb.AppendLine("      SET   PS_STATUS          = @pStatusAbandoned ");
        _sb.AppendLine("          , PS_FINISHED        = GETDATE()         ");
        _sb.AppendLine("    WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId   ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pStatusAbandoned", SqlDbType.Int).Value = PlaySessionStatus.Abandoned;
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

          return (_cmd.ExecuteNonQuery() == 1);
        }
      }
      catch (Exception _ex)
      {
        ErrorMsg.AppendLine(_ex.Message);
      }

      return false;
    } // Trx_PlaySessionMarkAsAbandoned

    public static Boolean Trx_ReleaseInactivePlaySession(Int64 PlaySessionId, Decimal FinalBalance, Int64 AccountId,
                                                         Int32 TerminalId, TerminalTypes TerminalType, String TerminalName,
                                                         String SystemCashierName,
                                                         out StringBuilder ErrorMsg, SqlTransaction Trx)
    {
      ErrorMsg = new StringBuilder();
      EndSessionInput _end_input;
      EndSessionOutput _end_output;
      AccountMovementsTable _account_movements;

      try
      {
        _end_input = new MultiPromos.EndSessionInput();       
        _end_input.MovementType = MovementType.ManualEndSession;
        _end_input.AccountId = AccountId;
        _end_input.CashierName = SystemCashierName;
        _end_input.BalanceFromGM = FinalBalance;
        _end_input.HasMeters = true;
        _end_input.PlaySessionId = PlaySessionId;
        _end_input.TransactionId = 0;
        
#if !SQL_BUSINESS_LOGIC
        _end_input.IsMico2 = Misc.IsMico2Mode();
        _end_input.IsTITO = TITO.Utils.IsTitoMode();
        if (_end_input.IsTITO)
        {
          _end_input.VirtualAccountId = Accounts.GetOrCreateVirtualAccount(TerminalId, Trx);

          if (!Trx_GetPlaySessionTITOSessionMeters(PlaySessionId, _end_input, Trx))
          {
            return false;
          }
        }
#endif

        if (!MultiPromos.Trx_OnEndCardSession(TerminalId, TerminalType, TerminalName, _end_input, out _end_output, Trx))
        {
          ErrorMsg.AppendLine(_end_output.Message);
          ErrorMsg.AppendLine("Trx_OnEndCardSession failed.");

          return false;
        }

        if (!MultiPromos.Trx_PlaySessionMarkAsAbandoned(PlaySessionId, ErrorMsg, Trx))
        {
          ErrorMsg.AppendLine("Trx_PlaySessionMarkAsAbandoned failed.");

          return false;
        }

        if (!MultiPromos.Trx_SetAccountBlocked(AccountId, AccountBlockReason.INACTIVE_PLAY_SESSION, Trx))
        {
          ErrorMsg.AppendLine("Trx_SetAccountBlocked failed.");

          return false;
        }

        _account_movements = new AccountMovementsTable(TerminalId, TerminalName, PlaySessionId);

        // Add account movement
        _account_movements.Add(0, AccountId, MovementType.AccountBlocked,
                               FinalBalance, 0, 0, FinalBalance, String.Empty);

        // Save accounts and cashier movements
        if (!_account_movements.Save(Trx))
        {
          return false;
        }


        return true;
      }
      catch (Exception _ex)
      {
        ErrorMsg.AppendLine(_ex.Message);
      }

      return false;
    } // Trx_ReleaseInactivePlaySession

    public static Boolean Trx_GetPlaySessionTITOSessionMeters(Int64 PlaySessionId, EndSessionInput Meters, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine(" SELECT   PS_PLAYED_COUNT ");
      _sb.AppendLine("        , PS_PLAYED_AMOUNT ");
      _sb.AppendLine("        , PS_WON_COUNT ");
      _sb.AppendLine("        , PS_WON_AMOUNT ");
      _sb.AppendLine("        , PS_REDEEMABLE_CASH_IN ");
      _sb.AppendLine("        , PS_PROMO_RE_TICKET_IN ");
      _sb.AppendLine("        , PS_PROMO_NR_TICKET_IN ");
      _sb.AppendLine("        , PS_RE_TICKET_OUT ");
      _sb.AppendLine("        , PS_PROMO_NR_TICKET_OUT ");
      _sb.AppendLine("        , PS_CASH_IN ");
      _sb.AppendLine("        , PS_REDEEMABLE_PLAYED ");
      _sb.AppendLine("        , PS_NON_REDEEMABLE_PLAYED ");
      _sb.AppendLine("   FROM   PLAY_SESSIONS ");
      _sb.AppendLine("  WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }
            Meters.Meters.PlayedCount = _reader.GetInt32(0);
            Meters.Meters.PlayedAmount = _reader.GetDecimal(1);
            Meters.Meters.WonCount = _reader.GetInt32(2);
            Meters.Meters.WonAmount = _reader.GetDecimal(3);
            Meters.TitoSessionMeters.TicketInCashable = _reader.IsDBNull(4) ? 0 : _reader.GetDecimal(4);
            Meters.TitoSessionMeters.TicketInPromoRe = _reader.IsDBNull(5) ? 0 : _reader.GetDecimal(5);
            Meters.TitoSessionMeters.TicketInPromoNr = _reader.IsDBNull(6) ? 0 : _reader.GetDecimal(6);
            Meters.TitoSessionMeters.TicketOutCashable = _reader.IsDBNull(7) ? 0 : _reader.GetDecimal(7);
            Meters.TitoSessionMeters.TicketOutPromoNr = _reader.IsDBNull(8) ? 0 : _reader.GetDecimal(8);
            Meters.TitoSessionMeters.CashIn.CashInTotal = _reader.IsDBNull(9) ? 0 : _reader.GetDecimal(9);        
            Meters.Meters.PlayedReedemable = _reader.IsDBNull(10) ? 0 : _reader.GetDecimal(10);
            Meters.Meters.PlayedRestrictedAmount = _reader.IsDBNull(11) ? 0 : _reader.GetDecimal(11);
          }

          return true;
        }
      }
      catch
      {
      }
      return false;

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Set, Add and Reset initial redeemable promotion in ACCOUNTS
    //
    //  PARAMS :
    //      - INPUT:
    //            - AccountId
    //            - Action 
    //            - Amount
    //            - SqlTrx
    //
    //      - OUTPUT :
    //            - None
    //
    // RETURNS :
    //      -  Updated ok. False: Otherwise. 
    public static Boolean Trx_AccountPromotionCost(Int64 AccountId,
                                                   ACCOUNTS_PROMO_COST Action,
                                                   Decimal Amount,
                                                   SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Int32 _num_active_nr_promos;

      try
      {
        _sb = new StringBuilder();
        _num_active_nr_promos = 0;

        switch (Action)
        {
          case ACCOUNTS_PROMO_COST.RESET_ON_REDEEM:
          case ACCOUNTS_PROMO_COST.SET_ON_FIRST_NR_PROMOTION:

            _sb.Length = 0;
            _sb.AppendLine(" SELECT   COUNT(1) ");
            _sb.AppendLine("   FROM   ACCOUNT_PROMOTIONS ");
            _sb.AppendLine("  WHERE   ACP_ACCOUNT_ID  = @pAccountId");
            _sb.AppendLine("    AND   ACP_STATUS      = @pStatusActive");
            _sb.AppendLine("    AND   ACP_CREDIT_TYPE IN (@pNR1,@pNR2)");

            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
            {
              _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
              _sql_cmd.Parameters.Add("@pNR1", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR1;
              _sql_cmd.Parameters.Add("@pNR2", SqlDbType.Int).Value = ACCOUNT_PROMO_CREDIT_TYPE.NR2;
              _sql_cmd.Parameters.Add("@pStatusActive", SqlDbType.Int).Value = ACCOUNT_PROMO_STATUS.ACTIVE;

              _num_active_nr_promos = (Int32)_sql_cmd.ExecuteScalar();
            }
            break;
        }

        switch (Action)
        {
          case ACCOUNTS_PROMO_COST.UPDATE_ON_RECHARGE:
          case ACCOUNTS_PROMO_COST.UPDATE_ON_REDEEM:
            if (Amount == 0)
            {
              return true;
            }

            // ADD AMOUNT recharge
            _sb.Length = 0;
            _sb.AppendLine("UPDATE   ACCOUNTS ");
            _sb.AppendLine("   SET   AC_PROMO_INI_RE_BALANCE = CASE WHEN ( AC_PROMO_INI_RE_BALANCE IS NOT NULL ) ");
            _sb.AppendLine("                              THEN CASE WHEN ( AC_PROMO_INI_RE_BALANCE + @pAmount >= 0) ");
            _sb.AppendLine("                              THEN AC_PROMO_INI_RE_BALANCE + @pAmount ");
            _sb.AppendLine("                              ELSE 0 END ELSE NULL END ");
            _sb.AppendLine(" WHERE   AC_ACCOUNT_ID = @pAccountId ");

            using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
            {
              _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
              _sql_command.Parameters.Add("@pAmount", SqlDbType.Money).Value = (Action == ACCOUNTS_PROMO_COST.UPDATE_ON_RECHARGE) ? Amount : -Amount;

              return (_sql_command.ExecuteNonQuery() == 1);
            }

          case ACCOUNTS_PROMO_COST.RESET_ON_RECHARGE:
          case ACCOUNTS_PROMO_COST.RESET_ON_REDEEM:
          case ACCOUNTS_PROMO_COST.SET_ON_FIRST_NR_PROMOTION:

            _sb.Length = 0;
            _sb.AppendLine("UPDATE   ACCOUNTS ");
            _sb.AppendLine("   SET   AC_PROMO_INI_RE_BALANCE    = @pAmount");
            _sb.AppendLine(" WHERE   AC_ACCOUNT_ID = @pAccountId ");
            using (SqlCommand _sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
            {
              _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
              _sql_command.Parameters.Add("@pAmount", SqlDbType.Money);

              if (Action == ACCOUNTS_PROMO_COST.RESET_ON_REDEEM)
              {
                if (_num_active_nr_promos == 0)
                {
                  _sql_command.Parameters["@pAmount"].Value = DBNull.Value;
                }
                else
                {
                  _sql_command.Parameters["@pAmount"].Value = Amount;
                }
              }
              else if (_num_active_nr_promos == 1 && Action == ACCOUNTS_PROMO_COST.SET_ON_FIRST_NR_PROMOTION)
              {
                _sql_command.Parameters["@pAmount"].Value = Amount;
              }
              // JML 28-Jan-2014 Reset cost promotions when recharging in the following cases: extra ball or the amount NR less than GP
              else if (Action == ACCOUNTS_PROMO_COST.RESET_ON_RECHARGE)
              {
                _sql_command.Parameters["@pAmount"].Value = DBNull.Value;
              }
              else
              {
                // Nothing to do
                return true;
              }

              return (_sql_command.ExecuteNonQuery() == 1);
            }

          default:
            return false;
        }
      }
      catch { ; }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Insert "No Open Play Session" into MS_PENDING_GAME_PLAY_SESSIONS
    //          for send to Multisite Center
    //
    //  PARAMS:
    //      - INPUT:
    //          - PlaySessionId
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //        
    //   NOTES :
    //
    private static Boolean Trx_InsertAgroupGamePlaySession(Int64 PlaySessionId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _multi_site_member;

      Int32.TryParse(ReadGeneralParams("Site", "MultiSiteMember", Trx), out _multi_site_member);
      if (_multi_site_member == 0)
      {
        return true;
      }

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("INSERT INTO   MS_PENDING_GAME_PLAY_SESSIONS");
        _sb.AppendLine("              (MPS_PLAY_SESSION_ID)");
        _sb.AppendLine("     VALUES   (@pPlaySessionId) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
          return true;
        }
      }
      catch
      {
      }

      return false;
    } // Trx_PlaySessionChangeStatus

    //------------------------------------------------------------------------------
    // PURPOSE: Inserts a command into WCP_COMMANDS
    //
    //  PARAMS:
    //      - INPUT:
    //          - TerminalId
    //          - PlaySessionId
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //          - True:               Executed succesfully
    //          - False:              Executed unsuccesfully
    //        
    //   NOTES :
    //
    private static Boolean InsertWcpCommand(Int32 TerminalId, Int64 PlaySessionId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("INSERT INTO WCP_COMMANDS ");
        _sb.AppendLine("           (CMD_TERMINAL_ID ");
        _sb.AppendLine("          , CMD_CODE ");
        _sb.AppendLine("          , CMD_STATUS ");
        _sb.AppendLine("          , CMD_CREATED ");
        _sb.AppendLine("          , CMD_STATUS_CHANGED ");
        _sb.AppendLine("          , CMD_PS_ID) ");
        _sb.AppendLine("     VALUES ( @TerminalId , 1 , 0 , GETDATE() , GETDATE() ,@PlaySessionId )");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@TerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@PlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
          return true;
        }
      }
      catch
      {
      }

      return false;
    } // InsertWcpCommand

    //------------------------------------------------------------------------------
    // PURPOSE: Check Max Won to Revise
    //
    //  PARAMS:
    //      - INPUT:
    //          - WonPoints
    //          - TimePlayed
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //          - True:               Revise
    //          - False:              Not Revise
    //        
    //   NOTES :
    //
    private static Boolean Trx_PlaySessionToRevise(Terminal.TerminalInfo Terminal,
                                                   Decimal ComputedPoints,
                                                   TimeSpan PlayTimeSpan,
                                                   PlaySessionCloseParameters Params,
                                                   EndSessionOutput Output,
                                                   out Boolean PointsAwarded,
                                                   SqlTransaction Trx)
    {
      Int32 _max_points;
      Int32 _max_points_per_minute;
      Int16 _has_zero_plays;
      Int16 _has_mismatch;
      AwardPointsStatus _awarded_points_status;
      Decimal _awarded_points;
      StringBuilder _warning;
      StringBuilder _warning_header;
      StringBuilder _sb;

      PointsAwarded = true;
      _awarded_points = ComputedPoints;

      _warning = new StringBuilder();
      _warning_header = new StringBuilder();
      _max_points = 0;
      _max_points_per_minute = 0;
      _has_zero_plays = 0;
      _has_mismatch = 0;

      _sb = new StringBuilder();
      _sb.Length = 0;
      _sb.AppendLine(" SELECT   GP_KEY_VALUE ");
      _sb.AppendLine("   FROM   GENERAL_PARAMS ");
      _sb.AppendLine("  WHERE   GP_GROUP_KEY   = 'PlayerTracking.DoNotAwardPoints' ");
      _sb.AppendLine("    AND   GP_SUBJECT_KEY = 'PlaySession.PointsAreGreaterThan'");
      _sb.AppendLine("  UNION ALL ");
      _sb.AppendLine(" SELECT   GP_KEY_VALUE ");
      _sb.AppendLine("   FROM   GENERAL_PARAMS ");
      _sb.AppendLine("  WHERE   GP_GROUP_KEY   = 'PlayerTracking.DoNotAwardPoints' ");
      _sb.AppendLine("    AND   GP_SUBJECT_KEY = 'PlaySession.PointsPerMinuteAreGreaterThan'");
      _sb.AppendLine("  UNION ALL ");
      _sb.AppendLine(" SELECT   GP_KEY_VALUE ");
      _sb.AppendLine("   FROM   GENERAL_PARAMS ");
      _sb.AppendLine("  WHERE   GP_GROUP_KEY   = 'PlayerTracking.DoNotAwardPoints' ");
      _sb.AppendLine("    AND   GP_SUBJECT_KEY = 'PlaySession.HasNoPlays' ");
      _sb.AppendLine("  UNION ALL ");
      _sb.AppendLine(" SELECT   GP_KEY_VALUE ");
      _sb.AppendLine("   FROM   GENERAL_PARAMS ");
      _sb.AppendLine("  WHERE   GP_GROUP_KEY   = 'PlayerTracking.DoNotAwardPoints' ");
      _sb.AppendLine("    AND   GP_SUBJECT_KEY = 'PlaySession.HasBalanceMismatch' ");
      _sb.AppendLine(" ;");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              if (!Int32.TryParse(_reader.GetString(0), out _max_points))
              {
                _max_points = 0;
              }
            }
            if (_reader.Read())
            {
              if (!Int32.TryParse(_reader.GetString(0), out _max_points_per_minute))
              {
                _max_points_per_minute = 0;
              }
            }
            if (_reader.Read())
            {
              if (!Int16.TryParse(_reader.GetString(0), out _has_zero_plays))
              {
                _has_zero_plays = 0;
              }
            }
            if (_reader.Read())
            {
              if (!Int16.TryParse(_reader.GetString(0), out _has_mismatch))
              {
                _has_mismatch = 0;
              }
            }
          }
        }

        if (PointsAwarded)
        {
          if (_max_points > 0 && _max_points < ComputedPoints)
          {
            PointsAwarded = false;
            _warning.AppendFormat("Computed Points: {0}", ComputedPoints.ToString());
            _warning.AppendFormat(", Points Are Greater Than: {0}", _max_points.ToString());
          }
        }

        if (PointsAwarded)
        {
          if (PlayTimeSpan.TotalMinutes > 0)
          {
            if (_max_points_per_minute > 0 && _max_points_per_minute < (ComputedPoints / (Decimal)PlayTimeSpan.TotalMinutes))
            {
              PointsAwarded = false;
              _warning.AppendFormat("Computed Points: {0}", ComputedPoints.ToString());
              _warning.AppendFormat(", Minutes Played: {0}", PlayTimeSpan.TotalMinutes.ToString("0.##"));
              _warning.AppendFormat(", Points Per Minute: {0}", ((Decimal)(ComputedPoints / (Decimal)PlayTimeSpan.TotalMinutes)).ToString("0.##"));
              _warning.AppendFormat(", Points Per Minute Are Greater Than: {0}", _max_points_per_minute.ToString());
            }
          }
        }

        if (PointsAwarded)
        {
          if (_has_zero_plays == 1 && Params.NumPlayed == 0)
          {
            if (Terminal.TerminalType == TerminalTypes.WIN)
            {
              // AJQ & MPO 31-OCT-2013
              //   - LKT WIN has the NumPlayed equal to 0 at this point, but has the right value on DB ...
              //   - Avoid the check till we update the code, till then continue awarding the points
            }
            else
            {
              PointsAwarded = false;
              _warning.AppendFormat("Num. Played: {0}", Params.NumPlayed.ToString());
              _warning.AppendFormat(", Has No Plays: True");
            }
          }
        }

        if (PointsAwarded)
        {
          if (_has_mismatch == 1 && Params.BalanceMismatch)
          {
            PointsAwarded = false;
            _warning.AppendFormat("Balance Mismatch: {0}", Params.BalanceMismatch.ToString());
            _warning.AppendFormat(", Has Balance Mismatch: True");
          }
        }

        _awarded_points_status = AwardPointsStatus.CalculatedAndReward;
        if (!PointsAwarded)
        {
          _awarded_points = 0;
          _awarded_points_status = AwardPointsStatus.Pending;

          _warning_header.Append("Play to revise -> ");
          _warning_header.AppendFormat("AccountId: {0}", Params.AccountId);
          _warning_header.AppendFormat(", TerminalId: {0}", Params.TerminalId);
          _warning_header.Append(" Criteria -> ");

          _warning.Insert(0, _warning_header.ToString());

          Output.Warning(_warning.ToString());
        }

        _sb = new StringBuilder();
        _sb.AppendLine("   UPDATE   PLAY_SESSIONS ");
        _sb.AppendLine("      SET   PS_AWARDED_POINTS_STATUS = CASE WHEN PS_STATUS <> @pStateOpened THEN @pPointsStatus   ELSE PS_AWARDED_POINTS_STATUS END ");
        _sb.AppendLine("          , PS_COMPUTED_POINTS       = CASE WHEN PS_STATUS <> @pStateOpened THEN @pComputedPoints ELSE NULL END ");
        _sb.AppendLine("          , PS_AWARDED_POINTS        = CASE WHEN PS_STATUS <> @pStateOpened THEN @pAwardedPoints  ELSE NULL END ");
        _sb.AppendLine("    WHERE   PS_PLAY_SESSION_ID       = @pPlaySessionId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pStateOpened", SqlDbType.Int).Value = (Int32)PlaySessionStatus.Opened;
          _cmd.Parameters.Add("@pPointsStatus", SqlDbType.Int).Value = (Int32)_awarded_points_status;
          _cmd.Parameters.Add("@pComputedPoints", SqlDbType.Money).Value = ComputedPoints;
          _cmd.Parameters.Add("@pAwardedPoints", SqlDbType.Money).Value = _awarded_points;
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = Params.PlaySessionId;

          return (_cmd.ExecuteNonQuery() == 1);
        }

      }
      catch
      { ;}

      return false;
    } // Trx_PlaySessionToRevise

    public static String ReadGeneralParams(String Group, String Subject, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;
      String _value;

      _value = "";

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT   GP_KEY_VALUE ");
        _sb.AppendLine("   FROM   GENERAL_PARAMS ");
        _sb.AppendLine("  WHERE   GP_GROUP_KEY    = @pGroup ");
        _sb.AppendLine("    AND   GP_SUBJECT_KEY  = @pSubject ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pGroup", SqlDbType.NVarChar, 50).Value = Group;
          _cmd.Parameters.Add("@pSubject", SqlDbType.NVarChar, 50).Value = Subject;

          _obj = _cmd.ExecuteScalar();
          if (_obj != null && _obj != DBNull.Value)
          {
            _value = (String)_obj;
          }
        }
      }
      catch
      { }

      return _value;
    } // ReadGeneralParams

    #endregion

    /// <summary>
    /// Get the next sequence
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <param name="SequenceId"></param>
    /// <param name="Value"></param>
    /// <returns></returns>
    public static Boolean Trx_SequenceGetValue(SqlTransaction SqlTrx, SequenceId SequenceId, out Int64 Value)
    {
      StringBuilder _sb;

      Value = 0;

      if (SequenceId == SequenceId.NotSet)
      {
        return false;
      }

      _sb = new StringBuilder();
      _sb.AppendLine("IF NOT EXISTS ( SELECT SEQ_NEXT_VALUE FROM SEQUENCES WHERE SEQ_ID = @pSeqId ) ");
      _sb.AppendLine("  INSERT INTO SEQUENCES VALUES (@pSeqId, 1); ");
      _sb.AppendLine(" ");
      _sb.AppendLine("UPDATE   SEQUENCES ");
      _sb.AppendLine("   SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1 ");
      _sb.AppendLine("OUTPUT   DELETED.SEQ_NEXT_VALUE ");
      _sb.AppendLine(" WHERE   SEQ_ID    = @pSeqId ");

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pSeqId", SqlDbType.Int).Value = SequenceId;

          Value = (Int64)_sql_cmd.ExecuteScalar();

          return true;
        }
      }
      catch (Exception)
      {
      }
      return false;
    } // GetValue


    /// <summary>
    /// Update play session with sequence of voucher
    /// </summary>
    /// <param name="PlaySessionId"></param>
    /// <param name="Sequence"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    private static Boolean Trx_PlaySessionUpdateSequence(Int64 PlaySessionId, Int64 Sequence, SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("  UPDATE   PLAY_SESSIONS                        ");
        _sb.AppendLine("     SET   PS_SEQUENCE = @pSequence             ");
        _sb.AppendLine("   WHERE   PS_PLAY_SESSION_ID = @pPlaySessionId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.BigInt).Value = PlaySessionId;
          _cmd.Parameters.Add("@pSequence", SqlDbType.BigInt).Value = Sequence;


          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }

          return true;
        }
      }
      catch
      {
      }

      return false;
    } // Trx_PlaySessionUpdateSequence

  }

}