//------------------------------------------------------------------------------
// Copyright � 2015 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: SharedGeneralParams.cs
// 
//   DESCRIPTION: Class to manage General Parameters (in Shared)
// 
//        AUTHOR: Ra�l Cervera
// 
// CREATION DATE: 12-MAR-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAR-2015 RCI & JML    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace WSI.Common
{
  public static partial class GeneralParam
  {
    public partial class Dictionary : Dictionary<String, String>
    {
      internal const String ALIAS_PREFIX = "$$ALIAS$$";

      public static Dictionary Create(SqlTransaction Trx)
      {
        Dictionary _dic;
        StringBuilder _sb;

        try
        {
          _dic = new Dictionary();

          _sb = new StringBuilder();
          _sb.AppendLine(" SELECT GP_GROUP_KEY, GP_SUBJECT_KEY, ISNULL(GP_KEY_VALUE, '') FROM GENERAL_PARAMS ");
          _sb.AppendLine(" ; ");
          // Alias only for CASHIER. If needed for every application, remove the WHERE part.
          _sb.AppendLine(" SELECT APP_NAME, UPPER(APP_MACHINE), ISNULL(APP_ALIAS, UPPER(APP_MACHINE)) FROM APPLICATIONS WHERE APP_NAME = 'CASHIER' ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              // Read GPs
              while (_reader.Read())
              {
                _dic.Add(_reader.GetString(0) + "." + _reader.GetString(1), _reader.GetString(2));
              }
              // Advance to Alias
              _reader.NextResult();
              while (_reader.Read())
              {
                _dic.Add(ALIAS_PREFIX + "." + _reader.GetString(0) + "." + _reader.GetString(1), _reader.GetString(2));
              }
            }
          }

          return _dic;
        }
        catch
        {
          return null;
        }
      }

      public static Dictionary Create(SqlTransaction Trx, string SpecifiedGroup)
      {
        Dictionary _dic;
        StringBuilder _sb;

        try
        {
          _dic = new Dictionary();

          _sb = new StringBuilder();
          _sb.AppendLine(
            string.Format(
              " SELECT GP_GROUP_KEY, GP_SUBJECT_KEY, ISNULL(GP_KEY_VALUE, '') FROM GENERAL_PARAMS where GP_GROUP_KEY = '{0}'",
              SpecifiedGroup));
          _sb.AppendLine(" ; ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              // Read GPs
              while (_reader.Read())
              {
                _dic.Add(_reader.GetString(0) + "." + _reader.GetString(1), _reader.GetString(2));
              }
              // Advance to Alias
            }
          }

          return _dic;
        }
        catch
        {
          return null;
        }
      }


      //------------------------------------------------------------------------------
      // PURPOSE : Return the value from the General Params (in memory)
      //
      //  PARAMS :
      //      - INPUT :
      //          - GroupKey
      //          - SubjectKey
      //
      //      - OUTPUT :
      //
      // RETURNS :
      //        The value of the selected General Param
      //
      //   NOTES :
      //
      public String Value(String GroupKey, String SubjectKey)
      {
        String _key;
        String _value;

        // Build the Key
        _key = GroupKey + "." + SubjectKey;

        // Get the value

        if (!TryGetValue(_key, out _value))
        {
          _value = String.Empty;
        }

        return _value;
      } // Value

      public Int32 GetInt32(String GroupKey, String SubjectKey, Int32 DefaultValue, Int32 MinValue, Int32 MaxValue)
      {
        String _str_value;

        Int32 _value;

        _str_value = Value(GroupKey, SubjectKey);

        if (String.IsNullOrEmpty(_str_value))
        {
          return DefaultValue;
        }

        if (!Int32.TryParse(_str_value, out _value))
        {
          return DefaultValue;
        }
        if (_value < MinValue)
        {
          _value = MinValue;
        }
        if (_value > MaxValue)
        {
          _value = MaxValue;
        }
        return _value;
      }

      public Int32 GetInt32(String GroupKey, String SubjectKey)
      {
        return GetInt32(GroupKey, SubjectKey, 0, Int32.MinValue, Int32.MaxValue);
      }

      public Int32 GetInt32(String GroupKey, String SubjectKey, Int32 DefaultValue)
      {
        return GetInt32(GroupKey, SubjectKey, DefaultValue, Int32.MinValue, Int32.MaxValue);
      }

      public Boolean GetBoolean(String GroupKey, String SubjectKey, Boolean DefaultValue)
      {
        return GetInt32(GroupKey, SubjectKey, DefaultValue ? 1 : 0) != 0;
      }

      public Boolean GetBoolean(String GroupKey, String SubjectKey)
      {
        return GetBoolean(GroupKey, SubjectKey, false);
      }


      public Decimal GetDecimal(String GroupKey, String SubjectKey, Decimal DefaultValue)
      {
        String _str_value;
        Decimal _value;

        _str_value = Value(GroupKey, SubjectKey);

        if (String.IsNullOrEmpty(_str_value))
        {
          return DefaultValue;
        }

        if (!Decimal.TryParse(_str_value,
                              System.Globalization.NumberStyles.Any,
                              System.Globalization.CultureInfo.GetCultureInfo("en-US").NumberFormat,
                              out _value))
        {
          return DefaultValue;
        }

        return _value;
      }

      public Decimal GetDecimal(String GroupKey, String SubjectKey)
      {
        return GetDecimal(GroupKey, SubjectKey, 0);
      }

#if !SQL_BUSINESS_LOGIC && !SQL_WIGOS_BL02
      public Currency GetCurrency(String GroupKey, String SubjectKey, Currency DefaultValue)
      {
        return GetDecimal(GroupKey, SubjectKey, DefaultValue);
      }

      public Currency GetCurrency(String GroupKey, String SubjectKey)
      {
        return GetDecimal(GroupKey, SubjectKey, 0);
      }
#endif

      public String GetString(String GroupKey, String SubjectKey)
      {
        return GetString(GroupKey, SubjectKey, String.Empty);
      }

      public String GetString(String GroupKey, String SubjectKey, String DefaultValue)
      {
        String _value;

        _value = Value(GroupKey, SubjectKey);

        if (String.IsNullOrEmpty(_value))
        {
          _value = DefaultValue;
        }

        return _value;
      }

      public Int64 GetInt64(String GroupKey, String SubjectKey, Int32 DefaultValue)
      {
        String _str_value;
        Int64 _value;

        _str_value = Value(GroupKey, SubjectKey);

        if (String.IsNullOrEmpty(_str_value))
        {
          return DefaultValue;
        }

        if (!Int64.TryParse(_str_value, out _value))
        {
          return DefaultValue;
        }

        return _value;
      }

      public Int64 GetInt64(String GroupKey, String SubjectKey)
      {
        return GetInt64(GroupKey, SubjectKey, 0);
      }
    }
  }

  public static partial class GeneralParam
  {
    //public Boolean m_print_voucher;

    public partial class OperationVoucherDictionary : Dictionary<OperationCode, VoucherParameters>
    {

      public static OperationVoucherDictionary Create(SqlTransaction Trx)
      {
        OperationVoucherDictionary _dic;
        StringBuilder _sb;

        try
        {
          // Get operation voucher parameters
          _dic = new OperationVoucherDictionary();

          _sb = new StringBuilder();
          _sb.AppendLine(" SELECT   OVP_OPERATION_CODE ");
          _sb.AppendLine("        , OVP_GENERATE ");
          _sb.AppendLine("        , OVP_PRINT ");
          _sb.AppendLine("        , OVP_PRINT_COPY ");
          _sb.AppendLine("   FROM   OPERATION_VOUCHER_PARAMETERS ");
          _sb.AppendLine("  WHERE   OVP_ENABLED = 1 ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
          {
            using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
            {
              // Read operation voucher parameters
              while (_reader.Read())
              {
                _dic.Add((OperationCode)_reader.GetInt32(0), new VoucherParameters(_reader.GetBoolean(1), _reader.GetBoolean(2), _reader.GetInt32(3)));
              }
            }
          }

          return _dic;
        }
        catch
        {
          return null;
        }
      }
    }

    public class VoucherParameters
    {
      public Boolean Generate;
      public Boolean Print;
      public Int32 PrintCopy;

      public VoucherParameters(Boolean Generate, Boolean Print, Int32 PrintCopy)
      {
        this.Generate = Generate;
        this.Print = Print;
        this.PrintCopy = PrintCopy;
      }
    }
  }
}
