using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common
{
  [Flags]
  public enum SAS_FLAGS
  {
    NONE = 0x00000000,
    USE_EXTENDED_METERS = 0x00000001,
    CREDITS_PLAY_MODE_SAS_MACHINE = 0x00000002,
    USE_HANDPAY_INFORMATION = 0x00000004,
    SPECIAL_PROGRESSIVE_METER = 0x00000008,
    USE_METER_FIXED_4BDC_LENGTH_BIT0 = 0x00000010,
    USE_METER_FIXED_4BDC_LENGTH_BIT1 = 0x00000020,
    USE_METER_FIXED_4BDC_LENGTH_BIT2 = 0x00000040,
    USE_METER_FIXED_4BDC_LENGTH_BIT3 = 0x00000080,
    USE_METER_FIXED_5BDC_LENGTH_BIT0 = 0x00000100,
    USE_METER_FIXED_5BDC_LENGTH_BIT1 = 0x00000200,
    USE_METER_FIXED_5BDC_LENGTH_BIT2 = 0x00000400,
    USE_METER_FIXED_5BDC_LENGTH_BIT3 = 0x00000800,
    USE_FLAGS_FORCE_NO_RTE = 0x00001000,
    ALLOW_SINGLE_BYTE = 0x00002000,
    SAS_FLAGS_IGNORE_NO_BET_PLAYS = 0x00004000,
    SAS_FLAG_DISABLED_AFT_ON_INTRUSION = 0x00008000,
    SAS_FLAGS_AFT_ALTHOUGH_LOCK_ZERO = 0x00010000,
    SAS_FLAGS_ENABLE_DISABLE_NOTE_ACCEPTOR = 0x00020000,
    SAS_FLAGS_AUTOMATIC_HANDPAY_PAYMENT = 0x00040000,
    SAS_FLAGS_RESERVE_TERMINAL = 0x00080000,
    SAS_FLAGS_LOCK_EGM_ON_RESERVE = 0x00100000,
  }

  public enum ACCOUNT_PROMO_CREDIT_TYPE
  {
    UNKNOWN = 0,
    NR1 = 1,      // Non-Redeemable - Spent at the end (with withhold)
    NR2 = 2,      // Non-Redeemable - Spent at beggining
    REDEEMABLE = 3,
    POINT = 4,
    UNR1 = 5,  // Spent at the end
    UNR2 = 6,  // Spent at beggining
    NR3 = 7,   // Non-Redeemable - Spent at the end (without withhold)
    REDEEMABLE_IN_KIND = 8,   // Redeemable in kind
  }

  public enum ACCOUNT_PROMO_STATUS
  {
    UNKNOWN = 0,
    AWARDED = 1,
    ACTIVE = 2,
    EXHAUSTED = 3,
    EXPIRED = 4,
    REDEEMED = 5,
    NOT_AWARDED = 6,
    ERROR = 7,
    CANCELLED_NOT_ADDED = 8,
    CANCELLED = 9,
    CANCELLED_BY_PLAYER = 10,
    PREASSIGNED = 11,
    PENDING_PLAYER = 12,
    PENDING_DRAW = 13,    
    BOUGHT = 14
  }

  //SSC 23-MAR-2012
  public enum GU_USER_TYPE
  {
    NOT_ASSIGNED = -1,
    USER = 0,
    SYS_ACCEPTOR = 1,
    SYS_PROMOBOX = 2,
    SYS_SYSTEM = 3,
    SYS_TITO = 4,
    SYS_GAMING_TABLE = 5,
    SYS_REDEMPTION = 6,
    SYS_C2GO = 7,
    SUPERUSER = 10,
  }

  public enum CASHIER_MOVEMENT
  {
    NOT_ASSIGNED = -1,
    OPEN_SESSION = 0,
    CLOSE_SESSION = 1,
    FILLER_IN = 2,
    FILLER_OUT = 3,
    CASH_IN = 4,
    CASH_OUT = 5,
    TAX_ON_PRIZE1 = 6,
    PROMOTION_NOT_REDEEMABLE = 7,
    PRIZES = 8,
    CARD_DEPOSIT_IN = 9,
    CARD_DEPOSIT_OUT = 10,
    CANCEL_NOT_REDEEMABLE = 11,
    MB_MANUAL_CHANGE_LIMIT = 12, //ManualChangeLimit
    MB_CASH_IN = 13,
    TAX_ON_PRIZE2 = 14,
    // AJQ 19-FEB-2010, Promotions
    PROMO_CREDITS = 20,
    PROMO_TO_REDEEMABLE = 21,
    PROMO_EXPIRED = 22,
    PROMO_CANCEL = 23,
    PROMO_START_SESSION = 24,
    PROMO_END_SESSION = 25,
    // TJG 04-MAR-2010 Replacement Cost for Personal Cards
    CARD_REPLACEMENT = 28,
    // RCI 14-JUN-2010 Draw Ticket Print
    DRAW_TICKET_PRINT = 29,
    // TJG 21-JUL-2010 Hand Pays
    HANDPAY = 30,
    HANDPAY_CANCELLATION = 31,
    // APB 31-AUG-2010 More Hand Pays
    MANUAL_HANDPAY = 32,
    MANUAL_HANDPAY_CANCELLATION = 33,
    // MBF 01-SEP-2010 Cash-in Split
    CASH_IN_SPLIT2 = 34,
    MB_CASH_IN_SPLIT2 = 35,
    DEV_SPLIT2 = 36,
    CASH_IN_SPLIT1 = 37,
    DEV_SPLIT1 = 38,
    MB_CASH_IN_SPLIT1 = 39,
    CANCEL_SPLIT1 = 40,
    CANCEL_SPLIT2 = 41,
    // RCI 29-OCT-2010: Draw Ticket in exchange of points
    POINTS_TO_DRAW_TICKET_PRINT = 42,
    PRIZE_COUPON = 43,
    // MBF 11-OCT-2011
    CARD_CREATION = 44,
    CARD_PERSONALIZATION = 45,
    // JMM 22-NOV-2011
    ACCOUNT_PIN_CHANGE = 46,
    ACCOUNT_PIN_RANDOM = 47,
    // RCI 17-FEB-2012
    CASH_IN_COVER_COUPON = 48,
    CASH_IN_NOT_REDEEMABLE2 = 49, // For future promotions
    // JMM 29-FEB-2012
    MB_CASH_IN_NOT_REDEEMABLE = 50,
    MB_CASH_IN_COVER_COUPON = 51,
    MB_CASH_IN_NOT_REDEEMABLE2 = 52,   // For future promotions
    MB_PRIZE_COUPON = 53,
    // SSC 08-MAR-2012: Added new movements for Note Acceptor
    NA_CASH_IN_SPLIT1 = 54,
    NA_CASH_IN_SPLIT2 = 55,
    NA_PRIZE_COUPON = 56,
    NA_CASH_IN_NOT_REDEEMABLE = 57,
    NA_CASH_IN_COVER_COUPON = 58,
    // JCM 24-APR-2012
    CARD_RECYCLED = 59,
    //SSC 11-JUN-2012
    POINTS_TO_NOT_REDEEMABLE = 60,
    POINTS_TO_REDEEMABLE = 61,

    // AJQ 25-JUN-2012
    REDEEMABLE_DEV_EXPIRED = 62,
    REDEEMABLE_PRIZE_EXPIRED = 63,
    NOT_REDEEMABLE_EXPIRED = 64,

    PROMOTION_REDEEMABLE = 65,
    CANCEL_PROMOTION_REDEEMABLE = 66,

    PROMOTION_POINT = 67,
    CANCEL_PROMOTION_POINT = 68,

    // AJQ 30-AUG-2012: Tax returning (redondeo por retenci�n)
    TAX_RETURNING_ON_PRIZE_COUPON = 69,
    // AJQ 10-SEP-2012: Decimal Rounding (redondeo por decimales)
    DECIMAL_ROUNDING = 70,
    // RRB 12-SEP-2012: Service Charge (carga por servicio)
    SERVICE_CHARGE = 71,
    // JCM 13-SEP-2012: Mobile Bank delivered cash on the MB Session Close 
    MB_CASH_LOST = 72,
    // DDM 27-SEP-2012: 
    REQUEST_TO_VOUCHER_REPRINT = 73,
    // JCM 17-JAN-2013: Payment Order (Orden de Pago, importe del cheque)
    CHECK_PAYMENT = 74,
    // QMP 02-MAY-2013: Mark Cashier Session as Pending Closing
    CASH_PENDING_CLOSING = 75,

    // JBP 07-MAY-2013: Add Block /Unblock account movements
    ACCOUNT_BLOCKED = 76,
    ACCOUNT_UNBLOCKED = 77,

    // ICS 12-JUN-2013: Currency Exchange movements
    CURRENCY_EXCHANGE_SPLIT1 = 78,
    CURRENCY_CARD_EXCHANGE_SPLIT1 = 79,
    CASINO_CHIPS_EXCHANGE_SPLIT1 = 80,

    //LEM 30-OCT-2013: TITO
    TITO_TICKET_CASHIER_PRINTED_CASHABLE = 84,

    // POINTS -> RECHARGE
    // RCI & FBA 26-AUG-2013: Need to split movements for: AsCashIn, CurrencyExchange, BankCard and CasinoChips.
    POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT1 = 85,
    POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT2 = 86,

    CURRENCY_EXCHANGE_SPLIT2 = 87,
    CURRENCY_CARD_EXCHANGE_SPLIT2 = 88,
    CASINO_CHIPS_EXCHANGE_SPLIT2 = 89,

    // NMR 02-SEP-2013: Cashier movement when reprinting ticket
    REQUEST_TO_TICKET_REPRINT = 90,

    // SMN 05-SEP-2013: Cashier movement for Participation in draw
    PARTICIPATION_IN_CASHDESK_DRAW = 91,

    //DLL 10-SEP-2013: Cashier movement Check
    CURRENCY_CHECK_SPLIT1 = 92,
    CURRENCY_CHECK_SPLIT2 = 93,


    PRIZE_IN_KIND1_GROSS = 94, // Prize UNR
    PRIZE_IN_KIND1_TAX1 = 95, // Prize UNR
    PRIZE_IN_KIND1_TAX2 = 96, // Prize UNR

    //DLL 13-SEP-2013: Cashier movement Close Cash Check/Bank Card and Cashier movement Fill out Check
    CLOSE_SESSION_BANK_CARD = 97,
    CLOSE_SESSION_CHECK = 98,
    FILLER_OUT_CHECK = 99,

    PRIZE_IN_KIND2_GROSS = 100, // Prize UNR Taxes paid by the Casino
    PRIZE_IN_KIND2_TAX1 = 101, // Prize UNR
    PRIZE_IN_KIND2_TAX2 = 102, // Prize UNR

    //LEM 30-OCT-2013: TITO
    TITO_TICKET_CASHIER_PAID_CASHABLE = 103,
    // JPJ 29-JAN-2014
    TITO_TICKET_CASHIER_PRINTED_PROMO_REDEEMABLE = 104,
    TITO_TICKET_MACHINE_PLAYED_CASHABLE = 105,
    TITO_TICKET_CASHIER_PRINTED_PROMO_NOT_REDEEMABLE = 106,
    TITO_TICKET_REISSUE = 107,
    TITO_TICKET_CREATE_CASHABLE_OFFLINE = 108,
    TITO_TICKET_CREATE_PLAYABLE_OFFLINE = 109,

    TITO_TICKET_EXPIRED_PROMO_REDEEMABLE = 110,
    TITO_TICKET_EXPIRED_PROMO_NOT_REDEEMABLE = 111,
    TITO_TICKET_EXPIRED_REDEEMABLE = 112,
    TITO_TICKET_CASHABLE_REDEEMED_OFFLINE = 113,
    TITO_TICKET_PLAYABLE_REDEEMED_OFFLINE = 114,
    TITO_TICKET_MACHINE_PRINTED_HANDPAY = 115,
    TITO_TICKET_MACHINE_PRINTED_JACKPOT = 116,
    TITO_TICKET_CASHIER_PAID_HANDPAY = 117,
    TITO_TICKET_CASHIER_PAID_JACKPOT = 118,
    //JML 07-FEB-2014 
    TITO_VALIDATE_TICKET = 119,
    // DHA 11-APR-2014 
    TITO_TICKET_MACHINE_PRINTED_CASHABLE = 120,
    TITO_TICKET_MACHINE_PRINTED_PROMO_NOT_REDEEMABLE = 121,
    TITO_TICKET_MACHINE_PLAYED_PROMO_REDEEMABLE = 122,
    TITO_TICKET_MACHINE_PLAYED_PROMO_NOT_REDEEMABLE = 123,
    TITO_TICKET_CASHIER_PAID_PROMO_REDEEMABLE = 124,
    TITO_TICKET_CASHIER_EXPIRED_PAID_CASHABLE = 125,
    TITO_TICKET_CASHIER_EXPIRED_PAID_PROMO_REDEEMABLE = 126,
    TITO_TICKET_CASHIER_EXPIRED_PAID_JACKPOT = 127,
    TITO_TICKET_CASHIER_EXPIRED_PAID_HANDPAY = 128,

    TITO_LAST_MOVEMENT = 139,
    // TITO Movements reserved to 139

    // RMS 13-JAN-2014: Transfer credit between accounts
    TRANSFER_CREDIT_OUT = 140,
    TRANSFER_CREDIT_IN = 141,

    // RCI 28-JAN-2014
    CASH_IN_TAX_SPLIT1 = 142,

    // AJQ 29-MAY-2014, Report the prize due to obtainig redeemable per points
    POINTS_TO_REDEEMABLE_AS_CASHIN_PRIZE = 143,

    //AMF 07-JUL-2014
    CARD_REPLACEMENT_FOR_FREE = 144,
    CARD_REPLACEMENT_IN_POINTS = 145,

    //DRV 25-JUL-2014
    CASH_IN_TAX_SPLIT2 = 146,

    // RMS 08-AUG-2014
    CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1 = 147,
    CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1 = 148,
    CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2 = 149,
    CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2 = 150,

    // DLL 01-SEP-2014
    CASH_ADVANCE_CURRENCY_EXCHANGE = 151,
    CASH_ADVANCE_GENERIC_BANK_CARD = 152,
    CASH_ADVANCE_CREDIT_BANK_CARD = 153,
    CASH_ADVANCE_DEBIT_BANK_CARD = 154,
    CASH_ADVANCE_CHECK = 155,
    CASH_ADVANCE_CASH = 156,

    // DHA 08-OCT-2014
    NOT_PLAYED_RECHARGE_REFUNDED = 157,

    // JVV & RCI 10-OCT-2014
    CASH_CLOSING_SHORT_DEPRECATED = 158,  // deprecated, see 160
    CASH_CLOSING_OVER_DEPRECATED = 159,   // deprecated, see 161

    // JML 03-AUG-2015
    CASH_CLOSING_SHORT = 160,
    CASH_CLOSING_OVER = 161,

    ////JML 23-NOV-2015: 
    ////CHIPS_RE_OPEN_SESSION = 162, // Reserved
    //CHIPS_RE_CLOSE_SESSION = 163,
    //CHIPS_RE_FILLER_IN = 164,
    //CHIPS_RE_FILLER_OUT = 165,
    ////CHIPS_NRE_OPEN_SESSION = 166, // Reserved
    //CHIPS_NRE_CLOSE_SESSION = 167,
    //CHIPS_NRE_FILLER_IN = 168,
    //CHIPS_NRE_FILLER_OUT = 169,
    ////CHIPS_COLOR_OPEN_SESSION = 170, // Reserved
    //CHIPS_COLOR_CLOSE_SESSION = 171,
    //CHIPS_COLOR_FILLER_IN = 172,
    //CHIPS_COLOR_FILLER_OUT = 173,

    // DHA 09-JUN-2016: Closing cash
    FILLER_IN_CLOSING_STOCK = 190,
    FILLER_OUT_CLOSING_STOCK = 191,

    //FBA 14-NOV-2013: 2xx is for CAGE
    CAGE_OPEN_SESSION = 200,
    CAGE_CLOSE_SESSION = 201,
    CAGE_FILLER_IN = 202,
    CAGE_FILLER_OUT = 203,

    CAGE_CASHIER_COLLECT_DROPBOX_TERMINAL = 204,
    //CAGE_CASHIER_COLLECT_REFILL = 205, // Reserved
    CAGE_CASHIER_REFILL_HOPPER_TERMINAL = 206,

    // DHA 13-MAY-2016
    CAGE_CASHIER_COLLECT_DROPBOX_GAMINGTABLE = 207,
    CAGE_CASHIER_COLLECT_DROPBOX_GAMINGTABLE_DETAIL = 208,


    CAGE_LAST_MOVEMENT = 299,

    //SMN 12-DEC-2013: 3xx is for CHIPS
    CHIPS_SALE = 300,
    CHIPS_PURCHASE = 301,
    CHIPS_SALE_DEVOLUTION_FOR_TITO = 302,
    CHIPS_SALE_TOTAL = 303,
    CHIPS_PURCHASE_TOTAL = 304,
    CHIPS_CHANGE_IN = 305,
    CHIPS_CHANGE_OUT = 306,

    // DRV: Movements for integrated chip operations
    CHIPS_SALE_WITH_CASH_IN = 307,
    CHIPS_PURCHASE_WITH_CASH_OUT = 308,

    // DHA: movements with exchange chips conversion
    CHIPS_SALE_TOTAL_EXCHANGE = 309,
    CHIPS_PURCHASE_TOTAL_EXCHANGE = 310,

    // DRV: Movements for chips sale register
    CHIPS_SALE_REGISTER_TOTAL = 311,

    // JML: Informative momevement of the remaining amount after chips sales
    CHIPS_SALE_REMAINING_AMOUNT = 312,

    // JML: Informative momevement of the consumed remaining amount after chips sales
    CHIPS_SALE_CONSUMED_REMAINING_AMOUNT = 313,

    CHIPS_LAST_MOVEMENT = 399,

    //FBA-RRR 14-JAN-2014: Gaming Tables Quadrature
    GAMING_TABLE_GAME_NETWIN = 500,

    //DLL 07-FEB-2014: Gaming Tables Quadrature
    GAMING_TABLE_GAME_NETWIN_CLOSE = 501,

    // MPO 08-SEP-2014
    HANDPAY_AUTHORIZED = 510,
    HANDPAY_UNAUTHORIZED = 511,
    HANDPAY_VOIDED = 512,
    HANDPAY_VOIDED_UNDONE = 513,

    //JBC 21-OCT-2014
    MB_EXCESS = 514,

    //JBC 11-DEC-2014
    COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1 = 515,
    COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT2 = 516,
    COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1 = 517,
    COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2 = 518,
    COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1 = 519,
    COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2 = 520,

    // OPC 12-DEC-2014
    HANDPAY_WITHHOLDING = 521,

    // DLL 17-DEC-2014
    COMPANY_B_CURRENCY_EXCHANGE_SPLIT1 = 522,
    COMPANY_B_CURRENCY_EXCHANGE_SPLIT2 = 523,
    COMPANY_B_CURRENCY_CHECK_SPLIT1 = 524,
    COMPANY_B_CURRENCY_CHECK_SPLIT2 = 525,
    COMPANY_B_CASH_ADVANCE_CURRENCY_EXCHANGE = 526,
    COMPANY_B_CASH_ADVANCE_GENERIC_BANK_CARD = 527,
    COMPANY_B_CASH_ADVANCE_CREDIT_BANK_CARD = 528,
    COMPANY_B_CASH_ADVANCE_DEBIT_BANK_CARD = 529,
    COMPANY_B_CASH_ADVANCE_CHECK = 530,

    //FJC 29-APR-2015
    WIN_LOSS_STATEMENT_REQUEST = 531,

    //FOS 23-JUN-2015
    CARD_DEPOSIT_IN_CHECK = 532,
    CARD_DEPOSIT_IN_CURRENCY_EXCHANGE = 533,
    CARD_DEPOSIT_IN_CARD_CREDIT = 534,
    CARD_DEPOSIT_IN_CARD_DEBIT = 535,
    CARD_DEPOSIT_IN_CARD_GENERIC = 536,
    CARD_REPLACEMENT_CHECK = 537,
    CARD_REPLACEMENT_CARD_CREDIT = 538,
    CARD_REPLACEMENT_CARD_DEBIT = 539,
    CARD_REPLACEMENT_CARD_GENERIC = 540,    

    // DCS 01-JUL-2015
    COMPANY_B_CARD_DEPOSIT_IN = 541,
    COMPANY_B_CARD_DEPOSIT_OUT = 542,
    COMPANY_B_CARD_REPLACEMENT = 543,
    COMPANY_B_CARD_DEPOSIT_IN_CHECK = 544,
    COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE = 545,
    COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT = 546,
    COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT = 547,
    COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC = 548,
    COMPANY_B_CARD_REPLACEMENT_CHECK = 549,
    COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT = 550,
    COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT = 551,
    COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC = 552,

    // FAV 13-08-2015
    TRANSFER_CASHIERS_SEND = 553,
    TRANSFER_CASHIERS_RECEIVE = 554,

    // YNM 22-10-2015
    CASHIER_REFILL_HOPPER_TERMINAL = 555,
    //CASHIER_GAMING_HALL_XX = 556, // Reserved
    TERMINAL_REFILL_HOPPER = 557,
    //CASHIER_GAMING_HALL_XX = 558, // Reserved
    CASHIER_CHANGE_STACKER_TERMINAL = 559,
    CASHIER_COLLECT_DROPBOX_TERMINAL = 560,
    //CASHIER_GAMING_HALL_XX = 561, // Reserved

    // FAV 20-NOV-2015
    TITO_TICKET_PROMOBOX_PRINTED_PROMO_NOT_REDEEMABLE = 562,
    TITO_TICKET_PROMOBOX_PRINTED_PROMO_REDEEMABLE = 563,

    // FAV 21-DEC-2015 Gaming Hall
    CASHIER_COLLECT_DROPBOX_TERMINAL_CAGE = 564,
    CASHIER_REFILL_HOPPER_TERMINAL_CAGE = 565,

    // JML 22-DEC-2015
    PRIZE_RE_IN_KIND1_GROSS = 566, // Taxes paid by the Customer
    PRIZE_RE_IN_KIND2_GROSS = 567, // Taxes paid by the Casino

    PRIZE_RE_IN_KIND1_TAX1 = 568, // Prize RE in kind - Taxes paid by the Customer
    PRIZE_RE_IN_KIND1_TAX2 = 569, // Prize RE in kind - Taxes paid by the Customer

    PRIZE_RE_IN_KIND2_TAX1 = 570, // Prize RE in kind - Taxes paid by the Casino
    PRIZE_RE_IN_KIND2_TAX2 = 571, // Prize RE in kind - Taxes paid by the Casino

    CUSTOMER_ENTRANCE = 572, //Customer Visit


    // DHA 25-JAN-2016
    TITO_TICKETS_CREATE_EXCHANGE_DUAL_CURRENCY = 573,
    TITO_TICKETS_PLAYED_EXCHANGE_DUAL_CURRENCY = 574,
    BILL_IN_EXCHANGE_DUAL_CURRENCY = 575,

    // FAV 09-02-1016
    TITO_TICKET_CASHIER_VOID_CASHABLE = 576,
    TITO_TICKET_CASHIER_VOID_PROMO_REDEEMABLE = 577,
    TITO_TICKET_CASHIER_VOID_PROMO_NOT_REDEEMABLE = 578,
    TITO_TICKET_MACHINE_VOID_CASHABLE = 579,
    TITO_TICKET_MACHINE_VOID_PROMO_NOT_REDEEMABLE = 580,

    // JML & DHA  01-MAR-2016
    TAX_PROVISIONS = 581,

    // RGR 09-03-2016
    TITO_TICKET_CASHIER_SWAP_FOR_CHIPS = 582,

    // RGR 12-04-2016
    TAX_CUSTODY = 583,

    // EOR 20-APR-2016
    TAX_RETURNING_CUSTODY = 584,

    // RAB 24-MAR-2016: Bug 10745
    CARD_ASSOCIATE = 586,

    // FAV 02-MAY-2016 Currency exchange (Multiple denominations)
    CURRENCY_EXCHANGE_TO_NATIONAL_CURRENCY = 587,
    CURRENCY_EXCHANGE_AMOUNT_OUT = 588,
    CURRENCY_EXCHANGE_TO_FOREIGN_CURRENCY = 589,
    CURRENCY_EXCHANGE_AMOUNT_IN = 590,

    // 12-APR-2016 YNM
    TITO_TICKET_COUNTR_PRINTED_CASHABLE = 591,

    // FAV 10-MAY-2016 Currency exchange (Multiple denominations)
    CURRENCY_EXCHANGE_AMOUNT_IN_CHANGE = 592,

    //YNM 21-MAR-2016
    CUSTOMER_ENTRANCE_CARD = 593,

    //YNM 21-MAR-2016
    CUSTOMER_ENTRANCE_DEBIT_CARD = 594,
    CUSTOMER_ENTRANCE_CREDIT_CARD = 595,
    CUSTOMER_ENTRANCE_CHECK = 596,

    // 18-FEB-2015 AVZ --moved & number changed 23-05-2016 SDS
    PROMOTION_REDEEMABLE_FEDERAL_TAX = 597,
    PROMOTION_REDEEMABLE_STATE_TAX = 598,

    // JBP 06-JUN-2016: Add Block / Unblock account movements (C2GO)
    // FJC 01-JUL-2016: Refactor Movements lock (C2GO)
    C2GO_LOCK_ACCOUNT_INTERNAL = 599,           // Lock Account from Wigos 
    C2GO_LOCK_ACCOUNT_EXTERNAL = 600,           // Lock Account from Broxel

    //JRC 29-JUN-2016 Recharge Withdrawal (C2GO)
    C2GO_CASH_IN_INTERNAL = 601,               // Recharge requested from WIGOS    => Money goes from BROXEL to WIGOS      7384
    C2GO_CASH_IN_EXTERNAL = 602,               // Recharge requested from C2GO     => Money goes from BROXEL to WIGOS      7385
    C2GO_CASH_OUT_INTERNAL = 603,              // Withdrawal requested from WIGOS  => Money goes from WIGOS to BROXEL      7386
    C2GO_CASH_OUT_EXTERNAL = 604,              // Withdrawal requested from C2GO   => Money goes from WIGOS to BROXEL      7387

    // FJC 01-JUL-2016: Activate, Deactivate & Renomination (C2GO)
    C2GO_ACTIVATE_ACCOUNT_INTERNAL = 605,      // Activate account from Wigos
    C2GO_DEACTIVATE_ACCOUNT_INTERNAL = 606,    // DeActivate account from Wigos
    C2GO_RENOMINATION_ACCOUNT_INTERNAL = 607,  // Change some personal data from Wigos
    C2GO_RENOMINATION_ACCOUNT_EXTERNAL = 608,  // Change some personal data from Broxel

    // PDM 26-JUL-2016
    ACCOUNT_ADD_BLACKLIST = 609,
    ACCOUNT_REMOVE_BLACKLIST = 610,

    // RAB 01-AUG-2016: BankCard Undo Movement (Televisa)    
    CURRENCY_CARD_EXCHANGE_SPLIT1_UNDO = 611,
    CURRENCY_CARD_EXCHANGE_SPLIT2_UNDO = 612,
    CASH_ADVANCE_CURRENCY_EXCHANGE_UNDO = 613,
    CASH_ADVANCE_GENERIC_BANK_CARD_UNDO = 614,
    CASH_ADVANCE_CREDIT_BANK_CARD_UNDO = 615,
    CASH_ADVANCE_DEBIT_BANK_CARD_UNDO = 616,
    
    CARD_DEPOSIT_IN_CARD_UNDO = 617,
    COMPANY_B_CARD_DEPOSIT_IN_CARD_UNDO = 618,

    // EOR 12-SEP-2016
    SERVICE_CHARGE_ON_COMPANY_A = 619,

    // RGR 04-OCT-2016
    PRIZE_DRAW_GROSS_OTHER = 620,

    REOPEN_CASHIER = 625,

    // FAV 07-MAR-2017 (Exped)
    TAX_ON_PRIZE3 = 626,
    PROMOTION_REDEEMABLE_COUNCIL_TAX = 627,
    PRIZE_IN_KIND1_TAX3 = 628,
    PRIZE_IN_KIND2_TAX3 = 629,
    PRIZE_RE_IN_KIND1_TAX3 = 630,
    PRIZE_RE_IN_KIND2_TAX3 = 631,

    // AMF 16-MAR-2017
    CREDIT_LINE_MARKER = 632,
    CREDIT_LINE_PAYBACK = 633,
    CREDIT_LINE_MARKER_CARD_REPLACEMENT = 634,
    CREDIT_LINE_PAYBACK_CARD_EXCHANGE = 635,
    CREDIT_LINE_PAYBACK_CREDIT_CARD_EXCHANGE = 636,
    CREDIT_LINE_PAYBACK_DEBIT_CARD_EXCHANGE = 637,
    CREDIT_LINE_PAYBACK_CHECK_EXCHANGE = 638,

    // FAV 18-MAY-2017
    GT_TOURNAMENTS_PRIZES = 639,

    GT_TOURNAMENT_ENROLL_CHECK = 640,
    GT_TOURNAMENT_ENROLL_CURRENCY_EXCHANGE = 641,
    GT_TOURNAMENT_ENROLL_CREDIT_BANK_CARD = 642,
    GT_TOURNAMENT_ENROLL_DEBIT_BANK_CARD = 643,
    GT_TOURNAMENT_ENROLL_GENERIC_BANK_CARD = 644,
    GT_TOURNAMENT_ENROLL_ACCOUNT_BALANCE = 645,
    GT_TOURNAMENT_ENROLL_FREE = 646,

    GT_TOURNAMENT_REBUY_CHECK = 647,
    GT_TOURNAMENT_REBUY_CURRENCY_EXCHANGE = 648,
    GT_TOURNAMENT_REBUY_CREDIT_BANK_CARD = 649,
    GT_TOURNAMENT_REBUY_DEBIT_BANK_CARD = 650,
    GT_TOURNAMENT_REBUY_GENERIC_BANK_CARD = 651,
    GT_TOURNAMENT_REBUY_ACCOUNT_BALANCE = 652,
    GT_TOURNAMENT_REBUY_FREE = 653,

    // JBM 26-SEP-2017
    CUSTOMER_NOTICES_MODIFICATION_MASTER = 654,   //A row per Account
    CUSTOMER_NOTICES_MODIFICATION_DETAIL = 655,   //A row per Customer notice

    // DLL Service Charge for gaming tables
    SERVICE_CHARGE_GAMING_TABLE = 656,
    SERVICE_CHARGE_GAMING_TABLE_ON_COMPANY_A = 657,

    /* [1100 - 1599] reserved for cashier buckets */
    // RAB 19-JAN-2016
    MULTIPLE_BUCKETS_END_SESSION = 1100,
    MULTIPLE_BUCKETS_END_SESSION_LAST = 1199,

    MULTIPLE_BUCKETS_EXPIRED = 1200,
    MULTIPLE_BUCKETS_EXPIRED_LAST = 1299,

    MULTIPLE_BUCKETS_MANUAL_ADD = 1300,
    MULTIPLE_BUCKETS_MANUAL_ADD_LAST = 1399,

    MULTIPLE_BUCKETS_MANUAL_SUB = 1400,
    MULTIPLE_BUCKETS_MANUAL_SUB_LAST = 1499,

    MULTIPLE_BUCKETS_MANUAL_SET = 1500,
    MULTIPLE_BUCKETS_MANUAL_SET_LAST = 1599,

    /* [1000000 - 2999999] reserved for cashier concept movements */
    //SMN 09-SEP-2014
    CASHIER_BY_CONCEPT_IN = 1000000,
    CASHIER_BY_CONCEPT_OUT = 2000000,
    CASHIER_BY_CONCEPT_LAST = 2999999,

  }

  public enum MovementType
  {
    Play = 0,
    CashIn = 1,
    CashOut = 2,
    Devolution = 3,
    TaxOnPrize1 = 4,
    StartCardSession = 5,
    EndCardSession = 6,
    Activate = 7,
    Deactivate = 8,
    PromotionNotRedeemable = 9,
    DepositIn = 10,
    DepositOut = 11,
    CancelNotRedeemable = 12,
    TaxOnPrize2 = 13,

    // The following numbers have been used in the past, can't be used again
    // = 14,
    // = 15,
    // = 16,
    // = 17,
    // = 18,
    // = 19,

    // AJQ 19-FEB-2010, Promotions
    PromoCredits = 20,
    PromoToRedeemable = 21,
    PromoExpired = 22,
    PromoCancel = 23,
    PromoStartSession = 24,
    PromoEndSession = 25,

    // Credits Expiration
    CreditsExpired = 26,
    CreditsNotRedeemableExpired = 27,

    // TJG 04-MAR-2010 Replacement Cost for Personal Cards
    CardReplacement = 28,

    // RCI 14-JUN-2010 Draw Ticket Print
    DrawTicketPrint = 29,

    // RCI 05-JUL-2010: Handpays
    Handpay = 30,
    HandpayCancellation = 31,

    // ACC 31-AUG-2010 Manual Close Session
    ManualEndSession = 32,
    PromoManualEndSession = 33,

    // APB 31-AUG-2010 More Hand Pays
    ManualHandpay = 34,
    ManualHandpayCancellation = 35,

    // ACC 03-SEP-2010 PlayerTracking Points

    PointsAwarded = 36,      //Replaced by MULTIPLE_BUCKETS_EndSession + Buckets.BucketId.RedemptionPoints       JRC:MovementType.PointsAwarded1


    PointsToGiftRequest = 37,
    PointsToNotRedeemable = 38,
    PointsGiftDelivery = 39,
    PointsExpired = 40,

    // RCI 29-OCT-2010: Draw Ticket in exchange of points
    PointsToDrawTicketPrint = 41,

    // RCI 05-NOV-2010: Services Gift
    PointsGiftServices = 42,

    // ACC 11-NOV-2010 Change holder levels
    HolderLevelChanged = 43,

    // AJQ 12-NOV-2010, Prize coupon     
    PrizeCoupon = 44,

    // Redemable Spent 
    //  - Used in Promotions     
    DEPRECATED_RedeemableSpentUsedInPromotions = 45,

    PointsToRedeemable = 46,

    PrizeExpired = 47,

    // MBF 11-OCT-2011
    CardCreated = 48,
    AccountPersonalization = 49,

    ManuallyAddedPointsOnlyForRedeem = 50, // GUI Reserved

    // JMM 22-NOV-2011
    AccountPINChange = 51,
    AccountPINRandom = 52,

    // RCI 16-FEB-2012: For the new Not-Redeemable credit (used now for Cover Coupon).
    CreditsNotRedeemable2Expired = 53,
    CashInCoverCoupon = 54,
    CashInNotRedeemable2 = 55, // For future promotions

    // ACC 21-MAR-2012
    CancelStartSession = 56,

    // JCM 24-APR-2012
    CardRecycled = 57,

    PromotionRedeemable = 58,
    CancelPromotionRedeemable = 59,

    PromotionPoint = 60,
    CancelPromotionPoint = 61,

    // RCI 28-AUG-2012: For manual level change from the GUI.
    ManualHolderLevelChanged = 62,

    // AJQ 30-AUG-2012: Tax returning (redondeo por retenci�n)
    TaxReturningOnPrizeCoupon = 63,
    // AJQ 10-SEP-2012: Decimal Rounding (redondeo por decimales)
    DecimalRounding = 64,
    // RRB 12-SEP-2012: Service Charge (carga por servicio)
    ServiceCharge = 65,
    // MPO 20-SEP-2012: Cancel gift instance (cancelaci�n del regalo, ya sea solicitado o entregado (para NR o RE))
    CancelGiftInstance = 66,

    // QMP 06-FEB-2013:  Change Account Points Status (Point Redemption Allowed / Not Allowed)
    PointsStatusChanged = 67,

    ManuallyAddedPointsForLevel = 68,  // Add points from GUI

    // IMPORT
    ImportedAccount = 70,               // Imported(account) 
    ImportedPointsOnlyForRedeem = 71,   // Imported points (not accounted for the level)    similar ManuallyAddedPointsOnlyForRedeem
    ImportedPointsForLevel = 72,        // Imported (points)    similar ManuallyAddedPointsForLevel
    ImportedPointsHistory = 73,         // History points imported         

    // JBP 07-MAY-2013: Add Block /Unblock account movements
    AccountBlocked = 75,
    AccountUnblocked = 76,

    Cancellation = 77,

    // RMS 13-JAN-2014: Card credit transfer
    TransferCreditOut = 78,
    TransferCreditIn = 79,

    // RCI 29-JAN-2014
    CashInTax = 82,

    // AMF 30-JUN-2014: Card Replacement
    CardReplacementForFree = 83,
    CardReplacementInPoints = 84,

    //DLL 01-SEP-2014
    CashAdvance = 85,

    // OPC 04-NOV-2014
    ExternalSystemPointsSubstract = 86,

    // MULTISITE
    MultiSiteCurrentLocalPoints = 101,  // Multisite - Current points in site, similar ManuallyAddedPointsOnlyForRedeem

    //LEM 30-OCT-2013: TITO
    TITO_TicketCashierPrintedCashable = 105,
    TITO_TicketCashierPaidCashable = 106,
    TITO_TicketCashierPrintedPromoRedeemable = 107,
    TITO_TicketMachinePlayedCashable = 108,
    TITO_TicketReissue = 109,
    TITO_AccountToTerminalCredit = 110,
    TITO_TerminalToAccountCredit = 111,
    TITO_TicketOffline = 112,
    TITO_TicketCashierPrintedPromoNotRedeemable = 113,
    TITO_TicketMachinePrintedHandpay = 114,
    TITO_TicketMachinePrintedJackpot = 115,
    TITO_TicketCashierPaidHandpay = 116,
    TITO_TicketCashierPaidJackpot = 117,
    // DHA & JML 11-APR-2014
    TITO_TicketMachinePrintedCashable = 118,
    TITO_TicketMachinePrintedPromoNotRedeemable = 119,
    TITO_TicketCashierPaidPromoRedeemable = 120,
    TITO_TicketMachinePlayedPromoRedeemable = 121,
    TITO_ticketMachinePlayedPromoNotRedeemable = 122,
    TITO_TicketCashierExpiredPaidCashable = 123,
    TITO_TicketCashierExpiredPaidPromoRedeemable = 124,
    TITO_TicketCashierExpiredPaidJackpot = 125,
    TITO_TicketCashierExpiredPaidHandpay = 126,
    // EndTito

    // SMN 05-SEP-2013: Cashier movement for Participation in draw
    CashdeskDrawParticipation = 200,
    CashdeskDrawPlaySession = 201,

    // SMN & JVV 12-DEC-2013: Movements for Chips
    ChipsSaleDevolutionForTito = 202,
    ChipsSaleTotal = 203,
    ChipsPurchaseTotal = 204,
    // RCI & DRV 08-JUL-2014
    //ChipsSaleWithCashIn = 205,
    //ChipsPurchaseWithCashOut = 206,

    // FJC 28-APR-2015
    WinLossStatementRequest = 205,
    //Hidden movements

    //FOS 23-JUN-2015
    CardDepositInCheck = 206,
    CardDepositInCurrencyExchange = 207,
    CardDepositInCardCredit = 208,
    CardDepositInCardDebit = 209,
    CardDepositInCardGeneric = 210,
    CardReplacementCheck = 211,
    CardReplacementCardCredit = 212,
    CardReplacementCardDebit = 213,
    CardReplacementCardGeneric = 214,

    //JCA 22-OCT-2015: Movements for GameGateway
    GameGatewayBet = 215,
    GameGatewayPrize = 216,

    // FAV 20-NOV-2015
    TITO_TicketPromoBoxPrintedPromoNotRedeemable = 217,
    TITO_TicketPromoBoxPrintedPromoRedeemable = 218,

    //FJC 07-DIC-2015: Movements for GameGateway
    GameGatewayReserveCredit = 219,

    //AMF 19-JAN-2016: GameGateway Rollback
    GameGatewayBetRollback = 220,

    //JML 01-Mar-2016
    TaxProvisions = 221,

    TaxCustody = 222,
    // EOR 20-APR-2016
    TaxReturnCustody = 223,

    // FAV 02-MAY-2016 Currency exchange (Multiple denominations)
    CurrencyExchangeCashierOut = 224,

    //YNM 13-APR-2016
    TITO_TicketCountRPrintedCashable = 225,

    // RAB 24-MAR-2016: Bug 10745
    CardAssociate = 226,

    // FAV 02-MAY-2016 Currency exchange (Multiple denominations)
    CurrencyExchangeCashierIn = 227,

    // 18-FEB-2015 AVZ --moved & number changed 23-05-2016 SDS
    PromotionRedeemableFederalTax = 228,
    PromotionRedeemableStateTax = 229,


    // 30-JUN-2016 JRC C2GO
    C2GOCashInInternal = 230,         // Recharge requested from WIGOS    => Money goes from BROXEL to WIGOS      7384
    C2GOCashInExternal = 231,         // Recharge requested from C2GO     => Money goes from BROXEL to WIGOS      7385
    C2GOCashOutInternal = 232,        // Withdrawal requested from WIGOS  => Money goes from WIGOS to BROXEL      7386
    C2GOCashOutExternal = 233,        // Withdrawal requested from C2GO   => Money goes from WIGOS to BROXEL      7387


    // FJC 01-JUL-2016 C2GO
    C2GOActivateAccountInternal = 234,      // Activate account from Wigos (Activation in broxel)
    C2GODeactivateAccountInternal = 235,    // DeActivate account from Wigos (DeActivation only in our Wigos System)
    C2GOLockAccountInternal = 236,          // Lock Account from Wigos 
    C2GOLockAccountExternal = 237,          // Lock Account from Broxel
    C2GORenominationAccountInternal = 238,  // Change some personal data from Wigos
    C2GORenominationAccountExternal = 239,  // Change some personal data from Broxel

    //PDM  27-JUL-2016
    AccountInBlacklist = 240,
    AccountNotInBlacklist = 241,
    
    //JBP 05-SEP-2016: Movements for PariPlay
    PariPlayBet = 242,
    PariPlayPrize = 243,
    PariPlayBetRollback = 244,

    // XGJ 07-AGU-2016
    Admission = 245,

    // JBP 03-NOV-2016
    FBSubAmount = 246,
    FBAddAmount = 247,
    FBRollback = 248,

    C2GODisassociateAccountInternal = 249,

    // FAV 07-MAR-2017
    TaxOnPrize3 = 250,
    PromotionRedeemableCouncilTax = 251,

    // AMF 16-MAR-2017
    CreditLineCreate = 252,
    CreditLineUpdate = 253,
    CreditLineApproved = 254,
    CreditLineNotApproved = 255,
    CreditLineSuspended = 256,
    CreditLineExpired = 257,
    CreditLineGetMarker = 258,
    CreditLinePayback = 259,
    CreditLineGetMarkerCardReplacement = 260,

    // FAV 20-MAY-2017
    GamingTablesTournamentPrizePayment = 261,
    GamingTablesTournamentEnroll = 262,
    GamingTablesTournamentRebuy = 263,

    // JML: Informative momevement of the remaining amount after chips sales
    ChipsSaleRemainingAmount = 264,

    // RAB: Informative momevement of the consumed remaining amount after chips sales
    ChipsSaleConsumedRemainingAmount = 265,

    // ETP: FBM CREDIT EXPIRATION
    ReservedExpired = 266,
    
    // LJM 28-JAN-2014: Credit card recharge
    HIDDEN_RechargeNotDoneWithCash = 1000,

    // ETP 20-JAN-2016 Multiple Buckets Code Operation [1100-1599]
    MULTIPLE_BUCKETS_EndSession = 1100,
    MULTIPLE_BUCKETS_EndSessionLast = 1199,

    MULTIPLE_BUCKETS_Expired = 1200,
    MULTIPLE_BUCKETS_ExpiredLast = 1299,

    MULTIPLE_BUCKETS_Manual_Add = 1300,
    MULTIPLE_BUCKETS_Manual_Add_Last = 1399,

    MULTIPLE_BUCKETS_Manual_Sub = 1400,
    MULTIPLE_BUCKETS_Manual_Sub_Last = 1499,

    MULTIPLE_BUCKETS_Manual_Set = 1500,
    MULTIPLE_BUCKETS_Manual_Set_Last = 1599,

    // RGR 25-MAY-2017 Fake Provision for Winpot only
    WinpotFakeProvision = 1600

  } // MovementType

  public enum PlaySessionType
  {
    NONE = 0,
    CADILLAC_JACK = 1,
    WIN = 2,
    ALESIS = 3,

    HANDPAY_CANCELLED_CREDITS = 100,
    HANDPAY_JACKPOT = 101,
    HANDPAY_ORPHAN_CREDITS = 102,
    HANDPAY_MANUAL = 110,
    HANDPAY_SITE_JACKPOT = 120,

    DRAW = 200,
    //XGJ 30-GEN-2017
    TERMINAL_DRAW = 201, 

    PROGRESSIVE_PROVISION = 300,
    PROGRESSIVE_RESERVE = 301,

    GAMEGATEWAY = 400,

    PARIPLAY = 500

  }

  public enum AccountType
  {
    ACCOUNT_UNKNOWN = 0,
    ACCOUNT_CADILLAC_JACK = 1,
    ACCOUNT_WIN = 2,
    ACCOUNT_ALESIS = 3,
    ACCOUNT_VIRTUAL_CASHIER = 4,
    ACCOUNT_VIRTUAL_TERMINAL = 5
  }

  public enum CashlessMode
  {
    NONE = 0,
    WIN = 1,
    ALESIS = 2, // 3GS
    // RMS 04-APR-2014: WIN+ALESIS Deprecated
    // WIN_AND_ALESIS = 3, // WIN & ALESIS
  }

  public enum PlaySessionStatus
  {
    Opened = 0,
    Closed = 1,
    Abandoned = 2,
    ManualClosed = 3,

    Cancelled = 4,

    HandpayPayment = 5,
    HandpayCancelPayment = 6,

    DrawWinner = 10,
    DrawLoser = 11,

    ProgressiveReserve = 20,
    ProgressiveProvision = 21,

    GameGatewayBet = 30,
    GameGatewayPrize = 31,
    GameGatewayBetRollback = 32,

    PariPlayBet = 33,
    PariPlayPrize = 34,
    PariPlayBetRollback = 35

  }

  public enum GTPlaySessionStatus // Game table play session game status
  {
    Opened = 0,
    Closed = 1,
    Away = 2,
  }

  public enum GTPlayerTrackingSpeed
  {
    Unknown = 0,
    Slow = 1,
    Medium = 2,
    Fast = 3,
  }

  public enum GTPlaySessionPlayerSkill
  {
    Unknown = 0,
    Soft = 1,
    Average = 2,
    Hard = 3,
  }

  public enum PendingGamePlaySessionStatus
  {
    Initial = 0,
    ELP01_Split_PS_Reddem_NoRedeem = 1,
    //GamePlaySession = 2
  }

  public enum GamingTableDropBoxCollectionCount
  {
    Cage = 0,
    Cashier = 1,
  }

  [Flags]
  public enum AccountBlockReason
  {                                                        // Old values:
    NONE = 0x0000,                                         // 0
    FROM_CASHIER = 0x0100,                                 // 1
    MAX_BALANCE = 0x0200,                                  // 2
    ABANDONED_CARD = 0x0400,                               // 3
    FROM_GUI = 0x0800,                                     // 4
    MAX_PIN_FAILURES = 0x1000,                             // 5
    IMPORT = 0x2000,                                       // 6
    EXTERNAL_SYSTEM = 0x4000, //SPACE (STATUS=0)           // 7
    HOLDER_LEVEL_DOWNGRADE = 0x8000,                       // 8
    HOLDER_LEVEL_UPGRADE = 0x10000,                        // 9
    EXTERNAL_SYSTEM_CANCELED = 0x20000,//SPACE (STATUS=2)  // 10
    EXTERNAL_AML = 0x40000,
    INTRUSION = 0x80000,
    INACTIVE_PLAY_SESSION = 0x100000,
    C2GO_INTERNAL = 0x200000,                             // C2GO INTERNAL (from WIGOS)
    C2GO_EXTERNAL = 0x400000,                             // C2GO EXTERNAL (from BROXEL)         
    BLACKLIST = 0x800000                              // C2GO EXTERNAL (from BROXEL)         
    // Max enum = 0x80000000 (2^31)
    // Because it becomes a bitmask (Int32).
  }

  public enum AccountBlockUnblockSource
  {
    GUI = 0,
    CASHIER = 1,
    C2GO = 2,
    RECEPTION_BLACKLIST = 3
  }

  public enum TerminalTypes
  {
    // Unknown
    UNKNOWN = -1,
    // Machines
    WIN = 1,                // LKT 
    T3GS = 3,               // 3GS
    SAS_HOST = 5,           // SasHost
    // Site 
    SITE = 100,             // Site 
    SITE_JACKPOT = 101,     // Site Jackpot
    MOBILE_BANK = 102,      // MobileBank-SasHost
    MOBILE_BANK_IMB = 103,  // MobileBank-IPod
    ISTATS = 104,           // IStats-IPad
    PROMOBOX = 105,         // Promobox  

    CASHDESK_DRAW = 106,    // CashDesk Draw
    GAMING_TABLE_SEAT = 107,    // Gaming Table Seat

    OFFLINE = 108,          //OFFLINE

    GAMEGATEWAY = 109,         // GAMEGATEWAY
    PARIPLAY = 110,         // PARIPLAY

    CASHDESK_DRAW_TABLE = 111, // CashDesk Draw Tables

    WIN_UP = 112, // Win Up

    TERMINAL_DRAW = 113,    // Terminal Draw

  }

  public enum HANDPAY_TYPE      // Why the Handpay was generated 
  {
    CANCELLED_CREDITS = 0,
    JACKPOT = 1,
    ORPHAN_CREDITS = 2,
    MANUAL = 10,
    SITE_JACKPOT = 20,
    SPECIAL_PROGRESSIVE = 30,
    UNKNOWN = 999,
    ///// Manual subtype
    //BEGIN MANUAL_SUBTYPE => 1000 + TYPE
    MANUAL_CANCELLED_CREDITS = 1000 + CANCELLED_CREDITS,
    MANUAL_JACKPOT = 1000 + JACKPOT,
    MANUAL_OTHERS = 1000 + MANUAL,
    //END MANUAL_SUBTYPE => 1000 + TYPE
  }

  public enum TerminalStatus
  {
    ACTIVE = 0,
    OUT_OF_SERVICE = 1,
    RETIRED = 2
  }

  public enum ACCOUNTS_PROMO_COST
  {
    RESET_ON_REDEEM = 0,
    SET_ON_FIRST_NR_PROMOTION = 1,
    UPDATE_ON_RECHARGE = 2,
    UPDATE_ON_REDEEM = 3,
    RESET_ON_RECHARGE = 4
  }

  public enum WWP_CONNECTION_TYPE
  {
    UNKNOWN = 0,
    MULTISITE = 1,
    SUPERCENTER = 2,
  }

  public enum WWP_CONNECTION_STATUS
  {
    UNKNOWN = 0,
    CONNECTED = 1,
    DISCONNECTED = 2,
  }

  public enum TYPE_MULTISITE_SITE_TASKS
  {
    Unknown = 0,
    DownloadConfiguration = 1,


    UploadPointsMovements = 11,     // ms_site_pending_account_movements
    UploadPersonalInfo = 12,        // ms_site_pending_accounts
    UploadAccountDocuments = 13,    // ms_site_pending_accounts_documents


    DownloadPoints_Site = 21,   // by last known sequence
    DownloadPoints_All = 22,    // by last known sequence

    DownloadPersonalInfo_Card = 31, // ms_site_synchronize_accounts
    DownloadPersonalInfo_Site = 32, // by last known sequence
    DownloadPersonalInfo_All = 33,  // by last known sequence    

    DownloadProvidersGames = 40,  // by last known sequence
    UploadProvidersGames = 41,  // by last known sequence 
    DownloadProviders = 42,  // by last known sequence 
    DownloadAccountDocuments = 43,    // by last known sequence

    // Multisite statistics
    UploadSalesPerHour = 50,
    UploadPlaySessions = 51,
    UploadProviders = 52,
    UploadBanks = 53,
    UploadAreas = 54,
    UploadTerminals = 55,
    UploadTerminalsConnected = 58,
    UploadGamePlaySessions = 59,

    DownloadMasterProfiles = 60,
    DownloadCorporateUsers = 61,

    UploadLastActivity = 62,
    UploadCashierMovementsGrupedByHour = 63,
    UploadAlarms = 64,
    DownloadAlarmPatterns = 65,
    DownloadLcdMessages = 66,
    UploadHandpays = 67,
    UploadSellsAndBuys = 68,    // buying and selling chips

    ReceptionEntry = 69,
    DownloadBucketsConfig = 70,
    UploadLinkedExternalAccounts = 71,
    DownloadExternalAccounts_Site = 72,
    DownloadLinkedExternalAccounts = 73,
    DownloadLinkedExternalAccounts_Site = 74,
    DownloadLinkedExternalAccounts_All = 75,

    UploadCreditLines = 76,
    UploadCreditLineMovements = 77
  }

  public enum SequenceId
  {
    NotSet = 0,
    VouchersSplitA = 1,
    VouchersSplitB = 2,
    VouchersSplitBCancel = 3,

    // Local Site
    AccountId = 4,

    // Multisite  Reserved range: 10..40
    TrackAccountChanges = 10,
    TrackPointChanges = 11,
    AccountMovements = 12,
    ProvidersGames = 13,
    Providers = 14,
    AccountDocuments = 15,

    //CashDeskDraw
    CashDeskDraw = 41,

    // LCDMessage
    LCDMessage = 42,

    //
    VoucherEntrances = 43,

    // JML 01-MAR-2016
    VoucherTaxProvisions = 44,

    // LTC 14-ABR-2016
    VoucherTaxCustody = 45,

    // FJC 03-MAY-2016
    OpenCloseLoopSiteRequests = 46,
    LinkedExternalAccounts = 47,

    // EOR 28-SEP-2016
    VoucherParticipationDraw = 48,
    VoucherSummaryCredits = 49,

    //TITO
    TITO_ValidationNumber = 100,

    // FGB 14-JUN-2016
    TerminalMetersHistoryEvents = 101,
    PinPadTransaction = 103,

    // LTC 19-JUL-2016
    AuditId = 102,

    // PariPlay
    ParyPlay = 104,

    // RGR 17-OCT-2017
    DC = 108,
    PP = 109,
    SC = 110,
    AU = 111,
    DD = 112,
    PR = 113,
    BUF = 114,
    //RGR 27-03-2018
    PS = 115,

    AutoPrintVoucherCashOut = 116,

    // RAR - S2S GSA
    S2SGsaConfigurationId = 1000,
    S2SGsaOutBoundCommandId = 1001,   // TODO: Change. Support CommandId persistent but it will be the same for all hosts.
    S2SGsaOutBoundMessageId = 1002,   // TODO: Change. Support MessageId logic for Amtote (similar to CommandId). S2S not say nothing about this behavior.

    CageConcepts = 1000000,
    CageConcepts_Last = 2000000,
  }

  // JML - GENERAL_PARAMS: GP_MS_DOWNLOAD_TYPE
  public enum ParamatersTypeForSite
  {
    NotDownload = 0,                        // Don't download (the parameter value is only to the Multisite)
    Download_Update = 1,                    // Update Site with value from Multisite
    Download_UpdatePriorityMultisite = 2,   // Update Site if MS value is not null or empty
    Download_UpdatePrioritySite = 3,        // Update Site if Site value is null or empty 
  }

  // QMP & ICS 29-APR-2013
  public enum CASHIER_SESSION_STATUS
  {
    OPEN = 0,
    CLOSED = 1,
    OPEN_PENDING = 2, // Session is marked as Pending Closing, but it's still used by some terminals
    PENDING_CLOSING = 3, // Session is marked as Pending Closing and it's not used by any terminal
  }

  // RMS 08-AUG-2014
  public enum CurrencyExchangeSubType
  {
    NONE = 0,
    CHECK = 1,
    BANK_CARD = 2,
    CREDIT_CARD = 3,
    DEBIT_CARD = 4,
    CARD2GO = 5,
    BEARER_CHECK = 6,
    NOMINAL_CHECK = 7,

  }

  [Serializable]
  public enum CurrencyExchangeType
  {
    CURRENCY = 0,
    CARD = 1,
    CHECK = 2,
    CASINOCHIP = 3,

    // See too: CageCurrencyType & ChipType
    CASINO_CHIP_RE = 1001,
    CASINO_CHIP_NRE = 1002,
    CASINO_CHIP_COLOR = 1003,

    FREE = 4,
    TICKET = 5,
    CARD2GO = 6,  //C2GO
    CREDITLINE = 7  //CREDITLINE
  }

  public enum TransactionType
  {
    CASH_ADVANCE = 0,
    RECHARGE = 1,
    RECHARGE_CARD_CREATION = 2,
    CARD_REPLACEMENT = 3,
  }

  // RCI 21-SEP-2010: When an OperationCode is added, you must define it in Operations.cs:DB_InsertOperation().
  public enum OperationCode
  {
    NOT_SET = 0,
    CASH_IN = 1,
    CASH_OUT = 2,
    PROMOTION = 3,
    MB_CASH_IN = 4,
    MB_PROMOTION = 5,
    GIFT_REQUEST = 6,
    GIFT_DELIVERY = 7,
    DRAW_TICKET = 8,
    GIFT_DRAW_TICKET = 9,
    GIFT_REDEEMABLE_AS_CASHIN = 10,

    TRANSFER_CREDIT_IN = 11,
    TRANSFER_CREDIT_OUT = 12,

    CHIPS_SALE = 13,
    CHIPS_SALE_WITH_RECHARGE = 14,
    CHIPS_PURCHASE = 15,
    CHIPS_PURCHASE_WITH_CASH_OUT = 16,

    // DLL 01-SEP-2014
    CASH_ADVANCE = 17,

    // JPJ 12-JAN-2015
    MB_DEPOSIT = 18,

    // HBB 19-JAN-2015
    CASH_DEPOSIT = 19,
    CASH_WITHDRAWAL = 20,

    REDEEMABLE_CREDITS_EXPIRED = 100,
    NOT_REDEEMABLE_CREDITS_EXPIRED = 101,
    POINTS_EXPIRED = 102,
    HOLDER_LEVEL_CHANGED = 103, // OperationData [-3 .. 3]
    ACCOUNT_CREATION = 104,
    ACCOUNT_PERSONALIZATION = 105,

    // JMM 22-NOV-2011
    ACCOUNT_PIN_CHANGED = 106,
    ACCOUNT_PIN_RANDOM = 107,
    NOT_REDEEMABLE2_CREDITS_EXPIRED = 108,  // RCI 16-FEB-2012: For the new Not-Redeemable credit (used now for Cover Coupon).

    //SSC 09-MAR-2012: Added new Operation Codes for Note Acceptor
    NA_CASH_IN = 109,
    NA_PROMOTION = 110,

    CANCEL_GIFT_INSTANCE = 111,

    //JMM 07-NOV-2012: PromoBOX Recharges summary report
    PROMOBOX_TOTAL_RECHARGES_PRINT = 112,

    //RBG 27-MAR-2013 
    HANDPAY = 113,
    HANDPAY_CANCELLATION = 114,
    HANDPAY_VALIDATION = 115,

    IMPORT_POINTS = 117,

    TITO_OFFLINE = 118,
    TITO_REISSUE = 119,

    // JML 07-FEB-2013 Future use as ticket in, now is use for validation 
    TITO_TICKET_VALIDATION = 120,

    // DHA 05-FEB-2015
    IMPORT_GAMING_TABLE_PLAY_SESSIONS = 121,

    // DRV 13-APR-2015
    SAFE_KEEPING_DEPOSIT = 122,
    SAFE_KEEPING_WITHDRAW = 123,

    // FJC 28APR-2015
    WIN_LOSS_STATEMENT_REQUEST = 124,

    // FOS 17-JUL-2015
    CARD_REPLACEMENT = 125,

    // FAV 13-08-2015
    TRANSFER_CASHIER_SESSIONS_WITHDRAWAL = 126,
    TRANSFER_CASHIER_SESSIONS_DEPOSIT = 127,

    // FAV 29-OCT-2015 GamingHall
    TERMINAL_REFILL_HOPPER = 128,
    TERMINAL_COLLECT_DROPBOX = 129,

    //FJC 07-DIC-2015: Movements for GameGateway
    GAMEGATEWAY_RESERVE_CREDIT = 130,

    // FAV 29-OCT-2015
    TERMINAL_CHANGE_STACKER_REQUEST = 131,

    // ETP 20-JAN-2016
    MULTIPLE_BUCKETS_EXPIRED = 132,
    MULTIPLE_BUCKETS_ENDSESSION = 133,
    MULTIPLE_BUCKETS_MANUAL = 134,

    //ADI & AVZ 14-JAN-2015
    CUSTOMER_ENTRANCE = 135,

    // FAV 16-JAN-2016 GamingHall
    CASHIER_REFILL_HOPPER = 136,
    CASHIER_REFILL_HOPPER_CAGE = 137,
    CASHIER_COLLECT_REFILL = 138,
    CASHIER_COLLECT_REFILL_CAGE = 139,

    // FAV 09-FEB-2016
    TITO_VOID_MACHINE_TICKET = 140,

    // RGR 09-03-2016
    CHIPS_SWAP = 141,

    // RAB 24-MAR-2016: Bug 10745
    CARD_ASSOCIATE = 142,

    //AVZ & GA 11-APR-2016   
    PRIZE_PAYOUT = 143,
    PRIZE_PAYOUT_AND_RECHARGE = 144,

    // FAV 02-MAY-2016 Currency exchange (Multiple denominations)
    CURRENCY_EXCHANGE_CHANGE = 145,
    CURRENCY_EXCHANGE_DEVOLUTION = 146,

    // AVZ 23-FEB-2016
    PROMOTION_WITH_TAXES = 147,


    //JRC 29-JUN-2016 C2GO
    C2GO_CASH_IN_INTERNAL = 148,               // Recharge requested from WIGOS    => Money goes from BROXEL to WIGOS
    C2GO_CASH_IN_EXTERNAL = 149,               // Recharge requested from C2GO     => Money goes from BROXEL to WIGOS
    C2GO_CASH_OUT_INTERNAL = 150,              // Withdrawal requested from WIGOS  => Money goes from WIGOS to BROXEL
    C2GO_CASH_OUT_EXTERNAL = 151,              // Withdrawal requested from C2GO   => Money goes from WIGOS to BROXEL

    // FJC 01-JUL-2016 C2GO
    C2GO_ACTIVATE_ACCOUNT_INTERNAL = 152,      // Activate account from Wigos
    C2GO_DEACTIVATE_ACCOUNT_INTERNAL = 153,    // DeActivate account from Wigos
    C2GO_LOCK_ACCOUNT_INTERNAL = 154,          // Lock Account from Wigos 
    C2GO_LOCK_ACCOUNT_EXTERNAL = 155,          // Lock Account from Broxel
    C2GO_RENOMINATION_ACCOUNT_INTERNAL = 156,  // Change some personal data from Wigos
    C2GO_RENOMINATION_ACCOUNT_EXTERNAL = 157,  // Change some personal data from Broxel



    CLOSE_OPERATION = 160,                     // Close operation
    REOPEN_OPERATION = 161,                    // Reopen operation

    HANDPAY_AUTOMATICALLY_PAYMENT = 162,       // Handpay automatically payment

    // AMF 15-MAR-2017
    CREDIT_LINE = 163,

    // MS 03-MAY-2017
    JUNKET = 164,

    // FAV 18-MAY-2017
    GT_TOURNAMENTS_PRIZE_PAYMENT = 165,
    GT_TOURNAMENTS_ENROLL = 166,
    GT_TOURNAMENTS_REBUY = 167,
    GT_TOURNAMENTS_UNDO_PRIZE_PAYMENT = 168,
    GT_TOURNAMENTS_UNDO_REBUY = 169,

    // FOS 03-08-2017
    PLAYCASH_DRAW = 170,
    // CCG 26-09-2017
    PLAYREWARD_DRAW = 171,
    PLAYPOINT_DRAW = 172,

    // JBM 27-SEP-2017
    CUSTOMER_NOTICE_OPERATION = 173,

    // JGC 08-05-2018
    AUTO_PRINT_VOUCHER_CASH_OUT = 177,
    // AMF & RCI 12-FEB-2015
    CAGE_CONCEPTS = 1000000,
    CAGE_CONCEPTS_LAST = 2000000,
  }

  public enum CashierVoucherType
  {
    NotSet = 0,
    CashIn_A = 1,
    CashIn_B = 2,
    CashOut_A = 3,
    Cancel_B = 4,
    ServiceCharge_B = 5,  // AJQ 12-SEP-2012, Service Charge: it should work like CashIn_B
    CashDeskDraw_Winner = 6, // Cash Desk Draw Winner Voucher
    TicketOut = 7, // JRM 28-AUG-2013. A cashier voucher for TITO tickets
    ChipsSale = 8, // DLL 04-FEB-2014. Voucher type for chips operations
    ChipsBuy = 9,
    ChipsChange = 10,
    ChipsSaleDealerCopy = 11,
    CashOpening = 12, // SGB 03-OCT-2014 Footer voucher type
    CashClosing = 13,
    MBSalesLimitChange = 14,
    MBDeposit = 15,
    MBClosing = 16,
    Commission_B = 17, // JPJ 12-DEC-2014 detail Company B commission voucher 
    CheckCommissionB = 18, // JBC 23-DEC-2014 detail Company B commission voucher 
    CashDeskDraw_Loser = 19, // DHA 19-DEC-2014 Cash Desk Draw Complementary Voucher
    ExchangeCommissionB = 20,  // JBC 23-DEC-2014 detail Company B commission voucher 
    CashDeposit = 21,
    CashWithdrawal = 22,
    CardDepositIn_B = 23,
    CardReplacement_B = 24,
    CardDepositOut_B = 25,
    CardReplacement = 26,
    CashDeskCloseUnbalanced = 27,
    CashierTransferCashDeposit = 28,
    CashierTransferCashWithdrawal = 29,
    TerminalsToCollect = 30, // FAV 11-NOV-2015 Gaming Hall (Collect Dropbox and refill Hoppers)
    TerminalsToRefill = 31, // FAV 11-NOV-2015 Gaming Hall (Refill Hoppers)
    CustomerEntrance = 32, //YNM 17-DIC-2015 Customer Visits 
    TaxProvisions = 33,  // JML 01-MAR-2016 Tax Provisions    

    //ESE 17-Mar-2016
    ChipsSwap = 34,

    //LTC 14-ABR-2016
    TaxCustody = 35,

    //LTC 05-JUL-2016
    CashWithdrawalCard = 36,

    //ESE 28-JUL-2016
    TotalizingStrip = 37,

    //EOR 28-SEP-2016
    ParticipationDraw = 38,
    SummaryCredits = 39,

    //MS  20-MAR-2017
    GetCreditLineMarker = 40,

    //RGR 19-OCT-2017
    CashOut_A_Prize = 44,

    //RGR 13-NOV-2017
    Promotion_RE_Prize = 45,

    //JCA 03-JAN-2018
    Buffet = 46,

    //RGR 27-03-2018
    PromotionSystem = 47,

    AutoPrintVoucherCashOut = 48,

    // AMF & RCI 12-FEB-2015
    CageConcepts = 1000000,
    CageConcepts_Last = 2000000,
  }

  public enum UndoStatus
  {
    None = 0,
    Undone = 1,
    UndoOperation = 2
  }

  public enum FULL_NAME_TYPE
  {
    ACCOUNT = 0,
    WITHHOLDING = 1
  }

  public enum WCP_TerminalType
  {
    IntermediateServer = 0
  , GamingTerminal = 1
  }
  //------------------------------------------------------------------------------
  // CLASS : CurrencyExchangeResult
  // 
  // NOTES :

  public class CurrencyExchangeResult
  {
    #region Members

#if !SQL_BUSINESS_LOGIC && !SQL_WIGOS_BL02
    private Currency m_in_amount;
    private Currency m_gross_amount;
    private Currency m_comission;
    private Currency m_net_amount_split1;
    private Currency m_net_amount_split2;
    private Currency m_NR2_amount;
    private Currency m_card_price;
    private Currency m_card_comissions;
#else
    private Decimal m_in_amount;
    private String m_in_iso_code;
    private String m_out_iso_code;
    private Decimal m_gross_amount;
    private Decimal m_comission;
    private Decimal m_net_amount_split1;
    private Decimal m_net_amount_split2;
    private Decimal m_NR2_amount;
    private Decimal m_card_price;
    private Decimal m_card_comissions;
#endif
    private Decimal m_change_rate;
    private Int32 m_num_decimals;
    private Boolean m_was_foreing_currency;
    private Boolean m_are_promotions_enabled = false;
    private CurrencyExchangeType m_in_type;
    private Int64 m_account_promotion_id;
    private BankTransactionData m_bank_transaction_data = new BankTransactionData();
    private Decimal m_net_amount_devolution;

    // RMS 08-AUG-2014
    private CurrencyExchangeSubType m_subtype = CurrencyExchangeSubType.NONE;

    #endregion

    #region Properties

    public Decimal InAmount
    {
      get { return m_in_amount; }
      set
      {
#if !SQL_BUSINESS_LOGIC && !SQL_WIGOS_BL02
        String _cur_iso_type = m_in_amount.CurrencyIsoCode;
        m_in_amount = value;
        m_in_amount.CurrencyIsoCode = _cur_iso_type;
#else
        m_in_amount = value;
#endif
      }
    }

    public Int64 AccountPromotionId
    {
      get { return m_account_promotion_id; }
      set { m_account_promotion_id = value; }
    }

    public String InCurrencyCode
    {
#if !SQL_BUSINESS_LOGIC && !SQL_WIGOS_BL02
      get { return m_in_amount.CurrencyIsoCode; }
      set { m_in_amount.CurrencyIsoCode = value; }
#else
      get { return m_in_iso_code; }
      set { m_in_iso_code = value; }
#endif
    }

    public String OutCurrencyCode
    {
#if !SQL_BUSINESS_LOGIC && !SQL_WIGOS_BL02
      get { return m_gross_amount.CurrencyIsoCode; }
      set { m_gross_amount.CurrencyIsoCode = value; }
#else
      get { return m_out_iso_code; }
      set { m_out_iso_code = value; }
#endif
    }

    public Decimal ChangeRate
    {
      get { return m_change_rate; }
      set { m_change_rate = value; }
    }

    public Int32 Decimals
    {
      get { return m_num_decimals; }
      set { m_num_decimals = value; }
    }

    public Boolean WasForeingCurrency
    {
      get { return m_was_foreing_currency; }
      set { m_was_foreing_currency = value; }
    }

    public Decimal GrossAmount
    {
      get { return m_gross_amount; }
      set { m_gross_amount = value; }
    }

    public Decimal Comission
    {
      get { return m_comission; }
      set { m_comission = value; }
    }

    public Decimal NetAmount
    {
      get { return m_net_amount_split1 + m_net_amount_split2; }
    }

    public Decimal NetAmountSplit1
    {
      get { return m_net_amount_split1; }
      set { m_net_amount_split1 = value; }
    }

    public Decimal NetAmountSplit2
    {
      get { return m_net_amount_split2; }
      set { m_net_amount_split2 = value; }
    }

    public Decimal NR2Amount
    {
      get { return m_NR2_amount; }
      set { m_NR2_amount = value; }
    }

    public Decimal CardPrice
    {
      get { return m_card_price; }
      set { m_card_price = value; }
    }

    public Decimal CardComissions
    {
      get { return m_card_comissions; }
      set { m_card_comissions = value; }
    }

    public Boolean ArePromotionsEnabled
    {
      get { return m_are_promotions_enabled; }
      set { m_are_promotions_enabled = value; }
    }

    public CurrencyExchangeType InType
    {
      get { return m_in_type; }
      set { m_in_type = value; }
    }

    // RMS 08-AUG-2014
    public CurrencyExchangeSubType SubType
    {
      get { return m_subtype; }
      set { m_subtype = value; }
    }

    public BankTransactionData BankTransactionData
    {
      get { return m_bank_transaction_data; }
      set { m_bank_transaction_data = value; }
    }

    public Decimal NetAmountDevolution
    {
      get { return m_net_amount_devolution; }
      set { m_net_amount_devolution = value; }
    }

    #endregion

  } // CurrencyExchangeResult

  public class BankTransactionData
  {
    private Int64 m_operation_id;
    private CurrencyExchangeSubType m_type;
    private String m_document_number;
    private String m_bank_name;
    private String m_holder_name;
    private Int32 m_bank_country;
    private String m_card_track_data;
    private decimal m_in_amount;
    private String m_currency_code;
    private String m_card_expiration_date;
    private Boolean m_card_data_edited;
    private String m_check_routing_number;
    private String m_check_account_number;
    private String m_comment;
    private DateTime m_check_datetime;
    private TransactionType m_transaction_type;
    private String m_account_holder_name;


    #region Properties
    public Int64 OperationId
    {
      get { return m_operation_id; }
      set { m_operation_id = value; }
    }

    public CurrencyExchangeSubType Type
    {
      get { return m_type; }
      set { m_type = value; }
    }

    public String DocumentNumber
    {
      get { return m_document_number; }
      set { m_document_number = value; }
    }

    public String BankName
    {
      get { return m_bank_name; }
      set { m_bank_name = value; }
    }

    public String HolderName
    {
      get { return m_holder_name; }
      set { m_holder_name = value; }
    }

    public Int32 BankCountry
    {
      get { return m_bank_country; }
      set { m_bank_country = value; }
    }

    public String TrackData
    {
      get { return m_card_track_data; }
      set { m_card_track_data = value; }
    }

    public decimal inAmount
    {
      get { return m_in_amount; }
      set { m_in_amount = value; }
    }

    public String CurrencyCode
    {
      get { return m_currency_code; }
      set { m_currency_code = value; }
    }


    public String ExpirationDate
    {
      get { return m_card_expiration_date; }
      set { m_card_expiration_date = value; }
    }

    public String RoutingNumber
    {
      get { return m_check_routing_number; }
      set { m_check_routing_number = value; }
    }

    public String AccountNumber
    {
      get { return m_check_account_number; }
      set { m_check_account_number = value; }
    }

    public Boolean CardEdited
    {
      get { return m_card_data_edited; }
      set { m_card_data_edited = value; }
    }

    public String Comments
    {
      get { return m_comment; }
      set { m_comment = value; }
    }

    public DateTime CheckDate
    {
      get { return m_check_datetime; }
      set { m_check_datetime = value; }
    }

    public TransactionType TransactionType
    {
      get { return m_transaction_type; }
      set { m_transaction_type = value; }
    }

    public String AccountHolderName
    {
      get { return m_account_holder_name; }
      set { m_account_holder_name = value; }
    }

    #endregion

    public BankTransactionData()
    {

    }

  }

  public enum GROUP_ELEMENT_TYPE
  {
    ALL = 0,
    GROUP = 1,
    PROV = 2,
    ZONE = 3,
    AREA = 4,
    BANK = 5,
    TERM = 6,
    QUERY = 7
  }

  public enum EXPLOIT_ELEMENT_TYPE
  {
    GROUP = 1,
    PROMOTION = 2,
    PROMOTION_RESTRICTED = 3,
    JACKPOT = 4,
    PATTERNS = 5,
    PROGRESSIVE = 6,
    LCD_MESSAGE = 7,
    // SDS 27-10-2015
    PROMOTION_PLAYED = 8,
    // AVZ 03-11-2015
    GAME_GATEWAY = 9,
    // XGJ 25-08-2016
    BUCKETS_MULTIPLIER = 10,
    // CCG 20-APR-2017
    JACKPOT_CONTRIBUTIONS = 11,
    JACKPOT_AWARDS = 12,
    // ATB 24-08-2017
    RESERVED_TERMINALS = 13
  }

  public enum ParticipateInCashDeskDraw
  {
    Default = 0,
    Yes = 1,
    No = 2,
  }

  // Defines actions on account when changing level
  public enum UpgradeDowngradeAction
  {
    None = 0,
    BlockOnEnter = 1,
    BlockOnLeave = 2,
    Both = 3
  }

  public enum AwardPointsStatus
  {
    Default = 0,
    CalculatedAndReward = 1,
    Pending = 2,
    Manual = 6,
    ManualCalculated = 7
  }

  public enum LanguageIdentifier
  {
    Neutral = 0,
    Chinese = 4,
    Czech = 5,
    English = 9,
    Spanish = 10,
    French = 12,
    Italian = 16,
    Korean = 18,
    Russian = 25,
  }

  //ACC 31-MAR-2014
  //ETP 19-OCT-2015 Added new gaming hall mode.
  public enum SYSTEM_MODE
  {
    CASHLESS = 0,
    TITO = 1,
    WASS = 2,
    GAMINGHALL = 3,
    MICO2 = 4,
  }

  //JMM 12-MAY-2014
  public enum PATTERN_TYPE
  {
    UNKNOWN = 0,
    CONSECUTIVE = 1,
    NO_CONSECUTIVE_WITH_ORDER = 2,
    NO_ORDER = 3,
    NO_ORDER_WITH_REPETITION = 4,
  }

  public enum GTPlayerType
  {
    Unknown = -1,
    AnonymousWithCard = 0,
    Customized = 1,
    AnonymousWithoutCard = 2,
  }

  public enum CageCurrencyType
  {
    Creditline = -7,
    Check = -2,
    BankCard = -1,
    Bill = 0,
    Coin = 1,
    Others = 99,
    // see too CageCurrencyType & CurrencyExchangeType
    ChipsRedimible = 1001,
    ChipsNoRedimible = 1002,
    ChipsColor = 1003,
  }


  // DCS 28-ABR-2015
  public enum CurrencyType
  {
    Foreign = 0,
    National = 1,
  }

  public enum StatusUpdateCurrencyType
  {
    Nothing = 0,
    NewCurrency = 1,
    NewSiteNationalCurrency = 2,
    NewSiteForeignCurrency = 4,
  }

  public class CashierConceptOperationResult
  {
    private Int64 m_concept_id;
    private Int32 m_quantity;
    private Decimal m_amount;
    private String m_iso_code;
    private CurrencyExchangeType m_currency_type;
    private String m_comment;

    public Int32 Quantity
    {
      get { return m_quantity; }
      set { m_quantity = value; }
    }

    public Decimal Amount
    {
      get { return m_amount; }
      set { m_amount = value; }
    }

    public String IsoCode
    {
      get { return m_iso_code; }
      set { m_iso_code = value; }
    }

    public CurrencyExchangeType CurrencyType
    {
      get { return m_currency_type; }
      set { m_currency_type = value; }
    }

    public Int64 ConceptId
    {
      get { return m_concept_id; }
      set { m_concept_id = value; }
    }

    public String Comment
    {
      get { return m_comment; }
      set { m_comment = value; }
    }
  }

  public enum PSA_TYPE
  {
    WINPSA = 1,
    ALESIS = 2,
  }


  public enum CashierSessionTransferStatus
  {
    None = 0,
    Sent = 1,
    Received = 2,
    Cancelled = 3,
  }

  public enum TerminalBoxType
  {
    None = 0,

    /// <summary>
    /// Box with coins and/or bills in the terminal
    /// </summary>
    Box = 1,

    /// <summary>
    /// Hopper(s) with coins in the terminal
    /// </summary>
    Hopper = 2,
  }

  public enum TerminalCashDirection
  {
    Both = 0,

    /// <summary>
    /// Cash-in in the terminal
    /// </summary>
    In = 1,

    /// <summary>
    /// Cash-out from the terminal
    /// </summary>
    Out = 2,
  }

  [Flags]
  public enum FlagBucketId
  {
    NONE = 0x00000000,          // 0000000
    PuntosCanje = 0x00000001,   // 0000001
    PuntosNivel = 0x00000002,   // 0000010 
    NR = 0x00000004,            // 0000100
    RE = 0x00000008,            // 0001000
    Comp1Canje = 0x00000010,    // 0010000
    Comp2NR = 0x00000020,       // 0100000
    Comp3RE = 0x00000040,       // 1000000
  }

  //11-MAY-2016 FJC Product Backlog Item 12931:Visitas / Recepci�n: Detectar CLiente VIP y Cumplea�os
  public enum HolderHappyBirthDayNotification
  {
    NONE = 0,
    HAPPY_BIRTHDAY_TODAY = 1,
    HAPPY_BIRTHDAY_INCOMMING = 2
  }

  public enum PinPadType
  {
    NONE = -1,
    DUMMY = 0,
    BANORTE = 1
  }

  public enum HolderLevelType
  {
    None = 0,
    A = 1,
    B = 2,
    C = 3,
    D = 4
  }

  //buckets
  //JPO & DDM 26-08-2016
  public enum BucketId
  {
    RedemptionPoints = 1,                //--PuntosCanje
    RankingLevelPoints = 2,              //--PuntosNivel
    Credit_NR = 3,                       //--NR     
    Credit_RE = 4,                       //--RE     
    Comp1_RedemptionPoints = 5,          //--Comp1Canje 
    Comp2_Credit_NR = 6,                 //--Comp2NR 
    Comp3_Credit_RE = 7,                 //--Comp3RE 
    RankingLevelPoints_Generated = 8,      //--PuntosNivel Generados        (automaticos)
    RankingLevelPoints_Discretional = 9,   //--PuntosNivel Discrecionales   (manuales)
  } //BucketId

  public enum PrizeCategory
  {
    Prize_1 = 1,
    Prize_2 = 2,
    Prize_3 = 3,
    Prize_4 = 4,
    Prize_5 = 5 // LTC 31-OCT-2016
  }

  public enum EditSelectionItemType
  {
    NONE                        = 0,

    //--------- JACKPOTS -----------//
    JACKPOT_DASHBOARD           = 1,

    // Jackpot configuration
    JACKPOT_CONFIG              = 2,
    JACKPOT_AWARD_PRIZE_CONFIG  = 3,
    JACKPOT_AWARD_CONFIG        = 4,

    // Jackpot viewer configuration
    JACKPOT_VIEWER_CONFIG       = 5,
    JACKPOT_VIEWER_MONITOR      = 6,
    JACKPOT_VIEWER_TERMINAL     = 7
  }

  public enum NodeStatus
  {
    Enabled = 0,
    Disabled = 1,
    Warning = 2
  }

  public enum ContextMenuAction
  {
    // Common
    NONE                    = 0,
    ROOT_COLLAPSE_ALL       = 1,
    ROOT_EXPAND_ALL         = 2,
    ROOT_REFRESH            = 3,

    // Jackpots
    JACKPOT_NEW             = 10,
    JACKPOT_CLONE           = 11,
    JACKPOT_ADD             = 12,
    JACKPOT_RESET           = 13,

    //.....
  }

  public enum TerminalDrawGame
  {
    NONE = 0,
    WELCOMEDRAW = 1,

  }

  [Flags]
  public enum SiteServices
  {
    NONE = 0,
    IN_HOUSE_API = 1

  } // SiteServices


}
