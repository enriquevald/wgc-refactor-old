using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace WSI.Common
{
  public static class SharedFunctions
  {

    public static String FormatFullName(FULL_NAME_TYPE FullNameType, String Name, String MiddleName, String LastName1, String LastName2, GeneralParam.Dictionary GeneralParam)
    {
      String _aux_full_name;

      if (string.IsNullOrEmpty(Name) && string.IsNullOrEmpty(LastName1))
      {
        return "";
      }

      switch (FullNameType)
      {
        case FULL_NAME_TYPE.WITHHOLDING:
          _aux_full_name = (LastName1 ?? "").Trim();
          if (LastName2 != "")
          {
            _aux_full_name += " " + (LastName2 ?? "").Trim();
          }
          _aux_full_name += ", " + (Name ?? "").Trim();

          return _aux_full_name.Trim();

        case FULL_NAME_TYPE.ACCOUNT:
        default:
          // RCI 13-AUG-2014: Don't use them if they are not visible.
          if (!GeneralParam.GetBoolean("Account.VisibleField", "Name2", true))
          {
            LastName2 = "";
          }
          if (!GeneralParam.GetBoolean("Account.VisibleField", "Name4", true))
          {
            MiddleName = "";
          }

          _aux_full_name = (Name ?? "").Trim() + " ";
          if (!String.IsNullOrEmpty(MiddleName))
          {
            _aux_full_name += (MiddleName ?? "").Trim() + " ";
          }
          _aux_full_name += (LastName1 ?? "").Trim() + " ";
          _aux_full_name += (LastName2 ?? "").Trim();

          return _aux_full_name.Trim();
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: It converts a Generic list to DataSet.
    // 
    //  PARAMS:
    //      - INPUT: IList<T> list
    //
    //
    // RETURNS:
    //      - DataSet
    // 
    //   NOTES:
    //
    public static DataSet ToDataSet<T>(IList<T> list)
    {
      Type elementType = typeof(T);
      DataSet ds = new DataSet();
      DataTable t = new DataTable();
      ds.Tables.Add(t);

      foreach (var propInfo in elementType.GetProperties())
      {
        Type ColType = Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;
        t.Columns.Add(propInfo.Name, ColType);
      }

      foreach (T item in list)
      {
        DataRow row = t.NewRow();

        foreach (var propInfo in elementType.GetProperties())
        {
          row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
        }

        t.Rows.Add(row);
      }

      return ds;
    }
    
  }
}
