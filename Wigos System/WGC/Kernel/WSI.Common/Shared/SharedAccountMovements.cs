//-------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   SharedAccountMovements.cs
// DESCRIPTION:   
// AUTHOR:        Jes�s �ngel Blanco
// CREATION DATE: 15-JUN-2012
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 15-JUN-2012  JAB    Initial version.
// 24-MAY-2013  LEM    New CashierMovementsTable constructor added that replace CashierSessionInfo.TerminalName (cashier terminal name) by new parameter TerminalName (real terminal name)
//                     Also replace CashierSessionInfo.TerminalId (cashier terminal Id) by new parameter TerminalId (real terminal Id) only for gaming terminals              
// 22-OCT-2013  LEM    Added new field to movements linking TrackData to movement info
// 31-OCT-2013  ICS    Fixed Bug WIG-356: Bad trackdata when has changed and there is an operation undo.
// 15-OCT-2013  JBP    Divide GetOrCreateVirtualAccount and new overload added.  
// 18-ENE-2016  JRC    For Buckets: AccountMovements can have different sessionID
// 28-SEP-2016  RGR    PBI 17447: Televisa - Draw Cash (Cash prize) - Recharge Operation
// 29-MAR-2017  DPC    WIGOS-685: CreditLine - Undo last operation "Get Marker"
// -------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common.ExtentionMethods;
using DataTable = System.Data.DataTable;

namespace WSI.Common
{
  public class AccountMovementsTest
  {
    public void Test1(CashierSessionInfo CashierSessionInfo, SqlTransaction SqlTrx)
    {
      AccountMovementsTable _table;
      MovementType _movement_type;

      _table = new AccountMovementsTable(CashierSessionInfo);

      _movement_type = MovementType.AccountPersonalization;

      //_table.Add(1, 2, _movement_type, 4, 5, 6, 7);
      //_table.Add(2, 3, _movement_type, 6, 3, 8, 9);
      //_table.Add(3, 7, _movement_type, 1, 1, 2, 1);
      //_table.Add(6, 4, _movement_type, 4, 2, 7, 9);
      //_table.Add(4, 8, _movement_type, 0, 3, 0, 0);
      _table.Add(1, 915, _movement_type, 101, 0, 0, 101);
      _table.Add(2, 915, _movement_type, 202, 202, 0, 0);

      _table.Save(SqlTrx);
    }

    public void Test2(Int32 TerminalId, String TerminalName, Int64 PlaySessionId, SqlTransaction SqlTrx)
    {
      AccountMovementsTable _table;
      MovementType _movement_type;

      _table = new AccountMovementsTable(TerminalId, TerminalName, PlaySessionId);

      _movement_type = MovementType.StartCardSession;

      //_table.Add(1, 2, _movement_type, 4, 5, 6, 7);
      //_table.Add(2, 3, _movement_type, 6, 3, 8, 9);
      //_table.Add(3, 7, _movement_type, 1, 1, 2, 1);
      //_table.Add(6, 4, _movement_type, 4, 2, 7, 9);
      //_table.Add(4, 8, _movement_type, 0, 3, 0, 0);
      _table.Add(3, 915, _movement_type, 303, 0, 0, 303);
      _table.Add(4, 915, _movement_type, 404, 404, 0, 0);

      _table.Save(SqlTrx);
    }

  } // AccountMovementsTest


  public class AccountMovementsTable
  {
    private DataTable m_table = null;

    private CashierSessionInfo m_cashier_session_info = null;

    private Int32 m_terminal_id = 0;
    private String m_terminal_name = "";
    private Int64 m_play_session_id = 0;

    private String m_track_data = null;
    private Int64 m_account_id = 0;

    // For now, used for manual end sessions. Indicates the Cashier or GUI terminal name and user.
    private String m_cashier_name = "";

    public void SetPlaySessionId(Int64 PlaySessionId)
    {
      if (m_play_session_id != 0)
      {
        return;
      }
      m_play_session_id = PlaySessionId;
    }

    public void SetAccountTrackData(Int64 AccountId, String TrackData)
    {
      if (String.IsNullOrEmpty(TrackData))
      {
        m_track_data = "";
      }
      else
      {
        m_track_data = TrackData;
        m_account_id = AccountId;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Default constructor.
    //
    //  PARAMS :
    //      - INPUT : 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public void InitSchema()
    {
      DataColumn _mov_col;

      m_table = new DataTable();

      _mov_col = m_table.Columns.Add("AM_MOVEMENT_ID", Type.GetType("System.Int64"));
      _mov_col.AutoIncrement = true;
      _mov_col.AutoIncrementSeed = -1;
      _mov_col.AutoIncrementStep = -1;

      m_table.Columns.Add("AM_OPERATION_ID", Type.GetType("System.Int64"));
      m_table.Columns.Add("AM_ACCOUNT_ID", Type.GetType("System.Int64")).AllowDBNull = false;

      m_table.Columns.Add("AM_TYPE", Type.GetType("System.Int32")).AllowDBNull = false;
      m_table.Columns.Add("AM_INITIAL_BALANCE", Type.GetType("System.Decimal")).AllowDBNull = false;
      m_table.Columns.Add("AM_SUB_AMOUNT", Type.GetType("System.Decimal")).AllowDBNull = false;
      m_table.Columns.Add("AM_ADD_AMOUNT", Type.GetType("System.Decimal")).AllowDBNull = false;
      m_table.Columns.Add("AM_FINAL_BALANCE", Type.GetType("System.Decimal")).AllowDBNull = false;

      m_table.Columns.Add("AM_DETAILS", Type.GetType("System.String")).AllowDBNull = true;
      m_table.Columns.Add("AM_REASONS", Type.GetType("System.String")).AllowDBNull = true;
      m_table.Columns.Add("AM_TRACK_DATA", Type.GetType("System.String")).AllowDBNull = true;

      m_table.Columns.Add("AM_TERMINAL_ID", Type.GetType("System.Int32")).AllowDBNull = true;
      m_table.Columns.Add("AM_TERMINAL_NAME", Type.GetType("System.String")).AllowDBNull = true;

      m_table.Columns.Add("AM_PLAY_SESSION_ID", Type.GetType("System.String")).AllowDBNull = true; //JRC 

      m_table.PrimaryKey = new DataColumn[] { m_table.Columns["AM_MOVEMENT_ID"] };

    } // InitSchema

    public AccountMovementsTable(CashierSessionInfo CashierSessionInfo)
    {
      InitSchema();

      m_cashier_session_info = CashierSessionInfo.Copy();

      m_terminal_id = 0;
      m_terminal_name = "";
      m_play_session_id = 0;

      m_cashier_name = "";

    } // AccountMovementsTable



    public AccountMovementsTable(CashierSessionInfo CashierSessionInfo, Int32 TerminalId, String TerminalName)
      : this(CashierSessionInfo)
    {
      if (!String.IsNullOrEmpty(TerminalName))
      {
        m_cashier_session_info.TerminalName = TerminalName;
      }

      if (m_cashier_session_info.UserType != GU_USER_TYPE.SYS_PROMOBOX)
      {
        m_terminal_id = TerminalId;
        m_terminal_name = m_cashier_session_info.TerminalName;
        if (!String.IsNullOrEmpty(m_cashier_session_info.AuthorizedByUserName))
        {
          m_terminal_name = m_cashier_session_info.AuthorizedByUserName + "@" + m_terminal_name;
        }
        m_cashier_session_info = null;
      }

    } // AccountMovementsTable

    public AccountMovementsTable(Int32 TerminalId, String TerminalName, Int64 PlaySessionId)
      : this(TerminalId, TerminalName, PlaySessionId, "")
    {

    } // AccountMovementsTable

    public AccountMovementsTable(Int32 TerminalId, String TerminalName, Int64 PlaySessionId, String CashierName)
    {
      InitSchema();

      m_cashier_session_info = null;

      m_terminal_id = TerminalId;
      m_terminal_name = TerminalName;
      m_play_session_id = PlaySessionId;

      m_cashier_name = CashierName;

    } // AccountMovementsTable

    //------------------------------------------------------------------------------
    // PURPOSE : Adds a new record.
    //
    //  PARAMS :
    //      - INPUT : 
    //             - OperationId:                     AM_OPERATION_ID
    //             - AccountId:                       AM_ACCOUNT_ID
    //             - Type:                            AM_TYPE
    //             - InitialBalance:                  AM_INITIAL_BALANCE
    //             - SubAmount:                       AM_SUB_AMOUNT 
    //             - AddAmount:                       AM_ADD_AMOUNT 
    //             - FinalBalance:                    AM_FINAL_BALANCE 
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: The record has been successfully added.
    //      - False: The record has not been successfully added.
    //
    public Boolean Add(Int64 OperationId,
                   Int64 AccountId,
                   MovementType Type,
                   Decimal InitialBalance,
                   Decimal SubAmount,
                   Decimal AddAmount,
                   Decimal FinalBalance)
    {
      return Add(OperationId, AccountId, Type, InitialBalance, SubAmount, AddAmount, FinalBalance, "", -1, "");
    }

    public Boolean Add(Int64 OperationId,
                   Int64 AccountId,
                   MovementType Type,
                   Decimal InitialBalance,
                   Decimal SubAmount,
                   Decimal AddAmount,
                   Decimal FinalBalance,
                   Int64 PlaySessionId
      )
    {
      return Add(OperationId, AccountId, Type, InitialBalance, SubAmount, AddAmount, FinalBalance, "", -1, "", "", PlaySessionId); //JRC
    }


    public Boolean Add(Int64 OperationId,
                       Int64 AccountId,
                       MovementType Type,
                       Decimal InitialBalance,
                       Decimal SubAmount,
                       Decimal AddAmount,
                       Decimal FinalBalance,
                       String Details)
    {
      return Add(OperationId, AccountId, Type, InitialBalance, SubAmount, AddAmount, FinalBalance, Details, -1, "");
    }

    public Boolean Add(Int64 OperationId,
                   Int64 AccountId,
                   MovementType Type,
                   Decimal InitialBalance,
                   Decimal SubAmount,
                   Decimal AddAmount,
                   Decimal FinalBalance,
                   String Details,
                   Int32 TerminalId,
                   String TerminalName)
    {
      // XGJ 24-FEB-2017
      return Add(OperationId, AccountId, Type, InitialBalance, SubAmount, AddAmount, FinalBalance, Details, TerminalId, TerminalName, "", m_play_session_id);//JRC
    }

    public Boolean Add(Int64 OperationId,
                       Int64 AccountId,
                       MovementType Type,
                       Decimal InitialBalance,
                       Decimal SubAmount,
                       Decimal AddAmount,
                       Decimal FinalBalance,
                       String Details,
                       Int32 TerminalId,
                       String TerminalName,
                       String Reasons)
    {
      return Add(OperationId, AccountId, Type, InitialBalance, SubAmount, AddAmount, FinalBalance, Details, TerminalId, TerminalName, Reasons, m_play_session_id);
    }


    public Boolean Add(Int64 OperationId,
                       Int64 AccountId,
                       MovementType Type,
                       Decimal InitialBalance,
                       Decimal SubAmount,
                       Decimal AddAmount,
                       Decimal FinalBalance,
                       String Details,
                       Int32 TerminalId,
                       String TerminalName,
                       String Reasons,
                       Int64 PlaySessionId) //JRC
    {
      DataRow _row;

      try
      {
        _row = m_table.NewRow();
        _row["AM_OPERATION_ID"] = OperationId;
        _row["AM_ACCOUNT_ID"] = AccountId;
        _row["AM_TYPE"] = Type;
        _row["AM_INITIAL_BALANCE"] = InitialBalance;
        _row["AM_SUB_AMOUNT"] = SubAmount;
        _row["AM_ADD_AMOUNT"] = AddAmount;
        _row["AM_FINAL_BALANCE"] = FinalBalance;

        if (String.IsNullOrEmpty(Details))
        {
          _row["AM_DETAILS"] = DBNull.Value;
        }
        else
        {
          _row["AM_DETAILS"] = Details;
        }

        if (String.IsNullOrEmpty(Reasons))
        {
          _row["AM_REASONS"] = DBNull.Value;
        }
        else
        {
          _row["AM_REASONS"] = Reasons;
        }

        if (TerminalId != -1)
        {
          _row["AM_TERMINAL_ID"] = TerminalId;
        }
        else
        {
          _row["AM_TERMINAL_ID"] = DBNull.Value;
        }

        if (String.IsNullOrEmpty(TerminalName))
        {
          _row["AM_TERMINAL_NAME"] = DBNull.Value;
        }
        else
        {
          _row["AM_TERMINAL_NAME"] = TerminalName;
        }

        if (PlaySessionId != 0) //JRC
        {
          _row["AM_PLAY_SESSION_ID"] = PlaySessionId;  //JRC
        }
        else
        {
          _row["AM_PLAY_SESSION_ID"] = m_play_session_id;  //JRC
        }


        m_table.Rows.Add(_row);

        return true;
      }
      catch
      { ; }

      return false;
    } // Add

    public static String ExternalTrackData(Int64 AccountId, IDbTransaction Trx)
    {
      String _sql_str;
      Object _obj;
      String _external_trackdata;

      _external_trackdata = "";

      try
      {
        _sql_str = " SELECT dbo.TrackDataToExternal(AC_TRACK_DATA) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId ";

        using (IDbCommand _cmd = Trx.Connection.CreateCommand())
        {
          _cmd.CommandText = _sql_str;
          _cmd.Connection = Trx.Connection;
          _cmd.Transaction = Trx;
          _cmd.AddParameter("@pAccountId", DbType.Int64, AccountId);

          _obj = _cmd.ExecuteScalar();
        }

        if (_obj != null && _obj != DBNull.Value)
        {
          _external_trackdata = (String)_obj;
        }
      }
      catch (Exception)
      { }

      return _external_trackdata;
    }

    public Int64 LastSavedMovementId
    {
      get
      {
        if (m_table.Rows.Count > 0)
          return (Int64)m_table.Rows[m_table.Rows.Count - 1]["AM_MOVEMENT_ID"];
        else
          return -1;
      }
    }

    public void Clear()
    {

      m_table.Clear();

    }

    public Int64 SavedMovementsId(Int32 Index)
    {
      if (m_table.Rows.Count > 0 && Index >= 0 && Index < m_table.Rows.Count)
      {
        return (Int64)m_table.Rows[Index]["AM_MOVEMENT_ID"];
      }
      else
        return -1;

    }

    //------------------------------------------------------------------------------
    // PURPOSE : Save the changes in the database.
    //
    //  PARAMS :
    //      - INPUT : 
    //             - Trx: SqlTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True: The changes are saved successfully.
    //      - False: The changes aren't saved successfully.
    //
    public Boolean Save(IDbTransaction Trx)
    {
      SqlTransaction _sql_trx = Trx as SqlTransaction;
      if (_sql_trx != null)
      {
        return Save(_sql_trx);
      }
      //TODO: if in usint test presune true
      else
        return true;
    }

    public Boolean Save(SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _num_rows_inserted;
      Boolean _is_multisite;
      Int32 _value;
      SqlParameter _out_param;
      Int64 _first_account_id;
      String _track_data;
      Int32.TryParse(MultiPromos.ReadGeneralParams("MultiSite", "IsCenter", Trx), out _value);
      _is_multisite = _value == 1;

      try
      {

        if (m_table.Rows.Count == 0)
        {
          return true;
        }

        if (LastSavedMovementId > 0)
        {
          return true;
        }

        if (m_track_data == null)
        {
          _first_account_id = (Int64)m_table.Rows[0]["AM_ACCOUNT_ID"];
          _track_data = ExternalTrackData(_first_account_id, Trx);
        }
        else
        {
          _first_account_id = m_account_id;
          _track_data = m_track_data;
        }

        foreach (DataRow _row in m_table.Rows)
        {
          if (_first_account_id == (Int64)_row["AM_ACCOUNT_ID"])
          {
            _row["AM_TRACK_DATA"] = _track_data;
          }
        }

        _sb = new StringBuilder();

        _sb.AppendLine(" DECLARE   @_trackdata AS NVARCHAR(50) ");
        _sb.AppendLine(" SET       @_trackdata = '' ");
        _sb.AppendLine(" IF @pCardData IS NULL ");
        _sb.AppendLine("   SELECT  @_trackdata = dbo.TrackDataToExternal(AC_TRACK_DATA) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId ");
        _sb.AppendLine(" IF @_trackdata = '' ");
        _sb.AppendLine("   SET     @_trackdata = @pCardData ");

        if (_is_multisite)
        {
          _sb.AppendLine("DECLARE   @pSequence12Value AS BIGINT ");
          _sb.AppendLine(" ");
          _sb.AppendLine("UPDATE   SEQUENCES ");
          _sb.AppendLine("   SET   SEQ_NEXT_VALUE = SEQ_NEXT_VALUE + 1 ");
          _sb.AppendLine(" WHERE   SEQ_ID         = @pSeqId12; ");
          _sb.AppendLine(" ");
          _sb.AppendLine("SELECT   @pSequence12Value = SEQ_NEXT_VALUE - 1 FROM SEQUENCES WHERE SEQ_ID = @pSeqId12; ");
          _sb.AppendLine(" ");
        }
        _sb.AppendLine("INSERT INTO   ACCOUNT_MOVEMENTS                 ");
        _sb.AppendLine("            ( AM_ACCOUNT_ID                     ");
        _sb.AppendLine("            , AM_TYPE                           ");
        _sb.AppendLine("            , AM_INITIAL_BALANCE                ");
        _sb.AppendLine("            , AM_SUB_AMOUNT                     ");
        _sb.AppendLine("            , AM_ADD_AMOUNT                     ");
        _sb.AppendLine("            , AM_FINAL_BALANCE                  ");
        _sb.AppendLine("            , AM_DATETIME                       ");
        _sb.AppendLine("            , AM_CASHIER_ID                     ");
        _sb.AppendLine("            , AM_CASHIER_NAME                   ");
        _sb.AppendLine("            , AM_OPERATION_ID                   ");
        _sb.AppendLine("            , AM_PLAY_SESSION_ID                ");
        _sb.AppendLine("            , AM_TERMINAL_ID                    ");
        _sb.AppendLine("            , AM_TERMINAL_NAME                  ");
        _sb.AppendLine("            , AM_DETAILS                        ");
        _sb.AppendLine("            , AM_REASONS                        ");
        _sb.AppendLine("            , AM_TRACK_DATA                     ");
        if (_is_multisite)
        {
          _sb.AppendLine("            , AM_MOVEMENT_ID                  ");
        }
        _sb.AppendLine(") ");
        _sb.AppendLine("     VALUES ( @pAccountId                       ");
        _sb.AppendLine("            , @pType                            ");
        _sb.AppendLine("            , @pInitialBalance                  ");
        _sb.AppendLine("            , @pSubAmount                       ");
        _sb.AppendLine("            , @pAddAmount                       ");
        _sb.AppendLine("            , @pFinalBalance                    ");
        _sb.AppendLine("            , GETDATE()                         ");
        _sb.AppendLine("            , @pCashierId                       ");
        _sb.AppendLine("            , @pCashierName                     ");
        _sb.AppendLine("            , @pOperationId                     ");
        _sb.AppendLine("            , CASE WHEN ISNULL(@pPlaySessionId,0) = 0 THEN NULL ELSE @pPlaySessionId END  ");
        _sb.AppendLine("            , @pTerminalId                      ");
        _sb.AppendLine("            , @pTerminalName                    ");
        _sb.AppendLine("            , @pDetails                         ");
        _sb.AppendLine("            , @pReasons                         ");
        _sb.AppendLine("            , @_trackdata                       ");
        if (_is_multisite)
        {
          _sb.AppendLine("            , @pSequence12Value               ");
        }
        _sb.AppendLine(") ");
        if (_is_multisite)
        {
          _sb.AppendLine(" SET @pMovementId = @pSequence12Value");
        }
        else
        {
          _sb.AppendLine(" SET @pMovementId = SCOPE_IDENTITY()");
        }

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).SourceColumn = "AM_OPERATION_ID";
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "AM_ACCOUNT_ID";

          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "AM_TYPE";
          _sql_cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).SourceColumn = "AM_INITIAL_BALANCE";
          _sql_cmd.Parameters.Add("@pSubAmount", SqlDbType.Money).SourceColumn = "AM_SUB_AMOUNT";
          _sql_cmd.Parameters.Add("@pAddAmount", SqlDbType.Money).SourceColumn = "AM_ADD_AMOUNT";
          _sql_cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).SourceColumn = "AM_FINAL_BALANCE";
          _sql_cmd.Parameters.Add("@pDetails", SqlDbType.NVarChar, 256).SourceColumn = "AM_DETAILS";
          _sql_cmd.Parameters.Add("@pReasons", SqlDbType.NVarChar, 64).SourceColumn = "AM_REASONS";
          _sql_cmd.Parameters.Add("@pCardData", SqlDbType.NVarChar, 50).SourceColumn = "AM_TRACK_DATA";


          if (m_cashier_session_info != null)
          {
            if (m_cashier_session_info.TerminalId > 0)
            {
              _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = m_cashier_session_info.TerminalId;
            }
            else
            {
              _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = DBNull.Value;
            }

            if (!String.IsNullOrEmpty(m_cashier_session_info.TerminalName))
            {
              if (String.IsNullOrEmpty(m_cashier_session_info.AuthorizedByUserName))
              {
                _sql_cmd.Parameters.Add("@pCashierName", SqlDbType.NVarChar, 50).Value = m_cashier_session_info.TerminalName;
              }
              else
              {
                _sql_cmd.Parameters.Add("@pCashierName", SqlDbType.NVarChar, 50).Value = m_cashier_session_info.AuthorizedByUserName + "@" + m_cashier_session_info.TerminalName;
              }
            }
            else
            {
              _sql_cmd.Parameters.Add("@pCashierName", SqlDbType.Int).Value = DBNull.Value;
            }

            _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "AM_TERMINAL_ID";
            //_sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = DBNull.Value;
            _sql_cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar, 50).SourceColumn = "AM_TERMINAL_NAME";
            //_sql_cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar, 50).Value = DBNull.Value;

            _sql_cmd.Parameters.Add("@pPlaySessionId", SqlDbType.NVarChar, 50).SourceColumn = "AM_PLAY_SESSION_ID"; //JRC

          }
          else
          {
            _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.Int).Value = DBNull.Value;

            // m_cashier_name is used, for example, when closing manually a PlaySession (from Cashier or GUI).
            if (String.IsNullOrEmpty(m_cashier_name))
            {
              _sql_cmd.Parameters.Add("@pCashierName", SqlDbType.NVarChar, 50).Value = DBNull.Value;
            }
            else
            {
              _sql_cmd.Parameters.Add("@pCashierName", SqlDbType.NVarChar, 50).Value = m_cashier_name;
            }

            if (m_terminal_id == 0)
            {
              _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = DBNull.Value;
            }
            else
            {
              _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = m_terminal_id;
            }

            if (String.IsNullOrEmpty(m_terminal_name))
            {
              _sql_cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar, 50).Value = DBNull.Value;
            }
            else
            {
              _sql_cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar, 50).Value = m_terminal_name;
            }

            _sql_cmd.Parameters.Add("@pPlaySessionId", SqlDbType.NVarChar, 50).Value = DBNull.Value; //JRC
          }

          if (_is_multisite)
          {
            _sql_cmd.Parameters.Add("@pSeqId12", SqlDbType.Int).Value = (Int32)SequenceId.AccountMovements;
          }

          _out_param = _sql_cmd.Parameters.Add("@pMovementId", SqlDbType.BigInt);
          _out_param.SourceColumn = "AM_MOVEMENT_ID";
          _out_param.Direction = ParameterDirection.Output;

          _sql_cmd.UpdatedRowSource = UpdateRowSource.OutputParameters;

          using (SqlDataAdapter _da = new SqlDataAdapter())
          {
            _da.InsertCommand = _sql_cmd;

            _num_rows_inserted = _da.Update(m_table);

            if (_num_rows_inserted == m_table.Rows.Count)
            {
              //HULK TODO
              if (!InsertCommissionPending(_first_account_id, Trx))
              {
                return false;
              }

              return true;
            }
          }
        }
      }
#if !SQL_BUSINESS_LOGIC
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
#else
      catch
      { ; }
#endif

      return false;
    } // Save 

    /// <summary>
    /// Get a datatable with movements
    /// </summary>
    /// <returns></returns>
    public DataTable getTable()
    {
      return m_table;
    }

    private Boolean InsertCommissionPending(Int64 AccountId, SqlTransaction SqlTrx)
    {

      StringBuilder _sb;
      TimeSpan _timespan;

      try
      {

        if (MultiPromos.ReadGeneralParams("Junkets", "Enabled", SqlTrx) != "1")
        {
          return true;
        }

        _timespan = new TimeSpan();

        _sb = new StringBuilder();
        _sb.AppendLine("   INSERT INTO   JUNKETS_COMMISSIONS_PENDING   ");
        _sb.AppendLine("               ( JCP_ACCOUNT_ID                ");
        _sb.AppendLine("   			       , JCP_DATETIME                  ");
        _sb.AppendLine("   			       )                               ");
        _sb.AppendLine("       VALUES                                  ");
        _sb.AppendLine("               ( @pAccountId                   ");
        _sb.AppendLine("   			       , @pDatetime                    ");
        _sb.AppendLine("   			       )                               ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = AccountId;
          _cmd.Parameters.Add("@pDatetime", SqlDbType.DateTime).Value = DateTime.Now.Subtract(_timespan);

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
        }

        return true;

      }
      catch
      { ; }

      return false;

    }

  } // AccountMovementsTable


}
