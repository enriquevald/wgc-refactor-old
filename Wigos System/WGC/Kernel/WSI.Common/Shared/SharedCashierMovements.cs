//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: SharedCashierMovements.cs
//  
//   DESCRIPTION: Implements the following classes:
//                1. CommonCashierSessionInfo
//                2. CashierMovementsTable
//
//        AUTHOR: Susana Samso
// 
// CREATION DATE: 15-JUN-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
//19-JUN-2012  SSC    Initial version.
//19-JUN-2012  JAB    Added GetCashierMovementAddSubAmount and GetCashierMovementBalanceIncrement functions.
//27-SEP-2012  DDM    Added new movement REQUEST_TO_VOUCHER_REPRINT
//24-MAY-2013  LEM    New CashierMovementsTable constructor added that replace CashierSessionInfo.TerminalName (cashier terminal name) by new parameter TerminalName (real terminal name)
//18-JUN-2013  ICS & DLL    Added new columns for currency exchange. Added new function to add new currency exchange movements
//02-SEP-2013  NMR    Added case for TITO ticket reprinting
//25-SEP-2013  NMR    Add case for TITO tickets movements
//22-OCT-2013  LEM    Link TrackData to movement info if account id is not null
//22-NOV-2013  SMN    Set a default value to column CM_AUX_AMOUNT
//                    Create a new column: CM_CURRENCY_DENOMINATION
//                    Replace null value with 0 of column CM_ACCOUNT_ID
//                    Function Save: Call stored procedure SP_ICM
// 10-DEC-2013 LJM    Fixed Bug WIG-468: Error undoing operations on Cash Balance
// 12-FEB-2014 RCI    Remove SP_ICM. All the work to save the cashier movements is done again in the Save() function.
// 08-APR-2014 ICS    Fixed Bug WIGOSTITO-1193: Amount awarded does not appears in the cashier movement of creating a handpay ticket
// 25-APR-2014 ICS    Fixed Bug WIGOSTITO-1209: The Handpay and Jackpot tickets do a mismatch in cash delivered column
// 25-APR-2014 DHA & JML Rename/Added TITO Cashier/Account Movements
// 06-JUN-2014 DLL    Fixed WIG-1020: Error chips sale when GamingTables->Cashier.Mode is enabled. Update csc_balance in cashier_sessions_by_currency when is not necessary
// 25-JUL-2014 DLL    Chips are moved to a separate table
// 08-AUG-2014 RMS    Added supoort for Specific Bank Card Types
// 04-SEP-2014 DLL    Function AddCashAdvanceExchange: New movement for cash advance
// 16-SEP-2014 SMN    Added new cashier movements (concept movements)
// 08-OCT-2014 DHA    Added new movement, advice if a recharge is going to refund without plays
// 04-NOV-2014 JBC    Fixed Bug WIG-1636: Added EXCESS to movement.
// 24-NOV-2014 MPO    LOGRAND CON02: Added IVA calculated (when needed) and input params class for cashin.
// 15-DEC-2014 JBC, DLL, JPJ    New Company B commission movements
// 21-JAN-2015 JPJ    Fixed WIG-1959: Closing with excess was contemplated and changed the balance generating a balance discrepancy. 
// 09-JUL-2015 FOS    Add replacement card movements
// 24-AUG-2015 FAV    TFS-3257: Transfer between cashier sessions. 
// 04-SEP-2015 ETP    Bug 3841: Changed order of INSERT query to cashier_movements table.
// 07-OCT-2015 GMV    TFS-4684: New card cost being shown on cash monitor when first recharge not onn national currency
// 04-SEP-2015 YNM    TFS-4096: Cashier: screen last inserted bills and tickets
// 07-OCT-2015 GMV    TFS-4684: New card cost being shown on cash monitor when first recharge not onn national currency
// 29-OCT-2015 FAV    PBI 4695: New movements type added for gaming hall
// 13-JAN-2015 JML    Product Backlog Item 6559: Cashier draw (RE in kind)
// 19-JAN-2016 RAB    Product Backlog Item 7941:Multiple buckets: Cajero: A�adir/Restar valores a los buckets
// 28-JAN-2016 DHA    Product Backlog Item 8351:Floor Dual Currency: Movimientos de tickets TITO en M.N.
// 16-FEB-2016 FAV    Fixed Bug 9347 Added value of the machine tickets voided
// 24-FEB-2016 AVZ    Product Backlog Item 9363:Promo RE impuestos: Kernel: Promociones Redimibles: Aplicar impuestos
// 02-MAR-2016 DHA    Product Backlog Item 10085:Winpot - Added Tax Provisions movements
// 21-MAR-2016 DHA    Product Backlog Item 10051: add CageCurrencyType to historification
// 04-APR-2016 RGR    Product Backlog Item 11018:UNPLANNED - Temas varios Winions - Mex Sprint 22
// 14-APR-2016 RGR    Product Backlog Item 11295: Codere - Custody Tax: Implement tax
// 20-APR-2016 EOR    Product Backlog Item 11296: Codere - Custody Tax: Implement Devolution Tax Custody
// 22-MAR-2106 YNM    Bug 10759: Reception error paying with currency
// 24-MAR-2016 RAB    Bug 10745: Reception: with the GP requiereNewCard to "0" when assigning a card client is counted as "replacement"
// 31-MAR-2016 SMN    Bug 11264:Buckets: las sesiones de caja de sistema cierran restando el total de puntos
// 04-ABR-2016 YNM    Bug 11282: Reception error paying with currency
// 04-APR-2016 RGR    Product Backlog Item 11018:UNPLANNED - Temas varios Winions - Mex Sprint 22
// 07-ABR-2016 YNM    Bug 11523: it didn't update propertly cash monitor
// 03-MAY-2016 FAV    Product Backlog Item 12653:Cajero: Currency exchange for multiple denominations (Mallorca)
// 23-MAY-2016 FAV     PBI 13491: Multicurrency, added functionality for Cashless
// 24-MAY-2016 DHA    Product Backlog Item 9848: drop box gaming tables
// 09-JUN-2016 DHA    Product Backlog Item 13402: Rolling & Fixed closing stock
// 06-JUL-2016 DHA    Product Backlog Item 15064: multicurrency chips sale/purchase
// 28-JUN-2016 DHA    Product Backlog Item 15903: Gaming tables (Fase 5): Drop box collection on Cashier
// 01-AUG-2016 RAB    Product Backlog Item 15614: TPV Televisa: Undo last operation on bank card
// 19-AGU-2016 DHA    Fixed Bug 16894: Error on closing gaming tables sessions
// 19-SEP-2016 EOR    PBI 17468: Televisa -Service Charge: Cashier Movements
// 27-SEP-2016 DHA    Fixed Bug 17847: added gaming profit for check and bank card
// 27-SEP-2016 RAB    Bug 17875: TPV Televisa: Shortages of balance to cancel a credit card operation. Problems with the price of the player card
// 28-SEP-2016 RGR    PBI 17447: Televisa - Draw Cash (Cash prize) - Recharge Operation
// 04-OCT-2016 RGR    PBI 17447: Televisa - Draw Cash (Cash prize) - Recharge Operation
// 30-NOV-2016 FOS    PBI 19197: Reopen cashier sessions and gambling tables sessions
// 09-JAN-2017 DPC&FOS Bug 21977: Error in cashier when realizes deposits 
// 25-JAN-2017 FGB    Bug 21547: Cashier: Cannot "reopen cash session" when it is closed with cash, bank card and check tickets.
// 08-FEB-2017 FAV    PBI 25268:Third TAX - Payment Voucher
// 21-MAR-2017 FAV    PBI 25735:Third TAX - Movement reports
// 22-MAY-2017 FAV    Bug 27550:Problema con cierre de caja banca movil
// 26-JUN-2017 EOR    Bug 28393: WIGOS-3185 Cash net is negative after withdraw partial redeem in cashier
// 06-JUL-2017 JML    PBI 28422: WIGOS-2702 Rounding - Consume the remaining amount
// 25-JUN-2018 FOS    Bug 33297:WIGOS-13059 Gambling tables: it is allowed to reopen a gambling table with integrated cashier (fixed banking) when the collection movements have been finished.
// 06-JUL-2018 DPC    Bug 33062:[WIGOS-12955]: The monitor cash form is unbalance with kiosk redemption
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace WSI.Common
{
  public class CashierMovementsTest
  {
    public void Test1(CashierSessionInfo CashierSessionInfo, SqlTransaction SqlTrx)
    {
      Int64 _operation_id;
      Decimal _amount;
      Int64 _account_id;
      CashierMovementsTable _table;

      _operation_id = 10628;

      _amount = 50;
      _account_id = 2307;

      _table = new CashierMovementsTable(CashierSessionInfo);
      _table.Add(_operation_id, CASHIER_MOVEMENT.PRIZE_COUPON, _amount, _account_id, "");

      _table.Save(SqlTrx);
    }
  } // CashierMovementsTest

  // This class is used for grouping the cashier session info.
  // There is no constructor. Exists 2 static methods that returns an instance of this class:
  //   - In the class WSI.Common.CommonCashierInformation.
  //   - In the class WSI.Cashier.Cashier.
  public class CashierSessionInfo
  {
    public Int64 CashierSessionId;
    public Int32 TerminalId;
    public Int32 UserId;
    public String TerminalName;
    public String UserName;
    public Int32 AuthorizedByUserId;
    public String AuthorizedByUserName;
    public Boolean IsMobileBank;
    public DateTime GamingDay;

    public GU_USER_TYPE UserType;

    public CashierSessionInfo Copy()
    {
      return (CashierSessionInfo)this.MemberwiseClone();
    } // Copy

  } // CashierSessionInfo

  public class CashierMovementsTable
  {
    private DataTable m_table = null;
    private DataRow m_row = null;

    private CashierSessionInfo m_cashier_session_info = null;

    private Int64 m_movement_id;

    public CashierSessionInfo CashierSessionInfo
    {
      get { return m_cashier_session_info; }
    }

    public DataTable DataTableToSave
    {
      set { m_table = value; }
    }


    public void InitSchema()
    {
      m_table = new DataTable();
      m_table.Columns.Add("CM_OPERATION_ID", Type.GetType("System.Int64"));
      m_table.Columns.Add("CM_ACCOUNT_ID", Type.GetType("System.Int64"));
      m_table.Columns.Add("CM_CARD_TRACK_DATA", Type.GetType("System.String"));

      m_table.Columns.Add("CM_TYPE", Type.GetType("System.Int32")).AllowDBNull = false;
      m_table.Columns.Add("CM_INITIAL_BALANCE", Type.GetType("System.Decimal")).AllowDBNull = false;
      m_table.Columns.Add("CM_ADD_AMOUNT", Type.GetType("System.Decimal")).AllowDBNull = false;
      m_table.Columns.Add("CM_SUB_AMOUNT", Type.GetType("System.Decimal")).AllowDBNull = false;
      m_table.Columns.Add("CM_FINAL_BALANCE", Type.GetType("System.Decimal")).AllowDBNull = false;
      m_table.Columns.Add("CM_BALANCE_INCREMENT", Type.GetType("System.Decimal")).AllowDBNull = false;

      m_table.Columns.Add("CM_DETAILS", Type.GetType("System.String")).AllowDBNull = true;

      m_table.Columns.Add("CM_CURRENCY_ISO_CODE", Type.GetType("System.String")).AllowDBNull = true;
      m_table.Columns.Add("CM_AUX_AMOUNT", Type.GetType("System.Decimal")).AllowDBNull = true;
      m_table.Columns["CM_AUX_AMOUNT"].DefaultValue = 0;

      m_table.Columns.Add("CM_CURRENCY_DENOMINATION", Type.GetType("System.Decimal")).AllowDBNull = true;
      m_table.Columns["CM_CURRENCY_DENOMINATION"].DefaultValue = 0;
      m_table.Columns.Add("CM_GAMING_TABLE_SESSION_ID", Type.GetType("System.Int64")).AllowDBNull = true;
      m_table.Columns.Add("CM_CHIP_ID", Type.GetType("System.Int64")).AllowDBNull = true;
      m_table.Columns.Add("CM_CAGE_CURRENCY_TYPE", Type.GetType("System.Int32")).AllowDBNull = true;
      m_table.Columns.Add("CURRENCY_TYPE", Type.GetType("System.Int32")).AllowDBNull = true;
      m_table.Columns.Add("CM_RELATED_ID", Type.GetType("System.Int64")).AllowDBNull = true;

      m_table.Columns.Add("CM_DATE", Type.GetType("System.DateTime")).AllowDBNull = true;

    } // InitSchema

    public CashierMovementsTable(CashierSessionInfo CashierSessionInfo)
    {
      InitSchema();

      m_cashier_session_info = CashierSessionInfo.Copy();

    } // CashierMovementsTable

    public CashierMovementsTable(CashierSessionInfo CashierSessionInfo, String TerminalName)
      : this(CashierSessionInfo)
    {
      if (!String.IsNullOrEmpty(TerminalName))
      {
        m_cashier_session_info.TerminalName = TerminalName;
      }

    } // CashierMovementsTable

    public Boolean Add(Int64 OperationId, CASHIER_MOVEMENT MovementType,
                   Decimal Amount, Int64 AccountId, String CardTrackData)
    {
      return Add(OperationId, MovementType, Amount, AccountId, CardTrackData, "");
    }

    public Boolean Add(Int64 OperationId, CASHIER_MOVEMENT MovementType,
                       Decimal Amount, Int64 AccountId, String CardTrackData, String MovDetails)
    {
      return Add(OperationId, MovementType, Amount, AccountId, CardTrackData, MovDetails, "");
    }

    public Boolean Add(Int64 OperationId, CASHIER_MOVEMENT MovementType,
                     Decimal Amount, Int64 AccountId, String CardTrackData, String PromoName, bool IsSwap)
    {
      return Add(OperationId, MovementType, Amount, AccountId, CardTrackData, PromoName, "", IsSwap);
    }

    public Boolean Add(Int64 OperationId, CASHIER_MOVEMENT MovementType,
                       Decimal Amount, Int64 AccountId, String CardTrackData, String MovDetails, String IsoCode)
    {
      return Add(OperationId, MovementType, Amount, AccountId, CardTrackData, MovDetails, IsoCode, 0);
    }

    public Boolean Add(Int64 OperationId, CASHIER_MOVEMENT MovementType,
                      Decimal Amount, Int64 AccountId, String CardTrackData, String PromoName, String IsoCode, bool IsSwap)
    {
      return Add(OperationId, MovementType, Amount, AccountId, CardTrackData, PromoName, IsoCode, 0, IsSwap);
    }

    public Boolean Add(Int64 OperationId, CASHIER_MOVEMENT MovementType,
                     Decimal Amount, Int64 AccountId, String CardTrackData, String PromoName, String IsoCode, Decimal CurrencyDenomination, bool IsSwap)
    {
      return Add(OperationId, MovementType, Amount, AccountId, CardTrackData, PromoName, IsoCode, CurrencyDenomination, 0, 0, IsSwap);
    }

    public Boolean Add(Int64 OperationId, CASHIER_MOVEMENT MovementType,
                       Decimal Amount, Int64 AccountId, String CardTrackData, String MovDetails, String IsoCode, Decimal CurrencyDenomination)
    {
      return Add(OperationId, MovementType, Amount, AccountId, CardTrackData, MovDetails, IsoCode, CurrencyDenomination, 0, 0);
    }

    public Boolean Add(Int64 OperationId, CASHIER_MOVEMENT MovementType,
                       Decimal Amount, Int64 AccountId, String CardTrackData, String MovDetails, String IsoCode, Decimal CurrencyDenomination,
                       Int64 MovementId, Int64 GamingTableSessionId)
    {
      return Add(OperationId, MovementType, Amount, AccountId, CardTrackData, MovDetails, IsoCode, CurrencyDenomination, MovementId, GamingTableSessionId, -1);
    }

    public Boolean Add(Int64 OperationId, CASHIER_MOVEMENT MovementType,
                      Decimal Amount, Int64 AccountId, String CardTrackData, String PromoName, String IsoCode, Decimal CurrencyDenomination,
                      Int64 MovementId, Int64 GamingTableSessionId, bool IsSwap)
    {
      return Add(OperationId, MovementType, Amount, AccountId, CardTrackData, PromoName, IsoCode, CurrencyDenomination, MovementId, GamingTableSessionId, -1, IsSwap);
    }

    public Boolean Add(Int64 OperationId, CASHIER_MOVEMENT MovementType,
                       Decimal Amount, Int64 AccountId, String CardTrackData, String MovDetails, String IsoCode, Decimal CurrencyDenomination,
                       Int64 MovementId, Int64 GamingTableSessionId, Decimal AuxAmount)
    {
      return Add(OperationId, MovementType, Amount, AccountId, CardTrackData, MovDetails, IsoCode, CurrencyDenomination, MovementId, GamingTableSessionId,
                 AuxAmount, CurrencyExchangeType.CURRENCY, CageCurrencyType.Bill, null, false);
    } // Add

    public Boolean Add(Int64 OperationId, CASHIER_MOVEMENT MovementType,
                      Decimal Amount, Int64 AccountId, String CardTrackData, String PromoName, String IsoCode, Decimal CurrencyDenomination,
                      Int64 MovementId, Int64 GamingTableSessionId, Decimal AuxAmount, bool IsSwap)
    {
      return Add(OperationId, MovementType, Amount, AccountId, CardTrackData, PromoName, IsoCode, CurrencyDenomination, MovementId, GamingTableSessionId,
                 AuxAmount, CurrencyExchangeType.CURRENCY, CageCurrencyType.Bill, null, IsSwap);
    } // Add

    public Boolean Add(Int64 OperationId,
                       CASHIER_MOVEMENT MovementType,
                       Decimal Amount,
                       Int64 AccountId,
                       String CardTrackData,
                       String MovDetails,
                       String IsoCode,
                       Decimal CurrencyDenomination,
                       Int64 MovementId,
                       Int64 GamingTableSessionId,
                       Decimal AuxAmount,
                       CurrencyExchangeType? CurrExchangeType,
                       CageCurrencyType? CurrencyType,
                       Int64? ChipId,
                       bool IsSwap = false)
    {
      Decimal _balance_increment;
      Decimal _add_amount;
      Decimal _sub_amount;

      try
      {
        m_movement_id = MovementId;

        GetCashierMovementAddSubAmount(MovementType, Amount, out _add_amount, out _sub_amount, ref CardTrackData, IsSwap);
        _balance_increment = GetCashierMovementBalanceIncrement(MovementType, _add_amount, _sub_amount, (CashierSessionInfo.UserType == GU_USER_TYPE.SYS_REDEMPTION));

        m_row = m_table.NewRow();

        m_row["CM_OPERATION_ID"] = OperationId;

        m_row["CM_TYPE"] = MovementType;
        m_row["CM_INITIAL_BALANCE"] = 0;
        m_row["CM_ADD_AMOUNT"] = _add_amount;
        m_row["CM_SUB_AMOUNT"] = _sub_amount;
        m_row["CM_FINAL_BALANCE"] = 0;
        m_row["CM_BALANCE_INCREMENT"] = _balance_increment;
        m_row["CM_CURRENCY_DENOMINATION"] = CurrencyDenomination;
        m_row["CM_GAMING_TABLE_SESSION_ID"] = GamingTableSessionId;

        if (String.IsNullOrEmpty(MovDetails))
        {
          m_row["CM_DETAILS"] = DBNull.Value;
        }
        else
        {
          m_row["CM_DETAILS"] = MovDetails;
        }

        if (!String.IsNullOrEmpty(CardTrackData))
        {
          m_row["CM_CARD_TRACK_DATA"] = CardTrackData;
        }
        else
        {
          m_row["CM_CARD_TRACK_DATA"] = DBNull.Value;
        }

        if (AccountId != 0)
        {
          m_row["CM_ACCOUNT_ID"] = AccountId;
        }
        else
        {
          m_row["CM_ACCOUNT_ID"] = DBNull.Value;
        }

        if (IsoCode != "")
        {
          m_row["CM_CURRENCY_ISO_CODE"] = IsoCode;
        }
        else
        {
          m_row["CM_CURRENCY_ISO_CODE"] = DBNull.Value;
        }

        if (AuxAmount >= 0)
        {
          m_row["CM_AUX_AMOUNT"] = AuxAmount;
        }
        else
        {
          m_row["CM_AUX_AMOUNT"] = DBNull.Value;
        }

        if (CurrencyType != null)
        {
          m_row["CM_CAGE_CURRENCY_TYPE"] = CurrencyType;
        }
        else
        {
          m_row["CM_CAGE_CURRENCY_TYPE"] = DBNull.Value;
        }

        if (ChipId != null)
        {
          m_row["CM_CHIP_ID"] = ChipId;
        }
        else
        {
          m_row["CM_CHIP_ID"] = DBNull.Value;
        }

        if (CurrExchangeType != null)
        {
          m_row["CURRENCY_TYPE"] = CurrExchangeType;
        }
        else
        {
          m_row["CURRENCY_TYPE"] = DBNull.Value;
        }

        m_row["CM_DATE"] = DBNull.Value;

        m_table.Rows.Add(m_row);

        return true;
      }
      catch
      { ; }

      return false;
    } // Add

    public Boolean AddExchange(Int64 OperationId, Int64 AccountId,
                               String CardTrackData, CurrencyExchangeResult ExchangeResult)
    {
      return AddExchange(OperationId, AccountId, CardTrackData, ExchangeResult, 0);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Add Currency Exchange operation
    // 
    //  PARAMS:
    //      - INPUT:
    //          OperationId
    //          AccountId
    //          CardTrackData
    //          ExchangeResult
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    public Boolean AddExchange(Int64 OperationId, Int64 AccountId,
                               String CardTrackData, CurrencyExchangeResult ExchangeResult, Decimal CurrencyDenomination)
    {
      CASHIER_MOVEMENT _mov_split1;
      CASHIER_MOVEMENT _mov_split2;
      Boolean _commission_to_company_b;

      _commission_to_company_b = false;

#if !SQL_BUSINESS_LOGIC
      _commission_to_company_b = Misc.IsCommissionToCompanyBEnabled();
#endif

      // For a recharge of 100.00 USD (change 1.00 USD = 13.00 MXN), (80% / 20%):
      //   InAmount = 100.00
      //   Comission = 21.00
      //   GrossAmount = 1,300.00
      //   NetAmountSplit1 = 1,040.00
      //   NetAmountSplit2 = 260.00
      //   NR2Amount = 21.00 

      switch (ExchangeResult.InType)
      {
        case CurrencyExchangeType.CARD:
          // RMS 08-AUG-2014
          switch (ExchangeResult.SubType)
          {
            case CurrencyExchangeSubType.CREDIT_CARD:
              if (!_commission_to_company_b)
              {
                _mov_split1 = CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1;
                _mov_split2 = CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2;
              }
              else
              {
                _mov_split1 = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1;
                _mov_split2 = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2;
              }
              break;
            case CurrencyExchangeSubType.DEBIT_CARD:
              if (!_commission_to_company_b)
              {
                _mov_split1 = CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1;
                _mov_split2 = CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2;
              }
              else
              {
                _mov_split1 = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1;
                _mov_split2 = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2;
              }
              break;
            default:
              if (!_commission_to_company_b)
              {
                _mov_split1 = CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1;
                _mov_split2 = CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2;
              }
              else
              {
                _mov_split1 = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1;
                _mov_split2 = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT2;
              }
              break;
          }
          break;
        case CurrencyExchangeType.CURRENCY:
          if (!_commission_to_company_b)
          {
            _mov_split1 = CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1;
            _mov_split2 = CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT2;
          }
          else
          {
            _mov_split1 = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT1;
            _mov_split2 = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT2;
          }
          break;
        case CurrencyExchangeType.CHECK:
          if (!_commission_to_company_b)
          {
            _mov_split1 = CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1;
            _mov_split2 = CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT2;
          }
          else
          {
            _mov_split1 = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1;
            _mov_split2 = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT2;
          }
          break;
        default:
          return false;
      }

      // Movement for Part A:
      //   InAmount = 100.00
      //   Comission = 21.00
      //   GrossAmount = 1,300.00
      //   NetAmountSplit1 = 1,040.00
      //   NetAmountSplit2 = 0.00
      //   NR2Amount = 21.00
      //
      if (!AddExchange_Internal(OperationId, AccountId, CardTrackData, ExchangeResult, _mov_split1, CurrencyDenomination))
      {
        return false;
      }

      // Movement for Part B:
      //   InAmount = 100.00
      //   Comission = 0.00
      //   GrossAmount = 1,300.00
      //   NetAmountSplit1 = 0.00
      //   NetAmountSplit2 = 260.00
      //   NR2Amount = 0.00
      //
      if (!AddExchange_Internal(OperationId, AccountId, CardTrackData, ExchangeResult, _mov_split2, CurrencyDenomination))
      {
        return false;
      }

      return true;
    }//AddExchange

    public Boolean AddExchangeCreditLine(Int64 OperationId, Int64 AccountId,
                              String CardTrackData, CurrencyExchangeResult ExchangeResult, Decimal CurrencyDenomination)
    {
      CASHIER_MOVEMENT _mov_split1;
      CASHIER_MOVEMENT _mov_split2;
      Boolean _commission_to_company_b;

      _commission_to_company_b = false;

#if !SQL_BUSINESS_LOGIC
      _commission_to_company_b = Misc.IsCommissionToCompanyBEnabled();
#endif

      // For a recharge of 100.00 USD (change 1.00 USD = 13.00 MXN), (80% / 20%):
      //   InAmount = 100.00
      //   Comission = 21.00
      //   GrossAmount = 1,300.00
      //   NetAmountSplit1 = 1,040.00
      //   NetAmountSplit2 = 260.00
      //   NR2Amount = 21.00 
      _mov_split1 = CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CREDIT_CARD_EXCHANGE;
      _mov_split2 = CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT2;

      switch (ExchangeResult.InType)
      {
        case CurrencyExchangeType.CARD:
          // RMS 08-AUG-2014
          switch (ExchangeResult.SubType)
          {
            case CurrencyExchangeSubType.CREDIT_CARD:
              _mov_split1 = CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CREDIT_CARD_EXCHANGE;
              break;
            case CurrencyExchangeSubType.DEBIT_CARD:
              _mov_split1 = CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_DEBIT_CARD_EXCHANGE;
              break;
            default:
              _mov_split1 = CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CARD_EXCHANGE;
              break;
          }
          break;
        case CurrencyExchangeType.CURRENCY:
          if (!_commission_to_company_b)
          {
            _mov_split1 = CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1;
            _mov_split2 = CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT2;
          }
          else
          {
            _mov_split1 = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT1;
            _mov_split2 = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT2;
          }
          break;
        case CurrencyExchangeType.CHECK:
          _mov_split1 = CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CHECK_EXCHANGE;
          break;
        default:
          return false;
      }

      // Movement for Part A:
      //   InAmount = 100.00
      //   Comission = 21.00
      //   GrossAmount = 1,300.00
      //   NetAmountSplit1 = 1,040.00
      //   NetAmountSplit2 = 0.00
      //   NR2Amount = 21.00
      //
      if (!AddExchange_Internal(OperationId, AccountId, CardTrackData, ExchangeResult, _mov_split1, CurrencyDenomination))
      {
        return false;
      }

      if (ExchangeResult.InType == CurrencyExchangeType.CURRENCY)
      {
      if (!AddExchange_Internal(OperationId, AccountId, CardTrackData, ExchangeResult, _mov_split2, CurrencyDenomination))
      {
        return false;
      }
      }

      return true;
    }//AddExchange

    //------------------------------------------------------------------------------
    // PURPOSE: Add Currency Exchange operation for MultiDenominations 
    // 
    //  PARAMS:
    //      - INPUT:
    //          OperationId
    //          AccountId
    //          CardTrackData
    //          ExchangeResult
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    public Boolean AddCurrencyExchange(Int64 OperationId, Int64 AccountId, String CardTrackData, CurrencyExchangeResult ExchangeResult, String NationalCurrency)
    {
      CASHIER_MOVEMENT _cashier_mov;

      if (ExchangeResult.InType != CurrencyExchangeType.CURRENCY)
        return false;

      if (ExchangeResult.InCurrencyCode == NationalCurrency && ExchangeResult.OutCurrencyCode != NationalCurrency)
        _cashier_mov = CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_FOREIGN_CURRENCY;
      else if (ExchangeResult.InCurrencyCode != NationalCurrency && ExchangeResult.OutCurrencyCode == NationalCurrency)
        _cashier_mov = CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_NATIONAL_CURRENCY;
      else
        return false;

      if (!AddExchange_Internal(OperationId, AccountId, CardTrackData, ExchangeResult, _cashier_mov, 0))
      {
        return false;
      }

      return true;
    }//AddCurrencyExchange


    //------------------------------------------------------------------------------
    // PURPOSE: Add Currency Exchange cash advance
    // 
    //  PARAMS:
    //      - INPUT:
    //          OperationId
    //          AccountId
    //          CardTrackData
    //          ExchangeResult
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    public Boolean AddCashAdvanceExchange(Int64 OperationId, Int64 AccountId,
      String CardTrackData, CurrencyExchangeResult ExchangeResult)
    {
      CASHIER_MOVEMENT _cashier_movement;
      Boolean _commission_to_company_b;

      _commission_to_company_b = false;

#if !SQL_BUSINESS_LOGIC
      _commission_to_company_b = Misc.IsCommissionToCompanyBEnabled();
#endif

      switch (ExchangeResult.InType)
      {
        case CurrencyExchangeType.CURRENCY:
          if (!_commission_to_company_b)
          {
            _cashier_movement = CASHIER_MOVEMENT.CASH_ADVANCE_CURRENCY_EXCHANGE;
          }
          else
          {
            _cashier_movement = CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CURRENCY_EXCHANGE;
          }
          break;
        case CurrencyExchangeType.CARD:
          switch (ExchangeResult.SubType)
          {
            case CurrencyExchangeSubType.NONE:
            case CurrencyExchangeSubType.BANK_CARD:
              if (!_commission_to_company_b)
              {
                _cashier_movement = CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD;
              }
              else
              {
                _cashier_movement = CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_GENERIC_BANK_CARD;
              }
              break;
            case CurrencyExchangeSubType.DEBIT_CARD:
              if (!_commission_to_company_b)
              {
                _cashier_movement = CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD;
              }
              else
              {
                _cashier_movement = CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_DEBIT_BANK_CARD;
              }
              break;
            case CurrencyExchangeSubType.CREDIT_CARD:
              if (!_commission_to_company_b)
              {
                _cashier_movement = CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD;
              }
              else
              {
                _cashier_movement = CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CREDIT_BANK_CARD;
              }
              break;
            default:
              return false;
          }
          break;

        case CurrencyExchangeType.CHECK:
          if (!_commission_to_company_b)
          {
            _cashier_movement = CASHIER_MOVEMENT.CASH_ADVANCE_CHECK;
          }
          else
          {
            _cashier_movement = CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CHECK;
          }
          break;
        default:
          return false;
      }

      if (!AddExchange_Internal(OperationId, AccountId, CardTrackData, ExchangeResult, _cashier_movement, 0))
      {
        return false;
      }

      return true;
    }//AddCashAdvanceExchange


    private Boolean AddExchange_Internal(Int64 OperationId, Int64 AccountId,
                                         String CardTrackData, CurrencyExchangeResult ExchangeResult, CASHIER_MOVEMENT Movement, Decimal CurrencyDenomination)
    {
      m_row = m_table.NewRow();

      m_row["CM_OPERATION_ID"] = OperationId;
      m_row["CM_TYPE"] = Movement;

      if (AccountId != 0)
      {
        m_row["CM_ACCOUNT_ID"] = AccountId;
      }
      else
      {
        m_row["CM_ACCOUNT_ID"] = DBNull.Value;
      }

      if (!String.IsNullOrEmpty(CardTrackData))
      {
        m_row["CM_CARD_TRACK_DATA"] = CardTrackData;
      }
      else
      {
        m_row["CM_CARD_TRACK_DATA"] = DBNull.Value;
      }

      m_row["CM_DETAILS"] = DBNull.Value;
      m_row["CM_BALANCE_INCREMENT"] = 0;

      m_row["CM_INITIAL_BALANCE"] = ExchangeResult.InAmount;
      m_row["CM_ADD_AMOUNT"] = ExchangeResult.GrossAmount;
      m_row["CM_CURRENCY_ISO_CODE"] = ExchangeResult.InCurrencyCode;

      // RMS 08-AUG-2014
      if (Movement == CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1
       || Movement == CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1
       || Movement == CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1
       || Movement == CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1
       || Movement == CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1
       || Movement == CASHIER_MOVEMENT.CASH_ADVANCE_CURRENCY_EXCHANGE
       || Movement == CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD
       || Movement == CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD
       || Movement == CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD
       || Movement == CASHIER_MOVEMENT.CASH_ADVANCE_CHECK
       || Movement == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1
       || Movement == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1
       || Movement == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1
       || Movement == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT1
       || Movement == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1
       || Movement == CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CURRENCY_EXCHANGE
       || Movement == CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_GENERIC_BANK_CARD
       || Movement == CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CREDIT_BANK_CARD
       || Movement == CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_DEBIT_BANK_CARD
       || Movement == CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CHECK 
       || Movement == CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CARD_EXCHANGE
       || Movement == CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CREDIT_CARD_EXCHANGE
       || Movement == CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_DEBIT_CARD_EXCHANGE
       || Movement == CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CHECK_EXCHANGE)
      {
        m_row["CM_SUB_AMOUNT"] = ExchangeResult.Comission;
        m_row["CM_FINAL_BALANCE"] = ExchangeResult.NetAmountSplit1;
        m_row["CM_AUX_AMOUNT"] = ExchangeResult.NR2Amount;

        if (Movement == CASHIER_MOVEMENT.CASH_ADVANCE_CURRENCY_EXCHANGE
       || Movement == CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD
       || Movement == CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD
       || Movement == CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD
       || Movement == CASHIER_MOVEMENT.CASH_ADVANCE_CHECK
       || Movement == CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CURRENCY_EXCHANGE
       || Movement == CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_GENERIC_BANK_CARD
       || Movement == CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CREDIT_BANK_CARD
       || Movement == CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_DEBIT_BANK_CARD
       || Movement == CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CHECK
       || Movement == CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CARD_EXCHANGE
       || Movement == CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CREDIT_CARD_EXCHANGE
       || Movement == CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_DEBIT_CARD_EXCHANGE
       || Movement == CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CHECK_EXCHANGE)
        {
          m_row["CM_FINAL_BALANCE"] = ExchangeResult.NetAmount;
        }
      }
      else if (Movement == CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_NATIONAL_CURRENCY)
      {
        // FAV 03-MAY-2016  Currency exchange
        m_row["CM_SUB_AMOUNT"] = ExchangeResult.Comission;
        m_row["CM_FINAL_BALANCE"] = ExchangeResult.NetAmount;
        m_row["CM_AUX_AMOUNT"] = ExchangeResult.NR2Amount;
      }
      else if (Movement == CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_FOREIGN_CURRENCY)
      {
        Decimal _amount_changed;
        _amount_changed = ExchangeResult.InAmount - ExchangeResult.NetAmountDevolution;

        // FAV 03-MAY-2016  Currency exchange devolution (Inverts the values)
        m_row["CM_INITIAL_BALANCE"] = ExchangeResult.NetAmount;
        m_row["CM_SUB_AMOUNT"] = ExchangeResult.Comission;
        m_row["CM_ADD_AMOUNT"] = _amount_changed;  // without comission
        m_row["CM_FINAL_BALANCE"] = _amount_changed;

        m_row["CM_AUX_AMOUNT"] = ExchangeResult.NR2Amount;
        m_row["CM_CURRENCY_ISO_CODE"] = ExchangeResult.OutCurrencyCode;
      }
      else
      {
        m_row["CM_SUB_AMOUNT"] = 0;
        m_row["CM_FINAL_BALANCE"] = ExchangeResult.NetAmountSplit2;
        m_row["CM_AUX_AMOUNT"] = 0;
      }

      m_row["CM_CURRENCY_DENOMINATION"] = CurrencyDenomination;

      m_row["CM_GAMING_TABLE_SESSION_ID"] = 0;

      m_row["CURRENCY_TYPE"] = ExchangeResult.InType;

      m_table.Rows.Add(m_row);

      return true;
    }//AddExchange_Internal

    public void AddRelatedIdInLastMov(Int64 RelatedId)
    {
      if (m_row != null)
      {
        m_row["CM_RELATED_ID"] = RelatedId;
      }
    }

    public void AddSubAmountInLastMov(Decimal SubAmount)
    {
      if (m_row != null)
      {
        m_row["CM_SUB_AMOUNT"] = SubAmount;
      }
    }

    public void SetAddAmountInLastMov(Decimal AddAmount)
    {
      if (m_row != null)
      {
        m_row["CM_ADD_AMOUNT"] = AddAmount;
      }
    }

    public void SetInitialBalanceInLastMov(Decimal AddAmount)
    {
      if (m_row != null)
      {
        m_row["CM_INITIAL_BALANCE"] = AddAmount;
      }
    }

    public void SetDateInLastMov(DateTime DateTime)
    {
      if (m_row != null)
      {
        m_row["CM_DATE"] = DateTime;
      }
    }

    public void SetInitialAndFinalBalanceAmountInLastMov(Decimal InitialAmount, Decimal FinalAmount)
    {
      if (m_row != null)
      {
        m_row["CM_INITIAL_BALANCE"] = InitialAmount;
        m_row["CM_FINAL_BALANCE"] = FinalAmount;
      }
    }

    public Int32 RowsCount()
    {
      Int32 _rows;
      _rows = -1;
      if (m_row != null)
      {
        _rows = m_row.ItemArray.Length;
      }

      return _rows;
    }

    public Boolean Save(SqlTransaction Trx, Boolean IsReOpen = false)
    {
      StringBuilder _sb;
      // RMS11111
      // StringBuilder _sb_chips;
      Int32 _num_rows_inserted;
      SqlParameter _param_iso_code;
      SqlParameter _param_currency_type;
      SqlParameter _param_increment;
      SqlParameter _param_currency_increment;
      SqlParameter _param_in_cashier_sessions;
      SqlParameter _param_in_cashier_sessions_by_currency;
      String _national_currency;
      CASHIER_MOVEMENT _movement;
      Int64 _first_account_id;
      String _track_data;
      String _cashier_mode_str;
      Boolean _cashier_mode;
      Boolean _has_not_errors;

      _has_not_errors = true;

      try
      {
        if (m_table.Rows.Count == 0)
        {
          return true;
        }

        _first_account_id = 0;
        _track_data = "";
        foreach (DataRow _row in m_table.Rows)
        {
          if (_row.IsNull("CM_ACCOUNT_ID"))
          {
            continue;
          }

          if (!_row.IsNull("CM_CARD_TRACK_DATA"))
          {
            continue;
          }

          if (_first_account_id == 0)
          {
            _first_account_id = (Int64)_row["CM_ACCOUNT_ID"];
            _track_data = AccountMovementsTable.ExternalTrackData(_first_account_id, Trx);
            _row["CM_CARD_TRACK_DATA"] = _track_data;
          }
          else
          {
            if (_first_account_id == (Int64)_row["CM_ACCOUNT_ID"])
            {
              _row["CM_CARD_TRACK_DATA"] = _track_data;
            }
          }
        }
        #region UpdateBalance

        UpdateBalance(Trx,
                      IsReOpen,
                      out _sb,
                      out _param_iso_code,
                      out _param_currency_type,
                      out _param_increment,
                      out _param_currency_increment,
                      out _param_in_cashier_sessions,
                      out _param_in_cashier_sessions_by_currency,
                      out _national_currency,
                      out _movement,
                      out _cashier_mode_str,
                      out _cashier_mode,
                      out _has_not_errors);
        if (!_has_not_errors)
        {
          return false;
        }

        #endregion UpdateBalance

        // Insert the movement
        _sb.Length = 0;
        _sb.AppendLine(" DECLARE   @_cm_movement_id AS BIGINT");
        _sb.AppendLine(" DECLARE   @_trackdata AS NVARCHAR(50) ");
        _sb.AppendLine(" SET       @_trackdata = '' ");
        _sb.AppendLine(" SET       @pCageCurrenciesType = ISNULL(@pCageCurrenciesType, 0) ");
        _sb.AppendLine(" IF @pCardData IS NULL AND @pAccountId IS NOT NULL ");
        _sb.AppendLine("   SELECT  @_trackdata = dbo.TrackDataToExternal(AC_TRACK_DATA) FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId ");
        _sb.AppendLine(" IF @_trackdata = '' ");
        _sb.AppendLine("   SET     @_trackdata = @pCardData ");

        _sb.AppendLine(" INSERT INTO   CASHIER_MOVEMENTS           ");
        _sb.AppendLine("             ( CM_SESSION_ID               ");
        _sb.AppendLine("             , CM_CASHIER_ID               ");
        _sb.AppendLine("             , CM_USER_ID                  ");
        _sb.AppendLine("             , CM_TYPE                     ");
        _sb.AppendLine("             , CM_INITIAL_BALANCE          ");
        _sb.AppendLine("             , CM_ADD_AMOUNT               ");
        _sb.AppendLine("             , CM_SUB_AMOUNT               ");
        _sb.AppendLine("             , CM_FINAL_BALANCE            ");
        _sb.AppendLine("             , CM_USER_NAME                ");
        _sb.AppendLine("             , CM_CASHIER_NAME             ");
        _sb.AppendLine("             , CM_CARD_TRACK_DATA          ");
        _sb.AppendLine("             , CM_ACCOUNT_ID               ");
        _sb.AppendLine("             , CM_OPERATION_ID             ");
        _sb.AppendLine("             , CM_DETAILS                  ");
        _sb.AppendLine("             , CM_CURRENCY_ISO_CODE        ");
        _sb.AppendLine("             , CM_AUX_AMOUNT               ");
        _sb.AppendLine("             , CM_CURRENCY_DENOMINATION    ");
        _sb.AppendLine("             , CM_GAMING_TABLE_SESSION_ID  ");
        _sb.AppendLine("             , CM_CHIP_ID                  ");
        _sb.AppendLine("             , CM_CAGE_CURRENCY_TYPE       ");
        _sb.AppendLine("             , CM_RELATED_ID               ");
        _sb.AppendLine("             , CM_DATE                     ");
        _sb.AppendLine("             )                             ");
        _sb.AppendLine("   VALUES    ( @pSessionId                 ");
        _sb.AppendLine("             , @pCashierId                 ");
        _sb.AppendLine("             , @pUserId                    ");
        _sb.AppendLine("             , @pType                      ");
        _sb.AppendLine("             , @pInitialBalance            ");
        _sb.AppendLine("             , @pAddAmount                 ");
        _sb.AppendLine("             , @pSubAmount                 ");
        _sb.AppendLine("             , @pFinalBalance              ");
        _sb.AppendLine("             , @pUserName                  ");
        _sb.AppendLine("             , @pCashierName               ");
        _sb.AppendLine("             , @_trackdata                 ");
        _sb.AppendLine("             , @pAccountId                 ");
        _sb.AppendLine("             , @pOperationId               ");
        _sb.AppendLine("             , @pDetails                   ");
        _sb.AppendLine("             , @pCurrencyCode              ");
        _sb.AppendLine("             , @pAuxAmount                 ");
        _sb.AppendLine("             , @pCurrencyDenomination      ");
        _sb.AppendLine("             , @pGamingTableSessionId      ");
        _sb.AppendLine("             , @pChipId                    ");
        _sb.AppendLine("             , @pCageCurrenciesType        ");
        _sb.AppendLine("             , @pRelatedId                 ");
        _sb.AppendLine("             , isNull(@pDate, GETDATE())   ");
        _sb.AppendLine("             )                             ");
        _sb.AppendLine(" SET   @_cm_movement_id = SCOPE_IDENTITY() ");

        _sb.AppendLine("IF @pType >= @pCageFirstValue AND @pType <= @pCageLastValue AND @pExternMovementId > 0");
        _sb.AppendLine("BEGIN");
        _sb.AppendLine("  INSERT INTO   CAGE_CASHIER_MOVEMENT_RELATION ");
        _sb.AppendLine("       VALUES ( ");
        _sb.AppendLine("                @pExternMovementId");
        _sb.AppendLine("              , @_cm_movement_id");
        _sb.AppendLine("              ) ");
        _sb.AppendLine("END");
        _sb.AppendLine(" BEGIN TRY");

        _sb.AppendLine(" IF @pCurrencyCode IS NULL                     ");
        _sb.AppendLine("   SET @pCurrencyCode = @pNationalCurrencyCode ");

        _sb.AppendLine("      EXEC dbo.CashierMovementsHistory @pSessionId, @_cm_movement_id, @pType, @pSubType, ");
        _sb.AppendLine("                                       @pInitialBalance, @pAddAmount, @pSubAmount, @pFinalBalance, ");
        _sb.AppendLine("                                       @pCurrencyCode, @pAuxAmount, @pCurrencyDenomination, @pCageCurrenciesType");

        _sb.AppendLine(" END TRY ");
        _sb.AppendLine(" BEGIN CATCH ");
        _sb.AppendLine("    DECLARE   @ErrorMessage NVARCHAR(4000); ");
        _sb.AppendLine("    DECLARE   @ErrorSeverity INT; ");
        _sb.AppendLine("    DECLARE   @ErrorState INT; ");
        _sb.AppendLine("    SELECT  ");
        _sb.AppendLine("        @ErrorMessage = ERROR_MESSAGE(), ");
        _sb.AppendLine("        @ErrorSeverity = ERROR_SEVERITY(), ");
        _sb.AppendLine("        @ErrorState = ERROR_STATE(); ");
        _sb.AppendLine("    RAISERROR (@ErrorMessage, ");
        _sb.AppendLine("               @ErrorSeverity, ");
        _sb.AppendLine("               @ErrorState);");
        _sb.AppendLine(" END CATCH");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).SourceColumn = "CM_OPERATION_ID";
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).SourceColumn = "CM_ACCOUNT_ID";
          _sql_cmd.Parameters.Add("@pCardData", SqlDbType.NVarChar, 50).SourceColumn = "CM_CARD_TRACK_DATA";

          _sql_cmd.Parameters.Add("@pType", SqlDbType.Int).SourceColumn = "CM_TYPE";
          _sql_cmd.Parameters.Add("@pInitialBalance", SqlDbType.Money).SourceColumn = "CM_INITIAL_BALANCE";
          _sql_cmd.Parameters.Add("@pAddAmount", SqlDbType.Money).SourceColumn = "CM_ADD_AMOUNT";
          _sql_cmd.Parameters.Add("@pSubAmount", SqlDbType.Money).SourceColumn = "CM_SUB_AMOUNT";
          _sql_cmd.Parameters.Add("@pFinalBalance", SqlDbType.Money).SourceColumn = "CM_FINAL_BALANCE";
          _sql_cmd.Parameters.Add("@pBalanceIncrement", SqlDbType.Money).SourceColumn = "CM_BALANCE_INCREMENT";
          _sql_cmd.Parameters.Add("@pDetails", SqlDbType.NVarChar, 256).SourceColumn = "CM_DETAILS";

          _sql_cmd.Parameters.Add("@pCurrencyCode", SqlDbType.NVarChar).SourceColumn = "CM_CURRENCY_ISO_CODE";
          _sql_cmd.Parameters.Add("@pAuxAmount", SqlDbType.Money).SourceColumn = "CM_AUX_AMOUNT";

          _sql_cmd.Parameters.Add("@pNationalCurrencyCode", SqlDbType.NVarChar).Value = _national_currency;

          if (m_cashier_session_info.CashierSessionId != 0)
          {
            _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = m_cashier_session_info.CashierSessionId;
          }
          else
          {
            _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).SourceColumn = "CM_SESSION_ID";
          }

          _sql_cmd.Parameters.Add("@pCashierId", SqlDbType.BigInt).Value = m_cashier_session_info.TerminalId;
          _sql_cmd.Parameters.Add("@pCashierName", SqlDbType.NVarChar, 50).Value = m_cashier_session_info.TerminalName;
          _sql_cmd.Parameters.Add("@pUserId", SqlDbType.BigInt).Value = m_cashier_session_info.AuthorizedByUserId;
          _sql_cmd.Parameters.Add("@pUserName", SqlDbType.NVarChar, 50).Value = m_cashier_session_info.AuthorizedByUserName;

          _sql_cmd.Parameters.Add("@pSubType", SqlDbType.Int).Value = 0;

          _sql_cmd.Parameters.Add("@pCurrencyDenomination", SqlDbType.Money).SourceColumn = "CM_CURRENCY_DENOMINATION";

          // 07-JAN-2014 - Gaming Tables
          _sql_cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).SourceColumn = "CM_GAMING_TABLE_SESSION_ID";

          //FBA 22-NOV-2013 - Cage
          _sql_cmd.Parameters.Add("@pCageFirstValue", SqlDbType.Int).Value = WSI.Common.CASHIER_MOVEMENT.CAGE_OPEN_SESSION;
          _sql_cmd.Parameters.Add("@pCageLastValue", SqlDbType.Int).Value = WSI.Common.CASHIER_MOVEMENT.CAGE_LAST_MOVEMENT;
          _sql_cmd.Parameters.Add("@pExternMovementId", SqlDbType.BigInt).Value = m_movement_id;

          //Dll 22-JUL-2014
          _sql_cmd.Parameters.Add("@pChipId", SqlDbType.BigInt).SourceColumn = "CM_CHIP_ID";
          _sql_cmd.Parameters.Add("@pCageCurrenciesType", SqlDbType.Int).SourceColumn = "CM_CAGE_CURRENCY_TYPE";

          //MPO 22-SEP-2014
          _sql_cmd.Parameters.Add("@pRelatedId", SqlDbType.BigInt).SourceColumn = "CM_RELATED_ID";

          //MPO 22-SEP-2014
          _sql_cmd.Parameters.Add("@pDate", SqlDbType.DateTime).SourceColumn = "CM_DATE";

          _sql_cmd.UpdatedRowSource = UpdateRowSource.None;

          using (SqlDataAdapter _sql_adap = new SqlDataAdapter())
          {
            _sql_adap.InsertCommand = _sql_cmd;

            _sql_adap.UpdateBatchSize = 500;

            _num_rows_inserted = _sql_adap.Update(m_table);

            if (_num_rows_inserted == m_table.Rows.Count)
            {
              return true;
            }
          }
        }
      }
      catch (SqlException _sql_ex)
      {
#if !SQL_BUSINESS_LOGIC
        if (_sql_ex.Number != -2) // Not connection timeout
        {
          Log.Warning(" *** Sql Exception: Number: " + _sql_ex.Number.ToString() + ". " + _sql_ex.Message.ToString() + " *** Details: \r\n" + _sql_ex.StackTrace + "\r\n");
        }
#endif

        throw _sql_ex;
      }
      catch (Exception _ex)
      {
#if !SQL_BUSINESS_LOGIC
        Log.Exception(_ex);
#endif

        throw _ex;
      }

      return false;
    } // Save

    private void UpdateBalance(SqlTransaction Trx, Boolean IsReOpen, out StringBuilder _sb, out SqlParameter _param_iso_code, out SqlParameter _param_currency_type, 
                                out SqlParameter _param_increment, out SqlParameter _param_currency_increment, out SqlParameter _param_in_cashier_sessions, 
                                out SqlParameter _param_in_cashier_sessions_by_currency, out String _national_currency, out CASHIER_MOVEMENT _movement, 
                                out String _cashier_mode_str, out Boolean _cashier_mode, out Boolean _has_not_errors)
    {
      _has_not_errors = true;
      _movement = CASHIER_MOVEMENT.NOT_ASSIGNED;

      _national_currency = MultiPromos.ReadGeneralParams("RegionalOptions", "CurrencyISOCode", Trx);

      //Update Balance
      //DLL 21-AUG-2013: Control register exists in CASHIER_SESSIONS_BY_CURRENCY
      _sb = new StringBuilder();
      _sb.AppendLine(" IF @InCashierSessions = 1  ");
      _sb.AppendLine(" BEGIN ");
      _sb.AppendLine("   UPDATE   CASHIER_SESSIONS  ");
      _sb.AppendLine("      SET   CS_BALANCE     = CS_BALANCE + (@pBalanceIncrement) ");
      _sb.AppendLine("   OUTPUT   DELETED.CS_BALANCE ");
      _sb.AppendLine("          , INSERTED.CS_BALANCE  ");
      _sb.AppendLine("    WHERE   CS_SESSION_ID  =   @pSessionId ");
      _sb.AppendLine("      AND   CS_STATUS     IN ( @pStatusOpen ");
      _sb.AppendLine("                             , @pStatusOpenPending ");
      _sb.AppendLine("                             , @pStatusPending ) ");
      _sb.AppendLine("     END ");
      _sb.AppendLine(" IF @InCashierSessionsByCurrency = 1 ");
      _sb.AppendLine(" BEGIN  ");
      _sb.AppendLine("   IF EXISTS ( SELECT   1  ");
      _sb.AppendLine("                 FROM   CASHIER_SESSIONS_BY_CURRENCY ");
      _sb.AppendLine("                WHERE   CSC_SESSION_ID = @pSessionId ");
      _sb.AppendLine("                  AND   CSC_ISO_CODE   = @pIsoCode ");
      _sb.AppendLine("                  AND   CSC_TYPE       = @pType) ");
      _sb.AppendLine("       UPDATE   CASHIER_SESSIONS_BY_CURRENCY ");
      _sb.AppendLine("          SET   CSC_BALANCE     = CSC_BALANCE + (@pBalanceCurrencyIncrement) ");
      _sb.AppendLine("       OUTPUT   DELETED.CSC_BALANCE ");
      _sb.AppendLine("              , INSERTED.CSC_BALANCE ");
      _sb.AppendLine("        WHERE   CSC_SESSION_ID         = @pSessionId ");
      _sb.AppendLine("          AND   CSC_ISO_CODE           = @pIsoCode ");
      _sb.AppendLine("          AND   CSC_TYPE               = @pType ");
      _sb.AppendLine("          AND   EXISTS ( ");
      _sb.AppendLine("                        SELECT CS_SESSION_ID ");
      _sb.AppendLine("                          FROM CASHIER_SESSIONS ");
      _sb.AppendLine("                         WHERE CS_SESSION_ID  =   @pSessionId ");
      _sb.AppendLine("                           AND CS_STATUS     IN ( @pStatusOpen ");
      _sb.AppendLine("                                                , @pStatusOpenPending ");
      _sb.AppendLine("                                                , @pStatusPending ) ");
      _sb.AppendLine("                         ) ");
      _sb.AppendLine("  ELSE ");
      _sb.AppendLine(String.Format("  IF (@pType = {0} AND @pIsoCode <> '{1}') or (@pType <> {0}) ", (Int32)CurrencyExchangeType.CURRENCY, _national_currency));
      _sb.AppendLine("     INSERT INTO   CASHIER_SESSIONS_BY_CURRENCY ");
      _sb.AppendLine("                 ( CSC_SESSION_ID ");
      _sb.AppendLine("                 , CSC_ISO_CODE ");
      _sb.AppendLine("                 , CSC_TYPE ");
      _sb.AppendLine("                 , CSC_BALANCE ");
      _sb.AppendLine("                 , CSC_COLLECTED  ");
      _sb.AppendLine("                 ) ");
      _sb.AppendLine("          OUTPUT   0.0 ");
      _sb.AppendLine("                 , INSERTED.CSC_BALANCE ");
      _sb.AppendLine("          VALUES    ");
      _sb.AppendLine("                 ( @pSessionId ");
      _sb.AppendLine("                 , @pIsoCode ");
      _sb.AppendLine("                 , @pType ");
      _sb.AppendLine("                 , @pBalanceCurrencyIncrement ");
      _sb.AppendLine("                 , 0.0 ");
      _sb.AppendLine("                 ) ");
      _sb.AppendLine("   END ");

      using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt).Value = m_cashier_session_info.CashierSessionId;
        _sql_cmd.Parameters.Add("@pStatusOpen", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;
        _sql_cmd.Parameters.Add("@pStatusOpenPending", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN_PENDING;
        _sql_cmd.Parameters.Add("@pStatusPending", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.PENDING_CLOSING;

        _param_iso_code = _sql_cmd.Parameters.Add("@pIsoCode", SqlDbType.VarChar);
        _param_currency_type = _sql_cmd.Parameters.Add("@pType", SqlDbType.Int);
        _param_increment = _sql_cmd.Parameters.Add("@pBalanceIncrement", SqlDbType.Money);
        _param_currency_increment = _sql_cmd.Parameters.Add("@pBalanceCurrencyIncrement", SqlDbType.Money);
        _param_in_cashier_sessions = _sql_cmd.Parameters.Add("@InCashierSessions", SqlDbType.Bit);
        _param_in_cashier_sessions_by_currency = _sql_cmd.Parameters.Add("@InCashierSessionsByCurrency", SqlDbType.Bit);
                
        _cashier_mode_str = MultiPromos.ReadGeneralParams("GamingTables", "Cashier.Mode", Trx);
        _cashier_mode = (_cashier_mode_str == "1");

        foreach (DataRow _row in m_table.Rows)
        {
          if (_has_not_errors)
          {
            // set default values
            _param_iso_code.Value = _row["CM_CURRENCY_ISO_CODE"];
     
       
            if (string.IsNullOrEmpty(_param_iso_code.Value.ToString()))
            {
              _param_iso_code.Value = _national_currency;
            }

            //DLL11111
            _param_currency_type.Value = _row["CURRENCY_TYPE"];
      
            //DLL11111 TODO Borrar cuando se tenga el ID en todas las pantallas
#if !SQL_BUSINESS_LOGIC
            if ((CurrencyExchangeType)_row["CURRENCY_TYPE"] == CurrencyExchangeType.CASINOCHIP)
            {
              DataTable _dt_chips;
              DataRow[] _chip_rows;

              _dt_chips = null;

              if (_dt_chips == null)
              {
                CurrencyExchange.GetAllowedChips(out _dt_chips, Trx);
              }

              _chip_rows = _dt_chips.Select("CH_DENOMINATION = '" + _row["CM_CURRENCY_DENOMINATION"] + "'");
              if (_chip_rows.Length > 0)
              {
                _row["CM_CHIP_ID"] = _chip_rows[0]["CH_CHIP_ID"];
              }
            }
#endif

            _param_in_cashier_sessions.Value = 1;
            _param_in_cashier_sessions_by_currency.Value = 0;
            _param_currency_increment.Value = 0;

            _movement = (CASHIER_MOVEMENT)_row["CM_TYPE"];

            switch (_movement)
            {
              // TODO: AJQ revise
              case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_PRIZE:
                _param_increment.Value = 0;
                break;

              case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT1:
              case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT2:
                _param_increment.Value = -(Decimal)_row["CM_ADD_AMOUNT"];
                break;

              case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT2:
              case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2:
              // RMS 08-AUG-2014
              case CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2:
              case CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2:
              case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1:
              case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT2:
              // DLL 15-DIC-2014
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT2:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2:
              // DLL 17-DIC-2014
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT1:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT2:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1:
              case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT2:
              // DHA 25-JAN-2016
              case CASHIER_MOVEMENT.TITO_TICKETS_CREATE_EXCHANGE_DUAL_CURRENCY:
              case CASHIER_MOVEMENT.TITO_TICKETS_PLAYED_EXCHANGE_DUAL_CURRENCY:
              case CASHIER_MOVEMENT.BILL_IN_EXCHANGE_DUAL_CURRENCY:
              case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CARD_EXCHANGE:
              case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CREDIT_CARD_EXCHANGE:
              case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_DEBIT_CARD_EXCHANGE:
              case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CHECK_EXCHANGE:

                _param_increment.Value = -((Decimal)_row["CM_FINAL_BALANCE"]);
                // RMS 08-AUG-2014
                if (_movement == CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1
                 || _movement == CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1
                 || _movement == CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1
                 || _movement == CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1
                 || _movement == CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1
                 || _movement == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1
                 || _movement == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1
                 || _movement == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1
                 || _movement == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT1
                 || _movement == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1
                 || _movement == CASHIER_MOVEMENT.BILL_IN_EXCHANGE_DUAL_CURRENCY
                 || _movement == CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_NATIONAL_CURRENCY
                 || _movement == CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_FOREIGN_CURRENCY
                 || _movement == CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CARD_EXCHANGE
                 || _movement == CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CREDIT_CARD_EXCHANGE
                 || _movement == CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_DEBIT_CARD_EXCHANGE
                 || _movement == CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CHECK_EXCHANGE)
                {
                  _param_currency_increment.Value = _row["CM_INITIAL_BALANCE"];
                }

                _param_in_cashier_sessions_by_currency.Value = 1;

                if (!_row.IsNull("CM_CURRENCY_ISO_CODE") && (String)_row["CM_CURRENCY_ISO_CODE"] == _national_currency)
                {
                  switch (_movement)
                  {
                    case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1:
                    case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2:
                    // RMS 08-AUG-2014
                    case CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
                    case CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2:
                    case CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
                    case CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2:
                    case CASHIER_MOVEMENT.CLOSE_SESSION_BANK_CARD:
                    // DLL 15-DIC-2014
                    case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1:
                    case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
                    case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
                    case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CARD_EXCHANGE:
                    case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CREDIT_CARD_EXCHANGE:
                    case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_DEBIT_CARD_EXCHANGE:
                      _param_currency_type.Value = (Int32)CurrencyExchangeType.CARD;
                      break;

                    case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1:
                    case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT2:
                    case CASHIER_MOVEMENT.CLOSE_SESSION_CHECK:
                    case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1:
                    case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT2:
                    case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CHECK_EXCHANGE:
                      _param_currency_type.Value = (Int32)CurrencyExchangeType.CHECK;
                      break;
                  }
                }

                break;

              // RAB 01-AUG-2016
              case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1_UNDO:
              case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2_UNDO:
                _param_in_cashier_sessions_by_currency.Value = 0;
                _param_in_cashier_sessions.Value = 1;
                _param_increment.Value = ((Decimal)_row["CM_SUB_AMOUNT"]);
                break;

              case CASHIER_MOVEMENT.CASH_ADVANCE_CURRENCY_EXCHANGE_UNDO:
              case CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD_UNDO:
              case CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD_UNDO:
              case CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD_UNDO:
                _param_in_cashier_sessions_by_currency.Value = 0;
                _param_in_cashier_sessions.Value = 1;
                _param_increment.Value = ((Decimal)_row["CM_ADD_AMOUNT"]);
                break;

              case CASHIER_MOVEMENT.FILLER_OUT_CHECK:
                _param_in_cashier_sessions_by_currency.Value = 1;
                _param_in_cashier_sessions.Value = 0;
                _param_increment.Value = 0;
                _param_currency_increment.Value = -((Decimal)_row["CM_SUB_AMOUNT"]);
                break;

              case CASHIER_MOVEMENT.OPEN_SESSION:
                _param_increment.Value = _row["CM_BALANCE_INCREMENT"];

                if (!_row.IsNull("CM_CURRENCY_ISO_CODE") && (String)_row["CM_CURRENCY_ISO_CODE"] != "")
                {
                  _param_increment.Value = 0;
                  _param_currency_increment.Value = _row["CM_BALANCE_INCREMENT"];

                  _param_in_cashier_sessions_by_currency.Value = 1;
                  _param_in_cashier_sessions.Value = 0;
                }
                break;

              case CASHIER_MOVEMENT.FILLER_IN:
              case CASHIER_MOVEMENT.FILLER_OUT:
              case CASHIER_MOVEMENT.CLOSE_SESSION:
              case CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN:
              case CASHIER_MOVEMENT.TRANSFER_CASHIERS_SEND:
              case CASHIER_MOVEMENT.TRANSFER_CASHIERS_RECEIVE:
              // DHA 09-JUN-2016
              case CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK:
                _param_currency_increment.Value = _row["CM_BALANCE_INCREMENT"];
                _param_increment.Value = _row["CM_BALANCE_INCREMENT"];
                // Insert in "...by_currency" for check and card, it is relacionated for game profit
                if (!_row.IsNull("CM_CURRENCY_ISO_CODE") && (String)_row["CM_CURRENCY_ISO_CODE"] != _national_currency ||
                  !_row.IsNull("CM_CURRENCY_ISO_CODE") && (String)_row["CM_CURRENCY_ISO_CODE"] == _national_currency && (CurrencyExchangeType)_param_currency_type.Value != CurrencyExchangeType.CURRENCY)
                {
                  _param_increment.Value = 0;
                  _param_in_cashier_sessions_by_currency.Value = 1;
                  _param_in_cashier_sessions.Value = 0;
                }
                break;

              // DHA 21-APR-2016: corrected the initial and final balance for each iso code/currency type
              case CASHIER_MOVEMENT.CASH_CLOSING_OVER:
              case CASHIER_MOVEMENT.CASH_CLOSING_SHORT:
                _param_currency_increment.Value = _row["CM_BALANCE_INCREMENT"];
                _param_increment.Value = _row["CM_BALANCE_INCREMENT"];
                if (!_row.IsNull("CM_CURRENCY_ISO_CODE") && (String)_row["CM_CURRENCY_ISO_CODE"] != "" &&
                  (String)_row["CM_CURRENCY_ISO_CODE"] == _national_currency && (CurrencyExchangeType)_param_currency_type.Value != CurrencyExchangeType.CURRENCY)
                {
                  _param_increment.Value = 0;
                  _param_in_cashier_sessions_by_currency.Value = 1;
                  _param_in_cashier_sessions.Value = 0;
                }
                break;

              case CASHIER_MOVEMENT.CAGE_CLOSE_SESSION:
                _row["CM_INITIAL_BALANCE"] = (Decimal)_row["CM_ADD_AMOUNT"];
                _row["CM_FINAL_BALANCE"] = (Decimal)_row["CM_ADD_AMOUNT"];
                _row["CM_BALANCE_INCREMENT"] = 0;
                _row["CM_ADD_AMOUNT"] = 0;
                break;

              // 20-SEP-2013 DLL
              case CASHIER_MOVEMENT.CLOSE_SESSION_BANK_CARD:
                _param_currency_increment.Value = _row["CM_BALANCE_INCREMENT"];
                _param_increment.Value = 0;
                _param_in_cashier_sessions_by_currency.Value = 1;
                _param_in_cashier_sessions.Value = 0;
                _param_currency_type.Value = (Int32)CurrencyExchangeType.CARD;
                break;

              // 20-SEP-2013 DLL
              case CASHIER_MOVEMENT.CLOSE_SESSION_CHECK:
                _param_currency_increment.Value = _row["CM_BALANCE_INCREMENT"];
                _param_increment.Value = 0;
                _param_in_cashier_sessions_by_currency.Value = 1;
                _param_in_cashier_sessions.Value = 0;
                _param_currency_type.Value = (Int32)CurrencyExchangeType.CHECK;
                break;

              case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL:
                _param_in_cashier_sessions_by_currency.Value = 1;
                _param_in_cashier_sessions.Value = 1;
                _param_increment.Value = 0;
                if (!_cashier_mode)
                {
                  _param_currency_increment.Value = -((Decimal)_row["CM_SUB_AMOUNT"]);
                }
                break;

              case CASHIER_MOVEMENT.CHIPS_SALE_REMAINING_AMOUNT:
                _param_in_cashier_sessions_by_currency.Value = 1;
                _param_in_cashier_sessions.Value = 1;
                _param_increment.Value = 0;
                if (!_cashier_mode)
                {
                  _param_currency_increment.Value = -((Decimal)_row["CM_SUB_AMOUNT"]);
                }
                break;

              case CASHIER_MOVEMENT.CHIPS_SALE_CONSUMED_REMAINING_AMOUNT:
                _param_in_cashier_sessions_by_currency.Value = 1;
                _param_in_cashier_sessions.Value = 1;
                _param_increment.Value = 0;
                if (!_cashier_mode)
                {
                  _param_currency_increment.Value = -((Decimal)_row["CM_ADD_AMOUNT"]);
                }
                break;

              case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL_EXCHANGE:
                if (!_row.IsNull("CM_CURRENCY_ISO_CODE") && (String)_row["CM_CURRENCY_ISO_CODE"] != _national_currency || (CurrencyExchangeType)_param_currency_type.Value != CurrencyExchangeType.CURRENCY)
                {
                  _param_in_cashier_sessions.Value = 0;
                  _param_in_cashier_sessions_by_currency.Value = 1;
                  _param_increment.Value = 0;
                  _param_currency_increment.Value = -(Decimal)_row["CM_INITIAL_BALANCE"];
                }
                else
                {
#if !SQL_BUSINESS_LOGIC
                  Log.Error(String.Format("Invalid currency for the Movement {0}", _movement));
#endif
                }
                break;

              case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL:
                _param_in_cashier_sessions_by_currency.Value = 1;
                _param_in_cashier_sessions.Value = 1;
                _param_increment.Value = 0;
                _param_currency_increment.Value = (Decimal)_row["CM_ADD_AMOUNT"];
                break;

              case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL_EXCHANGE:
                if (!_row.IsNull("CM_CURRENCY_ISO_CODE") && (String)_row["CM_CURRENCY_ISO_CODE"] != _national_currency || (CurrencyExchangeType)_param_currency_type.Value != CurrencyExchangeType.CURRENCY)
                {
                  _param_in_cashier_sessions.Value = 0;
                  _param_in_cashier_sessions_by_currency.Value = 1;
                  _param_increment.Value = 0;
                  _param_currency_increment.Value = (Decimal)_row["CM_INITIAL_BALANCE"];
                }
                else
                {
#if !SQL_BUSINESS_LOGIC
                  Log.Error(String.Format("Invalid currency for the Movement {0}", _movement));
#endif
                }
                break;

              case CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN_CLOSE:
              case CASHIER_MOVEMENT.FILLER_OUT_CLOSING_STOCK:
                _param_currency_increment.Value = 0;
                _param_increment.Value = 0;
                if (!_row.IsNull("CM_CURRENCY_ISO_CODE") && (String)_row["CM_CURRENCY_ISO_CODE"] != "")
                {
                  _param_in_cashier_sessions_by_currency.Value = 1;
                  _param_in_cashier_sessions.Value = 0;
                }
                break;

              case CASHIER_MOVEMENT.CASH_ADVANCE_CURRENCY_EXCHANGE:
              case CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD:
              case CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD:
              case CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD:
              case CASHIER_MOVEMENT.CASH_ADVANCE_CHECK:
              case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_GENERIC_BANK_CARD:
              case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CREDIT_BANK_CARD:
              case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_DEBIT_BANK_CARD:
              case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CHECK:
                _param_in_cashier_sessions.Value = 0;
                _param_increment.Value = 0;
                _param_in_cashier_sessions_by_currency.Value = 1;
                _param_currency_increment.Value = (Decimal)_row["CM_INITIAL_BALANCE"];
                break;

              case CASHIER_MOVEMENT.CASH_ADVANCE_CASH:
                _param_in_cashier_sessions.Value = 1;
                _param_increment.Value = -((Decimal)_row["CM_SUB_AMOUNT"]);
                break;

              case CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN:
                _param_in_cashier_sessions.Value = 1;
                _param_in_cashier_sessions_by_currency.Value = 0;
                _param_increment.Value = (Decimal)_row["CM_ADD_AMOUNT"];
                _param_currency_increment.Value = 0;

                if (!_row.IsNull("CM_CURRENCY_ISO_CODE") && (String)_row["CM_CURRENCY_ISO_CODE"] != _national_currency || (CurrencyExchangeType)_param_currency_type.Value != CurrencyExchangeType.CURRENCY)
                {
                  _param_in_cashier_sessions.Value = 0;
                  _param_in_cashier_sessions_by_currency.Value = 1;
                  _param_increment.Value = 0;
                  _param_currency_increment.Value = (Decimal)_row["CM_ADD_AMOUNT"];
                }

                break;

              case CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT:
                _param_in_cashier_sessions.Value = 1;
                _param_in_cashier_sessions_by_currency.Value = 0;
                _param_increment.Value = -((Decimal)_row["CM_SUB_AMOUNT"]);
                _param_currency_increment.Value = 0;

                if (!_row.IsNull("CM_CURRENCY_ISO_CODE") && (String)_row["CM_CURRENCY_ISO_CODE"] != _national_currency || (CurrencyExchangeType)_param_currency_type.Value != CurrencyExchangeType.CURRENCY)
                {
                  _param_in_cashier_sessions.Value = 0;
                  _param_in_cashier_sessions_by_currency.Value = 1;
                  _param_increment.Value = 0;
                  _param_currency_increment.Value = -((Decimal)_row["CM_SUB_AMOUNT"]);
                }

                break;
              case CASHIER_MOVEMENT.MB_CASH_IN:
                _param_currency_increment.Value = ((Decimal)_row["CM_ADD_AMOUNT"]);
                _param_increment.Value = ((Decimal)_row["CM_ADD_AMOUNT"]);
                break;

              // CARD DEPOSIT 
              case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CHECK:
              case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_CREDIT:
              case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_DEBIT:
              case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_GENERIC:
              case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CURRENCY_EXCHANGE:
              // CARD DEPOSIT (COMPANY B)
              case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK:
              case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT:
              case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT:
              case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC:
              case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE:
              // CARD REPLACEMENT
              case CASHIER_MOVEMENT.CARD_REPLACEMENT_CHECK:
              case CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_CREDIT:
              case CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_DEBIT:
              case CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_GENERIC:
              // CARD REPLACEMENT (COMPANY B)
              case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CHECK:
              case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT:
              case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT:
              case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC:
              // RECEPTION MOVEMENTS
              case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CARD:
              case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CREDIT_CARD:
              case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_DEBIT_CARD:
              case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CHECK:

                _param_in_cashier_sessions.Value = 0;
                _param_increment.Value = 0;
                _param_in_cashier_sessions_by_currency.Value = 1;
                //_param_currency_increment.Value = _row["CM_BALANCE_INCREMENT"];
                _param_currency_type.Value = (Int32)CurrencyExchangeType.CARD;

                if (_row.IsNull("CM_CURRENCY_ISO_CODE"))
                {
                  _row["CM_CURRENCY_ISO_CODE"] = _national_currency;
                  _param_iso_code.Value = _row["CM_CURRENCY_ISO_CODE"];
                }

                if (_movement == CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CURRENCY_EXCHANGE || _movement == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE)
                {
                  _param_currency_type.Value = (Int32)CurrencyExchangeType.CURRENCY;
                  _param_currency_increment.Value = 0;
                }

                if (_movement == CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CHECK || _movement == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK
                  || _movement == CASHIER_MOVEMENT.CARD_REPLACEMENT_CHECK || _movement == CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CHECK
                  || _movement == CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CHECK)
                {
                  _param_currency_type.Value = (Int32)CurrencyExchangeType.CHECK;
                }

                break;

              case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_UNDO:
              case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_UNDO:
                _param_in_cashier_sessions.Value = 0;
                _param_increment.Value = 0;
                _param_in_cashier_sessions_by_currency.Value = 1;
                //_param_currency_increment.Value = _row["CM_BALANCE_INCREMENT"];
                _param_currency_type.Value = (Int32)CurrencyExchangeType.CARD;

                if (_row.IsNull("CM_CURRENCY_ISO_CODE"))
                {
                  _row["CM_CURRENCY_ISO_CODE"] = _national_currency;
                  _param_iso_code.Value = _row["CM_CURRENCY_ISO_CODE"];
                }

                break;

              case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE:
                _param_currency_type.Value = (Int32)CurrencyExchangeType.CURRENCY;

                _param_in_cashier_sessions.Value = 0;
                _param_increment.Value = 0;
                _param_in_cashier_sessions_by_currency.Value = 0;
                _param_currency_increment.Value = 0;

                if (_param_iso_code.Value.ToString() == _national_currency)
                {
                  _param_in_cashier_sessions.Value = 1;
                  _param_increment.Value = _row["CM_BALANCE_INCREMENT"];
                }
                else
                {
                  _param_in_cashier_sessions_by_currency.Value = 1;
                  _param_currency_increment.Value = _row["CM_BALANCE_INCREMENT"];
                }

                break;

              case CASHIER_MOVEMENT.CASHIER_COLLECT_DROPBOX_TERMINAL_CAGE:
              case CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_TERMINAL:
              case CASHIER_MOVEMENT.CASHIER_REFILL_HOPPER_TERMINAL_CAGE:
              case CASHIER_MOVEMENT.CAGE_CASHIER_REFILL_HOPPER_TERMINAL:

                // 29-OCT-2015 FAV
                _param_in_cashier_sessions.Value = 1;
                _param_in_cashier_sessions_by_currency.Value = 0;
                _param_increment.Value = 0;
                _param_currency_increment.Value = 0;

                if (!_row.IsNull("CM_CURRENCY_ISO_CODE") && (String)_row["CM_CURRENCY_ISO_CODE"] != _national_currency)
                {
                  _param_in_cashier_sessions.Value = 0;
                  _param_in_cashier_sessions_by_currency.Value = 1;
                }
                break;

              case CASHIER_MOVEMENT.CASHIER_COLLECT_DROPBOX_TERMINAL:
              case CASHIER_MOVEMENT.TERMINAL_REFILL_HOPPER:
                _param_in_cashier_sessions.Value = 1;
                _param_in_cashier_sessions_by_currency.Value = 0;
                _param_increment.Value = (Decimal)_row["CM_ADD_AMOUNT"];
                _param_currency_increment.Value = 0;

                if (!_row.IsNull("CM_CURRENCY_ISO_CODE") && (String)_row["CM_CURRENCY_ISO_CODE"] != _national_currency)
                {
                  _param_in_cashier_sessions.Value = 0;
                  _param_in_cashier_sessions_by_currency.Value = 1;
                  _param_increment.Value = 0;
                  _param_currency_increment.Value = (Decimal)_row["CM_ADD_AMOUNT"];
                }
                break;

              case CASHIER_MOVEMENT.CASHIER_REFILL_HOPPER_TERMINAL:
                _param_in_cashier_sessions.Value = 1;
                _param_in_cashier_sessions_by_currency.Value = 0;
                _param_increment.Value = -((Decimal)_row["CM_SUB_AMOUNT"]);
                _param_currency_increment.Value = 0;

                if (!_row.IsNull("CM_CURRENCY_ISO_CODE") && (String)_row["CM_CURRENCY_ISO_CODE"] != _national_currency)
                {
                  _param_in_cashier_sessions.Value = 0;
                  _param_in_cashier_sessions_by_currency.Value = 1;
                  _param_increment.Value = 0;
                  _param_currency_increment.Value = (Decimal)_row["CM_SUB_AMOUNT"];
                }
                break;

              case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_OUT:
                // FAV 03-MAY-2015 Multiple denominations
                _param_in_cashier_sessions.Value = 1;
                _param_in_cashier_sessions_by_currency.Value = 0;
                _param_increment.Value = -((Decimal)_row["CM_SUB_AMOUNT"]);
                _param_currency_increment.Value = 0;

                if (!_row.IsNull("CM_CURRENCY_ISO_CODE") && (String)_row["CM_CURRENCY_ISO_CODE"] != _national_currency)
                {
                  _param_in_cashier_sessions.Value = 0;
                  _param_in_cashier_sessions_by_currency.Value = 1;
                  _param_increment.Value = 0;
                  _param_currency_increment.Value = -(Decimal)_row["CM_SUB_AMOUNT"];
                }
                break;

              case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_IN:
                // FAV 03-MAY-2015 Multiple denominations
                _param_in_cashier_sessions.Value = 1;
                _param_in_cashier_sessions_by_currency.Value = 0;
                _param_increment.Value = ((Decimal)_row["CM_ADD_AMOUNT"]);
                _param_currency_increment.Value = 0;

                if (!_row.IsNull("CM_CURRENCY_ISO_CODE") && (String)_row["CM_CURRENCY_ISO_CODE"] != _national_currency)
                {
                  _param_in_cashier_sessions.Value = 0;
                  _param_in_cashier_sessions_by_currency.Value = 1;
                  _param_increment.Value = 0;
                  _param_currency_increment.Value = (Decimal)_row["CM_ADD_AMOUNT"];
                }
                break;

              case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_NATIONAL_CURRENCY:
                // FAV 03-MAY-2015 Multiple denominations
                if (!_row.IsNull("CM_CURRENCY_ISO_CODE") && (String)_row["CM_CURRENCY_ISO_CODE"] != _national_currency)
                {
                  _param_in_cashier_sessions.Value = 0;
                  _param_in_cashier_sessions_by_currency.Value = 1;
                  _param_increment.Value = 0;
                  _param_currency_increment.Value = (Decimal)_row["CM_INITIAL_BALANCE"];
                }
                else
                {
#if !SQL_BUSINESS_LOGIC
                  Log.Error(String.Format("Invalid currency for the Movement {0}", _movement));
#endif
                }
                break;

              case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_FOREIGN_CURRENCY:
                // FAV 03-MAY-2015 Multiple denominations
                if (!_row.IsNull("CM_CURRENCY_ISO_CODE") && (String)_row["CM_CURRENCY_ISO_CODE"] != _national_currency)
                {
                  _param_in_cashier_sessions.Value = 0;
                  _param_in_cashier_sessions_by_currency.Value = 1;
                  _param_increment.Value = 0;
                  _param_currency_increment.Value = -(Decimal)_row["CM_INITIAL_BALANCE"];
                }
                else
                {
#if !SQL_BUSINESS_LOGIC
                  Log.Error(String.Format("Invalid currency for the Movement {0}", _movement));
#endif
                }
                break;

              case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_IN_CHANGE:
                // FAV 10-MAY-2015 Multiple denominations
                _param_in_cashier_sessions.Value = 1;
                _param_in_cashier_sessions_by_currency.Value = 0;
                _param_increment.Value = -((Decimal)_row["CM_SUB_AMOUNT"]);
                _param_currency_increment.Value = 0;

                if (!_row.IsNull("CM_CURRENCY_ISO_CODE") && (String)_row["CM_CURRENCY_ISO_CODE"] != _national_currency)
                {
                  _param_in_cashier_sessions.Value = 0;
                  _param_in_cashier_sessions_by_currency.Value = 1;
                  _param_increment.Value = 0;
                  _param_currency_increment.Value = -(Decimal)_row["CM_SUB_AMOUNT"];
                }
                break;

              case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_STATE_TAX:
              case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_FEDERAL_TAX:
              case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_COUNCIL_TAX:
                _param_increment.Value = 0;
                break;

              case CASHIER_MOVEMENT.CHIPS_CHANGE_IN:
                _param_in_cashier_sessions.Value = 1;
                _param_in_cashier_sessions_by_currency.Value = 0;
                _param_increment.Value = ((Decimal)_row["CM_ADD_AMOUNT"]);
                _param_currency_increment.Value = 0;

                if (!_row.IsNull("CM_CURRENCY_ISO_CODE") && (String)_row["CM_CURRENCY_ISO_CODE"] != _national_currency || (CurrencyExchangeType)_param_currency_type.Value != CurrencyExchangeType.CURRENCY)
                {
                  _param_in_cashier_sessions.Value = 0;
                  _param_in_cashier_sessions_by_currency.Value = 1;
                  _param_increment.Value = 0;
                  _param_currency_increment.Value = (Decimal)_row["CM_ADD_AMOUNT"];
                }
                break;

              case CASHIER_MOVEMENT.CHIPS_CHANGE_OUT:
                _param_in_cashier_sessions.Value = 1;
                _param_in_cashier_sessions_by_currency.Value = 0;
                _param_increment.Value = -((Decimal)_row["CM_SUB_AMOUNT"]);
                _param_currency_increment.Value = 0;

                if (!_row.IsNull("CM_CURRENCY_ISO_CODE") && (String)_row["CM_CURRENCY_ISO_CODE"] != _national_currency || (CurrencyExchangeType)_param_currency_type.Value != CurrencyExchangeType.CURRENCY)
                {
                  _param_in_cashier_sessions.Value = 0;
                  _param_in_cashier_sessions_by_currency.Value = 1;
                  _param_increment.Value = 0;
                  _param_currency_increment.Value = -(Decimal)_row["CM_SUB_AMOUNT"];
                }
                break;

              case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT1:
              case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT2:
                if ((Decimal)_row["CM_INITIAL_BALANCE"] == (Decimal)_row["CM_FINAL_BALANCE"])
                {
                  _param_currency_increment.Value = 0;
                  _param_increment.Value = 0;
                }
                else
                {
                  _param_currency_increment.Value = _row["CM_BALANCE_INCREMENT"];
                  _param_increment.Value = _row["CM_BALANCE_INCREMENT"];
                }
                break;
              case CASHIER_MOVEMENT.CREDIT_LINE_MARKER:
                _param_increment.Value = -(Decimal)_row["CM_SUB_AMOUNT"];
                _param_currency_increment.Value = -(Decimal)_row["CM_SUB_AMOUNT"];
                break;

              default:
                _param_currency_increment.Value = _row["CM_BALANCE_INCREMENT"];
                _param_increment.Value = _row["CM_BALANCE_INCREMENT"];

                if (_movement > CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN && _movement < CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT       // (1000000 - 2000000)
                  || _movement > CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT && _movement <= CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_LAST  // (2000000 - 2999999]
                  || _movement > CASHIER_MOVEMENT.MULTIPLE_BUCKETS_END_SESSION && _movement <= CASHIER_MOVEMENT.MULTIPLE_BUCKETS_END_SESSION_LAST   // (1100 - 1199] 
                  || _movement > CASHIER_MOVEMENT.MULTIPLE_BUCKETS_EXPIRED && _movement <= CASHIER_MOVEMENT.MULTIPLE_BUCKETS_EXPIRED_LAST           // (1200 - 1299]
                  || _movement > CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_ADD && _movement <= CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_ADD_LAST     // (1300 - 1399]
                  || _movement > CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SUB && _movement <= CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SUB_LAST     // (1400 - 1499]
                  || _movement > CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SET && _movement <= CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SET_LAST     // (1500 - 1599]
                   )
                {
                  _param_currency_increment.Value = 0;
                  _param_increment.Value = 0;
                }

                break;
            }

            if (_movement != CASHIER_MOVEMENT.CAGE_FILLER_IN &&
                _movement != CASHIER_MOVEMENT.CAGE_FILLER_OUT &&
                _movement != CASHIER_MOVEMENT.CAGE_CLOSE_SESSION &&
                _movement != CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_TERMINAL &&
                _movement != CASHIER_MOVEMENT.CAGE_CASHIER_REFILL_HOPPER_TERMINAL &&
                _movement != CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_GAMINGTABLE_DETAIL &&
                _movement != CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_GAMINGTABLE
               )
            {
              using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
              {
                if (!_reader.Read())
                {
                  _has_not_errors = false;
                  _movement = CASHIER_MOVEMENT.NOT_ASSIGNED;
                }

                switch (_movement)
                {
                  case CASHIER_MOVEMENT.NOT_ASSIGNED:
                    break;

                  case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT1:
                  case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_SPLIT2:
                  case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1:
                  case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2:
                  // RAB 01-AUG-2016
                  case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT1_UNDO:
                  case CASHIER_MOVEMENT.CURRENCY_CARD_EXCHANGE_SPLIT2_UNDO:
                  case CASHIER_MOVEMENT.CASH_ADVANCE_CURRENCY_EXCHANGE_UNDO:
                  case CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD_UNDO:
                  case CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD_UNDO:
                  case CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD_UNDO:
                  // RMS 08-AUG-2014
                  case CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
                  case CASHIER_MOVEMENT.CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2:
                  case CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
                  case CASHIER_MOVEMENT.CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2:
                  case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT1:
                  case CASHIER_MOVEMENT.CURRENCY_CHECK_SPLIT2:
                  case CASHIER_MOVEMENT.FILLER_OUT_CHECK:
                  case CASHIER_MOVEMENT.CASH_ADVANCE_CURRENCY_EXCHANGE:
                  case CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD:
                  case CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD:
                  case CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD:
                  case CASHIER_MOVEMENT.CASH_ADVANCE_CHECK:
                  // DLL 15-DEC-2014
                  case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT1:
                  case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT2:
                  case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT1:
                  case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CREDIT_CARD_EXCHANGE_SPLIT2:
                  case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT1:
                  case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_DEBIT_CARD_EXCHANGE_SPLIT2:
                  // DLL 17-DEC-2014
                  case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT1:
                  case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT2:
                  case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT1:
                  case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT2:
                  // DLL 18-DEC-2014
                  case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CURRENCY_EXCHANGE:
                  case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_GENERIC_BANK_CARD:
                  case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CREDIT_BANK_CARD:
                  case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_DEBIT_BANK_CARD:
                  case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CHECK:
                  // DHA 25-JAN-2016
                  case CASHIER_MOVEMENT.TITO_TICKETS_CREATE_EXCHANGE_DUAL_CURRENCY:
                  case CASHIER_MOVEMENT.TITO_TICKETS_PLAYED_EXCHANGE_DUAL_CURRENCY:
                  case CASHIER_MOVEMENT.BILL_IN_EXCHANGE_DUAL_CURRENCY:
                  // FAV 03-MAY-2016
                  case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_NATIONAL_CURRENCY:
                  case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_FOREIGN_CURRENCY:
                  // DHA 13-JUL-2016
                  case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL_EXCHANGE:
                  case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL_EXCHANGE:

                  case CASHIER_MOVEMENT.REOPEN_CASHIER:
                  case CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN_CLOSE:
                  case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CARD_EXCHANGE:
                  case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CREDIT_CARD_EXCHANGE:
                  case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_DEBIT_CARD_EXCHANGE:
                  case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK_CHECK_EXCHANGE:
                    break;

                  // FAV 11-JAN-2015
                  case CASHIER_MOVEMENT.CASHIER_COLLECT_DROPBOX_TERMINAL_CAGE:
                  case CASHIER_MOVEMENT.CASHIER_REFILL_HOPPER_TERMINAL_CAGE:

                    // Without changes in the final balance
                    _row["CM_INITIAL_BALANCE"] = _reader.GetDecimal(0);
                    _row["CM_FINAL_BALANCE"] = _reader.GetDecimal(0);
                    break;

                  default:
                    if (_movement > CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN && _movement < CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT                      // (1000000 - 2000000)
                     || _movement > CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT && _movement <= CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_LAST                   // (2000000 - 2999999]
                     || _movement > CASHIER_MOVEMENT.MULTIPLE_BUCKETS_END_SESSION && _movement <= CASHIER_MOVEMENT.MULTIPLE_BUCKETS_END_SESSION_LAST   // (1100 - 1199] 
                     || _movement > CASHIER_MOVEMENT.MULTIPLE_BUCKETS_EXPIRED && _movement <= CASHIER_MOVEMENT.MULTIPLE_BUCKETS_EXPIRED_LAST           // (1200 - 1299]
                     || _movement > CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_ADD && _movement <= CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_ADD_LAST     // (1300 - 1399]
                     || _movement > CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SUB && _movement <= CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SUB_LAST     // (1400 - 1499]
                     || _movement > CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SET && _movement <= CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SET_LAST     // (1500 - 1599]
                      )
                    {
                      break;
                    
                    }

                    if (IsReOpen)
                    {
                      _row["CM_INITIAL_BALANCE"] = _row["CM_INITIAL_BALANCE"];
                      _row["CM_FINAL_BALANCE"] = _row["CM_FINAL_BALANCE"];
                      break;
                    }

                    _row["CM_INITIAL_BALANCE"] = _reader.GetDecimal(0);
                    _row["CM_FINAL_BALANCE"] = _reader.GetDecimal(1);
                    break;
                }

              } // using SqlDataReader
            } // if
          } //if _has_not_errors
        } // foreach DataRow
      } // using SqlCommand
    }

    public static void GetCashierMovementAddSubAmount(CASHIER_MOVEMENT MovementType, Decimal Amount, out Decimal AddAmount, out Decimal SubAmount, ref String CardTrackData, bool IsSwap = false)
    {
      String _card_track_data;

      _card_track_data = CardTrackData;

      AddAmount = 0;
      SubAmount = 0;
      CardTrackData = "";

      #region Switch(MovementType)

      switch (MovementType)
      {
        case CASHIER_MOVEMENT.OPEN_SESSION:
        case CASHIER_MOVEMENT.CAGE_OPEN_SESSION:
        case CASHIER_MOVEMENT.CAGE_CLOSE_SESSION:
          AddAmount = Amount;
          SubAmount = 0;
          break;

        case CASHIER_MOVEMENT.CLOSE_SESSION:
          AddAmount = 0;
          SubAmount = 0;
          break;

        case CASHIER_MOVEMENT.CASH_CLOSING_OVER:
        case CASHIER_MOVEMENT.CASH_CLOSING_SHORT:
          AddAmount = Amount;
          SubAmount = 0;
          break;

        case CASHIER_MOVEMENT.PROMO_START_SESSION:
        case CASHIER_MOVEMENT.PROMO_END_SESSION:
          AddAmount = 0;
          SubAmount = 0;
          break;

        case CASHIER_MOVEMENT.PROMO_CREDITS:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.PROMO_CANCEL:
        case CASHIER_MOVEMENT.NOT_REDEEMABLE_EXPIRED:
        case CASHIER_MOVEMENT.REDEEMABLE_DEV_EXPIRED:
        case CASHIER_MOVEMENT.REDEEMABLE_PRIZE_EXPIRED:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.PROMO_TO_REDEEMABLE:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.PROMO_EXPIRED:
          AddAmount = 0;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.FILLER_IN:
        case CASHIER_MOVEMENT.CAGE_FILLER_IN:
        case CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN:
        case CASHIER_MOVEMENT.GAMING_TABLE_GAME_NETWIN_CLOSE:
        case CASHIER_MOVEMENT.TRANSFER_CASHIERS_RECEIVE:
        // DHA 09-JUN-2016
        case CASHIER_MOVEMENT.FILLER_IN_CLOSING_STOCK:
          AddAmount = Amount;
          SubAmount = 0;
          break;

        // GamingHall
        case CASHIER_MOVEMENT.CASHIER_COLLECT_DROPBOX_TERMINAL_CAGE:
        case CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_TERMINAL:
        //        case CASHIER_MOVEMENT.CAGE_HOPPER_FILLER_IN:
        case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE:
        case CASHIER_MOVEMENT.CASHIER_COLLECT_DROPBOX_TERMINAL:
        case CASHIER_MOVEMENT.TERMINAL_REFILL_HOPPER:
        case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CHECK:
        case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CREDIT_CARD:
        case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_DEBIT_CARD:
        case CASHIER_MOVEMENT.CUSTOMER_ENTRANCE_CARD:
          AddAmount = Amount;
          SubAmount = 0;
          break;
        case CASHIER_MOVEMENT.CASHIER_REFILL_HOPPER_TERMINAL_CAGE:
        case CASHIER_MOVEMENT.CASHIER_REFILL_HOPPER_TERMINAL:
        case CASHIER_MOVEMENT.CAGE_CASHIER_REFILL_HOPPER_TERMINAL:
          AddAmount = 0;
          SubAmount = Amount;
          break;

        case CASHIER_MOVEMENT.FILLER_OUT:
        case CASHIER_MOVEMENT.CAGE_FILLER_OUT:
        case CASHIER_MOVEMENT.FILLER_OUT_CHECK:
        case CASHIER_MOVEMENT.TRANSFER_CASHIERS_SEND:
        // DHA 09-JUN-2016
        case CASHIER_MOVEMENT.FILLER_OUT_CLOSING_STOCK:
        // DHA 27-JUL-2016
        case CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_GAMINGTABLE_DETAIL:
          AddAmount = 0;
          SubAmount = Amount;
          break;

        case CASHIER_MOVEMENT.CASH_IN:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.MB_CASH_LOST:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.MB_EXCESS:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CASH_OUT:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;
        // DHA 25-APR-2014 modified/added TITO tickets movements
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_CASHABLE:
          if (IsSwap)
          {
            AddAmount = 0;
            SubAmount = Amount;
          }
          else
          {
            AddAmount = Amount;
            SubAmount = 0;
          }
          CardTrackData = _card_track_data;

          break;
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_PROMO_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_PROMO_NOT_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_CASHABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_PROMO_NOT_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_CREATE_CASHABLE_OFFLINE:
        case CASHIER_MOVEMENT.TITO_TICKET_CREATE_PLAYABLE_OFFLINE:
        case CASHIER_MOVEMENT.TITO_TICKET_PLAYABLE_REDEEMED_OFFLINE:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHABLE_REDEEMED_OFFLINE:

        // YNM 13-APR-2016
        case CASHIER_MOVEMENT.TITO_TICKET_COUNTR_PRINTED_CASHABLE:

        case CASHIER_MOVEMENT.TITO_TICKET_REISSUE:
        case CASHIER_MOVEMENT.TITO_VALIDATE_TICKET:
        case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_HANDPAY:
        case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_JACKPOT:
        // SGB 01-DEC-2015
        case CASHIER_MOVEMENT.TITO_TICKET_PROMOBOX_PRINTED_PROMO_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_PROMOBOX_PRINTED_PROMO_NOT_REDEEMABLE:

          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_CASHABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_PROMO_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_HANDPAY:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_JACKPOT:
        case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_CASHABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_PROMO_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_PROMO_NOT_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_CASHABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_PROMO_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_JACKPOT:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_HANDPAY:

          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.PROMOTION_NOT_REDEEMABLE:
        case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE:
        case CASHIER_MOVEMENT.CASH_IN_COVER_COUPON:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_SWAP_FOR_CHIPS:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.PRIZES:
        case CASHIER_MOVEMENT.PRIZE_DRAW_GROSS_OTHER:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.TAX_ON_PRIZE1:
        case CASHIER_MOVEMENT.TAX_ON_PRIZE2:
        case CASHIER_MOVEMENT.TAX_ON_PRIZE3:
        case CASHIER_MOVEMENT.SERVICE_CHARGE:
        case CASHIER_MOVEMENT.SERVICE_CHARGE_ON_COMPANY_A:  // EOR 19-SEP-2016
        case CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE:
        case CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE_ON_COMPANY_A:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.TAX_RETURNING_ON_PRIZE_COUPON:
        case CASHIER_MOVEMENT.DECIMAL_ROUNDING:
        case CASHIER_MOVEMENT.TAX_RETURNING_CUSTODY: //EOR 20-APR-2016
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CARD_DEPOSIT_IN:
        // DHA 06-JUL-2015
        case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CHECK:
        case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CURRENCY_EXCHANGE:
        case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_CREDIT:
        case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_DEBIT:
        case CASHIER_MOVEMENT.CARD_DEPOSIT_IN_CARD_GENERIC:
        //FOS 09-JUL-2015
        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CARD_DEPOSIT_OUT:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_OUT:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CARD_REPLACEMENT:
        //FOS 09-JUL-2015
        case CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_CREDIT:
        case CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_DEBIT:
        case CASHIER_MOVEMENT.CARD_REPLACEMENT_CARD_GENERIC:
        case CASHIER_MOVEMENT.CARD_REPLACEMENT_CHECK:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_CREDIT:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_DEBIT:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CARD_GENERIC:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_REPLACEMENT_CHECK:
        // RAB 24-MAR-2016: Bug 10745
        case CASHIER_MOVEMENT.CARD_ASSOCIATE:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CANCEL_NOT_REDEEMABLE:
        case CASHIER_MOVEMENT.CANCEL_PROMOTION_REDEEMABLE:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.MB_MANUAL_CHANGE_LIMIT:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.MB_CASH_IN:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        //SSC 09-MAR-2012: Note Acceptor
        case CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1:
        case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT1:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        //SSC 09-MAR-2012: Note Acceptor
        case CASHIER_MOVEMENT.NA_CASH_IN_SPLIT2:
        case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT2:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        //SSC 11-JUN-2012: Movements for GiftRequest
        case CASHIER_MOVEMENT.POINTS_TO_NOT_REDEEMABLE:
        case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.DRAW_TICKET_PRINT:
        case CASHIER_MOVEMENT.POINTS_TO_DRAW_TICKET_PRINT:
          AddAmount = 0;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.HANDPAY:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.HANDPAY_CANCELLATION:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.MANUAL_HANDPAY:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.MANUAL_HANDPAY_CANCELLATION:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_PRIZE:
          AddAmount = 0;
          SubAmount = Amount; // The balance is not affected, next routine set the balance increment to 0.
          CardTrackData = _card_track_data;
          break;


        case CASHIER_MOVEMENT.CASH_IN_SPLIT1:
        case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT1:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CASH_IN_SPLIT2:
        case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT2:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.DEV_SPLIT1:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.DEV_SPLIT2:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CANCEL_SPLIT1:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CANCEL_SPLIT2:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.PRIZE_COUPON:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CARD_CREATION:
          AddAmount = 0;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CARD_PERSONALIZATION:
          AddAmount = 0;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;
        case CASHIER_MOVEMENT.CARD_RECYCLED:
          AddAmount = 0;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.ACCOUNT_PIN_CHANGE:
          AddAmount = 0;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.ACCOUNT_PIN_RANDOM:
          AddAmount = 0;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.PROMOTION_POINT:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CANCEL_PROMOTION_POINT:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.REQUEST_TO_VOUCHER_REPRINT:
        case CASHIER_MOVEMENT.REQUEST_TO_TICKET_REPRINT:
          AddAmount = Amount;
          SubAmount = 0;
          break;
        case CASHIER_MOVEMENT.CHECK_PAYMENT:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        // QMP 02-MAY-2013
        case CASHIER_MOVEMENT.CASH_PENDING_CLOSING:
          AddAmount = 0;
          SubAmount = 0;
          break;
        // SMN 05-SEP-2013
        case CASHIER_MOVEMENT.PARTICIPATION_IN_CASHDESK_DRAW:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.PRIZE_IN_KIND1_GROSS:
        case CASHIER_MOVEMENT.PRIZE_IN_KIND2_GROSS:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;
        case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX1:
        case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX2:
        case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX3:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;
        case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX1:
        case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX2:
        case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX3:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        // JML 22-DEC-2015
        case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_GROSS:
        case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_GROSS:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;
        case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX1:
        case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX2:
        case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX3:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;
        case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX1:
        case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX2:
        case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX3:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        // CHIPS
        case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL:
        case CASHIER_MOVEMENT.CHIPS_PURCHASE:
        case CASHIER_MOVEMENT.CHIPS_SALE_CONSUMED_REMAINING_AMOUNT:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL:
        case CASHIER_MOVEMENT.CHIPS_SALE_REGISTER_TOTAL:
        case CASHIER_MOVEMENT.CHIPS_SALE:
        case CASHIER_MOVEMENT.CHIPS_SALE_DEVOLUTION_FOR_TITO:
        case CASHIER_MOVEMENT.CHIPS_SALE_REMAINING_AMOUNT:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CASH_IN_TAX_SPLIT1:
        case CASHIER_MOVEMENT.CASH_IN_TAX_SPLIT2:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        // DHA 02-MAR-206
        case CASHIER_MOVEMENT.TAX_PROVISIONS:
        case CASHIER_MOVEMENT.TAX_CUSTODY:
          AddAmount = Amount;
          SubAmount = 0;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.CHIPS_CHANGE_IN:
          AddAmount = Amount;
          SubAmount = 0;
          break;

        case CASHIER_MOVEMENT.CHIPS_CHANGE_OUT:
          AddAmount = 0;
          SubAmount = Amount;
          break;

        case CASHIER_MOVEMENT.TRANSFER_CREDIT_IN:
          AddAmount = Amount;
          SubAmount = 0;
          break;

        case CASHIER_MOVEMENT.TRANSFER_CREDIT_OUT:
          AddAmount = 0;
          SubAmount = Amount;
          break;

        case CASHIER_MOVEMENT.TITO_TICKET_EXPIRED_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_EXPIRED_PROMO_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_EXPIRED_PROMO_NOT_REDEEMABLE:
          AddAmount = 0;
          SubAmount = Amount;
          break;

        case CASHIER_MOVEMENT.CASH_ADVANCE_CURRENCY_EXCHANGE:
        case CASHIER_MOVEMENT.CASH_ADVANCE_CHECK:
        case CASHIER_MOVEMENT.CASH_ADVANCE_GENERIC_BANK_CARD:
        case CASHIER_MOVEMENT.CASH_ADVANCE_CREDIT_BANK_CARD:
        case CASHIER_MOVEMENT.CASH_ADVANCE_DEBIT_BANK_CARD:
        // DLL 18-DEC-2014
        case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CURRENCY_EXCHANGE:
        case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_GENERIC_BANK_CARD:
        case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CREDIT_BANK_CARD:
        case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_DEBIT_BANK_CARD:
        case CASHIER_MOVEMENT.COMPANY_B_CASH_ADVANCE_CHECK:
          AddAmount = Amount;
          SubAmount = 0;
          break;

        case CASHIER_MOVEMENT.CASH_ADVANCE_CASH:
          AddAmount = 0;
          SubAmount = Amount;
          break;

        case CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN:
          AddAmount = Amount;
          SubAmount = 0;
          break;

        case CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT:
          AddAmount = 0;
          SubAmount = Amount;
          break;

        // DHA 08-OCT-2014: New movement (157) advice if a recharge is going to refund without plays
        case CASHIER_MOVEMENT.NOT_PLAYED_RECHARGE_REFUNDED:
          AddAmount = 0;
          SubAmount = Amount;
          CardTrackData = _card_track_data;
          break;

        case CASHIER_MOVEMENT.HANDPAY_WITHHOLDING:
          AddAmount = Amount;
          SubAmount = 0;
          break;

        // FAV 16-FEB-2016, information movement, it doesn't change the balance
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_CASHABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_PROMO_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_PROMO_NOT_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_VOID_CASHABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_VOID_PROMO_NOT_REDEEMABLE:
          AddAmount = Amount;
          SubAmount = 0;
          break;

        case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_IN:
          AddAmount = Amount;
          SubAmount = 0;
          break;

        case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_OUT:
          AddAmount = 0;
          SubAmount = Amount;
          break;

        case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_IN_CHANGE:
          AddAmount = 0;
          SubAmount = Amount;
          break;

        // AVZ 24-FEB-2016
        case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_FEDERAL_TAX:
        case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_STATE_TAX:
        case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE_COUNCIL_TAX:
          AddAmount = Amount;
          SubAmount = 0;
          break;

        case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL_EXCHANGE:
          AddAmount = 0;
          SubAmount = Amount;
          break;

        case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL_EXCHANGE:
          AddAmount = Amount;
          SubAmount = 0;
          break;

        case CASHIER_MOVEMENT.CREDIT_LINE_MARKER:
          AddAmount = 0;
          SubAmount = Amount;
          break;

        case CASHIER_MOVEMENT.CREDIT_LINE_MARKER_CARD_REPLACEMENT:
        case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK:
          AddAmount = Amount;
          SubAmount = 0;
          break;

        default:
          if (MovementType > CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_IN && MovementType < CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT)       // (1000000 - 2000000)
          {
            AddAmount = Amount;
            SubAmount = 0;
            break;
          }
          if (MovementType > CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_OUT && MovementType <= CASHIER_MOVEMENT.CASHIER_BY_CONCEPT_LAST)  // (2000000 - 2999999]
          {
            AddAmount = 0;
            SubAmount = Amount;
            break;
          }
          if (MovementType > CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_ADD && MovementType <= CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_ADD_LAST)
          {
            AddAmount = Amount;
            SubAmount = 0;
            break;
          }
          if (MovementType > CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SUB && MovementType <= CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SUB_LAST)
          {
            AddAmount = 0;
            SubAmount = Amount;
            break;
          }
          if (MovementType > CASHIER_MOVEMENT.MULTIPLE_BUCKETS_END_SESSION && MovementType <= CASHIER_MOVEMENT.MULTIPLE_BUCKETS_END_SESSION_LAST)
          {
            AddAmount = Amount;
            SubAmount = 0;
            break;
          }
          AddAmount = 0; //SMN
          SubAmount = 0;
          break;
      }

      #endregion

    } // GetCashierMovementAddSubAmount

    public static Decimal GetCashierMovementBalanceIncrement(CASHIER_MOVEMENT MovementType, Decimal AddAmount, Decimal SubAmount, Boolean IsCountR = false)
    {
      Decimal _balance_increment;

      switch (MovementType)
      {
        case CASHIER_MOVEMENT.MB_CASH_LOST:
        case CASHIER_MOVEMENT.TAX_ON_PRIZE1:
        case CASHIER_MOVEMENT.TAX_ON_PRIZE2:
        case CASHIER_MOVEMENT.TAX_ON_PRIZE3:
        case CASHIER_MOVEMENT.SERVICE_CHARGE:
        case CASHIER_MOVEMENT.SERVICE_CHARGE_ON_COMPANY_A:  //EOR 19-SEP-2016
        case CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE:
        case CASHIER_MOVEMENT.SERVICE_CHARGE_GAMING_TABLE_ON_COMPANY_A:
        case CASHIER_MOVEMENT.PRIZES:
        case CASHIER_MOVEMENT.PRIZE_DRAW_GROSS_OTHER:
        case CASHIER_MOVEMENT.PROMOTION_NOT_REDEEMABLE:
        case CASHIER_MOVEMENT.PROMOTION_REDEEMABLE:
        case CASHIER_MOVEMENT.CASH_IN_COVER_COUPON:
        case CASHIER_MOVEMENT.CANCEL_NOT_REDEEMABLE:
        case CASHIER_MOVEMENT.CANCEL_PROMOTION_REDEEMABLE:
        case CASHIER_MOVEMENT.MB_MANUAL_CHANGE_LIMIT:
        case CASHIER_MOVEMENT.PROMO_CREDITS:
        case CASHIER_MOVEMENT.PROMO_CANCEL:
        case CASHIER_MOVEMENT.PROMO_TO_REDEEMABLE:
        case CASHIER_MOVEMENT.HANDPAY:
        case CASHIER_MOVEMENT.HANDPAY_CANCELLATION:
        case CASHIER_MOVEMENT.MANUAL_HANDPAY:
        case CASHIER_MOVEMENT.MANUAL_HANDPAY_CANCELLATION:
        case CASHIER_MOVEMENT.CASH_IN_SPLIT1:
        case CASHIER_MOVEMENT.CASH_IN_SPLIT2:
        case CASHIER_MOVEMENT.DEV_SPLIT1:
        case CASHIER_MOVEMENT.DEV_SPLIT2:
        case CASHIER_MOVEMENT.CANCEL_SPLIT1:
        case CASHIER_MOVEMENT.CANCEL_SPLIT2:
        //SSC 09-MAR-2012: Note Acceptor
        case CASHIER_MOVEMENT.NA_CASH_IN_SPLIT1:
        case CASHIER_MOVEMENT.NA_CASH_IN_SPLIT2:
        case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT1:
        case CASHIER_MOVEMENT.MB_CASH_IN_SPLIT2:
        case CASHIER_MOVEMENT.PRIZE_COUPON:
        //case CASHIER_MOVEMENT.DEPOSIT_IN:
        //case CASHIER_MOVEMENT.DEPOSIT_OUT:
        //SSC 11-JUN-2012: Movements for GiftRequest
        case CASHIER_MOVEMENT.POINTS_TO_NOT_REDEEMABLE:
        case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE:

        case CASHIER_MOVEMENT.NOT_REDEEMABLE_EXPIRED:
        case CASHIER_MOVEMENT.REDEEMABLE_DEV_EXPIRED:
        case CASHIER_MOVEMENT.REDEEMABLE_PRIZE_EXPIRED:

        case CASHIER_MOVEMENT.PROMOTION_POINT:
        case CASHIER_MOVEMENT.CANCEL_PROMOTION_POINT:
        case CASHIER_MOVEMENT.TAX_RETURNING_ON_PRIZE_COUPON:
        case CASHIER_MOVEMENT.DECIMAL_ROUNDING:
        case CASHIER_MOVEMENT.REQUEST_TO_VOUCHER_REPRINT:
        case CASHIER_MOVEMENT.REQUEST_TO_TICKET_REPRINT:
        case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT1:
        case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_SPLIT2:
        case CASHIER_MOVEMENT.POINTS_TO_REDEEMABLE_AS_CASHIN_PRIZE:

        // EOR 20-APR-2016
        case CASHIER_MOVEMENT.TAX_RETURNING_CUSTODY:

        // SMN 05-SEP-2013
        case CASHIER_MOVEMENT.PARTICIPATION_IN_CASHDESK_DRAW:

        // AJQ 10-SEP-2013 UNR Prizes
        case CASHIER_MOVEMENT.PRIZE_IN_KIND1_GROSS:
        case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX1:
        case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX2:
        case CASHIER_MOVEMENT.PRIZE_IN_KIND1_TAX3:
        case CASHIER_MOVEMENT.PRIZE_IN_KIND2_GROSS:
        case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX1:
        case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX2:
        case CASHIER_MOVEMENT.PRIZE_IN_KIND2_TAX3:

        // JML 22-DEC-2015 URE Prizes
        case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_GROSS:
        case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX1:
        case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX2:
        case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND1_TAX3:
        case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_GROSS:
        case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX1:
        case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX2:
        case CASHIER_MOVEMENT.PRIZE_RE_IN_KIND2_TAX3:

        // DLL 17-SEP-2013 FILL OUT CHECK
        case CASHIER_MOVEMENT.FILLER_OUT_CHECK:
        // DHA 25-APR-2014 modified/added TITO tickets movements
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_CASHABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_PROMO_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PRINTED_PROMO_NOT_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_CASHABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_PROMO_NOT_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_CASHABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_PROMO_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PLAYED_PROMO_NOT_REDEEMABLE:

        // SGB 01-DEC-2015
        case CASHIER_MOVEMENT.TITO_TICKET_PROMOBOX_PRINTED_PROMO_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_PROMOBOX_PRINTED_PROMO_NOT_REDEEMABLE:

        case CASHIER_MOVEMENT.TITO_TICKET_REISSUE:
        case CASHIER_MOVEMENT.TITO_TICKET_CREATE_CASHABLE_OFFLINE:
        case CASHIER_MOVEMENT.TITO_TICKET_CREATE_PLAYABLE_OFFLINE:

        case CASHIER_MOVEMENT.CAGE_OPEN_SESSION:
        case CASHIER_MOVEMENT.CAGE_CLOSE_SESSION:
        case CASHIER_MOVEMENT.CAGE_FILLER_IN:
        case CASHIER_MOVEMENT.CAGE_FILLER_OUT:

        case CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_TERMINAL:
        case CASHIER_MOVEMENT.CAGE_CASHIER_REFILL_HOPPER_TERMINAL:

        case CASHIER_MOVEMENT.CHIPS_SALE:
        case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL:
        case CASHIER_MOVEMENT.CHIPS_PURCHASE:
        case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL:
        case CASHIER_MOVEMENT.CHIPS_SALE_REGISTER_TOTAL:

        case CASHIER_MOVEMENT.CASH_IN_TAX_SPLIT1:
        case CASHIER_MOVEMENT.CASH_IN_TAX_SPLIT2:

        // DHA 02-MAR-2016
        case CASHIER_MOVEMENT.TAX_PROVISIONS:
        case CASHIER_MOVEMENT.TRANSFER_CREDIT_IN:
        case CASHIER_MOVEMENT.TRANSFER_CREDIT_OUT:

        case CASHIER_MOVEMENT.TITO_TICKET_EXPIRED_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_EXPIRED_PROMO_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_EXPIRED_PROMO_NOT_REDEEMABLE:

        case CASHIER_MOVEMENT.TITO_VALIDATE_TICKET:
        case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_HANDPAY:
        case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_PRINTED_JACKPOT:

        // MPO 10-OCT-2014
        case CASHIER_MOVEMENT.HANDPAY_AUTHORIZED:
        case CASHIER_MOVEMENT.HANDPAY_UNAUTHORIZED:
        case CASHIER_MOVEMENT.HANDPAY_VOIDED:
        case CASHIER_MOVEMENT.HANDPAY_VOIDED_UNDONE:

        // DHA 10-OCT-2014
        case CASHIER_MOVEMENT.NOT_PLAYED_RECHARGE_REFUNDED:
        case CASHIER_MOVEMENT.CASH_CLOSING_SHORT:
        case CASHIER_MOVEMENT.CASH_CLOSING_OVER:

        case CASHIER_MOVEMENT.HANDPAY_WITHHOLDING:
        case CASHIER_MOVEMENT.MB_EXCESS:

        // FAV 11-JAN-2016
        case CASHIER_MOVEMENT.CASHIER_COLLECT_DROPBOX_TERMINAL_CAGE:
        case CASHIER_MOVEMENT.CASHIER_REFILL_HOPPER_TERMINAL_CAGE:


        // FAV 09-FEB-2016
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_CASHABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_PROMO_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_VOID_PROMO_NOT_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_VOID_CASHABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_MACHINE_VOID_PROMO_NOT_REDEEMABLE:

        // FAV 03-MAY-2016
        case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_IN:
        case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_OUT:
        case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_AMOUNT_IN_CHANGE:

        // DHA 16-MAY-2016
        case CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_GAMINGTABLE:
        case CASHIER_MOVEMENT.FILLER_OUT_CLOSING_STOCK:

        // DHA 13-JUL-2016
        case CASHIER_MOVEMENT.CHIPS_SALE_TOTAL_EXCHANGE:
        case CASHIER_MOVEMENT.CHIPS_PURCHASE_TOTAL_EXCHANGE:

        // DHA 27-JUL-2016
        case CASHIER_MOVEMENT.CAGE_CASHIER_COLLECT_DROPBOX_GAMINGTABLE_DETAIL:

        case CASHIER_MOVEMENT.CREDIT_LINE_MARKER:

        case CASHIER_MOVEMENT.CHIPS_SALE_REMAINING_AMOUNT:
        case CASHIER_MOVEMENT.CHIPS_SALE_CONSUMED_REMAINING_AMOUNT:

        // EOR 26-JUN-2017
        case CASHIER_MOVEMENT.TAX_CUSTODY:
        // DPC 14-JUN-2018 >> TITO ticket created in kiosk redemption do not have to be considered as physical money
        case CASHIER_MOVEMENT.TITO_TICKET_COUNTR_PRINTED_CASHABLE:
          _balance_increment = 0;
          break;

        // YNM 14-APR-2016
        case CASHIER_MOVEMENT.CREDIT_LINE_PAYBACK:
          _balance_increment = AddAmount;
          break;

        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_CASHABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_PROMO_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_HANDPAY:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_PAID_JACKPOT:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_CASHABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_PROMO_REDEEMABLE:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_HANDPAY:
        case CASHIER_MOVEMENT.TITO_TICKET_CASHIER_EXPIRED_PAID_JACKPOT:
          if (IsCountR)
          {
            _balance_increment = -SubAmount;
          }
          else
          {
            _balance_increment = 0;
          }
          break;

        default:

          if (MovementType > CASHIER_MOVEMENT.MULTIPLE_BUCKETS_END_SESSION && MovementType <= CASHIER_MOVEMENT.MULTIPLE_BUCKETS_MANUAL_SET_LAST)
          {
            _balance_increment = 0;
          }
          else
          {
            _balance_increment = AddAmount - SubAmount;
          }
          break;
      }

      return _balance_increment;
    } // GetCashierMovementBalanceIncrement

  } // CashierMovementsTable
}
