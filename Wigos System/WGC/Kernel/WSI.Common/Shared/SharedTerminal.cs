//-------------------------------------------------------------------
// Copyright © 2012 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   SharedTerminal.cs
// DESCRIPTION:   
// AUTHOR:        Marcos Piedra Osuna
// CREATION DATE: 18-JUL-2012
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 18-JUL-2012  MPO    Initial version
// 26-OCT-2013  JRM    Added TITO parameters to getParameters if terminal is on TITO mode
// 15-NOV-2013  NMR    Machine_ID disappear from parameters
// 27-NOV-2013  NMR    Fixed error when loading TITO parameters
// 29-NOV-2013  NMR    Fixed error when loading TITO parameters
// 30-MAR-2015  DHA    Added function to set terminal display name by a mask (General Param)
// 03-JUN-2015  FAV    Added function to get the Communication type by default for a Terminal Type
// 11-JUN-2015  JPJ    Added new information in the Terminal Information object
// 04-FEB-2016  ETP    Fixed bug 8976 added new Trx_GetTerminalInfoByExternalId 
// 17-AUG-2016  FAV    Product Backlog Item 16835: Add SAS Host Id
// 26-AGO-2016  PDM    PBI 16148:WebService FBM - Adaptar lógica a los métodos de FBM
// 18-OCT-2016  FAV    PBI 20407: TITO Ticket: Calculated fields
// 17-JAN-2017  JMM    PBI 19744:Pago automático de handpays
// 22-MAY-2017  JMM    Bug 27560:Pago automático de PM's activado indebidamente
// 22-MAY-2017  FOS    Bug 27560:Pago automático de PM's activado indebidamente
// -------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace WSI.Common
{
  public static class Terminal
  {

    #region Class

    public enum CreditsPlayMode
    {
      NotRedeemableLast = 0,   // NR2 -> PromoRE -> RE -> NR1
      PromotionalsFirst = 1,   // NR2 -> NR1 -> PromoRE -> RE
      SasMachine = 2,   // Defined by the SAS Machine
    }

    public class TerminalInfo
    {
      public Int32 TerminalId = 0;
      public Int32 ProvId = 0;
      public TerminalTypes TerminalType = TerminalTypes.UNKNOWN;
      public String Name = "";
      public String ProviderName = "";
      public Boolean Blocked = true;
      public TerminalStatus Status = TerminalStatus.RETIRED;
      public CreditsPlayMode PlayMode = CreditsPlayMode.NotRedeemableLast;
      public Int64 CurrentPlaySessionId = -1;
      public Int64 CurrentAccountId = 0;
      public Int32 SASFlags = 0;

      public Decimal PvPointsMultiplier = 0;
      public Boolean PvSiteJackpot = true;
      public Boolean PvOnlyRedeemable = false;

      public Boolean SASFlagActivated(SAS_FLAGS Flag)
      {
        return Terminal.SASFlagActived(this.SASFlags, Flag);
      }

      // TITO Params
      // Tito mode parameters
      //public bool TitoMode;
      public int SystemId;
      public Int64 SequenceId;
      public Int64 MachineId;
      public Boolean AllowCashableTicketOut;
      public Boolean AllowPromotionalTicketOut;
      public Boolean AllowTicketIn;
      public Int32 ExpirationTicketsCashable;
      public Int32 ExpirationTicketsPromotional;
      public String TitoTicketLocalization;
      public String TitoTicketAddress_1;
      public String TitoTicketAddress_2;
      public String TitoTicketTitleRestricted;
      public String TitoTicketTitleDebit;
      public Int64 TicketsMaxCreationAmount;
      public String ExternalId;
      public String FloorId;
      public Int32 BankId;
      public Int32 BankAreaId;
      public String BankName;
      public String CurrencyCode;
      public Int32 HostId;
      public String AreaName;
      
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// 
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean Trx_IsT3STerminal(Int32 TerminalId, SqlTransaction Trx)
    {
      Object _obj;
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.Length = 0;

        _sb.AppendLine(" SELECT   COUNT(*) ");
        _sb.AppendLine("   FROM   TERMINALS_3GS ");
        _sb.AppendLine("   INNER JOIN TERMINALS ON  T3GS_TERMINAL_ID  = TE_TERMINAL_ID");
        _sb.AppendLine("  WHERE ");
        _sb.AppendLine("  T3GS_TERMINAL_ID  = @TerminalId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@TerminalId", SqlDbType.Int).Value = TerminalId;

          _obj = _cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            Int32 _value;
            _value = (Int32)_obj;

            return (_value > 0);
          }

        }
      }
      catch
      { }

      return false;

    }

    // PURPOSE: Retrieve the terminal id according to  vendor id and the serial number
    //
    //  PARAMS:
    //     - INPUT:
    //           - VendorId:
    //           - SerialNumber:
    //           - MachineNumber:
    //           - TerminalId:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //      - true: Failed retrieving 
    //      - false: error.

    public static Boolean Trx_GetTerminalInfo(String VendorId,
                                              String SerialNumber,
                                              Int32 MachineNumber,
                                              out TerminalInfo Terminal,
                                              SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;

      Terminal = new TerminalInfo();

      try
      {
        _sb = new StringBuilder();
        _sb.Length = 0;

        _sb.AppendLine(" SELECT   @IgnoreMachineNumber = CAST(GP_KEY_VALUE AS int) ");
        _sb.AppendLine("   FROM   GENERAL_PARAMS ");
        _sb.AppendLine("  WHERE   GP_GROUP_KEY = 'Interface3GS' AND GP_SUBJECT_KEY ='IgnoreMachineNumber' ");

        _sb.AppendLine(" SELECT   TE_TERMINAL_ID ");
        _sb.AppendLine("   FROM   TERMINALS_3GS, TERMINALS ");
        _sb.AppendLine("  WHERE   T3GS_VENDOR_ID            = @pVendorId ");
        _sb.AppendLine("          AND ( T3GS_SERIAL_NUMBER  = @pSerialNumber  OR @IgnoreSerialNumber  = 1 ) ");
        _sb.AppendLine("          AND ( T3GS_MACHINE_NUMBER = @pMachineNumber OR @IgnoreMachineNumber = 1 ) ");
        _sb.AppendLine("          AND T3GS_TERMINAL_ID      = TE_TERMINAL_ID ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {

          _cmd.Parameters.Add("@pVendorId", SqlDbType.NVarChar).Value = VendorId;
          _cmd.Parameters.Add("@pSerialNumber", SqlDbType.NVarChar).Value = (SerialNumber == null ? "" : SerialNumber);
          _cmd.Parameters.Add("@IgnoreSerialNumber", SqlDbType.Int).Value = (SerialNumber == null);
          _cmd.Parameters.Add("@pMachineNumber", SqlDbType.Int).Value = MachineNumber;
          _cmd.Parameters.Add("@IgnoreMachineNumber", SqlDbType.Int).Value = 0;

          _obj = _cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            Int32 _terminal_id;

            _terminal_id = (Int32)_obj;

            return Trx_GetTerminalInfo(_terminal_id, out Terminal, Trx);
          }

          if (SerialNumber != null) // zsp_AccountStatus doesn't send the SerialNumber, so don't insert in pendings.
          {
            Trx_InsertPendingTerminal(0, VendorId, SerialNumber, MachineNumber, Trx);
          }

          return true;
        }
      }
      catch
      { }

      return false;

    } // Trx_GetTerminalId
    // PURPOSE: If exists the terminal it update the table "TERMINALS_PENDING", otherwise insert into "TERMINALS_PENDING" 
    //
    //  PARAMS:
    //     - INPUT:
    //           - Source:
    //           - VendorId:
    //           - SerialNumber:
    //           - MachineNumber:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //      - true: Fail updating TERMINALS_PENDING
    //      - false: error.

    public static Boolean Trx_InsertPendingTerminal(Int32 Source,
                                                    String VendorId,
                                                    String SerialNumber,
                                                    Int32 MachineNumber,
                                                    SqlTransaction Trx)
    {
      StringBuilder _sb;

      try
      {

        _sb = new StringBuilder();
        _sb.Length = 0;

        _sb.AppendLine(" IF NOT EXISTS ");
        _sb.AppendLine(" ( ");
        _sb.AppendLine(" SELECT   1 ");
        _sb.AppendLine("   FROM   TERMINALS_PENDING ");
        _sb.AppendLine("  WHERE   TP_SOURCE         = @pSource ");
        _sb.AppendLine("    AND   TP_VENDOR_ID      = @pVendorId ");
        _sb.AppendLine("    AND   TP_SERIAL_NUMBER  = @pSerialNumber ");
        _sb.AppendLine(" ) ");

        _sb.AppendLine("   INSERT INTO TERMINALS_PENDING ( TP_SOURCE ");
        _sb.AppendLine("                                 , TP_VENDOR_ID ");
        _sb.AppendLine("                                 , TP_SERIAL_NUMBER ");
        _sb.AppendLine("                                 , TP_MACHINE_NUMBER ) ");
        _sb.AppendLine("                          VALUES ( @pSource ");
        _sb.AppendLine("                                 , @pVendorId ");
        _sb.AppendLine("                                 , @pSerialNumber ");
        _sb.AppendLine("                                 , @pMachineNumber ) ");

        _sb.AppendLine(" ELSE ");

        _sb.AppendLine("   UPDATE   TERMINALS_PENDING ");
        _sb.AppendLine("      SET   TP_MACHINE_NUMBER = @pMachineNumber ");
        _sb.AppendLine("    WHERE   TP_SOURCE         = @pSource ");
        _sb.AppendLine("      AND   TP_VENDOR_ID      = @pVendorId ");
        _sb.AppendLine("      AND   TP_SERIAL_NUMBER  = @pSerialNumber ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pSource", SqlDbType.Int).Value = Source;
          _cmd.Parameters.Add("@pVendorId", SqlDbType.NVarChar).Value = VendorId;
          _cmd.Parameters.Add("@pSerialNumber", SqlDbType.NVarChar).Value = SerialNumber;
          //Parameter for insert or update
          _cmd.Parameters.Add("@pMachineNumber", SqlDbType.Int).Value = MachineNumber;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }

          return true;

        }

      }
      catch
      { }

      return false;

    } // Trx_InsertPendingTerminal

    // PURPOSE: Retrieve data of terminal
    //
    //  PARAMS:
    //     - INPUT:
    //           - TerminalId:
    //           - TerminalName:
    //           - OutTerminalInfo:
    //           - Trx:
    //     - OUTPUT:
    //           - none
    //
    // RETURNS:
    //      - true: Fail in retrieve data of db.
    //      - false: error.
    public static Boolean Trx_GetTerminalInfo(Int32 TerminalId,
                                              out TerminalInfo OutTerminalInfo,
                                              SqlTransaction Trx)
    {
      return Trx_GetTerminalInfo(TerminalId, "", "", out OutTerminalInfo, Trx);
    }

    public static Boolean Trx_GetTerminalInfo(String TerminalName,
                                              out TerminalInfo OutTerminalInfo,
                                              SqlTransaction Trx)
    {
      return Trx_GetTerminalInfo(0, TerminalName, "", out OutTerminalInfo, Trx);
    }

    public static Boolean Trx_GetTerminalInfoByExternalId(String TerminalExternalId,
                                             out TerminalInfo OutTerminalInfo,
                                             SqlTransaction Trx)
    {
      return Trx_GetTerminalInfo(0, "", TerminalExternalId, out OutTerminalInfo, Trx);
    }

    private static Boolean Trx_GetTerminalInfo(Int32 TerminalId,
                                               String TerminalName,
                                               String TerminalExternalId,
                                               out TerminalInfo OutTerminalInfo,
                                               SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _sas_flags_terminal;
      Int32 _sas_flags_terminal_use_site_default;
      Int32 _sas_flags_site_default;

      if (!Int32.TryParse(MultiPromos.ReadGeneralParams("SasHost", "SystemDefaultSasFlags", Trx), out _sas_flags_site_default))
      {
        _sas_flags_site_default = 0;
      }

      OutTerminalInfo = new TerminalInfo();
      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("   SELECT   TE_TERMINAL_ID                                         "); // 0
        _sb.AppendLine("          , TE_PROV_ID                                             "); // 1
        _sb.AppendLine("          , TE_TERMINAL_TYPE                                       "); // 2
        _sb.AppendLine("          , TE_NAME                                                "); // 3
        _sb.AppendLine("          , TE_PROVIDER_ID                                         "); // 4
        _sb.AppendLine("          , TE_BLOCKED                                             "); // 5
        _sb.AppendLine("          , TE_STATUS                                              "); // 6
        _sb.AppendLine("          , TE_CURRENT_PLAY_SESSION_ID                             "); // 7
        _sb.AppendLine("          , TE_CURRENT_ACCOUNT_ID                                  "); // 8
        _sb.AppendLine("          , PV_POINTS_MULTIPLIER                                   "); // 9
        _sb.AppendLine("          , CAST(ISNULL ( ( SELECT   1                             ");
        _sb.AppendLine("                         FROM   TERMINAL_GROUPS                    ");
        _sb.AppendLine("                        WHERE   TG_ELEMENT_TYPE = @pElementType    ");
        _sb.AppendLine("                          AND   TG_TERMINAL_ID = TE_TERMINAL_ID    ");
        _sb.AppendLine("                          AND   TG_ELEMENT_ID = 1 ), 0 ) AS BIT)   "); // Element Id 1 is JackPot Generic
        _sb.AppendLine("  	        AS SITE_JACKPOT                                        "); // 10                        
        _sb.AppendLine("          , PV_ONLY_REDEEMABLE                                     "); // 11
        _sb.AppendLine("          , TE_SAS_FLAGS                                           "); // 12
        _sb.AppendLine("          , TE_SAS_FLAGS_USE_SITE_DEFAULT                          "); // 13
        _sb.AppendLine("          , ISNULL ( ( SELECT   CAST (GP_KEY_VALUE AS INT)         ");
        _sb.AppendLine("   	                     FROM   GENERAL_PARAMS                     ");
        _sb.AppendLine("   	                    WHERE   GP_GROUP_KEY   = 'Credits'         ");
        _sb.AppendLine("   	                      AND   GP_SUBJECT_KEY = 'PlayMode'        ");
        _sb.AppendLine("  	                      AND   ISNUMERIC(GP_KEY_VALUE) = 1 ), 0 ) ");
        _sb.AppendLine("  	        AS SYSTEM_PLAY_MODE                                    "); // 14        
        _sb.AppendLine("  	      , TE_EXTERNAL_ID                                         "); // 15
        _sb.AppendLine("  	      , TE_FLOOR_ID                                            "); // 16
        _sb.AppendLine("          , BK_NAME AS BANK                                        "); // 17 (ISLA)
        _sb.AppendLine("          , TE_ISO_CODE                                            "); // 18
        _sb.AppendLine("          , TE_TITO_HOST_ID AS HOST_ID                             "); // 19
        _sb.AppendLine("          , AR_NAME AS AREA                                        "); // 20
        _sb.AppendLine("          , TE_BANK_ID AS BANK_ID                                  "); // 21
        _sb.AppendLine("          , BK_AREA_ID AS BANK_AREA_ID                             "); // 22
        _sb.AppendLine("     FROM   TERMINALS                                              ");
        _sb.AppendLine("LEFT  JOIN  PROVIDERS ON PV_ID = TE_PROV_ID                        ");
        _sb.AppendLine("INNER JOIN  BANKS ON BK_BANK_ID = TE_BANK_ID                       ");
        _sb.AppendLine("INNER JOIN  AREAS ON BK_AREA_ID = AR_AREA_ID                       ");

        if (!String.IsNullOrEmpty(TerminalName))
        {
          _sb.AppendLine("  WHERE   TE_NAME = @pTerminalName ");
        }
        else
        {
          if (!String.IsNullOrEmpty(TerminalExternalId))
          {
            _sb.AppendLine("  WHERE   TE_EXTERNAL_ID = @pTerminalExternalId ");
          }
          else
          {
            _sb.AppendLine("  WHERE   TE_TERMINAL_ID = @pTerminalId ");
          }
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pElementType", SqlDbType.Int).Value = EXPLOIT_ELEMENT_TYPE.JACKPOT;

          if (!String.IsNullOrEmpty(TerminalName))
          {
            _cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar, 50).Value = TerminalName;
          }
          else
          {
            if (!String.IsNullOrEmpty(TerminalExternalId))
            {
              _cmd.Parameters.Add("@pTerminalExternalId", SqlDbType.NVarChar, 50).Value = TerminalExternalId;
            }
            else
            {
              _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
            }
          }

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (!_reader.Read())
            {
              return false;
            }

            OutTerminalInfo.TerminalId = _reader.GetInt32(0);

            if (!_reader.IsDBNull(1))
            {
              OutTerminalInfo.ProvId = _reader.GetInt32(1);
            }

            OutTerminalInfo.TerminalType = (TerminalTypes)_reader.GetInt16(2);
            OutTerminalInfo.Name = _reader.GetString(3);
            OutTerminalInfo.ProviderName = _reader.GetString(4);
            OutTerminalInfo.Blocked = _reader.GetBoolean(5);
            OutTerminalInfo.Status = (TerminalStatus)_reader.GetInt32(6);
            OutTerminalInfo.CurrentPlaySessionId = _reader.IsDBNull(7) ? 0 : _reader.GetInt64(7);
            OutTerminalInfo.CurrentAccountId = _reader.IsDBNull(8) ? 0 : _reader.GetInt64(8);
            OutTerminalInfo.PvPointsMultiplier = Math.Max(0, _reader.GetDecimal(9));
            OutTerminalInfo.PvSiteJackpot = _reader.GetBoolean(10);
            OutTerminalInfo.PvOnlyRedeemable = _reader.GetBoolean(11);

            _sas_flags_terminal = _reader.GetInt32(12);
            _sas_flags_terminal_use_site_default = _reader.GetInt32(13);

            OutTerminalInfo.SASFlags = _sas_flags_site_default & _sas_flags_terminal_use_site_default
                                       |
                                       _sas_flags_terminal & ~_sas_flags_terminal_use_site_default;

            // Maintain value for SAS_FLAGS_RESERVE_TERMINAL, ignore the site default value
            if (WSI.Common.Terminal.SASFlagActived(_sas_flags_terminal, SAS_FLAGS.SAS_FLAGS_RESERVE_TERMINAL))
            {
              OutTerminalInfo.SASFlags |= (Int32)SAS_FLAGS.SAS_FLAGS_RESERVE_TERMINAL;
            }

            OutTerminalInfo.PlayMode = (CreditsPlayMode)_reader.GetInt32(14);
            OutTerminalInfo.ExternalId = _reader.IsDBNull(15) ? String.Empty : _reader.GetString(15);
            OutTerminalInfo.FloorId = _reader.IsDBNull(16) ? String.Empty : _reader.GetString(16);
            OutTerminalInfo.BankName = _reader.GetString(17);
            OutTerminalInfo.CurrencyCode = _reader.GetString(18);
            OutTerminalInfo.HostId = _reader.GetInt32(19);
            OutTerminalInfo.AreaName = _reader.IsDBNull(20) ? String.Empty : _reader.GetString(20);
            OutTerminalInfo.BankId = _reader.IsDBNull(21) ? 0 : _reader.GetInt32(21);
            OutTerminalInfo.BankAreaId= _reader.IsDBNull(22) ? 0 : _reader.GetInt32(22);


            switch (OutTerminalInfo.PlayMode)
            {
              case CreditsPlayMode.NotRedeemableLast:
              case CreditsPlayMode.PromotionalsFirst:
                break;

              default:
                OutTerminalInfo.PlayMode = CreditsPlayMode.NotRedeemableLast;
                break;
            }

            if (OutTerminalInfo.PlayMode == CreditsPlayMode.PromotionalsFirst)
            {
              if (OutTerminalInfo.TerminalType == TerminalTypes.SAS_HOST)
              {
                if (OutTerminalInfo.SASFlagActivated(SAS_FLAGS.CREDITS_PLAY_MODE_SAS_MACHINE))
                {
                  OutTerminalInfo.PlayMode = CreditsPlayMode.SasMachine;
                }
              }
            }
          }
        }
        return true;

      }
      catch
      { }

      return false;
    } // Trx_TerminalInfo

    //------------------------------------------------------------------------------
    // PURPOSE : Get display name from a Terminal/Session of the DataTable.
    //
    //  PARAMS :
    //      - INPUT :
    //          - Int32  TerminalId
    //
    //      - OUTPUT :
    //          - String Display Name
    //
    // RETURNS :
    //      - Boolean: If data has been retrieved.
    //
    public static Boolean GetTerminalDisplayName(Int32 TerminalId, String MaskName, out String DisplayName, SqlTransaction Trx)
    {
      StringBuilder _sb;
      String _terminal_name;
      String _terminal_base_name;
      String _treminal_floor_id;
      String _mask_name;

      _mask_name = MaskName;

      _terminal_name = String.Empty;
      _terminal_base_name = String.Empty;
      _treminal_floor_id = String.Empty;
      DisplayName = String.Empty;

      _sb = new StringBuilder();
      _sb.AppendLine("  SELECT   TE_NAME           ");
      _sb.AppendLine("         , TE_BASE_NAME      ");
      _sb.AppendLine("         , TE_FLOOR_ID       ");
      _sb.AppendLine("    FROM   TERMINALS         ");
      _sb.AppendLine("   WHERE   TE_TERMINAL_ID   = @pTerminaId ");

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminaId", SqlDbType.BigInt).Value = TerminalId;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            while (_reader.Read())
            {
              _terminal_name = _reader.IsDBNull(0) ? "" : _reader.GetString(0);
              _terminal_base_name = _reader.IsDBNull(1) ? "" : _reader.GetString(1);
              _treminal_floor_id = _reader.IsDBNull(2) ? "" : _reader.GetString(2);
            }
          }
        }

        _mask_name = _mask_name.Replace("%Name", _terminal_name);
        _mask_name = _mask_name.Replace("%BaseName", _terminal_base_name);
        _mask_name = _mask_name.Replace("%FloorId", _treminal_floor_id);

        DisplayName = _mask_name;

        return true;
      }
      catch
      {
      }

      return false;
    } // GetTerminalDisplayName

    public static Boolean SASFlagActived(SAS_FLAGS Flag, Int32 TerminalId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _terminal_sas_flags;
      Int32 _use_site_sas_flags;
      Int32 _site_sas_flags;

      _sb = new StringBuilder();

      _sb.AppendLine("   SELECT   TE_SAS_FLAGS                  "); // 0
      _sb.AppendLine("          , TE_SAS_FLAGS_USE_SITE_DEFAULT "); // 1
      _sb.AppendLine("     FROM   TERMINALS                     ");
      _sb.AppendLine("    WHERE   TE_TERMINAL_ID = @pTerminalId ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

        using (SqlDataReader _reader = _cmd.ExecuteReader())
        {
          if (!_reader.Read())
          {
            return false;
          }

          _terminal_sas_flags = _reader.GetInt32(_reader.GetOrdinal("TE_SAS_FLAGS"));
          _use_site_sas_flags = _reader.GetInt32(_reader.GetOrdinal("TE_SAS_FLAGS_USE_SITE_DEFAULT"));
        }
      }

      if (Terminal.SASFlagActived(_use_site_sas_flags, Flag))
      {
        // Use system defined

        if (!Int32.TryParse(MultiPromos.ReadGeneralParams("SasHost", "SystemDefaultSasFlags", Trx), out _site_sas_flags))
      {
          _site_sas_flags = 0;
      }
        return Terminal.SASFlagActived(_site_sas_flags, Flag);
      }

      // Terminal defined
      return Terminal.SASFlagActived(_terminal_sas_flags, Flag);
    }

    public static Boolean SASFlagActived(Int32 CurrentFlags, SAS_FLAGS Flag)
    {
      return ((CurrentFlags & (Int32)Flag) == (Int32)Flag);
    }

    // PURPOSE : Insert or update GAME_METERS for every play
    //
    //  PARAMS :
    //      - INPUT :
    //          - (I/O) ReportPlays
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :

    public static SqlCommand GetGameMetersUpdateCommand(SqlTransaction Trx)
    {
      StringBuilder _sb;
      SqlCommand _cmd;

      // Insert or Update command
      _sb = new StringBuilder();
      _sb.AppendLine("IF NOT EXISTS ( ");
      _sb.AppendLine("      SELECT   1 ");
      _sb.AppendLine("        FROM   GAME_METERS ");
      _sb.AppendLine("       WHERE   GM_TERMINAL_ID    = @pTerminalId ");
      _sb.AppendLine("         AND   GM_GAME_BASE_NAME = @pGameBaseName ");
      _sb.AppendLine("      ) ");
      _sb.AppendLine("   INSERT INTO   GAME_METERS ");
      _sb.AppendLine("               ( GM_TERMINAL_ID ");
      _sb.AppendLine("               , GM_GAME_BASE_NAME ");
      _sb.AppendLine("               , GM_WCP_SEQUENCE_ID ");
      _sb.AppendLine("               , GM_DENOMINATION ");
      _sb.AppendLine("               , GM_PLAYED_COUNT ");
      _sb.AppendLine("               , GM_PLAYED_AMOUNT ");
      _sb.AppendLine("               , GM_WON_COUNT ");
      _sb.AppendLine("               , GM_WON_AMOUNT ");
      _sb.AppendLine("               , GM_JACKPOT_AMOUNT ");
      _sb.AppendLine("               , GM_DELTA_GAME_NAME ");
      _sb.AppendLine("               , GM_DELTA_PLAYED_COUNT ");
      _sb.AppendLine("               , GM_DELTA_PLAYED_AMOUNT ");
      _sb.AppendLine("               , GM_DELTA_WON_COUNT ");
      _sb.AppendLine("               , GM_DELTA_WON_AMOUNT ");
      _sb.AppendLine("               , GM_LAST_REPORTED ");
      _sb.AppendLine("               ) ");
      _sb.AppendLine("        VALUES ");
      _sb.AppendLine("               ( @pTerminalId ");
      _sb.AppendLine("               , @pGameBaseName ");
      _sb.AppendLine("               , @pWcpSequenceId ");
      _sb.AppendLine("               , @pDenomination ");
      _sb.AppendLine("               , @pDeltaPlayedCount ");
      _sb.AppendLine("               , @pDeltaPlayedAmount ");
      _sb.AppendLine("               , @pDeltaWonCount ");
      _sb.AppendLine("               , @pDeltaWonAmount ");
      _sb.AppendLine("               , @pDeltaJackpotAmount ");
      _sb.AppendLine("               , @pGameBaseName ");
      _sb.AppendLine("               , @pDeltaPlayedCount ");
      _sb.AppendLine("               , @pDeltaPlayedAmount ");
      _sb.AppendLine("               , @pDeltaWonCount ");
      _sb.AppendLine("               , @pDeltaWonAmount ");
      _sb.AppendLine("               , GETDATE() ");
      _sb.AppendLine("               ) ");
      _sb.AppendLine("ELSE ");
      _sb.AppendLine("               ");
      _sb.AppendLine("   UPDATE   GAME_METERS ");
      _sb.AppendLine("      SET   GM_WCP_SEQUENCE_ID     = @pWcpSequenceId ");
      _sb.AppendLine("          , GM_DENOMINATION        = @pDenomination ");
      _sb.AppendLine("          , GM_PLAYED_COUNT        = GM_PLAYED_COUNT         + @pDeltaPlayedCount ");
      _sb.AppendLine("          , GM_PLAYED_AMOUNT       = GM_PLAYED_AMOUNT        + @pDeltaPlayedAmount ");
      _sb.AppendLine("          , GM_WON_COUNT           = GM_WON_COUNT            + @pDeltaWonCount ");
      _sb.AppendLine("          , GM_WON_AMOUNT          = GM_WON_AMOUNT           + @pDeltaWonAmount ");
      _sb.AppendLine("          , GM_DELTA_GAME_NAME     = @pGameBaseName ");
      _sb.AppendLine("          , GM_DELTA_PLAYED_COUNT  = GM_DELTA_PLAYED_COUNT   + @pDeltaPlayedCount ");
      _sb.AppendLine("          , GM_DELTA_PLAYED_AMOUNT = GM_DELTA_PLAYED_AMOUNT  + @pDeltaPlayedAmount ");
      _sb.AppendLine("          , GM_DELTA_WON_COUNT     = GM_DELTA_WON_COUNT      + @pDeltaWonCount ");
      _sb.AppendLine("          , GM_DELTA_WON_AMOUNT    = GM_DELTA_WON_AMOUNT     + @pDeltaWonAmount ");
      _sb.AppendLine("          , GM_JACKPOT_AMOUNT      = GM_JACKPOT_AMOUNT       + @pDeltaJackpotAmount ");
      _sb.AppendLine("          , GM_LAST_REPORTED       = GETDATE() ");
      _sb.AppendLine("    WHERE   GM_TERMINAL_ID         = @pTerminalId ");
      _sb.AppendLine("      AND   GM_GAME_BASE_NAME      = @pGameBaseName ");

      _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx);

      //_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).SourceColumn = "BU_COLUMN_TERMINAL_ID";
      //_cmd.Parameters.Add("@pGameBaseName", SqlDbType.NVarChar, 50).SourceColumn = "BU_COLUMN_GAME_NAME";
      //_cmd.Parameters.Add("@pDenomination", SqlDbType.Money).SourceColumn = "BU_COLUMN_DENOMINATION";
      //_cmd.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).SourceColumn = "BU_COLUMN_WCP_SEQUENCE_ID";
      //_cmd.Parameters.Add("@pDeltaPlayedCount", SqlDbType.BigInt).Value = 1;
      //_cmd.Parameters.Add("@pDeltaPlayedAmount", SqlDbType.Money).SourceColumn = "BU_COLUMN_PLAYED_AMOUNT";
      //_cmd.Parameters.Add("@pDeltaWonCount", SqlDbType.BigInt).SourceColumn = "BU_COLUMN_WON_COUNT";
      //_cmd.Parameters.Add("@pDeltaWonAmount", SqlDbType.Money).SourceColumn = "BU_COLUMN_WON_AMOUNT";
      //_cmd.Parameters.Add("@pDeltaJackpotAmount", SqlDbType.Money).Value = 0;

      return _cmd;

    } // Cmd_InsertOrUpdateGameMeters

    public static Boolean CloseAllButNewerOpenCashierSessions(Int32 TerminalId, SqlTransaction Trx)
    {
      Boolean _result;
      StringBuilder _sb;

      _result = false;
      _sb = new StringBuilder();

      _sb.AppendLine("  UPDATE   CASHIER_SESSIONS                                                                     ");
      _sb.AppendLine("     SET   CS_STATUS = @pPendingClosingStatus                                                   ");
      _sb.AppendLine("   WHERE   CS_CASHIER_ID = (                                                                    ");
      _sb.AppendLine("                            SELECT   CT_CASHIER_ID                                              ");
      _sb.AppendLine("                              FROM   CASHIER_TERMINALS                                          ");
      _sb.AppendLine("                             WHERE   CT_TERMINAL_ID = @pTerminalId                              ");
      _sb.AppendLine("                           )                                                                    ");
      _sb.AppendLine("     AND   CS_STATUS = @pOpenStatus                                                             ");
      _sb.AppendLine("     AND   CS_SESSION_ID <> (                                                                   ");
      _sb.AppendLine("                               SELECT   TOP 1 CS_SESSION_ID                                     ");
      _sb.AppendLine("                                 FROM   CASHIER_SESSIONS                                        ");
      _sb.AppendLine("                                WHERE   CS_CASHIER_ID = (                                       ");
      _sb.AppendLine("                                                         SELECT   CT_CASHIER_ID                 ");
      _sb.AppendLine("                                                           FROM   CASHIER_TERMINALS             ");
      _sb.AppendLine("                                                          WHERE   CT_TERMINAL_ID = @pTerminalId ");
      _sb.AppendLine("                                                        )                                       ");
      _sb.AppendLine("                                  AND   CS_STATUS = 0                                           ");
      _sb.AppendLine("                             ORDER BY   CS_OPENING_DATE DESC                                    ");
      _sb.AppendLine("                            )                                                                   ");

      try
      {
        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPendingClosingStatus", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.PENDING_CLOSING;
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
          _cmd.Parameters.Add("@pOpenStatus", SqlDbType.Int).Value = CASHIER_SESSION_STATUS.OPEN;

          _cmd.ExecuteNonQuery();

          _result = true;
        }
      }
      catch (Exception)
      {
        _result = false;
      }

      return _result;
    }

#if !SQL_BUSINESS_LOGIC && !SQL_WIGOS_BL02
    //------------------------------------------------------------------------------
    // PURPOSE : Get the Communication type by default for a Terminal Type
    //  PARAMS :
    //      - INPUT :
    //          - TerminalTypes:  Terminal Type
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - SMIB_COMMUNICATION_TYPE: Communication type by default
    //
    public static SMIB_COMMUNICATION_TYPE GetDefaultCommunicationTypeByTerminalType(TerminalTypes TerminalType)
    {
      switch (TerminalType)
      {
        case TerminalTypes.WIN:
          return SMIB_COMMUNICATION_TYPE.WCP;
        case TerminalTypes.SAS_HOST:
          return SMIB_COMMUNICATION_TYPE.SAS;
        default:
          return SMIB_COMMUNICATION_TYPE.NONE;
      }
    }

#endif

#if !SQL_BUSINESS_LOGIC && !SQL_WIGOS_BL02
    //------------------------------------------------------------------------------
    // PURPOSE : Get the active Terminal by SAS Host Id
    //  PARAMS :
    //      - INPUT :
    //          - HostId
    //
    //      - OUTPUT :
    //          - TerminalId
    //
    // RETURNS:
    //      - true: Failed retrieving 
    //      - false: error.
    public static Boolean Trx_GetActiveTerminalByHostId(Int32 HostId, SqlTransaction Trx, out Int32 TerminalId)
    {
      TerminalId = 0;
      StringBuilder _sb;
      Object _obj;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT     TE_TERMINAL_ID                ");
        _sb.AppendLine("  FROM     TERMINALS                     ");
        _sb.AppendLine(" WHERE     TE_TITO_HOST_ID = @pHostId    ");
        _sb.AppendLine("   AND     TE_TYPE = 1                   ");
        _sb.AppendLine("   AND     ISNULL(TE_SERVER_ID, 0) =  0  ");
        _sb.AppendLine("   AND     TE_BLOCKED = 0                ");
        _sb.AppendLine("   AND     TE_STATUS  = 0                ");

#if !SQL_BUSINESS_LOGIC
        _sb.AppendLine("   AND     TE_TERMINAL_TYPE IN ( " + Misc.GamingTerminalTypeListToString() + " ) ");
#else
        //TerminalTypes.WIN, TerminalTypes.T3GS, TerminalTypes.SAS_HOST, TerminalTypes.CASHDESK_DRAW, TerminalTypes.OFFLINE, TerminalTypes.GAMEGATEWAY 
        _sb.AppendLine("   AND     TE_TERMINAL_TYPE IN ( 1,3,5,106,108,109,110 ) ");
#endif

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pHostId", SqlDbType.Int).Value = HostId;

          _obj = _sql_cmd.ExecuteScalar();
          if (_obj != null && _obj != DBNull.Value)
          {
            TerminalId = (Int32)_obj;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Trx_GetExistsHostId 
#endif

    #endregion

    public static Boolean Trx_GetTerminalType(Int32 TerminalId, SqlTransaction Trx, out TerminalTypes TerminalType)
    {
      TerminalType = TerminalTypes.UNKNOWN;

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand("SELECT TE_TERMINAL_TYPE FROM TERMINALS WHERE TE_TERMINAL_ID = @pTerminalId", Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          TerminalType = (TerminalTypes)((Int16)_sql_cmd.ExecuteScalar());

          return true;
        }
      }
#if !SQL_BUSINESS_LOGIC
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
#else
      catch
      {
        ;
      }
#endif

      return false;
    } // Trx_GetTerminalType 

#if !SQL_BUSINESS_LOGIC
    // PURPOSE : Gets the protocol that terminal must connect (WCP, SAS, PULSES, ...)
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalID
    //          - SqlTrx
    //
    //      - OUTPUT :
    //          - SmibCommType
    //          - SmibConfId
    //
    // RETURNS :
    //
    //   NOTES :

    public static Boolean Trx_GetTerminalSmibProtocol(Int32 TerminalId, TerminalTypes TerminalType, SqlTransaction Trx, out SMIB_COMMUNICATION_TYPE SmibCommType, out Int64 SmibConfId)
    {
      SmibCommType = SMIB_COMMUNICATION_TYPE.NONE;
      SmibConfId = 0;

      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand("SELECT TE_SMIB2EGM_COMM_TYPE, TE_SMIB2EGM_CONF_ID FROM TERMINALS WHERE TE_TERMINAL_ID = @pTerminalId", Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              // ACC 22-JUL-2015 Default Smib Protocol for SasHost is SAS.
              if (_reader.IsDBNull(0) || (Int32)_reader[0] < 0)
              {
                SmibCommType = (TerminalType == TerminalTypes.SAS_HOST) ? SMIB_COMMUNICATION_TYPE.SAS : SMIB_COMMUNICATION_TYPE.NONE;
              }
              else
              {
                SmibCommType = (SMIB_COMMUNICATION_TYPE)_reader[0];
              }

              SmibConfId = _reader.IsDBNull(1) ? 0 : (Int64)_reader[1];
            }

            return true;
          }
        }
      }

      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // Trx_GetTerminalType  
#endif
  }
}
