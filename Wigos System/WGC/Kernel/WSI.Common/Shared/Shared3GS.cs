//-------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   Shared3GS.cs
// DESCRIPTION:   
// AUTHOR:        Andreu Julia & Raul Cervera
// CREATION DATE: 23-JUL-2012
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
// 23-JUL-2012  AJQ    Initial version
// 04-MAR-2013  AMF    Mapping events 17 & 18.
// 20-AGO-2016  PDM    PBI 16148:WebService FBM - Adaptar l�gica a los m�todos de FBM
// 15-FEB-2017  AMF    PBI 24612:WebService FBM: Nuevo Evento inserci�n de billetes
// 08-JUN-2017  XGJ    WIGOS-2752 FBM. Create two new events (Handpay - Cancel Credits, Handpay - Jackpot)
// 23-JUN-2017  FJC    WIGOS-3133 WS2S_StartCardSession
// 26-JUN-2017  OMC    WIGOS-3141 WS2S_EndCardSession
// 12-JUL-2017 DPC     PBI 16148:WebService FBM - Adaptar l�gica a los m�todos de FBM
// -------------------------------------------------------------------

using System;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using WSI.Common;

namespace WSI.Common
{
  public static class T3GS
  {
    public static MultiPromos.StartSessionOutput Start(String VendorId, String SerialNumber, Int32 MachineNumber,
                                                       String ExternalTrackData,
                                                       SqlTransaction Trx)
    {
      MultiPromos.StartSessionOutput _output;
      Terminal.TerminalInfo _terminal;
      MultiPromos.AccountInfo _account;

      _output = new MultiPromos.StartSessionOutput();

      if (!MultiPromos.Trx_UpdateAccountBalance(0, ExternalTrackData, MultiPromos.AccountBalance.Zero, 0, false, out _account, Trx))
      {
        _output.SetError(MultiPromos.StartSessionStatus.AccountUnknown, _account.ErrorMessage);

        return _output;
      }

      if (!Terminal.Trx_GetTerminalInfo(VendorId, SerialNumber, MachineNumber, out _terminal, Trx))
      {
        _output.SetError(MultiPromos.StartSessionStatus.TerminalUnknown, "");

        return _output;
      }

      Trx.Save("T3GS.Start");

      _output = MultiPromos.Trx_CommonStartCardSession(0, _terminal, _account, false, VendorId, Trx);

      if (_output.StatusCode != MultiPromos.StartSessionStatus.Ok)
      {
        Trx.Rollback("T3GS.Start");
      }

      //JBC 30-01-2015 BirthDay alarm feature
      MultiPromos.CheckBirthdayAlarm(_account.AccountId, _account.HolderName, _terminal.MachineId + " - " + _terminal.Name, TerminalTypes.T3GS, Trx);

      return _output;
    } // Start

    public static MultiPromos.EndSessionOutput Update(String VendorId, String SerialNumberDummy, Int32 MachineNumberDummy,
                                                      String ExternalTrackDataDummy,
                                                      Int64 PlaySessionId,
                                                      MultiPromos.PlayedWonMeters PlaySessionMeters,
                                                      Decimal FinalBalance,
                                                      SqlTransaction Trx)
    {
      MultiPromos.EndSessionOutput _output;
      MultiPromos.AccountInfo _account;
      Int64 _account_id;
      Int32 _terminal_id;

      _output = new MultiPromos.EndSessionOutput();
      _account_id = 0;
      _terminal_id = 0;

      Trx.Save("T3GS.Update");

      try
      {

        if (!T3GS.DB_CheckIfExistsVendorId(VendorId, Trx))
        {
          _output.SetError(MultiPromos.EndSessionStatus.FatalError, "");

          return _output;
        }

        if (!DB_GetAccountAndTerminalIdByPlaySessionId(PlaySessionId, VendorId, out _account_id, out _terminal_id, Trx))
        {
          _output.SetError(MultiPromos.EndSessionStatus.FatalError, "");

          return _output;
        }

        if (!MultiPromos.Trx_UpdateAccountBalance(_account_id, ExternalTrackDataDummy, MultiPromos.AccountBalance.Zero, 0, false, out _account, Trx))
        {
          _output.SetError(MultiPromos.EndSessionStatus.AccountUnknown, _account.ErrorMessage);

          return _output;
        }

        _output = Update_Internal(_terminal_id, _account, PlaySessionId, PlaySessionMeters, FinalBalance, Trx);

        return _output;
      }
      finally
      {
        if (_output.StatusCode != MultiPromos.EndSessionStatus.Ok
            && _output.StatusCode != MultiPromos.EndSessionStatus.BalanceMismatch)
        {
          Trx.Rollback("T3GS.Update");
        }
      }
    } // Update


    public static MultiPromos.EndSessionOutput End(String VendorId, String SerialNumberDummy, Int32 MachineNumberDummy,
                                                   String ExternalTrackDataDummy,
                                                   Int64 PlaySessionId,
                                                   MultiPromos.PlayedWonMeters PlaySessionMeters,
                                                   Decimal FinalBalance,
                                                   SqlTransaction Trx)
    {
      MultiPromos.EndSessionOutput _output;
      Terminal.TerminalInfo _terminal;
      MultiPromos.AccountInfo _account;
      MultiPromos.EndSessionInput _input;
      Int64 _account_id;
      Int32 _terminal_id_dummy;
      PlaySessionStatus _status;

      _output = new MultiPromos.EndSessionOutput();
      _account_id = 0;
      _terminal_id_dummy = 0;

      Trx.Save("T3GS.End");

      try

      {
        if (!T3GS.DB_CheckIfExistsVendorId(VendorId, Trx))
        {
          _output.SetError(MultiPromos.EndSessionStatus.FatalError, "");

          return _output;
        }

        if (!DB_GetAccountAndTerminalIdByPlaySessionId(PlaySessionId, VendorId, out _account_id, out _terminal_id_dummy, Trx))
        {
          _output.SetError(MultiPromos.EndSessionStatus.FatalError, "");

          return _output;
        }

        if (!MultiPromos.Trx_UpdateAccountBalance(_account_id, ExternalTrackDataDummy, MultiPromos.AccountBalance.Zero, 0, false, out _account, Trx))
        {
          _output.SetError(MultiPromos.EndSessionStatus.AccountUnknown, _account.ErrorMessage);

          return _output;
        }

        if (_account.CurrentPlaySessionStatus == PlaySessionStatus.Closed && FinalBalance == 0)
        {
          if (!getPlaySessionStatus(PlaySessionId, Trx, out _status))
          {
            _output.SetError(MultiPromos.EndSessionStatus.InvalidPlaySession, "");
          }

          if (_status == PlaySessionStatus.ManualClosed)
          {
            _output.SetError(MultiPromos.EndSessionStatus.Ok, "This session has been closed manually.");
            return _output;
          }
        }

        if (!Terminal.Trx_IsT3STerminal(_account.CurrentTerminalId, Trx))
        {
          _output.SetError(MultiPromos.EndSessionStatus.TerminalUnknown, "");

          return _output;
        }

        if (!Terminal.Trx_GetTerminalInfo(_account.CurrentTerminalId, out _terminal, Trx))
        {
          _output.SetError(MultiPromos.EndSessionStatus.TerminalUnknown, "");

          return _output;
        }

        _output = Update_Internal(_account.CurrentTerminalId, _account, PlaySessionId, PlaySessionMeters, FinalBalance, Trx);

        if (_output.StatusCode != MultiPromos.EndSessionStatus.Ok
            && _output.StatusCode != MultiPromos.EndSessionStatus.BalanceMismatch)
        {
          return _output;
        }

        _input = new MultiPromos.EndSessionInput();
        _input.AccountId = _account.AccountId;
        _input.BalanceFromGM = FinalBalance;
        _input.HasMeters = true;
        _input.Meters = PlaySessionMeters;
        _input.MovementType = MovementType.EndCardSession;
        _input.PlaySessionId = PlaySessionId;
        _input.TransactionId = 0;

        if (!MultiPromos.Trx_OnEndCardSession(_terminal.TerminalId, _terminal.TerminalType, _terminal.Name, _input, VendorId, out _output, Trx))
        {
          _output.SetError(MultiPromos.EndSessionStatus.FatalError, "Trx_OnEndCardSession failed!");

          return _output;
        }

        return _output;
      }
      finally
      {
        if (_output.StatusCode != MultiPromos.EndSessionStatus.Ok
            && _output.StatusCode != MultiPromos.EndSessionStatus.BalanceMismatch)
        {
          Trx.Rollback("T3GS.End");
        }
      }
    } // End

    public static MultiPromos.StartSessionOutput AccountStatus(String ExternalTrackData,
                                                               String VendorId, Int32 MachineNumberDummy,
                                                               SqlTransaction Trx)
    {
      MultiPromos.StartSessionOutput _output;
      MultiPromos.AccountInfo _account;

      _output = new MultiPromos.StartSessionOutput();

      Trx.Save("T3GS.AccountStatus");

      try
      {
        if (!T3GS.DB_CheckIfExistsVendorId(VendorId, Trx))
        {
          _output.SetError(MultiPromos.StartSessionStatus.Error, "");

          return _output;
        }

        if (!MultiPromos.Trx_UpdateAccountBalance(0, ExternalTrackData, MultiPromos.AccountBalance.Zero, 0, false, out _account, Trx))
        {
          _output.SetError(MultiPromos.StartSessionStatus.AccountUnknown, _account.ErrorMessage);

          return _output;
        }
        if (_account == null || _account.AccountId == 0)
        {
          _output.SetError(MultiPromos.StartSessionStatus.AccountUnknown, "");

          return _output;
        }
        if (_account.Blocked)
        {
          _output.SetError(MultiPromos.StartSessionStatus.AccountBlocked, "");

          return _output;
        }

        // S2S
        //if (!Terminal.Trx_GetTerminalInfo(VendorId, null, MachineNumberDummy, out _terminal, Trx))
        //{
        //  _output.SetError(MultiPromos.StartSessionStatus.TerminalUnknown, "Trx_GetTerminalInfo failed!");

        //  return _output;
        //}
        //if (_terminal == null || _terminal.TerminalId == 0)
        //{
        //  _output.SetError(MultiPromos.StartSessionStatus.TerminalUnknown, "");

        //  return _output;
        //}


        if (_account.CurrentPlaySessionId != 0)
        {
          _output.PlaySessionId = _account.CurrentPlaySessionId;
          _output.SetError(MultiPromos.StartSessionStatus.AccountInSession, "");

          return _output;
        }

        _output.FinBalance = _account.FinalBalance;
        _output.SetSuccess();

        return _output;
      }
      finally
      {
        if (_output.StatusCode != MultiPromos.StartSessionStatus.Ok)
        {
          Trx.Rollback("T3GS.AccountStatus");
        }
      }
    } // AccountStatus

    public static MultiPromos.StartSessionOutput SendEvent(String ExternalTrackData,
                                                           String VendorId, String SerialNumber, Int32 MachineNumber,
                                                           Int64 EventId,
                                                           Decimal Amount,
                                                           SqlTransaction Trx)
    {

      MultiPromos.StartSessionOutput _output;
      Terminal.TerminalInfo _terminal;
      MultiPromos.AccountInfo _account;
      Int32 _operation_code;
      StringBuilder _sb;
      Int64 _play_session_id;

      _output = new MultiPromos.StartSessionOutput();

      Trx.Save("T3GS.SendEvent");

      try
      {
        if (!Terminal.Trx_GetTerminalInfo(VendorId, SerialNumber, MachineNumber, out _terminal, Trx))
        {
          _output.SetError(MultiPromos.StartSessionStatus.TerminalUnknown, "");

          return _output;
        }
        if (_terminal == null || _terminal.TerminalId == 0)
        {
          _output.SetError(MultiPromos.StartSessionStatus.TerminalUnknown, "");

          return _output;
        }

        switch (EventId)
        {
          case 1:
            _operation_code = 16;
            break;

          case 2:
            _operation_code = 17;
            break;

          case 3:
            _operation_code = 18;
            break;

          case 4:
            _operation_code = 68;
            break;

          // RCI & AJQ 01-MAR-2013: Accept all documented operation codes.
          //                        But, for now, do nothing with these (do not insert in EVENT_HISTORY table).
          case 0:
          case 29:
            _output.SetSuccess();

            return _output;

          case 17:
            _operation_code = 1;
            break;

          case 18:
            _operation_code = 2;
            break;

          //  XGJ 08-JUN-2017: WIGOS-2752 FBM. Create two new events (Handpay - Cancel Credits, Handpay - Jackpot)
          case 10:
            _operation_code = 68;
            break;

          case 11:
            _operation_code = 69;
            break;

          default:
            _output.SetError(MultiPromos.StartSessionStatus.Error, "Unknown EventId.");

            return _output;
        }

        // Don't check error status. For SendEvent, the account is not important.
        MultiPromos.Trx_UpdateAccountBalance(0, ExternalTrackData, MultiPromos.AccountBalance.Zero, 0, false, out _account, Trx);

        _play_session_id = 0;
        if (_account != null && _account.CurrentPlaySessionId != 0)
        {
          _play_session_id = _account.CurrentPlaySessionId;
        }

        _sb = new StringBuilder();
        _sb.AppendLine(" INSERT INTO EVENT_HISTORY ( EH_TERMINAL_ID ");
        _sb.AppendLine("                           , EH_SESSION_ID ");
        _sb.AppendLine("                           , EH_DATETIME ");
        _sb.AppendLine("                           , EH_EVENT_TYPE ");
        _sb.AppendLine("                           , EH_OPERATION_CODE ");
        _sb.AppendLine("                           , EH_OPERATION_DATA) ");
        _sb.AppendLine("                    VALUES ( @pTerminalId ");
        _sb.AppendLine("                           , @pPlaySessionId ");
        _sb.AppendLine("                           , GETDATE() ");
        _sb.AppendLine("                           , @pEventType ");
        _sb.AppendLine("                           , @pOperationCode ");
        _sb.AppendLine("                           , @pAmount) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = _terminal.TerminalId;
          _cmd.Parameters.Add("@pPlaySessionId", SqlDbType.Int).Value = _play_session_id;
          _cmd.Parameters.Add("@pEventType", SqlDbType.Int).Value = 2;
          _cmd.Parameters.Add("@pOperationCode", SqlDbType.Int).Value = _operation_code;
          _cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = Amount;

          if (_cmd.ExecuteNonQuery() != 1)
          {
            _output.SetError(MultiPromos.StartSessionStatus.Error, "Error on insert event_history!");

            return _output;
          }
        }

        _output.SetSuccess();

        return _output;
      }
      finally
      {
        if (_output.StatusCode != MultiPromos.StartSessionStatus.Ok)
        {
          Trx.Rollback("T3GS.SendEvent");
        }
      }
    } // SendEvent

    // 08-JUN-2017 XGJ    WIGOS-2752 FBM. Create two new events (Handpay - Cancel Credits, Handpay - Jackpot)
    public static MultiPromos.StartSessionOutput Insert_Handpay(String VendorId, String SerialNumber, Int32 MachineNumber,
                                      Decimal HandPay,
                                      Int64 EventId,
                                      SqlTransaction Trx)
    {
      MultiPromos.StartSessionOutput _output;
      StringBuilder _sb;
      Terminal.TerminalInfo _terminal;
      int _hp_type;

      _hp_type = (EventId == 10) ? 0 : 1;

      _output = new MultiPromos.StartSessionOutput();
      if (!Terminal.Trx_GetTerminalInfo(VendorId, SerialNumber, MachineNumber, out _terminal, Trx))
      {
        _output.SetError(MultiPromos.StartSessionStatus.TerminalUnknown, "");

        return _output;
      }

      _sb = new StringBuilder();
      _sb.AppendLine("INSERT INTO HANDPAYS  ( HP_TERMINAL_ID,                       ");
      _sb.AppendLine("                        HP_AMOUNT,                            ");
      _sb.AppendLine("                        HP_TE_NAME,                           ");
      _sb.AppendLine("                        HP_TYPE,                              ");
      _sb.AppendLine("                        HP_TE_PROVIDER_ID)                    ");
      _sb.AppendLine("               VALUES ( @pTerminalId,                         ");
      _sb.AppendLine("                        @pAmount,                             ");
      _sb.AppendLine("                        @pTerminalName,                       ");
      _sb.AppendLine("                        @pHpType,                             ");
      _sb.AppendLine("                        (SELECT TE_PROVIDER_ID                ");
      _sb.AppendLine("                        FROM TERMINALS                        ");
      _sb.AppendLine("                        WHERE TE_TERMINAL_ID = @pTerminalId)) ");

      using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
      {
        _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = _terminal.TerminalId;
        _cmd.Parameters.Add("@pAmount", SqlDbType.Money).Value = HandPay;
        _cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar).Value = _terminal.Name;
        _cmd.Parameters.Add("@pHpType", SqlDbType.Int).Value = _hp_type;

        if (_cmd.ExecuteNonQuery() != 1)
        {
          _output.SetError(MultiPromos.StartSessionStatus.Error, "Error on insert handpay!");

          return _output;
        }
      }
      _output.SetSuccess();

      return _output;
    }

    private static MultiPromos.EndSessionOutput Update_Internal(Int32 TerminalId,
                                                                MultiPromos.AccountInfo Account,
                                                                Int64 PlaySessionId,
                                                                MultiPromos.PlayedWonMeters PlaySessionMeters,
                                                                Decimal FinalBalance,
                                                                SqlTransaction Trx)
    {
      MultiPromos.EndSessionOutput _output;
      MultiPromos.PlayedWonMeters _delta;
      Decimal _calculated;
      Decimal _effective_final_balance;
      Decimal _maximum_balance_error;
      Decimal _diff;
      Boolean _mismatch;
      Boolean _big_mismatch;

      _output = new MultiPromos.EndSessionOutput();

      if (Account == null || Account.AccountId == 0)
      {
        _output.SetError(MultiPromos.EndSessionStatus.AccountUnknown, "");

        return _output;
      }

      if (Account.CurrentPlaySessionId != PlaySessionId
          || Account.CurrentPlaySessionStatus != PlaySessionStatus.Opened)
      {
        _output.SetError(MultiPromos.EndSessionStatus.InvalidPlaySession,
                         String.Format("Invalid PlaySessionId. Reported: {0}, Current: {1}, Status: {2}",
                                        PlaySessionId.ToString(),
                                        Account.CurrentPlaySessionId.ToString(),
                                        Account.CurrentPlaySessionStatus.ToString()));
        return _output;
      }

      if (FinalBalance < 0
          || PlaySessionMeters.PlayedAmount < 0
          || PlaySessionMeters.PlayedCount < 0
          || PlaySessionMeters.WonAmount < 0
          || PlaySessionMeters.WonCount < 0
          || PlaySessionMeters.BillInAmount < 0)
      {
        _output.SetError(MultiPromos.EndSessionStatus.TerminalBlocked,
                         String.Format("Negative Meters. Final: {0}; Played: {1}, #{2}; Won: {3}, #{4}",
                                        FinalBalance.ToString(),
                                        PlaySessionMeters.PlayedAmount.ToString(),
                                        PlaySessionMeters.PlayedCount.ToString(),
                                        PlaySessionMeters.WonAmount.ToString(),
                                        PlaySessionMeters.WonCount.ToString()));
        return _output;
      }

      _effective_final_balance = Math.Max(0, FinalBalance);
      _calculated = Math.Max(0, Account.CurrentPlaySessionInitialBalance - PlaySessionMeters.PlayedAmount + PlaySessionMeters.WonAmount + PlaySessionMeters.BillInAmount);
      _diff = FinalBalance - _calculated;
      _mismatch = (_diff != 0);
      _big_mismatch = false;

      if (_mismatch)
      {
        Decimal.TryParse(MultiPromos.ReadGeneralParams("Interface3GS", "MaximumBalanceErrorCents", Trx), out _maximum_balance_error);
        _maximum_balance_error = _maximum_balance_error / 100;

        if (Math.Abs(_diff) > _maximum_balance_error)
        {
          // Big Balance Mismatch 
          _big_mismatch = true;
          _effective_final_balance = Math.Max(0, Math.Min(_calculated, FinalBalance));
        }
      }

      if (!MultiPromos.Trx_UpdatePlaySessionPlayedWon(TerminalTypes.T3GS, PlaySessionId, PlaySessionMeters, out _delta, Trx))
      {
        _output.SetError(MultiPromos.EndSessionStatus.FatalError, "Trx_UpdatePlaySessionPlayedWon failed!");

        return _output;
      }

      if (!MultiPromos.Trx_PlaySessionSetFinalBalance(PlaySessionId, _effective_final_balance, FinalBalance, _mismatch, MovementType.EndCardSession, Trx))
      {
        _output.SetError(MultiPromos.EndSessionStatus.FatalError, "Trx_PlaySessionSetFinalBalance failed!");

        return _output;
      }

      using (SqlCommand _cmd = WSI.Common.Terminal.GetGameMetersUpdateCommand(Trx))
      {
        _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
        _cmd.Parameters.Add("@pGameBaseName", SqlDbType.NVarChar, 50).Value = "GAME_3GS";
        _cmd.Parameters.Add("@pDenomination", SqlDbType.Money).Value = 0.01;
        _cmd.Parameters.Add("@pWcpSequenceId", SqlDbType.BigInt).Value = 0;
        _cmd.Parameters.Add("@pDeltaPlayedCount", SqlDbType.BigInt).Value = _delta.PlayedCount;
        _cmd.Parameters.Add("@pDeltaPlayedAmount", SqlDbType.Money).Value = _delta.PlayedAmount;
        _cmd.Parameters.Add("@pDeltaWonCount", SqlDbType.BigInt).Value = _delta.WonCount;
        _cmd.Parameters.Add("@pDeltaWonAmount", SqlDbType.Money).Value = _delta.WonAmount;
        _cmd.Parameters.Add("@pDeltaJackpotAmount", SqlDbType.Money).Value = 0;

        if (_cmd.ExecuteNonQuery() != 1)
        {
          _output.SetError(MultiPromos.EndSessionStatus.FatalError, "GameMetersUpdate failed!");

          return _output;
        }
      }

      if (_big_mismatch)
      {
        _output.SetError(MultiPromos.EndSessionStatus.BalanceMismatch,
                         String.Format("Balance Mismatch: Reported={0}, Calculated={1}, Final={2}",
                                        FinalBalance.ToString(), _calculated.ToString(), _effective_final_balance.ToString()));
        return _output;
      }

      _output.SetSuccess();

      return _output;
    } // Update_Internal

    public static void Translate(MultiPromos.StartSessionOutput StartSession, out int StatusCode, out String StatusText, out String ErrorText)
    {
      ErrorText = StartSession.Message;

      switch (StartSession.StatusCode)
      {
        case MultiPromos.StartSessionStatus.Ok:
          StatusCode = 0;
          StatusText = "Success";
          ErrorText = "";
          break;

        case MultiPromos.StartSessionStatus.AccountUnknown:
          StatusCode = 1;
          StatusText = "Invalid Account Number";
          break;

        case MultiPromos.StartSessionStatus.AccountInSession:
          StatusCode = 2;
          StatusText = "Account In Session";
          break;

        case MultiPromos.StartSessionStatus.TerminalUnknown:
        case MultiPromos.StartSessionStatus.TerminalBlocked:
          StatusCode = 3;
          StatusText = "Invalid Machine Information";
          break;

        case MultiPromos.StartSessionStatus.AccountBlocked:
        case MultiPromos.StartSessionStatus.Error:
        default:
          StatusCode = 4;
          StatusText = "Access Denied";
          break;
      } // switch (StartSession.StatusCode)
    } // Translate

    public static void Translate(MultiPromos.EndSessionOutput EndSession, out int StatusCode, out String StatusText, out String ErrorText)
    {
      ErrorText = EndSession.Message;

      switch (EndSession.StatusCode)
      {
        case MultiPromos.EndSessionStatus.Ok:
          StatusCode = 0;

          if (ErrorText == String.Empty)
          {
            StatusText = "Success";
          }
          else
          {
            StatusText = ErrorText;
          }

          ErrorText = "";
          break;

        case MultiPromos.EndSessionStatus.AccountUnknown:
          StatusCode = 1;
          StatusText = "Invalid Account Number";
          break;

        case MultiPromos.EndSessionStatus.TerminalUnknown:
        case MultiPromos.EndSessionStatus.TerminalBlocked:
          StatusCode = 2;
          StatusText = "Invalid Machine Information";
          break;

        case MultiPromos.EndSessionStatus.InvalidPlaySession:
          StatusCode = 3;
          StatusText = "Invalid Session ID Number";
          break;

        case MultiPromos.EndSessionStatus.ErrorRetry:
        case MultiPromos.EndSessionStatus.FatalError:
        case MultiPromos.EndSessionStatus.NotOpened:
        case MultiPromos.EndSessionStatus.AccountBlocked:
        default:
          StatusCode = 4;
          StatusText = "Access Denied";
          break;

        case MultiPromos.EndSessionStatus.BalanceMismatch:
          StatusCode = 5;
          StatusText = "Final balance does not match validation.";
          break;
      } // switch (EndSession.StatusCode)
    } // Translate

    #region Common changes FBM pending if has to be here

    /// <summary>
    /// Check if FBM is enabled and the session is open
    /// </summary>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean DB_CheckFBMSession(SqlTransaction Trx)
    {
      StringBuilder _sb;
      Int32 _status = 0; // LogStatus.Close;
      Int32 _block = 2; // LogStatus.Blocked;
      Int32 _FBM_status;

      try
      {
        Int32.TryParse(MultiPromos.ReadGeneralParams("FBM", "Enabled", Trx), out _FBM_status);

        if (_FBM_status != 1)
        {
          return true;
        }

        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT ( SELECT TOP 1   FL_STATUS                           ");
        _sb.AppendLine("                  FROM   FBM_LOG                             ");
        _sb.AppendLine("                 WHERE   FL_TYPE in (0,1)                    ");
        _sb.AppendLine("              ORDER BY   FL_CREATION desc) as SessionStatus, ");
        _sb.AppendLine("        ( SELECT TOP 1   FL_STATUS                           ");
        _sb.AppendLine("                  FROM   FBM_LOG                             ");
        _sb.AppendLine("                 WHERE   FL_TYPE IN (2,3)                    ");
        _sb.AppendLine("              ORDER BY   FL_CREATION desc) AS MachineStatus  ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          using (SqlDataReader _reader = _sql_cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              _status = (_reader.IsDBNull(_reader.GetOrdinal("SessionStatus"))) ? 0 : (Int32)_reader["SessionStatus"];
              _block = (_reader.IsDBNull(_reader.GetOrdinal("MachineStatus"))) ? 2 : (Int32)_reader["MachineStatus"];
            }
          }
        }

        return _status == 1 && _block == 3;
      }
      catch { }

      return false;
    } // DB_CheckFBMSession

    /// <summary>
    /// check if vendorId exists in table TERMINALS_3GS
    /// </summary>
    /// <param name="VendorId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean DB_CheckIfExistsVendorId(String VendorId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;

      _obj = null;
      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" SELECT  TOP 1 T3GS_VENDOR_ID         ");
        _sb.AppendLine("   FROM  TERMINALS_3GS                ");
        _sb.AppendLine("  WHERE  T3GS_VENDOR_ID = @pVendorId  ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pVendorId", SqlDbType.NVarChar).Value = VendorId;

          _obj = _cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            return true;
          }
        }
      }
      catch { }

      return false;
    } // DB_CheckIfExistsVendorId

    /// <summary>
    /// Get AccountId and TerminalId from PlaySessionId
    /// Verify the VendorId of the terminal associated
    /// </summary>
    /// <param name="PlaySessionId"></param>
    /// <param name="VendorId"></param>
    /// <param name="AccountId"></param>
    /// <param name="TerminalId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean DB_GetAccountAndTerminalIdByPlaySessionId(Int64 PlaySessionId, String VendorId, out Int64 AccountId, out Int32 TerminalId, SqlTransaction Trx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      AccountId = 0;
      TerminalId = 0;

      try
      {
        _sb.AppendLine("     SELECT   PS_ACCOUNT_ID                        ");
        _sb.AppendLine("            , PS_TERMINAL_ID                       ");
        _sb.AppendLine("       FROM   PLAY_SESSIONS                        ");
        _sb.AppendLine(" INNER JOIN   TERMINALS_3GS                        ");
        _sb.AppendLine("         ON   T3GS_TERMINAL_ID   = PS_TERMINAL_ID  ");
        _sb.AppendLine("        AND   T3GS_VENDOR_ID     = @pVendorId      ");
        _sb.AppendLine("      WHERE   PS_PLAY_SESSION_ID = @pPlaysessionId ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pPlaysessionId", SqlDbType.BigInt).Value = PlaySessionId;
          _cmd.Parameters.Add("@pVendorId", SqlDbType.NVarChar).Value = VendorId;

          using (SqlDataReader _reader = _cmd.ExecuteReader())
          {
            if (_reader.Read())
            {
              AccountId = _reader.GetInt64(0);
              TerminalId = _reader.GetInt32(1);

              return true;
            }
          }
        }
      }
      catch { }

      return false;
    } // DB_GetAccountAndTerminalIdByPlaySessionId
    #endregion

    private static Boolean getPlaySessionStatus(Int64 PlaySessionId, SqlTransaction Trx, out PlaySessionStatus Status)
    {

      StringBuilder _sb;
      Object _obj;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   PS_STATUS                        ");
        _sb.AppendLine("   FROM   PLAY_SESSIONS                    ");
        _sb.AppendLine("  WHERE   PS_PLAY_SESSION_ID = @pSessionID ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pSessionID", SqlDbType.BigInt).Value = PlaySessionId;

          _obj = _cmd.ExecuteScalar();

          Status = (PlaySessionStatus)_obj;
        }

        return true;

      }
      catch { }

      Status = PlaySessionStatus.Closed;
      return false;
    }

  }
}
