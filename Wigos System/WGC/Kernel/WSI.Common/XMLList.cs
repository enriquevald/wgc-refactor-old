﻿//------------------------------------------------------------------------------
// Copyright © 2015 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: XMLList.cs  
// 
//   DESCRIPTION: XMLList class
//
//        AUTHOR: Francisco Alejandro Vicente
// 
// CREATION DATE: 18-AUG-2015
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ---------- ------------------------------------------------------
// 21-JAN-2016 FAV        First release.
//------------------------------------------------------------------------------ 

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace WSI.Common
{
  public static class XMLList
  {
    #region Public Methods

    /// <summary>
    /// Converts a List to a XML string
    /// </summary>
    /// <param name="FillAmount"></param>
    /// <returns></returns>
    public static String ListToXML<T>(List<T> List)
    {
      SerializableList<T> _serializable_list = ConvertListToSerializable(List);
      var _serializer = new XmlSerializer(typeof(SerializableList<T>));
      string _xmlString;

      using (var _sw = new StringWriter())
      {
        using (var _writer = new XmlTextWriter(_sw))
        {
          _writer.Formatting = Formatting.Indented; 
          _serializer.Serialize(_writer, _serializable_list);
          _writer.Flush();
          _xmlString = _sw.ToString();
        }
      }
      return _xmlString;
    }

    /// <summary>
    ///  Converts a XML string to a List
    /// </summary>
    /// <param name="XMLString"></param>
    /// <returns></returns>
    public static List<T> XMLToList<T>(string XMLString)
    {
      XmlNodeReader _reader = null;
      XmlDocument _doc = null;
      var _serializer = new XmlSerializer(typeof(SerializableList<T>));

      _doc = new XmlDocument();
      _doc.LoadXml(XMLString);

      _reader = new XmlNodeReader(_doc.DocumentElement);

      return ConvertSerializableListToList((SerializableList<T>)_serializer.Deserialize(_reader));
    }

    #endregion

    #region Private Methods

    private static SerializableList<T> ConvertListToSerializable<T>(List<T> List)
    {
      SerializableList<T> _serializable_list = new SerializableList<T>();
      foreach (T entry in List)
      {
        _serializable_list.Add(entry);
      }

      return _serializable_list;
    }

    private static List<T> ConvertSerializableListToList<T>(SerializableList<T> SerializableList)
    {
      List<T> _sorted_list = new List<T>();
      foreach (T entry in SerializableList)
      {
        _sorted_list.Add(entry);
      }

      return _sorted_list;
    }

    #endregion
  }

  /// <summary>
  /// SerializableList class, this class implements IXmlSerializable that is necesary to work with serialization
  /// </summary>
  /// <typeparam name="TValue"></typeparam>
  [XmlRoot("List")]
  public class SerializableList<TValue> : List<TValue>, IXmlSerializable
  {
    public void ReadXml(System.Xml.XmlReader reader)
    {
      XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));
      bool wasEmpty = reader.IsEmptyElement;
      reader.Read();
      if (wasEmpty)
        return;

      while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
      {
        reader.ReadStartElement("item");
        TValue value = (TValue)valueSerializer.Deserialize(reader);
        reader.ReadEndElement();
        this.Add(value);
        reader.MoveToContent();
      }

      reader.ReadEndElement();
    }

    public void WriteXml(System.Xml.XmlWriter writer)
    {
      XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));
      foreach (TValue value in this)
      {
        writer.WriteStartElement("item");
        valueSerializer.Serialize(writer, value);
        writer.WriteEndElement();
      }
    }

    public System.Xml.Schema.XmlSchema GetSchema()
    {
      return null;
    }
  }
}
