//-------------------------------------------------------------------
// Copyright � 2013 Win Systems Ltd.
//-------------------------------------------------------------------
//
// MODULE NAME:   ValidateFormat.cs
// DESCRIPTION:   
// AUTHOR:        Marcos Piedra Osuna
// CREATION DATE: 8-FEB-2013
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ -----------------------------------------------
//  8-FEB-2013  MPO    Create empty class
// 11-FEB-2013  JMM    Fill class functions
// 06-FEB-2013  LEM    Added new constants YEAR_MAX_INTERVAL_ALLOWED and MINIMUM_MARRIAGE_AGE
// 11-MAR-2013  LEM    Fixed Bug #700: Character '@' is not required on Twitter Account
// 30-MAY-2013  JCA    Added new function to validate providers name
// 20-AUG-2013  CCG    Added validate if IsEmpty to Phone fields
// 21-AUG-2013  MMG    Aded new function: ValidateNumber_AlsoDecimalsAndNegatives
// 05-SEP-2013  ICS    Moved ValidateNumber_AlsoDecimalsAndNegatives to new function Number. Renamed function Numeric to RawNumber.
// 09-DEC-2013  QMP    Added IsGenericRFC function
// 20-JUN-2014  LEM    Fixed Bug WIGOSTITO-1241: Wrong RFC validation
// 03-NOV-2014  DHA    Fixed Bug WIG-1616: if RFC is generic validate country matches
// 04-NOV-2014  DHA    Fixed Bug WIG-1639: if RFC is generic and is anonymous account, not validate country
// 21-DEC-2016  ATB    Fixed Bug 21843: Error generating RFC at main screen v03.004.0048
// -------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace WSI.Common
{
  public class ValidateFormat
  {
    public const Int32 YEAR_MAX_INTERVAL_ALLOWED = 150;
    public const Int32 MINIMUM_MARRIAGE_AGE = 13;
    public const String RFC_REGEX = "[A-Z,�,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?";
    public const Int32 MIN_LENGHT_RFC = 10;
    public const Int32 MAX_LENGHT_RFC = 13;
    public const String RFC_GENERIC_01 = "XAXX010101000"; // Mexican
    public const String RFC_GENERIC_02 = "XEXX010101000"; // Foreign

    #region " Private Functions "

    //------------------------------------------------------------------------------
    //PURPOSE: Counts the numeric digits that are contained on a string
    // PARAMS:
    //    - INPUT:
    //          - Value As String
    //    - OUTPUT:
    //          - none
    //
    //RETURNS:
    //    - The number of numeric digits contained on a string
    private static int CountNumericDigits(string Value)
    {
      int _total_numeric_digits;

      _total_numeric_digits = 0;

      foreach (char _char in Value)
      {
        //only numeric digits are counted
        if (char.IsDigit(_char))
        {
          _total_numeric_digits++;
        }
      }

      return _total_numeric_digits;
    }

    //------------------------------------------------------------------------------
    //PURPOSE: Check if a string contains any No-Phone chars (Phone chars are 0..9, +, -, (, ) and blank space)
    // PARAMS:
    //    - INPUT:
    //          - Value As String
    //    - OUTPUT:
    //          - none
    //
    //RETURNS:
    //    - True:  Value contains any No-Phone chars
    //    - False: Otherwise
    private static bool ContainsNoPhoneChars(string Value)
    {
      foreach (char _char in Value)
      {
        if (!CheckPhoneNumber(_char))
        {
          return true;
        }
      }

      return false;
    }

    //------------------------------------------------------------------------------
    //PURPOSE: Check if a string contains a correctly balanced parentheses
    // PARAMS:
    //    - INPUT:
    //          - Value As String
    //    - OUTPUT:
    //          - none
    //
    //RETURNS:
    //    - True:  Value contains a correctly balanced parentheses
    //    - False: Otherwise
    private static bool IsParenthesesOK(string Value)
    {
      int _parenthesis_count;
      int _pos;

      // Check:
      //  - There isn't an opening-closing parenthesis without any char inside
      //  - All opening parenthesis are matched against a closing one

      //  - There isn't an opening-closing parenthesis without any char inside
      _pos = Value.IndexOf("()", 0);

      if (_pos > -1)
      {
        return false;
      }

      //  - All opening parenthesis are matched against a closing one
      _parenthesis_count = 0;

      foreach (char _char in Value)
      {
        if (_char == '(')
        {
          _parenthesis_count++;
        }

        if (_char == ')')
        {
          _parenthesis_count--;
        }

        // _parenthesis_count is lower than 0 means that a closing parenthesis is on the string 
        // without a previous opening non-closed parenthesis
        if (_parenthesis_count < 0)
        {
          return false;
        }
      }

      // _parenthesis_count > 0 means that there are at least one opening parenthesis with its correspondent closing parenthesis
      if (_parenthesis_count > 0)
      {
        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    //PURPOSE: Check if a text contains any character not allowed
    // PARAMS:
    //    - INPUT:
    //          - Value As String
    //          - NumbersAllowed As Boolean
    //
    //    - OUTPUT:
    //          - none
    //
    //RETURNS:
    //    - True:  Value contains only valid chars
    //    - False: Value contains any invalid char
    private static Boolean CheckText(String Value, Boolean NumbersAllowed)
    {
      foreach (char _char in Value)
      {
        if (!CheckCharacter(_char, NumbersAllowed)) return false;
      }
      return true;
    }


    #endregion // Private Functions

    #region " Public Functions "

    public static Boolean IsValidDate(DateTime DateToValidate)
    {
      DateTime _now;
      DateTime _min_date;
      DateTime _max_date;

      _now = WGDB.Now;

      _min_date = new DateTime(_now.Year, _now.Month, _now.Day).AddYears(-ValidateFormat.YEAR_MAX_INTERVAL_ALLOWED);
      _max_date = new DateTime(_now.Year, _now.Month, _now.Day).AddYears(1);

      if (DateToValidate < _min_date || DateToValidate > _max_date)
      {
        return false;
      }

      return true;
    } // IsValidDate

    //------------------------------------------------------------------------------
    // PURPOSE: Checks if a String is equal to one of the generic RFCs.
    // PARAMS:
    //    - INPUT:
    //          - RFC As String
    //    - OUTPUT:
    //          - none
    //
    //RETURNS:
    //    - TRUE if the string equals one of the generic RFCs.
    public static Boolean IsGenericRFC(String RFC)
    {
      if (RFC == RFC_GENERIC_01 || RFC == RFC_GENERIC_02)
      {
        return true;
      }

      return false;
    }

    //------------------------------------------------------------------------------
    //PURPOSE: Check if a text contains any character not allowed
    // PARAMS:
    //    - INPUT:
    //          - Value As String
    //          - NumbersAllowed As Boolean
    //
    //    - OUTPUT:
    //          - none
    //
    //RETURNS:
    //    - True:  Value contains only valid chars
    //    - False: Value contains any invalid char
    public static Boolean CheckProviderName(String Value)
    {
      foreach (char _char in Value)
      {
        if (!CheckProviderCharacter(_char, true)) return false;
      }
      return true;
    }

    //------------------------------------------------------------------------------
    //PURPOSE: Check if a string contains a numeric value
    // PARAMS:
    //    - INPUT:
    //          - Value As String
    //    - OUTPUT:
    //          - none
    //
    //RETURNS:
    //    - True:  Value contains a a numeric value
    //    - False: Otherwise
    public static Boolean RawNumber(String Value)
    {
      foreach (char _char in Value)
      {
        //only numeric digits
        if (!char.IsDigit(_char))
        {
          return false;
        }
      }

      return true;
    }

    //------------------------------------------------------------------------------
    //PURPOSE: Check if a string contains a valid phone number
    // PARAMS:
    //    - INPUT:
    //          - Value As String
    //    - OUTPUT:
    //          - none
    //
    //RETURNS:
    //    - True:  Value contains a valid phone number
    //    - False: Otherwise
    public static Boolean PhoneNumber(String Value, Boolean IsFilterSearch)
    {
      int _pos;

      // Checks:
      //  - If the field is empty, it's a valid value
      //  - Only numeric digits and some special: '+', '-', '(' and ')'
      //  - Add symbol (+) only able to be at the first position
      //  - Not allowed two consecutive hyphens (--)
      //  - At least 4-numeric-digits length
      //  - All parentheses are balanced and properly nested
      //  - Any hyphen should be between a couple of numeric digits

      // CCG 20-AUG-2013
      //  - If the field is empty, it's a valid value
      if (Value.Trim() == "")
      {
        return true;
      }

      //  - Only numeric digits and some special: '+', '-', '(' and ')'
      if (ContainsNoPhoneChars(Value))
      {
        return false;
      }

      //  - Add symbol (+) only able to be at the first position
      _pos = Value.IndexOf("+", 0);

      while (_pos > -1)
      {
        if (_pos > 0)
        {
          return false;
        }

        _pos = Value.IndexOf("+", _pos + 1);
      }

      //  - Not allowed two consecutive hyphens (--)
      _pos = Value.IndexOf("--");

      if (_pos > -1)
      {
        return false;
      }

      // In search filters, don't check length and parentheses.
      if (IsFilterSearch)
      {
        return true;
      }

      //  - At least 4-numeric-digits length
      if (CountNumericDigits(Value) < 4)
      {
        return false;
      }

      //  - All parentheses are balanced and properly nested
      if (!IsParenthesesOK(Value))
      {
        return false;
      }

      //  - Any hyphen should be between a couple of numeric digits
      _pos = Value.IndexOf("-", 0);

      while (_pos > -1)
      {
        // wrong phone if there is a hyphen on the beginin
        if (_pos == 0)
        {
          return false;
        }

        // there is a hyphen on the string's last position
        if (_pos == Value.Length - 1)
        {
          return false;
        }

        // the digits before and after a hyphen should be a numeric digit
        if (!char.IsDigit(Value[_pos - 1])
         || !char.IsDigit(Value[_pos + 1]))
        {
          return false;
        }

        _pos = Value.IndexOf("-", _pos + 1);
      }

      return true;
    }

    //------------------------------------------------------------------------------
    //PURPOSE: Check if a string contains a valid phone number
    // PARAMS:
    //    - INPUT:
    //          - Value As String
    //    - OUTPUT:
    //          - none
    //
    //RETURNS:
    //    - True:  Value contains a valid phone number
    //    - False: Otherwise
    public static Boolean Email(String Value, Boolean IsFilterSearch)
    {
      return Regex.IsMatch(Value,
        @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
    }

    //------------------------------------------------------------------------------
    //PURPOSE: Check if a string contains a valid trackdata
    // PARAMS:
    //    - INPUT:
    //          - Value As String
    //    - OUTPUT:
    //          - none
    //
    //RETURNS:
    //    - True:  Value contains a valid trackdata
    //    - False: Otherwise
    public static Boolean TrackData(String Value)
    {

      ExternalTrackData _card_track_data;

      _card_track_data = new ExternalTrackData(Value);


      return _card_track_data.IsCorrectTrackData;
    }

    //------------------------------------------------------------------------------
    //PURPOSE: Check if a text doesn't contain any numeric char
    // PARAMS:
    //    - INPUT:
    //          - Value As String
    //    - OUTPUT:
    //          - none
    //
    //RETURNS:
    //    - True:  Value doesn't contain any numeric char
    //    - False: Value contains any numeric char
    // NOTES:
    //    - In Cashier exists a CharacterTextBox with FormatMode = BaseMode that has the same functionality.
    public static Boolean TextWithoutNumbers(String Value)
    {
      // check if Value contains any invalid char
      if (!CheckText(Value, false))
      {
        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    //PURPOSE: Check if a text with numbers is valid
    // PARAMS:
    //    - INPUT:
    //          - Value As String
    //    - OUTPUT:
    //          - none
    //
    //RETURNS:
    //    - True:  Value is valid
    //    - False: Otherwise  
    public static Boolean TextWithNumbers(String Value)
    {
      if (!CheckText(Value, true))
      {
        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    //PURPOSE: Check if an email is valid
    // PARAMS:
    //    - INPUT:
    //          - Value As String
    //    - OUTPUT:
    //          - none
    //
    //RETURNS:
    //    - True:  Value contains a valid email address
    //    - False: Otherwise
    public static Boolean Email(String Value)
    {
      Regex _mail;
      System.Net.Mail.MailAddress _mail_address;

      try
      {
        if (String.IsNullOrEmpty(Value))
        {
          return false;
        }

        _mail = new Regex(@"^([a-z0-9_\-\.]+)@([a-z0-9_\-\.]+)$", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

        if (!_mail.IsMatch(Value))
        {
          return false;
        }

        _mail_address = new System.Net.Mail.MailAddress(Value);
      }
      catch (Exception)
      {
        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    //PURPOSE: Check if an twitter account is valid
    // PARAMS:
    //    - INPUT:
    //          - Value As String
    //    - OUTPUT:
    //          - none
    //
    //RETURNS:
    //    - True:  Value contains a valid twitter account
    //    - False: Otherwise
    public static Boolean Twitter(String Value)
    {
      Regex _twitter_regexp;
      //SJA - 11/03/2016
      //twitter validation was wrong
      // ^\w+$  does not take into account the preceding @ and the need for 1 to 15 chars

      _twitter_regexp = new Regex(@"^@?(\w){1,15}$", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

      try
      {
        //a twitter account is not allowed to contain the words admin or twitter
        if (String.IsNullOrEmpty(Value) || Value.ToUpperInvariant().Contains("TWITTER") || Value.ToUpperInvariant().Contains("ADMIN"))
        {
          return false;
        }

        return _twitter_regexp.IsMatch(Value);
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean CheckCharacter(Char Character, Boolean NumbersAllowed)
    {
      //only allowed chars (any letter, blank space, �, �, comma, point, -, ', &)
      if (!char.IsLetter(Character) && Character != '\b'
        && Character != ' ' && Character != '�' && Character != '�' && Character != ','
        && Character != '.' && Character != '-' && Character != '\'' && Character != '&')
      {
        if (!NumbersAllowed)
        {
          return false;
        }
        else
        {
          if (!char.IsDigit(Character) && Character != '(' && Character != ')' && Character != '/' && Character != '#')
          {
            return false;
          }
        }
      }
      return true;
    }

    //------------------------------------------------------------------------------
    //PURPOSE: Check if a string contains only letters, numbers and  ' - _ # . / ( ) +
    // PARAMS:
    //    - INPUT:
    //          - Value As String
    //    - OUTPUT:
    //          - none
    //
    //RETURNS:
    //    - True:  Value is correct
    //    - False: Otherwise
    public static Boolean CheckProviderCharacter(Char Character, Boolean NumbersAllowed)
    {
      //only allowed chars (any letter, blank space, ' - _ # . / ( ) +)
      if (!char.IsLetter(Character) && Character != '\b'
        && Character != ' ' && Character != '\'' && Character != '-'
        && Character != '_' && Character != '#' && Character != '.' && Character != '/'
        && Character != '(' && Character != ')' && Character != '+')
      {
        if (!NumbersAllowed)
        {
          return false;
        }
        else
        {
          if (!char.IsDigit(Character))
          {
            return false;
          }
        }
      }
      return true;
    }

    public static Boolean CheckPhoneNumber(Char Character)
    {
      //only numeric digits and +, -, (, ) and blank space chars allowed
      if (!char.IsDigit(Character)
        && Character != '+' && Character != '-' && Character != '(' && Character != ')' && Character != ' ' && Character != '\b')
      {
        return false;
      }
      return true;
    }

    public static Boolean CheckEmail(Char Character)
    {
      //only numeric digits and +, -, (, ) and blank space chars allowed
      return !char.IsWhiteSpace(Character);
    }

    //------------------------------------------------------------------------------
    //PURPOSE: Check if a string contains only letters and numbers 
    // PARAMS:
    //    - INPUT:
    //          - Value As String
    //    - OUTPUT:
    //          - none
    //
    //RETURNS:
    //    - True:  Value is correct
    //    - False: Otherwise
    public static Boolean AlphaNumeric(String Value)
    {
      foreach (Char _char in Value)
      {
        if (!char.IsLetterOrDigit(_char))
        {
          return false;
        }
      }

      return true;
    }

    //------------------------------------------------------------------------------
    //PURPOSE: Check if a string contains only letters
    // PARAMS:
    //    - INPUT:
    //          - Value As String
    //    - OUTPUT:
    //          - none
    //
    //RETURNS:
    //    - True:  Value is correct
    //    - False: Otherwise
    public static Boolean Alpha(String Value)
    {
      foreach (Char _char in Value)
      {
        if (!char.IsLetter(_char))
        {
          return false;
        }
      }

      return true;
    } // Alpha

    //------------------------------------------------------------------------------
    //  PURPOSE : Checking the format of the RFC according with the value of "CheckHolderId"(Cashier GP) 
    // 
    //  PARAMS :
    //     - INPUT : RFC, Surname1, Surname2, Name, Birthdate 
    //
    //     - OUTPUT : none
    //
    //RETURNS:
    //    - True:  Format is correct
    //    - False: Otherwise
    public static Boolean CheckRFC(String RFC,
                                    String Surname1,
                                    String Surname2,
                                    String Name,
                                    DateTime BirthDate)
    {
      Int32 _check_mode;
      String _expectedRFC;
      String _aux_string;

      _aux_string = BirthDate.ToString("yyMMdd");

      try
      {
        _check_mode = GeneralParam.GetInt32("Cashier", "CheckHolderId", 1);
        _check_mode = Math.Max(0, _check_mode);
        _check_mode = Math.Min(2, _check_mode);

        switch (_check_mode)
        {
          case 1:
            //_expectedRFC = "----" + _aux_string + "----";

            //       (RFC != _expectedRFC) ||                                       - Date part must be present in the RFC
            if ((RFC.Length < 13) || (RFC.Substring(4, 6) != _aux_string))
            {
              return false;
            }

            break;

          case 2:
            try
            {
              _expectedRFC = WSI.Common.RFC.RFC13(Surname1, Surname2, Name, BirthDate);
            }
            catch
            {
              _expectedRFC = string.Empty;
            }

              // ATB 21-DEC-2016
              // The RFC algorithm do not contemplates the last three digits, these are free
              // XAXX010101000 <-- XAXX010101 it's the mandatory data
            if ((RFC.Length < 13) || RFC.Substring(0, 10) != _expectedRFC.Substring(0, 10))
            {
              return false;
            }

            break;
        }

      }
      catch (Exception Ex)
      {
        Log.Exception(Ex);
      }

      return true;
    } //CheckRFC

    //------------------------------------------------------------------------------
    //  PURPOSE : Checking the format of the CURP according with the RFC  
    // 
    //  PARAMS :
    //     - INPUT : CURP, RFC, Gender (1 Man, 2 Woman)
    //
    //     - OUTPUT : none
    //
    //RETURNS:
    //    - True:  Format is correct
    //    - False: Otherwise
    public static Boolean CheckCURP(String CURP,
                                    String RFC,
                                    Int32 Gender)
    {
      // - CURP Length         - RFC and CURP must partially match (first 10 chars)                 - CURP must contain M for women and H for mentry
        int _check_mode = GeneralParam.GetInt32("Cashier", "CheckHolderId", 1);
        _check_mode = Math.Max(0, _check_mode);
        _check_mode = Math.Min(2, _check_mode);
        if (_check_mode == 2)
        {
            if ((CURP.Length < 16) || (RFC.Length < 10) || (RFC.Substring(0, 10) != CURP.Substring(0, 10)) ||
                (Gender == 1 && CURP.Substring(10, 1) != "H") || (Gender == 2 && CURP.Substring(10, 1) != "M"))
            {
                return false;
            }
        }
      else if (_check_mode == 1)
        {
            if ((CURP.Length < 16) || (RFC.Length < 10) || (RFC.Substring(0, 10) != CURP.Substring(0, 10)))
            {
                return false;
            }
            
        }

        return true;
    } //CheckCURP

    //------------------------------------------------------------------------------
    // PURPOSE : Check the RFC and CURP 
    // 
    //  PARAMS :
    //      - INPUT :
    //          - Name01
    //          - Name02
    //          - Name03
    //          - RFC
    //          - CURP
    //          - BirthDatedDay
    //          - BirthDatedMonth
    //          - BirthDatedYear
    //
    //      - OUTPUT :
    //          - WhichOneIsWrong : 0-OK  1-RFC  2-CURP 
    //          - ExpectedRFC 
    //
    // RETURNS : True when 
    //              - Check is not required 
    //              - Check is required and RFC and CURP (if any) are OK 
    // 
    //   NOTES:
    public static Boolean CheckHolderIds(String Name01, String Name02, String Name03,
                                         String RFC, String CURP,
                                         DateTime BirthDate,
                                         GENDER Gender,
                                         Boolean RFCCheckRequired,
                                         out ACCOUNT_HOLDER_ID_TYPE WhichOneIsWrong, out String ExpectedID)
    {
      String _aux_string;
      String _rfc;
      String _curp;
      Int32 _check_mode;

      WhichOneIsWrong = ACCOUNT_HOLDER_ID_TYPE.UNKNOWN;
      ExpectedID = "";

      try
      {
        _check_mode = GeneralParam.GetInt32("Cashier", "CheckHolderId", 1, 0, 2);

        // QMP 09-DEC-2013: Allow generic RFCs
        if (IsGenericRFC(RFC))
        {
          WhichOneIsWrong = ACCOUNT_HOLDER_ID_TYPE.UNKNOWN;
          ExpectedID = "";

          if (CURP.Trim() == "")
          {
            return true;
          }

          WhichOneIsWrong = ACCOUNT_HOLDER_ID_TYPE.CURP;
          ExpectedID = "";

          return false;
        }

        if (_check_mode == 0)
        {
          // Holder id's will not be checked out
          WhichOneIsWrong = ACCOUNT_HOLDER_ID_TYPE.UNKNOWN;
          ExpectedID = "";

          return true;
        }

        _rfc = RFC.Trim();

        // RFC empty, so we are checking only CURP.
        // On _rfc variable assing the correct RFC value
        if (_rfc == String.Empty && !RFCCheckRequired)
        {
          _rfc = ValidateFormat.ExpectedRFC(Name01, Name02, Name03, BirthDate);
        }

        _curp = CURP.Trim();

        if (_check_mode == 1)
        {
          _aux_string = BirthDate.ToString("yyMMdd");
          ExpectedID = "----" + _aux_string + "----";
        }

        if (_check_mode == 2)
        {
          ExpectedID = ValidateFormat.ExpectedRFC(Name01, Name02, Name03, BirthDate);
        }

        if (_check_mode == 2)
        {
          if (_rfc != ExpectedID)
          {
            WhichOneIsWrong = ACCOUNT_HOLDER_ID_TYPE.RFC;

            return false;
          }
        }

        if (_check_mode == 1)
        {
          //    - RFC Length
          if (_rfc.Length < 13)
          {
            WhichOneIsWrong = ACCOUNT_HOLDER_ID_TYPE.RFC;

            return false;
          }
          //    - Date part must be present in the RFC
          _aux_string = BirthDate.ToString("yyMMdd");
          if (_rfc.Substring(4, 6) != _aux_string)
          {
            WhichOneIsWrong = ACCOUNT_HOLDER_ID_TYPE.RFC;

            return false;
          }
        }

        // Check CURP
        //    - CURP Length
        //    - RFC and CURP must partially match 

        if (_curp != "")
        {
          ExpectedID = _rfc.Substring(0, 10) + "----";

          //    - CURP Length
          if (_curp.Length < 16)
          {
            WhichOneIsWrong = ACCOUNT_HOLDER_ID_TYPE.CURP;

            return false;
          }

          //    - RFC and CURP must partially match (first 10 chars)
          if (_rfc.Substring(0, 10) != _curp.Substring(0, 10))
          {
            WhichOneIsWrong = ACCOUNT_HOLDER_ID_TYPE.CURP;

            return false;
          }

          //    - CURP must contain M for women and H for men
          if ((Gender == GENDER.MALE && _curp.Substring(10, 1) != "H")
            || (Gender == GENDER.FEMALE && _curp.Substring(10, 1) != "M"))
          {
            WhichOneIsWrong = ACCOUNT_HOLDER_ID_TYPE.CURP;

            return false;
          }
        } // curp

        WhichOneIsWrong = ACCOUNT_HOLDER_ID_TYPE.UNKNOWN;

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // CheckHolderIds

      public static Boolean WrongRFC(String Value)
      {
          Regex _regex;
          String _value;
          Boolean _error;

          int _check_mode = GeneralParam.GetInt32("Cashier", "CheckHolderId", 1);
          _check_mode = Math.Max(0, _check_mode);
          _check_mode = Math.Min(2, _check_mode);

          switch (_check_mode)
          {
              case 2:
                  _regex = new Regex(RFC_REGEX);
                  _error = true;
                  if ((Value.Length <= MAX_LENGHT_RFC && Value.Length >= MIN_LENGHT_RFC) && (_regex.IsMatch(Value)))
                  {
                      _value = _regex.Replace(Value, "").Trim();
                      _error = !String.IsNullOrEmpty(_value);
                  }
                  break;
              default:
                  _error = false;
                  break;
          }
          return _error;
      }


      public static String ExpectedRFC(String Paterno, String Materno, String Nombre, DateTime Nacimiento)
      {
          String _expected_rfc;

          _expected_rfc = "";

          try
          {
              int _check_mode = GeneralParam.GetInt32("Cashier", "CheckHolderId", 1);
              _check_mode = Math.Max(0, _check_mode);
              _check_mode = Math.Min(2, _check_mode);

        // ATB 22-DEC-2016
        // Always generate the right RFC
                      _expected_rfc = WSI.Common.RFC.RFC13(Paterno, Materno, Nombre, Nacimiento);
          }
          catch
          {
              _expected_rfc = "";
          }

          return _expected_rfc;
      }

      // PURPOSE: Validates the format of a number or currency (accepting decimals and negatives) value.
    //
    //  PARAMS:
    //     - INPUT: - Value: String to validate (represents the value of the number to validate)
    //              - DecimalSeparator: Indicates the decimal separator character
    //              - NumDecimals: Indicates the number of decimals allowed.
    //              - AreNegativesAllowed: Indicates if must accept negative values
    //     - OUTPUT:
    //
    // RETURNS:
    //         - True: if the format is correct.
    //         - False: Otherwise.
    //
    public static Boolean Number(String Value, String DecimalSeparator, Int32 NumDecimals, Boolean AreNegativesAllowed)
    {
      Int32 _idx;
      Int32 _num_decimals;
      Boolean _has_decimals;

      _idx = 0;
      _num_decimals = 0;
      _has_decimals = false;

      foreach (Char _char in Value)
      {
        if ((_char.ToString() != DecimalSeparator) && !(_char >= '0' && _char <= '9') && (_char != '-'))
        {
          return false;
        }

        if ((_char == '-') && (!AreNegativesAllowed || _idx > 0))
        {
          return false;
        }

        if (_has_decimals)
        {
          _num_decimals++;
        }

        if (_char.ToString() == DecimalSeparator)
        {
          // Must have only one decimal separator
          if (_has_decimals)
          {
            return false;
          }
          _has_decimals = true;
        }

        if (_num_decimals > NumDecimals)
        {
          return false;
        }

        _idx++;
      }

      // Check if it has decimal separator but not decimals below
      if (_has_decimals && _num_decimals == 0)
      {
        return false;
      }

      return true;
    }// Number

    // PURPOSE: Validates the format of a string with a regular expresion.
    //
    //  PARAMS:
    //     - INPUT: - Value: String to validate (represents the value of the number to validate)
    //              - RegularExpresion: to validate string
    //     - OUTPUT:
    //
    // RETURNS:
    //         - True: if the format is correct.
    //         - False: Otherwise.
    //
    public static Boolean CheckRegularExpression(String Value, String RegularExpression)
    {
      if (!String.IsNullOrEmpty(RegularExpression))
      {
        return Regex.IsMatch(Value, RegularExpression, RegexOptions.IgnoreCase);
      }

      return true;
    }

    #endregion // Public Functions
  }

}
