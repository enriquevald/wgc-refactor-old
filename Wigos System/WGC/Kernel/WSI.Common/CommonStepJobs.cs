//------------------------------------------------------------------------------
// Copyright © 2013 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CommonStepJobs.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: JML
// 
// CREATION DATE: 05-MAR-2013
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-MAR-2013 JML    First release.
// 26-MAR-2013 ACC    Fixed defect #658: Error en Programa de puntos: cuenta que no sube de nivel
// 06-SEP-2013 FBA    Added control for automatic account block via GP if account level upgraded or downgraded 
// 22-OCT-2013 LEM    Unify saving account and cashier movements using SharedMovementsTables clases
// 15-ENE-2014 JML    AccountPointsCache --> Task 154: account_points_cache maintenance
// 28-APR-2015 DCS    Multisite Multicurrency -> Thread manteinance exchange_rates
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 05-FEB-2015 JRC    PBI 7909: Multiple Buckets
// 01-MAR-2016 FGB    Product Backlog Item 9781: Multiple Buckets: Refactorización
// 02-AGO-2016 JRC    PBI 16257:Buckets - RankingLevelPoints_Generated - RankingLevelPoints_Discretional
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace WSI.Common
{
  public class CommonStepJobs
  {
    static DateTime m_last_running_accounts_levels;
    static DateTime m_last_running_clear_account_points_cache;
    static DateTime m_last_running_manteinance_exchange_rates;
    static Int32 m_rand_minutes_shift;

    //------------------------------------------------------------------------------
    // PURPOSE : Init
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    public static void Init()
    {
      Random _rnd;

      m_last_running_accounts_levels = WGDB.Now.Date.AddDays(-1);
      m_last_running_clear_account_points_cache = WGDB.Now.Date.AddDays(-1);
      m_last_running_manteinance_exchange_rates = WGDB.Now.Date.AddDays(-1);

      _rnd = new Random(Environment.TickCount);
      m_rand_minutes_shift = _rnd.Next(0, 11);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Points expiration management
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    public static Boolean AccountsPointsExpiration()
    {
      Int32 _expiration_days;
      DataTable _accounts;
      StringBuilder _sb;

      if (!Int32.TryParse(Misc.ReadGeneralParams("PlayerTracking", "PointsExpirationDays"), out _expiration_days))
      {
        _expiration_days = 0;
      }
      _expiration_days = Math.Max(0, _expiration_days);

      if (_expiration_days == 0)
      {
        return true;
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          DateTime _last_activity1;
          DateTime _last_activity2;

          _last_activity2 = WGDB.Now.AddDays(-_expiration_days);
          _last_activity1 = _last_activity2.AddDays(-30);

          _sb = new StringBuilder();
          _sb.AppendLine("SELECT   AC_ACCOUNT_ID ");
          _sb.AppendLine("       , AC_POINTS ");
          _sb.AppendLine("       , AC_LAST_ACTIVITY ");

          _sb.AppendLine("  FROM   ACCOUNTS WITH (INDEX (IX_ac_last_activity)) ");
          _sb.AppendLine(" WHERE   AC_LAST_ACTIVITY >= @pLastActivity1 ");
          _sb.AppendLine("   AND   AC_LAST_ACTIVITY <  @pLastActivity2 ");
          _sb.AppendLine("   AND   AC_POINTS                                      > 0 ");
          _sb.AppendLine("   AND   AC_CURRENT_PLAY_SESSION_ID IS NULL "); // Ensure Not In Use

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pLastActivity1", SqlDbType.DateTime).Value = _last_activity1;
            _sql_cmd.Parameters.Add("@pLastActivity2", SqlDbType.DateTime).Value = _last_activity2;

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _accounts = new DataTable("ACCOUNTS");
              _sql_da.Fill(_accounts);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      foreach (DataRow _account in _accounts.Rows)
      {
        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            if (ExpireAccountPoints(_account, _db_trx.SqlTransaction))
            {
              _db_trx.Commit();
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);

          continue;
        }
      } // foreach

      return true;

    } // AccountsPointsExpiration

    //------------------------------------------------------------------------------
    // PURPOSE : Credits expiration for one account
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataRowAccount
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    private static Boolean ExpireAccountPoints(DataRow DataRowAccount, SqlTransaction SqlTrx)
    {
      String _sql_str;
      Int64 _account_id;
      Decimal _points;
      DateTime _last_activity;
      AccountMovementsTable _account_mov_table;

      _account_id = (Int64)DataRowAccount["AC_ACCOUNT_ID"];
      _points = (Decimal)DataRowAccount["AC_POINTS"];
      _last_activity = (DateTime)DataRowAccount["AC_LAST_ACTIVITY"];

      _sql_str = "UPDATE   ACCOUNTS " +
                 "   SET   AC_POINTS        = 0 " +
                 " WHERE   AC_ACCOUNT_ID    = @pAccountId " +
                 "   AND   AC_POINTS        = @pPoints " +
                 "   AND   AC_LAST_ACTIVITY = @pLastActivity " +
                 "   AND   AC_CURRENT_PLAY_SESSION_ID IS NULL ";
      try
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sql_str, SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = _account_id;
          _sql_cmd.Parameters.Add("@pPoints", SqlDbType.Money).Value = _points;
          _sql_cmd.Parameters.Add("@pLastActivity", SqlDbType.DateTime).Value = _last_activity;

          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            return false;
          }
        }

        _account_mov_table = new AccountMovementsTable(0, "", 0);
        if (!_account_mov_table.Add(0, _account_id, MovementType.PointsExpired, _points, _points, 0, 0))
        {
          return false;
        }

        if (!_account_mov_table.Save(SqlTrx))
        {
          return false;
        }

        return true;
      }
      catch (Exception)
      {
        return false;
      }
    } // ExpireAccountPoints

    //------------------------------------------------------------------------------
    // PURPOSE : Account levels management
    //
    //  PARAMS :
    //      - INPUT :
    //          - SqlConn
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static Boolean AccountsLevels(SqlConnection SqlConn)
    {
      const int NUM_LEVELS = 5; // 0: Anonymous, 1..4: PlayerTracking
      SqlCommand _sql_cmd;
      SqlDataAdapter _sql_da;
      DataTable _table_accounts;
      StringBuilder _sql_sb;
      Int32 _closing_time;

      Int32[] _points_to_enter;
      Int32[] _points_to_keep;
      Int32[] _min_days;
      Int32[] _max_no_activity;

      DateTime?[] _expiration;
      DateTime _min_expiration;
      Int32 _max_days;
      Boolean _expire_on_date;

      Int32 _days_counting_points;
      DateTime _run_date_time;
      Random _rnd;
      GeneralParam.Dictionary _general_param;

      _general_param = GeneralParam.GetDictionary();

      DateTime _today;

      // Get Closing Time
      _closing_time = _general_param.GetInt32("WigosGUI", "ClosingTime");

      _today = WGDB.Now.Date;
      _today = _today.AddHours((double)_closing_time);

      _run_date_time = _today.AddMinutes(15 + m_rand_minutes_shift);


      // Run after closing date
      // RCI 28-JUL-2011: Better run after run_date_time, that is closing date
      //                  plus 15 minutes + a random of minutes between (0, 10) ...
      if (WGDB.Now < _run_date_time)
      {
        return true;
      }

      // Today already run
      if (m_last_running_accounts_levels >= _today)
      {
        return true;
      }

      _days_counting_points = _general_param.GetInt32("PlayerTracking", "Levels.DaysCountingPoints");

      _points_to_enter = new Int32[NUM_LEVELS];
      _points_to_keep = new Int32[NUM_LEVELS];
      _max_no_activity = new Int32[NUM_LEVELS];
      _min_days = new Int32[NUM_LEVELS];
      _expiration = new DateTime?[NUM_LEVELS];

      Accounts.GetMinExpiration(out _expire_on_date, out _min_expiration);

      //
      // 400 ~ 1 Year + 1 Month 
      //
      _max_days = 400 + _days_counting_points;

      for (int _level = 0; _level < NUM_LEVELS; _level++)
      {
        if (_level <= 1)
        {
          _points_to_enter[_level] = 0;
          _points_to_keep[_level] = 0;
          _max_no_activity[_level] = 0;
          _min_days[_level] = 0;
          _expiration[_level] = null;
        }
        else
        {
          _points_to_enter[_level] = _general_param.GetInt32("PlayerTracking", "Level" + _level.ToString("00") + ".PointsToEnter");
          _points_to_keep[_level] = _general_param.GetInt32("PlayerTracking", "Level" + _level.ToString("00") + ".PointsToKeep");
          _max_no_activity[_level] = _general_param.GetInt32("PlayerTracking", "Level" + _level.ToString("00") + ".MaxDaysNoActivity", 0);
          _min_days[_level] = Math.Max(1, _general_param.GetInt32("PlayerTracking", "Level" + _level.ToString("00") + ".DaysOnLevel"));

          if (_points_to_enter[_level] <= 0)
          {
            Log.Error(String.Format("Points to enter level {0}: {1}", _level, _points_to_enter[_level]));

            return false;
          }

          _expiration[_level] = _today.AddDays(_min_days[_level]);

          if (_expire_on_date)
          {
            if (_expiration[_level] > _min_expiration)
            {
              _expiration[_level] = _min_expiration.AddYears(1);
            }
            else
            {
              _expiration[_level] = _min_expiration;
            }
          }
        }
      }

      // Now, that it's sure that the HUGE SELECT is going to execute, update the random minutes to shift.
      // Get another seed, to ensure randomized values.
      _rnd = new Random(Environment.TickCount);
      m_rand_minutes_shift = _rnd.Next(0, 11);

      //
      // Get Accounts to change level
      //
      _table_accounts = new DataTable("ACCOUNTS");
      _sql_da = new SqlDataAdapter();
      _sql_cmd = new SqlCommand();
      _sql_cmd.Connection = SqlConn;
      _sql_cmd.CommandTimeout = 15 * 60; // 15 minutes
      _sql_da.SelectCommand = _sql_cmd;

      _sql_sb = new StringBuilder();
      _sql_sb.AppendLine("                                                                                                          ");
      _sql_sb.AppendLine("declare @pCountFromDate      datetime                                                                     ");
      _sql_sb.AppendLine("declare @pLastActivity       datetime                                                                     ");
      _sql_sb.AppendLine("                                                                                                          ");
      _sql_sb.AppendLine("set @pCountFromDate  = DATEADD (DAY, -@pDaysCountingPoints,  @pToday)                                     ");
      _sql_sb.AppendLine("set @pLastActivity   = DATEADD (DAY, -@pMaxDays,             @pToday)                                     ");
      _sql_sb.AppendLine("                                                                                                          ");
      _sql_sb.AppendLine("SELECT   AC.ACCOUNT_ID      AS ACCOUNT_ID                                                                 ");
      _sql_sb.AppendLine("       , LEVEL              AS OLD_LEVEL                                                                  ");
      _sql_sb.AppendLine("       , EXPIRATION         AS OLD_EXPIRATION                                                             ");
      _sql_sb.AppendLine("       , LEVEL              AS NEW_LEVEL                                                                  ");
      _sql_sb.AppendLine("       , EXPIRATION         AS NEW_EXPIRATION                                                             ");
      _sql_sb.AppendLine("       , ISNULL (POINTS, 0) AS POINTS                                                                     ");

      _sql_sb.AppendLine("       , ISNULL(POINTS_GENERATED, 0)    AS POINTS_GENERATED                                               ");
      _sql_sb.AppendLine("       , ISNULL (POINTS_DISCRETIONAL, 0) AS POINTS_DISCRETIONAL                                           ");
      
      
      _sql_sb.AppendLine("       , LAST_ACTIVITY      AS LAST_ACTIVITY                                                              ");
      _sql_sb.AppendLine("FROM                                                                                                      ");
      _sql_sb.AppendLine("(                                                                                                         ");
      _sql_sb.AppendLine("--                                                                                                        ");
      _sql_sb.AppendLine("-- Account Level Expired Expiration                                                                       ");
      _sql_sb.AppendLine("--                                                                                                        ");
      _sql_sb.AppendLine("    SELECT   AC_ACCOUNT_ID              AS ACCOUNT_ID                                                     ");
      _sql_sb.AppendLine("           , AC_HOLDER_LEVEL            AS LEVEL                                                          ");
      _sql_sb.AppendLine("           , AC_HOLDER_LEVEL_EXPIRATION AS EXPIRATION                                                     ");
      _sql_sb.AppendLine("           , AC_LAST_ACTIVITY           AS LAST_ACTIVITY                                                  ");
      _sql_sb.AppendLine("      FROM   ACCOUNTS WITH (INDEX (IX_ac_last_activity))                                                  ");
      _sql_sb.AppendLine("     WHERE   AC_LAST_ACTIVITY >= @pLastActivity                                                           ");
      _sql_sb.AppendLine("       AND   AC_HOLDER_LEVEL  >= 1                                                                        ");
      _sql_sb.AppendLine("                                                                                                          ");
      _sql_sb.AppendLine(") AC                                                                                                      ");
      _sql_sb.AppendLine("LEFT JOIN                                                                                                 ");
      _sql_sb.AppendLine("(                                                                                                         ");
      _sql_sb.AppendLine("--                                                                                                        ");
      _sql_sb.AppendLine("-- Account Points                                                                                         ");
      _sql_sb.AppendLine("--                                                                                                        ");
      _sql_sb.AppendLine("    SELECT   AM_ACCOUNT_ID      AS ACCOUNT_ID                                                             ");
      _sql_sb.AppendLine("           , SUM( CASE WHEN AM_TYPE = @pExpirationPointsLevel THEN (0 - AM_SUB_AMOUNT)                    "); // Puntos expirados
      _sql_sb.AppendLine("                       WHEN AM_TYPE = @pManualSubPointsLevel  THEN (0 - AM_SUB_AMOUNT)                    "); // Sub Amount
      _sql_sb.AppendLine("                       WHEN AM_TYPE = @pManualSetPointsLevel  THEN (AM_ADD_AMOUNT - AM_SUB_AMOUNT)        "); // Set Amount
      _sql_sb.AppendLine("                       ELSE AM_ADD_AMOUNT END) AS POINTS                                                  ");
      _sql_sb.AppendLine("                                                                                                          ");
      _sql_sb.AppendLine("           , SUM( CASE WHEN AM_TYPE = @pPointsAwarded               THEN AM_ADD_AMOUNT                    ");
      _sql_sb.AppendLine("                       WHEN AM_TYPE = @pBucketIdPointsLevel         THEN AM_ADD_AMOUNT                    ");
      _sql_sb.AppendLine("                       WHEN AM_TYPE = @pExpirationPointsLevel       THEN (0 - AM_SUB_AMOUNT)              ");
      _sql_sb.AppendLine("                       ELSE 0 END) AS POINTS_GENERATED                                                    ");
      _sql_sb.AppendLine("                                                                                                          ");
      _sql_sb.AppendLine("           , SUM( CASE WHEN AM_TYPE = @pManuallyAddedPointsForLevel THEN AM_ADD_AMOUNT                    ");
      _sql_sb.AppendLine("                       WHEN AM_TYPE = @pImportedPointsForLevel      THEN AM_ADD_AMOUNT                    ");
      _sql_sb.AppendLine("                       WHEN AM_TYPE = @pImportedPointsHistory       THEN AM_ADD_AMOUNT                    ");
      _sql_sb.AppendLine("                       WHEN AM_TYPE = @pManualAddPointsLevel        THEN AM_ADD_AMOUNT                    ");
      _sql_sb.AppendLine("                       WHEN AM_TYPE = @pManualSubPointsLevel        THEN (0 - AM_SUB_AMOUNT)              ");
      _sql_sb.AppendLine("                       WHEN AM_TYPE = @pManualSetPointsLevel        THEN (AM_ADD_AMOUNT - AM_SUB_AMOUNT)  ");
      _sql_sb.AppendLine("                       ELSE 0 END) AS POINTS_DISCRETIONAL                                                 ");
      _sql_sb.AppendLine("                                                                                                          ");
      _sql_sb.AppendLine("      FROM   ACCOUNT_MOVEMENTS WITH (INDEX (IX_type_date_account))                                        ");
      _sql_sb.AppendLine("     WHERE   AM_TYPE IN ( @pPointsAwarded                                                                 ");
      _sql_sb.AppendLine("                        , @pManuallyAddedPointsForLevel                                                   ");
      _sql_sb.AppendLine("                        , @pImportedPointsForLevel                                                        ");
      _sql_sb.AppendLine("                        , @pImportedPointsHistory                                                         ");
      _sql_sb.AppendLine("                        , @pBucketIdPointsLevel                                                           ");
      _sql_sb.AppendLine("                        , @pExpirationPointsLevel                                                         ");
      _sql_sb.AppendLine("                        , @pManualAddPointsLevel                                                          ");
      _sql_sb.AppendLine("                        , @pManualSubPointsLevel                                                          ");
      _sql_sb.AppendLine("                        , @pManualSetPointsLevel)                                                         ");
      _sql_sb.AppendLine("       AND   AM_DATETIME  >= @pCountFromDate                                                              ");
      _sql_sb.AppendLine("       AND   AM_DATETIME  <  @pToday                                                                      ");
      _sql_sb.AppendLine("  GROUP BY   AM_ACCOUNT_ID                                                                                ");
      _sql_sb.AppendLine("                                                                                                          ");
      _sql_sb.AppendLine(") PO ON AC.ACCOUNT_ID = PO.ACCOUNT_ID                                                                     ");
      _sql_sb.AppendLine("                                                                                                          ");
      _sql_sb.AppendLine("WHERE ( LEVEL >= 1 )                                                                                      ");
      _sql_sb.AppendLine("                                                                                                          ");

      _sql_cmd.CommandText = _sql_sb.ToString();
      _sql_cmd.Parameters.Add("@pToday", SqlDbType.DateTime).Value = _today;
      _sql_cmd.Parameters.Add("@pDaysCountingPoints", SqlDbType.Int).Value = _days_counting_points;
      _sql_cmd.Parameters.Add("@pPointsToEnterLevel02", SqlDbType.Int).Value = _points_to_enter[2];
      _sql_cmd.Parameters.Add("@pMaxDays", SqlDbType.Int).Value = _max_days;

      //36-Awarded points, 1102 1202      Forman parte del bucket 2 y el bucket 8
      _sql_cmd.Parameters.Add("@pPointsAwarded", SqlDbType.Int).Value = (int)MovementType.PointsAwarded;
      _sql_cmd.Parameters.Add("@pBucketIdPointsLevel", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_EndSession + (int)Buckets.BucketId.RankingLevelPoints;
      _sql_cmd.Parameters.Add("@pExpirationPointsLevel", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_Expired + (int)Buckets.BucketId.RankingLevelPoints;
      
      
      // 05-FEB-2015 JRC    PBI 7909: Multiple Buckets
      // estos 6 Forman parte del bucket 2 y el bucket 9
      // 68-Manually add points from GUI, 72-Imported points, 73-History points imported 
      _sql_cmd.Parameters.Add("@pManuallyAddedPointsForLevel", SqlDbType.Int).Value = (int)MovementType.ManuallyAddedPointsForLevel;
      _sql_cmd.Parameters.Add("@pImportedPointsForLevel", SqlDbType.Int).Value = (int)MovementType.ImportedPointsForLevel;
      _sql_cmd.Parameters.Add("@pImportedPointsHistory", SqlDbType.Int).Value = (int)MovementType.ImportedPointsHistory;
      // 1302 1402 1502 
      _sql_cmd.Parameters.Add("@pManualAddPointsLevel", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_Manual_Add + (int)Buckets.BucketId.RankingLevelPoints;
      _sql_cmd.Parameters.Add("@pManualSubPointsLevel", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_Manual_Sub + (int)Buckets.BucketId.RankingLevelPoints;
      _sql_cmd.Parameters.Add("@pManualSetPointsLevel", SqlDbType.Int).Value = (int)MovementType.MULTIPLE_BUCKETS_Manual_Set + (int)Buckets.BucketId.RankingLevelPoints;



      
      _sql_da.Fill(_table_accounts);

      foreach (DataRow _account in _table_accounts.Rows)
      {
        int _old_level;
        int _new_level;
        Decimal _points; //bucket 2
        Decimal _points_discretional; //bucket 8
        Decimal _points_generated; // bucket 9

        Boolean _expired;
        DateTime _aux_date;
        TimeSpan _no_activity;
        DateTime? _old_expiration;
        DateTime? _new_expiration;
        Boolean _changed;
        Boolean _account_with_activity;

        _points = (Decimal)_account["POINTS"];
        _points_discretional = (Decimal)_account["POINTS_DISCRETIONAL"];
        _points_generated = (Decimal)_account["POINTS_GENERATED"];

        _old_level = (int)_account["OLD_LEVEL"];
        _old_expiration = null;
        if (!_account.IsNull("OLD_EXPIRATION"))
        {
          _old_expiration = (DateTime)_account["OLD_EXPIRATION"];
        }
        _new_level = _old_level;
        _new_expiration = _old_expiration;
        _expired = false;
        _account_with_activity = true;

        if (_old_level < 1)
        {
          continue;
        }

        // Synchronize Bucket: level points. This bucket must have the value of the sum of the movements.
        if (!SynchronizeBucket_LevelPoints((Int64)_account["ACCOUNT_ID"], _points, _points_discretional, _points_generated))
        {
          return false;
        }

        if (_old_level == 1 && _points < _points_to_enter[2])
        {
          //Is not necessary to evaluate a level change

          continue;
        }

        // Calculate days without activity
        _aux_date = (DateTime)_account["LAST_ACTIVITY"];
        _no_activity = _today - _aux_date;

        if (_old_level > 1)
        {
          // Check Level Expiration
          _aux_date = _old_expiration.HasValue ? (DateTime)_account["OLD_EXPIRATION"] : _today;
          if (_today >= _aux_date)
          {
            _expired = true;
          }

          if (_max_no_activity[_old_level] > 0)
          {
            // Check Days without activity
            if ((Int32)_no_activity.TotalDays >= _max_no_activity[_old_level])
            {
              _expired = true;
              _account_with_activity = false;
            }
          }

          // Check Minimum Points
          if (_points < _points_to_keep[_old_level])
          {
            _expired = true;
          }
        }

        if (_expired)
        {
          _new_level = 1;
        }

        if (_account_with_activity)
        {
          // Try to enter next levels
          for (int _level = _new_level + 1; _level < NUM_LEVELS; _level++)
          {
            if (_points < _points_to_enter[_level])
            {
              break;
            }

            // RCI & AJQ 07-JAN-2013: Check if no activity in the candidate new level
            if (_max_no_activity[_level] > 0)
            {
              if ((Int32)_no_activity.TotalDays >= _max_no_activity[_level])
              {
                continue;
              }
            }

            _new_level = _level;
          }
        }

        _new_level = Math.Max(1, _new_level);

        //
        // Level Greater than 1
        //
        if (_new_level != _old_level)
        {
          // Level Changed --> Set new expiration
          _new_expiration = _expiration[_new_level];
        }
        else
        {
          if (_new_level <= 1)
          {
            // Reset expiration
            _new_expiration = _expiration[1];
          }
          else
          {
            // Same level
            if (_expire_on_date)
            {
              if (_expired)
              {
                // Set new expiration ...
                _new_expiration = _expiration[_new_level];
              }
            }
            else
            {
              // Conditionally Extend expiration ...
              if (_points >= _points_to_enter[_new_level])
              {
                _new_expiration = _expiration[_new_level];
              }
            }
          }
        }

        _account["NEW_LEVEL"] = _new_level;
        if (_new_expiration.HasValue)
        {
          _account["NEW_EXPIRATION"] = _new_expiration.Value;
        }
        else
        {
          _account["NEW_EXPIRATION"] = DBNull.Value;
        }

        _changed = (_new_level != _old_level)
                || (_new_expiration.HasValue != _old_expiration.HasValue)
                || (_new_expiration.HasValue && _old_expiration.HasValue && _new_expiration.Value != _old_expiration.Value);

        if (!_changed)
        {
          continue;
        }

        try
        {
          using (DB_TRX _trx = new DB_TRX())
          {
            if (AccountLevelsUpdate(_account, _today, _trx.SqlTransaction))
            {
              _trx.Commit();
            }
            else
            {
              _trx.Rollback();
            }
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);

          return false;
        }
      } // foreach

      m_last_running_accounts_levels = _today;

      return true;

    } // AccountsLevels

    //------------------------------------------------------------------------------
    // PURPOSE : Update Bucket "Level Points". This bucket must have the value of the sum of the movements. 
    //
    //  PARAMS :
    //      - INPUT :
    //          - AccountId
    //          - Points
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - True
    //      - False
    //   NOTES :
    //
    private static Boolean SynchronizeBucket_LevelPoints(Int64 AccountId, Decimal Points, Decimal Points_Discretional, Decimal Points_Generated)
    {
      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          if (BucketsUpdate.UpdateCustomerBucket(AccountId, Buckets.BucketId.RankingLevelPoints, null, Points, Buckets.BucketOperation.SET_GENERATED, true, true, _trx.SqlTransaction))
          {
            if (BucketsUpdate.UpdateCustomerBucket(AccountId, Buckets.BucketId.RankingLevelPoints_Generated, null, Points_Generated, Buckets.BucketOperation.SET_GENERATED, true, true, _trx.SqlTransaction))
            {
              if (BucketsUpdate.UpdateCustomerBucket(AccountId, Buckets.BucketId.RankingLevelPoints_Discretional, null, Points_Discretional, Buckets.BucketOperation.SET_GENERATED, true, true, _trx.SqlTransaction))
              {
                _trx.Commit();

                return true;
              }
            }

          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Holder level data update for one account
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataRowAccount
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static Boolean AccountLevelsUpdate(DataRow Row, DateTime OpenDateTime, SqlTransaction SqlTrx)
    {
      SqlCommand _sql_command;
      String _sql_str;
      Int64 _account_id;
      Decimal _account_points;
      Int64 _operation_id;
      SqlDataReader _reader;

      Int32 _new_level;
      Int32 _old_level;
      Int32 _current_level;

      AccountMovementsTable _account_mov_table;
      Boolean _is_multisite;

      try
      {
        _is_multisite = GeneralParam.GetBoolean("MultiSite", "IsCenter", false);

        _account_id = (Int64)Row["ACCOUNT_ID"];
        _new_level = (Int32)Row["NEW_LEVEL"];
        _old_level = (Int32)Row["OLD_LEVEL"];
        _current_level = 0;

        _sql_str = " UPDATE   ACCOUNTS " +
                   "    SET   AC_HOLDER_LEVEL            = @pNewLevel " +
                   "        , AC_HOLDER_LEVEL_NOTIFY     = ISNULL (AC_HOLDER_LEVEL_NOTIFY, 0) + (@pNewLevel - @pOldLevel) " +
                   "        , AC_HOLDER_LEVEL_ENTERED    = CASE WHEN (@pNewLevel <> @pOldLevel) THEN @pOpenDateTime ELSE AC_HOLDER_LEVEL_ENTERED END " +
                   "        , AC_HOLDER_LEVEL_EXPIRATION = CASE WHEN (@pNewLevel = 1)           THEN NULL    ELSE @pLevelExpiration END " +
                   "  WHERE   AC_ACCOUNT_ID   = @pAccountId" +
                   "    AND   AC_HOLDER_LEVEL = @pOldLevel ;" +
                   " IF (@@ROWCOUNT = 1) ";

        _sql_str += "     SELECT   ISNULL(CBU_VALUE, 0) AC_POINTS, AC_HOLDER_LEVEL " +
                    "       FROM   ACCOUNTS LEFT JOIN CUSTOMER_BUCKET ON CBU_CUSTOMER_ID = @pAccountId AND CBU_BUCKET_ID = @pBucketId " +
                    "      WHERE   AC_ACCOUNT_ID = @pAccountId ";

        _sql_command = new SqlCommand(_sql_str, SqlTrx.Connection, SqlTrx);
        _sql_command.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = _account_id;
        _sql_command.Parameters.Add("@pNewLevel", SqlDbType.Int).Value = _new_level;
        _sql_command.Parameters.Add("@pOldLevel", SqlDbType.Int).Value = _old_level;
        _sql_command.Parameters.Add("@pOpenDateTime", SqlDbType.DateTime).Value = OpenDateTime;
        _sql_command.Parameters.Add("@pBucketId", SqlDbType.BigInt).Value = Buckets.BucketId.RedemptionPoints;

        if (Row.IsNull("NEW_EXPIRATION"))
        {
          _sql_command.Parameters.Add("@pLevelExpiration", SqlDbType.DateTime).Value = DBNull.Value;
        }
        else
        {
          _sql_command.Parameters.Add("@pLevelExpiration", SqlDbType.DateTime).Value = (DateTime)Row["NEW_EXPIRATION"];
        }
        _account_points = 0;
        _reader = _sql_command.ExecuteReader();

        if (_reader.Read())
        {
          _account_points = _reader.GetDecimal(0);
          _current_level = _reader.GetInt32(1);
        }
        _reader.Close();

        if (_current_level != _new_level)
        {
          return false;
        }
        if (_new_level != _old_level)
        {
          _operation_id = 0;
          if (!_is_multisite)
          {
            Operations.DB_InsertOperation(OperationCode.HOLDER_LEVEL_CHANGED, _account_id, 0, 0, 0, 0,
                                          0, _old_level * 100 + _new_level, 0, string.Empty, out _operation_id, SqlTrx);
          }

          _account_mov_table = new AccountMovementsTable(0, "", 0);
          if (!_account_mov_table.Add(_operation_id, _account_id, MovementType.HolderLevelChanged, _account_points,
                                      _old_level, _new_level, _account_points))
          {
            return false;
          }

          if (!_account_mov_table.Save(SqlTrx))
          {
            return false;
          }

          // FBA 06-SEP-2013 
          if (!BlockAccountOnUpgradeDowngrade(_old_level, _new_level, _account_id, SqlTrx))
          {
            return false;
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // AccountLevelsUpdate

    //------------------------------------------------------------------------------
    // PURPOSE : Checks if the Account must be blocked due to its level upgrade or downgrade and calls the function that blocks it.
    //
    //  PARAMS :
    //      - INPUT :
    //          - old level
    //          - new level
    //          - account id
    //          - sql transaction
    //
    //      - OUTPUT :
    //
    // RETURNS : 
    //
    //   NOTES :
    //
    private static Boolean BlockAccountOnUpgradeDowngrade(Int32 OldLevel, Int32 NewLevel, Int64 AccountID, SqlTransaction SqlTrx)
    {
      UpgradeDowngradeAction _old_level_action;
      UpgradeDowngradeAction _new_level_action;
      CardData _card_data;
      CashierSessionInfo _session_info;

      if (OldLevel == NewLevel)
      {
        // Do nothing
        return true;
      }
      try
      {
        // Gets the action defined in General Params for old and new levels
        _old_level_action = (UpgradeDowngradeAction)GeneralParam.GetInt32("PlayerTracking", String.Format("Level0{0}.UpgradeDowngradeAction", OldLevel));
        _new_level_action = (UpgradeDowngradeAction)GeneralParam.GetInt32("PlayerTracking", String.Format("Level0{0}.UpgradeDowngradeAction", NewLevel));

        if (_old_level_action == UpgradeDowngradeAction.Both)
        {
          _old_level_action = UpgradeDowngradeAction.BlockOnLeave;
        }
        if (_new_level_action == UpgradeDowngradeAction.Both)
        {
          _new_level_action = UpgradeDowngradeAction.BlockOnEnter;
        }

        if (_old_level_action != UpgradeDowngradeAction.BlockOnLeave && _new_level_action != UpgradeDowngradeAction.BlockOnEnter)
        {
          // Do nothing
          return true;
        }

        _card_data = new CardData();
        CardData.DB_CardGetAllData(AccountID, _card_data);
        _card_data.Blocked = true;
        if (NewLevel > OldLevel)
        {
          //UPGRADE
          _card_data.BlockReason = AccountBlockReason.HOLDER_LEVEL_UPGRADE;
          _card_data.BlockDescription = Resource.String("STR_UC_CARD_BLOCK_DESCRIPTION_HOLDER_LEVEL_UPGRADE");
        }
        else // (New_level < old_level)
        {
          //DOWNGRADE
          _card_data.BlockReason = AccountBlockReason.HOLDER_LEVEL_DOWNGRADE;
          _card_data.BlockDescription = Resource.String("STR_UC_CARD_BLOCK_DESCRIPTION_HOLDER_LEVEL_DOWNGRADE");
        }

        _session_info = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_SYSTEM);
        if (_session_info == null)
        {
          _session_info = new CashierSessionInfo();
          _session_info.AuthorizedByUserName = "";
          _session_info.TerminalId = 0;
          _session_info.TerminalName = "";
          _session_info.CashierSessionId = 0;
        }

        // Blocks the account if the requirements are meet
        if (CardData.DB_BlockUnblockCard(_card_data, _session_info, SqlTrx))
        {
          return true;
        }

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // BlockAccountOnUpgradeDowngrade

    //------------------------------------------------------------------------------
    // PURPOSE : Clear ACCOUNT_POINTS_CACHE (past registers)
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : Only runs once a day, 10+X minutes after time opening
    // 15-ENE-2014 JML    Product Backlog Item 151:AccountPointsCache --> Task 154: account_points_cache maintenance
    //
    //------------------------------------------------------------------------------
    public static Boolean ClearAccountPointsCache()
    {
      DataTable _apc_table;
      StringBuilder _sb;
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      _sql_conn = null;
      _sql_trx = null;

      // Today already run
      if (m_last_running_clear_account_points_cache == Misc.TodayOpening() || WGDB.Now < Misc.TodayOpening().AddMinutes(10 + m_rand_minutes_shift))
      {
        return true;
      }

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {

          _sb = new StringBuilder();
          _sb.AppendLine("SELECT   APC_ACCOUNT_ID ");
          _sb.AppendLine("       , APC_TODAY ");
          _sb.AppendLine("  FROM   ACCOUNT_POINTS_CACHE ");
          _sb.AppendLine(" WHERE   APC_TODAY < @pOpenToday ");

          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.Parameters.Add("@pOpenToday", SqlDbType.DateTime).Value = Misc.TodayOpening();

            using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
            {
              _apc_table = new DataTable("ACCOUNT_POINTS_CACHE");
              _sql_da.Fill(_apc_table);
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      if (_apc_table.Rows.Count == 0)
      {
        return true;
      }

      _sb = new StringBuilder();
      _sb.AppendLine("DELETE   FROM ACCOUNT_POINTS_CACHE ");
      _sb.AppendLine(" WHERE   APC_ACCOUNT_ID = @pAccountId ");
      _sb.AppendLine("   AND   APC_TODAY < @pOpenToday ");

      try
      {
        // Get Sql Connection 
        _sql_conn = WGDB.Connection();

        foreach (DataRow _apc_data_row in _apc_table.Rows)
        {
          _sql_trx = _sql_conn.BeginTransaction();
          try
          {
            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _sql_trx.Connection, _sql_trx))
            {
              _sql_cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = (Int64)_apc_data_row["APC_ACCOUNT_ID"];
              _sql_cmd.Parameters.Add("@pOpenToday", SqlDbType.DateTime).Value = Misc.TodayOpening();

              _sql_cmd.ExecuteNonQuery();
            }
            _sql_trx.Commit();

          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);

            continue;
          }

        } // foreach

        m_last_running_clear_account_points_cache = Misc.TodayOpening();
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            _sql_trx.Rollback();
          }
          // Close transaction 
          _sql_trx.Dispose();
          _sql_trx = null;
        }
        // Close connection
        if (_sql_conn != null)
        {
          _sql_conn.Close();
          _sql_conn = null;
        }

      } // try - finally

      return false;
    } // ClearAccountPointsCache

    //------------------------------------------------------------------------------
    // PURPOSE : Manteinance table EXCHANGE_RATES, insert daily rate and fill if exxists old gaps
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : 
    //------------------------------------------------------------------------------
    public static Boolean ManteinanceExchangeRates()
    {
      StringBuilder _sb;

      // Today already run
      if (m_last_running_manteinance_exchange_rates == DateTime.Today)
      {
        return true;
      }

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("EXECUTE   ManteinanceExchangeRates ");
        using (DB_TRX _db_trx = new DB_TRX())
        {
          using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
          {
            _sql_cmd.ExecuteNonQuery();
            _db_trx.Commit();
           
			return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        
      }
	  return false;
    }
  }
}
