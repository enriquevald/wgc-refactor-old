﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCashDeskCloseUnbalancedBody.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherCashDeskCloseUnbalancedBody : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCashDeskCloseUnbalancedBody(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
    {
      // 1. Load HTML structure.
      LoadVoucher("VoucherCashDeskCloseUnbalancedBody");

      //  4. Load NLS strings from resource
      LoadVoucherResources(CashierSessionStats);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
    {
      String _voucher_html;
      Int16 _set_another_currency;
      String _national_currency;
      CurrencyExchange _currency_exchange = new CurrencyExchange();
      String _iso_Code;
      Boolean _conciliate;

      _national_currency = CurrencyExchange.GetNationalCurrency();

      _voucher_html = "";
      _set_another_currency = 0;
      _conciliate = false;

      foreach (KeyValuePair<CurrencyIsoType, Decimal> collected_diff in CashierSessionStats.collected_diff) //CageAmounts)
      {
        _iso_Code = collected_diff.Key.IsoCode;

        _conciliate = collected_diff.Key.Type == CurrencyExchangeType.CARD && Misc.IsBankCardCloseCashEnabled();

        if (CashierSessionStats.collected_diff[collected_diff.Key] != 0 && !_conciliate)
        {
          if (_set_another_currency != 0)
          {
            _voucher_html += ("</td></tr><tr><td><hr noshade size=\"2\"><br /></td></tr><tr><td>");
          }
          _set_another_currency++;

          //  1. Load HTML structure for the current currency
          LoadVoucher("VoucherCashDeskCloseUnbalancedBody");

          StringBuilder _sb_currency;
          _sb_currency = new StringBuilder();

          if (collected_diff.Key.IsoCode != Cage.CHIPS_ISO_CODE)
          {
            CurrencyExchange.ReadCurrencyExchange(collected_diff.Key.Type, _iso_Code, out _currency_exchange);
            _sb_currency.AppendLine(_currency_exchange.Description);

          }
          else
          {
            _sb_currency.AppendLine(Resource.String("STR_CAGE_CASINO_CHIPS"));
          }

          // Header
          SetParameterValue("@VOUCHER_DESCRIPTION_CURRENCY", _sb_currency.ToString());
          SetParameterValue("@STR_FRM_CONTAINER_EVENTS_DATAGRID_HEADER_DESCRIPTION", Resource.String("STR_FRM_CONTAINER_EVENTS_DATAGRID_HEADER_DESCRIPTION"));
          SetParameterValue("@STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_AMOUNTS", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_AMOUNTS"));

          //DETAILS
          SetParameterValue("@STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED"));
          SetParameterValue("@STR_VOUCHER_CASHDESK_CLOSE_DIFFERENCE_EXPECTED", Resource.String("STR_VOUCHER_CASHDESK_CLOSE_DIFFERENCE_EXPECTED"));

          if (collected_diff.Value > 0)
            SetParameterValue("@STR_VOUCHER_DIFFERENCE", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA"));
          else
            SetParameterValue("@STR_VOUCHER_DIFFERENCE", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING"));


          if (_national_currency == collected_diff.Key.IsoCode && collected_diff.Key.Type == CurrencyExchangeType.CURRENCY)
          {
            AddCurrency("VOUCHER_CASHDESK_CLOSE_DIFFERENCE_EXPECTED", CashierSessionStats.final_balance);
            if (CashierSessionStats.collected.ContainsKey(collected_diff.Key))
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLLECTED", CashierSessionStats.collected[collected_diff.Key]);
            else
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLLECTED", 0);
            AddCurrency("VOUCHER_DIFFERENCE", Math.Abs(collected_diff.Value));
          }
          else
          {
            if (_national_currency == collected_diff.Key.IsoCode && collected_diff.Key.Type != CurrencyExchangeType.CURRENCY)
            {
              AddCurrency("VOUCHER_CASHDESK_CLOSE_DIFFERENCE_EXPECTED", CashierSessionStats.currencies_balance[collected_diff.Key]);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLLECTED", CashierSessionStats.collected[collected_diff.Key]);
              AddCurrency("VOUCHER_DIFFERENCE", Math.Abs(collected_diff.Value));
            }
            else
            {
              // 19-JAN-2016. Bug 8545. Si se usa divisa, currencies_balance parece no tener registros
              Decimal _currency_balance = 0;
              if (CashierSessionStats.currencies_balance.ContainsKey(collected_diff.Key))
                _currency_balance = CashierSessionStats.currencies_balance[collected_diff.Key];

              SetParameterValue("@VOUCHER_CASHDESK_CLOSE_DIFFERENCE_EXPECTED", Currency.Format(_currency_balance, _iso_Code));

              SetParameterValue("@VOUCHER_CASH_DESK_CLOSE_COLLECTED", Currency.Format(CashierSessionStats.collected[collected_diff.Key], _iso_Code));
              SetParameterValue("@VOUCHER_DIFFERENCE", Currency.Format(collected_diff.Value, _iso_Code));
            }
          }
          _voucher_html += VoucherHTML;
        }
      }
      VoucherHTML = _voucher_html;

    }
    #endregion
  }
}
