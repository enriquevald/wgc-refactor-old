﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCardTransferPoints.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  // 9. Transfer Points
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  // Origin Card Id: 
  // Origin Account Id:
  //
  // Destiny Card Id: 
  // Destiny Account Id:
  //
  // Initial Points Balance: $
  // Transfered Points Amount: $
  // Total Points:           $
  //
  //  Final Points Balance:  $
  //
  //         Voucher Footer
  /// <summary>
  /// Add Credit Voucher
  /// </summary>
  /// 
  public class VoucherCardTransferPoints : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCardTransferPoints(PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCardTransferPoints");

      // 2. Load voucher resources.
      LoadVoucherResources();

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources()
    {
      String value = "";

      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Values

      // - Title
      value = Resource.String("STR_VOUCHER_CARD_TRANSFER_POINTS_TITLE");
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_TITLE", value);

      // - General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

      value = Resource.String("STR_VOUCHER_TRANSFER_POINTS_ORIGIN");
      AddString("STR_VOUCHER_TRANSFER_POINTS_ORIGIN", value);

      value = Resource.String("STR_VOUCHER_TRANSFER_POINTS_DESTINY");
      AddString("STR_VOUCHER_TRANSFER_POINTS_DESTINY", value);

      value = Resource.String("STR_VOUCHER_CARD_CARD_ID") + ":";
      AddString("STR_VOUCHER_CARD_CARD_ID", value);

      value = Resource.String("STR_VOUCHER_CARD_ACCOUNT_ID") + ":";
      AddString("STR_VOUCHER_CARD_ACCOUNT_ID", value);

      value = Resource.String("STR_VOUCHER_CARD_MB_ID") + ":";
      AddString("STR_VOUCHER_CARD_MB_ID", value);

      // Voucher Specific 
      value = Resource.String("STR_VOUCHER_CARD_TRANSFER_POINTS_BALANCE_INITIAL") + ":";
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_BALANCE_INITIAL", value);

      value = Resource.String("STR_VOUCHER_CARD_TRANSFER_POINTS_TO_ADD") + ":";
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_TO_ADD", value);

      value = Resource.String("STR_VOUCHER_CARD_TRANSFER_POINTS_TRANSFERED") + ":";
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_TRANSFERED", value);

      value = Resource.String("STR_VOUCHER_CARD_TRANSFER_POINTS_BALANCE_FINAL") + ":";
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_BALANCE_FINAL", value);

      value = Resource.String("STR_VOUCHER_CARD_TRANSFER_POINTS_TOTAL_AMOUNT");
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_TOTAL_AMOUNT", value);

    }

    #endregion

    #region Public Methods

    #endregion
  }
}
