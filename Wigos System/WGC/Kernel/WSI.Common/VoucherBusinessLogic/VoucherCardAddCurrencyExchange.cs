﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCardAddCurrencyExchange.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
// 17-AGO-2016  ESE         Product Backlog Item 15248:TPV Televisa: Hide voucher recharge
// 01-SEP-2016  EOR         Product Backlog Item 15248: TPV Televisa: Cashier, Vouchers and tickets
// 30-SEP-2016  ETP         Bug 18003: Cashier: los vouchers de recarga no reflejan los cambios solicitados por Televisa
// 16-JUN-2017  ATB         PBI 28030: PalacioDeLosNumeros - New design in recharge vouchers
// 20-JUN-2017  ATB         PBI 28030: PalacioDeLosNumeros - New design in recharge vouchers
// 26-JUL-2017  DPC         WIGOS-4007: Creditline: missing label in voucher
// 13-OCT-2017  DHA         Bug 30243:WIGOS-5473 Cashier: it is not possible to perform a recharge by credit card when manual entry registration credit card number less than 4 digits
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherCardAddCurrencyExchange : Voucher
  {
    #region Class Attributes

    #endregion

    public Boolean Show { get; set; }

    #region Constructor


    public VoucherCardAddCurrencyExchange(String[] VoucherAccountInfo
                                          , Int64 AccountId
                                          , TYPE_SPLITS Splits
                                          , CurrencyExchangeResult ExchangeResult
                                          , CurrencyExchangeType ExchangeType
                                          , String NationalCurrency
                                          , OperationCode Operation
                                          , PrintMode Mode
                                          , Boolean ShowOnlyComission
                                          , SqlTransaction Trx
                                          , long OperationId)
      : base(Mode, Trx)
    {
      String _str_aux;
      String _voucher_to_load;
      String _str_reference;
      String _str_card_number;
      String _str_card_holder;
      String _str_bank_name;
      String _str_auth_code;
      Show = true;

      _str_reference = String.Empty;
      _str_card_number = String.Empty;
      _str_card_holder = String.Empty;
      _str_bank_name = String.Empty;
      _str_auth_code = String.Empty;

      _str_aux = "";

      // Constructor actions:
      //  0. Select the voucher to load by MovementType
      //  1. Load HTML structure.
      //  2. Load Header and Footer
      //  3. Load general voucher data.
      //  4. Load NLS strings from resource.
      //  5. Load specific voucher data.

      //  0. Select the voucher to load by MovementType

      if (ShowOnlyComission && Operation == OperationCode.CASH_ADVANCE)
      {
        _voucher_to_load = "Voucher.B.CashIn";
      }
      else
      {
        switch (ExchangeType)
        {
          case CurrencyExchangeType.CARD:
            _voucher_to_load = "VoucherCardAddBankCard";
            break;
          case CurrencyExchangeType.CHECK:
            _voucher_to_load = "VoucherCardAddCheck";
            break;
          case CurrencyExchangeType.CURRENCY:
            _voucher_to_load = "VoucherCardAddCurrencyExchange";
            break;
          default:
            return;
        }
      }

      // 1. Load HTML structure.
      LoadVoucher(_voucher_to_load);

      SetValue("CV_ACCOUNT_ID", AccountId);

      //  2. Load Header and Footer
      if (ShowOnlyComission && Operation == OperationCode.CASH_ADVANCE)
      {
        LoadHeader(Splits.company_b.header);
        LoadFooter(Splits.company_b.footer, Splits.company_b.footer_cashin);

        switch (ExchangeType)
        {
          case CurrencyExchangeType.CARD:
            AddString("VOUCHER_SPLIT_NAME", GeneralParam.GetString("Cashier.PaymentMethod", "CommissionToCompanyB.BankCard.VoucherTitle", Splits.company_b.cash_in_voucher_title));
            break;
          case CurrencyExchangeType.CHECK:
            AddString("VOUCHER_SPLIT_NAME", GeneralParam.GetString("Cashier.PaymentMethod", "CommissionToCompanyB.Check.VoucherTitle", Splits.company_b.cash_in_voucher_title));
            break;
          case CurrencyExchangeType.CURRENCY:
            AddString("VOUCHER_SPLIT_NAME", GeneralParam.GetString("Cashier.PaymentMethod", "CommissionToCompanyB.Currency.VoucherTitle", Splits.company_b.cash_in_voucher_title));
            break;
        }

        AddString("VOUCHER_SPLIT_NAME", Splits.company_b.cash_in_voucher_title);
        AddString("VOUCHER_CONCEPT", Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION"));
        SetValue("CV_TYPE", (Int32)CashierVoucherType.CashIn_B);

        AddString("STR_VOUCHER_CASH_IN_TAX", String.Empty);
        AddString("VOUCHER_CASH_IN_TAX", String.Empty);

        SetParameterValue("@VOUCHER_CARD_ADD_TEXT_TOTAL_AMOUNT",
            "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">" +
              "<tr>" +
                "<td align=\"center\">TOTAL</td>" +
              "</tr>" +
              "<tr>" +
                "<td align=\"center\"><B>@VOUCHER_CARD_ADD_TOTAL_AMOUNT</B></td>" +
              "</tr>" +
            "</table>" +
            "<hr noshade size=\"4\">");

        AddCurrency("VOUCHER_CARD_ADD_TOTAL_AMOUNT", ExchangeResult.Comission, Splits.company_b.total_in_letters);
      }
      else
      {
        // If the voucher mode is 1 (PalacioDeLosNumeros) it is necessary to modify the footer
        if (Misc.GetVoucherMode() == 1)
        {
          _str_card_number = ExchangeResult.BankTransactionData.DocumentNumber;
          _str_card_holder = ExchangeResult.BankTransactionData.HolderName;

          if (_str_card_number != null)
          {
            _str_card_number = _str_card_number.PadLeft(4, ' ');
          }

          Splits.company_a.footer = Splits.company_a.footer.Replace("@OPERATION_NUMBER", OperationId.ToString());
          Splits.company_a.footer = (!String.IsNullOrEmpty(_str_card_number)) ? Splits.company_a.footer.Replace("@CARD_LAST_4_DIGITS", _str_card_number.Substring(_str_card_number.Length - 4, 4)) : Splits.company_a.footer.Replace("@CARD_LAST_4_DIGITS", "_____");
          Splits.company_a.footer = (!String.IsNullOrEmpty(_str_card_holder)) ? Splits.company_a.footer.Replace("@CARD_HOLDER", _str_card_holder) : Splits.company_a.footer.Replace("@CARD_HOLDER", "_________________________");
        }
        LoadHeader(Splits.company_a.header);
        LoadFooter(Splits.company_a.footer, Splits.company_a.footer_cashin);
      }

      LoadVoucherGeneralData();
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      if (ShowOnlyComission && Operation == OperationCode.CASH_ADVANCE)
      {
        AddCurrency("VOUCHER_CARD_ADD_AMOUNT_TO_ADD", ExchangeResult.Comission);
        _str_aux = GeneralParam.GetString("Cashier", "CashInTax.VoucherText", Resource.String("STR_MOVEMENT_TYPE_CASH_IN_TAX")) + ":";
        AddString("STR_VOUCHER_CASH_IN_TAX", _str_aux);
      }
      else
      {
        // Change applied
        if (ExchangeResult.WasForeingCurrency)
        {
          _str_aux = Resource.String("STR_VOUCHER_APPLIED_EXCHANGE");

          SetParameterValue("@VOUCHER_CHANGE_APPLIED",
          "<tr>" +
            "<td align=\"left\"  width=\"35%\">" + _str_aux + " " + ExchangeResult.InCurrencyCode + ":</td>" +
            "<td align=\"right\" width=\"65%\">" + CurrencyExchange.ChangeRateFormat(ExchangeResult.ChangeRate) + " " + NationalCurrency +
            "</td>" +
          "</tr>");

          // Total Currency Exchange
          AddString("VOUCHER_EQUIVAL_CHANGE_AMOUNT", Currency.Format(ExchangeResult.GrossAmount, NationalCurrency));
          _str_aux = Resource.String("STR_FRM_AMOUNT_INPUT_EQUALS");
          AddString("STR_VOUCHER_EQUIVAL_CHANGE", _str_aux);

          //  5. Load specific voucher data.
          // In amount
          AddString("VOUCHER_IN_AMOUNT", Currency.Format(ExchangeResult.InAmount, ExchangeResult.InCurrencyCode));

          // Card price
          AddString("VOUCHER_CARD_PRICE_AMOUNT", Currency.Format(ExchangeResult.CardPrice, NationalCurrency));

          // Commission
          AddString("VOUCHER_COMISSION_AMOUNT", Currency.Format(ExchangeResult.Comission, NationalCurrency));

          // NR2 promotion. Control 0 value for promotion
          if (ExchangeResult.ArePromotionsEnabled)
          {
            AddString("VOUCHER_NR2_AMOUNT", Currency.Format(ExchangeResult.NR2Amount, NationalCurrency));
            _str_aux = Resource.String("STR_VOUCHER_NR2") + ":";
          }
          else
          {
            AddString("VOUCHER_NR2_AMOUNT", "");
            _str_aux = "";
          }
          AddString("STR_VOUCHER_NR2", _str_aux);

          // Total amount
          AddString("VOUCHER_TOTAL_AMOUNT", Currency.Format(ExchangeResult.NetAmount, NationalCurrency));
        }
        else
        {
          AddString("VOUCHER_CHANGE_APPLIED", "");
          AddString("VOUCHER_EQUIVAL_CHANGE_AMOUNT", "");
          AddString("STR_VOUCHER_EQUIVAL_CHANGE", "");

          //  5. Load specific voucher data.
          // In amount
          AddString("VOUCHER_IN_AMOUNT", ((Currency)ExchangeResult.InAmount).ToString());

          // Card price
          AddString("VOUCHER_CARD_PRICE_AMOUNT", ((Currency)ExchangeResult.CardPrice).ToString());

          // Commission
          AddString("VOUCHER_COMISSION_AMOUNT", ((Currency)ExchangeResult.Comission).ToString());

          // NR2 promotion. Control 0 value for promotion
          if (ExchangeResult.ArePromotionsEnabled && Operation == OperationCode.CASH_IN)
          {
            AddString("VOUCHER_NR2_AMOUNT", ((Currency)ExchangeResult.NR2Amount).ToString());
            _str_aux = Resource.String("STR_VOUCHER_NR2") + ":";
          }
          else
          {
            AddString("VOUCHER_NR2_AMOUNT", "");
            _str_aux = "";
          }
          AddString("STR_VOUCHER_NR2", _str_aux);

          // Total amount
          AddString("VOUCHER_TOTAL_AMOUNT", ((Currency)ExchangeResult.NetAmount).ToString());
        }

      }
      if (_voucher_to_load == "VoucherCardAddCurrencyExchange")
      {
        AddString("CURRENCY_STR_VOUCHER_TOTAL_AMOUNT", "");
        AddString("CURRENCY_VOUCHER_TOTAL_AMOUNT", "");
      }

      //  4. Load NLS strings from resource
      if (!LoadVoucherResources(ExchangeType, Operation))
      {
        return;
      }

    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private Boolean LoadVoucherResources(CurrencyExchangeType ExchangeType, OperationCode Operation)
    {
      String value = "";

      // NLS Values

      // - Title
      switch (ExchangeType)
      {
        case CurrencyExchangeType.CURRENCY:
          value = GeneralParam.GetString("Cashier.Voucher", "PaymentMethod.Currency.Title");
          if (String.IsNullOrEmpty(value))
          {
            value = Resource.String("STR_VOUCHER_CURRENCY_EXCHANGE");
          }
          AddString("STR_VOUCHER_TITLE", value);
          break;
        case CurrencyExchangeType.CARD:
          switch (Operation)
          {
            case OperationCode.CASH_IN:
              value = GeneralParam.GetString("Cashier.Voucher", "PaymentMethod.BankCard.Title");
              if (String.IsNullOrEmpty(value))
              {
                value = Resource.String("STR_VOUCHER_CURRENCY_CARD_EXCHANGE");
              }
              break;
            case OperationCode.CASH_ADVANCE:
              value = GeneralParam.GetString("Cashier.Voucher", "PaymentMethod.CashAdvance.BankCard.Title");
              if (String.IsNullOrEmpty(value))
              {
                value = Resource.String("STR_VOUCHER_CURRENCY_CARD_CASH_ADVANCE");
              }
              break;
          }

          AddString("STR_VOUCHER_TITLE", value);
          break;
        case CurrencyExchangeType.CHECK:
          switch (Operation)
          {
            case OperationCode.CASH_IN:
              value = GeneralParam.GetString("Cashier.Voucher", "PaymentMethod.Check.Title");
              if (String.IsNullOrEmpty(value))
              {
                value = Resource.String("STR_VOUCHER_CURRENCY_CHECK_EXCHANGE");
              }
              break;
            case OperationCode.CASH_ADVANCE:
              value = GeneralParam.GetString("Cashier.Voucher", "PaymentMethod.CashAdvance.Check.Title");
              if (String.IsNullOrEmpty(value))
              {
                value = Resource.String("STR_VOUCHER_CURRENCY_CHECK_CASH_ADVANCE");
              }
              break;
          }
          AddString("STR_VOUCHER_TITLE", value);
          break;
        case CurrencyExchangeType.CASINOCHIP:
          AddString("STR_VOUCHER_TITLE", Resource.String("STR_VOUCHER_CASINO_CHIPS_EXCHANGE"));
          break;
        default:
          Log.Error("VoucherCardAddCurrencyExchange. ExchangeType not valid: " + ExchangeType.ToString());
          return false;
      }

      // - General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

      value = Resource.String("STR_VOUCHER_IN_CHARGED") + ":";
      AddString("STR_VOUCHER_IN_AMOUNT", value);

      value = Misc.GetCardPlayerConceptName() + ":";
      AddString("STR_VOUCHER_CARD_PRICE", value);

      value = Resource.String("STR_VOUCHER_COMISSION") + ":";
      AddString("STR_VOUCHER_COMISSION", value);

      switch (Operation)
      {
        case OperationCode.CASH_IN:
          value = Resource.String("STR_VOUCHER_RECHARGE") + ":";
          break;
        case OperationCode.CASH_ADVANCE:
          value = Resource.String("STR_VOUCHER_CASH_ADVANCE") + ":";
          break;
        case OperationCode.CREDIT_LINE:
          value = Resource.String("STR_FRM_ACTION_CREDITLINE_PAYBACK") + ":";
          break;
        default:
          value = "";
          break;
      }
      AddString("STR_VOUCHER_TOTAL_AMOUNT", value);

      return true;
    }

    #endregion
  }
}
