﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCardDraw.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;


namespace WSI.Common
{
  // 11. Draw
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  // Initial Amount:   $
  // Open Amount:      $
  //
  //  Final Balance:  $
  //
  //         Voucher Footer
  public class VoucherCardDraw : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCardDraw(PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCardDraw");

      // 2. Load voucher resources.
      LoadVoucherResources(false, false);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(Boolean ShowDeposit, Boolean Promotion)
    {
      String value = "";

      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Values

      // - Title
      value = Resource.String("STR_VOUCHER_CARD_DRAW_TITLE");
      AddString("STR_VOUCHER_CARD_DRAW_TITLE", value);

      // - General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

      value = Resource.String("STR_VOUCHER_CARD_DRAW_TICKET_NUMBER") + ":";
      AddString("STR_VOUCHER_CARD_DRAW_TICKET_NUMBER", value);

    }

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Generates String Numbers - Voucher
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Parameter
    //          - FirstDrawNum
    //          - LastDrawNum
    //          - MaxNumber
    //          - HasBingoFormat
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true or false
    // 
    //   NOTES:
    public Boolean AddStringNumbers(String Parameter,
                                     Int64 FirstDrawNum,
                                     Int64 LastDrawNum,
                                     Int64 MaxNumber,
                                     Boolean HasBingoFormat)
    {
      Int64 _draw_num_mod;
      Int64 _idx_draw_num;
      String _table_draw_num;
      String _str_draw_num;
      Int32 _max_number_digits;
      String _font_size;


      // MaxNumber is used to define the digits to format the numbers.
      _max_number_digits = MaxNumber.ToString().Length;

      if (_max_number_digits >= 6)
      {
        _font_size = "100";
      }
      else
      {
        _font_size = "150";
      }

      if (HasBingoFormat)
      {
        _table_draw_num = "<td>";
      }
      else
      {
        _table_draw_num = "<td style=\"font-size:150%\">";
      }

      _table_draw_num += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" STYLE=\"font-size:" + _font_size + "%\" >";

      if (!HasBingoFormat)
      {
        _table_draw_num += "<tr>";
      }

      for (_idx_draw_num = FirstDrawNum; _idx_draw_num <= LastDrawNum; _idx_draw_num++)
      {
        _draw_num_mod = (_idx_draw_num - FirstDrawNum) % 2;

        if (HasBingoFormat)
        {
          _str_draw_num = DrawBingoFormat.NumberToBingo(_idx_draw_num, MaxNumber);
          _table_draw_num += "<tr><td align=\"center\">" + _str_draw_num + "</td></tr>";
          if (_idx_draw_num < LastDrawNum)
          {
            _table_draw_num += "<tr><td><table><tr><td>&nbsp;</td></tr></table></td></tr>";
          }
        }
        else
        {
          _str_draw_num = _idx_draw_num.ToString().PadLeft(_max_number_digits, '0');

          switch (_draw_num_mod)
          {
            case 0:
              _table_draw_num += "<td align=\"left\">" + _str_draw_num + "</td>";
              break;
            //case 1:
            //  _table_draw_num += "<td align=\"center\">" + _str_draw_num + "</td>";
            //  break;
            case 1:
              _table_draw_num += "<td align=\"right\">" + _str_draw_num + "</td>";
              if (_idx_draw_num < LastDrawNum)
              {
                _table_draw_num += "</tr><tr>";
              }
              break;
          }
        }
      }

      if (!HasBingoFormat)
      {
        _table_draw_num += "</tr>";
      }
      _table_draw_num += "</table></td>";

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      // Insert title into html voucher
      return SetParameterValue(Parameter, _table_draw_num);
    }

    #endregion
  }
}
