﻿//------------------------------------------------------------------------------
// Copyright © 2007-2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : VoucherTotalizingStrip.cs
// 
//   DESCRIPTION : Print totaling strip
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-JUL-2016 ESE    Product Backlog Item 15239:TPV Televisa: Cashier, totalizing strip
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using WSI.Common;

namespace WSI.Common
{
  //ESE 17-Mar-2016
  public class VoucherTotalizingStrip : Voucher
  {
    #region Constructor

    public VoucherTotalizingStrip(DataTable Pinpad_Trans
                              , DateTime CashierSessionDatetime
                              , PrintMode Mode
                              , SqlTransaction Trx)
      : base(Mode, Trx)
    {
      // 1. Load HTML structure.
      LoadVoucher("VoucherTotalizingStrip");

      //  4. Load NLS strings from resource
      LoadVoucherResources(Pinpad_Trans, CashierSessionDatetime);
    }

    #endregion

    #region Private Methods
    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(DataTable Pinpad_Trans, DateTime CashierSessionDatetime)
    {
      String _str_value;
      StringBuilder _sb_chips_denomination_header;
      StringBuilder _sb_chips_denomination;
      String _hr_4;
      String _hr_2;
      StringBuilder _str_ticket_head;
      StringBuilder _str_ticket_body;
      StringBuilder _str_mount;

      _str_ticket_head = new StringBuilder();
      _str_ticket_body = new StringBuilder();
      _str_mount = new StringBuilder();
      _sb_chips_denomination_header = new StringBuilder();
      _sb_chips_denomination = new StringBuilder();
      _hr_2 = String.Empty;
      _hr_4 = String.Empty;
      _str_value = String.Empty;

      //Load general voucher data.
      LoadVoucherGeneralData();

      SetValue("CV_TYPE", (Int32)CashierVoucherType.TotalizingStrip);


      //Voucher title
      _str_value = Resource.String("STR_CASHIER_TOTALIZING_STRIP");
      AddString("STR_TOTALIZING_STRIP_TITLE", _str_value);
      AddString("STR_TOTALIZING_STRIP_SESSION_NAME", Resource.String("STR_VOUCHER_CASH_DESK_OPEN_TITLE") + ": " + CashierSessionDatetime.ToString("dd/MM/yyyy H:mm"));

      //Voucher header
      _str_ticket_head.AppendLine("	<tr>  ");
      _str_ticket_head.AppendLine("    <td align='center' width='25%'>" + Resource.String("STR_FRM_CONTAINER_EVENTS_DATAGRID_HEADER_DATE") + "</td>");
      _str_ticket_head.AppendLine("    <td align='center' width='30%'>" + Resource.String("STR_HEADER_VOUCHER_REFERENCE") + "</td>");
      _str_ticket_head.AppendLine("    <td align='center' width='25%'>" + Resource.String("STR_HEADER_VOUCHER_AMOUNT") + "</td>");
      _str_ticket_head.AppendLine("    <td align='center' width='25%'>" + Resource.String("STR_HEADER_VOUCHER_CARD") + "</td>");
      _str_ticket_head.AppendLine("	</tr>  ");
      SetParameterValue("@VOUCHER_TOTALIZING_HEADER", _str_ticket_head.ToString());

      //Voucher body
      foreach (DataRow _aux_row in Pinpad_Trans.Rows)
      {
        _str_ticket_body.AppendLine("	<tr>  ");
        _str_ticket_body.AppendLine("    <td align='center' width='25%'>" + Format.DBDatetimeToLocalDateTime(_aux_row["PT_CREATED"].ToString()).ToString("H:mm") + "</td>");
        _str_ticket_body.AppendLine("    <td align='center' width='25%'>" + _aux_row["PT_REFERENCE"] + "</td>");
        _str_ticket_body.AppendLine("    <td align='center' width='25%'>" + decimal.Round(Decimal.Parse(_aux_row["PT_TOTAL_AMOUNT"].ToString()), 2).ToString("C") + "</td>");
        _str_ticket_body.AppendLine("    <td align='center' width='25%'>" + _aux_row["PT_CARD_NUMBER"].ToString().Replace("*", String.Empty).PadLeft(8, '*') + "</td>");
        _str_ticket_body.AppendLine("	</tr>  ");
      }
      SetParameterValue("@VOUCHER_TOTALIZING_BODY", _str_ticket_body.ToString());
    }

    #endregion
  }
}
