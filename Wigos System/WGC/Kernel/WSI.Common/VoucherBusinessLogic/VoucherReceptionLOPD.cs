﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherReceptionLOPD.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherReceptionLOPD : Voucher
  {

    #region Constructor

    public VoucherReceptionLOPD(String[] VoucherAccountInfo
                            , CashierVoucherType VoucherType
                            , Int64 AccountId
                            , PrintMode Mode
                            , SqlTransaction SQLTransaction)
      : base(Mode, VoucherType, SQLTransaction)
    {
      InitializeVoucher(VoucherAccountInfo, AccountId);
    }

    private void InitializeVoucher(String[] VoucherAccountInfo
                                  , Int64 AccountId)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherReceptionLOPD");

      SetValue("CV_ACCOUNT_ID", AccountId);

      // 2. Load voucher resources.
      LoadVoucherResources(VoucherAccountInfo);

      //VoucherSequenceId = SequenceId.AccountId;

      LoadFooter(GeneralParam.GetString("Cashier.Voucher", "Footer"), String.Empty);


      // 3. Load general voucher data.
      LoadVoucherGeneralData(true);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.P1550ff12
    /// 
    /// </summary>
    private void LoadVoucherResources(String[] VoucherAccountInfo)
    {
      String value = "";
      String _national_currency;

      _national_currency = CurrencyExchange.GetNationalCurrency();

      // - Title
      value = Resource.String("STR_VOUCHER_RECEPTION_TITLE");
      AddString("STR_VOUCHER_RECEPTION_TITLE", value);

      //Account Info
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      SetParameterValue("@VOUCHER_LOPD_DESCRIPTION1", GeneralParam.GetString("Reception", "Agreement.Text1"));
      SetParameterValue("@VOUCHER_LOPD_DESCRIPTION2", GeneralParam.GetString("Reception", "Agreement.Text2"));
      SetParameterValue("@VOUCHER_LOPD_DESCRIPTION3", GeneralParam.GetString("Reception", "Agreement.Text3"));
      SetParameterValue("@VOUCHER_LOPD_DESCRIPTION4", GeneralParam.GetString("Reception", "Agreement.Text4"));

    } // LoadVoucherResources

    #endregion
  }
}
