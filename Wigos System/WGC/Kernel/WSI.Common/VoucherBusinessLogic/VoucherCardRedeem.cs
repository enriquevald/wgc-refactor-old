﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCardRedeem.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  // 2. Card Redeem (Partial/Total) (Reintegro Parcial/Total)
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  // Card Id: 
  // Account Id:
  //
  // Current Balance: $
  // Redeem Amount:   $
  // Total:           $
  //
  //  Final Balance:  $
  //
  //         Voucher Footer

  public class VoucherCardRedeem : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCardRedeem(CASH_MODE RedeemType, PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCardRedeem");

      // 2. Load voucher resources.
      LoadVoucherResources(RedeemType);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(CASH_MODE RedeemType)
    {
      String value = "";

      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Values

      // - Title
      if (RedeemType == CASH_MODE.TOTAL_REDEEM)
      {
        // Total Redeem
        value = Resource.String("STR_VOUCHER_CARD_REDEEM_TOTAL_TITLE");
      }
      else
      {
        // Partial Redeem
        value = Resource.String("STR_VOUCHER_CARD_REDEEM_PARTIAL_TITLE");
      }
      AddString("STR_VOUCHER_CARD_REDEEM_TITLE", value);

      // - General
      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

      value = Resource.String("STR_VOUCHER_CARD_MB_ID") + ":";
      AddString("STR_VOUCHER_CARD_MB_ID", value);

      // - Voucher Specific
      value = Resource.String("STR_VOUCHER_CARD_REDEEM_BALANCE_INITIAL") + ":";
      AddString("STR_VOUCHER_CARD_REDEEM_BALANCE_INITIAL", value);

      value = Resource.String("STR_VOUCHER_CARD_REDEEM_CASH_FROM_CASH_IN") + ":";
      AddString("STR_VOUCHER_CARD_REDEEM_CASH_FROM_CASH_IN", value);

      value = Resource.String("STR_VOUCHER_CARD_REDEEM_CASH_FROM_WON") + ":";
      AddString("STR_VOUCHER_CARD_REDEEM_CASH_FROM_WON", value);

      value = Resource.String("STR_VOUCHER_CARD_REDEEM_CASH_FROM_TAXES") + ": ";
      AddString("STR_VOUCHER_CARD_REDEEM_CASH_FROM_TAXES", value);

      value = Resource.String("STR_VOUCHER_CARD_REDEEM_TOTAL") + ":";
      AddString("STR_VOUCHER_CARD_REDEEM_TOTAL", value);

      value = Resource.String("STR_VOUCHER_CARD_REDEEM_BALANCE_FINAL") + ":";
      AddString("STR_VOUCHER_CARD_REDEEM_BALANCE_FINAL", value);

      value = Resource.String("STR_VOUCHER_CLIENT_SIGN") + ":";
      AddString("STR_VOUCHER_CLIENT_SIGN", value);

      value = Resource.String("STR_VOUCHER_CLIENT_ACCEPT");
      AddString("STR_VOUCHER_CLIENT_ACCEPT", value);

      value = "";
      if (RedeemType == CASH_MODE.TOTAL_REDEEM)
      {
        // Total Redeem
        value = WSI.Common.Misc.GetCardPlayerConceptName() + ":";
      }
      AddString("STR_VOUCHER_CARD_REDEEM_CARD_DEPOSIT", value);

      value = Resource.String("STR_VOUCHER_CARD_REDEEM_AMOUNT_TO_REDEEM");
      AddString("STR_VOUCHER_CARD_REDEEM_AMOUNT_TO_REDEEM", value);
    }

    #endregion

    #region Public Methods

    #endregion

  }
}
