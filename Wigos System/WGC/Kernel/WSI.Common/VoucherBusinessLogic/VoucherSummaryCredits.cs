﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherSummaryCredits.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 28-SEP-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 28-SEP-2016  EOR         First release.
// 29-SEP-2016  EOR         PBI 17448: Televisa - Sorteo de Caja (Premio en efectivo) - Voucher
// 11-OCT-2016  EOR         Fixed Bug 18922: Televisa - Draw Cash (Cash prize) - Not show concept of cupón promocional in the ticket
// 07-NOV-2016  ATB         Bug 20147:Televisa: Cambios solicitados por Televisa para "Movimientos en caja"
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;


namespace WSI.Common
{
  /// EOR 28-SEP-2016
  /// <summary>
  /// Voucher LayOut for Participation in Draw
  /// </summary>
  public class VoucherSummaryCredits : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherSummaryCredits(String[] VoucherAccountInfo
                                , TYPE_SPLITS Splits
                                , Currency CustomerCredits
                                , Currency ParticipationDraw
                                , Currency CustomerCreditsOthers
                                , Currency CouponPromo
                                , PrintMode Mode
                                , SqlTransaction Trx
      )
      : base(Mode, Trx)
    {
      // 1. Load HTML structure.
      LoadVoucher("VoucherSummaryCredits");

      //  4. Load NLS strings from resource
      LoadVoucherResources(VoucherAccountInfo, Splits, CustomerCredits, ParticipationDraw, CustomerCreditsOthers, CouponPromo);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(String[] VoucherAccountInfo
                                    , TYPE_SPLITS Splits
                                    , Currency CustomerCredits
                                    , Currency ParticipationDraw
                                    , Currency CustomerCreditsOthers
                                    , Currency CouponPromo)
    {
      Boolean _hide_total;
      Boolean _add_currency_in_letters;
      Currency _total_ticket;

      SetValue("CV_TYPE", (Int32)CashierVoucherType.SummaryCredits);
      VoucherSequenceId = SequenceId.VoucherSummaryCredits;

      //  2. Load Header and Footer
      LoadHeader(Splits.company_a.header);
      LoadFooter(Splits.company_a.footer, Splits.company_a.footer_cashin);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      AddString("VOUCHER_SPLIT_NAME", Resource.String("STR_VOUCHER_TITLE_SUMMARY_CREDITS"));

      // ATB 04-NOV-2016 Bug 20147:Televisa: Cambios solicitados por Televisa para "Movimientos en caja"
      if (WSI.Common.Misc.IsVoucherModeTV())
      {
        if (ParticipationDraw > 0)
        {
          ParticipationDraw = ParticipationDraw * -1;
          Currency _final_credits = (CustomerCredits + ParticipationDraw);
          SetParameterValue("@VOUCHER_CONCEPT_CUSTOMER_CREDIT",
              "<tr>" +
                "<td align=\"left\" width=\"50%\">" + Splits.company_a.name + "</td>" +
                "<td align=\"right\" width=\"50%\">" + _final_credits + "</td>" +
              "</tr>");
        }
        else
        {
          SetParameterValue("@VOUCHER_CONCEPT_CUSTOMER_CREDIT",
              "<tr>" +
                "<td align=\"left\" width=\"50%\">" + Splits.company_a.name + "</td>" +
                "<td align=\"right\" width=\"50%\">" + CustomerCredits.ToString() + "</td>" +
              "</tr>");

        }
        SetParameterValue("@VOUCHER_CONCEPT_PARTICIPATION_DRAW", "");
      }
      else
      {
        SetParameterValue("@VOUCHER_CONCEPT_CUSTOMER_CREDIT",
              "<tr>" +
                "<td align=\"left\" width=\"50%\">" + Splits.company_a.name + "</td>" +
                "<td align=\"right\" width=\"50%\">" + CustomerCredits.ToString() + "</td>" +
              "</tr>");

        if (ParticipationDraw > 0)
        {
          ParticipationDraw = ParticipationDraw * -1;

          SetParameterValue("@VOUCHER_CONCEPT_PARTICIPATION_DRAW",
              "<tr>" +
                "<td align=\"left\" width=\"50%\">" + Resource.String("STR_FRM_CASH_DESK_DRAW_PARTICIPATION_TITLE") + "</td>" +
                "<td align=\"right\" width=\"50%\">" + ParticipationDraw.ToString() + "</td>" +
              "</tr>");
        }
        else
        {
          SetParameterValue("@VOUCHER_CONCEPT_PARTICIPATION_DRAW", "");
        }
      }


      if (CustomerCreditsOthers > 0)
      {
        SetParameterValue("@VOUCHER_CONCEPT_CUSTOMER_OTHERS",
            "<tr>" +
              "<td align=\"left\" width=\"50%\">" + Splits.company_a.name + " " + Resource.String("STR_GENERAL_CATALOG_REASON_OTHERS").ToLower() + "</td>" +
              "<td align=\"right\" width=\"50%\">" + CustomerCreditsOthers.ToString() + "</td>" +
            "</tr>");
      }
      else
      {
        SetParameterValue("@VOUCHER_CONCEPT_CUSTOMER_OTHERS", "");
      }

      if (Splits.promo_and_coupon)
      {
        SetParameterValue("@VOUCHER_CONCEPT_COUPON_PROMO",
            "<tr>" +
              "<td align=\"left\" width=\"50%\">" + Splits.text_promo_and_coupon + "</td>" +
              "<td align=\"right\" width=\"50%\">" + CouponPromo.ToString() + "</td>" +
            "</tr>");
      }
      else
      {
        CouponPromo = 0;
        SetParameterValue("@VOUCHER_CONCEPT_COUPON_PROMO", "");
      }

      _hide_total = Splits.company_a.hide_total;
      _add_currency_in_letters = Splits.company_a.total_in_letters;
      _total_ticket = CustomerCredits + ParticipationDraw + CustomerCreditsOthers + CouponPromo;

      if (_hide_total)
      {
        SetParameterValue("@VOUCHER_CARD_ADD_TEXT_TOTAL_AMOUNT", "");
      }
      else
      {
        SetParameterValue("@VOUCHER_CARD_ADD_TEXT_TOTAL_AMOUNT",
            "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">" +
              "<tr>" +
                "<td align=\"center\">TOTAL</td>" +
              "</tr>" +
              "<tr>" +
                "<td align=\"center\"><B>@VOUCHER_CARD_ADD_TOTAL_AMOUNT</B></td>" +
              "</tr>" +
            "</table>" +
            "<hr noshade size=\"4\">");

        AddCurrency("VOUCHER_CARD_ADD_TOTAL_AMOUNT", _total_ticket, _add_currency_in_letters);
      }
    }

    #endregion

  }
}


