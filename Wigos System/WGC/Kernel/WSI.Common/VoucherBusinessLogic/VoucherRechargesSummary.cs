﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherRechargesSummary.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  // 17. Recharges Summary
  // Format:
  //    Voucher Header
  //  
  //      Recharges List
  //
  //   Voucher Footer
  public class VoucherRechargesSummary : Voucher
  {
    #region Constructor

    public VoucherRechargesSummary(String[] VoucherAccountInfo,
                                   String[] NotesList,
                                   Currency TotalRecharges,
                                   Int64 AccountId,
                                   PrintMode Mode,
                                   SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load general voucher data.
      //  3. Load specific voucher data.

      // 1. Load HTML structure.
      LoadVoucher("Voucher.Recharges.Summary");

      LoadHeader(GeneralParam.Value("WigosKiosk", "Voucher.Recharges.Header"));
      LoadFooter(GeneralParam.Value("WigosKiosk", "Voucher.Recharges.Footer"), "");

      SetValue("CV_ACCOUNT_ID", AccountId);

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load specific voucher data.
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      AddString("VOUCHER_RECHARGES_SUMMARY_TITLE", Resource.String("STR_VOUCHER_RECHARGES_SUMMARY_TITLE"));

      AddString("VOUCHER_RECHARGES_SUMMARY_LIST", Resource.String("STR_VOUCHER_RECHARGES_SUMMARY_LIST"));

      AddString("VOUCHER_RECHARGES_SUMMARY_NAME", Resource.String("STR_VOUCHER_RECHARGES_SUMMARY_NAME"));

      AddMultipleLineString("VOUCHER_NOTES_LIST", NotesList);

      AddCurrency("VOUCHER_TOTAL_RECHARGES", TotalRecharges);

    } // VoucherCashIn

    #endregion
  }
}
