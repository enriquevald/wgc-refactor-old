﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherGeneratePIN.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  // 16. Generate PIN
  // Format:
  //     Voucher Logo
  //    Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:  
  // Cusotomer:  
  // Account Id:
  // Card Id: 
  //
  //      New PIN
  //
  //   Voucher Footer
  /// <summary>
  /// Add Credit Voucher
  /// </summary>
  public class VoucherGeneratePIN : Voucher
  {
    #region Constructor

    public VoucherGeneratePIN(String[] VoucherAccountInfo
                            , Int64 AccountId
                            , TYPE_SPLITS SplitInformation
                            , String NewPin
                            , PrintMode Mode
                            , SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load general voucher data.
      //  3. Load specific voucher data.

      // 1. Load HTML structure.
      LoadVoucher("Voucher.PIN.Change");

      SetValue("CV_ACCOUNT_ID", AccountId);

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load specific voucher data.     
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      AddString("VOUCHER_CARD_CHANGE_PIN_INFO", Resource.String("STR_VOUCHER_CARD_CHANGE_PIN_NEW_PIN"));
      AddString("VOUCHER_CARD_CHANGE_PIN_NEW_PIN", NewPin);

    } // VoucherCashIn

    #endregion
  }
}
