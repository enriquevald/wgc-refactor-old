﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherHandpayAuthorized.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherHandpayAuthorized : Voucher
  {
    #region Class Attributes

    public class VoucherHandpayAuthorizedInputParams
    {
      public String TerminalName;
      public String TerminalProvider;
      public String TypeName;
      public Currency TotalRedeemed;
      public Currency Taxes;
      public Currency TotalToPay;
      public PrintMode Mode;
      public String HandpayNumber;
      public Currency TaxAmount;
      public Currency TaxBaseAmount;
      public Decimal TaxPct;
    }

    #endregion

    #region Constructor

    public VoucherHandpayAuthorized(VoucherHandpayAuthorizedInputParams InputParams, OperationCode OperationCode,
                                    SqlTransaction SQLTransaction)
      : base(InputParams.Mode, OperationCode, SQLTransaction)
    {
      Currency _rounding;
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherAuthorization");

      // 2. Load voucher resources.
      LoadVoucherResources();

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      // Setup voucher's data
      //    - Account Number and Account Holder Name
      //    - Pay withot tax
      //    - Taxes
      //    - Total to pay

      AddString("VOUCHER_AUTHORITZATION_TERMINAL_NAME", InputParams.TerminalName);
      AddString("VOUCHER_AUTHORITZATION_PROVIDER_NAME", InputParams.TerminalProvider);
      AddString("VOUCHER_AUTHORITZATION_TYPE", InputParams.TypeName);

      AddCurrency("VOUCHER_AUTHORITZATION_INITIAL_AMOUNT_TO_TRANSFER", InputParams.TotalRedeemed);
      AddCurrency("VOUCHER_AUTHORITZATION_DEDUCTIONS", -InputParams.Taxes);
      AddCurrency("VOUCHER_AUTHORITZATION_AMOUNT_TO_TRANSFER", InputParams.TotalToPay);

      _rounding = (InputParams.TotalToPay - InputParams.TotalRedeemed) + InputParams.Taxes;

      if (Utils.IsTitoMode() && _rounding != 0)
      {
        AddString("STR_VOUCHER_CARD_HANDPAY_ROUNDING", Resource.String("STR_UC_CARD_BALANCE_ROUNDING") + ":");
        AddCurrency("VOUCHER_CARD_HANDPAY_ROUNDING", _rounding);
      }
      else
      {
        AddString("STR_VOUCHER_CARD_HANDPAY_ROUNDING", "");
        AddString("VOUCHER_CARD_HANDPAY_ROUNDING", "");
      }

      if (GeneralParam.GetBoolean("Cashier.Voucher", "Handpay.TotalInLetters", false))
      {
        AddCurrencyInLetters("VOUCHER_AMOUNT_TO_TRANSFER_STRING", InputParams.TotalToPay);
      }
      else
      {
        AddString("VOUCHER_AMOUNT_TO_TRANSFER_STRING", "");
      }

      SetSignatureRoomByVoucherType(this.GetType());

      //      - Voucher track data
      AddString("VOUCHER_HANDPAY_REQUEST_TRACKDATA", InputParams.HandpayNumber);

      if (WSI.Common.TITO.Utils.IsTitoMode())
      {
        String _param_name_tax;
        String _pct;
        StringBuilder _html_code;

        _html_code = new StringBuilder();

        _html_code.AppendLine("<tr>");
        _html_code.AppendLine("	<td>");
        _html_code.AppendLine("		<hr noshade size='4' />");
        _html_code.AppendLine("	</td>");
        _html_code.AppendLine("</tr>");
        _html_code.AppendLine("<tr>");
        _html_code.AppendLine("	<td>");
        _html_code.AppendLine("		<table width='100%' border='0' cellpadding='0' cellspacing='0'>");
        _html_code.AppendLine("			<tr>");
        _html_code.AppendLine("				<td colspan='3'>");
        _html_code.AppendLine("					@VOUCHER_TAX_DETAIL:");
        _html_code.AppendLine("				</td>");
        _html_code.AppendLine("			</tr>");
        _html_code.AppendLine("			<tr>");
        _html_code.AppendLine("				<td width='5%'>");
        _html_code.AppendLine("				</td>");
        _html_code.AppendLine("				<td>");
        _html_code.AppendLine("					@VOUCHER_BASE_TAX_NAME:");
        _html_code.AppendLine("				</td>");
        _html_code.AppendLine("				<td width='40%' align='right'>");
        _html_code.AppendLine("					@VOUCHER_BASE_TAX");
        _html_code.AppendLine("				</td>");
        _html_code.AppendLine("			</tr>");
        _html_code.AppendLine("			@TAXES");
        _html_code.AppendLine("		</table>");
        _html_code.AppendLine("	</td>");
        _html_code.AppendLine("</tr>");
        _html_code.AppendLine("<tr>");
        _html_code.AppendLine("	<td>");
        _html_code.AppendLine("		<hr noshade size='4' />");
        _html_code.AppendLine("	</td>");
        _html_code.AppendLine("</tr>");

        SetParameterValue("@TABLE_WITHHOLDING", _html_code.ToString());

        _param_name_tax = Resource.String("STR_VOUCHER_HANDPAY_WITHHOLDING");
        _pct = String.Format("{0:P2}", InputParams.TaxPct);

        AddString("VOUCHER_TAX_DETAIL", Resource.String("STR_HANDPAY_TAX_DETAIL", _param_name_tax.ToString()));
        AddString("VOUCHER_BASE_TAX_NAME", Resource.String("STR_BASE"));
        SetParameterValue("@TAXES", DevolutionPrizeParts.GetTaxesHtml(InputParams.TaxAmount != 0 ? InputParams.TaxBaseAmount : 0, true, true, false, true, OperationCode));

        AddCurrency("VOUCHER_BASE_TAX", InputParams.TaxAmount != 0 ? InputParams.TaxBaseAmount : 0);

      }
      else
      {
        SetParameterValue("@TABLE_WITHHOLDING", String.Empty);
      }

    } // VoucherAuthoritzation

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources()
    {
      // Images

      // NLS Strings

      // Header
      AddString("STR_VOUCHER_SITE_ID", Resource.String("STR_VOUCHER_SITE_ID") + ":");
      AddString("STR_VOUCHER_TERMINAL_USERNAME", Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":");
      AddString("STR_VOUCHER_AUTHORIZED_BY", Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":");
      AddString("STR_VOUCHER_TERMINAL_ID", Resource.String("STR_VOUCHER_TERMINAL_ID") + ":");
      AddString("STR_VOUCHER_VOUCHER_ID", Resource.String("STR_VOUCHER_VOUCHER_ID") + ":");

      // Title
      AddString("STR_VOUCHER_TITLE", Resource.String("STR_VOUCHER_AUTHORIZATION_001"));

      // Details
      AddString("STR_VOUCHER_CARD_HANDPAY_TERMINAL_NAME", Resource.String("STR_VOUCHER_CARD_HANDPAY_008") + ":");
      AddString("STR_VOUCHER_CARD_HANDPAY_PROVIDER_NAME", Resource.String("STR_VOUCHER_CARD_HANDPAY_009") + ":");
      AddString("STR_VOUCHER_CARD_HANDPAY_TYPE", Resource.String("STR_VOUCHER_CARD_HANDPAY_010") + ":");
      AddString("STR_VOUCHER_CARD_HANDPAY_AMOUNT_TO_TRANSFER", Resource.String("STR_VOUCHER_CARD_HANDPAY_005") + ":");
      if (Utils.IsTitoMode())
      {
        AddString("STR_VOUCHER_CARD_HANDPAY_INITIAL_BALANCE_AMOUNT", "");
        AddString("STR_VOUCHER_CARD_HANDPAY_FINAL_BALANCE_AMOUNT", "");
        AddString("STR_VOUCHER_CARD_HANDPAY_INITIAL_AMOUNT_TO_TRANSFER", Resource.String("STR_VOUCHER_CARD_HANDPAY_001") + ":");
        AddString("STR_VOUCHER_CARD_HANDPAY_DEDUCTIONS", Resource.String("STR_VOUCHER_HANDPAY_WITHHOLDING") + ":");
      }
      else
      {
        AddString("STR_VOUCHER_CARD_HANDPAY_INITIAL_BALANCE_AMOUNT", Resource.String("STR_VOUCHER_CARD_HANDPAY_004") + ":");
        AddString("STR_VOUCHER_CARD_HANDPAY_FINAL_BALANCE_AMOUNT", Resource.String("STR_VOUCHER_CARD_HANDPAY_006") + ":");
        AddString("STR_VOUCHER_CARD_HANDPAY_INITIAL_AMOUNT_TO_TRANSFER", "");
        AddString("STR_VOUCHER_CARD_HANDPAY_DEDUCTIONS", "");
      }
      AddString("STR_VOUCHER_FINAL_TITLE", Resource.String("STR_VOUCHER_CARD_HANDPAY_007"));
    }

    #endregion

    #region Public Methods

    #endregion
  }
}
