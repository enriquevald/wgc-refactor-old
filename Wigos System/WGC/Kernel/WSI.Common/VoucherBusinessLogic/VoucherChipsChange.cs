﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherChipsChange.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
// 19-JAN-2017  FGB         Bug 22029: Error showing totals in chips change voucher
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherChipsChange : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherChipsChange(TYPE_SPLITS Splits
                            , Dictionary<FeatureChips.Chips.Chip, Int32> ChipsIn
                            , Dictionary<FeatureChips.Chips.Chip, Int32> ChipsOut
                            , PrintMode Mode
                            , SqlTransaction Trx)
      : base(Mode, Trx)
    {
      // 1. Load HTML structure.
      LoadVoucher("VoucherChipsChange");

      // 2. Load NLS strings from resource
      LoadVoucherResources(Splits, ChipsIn, ChipsOut);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    /// <param name="Splits"></param>
    /// <param name="ChipsIn"></param>
    /// <param name="ChipsOut"></param>
    private void LoadVoucherResources(TYPE_SPLITS Splits
                                    , Dictionary<FeatureChips.Chips.Chip, Int32> ChipsIn
                                    , Dictionary<FeatureChips.Chips.Chip, Int32> ChipsOut)
    {
      String _str_value;
      StringBuilder _sb_chips_denomination;
      StringBuilder _sb_header_input_chips;
      StringBuilder _sb_header_output_chips;
      StringBuilder _sb_chips_value;
      Currency _total_amount;
      String _output_chip_type_description;
      String _input_chip_type_description;
      String _output_chip_iso_code;
      String _input_chip_iso_code;

      SetValue("CV_TYPE", (Int32)CashierVoucherType.ChipsChange);

      _total_amount = 0;
      _str_value = "";
      _sb_chips_denomination = new StringBuilder();
      _sb_header_input_chips = new StringBuilder();
      _sb_header_output_chips = new StringBuilder();
      _output_chip_type_description = String.Empty;
      _input_chip_type_description = String.Empty;
      _output_chip_iso_code = CurrencyExchange.GetNationalCurrency();
      _input_chip_iso_code = CurrencyExchange.GetNationalCurrency();

      // 1. Load Header and Footer
      LoadHeader(Splits.company_a.header);
      LoadFooter(Splits.company_a.footer, Splits.company_a.footer_cashin);

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      _str_value = Resource.String("STR_VOUCHER_CHIPS_CHANGE_TITLE");
      AddString("STR_VOUCHER_CASH_CHIPS_CHANGE_TITLE", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CHIPS_CHANGE_ENTER");
      AddString("STR_VOUCHER_ENTER_CHIPS", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CHIPS_CHANGE_OUTPUT");
      AddString("STR_VOUCHER_OUTPUT_CHIPS", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_REDEEM_TOTAL");
      AddString("STR_VOUCHER_CASH_CHIPS_TOTAL", _str_value);

      // Get input chip type description and iso code
      _input_chip_type_description = GetChipTypeDescription(ChipsIn);
      _input_chip_iso_code = GetChipIsoCode(ChipsIn);

      // Get output chip type description and iso code
      _output_chip_type_description = GetChipTypeDescription(ChipsOut);
      _output_chip_iso_code = GetChipIsoCode(ChipsOut);

      SetParameterValue("@VOUCHER_CURRENCY_TYPE_ENTER_CHIPS", _input_chip_type_description);
      SetParameterValue("@VOUCHER_CURRENCY_TYPE_OUTPUT_CHIPS", _output_chip_type_description);

      // 3. Set Chips header denomination
      _sb_chips_denomination.AppendLine("<tr>  ");
      _sb_chips_denomination.AppendLine("   <td align='right' width='30%'>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_DENOMINATION") + "</td> ");
      _sb_chips_denomination.AppendLine("   <td align='right' width='30%'>" + Resource.String("STR_VOUCHER_CASH_CHIPS_QUANTITY") + "</td> ");
      _sb_chips_denomination.AppendLine("   <td align='right' width='40%'>" + Resource.String("STR_VOUCHER_CASH_CHIPS_SUBTOTAL") + "</td> ");
      _sb_chips_denomination.AppendLine("</tr> ");
      SetParameterValue("@VOUCHER_DESCRIPTION_ENTER_CHIPS", _sb_chips_denomination.ToString());
      SetParameterValue("@VOUCHER_DESCRIPTION_OUTPUT_CHIPS", _sb_chips_denomination.ToString());

      // 4. Show input chips
      _sb_chips_value = GetChipsValuesList(ChipsIn, out _total_amount);
      SetParameterValue("@VOUCHER_DENOMINATION_ENTER_CHIPS", _sb_chips_value.ToString());

      // 5. Total input amount
      SetTotalAmount(_total_amount, _input_chip_iso_code, "VOUCHER_CASH_CHIPS_ENTER_TOTAL");

      // 6. Show output chips
      _sb_chips_value = GetChipsValuesList(ChipsOut, out _total_amount);
      SetParameterValue("@VOUCHER_DENOMINATION_OUTPUT_CHIPS", _sb_chips_value.ToString());

      // 7. Total output amount
      SetTotalAmount(_total_amount, _output_chip_iso_code, "VOUCHER_CASH_CHIPS_OUTPUT_TOTAL");
    }

    /// <summary>
    /// Set chip total amount
    /// </summary>
    /// <param name="TotalAmount"></param>
    /// <param name="ChipIsoCode"></param>
    private void SetTotalAmount(Currency TotalAmount, String ChipIsoCode, String ParameterName)
    {
      if (ChipIsoCode == CurrencyExchange.GetNationalCurrency())
      {
        AddCurrency(ParameterName, TotalAmount);
      }
      else
      {
        AddString(ParameterName, Currency.Format(TotalAmount, ""));
      }
    }

    /// <summary>
    /// Get chip list type description
    /// </summary>
    /// <param name="ChipsList"></param>
    /// <returns></returns>
    private String GetChipTypeDescription(Dictionary<FeatureChips.Chips.Chip, Int32> ChipsList)
    {
      foreach (KeyValuePair<FeatureChips.Chips.Chip, Int32> _chip_item in ChipsList)
      {
        if (_chip_item.Value > 0)
        {
          return FeatureChips.GetChipTypeDescription(_chip_item.Key.Type, _chip_item.Key.IsoCode);
        }
      }

      return String.Empty;
    }

    /// <summary>
    /// Get chip list iso code
    /// </summary>
    /// <param name="ChipsList"></param>
    /// <returns></returns>
    private String GetChipIsoCode(Dictionary<FeatureChips.Chips.Chip, Int32> ChipsList)
    {
      foreach (KeyValuePair<FeatureChips.Chips.Chip, Int32> _chip_item in ChipsList)
      {
        if (_chip_item.Value > 0)
        {
          return _chip_item.Key.IsoCode;
        }
      }

      return CurrencyExchange.GetNationalCurrency();
    }

    /// <summary>
    /// Returns list with chips values
    /// </summary>
    /// <param name="ChipsList"></param>
    /// <param name="TotalAmount"></param>
    /// <returns></returns>
    private StringBuilder GetChipsValuesList(Dictionary<FeatureChips.Chips.Chip, Int32> ChipsList, out Currency TotalAmount)
    {
      StringBuilder _sb_chips_value;

      String _empty_value; 
      Currency _chip_amount;

      _empty_value = "---";
      _sb_chips_value = new StringBuilder();

      TotalAmount = 0;

      foreach (KeyValuePair<FeatureChips.Chips.Chip, Int32> _chip_item in ChipsList)
      {
        if (_chip_item.Value > 0)
        {
          _chip_amount = ((Currency)_chip_item.Key.Denomination) * _chip_item.Value;

          _sb_chips_value.AppendLine("<tr>  ");

          if (_chip_item.Key.Type == FeatureChips.ConvertToChipType(CurrencyExchangeType.CASINO_CHIP_COLOR))
          {
            _sb_chips_value.AppendLine("   <td align='right' width='30%'>" + _empty_value + "</td> ");
            _sb_chips_value.AppendLine("   <td align='right' width='30%'>" + _chip_item.Value + "</td> ");
            _sb_chips_value.AppendLine("   <td align='right' width='40%'>" + _empty_value + "</td> ");
          }
          else
          {
            if (_chip_item.Key.IsoCode == CurrencyExchange.GetNationalCurrency())
            {
              _sb_chips_value.AppendLine("   <td align='right' width='30%'>" + ((Currency)_chip_item.Key.Denomination).ToString() + "</td> ");
              _sb_chips_value.AppendLine("   <td align='right' width='30%'>" + _chip_item.Value + "</td> ");
              _sb_chips_value.AppendLine("   <td align='right' width='40%'>" + _chip_amount + "</td> ");
            }
            else
            {
              _sb_chips_value.AppendLine("   <td align='right' width='30%'>" + Currency.Format(_chip_item.Key.Denomination, "") + "</td> ");
              _sb_chips_value.AppendLine("   <td align='right' width='30%'>" + _chip_item.Value + "</td> ");
              _sb_chips_value.AppendLine("   <td align='right' width='40%'>" + Currency.Format(_chip_amount, "") + "</td> ");
            }
          }

          _sb_chips_value.AppendLine("</tr> ");

          TotalAmount += _chip_amount;
        }
      }

      return _sb_chips_value;
    }

    #endregion
  }
}
