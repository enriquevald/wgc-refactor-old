﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCageFillInFillOutBody.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
// 19-AUG-2016  RAB         Product Backlog Item 15914: Gaming tables (Phase 5): Order summary box and voucher currency
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherCageFillInFillOutBody : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCageFillInFillOutBody(CASHIER_MOVEMENT MovementType, CageDenominationsItems.CageDenominationsDictionary CageAmounts)
    {
      // 1. Load HTML structure.
      LoadVoucher("VoucherCageFillInFillOutBody");

      //  4. Load NLS strings from resource
      LoadVoucherResources(MovementType, CageAmounts);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(CASHIER_MOVEMENT MovementType, CageDenominationsItems.CageDenominationsDictionary CageAmounts)
    {
      String _str_value;
      String _voucher_html;
      StringBuilder _sb_chips_denomination;
      String _column_header;
      DataTable _values;
      Int16 _set_another_currency;
      String _national_currency;
      int _count_cage_currencies;
      Decimal _total_currency_nacional = 0;
      CurrencyExchange _currency_exchange = new CurrencyExchange();

      Decimal _denomination;
      String _currency_type;

      Currency _sub_total;
      bool _currency_changed;
      String _currency_string;

      _national_currency = CurrencyExchange.GetNationalCurrency();

      _voucher_html = "";
      _set_another_currency = 0;
      _sb_chips_denomination = new StringBuilder();

      _sb_chips_denomination.AppendLine("<table border='0' align='left' cellpadding='0' cellspacing='0' width='270'>");
      _sb_chips_denomination.AppendLine("   <tr>");
      _sb_chips_denomination.AppendLine("   <td>");
      _voucher_html += _sb_chips_denomination.ToString();

      _count_cage_currencies = CageAmounts.Count;
      
      foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _cur_currency in CurrencyExchange.OrderCurrenciesFromCurrencyConfiguration(CageAmounts))
      {
        if(_cur_currency.Value.TotalAmount == 0)
        {
          continue;
        }

        _currency_string = Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_BILL_FULL");
        _sub_total = 0;
        _currency_changed = false;
        //if is MXN-2 break.
        //When "ISO CODE" && "-1" (Cage.BANK_CARD_CODE), it shouldn't print
        if ((MovementType == CASHIER_MOVEMENT.CLOSE_SESSION || _cur_currency.Value.TotalAmount > 0 || _cur_currency.Value.HasTicketsTito)
             && (_cur_currency.Key.ISOCode.ToString().Length == 3 || _cur_currency.Key.ToString().Contains(Cage.CHECK_CODE.ToString())))
        {
          _sb_chips_denomination = new StringBuilder();

          _set_another_currency++;
          if (_set_another_currency > 1)
          {
            _voucher_html += ("</td></tr><tr><td><br/></td></tr><tr><td>");
          }

          //  1. Load HTML structure for the current currency
          LoadVoucher("VoucherCageFillInFillOutBody");

          //  2. Load Header and Footer
          _column_header = VoucherBuilder.GetColumnHeaderByType(_cur_currency.Key.Type);


          StringBuilder _sb_currency;
          _sb_currency = new StringBuilder();
          CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CURRENCY, _cur_currency.Key.ISOCode, out _currency_exchange);

          _sb_currency.AppendLine("</br>");
          if (FeatureChips.IsCageCurrencyTypeChips(_cur_currency.Key.Type))
          {
            _sb_currency.AppendLine(FeatureChips.GetChipTypeDescription(FeatureChips.ConvertToCurrencyExchangeType(_cur_currency.Key.Type), _cur_currency.Key.ISOCode)); // Resource.String("STR_UC_TICKET_CREATE_GB_CHIPS"));
          }
          else if (_cur_currency.Key.ISOCode == (_national_currency + Cage.CHECK_CODE.ToString()))
          {
            _sb_currency.AppendLine(Resource.String("STR_VOUCHER_CAGESESSIONREPORT_CHECK"));
          }
          else
          {
            _sb_currency.AppendLine(_currency_exchange.Description);
          }

          SetParameterValue("@VOUCHER_DESCRIPTION_CURRENCY", _sb_currency.ToString());


          String _deno_string;
          _values = _cur_currency.Value.ItemAmounts;
          foreach (DataRow _row in _values.Rows)
          {
            _denomination = (Decimal)_row["DENOMINATION"];
            //SDS 06-10-2015
            if (_row.IsNull("CURRENCY_TYPE"))
            {
              _currency_type = "";
            }
            else
            {
              _currency_type = (String)_row["CURRENCY_TYPE"];
            }

            if (!FeatureChips.IsCageCurrencyTypeChips(_cur_currency.Key.Type) && _currency_type != Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_BILL") && !_currency_changed)
            {
              //print and reset subtotal. 
              if (_cur_currency.Key.Type == CageCurrencyType.ChipsColor)
              {
                _sb_chips_denomination.AppendLine("<tr><td colspan='4' border='1'></br><span style='float:right'><b>" + Resource.String("STR_VOUCHER_CASH_CHIPS_QUANTITY") + " " + _currency_string + "</b>&nbsp;&nbsp;&nbsp;" + _sub_total.ToString("#,##0") + "</span></br></br></td><tr>");
              }
              else
              {
                _sb_chips_denomination.AppendLine("<tr><td colspan='4' border='1'></br><span style='float:right'><b>" + Resource.String("STR_VOUCHER_CASH_CHIPS_SUBTOTAL") + " " + _currency_string + "</b>&nbsp;&nbsp;&nbsp;" + _sub_total.ToString(false) + "</span></br></br></td><tr>");
              }

              _sub_total = 0;
              _currency_changed = true;
              _currency_string = Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_COIN_FULL");
            }
            if (_row.IsNull("TOTAL"))
            {
              _row["TOTAL"] = 0;
            }

            // RMS 01-AUG-2014: Don't show denomination items without values
            if ((_row.IsNull("UN") || Convert.ToInt32(_row["UN"]) == 0) && Convert.ToDecimal(_row["TOTAL"]) == 0)
            {
              continue;
            }

            if (Convert.ToDecimal(_row["TOTAL"]) > 0 || MovementType == CASHIER_MOVEMENT.CLOSE_SESSION)
            {
              _sb_chips_denomination.AppendLine("	<tr>  ");



              //  Denomination   *******************************************************************************************************

              if (_cur_currency.Key.Type != CageCurrencyType.ChipsColor)
              {
                if (_denomination < 0)
                {
                  _deno_string = getDenominationOther(_denomination, _row["ISO_CODE"].ToString());
                  _sb_chips_denomination.AppendLine("	   <td align='right'>" + _deno_string + "</td> ");
                  if (_row.IsNull("UN"))
                  {
                    if (Convert.ToDecimal(_row["TOTAL"]) > 0)
                    {
                      _row["UN"] = 1;
                    }
                  }
                  else if (_denomination == Cage.TICKETS_CODE)
                  {
                    _row["TOTAL"] = 0;
                  }
                }
                else
                {
                  _sb_chips_denomination.AppendLine("	   <td align='right'>" + Currency.Format(_denomination, "") + "&nbsp</td> ");
                }
              }
              else
              {
                _sb_chips_denomination.AppendLine("	   <td align='left'>" + _row["CHIP_NAME"] + "</td> ");
              }
              // Type    *******************************************************************************************************
              if (!FeatureChips.IsCageCurrencyTypeChips(_cur_currency.Key.Type))
              {
                _sb_chips_denomination.AppendLine("	   <td align='center'>" + _currency_type + "</td> ");
              }

              // Cant.   *******************************************************************************************************
              if (_row.IsNull("UN"))
              {
                _row["UN"] = 0;
              }

              if (_denomination == Cage.COINS_CODE)
              {
                _sb_chips_denomination.AppendLine("	   <td align='right'></td> ");
              }
              else
              {
                if (_cur_currency.Key.Type == CageCurrencyType.ChipsColor)
                {
                  _sb_chips_denomination.AppendLine("	   <td align='left'>" + _row["CHIP_DRAW"] + "</td> ");
                }
                else
                {
                  int _aux_un = Convert.ToInt32(_row["UN"]);
                  _sb_chips_denomination.AppendLine("	   <td align='right'>" + _aux_un.ToString("#,##0") + "&nbsp</td> ");
                }
              }

              // Subtotal     *******************************************************************************************************
              if (_denomination == Cage.TICKETS_CODE)
              {
                _sb_chips_denomination.AppendLine("	   <td align='right'> </td> ");
              }
              else if (_cur_currency.Key.Type == CageCurrencyType.ChipsColor)
              {
                _sb_chips_denomination.AppendLine("	   <td align='right'>" + Convert.ToInt32(_row["TOTAL"]).ToString("#,##0") + "</td> ");
              }
              else
              {
                //_sb_chips_denomination.AppendLine("	   <td align='right'>" + Currency.Format(Convert.ToDecimal(_row["TOTAL"].ToString()), _cur_currency.Key.ISOCode) + " " + "</td> ");
                _sb_chips_denomination.AppendLine("	   <td align='right'>" + ((Currency)Convert.ToDecimal(_row["TOTAL"].ToString())).ToString(false) + "</td> ");
              }


              // *******************************************************************************************************
              _sub_total += Convert.ToDecimal(_row["TOTAL"].ToString());
              _sb_chips_denomination.AppendLine("	</tr> ");

            }

          } // foreach (DataRow _row in _values.Rows)


          if (!FeatureChips.IsCageCurrencyTypeChips(_cur_currency.Key.Type) && _sub_total > 0)
          {

            if (_national_currency == _cur_currency.Key.ISOCode.Substring(0, Cage.MAX_ISO_CODE_DIGITS))
              _sb_chips_denomination.AppendLine("<tr><td colspan='4' border='1'></br><span style='float:right'><b>" + Resource.String("STR_VOUCHER_CASH_CHIPS_SUBTOTAL") + " " + _currency_string + "</b>&nbsp;&nbsp;&nbsp;" + _sub_total.ToString(false) + "</span></br></br></td><tr>");
            else
              if (_cur_currency.Key.Type == CageCurrencyType.ChipsColor)
              {
                _sb_chips_denomination.AppendLine("<tr><td colspan='4' border='1'></br><span style='float:right'><b>" + Resource.String("STR_VOUCHER_CASH_CHIPS_QUANTITY") + " " + _currency_string + "</b>&nbsp;&nbsp;&nbsp;" + Convert.ToInt32(_sub_total).ToString("#,##0") + "</span></br></br></td><tr>");
              }
              else
              {
                _sb_chips_denomination.AppendLine("<tr><td colspan='4' border='1'></br><span style='float:right'><b>" + Resource.String("STR_VOUCHER_CASH_CHIPS_SUBTOTAL") + " " + _currency_string + "</b>&nbsp;&nbsp;&nbsp;" + _sub_total.ToString(false) + "</span></br></br></td><tr>");
              }
          }

          if (String.IsNullOrEmpty(_sb_chips_denomination.ToString()))
          {
            SetParameterValue("@VOUCHER_DESCRIPTION_CHIPS", "");
          }
          else
          {
            SetParameterValue("@VOUCHER_DESCRIPTION_CHIPS", _column_header);
          }

          SetParameterValue("@VOUCHER_DENOMINATION_CHIPS", _sb_chips_denomination.ToString());

          _str_value = Resource.String("STR_VOUCHER_CARD_REDEEM_TOTAL");

          AddString("STR_VOUCHER_CASH_CHIPS_TOTAL", _str_value);

          if (_national_currency == _cur_currency.Key.ISOCode.Substring(0, Cage.MAX_ISO_CODE_DIGITS))
          {
            AddString("VOUCHER_CASH_CHIPS_TOTAL", ((Currency)(_cur_currency.Value.TotalAmount + _total_currency_nacional)).ToString(false));
          }
          else
          {
            if (_cur_currency.Key.Type == CageCurrencyType.ChipsColor)
            {
              AddString("VOUCHER_CASH_CHIPS_TOTAL", Convert.ToInt32((_cur_currency.Value.TotalAmount + _total_currency_nacional)).ToString("#,##0"));
            }
            else
            {
              AddString("VOUCHER_CASH_CHIPS_TOTAL", ((Currency)(_cur_currency.Value.TotalAmount + _total_currency_nacional)).ToString(false));
            }
          }
          _total_currency_nacional = 0;


          _voucher_html += VoucherHTML;
          _set_another_currency = 1;
        }
      }
      _sb_chips_denomination.Length = 0;
      _sb_chips_denomination.AppendLine("     </td>");
      _sb_chips_denomination.AppendLine("    </tr>");
      _sb_chips_denomination.AppendLine("  </table>");
      VoucherHTML = _voucher_html;
    }

    /// <summary>
    /// Get String of Denomination.
    /// </summary>
    private String getDenominationOther(Decimal Denomination, String _iso_code)
    {
      String _denomination;

      _denomination = "";

      Int16 _deno_int16;
      _deno_int16 = (Int16)Denomination;

      switch (_deno_int16)
      {
        case Cage.COINS_CODE:
          if (_iso_code == Cage.CHIPS_ISO_CODE)
          {
            _denomination = Resource.String("STR_CAGE_TIPS"); //Propinas
          }
          else
          {
            _denomination = Resource.String("STR_CAGE_COINS"); //Monedas
          }
          break;

        case Cage.TICKETS_CODE:
          _denomination = Resource.String("STR_FRM_TITO_HANDPAYS_TICKETS");  // Tickets
          break;

        case Cage.CHECK_CODE:
          _denomination = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_CHECK_PAYMENT"); // Cheque
          break;

        case Cage.BANK_CARD_CODE:
          _denomination = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_BANK_CARD"); // Tarjeta bancaria
          break;

        default:
          if (Denomination.ToString() == Cage.CHIPS_ISO_CODE)
          {
            _denomination = Resource.String("STR_CAGE_CASINO_CHIPS"); // Fichas
          }
          break;

      }
      return _denomination;

    }

    /// <summary>
    /// Get CageCurrencies with national coin the first.
    /// </summary>
    private CageDenominationsItems.CageDenominationsDictionary GetCageCurrenciesOrderByNational(CageDenominationsItems.CageDenominationsDictionary CageAmounts)
    {
      String _national_currency;
      _national_currency = CurrencyExchange.GetNationalCurrency();

      CageDenominationsItems.CageDenominationsDictionary _outCageAmounts;
      _outCageAmounts = new CageDenominationsItems.CageDenominationsDictionary();

      foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _cur_currency in CageAmounts)
      {
        if (_cur_currency.Key.ToString() == _national_currency)
        {
          _outCageAmounts.Add(_cur_currency.Key, _cur_currency.Value);
          break;
        }
      }

      foreach (KeyValuePair<CageDenominationsItems.CageDenominationKey, CageDenominationsItems.CageDenominationsItem> _cur_currency in CageAmounts)
      {
        if (_cur_currency.Key.ToString() != _national_currency)
        {
          _outCageAmounts.Add(_cur_currency.Key, _cur_currency.Value);
        }
      }

      return _outCageAmounts;

    }

    #endregion
  }
}
