﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCashDeskWithdrawBody.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherCashDeskWithdrawBody : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCashDeskWithdrawBody(CurrencyIsoType IsoType, Decimal Initial, Decimal Amount, Boolean ShowBalance)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCashDeskWithdrawBody");

      // 2. Load voucher resources.
      LoadVoucherResources(IsoType, Initial, Amount, ShowBalance);
    }

    public VoucherCashDeskWithdrawBody(CurrencyIsoType IsoType, Decimal Initial, Decimal Amount)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCashDeskWithdrawBody");

      // 2. Load voucher resources.
      LoadVoucherResources(IsoType, Initial, Amount, true);
    }

    #endregion

    #region Private Methods
    private void LoadVoucherResources(CurrencyIsoType IsoType, Decimal Initial, Decimal Amount, Boolean ShowBalance)
    {
      String _value;
      String _national_currency;
      CurrencyExchange _currency_exchange = new CurrencyExchange();

      _national_currency = CurrencyExchange.GetNationalCurrency();

      //Title. Set currency type (efective or chips)
      if (IsoType.Type == CurrencyExchangeType.CASINOCHIP ||
        IsoType.Type == CurrencyExchangeType.CASINO_CHIP_RE ||
        IsoType.Type == CurrencyExchangeType.CASINO_CHIP_NRE ||
        IsoType.Type == CurrencyExchangeType.CASINO_CHIP_COLOR)
      {
        _value = FeatureChips.GetChipTypeDescription(IsoType.Type, IsoType.IsoCode);
      }
      else
      {
        CurrencyExchange.ReadCurrencyExchange(IsoType.Type, IsoType.IsoCode, out _currency_exchange);
        _value = _currency_exchange.Description;
      }

      _value = "<br/>" + _value + "<br/>";
      SetParameterValue("@STR_VOUCHER_CASH_DESK_WITHDRAW_TITLE", _value);
      SetParameterValue("@STR_HR_SIZE_4", "<hr noshade size=\"4\">");

      // - Voucher Specific for Filler Out
      _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_BALANCE_INITIAL") + ":";
      AddString("STR_VOUCHER_CASH_DESK_WITHDRAW_BALANCE_INITIAL", _value);

      _value = Resource.String("STR_VOUCHER_CASH_DESK_FILL_OUT_AMOUNT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_WITHDRAW_AMOUNT", _value);

      _value = Resource.String("STR_VOUCHER_CASH_DESK_FILL_OUT_BALANCE_FINAL") + ":";
      AddString("STR_VOUCHER_CASH_DESK_WITHDRAW_BALANCE_FINAL", _value);

      if (_national_currency == IsoType.IsoCode)
      {
        if (ShowBalance)
        {
          AddCurrency("VOUCHER_CASH_DESK_WITHDRAW_BALANCE_INITIAL", (Currency)Initial);
          AddCurrency("VOUCHER_CASH_DESK_WITHDRAW_AMOUNT", (Currency)Amount);
          AddCurrency("VOUCHER_CASH_DESK_WITHDRAW_BALANCE_FINAL", (Currency)(Initial - Amount));
        }
        else
        {
          AddString("VOUCHER_CASH_DESK_WITHDRAW_BALANCE_INITIAL", "---");
          AddCurrency("VOUCHER_CASH_DESK_WITHDRAW_AMOUNT", (Currency)Amount);
          AddString("VOUCHER_CASH_DESK_WITHDRAW_BALANCE_FINAL", "---");
        }
      }
      else if ((_national_currency != IsoType.IsoCode || IsoType.IsCasinoChip) && IsoType.Type != CurrencyExchangeType.CASINO_CHIP_COLOR)
      {
        if (ShowBalance)
        {
          AddString("VOUCHER_CASH_DESK_WITHDRAW_BALANCE_INITIAL", Currency.Format(Initial, IsoType.IsoCode));
          AddString("VOUCHER_CASH_DESK_WITHDRAW_AMOUNT", Currency.Format(Amount, IsoType.IsoCode));
          AddString("VOUCHER_CASH_DESK_WITHDRAW_BALANCE_FINAL", Currency.Format((Initial - Amount), IsoType.IsoCode));
        }
        else
        {
          AddString("VOUCHER_CASH_DESK_WITHDRAW_BALANCE_INITIAL", "---");
          AddString("VOUCHER_CASH_DESK_WITHDRAW_AMOUNT", Currency.Format(Amount, IsoType.IsoCode));
          AddString("VOUCHER_CASH_DESK_WITHDRAW_BALANCE_FINAL", "---");
        }
      }
      else
      {
        //Color chips
        if (ShowBalance)
        {
          AddString("VOUCHER_CASH_DESK_WITHDRAW_BALANCE_INITIAL", Convert.ToInt32(Initial).ToString());
          AddString("VOUCHER_CASH_DESK_WITHDRAW_AMOUNT", Convert.ToInt32(Amount).ToString());
          AddString("VOUCHER_CASH_DESK_WITHDRAW_BALANCE_FINAL", Convert.ToInt32(Initial - Amount).ToString());
        }
        else
        {
          AddString("VOUCHER_CASH_DESK_WITHDRAW_BALANCE_INITIAL", "---");
          AddString("VOUCHER_CASH_DESK_WITHDRAW_AMOUNT", Convert.ToInt32(Amount).ToString());
          AddString("VOUCHER_CASH_DESK_WITHDRAW_BALANCE_FINAL", "---");
        }
      }

    } // SetupCashStatusVoucher
    #endregion

    #region Public Methods

    #endregion

  }
}
