﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherTitoHandpayCancellation.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherTitoHandpayCancellation : Voucher
  {
    #region Class Attributes

    public class VoucherTitoHandpayCancellationInputParams
    {
      public String[] VoucherAccountInfo;
      public String TerminalName;
      public String TerminalProvider;
      public HANDPAY_TYPE Type;
      public Int32 Level;
      public Currency PayAmount;
      public Currency TotalAmount;
      public Currency Deductions;
    }

    #endregion

    #region Constructor

    public VoucherTitoHandpayCancellation(VoucherTitoHandpayCancellationInputParams InputParams,
                                          PrintMode Mode,
                                          SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherTITOHandpayCancelation");

      // 2. Load voucher resources.
      LoadVoucherResources();

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", InputParams.VoucherAccountInfo);
      AddString("VOUCHER_TERMINAL_NAME", InputParams.TerminalName);
      AddString("VOUCHER_PROVIDER_NAME", InputParams.TerminalProvider);
      AddString("VOUCHER_TYPE", Handpays.HandpayTypeString(InputParams.Type, InputParams.Level));
      AddCurrency("VOUCHER_VOIDING", InputParams.PayAmount);
      AddCurrency("VOUCHER_WITHHOLDING", InputParams.Deductions);
      AddCurrency("VOUCHER_VOIDED_PAYMENT", InputParams.TotalAmount);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources()
    {
      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Strings

      // Header
      AddString("STR_VOUCHER_SITE_ID", Resource.String("STR_VOUCHER_SITE_ID") + ":");
      AddString("STR_VOUCHER_TERMINAL_USERNAME", Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":");
      AddString("STR_VOUCHER_AUTHORIZED_BY", Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":");
      AddString("STR_VOUCHER_TERMINAL_ID", Resource.String("STR_VOUCHER_TERMINAL_ID") + ":");
      AddString("STR_VOUCHER_VOUCHER_ID", Resource.String("STR_VOUCHER_VOUCHER_ID") + ":");
      AddString("STR_VOUCHER_TYPE", Resource.String("STR_VOUCHER_CARD_HANDPAY_010") + ":");
      AddString("STR_VOUCHER_WITHHOLDING", Resource.String("STR_DEDUCTIONS") + ":");

      // Title
      AddString("STR_VOUCHER_TITLE", Resource.String("STR_VOUCHER_CARD_HANDPAY_013"));

      // Details
      AddString("STR_VOUCHER_TERMINAL_NAME", Resource.String("STR_VOUCHER_CARD_HANDPAY_008") + ":");
      AddString("STR_VOUCHER_PROVIDER_NAME", Resource.String("STR_VOUCHER_CARD_HANDPAY_009") + ":");

      AddString("STR_VOUCHER_VOIDING", Resource.String("STR_VOUCHER_HANDPAY_CANCELATION_01") + ":");
      AddString("STR_VOUCHER_VOIDED_PAYMENT", Resource.String("STR_VOUCHER_HANDPAY_CANCELATION_02") + ":");

      AddString("STR_VOUCHER_CARD_HANDPAY_AMOUNT_TO_TRANSFER", Resource.String("STR_VOUCHER_CARD_HANDPAY_014") + ":");
      AddString("STR_VOUCHER_FINAL_TITLE", Resource.String("STR_VOUCHER_CARD_HANDPAY_007"));

    }

    #endregion

    #region Public Methods

    //public void VoucherValuesCardHandpayCancellation(CashierBusinessLogic.TYPE_CARD_HANDPAY Params)
    //{
    //  //  1. Depósito
    //  //     ---------------------------
    //  //  2. Total
    //  //     ---------------------------

    //  //Params.account_id
    //  //Params.track_data
    //  //Params.amount
    //  //Params.terminal_name
    //  //Params.provider_name

    //  //AddString("VOUCHER_OLD_CARD_ID", Params.old_track_data);
    //  //AddString("VOUCHER_NEW_CARD_ID", Params.new_track_data);
    //  //AddString("VOUCHER_ACCOUNT_ID", Params.account_id.ToString());
    //  //AddCurrency("VOUCHER_CARD_REPLACEMENT_PRICE", Params.card_replacement_price);
    //  //AddCurrency("VOUCHER_CARD_REPLACEMENT_TOTAL_AMOUNT", Params.card_replacement_price);

    //} // VoucherValuesCardHandpayCancellation

    #endregion
  }
}
