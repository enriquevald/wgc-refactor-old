﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCageNewOperation.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  // N. Cage GUI new operation
  // Format:
  //          Voucher Logo
  //          Voucher Header
  //
  // Cage Session:
  // User: 
  // Voucher Id:
  // Tipo de movimiento (origen/destino)
  //
  //         Voucher Footer
  //
  public class VoucherCageNewOperation : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCageNewOperation(PrintMode Mode, String[] VcInfo, SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      //LoadVoucher("VoucherCashDeskWithdraw");
      LoadVoucher("VoucherCageNewOperation");

      // 2. Load voucher resources.
      LoadVoucherCageResourcesFromGUI((String.IsNullOrEmpty(VcInfo[3])) ? "" : VcInfo[3]);

      // 3. Load general voucher data.
      LoadVoucherCageGeneralDataFromGUI(VcInfo);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherCageResourcesFromGUI(String OperationTitle)
    {
      // - Title
      AddString("STR_VOUCHER_CAGE_NEW_OPERATION_TITLE", OperationTitle);

    } // LoadVoucherResources

    #endregion

    #region Public Methods

    #endregion

  }
}
