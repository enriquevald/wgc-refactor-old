﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCurrencyCash.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
// 05-AGO-2016  LTC         Product Backlog Item 16198:TPV Televisa: Closing Cash
// 23-AUG-2016  RAB         Bug 16906: Gaming Tables - GUI: Terms Fills and Credits on labels where not applicable.
// 06-SEP-2016  LTC         Bug 17354: Wrong information is shown in cashier resume about vouchers
// 07-SEP-2016  LTC         Bug 17070:Cashier - Cash Advance with bill couses wrong information in cashier
// 07-SEP-2016  EOR         Product Backlog Item 15335: TPV Televisa: Cash Summary
// 27-SEP-2016  ESE         Bug 17833: Televisa voucher, status cash
// 27-SEP-2016  DHA         Fixed Bug 17847: added gaming profit for check and bank card
// 03-OCT-2016  ESE         Bug 17833: Televisa voucher, status cash
// 04-OCT-2016  EOR         Product Backlog Item 15335: TPV Televisa: Cash Summary
// 04-OCT-2016  RGR         PBI 17447: Televisa - Draw Cash (Cash prize) - Recharge Operation
// 11-OCT-2016  ETP         Fixed Bug 18901:Multisite: Errors in cash summary in multisite mode.
// 11-OCT-2016  ESE         Bug 17833: Televisa voucher, status cash
// 25-OCT-2016  ATB         Bug 19275:Televisa: el voucher de cierre de caja no cumple los requisitos de Televisa
// 26-OCT-2016  ATB         Bug 19275:Televisa: el voucher de cierre de caja no cumple los requisitos de Televisa
// 10-NOV-2016  ATB         Bug 19929:Cashier: Faulty labels in the main cashier form
// 16-NOV-2016  EOR         Fixed Bug 17848: Cashier/Gaming Tables: Information incorrect summary in closed with check
// 22-NOV-2016  EOR         Fixed Bug 19766: WigosGUI: Show label incorrect in summary cash with recharge check
// 16-FEB-2017  RAB         Bug 24330: Session summary cashdesk: missing and excess appear in some types of cash, chips, check and currency
// 21-FEB-2017  FGB         Bug 24801: WigosGUI: Incorrect collection in the cash closing, due to Gp BankCard.Conciliate
// 15-MAR-2017  ETP         WIGOS- 109: Add marker information to cashier forms.
// 20-JUN-2017  EOR         Bug 28292: WIGOS-2979 The report "Resumen de sesion de caja" is not displaying the field "Report con tarjeta bancaria" 
// 09-AUG-2017  MS          Bug 29289: WIGOS-4137: Wrong amount for collected chips in Cash closing ticket when closing gaming table 
// 11-AUG-2017  MS          Bug 29325:[WIGOS-4389] Exception error in gambling table when Fill and Credit operations
// 22-AUG-2017  RGR         Bug 29424:WIGOS-3831 Missing entries for Commision of Credit Card and Check in Cash Summary report window when PinPad activated
// 19-OCT-2017  JML         Fixed Bug 30321:WIGOS-5924 [Ticket #9450] Fórmula datos tarjeta bancaria (entregado-faltante) - Resumen de sesión de caja
// 29-NOV-2017  RAB         Bug 30942:WIGOS-6509 PinPad: wrong cashier movements when some vouchers are withdrawn
// 19-DIC-2017  RAB         Bug 30942:WIGOS-6509 PinPad: wrong cashier movements when some vouchers are withdrawn
// 05-ENE-2018  EOR         Bug 31173:WIGOS-6835 Multicurrency - cashier: Currency exchange and refund concept are shown for redemed and not redemed chips on cash summary
// 26-JUN-2018  FOS         Bug 33335:WIGOS-13162 Gambling tables: Credit card movement is not showed correctly when a close partial withdraw has been done.
// 26-JUN-2018  DLM         Bug 33319:WIGOS-13186 Gambling tables: It is not possible to close the cash session balanced when there sale chips operation.
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherCurrencyCash : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCurrencyCash(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData, Boolean Collected)
      : this(CashierSessionData, Collected, false, false)
    {

    }

    public VoucherCurrencyCash(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData, Boolean Collected, Boolean IsMovementFilter, Boolean HideChipsAndCurrenciesConcepts)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      //LJM 18-DEC-2013: All the generating the structure, etc. Are inside the funcion, since now the loop is there
      LoadVoucherResources(CashierSessionData, Collected, IsMovementFilter, HideChipsAndCurrenciesConcepts, null);
    }

    public VoucherCurrencyCash(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData, Boolean Collected, Boolean IsMovementFilter, Boolean HideChipsAndCurrenciesConcepts, VoucherCashDeskOverview.ParametersCashDeskOverview ParametersOverview)
    {
      //LJM 18-DEC-2013: All the generating the structure, etc. Are inside the funcion, since now the loop is there
      LoadVoucherResources(CashierSessionData, Collected, IsMovementFilter, HideChipsAndCurrenciesConcepts, ParametersOverview);
    }
    #endregion

    #region Private Methods
    private void LoadVoucherResources(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData, Boolean Collected, Boolean IsMovementFilter, Boolean HideChipsAndCurrenciesConcepts, VoucherCashDeskOverview.ParametersCashDeskOverview ParametersOverview)
    {
      String _value;
      Decimal _currency_value;
      String _national_currency;
      Decimal _game_profit;
      Voucher _temp_voucher;
      string _currencies_html = "";
      Int32 _exchange_currencies_count;
      Decimal _exchange_currency_value;
      Decimal _collected_value;
      Boolean _is_color_chip_type;
      KeyValuePair<CurrencyIsoType, Decimal> _currencyDict;

      _national_currency = CurrencyExchange.GetNationalCurrency();
      _exchange_currencies_count = 0;
      _exchange_currency_value = 0;
      _is_color_chip_type = false;

      // Count difference currencies configured in the systems differnts of national currency
      if (CashierSessionData.currencies_balance != null)
      {
        foreach (KeyValuePair<CurrencyIsoType, Decimal> _currency in CashierSessionData.currencies_balance)
        {
          if (_currency.Key.Type.ToString() == CurrencyExchangeType.CURRENCY.ToString() && _currency.Key.IsoCode != _national_currency)
          {
            _exchange_currency_value = CashierSessionData.currency_cash_in_card_deposit_split1 + CashierSessionData.currency_cash_in_card_deposit_split2;

            _exchange_currencies_count += 1;
          }
        }

        foreach (KeyValuePair<CurrencyIsoType, Decimal> _currency in CashierSessionData.currencies_balance)
        {
          _currencyDict = _currency;

          switch (_currency.Key.Type)
          {
            default:
              _is_color_chip_type = _currency.Key.Type == CurrencyExchangeType.CASINO_CHIP_COLOR;

              if (HideChipsAndCurrenciesConcepts || _currency.Key.IsoCode == _national_currency && !FeatureChips.IsChipsType(_currency.Key.Type))
              {
                break;
              }

              _currencyDict = _currency;

              HtmlChipsInformation(CashierSessionData, Collected, IsMovementFilter, _national_currency, ref _currencies_html, _exchange_currencies_count, _exchange_currency_value, _is_color_chip_type, ref _currencyDict, out _value, out _currency_value, out _game_profit, out _collected_value);

              break;

            case CurrencyExchangeType.CHECK:
              HtmlCheckInformation(CashierSessionData, _national_currency, ref _currencies_html, ref _currencyDict, out _value, out _currency_value, out _temp_voucher);

              break;

            case CurrencyExchangeType.CARD:
              HtmlCardInformation(CashierSessionData, _national_currency, ref _currencies_html, ref _currencyDict, out _value, out _currency_value, out _temp_voucher, out _collected_value, ParametersOverview);

              break;

            case CurrencyExchangeType.CREDITLINE:
              HtmlCreditLineInformation(CashierSessionData, _national_currency, ref _currencies_html, ref _currencyDict, out _value, out _currency_value, out _temp_voucher);

              break;
          }
        }
      }
      //Card deposit only if more than one exchange currency avalaible
      if (_exchange_currencies_count > 1)
      {
        _currencies_html += AddCardDepositExchange(_exchange_currency_value, false);
      }

      VoucherHTML = _currencies_html;
    }

    private static void HtmlCheckInformation(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData, String _national_currency, ref string _currencies_html, ref KeyValuePair<CurrencyIsoType, Decimal> _currency, out String _value, out Decimal _currency_value, out Voucher _temp_voucher)
    {
      Decimal _game_profit;
      Decimal _balance_value;

      _game_profit = 0;
      _temp_voucher = new Voucher();

      _temp_voucher.LoadVoucher("VoucherCashBankTransactions");

      _value = CurrencyExchange.GetDescription(_currency.Key.Type, _currency.Key.IsoCode, _currency.Key.IsoCode);
      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_TITLE", _value);

      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CREDIT_CARD", "");
      _temp_voucher.AddString("VOUCHER_CASH_BANK_CREDIT_CARD", "");
      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_DEBIT_CARD", "");
      _temp_voucher.AddString("VOUCHER_CASH_BANK_DEBIT_CARD", "");
      _temp_voucher.AddString("STR_VOUCHER_WITHDRAWAL_BANK_CARD", "");  //EOR 20-JUN-2017
      _temp_voucher.AddString("VOUCHER_WITHDRAWAL_BANK_CARD", "");
      _temp_voucher.AddString("STR_VOUCHER_CASHBANK_DELIVERED_VOUCHERS", "");
      _temp_voucher.AddString("VOUCHER_CASHBANK_DELIVERED_VOUCHERS", "");
      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CARD_WITHDRAW", "");
      _temp_voucher.AddString("VOUCHER_CASH_BANK_CARD_WITHDRAW", "");

      //Withdraws
      _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW");
      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_WITHDRAWS", _value);
      _currency_value = CashierSessionData.fill_out_balance.ContainsKey(_currency.Key) ? CashierSessionData.fill_out_balance[_currency.Key] : 0;
      _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_WITHDRAWS", _currency_value);

      //Inputs
      _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN");
      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CASH_IN", _value);
      _currency_value = CashierSessionData.currency_cash_in.ContainsKey(_currency.Key) ? CashierSessionData.currency_cash_in[_currency.Key] : 0;
      _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_CASH_IN", _currency_value);

      //Game profit
      if (CashierSessionData.currency_game_profit.ContainsKey(_currency.Key))
      {
        _game_profit = CashierSessionData.currency_game_profit[_currency.Key];
        _value = Resource.String("STR_VOUCHER_OVERVIEW_GAME_PROFIT");
        _temp_voucher.AddString("STR_VOUCHER_GAME_PROFIT", _value);

        if (_currency.Key.IsoCode == _national_currency)
        {
          _temp_voucher.AddCurrency("VOUCHER_GAME_PROFIT", (Currency)_game_profit);
        }
        else
        {
          _temp_voucher.AddString("VOUCHER_GAME_PROFIT", Currency.Format(_game_profit, _currency.Key.IsoCode));
        }
      }
      else
      {
        _temp_voucher.AddString("STR_VOUCHER_GAME_PROFIT", "");
        _temp_voucher.AddString("VOUCHER_GAME_PROFIT", "");
      }

      // Balance
      _value = Resource.String("STR_IN_CASH_DESK");
      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_BALANCE", _value);
      _currency_value = (CashierSessionData.currencies_balance.ContainsKey(_currency.Key) ? CashierSessionData.currencies_balance[_currency.Key] : 0);
      _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_BALANCE", (Currency)_currency_value);

      // JML 05-AUG-2015
      if (!CashierSessionData.show_short_over)
      {
        _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLLECTED", "");
        _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLLECTED", "");

        _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", "");
        _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", "");
        _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", "");
        _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", "");
      }

      _balance_value = _currency.Value;

      //Short EOR 16-NOV-2016
      _currency_value = CashierSessionData.short_currency.ContainsKey(_currency.Key) ? CashierSessionData.short_currency[_currency.Key] : 0;

      if (_currency_value > 0)
      {
        _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING");
        _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", _value);

        _temp_voucher.AddCurrency("VOUCHER_CURRENCY_CASH_BANK_COLL_DIFF", _currency_value);
        _temp_voucher.AddCurrency("VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", _currency_value);

        _balance_value -= _currency_value;
      }
      else
      {
        _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", "");
        _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_DIFF", "");
        _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", "");
      }

      //Over  EOR 16-NOV-2016
      _currency_value = CashierSessionData.over_currency.ContainsKey(_currency.Key) ? CashierSessionData.over_currency[_currency.Key] : 0;

      if (_currency_value > 0)
      {
        _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA");
        _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", _value);

        _temp_voucher.AddCurrency("VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", _currency_value);

        _balance_value += _currency_value;
      }
      else
      {
        _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", "");
        _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", "");
      }

      //Collected EOR 16-NOV-2016
      _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED");
      _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLLECTED", _value);

      _currency_value = _balance_value;
      _temp_voucher.AddCurrency("VOUCHER_CURRENCY_CASH_BANK_COLLECTED", _currency_value);

      //Card deposit  EOR 22-NOV-2016
      if (Misc.IsVoucherModeTV())
      {
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CARD_DEPOSIT", "");
        _temp_voucher.AddString("VOUCHER_CASH_BANK_CARD_DEPOSIT", "");
      }
      else
      {
        _value = Misc.GetCardPlayerConceptName();
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CARD_DEPOSIT", _value);
        _currency_value = CashierSessionData.currency_cash_in_card_deposit.ContainsKey(_currency.Key) ? CashierSessionData.currency_cash_in_card_deposit[_currency.Key] : 0;
        _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_CARD_DEPOSIT", _currency_value); // + CashierSessionData.cash_advance_check_comissions);
      }

      // RGR 22-08-2017
      // Not show commission if is televisa mode
        if (Misc.IsVoucherModeTV())
        {
          _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_COMMISSION", "");
          _temp_voucher.AddString("VOUCHER_CASH_BANK_COMMISSION", "");

        }
        else
        {
          _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION");
          _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_COMMISSION", _value);
          _currency_value = CashierSessionData.currency_cash_in_commissions.ContainsKey(_currency.Key) ? CashierSessionData.currency_cash_in_commissions[_currency.Key] : 0;
          _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_COMMISSION", _currency_value); // + CashierSessionData.cash_advance_check_comissions);

      }


      // 07-SEP-2016  LTC
      // Vouchers
      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_VOUCHERS", "");
      _temp_voucher.AddString("VOUCHER_CASH_BANK_VOUCHERS", "");

      // Delivered Vouchers
      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_DELIVERED_VOUCHERS", "");
      _temp_voucher.AddString("VOUCHER_CASH_BANK_DELIVERED_VOUCHERS", "");

      // No Delivered Vouchers
      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_NO_DELIVERED_VOUCHERS", "");
      _temp_voucher.AddString("VOUCHER_CASH_BANK_NO_DELIVERED_VOUCHERS", "");

      _currencies_html += _temp_voucher.VoucherHTML;
    }

    private static void HtmlCardInformation(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData, String _national_currency, ref string _currencies_html, ref KeyValuePair<CurrencyIsoType, Decimal> _currency, out String _value, out Decimal _currency_value, out Voucher _temp_voucher, out Decimal _collected_value, VoucherCashDeskOverview.ParametersCashDeskOverview ParametersOverview)
    {
      Decimal _game_profit;
      Int32 _voucher_mode;

      _game_profit = 0;
      _temp_voucher = new Voucher();
      _voucher_mode = GeneralParam.GetInt32("Cashier.Voucher", "Mode", 0);

      _temp_voucher.LoadVoucher("VoucherCashBankTransactions");

      _value = CurrencyExchange.GetDescription(_currency.Key.Type, _currency.Key.IsoCode, _currency.Key.IsoCode);
      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_TITLE", _value);

      // Check for enabled specific bank card types
      if (GeneralParam.GetBoolean("Cashier.PaymentMethod", "BankCard.DifferentiateType", false) && !CashierSessionData.m_cashier_session_has_pinpad_operations)
      {
        // Credit Cards
        _value = Resource.String("STR_FRM_AMOUNT_BANK_CREDIT_CARD");
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CREDIT_CARD", _value);
        _currency_value = CashierSessionData.total_bank_credit_card + CashierSessionData.total_cash_advance_bank_credit_card;
        _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_CREDIT_CARD", _currency_value);

        // Debit Cards
        _value = Resource.String("STR_FRM_AMOUNT_BANK_DEBIT_CARD");
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_DEBIT_CARD", _value);
        _currency_value = CashierSessionData.total_bank_debit_card + CashierSessionData.total_cash_advance_bank_debit_card;
        _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_DEBIT_CARD", _currency_value);

        // Others (in Withdraws)
        _value = Resource.String("STR_VOUCHER_CASH_BANK_CARD_OTHERS");
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_WITHDRAWS", _value);
        _currency_value = (CashierSessionData.total_bank_card - CashierSessionData.total_bank_debit_card - CashierSessionData.total_bank_credit_card) + CashierSessionData.cash_advance_generic_card + CashierSessionData.cash_advance_generic_card_comissions;
        _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_WITHDRAWS", _currency_value);

         //Widthdrawal Credit Card
         _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW");
         _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CARD_WITHDRAW", _value);
         _currency_value = CashierSessionData.current_card_withdrawal_total;
         _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_CARD_WITHDRAW", _currency_value);
        
        // Inputs
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CASH_IN", "");
        _temp_voucher.AddString("VOUCHER_CASH_BANK_CASH_IN", "");
      }
      else
      {
        // Credit Cards
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CREDIT_CARD", "");
        _temp_voucher.AddString("VOUCHER_CASH_BANK_CREDIT_CARD", "");

        // Debit Cards
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_DEBIT_CARD", "");
        _temp_voucher.AddString("VOUCHER_CASH_BANK_DEBIT_CARD", "");

        // Others (in Withdraws)
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_WITHDRAWS", "");
        _temp_voucher.AddString("VOUCHER_CASH_BANK_WITHDRAWS", "");

        //Widthdrawal Credit Card
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CARD_WITHDRAW", "");
        _temp_voucher.AddString("VOUCHER_CASH_BANK_CARD_WITHDRAW", "");

        // Inputs
        _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN");
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CASH_IN", _value);
        _currency_value = CashierSessionData.total_bank_card + CashierSessionData.total_cash_advance_bank_card;
        _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_CASH_IN", _currency_value);
      }

      //Game profit
      if (CashierSessionData.currency_game_profit.ContainsKey(_currency.Key))
      {
        _game_profit = CashierSessionData.currency_game_profit[_currency.Key];
        _value = Resource.String("STR_VOUCHER_OVERVIEW_GAME_PROFIT");
        _temp_voucher.AddString("STR_VOUCHER_GAME_PROFIT", _value);

        if (_currency.Key.IsoCode == _national_currency)
        {
          _temp_voucher.AddCurrency("VOUCHER_GAME_PROFIT", (Currency)_game_profit);
        }
        else
        {
          _temp_voucher.AddString("VOUCHER_GAME_PROFIT", Currency.Format(_game_profit, _currency.Key.IsoCode));
        }
      }
      else
      {
        _temp_voucher.AddString("STR_VOUCHER_GAME_PROFIT", "");
        _temp_voucher.AddString("VOUCHER_GAME_PROFIT", "");
      }

      // ESE 27-SEP-2016
      // Balance
      if (Misc.IsBankCardCloseCashEnabled())
      {
        //Total credit cards
        _value = Resource.String("STR_IN_CASH_DESK_CREDIT_CARD");
      }
      else
      {
        //Cash
        _value = Resource.String("STR_IN_CASH_DESK");
      }

      // LTC 05-AGO-2016
      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_BALANCE", _value);
      if (Misc.IsBankCardCloseCashEnabled() && Misc.PrintBankCardSection) //LTC 06-SEP-2016
      {
        _currency_value = Withdrawal.AmountTotal;
      }
      else
      {
        //EOR 20-JUN-2017
        _currency_value = CashierSessionData.total_bank_card + 
                          CashierSessionData.total_cash_advance_bank_card + 
                          _game_profit - 
                          CashierSessionData.vouchers_delivered_amount -
                          CashierSessionData.current_card_withdrawal_total;
      }

      _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_BALANCE", (Currency)_currency_value);

      //FGB 21-FEB-2017 Bug 24801: WigosGUI: Incorrect collection in the cash closing, due to Gp BankCard.Conciliate
      //Has to hide short or over on bank card with pinpad and conciliate
      Boolean _has_to_hide_shortorover_onbankcardwithpinpad;
      _has_to_hide_shortorover_onbankcardwithpinpad = Misc.HasToHideShortOrOverOnBankCardWithPinpadAndConciliation(ParametersOverview) 
                                                   || CashierSessionData.m_cashier_session_has_pinpad_operations;

      // JML 05-AUG-2015
      if (!CashierSessionData.show_short_over || _has_to_hide_shortorover_onbankcardwithpinpad)
      {
        _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLLECTED", "");
        _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLLECTED", "");

        _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", "");
        _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", "");
        _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", "");
        _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", "");
      }

      //Collected
      _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED");
      _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLLECTED", _value);

      _collected_value = _currency_value;

      //Short
      _currency_value = CashierSessionData.short_currency.ContainsKey(_currency.Key) ? CashierSessionData.short_currency[_currency.Key] : 0;
      if (_currency_value > 0)
      {
        _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING");
        _collected_value -= _currency_value;

        _temp_voucher.AddCurrency("VOUCHER_CURRENCY_CASH_BANK_COLL_DIFF", _currency_value);
        _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", _value);
        _temp_voucher.AddCurrency("VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", _currency_value);
      }
      else
      {
        _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_DIFF", "");
        _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", "");
        _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", "");
      }

      //Over
      _currency_value = CashierSessionData.over_currency.ContainsKey(_currency.Key) ? CashierSessionData.over_currency[_currency.Key] : 0;
      if (_currency_value > 0)
      {
        _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA");
        _collected_value += _currency_value;

        _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", _value);
        _temp_voucher.AddCurrency("VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", _currency_value);
      }
      else
      {
        _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", "");
        _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", "");
      }

      // Collected
      _temp_voucher.AddCurrency("VOUCHER_CURRENCY_CASH_BANK_COLLECTED", _collected_value);

      // EOR 07-SEP-2016
      //Card deposit
      if (Misc.IsVoucherModeTV())
      {
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CARD_DEPOSIT", "");
        _temp_voucher.AddString("VOUCHER_CASH_BANK_CARD_DEPOSIT", "");
      }
      else
      {
        _value = Misc.GetCardPlayerConceptName();
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CARD_DEPOSIT", _value);
        _currency_value = CashierSessionData.currency_cash_in_card_deposit.ContainsKey(_currency.Key) ? CashierSessionData.currency_cash_in_card_deposit[_currency.Key] : 0;
        _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_CARD_DEPOSIT", _currency_value);
      }

      // RGR 22-08-2017
      // Not show commission if is televisa mode
      if (Misc.IsVoucherModeTV())
      {
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_COMMISSION", "");
        _temp_voucher.AddString("VOUCHER_CASH_BANK_COMMISSION", "");

      }
      else
      {
        _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION");
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_COMMISSION", _value);
        _currency_value = CashierSessionData.currency_cash_in_commissions.ContainsKey(_currency.Key) ? CashierSessionData.currency_cash_in_commissions[_currency.Key] : 0;
        _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_COMMISSION", _currency_value);

      }

      // LTC 05-AGO-2016
      //EOR 20-JUN-2017
      if (Misc.IsBankCardCloseCashEnabled())  //LTC 06-SEP-2016
      {
        if (Misc.PrintBankCardSection)
        {
        // Vouchers
        _value = Resource.String("STR_VOUCHER_CASH_DESK_WITHDRAWAL_COUNT_TITLE");
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_VOUCHERS", _value);
        _temp_voucher.AddInteger("VOUCHER_CASH_BANK_VOUCHERS", Withdrawal.Count);

        // Delivered Vouchers
        _value = Resource.String("STR_VOUCHER_MULTI_EXCHANGE_DELIVERED");
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_DELIVERED_VOUCHERS", _value);
        _temp_voucher.AddInteger("VOUCHER_CASH_BANK_DELIVERED_VOUCHERS", Withdrawal.CountDelivered);

        // No Delivered Vouchers
        _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING");
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_NO_DELIVERED_VOUCHERS", _value);
        _temp_voucher.AddInteger("VOUCHER_CASH_BANK_NO_DELIVERED_VOUCHERS", Withdrawal.Count - Withdrawal.CountDelivered);

          // Withdrawal EOR 20-JUN-2017
          _temp_voucher.AddString("STR_VOUCHER_WITHDRAWAL_BANK_CARD", "");
          _temp_voucher.AddString("VOUCHER_WITHDRAWAL_BANK_CARD", "");
          _temp_voucher.AddString("STR_VOUCHER_CASHBANK_DELIVERED_VOUCHERS", "");
          _temp_voucher.AddString("VOUCHER_CASHBANK_DELIVERED_VOUCHERS", "");

        _value = "<tr><td colspan=\"3\" style=\"height:15px;\"></td></tr>";
        _value = _value + "<tr><td colspan=\"3\"><hr noshade size=\"2\"><p align=\"center\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td><b>"
                        + Resource.String("STR_VOUCHER_CASH_DESK_WITHDRAWAL_FOOTER_SIGNATURE")
                        + "</b></td></tr></table></p></td></tr>";
        _value = _value + "<tr height=30px><td><b /></td></tr><tr><td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr valign=top><td align=\"left\" width=\"50%\">"
                        + Resource.String("STR_VOUCHER_CASH_DESK_WITHDRAWAL_FOOTER_ANFIT") + " :</td></tr></table></td></tr>";
        _value = _value + "<tr height=40px><td><b /></td></tr>";
        _value = _value + "<tr><td><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr valign=top><td align=\"left\" width=\"50%\">"
                        + Resource.String("STR_VOUCHER_CASH_DESK_WITHDRAWAL_FOOTER_CASHIER") + " : </td></tr></table></td></tr><tr height=30px><td><b /></td></tr>";

        _temp_voucher.VoucherHTML = _temp_voucher.VoucherHTML + _value;

        }//ESE 27-SEP-2016  //EOR 20-JUN-2017
      // "PrintBankCardSection_StatusCash" defines if the card info have to be shown (ATB 10-NOV-2016)
        else if (Misc.PrintBankCardSection_StatusCash)
      {
        // Vouchers
        _value = Resource.String("STR_VOUCHER_CASH_DESK_WITHDRAWAL_COUNT_TITLE");
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_VOUCHERS", _value);
        _temp_voucher.AddInteger("VOUCHER_CASH_BANK_VOUCHERS", Withdrawal.CountDeliveredCutOff + Withdrawal.CountNoDeliveredCutOff);

        // Delivered Vouchers
        _value = Resource.String("STR_VOUCHER_MULTI_EXCHANGE_DELIVERED");
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_DELIVERED_VOUCHERS", _value);
        _temp_voucher.AddInteger("VOUCHER_CASH_BANK_DELIVERED_VOUCHERS", Withdrawal.CountDeliveredCutOff);

        // No Delivered Vouchers
        _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING");
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_NO_DELIVERED_VOUCHERS", _value);
        _temp_voucher.AddInteger("VOUCHER_CASH_BANK_NO_DELIVERED_VOUCHERS", Withdrawal.CountNoDeliveredCutOff);

          // Withdrawal EOR 20-JUN-2017
          _temp_voucher.AddString("STR_VOUCHER_WITHDRAWAL_BANK_CARD", "");
          _temp_voucher.AddString("VOUCHER_WITHDRAWAL_BANK_CARD", "");
          _temp_voucher.AddString("STR_VOUCHER_CASHBANK_DELIVERED_VOUCHERS", "");
          _temp_voucher.AddString("VOUCHER_CASHBANK_DELIVERED_VOUCHERS", "");
        }
        else
        {
          //EOR 20-JUN-2017
          _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_DELIVERED_VOUCHERS", "");
          _temp_voucher.AddString("VOUCHER_CASH_BANK_DELIVERED_VOUCHERS", "");
          _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_NO_DELIVERED_VOUCHERS", "");
          _temp_voucher.AddString("VOUCHER_CASH_BANK_NO_DELIVERED_VOUCHERS", "");

          // Withdrawal
          if(CashierSessionData.m_cashier_session_has_pinpad_operations)
          {
            CashierSessionData.vouchers_delivered_amount = 0;
            _temp_voucher.AddString("STR_VOUCHER_WITHDRAWAL_BANK_CARD", String.Empty);
            _temp_voucher.AddString("VOUCHER_WITHDRAWAL_BANK_CARD", String.Empty);

          // Vouchers
          _value = Resource.String("STR_VOUCHER_CASH_DESK_WITHDRAWAL_COUNT_TITLE");
          _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_VOUCHERS", _value);
          _temp_voucher.AddInteger("VOUCHER_CASH_BANK_VOUCHERS", CashierSessionData.total_vouchers_number);

            _value = Resource.String("STR_SUMMARY_CASH_DESK_DELIVERED_VOUCHERS");
            _temp_voucher.AddString("STR_VOUCHER_CASHBANK_DELIVERED_VOUCHERS", _value);
            _temp_voucher.AddInteger("VOUCHER_CASHBANK_DELIVERED_VOUCHERS", CashierSessionData.vouchers_delivered);
          }
          else
          {
          _value = Resource.String("STR_SUMMARY_CASH_DESK_BANK_CARD_WITHDRAWAL");
          _temp_voucher.AddString("STR_VOUCHER_WITHDRAWAL_BANK_CARD", _value);
          _temp_voucher.AddCurrency("VOUCHER_WITHDRAWAL_BANK_CARD", CashierSessionData.vouchers_delivered_amount);

            _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_VOUCHERS", String.Empty);
            _temp_voucher.AddString("VOUCHER_CASH_BANK_VOUCHERS", String.Empty);
            _temp_voucher.AddString("STR_VOUCHER_CASHBANK_DELIVERED_VOUCHERS", String.Empty);
            _temp_voucher.AddString("VOUCHER_CASHBANK_DELIVERED_VOUCHERS", String.Empty);
          }
        }
      }
      else // ATB 25-10-2016
      {
        // In case of no conciliation, we don't show the voucher's count
        // Vouchers
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_VOUCHERS", "");
        _temp_voucher.AddString("VOUCHER_CASH_BANK_VOUCHERS", "");

        // Delivered Vouchers
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_DELIVERED_VOUCHERS", "");
        _temp_voucher.AddString("VOUCHER_CASH_BANK_DELIVERED_VOUCHERS", "");

        // No Delivered Vouchers
        _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_NO_DELIVERED_VOUCHERS", "");
        _temp_voucher.AddString("VOUCHER_CASH_BANK_NO_DELIVERED_VOUCHERS", "");

        // Withdrawal EOR 20-JUN-2017
        _temp_voucher.AddString("STR_VOUCHER_WITHDRAWAL_BANK_CARD", "");
        _temp_voucher.AddString("VOUCHER_WITHDRAWAL_BANK_CARD", "");
        _temp_voucher.AddString("STR_VOUCHER_CASHBANK_DELIVERED_VOUCHERS", "");
        _temp_voucher.AddString("VOUCHER_CASHBANK_DELIVERED_VOUCHERS", "");
      }

      _currencies_html += _temp_voucher.VoucherHTML;
    }

    private static void HtmlCreditLineInformation(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData, String _national_currency, ref string _currencies_html, ref KeyValuePair<CurrencyIsoType, Decimal> _currency, out String _value, out Decimal _currency_value, out Voucher _temp_voucher)
    {
      Decimal _game_profit;

      _game_profit = 0;
      _temp_voucher = new Voucher();

      _temp_voucher.LoadVoucher("VoucherCashBankTransactions");

      _value = CurrencyExchange.GetDescription(_currency.Key.Type, _currency.Key.IsoCode, _currency.Key.IsoCode);
      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_TITLE", _value);

      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CREDIT_CARD", "");
      _temp_voucher.AddString("VOUCHER_CASH_BANK_CREDIT_CARD", "");
      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_DEBIT_CARD", "");
      _temp_voucher.AddString("VOUCHER_CASH_BANK_DEBIT_CARD", "");

      _temp_voucher.AddString("STR_VOUCHER_WITHDRAWAL_BANK_CARD", "");  //EOR 20-JUN-2017
      _temp_voucher.AddString("VOUCHER_WITHDRAWAL_BANK_CARD", "");
      _temp_voucher.AddString("STR_VOUCHER_CASHBANK_DELIVERED_VOUCHERS", "");
      _temp_voucher.AddString("VOUCHER_CASHBANK_DELIVERED_VOUCHERS", "");

      //MARKER
      _value = Resource.String("STR_VOUCHER_CREDIT_LINE_MARKER");
      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_WITHDRAWS", _value);
      _currency_value = CashierSessionData.total_credit_line_marker;
      _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_WITHDRAWS", _currency_value);

      //PAYBACK
      _value = Resource.String("STR_VOUCHER_CREDIT_LINE_PAYBACK");
      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CASH_IN", _value);
      _currency_value = CashierSessionData.total_credit_line_payback;
      _temp_voucher.AddCurrency("VOUCHER_CASH_BANK_CASH_IN", _currency_value);

      //Game profit
      if (CashierSessionData.currency_game_profit.ContainsKey(_currency.Key))
      {
        _game_profit = CashierSessionData.currency_game_profit[_currency.Key];
        _value = Resource.String("STR_VOUCHER_OVERVIEW_GAME_PROFIT");
        _temp_voucher.AddString("STR_VOUCHER_GAME_PROFIT", _value);

        if (_currency.Key.IsoCode == _national_currency)
        {
          _temp_voucher.AddCurrency("VOUCHER_GAME_PROFIT", (Currency)_game_profit);
        }
        else
        {
          _temp_voucher.AddString("VOUCHER_GAME_PROFIT", Currency.Format(_game_profit, _currency.Key.IsoCode));
        }
      }
      else
      {
        _temp_voucher.AddString("STR_VOUCHER_GAME_PROFIT", "");
        _temp_voucher.AddString("VOUCHER_GAME_PROFIT", "");
      }

      // Balance ETP 15/03/2017 Not showing Credit Lines Balance.
      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_BALANCE", "");
      _temp_voucher.AddString("VOUCHER_CASH_BANK_BALANCE", "");

      _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLLECTED", "");
      _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLLECTED", "");
      _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", "");
      _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", "");
      _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", "");
      _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", "");
      

      _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", "");
      _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_DIFF", "");
      _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_SHORT", "");

      _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", "");
      _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLL_OVER", "");
      

      _temp_voucher.AddString("STR_VOUCHER_CURRENCY_CASH_BANK_COLLECTED", "");
      _temp_voucher.AddString("VOUCHER_CURRENCY_CASH_BANK_COLLECTED", "");

      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_CARD_DEPOSIT", "");
      _temp_voucher.AddString("VOUCHER_CASH_BANK_CARD_DEPOSIT", "");

      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_COMMISSION", "");
      _temp_voucher.AddString("VOUCHER_CASH_BANK_COMMISSION", "");

      // Vouchers
      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_VOUCHERS", "");
      _temp_voucher.AddString("VOUCHER_CASH_BANK_VOUCHERS", "");

      // Delivered Vouchers
      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_DELIVERED_VOUCHERS", "");
      _temp_voucher.AddString("VOUCHER_CASH_BANK_DELIVERED_VOUCHERS", "");

      // No Delivered Vouchers
      _temp_voucher.AddString("STR_VOUCHER_CASH_BANK_NO_DELIVERED_VOUCHERS", "");
      _temp_voucher.AddString("VOUCHER_CASH_BANK_NO_DELIVERED_VOUCHERS", "");

      _currencies_html += _temp_voucher.VoucherHTML;
    }

    private void HtmlChipsInformation(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData, Boolean Collected, Boolean IsMovementFilter, String _national_currency, ref string _currencies_html, Int32 _exchange_currencies_count, Decimal _exchange_currency_value, Boolean _is_color_chip_type, ref KeyValuePair<CurrencyIsoType, Decimal> _currency, out String _value, out Decimal _currency_value, out Decimal _game_profit, out Decimal _collected_value)
    {
      _game_profit = 0;
      _collected_value = 0;

      // Load HTML structure for the current currency
      LoadVoucher("VoucherCurrencyCash");

      // RMS & MPO & DRV: incorrect currency title label on credit card and check.
      _value = CurrencyExchange.GetDescription(_currency.Key.Type, _currency.Key.IsoCode, _currency.Key.IsoCode);

      //Title
      SetParameterValue("@STR_VOUCHER_CURRENCY_CASH_TITLE", _value + " <br/><hr noshade size=\"2\">");

      if (_is_color_chip_type)
      {
        _currency_value = ChipsColorReplace(CashierSessionData, Collected, IsMovementFilter, _national_currency, _exchange_currencies_count, _exchange_currency_value, ref _currency, ref _value, ref _game_profit, ref _collected_value);
      }
      else
      {
        _currency_value = ChipsValuesReplace(CashierSessionData, Collected, IsMovementFilter, _national_currency, _exchange_currencies_count, _exchange_currency_value, _is_color_chip_type, ref _currency, ref _value, ref _game_profit, ref _collected_value);
      }

      _currencies_html += VoucherHTML;
    }

    private decimal ChipsValuesReplace(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData, Boolean Collected, Boolean IsMovementFilter, String _national_currency, Int32 _exchange_currencies_count, Decimal _exchange_currency_value, Boolean _is_color_chip_type, ref KeyValuePair<CurrencyIsoType, Decimal> _currency, ref String _value, ref Decimal _game_profit, ref Decimal _collected_value)
    {
      Decimal _currency_value;
      Decimal _collected_diff;
      CashierSessionInfo _session_info;

      _session_info = CommonCashierInformation.CashierSessionInfo();


      SetParameterValue("@STR_VISIBILITY", "");

      //Chips
      if (_currency.Key.IsCasinoChip)
      {
        //Fills
        _value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CHIPS_CASH_IN");
        AddString("STR_VOUCHER_CURRENCY_CASH_DEPOSIT", _value);

        _currency_value = CashierSessionData.fill_in_balance.ContainsKey(_currency.Key) ? CashierSessionData.fill_in_balance[_currency.Key] : 0;
        AddCurrencyType("VOUCHER_CURRENCY_CASH_DEPOSIT", _currency.Key, _currency_value, _national_currency);

        //Credits
        _value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CHIPS_CASH_OUT");
        AddString("STR_VOUCHER_CURRENCY_CASH_WITHDRAW", _value);

        _currency_value = CashierSessionData.fill_out_balance.ContainsKey(_currency.Key) ? CashierSessionData.fill_out_balance[_currency.Key] : 0;
        AddCurrencyType("VOUCHER_CURRENCY_CASH_WITHDRAW", _currency.Key, _currency_value, _national_currency);
      }
      else
      {
        //Deposit
        _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT");
        AddString("STR_VOUCHER_CURRENCY_CASH_DEPOSIT", _value);

        _currency_value = CashierSessionData.fill_in_balance.ContainsKey(_currency.Key) ? CashierSessionData.fill_in_balance[_currency.Key] : 0;
        AddCurrencyType("VOUCHER_CURRENCY_CASH_DEPOSIT", _currency.Key, _currency_value, _national_currency);


        //Withdraws
        _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW");
        AddString("STR_VOUCHER_CURRENCY_CASH_WITHDRAW", _value);

        _currency_value = CashierSessionData.fill_out_balance.ContainsKey(_currency.Key) ? CashierSessionData.fill_out_balance[_currency.Key] : 0;
        AddCurrencyType("VOUCHER_CURRENCY_CASH_WITHDRAW", _currency.Key, _currency_value, _national_currency);
      } //chips

      //Game profit
      if (CashierSessionData.currency_game_profit.ContainsKey(_currency.Key))
      {
        _game_profit = CashierSessionData.currency_game_profit[_currency.Key];
        _value = Resource.String("STR_VOUCHER_OVERVIEW_GAME_PROFIT");
        AddString("STR_VOUCHER_GAME_PROFIT", _value);
        AddCurrencyType("VOUCHER_GAME_PROFIT", _currency.Key, _game_profit, _national_currency);
      }
      else
      {
        AddString("STR_VOUCHER_GAME_PROFIT", "");
        AddString("VOUCHER_GAME_PROFIT", "");
      } //Game Profit

      //Cash In
      // LJM 14-DEC-2013: Not needed on chips
      if (!_currency.Key.IsCasinoChip)
      {
        SetParameterValue("@STR_VISIBILITY", "");
        _value = Resource.String("STR_UC_MB_CARD_BALANCE_CASH_IN_NAME");
        AddString("STR_VOUCHER_CURRENCY_CASH_CASH_IN", _value);
        _currency_value = CashierSessionData.currency_cash_in.ContainsKey(_currency.Key) ? CashierSessionData.currency_cash_in[_currency.Key] : 0;
        AddCurrencyType("VOUCHER_CURRENCY_CASH_CASH_IN", _currency.Key, _currency_value, _national_currency);
      }
      else
      {
        SetParameterValue("@STR_VISIBILITY", "display: none;");
        AddString("STR_VOUCHER_CURRENCY_CASH_CASH_IN", "");
        AddString("VOUCHER_CURRENCY_CASH_CASH_IN", "");
      } // Cash in

      // EOR 05-ENE-2018 Not aplied for Chips Redemed and Not Redemed 
      // FAV 03-MAY-2015 (Currency exchange multiple denominations)
      if (Misc.IsMultiCurrencyExchangeEnabled() & !(_currency.Key.Type == CurrencyExchangeType.CASINO_CHIP_RE | _currency.Key.Type == CurrencyExchangeType.CASINO_CHIP_NRE))
      {
        _value = Resource.String("STR_VOUCHER_MULTI_EXCHANGE_CASH_DESK");
        AddString("STR_VOUCHER_CURRENCY_EXCHANGE_CASH_IN", _value);

        _currency_value = CashierSessionData.currency_exchange_cash_in.ContainsKey(_currency.Key) ? CashierSessionData.currency_exchange_cash_in[_currency.Key] : 0;
        AddCurrencyType("VOUCHER_CURRENCY_EXCHANGE_CASH_IN", _currency.Key, _currency_value, _national_currency);
      }
      else
      {
        AddString("STR_VOUCHER_CURRENCY_EXCHANGE_CASH_IN", "");
        AddString("VOUCHER_CURRENCY_EXCHANGE_CASH_IN", "");
      }  //Multiple Denominations Cash In

      // EOR 28-DEC-2017 Not aplied for Chips Redemed and Not Redemed 
      // FAV 03-MAY-2015 (Currency exchange multiple denominations)
      // Devoluciones de divisas
      if (Misc.IsMultiCurrencyExchangeEnabled() & !(_currency.Key.Type == CurrencyExchangeType.CASINO_CHIP_RE | _currency.Key.Type == CurrencyExchangeType.CASINO_CHIP_NRE))
      {
        _value = Resource.String("STR_VOUCHER_MULTI_EXCHANGE_DEVOLUTION_CASH_DESK");
        AddString("STR_VOUCHER_CURRENCY_EXCHANGE_CASH_OUT", _value);

        _currency_value = CashierSessionData.currency_exchange_cash_out.ContainsKey(_currency.Key) ? CashierSessionData.currency_exchange_cash_out[_currency.Key] : 0;
        AddCurrencyType("VOUCHER_CURRENCY_EXCHANGE_CASH_OUT", _currency.Key, _currency_value, _national_currency);
      }
      else
      {
        AddString("STR_VOUCHER_CURRENCY_EXCHANGE_CASH_OUT", "");
        AddString("VOUCHER_CURRENCY_EXCHANGE_CASH_OUT", "");
      }  //Multiple Denominations Cash Out

      //Balance
      _value = Resource.String("STR_IN_CASH_DESK");
      AddString("STR_VOUCHER_CURRENCY_CASH_BALANCE", _value);

      Boolean _chips_sale_mode = FeatureChips.CashierChipsSaleMode(false); // DLM 26-06-2018

      if (IsMovementFilter && _currency.Key.IsoCode == _national_currency)
      {
        AddString("VOUCHER_CURRENCY_CASH_BALANCE", "---");
      }
      else
      {
        //Adds Fill
        AddCurrencyType("VOUCHER_CURRENCY_CASH_BALANCE", _currency.Key, _currency.Value, _national_currency);
        _currency_value = _currency.Value;
      } // Balance

      // FAV 14-APR-2016
      _collected_diff = 0;
      if (CashierSessionData.collected_diff.ContainsKey(_currency.Key))
      {
        _collected_diff = CashierSessionData.collected_diff[_currency.Key];
      }

      // FAV 14-APR-2016
      _collected_diff = 0;
      // RAB 22-APR-2016
      Decimal _collected_chips = 0;
      if (CashierSessionData.collected_diff.ContainsKey(_currency.Key))
      {
        _collected_diff = CashierSessionData.collected_diff[_currency.Key];
      }

      // JML: only show cash over/short when cashier session desk is not integrated
      if (_session_info.UserType == GU_USER_TYPE.USER && !Cage.IsGamingTable(_session_info.CashierSessionId))
      {
      // JML 05-AUG-2015
      if (_collected_diff == 0)
      {
        if (Collected)
        {
          _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED");
          AddString("STR_VOUCHER_CURRENCY_CASH_COLLECTED", _value);

          if (_is_color_chip_type)
          {
            AddString("VOUCHER_CURRENCY_CASH_COLLECTED", Convert.ToInt32(_currency_value).ToString());
          }
          else
          {
            AddCurrencyType("VOUCHER_CURRENCY_CASH_COLLECTED", _currency.Key, _currency_value, _national_currency);
          }
        }
        else
        {
          AddString("STR_VOUCHER_CURRENCY_CASH_COLLECTED", "");
          AddString("VOUCHER_CURRENCY_CASH_COLLECTED", "");
        } //Collected

        AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", "");
        AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", "");
        AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", "");
        AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", "");
      }
      else
      { // _collected_diff != 0
        //Collected
        _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED");
        AddString("STR_VOUCHER_CURRENCY_CASH_COLLECTED", _value);

        _collected_value = _currency_value;

        if (_collected_diff < 0)
        {
          _collected_diff = -_collected_diff;
          if (_collected_diff != _collected_value)
          {
            _collected_chips = _collected_value - _collected_diff;
          }
          else
          {
            _collected_chips = 0;
          }

          //Short
          _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING");

          AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", _value);
          if (_is_color_chip_type)
          {
            AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", Convert.ToInt32(_collected_diff).ToString());

            // Collected
            AddString("VOUCHER_CURRENCY_CASH_COLLECTED", Convert.ToInt32(_collected_chips).ToString());
          }
          else
          {
            AddCurrencyType("VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", _currency.Key, _collected_diff, _national_currency);

            // Collected
            AddCurrencyType("VOUCHER_CURRENCY_CASH_COLLECTED", _currency.Key, _collected_chips, _national_currency);
          }

          AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", "");
          AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", "");
        }
        else
        {
          //Over
          _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA");

          if (_collected_diff != _collected_value)
          {
            _collected_chips = _collected_value + _collected_diff;
          }
          else
          {
            _collected_chips = 0;
          }

          AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", _value);
          AddCurrencyType("VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", _currency.Key, _collected_diff, _national_currency);
          if (_is_color_chip_type)
          {
            AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", Convert.ToInt32(_collected_diff).ToString());

            // Collected
            AddString("VOUCHER_CURRENCY_CASH_COLLECTED", Convert.ToInt32(_collected_chips).ToString());
          }
          else
          {
            AddCurrencyType("VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", _currency.Key, _collected_diff, _national_currency);

            // Collected
            AddCurrencyType("VOUCHER_CURRENCY_CASH_COLLECTED", _currency.Key, (_collected_chips), _national_currency);
          }

          AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", "");
          AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", "");
        }
        }

      }
      else // - // JML: only show cash over/short when cashier session desk is not integrated
      {

        //Collected
        if (CashierSessionData.ClosingStock != null && CashierSessionData.ClosingStock.SleepsOnTable)
        {
          _value = Resource.String("STR_UC_VOUCHER_CASH_DESK_CLOSE_ON_BANK");
          AddString("STR_VOUCHER_CURRENCY_CASH_COLLECTED", _value);
        }
        else
        {
          _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED");
          AddString("STR_VOUCHER_CURRENCY_CASH_COLLECTED", _value);
        }

        //_collected_value = _currency_value;
        if ((CashierSessionData.collected.Count > 0) && CashierSessionData.collected.ContainsKey(_currency.Key))
        {
        _collected_chips = CashierSessionData.collected[_currency.Key]; 
        }




        //Game profit
        if (CashierSessionData.collected_diff.ContainsKey(_currency.Key))
        {
          _game_profit = CashierSessionData.collected_diff[_currency.Key];
          _value = Resource.String("STR_VOUCHER_OVERVIEW_GAME_PROFIT");
          AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", _value);
          AddCurrencyType("VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", _currency.Key, _game_profit, _national_currency);
        }
        else
        {
          AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", "");
          AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", "");
        } //Game Profit



 


        // Collected
        if (_is_color_chip_type)
        {
          AddString("VOUCHER_CURRENCY_CASH_COLLECTED", Convert.ToInt32(_collected_chips).ToString());
        }
        else
        {
          AddCurrencyType("VOUCHER_CURRENCY_CASH_COLLECTED", _currency.Key, _collected_chips, _national_currency);
        }

        AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", "");
        AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", "");
        AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", "");
        AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", "");
      } //END - // JML: only show cash over/short when cashier session desk is not integrated

      if (_currency.Key.IsoCode != _national_currency && !_currency.Key.IsCasinoChip)
      {
        //Total Amount
        SetParameterValue("@STR_FONT_SIZE", "font-size: 90%;");
        _value = Resource.String("STR_FRM_AMOUNT_INPUT_EQUALS");
        AddString("STR_VOUCHER_CURRENCY_CASH_AMOUNT", _value);

        _currency_value = CashierSessionData.currency_cash_in_exchange.ContainsKey(_currency.Key) ? CashierSessionData.currency_cash_in_exchange[_currency.Key] : 0;
        AddCurrency("VOUCHER_CURRENCY_CASH_AMOUNT", (Currency)_currency_value);
      }
      else
      {
        SetParameterValue("@STR_FONT_SIZE", "");
        AddString("STR_VOUCHER_CURRENCY_CASH_AMOUNT", "");
        AddString("VOUCHER_CURRENCY_CASH_AMOUNT", "");
      }

      //Card deposit only if one exchange currency avalaible
      if (_exchange_currencies_count == 1)
      {
        if (!_currency.Key.IsCasinoChip && _currency.Key.IsoCode != _national_currency && _currency.Key.Type != CurrencyExchangeType.CHECK && _currency.Key.Type != CurrencyExchangeType.CARD)
        {
          SetParameterValue("@VOUCHER_CURRENCY_CASH_CARD_DEPOSIT", AddCardDepositExchange(_exchange_currency_value, true));
        }
      }
      AddString("VOUCHER_CURRENCY_CASH_CARD_DEPOSIT", "");

      //Commission
      if (!_currency.Key.IsCasinoChip)
      {
        SetParameterValue("@STR_VISIBILITY", "");
        _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION");
        AddString("STR_VOUCHER_CURRENCY_CASH_COMMISSION", _value);
        _currency_value = CashierSessionData.currency_cash_in_commissions.ContainsKey(_currency.Key) ? CashierSessionData.currency_cash_in_commissions[_currency.Key] : 0;
        AddCurrency("VOUCHER_CURRENCY_CASH_COMMISSION", (Currency)_currency_value);
      }
      else
      {
        AddString("STR_VOUCHER_CURRENCY_CASH_COMMISSION", "");
        AddString("VOUCHER_CURRENCY_CASH_COMMISSION", "");
      }

      _currency_value = 0;
      _value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CASH_IN");
      AddString("STR_UC_BANK_MOVEMENTS_SUMMARY_CHIPS_CASH_IN", _value);

      // DLM 26-JUN-2018
      if (CashierSessionData.chips_purchase.ContainsKey(_currency.Key))
      {
        _currency_value = CashierSessionData.chips_purchase[_currency.Key];
      }

      AddCurrencyType("VOUCHER_SUMMARY_CHIPS_CASH_IN", _currency.Key, (Currency)_currency_value, _national_currency);

      _currency_value = 0;
      _value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CASH_OUT");
      AddString("STR_UC_BANK_MOVEMENTS_SUMMARY_CHIPS_CASH_OUT", _value);

        // DLM 26-JUN-2018
      if (CashierSessionData.chips_sale.ContainsKey(_currency.Key))
      {
        _currency_value = CashierSessionData.chips_sale[_currency.Key];
      }

      AddCurrencyType("VOUCHER_CHIPS_CASH_OUT", _currency.Key, (Currency)_currency_value, _national_currency);

      if (_currency.Key.Type != CurrencyExchangeType.CASINO_CHIP_COLOR)
      {
        //SMN 16-SEP-2014
        if ((_currency.Key.IsoCode != _national_currency || FeatureChips.IsChipsType(_currency.Key.Type)))
        {
          SetParameterValue("@STR_VISIBILITY", "");
          _value = Resource.String("STR_VOUCHER_CONCEPTS_IN");
          AddString("STR_VOUCHER_CURRENCY_CONCEPTS_IN", _value);
          _value = Resource.String("STR_VOUCHER_CONCEPTS_OUT");
          AddString("STR_VOUCHER_CURRENCY_CONCEPTS_OUT", _value);
          _currency_value = CashierSessionData.concepts_currency_input.ContainsKey(_currency.Key) ? CashierSessionData.concepts_currency_input[_currency.Key] : 0;
          AddCurrencyType("VOUCHER_CURRENCY_CONCEPTS_IN", _currency.Key, _currency_value, _national_currency);
          _currency_value = CashierSessionData.concepts_currency_output.ContainsKey(_currency.Key) ? CashierSessionData.concepts_currency_output[_currency.Key] : 0;
          AddCurrencyType("VOUCHER_CURRENCY_CONCEPTS_OUT", _currency.Key, _currency_value, _national_currency);

          //AVZ 01-FEB-2016
          if (Entrance.GetReceptionMode() == Entrances.Enums.ENUM_RECEPTION_MODE.TicketWithPrice || CashierSessionData.entrances_currency_input != null)
          {
            SetParameterValue("@STR_VISIBILITY", "");
            _value = Resource.String("STR_MOVEMENT_TYPE_CUSTOMER_ENTRANCE");
            AddString("STR_VOUCHER_CURRENCY_ENTRANCES_IN", _value);

            _currency_value = CashierSessionData.entrances_currency_input.ContainsKey(_currency.Key) ? CashierSessionData.entrances_currency_input[_currency.Key] : 0;
            AddCurrencyType("VOUCHER_CURRENCY_ENTRANCES_IN", _currency.Key, _currency_value, _national_currency);
          }
          else
          {
            AddString("STR_VOUCHER_CURRENCY_ENTRANCES_IN", String.Empty);
            AddString("VOUCHER_CURRENCY_ENTRANCES_IN", String.Empty);
          }
        }
      }
      else
      {
        AddString("STR_VOUCHER_CURRENCY_CONCEPTS_IN", "");
        AddString("STR_VOUCHER_CURRENCY_CONCEPTS_OUT", "");
        AddString("VOUCHER_CURRENCY_CONCEPTS_IN", "");
        AddString("VOUCHER_CURRENCY_CONCEPTS_OUT", "");
        AddString("VOUCHER_CURRENCY_ENTRANCES_IN", "");
        AddString("STR_VOUCHER_CURRENCY_ENTRANCES_IN", "");
      }

      return _currency_value;
    }

    private decimal ChipsColorReplace(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData, Boolean Collected, Boolean IsMovementFilter, String _national_currency, Int32 _exchange_currencies_count, Decimal _exchange_currency_value, ref KeyValuePair<CurrencyIsoType, Decimal> _currency, ref String _value, ref Decimal _game_profit, ref Decimal _collected_value)
    {
      Decimal _currency_value;

      //Fills
      _value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CHIPS_CASH_IN");
      AddString("STR_VOUCHER_CURRENCY_CASH_DEPOSIT", _value);
      _currency_value = CashierSessionData.fill_in_balance.ContainsKey(_currency.Key) ? CashierSessionData.fill_in_balance[_currency.Key] : 0;
      AddString("VOUCHER_CURRENCY_CASH_DEPOSIT", Convert.ToInt32(_currency_value).ToString());

      //Credits
      _value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CHIPS_CASH_OUT");
      AddString("STR_VOUCHER_CURRENCY_CASH_WITHDRAW", _value);

      _currency_value = CashierSessionData.fill_out_balance.ContainsKey(_currency.Key) ? CashierSessionData.fill_out_balance[_currency.Key] : 0;
      AddString("VOUCHER_CURRENCY_CASH_WITHDRAW", Convert.ToInt32(_currency_value).ToString());

      SetParameterValue("@STR_VISIBILITY", "display: none;");
      AddString("STR_UC_BANK_MOVEMENTS_SUMMARY_CHIPS_CASH_IN ", "");
      AddString("VOUCHER_SUMMARY_CHIPS_CASH_IN ", "");
      AddString("STR_UC_BANK_MOVEMENTS_SUMMARY_CHIPS_CASH_OUT ", "");
      AddString("VOUCHER_CHIPS_CASH_OUT ", "");

      //Game profit
      if (CashierSessionData.currency_game_profit.ContainsKey(_currency.Key))
      {
        _game_profit = CashierSessionData.currency_game_profit[_currency.Key];
        SetParameterValue("@STR_VISIBILITY", "");
        _value = Resource.String("STR_VOUCHER_INCREMENT_COLOR");
        AddString("STR_VOUCHER_GAME_PROFIT", _value);
        AddString("VOUCHER_GAME_PROFIT", Convert.ToInt32(_game_profit).ToString());
      }
      else
      {
        AddString("STR_VOUCHER_GAME_PROFIT", "");
        AddString("VOUCHER_GAME_PROFIT", "");
      }

      //Balance
      _value = Resource.String("STR_IN_CASH_DESK");
      AddString("STR_VOUCHER_CURRENCY_CASH_BALANCE", _value);
      if (IsMovementFilter && _currency.Key.IsoCode == _national_currency)
      {
        AddString("VOUCHER_CURRENCY_CASH_BALANCE", "---");
      }
      else
      {
        _currency_value = _currency.Value;
        AddString("VOUCHER_CURRENCY_CASH_BALANCE", Convert.ToInt32(_currency.Value).ToString());
      }

      if (_currency.Key.IsoCode != _national_currency && !_currency.Key.IsCasinoChip)
      {
        //Total Amount
        SetParameterValue("@STR_FONT_SIZE", "font-size: 90%;");
        _value = Resource.String("STR_FRM_AMOUNT_INPUT_EQUALS");
        AddString("STR_VOUCHER_CURRENCY_CASH_AMOUNT", _value);

        _currency_value = CashierSessionData.currency_cash_in_exchange.ContainsKey(_currency.Key) ? CashierSessionData.currency_cash_in_exchange[_currency.Key] : 0;
        AddString("VOUCHER_CURRENCY_CASH_AMOUNT", Convert.ToInt32(_currency_value).ToString());
      }
      else
      {
        SetParameterValue("@STR_VISIBILITY", "display: none;");
        SetParameterValue("@STR_FONT_SIZE", "");
        AddString("STR_VOUCHER_CURRENCY_CASH_AMOUNT", "");
        AddString("VOUCHER_CURRENCY_CASH_AMOUNT", "");
      }

      AddString("STR_VOUCHER_CURRENCY_CONCEPTS_IN ", "");
      AddString(" VOUCHER_CURRENCY_CONCEPTS_IN ", "");
      AddString("STR_VOUCHER_CURRENCY_CONCEPTS_OUT ", "");
      AddString("VOUCHER_CURRENCY_CONCEPTS_OUT ", "");
      AddString("STR_VOUCHER_CURRENCY_ENTRANCES_IN ", "");
      AddString("VOUCHER_CURRENCY_ENTRANCES_IN ", "");
      AddString("STR_VOUCHER_CURRENCY_CASH_CASH_IN ", "");
      AddString("VOUCHER_CURRENCY_CASH_CASH_IN ", "");
      AddString("STR_VOUCHER_CURRENCY_CASH_AMOUNT  ", "");
      AddString("VOUCHER_CURRENCY_CASH_AMOUNT  ", "");
      AddString("VOUCHER_CURRENCY_CASH_CARD_DEPOSIT", "");
      AddString("STR_VOUCHER_CURRENCY_CASH_COMMISSION  ", "");
      AddString("VOUCHER_CURRENCY_CASH_COMMISSION  ", "");
      AddString("STR_VOUCHER_CURRENCY_CASH_BALANCE", "");
      AddString("VOUCHER_CURRENCY_CASH_BALANCE ", "");
      AddString("STR_VOUCHER_CURRENCY_CASH_COLLECTED ", "");
      AddString("VOUCHER_CURRENCY_CASH_COLLECTED ", "");
      AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT ", "");
      AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT ", "");
      AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER ", "");
      AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER ", "");
      AddString("STR_VOUCHER_GAME_PROFIT ", "");
      AddString("VOUCHER_GAME_PROFIT ", "");
      AddString("STR_VOUCHER_CURRENCY_CONCEPTS_IN ", "");
      AddString("VOUCHER_CURRENCY_CONCEPTS_IN ", "");
      AddString("STR_VOUCHER_CURRENCY_CONCEPTS_OUT ", "");
      AddString("VOUCHER_CURRENCY_CONCEPTS_OUT ", "");
      AddString("STR_VOUCHER_CURRENCY_ENTRANCES_IN ", "");
      AddString("VOUCHER_CURRENCY_ENTRANCES_IN ", "");
      AddString("STR_VOUCHER_CURRENCY_CASH_CASH_IN ", "");
      AddString("VOUCHER_CURRENCY_CASH_CASH_IN ", "");
      AddString("STR_VOUCHER_CURRENCY_CASH_AMOUNT  ", "");
      AddString("VOUCHER_CURRENCY_CASH_AMOUNT  ", "");
      AddString("VOUCHER_CURRENCY_CASH_CARD_DEPOSIT", "");
      AddString("STR_VOUCHER_CURRENCY_CASH_COMMISSION  ", "");
      AddString("VOUCHER_CURRENCY_CASH_COMMISSION  ", "");
      AddString("STR_VOUCHER_CURRENCY_CASH_BALANCE", "");
      AddString("VOUCHER_CURRENCY_CASH_BALANCE ", "");
      AddString("STR_VOUCHER_CURRENCY_CASH_COLLECTED ", "");
      AddString("VOUCHER_CURRENCY_CASH_COLLECTED ", "");
      AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT ", "");
      AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT ", "");
      AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER ", "");
      AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER ", "");

      SetParameterValue("@STR_VISIBILITY", "display: none;");
      AddString("STR_VOUCHER_CURRENCY_CASH_CASH_IN", "");
      AddString("VOUCHER_CURRENCY_CASH_CASH_IN", "");
      AddString("STR_VOUCHER_CURRENCY_EXCHANGE_CASH_OUT", "");
      AddString("VOUCHER_CURRENCY_EXCHANGE_CASH_OUT", "");

      Decimal _collected_diff;
      Decimal _collected_chips;
      _collected_diff = 0;
      _collected_chips = 0;

      if (CashierSessionData.collected_diff.ContainsKey(_currency.Key))
      {
        _collected_diff = CashierSessionData.collected_diff[_currency.Key];
      }

      if (_collected_diff == 0)
      {
        if (Collected)
        {
          _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED");
          AddString("STR_VOUCHER_CURRENCY_CASH_COLLECTED", _value);
          AddString("VOUCHER_CURRENCY_CASH_COLLECTED", Convert.ToInt32(_currency_value).ToString());
        }
        else
        {
          AddString("STR_VOUCHER_CURRENCY_CASH_COLLECTED", "");
          AddString("VOUCHER_CURRENCY_CASH_COLLECTED", "");
        }

        AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", "");
        AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", "");
        AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", "");
        AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", "");
      }
      else
      {
        //Collected
        _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED");
        AddString("STR_VOUCHER_CURRENCY_CASH_COLLECTED", _value);

        _collected_value = _currency_value;

        _collected_diff = -_collected_diff;
        if (_collected_diff != _collected_value)
        {
          _collected_chips = _collected_value - _collected_diff;
        }
        else
        {
          _collected_chips = 0;
        }

        // Collected
        AddString("VOUCHER_CURRENCY_CASH_COLLECTED", Convert.ToInt32(_collected_chips).ToString());

        //Short
        if (_collected_diff > 0)
        {
          _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING");

          AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", _value);
          AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", Convert.ToInt32(_collected_diff).ToString());
        }
        else
        {
          AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", "");
          AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_SHORT", "");
        }

        //Over
        _currency_value = CashierSessionData.over_currency.ContainsKey(_currency.Key) ? CashierSessionData.over_currency[_currency.Key] : 0;
        if (_currency_value > 0)
        {
          _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA");

          AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", _value);
          AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", Convert.ToInt32(_currency_value).ToString());
        }
        else
        {
          AddString("STR_VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", "");
          AddString("VOUCHER_CURRENCY_CASH_COLL_CASHIER_OVER", "");
        }
      }

      return _currency_value;
    }

    private String AddCardDepositExchange(Currency TotalCardDeposit, Boolean InsertTab)
    {
      StringBuilder _html;
      String _tab_format;

      _tab_format = string.Empty;

      if (InsertTab)
      {
        _tab_format = "font-size: 90%;";
      }

      _html = new StringBuilder();
      _html.AppendLine("<tr valign=top>");
      _html.AppendLine(String.Format("<td class=\"left\" align=\"left\"  width=\"57%\"><span style=\"{0} margin-left:10px\">{1}</span></td>", _tab_format, Misc.GetCardPlayerConceptName()));
      _html.AppendLine(String.Format("<td class=\"middle\"></td>"));
      _html.AppendLine(String.Format("<td class=\"right\" align=\"right\" width=\"43%\"><span style=\"{0}\">{1}</span></td>", _tab_format, TotalCardDeposit.ToString()));
      _html.AppendLine("</tr>");

      return _html.ToString();
    }

    private void AddCurrencyType(String Parameter, CurrencyIsoType IsoType, Decimal Value, String NationalCurrency)
    {
      if (IsoType.IsCasinoChip && IsoType.Type == CurrencyExchangeType.CASINO_CHIP_COLOR)
      {
        AddString(Parameter, Convert.ToInt32(Value).ToString());
      }
      else
      {
        if (IsoType.IsoCode == NationalCurrency)
        {
          AddCurrency(Parameter, (Currency)Value);
        }
        else
        {
          AddString(Parameter, Currency.Format(Value, IsoType.IsoCode));
        }
      }
    }

    #endregion

    #region Public Methods

    #endregion
  }
}
