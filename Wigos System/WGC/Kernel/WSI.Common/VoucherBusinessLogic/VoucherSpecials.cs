﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherSpecials.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  /// <summary>
  /// Print ticket for special I/O paiments
  /// </summary>
  public class VoucherSpecials : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherSpecials(Int64 OperationId,
                          CageConceptInformation CageConcept,
                          CashierConceptOperationResult Result,
                          PrintMode Mode,
                          SqlTransaction SQLTransaction)
      : base(Mode, CageConcept.VoucherType, SQLTransaction)
    //    public VoucherSpecials(String Title,
    //                    Currency Amount,
    //                    String Concept,
    //                        PrintMode Mode,
    //                        SqlTransaction SQLTransaction)
    //: base(Mode, SQLTransaction)
    {

      String _voucher_concept;
      String _voucher_currency_type;
      String _national_currency;
      String _currency_type;

      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load general voucher data.
      //  3. Load voucher data.
      //  4. Save voucher 

      // 1. Load HTML structure.
      LoadVoucher("VoucherSpecials");

      // 2. Load general voucher data.
      VoucherSequenceId = CageConcept.SequenceId;
      SetValue("CV_M01_DEV", Result.Amount);
      if (CageConcept.CashierAction == CageMeters.CageSourceTargetCashierAction.CashierInput
          && !string.IsNullOrEmpty(GeneralParam.Value("Cashier.Voucher", "Concept.In.Footer")))
      {
        LoadVoucherGeneralData(false);
        SetParameterValue("@VOUCHER_FOOTER", GeneralParam.Value("Cashier.Voucher", "Concept.In.Footer"));
      }
      else if (CageConcept.CashierAction == CageMeters.CageSourceTargetCashierAction.CashierOutput
          && !string.IsNullOrEmpty(GeneralParam.Value("Cashier.Voucher", "Concept.Out.Footer")))
      {
        LoadVoucherGeneralData(false);
        SetParameterValue("@VOUCHER_FOOTER", GeneralParam.Value("Cashier.Voucher", "Concept.Out.Footer"));
      }
      else
      {
        LoadVoucherGeneralData();
      }

      //  3. Load voucher data.
      if (CageConcept.CashierAction == CageMeters.CageSourceTargetCashierAction.CashierInput)
      {
        _voucher_concept = Resource.String("STR_VOUCHER_CONCEPTS_IN");
      }
      else
      {
        _voucher_concept = Resource.String("STR_VOUCHER_CONCEPTS_OUT");
      }
      if (!FeatureChips.IsChipsType(Result.CurrencyType))
      {
        _voucher_currency_type = Result.IsoCode;
      }
      else
      {
        _voucher_currency_type = Resource.String("STR_VOUCHER_OTHER_OPERATIONS_IN_CHIPS");
      }

      _currency_type = FeatureChips.GetChipTypeDescription(Result.CurrencyType, Result.IsoCode);
      AddString("STR_VOUCHER_CONCEPTS_IN_CURRENCY_TYPE", _currency_type);

      AddString("STR_VOUCHER_TITLE", CageConcept.Name);

      _national_currency = CurrencyExchange.GetNationalCurrency();
      if (_voucher_currency_type == _national_currency)
      {
        AddString("VOUCHER_AMOUNT", ((Currency)Result.Amount).ToString());
      }
      else
      {
        AddString("VOUCHER_AMOUNT", Currency.Format(Result.Amount, _voucher_currency_type));
      }

      AddString("STR_VOUCHER_CONCEPTS_IN", _voucher_concept);

      if (!string.IsNullOrEmpty(Result.Comment))
      {
        SetParameterValue("@VOUCHER_COMMENT", "<tr><td>" + Result.Comment + "</td></tr><tr><td>&nbsp;</td></tr>");
      }
      else
      {
        SetParameterValue("@VOUCHER_COMMENT", "");
      }

      //  4. Save voucher 
      this.Save(OperationId, SQLTransaction);

    } // VoucherSpecials

    #endregion
  }
}
