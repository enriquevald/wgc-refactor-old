﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherDrawCashDesk.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR    First release. Product Backlog Item 14808: Refactoring of vouchers
// 22-SEP-2016  LTC    Product Backlog Item 17461: Gamingtables 46 - Cash desk draw for gamblign tables: Chips purchase
// 04-OCT-2016  LTC    PBI 17448: Televisa - Cashdeskdraw (Cash desk) - Voucher
// 11-OCT-2016  EOR    Fixed Bug 18922: Televisa - Draw Cash (Cash prize) - Not show concept of cupón promocional in the ticket
// 12-OCT-2016  EOR    Fixed Bug 18922: Televisa - Draw Cash (Cash prize) - Not show concept of cupón promocional in the ticket
// 13-OCT-2016  EOR    Fixed Bug 19051: Draw box with cash prize (Televisa) is performed as loser draw with idle question of access
// 24-OCT-2016  JMV    Fixed Bug 19548:Ticket de cortesía - Formato incorrecto
// 28-OCT-2016  EOR    Fixed Bug 19193: Draw Cash: The voucher cash draw not show the symbol currency € alignment
// 15-NOV-2016  EOR    Fixed Bug 19193: Draw Cash: The voucher cash draw double line number draw
// 18-NOV-2016  ETP    Fixed Bug 20736: Cashdeskdraw. Total label only in cashprize and Televisa mode
// 12-JAN-2016  RAB    Bug 21120: CashDraw (Cash prize): Error in voucher
// 09-MAR-2017  FGB    PBI 25268: Third TAX - Payment Voucher
// 21-MAR-2017  FAV    PBI 25735:Third TAX - Movement reports
// 28-DEC-2017  JML    Bug 31168:WIGOS-7352 [Ticket #11361] Sorteo de caja con un monto de apuesta configurable
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace WSI.Common
{
  // 20. Draw Cashier Ticket
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  // 
  // Draw number:
  // Prize:
  //          Voucher Title
  //
  // Numbers
  //
  //         Voucher Footer
  public class VoucherDrawCashDesk : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherDrawCashDesk(String[] VoucherAccountInfo,
                               Result CashierDrawResult,
                               Int64 OperationId,
                               Boolean ComplementaryVoucher,
                               OperationCode OperationCode,
                               PrintMode Mode,
                               CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION CashDeskId, //LTC 22-SEP-2016
                               SqlTransaction SQLTransaction)
      : base(Mode, OperationCode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCashierDraw");

      // DHA 17-DEC-20104: added sequence to cash desk draw voucher
      VoucherSequenceId = SequenceId.VouchersSplitA;

      // Set parameters voucher
      if (!ComplementaryVoucher)
      {
        SetValue("CV_TYPE", (Int32)CashierVoucherType.CashDeskDraw_Winner);
      }
      else
      {
        SetValue("CV_TYPE", (Int32)CashierVoucherType.CashDeskDraw_Loser);
      }

      // 2. Load voucher resources.
      LoadVoucherResources(VoucherAccountInfo, ComplementaryVoucher, OperationCode);

      if (Mode == PrintMode.Print)
      {
        VoucherHTML = VoucherHTML.Replace("@VOUCHER_DRAW_INFO", AddCashDeskInfo(CashierDrawResult, ComplementaryVoucher, CashDeskId)); // LTC 22-SEP-2016
      }
      else
      {
        String _html_string;
        _html_string = String.Empty;

        _html_string += @"</BR>";
        _html_string += @"<div style=""font: bold 14px arial; text-align:center"">";
        _html_string += Resource.String("STR_VOUCHER_CASHIER_DRAW_WILL_BE_RUN");
        _html_string += @"</BR></BR>";
        _html_string += @"<hr noshade size=""4"">";

        VoucherHTML = VoucherHTML.Replace("@VOUCHER_DRAW_INFO", _html_string);
      }

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    // LTC 22-SEP-2016
    public VoucherDrawCashDesk(Result CashDeskDrawResult,
                               Boolean ComplementaryVoucher,
                               CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION CashDeskId)
    {
      VoucherHTML = AddCashDeskInfo(CashDeskDrawResult, ComplementaryVoucher, CashDeskId);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    /// 
    private void LoadVoucherResources(String[] VoucherAccountInfo, Boolean ComplementaryVoucher, OperationCode OperationCode)
    {
      String _value = "";
      // LTC 22-SEP-2016
      Int16 _CashDrawId = Convert.ToInt16(CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_00);

      if (OperationCode == Common.OperationCode.CHIPS_SALE_WITH_RECHARGE)
      {
        _CashDrawId = Convert.ToInt16(CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_01);
      }

      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Values

      // - Title
      if (ComplementaryVoucher)
      {
        _value = CashDeskDrawBusinessLogic.GetCashDeskDrawVoucherComplementaryTitle(_CashDrawId);//
      }
      else
      {
        _value = CashDeskDrawBusinessLogic.GetCashDeskDrawVoucherTitle(_CashDrawId);//
      }

      AddString("STR_VOUCHER_CAHIER_DRAW_TITLE", _value);

      // - General
      _value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", _value);

      _value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", _value);

      _value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", _value);

      _value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", _value);

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      _value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", _value);
    }

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Generates String Account Info and Numbers - Voucher
    // 
    //  PARAMS:
    //      - INPUT:
    //          - VoucherAccountInfo
    //          - DrawNumbersPlayer
    //          - DrawNumbersSystem
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true or false
    // 
    //   NOTES:
    public String AddCashDeskInfo(Result CashDeskDrawResult,
                                  Boolean ComplementaryVoucher,
                                  CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION CashDeskId)
    {
      String _html_info;
      Currency _amount;
      Int32 _factor;

      _html_info = String.Empty;
      _factor = 1;

      // LTC 28-SEP-2016
      if (ComplementaryVoucher)
      {
        _html_info += @"<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%""><td>";

        // DHA 12-FEB-2014: Added general param to configure loser prize label
        _amount = CashDeskDrawResult.m_is_winner ? 0m : CashDeskDrawResult.m_nr_awarded + CashDeskDrawResult.m_re_awarded;
        _html_info += "<td align='left' width='50%'>" + Misc.ReadGeneralParams("CashDesk.Draw" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(Convert.ToInt16(CashDeskId)), "Voucher.LoserPrizeLabel") + "</td>";
        _html_info += "<td align='right' width='50%'>" + _amount.ToString("N2") + "</td>"; ; // LTC 22-SEP-2016

        SetValue("CV_M01_BASE", _amount);
        _html_info += @"</table><hr noshade size=""4""></tr></td>";
      }
      else
      {
        _html_info += @"<div style=""font: normal 14px arial"">";
        _html_info += @"<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""100%"" style=""font: normal 14px arial"">";

        //EOR 28-OCT-2016
        _html_info += @"<tr>";
        _html_info += @"<td align=""left""  width=""65%"">";
        _html_info += Resource.String("STR_VOUCHER_CASHIER_DRAW_NUMBER") + ": " + CashDeskDrawResult.m_draw_id;
        _html_info += @"</td>";
        _html_info += @"<td align=""right"" width=""35%"">";
        _html_info += @"</td>";
        _html_info += @"</tr>";


        if (CashDeskDrawResult.m_participation_amount > 0)
        {
          _amount = CashDeskDrawResult.m_participation_amount;
          _html_info += AddHtmlRowLine(Resource.String("STR_VOUCHER_CASHIER_DRAW_BET") + ": ", _amount.ToString(false));
        }

        // EOR 12-OCT-2016
        _amount = CashDeskDrawResult.m_is_winner ? CashDeskDrawResult.m_nr_awarded + CashDeskDrawResult.m_re_awarded : 0m;

        if (CashDeskDrawResult.m_prize_category.m_prize_type == PrizeType.Redeemable && CashDeskDrawBusinessLogic.GetPrizeMode() == CashDeskDrawBusinessLogic.PrizeMode.CashPrize && CashDeskDrawResult.m_is_winner)
        {
          _amount = CashDeskDrawResult.m_re_gross;
        }

        // DHA 12-FEB-2014: Added general param to configure winner prize label
        if (CashDeskDrawResult.m_prize_category.m_prize_type == PrizeType.UNR || CashDeskDrawResult.m_prize_category.m_prize_type == PrizeType.ReInKind)
        {
          _html_info += AddHtmlRowLine(Misc.ReadGeneralParams("CashDesk.Draw" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(Convert.ToInt16(CashDeskId)), "Voucher.WinnerPrizeLabel") + ": ", _amount.ToString(false) + " " + Misc.ReadGeneralParams("CashDesk.Draw" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(Convert.ToInt16(CashDeskId)), "Voucher.UNRLabel")); // LTC 22-SEP-2016
        }
        else
        {
          _html_info += AddHtmlRowLine(Misc.ReadGeneralParams("CashDesk.Draw" + CashDeskDrawBusinessLogic.GetCashDeskPostfij(Convert.ToInt16(CashDeskId)), "Voucher.WinnerPrizeLabel") + ": ", _amount.ToString()); // LTC 22-SEP-2016
        }

        SetValue("CV_M01_BASE", _amount);

        // DHA 29-MAY-2015 added to set the taxes to negative value
        if (GeneralParam.GetBoolean("Cashier", "Tax.OnPrizeInKind.SetNegative", false))
        {
          _factor = -1;
        }

        // LTC 04-OCT-2016
        if (CashDeskDrawResult.CashPrizeMode.Tax1.TaxEnabled)
        {
          _html_info += AddHtmlRowLine(CashDeskDrawResult.CashPrizeMode.Tax1.TaxName + ": ", ((Currency)(CashDeskDrawResult.CashPrizeMode.Tax1.TaxAmount * _factor)).ToString(true));
        }

        if (CashDeskDrawResult.CashPrizeMode.Tax2.TaxEnabled)
        {
          _html_info += AddHtmlRowLine(CashDeskDrawResult.CashPrizeMode.Tax2.TaxName + ": ", ((Currency)(CashDeskDrawResult.CashPrizeMode.Tax2.TaxAmount * _factor)).ToString(true));
        }

        if (CashDeskDrawResult.CashPrizeMode.Tax3.TaxEnabled)
        {
          _html_info += AddHtmlRowLine(CashDeskDrawResult.CashPrizeMode.Tax3.TaxName + ": ", ((Currency)(CashDeskDrawResult.CashPrizeMode.Tax3.TaxAmount * _factor)).ToString(true));
        }

        if (CashDeskDrawResult.CashPrizeMode.ServiceCharge != 0)
        {
          _html_info += AddHtmlRowLine(Misc.ReadGeneralParams("Cashier.ServiceCharge", "Text") + ": ", ((Currency)(CashDeskDrawResult.CashPrizeMode.ServiceCharge * _factor)).ToString(true));
        }

        //UNR
        if (CashDeskDrawResult.m_prize_category.m_prize_type == PrizeType.UNR)
        {
          if (CashDeskDrawResult.m_unr_tax1 != 0)
          {
            _html_info += AddHtmlRowLine(Misc.ReadGeneralParams("Cashier", "Tax.OnPrizeInKind.1.Name") + ": ", (CashDeskDrawResult.m_unr_tax1 * _factor).ToString());
          }

          if (CashDeskDrawResult.m_unr_tax2 != 0)
          {
            _html_info += AddHtmlRowLine(Misc.ReadGeneralParams("Cashier", "Tax.OnPrizeInKind.2.Name") + ": ", (CashDeskDrawResult.m_unr_tax2 * _factor).ToString());
          }

          if (CashDeskDrawResult.m_unr_tax3 != 0)
          {
            _html_info += AddHtmlRowLine(Misc.ReadGeneralParams("Cashier", "Tax.OnPrizeInKind.3.Name") + ": ", (CashDeskDrawResult.m_unr_tax3 * _factor).ToString());
          }
        }

        //InKind
        if (CashDeskDrawResult.m_prize_category.m_prize_type == PrizeType.ReInKind)
        {
          if (CashDeskDrawResult.m_ure_tax1 != 0)
          {
            _html_info += AddHtmlRowLine(Misc.ReadGeneralParams("Cashier", "Tax.OnPrizeInKind.1.Name") + ": ", (CashDeskDrawResult.m_ure_tax1 * _factor).ToString());
          }

          if (CashDeskDrawResult.m_ure_tax2 != 0)
          {
            _html_info += AddHtmlRowLine(Misc.ReadGeneralParams("Cashier", "Tax.OnPrizeInKind.2.Name") + ": ", (CashDeskDrawResult.m_ure_tax2 * _factor).ToString());
          }

          if (CashDeskDrawResult.m_ure_tax3 != 0)
          {
            _html_info += AddHtmlRowLine(Misc.ReadGeneralParams("Cashier", "Tax.OnPrizeInKind.3.Name") + ": ", (CashDeskDrawResult.m_ure_tax3 * _factor).ToString());
          }
        }

        _html_info += @"</table>";

        if (Misc.IsVoucherModeTV() && CashDeskDrawResult.IsCashPrize)
        {
          _amount = 0;

          if (CashDeskDrawResult.m_is_winner)
          {
            _amount = CashDeskDrawResult.CashPrizeMode.NetPrize;

            _html_info += @"<div style=""font: bold 14px arial; text-align:center;"">";
            _html_info += @"<hr noshade size=""4"">";
            _html_info += @"" + Resource.String("STR_FRM_AMOUNT_TOTAL").ToUpper() + "";
            _html_info += @"</BR>";
            _html_info += @"" + ((Currency)_amount).ToString();
            _html_info += @"</div>";
          }
        }

        _html_info += @"<hr noshade size=""4"">";
        _html_info += AddStringNumbers(CashDeskDrawResult.m_bet_numbers, CashDeskDrawResult.m_winning_combination);
        _html_info += "</div>";
      }

      return _html_info;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Generates String Numbers - Voucher
    // 
    //  PARAMS:
    //      - INPUT:
    //          - DrawNumbersPlayer
    //          - DrawNumbersSystem
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true or false
    // 
    //   NOTES:
    public string AddStringNumbers(List<Int32> DrawNumbers,
                                   List<Int32> DrawNumbersResult)
    {
      string _html_numbers;

      _html_numbers = String.Empty;
      _html_numbers += @"<style type=""text/css"">";
      _html_numbers += ".numbers {          font: bold 17px/20px Helvetica, Verdana, Tahoma;";
      _html_numbers += "word-wrap: break-word;";
      _html_numbers += "text-align:center;         }";
      _html_numbers += "</style>";

      _html_numbers += @" <div class=""numbers"">";

      for (int _idx = 0; _idx <= DrawNumbers.Count - 1; _idx++)
      {
        if (_idx > 0)
        {
          _html_numbers += @"&nbsp ";
        }

        _html_numbers += DrawNumbers[_idx].ToString("00");
      }

      _html_numbers += @"</div>";

      if (DrawNumbersResult.Count > 0)
      {
        _html_numbers += Resource.String("STR_VOUCHER_CASHIER_DRAW_RESULTS");
      }

      _html_numbers += @" <div class=""numbers"">";

      for (int _idx = 0; _idx <= DrawNumbersResult.Count - 1; _idx++)
      {
        if (_idx > 0)
        {
          _html_numbers += @"&nbsp ";
        }

        _html_numbers += DrawNumbersResult[_idx].ToString("00");
      }

      _html_numbers += @"</div>";

      _html_numbers += @"<hr noshade size=""4"">";

      return _html_numbers;
    }

    // LTC 28-SEP-2016
    public string AddHtmlRowLine(String Concept, String Value)
    {
      String _html_info;

      _html_info = String.Empty;

      _html_info += @"<tr>";
      _html_info += @"  <td align=""left""  width=""50%"">";
      _html_info += Concept;
      _html_info += @"  </td>";
      _html_info += @"  <td align=""right""  width=""50%"">";
      _html_info += Value;
      _html_info += @"  </td>";
      _html_info += @"</tr>";

      return _html_info;
    }

    #endregion
  }
}
