﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherMBCardChange.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Data.SqlClient;

namespace WSI.Common
{
  // 6. MBCard Change Limit
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  // Card Id: 
  // Account Id:
  //
  // Initial Balance: $
  // Limit Change: $
  //
  //  Final Balance:  $
  //
  //         Voucher Footer
  /// <summary>
  /// Add Credit Voucher
  /// </summary>
  public class VoucherMBCardChange : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor
    public VoucherMBCardChange(PrintMode Mode, MBCardData MbCardData, SqlTransaction SQLTransaction)
      : base(Mode, CashierVoucherType.MBSalesLimitChange, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.
      //  4. Load mobile bank voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherMBCardChangeLimit");

      // 2. Load voucher resources.
      LoadVoucherResources();

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      // 4. Load mobile bank voucher data.
      LoadResourcesForMBCardData(MbCardData, SQLTransaction);
    }
    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources()
    {
      String value;

      //Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      //NLS Values

      //Title
      value = Resource.String("STR_VOUCHER_MBCARD_CHANGE_TITLE");
      AddString("STR_VOUCHER_MBCARD_CHANGE_TITLE", value);

      //General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

      // Voucher Specific - Initial amount
      value = Resource.String("STR_VOUCHER_MBCARD_CHANGE_INITIAL") + ":";
      AddString("STR_VOUCHER_MBCARD_CHANGE_INITIAL", value);

      // Voucher Specific - Final amount
      value = Resource.String("STR_VOUCHER_MBCARD_CHANGE_FINAL") + ":";
      AddString("STR_VOUCHER_MBCARD_CHANGE_FINAL", value);
    }

    /// <summary>
    /// Load resource for Mobile bank
    /// </summary>
    /// <param name="MbCardData"></param>
    /// <param name="SQLTransaction"></param>
    private void LoadResourcesForMBCardData(MBCardData MbCardData, SqlTransaction SQLTransaction)
    {
      MobileBankUserType _MB_user_type;
      String _value;
      UserLogin.UserInfo _user_info;

      if (MobileBankConfig.IsMobileBankLinkedToADevice)
        _MB_user_type = MobileBankUserType.UserDevice;
      else
        _MB_user_type = MobileBankUserType.MobileBankCard;

      _value = Resource.String("STR_VOUCHER_CARD_CARD_ID") + ":";      //Trackdata
      AddString("STR_VOUCHER_CARD_CARD_ID", _value);
      AddString("VOUCHER_CARD_CARD_ID", MobileBank.GetCardTrackDataForVoucher(MbCardData));

      if (_MB_user_type == MobileBankUserType.UserDevice)
      {
        _user_info = new UserLogin.UserInfo();

        if (UserLogin.GetUserIfExistInDb(MbCardData.UserId, _user_info, SQLTransaction))
        {
          _value = Resource.String("STR_SESSION_USER") + ":";     //User
          AddString("STR_VOUCHER_CARD_USERNAME", _value);
          AddString("VOUCHER_CARD_USERNAME", _user_info.user_name);

          _value = Resource.String("STR_FRM_CHANGE_PIN_HOLDER") + ":"; //Holder
          AddString("STR_VOUCHER_CARD_FULLNAME", _value);
          AddString("VOUCHER_CARD_FULLNAME", _user_info.full_name);
        }
        else
        {
          AddString("STR_VOUCHER_CARD_USERNAME", String.Empty);
          AddString("VOUCHER_CARD_USERNAME<br>", String.Empty);
          AddString("STR_VOUCHER_CARD_FULLNAME", String.Empty);
          AddString("VOUCHER_CARD_FULLNAME<br>", String.Empty);
        }

        AddString("STR_VOUCHER_CARD_MB_ID", String.Empty);        //Mobile Id
        AddString("VOUCHER_CARD_ACCOUNT_ID<br>", String.Empty);
        AddString("VOUCHER_MBCARD_HOLDER_NAME", String.Empty);    //Holder name (centered)
      }
      else
      {

        _value = Resource.String("STR_VOUCHER_CARD_MB_ID") + ":";  //Mobile Id
        AddString("STR_VOUCHER_CARD_MB_ID", _value);
        AddString("VOUCHER_CARD_ACCOUNT_ID", MbCardData.CardId.ToString());

        AddString("STR_VOUCHER_CARD_USERNAME", String.Empty);
        AddString("VOUCHER_CARD_USERNAME<br>", String.Empty);
        AddString("STR_VOUCHER_CARD_FULLNAME", String.Empty);
        AddString("VOUCHER_CARD_FULLNAME<br>", String.Empty);

        AddString("VOUCHER_MBCARD_HOLDER_NAME", MbCardData.HolderName);     //Holder name
      }
    }
    #endregion

    #region Public Methods
    #endregion
  } // VoucherMBCardChange
}
