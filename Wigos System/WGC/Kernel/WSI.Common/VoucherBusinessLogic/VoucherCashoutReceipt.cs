﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCashoutReceipt.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: MArk Stansfield
// 
// CREATION DATE: 04-MAY-2018
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 04-MAY-2018  MS         First release. 
//---------------------------------------------------------------------------------------------

using System;
using WSI.Common;
using System.Data.SqlClient;
using WSI.Common.AutoPrint.Entities;
using System.Globalization;

namespace WSI.Common
{
  public class VoucherCashOutReceipt : Voucher
  {
    #region Constructor

    public VoucherCashOutReceipt(eCashOutReceiptData CashOutReceipt
                               , Int64 OperationId
                               , PrintMode Mode
                               , SqlTransaction SQLTransaction)
      : base(Mode, OperationCode.AUTO_PRINT_VOUCHER_CASH_OUT, SQLTransaction)
    {

      LoadVoucher("Voucher.CashOutReceipt");

      //Labels

      AddString("LBL_VOUCHER_RECEIPT_NUMBER", Resource.String("STR_CASHOUT_VOUCHER_RECEIPT_NUMBER") + ":");
      AddString("LBL_VOUCHER_TRANSACTION_NUMBER", Resource.String("STR_CASHOUT_VOUCHER_TRANSACTION_NUMBER") + ":");
      AddString("LBL_VOUCHER_PROVIDER", Resource.String("STR_CASHOUT_VOUCHER_PROVIDER") + ":");
      AddString("LBL_VOUCHER_EGM", Resource.String("STR_CASHOUT_VOUCHER_EGM") + ":");
      AddString("LBL_VOUCHER_RECEIPT_VALUE", Resource.String("STR_CASHOUT_VOUCHER_RECEIPT_VALUE") + ":");
      AddString("LBL_VOUCHER_PLAYER_NAME", Resource.String("STR_CASHOUT_VOUCHER_PLAYER_NAME") + ":");
      AddString("LBL_VOUCHER_WORKSTATION", Resource.String("STR_CASHOUT_VOUCHER_WORKSTATION") + ":");
      AddString("LBL_VOUCHER_DATE", Resource.String("STR_CASHOUT_VOUCHER_DATE") + ":");
      AddString("LBL_VOUCHER_TIME", Resource.String("STR_CASHOUT_VOUCHER_TIME") + ":");

      //Values
     
      // Added cashier info label to get reprint literal if we do a reprint from wigos guis --> Cash desk voucher history
      AddString("VOUCHER_CASHIER_INFO", string.Empty);
      
      AddString("VOUCHER_HEADER", CashOutReceipt.Header); 
      AddString("VOUCHER_TITLE", CashOutReceipt.Title); 
      AddString("VOUCHER_DATE", CashOutReceipt.Date);
      AddString("VOUCHER_TIME", CashOutReceipt.Time); 
      AddString("VOUCHER_COMPANY_NAME", CashOutReceipt.CompanyName); 
      AddString("VOUCHER_SITE_NAME", CashOutReceipt.SiteName); 
      AddString("VOUCHER_CASHOUT_RECEIPT", CashOutReceipt.CashOutReceipt);
      AddString("VOUCHER_RECEIPT_NUMBER", CashOutReceipt.ReceiptNumber.ToString());
      AddString("VOUCHER_TRANSACTION_NUMBER", CashOutReceipt.TransactionNumber.ToString());
      AddString("VOUCHER_PROVIDER", CashOutReceipt.Provider);
      AddString("VOUCHER_EGM", CashOutReceipt.EGM.ToString());
      AddString("VOUCHER_RECEIPT_VALUE", CashOutReceipt.ReceiptValue.ToString(true)); 
      AddString("VOUCHER_PLAYER_NAME", CashOutReceipt.PlayerName);
      AddString("VOUCHER_WORKSTATION", CashOutReceipt.WorkStation);

      AddString("VOUCHER_FOOTER", CashOutReceipt.Footer);

    } // VoucherGetMarker

    #endregion
  }
}
