﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCardHolderUpdate.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
// 19-JAN-2017  FOS         Fixed Bug 22330: Print ticket 2 times, when modify the user data.
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  // 19. Card Holder Data
  // Format:
  //    Voucher Header
  //  
  //      Card Holder info
  //
  //   Voucher Footer
  public class VoucherCardHolderUpdate : Voucher
  {

    #region Constructor

    public VoucherCardHolderUpdate(CardData Account,
      //String[] VoucherCardHolderNewData,
                                  OperationCode OperationCode,
                                  PrintMode Mode,
                                  SqlTransaction SQLTransaction)
      : base(Mode,OperationCode, SQLTransaction)
    {
      String[] _voucher_card_holder_info;
      String[] _voucher_client_data;
      String _nls_string;

      _voucher_card_holder_info = Account.VoucherAccountInfo();

      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load general voucher data.
      //  3. Load specific voucher data.

      // 1. Load HTML structure.
      LoadHeader(GeneralParam.Value("Cashier.Voucher", "Header"));
      LoadVoucher("Voucher.AccountCustomize");
      LoadFooter(GeneralParam.Value("Cashier.Voucher", "AccountCustomize.Footer"), "");

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load specific voucher data.
      _nls_string = Resource.String("STR_VOUCHER_ACCOUNT_CUSTOMIZE_CLIENT_DATA_TITLE");
      AddString("VOUCHER_ACCOUNT_CUSTOMIZE_CLIENT_DATA_TITLE", _nls_string);

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", _voucher_card_holder_info);

      _voucher_client_data = GenerateVoucherDataInfo(Account);
      AddMultipleLineString("VOUCHER_ACCOUNT_CUSTOMIZE_NEW_DATA", _voucher_client_data);

    } // VoucherCardHolderUpdate

    #endregion

    #region Private Methods

    private String GetDocumentTypeString(ACCOUNT_HOLDER_ID_TYPE HolderIdType)
    {
      return IdentificationTypes.GetIdentificationName(HolderIdType); ;
    } // GetDocumentTypeString

    private String GetHolderFedEntity(Int64 FedEntityId)
    {
      DataTable _dt_fed_entities;
      String _fed_entity;

      _fed_entity = "";

      try
      {
        if (!States.GetStates(out _dt_fed_entities, Resource.CountryISOCode2))
        {
          return _fed_entity;
        }

        // Search the federal entity
        foreach (DataRow _dr in _dt_fed_entities.Rows)
        {
          if ((int)_dr.ItemArray[0] == FedEntityId)
          {
            _fed_entity = (String)_dr.ItemArray[1];
            break;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _fed_entity;
    } // GetHolderFedEntity

    private String GetConditionalString(String Prefix, String Text, String Suffix)
    {
      if (Text == "")
      {
        return "";
      }

      return Prefix + Text + Suffix;
    } // GetConditionalString

    private String[] GenerateVoucherDataInfo(CardData Account)
    {
      String[] _data_info;
      String _nls_string;
      String _fed_entity;
      const int _max_num_lines = 17;
      int _idx_line;

      _idx_line = 0;
      _data_info = new String[_max_num_lines];

      // Name
      if (GeneralParam.GetBoolean("Account.VisibleField", "Name3", true))
      {
        _nls_string = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NAME");
        _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderName3);
      }

      // Name 4
      if (GeneralParam.GetBoolean("Account.VisibleField", "Name4", true))
      {
        _nls_string = Resource.String("STR_FRM_PLAYER_EDIT_NAME4");
        _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderName4);
      }

      // Name 1
      if (GeneralParam.GetBoolean("Account.VisibleField", "Name1", true))
      {
        _nls_string = Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME1");
        _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderName1);
      }

      // Name 2
      if (GeneralParam.GetBoolean("Account.VisibleField", "Name2", true))
      {
        _nls_string = Resource.String("STR_FRM_PLAYER_EDIT_LAST_NAME2");
        _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderName2);
      }

      // HolderBirthDate
      if (GeneralParam.GetBoolean("Account.VisibleField", "BirthDate", true))
      {
        if (!DateTime.MinValue.Equals(Account.PlayerTracking.HolderBirthDate) && !String.IsNullOrEmpty(Account.PlayerTracking.HolderBirthDateText))
        {
          _nls_string = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_BIRTH_DATE");
          _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderBirthDateText);
        }
      }

      // HolderBirthCountry
      if (GeneralParam.GetBoolean("Account.VisibleField", "BirthCountry", true))
      {
        _nls_string = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_BIRTH_COUNTRY");
        _data_info[_idx_line++] = DataInfo(_nls_string, CardData.CountriesString((Int32)Account.PlayerTracking.HolderBirthCountry));
      }

      // HolderNationality
      if (GeneralParam.GetBoolean("Account.VisibleField", "Nationality", true))
      {
        _nls_string = Resource.String("STR_FRM_ACCOUNT_USER_EDIT_NATIONALITY");
        _data_info[_idx_line++] = DataInfo(_nls_string, CardData.NationalitiesString((Int32)Account.PlayerTracking.HolderNationality));
      }

      // HolderId1 (R.F.C.)
      if (IdentificationTypes.IsIdentificationEnabled(ACCOUNT_HOLDER_ID_TYPE.RFC))
      {
        _nls_string = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC);
        _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderId1);
      }

      // HolderId1 (C.U.R.P.)
      if (IdentificationTypes.IsIdentificationEnabled(ACCOUNT_HOLDER_ID_TYPE.CURP))
      {
        _nls_string = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP);
        _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderId2);
      }

      if (Account.PlayerTracking.HolderIdType != ACCOUNT_HOLDER_ID_TYPE.RFC && Account.PlayerTracking.HolderIdType != ACCOUNT_HOLDER_ID_TYPE.CURP)
      {
        // Document type
        if (GeneralParam.GetBoolean("Account.VisibleField", "Document", true))
        {
          _nls_string = Resource.String("STR_VOUCHER_ACCOUNT_CUSTOMIZE_NEW_DATA_DOCUMENT_TYPE");
          _data_info[_idx_line++] = DataInfo(_nls_string, GetDocumentTypeString(Account.PlayerTracking.HolderIdType));

          // Document id
          _nls_string = Resource.String("STR_VOUCHER_ACCOUNT_CUSTOMIZE_NEW_DATA_DOCUMENT_NUMBER");
          _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderId);
        }
      }

      // Address
      _fed_entity = GetHolderFedEntity(Account.PlayerTracking.HolderFedEntity);
      _nls_string = Resource.String("STR_VOUCHER_ACCOUNT_CUSTOMIZE_NEW_DATA_ADDRESS");
      String _address = "";
      Boolean _address_visible = false;
      if (GeneralParam.GetBoolean("Account.VisibleField", "Address", true))
      {
        _address = GetConditionalString("", Account.PlayerTracking.HolderAddress01, ""); // Street
        _address_visible = true;
      }
      if (GeneralParam.GetBoolean("Account.VisibleField", "ExtNum", true))
      {
        _address += GetConditionalString(_address_visible ? " " : "", Account.PlayerTracking.HolderExtNumber, ""); // Ext Number
        _address_visible = true;
      }
      if (GeneralParam.GetBoolean("Account.VisibleField", "City", true))
      {
        _address += GetConditionalString(_address_visible ? " " : "", Account.PlayerTracking.HolderCity, ""); // Municipio
        _address_visible = true;
      }
      if (GeneralParam.GetBoolean("Account.VisibleField", "Address02", true))
      {
        _address += GetConditionalString(_address_visible ? ", " : "", Account.PlayerTracking.HolderAddress02, ""); // Colonia
        _address_visible = true;
      }
      if (GeneralParam.GetBoolean("Account.VisibleField", "Address03", true))
      {
        _address += GetConditionalString(_address_visible ? ", " : "", Account.PlayerTracking.HolderAddress03, ""); // Delegacion
        _address_visible = true;
      }

      if (GeneralParam.GetBoolean("Account.VisibleField", "FedEntity", true))
      {
        _address += GetConditionalString(_address_visible ? ", " : "", _fed_entity, ""); // Fed entity
        _address_visible = true;
      }
      if (_address_visible)
      {
        _data_info[_idx_line++] = DataInfo(_nls_string, _address);
      }

      // Postal code
      if (GeneralParam.GetBoolean("Account.VisibleField", "Zip", true))
      {
        _nls_string = Resource.String("STR_VOUCHER_ACCOUNT_CUSTOMIZE_NEW_DATA_ZIP_CODE");
        _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderZip);
      }

      // Phone number
      if (GeneralParam.GetBoolean("Account.VisibleField", "Phone1", true))
      {
        _nls_string = Resource.String("STR_VOUCHER_ACCOUNT_CUSTOMIZE_NEW_DATA_PHONE_1");
        _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderPhone01);
      }

      if (GeneralParam.GetBoolean("Account.VisibleField", "Phone2", true))
      {
        if (Account.PlayerTracking.HolderPhone02 != "")
        {
          _nls_string = Resource.String("STR_VOUCHER_ACCOUNT_CUSTOMIZE_NEW_DATA_PHONE_2");
          _data_info[_idx_line++] = DataInfo(_nls_string, Account.PlayerTracking.HolderPhone02);
        }
      }

      // Occupation
      if (GeneralParam.GetBoolean("Account.VisibleField", "Occupation", true))
      {
        _nls_string = Resource.String("STR_VOUCHER_ACCOUNT_CUSTOMIZE_NEW_DATA_OCCUPATION");
        _data_info[_idx_line++] = DataInfo(_nls_string, CardData.OccupationsDescriptionString(Account.PlayerTracking.HolderOccupationId));
      }

      if (WSI.Common.Misc.HasBeneficiaryAndNotAMLExternalControl())
      {
        _nls_string = Resource.String("STR_VOUCHER_ACCOUNT_CUSTOMIZE_NEW_DATA_BENEFICIARY");
        if (Account.PlayerTracking.HolderHasBeneficiary)
        {
          _data_info[_idx_line++] = _nls_string + Account.PlayerTracking.BeneficiaryName1
                        + " " + Account.PlayerTracking.BeneficiaryName2
                        + " " + Account.PlayerTracking.BeneficiaryName3;
        }
      }

      return _data_info;

    } // GenerateVoucherDataInfo

    private String DataInfo(String Title, String Data)
    {
      return Title.TrimEnd(new Char[] { ':', ' ' }) + ": " + (String.IsNullOrEmpty(Data) ? "---" : Data);
    }
    #endregion

  }
}
