﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherGiftCancellation.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherGiftCancellation : Voucher
  {
    #region Constructor

    public VoucherGiftCancellation(String[] VoucherAccountInfo,
                                   Points InitialPoints,
                                   Points SpentPoints,
                                   GIFT_TYPE GiftType,
                                   String GiftNumber,
                                   String GiftName,
                                   Currency NotRedeemableCredits,
                                   Decimal GiftUnits,
                                   PrintMode Mode,
                                   SqlTransaction SQLTransaction)

      : base(Mode, SQLTransaction)
    {
      string[] _message_params = { "", "", "", "", "", "" };

      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.
      //  4. Load voucher parameters

      // 1. Load HTML structure.
      LoadVoucher("VoucherGiftCancellation");

      AddString("STR_VOUCHER_TITLE", Resource.String("STR_VOUCHER_GIFT_CANCELLATION_001"));

      AddString("STR_VOUCHER_GIFT_REQUEST_TRACKDATA", "");
      AddString("VOUCHER_GIFT_REQUEST_TRACKDATA", "");

      // 2. Load voucher resources.
      LoadVoucherResources();

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      //  4. Load voucher parameters
      //      - Account Info
      //      - Initial Points Balance
      //      - Spent Points
      //      - Final Points Balance
      //      - Gift Name
      //      - Voucher track data

      Points _final_points;

      //      - Account Info
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      //      - Initial Points Balance
      AddString("VOUCHER_GIFT_REQUEST_INITIAL_POINTS", InitialPoints.ToString("#,##0") + " " + Resource.String("STR_FRM_GIFT_REQUEST_008"));

      //      - Spent Points
      AddString("VOUCHER_GIFT_REQUEST_SPENT_POINTS", SpentPoints.ToString("#,##0") + " " + Resource.String("STR_FRM_GIFT_REQUEST_008"));

      //      - Final Points Balance
      _final_points = InitialPoints + SpentPoints;
      AddString("VOUCHER_GIFT_REQUEST_FINAL_POINTS", _final_points.ToString("#,##0") + " " + Resource.String("STR_FRM_GIFT_REQUEST_008"));

      //      - Gift Name
      switch (GiftType)
      {
        case GIFT_TYPE.OBJECT:
          AddString("VOUCHER_GIFT_REQUEST_GIFT_NAME", GiftName);
          break;

        case GIFT_TYPE.NOT_REDEEMABLE_CREDIT:
          // @p0 créditos no redimibles
          _message_params[0] = NotRedeemableCredits.ToString();
          AddString("VOUCHER_GIFT_REQUEST_GIFT_NAME", Resource.String(("STR_VOUCHER_GIFT_REQUEST_003"), _message_params));
          break;

        case GIFT_TYPE.REDEEMABLE_CREDIT:
          // @p0 créditos redimibles
          _message_params[0] = NotRedeemableCredits.ToString();
          AddString("VOUCHER_GIFT_REQUEST_GIFT_NAME", Resource.String(("STR_VOUCHER_GIFT_REQUEST_011"), _message_params));
          break;

        case GIFT_TYPE.DRAW_NUMBERS:
          AddMultipleLineString("VOUCHER_GIFT_REQUEST_GIFT_NAME", GiftName + "\n" + GiftUnits.ToString() + " " + Resource.String("STR_VOUCHER_GIFT_REQUEST_010"));
          break;

        case GIFT_TYPE.SERVICES:
          AddString("VOUCHER_GIFT_REQUEST_GIFT_NAME", GiftName);
          break;

        default:
          AddString("VOUCHER_GIFT_REQUEST_GIFT_NAME", "");
          break;
      }

      //      - Voucher track data
      AddString("VOUCHER_GIFT_REQUEST_TRACKDATA", GiftNumber);

    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources()
    {
      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Strings

      // Header
      AddString("STR_VOUCHER_SITE_ID", Resource.String("STR_VOUCHER_SITE_ID") + ":");
      AddString("STR_VOUCHER_TERMINAL_USERNAME", Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":");
      AddString("STR_VOUCHER_AUTHORIZED_BY", Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":");
      AddString("STR_VOUCHER_TERMINAL_ID", Resource.String("STR_VOUCHER_TERMINAL_ID") + ":");
      AddString("STR_VOUCHER_VOUCHER_ID", Resource.String("STR_VOUCHER_VOUCHER_ID") + ":");

      // Title
      AddString("STR_VOUCHER_TITLE", Resource.String("STR_VOUCHER_GIFT_REQUEST_001"));

      // Details
      AddString("STR_VOUCHER_GIFT_REQUEST_INITIAL_POINTS", Resource.String("STR_VOUCHER_GIFT_REQUEST_004") + ":");
      AddString("STR_VOUCHER_GIFT_REQUEST_SPENT_POINTS", Resource.String("STR_VOUCHER_GIFT_REQUEST_005") + ":");
      AddString("STR_VOUCHER_GIFT_REQUEST_FINAL_POINTS", Resource.String("STR_VOUCHER_GIFT_REQUEST_006") + ":");
      AddString("STR_VOUCHER_FINAL_TITLE", Resource.String("STR_VOUCHER_GIFT_REQUEST_007"));
      AddString("STR_VOUCHER_GIFT_REQUEST_TRACKDATA", Resource.String("STR_VOUCHER_GIFT_REQUEST_002"));
      AddString("STR_VOUCHER_GIFT_REQUEST_EXPIRATION", Resource.String("STR_VOUCHER_GIFT_REQUEST_008") + ":");
    }

    #endregion

    #region Public Methods
    #endregion
  }
}
