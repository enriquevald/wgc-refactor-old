﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherJunket.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: MArk Stansfield
// 
// CREATION DATE: 03-May-2017
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 03-May-2017  MS         First release. 
//---------------------------------------------------------------------------------------------

using System;
using WSI.Common;
using System.Data.SqlClient;
using WSI.Common.Junkets;

namespace WSI.Common
{ 
  public class VoucherJunket : Voucher
  {
        #region Constructor
    /// <summary>
    /// VoucherJunket
    /// </summary>
    /// <param name="VoucherAccountInfo"></param>
    /// <param name="FlyerTitle"></param>
    /// <param name="FlyerText"></param>
    /// <param name="FlyerCode"></param>
    /// <param name="Mode"></param>
    /// <param name="SQLTransaction"></param>
    public VoucherJunket(String[] VoucherAccountInfo
                       , String FlyerTitle
                       , String FlyerText
                       , String FlyerCode
                       , Int64 OperationId
                       , PrintMode Mode
                       , SqlTransaction SQLTransaction)
      : base(Mode, OperationCode.JUNKET, SQLTransaction)
    {
      LoadVoucher("Voucher.Junket");

      LoadVoucherGeneralData();

      // 3. Load specific voucher data.
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      //Labels
      AddString("LBL_VOUCHER_FLYER_CODE", Resource.String("STR_APP_JUNKET_FLYER_TITLE") + ":");

      //Values
      AddString("VOUCHER_FLYER_TITLE", FlyerTitle);
      AddString("VOUCHER_FLYER_TEXT", FlyerText);
      AddString("VOUCHER_FLYER_CODE", FlyerCode);

    } // VoucherGetMarker

    #endregion
  }
}
