﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherGamingHallCashDeskStatus.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherGamingHallCashDeskStatus : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherGamingHallCashDeskStatus(PrintMode Mode, SqlTransaction SQLTransaction,
      WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherGamingHallCashDeskStatus");

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load voucher resources and data
      LoadVoucherResources(CashierSessionStats);

    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
    {
      String _value = "";
      TYPE_SPLITS _split_data;
      //VoucherCurrencyCashSummary.ParametersCurrencyCashSummary _parameters_currency_cash_summary;
      Boolean _show_refill_cashier;
      Boolean _show_refill_cage;
      Boolean _show_collect_cashier;
      Boolean _show_collect_cage;

      if (!Split.ReadSplitParameters(out _split_data))
      {
        return;
      }

      // CASH SUMMARY
      /*_parameters_currency_cash_summary = new VoucherCurrencyCashSummary.ParametersCurrencyCashSummary();
      _parameters_currency_cash_summary.HideChipsAndCurrenciesConcepts = ParametersOverview.HideChipsAndCurrenciesConcepts;
      _parameters_currency_cash_summary.IsFilterMovements = ParametersOverview.IsFilterMovements;
      _parameters_currency_cash_summary.CurrencyIsoCode = ParametersOverview.CurrencyIsoCode;
      _value = new VoucherCurrencyCashSummary(CashierSessionStats, _parameters_currency_cash_summary).VoucherHTML;
      this.VoucherHTML = this.VoucherHTML.Replace("@STR_VOUCHER_CASH_DESK_CASH_SUMMARY", _value);*/


      // NLS Values

      // - Title
      _value = Resource.String("STR_VOUCHER_CASH_DESK_STATUS_TITLE");
      AddString("STR_VOUCHER_CASH_DESK_STATUS_TITLE", _value);

      // - General
      _value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", _value);

      _value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", _value);

      _value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", _value);

      _value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", _value);

      _value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", _value);

      _value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_DEPOSIT");
      AddString("STR_VOUCHER_DEPOSITS", _value);
      AddCurrency("VOUCHER_DEPOSITS", CashierSessionStats.deposits);

      _value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_WITHDRAW");
      AddString("STR_VOUCHER_WITHDRAW", _value);
      AddCurrency("VOUCHER_WITHDRAW", CashierSessionStats.withdraws);

      _value = Resource.String("STR_UC_BANK_HANDPAYS");
      AddString("STR_VOUCHER_HANDPAYS_TITLE", _value);
      AddCurrency("VOUCHER_TOTAL_HANDPAYS", CashierSessionStats.handpay_payment - CashierSessionStats.handpay_canceled);

      _show_refill_cashier = GeneralParam.GetBoolean("GamingHall", "Refill.ToCashier.Enabled") || CashierSessionStats.refill_amount_to_cashier != 0;
      _show_refill_cage = !GeneralParam.GetBoolean("GamingHall", "Refill.ToCashier.Enabled") || CashierSessionStats.refill_amount_to_cage != 0;
      _show_collect_cashier = GeneralParam.GetBoolean("GamingHall", "Collection.ToCashier.Enabled") || CashierSessionStats.collect_amount_to_cashier != 0;
      _show_collect_cage = !GeneralParam.GetBoolean("GamingHall", "Collection.ToCashier.Enabled") || CashierSessionStats.collect_amount_to_cage != 0;

      AddCurrency("VOUCHER_TOTAL_REFILL_TO_CASHIER", CashierSessionStats.refill_amount_to_cashier);
      AddString("STR_VOUCHER_TOTAL_REFILL_TO_CASHIER_HIDDEN", _show_refill_cashier ? "" : "hidden");

      AddCurrency("VOUCHER_TOTAL_REFILL_TO_CAGE", CashierSessionStats.refill_amount_to_cage);
      AddString("STR_VOUCHER_TOTAL_REFILL_TO_CAGE_HIDDEN", _show_refill_cage ? "" : "hidden");

      AddCurrency("VOUCHER_TOTAL_COLLECT_TO_CASHIER", CashierSessionStats.collect_amount_to_cashier);
      AddString("STR_VOUCHER_TOTAL_COLLECT_TO_CASHIER_HIDDEN", _show_collect_cashier ? "" : "hidden");

      AddCurrency("VOUCHER_TOTAL_COLLECT_TO_CAGE", CashierSessionStats.collect_amount_to_cage);
      AddString("STR_VOUCHER_TOTAL_COLLECT_TO_CAGE_HIDDEN", _show_collect_cage ? "" : "hidden");

      _value = Resource.String("STR_VOUCHER_CONCEPTS_IN");
      AddString("STR_VOUCHER_CONCEPTS_IN", _value);
      AddCurrency("VOUCHER_CONCEPTS_IN", CashierSessionStats.concepts_input);

      _value = Resource.String("STR_VOUCHER_CONCEPTS_OUT");
      AddString("STR_VOUCHER_CONCEPTS_OUT", _value);
      AddCurrency("VOUCHER_CONCEPTS_OUT", CashierSessionStats.concepts_output);

      _value = Resource.String("STR_FRM_CASH_DESK_NET_BALANCE");
      AddString("STR_VOUCHER_NATIONAL_BALANCE", _value);

      AddCurrency("VOUCHER_GAMING_HALL_BALANCE", CashierSessionStats.final_balance);
      _value = Resource.String("STR_VOUCHER_TOTAL_REFILL");
      AddString("STR_VOUCHER_TOTAL_REFILL", _value);
      _value = Resource.String("STR_VOUCHER_TOTAL_COLLECT");
      AddString("STR_VOUCHER_TOTAL_COLLECT", _value);




    } // LoadVoucherResources

    #endregion


  }
}
