﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherTerminalDraw.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Mark Stansfield
// 
// CREATION DATE: 13-NOV-2017
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 13-NOV-2017  MS          First release. PBI 30550: [WIGOS-5561] Codere EGM Draw
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;


namespace WSI.Common
{
  public class VoucherTerminalDraw : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherTerminalDraw(ArrayList VoucherList
                               , String[] AccountInfo
                               , Int64 OperationId
                               , String _CashierName 
                               , String _Terminal 
                               , String _Client
                               , String NumbersPlayed
                               , String NumbersDrawn
                               , Currency AmountBet
                               , Currency AmountWon
                               , PrintMode Mode
                               , RechargeOutputParameters OutputParams
                               , String _client_card
                               , long AccountID
                               , Boolean IsWinner
                               , Boolean LoserVoucher
                               , SqlTransaction SQLtransaction)
      : base(Mode, SQLtransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      String _cashier_name;
      String _cashier_terminal;
      String _operation_id;
      String _receipt_no;
      String _client;
      String _account;
      String _card_number;
      String _headert;
      String _footert;
      Boolean _mode;

      VoucherBusinessLogic.GetVoucherInformation(SQLtransaction, CashierVoucherType.NotSet, out _headert, out _footert, out _mode);

      // 1. Load HTML structure.



      if (!LoserVoucher)
      {
        LoadVoucher("VoucherTerminalDraw");

        AddString("VOUCHER_LOGO", "");

        //Build Header Info
        AddString("VOUCHER_HEADER", _headert);

        //Date
        AddDateTime("VOUCHER_DATE_TIME", OutputParams.CashDeskDrawResult.m_date_draw);

        //Build Cashier Info
        _cashier_name = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ": " + _CashierName;
        _cashier_terminal = Resource.String("STR_VOUCHER_TERMINAL_ID") + ": " + _Terminal;
        _operation_id = Resource.String("STR_OPERATION_ID") + ": " + OperationId.ToString();
        _receipt_no = Resource.String("STR_VOUCHER_VOUCHER_ID") + ": " + this.VoucherId.ToString();

        String[] _cashier = new String[] { _cashier_name, _cashier_terminal, _operation_id, _receipt_no };

        _client_card = "************" + _client_card.Remove(0, 12);

        if (string.IsNullOrEmpty(_Client))
        {
          _Client = Resource.String("STR_UC_CARD_CARD_HOLDER_ANONYMOUS");
        }

        //Build Account Info String
        _client = Resource.String("STR_VOUCHER_CARD_DRAW_HOLDER_NAME") + ": " + _Client;
        _account = Resource.String("STR_VOUCHER_CARD_ACCOUNT_ID") + ": " + AccountID.ToString();
        _card_number = Resource.String("STR_VOUCHER_CARD_CARD_ID") + ": " + _client_card;

        String[] _accountInfo = new String[] { _client, _account, _card_number };

        //Replace Html on Voucher

        AddMultipleLineString("VOUCHER_HEADER", "");
        //AddString("VOUCHER_DATE_TIME", _date_time);
        AddMultipleLineString("VOUCHER_CASHIER_INFO", _cashier);

        AddString("VOUCHER_TITLE", Resource.String("STR_VOUCHER_CARD_TERMINAL_DRAW_TITLE"));
        AddMultipleLineString("VOUCHER_ACCOUNT_INFO", _accountInfo);

        AddString("LBL_DRAW_NUMBER", Resource.String("STR_VOUCHER_CASHIER_DRAW_NUMBER"));
        AddString("DRAW_NUMBER", OutputParams.CashDeskDrawResult.m_draw_id.ToString());
        AddString("LBL_BET", Resource.String("STR_CASHIER_GAMING_TABLE_BET_GROUP_BOX"));
        AddCurrency("DRAW_BET", AmountBet);
        AddString("LBL_PRIZE", Resource.String("STR_FRM_LAST_MOVEMENTS_003"));

        if (!IsWinner)
        {
          AmountWon = 0;
        }

        AddCurrency("DRAW_PRIZE", AmountWon);


        AddString("VOUCHER_NUMBERS", NumbersPlayed);
        AddString("LBL_VOUCHER_NUMBERS", Resource.String("STR_VOUCHER_CASHIER_DRAW_RESULTS"));
        AddString("VOUCHER_DRAWN_NUMBERS", NumbersDrawn);


        AddString("VOUCHER_FOOTER", _footert);

      }
      else
      {
        LoadVoucher("VoucherTerminalDrawLoser");

        //Generate the Loser voucher
        AddString("VOUCHER_LOGO", "");

        //Build Header Info
        AddString("VOUCHER_HEADER", _headert);

        //Date
        AddDateTime("VOUCHER_DATE_TIME", OutputParams.CashDeskDrawResult.m_date_draw);

        //Build Cashier Info
        _cashier_name = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ": " + _CashierName;
        _cashier_terminal = Resource.String("STR_VOUCHER_TERMINAL_ID") + ": " + _Terminal;
        _operation_id = Resource.String("STR_OPERATION_ID") + ": " + OperationId.ToString();
        _receipt_no = Resource.String("STR_VOUCHER_VOUCHER_ID") + ": " + this.VoucherId.ToString();

        String[] _cashier = new String[] { _cashier_name, _cashier_terminal, _operation_id, _receipt_no };

        _client_card = "************" + _client_card.Remove(0, 12);

        if (string.IsNullOrEmpty(_Client))
        {
          _Client = Resource.String("STR_UC_CARD_CARD_HOLDER_ANONYMOUS");
        }

        //Build Account Info String
        _client = Resource.String("STR_VOUCHER_CARD_DRAW_HOLDER_NAME") + ": " + _Client;
        _account = Resource.String("STR_VOUCHER_CARD_ACCOUNT_ID") + ": " + AccountID.ToString();
        _card_number = Resource.String("STR_VOUCHER_CARD_CARD_ID") + ": " + _client_card;

        String[] _accountInfo = new String[] { _client, _account, _card_number };

        //Replace Html on Voucher

        AddMultipleLineString("VOUCHER_HEADER", "");

        AddMultipleLineString("VOUCHER_CASHIER_INFO", _cashier);

        AddString("VOUCHER_TITLE", GeneralParam.GetString("CashDesk.Draw.02", "LoserPrizeVoucherTitle"));

        AddMultipleLineString("VOUCHER_ACCOUNT_INFO", _accountInfo);

        AddString("LBL_LOSER", GeneralParam.GetString("CashDesk.Draw.02", "Voucher.LoserPrizeLabel"));
        AddCurrency("DRAW_LOSER", AmountWon + AmountBet);

        AddString("VOUCHER_FOOTER", _footert);

        //Remove HTML
        //AddString("LBL_DRAW_NUMBER", "");
        //AddString("DRAW_NUMBER", "");
        //AddString("LBL_BET", "");
        //AddString("DRAW_BET", "");
        //AddString("LBL_PRIZE", "");
        //AddString("DRAW_PRIZE", "");
        //AddString("VOUCHER_NUMBERS", "");
        //AddString("LBL_VOUCHER_NUMBERS", "");
        //AddString("VOUCHER_DRAWN_NUMBERS", "");

      }


    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(Boolean ShowDeposit, Boolean Promotion)
    {
      String value = "";

      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Values

      // - Title
      value = Resource.String("STR_VOUCHER_CARD_DRAW_TITLE");
      AddString("STR_VOUCHER_CARD_DRAW_TITLE", value);

      // - General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

      value = Resource.String("STR_VOUCHER_CARD_DRAW_TICKET_NUMBER") + ":";
      AddString("STR_VOUCHER_CARD_DRAW_TICKET_NUMBER", value);

    } //LoadVoucherResources

    #endregion

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Generates String Numbers - Voucher
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Parameter
    //          - FirstDrawNum
    //          - LastDrawNum
    //          - MaxNumber
    //          - HasBingoFormat
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - true or false
    // 
    //   NOTES:
    public Boolean AddStringNumbers(String Parameter,
                                     Int64 FirstDrawNum,
                                     Int64 LastDrawNum,
                                     Int64 MaxNumber,
                                     Boolean HasBingoFormat)
    {
      Int64 _draw_num_mod;
      Int64 _idx_draw_num;
      String _table_draw_num;
      String _str_draw_num;
      Int32 _max_number_digits;
      String _font_size;


      // MaxNumber is used to define the digits to format the numbers.
      _max_number_digits = MaxNumber.ToString().Length;

      if (_max_number_digits >= 6)
      {
        _font_size = "100";
      }
      else
      {
        _font_size = "150";
      }

      if (HasBingoFormat)
      {
        _table_draw_num = "<td>";
      }
      else
      {
        _table_draw_num = "<td style=\"font-size:150%\">";
      }

      _table_draw_num += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" STYLE=\"font-size:" + _font_size + "%\" >";

      if (!HasBingoFormat)
      {
        _table_draw_num += "<tr>";
      }

      for (_idx_draw_num = FirstDrawNum; _idx_draw_num <= LastDrawNum; _idx_draw_num++)
      {
        _draw_num_mod = (_idx_draw_num - FirstDrawNum) % 2;

        if (HasBingoFormat)
        {
          _str_draw_num = DrawBingoFormat.NumberToBingo(_idx_draw_num, MaxNumber);
          _table_draw_num += "<tr><td align=\"center\">" + _str_draw_num + "</td></tr>";
          if (_idx_draw_num < LastDrawNum)
          {
            _table_draw_num += "<tr><td><table><tr><td>&nbsp;</td></tr></table></td></tr>";
          }
        }
        else
        {
          _str_draw_num = _idx_draw_num.ToString().PadLeft(_max_number_digits, '0');

          switch (_draw_num_mod)
          {
            case 0:
              _table_draw_num += "<td align=\"left\">" + _str_draw_num + "</td>";
              break;
            //case 1:
            //  _table_draw_num += "<td align=\"center\">" + _str_draw_num + "</td>";
            //  break;
            case 1:
              _table_draw_num += "<td align=\"right\">" + _str_draw_num + "</td>";
              if (_idx_draw_num < LastDrawNum)
              {
                _table_draw_num += "</tr><tr>";
              }
              break;
          }
        }
      }

      if (!HasBingoFormat)
      {
        _table_draw_num += "</tr>";
      }
      _table_draw_num += "</table></td>";

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      // Insert title into html voucher
      return SetParameterValue(Parameter, _table_draw_num);

    } //AddStringNumbers

    #endregion
  }
}
