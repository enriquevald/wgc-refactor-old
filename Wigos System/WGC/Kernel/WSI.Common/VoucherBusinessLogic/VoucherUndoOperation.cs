﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherUndoOperation.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Data.SqlClient;

namespace WSI.Common
{
  public class VoucherUndoOperation : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    /// <summary>
    /// Undo voucher operation
    /// </summary>
    /// <param name="VoucherHtml"></param>
    /// <param name="OperationCodeToUndo"></param>
    /// <param name="OperationCode"></param>
    /// <param name="Mode"></param>
    /// <param name="Trx"></param>
    public VoucherUndoOperation(String VoucherHtml
                              , OperationCode OperationCodeToUndo
                              , OperationCode OperationCode
                              , PrintMode Mode
                              , SqlTransaction Trx)
      : base(Mode, OperationCode, Trx)
    {
      String _undo_header;
      String _undone_text;
      String _undo_title;

      // Read parameters configuration
      if (OperationCode == OperationCode.HANDPAY)
      {
        this.m_voucher_params = OperationVoucherParams.OperationVoucherDictionary.GetParameters(OperationCode.HANDPAY_CANCELLATION);
        this.NumPendingCopies = this.m_voucher_params.PrintCopy;
      }

      if (VoucherHtml.Contains(Voucher.VOUCHER_DIV_UNDO_OPERATION))
      {
        //Get voucher undo title
        _undo_title = GetUndoVoucherTitle(OperationCodeToUndo);

        _undo_header = "<div align=\"right\">@VOUCHER_DATE_TIME</div>\n";
        _undo_header += "@VOUCHER_CASHIER_INFO\n";
        _undo_header += "<div align=\"center\"><br/><b>" + _undo_title + "</b><br/><br/></div>\n";

        VoucherHtml = VoucherHtml.Replace(Voucher.VOUCHER_DIV_UNDO_OPERATION, _undo_header);
      }

      if (VoucherHtml.Contains(Voucher.VOUCHER_DIV_REPRINT))
      {
        //Replace the hidden div for the text of undone
        _undone_text = Voucher.VOUCHER_DIV_REPRINT;
        _undone_text += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n";
        _undone_text += "<tr><td align=\"center\"><br/><b>" + GeneralParam.GetString("Cashier.Voucher", "OperationUndo.UndoneText") + "</b><br/><br/></td></tr>\n";
        _undone_text += "</table>";

        VoucherHtml = VoucherHtml.Replace(Voucher.VOUCHER_DIV_REPRINT, _undone_text);
      }

      this.VoucherHTML = VoucherHtml;

      // Load cashier info and datetime
      LoadVoucherGeneralData();
    }

    /// <summary>
    /// Get the undo voucher title
    /// </summary>
    /// <param name="OperationCodeToUndo"></param>
    /// <returns></returns>
    private String GetUndoVoucherTitle(OperationCode OperationCodeToUndo)
    {
      String _undo_title;

      //Reopening a closed session?
      if (OperationCodeToUndo == OperationCode.REOPEN_OPERATION)
      {
        _undo_title = Resource.String("STR_REOPEN_SESSION_VOUCHER_TITLE");
      }
      else
      {
        _undo_title = GeneralParam.GetString("Cashier.Voucher", "OperationUndo.Title");
      }

      return _undo_title;
    }

    #endregion
  }
}
