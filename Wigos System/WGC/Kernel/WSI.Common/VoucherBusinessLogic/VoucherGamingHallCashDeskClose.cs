﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherGamingHallCashDeskClose.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherGamingHallCashDeskClose : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherGamingHallCashDeskClose(PrintMode Mode, SqlTransaction SQLTransaction,
      WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherGamingHallCashDeskClose");

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load voucher resources and data
      LoadVoucherResources(CashierSessionStats);

    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
    {
      String _value = "";
      TYPE_SPLITS _split_data;
      //VoucherCurrencyCashSummary.ParametersCurrencyCashSummary _parameters_currency_cash_summary;
      Boolean _show_refill_cashier;
      Boolean _show_refill_cage;
      Boolean _show_collect_cashier;
      Boolean _show_collect_cage;
      CashierSessionInfo _session_info;
      CurrencyIsoType _iso_type;

      _iso_type = new CurrencyIsoType();
      _iso_type.IsoCode = CurrencyExchange.GetNationalCurrency();
      _iso_type.Type = CurrencyExchangeType.CURRENCY;

      if (CashierSessionStats.collected.ContainsKey(_iso_type))
      {
        AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLLECTED", CashierSessionStats.collected[_iso_type]);
      }
      else
      {
        AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLLECTED", 0);
      }

      if (!Split.ReadSplitParameters(out _split_data))
      {
        return;
      }

      // CASH SUMMARY
      /*_parameters_currency_cash_summary = new VoucherCurrencyCashSummary.ParametersCurrencyCashSummary();
      _parameters_currency_cash_summary.HideChipsAndCurrenciesConcepts = ParametersOverview.HideChipsAndCurrenciesConcepts;
      _parameters_currency_cash_summary.IsFilterMovements = ParametersOverview.IsFilterMovements;
      _parameters_currency_cash_summary.CurrencyIsoCode = ParametersOverview.CurrencyIsoCode;
      _value = new VoucherCurrencyCashSummary(CashierSessionStats, _parameters_currency_cash_summary).VoucherHTML;
      this.VoucherHTML = this.VoucherHTML.Replace("@STR_VOUCHER_CASH_DESK_CASH_SUMMARY", _value);*/

      // NLS Values

      // - Title
      _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TITLE");
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_TITLE", _value);

      // - General
      _value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", _value);

      _value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", _value);

      _value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", _value);

      _value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", _value);

      _value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", _value);

      _value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_DEPOSIT");
      AddString("STR_VOUCHER_DEPOSITS", _value);
      AddCurrency("VOUCHER_DEPOSITS", CashierSessionStats.deposits);

      _value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_WITHDRAW");
      AddString("STR_VOUCHER_WITHDRAW", _value);
      AddCurrency("VOUCHER_WITHDRAW", CashierSessionStats.withdraws);

      _value = Resource.String("STR_UC_BANK_HANDPAYS");
      AddString("STR_VOUCHER_HANDPAYS_TITLE", _value);
      AddCurrency("VOUCHER_TOTAL_HANDPAYS", CashierSessionStats.handpay_payment - CashierSessionStats.handpay_canceled);

      _show_refill_cashier = GeneralParam.GetBoolean("GamingHall", "Refill.ToCashier.Enabled") || CashierSessionStats.refill_amount_to_cashier != 0;
      _show_refill_cage = !GeneralParam.GetBoolean("GamingHall", "Refill.ToCashier.Enabled") || CashierSessionStats.refill_amount_to_cage != 0;
      _show_collect_cashier = GeneralParam.GetBoolean("GamingHall", "Collection.ToCashier.Enabled") || CashierSessionStats.collect_amount_to_cashier != 0;
      _show_collect_cage = !GeneralParam.GetBoolean("GamingHall", "Collection.ToCashier.Enabled") || CashierSessionStats.collect_amount_to_cage != 0;

      AddCurrency("VOUCHER_TOTAL_REFILL_TO_CASHIER", CashierSessionStats.refill_amount_to_cashier);
      AddString("STR_VOUCHER_TOTAL_REFILL_TO_CASHIER_HIDDEN", _show_refill_cashier ? "" : "hidden");

      AddCurrency("VOUCHER_TOTAL_REFILL_TO_CAGE", CashierSessionStats.refill_amount_to_cage);
      AddString("STR_VOUCHER_TOTAL_REFILL_TO_CAGE_HIDDEN", _show_refill_cage ? "" : "hidden");

      AddCurrency("VOUCHER_TOTAL_COLLECT_TO_CASHIER", CashierSessionStats.collect_amount_to_cashier);
      AddString("STR_VOUCHER_TOTAL_COLLECT_TO_CASHIER_HIDDEN", _show_collect_cashier ? "" : "hidden");

      AddCurrency("VOUCHER_TOTAL_COLLECT_TO_CAGE", CashierSessionStats.collect_amount_to_cage);
      AddString("STR_VOUCHER_TOTAL_COLLECT_TO_CAGE_HIDDEN", _show_collect_cage ? "" : "hidden");

      _value = Resource.String("STR_VOUCHER_CONCEPTS_IN");
      AddString("STR_VOUCHER_CONCEPTS_IN", _value);
      AddCurrency("VOUCHER_CONCEPTS_IN", CashierSessionStats.concepts_input);

      _value = Resource.String("STR_VOUCHER_CONCEPTS_OUT");
      AddString("STR_VOUCHER_CONCEPTS_OUT", _value);
      AddCurrency("VOUCHER_CONCEPTS_OUT", CashierSessionStats.concepts_output);

      _value = Resource.String("STR_FRM_CASH_DESK_NET_BALANCE");
      AddString("STR_VOUCHER_NATIONAL_BALANCE", _value);

      AddCurrency("VOUCHER_GAMING_HALL_BALANCE", CashierSessionStats.final_balance);
      _value = Resource.String("STR_VOUCHER_TOTAL_REFILL");
      AddString("STR_VOUCHER_TOTAL_REFILL", _value);
      _value = Resource.String("STR_VOUCHER_TOTAL_COLLECT");
      AddString("STR_VOUCHER_TOTAL_COLLECT", _value);

      _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED", _value);

      _session_info = CommonCashierInformation.CashierSessionInfo();

      if (_session_info.UserType == GU_USER_TYPE.USER && !Cage.IsGamingTable(_session_info.CashierSessionId))
      {
        Decimal _collected_diff;

        if (CashierSessionStats.collected_diff.ContainsKey(_iso_type))
        {
          _collected_diff = CashierSessionStats.collected_diff[_iso_type];
          if (_collected_diff < 0)
          {
            _collected_diff = -_collected_diff;
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
            AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");

            _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISSING") + ":";
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", _value);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", (Currency)_collected_diff);
          }
          else
          {
            _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA") + ":";
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", _value);
            AddMultipleLineString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", ((Currency)_collected_diff).ToString());

            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");
            AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");
          }
        }
        else
        {
          _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISSING") + ":";
          AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", _value);
          AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", CashierSessionStats.final_balance);

          AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
          AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
        }
      }
      else
      {
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");
        AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");

        AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
        AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
      }


    } // LoadVoucherResources

    #endregion

    #region Public Methods

    public void SetupCashStatusVoucher(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
    {
      Currency signed_amount;
      String _str_value;
      Currency _cs_mb_diff;
      Boolean _is_tito_mode;
      Boolean _gp_pending_cash_partial;

      _is_tito_mode = Utils.IsTitoMode();
      _gp_pending_cash_partial = MobileBank.GP_GetRegisterShortfallOnDeposit();

      //  1. Depositos
      //  2. Retiros
      //  3. Entradas
      //  4. Salidas
      //     ---------------------------
      //  5. Saldo (Efectivo en Caja)
      // 
      //     Detalle Salidas
      //  6. Premios
      //  7. Impuestos
      //  8. Premios pagados
      // 
      //     ---------------------------

      //AddString("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_OPEN_NUM", CashierSessionStats.total_initial_movs.ToString());
      //AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_OPEN_AMT", CashierSessionStats.initial_balance);

      //  1. Depositos
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT_AMT", CashierSessionStats.deposits);

      //  2. Retiros
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW_AMT", CashierSessionStats.withdraws);

      //  3. Cash In
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN_AMT", CashierSessionStats.total_cash_in);

      //  4. Cash Out
      signed_amount = CashierSessionStats.total_cash_out; // *-1;
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_OUT_AMT", signed_amount);

      //  Diferencia Bancos Móviles
      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF_MISSING") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF", _str_value);
      _cs_mb_diff = CashierSessionStats.mb_total_balance_lost + CashierSessionStats.mb_pending;
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF", _cs_mb_diff);

      //  Sobrante Bancos Móviles
      if (_gp_pending_cash_partial)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_OVER_SHORTFALL_CASH_DESK_STATUS") + ":";
      }
      else
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_OVER") + ":";
      }

      AddString("STR_VOUCHER_CASH_OVER", _str_value);
      AddCurrency("VOUCHER_CASH_OVER", CashierSessionStats.total_cash_over);

      //  Chech Payments
      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS", _str_value);
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS_AMT", CashierSessionStats.payment_orders);

      //Credit Card Exchange
      if (CashierSessionStats.total_bank_card > 0)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD") + ":";
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", _str_value);
        AddCurrency("VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", CashierSessionStats.total_bank_card);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", "");
        AddString("VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", "");
      }

    }

    #endregion

  }
}
