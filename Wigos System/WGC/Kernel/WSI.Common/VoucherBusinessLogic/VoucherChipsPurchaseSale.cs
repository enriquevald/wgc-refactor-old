﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherChipsPurchaseSale.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
// 07-JUL-2017  DHA         PBI 28423:WIGOS-2731 Rounding - Voucher
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace WSI.Common
{
  public class VoucherChipsPurchaseSale : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherChipsPurchaseSale(String[] VoucherAccountInfo
                                  , TYPE_SPLITS Splits
                                  , FeatureChips.ChipsOperation ChipsOperation
                                  , PrintMode Mode
                                  , SqlTransaction Trx)
      : base(Mode, Trx)
    {
      // 1. Load HTML structure.
      LoadVoucher("VoucherChipsPurchaseSale");

      // 2. Load NLS strings from resource
      LoadVoucherResources(VoucherAccountInfo, Splits, ChipsOperation);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(String[] VoucherAccountInfo
                                    , TYPE_SPLITS Splits
                                    , FeatureChips.ChipsOperation ChipsOperation)
    {
      String _str_value;
      StringBuilder _sb_chips_denomination_header;
      StringBuilder _sb_chips_denomination;
      String _hr_4;
      String _hr_2;
      String _empty_value;
      String _column_currency_type;
      String _column_description;
      CageCurrencyType _currency_type;
      CurrencyExchange _currency_exchange;

      _currency_exchange = new CurrencyExchange();
      _hr_2 = "";
      _hr_4 = "";
      _empty_value = "---";

      _str_value = "";
      _column_currency_type = "";
      _column_description = "";
      _sb_chips_denomination_header = new StringBuilder();
      _sb_chips_denomination = new StringBuilder();

      // Load Footer
      LoadFooter(Splits.company_a.footer, Splits.company_a.footer_cashin);

      // Load general voucher data.
      LoadVoucherGeneralData();
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      if (ChipsOperation.OperationType == FeatureChips.ChipsOperation.Operation.SALE)
      {
        SetValue("CV_TYPE", (Int32)CashierVoucherType.ChipsSale);

        if (!String.IsNullOrEmpty(ChipsOperation.SelectedGamingTable.TypeName) && !String.IsNullOrEmpty(ChipsOperation.SelectedGamingTable.TableName))
        {
          AddString("VOUCHER_GAMING_TABLE_NAME", ChipsOperation.SelectedGamingTable.TypeName + " - " + ChipsOperation.SelectedGamingTable.TableName);
        }
        else
        {
          AddString("VOUCHER_GAMING_TABLE_NAME", "");
        }

        _str_value = Resource.String("STR_VOUCHER_CASH_CHIPS_SALE_TITLE");
      }
      else
      {
        SetValue("CV_TYPE", (Int32)CashierVoucherType.ChipsBuy);

        AddString("VOUCHER_GAMING_TABLE_NAME", "");
        _str_value = Resource.String("STR_VOUCHER_CASH_CHIPS_PURCHASE_TITLE");
      }
      AddString("STR_VOUCHER_CASH_CHIPS_TITLE", _str_value);

      if (TITO.Utils.IsTitoMode() && ChipsOperation.OperationType == FeatureChips.ChipsOperation.Operation.SALE)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_CHIPS_INITIAL_AMOUNT");
        AddString("VOUCHER_CASH_CHIPS_INITIAL_AMOUNT", Currency.Format(ChipsOperation.FillChipsAmount, ChipsOperation.IsoCode));
      }
      else
      {
        _str_value = "";
        AddString("VOUCHER_CASH_CHIPS_INITIAL_AMOUNT", "");
      }
      AddString("STR_VOUCHER_CASH_CHIPS_INITIAL_AMOUNT", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_REDEEM_TOTAL");
      AddString("STR_VOUCHER_CASH_CHIPS_TOTAL", _str_value);

      AddString("VOUCHER_CASH_CHIPS_TOTAL", Currency.Format(ChipsOperation.ChipsAmount, ChipsOperation.IsoCode));

      if (TITO.Utils.IsTitoMode() && ChipsOperation.OperationType == FeatureChips.ChipsOperation.Operation.SALE)
      {
        if (ChipsOperation.ChipsDifference >= 0)
        {
          _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING");
          AddString("VOUCHER_CASH_CHIPS_REFUND", Currency.Format(ChipsOperation.ChipsDifference, ChipsOperation.IsoCode));
        }
        else
        {
          _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA");
          AddString("VOUCHER_CASH_CHIPS_REFUND", Currency.Format(-ChipsOperation.ChipsDifference, ChipsOperation.IsoCode));
        }

        AddString("STR_VOUCHER_CASH_CHIPS_REFUND", _str_value);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_CHIPS_REFUND", "");
        AddString("VOUCHER_CASH_CHIPS_REFUND", "");
      }

      if ((!GeneralParam.GetBoolean("GamingTables", "Cashier.Mode")
            && !ChipsOperation.IsChipsSaleRegister || ChipsOperation.OperationType == FeatureChips.ChipsOperation.Operation.PURCHASE)
            && ((Cage.ShowDenominationsMode)GeneralParam.GetInt32("Cage", "ShowDenominations", 1) != Cage.ShowDenominationsMode.HideAllDenominations))
      {
        // Header
        _currency_type = FeatureChips.ConvertToCageCurrencyType(ChipsOperation.CurrencyExchangeType);
        CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CURRENCY, ChipsOperation.IsoCode, out _currency_exchange);

        if (FeatureChips.IsCageCurrencyTypeChips(_currency_type))
        {
          _column_currency_type = FeatureChips.GetChipTypeDescription(FeatureChips.ConvertToCurrencyExchangeType(_currency_type), ChipsOperation.IsoCode);
        }
        else
        {
          _column_currency_type = _currency_exchange.Description;
        }

        _column_description = VoucherBuilder.GetColumnHeaderByType(_currency_type);

        foreach (KeyValuePair<FeatureChips.Chips.Chip, Int32> _denominations in ChipsOperation.ChipsItemsUnits)
        {
          if (_denominations.Value > 0)
          {
            _sb_chips_denomination.AppendLine(" <tr>  ");

            if (_denominations.Key.Denomination != 0)
            {
              // Chips with denomination
              _sb_chips_denomination.AppendLine("    <td align='right'  width='30%'>" + Currency.Format(_denominations.Key.Denomination, _denominations.Key.IsoCode) + "</td> ");
            }
            else
            {
              // Chips without denomination (Color)
              _sb_chips_denomination.AppendLine("    <td align='right'  width='30%'>" + _empty_value + "</td> ");
            }

            _sb_chips_denomination.AppendLine("    <td align='right' width='25%'>" + _denominations.Value + "</td> ");
            _sb_chips_denomination.AppendLine("    <td align='right' width='45%'>" + Currency.Format(_denominations.Key.Denomination * _denominations.Value, _denominations.Key.IsoCode) + "</td> ");
            _sb_chips_denomination.AppendLine(" </tr> ");
          }
        }

        _hr_2 = "<hr noshade size='2'>";
        _hr_4 = "<hr noshade size='4'>";
      }

      SetParameterValue("@STR_HR_SIZE_4", _hr_4);
      SetParameterValue("@STR_HR_SIZE_2", _hr_2);

      SetParameterValue("@VOUCHER_CURRENCY_TYPE", _column_currency_type);
      SetParameterValue("@VOUCHER_DESCRIPTION_CHIPS", _column_description);
      SetParameterValue("@VOUCHER_DENOMINATION_CHIPS", _sb_chips_denomination.ToString());

      if (!TITO.Utils.IsTitoMode() && GamingTableBusinessLogic.IsEnableRoundingSaleChips() && ChipsOperation.RoundingSaleChips != null &&
        ChipsOperation.OperationType == FeatureChips.ChipsOperation.Operation.SALE && ChipsOperation.DevolutionRoundingSaleChips > 0)
      {
          SetParameterValue("@STR_VOUCHER_ROUNDED_AMOUNT", Resource.String("STR_VOUCHER_CASH_CHIPS_ACCUMULATED_CONSUMED"));
          SetParameterValue("@VOUCHER_ROUNDED_AMOUNT", Currency.Format(ChipsOperation.DevolutionRoundingSaleChips, CurrencyExchange.GetNationalCurrency()));
      }
      SetParameterValue("@STR_VOUCHER_ROUNDED_AMOUNT", "");
      SetParameterValue("@VOUCHER_ROUNDED_AMOUNT", "");
    }

    #endregion
  }
}