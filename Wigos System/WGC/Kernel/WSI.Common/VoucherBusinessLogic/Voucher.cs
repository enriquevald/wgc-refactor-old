//------------------------------------------------------------------------------
// Copyright � 2007-2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : Voucher.cs
// 
//   DESCRIPTION : File than implement the following classes:
// 
//      VoucherBusinessLogic: Database logic for creating and saving vouchers
//      VoucherBuilder:       Builder class for vouchers, 
//                            based on operations that create more than one 
//                            voucher at time
//
//      1. Class: Voucher (Main voucher class)
//
//      Card Operations Vouchers: 
//      2. Class: VoucherCardAdd
//      3. Class: VoucherCardRedeem
//
//      Cash Desk Operation vouchers:
//      4. Class: VoucherCashDeskOpen
//      5. Class: VoucherCashDeskClose
// 
//      ...
// 
//      Handpay vouchers:
//      12. VoucherCardHandpay
//      13. VoucherCardHandpayCancellation
// 
//      Gift  vouchers:
//      14. VoucherGiftRequest
//      15. VoucherGiftDelivery
//
//      16. Generate PIN
//      17. Recharges Summary
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 22-AUG-2007 AJQ    First release.
// 23-JUL-2010 TJG    Handpay Voucher.
// 06-AUG-2010 TJG    Handpay Cancellation Voucher.
// 21-SEP-2010 MBF    Moved to WSI.Common.
// 26-OCT-2011 MPO    Name of mobile bank (For voucher mobile bank case)
// 22-NOV-2011 JMM    PIN Changed Voucher
// 23-NOV-2011 RCI    Optional sections in Handpay vouchers: signatures, game meters and play session.
// 02-DEC-2011 JMM    Adding denomination to Handpay vouchers.
// 21-DEC-2011 RCI    Added Bingo format flag.
// 23-DEC-2011 MPO    Added fields for the ticket Cash in, free text of prize coupon and information of recharge.
// 27-DEC-2011 MPO    (CashIn) Show promotion if different of 0, text for promotion.
// 04-JAN-2012 RCI    Optionally, Hide Totals in A and B tickets. Remove field for the ticket Cash in.
// 05-JAN-2012 JMM    Added a promotion losing voucher.
// 08-MAR-2012 MPO    When load a voucher set the hidden div after the cashier information.
// 09-MAR-2012 MPO    Set the initial balance in Voucher.A.ChasOut.
// 21-MAR-2012 MPO    Added "promo_and_coupon". If it shows text "promo_and_coupon" then it didn't shows promotion text and prize coupon text.
// 27-MAR-2012 JAB    Fixed bug #241, edit and remove incorrect information in "VoucherPromoLost" and "VoucherGeneratePIN" class.
// 06-JUN-2012 SSC    Added parameters control for null values in method VoucherPrintInit.
// 06-AUG-2012 MPO    Added POINTS_AWARDED or POINTS_CANCELED in voucher CashIn, CashOut and .Promo.Lost.
// 07-AUG-2012 JCM    Added Sign on MB Limit Extension and MB deposit. Renamed and redesign fields on Sign on MB Limit Extension and MB deposit.
// 26-SEP-2012 MPO    Voucher: VoucherGiftCancellation.
// 31-OCT-2012 JMM    Added a recharges summary voucher launched from PromoBOX.
// 05-DEC-2012 RCI    Support for multiple cancellation.
// 01-MAR-2013 ICS    Fixed Bug #629: Check that CurrencySymbol is not empty.
// 19-MAR-2013 RRB    Hide Prize Coupon in awarded promotion vouchers.
// 02-APR-2013 QMP & RCI   Hide Prize Coupon in ALL promotion vouchers (awarded and canceled).
// 19-APR-2013 ICS    Changed the machine name by its alias.
// 02-JUL-2013 DLL    New voucher for currency exchange.
// 26-AUG-2013 RMS    New voucher for currency exchange with bank card.
//                    Currency exchange voucher loads different html files ( by MovementType ).
//                    Title of currency exchange voucher changes by MovementType.
//                    Removed unnecessary currency activation status in vouchers CashDeskClose and CashDeskStatus.
// 27-AUG-2013 RMS    Removed unnecessary parameters in CashDeskClose functions ( SessionId and OperationId ).
// 28-AUG-2013 JAB    Added new camps in Customized's Voucher.
// 29-AUG-2013 JCOR   Added STR_VOUCHER_AUTHORIZED_BY.
// 30-AUG-2013 FBA    Added Recharge for points in CashDeskClose and CashDeskStatus. Fixed bug WIG-173.
// 02-SEP-2013 NMR    Return ID of ticket asociated to 'voucher'.
// 05-SEP-2013 DLL    Fixed Bug WIG-186: CloseCash & CashDeskClose show deposit and withdraws amount with all currencies.
// 09-SEP-2013 NMR    Adding TITO-Ticket print customization and preparing for data for Ticket print extension.
// 10-SEP-2013 DLL    Added Voucher for Check Recharge.
// 25-SEP-2013 DLL    Currency exchange values moved to VoucherCurrencyCash.
// 01-OCT-2013 DHA    Fixed Bug #WIG-246: CashIn Voucher with negative promotion due to Cash Desk Draw UNR.
// 04-OCT-2013 DDM & DHA    Fixed Bug #WIG-261: Error cancelling Cash Desk Draw promotions.
// 14-OCT-2013 DLL    UNR's Taxes.
// 15-OCT-2013 LEM    Added tickets info to VoucherCashDeskOverview if TITOMode is activated and IsCashier is True.
// 16-OCT-2013 DHA    Fixed Bug #WIG-282: Cash Desk Draw voucher, characters near right margin are not visible.
// 16-OCT-2013 DHA    Fixed Bug #WIG-283: Cash Desk Draw voucher, the promotion's footer now is under tax detail.
//                                        Promotion's summary voucher, when the promotion has footer and taxes, the taxes were wrong format.
// 17-OCT-2013 RMS    Added function to obtain a sequence id for split2 of an operation by operation id.
// 18-OCT-2013 DHA    Fixed Bug #WIG-286: PromoLost, when tax is 0 not shown on voucher.
// 25-OCT-2013 ACM    Fixed Bug #WIG-313: Ticket does't print nationality and birthplace.
// 29-OCT-2013 DHA    Fixed Bug #WIG-333: Hide prize cupon when Cash Desk Draw is enable.
// 30-OCT-2013 DLL    Fixed Bug #WIG-339: Show liabilities with '---' when no has data.
// 30-OCT-2013 ICS    Added undo operation vouchers suport.
// 18-NOV-2013 DHA    Fixed Bug #WIG-411: Wrong format in UNR promotions.
// 26-NOV-2013 LJM    Added Approval's signature if required.
// 28-NOV-2013 LJM    Fixed Bug #WIG-449: Error on total Withdraw.
// 30-NOV-2013 DLL    Added ticket chips sale.
// 09-DEC-2013 DLL    Fixed Bug WIG-467.
// 11-DEC-2013 JAB    Added ShowBalance functionality to printed vouchers.
// 18-DEC-2013 LJM    Added functionality to print Chips.
// 02-JAN-2014 DLL    Added CashInTax to VoucherCashDeskOverview and VoucherCashIn.
// 14-JAN-2014 RMS    Added VoucherCardCreditTransfer for TransferCredit operation.
// 15-JAN-2014 DLL    Added VoucherChipsPurchaseSale & VoucherChipsChange.
// 05-FEB-2014 DHA    Added Ticket redeem handpay/jackpot to VoucherCashDeskOverview.
// 12-FEB-2014 FBA    Added account holder id type control via CardData class IdentificationTypes. 
// 13-FEB-2014 DHA    Added ticket TITO information when generate the CashOut voucher.
// 12-MAR-2014 ICS    Fixed Bug #WIGOSTITO-1140: In Handpay Manual doesn't appears amount without deductions.
// 18-MAR-2014 JPJ    While in Multisite certain items change behaviour.
// 09-APR-2014 DLL    Fixed Bug WIG-801: It's necessary to show game profit when negative amount.
// 25-APR-2014 DHA    Added Tickets TITO detail.
// 30-APR-2014 DLL    Added VoucherChipsSaleDealerCopy and adapt VoucherChipsPurchaseSale for not show denominations when GP(ShowDenominations) is disabled.
// 05-JUN-2014 DLL    Fixed Bug WIG-1008: Javascript error when print Barcode. 
// 05-JUN-2014 DLL    Fixed Bug WIG-1009: Text cutted when print with Barcode.
// 06-JUN-2014 JAB    Added two decimal characters in denominations fields.
// 10-JUN-2014 DLL    Fixed Bug WIG-1024: Incorrect messagen when cash balance = 0 and game profit > 0
// 13-JUN-2014 LEM    Added CashInTax in Total for VoucherCashIn
// 03-JUL-2014 AMF    Added VoucherPointToCardReplacement
// 11-JUL-2014 JMM    Fixed Bug #WIGOSTITO-1244: Error on pay handpay when there is no playsession and GP to print played amount is active
// 14-JUL-2014 AMF    Regionalization
// 25-JUL-2014 DLL    Chips are moved to a separate table
// 31-JUL-2014 RMS    Fixed Bug WIG-1144: Corrected denominations ticket printing
// 07-AUG-2014 RMS & MPO & DRV Fixed Bug WIG-1166: incorrect currency title label on credit card and check.
// 08-AUG-2014 RMS    Added supoort for Specific Bank Card Types
// 16-SEP-2014 SMN    Added new concepts in voucher
// 16-SEP-2014 RMS    Cash Balance summarized or CashDeskOverview
// 16-SEP-2014 LMRS   Added VoucherSpecials
// 18-SEP-2014 RMS    Added Cash Advance concept in CashDeskOverview
// 02-OCT-2014 SGB    New footer voucher and overload voucher
// 07-OCT-2014 SGB    Setup footer voucher if specific or general & modify voucherCashDeskOpen to call
// 17-OCT-2014 MPO    Added GP "Cashier.Voucher", "Handpay.TotalInLetters"
// 18-NOV-2014 MPO    Added html special char (&nbsp;) for currency and integer string in voucher.
// 18-NOV-2014 LEM    Fixed Bug WIG-1694: Wrong amount "Check : In cash desk".
// 24-NOV-2014 MPO & OPC     LOGRAND CON02: Added IVA calculated (when needed) and input params class for cashin.
// 25-NOV-2014 OPC    Fixed Bug WIG-1750: fixed the error when select Cashier.Voucher.Mode 0 and 3.
// 11-DEC-2014 AMF    Fixed Bug WIG-1822: show Company B in MultiSite
// 19-DEC-2014 JBC    Fixed Bug WIG-1875: base amount on CashInVoucher
// 23-DEC-2014 JBC    Fixed Bug WIG-1893: Cancel voucher doesn't show card commission.
// 17-DEC-2014 DHA    Added Operation ID for all tickets
// 19-DEC-2014 DHA    Fixed Bug #WIG-1863: for cash desk draw tickets, set SplitA sequence for both tickets and asociated the new type for complementary voucher
// 05-JAN-2014 OPC    Added new comission Voucher for Cash Advance operations
// 08-JAN-2014 JBC    Fixed Bug WIG-1912: When IsCommissionToCompanyBEnabled, cash closing voucher shows Commission in B Voucher.
// 19-FEB-2014 YNM    Fixed Bug WIG-1894: Changes that allows to list the annulled tickets in "Cash Desk voucher report"
// 25-FEB-2014 YNM    Fixed Bug WIG-2102: Concepts voucher amounts was in IsoCode instead of Currency Symbol.
// 09-MAR-2015 DRV    Fixed Bug WIG-2143: Error on cash in voucher when integrated chips operations are enabled
// 17-MAR-2015 MPO    Fixed bug WIG-2154: It isn't necessary show the base amount when there aren't tax amount.
// 25-MAR-2015 YNM    Fixed Bug WIG-2176: Ticket Handpay: The concept deduction must be breakdown.
// 26-MAR-2015 YNM & MPO    Fixed Bug WIG-2176: Ticket Handpay: Decimal rounding for negative.
// 14-APR-2015 YNM    Fixed Bug TFS-1088: Ticket Handpay/Ticket CashOut: Tax breakdown on Tax Detail/Deductions. 
// 13-APR-2015 FJC    BackLog Item 710
// 17-APR-2015 YNM    Fixed Bug WIG-2222: Ticket Handpay error calculating Taxes.
// 20-APR-2015 YNM    Fixed Bug WIG-2226: Ticket CashOut cashless error on prize amount.  
// 23-APR-2015 YNM    Fixed Bug WIG-2240: Payment Order + Chips Buy take cashier taxes 
// 19-MAY-2015 RCI    Fixed Bug WIG-2367: Wrong GP names for cash deposit and withdrawal personalized footer.
// 28-MAY-2015 DHA    Added changes in cash desk draw and promo lost ticket for Winpot
// 27-MAY-2015 DCS    Added new multisite multicurrency filter and necessary changes to operate in multicurrency mode
// 23-JUN-2015 DHA    Added custom parameters for cashin chips operations
// 17-AUG-2015 DCS    Fixed Bug: WIG-2181 The barcode is not printed when it hides the currency symbol
// 03-AUG-2015 ETP    Fixed BUG 3496 Use of common header for card replacement.
// 05-AUG-2015 FAV    TFS 2797. Changes in VoucherSpecials to print a comment and footer about Concepts (using general params).
// 07-AUG-2015 ETP    Fixed BUG-3596 Corrected white space for expired redeem in visualization of cage summary.
// 24-AUG-2015 FAV    TFS-3257: Transfer between cashier sessions. 
// 29-AUG-2015 YNM    TFS-2796: BV-15: Print Difference Voucher in Cashier Closing.
// 27-AUG-2015 DHA    Show cash over/short only when is cashier session not integrated
// 28-AUG-2015 RCI & DDM   Bug-4027:  Added general param "GamingTables-IntegratedChipsSales.Voucher.Promotion.Text" in promotion concept.
// 01-SEP-2015 DHA    Added number of tickets to pay concept
// 22-SEP-2015 FAV    Fixed BUG-4593: Cahiers summary, error with collected value
// 05-OCT-2015 AMF    Fixed Bug TFS-4922: Postal Code, Federal entity depends country
// 06-OCT-2015 SDS    Fixed Bug 4594: Hist�rico de t�ckets de caja: algunas denominaciones no son visibles en el cierre
// 11-NOV-2015 FOS    Product Backlog Item: 3709 Payment tickets & handpays
// 12-NOV-2015 FAV    Product Backlog Item 4695: Gaming Hall, Collect and Refill vouchers
// 17-NOV-2015 FAV    Backlog Item 6629: Custom reimbursement voucher (Migration to MICO)
// 19-NOV-2015 ECP    Backlog Item 6463 Garcia River-09 Cash Report Resume & Closing Cash Voucher - Include Tickets Tito
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 17-NOV-2015 FAV    Backlog Item 6629: Custom reimbursement voucher (Migration to MICO)
// 26-NOV-2015 MPO    Fixed TFS Bug 7124: Card deposit for voucher mode 7
// 01-DEC-2015 FOS    Fixed Bug 7205: Protected taxWaiverMode and renamed function ApplyTaxCollection to EnableTitoTaxWaiver
// 02-DEC-2015 FOS    Fixed Bug 7208: Incorrect Tax Value on Handpays
// 17-NOV-2015 FAV    Backlog Item 6629: Custom reimbursement voucher (Migration to MICO)
// 26-NOV-2015 MPO    Fixed TFS Bug 7124: Card deposit for voucher mode 7
// 13-JAN-2015 JML    Product Backlog Item 6559: Cashier draw (RE in kind)
// 19-JAN-2016 DDS    Bug 8545:Mesas de Juego: No se puede cerrar mesas de juego con divisa/fichas
// 25-JAN-2016 YNM    Product Backlog Item 6656:Visitas / Recepci�n: Crear ticket de entrada
// 01-FEB-2016 AVZ    Product Backlog Item 8673:Visitas / Recepci�n: Configuraci�n de Modo de Recepci�n
// 09-FEB-2016 FAV    PBI 9148: Added changes in VoucherTitoTicketOperation to support a void of Machine Ticket
// 16-FEB-2016 DDS    PBI 9442: Palacio de los N�meros. Use 'Texto cup�n' field on Voucher
// 01-MAR-2016 DHA    Product Backlog Item 10083: Winpot - Provisioning Tax
// 01-MAR-2016 JML    Product Backlog Item 10084: Winpot - Provisioning Tax: Cashdesk draw
// 02-MAR-2016 DDS    Fixed Bug 10157: Missing info in 'Res�men de caja'
// 03-MAR-2016 JML    Product Backlog Item 10085: Winpot - Tax Provisions: Include movement reports
// 17-MAR-2016 EOR    Product Backlog Item 10235:TITA: Reporte de tickets
// 17-MAR-2016 ESE    Product Backlog Item 10229:TITA: Create swap ticket
// 14-ABR-2016 LTC    Product Backlog Item 11295: Codere - Custody Tax: Implement tax
// 19-APR-2016 RGR    Product Backlog Item 11297: Codere - Tax Custody: Reports and cash movements and account
// 21-APR-2016 LTC    Product Backlog Item 11296:Codere - Tax Custody: Redeem Tax Implementation
// 27-APR-2016 RGR    Product Backlog Item 11298: Codere - Tax Custody: Modify reports GUI Income and Return
// 10-MAY-2016 EOR    Fixed Bug 12997: Tax Custody: Result of Cashier wrong amount
// 22-MAR-2106 YNM    Bug 10759: Reception error paying with currency
// 24-MAR-2016 RAB    Bug 10745: Reception: with the GP requiereNewCard to "0" when assigning a card client is counted as "replacement"
// 01-MAR-2016 DHA    Product Backlog Item 9756: added chips types and setsProvisions: Include movement reports
// 04-APR-2016 DHA    Product Backlog Item 9756: added chips (RE, NR and Color)
// 13-APR-2016 DHA    Product Backlog Item 9950: added chips operation (sales and purchases)
// 14-APR-2016 FAV    Bug 11718: Changes in Cash Closing voucher to shows Cash Excess and Cash Shortfall for foreign exchanges.
// 15-APR-2016 JML    Product Backlog Item 10826: Tables (Phase 1): Cage Vouchers. 
// 19-APR-2016 RAB    Product Backlog Item 10826: Tables (Phase 1): Chips Vouchers. Operation account and box
// 21-APR-2016 FAV    Bug 11718: Added support to payments with card and cheque
// 03-MAY-2016 FAV    Product Backlog Item 12653:Cajero: Currency exchange for multiple denominations (Mallorca)
// 05-MAY-2016 RAB    PBI 12736: Review 23: Changes and improvements (Vouchers gaming-table)
// 10-MAY-2015 JBP    Product Backlog Item 12931:Visitas / Recepci�n: 2� Ticket LOPD
// 19-MAY-2016 FAV    Bug 13394: Cashier summary with information unaligned
// 03-JUN-2016 FAV    Fixed Bug 11718: Changes in Cash Closing voucher to shows Cash Excess and Cash Shortfall for foreign exchanges.
// 28-JUN-2016 ESE    Fixed Bug 13773: TITA voucher zero padleft
// 28-JUN-2016 RAB    PBI 14524: GamingTables (Phase 4): Management from deposit and withdrawals - Multicurrency in chips.
// 01-JUL-2016 ETP    Fixed Bug 15018:Asociaci�n de tarjeta: Vista previa incorrecta
// 06-JUL-2016 DHA    Product Backlog Item 15064: multicurrency chips sale/purchase
// 27-JUL-2016 EOR    Product Backlog Item 15248: TPV Televisa: Cashier, Vouchers and Tickets
// 24-AUG-2016 RAB    Product Backlog Item 16748: UNPLANNED Winions Sprint 29: Modify vouchers: changes of currency
// 17-AGO-2016 ESE    Product Backlog Item 15248:TPV Televisa: Hide voucher recharge
// 07-SEP-2016 EOR    Product Backlog Item 15335: TPV Televisa: Cash Summary
// 22-SEP-2016 LTC    Product Backlog Item 17461: Gamingtables 46 - Cash desk draw for gamblign tables: Chips purchase
// 19-SEP-2016 EOR    PBI 17467: Televisa - Service Charge: Voucher
// 26-SEP-2016 ETP    Fixed Bug 16504: Cask desk draw voucher is not printet when VoucherMode = 5
// 27-SEP-2016 LTC    Product Backlog Item 17461: Gamingtables 46 - Cash desk draw for gamblign tables: Chips purchase
// 28-SEP-2016 EOR    PBI 17448: Televisa - Sorteo de Caja (Premio en efectivo) - Voucher
// 29-SEP-2016 EOR    PBI 17448: Televisa - Sorteo de Caja (Premio en efectivo) - Voucher
// 30-SEP-2016 ETP    Bug 18003: Cashier: los vouchers de recarga no reflejan los cambios solicitados por Televisa
// 04-OCT-2016 RGR    PBI 17447: Televisa - Draw Cash (Cash prize) - Recharge Operation
// 11-OCT-2016 EOR    Fixed Bug 18922: Televisa - Draw Cash (Cash prize) - Not show concept of cup�n promocional in the ticket
// 12-OCT-2016 EOR    Fixed Bug 18922: Televisa - Draw Cash (Cash prize) - Not show concept of cup�n promocional in the ticket
// 17-OCT-2016 EOR    Fixed Bug 18001: Cashier: Voucher of Recharge not show the amount of company B
// 19-OCT-2016 LTC    Bug 14809:Ref.409 - Card replacement - Wrong information in header section - must be B company information
// 01-DIC-2016 ETP    Bug 21195: Codere ticket incorrect loser nls.
// 29-DEC-2016 JML    PBI 21361: Company voucher B - VAT breakdown: Voucher modification
// 10-JAN-2017 RAB    Bug 22352: Not can close a GamingTable
// 31-JAN-2017 ATB    Bug 21719:CountR is applying taxes when it never owns
// 11-JAN-2016 ETP    Fixed Bug 22543Prize_coupon not aplicable if cashdesk enabled.
// 24-MAR-2017 ETP    WIGOS-110: Creditlines - Paybacks
// 04-MAY-2017 DPC    WIGOS-1574: Junkets - Flyers - New field "Text in Promotional Voucher" - Change voucher
// 09-MAY-2017 DPC    WIGOS-1219: Junkets - Voucher
// 16-JUN-2017 ATB    PBI 28030: PalacioDeLosNumeros - New design in recharge vouchers
// 20-JUN-2017 ATB    PBI 28030: PalacioDeLosNumeros - New design in recharge vouchers
// 20-FEB-2018 RGR    Bug 31607:WIGOS-7073 Wrong Service Charge label in "Nota de Venta" voucher when "Chips Buy" Gambling Tables operations 
// 04-MAY-2018 MS     PBI 32537: WIGOS-10601 Philippines - Generate a Cashout Receipt 
// 13-JUL-2018 AGS    Bug 33616:WIGOS-13496 Win Recycling Voucher Numbers
//------------------------------------------------------------------------------

using Microsoft.Win32;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using WSI.Common.CreditLines;
using WSI.Common.Junkets;
using WSI.Common.AutoPrint.Entities;

namespace WSI.Common
{
  public enum PrintMode
  {
    Preview = 0,
    Print = 1
  }

  public enum CashDeskCloseVoucherType
  {
    ALL = 0,
    A = 1,
    B = 2
  }

  public class VoucherValidationNumber
  {
    public Int64 ValidationNumber;
    public Currency Amount;
    public TITO_VALIDATION_TYPE ValidationType;
    public TITO_TICKET_TYPE TicketType;
    public String CreatedTerminalName;
    public Int64 CreatedPlaySessionId;
    public Int64 CreatedAccountId;
    public Int64 TicketId;
    public Int32 CreatedTerminalType;
    public Int64 SequenceId;
  }

  public class VoucherBusinessLogic
  {
    #region Public

    /// <summary>
    /// Voucher database initialization:
    /// 1. Get a new voucher id: For voucher mode preview vooucher id. is not requested from db)
    /// 2. Get voucher header, footer and print flag from general params.
    /// </summary>
    /// <param name="Mode">Voucher mode:</param>
    ///                    1. Preview.
    ///                    2. Print.
    /// <param name="SqlTrx">Sql Transaction</param>
    /// <param name="Header">Voucher header</param>
    /// <param name="Footer">Voucher footer</param>
    /// <param name="Print">Voucher print</param>
    /// <returns></returns>
    /// 
    public static Int64 VoucherPrintInit(Int64 SessionId
                                       , Int32 UserId
                                       , String UserName
                                       , Int32 TerminalId
                                       , String TerminalName
                                       , SqlTransaction SqlTrx
                                       , CashierVoucherType Type
                                       , OperationCode OperationCode
                                       , out String Header
                                       , out String Footer
                                       , out Boolean Print)
    {
      StringBuilder _sb;
      SqlParameter _sql_param;
      Int64 _id_value;

      // Initializations:
      Header = "";
      Footer = "";
      Print = false;

      // Steps:
      // 1. Get a new voucher id.

      // Note: Only for voucher mode Print an id is requested from database.
      _sb = new StringBuilder();
      _sb.AppendLine("INSERT INTO   cashier_vouchers ( cv_session_id, cv_user_id, cv_user_name, cv_cashier_id, cv_cashier_name ) ");
      _sb.AppendLine("     VALUES                    ( @p0, @p1, @p2, @p3, @p4 )  ");
      _sb.AppendLine("        SET    @p5 = SCOPE_IDENTITY() ");

      using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        if (SessionId == 0)
        {
          _sql_cmd.Parameters.Add("@p0", SqlDbType.BigInt).Value = DBNull.Value;
        }
        else
        {
          _sql_cmd.Parameters.Add("@p0", SqlDbType.BigInt).Value = SessionId;
        }

        if (UserId == 0)
        {
          _sql_cmd.Parameters.Add("@p1", SqlDbType.Int).Value = DBNull.Value;
        }
        else
        {
          _sql_cmd.Parameters.Add("@p1", SqlDbType.Int).Value = UserId;
        }

        if (UserName == null)
        {
          _sql_cmd.Parameters.Add("@p2", SqlDbType.NVarChar, 50).Value = "";
        }
        else
        {
          _sql_cmd.Parameters.Add("@p2", SqlDbType.NVarChar, 50).Value = UserName;
        }

        if (TerminalId == 0)
        {
          _sql_cmd.Parameters.Add("@p3", SqlDbType.Int).Value = DBNull.Value;
        }
        else
        {
          _sql_cmd.Parameters.Add("@p3", SqlDbType.Int).Value = TerminalId;
        }

        if (TerminalName == null)
        {
          _sql_cmd.Parameters.Add("@p4", SqlDbType.NVarChar, 50).Value = "";
        }
        else
        {
          _sql_cmd.Parameters.Add("@p4", SqlDbType.NVarChar, 50).Value = TerminalName;
        }

        _sql_param = _sql_cmd.Parameters.Add("@p5", SqlDbType.BigInt);
        _sql_param.Direction = ParameterDirection.Output;

        if (_sql_cmd.ExecuteNonQuery() != 1)
        {
          Log.Error("VoucherInit. New voucher id couldn�t be retrieved.");

          return 0;
        }

        _id_value = (Int64)_sql_param.Value;
      }

      // Get header, footer and print information from db.
      GetVoucherInformation(SqlTrx, Type, out Header, out Footer, out Print);

      return _id_value;
    } // VoucherPrintInit

    /// <summary>
    /// Voucher initialization:
    /// Only gets voucher header and footer from general params.
    /// </summary>
    /// <param name="Mode">Voucher mode:</param>
    ///                    1. Preview.
    ///                    2. Print.
    /// <param name="SqlTrx">Sql Transaction</param>
    /// <param name="Header">Voucher header</param>
    /// <param name="Footer">Voucher footer</param>
    /// <returns></returns>
    /// 
    public static void GetVoucherInformation(SqlTransaction SqlTrx, CashierVoucherType Type, out String Header, out String Footer, out Boolean Print)
    {
      Footer = GetCashierVoucherText(Type, CashierVoucherField.Footer);
      Header = GetCashierVoucherText(Type, CashierVoucherField.Header);
      Print = GetPrintVoucher(SqlTrx);
    } // GetVoucherInformation

    /// <summary>
    /// Insert HTML voucher into database. (Voucher Audit)
    /// </summary>
    /// <param name="VoucherId">Voucher id.</param>
    /// <param name="Html">Voucher HTML</param>
    /// <param name="SqlTrx">Sql Transaction</param>
    /// <returns></returns>
    public static Boolean VoucherSetHtml(Int64 VoucherId, Int64 OperationId, String Html, SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();
      _sb.AppendLine("  UPDATE   cashier_vouchers               ");
      _sb.AppendLine("     SET   cv_html         = @pHtml       ");
      _sb.AppendLine("       ,   cv_operation_id = @pOperationId ");
      _sb.AppendLine("   WHERE   cv_voucher_id   = @pVoucherId   ");

      using (SqlCommand sql_command = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
      {
        sql_command.Parameters.Add("@pHtml", SqlDbType.NVarChar, Int32.MaxValue, "cv_html").Value = Html;
        sql_command.Parameters.Add("@pOperationId", SqlDbType.BigInt, 8, "cv_operation_id").Value = OperationId;
        sql_command.Parameters.Add("@pVoucherId", SqlDbType.BigInt, 8, "cv_voucher_id").Value = VoucherId;

        if (sql_command.ExecuteNonQuery() != 1)
        {
          return false;
        }
      }

      return true;
    } // VoucherSetHtml

    /// <summary>
    /// Get Print mode from general params.
    /// </summary>
    /// <returns></returns>
    public static Boolean GetPrintVoucher()
    {
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      Boolean _print_voucher;

      _sql_conn = null;
      _sql_trx = null;

      try
      {
        _sql_conn = WGDB.Connection();
        _sql_trx = _sql_conn.BeginTransaction();

        _print_voucher = GetPrintVoucher(_sql_trx);

        _sql_trx.Commit();

        return _print_voucher;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            try { _sql_trx.Rollback(); }
            catch { }
          }
          _sql_trx.Dispose();
          _sql_trx = null;
        }

        // Close connection
        if (_sql_conn != null)
        {
          try { _sql_conn.Close(); }
          catch { }
          _sql_conn = null;
        }
      }
    }

    /// <summary>
    /// Get Print mode from general params.
    /// </summary>
    /// <param name="SqlTrx">Sql Transaction</param>
    /// <returns></returns>
    public static Boolean GetPrintVoucher(SqlTransaction SqlTrx)
    {
      String _print_voucher;
      String _env_print_voucher;

      _print_voucher = "";

      try
      {
        // If environment variable CASHIER_VOUCHER_PRINT is defined, use it instead of 
        // GeneralParam Cashier.Voucher.Print.
        _env_print_voucher = Environment.GetEnvironmentVariable("CASHIER_VOUCHER_PRINT");
        if (_env_print_voucher != null)
        {
          return (Resource.GetCompareInfo().Compare(_env_print_voucher, "1") == 0);
        }

        _print_voucher = GeneralParam.Value("Cashier.Voucher", "Print");
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return (Resource.GetCompareInfo().Compare(_print_voucher, "1") == 0);
    } // GetPrintVoucher

    //------------------------------------------------------------------------------
    // PURPOSE: Voucher text to footer, title or header
    //
    //  PARAMS:
    //      - INPUT:
    //          - CashierVoucherType: Taking the type Voucher as enum
    //          - CashierVoucherField: Taking the field Voucher as enum
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    private static string GetCashierVoucherText(CashierVoucherType Type, CashierVoucherField Field)
    {
      String _type;
      String _field;
      String _gp;
      String _text;

      _type = "";

      switch (Type)
      {
        case CashierVoucherType.NotSet:
          _type = "";
          break;

        case CashierVoucherType.MBSalesLimitChange:
          _type = "MobileBank.SalesLimitChange";
          break;

        case CashierVoucherType.MBDeposit:
          _type = "MobileBank.Deposit";
          break;

        case CashierVoucherType.MBClosing:
          _type = "MobileBank.Closing";
          break;

        // LTC 19-OCT-2016
        case CashierVoucherType.CardReplacement:
        case CashierVoucherType.CardReplacement_B:
          if (Misc.IsCardPlayerToCompanyB())
          {
            _type = "Voucher.B";
          }
          else
          {
            _type = "Voucher.A";
          }
          break;

        default:
          _type = Type.ToString();
          break;
      }

      _field = Field.ToString();

      _gp = _field;
      if (!String.IsNullOrEmpty(_type))
      {
        _gp = _type + "." + _field;
      }

      _text = GeneralParam.Value("Cashier.Voucher", _gp);
      if (String.IsNullOrEmpty(_text))
      {
        _text = GeneralParam.Value("Cashier.Voucher", _field);
      }

      return _text;
    } // GetCashierVoucherText


    #endregion
  } // VoucherBusinessLogic

  public class Voucher
  {
    #region Constants

    public const String VOUCHER_DIV_REPRINT = "<div id=\"HIDDEN_REPRINT_TEXT\"></div>";
    public const String VOUCHER_DIV_UNDO_OPERATION = "<div id=\"HIDDEN_UNDO_OPERATION_TEXT\"></div>";
    public const String VOUCHER_DIV_EXCHANGE = "@HIDDEN_EXCHANGE_TEXT";
    public const int VOUCHER_CACHIER_INFO_MAX_ELEMENTS = 20;

    #endregion

    #region Members

    public String VoucherFileName;
    public Boolean PrintVoucher;
    public String VoucherHTML;

    // RCI 17-MAY-2011
    public Int32 VoucherMode;
    public DateTime VoucherDateTime;
    // RCI 25-MAY-2011: For Mode 01 (Palacio de los N�meros)
    public SequenceId VoucherSequenceId;
    public Int64 VoucherSeqNumber;
    public PrintMode PrintMode;
    public SqlTransaction SqlTrx;
    // It's only used for B part.
    public Int64 CancelledSequenceId;

    private Dictionary<String, Object> m_dictionary = new Dictionary<String, Object>();
    private CashierVoucherType VoucherType = CashierVoucherType.NotSet;

    // DHA 17-JUL-2015
    public OperationCode OperationCode = OperationCode.NOT_SET;
    public OperationVoucherParams.VoucherParameters m_voucher_params;

    private Int32 m_num_pending_copies;

    public Int32 NumPendingCopies
    {
      get
      {
        return m_num_pending_copies;
      }
      set
      {
        m_num_pending_copies = value;
      }
    }

    #endregion

    #region Class Atributes

    public Int64 VoucherId;
    String VoucherHeader;
    String VoucherFooter;

    #endregion

    #region Constructor

    //------------------------------------------------------------------------------
    // PURPOSE: Generic constructor for vouchers
    //
    //  PARAMS:
    //      - INPUT:
    //          - PrintMode Mode: Print or preview mode
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public Voucher(PrintMode Mode, SqlTransaction SQLTransaction)
    {
      InitializeVoucher(Mode, SQLTransaction);
    } // Voucher

    //------------------------------------------------------------------------------
    // PURPOSE: Generic constructor for vouchers
    //
    //  PARAMS:
    //      - INPUT:
    //          - PrintMode Mode: Print or preview mode
    //          - CashierVoucherType: Taking the type Voucher as enum
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public Voucher(PrintMode Mode, CashierVoucherType Type, SqlTransaction SQLTransaction)
    {
      VoucherType = Type;
      SetValue("CV_TYPE", (Int32)VoucherType);

      InitializeVoucher(Mode, SQLTransaction);
    }

    public Voucher(PrintMode Mode, OperationCode OperationCode, SqlTransaction SQLTransaction)
    {
      this.OperationCode = OperationCode;

      InitializeVoucher(Mode, SQLTransaction);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize the Voucher fields
    //
    //  PARAMS:
    //      - INPUT:
    //          - PrintMode Mode: Print or preview mode
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    private void InitializeVoucher(PrintMode Mode, SqlTransaction SQLTransaction)
    {
      // Class Initializations
      VoucherId = 0;
      VoucherHTML = "";
      VoucherFileName = "";
      VoucherHeader = "";
      VoucherFooter = "";
      VoucherDateTime = DateTime.MinValue;

      VoucherSequenceId = SequenceId.NotSet;
      PrintMode = Mode;
      SqlTrx = SQLTransaction;

      // Load voucher configuration parameters
      m_voucher_params = OperationVoucherParams.OperationVoucherDictionary.GetParameters(OperationCode);
      m_num_pending_copies = m_voucher_params.PrintCopy;

      if (!Int32.TryParse(GeneralParam.Value("Cashier.Voucher", "Mode"), out VoucherMode))
      {
        VoucherMode = 0;
      }
      SetValue("CV_MODE", VoucherMode);

      // Initialize voucher.
      if (Mode == PrintMode.Preview)
      {
        // Voucher Mode: Get header, footer and print information from db.
        VoucherBusinessLogic.GetVoucherInformation(SQLTransaction, VoucherType, out VoucherHeader, out VoucherFooter, out PrintVoucher);
      }
      else if (!m_voucher_params.Generate)
      {
        VoucherId = 0;
      }
      else
      {
        // Voucher Mode: Print - Get header and footer and Voucher id. from db.
        VoucherId = VoucherBusinessLogic.VoucherPrintInit(CommonCashierInformation.SessionId
                                                        , CommonCashierInformation.UserId
                                                        , CommonCashierInformation.UserName
                                                        , (Int32)CommonCashierInformation.TerminalId
                                                        , CommonCashierInformation.TerminalName
                                                        , SQLTransaction
                                                        , VoucherType
                                                        , OperationCode
                                                        , out VoucherHeader
                                                        , out VoucherFooter
                                                        , out PrintVoucher);

        // DHA 17-JUL-2015: Set print mode with voucher configuration
        PrintVoucher = PrintVoucher && m_voucher_params.Print;
      }
    } //InitializeVoucher

    //------------------------------------------------------------------------------
    // PURPOSE: Constructor with no parameters
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    //      - RCI 27-DEC-2010: Used in Email class.
    // 
    public Voucher()
    {
      // Class Initializations
      VoucherId = 0;
      VoucherHTML = "";
      VoucherFileName = "";
      VoucherHeader = "";
      VoucherFooter = "";
      PrintVoucher = false;
    } // Voucher

    #endregion

    #region Properties

    public Int64 Id
    {
      get
      {
        return VoucherId;
      }
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Insert string into voucher HTML
    /// </summary>
    /// <param name="Parameter"></param>
    /// <param name="Value"></param>
    /// <returns></returns>
    public Boolean SetParameterValue(String Parameter, String Value)
    {
      String _modified_voucher;

      try
      {
        // Insert title into html voucher
        _modified_voucher = VoucherHTML.Replace(Parameter, Value);
        VoucherHTML = _modified_voucher;

        return true;
      }
      catch (Exception ex)
      {
        Log.Error(String.Format("SetParameterValue ({0}, {1}) failed!. Exception: {2}", Parameter, Value, ex.Message));
      }

      return false;
    } // SetParameterValue

    /// <summary>
    /// Convert especial characters into HTML check code.
    /// </summary>
    /// <param name="InitialStr"></param>
    /// <returns></returns>
    private string EncodeHTMLSpecialChars(String InputString, Boolean HTMLSpaceChar)
    {
      String _html;

      _html = HttpUtility.HtmlEncode(InputString);

      if (HTMLSpaceChar)
      {
        _html = _html.Replace(" ", "&nbsp;");
      }

      return _html;
    }

    private string[] VoucherCashierInfo()
    {
      String[] _voucher_cashier_info;
      int _idx;

      _idx = 0;
      _voucher_cashier_info = new String[VOUCHER_CACHIER_INFO_MAX_ELEMENTS];

      String _series = "";
      Int64 _sequence = 0;
      Boolean _show_sequence = false;

      if (VoucherSequenceId != SequenceId.NotSet)
      {
        // Get Branch, Sequence and Series.
        switch (VoucherSequenceId)
        {
          case SequenceId.VouchersSplitA:
            _series = GeneralParam.Value("Cashier.Voucher.Mode.01", "Series.A");
            _show_sequence = GeneralParam.GetBoolean("Cashier.Voucher", "Voucher.A.ShowSequence", false);
            break;

          case SequenceId.VouchersSplitB:
          case SequenceId.VouchersSplitBCancel:
            _series = GeneralParam.Value("Cashier.Voucher.Mode.01", "Series.B");
            _show_sequence = GeneralParam.GetBoolean("Cashier.Voucher", "Voucher.B.ShowSequence", false);
            break;

          case SequenceId.VoucherEntrances:
            _show_sequence = true;
            break;

          case SequenceId.VoucherTaxProvisions:
            _series = GeneralParam.Value("Cashier.TaxProvisions", "Series");
            _show_sequence = GeneralParam.GetBoolean("Cashier.TaxProvisions", "Voucher.ShowSequence", false);
            break;

          // LTC 14-ABR-2016
          case SequenceId.VoucherTaxCustody:
            _series = GeneralParam.Value("Cashier.TaxCustody", "Series");
            _show_sequence = GeneralParam.GetBoolean("Cashier.TaxCustody", "Voucher.ShowSequence", false);
            break;

          default:
            // AMF 12-FEB-2015: "Foliado" CageConcepts
            if (VoucherSequenceId >= SequenceId.CageConcepts && VoucherSequenceId <= SequenceId.CageConcepts_Last)
            {
              _series = "";
              _show_sequence = true;
            }
            break;
        }

        if (PrintMode == PrintMode.Print && _show_sequence && m_voucher_params.Generate)
        {
          Sequences.GetValue(SqlTrx, VoucherSequenceId, out _sequence);
          SetValue("CV_SEQUENCE", _sequence);
          VoucherSeqNumber = _sequence;
        }
      }

      // JML 14-AUG-2012
      switch (VoucherMode)
      {
        case 0:
        default:
          // DHA 19-DEC-2014: moved to esay way to understand
          if (CommonCashierInformation.MobileBankId != 0)
          {

            String _mb_id;
            _mb_id = CommonCashierInformation.MobileBankId.ToString();

            MobileBankUserType _MB_user_type;
            UserLogin.UserInfo _user_info;

            if (MobileBankConfig.IsMobileBankLinkedToADevice)
              _MB_user_type = MobileBankUserType.UserDevice;
            else
              _MB_user_type = MobileBankUserType.MobileBankCard;

            if (_MB_user_type == MobileBankUserType.UserDevice)
            {
              _user_info = new UserLogin.UserInfo();

              if (UserLogin.GetUserIfExistInDbByMobileBankId(CommonCashierInformation.MobileBankId, _user_info))
              {
                _voucher_cashier_info[_idx++]=Resource.String("STR_SESSION_USER")+ ": " + _user_info.user_name;
                _voucher_cashier_info[_idx++] = Resource.String("STR_FRM_CHANGE_PIN_HOLDER") + ": " + _user_info.full_name;
              }
              else
              {
                _voucher_cashier_info[_idx++] = Resource.String("STR_SESSION_USER") + ":" ;
                _voucher_cashier_info[_idx++] = Resource.String("STR_FRM_CHANGE_PIN_HOLDER") + ":";
              }

            }
            else
            {
  
              if (!String.IsNullOrEmpty(CommonCashierInformation.MobileBankUserName))
              {
                _mb_id += " - " + CommonCashierInformation.MobileBankUserName.Substring(0, Math.Min(20, CommonCashierInformation.MobileBankUserName.Length));
              }

              _voucher_cashier_info[_idx++] = Resource.String("STR_VOUCHER_GENERAL_NAME_MB") + ": " + _mb_id;

            }
          }

          _voucher_cashier_info[_idx++] = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ": " + CommonCashierInformation.UserName;

          _voucher_cashier_info[_idx++] = Resource.String("STR_VOUCHER_TERMINAL_ID") + ": " + Computer.Alias(CommonCashierInformation.TerminalName);

          // DHA 17-DEC-2014: added general param to show OperationId
          if (GeneralParam.GetBoolean("Cashier.Voucher", "ShowOperationId", false))
          {
            if (PrintMode == PrintMode.Preview)
            {
              // On preview mode show OperationId = 0
              _voucher_cashier_info[_idx++] = Resource.String("STR_FRM_VOUCHERS_REPRINT_014") + ": " + "0";
            }
            else
            {
              // On print, set a key for the Save method can insert the OperationId
              //_voucher_cashier_info[_idx++] = Resource.String("STR_FRM_VOUCHERS_REPRINT_014") + ": " + "@VOUCHER_OPERATION_ID";
              _voucher_cashier_info[_idx++] = "@VOUCHER_OPERATION_ID";
            }
          }

          // DHA 17-DEC-2014: added general param to show VoucherId
          if (GeneralParam.GetBoolean("Cashier.Voucher", "ShowVoucherId", true))
          {
            _voucher_cashier_info[_idx++] = Resource.String("STR_VOUCHER_VOUCHER_ID") + ": " + VoucherId.ToString();
          }

          if (_show_sequence)
          {
            _voucher_cashier_info[_idx] = Resource.String("STR_VOUCHER_FOLIO_ID")
                                  + ": " + _sequence.ToString("000000000000");
            if (_series != "")
            {
              _voucher_cashier_info[_idx] = _voucher_cashier_info[_idx]
                        + " " + Resource.String("STR_VOUCHER_SERIES_ID") + ": " + _series;
            }
            _idx++;
            // Para imprimir el n� de secuencia del documento devuelto
            if (CancelledSequenceId > 0) // no imprimir para Palacio de los N�meros
            {
              _voucher_cashier_info[_idx] = Resource.String("STR_VOUCHER_FOLIO_ID_REF")
                        + ": " + CancelledSequenceId.ToString("000000000000");
              if (_series != "")
              {
                _voucher_cashier_info[_idx] = _voucher_cashier_info[_idx]
                          + " " + Resource.String("STR_VOUCHER_SERIES_ID") + ": " + _series;
              }
              _idx++;
            }
          }
          break;

        case 1: // Palacio de los N�meros
          {
            // Always Show Sequence in "Palacio de los N�meros".
            _show_sequence = true;

            _voucher_cashier_info[_idx++] = Resource.String("STR_VOUCHER_CASHIER_ID")
                                  + " " + Computer.Alias(CommonCashierInformation.TerminalName)
                                  + " - " + CommonCashierInformation.UserName;
            _voucher_cashier_info[_idx++] = GeneralParam.Value("Cashier.Voucher.Mode.01", "Branch")
                                  + " " + VoucherDateTime.ToString();

            // DHA 17-DEC-2014: added general param to show OperationId
            if (GeneralParam.GetBoolean("Cashier.Voucher", "ShowOperationId", false))
            {
              if (PrintMode == PrintMode.Preview)
              {
                // On preview mode show OperationId = 0
                _voucher_cashier_info[_idx++] = Resource.String("STR_FRM_VOUCHERS_REPRINT_014") + ": " + "0";
              }
              else
              {
                // On print, set a key for the Save method can insert the OperationId
                _voucher_cashier_info[_idx++] = "@VOUCHER_OPERATION_ID";
              }
            }

            // DHA 17-DEC-2014: added general param to show VoucherId
            if (GeneralParam.GetBoolean("Cashier.Voucher", "ShowVoucherId", true))
            {
              _voucher_cashier_info[_idx++] = Resource.String("STR_VOUCHER_VOUCHER_ID") + ": " + VoucherId.ToString();
            }
          }

          if (_show_sequence)
          {
            _voucher_cashier_info[_idx] = Resource.String("STR_VOUCHER_FOLIO_ID")
                                  + ": " + _sequence.ToString("000000000000");
            if (_series != "")
            {
              _voucher_cashier_info[_idx] = _voucher_cashier_info[_idx]
                        + " " + Resource.String("STR_VOUCHER_SERIES_ID") + ": " + _series;
            }
            _idx++;
          }
          break;
      }

      if (CommonCashierInformation.UserId != CommonCashierInformation.AuthorizedByUserId && CommonCashierInformation.AuthorizedByUserId != 0)
      {
        _voucher_cashier_info[_idx++] = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ": " + CommonCashierInformation.AuthorizedByUserName; // jcor   

        //LJM 26-NOV-2013 If approver's signature is required
        if (GeneralParam.GetInt32("Cashier.Voucher", "AuthorizedBySignature") > 0)
        {
          _voucher_cashier_info[_idx++] = Resource.String("STR_VOUCHER_AUTHORIZED_BY_SIGNATURE") + ": ";

          for (int i = 0; i < GeneralParam.GetInt32("Cashier.Voucher", "AuthorizedBySignature", 0, 0, 10); i++)
          {
            _voucher_cashier_info[_idx - 1] += System.Environment.NewLine;
          }
        }
      }

      return _voucher_cashier_info;
    } // VoucherCashierInfo

    private string[] VoucherExchangeInfo()
    {
      String[] _voucher_cashier_info;
      int _idx;

      _voucher_cashier_info = new String[VOUCHER_CACHIER_INFO_MAX_ELEMENTS];
      _idx = 0;

      // Show exchange rate when is floor dual currency
      if (Misc.IsFloorDualCurrencyEnabled())
      {
        List<CurrencyExchangeType> _types = new List<CurrencyExchangeType>();
        List<CurrencyExchange> _currencies = null;
        CurrencyExchange _national_curr = null;

        _types.Add(CurrencyExchangeType.CURRENCY);

        CurrencyExchange.GetAllowedCurrencies(true, _types, false, false, false, false, out _national_curr, out _currencies);

        if (_currencies.Count > 1)
        {
          _idx++;
          _voucher_cashier_info[_idx] = " <table border='0' cellpadding='0' cellspacing='0' width='100%'>                                      ";
          _voucher_cashier_info[_idx] += "   <tr>                                                                                              ";
          _voucher_cashier_info[_idx] += "     <td colspan='5' align='center'><b>" + Resource.String("STR_VOUCHER_EXCHANGE_RATE") + "</b></td> ";
          _voucher_cashier_info[_idx] += "   </tr>                                                                                             ";
          _voucher_cashier_info[_idx] += "   <tr>                                                                                              ";
          _voucher_cashier_info[_idx] += "     <td>&nbsp;</td>                                                                                 ";
          _voucher_cashier_info[_idx] += "  </tr></br/>                                                                                        ";

          foreach (CurrencyExchange _cur in _currencies)
          {
            if (_national_curr.CurrencyCode != _cur.CurrencyCode)
            {
              _idx++;
              _voucher_cashier_info[_idx] = " <tr>                                                                                             ";
              _voucher_cashier_info[_idx] += "   <td align='center' style='width: 5%;'>1</td>                                                  ";
              _voucher_cashier_info[_idx] += "   <td align='left' style='width: 11%;'>" + _cur.CurrencyCode + "</td>                           ";
              _voucher_cashier_info[_idx] += "   <td align='center' style='width: 5%;'>=</td>                                                  ";
              _voucher_cashier_info[_idx] += "   <td align='right'>" + _cur.ChangeRate + "</td>                                                ";
              _voucher_cashier_info[_idx] += "   <td align='right' style='width: 11%;'>" + _national_curr.CurrencyCode + "</td>                ";
              _voucher_cashier_info[_idx] += "</tr>                                                                                            ";
            }
          }

          _idx++;
          _voucher_cashier_info[_idx] = "</table><br/><br/>                                                                                    ";
        }
      }

      return _voucher_cashier_info;
    } // VoucherCashierInfo

    private string ConvertCurrencyToLetters(Currency Value)
    {
      return Common.NumberToLetter.CurrencyString(Value);
    } // ConvertCurrencyToLetters

    private String GetDefaultSignatureRoom(Boolean ShowAuthorizedBy)
    {
      String _default_signature =
                "<center><b>" + Resource.String("STR_VOUCHER_SIGN_TITLE") + "</b></center>" +
                "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">" +
                  "<tr>" +
                    "<td>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_SIGN_CLIENT") + ":</td>" +
                  "</tr>" +
                  "<tr><td>&nbsp;</td></tr>" +
                  "<tr><td>&nbsp;</td></tr>" +

                  (ShowAuthorizedBy ?
                  "<tr>" +
                    "<td>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_SIGN_AUTHORIZED_BY") + ":</td>" +
                  "</tr>" : String.Empty) +

                  "<tr><td>&nbsp;</td></tr>" +
                  "<tr><td>&nbsp;</td></tr>" +
                "</table>" +
                "<hr noshade size=\"4\">";

      return _default_signature;
    }

    #endregion

    #region Public Methods

    public void SetValue(String Key, Object Value)
    {
      if (!m_dictionary.ContainsKey(Key))
      {
        m_dictionary.Add(Key, Value);
      }
      else
      {
        m_dictionary[Key] = Value;
      }
    } // SetValue

    /// <summary>
    /// Chage page setup settings for printing.
    /// 1. Clear header and footer.
    /// 2. Set proper margins.
    /// </summary>
    public static Boolean PageSetup()
    {
      RegistryKey regkey;
      Object ie_header;
      Object ie_footer;
      Object margin_top;
      Object margin_bottom;
      Object margin_left;
      Object margin_right;
      Object shrink_to_fit;
      PrintDocument _dummy_print_document;
      String _printer_name;
      Decimal _margin_left_value;
      Decimal _margin_right_value;

      // 1. Clear header and footer.
      regkey = Registry.CurrentUser;

      try
      {
        regkey = regkey.OpenSubKey(@"Software\Microsoft\Internet Explorer\PageSetup", true);
      }
      catch (Exception ex)
      {
        Log.Error("PageSetup. Error reading registry PageSetup key. " + ex.Message);

        return false;
      }

      if (regkey == null)
      {
        Log.Error("PageSetup. Registry key does not exist");

        return false;
      }

      try
      {
        ie_header = regkey.GetValue("header");
        regkey.SetValue("header", "");
      }
      catch (Exception ex)
      {
        Log.Error("PageSetup. Error updating header key on registry PageSetup. " + ex.Message);

        return false;
      }

      try
      {
        ie_footer = regkey.GetValue("footer");
        regkey.SetValue("footer", "");
      }
      catch (Exception ex)
      {
        Log.Error("PageSetup. Error updating footer key on registry PageSetup. " + ex.Message);

        return false;
      }

      // 2. Set proper margins.
      try
      {
        margin_top = regkey.GetValue("margin_top");
        regkey.SetValue("margin_top", "0");
      }
      catch (Exception ex)
      {
        Log.Error("PageSetup. Error updating margin_top key on registry PageSetup. " + ex.Message);

        return false;
      }

      try
      {
        margin_bottom = regkey.GetValue("margin_bottom");
        regkey.SetValue("margin_bottom", "0");
      }
      catch (Exception ex)
      {
        Log.Error("PageSetup. Error updating margin_bottom key on registry PageSetup. " + ex.Message);

        return false;
      }

      _dummy_print_document = new PrintDocument();
      _printer_name = _dummy_print_document.PrinterSettings.PrinterName.ToUpper();

      // XID 06-SEP-2010. Increased to 0.16 to support Epson TM-T88V
      _margin_left_value = 0.16m;
      _margin_right_value = 0;

      if (_printer_name.Contains("CUSTOM_SIZE"))
      {
        _margin_left_value = GeneralParam.GetDecimal("Cashier.Voucher", "PrinterCustomSize.Margin.Left", 0.16m);
        _margin_right_value = GeneralParam.GetDecimal("Cashier.Voucher", "PrinterCustomSize.Margin.Right", 0);
      }

      try
      {
        margin_left = regkey.GetValue("margin_left");
        regkey.SetValue("margin_left", Format.LocalNumberToDBNumber(_margin_left_value.ToString()));
      }
      catch (Exception ex)
      {
        Log.Error("PageSetup. Error updating margin_left key on registry PageSetup. " + ex.Message);

        return false;
      }

      try
      {
        margin_right = regkey.GetValue("margin_right");
        regkey.SetValue("margin_right", Format.LocalNumberToDBNumber(_margin_right_value.ToString()));
      }
      catch (Exception ex)
      {
        Log.Error("PageSetup. Error updating margin_right key on registry PageSetup. " + ex.Message);

        return false;
      }

      try
      {
        shrink_to_fit = regkey.GetValue("Shrink_To_Fit");
        regkey.SetValue("Shrink_To_Fit", "yes");
      }
      catch (Exception ex)
      {
        Log.Error("PageSetup. Error updating Shrink_To_Fit key on registry PageSetup. " + ex.Message);

        return false;
      }

      return true;
    } // PageSetup

    /// <summary>
    /// Call a browser control to print the input voucher.
    /// </summary>
    public string GetFileName()
    {
      FileStream file_stream;
      StreamWriter file_writer;
      String voucher_filename;

      file_writer = null;
      voucher_filename = "Voucher.html";

      try
      {
        // Create a temporary file with voucher to print
        voucher_filename = Path.Combine(Directory.GetCurrentDirectory(), voucher_filename);

        file_stream = new FileStream(voucher_filename, FileMode.Create, FileAccess.Write);
        file_writer = new StreamWriter(file_stream);

        // Write voucher
        file_writer.BaseStream.Seek(0, SeekOrigin.Begin);
        file_writer.Write(VoucherHTML);
      }
      catch (Exception ex)
      {
        Log.Error("Print. Error writing html voucher. File: " + voucher_filename.ToString());
        Log.Exception(ex);
      }
      finally
      {
        file_writer.Close();

        VoucherFileName = voucher_filename;
      }

      return voucher_filename;
    } // GetFileName

    public void LoadHeader(String Value)
    {
      VoucherHeader = Value;
    } // LoadHeader

    public void LoadFooter(String Footer, String ExtraFooter)
    {
      VoucherFooter = Footer;

      if (ExtraFooter != "")
      {
        VoucherFooter += "\n" + ExtraFooter;
      }
    } // LoadFooter

    /// <summary>
    /// Loads voucher structure from resource file.
    /// </summary>
    /// <returns>HTML voucher string</returns>
    public void LoadVoucher(String VoucherName)
    {
      String _html_doc;
      Int32 _position;
      Boolean _insert_hidden_div;
      String _data_before_div;

      _html_doc = "";

      // Get voucher structure from resource file.
      try
      {
        _html_doc = Resource.String(VoucherName);
      }
      catch (Exception ex)
      {
        Log.Error("Error reading voucher structure file from resource." + VoucherName);
        Log.Exception(ex);
      }

      if (String.IsNullOrEmpty(_html_doc))
      {
        Log.Error("LoadVoucher. Voucher retrieved from Db is empty. Voucher: " + VoucherName);
      }

      // UNDO OPERATION
      _insert_hidden_div = false;
      _data_before_div = "@VOUCHER_HEADER</div>";

      _position = _html_doc.IndexOf(_data_before_div);
      if (_position >= 0)
      {
        _position += _data_before_div.Length;
        _insert_hidden_div = true;
      }

      if (_insert_hidden_div)
      {
        _html_doc = _html_doc.Insert(_position, VOUCHER_DIV_UNDO_OPERATION);
      }

      // ExChange
      _insert_hidden_div = false;
      _data_before_div = "@VOUCHER_CASHIER_INFO";

      _position = _html_doc.IndexOf(_data_before_div);
      if (_position >= 0 && Misc.IsFloorDualCurrencyEnabled())
      {
        _position += _data_before_div.Length;
        _insert_hidden_div = true;
      }

      if (_insert_hidden_div)
      {
        _html_doc = _html_doc.Insert(_position, VOUCHER_DIV_EXCHANGE);
      }

      // REPRINT
      _insert_hidden_div = false;
      _data_before_div = "@VOUCHER_CASHIER_INFO";

      _position = _html_doc.IndexOf(_data_before_div);
      if (_position >= 0)
      {
        _position += _data_before_div.Length;
        _insert_hidden_div = true;
      }

      if (_insert_hidden_div)
      {
        _html_doc = _html_doc.Insert(_position, VOUCHER_DIV_REPRINT);
      }

      VoucherHTML = _html_doc;
    } // LoadVoucher

    /// <summary>
    /// Load voucher general data
    /// </summary>
    /// <param name="AddFooter">Checks if should to add a the general footer</param>
    protected void LoadVoucherGeneralData(bool AddFooter)
    {
      String[] header_data;
      String[] footer_data;
      String[] _vc_info;
      Int32 num_rows;
      Boolean _encode;

      // Header: Casino data from database - Split database string
      if (!String.IsNullOrEmpty(VoucherHeader))
      {
        String header = VoucherHeader.Replace("\\n", "\n");
        header_data = header.Split('\n');
      }
      else
      {
        header_data = new String[1] { "" };
      }

      AddMultipleLineString("VOUCHER_HEADER", header_data);

      // MBF 20-SEP-2010 Cannot insert Voucher Logo in database
      AddString("VOUCHER_LOGO", "");

      // VoucherDateTime must be set before calling VoucherCashierInfo().
      VoucherDateTime = WGDB.Now;
      AddDateTime("VOUCHER_DATE_TIME", VoucherDateTime);

      // Return an array of String containing the Voucher Cashier Info, but also
      // set the Sequence (Folio) for Mode 01 (Palacio de los N�meros).
      _vc_info = VoucherCashierInfo();

      AddMultipleLineString("VOUCHER_CASHIER_INFO", _vc_info);

      // Return an array of String containing the currencies exchange
      _vc_info = VoucherExchangeInfo();
      AddMultipleLineString("HIDDEN_EXCHANGE_TEXT", _vc_info, false, false);

      // Footer: Footer from database - Split database string
      // A final text should be added at the end in order to print line breaks.
      if (!String.IsNullOrEmpty(VoucherFooter))
      {
        num_rows = 0;
        String footer = VoucherFooter.Replace("\\n", "\n");
        footer_data = footer.Split('\n');

        // Line breaks are replaced by blanks un voucher printing: Add a 
        // character on the last line if there is an blank character line.
        // APB (05-MAR-2009)
        // GetUpperBound returns a 0-based number: it is not safe to use its result - 1
        num_rows = footer_data.GetUpperBound(num_rows);

        if (footer_data[num_rows].CompareTo(" ") == 0)
        {
          footer_data[num_rows] = String.Concat(footer_data[num_rows], ".");
        }
      }
      else
      {
        footer_data = new String[1] { "" };
      }

      //SGB 08-OCT-2014 Use especific CashierVoucherType for especial voucher footer.
      switch (VoucherType)
      {
        case CashierVoucherType.CashOpening:
        case CashierVoucherType.CashClosing:
        case CashierVoucherType.MBDeposit:
        case CashierVoucherType.MBClosing:
        case CashierVoucherType.MBSalesLimitChange:
        case CashierVoucherType.CashDeposit:
        case CashierVoucherType.CashWithdrawal:
        case CashierVoucherType.CashDeskCloseUnbalanced:
        case CashierVoucherType.CashierTransferCashDeposit:
        case CashierVoucherType.CashierTransferCashWithdrawal:
          _encode = false;
          break;

        default:
          _encode = true;
          break;
      }

      if (AddFooter)
      {
        AddMultipleLineString("VOUCHER_FOOTER", footer_data, _encode);
      }
    } // LoadVoucherGeneralData

    /// <summary>
    /// Load voucher general data
    /// </summary>
    protected void LoadVoucherGeneralData()
    {
      LoadVoucherGeneralData(true);
    } // LoadVoucherGeneralData

    /// <summary>
    /// Load voucher general data
    /// </summary>
    protected void LoadVoucherCageGeneralDataFromGUI(String[] VcInfo)
    {
      String[] header_data;
      String[] footer_data;
      int num_rows;

      // Header: Casino data from database - Split database string
      if (!String.IsNullOrEmpty(VoucherHeader))
      {
        String header = VoucherHeader.Replace("\\n", "\n");
        header_data = header.Split('\n');
      }
      else
      {
        header_data = new String[1] { "" };
      }

      AddMultipleLineString("VOUCHER_HEADER", header_data);

      // MBF 20-SEP-2010 Cannot insert Voucher Logo in database
      AddString("VOUCHER_LOGO", "");

      // VoucherDateTime must be set before calling VoucherCashierInfo().
      VoucherDateTime = WGDB.Now;
      AddDateTime("VOUCHER_DATE_TIME", VoucherDateTime);

      // Return an array of String containing the Voucher Cashier Info, but also
      // set the Sequence (Folio) for Mode 01 (Palacio de los N�meros).

      VcInfo[0] = Resource.String("STR_VOUCHER_CAGE_NEW_OPERATION_SESSION") + ": " + VcInfo[0];
      VcInfo[1] = Resource.String("STR_VOUCHER_CAGE_NEW_OPERATION_USER") + ": " + VcInfo[1];
      VcInfo[2] = Resource.String("STR_VOUCHER_VOUCHER_ID") + ": " + VoucherId.ToString();
      VcInfo[3] = "";

      AddMultipleLineString("VOUCHER_CAGE_NEW_OPERATION_INFO", VcInfo);

      // Footer: Footer from database - Split database string
      // A final text should be added at the end in order to print line breaks.
      if (!String.IsNullOrEmpty(VoucherFooter))
      {
        num_rows = 0;
        String footer = VoucherFooter.Replace("\\n", "\n");
        footer_data = footer.Split('\n');

        // Line breaks are replaced by blanks un voucher printing: Add a 
        // character on the last line if there is an blank character line.
        // APB (05-MAR-2009)
        // GetUpperBound returns a 0-based number: it is not safe to use its result - 1
        num_rows = footer_data.GetUpperBound(num_rows);

        if (footer_data[num_rows].CompareTo(" ") == 0)
        {
          footer_data[num_rows] = String.Concat(footer_data[num_rows], ".");
        }
      }
      else
      {
        footer_data = new String[1] { "" };
      }

      AddMultipleLineString("VOUCHER_FOOTER", footer_data);
    } // LoadVoucherGeneralData

    /// <summary>
    /// Save a voucher into the vouchers table
    /// </summary>
    /// <param name="SQLTransaction"></param>
    public Boolean Save(SqlTransaction SQLTransaction)
    {
      return Save(0, SQLTransaction);
    } // Save

    /// <summary>
    /// Save a voucher into the vouchers table
    /// </summary>
    /// <param name="SQLTransaction"></param>
    public Boolean Save(Int64 OperationId, SqlTransaction SQLTransaction)
    {
      Int64 _account_id;

      // DHA 17-JUL-2015: Save only if the operation code is configured
      if (!m_voucher_params.Generate)
      {
        return true;
      }

      if (OperationId > 0)
      {
        if (Operations.DB_GetAccountIdFromOperation(OperationId, out _account_id, SQLTransaction))
        {
          SetValue("CV_ACCOUNT_ID", _account_id);
        }

        // DHA 19-DEC-2014: added to set the operation id in tickets
        VoucherHTML = VoucherHTML.Replace("@VOUCHER_OPERATION_ID", Resource.String("STR_FRM_VOUCHERS_REPRINT_014") + ": " + OperationId.ToString());
      }

      VoucherHTML = VoucherHTML.Replace("@VOUCHER_OPERATION_ID<br>", "");

      SetValue("CV_HTML", VoucherHTML);
      SetValue("CV_OPERATION_ID", OperationId);

      return VoucherSaveDictionary();
    } // Save

    public Boolean VoucherSaveDictionary()
    {
      String _sql_txt;
      String _param_name;
      Int32 _idx_param;
      SqlCommand _sql_cmd;
      Decimal _dec_value;

      if (m_dictionary.Count == 0)
      {
        return true;
      }

      _sql_cmd = new SqlCommand();
      _sql_cmd.Connection = SqlTrx.Connection;
      _sql_cmd.Transaction = SqlTrx;

      _sql_txt = "";
      _sql_txt += "UPDATE   CASHIER_VOUCHERS ";

      _idx_param = 1;

      foreach (String _key in m_dictionary.Keys)
      {
        _param_name = "@p" + _idx_param;
        _sql_txt += _idx_param == 1 ? "  SET   " : "       , ";
        _sql_txt += _key + " = " + _param_name;

        if (m_dictionary[_key] is Currency)
        {
          _dec_value = (Decimal)((Currency)m_dictionary[_key]);
          _sql_cmd.Parameters.Add(new SqlParameter(_param_name, _dec_value));
        }
        else
        {
          _sql_cmd.Parameters.Add(new SqlParameter(_param_name, m_dictionary[_key]));
        }

        _idx_param++;
      }

      _sql_txt += " WHERE   CV_VOUCHER_ID   = @pVoucherId ";

      _sql_cmd.Parameters.Add("@pVoucherId", SqlDbType.BigInt).Value = VoucherId;

      _sql_cmd.CommandText = _sql_txt;

      if (_sql_cmd.ExecuteNonQuery() != 1)
      {
        return false;
      }

      return true;
    } // VoucherSaveDictionary

    /// <summary>
    /// Add string parameter into voucher.
    /// </summary>
    /// <param name="Parameter">Parameter name</param>
    /// <param name="Value">Parameter value</param>
    /// <returns></returns>
    public Boolean AddString(String Parameter, String Value)
    {
      //if (Parameter == "VOUCHER_CARD_ACCOUNT_ID" || Parameter == "VOUCHER_CARD_CARD_ID")
      //{
      //  Value = EncryptText(Parameter, Value);
      //}

      String param_value = "";

      // Check special characters
      param_value = EncodeHTMLSpecialChars(Value, false);

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      // Insert title into html voucher
      return SetParameterValue(Parameter, param_value);
    } // AddString

    /// <summary>
    /// Add string percent parameter into voucher.
    /// </summary>
    /// <param name="Parameter">Parameter name</param>
    /// <param name="Value">Parameter value</param>
    /// <returns></returns>
    public Boolean AddStringPercent(String Parameter, Decimal Value, Int32 Decimals)
    {
      //if (Parameter == "VOUCHER_CARD_ACCOUNT_ID" || Parameter == "VOUCHER_CARD_CARD_ID")
      //{
      //  Value = EncryptText(Parameter, Value);
      //}

      String param_value = "";

      // Check special characters
      param_value = EncodeHTMLSpecialChars(Convert.ToString(Math.Round(Value, Decimals)) + " %", true);

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      // Insert title into html voucher
      return SetParameterValue(Parameter, param_value);
    } // AddStringPercent

    /// <summary>
    /// Add string parameter into voucher if enabled
    /// </summary>
    /// <param name="Parameter"></param>
    /// <param name="Value"></param>
    /// <param name="ShowValue"></param>
    /// <returns></returns>
    public void AddStringIf(String Parameter, String Value, Boolean ShowValue)
    {
      String _tax_value;

      _tax_value = String.Empty;
      if (ShowValue)
      {
        _tax_value = Value;
      }

      AddString(Parameter, _tax_value);
    }

    private string EncryptText(string Parameter, string Value)
    {
      try
      {
        switch (Parameter)
        {
          case "VOUCHER_CARD_CARD_ID":
            Value = "************" + Value.Remove(0, 12); /* total number width - only number left for display */
            break;

          case "VOUCHER_CARD_ACCOUNT_ID":
            Value = "**************" + Value.Remove(0, 2);
            break;
        }
      }
      catch (Exception)
      {
        return "";
      }

      return Value;
    } // EncryptText

    public Boolean AddMultipleLineString(String Parameter, String PlainData)
    {
      String[] _str_multiline;

      if (!String.IsNullOrEmpty(PlainData))
      {
        String _str = PlainData.Replace("\\n", "\n");
        _str_multiline = _str.Split('\n');
      }
      else
      {
        _str_multiline = new String[1] { "" };
      }

      return AddMultipleLineString(Parameter, _str_multiline);
    } // AddMultipleLineString

    /// <summary>
    /// Add string parameter into voucher.
    /// </summary>
    /// <param name="Parameter">Parameter name</param>
    /// <param name="Value">Parameter value</param>
    /// <returns></returns>
    public Boolean AddMultipleLineString(String Parameter, String[] MultiLineData)
    {
      return AddMultipleLineString(Parameter, MultiLineData, true);
    }

    public Boolean AddMultipleLineString(String Parameter, String[] MultiLineData, Boolean Encode)
    {
      return AddMultipleLineString(Parameter, MultiLineData, Encode, true);
    }

    public Boolean AddMultipleLineString(String Parameter, String[] MultiLineData, Boolean Encode, Boolean AddBr)
    {
      String _param_value = "";
      String _param_complete = "";
      Int32 _num_lines;
      Int32 _idx_lines;

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      _num_lines = MultiLineData.Length;

      for (_idx_lines = 0; _idx_lines < _num_lines; _idx_lines++)
      {
        // Check for null string
        if (!String.IsNullOrEmpty(MultiLineData[_idx_lines]))
        {
          // Check special characters
          if (Encode)
          {
            _param_value = EncodeHTMLSpecialChars(MultiLineData[_idx_lines], false);
          }
          else
          {
            _param_value = MultiLineData[_idx_lines];
          }

          _param_value = _param_value.Replace(System.Environment.NewLine, "<br />");

          if (AddBr)
          {
            _param_value = _param_value + "<br>\n";
          }
          else
          {
            _param_value = _param_value + "\n";
          }

          _param_complete = String.Concat(_param_complete, _param_value);
        }
      }

      // Insert title into html voucher
      return SetParameterValue(Parameter, _param_complete);
    } // AddMultipleLineString

    /// <summary>
    /// Add currency parameter into voucher.
    /// </summary>
    /// <param name="Parameter">Parameter name</param>
    /// <param name="Value">Parameter value</param>
    /// <returns></returns>
    public Boolean AddCurrency(String Parameter, Currency Value)
    {
      return AddCurrency(Parameter, Value, false);
    } // AddCurrency

    /// <summary>
    /// Add currency parameter into voucher if enabled
    /// </summary>
    /// <param name="Parameter"></param>
    /// <param name="Value"></param>
    /// <param name="ShowValue"></param>
    /// <returns></returns>
    public Boolean AddCurrencyIf(String Parameter, Currency Value, Boolean ShowValue)
    {
      if (ShowValue)
      {
        return AddCurrency(Parameter, Value);
      }
      else
      {
        return AddString(Parameter, String.Empty);
      }
    }

    /// <summary>
    /// Add currency parameter into voucher.
    /// </summary>
    /// <param name="Parameter">Parameter name</param>
    /// <param name="Value">Parameter value</param>
    /// <param name="WithLetters">Indicate if to write the currency with letters</param>
    /// <returns></returns>
    public Boolean AddCurrency(String Parameter, Currency Value, Boolean WithLetters)
    {
      String param_value = "";
      String param_letters = "";

      // Apply currency format to string
      param_value = Value.ToString();

      // Check for special characters
      param_value = EncodeHTMLSpecialChars(param_value, true);

      if (WithLetters)
      {
        param_letters = EncodeHTMLSpecialChars(ConvertCurrencyToLetters(Value), false);
        param_value += "<br/>" + param_letters;
      }

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      // Insert title into html voucher
      return SetParameterValue(Parameter, param_value);
    } // AddCurrency

    // EOR 07-SEP-2016
    /// <summary>
    /// Add currency without symbol parameter into voucher.
    /// </summary>
    /// <param name="Parameter">Parameter name</param>
    /// <param name="Value">Parameter value</param>
    /// <returns></returns>
    public Boolean AddCurrencyWithOutSymbol(String Parameter, Currency Value)
    {
      String param_value = "";

      // Apply currency format to string
      param_value = Value.ToString();

      // Check for special characters
      param_value = EncodeHTMLSpecialChars(param_value, true);

      // Delete Currency Symbol
      if (NumberFormatInfo.CurrentInfo.CurrencySymbol.Length > 0)
      {
        if (param_value.Contains(NumberFormatInfo.CurrentInfo.CurrencySymbol))
        {
          param_value = param_value.Replace(NumberFormatInfo.CurrentInfo.CurrencySymbol, "");
        }
      }

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      // Insert title into html voucher
      return SetParameterValue(Parameter, param_value);
    } // AddCurrency

    /// <summary>
    /// Add numeric parameter into voucher.
    /// </summary>
    /// <param name="Parameter">Parameter name</param>
    /// <param name="Value">Parameter value</param>
    /// <returns></returns>
    public Boolean AddNumber(String Parameter, Int32 Value)
    {
      String param_value = "";

      // Apply currency format to string
      param_value = Value.ToString("#,##0.00");

      // Check for special characters
      param_value = EncodeHTMLSpecialChars(param_value, true);

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      // Insert title into html voucher
      return SetParameterValue(Parameter, param_value);
    } // AddNumber

    public Boolean AddPoint(String Parameter, Points Value)
    {
      String param_value = "";

      // Apply currency format to string
      param_value = Value.ToString();

      // Check for special characters
      param_value = EncodeHTMLSpecialChars(param_value, true);

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      // Insert title into html voucher
      return SetParameterValue(Parameter, param_value);
    } // AddPoint

    /// <summary>
    /// Add integer parameter into voucher.
    /// </summary>
    /// <param name="Parameter">Parameter name</param>
    /// <param name="Value">Parameter value</param>
    /// <returns></returns>
    public Boolean AddInteger(String Parameter, Int32 Value)
    {
      String param_value = "";

      // Apply currency format to string
      param_value = Value.ToString("#,##0");

      // Check for special characters
      param_value = EncodeHTMLSpecialChars(param_value, true);

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      // Insert title into html voucher
      return SetParameterValue(Parameter, param_value);
    } // AddInteger

    /// <summary>
    /// Add datetime parameter into voucher.
    /// </summary>
    /// <param name="Parameter"></param>
    /// <param name="Value"></param>
    /// <returns></returns>
    public Boolean AddDateTime(String Parameter, DateTime Value)
    {
      String param_value = "";

      // Apply currency format to string
      param_value = Format.CustomFormatDateTime(Value, true);

      // Check for special characters
      param_value = EncodeHTMLSpecialChars(param_value, false);

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      // Insert title into html voucher
      return SetParameterValue(Parameter, param_value);
    } // AddDateTime

    /// <summary>
    /// Add currency in letters.
    /// </summary>
    /// <param name="Parameter">Parameter name</param>
    /// <param name="Value">Parameter value</param>
    /// <returns></returns>
    public Boolean AddCurrencyInLetters(String Parameter, Currency Value)
    {
      String param_value;

      // Check for special characters
      param_value = EncodeHTMLSpecialChars(ConvertCurrencyToLetters(Value), false);

      // Add @ to parameter data
      Parameter = "@" + Parameter;

      return SetParameterValue(Parameter, param_value);
    } // AddCurrencyInLetters

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the voucher Sequence Id for B by Operation Id
    //
    //  PARAMS :
    //      - INPUT:
    //            - OperationId
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - 0 : If operation have no sequence 
    //      - >0 : A voucher sequence number 
    //
    //   NOTES: 
    //   
    internal Int64 GetSecuenceId_B(Int64 OperationId, SqlTransaction Trx)
    {
      StringBuilder _sb;
      Object _obj;
      Int64 _sequence_id;

      _sequence_id = 0;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   CV_SEQUENCE                      ");
        _sb.AppendLine("  FROM   CASHIER_VOUCHERS                 ");
        _sb.AppendLine(" WHERE   CV_OPERATION_ID  = @pOperationId ");
        _sb.AppendLine("   AND   CV_TYPE         >= 2             ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;

          _obj = _cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            _sequence_id = (Int64)_obj;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _sequence_id;
    } // GetSequenceId_B

    /// <summary>
    /// Delete currency simbol in voucher Html
    /// </summary>
    /// <param name="VoucherHtml">Html from voucher</param>
    /// <returns></returns>
    public void ReplaceCurrencySimbol(String ReplacementValue)
    {
      String _modified_voucher;

      _modified_voucher = VoucherHTML.Replace("<script type='text/javascript'>$", "@STR_JAVASCRIPT");

      _modified_voucher = _modified_voucher.Replace(NumberFormatInfo.CurrentInfo.CurrencySymbol, ReplacementValue);

      _modified_voucher = _modified_voucher.Replace("@STR_JAVASCRIPT", "<script type='text/javascript'>$");

      VoucherHTML = _modified_voucher;
    } // ReplaceCurrencySimbol

    /// <summary>
    /// Set the parameter value for the Signature Romm by Voucher type
    /// </summary>
    /// <param name="VoucherType"></param>
    internal void SetSignatureRoomByVoucherType(Type VoucherType)
    {
      Boolean _sign_room;
      String _param;

      if (VoucherType == typeof(VoucherCardHandpay))
      {
        _sign_room = GeneralParam.GetBoolean("Cashier.Voucher", "Handpay.SignaturesRoom");
        _param = "@VOUCHER_CARD_HANDPAY_SIGNATURES_ROOM";
      }
      else if (VoucherType == typeof(VoucherHandpayAuthorized))
      {
        _sign_room = true;
        _param = "@VOUCHER_AUTHORITZATION_SIGNATURES";
      }
      else if (VoucherType == typeof(VoucherGetMarker))
      {
        _sign_room = true;
        _param = "@VOUCHER_AUTHORITZATION_SIGNATURES";
      }
      else if (VoucherType == typeof(VoucherPaybackMarker))
      {
        _sign_room = true;
        _param = "@VOUCHER_AUTHORITZATION_SIGNATURES";
      }
      else if (VoucherType == typeof(VoucherCashOut))
      {
        _sign_room = GeneralParam.GetBoolean("Cashier.Voucher", "Redeem.SignaturesRoom");
        _param = "@VOUCHER_REDEEM_SIGNATURES_ROOM";
      }
      else
      {
        throw new Exception("The voucher type passed like a parameter is invalid to get a Signature room");
      }

      if (_sign_room)
      {
        SetParameterValue(_param, GetDefaultSignatureRoom(true));
      }
      else
      {
        SetParameterValue(_param, "");
      }
    }

    public static Boolean IsTerminalDraw()
    {
      return (GeneralParam.GetString("Cashier.Voucher", "Mode", "0") == "8"
            && GeneralParam.GetBoolean("CashDesk.Draw.02", "Enabled", false));
    }

    #endregion
  } // Voucher

  public static class VoucherBuilder
  {
    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the CashIn operation 
    //
    //  PARAMS:
    //      - INPUT:
    //          - Int64 CashierSessionId
    //          - Int64 OperationId   : Operation id (not need in Preview mode)
    //          - PrintMode Mode      : Print or Preview mode
    //          - SqlTransaction SQLTransaction
    //          - IsGamingtable
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static ArrayList CashDeskClose(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats
                                        , Int64 OperationId
                                        , PrintMode Mode
                                        , SqlTransaction SQLTransaction
                                        , Boolean IsGamingTable)
    {
      ArrayList _voucher_list;
      Voucher _temp_voucher;
      Boolean _print_unbalanced;
      VoucherCashDeskCloseUnbalanced _unbalanced_voucher;
      Decimal _difference;

      _voucher_list = new ArrayList();

      // Voucher for Cash Desk (All)
      if (Misc.IsGamingHallMode())
      {
        _temp_voucher = new VoucherGamingHallCashDeskClose(Mode, SQLTransaction, CashierSessionStats);
      }
      else
      {
        _temp_voucher = new VoucherCashDeskClose(CashierSessionStats
                                               , CashDeskCloseVoucherType.ALL
                                               , Mode
                                               , SQLTransaction);
      }

      if (Mode == PrintMode.Print)
      {
        _temp_voucher.Save(OperationId, SQLTransaction);
      }

      _voucher_list.Add(_temp_voucher);

      if (CashierSessionStats.split_enabled_b)
      {
        // Voucher for Cash Desk Business A
        _temp_voucher = new VoucherCashDeskClose(CashierSessionStats
                                               , CashDeskCloseVoucherType.A
                                               , Mode
                                               , SQLTransaction);

        if (Mode == PrintMode.Print)
        {
          _temp_voucher.Save(OperationId, SQLTransaction);
        }

        _voucher_list.Add(_temp_voucher);

        // Voucher for Cash Desk Business B
        _temp_voucher = new VoucherCashDeskClose(CashierSessionStats
                                               , CashDeskCloseVoucherType.B
                                               , Mode
                                               , SQLTransaction);

        if (Mode == PrintMode.Print)
        {
          _temp_voucher.Save(OperationId, SQLTransaction);
        }

        _voucher_list.Add(_temp_voucher);
      }

      _print_unbalanced = GeneralParam.GetBoolean("Cashier.Voucher", "CashClosing.Unbalanced.Print", false);

      if (!IsGamingTable && _print_unbalanced)
      {
        _difference = 0;

        foreach (KeyValuePair<CurrencyIsoType, Decimal> collected_diff in CashierSessionStats.collected_diff)
        {
          _difference += collected_diff.Value;
        }

        if (_difference != 0)
        {
          //print VoucherCurrenciesDifferences
          _unbalanced_voucher = new VoucherCashDeskCloseUnbalanced(CashierSessionStats
                                                                  , Mode
                                                                  , SQLTransaction);
          if (Mode == PrintMode.Print)
          {
            _unbalanced_voucher.Save(OperationId, SQLTransaction);
          }

          _voucher_list.Add(_unbalanced_voucher);
        }
      }

      return _voucher_list;
    } // CashIn

    public static ArrayList CashIn(String[] VoucherAccountInfo
                                   , Int64 AccountId
                                   , TYPE_SPLITS SplitInformation
                                   , VoucherCashIn.InputDetails Details
                                   , Result CashDeskDrawResult
                                   , Int64 OperationId
                                   , PrintMode Mode
                                   , CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION CashDeskId //LTC 22-SEP-2016
                                   , SqlTransaction SQLTransaction
                                   , JunketsBusinessLogic JunketInfo = null
                                   , Int64 PromoGameId = 0)
    {
      return CashIn(VoucherAccountInfo
                   , AccountId
                   , SplitInformation
                   , Details
                   , CashDeskDrawResult
                   , OperationId
                   , OperationCode.NOT_SET
                   , Mode
                   , CashDeskId
                   , SQLTransaction
                   , JunketInfo
                   , PromoGameId);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the CashIn operation 
    //
    //  PARAMS:
    //      - INPUT:
    //          - String HolderName     : Player holder name
    //          - String CardId         : Player account id
    //          - String Trackdata      : Player card trackdata
    //          - Currency CurrentBalance : Actual Balance of the card
    //          - Currency CashIn1      : Amount to pay as Deposit
    //          - Currency CashIn2      : Amount to pay as Uso de sala
    //          - Int64 OperationId     : Operation id (not need in Preview mode)
    //          - PrintMode Mode        : Print or Preview mode
    //          - SqlTransaction SQLTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static ArrayList CashIn(String[] VoucherAccountInfo
                                   , Int64 AccountId
                                   , TYPE_SPLITS SplitInformation
                                   , VoucherCashIn.InputDetails Details
                                   , Result CashDeskDrawResult
                                   , Int64 OperationId
                                   , OperationCode OpCode
                                   , PrintMode Mode
                                   , CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION CashDeskId
                                   , SqlTransaction SQLTransaction
                                   , JunketsBusinessLogic JunketInfo = null
                                   , Int64 PromoGameId = 0)
    {
      ArrayList _voucher_list;
      VoucherCashIn _temp_voucher;
      List<VoucherDrawCashDesk> _temp_voucher_draw_cash_desk;
      Currency _cash_in_2_aux;
      CurrencyExchangeType _payment_type;
      Currency _exchange_commission;
      Boolean _is_foreign_currency;
      CASHIER_MOVEMENT _voucher_cashier_movement;
      Int32 _voucher_mode;
      VoucherCashDeskParticipationDraw _temp_voucher_participation_draw;
      VoucherSummaryCredits _temp_voucher_summary_credits;
      MultiPromos.PromoBalance _increment_bal;
      Currency _total_promos;
      Boolean _chips_sale_winner;

      _voucher_list = new ArrayList();
      _cash_in_2_aux = Details.CashIn2;
      _payment_type = Details.PaymentType;
      _exchange_commission = Details.ExchangeCommission;
      _is_foreign_currency = Details.WasForeingCurrency;
      _voucher_cashier_movement = CASHIER_MOVEMENT.NOT_ASSIGNED;
      _voucher_mode = GeneralParam.GetInt32("Cashier.Voucher", "Mode", 0);
      _chips_sale_winner = true;

      // DHA 29-OCT-2013: Hide prize cupon when Cash Desk Draw is enable
      // DRV & RCI 30-JUL-2014: Integrated chip operations never use CashDeskDraw.
      // LTC 27-SEP-2016
      if (CashDeskDrawBusinessLogic.IsEnabledCashDeskDraws(Convert.ToInt16(CashDeskId)))
      {
        if (CashDeskDrawResult != null)
        {
          Details.FinalBalance.Balance.PromoNotRedeemable -= CashDeskDrawResult.m_nr_awarded;
          _chips_sale_winner = CashDeskDrawResult.m_is_winner;
        }

        _cash_in_2_aux = 0;
      }

      // Voucher for Cash In Deposit
      VoucherCashIn.InputDetails _details = new VoucherCashIn.InputDetails();

      _details.InitialBalance = Details.InitialBalance;
      _details.FinalBalance = Details.FinalBalance;
      _details.CashIn1 = Details.CashIn1;
      _details.CashIn2 = Details.CashIn2;   //EOR 17-OCT-2016
      _details.CashInTaxSplit1 = Details.CashInTaxSplit1;
      _details.CashInTaxSplit2 = Details.CashInTaxSplit2;
      _details.CashInTaxCustody = Details.CashInTaxCustody; // LTC 14-ABR-2016
      _details.TotalBaseTaxSplit2 = Details.TotalBaseTaxSplit2;
      _details.TotalTaxSplit2 = Details.TotalTaxSplit2;
      _details.IsIntegratedChipsSale = Details.IsIntegratedChipsSale;
      _details.IsIntegratedChipsSaleWinner = _chips_sale_winner;
      _increment_bal = Details.FinalBalance - Details.InitialBalance;
      _total_promos = _increment_bal.Balance.PromoRedeemable + _increment_bal.Balance.PromoNotRedeemable;
      // ATB 16-JUN-2017
      _details.CardNumber = Details.CardNumber;

      if (Details.CardNumber != null)
      {
        _details.CardNumber = _details.CardNumber.PadLeft(4, ' ');
      }

      _details.CardHolder = Details.CardHolder;
      _details.IsPayingWithCreditCard = Details.IsPayingWithCreditCard;

      if (_voucher_mode != 8)
      {
        _details.CardPrice = Details.CardPrice;
      }
      else
      {
        _details.CardPrice = 0;
      }

      _temp_voucher = new VoucherCashIn(VoucherAccountInfo
                                       , AccountId
                                       , SplitInformation
                                       , _details
                                       , CASHIER_MOVEMENT.CASH_IN_SPLIT1
                                       , OpCode
                                       , Mode
                                       , SQLTransaction
                                       , JunketInfo
                                       , OperationId
                                       , PromoGameId);

      if (SplitInformation.company_a.hide_currency_symbol &&
         !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
      {
        _temp_voucher.ReplaceCurrencySimbol("");
      }

      //If its a chips swap it means that there are not cash in
      if (OpCode != OperationCode.CHIPS_SWAP)
      {
        if (Mode == PrintMode.Print)
        {
          _temp_voucher.Save(OperationId, SQLTransaction);
        }

        _voucher_list.Add(_temp_voucher);
      }

      if (_temp_voucher.VoucherMode != 5)  // VoucherMode = 5 -> Logrand mode: Only one voucher (including A & B)
      {
        if (Details.CashIn1 > 0 && SplitInformation.enabled_b)
        {
          _details = new VoucherCashIn.InputDetails();

          _details.InitialBalance = Details.InitialBalance;
          _details.FinalBalance = Details.FinalBalance;
          _details.CashIn1 = Details.CashIn1;
          _details.CashIn2 = Details.CashIn2;
          _details.CashInTaxSplit1 = 0;
          _details.CashInTaxSplit2 = Details.CashInTaxSplit2;
          _details.TotalBaseTaxSplit2 = Details.TotalBaseTaxSplit2;
          _details.TotalTaxSplit2 = Details.TotalTaxSplit2;
          _details.IsIntegratedChipsSale = Details.IsIntegratedChipsSale;

          if (Misc.IsVoucherModeTV())
          {
            _details.CardPrice = Details.CardPrice;
            _details.CardAmount = Details.CardPrice;
            _details.CardCommission = Details.CardCommission;
          }
          else
          {
            _details.CardPrice = 0;
          }

          // Voucher for Cash In Uso de Sala
          _temp_voucher = new VoucherCashIn(VoucherAccountInfo
                                           , AccountId
                                           , SplitInformation
                                           , _details
                                           , CASHIER_MOVEMENT.CASH_IN_SPLIT2
                                           , OpCode
                                           , Mode
                                           , SQLTransaction
                                           , null
                                           , 0
                                           , PromoGameId);

          if (SplitInformation.company_b.hide_currency_symbol &&
           !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
          {
            _temp_voucher.ReplaceCurrencySimbol("");
          }

          if (OpCode != OperationCode.CHIPS_SWAP)
          {
            //If its a chips swap it means that there are not cash in
            if (Mode == PrintMode.Print)
            {
              _temp_voucher.Save(OperationId, SQLTransaction);
            }

            _voucher_list.Add(_temp_voucher);
          }

          //JBC 24-DEC-2014 Commission B Voucher
          if (Misc.IsCommissionToCompanyBEnabled() && _exchange_commission >= 0)
          {
            switch (_payment_type)
            {
              case CurrencyExchangeType.CARD:
                _voucher_cashier_movement = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT2;
                break;

              case CurrencyExchangeType.CHECK:
                _voucher_cashier_movement = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT2;
                break;

              case CurrencyExchangeType.CURRENCY:
                if (_is_foreign_currency)
                {
                  _voucher_cashier_movement = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT2;
                }
                break;

              default:
                break;
            }

            if (_voucher_cashier_movement != CASHIER_MOVEMENT.NOT_ASSIGNED)
            {
              _details.ExchangeCommission = _exchange_commission;
              // Voucher for Cash In Uso de Sala
              _temp_voucher = new VoucherCashIn(VoucherAccountInfo
                                               , AccountId
                                               , SplitInformation
                                               , _details
                                                   , _voucher_cashier_movement
                                               , OpCode
                                               , Mode
                                               , SQLTransaction);

              if (SplitInformation.company_b.hide_currency_symbol &&
                  !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
              {
                _temp_voucher.ReplaceCurrencySimbol("");
              }

              //If its a chips swap it means that there are not cash in
              if (OpCode != OperationCode.CHIPS_SWAP)
              {
                if (Mode == PrintMode.Print)
                {
                  _temp_voucher.Save(OperationId, SQLTransaction);
                }

                _voucher_list.Add(_temp_voucher);
              }
            }
          }
        }

        if (Misc.IsCardPlayerToCompanyB() && Details.CardPrice > 0)
        {
          _details = new VoucherCashIn.InputDetails();

          _details.InitialBalance = Details.InitialBalance;
          _details.FinalBalance = Details.FinalBalance;
          _details.CashIn1 = 0;
          _details.CashIn2 = 0;
          _details.CardPrice = Details.CardPrice;
          _details.CashInTaxSplit1 = 0;
          _details.CashInTaxSplit2 = 0;

          // Voucher for Cash In Uso de Sala
          _temp_voucher = new VoucherCashIn(VoucherAccountInfo
                                           , AccountId
                                           , SplitInformation
                                           , _details
                                           , CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN
                                           , OpCode
                                           , Mode
                                           , SQLTransaction);

          if (SplitInformation.company_a.hide_currency_symbol &&
           !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
          {
            _temp_voucher.VoucherHTML = _temp_voucher.VoucherHTML.Replace(NumberFormatInfo.CurrentInfo.CurrencySymbol, "");
          }

          //If its a chips swap it means that there are not cash in
          if (OpCode != OperationCode.CHIPS_SWAP)
          {
            if (Mode == PrintMode.Print)
            {
              _temp_voucher.Save(OperationId, SQLTransaction);
            }

            _voucher_list.Add(_temp_voucher);
          }
        }
      }

      // LTC 22-SEP-2016
      _temp_voucher_draw_cash_desk = CashDeskDrawBusinessLogic.GenerateVoucherDrawCashDesk(VoucherAccountInfo, CashDeskDrawResult, OperationId, OpCode, Mode, SQLTransaction, SplitInformation, CashDeskId);

      if (_temp_voucher_draw_cash_desk.Count != 0 && CashDeskDrawBusinessLogic.GetPrizeMode(Convert.ToInt16(CashDeskId)) == CashDeskDrawBusinessLogic.PrizeMode.CashPrize)
      {
        _temp_voucher_summary_credits = new VoucherSummaryCredits(VoucherAccountInfo
                                                             , SplitInformation
                                                             , Details.CashIn1 + Accounts.SubtractParticipationInDraw(CashDeskDrawResult, CashDeskId)
                                                             , CashDeskDrawBusinessLogic.GetParticipationPrice(Convert.ToInt16(CashDeskId))
                                                             , CashDeskDrawResult.m_re_awarded
                                                             , _total_promos
                                                             , Mode
                                                             , SQLTransaction);

        if (OpCode != OperationCode.CHIPS_SWAP && Mode == PrintMode.Print)
        {
          _temp_voucher_summary_credits.Save(OperationId, SQLTransaction);
          _voucher_list.Add(_temp_voucher_summary_credits);
        }

        if (CashDeskDrawBusinessLogic.GetParticipationPrice(Convert.ToInt16(CashDeskId)) > 0)
        {
          _temp_voucher_participation_draw = new VoucherCashDeskParticipationDraw(VoucherAccountInfo
                                                                            , SplitInformation
                                                                              , CashDeskDrawBusinessLogic.GetParticipationPrice(Convert.ToInt16(CashDeskId))
                                                                            , Mode
                                                                            , SQLTransaction);
          if (OpCode != OperationCode.CHIPS_SWAP && Mode == PrintMode.Print)
          {
            _temp_voucher_participation_draw.Save(OperationId, SQLTransaction);
            _voucher_list.Add(_temp_voucher_participation_draw);
          }
        }
      }

      if (OpCode != OperationCode.CHIPS_SWAP)
      {
        //If its a chips swap it means that there are not cash in.
        _voucher_list.AddRange(_temp_voucher_draw_cash_desk);
      }

      //#if DEBUG
      //      VoucherPrizeCouponDraw _temp_draw_voucher;
      //      _temp_draw_voucher = new VoucherPrizeCouponDraw(VoucherAccountInfo, Mode, SQLTransaction);

      //      if (Mode == PrintMode.Print)
      //      {
      //        _temp_draw_voucher.Save(OperationId, SQLTransaction);
      //      }

      //      _voucher_list.Add(_temp_draw_voucher);
      //#endif

      return _voucher_list;
    } // CashIn

    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the CashOut operation 
    //
    //  PARAMS:
    //      - INPUT:
    //          - String HolderName            : Player holder name
    //          - String CardId                : Player account id
    //          - String Trackdata             : Player card trackdata
    //          - Currency Balance             : Total Balance
    //          - TYPE_SPLITS SplitInformation : Split/Company A/B information
    //          - TYPE_CASH_REDEEM CompanyA    : Company/Voucher A parameteres
    //          - TYPE_CASH_REDEEM CompanyB    : Company/Voucher B parameteres
    //          - Int64 OperationId            : Operation id (not need in Preview mode)
    //          - PrintMode Mode               : Print or Preview mode
    //          - SqlTransaction SQLTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static ArrayList CashOut(String[] VoucherAccountInfo
                                  , Int64 AccountId
                                  , Currency BalanceBeforeCashOut
                                  , TYPE_SPLITS SplitInformation
                                  , TYPE_CASH_REDEEM CompanyA
                                  , TYPE_CASH_REDEEM CompanyB
                                  , Int64 OperationId
                                  , PrintMode Mode
                                  , List<VoucherValidationNumber> ValidationNumbers
                                  , SqlTransaction Trx
                                  , Boolean IsChipsPurchaseWithCashOut
                                  , Boolean IsApplyTax
                                  , Boolean IsTicketPayment)
    {
      PaymentOrder _pay_order;

      _pay_order = new PaymentOrder();
      return CashOut(VoucherAccountInfo
                    , AccountId
                    , BalanceBeforeCashOut
                    , SplitInformation
                    , CompanyA
                    , CompanyB
                    , OperationId
                    , Mode
                    , ValidationNumbers
                    , _pay_order
                    , OperationCode.NOT_SET
                    , Trx
                    , IsChipsPurchaseWithCashOut
                    , IsApplyTax
                    , IsTicketPayment);
    }
    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the CashOut operation in Form Prize Payout & PrizePayut and Recharge
    //
    //  PARAMS:
    //      - INPUT:
    //          - String HolderName            : Player holder name
    //          - String CardId                : Player account id
    //          - String Trackdata             : Player card trackdata
    //          - Currency Balance             : Total Balance
    //          - TYPE_SPLITS SplitInformation : Split/Company A/B information
    //          - TYPE_CASH_REDEEM CompanyA    : Company/Voucher A parameteres
    //          - TYPE_CASH_REDEEM CompanyB    : Company/Voucher B parameteres
    //          - Int64 OperationId            : Operation id (not need in Preview mode)
    //          - PrintMode Mode               : Print or Preview mode
    //          - SqlTransaction SQLTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static ArrayList CashOut(String[] VoucherAccountInfo
                                  , Int64 AccountId
                                  , Currency BalanceBeforeCashOut
                                  , TYPE_SPLITS SplitInformation
                                  , TYPE_CASH_REDEEM CompanyA
                                  , TYPE_CASH_REDEEM CompanyB
                                  , Int64 OperationId
                                  , PrintMode Mode
                                  , List<VoucherValidationNumber> ValidationNumbers
                                  , SqlTransaction Trx
                                  , Boolean IsChipsPurchaseWithCashOut
                                  , Boolean IsApplyTax
                                  , Boolean IsTicketPayment
                                  , OperationCode OperationCode)
    {
      PaymentOrder _pay_order;

      _pay_order = new PaymentOrder();
      return CashOut(VoucherAccountInfo
                    , AccountId
                    , BalanceBeforeCashOut
                    , SplitInformation
                    , CompanyA
                    , CompanyB
                    , OperationId
                    , Mode
                    , ValidationNumbers
                    , _pay_order
                    , OperationCode
                    , Trx
                    , IsChipsPurchaseWithCashOut
                    , IsApplyTax
                    , IsTicketPayment);
    }
    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the CashOut operation 
    //
    //  PARAMS:
    //      - INPUT:
    //          - String HolderName            : Player holder name
    //          - String CardId                : Player account id
    //          - String Trackdata             : Player card trackdata
    //          - Currency Balance             : Total Balance
    //          - TYPE_SPLITS SplitInformation : Split/Company A/B information
    //          - TYPE_CASH_REDEEM CompanyA    : Company/Voucher A parameteres
    //          - TYPE_CASH_REDEEM CompanyB    : Company/Voucher B parameteres
    //          - Int64 OperationId            : Operation id (not need in Preview mode)
    //          - PrintMode Mode               : Print or Preview mode
    //          - SqlTransaction SQLTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static ArrayList CashOut(String[] VoucherAccountInfo
                                  , Int64 AccountId
                                  , Currency BalanceBeforeCashOut
                                  , TYPE_SPLITS SplitInformation
                                  , TYPE_CASH_REDEEM CompanyA
                                  , TYPE_CASH_REDEEM CompanyB
                                  , Int64 OperationId
                                  , PrintMode Mode
                                  , List<VoucherValidationNumber> ValidationNumbers
                                  , PaymentOrder PayOrder
                                  , OperationCode OperationCode
                                  , SqlTransaction Trx
                                  , Boolean IsChipsPurchaseWithCashOut
                                  , Boolean IsApplyTaxes
                                  , Boolean IsTicketPayment)
    {
      ArrayList _voucher_list;
      VoucherCashOut _temp_voucher_cashout;
      VoucherCancel _temp_voucher_cancel;
      VoucherCashOutCompanyB _temp_voucher_cashout_company_b;
      Int32 _voucher_mode;

      _voucher_mode = GeneralParam.GetInt32("Cashier.Voucher", "Mode", 0);

      _voucher_list = new ArrayList();

      // Pay Order
      Barcode _voucher_number;

      _voucher_number = Barcode.Create(BarcodeType.PayOrder, OperationId);

      // Devolution
      _temp_voucher_cashout = new VoucherCashOut(VoucherAccountInfo
                                               , AccountId
                                               , BalanceBeforeCashOut
                                               , SplitInformation
                                               , CompanyA
                                               , CompanyB
                                               , CASHIER_MOVEMENT.DEV_SPLIT1
                                               , Mode
                                               , ValidationNumbers
                                               , _voucher_number.ExternalCode
                                               , PayOrder.CashPayment
                                               , PayOrder.CheckPayment
                                               , OperationCode
                                               , Trx
                                               , IsChipsPurchaseWithCashOut
                                               , IsApplyTaxes
                                               , IsTicketPayment);

      if (SplitInformation.company_a.hide_currency_symbol &&
         !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
      {
        if (Misc.IsVoucherModeCodere()) // CODERE
        {
          // AJQ Always show the currency symbol (The CashOut ticket is not really 'A' it's 'A+B')
        }
        else
        {
          _temp_voucher_cashout.ReplaceCurrencySimbol("");
        }
      }

      if (Mode == PrintMode.Print)
      {
        _temp_voucher_cashout.Save(OperationId, Trx);
      }

      _voucher_list.Add(_temp_voucher_cashout);

      if ((SplitInformation.company_b.devolution_pct > 0)
           || (CompanyB.cancellation > 0))
      {
        // RCI 05-DEC-2012: Multiple cancellation.
        foreach (CancellableOperation _cancellable_operation in CompanyB.cancellable_operations)
        {
          // Don't create cancellation voucher when PromoBalance = 0.
          if (_cancellable_operation.Balance.PromoRedeemable == 0)
          {
            continue;
          }

          // Cancellation of Uso de Sala Voucher.B.Cancel
          _temp_voucher_cancel = new VoucherCancel(VoucherAccountInfo
                                                 , SplitInformation
                                                 , CompanyB
                                                 , _cancellable_operation
                                                 , CASHIER_MOVEMENT.DEV_SPLIT2
                                                 , OperationCode
                                                 , Mode
                                                 , Trx);

          if (SplitInformation.company_b.hide_currency_symbol &&
             !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
          {
            _temp_voucher_cancel.ReplaceCurrencySimbol("");
          }

          if (Mode == PrintMode.Print)
          {
            _temp_voucher_cancel.Save(OperationId, Trx);
          }

          _voucher_list.Add(_temp_voucher_cancel);

        }
      }

      if (Misc.IsCardPlayerToCompanyB() && CompanyB.is_deposit)
      {
        _temp_voucher_cashout_company_b = new VoucherCashOutCompanyB(VoucherAccountInfo
                                               , SplitInformation
                                               , CompanyB
                                               , CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_OUT
                                               , OperationCode
                                               , Mode
                                               , Trx);

        if (SplitInformation.company_b.hide_currency_symbol &&
           !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
        {
          _temp_voucher_cashout_company_b.VoucherHTML = _temp_voucher_cashout_company_b.VoucherHTML.Replace(NumberFormatInfo.CurrentInfo.CurrencySymbol, "");
        }

        if (Mode == PrintMode.Print)
        {
          _temp_voucher_cashout_company_b.Save(OperationId, Trx);
        }

        _voucher_list.Add(_temp_voucher_cashout_company_b);
      }


      if (CompanyB.ServiceCharge > 0 && _voucher_mode != 8)
      {
        VoucherCashIn.InputDetails _details = new VoucherCashIn.InputDetails();

        _details.InitialBalance = MultiPromos.PromoBalance.Zero;
        _details.FinalBalance = MultiPromos.PromoBalance.Zero;
        _details.CashIn1 = 0;
        _details.CashIn2 = CompanyB.ServiceCharge;
        _details.CardPrice = 0;
        _details.CashInTaxSplit1 = 0;
        _details.CashInTaxSplit2 = 0;
        _details.IsChipsPurchaseWithCashOut = IsChipsPurchaseWithCashOut;

        VoucherCashIn _voucher;

        _voucher = new VoucherCashIn(VoucherAccountInfo,
                                     AccountId,
                                     SplitInformation,
                                     _details,
                                     CASHIER_MOVEMENT.SERVICE_CHARGE,
                                     OperationCode,
                                     Mode,
                                     Trx);

        if (SplitInformation.company_b.hide_currency_symbol &&
           !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
        {
          _voucher.ReplaceCurrencySimbol("");
        }

        if (Mode == PrintMode.Print)
        {
          _voucher.Save(OperationId, Trx);
        }

        _voucher_list.Add(_voucher);
      }

      return _voucher_list;

    }// CashOut

    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the promotion lost on CashOut operation 
    //
    //  PARAMS:
    //      - INPUT:
    //          - String HolderName            : Player holder name
    //          - String CardId                : Player account id
    //          - String Trackdata             : Player card trackdata
    //          - Currency Balance             : Total Balance
    //          - TYPE_SPLITS SplitInformation : Split/Company A/B information
    //          - TYPE_CASH_REDEEM CompanyA    : Company/Voucher A parameteres
    //          - TYPE_CASH_REDEEM CompanyB    : Company/Voucher B parameteres
    //          - Int64 OperationId            : Operation id (not need in Preview mode)
    //          - PrintMode Mode               : Print or Preview mode
    //          - SqlTransaction SQLTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static ArrayList PromoLost(String[] VoucherAccountInfo
                               , Int64 AccountId
                               , TYPE_SPLITS SplitInformation
                                , DataTable Promotions
                                , Boolean RewardTitle
                               , Int64 OperationId
                               , PrintMode Mode
                               , SqlTransaction SQLTransaction)
    {
      ArrayList _voucher_list;

      PromoLost(VoucherAccountInfo
                , AccountId
                , SplitInformation
                , true
                , Promotions
                , RewardTitle
                , OperationId
                , Mode
                , SQLTransaction
                , out _voucher_list);

      return _voucher_list;
    }

    public static Boolean PromoLost(String[] VoucherAccountInfo
                                   , Int64 AccountId
                                   , TYPE_SPLITS SplitInformation
                                   , Boolean SingleTicket
                                   , DataTable Promotions
                                   , Boolean IsReward
                                   , Int64 OperationId
                                   , PrintMode Mode
                                   , SqlTransaction SQLTransaction
                                   , out ArrayList VoucherList)
    {
      return PromoLost(VoucherAccountInfo
                , AccountId
                , SplitInformation
                , SingleTicket
                , Promotions
                , IsReward
                , OperationId
                , Mode
                , OperationCode.NOT_SET
                , null
                , SQLTransaction
                , out VoucherList);
    }


    public static Boolean PromoLost(String[] VoucherAccountInfo
                                   , Int64 AccountId
                                   , TYPE_SPLITS SplitInformation
                                   , Boolean SingleTicket
                                   , DataTable Promotions
                                   , Boolean IsReward
                                   , Int64 OperationId
                                   , PrintMode Mode
                                   , OperationCode OperationCode
                                   , String PromotionalTitoTicket
                                   , SqlTransaction SQLTransaction
                                   , out ArrayList VoucherList)
    {
      VoucherPromoLost _voucher;
      DataTable[] _promos;
      DataRow _row;
      DataTable _copy_of_promotions;
      Boolean _hide_prize_cupon_on_promotion;
      Boolean _hide_voucher_hide_cash_desk_draw_winner;
      Boolean _hide_voucher_hide_cash_desk_draw_loser;
      Boolean _hide_voucher_cash_desk_draw_not_selected;

      // RRB 19-MAR-2013: Hide prize coupon
      _hide_prize_cupon_on_promotion = GeneralParam.GetBoolean("Cashier.Voucher", "VoucherOnPromotion.HidePrizeCoupon", false);
      _hide_voucher_hide_cash_desk_draw_winner = GeneralParam.GetBoolean("Cashier.Voucher", "VoucherOnPromotion.HideCashDeskDrawWinner", true);
      _hide_voucher_hide_cash_desk_draw_loser = GeneralParam.GetBoolean("Cashier.Voucher", "VoucherOnPromotion.HideCashDeskDrawLoser", true);

      // DHA 21-JAN-2015: Hide Cash desk draw no participe promotion
      _hide_voucher_cash_desk_draw_not_selected = GeneralParam.GetBoolean("Cashier.Voucher", "VoucherOnPromotion.HideCashDeskDrawNotSelected", false);

      VoucherList = new ArrayList();

      try
      {

        if (!Promotions.Columns.Contains("ACP_TICKET_FOOTER"))
        {
          Promotions.Columns.Add("ACP_TICKET_FOOTER", Type.GetType("System.String"));
        }
        _promos = new DataTable[1];


        _copy_of_promotions = Promotions.Copy();

        _copy_of_promotions.AcceptChanges();
        // If found prize coupon delete datarow
        foreach (DataRow _dr in _copy_of_promotions.Rows)
        {
          switch ((Promotion.PROMOTION_TYPE)_dr["ACP_PROMO_TYPE"])
          {
            case Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_PRIZE_COUPON:
              if (_hide_prize_cupon_on_promotion)
              {
                _dr.Delete();
              }
              break;

            // DHA 21-JAN-2015: Hide Cash desk draw no participe promotion
            case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAYER_SAID_NO:
              if (_hide_voucher_cash_desk_draw_not_selected)
              {
                _dr.Delete();
              }
              break;

            case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE:
            case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR:
            case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR2_IN_KIND:
            case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE_IN_KIND:
              if (_hide_voucher_hide_cash_desk_draw_winner)
              {
                _dr.Delete();
              }
              break;

            case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_RE:
            case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR:
            case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR2_IN_KIND:
              if (_hide_voucher_hide_cash_desk_draw_loser)
              {
                _dr.Delete();
              }
              break;

            case Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAY_SESSION_WINNER_NR_HIDDEN:
              _dr.Delete();
              break;

            default:
              break;
          }
        }
        _copy_of_promotions.AcceptChanges();

        // Avoid create empty voucher
        if (_copy_of_promotions.Rows.Count == 0)
        {
          return true;
        }

        _promos[0] = _copy_of_promotions;

        if (!SingleTicket)
        {
          _promos = new DataTable[_copy_of_promotions.Rows.Count];
        }

        for (int _idx = 0; _idx < _copy_of_promotions.Rows.Count; _idx++)
        {
          String _promo_footer;
          _row = _copy_of_promotions.Rows[_idx];

          if (!GetPromoFooter((long)_row["ACP_PROMO_ID"], SQLTransaction, out _promo_footer))
          {
            return false;
          }

          _row["ACP_TICKET_FOOTER"] = _promo_footer;

          if (!SingleTicket)
          {
            _promos[_idx] = _copy_of_promotions.Clone();
            _promos[_idx].ImportRow(_row);
          }
        }

        foreach (DataTable _table in _promos)
        {
          // Voucher
          _voucher = new VoucherPromoLost(VoucherAccountInfo
                                         , AccountId
                                         , SplitInformation
                                         , Mode
                                         , _table
                                         , IsReward
                                         , SingleTicket
                                         , OperationCode
                                         , PromotionalTitoTicket
                                         , SQLTransaction);

          if ((SplitInformation.company_a.hide_currency_symbol || GeneralParam.GetBoolean("Cashier.Voucher", "VoucherOnPromotion.HideCurrencySymbol")) &&
             !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
          {
            _voucher.ReplaceCurrencySimbol("");
          }

          if (Mode == PrintMode.Print)
          {
            _voucher.Save(OperationId, SQLTransaction);
          }

          VoucherList.Add(_voucher);
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // PromoLost

    private static Boolean GetPromoFooter(long PromoId, SqlTransaction SQLTransaction, out String PromoFooter)
    {
      StringBuilder _sb;

      PromoFooter = "";

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine("SELECT   PM_TICKET_FOOTER                    ");
        _sb.AppendLine("  FROM   PROMOTIONS                          ");
        _sb.AppendLine("  WHERE  (PM_PROMOTION_ID = @pPromoId)       ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SQLTransaction.Connection, SQLTransaction))
        {
          _cmd.Parameters.Add("@pPromoId", SqlDbType.BigInt).Value = PromoId;
          Object _ticket_footer = _cmd.ExecuteScalar();
          PromoFooter = (_ticket_footer == DBNull.Value) ? "" : (String)_ticket_footer;
        }
      }
      catch (Exception)
      {
        return false;
      }

      return true;
    } // PromoLost

    public static String GetColumnHeaderByType(CageCurrencyType CurrencyExchangeType)
    {
      StringBuilder _sb_columns_header;

      _sb_columns_header = new StringBuilder();

      switch (CurrencyExchangeType)
      {
        case CageCurrencyType.ChipsRedimible:
        case CageCurrencyType.ChipsNoRedimible:
          _sb_columns_header.AppendLine("	<tr> ");
          _sb_columns_header.AppendLine("	   <td width='100%' colspan=4><hr noshade size=\"4\"></td> ");
          _sb_columns_header.AppendLine("	</tr> ");
          _sb_columns_header.AppendLine("	<tr>  ");
          _sb_columns_header.AppendLine("	   <td align='left' width='30%'>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_DENOMINATION") + "</td> ");
          _sb_columns_header.AppendLine("	   <td align='right' width='25%'>" + Resource.String("STR_VOUCHER_CASH_CHIPS_QUANTITY") + "</td> ");
          _sb_columns_header.AppendLine("	   <td align='right' width='45%' colspan=2>" + Resource.String("STR_VOUCHER_CASH_CHIPS_SUBTOTAL") + "</td> ");
          _sb_columns_header.AppendLine("	</tr> ");
          _sb_columns_header.AppendLine("	<tr> ");
          _sb_columns_header.AppendLine("	   <td width='100%' colspan=4><hr noshade size=\"2\"></td> ");
          _sb_columns_header.AppendLine("	</tr> ");
          break;

        case CageCurrencyType.ChipsColor:
          _sb_columns_header.AppendLine("	<tr> ");
          _sb_columns_header.AppendLine("	   <td width='100%' colspan=4><hr noshade size=\"4\"></td> ");
          _sb_columns_header.AppendLine("	</tr> ");
          _sb_columns_header.AppendLine("	<tr>  ");
          _sb_columns_header.AppendLine("	   <td align='left' width='34%'>" + Resource.String("STR_VOUCHER_CASH_CHIPS_NAME") + "</td> ");
          _sb_columns_header.AppendLine("	   <td align='left' width='34%'>" + Resource.String("STR_UC_CARD_BTN_PRIZE_DRAW") + "</td> ");
          _sb_columns_header.AppendLine("	   <td align='right' width='32%' colspan=2>" + Resource.String("STR_VOUCHER_CASH_CHIPS_QUANTITY") + "</td> ");
          _sb_columns_header.AppendLine("	</tr> ");
          _sb_columns_header.AppendLine("	<tr> ");
          _sb_columns_header.AppendLine("	   <td width='100%' colspan=4><hr noshade size=\"2\"></td> ");
          _sb_columns_header.AppendLine("	</tr> ");
          break;

        default:
          _sb_columns_header.AppendLine("	<tr> ");
          _sb_columns_header.AppendLine("	   <td width='100%' colspan=4><hr noshade size=\"4\"></td> ");
          _sb_columns_header.AppendLine("	</tr> ");
          _sb_columns_header.AppendLine("	<tr>  ");
          _sb_columns_header.AppendLine("	   <td align='left' width='32%'>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_DENOMINATION") + "&nbsp</td> ");
          _sb_columns_header.AppendLine("	   <td align='center' width='6%'>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE") + "</td> ");
          _sb_columns_header.AppendLine("	   <td align='right' width='24%'>" + Resource.String("STR_VOUCHER_CASH_CHIPS_QUANTITY") + "</td> ");
          _sb_columns_header.AppendLine("	   <td align='right' width='38%'>" + Resource.String("STR_VOUCHER_CASH_CHIPS_SUBTOTAL") + "</td> ");
          _sb_columns_header.AppendLine("	</tr> ");
          _sb_columns_header.AppendLine("	<tr> ");
          _sb_columns_header.AppendLine("	   <td width='100%' colspan=4><hr noshade size=\"2\"></td> ");
          _sb_columns_header.AppendLine("	</tr> ");
          break;

      }
      return _sb_columns_header.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Voucher builder for the Ticket printing operation 
    //
    //  PARAMS :
    //      - INPUT :
    //          - OperationId : Operation id (not needed in Preview mode)
    //          - MrvFontPath: Full path to MRV Font
    //          - Mode : Print or Preview mode
    //          - SQLTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :

    public static ArrayList TicketPrintOut(Int64 OperationId,
                                           String[] VoucherAccountInfo,
                                           Int64 TicketId,
                                           String ValidationNumber,
                                           Currency Amount,
                                           DateTime CreatedDateTime,
                                           DateTime ExpirationDateTime,
                                           Number PromotionId,
                                           String MrvFontPath,
                                           PrintMode Mode,
                                           SqlTransaction SQLTransaction)
    {
      ArrayList _voucher_list;
      VoucherTitoTicketPrintout _temp_voucher;

      _voucher_list = new ArrayList();

      // Gift request
      _temp_voucher = new VoucherTitoTicketPrintout(VoucherAccountInfo,
                                                    TicketId,
                                                    ValidationNumber,
                                                    Amount,
                                                    CreatedDateTime,
                                                    ExpirationDateTime,
                                                    PromotionId,
                                                    MrvFontPath,
                                                    Mode,
                                                    SQLTransaction);

      if (Mode == PrintMode.Print)
      {
        _temp_voucher.Save(OperationId, SQLTransaction);
      }

      _voucher_list.Add(_temp_voucher);

      return _voucher_list;
    } // TicketPrintOut

    //------------------------------------------------------------------------------
    // PURPOSE : Voucher builder for the Ticket Print operation; is a second version
    //           All parameters are stored in instance of class SmartParams, that are
    //           accesibles by name
    //
    //  PARAMS :
    //      - INPUT :
    //          - OperationId : Operation id (not needed in Preview mode)
    //          - TicketId: ID of Ticket to be printed
    //          - ValidationNumber: Ticket validation number
    //          - Amount: amount of ticket
    //          - CreatedDateTime: creation date
    //          - ExpirationDateTime: ticket expiration date
    //          - PromotionId: ID of promotion, if exists
    //          - MrvFontPath: Full path to MRV Font
    //          - Mode : Print or Preview mode
    //          - SQLTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - ArrayList of Voucher properties
    //
    //public static ArrayList TicketPrintOut(SmartParams Params, SqlTransaction SQLTransaction)
    //{
    //  Int64 _operation_Id;
    //  PrintMode _mode;
    //  ArrayList _voucher_list;
    //  VoucherTitoTicketPrintout _temp_voucher;

    //  // Gift request
    //  _mode = (PrintMode)Params.Get("PrintMode");
    //  _temp_voucher = new VoucherTitoTicketPrintout(Params, _mode, SQLTransaction);
    //  if (_mode == PrintMode.Print)
    //  {
    //    _operation_Id = Params.AsLong("OperationId");
    //    _temp_voucher.Save(_operation_Id, SQLTransaction);
    //  }

    //  _voucher_list = new ArrayList();
    //  _voucher_list.Add(_temp_voucher);

    //  return _voucher_list;
    //} // TicketPrintOut

    //------------------------------------------------------------------------------
    // PURPOSE : Voucher builder for the Gift Request operation 
    //
    //  PARAMS :
    //      - INPUT :
    //          - OperationId : Operation id (not needed in Preview mode)
    //          - Mode : Print or Preview mode
    //          - SQLTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :

    public static ArrayList GiftRequest(Int64 OperationId,
                                         String[] VoucherAccountInfo,
                                         Points InitialPoints,
                                         Points SpentPoints,
                                         GIFT_TYPE GiftType,
                                         String GiftNumber,
                                         String GiftName,
                                         Currency NotRedeemableCredits,
                                         Decimal GiftUnits,
                                         DateTime ExpirationDateTime,
                                         PrintMode Mode,
                                         SqlTransaction SQLTransaction)
    {
      ArrayList _voucher_list;
      VoucherGiftRequest _temp_voucher;

      _voucher_list = new ArrayList();

      // Gift request
      _temp_voucher = new VoucherGiftRequest(VoucherAccountInfo,
                                              InitialPoints,
                                              SpentPoints,
                                              GiftType,
                                              GiftName,
                                              GiftNumber,
                                              NotRedeemableCredits,
                                              GiftUnits,
                                              ExpirationDateTime,
                                              Mode,
                                              SQLTransaction);

      if (Mode == PrintMode.Print)
      {
        _temp_voucher.Save(OperationId, SQLTransaction);
      }

      _voucher_list.Add(_temp_voucher);

      return _voucher_list;
    } // GiftRequest

    //------------------------------------------------------------------------------
    // PURPOSE : Voucher builder for the Gift Delivery operation 
    //
    //  PARAMS :
    //      - INPUT :
    //          - OperationId : Operation id (not needed in Preview mode)
    //          - Mode : Print or Preview mode
    //          - SQLTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :

    public static ArrayList GiftDelivery(Int64 OperationId,
                                          String[] VoucherAccountInfo,
                                          String GiftNumber,
                                          String GiftName,
                                          PrintMode Mode,
                                          SqlTransaction SQLTransaction)
    {
      ArrayList _voucher_list;
      VoucherGiftDelivery _temp_voucher;

      _voucher_list = new ArrayList();

      // Gift request
      _temp_voucher = new VoucherGiftDelivery(VoucherAccountInfo,
                                               GiftNumber,
                                               GiftName,
                                               Mode,
                                               SQLTransaction);

      if (Mode == PrintMode.Print)
      {
        _temp_voucher.Save(OperationId, SQLTransaction);
      }

      _voucher_list.Add(_temp_voucher);

      return _voucher_list;
    } // GiftRequest

    //------------------------------------------------------------------------------
    // PURPOSE : Voucher builder for the Gift cancellation operation 
    //
    //  PARAMS :
    //      - INPUT :
    //          - OperationId : Operation id (not needed in Preview mode)
    //          - Mode : Print or Preview mode
    //          - SQLTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :

    public static ArrayList GiftCancellation(Int64 OperationId,
                                             String[] VoucherAccountInfo,
                                             Points InitialPoints,
                                             Points SpentPoints,
                                             GIFT_TYPE GiftType,
                                             String GiftNumber,
                                             String GiftName,
                                             Currency NotRedeemableCredits,
                                             Decimal GiftUnits,
                                             PrintMode Mode,
                                             SqlTransaction SQLTransaction)
    {
      ArrayList _voucher_list;
      VoucherGiftCancellation _temp_voucher;

      _voucher_list = new ArrayList();

      // Gift cancellation
      _temp_voucher = new VoucherGiftCancellation(VoucherAccountInfo,
                                                  InitialPoints,
                                                  SpentPoints,
                                                  GiftType,
                                                  GiftNumber,
                                                  GiftName,
                                                  NotRedeemableCredits,
                                                  GiftUnits,
                                                  Mode,
                                                  SQLTransaction);

      if (Mode == PrintMode.Print)
      {
        _temp_voucher.Save(OperationId, SQLTransaction);
      }

      _voucher_list.Add(_temp_voucher);

      return _voucher_list;
    } // GiftCancellation

    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the Generate PIN operation 
    //
    //  PARAMS:
    //      - INPUT:
    //          - String HolderName     : Player holder name
    //          - String CardId         : Player account id
    //          - String Trackdata      : Player card trackdata
    //          - Int64 OperationId     : Operation id (not need in Preview mode)
    //          - PrintMode Mode        : Print or Preview mode
    //          - SqlTransaction SQLTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static ArrayList GeneratePIN(String[] VoucherAccountInfo
                                      , Int64 AccountId
                                      , TYPE_SPLITS SplitInformation
                                      , Int64 OperationId
                                      , String NewPin
                                      , PrintMode Mode
                                      , SqlTransaction SQLTransaction)
    {
      ArrayList _voucher_list;
      VoucherGeneratePIN _temp_voucher;

      _voucher_list = new ArrayList();

      // Voucher for Cash In Deposit
      _temp_voucher = new VoucherGeneratePIN(VoucherAccountInfo
                                            , AccountId
                                            , SplitInformation
                                            , NewPin
                                            , Mode
                                            , SQLTransaction);

      if (Mode == PrintMode.Print)
      {
        _temp_voucher.Save(OperationId, SQLTransaction);
      }

      _voucher_list.Add(_temp_voucher);

      return _voucher_list;
    } // GeneratePIN

    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the Generate PIN operation 
    //
    //  PARAMS:
    //      - INPUT:
    //          - String HolderName     : Player holder name
    //          - String CardId         : Player account id
    //          - String Trackdata      : Player card trackdata
    //          - Int64 OperationId     : Operation id (not need in Preview mode)
    //          - PrintMode Mode        : Print or Preview mode
    //          - SqlTransaction SQLTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static ArrayList RechargesSummary(String[] VoucherAccountInfo,
                                             String[] NotesList,
                                             Currency TotalRecharges,
                                             Int64 AccountId,
                                             Int64 OperationId,
                                             PrintMode Mode,
                                             SqlTransaction SQLTransaction)
    {
      ArrayList _voucher_list;
      VoucherRechargesSummary _temp_voucher;

      _voucher_list = new ArrayList();

      // Voucher for Recharges Summary
      _temp_voucher = new VoucherRechargesSummary(VoucherAccountInfo,
                                                  NotesList,
                                                  TotalRecharges,
                                                  AccountId,
                                                  Mode,
                                                  SQLTransaction);

      if (Mode == PrintMode.Print)
      {
        _temp_voucher.Save(OperationId, SQLTransaction);
      }

      _voucher_list.Add(_temp_voucher);

      return _voucher_list;
    } // RechargesSummary

    //------------------------------------------------------------------------------
    // PURPOSE : Voucher builder for the Currency Exchange Operation
    //
    //  PARAMS :
    //      - INPUT :
    //          - OperationId : Operation id (not needed in Preview mode)
    //          - Mode : Print or Preview mode
    //          - SQLTransaction
    //
    //      - OUTPUT :
    //
    // RETURNS :
    // 
    //   NOTES :
    public static ArrayList CurrencyExchange(String[] VoucherAccountInfo
                               , Int64 AccountId
                               , TYPE_SPLITS SplitInformation
                               , CurrencyExchangeResult ExchangeResult
                               , CurrencyExchangeType ExchangeType
                               , String NationalCurrency
                               , Int64 OperationId
                               , PrintMode Mode
                               , SqlTransaction Trx)
    {
      return CurrencyExchange(VoucherAccountInfo, AccountId, SplitInformation, ExchangeResult, ExchangeType, NationalCurrency, OperationId, OperationCode.CASH_IN, Mode, Trx);
    }

    public static ArrayList CurrencyExchange(String[] VoucherAccountInfo
                                   , Int64 AccountId
                                   , TYPE_SPLITS SplitInformation
                                   , CurrencyExchangeResult ExchangeResult
                                   , CurrencyExchangeType ExchangeType
                                   , String NationalCurrency
                                   , Int64 OperationId
                                   , OperationCode Operation
                                   , PrintMode Mode
                                   , SqlTransaction Trx)
    {
      ArrayList _voucher_list;
      VoucherCardAddCurrencyExchange _temp_voucher;
      Boolean _hide_currency_exchange;

      _voucher_list = new ArrayList();

      switch (ExchangeType)
      {
        case CurrencyExchangeType.CARD:
          switch (Operation)
          {
            case OperationCode.CASH_IN:
              _hide_currency_exchange = GeneralParam.GetBoolean("Cashier.Voucher", "PaymentMethod.BankCard.HideVoucher") || Misc.IsVoucherModeTV();
              break;

            case OperationCode.CASH_ADVANCE:
              _hide_currency_exchange = GeneralParam.GetBoolean("Cashier.Voucher", "PaymentMethod.CashAdvance.BankCard.HideVoucher");
              break;

            default:
              _hide_currency_exchange = false;
              break;
          }
          break;

        case CurrencyExchangeType.CHECK:
          switch (Operation)
          {
            case OperationCode.CASH_IN:
              _hide_currency_exchange = GeneralParam.GetBoolean("Cashier.Voucher", "PaymentMethod.Check.HideVoucher");
              break;

            case OperationCode.CASH_ADVANCE:
              _hide_currency_exchange = GeneralParam.GetBoolean("Cashier.Voucher", "PaymentMethod.CashAdvance.Check.HideVoucher");
              break;

            default:
              _hide_currency_exchange = false;
              break;
          }
          break;

        case CurrencyExchangeType.CURRENCY:
          _hide_currency_exchange = GeneralParam.GetBoolean("Cashier.Voucher", "PaymentMethod.Currency.HideVoucher");
          break;
        case CurrencyExchangeType.CREDITLINE:
          _hide_currency_exchange = true; // No voucher showed
          break;

        default:
          _hide_currency_exchange = false;
          break;
      }

      if (_hide_currency_exchange)
      {
        return _voucher_list;
      }

      // Currency Exchange
      _temp_voucher = new VoucherCardAddCurrencyExchange(VoucherAccountInfo
                                                         , AccountId
                                                         , SplitInformation
                                                         , ExchangeResult
                                                         , ExchangeType
                                                         , NationalCurrency
                                                         , Operation
                                                         , Mode
                                                         , false
                                                         , Trx
                                                         , OperationId);

      if (_temp_voucher.Show)
      {
        // DLL & RCI 21-AUG-2013: Hide currency (if configured) for bank card recharge.
        if (SplitInformation.company_a.hide_currency_symbol &&
            !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
        {
          _temp_voucher.ReplaceCurrencySimbol("");
        }

        if (Mode == PrintMode.Print)
        {
          _temp_voucher.Save(OperationId, Trx);
        }

        _voucher_list.Add(_temp_voucher);
      }

      // OPC: Add comission voucher just for Cash advance.
      if (Misc.IsCommissionToCompanyBEnabled() && ExchangeResult.Comission >= 0 && Operation == OperationCode.CASH_ADVANCE)
      {
        VoucherCardAddCurrencyExchange _temp;

        _temp = new VoucherCardAddCurrencyExchange(VoucherAccountInfo
                                                   , AccountId
                                                   , SplitInformation
                                                   , ExchangeResult
                                                   , ExchangeType
                                                   , NationalCurrency
                                                   , Operation
                                                   , Mode
                                                   , true
                                                   , Trx
                                                   , OperationId);

        if (SplitInformation.company_b.hide_currency_symbol &&
          !String.IsNullOrEmpty(NumberFormatInfo.CurrentInfo.CurrencySymbol))
        {
          _temp.ReplaceCurrencySimbol("");
        }

        _voucher_list.Add(_temp);

        if (Mode == PrintMode.Print)
        {
          _temp.Save(OperationId, Trx);
        }
      }

      return _voucher_list;
    } // CurrencyExchange

    /// <summary>
    /// Voucher builder for the Undo Operations
    /// </summary>
    /// <param name="OperationIdToUndo">Operation to undo</param>
    /// <param name="CodeOperationToUndo">CodeOperation to undo</param>
    /// <param name="OperationId">Operation that undoes</param>
    /// <param name="CodeOperation">CodeOperation that undoes</param>
    /// <param name="Mode"></param>
    /// <param name="Trx"></param>
    /// <returns>ArrayList with undo operation vouchers</returns>
    public static ArrayList UndoOperation(Int64 OperationIdToUndo
                                        , OperationCode CodeOperationToUndo
                                        , Int64 OperationId
                                        , OperationCode CodeOperation
                                        , PrintMode Mode
                                        , SqlTransaction Trx)
    {
      DataTable _dt_vouchers;
      Voucher _temp_voucher;
      ArrayList _voucher_list;
      String _voucher_html;

      _voucher_list = new ArrayList();
      _dt_vouchers = VoucherManager.GetVouchersFromId(VoucherSource.FROM_OPERATION, OperationIdToUndo, Trx);

      foreach (DataRow _voucher in _dt_vouchers.Rows)
      {
        _voucher_html = (String)_voucher["CV_HTML"];
        _temp_voucher = new VoucherUndoOperation(_voucher_html, CodeOperationToUndo, CodeOperation, Mode, Trx);
        _temp_voucher.SetValue("CV_TYPE", _voucher["CV_TYPE"]);
        _temp_voucher.SetValue("CV_SEQUENCE", _voucher["CV_SEQUENCE"]);
        _temp_voucher.SetValue("CV_M01_DEV", (_voucher["CV_M01_DEV"] == System.DBNull.Value ? (Decimal)0 : -(System.Decimal)_voucher["CV_M01_DEV"]));
        _temp_voucher.SetValue("CV_M01_BASE", (_voucher["CV_M01_BASE"] == System.DBNull.Value ? (Decimal)0 : -(System.Decimal)_voucher["CV_M01_BASE"]));
        _temp_voucher.SetValue("CV_M01_TAX1", (_voucher["CV_M01_TAX1"] == System.DBNull.Value ? (Decimal)0 : -(System.Decimal)_voucher["CV_M01_TAX1"]));
        _temp_voucher.SetValue("CV_M01_TAX2", (_voucher["CV_M01_TAX2"] == System.DBNull.Value ? (Decimal)0 : -(System.Decimal)_voucher["CV_M01_TAX2"]));
        _temp_voucher.SetValue("CV_M01_FINAL", (_voucher["CV_M01_FINAL"] == System.DBNull.Value ? (Decimal)0 : -(System.Decimal)_voucher["CV_M01_FINAL"]));

        if (Mode == PrintMode.Print)
        {
          _temp_voucher.Save(OperationId, Trx);
        }

        _voucher_list.Add(_temp_voucher);
      } // foreach

      return _voucher_list;
    } // UndoOperation

    public static ArrayList PointToCardReplacement(Int64 OperationId,
                                                   String[] VoucherAccountInfo,
                                                   Points InitialPoints,
                                                   Points SpentPoints,
                                                   PrintMode Mode,
                                                   SqlTransaction SQLTransaction)
    {
      ArrayList _voucher_list;
      VoucherPointToCardReplacement _temp_voucher;

      _voucher_list = new ArrayList();

      // PointToCardReplacement
      _temp_voucher = new VoucherPointToCardReplacement(VoucherAccountInfo,
                                                        InitialPoints,
                                                        SpentPoints,
                                                        Mode,
                                                        SQLTransaction);

      if (Mode == PrintMode.Print)
      {
        _temp_voucher.Save(OperationId, SQLTransaction);
      }

      _voucher_list.Add(_temp_voucher);

      return _voucher_list;
    } // PointToCardReplacement

    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the Card player replacement
    //
    //  PARAMS:
    //      - INPUT:
    //              - OperationId
    //              - ParamsReplacement
    //              - OpCode
    //              - Mode
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //       - ArrayList with replacement voucher and if it's required the voucher with company b comissions
    // 
    public static ArrayList CardPlayerReplacement(Int64 OperationId
                                                , TYPE_CARD_REPLACEMENT ParamsReplacement
                                                , OperationCode OpCode
                                                , PrintMode Mode
                                                , SqlTransaction Trx
                                                , Boolean CardPaid = true)
    {
      ArrayList _voucher_list;
      VoucherCashIn _temp_voucher_cash_in;
      VoucherCardReplacement _temp_voucher_replacement;
      VoucherCashIn.InputDetails _details;
      CASHIER_MOVEMENT _cashier_movement;

      _voucher_list = new ArrayList();

      // Voucher Card Replacement 
      // RAB 24-MAR-2016: Bug 10745: Add input parameter CardPaid
      _temp_voucher_replacement = new VoucherCardReplacement(Mode, ParamsReplacement, Trx, CardPaid);

      if (Mode == PrintMode.Print)
      {
        _temp_voucher_replacement.Save(OperationId, Trx);
      }

      _voucher_list.Add(_temp_voucher_replacement);

      // Only when card player is send to company B and comission greather than 0
      // Voucher Cash IN comission company B
      if (Misc.IsCardPlayerToCompanyB() && ParamsReplacement.ExchangeResult.Comission > 0)
      {
        _details = new VoucherCashIn.InputDetails();
        _details.ExchangeCommission = ParamsReplacement.ExchangeResult.Comission;
        _details.InitialBalance = MultiPromos.PromoBalance.Zero;
        _details.FinalBalance = MultiPromos.PromoBalance.Zero;

        switch (ParamsReplacement.ExchangeResult.InType)
        {
          case CurrencyExchangeType.CARD:
            _cashier_movement = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT2;
            break;

          case CurrencyExchangeType.CHECK:
            _cashier_movement = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT2;
            break;

          case CurrencyExchangeType.CURRENCY:
            _cashier_movement = CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT2;
            break;

          default:
            _cashier_movement = CASHIER_MOVEMENT.CARD_REPLACEMENT;
            break;
        }

        _temp_voucher_cash_in = new VoucherCashIn(ParamsReplacement.AccountInfo
                                                 , ParamsReplacement.account_id
                                                 , ParamsReplacement.Splits
                                                 , _details
                                                 , _cashier_movement
                                                 , OpCode
                                                 , Mode
                                                 , Trx);
        if (Mode == PrintMode.Print)
        {
          _temp_voucher_cash_in.Save(OperationId, Trx);
        }

        _voucher_list.Add(_temp_voucher_cash_in);
      }

      return _voucher_list;
    }

    public static ArrayList VoucherReception(String[] VoucherAccountInfo
      , Int64 AccountId
      , CashierVoucherType VoucherType
      , Currency[] Amount
      , CurrencyExchangeResult ExchangeResult
      , String Coupon
      , Int32 ValidGamingDays
      , Boolean Agreement
      , PrintMode Mode
      , JunketsBusinessLogic BusinessLogic
      , Int64 OperationId
      , IDbTransaction IdbTransaction)
    {
      return VoucherReception(VoucherAccountInfo
        , AccountId
        , VoucherType
        , Amount
        , ExchangeResult
        , Coupon
        , ValidGamingDays
        , Agreement
        , Mode
        , BusinessLogic
        , OperationId
        , IdbTransaction as SqlTransaction);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Voucher builder for the CashIn operation 
    //
    //  PARAMS:
    //      - INPUT:
    //          - String HolderName     : Player holder name
    //          - String CardId         : Player account id
    //          - String Trackdata      : Player card trackdata
    //          - Currency CurrentBalance : Actual Balance of the card
    //          - Currency CashIn1      : Amount to pay as Deposit
    //          - Currency CashIn2      : Amount to pay as Uso de sala
    //          - Int64 OperationId     : Operation id (not need in Preview mode)
    //          - PrintMode Mode        : Print or Preview mode
    //          - SqlTransaction SQLTransaction
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    //   NOTES:
    // 
    public static ArrayList VoucherReception(String[] VoucherAccountInfo
                                             , Int64 AccountId
                                             , CashierVoucherType VoucherType
                                             , Currency[] Amount
                                             , CurrencyExchangeResult ExchangeResult
                                             , String Coupon
                                             , Int32 ValidGamingDays
                                             , Boolean Agreement
                                             , PrintMode Mode
                                             , JunketsBusinessLogic BusinessLogic
                                             , Int64 OperationId
                                             , SqlTransaction SQLTransaction)
    {
      ArrayList _voucher_list;
      VoucherReception _temp_voucher_reception;
      VoucherReceptionLOPD _temp_voucher_reception_lopd;

      _voucher_list = new ArrayList();
      _temp_voucher_reception_lopd = null;

      // Voucher for reception
      _temp_voucher_reception = new VoucherReception(VoucherAccountInfo, VoucherType, AccountId, Amount, ExchangeResult, Coupon, ValidGamingDays, Mode, SQLTransaction);
      if (Mode == PrintMode.Print)
      {
        _temp_voucher_reception.OperationCode = OperationCode.CUSTOMER_ENTRANCE;
        _temp_voucher_reception.Save(OperationId, SQLTransaction);
      }
      _voucher_list.Add(_temp_voucher_reception);

      // Voucher for reception LOPD
      if (Agreement)
      {
        _temp_voucher_reception_lopd = new VoucherReceptionLOPD(VoucherAccountInfo, VoucherType, AccountId, Mode, SQLTransaction);

        if (Mode == PrintMode.Print)
        {
          _temp_voucher_reception_lopd.OperationCode = OperationCode.CUSTOMER_ENTRANCE;
          _temp_voucher_reception_lopd.Save(OperationId, SQLTransaction);
        }

        _voucher_list.Add(_temp_voucher_reception_lopd);
      }

      if (BusinessLogic != null && BusinessLogic.PrintVoucher)
      {
        VoucherJunket _junket_voucher;

        _junket_voucher = new VoucherJunket(VoucherAccountInfo,
                                            BusinessLogic.TitleVoucher,
                                            BusinessLogic.TextVoucher,
                                            BusinessLogic.FlyerCode,
                                            OperationId,
                                            Mode,
                                            SQLTransaction);

        if (Mode == PrintMode.Print)
        {
          _junket_voucher.OperationCode = OperationCode.CUSTOMER_ENTRANCE;
          _junket_voucher.Save(OperationId, SQLTransaction);
        }

        _voucher_list.Add(_junket_voucher);
      }

      return _voucher_list;
    } // VoucherReception

    public static void GetVoucherCreditLineGetMarker(ArrayList VoucherList
                                                    , String[] VoucherAccountInfo
                                                    , Currency CreditGiven
                                                    , CreditLine CreditLineDetails
                                                    , TYPE_SPLITS SplitInformation
                                                    , Int64 OperationId
                                                    , PrintMode Mode
                                                    , SqlTransaction SQLTransaction)
    {

      VoucherGetMarker _temp_voucher;

      _temp_voucher = new VoucherGetMarker(VoucherAccountInfo, CreditGiven, CreditLineDetails, SplitInformation, OperationId, Mode, SQLTransaction);

      VoucherList.Add(_temp_voucher);

      if (Mode == PrintMode.Print)
      {
        _temp_voucher.Save(OperationId, SQLTransaction);
      }

    }

    public static Boolean GetVoucherCashOutReceipt(ArrayList VoucherList
                                              , eCashOutReceiptData CashOutReceipt
                                              , Int64 OperationId
                                              , PrintMode Mode
                                              , SqlTransaction SQLTransaction
                                              , out Int64 VoucherId)
    {
      VoucherCashOutReceipt _temp_voucher;

      VoucherId = -1;

      try
      {
        _temp_voucher = new VoucherCashOutReceipt(CashOutReceipt, OperationId, Mode, SQLTransaction);

        VoucherList.Add(_temp_voucher);

        if (Mode == PrintMode.Print)
        {
          _temp_voucher.SetValue("CV_TYPE", CashierVoucherType.AutoPrintVoucherCashOut);
          _temp_voucher.SetValue("CV_SEQUENCE", CashOutReceipt.ReceiptNumber);

          if (!_temp_voucher.Save(OperationId, SQLTransaction))
          {
            return false;
          }

          VoucherId = _temp_voucher.VoucherId;
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    }

    public static void GetVoucherTerminalDraw(String[] VoucherAccountInfo
                                              , Int64 OperationId
                                              , RechargeInputParameters InputParams
                                              , RechargeOutputParameters OutputParams
                                              , PrintMode Mode
                                              , Boolean IsWinner
                                              , Boolean LoserVoucher
                                              , SqlTransaction SQLtransaction)
    {
      String _numbers_played;
      String _numbers_drawn;
      Currency _amount_bet;
      Currency _amount_won;
      StringBuilder _sb;
      DataTable _voucher_info = new DataTable();
      String _cashier_name;
      String _terminal;
      String _client;
      String _client_card;
      VoucherTerminalDraw _temp_voucher;

      _cashier_name = "";
      _terminal = "";
      _client = "";
      _client_card = "";


      _sb = new StringBuilder();

      _sb.AppendLine("     SELECT   TOP (1)");
      _sb.AppendLine("              CM_USER_NAME");
      _sb.AppendLine("            , CM_CASHIER_NAME");
      _sb.AppendLine("            , AC_HOLDER_NAME");
      _sb.AppendLine("            , TE_NAME");
      _sb.AppendLine("            , DBO.TRACKDATATOEXTERNAL(AC_TRACK_DATA) AS CARD_NUMBER");
      _sb.AppendLine("       FROM   CASHIER_MOVEMENTS CM");
      _sb.AppendLine(" INNER JOIN   ACCOUNTS ");
      _sb.AppendLine("         ON   AC_ACCOUNT_ID = CM_ACCOUNT_ID");
      _sb.AppendLine(" INNER JOIN   PLAY_SESSIONS");
      _sb.AppendLine("         ON   AC_ACCOUNT_ID = PS_ACCOUNT_ID");
      _sb.AppendLine(" INNER JOIN   TERMINALS");
      _sb.AppendLine("         ON   PS_TERMINAL_ID  = TE_TERMINAL_ID");
      _sb.AppendLine("      WHERE   CM_OPERATION_ID = @pOperationId AND PS_STATUS = 0");
      _sb.AppendLine("   ORDER BY   PS_PLAY_SESSION_ID DESC");

      using (DB_TRX _db_trx = new DB_TRX())
      {
        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction))
        {
          _sql_cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;


          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            _sql_da.Fill(_voucher_info);
          }
        }

        if (_voucher_info.Rows.Count > 0)
        {
          _cashier_name = _voucher_info.Rows[0]["CM_USER_NAME"].ToString();
          _terminal = _voucher_info.Rows[0]["TE_NAME"].ToString();
          _client = _voucher_info.Rows[0]["AC_HOLDER_NAME"].ToString();
          _client_card = _voucher_info.Rows[0]["CARD_NUMBER"].ToString();
        }
      }

      _numbers_drawn = FormatNumbers(OutputParams.CashDeskDrawResult.m_winning_combination);
      _numbers_played = FormatNumbers(OutputParams.CashDeskDrawResult.m_bet_numbers);
      _amount_bet = OutputParams.CashDeskDrawResult.m_participation_amount;
      _amount_won = OutputParams.CashDeskDrawResult.m_re_awarded;

      _temp_voucher = new VoucherTerminalDraw(OutputParams.VoucherList, VoucherAccountInfo, OperationId
                                              , _cashier_name, _terminal, _client
                                              , _numbers_played, _numbers_drawn, _amount_bet, _amount_won, Mode
                                              , OutputParams, _client_card, InputParams.AccountId, IsWinner, LoserVoucher, SQLtransaction);

      OutputParams.VoucherList.Add(_temp_voucher);

      if (Mode == PrintMode.Print)
      {
        _temp_voucher.Save(OperationId, SQLtransaction);
      }

    }

    public static string FormatNumbers(List<int> List)
    {
      String _str;

      _str = "";
      foreach (int _number in List)
      {
        _str += _number.ToString("00") + " ";
      }

      _str = _str.TrimEnd('-');
      return _str;
    }

    public static void GetVoucherCreditLinePayback(ArrayList VoucherList
                                                   , String[] VoucherAccountInfo
                                                   , Currency CreditGiven
                                                   , CreditLine CreditLineDetails
                                                   , TYPE_SPLITS SplitInformation
                                                   , Int64 OperationId
                                                   , PrintMode Mode
                                                   , SqlTransaction SQLTransaction)
    {

      VoucherPaybackMarker _temp_voucher;

      _temp_voucher = new VoucherPaybackMarker(VoucherAccountInfo, CreditGiven, CreditLineDetails, SplitInformation, OperationId, Mode, SQLTransaction);

      VoucherList.Add(_temp_voucher);

      if (Mode == PrintMode.Print)
      {
        _temp_voucher.Save(OperationId, SQLTransaction);
      }

    }

  } // VoucherBuilder

}
