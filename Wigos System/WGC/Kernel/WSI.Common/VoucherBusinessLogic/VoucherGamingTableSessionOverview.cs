﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherGamingTableSessionOverview.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
// 10-AUG-2016  RAB         Product Backlog Item 15172: GamingTables (Phase 4): Cashier - Adapt report screen in Gaming Tables
// 22-FEB-2018  AGS         Bug 31566:WIGOS-8129 [Ticket #12342] Resumen de Caja Homologación de Nombres y Montos Sección Fichas V03.06.0035
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherGamingTableSessionOverview : Voucher
  {
    #region Class Attributes

    public class ParametersGamingTableSessionOverview
    {
      public Color BackColor;

      public ParametersGamingTableSessionOverview()
      {
        BackColor = Color.Transparent;
      }
    }
    #endregion

    #region Constructor

    public VoucherGamingTableSessionOverview(WSI.Common.GamingTable GamingTable
                                 , PrintMode Mode
                                 , ParametersGamingTableSessionOverview ParametersOverview
                                  )
      : base(Mode, null)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.


      // 1. Load HTML structure.
      LoadVoucher("VoucherGamingTableSessionInfo");

      // 2. Load NLS strings from resource.
      LoadVoucherResources(GamingTable, ParametersOverview);

      // 4. Load specific voucher data.
      LoadVoucherData(GamingTable);

    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(WSI.Common.GamingTable GamingTable, ParametersGamingTableSessionOverview ParametersOverview)
    {
      String _value;

      // Fill IN - Initial 
      _value = Resource.String("STR_GAMING_TABLE_REPORT_INITIAL_AMOUNT");
      AddString("STR_INITIAL_AMOUNT", _value);

      // Fill IN - Fills 
      _value = Resource.String("STR_UC_BANK_BTN_GAMING_TABLE_DEPOSIT");
      AddString("STR_TOTAL_FILL_IN", _value);

      // Credits
      _value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CHIPS_CASH_OUT");
      AddString("STR_TOTAL_WITHDRAW", _value);

      // Drop
      _value = Resource.String("STR_GAMING_TABLE_REPORT_DROP");
      AddString("STR_TOTAL_DROP", _value);

      // Session Name
      _value = GamingTable.GamingTableName;
      AddString("SESSION_NAME", _value);

      // Session Date Opening
      _value = GamingTable.CashierSessionOpening.ToString("dd/MMM/yyyy HH:mm");
      AddString("SESSION_DATE_OPEN", _value);

      SetParameterValue("@VOUCHER_STYLE", "BODY{background-color:'" + ColorTranslator.ToHtml(ParametersOverview.BackColor) + "';}");


    } // LoadVoucherResources

    /// <summary>
    /// Create cell with value
    /// </summary>
    private String GetHtmlStringWithValue(String Value)
    {
      String _str_currency;

      _str_currency = string.Empty;

      _str_currency = @"<td class=""left"">";
      _str_currency += Value;
      _str_currency += @"</td>";

      return _str_currency;
    } // GetHtmlStringWithValue

    #endregion

    #region Public Methods

    /// <summary>
    /// Load Personalized Data into voucher
    /// </summary>
    public void LoadVoucherData(WSI.Common.GamingTable GamingTable)
    {
      String _str;
      int _idx;
      int _num_currencies;
      String[] _currency_column;
      String _final_currency_column_name;
      Int32 _num_rows_dt;

      _str = string.Empty;
      _currency_column = new String[] { };
      _final_currency_column_name = string.Empty;
      _num_rows_dt = 0;

      // Decrease 1 more for Headers Rows
      _num_currencies = GamingTable.CashierSessionSummary.Columns.Count - 1;
      _num_rows_dt = GamingTable.CashierSessionSummary.Rows.Count;

      // Currency Headers
      for (_idx = 0; _idx < _num_currencies; _idx++)
      {
        //Literal Chips
        if (GamingTable.CashierSessionSummary.Columns[_idx + 1].ColumnName == Cage.CHIPS_ISO_CODE.ToString())
        {
          _str += GetHtmlStringWithValue(Resource.String("STR_CURRENCY_TYPE_0_ISO_X01"));
        }
        else
        {
          _currency_column = GetTypeAndTypeFromHeaderColumn(GamingTable.CashierSessionSummary, _idx + 1);
          _final_currency_column_name = FeatureChips.GetChipTypeDescription((CurrencyExchangeType)Convert.ToInt32(_currency_column[1]), _currency_column[0]);

          if(String.IsNullOrEmpty(_final_currency_column_name))
          {
            _final_currency_column_name = Resource.String("STR_REFUND_CASH") + " (" + _currency_column[0]  + ")";
          }
         
          _str += GetHtmlStringWithValue(_final_currency_column_name);
        }

      }
      SetParameterValue("@STR_HEADERS_ISO_CODE", _str);
      _str = string.Empty;

      // Fill in Rows
      for (_idx = 0; _idx < _num_currencies; _idx++)
      {
        _str += GetHtmlStringWithValue(String.Empty);

      }
      SetParameterValue("@STR_ROWS_FILL_IN", _str);
      _str = string.Empty;

      // Currency Initial Amount
      if(_num_rows_dt != 0)
      {
        for (_idx = 0; _idx < _num_currencies; _idx++)
        {
          _str += FillSummaryColumns(GamingTable.CashierSessionSummary, 0, _idx + 1);
        }
        SetParameterValue("@STR_VALUE_INITIAL_FILL_IN", _str);
        _str = string.Empty;
        _num_rows_dt = _num_rows_dt - 1;
      }
      else
      {
        SetParameterValue("@STR_VALUE_INITIAL_FILL_IN", GetHtmlStringWithValue(Currency.Format((Decimal)0, "")));
      }

      // Currency Total Fill IN 
      if(_num_rows_dt != 0)
      {
        for (_idx = 0; _idx < _num_currencies; _idx++)
        {
          _str += FillSummaryColumns(GamingTable.CashierSessionSummary, 1, _idx + 1);
        }
        SetParameterValue("@STR_VALUE_TOTAL_FILL_IN", _str);
        _str = string.Empty;
        _num_rows_dt = _num_rows_dt - 1;
      }
      else
      {
        SetParameterValue("@STR_VALUE_TOTAL_FILL_IN", GetHtmlStringWithValue(Currency.Format((Decimal)0, "")));
      }

      // Currency Total Withdraw
      if (_num_rows_dt != 0)
      {
        for (_idx = 0; _idx < _num_currencies; _idx++)
        {
          _str += FillSummaryColumns(GamingTable.CashierSessionSummary, 2, _idx + 1);
        }
        SetParameterValue("@STR_VALUE_TOTAL_WITHDRAW", _str);
        _str = string.Empty;
        _num_rows_dt = _num_rows_dt - 1;
      }
      else
      {
        SetParameterValue("@STR_VALUE_TOTAL_WITHDRAW", GetHtmlStringWithValue(Currency.Format((Decimal)0, "")));
      }

      // Currency Drop
      SetParameterValue("@STR_VALUE_TOTAL_DROP", GetHtmlStringWithValue(Currency.Format((Decimal)GamingTable.CashierSessionDrop, "")));

    } //LoadVoucherData

    /// <summary>
    /// Concatenates iso code with currency type according to value of header retrieved from the DataTable
    /// </summary>
    /// <param name="CashierSessionSummary">DataTable where data are</param>
    /// <param name="IdxColumn">Index of specific column</param>
    /// <param name="SplitChar">Character of split. Separation of iso code and currency type</param>
    /// <param name="ReplaceString">String of replace. Remove the '.' from the beginning when it's of the chip type</param>
    /// <returns></returns>
    private String[] GetTypeAndTypeFromHeaderColumn(DataTable CashierSessionSummary, Int32 IdxColumn, Char SplitChar = ' ', String ReplaceString = ".")
    {
      String[] _currency_column;
      _currency_column = new String[] { };

      _currency_column = CashierSessionSummary.Columns[IdxColumn].ColumnName.Split(SplitChar);
      _currency_column[0] = _currency_column[0].Replace(ReplaceString, "");

      return _currency_column;
    }

    /// <summary>
    /// Fill in the columns according to the datatable received by parameter. Complete the HTML to then show it.
    /// </summary>
    /// <param name="CashierSessionSummary">DataTable where data are</param>
    /// <param name="IdxRow">Index of specific row</param>
    /// <param name="IdxItemArray">Index of specific item array</param>
    /// <returns></returns>
    private String FillSummaryColumns(DataTable CashierSessionSumary, Int32 IdxRow, Int32 IdxItemArray)
    {
      String _str;
      String[] _currency_column;
      Boolean _is_chip_color;

      _str = string.Empty;
      _currency_column = new String[] { };
      _is_chip_color = false;

      _currency_column = GetTypeAndTypeFromHeaderColumn(CashierSessionSumary, IdxItemArray);
      _is_chip_color = (CurrencyExchangeType)Convert.ToInt32(_currency_column[1]) == CurrencyExchangeType.CASINO_CHIP_COLOR;

      if (CashierSessionSumary.Rows[IdxRow].ItemArray[IdxItemArray].ToString() == String.Empty)
      {
        if (_is_chip_color)
        {
          _str += GetHtmlStringWithValue("0");
        }
        else
        {
          _str += GetHtmlStringWithValue(Currency.Format((Decimal)0, ""));
        }
      }
      else
      {
        if (_is_chip_color)
        {
          _str += GetHtmlStringWithValue(Convert.ToInt32(CashierSessionSumary.Rows[IdxRow].ItemArray[IdxItemArray]).ToString());
        }
        else
        {
          _str += GetHtmlStringWithValue(Currency.Format((Decimal)CashierSessionSumary.Rows[IdxRow].ItemArray[IdxItemArray], ""));
        }
      }

      return _str;
    }
    #endregion
  }
}
