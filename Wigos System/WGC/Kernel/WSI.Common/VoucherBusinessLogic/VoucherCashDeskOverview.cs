﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCashDeskOverview.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
// 07-SEP-2016  EOR         Product Backlog Item 15335: TPV Televisa: Cash Summary
// 27-SEP-2016  ETP         Fixed Bug 18121: Payment order are duplicated in output summary.
// 30-SEP-2016  EOR         PBI 17964: Televisa - Sorteo de Caja (Premio en efectivo) - Configuración
// 04-OCT-2016  ESE         Bug 17914: Cash status, add info to report
// 06-OCT-2016  ETP         Bug 18580: Some nls errors corrected
// 07-OCT-2016  ETP         Bug 18580: Appears @VOUCHER_PRIZES when otheroutputs is not configured.
// 03-OCT-2016  RGR         PBI 17964: Televisa - Draw Cash (Cash prize) - Configuration
// 04-OCT-2016  RGR         PBI 17447: Televisa - Draw Cash (Cash prize) - Recharge Operation
// 05-OCT-2016  EOR         PBI 17963: Televisa - Service Charge: Modifications Voucher/Report/Settings
// 10-OCT-2016  ESE         Bug 17914: Add info to report
// 12-OCT-2016  RGR         Bug 18922: Draw box with cash prize (Televisa): the concept of the promotional coupon is not shown on the ticket cash desk
// 20-OCT-2016  PDM         Fixed Bug 19399:Cajero: No se muestran los cambios solicitados por Televisa en los "Movimientos de caja"
// 22-NOV-2016  EOR         Fixed Bug 19253:Televisa: Show labels incorrect in cashier
// 25-ENE-2017  DPC         Bug 23052:Resumen de Caja: excel e impresión no incluyen algunos conceptos
// 16-FEB-2017  RAB         Bug 24330: Session summary cashdesk: missing and excess appear in some types of cash, chips, check and currency
// 21-FEB-2017  FGB         Bug 24801: WigosGUI: Incorrect collection in the cash closing, due to Gp BankCard.Conciliate
// 08-MAR-2017  FGB         PBI 25268: Third TAX - Payment Voucher
// 20-MAR-2017  FGB         PBI 25269: Third TAX - Cash session summary
// 23-OCT-2017  RGR         PBI 30366: WIGOS-5680 Winpot - Facturation requirements - From 01/12/2017
// 27-FEB-2018  XGJ         Bug 31742: WIGOS-6832 [Ticket #10415] Tickets with status "Pending to Print" may cause discrepacies in net win calculations
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherCashDeskOverview : Voucher
  {
    #region Class Attributes

    public class ParametersCashDeskOverview
    {
      public Boolean FromCashier;
      public Boolean WithLiabilities;
      public Boolean IsFilterMovements;
      public Int64 CashierSessionId;
      public Color BackColor;
      public GU_USER_TYPE UserType;
      public Boolean HideChipsAndCurrenciesConcepts;
      public String CurrencyIsoCode;

      public ParametersCashDeskOverview()
      {
        FromCashier = false;
        WithLiabilities = false;
        IsFilterMovements = false;
        CashierSessionId = 0;
        UserType = GU_USER_TYPE.NOT_ASSIGNED;
        HideChipsAndCurrenciesConcepts = false;
        CurrencyIsoCode = CurrencyExchange.GetNationalCurrency();
      }
    }
    #endregion

    #region Constructor

    public VoucherCashDeskOverview(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats
                                 , PrintMode Mode
                                 , ParametersCashDeskOverview ParametersOverview
                                 , SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      if (!Misc.IsGamingHallMode())
      {
        LoadVoucher("VoucherCashDeskOverview");
      }
      else
      {
        LoadVoucher("VoucherGamingHallCashDeskOverview");
      }

      // 2. Load NLS strings from resource.
      LoadVoucherResources(CashierSessionStats, ParametersOverview);

      // 3. Load general voucher data.
      CashDeskOverview(CashierSessionStats, ParametersOverview);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats, ParametersCashDeskOverview ParametersOverview)
    {
      String _str_value;
      String _str_pending;
      String _str_deposit;
      Boolean _is_tito_mode;
      Boolean _mb_pending_visible;
      Boolean _gp_pending_cash_partial;
      VoucherCurrencyCashSummary.ParametersCurrencyCashSummary _parameters_currency_cash_summary;
      CurrencyIsoType _currency_iso_type;
      Boolean _show_refill_cashier;
      Boolean _show_refill_cage;
      Boolean _show_collect_cashier;
      Boolean _show_collect_cage;
      Boolean _show_others_outputs;

      //ESE 04-OCT-2016
      Int32 _voucher_mode;
      _voucher_mode = GeneralParam.GetInt32("Cashier.Voucher", "Mode", 0);

      // EOR 07-SEP-2016
      _show_others_outputs = GeneralParam.GetBoolean("Cashier", "Summary.ShowOthersOutputs", false);

      _is_tito_mode = Utils.IsTitoMode();

      _mb_pending_visible = ParametersOverview.UserType != GU_USER_TYPE.SYS_GAMING_TABLE
                         && ParametersOverview.UserType != GU_USER_TYPE.SYS_SYSTEM
                        && (ParametersOverview.UserType != GU_USER_TYPE.USER || !_is_tito_mode);

      _gp_pending_cash_partial = MobileBank.GP_GetRegisterShortfallOnDeposit();

      // CASH SUMMARY
      _parameters_currency_cash_summary = new VoucherCurrencyCashSummary.ParametersCurrencyCashSummary();
      _parameters_currency_cash_summary.HideChipsAndCurrenciesConcepts = ParametersOverview.HideChipsAndCurrenciesConcepts;
      _parameters_currency_cash_summary.IsFilterMovements = ParametersOverview.IsFilterMovements;
      _parameters_currency_cash_summary.CurrencyIsoCode = ParametersOverview.CurrencyIsoCode;

      //FGB 21-FEB-2017   Bug 24801: WigosGUI: Incorrect collection in the cash closing, due to Gp BankCard.Conciliate
      _str_value = new VoucherCurrencyCashSummary(CashierSessionStats, _parameters_currency_cash_summary, ParametersOverview).VoucherHTML;
      this.VoucherHTML = this.VoucherHTML.Replace("@STR_VOUCHER_CASH_DESK_CASH_SUMMARY", _str_value);

      // - Inputs
      _str_value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CASH_IN");
      AddString("STR_VOUCHER_ENTRIES_TITLE", _str_value);

      if (String.IsNullOrEmpty(CashierSessionStats.split_a_name))
      {
        TYPE_SPLITS _split_data;
        Split.ReadSplitParameters(out _split_data);
        //    - Inputs
        CashierSessionStats.split_a_name = _split_data.company_a.name;
        //    - uso de sala
        CashierSessionStats.split_b_name = _split_data.company_b.name;
      }

      if (String.IsNullOrEmpty(CashierSessionStats.split_b_name))
      {
        CashierSessionStats.split_b_name = Resource.String("STR_COMPANY_B_CONCEPT_CONFIG");
      }

      AddString("STR_SPLIT_B_HIDDEN", CashierSessionStats.split_enabled_b
                                      || CashierSessionStats.b_total_in != 0
                                      || CashierSessionStats.b_total_dev != 0 ? "" : "hidden");

      if (_voucher_mode == 8)
      {
        _str_value = Resource.String("STR_SPLIT_A_NAME");
      }
      else
      {
        _str_value = CashierSessionStats.split_a_name;
      }
      AddString("STR_VOUCHER_INPUTS", _str_value);

      _str_value = CashierSessionStats.split_b_name;
      AddString("STR_VOUCHER_B_TOTAL_IN", _str_value);

      // EOR 04-OCT-2016
      if (Misc.IsVoucherModeTV())
      {
        AddString("STR_PARTICIPATION_IN_DRAW", Resource.String("STR_FRM_CASH_DESK_DRAW_PARTICIPATION_TITLE"));

        AddString("STR_VOUCHER_CARD_USAGE", "");
        AddString("STR_VOUCHER_COMMISSIONS", "");
      }
      else
      {
        _str_value = WSI.Common.Misc.GetCardPlayerConceptName();
        AddString("STR_VOUCHER_CARD_USAGE", _str_value);

        _str_value = Resource.String("STR_UC_BANK_COMMISSION");
        AddString("STR_VOUCHER_COMMISSIONS", _str_value);

        AddString("STR_PARTICIPATION_IN_DRAW", "");
        AddString("VOUCHER_PARTICIPATION_IN_DRAW", "");
      }

      _str_value = Resource.String("STR_FRM_LAST_MOVEMENTS_SERVICE_CHARGE");
      AddString("STR_VOUCHER_SERVICE_CHARGE", _str_value);


      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL");
      AddString("STR_VOUCHER_TOTAL_ENTRIES", _str_value);

      // - Outputs
      _str_value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CASH_OUT");
      AddString("STR_VOUCHER_EXITS_TITLE", _str_value);
   
      if (_voucher_mode == 8)
      {
        _str_value = Resource.String("STR_CUSTOMER_WITHDRAWALS");
        AddString("STR_VOUCHER_REFUND_CARD_USAGE", String.Empty);
      }
      else
      {
        _str_value = CashierSessionStats.split_a_name;
        AddString("STR_VOUCHER_REFUND_CARD_USAGE", WSI.Common.Misc.GetCardPlayerConceptName());
      }
      AddString("STR_VOUCHER_REFUND_CHARGES", _str_value);

      _str_value = CashierSessionStats.split_b_name;
      AddString("STR_VOUCHER_B_TOTAL_REFUND", _str_value);

      _str_value = Resource.String("STR_FRM_CASH_DESK_PRIZE_PAYED");
      AddString("STR_VOUCHER_TOTAL_PRIZES", _str_value);

      if (Misc.IsTaxCustodyEnabled() || CashierSessionStats.tax_custody_return > 0)
      {
        _str_value = Misc.TaxCustodyRefundName();
        AddString("STR_TAX_CUSTODY_RETURN", _str_value);
      }
      else
      {
        AddString("STR_TAX_CUSTODY_RETURN", "");
        AddString("VOUCHER_TAX_CUSTODY_RETURN", "");
      }

      _str_value = Resource.String("STR_FRM_LAST_MOVEMENTS_DECIMAL_ROUNDING");
      AddString("STR_VOUCHER_DECIMAL_ROUNDING", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL");
      AddString("STR_VOUCHER_TOTAL_OUTPUTS", _str_value);

      // - Prizes 04-OCT-2016
      if (Misc.IsVoucherModeTV())
      {
        _str_value = Resource.String("STR_PRIZES_GROSS");
      }
      else
      {
        _str_value = Resource.String("STR_UC_BANK_PRIZES");
      }
      AddString("STR_VOUCHER_PRIZES", _str_value);

      if (Misc.IsVoucherModeTV())   //04-OCT-2016
      {
        _str_value = Resource.String("STR_RAFFLE_PRICE_PAYMENT");
      }
      else
      {
        _str_value = "";
      }
      AddString("STR_VOUCHER_GROSS_PRIZES", _str_value);

      _str_value = Resource.String("STR_UC_BANK_TAXES");
      AddString("STR_VOUCHER_TAXES", _str_value);

      _str_value = Resource.String("STR_UC_BANK_TAX_RETURNING_ON_PRIZE_COUPON");
      AddString("STR_VOUCHER_ROUNDING", _str_value);

      _str_value = Resource.String("STR_FRM_CASH_DESK_PRIZE_PAYED");
      AddString("STR_VOUCHER_TOTAL_PRIZES", _str_value);

      if (GeneralParam.GetBoolean("Cashier", "CashInTax.Enabled") || CashierSessionStats.cash_in_tax_split1 > 0)
      {
        _str_value = GeneralParam.GetString("Cashier", "CashInTax.Name", Resource.String("STR_MOVEMENT_TYPE_CASH_IN_TAX"));
        AddString("STR_VOUCHER_CASH_IN_TAX", _str_value);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_IN_TAX", "");
        AddString("VOUCHER_CASH_IN_TAX", "");
      }

      if (Misc.IsTaxProvisionsEnabled() || CashierSessionStats.tax_provisions > 0)
      {
        _str_value = GeneralParam.GetString("Cashier.TaxProvisions", "Voucher.Title", Resource.String("STR_VOUCHER_TAX_PROVISIONS_TITLE"));
        AddString("STR_VOUCHER_TAX_PROVISIONS", _str_value);
      }
      else
      {
        AddString("STR_VOUCHER_TAX_PROVISIONS", "");
        AddString("VOUCHER_TAX_PROVISIONS", "");
      }

      // EOR 10-MAY-2016
      if (Misc.IsTaxCustodyEnabled() || CashierSessionStats.tax_custody > 0 || CashierSessionStats.tax_custody_return > 0)
      {
        _str_value = Misc.TaxCustodyName();
        AddString("STR_VOUCHER_CUSTODY", _str_value);
      }
      else
      {
        AddString("STR_VOUCHER_CUSTODY", "");
        AddString("VOUCHER_CUSTODY", "");
      }

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL");
      AddString("STR_VOUCHER_TOTAL_TAXES", _str_value);

      // - Cash Desk Result
      // Balance title
      if (_voucher_mode == 8)
      {
        _str_value = Resource.String("STR_FRM_CASH_DESK_NET_BALANCE");
        AddString("STR_ORDER_PAYMENT", Resource.String("STR_UC_BANK_CHECK_PAYMENTS"));
        AddString("STR_BANK_CARD_OPERATION", Resource.String("STR_UC_BANK_CREDIT_CARD_PAYMENT"));
        AddCurrency("ORDER_PAYMENT_VALUE", CashierSessionStats.payment_orders);
        AddCurrency("BANK_CARD_OPERATION_VALUE", CashierSessionStats.total_bank_card + CashierSessionStats.total_cash_advance_bank_card);
      }
      else
      {
        _str_value = Resource.String("STR_UC_BANK_TOTAL");
        AddString("STR_ORDER_PAYMENT", String.Empty);
        AddString("STR_BANK_CARD_OPERATION", String.Empty);
        AddString("ORDER_PAYMENT_VALUE", String.Empty);
        AddString("BANK_CARD_OPERATION_VALUE", String.Empty);
      }
      AddString("STR_VOUCHER_CASH_DESK_RESULT", _str_value);

      // - HandPays
      _str_value = Resource.String("STR_UC_BANK_HANDPAYS");
      AddString("STR_VOUCHER_HANDPAYS_TITLE", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_HANDPAY_005");
      AddString("STR_VOUCHER_HANDPAYS", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_HANDPAY_014");
      AddString("STR_VOUCHER_CANCELLATION_HANDPAYS", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL");
      AddString("STR_VOUCHER_TOTAL_HANDPAYS", _str_value);

      _str_value = Resource.String("STR_UC_BANK_PROMOTION_REDEEMABLE");
      AddString("STR_VOUCHER_PROMO_RE", _str_value);

      _str_value = Resource.String("STR_UC_BANK_PROMOTION_NON_REDEEMABLE");
      AddString("STR_VOUCHER_PROMO_NR", _str_value);

      _str_value = Resource.String("STR_UC_BANK_NRE_TO_RE");
      AddString("STR_VOUCHER_NR_TO_RE", _str_value);

      _str_value = Resource.String("STR_UC_BANK_CANCEL_PROMOTION_REDEEMABLE");
      AddString("STR_VOUCHER_CANCEL_PROMO_RE", _str_value);

      _str_value = Resource.String("STR_UC_BANK_CANCEL_PROMOTION_NON_REDEEMABLE");
      AddString("STR_VOUCHER_CANCEL_PROMO_NR", _str_value);

      _str_value = Resource.String("STR_UC_BANK_CANCEL_PROMOTION_NON_REDEEMABLE");
      AddString("STR_VOUCHER_TOTAL_PRIZES", _str_value);

      // - Acceptors Deposits
      _str_value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_DEPOSIT");
      AddString("STR_VOUCHER_DEPOSITS", _str_value);

      _str_value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_WITHDRAW");
      AddString("STR_VOUCHER_WITHDRAW", _str_value);

      _str_value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CASH_IN");
      AddString("STR_VOUCHER_ENTRIES_TITLE", _str_value);

      _str_value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CASH_OUT");
      AddString("STR_VOUCHER_EXITS_TITLE", _str_value);

      _str_value = Resource.String("STR_UC_BANK_CHECK_PAYMENTS");
      AddString("STR_VOUCHER_CHECK_PAYMENTS", _str_value);

      _str_value = Resource.String("STR_UC_BANK_CREDIT_CARD_PAYMENT");
      AddString("STR_VOUCHER_CREDIT_CARD_RECHARGES", _str_value);

      if (Misc.IsVoucherModeTV() & (CashierSessionStats.total_check + CashierSessionStats.cash_advance_check + CashierSessionStats.cash_advance_check_comissions) == 0)
      {
        AddString("STR_VOUCHER_CHECK_RECHARGES", "");
        AddString("VOUCHER_CHECK_RECHARGES", "");
      }
      else
      {
        AddString("STR_VOUCHER_CHECK_RECHARGES", Resource.String("STR_UC_BANK_CHECK_RECHARGE"));
      }

      if (Misc.IsVoucherModeTV() & CashierSessionStats.total_currency_exchange == 0)
      {
        AddString("STR_VOUCHER_CURRENCY_EXCHANGES", "");
        AddString("VOUCHER_CURRENCY_EXCHANGES", "");
      }
      else
      {
        AddString("STR_VOUCHER_CURRENCY_EXCHANGES", Resource.String("STR_CURRENCY_EXCHANGE"));
      }

      // FAV 03-MAY-2016
      if (Misc.IsMultiCurrencyExchangeEnabled())
      {
        _str_value = Resource.String("STR_VOUCHER_MULTI_EXCHANGE_DEVOLUTION");
        AddString("STR_VOUCHER_DEVOLUTION_EXCHANGE", _str_value);
      }
      else
      {
        AddString("STR_VOUCHER_DEVOLUTION_EXCHANGE", "");
      }

      // RCI & DRV 01-AUG-2014: Changed && in condition
      if (GeneralParam.GetBoolean("GamingTables", "Cashier.RegisterChipSaleAtTableEnabled") || CashierSessionStats.chips_sale_register_total > 0)
      {
        _str_value = "<tr>";
        _str_value += "<td class='left'>";
        _str_value += Resource.String("STR_VOUCHER_CHIP_SALE_REGISTER") + "</td>";
        _str_value += "<td class='middle'>";
        _str_value += "<td class='right'>";
        _str_value += "@VOUCHER_CHIPS_SALE_REGISTER</td>";
        _str_value += "</tr>";
        _str_value += "<tr><td class='separator' colspan='2'>&#32;</td></tr>";

        SetParameterValue("@STR_VOUCHER_CHIPS_SALE_REGISTER", _str_value);
      }
      else
      {
        AddString("STR_VOUCHER_CHIPS_SALE_REGISTER", "");
        AddString("VOUCHER_CHIPS_SALE_REGISTER", "");
      }

      _str_value = Resource.String("STR_UC_BANK_REDEEMABLE_CASH_IN");
      AddString("STR_VOUCHER_POINTS_AS_CASHIN", _str_value);

      if (GeneralParam.GetBoolean("CreditLine","Enabled",false))
      {
        _str_value = Resource.String("STR_CREDIT_LINE_MARKER_VOUCHER");
        AddString("STR_VOUCHER_CREDIT_LINE_MARKER", _str_value);
      }
      else
      {
        AddString("STR_VOUCHER_CREDIT_LINE_MARKER", "");
      }

      if (_voucher_mode == 8)
      {
        _str_value = Resource.String("STR_UC_BANK_TOTAL_CASH_MOVEMENT");
      }
      else
      {
        _str_value = Resource.String("STR_FRM_CASH_DESK_NET_BALANCE");
      }
      AddString("STR_VOUCHER_NATIONAL_BALANCE", _str_value);

      _currency_iso_type = new CurrencyIsoType();
      _currency_iso_type.IsoCode = ParametersOverview.CurrencyIsoCode;
      _currency_iso_type.Type = CurrencyExchangeType.CURRENCY;

      // JML 05-AUG-2015
      if (!CashierSessionStats.show_short_over)
      {
        AddString("STR_VOUCHER_COLLECTED_NATIONAL_BALANCE", "");

        AddString("STR_VOUCHER_SHORT_NATIONAL_BALANCE", "");
        AddString("STR_VOUCHER_OVER_NATIONAL_BALANCE", "");
      }

      //Collected
      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED");
      AddString("STR_VOUCHER_COLLECTED_NATIONAL_BALANCE", _str_value);

      ////Short
      //_str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING");
      //AddString("STR_VOUCHER_SHORT_NATIONAL_BALANCE", _str_value);

      ////Over
      //_str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA");
      //AddString("STR_VOUCHER_OVER_NATIONAL_BALANCE", _str_value);

      if (ParametersOverview.FromCashier)
      {
        AddString("STR_VOUCHER_EXPIRATIONS_TITLE", "");
        AddString("STR_VOUCHER_EXPIRATION_DEV", "");
        AddString("STR_VOUCHER_EXPIRATION_PRIZE", "");
        AddString("STR_VOUCHER_EXPIRATION_NR", "");
      }
      else
      {
        _str_value = Resource.String("STR_UC_BANK_EXPIRATION");
        AddString("STR_VOUCHER_EXPIRATIONS_TITLE", _str_value);

        if (_is_tito_mode)
        {
          _str_value = Resource.String("STR_VOUCHER_TITO_TICKET_REDEEMABLE");
          AddString("STR_VOUCHER_EXPIRATION_DEV", _str_value);
          AddCurrency("VOUCHER_EXPIRATION_DEV", CashierSessionStats.tickets_expired_redeemable);

          _str_value = Resource.String("STR_VOUCHER_TITO_TICKET_PROMO_REDEEMABLE");
          AddString("STR_VOUCHER_EXPIRATION_PRIZE", _str_value);
          AddCurrency("VOUCHER_EXPIRATION_PRIZE", CashierSessionStats.tickets_expired_promo_redeemable);

          _str_value = Resource.String("STR_VOUCHER_TITO_TICKET_PLAYABLE");
          AddString("STR_VOUCHER_EXPIRATION_NR", _str_value);
          AddCurrency("VOUCHER_EXPIRATION_NR", CashierSessionStats.tickets_expired_promo_no_redeemable);
        }
        else
        {
          _str_value = Resource.String("STR_UC_BANK_EXPIRED_REDEEMABLE_REFUND");
          AddString("STR_VOUCHER_EXPIRATION_DEV", _str_value);

          _str_value = Resource.String("STR_UC_BANK_EXPIRED_REDEEMABLE_WINNINGS");
          AddString("STR_VOUCHER_EXPIRATION_PRIZE", _str_value);

          _str_value = Resource.String("STR_UC_BANK_EXPIRED_NON_REDEEMABLE");
          AddString("STR_VOUCHER_EXPIRATION_NR", _str_value);
        }
      }

      if (_is_tito_mode)
      {
        _str_pending = Resource.String("STR_VOUCHER_STACKER_PENDING");
        _str_deposit = Resource.String("STR_VOUCHER_STACKER_COLLECTED");
        _str_value = "";
      }
      else
      {
        switch (ParametersOverview.UserType)
        {
          case GU_USER_TYPE.NOT_ASSIGNED:
            //For global cash reports from GUI
            _str_pending = Resource.String("STR_VOUCHER_BACKORDERED");
            _str_deposit = Resource.String("STR_VOUCHER_COLLECTED_MB_ACCEPTOR");

            if (_gp_pending_cash_partial)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_OVER_SHORTFALL");
            }
            else
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_OVER");
            }

            break;

          case GU_USER_TYPE.SYS_REDEMPTION:
          case GU_USER_TYPE.USER:
            _str_pending = Resource.String("STR_UC_BANK_PENDING_AMOUNT");
            _str_deposit = Resource.String("STR_VOUCHER_COLLECTED_MB");

            if (_gp_pending_cash_partial)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_OVER_SHORTFALL");
            }
            else
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_OVER");
            }

            break;

          case GU_USER_TYPE.SYS_ACCEPTOR:
          case GU_USER_TYPE.SYS_PROMOBOX:
            _str_pending = Resource.String("STR_VOUCHER_PENDING_ACCEPTORS");
            _str_deposit = Resource.String("STR_UC_BANK_COLLECTED_ACCEPTORS");
            _str_value = "";

            break;

          default:
            _str_pending = "";
            _str_deposit = "";
            _str_value = "";

            break;
        }
      }

      AddString("STR_VOUCHER_PENDING_AMOUNT", _str_pending);
      AddString("STR_VOUCHER_CASH_OVER", _str_value);
      AddString("STR_VOUCHER_ACCEPTORS_DEPOSITS", _str_deposit);

      // RMS 18-SEP-2014   Cash Advance
      if (CurrencyExchange.IsCashAdvanceEnabled() || CashierSessionStats.total_cash_advance.SqlMoney > 0)
      {
        if (Misc.IsVoucherModeTV() & CashierSessionStats.total_cash_advance.SqlMoney == 0)
        {
          AddString("STR_VOUCHER_CASH_ADVANCE", "");
          AddString("VOUCHER_CASH_ADVANCE", "");
        }
        else
        {
          AddString("STR_VOUCHER_CASH_ADVANCE", Resource.String("STR_VOUCHER_CASH_ADVANCE"));
          AddCurrency("VOUCHER_CASH_ADVANCE", CashierSessionStats.total_cash_advance);
        }
      }
      else
      {
        AddString("STR_VOUCHER_CASH_ADVANCE", "");
        AddString("VOUCHER_CASH_ADVANCE", "");
      }

      //visible/hidden labels
      AddString("STR_TITO_VISIBLE", _is_tito_mode ? "" : "hidden");
      AddString("STR_MB_PENDING_VISIBLE", _mb_pending_visible ? "" : "hidden");
      AddString("STR_MB_OVERCASH_VISIBLE", _mb_pending_visible && !String.IsNullOrEmpty(_str_value) ? "" : "hidden");

      AddString("STR_MB_DEPOSITS_VISIBLE", !ParametersOverview.FromCashier && _mb_pending_visible ? "" : "hidden");
      AddString("STR_FROM_CASHIER_HIDDEN", ParametersOverview.FromCashier ? "hidden" : "");

      // JMV - 18-DEC-12 Gaming Hall
      if (!Misc.IsGamingHallMode())
      {
        _str_value = Resource.String("STR_VOUCHER_CONCEPTS");
      }
      else
      {
        _str_value = Resource.String("STR_VOUCHER_CONCEPTS_IN");
      }
      AddString("STR_VOUCHER_CONCEPTS_IN", _str_value);
      AddCurrency("VOUCHER_CONCEPTS_IN", CashierSessionStats.concepts_input);

      //AVZ 01-FEB-2016
      if (Entrance.GetReceptionMode() == Entrances.Enums.ENUM_RECEPTION_MODE.TicketWithPrice || CashierSessionStats.entrances_input != 0)
      {
        //YNM 25-JAN-2016
        _str_value = Resource.String("STR_MOVEMENT_TYPE_CUSTOMER_ENTRANCE");
        AddString("STR_VOUCHER_ENTRANCES_IN", _str_value);
        AddCurrency("VOUCHER_ENTRANCES_IN", CashierSessionStats.entrances_input);
      }
      else
      {
        AddString("STR_VOUCHER_ENTRANCES_IN", "");
        AddString("VOUCHER_ENTRANCES_IN", "");
      }

      if (!Misc.IsGamingHallMode())
      {
        _str_value = Resource.String("STR_VOUCHER_CONCEPTS");
      }
      else
      {
        _str_value = Resource.String("STR_VOUCHER_CONCEPTS_OUT");
      }
      AddString("STR_VOUCHER_CONCEPTS_OUT", _str_value);
      AddCurrency("VOUCHER_CONCEPTS_OUT", CashierSessionStats.concepts_output);

      if (Misc.IsGamingHallMode())
      {
        _show_refill_cashier = GeneralParam.GetBoolean("GamingHall", "Refill.ToCashier.Enabled") || CashierSessionStats.refill_amount_to_cashier != 0;
        _show_refill_cage = !GeneralParam.GetBoolean("GamingHall", "Refill.ToCashier.Enabled") || CashierSessionStats.refill_amount_to_cage != 0;
        _show_collect_cashier = GeneralParam.GetBoolean("GamingHall", "Collection.ToCashier.Enabled") || CashierSessionStats.collect_amount_to_cashier != 0;
        _show_collect_cage = !GeneralParam.GetBoolean("GamingHall", "Collection.ToCashier.Enabled") || CashierSessionStats.collect_amount_to_cage != 0;

        AddCurrency("VOUCHER_TOTAL_REFILL_TO_CASHIER", CashierSessionStats.refill_amount_to_cashier);
        AddString("STR_VOUCHER_TOTAL_REFILL_TO_CASHIER_HIDDEN", _show_refill_cashier ? "" : "hidden");

        AddCurrency("VOUCHER_TOTAL_REFILL_TO_CAGE", CashierSessionStats.refill_amount_to_cage);
        AddString("STR_VOUCHER_TOTAL_REFILL_TO_CAGE_HIDDEN", _show_refill_cage ? "" : "hidden");

        AddCurrency("VOUCHER_TOTAL_COLLECT_TO_CASHIER", CashierSessionStats.collect_amount_to_cashier);
        AddString("STR_VOUCHER_TOTAL_COLLECT_TO_CASHIER_HIDDEN", _show_collect_cashier ? "" : "hidden");

        AddCurrency("VOUCHER_TOTAL_COLLECT_TO_CAGE", CashierSessionStats.collect_amount_to_cage);
        AddString("STR_VOUCHER_TOTAL_COLLECT_TO_CAGE_HIDDEN", _show_collect_cage ? "" : "hidden");

        AddCurrency("VOUCHER_GAMING_HALL_BALANCE", CashierSessionStats.final_balance);
        _str_value = Resource.String("STR_VOUCHER_TOTAL_REFILL");
        AddString("STR_VOUCHER_TOTAL_REFILL", _str_value);
        _str_value = Resource.String("STR_VOUCHER_TOTAL_COLLECT");
        AddString("STR_VOUCHER_TOTAL_COLLECT", _str_value);
      }
    } // LoadVoucherResources

    private String LiabilitiesHTML(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
    {
      StringBuilder _sb_html;
      String _initial;
      String _final;
      String _increment;
      String _no_data;

      _no_data = "---";

      _sb_html = new StringBuilder();
      _sb_html.AppendLine(" <table border='0' align='left' cellpadding='0' cellspacing='0' width='100%'>");
      _sb_html.AppendLine("   <tr> ");
      _sb_html.AppendLine("     <td align='left'>" + Resource.String("STR_UC_BANK_LIABILITIES") + "</td> ");
      _sb_html.AppendLine("     <td align='right'>" + Resource.String("STR_UC_BANK_INITIAL") + "</td> ");
      _sb_html.AppendLine("     <td align='right'>" + Resource.String("STR_UC_BANK_FINAL") + "</td> ");
      _sb_html.AppendLine("     <td align='right'>" + Resource.String("STR_UC_BANK_INCREMENT") + "</td> ");
      _sb_html.AppendLine("   </tr> ");

      _initial = _no_data;
      _final = _no_data;
      _increment = _no_data;

      if (CashierSessionStats.liabilities_from.has_data)
      {
        _initial = CashierSessionStats.liabilities_from.re_balance.ToString();
      }
      if (CashierSessionStats.liabilities_to.has_data)
      {
        _final = CashierSessionStats.liabilities_to.re_balance.ToString();
      }
      if (CashierSessionStats.liabilities_from.has_data && CashierSessionStats.liabilities_to.has_data)
      {
        _increment = ((Currency)(CashierSessionStats.liabilities_to.re_balance - CashierSessionStats.liabilities_from.re_balance)).ToString();
      }

      _sb_html.AppendLine("   <tr>  ");
      _sb_html.AppendLine("     <td align='left' >" + Resource.String("STR_VOUCHER_RE_PROMO_LOST") + "</td> ");
      _sb_html.AppendLine("     <td align='right'>" + _initial + "</td> ");
      _sb_html.AppendLine("     <td align='right'>" + _final + "</td>  ");
      _sb_html.AppendLine("     <td align='right'>" + _increment + "</td> ");
      _sb_html.AppendLine("   </tr> ");

      if (CashierSessionStats.liabilities_from.has_data)
      {
        _initial = CashierSessionStats.liabilities_from.promo_re_balance.ToString();
      }
      if (CashierSessionStats.liabilities_to.has_data)
      {
        _final = CashierSessionStats.liabilities_to.promo_re_balance.ToString();
      }
      if (CashierSessionStats.liabilities_from.has_data && CashierSessionStats.liabilities_to.has_data)
      {
        _increment = ((Currency)(CashierSessionStats.liabilities_to.promo_re_balance - CashierSessionStats.liabilities_from.promo_re_balance)).ToString();
      }
      _sb_html.AppendLine("   <tr>  ");
      _sb_html.AppendLine("     <td align='left'>" + Resource.String("STR_UC_BANK_PROMO_RE") + "</td> ");
      _sb_html.AppendLine("     <td align='right'>" + _initial + "</td> ");
      _sb_html.AppendLine("     <td align='right'>" + _final + "</td> ");
      _sb_html.AppendLine("     <td align='right'>" + _increment + "</td> ");
      _sb_html.AppendLine("   </tr> ");

      if (CashierSessionStats.liabilities_from.has_data)
      {
        _initial = CashierSessionStats.liabilities_from.promo_nr_balance.ToString();
      }
      if (CashierSessionStats.liabilities_to.has_data)
      {
        _final = CashierSessionStats.liabilities_to.promo_nr_balance.ToString();
      }
      if (CashierSessionStats.liabilities_from.has_data && CashierSessionStats.liabilities_to.has_data)
      {
        _increment = ((Currency)(CashierSessionStats.liabilities_to.promo_nr_balance - CashierSessionStats.liabilities_from.promo_nr_balance)).ToString();
      }
      _sb_html.AppendLine("   <tr>  ");
      _sb_html.AppendLine("     <td align='left'>" + Resource.String("STR_UC_BANK_PROMO_NR") + "</td> ");
      _sb_html.AppendLine("     <td align='right'>" + _initial + "</td>   ");
      _sb_html.AppendLine("     <td align='right'>" + _final + "</td>     ");
      _sb_html.AppendLine("     <td align='right'>" + _increment + "</td> ");
      _sb_html.AppendLine("   </tr> ");

      if (CashierSessionStats.liabilities_from.has_data)
      {
        _initial = ((Currency)(CashierSessionStats.liabilities_from.re_balance +
                               CashierSessionStats.liabilities_from.promo_re_balance +
                               CashierSessionStats.liabilities_from.promo_nr_balance)).ToString();
      }
      if (CashierSessionStats.liabilities_to.has_data)
      {
        _final = ((Currency)(CashierSessionStats.liabilities_to.re_balance +
                             CashierSessionStats.liabilities_to.promo_re_balance +
                             CashierSessionStats.liabilities_to.promo_nr_balance)).ToString();
      }
      if (CashierSessionStats.liabilities_from.has_data && CashierSessionStats.liabilities_to.has_data)
      {
        _increment = ((Currency)((CashierSessionStats.liabilities_to.re_balance - CashierSessionStats.liabilities_from.re_balance) +
                                 (CashierSessionStats.liabilities_to.promo_re_balance - CashierSessionStats.liabilities_from.promo_re_balance) +
                                 (CashierSessionStats.liabilities_to.promo_nr_balance - CashierSessionStats.liabilities_from.promo_nr_balance))).ToString();
      }
      _sb_html.AppendLine("   <tr>  ");
      _sb_html.AppendLine("     <td align='left' style='border-top: solid 2px black;'>" + Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_TOTAL") + "</td> ");
      _sb_html.AppendLine("     <td align='right' style='border-top: solid 2px black;'> " + _initial + "</td> ");
      _sb_html.AppendLine("     <td align='right' style='border-top: solid 2px black'>" + _final + "</td> ");
      _sb_html.AppendLine("     <td align='right' style='border-top: solid 2px black;'>" + _increment + "</td> ");
      _sb_html.AppendLine("   </tr> ");
      _sb_html.AppendLine(" </table> ");

      return _sb_html.ToString();
    }

    private String TitoTicketsHTML(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats, GU_USER_TYPE UserType, Int32 CashierSessionId)
    {
      StringBuilder _sb_html;

      _sb_html = new StringBuilder();

      _sb_html.AppendLine("<tr> ");
      _sb_html.AppendLine("   <td colspan=\"3\" style=\"height:15px;\"> ");
      _sb_html.AppendLine("   </td> ");
      _sb_html.AppendLine("</tr> ");

      // Tickets TITO
      _sb_html.AppendLine("<tr> ");
      _sb_html.AppendLine("   <td colspan=\"3\" align=\"center\" ");
      _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
      _sb_html.AppendLine("           " + Resource.String("STR_VOUCHER_TITO_TICKETS_TITLE"));

      if (Misc.IsFloorDualCurrencyEnabled())
      {
        _sb_html.AppendLine(" - " + CurrencyExchange.GetNationalCurrency());
      }

      _sb_html.AppendLine("       </span> ");
      _sb_html.AppendLine("   </td> ");
      _sb_html.AppendLine("</tr> ");

      if (UserType == GU_USER_TYPE.USER || UserType == GU_USER_TYPE.NOT_ASSIGNED || UserType == GU_USER_TYPE.SYS_REDEMPTION)
      {
        // Cashier
        _sb_html.AppendLine("<tr> ");
        _sb_html.AppendLine("   <td class=\"left @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_TITLE"));
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("   <td class=\"middle @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("   <td class=\"right @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("</tr> ");

        // Created RE
        _sb_html.AppendLine("<tr> ");
        _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_RE_CREATED"));
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_cashier_printed_cashable_count).ToString());
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_cashier_printed_cashable).ToString());
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("</tr> ");

        // Created Promo RE
        _sb_html.AppendLine("<tr> ");
        _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_PROMO_RE_CREATED"));
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_cashier_printed_promo_redeemable_count).ToString());
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_cashier_printed_promo_redeemable).ToString());
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("</tr> ");

        // Created Promo NR
        _sb_html.AppendLine("<tr> ");
        _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_PROMO_NR_CREATED"));
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_cashier_printed_promo_no_redeemable_count).ToString());
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_cashier_printed_promo_no_redeemable).ToString());
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("</tr> ");

        // Paid RE
        _sb_html.AppendLine("<tr> ");
        _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_RE_PAID"));
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_cashier_paid_cashable_count).ToString());
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_cashier_paid_cashable).ToString());
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("</tr> ");

        // Paid Promo RE
        _sb_html.AppendLine("<tr> ");
        _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_PROMO_RE_PAID"));
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_cashier_paid_promo_redeemable_count).ToString());
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_cashier_paid_promo_redeemable).ToString());
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("</tr> ");

        // Paid Handpay
        _sb_html.AppendLine("<tr> ");
        _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_HANPAY_PAID"));
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_cancelled_redeemable_handpay_count).ToString());
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_cancelled_redeemable_handpay).ToString());
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("</tr> ");

        // Paid Jackpot
        _sb_html.AppendLine("<tr> ");
        _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_JACKPOT_PAID"));
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_cancelled_redeemable_jackpot_count).ToString());
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_cancelled_redeemable_jackpot).ToString());
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("</tr> ");
      }

      if (!TITO.Utils.IsTitoMode() || CashierSessionId == 0)
      {
        // Expired Redeemed
        if (CashierSessionStats.tickets_cashier_expired_redeemed > 0 || GeneralParam.GetInt32("TITO", "Payment.GracePeriod.Mode") != 0)
        {
          _sb_html.AppendLine("<tr> ");
          _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
          _sb_html.AppendLine("       " + Resource.String("STR_PAID_EXPIRED") + "");// + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_JACKPOT_CREATED"));
          _sb_html.AppendLine("   </td> ");
          _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
          _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_cashier_expired_redeemed_count).ToString());
          _sb_html.AppendLine("   </td> ");
          _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
          _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_cashier_expired_redeemed).ToString());
          _sb_html.AppendLine("   </td> ");
          _sb_html.AppendLine("</tr> ");
        }

        // EOR 20-MAY-2016 Tickets Swap for Chips
        if (GeneralParam.GetBoolean("TITA", "Enabled", false))
        {
          _sb_html.AppendLine("<tr> ");
          _sb_html.AppendLine("   <td class=\"sub-left @STR_TITO_VISIBLE\"> ");
          _sb_html.AppendLine("       " + Resource.String("STR_SWAP_CHIPS") + "");
          _sb_html.AppendLine("   </td> ");
          _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
          _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_swap_chips_count).ToString());
          _sb_html.AppendLine("   </td> ");
          _sb_html.AppendLine("   <td class=\"right @STR_TITO_VISIBLE\"> ");
          _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_swap_chips).ToString());
          _sb_html.AppendLine("   </td> ");
          _sb_html.AppendLine("</tr> ");
        }
      }

      //Totals
      if (UserType == GU_USER_TYPE.USER || UserType == GU_USER_TYPE.NOT_ASSIGNED || UserType == GU_USER_TYPE.SYS_REDEMPTION)
      {
        _sb_html.AppendLine("<tr> ");
        _sb_html.AppendLine("   <td class=\"left border @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
        _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_TOTAL_CREATED") + "");
        _sb_html.AppendLine("       </span> ");
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("   <td class=\"middleTITO border @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
        _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_created_count).ToString());
        _sb_html.AppendLine("       </span> ");
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("   <td class=\"rightTITO border @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
        _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_created_amount).ToString());
        _sb_html.AppendLine("       </span> ");
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("</tr> ");

        _sb_html.AppendLine("<tr> ");
        _sb_html.AppendLine("   <td class=\"left @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
        _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_CASHIER_TOTAL_CANCELLED") + "");
        _sb_html.AppendLine("       </span> ");
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
        _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_cancelled_count).ToString());
        _sb_html.AppendLine("       </span> ");
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
        _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
        _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_cancelled_amount).ToString());
        _sb_html.AppendLine("       </span> ");
        _sb_html.AppendLine("   </td> ");
        _sb_html.AppendLine("</tr> ");

        // White Line
        _sb_html.AppendLine("<tr>");
        _sb_html.AppendLine("  <td class='separator' colspan='3'>");
        _sb_html.AppendLine("&#32;</td>");
        _sb_html.AppendLine("</tr>");
      }

      //XGJ 27-DEC-2018 Wigos-6832: [Ticket #10415] Tickets with status "Pending to Print" may cause discrepacies in net win calculations
      //We don't have enough information in the system to know if the ticket has been printed by the EGM so, all the group "Terminal" have been disabled.

      //if (UserType == GU_USER_TYPE.SYS_TITO || UserType == GU_USER_TYPE.NOT_ASSIGNED)
      //{
      //  // Machine
      //  _sb_html.AppendLine("<tr> ");
      //  _sb_html.AppendLine("   <td class=\"left @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_TITLE"));
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("   <td class=\"middle @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("   <td class=\"right @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("</tr> ");

      //  // Played RE
      //  _sb_html.AppendLine("<tr> ");
      //  _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_RE_PLAYED"));
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_machine_played_cashable_count).ToString());
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_machine_played_cashable).ToString());
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("</tr> ");

      //  // Played Promo RE
      //  _sb_html.AppendLine("<tr> ");
      //  _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_PROMO_RE_PLAYED"));
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_machine_played_promo_redeemable_count).ToString());
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_machine_played_promo_redeemable).ToString());
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("</tr> ");

      //  // Played Promo NR
      //  _sb_html.AppendLine("<tr> ");
      //  _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_PROMO_NR_PLAYED"));
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_machine_played_promo_no_redeemable_count).ToString());
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_machine_played_promo_no_redeemable).ToString());
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("</tr> ");

      //  // Created RE
      //  _sb_html.AppendLine("<tr> ");
      //  _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_RE_CREATED"));
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_machine_printed_cashable_count).ToString());
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_machine_printed_cashable).ToString());
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("</tr> ");

      //  // Created Promo NR
      //  _sb_html.AppendLine("<tr> ");
      //  _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_NR_CREATED"));
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_machine_printed_promo_no_redeemable_count).ToString());
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_machine_printed_promo_no_redeemable).ToString());
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("</tr> ");

      //  // Created Handpay
      //  _sb_html.AppendLine("<tr> ");
      //  _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_HANDPAY_CREATED"));
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_created_redeemable_handpay_count).ToString());
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_created_amount_redeemable_handpay).ToString());
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("</tr> ");

      //  // Created Jackpot
      //  _sb_html.AppendLine("<tr> ");
      //  _sb_html.AppendLine("   <td class=\"sub-leftTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_JACKPOT_CREATED"));
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_created_redeemable_jackpot_count).ToString());
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_created_amount_redeemable_jackpot).ToString());
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("</tr> ");

      //  //Totals
      //  _sb_html.AppendLine("<tr> ");
      //  _sb_html.AppendLine("   <td class=\"left border @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
      //  _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_TOTAL_TICKET_IN") + "");
      //  _sb_html.AppendLine("       </span> ");
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("   <td class=\"middleTITO border @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
      //  _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_played_machine_count).ToString());
      //  _sb_html.AppendLine("       </span> ");
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("   <td class=\"rightTITO border @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
      //  _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_played_machine_amount).ToString());
      //  _sb_html.AppendLine("       </span> ");
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("</tr> ");

      //  _sb_html.AppendLine("<tr> ");
      //  _sb_html.AppendLine("   <td class=\"left @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
      //  _sb_html.AppendLine("       " + Resource.String("STR_VOUCHER_TITO_TICKETS_MACHINE_TOTAL_TICKET_OUT") + "");
      //  _sb_html.AppendLine("       </span> ");
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("   <td class=\"middleTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
      //  _sb_html.AppendLine("       " + ((Int32)CashierSessionStats.tickets_created_machine_count).ToString());
      //  _sb_html.AppendLine("       </span> ");
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("   <td class=\"rightTITO @STR_TITO_VISIBLE\"> ");
      //  _sb_html.AppendLine("       <span style=\"font-weight:bold\"> ");
      //  _sb_html.AppendLine("       " + ((Currency)CashierSessionStats.tickets_created_machine_amount).ToString());
      //  _sb_html.AppendLine("       </span> ");
      //  _sb_html.AppendLine("   </td> ");
      //  _sb_html.AppendLine("</tr> ");
      //}

      return _sb_html.ToString();
    }

    // EOR 07-SEP-2016
    private String OthersOutputsSummary(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
    {
      StringBuilder _sb_html;
      String _str_value;
      String _cur_value;

      _sb_html = new StringBuilder();

      //Title
      _str_value = Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CASH_OUT");
      _sb_html.AppendLine("<tr> ");
      _sb_html.AppendLine("   <td class=\"left\"> ");
      _sb_html.AppendLine("       " + _str_value + "");
      _sb_html.AppendLine("   </td> ");
      _sb_html.AppendLine("   <td class=\"right\"> ");
      _sb_html.AppendLine("   </td> ");
      _sb_html.AppendLine("</tr> ");

      //Cash
      _str_value = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_CASH_PAYMENT");
      _cur_value = ((Currency)(CashierSessionStats.total_cash_out - CashierSessionStats.payment_orders)).ToString();

      _sb_html.AppendLine("<tr> ");
      _sb_html.AppendLine("   <td class=\"sub-left\"> ");
      _sb_html.AppendLine("       " + _str_value);
      _sb_html.AppendLine("   </td> ");
      _sb_html.AppendLine("   <td class=\"right\"> ");
      _sb_html.AppendLine("       " + _cur_value);
      _sb_html.AppendLine("   </td> ");
      _sb_html.AppendLine("</tr> ");

      //Payments Orders
      _str_value = Resource.String("STR_UC_BANK_CHECK_PAYMENTS");
      _cur_value = ((Currency)CashierSessionStats.payment_orders).ToString();

      _sb_html.AppendLine("<tr> ");
      _sb_html.AppendLine("   <td class=\"sub-left\"> ");
      _sb_html.AppendLine("       " + _str_value);
      _sb_html.AppendLine("   </td> ");
      _sb_html.AppendLine("   <td class=\"right\"> ");
      _sb_html.AppendLine("       " + _cur_value);
      _sb_html.AppendLine("   </td> ");
      _sb_html.AppendLine("</tr> ");

      //Total
      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL");
      _cur_value = ((Currency)CashierSessionStats.total_cash_out).ToString();

      _sb_html.AppendLine("<tr> ");
      _sb_html.AppendLine("   <td class=\"left\" style=\"border-top: solid 2px black\"> ");
      _sb_html.AppendLine("       " + _str_value);
      _sb_html.AppendLine("   </td> ");
      _sb_html.AppendLine("   <td class=\"right\" style=\"border-top: solid 2px black\"> ");
      _sb_html.AppendLine("       " + _cur_value);
      _sb_html.AppendLine("   </td> ");
      _sb_html.AppendLine("</tr> ");

      //Separator
      _sb_html.AppendLine("<tr> ");
      _sb_html.AppendLine("   <td class=\"separator\" colspan=\"2\"> ");
      _sb_html.AppendLine("       &#32;");
      _sb_html.AppendLine("   </td> ");
      _sb_html.AppendLine("</tr> ");

      return _sb_html.ToString();
    }

    #endregion

    #region Public Methods

    public void CashDeskOverview(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats, ParametersCashDeskOverview ParametersOverview)
    {
      Voucher _voucher;
      String _str;
      Boolean _is_tito_mode;
      Currency _mb_pending;
      CurrencyIsoType _currency_iso_type;
      Currency _tmp_amount;
      Currency _collected_amount;
      Boolean _show_others_outputs;
      Currency _currency_prizes;
      Currency _currency_gross_prizes;

      //ESE 04-OCT-2016
      Int32 _voucher_mode;
      _voucher_mode = GeneralParam.GetInt32("Cashier.Voucher", "Mode", 0);

      // EOR 07-SEP-2016
      _show_others_outputs = GeneralParam.GetBoolean("Cashier", "Summary.ShowOthersOutputs", false);
      _is_tito_mode = Utils.IsTitoMode();
      _mb_pending = 0;

      // - Inputs
      AddCurrency("VOUCHER_IMPUTS", CashierSessionStats.a_total_in);

      if (Misc.IsVoucherModeTV())
      {
        AddCurrency("VOUCHER_B_TOTAL_IN", CashierSessionStats.b_total_in + CashierSessionStats.cards_usage + CashierSessionStats.total_commission);
        AddString("VOUCHER_CARD_USAGE", "");
        AddString("VOUCHER_COMMISSIONS", "");
        AddCurrency("VOUCHER_PARTICIPATION_IN_DRAW", CashierSessionStats.participation_in_draw);
      }
      else
      {
        AddCurrency("VOUCHER_B_TOTAL_IN", CashierSessionStats.b_total_in);
        AddCurrency("VOUCHER_CARD_USAGE", CashierSessionStats.cards_usage);
        AddCurrency("VOUCHER_COMMISSIONS", CashierSessionStats.total_commission);
      }

      AddCurrency("VOUCHER_SERVICE_CHARGE", CashierSessionStats.service_charge);

      AddCurrency("VOUCHER_TOTAL_ENTRIES", CashierSessionStats.total_cash_in);

      // - Outputs
      if (_voucher_mode == 8)
      {
        AddCurrency("VOUCHER_REFUND_CHARGES", CashierSessionStats.a_total_dev + CashierSessionStats.refund_cards_usage);
        AddString("VOUCHER_REFUND_CARD_USAGE", String.Empty);
      }
      else
      {
        AddCurrency("VOUCHER_REFUND_CHARGES", CashierSessionStats.a_total_dev);
        AddCurrency("VOUCHER_REFUND_CARD_USAGE", CashierSessionStats.refund_cards_usage);
      }

      AddCurrency("VOUCHER_B_TOTAL_REFUND", CashierSessionStats.b_total_dev);

      AddCurrency("VOUCHER_TOTAL_PRIZES", CashierSessionStats.prize_payment);

      if (Misc.IsTaxCustodyEnabled() || CashierSessionStats.tax_custody_return > 0)
      {
        _str = Misc.TaxCustodyRefundName();
        AddString("STR_TAX_CUSTODY_RETURN", _str);
        AddCurrency("VOUCHER_TAX_CUSTODY_RETURN", CashierSessionStats.tax_custody_return);
      }
      else
      {
        AddString("STR_TAX_CUSTODY_RETURN", "");
        AddString("VOUCHER_TAX_CUSTODY_RETURN", "");
      }

      AddCurrency("VOUCHER_DECIMAL_ROUNDING", CashierSessionStats.decimal_rounding);

      AddCurrency("VOUCHER_TOTAL_OUTPUTS_NO_WITHDRAW_CARD", CashierSessionStats.total_cash_out);

      _currency_prizes = CashierSessionStats.prize_gross;
      _currency_gross_prizes = 0;

      // - Prizes
      if (Misc.IsVoucherModeTV())
      {
        if (CashDeskDrawBusinessLogic.IsCashPrizeCashDeskDraw(CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_00))
        {
          AddCurrency("VOUCHER_PRIZES", CashierSessionStats.prize_gross);
          AddCurrency("VOUCHER_GROSS_PRIZES", CashierSessionStats.prize_gross_other);
        }
        else
        {
          AddCurrency("VOUCHER_PRIZES", CashierSessionStats.prize_gross);
          AddCurrency("VOUCHER_GROSS_PRIZES", 0);
        }
      }
      else
      {
        AddCurrency("VOUCHER_PRIZES", CashierSessionStats.prize_gross);
        AddString("VOUCHER_GROSS_PRIZES", "");
      }

      AddCurrency("VOUCHER_PRIZES", _currency_prizes);
      AddCurrency("VOUCHER_GROSS_PRIZES", _currency_gross_prizes);

      AddCurrency("VOUCHER_TAXES", CashierSessionStats.prize_taxes);

      AddCurrency("VOUCHER_ROUNDING", CashierSessionStats.tax_returning_on_prize_coupon);

      AddCurrency("VOUCHER_TOTAL_PRIZES", CashierSessionStats.prize_payment);

      AddCurrency("VOUCHER_TOTAL_TAXES", CashierSessionStats.prize_taxes);

      // - UNR
      if (GeneralParam.GetBoolean("Cashier", "Promotion.HideUNR", true) &&
          CashierSessionStats.unr_k1_gross == 0 && CashierSessionStats.unr_k2_gross == 0 &&
          CashierSessionStats.ure_k1_gross == 0 && CashierSessionStats.ure_k2_gross == 0)
      {
        AddString("STR_VOUCHER_UNR_KIND1", "");
        AddString("STR_VOUCHER_UNR_KIND2", "");

        AddString("VOUCHER_UNR_KIND1", "");
        AddString("VOUCHER_UNR_KIND2", "");
      }
      else
      {
        _str = Resource.String("STR_FRM_CASH_DESK_UNR_KIND", 1);
        AddString("STR_VOUCHER_UNR_KIND1", _str);

        _str = Resource.String("STR_FRM_CASH_DESK_UNR_KIND", 2);
        AddString("STR_VOUCHER_UNR_KIND2", _str);

        AddCurrency("VOUCHER_UNR_KIND1", CashierSessionStats.unr_k1_gross + CashierSessionStats.ure_k1_gross);
        AddCurrency("VOUCHER_UNR_KIND2", CashierSessionStats.unr_k2_gross + CashierSessionStats.ure_k2_gross);
      }

      // - Cash Desk Result
      AddCurrency("VOUCHER_TOTAL_INPUTS", CashierSessionStats.total_cash_in);

      AddCurrency("VOUCHER_TOTAL_OUTPUTS", CashierSessionStats.total_cash_out);

      // Taxes names and values
      AddTax("STR_VOUCHER_TAX_ON_PRIZE_1", "VOUCHER_TAX_ON_PRIZE_1", CashierSessionStats.prize_taxes_1_name, CashierSessionStats.prize_taxes_1);
      AddTax("STR_VOUCHER_TAX_ON_PRIZE_2", "VOUCHER_TAX_ON_PRIZE_2", CashierSessionStats.prize_taxes_2_name, CashierSessionStats.prize_taxes_2);
      AddTax("STR_VOUCHER_TAX_ON_PRIZE_3", "VOUCHER_TAX_ON_PRIZE_3", CashierSessionStats.prize_taxes_3_name, CashierSessionStats.prize_taxes_3);

      // Total taxes
      AddTax("STR_VOUCHER_TOTAL_TAX_1", "VOUCHER_TOTAL_TAX_1", CashierSessionStats.prize_taxes_1_name, CashierSessionStats.total_tax1);
      AddTax("STR_VOUCHER_TOTAL_TAX_2", "VOUCHER_TOTAL_TAX_2", CashierSessionStats.prize_taxes_2_name, CashierSessionStats.total_tax2);
      AddTax("STR_VOUCHER_TOTAL_TAX_3", "VOUCHER_TOTAL_TAX_3", CashierSessionStats.prize_taxes_3_name, CashierSessionStats.total_tax3);

      if (GeneralParam.GetBoolean("Cashier", "CashInTax.Enabled") || CashierSessionStats.cash_in_tax_split1 > 0)
      {
        _str = GeneralParam.GetString("Cashier", "CashInTax.Name", Resource.String("STR_MOVEMENT_TYPE_CASH_IN_TAX"));
        AddString("STR_VOUCHER_CASH_IN_TAX", _str);
        AddCurrency("VOUCHER_CASH_IN_TAX", CashierSessionStats.cash_in_tax_split1 + CashierSessionStats.cash_in_tax_split2);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_IN_TAX", "");
        AddString("VOUCHER_CASH_IN_TAX", "");
      }

      if (Misc.IsTaxProvisionsEnabled() || CashierSessionStats.tax_provisions > 0)
      {
        _str = GeneralParam.GetString("Cashier.TaxProvisions", "Voucher.Title", Resource.String("STR_VOUCHER_TAX_PROVISIONS_TITLE"));
        AddString("STR_VOUCHER_TAX_PROVISIONS", _str);
        AddCurrency("VOUCHER_TAX_PROVISIONS", CashierSessionStats.tax_provisions);
      }
      else
      {
        AddString("STR_VOUCHER_TAX_PROVISIONS", "");
        AddString("VOUCHER_TAX_PROVISIONS", "");
      }

      // EOR 10-MAY-2016
      if (Misc.IsTaxCustodyEnabled() || CashierSessionStats.tax_custody > 0)
      {
        _str = Misc.TaxCustodyName();
        AddString("STR_VOUCHER_CUSTODY", _str);
        AddString("STR_VOUCHER_TAX_CUSTODY", _str);

        AddCurrency("VOUCHER_CUSTODY", CashierSessionStats.tax_custody - CashierSessionStats.tax_custody_return);
        AddCurrency("VOUCHER_TAX_CUSTODY", CashierSessionStats.tax_custody);
      }
      else
      {
        AddString("STR_VOUCHER_CUSTODY", "");
        AddString("STR_VOUCHER_TAX_CUSTODY", "");

        AddString("VOUCHER_CUSTODY", "");
        AddString("VOUCHER_TAX_CUSTODY", "");
      }

      if (_voucher_mode == 8)
      {
        AddCurrency("VOUCHER_CASH_DESK_RESULT", CashierSessionStats.result_cashier - /*Bank card operation*/(CashierSessionStats.total_bank_card + CashierSessionStats.total_cash_advance_bank_card));
      }
      else
      {
        AddCurrency("VOUCHER_CASH_DESK_RESULT", CashierSessionStats.result_cashier);
      }

      if (CashierSessionStats.result_cashier < 0)
      {
        SetParameterValue("@STR_NEGATIVE", "negative");
      }
      else
      {
        AddString("STR_NEGATIVE", "");
      }

      AddCurrency("VOUCHER_HANDPAYS", CashierSessionStats.handpay_payment);

      AddCurrency("VOUCHER_CANCELLATION_HANDPAYS", CashierSessionStats.handpay_canceled);

      AddCurrency("VOUCHER_TOTAL_HANDPAYS", CashierSessionStats.handpay_payment - CashierSessionStats.handpay_canceled);

      // EOR 07-SEP-2016
      if (Misc.IsVoucherModeTV())
      {
        AddCurrencyWithOutSymbol("VOUCHER_PROMO_RE", CashierSessionStats.promo_re);

        AddCurrencyWithOutSymbol("VOUCHER_PROMO_NR", CashierSessionStats.promo_nr);

        AddCurrencyWithOutSymbol("VOUCHER_NR_TO_RE", CashierSessionStats.promo_nr_to_re);

        AddCurrencyWithOutSymbol("VOUCHER_CANCEL_PROMO_RE", CashierSessionStats.cancel_promo_re);

        AddCurrencyWithOutSymbol("VOUCHER_CANCEL_PROMO_NR", CashierSessionStats.cancel_promo_nr);
      }
      else
      {
        AddCurrency("VOUCHER_PROMO_RE", CashierSessionStats.promo_re);

        AddCurrency("VOUCHER_PROMO_NR", CashierSessionStats.promo_nr);

        AddCurrency("VOUCHER_NR_TO_RE", CashierSessionStats.promo_nr_to_re);

        AddCurrency("VOUCHER_CANCEL_PROMO_RE", CashierSessionStats.cancel_promo_re);

        AddCurrency("VOUCHER_CANCEL_PROMO_NR", CashierSessionStats.cancel_promo_nr);
      }

      AddCurrency("VOUCHER_DEPOSITS", CashierSessionStats.deposits);

      AddCurrency("VOUCHER_WITHDRAW", CashierSessionStats.withdraws);

      AddCurrency("VOUCHER_TOTAL_INPUTS", CashierSessionStats.total_cash_in);

      AddCurrency("VOUCHER_TOTAL_OUTPUTS", CashierSessionStats.total_cash_out);

      AddCurrency("VOUCHER_CHECK_PAYMENTS", CashierSessionStats.payment_orders);

      AddCurrency("VOUCHER_CREDIT_CARD_RECHARGES", CashierSessionStats.total_bank_card + CashierSessionStats.total_cash_advance_bank_card);

      AddCurrency("VOUCHER_CHECK_RECHARGES", CashierSessionStats.total_check + CashierSessionStats.cash_advance_check + CashierSessionStats.cash_advance_check_comissions);

      AddCurrency("VOUCHER_CURRENCY_EXCHANGES", CashierSessionStats.total_currency_exchange);

      if (GeneralParam.GetBoolean("CreditLine", "Enabled", false))
      {
        AddCurrency("VOUCHER_CREDIT_LINE_MARKER", CashierSessionStats.total_credit_line_marker);
      }
      else
      {
        AddString("VOUCHER_CREDIT_LINE_MARKER", "");
      }

      // FAV 03-MAY-2016
      if (Misc.IsMultiCurrencyExchangeEnabled())
      {
        AddCurrency("VOUCHER_DEVOLUTION_EXCHANGE", CashierSessionStats.cash_in_currency_exchange);
      }
      else
      {
        AddString("VOUCHER_DEVOLUTION_EXCHANGE", "");
      }

      AddCurrency("VOUCHER_CHIPS_SALE_REGISTER", CashierSessionStats.chips_sale_register_total);

      // EOR 07-SEP-2016
      if (Misc.IsVoucherModeTV())
      {
        AddCurrencyWithOutSymbol("VOUCHER_POINTS_AS_CASHIN", CashierSessionStats.points_as_cashin);
      }
      else
      {
        AddCurrency("VOUCHER_POINTS_AS_CASHIN", CashierSessionStats.points_as_cashin);
      }

      if (CashierSessionStats.national_game_profit != 0)
      {
        SetParameterValue("@STR_VISIBILITY", "");
        AddString("STR_VOUCHER_GAME_PROFIT", Resource.String("STR_VOUCHER_OVERVIEW_GAME_PROFIT"));
        AddCurrency("VOUCHER_GAME_PROFIT", CashierSessionStats.national_game_profit);
      }
      else
      {
        SetParameterValue("@STR_VISIBILITY", "display: none;");
        AddString("STR_VOUCHER_GAME_PROFIT", "");
        AddString("VOUCHER_GAME_PROFIT", "");
      }

      if (ParametersOverview.IsFilterMovements)
      {
        AddString("VOUCHER_TOTAL_NATIONAL_BALANCE", "---");
        AddString("VOUCHER_PENDING_AMOUNT", "---");
        AddString("VOUCHER_CASH_OVER", "---");
        AddString("VOUCHER_TOTAL_NATIONAL_BALANCE", "---");
        AddString("VOUCHER_TOTAL_COLLECTED_NATIONAL_BALANCE", "---");
        AddString("VOUCHER_TOTAL_SHORT_NATIONAL_BALANCE", "---");
        AddString("VOUCHER_TOTAL_OVER_NATIONAL_BALANCE", "---");

        //Short
        _str = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING");
        AddString("STR_VOUCHER_SHORT_NATIONAL_BALANCE", _str);

        //Over
        _str = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA");
        AddString("STR_VOUCHER_OVER_NATIONAL_BALANCE", _str);
      }
      else
      {
        AddCurrency("VOUCHER_TOTAL_NATIONAL_BALANCE", CashierSessionStats.final_balance);
        AddCurrency("VOUCHER_PENDING_AMOUNT", ((Currency)(CashierSessionStats.mb_pending + CashierSessionStats.mb_total_balance_lost)));
        AddCurrency("VOUCHER_CASH_OVER", ((Currency)(CashierSessionStats.total_cash_over)));

        // JML 05-AUG-2015
        if (!CashierSessionStats.show_short_over)
        {
          AddString("VOUCHER_TOTAL_COLLECTED_NATIONAL_BALANCE", "");

          AddString("VOUCHER_TOTAL_SHORT_NATIONAL_BALANCE", "");
          AddString("VOUCHER_TOTAL_OVER_NATIONAL_BALANCE", "");
        }

        _currency_iso_type = new CurrencyIsoType();
        _currency_iso_type.IsoCode = ParametersOverview.CurrencyIsoCode;
        _currency_iso_type.Type = CurrencyExchangeType.CURRENCY;

        //Collected
        _collected_amount = CashierSessionStats.final_balance;

        //Short
        _tmp_amount = 0;
        if (CashierSessionStats.short_currency != null && CashierSessionStats.short_currency.ContainsKey(_currency_iso_type))
        {
          _tmp_amount = CashierSessionStats.short_currency[_currency_iso_type];
          if (_tmp_amount < 0)
          {
            _tmp_amount = -_tmp_amount;
          }
        }
        _collected_amount -= _tmp_amount;

        String _str_value;

        if (_tmp_amount > 0)
        {
          _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING");
          AddString("STR_VOUCHER_SHORT_NATIONAL_BALANCE", _str_value);

          AddCurrency("VOUCHER_TOTAL_SHORT_NATIONAL_BALANCE", _tmp_amount);
        }
        else
        {
          AddString("STR_VOUCHER_SHORT_NATIONAL_BALANCE", "");
          AddString("VOUCHER_TOTAL_SHORT_NATIONAL_BALANCE", "");
        }

        //Over
        _tmp_amount = 0;
        if (CashierSessionStats.over_currency != null && CashierSessionStats.over_currency.ContainsKey(_currency_iso_type))
        {
          _tmp_amount = CashierSessionStats.over_currency[_currency_iso_type];
          if (_tmp_amount < 0)
          {
            _tmp_amount = -_tmp_amount;
          }
        }

        _collected_amount += _tmp_amount;

        if (_tmp_amount > 0)
        {
          _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA");
          AddString("STR_VOUCHER_OVER_NATIONAL_BALANCE", _str_value);
          AddCurrency("VOUCHER_TOTAL_OVER_NATIONAL_BALANCE", _tmp_amount);
        }
        else
        {
          AddString("STR_VOUCHER_OVER_NATIONAL_BALANCE", "");
          AddString("VOUCHER_TOTAL_OVER_NATIONAL_BALANCE", "");
        }

        //Collected
        AddCurrency("VOUCHER_TOTAL_COLLECTED_NATIONAL_BALANCE", _collected_amount);
      }

      if (MobileBank.GP_GetRegisterShortfallOnDeposit())
      {
        _mb_pending = (Currency)CashierSessionStats.mb_pending + CashierSessionStats.mb_total_balance_lost;
      }
      else
      {
        _mb_pending = CashierSessionStats.mb_pending;
      }

      if (_mb_pending > 0 && !ParametersOverview.IsFilterMovements)
      {
        SetParameterValue("@STR_COLOR_PENDING", "pending");
      }
      else
      {
        AddString("STR_COLOR_PENDING", "");
      }

      if (ParametersOverview.FromCashier)
      {
        AddString("VOUCHER_EXPIRATION_DEV", "");
        AddString("VOUCHER_EXPIRATION_PRIZE", "");
        AddString("VOUCHER_EXPIRATION_NR", "");
        AddString("VOUCHER_ACCEPTORS_DEPOSITS", "");
      }
      else
      {
        AddCurrency("VOUCHER_EXPIRATION_DEV", CashierSessionStats.redeemable_dev_expired);
        AddCurrency("VOUCHER_EXPIRATION_PRIZE", CashierSessionStats.redeemable_prize_expired);
        AddCurrency("VOUCHER_EXPIRATION_NR", CashierSessionStats.not_redeemable_expired);
        AddCurrency("VOUCHER_ACCEPTORS_DEPOSITS", CashierSessionStats.mb_deposit_amount);
      }

      SetParameterValue("@VOUCHER_STYLE", "BODY{background-color:'" + ColorTranslator.ToHtml(ParametersOverview.BackColor) + "';}");

      if (ParametersOverview.WithLiabilities)
      {
        SetParameterValue("@VOUCHER_LIABILITIES", LiabilitiesHTML(CashierSessionStats));
      }
      else
      {
        AddString("VOUCHER_LIABILITIES", "");
      }

      // DHA 25-APR-2014 Added TITO ticket detail
      if (_is_tito_mode && (ParametersOverview.UserType == GU_USER_TYPE.NOT_ASSIGNED || ParametersOverview.UserType == GU_USER_TYPE.SYS_TITO || ParametersOverview.UserType == GU_USER_TYPE.USER
        || ParametersOverview.UserType == GU_USER_TYPE.SYS_REDEMPTION))
      {
        SetParameterValue("@VOUCHER_TITO_TICKETS", TitoTicketsHTML(CashierSessionStats, ParametersOverview.UserType, (Int32)ParametersOverview.CashierSessionId));
      }
      else
      {
        AddString("VOUCHER_TITO_TICKETS", "");
      }

      // EOR 07-SEP-2016
      if (Misc.IsVoucherModeTV())
      {
        SetParameterValue("@VOUCHER_OTHERS_OUTPUTS_SUMMARY", OthersOutputsSummary(CashierSessionStats));
      }
      else
      {
        AddString("VOUCHER_OTHERS_OUTPUTS_SUMMARY", "");
      }

      _voucher = new WSI.Common.VoucherCurrencyCash(CashierSessionStats, false, ParametersOverview.IsFilterMovements, ParametersOverview.HideChipsAndCurrenciesConcepts, ParametersOverview);
      SetParameterValue("@VOUCHER_CURRENCY_CASH", _voucher.VoucherHTML);
    }

    /// <summary>
    /// Replaces TagName and TagValue tags for the TaxName and TaxValue if TaxAmount is different from 0
    /// </summary>
    /// <param name="TagName">Tag for the name</param>
    /// <param name="TagValue">Tag for the value</param>
    /// <param name="TaxName">Name of the tax</param>
    /// <param name="TaxAmount">Amount of the tax</param>
    private void AddTax(String TagName, String TagValue, String TaxName, Decimal TaxAmount)
    {
      if (TaxAmount != 0)
      {
        AddString(TagName, TaxName);
        AddCurrency(TagValue, TaxAmount);
      }
      else
      {
        AddString(TagName, String.Empty);
        AddString(TagValue, String.Empty);
      }
    }
    #endregion
  }
}
