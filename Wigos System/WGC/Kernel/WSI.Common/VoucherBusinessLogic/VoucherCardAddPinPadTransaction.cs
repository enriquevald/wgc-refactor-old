﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCardAddCurrencyExchange.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Jaume Barnés
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
// 07-NOV-2016  ATB         Bug 20154:Televisa: No se muestra el numero de "Afiliación" en los correspondientes vouchers
// 21-MAY-2017  ETP         Fixed Bug WIGOS-2997: Not controlled exception When PinPad is enabled and disconected.
// 10-JUL-2017  RGR         Bug 28587:WIGOS-3430 Error Printing Voucher/Ticket development PinPad/TPV WiGos
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;
using WSI.Common.PinPad;
using System.Threading;

namespace WSI.Common
{
  class VoucherCardAddPinPadTransaction : Voucher
  {
    #region " const "

    public const String MANUAL_ENTRY_MODE = "MANUAL";

    #endregion 

    #region Class Attributes

    public enum VoucherPinPadCopyType
    {
      COMPANY = 0,
      COSTUMER = 1
    }
    #endregion

    public Boolean Show { get; set; }

    #region Constructor

    public VoucherCardAddPinPadTransaction(CurrencyExchangeResult ExchangeResult
                                          , String NationalCurrency
                                          , PrintMode Mode
                                          , Boolean ShowOnlyComission
                                          , PinPadOutput VoucherData
                                          , SqlTransaction Trx
                                          , VoucherPinPadCopyType CopyType)
      : base(Mode, CashierVoucherType.CardReplacement,  Trx)
    {
      // ATB 07-NOV-2016
      String _str_afiliation;
      String _str_reference;
      String _str_card_number;
      String _str_card_holder;
      String _str_bank_name;
      String _str_auth_code;
      String _str_terminal_id;
      String _str_control_number;
      String _str_expiration_date;
      String _str_card_type;
      String _str_product_type;
      String _str_aid;
      String _str_tvr;
      String _str_tsi;
      String _str_apn;
      String _str_al;
      Boolean _signature;

      Show = true;

      // ATB 07-NOV-2016
      _str_afiliation = VoucherData.PinPadTransaction.MerchantId.ToString();
      _str_reference = VoucherData.PinPadTransaction.Reference;
      _str_card_number = VoucherData.PinPadTransaction.CardNumber;
      _str_card_holder = VoucherData.PinPadTransaction.CardHolder;
      _str_bank_name = VoucherData.PinPadTransaction.BankName;
      _str_auth_code = VoucherData.PinPadTransaction.AuthCode;
      _str_terminal_id = VoucherData.PinPadTransaction.PinPadId.ToString();
      _str_control_number = VoucherData.PinPadTransaction.ControlNumber;

      _str_expiration_date = FormatDate(VoucherData);
     
      _str_card_type = VoucherData.CardType;
      _str_product_type = VoucherData.ProductType;
      _str_aid = VoucherData.AID;
      _str_tvr = VoucherData.TVR;
      _str_tsi = VoucherData.TSI;
      _str_apn = VoucherData.APN;
      _str_al = VoucherData.AL;


      _signature = VoucherData.WithSignature;

      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load Header and Footer
      //  3. Load general voucher data.
      //  4. Load NLS strings from resource.
      //  5. Load specific voucher data.


      // 1. Load HTML structure.
      LoadVoucher("VoucherCardAddBankCardTransaction");


      AddString("HTML_VOUCHER_BANK_HEADER", "Banorte");
      AddString("HTML_VOUCHER_SALE_HEADER", Resource.String("STR_EXTERNAL_PAYMENT_AND_SALE_VALIDATION_OPERATION_TYPE_SALE"));


      //  2. Load Header and Footer
      if (CopyType == VoucherPinPadCopyType.COMPANY)
      {
        AddString("HTML_STR_CUSTOMER_OR_COMPANY", Resource.String("STR_VOUCHER_PINPAD_COMPANY_COPY"));
      }
      else
      {
        AddString("HTML_STR_CUSTOMER_OR_COMPANY", Resource.String("STR_VOUCHER_PINPAD_COSTUMER_COPY"));
      }

      AddString("HTML_STR_TITLE_APPROVED", Resource.String("STR_TITLE_APPROVED") + ":");
      AddString("HTML_STR_TITLE_CODE", _str_auth_code);

      // 3. Load general voucher data.
      // ATB 07-NOV-2016
      AddString("HTML_STR_HEADER_VOUCHER_AFILIATION", Resource.String("STR_HEADER_VOUCHER_AFILIATION") + ":");
      AddString("HTML_STR_AFILIATION", _str_afiliation);

      AddString("HTML_STR_HEADER_VOUCHER_TERMINAL_ID", Resource.String("STR_HEADER_VOUCHER_TERMINAL_ID") + ":");
      AddString("HTML_STR_TERMINAL_ID", _str_terminal_id);

      AddString("HTML_STR_HEADER_CONTROL_NUMBER", Resource.String("STR_HEADER_VOUCHER_CONTROL_NUMBER") + ":");
      AddString("HTML_STR_CONTROL_NUMBER", _str_control_number);

      AddString("HTML_STR_HEADER_VOUCHER_REFERENCE", Resource.String("STR_HEADER_VOUCHER_REFERENCE") + ":");
      AddString("HTML_STR_REFERENCE", _str_reference);

      AddString("HTML_STR_FORMAT_GENERAL_NAME_CARD", Resource.String("STR_FORMAT_GENERAL_NAME_CARD") + ":");
      AddString("HTML_STR_CARD_NUMBER", _str_card_number);

      AddString("HTML_STR_BANK_TRANSACTION_BANK_HOLDER_NAME", Resource.String("STR_BANK_TRANSACTION_BANK_HOLDER_NAME") + ":");
      AddString("HTML_STR_CARD_HOLDER", _str_card_holder);

      AddString("STR_FRM_HANDPAYS_006", Resource.String("STR_FRM_HANDPAYS_006") + ":");

      AddStringNotNull(_str_bank_name, Resource.String("STR_ACCOUNT_PAYMENT_BANK"), "HTML_STR_BANK_NAME", "HTML_STR_ACCOUNT_PAYMENT_BANK");

      AddStringNotNull(_str_expiration_date, Resource.String("STR_VOUCHER_VALIDITY"), "HTML_STR_EXPIRATION_DATE", "HTML_STR_FORMAT_EXPIRATION_DATE");

      AddStringNotNull(_str_card_type, Resource.String("STR_VOUCHER_CARD_TIPO"), "HTML_STR_CARD_TYPE", "HTML_STR_FORMAT_TYPE_CARD");

      AddStringNotNull(_str_product_type, Resource.String("STR_UC_TICKET_TERMINAL_DATA_TIPO"), "HTML_STR_PRODUCT_NAME", "HTML_STR_FORMAT_PRODUCT_NAME");

      if (VoucherData.WithSignature)
      {
        AddString("VOUCHER_SIGNATURE", Resource.String("STR_VOUCHER_ELECTRONIC_SIGNATURE"));
      }
      else
      {
        AddString("VOUCHER_SIGNATURE", Resource.String("STR_VOUCHER_AUTHORIZED_BY_SIGNATURE"));
      }

      LoadVoucherGeneralData();

      if (ExchangeResult.WasForeingCurrency)
      {
        //  5. Load specific voucher data.
        // In amount
        AddString("VOUCHER_IN_AMOUNT", Currency.Format(ExchangeResult.InAmount, ExchangeResult.InCurrencyCode));

        // Total amount
        AddString("VOUCHER_TOTAL_AMOUNT", Currency.Format(ExchangeResult.InAmount, NationalCurrency));
      }
      else
      {
        //  5. Load specific voucher data.
        // In amount
        AddString("VOUCHER_IN_AMOUNT", ((Currency)ExchangeResult.InAmount).ToString());

        // Total amount
        AddString("VOUCHER_TOTAL_AMOUNT", ((Currency)ExchangeResult.InAmount).ToString());
      }

      AddStringNotNull(_str_aid, Resource.String("STR_VOUCHER_AID"), "HTML_STR_AID", "HTML_STR_FORMAT_AID");
      AddStringNotNull(_str_tvr, Resource.String("STR_VOUCHER_TVR"), "HTML_STR_TVR", "HTML_STR_FORMAT_TVR");
      AddStringNotNull(_str_tsi, Resource.String("STR_VOUCHER_TSI"), "HTML_STR_TSI", "HTML_STR_FORMAT_TSI");

      if (!String.IsNullOrEmpty(_str_apn))
      {
        AddString("HTML_STR_FORMAT_APN_O_AL", Resource.String("STR_VOUCHER_APN") + ":");
        AddString("HTML_STR_APN_O_AL", _str_apn);
      }
      else
      {
        AddStringNotNull(_str_al, Resource.String("STR_VOUCHER_AL"), "HTML_STR_APN_O_AL", "HTML_STR_FORMAT_APN_O_AL");
      }

      //  4. Load NLS strings from resource
      if (!LoadVoucherResources())
      {
        return;
      }

    }

    /// <summary>
    /// Format date returned by bank
    /// </summary>
    /// <param name="VoucherData"></param>
    /// <returns></returns>
    private String FormatDate(PinPadOutput VoucherData)
    {
      String _return_date;
      String _month;
      String _year;

      if (VoucherData.EntryMode == MANUAL_ENTRY_MODE)
      {
        _month = VoucherData.ExpirationDate.Substring(0, 2);
        _year = VoucherData.ExpirationDate.Substring(2, 2);
      }
      else
      {
        _month = VoucherData.ExpirationDate.Substring(2, 2);
        _year = VoucherData.ExpirationDate.Substring(0, 2);
      }

      _return_date = String.Format("{0}/{1}", _month, _year);

      return _return_date;

    } // FormatDate

    #endregion

    #region Private Methods

    /// <summary>
    /// Adds String if property is not null
    /// </summary>
    /// <param name="Property"></param>
    /// <param name="Tittle"></param>
    /// <param name="PropertyStr"></param>
    /// <param name="TitleStr"></param>
    private void AddStringNotNull(String Property, String Tittle, String PropertyStr, String TitleStr)
    {
      if (String.IsNullOrEmpty(Property))
      {
        AddString(TitleStr, "");
        AddString(PropertyStr, "");
      }
      else
      {
        AddString(TitleStr, Tittle + ":");
        AddString(PropertyStr, Property );
      }
    } // AddStringNotNull
    
    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private Boolean LoadVoucherResources()
    {
      String value = "";

      // NLS Values

      value = GeneralParam.GetString("Cashier.Voucher", "PaymentMethod.BankCard.Title");
      if (String.IsNullOrEmpty(value))
      {
        value = Resource.String("STR_VOUCHER_CURRENCY_CARD_EXCHANGE");
      }

      // - General
      value = Resource.String("STR_VOUCHER_IN_CHARGED") + ":";
      AddString("STR_VOUCHER_IN_AMOUNT", value);

      value = Misc.GetCardPlayerConceptName() + ":";
      AddString("STR_VOUCHER_CARD_PRICE", value);

      value = Resource.String("STR_VOUCHER_COMISSION") + ":";
      AddString("STR_VOUCHER_COMISSION", value);

      value = Resource.String("STR_VOUCHER_RECHARGE") + ":";
      AddString("STR_VOUCHER_TOTAL_AMOUNT", value);

      return true;
    }

    #endregion
  }
}
