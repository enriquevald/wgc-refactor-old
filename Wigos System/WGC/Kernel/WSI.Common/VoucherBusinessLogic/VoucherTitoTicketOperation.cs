﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherTitoTicketOperation.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  // Tito Ticket Operation
  // Format:
  //          Voucher Logo
  //          Voucher Header
  //
  // Cashier Session:
  // User: 
  // Voucher Id:
  // Validation Number    Amount
  //
  //         Voucher Footer
  //
  public class VoucherTitoTicketOperation : Voucher
  {
    #region Constructor

    public VoucherTitoTicketOperation(String[] VoucherAccountInfo
                           , OperationCode OperationCode
                           , Boolean IsDiscardOperation
                           , List<VoucherValidationNumber> ValidationNumbers
                           , PrintMode Mode
                           , SqlTransaction SQLTransaction
                           , Ticket Ticket = null)
      : base(Mode, SQLTransaction)
    {
      String _str_html;
      String _title_voucher;
      String _ticket_label;
      String _msg_info;

      _title_voucher = String.Empty;
      _ticket_label = String.Empty;
      _msg_info = String.Empty;

      if (IsDiscardOperation)
      {
        _title_voucher += Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_VOIDING") + ": ";
      }

      switch (OperationCode)
      {
        case OperationCode.TITO_OFFLINE:
          {
            _title_voucher += Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_OFFLINE_TICKET");


            if (ValidationNumbers[0].TicketType == TITO_TICKET_TYPE.PROMO_NONREDEEM)
            {
              _msg_info = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_MSG_INFO_NR", (Currency)ValidationNumbers[0].Amount);
              _ticket_label = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_TICKETS_VOIDED");
            }
            else
            {
              _msg_info = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_MSG_INFO_REDEEMED", (Currency)ValidationNumbers[0].Amount);
              _ticket_label = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_TICKETS_REDEEMED");
            }
          }
          break;

        case OperationCode.TITO_REISSUE:
          {
            _title_voucher += Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_REISSUE_TICKET");
            _ticket_label = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_TICKETS_VOIDED");

            if (ValidationNumbers[0].TicketType == TITO_TICKET_TYPE.PROMO_NONREDEEM)
            {
              _msg_info = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_MSG_INFO_NR", (Currency)ValidationNumbers[0].Amount);
            }
            else
            {
              _msg_info = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_MSG_INFO_RE", (Currency)ValidationNumbers[0].Amount);
            }
          }
          break;

        case OperationCode.PROMOTION:
          {
            _title_voucher += Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_PROMO_TICKET");

            if (IsDiscardOperation)
            {
              _ticket_label = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_TICKETS_VOIDED");

              if (ValidationNumbers[0].TicketType == TITO_TICKET_TYPE.PROMO_NONREDEEM)
              {
                _msg_info = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_MSG_INFO_NR_NUM", new object[] { ValidationNumbers.Count, (Currency)ValidationNumbers[0].Amount });
              }
              else
              {
                _msg_info = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_MSG_INFO_RE_NUM", new object[] { ValidationNumbers.Count, (Currency)ValidationNumbers[0].Amount });
              }
            }
            else
            {
              _ticket_label = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_TICKETS");
            }
          }
          break;

        case OperationCode.TITO_TICKET_VALIDATION:
          {
            _title_voucher += Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_TICKET_IN");

            _ticket_label = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_TICKETS_IN");

            if (ValidationNumbers[0].TicketType == TITO_TICKET_TYPE.PROMO_NONREDEEM)
            {
              _msg_info = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_TICKET_IN_MSG_INFO_NR_NUM");
            }
            else
            {
              _msg_info = Resource.String("STR_VOUCHER_TITO_TICKET_OPERATION_TICKET_IN_MSG_INFO_RE_NUM");
            }
          }
          break;

        case OperationCode.TITO_VOID_MACHINE_TICKET:
          {
            _title_voucher += Resource.String("STR_VOUCHER_TITO_MACHINE_TICKET_VOID_TITLE");

            if (Ticket != null)
            {
              _ticket_label = Resource.String("STR_VOUCHER_TITO_MACHINE_TICKET_MACHINE_TITLE", Ticket.CreatedTerminalName);
            }

            _msg_info = "";
          }
          break;

        default:
          {
            Log.Error("VoucherTitoTicketOperation. OperationCode not valid: " + OperationCode.ToString());
            return;
          }
      }

      LoadVoucher("VoucherTitoTicketOperation");

      AddString("STR_VOUCHER_TITO_TICKET_OPERATION", _title_voucher);

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load specific voucher data.

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      if (ValidationNumbers != null)
      {
        _str_html = "<tr><td><p>";
        _str_html += _ticket_label;
        _str_html += "</p></td></tr>";
        for (int _idx = 0; _idx < ValidationNumbers.Count; _idx++)
        {
          _str_html += "<tr>";
          _str_html += "<td align=\"left\"  width=\"75%\">@VALIDATION_NUMBER</td>";
          _str_html += "<td align=\"right\" width=\"25%\">@VALUE</td>";
          _str_html += "</tr>";

          _str_html = _str_html.Replace("@VALIDATION_NUMBER", ValidationNumberManager.FormatValidationNumber(ValidationNumbers[_idx].ValidationNumber, (Int32)ValidationNumbers[_idx].ValidationType, true));
          _str_html = _str_html.Replace("@VALUE", HttpUtility.HtmlEncode(ValidationNumbers[_idx].Amount.ToString()));
        }
        _str_html += "<tr><td>&nbsp;</td></tr>";

        if (!String.IsNullOrEmpty(_msg_info))
        {
          _str_html += "<tr><td>&nbsp;</td></tr>";
          _str_html += "<tr><td colspan=2><p>";
          _str_html += _msg_info;
          _str_html += "</p></td></tr>";
          _str_html += "<tr><td>&nbsp;</td></tr>";
        }

        SetParameterValue("@VALIDATION_NUMBERS", _str_html);
      }
      else
      {
        SetParameterValue("@VALIDATION_NUMBERS", "");
      }

    } // VoucherCashOut

    #endregion
  }
}
