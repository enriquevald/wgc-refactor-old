﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCardHandpay.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  // 12. HandPay Generation
  // Format:
  //     Voucher Logo
  //    Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //    Voucher Title
  //
  // Card Id: 
  // Account Id:
  //
  // Initial Balance: $
  // Handpay:         $
  // Final Balance:   $
  //
  //   Final Balance
  //       $$$$
  //
  //   Voucher Footer
  /// <summary>
  /// Add Credit Voucher
  /// </summary>
  public class VoucherCardHandpay : Voucher
  {
    #region Class Attributes


    public class VoucherCardHandpayInputParams
    {
      public String[] VoucherAccountInfo;
      public String TerminalName;
      public String TerminalProvider;
      public HANDPAY_TYPE Type;
      public String TypeName;
      public Currency CardBalance;
      public Decimal Amount;
      public Currency SessionPlayedAmount;
      public String DenominacionStr;
      public String DenominacionValue;
      public PrintMode Mode;
      public VoucherValidationNumber ValidationNumber;
      public Decimal InitialAmount;
      public Decimal TaxBaseAmount;
      public Decimal TaxAmount;
      public Decimal TaxPct;
      public Decimal Deductions;
    }

    #endregion

    #region Constructor

    public VoucherCardHandpay(VoucherCardHandpayInputParams InputParams, OperationCode OperationCode, SqlTransaction SQLTransaction)
      : this(InputParams, OperationCode, false, SQLTransaction)
    {
      
    }

    public VoucherCardHandpay(VoucherCardHandpayInputParams InputParams, OperationCode OperationCode, Boolean AutomaticPayment, SqlTransaction SQLTransaction)
      : base(InputParams.Mode, OperationCode, SQLTransaction)
    {
      Boolean _gm_room;
      Boolean _sign_room;
      Boolean _print_played_amount;
      String _str_html;
      Currency _deductions;
      Currency _rounding;


      _deductions = -(InputParams.Deductions);

      _rounding = (InputParams.Amount - InputParams.InitialAmount) + InputParams.TaxAmount;

      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCardHandpay");

      // 2. Load voucher resources.
      LoadVoucherResources(AutomaticPayment);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      // Setup voucher's data
      //    - Account Number and Account Holder Name
      //    - Card Number (Trackdata) 
      //    - Initial Balance
      //    - Handpay Amount
      //    - Final Balance

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", InputParams.VoucherAccountInfo);

      AddString("VOUCHER_CARD_HANDPAY_TERMINAL_NAME", InputParams.TerminalName);
      AddString("VOUCHER_CARD_HANDPAY_PROVIDER_NAME", InputParams.TerminalProvider);
      AddString("VOUCHER_CARD_HANDPAY_TYPE", InputParams.TypeName);
      AddCurrency("VOUCHER_CARD_HANDPAY_AMOUNT_TO_TRANSFER", InputParams.Amount);

      if (Utils.IsTitoMode())
      {
        AddString("VOUCHER_CARD_HANDPAY_INITIAL_BALANCE_AMOUNT", "");
        AddString("VOUCHER_CARD_HANDPAY_FINAL_BALANCE_AMOUNT", "");
        AddCurrency("VOUCHER_CARD_HANDPAY_INITIAL_AMOUNT_TO_TRANSFER", InputParams.InitialAmount);
        AddCurrency("VOUCHER_CARD_HANDPAY_DEDUCTIONS", _deductions);
      }
      else
      {
        AddCurrency("VOUCHER_CARD_HANDPAY_INITIAL_BALANCE_AMOUNT", InputParams.CardBalance);
        AddCurrency("VOUCHER_CARD_HANDPAY_FINAL_BALANCE_AMOUNT", InputParams.CardBalance + InputParams.Amount);
        AddString("VOUCHER_CARD_HANDPAY_INITIAL_AMOUNT_TO_TRANSFER", "");
        AddString("VOUCHER_CARD_HANDPAY_DEDUCTIONS", "");
      }

      if (Utils.IsTitoMode() && _rounding != 0)
      {
        AddString("STR_VOUCHER_CARD_HANDPAY_ROUNDING", Resource.String("STR_UC_CARD_BALANCE_ROUNDING") + ":");
        AddCurrency("VOUCHER_CARD_HANDPAY_ROUNDING", _rounding);
      }
      else
      {
        AddString("STR_VOUCHER_CARD_HANDPAY_ROUNDING", "");
        AddString("VOUCHER_CARD_HANDPAY_ROUNDING", "");
      }

      if (InputParams.ValidationNumber != null)
      {
        _str_html = "<tr><td><p>";
        _str_html += Resource.String("STR_VOUCHER_VALIDATION_NUMBERS_TITLE");
        _str_html += "</p></td></tr>";

        _str_html += "<td colspan=\"2\" align=\"left\">@TICKET_TITO_TERMINAL_NAME</td>";
        _str_html = _str_html.Replace("@TICKET_TITO_TERMINAL_NAME", HttpUtility.HtmlEncode(Resource.String("STR_VOUCHER_TERMINAL_ID") + ": " + InputParams.ValidationNumber.CreatedTerminalName));

        _str_html += "<tr>";
        _str_html += "<td align=\"left\"  width=\"80%\">&nbsp&nbsp@VALIDATION_NUMBER</td>";
        _str_html += "<td align=\"right\" width=\"20%\">@VALUE</td>";
        _str_html += "</tr>";

        _str_html = _str_html.Replace("@VALIDATION_NUMBER", ValidationNumberManager.FormatValidationNumber(InputParams.ValidationNumber.ValidationNumber, (Int32)InputParams.ValidationNumber.ValidationType, true));
        _str_html = _str_html.Replace("@VALUE", HttpUtility.HtmlEncode(InputParams.ValidationNumber.Amount.ToString()));

        _str_html += "<tr><td>&nbsp;</td></tr>";
        SetParameterValue("@VALIDATION_NUMBERS", _str_html);
      }
      else
      {
        SetParameterValue("@VALIDATION_NUMBERS", "");
      }
      // If Manual Handpay return. Don't show any of the optional sections...
      if (InputParams.Type == HANDPAY_TYPE.MANUAL)
      {
        SetParameterValue("@VOUCHER_CARD_HANDPAY_LEAVE_ROOM", "");
        SetParameterValue("@VOUCHER_CARD_HANDPAY_SIGNATURES_ROOM", "");
        SetParameterValue("@VOUCHER_CARD_HANDPAY_GAME_METERS_ROOM", "");
        SetParameterValue("@VOUCHER_CARD_HANDPAY_PRINT_PLAYED_AMOUNT", "");

        return;
      }

      // RCI 22-NOV-2011: Exists 3 GeneralParams to indicate if a section in the voucher has to be printed.
      _sign_room = GeneralParam.GetBoolean("Cashier.Voucher", "Handpay.SignaturesRoom");
      _gm_room = GeneralParam.GetBoolean("Cashier.Voucher", "Handpay.GameMetersRoom");
      _print_played_amount = GeneralParam.GetBoolean("Cashier.Voucher", "Handpay.PrintPlayedAmount");

      // If have to print any of them, leave extra space after final balance.
      if (_sign_room || _gm_room || _print_played_amount)
      {
        SetParameterValue("@VOUCHER_CARD_HANDPAY_LEAVE_ROOM",
                     "<tr><td>&nbsp;</td></tr>" +
                     "<tr><td>&nbsp;</td></tr>");
      }
      else
      {
        SetParameterValue("@VOUCHER_CARD_HANDPAY_LEAVE_ROOM", "");
      }

      SetSignatureRoomByVoucherType(this.GetType());

      if (_gm_room)
      {
        SetParameterValue("@VOUCHER_CARD_HANDPAY_GAME_METERS_ROOM",
          "<center><b>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_GM_TITLE") + "</b></center>" +
          "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">" +
            "<tr>" +
              "<td>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_GM_PLAYED") + "</td>" +
              "<td>$____________</td>" +
              "<td>#____________</td>" +
            "</tr>" +
            "<tr>" +
              "<td>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_GM_WON") + "</td>" +
              "<td>$____________</td>" +
              "<td>#____________</td>" +
            "</tr>" +
            "<tr>" +
              "<td>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_GM_JACKPOT") + "</td>" +
              "<td>$____________</td>" +
              "<td>&nbsp;</td>" +
            "</tr>" +
            "<tr>" +
              "<td>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_GM_DROP") + "</td>" +
              "<td>$____________</td>" +
              "<td>&nbsp;</td>" +
            "</tr>" +
          "</table>" +
          "<hr noshade size=\"4\">");
      }
      else
      {
        SetParameterValue("@VOUCHER_CARD_HANDPAY_GAME_METERS_ROOM", "");
      }

      if (_print_played_amount)
      {
        if (InputParams.DenominacionValue != "")
        {
          InputParams.DenominacionStr += ":";
        }

        SetParameterValue("@VOUCHER_CARD_HANDPAY_PRINT_PLAYED_AMOUNT",
          "<center><b>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_PRINT_PLAYED_AMOUNT_TITLE") + "</b></center>" +
          "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">" +
            "<tr>" +
              "<td align=\"left\"  width=\"50%\">" + InputParams.DenominacionStr + "</td>" +
              "<td align=\"right\" width=\"50%\">" + InputParams.DenominacionValue + "</td>" +
            "</tr>" +
            "<tr>" +
              "<td align=\"left\"  width=\"50%\">" + Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_PLAYED_AMOUNT") + ":</td>" +
              "<td align=\"right\" width=\"50%\">" + (InputParams.SessionPlayedAmount == -1 ? "---" : InputParams.SessionPlayedAmount.ToString()) + "</td>" +
            "</tr>" +
          "</table>" +
          "<hr noshade size=\"4\">");
      }
      else
      {
        SetParameterValue("@VOUCHER_CARD_HANDPAY_PRINT_PLAYED_AMOUNT", "");
      }

      if (WSI.Common.TITO.Utils.IsTitoMode())
      {
        String _param_name_tax;
        String _pct;
        StringBuilder _html_code;

        _html_code = new StringBuilder();

        _html_code.AppendLine("<tr>");
        _html_code.AppendLine("	<td>");
        _html_code.AppendLine("		<table width='100%' border='0' cellpadding='0' cellspacing='0'>");
        _html_code.AppendLine("			<tr>");
        _html_code.AppendLine("				<td colspan='3'>");
        _html_code.AppendLine("					@VOUCHER_TAX_DETAIL:");
        _html_code.AppendLine("				</td>");
        _html_code.AppendLine("			</tr>");
        _html_code.AppendLine("			<tr>");
        _html_code.AppendLine("				<td width='5%'></td>");
        _html_code.AppendLine("				<td>");
        _html_code.AppendLine("					@VOUCHER_BASE_TAX_NAME:");
        _html_code.AppendLine("				</td>");
        _html_code.AppendLine("				<td width='40%' align='right'>");
        _html_code.AppendLine("					@VOUCHER_BASE_TAX");
        _html_code.AppendLine("				</td>");
        _html_code.AppendLine("			</tr>");
        _html_code.AppendLine("			@TAXES");
        _html_code.AppendLine("		</table>");
        _html_code.AppendLine("		<hr noshade size='4' /> ");
        _html_code.AppendLine("	</td>");
        _html_code.AppendLine("</tr>");

        SetParameterValue("@TABLE_WITHHOLDING", _html_code.ToString());

        _param_name_tax = Resource.String("STR_VOUCHER_HANDPAY_WITHHOLDING");
        _pct = String.Format("{0:P2}", InputParams.TaxPct);

        AddString("VOUCHER_TAX_DETAIL", Resource.String("STR_HANDPAY_TAX_DETAIL", _param_name_tax.ToString()));
        AddString("VOUCHER_BASE_TAX_NAME", Resource.String("STR_BASE"));
        SetParameterValue("@TAXES", DevolutionPrizeParts.GetTaxesHtml(InputParams.TaxBaseAmount, true, true, false, (_deductions != 0), OperationCode));

        // MPO 17-MAR-2015: WIG-2154: It isn't necessary show the base amount when there aren't tax amount.
        AddCurrency("VOUCHER_BASE_TAX", InputParams.TaxAmount != 0 ? InputParams.TaxBaseAmount : 0);

        SetParameterValue("@HR_CASHLESS_SEPARATOR", String.Empty);
      }
      else
      {
        StringBuilder _html_code;

        _html_code = new StringBuilder();

        _html_code.AppendLine("<hr noshade size='4' />");

        SetParameterValue("@TABLE_WITHHOLDING", String.Empty);
        SetParameterValue("@HR_CASHLESS_SEPARATOR", _html_code.ToString());
      }

    } // VoucherCardHandpay

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(Boolean AutomaticPayment)
    {
      // Images
      // AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Strings

      // Header
      AddString("STR_VOUCHER_SITE_ID", Resource.String("STR_VOUCHER_SITE_ID") + ":");
      AddString("STR_VOUCHER_TERMINAL_USERNAME", Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":");
      AddString("STR_VOUCHER_AUTHORIZED_BY", Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":");
      AddString("STR_VOUCHER_TERMINAL_ID", Resource.String("STR_VOUCHER_TERMINAL_ID") + ":");
      AddString("STR_VOUCHER_VOUCHER_ID", Resource.String("STR_VOUCHER_VOUCHER_ID") + ":");

      // Title
      AddString("STR_VOUCHER_TITLE", AutomaticPayment ? Resource.String("STR_VOUCHER_CARD_HANDPAY_015") : Resource.String("STR_VOUCHER_CARD_HANDPAY_001"));

      // Details
      AddString("STR_VOUCHER_CARD_HANDPAY_TERMINAL_NAME", Resource.String("STR_VOUCHER_CARD_HANDPAY_008") + ":");
      AddString("STR_VOUCHER_CARD_HANDPAY_PROVIDER_NAME", Resource.String("STR_VOUCHER_CARD_HANDPAY_009") + ":");
      AddString("STR_VOUCHER_CARD_HANDPAY_TYPE", Resource.String("STR_VOUCHER_CARD_HANDPAY_010") + ":");
      AddString("STR_VOUCHER_CARD_HANDPAY_AMOUNT_TO_TRANSFER", Resource.String("STR_VOUCHER_CARD_HANDPAY_005") + ":");

      if (Utils.IsTitoMode())
      {
        AddString("STR_VOUCHER_CARD_HANDPAY_INITIAL_BALANCE_AMOUNT", "");
        AddString("STR_VOUCHER_CARD_HANDPAY_FINAL_BALANCE_AMOUNT", "");
        AddString("STR_VOUCHER_CARD_HANDPAY_INITIAL_AMOUNT_TO_TRANSFER", Resource.String("STR_VOUCHER_CARD_HANDPAY_001") + ":");
        AddString("STR_VOUCHER_CARD_HANDPAY_DEDUCTIONS", Resource.String("STR_VOUCHER_HANDPAY_WITHHOLDING") + ":");
        AddString("STR_VOUCHER_FINAL_TITLE", "");
      }
      else
      {
        AddString("STR_VOUCHER_CARD_HANDPAY_INITIAL_BALANCE_AMOUNT", Resource.String("STR_VOUCHER_CARD_HANDPAY_004") + ":");
        AddString("STR_VOUCHER_CARD_HANDPAY_FINAL_BALANCE_AMOUNT", Resource.String("STR_VOUCHER_CARD_HANDPAY_006") + ":");
        AddString("STR_VOUCHER_CARD_HANDPAY_INITIAL_AMOUNT_TO_TRANSFER", "");
        AddString("STR_VOUCHER_CARD_HANDPAY_DEDUCTIONS", "");
        AddString("STR_VOUCHER_FINAL_TITLE", Resource.String("STR_VOUCHER_CARD_HANDPAY_007"));
      }
    }

    #endregion

    #region Public Methods

    //public void VoucherValuesCardHandpay(CashierBusinessLogic.TYPE_CARD_HANDPAY Params)
    //{
    //  //  1. Depósito
    //  //     ---------------------------
    //  //  2. Total
    //  //     ---------------------------

    //  //Params.account_id
    //  //Params.track_data
    //  //Params.amount
    //  //Params.terminal_name
    //  //Params.provider_name

    //  //AddString("VOUCHER_OLD_CARD_ID", Params.old_track_data);
    //  //AddString("VOUCHER_NEW_CARD_ID", Params.new_track_data);
    //  //AddString("VOUCHER_ACCOUNT_ID", Params.account_id.ToString());
    //  //AddCurrency("VOUCHER_CARD_REPLACEMENT_PRICE", Params.card_replacement_price);
    //  //AddCurrency("VOUCHER_CARD_REPLACEMENT_TOTAL_AMOUNT", Params.card_replacement_price);

    //} // VoucherValuesCardHandpay

    #endregion
  }
}
