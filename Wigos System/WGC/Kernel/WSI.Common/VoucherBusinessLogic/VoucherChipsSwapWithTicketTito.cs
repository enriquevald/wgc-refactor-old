﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherChipsSwapWithTicketTito.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  //ESE 17-Mar-2016
  public class VoucherChipsSwapWithTicketTito : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherChipsSwapWithTicketTito(String[] VoucherAccountInfo
                              , TYPE_SPLITS Splits
                              , FeatureChips.ChipsOperation ChipsOperation
                              , IDictionary<long, decimal> Tickets
                              , Boolean IsDealerCopy
                              , PrintMode Mode
                              , SqlTransaction Trx)
      : base(Mode, Trx)
    {
      // 1. Load HTML structure.
      LoadVoucher("VoucherChipsSwapWithTicketTito");

      //  4. Load NLS strings from resource
      LoadVoucherResources(VoucherAccountInfo, Splits, ChipsOperation, Tickets, IsDealerCopy);
    }

    #endregion

    #region Private Methods
    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(String[] VoucherAccountInfo
                                    , TYPE_SPLITS Splits
                                    , FeatureChips.ChipsOperation ChipsOperation
                                    , IDictionary<long, decimal> Tickets
                                    , Boolean IsDealerCopy)
    {
      String _str_value;
      StringBuilder _sb_chips_denomination_header;
      StringBuilder _sb_chips_denomination;
      String _hr_4;
      String _hr_2;
      StringBuilder _str_ticket_head;
      StringBuilder _str_ticket_body;
      StringBuilder _str_mount;

      _str_ticket_head = new StringBuilder();
      _str_ticket_body = new StringBuilder();
      _str_mount = new StringBuilder();
      _sb_chips_denomination_header = new StringBuilder();
      _sb_chips_denomination = new StringBuilder();
      _hr_2 = String.Empty;
      _hr_4 = String.Empty;
      _str_value = String.Empty;

      //  2. Load Header and Footer
      LoadHeader(Splits.company_a.header);
      LoadFooter(Splits.company_a.footer, Splits.company_a.footer_cashin);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      SetValue("CV_TYPE", (Int32)CashierVoucherType.ChipsSwap);

      //Load the gaming table
      AddString("VOUCHER_GAMING_TABLE_NAME", ChipsOperation.SelectedGamingTable.TypeName + " - " + ChipsOperation.SelectedGamingTable.TableName);


      //Voucher title
      _str_value = Resource.String("STR_VOUCHER_SWAP_CHIPS_TITLE");
      AddString("STR_VOUCHER_SWAP_CHIPS_TITLE", _str_value);

      if (!IsDealerCopy)
      {
        //Voucher subtitle
        AddString("STR_VOUCHER_CASH_CHIPS_DEALER_COPY", String.Empty);
      }
      else
      {
        //Voucher subtitle
        AddString("STR_VOUCHER_CASH_CHIPS_DEALER_COPY", GeneralParam.GetString("GamingTables", "Cashier.Mode.DealerCopy.Title"));
      }

      //For the swap tickets TITO
      _str_ticket_head.AppendLine("	<tr>  ");
      _str_ticket_head.AppendLine("    <td align='left' width='50%'>" + Resource.String("STR_VOUCHER_SWAP_SUBTITLE") + "</td>");
      _str_ticket_head.AppendLine("    <td align='right' width='50%'></td>");
      _str_ticket_head.AppendLine("	</tr>  ");
      SetParameterValue("@VOUCHER_SWAP_TICKETS_HEAD", _str_ticket_head.ToString());
      foreach (KeyValuePair<long, decimal> kvp in Tickets)
      {
        //Obtain all the ticket data
        Ticket _aux_ticket;
        _aux_ticket = Ticket.LoadTicket(kvp.Key);

        _str_ticket_body.AppendLine("	<tr>  ");
        _str_ticket_body.AppendLine("    <td align='left' width='75%'>" + ValidationNumberManager.FormatValidationNumber(_aux_ticket.ValidationNumber, (Int32)TITO_VALIDATION_TYPE.SYSTEM, false) + "</td>");
        _str_ticket_body.AppendLine("    <td align='right' width='25%'>" + decimal.Round(kvp.Value, 2).ToString("C") + "</td>");
        _str_ticket_body.AppendLine("	</tr>  ");
      }
      SetParameterValue("@VOUCHER_SWAP_TICKETS_BODY", _str_ticket_body.ToString());


      _str_value = Resource.String("STR_VOUCHER_CARD_REDEEM_TOTAL");
      AddString("STR_VOUCHER_CASH_CHIPS_TOTAL", _str_value);
      AddCurrency("VOUCHER_CASH_CHIPS_TOTAL", ChipsOperation.ChipsAmount);

      //Diference between tickets - chips
      _str_value = Resource.String("STR_VOUCHER_SWAP_DESK_CLOSE_COLL_DIFF_MISSING");
      AddString("STR_VOUCHER_CASH_CHIPS_REFUND", _str_value);
      AddCurrency("VOUCHER_CASH_CHIPS_REFUND", ChipsOperation.ChipsDifference);

      //Header from the table chips
      _sb_chips_denomination_header.AppendLine("	<tr>  ");
      _sb_chips_denomination_header.AppendLine("	   <td align='right' width='30%'>" + Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_DENOMINATION") + "</td> ");
      _sb_chips_denomination_header.AppendLine("	   <td align='right' width='30%'>" + Resource.String("STR_VOUCHER_CASH_CHIPS_QUANTITY") + "</td> ");
      _sb_chips_denomination_header.AppendLine("	   <td align='right' width='40%'>" + Resource.String("STR_VOUCHER_CASH_CHIPS_SUBTOTAL") + "</td> ");
      _sb_chips_denomination_header.AppendLine("	</tr> ");

      foreach (KeyValuePair<FeatureChips.Chips.Chip, Int32> _chip in ChipsOperation.ChipsItemsUnits)
      {
        if (_chip.Value > 0)
        {
          _sb_chips_denomination.AppendLine("	<tr>  ");
          _sb_chips_denomination.AppendLine("	   <td align='right'  width='30%'>" + ((Currency)_chip.Key.Denomination).ToString() + "</td> ");
          _sb_chips_denomination.AppendLine("	   <td align='right' width='30%'>" + _chip.Value + "</td> ");
          _sb_chips_denomination.AppendLine("	   <td align='right' width='40%'>" + ((Currency)_chip.Key.Denomination) * _chip.Value + "</td> ");
          _sb_chips_denomination.AppendLine("	</tr> ");
        }
      }
      _hr_2 = "<hr noshade size='2'>";
      _hr_4 = "<hr noshade size='4'>";

      SetParameterValue("@STR_HR_SIZE_4", _hr_4);
      SetParameterValue("@STR_HR_SIZE_2", _hr_2);

      SetParameterValue("@VOUCHER_DESCRIPTION_CHIPS", _sb_chips_denomination_header.ToString());
      SetParameterValue("@VOUCHER_DENOMINATION_CHIPS", _sb_chips_denomination.ToString());
    }
    #endregion
  }
}
