﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCashDeskWithdraw.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
// 01-FEB-2017  JML         Fixed Bug 23895: Tables: When making a Fill and Credit (old Deposit and Retirement) the vouchers are entitled "Deposit of Cash" and "Withdrawal of Cash"
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  // 5. Cash Desk Withdraw
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  // Initial Amount:   $
  // Close Amount:     $
  //
  //  Final Balance:  $
  //
  //         Voucher Footer
  public class VoucherCashDeskWithdraw : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCashDeskWithdraw(CASHIER_MOVEMENT MovementType, PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      InitializeVoucher(MovementType);
    }

    public VoucherCashDeskWithdraw(CASHIER_MOVEMENT MovementType, PrintMode Mode, SqlTransaction SQLTransaction, CashierVoucherType CashierVoucherType)
      : base(Mode, CashierVoucherType, SQLTransaction)
    {
      InitializeVoucher(MovementType);
    }

    private void InitializeVoucher(CASHIER_MOVEMENT MovementType)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCashDeskWithdraw");

      // 2. Load voucher resources.
      LoadVoucherResources(MovementType);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(CASHIER_MOVEMENT MovementType)
    {
      String _value = "";
      Int32 _gaming_table_system_user_id;
      String _gaming_table_system_user_name;

      if (!Cashier.GetSystemUser(GU_USER_TYPE.SYS_GAMING_TABLE, this.SqlTrx, out _gaming_table_system_user_id, out _gaming_table_system_user_name))
      {
        _gaming_table_system_user_id = 0;
        _gaming_table_system_user_name = String.Empty;
      }

      // Images
      // AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Values

      // - Title
      if (CommonCashierInformation.UserId == _gaming_table_system_user_id)
      {
        _value = Resource.String("STR_VOUCHER_GAMING_TABLE_FILL_OUT_TITLE");
      }
      else
      {
        _value = Resource.String("STR_VOUCHER_CASH_DESK_FILL_OUT_TITLE");
      }

      if (MovementType == CASHIER_MOVEMENT.CLOSE_SESSION)
      {
        _value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TITLE");
      }
      AddString("STR_VOUCHER_CASH_DESK_WITHDRAW_TITLE", _value);

      // - General
      _value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", _value);

      _value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", _value);

      _value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", _value);

      _value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", _value);

      _value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", _value);

    } // LoadVoucherResources

    #endregion

    #region Public Methods

    #endregion

  }
}
