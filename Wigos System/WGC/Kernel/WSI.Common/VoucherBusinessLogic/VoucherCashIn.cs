﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCashIn.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
// 27-JUL-2016  EOR         Product Backlog Item 15248: TPV Televisa: Cashier, Vouchers and Tickets
// 02-AUG-2016  EOR         Product Backlog Item 16149: Codere - Tax Custody: Add tax custody concept in ticket recharge
// 02-AGO-2016  ESE         Product Backlog Item 15248:TPV Televisa: Hide voucher recharge
// 28-SEP-2016  EOR         PBI 17448: Televisa - Sorteo de Caja (Premio en efectivo) - Voucher
// 29-SEP-2016  EOR         PBI 17448: Televisa - Sorteo de Caja (Premio en efectivo) - Voucher
// 30-SEP-2016  ETP         Fixed Bug 18003: Cashier: los vouchers de recarga no reflejan los cambios solicitados por Televisa
// 10-OCT-2016  ETP         Fixed Bug 18738: Cashier: VOUCHER_CARD_ADD_CARD is printed in some vouchers.
// 13-OCT-2016  ETP         Fixed Bug 18894:Cajero: Totales incorrectos en los vouchers al realizar una recarga
// 09-NOV-2016  ETP         Fixed Bug 20160: Duplicated concept when not participating in GamingTables cashdeskdraw
// 10-NOV-2016  ETP         Fixed Bug 20160: Duplicated concept when not participating in GamingTables cashdeskdraw
// 22-NOV-2016  EOR         Fixed Bug 20755: Cashier - Recharge: Incorrect totals in ticket with recharge check or bank card
// 28-NOV-2016  ATB         Fixed Bug 21025: Televisa: The anonymous card is billed in vouchers with the "company B amount" unchecked
// 01-DIC-2016  ETP         Fixed Bug 21195: Codere ticket incorrect loser nls.
// 02-DIC-2016  ETP         Fixed Bug 21024: Televisa voucher changes. 
// 20-DEC-2016  ATB         Fixed Bug 21785: Codere: Unify concepts in vouchers to show them all as available amount
// 29-DEC-2016  JML         PBI 21361: Company voucher B - VAT breakdown: Voucher modification
// 11-JAN-2016  ETP         Fixed Bug 22543 Prize_coupon not aplicable if cashdesk enabled.
// 17-FEB-2017  DPC         Bug 24729:Sorteo de máquina - Cupón Promocional
// 04-MAY-2017  DPC         WIGOS-1574: Junkets - Flyers - New field "Text in Promotional Voucher" - Change voucher
// 16-JUN-2017  ATB         PBI 28030: PalacioDeLosNumeros - New design in recharge vouchers
// 20-JUN-2017  ATB         PBI 28030: PalacioDeLosNumeros - New design in recharge vouchers
// 21-JUN-2017  ATB         PBI 28030: PalacioDeLosNumeros - New design in recharge vouchers (bug WIGOS-3101)
// 23-JUN-2017  ATB         PBI 28030: PalacioDeLosNumeros - New design in recharge vouchers (bug WIGOS-3105)
// 23-OCT-2017  RGR         PBI 30366:WIGOS-5680 Winpot - Facturation requirements - From 01/12/2017
// 01-NOV-2017  RGR         Bug 30506:WIGOS-6294 WINPOT: total amount for withdrawal is incorrect
// 26-JAN-2017  DHA         Bug 31325:WIGOS-7908 [Ticket #12031] Tickets Salas 016 y 049
// 20-FEB-2018  RGR         Bug 31607:WIGOS-7073 Wrong Service Charge label in "Nota de Venta" voucher when "Chips Buy" Gambling Tables operations 
// 06-MAR-2018  EOR         Bug 31749: WIGOS-8409 [Ticket #12778] Remplazo palabra “Promoción” por “S.I.G.”
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;
using WSI.Common.Junkets;

namespace WSI.Common
{
  public class VoucherCashIn : Voucher
  {
    #region Clase

    public class InputDetails
    {
      public MultiPromos.PromoBalance InitialBalance;
      public MultiPromos.PromoBalance FinalBalance;
      public Currency CashIn1;
      public Currency CashIn2;
      public Currency CardPrice;
      public Currency CashInTaxSplit1;
      public Currency CashInTaxSplit2;
      public Currency CashInTaxCustody; // LTC 14-ABR-2016
      public Currency TotalTaxSplit2;
      public Currency TotalBaseTaxSplit2;
      public CurrencyExchangeType PaymentType;
      public Currency ExchangeCommission;
      public Boolean WasForeingCurrency;
      public Boolean IsIntegratedChipsSale;
      public Currency CardCommission; //EOR 27-JUL-2016
      public Currency CardAmount; //EOR 27-JUL-2016
      public Boolean IsIntegratedChipsSaleWinner;
      public String CardNumber; // ATB 16-JUN-2017
      public String CardHolder; // ATB 16-JUN-2017
      public Boolean IsPayingWithCreditCard; // ATB 16-JUN-2017
      public Boolean IsChipsPurchaseWithCashOut; //RGR 20-FEB-2018
    }

    #endregion

    #region Constructor

    public VoucherCashIn(String[] VoucherAccountInfo
                       , Int64 AccountId
                       , TYPE_SPLITS Splits
                       , InputDetails Details
                       , CASHIER_MOVEMENT MovementType
                       , OperationCode OperationCode
                       , PrintMode Mode
                       , SqlTransaction SQLTransaction
                       , JunketsBusinessLogic JunketInfo = null
                       , long OperationId = 0
                       , Int64 PromoGameId = 0)
      : base(Mode, OperationCode, SQLTransaction)
    {
      Currency _cash_in_split1;
      Currency _cash_in_split2;
      Int32 _days;
      String _str_aux;
      Currency _cur_aux;
      Currency _promos_without_prize_coupon;
      Currency _total_promos;
      Boolean _hide_total;
      MultiPromos.PromoBalance _increment_bal;
      Boolean _add_currency_in_letters;
      Currency _cash_in_tax_split1;
      Currency _cash_in_tax_custody;
      //ESE 02-AGO-2016
      Boolean _hide_voucher_recharge;
      Boolean _chips_sale_winner;
      Boolean _print_tax_detail;
      Boolean _prize_cupon;
      CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION _cash_desk_id;
      String _str_promotional_coupon;
      StringBuilder _sb;

      _sb = new StringBuilder();

      _increment_bal = Details.FinalBalance - Details.InitialBalance;

      //ESE 02-AGO-2016 EOR 22-NOV-2016
      // ATB 28-NOV-2016
      // In case of Televisa (VoucherMode = 8) we must control the
      // company B showing info.
      _hide_voucher_recharge = false;

      _cash_in_split1 = 0;
      _cash_in_split2 = 0;
      _cash_in_tax_split1 = 0;
      _cash_in_tax_custody = Details.CashInTaxCustody; // LTC 13-ABR-2016 // EOR 02-AUG-2016
      _str_aux = "";
      _hide_total = false;
      _add_currency_in_letters = false;

      _chips_sale_winner = Details.IsIntegratedChipsSaleWinner && Details.IsIntegratedChipsSale && Misc.IsVoucherModeCodere();

      _cash_desk_id = Details.IsIntegratedChipsSale ? CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_01 : CashDeskDrawBusinessLogic.ENUM_DRAW_CONFIGURATION.DRAW_00;

      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load general voucher data.
      //  3. Load specific voucher data.

      // 1. Load HTML structure.
      switch (MovementType)
      {
        case CASHIER_MOVEMENT.CASH_IN_SPLIT1:
          // If the voucher mode is 1 (PalacioDeLosNumeros) it is necessary to modify the footer
          if (Misc.GetVoucherMode() == 1)
          {
            if (Details.IsPayingWithCreditCard)
            {
              Splits.company_a.footer = Splits.company_a.footer.Replace("@OPERATION_NUMBER", OperationId.ToString());
              Splits.company_a.footer = (!String.IsNullOrEmpty(Details.CardNumber)) ? Splits.company_a.footer.Replace("@CARD_LAST_4_DIGITS", Details.CardNumber.Substring(Details.CardNumber.Length - 4, 4)) : Splits.company_a.footer.Replace("@CARD_LAST_4_DIGITS", "_____");
              Splits.company_a.footer = (!String.IsNullOrEmpty(Details.CardHolder)) ? Splits.company_a.footer.Replace("@CARD_HOLDER", Details.CardHolder) : Splits.company_a.footer.Replace("@CARD_HOLDER", "_________________________");
            }
            else
            {
              Splits.company_a.footer = String.Empty;
            }
          }
          LoadVoucher("Voucher.A.CashIn");
          LoadHeader(Splits.company_a.header);
          LoadFooter(Splits.company_a.footer, Splits.company_a.footer_cashin);

          //DHA 23-JUN-2015 Added custom parameters for cashin chips operations
          if (Details.IsIntegratedChipsSale)
          {
            AddString("VOUCHER_SPLIT_NAME", GeneralParam.GetString("GamingTables", "IntegratedChipsSales.Split.A.CashInVoucherTitle", Splits.company_a.cash_in_voucher_title));
            AddString("VOUCHER_CONCEPT", GeneralParam.GetString("GamingTables", "IntegratedChipsSales.Split.A.Name", Splits.company_a.name));
          }
          else
          {
            AddString("VOUCHER_SPLIT_NAME", Splits.company_a.cash_in_voucher_title);
            AddString("VOUCHER_CONCEPT", Splits.company_a.name);
          }

          _cash_in_split1 = Details.CashIn1;
          _cash_in_split2 = Details.CashIn2;

          VoucherSequenceId = SequenceId.VouchersSplitA;
          SetValue("CV_TYPE", (Int32)CashierVoucherType.CashIn_A);

          _hide_total = Splits.company_a.hide_total;
          _add_currency_in_letters = Splits.company_a.total_in_letters;

          if (Details.CashInTaxSplit1 > 0)
          {
            if (Details.IsIntegratedChipsSale)
            {
              _str_aux = GeneralParam.GetString("GamingTables", "CashInTax.VoucherText", Resource.String("STR_MOVEMENT_TYPE_CASH_IN_TAX")) + ":";
            }
            else
            {
              _str_aux = GeneralParam.GetString("Cashier", "CashInTax.VoucherText", Resource.String("STR_MOVEMENT_TYPE_CASH_IN_TAX")) + ":";
            }
            AddString("STR_VOUCHER_CASH_IN_TAX", _str_aux);
            // Logrand mode: Only one voucher (including A & B)
            _cash_in_tax_split1 = (VoucherMode == 5) ? Details.CashInTaxSplit1.SqlMoney + Details.CashInTaxSplit2.SqlMoney : Details.CashInTaxSplit1.SqlMoney;
            AddCurrency("VOUCHER_CASH_IN_TAX", _cash_in_tax_split1);
          }
          else
          {
            AddString("STR_VOUCHER_CASH_IN_TAX", "");
            AddString("VOUCHER_CASH_IN_TAX", "");
          }

          break;

        case CASHIER_MOVEMENT.CASH_IN_SPLIT2:
          //add new movement 
          LoadVoucher("Voucher.B.CashIn");
          LoadHeader(Splits.company_b.header);
          LoadFooter(Splits.company_b.footer, Splits.company_b.footer_cashin);
          AddString("VOUCHER_SPLIT_NAME", Splits.company_b.cash_in_voucher_title);
          AddString("VOUCHER_CONCEPT", Splits.company_b.name);
          SetValue("CV_TYPE", (Int32)CashierVoucherType.CashIn_B);
          break;

        case CASHIER_MOVEMENT.SERVICE_CHARGE:
          //add new movement 
          LoadVoucher("Voucher.B.CashIn");
          LoadHeader(Splits.company_b.header);
          LoadFooter(Splits.company_b.footer, Splits.company_b.footer_cashin);
          AddString("VOUCHER_SPLIT_NAME", Splits.company_b.cash_in_voucher_title);
          AddString("VOUCHER_CONCEPT", Details.IsChipsPurchaseWithCashOut ? GeneralParam.GetString("GamingTables.ServiceCharge", "Text", string.Empty) :
                                                                            GeneralParam.GetString("Cashier.ServiceCharge", "Text", string.Empty));
          SetValue("CV_TYPE", (Int32)CashierVoucherType.ServiceCharge_B);
          break;

        case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT2:
          //add new movement 
          LoadVoucher("Voucher.B.CashIn");
          LoadHeader(Splits.company_b.header);
          LoadFooter(Splits.company_b.footer, Splits.company_b.footer_cashin);
          AddString("VOUCHER_SPLIT_NAME", GeneralParam.GetString("Cashier.PaymentMethod", "CommissionToCompanyB.BankCard.VoucherTitle", Splits.company_b.cash_in_voucher_title));
          AddString("VOUCHER_CONCEPT", Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION"));
          SetValue("CV_TYPE", (Int32)CashierVoucherType.Commission_B);
          break;

        case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT2:
          //add new movement 
          LoadVoucher("Voucher.B.CashIn");
          LoadHeader(Splits.company_b.header);
          LoadFooter(Splits.company_b.footer, Splits.company_b.footer_cashin);
          AddString("VOUCHER_SPLIT_NAME", GeneralParam.GetString("Cashier.PaymentMethod", "CommissionToCompanyB.Check.VoucherTitle", Splits.company_b.cash_in_voucher_title));
          AddString("VOUCHER_CONCEPT", Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION"));
          SetValue("CV_TYPE", (Int32)CashierVoucherType.CheckCommissionB);
          break;

        case CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT2:
          //add new movement 
          LoadVoucher("Voucher.B.CashIn");
          LoadHeader(Splits.company_b.header);
          LoadFooter(Splits.company_b.footer, Splits.company_b.footer_cashin);
          AddString("VOUCHER_SPLIT_NAME", GeneralParam.GetString("Cashier.PaymentMethod", "CommissionToCompanyB.Currency.VoucherTitle", Splits.company_b.cash_in_voucher_title));
          AddString("VOUCHER_CONCEPT", Resource.String("STR_FRM_LAST_MOVEMENTS_COMMISSION"));
          SetValue("CV_TYPE", (Int32)CashierVoucherType.ExchangeCommissionB);
          break;

        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK:
        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN:
          //add new movement 
          LoadVoucher("Voucher.B.CashIn");
          LoadHeader(Splits.company_b.header);
          LoadFooter(Splits.company_b.footer, Splits.company_b.footer_cashin);
          AddString("VOUCHER_SPLIT_NAME", Splits.company_b.cash_in_voucher_title);
          AddString("VOUCHER_CONCEPT", WSI.Common.Misc.GetCardPlayerConceptName());
          SetValue("CV_TYPE", (Int32)CashierVoucherType.CardDepositIn_B);
          break;

        //case new

        default:
          {
            Log.Error("VoucherCashIn. MovementType not valid: " + MovementType.ToString());
            return;
          }
      }

      if (MovementType != CASHIER_MOVEMENT.CASH_IN_SPLIT1)
      {
        if (MovementType == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT2 ||
            MovementType == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CHECK_SPLIT2 ||
            MovementType == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_EXCHANGE_SPLIT2)
        {
          _cash_in_split1 = Details.ExchangeCommission;

          VoucherSequenceId = SequenceId.VouchersSplitB;

          _hide_total = false;
          _add_currency_in_letters = true;

          AddString("STR_VOUCHER_CASH_IN_TAX", "");
          AddString("VOUCHER_CASH_IN_TAX", "");
        }
        else
        {
          _cash_in_split1 = Details.CashIn2;
          _cash_in_tax_split1 = Details.CashInTaxSplit2;

          if (_hide_voucher_recharge)
          {
            _cash_in_split1 += Details.CardCommission;
          }

          VoucherSequenceId = SequenceId.VouchersSplitB;

          _hide_total = Splits.company_b.hide_total;
          _add_currency_in_letters = Splits.company_b.total_in_letters;

          if (Details.CashInTaxSplit2 > 0 && MovementType == CASHIER_MOVEMENT.CASH_IN_SPLIT2)
          {
            if (Details.IsIntegratedChipsSale)
            {
              _str_aux = GeneralParam.GetString("GamingTables", "CashInTax.VoucherText", Resource.String("STR_MOVEMENT_TYPE_CASH_IN_TAX")) + ":";
            }
            else
            {
              _str_aux = GeneralParam.GetString("Cashier", "CashInTax.VoucherText", Resource.String("STR_MOVEMENT_TYPE_CASH_IN_TAX")) + ":";
            }
            AddString("STR_VOUCHER_CASH_IN_TAX", _str_aux);
            AddCurrency("VOUCHER_CASH_IN_TAX", Details.CashInTaxSplit2);
          }
          else
          {
            AddString("STR_VOUCHER_CASH_IN_TAX", "");
            AddString("VOUCHER_CASH_IN_TAX", "");
          }
        }
      }

      SetValue("CV_ACCOUNT_ID", AccountId);

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load specific voucher data.

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      _total_promos = _increment_bal.Balance.PromoRedeemable + _increment_bal.Balance.PromoNotRedeemable;

      _prize_cupon = Splits.prize_coupon && !CashDeskDrawBusinessLogic.IsEnabledCashDeskDraws(Convert.ToInt16(_cash_desk_id));

      // RRB 04-OCT-2012 Add VoucherMode 2 (Miravalle) 
      // Now have promotion part B and promos without prizecoupon (after promotion part B and total promos) TODO: Move to switch with new variable
      if (_prize_cupon || VoucherMode == 2)
      {
        _promos_without_prize_coupon = _total_promos - Details.CashIn2;
      }
      else
      {
        _promos_without_prize_coupon = _total_promos;
      }

      if (Splits.promo_and_coupon)
      {
        _cur_aux = _total_promos;
        _str_aux = Splits.text_promo_and_coupon;
        if (String.IsNullOrEmpty(_str_aux))
        {
          _str_aux = Resource.String("STR_VOUCHER_CARD_ADD_PROMOANDCOUPON");
        }

        if (PromoGameId <= TerminalDraw.TerminalDraw.WELCOME_DRAW_ID && IsTerminalDraw())
        {
          SetParameterValue("@VOUCHER_CARD_ADD_PROMOANDCOUPON",
        "<tr>" +
           "<td align=\"left\"  width=\"50%\">" + _str_aux + ":</td>" +
           "<td align=\"right\" width=\"50%\">" + _cur_aux.ToString() + "</td>" +
        "</tr>");
        }
        else
        {
          SetParameterValue("@VOUCHER_CARD_ADD_PROMOANDCOUPON", "");
        }
      }
      else
      {
        SetParameterValue("@VOUCHER_CARD_ADD_PROMOANDCOUPON", "");
      }

      String _points_awarded;

      _points_awarded = "";

      if (_increment_bal.Points > 0)
      {
        String _label_points;
        _label_points = "";

        _points_awarded += "<tr>";
        _points_awarded += "  <td align=\"left\"  width=\"50%\" style=\"height: 16px\"></td>";
        _points_awarded += "  <td align=\"right\" width=\"50%\" style=\"height: 16px\"></td>";
        _points_awarded += "</tr>";

        _points_awarded += "<tr>";
        _points_awarded += "  <td align=\"left\"  width=\"50%\" style=\"height: 16px\">#LABEL_POINTS_AWARDED#</td>";
        _points_awarded += "  <td align=\"right\" width=\"50%\" style=\"height: 16px\">#POINTS#</td>";
        _points_awarded += "</tr>";

        _points_awarded += "<tr>";
        _points_awarded += "  <td align=\"left\"  width=\"50%\" style=\"height: 16px\"></td>";
        _points_awarded += "  <td align=\"right\" width=\"50%\" style=\"height: 16px\"></td>";
        _points_awarded += "</tr>";

        _label_points = Resource.String("STR_VOUCHER_CARD_ADD_POINTS_AWARDED");

        _points_awarded = _points_awarded.Replace("#LABEL_POINTS_AWARDED#", _label_points);
        _points_awarded = _points_awarded.Replace("#POINTS#", ((Points)_increment_bal.Points).ToString());
      }

      SetParameterValue("@VOUCHER_CARD_ADD_POINTS_AWARDED", _points_awarded);

      //EOR 02-AUG-2016
      if (Misc.IsTaxCustodyEnabled() &&
          _cash_in_tax_custody > 0 &&
          Misc.GetTaxCustodyVoucher() == Misc.TaxCustodyVoucher.CustodyConcept)
      {
        SetParameterValue("@VOUCHER_ADD_CONCEPT_TAX_CUSTODY",
            "<tr>" +
              "<td align=\"left\"  width=\"50%\">" + Misc.TaxCustodyConcept() + ":</td>" +
              "<td align=\"right\" width=\"50%\">" + _cash_in_tax_custody.ToString() + "</td>" +
            "</tr>");
      }
      else
      {
        SetParameterValue("@VOUCHER_ADD_CONCEPT_TAX_CUSTODY", "");
      }

      // ATB 20-DEC-2016
      // In case of Codere and it's not a chips selling, we hide the promotions (they are unified with the company cash)
      if (Splits.promo_and_coupon || !_prize_cupon || Details.CashIn2 <= 0 || (Misc.IsVoucherModeCodere() && !Details.IsIntegratedChipsSale))
      {
        SetParameterValue("@VOUCHER_CARD_ADD_CARD_PRIZE_COUPON", "");
      }
      else if (_prize_cupon)
      {
        _str_promotional_coupon = String.Empty;

        if (!IsTerminalDraw())
        {
          _str_aux = Splits.prize_coupon_text;

          if (String.IsNullOrEmpty(_str_aux))
          {
            _str_aux = Resource.String("STR_VOUCHER_CARD_ADD_CARD_PRIZE_COUPON");
          }

          _str_promotional_coupon = "<tr>" +
                                      "<td align=\"left\"  width=\"50%\">@VOUCHER_CARD_ADD_CARD_PRIZE_COUPON_TEXT:</td>" +
                                      "<td align=\"right\" width=\"50%\">@VOUCHER_CARD_ADD_CARD_PRIZE_COUPON_VALUE</td>" +
                                                       "</tr>";
        }

        SetParameterValue("@VOUCHER_CARD_ADD_CARD_PRIZE_COUPON", _str_promotional_coupon);

        AddString("VOUCHER_CARD_ADD_CARD_PRIZE_COUPON_TEXT", _str_aux);
        AddCurrency("VOUCHER_CARD_ADD_CARD_PRIZE_COUPON_VALUE", Details.CashIn2);
      }

      // ATB 20-DEC-2016
      // In case of Codere and it's not a chips selling, we hide the promotions (they are unified with the company cash)
      if (Splits.promo_and_coupon || Splits.hide_promo || _promos_without_prize_coupon <= 0 || (Misc.IsVoucherModeCodere() && !Details.IsIntegratedChipsSale))
      {
        SetParameterValue("@VOUCHER_CARD_ADD_PROMOTION", "");
      }
      else
      {
        _str_aux = Splits.text_promo;

        //DHA 23-JUN-2015 Added custom parameters for cashin chips operations
        if (Details.IsIntegratedChipsSale)
        {
          if (_chips_sale_winner)
          {
            _str_aux = GeneralParam.GetString("GamingTables", "IntegratedChipsSales.Voucher.Promotion.Text", _str_aux);
          }
          else
          {
            _str_aux = Resource.String("STR_VOUCHER_CARD_ADD_COURTESY");
          }
        }

        if (Misc.IsVoucherModeCodere())
        {
          _str_aux = GeneralParam.GetString("GamingTables", "IntegratedChipsSales.Voucher.Promotion.Text", _str_aux);
        }

        if (String.IsNullOrEmpty(_str_aux))
        {
          _str_aux = Resource.String("STR_VOUCHER_CARD_ADD_PROMOTION");
        }

        SetParameterValue("@VOUCHER_CARD_ADD_PROMOTION",
            "<tr>" +
              "<td align=\"left\" width=\"50%\">" + _str_aux + ":</td>" +
              "<td align=\"right\" width=\"50%\">" + _promos_without_prize_coupon.ToString() + "</td>" +
            "</tr>");
      }

      if (MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT ||
          MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT ||
          MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC ||
          MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE ||
          MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK ||
          MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN)
      {
        SetParameterValue("@VOUCHER_CARD_ADD_CARD", "");

        if (Misc.IsCardPlayerToCompanyB())
        {
          AddString("VOUCHER_CONCEPT", Misc.GetCardPlayerConceptName());
          AddCurrency("VOUCHER_CARD_ADD_AMOUNT_TO_ADD", Details.CardPrice);
        }
      }
      else
      {
        // ATB 28-NOV-2016
        // Checking Televisa and not marked check regarding the company b amount
        if (Misc.IsCardPlayerToCompanyB() || (MovementType != CASHIER_MOVEMENT.CASH_IN_SPLIT1 && Misc.IsVoucherModeTV() && Misc.IsCardPlayerToCompanyB()))
        {
          SetParameterValue("@VOUCHER_CARD_ADD_CARD", "");
        }
        else
        {
          SetParameterValue("@VOUCHER_CARD_ADD_CARD",
                           "<tr>" +
                              "<td align=\"left\"  width=\"50%\" style=\"height: 16px\">@VOUCHER_CARD_ADD_CARD_NAME:</td>" +
                              "<td align=\"right\" width=\"50%\" style=\"height: 16px\">@VOUCHER_CARD_ADD_CARD_DEPOSIT</td>" +
                           "</tr>");
          AddCurrency("VOUCHER_CARD_ADD_CARD_DEPOSIT", Details.CardPrice);
          AddString("VOUCHER_CARD_ADD_CARD_NAME", Misc.GetCardPlayerConceptName());
        }
      }

      // ATB 20-DEC-2016
      // In case of Codere, we merge the company A cash + promos (only for recharge)
      if (Misc.IsVoucherModeCodere() && MovementType == CASHIER_MOVEMENT.CASH_IN_SPLIT1 && !Details.IsIntegratedChipsSale) // Codere
      {
        // Company A + Promotions
        AddCurrency("VOUCHER_CARD_ADD_AMOUNT_TO_ADD", _cash_in_split1 + Details.CashInTaxSplit1 + Details.CashInTaxSplit2);
      }
      else
      {
        AddCurrency("VOUCHER_CARD_ADD_AMOUNT_TO_ADD", _cash_in_split1);
      }

      // RCI 04-JAN-2012: Optional, hide total.
      if (_hide_total || (MovementType == CASHIER_MOVEMENT.CASH_IN_SPLIT1 && _hide_voucher_recharge))
      {
        SetParameterValue("@VOUCHER_CARD_ADD_TEXT_TOTAL_AMOUNT", "");
      }
      else
      {
        switch (VoucherMode)
        {
          case 1:
            SetParameterValue("(@VOUCHER_CARD_ADD_LETTERS_TOTAL_B_AMOUNT)</td>",
              "(@VOUCHER_CARD_ADD_LETTERS_TOTAL_B_AMOUNT)</td></tr>" +
                "<tr>" +
                "<td width='100%'>@TABLE_TAX_DETAIL</td>");

            break;
          default:
            SetParameterValue("@VOUCHER_CARD_ADD_TEXT_TOTAL_AMOUNT",
                "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">" +
                  "<tr>" +
                    "<td align=\"center\">TOTAL</td>" +
                  "</tr>" +
                  "<tr>" +
                    "<td align=\"center\"><B>@VOUCHER_CARD_ADD_TOTAL_AMOUNT</B></td>" +
                  "</tr>" +
                "</table>" +
                "<tr>" +
                  "<td>" +
                    "@TABLE_TAX_DETAIL" +
                  "</td>" +
                "</tr>");

            break;
        }
      }

      if (VoucherMode == 5)
      {
        if (Misc.IsCardPlayerToCompanyB())
        {
          AddCurrency("VOUCHER_CARD_ADD_TOTAL_AMOUNT", _cash_in_split1 + _cash_in_split2 + Details.CashInTaxSplit1 + Details.CashInTaxSplit2, _add_currency_in_letters);
        }
        else
        {
          AddCurrency("VOUCHER_CARD_ADD_TOTAL_AMOUNT", _cash_in_split1 + _cash_in_split2 + Details.CardPrice + Details.CashInTaxSplit1 + Details.CashInTaxSplit2, _add_currency_in_letters);
        }
      }
      else
      {
        if (Misc.IsCardPlayerToCompanyB() &&
         !(MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT ||
          MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT ||
          MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC ||
          MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE ||
          MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK ||
          MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN))
        {
          AddCurrency("VOUCHER_CARD_ADD_TOTAL_AMOUNT", _cash_in_split1 + _cash_in_tax_split1, _add_currency_in_letters);
        }
        else
        {
          AddCurrency("VOUCHER_CARD_ADD_TOTAL_AMOUNT", _cash_in_split1 + Details.CardPrice + _cash_in_tax_split1, _add_currency_in_letters);
        }
      }

      _print_tax_detail = GeneralParam.GetBoolean("Cashier", "Split.B.Tax.VoucherPrintDetail", false);


      if (_print_tax_detail)
      {
        if (VoucherSequenceId == SequenceId.VouchersSplitB && Splits.enabled_b
          || VoucherMode == 5)        // VoucherMode = 5 -> Logrand mode: Only one voucher (including A & B)
        {
          StringBuilder _html_code;

          _html_code = new StringBuilder();
          _html_code.AppendLine(" <table width='100%' border='0' cellpadding='0' cellspacing='0'> ");
          _html_code.AppendLine("   <hr noshade size='4'> ");
          _html_code.AppendLine("   <tr> ");
          _html_code.AppendLine("     <td colspan='3'>@VOUCHER_TAX_DETAIL_SPLIT2:</td>  ");
          _html_code.AppendLine("   </tr> ");
          _html_code.AppendLine("   <tr> ");
          _html_code.AppendLine("     <td width='5%'></td> ");
          _html_code.AppendLine("     <td>@VOUCHER_BASE_TAX_NAME_SPLIT2:</td> ");
          _html_code.AppendLine("     <td width='52%' align='right'>@VOUCHER_BASE_TAX_SPLIT2</td> ");
          _html_code.AppendLine("   </tr> ");
          _html_code.AppendLine("   <tr> ");
          _html_code.AppendLine("     <td width='5%'></td> ");
          _html_code.AppendLine("     <td>@VOUCHER_TAX_NAME_SPLIT2</td> ");
          _html_code.AppendLine("     <td width='52%' align='right'>@VOUCHER_TAX_SPLIT2</td> ");
          _html_code.AppendLine("   </tr> ");
          _html_code.AppendLine(" </table> ");
          _html_code.AppendLine(" <hr noshade size='4'> ");

          SetParameterValue("@TABLE_TAX_DETAIL", _html_code.ToString());

          // MPO & OPC LOGRAND CON02
          AddString("VOUCHER_TAX_DETAIL_SPLIT2", Resource.String("STR_TAX_DETAIL", Splits.company_b.tax_name));
          AddString("VOUCHER_BASE_TAX_NAME_SPLIT2", Resource.String("STR_BASE"));
          String _pct = String.Format("{0:P2}", (Splits.company_b.tax_pct / 100));
          AddString("VOUCHER_TAX_NAME_SPLIT2", Splits.company_b.tax_name + " (" + _pct.Trim() + ")");
          AddCurrency("VOUCHER_BASE_TAX_SPLIT2", Details.TotalBaseTaxSplit2);
          AddCurrency("VOUCHER_TAX_SPLIT2", Details.TotalTaxSplit2);
        }
        else
        {
          SetParameterValue("@TABLE_TAX_DETAIL", "<hr noshade size='4'>");
        }
      }
      else
      {
        SetParameterValue("@TABLE_TAX_DETAIL", "<hr noshade size='4'>");
      }

      // RCI 17-MAY-2011: Mode 01: Palacio de los Números
      // RCI 03-JUN-2011: Mode 02: Miravalle

      // JML save data for all Modes
      //switch (VoucherMode)
      //{
      //  case 1: // Palacio de los Números
      //    {
      Currency _ini_redeemable_balance;
      Currency _fin_redeemable_balance;

      // TODO: Revisar. Abans hi havia això... no entenc perquè es resta CashIn1 i es suma CashIn2 !!!!!!!!!
      //_fin_redeemable_balance = FinalRedeemableBalance;
      //_ini_redeemable_balance = FinalRedeemableBalance - CashIn1 + CashIn2;

      _ini_redeemable_balance = Details.InitialBalance.Balance.TotalRedeemable;
      _fin_redeemable_balance = Details.InitialBalance.Balance.TotalRedeemable + (Details.CashIn1 + Details.CashIn2);

      AddString("VOUCHER_CARD_ACCOUNT_ID", AccountId.ToString("000000"));
      AddCurrency("VOUCHER_CARD_ADD_AMOUNT_BALANCE_INITIAL", _ini_redeemable_balance);

      AddCurrency("VOUCHER_CARD_ADD_AMOUNT_BALANCE_FINAL", _fin_redeemable_balance);
      AddCurrency("VOUCHER_CARD_ADD_AMOUNT_PART_B", Details.CashIn2);

      AddString("VOUCHER_PERCENTAGE_PART_B", Splits.company_b.name);
      AddCurrency("VOUCHER_CARD_ADD_PERCENTAGE_PART_B", Details.TotalTaxSplit2);
      AddCurrencyInLetters("VOUCHER_CARD_ADD_LETTERS_TOTAL_B_AMOUNT", Details.CashIn2);
      AddCurrency("VOUCHER_CARD_TOTAL_OPERATION", (Details.CashIn1 + Details.CashIn2));

      if (!Int32.TryParse(GeneralParam.Value("Cashier", "CreditsExpireAfterDays"), out _days))
      {
        _days = 0;
      }

      if (_days == 0)
      {
        AddString("VOUCHER_CARD_ADD_EXPIRATION_DATE", "---");
      }
      else
      {
        AddString("VOUCHER_CARD_ADD_EXPIRATION_DATE", Format.CustomFormatDateTime(VoucherDateTime.AddDays(_days), false));
      }

      // Set necessary values for reports.
      switch (VoucherSequenceId)
      {
        case SequenceId.VouchersSplitA:
        default:
          SetValue("CV_M01_DEV", Details.CashIn1);
          SetValue("CV_M01_BASE", Details.CashIn2);
          SetValue("CV_M01_FINAL", _fin_redeemable_balance);
          break;

        case SequenceId.VouchersSplitB:
          {
            Currency _base_b;
            Currency _tax_b;

            if (MovementType == CASHIER_MOVEMENT.COMPANY_B_CURRENCY_CARD_EXCHANGE_SPLIT2)
            {
              _base_b = Details.ExchangeCommission;
              _tax_b = Math.Round(_base_b * Splits.company_b.tax_pct / 100, 2, MidpointRounding.AwayFromZero);
              _base_b = _base_b - _tax_b;
            }
            else
            {
              if (MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_CREDIT ||
                  MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_DEBIT ||
                  MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CARD_GENERIC ||
                  MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CURRENCY_EXCHANGE ||
                  MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN_CHECK ||
                  MovementType == CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_IN)
              {
                _base_b = Math.Round(Details.CardPrice / (1 + Splits.company_b.tax_pct / 100), 2, MidpointRounding.AwayFromZero);
                _tax_b = Math.Round(_base_b * Splits.company_b.tax_pct / 100, 2, MidpointRounding.AwayFromZero);
                _base_b = Details.CardPrice - _tax_b;
              }
              else
              {
                _base_b = Math.Round(Details.CashIn2 / (1 + Splits.company_b.tax_pct / 100), 2, MidpointRounding.AwayFromZero);
                _tax_b = Math.Round(_base_b * Splits.company_b.tax_pct / 100, 2, MidpointRounding.AwayFromZero);
                _base_b = Details.CashIn2 - _tax_b;
              }
            }

            SetValue("CV_M01_BASE", _base_b);
            SetValue("CV_M01_TAX1", _tax_b);
          }

          break;
      } // switch (VoucherSequenceId)
      //}
      //break; // case 1

      switch (VoucherMode)
      {
        case 1: // Palacio de los Números
          String _promo_text;

          _promo_text = GeneralParam.GetBoolean("Cashier", "SplitAsWon", false) ?
              GeneralParam.GetString("Cashier", "SplitAsWon.Text") : Resource.String("M01-STR_VOUCHER_CARD_ADD_PROMOTION");

          AddString("VOUCHER_PROMOTION", _promo_text);

          break; // case 1. Palacio de los Números

        case 2: // Miravalle
          {
            if (Splits.enabled_b && !Splits.prize_coupon)
            {
              SetParameterValue("@VOUCHER_CARD_ADD_PART_B_PROMOTION",
                  "<tr>" +
                    "<td align=\"left\"  width=\"50%\">" + Resource.String("STR_VOUCHER_CARD_ADD_PART_B_PROMOTION") + ":</td>" +
                    "<td align=\"right\" width=\"50%\">" + Details.CashIn2 + "</td>" +
                  "</tr>");
            }
            else
            {
              SetParameterValue("@VOUCHER_CARD_ADD_PART_B_PROMOTION", "");
            }
          }

          break; // case 2

        case 5:
          AddString("VOUCHER_SPLIT2_CONCEPT", Splits.company_b.name);
          AddCurrency("VOUCHER_SPLIT2_AMOUNT", _cash_in_split2);

          break;
      } // switch (VoucherMode)

      if (JunketInfo != null && JunketInfo.PromotionID != null && JunketInfo.PrintTextPromotion)
      {
        _sb.Append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
        _sb.Append("<tr>");
        _sb.Append(String.Format("<td align=\"center\">{0}</td>", JunketInfo.TextPromotion));
        _sb.Append("</tr>");
        _sb.Append("</table>");
        _sb.Append("<hr noshade size=\"4\">");

        SetParameterValue("@VOUCHER_FLYER_PROMOTIONAL_TEXT", _sb.ToString());
      }
      else
      {
        SetParameterValue("@VOUCHER_FLYER_PROMOTIONAL_TEXT", String.Empty);
      }


    } // VoucherCashIn

    #endregion
  }
}
