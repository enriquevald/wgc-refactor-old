﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherTaxCustody.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;


namespace WSI.Common
{
  /// LTC 14-ABR-2016
  /// <summary>
  /// Voucher LayOut for Tax Custody
  /// </summary>
  public class VoucherTaxCustody : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherTaxCustody(String[] VoucherAccountInfo
                                , TYPE_SPLITS Splits
                                , Currency TaxCustody
                                , PrintMode Mode
                                , SqlTransaction Trx
      )
      : base(Mode, Trx)
    {
      // 1. Load HTML structure.
      LoadVoucher("VoucherTaxCustody");

      //  4. Load NLS strings from resource
      LoadVoucherResources(VoucherAccountInfo, Splits, TaxCustody);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(String[] VoucherAccountInfo
                                    , TYPE_SPLITS Splits
                                    , Currency TaxCustody)
    {
      Boolean _hide_total;
      Boolean _add_currency_in_letters;

      SetValue("CV_TYPE", (Int32)CashierVoucherType.TaxCustody);
      VoucherSequenceId = SequenceId.VoucherTaxCustody;

      //  2. Load Header and Footer
      LoadHeader(Splits.company_a.header);
      LoadFooter(Splits.company_a.footer, Splits.company_a.footer_cashin);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      AddString("VOUCHER_SPLIT_NAME", Misc.TaxCustodyName());
      AddString("VOUCHER_CONCEPT", Misc.TaxCustodyConcept());

      AddCurrency("VOUCHER_CARD_ADD_AMOUNT_TO_ADD", TaxCustody);

      _hide_total = Splits.company_a.hide_total;
      _add_currency_in_letters = Splits.company_a.total_in_letters;

      if (_hide_total)
      {
        SetParameterValue("@VOUCHER_CARD_ADD_TEXT_TOTAL_AMOUNT", "");
      }
      else
      {
        SetParameterValue("@VOUCHER_CARD_ADD_TEXT_TOTAL_AMOUNT",
            "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">" +
              "<tr>" +
                "<td align=\"center\">TOTAL</td>" +
              "</tr>" +
              "<tr>" +
                "<td align=\"center\"><B>@VOUCHER_CARD_ADD_TOTAL_AMOUNT</B></td>" +
              "</tr>" +
            "</table>" +
            "<hr noshade size=\"4\">");

        AddCurrency("VOUCHER_CARD_ADD_TOTAL_AMOUNT", TaxCustody, _add_currency_in_letters);
      }
    }

    #endregion

  }
}
