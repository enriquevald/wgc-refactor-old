﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherGetMarker.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: MArk Stansfield
// 
// CREATION DATE: 20-MAR-2017
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 20-MAR-2017  MS         First release. 
//---------------------------------------------------------------------------------------------

using System;
using WSI.Common;
using System.Data.SqlClient;
using WSI.Common.CreditLines;

namespace WSI.Common
{
  public class VoucherGetMarker : Voucher
  {
    #region Constructor
    /// <summary>
    ///     VoucherGetMarker
    /// </summary>
    /// <param name="VoucherAccountInfo"></param>
    /// <param name="CreditGiven"></param>
    /// <param name="CreditLineDetails"></param>
    /// <param name="SplitInformation"></param>
    /// <param name="OperationId"></param>
    /// <param name="Mode"></param>
    /// <param name="SQLTransaction"></param>
    public VoucherGetMarker(String[] VoucherAccountInfo
                       , Currency CreditGiven
                       , CreditLine CreditLineDetails
                       , TYPE_SPLITS SplitInformation
                       , Int64 OperationId
                       , PrintMode Mode
                       , SqlTransaction SQLTransaction)
      : base(Mode, OperationCode.CREDIT_LINE, SQLTransaction)
    {
      Currency _available_credit = 0;
 
      LoadVoucher("Voucher.GetMarker");

      LoadVoucherGeneralData();

      // 3. Load specific voucher data.
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      //Title
      AddString("VOUCHER_SPLIT_NAME", Resource.String("STR_VOUCHER_CREDIT_LINE_TITLE"));

      //Labels

      AddString("LBL_VOUCHER_CREDIT_ISSUED", Resource.String("STR_VOUCHER_CREDIT_LINE_CREDIT") + ":");
      AddString("LBL_VOUCHER_SPENT", Resource.String("STR_VOUCHER_CREDIT_LINE_CREDIT_TOTAL") + ":");
      AddString("LBL_VOUCHER_CREDIT_AVAILABLE", Resource.String("STR_VOUCHER_CREDIT_LINE_CREDIT_AVAILABLE") + ":");
      AddString("LBL_VOUCHER_TOTAL_CREDIT_LIMIT", Resource.String("STR_VOUCHER_CREDIT_LINE_CREDIT_LIMIT_TOTAL") + ":");
      
      //Values

      //Calculated Values
      _available_credit = CreditLineDetails.AvailableAmountNoTTO;
      if (_available_credit < 0) 
      {
        _available_credit = 0;
      }

      AddCurrency("VOUCHER_CREDIT_ISSUED", CreditGiven); //Credit requested
      //AddCurrency("VOUCHER_SPENT", CreditLineDetails.SpentAmount + CreditGiven); //New Total Spent including this
      AddCurrency("VOUCHER_SPENT", CreditLineDetails.SpentAmount); //New Total Spent including this
      AddCurrency("VOUCHER_CREDIT_AVAILABLE", _available_credit); // Not including TTO, minimum 0
      AddCurrency("VOUCHER_TOTAL_CREDIT_LIMIT", CreditLineDetails.LimitAmount); //Base credit, not TTO

      //AddCurrency("VOUCHER_TTO", CreditLineDetails.ExtensionAmount);
      //AddCurrency("VOUCHER_NEW_DEBT", CreditLineDetails.SpentAmount + CreditGiven);

      SetSignatureRoomByVoucherType(this.GetType());

    } // VoucherGetMarker

    #endregion
  }
}
