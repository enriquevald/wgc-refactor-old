﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCashDeskClose.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
// 13-OCT-2016  ESE         Bug 17833: Televisa voucher, status cash
// 14-ENE-2017  DPC         Bug 24584:Cierre de Caja: el voucher de la empresa B no tiene en cuenta el precio de la tarjeta
// 13-JAN-2016  ESE         PBI 22283: Winpot - concepto de promociones
// 21-MAR-2017  LTC         Bug 25965:Close Cashier Voucher: "Redeemeable Promotion" is not in NLS
// 23-MAR-2017  XGJ         Bug 26138:Sorteo de máquina - El tiquet de de cierre de caja no refleja el redondeo de decimales
// 29-MAR-2017  XGJ         Bug 26302:Voucher de Cierre de Caja-Empresa B: no refleja correctamente las cancelaciones
// 11-APR-2017  FJC         Bug 26082:Sorteo de Máquina: Datos mal informados en Voucher despues de un Reintegro Total con sorteo ejecutado. (Reopened)
// 04-MAR-2017  XGJ         Bug 27232:Amounts in company "A" are incorrect when customer don't participate in terminal draw
// 28-JUL-2017  DPC         WIGOS-3986: [Ticket #7385] Error Montos Nota de Venta empresa B Telestar de Occidente
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  // 4. Cash Desk Close
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  // Initial Amount:   $
  // Close Amount:     $
  //
  //  Final Balance:  $
  //
  //         Voucher Footer
  public class VoucherCashDeskClose : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCashDeskClose(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats
                              , CashDeskCloseVoucherType Type
                              , PrintMode Mode
                              , SqlTransaction SQLTransaction)
      : base(Mode, CashierVoucherType.CashClosing, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.
      //  4. Load specific voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCashDeskClose");

      // 2. Load voucher resources.
      LoadVoucherResources(Type);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      // 4. Load specific voucher data.
      SetupCashCloseVoucher(CashierSessionStats, Type);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(CashDeskCloseVoucherType Type)
    {
      String _str_value;
      TYPE_SPLITS _split_data;

      if (!Split.ReadSplitParameters(out _split_data))
      {
        return;
      }

      // TODO: Check return codes!!!

      // NLS Values

      // - Title
      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TITLE");
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_TITLE", _str_value);

      // - General
      _str_value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", _str_value);

      _str_value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", _str_value);

      _str_value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", _str_value);

      _str_value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", _str_value);

      _str_value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", _str_value);

      // Cash desk close voucher
      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_NUMBERS");
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_NUMBERS", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_AMOUNTS");
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_AMOUNTS", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_OPEN") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_OPEN", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_OUT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_OUT", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CASH_OUT_DETAILS") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_CASH_OUT_DETAILS", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_MB_DEPOSIT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_MB_DEPOSIT", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CURRENT_AMOUNT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_CURRENT_AMOUNT", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_PENDING_AMOUNT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_PENDING_AMOUNT", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_INITIAL_AMOUNT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_INITIAL_AMOUNT", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_DEPOSIT", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_WITHDRAW", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_SUB_TOTAL") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_SUB_TOTAL", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_PRIZES") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_WON", _str_value);

      _str_value = Resource.String("STR_FRM_CASH_DESK_PRIZE_PAYED") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_PAYED_WON", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_DEVOLUTIONS") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_DEV", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TAXES") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_TAXES", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TOTAL_CASH_DESK") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_TOTAL_CASH_DESK", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_NOT_REDEEMABLE") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_NOT_REDEEMABLE", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TAXES") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_TAX_TAXES", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CASH_NET") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_CASH_NET", _str_value);

      if (Type == CashDeskCloseVoucherType.ALL)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED") + ":";
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED", _str_value);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED", "");
      }

      _str_value = Resource.String("STR_UC_BANK_HANDPAYS") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_TITLE", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_HANDPAY_005") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_PAYED", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_HANDPAY_014") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_CANCELED", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_TOTAL", _str_value);

      _str_value = Resource.String("STR_UC_CARD_BALANCE_PROMO_TO_REDEEM") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_PROMO_TO_REDEEM", _str_value);

      //value = Resource.String("STR_UC_CARD_BALANCE_NOT_REDEEMABLE") + ":";
      _str_value = _split_data.text_promo + ":";
      if (String.IsNullOrEmpty(_str_value))
      {
        _str_value = Resource.String("STR_VOUCHER_CARD_ADD_PROMOTION") + ":";
      }
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_NO_REDEEM", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_TOTAL_CANCELLED") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_TOTAL_CANCELLED", _str_value);
    } // LoadVoucherResources

    #endregion

    #region Public Methods

    public void SetupCashCloseVoucher(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats
                                     , CashDeskCloseVoucherType Type)
    {
      Currency signed_amount;
      String _str_value;
      Currency _cs_mb_diff;
      CurrencyIsoType _iso_type;
      Boolean _gp_pending_cash_partial;
      CashierSessionInfo _session_info;
      Boolean _is_tito_mode;

      //  1. Depositos
      //  2. Retiros
      //  3. Entradas
      //  4. Salidas
      //     ---------------------------
      //  5. Saldo (Efectivo en Caja)
      // 
      //     Detalle Salidas
      //  6. Premios
      //  7. Impuestos
      //  8. Premios pagados
      // 
      //     ---------------------------
      _iso_type = new CurrencyIsoType();
      _gp_pending_cash_partial = MobileBank.GP_GetRegisterShortfallOnDeposit();
      _session_info = CommonCashierInformation.CashierSessionInfo();
      _is_tito_mode = Utils.IsTitoMode();

      switch (Type)
      {
        case CashDeskCloseVoucherType.ALL:
          {
            //AddString("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_OPEN_NUM", CashierSessionStats.total_initial_movs.ToString());
            //AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_OPEN_AMT", CashierSessionStats.initial_balance);

            AddString("STR_VOUCHER_CASH_DESK_CLOSE_SUBTITLE", "");

            //  1. Depositos
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT_AMT", CashierSessionStats.deposits);

            //  2. Retiros
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW_AMT", CashierSessionStats.withdraws);

            //  3. Cash In
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN_AMT", CashierSessionStats.total_cash_in);

            //  4. Cash Out
            signed_amount = CashierSessionStats.total_cash_out;// * -1;
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_OUT_AMT", signed_amount);

            //  Diferencia Bancos Móviles
            _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF_MISSING") + ":";
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF", _str_value);
            _cs_mb_diff = CashierSessionStats.mb_total_balance_lost + CashierSessionStats.mb_pending;
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF", _cs_mb_diff);

            //  Sobrante Bancos Móviles
            if (_gp_pending_cash_partial)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_OVER_SHORTFALL") + ":";
            }
            else
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_OVER") + ":";
            }

            AddString("STR_VOUCHER_CASH_OVER", _str_value);
            AddCurrency("VOUCHER_CASH_OVER", CashierSessionStats.total_cash_over);

            //  Chech Payments
            _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS") + ":";
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS", _str_value);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS_AMT", CashierSessionStats.payment_orders);



            //Credit Card Exchange
            if (CashierSessionStats.total_bank_card > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", CashierSessionStats.total_bank_card);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", "");
            }

            //Check
            if (CashierSessionStats.total_check > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CHECK") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CHECK", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_CHECK", CashierSessionStats.total_check);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CHECK", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_CHECK", "");
            }

            // Currency Exchange
            if (CashierSessionStats.total_currency_exchange > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", CashierSessionStats.total_currency_exchange);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", "");
            }

            //Commission Exchange
            if (CashierSessionStats.total_commission > 0)
            {
              if (Misc.IsBankCardCloseCashEnabled())
              {
                _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_ADMINISTRATIVE_SERVICES") + ":";
              }
              else
              {
                _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION") + ":";
              }
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_COMMISSION", CashierSessionStats.total_commission);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_COMMISSION", "");
            }

            //Recharge for Points
            if (CashierSessionStats.points_as_cashin > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", CashierSessionStats.points_as_cashin);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", "");
            }

            //Service Charge
            if (CashierSessionStats.service_charge > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", CashierSessionStats.service_charge);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", "");
            }

            //  5. Saldo
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_CASH_NET", CashierSessionStats.final_balance);

            //  5.1 Entregado
            _iso_type.IsoCode = CurrencyExchange.GetNationalCurrency();
            _iso_type.Type = CurrencyExchangeType.CURRENCY;
            if (CashierSessionStats.collected.ContainsKey(_iso_type))
            {
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLLECTED", CashierSessionStats.collected[_iso_type]);
            }
            else
            {
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLLECTED", 0);
            }

            //  5.2 Diferencia Cajero
            _str_value = "";

            // DHA: only show cash over/short when cashier session desk is not integrated
            if (_session_info.UserType == GU_USER_TYPE.USER && !Cage.IsGamingTable(_session_info.CashierSessionId))
            {
              Decimal _collected_diff;

              if (CashierSessionStats.collected_diff.ContainsKey(_iso_type))
              {
                _collected_diff = CashierSessionStats.collected_diff[_iso_type];
                if (_collected_diff < 0)
                {
                  _collected_diff = -_collected_diff;
                  AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
                  AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");

                  _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISSING") + ":";
                  AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", _str_value);
                  AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", (Currency)_collected_diff);
                }
                else
                {
                  _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA") + ":";
                  AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", _str_value);
                  AddMultipleLineString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", ((Currency)_collected_diff).ToString());

                  AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");
                  AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");
                }
              }
              else
              {
                _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISSING") + ":";
                AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", _str_value);
                AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", CashierSessionStats.final_balance);

                AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
                AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
              }
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");

              AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
            }

            //  6. Premios
            signed_amount = CashierSessionStats.prize_gross;// * -1;
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_WON_AMT", signed_amount);

            // 7. Impuestos
            AddString("VOUCHER_CASH_DESK_CLOSE_TAX_TAXES_NUM", " ");
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_TAX_TAXES_AMT", CashierSessionStats.prize_taxes);

            //  8. Premios
            signed_amount = CashierSessionStats.prize_payment;// * -1;
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_WON_PAYED_AMT", signed_amount);

            //  9. Handpay
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_PAYED_AMT", CashierSessionStats.handpay_payment);
            signed_amount = CashierSessionStats.handpay_canceled;// * -1;
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_CANCELED_AMT", signed_amount);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_TOTAL_AMT", CashierSessionStats.handpay_payment - CashierSessionStats.handpay_canceled);

            // 10. Promotions
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_PROMO_TO_REDEEM_AMT", CashierSessionStats.promo_nr_to_re);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_NO_REDEEM_AMT", CashierSessionStats.promo_nr);

            // 11. Tickets TITO
            AddInteger("VOUCHER_CASH_DESK_CLOSE_MOV_TOTAL_CANCELLED_ACCOUNT", CashierSessionStats.tickets_cancelled_count);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_TOTAL_CANCELLED_AMOUNT", CashierSessionStats.tickets_cancelled_amount);

            Voucher _voucher;
            _voucher = new WSI.Common.VoucherCurrencyCash(CashierSessionStats, true);

            SetParameterValue("@VOUCHER_CURRENCY_CASH", _voucher.VoucherHTML);

            AddString("STR_TITO_VISIBLE", _is_tito_mode ? "" : "hidden");

            AddString("STR_VOUCHER_CASH_DESK_CLOSE_ROUNDING", "");
            AddString("VOUCHER_CASH_DESK_CLOSE_ROUNDING", "");

          }
          break;

        case CashDeskCloseVoucherType.A:
          {

            AddString("STR_VOUCHER_CASH_DESK_CLOSE_SUBTITLE", CashierSessionStats.split_a_company_name);

            //  1. Depositos
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT_AMT", CashierSessionStats.deposits);

            //  2. Retiros
            signed_amount = CashierSessionStats.withdraws;// * -1;
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW_AMT", signed_amount);

            //  3. Cash In
            // XGJ 04-MAR-2017
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN_AMT", CashierSessionStats.a_total_in);

            //  4. Cash Out
            // XGJ 04-MAR-2017
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_OUT_AMT", CashierSessionStats.b_total_dev_only_a);

            //  Diferencia Bancos Móviles
            _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF_MISSING") + ":";
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF", _str_value);
            _cs_mb_diff = CashierSessionStats.mb_total_balance_lost + CashierSessionStats.mb_pending;
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF", _cs_mb_diff);

            //  Sobrante Bancos Móviles
            if (_gp_pending_cash_partial)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_OVER_SHORTFALL") + ":";
            }
            else
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_OVER") + ":";
            }

            AddString("STR_VOUCHER_CASH_OVER", _str_value);
            AddCurrency("VOUCHER_CASH_OVER", CashierSessionStats.total_cash_over);

            //  Chech Payments
            _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS") + ":";
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS", _str_value);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS_AMT", CashierSessionStats.payment_orders);

            //Credit Card Exchange
            if (CashierSessionStats.total_bank_card > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", CashierSessionStats.bank_card_amount_only_a);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", "");
            }

            //Check
            if (CashierSessionStats.total_check > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CHECK") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CHECK", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_CHECK", CashierSessionStats.check_amount_only_a);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CHECK", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_CHECK", "");
            }

            // Currency Exchange
            if (CashierSessionStats.total_currency_exchange > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", CashierSessionStats.currency_exchange_amount_only_a);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", "");
            }

            //Commission Exchange
            if (CashierSessionStats.total_commission_a > 0)
            {
              if (Misc.IsBankCardCloseCashEnabled())
              {
                _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_ADMINISTRATIVE_SERVICES") + ":";
              }
              else
              {
                _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION") + ":";
              }
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_COMMISSION", CashierSessionStats.total_commission_a);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_COMMISSION", "");
            }

            //Recharge for Points
            if (CashierSessionStats.points_as_cashin > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", CashierSessionStats.points_as_cashin_only_a);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", "");
            }

            //Service Charge
            if (CashierSessionStats.service_charge_a > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", CashierSessionStats.service_charge);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", "");
            }

            // XGJ 24-MAR-2017
            // 11-APR-2017 FJC Bug 26082:Sorteo de Máquina: Datos mal informados en Voucher despues de un Reintegro Total con sorteo ejecutado. (Reopened)
            if (Misc.IsVoucherModeTV())
            {
              if (CashierSessionStats.decimal_rounding != 0)
              {
                _str_value = Resource.String("STR_UC_CARD_BALANCE_ROUNDING") + ":";
                AddString("STR_VOUCHER_CASH_DESK_CLOSE_ROUNDING", _str_value);
                AddCurrency("VOUCHER_CASH_DESK_CLOSE_ROUNDING", CashierSessionStats.decimal_rounding);
              }
              else
              {
                AddString("STR_VOUCHER_CASH_DESK_CLOSE_ROUNDING", "");
                AddString("VOUCHER_CASH_DESK_CLOSE_ROUNDING", "");
              }
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_ROUNDING", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_ROUNDING", "");
            }

            //  5. Saldo
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_CASH_NET", CashierSessionStats.final_balance_only_a);

            AddString("VOUCHER_CASH_DESK_CLOSE_COLLECTED", "");

            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
            AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");
            AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");

            //  6. Premios
            signed_amount = CashierSessionStats.prize_gross;// * -1;
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_WON_AMT", signed_amount);

            // 7. Impuestos
            AddString("VOUCHER_CASH_DESK_CLOSE_TAX_TAXES_NUM", " ");
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_TAX_TAXES_AMT", CashierSessionStats.prize_taxes);

            //  8. Premios
            signed_amount = CashierSessionStats.prize_payment;// * -1;
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_WON_PAYED_AMT", signed_amount);

            //  9. Handpay
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_PAYED_AMT", CashierSessionStats.handpay_payment);
            signed_amount = CashierSessionStats.handpay_canceled;// *-1;
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_CANCELED_AMT", signed_amount);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_TOTAL_AMT", CashierSessionStats.handpay_payment - CashierSessionStats.handpay_canceled);

            // 10. Promotions
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_PROMO_TO_REDEEM_AMT", CashierSessionStats.promo_nr_to_re);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_NO_REDEEM_AMT", CashierSessionStats.promo_nr);

            SetParameterValue("@VOUCHER_CURRENCY_CASH", "");

            AddString("STR_TITO_VISIBLE", _is_tito_mode ? "" : "hidden");


          }
          break;

        case CashDeskCloseVoucherType.B:
          {
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_SUBTITLE", CashierSessionStats.split_b_company_name);

            //  1. Depositos
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT_AMT", 0);

            //  2. Retiros
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW_AMT", 0);

            //  3. Cash In
            //if (Misc.GetVoucherMode() == 8 && Misc.IsTerminalDrawEnabled())
            //{
            //  AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN_AMT", CashierSessionStats.total_in_b + CashierSessionStats.service_charge_b);
            //}
            //else 
            //{
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN_AMT", CashierSessionStats.b_total_in + CashierSessionStats.service_charge_b);
            //}
            //  4. Cash Out
            //if (Misc.GetVoucherMode() == 8 && Misc.IsTerminalDrawEnabled())
            //{
            //  AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_OUT_AMT", CashierSessionStats.b_total_cash_out);
            //}
            //else
            //{
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_OUT_AMT", CashierSessionStats.b_total_dev);
            //}


            //  Diferencia Bancos Móviles
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF", "");
            AddString("VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF", "");

            //  Sobrante Bancos Móviles
            AddString("STR_VOUCHER_CASH_OVER", "");
            AddString("VOUCHER_CASH_OVER", "");

            //  Chech Payments
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS", "");
            AddString("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS_AMT", "");

            // Currency Exchange
            if (CashierSessionStats.total_currency_exchange > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", CashierSessionStats.currency_exchange_amount_only_b);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", "");
            }

            //Credit Card Exchange
            if (CashierSessionStats.total_bank_card > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", CashierSessionStats.bank_card_amount_only_b);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", "");
            }

            //Check
            if (CashierSessionStats.total_check > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CHECK") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CHECK", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_CHECK", CashierSessionStats.check_amount_only_b);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_CHECK", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_CHECK", "");
            }


            //Recharge for Points
            if (CashierSessionStats.points_as_cashin > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", CashierSessionStats.points_as_cashin_only_b);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", "");
            }

            //Service Charge
            if (CashierSessionStats.service_charge_b > 0)
            {
              _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE") + ":";
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", CashierSessionStats.service_charge);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", "");
            }

            //Commission Exchange
            if (CashierSessionStats.total_commission_b > 0)
            {
              if (Misc.IsBankCardCloseCashEnabled())
              {
                _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_ADMINISTRATIVE_SERVICES") + ":";
              }
              else
              {
                _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION") + ":";
              }
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION", _str_value);
              AddCurrency("VOUCHER_CASH_DESK_CLOSE_COMMISSION", CashierSessionStats.total_commission_b);
            }
            else
            {
              AddString("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION", "");
              AddString("VOUCHER_CASH_DESK_CLOSE_COMMISSION", "");
            }

            //  5. Saldo
            if (Misc.GetVoucherMode() == 8 && Misc.IsTerminalDrawEnabled())
            {
              CashierSessionStats.final_balance_only_b = CashierSessionStats.total_in_b
                                        + CashierSessionStats.cards_usage_b
                                        + CashierSessionStats.total_commission_b
                                        - CashierSessionStats.b_total_cash_out
                                        - CashierSessionStats.currency_exchange_amount_only_b
                                        - CashierSessionStats.bank_card_amount_only_b
                                        - CashierSessionStats.check_amount_only_b
                                        + CashierSessionStats.service_charge_b;
            }

            AddCurrency("VOUCHER_CASH_DESK_CLOSE_CASH_NET", CashierSessionStats.final_balance_only_b);

            AddString("VOUCHER_CASH_DESK_CLOSE_COLLECTED", "");

            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
            AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_EXTRA", "");
            AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");
            AddString("VOUCHER_CASH_DESK_CLOSE_COLL_CASHIER_DIFF_MISS", "");

            //  6. Premios
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_WON_AMT", 0);

            // 7. Impuestos
            AddString("VOUCHER_CASH_DESK_CLOSE_TAX_TAXES_NUM", " ");
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_TAX_TAXES_AMT", 0);

            //  8. Premios
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_WON_PAYED_AMT", 0);

            //  9. Handpay
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_PAYED_AMT", 0);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_CANCELED_AMT", 0);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_TOTAL_AMT", 0);

            // 10. Promotions
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_PROMO_TO_REDEEM_AMT", 0);
            AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_NO_REDEEM_AMT", 0);

            SetParameterValue("@VOUCHER_CURRENCY_CASH", "");

            AddString("STR_TITO_VISIBLE", _is_tito_mode ? "" : "hidden");

            AddString("STR_VOUCHER_CASH_DESK_CLOSE_ROUNDING", "");
            AddString("VOUCHER_CASH_DESK_CLOSE_ROUNDING", "");

          }
          break;

        default:
          {
            Log.Error("VoucherCashDesk. CashDeskCloseVoucherType not valid: " + Type.ToString());
            return;
          }

      }


    } // LoadVoucherResources
    #endregion

  }
}
