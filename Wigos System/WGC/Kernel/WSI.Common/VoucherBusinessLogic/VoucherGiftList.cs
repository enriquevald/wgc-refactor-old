﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherGiftList.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherGiftList : Voucher
  {
    #region Constructor

    public VoucherGiftList(String[] VoucherAccountInfo
                         , Points Points
                         , String AvailableGiftList
                         , String FutureGiftList
                         , PrintMode Mode
                         , SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      LoadVoucher("Voucher.Gift.List");

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load specific voucher data.

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      AddString("VOUCHER_GIFT_LIST_ACCOUNT_POINTS", Points.ToString());

      SetParameterValue("@VOUCHER_GIFT_LIST_AVAILABLE_GIFTS", AvailableGiftList);
      SetParameterValue("@VOUCHER_GIFT_LIST_FUTURE_GIFTS", FutureGiftList);

    } // VoucherGiftList

    #endregion
  }
}
