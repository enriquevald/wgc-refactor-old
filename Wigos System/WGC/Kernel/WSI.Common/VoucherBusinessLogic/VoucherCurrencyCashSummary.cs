﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCurrencyCashSummary.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
// 04-OCT-2016  ESE         Bug 17914: Cash status, add info to report
// 06-OCT-2016  ETP         Bug 18580: Some nls errors corrected
// 11-OCT-2016  ETP    Fixed Bug 18901:Multisite: Errors in cash summary in multisite mode.
// 13-OCT-2016  DHA         Fixed Bug 18810: show totals (cash_over, short_over) for chips
// 16-FEB-2017  RAB         Bug 24330: Session summary cashdesk: missing and excess appear in some types of cash, chips, check and currency
// 21-FEB-2017  FGB         Bug 24801: WigosGUI: Incorrect collection in the cash closing, due to Gp BankCard.Conciliate
// 30-AUG-2017  AMF         Bug 29494:[WIGOS-3980] After a currency exchange cancellation the foreign exchange still displayed in the cash report without any amount
// 19-OCT-2017  JML         Fixed Bug 30321:WIGOS-5924 [Ticket #9450] Fórmula datos tarjeta bancaria (entregado-faltante) - Resumen de sesión de caja
// 18-JUL-2018  AGS         Bug 33666:WIGOS-13712 Bank card amount entry in Cash Balance dissappears after "Devolución tarjeta/cheque" action from different cashier session
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherCurrencyCashSummary : Voucher
  {

    #region Class Attributes

    public class ParametersCurrencyCashSummary
    {
      public Boolean IsFilterMovements;
      public Boolean HideChipsAndCurrenciesConcepts;
      public String CurrencyIsoCode;

      public ParametersCurrencyCashSummary()
      {
        IsFilterMovements = false;
        HideChipsAndCurrenciesConcepts = false;
        CurrencyIsoCode = CurrencyExchange.GetNationalCurrency();
      }
    }

    #endregion

    #region Constructor

    public VoucherCurrencyCashSummary(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData, ParametersCurrencyCashSummary ParametersCashSummary, VoucherCashDeskOverview.ParametersCashDeskOverview ParametersOverview)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      //LJM 18-DEC-2013: All the generating the structure, etc. Are inside the funcion, since now the loop is there
      LoadVoucherResources(CashierSessionData, ParametersCashSummary, ParametersOverview);
    }
    #endregion

    #region Private Methods

    private void LoadVoucherResources(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionData, ParametersCurrencyCashSummary ParametersCashSummary, VoucherCashDeskOverview.ParametersCashDeskOverview ParametersOverview)
    {
      String _value;
      Decimal _currency_value;
      Decimal _balance_value;
      Decimal _short_value;
      Decimal _over_value;
      String _national_currency;
      string _currencies_html = "";
      Voucher _temp_voucher;
      String _currencies_balance = "";
      String _bank_card_balance = "";
      String _check_balance = "";
      String _chips_balance_re = "";
      String _chips_balance_nr = "";
      String _chips_balance_color = "";
      CurrencyIsoType _currency_iso_type;
      String _short_over_national = "";
      String _summary_total_label = "";
      Dictionary<String, Decimal> _summary_total_count;
      Int32 _voucher_mode;

      _summary_total_count = new Dictionary<string, decimal>();
      _voucher_mode = GeneralParam.GetInt32("Cashier.Voucher", "Mode", 0);

      // Load HTML structure for the current currency
      LoadVoucher("VoucherCurrencyCashSummary");

      _national_currency = ParametersCashSummary.CurrencyIsoCode;

      // Title
      AddString("STR_VOUCHER_CASH_BALANCE_TITLE", Resource.String("STR_VOUCHER_CASH_BALANCE_TITLE"));

      // Balance title
      if (_voucher_mode == 8)
      {
        AddString("STR_VOUCHER_CASH_SUMMARY_TITLE", Resource.String("STR_UC_BANK_MOVEMENTS_SUMMARY_CASH_IN"));
        AddString("STR_VOUCHER_CASH_SUMMARY_CASH", Resource.String("STR_ACCOUNT_PAYMENT_ORDER_CASH_PAYMENT"));
      }
      else
      {
        AddString("STR_VOUCHER_CASH_SUMMARY_TITLE", Resource.String("STR_FRM_CASH_DESK_NET_BALANCE"));
        AddString("STR_VOUCHER_CASH_SUMMARY_CASH", CurrencyExchange.GetDescription(CurrencyExchangeType.CURRENCY, _national_currency, ""));
      }

      // National Currency
      if (!ParametersCashSummary.IsFilterMovements)
      {
        AddCurrency("VOUCHER_CASH_SUMMARY_CASH", CashierSessionData.final_balance);

        _summary_total_count.Add(_national_currency, (Decimal)CashierSessionData.final_balance);
      }
      else
      {
        AddString("VOUCHER_CASH_SUMMARY_CASH", "---");
      }

      _currency_iso_type = new CurrencyIsoType();
      _currency_iso_type.IsoCode = _national_currency;
      _currency_iso_type.Type = CurrencyExchangeType.CURRENCY;
      if (CashierSessionData.show_short_over)
      {
        _temp_voucher = new Voucher();
        _currencies_html = "<tr valign=top>";
        _currencies_html += " <td class=\"sub-left-subitem\"><span style=\"font-size:90%\">@DELIVERED_NAME</span></td>";
        _currencies_html += " <td class=\"right\"><span style=\"font-size:90%\">@DELIVERED_VALUE</span></td>";
        _currencies_html += "</tr>";
        _currencies_html += "<tr valign=top>";
        _currencies_html += " <td class=\"sub-left-subitem\"><span style=\"font-size:90%\">@SHORT_NAME</span></td>";
        _currencies_html += " <td class=\"right\"><span style=\"font-size:90%\">@SHORT_VALUE</span></td>";
        _currencies_html += "</tr>";
        _currencies_html += "<tr valign=top>";
        _currencies_html += " <td class=\"sub-left-subitem\"><span style=\"font-size:90%\">@OVER_NAME</span></td>";
        _currencies_html += " <td class=\"right\"><span style=\"font-size:90%\">@OVER_VALUE</span></td>";
        _currencies_html += "</tr>";
        _temp_voucher.VoucherHTML = _currencies_html;

        _short_value = 0;
        if (CashierSessionData.short_currency.ContainsKey(_currency_iso_type))
        {
          _short_value = CashierSessionData.short_currency[_currency_iso_type];
        }

        _over_value = 0;
        if (CashierSessionData.over_currency.ContainsKey(_currency_iso_type))
        {
          _over_value = CashierSessionData.over_currency[_currency_iso_type];
        }

        _balance_value = CashierSessionData.final_balance + _over_value - _short_value;

        _temp_voucher.AddString("DELIVERED_NAME", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED"));

        if (ParametersCashSummary.IsFilterMovements)
        {
          _temp_voucher.AddString("DELIVERED_VALUE", "---");
          _temp_voucher.AddString("SHORT_VALUE", "---");
          _temp_voucher.AddString("OVER_VALUE", "---");
        }
        else
        {
          _temp_voucher.AddCurrency("DELIVERED_VALUE", _balance_value);
          if (_short_value > 0)
          {
            _temp_voucher.AddString("SHORT_NAME", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING"));
            _temp_voucher.AddCurrency("SHORT_VALUE", _short_value);
          }
          else
          {
            _temp_voucher.AddString("SHORT_NAME", "");
            _temp_voucher.AddString("SHORT_VALUE", "");
          }

          if (_over_value > 0)
          {
            _temp_voucher.AddString("OVER_NAME", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA"));
            _temp_voucher.AddCurrency("OVER_VALUE", _over_value);
          }
          else
          {
            _temp_voucher.AddString("OVER_NAME", "");
            _temp_voucher.AddString("OVER_VALUE", "");
          }
        }

        _short_over_national = _temp_voucher.VoucherHTML;
      }

      if (CashierSessionData.currencies_balance != null)
      {
        //Has to hide short or over on bank card with pinpad and conciliate
        Boolean _has_to_hide_shortorover_onbankcardwithpinpad;
        _has_to_hide_shortorover_onbankcardwithpinpad = Misc.HasToHideShortOrOverOnBankCardWithPinpadAndConciliation(ParametersOverview) 
                                                     || CashierSessionData.m_cashier_session_has_pinpad_operations;

        // Currencies
        foreach (KeyValuePair<CurrencyIsoType, Decimal> _currency in CashierSessionData.currencies_balance)
        {
          if (_currency.Key.Type == CurrencyExchangeType.CURRENCY && _currency.Key.IsoCode == _national_currency)
          {
            continue;
          }

          if (_currency.Value == 0)
          {
            continue;
          }

          _temp_voucher = new Voucher();

          _value = CurrencyExchange.GetDescription(_currency.Key.Type, _currency.Key.IsoCode, _currency.Key.IsoCode);
          _currency_value = _currency.Value;

          if (_summary_total_count.ContainsKey(_currency.Key.IsoCode))
          {
            _summary_total_count[_currency.Key.IsoCode] += _currency_value;
          }
          else
          {
            _summary_total_count.Add(_currency.Key.IsoCode, _currency_value);
          }

          _currencies_html = "<tr valign=top>";
          _currencies_html += " <td class=\"@CURRENCY_LEVEL\">@CURRENCY_NAME</td>";
          _currencies_html += " <td class=\"right\">@CURRENCY_VALUE</td>";
          _currencies_html += "</tr>";
          if (CashierSessionData.show_short_over && !ParametersCashSummary.IsFilterMovements)
          {
            _currencies_html += "<tr valign=top>";
            _currencies_html += " <td class=\"@SHORT_OVER_LEVEL\"><span style=\"font-size: 90%;\">@DELIVERED_NAME</span></td>";
            _currencies_html += " <td class=\"right\"><span style=\"font-size: 90%;\">@DELIVERED_VALUE</span></td>";
            _currencies_html += "</tr>";
            _currencies_html += "<tr valign=top>";
            _currencies_html += " <td class=\"@SHORT_OVER_LEVEL\"><span style=\"font-size: 90%;\">@SHORT_NAME</span></td>";
            _currencies_html += " <td class=\"right\"><span style=\"font-size: 90%;\">@SHORT_VALUE</span></td>";
            _currencies_html += "</tr>";
            _currencies_html += "<tr valign=top>";
            _currencies_html += " <td class=\"@SHORT_OVER_LEVEL\"><span style=\"font-size: 90%;\">@OVER_NAME</span></td>";
            _currencies_html += " <td class=\"right\"><span style=\"font-size: 90%;\">@OVER_VALUE</span></td>";
            _currencies_html += "</tr>";
          }
          _temp_voucher.VoucherHTML = _currencies_html;

          _temp_voucher.AddString("CURRENCY_NAME", _value);
          if (_currency_value != 0)
          {
            if (CurrencyMultisite.IsCenterAndMultiCurrency() || _currency.Key.IsoCode == _national_currency)
            {
              _temp_voucher.AddCurrency("CURRENCY_VALUE", (Currency)_currency_value);
            }
            else if (_currency.Key.Type == CurrencyExchangeType.CASINO_CHIP_COLOR)
            {
              _temp_voucher.AddInteger("CURRENCY_VALUE", (Int32)_currency_value);
            }
            else
            {
              _temp_voucher.AddString("CURRENCY_VALUE", Currency.Format(_currency_value, _currency.Key.IsoCode));
            }
          }
          else
          {
            _temp_voucher.AddString("CURRENCY_NAME", "");
            _temp_voucher.AddString("CURRENCY_VALUE", "");
          }

          if (_has_to_hide_shortorover_onbankcardwithpinpad && _currency.Key.Type == CurrencyExchangeType.CARD)
          {
            _temp_voucher.AddString("DELIVERED_NAME", String.Empty);
            _temp_voucher.AddString("DELIVERED_VALUE", String.Empty);
            _temp_voucher.AddString("OVER_NAME", String.Empty);
            _temp_voucher.AddString("OVER_VALUE", String.Empty);
            _temp_voucher.AddString("SHORT_NAME", String.Empty);
            _temp_voucher.AddString("SHORT_VALUE", String.Empty);
          }
          else if (CashierSessionData.show_short_over && !ParametersCashSummary.IsFilterMovements)
          {
            _temp_voucher.AddString("DELIVERED_NAME", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLLECTED"));

            _balance_value = _currency.Value;

            _currency_value = 0;
            if (CashierSessionData.short_currency.ContainsKey(_currency.Key))
            {
              _currency_value = CashierSessionData.short_currency[_currency.Key];
            }
            _balance_value -= _currency_value;

            if (_currency_value > 0)
            {
              _temp_voucher.AddString("SHORT_NAME", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_MISSING"));

              if (CurrencyMultisite.IsCenterAndMultiCurrency() || _currency.Key.IsoCode == _national_currency)
              {
                _temp_voucher.AddCurrency("SHORT_VALUE", _currency_value);
              }
              else if (_currency.Key.Type == CurrencyExchangeType.CASINO_CHIP_COLOR)
              {
                _temp_voucher.AddInteger("SHORT_VALUE", (Int32)_currency_value);
              }
              else
              {
                _temp_voucher.AddString("SHORT_VALUE", Currency.Format(_currency_value, _currency.Key.IsoCode));
              }
            }
            else
            {
              _temp_voucher.AddString("SHORT_NAME", "");
              _temp_voucher.AddString("SHORT_VALUE", "");
            }

            _currency_value = 0;
            if (CashierSessionData.over_currency.ContainsKey(_currency.Key))
            {
              _currency_value = CashierSessionData.over_currency[_currency.Key];
            }
            _balance_value += _currency_value;

            if (_currency_value > 0)
            {
              _temp_voucher.AddString("OVER_NAME", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_DIFF_EXTRA"));

              if (CurrencyMultisite.IsCenterAndMultiCurrency() || _currency.Key.IsoCode == _national_currency)
              {
                _temp_voucher.AddCurrency("OVER_VALUE", _currency_value);
              }
              else if (_currency.Key.Type == CurrencyExchangeType.CASINO_CHIP_COLOR)
              {
                _temp_voucher.AddInteger("OVER_VALUE", (Int32)_currency_value);
              }
              else
              {
                _temp_voucher.AddString("OVER_VALUE", Currency.Format(_currency_value, _currency.Key.IsoCode));
              }
            }
            else
            {
              _temp_voucher.AddString("OVER_NAME", "");
              _temp_voucher.AddString("OVER_VALUE", "");
            }

            if (CurrencyMultisite.IsCenterAndMultiCurrency() || _currency.Key.IsoCode == _national_currency)
            {
              _temp_voucher.AddCurrency("DELIVERED_VALUE", _balance_value);
            }
            else if (_currency.Key.Type == CurrencyExchangeType.CASINO_CHIP_COLOR)
            {
              _temp_voucher.AddInteger("DELIVERED_VALUE", (Int32)_balance_value);
            }
            else
            {
              _temp_voucher.AddString("DELIVERED_VALUE", Currency.Format(_balance_value, _currency.Key.IsoCode));
            }
          }

          switch (_currency.Key.Type)
          {
            case CurrencyExchangeType.CURRENCY:
              if (!ParametersCashSummary.HideChipsAndCurrenciesConcepts)
              {
                _temp_voucher.AddString("CURRENCY_LEVEL", "sub-left");
                _temp_voucher.AddString("SHORT_OVER_LEVEL", "sub-left-subitem");
                _currencies_balance += _temp_voucher.VoucherHTML;
              }
              break;
            case CurrencyExchangeType.CARD:
              _temp_voucher.AddString("CURRENCY_LEVEL", "left");
              _temp_voucher.AddString("SHORT_OVER_LEVEL", "sub-left");
              _bank_card_balance = _temp_voucher.VoucherHTML;
              break;
            case CurrencyExchangeType.CHECK:
              _temp_voucher.AddString("CURRENCY_LEVEL", "left");
              _temp_voucher.AddString("SHORT_OVER_LEVEL", "sub-left");
              _check_balance = _temp_voucher.VoucherHTML;
              break;
            case CurrencyExchangeType.CASINOCHIP:
            case CurrencyExchangeType.CASINO_CHIP_RE:
              if (!ParametersCashSummary.HideChipsAndCurrenciesConcepts)
              {
                _temp_voucher.AddString("CURRENCY_LEVEL", "left");
                _temp_voucher.AddString("SHORT_OVER_LEVEL", "sub-left");
                _chips_balance_re += _temp_voucher.VoucherHTML;
              }
              break;
            case CurrencyExchangeType.CASINO_CHIP_NRE:
              if (!ParametersCashSummary.HideChipsAndCurrenciesConcepts)
              {
                _temp_voucher.AddString("CURRENCY_LEVEL", "left");
                _temp_voucher.AddString("SHORT_OVER_LEVEL", "sub-left");
                _chips_balance_nr += _temp_voucher.VoucherHTML;
              }
              break;
            case CurrencyExchangeType.CASINO_CHIP_COLOR:
              if (!ParametersCashSummary.HideChipsAndCurrenciesConcepts)
              {
                _temp_voucher.AddString("CURRENCY_LEVEL", "left");
                _temp_voucher.AddString("SHORT_OVER_LEVEL", "sub-left");
                _chips_balance_color += _temp_voucher.VoucherHTML;
              }
              break;
            default:
              break;
          }
        }
      }

      _currencies_balance = _short_over_national + _currencies_balance;

      this.VoucherHTML = this.VoucherHTML.Replace("@VOUCHER_CASH_SUMMARY_CURRENCIES", _currencies_balance);

      // Bank Card
      this.VoucherHTML = this.VoucherHTML.Replace("@VOUCHER_CASH_SUMMARY_BANK_CARD", _bank_card_balance);

      // Check
      this.VoucherHTML = this.VoucherHTML.Replace("@VOUCHER_CASH_SUMMARY_CHECK", _check_balance);

      if (!ParametersCashSummary.HideChipsAndCurrenciesConcepts)
      {
        // Chips
        this.VoucherHTML = this.VoucherHTML.Replace("@VOUCHER_CASH_SUMMARY_CHIPS_RE", _chips_balance_re);
        this.VoucherHTML = this.VoucherHTML.Replace("@VOUCHER_CASH_SUMMARY_CHIPS_NR", _chips_balance_nr);
        this.VoucherHTML = this.VoucherHTML.Replace("@VOUCHER_CASH_SUMMARY_CHIPS_COLOR", _chips_balance_color);
      }
      else
      {
        this.VoucherHTML = this.VoucherHTML.Replace("@VOUCHER_CASH_SUMMARY_CHIPS_RE", "");
        this.VoucherHTML = this.VoucherHTML.Replace("@VOUCHER_CASH_SUMMARY_CHIPS_NR", _chips_balance_nr);
        this.VoucherHTML = this.VoucherHTML.Replace("@VOUCHER_CASH_SUMMARY_CHIPS_COLOR", _chips_balance_color);
      }

      // Balance total
      if (_voucher_mode == 8)
      {
        Boolean _add_line;

        _add_line = true;

        foreach (KeyValuePair<String, Decimal> _total in _summary_total_count)
        {
          _summary_total_label += "<tr>";

          if (_add_line)
          {
            _summary_total_label += "<td class='left' style='border-top: solid 2px black'>";
          }
          else
          {
            _summary_total_label += "<td class='left'>";
          }

          if (_total.Key == Cage.CHIPS_COLOR)
          {
            _summary_total_label += String.Format("Total ({0})", FeatureChips.GetChipTypeDescription(CurrencyExchangeType.CASINO_CHIP_COLOR, Cage.CHIPS_COLOR));
          }
          else
          {
            _summary_total_label += String.Format("Total ({0})", _total.Key);
          }

          if (_add_line)
          {
            _add_line = false;
            _summary_total_label += "<td class='right' style='border-top: solid 2px black'>";
          }
          else
          {
            _summary_total_label += "<td class='right'>";
          }

          if (_total.Key == Cage.CHIPS_COLOR)
          {
            _summary_total_label += (Int32)_total.Value;
          }
          else if (_total.Key != _national_currency)
          {
            _summary_total_label += Currency.Format(_total.Value, _total.Key);
          }
          else
          {
            _summary_total_label += ((Currency)_total.Value).ToString();
          }

          _summary_total_label += "</td>";
          _summary_total_label += "</tr>";
        }

        SetParameterValue("@VOUCHER_CASH_SUMMARY_TOTAL", _summary_total_label);
      }
      else
      {
        SetParameterValue("@VOUCHER_CASH_SUMMARY_TOTAL", String.Empty);
      }
    }

    #endregion

    #region Public Methods

    #endregion
  }
}
