﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherMultiCurrencyExchange.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;


namespace WSI.Common
{
  public class VoucherMultiCurrencyExchange : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructores

    public VoucherMultiCurrencyExchange(CurrencyExchangeVoucher CurrencyExchange, PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load general voucher data.
      //  3. Set specific params

      // 1. Load HTML structure.
      LoadVoucher("VoucherCardAddCurrencyExchange");

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Set params
      LoadMultiCurrencyResources(CurrencyExchange);
    }
    #endregion

    #region Private Methods
    private void LoadMultiCurrencyResources(CurrencyExchangeVoucher CurrencyExchangeVoucher)
    {
      String _str_aux;

      String paramName;
      String value;

      paramName = "STR_VOUCHER_TITLE";

      switch (CurrencyExchangeVoucher.CashierMovement)
      {
        case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_NATIONAL_CURRENCY:
          value = Resource.String("STR_VOUCHER_MULTI_EXCHANGE"); ;
          break;
        case CASHIER_MOVEMENT.CURRENCY_EXCHANGE_TO_FOREIGN_CURRENCY:
          value = Resource.String("STR_VOUCHER_MULTI_EXCHANGE_DEVOLUTION");
          break;
        default:
          value = "";
          Log.Warning("Voucher.LoadMultiCurrencyResources: Not movement definet");
          break;
      }
      AddString(paramName, value);

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", CurrencyExchangeVoucher.AccountInfo);

      _str_aux = Resource.String("STR_VOUCHER_APPLIED_EXCHANGE");

      SetParameterValue("@VOUCHER_CHANGE_APPLIED",
         "<tr>" +
           "<td align=\"left\" >" + _str_aux + " " + CurrencyExchangeVoucher.InIsoCode + ":</td>" +
           "<td align=\"right\" >" + CurrencyExchange.ChangeRateFormat(CurrencyExchangeVoucher.ChangeRate) + " " + CurrencyExchangeVoucher.OutIsoCode +
           "</td>" +
         "</tr>"
         );


      value = Resource.String("STR_VOUCHER_IN_CHARGED") + ":";
      AddString("STR_VOUCHER_IN_AMOUNT", value);



      value = Resource.String("STR_VOUCHER_COMISSION") + ":";
      AddString("STR_VOUCHER_COMISSION", value);

      value = Resource.String("STR_FRM_AMOUNT_INPUT_EQUALS");
      AddString("STR_VOUCHER_EQUIVAL_CHANGE", value);



      AddString("STR_VOUCHER_NR2", "");
      AddString("VOUCHER_NR2_AMOUNT", "");

      value = Resource.String("STR_VOUCHER_MULTI_EXCHANGE_DELIVERED") + ":";
      AddString("CURRENCY_STR_VOUCHER_TOTAL_AMOUNT", value);
      AddString("CURRENCY_VOUCHER_TOTAL_AMOUNT", Currency.Format(CurrencyExchangeVoucher.TotalAmount, CurrencyExchangeVoucher.OutIsoCode));

      if (CurrencyExchangeVoucher.NetAmountDevolution > 0)
      {
        AddString("STR_VOUCHER_TOTAL_AMOUNT", value);
        AddString("VOUCHER_TOTAL_AMOUNT", Currency.Format(CurrencyExchangeVoucher.NetAmountDevolution, CurrencyExchangeVoucher.InIsoCode));
      }
      else
      {
        AddString("VOUCHER_TOTAL_AMOUNT", "");
        AddString("STR_VOUCHER_TOTAL_AMOUNT", "");
      }


      AddString("VOUCHER_IN_AMOUNT", Currency.Format(CurrencyExchangeVoucher.Amount, CurrencyExchangeVoucher.InIsoCode));
      AddString("VOUCHER_EQUIVAL_CHANGE_AMOUNT", Currency.Format(CurrencyExchangeVoucher.EquivalChange, CurrencyExchangeVoucher.OutIsoCode));
      AddString("VOUCHER_CARD_PRICE_AMOUNT", "");
      AddString("VOUCHER_COMISSION_AMOUNT", Currency.Format(CurrencyExchangeVoucher.Comission, CurrencyExchangeVoucher.OutIsoCode));

      AddString("STR_VOUCHER_CARD_PRICE", "");
    }
    #endregion

  }
}
