﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCardCreditTransfer.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherCardCreditTransfer : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCardCreditTransfer(Int64 SourceAccountId
                                    , Int64 TargetAccountId
                                    , Accounts.TransferCreditAmounts Amounts
                                    , PrintMode Mode
                                    , SqlTransaction Trx)
      : base(Mode, Trx)
    {


      // 1. Load HTML structure.
      LoadVoucher("VoucherCardTransferPoints");

      //  4. Load NLS strings from resource
      LoadVoucherResources(SourceAccountId, TargetAccountId, Amounts);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(Int64 SourceAccountId
                                    , Int64 TargetAccountId
                                    , Accounts.TransferCreditAmounts Amounts)
    {
      TYPE_SPLITS _splits;
      String _str_value;
      _str_value = String.Empty;

      // Get Splits information
      if (!Split.ReadSplitParameters(out _splits))
      {
        return;
      }

      //  2. Load Header and Footer
      LoadHeader(_splits.company_a.header);
      LoadFooter(_splits.company_a.footer, _splits.company_a.footer_cashin);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      // General

      _str_value = Resource.String("STR_VOUCHER_TRANSFER_CREDIT_TITLE");
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_TITLE", _str_value);

      _str_value = Resource.String("STR_VOUCHER_TRANSFER_POINTS_ORIGIN");
      AddString("STR_VOUCHER_TRANSFER_POINTS_ORIGIN", _str_value);

      _str_value = Resource.String("STR_VOUCHER_TRANSFER_POINTS_DESTINY");
      AddString("STR_VOUCHER_TRANSFER_POINTS_DESTINY", _str_value);

      //_str_value = Resource.String("STR_VOUCHER_CARD_CARD_ID") + ":";
      AddString("STR_VOUCHER_CARD_CARD_ID", String.Empty);

      _str_value = Resource.String("STR_VOUCHER_CARD_ACCOUNT_ID") + ":";
      AddString("STR_VOUCHER_CARD_ACCOUNT_ID", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_TRANSFER_CREDIT_BALANCE_INITIAL") + ":";
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_BALANCE_INITIAL", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_TRANSFER_CREDIT_TRANSFERED") + ":";
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_TRANSFERED", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_TRANSFER_CREDIT_BALANCE_FINAL") + ":";
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_BALANCE_FINAL", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_TRANSFER_CREDIT_TO_ADD") + ":";
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_TO_ADD", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_TRANSFER_CREDIT_TOTAL_AMOUNT") + ":";
      AddString("STR_VOUCHER_CARD_TRANSFER_POINTS_TOTAL_AMOUNT", _str_value);

      // Source account info
      AddString("VOUCHER_TRANSFER_POINTS_CARD_ID_ORIGIN", String.Empty);
      AddString("VOUCHER_TRANSFER_POINTS_ACCOUNT_ID_ORIGIN", SourceAccountId.ToString());

      // Source account Data
      AddCurrency("VOUCHER_CARD_TRANSFER_POINTS_ORIGIN_BALANCE_INITIAL", Amounts.SourceInitialRedeemableBalance);
      AddCurrency("VOUCHER_CARD_TRANSFER_POINTS_ORIGIN_TO_ADD", Amounts.SourceTransferredAmount);
      AddCurrency("VOUCHER_CARD_TRANSFER_POINTS_ORIGIN_BALANCE_FINAL", Amounts.SourceFinalRedeemableBalance);

      // Destiny account info
      AddString("VOUCHER_TRANSFER_POINTS_CARD_ID_DESTINY", String.Empty);
      AddString("VOUCHER_TRANSFER_POINTS_ACCOUNT_ID_DESTINY", TargetAccountId.ToString());

      // Destiny account Data
      AddCurrency("VOUCHER_CARD_TRANSFER_POINTS_BALANCE_INITIAL", Amounts.TargetInitialRedeemableBalance);
      AddCurrency("VOUCHER_CARD_TRANSFER_POINTS_TO_ADD", Amounts.TargetTransferredAmount);
      AddCurrency("VOUCHER_CARD_TRANSFER_POINTS_BALANCE_FINAL", Amounts.TargetFinalRedeemableBalance);

      // Total
      AddCurrency("VOUCHER_CARD_TRANSFER_POINTS_TOTAL_AMOUNT", Amounts.TargetTransferredAmount);
    }

    #endregion
  }
}
