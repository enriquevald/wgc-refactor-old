﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCancel.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
// 11-JAN-2016  ETP         Fixed Bug 22543 Prize_coupon not aplicable if cashdesk enabled.
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherCancel : Voucher
  {
    #region Constructor

    public VoucherCancel(String[] VoucherAccountInfo
                       , TYPE_SPLITS SplitInformation
                       , TYPE_CASH_REDEEM CompanyB
                       , CancellableOperation CancellableOperation
                       , CASHIER_MOVEMENT MovementType
                       , OperationCode OperationCode
                       , PrintMode Mode
                       , SqlTransaction SQLTransaction)
      : base(Mode, OperationCode, SQLTransaction)
    {
      Currency _total_devolution_b;

      switch (MovementType)
      {

        case CASHIER_MOVEMENT.DEV_SPLIT2:
          {
            CancelledSequenceId = GetSecuenceId_B(CancellableOperation.OperationId, SQLTransaction);

            LoadVoucher("Voucher.B.Cancel");
            LoadHeader(SplitInformation.company_b.header);
            LoadFooter(SplitInformation.company_b.footer, SplitInformation.company_b.footer_cancel);
            AddString("VOUCHER_SPLIT_NAME", SplitInformation.company_b.cash_out_voucher_title);
            AddString("VOUCHER_CONCEPT", SplitInformation.company_b.name);

            VoucherSequenceId = SequenceId.VouchersSplitB;
            if (Misc.IsVoucherModeCodere())
            {
              VoucherSequenceId = SequenceId.VouchersSplitBCancel;
            }

            SetValue("CV_TYPE", (Int32)CashierVoucherType.Cancel_B);
          }
          break;

        default:
          {
            Log.Error("VoucherCancel. MovementType not valid: " + MovementType.ToString());
            return;
          }
      }

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load specific voucher data.
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      // TODO ANDREU: Verificar si amb PromoRedeemable es suficient.
      _total_devolution_b = CancellableOperation.Balance.PromoRedeemable;

      AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_AMOUNT", _total_devolution_b);
      // RCI 18-MAY-2011: Need to use separate TAGs to be able to use AmountWithLetters only in one TAG.
      AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_CASHED_AMOUNT", _total_devolution_b, SplitInformation.company_b.total_in_letters);

      // RCI 18-MAY-2011: Mode 01: Palacio de los Números
      // JML save data for all Modes
      //if (VoucherMode == 1)
      //{
      Currency _base_b;
      Currency _tax_b;

      AddCurrencyInLetters("VOUCHER_CARD_CASH_OUT_LETTERS_TOTAL_AMOUNT", _total_devolution_b);

      _base_b = Math.Round(_total_devolution_b / (1 + SplitInformation.company_b.tax_pct / 100), 2, MidpointRounding.AwayFromZero);
      _tax_b = Math.Round(_base_b * SplitInformation.company_b.tax_pct / 100, 2, MidpointRounding.AwayFromZero);
      _base_b = _total_devolution_b - _tax_b;

      SetValue("CV_M01_BASE", _base_b);
      SetValue("CV_M01_TAX1", _tax_b);

      if (CancelledSequenceId > 0)
      {
        SetValue("CV_M01_CANCELLED_SEQUENCE", CancelledSequenceId);
      }

      //} // if (VoucherMode == 1)

    } // VoucherCancel

    #endregion
  }
}
