﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCardReplacement.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
// 19-OCT-2016  LTC         Bug 14809:Ref.409 - Card replacement - Wrong information in header section - must be B company information
// 22-DEC-2017  EOR         Bug 31053:WIGOS-6903 [Ticket #10771] Mty I: Error in screen "Histórico de transacciones bancarias", no appear the movement "Sustitución tarjeta - Pago con tarjeta bancaria" 
//---------------------------------------------------------------------------------------------

using System;
using System.Data.SqlClient;

namespace WSI.Common
{
  // 10. Card Replacement
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  // Card Id: 
  // Account Id:
  //
  // Initial Balance: $
  // Recharge Amount: $
  // Total:           $
  //
  //  Final Balance:  $
  //
  //         Voucher Footer
  /// <summary>
  /// Add Credit Voucher
  /// </summary>
  public class VoucherCardReplacement : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCardReplacement(PrintMode Mode, TYPE_CARD_REPLACEMENT Params, SqlTransaction SQLTransaction, Boolean CardPaid)
      : base(Mode, CashierVoucherType.CardReplacement, SQLTransaction) // LTC 19-OCT-2016
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCardReplacement");

      // 2. Load voucher resources.
      // RAB 24-MAR-2016: Bug 10745: Add input parameter to function to load card associate or card remplacement operation.
      LoadVoucherResources(CardPaid);

      if (WSI.Common.Misc.IsCardPlayerToCompanyB())
      {
        VoucherSequenceId = SequenceId.VouchersSplitB;
        SetValue("CV_TYPE", (Int32)CashierVoucherType.CardReplacement_B);
      }
      else
      {
        VoucherSequenceId = SequenceId.VouchersSplitA;
        SetValue("CV_TYPE", (Int32)CashierVoucherType.CardReplacement);
      }

        SetValue("CV_M01_BASE", Params.TotalBaseTaxSplit2);
        SetValue("CV_M01_TAX1", Params.TotalTaxSplit2);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      // 4. Load specific data.
      VoucherValuesCardReplacement(Params);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(Boolean CardPaid)
    {
      String value = "";

      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Strings

      // General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":"; AddString("STR_VOUCHER_SITE_ID", value);
      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":"; AddString("STR_VOUCHER_TERMINAL_USERNAME", value);
      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":"; AddString("STR_VOUCHER_AUTHORIZED_BY", value);
      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":"; AddString("STR_VOUCHER_TERMINAL_ID", value);
      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":"; AddString("STR_VOUCHER_VOUCHER_ID", value);

      // Title
      value = CardPaid ? Resource.String("STR_VOUCHER_CARD_REPLACEMENT_001") : Resource.String("STR_FRM_CARD_ASSIGN_001"); AddString("STR_VOUCHER_TITLE", value);

      // Details
      value = Resource.String("STR_VOUCHER_CARD_REPLACEMENT_002") + ":"; AddString("STR_VOUCHER_OLD_CARD_ID", value);
      value = Resource.String("STR_VOUCHER_CARD_REPLACEMENT_006") + ":"; AddString("STR_VOUCHER_NEW_CARD_ID", value);
      value = Resource.String("STR_VOUCHER_CARD_REPLACEMENT_003") + ":"; AddString("STR_VOUCHER_ACCOUNT_ID", value);
      value = CardPaid ? Resource.String("STR_VOUCHER_CARD_REPLACEMENT_004") + ":" : Resource.String("STR_VOUCHER_CARD_REDEEM_PLAYER_CARD") + ":"; AddString("STR_VOUCHER_CARD_REPLACEMENT_PRICE", value);

      // DHA 14-JUL-2015: Comissions
      value = Resource.String("STR_VOUCHER_COMISSION") + ":"; AddString("STR_VOUCHER_COMISSION", value);

      value = Resource.String("STR_VOUCHER_CARD_REPLACEMENT_005"); AddString("STR_VOUCHER_CARD_REPLACEMENT_TOTAL_AMOUNT", value);
    }

    private void VoucherValuesCardReplacement(TYPE_CARD_REPLACEMENT Params)
    {
      //  1. Depósito
      //     ---------------------------
      //  2. Total
      //     ---------------------------

      Currency _comissions;
      String _comisisons_html;
      Currency _total;

      _comissions = 0;

      // DHA 14-JUL-2015 set comissions on card replacement
      if (Params.ExchangeResult != null)
      {
        _comissions = Params.ExchangeResult.Comission;
      }

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", Params.AccountInfo);
      AddCurrency("VOUCHER_CARD_REPLACEMENT_PRICE", Params.card_replacement_price);

      AddCurrency("VOUCHER_CARD_REPLACEMENT_TOTAL_AMOUNT", Params.card_replacement_price + _comissions);

      _comisisons_html = "";

      if ((!Misc.IsCardPlayerToCompanyB() && _comissions > 0) || VoucherMode == 5)
      {
        _comisisons_html += "<tr>";
        _comisisons_html += "  <td align=\"left\" width=\"50%\">#STR_VOUCHER_COMISSION</td>";
        _comisisons_html += "  <td align=\"right\" width=\"50%\">#VOUCHER_COMISSION_AMOUNT</td>";
        _comisisons_html += "</tr>";

        _comisisons_html = _comisisons_html.Replace("#STR_VOUCHER_COMISSION", Resource.String("STR_VOUCHER_COMISSION"));
        _comisisons_html = _comisisons_html.Replace("#VOUCHER_COMISSION_AMOUNT", _comissions.ToString());
      }

      SetParameterValue("@VOUCHER_CARD_COMISSIONS", _comisisons_html);

      switch (VoucherMode)
      {
        case 5: //Logrand
          String _param_name_tax = GeneralParam.GetString("Cashier", "Split.B.Tax.Name");
          Decimal _param_pct_tax = GeneralParam.GetDecimal("Cashier", "Split.B.Tax.Pct");

          AddString("VOUCHER_TAX_DETAIL_SPLIT2", Resource.String("STR_TAX_DETAIL", _param_name_tax));
          AddString("VOUCHER_BASE_TAX_NAME_SPLIT2", Resource.String("STR_BASE"));
          String _pct = String.Format("{0:P2}", (_param_pct_tax / 100));
          AddString("VOUCHER_TAX_NAME_SPLIT2", _param_name_tax + " (" + _pct.Trim() + ")");
          AddCurrency("VOUCHER_BASE_TAX_SPLIT2", Params.TotalBaseTaxSplit2);
          AddCurrency("VOUCHER_TAX_SPLIT2", Params.TotalTaxSplit2);

          break;
      }

    } // VoucherValuesCardReplacement

    #endregion
  }
}
