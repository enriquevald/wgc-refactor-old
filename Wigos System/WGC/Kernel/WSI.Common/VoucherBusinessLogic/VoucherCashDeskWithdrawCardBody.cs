﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : VoucherCashDeskWithdrawCardBody.cs
// 
//   DESCRIPTION : 
//
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 05-AGO-2016 LTC    First release.
// 23-AGO-2016 LTC    Product Backlog Item 16742:TPV Televisa: Reconciliation modification of cashier vouchers
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherCashDeskWithdrawCardBody : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCashDeskWithdrawCardBody(Int32 CountDelivered, Int32 Count, Decimal Amount)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCashDeskWithdrawCardBody");

      // 2. Load voucher resources.
      LoadVoucherResources(CountDelivered, Count, Amount);     
    }

    #endregion

    #region Private Methods
    private void LoadVoucherResources(Int32 CountDelivered, Int32 Count, Decimal Amount)
    {
      String _value;
      String _national_currency;
      CurrencyExchange _currency_exchange = new CurrencyExchange();
      
      _national_currency = CurrencyExchange.GetNationalCurrency();
      
      //CurrencyExchange.ReadCurrencyExchange(IsoType.Type, IsoType.IsoCode, out _currency_exchange);
      ////Title
      //_value = _currency_exchange.Description;
      //if (_national_currency == IsoType.IsoCode && IsoType.Type == CurrencyExchangeType.CURRENCY)
      //{
      //  _value = "";
      //}
      //AddString("STR_VOUCHER_CASH_DESK_WITHDRAW_TITLE", _value);

      // - Voucher Specific for Filler Out
      _value = Resource.String("STR_VOUCHER_CASH_DESK_WITHDRAWAL_COUNT_TITLE") + ":";
      AddString("STR_VOUCHER_CASH_DESK_WITHDRAWAL_COUNT_TITLE", _value);
      AddString("STR_VOUCHER_CASH_DESK_WITHDRAWAL_COUNT", Count.ToString());  //LTC 23-AGO-2016    

      _value = Resource.String("STR_VOUCHER_CASH_DESK_WITHDRAWAL_AMOUNT_TITLE") + ":";
      AddString("STR_VOUCHER_CASH_DESK_WITHDRAWAL_AMOUNT_TITLE", _value);
      AddCurrency("STR_VOUCHER_CASH_DESK_WITHDRAWAL_AMOUNT", (Currency)Amount);

      _value = Resource.String("STR_VOUCHER_CASH_DESK_WITHDRAWAL_GIVEN_TITLE") + ":";
      AddString("STR_VOUCHER_CASH_DESK_WITHDRAWAL_GIVEN_TITLE", _value);
      AddString("STR_VOUCHER_CASH_DESK_WITHDRAWAL_GIVEN", CountDelivered.ToString()); //LTC 23-AGO-2016    

      _value = Resource.String("STR_VOUCHER_CASH_DESK_WITHDRAWAL_FOOTER_SIGNATURE") + ":";
      AddString("STR_VOUCHER_CASH_DESK_WITHDRAWAL_FOOTER_SIGNATURE", _value);

      _value = Resource.String("STR_VOUCHER_CASH_DESK_WITHDRAWAL_FOOTER_ANFIT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_WITHDRAWAL_FOOTER_ANFIT", _value);

      _value = Resource.String("STR_VOUCHER_CASH_DESK_WITHDRAWAL_FOOTER_CASHIER") + ":";
      AddString("STR_VOUCHER_CASH_DESK_WITHDRAWAL_FOOTER_CASHIER", _value);

    } // SetupCashStatusVoucher
    #endregion

    #region Public Methods

    #endregion

  }
}
