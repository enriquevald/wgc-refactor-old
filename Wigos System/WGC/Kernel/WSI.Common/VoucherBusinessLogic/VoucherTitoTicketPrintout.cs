﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherTitoTicketPrintout.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  // 9. Tito Ticket printing
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  // Origin Card Id: 
  // Origin Account Id:
  //
  // Destiny Card Id: 
  // Destiny Account Id:
  //
  // Initial Points Balance: $
  // Transfered Points Amount: $
  // Total Points:           $
  //
  //  Final Points Balance:  $
  //
  //         Voucher Footer
  /// <summary>
  /// Add Credit Voucher
  /// </summary>
  /// 

  public class VoucherTitoTicketPrintout : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructores

    public VoucherTitoTicketPrintout(String[] VoucherAccountInfo,
                                     Int64 TicketId,
                                     String ValidationNumber,
                                     Currency Amount,
                                     DateTime CreatedDateTime,
                                     DateTime ExpirationDateTime,
                                     Number PromotionId,
                                     String MrvFontPath,
                                     PrintMode Mode,
                                     SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load general voucher data.
      //  3. Set specific params

      SetValue("CV_TYPE", (Int32)CashierVoucherType.TicketOut);
      //TODO: revisar si es posible utilizar esta columna
      SetValue("CV_SEQUENCE", TicketId);

      // 1. Load HTML structure.
      LoadVoucher("VoucherTitoTicket");

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Set params
      SetParameterValue("@URL_TICKET_MRV_FONT", "'" + MrvFontPath + "'");

      SetParameterValue("@VOUCHER_TICKET_HEADER_1", "xINSERT THIS SIDE UP");
      SetParameterValue("@VOUCHER_TICKET_HEADER_2", "xUnreadable header text goes here");

      SetParameterValue("@VOUCHER_TICKET_INFO_1", TicketId.ToString());
      SetParameterValue("@VOUCHER_TICKET_INFO_2", "xGAMING VOUCHER");
      SetParameterValue("@VOUCHER_TICKET_INFO_3", Resource.String("STR_VOUCHER_CARD_DRAW_TICKET_NUMBER") + ":&nbsp;" + ValidationNumber);
      SetParameterValue("@VOUCHER_TICKET_INFO_4", Resource.String("STR_UC_TICKET_CREATED_AT") + ":&nbsp;" + CreatedDateTime +
                                                  "&nbsp;&nbsp;" +
                                                  Resource.String("STR_VOUCHER_CARD_ADD_PROMOTION_EXPIRED_DATE") + ":&nbsp;" + ExpirationDateTime);
      SetParameterValue("@VOUCHER_TICKET_INFO_5", "xInstructions on how to use this ticket go here here and here");
      SetParameterValue("@VOUCHER_TICKET_INFO_6", Resource.String("STR_UC_TICKET_QUANTITY") + ":&nbsp;" + Amount.ToString());
      SetParameterValue("@VOUCHER_TICKET_INFO_7", "xUnreadable text goes here. TDB");

      SetParameterValue("@VOUCHER_TICKET_FOOTER_1", "xCompany info 3 here");
      SetParameterValue("@VOUCHER_TICKET_FOOTER_2", "xCompany info 2 here");
      SetParameterValue("@VOUCHER_TICKET_FOOTER_3", "xCompany info 1 here");
      SetParameterValue("@VOUCHER_TICKET_FOOTER_4", "xINSERT THIS SIDE UP");
    }

    /// <summary>
    /// second version of constructor with dynamic parameters
    /// </summary>
    /// <param name="Params"></param>
    /// <param name="SQLTransaction"></param>
    //public VoucherTitoTicketPrintout(SmartParams Params, PrintMode Mode, SqlTransaction SQLTransaction)
    //  : base(Mode, SQLTransaction)
    //{
    //  // Constructor actions:
    //  //  1. Load passed params
    //  //  2. Load HTML structure.
    //  //  3. Load general voucher data.
    //  //  4. Set specific params

    //  //TODO: String[] VoucherAccountInfo = (String[])Params.Get("VoucherAccountInfo");
    //  String _html_space = ":&nbsp;";

    //  SetValue("CV_TYPE", Params.AsShort("CashierVoucherType"));
    //  //TODO: revisar si es posible utilizar esta columna
    //  SetValue("CV_SEQUENCE", Params.AsString("TicketId"));

    //  // 1. Load HTML structure.
    //  LoadVoucher("VoucherTitoTicket");

    //  // 2. Load general voucher data.
    //  LoadVoucherGeneralData();

    //  // 3. Set params
    //  SetParameterValue("@URL_TICKET_MRV_FONT", "'" + Params.AsString("MrvFontPath") + "'");

    //  SetParameterValue("@VOUCHER_TICKET_HEADER_1", "xINSERT THIS SIDE UP");
    //  SetParameterValue("@VOUCHER_TICKET_HEADER_2", "xUnreadable header text goes here");

    //  SetParameterValue("@BAR_CODE_FIRST", Params.AsString("ValidationNumber"));
    //  SetParameterValue("@BAR_CODE_SECOND", Params.AsString("ValidationNumber"));

    //  SetParameterValue("@VOUCHER_TICKET_INFO_1", Params.AsString("Title"));
    //  SetParameterValue("@VOUCHER_TICKET_INFO_2", Params.AsString("Name"));
    //  SetParameterValue("@VOUCHER_TICKET_INFO_3", Resource.String("STR_VOUCHER_CARD_DRAW_TICKET_NUMBER") + _html_space +
    //                                              Params.AsString("ValidationNumber"));
    //  SetParameterValue("@VOUCHER_TICKET_INFO_4", Resource.String("STR_UC_TICKET_CREATED_AT") + _html_space +
    //                                              Params.AsString("CreationDate") + _html_space + _html_space +
    //                                              Resource.String("STR_VOUCHER_CARD_ADD_PROMOTION_EXPIRED_DATE") + _html_space +
    //                                              Params.AsString("ExpirationDate"));
    //  SetParameterValue("@VOUCHER_TICKET_INFO_5", Params.AsString("Description"));
    //  SetParameterValue("@VOUCHER_TICKET_INFO_6", Resource.String("STR_UC_TICKET_QUANTITY") + _html_space +
    //                                              Params.AsString("Amount"));
    //  SetParameterValue("@VOUCHER_TICKET_INFO_7", "xUnreadable text goes here. TDB");

    //  SetParameterValue("@VOUCHER_TICKET_FOOTER_1", Params.AsString("Location"));
    //  SetParameterValue("@VOUCHER_TICKET_FOOTER_2", Params.AsString("Address1"));
    //  SetParameterValue("@VOUCHER_TICKET_FOOTER_3", Params.AsString("Address2"));
    //  SetParameterValue("@VOUCHER_TICKET_FOOTER_4", "xINSERT THIS SIDE UP");
    //}

    #endregion
  }
}
