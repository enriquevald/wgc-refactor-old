﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherTerminalsRefillDetail.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherTerminalsRefillDetail : Voucher
  {

    #region Constructor

    public VoucherTerminalsRefillDetail(String TerminalName, Decimal TotalToRefill)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherTerminalsRefillDetail");

      // 2. Load voucher resources.
      LoadVoucherResources(TerminalName, TotalToRefill);
    }

    #endregion

    #region Private Methods
    private void LoadVoucherResources(String TerminalName, Decimal TotalToRefill)
    {
      String value;
      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);
      AddString("VOUCHER_TERMINAL_COLLECT_REFILL_NAME", TerminalName);

      value = Resource.String("STR_LBL_REFILL") + ":";
      AddString("STR_LBL_REFILL", value);
      AddCurrency("VOUCHER_REFILL_AMOUNT", TotalToRefill);
    }
    #endregion

  }
}
