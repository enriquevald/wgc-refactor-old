﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCashOut.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
// 13-SEP-2013  EOR         PBI 17467: Televisa - Service Charge: Voucher
// 04-OCT-2016  RGR         PBI 17447: Televisa - Draw Cash (Cash prize) - Recharge Operation
// 09-DEC-2016  ATB         Bug 21350: Hide the Service Charge info at voucher
// 17-ENE-2017  DPC         Bug 13216:En la previsualización del ticket de pago de premio en efectivo sale una línea de "Tickets Pagados"
// 08-FEB-2017  JML         BUG 25253: Errors on Custody in case Type Machines (1)
// 08-MAR-2017  FGB         PBI 25268: Third TAX - Payment Voucher
// 17-MAR-2017  FJC         Bug 25890:Sorteo de máquina -El ticket de reintegro no muestra el total pagado con redondeo
// 28-MAR-2016  FJC         Bug 26250:Al hacer una reintegro sin juego no se muestra la información correcta en el ticket de pago - Aguascalientes
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using WSI.Common.TITO;

namespace WSI.Common
{
  public class VoucherCashOut : Voucher
  {
    #region Constructor

    public VoucherCashOut(String[] VoucherAccountInfo
                        , Int64 AccountId
                        , Currency BalanceBeforeCashOut
                        , TYPE_SPLITS SplitInformation
                        , TYPE_CASH_REDEEM CompanyA
                        , TYPE_CASH_REDEEM CompanyB
                        , CASHIER_MOVEMENT MovementType
                        , PrintMode Mode
                        , List<VoucherValidationNumber> ValidationNumbers
                        , String BarCodeExternal
                        , Currency CashPayment
                        , Currency CheckPayment
                        , OperationCode OperationCode
                        , SqlTransaction SQLTransaction
                        , Boolean IsChipsPurchaseWithCashOut
                        , Boolean IsApplyTaxes
                        , Boolean IsTicketPayment)
      : base(Mode, OperationCode, SQLTransaction)
    {
      Currency _deductions;
      Currency _total_dev_ab;
      String _str_html;
      String _previous_terminal_name;
      String _payment_check;
      String _style_barcode;
      Currency _service_charge;   // EOR 21-SEP-2016

      _previous_terminal_name = String.Empty;
      _service_charge = 0;      // EOR 21-SEP-2016

      if (!InitVoucher(MovementType, SplitInformation))
      {
        Log.Error("VoucherCashOut. MovementType not valid: " + MovementType.ToString());
        return;
      }

      SetValue("CV_ACCOUNT_ID", AccountId);

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load specific voucher data.

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      _str_html = String.Empty;
      if (ValidationNumbers != null && ValidationNumbers.Count > 0)
      {
        // Group terminals
        ValidationNumbers.Sort(delegate(VoucherValidationNumber p1, VoucherValidationNumber p2)
        {
          return p1.CreatedTerminalName.CompareTo(p2.CreatedTerminalName);
        });

        _str_html = "<tr>";
        _str_html += "<td><p>";
        _str_html += String.Format("{0} {1}", Resource.String("STR_VOUCHER_VALIDATION_NUMBERS_TITLE"), ValidationNumbers.Count);
        _str_html += "</p></td>";
        _str_html += "</tr>";

        for (int _idx = 0; _idx < ValidationNumbers.Count; _idx++)
        {
          // Add terminal name for a groups of tickets
          if (String.Compare(_previous_terminal_name, ValidationNumbers[_idx].CreatedTerminalName) != 0)
          {
            _previous_terminal_name = ValidationNumbers[_idx].CreatedTerminalName;

            _str_html += "<td colspan=\"2\" align=\"left\">@TICKET_TITO_TERMINAL_NAME</td>";
            _str_html = _str_html.Replace("@TICKET_TITO_TERMINAL_NAME", HttpUtility.HtmlEncode(Resource.String("STR_VOUCHER_TERMINAL_ID") + ": " + _previous_terminal_name));
          }

          _str_html += "<tr>";
          _str_html += "  <td align=\"left\"  width=\"80%\">&nbsp&nbsp@VALIDATION_NUMBER</td>";
          _str_html += "  <td align=\"right\" width=\"20%\">@VALUE</td>";
          _str_html += "</tr>";

          _str_html = _str_html.Replace("@VALIDATION_NUMBER", ValidationNumberManager.FormatValidationNumber(ValidationNumbers[_idx].ValidationNumber, (Int32)ValidationNumbers[_idx].ValidationType, true));
          _str_html = _str_html.Replace("@VALUE", HttpUtility.HtmlEncode(ValidationNumbers[_idx].Amount.ToString()));
        }

        _str_html += "<tr><td>&nbsp;</td></tr>";
      }

      SetParameterValue("@VALIDATION_NUMBERS", _str_html);

      // Deductions = tax1 + tax2 + tax3 + service_charge, deductions is a negative number
      if (IsApplyTaxes)
      {
        _deductions = -(CompanyA.tax1 + CompanyA.tax2 + CompanyA.tax3 + CompanyB.service_charge);
      }
      else
      {
        _deductions = 0;
      }

      // Total devolution AB
      _total_dev_ab = CompanyA.TotalDevolution + CompanyB.TotalDevolution;

      AddCurrency("VOUCHER_TOTAL_CASH_IN", CompanyA.total_cash_in);

      //SDS 18-05-2016 Bug 13001 Prize Payout cancel Not Redeemable
      if (OperationCode != OperationCode.PRIZE_PAYOUT_AND_RECHARGE)
      {
        AddCurrency("VOUCHER_CARD_CASH_OUT_NO_REDEEM", CompanyA.lost_balance.Balance.PromoNotRedeemable);
      }
      else
      {
        AddCurrency("VOUCHER_CARD_CASH_OUT_NO_REDEEM", 0);
      }

      if (Tax.EnableTitoTaxWaiver(true))
      {
        AddCurrency("VOUCHER_CARD_CASH_OUT_DEVOLUTION", _deductions);
      }
      else
      {
        AddCurrency("VOUCHER_CARD_CASH_OUT_DEVOLUTION", CompanyA.TotalDevolution);

        // 21-APR-2016 LTC
        if (Misc.IsTaxCustodyEnabled()
         && (Misc.GetTaxCustodyType() == (Int32)Misc.TaxCustodyType.Both
         || (Misc.GetTaxCustodyType() == (Int32)Misc.TaxCustodyType.OnlyGamingTables && IsChipsPurchaseWithCashOut)
         || (Misc.GetTaxCustodyType() == (Int32)Misc.TaxCustodyType.OnlyMachines && !IsChipsPurchaseWithCashOut)))
        {

          SetParameterValue("@VOUCHER_CARD_ADD_CUSTODY",
                              "<tr>" +
                                "<td align=\"left\"  width=\"50%\" style=\"height: 16px\">@VOUCHER_CARD_ADD_CUSTODY_NAME:</td>" +
                                "<td align=\"right\" width=\"50%\" style=\"height: 16px\">@VOUCHER_CARD_ADD_CUSTODY_DEV</td>" +
                              "</tr>");

          AddString("VOUCHER_CARD_ADD_CUSTODY_NAME", Misc.TaxCustodyRefundName());
          AddCurrency("VOUCHER_CARD_ADD_CUSTODY_DEV", CompanyA.DevolutionCustody);
        }
        else
        {
          SetParameterValue("@VOUCHER_CARD_ADD_CUSTODY", "");
        }
      }

      if (!TITO.Utils.IsTitoMode())
      {
        AddCurrency("VOUCHER_CARD_CASH_OUT_PRIZE", CompanyA.prize);
      }

      //Add taxes
      AddTaxesValues(CompanyA, IsApplyTaxes);

      //EOR 13-SEP-2013
      if (Misc.IsVoucherModeTV())
      {
        if (CompanyB.service_charge > 0)
        {
          _service_charge = CompanyB.service_charge * -1;
        }

        // ATB 09-DEC-2016
        // When Service Charge is not enabled, this option won't be shown
        if (!IsChipsPurchaseWithCashOut)
        {
          if (GeneralParam.Value("Cashier.ServiceCharge", "Enabled") == "1")
          {
            AddString("VOUCHER_SERVICE_CHARGE_NAME", GeneralParam.Value("Cashier.ServiceCharge", "Text") + ":");
            AddString("VOUCHER_SERVICE_CHARGE_VALUE", _service_charge.ToString());
          }
          else
          {
            AddString("VOUCHER_SERVICE_CHARGE_NAME", "");
            AddString("VOUCHER_SERVICE_CHARGE_VALUE", "");
          }
        }
        else
        {
          if (GeneralParam.Value("GamingTables.ServiceCharge", "Enabled") == "1")
          {
            AddString("VOUCHER_SERVICE_CHARGE_NAME", GeneralParam.Value("GamingTables.ServiceCharge", "Text") + ":");
            AddString("VOUCHER_SERVICE_CHARGE_VALUE", _service_charge.ToString());
          }
          else
          {
            AddString("VOUCHER_SERVICE_CHARGE_NAME", "");
            AddString("VOUCHER_SERVICE_CHARGE_VALUE", "");
          }
        }

        

        if (CompanyA.card_deposit == 0)
        {
          SetParameterValue("@VOUCHER_CARD_ADD_CARD", "");
        }
      }

      if (VoucherMode == 7)
      {
        AddCurrency("VOUCHER_CARD_CASH_OUT_CARD_DEPOSIT", CompanyA.card_deposit);
      }

      if (Misc.IsCardPlayerToCompanyB())
      {
        SetParameterValue("@VOUCHER_CARD_ADD_CARD", "");
      }
      else
      {
        SetParameterValue("@VOUCHER_CARD_ADD_CARD",
                                    "<tr>" +
                                      "<td align=\"left\"  width=\"50%\" style=\"height: 16px\">@VOUCHER_CARD_ADD_CARD_NAME:</td>" +
                                      "<td align=\"right\" width=\"50%\" style=\"height: 16px\">@VOUCHER_CARD_ADD_CARD_DEPOSIT</td>" +
                                    "</tr>");

        AddString("VOUCHER_CARD_ADD_CARD_NAME", Misc.GetCardPlayerConceptName());
        AddCurrency("VOUCHER_CARD_ADD_CARD_DEPOSIT", CompanyA.card_deposit);
      }

      if (IsTicketPayment)
      {
        if (IsApplyTaxes)
        {
          // EOR 21-SEP-2016
          if (Misc.IsVoucherModeTV())
          {
            // JBP 07-03-2017 
            // FJC 28-MAR-2016 Bug 26250:Al hacer una reintegro sin juego no se muestra la información correcta en el ticket de pago - Aguascalientes
            //if (Misc.Common_CheckPendingTerminalDrawsRecharges(AccountId))
            //{
            //  //AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_AMOUNT", CompanyA.TotalDevolution - CompanyB.ServiceCharge + CompanyA.TotalRounding, SplitInformation.company_a.total_in_letters);
            //  AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_AMOUNT", CompanyA.TotalPaid - CompanyB.ServiceCharge, SplitInformation.company_a.total_in_letters);
            //}
            //else
            //{
            AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_AMOUNT", CompanyA.TotalPaid - CompanyB.ServiceCharge, SplitInformation.company_a.total_in_letters);
            //}
          }
          else
          {
            AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_AMOUNT", CompanyA.TotalPaid, SplitInformation.company_a.total_in_letters);
          }
        }
        else
        {
          AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_AMOUNT", CompanyA.prize, SplitInformation.company_a.total_in_letters);
        }
      }
      else
      {
        // EOR 21-SEP-2016
        if (Misc.IsVoucherModeTV())
        {
          AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_AMOUNT", CompanyA.TotalPaid - CompanyB.ServiceCharge, SplitInformation.company_a.total_in_letters);
        }
        else
        {
          AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_AMOUNT", CompanyA.TotalPaid, SplitInformation.company_a.total_in_letters);
        }
      }

      AddRounding(CompanyA);

      //Add cancelled point to voucher
      AddCanceledPoints(CompanyA);

      // RCI 18-MAY-2011: Mode 01: Palacio de los Números
      // JML save data for all Modes
      //if (VoucherMode == 1)
      //{
      Currency _ini_redeemable_balance;
      Currency _fin_redeemable_balance;

      _ini_redeemable_balance = BalanceBeforeCashOut - CompanyA.lost_balance.Balance.PromoRedeemable - CompanyA.lost_balance.Balance.PromoNotRedeemable;
      _fin_redeemable_balance = _ini_redeemable_balance - CompanyA.TotalRedeemed;

      AddString("VOUCHER_CARD_ACCOUNT_ID", AccountId.ToString("000000"));
      AddCurrency("VOUCHER_CARD_CASH_OUT_PREVIOUS_BALANCE", _ini_redeemable_balance);
      AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_CASH_OUT", CompanyA.TotalRedeemed);
      AddCurrency("VOUCHER_CARD_CASH_OUT_TAXES_VALUE", CompanyA.TotalTaxes);

      if (IsTicketPayment)
      {
        if (IsApplyTaxes)
        {
          AddCurrency("VOUCHER_CARD_CASH_OUT_AMOUNT_TO_PAY", CompanyA.TotalRedeemAfterTaxes);
        }
        else
        {
          AddCurrency("VOUCHER_CARD_CASH_OUT_AMOUNT_TO_PAY", CompanyA.prize);
        }
      }
      else
      {
        AddCurrency("VOUCHER_CARD_CASH_OUT_AMOUNT_TO_PAY", CompanyA.TotalRedeemAfterTaxes);
      }

      AddCurrency("VOUCHER_CARD_CASH_OUT_FINAL_BALANCE", _fin_redeemable_balance);

      // RRB 28-SEP-2012: Mode 03: Codere
      AddCurrency("VOUCHER_TOTAL_BEFORE_CASH_OUT", BalanceBeforeCashOut);
      AddCurrency("VOUCHER_PROMOTION", -CompanyA.lost_balance.Balance.TotalBalance + CompanyB.TotalDevolution);
      AddCurrency("VOUCHER_DEDUCTIONS", _deductions);
      AddCurrency("ROUNDING_VALUE", CompanyA.TotalRounding);
      AddCurrency("VOUCHER_TOTAL_DEVOLUTION", _total_dev_ab);

      if (IsTicketPayment)
      {
        if (IsApplyTaxes)
        {
          AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_PAID_AB", CompanyA.TotalPaid + CompanyB.TotalPaid, SplitInformation.company_a.total_in_letters);
        }
        else
        {
          AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_PAID_AB", CompanyA.prize, SplitInformation.company_a.total_in_letters);
        }
      }
      else
      {
        AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_PAID_AB", CompanyA.TotalPaid + CompanyB.TotalPaid, SplitInformation.company_a.total_in_letters);
      }

      // Set necessary values for reports.
      SetValue("CV_M01_BASE", CompanyA.prize);
      SetValue("CV_M01_TAX1", CompanyA.tax1);
      SetValue("CV_M01_TAX2", CompanyA.tax2);
      SetValue("CV_M01_TAX3", CompanyA.tax3);
      SetValue("CV_M01_DEV", CompanyA.TotalDevolution);
      SetValue("CV_M01_FINAL", _fin_redeemable_balance);
      //} // if (VoucherMode == 1)

      // DCS 16-03-2014:  If exists check include divided payment with check and payment with cash
      _payment_check = "";
      _style_barcode = "";

      if (CheckPayment > 0)
      {
        _payment_check += "<br />";
        _payment_check += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">";
        _payment_check += "  <tr>";
        _payment_check += "    <td align=\"left\" width=\"50%\">#PAYMENT_ORDER_CASH_NAME#</td>";
        _payment_check += "    <td align=\"right\" width=\"50%\">#PAYMENT_ORDER_CASH#</td>";
        _payment_check += "  </tr>";
        _payment_check += "  <tr>";
        _payment_check += "    <td align=\"left\" width=\"50%\">#PAYMENT_ORDER_CHECK_NAME#</td>";
        _payment_check += "    <td align=\"right\" width=\"50%\">#PAYMENT_ORDER_CHECK#</td>";
        _payment_check += "  </tr>";
        _payment_check += "  <tr>";
        _payment_check += "    <td colspan=\"2\" align=\"center\">";
        _payment_check += "      <br />";
        _payment_check += "      <div id='inputdata'/>";
        _payment_check += "     </td>";
        _payment_check += "  </tr>";
        _payment_check += "</table>";

        _payment_check = _payment_check.Replace("#PAYMENT_ORDER_CASH_NAME#", Resource.String("STR_VOUCHER_PAY_CHECK_CASH"));
        _payment_check = _payment_check.Replace("#PAYMENT_ORDER_CASH#", CashPayment.ToString());

        _payment_check = _payment_check.Replace("#PAYMENT_ORDER_CHECK_NAME#", Resource.String("STR_VOUCHER_PAY_CHECK_CHECK"));
        _payment_check = _payment_check.Replace("#PAYMENT_ORDER_CHECK#", CheckPayment.ToString());

        _style_barcode = "<script type='text/javascript'>$('#inputdata').barcode('#PAYMENT_BARCODE_ORDER_ID#', 'int25');</script>";
        _style_barcode = _style_barcode.Replace("#PAYMENT_BARCODE_ORDER_ID#", BarCodeExternal);
      }

      SetParameterValue("@VOUCHER_BARCODE_PAYMENT_CHECK", _style_barcode);
      SetParameterValue("@VOUCHER_AMOUNTS_PAYMENT_CHECK", _payment_check);

      if (WSI.Common.TITO.Utils.IsTitoMode() || VoucherMode == 7)
      {
        String _param_name_tax;
        StringBuilder _html_code;

        _html_code = new StringBuilder();

        _html_code.AppendLine("<tr>");
        _html_code.AppendLine("  <td>");
        _html_code.AppendLine("    <table width='100%' border='0' cellpadding='0' cellspacing='0'>");
        _html_code.AppendLine("      <tr>");
        _html_code.AppendLine("        <td colspan='3'>");
        _html_code.AppendLine("          @VOUCHER_TAX_DETAIL:");
        _html_code.AppendLine("        </td>");
        _html_code.AppendLine("      </tr>");
        _html_code.AppendLine("      <tr>");
        _html_code.AppendLine("        <td width='5%'></td>");
        _html_code.AppendLine("        <td>");
        _html_code.AppendLine("          @VOUCHER_BASE_TAX_NAME:");
        _html_code.AppendLine("        </td>");
        _html_code.AppendLine("        <td width='40%' align='right'>");
        _html_code.AppendLine("          @VOUCHER_BASE_TAX");
        _html_code.AppendLine("        </td>");
        _html_code.AppendLine("      </tr>");
        _html_code.AppendLine("      @TAXES");
        _html_code.AppendLine("    </table>");
        _html_code.AppendLine("    <hr noshade size='4' /> ");
        _html_code.AppendLine("  </td>");
        _html_code.AppendLine("</tr>");

        SetParameterValue("@TABLE_WITHHOLDING", _html_code.ToString());

        _param_name_tax = Resource.String("STR_VOUCHER_HANDPAY_WITHHOLDING");

        AddString("VOUCHER_TAX_DETAIL", Resource.String("STR_HANDPAY_TAX_DETAIL", _param_name_tax.ToString()));
        AddString("VOUCHER_BASE_TAX_NAME", Resource.String("STR_BASE"));
        SetParameterValue("@TAXES", DevolutionPrizeParts.GetTaxesHtml(CompanyA.prize, true, true, IsChipsPurchaseWithCashOut, IsApplyTaxes, OperationCode));

        // MPO 17-MAR-2015: WIG-2154: It isn't necessary show the base amount when there aren't tax amount.
        AddCurrency("VOUCHER_BASE_TAX", CompanyA.prize);
      }
      else
      {
        SetParameterValue("@TABLE_WITHHOLDING", String.Empty);
      }

      SetSignatureRoomByVoucherType(this.GetType());
    } // VoucherCashOut

    /// <summary>
    /// Add rounding to voucher
    /// </summary>
    /// <param name="CompanyA"></param>
    private void AddRounding(TYPE_CASH_REDEEM CompanyA)
    {
      StringBuilder _html_code;

      if (CompanyA.RoundingEnabled)
      {
        _html_code = new StringBuilder();
        _html_code.AppendLine("<tr>");
        _html_code.AppendLine("  <td align=\"left\"  width=\"70%\">@ROUNDING_NAME</td>");
        _html_code.AppendLine("  <td align=\"right\" width=\"30%\">@ROUNDING_VALUE</td>");
        _html_code.AppendLine("</tr>");

        SetParameterValue("@VOUCHER_CARD_CASH_OUT_ROUNDING", _html_code.ToString());

        AddString("ROUNDING_NAME", Resource.String("STR_UC_CARD_BALANCE_ROUNDING") + ":");
        AddCurrency("ROUNDING_VALUE", CompanyA.TotalRounding);
      }
      else
      {
        SetParameterValue("@VOUCHER_CARD_CASH_OUT_ROUNDING", "");
      }
    }

    /// <summary>
    /// Add canceled points to voucher
    /// </summary>
    /// <param name="CompanyA"></param>
    private void AddCanceledPoints(TYPE_CASH_REDEEM CompanyA)
    {
      String _points_canceled;

      _points_canceled = String.Empty;

      if (CompanyA.lost_balance.Points > 0)
      {
        _points_canceled += "<tr>";
        _points_canceled += "  <td align=\"left\"  width=\"50%\" style=\"height: 16px\"></td>";
        _points_canceled += "  <td align=\"right\" width=\"50%\" style=\"height: 16px\"></td>";
        _points_canceled += "</tr>";

        _points_canceled += "<tr>";
        _points_canceled += "  <td align=\"left\"  width=\"50%\" style=\"height: 16px\">#POINTS_CANCELED_LABEL#</td>";
        _points_canceled += "  <td align=\"right\" width=\"50%\" style=\"height: 16px\">#POINTS_CANCELED_VALUE#</td>";
        _points_canceled += "</tr>";

        _points_canceled += "<tr>";
        _points_canceled += "  <td align=\"left\"  width=\"50%\" style=\"height: 16px\"></td>";
        _points_canceled += "  <td align=\"right\" width=\"50%\" style=\"height: 16px\"></td>";
        _points_canceled += "</tr>";

        String _label_canceled_points;
        _label_canceled_points = Resource.String("STR_VOUCHER_CARD_ADD_POINTS_CANCELED");

        _points_canceled = _points_canceled.Replace("#POINTS_CANCELED_LABEL#", _label_canceled_points);
        _points_canceled = _points_canceled.Replace("#POINTS_CANCELED_VALUE#", ((Points)CompanyA.lost_balance.Points).ToString());
      }

      SetParameterValue("@VOUCHER_CARD_CASH_OUT_POINTS_CANCELED", _points_canceled);
    }

    /// <summary>
    /// Add taxes to voucher
    /// </summary>
    /// <param name="CompanyA"></param>
    /// <param name="IsApplyTaxes"></param>
    private void AddTaxesValues(TYPE_CASH_REDEEM CompanyA, Boolean IsApplyTaxes)
    {
      Currency _tax1, _tax2, _tax3;

      _tax1 = 0;
      _tax2 = 0;
      _tax3 = 0;

      //Taxes
      // Taxes is a negative number
      if (CompanyA.tax1 > 0)
      {
        _tax1 = CompanyA.tax1 * -1;
      }

      // Taxes is a negative number
      if (CompanyA.tax2 > 0)
      {
        _tax2 = CompanyA.tax2 * -1;
      }

      // Taxes is a negative number
      if (CompanyA.tax3 > 0)
      {
        _tax3 = CompanyA.tax3 * -1;
      }

      AddStringIf("VOUCHER_CARD_CASH_OUT_TAX1_NAME", CompanyA.tax1_name + ":", (CompanyA.tax1_pct != 0));
      AddStringIf("VOUCHER_CARD_CASH_OUT_TAX2_NAME", CompanyA.tax2_name + ":", (CompanyA.tax2_pct != 0));
      AddStringIf("VOUCHER_CARD_CASH_OUT_TAX3_NAME", CompanyA.tax3_name + ":", (CompanyA.tax3_pct != 0));

      AddCurrencyIf("VOUCHER_CARD_CASH_OUT_TAX1_VALUE", _tax1, ((CompanyA.tax1_pct != 0) && IsApplyTaxes));
      AddCurrencyIf("VOUCHER_CARD_CASH_OUT_TAX2_VALUE", _tax2, ((CompanyA.tax2_pct != 0) && IsApplyTaxes));
      AddCurrencyIf("VOUCHER_CARD_CASH_OUT_TAX3_VALUE", _tax3, ((CompanyA.tax3_pct != 0) && IsApplyTaxes));
    }

    /// <summary>
    /// Init common voucher properties
    /// </summary>
    /// <param name="SplitInformation"></param>
    /// <param name="MovementType"></param>
    /// <returns></returns>
    private Boolean InitVoucher(CASHIER_MOVEMENT MovementType, TYPE_SPLITS SplitInformation)
    {
      switch (MovementType)
      {
        case CASHIER_MOVEMENT.DEV_SPLIT1:
        case CASHIER_MOVEMENT.HANDPAY:
          if (WSI.Common.TITO.Utils.IsTitoMode())
          {
            LoadVoucher("Voucher.A.TITO.CashOut");
          }
          else
          {
            LoadVoucher("Voucher.A.CashOut");
          }

          LoadHeader(SplitInformation.company_a.header);
          LoadFooter(SplitInformation.company_a.footer, SplitInformation.company_a.footer_dev);
          AddString("VOUCHER_SPLIT_NAME", SplitInformation.company_a.cash_out_voucher_title);

          VoucherSequenceId = SequenceId.VouchersSplitA;
          SetValue("CV_TYPE", (Int32)CashierVoucherType.CashOut_A);

          return true;

        default:
          //Show log
          return false;
      }
    }
    #endregion
  }
}
