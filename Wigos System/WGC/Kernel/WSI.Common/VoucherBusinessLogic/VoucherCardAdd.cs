﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCardAdd.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  // 1. Card Buy/Add Credit (Compra/Recarga)
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  // Card Id: 
  // Account Id:
  //
  // Initial Balance: $
  // Recharge Amount: $
  // Total:           $
  //
  //  Final Balance:  $
  //
  //         Voucher Footer
  /// <summary>
  /// Add Credit Voucher
  /// </summary>
  public class VoucherCardAdd : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCardAdd(Boolean ShowDeposit, Boolean Promotion, PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCardAdd");

      // 2. Load voucher resources.
      LoadVoucherResources(ShowDeposit, Promotion);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(Boolean ShowDeposit, Boolean Promotion)
    {
      String value = "";

      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Values

      // - Title
      value = Resource.String("STR_VOUCHER_CARD_ADD_TITLE");
      AddString("STR_VOUCHER_CARD_ADD_TITLE", value);

      // - General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

      value = Resource.String("STR_VOUCHER_CARD_MB_ID") + ":";
      AddString("STR_VOUCHER_CARD_MB_ID", value);

      // Voucher Specific 
      if (Promotion)
      {
        AddString("STR_VOUCHER_CARD_ADD_CURRENT", "");

        value = Resource.String("STR_VOUCHER_CARD_ADD_PROMOTION_EXPIRED_DATE") + ":";
        AddString("STR_VOUCHER_CARD_ADD_AMOUNT_TO_ADD", value);

        value = Resource.String("STR_VOUCHER_CARD_ADD_PROMOTION_VOUCHER") + ":";
        AddString("STR_VOUCHER_CARD_ADD_BALANCE_INITIAL", value);
      }
      else
      {
        value = Resource.String("STR_VOUCHER_CARD_ADD_CURRENT") + ":";
        AddString("STR_VOUCHER_CARD_ADD_CURRENT", value);

        value = Resource.String("STR_VOUCHER_CARD_ADD_AMOUNT_TO_ADD") + ":";
        AddString("STR_VOUCHER_CARD_ADD_AMOUNT_TO_ADD", value);

        value = Resource.String("STR_VOUCHER_CARD_ADD_BALANCE_INITIAL") + ":";
        AddString("STR_VOUCHER_CARD_ADD_BALANCE_INITIAL", value);
      }
      value = "";
      if (ShowDeposit)
      {
        value = WSI.Common.Misc.GetCardPlayerConceptName() + ":";
      }
      AddString("STR_VOUCHER_CARD_ADD_CARD_DEPOSIT", value);

      if (Promotion)
      {
        AddString("STR_VOUCHER_CARD_ADD_BALANCE_FINAL", "");
      }
      else
      {
        value = Resource.String("STR_VOUCHER_CARD_ADD_BALANCE_FINAL") + ":";
        AddString("STR_VOUCHER_CARD_ADD_BALANCE_FINAL", value);
      }

      value = Resource.String("STR_VOUCHER_CARD_ADD_TOTAL_AMOUNT");
      AddString("STR_VOUCHER_CARD_ADD_TOTAL_AMOUNT", value);

    }

    #endregion

    #region Public Methods

    #endregion
  }
}
