﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherGiftDelivery.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  // 15. Gift Delivery
  // Format:
  //     Voucher Logo
  //    Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //    Voucher Title
  //
  // Card Id: 
  // Account Id:
  //
  // Initial Points Balance: $
  // Consumed Points:        $
  // Final Points Balance:   $
  //
  //        Gift Name
  //
  //       Gift Number
  //
  //   Voucher Footer
  /// <summary>
  /// Add Credit Voucher
  /// </summary>

  public class VoucherGiftDelivery : Voucher
  {
    #region Constructor

    public VoucherGiftDelivery(String[] VoucherAccountInfo,
                                String GiftNumber,
                                String GiftName,
                                PrintMode Mode,
                                SqlTransaction SQLTransaction)

      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.
      //  4. Load voucher parameters

      // 1. Load HTML structure.
      LoadVoucher("VoucherGiftDelivery");

      // 2. Load voucher resources.
      LoadVoucherResources();

      // 3. Load general voucher data.
      LoadVoucherGeneralData();

      //  4. Load voucher parameters
      //      - Account Info
      //      - Initial Points Balance
      //      - Spent Points
      //      - Gift Name
      //      - Voucher track data

      //      - Account Info
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      //      - Gift Name
      AddString("VOUCHER_GIFT_DELIVERY_NUMBER", GiftNumber);
      AddString("VOUCHER_GIFT_DELIVERY_GIFT_NAME", GiftName);
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources()
    {
      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Strings

      // Header
      AddString("STR_VOUCHER_SITE_ID", Resource.String("STR_VOUCHER_SITE_ID") + ":");
      AddString("STR_VOUCHER_TERMINAL_USERNAME", Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":");
      AddString("STR_VOUCHER_AUTHORIZED_BY", Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":");
      AddString("STR_VOUCHER_TERMINAL_ID", Resource.String("STR_VOUCHER_TERMINAL_ID") + ":");
      AddString("STR_VOUCHER_VOUCHER_ID", Resource.String("STR_VOUCHER_VOUCHER_ID") + ":");

      // Title
      AddString("STR_VOUCHER_TITLE", Resource.String("STR_VOUCHER_GIFT_DELIVERY_001"));   // "Entrega de Regalo"

      // Details
      AddString("STR_VOUCHER_GIFT_DELIVERY_NUMBER", Resource.String("STR_VOUCHER_GIFT_DELIVERY_002") + ":");
      AddString("STR_VOUCHER_GIFT_DELIVERY_REQUEST_DATE", Resource.String("STR_VOUCHER_GIFT_DELIVERY_004") + ":");
      AddString("STR_VOUCHER_GIFT_DELIVERY_GIFT_NAME", Resource.String("STR_VOUCHER_GIFT_DELIVERY_003") + ":");
    }

    #endregion

    #region Public Methods
    #endregion
  }
}
