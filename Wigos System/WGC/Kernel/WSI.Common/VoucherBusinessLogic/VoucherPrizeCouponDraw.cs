﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherPrizeCouponDraw.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  // 18. Lottery Summary
  // Format:
  //    Voucher Header
  //  
  //      Lottery info
  //
  //   Voucher Footer
  public class VoucherPrizeCouponDraw : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherPrizeCouponDraw(String[] VoucherAccountInfo,
                                  PrintMode Mode,
                              SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherPrizeCouponDraw");

      // 2. Load voucher resources.
      LoadVoucherResources(VoucherAccountInfo);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(String[] VoucherAccountInfo)
    {
      Currency _draw_prize;

      _draw_prize = 0;

      // - Title
      AddString("VOUCHER_PRIZE_COUPON_DRAW_TITLE", "Comprobante de sorteo"); //TODO Resource

      SetParameterValue("@VOUCHER_PRIZE_COUPON_DRAW_VOUCHER_ID", "Recibo de venta: 75338<br/>Número sorteo: 10920<br/>Fecha sorteo: 08/11/2012 18:21:47<br/><br/>");

      // - Selection numbers
      SetParameterValue("@VOUCHER_PRIZE_COUPON_DRAW_SELECTION_NUMBERS", "<center><B><font size=\"5\">04 07 14 19</font></B></center><br/>"); //TODO

      // - Drawn numbers
      SetParameterValue("@VOUCHER_PRIZE_COUPON_DRAW_DRAWN_NUMBERS", "Extracto<br/><center><B>01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20</B></center><br/>"); //TODO

      // - Prize //TODO: Parametro con el premio
      if (_draw_prize > 0)
      {
        SetParameterValue("@VOUCHER_PRIZE_COUPON_DRAW_TOTAL", "Premio: " + _draw_prize);  //TODO Resource
      }
      else
      {
        SetParameterValue("@VOUCHER_PRIZE_COUPON_DRAW_TOTAL", "Sin premio"); //TODO Resource
      }

      // - AcccountInfo
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);
    }

    #endregion

    #region Public Methods

    #endregion
  }
}
