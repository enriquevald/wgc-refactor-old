﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCashDeskCloseUnbalanced.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherCashDeskCloseUnbalanced : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCashDeskCloseUnbalanced(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats
                              , PrintMode Mode
                              , SqlTransaction SQLTransaction)
      : base(Mode, CashierVoucherType.CashDeskCloseUnbalanced, SQLTransaction)
    {
      InitializeVoucher(CashierSessionStats);

    }

    private void InitializeVoucher(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
    {
      String _footer;
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.


      LoadVoucher("VoucherCashDeskCloseUnbalanced");

      _footer = GeneralParam.GetString("Cashier.Voucher", "CashClosing.Unbalanced.Footer");

      if (!String.IsNullOrEmpty(_footer))
      {
        LoadFooter(_footer, null);
      }

      // 2. Load voucher resources.
      LoadVoucherResources(CashierSessionStats);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
    {
      String value = "";
      VoucherCashDeskCloseUnbalancedBody _voucher_body;

      // - Title
      SetParameterValue("@STR_VOUCHER_CASH_DESK_CLOSE_TITLE", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TITLE"));

      value = Resource.String("STR_VOUCHER_CASH_DESK_DIFFERENCE_TITLE");
      SetParameterValue("@STR_VOUCHER_CASH_DESK_DIFFERENCE_TITLE", value);

      _voucher_body = new VoucherCashDeskCloseUnbalancedBody(CashierSessionStats);
      SetParameterValue("@VOUCHER_CASH_DESK_DIFFERENCE_BODY", _voucher_body.VoucherHTML);

    } // LoadVoucherResources

    #endregion

    #region Public Methods

    #endregion

  }
}
