﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherUndoOperation.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: David Hernández
// 
// CREATION DATE: 05-MAY-2017
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 05-MAY-2017  DHA         First release.
//---------------------------------------------------------------------------------------------

using System;
using System.Data.SqlClient;

namespace WSI.Common
{
  public class VoucherReprint : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    /// <summary>
    /// Undo voucher operation
    /// </summary>
    /// <param name="VoucherHtml"></param>
    /// <param name="OperationCodeToUndo"></param>
    /// <param name="OperationCode"></param>
    /// <param name="Mode"></param>
    /// <param name="Trx"></param>
    public VoucherReprint(String VoucherHtml
                              , OperationCode OperationCode
                              , PrintMode Mode
                              , SqlTransaction Trx)
      : base(Mode, OperationCode, Trx)
    {
      String _str_is_copy;
      String _reprint_text;

      _str_is_copy = Misc.ReadGeneralParams("Cashier.Voucher", "ReprintText");

      if (!String.IsNullOrEmpty(_str_is_copy))
      {
        if (VoucherHtml.Contains(Voucher.VOUCHER_DIV_REPRINT))
        {
          _reprint_text = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n";
          _reprint_text += "<tr><td align=\"center\"><br/><b>" + System.Web.HttpUtility.HtmlEncode(_str_is_copy) + "</b><br/><br/></td></tr>\n";
          _reprint_text += "</table>";

          VoucherHtml = VoucherHtml.Replace(Voucher.VOUCHER_DIV_REPRINT, _reprint_text);
        }
        else
        {
          VoucherHtml += "<tr><td align='center'><br/><b>" + _str_is_copy + "</b></td></tr>\n";
        }
      }

      this.VoucherHTML = VoucherHtml;
    }



    #endregion
  }
}
