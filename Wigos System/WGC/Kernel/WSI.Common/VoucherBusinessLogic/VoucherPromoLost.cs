﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherPromoLost.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
// 22-MAR-2017  JMM         PBI 25958:Third TAX - Cash Desk draw voucher
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;


namespace WSI.Common
{
  public class VoucherPromoLost : Voucher
  {
    #region Constructor

    public VoucherPromoLost(String[] VoucherAccountInfo
                          , Int64 AccountId
                          , TYPE_SPLITS SplitInformation
                          , PrintMode Mode
                          , DataTable Promotions
                          , Boolean IsReward
                          , Boolean SingleTicket
                          , OperationCode OperationCode
                          , String PromotionalTitoTicket
                          , SqlTransaction SQLTransaction)
      : base(Mode, OperationCode, SQLTransaction)
    {
      MultiPromos.PromoBalance PromotionBalance;
      DataRow[] _promos;
      String _detail;
      String _title_voucher;
      Boolean _add_details;

      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load general voucher data.
      //  3. Load specific voucher data.
      //  4. Detail of promotion

      // 1. Load HTML structure.
      LoadVoucher("Voucher.Promo.Lost");

      SetValue("CV_ACCOUNT_ID", AccountId);

      // DHA 17-DEC-20104: added sequence to promo lost voucher
      VoucherSequenceId = SequenceId.VouchersSplitA;

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Load specific voucher data.     
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      if (!String.IsNullOrEmpty(PromotionalTitoTicket) && SingleTicket)
      {
        AddString("PROMOTIONAL_TITO_TICKET", Resource.String("STR_VOUCHER_PROMO_PROMOTIONAL_TITO_TICKET") + ": " + PromotionalTitoTicket);
      }
      else
      {
        AddString("PROMOTIONAL_TITO_TICKET", "");
      }

      // 4. Specific data

      // Title
      // DHA 29-OCT-2013: set the promotion voucher title from general params
      _title_voucher = IsReward ? GeneralParam.GetString("Cashier.Voucher", "VoucherOnPromotionAwarded.Title") : GeneralParam.GetString("Cashier.Voucher", "VoucherOnPromotionCancelled.Title");

      if (String.IsNullOrEmpty(_title_voucher))
      {
        _title_voucher = IsReward ? Resource.String("STR_VOUCHER_PROMO_REWARD_TITLE") : Resource.String("STR_VOUCHER_PROMO_CANCEL_TITLE");
      }

      AddString("PROMO_ACTION", _title_voucher);

      // Distributed among different parameters
      SetParameterValue("@VOUCHER_PROMO_DETAILS",
      "@VOUCHER_PROMO_NR_STRING" +
      "\n@VOUCHER_PROMO_NR_DETAIL" +
      "\n@VOUCHER_PROMO_NR_TOTAL" +
      "\n@VOUCHER_PROMO_UNR_STRING" +
      "\n@VOUCHER_PROMO_UNR_DETAIL" +
      "\n@VOUCHER_PROMO_UNR_TOTAL" +
      "\n@VOUCHER_PROMO_RE_STRING" +
      "\n@VOUCHER_PROMO_RE_DETAIL" +
      "\n@VOUCHER_PROMO_RE_TOTAL" +
      "\n@VOUCHER_PROMO_URE_STRING" +
      "\n@VOUCHER_PROMO_URE_DETAIL" +
      "\n@VOUCHER_PROMO_URE_TOTAL" +
      "\n@VOUCHER_PROMO_PT_STRING" +
      "\n@VOUCHER_PROMO_PT_DETAIL" +
      "\n@VOUCHER_PROMO_PT_TOTAL");

      // Details
      // * VOUCHER_PROMO_NR_DETAIL
      // * VOUCHER_PROMO_RE_DETAIL
      // * VOUCHER_PROMO_PT_DETAIL
      _detail = "";
      PromotionBalance = MultiPromos.PromoBalance.Zero;

      // RCI 05-SEP-2012: Only print promotions with balance > 0. This can happen with Point Promotions.
      _promos = Promotions.Select("ACP_BALANCE > 0", "ACP_CREDIT_TYPE");


      Currency _balance;
      //     Currency _balance_promo_ure;
      Object _footer;
      Currency _promo_unr;
      Currency _promo_ure;

      _promo_unr = 0;
      _promo_ure = 0;

      foreach (DataRow _promo in _promos)
      {
        _balance = (Decimal)_promo["ACP_BALANCE"];
        //_balance_promo_ure = (_promo["ACP_PRIZE_GROSS"] == DBNull.Value ? 0.0M : (Decimal)_promo["ACP_PRIZE_GROSS"]);
        _detail = NewRow("@PROMO_NAME", "@BALANCE", true);
        _detail += "@VOUCHER_PROMO_TAX"; // Add tax if necessary
        _detail += "@VOUCHER_PROMO_STATE_TAX";
        _detail += "@VOUCHER_PROMO_FEDERAL_TAX";

        _footer = _promo["ACP_TICKET_FOOTER"];

        if ((_footer != DBNull.Value) && _footer.ToString().Trim() != "")
        {
          _detail += NewFooterRow(); // @PROMO_FOOTER 
        }


        switch ((ACCOUNT_PROMO_CREDIT_TYPE)_promo["ACP_CREDIT_TYPE"])
        {
          case ACCOUNT_PROMO_CREDIT_TYPE.NR1:
          case ACCOUNT_PROMO_CREDIT_TYPE.NR2:

            if (_promo.Table.Columns.Contains("ACP_PROMO_TYPE") && _promo.Table.Columns.Contains("ACP_DETAILS"))
            {
              if (_promo["ACP_PROMO_TYPE"] != DBNull.Value
                  && _promo["ACP_DETAILS"] != DBNull.Value)
              {
                Promotion.PROMOTION_TYPE _type = ((Promotion.PROMOTION_TYPE)_promo["ACP_PROMO_TYPE"]);

                _add_details = false;

                switch (_type)
                {
                  case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM:
                  case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_01:
                  case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_02:
                  case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_03:
                  case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_04:
                  case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_05:
                  case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_06:
                  case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_07:
                  case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_08:
                  case Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM_RESTRICTED_09:
                    if (_promo["ACP_DETAILS"].ToString().Trim() != "")
                    {
                      _add_details = true;
                    }
                    break;
                }

                if (_add_details)
                {
                  _detail += NewPromoDetailRow(_promo["ACP_DETAILS"].ToString().Trim());
                }
              }
            }

            // Taxes from account
            if (_promo.Table.Columns.Contains("ACP_PRIZE_TYPE") &&
                _promo["ACP_PRIZE_TYPE"] != DBNull.Value &&
                ((AccountPromotion.PrizeType)_promo["ACP_PRIZE_TYPE"] == AccountPromotion.PrizeType.InKind1_TaxesFromAccount ||
                (AccountPromotion.PrizeType)_promo["ACP_PRIZE_TYPE"] == AccountPromotion.PrizeType.InKind2_TaxesFromSite))
            {
              _promo_unr += _balance;

              _detail += "@VOUCHER_PROMO_UNR_DETAIL"; // Continue details if necessary
              SetParameterValue("@VOUCHER_PROMO_UNR_DETAIL", _detail);

              // Set gross amount 
              SetParameterValue("@BALANCE", _balance.ToString(false) + " " + Misc.ReadGeneralParams("CashDesk.Draw", "Voucher.UNRLabel"));
              AddPromotionTaxes(_promo);
            }
            else
            {
              PromotionBalance.Balance.PromoNotRedeemable += _balance;

              _detail += "@VOUCHER_PROMO_NR_DETAIL"; // Continue details if necessary
              SetParameterValue("@VOUCHER_PROMO_NR_DETAIL", _detail);

              AddCurrency("BALANCE", _balance);
            }

            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE:
            PromotionBalance.Balance.Redeemable += _balance;

            if (_promo.Table.Columns.Contains("ACP_PROMO_TYPE") && _promo.Table.Columns.Contains("ACP_DETAILS"))
            {
              if (_promo["ACP_PROMO_TYPE"] != DBNull.Value
                  && _promo["ACP_DETAILS"] != DBNull.Value)
              {
                if (((Promotion.PROMOTION_TYPE)_promo["ACP_PROMO_TYPE"]) == Promotion.PROMOTION_TYPE.EXTERNAL_REDEEM && _promo["ACP_DETAILS"].ToString().Trim() != "")
                {
                  _detail += NewPromoDetailRow(_promo["ACP_DETAILS"].ToString().Trim());
                }
              }
            }

            _detail += "@VOUCHER_PROMO_RE_DETAIL"; // Continue details if necessary
            SetParameterValue("@VOUCHER_PROMO_RE_DETAIL", _detail);
            //
            AddCurrency("BALANCE", (Decimal)_promo["ACP_BALANCE"]);

            //AVZ 23-FEB-2016 - Add state and federal taxes to redeemable promotion
            //AddRedeemablePromotionTaxes(_promo, PromotionBalance.Balance);

            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE_IN_KIND:
            //PromotionBalance.Balance.Redeemable += _balance;

            if (_promo.Table.Columns.Contains("ACP_PROMO_TYPE") && _promo.Table.Columns.Contains("ACP_DETAILS"))
            {
              if (_promo["ACP_PROMO_TYPE"] != DBNull.Value
                  && _promo["ACP_DETAILS"] != DBNull.Value)
              {
                Promotion.PROMOTION_TYPE _type = ((Promotion.PROMOTION_TYPE)_promo["ACP_PROMO_TYPE"]);
                _detail += NewPromoDetailRow(_promo["ACP_DETAILS"].ToString().Trim());

              }
            }

            // Taxes from account
            if (_promo.Table.Columns.Contains("ACP_PRIZE_TYPE") &&
                _promo["ACP_PRIZE_TYPE"] != DBNull.Value &&
                ((AccountPromotion.PrizeType)_promo["ACP_PRIZE_TYPE"] == AccountPromotion.PrizeType.InKind1_TaxesFromAccount ||
                (AccountPromotion.PrizeType)_promo["ACP_PRIZE_TYPE"] == AccountPromotion.PrizeType.InKind2_TaxesFromSite))
            {
              _promo_ure += _balance;

              _detail += "@VOUCHER_PROMO_URE_DETAIL"; // Continue details if necessary
              SetParameterValue("@VOUCHER_PROMO_URE_DETAIL", _detail);

              //
              //SetParameterValue("@BALANCE", _balance_promo_ure.ToString(false) + " " + Misc.ReadGeneralParams("CashDesk.Draw", "Voucher.UNRLabel"));
              SetParameterValue("@BALANCE", _balance.ToString(false) + " " + Misc.ReadGeneralParams("CashDesk.Draw", "Voucher.UNRLabel"));
              AddPromotionTaxes(_promo);
            }
            else
            {
              PromotionBalance.Balance.PromoNotRedeemable += _balance;

              _detail += "@VOUCHER_PROMO_URE_DETAIL"; // Continue details if necessary
              SetParameterValue("@VOUCHER_PROMO_URE_DETAIL", _detail);

              AddCurrency("BALANCE", _balance);
            }
            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.POINT:
            PromotionBalance.Points += _balance;
            _detail += "@VOUCHER_PROMO_PT_DETAIL"; // Continue details if necessary
            SetParameterValue("@VOUCHER_PROMO_PT_DETAIL", _detail);
            //
            AddPoint("BALANCE", (Decimal)_promo["ACP_BALANCE"]);
            break;

          case ACCOUNT_PROMO_CREDIT_TYPE.UNKNOWN:
          default:
            break;
        }

        AddString("PROMO_NAME", (String)_promo["ACP_PROMO_NAME"]);
        if ((_footer != DBNull.Value) && _footer.ToString().Trim() != "")
        {
          AddMultipleLineString("PROMO_FOOTER", _footer.ToString());
        }

        SetParameterValue("@VOUCHER_PROMO_TAX", "");
        SetParameterValue("@VOUCHER_PROMO_STATE_TAX", "");
        SetParameterValue("@VOUCHER_PROMO_FEDERAL_TAX", "");

      }

      // Clear parameters
      SetParameterValue("@VOUCHER_PROMO_NR_DETAIL", "");
      SetParameterValue("@VOUCHER_PROMO_UNR_DETAIL", "");
      SetParameterValue("@VOUCHER_PROMO_RE_DETAIL", "");
      SetParameterValue("@VOUCHER_PROMO_URE_DETAIL", "");
      SetParameterValue("@VOUCHER_PROMO_PT_DETAIL", "");
      SetParameterValue("@VOUCHER_PROMO_TAX", "");
      SetParameterValue("@VOUCHER_PROMO_STATE_TAX", "");
      SetParameterValue("@VOUCHER_PROMO_FEDERAL_TAX", "");

      // Totals
      // VOUCHER_PROMO_NR_STRING
      // VOUCHER_PROMO_NR_TOTAL

      // VOUCHER_PROMO_RE_STRING
      // VOUCHER_PROMO_RE_TOTAL

      // VOUCHER_PROMO_PT_STRING
      // VOUCHER_PROMO_PT_TOTAL

      // No redeemable promotion.
      if (PromotionBalance.Balance.TotalNotRedeemable > 0)
      {
        // Name of promotion type
        _detail = NewRow("@NAME_STR", "", false);
        SetParameterValue("@VOUCHER_PROMO_NR_STRING", _detail);
        AddString("NAME_STR", SingleTicket ? Resource.String("STR_VOUCHER_NR_PROMO_LOST") : "");

        // Total
        _detail = LineTotal();
        _detail += NewRow("@TOTAL_STR", "@BALANCE", true);
        _detail += NewRow("&nbsp;", "", false); // white
        SetParameterValue("@VOUCHER_PROMO_NR_TOTAL", _detail);
        AddString("TOTAL_STR", "Total");
        AddCurrency("BALANCE", PromotionBalance.Balance.TotalNotRedeemable);
      }
      else
      {
        SetParameterValue("@VOUCHER_PROMO_NR_TOTAL", "");
        SetParameterValue("@VOUCHER_PROMO_NR_STRING", "");
      }

      // UNR Promotions
      if (_promo_unr.SqlMoney > 0)
      {
        // Name of promotion type
        _detail = NewRow("@NAME_STR", "", false);
        SetParameterValue("@VOUCHER_PROMO_UNR_STRING", _detail);
        AddString("NAME_STR", SingleTicket ? Resource.String("STR_VOUCHER_UNR_PROMO_LOST") : "");

        // DHA 02-MAR-2016: removed total line for UNR
        // Total
        _detail = NewRow("&nbsp;", "", false); // white
        SetParameterValue("@VOUCHER_PROMO_UNR_TOTAL", _detail);

      }
      else
      {
        SetParameterValue("@VOUCHER_PROMO_UNR_TOTAL", "");
        SetParameterValue("@VOUCHER_PROMO_UNR_STRING", "");
      }

      // Redeemable Promotions.
      if (PromotionBalance.Balance.Redeemable > 0)
      {
        // Name of promotion type
        _detail = NewRow("@NAME_STR", "", false);
        SetParameterValue("@VOUCHER_PROMO_RE_STRING", _detail);
        AddString("NAME_STR", SingleTicket ? Resource.String("STR_VOUCHER_RE_PROMO_LOST") : "");

        // Total
        _detail = LineTotal();
        _detail += NewRow("@TOTAL_STR", "@BALANCE", true);
        _detail += NewRow("&nbsp;", "", false); // white
        SetParameterValue("@VOUCHER_PROMO_RE_TOTAL", _detail);
        AddString("TOTAL_STR", "Total");
        AddCurrency("BALANCE", PromotionBalance.Balance.Redeemable);
      }
      else
      {
        SetParameterValue("@VOUCHER_PROMO_RE_STRING", "");
        SetParameterValue("@VOUCHER_PROMO_RE_TOTAL", "");
      }

      // URE Promotions
      if (_promo_ure.SqlMoney > 0)
      {
        // Name of promotion type
        _detail = NewRow("@NAME_STR", "", false);
        SetParameterValue("@VOUCHER_PROMO_URE_STRING", _detail);
        AddString("NAME_STR", SingleTicket ? Resource.String("STR_VOUCHER_URE_PROMO_LOST") : "");

        // Total
        _detail = LineTotal();
        _detail += NewRow("@TOTAL_STR", "@BALANCE", true);
        _detail += NewRow("&nbsp;", "", false); // white
        SetParameterValue("@VOUCHER_PROMO_URE_TOTAL", _detail);
        AddString("TOTAL_STR", "Total");
        SetParameterValue("@BALANCE", _promo_ure.ToString(false) + " " + Misc.ReadGeneralParams("CashDesk.Draw", "Voucher.UNRLabel"));
      }
      else
      {
        SetParameterValue("@VOUCHER_PROMO_URE_TOTAL", "");
        SetParameterValue("@VOUCHER_PROMO_URE_STRING", "");
      }

      // Points Promotions.
      if (PromotionBalance.Points > 0)
      {
        // Name of promotion type
        _detail = NewRow("@NAME_STR", "", false);
        SetParameterValue("@VOUCHER_PROMO_PT_STRING", _detail);
        AddString("NAME_STR", SingleTicket ? Resource.String("STR_VOUCHER_PT_PROMO_LOST") : "");

        //Total
        _detail = LineTotal();
        _detail += NewRow("@TOTAL_STR", "@BALANCE", true);
        _detail += NewRow("&nbsp;", "", false); // white
        SetParameterValue("@VOUCHER_PROMO_PT_TOTAL", _detail);
        AddString("TOTAL_STR", "Total");
        AddPoint("BALANCE", PromotionBalance.Points);
      }
      else
      {
        SetParameterValue("@VOUCHER_PROMO_PT_TOTAL", "");
        SetParameterValue("@VOUCHER_PROMO_PT_STRING", "");
      }

      // Signature if is canceled promotions

      SetSignatures();

    }


    private String NewRow(String NameKey, String NameValue, Boolean IsPromo)
    {
      return
         "<tr>" +
   "<td align=\"left\" width=\"65%\" colspan=\"2\"> " + (IsPromo ? "" : "<b>") + NameKey + (IsPromo ? ":" : "</b>") + "</td>" +
   "<td align=\"right\" width=\"35%\">" + NameValue + "</td>" +
   "</tr>";
    }

    private String NewPromoDetailRow(String PromoDetail)
    {
      return "<tr><td colspan=\"2\" align=\"left\" width=\"100%\"><i>" + PromoDetail + "</i></td></tr>";
    }

    private String NewFooterRow()
    {
      return NewRow("&nbsp;", "", false) +
        "<tr><td colspan=\"2\" align=\"left\" width=\"100%\"><font size=\"1\"><i>@PROMO_FOOTER</i></font></td></tr>" +
        NewRow("&nbsp;", "", false);
    }

    private String LineTotal()
    {
      return "<tr><td colspan=\"3\"><hr noshade size=\"1\"></td></tr>";
    }

    private void SetSignatures()
    {
      SetParameterValue("@VOUCHER_PROMO_SIGNATURES",
      "<center><b>" + Resource.String("STR_VOUCHER_CLIENT_SIGN") + "</b></center>" +
      "<br />" +
      "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">" +
        "<tr><td>&nbsp;</td></tr>" +
        "<tr><td>&nbsp;</td></tr>" +
      "</table>" +
      "<hr noshade size=\"4\">");
    }

    private String NewRowTax(String NameKey, String NameValue, Boolean IsPromo, Boolean WithTab)
    {
      if (!WithTab)
      {
        NewRow(NameKey, NameValue, IsPromo);
      }
      return
         "<tr>" +
   "<td align=\"right\" width=\"5%\"></td>" +
   "<td align=\"left\" width=\"60%\">" + (IsPromo ? "" : "<b>") + NameKey + (IsPromo ? ":" : "</b>") + "</td>" +
   "<td align=\"right\" width=\"35%\">" + NameValue + "</td>" +
   "</tr>";
    }

    private void AddPromotionTaxes(DataRow Promo)
    {
      String _detail;
      Int32 _factor;

      _detail = String.Empty;
      _factor = 1;

      for (int _idx = 1; _idx <= 3; _idx++)
      {
        if (Promo["ACP_PRIZE_TAX" + _idx] != DBNull.Value)
        {
          if (GeneralParam.GetDecimal("Cashier", "Tax.OnPrizeInKind." + _idx + ".Pct", 0m) != 0)
          {
            _detail = NewRowTax("@PROMO_TAX", "@TAX_VALUE", true, true);
            _detail += "@VOUCHER_PROMO_TAX"; // Add tax if necessary

            SetParameterValue("@VOUCHER_PROMO_TAX", _detail);
            SetParameterValue("@PROMO_TAX", Misc.ReadGeneralParams("Cashier", "Tax.OnPrizeInKind." + _idx + ".Name"));

            // DHA 29-MAY-2015 added to set the taxes to negative value
            if (GeneralParam.GetBoolean("Cashier", "Tax.OnPrizeInKind.SetNegative", false))
            {
              _factor = -1;
            }

            AddCurrency("TAX_VALUE", (Decimal)Promo["ACP_PRIZE_TAX" + _idx] * _factor);
          }
        }
      }

      SetParameterValue("@VOUCHER_PROMO_TAX", "");
    }

    #endregion
  }
}
