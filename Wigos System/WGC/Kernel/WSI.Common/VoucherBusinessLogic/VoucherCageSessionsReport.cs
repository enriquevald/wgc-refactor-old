//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Cage.cs
// 
//   DESCRIPTION:
//
//        AUTHOR: Sergi Mart�nez
// 
// CREATION DATE: 08-AUG-2014
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 08-AUG-2014 SMN    Initial version.
// 23-SEP-2014 LMRS   Change NLS
// 02-OCT-2014 DRV    Fixed Bug WIG-1370: Decimal denominations are truncated
// 03-OCT-2014 SMN    Total values is incorrect.
// 05-OCT-2014 RRR    Fixed Bug WIG-1648: Error showing total cage count
// 17-NOV-2014 RRR    Fixed Bug WIG-1690: Error showing total cage count in casino chips
// 24-NOV-2014 AMF    Fixed Bug WIG-1719: Error showing type in stock
// 16-JUN-2015 YNM    Fixed Bug WIG-2444: Cage General Report: incorrect data when export to excel
// 31-AUG-2015 FAV    Fixed Bug TFS-4097: Use the "Cashier.PlayerCard-ConceptName" general param for the "Deposito de tarjeta" concept.
// 02-MAY-2016 JML    Product Backlog Item 10825:Mesas (Fase 1): Conceptos, tickets & fichas (cajero y b�veda)
// 31-OCT-2016 JML        Fixed Bug 19865: Gamming tables stock control - not displayed in the cashier
// 31-OCT-2016 JML        Fixed Bug 19866: Chips shange control - not displayed in the cashier
// -----------------------------------------------------------------------------
//
using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
namespace WSI.Common
{
  public class VoucherCageSessionsReport : Voucher
  {
    #region Class Attributes

    public class ParametersCageSessionsReport
    {
      public String CageSessionId;
      public Color BackColor;
      public GU_USER_TYPE UserType;

      public ParametersCageSessionsReport()
      {
        CageSessionId = String.Empty;
        UserType = GU_USER_TYPE.NOT_ASSIGNED;
      }
    }
    #endregion

    #region Members

    // Deposits / Withdraws
    Dictionary<CurrencyIsoType, Decimal> m_total_deposits;
    Dictionary<CurrencyIsoType, Decimal> m_total_withdraws;
    Dictionary<CurrencyIsoType, Decimal> m_total_result;

    // Summary
    Decimal m_cash_summary;
    Decimal m_checks_summary;
    Decimal m_bank_cards_summary;
    Dictionary<CurrencyIsoType, Decimal> m_chips_summary;
    Decimal m_coins_summary;
    Dictionary<CurrencyIsoType, Decimal> m_currencies_summary;

    #endregion

    #region Constructor

    public VoucherCageSessionsReport(WSI.Common.CageSessionReport.TYPE_CAGE_GLOBAL_SESSION_REPORT CageSessionReport
                                 , PrintMode Mode
                                 , ParametersCageSessionsReport ParametersOverview
                                 , SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {

      m_total_deposits = new Dictionary<CurrencyIsoType, Decimal>();
      m_total_withdraws = new Dictionary<CurrencyIsoType, Decimal>();
      m_total_result = new Dictionary<CurrencyIsoType, Decimal>();

      m_cash_summary = 0;
      m_checks_summary = 0;
      m_bank_cards_summary = 0;
      m_chips_summary = new Dictionary<CurrencyIsoType, decimal>();
      m_coins_summary = 0;
      m_currencies_summary = new Dictionary<CurrencyIsoType, Decimal>();

      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCageGlobalSessionsReport");

      // 2. Resources
      LoadVoucherResources(CageSessionReport, ParametersOverview);

      // 3. Load specific voucher data.
      LoadSpecificVoucherData(CageSessionReport, ParametersOverview);
    }

    #endregion

    #region Private Methods

    #region Public Class
    public class CageTransactionsKey
    {
      public Int64 Order;
      public String IsoCode;
      public CageCurrencyType CageCurrencyType;
      public String SourceTargetName;
      public String ConceptDescription;
      public Int64 ConceptId;
      public Int64 SourceTargetId;

      public CageTransactionsKey()
      {
        Order = 0;
        IsoCode = String.Empty;
        CageCurrencyType = Common.CageCurrencyType.Bill;
        SourceTargetName = String.Empty;
        ConceptDescription = String.Empty;
        ConceptId = 0;
        SourceTargetId = 10;
      }

      public override bool Equals(object obj)
      {
        return (SourceTargetName == ((CageTransactionsKey)obj).SourceTargetName)
            && (ConceptDescription == ((CageTransactionsKey)obj).ConceptDescription)
            && (IsoCode == ((CageTransactionsKey)obj).IsoCode)
            && (CageCurrencyType == ((CageTransactionsKey)obj).CageCurrencyType)
            && (ConceptId == ((CageTransactionsKey)obj).ConceptId)
            && (SourceTargetId == ((CageTransactionsKey)obj).SourceTargetId);
      }

      public override int GetHashCode()
      {
        if (SourceTargetName == null || ConceptDescription == null || IsoCode == null) return 0;
        return SourceTargetName.GetHashCode() ^ ConceptDescription.GetHashCode() ^ IsoCode.GetHashCode() ^ CageCurrencyType.GetHashCode() ^ ConceptId.GetHashCode() ^ SourceTargetId.GetHashCode();
      }

      public class SortComparer : IComparer<CageTransactionsKey>
      {
        public Int32 Compare(CageTransactionsKey x, CageTransactionsKey y)
        {
          if (x.Order == y.Order)
          {
            if (x.SourceTargetName == y.SourceTargetName)
            {
              if (x.ConceptDescription == y.ConceptDescription)
              {
                if (x.IsoCode == y.IsoCode)
                {
                  if (x.CageCurrencyType == y.CageCurrencyType)
                  {
                    if (x.SourceTargetId == y.SourceTargetId)
                    {
                      return x.ConceptId.CompareTo(y.ConceptId);
                    }
                    else
                    {
                      return x.SourceTargetId.CompareTo(y.SourceTargetId);
                    }
                  }
                  else
                  {
                    return x.CageCurrencyType.CompareTo(y.CageCurrencyType);
                  }
                }
                else
                {
                  return x.IsoCode.CompareTo(y.IsoCode);
                }
              }
              else
              {
                return x.ConceptDescription.CompareTo(y.ConceptDescription);
              }
            }
            else
            {
              return x.SourceTargetName.CompareTo(y.SourceTargetName);
            }
          }
          else
          {
            return x.Order.CompareTo(y.Order);
          }
        }
      }
    }

    public class CageSummaryKey
    {
      public Int32 Order;
      public String IsoCode;
      public CageCurrencyType CageCurrencyType;
      public String CurrencyTypeDescription;

      public CageSummaryKey()
      {
        Order = 0;
        IsoCode = String.Empty;
        CageCurrencyType = Common.CageCurrencyType.Bill;
        CurrencyTypeDescription = String.Empty;
      }

      public override bool Equals(object obj)
      {
        return (IsoCode == ((CageSummaryKey)obj).IsoCode)
            && (CageCurrencyType == ((CageSummaryKey)obj).CageCurrencyType)
            && (CurrencyTypeDescription == ((CageSummaryKey)obj).CurrencyTypeDescription);
      }

      public override int GetHashCode()
      {
        if (CurrencyTypeDescription == null || IsoCode == null) return 0;
        return CurrencyTypeDescription.GetHashCode() ^ IsoCode.GetHashCode() ^ CageCurrencyType.GetHashCode();
      }

      public class SortComparer : IComparer<CageSummaryKey>
      {
        public Int32 Compare(CageSummaryKey x, CageSummaryKey y)
        {
          if (x.Order == y.Order)
          {
            if (x.IsoCode == y.IsoCode)
            {
              if (x.CageCurrencyType == y.CageCurrencyType)
              {
                return x.CurrencyTypeDescription.CompareTo(y.CurrencyTypeDescription);
              }
              else
              {
                return x.CageCurrencyType.CompareTo(y.CageCurrencyType);
              }
            }
            else
            {
              return x.IsoCode.CompareTo(y.IsoCode);
            }
          }
          else
          {
            return x.Order.CompareTo(y.Order);
          }
        }
      }
    }

    public class CageTransactionsClass
    {
      private Decimal m_input;
      private Decimal m_output;
      private Decimal m_difference;

      public Decimal Inputs
      {
        get { return m_input; }
        set
        {
          m_input = value;
          m_difference = m_input - m_output;
        }
      }

      public Decimal Outputs
      {
        get { return m_output; }
        set
        {
          m_output = value;
          m_difference = m_input - m_output;
        }
      }

      public Decimal Difference
      {
        get { return m_difference; }
      }

      public CageTransactionsClass()
      {
        m_input = 0;
        m_output = 0;
        m_difference = 0;
      }
    }

    #endregion

    private void LoadSpecificVoucherData(CageSessionReport.TYPE_CAGE_GLOBAL_SESSION_REPORT CageSessionReport, ParametersCageSessionsReport ParametersOverview)
    {
      SetParameterValue("@VOUCHER_STOCK_ROWS", CageStockHtml(CageSessionReport, ParametersOverview));
      SetParameterValue("@VOUCHER_ORIGIN_DESTIN_ROWS", CageTransactions(CageSessionReport, ParametersOverview));
      SetParameterValue("@VOUCHER_CUSTOM_LIABILITIES", CageCustomLiabilitiesHtml(CageSessionReport));
      SetParameterValue("@VOUCHER_DEPOSITS_WITHDRAWS", CageDepositsWithdrawsHtml());
      SetParameterValue("@VOUCHER_CURRENCIES_SUMMARY", CageSummaryHtml());
      SetParameterValue("@VOUCHER_SESSION_SUMMARY", SessionSummaryHtml(CageSessionReport));
      SetParameterValue("@VOUCHER_OTHER_SYSTEM_CONCEPTS", OtherConceptsHtml(CageSessionReport));

    } // LoadSpecificVoucherData

    private String CageTransactions(CageSessionReport.TYPE_CAGE_GLOBAL_SESSION_REPORT CageSessionReport, ParametersCageSessionsReport ParametersOverview)
    {
      StringBuilder _sb_html;

      _sb_html = new StringBuilder();

      // Cashier
      _sb_html.AppendLine(CageTransactionsHtml(CageSessionReport, WSI.Common.CageSessionReport.CAGE_SOURCE_TARGETS.CASHIERS));

      // GamingTables
      if (GamingTableBusinessLogic.IsGamingTablesEnabled())
      {
        _sb_html.AppendLine(CageTransactionsHtml(CageSessionReport, WSI.Common.CageSessionReport.CAGE_SOURCE_TARGETS.GAMING_TABLES));
      }

      // Stackers
      _sb_html.AppendLine(CageTransactionsHtml(CageSessionReport, WSI.Common.CageSessionReport.CAGE_SOURCE_TARGETS.TERMINALS));

      // Custom
      _sb_html.AppendLine(CageTransactionsHtml(CageSessionReport, WSI.Common.CageSessionReport.CAGE_SOURCE_TARGETS.CUSTOM));

      return _sb_html.ToString();

    } // CageTransactions

    private String CageTransactionsHtml(CageSessionReport.TYPE_CAGE_GLOBAL_SESSION_REPORT CageSessionReport
                                        , WSI.Common.CageSessionReport.CAGE_SOURCE_TARGETS SourceTarget)
    {
      SortedDictionary<CageTransactionsKey, CageTransactionsClass> _cage_transaction;
      StringBuilder _sb_html = new StringBuilder();
      String _currency;
      CageCurrencyType _cage_currency_type;
      String _where;
      String _source_target_name;
      String _concept_description;
      Int64 _concept_id;
      Int64 _source_target_id;
      String _national_currency;
      Int64 _order;

      _cage_transaction = new SortedDictionary<CageTransactionsKey, CageTransactionsClass>(new CageTransactionsKey.SortComparer());

      _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");

      DataRow[] _drs;

      if (SourceTarget != WSI.Common.CageSessionReport.CAGE_SOURCE_TARGETS.CUSTOM)
      {
        _where = String.Format("CM_SOURCE_TARGET_ID IN({0}) AND (CM_VALUE_IN <> 0 OR CM_VALUE_OUT <> 0 OR CM_CONCEPT_ID = 0)", (Int32)SourceTarget);
        _drs = CageSessionReport.CageDtTransactions.Select(_where, "ORDEN");
      }
      else
      {
        _where = String.Format("CM_SOURCE_TARGET_ID NOT IN({0}, {1}, {2}) AND (CM_VALUE_IN <> 0 OR CM_VALUE_OUT <> 0 OR CM_CONCEPT_ID = 0)"
                              , (Int32)WSI.Common.CageSessionReport.CAGE_SOURCE_TARGETS.CASHIERS
                              , (Int32)WSI.Common.CageSessionReport.CAGE_SOURCE_TARGETS.GAMING_TABLES
                              , (Int32)WSI.Common.CageSessionReport.CAGE_SOURCE_TARGETS.TERMINALS);
        _drs = CageSessionReport.CageDtTransactions.Select(_where, "ORDEN");
      }

      _order = 0;

      foreach (DataRow _dr in _drs)
      {
        _currency = (String)_dr["CM_ISO_CODE"];
        _cage_currency_type = (CageCurrencyType)_dr["CAGE_CURRENCY_TYPE"];
        _source_target_name = _dr.IsNull("CST_SOURCE_TARGET_NAME") ? String.Empty : (String)_dr["CST_SOURCE_TARGET_NAME"];
        _concept_id = (Int64)_dr["CM_CONCEPT_ID"];
        _source_target_id = (Int64)_dr["CM_SOURCE_TARGET_ID"];

        if (_concept_id != 0)
        {
          _concept_description = (String)_dr["CC_DESCRIPTION"];
        }
        else
        {
          _concept_description = String.Empty;
        }

        CageTransactionsKey _key = new CageTransactionsKey();
        _key.Order = _order;
        _key.IsoCode = _currency;
        _key.CageCurrencyType = _cage_currency_type;
        _key.SourceTargetName = _source_target_name;
        _key.ConceptDescription = _concept_description;
        _key.ConceptId = _concept_id;
        _key.SourceTargetId = _source_target_id;

        if (!_cage_transaction.ContainsKey(_key))
        {
          _cage_transaction.Add(_key, new CageTransactionsClass());
        }

        _cage_transaction[_key].Outputs = (Decimal)_dr["CM_VALUE_OUT"];
        _cage_transaction[_key].Inputs = (Decimal)_dr["CM_VALUE_IN"];
        _order += 1;
      }

      _sb_html.AppendLine(GetHtmlTransactionsFromDictionary(_cage_transaction));

      return _sb_html.ToString();
    } // GetHtmlCageTransaction


    private String GetHtmlTransactionsFromDictionary(SortedDictionary<CageTransactionsKey, CageTransactionsClass> CageTransaction)
    {
      StringBuilder _sb_html = new StringBuilder();

      Int64 _current_source_target_id;
      Int64 _current_concept_id;
      Int64 _old_source_target_id;
      Int64 _old_concept_id;
      String _currency;
      CurrencyIsoType _currency_iso_type;

      Boolean _show_zeros;

      Boolean _is_same_source_target = false;
      Boolean _is_same_concept = false;

      String _national_currency;

      _current_source_target_id = -1;
      _current_concept_id = -1;
      _old_source_target_id = -1;
      _old_concept_id = -1;

      _show_zeros = GeneralParam.GetBoolean("Cage", "Meters.Report.ShowItemsWithZeroAmount", true);
      _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");

      _sb_html.AppendLine("<tr><td style=\"width:18%\">&nbsp;</td><td>&nbsp;</td><td style=\"width:18%\">&nbsp;</td><td style=\"width:18%\">&nbsp;</td><td style=\"width:18%\">&nbsp;</td></tr>");


      foreach (KeyValuePair<CageTransactionsKey, CageTransactionsClass> _cage_trans in CageTransaction)
      {
        _is_same_source_target = false;
        _is_same_concept = false;
        _current_source_target_id = _cage_trans.Key.SourceTargetId;
        _current_concept_id = _cage_trans.Key.ConceptId;
        _currency_iso_type = new CurrencyIsoType();

        switch (_cage_trans.Key.SourceTargetId) 
        {
            case (Int32)CageMeters.CageSystemSourceTarget.Cashiers: 
                 _cage_trans.Key.SourceTargetName = Resource.String("STR_VOUCHER_CASHIERS");   //"Cajas"
            break;
             case (Int32)CageMeters.CageSystemSourceTarget.GamingTables: 
                 _cage_trans.Key.SourceTargetName = Resource.String("STR_VOUCHER_GAMING_TABLES");   //"Mesas"
            break;   
            case (Int32)CageMeters.CageSystemSourceTarget.Terminals: 
                 _cage_trans.Key.SourceTargetName = Resource.String("STR_VOUCHER_STACKERS");   //"Terminales"
            break;
        }

          _currency = _cage_trans.Key.IsoCode;
        _currency_iso_type.IsoCode = _cage_trans.Key.IsoCode;
        _currency_iso_type.Type = FeatureChips.ConvertToCurrencyExchangeType(_cage_trans.Key.CageCurrencyType);

        if (FeatureChips.IsCurrencyExchangeTypeChips(_currency_iso_type.Type))
        {
          _currency = FeatureChips.GetChipTypeDescription(_currency_iso_type);
        }

        if (_current_concept_id == 0)
        {
          this.UpdateDepositsWithdrwas(_currency_iso_type, _cage_trans.Value.Inputs, _cage_trans.Value.Outputs);
        }

        if (_current_source_target_id == _old_source_target_id)
        {
          _is_same_source_target = true;
        }
        else
        {
          _is_same_source_target = false;
        }

        if (_current_concept_id == _old_concept_id)
        {
          _is_same_concept = true;
        }
        else
        {
          _is_same_concept = false;
        }

        if (!_is_same_source_target)
        {
          if ((_show_zeros) || (_currency == _national_currency) || (_cage_trans.Value.Inputs > 0 || _cage_trans.Value.Outputs > 0))
          {
            _sb_html.AppendLine("<tr><td colspan=\"5\">&nbsp;</td></tr>");
            _sb_html.AppendLine(String.Format("<tr><td><strong>{0}</strong></td>", _cage_trans.Key.SourceTargetName));
            _sb_html.AppendLine(String.Format("<td style=\"text-align:left\"><strong>{0}</strong></td>", _currency));
            if (_currency_iso_type.Type != CurrencyExchangeType.CASINO_CHIP_COLOR)
            {
              _sb_html.AppendLine(String.Format("<td style=\"text-align:right\"><strong>{0}</strong></td>", ((Currency)_cage_trans.Value.Inputs).ToString(false)));
              _sb_html.AppendLine(String.Format("<td style=\"text-align:right\"><strong>{0}</strong></td>", ((Currency)_cage_trans.Value.Outputs).ToString(false)));
              _sb_html.AppendLine(String.Format("<td style=\"text-align:right\"><strong>{0}</strong></td>", ((Currency)(_cage_trans.Value.Inputs - _cage_trans.Value.Outputs)).ToString(false)));
            }
            else
            {
              _sb_html.AppendLine(String.Format("<td style=\"text-align:right\"><strong>{0}</strong></td>", Convert.ToInt32(_cage_trans.Value.Inputs).ToString()));
              _sb_html.AppendLine(String.Format("<td style=\"text-align:right\"><strong>{0}</strong></td>", Convert.ToInt32(_cage_trans.Value.Outputs).ToString()));
              _sb_html.AppendLine(String.Format("<td style=\"text-align:right\"><strong>{0}</strong></td>", Convert.ToInt32(_cage_trans.Value.Inputs - _cage_trans.Value.Outputs).ToString()));
            }
            _sb_html.AppendLine(String.Format("</tr>", "&nbsp;"));
          }
        }

        else if (!_is_same_concept)
        {
          if ((_show_zeros) || (_cage_trans.Value.Inputs > 0 || _cage_trans.Value.Outputs > 0))
          {
            _sb_html.AppendLine(String.Format("<tr><td>{0}</td>", _cage_trans.Key.ConceptDescription));
            _sb_html.AppendLine(String.Format("<td style=\"text-align:left\">{0}</td>", _currency));
            if (_currency_iso_type.Type != CurrencyExchangeType.CASINO_CHIP_COLOR)
            {
              _sb_html.AppendLine(String.Format("<td style=\"text-align:right\">{0}</td>", ((Currency)_cage_trans.Value.Inputs).ToString(false)));
              _sb_html.AppendLine(String.Format("<td style=\"text-align:right\">{0}</td>", ((Currency)_cage_trans.Value.Outputs).ToString(false)));
              _sb_html.AppendLine(String.Format("<td style=\"text-align:right\">{0}</td>", ((Currency)(_cage_trans.Value.Inputs - _cage_trans.Value.Outputs)).ToString(false)));
            }
            else
            {
              _sb_html.AppendLine(String.Format("<td style=\"text-align:right\">{0}</td>", Convert.ToInt32(_cage_trans.Value.Inputs).ToString()));
              _sb_html.AppendLine(String.Format("<td style=\"text-align:right\">{0}</td>", Convert.ToInt32(_cage_trans.Value.Outputs).ToString()));
              _sb_html.AppendLine(String.Format("<td style=\"text-align:right\">{0}</td>", Convert.ToInt32(_cage_trans.Value.Inputs - _cage_trans.Value.Outputs).ToString()));
            }
            _sb_html.AppendLine(String.Format("</tr>", "&nbsp;"));
          }
          else
          {
            _current_concept_id = _old_concept_id;
          }
        }

        else
        {
          if (_current_concept_id == 0)
          {
            if ((_show_zeros) || (_currency == _national_currency) || (_cage_trans.Value.Inputs > 0 || _cage_trans.Value.Outputs > 0))
            {
              _sb_html.AppendLine(String.Format("<tr><td><strong>{0}</strong></td>", "&nbsp;"));
              _sb_html.AppendLine(String.Format("<td style=\"text-align:left\"><strong>{0}</strong></td>", _currency));
              if (_currency_iso_type.Type != CurrencyExchangeType.CASINO_CHIP_COLOR)
              {
                _sb_html.AppendLine(String.Format("<td style=\"text-align:right\"><strong>{0}</strong></td>", ((Currency)_cage_trans.Value.Inputs).ToString(false)));
                _sb_html.AppendLine(String.Format("<td style=\"text-align:right\"><strong>{0}</strong></td>", ((Currency)_cage_trans.Value.Outputs).ToString(false)));
                _sb_html.AppendLine(String.Format("<td style=\"text-align:right\"><strong>{0}</strong></td>", ((Currency)(_cage_trans.Value.Inputs - _cage_trans.Value.Outputs)).ToString(false)));
              }
              else
              {
                _sb_html.AppendLine(String.Format("<td style=\"text-align:right\"><strong>{0}</strong></td>", Convert.ToInt32(_cage_trans.Value.Inputs).ToString()));
                _sb_html.AppendLine(String.Format("<td style=\"text-align:right\"><strong>{0}</strong></td>", Convert.ToInt32(_cage_trans.Value.Outputs).ToString()));
                _sb_html.AppendLine(String.Format("<td style=\"text-align:right\"><strong>{0}</strong></td>", Convert.ToInt32(_cage_trans.Value.Inputs - _cage_trans.Value.Outputs).ToString()));
              }
              _sb_html.AppendLine(String.Format("</tr>", "&nbsp;"));
            }
          }
          else
          {
            if ((_show_zeros) || (_cage_trans.Value.Inputs > 0 || _cage_trans.Value.Outputs > 0))
            {
              _sb_html.AppendLine(String.Format("<tr><td>{0}</td>", "&nbsp;"));
              _sb_html.AppendLine(String.Format("<td style=\"text-align:left\">{0}</td>", _currency));
              if (_currency_iso_type.Type != CurrencyExchangeType.CASINO_CHIP_COLOR)
              {
                _sb_html.AppendLine(String.Format("<td style=\"text-align:right\">{0}</td>", ((Currency)_cage_trans.Value.Inputs).ToString(false)));
                _sb_html.AppendLine(String.Format("<td style=\"text-align:right\">{0}</td>", ((Currency)_cage_trans.Value.Outputs).ToString(false)));
                _sb_html.AppendLine(String.Format("<td style=\"text-align:right\">{0}</td>", ((Currency)(_cage_trans.Value.Inputs - _cage_trans.Value.Outputs)).ToString(false)));
              }
              else
              {
                _sb_html.AppendLine(String.Format("<td style=\"text-align:right\">{0}</td>", Convert.ToInt32(_cage_trans.Value.Inputs).ToString()));
                _sb_html.AppendLine(String.Format("<td style=\"text-align:right\">{0}</td>", Convert.ToInt32(_cage_trans.Value.Outputs).ToString()));
                _sb_html.AppendLine(String.Format("<td style=\"text-align:right\">{0}</td>", Convert.ToInt32(_cage_trans.Value.Inputs - _cage_trans.Value.Outputs).ToString()));
              }
                _sb_html.AppendLine(String.Format("</tr>", "&nbsp;"));
            }
            else
            {
              _current_concept_id = _old_concept_id;
            }
          }
        }

        _old_source_target_id = _current_source_target_id;
        _old_concept_id = _current_concept_id;

      }

      return _sb_html.ToString();

    }

    private string SessionSummaryHtml(CageSessionReport.TYPE_CAGE_GLOBAL_SESSION_REPORT CageSessionReport)
    {
      StringBuilder _sb_html;

      String _current_currency;
      CurrencyIsoType _currency_iso_type;
      String _previous_currency_iso;
      String _previous_currency_desc;
      CurrencyExchangeType _previous_currency_type;

      String _payment_method;
      Decimal _cage_count_amount;
      Decimal _cage_count_total_amount;

      Decimal _total_currency;
      Decimal _total_payment_method;
      Decimal _deposit;
      Decimal _withdraw;

      String _total_currency_string;
      String _cage_count_total_amount_string;

      _sb_html = new StringBuilder();

      _previous_currency_desc = String.Empty;
      _previous_currency_type = CurrencyExchangeType.CURRENCY;
      _current_currency = String.Empty;
      _previous_currency_iso = String.Empty;
      _total_currency = 0;
      _cage_count_amount = 0;
      _cage_count_total_amount = 0;


      foreach (DataRow _dr in CageSessionReport.CageDtSessionSummary.Rows)
      {
        _currency_iso_type = new CurrencyIsoType();
        _currency_iso_type.IsoCode = _dr["ISO_CODE"].ToString();
        _currency_iso_type.Type = FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_dr["CAGE_CURRENCY_TYPE"]);
        _current_currency = _currency_iso_type.IsoCode;
        if (FeatureChips.IsCurrencyExchangeTypeChips(_currency_iso_type.Type))
        {
          _current_currency = FeatureChips.GetChipTypeDescription(_currency_iso_type);
        }

        _deposit = (Decimal)_dr["DEPOSITS"];
        _withdraw = (Decimal)_dr["WITHDRAWALS"];

        // Set label values
        switch ((Int32)_dr["ORIGIN"])
        {
          case -1:
            _payment_method = "&nbsp;&nbsp;&nbsp;" + Resource.String("STR_VOUCHER_CAGESESSIONREPORT_BANC_CARD"); //Tarjetas Bancarias";
            break;
          case -2:
            _payment_method = "&nbsp;&nbsp;&nbsp;" + Resource.String("STR_VOUCHER_CAGESESSIONREPORT_CHECK"); //Cheques";
            break;
          case -100:
            _payment_method = "&nbsp;&nbsp;&nbsp;" + Resource.String("STR_CAGE_COINS"); //Monedas";
            break;
          case -200:
            _payment_method = "&nbsp;&nbsp;&nbsp;" + Resource.String("STR_FRM_TITO_HANDPAYS_TICKETS"); //Tickets";
            break;
          default:
            if (FeatureChips.IsCurrencyExchangeTypeChips(_currency_iso_type.Type))
            {
              _payment_method = "&nbsp;&nbsp;&nbsp;" + Resource.String("STR_CAGE_CASINO_CHIPS");  //Fichas
            }
            else
            {
            _payment_method = "&nbsp;&nbsp;&nbsp;" + Resource.String("STR_REFUND_CASH"); //Efectivo";
            }
            break;
        }

        // Cage Count
        _cage_count_amount = 0;
        _cage_count_total_amount = 0;

        if (CageSessionReport.CageDtCounts.Select("ORIGIN = " + (Int32)_dr["ORIGIN"] + " AND ISO_CODE = '" + _dr["ISO_CODE"].ToString() + "' AND CAGE_CURRENCY_TYPE = " + _dr["CAGE_CURRENCY_TYPE"].ToString() + "").Length > 0)
        {
          _cage_count_amount = (Decimal)CageSessionReport.CageDtCounts.Compute("SUM(DEPOSITS)", "ORIGIN = " + (Int32)_dr["ORIGIN"] + " AND ISO_CODE = '" + _dr["ISO_CODE"].ToString() + "' AND CAGE_CURRENCY_TYPE = " + _dr["CAGE_CURRENCY_TYPE"].ToString() + "");
        }

        // If new currency
        if (_current_currency != _previous_currency_desc || _currency_iso_type.Type != _previous_currency_type)
        {
          _total_payment_method = _deposit - _withdraw;

          // If not first, insert total
          if (_previous_currency_desc != String.Empty)
          {
            _cage_count_total_amount = 0;
            if (CageSessionReport.CageDtCounts.Select("ORIGIN IS NULL AND ISO_CODE = '" + _previous_currency_iso + "' AND CAGE_CURRENCY_TYPE = " + (Int32)_previous_currency_type + " ").Length > 0)
            {
              _cage_count_total_amount = (Decimal)CageSessionReport.CageDtCounts.Compute("SUM(DEPOSITS)", "ORIGIN IS NULL AND ISO_CODE = '" + _previous_currency_iso + "' AND CAGE_CURRENCY_TYPE = " + (Int32)_previous_currency_type + " ");
            }

            if (_previous_currency_type != CurrencyExchangeType.CASINO_CHIP_COLOR)
            {
              _total_currency_string = ((Currency)_total_currency).ToString(false);
              _cage_count_total_amount_string = ((Currency)_cage_count_total_amount).ToString(false);
            }
            else
            {
              _total_currency_string = Convert.ToInt32(_total_currency).ToString();
              _cage_count_total_amount_string = Convert.ToInt32(_cage_count_total_amount).ToString();
            }

            _sb_html.AppendLine(String.Format("<tr><td><strong>{0}</strong></td><td style=\"text-align:right\"><strong>{1}</strong></td><td style=\"text-align:right\"><strong>{2}</strong></td></tr>"
                                              , "&nbsp;&nbsp;&nbsp;Total"
                                              , _total_currency_string
                                              , _cage_count_total_amount_string));
            _sb_html.AppendLine("<tr><td>&nbsp;</td><td>&nbsp;</td></tr>");
            _total_currency = 0;
          }

          _total_currency = _total_currency + _total_payment_method;

          _sb_html.AppendLine(String.Format("<tr><td><strong>{0}</strong></td><td>&nbsp;</td><td>&nbsp;</td></tr>"
                                            , _current_currency));

          if (_currency_iso_type.Type != CurrencyExchangeType.CASINO_CHIP_COLOR)
          {
            _total_currency_string = ((Currency)_total_payment_method).ToString(false);
            _cage_count_total_amount_string = ((Currency)_cage_count_amount).ToString(false);
          }
          else
          {
            _total_currency_string = Convert.ToInt32(_total_payment_method).ToString();
            _cage_count_total_amount_string = Convert.ToInt32(_cage_count_amount).ToString();
          }

          _sb_html.AppendLine(String.Format("<tr><td>{0}</td><td style=\"text-align:right\">{1}</td><td style=\"text-align:right\">{2}</td></tr>"
                                              , _payment_method
                                              , _total_currency_string
                                              , _cage_count_total_amount_string));

          _previous_currency_iso = _currency_iso_type.IsoCode;
          _previous_currency_desc = _current_currency;
          _previous_currency_type = _currency_iso_type.Type;
        }

        else
        {
          _total_payment_method = _deposit - _withdraw;
          _total_currency = _total_currency + _total_payment_method;

          if (_currency_iso_type.Type != CurrencyExchangeType.CASINO_CHIP_COLOR)
          {
            _total_currency_string = ((Currency)_total_payment_method).ToString(false);
            _cage_count_total_amount_string = ((Currency)_cage_count_amount).ToString(false);
          }
          else
          {
            _total_currency_string = Convert.ToInt32(_total_payment_method).ToString();
            _cage_count_total_amount_string = Convert.ToInt32(_cage_count_amount).ToString();
          }
          
          _sb_html.AppendLine(String.Format("<tr><td>{0}</td><td style=\"text-align:right\">{1}</td><td style=\"text-align:right\">{2}</td></tr>"
                                            , _payment_method
                                            , _total_currency_string
                                            , _cage_count_total_amount_string));
        }
      }

      _cage_count_total_amount = 0;

      if (_previous_currency_desc == Resource.String("STR_CAGE_CASINO_CHIPS"))
      {
        _previous_currency_desc = Cage.CHIPS_ISO_CODE;
      }

      if (CageSessionReport.CageDtCounts.Select("ORIGIN IS NULL AND ISO_CODE = '" + _previous_currency_iso + "' AND CAGE_CURRENCY_TYPE = " + (Int32)_previous_currency_type + " ").Length > 0)
      {
        _cage_count_total_amount = (Decimal)CageSessionReport.CageDtCounts.Select("ORIGIN IS NULL AND ISO_CODE = '" + _previous_currency_iso + "' AND CAGE_CURRENCY_TYPE = " + (Int32)_previous_currency_type + " ")[0].ItemArray[3];
      }

      if (_previous_currency_type != CurrencyExchangeType.CASINO_CHIP_COLOR)
      {
        _total_currency_string = ((Currency)_total_currency).ToString(false);
        _cage_count_total_amount_string = ((Currency)_cage_count_total_amount).ToString(false);
      }
      else
      {
        _total_currency_string = Convert.ToInt32(_total_currency).ToString();
        _cage_count_total_amount_string = Convert.ToInt32(_cage_count_total_amount).ToString();
      }

      _sb_html.AppendLine(String.Format("<tr><td><strong>{0}</strong></td><td style=\"text-align:right\"><strong>{1}</strong></td><td style=\"text-align:right\"><strong>{2}</strong></td></tr>"
                                        , "&nbsp;&nbsp;&nbsp;Total"
                                        , _total_currency_string
                                        , _cage_count_total_amount_string));

      return _sb_html.ToString();
    }

    private String CageStockHtml(CageSessionReport.TYPE_CAGE_GLOBAL_SESSION_REPORT CageSessionReport, ParametersCageSessionsReport ParametersOverview)
    {
      StringBuilder _sb_html;
      String _currency;
      Decimal _currency_total;
      Decimal _denomination;
      Decimal _quantity;
      Boolean _show_total_currency;
      Decimal _denom_per_quantity;
      CageCurrencyType _currency_type;
      CageCurrencyType _last_currency_type;

      String _currency_show;
      Decimal _currency_total_show;
      String _denomination_show;
      String _quantity_show;
      String _currency_type_show;

      String _name;
      String _drawing;

      String _national_currency;
      CurrencyIsoType _currency_iso_type;

      _sb_html = new StringBuilder();
      _currency = String.Empty;
      _currency_show = String.Empty;
      _currency_total_show = 0;
      _currency_total = 0;
      _show_total_currency = false;
      _quantity = 0;
      _denomination = 0;
      _denomination_show = String.Empty;
      _quantity_show = String.Empty;
      _currency_type = CageCurrencyType.Bill;
      _currency_type_show = String.Empty;
      _last_currency_type = CageCurrencyType.Bill;

      _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");

      foreach (DataRow _dr in CageSessionReport.CageDtStock.Rows)
      {
        _denomination = (Decimal)_dr["cgs_denomination"];
        _quantity = (Decimal)_dr["cgs_quantity"];
        _currency_type_show = String.Empty;
        _name = String.Empty;
        _drawing = String.Empty;
        _currency_iso_type = new CurrencyIsoType();

        _currency_iso_type.IsoCode = _dr["cgs_iso_code"].ToString();
        _currency_iso_type.Type = FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_dr["cgs_cage_currency_type"]);

        switch ((Int32)(_denomination))
        {
          case Cage.COINS_CODE:
            _denomination_show = Resource.String("STR_CAGE_COINS"); // "Monedas"
            _quantity_show = "&nbsp;";
            _denom_per_quantity = Math.Round(_quantity, 2);

            if (_currency_iso_type.IsoCode == _national_currency)
            {
              m_coins_summary = _denom_per_quantity;
            }
            else if (_currency_iso_type.IsoCode != Cage.CHIPS_ISO_CODE && !FeatureChips.IsCurrencyExchangeTypeChips(_currency_iso_type.Type))
            {
              this.UpdateCurrenciesSummary(_currency_iso_type, _denom_per_quantity);
            }
            else
            {
              this.UpdateChipsSummary(_currency_iso_type, _denomination, _quantity);
            }
            break;

          case Cage.TICKETS_CODE:
            continue;

          case Cage.BANK_CARD_CODE:
            _denomination_show = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_BANK_CARD"); // "Tarjeta bancaria"
            _quantity_show = "&nbsp;";
            _denom_per_quantity = Math.Round(_quantity, 2);

            if (_currency_iso_type.IsoCode == _national_currency)
            {
              m_bank_cards_summary = _denom_per_quantity;
            }
            else if (_currency_iso_type.IsoCode != Cage.CHIPS_ISO_CODE && !FeatureChips.IsCurrencyExchangeTypeChips(_currency_iso_type.Type))
            {
              this.UpdateCurrenciesSummary(_currency_iso_type, _denom_per_quantity);
            }
            else
            {
              this.UpdateChipsSummary(_currency_iso_type, _denomination, _quantity);
            }
            break;

          case Cage.CHECK_CODE:
            _denomination_show = Resource.String("STR_ACCOUNT_PAYMENT_ORDER_CHECK_PAYMENT"); // "Cheques"
            _quantity_show = "&nbsp;";
            _denom_per_quantity = Math.Round(_quantity, 2);

            if (_currency_iso_type.IsoCode == _national_currency)
            {
              m_checks_summary = _denom_per_quantity;
            }
            else if (_currency_iso_type.IsoCode != Cage.CHIPS_ISO_CODE && !FeatureChips.IsCurrencyExchangeTypeChips(_currency_iso_type.Type))
            {
              this.UpdateCurrenciesSummary(_currency_iso_type, _denom_per_quantity);
            }
            else
            {
              this.UpdateChipsSummary(_currency_iso_type, _denomination, _quantity);
            }
            break;

          default:
            _denomination_show = Math.Round(_denomination, 2).ToString();
            _quantity_show = Math.Round(_quantity, 0).ToString();
            _denom_per_quantity = _denomination * _quantity;
            if (_currency_iso_type.Type == CurrencyExchangeType.CASINO_CHIP_COLOR)
            {
              _denom_per_quantity = _quantity;
            }
            _currency_type = (CageCurrencyType)_dr["cgs_cage_currency_type"];

            if (_currency_type == CageCurrencyType.Bill)
            {
              _currency_type_show = Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_BILL"); // "B"
            }
            else if (!FeatureChips.IsCageCurrencyTypeChips(_currency_type))
            {
              _currency_type_show = Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE_COIN"); // "M"
            }

            if (_currency_iso_type.IsoCode == _national_currency && !FeatureChips.IsCurrencyExchangeTypeChips(_currency_iso_type.Type))
            {
              m_cash_summary = m_cash_summary + _denom_per_quantity;
            }
            else if (_currency_iso_type.IsoCode != Cage.CHIPS_ISO_CODE && !FeatureChips.IsCurrencyExchangeTypeChips(_currency_iso_type.Type))
            {
              this.UpdateCurrenciesSummary(_currency_iso_type, _denom_per_quantity);
            }
            else
            {
              _currency_type_show = String.Empty;
              this.UpdateChipsSummary(_currency_iso_type, _denomination, _quantity);
            }

            break;
        }

        if (FeatureChips.IsCageCurrencyTypeChips(_currency_type))
        {
          _name = _dr["name"].ToString();
          _drawing = _dr["drawing"].ToString();
        }


        if (_currency_iso_type.IsoCode == _currency && _last_currency_type == (CageCurrencyType)_dr["cgs_cage_currency_type"]
            || _currency_iso_type.IsoCode == _currency && !FeatureChips.IsCageCurrencyTypeChips((CageCurrencyType)_dr["cgs_cage_currency_type"]))
        {
          _show_total_currency = false;
          _currency_show = "&nbsp;";
          _currency_total += _denom_per_quantity;
        }
        else
        {
          _show_total_currency = _currency != String.Empty || CageSessionReport.CageDtStock.Rows.Count == 1 ? true : false; //not first time
          _currency = _currency_iso_type.IsoCode;
          _last_currency_type = (CageCurrencyType)_dr["cgs_cage_currency_type"];

          _currency_iso_type.IsoCode = _dr["cgs_iso_code"].ToString(); ;
          _currency_iso_type.Type = FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_dr["cgs_cage_currency_type"]);

          if (FeatureChips.IsCurrencyExchangeTypeChips(FeatureChips.ConvertToCurrencyExchangeType(_last_currency_type)))
          {
            _currency_show = FeatureChips.GetChipTypeDescription(_currency_iso_type);
          }
          else
          {
            _currency_show = _currency_iso_type.IsoCode;
          }
          _currency_total_show = _currency_total;
          _currency_total = _denom_per_quantity;
        }

        if (_show_total_currency)
        {
          // total currency
          _sb_html.AppendLine(String.Format("<tr style=\"text-align:right\"><td>{0}</td><td><strong>{1}</strong></td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td><strong>{6}</strong></td></tr>"
            , "&nbsp;"
            , Resource.String("STR_VOUCHER_TOTAL_AMOUNT")
            , "&nbsp;"
            , "&nbsp;"
            , "&nbsp;"
            , "&nbsp;"
            , ((Currency)_currency_total_show).ToString(false)));

          _sb_html.AppendLine("<tr><td colspan=\"6\">&nbsp;</td></tr>");
          //_currency_total = 0;
        }

        if (_currency_type == CageCurrencyType.ChipsColor)
        {
          _sb_html.AppendLine(String.Format("<tr style=\"text-align:right\"><td style=\"text-align:left;\"><strong>{0}</strong></td><td colspan=3>{1}&nbsp;</td><td colspan=2 style=\"text-align:left\">&nbsp;{2}</td><td>{3}&nbsp;&nbsp;</td></tr>"
            , _currency_show == Cage.CHIPS_ISO_CODE ? Resource.String("STR_CURRENCY_TYPE_0_ISO_X01").ToUpper() : _currency_show
            , _name
            , _drawing
            , _quantity_show));
        }
        else
        {
          _sb_html.AppendLine(String.Format("<tr style=\"text-align:right\"><td style=\"text-align:left\"><strong>{0}</strong></td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td></tr>"
          , _currency_show == Cage.CHIPS_ISO_CODE ? Resource.String("STR_CURRENCY_TYPE_0_ISO_X01").ToUpper() : _currency_show
          , _denomination_show
          , _currency_type_show
            , _name
            , String.Empty // _drawing
          , _quantity_show
          , ((Currency)_denom_per_quantity).ToString(false)));
        }
      }

      // total currency
      if (_currency_type == CageCurrencyType.ChipsColor)
      {
        _sb_html.AppendLine(String.Format("<tr style=\"text-align:right\"><td>{0}</td><td><strong>{1}</strong></td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td><strong>{6}&nbsp;</strong></td></tr>"
          , "&nbsp;"
          , Resource.String("STR_VOUCHER_TOTAL_AMOUNT")
          , "&nbsp;"
          , "&nbsp;"
          , "&nbsp;"
          , "&nbsp;"
          , Convert.ToInt32(_currency_total).ToString()));
      }
      else
      {
        _sb_html.AppendLine(String.Format("<tr style=\"text-align:right\"><td>{0}</td><td><strong>{1}</strong></td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td><strong>{6}</strong></td></tr>"
          , "&nbsp;"
          , Resource.String("STR_VOUCHER_TOTAL_AMOUNT")
          , "&nbsp;"
          , "&nbsp;"
          , "&nbsp;"
          , "&nbsp;"
          , ((Currency)_currency_total).ToString(false)));
      }
      _sb_html.AppendLine("<tr><td colspan=\"6\">&nbsp;</td></tr>");


      return _sb_html.ToString();
    } // CageStockHTML

    private String CageCustomLiabilitiesHtml(CageSessionReport.TYPE_CAGE_GLOBAL_SESSION_REPORT CageSessionReport)
    {
      StringBuilder _sb_html = new StringBuilder();

      Int64 _current_concept_id;
      Int64 _old_concept_id;

      String _national_currency;
      String _currency_to_show;
      String _liability_name;
      CurrencyIsoType _currency_iso_type;

      Boolean _is_same_concept = false;

      String _liability_value_string;

      _liability_value_string = String.Empty;

      _current_concept_id = -1;
      _old_concept_id = -1;

      _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");

      foreach (DataRow _dr in CageSessionReport.CageDtCustomLiabilities.Rows)
      {
        _is_same_concept = false;
        _current_concept_id = (Int64)_dr["LIABILITY_CONCEPT_ID"];

        _currency_iso_type = new CurrencyIsoType();
        _currency_iso_type.IsoCode = _dr["LIABILITY_ISO_CODE"].ToString();
        _currency_iso_type.Type = FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_dr["LIABILITY_CAGE_CURRENCY_TYPE"]);

        _currency_to_show = _currency_iso_type.IsoCode;

        if (FeatureChips.IsCurrencyExchangeTypeChips(_currency_iso_type.Type))
        {
          _currency_to_show = FeatureChips.GetChipTypeDescription(_currency_iso_type);
        }

        if (_current_concept_id == _old_concept_id)
        {
          _is_same_concept = true;
        }
        else
        {
          _is_same_concept = false;
        }

        if (_currency_iso_type.Type == CurrencyExchangeType.CASINO_CHIP_COLOR)
        {
          _liability_value_string = Convert.ToInt32((Decimal)_dr["LIABILITY_VALUE"]).ToString();
        }
        else
        {
          _liability_value_string = ((Currency)(Decimal)_dr["LIABILITY_VALUE"]).ToString(false);
        }

        if (!_is_same_concept)
        {
          _liability_name = _dr["LIABILITY_NAME"].ToString();

          if (_current_concept_id == 5)
            _liability_name = WSI.Common.Misc.GetCardPlayerConceptName(); // ID = 5 is "Dep�sito de Tarjetas" (CAGE_CONCEPTS table). But we will use the value found in the general params

          _sb_html.AppendLine("<tr style=\"text-align:left\">");
          _sb_html.AppendLine(String.Format("<td style=\"width:27%\">{0}</td>", _liability_name));
          _sb_html.AppendLine(String.Format("<td>{0}</td>", _currency_to_show));
          _sb_html.AppendLine(String.Format("<td align=\"right\" style=\"width:29%\">{0}</td>", _liability_value_string));
          _sb_html.AppendLine("</tr>");
        }
        else
        {
          _sb_html.AppendLine("<tr style=\"text-align:left\">");
          _sb_html.AppendLine(String.Format("<td style=\"width:27%\">{0}</td>", "&nbsp;&nbsp;"));
          _sb_html.AppendLine(String.Format("<td>{0}</td>", _currency_to_show));
          _sb_html.AppendLine(String.Format("<td align=\"right\" style=\"width:29%\">{0}</td>", _liability_value_string));
          _sb_html.AppendLine("</tr>");
        }

        _old_concept_id = _current_concept_id;

      }

      return _sb_html.ToString();

    } // CageCustomLiabilities

    private String CageDepositsWithdrawsHtml()
    {
      StringBuilder _sb_html = new StringBuilder();

      String _national_currency;
      String _section_label;
      String _bold_mod_open;
      String _bold_mod_close;
      String _currency;
      Boolean _is_first_time;
      String _total_deposits_string;

      _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");

      _sb_html.AppendLine("");

      _is_first_time = true;

      foreach (CurrencyIsoType _currency_iso_type in m_total_deposits.Keys)
      {
        _bold_mod_open = String.Empty;
        _bold_mod_close = String.Empty;
        _section_label = "&nbsp;";

        _currency = _currency_iso_type.IsoCode;
        if (_currency_iso_type.IsoCode == Cage.CHIPS_ISO_CODE || FeatureChips.IsCurrencyExchangeTypeChips(_currency_iso_type.Type))
        {
          _currency = FeatureChips.GetChipTypeDescription(_currency_iso_type);
        }

        if (_currency_iso_type.IsoCode == _national_currency && _is_first_time)
        {
          _section_label = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_CAGE_IN");// "Entradas a B�veda"; 
          _bold_mod_open = "<strong>";
          _bold_mod_close = "</strong>";
          _is_first_time = false;
        }

        if(_currency_iso_type.Type == CurrencyExchangeType.CASINO_CHIP_COLOR )
        {
          _total_deposits_string = Convert.ToInt32(m_total_deposits[_currency_iso_type]).ToString();
        }
        else
        {
          _total_deposits_string = ((Currency)m_total_deposits[_currency_iso_type]).ToString(false);
        }
        _sb_html.AppendLine(String.Format("<tr><td>{3}{0}{4}</td><td style=\"width:30%\">{3}{1}{4}</td><td align=\"right\" style=\"width:30%\">{3}{2}{4}</td></tr>"
                         , _section_label
                         , _currency
                         , _total_deposits_string
                         , _bold_mod_open
                         , _bold_mod_close));
      }

      _is_first_time = true;

      foreach (CurrencyIsoType _currency_iso_type in m_total_withdraws.Keys)
      {
        _bold_mod_open = String.Empty;
        _bold_mod_close = String.Empty;
        _section_label = "&nbsp;";

        _currency = _currency_iso_type.IsoCode;
        if (_currency_iso_type.IsoCode == Cage.CHIPS_ISO_CODE || FeatureChips.IsCurrencyExchangeTypeChips(_currency_iso_type.Type))
        {
          _currency = FeatureChips.GetChipTypeDescription(_currency_iso_type);
        }

        if (_currency_iso_type.IsoCode == _national_currency && _is_first_time)
        {
          _section_label = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_CAGE_OUT");// "Salidas de B�veda";
          _bold_mod_open = "<strong>";
          _bold_mod_close = "</strong>";
          _is_first_time = false;
        }

        if ( _currency_iso_type.Type == CurrencyExchangeType.CASINO_CHIP_COLOR )
        {
          _total_deposits_string = Convert.ToInt32(m_total_withdraws[_currency_iso_type]).ToString();
        }
        else
        {
          _total_deposits_string = ((Currency)m_total_withdraws[_currency_iso_type]).ToString(false);
        }

        _sb_html.AppendLine(String.Format("<tr><td>{3}{0}{4}</td><td style=\"width:30%\">{3}{1}{4}</td><td align=\"right\" style=\"width:30%\">{3}{2}{4}</td></tr>"
                         , _section_label
                         , _currency
                         , _total_deposits_string
                         , _bold_mod_open
                         , _bold_mod_close));
      }

      _sb_html.AppendLine("<tr><td colspan=\"3\" class=\"line_top_td\">&nbsp;</td></tr>");

      _is_first_time = true;

      foreach (CurrencyIsoType _currency_iso_type in m_total_result.Keys)
      {
        _bold_mod_open = String.Empty;
        _bold_mod_close = String.Empty;
        _section_label = "&nbsp;";

        _currency = _currency_iso_type.IsoCode;
        if (_currency_iso_type.IsoCode == Cage.CHIPS_ISO_CODE || FeatureChips.IsCurrencyExchangeTypeChips(_currency_iso_type.Type))
        {
          _currency = FeatureChips.GetChipTypeDescription(_currency_iso_type);
        }

        if (_currency_iso_type.Type == CurrencyExchangeType.CASINO_CHIP_COLOR)
        {
          _total_deposits_string = Convert.ToInt32(m_total_result[_currency_iso_type]).ToString();
        }
        else
        {
          _total_deposits_string = ((Currency)m_total_result[_currency_iso_type]).ToString(false);
        }

        if (_currency_iso_type.IsoCode == _national_currency && _is_first_time)
        {
          _section_label = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_INOUT");// "(Entradas - Salidas)";
          _bold_mod_open = "<strong>";
          _bold_mod_close = "</strong>";
          _is_first_time = false;
        }

        _sb_html.AppendLine(String.Format("<tr><td>{3}{0}{4}</td><td style=\"width:30%\">{3}{1}{4}</td><td align=\"right\" style=\"width:30%\">{3}{2}{4}</td></tr>"
                         , _section_label
                         , _currency
                         , _total_deposits_string
                         , _bold_mod_open
                         , _bold_mod_close));
      }

      return _sb_html.ToString();

    }

    private String OtherConceptsHtml(CageSessionReport.TYPE_CAGE_GLOBAL_SESSION_REPORT CageSessionReport)
    {
      StringBuilder _sb_html = new StringBuilder();

      CurrencyIsoType _currency;
      String _current_currency;
      Int64 _current_concept;
      Int64 _old_concept;

      String _bold_mod_open;
      String _bold_mod_close;
      String _section_label;

      String _national_currency;

      String _value_string;

      _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");
      _current_currency = String.Empty;
      _old_concept = 0;

      foreach (DataRow _dr in CageSessionReport.CageDtSessionOtherSystemConcepts.Rows)
      {
        _currency = new CurrencyIsoType();
        _bold_mod_open = String.Empty;
        _bold_mod_close = String.Empty;
        _section_label = "&nbsp;";

        _currency.IsoCode = _dr["CM_ISO_CODE"].ToString();
        _currency.Type = FeatureChips.ConvertToCurrencyExchangeType((CageCurrencyType)_dr["CAGE_CURRENCY_TYPE"]);
        _current_concept = (Int64)_dr["CM_CONCEPT_ID"];

        _current_currency = _currency.IsoCode;
        if (FeatureChips.IsCurrencyExchangeTypeChips(_currency.Type))
        {
          _current_currency = FeatureChips.GetChipTypeDescription(_currency);
        }

        if (_current_currency == _national_currency)
        {
          if (_current_concept != _old_concept)
          {
            _section_label = _dr["CM_DESCRIPTION"].ToString();
            _bold_mod_open = "<strong>";
            _bold_mod_close = "</strong>";
          }
        }

        if (_currency.Type == CurrencyExchangeType.CASINO_CHIP_COLOR)
        {
          _value_string = Convert.ToInt32((Decimal)_dr["CM_VALUE"]).ToString();
        }
        else
        {
          _value_string = ((Currency)(Decimal)_dr["CM_VALUE"]).ToString(false);
        }

        _sb_html.AppendLine(String.Format("<tr><td>{3}{0}{4}</td><td>{3}{1}{4}</td><td align=\"right\">{3}{2}{4}</td></tr>"
                         , _section_label
                         , _current_currency
                         , _value_string
                         , _bold_mod_open
                         , _bold_mod_close));

        _old_concept = _current_concept;
      }

      return _sb_html.ToString();
    }

    private String CageSummaryHtml()
    {
      String _str_value;
      StringBuilder _sb_html = new StringBuilder();
      Decimal _chips_summary;
      String _chips_descriptions;
      String _chips_values;
      String _iso_code_desc;
      String _national_currency;

      _national_currency = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode");

      _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_STOCK_SUMMARY");// "Resumen de Stock de B�veda"; // "Resumen de Stock de B�veda"
      AddString("STR_VOUCHER_STOCK_SUMMARY", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_TOTAL_CASH");//"Total Efectivo"; // "Total efectivo"
      AddString("STR_VOUCHER_SUMMARY_CASH", _str_value);
      AddString("VOUCHER_SUMMARY_CASH", ((Currency)m_cash_summary).ToString(true));

      _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_TOTAL_CHECK");// "Total Cheques"; // "Total cheques"
      AddString("STR_VOUCHER_SUMMARY_CHECKS", _str_value);
      AddString("VOUCHER_SUMMARY_CHECKS", ((Currency)m_checks_summary).ToString(true));

      _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_TOTAL_CARD_BANK");//"Total Tarjetas Banc."; // "Total tarjetas bancarias"
      AddString("STR_VOUCHER_SUMMARY_BANK_CARDS", _str_value);
      AddString("VOUCHER_SUMMARY_BANK_CARDS", ((Currency)m_bank_cards_summary).ToString(true));

      _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_TOTAL_CURRENCY");//"Total Monedas"; // "Total monedas"
      AddString("STR_VOUCHER_SUMMARY_COINS", _str_value);
      AddString("VOUCHER_SUMMARY_COINS", ((Currency)m_coins_summary).ToString(true));

      _chips_summary = 0;
      _chips_descriptions = String.Empty;
      _chips_values = String.Empty;

      foreach (CurrencyIsoType _currency_iso_type in m_chips_summary.Keys)
      {
        if (_currency_iso_type.IsoCode == _national_currency || _currency_iso_type.Type == CurrencyExchangeType.CASINO_CHIP_COLOR)
        {
          _chips_descriptions = _chips_descriptions + FeatureChips.GetChipTypeDescription(_currency_iso_type) + "\n";
          if (_currency_iso_type.Type == CurrencyExchangeType.CASINO_CHIP_COLOR)
          {
            _chips_values = _chips_values + ((Int64)m_chips_summary[_currency_iso_type]) + "  \n";
          }
          else
          {
            _chips_values = _chips_values + ((Currency)m_chips_summary[_currency_iso_type]).ToString(false) + "\n";
            _chips_summary += (Currency)m_chips_summary[_currency_iso_type];
          }
        }
      }
      //_str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_TOTAL_CHIPS");//"Total Fichas"; // "Total fichas"
      AddMultipleLineString("STR_VOUCHER_SUMMARY_CHIPS", _chips_descriptions);
      AddMultipleLineString("VOUCHER_SUMMARY_CHIPS", _chips_values);

      _str_value = Resource.String("STR_VOUCHER_CARD_REDEEM_TOTAL");// "Total:"; // "Total:"
      AddString("STR_SUMMARY_TOTAL_VALUE", _str_value);
      AddString("VOUCHER_SUMMARY_TOTAL_VALUE", ((Currency)(m_cash_summary + m_checks_summary + _chips_summary + m_coins_summary + m_bank_cards_summary)).ToString(true));


      _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_OTHER_CURRENCYS");// "Otras Divisas"; // "Otras Divisas"
      AddString("STR_OTHER_CURRENCIES", _str_value);

      // Other currencies section build

      foreach (CurrencyIsoType _currency_iso_type in m_currencies_summary.Keys)
      {
        _iso_code_desc = _currency_iso_type.IsoCode;
        if (_currency_iso_type.Type == CurrencyExchangeType.CASINO_CHIP_COLOR)
        {
          _str_value = Convert.ToInt32((Currency)m_currencies_summary[_currency_iso_type]).ToString();
        }
        else 
        {
          _str_value = ((Currency)m_currencies_summary[_currency_iso_type]).ToString(false);
        }
        if (FeatureChips.IsCurrencyExchangeTypeChips(_currency_iso_type.Type))
        {
          _iso_code_desc = FeatureChips.GetChipTypeDescription(_currency_iso_type);
        }
        _sb_html.AppendLine(String.Format("<tr><td>&nbsp;&nbsp;{0}</td><td>&nbsp;</td><td align=\"right\">{1}</td></tr>"
                                          , _iso_code_desc
                                          , _str_value));

        foreach (CurrencyIsoType _currency_iso_type_chips in m_chips_summary.Keys)
        {
          _chips_descriptions = String.Empty;
          _chips_values = String.Empty;
          if (_currency_iso_type_chips.IsoCode == _currency_iso_type.IsoCode)
          {
            _chips_descriptions = _chips_descriptions + FeatureChips.GetChipTypeDescription(_currency_iso_type_chips) + "\n";
            _chips_values = _chips_values + ((Currency)m_chips_summary[_currency_iso_type_chips]).ToString(false) + "\n";
            _sb_html.AppendLine(String.Format("<tr><td>&nbsp;&nbsp;{0}</td><td>&nbsp;</td><td align=\"right\">{1}</td></tr>"
                                              , _chips_descriptions
                                              , _chips_values));
          }
        }
        _sb_html.AppendLine(String.Format("<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>"));

      }

      return _sb_html.ToString();
    }

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(CageSessionReport.TYPE_CAGE_GLOBAL_SESSION_REPORT CageSessionReport, ParametersCageSessionsReport ParametersOverview)
    {

      String _str_value;

      SetParameterValue("@VOUCHER_STYLE", "BODY{background-color:'" + ColorTranslator.ToHtml(ParametersOverview.BackColor) + "';}");

      _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_CAGE_SUMMARY");// "Resumen de B�veda"; // "Resumen de B�veda"
      AddString("STR_VOUCHER_CAGE_SUMMARY", _str_value);

      _str_value = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode"); // "ISO-Code moneda nacional"
      AddString("STR_VOUCHER_CAGE_REPORT_NATIONAL_CURRENCY", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_CAGE_RESULT"); // "Resultado de B�veda"
      AddString("STR_VOUCHER_CAGE_RESULT", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_INOUT"); // "(Entradas - Salidas)"
      AddString("STR_VOUCHER_CAGE_DEPOSITS_WITHDRAWALS", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_COUNT"); // "Arqueos"
      AddString("STR_VOUCHER_CAGE_COUNTS", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_DEPOSITS"); // "Entradas"
      AddString("STR_VOUCHER_DEPOSITS", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_WITHDRAWS"); // "Salidas"
      AddString("STR_VOUCHER_WITHDRAWS", _str_value);

      if (!CageSessionReport.StockIsStored)
      {
        _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_CAGE_STOCK"); // "Stock de B�veda"
      }
      else
      {
        _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_STORED_CAGE_STOCK"); // "Stock de B�veda (Al Cierre de la Sesi�n)"
      }
      AddString("STR_VOUCHER_CAGE_STOCK", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_CURRENCY"); // "Divisa"
      AddString("STR_VOUCHER_CURRENCY", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_DENOMINATION"); // "Denominaci�n"
      AddString("STR_VOUCHER_DENOMINATION", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_HANDPAY_SESSION_CURRENCY_TYPE"); // "Tipo"
      AddString("STR_VOUCHER_TYPE", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CASH_CHIPS_NAME"); // "Name"
      AddString("STR_VOUCHER_NAME", _str_value);

      _str_value = Resource.String("STR_UC_CARD_BTN_PRIZE_DRAW"); // "Draw"
      AddString("STR_VOUCHER_DRAW", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_QUANTITY"); // "Cantidad"
      AddString("STR_VOUCHER_QUANTITY", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_TOTAL"); // "Total"
      AddString("STR_VOUCHER_STOCK_TOTAL", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_TOTAL"); // "Total"
      AddString("STR_VOUCHER_TOTAL", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_CAGE_TRANSACTIONS"); // "Transacciones de B�veda"
      AddString("STR_VOUCHER_ORIGIN_DESTIN", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_CUSTOM"); // "Personalizado"
      AddString("STR_VOUCHER_CUSTOM", _str_value);

      AddCurrency("VALUE_DIFF", CageSessionReport.CageDeposits - CageSessionReport.CageWithdraws);

      _str_value = Resource.String("STR_VOUCHER_CAGESESSIONREPORT_LIABILITIES"); // "Provisiones"
      AddString("STR_VOUCHER_LIABILITIES", _str_value);

    } // LoadVoucherResources

    private void UpdateDepositsWithdrwas(CurrencyIsoType CurrencyIsoType, Decimal ValueIn, Decimal ValueOut)
    {
      // Entradas
      if (m_total_deposits.ContainsKey(CurrencyIsoType))
      {
        m_total_deposits[CurrencyIsoType] = m_total_deposits[CurrencyIsoType] + ValueIn;
      }
      else
      {
        m_total_deposits.Add(CurrencyIsoType, ValueIn);
      }

      // Salidas
      if (m_total_withdraws.ContainsKey(CurrencyIsoType))
      {
        m_total_withdraws[CurrencyIsoType] = m_total_withdraws[CurrencyIsoType] + ValueOut;
      }
      else
      {
        m_total_withdraws.Add(CurrencyIsoType, ValueOut);
      }

      // Entradas - Salidas
      if (m_total_result.ContainsKey(CurrencyIsoType))
      {
        m_total_result[CurrencyIsoType] = m_total_result[CurrencyIsoType] + ValueIn - ValueOut;
      }
      else
      {
        m_total_result.Add(CurrencyIsoType, (ValueIn - ValueOut));
      }
    }

    private void UpdateCurrenciesSummary(CurrencyIsoType CurrencyIsoType, Decimal Value)
    {
      if (m_currencies_summary.ContainsKey(CurrencyIsoType))
      {
        m_currencies_summary[CurrencyIsoType] = m_currencies_summary[CurrencyIsoType] + Value;
      }
      else
      {
        m_currencies_summary.Add(CurrencyIsoType, Value);
      }
    }

    private void UpdateChipsSummary(CurrencyIsoType CurrencyIsoType, Decimal Denomination, Decimal Quantity)
    {
      Decimal _denom_per_quantity;
      _denom_per_quantity = Denomination * Quantity;

      if (CurrencyIsoType.Type == CurrencyExchangeType.CASINO_CHIP_COLOR)
      {
        _denom_per_quantity = Quantity;
      }

      if (m_chips_summary.ContainsKey(CurrencyIsoType))
      {
        m_chips_summary[CurrencyIsoType] = m_chips_summary[CurrencyIsoType] + _denom_per_quantity;
      }
      else
      {
        m_chips_summary.Add(CurrencyIsoType, _denom_per_quantity);
    }

    }
    #endregion

    #region Public Methods

    public void CashDeskOverview(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats, ParametersCageSessionsReport ParametersOverview)
    { }

    #endregion
  }
}
