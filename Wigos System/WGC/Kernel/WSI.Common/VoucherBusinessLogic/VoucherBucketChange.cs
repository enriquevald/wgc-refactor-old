﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherBucketChange.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  // Bucket Change
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Bucket name:
  // Bucket initial:    $/#
  // Bucket increment:  $/#
  // Bucket final:      $/#
  //
  //         Voucher Footer
  public class VoucherBucketChange : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructores

    public VoucherBucketChange(BucketVoucher BucketVoucher, PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load general voucher data.
      //  3. Set specific params

      // 1. Load HTML structure.
      LoadVoucher("VoucherBucketChange");

      // 2. Load general voucher data.
      LoadVoucherGeneralData();

      // 3. Set params
      LoadVoucherResources(BucketVoucher);
    }
    #endregion

    #region Private Methods
    private void LoadVoucherResources(BucketVoucher BucketVoucherInstance)
    {
      String _paramName;
      String _paramValue;
      _paramName = "STR_VOUCHER_BUCKET_CHANGE_TITLE";
      _paramValue = Resource.String(_paramName) + " " + BucketVoucherInstance.BucketName;
      AddString(_paramName, _paramValue);

      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", BucketVoucherInstance.AccountInfo);

      _paramName = "STR_BUCKET_INITIAL";
      _paramValue = Resource.String("STR_UC_BANK_INITIAL") + ":";
      AddString(_paramName, _paramValue);

      _paramName = "STR_BUCKET_INCREMENT";
      _paramValue = Resource.String("STR_UC_BANK_INCREMENT") + ":";
      AddString(_paramName, _paramValue);

      _paramName = "STR_BUCKET_FINAL";
      _paramValue = Resource.String("STR_UC_BANK_FINAL") + ":";
      AddString(_paramName, _paramValue);

      _paramName = "STR_BUCKET_DETAILS";
      _paramValue = Resource.String("STR_UC_PLAYER_TRACK_COMMENTS") + ":";
      AddString(_paramName, _paramValue);

      AddString("VOUCHER_BUCKET_INITIAL", BucketVoucherInstance.Initial);
      AddString("VOUCHER_BUCKET_INCREMENT", BucketVoucherInstance.Increment);
      AddMultipleLineString("VOUCHER_BUCKET_DETAILS", BucketVoucherInstance.Details);
      AddString("VOUCHER_BUCKET_FINAL", BucketVoucherInstance.Final);
    }
    #endregion

  }
}
