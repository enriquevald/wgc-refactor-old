﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCashDeskStatus.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
// 11-OCT-2016  ESE         Bug 17833: Televisa voucher, status cash
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  // 8. Cash Desk Status
  // Format:
  //          Voucher Logo
  //         Voucher Header
  //
  // Cashier name:
  // Terminal number: 
  // Voucher Id:
  //          Voucher Title
  //
  //  Here data...
  //
  //  Final Balance:  $
  //
  //         Voucher Footer
  public class VoucherCashDeskStatus : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherCashDeskStatus(PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherCashDeskStatus");

      // 2. Load voucher resources.
      LoadVoucherResources();

      // 3. Load general voucher data.      
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources()
    {
      String value = "";
      TYPE_SPLITS _split_data;

      if (!Split.ReadSplitParameters(out _split_data))
      {
        return;
      }

      //Header
      if (Misc.IsBankCardCloseCashEnabled())
      {
        LoadHeader(_split_data.company_a.header);
        LoadFooter("", "");
      }

      // Images
      //AddLogo("VOUCHER_LOGO", "IMG_LOGO_VOUCHER", "VoucherLogo.jpg");

      // NLS Values

      // - Title
      value = Resource.String("STR_VOUCHER_CASH_DESK_STATUS_TITLE");
      AddString("STR_VOUCHER_CASH_DESK_STATUS_TITLE", value);

      // - General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

      // Cash desk close voucher
      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_NUMBERS");
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_NUMBERS", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_AMOUNTS");
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_AMOUNTS", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_OPEN") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_OPEN", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_OUT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_OUT", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CASH_OUT_DETAILS") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_CASH_OUT_DETAILS", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_MB_DEPOSIT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_MB_DEPOSIT", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CURRENT_AMOUNT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_CURRENT_AMOUNT", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_PENDING_AMOUNT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_PENDING_AMOUNT", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_INITIAL_AMOUNT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_INITIAL_AMOUNT", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_DEPOSIT", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_WITHDRAW", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_SUB_TOTAL") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_SUB_TOTAL", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_PRIZES") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_WON", value);

      value = Resource.String("STR_FRM_CASH_DESK_PRIZE_PAYED") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_PAYED_WON", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_DEVOLUTIONS") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_DEV", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TAXES") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_TAXES", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TOTAL_CASH_DESK") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_TOTAL_CASH_DESK", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_NOT_REDEEMABLE") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_NOT_REDEEMABLE", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_TAXES") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_TAX_TAXES", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CASH_NET") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_CASH_NET", value);

      value = Resource.String("STR_UC_BANK_HANDPAYS") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_TITLE", value);

      value = Resource.String("STR_VOUCHER_CARD_HANDPAY_005") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_PAYED", value);

      value = Resource.String("STR_VOUCHER_CARD_HANDPAY_014") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_CANCELED", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_TOTAL") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_TOTAL", value);

      value = Resource.String("STR_UC_CARD_BALANCE_PROMO_TO_REDEEM") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_PROMO_TO_REDEEM", value);

      //value = Resource.String("STR_UC_CARD_BALANCE_NOT_REDEEMABLE") + ":";
      value = _split_data.text_promo + ":";
      if (String.IsNullOrEmpty(value))
      {
        value = Resource.String("STR_VOUCHER_CARD_ADD_PROMOTION");
      }
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_NO_REDEEM", value);

      value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_TOTAL_CANCELLED") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_TOTAL_CANCELLED", value);

    } // LoadVoucherResources

    #endregion

    #region Public Methods

    public void SetupCashStatusVoucher(WSI.Common.Cashier.TYPE_CASHIER_SESSION_STATS CashierSessionStats)
    {
      Currency signed_amount;
      String _str_value;
      Currency _cs_mb_diff;
      Boolean _is_tito_mode;
      Boolean _gp_pending_cash_partial;

      _is_tito_mode = Utils.IsTitoMode();
      _gp_pending_cash_partial = MobileBank.GP_GetRegisterShortfallOnDeposit();

      //  1. Depositos
      //  2. Retiros
      //  3. Entradas
      //  4. Salidas
      //     ---------------------------
      //  5. Saldo (Efectivo en Caja)
      // 
      //     Detalle Salidas
      //  6. Premios
      //  7. Impuestos
      //  8. Premios pagados
      // 
      //     ---------------------------

      //AddString("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_OPEN_NUM", CashierSessionStats.total_initial_movs.ToString());
      //AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_OPEN_AMT", CashierSessionStats.initial_balance);

      //  1. Depositos
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_DEPOSIT_AMT", CashierSessionStats.deposits);

      //  2. Retiros
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_WITHDRAW_AMT", CashierSessionStats.withdraws);

      //  3. Cash In
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_IN_AMT", CashierSessionStats.total_cash_in);

      //  4. Cash Out
      signed_amount = CashierSessionStats.total_cash_out; // *-1;
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CASH_OUT_AMT", signed_amount);

      //  Diferencia Bancos Móviles
      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF_MISSING") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF", _str_value);
      _cs_mb_diff = CashierSessionStats.mb_total_balance_lost + CashierSessionStats.mb_pending;
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_COLL_MB_DIFF", _cs_mb_diff);

      //  Sobrante Bancos Móviles
      if (_gp_pending_cash_partial)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_OVER_SHORTFALL_CASH_DESK_STATUS") + ":";
      }
      else
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_OVER") + ":";
      }

      AddString("STR_VOUCHER_CASH_OVER", _str_value);
      AddCurrency("VOUCHER_CASH_OVER", CashierSessionStats.total_cash_over);

      //  Chech Payments
      _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS") + ":";
      AddString("STR_VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS", _str_value);
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_SUMMARY_CHECK_PAYMENTS_AMT", CashierSessionStats.payment_orders);

      //Credit Card Exchange
      if (CashierSessionStats.total_bank_card > 0)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD") + ":";
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", _str_value);
        AddCurrency("VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", CashierSessionStats.total_bank_card);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", "");
        AddString("VOUCHER_CASH_DESK_CLOSE_CREDIT_CARD", "");
      }

      //Check
      if (CashierSessionStats.total_check > 0)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CHECK") + ":";
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_CHECK", _str_value);
        AddCurrency("VOUCHER_CASH_DESK_CLOSE_CHECK", CashierSessionStats.total_check);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_CHECK", "");
        AddString("VOUCHER_CASH_DESK_CLOSE_CHECK", "");
      }

      // Currency Exchange
      if (CashierSessionStats.total_currency_exchange > 0)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE") + ":";
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", _str_value);
        AddCurrency("VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", CashierSessionStats.total_currency_exchange);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", "");
        AddString("VOUCHER_CASH_DESK_CLOSE_CURRENCY_EXCHANGE", "");
      }

      //Commission Exchange
      if (CashierSessionStats.total_commission > 0)
      {
        if (Misc.IsBankCardCloseCashEnabled())
        {
          _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_ADMINISTRATIVE_SERVICES") + ":";
        }
        else
        {
          _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION") + ":";
        }

        AddString("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION", _str_value);
        AddCurrency("VOUCHER_CASH_DESK_CLOSE_COMMISSION", CashierSessionStats.total_commission);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_COMMISSION", "");
        AddString("VOUCHER_CASH_DESK_CLOSE_COMMISSION", "");
      }

      //Recharge for Points
      if (CashierSessionStats.points_as_cashin > 0)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS") + ":";
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", _str_value);
        AddCurrency("VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", CashierSessionStats.points_as_cashin);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", "");
        AddString("VOUCHER_CASH_DESK_CLOSE_RECHARGE_FOR_POINTS", "");
      }

      //Service Charge
      if (CashierSessionStats.service_charge > 0)
      {
        _str_value = Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE") + ":";
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", _str_value);
        AddCurrency("VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", CashierSessionStats.service_charge);
      }
      else
      {
        AddString("STR_VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", "");
        AddString("VOUCHER_CASH_DESK_CLOSE_SERVICE_CHARGE", "");
      }

      AddString("STR_VOUCHER_CASH_DESK_CLOSE_CASH_NET", Resource.String("STR_VOUCHER_CASH_DESK_CLOSE_CASH_NET") + ":");
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_CASH_NET", CashierSessionStats.final_balance);

      //  6. Premios
      signed_amount = CashierSessionStats.prize_gross; // *-1;
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_WON_AMT", signed_amount);

      // 7. Impuestos
      AddString("VOUCHER_CASH_DESK_CLOSE_TAX_TAXES_NUM", " ");
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_TAX_TAXES_AMT", CashierSessionStats.prize_taxes);

      //  8. Premios
      signed_amount = CashierSessionStats.prize_payment; // *-1;
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_CASH_OUT_WON_PAYED_AMT", signed_amount);

      //  9. Handpay
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_PAYED_AMT", CashierSessionStats.handpay_payment);
      signed_amount = CashierSessionStats.handpay_canceled; // *-1;
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_CANCELED_AMT", signed_amount);
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_HANDPAY_TOTAL_AMT", CashierSessionStats.handpay_payment - CashierSessionStats.handpay_canceled);

      // 10. Promotions
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_PROMO_TO_REDEEM_AMT", CashierSessionStats.promo_nr_to_re);
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_NO_REDEEM_AMT", CashierSessionStats.promo_nr);

      // 11. Tickets TITO
      AddInteger("VOUCHER_CASH_DESK_CLOSE_MOV_TOTAL_CANCELLED_ACCOUNT", CashierSessionStats.tickets_cancelled_count);
      AddCurrency("VOUCHER_CASH_DESK_CLOSE_MOV_TOTAL_CANCELLED_AMOUNT", CashierSessionStats.tickets_cancelled_amount);

      Voucher _voucher;
      _voucher = new WSI.Common.VoucherCurrencyCash(CashierSessionStats, false);

      SetParameterValue("@VOUCHER_CURRENCY_CASH", _voucher.VoucherHTML);
      //_voucher.VoucherHTML

      AddString("STR_MB_DEPOSITS_VISIBLE", _is_tito_mode ? "hidden" : "");
      AddString("STR_TITO_VISIBLE", _is_tito_mode ? "" : "hidden");

    } // SetupCashStatusVoucher
    #endregion
  }
}
