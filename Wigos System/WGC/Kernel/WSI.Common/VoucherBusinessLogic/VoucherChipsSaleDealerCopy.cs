﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherChipsSaleDealerCopy.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
// 14-NOV-2016  EOR         PBI 19963: MES08: Ticket Copy Dealer the amount sale resize by general parameter
// 24-MAR-2017  RAB         PBI 25974: MES10 Ticket validation - Dealer copy voucher
// 26-FEB-2018  DHA         Bug 31730:WIGOS-8398 [Ticket #12336] Mesas
// 28-FEB-2018  JML         Fixed Bug 31758:WIGOS-8398 [Ticket #12336] Mesas
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherChipsSaleDealerCopy : Voucher
  {
    #region Class Attributes

    #endregion

    #region Constructor

    public VoucherChipsSaleDealerCopy(String[] VoucherAccountInfo
                              , TYPE_SPLITS Splits
                              , FeatureChips.ChipsOperation ChipsAmount
                              , String BarCodeExternal
                              , PrintMode Mode
                              , DateTime ExpirationDate                        
                              , SqlTransaction Trx)
      : base(Mode, Trx)
    {

      // Configured only for one print
      m_voucher_params.PrintCopy = 1;
      NumPendingCopies = m_voucher_params.PrintCopy;

      // 1. Load HTML structure.
      LoadVoucher("VoucherChipsSaleDealerCopy");

      //  4. Load NLS strings from resource
      LoadVoucherResources(VoucherAccountInfo, Splits, ChipsAmount);

      // DHA 23-JUN-2015: print barcode and expiration date
      PrintBarcode(BarCodeExternal, ExpirationDate);

      PrintSignatures();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS.
    /// </summary>
    private void LoadVoucherResources(String[] VoucherAccountInfo
                                    , TYPE_SPLITS Splits
                                    , FeatureChips.ChipsOperation ChipsAmount)
    {
      String _str_value;
      StringBuilder _sb_chips_denomination;

      _str_value = "";
      _sb_chips_denomination = new StringBuilder();

      //  2. Load Header and Footer
      LoadHeader(Splits.company_a.header);
      LoadFooter(Splits.company_a.footer, Splits.company_a.footer_cashin);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      SetValue("CV_TYPE", (Int32)CashierVoucherType.ChipsSaleDealerCopy);

      if (!String.IsNullOrEmpty(ChipsAmount.SelectedGamingTable.TypeName) && !String.IsNullOrEmpty(ChipsAmount.SelectedGamingTable.TableName))
      {
        AddString("VOUCHER_GAMING_TABLE_NAME", ChipsAmount.SelectedGamingTable.TypeName + " - " + ChipsAmount.SelectedGamingTable.TableName);
      }
      else
      {
        AddString("VOUCHER_GAMING_TABLE_NAME", "");
      }

      _str_value = Resource.String("STR_VOUCHER_CASH_CHIPS_SALE_TITLE");
      AddString("STR_VOUCHER_CASH_CHIPS_TITLE", _str_value);

      _str_value = Resource.String("STR_VOUCHER_CARD_REDEEM_TOTAL");
      AddString("STR_VOUCHER_CASH_CHIPS_TOTAL", _str_value);

      SetParameterValue("@VOUCHER_CASH_CHIPS_TOTAL", GetFormatWithSizeAmount(ChipsAmount.ChipsAmount, ChipsAmount.IsoCode));

      AddString("STR_VOUCHER_CASH_CHIPS_DEALER_COPY", GeneralParam.GetString("GamingTables", "Cashier.Mode.DealerCopy.Title"));

    }

    private string GetFormatWithSizeAmount(Currency Amount, string IsoCode)
    {
      int _SizePx;
      string _amount;
      NumberFormatInfo _nfi;

      _nfi = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat;

      _SizePx = GeneralParam.GetInt32("GamingTables", "Cashier.Mode.DealerCopy.SizeAmount", 14);
      _SizePx = Math.Max(_SizePx, 14);
      _SizePx = Math.Min(_SizePx, 40);

      if (_SizePx != 14)
      {
        _amount = Amount.ToString();

        if (!String.IsNullOrEmpty(_nfi.CurrencySymbol))
        {
          _amount = _amount.Replace(_nfi.CurrencySymbol, "");
        }

        return "<span style='font-size:" + _SizePx.ToString() +"px'>" + _amount + "</span>" + " " + IsoCode;
      }

      return Currency.Format(Amount, IsoCode);
    }

    /// <summary>
    /// Print signatures if actived in General Params.
    /// </summary>
    private void PrintSignatures()
    {
      if (GeneralParam.GetBoolean("GamingTables", "Cashier.Mode.DealerCopy.Signature"))
      {
        SetParameterValue("@VOUCHER_SIGNATURE_DEALER",
          "<table border='0' cellpadding='0' cellspacing='0' width='100%'>" +
            "<tr>" +
              "<td></br><b>" + Resource.String("STR_VOUCHER_AUTHORIZED_BY_SIGNATURE") + ": " + "</b></td>" +
            "</tr>" +
            "<tr style='height:80px'><td>&nbsp;</td></tr>" +
          "</table>" +
          "<hr noshade size='4'>");
      }
      else
      {
        AddString("VOUCHER_SIGNATURE_DEALER", "");
      }
    }

    /// <summary>
    /// Print barcode and expiration date if actived in General Params.
    /// </summary>
    private void PrintBarcode(String BarCodeExternal, DateTime ExpirationDate)
    {
      String _style;
      String _style_barcode;
      String _barcode_data;
      String _expiration_date;
      String _barcode_size;
      Int32 _barcode_witek;

      _style = String.Empty;
      _style_barcode = String.Empty;
      _barcode_data = String.Empty;
      _expiration_date = String.Empty;

      _barcode_size = "<script type='text/javascript' src='barcode_small.js'></script>";

      Int32.TryParse(Environment.GetEnvironmentVariable("LKS_WITEK_BARCODE_READER"), out _barcode_witek);

      if(_barcode_witek > 0)
      {
        _barcode_size = "<script type='text/javascript' src='barcode.js'></script>";
      }

      _barcode_data += "<br />";
      _barcode_data += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">";
      _barcode_data += "  <tr>";
      _barcode_data += "    <td colspan=\"2\" align=\"center\">";
      _barcode_data += "      <br />";
      _barcode_data += "      <div id='inputdata'/>";
      _barcode_data += "     </td>";
      _barcode_data += "  </tr>";
      _barcode_data += "</table>";

      if (GamingTableBusinessLogic.GetDealerCopyTicketsExpirationDays() > 0)
      {
        _expiration_date += "<br />";
        _expiration_date += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">";
        _expiration_date += "  <tr>";
        _expiration_date += "    <td>";
        _expiration_date += "     <b>" + Resource.String("STR_VOUCHER_CARD_ADD_PROMOTION_EXPIRED_DATE") + ": </b>" + ExpirationDate.ToString();
        _expiration_date += "   </td>";
        _expiration_date += " </tr>";
        _expiration_date += "</table>";
      }

      _style_barcode = "<script type='text/javascript'>$('#inputdata').barcode('#PAYMENT_BARCODE_ORDER_ID#', 'int25');</script>";
      _style_barcode = _style_barcode.Replace("#PAYMENT_BARCODE_ORDER_ID#", BarCodeExternal);

      SetParameterValue("@VOUCHER_BARCODE_SIZE", _barcode_size);
      SetParameterValue("@VOUCHER_BARCODE_STYLE", _style_barcode);
      SetParameterValue("@VOUCHER_EXPIRATION_DATE", _expiration_date);
      SetParameterValue("@VOUCHER_BARCODE_DATA", _barcode_data);
    }

    #endregion
  }
}
