﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherReception.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherReception : Voucher
  {

    #region Constructor

    public VoucherReception(String[] VoucherAccountInfo
                            , CashierVoucherType VoucherType
                            , Int64 AccountId
                            , Currency[] Amount
                            , CurrencyExchangeResult ExchangeResult
                            , String Coupon
                            , Int32 ValidGamingDays
                            , PrintMode Mode
                            , SqlTransaction SQLTransaction)
      : base(Mode, VoucherType, SQLTransaction)
    {
      InitializeVoucher(VoucherAccountInfo, AccountId, Amount, ExchangeResult, Coupon, ValidGamingDays);

      PrintBarcode();
    }

    private void InitializeVoucher(String[] VoucherAccountInfo
                                  , Int64 AccountId
                                  , Currency[] Amount
                                  , CurrencyExchangeResult ExchangeResult
                                  , String Coupon
                                  , Int32 ValidGamingDays)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherReception");

      SetValue("CV_ACCOUNT_ID", AccountId);

      // 2. Load voucher resources.
      LoadVoucherResources(VoucherAccountInfo, Amount, Coupon, ExchangeResult, ValidGamingDays);

      VoucherSequenceId = SequenceId.VoucherEntrances;

      LoadFooter(String.Empty, GeneralParam.GetString("Cashier.Voucher", "Footer"));

      // 3. Load general voucher data.
      LoadVoucherGeneralData(true);

    }

    #endregion
    private string EncodeHTMLSpecialChars(String InputString, Boolean HTMLSpaceChar)
    {
      String _html;

      _html = HttpUtility.HtmlEncode(InputString);

      if (HTMLSpaceChar)
      {
        _html = _html.Replace(" ", "&nbsp;");
      }

      return _html;
    }
    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(String[] VoucherAccountInfo
                                    , Currency[] Amount
                                    , String Coupon
                                    , CurrencyExchangeResult ExchangeResult
                                    , Int32 ValidationGamingDays)
    {
      String value = "";
      String[] _params;
      String _national_currency;
      String _str_aux;
      string _str_dif;
      string _diff;

      _diff = string.Empty;
      _str_dif = "";

      _national_currency = CurrencyExchange.GetNationalCurrency();

      // - Title
      value = Resource.String("STR_VOUCHER_RECEPTION_TITLE");
      AddString("STR_VOUCHER_RECEPTION_TITLE", value);

      //Account Info
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      //Base Price
      value = Resource.String("STR_VOUCHER_RECEPTION_CONCEPT");
      AddString("STR_VOUCHER_RECEPTION_CONCEPT", value);

      //Difference
      value = Resource.String("STR_VOUCHER_RECEPTION_DIF_CONCEPT");
      AddString("STR_VOUCHER_RECEPTION_DIF_CONCEPT", value);


      if (ExchangeResult == null || !ExchangeResult.WasForeingCurrency)
      {
        if (Amount[1] < Amount[0])
        {
          AddCurrency("STR_VOUCHER_RECEPTION_AMOUNT", Amount[0]);
          _diff = EncodeHTMLSpecialChars(Amount[2].ToString(), true);
        }
        else
        {
          AddCurrency("STR_VOUCHER_RECEPTION_AMOUNT", Amount[1]);
        }
        //Paid Price 
        AddCurrency("STR_VOUCHER_RECEPTION_TOTAL_AMOUNT", Amount[1]);
        AddString("VOUCHER_CHANGE_APPLIED", String.Empty);
      }
      else
      {
        if (Amount[1] < Amount[0])
        {
          _diff = Currency.Format(Amount[2], ExchangeResult.InCurrencyCode);
          AddString("STR_VOUCHER_RECEPTION_AMOUNT", Currency.Format(Amount[0], ExchangeResult.InCurrencyCode));
        }
        else
        {
          AddString("STR_VOUCHER_RECEPTION_AMOUNT", Currency.Format(Amount[1], ExchangeResult.InCurrencyCode));
        }

        //Paid Price 
        AddString("STR_VOUCHER_RECEPTION_TOTAL_AMOUNT", Currency.Format(Amount[1], ExchangeResult.InCurrencyCode));

        _str_aux = Resource.String("STR_VOUCHER_APPLIED_EXCHANGE");

        SetParameterValue("@VOUCHER_CHANGE_APPLIED",
          "<tr>" +
          "<td align=\"left\" >" + _str_aux + " " + ExchangeResult.InCurrencyCode + ":</td>" +
          "<td align=\"right\" >" + CurrencyExchange.ChangeRateFormat(ExchangeResult.ChangeRate) + " " +
          _national_currency +
          "</td>" +
          "</tr>" +
          "<tr>" +
          "<td align=\"left\" >" + Resource.String("STR_FRM_AMOUNT_INPUT_EQUALS") + "</td>" +
          "<td align=\"right\" >" + Currency.Format(ExchangeResult.InAmount, _national_currency) +
          "</td>" +
          "</tr>");


      }
      if (!string.IsNullOrEmpty(_diff))
      {
        _str_dif = "<tr>" +
                   "<td align=\"left\" style=\"width: 40%\">" +
                    Resource.String("STR_VOUCHER_RECEPTION_DIF_CONCEPT") +
                   "</td>" +
                   "<td align=\"right\" width=\"60%\">" +
                  _diff +
                   "</td>" +
                   "</tr>";
      }

      SetParameterValue("@VOUCHER_DIFFERENCE", _str_dif);

      if (!String.IsNullOrEmpty(Coupon))
      {
        value = Resource.String("STR_FRM_RECEPTION_TICKET_COUPON_NUMBER") + " " + Coupon;
      }
      else
      {
        value = String.Empty;
      }
      AddString("STR_VOUCHER_RECEPTION_COUPON", value);

      //Comissions 
      value = String.Empty;//Resource.String("STR_VOUCHER_RECEPTION_COMSIONS");
      AddString("STR_VOUCHER_RECEPTION_COMSIONS", value);

      //TOTAL text
      value = Resource.String("STR_VOUCHER_RECEPTION_TOTAL");
      AddString("STR_VOUCHER_RECEPTION_TOTAL_TEXT", value);

      //Expiration 
      _params = new String[1];
      _params[0] = Entrance.GetExpirationDate(ValidationGamingDays).ToShortDateString();
      value = Resource.String("STR_VOUCHER_EXPIRATION_TEXT", _params);
      AddString("STR_VOUCHER_EXPIRATION_TEXT", value);

    } // LoadVoucherResources

    private void PrintBarcode()
    {
      Barcode _barcode;
      String _style;
      String _style_barcode;
      String _barcode_data;


      //Cashier Session Info: Cashier.CashierSessionInfo();  

      _barcode = Barcode.Create(BarcodeType.Reception, VoucherSeqNumber);

      _style = String.Empty;
      _style_barcode = String.Empty;
      _barcode_data = String.Empty;

      _barcode_data += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">";
      _barcode_data += "  <tr>";
      _barcode_data += "    <td colspan=\"2\" align=\"center\">";
      _barcode_data += "      <div id='inputdata'/>";
      _barcode_data += "     </td>";
      _barcode_data += "  </tr>";
      _barcode_data += "</table>";

      _style_barcode = "<script type='text/javascript'>$('#inputdata').barcode('#PAYMENT_BARCODE_ORDER_ID#', 'int25');</script>";
      _style_barcode = _style_barcode.Replace("#PAYMENT_BARCODE_ORDER_ID#", _barcode.ExternalCode);

      SetParameterValue("@VOUCHER_BARCODE_STYLE", _style_barcode);
      SetParameterValue("@VOUCHER_BARCODE_DATA", _barcode_data);
    }

    #endregion

  }
}
