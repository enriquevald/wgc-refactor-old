﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherTransferCashiersSent.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherTransferCashiersSent : Voucher
  {

    #region Constructor

    public VoucherTransferCashiersSent(CashierVoucherType CashierVoucherType, CashierSessionInfo SourceCashierSession, CashierSessionInfo TargetCashierSession, PrintMode Mode, SqlTransaction SQLTransaction)
      : base(Mode, CashierVoucherType, SQLTransaction)
    {
      InitializeVoucher(SourceCashierSession, TargetCashierSession);
    }

    private void InitializeVoucher(CashierSessionInfo SourceCashierSession, CashierSessionInfo TargetCashierSession)
    {
      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherTransferCashiersSent");

      // 2. Load voucher resources.
      LoadVoucherResources(SourceCashierSession, TargetCashierSession);

      // 3. Load general voucher data.
      LoadVoucherGeneralData();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Load voucher resources: Images, NLS, etc.
    /// </summary>
    private void LoadVoucherResources(CashierSessionInfo SourceCashierSession, CashierSessionInfo TargetCashierSession)
    {
      String value = "";

      // - Title
      value = Resource.String("STR_VOUCHER_CASHIERS_TRANSFER_OUT_TITLE");
      AddString("STR_VOUCHER_CASHIERS_TRANSFER_OUT_TITLE", value);

      // - General
      value = Resource.String("STR_VOUCHER_SITE_ID") + ":";
      AddString("STR_VOUCHER_SITE_ID", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ":";
      AddString("STR_VOUCHER_TERMINAL_USERNAME", value);

      value = Resource.String("STR_VOUCHER_AUTHORIZED_BY") + ":";
      AddString("STR_VOUCHER_AUTHORIZED_BY", value);

      value = Resource.String("STR_VOUCHER_TERMINAL_ID") + ":";
      AddString("STR_VOUCHER_TERMINAL_ID", value);

      value = Resource.String("STR_VOUCHER_VOUCHER_ID") + ":";
      AddString("STR_VOUCHER_VOUCHER_ID", value);

      // - Transfer Information
      List<string> _data = new List<string>();
      _data.Add(Resource.String("STR_VOUCHER_CASHIERS_SOURCE_SESSION_NAME"));
      _data.Add("&nbsp;&nbsp;" + Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ": " + SourceCashierSession.UserName);
      _data.Add("&nbsp;&nbsp;" + Resource.String("STR_VOUCHER_TERMINAL_ID") + ": " + SourceCashierSession.TerminalName);
      _data.Add(Resource.String("STR_VOUCHER_CASHIERS_TARGET_SESSION_NAME"));
      _data.Add("&nbsp;&nbsp;" + Resource.String("STR_VOUCHER_TERMINAL_USERNAME") + ": " + TargetCashierSession.UserName);
      _data.Add("&nbsp;&nbsp;" + Resource.String("STR_VOUCHER_TERMINAL_ID") + ": " + TargetCashierSession.TerminalName);
      AddMultipleLineString("CASHIER_SESSION_SOURCE_TARGET", _data.ToArray(), false);

    } // LoadVoucherResources

    #endregion

  }
}
