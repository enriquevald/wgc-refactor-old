﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherCashOutCompanyB.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
// 11-JAN-2016  ETP         Fixed Bug 22543 Prize_coupon not aplicable if cashdesk enabled.
// 08-MAR-2017  FGB         PBI 25268: Third TAX - Payment Voucher
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  public class VoucherCashOutCompanyB : Voucher
  {
    #region Constructor

    public VoucherCashOutCompanyB(String[] VoucherAccountInfo
                       , TYPE_SPLITS SplitInformation
                       , TYPE_CASH_REDEEM CompanyB
                       , CASHIER_MOVEMENT MovementType
                       , OperationCode OperationCode
                       , PrintMode Mode
                       , SqlTransaction SQLTransaction)
      : base(Mode, OperationCode, SQLTransaction)
    {
      String _concept_name;
      Currency _concept_value;

      // 1. Load HTML structure.
      switch (MovementType)
      {
        case CASHIER_MOVEMENT.COMPANY_B_CARD_DEPOSIT_OUT:
          {
            LoadVoucher("Voucher.B.CashOut");
            LoadHeader(SplitInformation.company_b.header);
            LoadFooter(SplitInformation.company_b.footer, SplitInformation.company_b.footer_cancel);
            AddString("VOUCHER_SPLIT_NAME", SplitInformation.company_b.cash_out_voucher_title);

            VoucherSequenceId = SequenceId.VouchersSplitB;
            if (Misc.IsVoucherModeCodere())
            {
              VoucherSequenceId = SequenceId.VouchersSplitBCancel;
            }

            SetValue("CV_TYPE", (Int32)CashierVoucherType.CardDepositOut_B);

            _concept_name = Misc.GetCardPlayerConceptName();
            _concept_value = CompanyB.card_deposit;

            SetValue("CV_M01_BASE", CompanyB.card_deposit);
            SetValue("CV_M01_TAX1", CompanyB.tax1);
            SetValue("CV_M01_TAX2", CompanyB.tax2);
            SetValue("CV_M01_TAX3", CompanyB.tax3);
            SetValue("CV_M01_DEV", CompanyB.TotalDevolution);
          }

          break;

        default:
          {
            Log.Error("VoucherCashOutCompanyB. MovementType not valid: " + MovementType.ToString());
            return;
          }
      }

      // 2. Load general voucher data.
      LoadVoucherGeneralData();
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      // 3. Load specific voucher data.
      AddString("VOUCHER_CONCEPT", _concept_name);
      AddCurrency("VOUCHER_CARD_CASH_OUT_AMOUNT", _concept_value);
      AddCurrency("VOUCHER_CARD_CASH_OUT_TOTAL_AMOUNT", CompanyB.TotalPaid - CompanyB.TotalDevolution);

    } // VoucherCashOutCompanyB

    #endregion
  }
}
