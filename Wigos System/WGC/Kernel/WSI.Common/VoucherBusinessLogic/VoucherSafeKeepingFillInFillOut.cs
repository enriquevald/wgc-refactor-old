﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherSafeKeepingFillInFillOut.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: Ervin Olvera
// 
// CREATION DATE: 21-JUL-2016
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-JUL-2016  EOR         First release. Product Backlog Item 14808: Refactoring of vouchers
//---------------------------------------------------------------------------------------------

using System;
using System.Text;
using System.Data;
using System.IO;
using System.Globalization;
using System.Resources;
using System.Reflection;
using WSI.Common;
using System.Web;
using System.Data.SqlClient;
using System.Collections.Generic;

using Microsoft.Win32;
using System.Collections;
using WSI.Common.TITO;
using System.Drawing;
using WSI.Common.GamingHall;
using WSI.Common.Entrances;
using System.Drawing.Printing;

namespace WSI.Common
{
  //SafeKeeping Voucher (Fill In && Fill Out)
  // Format:
  //          Voucher Logo
  //          Voucher Header
  //
  // Cashier Session:
  // User: 
  // Voucher Id:
  //
  // FillIn or FillOut  Amount
  // Current Balance    Amount
  //
  //         Voucher Footer
  //
  public class VoucherSafeKeepingFillInFillOut : Voucher
  {

    #region Class Attributes

    public class VoucherSafeKeepingFillInFillOutInputParams
    {
      public String CageSession;
      public String GuiUser;
      public Int64 HolderId;
      public String HolderName;
      public OperationCode TypeFillInOutOperation;
      public Currency AmountFillInOut;
      public Currency CurrentBalance;
      public Int64 OperationId;
    }

    #endregion

    #region Constructor


    public VoucherSafeKeepingFillInFillOut(PrintMode Mode,
                                           VoucherSafeKeepingFillInFillOutInputParams InputParams,
                                           SqlTransaction SQLTransaction)
      : base(Mode, SQLTransaction)
    {
      String[] VcInfo;
      String _operation;

      _operation = "";
      VcInfo = new String[4];

      // Constructor actions:
      //  1. Load HTML structure.
      //  2. Load NLS strings from resource.
      //  3. Load descriptions
      //  4. Load Amounts
      //  5. Load general voucher data.

      // 1. Load HTML structure.
      LoadVoucher("VoucherSafeKeepingFillInFillOut");

      // 2. Load voucher resources.
      // - Title
      AddString("STR_VOUCHER_SAFE_KEEPING_TITLE", Resource.String("STR_SAFE_KEEPING_TITLE"));

      // 3. Load descriptions
      AddString("VOUCHER_SAFE_KEEPING_TITLE_BALANCE", Resource.String("STR_SAFE_KEEPING_ACTUAL_BALANCE"));

      switch (InputParams.TypeFillInOutOperation)
      {
        case OperationCode.SAFE_KEEPING_DEPOSIT:
          _operation = Resource.String("STR_SAFE_KEEPING_FILL_IN");

          break;
        case OperationCode.SAFE_KEEPING_WITHDRAW:
          _operation = Resource.String("STR_SAFE_KEEPING_FILL_OUT");

          break;
        default:

          break;
      }

      AddString("VOUCHER_SAFEKEEPING_HOLDER_ID_NAME", Resource.String("STR_FORMAT_GENERAL_NAME_ACCOUNT") + ": " +
                                                      InputParams.HolderId + " - " + InputParams.HolderName);

      AddString("VOUCHER_SAFE_KEEPING_TITLE_FILL_IN_OUT", _operation);

      // 4. Load Amounts
      AddString("VOUCHER_SAFE_KEEPING_AMOUNT_FILL_IN_OUT", InputParams.AmountFillInOut.ToString());
      AddString("VOUCHER_SAFE_KEEPING_AMOUNT_BALANCE", InputParams.CurrentBalance.ToString());

      // 5. Load general voucher data.
      VcInfo[0] = InputParams.CageSession;
      VcInfo[1] = InputParams.GuiUser;
      VcInfo[2] = _operation;
      VcInfo[3] = String.Empty;

      LoadVoucherCageGeneralDataFromGUI(VcInfo);

      SetValue("CV_USER_NAME", InputParams.GuiUser);
      SetValue("CV_AMOUNT", InputParams.AmountFillInOut);
      SetValue("CV_CASHIER_NAME", Environment.MachineName);
    }

    #endregion
  }
}
