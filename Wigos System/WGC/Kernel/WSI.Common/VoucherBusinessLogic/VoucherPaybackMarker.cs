﻿//------------------------------------------------------------------------------
// Copyright © 2016 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: VoucherPaybackMarker.cs
// 
//   DESCRIPTION: Inherited Class Voucher: Logic and Builder Voucher
// 
//        AUTHOR: MArk Stansfield
// 
// CREATION DATE: 20-MAR-2017
// 
// REVISION HISTORY:
// 
// Date         Author      Description
// --------------------------------------------------------------------------------------------
// 21-MAR-2017  MS          First release. 
// 04-AUG-2017  DPC         PBI 25791:CreditLine - Amortize a CreditLine
//---------------------------------------------------------------------------------------------

using System;
using WSI.Common;
using System.Data.SqlClient;
using WSI.Common.CreditLines;

namespace WSI.Common
{ 
  public class VoucherPaybackMarker : Voucher
  {
        #region Constructor
    /// <summary>
    ///    VoucherPaybackMarker
    /// </summary>
    /// <param name="VoucherAccountInfo"></param>
    /// <param name="AmountPaid"></param>
    /// <param name="CreditLineDetails"></param>
    /// <param name="SplitInformation"></param>
    /// <param name="OperationId"></param>
    /// <param name="Mode"></param>
    /// <param name="SQLTransaction"></param>
    public VoucherPaybackMarker(String[] VoucherAccountInfo
                       , Currency AmountPaid
                       , CreditLine CreditLineDetails
                       , TYPE_SPLITS SplitInformation
                       , Int64 OperationId
                       , PrintMode Mode
                       , SqlTransaction SQLTransaction)
      : base(Mode, OperationCode.CREDIT_LINE, SQLTransaction)
    {
      Currency _available_credit = 0;
      LoadVoucher("Voucher.PaybackMarker");

      LoadVoucherGeneralData();


      // 3. Load specific voucher data.
      AddMultipleLineString("VOUCHER_ACCOUNT_INFO", VoucherAccountInfo);

      //Title
      AddString("VOUCHER_SPLIT_NAME", Resource.String("STR_VOUCHER_CREDIT_LINE_PAYBACK_TITLE"));

      //Labels
      AddString("LBL_VOUCHER_CREDIT_PAIDBACK", Resource.String("STR_VOUCHER_CREDIT_LINE_PAYBACK") + ":");
      AddString("LBL_VOUCHER_SPENT", Resource.String("STR_VOUCHER_CREDIT_LINE_CREDIT_TOTAL") + ":");
      AddString("LBL_VOUCHER_CREDIT_AVAILABLE", Resource.String("STR_VOUCHER_CREDIT_LINE_CREDIT_AVAILABLE") + ":");
      AddString("LBL_VOUCHER_TOTAL_CREDIT_LIMIT", Resource.String("STR_VOUCHER_CREDIT_LINE_CREDIT_LIMIT_TOTAL") + ":");

      //Calculated Values
      _available_credit = CreditLineDetails.AvailableAmountNoTTO;
      if (_available_credit < 0) 
      {
        _available_credit = 0;
      }

      //Values
      AddCurrency("VOUCHER_CREDIT_PAIDBACK", AmountPaid);
      AddCurrency("VOUCHER_SPENT", CreditLineDetails.SpentAmount);
      AddCurrency("VOUCHER_CREDIT_AVAILABLE", _available_credit); // Not including TTO, minimum 0
      AddCurrency("VOUCHER_TOTAL_CREDIT_LIMIT", CreditLineDetails.LimitAmount); //Not including TTO

      //AddCurrency("VOUCHER_TOTAL_CREDIT_LIMIT", CreditLineDetails.LimitAmount);
      //AddCurrency("VOUCHER_TTO", CreditLineDetails.ExtensionAmount);
      //AddCurrency("VOUCHER_CREDIT_AVAILABLE", CreditLineDetails.LimitAmount +
      //                                        CreditLineDetails.ExtensionAmount +
      //                                        AmountPaid -
      //                                        CreditLineDetails.SpentAmount);
      //AddCurrency("VOUCHER_NEW_DEBT", CreditLineDetails.SpentAmount - AmountPaid);

      SetSignatureRoomByVoucherType(this.GetType());

    } // VoucherGetMarker

    #endregion
  }
}
