//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems International
//------------------------------------------------------------------------------
// 
//   MODULE NAME : VoucherManager.cs
// 
//   DESCRIPTION : Voucher Operation
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 04-APR-2012 JAB    First release.
// 05-APR-2012 JAB    Add routines to view Vouchers
// 13-APR-2012 JAB    Changes to move print voucher functions to the project WSI.Common (VoucherManager.cs)
// 16-APR-2012 JAB    Add functionality to choice print to GUI document.
// 13-JUN-2012 RCI    FontSetup: Added printer POS-X Thermal Printer.
// 10-AUG-2012 ACC    Use HtmlPrinter class for printing html vouchers 
// 05-APR-2013 ICS    Added a funtion to audit voucher reprint
// 19-FEB-2014 YNM    Fixed Bug WIG-1894:  Changes that allows to list the annulled tickets in "Cash Desk voucher report"
// -----------------------------------------------------------------------------
//
using WSI.Common;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading;
using System.Drawing.Printing;
using System.Collections.Generic;

namespace WSI.Common
{
  // Class to control printing: pause/continue, cancel while printing, show printing message status (current/total)
  public class PrintParameters
  {
    public Boolean m_printing;
    public Int32 m_current;
    public Int32 m_total;
    public Boolean m_pause;
    public Boolean m_cancel;
    public Boolean m_print_end;
    public String m_printer_name;
    public PaperSize m_paper_size;
  } // PrintParameters

  public class VoucherManager
  {
    public class AuditInfo
    {
      public Int64 Id;
      public Int64 AccountId;
      public String OperationName;

      public AuditInfo()
      {
        this.Id = 0;
        this.AccountId = 0;
        this.OperationName = String.Empty;
      }
    }

    private static PrintParameters m_print_parameters;
    private static List<String> m_js_copied_path;

    private const Int32 PRINTER_MAX_QUEUE_ITEMS = 1;
    private const Int32 COMMON_MODULE_GUI_PLAYER_TRACKING = 16000;

    #region Public Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Constructor method.
    //           1. Initialize m_print_parameters.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public VoucherManager()
    {
      m_print_parameters = new PrintParameters();
      m_print_parameters.m_printing = false;
      m_print_parameters.m_pause = false;
      m_print_parameters.m_cancel = false;
      m_js_copied_path = new List<String>();
    } // VoucherManager

    //------------------------------------------------------------------------------
    // PURPOSE : Load all the vouchers associated to an OperationId into the web_browser.
    //
    //  PARAMS :
    //      - INPUT :
    //          - OperationId
    //          - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static Boolean LoadVouchers(VoucherSource VoucherSourceP,
                                       Int64 Ident,
                                       Boolean CashierSource,
                                       WebBrowser WebBrowser)
    {
      DataTable _dt_vouchers;
      String _doc;
      String _file_vouchers;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          _dt_vouchers = GetVouchersFromId(VoucherSourceP, Ident, _db_trx.SqlTransaction);

          if (_dt_vouchers == null)
          {
            return false;
          }
          else if (_dt_vouchers.Rows.Count == 0)
          {
            WebBrowser.DocumentText = "<div style=\"font: normal 14px arial\" width=\"270\" align=\"center\"><B>" + Resource.String("STR_FRM_VOUCHER_REPRINT_OPERATION_WITHOUT_TICKETS") + "</B></div>";
            return true;
          }

          _doc = VoucherManager.GetVoucherHTML(_dt_vouchers, CashierSource);
          _file_vouchers = SaveHTMLVoucherOnFile(CashierSource ? "REPRINT" : "GUI_VB_", _doc);

          WebBrowser.Navigate(_file_vouchers);

          _db_trx.Commit();
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // LoadVouchers

    //------------------------------------------------------------------------------
    // PURPOSE: Save HTML string into voucher file to be loaded afterwards.
    //
    //  PARAMS:
    //      - INPUT:
    //          - VoucherHTML
    //          - Index
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public static String SaveHTMLVoucherOnFile(String Prefix, String VoucherHTML)
    {
      FileStream _file_stream;
      StreamWriter _file_writer;
      String _voucher_filename;
      String _temp_path;

      _file_writer = null;

      _voucher_filename = Prefix + "Voucher.html";
      _temp_path = Path.GetTempPath();

      CreateJSFile(_temp_path);

      try
      {
        // Create a temporary file with voucher to print
        _voucher_filename = Path.Combine(_temp_path, _voucher_filename);

        _file_stream = new FileStream(_voucher_filename, FileMode.Create, FileAccess.Write);
        _file_writer = new StreamWriter(_file_stream);

        // Write voucher
        _file_writer.BaseStream.Seek(0, SeekOrigin.Begin);
        _file_writer.Write(VoucherHTML);

      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        if (_file_writer != null)
        {
          _file_writer.Close();
        }
      }

      return _voucher_filename;
    } // SaveHTMLVoucherOnFile

    //------------------------------------------------------------------------------
    // PURPOSE: Get all the Vouchers for the OperationId or the VoucherId, depending on VoucherSource.
    //
    //  PARAMS:
    //      - INPUT:
    //          - VoucherSource
    //          - OperationId
    //          - SqlTrx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public static DataTable GetVouchersFromId(VoucherSource VoucherSourceP,
                                              Int64 Ident,
                                              SqlTransaction SqlTrx)
    {
      DataTable _dt_vouchers;
      StringBuilder _sql_txt;

      try
      {
        _dt_vouchers = new DataTable("VOUCHERS");
        _sql_txt = new StringBuilder();

        switch (VoucherSourceP)
        {
          case VoucherSource.FROM_OPERATION:
            _sql_txt.AppendLine("SELECT   CV_VOUCHER_ID ");
            _sql_txt.AppendLine("       , CV_HTML ");
            _sql_txt.AppendLine("       , CV_TYPE ");
            _sql_txt.AppendLine("       , CV_SEQUENCE ");
            _sql_txt.AppendLine("       , CV_M01_DEV ");
            _sql_txt.AppendLine("       , CV_M01_BASE ");
            _sql_txt.AppendLine("       , CV_M01_TAX1 ");
            _sql_txt.AppendLine("       , CV_M01_TAX2 ");
            _sql_txt.AppendLine("       , CV_M01_FINAL ");
            _sql_txt.AppendLine("  FROM   CASHIER_VOUCHERS ");
            _sql_txt.AppendLine(" WHERE   CV_OPERATION_ID = @pIdent ");
            _sql_txt.AppendLine("ORDER BY 1 ");
            break;

          case VoucherSource.FROM_VOUCHER:
            _sql_txt.AppendLine("SELECT   CV_VOUCHER_ID ");
            _sql_txt.AppendLine("       , CV_HTML ");
            //TODO: PREGUNTAR A YINETH SI ESTO NO ROMPE OTRA COSA DE LA APLICACION
            _sql_txt.AppendLine("       , CV_SEQUENCE ");
            _sql_txt.AppendLine("  FROM   CASHIER_VOUCHERS ");
            _sql_txt.AppendLine(" WHERE   CV_VOUCHER_ID = @pIdent ");
            break;

          default:
            return null;
        }

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_txt.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _sql_cmd.Parameters.Add("@pIdent", SqlDbType.BigInt).Value = Ident;

          using (SqlDataAdapter _sql_adap = new SqlDataAdapter(_sql_cmd))
          {
            _sql_adap.Fill(_dt_vouchers);
          }
        }

        return _dt_vouchers;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return null;
      }
    } // GetVouchersFromOperationId


    //------------------------------------------------------------------------------
    // PURPOSE: Create JavaScript File needed to create barcode
    //
    //  PARAMS:
    //      - INPUT:
    //          - Path
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public static void CreateJSFile(String Directory)
    {
      String _file_path;
      FileStream _file_js;
      StreamWriter _file_writer_js;
      string[] _files_js = { "barcode.js", "barcode_small.js" };

      if (m_js_copied_path == null)
      {
        m_js_copied_path = new List<String>();
      }

      for (int _idx = 0; _idx < _files_js.Length; _idx++)
      {
        _file_writer_js = null;

        _file_path = Path.Combine(Directory, _files_js[_idx]);

        if (!m_js_copied_path.Contains(_file_path) || !System.IO.File.Exists(_file_path))
        {
          try
          {
            _file_js = new FileStream(_file_path, FileMode.Create, FileAccess.Write);
            _file_writer_js = new StreamWriter(_file_js);

            // Write voucher
            _file_writer_js.BaseStream.Seek(0, SeekOrigin.Begin);

            if (_idx == 0)
            {
              _file_writer_js.Write(Resources.Resource.barcode);
            }
            else
            {
              _file_writer_js.Write(Resources.Resource.barcode_small);
            }
            

            if (!m_js_copied_path.Contains(_file_path))
            {
              m_js_copied_path.Add(_file_path);
            }
          }
          catch (Exception ex)
          {
            Log.Exception(ex);
          }
          finally
          {
            if (_file_writer_js != null)
            {
              _file_writer_js.Close();
            }
          }
        }
      }

      
    } //CreateJSFile

    //------------------------------------------------------------------------------
    // PURPOSE: Mounts the ticket (HTML code).
    //
    //  PARAMS:
    //      - INPUT:
    //          - VoucherSource
    //          - OperationId
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private static string GetVoucherHTML(DataTable Vouchers,
                                         Boolean CashierSource)
    {
      String _doc;
      Int32 _idx_row;
      String _color;

      _color = CashierSource ? "Transparent" : "Black";

      try
      {
        _doc = "<table>\n";
        _idx_row = 1;
        foreach (DataRow _row in Vouchers.Rows)
        {
          if (_row[1] != System.DBNull.Value && _row[1] != null && (String)_row[1] != "")
          {
            _doc += "<tr><td>\n" + _row[1] + "</td></tr>\n";
            if (_idx_row < Vouchers.Rows.Count)
            {
              _doc += "<tr><td>\n<hr color='" + _color + "' noshade size='12'>\n</td></tr>\n";
            }
          }
          _idx_row++;
        }
        _doc += "</table>";

        return _doc;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
        return null;
      }
    } // GetVoucherHTML

    //------------------------------------------------------------------------------
    // PURPOSE: Print the vouchers from DataTable Vouchers.
    //          PrintParams has parameters to pause/continue, show printing status message, from the Form
    //          which calls this function.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Vouchers
    //          - LastPrintedVoucherId
    //          - VoucherBrowser
    //          - ref PrintParams
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public static void Print(DataTable Vouchers,
                             Int64 LastPrintedVoucherId,
                             WebBrowser VoucherBrowser,
                             ref PrintParameters PrintParams,
                             SqlTransaction Trx)
    {

      Print(Vouchers, LastPrintedVoucherId, VoucherBrowser, ref PrintParams, Trx, true);

    } // Print

    //------------------------------------------------------------------------------
    // PURPOSE: Print the vouchers from DataTable Vouchers.
    //          PrintParams has parameters to pause/continue, show printing status message, from the Form
    //          which calls this function.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Vouchers
    //          - LastPrintedVoucherId
    //          - VoucherBrowser
    //          - ref PrintParams
    //          - Trx
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    public static void Print(DataTable Vouchers,
                             Int64 LastPrintedVoucherId,
                             WebBrowser VoucherBrowser,
                             ref PrintParameters PrintParams,
                             SqlTransaction Trx,
                             Boolean Reprint)
    {
      ////Int32 _print_time;
      Int64 _voucher_id;
      String _voucher_html;
      String _file_voucher;
      String _doc;
      String _str_is_copy;
      WebBrowser _web_browser;
      Int32 _tick0;
      String _reprint_text;

      if (!VoucherBusinessLogic.GetPrintVoucher() && !(Reprint && GeneralParam.GetBoolean("Cashier.Voucher", "Reprint", true)))
      {
        return;
      }

      // Get parameter EstimatedPrintTime from GeneralParms: in ms.
      ////if (!Int32.TryParse(Misc.ReadGeneralParams("Cashier.Voucher", "EstimatedPrintTime"), out _print_time))
      ////{
      ////  _print_time = 0;
      ////}

      _web_browser = null;
      _file_voucher = "";

      m_print_parameters = PrintParams;

      // Set header, footer and margins settings for internet explorer
      Voucher.PageSetup();

      try
      {
        m_print_parameters.m_printing = true;
        m_print_parameters.m_total = Vouchers.Rows.Count;
        m_print_parameters.m_current = 0;

        foreach (DataRow _row in Vouchers.Rows)
        {
          _voucher_id = (Int64)_row[0];
          _voucher_html = (String)_row[1];

          if (String.IsNullOrEmpty(_voucher_html))
          {
            continue;
          }

          // Filter _voucher_id less or equal than LastPrintedVoucher_id. Don't print them.
          if (LastPrintedVoucherId > 0 && _voucher_id <= LastPrintedVoucherId)
          {
            continue;
          }

          // Pause treatment
          while (m_print_parameters.m_pause)
          {
            Thread.Sleep(10);
            Application.DoEvents();

            // Cancel while in pause mode
            if (m_print_parameters.m_cancel)
            {
              return;
            }
          }

          // Need a different WebBrowser to print each voucher. It's Ok to use a local variable.
          _web_browser = new WebBrowser();

          // Need <table> to view the voucher the same way it was previously showed.
          // If no <table> is used, when printing, an small difference in the location of the voucher inside
          // the web_browser is noticed.

          _doc = "<table>\n";
          _doc += "<tr><td>\n" + _voucher_html + "</td></tr>\n";

          if (Reprint)
          {
            // TJG 20-FEB-2011  Append the configured "Reprint Text" when re-printing the voucher
            _str_is_copy = Misc.ReadGeneralParams("Cashier.Voucher", "ReprintText");

            if (!String.IsNullOrEmpty(_str_is_copy))
            {
              if (_doc.Contains(Voucher.VOUCHER_DIV_REPRINT))
              {
                //07-MAR-2012 MPO Replace the hidden div for the text of reprint
                _reprint_text = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n";
                _reprint_text += "<tr><td align=\"center\"><br/><b>" + System.Web.HttpUtility.HtmlEncode(_str_is_copy) + "</b><br/><br/></td></tr>\n";
                _reprint_text += "</table>";
                _doc = _doc.Replace(Voucher.VOUCHER_DIV_REPRINT, _reprint_text);
              }
              else
              {
                //08-MAR-2012 MPO Maintain the old feature
                _doc += "<tr><td align='center'><br/><b>" + _str_is_copy + "</b></td></tr>\n";
              }
            }
          }

          _doc += "</table>";

          // RCI 02-MAR-2011: Adjust font and voucher size
          _doc = FontSetup(_doc);

          // ACC 10-AUG-2012 Use HtmlPrinter class for printing html vouchers 
          HtmlPrinter.AddHtml(_doc);

          ////////////////_file_voucher = VoucherManager.SaveHTMLVoucherOnFile("MB", _doc);

          ////////////////// Load the document to print.
          ////////////////_web_browser.Navigate(_file_voucher);

          ////////////////if (VoucherBrowser != null)
          ////////////////{
          ////////////////  // Load the document in to the Form WebBrowser to show the voucher that is printing.
          ////////////////  VoucherBrowser.Navigate(_file_voucher);
          ////////////////}

          ////////////////// Wait until the document is completely loaded.
          ////////////////while (_web_browser.ReadyState != WebBrowserReadyState.Complete)
          ////////////////{
          ////////////////  Thread.Sleep(10);
          ////////////////  Application.DoEvents();

          ////////////////  // Cancel while waiting for document load
          ////////////////  if (m_print_parameters.m_cancel)
          ////////////////  {
          ////////////////    return;
          ////////////////  }
          ////////////////}

          m_print_parameters.m_current++;

          ////////////////// RCI & AJQ 13-JUL-2011: Don't use WaitNotTooBusy(). The printer can work wrong and we can stay sleeping inside forever.
          //////////////////// RCI 12-JUL-2011: Wait for the printer to be not too busy.
          //////////////////Printer.WaitNotTooBusy(ref m_print_parameters.m_cancel);

          ////////////////// Print the document when is fully loaded.
          ////////////////// if is GUI document - printer question
          ////////////////_web_browser.Print();

          // RCI & AJQ 13-JUL-2011: Sleep again as before without knowing the number of jobs in the printer queue.
          // Sleep to see message, and allow to pause/continue while printing.

          // ACC 10-AUG-2012 For repaint label "Imprimiendo N de M"
          _tick0 = Misc.GetTickCount();
          while (Misc.GetElapsedTicks(_tick0) < 500
                 || HtmlPrinter.EnqueuedItems() >= PRINTER_MAX_QUEUE_ITEMS)
          {
            Thread.Sleep(10);
            Application.DoEvents();

            // Cancel while in printer wait time
            if (m_print_parameters.m_cancel)
            {
              return;
            }
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return;
      }
      finally
      {
        if (_web_browser != null)
        {
          // Can't dispose now, THE error ocurrs if it does. The system will dispose it when it can.
          //_web_browser.Dispose();
          _web_browser = null;
        }

        m_print_parameters.m_printing = false;
        m_print_parameters.m_pause = false;
        m_print_parameters.m_cancel = false;

        try { File.Delete(_file_voucher); }
        catch { }
      }
    } // PrintPending

    //------------------------------------------------------------------------------
    // PURPOSE: Voucher setup font and voucher size
    //
    //  PARAMS:
    //      - INPUT:
    //          - String HtmlDoc
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - New html document
    // 
    //   NOTES:
    // 
    public static String FontSetup(String HtmlDoc)
    {
      String _new_html_doc;
      PrintDocument _dummy_print_document;
      String _printer_name;
      Int32 _size;
      Int32 _width;

      _new_html_doc = HtmlDoc;

      try
      {
        _dummy_print_document = new PrintDocument();
        _printer_name = _dummy_print_document.PrinterSettings.PrinterName.ToUpper();

        _size = 12;
        _width = 255;

        if (_printer_name.Contains("CUSTOM_SIZE"))
        {
          _size = GeneralParam.GetInt32("Cashier.Voucher", "PrinterCustomSize.Size", 12);
          _width = GeneralParam.GetInt32("Cashier.Voucher", "PrinterCustomSize.Width", 255);
        }

        if (_printer_name == "VENDOR THERMAL PRINTER"
          || _printer_name == "POS-X THERMAL PRINTER"
          || _printer_name.Contains("TSP100")
          || _printer_name.Contains("CUSTOM_SIZE"))
        {
          _new_html_doc = _new_html_doc.Replace("table {font: normal 14px arial}", "table {font: normal " + _size.ToString() + "px arial}");
          _new_html_doc = _new_html_doc.Replace("width=\"270\"", "width=\"" + _width.ToString() + "\"");
          _new_html_doc = _new_html_doc.Replace("width='270'", "width='" + _width.ToString() + "'");
        }
      }
      catch { }

      return _new_html_doc;
    } // FontSetup

    //------------------------------------------------------------------------------
    // PURPOSE: Audit voucher reprint
    //
    //  PARAMS:
    //      - INPUT:
    //          - List<AuditInfo> Vouchers
    //          - ENUM_GUI GuiId
    //          - Int32 UserId
    //          - String UserName
    //          - String ComputerName
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //        - True: if the audit can be registered
    //        - False: Otherwise
    public static Boolean Audit(List<AuditInfo> Vouchers,
                                ENUM_GUI GuiId,
                                Int32 UserId,
                                String UserName,
                                String ComputerName)
    {
      Auditor _auditor;
      Int32 _nls_id;
      String _voucher_id_str;
      String _account_id_str;

      if (Vouchers.Count < 1)
      {
        return false;
      }

      // AUDIT_CODE_REPRINT_VOUCHERS = 41
      _auditor = new Auditor(41);

      if (Vouchers.Count == 1)
      {
        _nls_id = 1850; // "Voucher: %1, Account: %2, Operation: %3"
        _voucher_id_str = Vouchers[0].Id.ToString();
        _account_id_str = Vouchers[0].AccountId.ToString();

        if (String.IsNullOrEmpty(Vouchers[0].OperationName))
        {
          _nls_id = 1851; // "Voucher: %1, Account: %2"

          if (Vouchers[0].AccountId == 0)
          {
            _nls_id = 1852; // "Voucher: %1"
            _account_id_str = String.Empty;
          }
        }
        _auditor.SetName(COMMON_MODULE_GUI_PLAYER_TRACKING + _nls_id, _voucher_id_str, _account_id_str, Vouchers[0].OperationName, "", "");
      }
      else
      {
        foreach (AuditInfo _voucher in Vouchers)
        {
          _nls_id = 1850; // "Voucher: %1, Account: %2, Operation: %3"
          _voucher_id_str = _voucher.Id.ToString();
          _account_id_str = _voucher.AccountId.ToString();

          if (String.IsNullOrEmpty(_voucher.OperationName))
          {
            _nls_id = 1851; // "Voucher: %1, Account: %2"

            if (_voucher.AccountId == 0)
            {
              _nls_id = 1852; // "Voucher: %1"
              _account_id_str = String.Empty;
            }
          }
          _auditor.SetField(COMMON_MODULE_GUI_PLAYER_TRACKING + _nls_id, _voucher_id_str, _account_id_str, _voucher.OperationName, "", "");
        }

        // Multiple reprint
        _auditor.SetName(COMMON_MODULE_GUI_PLAYER_TRACKING + 1849, "", "", "", "", "");
      }

      return _auditor.Notify(GuiId, UserId, UserName, ComputerName);
    }

    #endregion Public Functions
  }
}