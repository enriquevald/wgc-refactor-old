//------------------------------------------------------------------------------
// Copyright � 2011 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Printer.cs
// 
//   DESCRIPTION: 
//
//        AUTHOR: RCI
// 
// CREATION DATE: 12-JUL-2011
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-JUL-2011 RCI     First release.
// 10-FEB-2011 MPO     Methods for print a pdf file
// 02-JUN-2014 ACC     Check PRINTER_ATTRIBUTE_WORK_OFFLINE 0x400
//
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing.Printing;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Diagnostics;
using System.IO;
using System.Management;

namespace WSI.Common
{
  public static class Printer
  {
    public enum Status
    {
      Ready = 0,
      OfflineOrError = 2,
      OutOfPaper = 16,

      Unknown = 99
    }

    #region DllImport

    [DllImport("winspool.drv", EntryPoint = "OpenPrinterA", SetLastError = true,
      CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
    private static extern Boolean OpenPrinter([MarshalAs(UnmanagedType.LPStr)] String szPrinter, out IntPtr hPrinter, Int32 pDefault);

    [DllImport("winspool.drv", SetLastError = true)]
    private static extern Boolean ClosePrinter(IntPtr hPrinter);

    [DllImport("winspool.drv", SetLastError = true)]
    private static extern Boolean GetPrinter(IntPtr hPrinter, Int32 dwLevel, IntPtr pPrinter, Int32 dwBuf, ref Int32 dwNeeded);

    [DllImport("winspool.drv", CharSet = CharSet.Auto, SetLastError = true)]
    private static extern Boolean SetDefaultPrinter(String Name); 

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    private struct PRINTER_INFO_2
    {
      [MarshalAs(UnmanagedType.LPTStr)]
      public string pServerName;
      [MarshalAs(UnmanagedType.LPTStr)]
      public string pPrinterName;
      [MarshalAs(UnmanagedType.LPTStr)]
      public string pShareName;
      [MarshalAs(UnmanagedType.LPTStr)]
      public string pPortName;
      [MarshalAs(UnmanagedType.LPTStr)]
      public string pDriverName;
      [MarshalAs(UnmanagedType.LPTStr)]
      public string pComment;
      [MarshalAs(UnmanagedType.LPTStr)]
      public string pLocation;
      public IntPtr pDevMode;
      [MarshalAs(UnmanagedType.LPTStr)]
      public string pSepFile;
      [MarshalAs(UnmanagedType.LPTStr)]
      public string pPrintProcessor;
      [MarshalAs(UnmanagedType.LPTStr)]
      public string pDatatype;
      [MarshalAs(UnmanagedType.LPTStr)]
      public string pParameters;
      public IntPtr pSecurityDescriptor;
      public uint Attributes;
      public uint Priority;
      public uint DefaultPriority;
      public uint StartTime;
      public uint UntilTime;
      public uint Status;
      public uint cJobs;
      public uint AveragePPM;
    }

    [DllImport("user32.dll")]
    private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
    private const int SW_MINIMIZE = 6;
    private const int SW_MAXIMIZE = 3;
    private const int SW_RESTORE = 9;

    #endregion //DllImport

    const Int32 MAX_JOBS_IN_QUEUE = 10;

    static String m_printer_name = "";
    //////static Int32 m_tick0 = 0;

    //------------------------------------------------------------------------------
    // PURPOSE: Wait for the default printer to be not too busy (less than 5 pending jobs).
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Boolean
    // 
    public static Boolean WaitNotTooBusy()
    {
      Boolean _cancel_dummy;

      _cancel_dummy = false;

      return WaitNotTooBusy(ref _cancel_dummy);
    } // WaitNotTooBusy

    //------------------------------------------------------------------------------
    // PURPOSE: Wait for the default printer to be not too busy (less than 5 pending jobs).
    //          Cancel can be modified from outside. If Cancel = true, then exit.
    // 
    //  PARAMS:
    //      - INPUT:
    //          - Boolean Cancel
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Boolean
    // 
    public static Boolean WaitNotTooBusy(ref Boolean Cancel)
    {
      IntPtr _handle;
      Int32 _bytes_needed;
      IntPtr _addr;
      PRINTER_INFO_2 _info2;
      Int32 _num_jobs;
      Int32 _wait_time;
      Int32 _tick0;

      _num_jobs = -1;

      _handle = IntPtr.Zero;
      _addr = IntPtr.Zero;

      try
      {
        if (String.IsNullOrEmpty(m_printer_name))
        {
          PrintDocument _dummy_print_document;

          _dummy_print_document = new PrintDocument();
          m_printer_name = _dummy_print_document.PrinterSettings.PrinterName;
        }

        if (!OpenPrinter(m_printer_name, out _handle, 0))
        {
          return false;
        }

        _bytes_needed = 0;

        // Get the size of the buffer PRINTER_INFO_2.
        GetPrinter(_handle, 2, IntPtr.Zero, 0, ref _bytes_needed);

        if (_bytes_needed == 0)
        {
          return false;
        }

        _addr = Marshal.AllocHGlobal(_bytes_needed);

        do
        {
          if (Cancel)
          {
            break;
          }

          if (!GetPrinter(_handle, 2, _addr, _bytes_needed, ref _bytes_needed))
          {
            return false;
          }

          _info2 = (PRINTER_INFO_2)Marshal.PtrToStructure(_addr, typeof(PRINTER_INFO_2));
          _num_jobs = (Int32)_info2.cJobs;

          // ACC 14-AUG-2012 The queue printer is implemented in HtmlPrinter class.
          _num_jobs += HtmlPrinter.EnqueuedItems();

          if (_num_jobs >= MAX_JOBS_IN_QUEUE)
          {
            _wait_time = 100;

            _tick0 = Misc.GetTickCount();
            while (Misc.GetElapsedTicks(_tick0) < _wait_time)
            {
              System.Threading.Thread.Sleep(10);
              Application.DoEvents();

              if (Cancel)
              {
                break;
              }
            }
          }

        } while (_num_jobs >= MAX_JOBS_IN_QUEUE);

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_addr != IntPtr.Zero)
        {
          Marshal.FreeHGlobal(_addr);
        }
        if (_handle != IntPtr.Zero)
        {
          ClosePrinter(_handle);
        }
      }
    } // WaitNotTooBusy

    //------------------------------------------------------------------------------
    // PURPOSE: Get Status & Num Jobs in Printer
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //          - pStatus
    //          - pNumJobs
    //
    // RETURNS:
    //      - Boolean
    // 
    public static Boolean GetStatus(out Int32 pStatus, out Int32 pNumJobs)
    {
      IntPtr _handle;
      Int32 _bytes_needed;
      IntPtr _addr;
      PRINTER_INFO_2 _info2;

      pStatus = -1;
      pNumJobs = -1;

      _handle = IntPtr.Zero;
      _addr = IntPtr.Zero;

      try
      {
        if (String.IsNullOrEmpty(m_printer_name))
        {
          PrintDocument _dummy_print_document;

          _dummy_print_document = new PrintDocument();
          m_printer_name = _dummy_print_document.PrinterSettings.PrinterName;
        }

        if (!OpenPrinter(m_printer_name, out _handle, 0))
        {
          //Log.Message("ERROR: llamando a OpenPrinter ");

          return false;
        }

        _bytes_needed = 0;

        // Get the size of the buffer PRINTER_INFO_2.
        GetPrinter(_handle, 2, IntPtr.Zero, 0, ref _bytes_needed);

        if (_bytes_needed == 0)
        {
          //Log.Message("ERROR: llamando a GetPrinter (_bytes_needed) ");

          return false;
        }

        _bytes_needed *= 3;

        _addr = Marshal.AllocHGlobal(_bytes_needed);

        if (!GetPrinter(_handle, 2, _addr, _bytes_needed, ref _bytes_needed))
        {
          //Log.Message("ERROR: llamando a GetPrinter ");

          return false;
        }

        _info2 = (PRINTER_INFO_2)Marshal.PtrToStructure(_addr, typeof(PRINTER_INFO_2));
        pStatus = (Int32)_info2.Status;
        pNumJobs = (Int32)_info2.cJobs;

        // ACC 14-AUG-2012 The queue printer is implemented in HtmlPrinter class.
        pNumJobs += HtmlPrinter.EnqueuedItems();

        //
        // ACC 02-JUN-2014 Check PRINTER_ATTRIBUTE_WORK_OFFLINE 0x400
        //
        if (m_printer_name == "CUSTOM TG2480-H"
            && (_info2.Attributes & 0x400) == 0x400)
        {
          pStatus = 2;
        }

        //Log.Message("OK (" + m_printer_name + "), PrinterStatus: " + pStatus.ToString());
        
        ////Log.Message("OK (" + m_printer_name + "), PrinterStatus: " + pStatus.ToString() + ", " + _info2.pPrinterName + ", " + _info2.Attributes.ToString() + ", " + _info2.cJobs.ToString() + ", " + _info2.Priority.ToString() + ", " + _info2.AveragePPM.ToString());

        ////if (Misc.GetElapsedTicks(m_tick0) > 10000)
        ////{
        ////  String _printer_name = "CUSTOM TG2480-H";
        ////  String _query = String.Format("SELECT * from Win32_Printer WHERE Name LIKE '%{0}'", _printer_name);
        ////  //String _query = String.Format("SELECT * from Win32_Printer");
        ////  ManagementObjectSearcher _searcher = new ManagementObjectSearcher(_query);
        ////  ManagementObjectCollection _coll = _searcher.Get();

        ////  Log.Message("ManagementObjectCollection.Count: " + _coll.Count.ToString());

        ////  foreach (ManagementObject _printer in _coll)
        ////  {
        ////    foreach (PropertyData _property in _printer.Properties)
        ////    {
        ////      Log.Message(_property.Name + ": " + _property.Value);
        ////    }
        ////  }

        ////  m_tick0 = Misc.GetTickCount();
        ////}

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_addr != IntPtr.Zero)
        {
          Marshal.FreeHGlobal(_addr);
        }
        if (_handle != IntPtr.Zero)
        {
          ClosePrinter(_handle);
        }
      }
    } // GetStatus

    //------------------------------------------------------------------------------
    // PURPOSE: Print a pdf file with acrobat reader.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Boolean: Process completed successfully
    // 
    public static Boolean PrintPDF(String FileName) 
    {

      PrintDocument _print_document;
      String _printer_default;
      Boolean _return_process;

      _print_document = new PrintDocument();
      _printer_default=_print_document.PrinterSettings.PrinterName;

      _return_process = PrintPDF(FileName, _printer_default);
      _print_document.Dispose();

      return _return_process;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Print a pdf file with acrobat reader.
    // 
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //      - Boolean: Process completed successfully
    // 
    public static Boolean PrintPDF(String FileName, String Printer)
    {
      RegistryKey _reg;
      String _path_acrord32;
      String _arguments;
      ProcessStartInfo _start_info;
      Process _process_acrord32;
      List<Int32> _list_process_acrord32;
      IntPtr _win_handle;

      try
      {

        // Get the path of AcroRd32.exe 
        _reg = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("Software\\Adobe\\Acrobat Reader\\10.0\\InstallPath");
        if (_reg==null)
          _reg = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("Software\\Adobe\\Acrobat Reader\\9.0\\InstallPath");

        _path_acrord32 = _reg.GetValue("").ToString();

        //Prepare the process to print with arguments
        // Argument available:
        //    /n - Launch a new instance of Reader even if one is already open
        //  * /s - Don't show the splash screen
        //  * /o - Don't show the open file dialog
        //    /h - Open as a minimized window
        //    /p <filename> - Open and go straight to the print dialog
        //  * /t <filename> <printername> <drivername> <portname> - Print the file the specified printer.
        //  * --> USED

        _arguments = "/s /o /t \"" + FileName + "\" \"" + Printer + "\"";
        _start_info = new System.Diagnostics.ProcessStartInfo();
        _start_info.Arguments = _arguments;
        _start_info.FileName = Path.Combine(_path_acrord32, "AcroRd32.exe");
        _start_info.CreateNoWindow = true;
        _start_info.WindowStyle = System.Diagnostics.ProcessWindowStyle.Minimized;
        _start_info.RedirectStandardOutput = true;
        _start_info.UseShellExecute = false;
        _start_info.RedirectStandardOutput = true;

        _process_acrord32 = new System.Diagnostics.Process();
        _process_acrord32.StartInfo = _start_info;

        //
        _list_process_acrord32 = new List<Int32>();

        //Start the print
        _process_acrord32.Start();

        //Minimize all process acrobat reader
        foreach (Process _process in Process.GetProcesses())
        {

          if (_process.ProcessName == "AcroRd32")
          {
            _win_handle = _process.MainWindowHandle;
            ShowWindow(_win_handle, SW_MINIMIZE);
          }

        }

        System.Threading.Thread.Sleep(6000);
        if (!_process_acrord32.HasExited)
        {
          _process_acrord32.WaitForInputIdle();
          _process_acrord32.CloseMainWindow();
          _process_acrord32.Kill();
        }

      }
      catch (Exception _ex)
      {
        Log.Error("Printer.PrinterPDF FileName=" + FileName.ToString());
        Log.Exception(_ex);

        return false;
      }

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Prompts the user to select the printer and return its name.
    // 
    //  PARAMS:
    //      - INPUT: NumCopies
    //
    //      - OUTPUT: 
    //
    // RETURNS:
    //      - Boolean: String (Printer name)
    // 
    public static String GetPrinterName(ref Int16 NumCopies)
    {
      DialogResult _dialog_result;
      PrinterSettings _ps_selected;
      PrinterSettings _ps_default;
      PrintDialog _print_dialog;

      _ps_default = new PrinterSettings();
      _ps_default.Copies = NumCopies;

      _print_dialog = new PrintDialog();
      _print_dialog.PrinterSettings = _ps_default;
      _print_dialog.UseEXDialog = true;
      _print_dialog.PrintToFile = false;
      _print_dialog.AllowPrintToFile = false;
      _print_dialog.AllowSomePages = false;
      _dialog_result = _print_dialog.ShowDialog();

      _ps_selected = _print_dialog.PrinterSettings;

      NumCopies = _ps_selected.Copies;

      // RCI & ICS 16-APR-2014 Free the object
      _print_dialog.Dispose();

      if (_dialog_result == DialogResult.OK)
      {
        return _ps_selected.PrinterName;
      }

      return "";
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Return default printer name.
    // 
    //  PARAMS:
    //      - INPUT: 
    //
    //      - OUTPUT: 
    //
    // RETURNS:
    //      - Boolean: String (Printer name)
    // 
    public static String GetDefaultPrinter()
    {
      PrinterSettings setting = new PrinterSettings();
      return setting.PrinterName;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Set a default printer.
    // 
    //  PARAMS:
    //      - INPUT: String PrinterName (Printer name)
    //
    //      - OUTPUT: 
    //
    // RETURNS:
    //      - Boolean: Boolean (true when the operation is successful)
    // 
    public static Boolean SetPrinterAsDefault(String PrinterName)
    {
      return SetDefaultPrinter(PrinterName);
    }


  } // Printer
} // WSI.Common
