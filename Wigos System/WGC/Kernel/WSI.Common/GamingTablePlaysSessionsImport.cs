//------------------------------------------------------------------------------
// Copyright © 2007-2009 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME:
// 
//   DESCRIPTION: Imports gaming table plays sessions from excel document.
//
//        AUTHOR: David Hernández
// 
// CREATION DATE: 09-FEB-2015
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 09-FEB-2015 DHA    First release.
// 20-OCT-2015 ECP    Backlog Item 1080 GUI - Improvements to Gaming Table Plays Sessions Import
// 23-NOV-2015 FOS    Bug 6771:Payment tickets & handpays
// 15-JUL-2016 RAB    Product Backlog Item 15066: GamingTables (Phase 4): Adapt reports to MultiCurrency
// 31-OCT-2017 RAB    Bug 30497:WIGOS-6175 Gambling tables: Table gaming session import error
// ----------- ------ ----------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Office.Interop.Excel;
using System.IO;
using System.Reflection;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Common;
using Data = System.Data;
using Excel = Microsoft.Office.Interop.Excel;
using System.Diagnostics;
using SpreadsheetGear;

namespace WSI.Common
{
  public class GamingTablePlaysSessionsImport
  {
    #region ENUMS

    public enum IMPORT_STATES
    {
      ReadingDocument = 0,
      DocumentReaded = 1,
      UpdatingDB = 2,
      DBUpdated = 3,
      UpdatingBalances = 4,
      BalancesUpdated = 5,
      Aborted = 6
    }

    public enum REPORT_MODE
    {
      SUMMARY = 0,
      DETAILS = 1,
      ONLY_ERRORS = 3,
    }

    public enum TABLE_COLUMNS
    {
      GamingTableName = 0,
      AccountId = 1,
      Arrival = 2,
      Departure = 3,
      WalkTime = 4,
      CurrentBet = 5,
      PlayedAmount = 6,
      NoPlays = 7,
      MinBet = 8,
      MaxBet = 9,
      ChipsIn = 10,
      ChipsOut = 11,
      BuyIn = 12,
      AwaredPoints = 13,
      IsoCode = 14,
      GamingTableId = 15,
      VirtualTerminalId = 16,
      VirtualAccountId = 17,
      GamingTableSessionId = 18,
      ComputedPoints = 19,
      GameTime = 20,
      AccountName = 21
    }

    #endregion ENUMS

    #region Constants
    private const decimal ROWS_FOR_EVENTS = 2000;
    private const int FIRST_DATA_ROW = 2;
    private String m_file_out;
    #endregion Constants

    #region Constructors

    public GamingTablePlaysSessionsImport(String LogFile)
    {
      this.m_file_out = LogFile;
    }

    # endregion

    #region Events
    public delegate void RowUpdatedEvent(object sender, GamingTablePlaysSessionsImportEventArgs e);
    public event RowUpdatedEvent OnRowUpdated;
    #endregion Events

    #region PrivateFunctions
    //------------------------------------------------------------------------------
    // PURPOSE: Insert gaming table play session.
    //
    //  PARAMS:
    //      - INPUT:
    //          - AccountId
    //          - Points
    //          - Trx
    //
    // RETURNS:
    //      - Boolean: If operation has been set correctly.
    //
    private static Boolean DB_InsertGamingTablePlaysSessions(DataRow GamingTablePlaySession, Int64 OperationId, Int32 CurrentUserId, String CurrentUserName, SqlTransaction Trx)
    {
      StringBuilder _sb;
      int _row_affected;

      try
      {
        // Update account points balance
        if ((Decimal)GamingTablePlaySession[(Int32)TABLE_COLUMNS.AwaredPoints] > 0)
        {
          if (!WSI.Common.GamingTableBusinessLogic.UpdateAccountPoints(OperationId,
                                                                       (Int64)GamingTablePlaySession[(Int32)TABLE_COLUMNS.AccountId],
                                                                       (String)GamingTablePlaySession[(Int32)TABLE_COLUMNS.AccountName],
                                                                       (Decimal)GamingTablePlaySession[(Int32)TABLE_COLUMNS.AwaredPoints],
                                                                       null,
                                                                       CurrentUserName + "@" + Environment.MachineName,
                                                                       String.Empty,
                                                                       MovementType.ImportedPointsForLevel,
                                                                       Trx))
          {
            Log.Message("WSI.Common.GamingTableBusinessLogic.UpdateAccount()");
            return false;
          }
        }

        _sb = new StringBuilder();
        _sb.AppendLine("INSERT INTO   GT_PLAY_SESSIONS ");
        _sb.AppendLine("            ( GTPS_GAMING_TABLE_ID ");
        _sb.AppendLine("            , GTPS_GAMING_TABLE_SESSION_ID ");
        _sb.AppendLine("            , GTPS_STARTED ");
        _sb.AppendLine("            , GTPS_FINISHED ");
        _sb.AppendLine("            , GTPS_ACCOUNT_ID ");
        _sb.AppendLine("            , GTPS_SEAT_ID ");
        _sb.AppendLine("            , GTPS_STATUS ");
        _sb.AppendLine("            , GTPS_PLAYS ");
        _sb.AppendLine("            , GTPS_CURRENT_BET ");
        _sb.AppendLine("            , GTPS_BET_MIN ");
        _sb.AppendLine("            , GTPS_BET_MAX ");
        _sb.AppendLine("            , GTPS_PLAYED_AMOUNT ");
        _sb.AppendLine("            , GTPS_PLAYED_AVERAGE ");
        _sb.AppendLine("            , GTPS_POINTS ");
        _sb.AppendLine("            , GTPS_COMPUTED_POINTS ");
        _sb.AppendLine("            , GTPS_GAME_TIME ");
        _sb.AppendLine("            , GTPS_START_WALK ");
        _sb.AppendLine("            , GTPS_WALK ");
        _sb.AppendLine("            , GTPS_PLAYER_SKILL ");
        _sb.AppendLine("            , GTPS_PLAYER_SPEED ");
        _sb.AppendLine("            , GTPS_FINISHED_USER_ID ");
        _sb.AppendLine("            , GTPS_UPDATED_USER_ID ");
        _sb.AppendLine("            , GTPS_TOTAL_SELL_CHIPS ");
        _sb.AppendLine("            , GTPS_CHIPS_OUT ");
        _sb.AppendLine("            , GTPS_CHIPS_IN ");
        _sb.AppendLine("            , GTPS_NETWIN ");
        _sb.AppendLine("            , GTPS_IMPORT_OPERATION_ID ");
        _sb.AppendLine("            , GTPS_ISO_CODE )");
        _sb.AppendLine("     VALUES ");
        _sb.AppendLine("            ( @pGamingTableId ");
        _sb.AppendLine("            , @pGamingTableSessionId ");
        _sb.AppendLine("            , @pStarted ");
        _sb.AppendLine("            , @pFinished ");
        _sb.AppendLine("            , @pAccountId ");
        _sb.AppendLine("            , @pSeat ");
        _sb.AppendLine("            , @Status ");
        _sb.AppendLine("            , @pPlays ");
        _sb.AppendLine("            , @pCurrentBet ");
        _sb.AppendLine("            , @pBetMin ");
        _sb.AppendLine("            , @pBetMax ");
        _sb.AppendLine("            , @pPlayedAmount ");
        _sb.AppendLine("            , @pPlayedAverage ");
        _sb.AppendLine("            , @pPoints ");
        _sb.AppendLine("            , @pComputedPoints ");
        _sb.AppendLine("            , @pGameTime ");
        _sb.AppendLine("            , @pStartWalk ");
        _sb.AppendLine("            , @pWalk ");
        _sb.AppendLine("            , @pPlayerSkill ");
        _sb.AppendLine("            , @pPlayerSpeed ");
        _sb.AppendLine("            , @pFinishedUserId ");
        _sb.AppendLine("            , @pUpdatedUserId ");
        _sb.AppendLine("            , @pTotalSellChips ");
        _sb.AppendLine("            , @pChipsOut ");
        _sb.AppendLine("            , @pChipsIn ");
        _sb.AppendLine("            , @pNetwin ");
        _sb.AppendLine("            , @pOperationId ");
        _sb.AppendLine("            , @pIsoCode) ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _cmd.Parameters.Add("@pGamingTableId", SqlDbType.Int).Value = GamingTablePlaySession[(Int32)TABLE_COLUMNS.GamingTableId];
          _cmd.Parameters.Add("@pGamingTableSessionId", SqlDbType.BigInt).Value = GamingTablePlaySession[(Int32)TABLE_COLUMNS.GamingTableSessionId];
          _cmd.Parameters.Add("@pStarted", SqlDbType.DateTime).Value = GamingTablePlaySession[(Int32)TABLE_COLUMNS.Arrival];
          _cmd.Parameters.Add("@pFinished", SqlDbType.DateTime).Value = GamingTablePlaySession[(Int32)TABLE_COLUMNS.Departure];
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = GamingTablePlaySession[(Int32)TABLE_COLUMNS.AccountId];
          _cmd.Parameters.Add("@pSeat", SqlDbType.BigInt).Value = -1;
          _cmd.Parameters.Add("@Status", SqlDbType.Int).Value = GTPlaySessionStatus.Closed;
          _cmd.Parameters.Add("@pPlays", SqlDbType.Int).Value = GamingTablePlaySession[(Int32)TABLE_COLUMNS.NoPlays];
          _cmd.Parameters.Add("@pCurrentBet", SqlDbType.Money).Value = GamingTablePlaySession[(Int32)TABLE_COLUMNS.CurrentBet];
          _cmd.Parameters.Add("@pBetMin", SqlDbType.Money).Value = GamingTablePlaySession[(Int32)TABLE_COLUMNS.MinBet];
          _cmd.Parameters.Add("@pBetMax", SqlDbType.Money).Value = GamingTablePlaySession[(Int32)TABLE_COLUMNS.MaxBet];
          _cmd.Parameters.Add("@pPlayedAmount", SqlDbType.Money).Value = GamingTablePlaySession[(Int32)TABLE_COLUMNS.PlayedAmount];
          _cmd.Parameters.Add("@pPlayedAverage", SqlDbType.Money).Value = GamingTablePlaySession[(Int32)TABLE_COLUMNS.CurrentBet];
          _cmd.Parameters.Add("@pPoints", SqlDbType.Money).Value = GamingTablePlaySession[(Int32)TABLE_COLUMNS.AwaredPoints];
          _cmd.Parameters.Add("@pComputedPoints", SqlDbType.Money).Value = GamingTablePlaySession[(Int32)TABLE_COLUMNS.ComputedPoints];
          _cmd.Parameters.Add("@pGameTime", SqlDbType.Int).Value = GamingTablePlaySession[(Int32)TABLE_COLUMNS.GameTime];
          _cmd.Parameters.Add("@pStartWalk", SqlDbType.DateTime).Value = DBNull.Value;
          _cmd.Parameters.Add("@pWalk", SqlDbType.BigInt).Value = GamingTablePlaySession[(Int32)TABLE_COLUMNS.WalkTime];
          _cmd.Parameters.Add("@pPlayerSkill", SqlDbType.Int).Value = GTPlaySessionPlayerSkill.Average;
          _cmd.Parameters.Add("@pPlayerSpeed", SqlDbType.Int).Value = GTPlayerTrackingSpeed.Medium;
          _cmd.Parameters.Add("@pFinishedUserId", SqlDbType.Int).Value = CurrentUserId;
          _cmd.Parameters.Add("@pUpdatedUserId", SqlDbType.Int).Value = CurrentUserId;
          _cmd.Parameters.Add("@pTotalSellChips", SqlDbType.Money).Value = GamingTablePlaySession[(Int32)TABLE_COLUMNS.BuyIn];
          _cmd.Parameters.Add("@pChipsOut", SqlDbType.Money).Value = GamingTablePlaySession[(Int32)TABLE_COLUMNS.ChipsOut];
          _cmd.Parameters.Add("@pChipsIn", SqlDbType.Money).Value = GamingTablePlaySession[(Int32)TABLE_COLUMNS.ChipsIn];
          _cmd.Parameters.Add("@pNetwin", SqlDbType.Money).Value = (Decimal)GamingTablePlaySession[(Int32)TABLE_COLUMNS.ChipsIn] + (Decimal)GamingTablePlaySession[(Int32)TABLE_COLUMNS.BuyIn] - (Decimal)GamingTablePlaySession[(Int32)TABLE_COLUMNS.ChipsOut];
          _cmd.Parameters.Add("@pOperationId", SqlDbType.BigInt).Value = OperationId;
          _cmd.Parameters.Add("@pIsoCode", SqlDbType.NVarChar).Value = GamingTablePlaySession[(Int32)TABLE_COLUMNS.IsoCode];

          _row_affected = _cmd.ExecuteNonQuery();

          if (_row_affected < 1)
          {
            return false;
          }

          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;
    } // DB_InsertGamingTableSessions

    //------------------------------------------------------------------------------
    // PURPOSE: Returns True or False if the Account: not exists, is blocked or is anonymous.
    //
    //  PARAMS:
    //      - INPUT:
    //          - Account: DataRow that we're checking.
    //          - SqlTrx: DataBase Transaction object
    //      - OUT:
    //          - Warning: True if there's a warning.
    //          - WarningMessage: String message.
    //
    // RETURNS:
    //      - Object: CellValue.
    //
    private Boolean CheckAccountId(ref Object Account, out String AccountHolderName, out Boolean Warning, out String WarningMessage, SqlTransaction SqlTrx)
    {
      Object _obj;
      Int64 _account;

      //Initialize out parameters
      AccountHolderName = "";
      Warning = false;
      WarningMessage = String.Empty;

      try
      {
        if (Account == DBNull.Value)
        {
          return true;
        }

        if (!Int64.TryParse(Account.ToString(), out _account))
        {
          Warning = true;
          WarningMessage = Resource.String("STR_GTPS_IMPORT_FIELD_NOT_VALID", Resource.String("STR_GTPS_IMPORT_ACCOUNT_ID"));

          return false;
        }

        // Is anonymous?
        using (SqlCommand _cmd = new SqlCommand("SELECT AC_HOLDER_NAME FROM ACCOUNTS WHERE AC_ACCOUNT_ID = @pAccountId AND AC_HOLDER_LEVEL > 0", SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pAccountId", SqlDbType.BigInt).Value = _account;
          _obj = _cmd.ExecuteScalar();
        }

        if (_obj == null || _obj == DBNull.Value)
        {
          Warning = true;
          WarningMessage = Resource.String("STR_GTPS_IMPORT_FIELD_NOT_VALID", Resource.String("STR_GTPS_IMPORT_ACCOUNT_ID"));

          return false;
        }

        AccountHolderName = (String)_obj;

        return true;
      }
      catch (Exception)
      {
        Warning = true;
        WarningMessage = Resource.String("STR_GTPS_IMPORT_FIELD_NOT_VALID", Resource.String("STR_GTPS_IMPORT_ACCOUNT_ID"));

        return false;
      }
    } // CheckAccountId

    //------------------------------------------------------------------------------
    // PURPOSE: Returns True or False if the are a gaming table session between the dates
    //
    //  PARAMS:
    //      - INPUT:
    //          - Account: DataRow that we're checking.
    //          - SqlTrx: DataBase Transaction object
    //      - OUT:
    //          - Warning: True if there's a warning.
    //          - WarningMessage: String message.
    //
    // RETURNS:
    //      - Object: CellValue.
    //
    private Boolean CheckGamingTableSessionDates(Int32 GamingTableId, DateTime StartSession, DateTime EndSession, out Int64 GamingTableSessionId, SqlTransaction SqlTrx)
    {
      Object _obj;
      StringBuilder _sb;

      //Initialize out parameters
      GamingTableSessionId = 0;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine(" SELECT   GTS_GAMING_TABLE_SESSION_ID ");
        _sb.AppendLine("   FROM   GAMING_TABLES_SESSIONS ");
        _sb.AppendLine("  INNER   JOIN CASHIER_SESSIONS ON CS_SESSION_ID = GTS_CASHIER_SESSION_ID ");
        _sb.AppendLine("  WHERE   GTS_GAMING_TABLE_ID = @pGamingTableId ");
        _sb.AppendLine("    AND   CS_OPENING_DATE <= @pStartSession ");
        _sb.AppendLine("    AND   ISNULL(CS_CLOSING_DATE, GETDATE()) >= @pEndSession ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pGamingTableId", SqlDbType.BigInt).Value = GamingTableId;
          _cmd.Parameters.Add("@pStartSession", SqlDbType.DateTime).Value = StartSession;
          _cmd.Parameters.Add("@pEndSession", SqlDbType.DateTime).Value = EndSession;
          _obj = _cmd.ExecuteScalar();
        }

        if (_obj == null || _obj == DBNull.Value)
        {
          return false;
        }

        GamingTableSessionId = (Int64)_obj;

        return true;
      }
      catch (Exception)
      {
        return false;
      }
    } // CheckGamingTableSessionDates

    //------------------------------------------------------------------------------
    // PURPOSE: Returns True or False if the Table: exist with seats
    //
    //  PARAMS:
    //      - INPUT:
    //          - TableName: DataRow that we're checking.
    //          - SqlTrx: DataBase Transaction object
    //      - OUT:
    //          - TableId:
    //          - VirtualTerminalId:
    //          - VirtualAccountId:
    //          - Warning: True if there's a warning.
    //          - WarningMessage: String message.
    //
    // RETURNS:
    //      - Boolean
    //
    private Boolean CheckTableName(Object TableName, SqlTransaction SqlTrx, out Int32 TableId, out Int32 VirtualTerminalId, out Int64 VirtualAccountId, out Boolean Warning, out String WarningMessage)
    {
      StringBuilder _sb;

      //Initialize out parameters
      TableId = 0;
      VirtualTerminalId = 0;
      VirtualAccountId = 0;

      Warning = false;
      WarningMessage = String.Empty;
      
      //Initialize variables
      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine("WITH     Gam_Tab_Seats AS ( ");
        _sb.AppendLine("    SELECT GTS_GAMING_TABLE_ID, GTS_TERMINAL_ID, GTS_ACCOUNT_ID, ROW_NUMBER() ");
        _sb.AppendLine("                                                                  OVER ( ");
        _sb.AppendLine("                                                                      PARTITION BY GTS_GAMING_TABLE_ID ");
        _sb.AppendLine("                                                                      ORDER BY GTS_TERMINAL_ID, GTS_ACCOUNT_ID ");
        _sb.AppendLine("                                                                  ) AS SEAT_NO ");
        _sb.AppendLine("    FROM GT_SEATS ");
        _sb.AppendLine("   INNER JOIN GAMING_TABLES ON GT_GAMING_TABLE_ID = GTS_GAMING_TABLE_ID ");
        _sb.AppendLine("   WHERE GT_NAME = @pTableName ");
        _sb.AppendLine(") ");

        _sb.AppendLine("SELECT   GT_GAMING_TABLE_ID ");
        _sb.AppendLine("       , GTS_TERMINAL_ID ");
        _sb.AppendLine("       , GTS_ACCOUNT_ID ");
        _sb.AppendLine("  FROM   GAMING_TABLES ");
        _sb.AppendLine(" INNER   JOIN Gam_Tab_Seats    ON GT_GAMING_TABLE_ID = Gam_Tab_Seats.GTS_GAMING_TABLE_ID   AND SEAT_NO    <= 1 ");

        //Is null?
        Warning = (TableName == DBNull.Value);
        if (Warning)
        {
          WarningMessage = Resource.String("STR_GTPS_IMPORT_FIELD_NOT_VALID", Resource.String("STR_GTPS_IMPORT_TABLE_NAME"));

          return false;
        }

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pTableName", SqlDbType.NVarChar).Value = TableName;

          using (SqlDataReader _sql_rd = _cmd.ExecuteReader())
          {
            if (!_sql_rd.Read())
            {
              Warning = true;
              WarningMessage = Resource.String("STR_GTPS_IMPORT_FIELD_NOT_VALID", Resource.String("STR_GTPS_IMPORT_TABLE_NAME"));

              return false;
            }

            TableId = _sql_rd.GetInt32(0);
            VirtualTerminalId = _sql_rd.GetInt32(1);
            VirtualAccountId = _sql_rd.GetInt64(2);
          }
        }

        return true;
      }
      catch (Exception)
      {
        Warning = true;
        WarningMessage = Resource.String("STR_GTPS_IMPORT_FIELD_NOT_VALID", Resource.String("STR_GTPS_IMPORT_TABLE_NAME"));
      }

      return false;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Returns True or False if the Value can be casted to Decimal
    //
    //  PARAMS:
    //      - INPUT:
    //          - Value: object to cast to Decimal
    //          - Field: name of the field (detaill of error message)
    //      - OUT:
    //          - ValidateValue: casted Value
    //          - Warning: True if there's a warning.
    //          - WarningMessage: String message.
    //
    // RETURNS:
    //      - Boolean:
    //
    private Boolean CheckDecimalType(Object Value, String Field, out Object ValidateValue, out Boolean Warning, out String WarningMessage)
    {
      Decimal? _decimal_value;

      return CheckDecimalTypeWithValue(Value, Field, out ValidateValue, out Warning, out WarningMessage, out _decimal_value);
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Returns True or False if the Value can be casted to Decimal
    //
    //  PARAMS:
    //      - INPUT:
    //          - Value: object to cast to Decimal
    //          - Field: name of the field (detaill of error message)
    //      - OUT:
    //          - ValidateValue: casted Value
    //          - Warning: True if there's a warning.
    //          - WarningMessage: String message.
    //          - DecimalValue: Decimal value of the Value object
    //
    // RETURNS:
    //      - Boolean:
    //
    private Boolean CheckDecimalTypeWithValue(Object Value, String Field, out Object ValidateValue, out Boolean Warning, out String WarningMessage, out Decimal? DecimalValue)
    {
      Decimal _decimal_value;

      //Initialize out parameters
      ValidateValue = Value;

      Warning = false;
      WarningMessage = "";

      if (Value == DBNull.Value)
      {
        ValidateValue = 0;
        DecimalValue = 0;
        return true;
      }

      if (!Decimal.TryParse(Value.ToString(), out _decimal_value))
      {
        Warning = true;
        WarningMessage = Resource.String("STR_GTPS_IMPORT_FIELD_NOT_VALID", Field);
        DecimalValue = null;
        return false;
      }

      DecimalValue = _decimal_value;
      if (_decimal_value < 0)
      {
        Warning = true;
        WarningMessage = Resource.String("STR_GTPS_IMPORT_FIELD_NEGATIVE", Field);
        return false;
      }

      //If arrives here is not null, or NaN, or negative.
      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Returns True or False if the Value can be casted to Int32
    //
    //  PARAMS:
    //      - INPUT:
    //          - Value: object to cast to Int32
    //          - Field: name of the field (detaill of error message)
    //      - OUT:
    //          - ValidateValue: casted Value
    //          - Warning: True if there's a warning.
    //          - WarningMessage: String message.
    //
    // RETURNS:
    //      - Boolean:
    //
    private Boolean CheckInt32Type(Object Value, String Field, out Object ValidateValue, out Boolean Warning, out String WarningMessage)
    {
      Int32 int32_value;

      //Initialize out parameters
      ValidateValue = Value;

      Warning = false;
      WarningMessage = "";

      if (Value == DBNull.Value)
      {
        ValidateValue = 0;
        return true;
      }

      if (Int32.TryParse(Value.ToString(), out int32_value))
      {
        if (int32_value < 0)
        {
          Warning = true;
          WarningMessage = Resource.String("STR_GTPS_IMPORT_FIELD_NEGATIVE", Field);
        }
        else
        {
          return true;
        }
      }
      else
      {
        Warning = true;
        WarningMessage = Resource.String("STR_GTPS_IMPORT_FIELD_NOT_VALID", Field);
      }

      return false;
    }

    #endregion PrivateFunctions

    #region PublicFunctions
    //------------------------------------------------------------------------------
    // PURPOSE : Sends the last Event
    //  PARAMS :
    //      - INPUT :
    //
    // RETURNS :     Name of column or "" if there's any error.
    //
    public void setLastEvent()
    {
      GamingTablePlaysSessionsImportEventArgs _rowupdateEvent;

      _rowupdateEvent = new GamingTablePlaysSessionsImportEventArgs();
      _rowupdateEvent.Message = Resource.String("STR_GTPS_IMPORT_CANCELED").Replace("\\r\\n", "\r\n");

      _rowupdateEvent.Row_count = 0;
      _rowupdateEvent.Pending_rows = 0;

      WriteLog(_rowupdateEvent.Message);
      OnRowUpdated(this, _rowupdateEvent);

    }//setLastEvent

    //------------------------------------------------------------------------------
    // PURPOSE : Generate an error message event
    //  PARAMS :
    //      - INPUT :
    //
    // RETURNS :     STR_AC_IMPORT_POINTS_READ_ERROR
    //
    public void setErrorEvent()
    {
      GamingTablePlaysSessionsImportEventArgs _rowupdateEvent;

      _rowupdateEvent = new GamingTablePlaysSessionsImportEventArgs();
      _rowupdateEvent.Message = "\r\n" + Resource.String("STR_AC_IMPORT_POINTS_READ_ERROR").Replace("\\r\\n", "\r\n");

      _rowupdateEvent.Row_count = 0;
      _rowupdateEvent.Pending_rows = 0;

      WriteLog(_rowupdateEvent.Message);
      OnRowUpdated(this, _rowupdateEvent);
    } //setLastEvent

    //------------------------------------------------------------------------------
    // PURPOSE : Open and check a DataTable with accounts points to import. Then, fill a DataSet with correct accounts
    //           and errors.
    //  PARAMS :
    //      - INPUT :
    //          - DataTable: PointsToImport
    //          - Boolean: AllowNegativePoints
    //
    //      - OUTPUT : 
    //          - DataSet: Accounts
    //
    // RETURNS :     True if the process ends, false if there's an error.
    //
    public Boolean GetGamingTablePlaysSessions(System.Data.DataTable GamingTablePlaySessionsToImport, out System.Data.DataSet GamingTablePlaysSessions)
    {
      Int32 _table_id;
      Int32 _virtual_terminal;
      Int64 _virtual_account;
      DateTime _datetime_value;
      TimeSpan _timespan_value;
      Int64 _walk_time;
      DateTime _arrival_date;
      DateTime _departure_date;
      Int64 _gaming_table_session;
      String _warning_message;
      Boolean _warning;
      Object _obj;
      Int32 _elp_mode;
      Decimal _computed_points;
      Decimal _computed_played_amount;
      String _account_name;
      Boolean _set_points_computed_to_awared;
      String _iso_code;
      String _national_currency;
      Int32 _number_of_chips_currencies;
      Decimal? _buy_in;

      //Initialize out parameters
      GamingTablePlaysSessions = null;

      _warning = false;
      _elp_mode = GeneralParam.GetInt32("PlayerTracking.ExternalLoyaltyProgram", "Mode", 0);
      _set_points_computed_to_awared = false;

      if (GamingTablePlaySessionsToImport.Rows.Count == 0)
      {
        return false;
      }

      // Tables from DataSet
      System.Data.DataTable _dt_errors;
      System.Data.DataTable _dt_gaming_table_sessions;

      GamingTablePlaysSessionsImportEventArgs _row_update_event;
      Int32 _correct_gt_play_sessions;
      DataRow _new_row;
      Boolean _insert_row;
      Boolean _is_cashless_mode;

      // Create and configure DataTables from DataSet
      _dt_gaming_table_sessions = new System.Data.DataTable("GamingTablePlaysSessions");
      _dt_gaming_table_sessions.Columns.Add("GamingTableName", typeof(String));
      _dt_gaming_table_sessions.Columns.Add("AccountId", typeof(Int64));
      _dt_gaming_table_sessions.Columns.Add("Arrival", typeof(DateTime));
      _dt_gaming_table_sessions.Columns.Add("Departure", typeof(DateTime));
      _dt_gaming_table_sessions.Columns.Add("WalkTime", typeof(Int64));
      _dt_gaming_table_sessions.Columns.Add("CurrentBet", typeof(Decimal));
      _dt_gaming_table_sessions.Columns.Add("PlayedAmount", typeof(Decimal));
      _dt_gaming_table_sessions.Columns.Add("NoPlays", typeof(Int32));
      _dt_gaming_table_sessions.Columns.Add("MinBet", typeof(Decimal));
      _dt_gaming_table_sessions.Columns.Add("Maxbet", typeof(Decimal));
      _dt_gaming_table_sessions.Columns.Add("ChipsIn", typeof(Decimal));
      _dt_gaming_table_sessions.Columns.Add("ChipsOut", typeof(Decimal));
      _dt_gaming_table_sessions.Columns.Add("BuyIn", typeof(Decimal));
      _dt_gaming_table_sessions.Columns.Add("AwaredPoints", typeof(Decimal));
      _dt_gaming_table_sessions.Columns.Add("IsoCode", typeof(String));
      _dt_gaming_table_sessions.Columns.Add("GamingTableId", typeof(Int32));
      _dt_gaming_table_sessions.Columns.Add("VirtualTerminalId", typeof(Int32));
      _dt_gaming_table_sessions.Columns.Add("VirtualAccountId", typeof(Int32));
      _dt_gaming_table_sessions.Columns.Add("GamingTableSessionId", typeof(Int64));
      _dt_gaming_table_sessions.Columns.Add("ComputedPoints", typeof(Decimal));
      _dt_gaming_table_sessions.Columns.Add("GameTime", typeof(Int64));
      _dt_gaming_table_sessions.Columns.Add("AccountName", typeof(String));

      _dt_errors = new System.Data.DataTable("Errors");
      _dt_errors.Columns.Add("row", typeof(Int32));
      _dt_errors.Columns.Add("message", typeof(String));

      //Configure thread
      System.Globalization.CultureInfo _old_ci = System.Threading.Thread.CurrentThread.CurrentCulture;
      System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-us");

      //Set common values
      _correct_gt_play_sessions = 0;
      _number_of_chips_currencies = CurrencyExchange.GetNumberOfCurrenciesUsedInChips(false);
      _national_currency = CurrencyExchange.GetNationalCurrency();
      _is_cashless_mode = WSI.Common.Misc.IsCashlessMode();

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          foreach (DataRow _row in GamingTablePlaySessionsToImport.Rows)
          {
            _insert_row = true;
            _new_row = _dt_gaming_table_sessions.NewRow();

            if (OnRowUpdated != null && ((GamingTablePlaySessionsToImport.Rows.IndexOf(_row) == GamingTablePlaySessionsToImport.Rows.Count) || ((GamingTablePlaySessionsToImport.Rows.IndexOf(_row) % ROWS_FOR_EVENTS) == 0)))// Create an event, and fill with the current info.
            {
              _row_update_event = new GamingTablePlaysSessionsImportEventArgs();
              _row_update_event.Message = "\r\n" + Resource.String("STR_AC_IMPORT_PROCESSING") + Resource.String("STR_AC_IMPORT_READ_ACCOUNTS") + (GamingTablePlaySessionsToImport.Rows.IndexOf(_row) + 1);

              if (GamingTablePlaySessionsToImport.Rows.IndexOf(_row) != GamingTablePlaySessionsToImport.Rows.Count)
              {
                _row_update_event.Message += "\r\n" + Resource.String("STR_AC_IMPORT_PROCESSING") + Resource.String("STR_AC_IMPORT_READ_ERRORS_FOUND") + _dt_errors.Rows.Count;
              }

              _row_update_event.Row_count = GamingTablePlaySessionsToImport.Rows.Count;
              _row_update_event.Pending_rows = GamingTablePlaySessionsToImport.Rows.Count - GamingTablePlaySessionsToImport.Rows.IndexOf(_row);
              
              if (_row_update_event.Pending_rows <= 0)
              {
                _row_update_event.State = IMPORT_STATES.DocumentReaded;
              }
              else
              {
                _row_update_event.State = IMPORT_STATES.ReadingDocument;
              }

              WriteLog(_row_update_event.Message);
              OnRowUpdated(this, _row_update_event); //Call event, handled byGUI [AddHandler m_Accounts_Points.OnRowUpdated, AddressOf UpdateProcess]
            } // if (OnRowUpdated != null)

            foreach (DataColumn _column in GamingTablePlaySessionsToImport.Columns)
            {
              _warning = false;
              _warning_message = String.Empty;
              _set_points_computed_to_awared = false;

              switch ((TABLE_COLUMNS)GamingTablePlaySessionsToImport.Columns.IndexOf(_column))
              {
                case TABLE_COLUMNS.GamingTableName: // Table Name

                  if (CheckTableName(_row[_column], _db_trx.SqlTransaction, out _table_id, out _virtual_terminal, out _virtual_account, out _warning, out _warning_message))
                  {
                    _new_row[(Int32)TABLE_COLUMNS.GamingTableName] = _row[_column];
                    _new_row[(Int32)TABLE_COLUMNS.GamingTableId] = _table_id;
                    _new_row[(Int32)TABLE_COLUMNS.VirtualTerminalId] = _virtual_terminal;
                    _new_row[(Int32)TABLE_COLUMNS.VirtualAccountId] = _virtual_account;
                  }

                  break;

                case TABLE_COLUMNS.AccountId: // Account Id

                  _obj = _row[_column];
                  _account_name = "";
                  if (CheckAccountId(ref _obj, out _account_name, out _warning, out _warning_message, _db_trx.SqlTransaction))
                  {
                    _new_row[(Int32)TABLE_COLUMNS.AccountId] = _obj;
                  }

                  _new_row[(Int32)TABLE_COLUMNS.AccountName] = _account_name;

                  break;

                case TABLE_COLUMNS.Arrival: // Arrival

                  if (_row[_column] != DBNull.Value && ExcelConversion.ConvertDateTime(_row[_column], out _datetime_value))
                  {
                    _new_row[(Int32)TABLE_COLUMNS.Arrival] = _datetime_value;
                  }
                  else
                  {
                    _warning = true;
                    _warning_message = Resource.String("STR_GTPS_IMPORT_FIELD_NOT_VALID", Resource.String("STR_GTPS_IMPORT_ARRIVAL_DATE"));
                  }

                  break;

                case TABLE_COLUMNS.Departure: // Departure

                  if (_row[_column] != DBNull.Value && ExcelConversion.ConvertDateTime(_row[_column], out _datetime_value))
                  {
                    _new_row[(Int32)TABLE_COLUMNS.Departure] = _datetime_value;
                  }
                  else if (_row[_column] != DBNull.Value)
                  {
                    _warning = true;
                    _warning_message = Resource.String("STR_GTPS_IMPORT_FIELD_NOT_VALID", Resource.String("STR_GTPS_IMPORT_DEPARTURE_DATE"));
                  }

                  break;

                case TABLE_COLUMNS.WalkTime: // Walk time

                  if (_row[_column] != DBNull.Value && ExcelConversion.ConvertDateTime(_row[_column], out _datetime_value))
                  {
                    DateTime _now;

                    _now = WGDB.Now;
                    _timespan_value = _now.Date.AddHours(_datetime_value.Hour).AddMinutes(_datetime_value.Minute).AddSeconds(_datetime_value.Second) - _now.Date;
                    _new_row[(Int32)TABLE_COLUMNS.WalkTime] = _timespan_value.TotalSeconds;
                  }
                  else if (_row[_column] != DBNull.Value)
                  {
                    _warning = true;
                    _warning_message = Resource.String("STR_GTPS_IMPORT_FIELD_NOT_VALID", Resource.String("STR_CASHIER_GAMING_TABLE_GB_AWAY_TIME"));
                  }

                  break;

                case TABLE_COLUMNS.CurrentBet: // Current Bet

                  if (CheckDecimalType(_row[_column], Resource.String("STR_CASHIER_GAMING_TABLE_CURRENT_BET"), out _obj, out _warning, out _warning_message))
                  {
                    _new_row[GamingTablePlaySessionsToImport.Columns.IndexOf(_column)] = _obj;
                  }
                  break;

                case TABLE_COLUMNS.PlayedAmount: // Played Amount

                  if (CheckDecimalType(_row[_column], Resource.String("STR_GTPS_IMPORT_TOTAL_PLAYED"), out _obj, out _warning, out _warning_message))
                  {
                    _new_row[GamingTablePlaySessionsToImport.Columns.IndexOf(_column)] = _obj;
                  }
                  break;

                case TABLE_COLUMNS.NoPlays: // No Plays

                  if (CheckInt32Type(_row[_column], Resource.String("STR_GTPS_IMPORT_NUMBER_PLAYS"), out _obj, out _warning, out _warning_message))
                  {
                    _new_row[GamingTablePlaySessionsToImport.Columns.IndexOf(_column)] = _obj;
                  }
                  break;

                case TABLE_COLUMNS.MinBet: // Min Bet

                  if (CheckDecimalType(_row[_column], Resource.String("STR_CASHIER_GAMING_TABLE_MIN_BET"), out _obj, out _warning, out _warning_message))
                  {
                    _new_row[GamingTablePlaySessionsToImport.Columns.IndexOf(_column)] = _obj;
                  }
                  break;

                case TABLE_COLUMNS.MaxBet: // Max Bet

                  if (CheckDecimalType(_row[_column], Resource.String("STR_CASHIER_GAMING_TABLE_MAX_BET"), out _obj, out _warning, out _warning_message))
                  {
                    _new_row[GamingTablePlaySessionsToImport.Columns.IndexOf(_column)] = _obj;
                  }
                  break;

                case TABLE_COLUMNS.ChipsIn: // Chips In

                  if (CheckDecimalType(_row[_column], Resource.String("STR_CASHIER_GAMING_TABLE_CHIPS_IN"), out _obj, out _warning, out _warning_message))
                  {
                    _new_row[GamingTablePlaySessionsToImport.Columns.IndexOf(_column)] = _obj;
                  }
                  break;

                case TABLE_COLUMNS.ChipsOut: // Chips Out

                  if (CheckDecimalType(_row[_column], Resource.String("STR_CASHIER_GAMING_TABLE_CHIPS_OUT"), out _obj, out _warning, out _warning_message))
                  {
                    _new_row[GamingTablePlaySessionsToImport.Columns.IndexOf(_column)] = _obj;
                  }
                  break;

                case TABLE_COLUMNS.BuyIn: // Buy In

                  if (CheckDecimalTypeWithValue(_row[_column], Resource.String("STR_GAMING_TABLE_BTN_SELL_CHIPS"), out _obj, out _warning, out _warning_message, out _buy_in))
                  {
                    //If CashLess, then no allowed BuyIn in currency not equal to NationalCurrency
                    if ((_is_cashless_mode) && (_buy_in != null) && (_buy_in != 0) && (_row[(Int32)TABLE_COLUMNS.IsoCode].ToString() != _national_currency) && (_number_of_chips_currencies > 1))
                    {
                      _warning = true;
                      _warning_message = Resource.String("STR_GTPS_IMPORT_BUY_IN_INVALID_CURRENCY");
                    }
                    else
                    {
                      _new_row[GamingTablePlaySessionsToImport.Columns.IndexOf(_column)] = _obj;
                    }
                  }
                  break;

                case TABLE_COLUMNS.AwaredPoints: // Awared Points

                  // If awared points are not set, use the computed points
                  if (_row[_column] == DBNull.Value)
                  {
                    _set_points_computed_to_awared = true;
                  }

                  if (CheckDecimalType(_row[_column], Resource.String("STR_CASHIER_GAMING_TABLE_POINTS_GROUP_BOX"), out _obj, out _warning, out _warning_message))
                  {
                    _new_row[GamingTablePlaySessionsToImport.Columns.IndexOf(_column)] = _obj;
                  }
                  break;
                case TABLE_COLUMNS.IsoCode: // Iso Code
                  if ((_row[_column] == DBNull.Value) || (String.IsNullOrEmpty(_row[_column].ToString())))
                  {
                    if (_number_of_chips_currencies > 1)
                    {
                      _warning = true;
                      _warning_message = Resource.String("STR_GTPS_IMPORT_CURRENCY_EMPTY");
                    }
                    else
                    {
                      //If there is only a currency then use NationalCurrency
                      _new_row[(Int32)TABLE_COLUMNS.IsoCode] = _national_currency;
                    }
                  }
                  else
                  {
                    _iso_code = (String)_row[_column];
                    _new_row[(Int32)TABLE_COLUMNS.IsoCode] = _iso_code;
                  }

                  break;

                default:
                  break;
              }

              // If error, add to Errors table
              if (_warning)
              {
                _dt_errors.Rows.Add(GamingTablePlaySessionsToImport.Rows.IndexOf(_row) + FIRST_DATA_ROW, _warning_message);
                _insert_row = false;
                break;
              }
            } // For each Column

            if (!_warning)
            {
              // Validate points
              if (_new_row[(Int32)TABLE_COLUMNS.AccountId] == DBNull.Value || _elp_mode > 0)
              {
                // Unknow Or ELP
                _new_row[(Int32)TABLE_COLUMNS.AwaredPoints] = 0;
                _new_row[(Int32)TABLE_COLUMNS.ComputedPoints] = 0;
              }
              else
              {
                // Compute points
                _computed_points = 0;
                _computed_played_amount = (Decimal)_new_row[(Int32)TABLE_COLUMNS.PlayedAmount];

                WonBucketDict _wonBucket = new WonBucketDict();
                Boolean _bucket_awarded;

                GamingTableBusinessLogic.Get_CalculateBuckets(Convert.ToInt64(_new_row[(Int32)TABLE_COLUMNS.VirtualTerminalId]),
                                                              (Int64)_new_row[(Int32)TABLE_COLUMNS.AccountId],
                                                              _computed_played_amount,
                                                              1,
                                                              0,
                                                              ref _wonBucket,
                                                              _db_trx.SqlTransaction);

                if (_wonBucket.Dict.ContainsKey(Buckets.BucketId.RedemptionPoints))
                {
                  _computed_points = _wonBucket.Dict[Buckets.BucketId.RedemptionPoints].WonValue;
                  _bucket_awarded = _wonBucket.Dict[Buckets.BucketId.RedemptionPoints].ValueHaveToBeAwarded;
                }

                _new_row[(Int32)TABLE_COLUMNS.ComputedPoints] = _computed_points;

                // If awared points are not set, use the computed points
                if (_set_points_computed_to_awared)
                {
                  _new_row[(Int32)TABLE_COLUMNS.AwaredPoints] = _computed_points;
                }
              }

              // Set virtual account to account field if it isn't defined
              if (_new_row[(Int32)TABLE_COLUMNS.AccountId] == DBNull.Value)
              {
                _new_row[(Int32)TABLE_COLUMNS.AccountId] = _new_row[(Int32)TABLE_COLUMNS.VirtualAccountId];
              }

              // Validate Dates
              _arrival_date = (DateTime)_new_row[(Int32)TABLE_COLUMNS.Arrival];
              _walk_time = _new_row[(Int32)TABLE_COLUMNS.WalkTime] == DBNull.Value ? 0 : (Int64)_new_row[(Int32)TABLE_COLUMNS.WalkTime];
              _departure_date = _new_row[(Int32)TABLE_COLUMNS.Departure] == DBNull.Value ? _arrival_date : (DateTime)_new_row[(Int32)TABLE_COLUMNS.Departure];
              _new_row[(Int32)TABLE_COLUMNS.Departure] = _departure_date;

              // Calculate Game Time
              _timespan_value = _departure_date - _arrival_date;
              _new_row[(Int32)TABLE_COLUMNS.GameTime] = _timespan_value.TotalSeconds - _walk_time;

              if (_arrival_date.AddSeconds(_walk_time) > _departure_date)
              {
                _warning_message = Resource.String("STR_GTPS_IMPORT_DATES_NOT_VALID", Resource.String("STR_GTPS_IMPORT_DEPARTURE_DATE"), Resource.String("STR_GTPS_IMPORT_ARRIVAL_DATE"), Resource.String("STR_CASHIER_GAMING_TABLE_GB_AWAY_TIME"));
                _dt_errors.Rows.Add(GamingTablePlaySessionsToImport.Rows.IndexOf(_row) + FIRST_DATA_ROW, _warning_message);
                _insert_row = false;
              }

              // Find a gaming table session in the dates range
              if (!CheckGamingTableSessionDates((Int32)_new_row[(Int32)TABLE_COLUMNS.GamingTableId], _arrival_date, _departure_date, out _gaming_table_session, _db_trx.SqlTransaction))
              {
                _warning_message = Resource.String("STR_GTPS_IMPORT_NOT_EXIST_SESSION_IN_RANGE"); ;
                _dt_errors.Rows.Add(GamingTablePlaySessionsToImport.Rows.IndexOf(_row) + FIRST_DATA_ROW, _warning_message);
                _insert_row = false;
              }
              else
              {
                _new_row[(Int32)TABLE_COLUMNS.GamingTableSessionId] = _gaming_table_session;
              }
            }

            // Insert Row
            if (_insert_row)
            {
              _dt_gaming_table_sessions.Rows.Add(_new_row);
              _correct_gt_play_sessions++;
            }

          } // For each Row
        }
      }
      catch (Exception)
      {
        return false;
      }

      System.Threading.Thread.CurrentThread.CurrentCulture = _old_ci;

      GamingTablePlaysSessions = new DataSet();
      GamingTablePlaysSessions.Tables.Add(_dt_gaming_table_sessions);
      GamingTablePlaysSessions.Tables.Add(_dt_errors);

      _row_update_event = new GamingTablePlaysSessionsImportEventArgs();
      _row_update_event.Message = Resource.String("STR_AC_IMPORT_POINTS_READ_FINISHED").Replace("\\r\\n", "\r\n");
      _row_update_event.Message += "\r\n" + Resource.String("STR_GTPS_IMPORT_CORRECT_PLAY_SESSIONS") + _correct_gt_play_sessions;
      _row_update_event.State = IMPORT_STATES.DocumentReaded;
      _row_update_event.Row_count = GamingTablePlaySessionsToImport.Rows.Count;
      _row_update_event.Pending_rows = 0;

      WriteLog(_row_update_event.Message);
      OnRowUpdated(this, _row_update_event);

      return true;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : 
    //
    //  PARAMS :
    //      - INPUT :
    //          - DataTable with Gaming Table Sessions
    //
    //      - OUTPUT :
    //
    // RETURNS : True if operations were successful
    //
    public void AddImportedGamingTablePlaysSessions(System.Data.DataTable GamingTablePlaySessions, Int32 CurrentUserId, String CurrentUserName)
    {
      Int64 _operation_id;
      GamingTablePlaysSessionsImportEventArgs _events;
      Int32 _total_gtps_imported;
      Int32 _total_gtps_error;
      StringBuilder _gtps_error;

      _events = new GamingTablePlaysSessionsImportEventArgs();
      _events.State = IMPORT_STATES.UpdatingDB;

      _events.Row_count = GamingTablePlaySessions.Rows.Count;
      _events.Pending_rows = GamingTablePlaySessions.Rows.Count;
      _total_gtps_imported = 0;
      _total_gtps_error = 0;
      _gtps_error = new StringBuilder();

      // Insert Operation
      if (GeneralParam.GetBoolean("MultiSite", "IsCenter")) // MultiSite Center
      {
        _operation_id = 0; //There is not account_operations in MultiSite Center.
      }
      else // NOT a MultiSite Center and NOT a MultiSite Member.
      {
        try
        {
          using (DB_TRX _db_trx = new DB_TRX())
          {
            // Create new operation code
            if (!Operations.DB_InsertOperation(OperationCode.IMPORT_GAMING_TABLE_PLAY_SESSIONS,
                                               0, // account_id = 0 because it would be the same operationId for different accounts.
                                               0, // Cashier session id
                                               0, // Mb Account id
                                               0, // promo id
                                               0, // currency ammount
                                               0, // currency NR
                                               0, // Operation Data
                                               0,
                                               string.Empty,
                                               out _operation_id, _db_trx.SqlTransaction))
            {
              throw new Exception();
            }

            if (!_db_trx.Commit())
            {
              throw new Exception();
            }
          }
        }
        catch (Exception)
        {
          _events.Message = Resource.String("STR_GTPS_IMPORT_PLAY_SESSION_OPERATION_ERROR").Replace("\\r\\n", "\r\n");
          
          WriteLog(_events.Message);
          OnRowUpdated(this, _events);

          return;
        }
      }

      try
      {
        _events.Message = Resource.String("STR_AC_IMPORT_POINTS_INSERTED").Replace("\\r\\n", "\r\n");
        WriteLog(_events.Message);
        OnRowUpdated(this, _events);
        _events.Message = String.Empty;

        foreach (DataRow _dr_gt_play_sesion in GamingTablePlaySessions.Rows)
        {
          try
          {
            using (DB_TRX _db_trx = new DB_TRX())
            {
              // Insert gaming table play sessions and update points
              if (!DB_InsertGamingTablePlaysSessions(_dr_gt_play_sesion, _operation_id, CurrentUserId, CurrentUserName, _db_trx.SqlTransaction))
              {
                throw new Exception();
              }

              // Commit Transaction
              if (!_db_trx.Commit())
              {
                throw new Exception();
              }
            } // End _db_trx

            _events.Pending_rows--;
            _total_gtps_imported++;
            if ((_total_gtps_imported % ROWS_FOR_EVENTS) == 0) //Every 2000 updated accounts, throw a message.
            {
              _events.Message = Resource.String("STR_GTPS_IMPORT_PLAY_SESSIONS_INSERTED_2", _total_gtps_imported, _events.Pending_rows).Replace("\\r\\n", "\r\n");
            }

            WriteLog(_events.Message);
            OnRowUpdated(this, _events);

            _events.Message = String.Empty;

          }
          catch (Exception)
          {
            //Throw a message with the account info and continues with the next one.
            _total_gtps_error++;
            _gtps_error.AppendLine(Resource.String("STR_GTPS_IMPORT_PLAY_SESSIONS_INFO", _dr_gt_play_sesion[(Int32)TABLE_COLUMNS.GamingTableName], _dr_gt_play_sesion[(Int32)TABLE_COLUMNS.AccountId], _dr_gt_play_sesion[(Int32)TABLE_COLUMNS.Arrival]));
            _events.Pending_rows--;

            WriteLog(_events.Message);
            OnRowUpdated(this, _events);
          }
        }// End For

        _events.Message = Resource.String("STR_AC_IMPORT_POINTS_PROCESS_FINISHED").Replace("\\r\\n", "\r\n");

        WriteLog(_events.Message);
        OnRowUpdated(this, _events);
      }
      catch (Exception Ex)
      {
        Log.Message("AddImportedGamingTablePlaysSessions() - Error updating imported gaming table play sessions in DB");
        Log.Exception(Ex);
        WriteLog(Ex.Message);
        
        return;
      }
      finally // Summary
      {
        // Total imported
        _events.Message = Resource.String("STR_GTPS_IMPORT_PLAY_SESSIONS_TOTAL_SUCCESS", _total_gtps_imported).Replace("\\r\\n", "\r\n");

        // Import errors        
        if (_total_gtps_error > 0)
        {
          _events.Message += Resource.String("STR_GTPS_IMPORT_PLAY_SESSIONS_TOTAL_ERROR", _total_gtps_error).Replace("\\r\\n", "\r\n");
          _events.Message += _gtps_error.ToString();
        }

        _events.State = IMPORT_STATES.DBUpdated;
        _events.Row_count = 0;
        _events.Pending_rows = 0;
        
        WriteLog(_events.Message);
        OnRowUpdated(this, _events);
      }
    } // AddImportedGamingTableSessions

    //------------------------------------------------------------------------------
    // PURPOSE: Returns True or False if the stream text line it could be inserted in the Log file
    //
    //  PARAMS:
    //      - INPUT:
    //          - Value: stream text line 
    //      - OUT:
    //          - True or false
    //
    // RETURNS:
    //      - Boolean:
    //
    public Boolean WriteLog(String LogLine)
    {
      FileStream _fs_log;
      StreamWriter _sw_log;
      _fs_log = new FileStream(m_file_out, FileMode.Append, FileAccess.Write);
      _sw_log = new StreamWriter(_fs_log);

      try
      {
        _sw_log.WriteLine("" + LogLine);
        _sw_log.Close();
        _fs_log.Close();

        return true;
      }
      catch
      {
        _sw_log.Close();
        _fs_log.Close();

        return false;
      }
    }
    #endregion PublicFunctions
  }

  public class GamingTablePlaysSessionsImportEventArgs : EventArgs
  {
    private int _row_count;

    private int _pending_rows;

    private String _message;

    private GamingTablePlaysSessionsImport.IMPORT_STATES _state;

    public GamingTablePlaysSessionsImport.IMPORT_STATES State
    {
      get { return _state; }
      set { _state = value; }
    }

    public int Row_count
    {
      get { return _row_count; }
      set { _row_count = value; }
    }

    public int Pending_rows
    {
      get { return _pending_rows; }
      set { _pending_rows = value; }
    }

    public String Message
    {
      get { return _message; }
      set { _message = value; }
    }
  }
}