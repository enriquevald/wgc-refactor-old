﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems International.
//------------------------------------------------------------------------------
// 
//      MODULE NAME: InTouchGift.cs
// 
//      DESCRIPTION: Class to get the player gift request
// 
//           AUTHOR: Ferran Ortnerr
// 
//    CREATION DATE: 31-OCT-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 31-OCT-2017 FOS    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace WSI.Common
{
  public class WCPPlayerRequestFuncionalitySchedule
  {
    #region " Constructor "
    public WCPPlayerRequestFuncionalitySchedule()
    {
    }
    #endregion

    # region " Public functions "
    public bool WKT_FunctionalitySchedule(PromoBOX.Functionality Functionality)
    {
      List<PromoBOX.Functionality> _func_list;
      Int32 _from_time;
      Int32 _to_time;
      String _aux_key_value;

      _from_time = 0;
      _to_time = 0;

      using (DB_TRX _trx = new DB_TRX())
      {
        _func_list = WSI.Common.PromoBOX.ReadPromoBoxFunc(Functionality, true, _trx.SqlTransaction);

        if (_func_list.Count == 0)
        {
          // Requested functionality is not limited by schedule => go forward with the request
          return true;
        }

        // Schedule time from
        _aux_key_value = GeneralParam.Value("WigosKiosk", "Schedule.From");
        _from_time = this.GeneralParamToScheduleTime(_aux_key_value);

        // Schedule time to
        _aux_key_value = GeneralParam.Value("WigosKiosk", "Schedule.To");
        _to_time = this.GeneralParamToScheduleTime(_aux_key_value);
      }

      return ScheduleDay.IsTimeInRange(DateTime.Now.TimeOfDay, _from_time, _to_time);
    } // WKT_FunctionalitySchedule

    public Int32 GeneralParamToScheduleTime(String GeneralParam)
    {
      Int32 _aux_hours;
      Int32 _aux_mins;
      Int32 _schedule_time;

      _schedule_time = 0;

      //Time is stored in HHMM format, so the string length should be 4.  
      //In any other case, the return value will be 0
      if (GeneralParam.Length != 4)
      {
        return _schedule_time;
      }

      if (Int32.TryParse(GeneralParam.Substring(0, 2), out _aux_hours)
        && Int32.TryParse(GeneralParam.Substring(2, 2), out _aux_mins))
      {
        _schedule_time = (Int32)DateTime.Today.AddHours(_aux_hours).AddMinutes(_aux_mins).TimeOfDay.TotalSeconds;
      }

      return _schedule_time;
    } // GeneralParamToScheduleTime
    #endregion 

  }
}
