//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Split.cs
// 
//   DESCRIPTION: Class to manage Split parameters from GeneralParams
// 
//        AUTHOR: Ra�l Cervera
// 
// CREATION DATE: 27-SEP-2010
// 
// REVISION HISTORY:
// 
// Date        Author     Description
// ----------- ---------- ----------------------------------------------------------
// 27-SEP-2010 RCI        First release.
// 04-JAN-2012 RCI        Optionally, Hide Totals in A and B tickets. Remove field for the ticket Cash in.
// 21-MAR-2012 MPO        Set the values of general params of promo_coupon and text_promo_coupontogether
// 28-MAY-2014 LEM & JPJ  Fixed Bug WIG-957: Wrong Company B name in Multisite Center
// 12-JUL-2016 EOR        Product Backlog Item 15318: Tables-21: Sale of Chips with Company A and B
// 17-OCT-2016 ESE        Bug 18076: Is not clear the log message, must be more specific
// 21-DEC-2017 RGR        Bug 31142:WIGOS-7253 Cashier - Error is registered when a deposit or withdraw is done with redemed, not redemed and color chips
//----------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace WSI.Common
{
  public static class Split
  {

    #region Public Methods

    //------------------------------------------------------------------------------
    // PURPOSE: Read Split Parameters from General Params.
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //          - TYPE_SPLITS Splits
    //
    // RETURNS:
    //      - Boolean: If split parameters have been read correctly.
    //
    public static Boolean ReadSplitParameters(out TYPE_SPLITS Splits)
    {
      Splits = new TYPE_SPLITS();
      Splits.enabled_b = false;
      Splits.prize_coupon = false;
      Splits.prize_coupon_text = "";
      Splits.hide_promo = false;
      Splits.text_promo = "";
      Splits.promo_and_coupon = false;
      Splits.text_promo_and_coupon = "";

      Splits.company_a.company_name = "";
      Splits.company_a.name = "";
      Splits.company_a.cashin_pct = 100;
      Splits.company_a.cancellation_pct = Splits.company_a.cashin_pct;
      Splits.company_a.devolution_pct = 100;
      Splits.company_a.tax_name = "";
      Splits.company_a.tax_pct = 0;

      Splits.company_a.header = "";
      Splits.company_a.footer = "";
      Splits.company_a.footer_cashin = "";
      Splits.company_a.footer_dev = "";
      Splits.company_a.footer_cancel = "";

      Splits.company_a.hide_total = false;
      Splits.company_a.hide_currency_symbol = false;
      Splits.company_a.total_in_letters = false;

      Splits.company_a.cash_in_voucher_title = "";
      Splits.company_a.cash_out_voucher_title = "";

      Splits.company_b.company_name = "";
      Splits.company_b.name = "";
      Splits.company_b.cashin_pct = 0;
      Splits.company_b.cancellation_pct = Splits.company_b.cashin_pct;
      Splits.company_b.devolution_pct = 0;
      Splits.company_b.tax_name = "";
      Splits.company_b.tax_pct = 0;

      Splits.company_b.header = "";
      Splits.company_b.footer = "";
      Splits.company_b.footer_cashin = "";
      Splits.company_b.footer_dev = "";
      Splits.company_b.footer_cancel = "";

      Splits.company_b.hide_total = false;
      Splits.company_b.hide_currency_symbol = false;
      Splits.company_b.total_in_letters = false;

      Splits.company_b.cash_in_voucher_title = "";
      Splits.company_b.cash_out_voucher_title = "";

      try
      {
        Splits.enabled_b = GeneralParam.GetBoolean("Cashier", "Split.B.Enabled");
        Splits.promo_and_coupon = GeneralParam.GetBoolean("Cashier", "Promo&CouponTogether");
        Splits.text_promo_and_coupon = GeneralParam.GetString("Cashier", "Promo&CouponTogether.Text");

        Splits.prize_coupon = GeneralParam.GetBoolean("Cashier", "SplitAsWon");
        Splits.prize_coupon_text = GeneralParam.GetString("Cashier", "SplitAsWon.Text");

        Splits.hide_promo = GeneralParam.GetBoolean("Cashier", "HidePromotion");
        Splits.text_promo = GeneralParam.GetString("Cashier", "Promotion.Text");

        Splits.company_a.hide_total = GeneralParam.GetBoolean("Cashier", "Split.A.HideTotal");
        Splits.company_a.hide_currency_symbol = GeneralParam.GetBoolean("Cashier.Voucher", "Voucher.A.HideCurrencySymbol");
        Splits.company_a.total_in_letters = GeneralParam.GetBoolean("Cashier.Voucher", "Voucher.A.TotalInLetters");
        Splits.company_a.company_name = GeneralParam.GetString("Cashier", "Split.A.CompanyName");
        Splits.company_a.name = GeneralParam.GetString("Cashier", "Split.A.Name");
        Splits.company_a.cashin_pct = GeneralParam.GetDecimal("Cashier", "Split.A.CashInPct");
        Splits.company_a.tax_name = GeneralParam.GetString("Cashier", "Split.A.Tax.Name");
        Splits.company_a.tax_pct = GeneralParam.GetDecimal("Cashier", "Split.A.Tax.Pct");

        Splits.company_a.cash_in_voucher_title = GeneralParam.GetString("Cashier", "Split.A.CashInVoucherTitle");
        Splits.company_a.cash_out_voucher_title = GeneralParam.GetString("Cashier", "Split.A.CashOutVoucherTitle");
        // Voucher
        Splits.company_a.header = GeneralParam.GetString("Cashier.Voucher", "Voucher.A.Header");
        Splits.company_a.footer = GeneralParam.GetString("Cashier.Voucher", "Voucher.A.Footer");
        Splits.company_a.footer_cashin = GeneralParam.GetString("Cashier.Voucher", "Voucher.A.Footer.CashIn");
        Splits.company_a.footer_dev = GeneralParam.GetString("Cashier.Voucher", "Voucher.A.Footer.Dev");
        Splits.company_a.footer_cancel = GeneralParam.GetString("Cashier.Voucher", "Voucher.A.Footer.Cancel");

        Splits.company_b.company_name = GeneralParam.GetString("Cashier", "Split.B.CompanyName");
        Splits.company_b.name = GeneralParam.GetString("Cashier", "Split.B.Name");

        // EOR 12-JUL-2016
        Splits.company_a.chips_sale_pct = GeneralParam.GetDecimal("GamingTables", "Split.A.ChipsSalePct", Splits.company_a.cashin_pct);

        if (Splits.enabled_b)
        {
          Splits.company_a.devolution_pct = 100;
          Splits.company_b.devolution_pct = 0;

          Splits.company_b.hide_total = GeneralParam.GetBoolean("Cashier", "Split.B.HideTotal");
          Splits.company_b.hide_currency_symbol = GeneralParam.GetBoolean("Cashier.Voucher", "Voucher.B.HideCurrencySymbol");
          Splits.company_b.total_in_letters = GeneralParam.GetBoolean("Cashier.Voucher", "Voucher.B.TotalInLetters");
          Splits.company_b.cashin_pct = GeneralParam.GetDecimal("Cashier", "Split.B.CashInPct");

          Splits.company_b.tax_name = GeneralParam.GetString("Cashier", "Split.B.Tax.Name");
          Splits.company_b.tax_pct = GeneralParam.GetDecimal("Cashier", "Split.B.Tax.Pct"); ;

          Splits.company_a.cancellation_pct = Splits.company_a.cashin_pct;
          Splits.company_b.cancellation_pct = Splits.company_b.cashin_pct;

          Splits.company_b.cash_in_voucher_title = GeneralParam.GetString("Cashier", "Split.B.CashInVoucherTitle");
          Splits.company_b.cash_out_voucher_title = GeneralParam.GetString("Cashier", "Split.B.CashOutVoucherTitle");

          // Voucher
          Splits.company_b.header = GeneralParam.GetString("Cashier.Voucher", "Voucher.B.Header");
          Splits.company_b.footer = GeneralParam.GetString("Cashier.Voucher", "Voucher.B.Footer");
          Splits.company_b.footer_cashin = GeneralParam.GetString("Cashier.Voucher", "Voucher.B.Footer.CashIn");
          Splits.company_b.footer_dev = GeneralParam.GetString("Cashier.Voucher", "Voucher.B.Footer.Dev");
          Splits.company_b.footer_cancel = GeneralParam.GetString("Cashier.Voucher", "Voucher.B.Footer.Cancel");

          // EOR 12-JUL-2016
          Splits.company_b.chips_sale_pct = GeneralParam.GetDecimal("GamingTables", "Split.B.ChipsSalePct", Splits.company_b.cashin_pct);
        }
        else
        {
          Splits.prize_coupon = false;
          Splits.company_a.cashin_pct = 100;
          Splits.company_b.cashin_pct = 0;
          Splits.company_a.devolution_pct = 100;
          Splits.company_b.devolution_pct = 0;
          Splits.company_a.cancellation_pct = 100;
          Splits.company_b.cancellation_pct = 0;
        }

        // EOR 12-JUL-2016
        // RGR 21-DEC-2017
        if ((Splits.company_a.chips_sale_pct + Splits.company_b.chips_sale_pct) != 100)
        {
          Splits.company_a.chips_sale_pct = 100;
          Splits.company_b.chips_sale_pct = 0;

          Log.Warning(" Wrong chips sale percentage configuration. (Company A + Company B) must be %100");
          if (Splits.enabled_b)
          {
            Log.Warning(" Company_A.chips_sale = 100, Company_B.chips_sale = 0");
          }
          else
          {
            Log.Warning(" Company_A.chips_sale = 100, Company_B.chips_sale = 0, because company B is not activate");
          }
        }


        if (!GeneralParam.GetBoolean("MultiSite", "IsCenter", false))
        {
          if (Splits.company_a.cashin_pct + Splits.company_b.cashin_pct != 100)              
          {
            Log.Error("Wrong cash in percentage configuration, (Company A + Company B) must be %100");
            return false;
          }

          if (Splits.company_a.devolution_pct + Splits.company_b.devolution_pct != 100)
          {
            Log.Error("Wrong devolution percentage configuration, (Company A + Company B) must be %100");
            return false;
          }

          if (Splits.company_a.cancellation_pct + Splits.company_b.cancellation_pct != 100)
          {
            Log.Error("Wrong cancelation percentage configuration, (Company A + Company B) must be %100");
            return false;
          }
        }
        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // ReadSplitParameters

    #endregion // Public Methods

  } // Split

} // WSI.Common
