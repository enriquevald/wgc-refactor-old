//------------------------------------------------------------------------------
// Copyright � 2012 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PromotionCategories.cs
// 
//   DESCRIPTION: Class to manage Categories of the Promotions
// 
//        AUTHOR: Xavier Iglesia
// 
// CREATION DATE: 25-SEP-2012
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 25-SEP-2012 XIT    First release.
// 11-JUL-2014 XIT    Added MultiLanguage support
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Threading;
using System.Diagnostics;
using System.Data;

namespace WSI.Common
{
  public static class PromotionCategories
  {
    #region Constants
    private const Int32 REFRESH_PERIOD = 2 * 60 * 1000; // 2 minutes
    private const Int32 ERROR_WAIT_HINT = 10 * 1000;    // 10 seconds 
    #endregion

    #region Members
    private static ManualResetEvent m_ev_values_available = new ManualResetEvent(false);
    private static Dictionary<Int32, String> m_categories = new Dictionary<Int32, String>();
    private static Boolean m_init = PromotionCategories.Init();
    private static Int64 m_num_read = 0; 
    #endregion

    #region Properties
    public static Dictionary<Int32, String> Categories
    {
      get
      {
        // Wait till the values have been retrieved from the DB
        m_ev_values_available.WaitOne();
        return PromotionCategories.m_categories;
      }
 
    } 
    #endregion


    #region Private Methods
    //------------------------------------------------------------------------------
    // PURPOSE : Creates the auto-refresh thread
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES : This function is "automatically" called on the first call to PromotionCategories
    //
    private static Boolean Init()
    {
      Thread _thread = null;

      if (!String.IsNullOrEmpty(Environment.GetEnvironmentVariable("LKS_VC_DEV")))
      {
        if (System.Diagnostics.Process.GetCurrentProcess().ProcessName == "devenv")
        {
          m_ev_values_available.Set();

          return true;
        }
      }

      _thread = new Thread(new ThreadStart(PromotionCategoriesThread));
      _thread.Name = "PromotionCategoriesThread";
      _thread.Start();

      return true;
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE : Automatic reload of the General Params
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //        The value of the selected General Param
    //
    //   NOTES :
    //
    private static void PromotionCategoriesThread()
    {
      StringBuilder _sb;

      while (true)
      {
        try
        {
          // Ensure DB is initialized
          while (!WGDB.Initialized)
          {
            Thread.Sleep(250);
          }

          using (DB_TRX _db_trx = new DB_TRX())
          {
            _sb = new StringBuilder();
            _sb.AppendLine("   SELECT   PC_CATEGORY_ID     ");
            _sb.AppendLine("          , PC_NAME         ");
            _sb.AppendLine("     FROM   PROMOTION_CATEGORIES  ");
            _sb.AppendLine("    WHERE   PC_LANGUAGE_ID = @pLanguageId");

            using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString()))
            {
              _sql_cmd.Parameters.Add("@pLanguageId", SqlDbType.Int).Value = Resource.LanguageId;
              using (SqlDataReader _reader = _db_trx.ExecuteReader(_sql_cmd))
              {
                lock (m_categories)
                {
                  // Remove all the parameters & add the just read ones.
                  m_categories.Clear();
                  while (_reader.Read())
                  {
                    m_categories.Add(_reader.GetInt32(0), _reader.GetString(1));
                  }
                }
                // Notify 'Parameters Available'
                m_ev_values_available.Set();
              }
            }
          }
          Thread.Sleep(REFRESH_PERIOD);
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
          Thread.Sleep(ERROR_WAIT_HINT);
        }
      }

    } // PromotionCategoriesThread 
    #endregion

    #region Public Methods
    //------------------------------------------------------------------------------
    // PURPOSE : Return the value from the PromotionCategories (in memory)
    //
    //  PARAMS :
    //      - INPUT :
    //          - Id
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //        The value of the selected Key
    //
    //   NOTES :
    //
    public static String Value(Int32 Id)
    {
      String _value;

      // Wait till the values have been retrieved from the DB
      m_ev_values_available.WaitOne();

      // Get the value
      lock (m_categories)
      {
        if (!m_categories.TryGetValue(Id, out _value))
        {
          _value = String.Empty;
        }
      }

      Interlocked.Increment(ref m_num_read);

      return _value;
    } // Value
    
    #endregion
  }
}
