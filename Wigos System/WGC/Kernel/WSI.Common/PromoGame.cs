﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: PromoGame.cs
// 
//   DESCRIPTION: Miscellaneous utilities for PromoGame.
// 
//        AUTHOR: Rubén Lama
// 
// CREATION DATE: 20-JUL-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 20-JUL-2017 RLO    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;

namespace WSI.Common
{
  public class PromoGame
  {

    private IPromoGame m_promogame_interface;

    #region " Constants "

    // Private constants
    private const Int32 DB_PG_ID                      = 0;
    private const Int32 DB_PG_NAME                    = 1;
    private const Int32 DB_PG_TYPE                    = 2;
    private const Int32 DB_PG_PRICE                   = 3;
    private const Int32 DB_PG_PRICE_UNITS             = 4;
    private const Int32 DB_PG_RETURN_PRICE            = 5;
    private const Int32 DB_PG_RETURN_UNITS            = 6;
    private const Int32 DB_PG_PERCENTATGE_COST_RETURN = 7;
    private const Int32 DB_PG_GAME_URL                = 8;
    private const Int32 DB_PG_MANDATORY_IC            = 9;
    private const Int32 DB_PG_SHOW_BUY_DIALOG         = 10;
    private const Int32 DB_PG_PLAYER_CAN_CANCEL       = 11;
    private const Int32 DB_PG_AUTOCANCEL              = 12;
    private const Int32 DB_PG_TRANSFER_SCREEN         = 13;
    private const Int32 DB_PG_TRANSFER_TIME_OUT       = 14;
    private const Int32 DB_PG_LAST_UPDATE             = 15;
    private const Int32 DB_PG_PROMOTION_ID            = 16;
    private const Int32 DB_PG_MINIMUM_PRIZE           = 17;
    private const Int32 DB_PG_PROMOTION_TYPE          = 18;

    // Public constants
    public const Int32 DEFAULT_ID_VALUE = -1;
    public const Int32 DEFAULT_INT_VALUE = 0;
    public const Decimal DEFAULT_DECIMAL_VALUE = 0;

    public const Int32 DEFAULT_TIME_VALUE = -1;
    public const Int32 DEFAULT_TIME_FROM_VALUE = 0;
    public const Int32 DEFAULT_TIME_TO_VALUE = 1439;

    #endregion

    #region " Enums "

    public enum GameType
    {
      Welcome    = 0,
      PlayCash   = 1,
      PlayReward = 2,
      PlayPoint = 3,
      All        = 4,
      
      Unknown    = 99
    } // GameType

    #endregion

    #region " Properties "

    //Base Properties
    public Boolean IsNew
    {
      get { return (this.Id == DEFAULT_ID_VALUE); }
    }
    public Int64 Id { get; set; }
    public String Name { get; set; }
    public GameType Type { get; set; }
    public Decimal Price { get; set; }
    public PrizeType PriceUnits { get; set; }
    public Boolean ReturnPrice { get; set; }
    public PrizeType ReturnUnits { get; set; }
    public Decimal PercentatgeCostReturn { get; set; }
    public String GameURL { get; set; }
    public Boolean MandatoryIC { get; set; }
    public Boolean ShowBuyDialog { get; set; }
    public Boolean PlayerCanCancel { get; set; }
    public Int32 AutoCancel { get; set; }
    public Boolean TransferScreen { get; set; }
    public Int32 TransferTimeOut { get; set; }
    public Int64 PromotionId { get; set; }
    public Currency MinimumPrize { get; set; }
    //public XML PlayTable { get; set; }
    public DateTime LastUpdate { get; set; }
    public DateTime LastModification { get; set; }
    public Boolean IsModified
    {
      get
      {
        return this.LastModification > WGDB.MinDate;
      }
    }
    public Int32 PromotionType { get; set; }
    #endregion " Properties "

    #region " Public Methods "

    #region " Constructors "

    public PromoGame()
    {
      this.Id                    = DEFAULT_ID_VALUE;
      this.Name                  = String.Empty;
      this.Type                  = GameType.PlayCash;
      this.Price                 = DEFAULT_DECIMAL_VALUE;
      this.PriceUnits            = PrizeType.Redeemable;
      this.ReturnPrice           = false;
      this.ReturnUnits           = PrizeType.Redeemable;
      this.PercentatgeCostReturn = DEFAULT_DECIMAL_VALUE;
      this.GameURL               = String.Empty;
      this.MandatoryIC           = false;
      this.ShowBuyDialog         = false;
      this.PlayerCanCancel       = false;
      this.AutoCancel            = DEFAULT_INT_VALUE;
      this.TransferScreen        = false;
      this.TransferTimeOut       = DEFAULT_INT_VALUE;
      this.PromotionId           = DEFAULT_ID_VALUE;
      this.MinimumPrize          = DEFAULT_DECIMAL_VALUE;
      //this.PlayTable              = new XML;
      this.LastModification      = WGDB.MinDate;
      this.LastUpdate            = WGDB.MinDate;
      this.PromotionType         = DEFAULT_INT_VALUE;
      

    } // PromoGame

    public PromoGame(Int64 PromoGameId)
    {
      this.DB_GetByGameId(PromoGameId);
    } // PromoGame

    public PromoGame(Int64 PromoGameId, SqlTransaction SqlTrx)
    {
      this.DB_ByGameId(PromoGameId, SqlTrx);
    } // PromoGame

    public PromoGame(IPromoGame repos)
    {
      m_promogame_interface = repos;
    }  // PromoGame

    public PromoGame(PromoGame repos)
    {
      this.Id                     = repos.Id;
      this.Name                   = repos.Name;
      this.Type                   = repos.Type;
      this.Price                  = repos.Price;
      this.PriceUnits             = repos.PriceUnits;
      this.ReturnPrice            = repos.ReturnPrice;
      this.ReturnUnits            = repos.ReturnUnits;
      this.PercentatgeCostReturn  = repos.PercentatgeCostReturn;
      this.GameURL                = repos.GameURL;
      this.MandatoryIC            = repos.MandatoryIC;
      this.ShowBuyDialog          = repos.ShowBuyDialog;
      this.PlayerCanCancel        = repos.PlayerCanCancel;
      this.AutoCancel             = repos.AutoCancel;
      this.TransferScreen         = repos.TransferScreen;
      this.TransferTimeOut        = repos.TransferTimeOut;
      this.PromotionId            = repos.PromotionId;
      this.MinimumPrize           = repos.MinimumPrize;
      //this.PlayTable              = repos.PlayTable;          
      this.LastModification       = repos.LastModification;
      this.LastUpdate             = repos.LastUpdate;
      this.PromotionType          = repos.PromotionType;
    }  // PromoGame


    #endregion

    /// <summary>
    /// 
    /// Save InTouch Promo Game DB Method
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public virtual Boolean Save()
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        if (this.Save(_db_trx.SqlTransaction))
        {
          return _db_trx.Commit();
        }
      }

      return false;
    } // Save
    public virtual Boolean Save(SqlTransaction SqlTrx)
    {
      return this.DB_Save(SqlTrx);
    }     // Save

    /// <summary>
    /// Clone PromoGame 
    /// </summary>
    /// <returns></returns>
    public PromoGame Clone()
    {
      return new PromoGame()
      {
        Id                    = this.Id,
        Name                  = this.Name,
        Type                  = this.Type,
        Price                 = this.Price,
        PriceUnits            = this.PriceUnits,
        ReturnPrice           = this.ReturnPrice,
        ReturnUnits           = this.ReturnUnits,
        PercentatgeCostReturn = this.PercentatgeCostReturn,
        GameURL               = this.GameURL,
        MandatoryIC           = this.MandatoryIC,
        ShowBuyDialog         = this.ShowBuyDialog,
        PlayerCanCancel       = this.PlayerCanCancel,
        AutoCancel            = this.AutoCancel,
        TransferScreen        = this.TransferScreen,
        TransferTimeOut       = this.TransferTimeOut,
        PromotionId           = this.PromotionId,
        MinimumPrize          = this.MinimumPrize,
        LastUpdate            = this.LastUpdate,
        PromotionType         = this.PromotionType,
        //PlayTable              = this.PlayTable,

      };
    } // Clone

    /// <summary>
    /// Reset all members id
    /// </summary>
    public virtual void Reset()
    {
      this.Id = DEFAULT_ID_VALUE;
    } // Reset

    /// <summary>
    /// Delete from BBDD
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public virtual Boolean Delete(SqlTransaction SqlTrx)
    {
      return this.DB_Delete(SqlTrx);
    }//Delete

    public virtual Boolean CheckPromotionWithPromogame()
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return this.DB_CheckPromotionWithPromogame(_db_trx.SqlTransaction);
      }
    }

    /// <summary>
    /// Public method to Get a List of Objects
    /// </summary>
    /// <param name="SqlTrans"></param>
    /// <returns></returns>
    public List<PromoGame> GetByGameTypeListAndPromotionId(List<GameType> GameTypeList, Int64 PromotionId)
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          return GetByGameTypeListAndPromotionId(GameTypeList, PromotionId, _db_trx.SqlTransaction);
        }
      }
      catch (ArgumentException)
      {
        return null; // no object  with that id was found
      }
    }
    public List<PromoGame> GetByGameTypeListAndPromotionId(List<GameType> GameTypeList, Int64 PromotionId, SqlTransaction SqlTrx)
    {
      try
      {
        return DB_GetByGameTypeListAndPromotionId(GameTypeList, PromotionId, SqlTrx);
      }
      catch (ArgumentException)
      {
        return null; // no object  with that id was found
      }
    } // GetByGameTypeListAndPromotionId

    /// <summary>
    /// Get a list filtered by game type and prize type
    /// </summary>
    /// <param name="GameTypeList"></param>
    /// <param name="PrizeTypeList"></param>
    /// <returns></returns>
    public List<PromoGame> GetByGameTypeListAndPrizeTypeList(List<GameType> GameTypeList, List<PrizeType> PrizeTypeList)
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          return this.DB_GetByGameTypeListAndPrizeTypeList(GameTypeList, PrizeTypeList, _db_trx.SqlTransaction);
        }
      }
      catch (ArgumentException)
      {
        return null; // no object  with that id was found
      }
    } // GetByGameTypeListAndPrizeTypeList

    /// <summary>
    /// Get a list filtered by game type and prize type
    /// </summary>
    /// <param name="GameTypeList"></param>
    /// <param name="PrizeTypeList"></param>
    /// <returns></returns>
    public List<PromoGame> GetByGameTypeListAndPrizeTypeListAndPromotionId(List<GameType> GameTypeList, List<PrizeType> PrizeTypeList, long PromotionId)
    {
      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          return this.DB_GetByGameTypeListAndPrizeTypeListAndPromotionId(GameTypeList, PrizeTypeList, PromotionId, _db_trx.SqlTransaction);
        }
      }
      catch (ArgumentException)
      {
        return null; // no object  with that id was found
      }
    } // GetByGameTypeListAndPrizeTypeList

    #endregion " Public Methods "

    #region " Internal Methods "

    #region " DataBase Methods "

    /// <summary>
    /// Insert an specific InTouchPromoGame in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
     
    internal Boolean DB_Insert()
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return DB_Insert(_db_trx.SqlTransaction);
      }
    } // DB_Insert

    internal Boolean DB_Insert(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      SqlParameter _op;

      _sb = new StringBuilder();

      try
      {
        _sb.AppendLine(" INSERT INTO    PROMOGAMES                 ");
        _sb.AppendLine("              ( PG_NAME                     ");
        _sb.AppendLine("              , PG_TYPE                     ");
        _sb.AppendLine("              , PG_PRICE                    ");
        _sb.AppendLine("              , PG_PRICE_UNITS              ");
        _sb.AppendLine("              , PG_RETURN_PRICE             ");
        _sb.AppendLine("              , PG_RETURN_UNITS             ");
        _sb.AppendLine("              , PG_PERCENTATGE_COST_RETURN  ");
        _sb.AppendLine("              , PG_GAME_URL                 ");
        _sb.AppendLine("              , PG_MANDATORY_IC             ");
        _sb.AppendLine("              , PG_SHOW_BUY_DIALOG          ");
        _sb.AppendLine("              , PG_PLAYER_CAN_CANCEL        ");
        _sb.AppendLine("              , PG_AUTOCANCEL               ");
        _sb.AppendLine("              , PG_TRANSFER_SCREEN          ");
        _sb.AppendLine("              , PG_TRANSFER_TIMEOUT         ");
        _sb.AppendLine("              , PG_LAST_UPDATE )            ");
        //_sb.AppendLine("              , PG_PAY_TABLE)               ");

        _sb.AppendLine("      VALUES                                ");
        _sb.AppendLine("              ( @pName                      ");
        _sb.AppendLine("              , @pType                      ");
        _sb.AppendLine("              , @pPrice                     ");
        _sb.AppendLine("              , @pPriceUnits                ");
        _sb.AppendLine("              , @pReturnPrice               ");
        _sb.AppendLine("              , @pReturnUnits               ");
        _sb.AppendLine("              , @pPercentatgeCostReturn     ");
        _sb.AppendLine("              , @pGameURL                   ");
        _sb.AppendLine("              , @pMandatoryIC               ");
        _sb.AppendLine("              , @pShowBuyDialog             ");
        _sb.AppendLine("              , @pPlayerCanCancel           ");
        _sb.AppendLine("              , @pAutoCancel                ");
        _sb.AppendLine("              , @pTransferScreen            ");
        _sb.AppendLine("              , @pTransferTimeOut           ");
        _sb.AppendLine("              , @pLastUpdate)               ");
        //_sb.AppendLine("              , @pPayTable)                 ");

        _sb.AppendLine(" SET     @pPromoGameID = SCOPE_IDENTITY()     ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pName"                    , SqlDbType.NVarChar).Value = this.Name;
          _cmd.Parameters.Add("@pType"                    , SqlDbType.Int).Value = this.Type;
          _cmd.Parameters.Add("@pPrice"                   , SqlDbType.Decimal).Value = this.Price;
          _cmd.Parameters.Add("@pPriceUnits"              , SqlDbType.Int).Value = this.PriceUnits;
          _cmd.Parameters.Add("@pReturnPrice"             , SqlDbType.Int).Value = this.ReturnPrice;
          _cmd.Parameters.Add("@pReturnUnits"             , SqlDbType.Int).Value = this.ReturnUnits;
          _cmd.Parameters.Add("@pPercentatgeCostReturn"   , SqlDbType.Int).Value = this.PercentatgeCostReturn;
          _cmd.Parameters.Add("@pGameURL"                 , SqlDbType.NVarChar).Value = this.GameURL;
          _cmd.Parameters.Add("@pMandatoryIC"             , SqlDbType.Bit).Value = this.MandatoryIC;
          _cmd.Parameters.Add("@pShowBuyDialog"           , SqlDbType.Bit).Value = this.ShowBuyDialog;
          _cmd.Parameters.Add("@pPlayerCanCancel"         , SqlDbType.Bit).Value = this.PlayerCanCancel;
          _cmd.Parameters.Add("@pAutoCancel"              , SqlDbType.Int).Value = this.AutoCancel;
          _cmd.Parameters.Add("@pTransferScreen"          , SqlDbType.Bit).Value = this.TransferScreen;
          _cmd.Parameters.Add("@pTransferTimeOut"         , SqlDbType.Int).Value = this.TransferTimeOut;

          // Last update
          this.LastUpdate = WGDB.Now;
          _cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = (this.LastUpdate == WGDB.MinDate) ? DBNull.Value : (Object)this.LastUpdate;
   
          //_cmd.Parameters.Add("@pPayTable"                , SqlDbType.Xml       ).Value = this.PlayTable;

          //Output parameter
          _op = new SqlParameter("@pPromoGameID", SqlDbType.Int);
          _op.Direction = ParameterDirection.Output;
          _cmd.Parameters.Add(_op);

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }

          Log.Error(String.Format("PromoGame.DB_Insert -> PromoGame: {0}", this.Name));
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("PromoGame.DB_Insert -> PromoGame: {0}", this.Name));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Insert

    /// <summary>
    /// Update an specific Promogame in database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal Boolean DB_Update(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      try
      {

        _sb.AppendLine(" UPDATE   PROMOGAMES                                             ");
        _sb.AppendLine("    SET   PG_NAME                    = @pName                    ");
        _sb.AppendLine("        , PG_TYPE                    = @pType                    ");
        _sb.AppendLine("        , PG_PRICE                   = @pPrice                   ");
        _sb.AppendLine("        , PG_PRICE_UNITS             = @pPriceUnits              ");
        _sb.AppendLine("        , PG_RETURN_PRICE            = @pReturnPrice             ");
        _sb.AppendLine("        , PG_RETURN_UNITS            = @pReturnUnits             ");
        _sb.AppendLine("        , PG_PERCENTATGE_COST_RETURN = @pPercentatgeCostReturn   ");
        _sb.AppendLine("        , PG_GAME_URL                = @pGameURL                 ");
        _sb.AppendLine("        , PG_MANDATORY_IC            = @pMandatoryIC             ");
        _sb.AppendLine("        , PG_SHOW_BUY_DIALOG         = @pShowBuyDialog           ");
        _sb.AppendLine("        , PG_PLAYER_CAN_CANCEL       = @pPlayerCanCancel         ");
        _sb.AppendLine("        , PG_AUTOCANCEL              = @pAutoCancel              ");
        _sb.AppendLine("        , PG_TRANSFER_SCREEN         = @pTransferScreen          ");
        _sb.AppendLine("        , PG_TRANSFER_TIMEOUT        = @pTransferTimeOut         ");
        //_sb.AppendLine("        , PG_PAY_TABLE               = @pPlayTable               ");
        _sb.AppendLine("        , PG_LAST_UPDATE             = @pLastUpdate              ");
        _sb.AppendLine("  WHERE   PG_ID                      = @pPG_ID                   ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pPG_ID"                       , SqlDbType.Int).Value = this.Id;
          _cmd.Parameters.Add("@pName"                        , SqlDbType.NVarChar).Value = this.Name;
          _cmd.Parameters.Add("@pType"                        , SqlDbType.Int).Value = this.Type;
          _cmd.Parameters.Add("@pPrice"                       , SqlDbType.Decimal).Value = this.Price;
          _cmd.Parameters.Add("@pPriceUnits"                  , SqlDbType.Int).Value = this.PriceUnits;
          _cmd.Parameters.Add("@pReturnPrice"                 , SqlDbType.Int).Value = this.ReturnPrice;
          _cmd.Parameters.Add("@pReturnUnits"                 , SqlDbType.Int).Value = this.ReturnUnits;
          _cmd.Parameters.Add("@pPercentatgeCostReturn"       , SqlDbType.Decimal).Value = this.PercentatgeCostReturn;
          _cmd.Parameters.Add("@pGameURL"                     , SqlDbType.NVarChar).Value = this.GameURL;
          _cmd.Parameters.Add("@pMandatoryIC"                 , SqlDbType.Bit).Value = this.MandatoryIC;
          _cmd.Parameters.Add("@pShowBuyDialog"               , SqlDbType.Bit).Value = this.ShowBuyDialog;
          _cmd.Parameters.Add("@pPlayerCanCancel"             , SqlDbType.Bit).Value = this.PlayerCanCancel;
          _cmd.Parameters.Add("@pAutoCancel"                  , SqlDbType.Int).Value = this.AutoCancel;
          _cmd.Parameters.Add("@pTransferScreen"              , SqlDbType.Bit).Value = this.TransferScreen;
          _cmd.Parameters.Add("@pTransferTimeOut"             , SqlDbType.Int).Value = this.TransferTimeOut;
          //_cmd.Parameters.Add("@pPlayTable"              , SqlDbType.Xml    ).Value = this.PlayTable;
          // Last update:
          this.LastUpdate = this.LastModification;
          _cmd.Parameters.Add("@pLastUpdate", SqlDbType.DateTime).Value = this.LastUpdate;


          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }

          Log.Error(String.Format("PromoGame.DB_Update -> PromoGameId: {0}", this.Id));
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("PromoGame.DB_Update -> PromoGameId: {0}", this.Id));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Update

    /// <summary>
    /// Delete an specific CountR from database
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal Boolean DB_Delete(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;

      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" DELETE                     ");
        _sb.AppendLine("   FROM   PROMOGAMES ");
        _sb.AppendLine("  WHERE   PG_ID = @pId      ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pId", SqlDbType.Int).Value = this.Id;

          if (_cmd.ExecuteNonQuery() == 1)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("PROMOGAMES.DB_Delete -> Id: {0}", this.Id));
        Log.Exception(_ex);
      }

      return false;
    } // DB_Delete

    internal Boolean DB_CheckPromotionWithPromogame(SqlTransaction SqlTrx)
    {
      StringBuilder _sb;
      Object _obj;
      try
      {
        _sb = new StringBuilder();
        _sb.AppendLine(" SELECT *                                 ");
        _sb.AppendLine("   FROM   PROMOTIONS                      ");
        _sb.AppendLine("  WHERE   PM_PROMOGAME_ID = @pPromogameID ");

        using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pPromogameID", SqlDbType.Int).Value = this.Id;

          _obj = _cmd.ExecuteScalar();

          if (_obj != null && _obj != DBNull.Value)
          {
            return true;
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("PROMOGAME.DB_Delete -> PG_ID: {0}", this.Id));
        Log.Exception(_ex);
      }

      return false;
    }

    #endregion

    #region " Private Methods "

    /// <summary>
    /// Gets Promo Game select query
    /// </summary>
    /// <returns></returns>
    private String DB_PromoGameQuery_Select(Boolean PromoGameIdFilter, String GameTypeListString, String ReturnTypeListString, Boolean PromotionIdFilter)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("    SELECT   PG_ID                                                      "); // 0
      _sb.AppendLine("           , PG_NAME                                                    "); // 1
      _sb.AppendLine("           , PG_TYPE                                                    "); // 2
      _sb.AppendLine("           , PG_PRICE                                                   "); // 3
      _sb.AppendLine("           , PG_PRICE_UNITS                                             "); // 4
      _sb.AppendLine("           , PG_RETURN_PRICE                                            "); // 5
      _sb.AppendLine("           , PG_RETURN_UNITS                                            "); // 6
      _sb.AppendLine("           , PG_PERCENTATGE_COST_RETURN                                 "); // 7
      _sb.AppendLine("           , PG_GAME_URL                                                "); // 8
      _sb.AppendLine("           , PG_MANDATORY_IC                                            "); // 9
      _sb.AppendLine("           , PG_SHOW_BUY_DIALOG                                         "); // 10
      _sb.AppendLine("           , PG_PLAYER_CAN_CANCEL                                       "); // 11
      _sb.AppendLine("           , PG_AUTOCANCEL                                              "); // 12
      _sb.AppendLine("           , PG_TRANSFER_SCREEN                                         "); // 13
      _sb.AppendLine("           , PG_TRANSFER_TIMEOUT                                        "); // 14
      _sb.AppendLine("           , PG_LAST_UPDATE                                             "); // 15
      _sb.AppendLine("           , ISNULL(PM_PROMOTION_ID, 0) AS PROMOTION_ID                 "); // 16
      _sb.AppendLine("           , ISNULL(ACP_INI_BALANCE, 0) AS PROMO_PRIZE                  "); // 17
      _sb.AppendLine("           , ISNULL(PM_TYPE, 0) AS PROMOTION_TYPE                       "); // 18
      _sb.AppendLine("      FROM   PROMOGAMES                                                 ");
      _sb.AppendLine("LEFT  JOIN   PROMOTIONS  ON PG_ID = PM_PROMOGAME_ID                     ");
      _sb.AppendLine("LEFT  JOIN   ACCOUNT_PROMOTIONS  ON PM_PROMOTION_ID = ACP_PROMO_ID      ");
      _sb.AppendLine("     WHERE   1 = 1                                                      ");

      if (PromoGameIdFilter)
      {
        _sb.AppendLine(" AND PG_ID = @pPromoGameId ");
      }

      if (GameTypeListString != String.Empty)
      {
        _sb.AppendLine(" AND PG_TYPE IN (" + GameTypeListString + ") ");
      }

      if (ReturnTypeListString != String.Empty)
      {
        _sb.AppendLine("    AND   PG_RETURN_UNITS IN (" + ReturnTypeListString + ") ");
        _sb.AppendLine("    AND   PG_RETURN_PRICE = 'TRUE'");
      }

      if (PromotionIdFilter)
      {
        _sb.AppendLine(" AND (PM_PROMOGAME_ID IS NULL OR PM_PROMOTION_ID = @pPromotionId) ");
      }

      _sb.AppendLine(" GROUP BY   PG_ID                      ");
      _sb.AppendLine("          , PG_NAME                    ");
      _sb.AppendLine("          , PG_TYPE                    ");
      _sb.AppendLine("          , PG_PRICE                   ");
      _sb.AppendLine("          , PG_PRICE_UNITS             ");
      _sb.AppendLine("          , PG_RETURN_PRICE            ");
      _sb.AppendLine("          , PG_RETURN_UNITS            ");
      _sb.AppendLine("          , PG_PERCENTATGE_COST_RETURN ");
      _sb.AppendLine("          , PG_GAME_URL                ");
      _sb.AppendLine("          , PG_MANDATORY_IC            ");
      _sb.AppendLine("          , PG_SHOW_BUY_DIALOG         ");
      _sb.AppendLine("          , PG_PLAYER_CAN_CANCEL       ");
      _sb.AppendLine("          , PG_AUTOCANCEL              ");
      _sb.AppendLine("          , PG_TRANSFER_SCREEN         ");
      _sb.AppendLine("          , PG_TRANSFER_TIMEOUT        ");
      _sb.AppendLine("          , PG_LAST_UPDATE             ");
      _sb.AppendLine("          , PM_PROMOTION_ID            ");
      _sb.AppendLine("          , ACP_INI_BALANCE            ");
      _sb.AppendLine("          , PM_TYPE                    ");

      return _sb.ToString();

    } // DB_PromoGameQuery_Select

    /// <summary>
    /// To get all promo games
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private List<PromoGame> DB_GetAll(SqlTransaction SqlTrx)
    {
      List<PromoGame> _PromoGame_list;
      PromoGame _PromoGame;

      _PromoGame_list = new List<PromoGame>();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_PromoGameQuery_Select(false, String.Empty, String.Empty, false), SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              _PromoGame = this.PromoGameMappFromSqlReader(_sql_reader, SqlTrx);

              if (_PromoGame != null)
              {
                _PromoGame_list.Add(_PromoGame);
              }
            }
          }

          return _PromoGame_list;
        }
      }
      catch (Exception _ex)
      {
        Log.Error("PromoGame.DB_GetAllPromoGame --> Error on read all Promo Games list");
        Log.Exception(_ex);
      }

      return null;

    } // DB_GetAll

    /// <summary>
    /// To get promo games filtered by game type list and promotion id
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private List<PromoGame> DB_GetByGameTypeListAndPromotionId(List<GameType> GameTypeList, Int64 PromotionId, SqlTransaction SqlTrx)
    {
      List<PromoGame> _promo_game_list;
      PromoGame _promo_game;

      _promo_game_list = new List<PromoGame>();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_ComboPromoGameQuery_Select(this.GameTypeListToString(GameTypeList)), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pPromotionId", SqlDbType.Int).Value = PromotionId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              _promo_game = this.PromoGameMappFromSqlReader(_sql_reader, SqlTrx);

              if (_promo_game != null)
              {
                _promo_game_list.Add(_promo_game);
              }
            }
          }

          return _promo_game_list;
        }
      }
      catch (Exception _ex)
      {
        Log.Error("PromoGame.DB_GetAllPromoGame --> Error on read Promo Games list");
        Log.Exception(_ex);
      }

      return null;

    } // DB_GetByGameTypeListAndPromotionId

    /// <summary>
    /// To get a list of promo games filtered by game type list and prize type list
    /// </summary>
    /// <param name="GameTypeList"></param>
    /// <param name="PrizeTypeList"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private List<PromoGame> DB_GetByGameTypeListAndPrizeTypeList(List<GameType> GameTypeList, List<PrizeType> PrizeTypeList, SqlTransaction SqlTrx)
    {
      List<PromoGame> _promo_game_list;
      PromoGame _promo_game;

      _promo_game_list = new List<PromoGame>();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_PromoGameQuery_Select(false, this.GameTypeListToString(GameTypeList), this.PrizeTypeListToString(PrizeTypeList), false), SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              _promo_game = this.PromoGameMappFromSqlReader(_sql_reader, SqlTrx);

              if (_promo_game != null)
              {
                _promo_game_list.Add(_promo_game);
              }
            }
          }

          return _promo_game_list;
        }
      }
      catch (Exception _ex)
      {
        Log.Error("PromoGame.DB_GetAllPromoGame --> Error on read Promo Games list");
        Log.Exception(_ex);
      }

      return null;

    } // DB_GetByGameTypeListAndPrizeTypeList   

    /// <summary>
    /// To get a list of promo games filtered by game type list and prize type list and promotion id
    /// </summary>
    /// <param name="GameTypeList"></param>
    /// <param name="PrizeTypeList"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private List<PromoGame> DB_GetByGameTypeListAndPrizeTypeListAndPromotionId(List<GameType> GameTypeList, List<PrizeType> PrizeTypeList, long PromotionId, SqlTransaction SqlTrx)
    {
      List<PromoGame> _promo_game_list;
      PromoGame _promo_game;

      _promo_game_list = new List<PromoGame>();

      try
      {
        using (SqlCommand _cmd = new SqlCommand(DB_PromoGameQuery_Select(false, this.GameTypeListToString(GameTypeList), this.PrizeTypeListToString(PrizeTypeList), true), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pPromotionId", SqlDbType.Int).Value = PromotionId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              _promo_game = this.PromoGameMappFromSqlReader(_sql_reader, SqlTrx);

              if (_promo_game != null)
              {
                _promo_game_list.Add(_promo_game);
              }
            }
          }

          return _promo_game_list;
        }
      }
      catch (Exception _ex)
      {
        Log.Error("PromoGame.DB_GetAllPromoGame --> Error on read Promo Games list");
        Log.Exception(_ex);
      }

      return null;

    } // DB_GetByGameTypeListAndPrizeTypeListAndPromotionId   

    /// <summary>
    /// Return a string with the game type ids from the list
    /// </summary>
    /// <param name="GameTypeList"></param>
    /// <returns></returns>
    private String GameTypeListToString(List<GameType> GameTypeList)
    {
      StringBuilder _game_type_string;

      _game_type_string = new StringBuilder();

      if (GameTypeList != null)
      {
        foreach (GameType _game_type in GameTypeList)
        {
          _game_type_string.Append((Int16)_game_type + ",");
        }
      }
      return _game_type_string.ToString().TrimEnd(',');
    } // GameTypeListToString

    /// <summary>
    /// Return a string with the prize type ids from the list
    /// </summary>
    /// <param name="ReturnTypeList"></param>
    /// <returns></returns>
    private String PrizeTypeListToString(List<PrizeType> ReturnTypeList)
    {
      StringBuilder _prize_type_string;

      _prize_type_string = new StringBuilder();

      foreach (PrizeType _prize_type in ReturnTypeList)
      {
        _prize_type_string.Append((Int16)_prize_type + ",");
      }

      return _prize_type_string.ToString().TrimEnd(',');
    } // PrizeTypeListToString

    /// <summary>
    /// To get an specific promo game 
    /// </summary>
    /// <param name="PromoGameId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_GetByGameId(Int64 PromoGameId)
    {
      using (DB_TRX _db_trx = new DB_TRX())
      {
        return this.DB_ByGameId(PromoGameId, _db_trx.SqlTransaction);
      }
    } // DB_GetByGameId   

    /// <summary>
    /// To get an specific promo game
    /// </summary>
    /// <param name="PromoGameId"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_ByGameId(Int64 PromoGameId, SqlTransaction SqlTrx)
    {
      try
      {
        using (SqlCommand _cmd = new SqlCommand(this.DB_PromoGameQuery_Select(true, String.Empty, String.Empty, false), SqlTrx.Connection, SqlTrx))
        {
          _cmd.Parameters.Add("@pPromoGameId", SqlDbType.Int).Value = PromoGameId;

          using (SqlDataReader _sql_reader = _cmd.ExecuteReader())
          {
            if (_sql_reader.Read())
            {
              if (!this.DB_PromoGameDataMapper(_sql_reader, SqlTrx))
              {
                return false;
              }
            }
          }
          return true;
        }
      }
      catch (Exception _ex)
      {
        Log.Error(String.Format("PromoGame.DB_ByGameId -> PromoGameId: {0}", PromoGameId));
        Log.Exception(_ex);
      }

      return false;

    } // DB_ByGameId

    /// <summary>
    /// Load PromoGame data from SQLReader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private Boolean DB_PromoGameDataMapper(SqlDataReader SqlReader, SqlTransaction SqlTrx)
    {
      return DB_PromoGameDataMapper(this, SqlReader, SqlTrx);
    } // DB_PromoGameDataMapper

    private Boolean DB_PromoGameDataMapper(PromoGame PromoGame, SqlDataReader SqlReader, SqlTransaction SqlTrx)
    {
      try
      {

        PromoGame.Id                    = SqlReader.GetInt64(DB_PG_ID);
        PromoGame.Name                  = SqlReader.GetString(DB_PG_NAME);
        PromoGame.Type                  = (GameType)SqlReader.GetInt64(DB_PG_TYPE);
        PromoGame.Price                 = SqlReader.GetDecimal(DB_PG_PRICE);
        PromoGame.PriceUnits            = (PrizeType)SqlReader.GetInt32(DB_PG_PRICE_UNITS);
        PromoGame.ReturnPrice           = SqlReader.GetBoolean(DB_PG_RETURN_PRICE);
        PromoGame.ReturnUnits           = (PrizeType)SqlReader.GetInt32(DB_PG_RETURN_UNITS);
        PromoGame.PercentatgeCostReturn = SqlReader.GetDecimal(DB_PG_PERCENTATGE_COST_RETURN);
        PromoGame.GameURL               = SqlReader.GetString(DB_PG_GAME_URL);
        PromoGame.MandatoryIC           = SqlReader.GetBoolean(DB_PG_MANDATORY_IC);
        PromoGame.ShowBuyDialog         = SqlReader.GetBoolean(DB_PG_SHOW_BUY_DIALOG);
        PromoGame.PlayerCanCancel       = SqlReader.GetBoolean(DB_PG_PLAYER_CAN_CANCEL);
        PromoGame.AutoCancel            = SqlReader.GetInt32(DB_PG_AUTOCANCEL);
        PromoGame.TransferScreen        = SqlReader.GetBoolean(DB_PG_TRANSFER_SCREEN);
        PromoGame.TransferTimeOut       = SqlReader.GetInt32(DB_PG_TRANSFER_TIME_OUT);       
        PromoGame.LastUpdate            = SqlReader.GetDateTime(DB_PG_LAST_UPDATE);
        PromoGame.PromotionId           = SqlReader.GetInt64(DB_PG_PROMOTION_ID);
        PromoGame.MinimumPrize          = SqlReader.GetDecimal(DB_PG_MINIMUM_PRIZE);
        PromoGame.PromotionType         = SqlReader.GetInt32(DB_PG_PROMOTION_TYPE); 

        return true;
      }
      catch (Exception _ex)
      {
        Log.Error("Error on DB_PromoGameDataMapper");
        Log.Exception(_ex);
      }

      PromoGame = null;

      return false;

    } // DB_PromoGameDataMapper    

    /// <summary>
    /// Mapp PromoGame from sql reader
    /// </summary>
    /// <param name="SqlReader"></param>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    private PromoGame PromoGameMappFromSqlReader(SqlDataReader SqlReader, SqlTransaction SqlTrx)
    {
      PromoGame _promoGame;

      _promoGame = new PromoGame();

      if (!DB_PromoGameDataMapper(_promoGame, SqlReader, SqlTrx))
      {
        Log.Error(String.Format("PromoGame.PromoGameMappFromSqlReader -> Error on load PromoGameId: {0}", SqlReader.GetInt64(DB_PG_ID)));
      }

      return _promoGame;
    } // PromoGameMappFromSqlReader


    /// <summary>
    /// Query that set the combo values in promotions-edit forms
    /// </summary>
    /// <param name="GameTypeListString"></param>
    /// <returns></returns>
    private String DB_ComboPromoGameQuery_Select(String GameTypeListString)
    {
      StringBuilder _sb;

      _sb = new StringBuilder();

      _sb.AppendLine("    SELECT   PG_ID                                                      "); // 0
      _sb.AppendLine("           , PG_NAME                                                    "); // 1
      _sb.AppendLine("           , PG_TYPE                                                    "); // 2
      _sb.AppendLine("           , PG_PRICE                                                   "); // 3
      _sb.AppendLine("           , PG_PRICE_UNITS                                             "); // 4
      _sb.AppendLine("           , PG_RETURN_PRICE                                            "); // 5
      _sb.AppendLine("           , PG_RETURN_UNITS                                            "); // 6
      _sb.AppendLine("           , PG_PERCENTATGE_COST_RETURN                                 "); // 7
      _sb.AppendLine("           , PG_GAME_URL                                                "); // 8
      _sb.AppendLine("           , PG_MANDATORY_IC                                            "); // 9
      _sb.AppendLine("           , PG_SHOW_BUY_DIALOG                                         "); // 10
      _sb.AppendLine("           , PG_PLAYER_CAN_CANCEL                                       "); // 11
      _sb.AppendLine("           , PG_AUTOCANCEL                                              "); // 12
      _sb.AppendLine("           , PG_TRANSFER_SCREEN                                         "); // 13
      _sb.AppendLine("           , PG_TRANSFER_TIMEOUT                                        "); // 14
      _sb.AppendLine("           , PG_LAST_UPDATE                                             "); // 15
      _sb.AppendLine("           , ISNULL(PM_PROMOTION_ID, 0) AS PROMOTION_ID                 "); // 16
      _sb.AppendLine("           , 0.0  AS PROMO_PRIZE                                          "); // 17
      _sb.AppendLine("           , ISNULL(PM_TYPE, 0) AS PROMOTION_TYPE                       "); // 18
      _sb.AppendLine("      FROM   PROMOGAMES                                                 ");
      _sb.AppendLine("LEFT  JOIN   PROMOTIONS  ON PG_ID = PM_PROMOGAME_ID                     ");
      _sb.AppendLine("     WHERE  (PM_PROMOGAME_ID IS NULL OR PM_PROMOTION_ID = @pPromotionId)");
      _sb.AppendLine("       AND   PG_TYPE IN (" + GameTypeListString + ")                    ");
    
      return _sb.ToString();

    } // DB_ComboPromoGameQuery_Select
    
    #endregion

    #region "Internal Methods"

    /// <summary>
    /// Save PromoGame DB Method
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    internal virtual Boolean DB_Save(SqlTransaction SqlTrx)
    {
      if (this.IsNew || this.Id == 0)
      {
        return this.DB_Insert(SqlTrx);
      }

      if (this.IsModified)
      {
        return this.DB_Update(SqlTrx);
      }

      return true;
    } // DB_Save

    #endregion

    #endregion " Private Methods "

    #region " Special Methods for Testing "

    /// <summary>
    /// Public method to Get a List of Objects
    /// </summary>
    /// <param name="SqlTrans"></param>
    /// <returns></returns>
    public List<PromoGame> GetListPromoGame(SqlTransaction SqlTrans)
    {
      try
      {
        return DB_GetAll(SqlTrans);
      }
      catch (ArgumentException)
      {
        return null; // no object  with that id was found
      }
    } // GetListPromoGame

    /// <summary>
    /// Public method to test Get Object by Id
    /// </summary>
    /// <param name="PromoGameId"></param>
    /// <returns></returns>
    public PromoGame GetPromoGameById(Int64 PromoGameId)
    {
      try
      {
        DB_GetByGameId(PromoGameId);

        return this;
      }
      catch (ArgumentException)
      {
        return null; // no object  with that id was found
      }

    } // GetPromoGameById

    /// <summary>
    /// Public method to test Insert Object
    /// </summary>
    /// <param name="SqlTrans"></param>
    /// <returns></returns>
    public Boolean InsertPromoGame(SqlTransaction SqlTrans)
    {
      try
      {
        return DB_Insert(SqlTrans);
      }
      catch (ArgumentException)
      {
        return false; // no object  Not inserted
      }
    } // InsertPromoGame

    /// <summary>
    /// Public method to test update an Object
    /// </summary>
    /// <param name="SqlTrans"></param>
    /// <returns></returns>
    public Boolean UpdatePromoGame(SqlTransaction SqlTrans)
    {
      try
      {
        return DB_Update(SqlTrans);
      }
      catch (ArgumentException)
      {
        return false; // not updated
      }
    } // UpdatePromoGame

    #endregion " Special Methods for Testing "

  } // PromoGame
}
