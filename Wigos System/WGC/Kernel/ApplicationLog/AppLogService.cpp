//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : AppLogService.cpp
//   DESCRIPTION : Functions and Methods to Application log service.
//        AUTHOR : CIR
// CREATION DATE : 02-APR-2002
// 
// REVISION HISTORY
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 02-APR-2002 CIR    Initial draft.
// 
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_MISC
#define INCLUDE_IPC_API
#define INCLUDE_APPLICATION_LOG
#define INCLUDE_PRIVATE_LOG
#define INCLUDE_NLS_API
#define INCLUDE_SERVICE
#include "CommonDef.h"

#include "AppLogInternals.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------
#define MY_SERVICE_NAME             _T("APP_LOG")
#define MY_SERVICE_DISPLAY_NAME     _T("Application Log")
#define MY_SERVICE_HELP             NULL
#define MY_SERVICE_IPC_NODE_NAME    IPC_NODE_NAME_APP_LOG
#define LOG_PATH                    _T("..\\\\LogFiles\\\\")
//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------
APPLOG_CONTROL GLB_AppLogControl;

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------
// Service Interface
BOOL ApplicationLogInit (TYPE_SERVICE_DATA * pServiceData);
BOOL ApplicationLogMain (TYPE_SERVICE_DATA * pServiceData);
BOOL ApplicationLogStop (TYPE_SERVICE_DATA * pServiceData);
BOOL AppLog_GetEnvironment (void);

//------------------------------------------------------------------------------
//  PUBLIC DATA STRUCTURES 
//------------------------------------------------------------------------------
TYPE_SERVICE GLB_Service = { sizeof (TYPE_SERVICE),    // Application name
                             MY_SERVICE_NAME,           // Service name
                             MY_SERVICE_DISPLAY_NAME,   // Service display name
                             MY_SERVICE_HELP,           // Help
                             TRUE,                      // Flag: IPC
                             FALSE,                     // Flag: DB
                             TRUE,                      // Flag: Logger
                             FALSE,                     // Flag: Alarm Manager 
                             MY_SERVICE_IPC_NODE_NAME,  // IPC node name 
                             ApplicationLogInit,        // Init routine
                             ApplicationLogMain,        // Main routine
                             ApplicationLogStop };      // Stop routine

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PURPOSE : Initializes the ApplicationLog service. 
//
//  PARAMS :
//     - INPUT : 
//         - pServiceData, pointer to application data
//
//     - OUTPUT : 
//
// RETURNS :
//
//   NOTES :
//
static BOOL ApplicationLogInit (TYPE_SERVICE_DATA * pServiceData)
{
  TCHAR           _function_name [] = _T("ApplicationLogInit");
  BOOL            _rc_bool;
  HANDLE          _handle;
  HANDLE          _timer;

  // Reset the Control Data
  memset (&GLB_AppLogControl, 0, sizeof (APPLOG_CONTROL));

  if ( pServiceData->control_block != sizeof (TYPE_SERVICE_DATA) )
  {
    // Error log
    ServiceLogWrongSize (_function_name, sizeof (TYPE_SERVICE_DATA), pServiceData->control_block);
    ServiceStop ();

    return FALSE;
  } // if

  // Init log file environment
  _rc_bool = AppLog_GetEnvironment ();
  if ( ! _rc_bool )
  {
    // Error log
    ServiceLogError (_function_name, _T("AppLog_GetEnvironment"), FALSE);
    ServiceStop ();

    return FALSE;
  } // if

  _handle = CreateEvent (NULL,  // Security
                         FALSE, // No manual Reset
                         FALSE, // Signaled
                         NULL); // Name
  
  if ( _handle == NULL )
  {
    ServiceLogError (_function_name, _T("CreateEvent"), NULL);

    return FALSE;
  } // if
 
  if ( ServiceTimerCreate (&_timer, _function_name) == FALSE )
  {
    ServiceLogError (_function_name, _T(""), FALSE);

    return FALSE;
  } // if

  // Set the IPC node instance Id
  GLB_AppLogControl.ipc_node_id                 = pServiceData->ipc_node_instance_id; 
  
  // Store the handles
  GLB_AppLogControl.handles[IDX_HANDLE_STOP]    = pServiceData->stop_event;
  GLB_AppLogControl.handles[IDX_HANDLE_RECEIVE] = _handle;
  GLB_AppLogControl.handles[IDX_HANDLE_TIMER]   = _timer;

  // Set the event
  GLB_AppLogControl.ipc_message.event           = _handle;
  // Set the context
  GLB_AppLogControl.sql_context                 = pServiceData->sql_context;
  
  pServiceData->p_user_data = &GLB_AppLogControl;

  return TRUE;
}  // ApplicationLogInit

//-----------------------------------------------------------------------------
// PURPOSE : Releases any data allocated by the Applog.
//
//  PARAMS :
//     - INPUT : None
//
//     - OUTPUT : None
//
// RETURNS :
//     - None
//
//   NOTES :
//
static BOOL ApplicationLogStop (TYPE_SERVICE_DATA * pServiceData)
{   
  TCHAR           _function_name [] = _T("ApplicationLogStop");
  APPLOG_CONTROL  * p_applog_control;
  HANDLE          _handle;
  BOOL            _rc_bool;

  if ( pServiceData->control_block != sizeof (TYPE_SERVICE_DATA) )
  {
    // Error log
    ServiceLogWrongSize (_function_name, sizeof (TYPE_SERVICE_DATA), pServiceData->control_block);
    ServiceStop ();

    return FALSE;
  } // if

  _rc_bool = TRUE;

  if ( pServiceData->p_user_data != NULL )
  {
    p_applog_control = (APPLOG_CONTROL *) pServiceData->p_user_data;

    // Status
    _rc_bool = AppLog_WriteStatus (p_applog_control->p_log_file, LOG_STATUS_STOPPED);
    // Close log file
    LogFileClose (p_applog_control->p_log_file);
    
    _handle = p_applog_control->handles[IDX_HANDLE_RECEIVE];
    if ( _handle != NULL )
    { 
      if ( CloseHandle (_handle) == FALSE )
      {
        ServiceLogError (_function_name, _T("CloseHandle"), FALSE);
        _rc_bool = FALSE;
      }
    } // if

    _handle = p_applog_control->handles[IDX_HANDLE_TIMER];
    if ( _handle != NULL )
    { 
      if ( ServiceTimerDelete (_handle, _function_name) == FALSE )
      {
        ServiceLogError (_function_name, _T("ServiceTimerDelete"), FALSE);
        _rc_bool = FALSE;
      }
    } // if
   
    // Reset user data
    pServiceData->p_user_data = NULL;

  } // if

  return _rc_bool;

} // ApplicationLogStop

//-----------------------------------------------------------------------------
// PURPOSE : Service main function
//
//  PARAMS :
//     - INPUT 
//
//     - OUTPUT
//
// RETURNS :
//     - TRUE on success
//     - FALSE on error
//
//   NOTES :
//
static BOOL ApplicationLogMain (TYPE_SERVICE_DATA * pServiceData)
{
  TCHAR           _function_name [] = _T("ApplicationLogMain");
  APPLOG_CONTROL  * p_applog_control;
  BOOL            _stop;
  BOOL            _rc_bool;
  DWORD           _error_code;
  EVENT_TYPE      _event_type;
  BOOL            _error;

  _error_code = FALSE;

  if ( pServiceData->control_block != sizeof (TYPE_SERVICE_DATA) )
  {
    ServiceLogWrongSize (_function_name, sizeof (TYPE_SERVICE_DATA), pServiceData->control_block);
    ServiceStop ();

    return FALSE;
  } // if

  p_applog_control = (APPLOG_CONTROL *) pServiceData->p_user_data;

  // Enable the timer
  _rc_bool = ServiceTimerEnable (p_applog_control->handles[IDX_HANDLE_TIMER], LOG_TIMER_INTERVAL, _function_name);
  if ( _rc_bool == FALSE )
  {
    ServiceLogError (_function_name, _T("ServiceTimerEnable"), FALSE);
    ServiceStop ();

    return FALSE;
  } // if

  // Post a new receive
  _rc_bool = IpcMessageReceive (&p_applog_control->ipc_message);
  if ( _rc_bool == FALSE )
  {
    ServiceLogError (_function_name, _T("IpcMessageReceive"), FALSE);
    ServiceStop ();

    return FALSE;
  } // if

  // Control Log File
  _rc_bool = AppLog_ControlLogFile (p_applog_control);
  if ( _rc_bool == FALSE )
  {
    ServiceLogError (_function_name, _T("AppLog_ControlLogFile"), FALSE);
    ServiceStop ();

    return FALSE;
  } // if

  _rc_bool = AppLog_WriteStatus (p_applog_control->p_log_file, LOG_STATUS_STARTED);
  if ( _rc_bool == FALSE )
  {
    ServiceLogError (_function_name, _T("AppLog_WriteStatus"), FALSE);
    ServiceStop ();

    return FALSE;
  } // if


  _stop  = FALSE;
  _error = FALSE;
 
  while ( ! _stop )
  {
    _event_type = WaitEvent (p_applog_control);

    switch ( _event_type )
    {
      case EVENT_RECEIVE:
      {
        // Process the incoming message
        IpcMessageHandler (&p_applog_control->ipc_message);

        // Post a new receive
        _rc_bool = IpcMessageReceive (&p_applog_control->ipc_message);
        if ( _rc_bool == FALSE )
        {
          ServiceLogError (_function_name, _T("IpcMessageReceive"), FALSE);
          ServiceStop ();
          _error = TRUE;
        } // if
      }
      break;

      case EVENT_TIMER:
      {
        // Disable the timer
        ServiceTimerDisable (p_applog_control->handles[IDX_HANDLE_TIMER], _function_name);

        // Timer event
        TimerHandler ();
        
        // Enable the timer
        _rc_bool = ServiceTimerEnable (p_applog_control->handles[IDX_HANDLE_TIMER], LOG_TIMER_INTERVAL, _function_name);
        if ( _rc_bool == FALSE )
        {
          ServiceLogError (_function_name, _T("ServiceTimerEnable"), FALSE);
          ServiceStop ();
          _error = TRUE;
        } // if
      }
      break;
 
      case EVENT_STOP:
      {
        _stop = TRUE;
      }
      break;
 
      case EVENT_ERROR: // Fall through
      default:
      {
        // Error log
        ServiceLogError (_function_name, _T("WaitEvent"), _event_type);
        ServiceStop ();
        _error  = TRUE;
      }
      break;
    } // switch
  } // while

  return (_error == FALSE);

} // ApplicationLogMain

//-----------------------------------------------------------------------------
// PURPOSE : Initializes Log file environment. 
//
//  PARAMS :
//     - INPUT : 
//
//     - OUTPUT : 
//
// RETURNS :
//     - TRUE on success
//     - FALSE on error
//
//   NOTES :
//
static BOOL AppLog_GetEnvironment ()
{
  TCHAR   _function_name [] = _T("AppLog_GetEnvironment");
  //TCHAR   _log_files_path [COMMON_MAX_PATH_SIZE + 1];
  DWORD   _system_language;
  WORD    _rc;
  BOOL    _rc_bool;

  // Get log files path
  _rc_bool = AppLog_GetLogConfiguration (&_system_language);
  if ( ! _rc_bool )
  {
    return FALSE;
  } // if

  // Assign log files path
  _tcscpy (GLB_AppLogControl.working_directory, LOG_PATH/*_log_files_path*/);

  // Set system language 
  _rc = NLS_SetLanguage ((WORD) _system_language);
  if ( _rc != NLS_STATUS_OK )
  {
    // Error log
    ServiceLogError (_function_name, _T("NLS_SetLanguage"), _rc);

    return FALSE;
  } // if

  //// Format file name
  //AppLog_FormatFileName (&GLB_AppLogControl, _full_file_name);

  //// Open log file
  //_rc_bool = LogFileOpen (_full_file_name, &GLB_AppLogControl);
  //if ( ! _rc_bool )
  //{
  //  // Error opening file
  //  // Error log
  //  _stprintf (_aux_char, _T("%hu"), _rc_bool);
  //  PrivateLog (2, SERVICE_NAME_PRIVATE_LOG, _function_name, _aux_char, _T("LogFileOpen"));

  //  return FALSE;
  //} // if

  //// Insert service started message
  //_rc_bool = AppLog_WriteStatus (&GLB_AppLogControl, LOG_STATUS_STARTED);
  //if ( ! _rc_bool )
  //{
  //  // Error opening file
  //  // Error log
  //  _stprintf (_aux_char, _T("%hu"), _rc_bool);
  //  PrivateLog (2, SERVICE_NAME_PRIVATE_LOG, _function_name, _aux_char, _T("AppLog_WriteStatus"));

  //  return FALSE;
  //} // if

  return TRUE;

} // AppLog_GetEnvironment
