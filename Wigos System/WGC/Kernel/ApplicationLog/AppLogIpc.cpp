//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : AppLogIpc.cpp
//   DESCRIPTION : Functions to handle the IPC Messages
//        AUTHOR : Andreu Juli�
// CREATION DATE : 06-NOV-2002
// 
// REVISION HISTORY
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 06-NOV-2002 AJQ    Initial draft.
// 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_MISC
#define INCLUDE_IPC_API
#define INCLUDE_NLS_API
#define INCLUDE_PRIVATE_LOG
#define INCLUDE_QUEUE_API
#define INCLUDE_SERVICE
#define INCLUDE_APPLICATION_LOG
#include "CommonDef.h"

#include "AppLogInternals.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATATYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------
static VOID CmdLog  (TYPE_IPC_MESSAGE *pIpcMessage);

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PURPOSE : Processes the incoming IPC messages
//
//  PARAMS :
//     - INPUT : pIpcMessage
//     - OUTPUT : None
//
// RETURNS :
//     - Nothing
//
//   NOTES :
//
VOID IpcMessageHandler (TYPE_IPC_MESSAGE *pIpcMessage)
{
  TCHAR _function_name [] = _T("IpcMessageHandler");
  DWORD _msg_cmd;

  // Check the output status returned by the API
  if ( pIpcMessage->api_output_params.output_1 != IPC_STATUS_OK )
  {
    ServiceLogError (_function_name, _T("Ipc_API"), pIpcMessage->api_output_params.output_1);

    return;
  } // if

  // Get the CMD
  _msg_cmd = pIpcMessage->ipc_oud_receive.msg_cmd;

  switch ( _msg_cmd )
  {
    case 0: // Temporal
    case 1: // Temporal
    case MSG_CMD_LOG:
    {
      // Log the received message
      CmdLog (pIpcMessage);
    }
    break;

    default:
    {
      ServiceLogUnexpected (_function_name, _T("MsgCmd"), _msg_cmd);
    }
  } // switch

} // IpcMessageHandler

//-----------------------------------------------------------------------------
// PURPOSE : Post a new receive of the IPC
//
//  PARAMS :
//     - INPUT/OUTPUT : pIpcMessage
//     - OUTPUT : None
//
// RETURNS :
//      - TRUE:  on success.
//      - FALSE: on failure
//
//   NOTES :

BOOL IpcMessageReceive (TYPE_IPC_MESSAGE * pIpcMessage)
{
  TCHAR                     _function_name [] = _T("IpcMessageReceive");
  API_INPUT_PARAMS          _api_input_params;
  IPC_IUD_RECEIVE           _ipc_iud_receive;
  WORD                      _rc_api;

  // Put the event to Non-Signaled
  ResetEvent (pIpcMessage->event); 

  // INPUT PARAMS :
  //   - API
  _api_input_params.control_block = sizeof (_api_input_params);
  _api_input_params.event_object  = pIpcMessage->event;
  _api_input_params.function_code = IPC_CODE_RECEIVE;
  _api_input_params.mode          = API_MODE_ASYNC;
  _api_input_params.sub_function_code = 0;
  //   - IPC
  _ipc_iud_receive.control_block = sizeof (_ipc_iud_receive);
  _ipc_iud_receive.node_inst_id  = GLB_AppLogControl.ipc_node_id;  
  _ipc_iud_receive.timeout       = IPC_TIMEOUT_INFINITE;
  // OUPUT PARAMS :
  //   - API
  pIpcMessage->api_output_params.control_block = sizeof (pIpcMessage->api_output_params);
  //   - IPC
  pIpcMessage->ipc_oud_receive.control_block   = sizeof (pIpcMessage->ipc_oud_receive);

  // API CALL
  _rc_api = Ipc_API (&_api_input_params,              // Input Params
                     &_ipc_iud_receive,               // Input User Data
                     &pIpcMessage->api_output_params, // Output Params
                     &pIpcMessage->ipc_oud_receive);  // Output User Data


  if ( _rc_api == API_STATUS_OK )
  {
    return TRUE;  
  }
  else
  {
    // Log the error
    ServiceLogError (_function_name, _T("Ipc_API"), _rc_api);
  }

  return FALSE;

} // IpcMessageReceive

//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------
static VOID CmdLog  (TYPE_IPC_MESSAGE *pIpcMessage)
{
  TCHAR _function_name [] = _T("CmdLog");
  BOOL  _rc_bool;

  // Control log file
  _rc_bool = AppLog_ControlLogFile (&GLB_AppLogControl);

  if ( _rc_bool == FALSE )
  {
    // Log err
    ServiceLogError (_function_name, _T("AppLog_ControlLogFile"), FALSE);

    return;
  }

  // Write message in log file
  _rc_bool = LogFileWrite (GLB_AppLogControl.p_log_file, 
                              (TYPE_LOG_MESSAGE *) pIpcMessage->ipc_oud_receive.user_buffer);
  
  if ( _rc_bool == FALSE )
  {
    // Log err
    ServiceLogError (_function_name, _T("LogFileWrite"), FALSE);

    return;
  } // if

} // CmdLog
