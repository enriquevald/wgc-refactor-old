//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : AppLogFile.cpp
// 
//   DESCRIPTION : Functions to handle the log file
// 
//        AUTHOR : Andreu Juli�
// 
// CREATION DATE : 11-NOV-2002
// 
// REVISION HISTORY
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 11-NOV-2002 AJQ    Initial draft.
// 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_MISC
#define INCLUDE_IPC_API
#define INCLUDE_NLS_API
#define INCLUDE_APPLICATION_LOG
#define INCLUDE_PRIVATE_LOG
#define INCLUDE_SERVICE
#include "CommonDef.h"

#include "AppLogInternals.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS
//------------------------------------------------------------------------------

#define APPLOG_MAX_SIZE_DATE    100

//------------------------------------------------------------------------------
// PRIVATE DATATYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------
BOOL  AppLog_IsCurrentDateFile (APPLOG_CONTROL * pApplogControl);

static BOOL AppLog_WriteFilename (FILE *pLogFile, TCHAR * pFullFileName);

static BOOL LogFileFormatMessage (TYPE_LOG_MESSAGE * pLogMessage, TCHAR * pFormatedMessage);

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PURPOSE : Open log file
//
//  PARAMS :
//     - INPUT :
//         - pFullFileName, path name + file name
//
//     - OUTPUT :
//         - pApplogControl, file log structure to store file handle
//
// RETURNS :
//     - TRUE on success
//     - FALSE on error
//
//   NOTES :

FILE * LogFileOpen  (TCHAR * pPath, TCHAR * pFileName)
{
  TCHAR   _function_name[] = _T("LogFileOpen");
  FILE    * p_log_file;
  DWORD   _rc;

 if (!CreateDirectory (pPath, NULL))
 {
   _rc = GetLastError();
   if ( _rc != ERROR_ALREADY_EXISTS )
   {
     // Log error
     ServiceLogError (_function_name, _T("CreateDirectory"), _rc, LOG_TO_BOTH);
     return NULL;
   }
 }

  p_log_file = _tfopen (pFileName, _T("a"));
  if ( p_log_file == NULL )
  {
    // Log error
    ServiceLogError (_function_name, _T("_tfopen"), errno, LOG_TO_BOTH);
  } // if

  return p_log_file;

} // LogFileOpen

//-----------------------------------------------------------------------------
// PURPOSE : Close current log file
//
//  PARAMS :
//     - INPUT :
//        - pLogFile: LogFile pointer
//
//     - OUTPUT :
//        - None
//
// RETURNS : Nothing
//
//   NOTES :

VOID LogFileClose (FILE *pLogFile)
{
  TCHAR _function_name[] = _T("LogFileClose");

  if ( pLogFile != NULL )
  {
    if ( fclose (pLogFile) != 0 )
    {
      ServiceLogError (_function_name, _T(""), errno);
    } // if
  } // if

} // LogFileClose

//-----------------------------------------------------------------------------
// PURPOSE : Writes the message to the file
//
//  PARAMS :
//     - INPUT :
//        - pLogFile:  The file
//        - pMessage:  The message
//
//     - OUTPUT : 
//        - None
//
// RETURNS :
//     - TRUE on success
//     - FALSE on error
//
//   NOTES :

BOOL LogFileWrite (FILE * pLogFile, TYPE_LOG_MESSAGE * pLogMessage)
{
  TCHAR _function_name[] = _T("LogFileWrite");
  TCHAR _formated_message [LOG_MESSAGE_BUFFER_SIZE + 1];
  BOOL  _rc_bool;

  // Format log message
  _rc_bool = LogFileFormatMessage (pLogMessage, _formated_message);
  if ( ! _rc_bool )
  {
    // Log error
    ServiceLogError (_function_name, _T(""), _rc_bool);

    return FALSE;
  } // if

  // Write the log message to the file
  if ( _ftprintf (pLogFile, _T("%s\n"), _formated_message) < 0 )
  {
    // Log error
    ServiceLogError (_function_name, _T("_ftprintf"), errno);

    return FALSE;
  } // if

  // Flush 
  if ( fflush (pLogFile) == EOF )
  {
    // Log error
    ServiceLogError (_function_name, _T("fflush"), errno);

    return FALSE;
  }

  return TRUE;

} // LogFileWrite

//------------------------------------------------------------------------------
// PURPOSE: 
//
//  PARAMS:
//      - INPUT:
//
//      - OUTPUT: 
//          - pDateTimeString : Formatted date-time string
//
// RETURNS: None
//
//   NOTES: None

static BOOL     LogFileGetTimeString (TCHAR  * pTimeString)
{
  SYSTEMTIME  _curr_time;
  TCHAR       _date_string [APPLOG_MAX_SIZE_DATE + 1];      // Add terminating NULL char

  // Get current time from system
  GetLocalTime (&_curr_time);

  if ( GetDateFormat (LOCALE_USER_DEFAULT,              // Locale
                      DATE_SHORTDATE,                   // Options
                      &_curr_time,                      // Date
                      NULL,                             // Date format
                      _date_string,                     // Formatted string
                      APPLOG_MAX_SIZE_DATE) == 0 )  // Size of formatted string
  {
    pTimeString[0] = '\0';

    return TRUE; 
  }

  _stprintf (pTimeString, _T("%02hu:%02hu:%02hu.%02hu"), _curr_time.wHour,
                                                         _curr_time.wMinute,
                                                         _curr_time.wSecond,
                                                         (WORD) (_curr_time.wMilliseconds / 10));

  return TRUE;
} // LogFileGetTimeString

//------------------------------------------------------------------------------
// PURPOSE: Prefixes the input message with the current date and time.
// 
//  PARAMS:
//      - INPUT:
//        - pMessage: The original message
//
//      - OUTPUT:
//        - pLogMesg: The original message prefixed with date & time
// 
// RETURNS: Nothing
// 
//   NOTES:
//
static DWORD       LogFilePrefixTime (const TCHAR  * pMessage,
                                      const DWORD  BufferSize,
                                      TCHAR        * pLogMsg)
{
  DWORD     _max_len;
  TCHAR     _blank []  = _T(" ");
  DWORD     _blank_len = _tcslen (_blank);
  DWORD     _append;

  LogFileGetTimeString (pLogMsg);

  // 0         1         2         3         4         5         6         7         8
  // 0----5----0----5----0----5----0----5----0----5----0----5----0----5----0----5----0 ...
  // DD/MM/YYYY HH:MM:SS.CC CCCCCCCCCCCC MMMMMMMMMMMM FFFFFFFFFFFFFFFFFF IIIII MESSAGE ...
  // Max Length
  // Max Length
  _max_len = BufferSize - 1 - _tcslen (pLogMsg);

  if ( pMessage != NULL )
  {
    _append  = _tcslen (pMessage);
    if ( _append > _max_len )
    {
      _append = _max_len;    
    }
    
    if ( _append > 0 )
    {
      // Copy the pMessage
      _tcsncat (pLogMsg, _blank, _blank_len);
      _tcsncat (pLogMsg, pMessage, _append);
    }
  }

  return _tcslen (pLogMsg);
} // LogFilePrefixTime

//-----------------------------------------------------------------------------
// PURPOSE : Format log message 
//
//  PARAMS :
//    - INPUT :
//      - pLogMessage, pointer to the input/received message
//
//    - OUTPUT :
//      - pFormatedMessage, output buffer with the message formated
//
// RETURNS :
//    - TRUE on success
//    - FALSE on error
//
//   NOTES :

static BOOL LogFileFormatMessage (TYPE_LOG_MESSAGE * pLogMessage, TCHAR * pFormatedMessage)
{
  TCHAR               _function_name [] = _T("AppLog_LogFileFormatMessage");
  DWORD               _msg_len;

  // AJQ 04-SET-2002, Variable Length Messages -> Don't check the control block 
  //                  But the length of the string can be checked

  // Compute the message length
  _msg_len = _tcslen (pLogMessage->message);

  if ( _msg_len != pLogMessage->message_length )
  {
    // Error log
    ServiceLogWrongSize (_function_name, _msg_len, pLogMessage->message_length);

    return FALSE;
  } // if

  LogFilePrefixTime (pLogMessage->message, LOG_MESSAGE_BUFFER_SIZE, pFormatedMessage);

  return TRUE;

} // LogFileFormatMessage

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------
