//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: AppLogInternals.h
//   DESCRIPTION: Constants, types, variables definitions and prototypes 
//                for application log
//        AUTHOR: CIR
// CREATION DATE: 03-APR-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 04-DEC-2004 AJQ    10 DbLogs are read at once. Read Frequency 0.5s (before 1s)
// 06-NOV-2002 AJQ    Rewriting of some functions & Timer to read the DB
// 03-APR-2002 CIR    Initial draft.
//------------------------------------------------------------------------------

#ifndef __APP_LOG_INTERNALS_H
#define __APP_LOG_INTERNALS_H

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------
// Handles
#define IDX_HANDLE_STOP               0 
#define IDX_HANDLE_RECEIVE            1
#define IDX_HANDLE_TIMER              2 // Add a timer to Log DB Errors
#define NUM_HANDLES                   3 

#define SERVICE_NAME_PRIVATE_LOG      _T("ApplicationLogService")
#define LOG_STATUS_STARTED            0
#define LOG_STATUS_STOPPED            1

#define LOG_TIMER_INTERVAL            1000  // Milliseconds
#define LOG_MAX_DB_MSG                1

 //------------------------------------------------------------------------------
 //  PRIVATE DATATYPES 
 //------------------------------------------------------------------------------

typedef struct
{
  HANDLE                    event;
  API_OUTPUT_PARAMS         api_output_params;
  IPC_OUD_RECEIVE           ipc_oud_receive;

} TYPE_IPC_MESSAGE;

 typedef struct
 {
   DWORD   ipc_node_id;            // IPC node Id
   HANDLE  handles [NUM_HANDLES];
   DWORD   log_file_date;           // System date when file was created
   TCHAR   working_directory [COMMON_MAX_PATH_SIZE + 1];
   FILE    * p_log_file;

   SQL_CONTEXT      sql_context;   // Database
   TYPE_IPC_MESSAGE ipc_message;   // IPC Message
 
 } APPLOG_CONTROL;
 
 typedef enum
 {
   EVENT_ERROR   = 0,
   EVENT_RECEIVE = 1,
   EVENT_STOP    = 2,
   EVENT_TIMER   = 3
 
 } EVENT_TYPE;
 
 
 //------------------------------------------------------------------------------
 //  PRIVATE DATA STRUCTURES 
 //------------------------------------------------------------------------------
 extern APPLOG_CONTROL GLB_AppLogControl;

 //------------------------------------------------------------------------------
 //  PRIVATE FUNCTION PROTOTYPES 
 //------------------------------------------------------------------------------

// IPC
BOOL IpcMessageReceive (TYPE_IPC_MESSAGE * pIpcMessage);
VOID IpcMessageHandler (TYPE_IPC_MESSAGE * pIpcMessage);

// Timer
VOID TimerHandler (VOID);

// LogFile
FILE * LogFileOpen  (TCHAR * pPath, TCHAR * pFileName);
VOID   LogFileClose (FILE  * pLogFile);
BOOL   LogFileWrite (FILE  * pLogFile, TYPE_LOG_MESSAGE * pLogMessage);

BOOL  AppLog_GetLogConfiguration  (DWORD * pSystemLanguage);
void  AppLog_FormatFileName       (APPLOG_CONTROL * pApplogControl, TCHAR * pFullFileName);
BOOL  AppLog_ControlLogFile       (APPLOG_CONTROL * pApplogControl);

// AppLog
BOOL  AppLog_WriteStatus (FILE * pLogFile, WORD Status);


// Event ...
EVENT_TYPE WaitEvent (APPLOG_CONTROL *pAppLogControl);

 //------------------------------------------------------------------------------
 //  PUBLIC DATA STRUCTURES 
 //------------------------------------------------------------------------------
 
 //------------------------------------------------------------------------------
 //  PUBLIC CONSTANTS 
 //------------------------------------------------------------------------------
 
 //------------------------------------------------------------------------------
 //  PUBLIC DATATYPES 
 //------------------------------------------------------------------------------
 
 //------------------------------------------------------------------------------
 //  PUBLIC DATA STRUCTURES 
 //------------------------------------------------------------------------------
 
 //------------------------------------------------------------------------------
 //  PUBLIC FUNCTION PROTOTYPES 
 //------------------------------------------------------------------------------

#endif // __APP_LOG_INTERNALS_H
