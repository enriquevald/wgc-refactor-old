//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems
//------------------------------------------------------------------------------
//
// MODULE NAME :   APPLICATIONLOG.CPP
// DESCRIPTION :   Functions and Methods for ApplicationLog function
// AUTHOR :        CIR
// CREATION DATE : 02-04-2002
//
// REVISION HISTORY
//
// Date        Author Description
// ----------  ------ ----------------------------------------------------------
// 02-04-2002  CIR    Initial release
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//   INCLUDES
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_MISC
#define INCLUDE_IPC_API
#define INCLUDE_NLS_API
#define INCLUDE_APPLICATION_LOG
#define INCLUDE_PRIVATE_LOG
#define INCLUDE_SERVICE
#include "CommonDef.h"

#include "AppLogInternals.h"
 
//------------------------------------------------------------------------------
// PRIVATE CONSTANTS
//------------------------------------------------------------------------------

// Log file name format
#define LOG_FILE_NAME_FORMAT          _T("LOG%s.LOG")
#define LOG_DATE_SIZE                 50 

//------------------------------------------------------------------------------
// PRIVATE DATATYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------
BOOL  AppLog_IsCurrentDateFile (APPLOG_CONTROL * pApplogControl);
static BOOL LogFileFormatMessage (TYPE_LOG_MESSAGE * pLogMessage, TCHAR * pFormatedMessage);

static VOID AppLog_WriteFilename (FILE *pLogFile, TCHAR * pFullFileName);
 
//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------
 
//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PURPOSE : Wait for any event
//
//  PARAMS :
//     - INPUT : None
//         - pApplogControl, pointer to application data
//
//     - OUTPUT : None
//
// RETURNS :
//     - Event received
//
//   NOTES :
//
EVENT_TYPE WaitEvent (APPLOG_CONTROL *pAppLogControl)
{
  TCHAR _function_name[] = _T("WaitEvent");
  DWORD             _rc;
  DWORD             _idx_handle;

  _rc = WaitForMultipleObjects (NUM_HANDLES,               // #Handles
                                pAppLogControl->handles,   // The handles to wait for
                                FALSE,                     // Wait for all objects?
                                INFINITE);                 // No timeout

  switch ( _rc )
  {
    case WAIT_FAILED:
    {
      // Log error
      ServiceLogError (_function_name, _T("WaitForMultipleObjects"), _rc);

      return EVENT_ERROR;
    }
    break;

    default:
      if (   _rc >= WAIT_OBJECT_0 
          && _rc < WAIT_OBJECT_0 + NUM_HANDLES )
      {
        _idx_handle = _rc - WAIT_OBJECT_0;
        switch ( _idx_handle )
        {
          case IDX_HANDLE_STOP:
          {
            return EVENT_STOP;
          }
          break;

          case IDX_HANDLE_RECEIVE:
          {
            return EVENT_RECEIVE;
          }
          break;

          case IDX_HANDLE_TIMER:
          {
            return EVENT_TIMER;
          }
          break;
        
          default:
          break;
        } // switch
      } // if
    break;
  } // switch
 
  return EVENT_ERROR;

} // WaitEvent

//-----------------------------------------------------------------------------
// PURPOSE : Get log files path from registry
//
//  PARAMS :
//     - INPUT :
//
//     - OUTPUT : 
//         - SystemLanguage
//
// RETURNS :
//     TRUE, on success
//     FALSE, on error
//
//   NOTES :
//
BOOL  AppLog_GetLogConfiguration  (DWORD * pSystemLanguage)
{
  TCHAR _function_name[] = _T("AppLog_GetLogConfiguration");
  TCHAR _aux_char [COMMON_LOGGER_PARAMETER_SIZE];
  TYPE_CENTER_CONFIGURATION_EX  _center_configuration_ex;
  DWORD _rc;

  memset (&_center_configuration_ex, 0, sizeof (TYPE_CENTER_CONFIGURATION_EX));
  // Set control block size
  _center_configuration_ex.control_block = sizeof (TYPE_CENTER_CONFIGURATION_EX);

  //Get center configuration from registry
  _rc = Common_GetConfigurationEx (&_center_configuration_ex);
  if ( _rc != COMMON_OK )
  {
    // Error getting center configuration
    _stprintf (_aux_char, _T("%lu"), _rc);
    PrivateLog (2, SERVICE_NAME_PRIVATE_LOG, _function_name, _aux_char,  _T("Common_GetConfigurationEx"));

    return FALSE;
  } // if

  //// Remove blank spaces at the end of string (if any)
  //_path_len = _tcslen(_center_configuration_ex.log_path);
  //for ( _idx = (_path_len - 1); _idx >= 0; _idx -- )
  //{
  //  if ( _center_configuration_ex.log_path [_idx] == _T(' ') )
  //  {
  //    // Remove blank space
  //    _center_configuration_ex.log_path [_idx] = _T('\0');
  //  } // if
  //  else
  //  {
  //    // No blank spaces at the end
  //    break;
  //  } // else
  //} // for

  //// Check if log path is back slash ended
  //_path_len = _tcslen(_center_configuration_ex.log_path);
  //if ( _center_configuration_ex.log_path [_path_len - 1] != _T('\\') )
  //{
  //  // Not bach slash ended. Insert it
  //  _tcscat (_center_configuration_ex.log_path, _T("\\"));
  //} // if

  //// Assign log files path to output parameter
  //_tcscpy (pLogFilesPath, _center_configuration_ex.log_path);
  * pSystemLanguage = _center_configuration_ex.language;

  return TRUE;

} // AppLog_GetLogConfiguration

//-----------------------------------------------------------------------------
// PURPOSE : Format new log file name
//
//  PARAMS :
//     - INPUT :
//         - pApplogControl, pointer to application data
//
//     - OUTPUT :
//         - pFullFileName, path name + file name
//
// RETURNS :
//
//   NOTES :
//
VOID AppLog_FormatFileName (APPLOG_CONTROL * pApplogControl, TCHAR * pFormatedFileName)
{
  SYSTEMTIME       _current_time;
  TCHAR            _aux_str [COMMON_MAX_PATH_SIZE + 1];
  TCHAR            _aux_file_name [COMMON_MAX_PATH_SIZE + 1];
  TCHAR            _aux_path [COMMON_MAX_PATH_SIZE + 1];
  DWORD            _file_date;

  // Get current time from system
  GetLocalTime (&_current_time);

  // Format file name
  _stprintf (_aux_str, _T("%04hu%02hu%02hu"), _current_time.wYear, 
  _current_time.wMonth,  
  _current_time.wDay);

  _stprintf (_aux_file_name, LOG_FILE_NAME_FORMAT, _aux_str);
  _tcscpy (_aux_path, pApplogControl->working_directory);
  
  // Assign full file name to output parameter
  _stprintf (pFormatedFileName, _T("%s%s"), _aux_path, _aux_file_name);

  // Store file date in control structure
  // File date format: YYYYMMDD
  _file_date = 0;
  _file_date += (DWORD) ((_current_time.wYear) * 10000);
  _file_date += (DWORD) ((_current_time.wMonth) * 100);
  _file_date += (DWORD) _current_time.wDay;

  pApplogControl->log_file_date = _file_date;

} // AppLog_FormatFileName

//-----------------------------------------------------------------------------
// PURPOSE : Check if log file corresponds to current date
//
//  PARAMS :
//     - INPUT :
//         - pApplogControl, pointer to application data
//
//     - OUTPUT :
//
// RETURNS :
//     - TRUE, updated log file
//     - FALSE obsolete log file
//
//   NOTES :
//
BOOL  AppLog_IsCurrentDateFile (APPLOG_CONTROL * pApplogControl)
{
  TCHAR _function_name[] = _T("AppLog_IsCurrentDateFile");

  SYSTEMTIME       _current_time;
  DWORD            _current_file_date;

  // Get current time from system
  GetLocalTime (&_current_time);

  // Build current file date 
  // File date format: YYYYMMDD
  _current_file_date = 0;
  _current_file_date += (DWORD) ((_current_time.wYear) * 10000);
  _current_file_date += (DWORD) ((_current_time.wMonth) * 100);
  _current_file_date += (DWORD) _current_time.wDay;

  if ( pApplogControl->log_file_date != _current_file_date )
  {
    // Obsolete file
    return FALSE;
  } // if
  
  // Updated file
  return TRUE;

} // AppLog_IsCurrentDateFile

//-----------------------------------------------------------------------------
// PURPOSE : Insert status log message (started or stopped)
//
//  PARAMS :
//     - INPUT :
//         - pApplogControl, pointer to application data
//         _ Status, Service status
//
//     - OUTPUT :
//
// RETURNS :
//     TRUE, On success
//     FALSE, On Error 
//
//   NOTES :
//
BOOL  AppLog_WriteStatus (FILE * pLogFile, WORD Status)
{
  TCHAR _function_name[] = _T("AppLog_WriteStatus");
  TCHAR               _nls_message [NLS_MSG_STRING_MAX_LENGTH];
  DWORD               _string_id;
  WORD                _rc;
  BOOL                _rc_bool;
  TYPE_LOG_MESSAGE    _log_message;

  switch ( Status )
  {
    case LOG_STATUS_STARTED:
    {
      // Started string
      _string_id = NLS_ID_APPLICATION_LOG(1);
    }
    break;

    case LOG_STATUS_STOPPED:
    {
      // Stopped string
      _string_id = NLS_ID_APPLICATION_LOG(3);
    }
    break;

    default:
    {
      // Log error
      ServiceLogUnexpected (_function_name, _T("Status"), Status);

      return FALSE;
    }
    break;
  }

  // Get string
  _rc = NLS_GetString (_string_id, _nls_message, NULL, NULL, NULL, NULL, NULL);

  if ( _rc != NLS_STATUS_OK )
  {
    // Error Log
    ServiceLogError (_function_name, _T("NLS_GetString"), _rc);
  } // if

  // Set control block
  _log_message.control_block  = sizeof (TYPE_LOG_MESSAGE);
  _tcscpy (_log_message.message, _nls_message);
  _log_message.message_length = _tcslen (_log_message.message);
  _log_message.control_block  = sizeof (TYPE_LOG_MESSAGE) - sizeof (_log_message.message) +
                                  sizeof (TCHAR) * (1 + _log_message.message_length);

  // Insert message
  _rc_bool = LogFileWrite (pLogFile, &_log_message);
  if ( ! _rc_bool )
  {
    // Error inserting message. Error log has been inserted inspecific function
    return FALSE;
  } // if

  return TRUE;

} // AppLog_WriteStatus

//-----------------------------------------------------------------------------
// PURPOSE : Controls log file date. If log file is obsolete,
//          a new one for the current day is created
//
//  PARAMS :
//     - INPUT :
//
//     - OUTPUT :
//         - pApplogControl, pointer to application data
//
// RETURNS :
//     TRUE, On success
//     FALSE, On Error 
//
//   NOTES :
//
BOOL AppLog_ControlLogFile (APPLOG_CONTROL * pApplogControl)
{
  TCHAR _function_name [] = _T("AppLog_ControlLogFile");
  TCHAR _filename [COMMON_MAX_PATH_SIZE + 1];
  BOOL  _rc_bool;

  _rc_bool = AppLog_IsCurrentDateFile (pApplogControl);
  if ( ! _rc_bool )
  {
    // Obsolete file, close it and create a new one
    LogFileClose (pApplogControl->p_log_file);

    // Build file name
    AppLog_FormatFileName (pApplogControl, _filename);

    // Open new file
    pApplogControl->p_log_file = LogFileOpen (pApplogControl->working_directory, _filename);

    if ( pApplogControl->p_log_file == NULL )
    {
      // Error opening log file
      ServiceLogError (_function_name, _T("LogFileOpen"), NULL);

      return FALSE;
    } // if

    // Write the filename into the file
    AppLog_WriteFilename (pApplogControl->p_log_file, _filename);
  } // if
  
  return TRUE;

} // AppLog_ControlLogFile


//-----------------------------------------------------------------------------
// PURPOSE : Insert log file name
//
//  PARAMS :
//    - INPUT :
//      - pFullFileName:  Full path path + file name
//
//     - OUTPUT :
//
// RETURNS :
//     TRUE, On success
//     FALSE, On Error 
//
//   NOTES :
//
static VOID AppLog_WriteFilename (FILE *pLogFile, TCHAR * pFullFileName)
{
  TCHAR _function_name[] = _T("AppLog_WriteFilename");
  TCHAR               _nls_message [NLS_MSG_STRING_MAX_LENGTH + 1];
  TCHAR               _nls_param1 [NLS_MSG_PARAM_MAX_LENGTH + 1];
  TCHAR               _nls_param2 [NLS_MSG_PARAM_MAX_LENGTH + 1];
  TCHAR               _nls_param3 [NLS_MSG_PARAM_MAX_LENGTH + 1];
  TCHAR               _nls_param4 [NLS_MSG_PARAM_MAX_LENGTH + 1];
  TCHAR               _nls_param5 [NLS_MSG_PARAM_MAX_LENGTH + 1];
  WORD                _rc;
  TYPE_LOG_MESSAGE    _log_message;

  memset (&_log_message, 0, sizeof (TYPE_LOG_MESSAGE));
  _log_message.control_block = sizeof (TYPE_LOG_MESSAGE);
  
  // Split log file name
  _rc = NLS_SplitString (pFullFileName, _nls_param1,
                                        _nls_param2, 
                                        _nls_param3, 
                                        _nls_param4, 
                                        _nls_param5);

  if ( _rc != NLS_STATUS_OK )
  {
    // Log error
    ServiceLogError (_function_name, _T("NLS_SplitString"), _rc);
  } // if

  // Get log file name string
  _rc = NLS_GetString (NLS_ID_APPLICATION_LOG(2), _nls_message, _nls_param1,
                                                                _nls_param2,
                                                                _nls_param3,
                                                                _nls_param4,
                                                                _nls_param5);
  if ( _rc != NLS_STATUS_OK )
  {
    // Log error
    ServiceLogError (_function_name, _T("NLS_GetString"), _rc);
  } // if

  // Write the message to the buffer
  _tcscpy (_log_message.message, _nls_message);

  _log_message.message_length = _tcslen (_log_message.message);
  _log_message.control_block  = sizeof (TYPE_LOG_MESSAGE) - sizeof (_log_message.message) +
                                sizeof (TCHAR) * (1 + _log_message.message_length);

  // Don't care about the return code
  LogFileWrite (pLogFile, &_log_message);

} // AppLog_WriteFilename



