//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : AppLogIpc.cpp
//   DESCRIPTION : Functions to handle the IPC Messages
//        AUTHOR : Andreu Juli�
// CREATION DATE : 06-NOV-2002
// 
// REVISION HISTORY
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 06-NOV-2002 AJQ    Initial draft.
// 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_MISC
#define INCLUDE_IPC_API
#define INCLUDE_NLS_API
#define INCLUDE_PRIVATE_LOG
#define INCLUDE_QUEUE_API
#define INCLUDE_SERVICE
#define INCLUDE_APPLICATION_LOG
#include "CommonDef.h"

#include "AppLogInternals.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATATYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------
static VOID BroadcastDbQueueMessage (TYPE_DB_QUEUE_MESSAGE * pDbQueueMessage);

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PURPOSE: TimerHandler
//
//  PARAMS:
//     - INPUT:  None
//     - OUTPUT: None
//
// RETURNS:
//     - Nothing
//
//   NOTES:

VOID TimerHandler (VOID)
{
  TCHAR                 _function_name [] = _T("TimerHandler");
//  TYPE_DB_QUEUE_MESSAGE _queue_message;
  BOOL                  _rc_bool;
//  DWORD                 _rc;
//  DWORD                 _idx_msg;
//  DWORD                 _tick0, _tick1;
  static DWORD          _max_ticks = 1000; // 1sec

  // Each interval the file date is checked to create a new file (Date changed)
  _rc_bool = AppLog_ControlLogFile (&GLB_AppLogControl);
  if ( _rc_bool == FALSE )
  {
    // Log err
    ServiceLogError (_function_name, _T("TimerHandler"), FALSE);

    return;
  }
  
  //////////////for ( _idx_msg = 0; _idx_msg < LOG_MAX_DB_MSG; _idx_msg++ )
  //////////////{
  //////////////  // Read the MSG_LOGGER Queue
  //////////////  memset (&_queue_message, 0, sizeof (TYPE_DB_QUEUE_MESSAGE));
  //////////////  _queue_message.control_block = sizeof (TYPE_DB_QUEUE_MESSAGE);
  //////////////  _stprintf (_queue_message.consumer_name, _T("APP_LOG"));
  //////////////  _queue_message.context = GLB_AppLogControl.sql_context;
  //////////////  _stprintf (_queue_message.queue_name,  _T("MSG_LOGGER_QUEUE"));
  //////////////  _stprintf (_queue_message.sub_queue_name, _T("LOGGER_DB"));

  //////////////  _tick0 = GetTickCount ();

  //////////////  CALL_ORACLE (GLB_AppLogControl.sql_context, 
  //////////////              _rc, 
  //////////////              Common_ReadDbQueue (&_queue_message));

  //////////////  _tick1 = GetTickCount ();
  //////////////  
  //////////////  if ( Common_DiffTickCount (_tick0, _tick1) > _max_ticks )
  //////////////  {
  //////////////    _max_ticks = Common_DiffTickCount (_tick0, _tick1);
  //////////////    ServiceLogUnexpected (_T(__FUNCTION__), _T("*** TOO MUCH *** Ellapsed Time (msec)"), _max_ticks);
  //////////////  }

  //////////////  switch ( _rc )
  //////////////  {
  //////////////    case COMMON_OK:
  //////////////    {
  //////////////      BroadcastDbQueueMessage (&_queue_message);
  //////////////    }
  //////////////    break;

  //////////////    case COMMON_ERROR_NOT_FOUND:
  //////////////    {
  //////////////      // There was no message
  //////////////      return;
  //////////////    }
  //////////////    break;

  //////////////    default:
  //////////////    {
  //////////////      ServiceLogError (_function_name, _T("Common_ReadDbQueue"), _rc);

  //////////////      return;
  //////////////    }
  //////////////    break;
  //////////////  } // switch
  //////////////}

} // TimerHandler


//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// PURPOSE : Sends the given message to all the AppLog's
// 
//  PARAMS:
//      - INPUT:
//          - pDbQueueMessage: The message to send to all the AppLog's
//
//      - OUTPUT: None
// 
// RETURNS: Nothing
// 
// NOTES:
//
static VOID BroadcastDbQueueMessage (TYPE_DB_QUEUE_MESSAGE * pDbQueueMessage)
{
  TCHAR               _function_name [] = _T("Common_LoggerMsg");
  static BOOL         _first_time = TRUE; 
  static DWORD        _ipc_target_node_id;
  WORD                _rc;
  API_INPUT_PARAMS    _api_input_params;
  IPC_IUD_SEND        _ipc_iud_send;
  API_OUTPUT_PARAMS   _api_output_params;
  TYPE_LOG_MESSAGE    * p_log_message;
  TCHAR               _log_text [LOG_MESSAGE_BUFFER_SIZE + 1];  // Add terminating NULL char

  // Get computer name
  if ( _first_time )
  {
    if ( Common_IpcGetNodeInstanceId (IPC_NODE_NAME_APP_LOG, &_ipc_target_node_id, FALSE) == FALSE )
    {
      ServiceLogError (_function_name, _T("Common_IpcGetNodeInstanceId"), FALSE);

      return;
    }
    _first_time = FALSE;
  }

  // Input Params
  _api_input_params.control_block = sizeof (API_INPUT_PARAMS);
  _api_input_params.event_object  = NULL;
  _api_input_params.function_code = IPC_CODE_SEND;
  _api_input_params.mode          = API_MODE_NO_WAIT;

  _ipc_iud_send.control_block       = sizeof (IPC_IUD_SEND);
  _ipc_iud_send.msg_cmd             = MSG_CMD_LOG;
  _ipc_iud_send.msg_id              = 0;
  _ipc_iud_send.source_node_inst_id = IPC_NODE_UNKNOWN;
  _ipc_iud_send.target_node_inst_id = _ipc_target_node_id; 
  _ipc_iud_send.user_buffer_length  = sizeof (TYPE_LOG_MESSAGE);

  _api_output_params.control_block  = sizeof (API_OUTPUT_PARAMS);

  p_log_message = (TYPE_LOG_MESSAGE *) &_ipc_iud_send.user_buffer; 

  // Specific data
  p_log_message->control_block = sizeof (TYPE_LOG_MESSAGE);

  _rc = NLS_GetString (pDbQueueMessage->fld_nls_id, 
                       _log_text, 
                       pDbQueueMessage->fld_nls_param1, 
                       pDbQueueMessage->fld_nls_param2, 
                       pDbQueueMessage->fld_nls_param3, 
                       pDbQueueMessage->fld_nls_param4, 
                       pDbQueueMessage->fld_nls_param5);

  if (   _rc != NLS_STATUS_OK 
      && _rc != NLS_STATUS_DLL_NOT_LOADED )
  {
    _stprintf (_log_text, _T("%s"), _T("Error in Nls API."));
  }

  LogMsgText (pDbQueueMessage->fld_text0,
              pDbQueueMessage->fld_text1, 
              pDbQueueMessage->fld_text2, 
              (WORD) pDbQueueMessage->fld_nls_id,
              _log_text,
              NULL, 
              LOG_MESSAGE_BUFFER_SIZE, 
              p_log_message->message);

  // Compute the message length 
  p_log_message->message_length = _tcslen (p_log_message->message);

  // Compute the User Buffer length
  _ipc_iud_send.user_buffer_length = sizeof (TYPE_LOG_MESSAGE) 
                                     - sizeof (p_log_message->message) 
                                     + sizeof (TCHAR) * (1 + p_log_message->message_length);
  
  // API CALL
  // don't care about the return code
  _rc = Ipc_API (&_api_input_params,    // Input Params
                 &_ipc_iud_send,        // Input User Data
                 &_api_output_params,   // Output Params
                 NULL);                 // Output User Data

  
  if ( _rc != API_STATUS_OK )
  {
    ServiceLogError (_function_name, _T("Ipc_API"), _rc);

    return;
  }

  return;
}
