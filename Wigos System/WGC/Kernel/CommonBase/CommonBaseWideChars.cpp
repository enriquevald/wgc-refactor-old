//-----------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//-----------------------------------------------------------------------------
// 
//   MODULE NAME: CommonBaseWideChars.cpp
//   DESCRIPTION: Functions and Methods of the wide chars to handle the 
//                CommonMisc dll
//        AUTHOR: Alberto Cuesta
// CREATION DATE: 11-MAR-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 11-MAR-2002 ACC    Initial draft.
// 04-NOV-2002 AJQ    Moved from CommonMisc to CommonBase
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
#include "CommonDef.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE: Call MultiByteToWideChar for convert byte to WideChar (TCHAR *).
// 
//  PARAMS:
//      - INPUT:
//          - CodePage (see WideCharToMultiByte function)
//          - Flags (see WideCharToMultiByte function)
//          - pMultiByteStr: Multibyte string
//          - MultiByteLength: Multibyte string buffer size
//          - pWideCharStr: Wide char string
//          - WideCharSize: Wide char string length (in wide chars)
//
//      - OUTPUT: None
// 
// RETURNS: number of wide characters 
// 
//   NOTES:

COMMONBASE_API WORD WINAPI Common_MultiByteToWideChar (UINT     CodePage, 
                                                       DWORD    Flags,
                                                       LPCSTR   pMultiByteStr,
                                                       int      MultiByteLength,
#if !defined (_WINDOWS) || defined (UNICODE)
                                                       LPWSTR   pWideCharStr,
#else
                                                       TCHAR    * pWideCharStr,
#endif
                                                       int      WideCharSize)
{

#if !defined (_WINDOWS) || defined (UNICODE)
  //TYPE_LOGGER_PARAM   _log_param_1;
  WORD      _conv_rc;

  memset (pWideCharStr, 0, WideCharSize * sizeof (TCHAR));

  MB_PRECOMPOSED
  _conv_rc = MultiByteToWideChar (CodePage, 
                                  Flags, 
                                  pMultiByteStr, 
                                  MultiByteLength,
                                  pWideCharStr, 
                                  WideCharSize);
  if (_conv_rc == 0)  // Conversion error 
  {
    // Logger message 
    //_stprintf (_log_param_1, _T("%lu"), GetLastError ());
    //Common_Logger (NLS_ID_LOGGER(2), _log_param_1, _T("MultiByteToWideChar"), _T(""));
  }

  return _conv_rc;

#else // #if !defined (_WINDOWS) || defined (UNICODE)

  WORD  _num_bytes;
  WORD  _wide_char_buffer_size;

  _wide_char_buffer_size = WideCharSize * sizeof (TCHAR);
  memset (pWideCharStr, 0, _wide_char_buffer_size);

  // No conversion necessary, just copy
  if ( _wide_char_buffer_size < MultiByteLength )
  {
    _num_bytes = _wide_char_buffer_size;
  }
  else
  {
    _num_bytes = MultiByteLength;
  }

  memcpy (pWideCharStr, pMultiByteStr, _num_bytes);

  return _num_bytes;

#endif // #if !defined (_WINDOWS) || defined (UNICODE)

} // Common_MultiByteToWideChar 

//------------------------------------------------------------------------------
// PURPOSE: Converst from widechar to multibyte.
// 
//  PARAMS:
//      - INPUT:
//          - CodePage (see WideCharToMultiByte function)
//          - Flags (see WideCharToMultiByte function)
//          - pWideCharStr: Wide char string
//          - WideCharLength: Wide char string length (in wide chars)
//          - pMultiByteStr: Multibyte string
//          - MultiByteSize: Multibyte string buffer size
//
//      - OUTPUT: None
// 
// RETURNS: Number of bytes written to the buffer pointed to by pMultiByteStr. 
//          The number includes the byte for the null terminator
// 
//   NOTES:

COMMONBASE_API WORD WINAPI Common_WideCharToMultiByte (UINT     CodePage, 
                                                       DWORD    Flags,
#if !defined (_WINDOWS) || defined (UNICODE)
                                                       LPCWSTR  pWideCharStr,  
#else
                                                       TCHAR    * pWideCharStr,
#endif
                                                       int      WideCharLength,
                                                       LPSTR    pMultiByteStr, 
                                                       int      MultiByteSize)
{

#if !defined (_WINDOWS) || defined (UNICODE)
  //TYPE_LOGGER_PARAM   _log_param_1;
  WORD      _conv_rc;

  memset (pMultiByteStr, 0, MultiByteSize);

  _conv_rc = WideCharToMultiByte (CodePage, Flags, 
                                  pWideCharStr, 
                                  WideCharLength,
                                  pMultiByteStr, 
                                  MultiByteSize, 
                                  NULL, NULL);             
  if ( _conv_rc == 0 )      // Conversion error 
  {
    // Logger message 
    //_stprintf (_log_param_1, _T("%lu"), GetLastError ());
    //Common_Logger (NLS_ID_LOGGER(2), _log_param_1, _T("WideCharToMultiByte"), _T(""));
  }

  return _conv_rc;

#else // #if !defined (_WINDOWS) || defined (UNICODE)

  WORD _num_bytes;
  WORD _wide_char_buffer_size;

  _wide_char_buffer_size = WideCharLength * sizeof (TCHAR);
  memset (pMultiByteStr, 0, MultiByteSize);

  if ( _wide_char_buffer_size < MultiByteSize )
  {
    _num_bytes = _wide_char_buffer_size;
  }
  else
  {
    _num_bytes = MultiByteSize;
  }

  memcpy (pMultiByteStr, pWideCharStr, _num_bytes);

  return _num_bytes;

#endif // #if !defined (_WINDOWS) || defined (UNICODE)

} // Common_WideCharToMultiByte 
