//-----------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
//-----------------------------------------------------------------------------
// 
//   MODULE NAME : CommonBaseSwpackage.cpp
//
//   DESCRIPTION : Functions and Methods to manage Sw Package files
//
// REVISION HISTORY
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 15-SEP-2008 TJG    First release.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES 
//------------------------------------------------------------------------------

#define INCLUDE_COMMON_BASE
#define INCLUDE_NLS_API
#define INCLUDE_PRIVATE_LOG
#include "CommonDef.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS
//------------------------------------------------------------------------------

#define MODULE_NAME               _T("COMMONBASE")

#define CRC_BUFFER_SIZE           65536

#define MAX_PACKED_FILENAME_LEN   MAX_PATH
#define MAX_VERSION_LEN           7             // "XX-XXX\0"                           
#define MAX_PACKAGE_FILES         200           // Max number of files to be packed     
// Return Code Constants : 
#define INITIAL_CRC               0xFFFFFFFFL   // Initial value for the CRC            
#define PACKER_VERSION            1L            // Packer version checked in unpacking  

#define UNPACK_OK             0
#define UNPACK_ERROR_1        1
#define UNPACK_ERROR_2        2
#define UNPACK_ERROR_3        3 
#define UNPACK_ERROR_4        4
#define UNPACK_ERROR_5        5
#define UNPACK_ERROR_6        6

static DWORD const CRC_32_TAB[] =
{
  0x00000000L, 0x77073096L, 0xee0e612cL, 0x990951baL, 0x076dc419L,
  0x706af48fL, 0xe963a535L, 0x9e6495a3L, 0x0edb8832L, 0x79dcb8a4L,
  0xe0d5e91eL, 0x97d2d988L, 0x09b64c2bL, 0x7eb17cbdL, 0xe7b82d07L,
  0x90bf1d91L, 0x1db71064L, 0x6ab020f2L, 0xf3b97148L, 0x84be41deL,
  0x1adad47dL, 0x6ddde4ebL, 0xf4d4b551L, 0x83d385c7L, 0x136c9856L,
  0x646ba8c0L, 0xfd62f97aL, 0x8a65c9ecL, 0x14015c4fL, 0x63066cd9L,
  0xfa0f3d63L, 0x8d080df5L, 0x3b6e20c8L, 0x4c69105eL, 0xd56041e4L,
  0xa2677172L, 0x3c03e4d1L, 0x4b04d447L, 0xd20d85fdL, 0xa50ab56bL,
  0x35b5a8faL, 0x42b2986cL, 0xdbbbc9d6L, 0xacbcf940L, 0x32d86ce3L,
  0x45df5c75L, 0xdcd60dcfL, 0xabd13d59L, 0x26d930acL, 0x51de003aL,
  0xc8d75180L, 0xbfd06116L, 0x21b4f4b5L, 0x56b3c423L, 0xcfba9599L,
  0xb8bda50fL, 0x2802b89eL, 0x5f058808L, 0xc60cd9b2L, 0xb10be924L,
  0x2f6f7c87L, 0x58684c11L, 0xc1611dabL, 0xb6662d3dL, 0x76dc4190L,
  0x01db7106L, 0x98d220bcL, 0xefd5102aL, 0x71b18589L, 0x06b6b51fL,
  0x9fbfe4a5L, 0xe8b8d433L, 0x7807c9a2L, 0x0f00f934L, 0x9609a88eL,
  0xe10e9818L, 0x7f6a0dbbL, 0x086d3d2dL, 0x91646c97L, 0xe6635c01L,
  0x6b6b51f4L, 0x1c6c6162L, 0x856530d8L, 0xf262004eL, 0x6c0695edL,
  0x1b01a57bL, 0x8208f4c1L, 0xf50fc457L, 0x65b0d9c6L, 0x12b7e950L,
  0x8bbeb8eaL, 0xfcb9887cL, 0x62dd1ddfL, 0x15da2d49L, 0x8cd37cf3L,
  0xfbd44c65L, 0x4db26158L, 0x3ab551ceL, 0xa3bc0074L, 0xd4bb30e2L,
  0x4adfa541L, 0x3dd895d7L, 0xa4d1c46dL, 0xd3d6f4fbL, 0x4369e96aL,
  0x346ed9fcL, 0xad678846L, 0xda60b8d0L, 0x44042d73L, 0x33031de5L,
  0xaa0a4c5fL, 0xdd0d7cc9L, 0x5005713cL, 0x270241aaL, 0xbe0b1010L,
  0xc90c2086L, 0x5768b525L, 0x206f85b3L, 0xb966d409L, 0xce61e49fL,
  0x5edef90eL, 0x29d9c998L, 0xb0d09822L, 0xc7d7a8b4L, 0x59b33d17L,
  0x2eb40d81L, 0xb7bd5c3bL, 0xc0ba6cadL, 0xedb88320L, 0x9abfb3b6L,
  0x03b6e20cL, 0x74b1d29aL, 0xead54739L, 0x9dd277afL, 0x04db2615L,
  0x73dc1683L, 0xe3630b12L, 0x94643b84L, 0x0d6d6a3eL, 0x7a6a5aa8L,
  0xe40ecf0bL, 0x9309ff9dL, 0x0a00ae27L, 0x7d079eb1L, 0xf00f9344L,
  0x8708a3d2L, 0x1e01f268L, 0x6906c2feL, 0xf762575dL, 0x806567cbL,
  0x196c3671L, 0x6e6b06e7L, 0xfed41b76L, 0x89d32be0L, 0x10da7a5aL,
  0x67dd4accL, 0xf9b9df6fL, 0x8ebeeff9L, 0x17b7be43L, 0x60b08ed5L,
  0xd6d6a3e8L, 0xa1d1937eL, 0x38d8c2c4L, 0x4fdff252L, 0xd1bb67f1L,
  0xa6bc5767L, 0x3fb506ddL, 0x48b2364bL, 0xd80d2bdaL, 0xaf0a1b4cL,
  0x36034af6L, 0x41047a60L, 0xdf60efc3L, 0xa867df55L, 0x316e8eefL,
  0x4669be79L, 0xcb61b38cL, 0xbc66831aL, 0x256fd2a0L, 0x5268e236L,
  0xcc0c7795L, 0xbb0b4703L, 0x220216b9L, 0x5505262fL, 0xc5ba3bbeL,
  0xb2bd0b28L, 0x2bb45a92L, 0x5cb36a04L, 0xc2d7ffa7L, 0xb5d0cf31L,
  0x2cd99e8bL, 0x5bdeae1dL, 0x9b64c2b0L, 0xec63f226L, 0x756aa39cL,
  0x026d930aL, 0x9c0906a9L, 0xeb0e363fL, 0x72076785L, 0x05005713L,
  0x95bf4a82L, 0xe2b87a14L, 0x7bb12baeL, 0x0cb61b38L, 0x92d28e9bL,
  0xe5d5be0dL, 0x7cdcefb7L, 0x0bdbdf21L, 0x86d3d2d4L, 0xf1d4e242L,
  0x68ddb3f8L, 0x1fda836eL, 0x81be16cdL, 0xf6b9265bL, 0x6fb077e1L,
  0x18b74777L, 0x88085ae6L, 0xff0f6a70L, 0x66063bcaL, 0x11010b5cL,
  0x8f659effL, 0xf862ae69L, 0x616bffd3L, 0x166ccf45L, 0xa00ae278L,
  0xd70dd2eeL, 0x4e048354L, 0x3903b3c2L, 0xa7672661L, 0xd06016f7L,
  0x4969474dL, 0x3e6e77dbL, 0xaed16a4aL, 0xd9d65adcL, 0x40df0b66L,
  0x37d83bf0L, 0xa9bcae53L, 0xdebb9ec5L, 0x47b2cf7fL, 0x30b5ffe9L,
  0xbdbdf21cL, 0xcabac28aL, 0x53b39330L, 0x24b4a3a6L, 0xbad03605L,
  0xcdd70693L, 0x54de5729L, 0x23d967bfL, 0xb3667a2eL, 0xc4614ab8L,
  0x5d681b02L, 0x2a6f2b94L, 0xb40bbe37L, 0xc30c8ea1L, 0x5a05df1bL,
  0x2d02ef8dL

}; //end CRC_32_TAB 

//------------------------------------------------------------------------------
// PRIVATE DATATYPES 
//------------------------------------------------------------------------------

typedef struct
{
  DWORD       packer_version;         // Packer Version

  DWORD       filesize0;              // Initial packet size
  DWORD       filesize1;              // Compressed packet file
  DWORD       filesize2;              // Final packet file
  DWORD       compressed;             // Flag indicating whether or not the file has been compressed

  DWORD       terminal_type;          // 0 : LKAS, 1 : LKT
  DWORD       client_id;              // Client Id
  DWORD       build_id;               // Build  Id
  DWORD       from_build_id;          // Upgrade From (same as build for FULL Version)

  DWORD       reserved [3];           // Reserved

  DWORD       info_size;              // Info size
  DWORD       info_start;             // Info start

  TCHAR       package_name [64];      // Packet Name

} TYPE_PACKAGE_FOOTER;

typedef TYPE_PACKAGE_FOOTER     TYPE_PACKAGE_HEADER;

typedef struct
{
  TYPE_PACKAGE_HEADER   header;
  DWORD                 header_crc;
  DWORD                 reserved;

} TYPE_PACKAGE_HEADER_CRC;

typedef struct
{
  TYPE_PACKAGE_FOOTER   footer;
  DWORD                 footer_crc;
  DWORD                 file_crc;

} TYPE_PACKAGE_FOOTER_CRC;

typedef struct
{
  TCHAR                   name [256];       // 256 
  TCHAR                   version [32];     // DWORD Aligned future use (now is left empty)
  DWORD                   start;
  DWORD                   size;
  DWORD                   crc;
  FILETIME                last_write_time;  
  DWORD                   flags;            // Reserved must be 0

} TYPE_PACKET_FILE_INFO;

typedef union
{
  BYTE                    record [512];
  TYPE_PACKET_FILE_INFO   file;

} TYPE_PACKAGE_MODULE;

typedef struct
{
  DWORD                   num_records;
  TYPE_PACKAGE_MODULE     record [MAX_PACKAGE_FILES];

} TYPE_SW_PACKAGE_FILE_LIST;

typedef struct
{
  DWORD                   control_block;
  DWORD                   client_id;
  DWORD                   build_id;
  DWORD                   build_type;             // 0: Full; 1: Upgrade
  DWORD                   terminal_type;

  DWORD                   packer_version;         // Packer Version
  DWORD                   compressed;             // Flag indicating whether or not the file has been compressed

  //TCHAR                   package_name [64];       // Packet Name

  DWORD                   num_modules;

  TCHAR                   working_folder [256];

} TYPE_SW_PACKAGE_FILE;

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------
TYPE_SW_PACKAGE_FILE_LIST   GLB_SwPackage;        // Package's file list (extracted from a compressed file)

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

static DWORD ComputeInitCRC (DWORD InitialCrc, VOID * pBuffer, DWORD BufferSize);
static BOOL CopyData (HANDLE   Source, HANDLE Target, DWORD Size, DWORD * pDataCRC, DWORD BufferSize, VOID * pBuffer);
static BOOL ExtractRecord (HANDLE Source, TCHAR * pPath, TYPE_PACKET_FILE_INFO * pFileInfo, DWORD BufferSize, VOID * pBuffer);
static BOOL VerifyCRC (TCHAR * pFileName, TYPE_PACKAGE_FOOTER * pFooter, TYPE_SW_PACKAGE_FILE_LIST * pFileList);

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : CRC-32 calculation, using an initial value
//
//  PARAMS :
//      - INPUT :
//          - InitialCrc : Initial CRC value.
//          - BufferPtr : Data Buffer
//          - BufferSize : Buffer size
//
//      - OUTPUT :
//
// RETURNS :
//      - calculated CRC-32
//
//   NOTES : 

static DWORD  ComputeInitCRC (DWORD InitialCrc, VOID * pBuffer, DWORD BufferSize)
{
  DWORD   _aux_crc;
  BYTE    * p_byte = (BYTE *) pBuffer;
  DWORD   _aux_buff_size;

  p_byte = (BYTE *) pBuffer;
  _aux_crc = InitialCrc;
  _aux_buff_size = BufferSize;

  if ( ! _aux_buff_size )
  {
    // Buffer empty => CRC not modified
    return InitialCrc;
  }

  do
  {
    _aux_crc = CRC_32_TAB[((short)_aux_crc ^ (*p_byte++)) & 0xFF] ^ (_aux_crc >> 8);
    _aux_crc = _aux_crc ^ 0xFFFFFFFFL;
  } while ( --_aux_buff_size );
   
  return _aux_crc;

  return (_aux_crc ^ 0xFFFFFFFFL);

} // ComputeInitCRC

//------------------------------------------------------------------------------
// PURPOSE : Copies data from the Source file to Target. The CRC is computed.
//
//  PARAMS :
//      - INPUT :
//          - Source:    Handle to source file
//          - Target:    Handle to target file
//          - Size:      Number of bytes to be copied
//          - pDataCRC:  Actual CRC
//          - BufferSize:Buffer size
//          - pBuffer:   Working buffer
//
//      - OUTPUT :
//          - pDataCRC:  Final CRC
//
// RETURNS :
//      - TRUE when succeded
//      - FALSE otherwise
//
// NOTES : If Target is equal to INVALID_HANDLE_VALUE then the data is read and its CRC is computed 

static BOOL   CopyData (HANDLE   Source, 
                        HANDLE   Target, 
                        DWORD    Size, 
                        DWORD    * pDataCRC,
                        DWORD    BufferSize,
                        VOID     * pBuffer)
{
  TYPE_LOGGER_PARAM   _log_param1;
  DWORD               _left;
  DWORD               _data_crc;
  BOOL                _success;
  DWORD               _read;
  DWORD               _to_read;
  DWORD               _written;

  _data_crc = * pDataCRC;

  _left = Size;
  while ( _left > 0 )
  {
    _to_read = BufferSize;
    if ( _to_read > _left )
    {
      _to_read = _left;
    }

    // Read data
    _success = ReadFile (Source, pBuffer, _to_read, &_read, NULL);
    _success = _success && (_to_read == _read);
    _left    = _left - _read;

    if ( _success )
    {
      // Compute the CRC
      _data_crc = ComputeInitCRC (_data_crc, pBuffer, _read);

      // Write data
      if ( Target != INVALID_HANDLE_VALUE )
      {
        _success = WriteFile (Target, pBuffer, _read, &_written, NULL);
        _success = _success && (_read == _written);
      }
    }

    if ( ! _success )
    {
      _stprintf (_log_param1, _T("%lu"), GetLastError ()); 
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("ReadFile"));          

      return FALSE;
    }
  }

  * pDataCRC = _data_crc;

  return TRUE;
} // CopyData

//------------------------------------------------------------------------------
// PURPOSE : Extracts the selected file to disk
//
//  PARAMS :
//      - INPUT :
//          - Source:    Handle to source file
//          - pPath:     Target directory
//          - pFileInfo: Pointer to the file info record
//          - BufferSize:Buffer size
//          - pBuffer:   Working buffer
//
//      - OUTPUT :
//
// RETURNS :
//      - TRUE when succeded
//      - FALSE otherwise
//
// NOTES : 
//  AJQ 05-JAN-2006, The Creation,Write and LastAccess file times are set to
//                   the current time (extraction time).

static BOOL   ExtractRecord (HANDLE                 Source, 
                             TCHAR                  * pPath,
                             TYPE_PACKET_FILE_INFO  * pFileInfo, 
                             DWORD                  BufferSize, 
                             VOID                   * pBuffer)
{
  TYPE_LOGGER_PARAM _log_param1;
  TYPE_LOGGER_PARAM _log_param2;
  HANDLE            _target;
  DWORD             _crc;
  BOOL              _succeeded;
  TCHAR             _full_name [MAX_PATH];
  SYSTEMTIME        _st_now;
  FILETIME          _ft_now;

  // Filename
  _makepath (_full_name, NULL, pPath, pFileInfo->name, NULL);

  // Create the file
  _target = CreateFile (_full_name,            // File name               
                        GENERIC_WRITE,         // Desired Access          
                        0,                     // Share Mode = Not shared  
                        NULL,                  // Security attributes     
                        CREATE_ALWAYS,         // Creation Disposition. Only open if file exists 
                        FILE_ATTRIBUTE_NORMAL, // Flags And Attributes    
                        0);

  if ( _target == INVALID_HANDLE_VALUE )
  {
    _stprintf (_log_param1, _T("%lu"), GetLastError ()); 
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("CreateFile"));    

    return FALSE;
  }

  // Default return value
  _succeeded = FALSE;

  __try
  {
    // Locate the file data
    if ( SetFilePointer (Source, pFileInfo->start, NULL, FILE_BEGIN) != pFileInfo->start )
    {
      _stprintf (_log_param1, _T("%lu"), GetLastError ()); 
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("SetFilePointer"));    

      return FALSE;
    }

    _crc = INITIAL_CRC;
    // Copy data
    if ( ! CopyData (Source, _target, pFileInfo->size, &_crc, BufferSize, pBuffer) )
    {
      _stprintf (_log_param1, _T("%lu"), FALSE); 
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("CopyData"));          

      return FALSE;
    } // if

    // Verify the file CRC
    if ( _crc != pFileInfo->crc )
    {      
      _stprintf (_log_param1, _T("0x%08X"), _crc);
      _stprintf (_log_param2, _T("0x%08X"), pFileInfo->crc);
      PrivateLog (3, MODULE_NAME, __FUNCTION__, _T("ComputeCRC"), _log_param1, _log_param2);

      return FALSE;
    }

    GetTime (GET_TIME_UNIVERSAL_TIME, &_st_now);
    if ( ! SystemTimeToFileTime (&_st_now, &_ft_now) )
    {
      _stprintf (_log_param1, _T("%lu"), GetLastError ()); 
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("SystemTimeToFileTime"));

      return FALSE;
    }

    // Change the file times
    if ( ! SetFileTime (_target,    // File Handle
                        &_ft_now,   // Creation Time
                        &_ft_now,   // Last Access Time
                        &_ft_now) ) // Last Write Time
    {
      _stprintf (_log_param1, _T("%lu"), GetLastError ()); 
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("SetFileTime"));

      return FALSE;
    }

    _succeeded = TRUE;
  }
  __finally
  {
    // Close file
    if ( ! CloseHandle (_target) )
    {
      _stprintf (_log_param1, _T("%lu"), GetLastError ()); 
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("CloseHandle"));

      _succeeded = FALSE;
    } // if

    if ( ! _succeeded )
    {
      // Try to delete the file
      DeleteFile (_full_name);
    }
  }

  return _succeeded;
} // ExtractRecord

//------------------------------------------------------------------------------
// PURPOSE : CRC-32 verification funtion.
//
//  PARAMS :
//      - INPUT :
//          - pFileName : File name to open and verify.
//          - pFooter : Footer pointer.
//          - pFileList : Pointer to the file list structure to be verified.
//
//      - OUTPUT :
//
// RETURNS :
//      - calculated CRC-32
//
//   NOTES : 

static BOOL     VerifyCRC (TCHAR                      * pFileName, 
                           TYPE_PACKAGE_FOOTER        * pFooter,
                           TYPE_SW_PACKAGE_FILE_LIST  * pFileList)
{
  TCHAR                   _function_name[] = _T("VerifyCRC");
  TYPE_LOGGER_PARAM       _log_param1;
  TYPE_LOGGER_PARAM       _log_param2;
  HANDLE                  _file;
  DWORD                   _filesize;
  DWORD                   _read;
  BOOL                    _bool_rc;
  BYTE                    _buffer [CRC_BUFFER_SIZE];
  DWORD                   _file_crc;
  DWORD                   _file_crc1;
  DWORD                   _footer_crc;
  DWORD                   _read_crc;
  DWORD                   _prev_read;
  DWORD                   * p_crc;
  TYPE_PACKAGE_FOOTER_CRC  * p_footer_crc;

  // Variables initialization
  _read_crc = 0;
  _footer_crc = 0;

  // Open  file 
  _file = CreateFile (pFileName,                // File name               
                      GENERIC_READ,             // Desired Access          
                      0,                        // Share Mode = Not shared  
                      NULL,                     // Security attributes     
                      OPEN_EXISTING,            // Creation Disposition. Only open if file exists 
                      FILE_ATTRIBUTE_NORMAL,    // Flags And Attributes    
                      0); 

  if ( _file == INVALID_HANDLE_VALUE )
  {
    _stprintf (_log_param1, _T("%lu"), GetLastError ()); 
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("CreateFile"));

    return FALSE;
  } // if

  // Check for invalid file size
  _filesize = GetFileSize (_file, NULL);
  if ( _filesize == INVALID_FILE_SIZE )
  {
    _stprintf (_log_param1, _T("%lu"), GetLastError ());
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("GetFileSize"));

    CloseHandle (_file);

    return FALSE;
  } // if

  // Check for empty file
  if ( _filesize == 0 )
  {
    //Common_Logger (4, _T("GetFileSize"), _T("Empty File"), _T(""));

    CloseHandle (_file);

    return FALSE;
  } // if

  // Compute the File CRC
  _file_crc  = 0xFFFFFFFFL;
  _file_crc1 = _file_crc;
  for (;;)
  {
    _bool_rc  = ReadFile (_file, 
                          _buffer, 
                          sizeof (_buffer), 
                          &_read, 
                          0);

    if ( ! _bool_rc )
    {
      // Error
      _stprintf (_log_param1, _T("%lu"), GetLastError ()); 
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("ReadFile"));      

      CloseHandle (_file);

      return FALSE;
    }
    else
    {
      if ( _read == 0 )
      {
        // End of file
        _file_crc = _file_crc1;

        break;
      }
      else
      {
        if ( _read < sizeof (DWORD) )
        {
          CloseHandle (_file);
          
          return FALSE;
        }

        p_crc = (DWORD *) &_buffer[_read];
        p_footer_crc = (TYPE_PACKAGE_FOOTER_CRC *) &_buffer[_read];
        _read_crc  = p_crc[-1];
        _file_crc1 = ComputeInitCRC (_file_crc, _buffer, _read - sizeof (DWORD) );
        _file_crc  = ComputeInitCRC (_file_crc, _buffer, _read);

        _prev_read = _read_crc;

      } // if
    } // if
  } // if

  if ( _read_crc != _file_crc )
  {
    _stprintf (_log_param1, _T("0x%08X"), _read_crc);
    _stprintf (_log_param2, _T("0x%08X"), _file_crc);
    PrivateLog (3, MODULE_NAME, __FUNCTION__, _T("FooterCRC"), _log_param1, _log_param2);      

    CloseHandle (_file);

    return FALSE;
  } // if

  * pFooter = p_footer_crc[-1].footer;
  _footer_crc = ComputeInitCRC (0xFFFFFFFFL, pFooter, sizeof (TYPE_PACKAGE_FOOTER));
  if ( _footer_crc != p_footer_crc[-1].footer_crc )
  {
    _stprintf (_log_param1, _T("0x%08X"), p_footer_crc[-1].footer_crc);
    _stprintf (_log_param2, _T("0x%08X"), _footer_crc);
    PrivateLog (3, MODULE_NAME, __FUNCTION__, _T("FooterCRC"), _log_param1, _log_param2);

    CloseHandle (_file);

    return FALSE;
  }

  // Read Module List
  memset (pFileList, 0, sizeof (TYPE_SW_PACKAGE_FILE_LIST));
  if ( pFooter->info_size <= sizeof (TYPE_SW_PACKAGE_FILE_LIST) )
  {
    SetFilePointer (_file,                // File handle      
                    pFooter->info_start,  // Distance To Move 
                    NULL,                 // Allways null     
                    FILE_BEGIN);          // Starting point 

    ReadFile (_file, 
              pFileList,
              pFooter->info_size, 
              &_read, 
              0);
  }

  CloseHandle (_file);

  return TRUE;
} // VerifyCRC

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Delete files within a specified folder and optionally the folder itself
//
//  PARAMS :
//      - INPUT :
//          - pWorkingPathName : Path to clean of temporary files
//          - DeleteDirectory : remove directory or not
//
//      - OUTPUT :
//        
// RETURNS :
//      - TRUE : Path has been cleaned
//      - FALSE : Path could not be cleaned
//
//   NOTES : 
//      Any existing subdirectories inside are NOT removed

COMMONBASE_API  BOOL WINAPI Common_CleanPath (TCHAR * pWorkingPathName, BOOL DeleteDirectory)
{
  TCHAR             _search_mask [MAX_PATH];
  TCHAR             _complete_file_name [MAX_PATH];
  HANDLE            _search_handle;
  WIN32_FIND_DATA   _find_data;
  DWORD             _failing_reason;
  BOOL              _find_result;
  BOOL              _delete_result;

  // Safety check
  if (pWorkingPathName == NULL)
  {
    return FALSE;
  } // if

  // Build the file search mask
  _stprintf (_search_mask, _T("%s\\*"), pWorkingPathName);

  memset (&_find_data, 0, sizeof (_find_data));
  _search_handle = FindFirstFile (_search_mask, &_find_data);

  if ( _search_handle == INVALID_HANDLE_VALUE )
  {
    // Error may happen because:
    //  - Directory is empty
    //  - Directory does not exist or another error
    _failing_reason = GetLastError ();
    if ( _failing_reason != ERROR_FILE_NOT_FOUND )
    {
      // Error trying to find files
      _failing_reason = GetLastError();

      return FALSE;
    } // if
  } // if
  else
  {
    // Directory is not empty: try to delete existing files
    do
    {
      // Check to skip subdirectories
      if ( ! (_find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) )
      {
        // File names returned do not include path, it must be concatenated
        _stprintf (_complete_file_name, _T("%s\\%s"), pWorkingPathName, _find_data.cFileName);
        _delete_result = DeleteFile (_complete_file_name);
        if ( ! _delete_result )
        {
          _failing_reason = GetLastError();
        } // if
      } // if

      _find_result = FindNextFile (_search_handle, &_find_data);
    } while ( _find_result != 0 );
  } // else

  // Close the search handle
  FindClose (_search_handle);

  if ( DeleteDirectory )
  {
    // Now try to remove the working directory
    _delete_result = RemoveDirectory (pWorkingPathName);
    if ( ! _delete_result )
    {
      _failing_reason = GetLastError();

      return FALSE;
    } // if
  } // if

  return TRUE;
} // Common_CleanPath

//------------------------------------------------------------------------------
// PURPOSE : Execute system command.
//
//  PARAMS :
//      - INPUT :
//          - pCommandLine
//
//      - OUTPUT :
//          - pError
//
// RETURNS :
//      - TRUE  : successfull
//      - FALSE : other case.
//
//   NOTES : 

COMMONBASE_API  BOOL WINAPI Common_ExecuteSystemCommand (TCHAR     * pCommandLine, 
                                                         DWORD     * pError,
                                                         DWORD     TimeOut)
{
  STARTUPINFO           _startup_info;
  PROCESS_INFORMATION   _process_information;
  DWORD                 _timeout;
  DWORD                 _rc;

  // Initialize variables
  *pError = 0;
  memset (&_startup_info, 0, sizeof (_startup_info));
  _startup_info.cb = sizeof (_startup_info);
  memset (&_process_information, 0, sizeof (_process_information));
  _process_information.hProcess = INVALID_HANDLE_VALUE;
  _process_information.hThread  = INVALID_HANDLE_VALUE;

  __try
  {
    if ( ! CreateProcess (NULL,                      // AplicationName     
                          pCommandLine,              // CommandLine
                          NULL,                      // ProcessAttributes  
                          NULL,                      // ThreadAttributes   
                          TRUE,                      // InheritHandles     
                          CREATE_NO_WINDOW,          // CreationFlags 
                          NULL,                      // Environment        
                          NULL,                      // CurrentDirectory   
                          &_startup_info,            // StartupInfo        
                          &_process_information) )   // ProcessInformation 
    {
      return FALSE;
    }

    if ( ! TimeOut )
    {
      _timeout = INFINITE;
    }
    else
    {
      _timeout = TimeOut;
    }

    // Wait until child process exits.
    _rc = WaitForSingleObject (_process_information.hProcess, _timeout);
    if ( _rc != WAIT_OBJECT_0 )
    {
      if ( _rc == WAIT_TIMEOUT )
      {
        TerminateProcess (_process_information.hProcess, 0);
      }
      else
      {
        return FALSE;
      }
    }

    if ( ! GetExitCodeProcess (_process_information.hProcess, pError) )
    {
      return FALSE;
    }

    return TRUE;
  }

  __finally
  {
    // Close previous execution handles
    if ( _process_information.hProcess != INVALID_HANDLE_VALUE )
    {
      CloseHandle (_process_information.hProcess);
    }
    if ( _process_information.hThread != INVALID_HANDLE_VALUE )
    {
      CloseHandle (_process_information.hThread);
    }
  }

  return TRUE;
} // Common_ExecuteSystemCommand

//------------------------------------------------------------------------------
// PURPOSE : Reads and extracts the contents of the Sw Package File into a temporary path
//
//  PARAMS :
//      - INPUT :
//          - pFileName : Sw Package File name
//
//      - OUTPUT :
//          - pWorkingFolder : temporary path
//        
// RETURNS :
//      - Success/Failure of operation
//
//   NOTES : 

COMMONBASE_API DWORD WINAPI Common_ExpandPackageFile (TCHAR * pFileName,
                                                      TCHAR * pWorkingFolder)
{
  TYPE_PACKAGE_FOOTER     _footer;
  TYPE_LOGGER_PARAM       _log_param1;
  TCHAR                   _tmp_filename [MAX_PATH];
  TCHAR                   _cmd [512];
  BOOL                    _error;
  BYTE                    _buffer [256]; // [64*1024];
  HANDLE                  _file;
  DWORD                   _idx_record;
  BOOL                    _rc_bool;
  TCHAR                   * p_filename;
  TCHAR                   _w_dir [MAX_PATH];
  TCHAR                   _w_pack_file [MAX_PATH];
  TCHAR                   _w_pack_path_file [MAX_PATH];
  DWORD                   _exit_code;
  DWORD                   _fail_reason;

  _exit_code = 0;

  // Generate the working paths and file names
  {
    TCHAR     _aux_drive [MAX_PATH];
    TCHAR     _aux_path [MAX_PATH];
    TCHAR     _aux_temp_path [MAX_PATH];
    TCHAR     _aux_name [MAX_PATH];
    TCHAR     _aux_ext [MAX_PATH];
    TCHAR     _aux_package_ext [MAX_PATH];
    DWORD     _rc;

    _splitpath (pFileName, _aux_drive, _aux_path, _aux_name, _aux_ext);
    
    // Working directory
    _rc = GetTempPath (MAX_PATH, _aux_temp_path);
    if ( _rc == 0 )
    {
      _fail_reason = GetLastError ();

      return UNPACK_ERROR_1;
    } // if

    //_makepath (_w_dir, _aux_drive, _aux_path, _aux_name, NULL);
    _stprintf (_w_dir, _T("%s%s"), _aux_temp_path, _aux_name);

    _stprintf (pWorkingFolder, _T("%s"), _w_dir);

    // Working package file
    _stprintf (_aux_package_ext, _T("%s_%s"), _aux_ext, _T("WORK"));
    _makepath (_w_pack_file, NULL, NULL, _aux_name, _aux_package_ext);
    _makepath (_w_pack_path_file, NULL, _w_dir, _w_pack_file, NULL);
  }

  //GLB_SwdlUnpackProgress.phase_status = UNPACK_OK;

  // Steps :
  //    - Verify package CRC and get information about its contents
  //    - Truncate the file to remove the package's footer
  //    - Expand the truncated package file if it is compressed
  //    - Extract the files from the processed package file
  //    - Delete temporary file

  //    - Verify package CRC and get information about its contents
  _rc_bool = VerifyCRC (pFileName, &_footer, &GLB_SwPackage);
  if ( ! _rc_bool )
  {
    _stprintf (_log_param1, _T("%lu"), _rc_bool); 
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("VerifyCRC"));

    //GLB_SwdlUnpackProgress.phase_status = UNPACK_ERROR_1;
    _fail_reason = GetLastError ();

    return UNPACK_ERROR_1;
  } // if

  //    - Set up the working directory
  //        - Create the working directory
  //        - Copy the package to the working directory
  {
    //      - Create the working directory
    if ( ! CreateDirectory (_w_dir, NULL) )
    {      
      _fail_reason = GetLastError ();

      if (_fail_reason == ERROR_ALREADY_EXISTS )
      {
        // If directory already exists, try to empty it
        _rc_bool = Common_CleanPath (_w_dir, FALSE);
        if (! _rc_bool )
        {
          _fail_reason = GetLastError ();

          return UNPACK_ERROR_1;
        } // if
      } // if      
      else
      {
        // Other IO error
        return FALSE;
      } // else
    }

    //      - Copy the package to the working directory
    
    if ( ! CopyFile (pFileName, _w_pack_path_file, FALSE) )
    {
      _fail_reason = GetLastError ();

      return UNPACK_ERROR_1;
    }
  }

  //    - Truncate the file to remove the package's footer
  //        - Open the package file
  //        - Move the file pointer where the footer starts
  //        - Set the EOF mark
  //        - Close the package file
  _error = TRUE;

  //        - Open the package file
  _file = CreateFile (_w_pack_path_file,              // File name               
                      GENERIC_READ | GENERIC_WRITE,   // Desired Access          
                      0,                              // Share Mode = Not shared  
                      NULL,                           // Security attributes     
                      OPEN_EXISTING,                  // Creation Disposition. Only open if file exists 
                      FILE_ATTRIBUTE_NORMAL,          // Flags And Attributes    
                      0); 

  if ( _file != INVALID_HANDLE_VALUE )
  {
    //    - Move the file pointer where the footer starts
    if ( SetFilePointer (_file,                               // Handle
                         _footer.filesize1,                   // Distance to move
                         NULL,                                // Distance to move High
                         FILE_BEGIN) == _footer.filesize1 )   // Move Method
    {
      //    - Set the EOF mark
      if ( SetEndOfFile (_file) )
      {
        _error = FALSE;
      }
    }

    //      - Close the package file
    CloseHandle (_file);
  }
  else
  {
    // Error opening file
    _stprintf (_log_param1, _T("%lu"), GetLastError ()); 
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("CreateFile"));
  }

  if ( _error )
  {
    //GLB_SwdlUnpackProgress.phase_status = UNPACK_ERROR_1;
    _fail_reason = GetLastError ();

    return UNPACK_ERROR_1;
  }

  //    - Expand the truncated package file if it is compressed
  p_filename = _w_pack_path_file;
  if ( _footer.compressed )
  {
    _stprintf (_tmp_filename, _T("%s_TMP"), _w_pack_path_file);
    _stprintf (_cmd, _T("EXPAND \"%s\" \"%s\""), _w_pack_path_file
                                               , _tmp_filename);

    if ( ! Common_ExecuteSystemCommand (_cmd, &_exit_code) )
    {
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("ExecuteSystemCommand"));

      //GLB_SwdlUnpackProgress.phase_status = UNPACK_ERROR_2;
      _fail_reason = GetLastError ();

      return UNPACK_ERROR_2;
    }

    if ( _exit_code != 0 )
    {
      _stprintf (_log_param1, _T("%lu"), _exit_code); 
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("Command Expand"));

      //GLB_SwdlUnpackProgress.phase_status = UNPACK_ERROR_2;

      _fail_reason = GetLastError ();

      return UNPACK_ERROR_2;
    }

    p_filename = _tmp_filename;
  }

  //    - Extract the files from the processed package file
  //        - Open the package file
  //        - Browse the file list and extract the package files
  //        - Close the package file

  //        - Open the package file
  _file = CreateFile (p_filename,             // File name               
                      GENERIC_READ,           // Desired Access          
                      0,                      // Share Mode = Not shared  
                      NULL,                   // Security attributes     
                      OPEN_EXISTING,          // Creation Disposition. Only open if file exists 
                      FILE_ATTRIBUTE_NORMAL,  // Flags And Attributes    
                      0); 

  if ( _file == INVALID_HANDLE_VALUE )
  {
    _stprintf (_log_param1, _T("%lu"), GetLastError ()); 
    PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("CreateFile"));

    //GLB_SwdlUnpackProgress.phase_status = UNPACK_ERROR_3;
    
    _fail_reason = GetLastError ();

    return UNPACK_ERROR_3;
  }

  _error = FALSE;

  //        - Set up progress information
  //GLB_SwdlUnpackProgress.num_records    = GLB_SwPackage.num_records;
  //GLB_SwdlUnpackProgress.current_record = 0;

  //        - Browse the file list and extract the package files
  for ( _idx_record = 0; _idx_record < GLB_SwPackage.num_records; _idx_record++ )
  {
    if ( ! ExtractRecord (_file, 
                          _w_dir,
                          &GLB_SwPackage.record[_idx_record].file, 
                          sizeof (_buffer), 
                          _buffer) )
    {
      _stprintf (_log_param1, _T("%lu"), FALSE); 
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("ExtractRecord"));

      //GLB_SwdlUnpackProgress.phase_status = UNPACK_ERROR_4;

      _fail_reason = GetLastError ();

      _error = TRUE;

      break;
    }

    //      - Update progress information
    //GLB_SwdlUnpackProgress.current_record = _idx_record;
    //GLB_SwdlUnpackProgress.progress_pct = (((double)GLB_SwdlUnpackProgress.current_record / (double)GLB_SwdlUnpackProgress.num_records) * 100.0);
  } // for
  
  //        - Close the package file
  if ( ! CloseHandle (_file) )
  {
    if ( ! _error )
    {
      _stprintf (_log_param1, _T("%lu"), GetLastError ()); 
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("CloseHandle"));

      //GLB_SwdlUnpackProgress.phase_status = UNPACK_ERROR_5;

      _fail_reason = GetLastError ();

      _error = TRUE;
    }
  }

  //    - Delete temporary files
  //        - Working Package
  //        - Working Uncompressed Package 
 
  //        - Working Package
  if ( ! DeleteFile (_w_pack_path_file) )
  {
    if ( ! _error )
    {
      _stprintf (_log_param1, _T("%lu"), GetLastError ()); 
      PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("DeleteFile"));

      //GLB_SwdlUnpackProgress.phase_status = UNPACK_ERROR_6;

      _fail_reason = GetLastError ();

      _error = TRUE;
    } // if
  } // if

  //        - Working Uncompressed Package 
  if ( _footer.compressed )
  {
    // Delete File
    if ( ! DeleteFile (_tmp_filename) )
    {
      if ( ! _error )
      {
        _stprintf (_log_param1, _T("%lu"), GetLastError ()); 
        PrivateLog (2, MODULE_NAME, __FUNCTION__, _log_param1, _T("DeleteFile"));

        //GLB_SwdlUnpackProgress.phase_status = UNPACK_ERROR_6;

        _fail_reason = GetLastError ();

        _error = TRUE;
      }
    } // if
  } // if

  ////return  GLB_SwdlUnpackProgress.phase_status;
  return UNPACK_OK;
} // Common_ExpandPackageFile

//------------------------------------------------------------------------------
// PURPOSE : Get information of a file by its index
//
//  PARAMS :
//      - INPUT :
//          - Index : Index of chosen file.
//
//      - OUTPUT :
//          - pFileName : Chosen file�s name 
//          - pVersion  : Chosen file�s version information 
//          - pFileSize : Chosen file�s size
//        
// RETURNS :
//      - Success/Failure of operation
//
//   NOTES : 

COMMONBASE_API BOOL WINAPI    Common_GetSwPackageFile (DWORD    Index,
                                                       TCHAR    * pFileName,
                                                       TCHAR    * pVersion,
                                                       DWORD    * pFileSize)
{
  TYPE_PACKAGE_MODULE   * p_file_record;

  if ( Index >= GLB_SwPackage.num_records )
  {
    return FALSE;
  }

  p_file_record = &(GLB_SwPackage.record[Index]);

  memcpy (pFileName, p_file_record->file.name, sizeof (p_file_record->file.name));
  memcpy (pVersion, p_file_record->file.version, sizeof (p_file_record->file.version));

  * pFileSize = p_file_record->file.size;
  
  return TRUE;
} // Common_GetSwPackageFile

//------------------------------------------------------------------------------
// PURPOSE : Reads and checks out the contents of the Sw Package File 
//
//  PARAMS :
//      - INPUT :
//          - pFileName : Sw Package File name
//          - pSwPackageFile
//
//      - OUTPUT :
//        
// RETURNS :
//      - Success/Failure of operation
//
//   NOTES : 

COMMONBASE_API DWORD WINAPI   Common_GetPackageFileDetails (TCHAR                 * pFileName,
                                                            TYPE_SW_PACKAGE_FILE  * pSwPackageFile)
{
  TYPE_PACKAGE_FOOTER     _footer;
  BOOL                    _rc_bool;

  //    - Verify package CRC and get information about its contents
  _rc_bool = VerifyCRC (pFileName, &_footer, &GLB_SwPackage);
  if ( ! _rc_bool )
  {
    return UNPACK_ERROR_1;
  } // if

  pSwPackageFile->terminal_type  = _footer.terminal_type;
  pSwPackageFile->client_id      = _footer.client_id;
  pSwPackageFile->build_id       = _footer.build_id;
  if (_footer.from_build_id == _footer.build_id)
  {
    // Full Package
    pSwPackageFile->build_type     = 0;
  } // if
  else
  {
    // Upgrade Package
    pSwPackageFile->build_type     = 1;
  } // if
  pSwPackageFile->compressed     = _footer.compressed;
  pSwPackageFile->packer_version = _footer.packer_version;
  pSwPackageFile->num_modules    = GLB_SwPackage.num_records;

  return UNPACK_OK;
} // Common_GetPackageFileDetails

//------------------------------------------------------------------------------
// PURPOSE : Copy the files in the global list between two locations
//
//  PARAMS :
//      - INPUT :
//          - pSourcePath : Path where the original files are placed
//          - pTargetPath : Path where the files are to be copied
//
//      - OUTPUT :
//        
// RETURNS :
//      - TRUE : All files have been cleaned
//      - FALSE : Not all files could be copied
//
//   NOTES : 
//      - File list is taken from GLB_SwPackage

COMMONBASE_API BOOL WINAPI   Common_CopyFileList (TCHAR * pSourcePath, TCHAR * pTargetPath)
{
  DWORD     _idx_record;
  TCHAR     _source_file_spec [MAX_PATH];
  TCHAR     _target_file_spec [MAX_PATH];
  BOOL      _copy_result;
  DWORD     _fail_reason;
  BOOL      _aux_result;

  // Sanity check
  if ( pSourcePath == NULL || pTargetPath == NULL )
  {
    return FALSE;
  } // if

  // Try to create directory in target location
  if ( ! CreateDirectory (pTargetPath, NULL) )
  {      
    _fail_reason = GetLastError ();

    if (_fail_reason == ERROR_ALREADY_EXISTS )
    {
      // If directory already exists, try to empty it
      _aux_result = Common_CleanPath (pTargetPath, FALSE);
      if (! _aux_result )
      {
        _fail_reason = GetLastError ();

        return FALSE;
      } // if
    } // if
    else
    {

      return FALSE;
    } // else
  } // if

  for ( _idx_record = 0; _idx_record < GLB_SwPackage.num_records; _idx_record++ )
  {
    // Source and Target Filenames
    _makepath (_source_file_spec, NULL, pSourcePath, GLB_SwPackage.record [_idx_record].file.name, NULL);
    _makepath (_target_file_spec, NULL, pTargetPath, GLB_SwPackage.record [_idx_record].file.name, NULL);

    _copy_result = CopyFile (_source_file_spec, _target_file_spec, TRUE);

    // Abort copy process if a file fails
    if ( ! _copy_result )
    {
      break;      
    } // if
  } // for

  if ( ! _copy_result )
  {
    return FALSE;
  } // if

  return TRUE;
} // Common_CopyFileList
