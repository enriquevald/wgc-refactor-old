//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: CommonGetPassword.pc
//
//   DESCRIPTION: Functions and Methods for DB access in CommonMisc.
//
//        AUTHOR: Alberto Cuesta
//
// CREATION DATE: 20-FEB-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 20-FEB-2007 ACC    Initial draft.
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------


#define INCLUDE_NLS_API
#define INCLUDE_VERSION_CONTROL
#include "CommonDef.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS 
//------------------------------------------------------------------------------
#define PUBLIC_USER_NAME        _T("LKPUBLIC")
#define PUBLIC_PASSWORD         _T("LKPUBLIC")

#define LKROOT_USER_NAME        _T("LKROOT")
#define LKROOT_PASSWORD         _T("LKROOT")

#define LKVIEWER_USER_NAME      _T("LKVIEWER")
#define LKVIEWER_PASSWORD       _T("LKVIEWER")

#define LKGUI_USER_NAME         _T("LKGUI")
#define LKGUI_PASSWORD          _T("LKGUI")

//------------------------------------------------------------------------------
// PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

extern CRITICAL_SECTION      GLB_CriticalSectionUserPassword;

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Gets the password related to the user name
//
//  PARAMS :
//      - INPUT :
//          - pUserName
//
//      - OUTPUT :
//          - pPassword
//
// RETURNS :
//      - RC_OK
//      - RC_ERROR
//
//   NOTES :
 
COMMONBASE_API DWORD WINAPI Common_GetDBUserPassword (TCHAR       * pUserName,
                                                      TCHAR       * pConnectString,
                                                      TCHAR       * pPassword)
{
  __try
  {
    EnterCriticalSection (&GLB_CriticalSectionUserPassword);

    _tcscpy (pPassword, _T(""));
    
    if ( _tcscmp (pUserName, PUBLIC_USER_NAME) == 0 )
    {
      _tcscpy (pPassword, PUBLIC_PASSWORD);
    }
    else if ( _tcscmp (pUserName, LKROOT_USER_NAME) == 0 )
    {
      _tcscpy (pPassword, LKROOT_PASSWORD);
    }
    else if ( _tcscmp (pUserName, LKVIEWER_USER_NAME) == 0 )
    {
      _tcscpy (pPassword, LKVIEWER_PASSWORD);
    }
    else if ( _tcscmp (pUserName, LKGUI_USER_NAME) == 0 )
    {
      _tcscpy (pPassword, LKGUI_PASSWORD);
    }

    if ( _tcslen (pPassword) == 0 )
    {
      return COMMON_ERROR_NOT_FOUND;
    }
  }
  __finally
  {
    LeaveCriticalSection (&GLB_CriticalSectionUserPassword);
  }

  return COMMON_OK;
} // Common_GetDBUserPassword
