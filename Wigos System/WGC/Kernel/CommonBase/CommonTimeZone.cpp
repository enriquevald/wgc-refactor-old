//-----------------------------------------------------------------------------
// Copyright � 2004 Win Systems Ltd.
//-----------------------------------------------------------------------------
//
//   MODULE NAME : CommonBaseTimeZone.cpp
//
//   DESCRIPTION : Functions and Methods to handle TimeZone.
//
//        AUTHOR : Andreu Juli�
//
// CREATION DATE : 30-JUN-2004
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 30-NOV-2004 AJQ First release.
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------
#define INCLUDE_PRIVATE_LOG

#include "CommonDef.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS
//------------------------------------------------------------------------------

static TIME_ZONE_INFORMATION GLB_CurrentTimeZoneInformation;

//------------------------------------------------------------------------------
// PRIVATE DATATYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Returns a pointer to the current Time Zone Information
//
//  PARAMS :
//      - INPUT :
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES :

static TIME_ZONE_INFORMATION * WINAPI GetCurrentTimeZoneInformation (void)
{
  static BOOL _init = FALSE;

  if ( !_init )
  {
    if ( GetTimeZoneInformation (&GLB_CurrentTimeZoneInformation) == TIME_ZONE_ID_INVALID )
    {
      TCHAR _error_code [20];

      _stprintf (_error_code, _T("%lu"), GetLastError ());
      PrivateLog (2, _T("COMMON_BASE"), _T(__FUNCTION__), _error_code, _T("GetTimeZoneInformation")); 

      return NULL;
    }
    _init = TRUE;
  }

  return &GLB_CurrentTimeZoneInformation;

} // GetCurrentTimeZoneInformation

//------------------------------------------------------------------------------
// PURPOSE : Converts a Local Time into a UTC Time.
//
//  PARAMS :
//      - INPUT :
//          - SYSTEMTIME / CTime : pLocalTime
//          - pTimeZoneInfo: the TimeZone
//
//      - OUTPUT :
//          - SYSTEMTIME / CTime : pUniversalTime
//
// RETURNS :
//
//   NOTES :

COMMONBASE_API void WINAPI LocalTimeToUniversalTime (SYSTEMTIME            * pLocalTime, 
                                                     SYSTEMTIME            * pUniversalTime, 
                                                     TIME_ZONE_INFORMATION * pTimeZoneInfo)
{
  TIME_ZONE_INFORMATION   * p_tzi;

  if ( pTimeZoneInfo == NULL )
  {
    p_tzi = GetCurrentTimeZoneInformation (); 
  }
  else
  {
    p_tzi = pTimeZoneInfo;
  }

  TzSpecificLocalTimeToSystemTime (p_tzi,            // TimeZoneInformation
                                   pLocalTime,       // LocalTime
                                   pUniversalTime);  // UniversalTime
} // LocalTimeToUniversalTime

COMMONBASE_API void WINAPI LocalTimeToUniversalTime (CTIME                 * pLocalTime, 
                                                     CTIME                 * pUniversalTime, 
                                                     TIME_ZONE_INFORMATION * pTimeZoneInfo)
{
  SYSTEMTIME  _local;
  SYSTEMTIME  _utc;

  CTimeToSystemTime (* pLocalTime, &_local);
  LocalTimeToUniversalTime (&_local, &_utc, pTimeZoneInfo);
  SystemTimeToCTime (&_utc, pUniversalTime);

} // LocalTimeToUniversalTime

//------------------------------------------------------------------------------
// PURPOSE : Converts an UTC Time into a Local Time.
//
//  PARAMS :
//      - INPUT :
//          - SYSTEMTIME / Ctime: pUniversalTime
//
//      - OUTPUT :
//          - SYSTEMTIME / Ctime: pLocalTime
//
// RETURNS :
//
//   NOTES :

COMMONBASE_API void WINAPI UniversalTimeToLocalTime (SYSTEMTIME            * pUniversalTime,
                                                     SYSTEMTIME            * pLocalTime, 
                                                     TIME_ZONE_INFORMATION * pTimeZoneInfo)
{
  TIME_ZONE_INFORMATION * p_tzi;

  if ( pTimeZoneInfo == NULL )
  {
    p_tzi = GetCurrentTimeZoneInformation (); 
  }
  else
  {
    p_tzi = pTimeZoneInfo;
  }

  SystemTimeToTzSpecificLocalTime (p_tzi,           // TimeZoneInformation
                                   pUniversalTime,  // UniversalTime
                                   pLocalTime);     // LocalTime
} // UniversalTimeToLocalTime

COMMONBASE_API void WINAPI UniversalTimeToLocalTime (CTIME                 * pUniversalTime,
                                                     CTIME                 * pLocalTime, 
                                                     TIME_ZONE_INFORMATION * pTimeZoneInfo)
{
  SYSTEMTIME  _local;
  SYSTEMTIME  _utc;

  CTimeToSystemTime (* pUniversalTime, &_utc);
  UniversalTimeToLocalTime (&_utc, &_local, pTimeZoneInfo);
  SystemTimeToCTime (&_local, pLocalTime);
}

//------------------------------------------------------------------------------
// PURPOSE : Converts a System Time value into a CTIME one.
//
//  PARAMS :
//      - INPUT :
//          - SYSTEMTIME : pSystemTime
//
//      - OUTPUT :
//          - CTIME: pCTime
//
// RETURNS :
//
//   NOTES :

COMMONBASE_API void WINAPI SystemTimeToCTime (SYSTEMTIME  * pSystemTime,
                                              CTIME       * pCTime)
{
  ULARGE_INTEGER  _time_ul;
  FILETIME        _time_ft;

  SystemTimeToFileTime (pSystemTime, &_time_ft);

  _time_ul.LowPart   = _time_ft.dwLowDateTime;
  _time_ul.HighPart  = _time_ft.dwHighDateTime;
  _time_ul.QuadPart -= (DWORDLONG) 116444736000000000;
  _time_ul.QuadPart /= (DWORDLONG) 10000000;

  * pCTime = (DWORD) _time_ul.QuadPart;

} // SystemTimeToCTime

//------------------------------------------------------------------------------
// PURPOSE : Converts a CTIME value into a SYSTEMTIME one.
//
//  PARAMS :
//      - INPUT :
//          - CTIME : CTime
//
//      - OUTPUT :
//          - SYSTEMTIME: pSystemTime
//
// RETURNS :
//
//   NOTES :

COMMONBASE_API void WINAPI CTimeToSystemTime (CTIME      CTime, 
                                              SYSTEMTIME * pSystemTime)
{
  ULARGE_INTEGER  _time_ul;
  FILETIME        _time_ft;

  _time_ul.QuadPart  = (DWORDLONG) CTime;
  _time_ul.QuadPart *= (DWORDLONG) 10000000;
  _time_ul.QuadPart += (DWORDLONG) 116444736000000000;

  _time_ft.dwLowDateTime  = _time_ul.LowPart;
  _time_ft.dwHighDateTime = _time_ul.HighPart;

  FileTimeToSystemTime (&_time_ft, pSystemTime);

} // CTimeToSystemTime

//------------------------------------------------------------------------------
// PURPOSE : Truncates the time of a Datetime.
//
//  PARAMS :
//      - INPUT / OUTPUT :
//          - CTIME : pDateTime
//
//      - OUTPUT :
//          - CTIME: pDate
//
// RETURNS :
//
//   NOTES :

COMMONBASE_API void WINAPI TruncateTime (CTIME * pDateTime, 
                                         CTIME * pDate)
{
  SYSTEMTIME  _aux;
  CTIME       * p_date;

  if ( pDate != NULL )
  {
    p_date = pDate;
  }
  else
  {
    p_date = pDateTime;
  }
  CTimeToSystemTime (*pDateTime, &_aux);
  TruncateTime (&_aux);
  SystemTimeToCTime (&_aux, p_date);

} // TruncateTime

COMMONBASE_API void WINAPI TruncateTime (SYSTEMTIME * pDateTime, 
                                         SYSTEMTIME * pDate)
{
  SYSTEMTIME * p_date;

  if ( pDate != NULL )
  {
    p_date = pDate;
    * p_date = * pDateTime;
  }
  else
  {
    p_date = pDateTime;
  }

  p_date->wHour         = 0;
  p_date->wMinute       = 0;
  p_date->wSecond       = 0;
  p_date->wMilliseconds = 0;

} // TruncateTime

//------------------------------------------------------------------------------
// PURPOSE : Gets the current DateTime of the LOCAL or UTC time.
//
//  PARAMS :
//      - INPUT :
//          - ENUM_GET_TIME : GetTimeType
//
//      - OUTPUT :
//          - SYSTEMTIME / CTime: pNow
//
// RETURNS :
//
//   NOTES :

COMMONBASE_API void WINAPI GetTime   (ENUM_GET_TIME GetTimeType, 
                                      SYSTEMTIME    * pNow)
{

  if ( GetTimeType == GET_TIME_LOCAL_TIME )
  {
    SYSTEMTIME _utc;

    GetSystemTime (&_utc);
    UniversalTimeToLocalTime (&_utc, pNow);

    return;
  }

  GetSystemTime (pNow);

} // GetTime

COMMONBASE_API void WINAPI GetTime   (ENUM_GET_TIME GetTimeType, 
                                      CTIME         * pNow)
{
  SYSTEMTIME _aux;

  GetTime (GetTimeType, &_aux);
  SystemTimeToCTime (&_aux, pNow);

} // GetTime

//------------------------------------------------------------------------------
// PURPOSE : Gets the current Date of the LOCAL or UTC Date.
//
//  PARAMS :
//      - INPUT :
//          - ENUM_GET_TIME : GetTimeType
//
//      - OUTPUT :
//          - SYSTEMTIME / CTime: pToday
//
// RETURNS :
//
//   NOTES :

COMMONBASE_API void WINAPI GetDate (ENUM_GET_TIME GetTimeType, 
                                    CTIME         * pToday)
{
  SYSTEMTIME _aux;

  GetDate (GetTimeType, &_aux);
  SystemTimeToCTime (&_aux, pToday);

} // GetDate

COMMONBASE_API void WINAPI GetDate (ENUM_GET_TIME GetTimeType, 
                                    SYSTEMTIME    * pToday)
{
  SYSTEMTIME  _local;

  GetTime (GET_TIME_LOCAL_TIME, &_local);
  _local.wHour         = 0;
  _local.wMinute       = 0;
  _local.wSecond       = 0;
  _local.wMilliseconds = 0;

  if ( GetTimeType == GET_TIME_LOCAL_TIME )
  {
    * pToday = _local;

    return;
  }

  LocalTimeToUniversalTime (&_local, pToday);

} // GetDate
