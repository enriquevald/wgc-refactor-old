//-----------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//-----------------------------------------------------------------------------
// 
//   MODULE NAME : CommonBaseConfig.cpp
// 
//   DESCRIPTION : Functions and Methods of the registry information.
// 
//        AUTHOR : Ignasi Ripoll
// 
// CREATION DATE : 01-DEC-2003
// 
// REVISION HISTORY
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-DEC-2003 IRP    Initial draft.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_BASE
#define INCLUDE_PRIVATE_LOG
#include "CommonDef.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

#define NUM_DEFAULT_FILE_LINES        20
#define NUM_LINES_REG_IN_LKS_SYS_CFG  4
#define LOG_AUX_STRING_LENGTH         256
#define MODULE_NAME                   _T("COMMON_BASE")

//------------------------------------------------------------------------------
// PRIVATE DATATYPES 
//------------------------------------------------------------------------------
typedef struct
{
  TCHAR  registry_path [COMMON_DEFAULT_STRING_SIZE]; 
  TCHAR  connect_string [COMMON_DB_CONNECT_STRING_SIZE + 1]; 
  DWORD  language_id; 
  DWORD  client_id;

} TYPE_LKS_SYSTEM_CFG;

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------
TCHAR     GLB_UserName [COMMON_DB_USER_SIZE + 1] = _T("LKGUI");

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------
static BOOLEAN   ReadLineFromFile (FILE     * pFile, 
                                   TCHAR    * pTextLine);

static DWORD     Read_LkSystemCfg (TCHAR    * pRegistryPath, 
                                   DWORD    * pClientId);

static DWORD     Common_CreateFileConfiguration ();

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Gets Registry Path and Client Id from System Configuration File
// 
//  PARAMS :
//      - INPUT :
//          - None
//
//      - OUTPUT : 
//          - pRegistryPath : Key value in Registry for LK system.
//          - pClientId : Client Id value in Registry for LK system.
// 
// RETURNS : 
//      - True: if succesfull both key values
//      - False: Otherwise
//
//   NOTES :

DWORD Read_LkSystemCfg (TYPE_LKS_SYSTEM_CFG * pParamList)
{
  FILE      * p_file;
  TCHAR     _line_string [COMMON_DEFAULT_STRING_SIZE];
  TCHAR     * p_contents;
  DWORD     _len_contents;
  TCHAR     * pLanguageIdChar; 
  DWORD     _rc;
  BOOL      _connect_string_found = FALSE; 
  BOOL      _language_found = FALSE; 
  BOOL      _registry_found = FALSE; 
  
  // Open file for read information.
  p_file = _tfopen (COMMON_CONF_FILE_NAME, _T("rt"));
  if ( p_file == NULL )
  {
    _rc = Common_CreateFileConfiguration();
    if (_rc != COMMON_OK) 
    {
      return COMMON_ERROR_NOT_FOUND;
    }

    p_file = _tfopen (COMMON_CONF_FILE_NAME, _T("rt"));
    if ( p_file == NULL )
    {
      return COMMON_ERROR_NOT_FOUND;
    }
  } // if

  // Read lines until found REGISTRY entry
  for ( ;; )
  {
    // Read configuration line (skip comentaries)
    if ( ! ReadLineFromFile (p_file, _line_string) )
    {
      break;
    }

    // Check entry:
    //      - REGISTRY
    if ( ! _tcsnicmp (_line_string, COMMON_CONF_ENTRY_REGISTRY, _tcslen (COMMON_CONF_ENTRY_REGISTRY)) )
    {
      p_contents = _line_string + _tcslen (COMMON_CONF_ENTRY_REGISTRY);
      _len_contents = _tcslen (p_contents);

      if (   p_contents[0] == COMMON_SPACE_CHAR 
          || p_contents[_len_contents - 1] == COMMON_SPACE_CHAR 
          || p_contents[_len_contents - 1] == COMMON_BACKSLASH_CHAR )
      {
        break;
      }

      _tcscpy (pParamList->registry_path, _tcsupr(_line_string + _tcslen (COMMON_CONF_ENTRY_REGISTRY)));
      _registry_found = TRUE;
    } 

    // Check entry:
    //      - CONNECT_STRING
    if ( ! _tcsnicmp (_line_string, COMMON_CONF_ENTRY_CONNECT_STRING, _tcslen (COMMON_CONF_ENTRY_CONNECT_STRING)) )
    {
      p_contents = _line_string + _tcslen (COMMON_CONF_ENTRY_CONNECT_STRING);
      _len_contents = _tcslen (p_contents);

      if (   p_contents[0] == COMMON_SPACE_CHAR 
          || p_contents[_len_contents - 1] == COMMON_SPACE_CHAR 
          || p_contents[_len_contents - 1] == COMMON_BACKSLASH_CHAR )
      {
        break;
      }

      _tcscpy (pParamList->connect_string, _tcsupr(_line_string + _tcslen (COMMON_CONF_ENTRY_CONNECT_STRING)));
      _connect_string_found = TRUE;
    }

    // Check entry:
    //      - LANGUAGE_ID
    if ( ! _tcsnicmp (_line_string, COMMON_CONF_ENTRY_LANGUAGE_ID, _tcslen (COMMON_CONF_ENTRY_LANGUAGE_ID)) )
    {
      pLanguageIdChar = _line_string + _tcslen (COMMON_CONF_ENTRY_LANGUAGE_ID);
      pParamList->language_id = (DWORD) _tstoi (pLanguageIdChar);
      if ( pParamList->language_id == 0 )
      {
        // Invalid Format
        break;
      }

      _language_found = TRUE;
    }  
  } // for

  fclose (p_file);
  
  if ( !_connect_string_found )
  {
    PrivateLog (11, MODULE_NAME, _T(__FUNCTION__), COMMON_CONF_ENTRY_CONNECT_STRING, COMMON_CONF_FILE_NAME);

    return COMMON_ERROR;
  }

  if ( !_language_found )
  {
    PrivateLog (11, MODULE_NAME, _T(__FUNCTION__), COMMON_CONF_ENTRY_LANGUAGE_ID, COMMON_CONF_FILE_NAME);

    return COMMON_ERROR;
  }

  if ( !_registry_found )
  {
    PrivateLog (11, MODULE_NAME, _T(__FUNCTION__), COMMON_CONF_ENTRY_REGISTRY, COMMON_CONF_FILE_NAME);

    return COMMON_ERROR;
  }
 
  return COMMON_OK;  
} // Read_LkSystemCfg

//------------------------------------------------------------------------------
// PURPOSE : Read one line from file.
// 
//  PARAMS :
//      - INPUT :
//          - pFile : handle to file.
//
//      - OUTPUT : 
//          - pTextLine : read line
// 
// RETURNS : 
//      - TRUE
//      - FALSE
//
//   NOTES :
//      - Comment lines: starting ';' character are ignored
//

static BOOLEAN   ReadLineFromFile (FILE * pFile, TCHAR * pTextLine)
{
  for ( ;; )
  {
    if ( _fgetts (pTextLine, COMMON_DEFAULT_STRING_SIZE, pFile) == NULL )
    {
      return FALSE;
    }

    // Discard lines beginning with this characters.
    if (   pTextLine [0] == COMMON_SEMICOLON_CHAR 
        || pTextLine [0] == COMMON_SPACE_CHAR
        || pTextLine [0] == COMMON_LF_CHAR 
        || pTextLine [0] == COMMON_CR_CHAR )
    {
      continue;
    } // if

    break;
  }

  if (   pTextLine [_tcslen(pTextLine) - 1] == COMMON_LF_CHAR
      || pTextLine [_tcslen(pTextLine) - 1] == COMMON_CR_CHAR )
  {
    pTextLine [_tcslen(pTextLine) - 1] = COMMON_NULL_CHAR;
  } // if

  if (   pTextLine [_tcslen(pTextLine) - 1] == COMMON_LF_CHAR
      || pTextLine [_tcslen(pTextLine) - 1] == COMMON_CR_CHAR )
  {
    pTextLine [_tcslen(pTextLine) - 1] = COMMON_NULL_CHAR;
  }

  return TRUE;

} // ReadLineFromFile

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Get LK system configuration from system file.
//          Entrys to get:
//              - Configuration
//                  - System Language
//                  - Log files path
// 
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT : 
//          - pConfiguration : struct with entry values
// 
// RETURNS : 
//      - COMMON_OK
//      - COMMON_ERROR
//      - COMMON_ERROR_CONTROL_BLOCK
//
//   NOTES :

COMMONBASE_API DWORD WINAPI Common_GetConfigurationFile (TYPE_CENTER_CONFIGURATION  * pConfiguration)
{
  TYPE_LKS_SYSTEM_CFG _cfg_list;

  // Check control block
  if ( pConfiguration->control_block != sizeof (TYPE_CENTER_CONFIGURATION) )
  {
    return COMMON_ERROR_CONTROL_BLOCK;
  }

  if ( Read_LkSystemCfg (&_cfg_list) != COMMON_OK )
  {
    return COMMON_ERROR;
  }

  pConfiguration->language = _cfg_list.language_id;  
  _tcscpy (pConfiguration->database.p_connect_string, _cfg_list.connect_string);
  _tcscpy (pConfiguration->database.p_user, GLB_UserName);

  return COMMON_OK;
} // Common_GetConfigurationFile

//------------------------------------------------------------------------------
// PURPOSE : Get LK system configuration from registry.
//          Entrys to get:
//              - Configuration
//                  - System Language
//                  - Log files path
//              - DataBase 
//                  - User DB
//                  - Password DB 
//                  - Connect string DB
// 
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT : 
//          - pConfiguration : struct with entry values
// 
// RETURNS : 
//      - COMMON_OK
//      - COMMON_ERROR
//      - COMMON_ERROR_CONTROL_BLOCK
//      - COMMON_ERROR_DB
//
//   NOTES :

COMMONBASE_API DWORD WINAPI Common_GetConfiguration (TYPE_CENTER_CONFIGURATION * pConfiguration)
{
  DWORD                 _rc;
  TYPE_LKS_SYSTEM_CFG   _cfg_list;
  TYPE_LOGGER_PARAM     _log_param1; 

  // Check control block
  if ( pConfiguration->control_block != sizeof (TYPE_CENTER_CONFIGURATION) )
  {
    return COMMON_ERROR_CONTROL_BLOCK;
  }

  _rc = Read_LkSystemCfg (&_cfg_list); 
  if ( _rc != COMMON_OK )
  {
    _stprintf (_log_param1, _T("%lu"), _rc);
    PrivateLog (2, MODULE_NAME, _T(__FUNCTION__), _log_param1, _T("Read_LkSystemCfg"));

    return COMMON_ERROR;
  }

  pConfiguration->language = _cfg_list.language_id;
  _tcscpy (pConfiguration->database.p_user, GLB_UserName);
  _tcscpy (pConfiguration->database.p_connect_string, _cfg_list.connect_string);

  _rc = Common_GetDBUserPassword (GLB_UserName, 
                                  pConfiguration->database.p_connect_string, 
                                  pConfiguration->database.p_password);
  if ( _rc != COMMON_OK )
  {
    _stprintf (_log_param1, _T("%lu"), _rc);
    PrivateLog (2, MODULE_NAME, _T(__FUNCTION__), _log_param1, _T("Common_GetDBUserPassword"));

    return COMMON_ERROR_DB;
  }

  return COMMON_OK;
} // Common_GetConfiguration

//------------------------------------------------------------------------------
// PURPOSE : Gets the LKS Registry Root Path from System Configuration File
// 
//  PARAMS :
//      - INPUT :
//          - None
//
//      - OUTPUT : 
//          - pRegistryRoot : Registry Root Path value for LK system.
// 
// RETURNS : 
//      - True:  Success
//      - False: File not found, Error
//
//   NOTES :

COMMONBASE_API BOOL WINAPI Common_GetRegistryRoot (TCHAR  * pRegistryRoot)
{
  TYPE_LKS_SYSTEM_CFG _list_cfg;
  
  if ( Read_LkSystemCfg (&_list_cfg) == COMMON_OK )
  {
    _tcscpy (pRegistryRoot, _list_cfg.registry_path);
    return TRUE;
  }
  
  return FALSE;
} // Common_GetRegistryRoot

//------------------------------------------------------------------------------
// PURPOSE : Set DB UserName
// 
//  PARAMS :
//      - INPUT :
//          - pDBUser: Database user Name
//
//      - OUTPUT : 
//          - None
// 
// RETURNS : 
//      - TRUE on success
//
//   NOTES :

COMMONBASE_API BOOL WINAPI  Common_SetDBUserName (TCHAR * pDBUserName)
{
  if ( strlen(pDBUserName) > COMMON_DB_USER_SIZE )
  {
    return FALSE;
  }
  
  _tcscpy (GLB_UserName, pDBUserName);

  return TRUE;
} // Common_SetDBUserName

//------------------------------------------------------------------------------
// PURPOSE : Create default file LKSystem.cfg
// 
//  PARAMS :
//      - INPUT :
//          - None
//
//      - OUTPUT : 
//          - None
// 
// RETURNS : 
//      - COMMON_OK
//      - COMMON_ERROR
//
//   NOTES :

static DWORD Common_CreateFileConfiguration ()
{
  FILE       * p_file;
  TCHAR      _line_string [NUM_DEFAULT_FILE_LINES][COMMON_DEFAULT_STRING_SIZE];
  DWORD      _rc;
  WORD       _idx_line;
  
  // Create file
  p_file = _tfopen (COMMON_CONF_FILE_NAME, _T("wt"));
  if ( p_file == NULL )
  {
    return COMMON_ERROR;
  }

  // Default return code
  _rc = COMMON_OK;

  // Assign lines
  _tcscpy (_line_string [0], _T(";\n"));
  _tcscpy (_line_string [1], _T("; Sample LKSystem.cfg file for Setup application.\n"));
  _tcscpy (_line_string [2], _T(";\n"));
  _tcscpy (_line_string [3], _T("\n"));
  _tcscpy (_line_string [4], _T("; ----------------------------------------------------------------\n"));
  _tcscpy (_line_string [5], _T("; REGISTRY: The configuration system is created in the registry \n"));
  _tcscpy (_line_string [6], _T(";           path from LOCAL_MACHINE key.\n"));
  _tcscpy (_line_string [7], _T(";\n"));
  _tcscpy (_line_string [8], _T(";        Ex: REGISTRY=SOFTWARE\\LK_SYSTEM, the configuration is \n"));
  _tcscpy (_line_string [9], _T(";            created in path: \n"));
  _tcscpy (_line_string [10], _T(";\n"));
  _tcscpy (_line_string [11], _T(";               HKEY_LOCAL_MACHINE\\SOFTWARE\\LK_SYSTEM\n"));
  _tcscpy (_line_string [12], _T(";\n"));
  _tcscpy (_line_string [13], _T("; ----------------------------------------------------------------\n"));
  _tcscpy (_line_string [14], _T("\n"));
  _tcscpy (_line_string [15], _T("REGISTRY=SOFTWARE\\LK_SYSTEM\n"));
  _tcscpy (_line_string [16], _T("LANGUAGE_ID=9\n")); // AJQ 18-MAR-2004, Default Language = English
  _tcscpy (_line_string [17], _T("CONNECT_STRING=LKDB\n"));
  _tcscpy (_line_string [18], _T("\n"));
  _tcscpy (_line_string [19], _T("\n"));

  // Write lines 
  for ( _idx_line = 0; _idx_line < NUM_DEFAULT_FILE_LINES; _idx_line++ )
  {
    if ( _fputts (_line_string [_idx_line] , p_file) == WEOF )
    {
      _rc = COMMON_ERROR;
    }
  }

  fclose (p_file);

  return _rc;
} // Common_CreateFileConfiguration
