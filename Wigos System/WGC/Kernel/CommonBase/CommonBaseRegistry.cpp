//-----------------------------------------------------------------------------
// Copyright � 2002-2008 Win Systems Ltd.
//-----------------------------------------------------------------------------
// 
//   MODULE NAME : CommonBaseRegistry.cpp
// 
//   DESCRIPTION : Functions related to registry information management.
// 
//        AUTHOR : Alberto Cuesta
// 
// CREATION DATE : 13-MAR-2002
// 
// REVISION HISTORY
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 13-MAR-2002 ACC    Initial draft.
// 30-OCT-2002 AJQ    Registry functions moved to the CommonBase.dll
// 31-NOV-2003 IRP    Move from CommonMisc To CommonBase
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_BASE
#define INCLUDE_VERSION_CONTROL
#define INCLUDE_COMMON_MISC

#include "CommonDef.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS 
//------------------------------------------------------------------------------
#define SEPARATOR_KEYS            _T("\\")

//------------------------------------------------------------------------------
// PRIVATE DATATYPES 
//------------------------------------------------------------------------------

typedef TYPE_COMMON_VECTOR TYPE_REGISTRY_DATA_LIST;

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------
COMMONBASE_API DWORD WINAPI  Common_GetRegistryValue (TYPE_REGISTRY_DATA_ITEM * pRegistryDataItem);

COMMONBASE_API BOOL WINAPI   Common_GetRegistryRoot (TCHAR  * pRegistryRoot);

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE :  Gets all the values from the registry subkeys belonging to the specified key.
// 
//  PARAMS :
//      - INPUT :
//          - pKeyRoot: Key root.
//          - KeyType: Key type.
//                - COMMON_REG_KEY_TYPE_MACHINE (HKEY_LOCAL_MACHINE)
//                - COMMON_REG_KEY_TYPE_USER (HKEY_CURRENT_USER)
//          - pKeyName: Path of the key.
//      
//      - OUTPUT : 
//          - pRegistryData: Vector containing all the values
// 
// RETURNS : 
//      - COMMON_BASE_ERROR
//      - COMMON_BASE_OK
//
//   NOTES :

COMMONBASE_API DWORD WINAPI  Common_GetRegistryData (TCHAR * pKeyRoot, DWORD KeyType, TCHAR * pKeyName, TYPE_REGISTRY_DATA_LIST * pRegistryData)
{
  HKEY      _hk;
  DWORD     _rc;
  TCHAR     _ach_key[COMMON_MAX_REGISTRY_KEY_NAME]= ""; 
  TCHAR     _ach_class[MAX_PATH] = "";  // buffer for class name 
  DWORD     _cch_class_name = MAX_PATH; // length of class string 
  DWORD     _csub_keys;                 // number of subkeys 
  DWORD     _cb_max_sub_key;            // longest subkey size 
  DWORD     _cch_max_class;             // longest class string 
  DWORD     _cvalues;                   // number of values for key 
  DWORD     _cch_max_value;             // longest value name 
  DWORD     _cb_max_value_data;         // longest value data 
  DWORD     _cb_security_descriptor;    // size of security descriptor 
  FILETIME  _ft_last_write_time;
  DWORD     _i=0;
  DWORD     _z=0; 
  DWORD     _ret_code;
  DWORD     _ret_value; 
  TCHAR     _ach_value [COMMON_MAX_REGISTRY_VALUE_NAME]; 
  DWORD     _cch_value;
  DWORD     _cch_value_data; 
  DWORD     _dwType;
  DWORD     size_path;
  TCHAR     _key_name_complete [MAX_PATH];
  TCHAR     _key_name_copy [MAX_PATH];  
  HKEY      _hkey;
  TYPE_REGISTRY_DATA_ITEM *_p_items;

  _cch_value_data = COMMON_MAX_REGISTRY_VALUE_DATA;

  memset(_key_name_complete, 0, sizeof(_key_name_complete));

  _tcscat(_key_name_complete, pKeyRoot);  
  _tcscat(_key_name_complete, SEPARATOR_KEYS);
  _tcscat(_key_name_complete, pKeyName);

  if ( KeyType == COMMON_REG_KEY_TYPE_MACHINE )
  {
    _hkey = HKEY_LOCAL_MACHINE;
  }
  else
  {
    _hkey = HKEY_CURRENT_USER;
  }
    
    // Get the key's handle
  _rc = RegOpenKeyEx (_hkey, 
                      _key_name_complete, 
                      0, 
                      KEY_READ, 
                      &_hk);
  
  if ( _rc != ERROR_SUCCESS )
  {
    return COMMON_BASE_ERROR;
  }

  RegQueryInfoKey (_hk,                       // key handle 
                   _ach_class,                // buffer for class name 
                   &_cch_class_name,          // length of class string 
                   NULL,                      // reserved 
                   &_csub_keys,               // number of subkeys 
                   &_cb_max_sub_key,          // longest subkey size 
                   &_cch_max_class,           // longest class string 
                   &_cvalues,                 // number of values for this key 
                   &_cch_max_value,           // longest value name 
                   &_cb_max_value_data,       // longest value data 
                   &_cb_security_descriptor,  // security descriptor 
                   &_ft_last_write_time);     // last write time 

  if ( ! _cvalues && ! _csub_keys )
  {
    return COMMON_BASE_ERROR;
  }

  // LRS, Loop for the Subkeys
  for ( _i = 0, _ret_code = ERROR_SUCCESS; _ret_code == ERROR_SUCCESS; _i++) 
  { 
    _ret_code = RegEnumKeyEx (_hk, 
                              _i, 
                              _ach_key, 
                              (LPDWORD) &size_path, 
                              NULL, 
                              NULL, 
                              NULL, 
                              &_ft_last_write_time); 

    if ( _ret_code == ERROR_SUCCESS)
    {  
      // LRS, a new Subkey Exists
      _stprintf(_key_name_copy, _T("%s"), pKeyName);
      _tcscat(pKeyName, SEPARATOR_KEYS);
      _tcscat(pKeyName, _ach_key);

      RegCloseKey (_hk);
      // LRS, Recursive
      _rc = Common_GetRegistryData(pKeyRoot, KeyType, pKeyName, pRegistryData);
      _stprintf(pKeyName, _T("%s"), _key_name_copy);
      _rc = RegOpenKeyEx (_hkey, 
                          _key_name_complete,  
                          0, 
                          KEY_READ, 
                          &_hk);
    }
  }

  // Get the Values of the current Key
  _p_items = (TYPE_REGISTRY_DATA_ITEM *) pRegistryData->p_items;  
          
  for ( _i = 0, _ret_value = ERROR_SUCCESS; _i < _cvalues; _i++) 
  { 
    _cch_value = COMMON_MAX_REGISTRY_KEY_NAME; 
    _z = pRegistryData->num_items + _i;

  /*  if ( _z > COMMON_MAX_REGISTRY_KEYS)
    {
      return COMMON_BASE_ERROR;
    }*/

    _ret_value = RegEnumValue(_hk, 
                              _i, 
                              _ach_value, 
                              &_cch_value, 
                              NULL, 
                              &_dwType,    
                              NULL,
                              &_cch_value_data);

    if ( _ret_value != (DWORD) ERROR_SUCCESS && _ret_value != ERROR_INSUFFICIENT_BUFFER) 
    { 
      return COMMON_BASE_ERROR;
    } 

    _p_items[_z].key_type  = KeyType;
    _p_items[_z].value_type = _dwType;
    _stprintf(_p_items[_z].key_name, _T("%s"), pKeyName);
    _stprintf(_p_items[_z].value_name, _T("%s"), _ach_value);
    _p_items[_z].value_length = _cch_value_data;

    RegCloseKey (_hk);
    Common_GetRegistryValue(pKeyRoot, &_p_items[_z]);
    _rc = RegOpenKeyEx (_hkey, 
                        _key_name_complete, 
                        0, 
                        KEY_READ, 
                        &_hk);
  } 
  
  pRegistryData->num_items = pRegistryData->num_items + _cvalues;
  RegCloseKey (_hk);

  return COMMON_BASE_OK;
} // Common_GetRegistryData

//------------------------------------------------------------------------------
// PURPOSE: Sets into the registry all the values contained in the given vector
// 
//  PARAMS:
//      - INPUT :
//        - pKeyRoot: Key root.
//        - pRegistryData: Vector containing all the values
//      
//      - OUTPUT:
// 
// RETURNS: 
//      - COMMON_BASE_OK
//      - COMMON_BASE_ERROR
//
//   NOTES :

COMMONBASE_API DWORD WINAPI  Common_SetRegistryData (TCHAR * pKeyRoot, TYPE_REGISTRY_DATA_LIST * pRegistryData)
{
  HKEY    _hk;
  DWORD   _rc;
  WORD    _idx;
  DWORD    _aux_dword;
  BYTE *  _p_data;
  TCHAR    _key_name_complete[MAX_PATH];
  TYPE_REGISTRY_DATA_ITEM *_p_items;
  HKEY    _hkey;
  
  _p_items = (TYPE_REGISTRY_DATA_ITEM *) pRegistryData->p_items;  
  
  for ( _idx = 0; _idx < pRegistryData->num_items ; _idx++)
  {
    if ( (_p_items[_idx].key_name == NULL) || 
        (_p_items[_idx].value_name == NULL) ||
        (_p_items[_idx].value_type == NULL))
    {
      return COMMON_BASE_ERROR;
    }
    
    memset(_key_name_complete, 0, sizeof(_key_name_complete));
    _tcscat(_key_name_complete, pKeyRoot);  
    _tcscat(_key_name_complete, SEPARATOR_KEYS);
    _tcscat(_key_name_complete, _p_items[_idx].key_name);

    if ( _p_items[_idx].key_type  == COMMON_REG_KEY_TYPE_MACHINE )
    {
      _hkey = HKEY_LOCAL_MACHINE;
    }
    else
    {
      _hkey = HKEY_CURRENT_USER;
    }
    
    // Get the key's handle
    _rc = RegOpenKeyEx (_hkey, 
                        _key_name_complete, 
                        0, 
                        KEY_WRITE, 
                        &_hk);
  
    if ( _rc != ERROR_SUCCESS )
    {
        // Get the key's handle
      _rc = RegCreateKeyEx (_hkey, 
                            _key_name_complete, 
                            0, 
                            _T(""), 
                            REG_OPTION_NON_VOLATILE, 
                            KEY_WRITE, 
                            NULL, 
                            &_hk, 
                            NULL);

      if ( _rc != ERROR_SUCCESS )
      {
        return COMMON_BASE_ERROR;
      }
    }

    switch ( _p_items[_idx].value_type)
    {
      case REG_SZ :
        _p_data = (BYTE *) _p_items[_idx].value_data;
        _rc = RegSetValueEx (_hk, 
                             _p_items[_idx].value_name , 
                             0, 
                             REG_SZ, 
                             _p_data ,
                             _p_items[_idx].value_length);

      break;

      case REG_DWORD:
        _aux_dword = (DWORD) _tstoi (_p_items[_idx].value_data);
        _p_data = (BYTE *) &_aux_dword;
        _rc = RegSetValueEx (_hk, 
                             _p_items[_idx].value_name , 
                             0, 
                             REG_DWORD, 
                             _p_data ,
                             sizeof (DWORD));
      break;

      case REG_BINARY:
        _p_data = (BYTE *) _p_items[_idx].value_data;
        _rc = RegSetValueEx (_hk, 
                             _p_items[_idx].value_name , 
                             0, 
                             REG_BINARY, 
                             _p_data ,
                             _p_items[_idx].value_length);
      break;
      
      default :
      break;
    }
        
    RegCloseKey (_hk);

    if ( _rc != ERROR_SUCCESS )
    {
      return COMMON_BASE_ERROR;
    }
  } // for
  
  return COMMON_BASE_OK;

} // Common_SetRegistryData

//------------------------------------------------------------------------------
// PURPOSE : Gets a value from registry's HKEY_LOCAL_MACHINE
// 
//  PARAMS :
//      - INPUT/OUTPUT :
//          - pRegistryDataItem : Registry Value Data Item
// 
// RETURNS : 
//      - COMMON_BASE_ERROR
//      - COMMON_BASE_OK
//
//   NOTES :

COMMONBASE_API DWORD WINAPI  Common_GetValue (TCHAR                    * pValueName, 
                                              TYPE_REGISTRY_DATA_LIST * pRegistryData, 
                                              TYPE_REGISTRY_DATA_ITEM * pRegistryDataItem)
{
  WORD    _idx;
  TYPE_REGISTRY_DATA_ITEM *_p_items;

  memset(pRegistryDataItem, 0, sizeof(pRegistryDataItem));
  _p_items = (TYPE_REGISTRY_DATA_ITEM *) pRegistryData->p_items;  

  for ( _idx = 0; _idx < pRegistryData->num_items ; _idx++)
  {
    if ( !strcmp(_p_items[_idx].value_name, pValueName))
    {
      strncpy(pRegistryDataItem->key_name, _p_items[_idx].key_name, sizeof(_p_items[_idx].key_name));
      strncpy(pRegistryDataItem->value_name , _p_items[_idx].value_name, sizeof(_p_items[_idx].value_name));
      pRegistryDataItem->value_type = _p_items[_idx].value_type;
      strncpy(pRegistryDataItem->value_data, _p_items[_idx].value_data, sizeof(_p_items[_idx].value_data));
      pRegistryDataItem->value_length = _p_items[_idx].value_length;

      return COMMON_BASE_OK;
    }
  }

  return COMMON_BASE_ERROR;

} // Common_GetValue

//------------------------------------------------------------------------------
// PURPOSE : Gets a value from registry's.
// 
//  PARAMS :
//      - INPUT:
//          - pKeyRoot: Key root.
//          - pRegistryDataItem : Registry Value Data Item
//
///     - OUTPUT:
// 
// RETURNS : 
//      - COMMON_BASE_ERROR
//      - COMMON_BASE_OK
//
//   NOTES :

COMMONBASE_API DWORD WINAPI  Common_GetRegistryValue (TCHAR * pKeyRoot, TYPE_REGISTRY_DATA_ITEM * pRegistryDataItem)
{
  HKEY    _hk;
  DWORD   _rc;
  TCHAR    _ach_value_data [COMMON_MAX_REGISTRY_VALUE_DATA]; 
  DWORD    _cch_value_data; 
  DWORD    _pAux_dword;
  BYTE    _pAux_byte [COMMON_MAX_REGISTRY_VALUE_DATA]; 
  TCHAR    _key_name_complete [MAX_PATH];
  HKEY    _hkey;
  
  _cch_value_data = COMMON_MAX_REGISTRY_VALUE_DATA;
  memset(_key_name_complete, 0, sizeof(_key_name_complete));
  
  _tcscat(_key_name_complete, pKeyRoot);
  _tcscat(_key_name_complete, SEPARATOR_KEYS);
  _tcscat(_key_name_complete, pRegistryDataItem->key_name);

  if ( pRegistryDataItem->key_type  == COMMON_REG_KEY_TYPE_MACHINE )
  {
    _hkey = HKEY_LOCAL_MACHINE;
  }
  else
  {
    _hkey = HKEY_CURRENT_USER;
  }
    
  // Get the key's handle
  _rc = RegOpenKeyEx (_hkey, 
                      _key_name_complete, 
                      0, 
                      KEY_READ, 
                      &_hk);

  _cch_value_data = COMMON_MAX_REGISTRY_VALUE_DATA;
  memset(_ach_value_data, 0, sizeof(_ach_value_data));
  memset(&_pAux_dword, 0, sizeof(_pAux_dword));
  memset(_pAux_byte, 0, sizeof(_pAux_byte));

    switch ( pRegistryDataItem->value_type)
    {
      case REG_SZ:
        RegQueryValueEx(_hk, pRegistryDataItem->value_name , NULL, &pRegistryDataItem->value_type, (LPBYTE) _ach_value_data, &_cch_value_data );
        _stprintf(pRegistryDataItem->value_data, _T("%s"), _ach_value_data);
      break;

      case REG_DWORD:
        RegQueryValueEx(_hk, pRegistryDataItem->value_name, NULL, &pRegistryDataItem->value_type, (LPBYTE) &_pAux_dword, &_cch_value_data );
        _stprintf(pRegistryDataItem->value_data, _T("%i"), _pAux_dword);
      break;

      case REG_BINARY:
        RegQueryValueEx(_hk, pRegistryDataItem->value_name, NULL, &pRegistryDataItem->value_type, _pAux_byte, &_cch_value_data );
        _stprintf(pRegistryDataItem->value_data, _T("%s"), _pAux_byte);
      break;

      default :
      break;
    }

  RegCloseKey (_hk);

  return _rc;
} // Common_GetRegistryValue

//------------------------------------------------------------------------------
// PURPOSE : Gets a DWORD value from registry's HKEY_LOCAL_MACHINE
// 
//  PARAMS :
//      - INPUT :
//          - pKeyName : Key name 
//          - pSubKeyName : Subkey name 
//
//      - OUTPUT :
//          - pSubKeyValue : Subkey value
// 
// RETURNS : 
//      - COMMON_BASE_ERROR
//      - COMMON_BASE_ERROR
//      - COMMON_BASE_OK
//
//   NOTES :

COMMONBASE_API DWORD WINAPI Common_GetRegistryValueDWord (TCHAR * pKeyName,
                                                          TCHAR * pSubKeyName,
                                                          DWORD * pSubKeyValue)
{
  HKEY    _hk;
  DWORD   _rc;
  DWORD   _type;
  DWORD   _size;

  // Check parameters: key
  if ( pKeyName == NULL )
  {
    return COMMON_BASE_ERROR;
  }

  // Check parameters: subkey name and output parameter
  if ( pSubKeyName == NULL || pSubKeyValue == NULL )
  {
    return COMMON_BASE_ERROR;
  }

  if ( pSubKeyName [0] == 0x00 )
  {
    return COMMON_BASE_ERROR;
  }

  // Get the key's handle
  _rc = RegOpenKeyEx (HKEY_LOCAL_MACHINE, 
                      pKeyName, 
                      0, 
                      KEY_READ, 
                      &_hk);

  if ( _rc != ERROR_SUCCESS )
  {
    return COMMON_BASE_ERROR;
  }

  // Request DWORD value to registry
  _type = REG_DWORD;
  _size = sizeof (DWORD);

  _rc = RegQueryValueEx (_hk,           // Key
                         pSubKeyName,   // Subkey
                         NULL,
                         &_type,
                         (BYTE *) pSubKeyValue,
                         &_size);

  // Close the key only if we opened it
  RegCloseKey (_hk);

  if ( _rc != ERROR_SUCCESS )
  {
    return COMMON_BASE_ERROR;
  }

  return COMMON_BASE_OK;
} // Common_GetRegistryValueDWord

//------------------------------------------------------------------------------
// PURPOSE : Gets a binary value from registry's HKEY_LOCAL_MACHINE
// 
//  PARAMS :
//      - INPUT :
//          - pKeyName : Key name 
//          - pSubKeyName : Subkey name 
//          - pSubKeyValueLength : Subkey value length
//
//      - OUTPUT : 
//          - pSubKeyValue : Subkey value
// 
// RETURNS : 
//      - COMMON_BASE_ERROR
//      - COMMON_BASE_ERROR
//      - COMMON_BASE_OK
//
//   NOTES :

COMMONBASE_API DWORD WINAPI  Common_GetRegistryValueBin (TCHAR * pKeyName,
                                                         TCHAR * pSubKeyName,
                                                         BYTE  * pSubKeyValue,
                                                         DWORD * pSubKeyValueLength)
{
  HKEY    _hk;
  DWORD   _rc;
  DWORD   _type;

  // Check parameters: key
  if ( pKeyName == NULL )
  {
    return COMMON_BASE_ERROR;
  }

  // Check parameters: subkey name, output parameter and output parameter length
  if ( pSubKeyName == NULL || pSubKeyValue == NULL || pSubKeyValueLength == NULL )
  {
    return COMMON_BASE_ERROR;
  }

  if ( pSubKeyName [0] == 0x00 || * pSubKeyValueLength == 0 )
  {
    return COMMON_BASE_ERROR;
  }

  // Get the key's handle
  _rc = RegOpenKeyEx (HKEY_LOCAL_MACHINE, 
                      pKeyName, 
                      0, 
                      KEY_READ, 
                      &_hk);

  if ( _rc != ERROR_SUCCESS )
  {
    return COMMON_BASE_ERROR;
  }

  // Request BINARY value to remote registry
  _type = REG_BINARY;

  _rc = RegQueryValueEx (_hk,           // Key
                         pSubKeyName,   // Subkey
                         NULL,
                         &_type,
                         pSubKeyValue,
                         pSubKeyValueLength);

  // Close the key only if we opened it
  RegCloseKey (_hk);

  if ( _rc != ERROR_SUCCESS )
  {
    return COMMON_BASE_ERROR;
  }

  return COMMON_BASE_OK;

} // Common_GetRegistryValueBin

//------------------------------------------------------------------------------
// PURPOSE : Gets a STRING value from registry's HKEY_LOCAL_MACHINE
//
//  PARAMS :
//      - INPUT :
//          - pKeyName : Key name 
//          - pSubKeyName : Subkey name 
//          - pSubKeyValueLength : Subkey value length
//
//      - OUTPUT : 
//          - pSubKeyValue : Subkey value
// 
// RETURNS : 
//      - COMMON_BASE_ERROR
//      - COMMON_BASE_ERROR
//      - COMMON_BASE_OK
//
//   NOTES :

COMMONBASE_API DWORD WINAPI  Common_GetRegistryValueString (TCHAR    * pKeyName,
                                                            TCHAR    * pSubKeyName,
                                                            TCHAR    * pSubKeyValue,
                                                            DWORD    * pSubKeyValueLength)
{
  HKEY    _hk;
  DWORD   _rc;
  DWORD   _type;

  // Check parameters: key
  if ( pKeyName == NULL )
  {
    return COMMON_BASE_ERROR;
  }

  // Check parameters: subkey name, output parameter and output parameter length
  if ( pSubKeyName == NULL || pSubKeyValue == NULL || pSubKeyValueLength == NULL )
  {
    return COMMON_BASE_ERROR;
  }

  if ( pSubKeyName [0] == 0x00 || * pSubKeyValueLength == 0 )
  {
    return COMMON_BASE_ERROR;
  }

  // Get the key's handle
  _rc = RegOpenKeyEx (HKEY_LOCAL_MACHINE, 
                      pKeyName, 
                      0, 
                      KEY_READ, 
                      &_hk);

  if ( _rc != ERROR_SUCCESS )
  {
    return COMMON_BASE_ERROR;
  }

  // Request string value to remote registry
  _type = REG_SZ;
  memset (pSubKeyValue, 0, * pSubKeyValueLength);

  _rc = RegQueryValueEx (_hk,           // Key
                         pSubKeyName,   // Subkey
                         NULL,
                         &_type,
                         (BYTE *) pSubKeyValue,
                         pSubKeyValueLength);

  // Close the key only if we opened it
  RegCloseKey (_hk);

  if ( _rc != ERROR_SUCCESS )
  {
    return COMMON_BASE_ERROR;
  }

  return COMMON_BASE_OK;
} // Common_GetRegistryValueString 


//------------------------------------------------------------------------------
// PURPOSE : Saves a long value (DWORD) to the registry.
// 
//  PARAMS :
//      - INPUT :
//          - pKeyName : Key name 
//          - pSubKeyName : Subkey name 
//
//      - OUTPUT : 
//          - pSubKeyValue : Subkey value
// 
// RETURNS : 
//      - COMMON_BASE_ERROR
//      - COMMON_BASE_ERROR
//      - COMMON_BASE_OK
//
//   NOTES :

COMMONBASE_API DWORD WINAPI  Common_SaveRegistryValueDWord (TCHAR    * pKeyName,
                                                            TCHAR    * pSubKeyName,
                                                            DWORD    * pSubKeyValue)
{
  HKEY      _hk;
  DWORD     _rc;

  // Check parameters: key
  if ( pKeyName == NULL )
  {
    return COMMON_BASE_ERROR;
  }

  // Check parameters: subkey name and output parameter
  if ( pSubKeyName == NULL || pSubKeyValue == NULL )
  {
    return COMMON_BASE_ERROR;
  }

  if ( pSubKeyName [0] == 0x00 )
  {
    return COMMON_BASE_ERROR;
  }

  // Get the key's handle
  _rc = RegCreateKeyEx (HKEY_LOCAL_MACHINE, 
                        pKeyName, 
                        0, 
                        _T(""), 
                        REG_OPTION_NON_VOLATILE, 
                        KEY_WRITE, 
                        NULL, 
                        &_hk, 
                        NULL);

  if ( _rc != ERROR_SUCCESS )
  {
    return COMMON_BASE_ERROR;
  }


  _rc = RegSetValueEx (_hk, 
                       pSubKeyName, 
                       0, 
                       REG_DWORD, 
                       (BYTE *) pSubKeyValue,
                       sizeof (DWORD));

  // Close the key only if we opened it
  RegCloseKey (_hk);

  if ( _rc != ERROR_SUCCESS )
  {
    return COMMON_BASE_ERROR;
  }

  return COMMON_BASE_OK;
} // Common_SaveRegistryValueDWord

//------------------------------------------------------------------------------
// PURPOSE : Saves a STRING value to the registry.
// 
//  PARAMS :
//      - INPUT :
//          - pKeyName : Key name 
//          - pSubKeyName : Subkey name 
//
//      - OUTPUT : 
//          - pSubKeyValue : Subkey value
// 
// RETURNS : 
//      - COMMON_BASE_ERROR
//      - COMMON_BASE_ERROR
//      - COMMON_BASE_OK
//
//   NOTES :

COMMONBASE_API DWORD WINAPI  Common_SaveRegistryValueString (TCHAR   * pKeyName,
                                                             TCHAR   * pSubKeyName,
                                                             TCHAR   * pSubKeyValue)
{
  HKEY      _hk;
  DWORD     _rc;

  // Check parameters: key
  if ( pKeyName == NULL )
  {
    return COMMON_BASE_ERROR;
  }

  // Check parameters: subkey name, output parameter and output parameter length
  if ( pSubKeyName == NULL || pSubKeyValue == NULL )
  {
    return COMMON_BASE_ERROR;
  }

  if ( pSubKeyName [0] == 0x00 )
  {
    return COMMON_BASE_ERROR;
  }

  // Get the key's handle
  _rc = RegCreateKeyEx (HKEY_LOCAL_MACHINE, 
                        pKeyName, 
                        0, 
                        _T(""), 
                        REG_OPTION_NON_VOLATILE, 
                        KEY_WRITE, 
                        NULL, 
                        &_hk, 
                        NULL);

  if ( _rc != ERROR_SUCCESS )
  {
    return COMMON_BASE_ERROR;
  }

  _rc = RegSetValueEx (_hk, 
                       pSubKeyName, 
                       0, 
                       REG_SZ, 
                       (BYTE *) pSubKeyValue,
                       _tcslen (pSubKeyValue));
  
  // Close the key only if we opened it
  RegCloseKey (_hk);

  if ( _rc != ERROR_SUCCESS )
  {
    return COMMON_BASE_ERROR;
  }

  return COMMON_BASE_OK;

} // Common_SaveRegistryValueString 

//------------------------------------------------------------------------------
// PURPOSE : Saves a binary values (BYTE) to the registry.
// 
//  PARAMS :
//      - INPUT :
//          - pKeyName : Key name 
//          - pSubKeyName : Subkey name 
//          - pSubKeyValueLength : Subkey value length
//
//      - OUTPUT : 
//          - pSubKeyValue : Subkey value
// 
// RETURNS : 
//      - COMMON_BASE_ERROR
//      - COMMON_BASE_ERROR
//      - COMMON_BASE_OK
//
//   NOTES :

COMMONBASE_API DWORD WINAPI  Common_SaveRegistryValueBin (TCHAR * pKeyName,
                                                          TCHAR * pSubKeyName,
                                                          BYTE  * pSubKeyValue,
                                                          DWORD * pSubKeyValueLength)
{
  HKEY      _hk;
  DWORD     _rc;

  // Check parameters: key
  if ( pKeyName == NULL )
  {
    return COMMON_BASE_ERROR;
  }

  // Check parameters: subkey name and output parameter
  if ( pSubKeyName == NULL || pSubKeyValue == NULL )
  {
    return COMMON_BASE_ERROR;
  }

  if ( pSubKeyName [0] == 0x00 )
  {
    return COMMON_BASE_ERROR;
  }

  // Get the key's handle
  _rc = RegCreateKeyEx (HKEY_LOCAL_MACHINE, 
                        pKeyName, 
                        0, 
                        _T(""), 
                        REG_OPTION_NON_VOLATILE, 
                        KEY_WRITE, 
                        NULL, 
                        &_hk, 
                        NULL);

  if ( _rc != ERROR_SUCCESS )
  {
    return COMMON_BASE_ERROR;
  }

  _rc = RegSetValueEx (_hk, 
                       pSubKeyName, 
                       0, 
                       REG_BINARY, 
                       (BYTE *) pSubKeyValue,
                       *pSubKeyValueLength);

  // Close the key only if we opened it
  RegCloseKey (_hk);

  if ( _rc != ERROR_SUCCESS )
  {
    return COMMON_BASE_ERROR;
  }

  return COMMON_BASE_OK;
} // Common_SaveRegistryValueBin
