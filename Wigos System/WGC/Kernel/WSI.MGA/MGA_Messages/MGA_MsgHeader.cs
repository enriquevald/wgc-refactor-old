//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgHeader.cs
// 
//   DESCRIPTION: MGA Message Header
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 12-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAR-2007 AJQ    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

using WSI.Common;


namespace WSI.MGA
{
  /// <summary>
  /// MGA message types
  /// </summary>
  public enum MGA_MsgTypes
  {
    // Request                  // Reply
    MGA_MsgRequestService,      MGA_MsgRequestServiceReply
  , MGA_MsgEnrollServer,        MGA_MsgEnrollServerReply
  , MGA_MsgEnrollTerminal,      MGA_MsgEnrollTerminalReply
  , MGA_MsgGetParameters,       MGA_MsgGetParametersReply
  , MGA_MsgUnenrollTerminal,    MGA_MsgUnenrollTerminalReply
  , MGA_MsgUnenrollServer,      MGA_MsgUnenrollServerReply
  , MGA_MsgGetCardType,         MGA_MsgGetCardTypeReply
  , MGA_MsgLockCard,            MGA_MsgLockCardReply
  , MGA_MsgStartCardSession,    MGA_MsgStartCardSessionReply
  , MGA_MsgPlayConditions,      MGA_MsgPlayConditionsReply
  , MGA_MsgReportPlay,          MGA_MsgReportPlayReply
  , MGA_MsgAddCredit,           MGA_MsgAddCreditReply
  , MGA_MsgEndSession,          MGA_MsgEndSessionReply
  , MGA_MsgReportEvent,         MGA_MsgReportEventReply
  , MGA_MsgExecuteCommand,      MGA_MsgExecuteCommandReply
  , MGA_MsgReportMeters,        MGA_MsgReportMetersReply
  , MGA_MsgIntegrityCheck,      MGA_MsgIntegrityCheckReply
  , MGA_MsgKeepAlive,           MGA_MsgKeepAliveReply
  , MGA_MsgReportEventList,     MGA_MsgReportEventListReply
,
    MGA_MsgError
  , MGA_MsgUnknown
  }

  /// <summary>
  /// MGA response codes
  /// </summary>
  public enum MGA_ResponseCodes
  {
    MGA_RC_OK
  ,
    MGA_RC_ERROR
  ,
    MGA_RC_DATABASE_OFFLINE
  ,
    MGA_RC_SERVICE_NOT_AVAILABLE
  ,
    MGA_RC_SERVICE_NOT_FOUND
  ,
    MGA_RC_SERVER_NOT_AUTHORIZED
  ,
    MGA_RC_SERVER_SESSION_NOT_VALID
  ,
    MGA_RC_TERMINAL_NOT_AUTHORIZED
  ,
    MGA_RC_TERMINAL_SESSION_NOT_VALID
  ,
    MGA_RC_CARD_NOT_VALID
  ,
    MGA_RC_CARD_BLOCKED
  ,
    MGA_RC_CARD_EXPIRED
  ,
    MGA_RC_CARD_ALREADY_IN_USE
  ,
    MGA_RC_CARD_NOT_VALIDATED
  ,
    MGA_RC_CARD_BALANCE_MISMATCH
  ,
    MGA_RC_CARD_REMOTE_BALANCE_MISMATCH
  ,
    MGA_RC_CARD_SESSION_NOT_CLOSED
  ,
    MGA_RC_CARD_SESSION_NOT_VALID
  ,
    MGA_RC_INVALID_SESSION_ID
  ,
    MGA_RC_UNKNOWN_GAME_ID
  ,
    MGA_RC_INVALID_AMOUNT
  ,
    MGA_RC_EVENT_NOT_VALID
  ,
    MGA_RC_CRC_MISMATCH
  ,
    MGA_RC_NOT_ENOUGH_PLAYERS
  ,
    MGA_RC_PROVIDER_NOT_VALID
  ,
    MGA_RC_ERROR_UPDATING_PROVIDER

  ,
    MGA_RC_MSG_FORMAT_ERROR
  ,
    MGA_RC_INVALID_MSG_TYPE
  ,
    MGA_RC_INVALID_PROTOCOL_VERSION
  ,
    MGA_RC_INVALID_SEQUENCE_ID
  ,
    MGA_RC_INVALID_SERVER_ID
  ,
    MGA_RC_INVALID_TERMINAL_ID
  ,
    MGA_RC_INVALID_SERVER_SESSION_ID
  ,
    MGA_RC_INVALID_TERMINAL_SESSION_ID
  ,
    MGA_RC_INVALID_COMPRESSION_METHOD
  ,
    MGA_RC_INVALID_RESPONSE_CODE

  ,
    MGA_RC_UNKNOWN
  , MGA_RC_LAST
  }

  public enum MGA_EventTypesCodes
  {
    MGA_EV_DEVICE_STATUS = 1,
    MGA_EV_OPERATION = 2,

    MGA_EV_DOOR_OPENED = 101,
    MGA_EV_DOOR_CLOSED = 102,
    MGA_EV_NOTE_ACCEPTOR_JAM = 103,
    MGA_EV_NOTE_ACCEPTOR_FULL = 104,
    MGA_EV_NOTE_ACCEPTOR_ERROR = 105,
    MGA_EV_PRINTER_NO_PAPER = 106,
    MGA_EV_PRINTER_JAM = 107,
    MGA_EV_PRINTER_ERROR = 108,
    MGA_EV_POWER_FAILURE = 109,
    MGA_EV_LOST_CONNECTION = 110,
    MGA_EV_STARTUP = 111,
    MGA_EV_SHUTDOWN = 112,
    MGA_EV_CREDIT_LIMIT = 113,
    MGA_EV_DISPLAY_OFF = 114,
    MGA_EV_CARDREADER_OFF = 115,
  }

  public enum MGA_CardTypes
  {
    CARD_TYPE_UNKNOWN
  , CARD_TYPE_PLAYER
  }


  public enum MGA_CashTypes
  {
    CASH_TYPE_CASH_IN = 1
  , CASH_TYPE_CASH_OUT = 2
  }


  public enum MGA_MoneyTypes
  {
    MONEY_TYPE_NOTE = 1
  , MONEY_TYPE_COIN = 2
  }

  public enum MGA_CommandTypes
  {
    MGA_CMD_SHUTDOWN
  ,
    MGA_CMD_RESTART
  ,
    MGA_CMD_RELOAD_PARAMETERS
  ,
    MGA_CMD_RESET_METERS
  ,
    MGA_CMD_REPORT_METERS
  ,
    MGA_CMD_LOCK
  ,
    MGA_CMD_DISCONNECT
  , MGA_CMD_INTEGRITY_CHECK
  }

  /// <summary>
  /// MGA compression methods
  /// </summary>
  public enum MGA_CompressionMethod
  {
    NONE
  }

  /// <summary>
  /// Device Codes
  /// </summary>
  public enum DeviceCodes
  {
    MGA_DEVICE_CODE_NO_DEVICE = 0
  ,
    MGA_DEVICE_CODE_PRINTER = 1
  ,
    MGA_DEVICE_CODE_NOTE_ACCEPTOR = 2
  ,
    MGA_DEVICE_CODE_SMARTCARD_READER = 3
  ,
    MGA_DEVICE_CODE_CUSTOMER_DISPLAY = 4
  ,
    MGA_DEVICE_CODE_COIN_ACCEPTOR = 5
  ,
    MGA_DEVICE_CODE_UPPER_DISPLAY = 6
  ,
    MGA_DEVICE_CODE_BAR_CODE_READER = 7
  ,
    MGA_DEVICE_CODE_UPS = 9
  , MGA_DEVICE_CODE_MODULE_IO = 15
  }

  /// <summary>
  /// Device Status
  /// </summary>
  static public class DeviceStatus
  {
    static public readonly Int32 MGA_DEVICE_STATUS_UNKNOWN = 0;
    static public readonly Int32 MGA_DEVICE_STATUS_OK = 1;
    static public readonly Int32 MGA_DEVICE_STATUS_ERROR = 2;
    static public readonly Int32 MGA_DEVICE_STATUS_NOT_INSTALLED = 100;
    static public readonly Int32 MGA_DEVICE_STATUS_PRINTER_OFF = 3;
    static public readonly Int32 MGA_DEVICE_STATUS_PRINTER_READY = 4;
    static public readonly Int32 MGA_DEVICE_STATUS_PRINTER_NOT_READY = 5;
    static public readonly Int32 MGA_DEVICE_STATUS_PRINTER_PAPER_OUT = 6;
    static public readonly Int32 MGA_DEVICE_STATUS_PRINTER_PAPER_LOW = 7;
    static public readonly Int32 MGA_DEVICE_STATUS_PRINTER_OFFLINE = 8;
    static public readonly Int32 MGA_DEVICE_STATUS_NOTE_ACCEPTOR_JAM = 3;
    static public readonly Int32 MGA_DEVICE_STATUS_NOTE_ACCEPTOR_SN_ERROR = 4;
    static public readonly Int32 MGA_DEVICE_STATUS_NOTE_ACCEPTOR_STACKER_FULL = 5;
    static public readonly Int32 MGA_DEVICE_STATUS_UPS_NO_AC = 3;
    static public readonly Int32 MGA_DEVICE_STATUS_UPS_BATTERY_LOW = 4;
    static public readonly Int32 MGA_DEVICE_STATUS_UPS_OVERLOAD = 5;
    static public readonly Int32 MGA_DEVICE_STATUS_UPS_OFF = 6;
    static public readonly Int32 MGA_DEVICE_STATUS_UPS_BATTERY_FAIL = 7;
    static public readonly Int32 MGA_DEVICE_STATUS_SMARTCARD_REMOVED = 3;
    static public readonly Int32 MGA_DEVICE_STATUS_SMARTCARD_INSERTED = 4;
    static public readonly Int32 MGA_DEVICE_STATUS_COIN_ACCEPTOR_JAM = 3;
    static public readonly Int32 MGA_DEVICE_STATUS_COIN_ACCEPTOR_SN_ERROR = 4;
    static public readonly Int32 MGA_DEVICE_STATUS_COIN_ACCEPTOR_STACKER_FULL = 5;
  }

  /// <summary>
  /// Priority
  /// </summary>
  public enum DevicePriority
  {
    MGA_DEVICE_PRIORITY_OK = 0
  ,
    MGA_DEVICE_PRIORITY_WARNING = 1
  , MGA_DEVICE_PRIORITY_ERROR = 2
  }

  /// <summary>
  /// Operations
  /// </summary>
  public enum OperationCodes
  {
    MGA_OPERATION_CODE_NO_OPERATION = 0
  ,
    MGA_OPERATION_CODE_START = 1
  ,
    MGA_OPERATION_CODE_SHUTDOWN = 2
  ,
    MGA_OPERATION_CODE_RESTART = 3
  ,
    MGA_OPERATION_CODE_TUNE_SCREEN = 4
  ,
    MGA_OPERATION_CODE_LAUNCH_EXPLORER = 5
  ,
    MGA_OPERATION_CODE_ASSIGN_KIOSK = 6
  ,
    MGA_OPERATION_CODE_DEASSIGN_KIOSK = 7
  ,
    MGA_OPERATION_CODE_UNLOCK_DOOR = 8
  ,
    MGA_OPERATION_CODE_DISPLAY_SETTINGS = 11
  ,
    MGA_OPERATION_CODE_DOOR_OPENED = 12
  ,
    MGA_OPERATION_CODE_DOOR_CLOSED = 13
  ,
    MGA_OPERATION_CODE_BLOCK_KIOSK = 14
  ,
    MGA_OPERATION_CODE_UNBLOCK_KIOSK = 15
  ,
    MGA_OPERATION_CODE_JACKPOT_WON = 16
  ,
    MGA_OPERATION_CODE_KIOSK_BLOCKED_HIGH_PRIZE = 17
  ,
    MGA_OPERATION_CODE_CALL_ATTENDANT = 18
  ,
    MGA_OPERATION_CODE_TEST_MODE_ENTER = 19
  , MGA_OPERATION_CODE_TEST_MODE_LEAVE = 20

  }

  ///// <summary>
  ///// The MGA message header
  ///// </summary>
  //public class MGA_MsgHeader
  //{
  //  #region CONSTANTS

  //  public const string PROTOCOL_VERSION = "1.0.0.4";
  //  const string COMPRESSION_METHOD = "NONE";

  //  #endregion // CONSTANTS

  //  #region MEMBERS

  //  private string ProtocolVersion;
  //  public string CompressionMethod;
  //  private DateTime SystemTime;
  //  private Int32 TimeZone;
  //  public string ServerId;
  //  public string TerminalId;
  //  public Int64 ServerSessionId;
  //  public Int64 TerminalSessionId;
  //  public MGA_MsgTypes MsgType;
  //  public Int64 SequenceId;
  //  public int MsgContentSize;
  //  public MGA_ResponseCodes ResponseCode;
  //  public string ResponseCodeText;
  //  //////public static Int64 SequenceIdResponse = 0;

  //  #endregion // MEMBERS

  //  #region PUBLIC

  //  /// <summary>
  //  ///   Gets the Protocol Version
  //  /// </summary>
  //  public string GetProtocolVersion
  //  {
  //    get { return ProtocolVersion; }
  //  }

  //  /// <summary>
  //  ///   Gets the Compression Method
  //  /// </summary>
  //  MGA_CompressionMethod GetCompressionMethod
  //  {
  //    get { return MGA_CompressionMethod.NONE; }
  //  }

  //  /// <summary>
  //  /// Gets the System Time
  //  /// </summary>
  //  public DateTime GetSystemTime
  //  {
  //    get { return SystemTime; }
  //  }

  //  /// <summary>
  //  /// Get time Zone
  //  /// </summary>
  //  public Int32 GetTimeZone
  //  {
  //    get { return TimeZone; }
  //  }

  //  /// <summary>
  //  /// Creates the reply header.
  //  /// The following fields are copied from the request:
  //  /// - ServerId
  //  /// - TerminalId
  //  /// - ServerSessionId
  //  /// - TerminalSessionId
  //  /// - SequenceId
  //  /// </summary>
  //  /// <param name="ResponseCode">Response Code</param>
  //  /// <returns>The reply header</returns>
  //  public MGA_MsgHeader ReplyHeader(MGA_ResponseCodes ResponseCode)
  //  {
  //    MGA_MsgHeader reply;

  //    reply = new MGA_MsgHeader();
  //    reply.ServerId = ServerId;
  //    reply.TerminalId = TerminalId;
  //    reply.ServerSessionId = ServerSessionId;
  //    reply.TerminalSessionId = TerminalSessionId;
  //    reply.MsgType = MsgType;
  //    reply.MsgType++;
  //    reply.SequenceId = SequenceId;
  //    reply.ResponseCode = ResponseCode;
  //    reply.ResponseCodeText = "";

  //    return reply;
  //  }

  //  public void PrepareReply(MGA_MsgHeader RequestHeader)
  //  {
  //    ServerId = RequestHeader.ServerId;
  //    TerminalId = RequestHeader.TerminalId;
  //    ServerSessionId = RequestHeader.ServerSessionId;
  //    TerminalSessionId = RequestHeader.TerminalSessionId;
  //    SequenceId = RequestHeader.SequenceId;

  //    if (MsgType == MGA_MsgTypes.MGA_MsgUnknown)
  //    {
  //      MsgType = RequestHeader.MsgType + 1;
  //    }
  //  }


  //  /// <summary>
  //  /// Extracts an enum object value from a 
  //  /// Enum Object giving the string name of 
  //  /// the value and getting the index.
  //  /// </summary>
  //  /// <param name="ResponseCode">Response Code</param>
  //  /// <returns>The reply header</returns>
  //  public static Object Decode(String str, Object defaultValue)
  //  {
  //    foreach (String s in System.Enum.GetNames(defaultValue.GetType()))
  //    {
  //      if (s.Equals(str))
  //      {
  //        return System.Enum.Parse(defaultValue.GetType(), s);
  //      }
  //    }
  //    return defaultValue;
  //  }

  //  public void LoadXml(XmlReader reader)
  //  {
  //    ProtocolVersion = XML.GetValue(reader, "ProtocolVersion");
  //    CompressionMethod = XML.GetValue(reader, "CompressionMethod");

  //    string time;
  //    string time_zone_hours;
  //    string time_zone_minutes;
  //    Int32 minutes;

  //    time = XML.GetValue(reader, "SystemTime");

  //    DateTime.TryParse(time, out SystemTime);

  //    time_zone_hours = time.Substring(time.Length - 6, 3);
  //    time_zone_minutes = time.Substring(time.Length - 2, 2);

  //    Int32.TryParse(time_zone_hours, out TimeZone);
  //    Int32.TryParse(time_zone_minutes, out minutes);
  //    TimeZone *= 60;
  //    if (TimeZone >= 0)
  //    {
  //      TimeZone += minutes;
  //    }
  //    else
  //    {
  //      TimeZone -= minutes;
  //    }

  //    ServerId = XML.GetValue(reader, "ServerId");
  //    TerminalId = XML.GetValue(reader, "TerminalId");
  //    long.TryParse(XML.GetValue(reader, "ServerSessionId"), out ServerSessionId);
  //    long.TryParse(XML.GetValue(reader, "TerminalSessionId"), out TerminalSessionId);
  //    MsgType = (MGA_MsgTypes)Decode(XML.GetValue(reader, "MsgType"), MGA_MsgTypes.MGA_MsgUnknown);
  //    long.TryParse(XML.GetValue(reader, "SequenceId"), out SequenceId);
  //    int.TryParse(XML.GetValue(reader, "MsgContentSize"), out MsgContentSize);
  //    ResponseCode = (MGA_ResponseCodes)Decode(XML.GetValue(reader, "ResponseCode"), MGA_ResponseCodes.MGA_RC_UNKNOWN);
  //    ResponseCodeText = XML.GetValue(reader, "ResponseCodeText");
  //  }


    //public String ToXml()
    //{

    //  DateTime now;
    //  string str;

    //  string strtemp;
    //  strtemp = "";

    //  now = DateTime.Now;
  //  str = XML.XmlDateTimeString(now);

    //  strtemp += "<MGA_MsgHeader>";

    //  strtemp += "<ProtocolVersion>";
    //  strtemp += ProtocolVersion;
    //  strtemp += "</ProtocolVersion>";

    //  strtemp += "<CompressionMethod>";
    //  strtemp += CompressionMethod;
    //  strtemp += "</CompressionMethod>";

    //  strtemp += "<SystemTime>";
    //  strtemp += str;
    //  strtemp += "</SystemTime>";

    //  strtemp += "<ServerId>";
    //  strtemp += ServerId;
    //  strtemp += "</ServerId>";

    //  strtemp += "<TerminalId>";
    //  strtemp += TerminalId;
    //  strtemp += "</TerminalId>";

    //  strtemp += "<ServerSessionId>";
    //  strtemp += ServerSessionId.ToString();
    //  strtemp += "</ServerSessionId>";

    //  strtemp += "<TerminalSessionId>";
    //  strtemp += TerminalSessionId.ToString();
    //  strtemp += "</TerminalSessionId>";

    //  strtemp += "<MsgType>";
    //  strtemp += MsgType.ToString();
    //  strtemp += "</MsgType>";

    //  strtemp += "<SequenceId>";
    //  strtemp += SequenceId.ToString();
    //  strtemp += "</SequenceId>";

    //  strtemp += "<MsgContentSize>";
    //  strtemp += MsgContentSize.ToString();
    //  strtemp += "</MsgContentSize>";

    //  strtemp += "<ResponseCode>";
    //  strtemp += ResponseCode.ToString();
    //  strtemp += "</ResponseCode>";

    //  strtemp += "<ResponseCodeText>";
    //  strtemp += ResponseCodeText;
    //  strtemp += "</ResponseCodeText>";

    //  strtemp += "</MGA_MsgHeader>";


    //  return strtemp;
    //}
    ///// <summary>
    ///// Adds the header to the given Xml MGA message
    ///// </summary>
    ///// <param name="XmlMGAMessage">The message</param>
    //public void ToXml(XmlDocument XmlMGAMessage)
    //{
    //  XmlNode root;
    //  XmlNode header;
    //  XmlNode old_header;
    //  DateTime now;
    //  string str;

    //  now = DateTime.Now;
  //  str = XML.XmlDateTimeString(now);

    //  root = XmlMGAMessage.SelectSingleNode("MGA_Message");
    //  header = XmlMGAMessage.CreateNode(XmlNodeType.Element, "", "MGA_MsgHeader", "");

    //  XML.AppendChild(header, "ProtocolVersion", ProtocolVersion);
    //  XML.AppendChild(header, "CompressionMethod", CompressionMethod);
    //  XML.AppendChild(header, "SystemTime", str);
    //  XML.AppendChild(header, "ServerId", ServerId);
    //  XML.AppendChild(header, "TerminalId", TerminalId);
    //  XML.AppendChild(header, "ServerSessionId", ServerSessionId.ToString());
    //  XML.AppendChild(header, "TerminalSessionId", TerminalSessionId.ToString());
    //  XML.AppendChild(header, "MsgType", MsgType.ToString());
    //  XML.AppendChild(header, "SequenceId", SequenceId.ToString());
    //  XML.AppendChild(header, "MsgContentSize", MsgContentSize.ToString());
    //  XML.AppendChild(header, "ResponseCode", ResponseCode.ToString());
    //  XML.AppendChild(header, "ResponseCodeText", ResponseCodeText);

    //  old_header = root.SelectSingleNode("MGA_MsgHeader");
    //  if (old_header != null)
    //  {
    //    root.RemoveChild(old_header);
    //  }

    //  if (root.FirstChild != null)
    //  {
    //    root.InsertBefore(header, root.FirstChild);
    //  }
    //  else
    //  {
    //    root.AppendChild(header);
    //  }
    //}

    ///// <summary>
    ///// Creates a header
    ///// </summary>
    //public MGA_MsgHeader()
    //{
    //  Init();
    //}

    //#endregion // PUBLIC

    //#region PRIVATE

    ///// <summary>
    ///// Initializes the header
    ///// </summary>
    //private void Init()
    //{
    //  ProtocolVersion = PROTOCOL_VERSION;
    //  CompressionMethod = COMPRESSION_METHOD;
    //  SystemTime = System.DateTime.Now;
    //  ServerId = "";
    //  TerminalId = "";
    //  ServerSessionId = 0;
    //  TerminalSessionId = 0;
    //  MsgType = MGA_MsgTypes.MGA_MsgUnknown;
    //  SequenceId = 0;
    //  MsgContentSize = 0;
    //  ResponseCode = MGA_ResponseCodes.MGA_RC_OK;
    //  ResponseCodeText = "";
    //}


    //#endregion // PRIVATE
  //}
}
