//------------------------------------------------------------------------------
// Copyright © 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgBeginSessionWithCard.cs
// 
//   DESCRIPTION: MGA_MsgBeginSessionWithCard class
// 
//        AUTHOR: Daniel Rodríguez Gil
// 
// CREATION DATE: 21-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2009 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.MGA
{
  public class MGA_MsgBeginSessionWithCard:IXml
  {
    public String Track;
    public Int64 InstanceID;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  This message is sent to the 3PGS Device Service to begin a session with a player card. 
    //  The EGM should pass the card string as is without stripping any part of the card string.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES :
    //
    //    <BeginSessionWithCard> 
    //      <InstanceID type="int">51462</InstanceID> 
    //      <CardString type="string">"3829990005550100"</CardString> 
    //    </BeginSessionWithCard>

    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<BeginSessionWithCard><InstanceID type=\"int\">");
      str_builder.Append(InstanceID);
      str_builder.Append("</InstanceID><CardString type=\"string\">");
      str_builder.Append(Track);
      str_builder.Append("</CardString></BeginSessionWithCard>");
      
      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : This function should not be called from our side.
    //
    public void LoadXml(XmlReader Xmlreader)
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag BeginSessionWithCard Card element missing");
    }
  }

  public class MGA_MsgBeginSessionResponse : IXml
  {
    public int ResponseCode;
    public string ResponseCodeText;
    public int SessionID;
    public int SessionCashBalance;
    public int SessionCouponBalance;
    public Guid SecurityTransactionGUID;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES : This function should not be called from our side.
    //
    public String ToXml()
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag BeginSessionResponse not found in content area");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  This message is sent in response to the BeginSessionWithCard message. 
    //  The session balance for a card account is updated with every transaction. 
    //  If the connection goes off line and is restarted, the card account session 
    //  balance would be the current session balance. The EGM should take the Cash and Coupon 
    //  balance values and add them together to present as the total credits (cash) available to the player.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : This function should not be called from our side.
    //
    //    0   - SRC_OK
    //    14  - SRC_InvalidInstanceID
    //    73  - SRC_SessionInProgress
    //    90  - SRC_NotReadyToPlay
    //    91  - SRC_ServerError
    //    108 - SRC_UnknownCardString
    //    145 - SRC_UnknownAccount
    //    522 - SRC_AccountLocked


    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "BeginSessionResponse")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag BeginSessionResponse not found in content area");
      }
      else
      {
        ResponseCode = int.Parse(XML.GetValue(reader, "ResponseCode"));
        ResponseCodeText = XML.GetValue(reader, "ResponseCodeText");
        SessionID = int.Parse(XML.GetValue(reader, "SessionID"));
        SessionCashBalance = int.Parse(XML.GetValue(reader, "SessionCashBalance"));
        SessionCouponBalance = int.Parse(XML.GetValue(reader, "SessionCouponBalance"));
        SecurityTransactionGUID = new Guid(XML.GetValue(reader, "SecurityTransactionGUID"));
      }
    }
  }
}
