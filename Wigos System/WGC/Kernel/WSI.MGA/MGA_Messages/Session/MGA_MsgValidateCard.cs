//------------------------------------------------------------------------------
// Copyright © 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgValidateCard.cs
// 
//   DESCRIPTION: MGA_MsgValidateCard class
// 
//        AUTHOR: Daniel Rodríguez Gil
// 
// CREATION DATE: 21-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2009 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.MGA
{
  public class CardInformation
  {
    public string CardString;
    public int Track;
    public string CardType;
  }

  public class MGA_MsgValidateCard:IXml
  {
    public String Track;
    public Int64 InstanceID;
    public ArrayList CardInfo = new ArrayList();//Array of CardInformation

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES : The CardType is always empty
    //
    //  <ValidateCard> 
    //    <InstanceID type="int">15652</InstanceID>
    //    <CardInformations Count="2">
    //      <elem>
    //        <CardString type="string">XXXXXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX</CardString>
    //        <Track type="int">X</Track>
    //        <CardType type="string"></CardType>
    //      </elem>
    //      ...
    //    <CardInformations>
    //  </ValidateCard>

    public String ToXml()
    {
      StringBuilder str_builder;
      int idx_cardinfo;

      str_builder = new StringBuilder();
      str_builder.Append("<ValidateCard><InstanceID type=\"int\">");
      str_builder.Append(InstanceID);
      str_builder.Append("</InstanceID><CardInformations Count=" + CardInfo.Count + ">");
      for (idx_cardinfo = 0; idx_cardinfo < CardInfo.Count; idx_cardinfo++)
      {
        CardInformation card_info = new CardInformation();
        card_info = (CardInformation)CardInfo[idx_cardinfo];
        str_builder.Append("<elem><CardString type=\"string\">");
        str_builder.Append(card_info.CardString);
        str_builder.Append("</CardString><Track type=\"int\"");
        str_builder.Append(card_info.Track);
        str_builder.Append("</Track><CardType type=\"string\"");
        str_builder.Append(card_info.CardType);
        str_builder.Append("</CardType></elem>");
      }
      str_builder.Append("</CardInformations></ValidateCard>");
      
      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : This function should not be called from our side.
    //
    public void LoadXml(XmlReader Xmlreader)
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag ValidateCard Card element missing");
    }
  }

  public class MGA_MsgValidateCardResponse : IXml
  {
    public ArrayList CardInfo = new ArrayList(); //Array of CardInformation
    public int ResponseCode;
    public string ResponseCodeText;


    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES : This function should not be called from our side.
    //
    public String ToXml()
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag ValidateCardResponse not found in content area");
    }


    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT : Xml reader, contaning
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : 
    //  <ValidateCardResponse> 
    //    <CardInformations Count="X"> 
    //      <elem> 
    //        <CardString type="string">XXXXXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX</CardString> 
    //        <Track type="int">X</Track> 
    //        <CardType type="string">XXXXX</CardType> 
    //        <ResponseCode type="int">X</ResponseCode> 
    //        <ResponseCodeText type="string">XXXXX</ResponseCodeText> 
    //      </elem> 
    //    </CardInformations> 
    //  </ValidateCardResponse>
    //  Possible Responses
    //   0    - SRC_OK
    //   14   - SRC_InvalidInstanceID
    //   91   - SRC_ServerError
    //   108  - SRC_UnknownCardString

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "ValidateCardResponse")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag ValidateCardResponse not found in content area");
      }
      else
      {
        while (((reader.Name != "CardInformations") || (reader.NodeType != XmlNodeType.EndElement)) && !reader.EOF)
        {
          CardInformation card_info = new CardInformation();

          card_info.CardString  = XML.GetValue(reader, "CardString");
          card_info.Track  = int.Parse(XML.GetValue(reader, "Track"));
          card_info.CardType = XML.GetValue(reader, "CardType");
          CardInfo.Add(card_info);
          ResponseCode = int.Parse(XML.GetValue(reader, "ResponseCode"));
          ResponseCodeText = XML.GetValue(reader, "ResponseCodeText");
          reader.Read(); //After this "read" the reader will be at the end of SessionCouponBalance
          reader.Read(); //After this "read" the reader will be at the end of elem
          reader.Read(); //After this "read" the reader will be at the end of CardInformations or the beginning of new a elem
        }
      }
    }
  }
}
