//------------------------------------------------------------------------------
// Copyright © 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgRecordPlay.cs
// 
//   DESCRIPTION: MGA_MsgRecordPlay class
// 
//        AUTHOR: Daniel Rodríguez Gil
// 
// CREATION DATE: 21-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2009 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.MGA
{

  public class MGA_MsgRecordPlay:IXml
  {


















    public int InstanceID;                  
    public int SessionID;                   
    public Int64 SessionCashBalance;        
    public Int64 SessionCouponBalance;      
    public int GameUPCNumber;               
    public int GameIndex;                   
    public int NumberOfCredits;             
    public int Denomination;                
    public double  PayoutPercentage;        
    public string GameTheme;                
    public int LocalTransactionID;          
    public Guid  TransactionSecurityGUID;   
    public Int64 SalesAmount;               
    public Int64 PrizeAmount;               
    public bool ProgressiveJackpot;         

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  This message is sent to the 3PGS Device Service to request recording of game sales 
    //  and prize award in a single round trip. All account balances are maintained by the back-office. 
    //  The EGM MUST NOT try and calculate the balance after each play. 
    //  The response message will contain the new balance. If the EGM reports a cash or coupon 
    //  balance that does not match the BeginSessionResponse (in the case of the first play) or 
    //  a previous RecordPlayResonse, it will result in a SessionBalanceMismatch. This is fatal error and 
    //  the EGM MUST lock for an attendant.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES :
    //
    //  <RecordPlay> 
    //    <InstanceID type="int">10254</InstanceID> 
    //    <SessionID type="int">20652</SessionID> 
    //    <SessionCashBalance type="int64">10000</SessionCashBalance> 
    //    <SessionCouponBalance type="int64">0</SessionCouponBalance> 
    //    <GameUPCNumber type="int">1011025</GameUPCNumber> 
    //    <GameIndex type="int">0</GameIndex> 
    //    <NumberOfCredits type="int">1</NumberOfCredits> 
    //    <Denomination type="int">25</Denomination> 
    //    <PayoutPercentage type="double">96.8</PayoutPercentage> 
    //    <GameTheme type=" string">Meltdown </GameTheme> 
    //    <LocalTransactionID type="int">10</LocalTransactionID> 
    //    <TransactionSecurityGUID type="guid16">1FCF1722-3C27-4284-AC8A-9E3E2957F168</TransactionSecurityGUID> 
    //    <SalesAmount type="int64">25</SalesAmount> 
    //    <PrizeAmount type="int64">0</PrizeAmount> 
    //    <ProgressiveJackpot type="bool">true</ProgressiveJackpot> 
    //  </RecordPlay>
    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<RecordPlay><InstanceID type=\"int\">");
      str_builder.Append(InstanceID);
      str_builder.Append("</InstanceID><SessionID type=\"int\">");
      str_builder.Append(SessionID);
      str_builder.Append("</SessionID><SessionCashBalance type=\"Int64\">");
      str_builder.Append(SessionCashBalance);
      str_builder.Append("</SessionCashBalance><SessionCouponBalance type=\"Int64\">");
      str_builder.Append(SessionCouponBalance);
      str_builder.Append("</SessionCouponBalance><GameUPCNumber type=\"int\">");
      str_builder.Append(GameUPCNumber);
      str_builder.Append("</GameUPCNumber><GameIndex type=\"int\">");
      str_builder.Append(GameIndex);
      str_builder.Append("</GameIndex><NumberOfCredits type=\"int\">");
      str_builder.Append(NumberOfCredits);
      str_builder.Append("</NumberOfCredits><Denomination type=\"int\">");
      str_builder.Append(Denomination);
      str_builder.Append("</Denomination><PayoutPercentage type=\"double\">");
      str_builder.Append(PayoutPercentage);
      str_builder.Append("</PayoutPercentage><GameTheme type=\"string\">");
      str_builder.Append(GameTheme);
      str_builder.Append("</GameTheme><LocalTransactionID type=\"int\">");
      str_builder.Append(LocalTransactionID);
      str_builder.Append("</LocalTransactionID><TransactionSecurityGUID type=\"guid16\">");
      str_builder.Append(TransactionSecurityGUID);
      str_builder.Append("</TransactionSecurityGUID><SalesAmount type=\"Int64\">");
      str_builder.Append(SalesAmount);
      str_builder.Append("</SalesAmount><PrizeAmount type=\"Int64\">");
      str_builder.Append(PrizeAmount);
      str_builder.Append("</PrizeAmount><ProgressiveJackpot type=\"bool\">");
      if (ProgressiveJackpot)
      {
        str_builder.Append("true");
      }
      else
      {
        str_builder.Append("false");
      }
      str_builder.Append("</ProgressiveJackpot></RecordPlay>");
      
      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : This function should not be called from our side.
    //
    public void LoadXml(XmlReader Xmlreader)
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RecordPlay Card element missing");
    }
  }

  public class MGA_MsgRecordPlayResponse : IXml
  {
    public int ResponseCode;
    public string ResponseCodeText;
    public Int64 SessionCashBalance;
    public Int64 SessionCouponBalance;
    public Int64 ServerTransactionID;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES : This function should not be called from our side.
    //
    public String ToXml()
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RecordPlayResponse not found in content area");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  This message is sent in response to the RecordPlay messages.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : 
    //
    //  <RecordPlayResponse> 
    //    <ResponseCode type="int">0</ResponseCode> 
    //    <ResponseCodeText type="string">SRC_OK</ResponseCodeText> 
    //    <SessionCashBalance type="int64">12000</SessionCashBalance> 
    //    <SessionCouponBalance type="int64">0</SessionCouponBalance> 
    //    <ServerTransactionID type="int64">19287653</ServerTransactionID> 
    //  </RecordPlayResponse>
    //
    //  Response Codes:
    //  0   - SRC_OK
    //  4   - SRC_InvalidAmount
    //  14  - SRC_InvalidInstanceID
    //  15  - SRC_InvalidLocalTransactionID
    //  21  - SRC_InvalidDenomination
    //  26  - SRC_InvalidSessionID
    //  74  - SRC_SessionBalanceMismatch
    //  89  - SRC_NoGameRegistered
    //  90  - SRC_NotReadyToPlay
    //  91  - SRC_ServerError
    //  104 - SRC_InsufficientFunds
    //  155 - SRC_InvalidPayoutPercentage
    //  510 - SRC_InvalidGameTheme
    //  511 - SRC_SessionNoSessionInProgress
    //  519 - SRC_GameInvalidNumberOfCredits
    //  522 - SRC_AccountLocked
    //  523 - SRC_GameInvalidGameUPCNumber
    //
    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "RecordPlayResponse")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RecordPlayResponse not found in content area");
      }
      else
      {
        ResponseCode = int.Parse(XML.GetValue(reader, "SessionCashBalance"));
        ResponseCodeText = XML.GetValue(reader, "SessionCouponBalance");
        SessionCashBalance = Int64.Parse(XML.GetValue(reader, "SessionCashBalance"));
        SessionCouponBalance = Int64.Parse(XML.GetValue(reader, "SessionCouponBalance"));
        ServerTransactionID = Int64.Parse(XML.GetValue(reader, "ServerTransactionID"));
      }
    }
  }
}
