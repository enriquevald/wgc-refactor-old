//------------------------------------------------------------------------------
// Copyright © 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgEndSession.cs
// 
//   DESCRIPTION: MGA_MsgEndSession class
// 
//        AUTHOR: Daniel Rodríguez Gil
// 
// CREATION DATE: 21-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2009 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.MGA
{

  public class MGA_MsgEndSession : IXml
  {
    public int InstanceID;
    public int SessionID;
    public int LocalTransactionID;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  This message is sent to the 3PGS Device Service to request the termination of an open session.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES :
    //
    //  <EndSession> 
    //    <InstanceID type="int">10254</InstanceID> 
    //    <SessionID type="int">42523</SessionID> 
    //    <LocalTransactionID type="int">0</LocalTransactionID> 
    //  </EndSession>
    //

    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<EndSession><InstanceID type=\"int\">");
      str_builder.Append(InstanceID);
      str_builder.Append("</InstanceID><SessionID type=\"int\">");
      str_builder.Append(SessionID);
      str_builder.Append("</SessionID><LocalTransactionID type=\"int\">");
      str_builder.Append(LocalTransactionID);
      str_builder.Append("</LocalTransactionID></EndSession>");

      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : This function should not be called from our side.
    //
    public void LoadXml(XmlReader Xmlreader)
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag EndSession Card element missing");
    }
  }

  public class MGA_MsgEndSessionResponse : IXml
  {
    public int ResponseCode;
    public string ResponseCodeText;


    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES : This function should not be called from our side.
    //
    public String ToXml()
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag EndSessionResponse not found in content area");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  This message is sent in response to the EndSession message.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : 
    //
    //  <EndSessionResponse> 
    //    <ResponseCode type="int">0</ResponseCode> 
    //    <ResponseCodeText type="string">SRC_OK</ResponseCodeText> 
    //  </EndSessionResponse>
    //
    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "EndSessionResponse")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag EndSessionResponse not found in content area");
      }
      else
      {
        ResponseCode = int.Parse(XML.GetValue(reader, "ResponseCode"));
        ResponseCodeText = XML.GetValue(reader, "ResponseCodeText");
      }
    }
  }
}
