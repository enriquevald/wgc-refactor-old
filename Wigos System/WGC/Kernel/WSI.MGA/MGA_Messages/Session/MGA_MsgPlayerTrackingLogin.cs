//------------------------------------------------------------------------------
// Copyright © 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgPlayerTrackingLogin.cs
// 
//   DESCRIPTION: MGA_MsgPlayerTrackingLogin class
// 
//        AUTHOR: Daniel Rodríguez Gil
// 
// CREATION DATE: 21-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2009 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.MGA
{
  public class MGA_MsgPlayerTrackingLogin : IXml
  {
    public Int64 InstanceID;
    public string PlayerTrackingString;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  Notifies the back-office a player tracking card has been inserted into an EGM. 
    //  This message must be called to instruct the back-office that the customer 
    //  has inserted their card for player tracking benefits.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES :
    //
    //  <PlayerTrackingLogin> 
    //    <InstanceID type="int">10254</InstanceID> 
    //    <PlayerTrackingString type="string">PLY328990005550101000</PlayerTrackingString> 
    //  </PlayerTrackingLogin>
    //
    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<PlayerTrackingLogin><InstanceID type=\"int\">");
      str_builder.Append(InstanceID);
      str_builder.Append("</InstanceID><PlayerTrackingString type=\"string\">");
      str_builder.Append(PlayerTrackingString);
      str_builder.Append("</PlayerTrackingString></PlayerTrackingLogin>");

      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : This function should not be called from our side.
    //
    public void LoadXml(XmlReader Xmlreader)
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag PlayerTrackingLogin Card element missing");
    }
  }

  public class MGA_MsgPlayerTrackingLoginResponse : IXml
  {
    public int ResponseCode;
    public string ResponseCodeText;

    public string PlayerName;
    public int PlayerPoints;
    public string PromotionalInfo;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES : This function should not be called from our side.
    //
    public String ToXml()
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag PlayerTrackingLoginResponse not found in content area");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  This message is sent in response to PlayerTrackingLogin. The EGM MUST display the player's name, 
    //  number of points, and the promotional information on the display of the EGM.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : 
    //
    //<PlayerTrackingLoginResponse> 
    //  <ResponseCode type="int">0</ResponseCode> 
    //  <ResponseCodeText type="string">SRC_OK</ResponseCodeText> 
    //  <PlayerName type="string">Billy Durango</PlayerName> 
    //  <PlayerPoints type="int">267</PlayerPoints> 
    //  <PromotionalInfo type="string">Welcome!!</PromotionalInfo> 
    //</PlayerTrackingLoginResponse> 
    //
    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "PlayerTrackingLoginResponse")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag PlayerTrackingLoginResponse not found in content area");
      }
      else
      {
        ResponseCode = int.Parse(XML.GetValue(reader, "ResponseCode"));
        ResponseCodeText = XML.GetValue(reader, "ResponseCodeText");
        PlayerName = XML.GetValue(reader, "PlayerName");
        PlayerPoints = int.Parse(XML.GetValue(reader, "PlayerPoints"));
        PromotionalInfo = XML.GetValue(reader, "PromotionalInfo");
      }
    }
  }
}
