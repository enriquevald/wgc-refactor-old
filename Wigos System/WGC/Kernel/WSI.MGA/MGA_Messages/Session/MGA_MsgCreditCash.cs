//------------------------------------------------------------------------------
// Copyright © 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgCreditCash.cs
// 
//   DESCRIPTION: MGA_MsgCreditCash class
// 
//        AUTHOR: Daniel Rodríguez Gil
// 
// CREATION DATE: 21-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2009 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.MGA
{

  public class MGA_MsgCreditCash : IXml
  {
    public int InstanceID;
    public int SessionID;
    public Int64 Amount;
    public int LocalTransactionID;
    public Guid TransactionSecurityGUID;


    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  This message is sent to the 3PGS Device Service to credit an open session with cash.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES :

    public String ToXml()
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag CreditCash Card element missing");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : 
    //
    //  <CreditCash> 
    //    <InstanceID type="int">51652</InstanceID> 
    //    <SessionID type="int">12665</SessionID> 
    //    <Amount type="int64">10000</Amount> 
    //    <LocalTransactionID type="int">21</LocalTransactionID> 
    //    <TransactionSecurityGUID type="guid16">A47C7931-102A-423f-B07A-0882174D264A</TransactionSecurityGUID> 
    //  </CreditCash>
    //

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "CreditCash")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag CreditCash not found in content area");
      }
      else
      {
        InstanceID = int.Parse(XML.GetValue(reader, "InstanceID"));
        SessionID = int.Parse(XML.GetValue(reader, "SessionID"));
        Amount = Int64.Parse(XML.GetValue(reader, "Amount"));
        LocalTransactionID = int.Parse(XML.GetValue(reader, "LocalTransactionID"));
        TransactionSecurityGUID = new Guid(XML.GetValue(reader, "TransactionSecurityGUID"));
      }
    }
  }

  public class MGA_MsgCreditResponse : IXml
  {
    public int ResponseCode;
    public string ResponseCodeText;
    public Int64 SessionCashBalance;
    public Int64 SessionCouponBalance;
    public Int64 ServerTransactionID;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  This message is sent in response to the CreditCash messages.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //
    //  0   - SRC_OK
    //  14  - SRC_InvalidInstanceID
    //  15  - SRC_InvalidLocalTransactionID
    //  26  - SRC_InvalidSessionID
    //  91  - SRC_ServerError
    //  511 - SRC_SessionNoSessionInProgress
    //  522 - SRC_AccountLocked

    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<CreditResponse><ResponseCode type=\"int\">");
      str_builder.Append(ResponseCode);
      str_builder.Append("</ResponseCode><ResponseCodeText type=\"string\">");
      str_builder.Append(ResponseCodeText);
      str_builder.Append("</ResponseCodeText><SessionCashBalance type=\"int64\">");
      str_builder.Append(SessionCashBalance);
      str_builder.Append("</SessionCashBalance><SessionCouponBalance type=\"int64\">");
      str_builder.Append(SessionCouponBalance);
      str_builder.Append("</SessionCouponBalance><ServerTransactionID type=\"int64\">");
      str_builder.Append(ServerTransactionID);
      str_builder.Append("</ServerTransactionID></CreditResponse>");

      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : This function should not be called from our side.
 
    public void LoadXml(XmlReader reader)
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag CreditResponse not found in content area");
    }
  }
}
