//------------------------------------------------------------------------------
// Copyright © 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgCancelPlay.cs
// 
//   DESCRIPTION: MGA_MsgCancelPlay class
// 
//        AUTHOR: Daniel Rodríguez
// 
// CREATION DATE: 28-JUL-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-JUL-2008 DRG    First release.
//------------------------------------------------------------------------------
 
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.MGA
{
  public class MGA_MsgCancelPlay : IXml
  {
    public Int64 TransactionId;

    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<MGA_MsgCancelPlay><TransactionId>");
      str_builder.Append(TransactionId);
      str_builder.Append("</TransactionId></MGA_MsgCancelPlay>");
      return str_builder.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "MGA_MsgCancelPlay")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag MGA_MsgCancelPlay not found in content area");
      }
      else
      {
        TransactionId = Int64.Parse(XML.GetValue(reader, "TransactionId"));
      }
    }
  }

  public class MGA_MsgCancelPlayReply : IXml
  {
    public Int64 TransactionId;

    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();

      str_builder.Append("<MGA_MsgCancelPlayReply><TransactionId>");
      str_builder.Append(TransactionId);
      str_builder.Append("</TransactionId></MGA_MsgCancelPlayReply>");
      return str_builder.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "MGA_MsgCancelPlayReply")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag MGA_MsgCancelPlayReply not found in content area");
      }
      else
      {
        TransactionId = Int64.Parse(XML.GetValue(reader, "TransactionId"));
      }
    }
  }
}
