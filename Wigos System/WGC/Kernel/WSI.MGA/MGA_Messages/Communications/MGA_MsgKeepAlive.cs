//------------------------------------------------------------------------------
// Copyright © 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgKeepAlive.cs
// 
//   DESCRIPTION: MGA_MsgKeepAlive class
// 
//        AUTHOR: Daniel Rodríguez
// 
// CREATION DATE: 21-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2009 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;

namespace WSI.MGA
{
  public class MGA_MsgKeepAlive : IXml
  {
    public int InstanceID;
    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  The KeepAlive message is a heartbeat sent by the EGM to verify continued contact 
    //  of each connection to the back-office. Some communication with the back-office is required 
    //  within specified regular time intervals. This interval is configurable. If an EGM connection 
    //  has not sent a message for the specified time period it is required to send the KeepAlive message. 
    //  If no messages are received by the back-office beyond the time period, the connection will be dropped on 
    //  the EGM's end. If the connection is with an EGM during a session, the session will be closed. 
    //  The interval commences at the time a RegisterInstanceResponse message is received by the EGM for that connection.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //  <KeepAlive> 
    //    <InstanceID type="int">10254</InstanceID> 
    //  </KeepAlive>
    //
    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<KeepAlive><InstanceID type=\"int\">");
      str_builder.Append(InstanceID);
      str_builder.Append("</InstanceID></KeepAlive>");
      
      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    // NOTE : This function should not be called from our side
    //
    public void LoadXml(XmlReader reader)
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag MGA_MsgKeepAlive not found in content area");
    }

  }

  public class MGA_MsgKeepAliveResponse : IXml
  {
    public int ResponseCode;
    public string ResponseCodeText;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    // NOTE : This function should not be called from our side
    //
    public String ToXml()
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag KeepAliveResponse not found in content area");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  This message is sent in response to a KeepAlive messages. If a EGM does not receive a response 
    //  from the 3PGS Device Service during the period prescribed by the system attribute 
    //  sysMGAMConnectionTimeout, the EGM should disconnect and follow the connection lost procedure 
    //  illustrated in the sequence diagrams.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES :
    //  <KeepAliveResponse> 
    //    <ResponseCode type="int">0</ResponseCode> 
    //    <ResponseCodeText type="string">SRC_OK</ResponseCodeText> 
    //  </KeepAliveResponse>
    //
    // Response Codes:
    //  0   - SRC_OK
    //  14  - SRC_InvalidInstanceID
    //  91  - SRC_ServerError
    //
    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "KeepAliveResponse")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag KeepAliveResponse not found in content area");
      }
      else
      {
        ResponseCode = int.Parse(XML.GetValue(reader, "ResponseCode"));
        ResponseCodeText = XML.GetValue(reader, "ResponseCodeText");
      }
    }
  }
}
