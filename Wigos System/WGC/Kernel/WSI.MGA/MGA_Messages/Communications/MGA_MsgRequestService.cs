//------------------------------------------------------------------------------
// Copyright © 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgKeepAlive.cs
// 
//   DESCRIPTION: MGA_MsgKeepAlive class
// 
//        AUTHOR: Daniel Rodríguez
// 
// CREATION DATE: 21-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2009 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Net;
using WSI.Common;


namespace WSI.MGA
{
  public class MGA_MsgRequestService:IXml
  {
    public  IPEndPoint ReplyToUDPAddress;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  To locate services provided by the back office, the EGM should broadcast the RequestService message 
    //  to the Directory Service. The RequestService message is used to obtain specific connection 
    //  information that designates the location of the service. A UDP broadcast is defined as a destination 
    //  IP Address of "255.255.255.255" and a destination MAC Address of "ff:ff:ff:ff:ff:ff". 
    //  The UDP port number (22046 by default) should be configurable based on site preferences. 
    //  The EGM should first setup a UDP listener socket to receive the connection string response. 
    //  This listener socket IP Address and port number MUST be on the MGAM network and used in the request 
    //  as the ResponseAddress element. Due to the nature of UDP broadcasting, the time interval between each 
    //  discovery broadcast MUST be at least five seconds apart. The discovery process MUST be continued until 
    //  a Directory Service is discovered.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //  <RequestService> 
    //    <ServiceName type="string">MGAM 3PGS Device Service</ServiceName> 
    //    <ResponseAddress type="string">192.168.1.113:6559</ResponseAddress> 
    //  </RequestService>
    //
    public String ToXml()
    {

      StringBuilder str_builder;
      String ServiceName = "MGAM 3PGS Device Service";
      str_builder = new StringBuilder();
      str_builder.Append("<RequestService><ServiceName type=\"string\">");
      str_builder.Append(ServiceName);
      str_builder.Append("</ServiceName><ResponseAddress type=\"string\">");
      str_builder.Append(ReplyToUDPAddress.ToString());
      str_builder.Append("</ResponseAddress></RequestService>");

      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : This function should not be called from our side.
    //
    public void LoadXml(XmlReader reader)
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag MGA_MsgRequestService not found in content area");
    }
  }

  public class MGA_MsgRequestServiceResponse:IXml
  {
    public IPEndPoint ServiceAddress;
    public int ResponseCode;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //
    public String ToXml()
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag MGA_MsgRequestServiceResponse not found in content area");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  This message is received as a response to the RequestService message and contains the connection string for the 
    //  3PGS Service. If multiple responses are received (i.e. multiple instances exist on the subnet) 
    //  the EGM MUST ignore all but the first response.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES :
    //  <RequestServiceResponse> 
    //    <ResponseCode type="int">0</ResponseCode> 
    //    <ConnectionString type="string">192.168.100.1:24055</ConnectionString> 
    //  </RequestServiceResponse>
    //
    public void LoadXml(XmlReader reader)
    {
      String ipe_str;
      int idx;
      IPAddress ip;
      int port;

      if (reader.Name != "RequestServiceResponse")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RequestServiceResponse not found in content area");
      }
      else
      {
        ResponseCode = int.Parse(XML.GetValue(reader, "ResponseCode"));
        ipe_str = XML.GetValue(reader, "ConnectionString");
        idx = ipe_str.IndexOf(':');
        ip = IPAddress.Parse(ipe_str.Substring(0, idx));
        port = int.Parse(ipe_str.Substring(1 + idx));
        ServiceAddress = new IPEndPoint(ip, port);
      }
    }
  }
}
