//------------------------------------------------------------------------------
// Copyright © 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgAttributesChanged.cs
// 
//   DESCRIPTION: MGA_MsgAttributesChanged class
// 
//        AUTHOR: Daniel Rodríguez Gil
// 
// CREATION DATE: 21-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2009 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.MGA
{
  public class Attribute
  {
    public string Name;
    public string Scope;
    public string Value;
  }


  public class MGA_MsgAttributesChanged:IXml
  {

    public ArrayList Attributes = new ArrayList(); //ArrayList of Attributes

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //
    public String ToXml()
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag AttributesChanged Card element missing");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  This message will be received by the EGM when the back-office detects that an attribute 
    //  registered with the current InstanceID has changed. The names and values of all changed attributes are listed.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES :
    //  <AttributesChanged> 
    //    <Attributes count="3"> 
    //      <elem> 
    //        <Name type="string">Name1</Name> 
    //        <Scope type="string">application</Scope> 
    //        <Value type="string">String</Value> 
    //      </elem> 
    //      ...
    //    </Attributes> 
    //  </AttributesChanged>
    //
    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "AttributesChanged")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag AttributesChanged not found in content area");
      }
      else
      {
        while (((reader.Name != "Attributes") || (reader.NodeType != XmlNodeType.EndElement)) && !reader.EOF)
        {
          Attribute attrib = new Attribute();
          attrib.Name = XML.GetValue(reader, "Name");
          attrib.Scope = XML.GetValue(reader, "Scope");
          attrib.Value = XML.GetValue(reader, "Value");
          Attributes.Add(attrib);
          reader.Read(); //After this "read" the reader will be at the end of Value
          reader.Read(); //After this "read" the reader will be at the end of elem
          reader.Read(); //After this "read" the reader will be at the end of Attributes or the beginning of new a elem
        }
      }
    }
  }
}
