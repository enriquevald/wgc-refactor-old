//------------------------------------------------------------------------------
// Copyright © 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgCommand.cs
// 
//   DESCRIPTION: MGA_MsgCommand class
// 
//        AUTHOR: Daniel Rodríguez Gil
// 
// CREATION DATE: 21-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2009 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.MGA
{

  public class MGA_MsgCommand:IXml
  {

    public int  CommandID;
    public string Parameter;
    public string CommandText;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES : This function should not be called from our side.
    //
    public String ToXml()
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag Command Card element missing");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  This message is used by the back-office to cause the EGM to execute certain actions. 
    //  Commands require a response code of SRC_OK as acknowledgement of the command having been received. 
    //  There is no response code to indicate success or failure of the command's execution by the EGM.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES :
    //  <Command> 
    //    <CommandID type="int">203</CommandID> 
    //    <Parameter type="string">Congratulations!!!</Parameter> 
    //    <CommandText type="string">Progressive Winner</CommandText> 
    //  </Command>
    //
    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "NotificationResponse")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag NotificationResponse not found in content area");
      }
      else
      {
        CommandID = int.Parse(XML.GetValue(reader, "CommandID"));
        Parameter = XML.GetValue(reader, "Parameter");
        CommandText = XML.GetValue(reader, "CommandText");
      }
    }
  }


  public class MGA_MsgCommandResponse : IXml
  {
    public int InstanceID;
    public int CommandID;
    public int ResponseCode;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  This response is required by the Command message. It signifies that the command was received by the EGM. 
    //  There is no requirement to report the disposition of the command once it's been received.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES : 
    //  <CommandResponse> 
    //    <InstanceID type="int">5532</InstanceID> 
    //    <CommandID type="int">202</CommandID> 
    //    <ResponseCode type="int">0</ResponseCode> 
    //  </CommandResponse>
    //
    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<CommandResponse><InstanceID type=\"int\">");
      str_builder.Append(InstanceID);
      str_builder.Append("</InstanceID><CommandID type=\"int\">");
      str_builder.Append(CommandID);
      str_builder.Append("</CommandID><ResponseCode type=\"int\">");
      str_builder.Append(ResponseCode);
      str_builder.Append("</ResponseCode></CommandResponse>");

      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //
    //

    public void LoadXml(XmlReader reader)
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag CommandResponse not found in content area");
    }
  }
}
