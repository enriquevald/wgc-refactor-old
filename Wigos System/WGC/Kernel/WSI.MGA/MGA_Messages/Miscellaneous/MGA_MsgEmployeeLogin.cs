//------------------------------------------------------------------------------
// Copyright © 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgEmployeeLogin.cs
// 
//   DESCRIPTION: MGA_MsgEmployeeLogin class
// 
//        AUTHOR: Daniel Rodríguez Gil
// 
// CREATION DATE: 21-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2009 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.MGA
{

  public class MGA_MsgEmployeeLogin:IXml
  {

    public int InstanceID;
    public string CardString;
    public string PIN;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  This message must be sent after an Employee card (see ValidateCard) is inserted to get 
    //  information about the employee. See RegisterAction for more details on actions and how to use them.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES :
    //  <EmployeeLogin> 
    //    <InstanceID type="int">10254</InstanceID> 
    //    <CardString type="string">EMP01922343445818</ CardString> 
    //    <PIN type="string">1234</PIN> 
    //  </EmployeeLogin>
    //
    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<EmployeeLogin><InstanceID type=\"int\">");
      str_builder.Append(InstanceID);
      str_builder.Append("</InstanceID><CardString type=\"string\">");
      str_builder.Append(CardString);
      str_builder.Append("</CardString><PIN type=\"string\">");
      str_builder.Append(PIN);
      str_builder.Append("</PIN></EmployeeLogin>");
      
      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : This function should not be called from our side.
    //
    public void LoadXml(XmlReader Xmlreader)
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag EmployeeLogin Card element missing");
    }
  }

  public class Action
  {
    public string ActionName;
    public Guid  ActionGUID;
    public string ActionDescription;
  }

  public class MGA_MsgEmployeeLoginResponse : IXml
  {
    public int ResponseCode;
    public string ResponseCodeText;
    public string CardString;
    public string EmployeeName;
    public int EmployeeID;
    public ArrayList Actions = new ArrayList();

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES : This function should not be called from our side.
    //
    public String ToXml()
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag EmployeeLoginResponse not found in content area");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  This message is sent in response to EmployeeLogin. It returns the name and ID of the employee. 
    //  Additionally returned is a list of all Actions registered by the EGM that this employee has been 
    //  granted permission to perform.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : 
    //  <EmployeeLoginResponse>
    //    <ResponseCode type="int">0</ResponseCode> 
    //    <ResponseCodeText type="string">SRC_OK</ResponseCodeText> 
    //    <CardString type="string">EMP0192818</CardString> 
    //    <EmployeeName type="string">John Rayburn</EmployeeName> 
    //    <EmployeeID type="int">01762</EmployeeID> 
    //    <Actions Count="3"> 
    //      <elem>
    //        <ActionName type="string">MGAMRemoveCashbox</ActionName> 
    //        <ActionGUID type=" guid16"> 05397625-1762-9754-7261-716241825645</ActionGUID> 
    //        <ActionDescription type="string">Can Remove Cashbox</ActionDescription> 
    //      </elem> 
    //      ...
    //    </Actions> 
    //  </EmployeeLoginResponse>
    //
    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "EmployeeLoginResponse")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag EmployeeLoginResponse not found in content area");
      }
      else
      {
        ResponseCode = int.Parse(XML.GetValue(reader, "ResponseCode"));
        ResponseCodeText = XML.GetValue(reader, "ResponseCodeText");
        CardString = XML.GetValue(reader, "CardString");
        EmployeeName = XML.GetValue(reader, "EmployeeName");
        EmployeeID = int.Parse(XML.GetValue(reader, "EmployeeID"));
        while (((reader.Name != "CardInformations") || (reader.NodeType != XmlNodeType.EndElement)) && !reader.EOF)
        {
          Action action = new Action();
          action.ActionName = XML.GetValue(reader, "ActionName");
          action.ActionGUID = new Guid(XML.GetValue(reader, "ActionGUID"));
          action.ActionDescription = XML.GetValue(reader, "ActionDescription");
          Actions.Add(action);
          reader.Read(); //After this "read" the reader will be at the end of SessionCouponBalance
          reader.Read(); //After this "read" the reader will be at the end of elem
          reader.Read(); //After this "read" the reader will be at the end of CardInformations or the beginning of new a elem
        }
      }
    }
  }
}
