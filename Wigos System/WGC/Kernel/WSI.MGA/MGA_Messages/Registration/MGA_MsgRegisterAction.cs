//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgRegisterAction.cs
// 
//   DESCRIPTION: MGA_MsgRegisterAction class
// 
//        AUTHOR: Daniel Rodríguez Gil
// 
// CREATION DATE: 21-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2009 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.MGA
{
  public class MGA_MsgRegisterAction : IXml
  {
    public int InstanceID;
    public Guid ActionGUID;
    public string ActionName;
    public string ActionDescription;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES :
    //
    // Sample:
    //  <RegisterAction>
    //    <InstanceID type="int">XXXXX</InstanceID>
    //    <ActionGUID type="guid16">XXXXXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXX</ActionGUID>
    //    <ActionName type="string">ClearIRSLock</ActionName>
    //    <ActionDescription type="string">Clears IRS triggered account lock</ActionDescription>
    //  </RegisterAction>

    public String ToXml()
    {

      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<RegisterAction><InstanceID type=\"int\">");
      str_builder.Append(InstanceID);
      str_builder.Append("</InstanceID><ActionGUID type=\"guid16\">");
      str_builder.Append(ActionGUID);
      str_builder.Append("</ActionGUID><ActionName type=\"string\">");
      str_builder.Append(ActionName);
      str_builder.Append("</ActionName><ActionDescription type=\"string\">");
      str_builder.Append(ActionDescription);
      str_builder.Append("</ActionDescription></RegisterAction>");

      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : This function should not be called from our side.
    //
    public void LoadXml(XmlReader reader)
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterAction not found in content area");
    }
  }

  public class MGA_MsgRegisterActionResponse : IXml
  {
    public int ResponseCode;
    public string ResponseCodeText;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES : This function should not be called from our side.
    //
    // Possible response codes:
    //  0   - SRC_OK
    //  14  - SRC_InvalidInstanceID
    //  91  - SRC_ServerError
    //  132 - SRC_ActionInvalidGUID
    //  135 - SRC_ActionAlreadyRegistered


    public String ToXml()
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterActionResponse not found in content area");
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "RegisterActionResponse")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterActionResponse not found in content area");
      }
      else
      {
        ResponseCode = int.Parse(XML.GetValue(reader, "ResponseCode"));
        ResponseCodeText = XML.GetValue(reader, "ResponseCodeText");
      }
    }
  }
}
