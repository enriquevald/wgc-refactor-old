//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgRegisterGame.cs
// 
//   DESCRIPTION: MGA_MsgRegisterGame class
// 
//        AUTHOR: Daniel Rodríguez Gil
// 
// CREATION DATE: 21-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2009 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.MGA
{
  public class MGA_MsgRegisterGame : IXml
  {
    public int InstanceID;
    public int GameUPCNumber;
    public string GameTheme;
    public string GameDescription;
    public int GameIndex;
    public string GameIndexDescription;
    public int NumberOfCredits;
    public string GameType;
    public double PayoutPercentage;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  This message is used to notify the back-office of the different games that the EGM can play. 
    //  Any game line and credit combination that can be played by a player MUST be registered with RegisterGame. 
    //  When actual play is recorded via RecordPlay, it MUST have a matching UPC and GameIndex already registered. 
    //  For example, if the game being registered supports a maximum of 3 lines and 3 credits per line, 
    //  then the EGM MUST register each line and credit combination possible. 
    //  (1 Line 1 Credit, 1 Line 2 Credits, 1 Line 3 Credits, 2 Lines 1 Credit, 2 Lines 2 Credits, … up to 3 Lines 3 Credits) 
    //  Each registration for a specific game theme will use the same GameUPCNumber but different 
    //  GameIndex values for each line/credit combination. Failure will result if the previously registered 
    //  GameUPCNumber does not match the parameters of the previously registered RegisterGame. 
    //  If ANY value of the game registration needs to be updated or changed, then a new GameUPCNumber MUST be used. 
    //  All string values MUST be less than 255 characters. 
    //
    //  NOTE: GameUPCNumber must be unique for each GameTheme and can only range from 1 to 65535.
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES :
    //
    // Sample:
    //  
    //  <RegisterGame> 
    //    <InstanceID type="int">10254</InstanceID> 
    //    <GameUPCNumber type="int">2048</GameUPCNumber> 
    //    <NumberOfCredits type="int">3</NumberOfCredits> 
    //    <GameTheme type="string">Meltdown 3 Lines 3 Credits</GameTheme> 
    //    <GameDescription type="string">Meltdown</GameDescription> 
    //    <GameIndex type="int">0</ GameIndex > 
    //    <GameIndexDescription type="string">1 Line 1 Credit</GameIndexDescription> 
    //    <GameType type="string">Progressive</GameType> 
    //    <PayoutPercentage type="double">96.8</PayoutPercentage> 
    //  </RegisterGame>

    public String ToXml()
    {

      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<RegisterGame><InstanceID type=\"int\">");
      str_builder.Append(InstanceID);
      str_builder.Append("</InstanceID><GameUPCNumber type=\"int\">");
      str_builder.Append(GameUPCNumber);
      str_builder.Append("</GameUPCNumber><NumberOfCredits type=\"string\">");
      str_builder.Append(NumberOfCredits);
      str_builder.Append("</NumberOfCredits><GameTheme type=\"string\">");
      str_builder.Append(GameTheme);
      str_builder.Append("</GameTheme><GameDescription type=\"int\">");
      str_builder.Append(GameDescription);
      str_builder.Append("</GameDescription><GameIndex type=\"string\">");
      str_builder.Append(GameIndex);
      str_builder.Append("</GameIndex><GameIndexDescription type=\"int\">");
      str_builder.Append(GameIndexDescription);
      str_builder.Append("</GameIndexDescription><GameType type=\"string\">");
      str_builder.Append(GameType);
      str_builder.Append("</GameType><PayoutPercentage type=\"double\">");
      str_builder.Append(PayoutPercentage);
      str_builder.Append("</PayoutPercentage></RegisterGame>");

      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : This function should not be called from our side.
    //
    public void LoadXml(XmlReader reader)
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterGame not found in content area");
    }
  }

  public class MGA_MsgRegisterGameResponse : IXml
  {
    public int ResponseCode;
    public string ResponseCodeText;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES : This function should not be called from our side.
    //
    // Possible response codes:
    //
    public String ToXml()
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterGameResponse not found in content area");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  This message is sent by the back-office to the EGM in response to the RegisterGame message.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES : 
    //
    //  <RegisterGameResponse> 
    //    <ResponseCode type="int">0</ResponseCode> 
    //    <ResponseCodeText type="string">SRC_OK</ResponseCodeText> 
    //  </RegisterGameResponse>
    //
    // Possible response codes:
    //    0   - SRC_OK
    //    14  - SRC_InvalidInstanceID
    //    91  - SRC_ServerError
    //    510 - SRC_InvalidGameTheme
    //    517 - SRC_GameAlreadyRegistered
    //    518 - SRC_GameInvalidGameIndex
    //    519 - SRC_GameInvalidNumberOfCredits
    //    520 - SRC_GameInvalidGameDescription
    //    523 - SRC_GameInvalidGameUpcNumber
    //    528 - SRC_GameInvalidPayoutPercentage
    //
    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "RegisterGameResponse")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterGameResponse not found in content area");
      }
      else
      {
        ResponseCode = int.Parse(XML.GetValue(reader, "ResponseCode"));
        ResponseCodeText = XML.GetValue(reader, "ResponseCodeText");
      }
    }
  }
}
