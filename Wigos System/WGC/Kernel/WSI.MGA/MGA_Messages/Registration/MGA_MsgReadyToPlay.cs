//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgReadyToPlay.cs
// 
//   DESCRIPTION: MGA_MsgReadyToPlay class
// 
//        AUTHOR: Daniel Rodríguez Gil
// 
// CREATION DATE: 21-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2009 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.MGA
{
  public class MGA_MsgReadyToPlay : IXml
  {
    public int InstanceID;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES :
    //
    // 
    //  <ReadyToPlay>
    //    <InstanceID type="int">XXXXX</InstanceID>
    //  </ReadyToPlay>

    public String ToXml()
    {

      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<ReadyToPlay><InstanceID type=\"int\">");
      str_builder.Append(InstanceID);
      str_builder.Append("</InstanceID></ReadyToPlay>");

      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : This function should not be called from our side.
    //
    public void LoadXml(XmlReader reader)
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag ReadyToPlay not found in content area");
    }
  }

  public class MGA_MsgReadyToPlayResponse : IXml
  {
    public int ResponseCode;
    public string ResponseCodeText;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES : This function should not be called from our side.
    //
    public String ToXml()
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag ReadyToPlayResponse not found in content area");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : 
    //
    // Sample:
    //  <ReadyToPlayResponse>
    //    <ResponseCode type="int">X</ResponseCode>
    //    <ResponseCodeText type="string">XXXXXX</ResponseCodeText>
    //  </ReadyToPlayResponse>
    //
    // Possible response codes:
    //  
    //  0   - SRC_OK
    //  14  - SRC_InvalidInstanceID
    //  91  - SRC_ServerError
    //  95  - SRC_NotAllRequiredAttributesRegistered
    //  115 - SRC_NotAllRequiredNotificationsRegistered
    //  116 - SRC_NotAllRequiredCommandsRegistered
    
    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "ReadyToPlayResponse")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag ReadyToPlayResponse not found in content area");
      }
      else
      {
        ResponseCode = int.Parse(XML.GetValue(reader, "ResponseCode"));
        ResponseCodeText = XML.GetValue(reader, "ResponseCodeText");
      }
    }
  }
}
