//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgRegisterNotification.cs
// 
//   DESCRIPTION: MGA_MsgRegisterNotification class
// 
//        AUTHOR: Daniel Rodríguez Gil
// 
// CREATION DATE: 21-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2009 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.MGA
{
  public class MGA_MsgRegisterNotification : IXml
  {
    public int InstanceID;
    public int NotificationID;
    public string Description;
    public int PriorityLevel;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //  This message is sent by the EGM to register notifications with to the back-office. 
    //  Notifications are messages used by the vendor to notify the back-office of certain events. 
    //  The vendor is required to support a back-office defined set of notification messages.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES :
    //
    //  <RegisterNotification> 
    //    <InstanceID type="int">10254</InstanceID> 
    //    <NotificationID type="int">1</NotificationID> 
    //    <Description type="string">Lost Connection</Description> 
    //    <PriorityLevel type="int">4259842</PriorityLevel> 
    //  </RegisterNotification>
    //
    // Possible notifications:
    //      LOCKED_DOOR_OPEN                               1
    //      DOOR_OPENED                                    2
    //      DOOR_CLOSED                                    3
    //      EMPLOYEE_LOGGED_IN                             4
    //      EMPLOYEE_LOGGED_OUT                            5
    //      LOCKED_BA_JAM                                  6
    //      LOCKED_BA_FULL                                 7
    //      LOCKED_TILT                                    10
    //      LOCKED_BEGIN_SESSION_WITH_CASH_FAILED          13
    //      LOCKED_REGISTRATION_FAILED                     16
    //      LOCKED_CREDIT_CASH_FAILED                      17
    //      LOCKED_SOFTWARE_ERROR                          19
    //      LOCKED_MALFORMED_MESSAGE                       20
    //      LOCKED_GAME_MALFUNCTION                        21
    //      LOCKED_COMMANDED                               22
    //      POWER_FAILURE                                  23
    //      CANCELED_CREDIT_HAND_PAY                       24
    //      PROGRESSIVE_JACKPOT                            25
    //      LOST_CONNECTION                                27
    //      LOCKED_WIN_THRESHOLD_EXCEEDED                  30
    //      LOCK_CLEARED                                   31

    public String ToXml()
    {

      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<RegisterNotification><InstanceID type=\"int\">");
      str_builder.Append(InstanceID);
      str_builder.Append("</InstanceID><NotificationID type=\"int\">");
      str_builder.Append(NotificationID);
      str_builder.Append("</NotificationID><Description type=\"string\">");
      str_builder.Append(Description);
      str_builder.Append("</Description><PriorityLevel type=\"int\">");
      str_builder.Append(PriorityLevel);
      str_builder.Append("</PriorityLevel></RegisterNotification>");

      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : This function should not be called from our side.
    //
    public void LoadXml(XmlReader reader)
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterNotification not found in content area");
    }
  }

  public class MGA_MsgRegisterNotificationResponse : IXml
  {
    public int ResponseCode;
    public string ResponseCodeText;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES : This function should not be called from our side.
    //
    public String ToXml()
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterNotificationResponse not found in content area");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    // This message is sent in response to the RegisterNotification message.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : 
    //  <RegisterNotificationResponse> 
    //    <ResponseCode type="int">0</ResponseCode> 
    //    <ResponseCodeText type="string">SRC_OK</ResponseCodeText> 
    //  </RegisterNotificationResponse>
    // Response codes:
    //  0   - SRC_OK
    //  14  - SRC_InvalidInstanceID
    //  91  - SRC_ServerError
    //  58  - SRC_NotificationAlreadyRegistered
  public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "RegisterNotificationResponse")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterNotificationResponse not found in content area");
      }
      else
      {
        ResponseCode = int.Parse(XML.GetValue(reader, "ResponseCode"));
        ResponseCodeText = XML.GetValue(reader, "ResponseCodeText");
      }
    }
  }
}
