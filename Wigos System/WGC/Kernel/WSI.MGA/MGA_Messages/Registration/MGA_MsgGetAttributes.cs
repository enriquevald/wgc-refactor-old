//------------------------------------------------------------------------------
// Copyright © 2009 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgGetAttributes.cs
// 
//   DESCRIPTION: MGA_MsgGetAttributes class
// 
//        AUTHOR: Daniel Rodríguez Gil
// 
// CREATION DATE: 21-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2009 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.MGA
{

  public class MGA_MsgGetAttributes : IXml
  {

    public int InstanceID;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  This message will cause the back-office to send all currently enrolled attributes to the EGM, 
    //  with their current values.    
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES :
    //  <GetAttributes> 
    //    <InstanceID type="int">10254</InstanceID> 
    //  </GetAttributes>    
    //
    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<GetAttributes><InstanceID type=\"int\">");
      str_builder.Append(InstanceID);
      str_builder.Append("</InstanceID></GetAttributes>");

      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : This function should not be called from our side.
    //
    public void LoadXml(XmlReader Xmlreader)
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag GetAttributes Card element missing");
    }
  }

  public class MGA_MsgGetAttributesResponse : IXml
  {
    public int ResponseCode;
    public string ResponseCodeText;
    public ArrayList Attributes = new ArrayList();

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES : This function should not be called from our side.
    //
    public String ToXml()
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag GetAttributesResponse not found in content area");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  This message is sent in response to GetAttributes. It returns the name and ID of the employee. 
    //  Additionally returned is a list of all Actions registered by the EGM that this employee has been 
    //  granted permission to perform.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : 
    //  <GetAttributesResponse> 
    //    <ResponseCode type="int">0</ResponseCode> 
    //    <ResponseCodeText type="string">SRC_OK</ResponseCodeText> 
    //    <Attributes Count="2"> 
    //      <elem> 
    //        <Name type="string">sitMGAMSessionBalanceLimit</Name> 
    //        <Scope type="string">site</Scope> 
    //        <Value type="string">500000</Value> 
    //      </elem> 
    //      ...
    //    </Attributes> 
    //  </GetAttributesResponse>
    //
    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "GetAttributesResponse")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag GetAttributesResponse not found in content area");
      }
      else
      {
        ResponseCode = int.Parse(XML.GetValue(reader, "ResponseCode"));
        ResponseCodeText = XML.GetValue(reader, "ResponseCodeText");
        while (((reader.Name != "CardInformations") || (reader.NodeType != XmlNodeType.EndElement)) && !reader.EOF)
        {
          Attribute attrib = new Attribute();
          attrib.Name = XML.GetValue(reader, "Name");
          attrib.Scope = XML.GetValue(reader, "Scope");
          attrib.Value = XML.GetValue(reader, "Value");
          Attributes.Add(attrib);
          reader.Read(); //After this "read" the reader will be at the end of Value
          reader.Read(); //After this "read" the reader will be at the end of elem
          reader.Read(); //After this "read" the reader will be at the end of Attributes or the beginning of new a elem
        }
      }
    }
  }
}
