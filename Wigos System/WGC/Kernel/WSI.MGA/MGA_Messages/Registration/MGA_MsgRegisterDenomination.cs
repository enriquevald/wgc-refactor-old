//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgRegisterDenomination.cs
// 
//   DESCRIPTION: MGA_MsgRegisterDenomination class
// 
//        AUTHOR: Daniel Rodríguez Gil
// 
// CREATION DATE: 21-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2009 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.MGA
{
  public class MGA_MsgRegisterDenomination : IXml
  {
    public int InstanceID;
    public int GameUPCNumber;
    public int Denomination;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  This message notifies the back-office of denominations supported by each GameUPC. 
    //  For each denomination (i.e. if the game supports multi-denomination feature) the RegisterDenomination 
    //  MUST be called. The GameUPCNumber MUST match the games registered via RegisterGame. 
    //  RegisterDenomination MUST be called after all the game UPCs have been registered. If more then one 
    //  GameUPCNumber has been registered via RegisterGame, one ore more denominations MUST be registered 
    //  for each GameUPCNumber. For example, if an EGM has registered a GameUPCNumber 2048 which 
    //  supports $0.10, $0.25, and $0.50 denominations, the EGM MUST call RegisterDenomination three times. 
    //  One for each denomination supported. An attempt to call RecordPlay with a denomination value that 
    //  has not been previously registered will result in a failure.
    //  
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES :
    //
    // Sample:
    //  
    //  <RegisterDenomination> 
    //    <InstanceID type="int">15625</InstanceID> 
    //    <GameUPCNumber type="int">2048</GameUPCNumber> 
    //    <Denomination type="int">25</Denomination> 
    //  </RegisterDenomination>
    //

    public String ToXml()
    {

      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<RegisterDenomination><InstanceID type=\"int\">");
      str_builder.Append(InstanceID);
      str_builder.Append("</InstanceID><GameUPCNumber type=\"int\">");
      str_builder.Append(GameUPCNumber);
      str_builder.Append("</GameUPCNumber><Denomination type=\"string\">");
      str_builder.Append(Denomination);
      str_builder.Append("</Denomination></RegisterDenomination>");

      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : This function should not be called from our side.
    //
    public void LoadXml(XmlReader reader)
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterDenomination not found in content area");
    }
  }

  public class MGA_MsgRegisterDenominationResponse : IXml
  {
    public int ResponseCode;
    public string ResponseCodeText;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES : This function should not be called from our side.
    //
    // Possible response codes:
    //
    public String ToXml()
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterDenominationResponse not found in content area");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  This message is sent by the back-office to the EGM in response to the RegisterDenomination message.    
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES : 
    //
    // Possible response codes:
    //  0   - SRC_OK SRC_OK
    //  14  - SRC_InvalidInstanceID
    //  21  - SRC_InvalidDenomination
    //  89  - SRC_NoGameRegistered
    //  91  - SRC_ServerError
    //  97  - SRC_DenominationAlreadyRegistered
    //  523 - SRC_GameInvalidGameUpcNumber
    //
    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "RegisterDenominationResponse")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterDenominationResponse not found in content area");
      }
      else
      {
        ResponseCode = int.Parse(XML.GetValue(reader, "ResponseCode"));
        ResponseCodeText = XML.GetValue(reader, "ResponseCodeText");
      }
    }
  }
}
