//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgRegisterInstance.cs
// 
//   DESCRIPTION: MGA_MsgRegisterInstance class
// 
//        AUTHOR: Daniel Rodríguez Gil
// 
// CREATION DATE: 21-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2009 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.MGA
{
  public class MGA_MsgRegisterInstance : IXml
  {
    public string ManufacturerName;
    public Guid DeviceGUID;
    public string DeviceName;
    public Guid ApplicationGUID;
    public string ApplicationName;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //           The RegisterInstance message is used to register the EGM with the 3PGS Device Service. 
    //           The RegisterInstance message is the first message to be sent by an EGM once 
    //           it has obtained a connection string via RequestService. 
    //           The EGM MUST persistently store the values used in the RegisterInstance message and use the same 
    //           values each time it registers. 
    //           If the version of the application on the EGM changes, then the ApplicationGUID MUST be changed as well.
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES :
    //    <RegisterInstance>
    //      <ManufacturerName type="string">Multimedia Games, Inc.</ManufacturerName>
    //      <DeviceGUID type="guid16">01672121-6716-7621-1092-916725498120</DeviceGUID>
    //      <DeviceName type="string">EGM</DeviceName>
    //      <ApplicationGUID type="guid16">01672121-6716-7621-1092-916725498122</ApplicationGUID>
    //      <ApplicationName type="string">EGM.Application.01</ApplicationName>
    //    </RegisterInstance>
    //
    public String ToXml()
    {

      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<RegisterInstance><ManufacturerName type=\"string\">");
      str_builder.Append(ManufacturerName);
      str_builder.Append("</ManufacturerName><DeviceGUID type=\"guid16\">");
      str_builder.Append(DeviceGUID);
      str_builder.Append("</DeviceGUID><DeviceName type=\"string\">");
      str_builder.Append(DeviceName);
      str_builder.Append("</DeviceName><ApplicationGUID type=\"guid16\">");
      str_builder.Append(ApplicationGUID);
      str_builder.Append("</ApplicationGUID><ApplicationName type=\"string\">");
      str_builder.Append(ApplicationName);
      str_builder.Append("</ApplicationName></RegisterInstance>");

      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : This function should not be called from our side.
    //
    public void LoadXml(XmlReader reader)
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterInstance not found in content area");
    }
  }

  public class MGA_MsgRegisterInstanceResponse : IXml
  {
    public int InstanceID;
    public string TimeStamp;
    public int ResponseCode;
    public string ResponseCodeText;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES : This function should not be called from our side.
    //
    public String ToXml()
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterInstanceResponse not found in content area");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  This message is sent in response to a RegisterInstance message. 
    //  The InstanceID returned will be referenced in all future communication with the service. 
    //  If you do not get a response code of SRC_OK or SRC_KnownRegistration then you must stop. 
    //  You cannot continue the registration process. If the EGM receives a ResponseCode of SRC_KnownRegistration, 
    //  it is RECOMMENED that the EGM skips the registration of the required Attributes, Commands, and Notifications. 
    //  This allows for quicker registration.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : 
    //
    // SAMPLE  :
    //
    //  <RegisterInstanceResponse> 
    //    <InstanceID type="int">10254</InstanceID> 
    //    <TimeStamp type="string">09-12-2008 15:21:23</TimeStamp> 
    //    <ResponseCode type="int">0</ResponseCode> 
    //    <ResponseCodeText type="string">SRC_OK</ResponseCodeText> 
    //  </RegisterInstanceResponse>
    //
    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "RegisterInstanceResponse")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterInstanceResponse not found in content area");
      }
      else
      {
        InstanceID = int.Parse(XML.GetValue(reader, "InstanceID"));
        TimeStamp = XML.GetValue(reader, "TimeStamp");
        ResponseCode = int.Parse(XML.GetValue(reader, "ResponseCode"));
        ResponseCodeText = XML.GetValue(reader, "ResponseCodeText");
      }
    }
  }
}
