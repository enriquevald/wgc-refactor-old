//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgRegisterCommand.cs
// 
//   DESCRIPTION: MGA_MsgRegisterCommand class
// 
//        AUTHOR: Daniel Rodríguez Gil
// 
// CREATION DATE: 21-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2009 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.MGA
{
  public class MGA_MsgRegisterCommand : IXml
  {
    public int InstanceID;
    public int CommandID;
    public string Description;
    public string ParameterName;
    public int AccessType;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES :
    //
    //  <RegisterCommand>
    //    <InstanceID type="int">XXXX</InstanceID>
    //    <CommandID type="int">XXX</CommandID>
    //    <Description type="string">XXXXXX</Description>
    //    <ParameterName type="string">XXX</ParameterName>
    //    <AccessType type="int">XX</AccessType>
    //  </RegisterCommand>


    public String ToXml()
    {

      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<RegisterCommand><InstanceID type=\"int\">");
      str_builder.Append(InstanceID);
      str_builder.Append("</InstanceID><CommandID type=\"int\">");
      str_builder.Append(CommandID);
      str_builder.Append("</CommandID><Description type=\"string\">");
      str_builder.Append(Description);
      str_builder.Append("</Description><ParameterName type=\"string\">");
      str_builder.Append(ParameterName);
      str_builder.Append("</ParameterName><AccessType type=\"int\">");
      str_builder.Append(AccessType);
      str_builder.Append("</AccessType></RegisterCommand>");

      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : This function should not be called from our side.
    //
    public void LoadXml(XmlReader reader)
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterCommand not found in content area");
    }
  }

  public class MGA_MsgRegisterCommandResponse : IXml
  {
    public int ResponseCode;
    public string ResponseCodeText;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES : This function should not be called from our side.
    //

    public String ToXml()
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterCommandResponse not found in content area");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : 
    //
    // Possible response code:
    //  0   - SRC_OK
    //  14  - SRC_InvalidInstanceID
    //  38  - SRC_CommandAlreadyRegistered
    //  91  - SRC_ServerError
    //
    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "RegisterCommandResponse")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterCommandResponse not found in content area");
      }
      else
      {
        ResponseCode = int.Parse(XML.GetValue(reader, "ResponseCode"));
        ResponseCodeText = XML.GetValue(reader, "ResponseCodeText");
      }
    }
  }
}
