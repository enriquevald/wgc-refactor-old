//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_MsgRegisterAttribute.cs
// 
//   DESCRIPTION: MGA_MsgRegisterAttribute class
// 
//        AUTHOR: Daniel Rodríguez Gil
// 
// CREATION DATE: 21-MAY-2009
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 21-MAY-2009 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.MGA
{
  public class MGA_MsgRegisterAttribute : IXml
  {
    public int InstanceID;
    public string Scope;
    public string ItemName;
    public string ItemValue;
    public string Minimum;
    public string Maximum;
    public string AllowedValues;
    public string Type;
    public string ControlType;
    public int AccessType;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES :
    //
    //  Sample:
    //  <RegisterAttribute>
    //    <InstanceID type="int">XXXX</InstanceID>
    //    <Scope type="string">XXXXX</Scope>
    //    <ItemName type="string">XXXXXXXX</ItemName>
    //    <ItemValue type="string">XX</ItemValue>
    //    <Minimum type="string">XXX</Minimum>
    //    <Maximum type="string">XXX</Maximum>
    //    <AllowedValues type="string">XXX</AllowedValues>
    //    <Type type="string">XXX</Type>
    //    <ControlType type="string">XXX</ControlType>
    //    <AccessType type="int">XX</AccessType>
    //  </RegisterAttribute>
    //
    // Possible attributes:
    //    sitMGAMIRSWinThreshold
    //    sitMGAMLockOnProgressiveJackpot
    //    sysMGAMKeepAliveInterval
    //    sysMGAMConnectionTimeout
    //    devMGAMBillAcceptorEnabled
    //    insMGAMPlayerTrackingPoints
    //    insMGAMPenniesPerPoint
    //    insMGAMPointsPerEntry
    //    insMGAMSweepstakesEntries
    //    insMGAMPromotionalInfo
    public String ToXml()
    {

      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<RegisterAttribute><InstanceID type=\"int\">");
      str_builder.Append(InstanceID);
      str_builder.Append("</InstanceID><Scope type=\"string\">");
      str_builder.Append(Scope);
      str_builder.Append("</Scope><ItemName type=\"string\">");
      str_builder.Append(ItemName);
      str_builder.Append("</ItemName><ItemValue type=\"string\">");
      str_builder.Append(ItemValue);
      str_builder.Append("</ItemValue><Minimum type=\"string\">");
      str_builder.Append(Minimum);
      str_builder.Append("</Minimum><Maximum type=\"string\">");
      str_builder.Append(Maximum);
      str_builder.Append("</Maximum><RegisterAttribute type=\"string\">");
      str_builder.Append(Minimum);
      str_builder.Append("</Minimum><AllowedValues type=\"string\">");
      str_builder.Append(AllowedValues);
      str_builder.Append("</AllowedValues><Type type=\"string\">");
      str_builder.Append(Type);
      str_builder.Append("</Type><ControlType type=\"string\">");
      str_builder.Append(ControlType);
      str_builder.Append("</ControlType><AccessType type=\"int\">");
      str_builder.Append(AccessType);
      str_builder.Append("</AccessType></RegisterAttribute>");

      return str_builder.ToString();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : This function should not be called from our side.
    //
    public void LoadXml(XmlReader reader)
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterAttribute not found in content area");
    }
  }

  public class MGA_MsgRegisterAttributeResponse : IXml
  {
    public int ResponseCode;
    public string ResponseCodeText;

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the XML string
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES : This function should not be called from our side.
    //
    public String ToXml()
    {
      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterAttributeResponse not found in content area");
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Parses the XML information
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : 
    //
    //   NOTES : This function should not be called from our side.
    //
    // Possible response code:
    //  0   - SRC_OK
    //  14  - SRC_InvalidInstanceID
    //  32  - SRC_AttributeAlreadyRegistered
    //  91  - SRC_ServerError

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "RegisterAttributeResponse")
      {
        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag RegisterAttributeResponse not found in content area");
      }
      else
      {
        ResponseCode = int.Parse(XML.GetValue(reader, "ResponseCode"));
        ResponseCodeText = XML.GetValue(reader, "ResponseCodeText");
      }
    }
  }
}
