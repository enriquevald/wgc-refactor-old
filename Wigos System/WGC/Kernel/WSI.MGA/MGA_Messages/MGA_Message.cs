//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: MGA_Mesage.cs
// 
//   DESCRIPTION: MGA_Mesage class
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 12-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-MAR-2007 AJQ    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using WSI.Common;


namespace WSI.MGA
{
  public interface IXml
  {
    ///TODO: cambiar tambi�n todos los ToXml para que en lugar del append child hagan 
    ///una string directa con los valores.
    String ToXml();
    // void LoadXml (String Xml);
    void LoadXml(XmlReader Xml);
  }

  public interface IXml_CJ
  {
    String ToXml();
    void LoadXml (String Xml);
  }

  public class MGA_Exception : Exception
  {
    MGA_ResponseCodes response_code;
    String response_code_text;

    public MGA_ResponseCodes ResponseCode
    {
      get
      {
        return response_code;
      }
    }
    public String ResponseCodeText
    {
      get
      {
        return response_code_text;
      }
    }

    public MGA_Exception (MGA_ResponseCodes Value)
    {
      response_code = Value;
    }

    public MGA_Exception (MGA_ResponseCodes Code, String Text):base(Text)
    {
      response_code = Code;
      response_code_text = Text;
    }

    public MGA_Exception (MGA_ResponseCodes Code, String Text, Exception InnerException): base (Text, InnerException)
    {
      response_code = Code;
      response_code_text = Text;
    }
  }


  public partial class MGA_Message
  {
    public IXml MsgContent;

    //public Int64 TransactionId
    //{
    //  get
    //  {
    //    //switch ( MsgHeader.MsgType )
    //    //{
    //    //  //case MGA_MsgTypes.MGA_MsgReportPlay:
    //    //  //  return ( (MGA_MsgReportPlay) MsgContent ).TransactionId;

    //    //  //case MGA_MsgTypes.MGA_MsgAddCredit:
    //    //  //  return ( (MGA_MsgAddCredit) MsgContent ).TransactionId;

    //    //  default:
    //        return 0;
    //    //}
    //  }
    //}

    private MGA_Message ()
    {
    }


    //////public static MGA_Message CreateMessage (String Xml)
    //////{
    //////  MGA_Message MGA_message;

    //////  MGA_message = null;

    //////  try
    //////  {
    //////    XmlDocument document;
    //////    XmlNode node;
    //////    MGA_MsgTypes msg_type;
    //////    String target_type;

    //////    document = new XmlDocument ();
    //////    document.LoadXml (Xml);

    //////    node = document.SelectSingleNode ("MGA_Message/MGA_MsgHeader/MsgType");
    //////    if ( node == null )
    //////    { 
    //////      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag not found: MsgType"); 
    //////    }

    //////    target_type = node.InnerText;

    //////    for ( msg_type = 0; msg_type < MGA_MsgTypes.MGA_MsgUnknown; msg_type++ )
    //////    {
    //////      if ( target_type == msg_type.ToString () )
    //////      {
    //////        break;
    //////      }
    //////    }
    //////    if ( msg_type >= MGA_MsgTypes.MGA_MsgUnknown )
    //////    {
    //////      return null;
    //////    }

    //////    MGA_message = CreateMessage (msg_type);

    //////    node = document.SelectSingleNode ("MGA_Message/MGA_MsgHeader");
    //////    if ( node == null )
    //////    {
    //////      throw new MGA_Exception (MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag not found: MGA_MsgHeader");
    //////    }
    //////    MGA_message.MsgHeader.LoadXml (node.OuterXml);
    //////    node = document.SelectSingleNode ("MGA_Message/MGA_MsgContent");
    //////    if ( node == null )
    //////    {
    //////      throw new MGA_Exception (MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag not found: MGA_MsgContent");
    //////    }
    //////    node = node.SelectSingleNode (target_type);
    //////    if ( node == null )
    //////    {
    //////      throw new MGA_Exception (MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag not found: " + target_type.ToString ());
    //////    }
    //////    MGA_message.MsgContent.LoadXml (node.OuterXml);
    //////  }
    //////  catch ( MGA_Exception MGA_ex )
    //////  {
    //////    throw MGA_ex;
    //////  }
    //////  catch ( Exception ex )
    //////  {
    //////    // Format Error in message
    //////    throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, ex.Message, ex);
    //////  }

    //////  return MGA_message;
    //////}

    //////public static MGA_Message CreateMessage (MGA_MsgTypes MsgType)
    //////{
    //////  MGA_Message msg;
    //////  IXml content;

    //////  Assembly assembly;
    //////  String type_name;
    //////  Type target_type;


    //////  // The Power of Reflection
    //////  content = null;

    //////  type_name = "WSI.MGA." + MsgType.ToString ();

    //////  assembly = Assembly.GetExecutingAssembly ();
    //////  target_type = assembly.GetType (type_name);

    //////  Type[] types = new Type[0];
    //////  ConstructorInfo info = target_type.GetConstructor (types);
    //////  content = (IXml) info.Invoke (null);

    //////  msg = new MGA_Message ();

    //////  msg.MsgHeader = new MGA_MsgHeader ();
    //////  msg.MsgHeader.MsgType = MsgType;
    //////  msg.MsgContent = content;

    //////  return msg;
    //////}


    //public static MGA_Message CreateMessage(String Xml)
    //{
    //  MGA_Message MGA_message;

    //  MGA_message = null;

    //  try
    //  {
    //    XmlReader xmlreader;

    //    bool found;

    //    System.IO.StringReader textreader = new System.IO.StringReader(Xml);
    //    xmlreader = XmlReader.Create(textreader);

    //    //Loads the message header
    //    hdr.LoadXml(xmlreader);
    //    if (hdr.MsgType >= MGA_MsgTypes.MGA_MsgUnknown)
    //    {
    //      return null;
    //    }
    //    //Creates the message if the message header is correct.
    //    MGA_message = CreateMessage(hdr.MsgType);
    //    //Assign the header to the current message.

    //    //Put the xmlreader at the beginning of the Message Content
    //    found = false;
    //    while (xmlreader.Read() && !found)
    //    {
    //      if ((xmlreader.Name == "MGA_MsgContent") && (xmlreader.NodeType.Equals(XmlNodeType.Element)))
    //      {
    //        found = true;
    //      }
    //    }
    //    if (found)
    //    {
    //      MGA_message.MsgContent.LoadXml(xmlreader);
    //      if (MGA_message.MsgContent == null)
    //      {
    //        throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag not found: " + MGA_message.MsgHeader.MsgType.ToString());
    //      }
    //    }
    //    else
    //    {
    //      throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, "Tag not found: " + MGA_message.MsgHeader.MsgType.ToString());
    //    }
    //    xmlreader.Close();
    //  }
    //  catch (MGA_Exception MGA_ex)
    //  {
    //    throw MGA_ex;
    //  }
    //  catch (Exception ex)
    //  {
    //    // Format Error in message
    //    throw new MGA_Exception(MGA_ResponseCodes.MGA_RC_MSG_FORMAT_ERROR, ex.Message, ex);
    //  }
    //  return MGA_message;
    //}


    public static MGA_Message CreateMessage(MGA_MsgTypes MsgType)
    {
      MGA_Message msg;
      IXml content;

      Assembly assembly;
      String type_name;
      Type target_type;

      msg = new MGA_Message();

      // The Power of Reflection
      content = null;

      type_name = "WSI.MGA." + MsgType.ToString();

      assembly = Assembly.GetExecutingAssembly();
      target_type = assembly.GetType(type_name);

      Type[] types = new Type[0];
      ConstructorInfo info = target_type.GetConstructor(types);
      content = (IXml)info.Invoke(null);

      msg.MsgContent = content;

      return msg;
    }


    //------------------------------------------------------------------------------
    // PURPOSE : This constructs the whole message prepared to be send
    //          
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT : 
    //
    // RETURNS : XML String
    //
    //   NOTES :
    //  The header will always look like this:
    //
    //  MGAM Payload: XMLDataSet 
    //  MGAM Size: XXX 
    //  <?xml version="1.0" encoding="UTF-8"?>
    //
    public String ToXml()
    {
      String xml;
      String xml_content;

      xml_content = MsgContent.ToXml ();

      xml = "";
      xml += "MGAM Payload: XMLDataSet";
      xml += "MGAM Size: " + xml_content.Length ;
      xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
      xml += xml_content;

      return xml;
    }
  }

}
