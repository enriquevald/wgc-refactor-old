//------------------------------------------------------------------------------
// Copyright � 2003 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : ServiceTrxMgr.c
//
//   DESCRIPTION : Functions and Methods used by any kind of TrxMgr.
//                 A TrxMgr's needs to communicate via a CommMgr.
//                 A TrxMgr's needs to know the status of the other TrxMgr's 
//                 of the same type.
//
//        AUTHOR : Andreu Juli�
//
// CREATION DATE : 11-FEB-2003
// 
// REVISION HISTORY :
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 11-FEB-2003 AJQ    Initial draft.
//
//-------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_MISC
#define INCLUDE_QUEUE_API
#define INCLUDE_IPC_API
#define INCLUDE_PRIVATE_LOG
#define INCLUDE_LICENSE
#define INCLUDE_ALARMS
#define INCLUDE_SERVICE
#define INCLUDE_NLS_API
#define INCLUDE_WATCHDOG_API
#include "CommonDef.h"

#include "ServiceInternals.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------
#define NO_LOCAL_COMM_MGR     (-1)

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------
typedef struct 
{
  DWORD index;    // Index
  DWORD node_id;  // Ipc node instance id
  DWORD wdog_id;  // Ipc wdog instance id
  BOOL  is_local; // Flag that indicates whether or not is a local node

} TYPE_IPC_SERVICE;

typedef struct
{
  DWORD               num_ipc_services;               // Number of IPC Service's
  TYPE_IPC_SERVICE    ipc_service [MAX_IPC_SERVICES]; // IPC Service's

} TYPE_IPC_SERVICE_LIST;

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------
static TYPE_IPC_SERVICE_LIST           GLB_CommMgrList;
static TYPE_IPC_SERVICE_LIST           GLB_TrxMgrList;
static WDOG_OUD_GET_AUTO_QUERY_STATUS  GLB_AutoQueryStatus;
static DWORD                           GLB_LocalCommMgrIndex = NO_LOCAL_COMM_MGR;
static BYTE                            GLB_TrxType = 0xFF;

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------
static BOOL ServiceTrxMgr_InitIpcServiceList (TCHAR                 * pBaseNodeName, 
                                              TYPE_IPC_SERVICE_LIST * pIpcServiceList);

static BOOL ServiceTrxMgr_SetAutoQuery (TYPE_IPC_SERVICE_LIST * pCommMgrList, 
                                        TYPE_IPC_SERVICE_LIST * pTrxMgrList);

static BOOL ServiceTrxMgr_GetAutoQuery (WDOG_OUD_GET_AUTO_QUERY_STATUS * pAutoQueryStatus);


//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Initializes the list of services (TrxMgr's & CommMgr's).
//           The Watchdog API is also initialized. 
//           The TrxMgr's and CommMgr's will be continuosly 'pinged'
//
//  PARAMS :
//      - INPUT :
//          - TrxType : TrxMgr Type (Transaction Type)
//          - AutoQueryCommMgr : Indicates whether or not query for the CommMgr's
//          - AutoQueryTrxMgr :  The same for the TrxMgr's
//
//      - OUTPUT :
//
// RETURNS :
//      - TRUE :  Success
//      - FALSE : Failure
//
//   NOTES : The service CAN NOT use the Watchdog API to perform another auto-query

BOOL    ServiceTrxMgr_Init (DWORD TrxType, 
                            BOOL  AutoQueryCommMgr, 
                            BOOL  AutoQueryTrxMgr)
{
  TCHAR   _base_name [IPC_NODE_NAME_LENGTH + 1];
  DWORD   _idx;
  
  // Reset List's
  memset (&GLB_CommMgrList, 0, sizeof (GLB_CommMgrList));
  memset (&GLB_TrxMgrList,  0, sizeof (GLB_TrxMgrList));
  
  // Set TrxType
  GLB_TrxType = (BYTE) TrxType;

  // Local CommMgr
  GLB_LocalCommMgrIndex = NO_LOCAL_COMM_MGR;

  if ( AutoQueryCommMgr )
  {
    if ( ServiceTrxMgr_InitIpcServiceList (IPC_NODE_NAME_COMM_MGR, &GLB_CommMgrList) == FALSE )
    {
      return FALSE;
    }

    for ( _idx = 0; _idx < GLB_CommMgrList.num_ipc_services; _idx++ )
    {
      if ( GLB_CommMgrList.ipc_service[_idx].is_local == TRUE )
      {
        GLB_LocalCommMgrIndex = _idx;

        break;
      }
    }
  }

  if ( AutoQueryTrxMgr )
  {
    // Build TrxMgr Node Name
    IpcBuildTrxMgrNodeName (TrxType, _base_name);
    if ( ServiceTrxMgr_InitIpcServiceList (_base_name, &GLB_TrxMgrList) == FALSE )
    {
      return FALSE;
    }
  }

  if ( AutoQueryCommMgr || AutoQueryTrxMgr )
  {
    if ( ServiceTrxMgr_SetAutoQuery (&GLB_CommMgrList, &GLB_TrxMgrList) == FALSE )
    {
      return FALSE;
    }
  }

  return TRUE;

} // ServiceTrxMgr_Init

//------------------------------------------------------------------------------
// PURPOSE : Returns a CommMgr node instance id suitable to start communications.
//           The preferred CommMgr is the local one.
//
//  PARAMS :
//      - INPUT : None
//
//      - OUTPUT :
//          - pCommMgrNodeInstId:
//
// RETURNS :
//      - TRUE :  Success
//      - FALSE : Failure
//
//   NOTES : To evenly distribute the load, when the local instance is not alive,
//           the last returned CommMgr is saved and the searching starts from the
//           next one.
//      

BOOL    ServiceTrxMgr_GetCommMgr (DWORD * pCommMgrNodeInstId)
{
  DWORD             _idx;
  static DWORD      _idx_start;

  // Default return value
  * pCommMgrNodeInstId = IPC_NODE_UNKNOWN;

  if ( GLB_CommMgrList.num_ipc_services == 0 )
  {
    return FALSE;
  }

  if ( ServiceTrxMgr_GetAutoQuery (&GLB_AutoQueryStatus) == FALSE )
  {
    ServiceLogError (__FUNCTION__, _T("ServiceTrxMgr_GetAutoQuery"), FALSE);

    return FALSE;
  }

  if ( GLB_LocalCommMgrIndex != NO_LOCAL_COMM_MGR )
  {
    // Return the local one if it is alive
    if ( GLB_AutoQueryStatus.wdog_node_status[GLB_LocalCommMgrIndex] == WDOG_NODE_STATUS_OK )
    {
      * pCommMgrNodeInstId = GLB_CommMgrList.ipc_service[GLB_LocalCommMgrIndex].node_id; 

      return TRUE;
    } // if
  }

  // Start seeking at the next CommMgr, try to distribute the load
  _idx_start = (_idx_start + 1) % GLB_CommMgrList.num_ipc_services;

  // Returns the first CommMgr that is alive
  // Note that the loop starts seeking at _idx_start
  for ( _idx = 0; _idx < GLB_CommMgrList.num_ipc_services; _idx++ )
  {
    if ( GLB_AutoQueryStatus.wdog_node_status[_idx_start] == WDOG_NODE_STATUS_OK )
    {
      * pCommMgrNodeInstId = GLB_CommMgrList.ipc_service[_idx_start].node_id; 

      return TRUE;
    } // if
    _idx_start = (_idx_start + 1) % GLB_CommMgrList.num_ipc_services;
  } // for

  return FALSE;

} // ServiceTrxMgr_GetCommMgr

//------------------------------------------------------------------------------
// PURPOSE : Returns a CommMgr node instance id suitable to start communications.
//           The preferred CommMgr is the local one.
//
//  PARAMS :
//      - INPUT : None
//
//      - OUTPUT :
//          - pCommMgrNodeInstId:
//
// RETURNS :
//      - TRUE :  Success
//      - FALSE : Failure
//
//   NOTES : To evenly distribute the load, when the local instance is not alive,
//           the last returned CommMgr is saved and the searching starts from the
//           next one.
//      

BOOL    ServiceTrxMgr_GetAllCommMgr (DWORD * pNumCommMgr, DWORD * pCommMgrNodeInstId)
{
  DWORD _idx;
  DWORD _count;

  // Default return value
  * pNumCommMgr = 0;

  if ( GLB_CommMgrList.num_ipc_services == 0 )
  {
    return FALSE;
  }

  if ( ServiceTrxMgr_GetAutoQuery (&GLB_AutoQueryStatus) == FALSE )
  {
    ServiceLogError (__FUNCTION__, _T("ServiceTrxMgr_GetAutoQuery"), FALSE);

    return FALSE;
  }

  // Returns the first CommMgr that is alive
  // Note that the loop starts seeking at _idx_start
  _count = 0;
  for ( _idx = 0; _idx < GLB_CommMgrList.num_ipc_services; _idx++ )
  {
    if ( GLB_AutoQueryStatus.wdog_node_status[_idx] == WDOG_NODE_STATUS_OK )
    {
      pCommMgrNodeInstId[_count] = GLB_CommMgrList.ipc_service[_idx].node_id; 
      _count++;
    } // if
  } // for

  * pNumCommMgr = _count;

  return TRUE;

} // ServiceTrxMgr_GetAllCommMgr


//------------------------------------------------------------------------------
// PURPOSE : 
//
//  PARAMS :
//      - INPUT : None
//
//      - OUTPUT :
//          - pNumDeadTrxMgr:    Number of dead TrxMgr's
//          - pDeadTrxMgrLockId: The WatchdogId of the dead ones.
//
// RETURNS :
//      - TRUE :  Success
//      - FALSE : Failure
//
//   NOTES : pDeadTrxMgrLockId must be dimensioned to MAX_IPC_SERVICES
//      

BOOL    ServiceTrxMgr_GetDeadTrxMgr (DWORD * pNumDeadTrxMgr, DWORD * pDeadTrxMgrLockId)
{
  DWORD   _idx_auto_query;
  DWORD   _idx_dead;
  DWORD   _idx_trx_mgr;

  // Default return value
  * pNumDeadTrxMgr = 0;

  if ( GLB_TrxMgrList.num_ipc_services == 0 )
  {
    return FALSE;
  }

  if ( ServiceTrxMgr_GetAutoQuery (&GLB_AutoQueryStatus) == FALSE )
  {
    ServiceLogError (__FUNCTION__, _T("ServiceTrxMgr_GetAutoQuery"), FALSE);

    return FALSE;
  }

  _idx_dead = 0;
  _idx_auto_query = GLB_CommMgrList.num_ipc_services;
  for ( _idx_trx_mgr = 0; _idx_trx_mgr < GLB_TrxMgrList.num_ipc_services; _idx_trx_mgr++ )
  {
    if ( GLB_AutoQueryStatus.wdog_node_status[_idx_auto_query] == WDOG_NODE_STATUS_NOT_RESPONDING )
    {
      pDeadTrxMgrLockId[_idx_dead] = GLB_AutoQueryStatus.wdog_node_id[_idx_auto_query];
      _idx_dead = _idx_dead + 1;
    }
    _idx_auto_query = _idx_auto_query + 1;
  }

  // NIR 27-02-03
  * pNumDeadTrxMgr = _idx_dead;

  return TRUE;

} // ServiceTrxMgr_GetDeadTrxMgr

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Initializes the list of services. All with the same base name.
//
//  PARAMS :
//      - INPUT :
//          - pBaseNodeName : IPC node name
//
//      - OUTPUT :
//
// RETURNS :
//      - TRUE :  Success
//      - FALSE : Failure
//
//   NOTES : This function fails if there are not any node with the desired base name.

static BOOL     ServiceTrxMgr_InitIpcServiceList (TCHAR                 * pBaseNodeName, 
                                                  TYPE_IPC_SERVICE_LIST * pIpcServiceList)
{
  BOOL      _bool_rc;
  TCHAR     _node_name [IPC_NODE_NAME_LENGTH + 1];
  TCHAR     _wdog_name [IPC_NODE_NAME_LENGTH + 1];
  DWORD     _idx_node;
  DWORD     _idx_service;
  // Node & Wdog Instances
  DWORD     _num_node_instances;
  DWORD     _num_wdog_instances;
  DWORD     _node_cfg_inst_id [IPC_MAX_INSTANCES_PER_NODE];
  DWORD     _node_inst_id [IPC_MAX_INSTANCES_PER_NODE];
  BOOL      _node_is_local_inst [IPC_MAX_INSTANCES_PER_NODE];

  DWORD     _wdog_cfg_inst_id [IPC_MAX_INSTANCES_PER_NODE];
  DWORD     _wdog_inst_id [IPC_MAX_INSTANCES_PER_NODE];
  BOOL      _wdog_is_local_inst [IPC_MAX_INSTANCES_PER_NODE];

  // Reset the structure
  memset (pIpcServiceList, 0, sizeof (TYPE_IPC_SERVICE_LIST));

  _idx_service = 0;
  for ( _idx_node = 0; _idx_node < MAX_IPC_SERVICES; _idx_node++ )
  {
    IpcBuildNodeName (pBaseNodeName, _idx_node, _node_name);
    IpcBuildWatchdogNodeName (_node_name, _wdog_name);

    _bool_rc = TRUE;
    _bool_rc = _bool_rc && Common_IpcGetNodeInstanceIdList (_node_name, 
                                                            &_num_node_instances,
                                                            _node_cfg_inst_id,
                                                            _node_inst_id, 
                                                            _node_is_local_inst);

    _bool_rc = _bool_rc && Common_IpcGetNodeInstanceIdList (_wdog_name, 
                                                            &_num_wdog_instances,
                                                            _wdog_cfg_inst_id, 
                                                            _wdog_inst_id, 
                                                            _wdog_is_local_inst);

    if ( _bool_rc == TRUE )
    {
      // Node found ...
      if ( (_num_node_instances == 1) && (_num_wdog_instances == 1) )
      {
        pIpcServiceList->ipc_service[_idx_service].index    = _node_cfg_inst_id[0];
        pIpcServiceList->ipc_service[_idx_service].is_local = _node_is_local_inst[0];
        pIpcServiceList->ipc_service[_idx_service].node_id  = _node_inst_id[0];
        pIpcServiceList->ipc_service[_idx_service].wdog_id  = _wdog_inst_id[0];
        _idx_service = _idx_service + 1;  
      }
      else
      {
        ServiceLogUnexpected (__FUNCTION__, _node_name, _num_node_instances);
        ServiceLogUnexpected (__FUNCTION__, _wdog_name, _num_wdog_instances);
      }
    } // if
  } // for (_idx_node)
    
  // Set the number of IPC Services
  pIpcServiceList->num_ipc_services = _idx_service;

  if ( _idx_service == 0 )
  {
    return FALSE;
  }

  return TRUE;

} // ServiceTrxMgr_InitIpcServiceList

//------------------------------------------------------------------------------
// PURPOSE : Initializes the WatchDog API to perform the AutoQuery over the 
//           list of CommMgr's and TrxMgr's.
//
//  PARAMS :
//      - INPUT :
//          - pTrxMgrList :
//          - pCommMgrList :
//
//      - OUTPUT :
//
// RETURNS :
//      - TRUE :  Success
//      - FALSE : Failure
//
//   NOTES :

static BOOL     ServiceTrxMgr_SetAutoQuery  (TYPE_IPC_SERVICE_LIST * pCommMgrList, 
                                             TYPE_IPC_SERVICE_LIST * pTrxMgrList)
{
  WORD              _api_rc;
  DWORD             _idx_service;
  DWORD             _idx_auto_query;
  API_INPUT_PARAMS  _api_input_params;
  API_OUTPUT_PARAMS _api_output_params;
  WDOG_IUD_SET_AUTO_QUERY_STATUS _wdog_iud_set_auto_query_status;
  
  //  INPUT:
  //    - API
  _api_input_params.control_block = sizeof (API_INPUT_PARAMS);
  _api_input_params.event_object  = NULL;
  _api_input_params.function_code = WDOG_CODE_SET_AUTO_QUERY_STATUS;
  _api_input_params.mode          = API_MODE_SYNC;
  _api_input_params.sub_function_code = 0;
  //    - WDOG
  memset (&_wdog_iud_set_auto_query_status, 0, sizeof (WDOG_IUD_SET_AUTO_QUERY_STATUS));
  _wdog_iud_set_auto_query_status.control_block = sizeof (WDOG_IUD_SET_AUTO_QUERY_STATUS);

  // Reset the Index
  _idx_auto_query = 0;
  // Loop over the CommMgr's
  for ( _idx_service = 0; _idx_service < pCommMgrList->num_ipc_services; _idx_service++ )
  {
    _wdog_iud_set_auto_query_status.wdog_node_id[_idx_auto_query] = pCommMgrList->ipc_service[_idx_service].wdog_id;    
    _idx_auto_query = _idx_auto_query + 1;
  } // for

  // Loop over the TrxMgr's
  for ( _idx_service = 0; _idx_service < pTrxMgrList->num_ipc_services; _idx_service++ )
  {
    _wdog_iud_set_auto_query_status.wdog_node_id[_idx_auto_query] = pTrxMgrList->ipc_service[_idx_service].wdog_id;    
    _idx_auto_query = _idx_auto_query + 1;
  } // for

  _wdog_iud_set_auto_query_status.wdog_count = _idx_auto_query;

  //  OUTPUT:
  _api_output_params.control_block = sizeof (API_OUTPUT_PARAMS);

  _api_rc = Wdog_API (&_api_input_params, 
                      &_wdog_iud_set_auto_query_status, 
                      &_api_output_params,
                      NULL);

  if ( _api_rc == API_STATUS_OK )
  {
    if ( _api_output_params.output_1 == WDOG_STATUS_OK )
    {
      return TRUE;    
    }
    else
    {
      ServiceLogError (__FUNCTION__, _T("Wdog_API"), _api_output_params.output_1);
    }
  }
  else
  {
    ServiceLogError (__FUNCTION__, _T("Wdog_API"), _api_rc);
  }

  return FALSE;

} // ServiceTrxMgr_SetAutoQuery


//------------------------------------------------------------------------------
// PURPOSE : Gets the AutoQueryStatus
//
//  PARAMS :
//      - INPUT : None
//
//      - OUTPUT :
//          - pAutoQueryStatus:
//
// RETURNS :
//      - TRUE :  Success
//      - FALSE : Failure
//
//   NOTES : 

static BOOL     ServiceTrxMgr_GetAutoQuery (WDOG_OUD_GET_AUTO_QUERY_STATUS * pAutoQueryStatus)
{
  WORD                _api_rc;
  API_INPUT_PARAMS    _api_input_params;
  API_OUTPUT_PARAMS   _api_output_params;

  // INPUT:
  //  - API
  _api_input_params.control_block = sizeof (API_INPUT_PARAMS);
  _api_input_params.event_object  = NULL;
  _api_input_params.function_code = WDOG_CODE_GET_AUTO_QUERY_STATUS;
  _api_input_params.mode          = API_MODE_SYNC;
  _api_input_params.sub_function_code = 0;
  // OUTPUT:
  //  - API
  _api_output_params.control_block = sizeof (API_OUTPUT_PARAMS);
  //  - WDOG
  pAutoQueryStatus->control_block = sizeof (WDOG_OUD_GET_AUTO_QUERY_STATUS);

  _api_rc = Wdog_API (&_api_input_params,     // API_INPUT_PARAMS
                      NULL,                   // API_INPUT_USER_DATA
                      &_api_output_params,    // API_OUTPUT_PARAMS
                      pAutoQueryStatus);      // API_OUTPUT_USER_DATA

  if ( _api_rc == API_STATUS_OK )
  {
    if ( _api_output_params.output_1 == WDOG_STATUS_OK )
    {
      return TRUE;
    }

    // LogError
    ServiceLogUnexpected (__FUNCTION__, _T("Output1"), _api_output_params.output_1);

    return FALSE;
  }

  // LogError
  ServiceLogError (__FUNCTION__, _T("Wdog_API"), _api_output_params.output_1);  

  return FALSE;

} // ServiceTrxMgr_GetAutoQuery


//------------------------------------------------------------------------------
// PURPOSE : Gets the TrxType
//
//  PARAMS :
//      - INPUT:  None
//      - OUTPUT: None
//
// RETURNS :
//      - TrxType, Success
//      - 0xFF,    No TrxMgr
//
//   NOTES : 

BYTE ServiceTrxMgr_GetTrxType (void)
{
  return GLB_TrxType;

} // ServiceTrxMgr_GetTrxType



