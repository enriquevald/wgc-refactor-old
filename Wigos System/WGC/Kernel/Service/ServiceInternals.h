//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Service.h
//
//   DESCRIPTION: Constants, types, variables definitions and prototypes 
//                for generic Services
//
//        AUTHOR: Andreu Juli�
//
// CREATION DATE: 12-MAR-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 12-MAR-2002 AJQ    Initial draft.
// 17-MAY-2002 AJQ    ????
//------------------------------------------------------------------------------

#ifndef __SERVICE_INTERNALS_H
#define __SERVICE_INTERNALS_H

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC CONSTANTS 
//------------------------------------------------------------------------------
#define WDOG_SUFIX            _T("_WDOG")

//------------------------------------------------------------------------------
//  PUBLIC DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------
BOOL        ServiceInit (TYPE_SERVICE_DATA  * pServiceData, 
                         BOOL               OnlyLanguage = FALSE);

BOOL        ServiceDone (TYPE_SERVICE_DATA  * pServiceData, 
                         BOOL               Log = TRUE);

BOOL        LicenseInit (TCHAR  * pLicensePath,
                         DWORD  ClientId);

BOOL        CheckLicense (TYPE_LICENSE  * pLicense,
                          DWORD         ClientId);

BOOL        Watchdog_Stop (VOID);

void        GetServiceTrxCmdName (TCHAR *pBuffer,
                                  WORD TrxCmd);

#endif // __SERVICE_INTERNALS_H
