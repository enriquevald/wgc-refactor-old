//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Service.c
// 
//   DESCRIPTION: Functions and Methods to Install/Remove/Debug a Service
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 12-MAR-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 12-MAR-2002 AJQ    Initial draft.
// 22-MAY-2002 AJQ    NLS messages.
// 15-JUL-2002 AJQ    Watchdog
// 30-JUL-2002 AJQ    Control Version
// 05-AUG-2002 AJQ    ServiceTrace
// 22-FEB-2002 AJQ    Now uses the Get/SetCurrentDirectory
//
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_MISC
#define INCLUDE_QUEUE_API
#define INCLUDE_IPC_API
#define INCLUDE_PRIVATE_LOG
#define INCLUDE_LICENSE
#define INCLUDE_ALARMS
#define INCLUDE_SERVICE
#define INCLUDE_NLS_API
#include "CommonDef.h"

#include "ServiceInternals.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------
#define SERVICE_NAME            _T("SERVICE")
#define NULL_CHARACTER          _T('\0')
#define MODULE_NAME_LEN         128

// Commands
#define CMD_INSTALL             _T("install")
#define CMD_REMOVE              _T("remove")
#define CMD_VERBOSE             _T("verbose")
#define CMD_HELP                _T("help")
#define CMD_PREFIX_1            _T("-")
#define CMD_PREFIX_2            _T("/")

#define WAIT_SERVICE_STOP       1000   // Wait to stop the service (Verbose)
#define DEFAULT_WAIT_HINT       1000   // Wait hint
#define START_WAIT_HINT         60000  // Wait hint to star the service
#define STOP_WAIT_HINT          60000  // Wait hint to stop the service

#define SERVICE_DEPENDENCIES    _T("") // Service dependencies

#define ERROR_MESSAGE_LEN       256    // Max length for an error message

#define CLIENT_NUM_DIGITS       2      // Number of digits of client number

#define ERROR_SERVICE_ALREADY_EXISTS 1073L

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------
typedef enum
{
  SERVICE_DISPATCH = 0, // Dispatch the service (Default)
  SERVICE_INSTALL  = 1, // Install the service
  SERVICE_REMOVE   = 2, // Remove the service
  SERVICE_VERBOSE  = 3, // Run the service as a console application
  SERVICE_HELP     = 4, // Display the help

} SERVICE_COMMAND;

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------
static DWORD                  GLB_ServiceCurrentState;
static SERVICE_STATUS_HANDLE  GLB_ServiceStatusHandle;
static TYPE_SERVICE_DATA      GLB_ServiceData;
static BOOL                   GLB_Verbose = FALSE;
static TCHAR                  GLB_ServiceAlias [CLIENT_NUM_DIGITS + 1];
// Backup the old Exception Filter
static LPTOP_LEVEL_EXCEPTION_FILTER GLB_OldExceptionFilter;
static  WORD                  GLB_ClientId = 0;

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------
// Main
void        main (int NumArgs, char ** pArgList);

// Function Handlers
void WINAPI ServiceMain  (DWORD NumArgs, LPTSTR * pArgList);

void WINAPI ServiceCtrl  (DWORD CtrlCode);

BOOL WINAPI CtrlHandler  (DWORD CtrlCode); // Console Control Handler (Debug)

// Exception Handler ...
LONG WINAPI ServiceExceptionHandler (EXCEPTION_POINTERS * pExceptionPointers);

// Status
void        ServiceReportStatus (DWORD  CurrentState, 
                                 DWORD  ErrorCode = 0);

// Start routine
void        ServiceStart (void);

// Commands
void        InstallService (TCHAR   * pAccount, 
                            TCHAR   * pPassword);
                            
void        RemoveService (void);

void        VerboseService (void);

void        HelpService (void);

void        DispatchService (void);

// Command Line
SERVICE_COMMAND ParseCommandLine (int NumArgs, TCHAR ** pArgList,
                                  TCHAR ** pAccount, TCHAR ** pPassword);
// Error
TCHAR * GetLastErrorText (void);
// Application Name
TCHAR * GetApplicationName (void);
TCHAR * GetApplicationPath (void);

// client number format
// BOOL CheckClientNumberFormat (WORD NumArguments, TCHAR * pClientNumber);
BOOL GetServiceAlias (TCHAR * pName, TCHAR * pDisplayName);

// ServiceData
BOOL ServiceDataInit (TYPE_SERVICE_DATA * pServiceData);
BOOL ServiceDataDone (TYPE_SERVICE_DATA * pServiceData);

static void LogException (EXCEPTION_POINTERS * pExceptionPointers);
static BOOL GetAddressDetails  (void    * pAddress, 
                                TCHAR   ModuleName [_MAX_PATH], 
                                DWORD   * pModuleAllocationBase,
                                DWORD   * pOffset, 
                                char    SectionName [IMAGE_SIZEOF_SHORT_NAME+1]);

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// PURPOSE: Service Entry Point
//
//  PARAMS:
//      - INPUT:
//          - NumArgs:  number of command line arguments
//          - pArgList: array of command line arguments
//
//      - OUTPUT:
//
//  RETURNS:
//    
//
//    NOTES:
//   Processes the command line and then
//   calls StartServiceCtrlDispatcher to register the
//   main service thread.  When the this call returns,
//   the service has stopped, so exit.
//
void main (int NumArgs, char ** pArgList)
{
  TCHAR               _function_name [] = _T("main");
  int                 _argc;
  LPTSTR              * p_argv;
  TCHAR               * p_account;
  TCHAR               * p_password;
  SERVICE_COMMAND     _service_cmd;

#ifdef UNICODE
  p_argv = CommandLineToArgvW (GetCommandLineW (), &(_argc));
#else
  _argc  = NumArgs;
  p_argv = pArgList;
#endif

  _service_cmd = ParseCommandLine (_argc, p_argv, &p_account, &p_password);

  switch ( _service_cmd )
  {
    case SERVICE_INSTALL:
      InstallService (p_account,p_password);
    break;

    case SERVICE_REMOVE:
      RemoveService ();
    break;

    case SERVICE_VERBOSE:
      VerboseService ();
    break;

    case SERVICE_HELP:
      HelpService ();
    break;

    case SERVICE_DISPATCH: // Fall through
    default:
      DispatchService ();
    break;
  } // switch

#ifdef UNICODE
  if ( p_argv != NULL )
  {
    GlobalFree (p_argv);
  }
#endif

  exit (EXIT_SUCCESS);

  return;
} // main

//-----------------------------------------------------------------------------
// PURPOSE: Parse the command line
//
//  PARAMS:
//      - INPUT:
//          - NumArgs:  number of command line arguments
//          - pArgList: array of command line arguments
//
//      - OUTPUT:
//          - pAccount: a pointer to the account name
//          - pPassword: a pointer to the account password
//
//  RETURNS: The command to perform
//
//    NOTES:

static SERVICE_COMMAND ParseCommandLine (int NumArgs, TCHAR ** pArgList,
                                         TCHAR ** pAccount, TCHAR ** pPassword)
{
  TCHAR _function_name [] = _T("ParseCommandLine");
  TCHAR * p_cmd;

  if ( NumArgs > 1 && ( * pArgList[1] == CMD_PREFIX_1[0] || * pArgList[1] == CMD_PREFIX_2[0]) )
  {
    if ( ServiceInit (&GLB_ServiceData, TRUE) == FALSE )
    {
      ServiceLogError (_function_name, _T("ServiceInit"), FALSE, LOG_TO_PRIVATE);
      exit(0);
    }

    p_cmd = pArgList[1] + 1;

    if ( _tcsicmp (p_cmd, CMD_INSTALL) == 0 )
    {
      * pAccount  = NULL;
      * pPassword = NULL;
        
      if ( NumArgs >= 3 )
      {
        * pAccount = pArgList[2];
        
        if ( NumArgs >= 4 )
        {
          * pPassword = pArgList[3];
        } // if
      } // if

      return SERVICE_INSTALL;
    } // if

    if ( _tcsicmp (p_cmd, CMD_REMOVE) == 0 )
    {
      return SERVICE_REMOVE;
    } // if
    
    if ( _tcsicmp (p_cmd, CMD_VERBOSE) == 0 )
    {
      return SERVICE_VERBOSE;
    } // if
    
    if ( _tcsicmp (p_cmd, CMD_HELP) == 0 )
    {
      return SERVICE_HELP;
    } // if
  } // if

  return SERVICE_DISPATCH;
} // ParseCommandLine

//-----------------------------------------------------------------------------
// PURPOSE: ServiceMain Function Handler
//
//  PARAMS:
//      - INPUT:
//          - NumArgs:  number of command line arguments
//          - pArgList: array of command line arguments
//
//      - OUTPUT: 
//
//  RETURNS: 
//
//    NOTES: Simply calls the ServiceStart routine

static void WINAPI ServiceMain (DWORD dwArgc, LPTSTR *lpszArgv)
{
  TCHAR _function_name [] = _T("ServiceMain");

  // Register the Service Control handler
  //   The service status handle does not have to be closed.
  GLB_ServiceStatusHandle = RegisterServiceCtrlHandler (GLB_Service.name, // Service Name
                                                        ServiceCtrl);     // Service Control Handler

  if ( GLB_ServiceStatusHandle == NULL )
  {
    ServiceLogError (_function_name ,_T(""), GetLastError(), LOG_TO_PRIVATE);

    return;
  }

  ServiceStart ();

  return;
} // ServiceMain

//-----------------------------------------------------------------------------
// PURPOSE: ServiceCtrl Function Handler
//
//  PARAMS:
//      - INPUT:
//    CtrlCode: The Control Code
//
//      - OUTPUT:
//
//  RETURNS:
//
//    NOTES:

static void WINAPI ServiceCtrl (DWORD CtrlCode)
{
  switch ( CtrlCode )
  {
    // Stop the service.
    case SERVICE_CONTROL_STOP:
    case SERVICE_CONTROL_SHUTDOWN:
      ServiceStop ();
    break;

    default:
    break;
  }
 
  // Update Service Status
  ServiceReportStatus (GLB_ServiceCurrentState);

  return;
} // ServiceCtrl

//-----------------------------------------------------------------------------
// PURPOSE: CtrlHandler Function Handler, handle the console events
//
// PARAMS:
//      - INPUT:
//    CtrlCode: The Control Code
//
//      - OUTPUT:
//    
//
// RETURNS:
//
//    NOTES:

static BOOL WINAPI CtrlHandler (DWORD CtrlCode)
{
  switch ( CtrlCode )
  {
    case CTRL_BREAK_EVENT:  // use Ctrl+C or Ctrl+Break to simulate
    case CTRL_C_EVENT:      // SERVICE_CONTROL_STOP in debug mode
      ServiceStop ();

      return TRUE;
    break;

    default:
    break;
  }

  return FALSE;
} // CtrlHandler

//-----------------------------------------------------------------------------
// PURPOSE: Starts the service. Calls the following specific routines
//          of the service:
//            - p_init, can be used to initialize your service
//            - p_main, the main routine
//            - p_stop, can be used to cleanup your service
//
// PARAMS:
//      - INPUT:  
//      - OUTPUT: 
//
// RETURNS:   
//
//    NOTES:

static void ServiceStart (void)
{
  TCHAR       _function_name [] = _T("ServiceStart");
  TCHAR       _path [_MAX_PATH];
  TCHAR       _computer [NLS_MSG_PARAM_MAX_LENGTH + 1]; 
  TCHAR       _msg [NLS_MSG_STRING_MAX_LENGTH + 1];
  WORD        _rc;
  
  ServiceReportStatus (SERVICE_START_PENDING);

  // Set the Exception Handler
  GLB_OldExceptionFilter = SetUnhandledExceptionFilter (ServiceExceptionHandler);

  if ( GLB_Verbose == FALSE ) 
  {
    // Set the working directory to the installation path
    SetCurrentDirectory (GetApplicationPath ());
  }
  else
  {
    GetCurrentDirectory (_MAX_PATH, _path);
    NLS_GetString (NLS_ID_SERVICES(309), _msg); 
    _tcscat (_msg, _T("\n"));
    ServiceTrace (_msg, _path);
  }

  if ( ServiceDataInit (&GLB_ServiceData) == FALSE )
  {
    ServiceLogError (_function_name, _T("ServiceDataInit"), FALSE, LOG_TO_PRIVATE);
    ServiceReportStatus (SERVICE_STOPPED, 100000);

    return;
  }

  ServiceReportStatus (SERVICE_START_PENDING);

  if ( ServiceInit (&GLB_ServiceData) == FALSE )
  {
    ServiceLogError (_function_name, _T("ServiceInit"), FALSE, LOG_TO_PRIVATE);
    ServiceReportStatus (SERVICE_STOPPED, 100000);
    ServiceDone (&GLB_ServiceData, FALSE);

    return;
  }

  ServiceReportStatus (SERVICE_START_PENDING);

  if ( GLB_Service.p_init != NULL )
  {
    if ( GLB_Service.p_init (&GLB_ServiceData) == FALSE )
    {
      ServiceLogError (_function_name, _T("GLB_Service.p_init"), FALSE, LOG_TO_PRIVATE);
      ServiceReportStatus (SERVICE_STOPPED, 100000);
      ServiceDone (&GLB_ServiceData, FALSE);

      return;      
    }
  }

  // Now the service is running
  ServiceReportStatus (SERVICE_RUNNING);

  // Notify that the service is running ...
  //  - Get Computer Name
  Common_GetComputerName (_computer, NLS_MSG_PARAM_MAX_LENGTH);
  //  - Build the msg string
  _rc = NLS_GetString (NLS_ID_SERVICES(2), _msg, GLB_Service.display_name);
  if ( _rc != NLS_STATUS_OK )
  {
    ServiceLogError(_function_name, _T("NLS_GetString"), _rc, LOG_TO_BOTH);
  }
  else
  {
    ServiceLogMsg (_function_name, _msg, LOG_TO_BOTH);
  }

  // Event: Service Started
  Common_Alarm (ALARM_ID_EVENT, 0, 0, ALARM_PRIORITY_EVENT,
                TRUE, 
                NULL,
                NLS_ID_SERVICES(101), 
                GLB_Service.display_name, _computer, _T(""), _T(""), _T(""));
  
  if ( GLB_Service.p_main (&GLB_ServiceData) == FALSE )
  {
    ServiceLogError (_function_name, _T("GLB_Service.p_main"), FALSE, LOG_TO_PRIVATE);
    ServiceReportStatus (SERVICE_STOPPED, 100000);
    ServiceDone (&GLB_ServiceData, FALSE);

    return;      
  }

  // Event: Service Stopped
  Common_Alarm (ALARM_ID_EVENT, 0, 0, ALARM_PRIORITY_EVENT,
                TRUE,
                NULL, 
                NLS_ID_SERVICES(102), 
                GLB_Service.display_name, _computer, _T(""), _T(""), _T(""));

  //  - Build the msg string
  _rc = NLS_GetString (NLS_ID_SERVICES(3), _msg, GLB_Service.display_name);
  if ( _rc != NLS_STATUS_OK )
  {
    ServiceLogError(_function_name, _T("NLS_GetString"), _rc, LOG_TO_BOTH);
  }
  else
  {
    ServiceLogMsg (_function_name, _msg, LOG_TO_BOTH);
  }

  ServiceReportStatus (SERVICE_STOP_PENDING);

  // IPC
  if ( Common_Ipc (IPC_CODE_WAIT_SEND_QUEUE_EMPTY) == FALSE )
  {
    ServiceLogError (_function_name, _T("Common_Ipc"), FALSE, LOG_TO_PRIVATE);
  }
    
  if ( GLB_Service.p_stop != NULL )
  {
    if ( GLB_Service.p_stop (&GLB_ServiceData) == FALSE )
    {
      ServiceLogError (_function_name, _T("GLB_Service.p_stop"), FALSE, LOG_TO_PRIVATE);
      ServiceReportStatus (SERVICE_STOPPED, 100000);
      ServiceDone (&GLB_ServiceData, FALSE);

      return;      
    }
  }

  if ( ServiceDone (&GLB_ServiceData) == FALSE )
  {
    ServiceLogError (_function_name, _T("ServiceDone"), FALSE, LOG_TO_PRIVATE);
  }

  if ( ServiceDataDone (&GLB_ServiceData) == FALSE )
  {
    ServiceLogError (_function_name, _T("ServiceDataDone"), FALSE, LOG_TO_PRIVATE);
  }

  ServiceReportStatus (SERVICE_STOPPED);

  if ( GLB_Verbose == TRUE )
  {
    NLS_GetString (NLS_ID_SERVICES(314), _msg, GLB_Service.display_name);
    _tprintf (_T("%s\n"), _msg);
  }
} // ServiceStart

//-----------------------------------------------------------------------------
// PURPOSE: Sets the current status of the service and
//          reports it to the Service Control Manager
//
// PARAMS:
//      - INPUT:
//    - CurrentState: The service state
//    - ErrorCode:
//
//      - OUTPUT: 
//
// RETURNS: 
//
//    NOTES:
//
static void ServiceReportStatus (DWORD CurrentState, DWORD ErrorCode)
{
  TCHAR           _function_name [] = _T("ServiceReportStatus");
  SERVICE_STATUS  _status;
  DWORD           _controls_accepted;
  DWORD           _wait_hint;

  static DWORD    _check_point = 0;

  // Store the current state
  GLB_ServiceCurrentState = CurrentState;

  if ( GLB_Verbose == TRUE )
  {
    // Debug: we don't report to the SCM
    return;
  }

  _check_point = _check_point + 1;
  _controls_accepted = SERVICE_ACCEPT_STOP;
  _wait_hint = DEFAULT_WAIT_HINT;

  switch ( CurrentState )
  {
    case SERVICE_STOPPED:
      _check_point = 0;
    break;

    case SERVICE_START_PENDING:
      _controls_accepted = 0;
      _wait_hint = START_WAIT_HINT;
    break;

    case SERVICE_STOP_PENDING:
      _wait_hint = STOP_WAIT_HINT;
    break;

    case SERVICE_RUNNING:
      _check_point = 0;
    break;

    default:
    break;
  }

  _status.dwCheckPoint               = _check_point;
  _status.dwControlsAccepted         = _controls_accepted; 
  _status.dwCurrentState             = CurrentState; 
  _status.dwServiceSpecificExitCode  = ErrorCode;
  _status.dwServiceType              = SERVICE_WIN32_OWN_PROCESS;
  _status.dwWaitHint                 = _wait_hint;
  _status.dwWin32ExitCode            = ErrorCode;

  // Report the status of the service to the service control manager.
  if ( SetServiceStatus (GLB_ServiceStatusHandle, &_status) == FALSE ) 
  {
    //AddToMessageLog(NULL,0,EVENTLOG_ERROR_TYPE,SC_SYSTEM,
    //EVLG_STD_SETSERVICESTATUS_FAILED);
    ServiceLogError (_function_name, _T("SetServiceStatus"), GetLastError(), LOG_TO_PRIVATE);

    return;
  }
} // ServiceReportStatus

//-----------------------------------------------------------------------------
// PURPOSE: Dispatches the service
//
// PARAMS:
//      - INPUT:  
//      - OUTPUT: 
//
// RETURNS:   
//
//    NOTES:
//
static void DispatchService (void)
{
  TCHAR               _function_name [] = _T("DispatchService");
  SERVICE_TABLE_ENTRY _dispatch_table [2];

  // Initialize the dispatch table
  _dispatch_table[0].lpServiceName = GLB_Service.name;
  _dispatch_table[0].lpServiceProc = ServiceMain;
  _dispatch_table[1].lpServiceName = NULL;
  _dispatch_table[1].lpServiceProc = NULL;
  
  if ( StartServiceCtrlDispatcher (_dispatch_table) == FALSE )
  {
    // Set Language ...
    if ( ServiceInit (&GLB_ServiceData, TRUE) == FALSE )
    {
      ServiceLogError (_function_name, _T("ServiceInit"), FALSE, LOG_TO_PRIVATE);
    }
    else
    {
      ServiceLogError (_function_name, _T("StartServiceCtrlDispatcher"), GetLastError(), LOG_TO_PRIVATE);
    }
    // Display the help ...
    HelpService ();
  }

  return;
} // DispatchService

//-----------------------------------------------------------------------------
// PURPOSE: Installs the service
//
// PARAMS:
//      - INPUT:  
//      - OUTPUT: 
//
// RETURNS:   
//
//    NOTES:
//
static void InstallService (TCHAR * pAccount, TCHAR * pPassword)
{
  TCHAR                 _function_name [] = _T("InstallService");
  SC_HANDLE             _service;
  SC_HANDLE             _manager;
  TCHAR                 _path [_MAX_PATH];
  TCHAR                 _msg [NLS_MSG_STRING_MAX_LENGTH + 1];
  DWORD                 _error;
  TCHAR                 _service_name [SERVICE_NAME_LEN + 1];
  TCHAR                 _service_display_name [SERVICE_NAME_LEN + 1];
  
  if ( GetServiceAlias (_service_name, _service_display_name) == FALSE )
  {
    ServiceLogError (_function_name, _T(""), FALSE, LOG_TO_PRIVATE);
    NLS_GetString (NLS_ID_SERVICES(303), _msg, GLB_Service.display_name); 
    _tprintf (_T("%s\n"), _msg);

    return;
  }

  if ( GetModuleFileName (NULL, _path, _MAX_PATH) == 0 )
  {
    ServiceLogError (_function_name, _T("GetModuleFileName"), GetLastError(), LOG_TO_PRIVATE);
    NLS_GetString (NLS_ID_SERVICES(303), _msg, _service_display_name); 
    _tprintf (_T("%s\n"), _msg);

    return;
  }

  _manager = OpenSCManager (NULL,                   // machine (NULL == local)
                            NULL,                   // database (NULL == default)
                            SC_MANAGER_ALL_ACCESS); // access required

  if ( _manager == NULL )
  {
    ServiceLogError (_function_name, _T("OpenSCManager"), GetLastError(), LOG_TO_PRIVATE);
    NLS_GetString (NLS_ID_SERVICES(303), _msg, _service_display_name); 
    _tprintf (_T("%s\n"), _msg);

    return;
  }

  _service = CreateService (_manager,                   // SCManager database
                            _service_name,              // name of service
                            _service_display_name,      // name to display
                            SERVICE_ALL_ACCESS,         // desired access
                            SERVICE_WIN32_OWN_PROCESS,  // service type
                            SERVICE_DEMAND_START,       // start type
                            SERVICE_ERROR_NORMAL,       // error control type
                            _path,                      // service's binary
                            NULL,                       // no load ordering group
                            NULL,                       // no tag identifier
                            SERVICE_DEPENDENCIES,       // dependencies
                            pAccount,                   // Administrator account
                            pPassword);                 // password

  if ( _service )
  {
    NLS_GetString (NLS_ID_SERVICES(301), _msg, _service_display_name); 
    _tprintf (_T("%s\n"), _msg);

    if ( CloseServiceHandle (_service) == FALSE )
    {
      ServiceLogError (_function_name,_T("CloseServiceHandle"), GetLastError(), LOG_TO_PRIVATE);
    }
    // AddEventSource();
    // AddParametersRegistry (TRUE, 0);
  }
  else
  {
    _error = GetLastError();
    ServiceLogError (_function_name,_T("CreateService"), _error, LOG_TO_PRIVATE);
    if ( _error == ERROR_SERVICE_ALREADY_EXISTS )
    {
      NLS_GetString (NLS_ID_SERVICES(302), _msg, _service_display_name); 
    }
    else
    {
      NLS_GetString (NLS_ID_SERVICES(303), _msg, _service_display_name); 
    }
    _tprintf (_T("%s\n"), _msg);
  }

  if ( CloseServiceHandle (_manager) == FALSE )
  {
    ServiceLogError (_function_name,_T("CloseServiceHandle"), GetLastError(), LOG_TO_PRIVATE);
  }

  return;
} // InstallService

//-----------------------------------------------------------------------------
// PURPOSE: Stops and removes the service
//
// PARAMS:
//      - INPUT:  
//      - OUTPUT: 
//
// RETURNS:   
//
//    NOTES:
//
static void RemoveService()
{
  TCHAR           _function_name [] = _T("RemoveService");
  SC_HANDLE       _service;
  SC_HANDLE       _manager;
  SERVICE_STATUS  _status;
  TCHAR           _msg [NLS_MSG_STRING_MAX_LENGTH + 1];
  TCHAR           _service_name [SERVICE_NAME_LEN + 1];
  TCHAR           _service_display_name [SERVICE_NAME_LEN + 1];

  if ( GetServiceAlias (_service_name, _service_display_name) == FALSE )
  {
    ServiceLogError (_function_name, _T(""), FALSE, LOG_TO_PRIVATE);
    NLS_GetString (NLS_ID_SERVICES(303), _msg, GLB_Service.display_name); 
    _tprintf (_T("%s\n"), _msg);

    return;
  }

  _manager = OpenSCManager (NULL,                   // machine (NULL == local)
                            NULL,                   // database (NULL == default)
                            SC_MANAGER_ALL_ACCESS); // access required

  if ( _manager == NULL )
  {
    ServiceLogError(_function_name, _T("OpenSCManager"), GetLastError(), LOG_TO_PRIVATE);
    NLS_GetString (NLS_ID_SERVICES(306), _msg, _service_display_name); 
    _tprintf (_T("%s\n"), _msg);

    return;
  }

  _service = OpenService (_manager,            // SCManager
                          _service_name,       // Service Name
                          SERVICE_ALL_ACCESS); // access required

  if ( _service == NULL )
  {
    NLS_GetString (NLS_ID_SERVICES(305), _msg, _service_display_name); 
    _tprintf (_T("%s\n"), _msg);

    ServiceLogError(_function_name, _T("OpenService"), GetLastError(), LOG_TO_PRIVATE);
    if ( CloseServiceHandle (_manager) == FALSE )
    {
      ServiceLogError(_function_name, _T("CloseServiceHandle"), GetLastError(), LOG_TO_PRIVATE);
    }
  
    return;
  }

  if ( ControlService (_service, SERVICE_CONTROL_STOP, &_status) )
  {
    NLS_GetString (NLS_ID_SERVICES(308), _msg, _service_display_name);
    _tprintf (_T("%s\n"), _msg);

    while ( QueryServiceStatus (_service, &_status) )
    {
      if ( _status.dwCurrentState == SERVICE_STOP_PENDING )
      {
        _tprintf (_T("."));
        Sleep ( WAIT_SERVICE_STOP );
      }
      else
      {
          break;
      }
    } // while

    if ( _status.dwCurrentState == SERVICE_STOPPED )
    {
      NLS_GetString (NLS_ID_SERVICES(314), _msg, _service_display_name);
      _tprintf (_T("\n%s\n"), _msg);
    }
    else
    {
      NLS_GetString (NLS_ID_SERVICES(313), _msg, _service_display_name);
      _tprintf (_T("\n%s\n"), _msg);
    }
  }

  if ( DeleteService (_service) )
  {
    NLS_GetString (NLS_ID_SERVICES(304), _msg, _service_display_name); 
    _tprintf (_T("%s\n"), _msg);
  }
  else
  {
    ServiceLogError (_function_name, _T("DeleteService"), GetLastError(), LOG_TO_PRIVATE);
    NLS_GetString (NLS_ID_SERVICES(306), _msg, _service_display_name); 
    _tprintf (_T("%s\n"), _msg);
  }

  // Close handles
  if ( CloseServiceHandle (_service) == FALSE )
  {
    ServiceLogError (_function_name, _T("CloseServiceHandle"), GetLastError(), LOG_TO_PRIVATE);
  }
  if ( CloseServiceHandle (_manager) == FALSE )
  {
    ServiceLogError (_function_name, _T("CloseServiceHandle"), GetLastError(), LOG_TO_PRIVATE);
  }

  return;
} // RemoveService

//-----------------------------------------------------------------------------
// PURPOSE: Runs the service as a console application, the debug flag is set.
//
// PARAMS:
//      - INPUT:  
//      - OUTPUT: 
//
// RETURNS:   
//
//    NOTES:
//
static void VerboseService (void)
{
  TCHAR   _function_name [] = _T("VerboseService");
  TCHAR   _msg [NLS_MSG_STRING_MAX_LENGTH + 1];

  // Activate the debug flag
  GLB_Verbose = TRUE;

  NLS_GetString (NLS_ID_SERVICES(307), _msg, GLB_Service.display_name);
  ServiceTrace (_T("%s\n"), _msg);

  // Register the Console Control Handler
  if ( SetConsoleCtrlHandler (CtrlHandler, TRUE) == FALSE )
  {
    ServiceLogError (_function_name, _T("SetConsoleCtrlHandler"), GetLastError(), LOG_TO_PRIVATE);

    return;
  }

  ServiceStart ();

  return;
} // VerboseService

//-----------------------------------------------------------------------------
// PURPOSE: Sets the stop flag and the stop event.
//
// PARAMS:
//      - INPUT:  
//      - OUTPUT: 
//
// RETURNS:   
//
//    NOTES:
//
void ServiceStop (void)
{
  TCHAR   _function_name [] = _T("ServiceStop");
  TCHAR   _msg [NLS_MSG_STRING_MAX_LENGTH + 1];

  if ( GLB_Verbose == TRUE )
  {
    NLS_GetString (NLS_ID_SERVICES(308), _msg, GLB_Service.display_name);
    _tprintf (_T("%s\n"), _msg);
  }

  // Notify that the service is stoping to the SCManager 
  ServiceReportStatus (SERVICE_STOP_PENDING);

  // Activate the stop flag
  GLB_ServiceData.flags.stop = TRUE;

  // Set the stop event
  if ( SetEvent (GLB_ServiceData.stop_event) == FALSE )
  {
    ServiceLogError (_function_name, _T("SetEvent"), GetLastError(), LOG_TO_PRIVATE);

    // The Service will not be notified ... so abort it!!
    abort ();

    // This return is never reached!!!
    return;
  }

  // AJQ 24-FEB-2003 Stop the Watchdog now!
  Watchdog_Stop ();

  return;
} // ServiceStop

//-----------------------------------------------------------------------------
// PURPOSE: Displays the service help
//
// PARAMS:
//      - INPUT:
//    
//
//      - OUTPUT:
//    
//
// RETURNS:
//
//    NOTES:

static void HelpService (void)
{
  TCHAR       * p_app_name;
  TCHAR       _msg [NLS_MSG_STRING_MAX_LENGTH + 1];

  p_app_name = GetApplicationName ();

  // HEADER
  NLS_GetString (NLS_ID_SERVICES(201), _msg, GLB_Service.display_name); 
  _tprintf (_T("\n"));
  _tprintf (_T("%s\n"), _msg); // ** AYUDA **
  _tprintf (_T("\n"));

  // PRIVATE HELP
  if ( GLB_Service.p_help != NULL )
  {
    _tprintf (_T("%s\n"), GLB_Service.p_help);
    _tprintf (_T("\n"));
  }

  // HELP IT SELF
  NLS_GetString (NLS_ID_SERVICES(202), _msg, p_app_name, CMD_PREFIX_1, CMD_INSTALL); 
  _tprintf (_T("%s\n"), _msg); 
  NLS_GetString (NLS_ID_SERVICES(203), _msg, p_app_name, CMD_PREFIX_1, CMD_REMOVE); 
  _tprintf (_T("%s\n"), _msg); 
  NLS_GetString (NLS_ID_SERVICES(204), _msg, p_app_name, CMD_PREFIX_1, CMD_VERBOSE); 
  _tprintf (_T("%s\n"), _msg); 
  NLS_GetString (NLS_ID_SERVICES(205), _msg, p_app_name, CMD_PREFIX_1, CMD_HELP); 
  _tprintf (_T("%s\n"), _msg); 
  _tprintf (_T("\n"));

  return;
} // HelpService

//-----------------------------------------------------------------------------
// PURPOSE: Returns the error message string
//
// PARAMS:
//      - INPUT:  
//      - OUTPUT: 
//
// RETURNS:
//  The error message string
//
//    NOTES:
//
static TCHAR * GetLastErrorText (void)
{
  DWORD         _err;
  DWORD         _size;
  static TCHAR  _error_message [ERROR_MESSAGE_LEN];

  _err  = GetLastError ();

  _size = FormatMessage (FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ARGUMENT_ARRAY,
                         NULL,
                         _err,
                         LANG_NEUTRAL,
                         _error_message,
                         ERROR_MESSAGE_LEN,
                         NULL );

  if ( (_size == 0) || (ERROR_MESSAGE_LEN < (_size + 14)) )
  {
    _stprintf (_error_message, _T("ERROR: 0x%x"), _err);
  }
  else
  {
    _error_message[lstrlen(_error_message)-2] = NULL_CHARACTER; // Remove Cr/Lf
    _stprintf (_error_message, _T("%s (0x%x)"), _error_message, _err);
  }

  return _error_message;
} // GetLastErrorText

//-----------------------------------------------------------------------------
// PURPOSE: Returns the application name
//
// PARAMS:
//      - INPUT:  
//      - OUTPUT: 
//
// RETURNS:
//  The application name
//
//    NOTES:
//
static TCHAR * GetApplicationName (void)
{
  static TCHAR  _fname [_MAX_FNAME];
  TCHAR         _path [_MAX_PATH];

  // Obtain executable path
  if ( GetModuleFileName (NULL, _path, _MAX_PATH) == 0 )
  {
    _stprintf (_fname, SERVICE_NAME);
  }
  else
  {
    _tsplitpath (_path, NULL, NULL, _fname, NULL);
  }

  return _fname;
} // GetApplicationName

//-----------------------------------------------------------------------------
// PURPOSE: Write to the private log. The name of the service is also printed.
//
// PARAMS:
//      - INPUT:  
//      - OUTPUT: 
//
// RETURNS:
//  The application name
//
//    NOTES:
//
void ServicePrivateLog (TCHAR * pFunctionName, TCHAR * pCalledFunction)
{
  TCHAR _module_name [MODULE_NAME_LEN + 1];
 
  _stprintf (_module_name, _T("%.*s"), MODULE_NAME_LEN, GLB_Service.name );

  PrivateLog (2, 
              _module_name, 
              pFunctionName, 
              GetLastErrorText () , 
              pCalledFunction); 

  return;
} // ServicePrivateLog


//-----------------------------------------------------------------------------
// PURPOSE: Returns the application path
//
// PARAMS:
//      - INPUT:  
//      - OUTPUT: 
//
// RETURNS:
//  The application name
//
//    NOTES:
//
static TCHAR * GetApplicationPath (void)
{
  static TCHAR  _path [_MAX_PATH];
  TCHAR         _drive[_MAX_DRIVE];
  TCHAR         _dir[_MAX_DIR];

  // Obtain executable path
  if ( GetModuleFileName (NULL, _path, _MAX_PATH) == 0 )
  {
    GetCurrentDirectory (_MAX_PATH, _path);
  }
  else
  {  
    _tsplitpath (_path, _drive, _dir, NULL, NULL);
    _tmakepath (_path, _drive, _dir, "", "");
  }
 
  return _path;
} // GetApplicationPath

//-----------------------------------------------------------------------------
// PURPOSE: Handle the unexpected exceptions
//
// PARAMS:
//      - INPUT:  
//      - OUTPUT: 
//
// RETURNS:
//    EXCEPTION_EXECUTE_HANDLER
//
//    NOTES:
//
static LONG WINAPI ServiceExceptionHandler (EXCEPTION_POINTERS * pExceptionPointers)
{
  // Restore the old exception filter 
  // any excepctions produced here arae catched by old exeception filter,
  // avoiding recursive calls
  SetUnhandledExceptionFilter (GLB_OldExceptionFilter);  

  // Signal the Event Stop
  ServiceStop ();

  // Try to log the Exception
  LogException (pExceptionPointers);

  // Clean up the Service ...
  ServiceDone (&GLB_ServiceData, FALSE);

  return EXCEPTION_EXECUTE_HANDLER;
}

//-----------------------------------------------------------------------------
// PURPOSE: Initializes the service data
//
// PARAMS:
//      - INPUT:
//    - pServiceData:
//
//      - OUTPUT:
//
// RETURNS:
//    TRUE on success
//    FALSE otherwise
//
//    NOTES:
//
static BOOL ServiceDataInit (TYPE_SERVICE_DATA * pServiceData)
{
  TCHAR   _function_name [] = _T("ServiceDataInit");

  // Initialize the Service Data
  pServiceData->control_block = sizeof (TYPE_SERVICE_DATA);
  pServiceData->flags.debug   = FALSE;
  pServiceData->flags.error   = FALSE;
  pServiceData->flags.stop    = FALSE;
  pServiceData->ipc_node_instance_id = IPC_NODE_UNKNOWN;
  pServiceData->p_user_data   = NULL;
  pServiceData->sql_context   = NULL;
  pServiceData->stop_event    = CreateEvent (NULL,  // Attributes
                                             TRUE,  // Manual Reset
                                             FALSE, // Inital State
                                             NULL); // Name
  
  if ( pServiceData->stop_event == NULL )
  {
    ServiceLogError (_function_name, _T("CreateEvent"), GetLastError(), LOG_TO_PRIVATE);

    return FALSE;
  }

  return TRUE;
} // ServiceDataInit

//-----------------------------------------------------------------------------
// PURPOSE: Release any service data
//
//  PARAMS:
//      - INPUT:
//          - pServiceData
//
//      - OUTPUT: 
//
// RETURNS:
//    TRUE on success
//    FALSE otherwise
//
//    NOTES:

static BOOL ServiceDataDone (TYPE_SERVICE_DATA * pServiceData)
{
  TCHAR   _function_name [] = _T("ServiceDataDone");
  BOOL    _error;

  _error = FALSE;

  if ( pServiceData->sql_context != NULL )
  {

    // *** TODO *** ACC 

    //////////_rc = Common_ContextFree (&pServiceData->sql_context);
    //////////if ( _rc != COMMON_OK )
    //////////{
    //////////  ServiceLogError (_function_name,_T("Common_ContextFree"), _rc, LOG_TO_BOTH);    
    //////////  _error = TRUE;
    //////////}
  };

  if ( pServiceData->stop_event != NULL )
  {
    if ( CloseHandle (pServiceData->stop_event) == FALSE )
    {
      ServiceLogError (_function_name,_T("CloseHandle"), GetLastError(), LOG_TO_BOTH);    
      _error = TRUE;
    }
  }

  if ( _error == TRUE )
  {
    return FALSE;
  }
  
  return TRUE;
} // ServiceDataDone

//-----------------------------------------------------------------------------
// PURPOSE: Builds the service name and the display name
//
// PARAMS:
//      - INPUT:
//
//      - OUTPUT:
//
// RETURNS:
//    TRUE on success
//    FALSE otherwise
//
//    NOTES:
//  
//
static BOOL GetServiceAlias (TCHAR * pName, TCHAR * pDisplayName)
{
  TCHAR             _function_name [] = _T("GetServiceNames");
  TCHAR             _name [NLS_MSG_STRING_MAX_LENGTH + 1];
  WORD              _rc;
  DWORD             _client_id;
  TCHAR             _path [_MAX_PATH];
  TYPE_MODULE_ABOUT _module_about;

  if ( GetCurrentDirectory (_MAX_PATH, _path) == 0 )
  {
    ServiceLogError (__FUNCTION__, _T("GetCurrentDirectory"), errno, LOG_TO_PRIVATE);

    return FALSE;
  }
  
  _module_about.control_block = sizeof (_module_about);
  _rc = Version_CheckModuleVersion (GLB_Service.name, _path, &_module_about);
  if ( _rc != VERSION_STATUS_OK )
  {
    ServiceLogError (__FUNCTION__, _T("Version_CheckModuleVersion"), _rc, LOG_TO_BOTH);

    return FALSE;
  }

  GLB_ServiceData.client_id = (WORD) _module_about.client_id;

  _client_id = GLB_ServiceData.client_id;

  // Build the Client Id
  _stprintf (GLB_ServiceAlias, _T("%02d"), _client_id);

  // AJQ 04-APR-2003, Use the IpcNodeName to register the service, 
  //                  instead of the GLB_Service.name
  // _rc = NLS_GetString (NLS_ID_SERVICES(1), _name, GLB_ServiceAlias, GLB_Service.name);
  _rc = NLS_GetString (NLS_ID_SERVICES(1), _name, GLB_ServiceAlias, GLB_Service.ipc_node_name);

  if ( _rc == NLS_STATUS_OK )
  {
    _stprintf (pName, _T("%.*s"), SERVICE_NAME_LEN, _name);
  }
  else
  {
    ServiceLogError (_function_name, _T("NLS_GetString"), _rc, LOG_TO_PRIVATE);
    NLS_GetString(NLS_ID_SERVICES(306), _name, GLB_Service.display_name);
    ServiceLogMsg (_function_name, _name, LOG_TO_PRIVATE);
    _tprintf(_T("%s\n"), _name);

    return FALSE;
  }

  _rc = NLS_GetString (NLS_ID_SERVICES(1), _name, GLB_ServiceAlias, GLB_Service.display_name);
  if ( _rc == NLS_STATUS_OK )
  {
    _stprintf (pDisplayName, _T("%.*s"), SERVICE_NAME_LEN, _name);
  }
  else
  {
    ServiceLogError (_function_name, _T("NLS_GetString"), _rc, LOG_TO_PRIVATE);
    NLS_GetString(NLS_ID_SERVICES(306), _name, GLB_Service.display_name);
    ServiceLogMsg (_function_name, _name, LOG_TO_PRIVATE);
    _tprintf(_T("%s\n"), _name);

    return FALSE;
  }

  return TRUE;
} // GetServiceAlias

//-----------------------------------------------------------------------------
// PURPOSE: Prints a message to the screen only in debug mode.
//          (like printf).
//
// PARAMS:
//      - INPUT: pFormat - The format string
//
//      - OUTPUT:
//
// RETURNS:
//  
//    NOTES:
//  AJQ, 5-AUG-2002, Changed to use VarArgs

void      ServiceTrace (const TCHAR * pFormat, ...)
{
  if ( GLB_Verbose == TRUE )
  {    
    // Debug mode - print message to the screen
    va_list _arg_list;

    { 
      SYSTEMTIME  _localtime;
      
      GetLocalTime (&_localtime);

      DWORD _ticks;
      _ticks  = GetTickCount ();
      _tprintf (_T("%02u:%02u:%02u.%03u "), _localtime.wHour, 
                                            _localtime.wMinute,
                                            _localtime.wSecond,
                                            _localtime.wMilliseconds);
    }
    va_start (_arg_list, pFormat); 
    _vtprintf (pFormat, _arg_list);
    va_end (_arg_list);
    fflush (stdout);
  }

  return;
} // ServiceTrace

//------------------------------------------------------------------------------
// PURPOSE: Gets the TrxMgr IPC node instance Id
//
//  PARAMS:
//      - INPUT:  None
//      - OUTPUT: None
//
// RETURNS:
//      - ipc_node_instance_id
//
//   NOTES: 

DWORD ServiceTrxMgr_GetIpcNodeInstId (void)
{
  return GLB_ServiceData.ipc_node_instance_id;

} // ServiceTrxMgr_GetIpcNodeInstId



//------------------------------------------------------------------------------
// PURPOSE: Log all the available information of an Exception
//    
// PARAMS:
//  - INPUT:  
//      - pExceptionPointers
//
// RETURNS:
//
// NOTES:
//
static void LogException (EXCEPTION_POINTERS * pExceptionPointers)
{
  TYPE_LOGGER_PARAM   _param1;
  TYPE_LOGGER_PARAM   _param2;
  TYPE_LOGGER_PARAM   _param3;
  TCHAR               _module_name [_MAX_PATH];
  DWORD               _module_base;
  TCHAR               _fname [_MAX_PATH];
  TCHAR               _fext  [_MAX_PATH];
  DWORD               _offset;
  char                _section_name [IMAGE_SIZEOF_SHORT_NAME+1];
  BOOL                _available;
  TCHAR               _msg [512];

  // Clean parameters
  ZeroMemory (_param1, sizeof (_param1));
  ZeroMemory (_param2, sizeof (_param2));
  ZeroMemory (_param3, sizeof (_param3));

  // Build and send the Log's Message
  _stprintf (_param1, _T("Code: 0x%08X"),    pExceptionPointers->ExceptionRecord->ExceptionCode);
  _stprintf (_param2, _T("Flags: 0x%08X"),   pExceptionPointers->ExceptionRecord->ExceptionFlags);
  _stprintf (_param3, _T("Address: 0x%08X"), pExceptionPointers->ExceptionRecord->ExceptionAddress);
  _stprintf (_msg,    _T("*** EXCEPTION *** ThreadId: %lu %s %s %s"), 
                      GetCurrentThreadId (),
                      _param1,
                      _param2,
                      _param3);
  ServiceLogMsg (_T(__FUNCTION__), _msg);

  if ( pExceptionPointers->ExceptionRecord->ExceptionCode == EXCEPTION_ACCESS_VIOLATION )
  {
    // Clean parameters
    ZeroMemory (_param1, sizeof (_param1));
    ZeroMemory (_param2, sizeof (_param2));
    ZeroMemory (_param3, sizeof (_param3));

    // Build and send the Log's Message
    _tcscpy (_param1, _T("Access Violation")); 
    if ( pExceptionPointers->ExceptionRecord->NumberParameters > 0 )
    {
      if ( pExceptionPointers->ExceptionRecord->ExceptionInformation[0] == 0 )
      {
        _tcscpy (_param2, _T("Reading")); 
      }
      else
      {
        _tcscpy (_param2, _T("Writting")); 
      }
      if ( pExceptionPointers->ExceptionRecord->NumberParameters > 1 )
      {
        _stprintf (_param3, _T("Data: 0x%08X"), pExceptionPointers->ExceptionRecord->ExceptionInformation[1]);
      }
    }
    _stprintf (_msg,    _T("*** EXCEPTION *** ThreadId: %lu %s %s %s"), 
                        GetCurrentThreadId (),
                        _param1,
                        _param2,
                        _param3);
    ServiceLogMsg (_T(__FUNCTION__), _msg);
  } // if Access Violation

  Common_LogThreadInfo (GetCurrentThreadId ());

  _available = GetAddressDetails (pExceptionPointers->ExceptionRecord->ExceptionAddress,
                                  _module_name,
                                  &_module_base,
                                  &_offset,
                                  _section_name);
  if ( _available )
  {
    // Clean parameters
    ZeroMemory (_param1, sizeof (_param1));
    ZeroMemory (_param2, sizeof (_param2));
    ZeroMemory (_param3, sizeof (_param3));

    _tsplitpath (_module_name, NULL, NULL, _fname, _fext);
    _tmakepath  (_module_name, NULL, NULL, _fname, _fext);
    _stprintf (_param1, _T("Address: 0x%08X"), pExceptionPointers->ExceptionRecord->ExceptionAddress);
    _stprintf (_param2, _T("0x%08X:%s"), _module_base, _module_name);
    _stprintf (_param3, _T("Section: [%hs]:0x%08X"), _section_name, _offset);
    _stprintf (_msg,    _T("*** EXCEPTION *** ThreadId: %lu %s %s %s"), 
                        GetCurrentThreadId (),
                        _param1,
                        _param2,
                        _param3);
    ServiceLogMsg (_T(__FUNCTION__), _msg);
  }

} // LogException


//------------------------------------------------------------------------------
// PURPOSE: Obtain all the information available at runtime for a given 
//           process address
//
// PARAMS:
//  - INPUT:  
//      - pAddress: address
//
//  - OUTPUT: 
//      - ModuleName
//      - pOffset
//      - pSectionName
//
// RETURNS:
//    - TRUE, when details are available
//    - FALSE, otherwise
//
// NOTES:
//

static BOOL GetAddressDetails  (void    * pAddress, 
                                TCHAR   ModuleName [_MAX_PATH], 
                                DWORD   * pModuleAllocationBase,
                                DWORD   * pOffset, 
                                char    SectionName [IMAGE_SIZEOF_SHORT_NAME+1])
{
  // Memory
  MEMORY_BASIC_INFORMATION  _mem_basic_info;
  // Module
  HMODULE                   _module;
  // Image
  PIMAGE_DOS_HEADER         p_image_DOS_header;
  PIMAGE_NT_HEADERS         p_image_NT_header;
  PIMAGE_SECTION_HEADER     p_image_section;
  //
  DWORD                     _idx_section;
  DWORD                     _section_start;
  DWORD                     _section_end;
  //  
  DWORD                     _RVA_addr;

  // Reset output parameters
  ZeroMemory (ModuleName,  sizeof (ModuleName));
  ZeroMemory (SectionName, sizeof (ModuleName));
  * pModuleAllocationBase = 0;

  // First thing to do is we need to get the base address in memory (i.e. a HINSTANCE) from pAddress
  if ( !VirtualQuery (pAddress, &_mem_basic_info, sizeof (MEMORY_BASIC_INFORMATION)) )
  {
    return FALSE;
  }

  // Check that _mem_basic_info.AllocationBase is valid
  if ( _mem_basic_info.State == MEM_FREE )
  {
    return FALSE;
  }

  // From the HINSTANCE we can get the module name
  _module = (HMODULE) _mem_basic_info.AllocationBase;

  * pModuleAllocationBase = (DWORD) _mem_basic_info.AllocationBase;

  if ( !GetModuleFileName (_module, ModuleName, _MAX_PATH) )
  {
    return FALSE;
  }

  // Now do our in memory traversal of the PE image to loop across the sections in that image
  p_image_DOS_header = (PIMAGE_DOS_HEADER) _module;
  p_image_NT_header  = (PIMAGE_NT_HEADERS) (((BYTE*)p_image_DOS_header) + p_image_DOS_header->e_lfanew);
  p_image_section    = IMAGE_FIRST_SECTION (p_image_NT_header);

  // Express the "pAddress" parameter as an "RVA"
  _RVA_addr = (DWORD) ((BYTE*)pAddress - (BYTE*)_mem_basic_info.AllocationBase); 

  // Loop across the sections to find the one which contains our faulting address
  for ( _idx_section = 0; _idx_section < p_image_NT_header->FileHeader.NumberOfSections; _idx_section )
  {
    _section_start = p_image_section->VirtualAddress;
    _section_end   = _section_start + max(p_image_section->SizeOfRawData, p_image_section->Misc.VirtualSize);

    // Is the "pAddress" parameter (expressed as a RVA) inside this section?
    if (   (_RVA_addr >= _section_start) 
        && (_RVA_addr <= _section_end) )
    {
      * pOffset = _RVA_addr - _section_start;
      CopyMemory (SectionName, p_image_section->Name, IMAGE_SIZEOF_SHORT_NAME);
      SectionName[IMAGE_SIZEOF_SHORT_NAME] = NULL; //Don't forget to NULL terminate the Section name

      return TRUE;
    }

    // Next section
    p_image_section++;
  }

  //Got here, then return failure
  return FALSE;

} // GetAddressDetails

