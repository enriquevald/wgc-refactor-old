//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : Service_Misc.cpp
//
//   DESCRIPTION : Functions and Methods to Install/Remove/Debug a Service
//
//        AUTHOR : Andreu Juli�
//
// CREATION DATE : 17-MAY-2002
// 
// REVISION HISTORY :
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-MAY-2002 AJQ    Initial draft.
// 01-AUG-2002 CIR    Added License checking functions calling.
// 22-FEB-2002 AJQ    Now uses the Get/SetCurrentDirectory
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_MISC
#define INCLUDE_LICENSE
#define INCLUDE_SERVICE
#define INCLUDE_IPC_API
#define INCLUDE_NLS_API
#define INCLUDE_PRIVATE_LOG
#define INCLUDE_QUEUE_API
#define INCLUDE_WATCHDOG_API
#define INCLUDE_VERSION_CONTROL
#include "CommonDef.h"

#include "ServiceInternals.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------
#define SERVICE_DB_USER_NAME           _T("LKROOT")

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------
extern BOOL      GLB_Verbose;

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------
static BOOL Watchdog_Init (TCHAR * pNodeName, DWORD * pWdogId);

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PURPOSE : Logs a message
//
//  PARAMS :
//      - INPUT :
//          - pFunctionName: The current function name
//          - pMessage:      The message to be logged
//          - LogTarget: The target for logging (try to always use LOG_TO_DEFAULT)
//
//  RETURNS :
//
//   NOTES :

void        ServiceLogMsg (TCHAR     * pFunctionName,
                           TCHAR     * pMessage,
                           ENUM_LOG  LogTarget)
{
  TCHAR * p_param_1;
  TCHAR * p_param_2;
  TCHAR * p_param_3;
  TCHAR _emtpy [] = _T("");

  if ( (LogTarget & LOG_TO_PRIVATE) == LOG_TO_PRIVATE )
  {
    p_param_1 = pMessage;
    p_param_2 = _emtpy;
    p_param_3 = _emtpy;
    if ( _tcslen (p_param_1) > PRIVATELOG_SIZE_PARAM )
    {
      p_param_2 = &p_param_1[PRIVATELOG_SIZE_PARAM];
      if ( _tcslen (p_param_2) > PRIVATELOG_SIZE_PARAM )
      {
        p_param_3 = &p_param_2[PRIVATELOG_SIZE_PARAM];
      }
    }

    PrivateLog (5, GLB_Service.name, pFunctionName, p_param_1, p_param_2, p_param_3);
  }

  if ( (LogTarget & LOG_TO_LOGGER) == LOG_TO_LOGGER )
  {
    p_param_1 = pMessage;
    p_param_2 = _emtpy;
    p_param_3 = _emtpy;
    if ( _tcslen (p_param_1) > NLS_MSG_PARAM_MAX_LENGTH )
    {
      p_param_2 = &p_param_1[NLS_MSG_PARAM_MAX_LENGTH];
      if ( _tcslen (p_param_2) > NLS_MSG_PARAM_MAX_LENGTH )
      {
        p_param_3 = &p_param_2[NLS_MSG_PARAM_MAX_LENGTH];
      }
    }

    Common_LoggerMsg (NLS_ID_LOGGER(8),     // Message Id
                      GLB_Service.name,     // Module Name
                      pFunctionName,        // Function Name
                      p_param_1,            // Text
                      p_param_2,
                      p_param_3);
  }
  
  return;
} // ServiceLogMsg

//-----------------------------------------------------------------------------
// PURPOSE : Logs an error of control block
//
//  PARAMS :
//      - INPUT :
//          - pFunctionName : The current function name
//          - pCalledFunctionName : The called function name
//          - ErrorCode : The error code
//          - LogTarget : The target for logging (try to always use LOG_TO_DEFAULT)
//
//  RETURNS :
//
//   NOTES :

void        ServiceLogError (TCHAR      * pFunctionName,
                             TCHAR      * pCalledFunctionName,
                             DWORD      ErrorCode,
                             ENUM_LOG   LogTarget)
{
  TCHAR _error_code [NLS_MSG_PARAM_MAX_LENGTH];

  if ( ErrorCode <= 9999 )
  {
    _stprintf (_error_code, _T("%lu"), ErrorCode);
  }
  else if ( ErrorCode <= 65535 )
  {
    _stprintf (_error_code, _T("0x%04X"), ErrorCode);
  }
  else
  {
    _stprintf (_error_code, _T("0x%08X"), ErrorCode);
  }

  if ( (LogTarget & LOG_TO_PRIVATE) == LOG_TO_PRIVATE )
  {
    PrivateLog (2, GLB_Service.name, pFunctionName, _error_code, pCalledFunctionName); 
  }

  if ( (LogTarget & LOG_TO_LOGGER) == LOG_TO_LOGGER )
  {
    Common_LoggerMsg (NLS_ID_LOGGER(2),     // Message Id
                      GLB_Service.name,     // Module Name
                      pFunctionName,        // Function Name
                      _error_code,          // Text
                      pCalledFunctionName); // Called Function Name          
  } 
} // ServiceLogError

//-----------------------------------------------------------------------------
// PURPOSE : Logs an error of control block
//
//  PARAMS :
//      - INPUT :
//    - pFunctionName: The current function name
//    - ExpectedValue: The expected value
//    - ReceivedValue: The received value
//    - LogTarget:     The target for logging (try to always use LOG_TO_DEFAULT)
//
//  RETURNS :
//
//   NOTES :

void        ServiceLogWrongSize (TCHAR      * pFunctionName,
                                 DWORD      ExpectedValue,
                                 DWORD      ReceivedValue,
                                 ENUM_LOG   LogTarget)
{
  TCHAR _expected [NLS_MSG_PARAM_MAX_LENGTH];
  TCHAR _received [NLS_MSG_PARAM_MAX_LENGTH];

  _stprintf (_expected, _T("%lu"), ExpectedValue);
  _stprintf (_received, _T("%lu"), ReceivedValue);

  if ( (LogTarget & LOG_TO_PRIVATE) == LOG_TO_PRIVATE )
  {
    PrivateLog (3, GLB_Service.name, pFunctionName, _T("CONTROL_BLOCK"), _expected, _received);  
  }

  if ( (LogTarget & LOG_TO_LOGGER) == LOG_TO_LOGGER )
  {
    Common_LoggerMsg (NLS_ID_LOGGER(3),     // Message Id
                      GLB_Service.name, // Module Name
                      pFunctionName,        // Function Name
                      _T("CONTROL_BLOCK"),  // Error
                      _expected,            // Expected Value
                      _received);           // Received Value
  }
} // ServiceLogWrongSize

//-----------------------------------------------------------------------------
// PURPOSE : Logs an unexpected value
//
//  PARAMS :
//      - INPUT :
//    - pFunctionName: The current function name
//    - pFieldName:    The field name
//    - Value:         The received value
//    - LogTarget:     The target for logging (try to always use LOG_TO_DEFAULT)
//
//  RETURNS :
//
//   NOTES :

void ServiceLogUnexpected (TCHAR    * pFunctionName,
                           TCHAR    * pFieldName,
                           DWORD    Value,
                           ENUM_LOG LogTarget)
{
  TCHAR _value [NLS_MSG_PARAM_MAX_LENGTH];

  if ( Value <= 9999 )
  {
    _stprintf (_value, _T("%lu"), Value);
  }
  else if ( Value <= 65535 )
  {
    _stprintf (_value, _T("0x%04X"), Value);
  }
  else
  {
    _stprintf (_value, _T("0x%08X"), Value);
  }

  if ( (LogTarget & LOG_TO_PRIVATE) == LOG_TO_PRIVATE )
  {
    PrivateLog (6, GLB_Service.name, pFunctionName, pFieldName, _value);         
  } 

  if ( (LogTarget & LOG_TO_LOGGER) == LOG_TO_LOGGER )
  {
    Common_LoggerMsg (NLS_ID_LOGGER(7),     // Message Id
                      GLB_Service.name, // Module Name
                      pFunctionName,        // Function Name
                      pFieldName,           // Field Name
                      _value);              // Unexpected Value
  } 
}

//-----------------------------------------------------------------------------
// PURPOSE : Logs the invalid license message ans stops the service
//
//  PARAMS :
//      - INPUT :
//    - LogTarget: The target for logging (Logger or privatelog)
//
//  RETURNS :
//
//   NOTES :

void        ServiceLogInvalidLicense (ENUM_LOG LogTarget)
{

  if ( (LogTarget & LOG_TO_PRIVATE) == LOG_TO_PRIVATE )
  {
    PrivateLog (9, GLB_Service.name, _T("CheckLicense"), GLB_Service.name);
  } 

  if ( (LogTarget & LOG_TO_LOGGER) == LOG_TO_LOGGER )
  {
    Common_LoggerMsg (NLS_ID_LOGGER(11),    // Message Id
                      GLB_Service.name,      // Service Name
                      _T("CheckLicense"),    // Check license Function Name
                      GLB_Service.name);     // Service name
  } 

  return;
} // ServiceLogInvalidLicense

//-----------------------------------------------------------------------------
// PURPOSE : Connects to the DB and initializes the IPC.
//
//  PARAMS :
//      - INPUT/OUTPUT :
//          - pServiceData: The Service Data
//
//  RETURNS :
//
//   NOTES :

BOOL        ServiceInit (TYPE_SERVICE_DATA  * pServiceData, 
                         BOOL               OnlyLanguage)
{
  TYPE_CENTER_CONFIGURATION _config;
  TYPE_MODULE_ABOUT _module_about;
  TCHAR             _connect_string [COMMON_DB_CONNECT_STRING_SIZE +1];
  TCHAR             _password [COMMON_DB_PASSWORD_SIZE + 1];
  TCHAR             _user [COMMON_DB_USER_SIZE + 1];
  DWORD             _rc;
  BOOL              _rc_bool;
  TCHAR             _path [_MAX_PATH];

  // Set DBUser - All Services have the same user name to connect DB
  _rc_bool = Common_SetDBUserName (SERVICE_DB_USER_NAME);
  if ( ! _rc_bool )
  {
    ServiceLogError (__FUNCTION__, _T("Common_SetDBUserName"), _rc_bool, LOG_TO_PRIVATE);
  }

  // Get the current configuration
  _config.control_block = sizeof (TYPE_CENTER_CONFIGURATION);
  _config.database.p_connect_string  = _connect_string;
  _config.database.p_password = _password;
  _config.database.p_user = _user;
  _rc = Common_GetConfiguration (&_config);
  if ( _rc != COMMON_OK )
  {
    ServiceLogError (__FUNCTION__, _T("Common_GetConfiguration"), _rc, LOG_TO_PRIVATE);

    return FALSE;
  }

  // Set the current language
  _rc = NLS_SetLanguage ((WORD)_config.language);
  if ( _rc != NLS_STATUS_OK )
  {
    ServiceLogError (__FUNCTION__, _T("NLS_SetLanguage"), _rc, LOG_TO_PRIVATE);

    return FALSE;
  }
  
  if ( OnlyLanguage == TRUE )
  {
    return TRUE;
  }

  // AJQ 14-FEB-2003 
  // Use the Working Directory instead of the Executable Path. This avoids the 
  // need of making a copy of the licence file and distribution file, to the 
  // Debug directory when debugging.

  if ( GetCurrentDirectory (_MAX_PATH, _path) == 0 )
  {
    ServiceLogError (__FUNCTION__, _T("GetCurrentDirectory"), errno, LOG_TO_PRIVATE);

    return FALSE;
  }
  
  // TJG 26-JAN-2004
  // Due that license includes the client id, the version checking must be made 
  // before the license verifying.
  _module_about.control_block = sizeof (_module_about);
  _rc = Version_CheckModuleVersion (GLB_Service.name, _path, &_module_about);
  if ( _rc != VERSION_STATUS_OK )
  {
    ServiceLogError (__FUNCTION__, _T("Version_CheckModuleVersion"), _rc, LOG_TO_BOTH);

    return FALSE;
  }

  pServiceData->client_id = (WORD) _module_about.client_id; 

  // Init license checking thread
  _rc_bool = LicenseInit (_path, _module_about.client_id);
  if ( ! _rc_bool )
  {
    // Thread function could not be started. Error message
    // has been written in specific function
    return FALSE;
  }

  if ( GLB_Service.flag_ipc == TRUE )
  {
    // IPC
    if ( Common_Ipc (IPC_CODE_MODULE_INIT) == FALSE )
    {
      ServiceLogError (__FUNCTION__, _T("Common_Ipc"), FALSE, LOG_TO_PRIVATE);

      return FALSE;
    } 

    // AJQ, 12-AUG-2002 Get the node name
    if ( ! Common_IpcGetLocalNodeInstanceId (GLB_Service.ipc_node_name,
                                             pServiceData->ipc_node_name,
                                             &pServiceData->ipc_node_index,
                                             &pServiceData->ipc_node_instance_id) )
    {
      ServiceLogError (__FUNCTION__, _T("Common_IpcGetLocalInstanceId"), FALSE, LOG_TO_PRIVATE);
      
      return FALSE;    
    } 

    // AJQ, 12-AUG-2002 Use the node name returned by the above routine
    if ( ! Common_IpcNodeInit (pServiceData->ipc_node_name, &pServiceData->ipc_node_instance_id, 0) )
    {
      ServiceLogError (__FUNCTION__, _T("Common_IpcNodeInit"), FALSE, LOG_TO_PRIVATE);

      return FALSE;
    }
  }
  
  // Watchdog
  // AJQ, 12-AUG-2002 Use the node name returned by the above routine
  if ( ! Watchdog_Init (pServiceData->ipc_node_name, &pServiceData->wdog_id) )
  {
    ServiceLogError (__FUNCTION__, _T("Watchdog_Init"), 0, LOG_TO_PRIVATE);

    return FALSE;
  }

  if ( GLB_Service.flag_database == TRUE )
  {
    GLB_Service.flag_database = FALSE;

    // *** TODO *** ACC

    //////////// Connect to the DB
    //////////_rc = Common_RetryConnect (&pServiceData->sql_context);

    //////////switch ( _rc )
    //////////{
    //////////  case COMMON_OK:
    //////////  break;

    //////////  default:
    //////////  {
    //////////    ServiceLogError (__FUNCTION__, _T("Common_RetryConnect"), _rc, LOG_TO_BOTH);
    //////////  
    //////////    if ( _rc == COMMON_ERROR_DB_FATAL )
    //////////    {
    //////////      // Avoid the disconnect ...
    //////////      GLB_Service.flag_database = FALSE;

    //////////      // Wrong TNS names, the service will never get connected. Stop it!
    //////////      return FALSE;
    //////////    }

    //////////    // Continue the Service will get connected later ...
    //////////  }
    //////////  break;
    //////////}
  }

  return TRUE;
} // ServiceInit

//-----------------------------------------------------------------------------
// PURPOSE : Disconnects from the DB and stops the IPC.
//
//  PARAMS :
//      - INPUT :
//    pServiceData: The Service Data
//
//      - OUTPUT :
//
//  RETURNS :
//
//   NOTES :

BOOL        ServiceDone (TYPE_SERVICE_DATA  * pServiceData, 
                         BOOL               Log)
{
  BOOL    _error;

  _error = FALSE;

  // Watchdog
  if ( ! Watchdog_Stop () )
  {
    if ( Log )
    {
      ServiceLogError (__FUNCTION__, _T("Watchdog_Stop"), 0, LOG_TO_PRIVATE);
    }

    _error = TRUE;
  }
  
  if ( GLB_Service.flag_database == TRUE )
  {
    // *** TODO *** ACC 

    ////////// Disconnet from DB
    ////////_rc = Common_Disconnect (pServiceData->sql_context);
    ////////if ( _rc != COMMON_OK )
    ////////{
    ////////  if ( Log )
    ////////  {
    ////////    ServiceLogError (__FUNCTION__, _T("Common_Disconnect"), _rc, LOG_TO_PRIVATE);
    ////////  }

    ////////  _error = TRUE;
    ////////}

    ////////_rc = Common_ContextFree (&pServiceData->sql_context);
    ////////if ( _rc != COMMON_OK )
    ////////{
    ////////  if ( Log )
    ////////  {
    ////////    ServiceLogError (__FUNCTION__, _T("Common_ContextFree"), _rc, LOG_TO_PRIVATE);
    ////////  }

    ////////  _error = TRUE;
    ////////}
  }

  // Always wait till the SendQueue is empty
  // IPC
  if ( ! Common_Ipc (IPC_CODE_WAIT_SEND_QUEUE_EMPTY) )
  {
    if ( Log )
    {
      ServiceLogError (__FUNCTION__, _T("Common_Ipc"), FALSE, LOG_TO_PRIVATE);
    }

    _error = TRUE;
  }

  if ( ! Common_Ipc (IPC_CODE_MODULE_STOP) )
  {
    if ( Log )
    {
      ServiceLogError (__FUNCTION__, _T("Common_Ipc"), FALSE, LOG_TO_PRIVATE);
    }

    _error = TRUE;
  }
  
  if ( _error == TRUE )
  {
    return FALSE;
  }

  return TRUE;
} // ServiceDone

//-----------------------------------------------------------------------------
// PURPOSE : Starts the Watchdog
//
//  PARAMS :
//      - INPUT :
//    pNodeName: The IPC Node Name (the sufix '_WDOG' is appended).
//
//      - OUTPUT :
//    pWdogId:   The Node Instance Id of the Watchdog
//
// RETURNS :
//    TRUE on success, 
//    FALSE otherwise
//
//   NOTES :

static BOOL     Watchdog_Init (TCHAR * pNodeName, DWORD * pWdogId)
{
  API_INPUT_PARAMS  _api_input_params;
  API_OUTPUT_PARAMS _api_output_params;
  WDOG_IUD_INIT     _wdog_iud_init;
  WDOG_OUD_INIT     _wdog_oud_init;
  WORD              _api_rc;

  // INPUT
  //  - API
  _api_input_params.control_block = sizeof (API_INPUT_PARAMS);
  _api_input_params.event_object  = NULL;
  _api_input_params.function_code = WDOG_CODE_INIT;
  _api_input_params.mode          = API_MODE_SYNC;
  _api_input_params.sub_function_code = 0;
  //  - WDOG
  _wdog_iud_init.control_block    = sizeof (WDOG_IUD_INIT);
  _stprintf (_wdog_iud_init.wdog_node_name, _T("%s%s"), pNodeName, WDOG_SUFIX);
  // OUTPUT
  _api_output_params.control_block = sizeof (API_OUTPUT_PARAMS);
  _wdog_oud_init.control_block     = sizeof (WDOG_OUD_INIT);

  // API Call
  _api_rc = Wdog_API (&_api_input_params, 
                      &_wdog_iud_init, 
                      &_api_output_params, 
                      &_wdog_oud_init);

  if ( _api_rc == API_STATUS_OK )
  {
    if ( _api_output_params.output_1 == WDOG_STATUS_OK )
    {
      * pWdogId = _wdog_oud_init.wdog_node_inst_id;

      return TRUE;
    }

    ServiceLogError (__FUNCTION__, _T("Wdog_API"), _api_output_params.output_1, LOG_TO_BOTH);
  } 
  else
  {
    ServiceLogError (__FUNCTION__, _T("Wdog_API"), _api_rc, LOG_TO_BOTH);
  }

  return FALSE;
} // Watchdog_Init 

//-----------------------------------------------------------------------------
// PURPOSE : Stops the Watchdog
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
//  RETURNS :
//      - TRUE on success, 
//      - FALSE otherwise
//
//   NOTES :

BOOL        Watchdog_Stop (void)
{
  API_INPUT_PARAMS  _api_input_params;
  API_OUTPUT_PARAMS _api_output_params;
  WORD              _api_rc;

  // INPUT
  //  - API
  _api_input_params.control_block     = sizeof (API_INPUT_PARAMS);
  _api_input_params.event_object      = NULL;
  _api_input_params.function_code     = WDOG_CODE_STOP;
  _api_input_params.mode              = API_MODE_SYNC;
  _api_input_params.sub_function_code = 0;
  // OUTPUT
  _api_output_params.control_block = sizeof (API_OUTPUT_PARAMS);

  // API Call
  _api_rc = Wdog_API (&_api_input_params, 
                      NULL, 
                      &_api_output_params, 
                      NULL);

  // Don't write to logger, may be the WDOG hasn't been initialized
  if ( _api_rc == API_STATUS_OK )
  {
    if ( _api_output_params.output_1 == WDOG_STATUS_OK )
    {
      return TRUE;
    }
  } 

  return FALSE;
} // Watchdog_Stop
