//------------------------------------------------------------------------------
// Copyright © 2004 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : Service_TrxMgr_Receive.cpp
//
//   DESCRIPTION : General Service Functions and Methods related to 
//                 messages reception processing
//
//        AUTHOR : Ronald Rodríguez T.
//
// CREATION DATE : 26-FEB-2004
// 
// REVISION HISTORY :
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 26-FEB-2004 RRT    Initial draft.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_MISC
#define INCLUDE_IPC_API
#define INCLUDE_NLS_API
#define INCLUDE_SERVICE
#define INCLUDE_LICENSE
#define INCLUDE_COMMUNICATIONS
#include "CommonDef.h"

#include "ServiceInternals.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PURPOSE: General Checking of a Message Receive from Communications Manager
//  PARAMS:
//     - INPUT:
//
//     - OUTPUT:
//
// RETURNS:
//
// NOTES:
//
BOOL ServiceTrxMgr_HandleEventRcv (IPC_OUD_RECEIVE       * pIpcOudReceive,
                                   API_OUTPUT_PARAMS     * pApiOutputParams)
{
  TCHAR                 _function_name [] = _T("ServiceTrxMgr_HandleEventRcv");

  ServiceTrace ( _T("\t%s\n"), _function_name);

  // Check internal status
  if ( pApiOutputParams->output_1 != IPC_STATUS_OK )
  {
    // Some error occured
    ServiceLogError (_function_name, _T("Ipc_API"), pApiOutputParams->output_1);

    return FALSE;
  } // if

  // Check the received message command
  switch ( pIpcOudReceive->msg_cmd )
  {
    case IPC_MSG_CMD_COMM_MGR_SESSION_OPENED:
    {
      // New Session
      ServiceTrace (_T("\t\t%s\n"), _T("Session Opened"));

      ServiceTrxMgr_OpenSession (pIpcOudReceive);
    }
    break;
    
    case IPC_MSG_CMD_COMM_MGR_DATA_RECEIVED:
    {
      // Data received
      ServiceTrace (_T("\t\t%s\n"), _T("Data Received"));
      
      ServiceTrxMgr_DataReceived (pIpcOudReceive);
    } // case
    break;

    case IPC_MSG_CMD_COMM_MGR_SESSION_CLOSED:
    {
      // Close Session
      ServiceTrace (_T("\t\t%s\n"), _T("Session Closed"));

      ServiceTrxMgr_CloseSession (pIpcOudReceive);
      
    } // case

    default:
    {
      // Unexpected Command
      ServiceLogUnexpected (_function_name, _T("Unexpected IPC Command"), pIpcOudReceive->msg_cmd);

      return FALSE;
    } // case
  } // switch

  return TRUE;

} // ServiceHandleEventReceive


//-----------------------------------------------------------------------------
// PURPOSE: General Service Processing of the Open Session Request
//
//  PARAMS:
//     - INPUT: 
//           IntAgencyId: Internal Sender Agency Id.
//     - OUTPUT: 
//
// RETURNS:
//     - TRUE : Sucess
//     - FALSE: Failure
//
// NOTES:
//

BOOL ServiceTrxMgr_OpenSession (IPC_OUD_RECEIVE * pIpcOudReceive)
{
  TCHAR                 _function_name [] = _T("ServiceTrxMgr_OpenSession");
  DWORD                 _agency_server_id;  

  ServiceTrace ( _T("\t%s\n"), _function_name);

  // Variables Initialization
  _agency_server_id   = pIpcOudReceive->user_dword;

  //
  // Here: Common Service instructions before call specific Trx Mgr. Service Function
  //

  // Common Service Open Session
  if ( GLB_Service.trx_mgr_function.p_session_opened != NULL )
  {
    if ( GLB_Service.trx_mgr_function.p_session_opened (_agency_server_id) == FALSE )
    {
      ServiceLogError (_function_name, _T("GLB_Service.trx_mgr_function.p_session_opened"), FALSE, LOG_TO_PRIVATE);

      return FALSE;
    } // if
  } // if

  //
  // Here: Common Service instructions after call specific Trx Mgr. Service Function
  //

  return TRUE;

} // ServiceTrxMgr_OpenSession


//-----------------------------------------------------------------------------
// PURPOSE: General Service Processing when a message is received
//
//  PARAMS:
//     - INPUT: 
//           IntAgencyId: Internal Sender Agency Id.
//     - OUTPUT: 
//
// RETURNS:
//     - TRUE : Sucess
//     - FALSE: Failure
//
// NOTES:
//

BOOL ServiceTrxMgr_DataReceived (IPC_OUD_RECEIVE * pIpcOudReceive)
{
  TCHAR                 _function_name [] = _T("ServiceTrxMgr_DataReceived");
  DWORD                 _agency_server_id; 
  
  ServiceTrace ( _T("\t%s\n"), _function_name);

  // Variables Initialization
  _agency_server_id   = pIpcOudReceive->user_dword;

  //
  // Here: Common Service instructions before call specific Trx Mgr. Service Function
  //

  // Common Service Open Session
  if ( GLB_Service.trx_mgr_function.p_data_recv != NULL )
  {
    if ( GLB_Service.trx_mgr_function.p_data_recv (_agency_server_id) == FALSE )
    {
      ServiceLogError (_function_name, _T("GLB_Service.trx_mgr_function.p_data_recv"), FALSE, LOG_TO_PRIVATE);

      return FALSE;
    } // if
  } // if

  //
  // Here: Common Service instructions after call specific Trx Mgr. Service Function
  //

  return TRUE;

} // ServiceTrxMgr_DataReceived

//-----------------------------------------------------------------------------
// PURPOSE: General Service Processing of the Close Session Request
//
//  PARAMS:
//     - INPUT: 
//           IntAgencyId: Internal Sender Agency Id.
//     - OUTPUT: 
//
// RETURNS:
//     - TRUE : Sucess
//     - FALSE: Failure
//
// NOTES:
//

BOOL ServiceTrxMgr_CloseSession (IPC_OUD_RECEIVE * pIpcOudReceive)
{
  TCHAR                 _function_name [] = _T("ServiceTrxMgr_CloseSession");
  DWORD                 _agency_server_id;

  ServiceTrace ( _T("\t%s\n"), _function_name);

  // Variables Initialization
  _agency_server_id   = pIpcOudReceive->user_dword;

  //
  // Here: Common Service instructions before call specific Trx Mgr. Service Function
  //

  // Common Service Open Session
  if ( GLB_Service.trx_mgr_function.p_session_closed != NULL )
  {
    if ( GLB_Service.trx_mgr_function.p_session_closed (_agency_server_id) == FALSE )
    {
      ServiceLogError (_function_name, _T("GLB_Service.trx_mgr_function.p_session_closed"), FALSE, LOG_TO_PRIVATE);

      return FALSE;
    } // if
  } // if

  //
  // Here: Common Service instructions after call specific Trx Mgr. Service Function
  //

  return TRUE;

} // ServiceTrxMgr_CloseSession
