//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : Service_License.cpp
//
//   DESCRIPTION : Functions and Methods to Load and check LK License
//
//        AUTHOR : Carles Iglesias
//
// CREATION DATE : 01-AUG-2002
// 
// REVISION HISTORY :
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 01-AUG-2002 CIR    Initial draft.
// 14-FEB-2003 AJQ    The license is checked before call to sleep
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_MISC
#define INCLUDE_IPC_API
#define INCLUDE_LICENSE
#define INCLUDE_SERVICE
#define INCLUDE_NLS_API
#include "CommonDef.h"

#include "ServiceInternals.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------
#define LICENSE_INTERVAL_CHECK  60 * 1000  // Every 1 minute
#define MAX_THREAD_STACK        50000

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATATYPES 
//------------------------------------------------------------------------------
typedef struct
{
  TYPE_LICENSE  license;
  DWORD         client_id;

} TYPE_THREAD_LICENSE;

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------
static DWORD WINAPI LicenseThread (LPVOID pThreadParams);

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Checks license conditions
//
//  PARAMS :
//      - INPUT :
//          - pLicense : License information
//          - ClientId
//
//      - OUTPUT :
//
// RETURNS :
//      - TRUE : License OK
//      - FALSE : Invalid license
//
//   NOTES :

BOOL        CheckLicense (TYPE_LICENSE  * pLicense,
                          DWORD         ClientId)
{
  // TJG 26-JAN-2004
  // License includes the client id
  if ( ClientId != 0 )
  {
    if ( ClientId != pLicense->client_id )
    {
      // Invalid license
      return FALSE;
    }
  }

  switch ( pLicense->type )
  {
    case LICENSE_TYPE_NEVER_EXPIRE :
      // Do not check expiration date
    break;

    case LICENSE_TYPE_EXPIRE_ON_DATE :
    {
      time_t    _current_date_time;

      // Check expiration date :
      //    - Get current date-time
      time (&_current_date_time);
      if ( _current_date_time > pLicense->expiration_date )
      {
        // Invalid license
        return FALSE;
      } 
    } 
    break;

    default :
      // Invalid license
      return FALSE;
    break;
  }

  // License OK
  return TRUE;
} // CheckLicense

//------------------------------------------------------------------------------
// PURPOSE : Periodically checks license conditions
//
//  PARAMS :
//      - INPUT :
//          - pThreadParams : License information
//
//      - OUTPUT :
//
// RETURNS :
//      - TRUE On success
//
//   NOTES :

DWORD WINAPI LicenseThread (LPVOID pThreadParams)
{
  TYPE_THREAD_LICENSE   * p_thread_params;

  p_thread_params = (TYPE_THREAD_LICENSE *) pThreadParams;

  for ( ;; )
  {
    // Check license
    if ( CheckLicense (&p_thread_params->license,
                       p_thread_params->client_id) )
    {
      // License OK
      Sleep (LICENSE_INTERVAL_CHECK);

      continue;
    }

    // Invalid license => Write logger + Stop service
    ServiceLogInvalidLicense (LOG_TO_BOTH);
    ServiceStop ();

    ExitThread (0);
  } // for

  ExitThread (1);
} // LicenseThread

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Reads license and starts License checking thread
//
//  PARAMS :
//      - INPUT :
//          - pLicensePath : Path where license file is located
//          - ClientId :
//
//      - OUTPUT :
//
// RETURNS :
//      - TRUE : Success
//      - FALSE : Error
//
//   NOTES :

BOOL        LicenseInit (TCHAR  * pLicensePath,
                         DWORD  ClientId)
{
  TCHAR                 _function_name [] = _T("LicenseInit");
  TYPE_THREAD_LICENSE   * p_thread_params;
  DWORD                 _thread_id;
  HANDLE                _thread_handle;
  TYPE_LICENSE          _license;

  // Read license information
  memset (&_license, 0, sizeof (TYPE_LICENSE));
  if ( ! ReadLicense (&_license, pLicensePath) )
  {
    // License not present or it could not be read
    ServiceLogInvalidLicense (LOG_TO_BOTH);

    return FALSE;
  }

  // Check license before start anything else
  if ( ! CheckLicense (&_license, ClientId) )
  {
    // Invalid license
    ServiceLogInvalidLicense (LOG_TO_BOTH);

    return FALSE;
  }

  p_thread_params = (TYPE_THREAD_LICENSE *) malloc (sizeof (TYPE_THREAD_LICENSE));
  if ( p_thread_params == NULL )
  {
    // Could not allocate memory for thread params
    // Write logger
    ServiceLogError (_function_name, 
                     _T("malloc"), 
                     NULL, 
                     LOG_TO_BOTH);
    return FALSE;
  }

  p_thread_params->license   = _license;
  p_thread_params->client_id = ClientId;

#define MODULE_NAME     _T("CHK_LICENSE")
  // Start license thread
  _thread_handle = CREATE_THREAD ((LPSECURITY_ATTRIBUTES) NULL,
                                 MAX_THREAD_STACK,
                                 (LPTHREAD_START_ROUTINE) LicenseThread,
                                 (LPVOID) p_thread_params,
                                 0,
                                 (LPDWORD) &_thread_id);
  if ( _thread_handle == NULL )
  {
    // Could not start checking thread 
    // Write logger
    ServiceLogError (_function_name, 
                     _T("CREATE_THREAD"), 
                     NULL, 
                     LOG_TO_BOTH);

    return FALSE;
  }

  SetThreadPriority (_thread_handle, THREAD_PRIORITY_NORMAL);

  return TRUE;
} // LicenseInit
