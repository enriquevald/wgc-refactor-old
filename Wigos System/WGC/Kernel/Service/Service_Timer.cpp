//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: Service_Timer.cpp
//
//   DESCRIPTION: Functions and Methods to handle the Service Timer
//
//        AUTHOR: Andreu Juli�
//
// CREATION DATE: 5-NOV-2002
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 5-NOV-2002 AJQ    Initial draft.
// 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_MISC
#define INCLUDE_QUEUE_API
#define INCLUDE_IPC_API
#define INCLUDE_PRIVATE_LOG
#define INCLUDE_LICENSE
#define INCLUDE_ALARMS
#define INCLUDE_SERVICE
#define INCLUDE_NLS_API
#include "CommonDef.h"

#include "ServiceInternals.h"

//------------------------------------------------------------------------------
//  PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PURPOSE: Creates a timer
//
//  PARAMS:
//     - INPUT: 
//        - Timer
//        - FunctionName
//     - OUTPUT: None
//
// RETURNS:
//      - TRUE: on success
//      - FALSE: on failure
//
// NOTES:
//
BOOL ServiceTimerCreate (HANDLE * pTimer, TCHAR * pFunctionName)
{
  * pTimer = NULL;
  * pTimer = CreateWaitableTimer (NULL,   // Timer attributes
                                 TRUE,   // Manual reset
                                 NULL);  // Timer name
  if ( * pTimer == NULL )
  {
    ServiceLogError (pFunctionName, _T("CreateWaitableTimer"), GetLastError());

    return FALSE;
  } // if

  return TRUE;

} // ServiceTimerCreate

//-----------------------------------------------------------------------------
// PURPOSE: Enables a timer
//
//  PARAMS:
//     - INPUT: 
//        - Timer
//        - FunctionName
//     - OUTPUT: None
//
// RETURNS:
//      - TRUE: on success
//      - FALSE: on failure
//
// NOTES:
//
BOOL ServiceTimerEnable (HANDLE Timer, DWORD  Milliseconds, TCHAR * pFunctionName)
{
  BOOL          _bool_rc;
  LARGE_INTEGER _due_time;

  if ( Timer == NULL )
  {
    return FALSE;
  } // if

  _due_time.QuadPart = (__int64) -10000L * (__int64) Milliseconds;

  _bool_rc = SetWaitableTimer (Timer,         // Timer
                               &_due_time,    // DueTime
                               0,             // Period
                               NULL,          // APC Routine
                               NULL,          // APC Parameter
                               FALSE);        // Resume

  if ( _bool_rc == FALSE )
  {
    ServiceLogError (pFunctionName, _T("SetWaitableTimer"), GetLastError());
  } // if

  return _bool_rc;

} // ServiceTimerEnable

//-----------------------------------------------------------------------------
// PURPOSE: Disables a timer
//
//  PARAMS:
//     - INPUT: 
//        - Timer
//        - FunctionName
//     - OUTPUT: None
//
// RETURNS:
//      - TRUE: on success
//      - FALSE: on failure
//
// NOTES:
//
BOOL ServiceTimerDisable (HANDLE Timer, TCHAR * pFunctionName)
{
  BOOL          _bool_rc;
  
  if ( Timer == NULL )
  {
    return TRUE;
  } // if

  _bool_rc = CancelWaitableTimer (Timer);

  if ( _bool_rc == FALSE )
  {
    ServiceLogError (pFunctionName, _T("CancelWaitableTimer"), GetLastError());
  } // if

  return _bool_rc;

} // ServiceTimerDisable

//-----------------------------------------------------------------------------
// PURPOSE: Deletes a timer
//
//  PARAMS:
//     - INPUT: 
//        - Timer
//        - FunctionName
//     - OUTPUT: None
//
// RETURNS:
//      - TRUE: on success
//      - FALSE: on failure
//
// NOTES:
//
BOOL ServiceTimerDelete (HANDLE Timer, TCHAR * pFunctionName)
{
  BOOL          _bool_rc;
  
  if ( Timer == NULL )
  {
    return TRUE;
  } // if

  _bool_rc = CloseHandle (Timer);

  if ( _bool_rc == FALSE )
  {
    ServiceLogError (pFunctionName, _T("CloseHandle"), GetLastError());
  } // if

  return _bool_rc;

} // ServiceTimerDelete
