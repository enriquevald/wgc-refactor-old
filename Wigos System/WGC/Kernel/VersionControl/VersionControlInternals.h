//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : VersionControlInternals.h
//
//   DESCRIPTION : Constants, types, variables definitions and prototypes
//                 for VersionControl
//
//                 This module is SHARED in LKC, LKAS and LKT solutions.
//
//                 VERY IMPORTANT !!!
//                 Due that this is a module is shared (LKC, LKAS & LKT), 
//                 ANY change must be checked twice by compiling LKC, LKAS
//                 and LKT DLLs
//
//        AUTHOR : Carles Iglesias
//
// CREATION DATE : 25-JUN-2002
//
// REVISION HISTORY
//
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 25-JUN-2002 CIR    Initial draft.
// 01-AUG-2002 ACC    Module is shared.
// 20-NOV-2002 RRT    Moved encryption constants to VersionControl.h
//------------------------------------------------------------------------------

#ifndef __VERSIONCONTROLINTERNALS_H
#define __VERSIONCONTROLINTERNALS_H

//------------------------------------------------------------------------------
// INCLUDES
//------------------------------------------------------------------------------
#include <wincrypt.h>

//------------------------------------------------------------------------------
// PUBLIC CONSTANTS
//------------------------------------------------------------------------------
#define MODULE_NAME                         _T("VERSION_CONTROL")
#define MODULE_NAME_SKIP_VERSION_CONTROL    _T("GUI_VERSION_CONTROL")

// The strings for VERSION_LOG_CODE_1 and VERSION_LOG_CODE_4 are identical 
// in LKC and LKT+LKAS except the order of the parameters.
#if defined(LKT_VERSION) || defined(LKAS_VERSION)
// LKAS & LKT
// ACC 25-NOV-2003, The Version Control can't call the Logger
#define Common_LoggerVersion(MsgId, Param1, Param2, Param3) {;}
//#define Common_LoggerVersion(MsgId, Param1, Param2, Param3) Common_Logger (MsgId, Param2, Param1, Param3); 

#else
// LKC
// AJQ 02-APR-2003, The Version Control can't call the Logger
#define Common_LoggerVersion(MsgId, Param1, Param2, Param3) {;}
// #define Common_LoggerVersion(MsgId, Param1, Param2, Param3) Common_Logger (MsgId, Param1, Param2, Param3);

#endif  // defined(LKT_VERSION) || defined(LKAS_VERSION)


// ACC 04-SEP-2002
// The strings for VERSION_LOG_CODE_1 and VERSION_LOG_CODE_4 are identical 
// in LKC and LKT, LKAS except the order of the parameters.

#if defined(LKT_VERSION) || defined(LKAS_VERSION)
// LKAS & LKT
#define VERSION_LOG_CODE_1              4
#define VERSION_LOG_CODE_2              32
#define VERSION_LOG_CODE_3              31
#define VERSION_LOG_CODE_4              21
#define VERSION_LOG_CODE_5              35

#define COMMON_DB_USER_SIZE             20
#define COMMON_DB_PASSWORD_SIZE         20
#define COMMON_DB_CONNECT_STRING_SIZE   30

#define COMMON_MAX_PATH_SIZE            _MAX_PATH   // Visual C++ constant

#else

// LKC
#define VERSION_LOG_CODE_1              NLS_ID_LOGGER(2)
#define VERSION_LOG_CODE_2              NLS_ID_LOGGER(3)
#define VERSION_LOG_CODE_3              NLS_ID_LOGGER(7)
#define VERSION_LOG_CODE_4              NLS_ID_LOGGER(9)
#define VERSION_LOG_CODE_5              NLS_ID_LOGGER(10)

#endif // defined(LKT_VERSION) || defined(LKAS_VERSION)

//------------------------------------------------------------------------------
// PUBLIC DATATYPES
//------------------------------------------------------------------------------

typedef struct
{
  BOOL  signature_status [VERSION_MAX_COMPONENTS];

} TYPE_SIGNATURE_CHECK;

//------------------------------------------------------------------------------
// PUBLIC DATA STRUCTURES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PUBLIC FUNCTION PROTOTYPES
//------------------------------------------------------------------------------
void            Version_FormatDLName (TCHAR * pModulePath,
                                      TCHAR * pModuleName,
                                      TCHAR * pFormattedDLPath);

void            Version_FormatFullPathName (TCHAR   * pPath,
                                            TCHAR   * pName,
                                            TCHAR   * pFullPathName);

WORD            Version_GetDLData (TCHAR                * pModulePath,
                                   TCHAR                * pModuleName,
                                   TYPE_VERSION_MODULE  * pModuleVersion,
                                   TYPE_SIGNATURE_CHECK * pSignatureCheck,
                                   BOOL                 CheckFile);

void            Version_FormatVersionString (DWORD   Client,
                                             DWORD   Build,
                                             TCHAR   * pVersionString);

void            Version_FormatModuleVersionString (DWORD   Client,
                                                   DWORD   Build,
                                                   DWORD   CommonDatabase,
                                                   DWORD   ClientDatabase,
                                                   TCHAR   * pVersionString);

BOOL            Version_SetCspEnvironment (BYTE         * pKeyBlob,
                                           HCRYPTPROV   * pCspProvider,
                                           HCRYPTKEY    * pCspKey);

void            Version_ReleaseCspEnvironment (HCRYPTPROV   CspProvider,
                                               HCRYPTKEY    CspKey);

BOOL            Version_CryptCreateHash (HCRYPTPROV CspProvider,
                                         ALG_ID     AlgorithmId,
                                         HCRYPTKEY  KeyType,
                                         DWORD      Flags,
                                         HCRYPTHASH * pCspHash);

BOOL            Version_CryptHashData (HCRYPTHASH   CspHash,
                                       BYTE         * pBufferToHash,
                                       DWORD        BufferSize, 
                                       DWORD        Flags);

BOOL            Version_CryptAcquireContext (HCRYPTPROV * pCspProvider,
                                             LPCTSTR    pContainer,
                                             LPCTSTR    pProviderName,
                                             DWORD      ProviderType,
                                             DWORD      Flags);

BOOL            Version_CryptImportKey (HCRYPTPROV  CspProvider,
                                        BYTE        * pKeyBlob,
                                        DWORD       KeyBlobLength,
                                        HCRYPTKEY   KeyBlobKey,
                                        DWORD       Flags,
                                        HCRYPTKEY   * pCspKey);

BOOL            Version_CryptVerifySignature (HCRYPTHASH    CspHash,
                                              BYTE          * pSignature,
                                              DWORD         SignatureLength,
                                              HCRYPTKEY     CspKey,
                                              LPCTSTR       Description,
                                              DWORD         Flags);

BOOL            Version_CryptReleaseContext (HCRYPTPROV     CspProvider, 
                                             DWORD          Flags);

BOOL            Version_CryptDestroyHash (HCRYPTHASH CspHash);

BOOL            Version_CryptDestroyKey (HCRYPTKEY  CspKey);

#endif // __VERSIONCONTROLINTERNALS_H
