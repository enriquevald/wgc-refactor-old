//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME : VersionControlCryptoAPI.cpp
//
//   DESCRIPTION : Functions and Methods to Encrypt and deEncrypt.
//
//                 This module is SHARED in LKC, LKAS and LKT solutions.
//
//                 VERY IMPORTANT !!!
//                 Due that this is a module is shared (LKC, LKAS & LKT), 
//                 ANY change must be checked twice by compiling LKC, LKAS
//                 and LKT DLLs
//
//        AUTHOR : Alberto Cuesta
//
// CREATION DATE : 30-JUL-2002
// 
// REVISION HISTORY
// 
// Date        Author Description
// ----------- ------ -----------------------------------------------------------
// 30-JUL-2002 ACC    Initial draft.
// 03-FEB-2003 ACC    Module shared.
// 
//-------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// INCLUDES 
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_MISC
#define INCLUDE_VERSION_CONTROL
#define INCLUDE_NLS_API

#include "CommonDef.h"

#include "VersionControlInternals.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATATYPES 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES 
//------------------------------------------------------------------------------
// Ticket number encryption key (CMEA)
unsigned char CMEAKey [] = { 0xab, 0x2b, 0xc4, 0x18, 0x74, 0x60, 0x0b, 0x91 };

// Ticket number encryption CAVE table
unsigned char CaveTable [] = { 0xd9, 0x0a, 0x59, 0x49, 0xe2, 0x86, 0xb8, 0xb1, 0xbb, 0x91, 
                               0xe7, 0x5a, 0x4e, 0x04, 0x91, 0xf5, 0x23, 0x46, 0x47, 0x56,
                               0x38, 0xbd, 0x77, 0x30, 0x86, 0x76, 0xfa, 0x6f, 0x96, 0x79, 
                               0x34, 0x36, 0x5f, 0x77, 0xe3, 0x23, 0xba, 0x0a, 0x80, 0xa4, 
                               0x0d, 0xf0, 0xd8, 0x9b, 0xa8, 0xe3, 0xf6, 0xae, 0xe6, 0x8d, 
                               0xd2, 0x15, 0x44, 0xf1, 0xd1, 0x96, 0x7a, 0x17, 0x81, 0xd9, 
                               0x5a, 0xc7, 0x5c, 0x01, 0xca, 0x10, 0xff, 0x97, 0x9f, 0x3c, 
                               0x12, 0xf8, 0x97, 0x43, 0x6f, 0xfe, 0xb5, 0x1b, 0x67, 0x2f, 
                               0x68, 0x9f, 0xae, 0xe4, 0x83, 0xa7, 0x26, 0x57, 0x13, 0x38, 
                               0x00, 0x71, 0xd7, 0x66, 0x89, 0x94, 0x97, 0x5e, 0x64, 0xcb, 
                               0x5d, 0x29, 0xac, 0x49, 0x6c, 0x29, 0x14, 0x44, 0xc3, 0x81, 
                               0x73, 0xc3, 0xb0, 0x62, 0xca, 0x6f, 0x1c, 0x93, 0x6d, 0x8e, 
                               0x4e, 0x84, 0x42, 0xc5, 0x8d, 0x4a, 0x05, 0x49, 0x7b, 0xf1, 
                               0x15, 0xf2, 0xde, 0xcb, 0xe9, 0x05, 0x51, 0xa2, 0x25, 0x37,
                               0x3f, 0x25, 0x22, 0x8b, 0xf2, 0x34, 0x8b, 0x70, 0xab, 0x45, 
                               0xcf, 0x1f, 0x30, 0xdb, 0x7c, 0xa2, 0xf2, 0x9d, 0xaa, 0xbd, 
                               0x0c, 0xec, 0x7d, 0x3c, 0xc7, 0x5f, 0xf3, 0x62, 0xe5, 0xef, 
                               0x5d, 0x88, 0xec, 0xdc, 0xcb, 0x58, 0x34, 0xa5, 0x38, 0x88, 
                               0x65, 0xe8, 0x54, 0x7c, 0xf2, 0x65, 0xc9, 0x2d, 0x04, 0x5f, 
                               0xee, 0x12, 0x11, 0xc9, 0x21, 0xba, 0xf1, 0x10, 0x3a, 0xc3, 
                               0x2f, 0x5e, 0x9e, 0x00, 0x60, 0x3e, 0xbf, 0xe0, 0xa5, 0x83, 
                               0xbc, 0xd1, 0x76, 0x74, 0x0b, 0x2b, 0xd8, 0xca, 0xb6, 0xb6,
                               0x71, 0xb0, 0x18, 0x77, 0x8d, 0xd8, 0x96, 0x0d, 0x09, 0x62, 
                               0x95, 0xda, 0xc4, 0x0d, 0x33, 0x13, 0x1b, 0xf8, 0xd0, 0x6c, 
                               0x4e, 0x2b, 0x00, 0xae, 0x20, 0xde, 0x4e, 0xed, 0xa9, 0xbc, 
                               0xab, 0xec, 0x29, 0xa2, 0x4d, 0xda };

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES 
//------------------------------------------------------------------------------
unsigned char   Tbox (unsigned int Z);

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Gets k value for encryption process.
// 
//  PARAMS :
//      - INPUT :
//          - Z (unsigned int): integer value of range 0 to 255
//
//      - OUTPUT : 
// 
// RETURNS : 
//      - Value asigned to k in encryption/decryption routine
//
//   NOTES :

unsigned char Tbox (unsigned int Z)
{
  unsigned char temp;
  int           _idx_k, idx;

  _idx_k = 0;
  temp = (unsigned char) Z;

  for ( idx = 0; idx < 4; idx++ )
  {
    temp ^= CMEAKey[_idx_k];
    temp += CMEAKey[_idx_k + 1];
    temp = (unsigned char) Z + CaveTable[temp];
    _idx_k += 2;
  }

  return temp;
} // Tbox

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION 
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Encrypt/Decrypt Password (sequence) adapting the CMEA 
//          encryption algorithm.
// 
//  PARAMS :
//      - INPUT :
//          - pInBuffer : buffer with Password to encrypt or decrypt
//          - SizeOfBuffers (int): Size of buffers (4 of ulint size)
//
//      - OUTPUT : 
//          - pOutBuffer : buffer with the result data
// 
// RETURNS : 
//
//   NOTES :

void        Version_EncryptDecryptBuffer (unsigned char * pInBuffer,
                                          unsigned char * pOutBuffer,
                                          int           SizeOfBuffers)
{
  unsigned int _idx_msg;
  unsigned int k;
  unsigned int z;
  unsigned int half;

  z = 0;
  memcpy (pOutBuffer, pInBuffer, SizeOfBuffers);

  for (_idx_msg = 0; _idx_msg < (unsigned int) SizeOfBuffers; _idx_msg++)
  {
    k = Tbox (z ^ _idx_msg);
    pOutBuffer[_idx_msg] += k;
    z += pOutBuffer[_idx_msg];
  }

  half = SizeOfBuffers / 2;

  for (_idx_msg = 0; _idx_msg < half; _idx_msg++)
  {
    pOutBuffer[_idx_msg] ^= (pOutBuffer[(SizeOfBuffers - 1) - _idx_msg] | 1);
  }

  z = 0;

  for (_idx_msg = 0; _idx_msg < (unsigned int) SizeOfBuffers; _idx_msg++)
  {
    k = Tbox (z ^ _idx_msg);
    z += pOutBuffer[_idx_msg];
    pOutBuffer[_idx_msg] -= k;
  }

  return;
} // Version_EncryptDecryptBuffer

// Cryptografic Service Provider (CSP) and version functions

//------------------------------------------------------------------------------
// PURPOSE : Get cryptografic service provider context
// 
//  PARAMS :
//      - INPUT :
//          - pContainer : Container name where keys generated for
//                         this provider will be stored
//          - pProviderName: Provider name which provider handle will
//                           be created in this function
//          - ProviderType: Provider type
//          - Flags: Provider characteristics
//
//      - OUTPUT : 
//          - pCspProvider : Provider handle created
// 
// RETURNS : 
//          - TRUE on success
//          - FALSE on error
//
//   NOTES :

BOOL            Version_CryptAcquireContext (HCRYPTPROV * pCspProvider,
                                             LPCTSTR    pContainer,
                                             LPCTSTR    pProviderName,
                                             DWORD      ProviderType,
                                             DWORD      Flags)
{
  TCHAR     _function_name [] = _T("Version_CryptAcquireContext");

  if ( ! CryptAcquireContext (pCspProvider,
                              pContainer,
                              pProviderName,
                              ProviderType,
                              Flags) )
  {
    TYPE_LOGGER_PARAM  _log_param1;
    DWORD     _last_error;

    _last_error = GetLastError ();
    _stprintf (_log_param1, _T("0x%X"), _last_error);
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("CryptAcquireContext"), _T(""));

    return FALSE;
  }           

  return TRUE;
} // Version_CryptAcquireContext

//------------------------------------------------------------------------------
// PURPOSE : Create a hash object to store encrypted data
// 
//  PARAMS :
//      - INPUT :
//          - CspProvider : Provider handle 
//          - AlgorithmId : Hashing algorithm ID
//          - KeyType: Hashing key type
//          - Flags: Reserved field. It must be zero
//
//      - OUTPUT : 
//          - pCspHash : Hash object handle
// 
// RETURNS : 
//          - TRUE on success
//          - FALSE on error
//
//   NOTES :

BOOL            Version_CryptCreateHash (HCRYPTPROV CspProvider,
                                         ALG_ID     AlgorithmId,
                                         HCRYPTKEY  KeyType,
                                         DWORD      Flags,
                                         HCRYPTHASH * pCspHash)
{
  TCHAR     _function_name [] = _T("Version_CryptCreateHash");

  if ( ! CryptCreateHash (CspProvider,
                          AlgorithmId,
                          KeyType,
                          Flags,
                          pCspHash) )
  {
    TYPE_LOGGER_PARAM   _log_param1;
    DWORD               _last_error;

    _last_error = GetLastError ();
    _stprintf (_log_param1, _T("0x%X"), _last_error);
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("CryptCreateHash"), _T(""));

    return FALSE;
  }

  return TRUE;
} // Version_CryptCreateHash

//------------------------------------------------------------------------------
// PURPOSE : Import a cryptographic key from a key blob into the current
//          cryptographic provider
// 
//  PARAMS :
//      - INPUT :
//          - CspProvider : Provider handle 
//          - pKeyBlob : Key blob where encrypt key will be import from
//          - KeyBlobLength: LEngth of key blob
//          - KeyBlobKey: Key to decrypt the kry blob
//          - Flags: Key flags
//
//      - OUTPUT : 
//          - pCspKey : Imported key
// 
// RETURNS : 
//          - TRUE on success
//          - FALSE on error
//
//   NOTES :

BOOL            Version_CryptImportKey (HCRYPTPROV  CspProvider,
                                        BYTE        * pKeyBlob,
                                        DWORD       KeyBlobLength,
                                        HCRYPTKEY   KeyBlobKey,
                                        DWORD       Flags,
                                        HCRYPTKEY   * pCspKey)
{
  TCHAR     _function_name [] = _T("Version_CryptImportKey");

  if ( ! CryptImportKey (CspProvider,
                         pKeyBlob,
                         KeyBlobLength,
                         KeyBlobKey,
                         Flags,
                         pCspKey) )
  {
    TYPE_LOGGER_PARAM   _log_param1;
    DWORD               _last_error;

    _last_error = GetLastError ();
    _stprintf (_log_param1, _T("0x%X"), _last_error);
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("CryptImportKey"), _T(""));

    return FALSE;
  }

  return TRUE;
} // Version_CryptImportKey

//------------------------------------------------------------------------------
// PURPOSE : Hash data from a buffer into a hashing object
// 
//  PARAMS :
//      - INPUT :
//          - CspHash : Hash object to add hashed data  
//          - pBufferToHash : Data to be hashed
//          - BufferSize : Buffer size to be hashed
//          - Flags : Key to decrypt the kry blob
//          - Flags: Not used for all Microsoft providers
//
//      - OUTPUT : 
// 
// RETURNS : 
//          - TRUE on success
//          - FALSE on error
//
//   NOTES :

BOOL            Version_CryptHashData (HCRYPTHASH   CspHash,
                                       BYTE         * pBufferToHash,
                                       DWORD        BufferSize, 
                                       DWORD        Flags)
{
  TCHAR     _function_name [] = _T("Version_CryptHashData");

  if ( ! CryptHashData (CspHash,
                        pBufferToHash,
                        BufferSize, 
                        Flags) )
  {
    TYPE_LOGGER_PARAM   _log_param1;
    DWORD               _last_error;

    _last_error = GetLastError ();
    _stprintf (_log_param1, _T("0x%X"), _last_error);
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("CryptHashData"), _T(""));

    return FALSE;
  }

  return TRUE;
} // Version_CryptHashData

//------------------------------------------------------------------------------
// PURPOSE : Verify a digital signature against hashed data
//          
//  PARAMS :
//      - INPUT :
//          - CspHash : Hash object where hashed data is stored
//          - pSignature : Signature to validate
//          - SignatureLength : Length of signature buffer
//          - CspKey : Key used to validate digital signature
//          - Description: Not used. It must be NULL
//          - Flags: Flags for the verification
//
//      - OUTPUT : 
// 
// RETURNS : 
//          - TRUE on success
//          - FALSE on error
//
//   NOTES :

BOOL        Version_CryptVerifySignature (HCRYPTHASH    CspHash,
                                          BYTE          * pSignature,
                                          DWORD         SignatureLength,
                                          HCRYPTKEY     CspKey,
                                          LPCTSTR       Description,
                                          DWORD         Flags)
{
  TCHAR     _function_name [] = _T("Version_CryptVerifySignature");

  if ( ! CryptVerifySignature (CspHash,
                               pSignature,
                               SignatureLength,
                               CspKey,
                               Description,
                               Flags) )
  {
    TYPE_LOGGER_PARAM   _log_param1;
    DWORD               _last_error;

    _last_error = GetLastError ();
    _stprintf (_log_param1, _T("0x%X"), _last_error);
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("CryptVerifySignature"), _T(""));

    return FALSE;
  }                               

  return TRUE;
} // Version_CryptVerifySignature

//------------------------------------------------------------------------------
// PURPOSE : Release a CSP context 
// 
//  PARAMS :
//      - INPUT :
//          - CspProvider : Provides of the CSP context to release
//          - Flags: Reserved parameter. It must be 0 (zero)
//
//      - OUTPUT : 
// 
// RETURNS : 
//          - TRUE on success
//          - FALSE on error
//
//   NOTES :

BOOL        Version_CryptReleaseContext (HCRYPTPROV  CspProvider, 
                                         DWORD       Flags)
{
  TCHAR     _function_name [] = _T("Version_CryptReleaseContext");
  
  if ( ! CryptReleaseContext (CspProvider, Flags) )
  {
    TYPE_LOGGER_PARAM   _log_param1;
    DWORD               _last_error;

    _last_error = GetLastError ();
    _stprintf (_log_param1, _T("0x%X"), _last_error);
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("CryptReleaseContext"), _T(""));

    return FALSE;
  }

  return TRUE;
} // Version_CryptReleaseContext

//------------------------------------------------------------------------------
// PURPOSE : Destroy a hash object
// 
//  PARAMS :
//      - INPUT :
//          - CspHash : Hashs object to destroy
//
//      - OUTPUT : 
// 
// RETURNS : 
//          - TRUE on success
//          - FALSE on error
//
//   NOTES :

BOOL  Version_CryptDestroyHash (HCRYPTHASH CspHash)
{
  TCHAR     _function_name [] = _T("Version_CryptDestroyHash");
  
  if ( ! CryptDestroyHash (CspHash) )
  {
    TYPE_LOGGER_PARAM   _log_param1;
    DWORD               _last_error;

    _last_error = GetLastError ();
    _stprintf (_log_param1, _T("0x%X"), _last_error);
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("CryptDestroyHash"), _T(""));

    return FALSE;
  }           

  return TRUE;
} // Version_CryptDestroyHash

//------------------------------------------------------------------------------
// PURPOSE : Destroy a cryptographic key
// 
//  PARAMS :
//      - INPUT :
//          - CspKey : cryptographic key to destroy
//
//      - OUTPUT : 
// 
// RETURNS : 
//          - TRUE on success
//          - FALSE on error
//
//   NOTES :

BOOL        Version_CryptDestroyKey (HCRYPTKEY CspKey)
{
  TCHAR     _function_name [] = _T("Version_CryptDestroyKey");
  
  if ( ! CryptDestroyKey (CspKey) )
  {
    TYPE_LOGGER_PARAM   _log_param1;
    DWORD               _last_error;

    _last_error = GetLastError ();
    _stprintf (_log_param1, _T("0x%X"), _last_error);
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("CryptDestroyKey"), _T(""));

    return FALSE;
  }           

  return TRUE;
} // Version_CryptDestroyKey
