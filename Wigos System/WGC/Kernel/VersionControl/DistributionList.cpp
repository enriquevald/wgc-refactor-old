//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : DistributionList.cpp
//
//   DESCRIPTION : Functions and Methods to handle Distribution List in system
//                 modules version control.
//
//                 This module is SHARED in LKC, LKAS and LKT solutions.
//
//                 VERY IMPORTANT !!!
//                 Due that this is a module is shared (LKC, LKAS & LKT), 
//                 ANY change must be checked twice by compiling LKC, LKAS
//                 and LKT DLLs
//
//        AUTHOR : Carles Iglesias
//
// CREATION DATE : 25-JUN-2002
//
// REVISION HISTORY
//
// Date        Author Description
//----------- ------ -----------------------------------------------------------
// 25-JUN-2002 CIR    Initial draft.
// 01-AUG-2002 ACC    Module is shared.
// 02-MAR-2004 RRT    Added two(2) Database versions: CommonDatabase, ClientDatabase.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------
#define INCLUDE_COMMON_MISC
#define INCLUDE_VERSION_CONTROL
#define INCLUDE_NLS_API

#include "CommonDef.h"

#include "VersionControlInternals.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS
//------------------------------------------------------------------------------
#define VERSION_DL_FILE_EXT                     _T(".DIST")

#define VERSION_SIZE_COMPONENT_BUFFER           1024 * 4 //1024 * 32// 4Kb for hashing buffer
#define VERSION_SIZE_CRC_32                     sizeof (DWORD)
#define VERSION_SIZE_DL_REGISTER                VERSION_SIZE_FILLER_REG + VERSION_SIZE_CRC_32

// Development environment system variable
#define VERSION_DEV_ENV_VAR_NAME                _T("LKS_VC_DEV")
#define VERSION_DEVELOPMENT_ENVIRONMENT         _T("1")

#define VERSION_STATUS_SIGNATURE_NOT_VALIDATED  1001
#define VERSION_STATUS_EOF                      1002

//------------------------------------------------------------------------------
// PRIVATE DATATYPES
//------------------------------------------------------------------------------

// Distribution list file register structure
//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------
WORD        Version_ValidateSignature (TCHAR      * pFileName,
                                       HCRYPTPROV CspProvider,
                                       HCRYPTKEY  CspKey,
                                       DWORD      Offset,
                                       BYTE       * pSignature);

DWORD       Version_GetComponentFromFile (HANDLE    File,
                                          BYTE      * pOutBuffer);

BOOL        Version_IsDevelopmentEnvironment (void);

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Format DL full path and name
//
//  PARAMS :
//      - INPUT :
//          - pModulePath: Full path where DL file and module components
//                         are located
//          - pModuleName
//
//      - OUTPUT :
//          - pFormattedDLPath: Formatted path
//
// RETURNS :
//
//   NOTES :

void            Version_FormatDLName (TCHAR     * pModulePath,
                                      TCHAR     * pModuleName,
                                      TCHAR     * pFormattedDLPath)
{
  Version_FormatFullPathName (pModulePath, pModuleName, pFormattedDLPath);

  // Concatenate DL extension to module name
  _tcscat (pFormattedDLPath, VERSION_DL_FILE_EXT);

  return;
} // Version_FormatDLName

//------------------------------------------------------------------------------
// PURPOSE : Get module information from Distribution list file
//
//  PARAMS :
//      - INPUT :
//          - pModulePath: Full path where DL file and module components
//                         are located
//          - pModuleName
//
//      - OUTPUT :
//          - pModuleVersion: Module data read from DL file (expected versions)
//
// RETURNS :
//          - VERSION_STATUS_OK
//          - VERSION_STATUS_ERROR
//          - VERSION_STATUS_DL_NOT_FOUND
//          - VERSION_STATUS_COMPONENT_NOT_FOUND
//   NOTES :

WORD            Version_GetDLData (TCHAR                * pModulePath,
                                   TCHAR                * pModuleName,
                                   TYPE_VERSION_MODULE  * pModuleVersion,
                                   TYPE_SIGNATURE_CHECK * pSignatureCheck,
                                   BOOL                 CheckFile)
{
  TCHAR             _function_name [] = _T("Version_GetDLData");
  TYPE_LOGGER_PARAM _log_param1;
  TCHAR             _full_dl_name [COMMON_MAX_PATH_SIZE + 1];
  TCHAR             _full_component_name [COMMON_MAX_PATH_SIZE + 1];
  TCHAR             _aux_module_name [VERSION_LENGTH_MODULE_NAME + 1];
  HANDLE            _dl_file;
  TYPE_DL_REGISTER  _dl_data;
  WORD              _idx_register;
  WORD              _idx_component;
  HCRYPTPROV        _csp_provider;
  HCRYPTKEY         _csp_key;
  BOOL              _rc_bool;
  DWORD             _rc_comp;
  int               _rc_cmp;

  // Default signature check is OK
  for ( _idx_component = 0; _idx_component < VERSION_MAX_COMPONENTS; _idx_component++ )
  {
    pSignatureCheck->signature_status[_idx_component] = TRUE;
  } // for
  
  // Skip GUI_VersionGenerator and GUI_VersionViewer modules
  if ( ! _tcscmp (pModuleName, MODULE_NAME_SKIP_VERSION_CONTROL) )
  {
    return VERSION_STATUS_OK;
  } // if

  // Format a full path DL file name from module name
  Version_FormatDLName (pModulePath, pModuleName, _full_dl_name);

  // Open DL file
  _dl_file = CreateFile (_full_dl_name, 
                         GENERIC_READ,
                         FILE_SHARE_READ,
                         NULL,
                         OPEN_EXISTING,
                         FILE_FLAG_SEQUENTIAL_SCAN,
                         NULL);

  if ( _dl_file == INVALID_HANDLE_VALUE )
  {
    // Could not open DL file or file not found
    // Write Logger
    _stprintf (_log_param1, _T("%lu"), GetLastError ());
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("CreateFile"), _T(""));

    return VERSION_STATUS_DL_NOT_FOUND;
  } // if

  _idx_register  = 0;
  _idx_component = 0;

  // Read data from DL file
  //  - First register corresponds to header register.
  //    - Set Cryptographic Service Provider (CSP) environment
  //    - Validate general DL file signature
  //  - Second register corresponds to module data.
  //    - This register is not relevant for version control purposes, skip it
  //  - From third register corresponds to component register.
  //    - Get full component name (full path + component name) 
  //    - Validate digital signature of specific component

  while ( TRUE )
  {
    // Reset read buffer
    memset (&_dl_data, 0, sizeof (TYPE_DL_REGISTER));

    _rc_comp = Version_GetComponentFromFile (_dl_file, (BYTE *) &_dl_data);
    if ( _rc_comp != VERSION_STATUS_OK )
    {
      if ( _rc_comp == VERSION_STATUS_EOF )
      {
        // end of file
        break;
      } // if

      // Error reading file
      // Write Logger
      _stprintf (_log_param1, _T("%lu"), GetLastError ());
      Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("Version_GetComponentFromFile"), NULL);

      // Close DL file
      CloseHandle (_dl_file);

      return VERSION_STATUS_ERROR;
    } 

    //      - First register corresponds to header register.
    //      - Second register corresponds to module data.
    //      - From third register corresponds to component register.
    switch ( _idx_register )
    {
      case 0 :
        //  - First register corresponds to header register.
        //      - Set Cryptographic Service Provider (CSP) environment
        //      - Validate general DL file signature
        //      - Process next register

        //      - Set Cryptographic Service Provider (CSP) environment
        _rc_bool = Version_SetCspEnvironment (_dl_data.reg_data.header.key_blob,
                                              &_csp_provider,
                                              &_csp_key);
        if ( ! _rc_bool )
        {
          // Could not set CSP environment
          // Write Logger
          _stprintf (_log_param1, _T("%hu"), _rc_bool);
          Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("Version_SetCspEnvironment"), _T(""));

          // Close DL file
          CloseHandle (_dl_file);

          return VERSION_STATUS_ERROR;
        } 

        //      - Validate general DL file signature
        _rc_comp = Version_ValidateSignature (_full_dl_name, 
                                              _csp_provider,
                                              _csp_key,
                                              sizeof (TYPE_DL_REGISTER),                    // Offset to start hashing
                                              _dl_data.reg_data.header.digital_signature);  // from second register because
                                                                                      // first register (header)
                                                                                      // contains DL signature value
        if ( _rc_comp != VERSION_STATUS_OK )
        {
          // Error in digital signature
          // Write Logger
          _stprintf (_log_param1, _T("%hu"), _rc_comp);
          Common_LoggerVersion (VERSION_LOG_CODE_1, _T("Version_ValidateSignature"), _T("DL File"), NULL);

          // Release CSP environment
          Version_ReleaseCspEnvironment (_csp_provider,
                                         _csp_key);
          // Close DL file
          CloseHandle (_dl_file);

          return VERSION_STATUS_ERROR;
        } 
        
        //      - Process next register
        _idx_register++;
      break;

      case 1 :
        //  - Second register corresponds to module data. (Virtual Module Version)
        //      - Check if module name in distribution list corresponds to 
        //        module name parameter
        //      - Store virtual module data in output parameter
        //

        //      - Check if module name in distribution list corresponds to 
        //        module name parameter

        // Copy module name to aux variable in order to avoid
        // modification of read only memory
        _tcscpy (_aux_module_name, pModuleName);
        _rc_cmp = _tcscmp (_tcsupr (_dl_data.reg_data.component.name), 
                           _tcsupr (_aux_module_name));
        // Check module name against file name
        if ( CheckFile )
        {
          if ( _rc_cmp != 0 )
          {
            // Distribution list does not correspond to this module
            Common_LoggerVersion (VERSION_LOG_CODE_2, _T("DL Module name"), pModuleName, _dl_data.reg_data.component.name);

            // Release CSP environment
            Version_ReleaseCspEnvironment (_csp_provider,
                                          _csp_key);
            // Close DL file
            CloseHandle (_dl_file);

            return VERSION_STATUS_ERROR;
          } 
        }       

        //      - Store virtual module data in output parameter
        //          - Client number
        //          - Build number
        //          - Common Database number (ODB)
        //          - Client Database number (LDB)
        //          - Component name
        //          - Version string (CC.BBB.DDD.ODB.LDB)

        //          - Client number
        pModuleVersion->component[_idx_component].expected.client = _dl_data.reg_data.component.client;
        //          - Build number
        pModuleVersion->component[_idx_component].expected.build = _dl_data.reg_data.component.build;
        //          - Common Database number
        pModuleVersion->component[_idx_component].expected.common_database = _dl_data.reg_data.component.common_database;
        //          - Client Database number
        pModuleVersion->component[_idx_component].expected.client_database = _dl_data.reg_data.component.client_database;
        //          - Component name
        _tcscpy (pModuleVersion->component[_idx_component].name, _dl_data.reg_data.component.name);
        //          - Version string (CC.BBB.DDD.ODB.LDB)
        Version_FormatModuleVersionString (pModuleVersion->component[_idx_component].expected.client,
                                           pModuleVersion->component[_idx_component].expected.build,
                                           pModuleVersion->component[_idx_component].expected.common_database,
                                           pModuleVersion->component[_idx_component].expected.client_database,
                                           pModuleVersion->component[_idx_component].expected.version);

        //      - Process next register
        _idx_register++;
        _idx_component++;
      break;

      default :
        //  - From third register corresponds to component register.
        //      - Get full component name (full path + component name) 
        //      - Store expected component data in output parameter
        //      - Process next register

        //      - Get full component name (full path + component name) 
        Version_FormatFullPathName (pModulePath,
                                    _dl_data.reg_data.component.name,
                                    _full_component_name);

        // Component digital signature is not checked when application
        // is running under development environment. Development 
        // environment is set ON or OFF using the LKS_VC_DEV environment
        // variable created for this operation. Values that user can set
        // to this variable are the following
        //
        // 0 -> Not development environment (application checks component signature)
        // 1 -> Development environment (application does not check component signature)
        _rc_bool = Version_IsDevelopmentEnvironment ();
        if ( ! _rc_bool )
        {
          // It is not development environment
          //    - Validate digital signature of specific component
          // Note: Signature is going to be validated only if the file is being checked
          if ( CheckFile )
          {
            _rc_comp = Version_ValidateSignature (_full_component_name, 
                                                  _csp_provider,
                                                  _csp_key,
                                                  0,
                                                  _dl_data.reg_data.component.digital_signature);
            if ( _rc_comp != VERSION_STATUS_OK )
            {
              // Error in signature validation
              // Write Logger
              _stprintf (_log_param1, _T("%hu"), _rc_comp);
              Common_LoggerVersion (VERSION_LOG_CODE_4, _log_param1, _T("Version_ValidateSignature"), _dl_data.reg_data.component.name);

              pSignatureCheck->signature_status[_idx_component] = FALSE;
            } // if
          } // if
        } // if

        //      - Store expected component data in output parameter
        //          - Client number
        //          - Build number
        //          - Component name
        //          - Version string (CC.BBB)

        //          - Client number
        pModuleVersion->component[_idx_component].expected.client = _dl_data.reg_data.component.client;
        //          - Build number
        pModuleVersion->component[_idx_component].expected.build = _dl_data.reg_data.component.build;
        //          - Common Database number
        pModuleVersion->component[_idx_component].expected.common_database = _dl_data.reg_data.component.common_database;
        //          - Client Database number
        pModuleVersion->component[_idx_component].expected.client_database = _dl_data.reg_data.component.client_database;
        //          - Component name
        _tcscpy (pModuleVersion->component[_idx_component].name, _dl_data.reg_data.component.name);
        //          - Version string (CC.BBB)
        Version_FormatVersionString (pModuleVersion->component[_idx_component].expected.client,
                                     pModuleVersion->component[_idx_component].expected.build,
                                     pModuleVersion->component[_idx_component].expected.version);

        //      - Process next register
        _idx_register++;
        _idx_component++;
      break;
    } // switch
  } // while

  // Components found
  // Set number of module components
  pModuleVersion->num_components = _idx_component;

  // Check if it is an empty DL file or this module
  // does not have components
  if ( pModuleVersion->num_components <= 1 )
  {
    // Physical components not found in DL file
    // Set 0 as number of components
    pModuleVersion->num_components = 0;
    // Close DL file
    CloseHandle (_dl_file);

    return VERSION_STATUS_COMPONENT_NOT_FOUND;
  } 

  // Close DL file
  CloseHandle (_dl_file);

  return VERSION_STATUS_OK;
} // Version_GetDLData

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE : Validate electronic signature of a file
//
//  PARAMS :
//      - INPUT :
//          - pFileName: Full file name (full path + file name)
//          - CspProvider : CSP provider to create a hash object
//          - CspKey: Encryption key which digital signature was generated from
//          - Offset: File offset where hashing process will start.
//          - pSignature: Digital signature of file to validate it against
//                        calculated hash   
//
//      - OUTPUT :
//
// RETURNS :
//          - VERSION_STATUS_OK
//          - VERSION_STATUS_ERROR
//          - VERSION_STATUS_SIGNATURE_NOT_VALIDATED
//
//   NOTES : The Offset parameter is used in this way:
//          - If offset is 0 (zero), hashing process will start from beginning
//            of file to end of file
//          - If offset has a value (for example 10) hashing process will start
//            in file byte pointed by this value (in this case from byte 10) to
//            the end of file

WORD        Version_ValidateSignature (TCHAR      * pFileName,
                                       HCRYPTPROV CspProvider,
                                       HCRYPTKEY  CspKey,
                                       DWORD      Offset,
                                       BYTE       * pSignature)
{
  TCHAR             _function_name [] = _T("Version_ValidateSignature");
  TYPE_LOGGER_PARAM _log_param1;
  BYTE              _component_buffer [VERSION_SIZE_COMPONENT_BUFFER];
  HANDLE            _file;
  HCRYPTHASH        _csp_hash;
  DWORD             _num_bytes_read;
  BOOL              _rc_bool;
  DWORD             _rc_pos;

  // Open file
  _file = CreateFile (pFileName, 
                      GENERIC_READ,
                      FILE_SHARE_READ,
                      NULL,
                      OPEN_EXISTING,
                      FILE_FLAG_SEQUENTIAL_SCAN,
                      NULL);

  if ( _file == INVALID_HANDLE_VALUE )
  {
    // Could not open file or file not found
    // Write Logger
    _stprintf (_log_param1, _T("%lu"), GetLastError ());
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("CreateFile"), _T(""));

    return VERSION_STATUS_DL_NOT_FOUND;
  } 

  // Set file pointer to initial hashing position (depending on the Offset parameter)
  _rc_pos = SetFilePointer (_file,
                            Offset,
                            NULL,
                            FILE_BEGIN);

  if ( _rc_pos == INVALID_SET_FILE_POINTER )
  {
    // Could not set file pointer in initial hashing position
    // Write Logger
    _stprintf (_log_param1, _T("%lu"), GetLastError ());
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("SetFilePointer"), _T(""));

    // Close file
    CloseHandle (_file);

    return VERSION_STATUS_ERROR;
  } 

  // Create hash object to store hashed data
  _rc_bool = Version_CryptCreateHash (CspProvider,
                                      CALG_SHA1,
                                      0,
                                      0,
                                      &_csp_hash);
  if ( ! _rc_bool )
  {
    // Error creating hash object
    _stprintf (_log_param1, _T("0x%X"), _rc_bool);
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("Version_CryptCreateHash"), NULL);

    // Close file
    CloseHandle (_file);

    return FALSE;
  } 

  // Read all file in portions of VERSION_SIZE_COMPONENT_BUFFER size
  // and hashing every one
  while ( TRUE )
  {
    _rc_bool = ReadFile (_file, 
                         _component_buffer, 
                         VERSION_SIZE_COMPONENT_BUFFER, 
                         &_num_bytes_read,
                         NULL);
    if ( !_rc_bool )
    {
      // Error reading file
      // Write Logger
      _stprintf (_log_param1, _T("%lu"), GetLastError ());
      Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("ReadFile"), NULL);

      // Destroy hash object
      _rc_bool = Version_CryptDestroyHash (_csp_hash);
      if ( ! _rc_bool )
      {
        _stprintf (_log_param1, _T("0x%X"), _rc_bool);
        Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("Version_CryptDestroyHash"), NULL);
      }

      // Close file
      CloseHandle (_file);

      return VERSION_STATUS_ERROR;
    } 

    if ( _num_bytes_read == 0 )
    {
      // End of file. Exit from loop
      break;
    } 

    // Hash data read from component file 
    _rc_bool = Version_CryptHashData (_csp_hash,
                                      _component_buffer,
                                      _num_bytes_read,
                                      0);
    if ( ! _rc_bool )
    {
      // Error hashing file data
      _stprintf (_log_param1, _T("0x%X"), _rc_bool);
      Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("Version_CryptHashData"), _T(""));
 
      // Destroy hash object
      _rc_bool = Version_CryptDestroyHash (_csp_hash);
      if ( ! _rc_bool )
      {
        _stprintf (_log_param1, _T("0x%X"), _rc_bool);
        Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("Version_CryptDestroyHash"), NULL);
      }

      // Close file
      CloseHandle (_file);

      return VERSION_STATUS_ERROR;
    } 
  } // while

  // Verify signature
  _rc_bool = Version_CryptVerifySignature (_csp_hash,
                                          pSignature,
                                          VERSION_SIZE_DIGITAL_SIGNATURE,
                                          CspKey,
                                          NULL,
                                          0);
  if ( ! _rc_bool )
  {
    // Error verifying digital signature
    _stprintf (_log_param1, _T("0x%X"), _rc_bool);
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("Version_CryptVerifySignature"), _T(""));

    // Destroy hash object
    _rc_bool = Version_CryptDestroyHash (_csp_hash);
    if ( ! _rc_bool )
    {
      _stprintf (_log_param1, _T("0x%X"), _rc_bool);
      Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("Version_CryptDestroyHash"), NULL);
    }

    // Close file
    CloseHandle (_file);

    return VERSION_STATUS_SIGNATURE_NOT_VALIDATED;
  } 

  // Signature verification successfully 
  // Destroy hash object
  _rc_bool = Version_CryptDestroyHash (_csp_hash);
  if ( ! _rc_bool )
  {
    _stprintf (_log_param1, _T("0x%X"), _rc_bool);
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("Version_CryptDestroyHash"), NULL);
  }


  // Close file
  CloseHandle (_file);

  return VERSION_STATUS_OK;
} // Version_ValidateSignature

//------------------------------------------------------------------------------
// PURPOSE : Set Cryptographic Service Provider (CSP) environment
//
//  PARAMS :
//      - INPUT :
//          - pKeyBlob: Key Blob from encryption key will be extracted
//
//      - OUTPUT :
//          - pCspProvider: Provider of the CSP context
//          - pCspKey: Key used to encrypt hash
//
// RETURNS :
//          - TRUE on success
//          - FALSE on error
//
//   NOTES : In order to create CSP environment, this function is in charge of:
//          - Acquire CSP context
//          - Extract encryption key from key blob

BOOL          Version_SetCspEnvironment (BYTE       * pKeyBlob,
                                         HCRYPTPROV * pCspProvider,
                                         HCRYPTKEY  * pCspKey)
{
  TCHAR               _function_name [] = _T("Version_SetCspEnvironment");
  TYPE_LOGGER_PARAM   _log_param1;
  BOOL                _rc;
 
  // Get Cryptographic Service Provider (CSP) context
  _rc = Version_CryptAcquireContext (pCspProvider,
                                     NULL,
                                     MS_DEF_PROV,
                                     PROV_RSA_FULL,
                                     CRYPT_VERIFYCONTEXT);
  if ( ! _rc )
  {
    // Error getting CSP context
    _stprintf (_log_param1, _T("0x%X"), _rc);
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("Version_CryptAcquireContext"), NULL);

    return FALSE;
  } 

  // Import encryption key from key blob
  _rc = Version_CryptImportKey (* pCspProvider,
                                pKeyBlob,
                                VERSION_SIZE_KEY_BLOB,
                                0,
                                0,
                                pCspKey);
  if ( ! _rc )
  {
    // Error importing encryption key
    _stprintf (_log_param1, _T("0x%X"), _rc);
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("Version_CryptImportKey"), NULL);

    // Release previous acquired context
    _rc = Version_CryptReleaseContext (* pCspProvider, 0);
    if ( ! _rc )
    {
      _stprintf (_log_param1, _T("0x%X"), _rc);
      Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("Version_CryptReleaseContext"), NULL);
    }

    return FALSE;
  } 

  return TRUE;
} // Version_SetCspEnvironment

//------------------------------------------------------------------------------
// PURPOSE : Release Cryptographic Service Provider (CSP) environment
//
//  PARAMS :
//      - INPUT :
//          - CspProvider: Provider of the CSP context
//          - CspKey: Key used to encrypt hash
//
//      - OUTPUT :
//
// RETURNS :
//
//   NOTES : In order to release CSP environment, this function is in charge of:
//          - Release acquire CSP context
//          - Remove imported encryption key

void          Version_ReleaseCspEnvironment (HCRYPTPROV CspProvider,
                                             HCRYPTKEY  CspKey)
{
  TCHAR               _function_name [] = _T("Version_ReleaseCspEnvironment");
  TYPE_LOGGER_PARAM   _log_param1;
  BOOL                _rc;
  
  // Destroy current key
  _rc = Version_CryptDestroyKey (CspKey);
  if ( ! _rc )
  {
    _stprintf (_log_param1, _T("0x%X"), _rc);
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("Version_CryptDestroyKey"), NULL);
  }

  // Release current CSP context
  _rc = Version_CryptReleaseContext (CspProvider, 0);
  if ( ! _rc )
  {
    _stprintf (_log_param1, _T("0x%X"), _rc);
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("Version_CryptReleaseContext"), NULL);
  }

  return;
} // Version_ReleaseCspEnvironment

//------------------------------------------------------------------------------
// PURPOSE : Read from a file and decrypt content read
//
//  PARAMS :
//      - INPUT :
//          - File: File handle
//
//      - OUTPUT :
//          - pOutBuffer: Read data
//
// RETURNS :
//          - VERSION_STATUS_OK
//          - VERSION_STATUS_ERROR
//          - VERSION_STATUS_EOF
//
//   NOTES : 

DWORD          Version_GetComponentFromFile (HANDLE    File,
                                             BYTE      * pOutBuffer)
{
  TCHAR             _function_name [] = _T("Version_GetComponentFromFile");
  TYPE_LOGGER_PARAM _log_param1;
  TYPE_LOGGER_PARAM _log_param2;
  TYPE_DL_REGISTER  * p_dl_register;
  BYTE              _encrypt_buffer [VERSION_SIZE_DL_REGISTER];
  DWORD             _bytes_read;
  DWORD             _calculated_crc;
  BOOL              _rc;

  _rc = ReadFile (File, 
                  _encrypt_buffer,
                  sizeof (_encrypt_buffer),
                  &_bytes_read,
                  NULL);

  if ( ! _rc )
  {
    // Error reading file
    // Write Logger
    _stprintf (_log_param1, _T("%lu"), GetLastError ());
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("ReadFile"), NULL);

    return VERSION_STATUS_ERROR;
  } 

  if ( _bytes_read == 0 )
  {
    // End of file
    return VERSION_STATUS_EOF;
  } 

  // Decrypt register
  _rc = Version_Encrypt (_encrypt_buffer, 
                         VERSION_SIZE_DL_REGISTER, 
                         pOutBuffer, 
                         VERSION_SIZE_DL_REGISTER, 
                         (BYTE *) VERSION_ENCRYPT_KEY,
                         VERSION_DES_DECRYPT, 
                         VERSION_ENCRYPT_DES_TRIPLE);
  if ( ! _rc )
  {
    // Error decrypting buffer
    // Write Logger
    _stprintf (_log_param1, _T("%hu"), _rc);
    Common_LoggerVersion (VERSION_LOG_CODE_1, _log_param1, _T("Version_Encrypt"), NULL);

    return VERSION_STATUS_ERROR;
  } 
  
  // Compute CRC 32 excluding CRC field
  _calculated_crc = Version_Crc32_ComputeCRC (pOutBuffer, 
                                              VERSION_SIZE_DL_REGISTER - VERSION_SIZE_CRC_32);

  // Check CRC in register against calculated CRC
  p_dl_register = (TYPE_DL_REGISTER *) pOutBuffer;
  if ( p_dl_register->crc_32 != _calculated_crc )
  {
    // Error in register CRC
    _stprintf (_log_param1, _T("%lu"), p_dl_register->crc_32);
    _stprintf (_log_param2, _T("%lu"), _calculated_crc);
    Common_LoggerVersion (VERSION_LOG_CODE_2, _T("Register CRC"), _log_param1, _log_param2);

    return VERSION_STATUS_ERROR;
  } 

  return VERSION_STATUS_OK;
} // Version_GetComponentFromFile

//------------------------------------------------------------------------------
// PURPOSE : Check if it is a development environment reading development
//          environment system variable
//
//  PARAMS :
//      - INPUT :
//
//      - OUTPUT :
//
// RETURNS :
//          - TRUE  Development environment
//          - FALSE Not development environment
//
//   NOTES : 

BOOL        Version_IsDevelopmentEnvironment (void)
{
  TCHAR   _environment_variable_value [1 + 1];
  int     _rc_cmp;
  DWORD   _rc;

#if defined(LKT_VERSION) 
  return TRUE;
#endif

  _rc = GetEnvironmentVariable (VERSION_DEV_ENV_VAR_NAME,
                                _environment_variable_value,
                                sizeof (_environment_variable_value));
  if ( _rc > 0 )
  {
    // Environment variable is present. Check if its value
    // is 0 -> Not development environment 
    //    1 -> Development environment
    _rc_cmp = _tcscmp (_environment_variable_value, VERSION_DEVELOPMENT_ENVIRONMENT);
    if ( _rc_cmp == 0 )
    {
      // Development environment
      return TRUE;
    } 
  } 

  // Environment variable not present or it is not 
  // development environment value
  return FALSE;

} // Version_IsDevelopmentEnvironment