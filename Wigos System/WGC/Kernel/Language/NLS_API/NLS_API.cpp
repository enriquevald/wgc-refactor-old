//------------------------------------------------------------------------------
// Copyright � 2002 Win Systems
//------------------------------------------------------------------------------
//
// MODULE NAME:   NLS_API.CPP
// DESCRIPTION:   Functions and Methods to suport NLS
// AUTHOR:        Carlos A. Costa
// CREATION DATE: 05-03-2002
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------  ------ ----------------------------------------------------------
// 05-03-2002  CC     Initial release
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//   INCLUDES
//------------------------------------------------------------------------------
#define INCLUDE_NLS_API
#define INCLUDE_PRIVATE_LOG
#include "CommonDef.h"

//------------------------------------------------------------------------------
// PRIVATE CONSTANTS
//------------------------------------------------------------------------------
#define NLS_API_NAME                _T("NLS_API")
#define NLS_MAX_DLL_NAME            40
#define NLS_DLL_NAME                _T("NLS_%03d.DLL")

// Hardcoded messages, independent of dll language
#define NLS_PRIVATE_MESSAGE_INI     65000
#define NLS_MAX_PRIVATE_MESSAGES    2
#define NLS_MAX_LOGGER_MESSAGES     11

#define IDS_STRING_NOT_FOUND        65000
#define IDS_STRING65001             65001

// INITIAL and FINAL ID's for Logger messages
#define ID_LOGGER_MESSAGE_INITIAL   4501
#define ID_LOGGER_MESSAGE_FINAL     4509
// ID's for Logger messages

//------------------------------------------------------------------------------
// PRIVATE DATATYPES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PRIVATE DATA STRUCTURES
//------------------------------------------------------------------------------

static TCHAR  PrivateStrings [NLS_MAX_PRIVATE_MESSAGES][NLS_MSG_STRING_MAX_LENGTH] =
{
  /* IDS_STRING_NOT_FOUND  */    _T("NLS string not found. ID=%d; %1; %2; %3; %4; %5"),
  /* IDS_STRING65001       */    _T("%1 %2 %3 %4 %5")
};

static TCHAR PrivateLoggerMessages [NLS_MAX_LOGGER_MESSAGES][NLS_MSG_STRING_MAX_LENGTH] =
{
  /* ID_LOGGER_MESSAGE_INITIAL      (1)  */  _T("NLS string not found. ID=%1;"),
  /* ID_LOGGER_MESSAGE_INITIAL + 1  (2)  */  _T("Error=%1 calling to %2."),
  /* ID_LOGGER_MESSAGE_INITIAL + 2  (3)  */  _T("Error=%1 Expected=%2 Received=%3."),
  /* ID_LOGGER_MESSAGE_INITIAL + 3  (4)  */  _T("Error=%1 calling to BD. %2%3"),
  /* ID_LOGGER_MESSAGE_INITIAL + 4  (5)  */  _T("Exception: %1%2%3"),
  /* ID_LOGGER_MESSAGE_INITIAL + 5  (6)  */  _T("Sentence: %1%2%3"),
  /* ID_LOGGER_MESSAGE_INITIAL + 6  (7)  */  _T("Unexpected value for %1: %2."),
  /* ID_LOGGER_MESSAGE_INITIAL + 7  (8)  */  _T("%1%2%3")
  /* ID_LOGGER_MESSAGE_INITIAL + 8  (9)  */  _T("Error=%1 calling to %2: %3")
  /* ID_LOGGER_MESSAGE_INITIAL + 9  (10) */  _T("Incorrect version %1. Expected=%2 Found=%3.")
  /* ID_LOGGER_MESSAGE_INITIAL + 10 (11) */  _T("Invalid LK System license.")
};

// End private messages

static DWORD        GLB_CurrentLanguage = LANG_NEUTRAL;
static HINSTANCE    GLB_CurrentDllHandle = NULL;

//------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------
static WORD NLS_FormatString (const TCHAR       * pFormatText,
                              const TCHAR       * pParam1,
                              const TCHAR       * pParam2,
                              const TCHAR       * pParam3,
                              const TCHAR       * pParam4,
                              const TCHAR       * pParam5,
                              int               TextLength,
                              TCHAR             * pResultString);

static WORD NLS_GetPrivateString (const DWORD   StringId,
                                  TCHAR         * pText,
                                  int           TextLength);

//------------------------------------------------------------------------------
// PRIVATE FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE: Replace n params in one string
//
// PARAMETERS:
//   - INPUT:
//       FormatText: String to replace
//       Param1 : first param to replace in string
//       Param2 : second param to replace in string
//       Param3 : third param to replace in string
//       Param4 : fourth param to replace in string
//       Param5 : five param to replace in string
//       TextLength: Max number of characters in string
//
//   - OUTPUT:
//       ResultString: String with replaced params
//
// RETURN:
//   NLS_STATUS_OK
//   NLS_STATUS_ERROR

static WORD NLS_FormatString (const TCHAR   * pFormatText,
                              const TCHAR   * pParam1,
                              const TCHAR   * pParam2,
                              const TCHAR   * pParam3,
                              const TCHAR   * pParam4,
                              const TCHAR   * pParam5,
                              int           TextLength,
                              TCHAR         * pResultString)
{
  int   _current_idx_format;                        // Index to FormatText
  int   _current_idx_result;                        // Index to ResultString
  int   _idx_param_replace;                         // Index of parameter to replace
  int   _current_idx_param;                         // Index to param string
  TCHAR _param_replace [NLS_MSG_PARAM_MAX_LENGTH + 1];  // Param to replace

  memset ((void *) _param_replace, 0, sizeof(_param_replace));

  // initialize local variables
  _current_idx_format = 0;
  _current_idx_result = 0;

  // Replace parameters
  while ( _current_idx_result < TextLength )
  {
    // if we find % then we replace it with params
    if (   pFormatText [_current_idx_format] == _T('%')
        && (_current_idx_format + 1 ) < TextLength )
    {

      // search param to replace if exists
      if ( _istdigit(pFormatText[_current_idx_format + 1]) )
      {
        _idx_param_replace = pFormatText[_current_idx_format + 1] - _T('0');
        _param_replace[0] = 0x00;
        switch ( _idx_param_replace )
        {
          case 1:
            if ( pParam1 != NULL )
            {
              _tcsncpy (_param_replace, pParam1, NLS_MSG_PARAM_MAX_LENGTH);
            }
          break;

          case 2:
            if ( pParam2 != NULL )
            {
              _tcsncpy(_param_replace,pParam2,NLS_MSG_PARAM_MAX_LENGTH);
            }
          break;

          case 3:
            if ( pParam3 != NULL )
            {
              _tcsncpy(_param_replace,pParam3,NLS_MSG_PARAM_MAX_LENGTH);
            }
          break;

          case 4:
            if ( pParam4 != NULL )
            {
              _tcsncpy(_param_replace,pParam4,NLS_MSG_PARAM_MAX_LENGTH);
            }
          break;

          case 5:
            if ( pParam5 != NULL )
            {
              _tcsncpy(_param_replace,pParam5,NLS_MSG_PARAM_MAX_LENGTH);
            }
          break;
        }

        _param_replace[NLS_MSG_PARAM_MAX_LENGTH] = 0x00;

        // if param exists and not is null then replace % by param
        if ( _param_replace[0] != 0x00 && _param_replace != NULL )
        {
          _current_idx_param = 0;
          while (   _current_idx_result < TextLength
                 && _param_replace[_current_idx_param] != 0x00 )
          {
            if ( _param_replace[_current_idx_param] != 0x00 )
            {
              pResultString[_current_idx_result] = _param_replace[_current_idx_param];
              _current_idx_result++;
              _current_idx_param++;
            } // if
          }
        } // if

        // If no param replace the "&xx" format vill be eliminated
        _current_idx_format += 2;

        continue;
      } // if
    } // if

    // we copy string not replace with params
    pResultString[_current_idx_result] = pFormatText[_current_idx_format];
    _current_idx_result++;
    _current_idx_format++;
  } // while

  // Add /0 to string
  pResultString[_current_idx_result] = 0x00;

  return NLS_STATUS_OK;
}


//------------------------------------------------------------------------------
// PURPOSE: Get internal message, not dependant of dll lenguage
//
// PARAMETERS:
//      - INPUT:
//          - StringId : Internal message identifier
//          - TextLength : Max length of string
//
//      - OUTPUT:
//          - pText : Internal message string
//
//  RETURN:
//    NLS_STATUS_OK
//    NLS_STATUS_ERROR

static WORD NLS_GetPrivateString (const DWORD   StringId,
                                  TCHAR         * pText,
                                  int           TextLength)
{
  int   _index_message;

  // index message ok ?
  if (   StringId < NLS_PRIVATE_MESSAGE_INI
      || StringId > (NLS_PRIVATE_MESSAGE_INI + NLS_MAX_PRIVATE_MESSAGES) )
  {
    return NLS_STATUS_ERROR;
  } // if

  // message defined ?
  _index_message = StringId - NLS_PRIVATE_MESSAGE_INI;
  if (   PrivateStrings[_index_message][0] == 0x00
      || PrivateStrings[_index_message] == NULL )
  {
    return NLS_STATUS_ERROR;
  } // if

  _stprintf (pText, _T("%.*s"), TextLength - 1, PrivateStrings[_index_message]);

  return NLS_STATUS_OK;
}

//------------------------------------------------------------------------------
// PURPOSE: Get internal message, not dependant of dll lenguage
//
// PARAMETERS:
//      - INPUT:
//          - StringId : Internal message identifier
//          - TextLength : Max length of string
//
//      - OUTPUT:
//          - pText : Internal message string
//
//  RETURN:
//    NLS_STATUS_OK
//    NLS_STATUS_ERROR

static WORD NLS_GetPrivateLogMessage (const DWORD   StringId,
                                      TCHAR         * pText,
                                      int           TextLength)
{
  int   _index_message;

  // index message ok ?
  if (   StringId < ID_LOGGER_MESSAGE_INITIAL
      || StringId > ID_LOGGER_MESSAGE_FINAL )
  {
    return NLS_STATUS_ERROR;
  } // if

  // message defined ?
  _index_message = StringId - ID_LOGGER_MESSAGE_INITIAL;
  if (   PrivateLoggerMessages[_index_message][0] == 0x00
      || PrivateLoggerMessages[_index_message] == NULL )
  {
    return NLS_STATUS_ERROR;
  } // if

  _stprintf (pText, _T("%.*s"), TextLength - 1, PrivateLoggerMessages[_index_message]);

  return NLS_STATUS_OK;
}

//------------------------------------------------------------------------------
// PUBLIC FUNCTIONS IMPLEMENTATION
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// PURPOSE: Choose the language that will use in all user interface elements.
//
// PARAMETERS:
//      - INPUT:
//          - LanguageCode: Language code to set active
//
//      - OUTPUT:
//
// RETURN:
//   NLS_STATUS_OK
//   NLS_STATUS_ERROR

NLS_API WORD WINAPI NLS_SetLanguage (DWORD LanguageCode)
{
  TCHAR         _dll_name [NLS_MAX_DLL_NAME];
  TCHAR         _log_param [NLS_MSG_PARAM_MAX_LENGTH];
  HINSTANCE     _dll_handle;

  // Load library
  _stprintf (_dll_name, NLS_DLL_NAME, LanguageCode);
  _dll_handle = LoadLibrary ((LPCTSTR) _dll_name);

  if ( _dll_handle == NULL )
  {
    _stprintf (_log_param, _T("%ld"), GetLastError());
    PrivateLog (2, NLS_API_NAME, _T(__FUNCTION__), _log_param,  _T("LoadLibrary"));

    return NLS_STATUS_ERROR;
  } // if

  if ( GLB_CurrentDllHandle != NULL )
  {
    if ( FreeLibrary (GLB_CurrentDllHandle) == 0 )
    {
      _stprintf (_log_param, _T("%ld"), GetLastError());
      PrivateLog (2, NLS_API_NAME, _T(__FUNCTION__), _log_param, _T("FreeLibrary"));
    } // if
  } // if

  GLB_CurrentLanguage = LanguageCode;
  GLB_CurrentDllHandle = _dll_handle;

  return NLS_STATUS_OK;
}

//------------------------------------------------------------------------------
// PURPOSE: Gets a string from the current language resource and formats it
//              by replacing the format specifications with the arguments received.
//
// PARAMETERS:
//   - INPUT:
//       StringId: String identifier of the resource string table
//       Param1: first param to replace in string identifier
//       Param2: second param to replace in string identifier
//       Param3: third param to replace in string identifier
//       Param4: fourth param to replace in string identifier
//       Param5: five param to replace in string identifier
//
//   - OUTPUT:
//       ResultString: result of replace the params in the string identifier
//
// RETURN:
//   (WORD) Return error code or function success
//   NLS_STATUS_OK
//   NLS_STATUS_ERROR
//   NLS_STATUS_DLL_NOT_LOADED
//   NLS_STATUS_STRING_NOT_EXIST

NLS_API WORD WINAPI NLS_GetString (const DWORD  StringId,
                                   TCHAR        * pResultString,
                                   const TCHAR  * pParam1,
                                   const TCHAR  * pParam2,
                                   const TCHAR  * pParam3,
                                   const TCHAR  * pParam4,
                                   const TCHAR  * pParam5)
{
  TCHAR     _text [NLS_MSG_STRING_MAX_LENGTH];
  TCHAR     _temp [NLS_MSG_STRING_MAX_LENGTH];
  WORD      _rc;
  TCHAR     _log_param [NLS_MSG_STRING_MAX_LENGTH];
  int       _length;

  memset ((void *) _text, 0, sizeof(_text));
  memset ((void *) _temp, 0, sizeof(_temp));

  // Limit the pattern string to NLS_MSG_STRING_MAX_LENGTH-1-3
  // NLS_MSG_STRING_MAX_LENGTH-1 because it's the maximum
  // -3 because the %d takes 2 chars but, when replacing, it will take
  // a maximum of 5(WORD), hence 2-5=-3

  // Check parameters
  if ( pResultString == NULL )
  {
    return NLS_STATUS_ERROR;
  }

  _length = NLS_MSG_STRING_MAX_LENGTH - 1 - 3;

  _rc = NLS_STATUS_OK;

  // Resource not available
  if ( GLB_CurrentDllHandle == NULL )
  {
    if (   StringId >= ID_LOGGER_MESSAGE_INITIAL 
        && StringId <= ID_LOGGER_MESSAGE_FINAL )
    {
      if ( NLS_GetPrivateLogMessage (StringId, _text, _length) == NLS_STATUS_ERROR )
      {
        _tcscpy (pResultString, _T(""));

        return NLS_STATUS_ERROR;
      }

      NLS_FormatString (_text,
                        pParam1,
                        pParam2,
                        pParam3,
                        pParam4,
                        pParam5,
                        _length,
                        pResultString);
    }
    else 
    {
      if ( NLS_GetPrivateString (IDS_STRING_NOT_FOUND, _temp, _length) == NLS_STATUS_ERROR )
      {
        _tcscpy (pResultString, _T(""));

        return NLS_STATUS_ERROR;
      }

      NLS_FormatString (_temp,
                        pParam1,
                        pParam2,
                        pParam3,
                        pParam4,
                        pParam5,
                        _length,
                        _text);

      _stprintf (pResultString, _text, StringId);
    }

    return NLS_STATUS_DLL_NOT_LOADED;
  }

  // Get the message from the resource
  if ( ! LoadString (GLB_CurrentDllHandle, (UINT) StringId, _text, _length) )
  {
    _stprintf (_log_param, _T("%ld"), StringId);
    PrivateLog (4, NLS_API_NAME, _T(__FUNCTION__), _log_param);

    _rc = NLS_STATUS_STRING_NOT_EXIST;
  }

  if ( _rc == NLS_STATUS_STRING_NOT_EXIST )
  {
    if ( NLS_GetPrivateString (IDS_STRING_NOT_FOUND, _text, _length) == NLS_STATUS_ERROR )
    {
      _tcscpy (pResultString, _T(""));

      return NLS_STATUS_ERROR;
    }
  }

  // Replace params string
  NLS_FormatString (_text,
                    pParam1,
                    pParam2,
                    pParam3,
                    pParam4,
                    pParam5,
                    _length,
                    pResultString);

  return _rc;
}

//------------------------------------------------------------------------------
// PURPOSE: Shows a GUI style message box
//
// PARAMETERS:
//    - INPUT:
//      StringId : Internal message identifier
//
//      MsgType : Type of display buttons and icons.
//        -  NLS_MSG_WARNING
//        -  NLS_MSG_ERROR
//        -  NLS_MSG_QUESTION
//        -  NLS_MSG_INFORMATION
//
//      ButtonsDisplay: Button to display
//        -  NLS_BUTTON_OK
//        -  NLS_BUTTON_OK_CANCEL
//        -  NLS_BUTTON_YES_NO
//        -  NLS_BUTTON_YES_NO_CANCEL
//        - NLS_BUTTON_ABORTRETRYIGNORE
//        - NLS_BUTTON_RETRYCANCEL
//
//     ButtonDefault: Defaukt button focused
//        -  NLS_FIRST_BUTTON
//        -  NLS_SECOND_BUTTON
//        -  NLS_THIRD_BUTTON
//
//     pButtonPressed: Button pressed
//
//     parameter 1: Parameter 1 in teh fotmat string
//     parameter 2: Parameter 2 in teh fotmat string
//     parameter 3: Parameter 3 in teh fotmat string
//     parameter 4: Parameter 4 in teh fotmat string
//     parameter 5: Parameter 5 in teh fotmat string
//
//
//   - OUTPUT:
//
// RETURN:
//   NLS_STATUS_OK
//   NLS_STATUS_ERROR

NLS_API WORD WINAPI NLS_DisplayMsgBox (const HWND     hWnd,
                                       const DWORD    StringId,
                                       const WORD     MsgType,
                                       const WORD     ButtonsDisplay,
                                       const WORD     ButtonDefault,
                                       const WORD     * pButtonPressed,
                                       const TCHAR    * pParam1,
                                       const TCHAR    * pParam2,
                                       const TCHAR    * pParam3,
                                       const TCHAR    * pParam4,
                                       const TCHAR    * pParam5 )
{
  TCHAR     _text [NLS_MSG_STRING_MAX_LENGTH];
  TCHAR     _msg_text [NLS_MSG_STRING_MAX_LENGTH];
  WORD      _rc;
  WORD      _button;

  // Assume no buttons
  _button = 0;

  // Decode message type (icon )
  switch ( MsgType )
  {
    case NLS_MSG_WARNING :
      _button |= MB_ICONEXCLAMATION;
    break;

    case NLS_MSG_ERROR :
      _button |= MB_ICONERROR;
    break;

    case NLS_MSG_QUESTION :
      _button |= MB_ICONQUESTION;
    break;

    case NLS_MSG_INFORMATION :
    default :
      _button |= MB_ICONINFORMATION;
    break;
  }

  // Decode the button parameter
  switch ( ButtonsDisplay )
  {
    case NLS_BUTTON_OK_CANCEL :
      _button |= MB_OKCANCEL;
    break;

    case NLS_BUTTON_YES_NO :
      _button |= MB_YESNO;
    break;

    case NLS_BUTTON_CANCEL :
      _button |= MB_OKCANCEL;
    break;

    case NLS_BUTTON_ABORTRETRYIGNORE :
      _button |= MB_ABORTRETRYIGNORE;
    break;

    case NLS_BUTTON_RETRYCANCEL :
      _button |= MB_RETRYCANCEL;
    break;

    case NLS_BUTTON_OK :
    default :
      _button |= MB_OK;
    break;
  }

  // Decode the default button parameter
  switch ( ButtonDefault )
  {
    case NLS_SECOND_BUTTON :
      _button |= MB_DEFBUTTON2;
    break;

    case NLS_THIRD_BUTTON :
      _button |= MB_DEFBUTTON3;
    break;

    case NLS_FIRST_BUTTON:
    default :
      _button |= MB_DEFBUTTON1;
    break;
  }

  /* Replace params string */
  _rc = NLS_GetString (StringId,
                       _text,
                       pParam1,
                       pParam2,
                       pParam3,
                       pParam4,
                       pParam5);


  _stprintf (_msg_text, _T("[LKC - %05d] %s"), StringId, _text);

  // ---------------------------------
  // TODO: Change standard caption...
  // ---------------------------------
  return MessageBox (hWnd,
                     (LPCTSTR) _msg_text,
                     (LPCTSTR) _T("Caption"),
                     _button);

} // NLS_DisplayMsgBox


//------------------------------------------------------------------------------
// PURPOSE: Splir messge into params
//
// PARAMETERS:
//   - INPUT:
//       pInputString: String identifier of the resource string table
//
//   - OUTPUT:
//       Param1: first param to contain the input message
//       Param2: second param to contain the input message
//       Param3: third param to contain the input message
//       Param4: fourth param to contain the input message
//       Param5: five param to contain the input message

//
// RETURN:
//   (WORD) Return error code or function success
//   NLS_STATUS_OK
//   NLS_STATUS_ERROR

NLS_API WORD WINAPI NLS_SplitString (const TCHAR * pInputString,
                                      TCHAR       * pParam1,
                                      TCHAR       * pParam2,
                                      TCHAR       * pParam3,
                                      TCHAR       * pParam4,
                                      TCHAR       * pParam5)
{
  TCHAR    *_pParams[NLS_MAX_PARAMS];
  int       _nParans;
  TCHAR    *_pTemp;
  WORD      _rc;
  int       _idx;

  _rc = NLS_STATUS_OK;

   // Check parameters
  if ( pInputString == NULL )
  {
    return NLS_STATUS_ERROR;
  }

  _pTemp = (TCHAR *) pInputString;
  _pParams[0] = pParam1;
  _pParams[1] = pParam2;
  _pParams[2] = pParam3;
  _pParams[3] = pParam4;
  _pParams[4] = pParam5;

  _nParans = _tcslen (_pTemp) / NLS_MSG_PARAM_MAX_LENGTH;

  if ( _tcslen (_pTemp) % NLS_MSG_PARAM_MAX_LENGTH && _nParans < NLS_MAX_PARAMS )
  {
    _nParans++;
  }

  if ( _nParans > NLS_MAX_PARAMS )
  {
    _nParans = NLS_MAX_PARAMS;
  }

  // Reset all the parameters
  for (_idx = 0; _idx < NLS_MAX_PARAMS; _idx++)
  {
    if ( _pParams[_idx] == NULL )
    {
      break;
    }

    memset ((void *) _pParams[_idx], 0, NLS_MSG_PARAM_MAX_LENGTH + 1);
  }

  for (_idx = 0; _idx < _nParans; _idx++)
  {
    if ( _pParams[_idx] == NULL )
    {
      return NLS_STATUS_ERROR;
    }

    _tcsncpy (_pParams[_idx], 
              _pTemp+ _idx * NLS_MSG_PARAM_MAX_LENGTH, 
              NLS_MSG_PARAM_MAX_LENGTH);
  }

  return _rc;
}  // NLS_SplitMessage
