// ------------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
// ------------------------------------------------------------------------------------
//
// MODULE NAME:   NLS_010_TrxAudit.rc
// DESCRIPTION:   NLS resource for Transaction auditing
// AUTHOR:        Agust� Poch
// CREATION DATE: 18-OCT-2002
//
// REVISION HISTORY:
//
// Date         Author Description
// -----------  ------ ------------------------------------------------------------------
// 18-OCT-2002  APB    Initial release.
// 23-OCT-2002  APB    Reformatted Strings (201-226; 261-286).
// 10-JAN-2003  APB    Added new strings (227-230).
// 29-APR-2003  APB    Reformatted Strings (218; 278).
// 08-MAY-2003  APB    Added new string (231).
// --------------------------------------------------------------------------------------

// WARNING !!!
//
// Do NOT modify this file with Visual Studio Resource Editor.
// This file MUST be edited using a TEXT editor.
//
// WARNING !!!

STRINGTABLE DISCARDABLE
BEGIN
  // Note that every range must finish with an empty string !!!

  // IDs from 1 to 20 are reserved for Transaction Manager Names
  // IDs from 21 to 50 are reserved for future use
  // IDs from 51 to 200 are reserved for short ranges (30 ids each)
  // IDs from 201 to 499 are reserved for long ranges (60 ids each)

  // Application Names: 1-20
  NLS_ID_TRX_AUDIT(1)     "Batch"
  NLS_ID_TRX_AUDIT(2)     "Online"
  NLS_ID_TRX_AUDIT(3)     "Jackpot"
  NLS_ID_TRX_AUDIT(4)     "Software"
  NLS_ID_TRX_AUDIT(5)     ""

  // IDs from 51 to 200 are reserved for short ranges (30 ids each)
  // Short Range #1: Jackpot Trx. Mgr.
  NLS_ID_TRX_AUDIT(51)    "Jackpot - Trx01"
  NLS_ID_TRX_AUDIT(52)    "Jackpot - Trx02"
  NLS_ID_TRX_AUDIT(53)    "Jackpot - Trx03"
  NLS_ID_TRX_AUDIT(54)    "Jackpot - Trx04"
  NLS_ID_TRX_AUDIT(55)    "Jackpot - Trx05"
  NLS_ID_TRX_AUDIT(56)    "Jackpot - Trx06"
  NLS_ID_TRX_AUDIT(57)    ""



  // Short Range #2: Software Trx. Mgr.
  NLS_ID_TRX_AUDIT(81)    "Software - Trx01"
  NLS_ID_TRX_AUDIT(82)    "Software - Trx02"
  NLS_ID_TRX_AUDIT(83)    "Software - Trx03"
  NLS_ID_TRX_AUDIT(84)    "Software - Trx04"
  NLS_ID_TRX_AUDIT(85)    "Software - Trx05"
  NLS_ID_TRX_AUDIT(86)    ""

  // Short Range
  NLS_ID_TRX_AUDIT(100)    "OK"
  NLS_ID_TRX_AUDIT(101)    "Database Error"
  NLS_ID_TRX_AUDIT(102)    "Already Downloaded"
  NLS_ID_TRX_AUDIT(103)    "Book Not Found"
  NLS_ID_TRX_AUDIT(104)    "Segment Not Found"
  NLS_ID_TRX_AUDIT(105)    "No Prize"
  NLS_ID_TRX_AUDIT(106)    "Wrong Control Block"
  NLS_ID_TRX_AUDIT(107)    "Does Not Exist"
  NLS_ID_TRX_AUDIT(108)    "Prize Expired"
  NLS_ID_TRX_AUDIT(109)    "Cash Not Allowed"
  NLS_ID_TRX_AUDIT(110)    "Bad Validation Code"
  NLS_ID_TRX_AUDIT(111)    "Ticket Not Valid"
  NLS_ID_TRX_AUDIT(112)    "Concession Expired"
  NLS_ID_TRX_AUDIT(113)    "Not Awarded"
  NLS_ID_TRX_AUDIT(114)    "Incorrect Keys"
  NLS_ID_TRX_AUDIT(115)    "Shutdown Error"
  NLS_ID_TRX_AUDIT(116)    "Ticket Not Sold"
  NLS_ID_TRX_AUDIT(117)    "Retry"
  NLS_ID_TRX_AUDIT(118)    "Abort"
  NLS_ID_TRX_AUDIT(119)    "Request Unpack"
  NLS_ID_TRX_AUDIT(120)    "Download Finished"
  NLS_ID_TRX_AUDIT(121)    "Shutdown in progress"
  /////////////////////////////////////////   22      
  /////////////////////////////////////////   23      
  /////////////////////////////////////////   24
  /////////////////////////////////////////   25
  /////////////////////////////////////////   26
  NLS_ID_TRX_AUDIT(127)    "Next Segment"
  /////////////////////////////////////////   28                   
  NLS_ID_TRX_AUDIT(129)    "Crypto Problems"
  /////////////////////////////////////////   30                   
  /////////////////////////////////////////   31                   
  /////////////////////////////////////////   32                   
  /////////////////////////////////////////   33                   
  NLS_ID_TRX_AUDIT(134)    "Jackpot Concession In Process"
  NLS_ID_TRX_AUDIT(135)    "Jackpot In Doubt"
  NLS_ID_TRX_AUDIT(136)    "Swdl Error"
  NLS_ID_TRX_AUDIT(137)    "Too Many Books"
  NLS_ID_TRX_AUDIT(138)    "Already Cashed"
  NLS_ID_TRX_AUDIT(139)    "Game Not Found"
  NLS_ID_TRX_AUDIT(140)    "Wrong Current Password"
  NLS_ID_TRX_AUDIT(141)    "Invalid New Password"
  NLS_ID_TRX_AUDIT(142)    "Function Not Allowed"
  NLS_ID_TRX_AUDIT(143)    "User Not Found"


  // IDs from 201 to 499 are reserved for long ranges (60 ids each)
  // Long Range #1: Batch Trx. Mgr.
  NLS_ID_TRX_AUDIT (201)    "Par�metros de Agencia"
  NLS_ID_TRX_AUDIT (202)    "Par�metros de Juego"
  NLS_ID_TRX_AUDIT (203)    "Lista de Usuarios"
  NLS_ID_TRX_AUDIT (204)    "Sincronizaci�n"
  NLS_ID_TRX_AUDIT (205)    "Estado de Agencia"
  NLS_ID_TRX_AUDIT (206)    "Estado de Dispositivos"
  NLS_ID_TRX_AUDIT (207)    "Petici�n de Inventario"
  NLS_ID_TRX_AUDIT (208)    "Bajada de Libro"
  NLS_ID_TRX_AUDIT (209)    "Ventas"
  NLS_ID_TRX_AUDIT (210)    "Retorno de Libro"
  NLS_ID_TRX_AUDIT (211)    "Informaci�n de Pozo"
  NLS_ID_TRX_AUDIT (212)    "Asignaci�n de Pozo"
  NLS_ID_TRX_AUDIT (213)    "Desasignaci�n de Pozo"
  NLS_ID_TRX_AUDIT (214)    "Confirmaci�n de Pozo"
  NLS_ID_TRX_AUDIT (215)    "Petici�n de Saldos de tarjetas"
  NLS_ID_TRX_AUDIT (216)    "Transferencia de Saldos de tarjetas"
  NLS_ID_TRX_AUDIT (217)    "Mensaje de Usuario"
  NLS_ID_TRX_AUDIT (218)    "Inventario de Ventas"
  NLS_ID_TRX_AUDIT (219)    "Comprobaci�n de Integridad"
  NLS_ID_TRX_AUDIT (220)    "Cambio de Clave RSA en LKC"
  NLS_ID_TRX_AUDIT (221)    "Petici�n de cambio de Clave RSA en LKAS"
  NLS_ID_TRX_AUDIT (222)    "Confirmaci�n de cambio de Clave RSA en LKAS"
  NLS_ID_TRX_AUDIT (223)    "Petici�n de Datos Contables"
  NLS_ID_TRX_AUDIT (224)    "Comprobaci�n de Premio Mayor"
  NLS_ID_TRX_AUDIT (225)    "Cobro de Premio Mayor"
  NLS_ID_TRX_AUDIT (226)    "Reporte de Ventas por Horas"
  NLS_ID_TRX_AUDIT (227)    "Reporte de Ventas por Terminal"
  NLS_ID_TRX_AUDIT (228)    "Petici�n de Saldos no redimidos"
  NLS_ID_TRX_AUDIT (229)    "Transferencia de Saldos no redimidos"
  NLS_ID_TRX_AUDIT (230)    "Petici�n de sesi�n Batch"
  NLS_ID_TRX_AUDIT (231)    "Transferencia de Software"
  NLS_ID_TRX_AUDIT (232)    "Petici�n de Reporte Online"
  NLS_ID_TRX_AUDIT (233)    "Reporte Online Finalizado"
  NLS_ID_TRX_AUDIT (234)    "Retorno de Libros"
  NLS_ID_TRX_AUDIT (235)    "Reporte de Eventos"
  NLS_ID_TRX_AUDIT (236)    "Reporte de Comprobantes"
  NLS_ID_TRX_AUDIT (237)    "Cambio de Estado de Libro"
  NLS_ID_TRX_AUDIT (238)    "Bloqueo de Libros"
  NLS_ID_TRX_AUDIT (239)    "Cambio Contrase�a de Usuario"
  NLS_ID_TRX_AUDIT (240)    "Login"
  NLS_ID_TRX_AUDIT (241)    "Logout"
  NLS_ID_TRX_AUDIT (242)    "Shutdown"
  NLS_ID_TRX_AUDIT (243)    "Send Allowed Terminals"
  NLS_ID_TRX_AUDIT (244)    "Configured Terminals"
  NLS_ID_TRX_AUDIT (245)    "Get Pages"
  NLS_ID_TRX_AUDIT (246)    "Set Pages"
  NLS_ID_TRX_AUDIT (247)    "Wings Sales"
  NLS_ID_TRX_AUDIT (248)    "Wings N�meros Aleatorios"


  NLS_ID_TRX_AUDIT (260)    "---"

  // Long Range #2: Free
  NLS_ID_TRX_AUDIT (261)    ""

  // Long Range #3
  NLS_ID_TRX_AUDIT (319)    "%1. Retornado no pedido"
  NLS_ID_TRX_AUDIT (320)    "%1. Pedido no reportado"
  NLS_ID_TRX_AUDIT (321)    "Error en la base de datos de la agencia"
  NLS_ID_TRX_AUDIT (322)    "Segment not found: %1 <> %2 (total=%3)"
  NLS_ID_TRX_AUDIT (323)    "Error en la base de datos del centro"
  NLS_ID_TRX_AUDIT (324)    "Segmento no esperado: recibido=%1 esperado=%2 (total=%3)"
  NLS_ID_TRX_AUDIT (325)    "Sesi�n establecida."
  NLS_ID_TRX_AUDIT (326)    "Sesi�n finalizada."
  NLS_ID_TRX_AUDIT (327)    "Sesi�n abortada por desconexi�n."
  NLS_ID_TRX_AUDIT (328)    "Sesi�n abortada por inactividad."

  // Long Range #4 - Trx Mgr messages received and sent
  NLS_ID_TRX_AUDIT (381)    "Mensaje recibido."
  NLS_ID_TRX_AUDIT (382)    "Mensaje recibido: siendo procesado."
  NLS_ID_TRX_AUDIT (383)    "Mensaje recibido: ya procesado."
  NLS_ID_TRX_AUDIT (384)    "Mensaje recibido: nueva petici�n no permitida."
  NLS_ID_TRX_AUDIT (385)    "Mensaje recibido: error."
  NLS_ID_TRX_AUDIT (386)    "Mensaje recibido: inesperado."
  NLS_ID_TRX_AUDIT (387)    "Mensaje enviado."
  NLS_ID_TRX_AUDIT (388)    "Mensaje enviado: error"
  NLS_ID_TRX_AUDIT (389)    "Mensaje enviado: Reintento %1 (Timeout: %2)"
  NLS_ID_TRX_AUDIT (390)    "El n�mero de juegos habilitados excede el m�ximo. Solo se bajar�n libros para los primeros %1 juegos."
  NLS_ID_TRX_AUDIT (391)    "Versi�n %1 incorrecta, Actual %2 - Reportada %3."
  NLS_ID_TRX_AUDIT (392)    "La versi�n de uno o m�s ficheros del m�dulo %1 no es correcta."
  NLS_ID_TRX_AUDIT (393)    "Error de encriptaci�n."
  NLS_ID_TRX_AUDIT (394)    "Identificador de pozo incorrecto."
  NLS_ID_TRX_AUDIT (395)    "L�mite de Ventas superado."
  NLS_ID_TRX_AUDIT (396)    "Auditoria no inicializada."
  NLS_ID_TRX_AUDIT (397)    "Reintento de Bajada de Software %1 de %2."
  NLS_ID_TRX_AUDIT (398)    "Versi�n %1 %2 %3"
  NLS_ID_TRX_AUDIT (399)    "No responde."
  NLS_ID_TRX_AUDIT (400)    "Mensaje inesperado. Msg Id: %1; Identificador Externo: %2."
  NLS_ID_TRX_AUDIT (401)    "Reintento de mensaje %1 de %2. Msg Id: %3"
  NLS_ID_TRX_AUDIT (402)    "Problema leyendo la versi�n %1 %2 %3."
  NLS_ID_TRX_AUDIT (403)    "No hay usuarios."

  // Long Range #5
  NLS_ID_TRX_AUDIT (441)    ""

  // --- Book Inventory ---
  NLS_ID_TRX_AUDIT (450)    "Libro (J:%1 S:%2 L:%3) asignado y no reportado"
  NLS_ID_TRX_AUDIT (451)    "Libro (J:%1 S:%2 L:%3) reportado y no asignado"
  NLS_ID_TRX_AUDIT (452)    "Libro (J:%1 S:%2 L:%3) bajado"

END
