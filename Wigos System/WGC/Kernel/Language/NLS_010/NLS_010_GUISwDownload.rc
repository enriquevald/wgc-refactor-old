// ------------------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
// ------------------------------------------------------------------------------------
//
//   MODULE NAME : NLS_010_GUISwDownload.rc
//
//   DESCRIPTION : NLS resources for GUI Software Download.
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -------------------------------------------------------------------
// 05-SEP-2008 TJG    Initial release
// --------------------------------------------------------------------------------------
//
// WARNING !!!
//
// Do NOT modify this file with Visual Studio Resource Editor.
// This file MUST be edited using a TEXT editor.
//
// WARNING !!!

STRINGTABLE DISCARDABLE
BEGIN

  // IDs from 1 to 25 are reserved for buttons
  // IDs from 26 to 100 are reserved for application specific requirements
  // IDs from 101 to 200 are reserved for messages
  // IDs from 201 to 450 are reserved for Labels
  // IDs from 451 to 499 are reserved for Menu Items

  // Buttons : 1-25
  NLS_ID_GUI_SW_DOWNLOAD(1)    "Descargar"
  NLS_ID_GUI_SW_DOWNLOAD(2)    "Salir"
  NLS_ID_GUI_SW_DOWNLOAD(3)    "Nueva"
  NLS_ID_GUI_SW_DOWNLOAD(4)    "Borrar"

  // IDs from 26 to 100 are reserved for application specific requirements

  // IDs from 101 to 200 are reserved for messages
  NLS_ID_GUI_SW_DOWNLOAD(101)  "Error en el intervalo de fechas: la fecha inicial debe ser menor que la final."
  NLS_ID_GUI_SW_DOWNLOAD(102)  "Identificador de cliente inesperado: el sistema solo acepta paquetes para el cliente %1; el c�digo reportado por el paquete es el %2."
  NLS_ID_GUI_SW_DOWNLOAD(103)  "Ya existe en la base de datos un paquete para el Cliente %1, Build %2 y Tipo %3."
  NLS_ID_GUI_SW_DOWNLOAD(104)  "�Est� seguro que quiere borrar el paquete %1?"
  NLS_ID_GUI_SW_DOWNLOAD(105)  "El paquete %1 solo se pudo borrar de %2 repositorio(s)."
  NLS_ID_GUI_SW_DOWNLOAD(106)  "El paquete %1 solo se pudo copiar en %2 repositorio(s) de %3."
  NLS_ID_GUI_SW_DOWNLOAD(107)  "El paquete %1 se copi� a todos los repositorios (%2)."
  NLS_ID_GUI_SW_DOWNLOAD(108)  "El paquete %1 no se pudo copiar a ninguno de los repositorio(s) definidos (%2)."
  NLS_ID_GUI_SW_DOWNLOAD(109)  "No se ha definido ninguna direcci�n para el repositorio de versiones."
  NLS_ID_GUI_SW_DOWNLOAD(110)  "El paquete %1 ha sido borrado."
  NLS_ID_GUI_SW_DOWNLOAD(111)  "No se pudo recuperar la informaci�n de versi�n del paquete %1."
  NLS_ID_GUI_SW_DOWNLOAD(112)  "No se pudo obtener la informaci�n sobre los repositorios de la base de datos."
  NLS_ID_GUI_SW_DOWNLOAD(113)  "El paquete %1 ha reportado un tipo incorrecto (%2)."
  NLS_ID_GUI_SW_DOWNLOAD(114)  "El sistema solo acepta paquetes COMPLETOS."
  NLS_ID_GUI_SW_DOWNLOAD(115)  "No se pudo obtener informaci�n acerca de los m�dulos del paquete %1."
  NLS_ID_GUI_SW_DOWNLOAD(116)  "Se ha encontrado una nueva versi�n de GUI. �Quiere instalarla ahora?"
  NLS_ID_GUI_SW_DOWNLOAD(117)  "Reinicie la aplicaci�n para actualizar..."
  NLS_ID_GUI_SW_DOWNLOAD(118)  "El paquete %1 no se pudo borrar de ninguno de los repositorios definidos (%2)."
  NLS_ID_GUI_SW_DOWNLOAD(119)  ""
  NLS_ID_GUI_SW_DOWNLOAD(120)  "�Est� seguro que quiere borrar la licencia con fecha de caducidad %1?"
  NLS_ID_GUI_SW_DOWNLOAD(121)  "Se ha producido un error leyendo datos del Paquete de Software=%1."
  NLS_ID_GUI_SW_DOWNLOAD(122)  "Se ha producido un error leyendo el fichero de licencia %1."
  NLS_ID_GUI_SW_DOWNLOAD(123)  "Se ha producido un error leyendo datos de licencia."
  NLS_ID_GUI_SW_DOWNLOAD(124)  "La licencia se ha guardado correctamente."
  NLS_ID_GUI_SW_DOWNLOAD(125)  "�Est� seguro que quiere borrar la licencia con fecha de caducidad %1?"

  // IDs from 201 to 450 are reserved for Labels
  // Form Title
  NLS_ID_GUI_SW_DOWNLOAD(201)   "Actualizaci�n de componentes"
  NLS_ID_GUI_SW_DOWNLOAD(202)   "Build"
  NLS_ID_GUI_SW_DOWNLOAD(203)   "Versi�n %1."
  NLS_ID_GUI_SW_DOWNLOAD(204)   "Estatus"
  NLS_ID_GUI_SW_DOWNLOAD(205)   "Fecha"
  NLS_ID_GUI_SW_DOWNLOAD(206)   "Desde"
  NLS_ID_GUI_SW_DOWNLOAD(207)   "Hasta"
  NLS_ID_GUI_SW_DOWNLOAD(208)   "Tipo"
  NLS_ID_GUI_SW_DOWNLOAD(209)   "Todos"
  NLS_ID_GUI_SW_DOWNLOAD(210)   "Kiosco Win"
  NLS_ID_GUI_SW_DOWNLOAD(211)   "Cajero Win"
  NLS_ID_GUI_SW_DOWNLOAD(212)   "Wigos GUI"
  // Form Title
  NLS_ID_GUI_SW_DOWNLOAD(213)   "Estado de terminales"
  NLS_ID_GUI_SW_DOWNLOAD(214)   "Nombre Terminal"
  NLS_ID_GUI_SW_DOWNLOAD(215)   "Proveedor"
  NLS_ID_GUI_SW_DOWNLOAD(216)   "Ocupado"
  NLS_ID_GUI_SW_DOWNLOAD(217)   "Versi�n"
  NLS_ID_GUI_SW_DOWNLOAD(218)   "Libre"
  NLS_ID_GUI_SW_DOWNLOAD(219)   "Estado"
  NLS_ID_GUI_SW_DOWNLOAD(220)   "Actualizado"
  NLS_ID_GUI_SW_DOWNLOAD(221)   "Pendiente"
  NLS_ID_GUI_SW_DOWNLOAD(222)   "�ltima versi�n"
  NLS_ID_GUI_SW_DOWNLOAD(223)   "Estado"
  NLS_ID_GUI_SW_DOWNLOAD(224)   "No disponible"
  NLS_ID_GUI_SW_DOWNLOAD(225)   "Parado"
  NLS_ID_GUI_SW_DOWNLOAD(226)   "Servicio WCP"
  NLS_ID_GUI_SW_DOWNLOAD(227)   "Servicio WC2"
  NLS_ID_GUI_SW_DOWNLOAD(228)   "�ltima versi�n"
  // Form Title
  NLS_ID_GUI_SW_DOWNLOAD(229)   "Licencias"
  NLS_ID_GUI_SW_DOWNLOAD(230)   "Versi�n"
  NLS_ID_GUI_SW_DOWNLOAD(231)   "Build"
  NLS_ID_GUI_SW_DOWNLOAD(232)   "Desde"
  NLS_ID_GUI_SW_DOWNLOAD(233)   "Tipo"
  NLS_ID_GUI_SW_DOWNLOAD(234)   "Insertado"
  NLS_ID_GUI_SW_DOWNLOAD(235)   "Paquete "
  NLS_ID_GUI_SW_DOWNLOAD(236)   "Cliente"
  NLS_ID_GUI_SW_DOWNLOAD(237)   "Copia OK"
  NLS_ID_GUI_SW_DOWNLOAD(238)   "Copia no OK"
  // Form Title
  NLS_ID_GUI_SW_DOWNLOAD(239)   "Control de versiones de aplicaciones"
  NLS_ID_GUI_SW_DOWNLOAD(240)   "Aplicaci�n"
  NLS_ID_GUI_SW_DOWNLOAD(241)   "Servicios"
  NLS_ID_GUI_SW_DOWNLOAD(242)   "Wigos GUI"
  NLS_ID_GUI_SW_DOWNLOAD(243)   "Cajero WIN"
  NLS_ID_GUI_SW_DOWNLOAD(244)   "Usuario SO"
  NLS_ID_GUI_SW_DOWNLOAD(245)   "Usuario apl."
  NLS_ID_GUI_SW_DOWNLOAD(246)   "Computadora"
  NLS_ID_GUI_SW_DOWNLOAD(247)   "�ltima actualizaci�n"
  NLS_ID_GUI_SW_DOWNLOAD(248)   "Direcci�n IP"
  NLS_ID_GUI_SW_DOWNLOAD(249)   "Nunca"
  NLS_ID_GUI_SW_DOWNLOAD(250)   "Inserci�n"
  // Form Title
  NLS_ID_GUI_SW_DOWNLOAD(251)   "Detalle de licencia"
  NLS_ID_GUI_SW_DOWNLOAD(252)   "Licencia con caducidad %1"
  NLS_ID_GUI_SW_DOWNLOAD(253)   "Fecha de instalaci�n:"
  NLS_ID_GUI_SW_DOWNLOAD(254)   "Fecha de caducidad:"
  NLS_ID_GUI_SW_DOWNLOAD(255)   "V�lido hasta (inclusive)"
  NLS_ID_GUI_SW_DOWNLOAD(256)   "Wigos MultiSite GUI"

  // Form Title
  NLS_ID_GUI_SW_DOWNLOAD(300)   "Detalles del paquete de software"
  NLS_ID_GUI_SW_DOWNLOAD(301)   "Versi�n %1"
  NLS_ID_GUI_SW_DOWNLOAD(302)   "Direcci�n repositorio"
  NLS_ID_GUI_SW_DOWNLOAD(303)   "Insertado"
  NLS_ID_GUI_SW_DOWNLOAD(304)   "Tipo"
  NLS_ID_GUI_SW_DOWNLOAD(305)   "Destino"
  NLS_ID_GUI_SW_DOWNLOAD(306)   "Completo"
  NLS_ID_GUI_SW_DOWNLOAD(307)   "Parcial"
  NLS_ID_GUI_SW_DOWNLOAD(308)   ""
  NLS_ID_GUI_SW_DOWNLOAD(309)   "Paquete"
  NLS_ID_GUI_SW_DOWNLOAD(310)   "M�dulo"
  NLS_ID_GUI_SW_DOWNLOAD(311)   "Versi�n"
  NLS_ID_GUI_SW_DOWNLOAD(312)   "Tama�o (Bytes)"
  // IDs 313-320 reserved for package status
  NLS_ID_GUI_SW_DOWNLOAD(313)   "OK"
  NLS_ID_GUI_SW_DOWNLOAD(314)   "Error"
  NLS_ID_GUI_SW_DOWNLOAD(315)   ""
  NLS_ID_GUI_SW_DOWNLOAD(316)   ""
  NLS_ID_GUI_SW_DOWNLOAD(317)   ""
  NLS_ID_GUI_SW_DOWNLOAD(318)   ""
  NLS_ID_GUI_SW_DOWNLOAD(319)   ""
  NLS_ID_GUI_SW_DOWNLOAD(320)   ""

  NLS_ID_GUI_SW_DOWNLOAD(321)   "Kiosco SAS Host"
  NLS_ID_GUI_SW_DOWNLOAD(322)   "Tipo"
  NLS_ID_GUI_SW_DOWNLOAD(323)   "Kiosco"

  NLS_ID_GUI_SW_DOWNLOAD(324)   "�ltima actualizaci�n"
  NLS_ID_GUI_SW_DOWNLOAD(325)   "Conectado"
  NLS_ID_GUI_SW_DOWNLOAD(326)   "Desconectado"
  NLS_ID_GUI_SW_DOWNLOAD(327)   "Tipo"
  NLS_ID_GUI_SW_DOWNLOAD(328)   "Jackpot de Sala"
  NLS_ID_GUI_SW_DOWNLOAD(329)   "Banco M�vil"
  NLS_ID_GUI_SW_DOWNLOAD(330)   "iMB"
  NLS_ID_GUI_SW_DOWNLOAD(331)   "iStats"
  NLS_ID_GUI_SW_DOWNLOAD(332)   "Tipo: %1"
  NLS_ID_GUI_SW_DOWNLOAD(333)   "Servicio WWP Site"
  NLS_ID_GUI_SW_DOWNLOAD(334)   "Servicio WWP Center"
  NLS_ID_GUI_SW_DOWNLOAD(335)   "Servicio WXP"
  NLS_ID_GUI_SW_DOWNLOAD(336)   "PromoBOX"
  NLS_ID_GUI_SW_DOWNLOAD(337)   "Wigos MultiSite GUI"
  NLS_ID_GUI_SW_DOWNLOAD(338)   "Servicio PSA Client"
  NLS_ID_GUI_SW_DOWNLOAD(339)   "Wigos Draw GUI"
  NLS_ID_GUI_SW_DOWNLOAD(340)   "Intellia Data Service"
  NLS_ID_GUI_SW_DOWNLOAD(341)   "Servicio AFIP Client"
  NLS_ID_GUI_SW_DOWNLOAD(342)   "Servicio WSI.Protocols"

  // IDs from 451 to 499 are reserved for Menu Items
  NLS_ID_GUI_SW_DOWNLOAD(451)  "Versiones de software"
  NLS_ID_GUI_SW_DOWNLOAD(452)  "Actualizaci�n de componentes"
  NLS_ID_GUI_SW_DOWNLOAD(453)  "Estado de terminales"
  NLS_ID_GUI_SW_DOWNLOAD(454)  "Control de versiones"
  NLS_ID_GUI_SW_DOWNLOAD(455)  "Licencias"

  // FJC 12-JAN-2015 (New Menu of Wigos GUI)
  NLS_ID_GUI_SW_DOWNLOAD(456)  "Software"

END
