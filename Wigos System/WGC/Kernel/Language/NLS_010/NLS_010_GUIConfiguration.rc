// ------------------------------------------------------------------------------------
// Copyright � 2002 Win Systems Ltd.
// ------------------------------------------------------------------------------------
//
//   MODULE NAME: NLS_010_GUIConfiguration.rc
//
//   DESCRIPTION: NLS resource LKC GUIConfiguration resources
//
//        AUTHOR: Toni Jord�
//
// CREATION DATE: 13-MAR-2002
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -------------------------------------------------------------------
// 13-MAR-2002 TJG    Initial release
// --------------------------------------------------------------------------------------

// WARNING !!!
//
// Do NOT modify this file with Visual Studio Resource Editor.
// This file MUST be edited using a TEXT editor.
//
// WARNING !!!

STRINGTABLE DISCARDABLE
BEGIN

  // IDs from 1 to 50 are reserved for buttons
  // IDs from 51 to 100 are reserved for application specific requirements
  // IDs from 101 to 200 are reserved for messages
  // IDs from 201 to 450 are reserved for Labels
  // IDs from 451 to 499 are reserved for Menu Items


  // Buttons: 1-50
  NLS_ID_GUI_CONF(1)              "Importar"
  NLS_ID_GUI_CONF(2)              "Nuevo Perfil"
  NLS_ID_GUI_CONF(3)              "Nuevo Usu."
  NLS_ID_GUI_CONF(4)              "Nueva Sala"
  NLS_ID_GUI_CONF(5)              "Nuevo Oper."

  NLS_ID_GUI_CONF(7)              "(+)"
  NLS_ID_GUI_CONF(8)              "(-)"

  // Cashier Sessions Grid Header
  NLS_ID_GUI_CONF(40)             "Promo. Redimible"
  NLS_ID_GUI_CONF(41)             "Promo. No Redimible"
  NLS_ID_GUI_CONF(42)             "No Redimible a Redimible"

  // IDs from 101 to 200 are reserved for messages
  NLS_ID_GUI_CONF(101)            "Debe indicar el valor para %1."
  // %1: 'Operador', 'Distribuidor', 'Agencia'; %2: External Hierarchy Instance Identifier
  NLS_ID_GUI_CONF(102)            "Se ha producido un error al leer el %1 %2."
  // Same as 102 (female)
  NLS_ID_GUI_CONF(103)            "Se ha producido un error al leer la %1 %2."
  // %1: 'Operador', 'Distribuidor', 'Agencia'; %2: External Hierarchy Instance Identifier
  NLS_ID_GUI_CONF(104)            "Se ha producido un error al insertar el %1 %2."
  // Same as 104 (female)
  NLS_ID_GUI_CONF(105)            "Se ha producido un error al insertar la %1 %2."
  // %1: 'Operador', 'Distribuidor', 'Agencia'; %2: External Hierarchy Instance Identifier
  NLS_ID_GUI_CONF(106)            "Se ha producido un error al modificar el %1 %2."
  // Same as 106 (female)
  NLS_ID_GUI_CONF(107)            "Se ha producido un error al modificar la %1 %2."

  // %2: External Hierarchy Instance Identifier; %1: 'Operador', 'Distribuidor', ...
  NLS_ID_GUI_CONF(110)            "Ya existe un %1 con identificador %2."
  // %1: 'Operador', 'Distribuidor', 'Agencia'; %2: External Hierarchy Instance Identifier
  NLS_ID_GUI_CONF(111)            "�Est� seguro que quiere borrar el %1 %2?"
  // Same as 111 (female)
  NLS_ID_GUI_CONF(112)            "�Est� seguro que quiere borrar la %1 %2?"
  NLS_ID_GUI_CONF(113)            "La suma de ambos impuestos no debe superar el %1."
  // Same as 110 (female)
  NLS_ID_GUI_CONF(114)            "Ya existe una %1 con identificador %2."
  NLS_ID_GUI_CONF(115)            "Se ha producido un error al borrar el %1 %2."
  // Same as 115 (female)
  NLS_ID_GUI_CONF(116)            "Se ha producido un error al borrar la %1 %2."

  NLS_ID_GUI_CONF(121)            "La clave y la verificaci�n no coinciden."

  NLS_ID_GUI_CONF(124)            "%1 debe ser mayor o igual que %2."
  NLS_ID_GUI_CONF(125)            "Debe indicar un valor mayor que 0 para %1."
  // %1: UserName
  NLS_ID_GUI_CONF(126)            "La clave introducida para el usuario %1 no es v�lida."
  NLS_ID_GUI_CONF(127)            "La clave introducida para el usuario %1 no es v�lida o ya ha sido usada."
  // %1: Profile Id.
  NLS_ID_GUI_CONF(128)            "No se puede borrar el perfil '%1' porque tiene usuarios asociados."
  NLS_ID_GUI_CONF(129)            "Se ha producido un error al obtener la informaci�n de los formularios existentes."
  NLS_ID_GUI_CONF(130)            "%1 debe ser mayor que %2."

  NLS_ID_GUI_CONF(131)            "El campo %1 debe de estar entre el valor %2 y %3."

  NLS_ID_GUI_CONF(134)            "No ha seleccionado ning�n permiso de acceso.\nEste perfil de usuario es equivalente a deshabilitar los usuarios que lo utilicen.\n�Desea continuar?"
  NLS_ID_GUI_CONF(135)            "El perfil de usuario no puede ser guardado ya que concede permisos de acceso superiores a los permisos de acceso asignados al usuario %1."
  NLS_ID_GUI_CONF(136)            "El perfil de usuario %2 no podr� ser modificado ya que contiene permisos de acceso superiores a los permisos de acceso asignados al usuario %1."

  NLS_ID_GUI_CONF(146)            "No se puede borrar el usuario porque tiene informaci�n de auditor�a asociada."

  NLS_ID_GUI_CONF(193)            "�Seguro que desea guardar los cambios?"
  NLS_ID_GUI_CONF(194)            "El porcentaje no puede ser superior a 100%."
  NLS_ID_GUI_CONF(195)            "El porcentaje no puede ser superior a 1000%."
  NLS_ID_GUI_CONF(196)            "No ha seleccionado ning�n servidor de la lista de autorizados.\n�Desea continuar?"
  NLS_ID_GUI_CONF(197)            "El nombre de juego %1 introducido para %2 no existe. �Desea crear un nuevo juego con este nombre?"
  NLS_ID_GUI_CONF(198)            "La fecha de retirada seleccionada no puede ser posterior a la jornada de ma�ana."
  NLS_ID_GUI_CONF(199)            "La fecha de retirada seleccionada debe ser anterior a la fecha de retirada (%2) del terminal %1."
  NLS_ID_GUI_CONF(200)            "La fecha de retirada seleccionada debe ser posterior a la �ltima sesi�n de juego del terminal %1."

  // IDs from 201 to 450 are reserved for Labels
  NLS_ID_GUI_CONF(201)            "Configuraci�n de operaciones generales"
  NLS_ID_GUI_CONF(202)            "Direcci�n IP"
  NLS_ID_GUI_CONF(203)            "Nombre de la Base de Datos"
  NLS_ID_GUI_CONF(204)            "d�a(s)"
  NLS_ID_GUI_CONF(205)            "Vendor ID"
  NLS_ID_GUI_CONF(206)            "Nombre de usuario"
  NLS_ID_GUI_CONF(207)            "Clave"
  NLS_ID_GUI_CONF(208)            "Tipo de cajero"
  NLS_ID_GUI_CONF(209)            "ID"
  NLS_ID_GUI_CONF(210)            "Nombre"
  NLS_ID_GUI_CONF(211)            "ID m�quina"
  NLS_ID_GUI_CONF(212)            "Terminales configurados"
  NLS_ID_GUI_CONF(213)            "Terminales no configurados"
  NLS_ID_GUI_CONF(214)            "N�mero de serie"
  NLS_ID_GUI_CONF(215)            ""  // FREE
  NLS_ID_GUI_CONF(216)            "Par�metros de conexi�n"
  NLS_ID_GUI_CONF(217)            "Terminales configurados"
  NLS_ID_GUI_CONF(218)            "Terminales sin configurar"
  NLS_ID_GUI_CONF(219)            "El campo '%1' es obligatorio"
  NLS_ID_GUI_CONF(220)            "WIN"
  NLS_ID_GUI_CONF(221)            "3GS" 
  NLS_ID_GUI_CONF(222)            ""  // FREE
  NLS_ID_GUI_CONF(223)            "Terminales"
  NLS_ID_GUI_CONF(224)            "Dep�sito de tarjeta an�nima"
  NLS_ID_GUI_CONF(225)            "Modo de caja"
  NLS_ID_GUI_CONF(226)            "Idioma"
  NLS_ID_GUI_CONF(227)            "Impuesto Federal sobre ganancias"
  NLS_ID_GUI_CONF(228)            "Cabecera"
  NLS_ID_GUI_CONF(229)            "Pie"
  NLS_ID_GUI_CONF(230)            "Imprimir"
  NLS_ID_GUI_CONF(231)            "Por Usuario"
  NLS_ID_GUI_CONF(232)            "Por Terminal"
  NLS_ID_GUI_CONF(233)            "es"
  NLS_ID_GUI_CONF(234)            "PerTerminal"
  NLS_ID_GUI_CONF(235)            "PerUser"
  NLS_ID_GUI_CONF(236)            "Par�metros"
  NLS_ID_GUI_CONF(237)            "Ticket de caja"
  NLS_ID_GUI_CONF(238)            "Terminal"
  NLS_ID_GUI_CONF(239)            "OK"
  NLS_ID_GUI_CONF(240)            "Error"
  NLS_ID_GUI_CONF(241)            "La contrase�a caduca cada"
  NLS_ID_GUI_CONF(242)            "Pendiente"
  NLS_ID_GUI_CONF(243)            "Sin configurar"
  NLS_ID_GUI_CONF(244)            "Estado de la configuraci�n"
  NLS_ID_GUI_CONF(245)            "en-US"
  NLS_ID_GUI_CONF(246)            "Precio de tarjeta personalizada"
  NLS_ID_GUI_CONF(247)            "Sustituci�n de tarjeta personalizada"
  NLS_ID_GUI_CONF(248)            "Tarjeta an�nima"
  NLS_ID_GUI_CONF(249)            "Precio de tarjetas"
  NLS_ID_GUI_CONF(250)            "Player Tracking"
  NLS_ID_GUI_CONF(251)            "Nombre"
  NLS_ID_GUI_CONF(252)            "Redimible jugado"
  NLS_ID_GUI_CONF(253)            "Total jugado"
  NLS_ID_GUI_CONF(254)            "Redimible agotado"
  NLS_ID_GUI_CONF(255)            "Conversi�n de Cr�ditos a Puntos"
  NLS_ID_GUI_CONF(256)            "Niveles"
  NLS_ID_GUI_CONF(257)            "= 1pt."
  // Form Name
  NLS_ID_GUI_CONF(258)            "Salas y Operadores"
  NLS_ID_GUI_CONF(259)            "Estado"
  NLS_ID_GUI_CONF(260)            "Impuesto Estatal sobre ganancias"
  NLS_ID_GUI_CONF(261)            "Pago manual"
  NLS_ID_GUI_CONF(262)            "Tiempo anulaci�n"
  NLS_ID_GUI_CONF(263)            "Periodo de tiempo"
  NLS_ID_GUI_CONF(264)            "S�"
  NLS_ID_GUI_CONF(265)            "No"
  NLS_ID_GUI_CONF(266)            "Impresora de tarjetas"
  NLS_ID_GUI_CONF(267)            "Top X"
  NLS_ID_GUI_CONF(268)            "Top Y"
  NLS_ID_GUI_CONF(269)            "Bottom X"
  NLS_ID_GUI_CONF(270)            "Bottom Y"
  NLS_ID_GUI_CONF(271)            "Tama�o de letra"
  NLS_ID_GUI_CONF(272)            "CardPrint"
  NLS_ID_GUI_CONF(273)            "PrintNameArea_TopX"
  NLS_ID_GUI_CONF(274)            "PrintNameArea_TopY"
  NLS_ID_GUI_CONF(275)            "PrintNameArea_BottomX"
  NLS_ID_GUI_CONF(276)            "PrintNameArea_BottomY"
  NLS_ID_GUI_CONF(277)            "PrintNameArea_FontSize"
  NLS_ID_GUI_CONF(278)            "Impresora"
  NLS_ID_GUI_CONF(279)            "QueueName"
  NLS_ID_GUI_CONF(280)            "Texto en negrita"
  NLS_ID_GUI_CONF(281)            "Texto Normal"
  NLS_ID_GUI_CONF(282)            "PrintNameArea_Bold"
  NLS_ID_GUI_CONF(283)            "seg."
  NLS_ID_GUI_CONF(284)            "Tiempo m�nimo cierre sesi�n"
  NLS_ID_GUI_CONF(285)            "min."
  NLS_ID_GUI_CONF(286)            "d�as"
  NLS_ID_GUI_CONF(287)            "Caducidad de puntos"
  NLS_ID_GUI_CONF(288)            "Caducidad de solicitudes de regalos"
  NLS_ID_GUI_CONF(289)            "B�sico"
  NLS_ID_GUI_CONF(290)            "Medio"
  NLS_ID_GUI_CONF(291)            "Alto"
  NLS_ID_GUI_CONF(292)            "Programa de puntos"
  NLS_ID_GUI_CONF(293)            "Caducidad"
  NLS_ID_GUI_CONF(294)            "Se obtiene 1 punto por cada una de las cantidades siguientes:"
  NLS_ID_GUI_CONF(295)            "*Puntos obtenidos en la sesi�n de juego:\nBalance inicial   = %1\nRetenido          = %2\nJugado            = %3\nGanado            = %4\nBalance final     = %5"
  NLS_ID_GUI_CONF(296)            "Por lo tanto:\nRedimible agotado = %1\nRedimible jugado  = %2\nTotal jugado      = %3"
  NLS_ID_GUI_CONF(297)            "Puntos obtenidos*"
  NLS_ID_GUI_CONF(298)            "Sorteos"
  NLS_ID_GUI_CONF(299)            "Tipo de sorteo"
  NLS_ID_GUI_CONF(300)            "Desde"
  NLS_ID_GUI_CONF(301)            "Hasta"
  NLS_ID_GUI_CONF(302)            "Por cr�dito jugado"
  NLS_ID_GUI_CONF(303)            "Por cr�dito redimible agotado"
  NLS_ID_GUI_CONF(304)            "Login"
  NLS_ID_GUI_CONF(305)            "Clave"
  NLS_ID_GUI_CONF(306)            "Verificaci�n"
  NLS_ID_GUI_CONF(307)            "V�lido desde"
  NLS_ID_GUI_CONF(308)            "Caducidad de contrase�a"
  NLS_ID_GUI_CONF(309)            "Clave nunca expira"
  NLS_ID_GUI_CONF(310)            "Requiere cambio de clave en el pr�ximo login"
  NLS_ID_GUI_CONF(311)            "Habilitado"
  NLS_ID_GUI_CONF(312)            "Permisos"
  NLS_ID_GUI_CONF(313)            "Por cr�dito total agotado"
  NLS_ID_GUI_CONF(314)            "Cr�dito m�ximo permitido"
  NLS_ID_GUI_CONF(315)            "Valores por defecto para nuevos sorteos"
  NLS_ID_GUI_CONF(316)            "Numeraci�n"
  NLS_ID_GUI_CONF(317)            "Requiere cambio de clave"
  NLS_ID_GUI_CONF(318)            "Edici�n de Perfil de Usuario"
  NLS_ID_GUI_CONF(319)            "Perfil"
  NLS_ID_GUI_CONF(320)            "Formulario"
  NLS_ID_GUI_CONF(321)            "Lectura"
  NLS_ID_GUI_CONF(322)            "Escritura"
  NLS_ID_GUI_CONF(323)            "Borrado"
  NLS_ID_GUI_CONF(324)            "Ejecuci�n"
  NLS_ID_GUI_CONF(325)            "Edici�n de usuario"
  NLS_ID_GUI_CONF(326)            "Usuario"
  NLS_ID_GUI_CONF(327)            "Nombre Completo"
  NLS_ID_GUI_CONF(328)            "Deshabilitado"
  NLS_ID_GUI_CONF(329)            "Por puntos"
  NLS_ID_GUI_CONF(330)            "Por recarga"
  NLS_ID_GUI_CONF(331)            "GUI"

  // RCI 11-NOV-2010: Programa de Puntos
  NLS_ID_GUI_CONF(332)            "Premium"
  NLS_ID_GUI_CONF(333)            "Puntos entrada"
  NLS_ID_GUI_CONF(334)            "D�as Permanencia"
  NLS_ID_GUI_CONF(335)            "Puntos obtenidos en los �ltimos"
  NLS_ID_GUI_CONF(336)            "Ocultar promoci�n"
  NLS_ID_GUI_CONF(337)            "Cup�n premio"
  NLS_ID_GUI_CONF(338)            "Condiciones de recarga"
  NLS_ID_GUI_CONF(339)            "M�nimo jugado"
  NLS_ID_GUI_CONF(340)            "M�nimo agotado"

  // Form permission: Read
  NLS_ID_GUI_CONF(341)            "L"
  // Form permission: Write
  NLS_ID_GUI_CONF(342)            "E"
  // Form permission: Delete
  NLS_ID_GUI_CONF(343)            "B"
  // Form permission: Execution
  NLS_ID_GUI_CONF(344)            "X"
  // Form Name: sle 13-11-09
  NLS_ID_GUI_CONF(345)            "Cajero"
  NLS_ID_GUI_CONF(346)            "Cuentas: A�adir cr�dito promocional no redimible"
  NLS_ID_GUI_CONF(347)            "Caja: Apertura y Cierre"
  NLS_ID_GUI_CONF(348)            "Caja: Acceso"
  NLS_ID_GUI_CONF(349)            "Caja: Dep�sito"
  NLS_ID_GUI_CONF(350)            "Caja: Retiro"
  NLS_ID_GUI_CONF(351)            "Cuentas: Recarga de cr�dito"
  // Form Name
  NLS_ID_GUI_CONF(352)            "Edici�n de Operador de Salas"
  // Form Name
  NLS_ID_GUI_CONF(353)            "Edici�n de Sala"
  NLS_ID_GUI_CONF(354)            "Cuentas: Ver �ltimos Movimientos"
  NLS_ID_GUI_CONF(355)            "Cuentas: Asociar Tarjeta"
  NLS_ID_GUI_CONF(356)            "Cuentas: Editar personales"
  NLS_ID_GUI_CONF(357)            "Cuentas: Bloquear"
  NLS_ID_GUI_CONF(358)            "Banco M�vil"
  NLS_ID_GUI_CONF(359)            "Opciones: Configuraci�n de la Base de Datos"
  NLS_ID_GUI_CONF(360)            "Opciones: Cambiar Idioma"
  NLS_ID_GUI_CONF(361)            "Opciones: Calibrar pantalla"
  NLS_ID_GUI_CONF(362)            "Consultar sesiones de juego"
  NLS_ID_GUI_CONF(363)            "Imprimir Tarjeta"
  NLS_ID_GUI_CONF(364)            "Pago Manual - Pago"
  NLS_ID_GUI_CONF(365)            "Pago Manual � Anular Pago"
  NLS_ID_GUI_CONF(366)            "Pago Manual - Entrada Manual - Pago"
  NLS_ID_GUI_CONF(367)            "Pago Manual - Entrada Manual - Anulaci�n"
  NLS_ID_GUI_CONF(368)            "Porcentaje"
  NLS_ID_GUI_CONF(369)            "Concepto"
  NLS_ID_GUI_CONF(370)            "Divisi�n de las entradas (cash-in)"
  NLS_ID_GUI_CONF(371)            "Devolver como premio"
  NLS_ID_GUI_CONF(372)            "Regalos - Petici�n"
  NLS_ID_GUI_CONF(373)            "Regalos - Entrega"
  NLS_ID_GUI_CONF(374)            "M�nima recarga"
  NLS_ID_GUI_CONF(375)            "Regalos - Imprimir Lista"
  NLS_ID_GUI_CONF(376)            "Tickets Sorteo - Imprimir"
  NLS_ID_GUI_CONF(377)            "Cuentas: B�squeda por Nombre"
  NLS_ID_GUI_CONF(378)            "Cuentas: A�adir Cr�dito: Superar m�xima recarga"

  NLS_ID_GUI_CONF(379)            "Tipo"
  NLS_ID_GUI_CONF(380)            "Sala"
  NLS_ID_GUI_CONF(381)            "Operador"
  NLS_ID_GUI_CONF(382)            "Salas"
  NLS_ID_GUI_CONF(383)            "Perfiles y usuarios"

  // RCI 16-DEC-2010: Cadillac Jack
  NLS_ID_GUI_CONF(384)            "CADILLAC JACK"
  NLS_ID_GUI_CONF(385)            "Configuraci�n para Servicio WCP 1"
  NLS_ID_GUI_CONF(386)            "Configuraci�n para Servicio WCP 2"
  NLS_ID_GUI_CONF(387)            "IP Local"
  NLS_ID_GUI_CONF(388)            "IP Remota (Servidor Cadillac Jack)"
  NLS_ID_GUI_CONF(389)            "Vendor ID"
  NLS_ID_GUI_CONF(390)            "CJ IP Local 1"                          // Auditor label
  NLS_ID_GUI_CONF(391)            "CJ IP Remota (Servidor CJ 1)"           // Auditor label
  NLS_ID_GUI_CONF(392)            "CJ Vendor ID 1"                         // Auditor label
  NLS_ID_GUI_CONF(393)            "CJ IP Local 2"                          // Auditor label
  NLS_ID_GUI_CONF(394)            "CJ IP Remota (Servidor CJ 2)"           // Auditor label
  NLS_ID_GUI_CONF(395)            "CJ Vendor ID 2"                         // Auditor label

  // RCI 03-JAN-2011: Mailing
  NLS_ID_GUI_CONF(396)            "Mailing Habilitado"
  NLS_ID_GUI_CONF(397)            "SMTP Habilitado"
  NLS_ID_GUI_CONF(398)            "Configuraci�n de SMTP"
  NLS_ID_GUI_CONF(399)            "Credenciales"
  NLS_ID_GUI_CONF(400)            "Servidor"
  NLS_ID_GUI_CONF(401)            "Puerto"
  NLS_ID_GUI_CONF(402)            "Seguro (SSL)"
  NLS_ID_GUI_CONF(403)            "Usuario"
  NLS_ID_GUI_CONF(404)            "Contrase�a"
  NLS_ID_GUI_CONF(405)            "Dominio"
  NLS_ID_GUI_CONF(406)            "Buz�n"
  NLS_ID_GUI_CONF(407)            "SMTP"                                   // Auditor label
  NLS_ID_GUI_CONF(408)            "Mailing"
  NLS_ID_GUI_CONF(409)            "Enviar Correo de Prueba"
  NLS_ID_GUI_CONF(410)            "Para"
  NLS_ID_GUI_CONF(411)            "Enviar"
  NLS_ID_GUI_CONF(412)            "Servidores Autorizados"

  NLS_ID_GUI_CONF(413)            "iStats"
  NLS_ID_GUI_CONF(414)            "iStats Aplicaci�n iPad"

  NLS_ID_GUI_CONF(415)            "Cuentas: A�adir Cr�dito: Superar m�xima recarga diaria por cliente"
  NLS_ID_GUI_CONF(416)            "Impresi�n Autom�tica de Tickets de Banco M�vil"
  NLS_ID_GUI_CONF(417)            "Cuentas: Cerrar sesi�n: Incrementando el balance"

  NLS_ID_GUI_CONF(418)            "Programa"
  NLS_ID_GUI_CONF(419)            "Payout te�rico (%)"
  NLS_ID_GUI_CONF(420)            "Denominaci�n"
  NLS_ID_GUI_CONF(421)            "Multidenominaci�n"

  // JMM 02-JAN-2012: Raffles
  NLS_ID_GUI_CONF(422)            "Por recarga m�nima y agotado"

  NLS_ID_GUI_CONF(423)            "La longitud del campo %1 no puede superar los %2 caracteres"

  // RCI 26-JAN-2012: Permission Functionality
  NLS_ID_GUI_CONF(424)            "Reimpresi�n de Tickets de Caja - Acceso"
  NLS_ID_GUI_CONF(425)            "Reimpresi�n de Tickets de Caja - Imprimir"
  NLS_ID_GUI_CONF(426)            "Reimpresi�n de Tickets de Banco M�vil - Imprimir Todos"
  
  // SSC 14-FEB-2012: Terminals Edition
  NLS_ID_GUI_CONF(427)            "Tipo de juego"
  NLS_ID_GUI_CONF(428)            "Fecha de entrada"
  NLS_ID_GUI_CONF(429)            "Propietario"
  NLS_ID_GUI_CONF(430)            "Zona"
  NLS_ID_GUI_CONF(431)            "Isla"
  NLS_ID_GUI_CONF(432)            "ID en planta"
  NLS_ID_GUI_CONF(433)            "Fechas" //LEM 15-JUL-2013: Don't change, it's used in Cashier Sessions
  NLS_ID_GUI_CONF(434)            "Tipo / Versi�n"
  NLS_ID_GUI_CONF(435)            "�rea"
  NLS_ID_GUI_CONF(436)            "Fumadores"
  NLS_ID_GUI_CONF(437)            "No Fumadores"

  // JML 15-JUN-2012:  Datos de Proveedor
  NLS_ID_GUI_CONF(438)            "Datos de Proveedor"
  NLS_ID_GUI_CONF(439)            "Validador de billetes"

  // RRB 15-NOV-2012: Puntos Permanecer
  NLS_ID_GUI_CONF(440)            "M�nimo puntos *"
  NLS_ID_GUI_CONF(441)            "D�as de permanencia"
  NLS_ID_GUI_CONF(442)            "Permanencia"
  NLS_ID_GUI_CONF(443)            "M�nimo D�as de Permanencia"

  // RRB 21-JAN-2013: Precio de Tarjetas
  NLS_ID_GUI_CONF(444)            "Precio de Tarjeta An�nima"
  NLS_ID_GUI_CONF(445)            "Dep�sito No Reembolsable"
  NLS_ID_GUI_CONF(446)            "Dep�sito Reembolsable"
  NLS_ID_GUI_CONF(447)            "Venta"
  NLS_ID_GUI_CONF(448)            "Puntos obtenidos en los �ltimos d�as (Periodo de estudio)"

  NLS_ID_GUI_CONF(450)            "Texto al reimprimir"

  // IDs from 451 to 499 are reserved for Menu Items

  NLS_ID_GUI_CONF(451)            "Operaciones generales"
  NLS_ID_GUI_CONF(452)            "Configuraci�n"
  NLS_ID_GUI_CONF(453)            "Salas y Operadores"

  NLS_ID_GUI_CONF(454)            "Retirada de Terminales"
  NLS_ID_GUI_CONF(455)            "Fecha de Retirada"
  NLS_ID_GUI_CONF(456)            "Orden de Retirada"

  NLS_ID_GUI_CONF(467)            "Perfiles y usuarios"
  NLS_ID_GUI_CONF(468)            "Relaci�n terminal-juego"
  NLS_ID_GUI_CONF(469)            "Proveedor"
  NLS_ID_GUI_CONF(470)            "Terminal"
  NLS_ID_GUI_CONF(471)            "Juego Reportado"
  NLS_ID_GUI_CONF(472)            "Juego a Mostrar"
  NLS_ID_GUI_CONF(473)            "Mostrar solo juegos reportados sin renombrar"
  NLS_ID_GUI_CONF(474)            "Computadora"
  NLS_ID_GUI_CONF(475)            "Terminal"
  NLS_ID_GUI_CONF(476)            "Todas"
  NLS_ID_GUI_CONF(477)            ""	//"El nombre de juego %1 introducido para %2 no existe. �Desea crear un nuevo juego con este nombre?"
  NLS_ID_GUI_CONF(478)            "Cuentas: Cerrar sesi�n"
  NLS_ID_GUI_CONF(479)            "Cuentas: Nivel"
  NLS_ID_GUI_CONF(480)            "Promociones: Permiso Especial A"
  NLS_ID_GUI_CONF(481)            "Promociones: Permiso Especial B"
  NLS_ID_GUI_CONF(482)            "Cuadratura de estad�sticas"
  NLS_ID_GUI_CONF(483)            "Permitir crear cuentas an�nimas"
  NLS_ID_GUI_CONF(484)            "Generar Documentos de Constancia de Premios"
  NLS_ID_GUI_CONF(485)            "Cuentas: Reciclar Tarjeta"

  NLS_ID_GUI_CONF(486)            "Actividad de proveedores / caja"

  NLS_ID_GUI_CONF(487)            "Zona Horaria"
  NLS_ID_GUI_CONF(488)            "GMT"
  NLS_ID_GUI_CONF(489)            "Horario de Verano Habilitado"
  NLS_ID_GUI_CONF(490)            "Comienzo"
  NLS_ID_GUI_CONF(491)            "Final"
  NLS_ID_GUI_CONF(492)            "Hora"
  NLS_ID_GUI_CONF(493)            "Mes"
  NLS_ID_GUI_CONF(494)            "D�a de Semana"
  NLS_ID_GUI_CONF(495)            "D�a"
  NLS_ID_GUI_CONF(496)            "Solo activos"

  NLS_ID_GUI_CONF(497)            "Todos"
  NLS_ID_GUI_CONF(498)            "Todos menos"
  NLS_ID_GUI_CONF(499)            "Seleccionados"
	

  /////////////////////////////////////////////////////////////

	NLS_ID_GUI_CONF(60)             "Se van a modificar las cuentas seleccionadas.\n\n�Desea realizar la operaci�n?"
  NLS_ID_GUI_CONF(61)             "Grupo"
  NLS_ID_GUI_CONF(62)             "Descripci�n"
  NLS_ID_GUI_CONF(63)             "Valor"
  NLS_ID_GUI_CONF(64)             "Cuentas"
  NLS_ID_GUI_CONF(65)             "Todas las cuentas"
  NLS_ID_GUI_CONF(66)             "Solo cuentas an�nimas"
  NLS_ID_GUI_CONF(67)             "Solo cuentas personalizadas"
  NLS_ID_GUI_CONF(68)             "Aplica a:"

  NLS_ID_GUI_CONF(69)             "Modificar"
  NLS_ID_GUI_CONF(70)             "Modificar Cuentas Clientes"
  NLS_ID_GUI_CONF(71)             "Puntos"
  NLS_ID_GUI_CONF(72)             "Cambiar nivel"
  NLS_ID_GUI_CONF(73)             "Tipo de modificaci�n"
  NLS_ID_GUI_CONF(74)             "A�adir puntos"
  NLS_ID_GUI_CONF(75)             "Fijar puntos"
  NLS_ID_GUI_CONF(76)             "Debe indicar el n�mero de puntos."
  NLS_ID_GUI_CONF(77)             "Modificar puntos"
  NLS_ID_GUI_CONF(78)             "A�adir puntos"
  NLS_ID_GUI_CONF(79)             "Cambiar nivel"
	NLS_ID_GUI_CONF(80)             "Se ha producido un error al leer los niveles de cuentas en la base de datos."
	NLS_ID_GUI_CONF(81)             "Debe seleccionar un nivel."
	NLS_ID_GUI_CONF(82)             "Se ha producido un error al intentar actualizar en la base de datos."
  NLS_ID_GUI_CONF(83)             "antes"
	NLS_ID_GUI_CONF(84)             "El n�mero de puntos debe ser mayor que 0."
	NLS_ID_GUI_CONF(123)            "A las cuentas que ya tienen el nuevo nivel, se les extiende la permanencia."
	NLS_ID_GUI_CONF(132)            "Cuentas seleccionadas: "
	NLS_ID_GUI_CONF(133)            "Las cuentas seleccionadas se han modificado correctamente."
	NLS_ID_GUI_CONF(500)            "Las cuentas an�nimas (%1) no ser�n modificadas."

	NLS_ID_GUI_CONF(85)             "Evento SAS no definido, C�digo de Excepci�n SAS: %1"
	NLS_ID_GUI_CONF(86)             "Evento SAS no definido, Data: %1"
	NLS_ID_GUI_CONF(87)             "Evento Desconocido, Data: %1"

  NLS_ID_GUI_CONF(88)             "Dado de baja"
  NLS_ID_GUI_CONF(89)             "Desbloquear"
  NLS_ID_GUI_CONF(90)             "Mostrar todos"
  NLS_ID_GUI_CONF(91)             "Deshabilitado"
  NLS_ID_GUI_CONF(92)             "Bloqueado"
  NLS_ID_GUI_CONF(93)             "No bloqueado"
	NLS_ID_GUI_CONF(94)             "Se va a modificar la cuenta seleccionada.\n\n�Desea realizar la operaci�n?"
	NLS_ID_GUI_CONF(95)             "La cuenta seleccionada se ha modificado correctamente."

	
		// SAS Events
  NLS_ID_GUI_CONF(147)             "Se abri� la puerta de la ranura"
	NLS_ID_GUI_CONF(148)             "Se cerr� la puerta de la ranura"
	NLS_ID_GUI_CONF(149)             "Se abri� la puerta trasera"
	NLS_ID_GUI_CONF(150)             "Se cerr� la puerta trasera"
	NLS_ID_GUI_CONF(151)             "Se abri� la caja de tarjetas"
	NLS_ID_GUI_CONF(152)             "Se cerr� la caja de tarjetas"
	NLS_ID_GUI_CONF(153)             "Terminal de juego conectado a la corriente"
	NLS_ID_GUI_CONF(154)             "Terminal de juego desconectado de la corriente"
	NLS_ID_GUI_CONF(155)             "Se abri� la puerta de la caja"
	NLS_ID_GUI_CONF(156)             "Se cerr� la puerta de la caja"
	NLS_ID_GUI_CONF(157)             "Caja extra�da"
	NLS_ID_GUI_CONF(158)             "Caja instalada"
	NLS_ID_GUI_CONF(159)             "Se abri� la puerta delantera"
	NLS_ID_GUI_CONF(160)             "Se cerr� la puerta delantera"
	NLS_ID_GUI_CONF(161)             "Fallo general"
	NLS_ID_GUI_CONF(162)             "Error en RAM CMOS (datos recuperados de la EEPROM)"
	NLS_ID_GUI_CONF(163)             "Error en RAM CMOS (datos no recuperados de la EEPROM)"
	NLS_ID_GUI_CONF(164)             "Error en RAM CMOS (error de dispositivo)"
	NLS_ID_GUI_CONF(165)             "Error en EEPROM (error de datos)"
	NLS_ID_GUI_CONF(166)             "Error en EEPROM (error de dispositivo)"
	NLS_ID_GUI_CONF(167)             "Error en EEPROM (checksum distinto - cambio de versi�n)"
	NLS_ID_GUI_CONF(168)             "Error en EEPROM (error de comparaci�n de checksum)"
	NLS_ID_GUI_CONF(169)             "Error en EEPROM particionada (checksum - cambio de versi�n)"
	NLS_ID_GUI_CONF(170)             "Error en EEPROM particionada (error de comparaci�n de checksum)"
	NLS_ID_GUI_CONF(171)             "Error de memoria eliminado (el operador us� switch de auto-test)"
	NLS_ID_GUI_CONF(172)             "Bater�a de reserva con poca carga"
	NLS_ID_GUI_CONF(173)             "El operador modific� opciones"
	NLS_ID_GUI_CONF(174)             "Pago manual pendiente"
	NLS_ID_GUI_CONF(175)             "Pago manual completado"
  NLS_ID_GUI_CONF(176)             "Progresivo ganado (cashout / cr�dito pagado)"
	NLS_ID_GUI_CONF(177)             "Pago manual cancelado por el jugador"
	NLS_ID_GUI_CONF(178)             "Nivel de progresivo SAS alcanzado"
	NLS_ID_GUI_CONF(179)             "Desbordamiento en el buffer de excepciones"
	NLS_ID_GUI_CONF(180)             "Contadores de terminal de juego puestos a cero"
	NLS_ID_GUI_CONF(181)             "Entrada a pantalla de contadores o men� de asistente"
	NLS_ID_GUI_CONF(182)             "Salida de pantalla de contadores o men� de asistente"
	NLS_ID_GUI_CONF(183)             "Entrada a auto-test o men� de operador"
	NLS_ID_GUI_CONF(184)             "Salida de auto-test o men� de operador"
	NLS_ID_GUI_CONF(185)             "Terminal de juego deshabilitado (por asistente)"
	NLS_ID_GUI_CONF(186)             "Acceso a hist�rico de jugadas"
	NLS_ID_GUI_CONF(187)             "Acceso a card cage con terminal apagado"
	NLS_ID_GUI_CONF(188)             "Acceso a slot door con terminal apagado"
	NLS_ID_GUI_CONF(189)             "Acceso a cashbox con terminal apagado"
	NLS_ID_GUI_CONF(190)             "Acceso a drop door con terminal apagado"
	NLS_ID_GUI_CONF(191)             "SAS desconectado"
	NLS_ID_GUI_CONF(192)             "SAS conectado"
	NLS_ID_GUI_CONF(120)             "Petici�n de asistencia activada"
	NLS_ID_GUI_CONF(122)             "Petici�n de asistencia desactivada"

	NLS_ID_GUI_CONF(137)             "Contador de juego eliminado"
	NLS_ID_GUI_CONF(138)             "Gran incremento de contador de juego"
	NLS_ID_GUI_CONF(139)             "Gran incremento de contador HPC"
	NLS_ID_GUI_CONF(140)             "WCP conectado"
	NLS_ID_GUI_CONF(141)             "WCP desconectado"
	NLS_ID_GUI_CONF(142)             "WC2 conectado"
	NLS_ID_GUI_CONF(143)             "WC2 desconectado"
	NLS_ID_GUI_CONF(144)             "Servicio iniciado"
	NLS_ID_GUI_CONF(145)             "Servicio parado"
	NLS_ID_GUI_CONF(108)             "Servicio reiniciado"
	NLS_ID_GUI_CONF(109)             "Licencia caducada"
	NLS_ID_GUI_CONF(117)             "La licencia caducar� pronto"
	NLS_ID_GUI_CONF(118)             "Nueva versi�n descargada"
	NLS_ID_GUI_CONF(119)             "Nueva versi�n disponible"


  
  NLS_ID_GUI_CONF(20)              "�nico Rubro"
  NLS_ID_GUI_CONF(21)              "Textos de los tickets de caja"
  NLS_ID_GUI_CONF(22)              "No se puede borrar la %1 porque ya tiene %2 asignadas."
  NLS_ID_GUI_CONF(23)              "No se puede borrar la %1 porque ya tiene %2 asignados."


END
