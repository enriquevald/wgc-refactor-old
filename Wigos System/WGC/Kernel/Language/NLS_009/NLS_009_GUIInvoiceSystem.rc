/// ------------------------------------------------------------------------------------
// Copyright � 2004 Win Systems Ltd.
// ------------------------------------------------------------------------------------
//
//   MODULE NAME: NLS_009_GUIInvoiceSystem.rc
//
//   DESCRIPTION: NLS resource for LKC GUIInvoiceSystem.
//
//        AUTHOR: Gabi Zutel
//
// CREATION DATE: 09-MAR-2004
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -------------------------------------------------------------------
// 09-MAR-2004 GZ     Initial release
// --------------------------------------------------------------------------------------

// WARNING !!!
//
// Do NOT modify this file with Visual Studio Resource Editor.
// This file MUST be edited using a TEXT editor.
//
// WARNING !!!

STRINGTABLE DISCARDABLE
BEGIN

  // IDs from 1 to 25 are reserved for buttons
  // IDs from 26 to 100 are reserved for application specific requirements
  // IDs from 101 to 200 are reserved for messages
  // IDs from 201 to 450 are reserved for Labels
  // IDs from 451 to 499 are reserved for Menu Items

  // Buttons: 1-25
  NLS_ID_GUI_INVOICE_SYSTEM(1)      "Exit"
  NLS_ID_GUI_INVOICE_SYSTEM(2)      "Movements"
  NLS_ID_GUI_INVOICE_SYSTEM(3)      "Exit"
  NLS_ID_GUI_INVOICE_SYSTEM(4)      "Cancel"
  NLS_ID_GUI_INVOICE_SYSTEM(5)      "Detail"
  NLS_ID_GUI_INVOICE_SYSTEM(6)      "Mobile Bank"
  NLS_ID_GUI_INVOICE_SYSTEM(7)      "Report"
  NLS_ID_GUI_INVOICE_SYSTEM(8)      "Close Cash"
  NLS_ID_GUI_INVOICE_SYSTEM(9)      "Collect Bills"

  // IDs from 26 to 100 are reserved for application specific requirements
  // Reserved 26-40: Cash Desk movement types
  NLS_ID_GUI_INVOICE_SYSTEM(26)   	"Cash Opening"
  NLS_ID_GUI_INVOICE_SYSTEM(27)   	"Cash Closing"
  NLS_ID_GUI_INVOICE_SYSTEM(28)   	"Cash Deposit"
  NLS_ID_GUI_INVOICE_SYSTEM(29)   	"Cash Withdrawal"
  NLS_ID_GUI_INVOICE_SYSTEM(30)   	"Recharge"
  NLS_ID_GUI_INVOICE_SYSTEM(31)   	"Refund"
  NLS_ID_GUI_INVOICE_SYSTEM(32)   	"Taxes"
  NLS_ID_GUI_INVOICE_SYSTEM(33)   	"Gross Prize" // ATB 20-MAR-2017: 25737: Third Tax - Report Columns
  // SSC 07-FEB-2012
  NLS_ID_GUI_INVOICE_SYSTEM(34)   	"Pending for collection" // No collected
  // SSC 09-FEB-2012
  NLS_ID_GUI_INVOICE_SYSTEM(35)   	"Bill History" // Menu Item
  NLS_ID_GUI_INVOICE_SYSTEM(36)   	"Transfer"
  NLS_ID_GUI_INVOICE_SYSTEM(37)   	"Bills"

  NLS_ID_GUI_INVOICE_SYSTEM(38)   	"Non-Redeemable 2 (Cover Coupon)"
  NLS_ID_GUI_INVOICE_SYSTEM(39)     "Pending validators"
  NLS_ID_GUI_INVOICE_SYSTEM(40)     "Cash Closing"

  // Reserved 41-80: Card movement types
  NLS_ID_GUI_INVOICE_SYSTEM(41)   	"Play"
  NLS_ID_GUI_INVOICE_SYSTEM(42)   	"Recharge"
  NLS_ID_GUI_INVOICE_SYSTEM(43)   	"Winnings"
  NLS_ID_GUI_INVOICE_SYSTEM(44)   	"Refund"
  NLS_ID_GUI_INVOICE_SYSTEM(45)   	"Taxes"
  NLS_ID_GUI_INVOICE_SYSTEM(46)   	"Session Started"
  NLS_ID_GUI_INVOICE_SYSTEM(47)   	"Session Finished"
  NLS_ID_GUI_INVOICE_SYSTEM(48)   	"Activation"
  NLS_ID_GUI_INVOICE_SYSTEM(49)   	"Deactivation"
  NLS_ID_GUI_INVOICE_SYSTEM(50)   	"Non-Redeemable Promo."
  NLS_ID_GUI_INVOICE_SYSTEM(51)   	"Card Deposit/Price Payment"
  NLS_ID_GUI_INVOICE_SYSTEM(52)   	"Card Deposit/Price Refund"
  NLS_ID_GUI_INVOICE_SYSTEM(53)   	"Non-Redeemable Promo. Cancel"
  NLS_ID_GUI_INVOICE_SYSTEM(54)   	"MB Sales Limit"
  NLS_ID_GUI_INVOICE_SYSTEM(55)   	"MB Recharge"
  NLS_ID_GUI_INVOICE_SYSTEM(56)   	"== FED: TO REMOVE ==" //Federal Taxes
  NLS_ID_GUI_INVOICE_SYSTEM(57)   	"== EST: TO REMOVE ==" //State Taxes
  NLS_ID_GUI_INVOICE_SYSTEM(58)   	"Redemption: Playable"
  NLS_ID_GUI_INVOICE_SYSTEM(59)   	"Redemption: Restaurant"
  NLS_ID_GUI_INVOICE_SYSTEM(60)   	"Transfer: Restaurant"
  NLS_ID_GUI_INVOICE_SYSTEM(61)   	"Transfer: Playable"
  NLS_ID_GUI_INVOICE_SYSTEM(62)   	"Expired credit transfer"
  NLS_ID_GUI_INVOICE_SYSTEM(63)   	"Expired Points"
  NLS_ID_GUI_INVOICE_SYSTEM(64)   	"Promo. Credit Added"
  NLS_ID_GUI_INVOICE_SYSTEM(65)   	"Non-Redeemable to Redeemable (Lock)"
  NLS_ID_GUI_INVOICE_SYSTEM(66)   	"Expired Promo."
  NLS_ID_GUI_INVOICE_SYSTEM(67)   	"Canceled Promo."
  NLS_ID_GUI_INVOICE_SYSTEM(68)   	"Promo. Session Started"
  NLS_ID_GUI_INVOICE_SYSTEM(69)   	"Promo. Session Finished"
  NLS_ID_GUI_INVOICE_SYSTEM(70)   	"Card Replacement"
  NLS_ID_GUI_INVOICE_SYSTEM(71)   	"Raffle Ticket"
  NLS_ID_GUI_INVOICE_SYSTEM(72)   	"Payment"
  NLS_ID_GUI_INVOICE_SYSTEM(73)   	"Cancelation"
  NLS_ID_GUI_INVOICE_SYSTEM(74)   	"Session Terminated Manually"
  NLS_ID_GUI_INVOICE_SYSTEM(75)   	"Promo. Session Terminated Manually"
  NLS_ID_GUI_INVOICE_SYSTEM(76)   	"Awarded Points"
  NLS_ID_GUI_INVOICE_SYSTEM(77)   	"Points: Gift Request"
  NLS_ID_GUI_INVOICE_SYSTEM(78)   	"Points: Non-Redeemable Credit"
  NLS_ID_GUI_INVOICE_SYSTEM(79)   	"Points: Gift Delivery"
  NLS_ID_GUI_INVOICE_SYSTEM(80)   	"Cancelation"
  NLS_ID_GUI_INVOICE_SYSTEM(81)   	"Expired Points"
  NLS_ID_GUI_INVOICE_SYSTEM(82)   	"Mobile Bank Deposit"
  NLS_ID_GUI_INVOICE_SYSTEM(83)   	"Points: Raffle Ticket"
  NLS_ID_GUI_INVOICE_SYSTEM(84)   	"Points: Services"
  NLS_ID_GUI_INVOICE_SYSTEM(85)   	"Level Change: %1 (was %2)"
  NLS_ID_GUI_INVOICE_SYSTEM(86)   	"Prize Promotion"
  NLS_ID_GUI_INVOICE_SYSTEM(87)   	"Redeemable Spent (Promotion)"
  NLS_ID_GUI_INVOICE_SYSTEM(88)   	"Points: Redeemable Credit"
  NLS_ID_GUI_INVOICE_SYSTEM(89)     "Winnings Expired"
  NLS_ID_GUI_INVOICE_SYSTEM(90)     "Account Created"
  NLS_ID_GUI_INVOICE_SYSTEM(91)     "Account Personalized"
  NLS_ID_GUI_INVOICE_SYSTEM(92)     "Add/Set points"
  NLS_ID_GUI_INVOICE_SYSTEM(93)     "PIN Change"
  NLS_ID_GUI_INVOICE_SYSTEM(94)     "PIN Generation"
  NLS_ID_GUI_INVOICE_SYSTEM(95)     "Non-Redeemable 2 Expired"
  NLS_ID_GUI_INVOICE_SYSTEM(96)   	"Session Canceled"
  NLS_ID_GUI_INVOICE_SYSTEM(97)   	"Source card"
  NLS_ID_GUI_INVOICE_SYSTEM(98)   	"Transferred to"
  NLS_ID_GUI_INVOICE_SYSTEM(99)     "Collected validators"
  NLS_ID_GUI_INVOICE_SYSTEM(100)    "Redeemable Promo."
  // Continue in PlayerTracking: from 1120 to 1150 reserved to movement types

  // IDs from 101 to 149 are reserved for messages
  NLS_ID_GUI_INVOICE_SYSTEM(101)    "Error in dates interval: Date range error: initial date must be before end date."
  NLS_ID_GUI_INVOICE_SYSTEM(102)    "You must select at least one movement type."
  NLS_ID_GUI_INVOICE_SYSTEM(103)    "You must select at least one option from 'Group' and 'Totalizer'."
  NLS_ID_GUI_INVOICE_SYSTEM(104)    "You must select at least one option from 'Terminals', 'Subtotal per provider' and '%1 subtotal' from 'Group'."
  NLS_ID_GUI_INVOICE_SYSTEM(105)    "You must select at least one option from 'Totalizer'."
  NLS_ID_GUI_INVOICE_SYSTEM(106)    "An error occurred while retrieving account %1 data.\n\nMovements from these accounts will not be shown."
  NLS_ID_GUI_INVOICE_SYSTEM(107)    "You must enter a minimum winnings value."
  NLS_ID_GUI_INVOICE_SYSTEM(108)    "No associated documents were found."
  NLS_ID_GUI_INVOICE_SYSTEM(109)    "The selected row has no associated tax statement."
  NLS_ID_GUI_INVOICE_SYSTEM(110)    "Error closing the cash session."
  NLS_ID_GUI_INVOICE_SYSTEM(111)    "Are you sure you want to close the cash session?"
  NLS_ID_GUI_INVOICE_SYSTEM(112)    "Cash Net is %1.\n\nIf you close the session, this amount will be lost.\n\nAre you sure you want to close the cash session?"
  NLS_ID_GUI_INVOICE_SYSTEM(113)    "Cash Net is %1 \nand Mobile Banks pending amount is %2.\n\nIf you close the session, these amounts will be lost.\n\nAre you sure you want to close the cash session?"
  NLS_ID_GUI_INVOICE_SYSTEM(114)    "Cash session was already closed."
	NLS_ID_GUI_INVOICE_SYSTEM(115)    "Account"
  NLS_ID_GUI_INVOICE_SYSTEM(116)    "Raffle"

  // IDs from 150 to 450 are reserved for Labels
  NLS_ID_GUI_INVOICE_SYSTEM(150)   	"Company"
  NLS_ID_GUI_INVOICE_SYSTEM(151)   	"Both"
  NLS_ID_GUI_INVOICE_SYSTEM(152)   	"Status (S)"
  NLS_ID_GUI_INVOICE_SYSTEM(153)   	"Open (O)"
  NLS_ID_GUI_INVOICE_SYSTEM(154)   	"Closed (C)"
  NLS_ID_GUI_INVOICE_SYSTEM(155)   	"S"
  NLS_ID_GUI_INVOICE_SYSTEM(156)   	"O"
  NLS_ID_GUI_INVOICE_SYSTEM(157)   	"C"

  NLS_ID_GUI_INVOICE_SYSTEM(158)    "Winnings"

  NLS_ID_GUI_INVOICE_SYSTEM(159)   	"Payment"
  NLS_ID_GUI_INVOICE_SYSTEM(160)   	"%"

  NLS_ID_GUI_INVOICE_SYSTEM(161)    "Non-Redeemable Promo. Cancelation"
  NLS_ID_GUI_INVOICE_SYSTEM(162)    "Redeemable Promo. Cancelation"

  NLS_ID_GUI_INVOICE_SYSTEM(163)    "Expiration"
  NLS_ID_GUI_INVOICE_SYSTEM(164)    "Expired Redeemable - Refund"
  NLS_ID_GUI_INVOICE_SYSTEM(165)    "Expired Redeemable - Winnings"
  NLS_ID_GUI_INVOICE_SYSTEM(166)    "Expired Non-Redeemable"

  NLS_ID_GUI_INVOICE_SYSTEM(167)    "Voucher"
  NLS_ID_GUI_INVOICE_SYSTEM(168)    "Canceled Folio"

  NLS_ID_GUI_INVOICE_SYSTEM(169)    "Backordered"

  NLS_ID_GUI_INVOICE_SYSTEM(201)    "Date"
  NLS_ID_GUI_INVOICE_SYSTEM(202)    "From"
  NLS_ID_GUI_INVOICE_SYSTEM(203)    "To"
  // Form name
  NLS_ID_GUI_INVOICE_SYSTEM(204)    "Winnings taxes daily summary"
  NLS_ID_GUI_INVOICE_SYSTEM(205)   	"TOTAL: "
  NLS_ID_GUI_INVOICE_SYSTEM(206)   	"Taxable Amount"
  NLS_ID_GUI_INVOICE_SYSTEM(207)   	"Taxes"
  NLS_ID_GUI_INVOICE_SYSTEM(208)   	"Session"
  NLS_ID_GUI_INVOICE_SYSTEM(209)   	"Terminal"
  NLS_ID_GUI_INVOICE_SYSTEM(210)   	"Cashier"
  // Movement type
  NLS_ID_GUI_INVOICE_SYSTEM(211)   	"Type"
  NLS_ID_GUI_INVOICE_SYSTEM(212)   	"Card"
  NLS_ID_GUI_INVOICE_SYSTEM(213)   	"Initial"
  NLS_ID_GUI_INVOICE_SYSTEM(214)   	"Subtracted"
  NLS_ID_GUI_INVOICE_SYSTEM(215)   	"Added"
  NLS_ID_GUI_INVOICE_SYSTEM(216)   	"Final"
  // Form name
  NLS_ID_GUI_INVOICE_SYSTEM(217)   	"Cash movements"
  // Control One / All (male)
  NLS_ID_GUI_INVOICE_SYSTEM(218)    "One"
  NLS_ID_GUI_INVOICE_SYSTEM(219)    "All"
  NLS_ID_GUI_INVOICE_SYSTEM(220)    "User"
  NLS_ID_GUI_INVOICE_SYSTEM(221)    "Session"
  NLS_ID_GUI_INVOICE_SYSTEM(222)    "ID"
  // Control Several / All (male)
  NLS_ID_GUI_INVOICE_SYSTEM(223)    "Several"
  NLS_ID_GUI_INVOICE_SYSTEM(224)    "All"
  // Movement type classes: 225-226
  NLS_ID_GUI_INVOICE_SYSTEM(225)    "Cash Desk"
  NLS_ID_GUI_INVOICE_SYSTEM(226)    "Card"
  NLS_ID_GUI_INVOICE_SYSTEM(227)   	"Cash sessions" // Form name
  NLS_ID_GUI_INVOICE_SYSTEM(228)   	"Account movements"  // Form name
  NLS_ID_GUI_INVOICE_SYSTEM(229)   	"Account summary"    // Form Name
  NLS_ID_GUI_INVOICE_SYSTEM(230)   	"Account"
  NLS_ID_GUI_INVOICE_SYSTEM(231)   	"Number"
  NLS_ID_GUI_INVOICE_SYSTEM(232)   	"Terminal"  // Group Box
  NLS_ID_GUI_INVOICE_SYSTEM(233)   	"Terminal"
  NLS_ID_GUI_INVOICE_SYSTEM(234)   	"Cash Desk"
  NLS_ID_GUI_INVOICE_SYSTEM(235)   	"Holder"
  NLS_ID_GUI_INVOICE_SYSTEM(236)   	"Movement"
  NLS_ID_GUI_INVOICE_SYSTEM(237)   	"Status"
  NLS_ID_GUI_INVOICE_SYSTEM(238)   	"Locked"
  NLS_ID_GUI_INVOICE_SYSTEM(239)   	"Not Locked"
  NLS_ID_GUI_INVOICE_SYSTEM(240)   	"Balance"
  NLS_ID_GUI_INVOICE_SYSTEM(241)   	"Non-redeemable"
  NLS_ID_GUI_INVOICE_SYSTEM(242)   	"Payable"
  NLS_ID_GUI_INVOICE_SYSTEM(243)   	"Total *"
  NLS_ID_GUI_INVOICE_SYSTEM(244)   	"Opening"
  NLS_ID_GUI_INVOICE_SYSTEM(245)   	"Open"
  NLS_ID_GUI_INVOICE_SYSTEM(246)   	"Closed"
  NLS_ID_GUI_INVOICE_SYSTEM(247)   	"Profit"
  NLS_ID_GUI_INVOICE_SYSTEM(248)   	"Name"
  // Form Name
  NLS_ID_GUI_INVOICE_SYSTEM(249)   	"Statistics grouped by date, provider and terminal"
  NLS_ID_GUI_INVOICE_SYSTEM(250)   	"Reported"
  NLS_ID_GUI_INVOICE_SYSTEM(251)    "Machine"
  NLS_ID_GUI_INVOICE_SYSTEM(252)    "Server"
  NLS_ID_GUI_INVOICE_SYSTEM(253)    "Terminal"
  NLS_ID_GUI_INVOICE_SYSTEM(254)    "All"
  NLS_ID_GUI_INVOICE_SYSTEM(255)    "Cash"
  NLS_ID_GUI_INVOICE_SYSTEM(256)    "Played"
  NLS_ID_GUI_INVOICE_SYSTEM(257)    "Won"
  NLS_ID_GUI_INVOICE_SYSTEM(258)    "Payout %"
  NLS_ID_GUI_INVOICE_SYSTEM(259)    "Netwin"
  NLS_ID_GUI_INVOICE_SYSTEM(260)    "Netwin %"
  NLS_ID_GUI_INVOICE_SYSTEM(261)    "Plays"
  NLS_ID_GUI_INVOICE_SYSTEM(262)    "Wins"
  NLS_ID_GUI_INVOICE_SYSTEM(263)    "Wins %"
  NLS_ID_GUI_INVOICE_SYSTEM(264)    "Whole week included"
  NLS_ID_GUI_INVOICE_SYSTEM(265)    "Whole month included"
  NLS_ID_GUI_INVOICE_SYSTEM(266)   	"Closing"
  // Form name
  NLS_ID_GUI_INVOICE_SYSTEM(267)   	"Activity per day and terminal"
  NLS_ID_GUI_INVOICE_SYSTEM(268)    "Provider"
  NLS_ID_GUI_INVOICE_SYSTEM(269)    "Term. x Day"
  NLS_ID_GUI_INVOICE_SYSTEM(270)    "%1 - %2"
  NLS_ID_GUI_INVOICE_SYSTEM(271)    "Providers: "
  NLS_ID_GUI_INVOICE_SYSTEM(272)    "All"
  NLS_ID_GUI_INVOICE_SYSTEM(273)    "Several"
  NLS_ID_GUI_INVOICE_SYSTEM(274)    "Filter by activity"
  NLS_ID_GUI_INVOICE_SYSTEM(275)    "Filter by provider"
  NLS_ID_GUI_INVOICE_SYSTEM(276)    "You must select a provider."
  NLS_ID_GUI_INVOICE_SYSTEM(277)    "Daily detail"
  NLS_ID_GUI_INVOICE_SYSTEM(278)    "Daily total"
  NLS_ID_GUI_INVOICE_SYSTEM(279)    "Netwin/(Term. x Day)"
  NLS_ID_GUI_INVOICE_SYSTEM(280)    "Daily total %1:"
  NLS_ID_GUI_INVOICE_SYSTEM(281)    "* Total includes redeemable and non-redeemable balance."
  NLS_ID_GUI_INVOICE_SYSTEM(282)   	"Withheld"
  NLS_ID_GUI_INVOICE_SYSTEM(283)   	"Redeemable Total > 0"
  NLS_ID_GUI_INVOICE_SYSTEM(284)   	"Non-Redeemable > 0"
  NLS_ID_GUI_INVOICE_SYSTEM(285)   	"Total > 0"
  NLS_ID_GUI_INVOICE_SYSTEM(286)   	"Movements"
  NLS_ID_GUI_INVOICE_SYSTEM(287)   	"Amount"
  NLS_ID_GUI_INVOICE_SYSTEM(288)   	"Initial"
  NLS_ID_GUI_INVOICE_SYSTEM(289)   	"Inputs"
  NLS_ID_GUI_INVOICE_SYSTEM(290)   	"Outputs"
  NLS_ID_GUI_INVOICE_SYSTEM(291)   	"Cash Deposits"
  NLS_ID_GUI_INVOICE_SYSTEM(292)   	"Cash Withdrawals"
  NLS_ID_GUI_INVOICE_SYSTEM(293)   	"Current Amount"
  NLS_ID_GUI_INVOICE_SYSTEM(294)   	"Initial Amount"
  NLS_ID_GUI_INVOICE_SYSTEM(295)   	"Subtotal"
  // Form name
  NLS_ID_GUI_INVOICE_SYSTEM(296)   	"Cash Session Summary"
  NLS_ID_GUI_INVOICE_SYSTEM(297)   	"Cash Result"
  NLS_ID_GUI_INVOICE_SYSTEM(298)   	"Winnings"
  NLS_ID_GUI_INVOICE_SYSTEM(299)   	"Cash Total"
  NLS_ID_GUI_INVOICE_SYSTEM(300)   	"MB Inputs"
  NLS_ID_GUI_INVOICE_SYSTEM(301)   	"MB Pending"
  NLS_ID_GUI_INVOICE_SYSTEM(302)   	"Federal"
  NLS_ID_GUI_INVOICE_SYSTEM(303)   	"State"
  NLS_ID_GUI_INVOICE_SYSTEM(304)   	"Input + MB"
  NLS_ID_GUI_INVOICE_SYSTEM(305)   	"Points"
  // Form name
  NLS_ID_GUI_INVOICE_SYSTEM(306)   	"Point Movements"
  NLS_ID_GUI_INVOICE_SYSTEM(307)   	"Account 2" //"Target Acc."
  NLS_ID_GUI_INVOICE_SYSTEM(308)   	"Card"
  NLS_ID_GUI_INVOICE_SYSTEM(309)   	"Cash In"
  NLS_ID_GUI_INVOICE_SYSTEM(310)   	"Cash Out"
  NLS_ID_GUI_INVOICE_SYSTEM(311)   	"Promotion"
  NLS_ID_GUI_INVOICE_SYSTEM(312)   	"No Redeemable to Redeemable"
  // Form name
  NLS_ID_GUI_INVOICE_SYSTEM(313)    "Mobile bank movements"
  NLS_ID_GUI_INVOICE_SYSTEM(314)    "Type"	// Handpay Type
  NLS_ID_GUI_INVOICE_SYSTEM(315)    "Mobile Bank"
  NLS_ID_GUI_INVOICE_SYSTEM(316)    "Show Movements"
  NLS_ID_GUI_INVOICE_SYSTEM(317)    "Amount"
  NLS_ID_GUI_INVOICE_SYSTEM(318)    "Recharge Total"
  NLS_ID_GUI_INVOICE_SYSTEM(319)    "Recharge Session Total"

    // Reserved 320-335: Handpay types
  NLS_ID_GUI_INVOICE_SYSTEM(320)   	"Canceled Credit"
  NLS_ID_GUI_INVOICE_SYSTEM(321)   	"Jackpot"
  NLS_ID_GUI_INVOICE_SYSTEM(322)   	"Abandoned Credit"
  NLS_ID_GUI_INVOICE_SYSTEM(323)   	"Site Jackpot"
  NLS_ID_GUI_INVOICE_SYSTEM(324)   	""
  NLS_ID_GUI_INVOICE_SYSTEM(325)   	""
  NLS_ID_GUI_INVOICE_SYSTEM(326)   	""
  NLS_ID_GUI_INVOICE_SYSTEM(327)   	""
  NLS_ID_GUI_INVOICE_SYSTEM(328)   	""
  NLS_ID_GUI_INVOICE_SYSTEM(329)   	""
  NLS_ID_GUI_INVOICE_SYSTEM(330)   	"Manual"
  NLS_ID_GUI_INVOICE_SYSTEM(331)   	""
  NLS_ID_GUI_INVOICE_SYSTEM(332)   	""
  NLS_ID_GUI_INVOICE_SYSTEM(333)   	""
  NLS_ID_GUI_INVOICE_SYSTEM(334)   	""
  NLS_ID_GUI_INVOICE_SYSTEM(335)   	""

  // Type of credits
  NLS_ID_GUI_INVOICE_SYSTEM(336)    "Credit type"
  NLS_ID_GUI_INVOICE_SYSTEM(337)    "Redeemable"
  NLS_ID_GUI_INVOICE_SYSTEM(338)    "Non-redeemable"
  NLS_ID_GUI_INVOICE_SYSTEM(339)    "All"
  NLS_ID_GUI_INVOICE_SYSTEM(340)    "Delivered:"
  NLS_ID_GUI_INVOICE_SYSTEM(341)    "Redeemable Total"
  NLS_ID_GUI_INVOICE_SYSTEM(342)    "Redeemable Promo"
  NLS_ID_GUI_INVOICE_SYSTEM(343)    "Points"

  // Leave space for other future type of credits
  // Form name
  NLS_ID_GUI_INVOICE_SYSTEM(345)   	"Old promotion report"
  // Form Name
  NLS_ID_GUI_INVOICE_SYSTEM(346)    "Tax report"
  NLS_ID_GUI_INVOICE_SYSTEM(347)    "Closing Date"
  NLS_ID_GUI_INVOICE_SYSTEM(348)    "Item"
  NLS_ID_GUI_INVOICE_SYSTEM(349)    "Value"
  NLS_ID_GUI_INVOICE_SYSTEM(350)    "Operation"
  NLS_ID_GUI_INVOICE_SYSTEM(351)    "Non-redeemable funds promotion"
  NLS_ID_GUI_INVOICE_SYSTEM(352)   	"Refunds"
  NLS_ID_GUI_INVOICE_SYSTEM(353)   	"Deleted promotion ID:"
  NLS_ID_GUI_INVOICE_SYSTEM(354)   	"Base"
  NLS_ID_GUI_INVOICE_SYSTEM(355)    "Promotion report - Detail"
  NLS_ID_GUI_INVOICE_SYSTEM(356)    "Non-Redeemable Promotion"
  NLS_ID_GUI_INVOICE_SYSTEM(357)    "Promo. to Redeemable"
  NLS_ID_GUI_INVOICE_SYSTEM(358)    "Redeemable Promotion"

  NLS_ID_GUI_INVOICE_SYSTEM(360)   	"Cash Result"  // It's like 297. Need two labels.

    // RCI 17-DEC-2010: Terminals without activity
  NLS_ID_GUI_INVOICE_SYSTEM(361)   	"Terminals without activity"   // Form Name
  NLS_ID_GUI_INVOICE_SYSTEM(362)   	"Only active machines"

  // RCI 20-DEC-2010: Mobile Bank Movements: Added Deposit and Pending MB
  NLS_ID_GUI_INVOICE_SYSTEM(363)    "Deposit Total"
  NLS_ID_GUI_INVOICE_SYSTEM(364)    "Deposit Session Total"
  NLS_ID_GUI_INVOICE_SYSTEM(365)    "Pending Total"
  NLS_ID_GUI_INVOICE_SYSTEM(366)    "RECHARGE TOTAL: "
  NLS_ID_GUI_INVOICE_SYSTEM(367)   	"DEPOSIT TOTAL: "
  NLS_ID_GUI_INVOICE_SYSTEM(368)    "PENDING TOTAL: "

  NLS_ID_GUI_INVOICE_SYSTEM(369)    "Options"
  NLS_ID_GUI_INVOICE_SYSTEM(370)    "Terminals"
  NLS_ID_GUI_INVOICE_SYSTEM(371)    "%1 subtotal"
  NLS_ID_GUI_INVOICE_SYSTEM(372)    "Totalizer"
  NLS_ID_GUI_INVOICE_SYSTEM(373)    "Total by provider"
  NLS_ID_GUI_INVOICE_SYSTEM(374)    "%1 total"
  NLS_ID_GUI_INVOICE_SYSTEM(375)    "Subtotal:"
  NLS_ID_GUI_INVOICE_SYSTEM(376)    "Subtotal by provider"
  NLS_ID_GUI_INVOICE_SYSTEM(377)    "Grouped"
  NLS_ID_GUI_INVOICE_SYSTEM(378)    "Totalizer"
  NLS_ID_GUI_INVOICE_SYSTEM(379)    "Hourly"
  NLS_ID_GUI_INVOICE_SYSTEM(380)    "24 Hours"

  // RCI 06-APR-2011: New column Level in Card Summary
  NLS_ID_GUI_INVOICE_SYSTEM(381)    "Level"

    // RCI 30-MAY-2011: Tickets report
  NLS_ID_GUI_INVOICE_SYSTEM(382)    "Cash Desk vouchers"                   // Menu Item
  NLS_ID_GUI_INVOICE_SYSTEM(383)    "Report"                               // Menu Item
  NLS_ID_GUI_INVOICE_SYSTEM(384)    "Cash Desk Voucher Report"             // Form Name

  NLS_ID_GUI_INVOICE_SYSTEM(385)    "Grouped Daily"
  NLS_ID_GUI_INVOICE_SYSTEM(386)    "Operation Type"
  NLS_ID_GUI_INVOICE_SYSTEM(387)    "Sales Note"
  NLS_ID_GUI_INVOICE_SYSTEM(388)    "Cancelation - Sales Note"
  NLS_ID_GUI_INVOICE_SYSTEM(389)    "Folio"
  NLS_ID_GUI_INVOICE_SYSTEM(390)    "Date and Time"
  NLS_ID_GUI_INVOICE_SYSTEM(391)    "Amount"
  NLS_ID_GUI_INVOICE_SYSTEM(392)    "VAT"
  NLS_ID_GUI_INVOICE_SYSTEM(393)    "Cash"
  NLS_ID_GUI_INVOICE_SYSTEM(394)    "Player Name"
  NLS_ID_GUI_INVOICE_SYSTEM(395)    "Deposit"
  NLS_ID_GUI_INVOICE_SYSTEM(396)    "Initial Balance"
  NLS_ID_GUI_INVOICE_SYSTEM(397)    "Series"
  NLS_ID_GUI_INVOICE_SYSTEM(398)    "%1 Trans."
  NLS_ID_GUI_INVOICE_SYSTEM(399)    "Paid Amount"
  NLS_ID_GUI_INVOICE_SYSTEM(400)    "Final Balance"
  NLS_ID_GUI_INVOICE_SYSTEM(401)    "Day"
  NLS_ID_GUI_INVOICE_SYSTEM(402)    "Transactions"

  // RCI 26-JUL-2011: Added email and address to the Card Summary Screen.
  NLS_ID_GUI_INVOICE_SYSTEM(403)    "Gender"
  //NLS_ID_GUI_INVOICE_SYSTEM(404)    "Last Activity"
  NLS_ID_GUI_INVOICE_SYSTEM(405)    "Birthday"
  NLS_ID_GUI_INVOICE_SYSTEM(406)    "Email / Twitter"
  NLS_ID_GUI_INVOICE_SYSTEM(407)    "Street"
  NLS_ID_GUI_INVOICE_SYSTEM(408)    "Colony"
  NLS_ID_GUI_INVOICE_SYSTEM(409)    "Delegation"
  NLS_ID_GUI_INVOICE_SYSTEM(410)    "City"
  NLS_ID_GUI_INVOICE_SYSTEM(411)    "Zip Code"
  NLS_ID_GUI_INVOICE_SYSTEM(412)    "Entered"
  NLS_ID_GUI_INVOICE_SYSTEM(413)    "Male"
  NLS_ID_GUI_INVOICE_SYSTEM(414)    "Female"
  NLS_ID_GUI_INVOICE_SYSTEM(415)    "Age"
  NLS_ID_GUI_INVOICE_SYSTEM(416)    "Exited"
  NLS_ID_GUI_INVOICE_SYSTEM(417)    "Permanence days"
  NLS_ID_GUI_INVOICE_SYSTEM(418)    "L."
  NLS_ID_GUI_INVOICE_SYSTEM(419)    "L"
  NLS_ID_GUI_INVOICE_SYSTEM(420)    "Holder data"

  // RCI 03-AUG-2011: Play Sessions Screen
  NLS_ID_GUI_INVOICE_SYSTEM(421)    "By provider"
  NLS_ID_GUI_INVOICE_SYSTEM(422)    "By account"
  NLS_ID_GUI_INVOICE_SYSTEM(423)    "Provider"
  NLS_ID_GUI_INVOICE_SYSTEM(424)    "Terminal"
  NLS_ID_GUI_INVOICE_SYSTEM(425)    "Show sessions"
  NLS_ID_GUI_INVOICE_SYSTEM(426)    "Prov: %1"

  // RCI 04-AUG-2011: Provider Activity Screen
  NLS_ID_GUI_INVOICE_SYSTEM(427)    "Show terminals"

  NLS_ID_GUI_INVOICE_SYSTEM(428)    "Date of Birth"
  NLS_ID_GUI_INVOICE_SYSTEM(429)    "Greater than"
  NLS_ID_GUI_INVOICE_SYSTEM(430)    "RFC"
  NLS_ID_GUI_INVOICE_SYSTEM(431)    "CURP"
  NLS_ID_GUI_INVOICE_SYSTEM(432)    "Net Prize" // ATB 20-MAR-2017: 25737: Third Tax - Report Columns
  NLS_ID_GUI_INVOICE_SYSTEM(433)    "Report Type"
  NLS_ID_GUI_INVOICE_SYSTEM(434)    "Grouped by account"
  NLS_ID_GUI_INVOICE_SYSTEM(435)    "In chronological order"
  NLS_ID_GUI_INVOICE_SYSTEM(436)    "With tax statement"
  NLS_ID_GUI_INVOICE_SYSTEM(437)    "Tax statement"
  NLS_ID_GUI_INVOICE_SYSTEM(438)    "* T: Tax statement available."
  NLS_ID_GUI_INVOICE_SYSTEM(439)    "T"                     //see previous entry
  NLS_ID_GUI_INVOICE_SYSTEM(440)    "With tax statement and greater than"
  // SSC 03-FEB-2012 Terminal Money
  NLS_ID_GUI_INVOICE_SYSTEM(441)    "By terminal"
  NLS_ID_GUI_INVOICE_SYSTEM(442)    "By terminal and bill"
  NLS_ID_GUI_INVOICE_SYSTEM(443)    "Nominal"
  NLS_ID_GUI_INVOICE_SYSTEM(444)    "Tax Statement viewer"
  NLS_ID_GUI_INVOICE_SYSTEM(445)    "Accepted by bill validator"
  NLS_ID_GUI_INVOICE_SYSTEM(446)    "Not transferred"
  NLS_ID_GUI_INVOICE_SYSTEM(447)    "Total value"

  NLS_ID_GUI_INVOICE_SYSTEM(448)    "Accepted Bills"   //Menu item
  NLS_ID_GUI_INVOICE_SYSTEM(449)    "Winnings taxes"          //Menu item
  NLS_ID_GUI_INVOICE_SYSTEM(450)    "Export account movements" //Menu item

  // IDs from 451 to 499 are reserved for Menu Items
  NLS_ID_GUI_INVOICE_SYSTEM(451)    "Accounting"
  NLS_ID_GUI_INVOICE_SYSTEM(452)    "Winnings taxes daily summary"
  NLS_ID_GUI_INVOICE_SYSTEM(453)    "Cash sessions"
  NLS_ID_GUI_INVOICE_SYSTEM(454)    "Cash movements"
  NLS_ID_GUI_INVOICE_SYSTEM(455)    "Account summary"
  NLS_ID_GUI_INVOICE_SYSTEM(456)    "Account movements"
  NLS_ID_GUI_INVOICE_SYSTEM(457)    "Grouped by date, provider and terminal"
  NLS_ID_GUI_INVOICE_SYSTEM(458)    "Group"
  NLS_ID_GUI_INVOICE_SYSTEM(459)    "Daily"
  NLS_ID_GUI_INVOICE_SYSTEM(460)    "Weekly"
  NLS_ID_GUI_INVOICE_SYSTEM(461)    "Monthly"
  NLS_ID_GUI_INVOICE_SYSTEM(462)    "Game"
  NLS_ID_GUI_INVOICE_SYSTEM(463)    "Closing Time"
  NLS_ID_GUI_INVOICE_SYSTEM(464)    "Exclude rows without activity"
  NLS_ID_GUI_INVOICE_SYSTEM(465)    "Provider activity"
  NLS_ID_GUI_INVOICE_SYSTEM(466)    "Point Movements" //Probably can be deleted...
  NLS_ID_GUI_INVOICE_SYSTEM(467)    "Last Activity"
  NLS_ID_GUI_INVOICE_SYSTEM(468)    "Registration"

  // Form Name
  NLS_ID_GUI_INVOICE_SYSTEM(469)    "Expired credit"
  NLS_ID_GUI_INVOICE_SYSTEM(470)    "Track"
  NLS_ID_GUI_INVOICE_SYSTEM(471)    "Expiration Date"
  NLS_ID_GUI_INVOICE_SYSTEM(472)    "Type"
  NLS_ID_GUI_INVOICE_SYSTEM(473)    "Redeemable"
  NLS_ID_GUI_INVOICE_SYSTEM(474)    "Non-redeemable"
  NLS_ID_GUI_INVOICE_SYSTEM(475)    "Promo"
  NLS_ID_GUI_INVOICE_SYSTEM(476)    "Show detail"
  NLS_ID_GUI_INVOICE_SYSTEM(477)    "Redeemable Expired"
  NLS_ID_GUI_INVOICE_SYSTEM(478)    "Non-Redeemable Expired"
  NLS_ID_GUI_INVOICE_SYSTEM(479)    "Yes"
  NLS_ID_GUI_INVOICE_SYSTEM(480)    "No"
  NLS_ID_GUI_INVOICE_SYSTEM(481)    "Handpays"
  NLS_ID_GUI_INVOICE_SYSTEM(482)    "Mobile bank movements"
  NLS_ID_GUI_INVOICE_SYSTEM(483)    "Tax Rounding"
  NLS_ID_GUI_INVOICE_SYSTEM(484)    "Card Deposit"
  NLS_ID_GUI_INVOICE_SYSTEM(485)    "Winnings Payout"
  NLS_ID_GUI_INVOICE_SYSTEM(486)    "Winnings taxes"
  NLS_ID_GUI_INVOICE_SYSTEM(487)    "Mobile Bank Pending"
  NLS_ID_GUI_INVOICE_SYSTEM(488)    "Total"
  NLS_ID_GUI_INVOICE_SYSTEM(489)   	"Withdrawal"

  NLS_ID_GUI_INVOICE_SYSTEM(490)   	"Promotion Summary"
  NLS_ID_GUI_INVOICE_SYSTEM(491)   	"Cash net"
  NLS_ID_GUI_INVOICE_SYSTEM(492)   	"You must select an initial date."
  NLS_ID_GUI_INVOICE_SYSTEM(493)   	"Cash Session Report"
  NLS_ID_GUI_INVOICE_SYSTEM(494)   	"Global Report"

  // RCI 20-DEC-2010: Terminals without activity
  NLS_ID_GUI_INVOICE_SYSTEM(495)   	"Terminals without activity"   // Menu Item

  NLS_ID_GUI_INVOICE_SYSTEM(496)   	"Deposits"
  NLS_ID_GUI_INVOICE_SYSTEM(497)   	"Withdrawals"
  NLS_ID_GUI_INVOICE_SYSTEM(498)   	"Collected"
  NLS_ID_GUI_INVOICE_SYSTEM(499)   	"Difference"
  
  // AVZ 04-NOV-2015: Accounst summary - reserved
  NLS_ID_GUI_INVOICE_SYSTEM(500)   	"Reserved"

  NLS_ID_GUI_INVOICE_SYSTEM(510)   	"Added in barred list"
  NLS_ID_GUI_INVOICE_SYSTEM(511)   	"Removed from barred list"

  // ATB 04-NOV-2016: Bug 20147:Televisa: Cambios solicitados por Televisa para "Movimientos en caja"
  // Only for Televisa
  NLS_ID_GUI_INVOICE_SYSTEM(512)    "Raffle result"

  // ATB 20-MAR-2017: 25737: Third Tax - Report Columns
  NLS_ID_GUI_INVOICE_SYSTEM(513)    "Total Taxes"


END
