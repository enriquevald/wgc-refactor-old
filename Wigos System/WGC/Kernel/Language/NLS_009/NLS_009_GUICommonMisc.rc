// ------------------------------------------------------------------------------------
// Copyright © 2004 Win Systems Ltd.
// ------------------------------------------------------------------------------------
//
//   MODULE NAME: NLS_009_GUICommonMisc.rc
//
//   DESCRIPTION: NLS resource LKC GUI Common Misc.
//
//        AUTHOR: Ronald Rodríguez T.
//
// CREATION DATE: 09-MAR-2004
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -------------------------------------------------------------------
// 09-MAR-2004 RRT    Initial release
// --------------------------------------------------------------------------------------

// WARNING !!!
//
// Do NOT modify this file with Visual Studio Resource Editor.
// This file MUST be edited using a TEXT editor.
//
// WARNING !!!

STRINGTABLE DISCARDABLE
BEGIN

  // Auditing Strings
  // %1 Field Name; %2 Current Value; %3 Previous value
  NLS_ID_GUI_COMMON_MISC(1)     "%1 = %2"
  NLS_ID_GUI_COMMON_MISC(2)     "%1 = %2 (was %3)"

  // Auditable Operations
  // %1 Entity who is being modified; %2 Entity Identifier
  NLS_ID_GUI_COMMON_MISC(3)     "Creation of %1 %2"
  NLS_ID_GUI_COMMON_MISC(4)     "Modification of %1 %2"
  NLS_ID_GUI_COMMON_MISC(5)     "Deletion of %1 %2"

  // Generic Operation
  // Real operation string will be provided by the caller
  // This way we avoid defining every possible operation here
  NLS_ID_GUI_COMMON_MISC(6)     "%1%2%3%4%5"
  NLS_ID_GUI_COMMON_MISC(7)     "WGC"
  NLS_ID_GUI_COMMON_MISC(8)     "WIGOS System"
  NLS_ID_GUI_COMMON_MISC(9)     "Deleted"
  NLS_ID_GUI_COMMON_MISC(10)    "Added"
  NLS_ID_GUI_COMMON_MISC(11)    "Manager"
  // 12 Months - must be continued
  NLS_ID_GUI_COMMON_MISC(12)    "January"
  NLS_ID_GUI_COMMON_MISC(13)    "February"
  NLS_ID_GUI_COMMON_MISC(14)    "March"
  NLS_ID_GUI_COMMON_MISC(15)    "April"
  NLS_ID_GUI_COMMON_MISC(16)    "May"
  NLS_ID_GUI_COMMON_MISC(17)    "June"
  NLS_ID_GUI_COMMON_MISC(18)    "July"
  NLS_ID_GUI_COMMON_MISC(19)    "August"
  NLS_ID_GUI_COMMON_MISC(20)    "September"
  NLS_ID_GUI_COMMON_MISC(21)    "October"
  NLS_ID_GUI_COMMON_MISC(22)    "November"
  NLS_ID_GUI_COMMON_MISC(23)    "December"
  // 7 Days of the week - must be continued
  NLS_ID_GUI_COMMON_MISC(24)    "Sunday"
  NLS_ID_GUI_COMMON_MISC(25)    "Monday"
  NLS_ID_GUI_COMMON_MISC(26)    "Tuesday"
  NLS_ID_GUI_COMMON_MISC(27)    "Wednesday"
  NLS_ID_GUI_COMMON_MISC(28)    "Thursday"
  NLS_ID_GUI_COMMON_MISC(29)    "Friday"
  NLS_ID_GUI_COMMON_MISC(30)    "Saturday"
  // Order of selected day in the month (5) - must be continued
  NLS_ID_GUI_COMMON_MISC(31)    "First"
  NLS_ID_GUI_COMMON_MISC(32)    "Second"
  NLS_ID_GUI_COMMON_MISC(33)    "Third"
  NLS_ID_GUI_COMMON_MISC(34)    "Fourth"
  NLS_ID_GUI_COMMON_MISC(35)    "Last"

  // Messages
  NLS_ID_GUI_COMMON_MISC(200)   "Starting..."
  NLS_ID_GUI_COMMON_MISC(201)   "Reading Configuration..."
  NLS_ID_GUI_COMMON_MISC(202)   "Connecting to Database..."

END
