// ------------------------------------------------------------------------------------
// Copyright � 2004 Win Systems Ltd.
// ------------------------------------------------------------------------------------
//
//   MODULE NAME: NLS_009_GUIAuditor.rc
//
//   DESCRIPTION: NLS resource LKC GUI Auditor.
//
//        AUTHOR: Ronald Rodr�guez T.
//
// CREATION DATE: 09-MAR-2004
//
// REVISION HISTORY:
//
// Date        Author Description
// ----------- ------ -------------------------------------------------------------------
// 09-MAR-2004 RRT    Initial release
// --------------------------------------------------------------------------------------

// WARNING !!!
//
// Do NOT modify this file with Visual Studio Resource Editor.
// This file MUST be edited using a TEXT editor.
//
// WARNING !!!

STRINGTABLE DISCARDABLE
BEGIN

  // IDs from 1 to 50 are reserved for buttons
  // IDs from 51 to 100 are reserved for application specific requirements
  // IDs from 101 to 200 are reserved for messages
  // IDs from 201 to 450 are reserved for Labels
  // IDs from 451 to 499 are reserved for Menu Items

  // Application Names & Buttons: 1-50
  //NLS_ID_GUI_AUDIT(1)     "Alarms"
  //NLS_ID_GUI_AUDIT(2)     "Versions Control"
  NLS_ID_GUI_AUDIT(3)     "Audit"
  //NLS_ID_GUI_AUDIT(4)     "Hierarchies"
  //NLS_ID_GUI_AUDIT(5)     "Configuration"
  //NLS_ID_GUI_AUDIT(6)     "Communications"
  //NLS_ID_GUI_AUDIT(7)     "Control"
  //NLS_ID_GUI_AUDIT(8)     "Tickets Manager"
  //NLS_ID_GUI_AUDIT(9)     "Statistics"
  //NLS_ID_GUI_AUDIT(10)    "Jackpots Manager"
  //NLS_ID_GUI_AUDIT(11)    "System Monitor"
  NLS_ID_GUI_AUDIT(12)    "Exit"
  //NLS_ID_GUI_AUDIT(13)    "Financial System"
  //NLS_ID_GUI_AUDIT(14)    "Software Download"
  //NLS_ID_GUI_AUDIT(15)    "Date Code"
  // RCI 13-MAY-2010: Terminals 3GS Edit Screen
  NLS_ID_GUI_AUDIT(16)    "Assign"
  NLS_ID_GUI_AUDIT(17)    "Fill"
  NLS_ID_GUI_AUDIT(18)    "Add"
  NLS_ID_GUI_AUDIT(19)    "Replace"

  // IDs from 51 to 100 are reserved for application specific requirements
	// After 99 continues on 483
  // Audit Codes
  //NLS_ID_GUI_AUDIT(51)    "Hierarchies"
  //NLS_ID_GUI_AUDIT(52)    "Alarms"
  //NLS_ID_GUI_AUDIT(53)    "Series"
  //NLS_ID_GUI_AUDIT(54)    "Login/Logout"
  //NLS_ID_GUI_AUDIT(55)    "Price Levels"
  NLS_ID_GUI_AUDIT(56)    "Users"
  NLS_ID_GUI_AUDIT(57)    "Profiles"
  //NLS_ID_GUI_AUDIT(58)    "Games"
  NLS_ID_GUI_AUDIT(59)    "Terminals"
  //NLS_ID_GUI_AUDIT(60)    "Stocks"
  //NLS_ID_GUI_AUDIT(61)    "Computers"
  //NLS_ID_GUI_AUDIT(62)    "Nodes"
  //NLS_ID_GUI_AUDIT(63)    "Connections"
  //NLS_ID_GUI_AUDIT(64)    "Books"
  //NLS_ID_GUI_AUDIT(65)    "Jackpot Parameters"
  //NLS_ID_GUI_AUDIT(66)    "Client Window"
  NLS_ID_GUI_AUDIT(67)    "General parameters"
  //NLS_ID_GUI_AUDIT(68)    "Music"
  //NLS_ID_GUI_AUDIT(69)    "Monitoring"
  //NLS_ID_GUI_AUDIT(70)    "Financial"
  NLS_ID_GUI_AUDIT(71)    "Download"
  NLS_ID_GUI_AUDIT(72)    "Jackpot - WIN"
  NLS_ID_GUI_AUDIT(73)    "Terminal-game relation"
  NLS_ID_GUI_AUDIT(74)    "Promotions"
  NLS_ID_GUI_AUDIT(75)    "Gifts"
  NLS_ID_GUI_AUDIT(76)    "Jackpot - Site"
  NLS_ID_GUI_AUDIT(77)    "Mailing"
  NLS_ID_GUI_AUDIT(78)    "Configuration"
  NLS_ID_GUI_AUDIT(79)    "Accounts"
  NLS_ID_GUI_AUDIT(80)    "Raffles"
  NLS_ID_GUI_AUDIT(81)    "Access control"
  NLS_ID_GUI_AUDIT(82)    "Card writer"
  NLS_ID_GUI_AUDIT(83)    "Terminal - Retirement"
  NLS_ID_GUI_AUDIT(84)    "Alarms"
  NLS_ID_GUI_AUDIT(85)    "Bill validators"
  NLS_ID_GUI_AUDIT(86)    "Cash sessions"
  NLS_ID_GUI_AUDIT(87)    "MultiSite"
  NLS_ID_GUI_AUDIT(88)    "Cash Desk voucher reprint"
  NLS_ID_GUI_AUDIT(89)    "Providers"
  NLS_ID_GUI_AUDIT(90)    "TITO"
  NLS_ID_GUI_AUDIT(91)    "Reprint ticket"
	NLS_ID_GUI_AUDIT(92)		"Forms"
	NLS_ID_GUI_AUDIT(93)		"Cash cage"
  NLS_ID_GUI_AUDIT(94)		"Gambling tables"
  NLS_ID_GUI_AUDIT(95)		"Bonuses"
	NLS_ID_GUI_AUDIT(96)		"Play sessions"
  NLS_ID_GUI_AUDIT(97)		"Patterns"
  NLS_ID_GUI_AUDIT(98)		"Progressive provisions"
  NLS_ID_GUI_AUDIT(99)		"Progressive jackpots"

  NLS_ID_GUI_AUDIT(100)   "WigosGUI"   // Aplication Name

  // IDs from 101 to 200 are reserved for messages
  NLS_ID_GUI_AUDIT(101)   "Date range error: initial date must be before end date."
  NLS_ID_GUI_AUDIT(102)   "You must select a %1."
  //NLS_ID_GUI_AUDIT(103)   "Are you sure you want to exit?"
  NLS_ID_GUI_AUDIT(104)   "You must select a Device/Operation."
  //NLS_ID_GUI_AUDIT(105)   "You must select at least one type."
  //NLS_ID_GUI_AUDIT(106)   "You must select an %1."
  //NLS_ID_GUI_AUDIT(107)   "The Kiosk number should be between 1 and %1."
  NLS_ID_GUI_AUDIT(108)   "Changes were successfully saved."
  NLS_ID_GUI_AUDIT(109)   "Error saving changes: \n%1"
  NLS_ID_GUI_AUDIT(110)   "Servers with assigned terminals cannot be deleted.\nAll your changes will be lost."
  // RCI 13-MAY-2010: Terminals 3GS Edit Screen
  NLS_ID_GUI_AUDIT(111)   "The assignment will result in the loss of all changes. Do you want to continue?"
  NLS_ID_GUI_AUDIT(112)   "The assignment was successfully completed."
  NLS_ID_GUI_AUDIT(113)   "Error managing terminal: \n%1"
  NLS_ID_GUI_AUDIT(114)   "%1 field must not be left blank."
  NLS_ID_GUI_AUDIT(115)   "The pair %1 / %2 already exists.\nThe pair %3 / %4 must be unique."
  NLS_ID_GUI_AUDIT(116)   "%1 already exists.\n%2 must be unique."
  NLS_ID_GUI_AUDIT(117)   "Unable to add all terminals."
  NLS_ID_GUI_AUDIT(118)   "You must select at least one terminal."
  NLS_ID_GUI_AUDIT(119)   "Unable to disconnect terminals."
  NLS_ID_GUI_AUDIT(120)   "Unable to replace all terminals."

  // RCI 25-JUN-2010: Handpays Screen
  NLS_ID_GUI_AUDIT(121)   "Date: %1\nProvider: %2\nTerminal: %3\nAmount: %4"
  NLS_ID_GUI_AUDIT(122)   "\Now:\n   Provider: %1\n   Terminal: %2"

  NLS_ID_GUI_AUDIT(123)   "Terminal %1 cannot be retired because it is a 3GS terminal"
  NLS_ID_GUI_AUDIT(124)   "You must select 'SAS Host' terminals to disconnect eBox."
  NLS_ID_GUI_AUDIT(125)   "You must save all pending changes to edit a terminal."

  NLS_ID_GUI_AUDIT(126)   "There is already a terminal named %1."

  // RCI 01-AUG-2011: Capacity Screen
  NLS_ID_GUI_AUDIT(127)   "You must select at least one provider."
  NLS_ID_GUI_AUDIT(128)   "You must select at least one option from '%1' and '%2'."
  NLS_ID_GUI_AUDIT(129)   "You must select at least one option from '%1', '%2' and '%3'."

  // ICS 17-DEC-2012: Capacity Screen
  NLS_ID_GUI_AUDIT(130)   "You must select a row."

	// ACM 11-OCT-2013: Terminals pending
	NLS_ID_GUI_AUDIT(131)   "%1 field cannot contain line breaks or other invalid characters."

  // IDs from 201 to 450 are reserved for Labels
  NLS_ID_GUI_AUDIT(251)   "(All)"
  NLS_ID_GUI_AUDIT(252)   "Message"
  NLS_ID_GUI_AUDIT(253)   "GUI"
  NLS_ID_GUI_AUDIT(254)   "User"
  //NLS_ID_GUI_AUDIT(255)   "Level"
  NLS_ID_GUI_AUDIT(256)   "Audit Date"
  NLS_ID_GUI_AUDIT(257)   "From"
  NLS_ID_GUI_AUDIT(258)   "To"
  //NLS_ID_GUI_AUDIT(259)   "Audit List"
  NLS_ID_GUI_AUDIT(260)   "Audit Code"
  NLS_ID_GUI_AUDIT(261)   "Date"
  NLS_ID_GUI_AUDIT(262)   "One"
  NLS_ID_GUI_AUDIT(263)   "All"
  NLS_ID_GUI_AUDIT(264)   "Code"
  //NLS_ID_GUI_AUDIT(265)   "Book Audit"
  //NLS_ID_GUI_AUDIT(266)   "Book History"
  // Form Name
  NLS_ID_GUI_AUDIT(267)   "Event History"
  //NLS_ID_GUI_AUDIT(268)   "Vouchers Audit"
  //NLS_ID_GUI_AUDIT(269)   "Devices"
  NLS_ID_GUI_AUDIT(270)   "Several"
  //NLS_ID_GUI_AUDIT(271)   "Details"
  //NLS_ID_GUI_AUDIT(272)   "Agency"
  NLS_ID_GUI_AUDIT(273)   "Kiosk"
  //NLS_ID_GUI_AUDIT(274)   "Status"
  //NLS_ID_GUI_AUDIT(275)   "OK"
  //NLS_ID_GUI_AUDIT(276)   "Error"
  //NLS_ID_GUI_AUDIT(277)   "Hierarchy"
  //NLS_ID_GUI_AUDIT(278)   "Terminal"
  //NLS_ID_GUI_AUDIT(279)   "Device / Operation"
  //NLS_ID_GUI_AUDIT(280)   "---"
  //// Voucher Type
  //NLS_ID_GUI_AUDIT(281)   "Cash"
  //NLS_ID_GUI_AUDIT(282)   "Major Prize"
  //NLS_ID_GUI_AUDIT(283)   "Jackpot"
  //NLS_ID_GUI_AUDIT(284)   "Cash Operations"
  // Event Types
  //NLS_ID_GUI_AUDIT(285)   "Print from Agency"
  //NLS_ID_GUI_AUDIT(286)   "Print from Kiosk"
  //NLS_ID_GUI_AUDIT(287)   "Cash"
  //NLS_ID_GUI_AUDIT(288)   "Reprint"
  //NLS_ID_GUI_AUDIT(289)   "Expired Cash"
  //NLS_ID_GUI_AUDIT(290)   "Open Cash"
  //NLS_ID_GUI_AUDIT(291)   "Close Cash"
  //NLS_ID_GUI_AUDIT(292)   "Payment Receipt"
  //// Labels
  //NLS_ID_GUI_AUDIT(293)   "Voucher"
  //NLS_ID_GUI_AUDIT(294)   "Series"
  //NLS_ID_GUI_AUDIT(295)   "Book"
  //NLS_ID_GUI_AUDIT(296)   "Ticket"
  //NLS_ID_GUI_AUDIT(297)   "Game"
  //NLS_ID_GUI_AUDIT(298)   "Type"
  //NLS_ID_GUI_AUDIT(299)   "Amount"
  //NLS_ID_GUI_AUDIT(300)   "Desc."
  //NLS_ID_GUI_AUDIT(301)   "LKAS"
  //NLS_ID_GUI_AUDIT(302)   "LKT"
  //NLS_ID_GUI_AUDIT(303)   "Operations"
  //NLS_ID_GUI_AUDIT(304)   "Warning"
  //NLS_ID_GUI_AUDIT(305)   "Smart Card"
  //NLS_ID_GUI_AUDIT(306)   "Operation"
  //NLS_ID_GUI_AUDIT(307)   "Identifier"
  //NLS_ID_GUI_AUDIT(308)   "Tickets"
  //NLS_ID_GUI_AUDIT(309)   "Sold"
  //NLS_ID_GUI_AUDIT(310)   "AS / Kiosk"
  NLS_ID_GUI_AUDIT(311)   "AS"
  //NLS_ID_GUI_AUDIT(312)   "Automatic Shutdown"
  //NLS_ID_GUI_AUDIT(313)   "Shutdown (UPS)"
  //NLS_ID_GUI_AUDIT(314)   "Device status"
  //NLS_ID_GUI_AUDIT(315)   "Prev."
  //NLS_ID_GUI_AUDIT(316)   "Sold Tickets"
  //NLS_ID_GUI_AUDIT(317)   "Total Tickets in Book"
  //NLS_ID_GUI_AUDIT(318)   "Generated"
  //NLS_ID_GUI_AUDIT(319)   "Deleted"
  //NLS_ID_GUI_AUDIT(320)   "Activated"
  //NLS_ID_GUI_AUDIT(321)   "Bought"
  //NLS_ID_GUI_AUDIT(322)   "Withdrawn"
  //NLS_ID_GUI_AUDIT(323)   "Assigned"
  //NLS_ID_GUI_AUDIT(324)   "Sold"
  //NLS_ID_GUI_AUDIT(325)   "Deassigned"
  //NLS_ID_GUI_AUDIT(326)   "Ticket Sold"
  // Form Name
  NLS_ID_GUI_AUDIT(327)   "GUI Audit"
  NLS_ID_GUI_AUDIT(328)   "Terminals"
  // Terminals Edit screen
  NLS_ID_GUI_AUDIT(329)   "Server"
  NLS_ID_GUI_AUDIT(330)   "Name"
  NLS_ID_GUI_AUDIT(331)   "Protocol ID"
  NLS_ID_GUI_AUDIT(332)   "Locked"
  NLS_ID_GUI_AUDIT(333)   "Terminal"
  NLS_ID_GUI_AUDIT(334)   "Servers"
  NLS_ID_GUI_AUDIT(335)   "Server %1 Terminals"
  NLS_ID_GUI_AUDIT(336)   "Yes"
  NLS_ID_GUI_AUDIT(337)   "No"
  NLS_ID_GUI_AUDIT(338)   "Server"
  NLS_ID_GUI_AUDIT(339)   "Terminal"
  NLS_ID_GUI_AUDIT(340)   "Serverless terminals"
  NLS_ID_GUI_AUDIT(341)   "Provider"
  NLS_ID_GUI_AUDIT(342)   "Version"
  NLS_ID_GUI_AUDIT(343)   "Client"
  NLS_ID_GUI_AUDIT(344)   "Vendor"

  // RCI 13-MAY-2010: Terminals 3GS Edit Screen
  NLS_ID_GUI_AUDIT(345)   "Pending Terminal"
  NLS_ID_GUI_AUDIT(346)   "3GS Terminal"
  NLS_ID_GUI_AUDIT(347)   "Pending terminals"
  NLS_ID_GUI_AUDIT(348)   "3GS terminals"
  NLS_ID_GUI_AUDIT(349)   "Source"
  //NLS_ID_GUI_AUDIT(350)   "Machine Number"   //Instead, use CONF(211)
  NLS_ID_GUI_AUDIT(351)   "Reported Date"
  NLS_ID_GUI_AUDIT(352)   "Ignored"
  NLS_ID_GUI_AUDIT(353)   "Hide ignored"
  //NLS_ID_GUI_AUDIT(354)   "Serial Number"    //Instead, use CONF(214)

  NLS_ID_GUI_AUDIT(355)   "Source_0"
  NLS_ID_GUI_AUDIT(356)   "Source_1"
  NLS_ID_GUI_AUDIT(357)   "Source_2"
  NLS_ID_GUI_AUDIT(358)   "Source_3"
  NLS_ID_GUI_AUDIT(359)   "Source_4"
  NLS_ID_GUI_AUDIT(360)   "Source_5"
  NLS_ID_GUI_AUDIT(361)   "Source_6"
  NLS_ID_GUI_AUDIT(362)   "Source_7"
  NLS_ID_GUI_AUDIT(363)   "Source_8"
  NLS_ID_GUI_AUDIT(364)   "Source_9"

  NLS_ID_GUI_AUDIT(365)   "Terminal Editor"

  // RCI 25-JUN-2010: Handpays Screen
  NLS_ID_GUI_AUDIT(371)   "Handpay History"
  NLS_ID_GUI_AUDIT(372)   "Amount"
  NLS_ID_GUI_AUDIT(373)   "Pending"
  NLS_ID_GUI_AUDIT(374)   "Paid to account %1, by %2"
  NLS_ID_GUI_AUDIT(375)   "Total Paid:"
  NLS_ID_GUI_AUDIT(376)   "Total Pending:"
  NLS_ID_GUI_AUDIT(377)   "Total Expired:"
  NLS_ID_GUI_AUDIT(378)   "Expired"

  // RCI 07-JAN-2011
  NLS_ID_GUI_AUDIT(379)   "LKT WIN"         // 1
  NLS_ID_GUI_AUDIT(380)   "SAS Host"    // 5
  NLS_ID_GUI_AUDIT(381)   "3GS"             // 3
  NLS_ID_GUI_AUDIT(382)   "SITE JACKPOT"    // 101
  NLS_ID_GUI_AUDIT(383)   "MOBILE BANK"     // 102
  NLS_ID_GUI_AUDIT(384)   "SITE"            // 100
  NLS_ID_GUI_AUDIT(385)   "iMB"             // 103
  NLS_ID_GUI_AUDIT(386)   "iSTATS"          // 104
  NLS_ID_GUI_AUDIT(387)   "PromoBOX"        // 105

  // RCI 29-JUL-2011: Capacity Screen
  NLS_ID_GUI_AUDIT(400)   "Capacity by provider"
  NLS_ID_GUI_AUDIT(401)   "Men"
  NLS_ID_GUI_AUDIT(402)   "Women"
  NLS_ID_GUI_AUDIT(403)   "Anonymous"
  NLS_ID_GUI_AUDIT(404)   "Provider"
  NLS_ID_GUI_AUDIT(405)   "Time"
  NLS_ID_GUI_AUDIT(406)   "Whole site"
  NLS_ID_GUI_AUDIT(407)   "Show provider capacity"
  NLS_ID_GUI_AUDIT(408)   "Show site capacity"
  NLS_ID_GUI_AUDIT(409)   "Exclude providers without activity"
  NLS_ID_GUI_AUDIT(410)   "Data for today after current time is zero"
  NLS_ID_GUI_AUDIT(411)   "Average capacity for the selected date range is displayed"
  NLS_ID_GUI_AUDIT(412)   "Number of days in date range: %1"
  NLS_ID_GUI_AUDIT(413)   "Days"
  NLS_ID_GUI_AUDIT(414)   "Bills"
  NLS_ID_GUI_AUDIT(415)   "Day"
  NLS_ID_GUI_AUDIT(416)   "By day and provider"
  NLS_ID_GUI_AUDIT(417)   "By provider and day"
  NLS_ID_GUI_AUDIT(418)   "Show daily average"
  NLS_ID_GUI_AUDIT(419)   "Date range error: initial date must be before current date."
  NLS_ID_GUI_AUDIT(420)   "Average Capacity"
  NLS_ID_GUI_AUDIT(421)   "Time"
  NLS_ID_GUI_AUDIT(422)   "Detailed Capacity by Hour"
  NLS_ID_GUI_AUDIT(423)   "Total"
  NLS_ID_GUI_AUDIT(424)   "Detail"
  
  // IDs from 451 to 499 are reserved for Menu Items
  NLS_ID_GUI_AUDIT(451)   "GUI"
  NLS_ID_GUI_AUDIT(452)   "Exit"
  NLS_ID_GUI_AUDIT(453)   "System"
  //NLS_ID_GUI_AUDIT(454)   "Books"
  NLS_ID_GUI_AUDIT(455)   "Events"
  //NLS_ID_GUI_AUDIT(456)   "Vouchers"
  NLS_ID_GUI_AUDIT(457)   "History"
  NLS_ID_GUI_AUDIT(458)   "Terminals"
  NLS_ID_GUI_AUDIT(459)   "Audit"
  NLS_ID_GUI_AUDIT(460)   "General parameters"

  // RCI 13-MAY-2010: Terminals 3GS Edit Screen
  NLS_ID_GUI_AUDIT(461)   "3GS terminals"

  // RCI 28-JUN-2010: Handpays Screen
  NLS_ID_GUI_AUDIT(462)   "Handpays"

  // MBF 16-AUG-2010: Machine Management
  NLS_ID_GUI_AUDIT(463)   "Pending terminals"
  NLS_ID_GUI_AUDIT(464)   "The following terminals will be disconnected:\n%1%2%3%4%5\nDo you want to continue?"
  NLS_ID_GUI_AUDIT(465)   "Added/Replaced Terminals"
  NLS_ID_GUI_AUDIT(466)   "Retire"
  NLS_ID_GUI_AUDIT(467)   "Unlock"
  NLS_ID_GUI_AUDIT(468)   "Disconnect eBox"
  NLS_ID_GUI_AUDIT(469)   "Terminal Retirement"

  // RCI 29-JUL-2011: Capacity Screen
  NLS_ID_GUI_AUDIT(470)   "Capacity by provider"
  NLS_ID_GUI_AUDIT(471)   "One or more terminal names are already in use.\nNames already in use: \n%1"

  // MPO 06-MAR-2013: Customer - Multisite
  NLS_ID_GUI_AUDIT(472)   "Customers"
	
  NLS_ID_GUI_AUDIT(473)   "" // Free

	// RRR 25-JUN-2013: New Sales Limit Audit View
  NLS_ID_GUI_AUDIT(474)   "Sales Limit Audit"

	// JCA 28-AUG-2013: CashDesk Raffle
	NLS_ID_GUI_AUDIT(475)   "Wigos Draw GUI"

	//LJM 11-nov-2013: New audit for searches
	NLS_ID_GUI_AUDIT(476)		"Search on form: "
	NLS_ID_GUI_AUDIT(477)   "Filters"
	NLS_ID_GUI_AUDIT(478)		"Search - "
	NLS_ID_GUI_AUDIT(479)		"Access - "
	NLS_ID_GUI_AUDIT(480)		"Excel - "
	NLS_ID_GUI_AUDIT(481)		"Print - "
	NLS_ID_GUI_AUDIT(482)		"Formato: "

	// DHA 16-APR-2015: New audit code for meters
	NLS_ID_GUI_AUDIT(483)		"Meters"

	// SDS 26-AUG-2015: Nuevo formulario de cat�logos existentes
	NLS_ID_GUI_AUDIT(484)		"Catalogs"
	NLS_ID_GUI_AUDIT(485)		"Catalog Edit"


  // JRC 16-NOV-2015 Product Backlog Item 5993 GUI - Creaci�n e Impresi�n C�digos QR
  NLS_ID_GUI_AUDIT(486)		"Print machine QR codes"

  // PDM 09-AGO-2016
  NLS_ID_GUI_AUDIT(487)		"Export -"

  //AVZ - GDA 21-NOV-2016 - WINUP Audit data
  NLS_ID_GUI_AUDIT(488)		"Winup"

  // AMF 10-AUG-2017
  NLS_ID_GUI_AUDIT(491)		"Credit lines"
  NLS_ID_GUI_AUDIT(492)		"Junkets"

  // FGB 05-SEP-2017
  NLS_ID_GUI_AUDIT(493)   "Terminal reservation"
  NLS_ID_GUI_AUDIT(494)   "Customer notices"
  NLS_ID_GUI_AUDIT(495)   "MoviBank"

  //FJC 20-12-2017
  NLS_ID_GUI_AUDIT(496)   "Billing"

	END
