//------------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_PendingnrollsQueue.cs
// 
//   DESCRIPTION: WC2_PendingEnrollsQueue class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 07-NOV-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 07-NOV-2008 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Messaging;
using System.Collections.Generic;
using System.Text;
using System.Net;
using WSI.Common;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using WSI.WC2;
using System.Collections;

namespace WSI.WC2
{
  /// <summary>
  /// Represents a enroll that is waiting to be processed 
  /// </summary>
  public class EnrollQueueElement
  {
    private Object m_wc2_listener;
    private Object m_wc2_client;
    private WC2_Message m_xml_message_enroll;

    // Constructor
    public EnrollQueueElement(Object Wc2Listener, Object Wc2Client, WC2_Message XmlMessageEnroll)
    {
      m_wc2_listener = Wc2Listener;
      m_wc2_client = Wc2Client;
      m_xml_message_enroll = XmlMessageEnroll;
    }

    //
    // Properties
    //

    public Object Wc2Listener
    {
      get
      {
        return m_wc2_listener;
      }
      set
      {
        m_wc2_listener = value;
      }
    }

    public Object Wc2Client
    {
      get
      {
        return m_wc2_client;
      }
      set
      {
        m_wc2_client = value;
      }
    }

    public WC2_Message XmlMessageEnroll
    {
      get
      {
        return m_xml_message_enroll;
      }
      set
      {
        m_xml_message_enroll = value;
      }
    }
  }

  public class EnrollProcessTask : Task
  {
    Object m_wc2_listener;
    Object m_wc2_client;
    WC2_Message m_xml_message_enroll;

    int m_tick_arrived;
    int m_tick_dequeued;
    int m_tick_processed;

    public EnrollProcessTask(Object Wc2Listener, Object Wc2Client, WC2_Message XmlMessageEnroll)
    {
      m_wc2_listener = Wc2Listener;
      m_wc2_client = Wc2Client;
      m_xml_message_enroll = XmlMessageEnroll;

      m_tick_arrived = Environment.TickCount;
      m_tick_dequeued = m_tick_arrived;
      m_tick_processed = m_tick_arrived;
    }

    public override void Execute()
    {
      SecureTcpClient _tcp_client;
      SecureTcpServer _tcp_server;
      long _interval;

      m_tick_dequeued = Environment.TickCount;
      m_tick_processed = m_tick_dequeued;
      _interval = Misc.GetElapsedTicks(m_tick_arrived, m_tick_dequeued);
      if (_interval >= 10000)
      {
        // Ignore the task     
        Log.Warning("Enroll Message Ignored: timeout. Queued: " + _interval.ToString() + " ms. Terminal: " + m_xml_message_enroll.MsgHeader.TerminalId);

        return;
      }

      _tcp_client = (SecureTcpClient)m_wc2_client;
      _tcp_server = (SecureTcpServer)m_wc2_listener;

      if (!_tcp_server.IsConnected(_tcp_client))
      {
        // Ignore the task     
        Log.Warning("Enroll Message Ignored: disconnected. Queued: " + _interval.ToString() + " ms. Terminal: " + m_xml_message_enroll.MsgHeader.TerminalId);

        return;
      }

      try
      {
        WC2_PendingEnrollsQueue.EnrollWorker(m_wc2_listener, m_wc2_client, m_xml_message_enroll);
      }
      catch
      {
      }
      finally
      {
        long _tq;
        long _tx;
        long _tt;

        m_tick_processed = Environment.TickCount;

        _tq = Misc.GetElapsedTicks(m_tick_arrived, m_tick_dequeued);
        _tx = Misc.GetElapsedTicks(m_tick_dequeued, m_tick_processed);
        _tt = Misc.GetElapsedTicks(m_tick_arrived, m_tick_processed);

        if (_tq > 10000
            || _tx > 10000
            || _tt > 20000)
        {
          Log.Warning("Enroll Message times: Queued/Process/Total = " + _tq.ToString() + "/" + _tx.ToString() + "/" + _tt.ToString() + " ----> Terminal: " + m_xml_message_enroll.MsgHeader.TerminalId);
        }
      }
    }
  }

  /// <summary>
  /// Holds enroll data that awaits to be processed
  /// </summary>
  public class WC2_PendingEnrollsQueue
  {
    private Worker wc2_enroll_worker;
    private WaitableQueue wc2_enroll_waitable_queue;

    private static WC2_PendingEnrollsQueue wc2_pending_enrolls;

    //------------------------------------------------------------------------------
    // PURPOSE : Initialize an instance of the WC2_PendingEnrollsQueue class 
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void Init()
    {
      wc2_pending_enrolls = new WC2_PendingEnrollsQueue();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Returns the Current instance of the WC2_PendingEnrollsQueue Class
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - instance of the WC2_PendingEnrollsQueue
    //
    //   NOTES :
    //
    public static WC2_PendingEnrollsQueue GetInstance()
    {
      return wc2_pending_enrolls;
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Start workers for this tasks
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private void StartWorker()
    {
      // Create queue enrolls
      wc2_enroll_waitable_queue = new WaitableQueue();

      // - Message received processing worker
      // Start workers for WC2 requests
      //
      // ONLY 1 WORKER for enroll trx !!!
      //
      wc2_enroll_worker = new Worker(wc2_enroll_waitable_queue);
      wc2_enroll_worker.Start();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Public function for start workers
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void Start()
    {
      // Workers start
      wc2_pending_enrolls.StartWorker();
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Enqueues a enroll element
    //
    //  PARAMS :
    //      - INPUT :
    //         - EnrollElement
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public void EnqueueEnroll(EnrollQueueElement EnrollElement)
    {
      EnrollProcessTask enroll_process_task;

      // Enqueue DRAW
      enroll_process_task = new EnrollProcessTask(EnrollElement.Wc2Listener, EnrollElement.Wc2Client, EnrollElement.XmlMessageEnroll);
      wc2_enroll_waitable_queue.Enqueue(enroll_process_task);
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Enroll worker
    //
    //  PARAMS :
    //      - INPUT :
    //         - Wc2Listener
    //         - Wc2Client
    //         - XmlMessageEnroll
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    public static void EnrollWorker(Object Wc2Listener, Object Wc2Client, WC2_Message XmlMessageEnroll)
    {
      WC2_Message wc2_request;
      WC2_Message wc2_response;
      SecureTcpClient client;
      SqlConnection sql_conn;
      SqlTransaction sql_trx;
      String str_response;
      Int64 wc2_message_id;
      Boolean error;
      Int64 response_session_id;
      Int32 response_terminal_id;

      wc2_request = XmlMessageEnroll;

      client = (SecureTcpClient)Wc2Client;

      sql_conn = null;
      sql_trx = null;
      error = true;
      wc2_response = null;

      try
      {
        wc2_response = WC2_Message.CreateMessage(wc2_request.MsgHeader.MsgType + 1);
        wc2_response.MsgHeader.PrepareReply(wc2_request.MsgHeader);

        response_session_id = wc2_request.MsgHeader.TerminalSessionId;
        response_terminal_id = client.InternalId;

        // Get Sql Connection 
        sql_conn = WGDB.Connection();

        // Begin Transaction
        sql_trx = sql_conn.BeginTransaction();

        switch (wc2_request.MsgHeader.MsgType)
        {
          case WC2_MsgTypes.WC2_MsgEnrollTerminal:
          {
            EnrollTerminal(client, wc2_request, wc2_response, sql_trx);
            response_terminal_id = client.InternalId;
            response_session_id = client.SessionId;
          }
          break;

          case WC2_MsgTypes.WC2_MsgUnenrollTerminal:
          {
            UnenrollTerminal(client, wc2_request, wc2_response, sql_trx);
          }
          break;

          default:
            throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_ERROR, "Unexpected MsgType");
          // Unreachable code detected
          // break;
        }

        str_response = wc2_response.ToXml();

        // Insert wc2 message and Update wc2 trx
        WC2_BusinessLogic.DB_InsertWC2Message(response_terminal_id, response_session_id, true, str_response, wc2_response.MsgHeader.SequenceId, out wc2_message_id, sql_trx);

        // Insert WC2 Trx
        if ( wc2_request.MsgHeader.MsgType != WC2_MsgTypes.WC2_MsgEnrollTerminal )
        {
          // Update wc2 trx
          WC2_BusinessLogic.DB_UpdateWC2Transaction(response_terminal_id, wc2_request.MsgHeader.SequenceId, true, wc2_message_id, sql_trx);
        }

        // End Transaction
        sql_trx.Commit();

        // Send Enroll response
        client.Send(str_response);
        NetLog.Add(NetEvent.MessageSent, client.Identity, str_response);

        error = false;
      }
      catch (WC2_Exception wc2_ex)
      {
        String wc2_error_response;
        wc2_error_response = null;
        try
        {
          if (wc2_response == null)
          {
            wc2_response = WC2_Message.CreateMessage(WC2_MsgTypes.WC2_MsgError);
          }
          wc2_response.MsgHeader.ResponseCode = wc2_ex.ResponseCode;
          wc2_response.MsgHeader.ResponseCodeText = wc2_ex.ResponseCodeText;

          // Create error response
          wc2_error_response = wc2_response.ToXml();

          client.Send(wc2_error_response);
          NetLog.Add(NetEvent.MessageSent, client.Identity, wc2_error_response);

        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      }
      catch (Exception ex)
      {
        NetLog.Add(NetEvent.MessageFormatError, client.Identity + " a.1)", ex.Message);
        NetLog.Add(NetEvent.MessageFormatError, client.Identity + " b.1)", XmlMessageEnroll.ToXml());

        Log.Exception(ex);

        return;
      }
      finally
      {
        if (sql_trx != null)
        {
          if (error && sql_trx.Connection != null)
          {
            sql_trx.Rollback();
          }

          sql_trx.Dispose();
          sql_trx = null;
        }

        // Close connection
        if (sql_conn != null)
        {
          sql_conn.Close();
          sql_conn = null;
        }

      } // try - finally

    } // EnrollWorker

    #region Private Functions

    //------------------------------------------------------------------------------
    // PURPOSE : Enroll Terminal
    //
    //  PARAMS :
    //      - INPUT :
    //         - Wc2Client
    //         - Wc2RequestMessage
    //         - Wc2ResponseMessage
    //         - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void EnrollTerminal(SecureTcpClient Wc2Client, WC2_Message Wc2RequestMessage, WC2_Message Wc2ResponseMessage, SqlTransaction SqlTrx)
    {
      Int32 terminal_id;
      Int64 session_id;
      Boolean enrolled_terminal;
      Currency _sas_account_denom;

      WC2_MsgEnrollTerminal request_enroll;
      WC2_MsgEnrollTerminalReply response_enroll;

      request_enroll = (WC2_MsgEnrollTerminal)Wc2RequestMessage.MsgContent;
      response_enroll = (WC2_MsgEnrollTerminalReply)Wc2ResponseMessage.MsgContent;

      Wc2Client.Identity = Wc2RequestMessage.MsgHeader.TerminalId;

      if ( ! WC2_BusinessLogic.DB_GetEnrollData (Wc2Client.Identity,
                                                 WC2_TerminalType.GamingTerminal,
                                                 out terminal_id,
                                                 out session_id,
                                                 out response_enroll.LastSequenceId,
                                                 out response_enroll.LastTransactionId,
                                                 out enrolled_terminal,
                                                 SqlTrx))
      {
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_TERMINAL_NOT_AUTHORIZED, "Terminal not authorized. Not found or blocked");
      }

      if ( enrolled_terminal )
      {
        Int32 _terminal_id;
        String _provider_id;
        String _name;
        TerminalStatus _status;
        String _config_svr_ext_id;

        Wc2Client.InternalId = terminal_id;
        Wc2Client.SessionId = session_id;
        Wc2ResponseMessage.MsgHeader.TerminalSessionId = Wc2Client.SessionId;

        Misc.GetTerminalInfo(Wc2Client.Identity, (Int32)WC2_TerminalType.GamingTerminal, out _terminal_id, out _provider_id, out _name, out _status, out _config_svr_ext_id, out _sas_account_denom, SqlTrx);

        Common.BatchUpdate.TerminalSession.AddSession(terminal_id,
                                                      session_id,
                                                      0,
                                                      0,
                                                      _provider_id,       // WC2 Protocol: ProviderId Not used
                                                      _name,              // WC2 Protocol: Name Not used
                                                      1);                 // WC2 Protocol: TerminalType Not used

      }
      else
      {
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_TERMINAL_NOT_AUTHORIZED, "Terminal not authorized. Max licensed terminals reached.");
      }
    } // EnrollTerminal

    //------------------------------------------------------------------------------
    // PURPOSE : Unenroll Terminal
    //
    //  PARAMS :
    //      - INPUT :
    //         - Wc2Client
    //         - Wc2RequestMessage
    //         - Wc2ResponseMessage
    //         - SqlTrx
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    //   NOTES :
    //
    private static void UnenrollTerminal(SecureTcpClient Wc2Client, WC2_Message Wc2RequestMessage, WC2_Message Wc2ResponseMessage, SqlTransaction SqlTrx)
    {
      Common.BatchUpdate.TerminalSession.CloseSession(Wc2Client.InternalId, Wc2RequestMessage.MsgHeader.TerminalSessionId, (int)WC2_SessionStatus.Closed);
      Wc2Client.InternalId = 0;
      Wc2Client.SessionId = 0;

    } // UnenrollTerminal

    #endregion Private Functions

  } // classs WC2_PendingEnrollsQueue
} // namespace WSI.WC2
