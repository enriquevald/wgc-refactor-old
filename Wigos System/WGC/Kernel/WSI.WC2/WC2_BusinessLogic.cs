using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using WSI.Common;
using System.Xml;
using System.Collections;

namespace WSI.WC2
{
  #region Enums

  public enum WC2_TerminalType
  { 
      IntermediateServer    = 0
    , GamingTerminal  = 1
  }

  public enum WC2_SessionStatus
  {
      Opened = 0
    , Closed = 1
    , Abandoned = 2
    , Timeout = 3
    , Disconnected = 4
  }

  #endregion Enums

  public class WC2_BusinessLogic
  {
    #region EnrollAndSession

    /// <summary>
    /// Get Database enroll data
    /// </summary>
    /// <param name="ExternalTerminalId"></param>
    /// <param name="Type"></param>
    /// <param name="TerminalId"></param>
    /// <param name="SessionId"></param>
    /// <param name="SequenceId"></param>
    /// <param name="TransactionId"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean DB_GetEnrollData(String ExternalTerminalId, 
                                           WC2_TerminalType Type,
                                           out Int32 TerminalId,
                                           out Int64 SessionId,
                                           out Int64 SequenceId,
                                           out Int64 TransactionId,
                                           out Boolean EnrolledTerminal, 
                                           SqlTransaction Trx)
    {
      SqlCommand _sql_cmd;
      SqlDataReader _sql_reader;
      SqlParameter p;
      String _cmd_txt;
      String _config_svr_ext_id;
      bool _exists;
      TerminalStatus _status;
      String _dummy_provider_id;
      String _dummy_name;
      Currency _sas_account_denom;

      // Initialize output parameters
      TerminalId = 0;
      SessionId = 0;
      SequenceId = 0;
      TransactionId = 0;
      EnrolledTerminal = true;

      if (ExternalTerminalId.Length > 40)
      {
        ExternalTerminalId = ExternalTerminalId.Substring(0, 40);
      }

      _exists = Misc.GetTerminalInfo(ExternalTerminalId, (int) Type, out TerminalId, out _dummy_provider_id, out _dummy_name, out _status, out _config_svr_ext_id, out _sas_account_denom, Trx);

      // Not found or blocked
      if (!_exists 
          || TerminalId == 0
          || _status != TerminalStatus.ACTIVE)
      {
        return false;
      }

      _sql_reader = null;
      _sql_cmd = null;

      try
      {
        //
        // Get MAX Last Sequence Id, Transaction Id
        //
        _cmd_txt = "";
        _cmd_txt += "SELECT   MAX (W2S_LAST_SEQUENCE_ID)    ";
        _cmd_txt += "       , MAX (W2S_LAST_TRANSACTION_ID) ";
        _cmd_txt += "  FROM   WC2_SESSIONS ";
        _cmd_txt += " WHERE   W2S_TERMINAL_ID = @pTerminalId";

        _sql_cmd = new SqlCommand(_cmd_txt);
        _sql_cmd.Connection = Trx.Connection;
        _sql_cmd.Transaction = Trx;

        _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;

        _sql_reader = _sql_cmd.ExecuteReader();
        if (_sql_reader.Read())
        {
          SequenceId = _sql_reader.IsDBNull(0) ? 0 : _sql_reader.GetInt64(0);
          TransactionId = _sql_reader.IsDBNull(1) ? 0 : _sql_reader.GetInt64(1);
        }

        _sql_reader.Close();
        _sql_reader.Dispose();
        _sql_reader = null;

        //
        // Insert New Session
        //
        _cmd_txt = "";
        _cmd_txt += "INSERT INTO WC2_SESSIONS ( W2S_TERMINAL_ID ";
        _cmd_txt += "                         , W2S_LAST_SEQUENCE_ID ";
        _cmd_txt += "                         , W2S_LAST_TRANSACTION_ID ";
        _cmd_txt += "                         , W2S_LAST_RCVD_MSG ";
        _cmd_txt += "                         , W2S_SERVER_NAME ";
        _cmd_txt += "                         )  ";
        _cmd_txt += "                  VALUES ( @pTerminalId ";
        _cmd_txt += "                         , @pSequenceId ";
        _cmd_txt += "                         , @pTransactionId ";
        _cmd_txt += "                         , GETDATE() ";
        _cmd_txt += "                         , @pServerName ";
        _cmd_txt += "                         ) ";
        _cmd_txt += "                     SET   @pSessionId = SCOPE_IDENTITY() ";

        _sql_cmd = new SqlCommand(_cmd_txt);
        _sql_cmd.Connection = Trx.Connection;
        _sql_cmd.Transaction = Trx;

        _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId;
        _sql_cmd.Parameters.Add("@pSequenceId", SqlDbType.BigInt).Value = SequenceId;
        _sql_cmd.Parameters.Add("@pTransactionId", SqlDbType.BigInt).Value = TransactionId;
        _sql_cmd.Parameters.Add("@pServerName", SqlDbType.NVarChar).Value = Environment.MachineName;
        p = _sql_cmd.Parameters.Add("@pSessionId", SqlDbType.BigInt);
        p.Direction = ParameterDirection.Output;

        if (_sql_cmd.ExecuteNonQuery() != 1)
        {
          return false;
        }

        SessionId = (Int64)p.Value;

        //
        // Abandoned sessions: Moved to BatchUpdate
        //

        return true;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        if (_sql_reader != null)
        {
          _sql_reader.Close();
          _sql_reader.Dispose();
        }
        if (_sql_cmd != null)
        {
          _sql_cmd.Dispose();
        }
      }

      return false;
    } // DB_GetEnrollData


    /// <summary>
    /// Set session disconnected in DB.
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SessionId"></param>
    /// <param name="Status"></param>
    /// <param name="Trx"></param>
    public static void DB_SessionDisconnected(Int32 TerminalId,
                                              Int64 SessionId,
                                              SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;
      Int32 num_rows_updated;

      // Avoid the update when one of those fields is zero.
      // - They were reset to zero in a previous message
      if (TerminalId == 0
          || SessionId == 0)
      {
        return;
      }


      sql_str = "UPDATE WC2_SESSIONS SET W2S_STATUS = @p1, " +
                                        "W2S_FINISHED = GETDATE() " +
                                  "WHERE W2S_TERMINAL_ID = @p2 " +
                                    "AND W2S_SESSION_ID = @p3 " +
                                    "AND W2S_STATUS = 0 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "W2S_STATUS").Value = WC2_SessionStatus.Disconnected;
      sql_command.Parameters.Add("@p2", SqlDbType.Int, 4, "W2S_TERMINAL_ID").Value = TerminalId;
      sql_command.Parameters.Add("@p3", SqlDbType.BigInt, 8, "W2S_SESSION_ID").Value = SessionId;

      num_rows_updated = sql_command.ExecuteNonQuery();

      if (num_rows_updated != 1)
      {
        Log.Warning("Session not updated to disconnected. TerminalId: " + TerminalId.ToString() + " SessionId: " + SessionId.ToString());
      }

    } // DB_SessionDisconnected

    /// <summary>
    /// Update timeout wc2 sessions
    /// </summary>
    /// <param name="Connection"></param>
    public static void DB_UpdateTimeoutWc2Sessions(SqlConnection Connection)
    {
      SqlTransaction sql_trx;
      SqlCommand sql_command;
      String sql_str;
      Int32 num_rows_updated;

      sql_trx = null;

      try
      {
        sql_trx = Connection.BeginTransaction();

        //
        // Update Timeout Sessions
        //
        sql_str = "UPDATE WC2_SESSIONS SET W2S_STATUS = @p1, " +
                                          "W2S_FINISHED = GETDATE() " +
                                    "WHERE DATEDIFF(MINUTE ,W2S_LAST_RCVD_MSG, GETDATE()) >= 5 " +
                                      "AND W2S_STATUS = 0 " +
                                      "AND W2S_FINISHED IS NULL ";

        
        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = sql_trx.Connection;
        sql_command.Transaction = sql_trx;

        sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "W2S_STATUS").Value = WC2_SessionStatus.Timeout;

        num_rows_updated = sql_command.ExecuteNonQuery();
        sql_trx.Commit();

      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        if (sql_trx != null)
        {
          if (sql_trx.Connection != null)
          {
            sql_trx.Rollback();
          }
        }
      }
    } // DB_UpdateTimeoutWc2Sessions

    #endregion EnrollAndSession

    #region WC2_MessagesInsertArray

    public class WC2_InsertWc2Message
    {
      public Int32 terminal_id;
      public Int64 session_id;
      public Boolean towards_to_terminal;
      public String msg_xml_text;
      public Int64 sequence_id;
      public Int64 out_message_id;
      public Boolean error;
      public string error_message;
    }

    public class class_InsertWc2Messages
    {
      public Int32 num_items;
      private ArrayList items = new ArrayList();  // WC2_InsertWc2Message

      public void InsertItem(WC2_InsertWc2Message Item)
      {
        items.Add(Item);
        num_items++;
      }

      public WC2_InsertWc2Message GetItem(Int32 IdxItem)
      {
        if (items.Count <= IdxItem)
        {
          return null;
        }

        return (WC2_InsertWc2Message)items[IdxItem];
      }
    }

    /// <summary>
    /// For row updated in DB_InsertWC2MessageArray function
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    static void da_Wc2MessageRowUpdated(object sender, SqlRowUpdatedEventArgs e)
    {
      if (e.Status == UpdateStatus.ErrorsOccurred)
      {
        e.Row.RowError = e.Errors.Message;
        e.Status = UpdateStatus.SkipCurrentRow;
        e.Row["W2M_MESSAGE_ID"] = 0;
      }
      else if (e.StatementType == StatementType.Insert)
      {
        e.Row["W2M_MESSAGE_ID"] = (Int64)e.Command.Parameters["@p6"].Value;
      }
    }

    /// <summary>
    /// Insert into wc2_message in array.
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SessionId"></param>
    /// <param name="TowardsToTerminal"></param>
    /// <param name="MsgXmlText"></param>
    /// <param name="SequenceId"></param>
    /// <param name="NumInserts"></param>
    /// <param name="MessageId"></param>
    /// <param name="Trx"></param>
    public static void DB_InsertWC2MessageArray(class_InsertWc2Messages InsertArray,
                                                SqlTransaction Trx)
    {
      DataTable data_table;
      SqlDataAdapter da;
      StringBuilder sb;
      SqlCommand cmd_select;
      SqlCommand cmd_insert;
      String sql_str;
      Int64 idx_insert; 
      DataRow message_row;
      SqlParameter p;
      WC2_InsertWc2Message insert_message;

      try
      {
        da = new SqlDataAdapter();
        data_table = new DataTable();

        sb = new StringBuilder();
        sb.AppendLine("SELECT * FROM WC2_MESSAGES");
        cmd_select = new SqlCommand();
        cmd_select.Connection = Trx.Connection;
        cmd_select.Transaction = Trx;
        cmd_select.CommandText = sb.ToString();
        da.SelectCommand = cmd_select;

        da.FillSchema(data_table, SchemaType.Source);

        data_table.Columns["W2M_MESSAGE_ID"].AutoIncrement = true;
        data_table.Columns["W2M_MESSAGE_ID"].AutoIncrementSeed = -1;
        data_table.Columns["W2M_MESSAGE_ID"].AutoIncrementStep = -1;
        data_table.Columns["W2M_MESSAGE_ID"].ReadOnly = false;
        data_table.Columns["W2M_DATETIME"].AllowDBNull = true;

        // test
        data_table.Columns["W2M_MESSAGE"].AllowDBNull = true;

        sql_str = "INSERT INTO WC2_MESSAGES (W2M_TERMINAL_ID, W2M_SESSION_ID, W2M_TOWARDS_TO_TERMINAL, W2M_MESSAGE, W2M_SEQUENCE_ID) " +
                  "VALUES (@p1, @p2, @p3, @p4, @p5)" +
                  "   SET   @p6 = SCOPE_IDENTITY()";

        sb = new StringBuilder();
        sb.AppendLine(sql_str);
        cmd_insert = new SqlCommand();
        cmd_insert.Connection = Trx.Connection;
        cmd_insert.Transaction = Trx;
        cmd_insert.CommandText = sb.ToString();
        da.InsertCommand = cmd_insert;

        cmd_insert.Parameters.Add("@p1", SqlDbType.Int, 4, "W2M_TERMINAL_ID");
        cmd_insert.Parameters.Add("@p2", SqlDbType.BigInt, 8, "W2M_SESSION_ID");
        cmd_insert.Parameters.Add("@p3", SqlDbType.Bit, 1, "W2M_TOWARDS_TO_TERMINAL");
        cmd_insert.Parameters.Add("@p4", SqlDbType.Xml, Int32.MaxValue, "W2M_MESSAGE");
        cmd_insert.Parameters.Add("@p5", SqlDbType.BigInt, 8, "W2M_SEQUENCE_ID");

        p = cmd_insert.Parameters.Add("@p6", SqlDbType.BigInt, 8, "W2M_MESSAGE_ID");
        p.Direction = ParameterDirection.Output;

        da.RowUpdated += new SqlRowUpdatedEventHandler (da_Wc2MessageRowUpdated);

        for (idx_insert = 0; idx_insert < InsertArray.num_items; idx_insert++)
        {
          insert_message = InsertArray.GetItem((Int32)idx_insert);

          message_row = data_table.NewRow();
          message_row["W2M_TERMINAL_ID"] = insert_message.terminal_id;
          message_row["W2M_SESSION_ID"] = insert_message.session_id;
          message_row["W2M_TOWARDS_TO_TERMINAL"] = insert_message.towards_to_terminal;
          message_row["W2M_MESSAGE"] = insert_message.msg_xml_text;
          message_row["W2M_SEQUENCE_ID"] = insert_message.sequence_id;
          data_table.Rows.Add(message_row);
        }

        da.UpdateBatchSize = 500;
        da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
        da.ContinueUpdateOnError = true;
        da.Update(data_table);

        for (idx_insert = 0; idx_insert < InsertArray.num_items; idx_insert++)
        {
          insert_message = InsertArray.GetItem((Int32)idx_insert);

          if (data_table.Rows[((Int32)(idx_insert))].HasErrors)
          {
            insert_message.out_message_id = 0;
            insert_message.error = true;
            insert_message.error_message = data_table.Rows[((Int32)(idx_insert))].RowError;
          }
          else
          {
            insert_message.out_message_id = (Int64)data_table.Rows[((Int32)(idx_insert))]["W2M_MESSAGE_ID"];
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);

        throw new Exception ("TEST Insert Array Failed.");
      }
    }

    #endregion WC2_MessagesInsertArray

    #region WC2_MessagesAndTrx

    /// <summary>
    /// Insert message in the database
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SessionId"></param>
    /// <param name="TowardsToTerminal"></param>
    /// <param name="MsgXmlText"></param>
    /// <param name="SequenceId"></param>
    /// <param name="Trx"></param>
    public static void DB_InsertWC2Message(Int32 TerminalId,
                                           Int64 SessionId,
                                           Boolean TowardsToTerminal,
                                           String MsgXmlText,
                                           Int64 SequenceId,
                                           out Int64 MessageId,
                                           SqlTransaction Trx)
    {
      SqlCommand sql_command;
      SqlParameter p;
      String sql_str;
      Int32 num_rows_inserted;

      sql_str = "INSERT INTO WC2_MESSAGES (W2M_TERMINAL_ID, W2M_SESSION_ID, W2M_TOWARDS_TO_TERMINAL, W2M_MESSAGE, W2M_SEQUENCE_ID) " +
                "VALUES (@p1, @p2, @p3, @p4, @p5) " +
                   "SET @p6 = SCOPE_IDENTITY()";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      if (TerminalId != 0)
      {
        sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "W2M_TERMINAL_ID").Value = TerminalId;
      }
      else
      {
        sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "W2M_TERMINAL_ID").Value = DBNull.Value;
      }

      if (SessionId != 0)
      {
        sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "W2M_SESSION_ID").Value = SessionId;
      }
      else
      {
        sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "W2M_SESSION_ID").Value = DBNull.Value;
      }

      sql_command.Parameters.Add("@p3", SqlDbType.Bit, 1, "W2M_TOWARDS_TO_TERMINAL").Value = TowardsToTerminal;
      sql_command.Parameters.Add("@p4", SqlDbType.Xml, Int32.MaxValue, "W2M_MESSAGE").Value = MsgXmlText;
      sql_command.Parameters.Add("@p5", SqlDbType.BigInt, 8, "W2M_SEQUENCE_ID").Value = SequenceId;

      p = sql_command.Parameters.Add("@p6", SqlDbType.BigInt, 8, "W2M_MESSAGE_ID");
      p.Direction = ParameterDirection.Output;

      try
      {
        num_rows_inserted = sql_command.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: DB_InsertWC2Message. TerminalId: " + TerminalId.ToString() + ", SessionId: " + SessionId.ToString() + ", SequenceId: " + SequenceId.ToString());

        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_ERROR, "DB Error inserting WC2 message.");
      }

      if (num_rows_inserted != 1)
      {
        Log.Warning("Exception throw: WC2 message not inserted.TerminalId: " + TerminalId.ToString() + " SessionId: " + SessionId.ToString() + " SeqId: " + SequenceId.ToString());
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_ERROR, "MESSAGE Not Inserted.");
      }

      MessageId = (Int64)p.Value;

    } // DB_InsertWC2Message

    /// <summary>
    /// Insert wc2 transaction
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SessionId"></param>
    /// <param name="SequenceId"></param>
    /// <param name="RequestedByTerminal"></param>
    /// <param name="TransactionId"></param>
    /// <param name="RequestMsgXmlText"></param>
    /// <param name="Trx"></param>
    public static void DB_InsertWC2Transaction(Int32 TerminalId,
                                               Int64 SessionId,
                                               Int64 SequenceId,
                                               Boolean RequestedByTerminal,
                                               Int64 TransactionId,
                                               Int64 RequestMsgId,
                                               SqlTransaction Trx)
    {
      SqlCommand  sql_command;
      String sql_str;
      Int32 num_rows_inserted;
      Int32 num_rows_updated;

      try
      {
        //
        // Insert into WC2_TRANSACTIONS table
        //
        sql_str = "INSERT INTO WC2_TRANSACTIONS (W2TX_TERMINAL_ID, " +
                                                "W2TX_SEQUENCE_ID, " +
                                                "W2TX_REQUESTED_BY_TERMINAL, " +
                                                "W2TX_SESSION_ID, " +
                                                "W2TX_STATUS, " +
                                                "W2TX_REQUEST_MSG_ID) " +
                                        "VALUES (@p1, " +
                                                "@p2, " +
                                                "@p3, " +
                                                "@p4, " +
                                                "0, " +   // Running
                                                "@p5)";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "W2TX_TERMINAL_ID").Value = TerminalId;
        sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "W2TX_SEQUENCE_ID").Value = SequenceId;
        sql_command.Parameters.Add("@p3", SqlDbType.Bit, 1, "W2TX_REQUESTED_BY_TERMINAL").Value = RequestedByTerminal;
        sql_command.Parameters.Add("@p4", SqlDbType.BigInt, 8, "W2TX_SESSION_ID").Value = SessionId;
        sql_command.Parameters.Add("@p5", SqlDbType.BigInt, 8, "W2TX_REQUEST_MSG_ID").Value = RequestMsgId;

        num_rows_inserted = sql_command.ExecuteNonQuery();

        if (num_rows_inserted != 1)
        {
          Log.Warning("Exception throw: inserting WC2 transaction.TerminalId: " + TerminalId.ToString() + " SessionId: " + SessionId.ToString() + " SeqId: " + SequenceId.ToString() + " RequestMsgId: " + RequestMsgId.ToString());
          throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_ERROR, "TRANSACTION Not Inserted.");
        }

        //
        // Update SequenceId WC2_SESSIONS table
        //
        if (TransactionId > 0)
        {
          sql_str = "UPDATE WC2_SESSIONS SET W2S_LAST_SEQUENCE_ID = dbo.Maximum_Bigint(W2S_LAST_SEQUENCE_ID, @p1), " +
                                            "W2S_LAST_TRANSACTION_ID = dbo.Maximum_Bigint(W2S_LAST_TRANSACTION_ID, @p2), " +
                                            "W2S_LAST_RCVD_MSG = dbo.Maximum_Datetime(W2S_LAST_RCVD_MSG, GETDATE()) " +
                                      "WHERE W2S_TERMINAL_ID = @p3 " +
                                        "AND W2S_SESSION_ID = @p4 " +
                                        "AND W2S_STATUS = 0";
        }
        else
        {
          sql_str = "UPDATE WC2_SESSIONS SET W2S_LAST_SEQUENCE_ID = dbo.Maximum_Bigint(W2S_LAST_SEQUENCE_ID, @p1), " +
                                            "W2S_LAST_RCVD_MSG = dbo.Maximum_Datetime(W2S_LAST_RCVD_MSG, GETDATE()) " +
                                      "WHERE W2S_TERMINAL_ID = @p3 " +
                                        "AND W2S_SESSION_ID = @p4 " +
                                        "AND W2S_STATUS = 0";
        }

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;

        sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "W2TX_SEQUENCE_ID").Value = SequenceId;

        if (TransactionId > 0)
        {
          sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "W2S_LAST_TRANSACTION_ID").Value = TransactionId;
        }

        sql_command.Parameters.Add("@p3", SqlDbType.Int, 4, "W2TX_TERMINAL_ID").Value = TerminalId;
        sql_command.Parameters.Add("@p4", SqlDbType.BigInt, 8, "W2TX_SESSION_ID").Value = SessionId;

        num_rows_updated = sql_command.ExecuteNonQuery();

        if (num_rows_updated != 1)
        {
          Log.Warning("Exception throw: updating WC2 session.TerminalId: " + TerminalId.ToString() + " SessionId: " + SessionId.ToString());
          throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_TERMINAL_SESSION_NOT_VALID, "SESSION Not Updated.");
        }
      }
      catch (WC2_Exception wc2_ex)
      {
        throw wc2_ex;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: inserting WC2 transaction.TerminalId: " + TerminalId.ToString() + " SessionId: " + SessionId.ToString() + " SeqId: " + SequenceId.ToString() + " TransactionId: " + TransactionId.ToString() + " RequestMsgId: " + RequestMsgId.ToString());
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_ERROR, "TRANSACTION Not Inserted.");
      }

    } // DB_InsertWC2Transaction

    /// <summary>
    /// Special Update Session for KeepAlive. Open and close DB conection.
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SessionId"></param>
    public static void DB_UpdateWc2SessionMessageReceived(Int32 TerminalId, Int64 SessionId, Int64 SequenceId, Int64 TransactionId)
    {

      if (!Common.BatchUpdate.TerminalSession.SessionMessageReceived(TerminalId, SessionId, SequenceId, TransactionId))
      {
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_TERMINAL_SESSION_NOT_VALID, "Session not opened.");
      }

    }

    /// <summary>
    /// Update trx, insert response
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SequenceId"></param>
    /// <param name="RequestedByTerminal"></param>
    /// <param name="ResponseMsgXmlText"></param>
    /// <param name="Trx"></param>
    public static void DB_UpdateWC2Transaction(Int32 TerminalId,
                                               Int64 SequenceId,
                                               Boolean RequestedByTerminal,
                                               Int64 ResponseMsgId,
                                               SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;
      Int32 num_rows_updated;

      sql_str = "UPDATE WC2_TRANSACTIONS SET W2TX_RESPONSE_MSG_ID = @p1, W2TX_STATUS = 1" + // Finished
                                     " WHERE W2TX_TERMINAL_ID = @p2 " +
                                        "AND W2TX_SEQUENCE_ID = @p3 " +
                                        "AND W2TX_REQUESTED_BY_TERMINAL = @p4 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "W2TX_RESPONSE_MSG_ID").Value = ResponseMsgId;
      sql_command.Parameters.Add("@p2", SqlDbType.Int, 4, "W2TX_TERMINAL_ID").Value = TerminalId;
      sql_command.Parameters.Add("@p3", SqlDbType.BigInt, 8, "W2TX_SEQUENCE_ID").Value = SequenceId;
      sql_command.Parameters.Add("@p4", SqlDbType.Bit, 1, "W2TX_REQUESTED_BY_TERMINAL").Value = RequestedByTerminal;

      try
      {
        num_rows_updated = sql_command.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: DB_UpdateWC2Transaction. TerminalId: " + TerminalId.ToString() + ", SequenceId: " + SequenceId.ToString());

        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_ERROR, "DB Error updating WC2 transaction.");
      }

      if (num_rows_updated != 1)
      {
        Log.Warning("Exception throw: updating WC2 transaction.TerminalId: " + TerminalId.ToString() + " SeqId: " + SequenceId.ToString());
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_ERROR, "WC2_TRANSACTION Not Updated.");
      }

    } // DB_UpdateWC2Transaction

    /// <summary>
    /// Check if this messange is a retry.
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SequenceId"></param>
    /// <param name="RequestedByTerminal"></param>
    /// <param name="MsgXmlText"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean DB_CheckMsgIsRetry(Int32 TerminalId,
                                             Int64 SequenceId,
                                             Boolean RequestedByTerminal,
                                             out String MsgXmlText,
                                             SqlTransaction Trx)
    {
      SqlCommand sql_command;
      SqlDataReader reader;
      String sql_str;

      MsgXmlText = null;

      sql_str = "SELECT W2M_MESSAGE " +
                  "FROM WC2_MESSAGES, WC2_TRANSACTIONS " +
                 "WHERE W2M_MESSAGE_ID = W2TX_RESPONSE_MSG_ID " +
                   "AND W2TX_TERMINAL_ID = @p1 " +
                   "AND W2TX_SEQUENCE_ID = @p2 " +
                   "AND W2TX_REQUESTED_BY_TERMINAL = @p3 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "W2TX_TERMINAL_ID").Value = TerminalId;
      sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "W2TX_SEQUENCE_ID").Value = SequenceId;
      sql_command.Parameters.Add("@p3", SqlDbType.Bit, 1, "W2TX_REQUESTED_BY_TERMINAL").Value = RequestedByTerminal;

      reader = null;

      try
      {
        reader = sql_command.ExecuteReader();
        if (reader.Read())
        {
          if (reader.IsDBNull(0))
          {
            MsgXmlText = null;
          }
          else
          {

            MsgXmlText = (String)reader[0];
          }

          return true;
        }

        return false;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        if (reader != null)
        {
          reader.Close();
          reader.Dispose();
          reader = null;
        }
      }

      return false;
    } // DB_CheckMsgIsRetry

    #endregion WC2_MessagesAndTrx

    #region WC2_Cards

    /// <summary>
    /// Checks if there are bingo cards stored in the DB,
    /// if not bingo cards are generated then stored to the DB
    /// </summary>
    public static void DB_WC2_CheckCards()
    {
      const int CARD_NUM_COLS = 5;
      const int CARD_NUM_ROWS = 5;
      const int NUMBERS_PER_COL = 15;

      SqlConnection conn;
      SqlTransaction  sql_trx;
      SqlCommand      sql_command;
      String          sql_str;
      CardManager     card_manager;

      conn = null;
      sql_trx = null;

      try
      {
        conn = WGDB.Connection();
        sql_trx = conn.BeginTransaction();

        if (CardManager.CheckDBCards(sql_trx))
        {
          return;
        }

        Log.Message("Generating cards...");
        Console.WriteLine("Generating cards...");  

        sql_str = "";
        sql_str = "DELETE C2_CARDS";
        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = sql_trx.Connection;
        sql_command.Transaction = sql_trx;

        sql_command.ExecuteNonQuery();
        card_manager = new CardManager(CARD_NUM_COLS, CARD_NUM_ROWS, NUMBERS_PER_COL);

        for (int count = 0; count < CardManager.MIN_CARDS_NEEDED; count++)
        {
          card_manager.AddCard();
        }

        card_manager.InsertCardsToDB(sql_trx);

        sql_trx.Commit();

        Log.Message("Cards generated.");
        Console.WriteLine("Cards generated.");  
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        if (sql_trx != null)
        {
          if (sql_trx.Connection != null)
          {
            sql_trx.Rollback();
          }
        }

        if (conn != null)
        {
          conn.Close();
          conn = null;
        }
      }
    } // DB_WC2_CheckCards

    /// <summary>
    /// Get C2 Cards
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="NumCards"></param>
    /// <param name="CardType"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static ArrayList DB_WC2_GetCards(Int32 TerminalId,
                                            Int32 NumCards,
                                            String CardType,
                                            SqlTransaction Trx)
    {
      String link_card_id;
      int idx_try;
      bool card_linked;
      SqlCommand _sql_command;
      String sql_str;
      Int32 num_rows_updated;
      WC2_Card card;
      ArrayList array_list;
      Random rnd;
      SqlDataAdapter da;
      DataTable dt_cards;
      Object _obj;

      array_list = new ArrayList();

      //
      // PRECONDITIONS: 
      //
      //     - Only 1 Card supported
      //     - Only CardType 'Card5x5' supported
      //
      if (NumCards != 1)
      {
        Log.Warning("Exception throw: NumCards not supported.TerminalId: " + TerminalId.ToString());
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_ERROR, "NumCards not supported.");
      }

      if (CardType != "Card5x5")
      {
        Log.Warning("Exception throw: CardType not supported.TerminalId: " + TerminalId.ToString());
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_INVALID_CARD_TYPE, "CardType not supported.");
      }

      //
      // Unlink the previous Cards
      //
      sql_str = "UPDATE C2_CARDS SET C2C_TERMINAL_ID = NULL " +
                                  "WHERE C2C_TERMINAL_ID = @p1 ";

      _sql_command = new SqlCommand(sql_str);
      _sql_command.Connection = Trx.Connection;
      _sql_command.Transaction = Trx;

      _sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "C2C_TERMINAL_ID").Value = TerminalId;
      num_rows_updated = _sql_command.ExecuteNonQuery();

      //
      // Get all unlinked cards 
      //
      dt_cards = null;
      da = null;
      _sql_command = null;
      link_card_id = "";

      try
      {
        dt_cards = new DataTable("C2_CARDS");
        da = new SqlDataAdapter();
        _sql_command = new SqlCommand();
        _sql_command.Connection = Trx.Connection;
        _sql_command.Transaction = Trx;
        da.SelectCommand = _sql_command;

        sql_str = "SELECT C2C_CARD_ID " +
                    "FROM C2_CARDS " +
                   "WHERE C2C_TERMINAL_ID IS NULL ";

        _sql_command.CommandText = sql_str;

        // Retrieve data
        da.Fill(dt_cards);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error Get all unlinked cards. TerminalId: " + TerminalId.ToString());
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_NO_CARDS_AVAILABLE, "Error Get all unlinked cards.");
      }
      finally
      {
        if (da != null)
        {
          da.SelectCommand = null;
          da = null;
        }

        if (_sql_command != null)
        {
          _sql_command.Connection = null;
          _sql_command = null;
        }
      }

      if (dt_cards.Rows.Count == 0)
      {
        Log.Warning("Exception throw: No Cards Available. TerminalId: " + TerminalId.ToString());
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_NO_CARDS_AVAILABLE, "No Cards Available.");
      }

      //
      // Link Cards
      //
      rnd = new Random();
      card_linked = false;
      for (idx_try = 0; idx_try < 3; idx_try++)
      {
        link_card_id = (String)dt_cards.Rows[rnd.Next(dt_cards.Rows.Count)]["C2C_CARD_ID"];

        sql_str = "UPDATE C2_CARDS SET C2C_TERMINAL_ID  = @p1 " +
                                    ", C2C_REQUEST_TIME = GETDATE() " +
                                    "WHERE C2C_CARD_ID     = @p2 " +
                                      "AND C2C_TERMINAL_ID IS NULL";

        //
        // Link the 1st free card
        //
        _sql_command = new SqlCommand(sql_str);
        _sql_command.Connection = Trx.Connection;
        _sql_command.Transaction = Trx;

        _sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "C2C_TERMINAL_ID").Value = TerminalId;
        _sql_command.Parameters.Add("@p2", SqlDbType.NVarChar, 20, "C2C_CARD_ID").Value = link_card_id;
        num_rows_updated = _sql_command.ExecuteNonQuery();

        if (num_rows_updated != 1)
        {
          continue;
        }
        else
        {
          card_linked = true;

          //
          // Read Card Data
          //
          sql_str = "SELECT C2C_CONTENTS " +
                      "FROM C2_CARDS " +
                     "WHERE C2C_CARD_ID = @p1 ";

          _sql_command = new SqlCommand(sql_str);
          _sql_command.Connection = Trx.Connection;
          _sql_command.Transaction = Trx;

          _sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 20, "C2C_CARD_ID").Value = link_card_id;

          try
          {
            _obj = _sql_command.ExecuteScalar();
            if (_obj == null)
            {
              Log.Warning("Exception throw: Error Assign CardNumbers. TerminalId: " + TerminalId.ToString());
              throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_NO_CARDS_AVAILABLE, "Error Assign CardNumbers.");
            }
            card = new WC2_Card((String)_obj);
            array_list.Add(card);
          }
          catch (Exception _ex)
          {
            Log.Warning("Exception throw: Error Assign CardNumbers. TerminalId: " + TerminalId.ToString());
            Log.Exception(_ex);
            throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_NO_CARDS_AVAILABLE, "Error Assign CardNumbers.");
          }

          // Only 1 card is supported
          break;
        }
      }

      if (!card_linked)
      {
        Log.Warning("Exception throw: No Cards Available. TerminalId: " + TerminalId.ToString());
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_NO_CARDS_AVAILABLE, "No Cards Available.");
      }

      return array_list;
    } // DB_WC2_GetCards

    #endregion WC2_Cards

    #region WC2_Jackpot

    /// <summary>
    /// Locks a table 
    /// </summary>
    /// <param name="tablename">Table to be locked</param>
    /// <param name="Trx">Param to lock the table</param>
    /// <returns>true: table is locked</returns>
    public static Boolean LockTable(String TableName, String id, SqlTransaction Trx)
    {
      String sql_str;
      Int32 num_rows_updated;
      SqlCommand sql_command;

      sql_command = new SqlCommand();

      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_str = "UPDATE "+ TableName +
                  " SET " + id + " = " + id;

      sql_command.CommandText = sql_str;

      // Retrieve data
      num_rows_updated = sql_command.ExecuteNonQuery();

      if (num_rows_updated == 0)
      {
        Log.Warning("Error Locking table " + TableName + "...");

        return false;
      }

      return true;
    }

    /// <summary>
    /// Gets all the data of the specified table 
    /// </summary>
    /// <param name="tablename"></param>
    /// <param name="Trx"></param>
    /// <param name="OrderBy"></param>
    /// <returns></returns>
    public static DataTable GetTable(String tablename, SqlTransaction Trx, String OrderBy)
    {
      String sql_str;
      SqlDataAdapter da;
      DataTable dt;
      SqlCommand sql_command;

      da = null;
      sql_command = null;

      try
      {
        da = new SqlDataAdapter();
        sql_command = new SqlCommand();
        dt = new DataTable(tablename);

        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;
        da.SelectCommand = sql_command;

        sql_str = "SELECT * " +
                    "FROM " + tablename;

        if (OrderBy.Length > 0)
        {
          sql_str += " ORDER BY " + OrderBy;
        }

        sql_command.CommandText = sql_str;

        // Retrieve data
        da.Fill(dt);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("JackpotContribution: Error reading " + tablename + "...");
        return null;
      }
      finally
      {
        if (da != null)
        {
          da.SelectCommand = null;
          da = null;
        }

        if (sql_command != null)
        {
          sql_command.Connection = null;
          sql_command = null;
        }
      }
      return dt;
    }

    /// <summary>
    /// Gets all the data of the specified table 
    /// </summary>
    /// <param name="tablename"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static DataTable GetTable(String tablename, SqlTransaction Trx)
    {
      return GetTable(tablename, Trx, "");
    }

    /// <summary>
    /// Every 
    /// </summary>
    /// <param name="JackpotBoundCredits"></param>
    /// <param name="Denomination"></param>
    /// <param name="Trx"></param>
    public static void DB_WC2_SetJackpotContribution(Decimal JackpotAmount,
                                                     SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String     sql_str;
      DataTable  dt_jackpot_parameters;
      DataTable  dt_jackpot_instances;
      Int32      num_rows_updated;
      Decimal    jackpot_contribution_amount;
      Decimal    individual_contribution;
      //////Decimal    individual_compensation;
      int        idx_jackpot;

      // Amount that will contribute to the jackpot
      jackpot_contribution_amount = JackpotAmount;

      //
      // Get Jackpot parameters from C2_JACKPOT_PARAMETERS
      //
      dt_jackpot_parameters = null;
      dt_jackpot_instances = null;
      sql_command = null;

      //
      // Get Jackpot parameters from C2_JACKPOT_PARAMETERS
      // 
      dt_jackpot_parameters = GetTable("C2_JACKPOT_PARAMETERS", Trx);
      if (dt_jackpot_parameters == null)
      {
        Log.Warning("JackpotContribution: Error getting jackpot parameters...");

        return;
      }
      // Check number of rows
      if (dt_jackpot_parameters.Rows.Count < 1)
      {
        Log.Warning("JackpotContribution: Error reading jackpot parameters...");

        return;
      }

      // Check that jackpot system is enabled
      if ((Boolean)dt_jackpot_parameters.Rows[0]["C2JP_ENABLED"] != true)
      {
        return;
      }      
      //
      // Get Jackpot instances from C2_JACKPOT_INSTANCES
      // 
      dt_jackpot_instances = GetTable("C2_JACKPOT_INSTANCES", Trx, "C2JI_INDEX");
      if (dt_jackpot_instances == null)
      {
        return;
      }
      
      for (idx_jackpot = 0; idx_jackpot < dt_jackpot_instances.Rows.Count; idx_jackpot++)
      {
        Decimal _contrib_lo;
        Decimal _contrib_hi;
        Decimal _compens_lo;
        Decimal _compens_hi;
        
        // Amount that contribute to the instance jackpot
        individual_contribution = jackpot_contribution_amount * (((Decimal)dt_jackpot_instances.Rows[idx_jackpot]["C2JI_CONTRIBUTION_PCT"]) / 100);

        ////////Get the compensation 
        //////individual_compensation = WC2_JackpotManager.IndividualCompensation(individual_contribution, idx_jackpot);

        // Low compensation
        _contrib_lo = individual_contribution;
        _compens_lo = 0.25m * _contrib_lo;
        _contrib_lo -= _compens_lo;

        // High compensation
        _contrib_hi = individual_contribution;
        _compens_hi = 0.75m * _contrib_hi;
        _contrib_hi -= _compens_hi;
        //
        // Update C2JC_TO_COMPENSATE & C2JC_ACCUMULATED in C2_JACKPOT_COUNTERS table.
        //
        sql_str = "UPDATE   C2_JACKPOT_COUNTERS " +
                     "SET   C2JC_TO_COMPENSATE  = C2JC_TO_COMPENSATE - CASE WHEN (C2JC_TO_COMPENSATE <= C2JC_ACCUMULATED) THEN @pCompensLo ELSE @pCompensHi END " +
                     "    , C2JC_ACCUMULATED    = C2JC_ACCUMULATED   + CASE WHEN (C2JC_TO_COMPENSATE <= C2JC_ACCUMULATED) THEN @pContribLo ELSE @pContribHi END  " +
                   "WHERE   C2JC_INDEX          = @pJckpIndex ";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection  = Trx.Connection;
        sql_command.Transaction = Trx;

        sql_command.Parameters.Add("@pCompensLo", SqlDbType.Decimal).Value = _compens_lo;
        sql_command.Parameters.Add("@pCompensHi", SqlDbType.Decimal).Value = _compens_hi;
        sql_command.Parameters.Add("@pContribLo", SqlDbType.Decimal).Value = _contrib_lo;
        sql_command.Parameters.Add("@pContribHi", SqlDbType.Decimal).Value = _contrib_hi;
        sql_command.Parameters.Add("@pJckpIndex", SqlDbType.Int).Value = (int)(dt_jackpot_instances.Rows[idx_jackpot]["C2JI_INDEX"]);

        num_rows_updated = sql_command.ExecuteNonQuery();

        if (num_rows_updated != 1)
        {
          Log.Warning("JackpotContribution: Error updating jackpot compensation...");
        }
      } // for (idx_jackpot)

      return;
    } // DB_WC2_SetJackpotContribution

    #endregion WC2_Jackpot

    #region WC2_Pattern
    /// <summary>
    /// Checks the 
    /// </summary>
    public static void DB_WC2_CheckPatterns()
    {
      SqlConnection conn;
      SqlTransaction sql_trx;
      SqlCommand sql_command;
      String sql_str;

      WC2_BingoPatterns BingoPatterns = new WC2_BingoPatterns();

      conn = null;
      sql_trx = null;

      try
      {
        conn = WGDB.Connection();
        sql_trx = conn.BeginTransaction();

        if (BingoPatterns.CheckDBPatterns(sql_trx, WC2_Patterns.NUM_PATTERNS))
        {
          return;
        }

        sql_str = "DELETE C2_WINNING_PATTERNS";
        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = sql_trx.Connection;
        sql_command.Transaction = sql_trx;

        sql_command.ExecuteNonQuery();

        BingoPatterns.GeneratePatterns();
        BingoPatterns.InsertPatternsToDB(sql_trx);

        sql_trx.Commit();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        if (sql_trx != null)
        {
          if (sql_trx.Connection != null)
          {
            sql_trx.Rollback();
          }
        }

        if (conn != null)
        {
          conn.Close();
          conn = null;
        }
      }
    } // DB_WC2_CheckPatterns



    /// <summary>
    /// Gets an array list of all patterns in the Database
    /// </summary>
    /// <returns></returns>
    public static ArrayList WC2_GetPatterns()
    {
      ArrayList pattern_list;
      SqlConnection sql_conn;
      SqlTransaction sql_trx;
      WC2_Pattern void_pattern;
      Boolean error = true;
      sql_conn = null;
      sql_trx = null;
      try
      {
        // Get Sql Connection 
        sql_conn = WGDB.Connection();
        sql_trx = sql_conn.BeginTransaction();
        pattern_list = new ArrayList();
        
        //Added a void pattern to prize 0
        void_pattern = new WC2_Pattern();
        void_pattern.Max = 0;
        void_pattern.Min = 0;
        void_pattern.NumPositions = 0;
        void_pattern.Order = 0;
        void_pattern.PositionList = "0";

        pattern_list.Add(void_pattern);
        pattern_list.AddRange(WC2_BusinessLogic.DB_WC2_GetPatterns(sql_trx));
        error = false;
      }
      finally
      {
        if (sql_trx != null)
        {
          if (error && sql_trx.Connection != null)
          {
            sql_trx.Rollback();
          }

          sql_trx.Dispose();
          sql_trx = null;
        }

        // Close connection
        if (sql_conn != null)
        {
          sql_conn.Close();
          sql_conn = null;
        }
      }
      return pattern_list;
    }

    /// <summary>
    /// Converts the PositionList to PositionListProtocol
    /// </summary>
    /// <param name="positions"></param>
    /// <returns></returns>
    private static String GetPositionsProtocol(Int64 positions)
    {
      String str_positions;
      String zeroes;
      String result;
      int num_zeroes;
      int idx_byte;
      int idx_positions;

      str_positions = Convert.ToString(positions, 2);
      num_zeroes = (8 - str_positions.Length % 8);
      zeroes = "";
      result = "";

      for (int i = 0; i < num_zeroes; i++)
      {
        zeroes += "0";
      }
      str_positions = str_positions.Insert(0, zeroes);

      for (idx_byte = 0; idx_byte < str_positions.Length; idx_byte += 8)
      {
        for (idx_positions = 7; idx_positions >= 0; idx_positions--)
        {
          result = result.Insert(result.Length, str_positions[idx_byte + idx_positions].ToString());
        }
      }

      return Convert.ToString(Convert.ToInt64(result, 2),16);
    }

    /// <summary>
    /// Get C2 Patterns
    /// </summary>
    /// <returns>Arraylist of patterns</returns>
    public static ArrayList DB_WC2_GetPatterns(SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;
      DataTable dt_patterns;
      SqlDataAdapter da;
      Int64 positions;

      WC2_Pattern pattern;
      ArrayList array_list;
      int idx_pattern;

      array_list = new ArrayList();

      dt_patterns = null;
      da = null;
      sql_command = null;

      try
      {
        dt_patterns = new DataTable("C2_WINNING_PATTERNS");
        da = new SqlDataAdapter();
        sql_command = new SqlCommand();
        sql_command.Connection = Trx.Connection;
        sql_command.Transaction = Trx;
        da.SelectCommand = sql_command;

        sql_str = "SELECT * FROM C2_WINNING_PATTERNS ORDER BY W2P_PATTERN_ID";

        sql_command.CommandText = sql_str;

        da.Fill(dt_patterns);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error Get all patterns.");
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_ERROR, "Error Getting all patterns.");
      }
      finally
      {
        if (da != null)
        {
          da.SelectCommand = null;
          da = null;
        }

        if (sql_command != null)
        {
          sql_command.Connection = null;
          sql_command = null;
        }
      }
       
      for (idx_pattern = 0; idx_pattern < dt_patterns.Rows.Count; idx_pattern++)
      {
        
        pattern = new WC2_Pattern();
        pattern.Order = (Int32)dt_patterns.Rows[idx_pattern]["W2P_PATTERN_ID"]; ;
        pattern.Max = (Int64)dt_patterns.Rows[idx_pattern]["W2P_MAX_PRIZE"];
        pattern.Min = (Int64)dt_patterns.Rows[idx_pattern]["W2P_MIN_PRIZE"];
        positions = (Int64)dt_patterns.Rows[idx_pattern]["W2P_POSITION_LIST"];
        pattern.NumPositions = Convert.ToString(positions, 2).Replace("0", "").Length;
        pattern.PositionList = Convert.ToString(positions, 16).ToUpper();
        pattern.PositionListProtocol = GetPositionsProtocol(positions);
        array_list.Add(pattern);
      }
      
      return array_list;
    } // DB_WC2_GetCards

    #endregion WC2_Pattern

    #region Draws

    /// <summary>
    /// Inserts Draw Information to Draw Audit DB Table
    /// </summary>
    /// <param name="DrawResult">The Draw data</param>
    /// <param name="DrawDateTime">Draw date time</param>
    /// <param name="DrawId">Out Param that returns the DrawId where the Draw data was inserted</param>
    /// <param name="SqlTrx">Sql transaction</param>
    public static void DB_InsertDrawAudit(WC2_Draw DrawResult,
                                          DateTime DrawDateTime,
                                          out Int64 DrawId,
                                          SqlTransaction SqlTrx)
    {
      SqlCommand sql_command;
      String sql_str;
      SqlParameter p;
      Int32 num_rows_inserted;

      DrawId = 0;

      sql_str = "INSERT INTO C2_DRAW_AUDIT (DA_DRAW_DATETIME, DA_DATETIME, DA_WINNING_NUMBERS) " +
                "VALUES (@p1, GETDATE(), @p2) " +
                "SET @p3 = SCOPE_IDENTITY()";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = SqlTrx.Connection;
      sql_command.Transaction = SqlTrx;

      sql_command.Parameters.Add("@p1", SqlDbType.DateTime, 8, "DA_DRAW_DATETIME").Value = DrawDateTime;
      sql_command.Parameters.Add("@p2", SqlDbType.Xml, Int32.MaxValue, "DA_WINNING_NUMBERS").Value = DrawResult.ToXml();
      p = sql_command.Parameters.Add("@p3", SqlDbType.BigInt, 8, "DA_DRAW_ID");
      p.Direction = ParameterDirection.Output;


      try
      {
        num_rows_inserted = sql_command.ExecuteNonQuery();
        if (num_rows_inserted != 1)
        {
          Log.Warning("Exception throw: DrawAudit Not inserted.");
          throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_ERROR, "DrawAudit Not Inserted.");
        }
        else
        {
          DrawId = (Int64)p.Value;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: DrawAudit Not inserted.");
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_ERROR, "DrawAudit Not Inserted.");
      }


    } // DB_InsertDrawAudit

    /// <summary>
    /// Inserts Play Data to Play Audit DB Table
    /// </summary>
    /// <param name="TerminalId">The terminal Id that belongs to the generated play</param>
    /// <param name="Play">The Play data</param>
    /// <param name="PlayReply">The Play reply data</param>
    /// <param name="DrawId">The Draw Id generated</param>
    /// <param name="SqlTrx">Sql transaction</param>
    public static void DB_InsertPlayAudit(Int32 TerminalId,
                                          WC2_MsgPlay Play,
                                          WC2_MsgPlayReply PlayReply,
                                          Int64 DrawId,
                                          out Int64 PlayId,
                                          SqlTransaction SqlTrx)
    {
      SqlCommand sql_command;
      String sql_str;
      SqlParameter p;
      Int32 num_rows_inserted;

      PlayId = 0;

      sql_str = "INSERT INTO C2_DRAW_AUDIT_PLAYS " +
                " ( DAP_DRAW_ID                  " +
                " , DAP_PLAY_DATETIME            " +
                " , DAP_GAME_ID                  " +
                " , DAP_GAME_NAME                " +
                " , DAP_TERMINAL_ID              " +
                " , DAP_TERMINAL_NAME            " +
                " , DAP_DENOMINATION             " +
                " , DAP_PLAYED_CREDITS           " +
                " , DAP_WON_CREDITS              " +
                " , DAP_JACKPOT_BOUND_CREDITS    " +
                " , DAP_PRIZE_LIST               " +
                " , DAP_CARDS )                  " +
                "VALUES (@p1, GETDATE(), @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @pCompensLo, @pCompensHi) " +
                "SET @p13 = SCOPE_IDENTITY()";


      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = SqlTrx.Connection;
      sql_command.Transaction = SqlTrx;

      sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "DAP_DRAW_ID").Value = DrawId;
      sql_command.Parameters.Add("@p3", SqlDbType.Int, 4, "DAP_GAME_ID").Value = WC2_BusinessLogic.DB_GetGameId(Play.GameId, SqlTrx);
      if (Play.GameId.Length > 50)
      {
        sql_command.Parameters.Add("@p4", SqlDbType.VarChar, 50, "DAP_GAME_NAME").Value = Play.GameId.Substring(0, 50);
      }
      else
      {
        sql_command.Parameters.Add("@p4", SqlDbType.VarChar, 50, "DAP_GAME_NAME").Value = Play.GameId;
      }
      sql_command.Parameters.Add("@p5", SqlDbType.Int, 4, "DAP_TERMINAL_ID").Value = TerminalId;
      sql_command.Parameters.Add("@p6", SqlDbType.VarChar, 50, "DAP_TERMINAL_NAME").Value = WC2_BusinessLogic.DB_GetTerminalName(TerminalId, SqlTrx);
      sql_command.Parameters.Add("@p7", SqlDbType.Money, 8, "DAP_DENOMINATION").Value = ((Decimal) Play.Denomination) / 100;
      sql_command.Parameters.Add("@p8", SqlDbType.BigInt, 8, "DAP_PLAYED_CREDITS").Value = Play.TotalPlayedCredits;
      sql_command.Parameters.Add("@p9", SqlDbType.BigInt, 8, "DAP_WON_CREDITS").Value = Play.WonCredits;
      sql_command.Parameters.Add("@p10", SqlDbType.BigInt, 8, "DAP_JACKPOT_BOUND_CREDITS").Value = Play.JackpotBoundCredits;
      sql_command.Parameters.Add("@pCompensLo", SqlDbType.Xml, Int32.MaxValue, "DAP_PRIZE_LIST").Value = PlayReply.GetPrizeList();
      sql_command.Parameters.Add("@pCompensHi", SqlDbType.Xml, Int32.MaxValue, "DAP_CARDS").Value = PlayReply.GetCardList();
      p = sql_command.Parameters.Add("@p13", SqlDbType.BigInt, 8, "DAP_PLAY_ID");
      p.Direction = ParameterDirection.Output;

      try
      {
        num_rows_inserted = sql_command.ExecuteNonQuery();

        if (num_rows_inserted != 1)
        {
          Log.Warning("Exception throw: DrawAuditPlays not inserted.TerminalId: " + TerminalId.ToString() + " TransactionId: " + Play.TransactionId.ToString());
          throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_ERROR, "DrawAuditPlay Not Inserted.");
        }
        else
        {
          PlayId = (Int64)p.Value;
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: DrawAuditPlays not inserted.TerminalId: " + TerminalId.ToString() + " TransactionId: " + Play.TransactionId.ToString());
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_ERROR, "DrawAuditPlay Not Inserted.");
      }

    } // DB_InsertPlayAudit


    #endregion Draws

    #region Miscellaneous

    /// <summary>
    /// Get terminal name
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <returns></returns>
    public static String DB_GetTerminalName(Int64 TerminalId, SqlTransaction SqlTrx)
    {
      SqlCommand _sql_command;
      String _sql_str;
      String _terminal_name;
      Object _obj;

      _terminal_name = "";

      _sql_str = "SELECT TE_NAME FROM TERMINALS WHERE TE_TERMINAL_ID = @p1";

      _sql_command             = new SqlCommand(_sql_str);
      _sql_command.Connection  = SqlTrx.Connection;
      _sql_command.Transaction = SqlTrx;

      _sql_command.Parameters.Add("@p1", SqlDbType.BigInt).Value = TerminalId;

      try
      {
        _obj = _sql_command.ExecuteScalar();
        if (_obj != null)
        {
          _terminal_name = (String)_obj;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return _terminal_name;
    } // DB_GetTerminalName


    /// <summary>
    /// InsertGame
    /// </summary>
    /// <param name="ExternalTerminalId"></param>
    /// <param name="Type"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static void InsertGame(String GameName, SqlTransaction Trx)
    {
      SqlCommand sql_command;
      String sql_str;
      // 
      // Get Terminal Id
      //
      sql_str = "INSERT INTO GAMES (GM_NAME)" +
                                        "VALUES (@p1);";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.NVarChar, 50, "GM_NAME").Value = GameName;
      try
      {
        sql_command.ExecuteNonQuery();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    }//InsertGame



    /// <summary>
    /// 
    /// </summary>
    /// <param name="ExternalTerminalId"></param>
    /// <param name="Type"></param>
    /// <param name="Trx"></param>
    /// <returns></returns>
    public static Boolean ExistsGame(String GameName, SqlTransaction Trx, out Int32 GameId )
    {
      SqlCommand sql_command;
      SqlDataReader reader;
      String sql_str;

      GameId = 0;
      reader = null;
      // 
      // Get Terminal Id
      //
      sql_str = "SELECT GM_GAME_ID FROM GAMES WHERE GM_NAME = @p1";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.VarChar, 50, "GM_NAME").Value = GameName;

      try
      {
        reader = sql_command.ExecuteReader();
        if (reader.HasRows)
        {
          if (reader.Read())
          {
            GameId = reader.GetInt32(0);

            return true;
          }
        }

        return false;
      }
      catch
      {
        return false;
      }
      finally
      {
        if (reader != null)
        {
          reader.Close();
          reader.Dispose();
        }
        reader = null;
      }
    }

    /// <summary>
    /// Returns the game id of a game name
    /// </summary>
    /// <param name="GameName"></param>
    /// <returns></returns>
    public static Int32 DB_GetGameId(String GameName, SqlTransaction SqlTrx)
    {
      Int32 return_game_id;

      return_game_id = 0;

      if (!ExistsGame(GameName, SqlTrx, out return_game_id))
      {
        InsertGame(GameName, SqlTrx);
        ExistsGame(GameName, SqlTrx, out return_game_id);
      }

      return return_game_id;
    } // DB_GetGameId


    public static void DB_GetSoftwareVersion(int TerminalId, out int ClientId, out int BuildId, out string TerminalType, SqlTransaction SqlTrx)
    {
      SqlCommand _sql_command;
      SqlDataReader _sql_reader;
      String _sql_str;
      Int16 _terminal_type;
      Object _obj;

      ClientId = 0;
      BuildId = 0;
      TerminalType = "";
      _terminal_type = (Int16)TerminalTypes.UNKNOWN;

      //
      // Get Terminal Type
      //
      _sql_str = "SELECT ISNULL (TE_TERMINAL_TYPE, -1) FROM TERMINALS WHERE TE_TERMINAL_ID = @p1 ";

      _sql_command = new SqlCommand(_sql_str);
      _sql_command.Connection = SqlTrx.Connection;
      _sql_command.Transaction = SqlTrx;

      _sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "TE_TERMINAL_ID").Value = TerminalId;

      try
      {
        _obj = _sql_command.ExecuteScalar();
        if (_obj != null)
        {
          _terminal_type = (Int16)_obj;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return;
      }

      if ( _terminal_type == (Int16) TerminalTypes.UNKNOWN )
      {
        _terminal_type = (Int16) TerminalTypes.WIN;
      }

      // Version for MOBILE_BANK terminal type: SAS HOST
      if ( _terminal_type == (Int16)TerminalTypes.MOBILE_BANK )
      {
        _terminal_type = (Int16)TerminalTypes.SAS_HOST;
      }

      if ( _terminal_type != (Int16) TerminalTypes.WIN 
        && _terminal_type != (Int16) TerminalTypes.SAS_HOST
        && _terminal_type != (Int16)TerminalTypes.SITE_JACKPOT
        && _terminal_type != (Int16)TerminalTypes.MOBILE_BANK_IMB
        && _terminal_type != (Int16)TerminalTypes.ISTATS
        && _terminal_type != (Int16)TerminalTypes.PROMOBOX)
      {
        Log.Error("Unexpected TerminalType: " + _terminal_type.ToString() + " for TerminalId: " + TerminalId.ToString());

        return;
      }

      _sql_str = "SELECT TOP (1) TSV_CLIENT_ID, TSV_BUILD_ID, TSV_TERMINAL_TYPE" 
                       +  " FROM TERMINAL_SOFTWARE_VERSIONS" 
                       + " WHERE TSV_TERMINAL_TYPE = @pTerminalType" 
                       + " ORDER BY TSV_INSERTION_DATE DESC" ;

      _sql_command = new SqlCommand(_sql_str);
      _sql_command.Connection = SqlTrx.Connection;
      _sql_command.Transaction = SqlTrx;

      _sql_command.Parameters.Add("@pTerminalType", SqlDbType.SmallInt, 2, "TSV_TERMINAL_TYPE").Value = _terminal_type;

      try
      {
        _sql_reader = _sql_command.ExecuteReader();
        
        if (_sql_reader.HasRows)
        {
          _sql_reader.Read();
          ClientId = _sql_reader.GetInt16(0);
          BuildId = _sql_reader.GetInt16(1);
          TerminalType = _sql_reader.GetInt16(2).ToString();
        }
        _sql_reader.Close();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
    } // DB_GetSoftwareVersion


    public static ArrayList DG_GetFtpLocations(SqlTransaction SqlTrx)
    {
      SqlCommand sql_command;
      String sql_str;
      ArrayList return_key_values;
      SqlDataReader sql_reader;

      return_key_values = new ArrayList();

      sql_str = " SELECT GP_KEY_VALUE " +
                "   FROM GENERAL_PARAMS " +
                "  WHERE GP_GROUP_KEY = 'Software.Download' " +
                "    AND (GP_SUBJECT_KEY  = 'Location1' "+
                "         OR GP_SUBJECT_KEY = 'Location2')";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = SqlTrx.Connection;
      sql_command.Transaction = SqlTrx;

      try
      {
        sql_reader = sql_command.ExecuteReader();
        while (sql_reader.Read())
        {
          return_key_values.Add(sql_reader.GetString(0));
        }
        sql_reader.Close();
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        return_key_values = null;
      }
      return return_key_values;
    } // DG_GetKeyValues

    #endregion Miscellaneous


    internal static bool DB_UpdateTerminalSoftwareVersion(int TerminalId, int ClientId, int BuildId, SqlTransaction Trx)
    {


      String sql_str;
      SqlCommand sql_command;
      Int32 num_rows_updated;

      //
      // Update Terminals
      //
      sql_str = "UPDATE TERMINALS SET TE_CLIENT_ID   = @p1 " +
                                  ",  TE_BUILD_ID    = @p2 " +
                               "WHERE TE_TERMINAL_ID = @p3 ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = Trx.Connection;
      sql_command.Transaction = Trx;

      sql_command.Parameters.Add("@p1", SqlDbType.SmallInt, 2, "TE_CLIENT_ID").Value = ClientId;
      sql_command.Parameters.Add("@p2", SqlDbType.SmallInt, 2, "TE_BUILD_ID").Value = BuildId;
      sql_command.Parameters.Add("@p3", SqlDbType.Int, 4, "TE_TERMINAL_ID").Value = TerminalId;

      try
      {
        num_rows_updated = sql_command.ExecuteNonQuery();
      }
      catch
      {
        return false;
      }
      return true;
      
    }
  } // class
} // namespace
