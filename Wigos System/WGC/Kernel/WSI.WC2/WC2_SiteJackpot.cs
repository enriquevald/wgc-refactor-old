//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_SiteJackpot.cs
// 
//   DESCRIPTION: Site Jackpot Manager
// 
//        AUTHOR: Andreu Julia Quiles
// 
// CREATION DATE: 01-DEC-2010
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 01-DEC-2010 AJQ    First release. 
// 10-APR-2012 MPO    Set the values "terminal name" and "winner name" in "GetSiteJackpotHistory"
//                    Added ShowWinnerName,ShowTerminalName in GetSiteJackpotParameters
// 15-JUL-2013 LEM    Modified JackpotAward to make automatic payment if (Handpays.JackpotPayoutMode == JACKPOT_PAYOUT_MODE.AUTOMATIC)  
// 14-MAR-2014 JMM    Modified DB_ReadActivePlayingSessions to read only sessions on SAS Host terminals
// 24-OCT-2014 OPC    Fixed bug #1557: When handpay is insert should be paid, when movement id it's != 0.
// 29-OCT-2014 JMV    Fixed bug WIG-1592: "SiteJackpotManager.Contribution" visible en el log WC2
// 10-FEB-2015 FJC    Fixed bug WIG-1942: HandPays Screen appear a duplicated HandPay .
// 23-APR-2015 ANM    Add week days contribution if user wants
// 27-AUG-2015 JMM    Fixed bug Bug 4053: On bonusing mode, Jackpot Viewer doesn't show the jackpot animation.
// 22-FEB-2016 ETP    Product Backlog Item 9751: TITO Jackpot de sala: Only registered accounts.
// 05-APR-2016 JML    Fixed bug 11371: The site jackpot was not generating correctly.
// 15-MAR-2017 ETP    WIGOS-254: Contribution has to be done in one single step.
// 27-MAR-2017 ETP    WIGOS-255: Awarding on Bonus notified to EGM.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using WSI.Common;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Collections;
using System.Threading;
using WSI.Common.Jackpot;

namespace WSI.WC2
{
  public struct SiteJackpot
  {
    public int index;
    public String name;
    public Decimal accumulated;
    public Decimal minimum_bet;
  }
  public struct SiteJackpotHistory
  {
    public int index;
    public String name;
    public Decimal amount;
    public DateTime awarded;
    public String player_name;
    public String terminal_name;
  }

  public static class WC2_SiteJackpotManager
  {
    private struct SiteJackpotDBParams
    {
      public int week_days;
      public int t0;
      public int t1;
      public Boolean exclude_promotions;
      public Decimal min_occupation;
      public Boolean exclude_anonymous;
      public Decimal to_compensate;
      public Decimal sum_minimum;
      public int jackpot_index;
      public Decimal accumulated;
      public Decimal minimum;
      public Decimal maximum;
      public Decimal average;
      public String jackpot_name;
      public Decimal min_bet;
    }

    private static System.Threading.Thread m_thread = null;
    private static System.Threading.ReaderWriterLock m_rw_lock = new System.Threading.ReaderWriterLock();

    private static System.Threading.Thread m_awarding_thread = null;

    private const int NUM_JACKPOTS = 3;
    private const int NUM_MESSAGES = 3;

    private const Int32 AUTOMATIC_PAYMENT_WAIT_INTERVAL = 5 * 1000; //5 seconds
    private const Int64 AUTOMATIC_PAYMENT_TOTAL_INTERVAL = 60 * 60 * 1000; //1 hour

    private static SiteJackpot[] m_site_jackpot = new SiteJackpot[NUM_JACKPOTS];
    private static Decimal[] m_jackpot1 = new Decimal[NUM_JACKPOTS];
    private static Decimal[] m_v = new Decimal[NUM_JACKPOTS];
    private static String[] m_msg = new String[NUM_MESSAGES];

    private static ArrayList m_history = new ArrayList();

    private static Boolean m_history_read = false;

    private static ReaderWriterLock m_site_jackpot_params_lock = new ReaderWriterLock();

    private static int m_animation_interval_01 = 15;
    private static int m_animation_interval_02 = 15;
    private static int m_recent_history_interval = 60;

    private static Boolean m_show_winner_name = false;
    private static Boolean m_show_terminal_name = false;

    //------------------------------------------------------------------------------
    // PURPOSE: Site Jackpot Values
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public static SiteJackpot[] Jackpot
    {
      get
      {
        SiteJackpot[] _jackpot;

        m_rw_lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
        _jackpot = (SiteJackpot[])m_site_jackpot.Clone();
        m_rw_lock.ReleaseReaderLock();

        return _jackpot;
      }
    } // Jackpot

    //------------------------------------------------------------------------------
    // PURPOSE: Site Jackpot Messages
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public static ArrayList Messages
    {
      get
      {
        ArrayList _messages;

        _messages = new ArrayList();

        m_rw_lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
        for (int _idx = 0; _idx < 3; _idx++)
        {
          if (m_msg[_idx] == null)
          {
            continue;
          }
          if (m_msg[_idx] == "")
          {
            continue;
          }
          _messages.Add(m_msg[_idx]);
        }
        m_rw_lock.ReleaseReaderLock();

        return _messages;
      }
    } // Messages

    //------------------------------------------------------------------------------
    // PURPOSE: Site Jackpot History
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    public static ArrayList History
    {
      get
      {
        ArrayList _history;

        _history = new ArrayList();
        m_rw_lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
        _history = (ArrayList)m_history.Clone();
        m_rw_lock.ReleaseReaderLock();

        return _history;
      }
    } // History

    public static Boolean IsOldMode { get; set; }

    //------------------------------------------------------------------------------
    // PURPOSE: Site Jackpot Thread
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //     
    private static void SiteJackpotThread()
    {
      int _tick_increment;
      int _tick_contribution;

      using (DB_TRX _db_trx = new DB_TRX())
      {
        WC2_SiteJackpotManager.IsOldMode = !new JackpotViewer().ExistJackpotViewersEnabledAndRelatedToViewer(_db_trx.SqlTransaction);
      }

      if (!WC2_SiteJackpotManager.IsOldMode)
      {
        JackpotsContribution(0);
      }
      else
      {
        Contribution(0);
      }

      _tick_contribution = Common.Misc.GetTickCount();
      _tick_increment = _tick_contribution;

      if (m_awarding_thread == null)
      {
        m_awarding_thread = new Thread(new System.Threading.ThreadStart(SiteJackpotAwardingThread));
        m_awarding_thread.Start();
      }

      while (true)
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {
          WC2_SiteJackpotManager.IsOldMode = !new JackpotViewer().ExistJackpotViewersEnabledAndRelatedToViewer(_db_trx.SqlTransaction);
        }

        Thread.Sleep(1000);

        if (Misc.GetElapsedTicks(_tick_contribution) >= 60 * 1000)
        {
          if (!WC2_SiteJackpotManager.IsOldMode)
          {
            JackpotsContribution(Misc.GetElapsedTicks(_tick_contribution));
          }
          else
          {
            Contribution(Misc.GetElapsedTicks(_tick_contribution));
          }

          _tick_contribution = Misc.GetTickCount();
        }

        if (Increment(Misc.GetElapsedTicks(_tick_increment)))
        {
          _tick_increment = Misc.GetTickCount();
        }

      }
    } // SiteJackpotThread

    private static void SiteJackpotInstances_CheckBonusConfirmed()
    {
      MultiPromos.AccountBalance _transferred;
      BonusTransferStatus _status;
      SiteJackpotDBParams _site_jackpot_db_params;
      CashierSessionInfo _session_info;
      int _db_terminal_id;
      String _db_provider_id;
      String _db_terminal_name;
      DateTime _handpay_date;
      Int32 _target_terminal_id;
      Int64 _target_account_id;
      ArrayList _history;
      Int64 _movement_id;
      AccountMovementsTable _account_mov_table;
      MultiPromos.AccountBalance _ini_balance;
      Boolean _show_on_notification_EGM;


      _show_on_notification_EGM = GeneralParam.GetBoolean("SiteJackpot", "Bonusing.ShowOnNotificationEGM", false);

      using (DB_TRX _db_trx = new DB_TRX())
      {
        //Get Jackpot params
        if (!DB_ReadSiteJackpotParameters(true, _db_trx.SqlTransaction, out _site_jackpot_db_params))
        {
          return;
        }

        if (!Bonusing.SiteJackpotReadBonusStatus(_site_jackpot_db_params.jackpot_index, _db_trx.SqlTransaction,
                                                 out _status, out _transferred, out _target_terminal_id, out _target_account_id))
        {
          //No bonus related to the Jackpot instance
          return;
        }




        if (_status == BonusTransferStatus.Pending
          || _status == BonusTransferStatus.NotifiedInDoubt
          || (_status == BonusTransferStatus.Notified && !_show_on_notification_EGM)
          || (_status == BonusTransferStatus.Confirmed && _show_on_notification_EGM))
        {
          //Bonus still in progress or bonus alredy finished (only if show on notification)
          return;
        }



        if ((_status != BonusTransferStatus.Confirmed && !(_status == BonusTransferStatus.Notified && _show_on_notification_EGM)))
        {
          //Bonus already finished.  Unbind from the Jackpot instance
          if (!Bonusing.BonusAwarded_UnlinkSiteJackpotInstance(_site_jackpot_db_params.jackpot_index, _db_trx.SqlTransaction))
          {
            return;
          }

          _db_trx.Commit();

          return;
        }

        //
        // Confirmed or Notified
        //
        if (!DB_ReadSiteTerminal(_db_trx.SqlTransaction, out _db_terminal_id, out _db_provider_id, out _db_terminal_name))
        {
          return;
        }

        if (!DB_UpdateJackpotParams(_site_jackpot_db_params, _transferred.TotalBalance, _db_trx.SqlTransaction))
        {
          return;
        }

        // Update "Accumulated"
        _site_jackpot_db_params.accumulated = _site_jackpot_db_params.accumulated - _transferred.TotalBalance + _site_jackpot_db_params.minimum;

        _session_info = Cashier.GetSystemCashierSessionInfo(GU_USER_TYPE.SYS_SYSTEM, _db_trx.SqlTransaction);

        if (!MultiPromos.Trx_UpdateAccountBalance(_target_account_id, MultiPromos.AccountBalance.Zero, out _ini_balance, _db_trx.SqlTransaction))
        {
          return;
        }

        _account_mov_table = new AccountMovementsTable(_session_info);

        if (!_account_mov_table.Add(0, _target_account_id, MovementType.Handpay,
                                    _ini_balance.TotalBalance, 0, _transferred.TotalBalance, _ini_balance.TotalBalance + _transferred.TotalBalance))
        {
          Log.Error("SiteJackpotInstancesAwarded: Error adding jackpot movement.");

          return;
        }
        if (!_account_mov_table.Save(_db_trx.SqlTransaction))
        {
          Log.Error("SiteJackpotInstancesAwarded: Error adding jackpot movement.");

          return;
        }

        _movement_id = _account_mov_table.LastSavedMovementId;

        // Insert Handpay
        if (!DB_InsertHandpay(_site_jackpot_db_params, _db_terminal_id, _db_provider_id, _db_terminal_name, _transferred.TotalBalance,
                              _target_terminal_id, _target_account_id, _movement_id, _db_trx.SqlTransaction, out _handpay_date))
        {
          return;
        }

        // New Accumulated Jackpot
        m_rw_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
        m_site_jackpot[_site_jackpot_db_params.jackpot_index - 1].accumulated = _site_jackpot_db_params.accumulated;
        m_jackpot1[_site_jackpot_db_params.jackpot_index - 1] = _site_jackpot_db_params.accumulated;
        m_rw_lock.ReleaseWriterLock();

        if (GetSiteJackpotHistory(out _history, _db_trx.SqlTransaction))
        {
          if (_history.Count == 0)  // Must be greater than 0, a Jackpot has been awarded just now!!
          {
            return;
          }
        }

        //Bonus already finished.  Unbind from the Jackpot instance
        if (!Bonusing.BonusAwarded_UnlinkSiteJackpotInstance(_site_jackpot_db_params.jackpot_index, _db_trx.SqlTransaction))
        {
          return;
        }

        // Update the jackpot history to notify to the jackpot viewer
        // New Accumulated Jackpot
        m_rw_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
        m_site_jackpot[_site_jackpot_db_params.jackpot_index - 1].accumulated = _site_jackpot_db_params.accumulated;
        m_jackpot1[_site_jackpot_db_params.jackpot_index - 1] = _site_jackpot_db_params.accumulated;
        // History
        m_history = _history;
        m_history_read = true;
        m_rw_lock.ReleaseWriterLock();

        _db_trx.Commit();
      }

      return;
    } // SiteJackpotInstancesAwarded

    //------------------------------------------------------------------------------
    // PURPOSE: Site Jackpot Awarding Thread
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //     
    private static void SiteJackpotAwardingThread()
    {
      HANDPAY_PAYMENT_MODE _jackpot_mode;
      int _min_wait_seconds;
      int _max_wait_seconds;
      int _tick_award;
      int _random_interval;
      bool _jackpot_in_progress;
      Int32 _first_time;
      int _unexpected_jackpot_mode_tick;
      bool _unexpected_jackpot_mode;

      _first_time = 0;
      _first_time = _first_time + 0;

      _tick_award = Misc.GetTickCount();
      _random_interval = 60000;

      _unexpected_jackpot_mode_tick = Common.Misc.GetTickCount() - 15 * 60 * 1000; // To force write the 1st time


      while (true)
      {
        Thread.Sleep(1000);

        //Check if there is any jackpot in progress
        _jackpot_mode = Handpays.SiteJackpotPaymentMode;
        _jackpot_in_progress = false;
        _unexpected_jackpot_mode = false;

        switch (_jackpot_mode)
        {
          case HANDPAY_PAYMENT_MODE.MANUAL:
          case HANDPAY_PAYMENT_MODE.AUTOMATIC:
            break;

          case HANDPAY_PAYMENT_MODE.BONUSING:
            // This code avoids awarding the 3 instances at the same time through bonusing
            _jackpot_in_progress = Bonusing.AnySiteJackpotBonusPending();
            break;

          default:
            if (Misc.GetElapsedTicks(_unexpected_jackpot_mode_tick) >= 15 * 60 * 1000)
            {
              Log.Message(String.Format("SiteJackpot.SiteJackpotAwardingThread unexpected value for JackpotMode parameter ({0})!", _jackpot_mode));
              _unexpected_jackpot_mode_tick = Misc.GetTickCount();
            }
            _unexpected_jackpot_mode = true;

            break;
        }

        if (_unexpected_jackpot_mode)
        {
          continue;
        }

        if (!_jackpot_in_progress)
        {
          if (Common.Misc.GetElapsedTicks(_tick_award) >= _random_interval)
          {
            Boolean _awarded;

            JackpotAward(out _awarded);

            _tick_award = Common.Misc.GetTickCount();

            _min_wait_seconds = 30;
            _max_wait_seconds = 60;
            if (_awarded)
            {
              _min_wait_seconds = 60;
              _max_wait_seconds = 120;
            }
            _random_interval = 1000 * (_min_wait_seconds + (int)CERTIFIED_RNG.GetRandomNumber(_max_wait_seconds - _min_wait_seconds));
          }
        }

        //Clear the awarded bonuses from the Jackpot instances
        if (_jackpot_mode == HANDPAY_PAYMENT_MODE.BONUSING)
        {
          SiteJackpotInstances_CheckBonusConfirmed();
        }

      } // while (true)

    } // SiteJackpotAwardingThread

    //------------------------------------------------------------------------------
    // PURPOSE: Increment the Jackpots
    //
    //  PARAMS:
    //      - INPUT:
    //          - Milliseconds: Elapsed time since last increment
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private static Boolean Increment(long Milliseconds)
    {
      Decimal _seconds;
      Decimal _jackpot0;

      _seconds = (Decimal)Milliseconds / 1000m;

      try
      {
        m_rw_lock.AcquireWriterLock(1000);

        try
        {
          for (int _idx = 0; _idx < NUM_JACKPOTS; _idx++)
          {
            // Increment Jackpot            
            if (GeneralParam.GetBoolean("SiteJackpot", "Buffering.Enabled", true))
            {
              _jackpot0 = m_site_jackpot[_idx].accumulated;
              _jackpot0 += m_v[_idx] * _seconds; // j = j0 + v x t; t = _seconds;
              _jackpot0 = Math.Min(_jackpot0, m_jackpot1[_idx]);
            }
            else
            {
              _jackpot0 = m_jackpot1[_idx];
            }
            m_site_jackpot[_idx].accumulated = _jackpot0;
          } // for ( _idx )
        }
        finally
        {
          m_rw_lock.ReleaseWriterLock();
        }

        return true;
      }
      catch (ApplicationException)
      {
        // Do nothing, lock timeout
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }

      return false;

    } // Increment

    //------------------------------------------------------------------------------
    // PURPOSE: Set New Jackpots
    //
    //  PARAMS:
    //      - INPUT:
    //          - Milliseconds: Elapsed time since last increment
    //          - NewJackpot: New Jackpot Values
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //
    private static void SetNewValues(long Milliseconds, SiteJackpot[] Jackpot, String[] Messages)
    {
      Decimal _t;
      Decimal _jackpot0;

      // Period
      _t = (Decimal)Milliseconds / 1000m;

      try
      {
        m_rw_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);

        for (int _idx = 0; _idx < NUM_JACKPOTS; _idx++)
        {
          _jackpot0 = m_site_jackpot[_idx].accumulated;

          m_site_jackpot[_idx] = Jackpot[_idx];

          // New Jackpot value
          m_jackpot1[_idx] = m_site_jackpot[_idx].accumulated;
          if (_jackpot0 == 0 || m_jackpot1[_idx] < _jackpot0)
          {
            // First time
            _jackpot0 = m_site_jackpot[_idx].accumulated;
            m_v[_idx] = 0;
          }

          // Compute New Acceleration
          _jackpot0 = Math.Min(_jackpot0, m_jackpot1[_idx]);

          // Visible Jackpot Amount
          m_site_jackpot[_idx].accumulated = _jackpot0;

          Decimal _v;
          _v = (m_jackpot1[_idx] - _jackpot0) / 60;
          m_v[_idx] = (99 * m_v[_idx] + _v) / 100;
        }

        for (int _idx = 0; _idx < NUM_MESSAGES; _idx++)
        {
          m_msg[_idx] = Messages[_idx].Trim();
        }
      }
      finally
      {
        m_rw_lock.ReleaseWriterLock();
      }
    } // SetNewValues

    private static Boolean GetSiteJackpotHistory(out ArrayList History, SqlTransaction SqlTrx)
    {
      SiteJackpotHistory _sjh;
      StringBuilder _sql_sb;

      History = new ArrayList();
      _sql_sb = new StringBuilder();

      try
      {
        _sql_sb.AppendLine(" SELECT DISTINCT ");
        _sql_sb.AppendLine("    HP_DATETIME ");
        _sql_sb.AppendLine("  , HP_SITE_JACKPOT_INDEX ");
        _sql_sb.AppendLine("  , ISNULL(HP_SITE_JACKPOT_NAME, '') ");
        _sql_sb.AppendLine("  , HP_AMOUNT ");
        _sql_sb.AppendLine("  , ISNULL(AC_HOLDER_NAME, '') ");
        _sql_sb.AppendLine("  , ISNULL(TE_NAME, '') ");
        _sql_sb.AppendLine("  , ISNULL(TE_FLOOR_ID, '') ");
        _sql_sb.AppendLine(" FROM ");
        _sql_sb.AppendLine("  ( ");
        _sql_sb.AppendLine("     SELECT TOP 3 HP_DATETIME, HP_SITE_JACKPOT_INDEX, HP_SITE_JACKPOT_NAME, HP_AMOUNT, HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID, HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID FROM HANDPAYS WHERE  HP_TYPE = 20                                ORDER BY 1 DESC ");
        _sql_sb.AppendLine("     UNION ");
        _sql_sb.AppendLine("     SELECT TOP 1 HP_DATETIME, HP_SITE_JACKPOT_INDEX, HP_SITE_JACKPOT_NAME, HP_AMOUNT, HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID, HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID FROM HANDPAYS WHERE  HP_TYPE = 20 AND  HP_SITE_JACKPOT_INDEX = 1 ORDER BY 1 DESC ");
        _sql_sb.AppendLine("     UNION ");
        _sql_sb.AppendLine("     SELECT TOP 1 HP_DATETIME, HP_SITE_JACKPOT_INDEX, HP_SITE_JACKPOT_NAME, HP_AMOUNT, HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID, HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID FROM HANDPAYS WHERE  HP_TYPE = 20 AND  HP_SITE_JACKPOT_INDEX = 2 ORDER BY 1 DESC ");
        _sql_sb.AppendLine("     UNION ");
        _sql_sb.AppendLine("     SELECT TOP 1 HP_DATETIME, HP_SITE_JACKPOT_INDEX, HP_SITE_JACKPOT_NAME, HP_AMOUNT, HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID, HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID FROM HANDPAYS WHERE  HP_TYPE = 20 AND  HP_SITE_JACKPOT_INDEX = 3 ORDER BY 1 DESC ");
        _sql_sb.AppendLine("  ) X ");
        _sql_sb.AppendLine("  INNER JOIN TERMINALS ON TE_TERMINAL_ID = X.HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ");
        _sql_sb.AppendLine("  INNER JOIN ACCOUNTS  ON AC_ACCOUNT_ID  = X.HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID ");
        _sql_sb.AppendLine("  ORDER BY 1 DESC ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sql_sb.ToString(), SqlTrx.Connection, SqlTrx))
        {
          using (SqlDataReader _sql_reader = _sql_cmd.ExecuteReader())
          {
            while (_sql_reader.Read())
            {
              String _floor_id;

              _sjh = new SiteJackpotHistory();

              _sjh.awarded = _sql_reader.GetDateTime(0);
              _sjh.index = _sql_reader.GetInt32(1);
              _sjh.name = _sql_reader.GetString(2);
              _sjh.amount = _sql_reader.GetDecimal(3);
              _sjh.player_name = _sql_reader.GetString(4).Trim();
              _sjh.terminal_name = _sql_reader.GetString(5).Trim();

              _floor_id = _sql_reader.GetString(6).Trim();
              if (!String.IsNullOrEmpty(_floor_id))
              {
                // Prefix the terminal name with the FloorId
                _sjh.terminal_name = _floor_id + " " + _sjh.terminal_name;
              }

              History.Add(_sjh);
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE: Contribution & Awarding process
    //          The awarding is based on the current contribution
    //
    //  PARAMS:
    //      - INPUT:
    //          - Milliseconds: Elapsed time since last contribution
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //     
    private static void Contribution(long Milliseconds)
    {
      // Read "Played"
      string _sql_txt;
      Decimal _pending_contribution;
      Decimal _total_contribution;
      Decimal _total_compensated;
      Decimal _to_compensate;
      Decimal _compens_pct;
      Decimal _total_contributed;

      Decimal _used_played;
      SqlTransaction _sql_trx;
      SqlConnection _sql_con;
      SqlCommand _sql_cmd;

      Decimal _db_contrib_pct;
      Decimal _db_to_compensate;
      Decimal _db_sum_minimum;
      Decimal _db_sum_average;
      Boolean _db_only_redeemable;
      Decimal _db_played;
      Decimal _avg_played;
      int _db_avg_int_hours;
      int _db_num_play_sessions;
      Boolean _db_exceed_maximum_allowed;
      Boolean _too_much_to_compensate;
      SiteJackpot[] _new_values = new SiteJackpot[3];

      String[] _new_msg = new String[NUM_MESSAGES];

      Decimal _seconds;
      Boolean _awarded;

      DayOfWeek _working_day;

      _sql_con = null;
      _sql_trx = null;
      _sql_cmd = null;

      _to_compensate = 0;
      _total_compensated = 0;
      _seconds = (Decimal)(Milliseconds / 1000);

      try
      {
        using (DB_TRX _trx = new DB_TRX())
        {
          _sql_trx = _trx.SqlTransaction;
          if (_sql_trx == null)
          {
            return;
          }
          _sql_con = _sql_trx.Connection;
          if (_sql_con == null)
          {
            return;
          }

          _sql_txt = "";
          _sql_txt += "SELECT   SJP_CONTRIBUTION_PCT       ";
          _sql_txt += "       , SJP_TO_COMPENSATE          ";
          _sql_txt += "       , (SELECT SUM (SJI_MINIMUM) FROM SITE_JACKPOT_INSTANCES) SJP_SUM_MINIMUM";
          _sql_txt += "       , (SELECT SUM (SJI_AVERAGE) FROM SITE_JACKPOT_INSTANCES) SJP_SUM_AVERAGE";
          _sql_txt += "       , SJP_ONLY_REDEEMABLE        ";
          _sql_txt += "       , SJP_PLAYED                 ";
          _sql_txt += "       , SJP_AVERAGE_INTERVAL_HOURS ";
          _sql_txt += "       , SJP_PROMO_MESSAGE1         ";
          _sql_txt += "       , SJP_PROMO_MESSAGE2         ";
          _sql_txt += "       , SJP_PROMO_MESSAGE3         ";
          _sql_txt += "       , SJP_ANIMATION_INTERVAL     ";
          _sql_txt += "       , SJP_ANIMATION_INTERVAL2    ";
          _sql_txt += "       , SJP_RECENT_INTERVAL        ";
          _sql_txt += "       , SJP_SHOW_WINNER_NAME       ";
          _sql_txt += "       , SJP_SHOW_TERMINAL_NAME     ";
          _sql_txt += "       , SJP_EXCEED_MAXIMUM_ALLOWED ";
          // Number of "Opened" PlaySessions
          _sql_txt += "      , ( SELECT COUNT(1)           ";
          _sql_txt += "             FROM  ( SELECT DISTINCT PS_TERMINAL_ID FROM PLAY_SESSIONS WITH (INDEX (IX_PS_STATUS))";
          _sql_txt += "             INNER JOIN TERMINAL_GROUPS ON TG_TERMINAL_ID = PS_TERMINAL_ID AND TG_ELEMENT_ID = 1  ";
          _sql_txt += "             AND   TG_ELEMENT_TYPE = 4 WHERE PS_STATUS = 0                                        ";
          _sql_txt += "             UNION SELECT DISTINCT PS_TERMINAL_ID FROM PLAY_SESSIONS WITH (INDEX (IX_PS_FINISHED_STATUS))";
          _sql_txt += "             INNER JOIN TERMINAL_GROUPS ON TG_TERMINAL_ID = PS_TERMINAL_ID AND TG_ELEMENT_ID = 1  ";
          _sql_txt += "             AND   TG_ELEMENT_TYPE = 4 WHERE PS_FINISHED >= DATEADD (MINUTE, -10, GETDATE())      ";
          _sql_txt += "             AND   PS_STATUS <> 0 ) PLAYING ) PLAYING ";
          _sql_txt += "       , SJP_CONTRIBUTION_FIX       ";
          _sql_txt += "       , SJP_CONTRIBUTION_PCT0      ";
          _sql_txt += "       , SJP_CONTRIBUTION_PCT1      ";
          _sql_txt += "       , SJP_CONTRIBUTION_PCT2      ";
          _sql_txt += "       , SJP_CONTRIBUTION_PCT3      ";
          _sql_txt += "       , SJP_CONTRIBUTION_PCT4      ";
          _sql_txt += "       , SJP_CONTRIBUTION_PCT5      ";
          _sql_txt += "       , SJP_CONTRIBUTION_PCT6      ";
          _sql_txt += "  FROM   SITE_JACKPOT_PARAMETERS    ";
          _sql_txt += " WHERE   SJP_ENABLED = 1            ";


          _sql_cmd = new SqlCommand(_sql_txt, _sql_con, _sql_trx);
          using (SqlDataReader _sql_red = _sql_cmd.ExecuteReader())
          {
            if (!_sql_red.Read())
            {
              return;
            }
            //CONTRIBUTION value
            if (_sql_red.GetBoolean(17))   // SJP_CONTRIBUTION_FIX
            {
              _db_contrib_pct = _sql_red.GetDecimal(0);   // SJP_CONTRIBUTION_PCT
            }
            else
            {
              //Determine the right working day, depending on closing time
              _working_day = Misc.Opening(WGDB.Now).DayOfWeek;
              _db_contrib_pct = _sql_red.GetDecimal(18 + (Int32)_working_day); // SJP_CONTRIBUTION_PCTx (0->6)
            }

            _db_to_compensate = _sql_red.GetDecimal(1);   // SJP_TO_COMPENSATE
            _db_sum_minimum = _sql_red.GetDecimal(2);   // SJP_SUM_MINIMUM
            _db_sum_average = _sql_red.GetDecimal(3);   // SJP_SUM_AVERAGE
            _db_only_redeemable = _sql_red.GetBoolean(4);   // SJP_ONLY_REDEEMABLE             
            _db_played = _sql_red.GetDecimal(5);   // SJP_PLAYED
            if (_db_played < 0)
            {
              Log.Warning("SJP_PLAYED less than 0");

              _db_played = 0;
            }
            _db_avg_int_hours = _sql_red.GetInt32(6);     // SJP_AVERAGE_INTERVAL_HOURS

            _new_msg[0] = _sql_red.IsDBNull(7) ? "" : _sql_red.GetString(7); // SJP_PROMO_MESSAGE1
            _new_msg[1] = _sql_red.IsDBNull(8) ? "" : _sql_red.GetString(8); // SJP_PROMO_MESSAGE2
            _new_msg[2] = _sql_red.IsDBNull(9) ? "" : _sql_red.GetString(9); // SJP_PROMO_MESSAGE3

            m_site_jackpot_params_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
            try
            {
              m_animation_interval_01 = _sql_red.GetInt32(10);     // SJP_ANIMATION_INTERVAL
              m_animation_interval_02 = _sql_red.GetInt32(11);     // SJP_ANIMATION_INTERVAL2
              m_recent_history_interval = _sql_red.GetInt32(12);     // SJP_RECENT_INTERVAL
              m_show_winner_name = _sql_red.GetBoolean(13);           // SJP_SHOW_WINNER_NAME   
              m_show_terminal_name = _sql_red.GetBoolean(14);         // SJP_SHOW_TERMINAL_NAME 
            }
            finally
            {
              m_site_jackpot_params_lock.ReleaseWriterLock();
            }


            _db_exceed_maximum_allowed = _sql_red.GetBoolean(15);
            // Number of PlaySessions (Openned + "Recently Closed")
            _db_num_play_sessions = _sql_red.GetInt32(16);

          }

          // Avoid too many jackpots awarded (protection against faulty random generator)
          _too_much_to_compensate = (_db_to_compensate >= _db_sum_minimum);

          _db_avg_int_hours = Math.Max(0, _db_avg_int_hours);
          if (_db_avg_int_hours == 0)
          {
            _db_avg_int_hours = 8;
          }

          if (GeneralParam.GetBoolean("SiteJackpot", "Buffering.Enabled", true))
          {
            _avg_played = _db_played / (_db_avg_int_hours * 3600m); // 1 day -> 24h --> AvgPlayed per Second
            _used_played = Math.Round(_avg_played * Milliseconds / 1000.0m, 2, MidpointRounding.AwayFromZero);
            _used_played = Math.Max(_used_played, 1.0m);        // At least $1
            _used_played = Math.Min(_used_played, _db_played);
            _used_played = Math.Max(_used_played, 0);           // Ensure > 0; the previus maximum is necessary to ensure that the used_played is at least 1 and less than the 'db_played'
          }
          else
          {
            _used_played = _db_played;
          }


          // AJQ 06-JUN-2014, Don't increment the Jackpot when nobody is playing ...
          if (_db_num_play_sessions <= 0)
          {
            // No active play sessions
            _used_played = 0;
          }

          // Total Contribution
          _total_contribution = _used_played * _db_contrib_pct / 100.0m;

          // Compensation 
          _compens_pct = JackpotBusinessLogic.GetCompensationPct(_db_to_compensate, _db_sum_minimum, _db_sum_average);
          _total_compensated = Math.Round(_total_contribution * _compens_pct / 100.0m, 8, MidpointRounding.AwayFromZero);

          _pending_contribution = _total_contribution - _total_compensated;


          // AJQ 23-OCT-2014, Avoid Jackpot "inversion"
          StringBuilder _sb;
          _sb = new StringBuilder();
          _sb.AppendLine("declare @_acum1   numeric(20,8)");
          _sb.AppendLine("declare @_acum2   numeric(20,8)");
          _sb.AppendLine("declare @_acum3   numeric(20,8)");
          _sb.AppendLine("declare @_aux     numeric(20,8)");
          _sb.AppendLine("declare @_changed bit");
          _sb.AppendLine("");
          _sb.AppendLine("SELECT @_acum1 = SJI_ACCUMULATED FROM SITE_JACKPOT_INSTANCES WHERE SJI_INDEX = 1");
          _sb.AppendLine("SELECT @_acum2 = SJI_ACCUMULATED FROM SITE_JACKPOT_INSTANCES WHERE SJI_INDEX = 2");
          _sb.AppendLine("SELECT @_acum3 = SJI_ACCUMULATED FROM SITE_JACKPOT_INSTANCES WHERE SJI_INDEX = 3");
          _sb.AppendLine("");
          _sb.AppendLine("IF ( @_acum1 < @_acum2 )");
          _sb.AppendLine("BEGIN");
          _sb.AppendLine("  SET @_aux     = @_acum1 ");
          _sb.AppendLine("  SET @_acum1   = @_acum2 ");
          _sb.AppendLine("  SET @_acum2   = @_aux");
          _sb.AppendLine("  SET @_changed = 1");
          _sb.AppendLine("END");
          _sb.AppendLine("");
          _sb.AppendLine("IF ( @_acum2 < @_acum3 )");
          _sb.AppendLine("BEGIN");
          _sb.AppendLine("  SET @_aux     = @_acum2 ");
          _sb.AppendLine("  SET @_acum2   = @_acum3 ");
          _sb.AppendLine("  SET @_acum3   = @_aux");
          _sb.AppendLine("  SET @_changed = 1");
          _sb.AppendLine("END");
          _sb.AppendLine("");
          _sb.AppendLine("IF ( @_acum1 < @_acum2 )");
          _sb.AppendLine("BEGIN");
          _sb.AppendLine("  SET @_aux     = @_acum1 ");
          _sb.AppendLine("  SET @_acum1   = @_acum2 ");
          _sb.AppendLine("  SET @_acum2   = @_aux");
          _sb.AppendLine("  SET @_changed = 1");
          _sb.AppendLine("END");
          _sb.AppendLine("");
          _sb.AppendLine("IF ( @_changed = 1 )");
          _sb.AppendLine("BEGIN");
          _sb.AppendLine("  UPDATE SITE_JACKPOT_INSTANCES SET SJI_ACCUMULATED = @_acum1 WHERE SJI_INDEX = 1");
          _sb.AppendLine("  UPDATE SITE_JACKPOT_INSTANCES SET SJI_ACCUMULATED = @_acum2 WHERE SJI_INDEX = 2");
          _sb.AppendLine("  UPDATE SITE_JACKPOT_INSTANCES SET SJI_ACCUMULATED = @_acum3 WHERE SJI_INDEX = 3");
          _sb.AppendLine("END");
          _sb.AppendLine("");

          using (SqlCommand _cmd = new SqlCommand(_sb.ToString(), _sql_con, _sql_trx))
          {
            _cmd.ExecuteNonQuery();
          }

          _total_contributed = 0;
          for (int _idx = 1; _idx <= NUM_JACKPOTS; _idx++)
          {
            Decimal _individual_contrib;
            Decimal _db_sji_contrib_pct;
            Decimal _db_sji_minimum;
            Decimal _db_sji_maximum;
            Decimal _db_sji_average;
            Decimal _db_sji_accumulated;
            Decimal _db_sji_increment;
            Decimal _db_sji_minimum_bet;

            Decimal _missing;
            Decimal _new_accumulated;
            int _db_sji_num_pending;
            String _db_sji_name;
            SiteJackpot _site_jackpot;

            _sql_txt = "";
            _sql_txt += "  SELECT   SJI_CONTRIBUTION_PCT  ";
            _sql_txt += "         , SJI_MINIMUM           ";
            _sql_txt += "         , SJI_MAXIMUM           ";
            _sql_txt += "         , SJI_ACCUMULATED       ";
            _sql_txt += "         , SJI_AVERAGE           ";
            _sql_txt += "         , SJI_NUM_PENDING       ";
            _sql_txt += "         , SJI_NAME              ";
            _sql_txt += "         , SJI_MINIMUM_BET       ";
            _sql_txt += "    FROM   SITE_JACKPOT_INSTANCES";
            _sql_txt += "   WHERE   SJI_INDEX = @pJackpotIdx   ";

            _sql_cmd = new SqlCommand(_sql_txt, _sql_con, _sql_trx);
            _sql_cmd.Parameters.Add("@pJackpotIdx", System.Data.SqlDbType.Int).Value = _idx;
            using (SqlDataReader _sql_red = _sql_cmd.ExecuteReader())
            {
              if (!_sql_red.Read())
              {
                return;
              }

              _db_sji_contrib_pct = _sql_red.GetDecimal(0); // SJI_CONTRIBUTION_PCT
              _db_sji_minimum = _sql_red.GetDecimal(1); // SJI_MINIMUM
              _db_sji_maximum = _sql_red.GetDecimal(2); // SJI_MAXIMUM
              _db_sji_accumulated = _sql_red.GetDecimal(3); // SJI_ACCUMULATED
              _db_sji_average = _sql_red.GetDecimal(4); // SJI_AVERAGE
              _db_sji_num_pending = _sql_red.GetInt32(5);   // SJI_NUM_PENDING
              _db_sji_name = _sql_red.GetString(6);  // SJI_NAME
              _db_sji_minimum_bet = _sql_red.GetDecimal(7); // SJI_MINIMUM_BET
            }

            // Individual contribution
            _individual_contrib = Math.Round(_total_contribution * _db_sji_contrib_pct / 100.0m, 8, MidpointRounding.AwayFromZero);

            // Awarded?
            if (_db_sji_num_pending == 0)
            {
              // No Jackpot pending
              if (_db_sji_accumulated < _db_sji_maximum && _too_much_to_compensate)
              {
                // Too much to compensate: DON'T DRAW IT
                _awarded = false;
              }
              else
              {
                // DRAW IT
                _awarded = JackpotBusinessLogic.JackpotHit(_individual_contrib, _db_sji_average, _db_sji_accumulated, _db_sji_maximum, true);
              }

              if (_awarded)
              {
                _db_sji_num_pending++;
              }
            }
            else
            {
              // There are some Jackpots pending, don't award any more
              _awarded = false;
            }

            _db_sji_increment = Math.Round(_pending_contribution * _db_sji_contrib_pct / 100.0m, 8, MidpointRounding.AwayFromZero);

            // New Accumulated only for local calculations ...
            _new_accumulated = _db_sji_accumulated + _db_sji_increment;

            // Maximum
            if (!_db_exceed_maximum_allowed)
            {
              if (_new_accumulated > _db_sji_maximum)
              {
                _db_sji_increment -= (_new_accumulated - _db_sji_maximum); // Incr -= ( excess )
                _new_accumulated = _db_sji_accumulated + _db_sji_increment;
              }
            }

            _total_contributed += _db_sji_increment;

            // Minimum
            if (_new_accumulated < _db_sji_minimum)
            {
              _missing = _db_sji_minimum - _new_accumulated;
              _to_compensate += _missing;
              _db_sji_increment += _missing;
              _new_accumulated += _missing;
            }

            _sql_txt = "";
            _sql_txt += "UPDATE   SITE_JACKPOT_INSTANCES";
            _sql_txt += "   SET   SJI_ACCUMULATED = SJI_ACCUMULATED + @pJackpotIncr";
            _sql_txt += "       , SJI_NUM_PENDING = SJI_NUM_PENDING + @pAwarded";
            _sql_txt += " WHERE   SJI_INDEX       = @pJackpotIdx";

            _sql_cmd = new SqlCommand(_sql_txt, _sql_con, _sql_trx);
            _sql_cmd.Parameters.Add("@pJackpotIncr", System.Data.SqlDbType.Decimal).Value = _db_sji_increment;
            _sql_cmd.Parameters.Add("@pAwarded", System.Data.SqlDbType.Int).Value = _awarded ? 1 : 0;
            _sql_cmd.Parameters.Add("@pJackpotIdx", System.Data.SqlDbType.Int).Value = _idx;

            if (_sql_cmd.ExecuteNonQuery() != 1)
            {
              return;
            }

            _site_jackpot = new SiteJackpot();
            _site_jackpot.index = _idx;
            _site_jackpot.name = _db_sji_name;
            _site_jackpot.accumulated = _new_accumulated;
            _site_jackpot.minimum_bet = _db_sji_minimum_bet;
            _new_values[_idx - 1] = _site_jackpot;

          } // for all jackpots

          // Contributed less than Contribution?
          if (_total_contributed < _pending_contribution)
          {
            // Use the difference to compensate
            _total_compensated += (_pending_contribution - _total_contributed);
          }
          // Contributed more than Contribution?
          if (_total_contributed > _pending_contribution)
          {
            // The difference must be compensated
            _to_compensate += (_total_contributed - _pending_contribution);
          }

          _sql_txt = "";
          _sql_txt += "UPDATE   SITE_JACKPOT_PARAMETERS ";
          _sql_txt += "   SET   SJP_TO_COMPENSATE            = SJP_TO_COMPENSATE - @pCompensated + @pToCompensate ";
          _sql_txt += "       , SJP_PLAYED                   = SJP_PLAYED        - @pUsedPlayed                   ";
          _sql_txt += "       , SJP_CURRENT_COMPENSATION_PCT = @pCompensationPct ";
          _sql_txt += " WHERE   SJP_PLAYED                  >= @pUsedPlayed ";

          _sql_cmd = new SqlCommand(_sql_txt, _sql_con, _sql_trx);
          _sql_cmd.Parameters.Add("@pCompensated", System.Data.SqlDbType.Decimal).Value = _total_compensated;
          _sql_cmd.Parameters.Add("@pToCompensate", System.Data.SqlDbType.Decimal).Value = _to_compensate;
          _sql_cmd.Parameters.Add("@pUsedPlayed", System.Data.SqlDbType.Decimal).Value = _used_played;
          _sql_cmd.Parameters.Add("@pCompensationPct", System.Data.SqlDbType.Decimal).Value = _compens_pct;


          if (_sql_cmd.ExecuteNonQuery() != 1)
          {
            return;
          }

          if (!m_history_read)
          {
            ArrayList _history;

            if (GetSiteJackpotHistory(out _history, _sql_trx))
            {
              m_rw_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
              if (!m_history_read)
              {
                m_history = _history;
                m_history_read = true;
              }
              m_rw_lock.ReleaseWriterLock();
            }
          }

          if (_trx.Commit())
          {
            // Set new jackpot values
            SetNewValues(Milliseconds, _new_values, _new_msg);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        if (_sql_cmd != null)
        {
          _sql_cmd.Dispose();
          _sql_cmd = null;
        }
      }
    } // Contribution

    /// <summary>
    /// Jackpots contribution manager
    /// </summary>
    /// <param name="Milliseconds"></param>
    private static void JackpotsContribution(long Milliseconds)
    {
      // Read "Played"
      Decimal _pending_contribution;
      Decimal _total_contribution;
      Decimal _total_compensated;
      Decimal _to_compensate;
      Decimal _compens_pct;
      Decimal _total_contributed;

      Decimal _used_played;

      Decimal _db_contrib_pct;
      Decimal _db_to_compensate;
      Decimal _db_sum_minimum;
      Decimal _db_sum_average;
      Decimal _db_played;
      Decimal _avg_played;
      int _db_avg_int_hours;
      int _db_num_play_sessions;
      Boolean _too_much_to_compensate;
      SiteJackpot[] _new_values = new SiteJackpot[3];

      String[] _new_msg = new String[NUM_MESSAGES];

      Decimal _seconds;

      Boolean _jackpot_viewers_enabled;
      JackpotViewer _jackpot_viewer_obj = new JackpotViewer();
      List<JackpotViewerItemParameters> _jackpot_viewer_item_params_list;
      List<JackpotViewer> _enabled_jackpot_viewers;

      _jackpot_viewers_enabled = false;
      _enabled_jackpot_viewers = new List<JackpotViewer>();
      _jackpot_viewer_item_params_list = new List<JackpotViewerItemParameters>();

      _db_num_play_sessions = 0;
      _db_to_compensate = 0;
      _db_sum_minimum = 0;
      _db_sum_average = 0;
      _to_compensate = 0;
      _total_compensated = 0;
      _seconds = Milliseconds / 1000;

      try
      {
        using (DB_TRX _db_trx = new DB_TRX())
        {                                  
          _jackpot_viewers_enabled = _jackpot_viewer_obj.ExistJackpotViewersEnabledAndRelatedToViewer(_db_trx.SqlTransaction);

          // Get enabled jackpot viewer list
          _enabled_jackpot_viewers = _jackpot_viewer_obj.GetEnabledJackpotViewers(_db_trx.SqlTransaction);
          if (_enabled_jackpot_viewers.Count > 0)
          {
            _jackpot_viewer_item_params_list = new JackpotViewerItemParameters().GetJackpotViewerParametersList(_enabled_jackpot_viewers[0].Id, _db_trx.SqlTransaction);
          }

          _total_contributed = 0;
          for (int _idx = 0; _idx < NUM_JACKPOTS && _idx < _jackpot_viewer_item_params_list.Count; _idx++)
          {
            Decimal _individual_contrib;
            Decimal _db_sji_contrib_pct = 0;
            Decimal _db_sji_minimum = 0;
            Decimal _db_sji_maximum = 0;
            Decimal _db_sji_average = 0;
            Decimal _db_sji_accumulated = 0;
            Decimal _db_sji_increment = 0;
            Decimal _db_sji_minimum_bet = 0;

            Decimal _missing;
            Decimal _new_accumulated;
            int _db_sji_num_pending = 0;
            String _db_sji_name = "";
            SiteJackpot _site_jackpot;

            if (_jackpot_viewer_item_params_list.Count > 0)
            {
              _db_to_compensate = _jackpot_viewer_item_params_list[_idx].ToCompensate;
              _db_sum_minimum = _jackpot_viewer_item_params_list[_idx].SumMinimum;
              _db_sum_average = _jackpot_viewer_item_params_list[_idx].Average;
              _db_sji_contrib_pct = _jackpot_viewer_item_params_list[_idx].ContributionPct;
              _db_sji_minimum = _jackpot_viewer_item_params_list[_idx].Minimum;
              _db_sji_maximum = _jackpot_viewer_item_params_list[_idx].Maximum;
              _db_sji_accumulated = _jackpot_viewer_item_params_list[_idx].Accumulated;
              _db_sji_average = _jackpot_viewer_item_params_list[_idx].Average;
              _db_sji_num_pending = _jackpot_viewer_item_params_list[_idx].NumPending;
              _db_sji_name = _jackpot_viewer_item_params_list[_idx].Name;
              _db_sji_minimum_bet = _jackpot_viewer_item_params_list[_idx].MinimumBet;
              _db_num_play_sessions = _jackpot_viewer_item_params_list[_idx].Playing;
            }

            // CONTRIBUTION value
            if (_db_sji_contrib_pct > 0)   // SJP_CONTRIBUTION_FIX
            {
              _db_contrib_pct = _db_sji_contrib_pct;
            }
            else
            {
              //Determine the right working day, depending on closing time
              _db_contrib_pct = _jackpot_viewer_item_params_list[_idx].GetWorkindDayPct(Misc.Opening(WGDB.Now).DayOfWeek);
            }
         
            _db_played = 0;   // SJP_PLAYED

            _db_avg_int_hours = 8;

            _new_msg[0] = "";
            _new_msg[1] = "";
            _new_msg[2] = "";

            m_site_jackpot_params_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
            try
            {
              m_animation_interval_01 = 15;
              m_animation_interval_02 = 0;
              m_recent_history_interval = 3600;
              m_show_winner_name = false;
              m_show_terminal_name = false;
            }
            finally
            {
              m_site_jackpot_params_lock.ReleaseWriterLock();
            }

            // Avoid too many jackpots awarded (protection against faulty random generator)
            _too_much_to_compensate = (_db_to_compensate >= _db_sum_minimum);

            if (GeneralParam.GetBoolean("SiteJackpot", "Buffering.Enabled", true))
            {
              _avg_played = _db_played / (_db_avg_int_hours * 3600m); // 1 day -> 24h --> AvgPlayed per Second
              _used_played = Math.Round(_avg_played * Milliseconds / 1000.0m, 2, MidpointRounding.AwayFromZero);
              _used_played = Math.Max(_used_played, 1.0m);        // At least $1
              _used_played = Math.Min(_used_played, _db_played);
              _used_played = Math.Max(_used_played, 0);           // Ensure > 0; the previus maximum is necessary to ensure that the used_played is at least 1 and less than the 'db_played'
            }
            else
            {
              _used_played = _db_played;
            }
            
            // AJQ 06-JUN-2014, Don't increment the Jackpot when nobody is playing ...
            if (_db_num_play_sessions <= 0)
            {
              // No active play sessions
              _used_played = 0;
            }

            // Total Contribution
            _total_contribution = _used_played * _db_contrib_pct / 100.0m;

            // Compensation 
            _compens_pct = JackpotBusinessLogic.GetCompensationPct(_db_to_compensate, _db_sum_minimum, _db_sum_average);
            _total_compensated = Math.Round(_total_contribution * _compens_pct / 100.0m, 8, MidpointRounding.AwayFromZero);

            _pending_contribution = _total_contribution - _total_compensated;
           
            // Individual contribution
            _individual_contrib = Math.Round(_total_contribution * _db_sji_contrib_pct / 100.0m, 8, MidpointRounding.AwayFromZero);

            _db_sji_increment = Math.Round(_pending_contribution * _db_sji_contrib_pct / 100.0m, 8, MidpointRounding.AwayFromZero);

            // New Accumulated only for local calculations ...
            _new_accumulated = _db_sji_accumulated + _db_sji_increment;

            // Maximum
            if (_new_accumulated > _db_sji_maximum)
            {
              _db_sji_increment -= (_new_accumulated - _db_sji_maximum); // Incr -= ( excess )
              _new_accumulated = _db_sji_accumulated + _db_sji_increment;
            }

            _total_contributed += _db_sji_increment;

            // Minimum
            if (_new_accumulated < _db_sji_minimum)
            {
              _missing = _db_sji_minimum - _new_accumulated;
              _to_compensate += _missing;
              _db_sji_increment += _missing;
              _new_accumulated += _missing;
            }
          
            _site_jackpot = new SiteJackpot();
            _site_jackpot.index = _idx + 1;
            _site_jackpot.name = _db_sji_name;
            _site_jackpot.accumulated = _new_accumulated;
            _site_jackpot.minimum_bet = _db_sji_minimum_bet;
            _new_values[_idx] = _site_jackpot;

          } // for all jackpots    

          if (!m_history_read)
          {
            ArrayList _history;
            _jackpot_viewers_enabled = false;
            _history = new ArrayList();
            if (_jackpot_viewers_enabled && _enabled_jackpot_viewers.Count > 0)
            {
              _enabled_jackpot_viewers[0].ReadAwards(_enabled_jackpot_viewers[0].Code);

              if (_enabled_jackpot_viewers[0].JackpotAwards.Count > 0)
              {
                m_rw_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);

                foreach (JackpotAwardItemList JackpotItem in _enabled_jackpot_viewers[0].JackpotAwards)
                {
                  foreach (JackpotAwardItem AwardItem in JackpotItem.Awards)
                  {
                    SiteJackpotHistory _sjh;
                    String _floor_id;

                    _sjh = new SiteJackpotHistory();

                    _sjh.awarded = AwardItem.DateAward;
                    _sjh.index = AwardItem.Position;
                    _sjh.name = AwardItem.Name;
                    _sjh.amount = AwardItem.Amount;
                    _sjh.player_name = "";
                    _sjh.terminal_name = "";

                    _floor_id = "";
                    if (!String.IsNullOrEmpty(_floor_id))
                    {
                      // Prefix the terminal name with the FloorId
                      _sjh.terminal_name = _floor_id + " " + _sjh.terminal_name;
                    }
                    _history.Add(_sjh);
                  }                  
                }

                if (!m_history_read)
                {
                  m_history = _history;
                  m_history_read = true;
                }
                m_rw_lock.ReleaseWriterLock();
              }
            }
            else
            {
              if (GetSiteJackpotHistory(out _history, _db_trx.SqlTransaction))
              {
                m_rw_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
                if (!m_history_read)
                {
                  m_history = _history;
                  m_history_read = true;
                }
                m_rw_lock.ReleaseWriterLock();
              }
            }
          }

           // If jackpotviewer has 2 or less jackpots TODO RLO
          if (_jackpot_viewer_item_params_list.Count < NUM_JACKPOTS)
          {
            ResetEmptyJackpots(_jackpot_viewer_item_params_list.Count, _new_values);
          }

          if (_db_trx.Commit())
          {
            // Set new jackpot values
            SetNewValues(Milliseconds, _new_values, _new_msg);
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    } // JackpotsContribution

    /// <summary>
    /// Reset empty jackpots to visualize correctly
    /// </summary>
    /// <param name="JackpotCount"></param>
    /// <param name="NewValues"></param>
    private static void ResetEmptyJackpots(int JackpotCount,  SiteJackpot[] NewValues)
    {
      SiteJackpot _site_jackpot;

      for (int _idx = JackpotCount; _idx < NUM_JACKPOTS; _idx++)
      {
        _site_jackpot = new SiteJackpot();
        _site_jackpot.index = _idx + 1;
        _site_jackpot.name = "";
        _site_jackpot.accumulated = 0;
        _site_jackpot.minimum_bet = 0;
        NewValues[_idx] = _site_jackpot;
      }
    } //ResetEmptyJackpots

    //------------------------------------------------------------------------------
    // PURPOSE: Reads the site jackpot params from SITE_JACKPOT_PARAMETERS table
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //   
    private static bool DB_ReadSiteJackpotParameters(bool BeingAwardedViaBonus, SqlTransaction Trx, out SiteJackpotDBParams JackpotParams)
    {
      SqlCommand _sql_cmd;
      SqlDataReader _dr;
      StringBuilder _sb;

      _dr = null;
      JackpotParams = new SiteJackpotDBParams();

      try
      {
        // Parameters
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT   SJP_AWARDING_DAYS                ");
        _sb.AppendLine("       , SJP_AWARDING_START               ");
        _sb.AppendLine("       , SJP_AWARDING_END                 ");
        _sb.AppendLine("       , SJP_AWARDING_EXCLUDE_PROMOTIONS  ");
        _sb.AppendLine("       , SJP_AWARDING_MIN_OCCUPATION_PCT  ");
        _sb.AppendLine("       , SJP_AWARDING_EXCLUDE_ANONYMOUS   ");
        _sb.AppendLine("       , SJP_TO_COMPENSATE                ");
        _sb.AppendLine("       , (SELECT SUM (SJI_MINIMUM) FROM SITE_JACKPOT_INSTANCES) SJP_SUM_MINIMUM ");
        _sb.AppendLine("       , SJI_INDEX                        ");
        _sb.AppendLine("       , SJI_ACCUMULATED                  ");
        _sb.AppendLine("       , SJI_MINIMUM                      ");
        _sb.AppendLine("       , SJI_MAXIMUM                      ");
        _sb.AppendLine("       , SJI_AVERAGE                      ");
        _sb.AppendLine("       , SJI_NAME                         ");
        _sb.AppendLine("       , SJI_MINIMUM_BET                  ");
        _sb.AppendLine("  FROM   SITE_JACKPOT_PARAMETERS          ");
        _sb.AppendLine("       , SITE_JACKPOT_INSTANCES           ");
        _sb.AppendLine("  WHERE  SJP_ENABLED = 1                  ");

        //instance filter
        if (BeingAwardedViaBonus)
        {
          _sb.AppendLine("AND  SJI_INDEX   = ( SELECT MAX (SJI_INDEX) FROM SITE_JACKPOT_INSTANCES WHERE SJI_NUM_PENDING > 0 AND SJI_BONUS_ID IS NOT NULL )");
        }
        else
        {
          _sb.AppendLine("AND  SJI_INDEX   = ( SELECT MAX (SJI_INDEX) FROM SITE_JACKPOT_INSTANCES WHERE SJI_NUM_PENDING > 0 AND SJI_BONUS_ID IS NULL )");
        }


        _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx);
        _dr = _sql_cmd.ExecuteReader();
        if (!_dr.Read())
        {
          if (GeneralParam.GetBoolean("SiteJackpot", "LogEnabled", false)
            && !BeingAwardedViaBonus)
          {
            // SPECIAL
            Log.Message("SiteJackpot.DB_ReadSiteJackpotParameters read (false)");
          }
          return false;
        }

        JackpotParams.week_days = _dr.GetInt32(_dr.GetOrdinal("SJP_AWARDING_DAYS"));
        JackpotParams.t0 = _dr.GetInt32(_dr.GetOrdinal("SJP_AWARDING_START"));
        JackpotParams.t1 = _dr.GetInt32(_dr.GetOrdinal("SJP_AWARDING_END"));
        JackpotParams.exclude_promotions = _dr.GetBoolean(_dr.GetOrdinal("SJP_AWARDING_EXCLUDE_PROMOTIONS"));
        JackpotParams.min_occupation = _dr.GetDecimal(_dr.GetOrdinal("SJP_AWARDING_MIN_OCCUPATION_PCT"));
        JackpotParams.exclude_anonymous = _dr.GetBoolean(_dr.GetOrdinal("SJP_AWARDING_EXCLUDE_ANONYMOUS"));
        JackpotParams.to_compensate = _dr.GetDecimal(_dr.GetOrdinal("SJP_TO_COMPENSATE"));
        JackpotParams.sum_minimum = _dr.GetDecimal(_dr.GetOrdinal("SJP_SUM_MINIMUM"));
        JackpotParams.jackpot_index = _dr.GetInt32(_dr.GetOrdinal("SJI_INDEX"));
        JackpotParams.accumulated = _dr.GetDecimal(_dr.GetOrdinal("SJI_ACCUMULATED"));
        JackpotParams.minimum = _dr.GetDecimal(_dr.GetOrdinal("SJI_MINIMUM"));
        JackpotParams.maximum = _dr.GetDecimal(_dr.GetOrdinal("SJI_MAXIMUM"));
        JackpotParams.average = _dr.GetDecimal(_dr.GetOrdinal("SJI_AVERAGE"));
        JackpotParams.jackpot_name = _dr.GetString(_dr.GetOrdinal("SJI_NAME"));
        JackpotParams.min_bet = _dr.GetDecimal(_dr.GetOrdinal("SJI_MINIMUM_BET"));

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_dr != null)
        {
          _dr.Close();
          _dr.Dispose();
          _dr = null;
        }
      }

    } // DB_ReadSiteJackpotParameters
 
    //------------------------------------------------------------------------------
    // PURPOSE: Checks if occupation is enough to award the jackpot
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //   
    private static bool DB_CheckOccupation(SiteJackpotDBParams SiteJackpotDBPparams, SqlTransaction Trx)
    {
      SqlCommand _sql_cmd;
      StringBuilder _sb;
      Decimal _db_occupation;

      _sb = new StringBuilder();

      _sb.AppendLine("SELECT   100.0 * ( SELECT COUNT(1) FROM (SELECT DISTINCT PS_TERMINAL_ID FROM PLAY_SESSIONS WITH (INDEX (IX_PS_STATUS))  WHERE PS_STATUS = 0) PLAYING ) ");
      _sb.AppendLine("       /         ( SELECT COUNT(*) FROM TERMINALS WHERE TE_TYPE = 1 AND TE_STATUS = 0 AND TE_TERMINAL_TYPE < 100 ) ");

      _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx);

      _db_occupation = (Decimal)_sql_cmd.ExecuteScalar();

      if (_db_occupation < SiteJackpotDBPparams.min_occupation)
      {
        if (GeneralParam.GetBoolean("SiteJackpot", "LogEnabled", false))
        {
          // SPECIAL
          Log.Message("SiteJackpot.JackpotAward MinOccupation > " + _db_occupation.ToString());
        }

        return false;
      }

      return true;
    } // DB_CheckOccupation

    //------------------------------------------------------------------------------
    // PURPOSE: Reads current active playing sessions
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private static bool DB_ReadActivePlayingSessions(SiteJackpotDBParams SiteJackpotDBPparams, HANDPAY_PAYMENT_MODE _jackpot_mode,
                                                     SqlTransaction Trx, out DataTable PlayingSessions)
    {
      StringBuilder _sb;

      PlayingSessions = null;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT   PS_TERMINAL_ID                                     ");
        _sb.AppendLine("       , PS_ACCOUNT_ID                                      ");
        _sb.AppendLine("  FROM   PLAY_SESSIONS                                      ");
        _sb.AppendLine("       , ACCOUNTS                                           ");

        // Participating Providers (from part)
        _sb.AppendLine("       , TERMINALS                                          ");
        _sb.AppendLine("       , PROVIDERS                                          ");
        _sb.AppendLine("       , TERMINAL_GROUPS                                    ");

        _sb.AppendLine(" WHERE   PS_STATUS        = @pPlaySessionStatusOpened       ");
        _sb.AppendLine("   AND   PS_PLAYED_COUNT  > 0                               "); // Has played some game
        _sb.AppendLine("   AND   PS_PLAYED_AMOUNT > 0                               "); // Has played some money
        _sb.AppendLine("   AND   PS_ACCOUNT_ID   = AC_ACCOUNT_ID                    ");
        _sb.AppendLine("   AND   DATEDIFF (SECOND, PS_FINISHED, GETDATE ()) <= 300  "); // 5 minutes
        _sb.AppendLine("   AND   ISNULL (AC_NR_WON_LOCK, 0)            = 0          "); // No Lock
        _sb.AppendLine("   AND   TG_TERMINAL_ID = PS_TERMINAL_ID                    ");
        _sb.AppendLine("   AND   TG_ELEMENT_ID = 1                                  ");  // Element Id 1 is JackPot Generic
        _sb.AppendLine("   AND   TG_ELEMENT_TYPE = @pElementType                    ");  // Provider participates on the Site Jackpot

        if (SiteJackpotDBPparams.exclude_promotions)
        {
          _sb.AppendLine("   AND   ISNULL (AC_IN_SESSION_PROMO_NR_TO_GM, 0) = 0     "); // No Promotion
        }

        if (SiteJackpotDBPparams.exclude_anonymous)
        {
          _sb.AppendLine("   AND   AC_USER_TYPE    <> 0 ");
          _sb.AppendLine("   AND   AC_HOLDER_LEVEL <> 0 ");
        }
        // Participating Providers (where part)
        _sb.AppendLine("   AND   TE_TERMINAL_ID  = PS_TERMINAL_ID                   ");
        _sb.AppendLine("   AND   TE_PROV_ID      = PV_ID                            ");

        // On bonusing, only award to SAS Host terminals
        if (_jackpot_mode == HANDPAY_PAYMENT_MODE.BONUSING)
        {
          _sb.AppendLine("   AND   TE_TERMINAL_TYPE      = @TerminalType ");
        }

        // Minimum Bet
        _sb.AppendLine("   AND   PS_PLAYED_AMOUNT >= PS_PLAYED_COUNT * @pMinimumBet ");

        using (SqlCommand _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx))
        {
          _sql_cmd.Parameters.Add("@pPlaySessionStatusOpened", SqlDbType.Int).Value = PlaySessionStatus.Opened;
          _sql_cmd.Parameters.Add("@pElementType", SqlDbType.Int).Value = EXPLOIT_ELEMENT_TYPE.JACKPOT;

          if (_jackpot_mode == HANDPAY_PAYMENT_MODE.BONUSING)
          {
            _sql_cmd.Parameters.Add("@TerminalType", SqlDbType.Int).Value = TerminalTypes.SAS_HOST;
          }

          _sql_cmd.Parameters.Add("@pMinimumBet", SqlDbType.Decimal).Value = SiteJackpotDBPparams.min_bet;

          using (SqlDataAdapter _sql_da = new SqlDataAdapter(_sql_cmd))
          {
            PlayingSessions = new DataTable();

            _sql_da.Fill(PlayingSessions);

            if (PlayingSessions.Rows.Count <= 0)
            {
              if (GeneralParam.GetBoolean("SiteJackpot", "LogEnabled", false))
              {
                // SPECIAL
                Log.Message("SiteJackpot.JackpotAward (No Active Playing Sessions)");
              }

              return false;
            }
          }
        }

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
    } // DB_ReadActivePlayingSessions  

    //------------------------------------------------------------------------------
    // PURPOSE: Reads terminals
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    // 
    private static bool DB_ReadSiteTerminal(SqlTransaction Trx, out int TerminalId, out String ProviderId, out String TerminalName)
    {
      SqlCommand _sql_cmd;
      SqlDataReader _dr;
      StringBuilder _sb;

      _dr = null;
      TerminalId = 0;
      ProviderId = "";
      TerminalName = "";

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("SELECT   TE_TERMINAL_ID ");
        _sb.AppendLine("       , TE_PROVIDER_ID ");
        _sb.AppendLine("       , TE_NAME        ");
        _sb.AppendLine("  FROM   TERMINALS      ");
        _sb.AppendLine(" WHERE   TE_TERMINAL_ID = (SELECT MIN(TE_TERMINAL_ID) FROM TERMINALS WHERE TE_TERMINAL_TYPE = @pTerminalType)");

        _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx);
        _sql_cmd.Parameters.Add("@pTerminalType", SqlDbType.SmallInt).Value = TerminalTypes.SITE;
        _dr = _sql_cmd.ExecuteReader();

        if (!_dr.Read())
        {
          Log.Error("SiteJackpot.JackpotAward (Read Terminals(Site) Failed!)");

          return false;
        }

        TerminalId = _dr.GetInt32(_dr.GetOrdinal("TE_TERMINAL_ID"));
        ProviderId = _dr.GetString(_dr.GetOrdinal("TE_PROVIDER_ID"));
        TerminalName = _dr.GetString(_dr.GetOrdinal("TE_NAME"));

        return true;
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        if (_dr != null)
        {
          _dr.Close();
          _dr.Dispose();
          _dr = null;
        }
      }
    } // DB_ReadTerminals

    //------------------------------------------------------------------------------
    // PURPOSE: Update jackpot params
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //   
    private static bool DB_UpdateJackpotParams(SiteJackpotDBParams SiteJackpotDBPparams, Decimal JackpotPrize, SqlTransaction Trx)
    {
      SqlCommand _sql_cmd;
      StringBuilder _sb;
      Decimal _avg;
      Decimal _excess;

      // Set the new initial Jackpot
      try
      {
        _excess = -(SiteJackpotDBPparams.to_compensate + SiteJackpotDBPparams.sum_minimum);
        _excess = Math.Max(0, _excess);                                             // Excess.Minimum = 0
        _avg = Math.Max(0, 0.10m * (SiteJackpotDBPparams.average - SiteJackpotDBPparams.minimum));
        _excess = Math.Min(_excess, _avg);

        SiteJackpotDBPparams.minimum = SiteJackpotDBPparams.minimum + _excess * CERTIFIED_RNG.GetRandomNumber(1000000) / 1000000m;
        SiteJackpotDBPparams.minimum = Math.Round(SiteJackpotDBPparams.minimum, 8, MidpointRounding.AwayFromZero);

        // Compensate
        _sb = new StringBuilder();

        _sb.AppendLine("UPDATE   SITE_JACKPOT_PARAMETERS ");
        _sb.AppendLine("   SET   SJP_TO_COMPENSATE = SJP_TO_COMPENSATE + @pMinimum ");

        _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx);

        _sql_cmd.Parameters.Add("@pMinimum", SqlDbType.Money).Value = SiteJackpotDBPparams.minimum;

        if (_sql_cmd.ExecuteNonQuery() != 1)
        {
          Log.Error("SiteJackpot.JackpotAward Update Parameters Failed!");

          return false;
        }

        _sb = new StringBuilder();

        _sb.AppendLine("UPDATE   SITE_JACKPOT_INSTANCES");
        _sb.AppendLine("   SET   SJI_ACCUMULATED = SJI_ACCUMULATED - @pJackpotPrize + @pMinimum ");
        _sb.AppendLine("       , SJI_NUM_PENDING = SJI_NUM_PENDING - 1 ");
        _sb.AppendLine(" WHERE   SJI_INDEX       = @JackpotIdx ");
        _sb.AppendLine("   AND   SJI_NUM_PENDING > 0");

        _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx);

        _sql_cmd.Parameters.Add("@pJackpotPrize", SqlDbType.Money).Value = JackpotPrize;
        _sql_cmd.Parameters.Add("@pMinimum", SqlDbType.Money).Value = SiteJackpotDBPparams.minimum;
        _sql_cmd.Parameters.Add("@JackpotIdx", SqlDbType.Int).Value = SiteJackpotDBPparams.jackpot_index;

        if (_sql_cmd.ExecuteNonQuery() != 1)
        {
          Log.Error("SiteJackpot.JackpotAward Update Instances Failed!");

          return false;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;

    } // DB_UpdateJackpotParams

    //------------------------------------------------------------------------------
    // PURPOSE: Inserts the handpay related to the jackpot
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //   
    private static bool DB_InsertHandpay(SiteJackpotDBParams SiteJackpotDBPparams, int TerminalId, String ProviderId, String TerminalName,
      Decimal JackpotPrize, int AwardedOnTerminalId, Int64 AwardedToAccountId, Int64 MovementId,
                                         SqlTransaction Trx, out DateTime HandpayDate)
    {
      SqlCommand _sql_cmd;
      StringBuilder _sb;

      HandpayDate = WGDB.Now;

      try
      {
        _sb = new StringBuilder();

        _sb.AppendLine("INSERT INTO HANDPAYS ( HP_TERMINAL_ID                         ");
        _sb.AppendLine("                     , HP_DATETIME                            ");
        _sb.AppendLine("                     , HP_AMOUNT                              ");
        _sb.AppendLine("                     , HP_TE_NAME                             ");
        _sb.AppendLine("                     , HP_TE_PROVIDER_ID                      ");
        _sb.AppendLine("                     , HP_TYPE                                ");
        _sb.AppendLine("                     , HP_SITE_JACKPOT_INDEX                  ");
        _sb.AppendLine("                     , HP_SITE_JACKPOT_NAME                   ");
        _sb.AppendLine("                     , HP_SITE_JACKPOT_AWARDED_ON_TERMINAL_ID ");
        _sb.AppendLine("                     , HP_SITE_JACKPOT_AWARDED_TO_ACCOUNT_ID  ");
        _sb.AppendLine("                     , HP_MOVEMENT_ID                         ");
        _sb.AppendLine("                     , HP_STATUS                              ");
        _sb.AppendLine("                     , HP_PAYMENT_MODE                        ");
        _sb.AppendLine("                     , HP_AMT0                                ");
        _sb.AppendLine("                     , HP_CUR0                                ");
        _sb.AppendLine("                     )                                        ");
        _sb.AppendLine("             VALUES  ( @TerminalId                            ");
        _sb.AppendLine("                     , @Date                                  ");
        _sb.AppendLine("                     , @Amount                                ");
        _sb.AppendLine("                     , @TermName                              ");
        _sb.AppendLine("                     , @ProviderId                            ");
        _sb.AppendLine("                     , @HandpayType                           ");
        _sb.AppendLine("                     , @JackpotIdx                            ");
        _sb.AppendLine("                     , @JackpotName                           ");
        _sb.AppendLine("                     , @AwardedOnTerminalId                   ");
        _sb.AppendLine("                     , @AwardedToAccountId                    ");
        _sb.AppendLine("                     , @MovementId                            ");
        _sb.AppendLine("                     , @HpStatus                              ");
        _sb.AppendLine("                     , @HpPaymentMode                         ");
        _sb.AppendLine("                     , @Amt0                                  ");
        _sb.AppendLine("                     , @cur0                                  ");
        _sb.AppendLine("                    )                                         ");


        _sql_cmd = new SqlCommand(_sb.ToString(), Trx.Connection, Trx);

        _sql_cmd.Parameters.Add("@TerminalId", SqlDbType.Int).Value = TerminalId;
        _sql_cmd.Parameters.Add("@ProviderId", SqlDbType.NVarChar).Value = ProviderId;
        _sql_cmd.Parameters.Add("@TermName", SqlDbType.NVarChar).Value = TerminalName;
        _sql_cmd.Parameters.Add("@Amount", SqlDbType.Money).Value = JackpotPrize;
        _sql_cmd.Parameters.Add("@HandpayType", SqlDbType.Int).Value = ((int)HANDPAY_TYPE.SITE_JACKPOT);
        _sql_cmd.Parameters.Add("@JackpotIdx", SqlDbType.Int).Value = SiteJackpotDBPparams.jackpot_index;
        _sql_cmd.Parameters.Add("@JackpotName", SqlDbType.NVarChar).Value = SiteJackpotDBPparams.jackpot_name;
        _sql_cmd.Parameters.Add("@AwardedOnTerminalId", SqlDbType.Int).Value = AwardedOnTerminalId;
        _sql_cmd.Parameters.Add("@AwardedToAccountId", SqlDbType.BigInt).Value = AwardedToAccountId;
        _sql_cmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = HandpayDate;
        _sql_cmd.Parameters.Add("@HpStatus", SqlDbType.Int).Value = MovementId == 0 ? HANDPAY_STATUS.PENDING : HANDPAY_STATUS.PAID;
        _sql_cmd.Parameters.Add("@HpPaymentMode", SqlDbType.Int).Value = ((int)Handpays.SiteJackpotPaymentMode);
        _sql_cmd.Parameters.Add("@MovementId", SqlDbType.BigInt).Value = MovementId == 0 ? DBNull.Value : (Object)MovementId;

        _sql_cmd.Parameters.Add("Amt0", SqlDbType.Money).Value = JackpotPrize;
        _sql_cmd.Parameters.Add("Cur0", SqlDbType.NVarChar).Value = CurrencyExchange.GetNationalCurrency();

        if (_sql_cmd.ExecuteNonQuery() != 1)
        {
          Log.Error("SiteJackpot.JackpotAward Insert Handpay Failed!");

          return false;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }

      return true;
    } // DB_InsertHandpay

    //------------------------------------------------------------------------------
    // PURPOSE: Awarding process
    //
    //  PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS:
    //   
    private static void JackpotAward(out Boolean Awarded)
    {
      SqlTransaction _sql_trx;
      DataTable _current_sessions;
      int _db_awarded_on_terminal_id;
      Int64 _db_awarded_to_account_id;
      Decimal _db_jackpot_prize;
      int _db_terminal_id;
      String _db_provider_id;
      String _db_terminal_name;
      DateTime _handpay_date;
      Handpays.HandpayRegisterOrCancelParameters _input_handpay;
      SiteJackpotDBParams _site_jackpot_db_params;
      HANDPAY_PAYMENT_MODE _jackpot_mode;
      TimeSpan _time;
      int _day_mask;
      ArrayList _history;

      _sql_trx = null;

      _current_sessions = null;

      Awarded = false;

      try
      {
        if (GeneralParam.GetBoolean("SiteJackpot", "LogEnabled", false))
        {
          // SPECIAL
          Log.Message("SiteJackpot.JackpotAward *** BEGIN ***");
        }

        _jackpot_mode = Handpays.SiteJackpotPaymentMode;
        switch (_jackpot_mode)
        {
          case HANDPAY_PAYMENT_MODE.MANUAL:
          case HANDPAY_PAYMENT_MODE.AUTOMATIC:
          case HANDPAY_PAYMENT_MODE.BONUSING:
            break;

          default:
            Log.Message(String.Format("SiteJackpot.JackpotAward unexpected value for JackpotMode parameter ({0})!", _jackpot_mode));
            return;
        } // switch

        _sql_trx = WGDB.Connection().BeginTransaction();

        if (!DB_ReadSiteJackpotParameters(false, _sql_trx, out _site_jackpot_db_params))
        {
          return;
        }

        // JACKPOT PRIZE
        _db_jackpot_prize = Math.Truncate(_site_jackpot_db_params.accumulated);

        _day_mask = (1 << (int)Common.Misc.TodayOpening().DayOfWeek);
        if ((_site_jackpot_db_params.week_days & _day_mask) != _day_mask)
        {
          if (GeneralParam.GetBoolean("SiteJackpot", "LogEnabled", false))
          {
            // SPECIAL
            Log.Message("SiteJackpot.JackpotAward (DayMask)");
          }
          return;
        }

        _time = WGDB.Now.Subtract(WGDB.Now.Date);
        if (_site_jackpot_db_params.t0 == _site_jackpot_db_params.t1)
        {
          // 24h
        }
        else if (_site_jackpot_db_params.t0 < _site_jackpot_db_params.t1)
        {
          // t0 <= _time < t1
          if (_site_jackpot_db_params.t0 <= _time.TotalSeconds
              && _time.TotalSeconds < _site_jackpot_db_params.t1)
          {
            // Ok
          }
          else
          {
            if (GeneralParam.GetBoolean("SiteJackpot", "LogEnabled", false))
            {
              // SPECIAL
              Log.Message("SiteJackpot.JackpotAward (Time: A)");
            }
            return;
          }
        }
        else
        {
          if (_site_jackpot_db_params.t0 <= _time.TotalSeconds      // _t0 <= _tt < 24h
              || _time.TotalSeconds < _site_jackpot_db_params.t1)     // 0h  <= _tt < _t1
          {
            // Ok
          }
          else
          {
            if (GeneralParam.GetBoolean("SiteJackpot", "LogEnabled", false))
            {
              // SPECIAL
              Log.Message("SiteJackpot.JackpotAward (Time: B)");
            }

            return;
          }
        }

        // Occupation
        if (!DB_CheckOccupation(_site_jackpot_db_params, _sql_trx))
        {
          return;
        }

        if (!DB_ReadActivePlayingSessions(_site_jackpot_db_params, _jackpot_mode, _sql_trx, out _current_sessions))
        {
          return;
        }

        // Select an Account: The Lucky One
        int _idx_lucky;
        _idx_lucky = (int)CERTIFIED_RNG.GetRandomNumber(_current_sessions.Rows.Count);
        _db_awarded_on_terminal_id = (int)_current_sessions.Rows[_idx_lucky][0];
        _db_awarded_to_account_id = (Int64)_current_sessions.Rows[_idx_lucky][1];

        if (!DB_ReadSiteTerminal(_sql_trx, out _db_terminal_id, out _db_provider_id, out _db_terminal_name))
        {
          return;
        }

        if (_jackpot_mode == HANDPAY_PAYMENT_MODE.MANUAL
            || _jackpot_mode == HANDPAY_PAYMENT_MODE.AUTOMATIC)
        {
          if (!DB_UpdateJackpotParams(_site_jackpot_db_params, _db_jackpot_prize, _sql_trx))
          {
            return;
          }

          // Update "Accumulated"
          _site_jackpot_db_params.accumulated = _site_jackpot_db_params.accumulated - _db_jackpot_prize + _site_jackpot_db_params.minimum;

          // Insert Handpay
          if (!DB_InsertHandpay(_site_jackpot_db_params, _db_terminal_id, _db_provider_id, _db_terminal_name, _db_jackpot_prize,
                                _db_awarded_on_terminal_id, _db_awarded_to_account_id, 0, _sql_trx, out _handpay_date))
          {
            return;
          }

          if (_jackpot_mode == HANDPAY_PAYMENT_MODE.AUTOMATIC)
          {
            _input_handpay = new Handpays.HandpayRegisterOrCancelParameters();
            _input_handpay.TerminalId = _db_terminal_id;
            _input_handpay.TerminalProvider = _db_provider_id;
            _input_handpay.TerminalName = _db_terminal_name;
            _input_handpay.HandpayType = HANDPAY_TYPE.SITE_JACKPOT;
            _input_handpay.HandpayAmount = _db_jackpot_prize;
            _input_handpay.HandpayAmt0 = _db_jackpot_prize;
            _input_handpay.HandpayCur0 = CurrencyExchange.GetNationalCurrency();
            _input_handpay.HandpayAmount = _db_jackpot_prize;
            _input_handpay.HandpayDate = _handpay_date;
            _input_handpay.CancelHandpay = false;
            _input_handpay.SiteJackpotAwardedOnTerminalId = _db_awarded_on_terminal_id;

            if (!WSI.Common.Handpays.AutomaticallyPayHandPay(_db_awarded_to_account_id, _input_handpay, _sql_trx))
            {
              return;
            }
          }

          if (!GetSiteJackpotHistory(out _history, _sql_trx))
          {
            return;
          }

          // ACC on 19-NOV-2014 Send notification to TerminalId on player is playing
          try
          {
            SiteJackpotAwarded _wcp_command;
            CardData _card_data;
            Terminal.TerminalInfo _terminal_info;

            _wcp_command = new WSI.Common.SiteJackpotAwarded();

            _wcp_command.JackpotId = 0;
            _wcp_command.JackpotDatetime = _handpay_date;
            _wcp_command.JackpotLevelId = _site_jackpot_db_params.jackpot_index;
            _wcp_command.JackpotLevelName = _site_jackpot_db_params.jackpot_name;
            _wcp_command.JackpotAmountCents = (Int64)_db_jackpot_prize * 100;

            _wcp_command.JackpotMachineName = "";
            if (Terminal.Trx_GetTerminalInfo(_db_awarded_on_terminal_id, out _terminal_info, _sql_trx))
            {
              _wcp_command.JackpotMachineName = _terminal_info.Name;
            }

            _card_data = new CardData();
            if (CardData.DB_CardGetAllData(_db_awarded_to_account_id, _card_data, _sql_trx))
            {
              _wcp_command.WinnerTrackData = _card_data.TrackData;
              _wcp_command.WinnerHolderName = _card_data.PlayerTracking.HolderName3;

              _wcp_command.ParametersShowOnlyToWinner = 0;
              _wcp_command.ParametersWinnerShowTimeSeconds = GeneralParam.GetInt32("SasHost", "SiteJackpotWinnerShowTimeSeconds", 60);
              _wcp_command.ParametersNoWinnerShowTimeSeconds = GeneralParam.GetInt32("SasHost", "SiteJackpotNoWinnerShowTimeSeconds", 60);

              WSI.Common.WcpCommands.InsertWcpCommand(_db_awarded_on_terminal_id, WSI.Common.WCP_CommandCode.SiteJackpotAwarded, _wcp_command.ToXml(), _sql_trx);
            }

          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }

          _sql_trx.Commit();

          Awarded = true;

          // New Accumulated Jackpot
          m_rw_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
          m_site_jackpot[_site_jackpot_db_params.jackpot_index - 1].accumulated = _site_jackpot_db_params.accumulated;
          m_jackpot1[_site_jackpot_db_params.jackpot_index - 1] = _site_jackpot_db_params.accumulated;
          // History
          m_history = _history;
          m_history_read = true;
          m_rw_lock.ReleaseWriterLock();

          return;

        } //if ( _jackpot_mode == WC2_JACKPOT_MODE.Standard )

        if (_jackpot_mode == HANDPAY_PAYMENT_MODE.BONUSING)
        {
          MultiPromos.AccountBalance _to_transfer;
          BonusAwardInfo _info;

          _to_transfer = new MultiPromos.AccountBalance();
          _info = new BonusAwardInfo();

          _to_transfer.Redeemable = _db_jackpot_prize;
          _to_transfer.PromoRedeemable = (Decimal)0;
          _to_transfer.PromoNotRedeemable = (Decimal)0;

          _info.SourceType = (Int32)BonusSource.SiteJackpot;
          _info.TargetTerminalId = _db_awarded_on_terminal_id;
          _info.TargetType = 1;
          _info.SourceBigInt1 = 0;
          _info.SourceBigInt2 = 0;
          _info.SourceInt1 = 0;
          _info.SourceInt2 = 0;
          _info.TargetAccountId = _db_awarded_to_account_id;

          if (!Bonusing.SiteJackpotInsertToBonus(_site_jackpot_db_params.jackpot_index, _to_transfer, _info, _sql_trx))
          {
            return;
          }

          _sql_trx.Commit();

          Awarded = true;

          return;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally
      {
        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            try { _sql_trx.Rollback(); }
            catch { }
          }
          _sql_trx.Dispose();
          _sql_trx = null;
        }

        if (GeneralParam.GetBoolean("SiteJackpot", "LogEnabled", false))
        {
          // SPECIAL
          Log.Message("SiteJackpot.JackpotAward *** END ***");
        }
      }
    }  // JackpotAward

    //------------------------------------------------------------------------------
    // PURPOSE : Get Jackpot Parameters.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //          - Mode
    //          - BlockInterval
    //          - AnimationInterval01
    //          - AnimationInterval02
    //          - RecentHistoryInterval
    //          - MinBlockAmount
    //
    // RETURNS :
    //
    public static void GetSiteJackpotParameters(out String Mode,
                                                out int BlockInterval,
                                                out int AnimationInterval01,
                                                out int AnimationInterval02,
                                                out int RecentHistoryInterval,
                                                out Decimal MinBlockAmount,
                                                out Boolean ShowWinnerName,
                                                out Boolean ShowTerminalName)
    {

      Mode = "BLOCK_NONE";
      BlockInterval = 10;
      AnimationInterval01 = 15;
      AnimationInterval02 = 15;
      MinBlockAmount = 0;
      RecentHistoryInterval = 60;
      ShowWinnerName = false;
      ShowTerminalName = false;

      m_site_jackpot_params_lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
      try
      {
        AnimationInterval01 = m_animation_interval_01;
        AnimationInterval02 = m_animation_interval_02;
        RecentHistoryInterval = m_recent_history_interval;
        ShowWinnerName = m_show_winner_name;
        ShowTerminalName = m_show_terminal_name;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error Getting data from SiteJackpotParameters.");
      }
      finally
      {
        m_site_jackpot_params_lock.ReleaseReaderLock();
      }

    } // GetSiteJackpotParameters

    /// <summary>
    /// Start Thread
    /// </summary>
    public static void Start()
    {
      if (m_thread == null)
      {
        m_thread = new System.Threading.Thread(new System.Threading.ThreadStart(SiteJackpotThread));
        m_thread.Start();
      }
    } // Start
  }
} // WC2_SiteJackpotManager
