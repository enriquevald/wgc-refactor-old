﻿//------------------------------------------------------------------------------
// Copyright © 2017 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_Jackpots.cs
// 
//   DESCRIPTION: Jackpots thread
// 
//        AUTHOR: Alberto Marcos
// 
// CREATION DATE: 27-APR-2017
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 27-APR-2017 AMF    First release.
// 18-JUL-2017 ETP    WIGOS-3515 New Jackpots option menu appears in wrong place and with wrong description.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using WSI.Common;
using WSI.Common.Jackpot;

namespace WSI.WC2
{
  public class WC2_Jackpots
  {
    #region  " Enum "

    #endregion " Enums "

    #region " Atributes "

    #endregion " Atributes "

    #region " Properties "

    #endregion " Properties "

    #region " Public Methotds "

    /// <summary>
    /// Initializes Jackpots thread
    /// </summary>
    public static void Init()
    {
      Thread _thread_contribution;
      Thread _thread_check_conditions;
      Thread _thread_award;
      Thread _thread_fill_meters;

      _thread_contribution = new Thread(JackpotsContribution);
      _thread_contribution.Name = "JackpotsContribution";
      _thread_contribution.Start();

      _thread_check_conditions = new Thread(JackpotsCheckConditions);
      _thread_check_conditions.Name = "JackpotsCheckConditions";
      _thread_check_conditions.Start();

      _thread_award = new Thread(JackpotsAward);
      _thread_award.Name = "JackpotsAward";
      _thread_award.Start();

      _thread_fill_meters = new Thread(JackpotsFillMeters);
      _thread_fill_meters.Name = "JackpotsFillMeters";
      _thread_fill_meters.Start();

    } // Init

    #endregion " Public Methotds "

    #region " Private Methotds "

    /// <summary>
    /// Amounts distribution to Jackpots
    /// </summary>
    private static void JackpotsContribution()
    {
      Int32 _sleep;
      Int32 _idle_sleep;
      Int32 _check_sleep;

      _idle_sleep = 10000;
      _check_sleep = 1000;

      _sleep = GeneralParam.GetInt32("Jackpots", "Contribution.IdleIntervalMiliseconds", 10000);

      while (true)
      {
        try
        {
          Thread.Sleep(_sleep);
          if (!JackpotBusinessLogic.IsJackpotsEnabled())
          {
            _sleep = _idle_sleep;
            break;
          }

          _idle_sleep = GeneralParam.GetInt32("Jackpots", "Contribution.IdleIntervalMiliseconds", 10000);
          _check_sleep = GeneralParam.GetInt32("Jackpots", "Contribution.CheckIntervalMiliseconds", 1000);

          switch (JackpotBusinessLogic.ThreadDistribution())
          {
            case ThreadStatus.IDLE:
              _sleep = _idle_sleep;
              break;

            case ThreadStatus.CHECK:
              _sleep = _check_sleep;
              break;

            case ThreadStatus.ERROR:
              _sleep = _idle_sleep;
              Log.Error("WC2_Jackpots.JackpotsContribution - Thread Error");
              break;

            default:
              _sleep = _idle_sleep;
              break;
          }
        }
        catch (Exception _ex)
        {
          Log.Error("WC2_Jackpots.JackpotsContribution: Exception");
          Log.Exception(_ex);
          _sleep = _idle_sleep;
        }
      }


    } // JackpotsContribution

    /// <summary>
    /// Check conditions for award the Jackpots
    /// </summary>
    private static void JackpotsCheckConditions()
    {
      Int32 _sleep;

      while (true)
      {
        try
        {
          _sleep = GeneralParam.GetInt32("Jackpots", "CheckConditions.CheckIntervalMiliseconds", 10000);

          Thread.Sleep(_sleep);

          if (!JackpotBusinessLogic.IsJackpotsEnabled())
          {
            break;
          }

          if (!JackpotBusinessLogic.ThreadCheckConditions())
          {
            Log.Warning("WC2_Jackpots.JackpotsCheckConditions: Error");
          }
        }
        catch (Exception _ex)
        {
          Log.Error("WC2_Jackpots.JackpotsCheckConditions: Exception");
          Log.Exception(_ex);
        }
      }
    } // JackpotsCheckConditions

    /// <summary>
    /// Thread award jackpot
    /// </summary>
    private static void JackpotsAward()
    {
      Int32 _sleep;

      while (true)
      {
        try
        {
          _sleep = GeneralParam.GetInt32("Jackpots", "Award.CheckIntervalMiliseconds", 10000);

          Thread.Sleep(_sleep);

          if (!JackpotBusinessLogic.IsJackpotsEnabled())
          {
            break;
          }

          if (!JackpotBusinessLogic.ThreadAward())
          {
            Log.Warning("WC2_Jackpots.JackpotsAward: Error");
          }
        }
        catch (Exception _ex)
        {
          Log.Error("WC2_Jackpots.JackpotsAward: Exception");
          Log.Exception(_ex);
        }
      }
    } // JackpotsAward

    /// <summary>
    /// Thread fill jackpot meters
    /// </summary>
    private static void JackpotsFillMeters()
    {
      Int32 _sleep;

      while (true)
      {
        try
        {
          _sleep = GeneralParam.GetInt32("Jackpots", "FillMeters.CheckIntervalMiliseconds", 60000);

          Thread.Sleep(_sleep);

          if (!JackpotBusinessLogic.IsJackpotsEnabled())
          {
            break;
          }

          if (!JackpotBusinessLogic.ThreadFillMeters())
          {
            Log.Warning("WC2_Jackpots.ThreadFillMeters: Error");
          }
        }
        catch (Exception _ex)
        {
          Log.Error("WC2_Jackpots.ThreadFillMeters: Exception");
          Log.Exception(_ex);
        }
      }
    } // JackpotsFillMeters

    #endregion " Private Methotds "

  }
}
