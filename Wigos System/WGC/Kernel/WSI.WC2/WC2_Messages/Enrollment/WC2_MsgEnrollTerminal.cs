//------------------------------------------------------------------------------
// Copyright © 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_MsgEnrollTerminal.cs
// 
//   DESCRIPTION: WC2_MsgEnrollTerminal class
// 
//        AUTHOR: Daniel Rodríguez
// 
// CREATION DATE: 28-JUL-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-JUL-2007 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;

namespace WSI.WC2
{
  public class WC2_MsgEnrollTerminal:IXml
  {
    public String ToXml ()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder ();

      str_builder.Append ("<WC2_MsgEnrollTerminal></WC2_MsgEnrollTerminal>");

      return str_builder.ToString ();
    }

    public void LoadXml (XmlReader Xmlreader)
    {
      if ( Xmlreader.Name != "WC2_MsgEnrollTerminal" )
      {
        throw new WC2_Exception (WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgEnrollTerminal not found in content area");
      }
    }
  }

  public class WC2_MsgEnrollTerminalReply:IXml
  {
    public Int64 LastSequenceId;
    public Int64 LastTransactionId;

    public String ToXml ()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();

      str_builder.Append("<WC2_MsgEnrollTerminalReply><LastSequenceId>");
      str_builder.Append(LastSequenceId.ToString());
      str_builder.Append("</LastSequenceId><LastTransactionId>");
      str_builder.Append(LastTransactionId.ToString());
      str_builder.Append("</LastTransactionId></WC2_MsgEnrollTerminalReply>");

      return str_builder.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WC2_MsgEnrollTerminalReply")
      {
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgEnrollTerminalReply not found in content area");
      }
      else
      {
        LastSequenceId = Int64.Parse(XML.GetValue(reader, "LastSequenceId"));
        LastTransactionId = Int64.Parse(XML.GetValue(reader, "LastTransactionId"));
      }
    }
  }
}
