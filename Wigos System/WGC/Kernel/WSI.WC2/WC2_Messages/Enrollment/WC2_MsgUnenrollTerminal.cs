//------------------------------------------------------------------------------
// Copyright © 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_MsgUnenrollTerminal.cs
// 
//   DESCRIPTION: WC2_MsgUnenrollTerminal class
// 
//        AUTHOR: Daniel Rodríguez
// 
// CREATION DATE: 28-JUL-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-JUL-2008 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;


namespace WSI.WC2
{
  public class WC2_MsgUnenrollTerminal:IXml
  {
    public String ToXml ()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<WC2_MsgUnenrollTerminal></WC2_MsgUnenrollTerminal>");
      return str_builder.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WC2_MsgUnenrollTerminal")
      {
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgUnenrollTerminal not found in content area");
      }
    }
  }

  public class WC2_MsgUnenrollTerminalReply:IXml
  {
    public String ToXml ()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<WC2_MsgUnenrollTerminalReply></WC2_MsgUnenrollTerminalReply>");
      return str_builder.ToString();

    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WC2_MsgUnenrollTerminalReply")
      {
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgUnenrollTerminalReply not found in content area");
      }
    }
  }
}
