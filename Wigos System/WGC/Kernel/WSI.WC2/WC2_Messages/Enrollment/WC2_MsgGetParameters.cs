//------------------------------------------------------------------------------
// Copyright © 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_MsgEnrollTerminal.cs
// 
//   DESCRIPTION: WC2_MsgGetParameters class
// 
//        AUTHOR: Daniel Rodríguez
// 
// CREATION DATE: 28-JUL-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-JUL-2008 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic; 
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;
using System.Collections;

namespace WSI.WC2
{
  public class WC2_MsgGetParameters:IXml
  {
    public int ClientId;
    public int BuildId;
    public String ToXml ()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();

      str_builder.Append("<WC2_MsgGetParameters><SoftwareVersion><ClientId>");
      str_builder.Append(ClientId.ToString());
      str_builder.Append("</ClientId><BuildId>");
      str_builder.Append(BuildId.ToString());
      str_builder.Append("</BuildId></SoftwareVersion></WC2_MsgGetParameters>");
      return str_builder.ToString();
    }


    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WC2_MsgGetParameters")
      {
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgGetParameters not found in content area");
      }
      else
      {
        try
        {
          ClientId = int.Parse(XML.GetValue(reader, "ClientId"));
          BuildId = int.Parse(XML.GetValue(reader, "BuildId"));
        }
        catch
        {
          ClientId = 0;
          BuildId = 0;
        }
      }
    }
  }

  public class WC2_MsgGetParametersReply:IXml
  {
    public int ConnectionTimeout;
    public int KeepAliveInterval;
    public int JackpotRequestInterval;
    public String LocationName;
    public String LocationAddress;
    public int ClientId;
    public int BuildId;
    public int NumLocations;
    public ArrayList Locations = new ArrayList();

    private static int m_last_build_id = 0;
    private static int m_num_received = 0;

    private void DeleteTempFiles()
    {
      String [] _tmp_dir;
      String [] _tmp_files;      
      String _path;

      try
      {
        // AL PARROT! this path is only for windows xp
        _path = Environment.GetEnvironmentVariable("UserProfile") + "\\Local Settings\\Temporary Internet Files\\";
        _tmp_files = System.IO.Directory.GetFiles(_path);
        foreach (String _file in _tmp_files)
        {

          if ( _file.Contains("index.dat") )
          {
            // this file can't be deleted. It's a OS file.
            continue;
          }
          else
          {
            try
            {
              System.IO.File.Delete(_file);
            }
            catch (Exception _ex)
            {
              Log.Exception(_ex);
            }
          }
        }

        _tmp_dir = System.IO.Directory.GetDirectories(_path);

        foreach (String _dir in _tmp_dir)
        {
          _tmp_files = System.IO.Directory.GetFiles(_dir);
          foreach (String _file in _tmp_files)
          {
            if (_file.Contains("index.dat")) 
            {
              // this file can't be deleted. It's a OS file.
              continue;
            }
            else
            {
              try
              {
                System.IO.File.Delete(_file);
              }
              catch(Exception _ex)
              {
                Log.Exception(_ex);
              }
            }
          }
          try
          {
            System.IO.Directory.Delete(_dir, true);
          }
          catch (Exception _ex)
          {
            Log.Exception(_ex);
          }
        }        
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
    }

    public WC2_MsgGetParametersReply ()
    {
      ConnectionTimeout = 60; // secs
      KeepAliveInterval = 30; // secs      
    }

    public String ToXml ()
    {
      StringBuilder str_builder;
      int idx_locations;

      str_builder = new StringBuilder();

      str_builder.Append("<WC2_MsgGetParametersReply><ConnectionTimeout>");
      str_builder.Append(ConnectionTimeout.ToString());
      str_builder.Append("</ConnectionTimeout><KeepAliveInterval>");
      str_builder.Append(KeepAliveInterval.ToString());
      str_builder.Append("</KeepAliveInterval><JackpotRequestInterval>");
      str_builder.Append(JackpotRequestInterval.ToString());
      str_builder.Append("</JackpotRequestInterval><SoftwareVersion><ClientId>");
      str_builder.Append(ClientId.ToString());
      str_builder.Append("</ClientId><BuildId>");
      str_builder.Append(BuildId.ToString());
      str_builder.Append("</BuildId><Locations><NumLocations>");
      str_builder.Append(NumLocations);
      str_builder.Append("</NumLocations>");
      for (idx_locations = 0 ; idx_locations < NumLocations ; idx_locations++){
        str_builder.Append("<Location>");
        str_builder.Append((string)Locations[idx_locations]);
        str_builder.Append("</Location>");
      }
      str_builder.Append("</Locations></SoftwareVersion></WC2_MsgGetParametersReply>");

      return str_builder.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      String Location;
      if (reader.Name != "WC2_MsgGetParametersReply")
      {
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgGetParametersReply not found in content area");
      }
      else
      {
        ConnectionTimeout = int.Parse(XML.GetValue(reader, "ConnectionTimeout"));
        KeepAliveInterval = int.Parse(XML.GetValue(reader, "KeepAliveInterval"));
        JackpotRequestInterval = int.Parse(XML.GetValue(reader, "JackpotRequestInterval"));
        ClientId = int.Parse(XML.GetValue(reader, "ClientId"));
        BuildId = int.Parse(XML.GetValue(reader, "BuildId"));
        NumLocations = int.Parse(XML.GetValue(reader, "NumLocations"));
        if (NumLocations > 0)
        {
          Locations = new ArrayList();
          while (((reader.Name != "Locations") || (reader.NodeType != XmlNodeType.EndElement)) && !reader.EOF)
          {
            Location = XML.GetValue(reader, "Location");
            Locations.Add(Location);
            reader.Read(); //After this "read" the reader will be at the end of Location
            reader.Read(); //After this "read" the reader will be at the end of Repositories or the beginning of new a Location
          }
        }

        m_num_received++;
        if ( m_num_received % 60 == 0 || m_last_build_id != BuildId )
        {
          // Delete files every hour or on version update
          m_last_build_id = BuildId;
          DeleteTempFiles ();
        }
      }
    }
  }
}
