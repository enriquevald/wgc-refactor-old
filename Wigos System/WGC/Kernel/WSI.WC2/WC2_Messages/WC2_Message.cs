//------------------------------------------------------------------------------
// Copyright © 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_Mesage.cs
// 
//   DESCRIPTION: WC2_Mesage class
// 
//        AUTHOR: Daniel Rodríguez
// 
// CREATION DATE: 28-JUL-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-JUL-2008 DRG    First release.
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using WSI.Common;


namespace WSI.WC2
{
  public interface IXml
  {
    String ToXml();
    void LoadXml(XmlReader Xml);
  }

  public class WC2_Exception : Exception
  {
    WC2_ResponseCodes response_code;
    String response_code_text;

    public WC2_ResponseCodes ResponseCode
    {
      get
      {
        return response_code;
      }
    }
    public String ResponseCodeText
    {
      get
      {
        return response_code_text;
      }
    }

    public WC2_Exception (WC2_ResponseCodes Value)
    {
      response_code = Value;
    }

    public WC2_Exception (WC2_ResponseCodes Code, String Text):base(Text)
    {
      response_code = Code;
      response_code_text = Text;
    }

    public WC2_Exception (WC2_ResponseCodes Code, String Text, Exception InnerException): base (Text, InnerException)
    {
      response_code = Code;
      response_code_text = Text;
    }
  }


  public partial class WC2_Message
  {
    public WC2_MsgHeader MsgHeader;
    public IXml MsgContent;

    public Int64 TransactionId
    {
      get
      {
        switch (MsgHeader.MsgType)
        {
          case WC2_MsgTypes.WC2_MsgCancelPlay:
            return ((WC2_MsgCancelPlay)MsgContent).TransactionId;

          case WC2_MsgTypes.WC2_MsgPlay:
            return ((WC2_MsgPlay)MsgContent).TransactionId;

          default:
            return 0;
        }
      }
    }

    private WC2_Message ()
    {
    }


    public static WC2_Message CreateMessage(String Xml)
    {
      WC2_Message WC2_message;

      WC2_message = null;

      try
      {
        XmlReader xmlreader;

        bool found;

        WC2_MsgHeader hdr;
        hdr = new WC2_MsgHeader();
        System.IO.StringReader textreader = new System.IO.StringReader(Xml);
        xmlreader = XmlReader.Create(textreader);

        //Loads the message header
        hdr.LoadXml(xmlreader);

        if ( hdr.MsgType == WC2_MsgTypes.WC2_MsgUnknown )
        {
          return null;
        }

        //Creates the message if the message header is correct.
        WC2_message = CreateMessage(hdr.MsgType);
        //Assign the header to the current message.
        WC2_message.MsgHeader = hdr;

        //Put the xmlreader at the beginning of the Message Content
        found = false;
        while (xmlreader.Read() && !found)
        {
          if ((xmlreader.Name == "WC2_MsgContent") && (xmlreader.NodeType.Equals(XmlNodeType.Element)))
          {
            found = true;
          }
        }
        if (found)
        {
          WC2_message.MsgContent.LoadXml(xmlreader);
          if (WC2_message.MsgContent == null)
          {
            throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag not found: " + WC2_message.MsgHeader.MsgType.ToString());
          }
        }
        else
        {
          throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag not found: " + WC2_message.MsgHeader.MsgType.ToString());
        }
        xmlreader.Close();
      }
      catch (WC2_Exception WC2_ex)
      {
        throw WC2_ex;
      }
      catch (Exception ex)
      {
        // Format Error in message
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, ex.Message, ex);
      }
      return WC2_message;
    }


    public static WC2_Message CreateMessage(WC2_MsgTypes MsgType)
    {
      WC2_Message msg;
      IXml content;

      Assembly assembly;
      String type_name;
      Type target_type;

      msg = new WC2_Message();

      msg.MsgHeader = new WC2_MsgHeader();
      msg.MsgHeader.MsgType = MsgType;

      // The Power of Reflection
      content = null;

      type_name = "WSI.WC2." + MsgType.ToString();

      assembly = Assembly.GetExecutingAssembly();
      target_type = assembly.GetType(type_name);

      Type[] types = new Type[0];
      ConstructorInfo info = target_type.GetConstructor(types);
      content = (IXml)info.Invoke(null);

      msg.MsgContent = content;

      return msg;
    }

    public String ToXml ()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();
      String xml_header;
      String xml_content;

      xml_header = MsgHeader.ToXml ();
      xml_content = MsgContent.ToXml ();

      str_builder.Append("<WC2_Message>");
      str_builder.Append(xml_header);
      str_builder.Append("<WC2_MsgContent>");
      str_builder.Append(xml_content);
      str_builder.Append("</WC2_MsgContent></WC2_Message>");

      return str_builder.ToString();
    }
  }

}
