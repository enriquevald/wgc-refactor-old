using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Xml;
using WSI.Common;
using System.IO;

namespace WSI.WC2
{
  public static class Protocol
  {
    public const int PORT_NUMBER = 65534;           // Listener port number
    public const int SERVICE_PORT_NUMBER = 13001;   // Service port number
    public const String PROTOCOL_NAME = "WC2";
    public const String SERVICE_NAME = "WC2_Service";  // Service Name
  }
  
  public class WC2_Card
  {
    public String CardId;
    public String CardType;
    public String CardNumbers;

    public WC2_Card()
    {
    }

    public WC2_Card(String CardXml)
    {
      System.IO.StringReader text_reader;
      XmlReader reader;

      text_reader = new System.IO.StringReader(CardXml);
      reader = XmlReader.Create(text_reader);

      CardId = XML.GetValue(reader, "CardId");
      CardType = XML.GetValue(reader, "CardType");
      CardNumbers = XML.GetValue(reader, "CardNumbers");
    }

    public int[] GetNumbers()
    {
      int[] numbers;
      String[] numbers_str;
      int idx_number;

      numbers_str = CardNumbers.Split(' ');
      numbers = new int[numbers_str.Length];
      for (idx_number = 0; idx_number < numbers_str.Length; idx_number++)
      {
        numbers[idx_number] = Convert.ToInt32(numbers_str[idx_number]);
      }

      return numbers;
    }
  }
  public class WC2_Pattern
  {
    public int Order;
    public Int64 Min;
    public Int64 Max;
    public int NumPositions;
    public String PositionList;
    public String PositionListProtocol;

    public string GetStringList()
    {
      string list_numbers = "";
      int idx_number;
      int idx_binary;
      string binary_positions;

      idx_number = 0;
      binary_positions = Convert.ToString(Convert.ToInt64(PositionList, 16), 2);
      for (idx_binary = binary_positions.Length - 1; idx_binary >= 0; idx_binary--)
      {
        if (binary_positions[idx_binary].Equals('1'))
        {
          idx_number  = (binary_positions.Length - idx_binary - 1);
          list_numbers += " " + idx_number.ToString();
        }
      }
      return list_numbers.Trim();
    }

    public int[] GetNumbers()
    {
      int[] numbers;
      int idx_number;
      string binary_positions;
      int idx_binary;

      idx_number = 0;
      numbers = new int[NumPositions];
      binary_positions = Convert.ToString(Convert.ToInt64(PositionList, 16), 2);
      for (idx_binary = binary_positions.Length - 1; idx_binary >= 0; idx_binary--)
      {
        if (binary_positions[idx_binary].Equals('1'))
        {
          numbers[idx_number] = binary_positions.Length - idx_binary - 1;
          idx_number++;
        }
      }
      return numbers;
    }

    /// <summary>
    /// Checks if the card contains this prize having a proposal of draw with the current pattern
    /// </summary>
    /// <param name="DrawBalls"></param>
    /// <param name="CardNumbers"></param>
    /// <param name="prize"></param>
    /// <returns></returns>
    public bool Match(ArrayList DrawBalls, int[] CardNumbers)
    {
      int[] positions = GetNumbers();
      //true because if the pattern is 0 (NumPositions = 0) it must return true.
      bool matching = true;
      for (int i = 0; i < NumPositions; i++)
      {
        matching = false;
        for (int j = 0; j < DrawBalls.Count; j++)
        {
          if ((CardNumbers[positions[i]]) == (int)DrawBalls[j])
          {
            matching = true;
            break;
          }
        }
        if (!matching)
        {
          break;
        }
      }
      return matching;
    }
  }

  public class WC2_Patterns
  {
    static public ArrayList p_list = new ArrayList();
    static public int NUM_PATTERNS = 64;

    /// <summary>
    /// Having a draw, a card and a prize this function determines if 
    /// this prize or lesser ones matches the draw with this card
    /// 
    /// IMPORTANT: The pattern list must be sorted ascending from MIN = 0 to MIN = 10000
    /// </summary>
    /// <param name="DrawBalls"></param>
    /// <param name="CardNumbers"></param>
    /// <param name="prize"></param>
    /// <returns></returns>
    static public bool CheckPrize(ArrayList DrawBalls, int[] CardNumbers, Decimal Prize)
    {
      bool match_found;
      match_found = false;
      foreach (WC2_Pattern patt in p_list) 
      {
        if (Prize <= patt.Max) // Only checks the patterns with the maximum prize greatter than the desired prize becaue lesser prizes doesn't affect to the final result
        {
          if (patt.Match(DrawBalls, CardNumbers)) //Returns true if the pattern is 0.
          {
            if (!match_found) //We know that the pattern list is sorted ascending, so if we found a new match, we must return false
            {
              if (((patt.Max >= Prize) && (Prize > patt.Min))
                ||(Prize == 0 && patt.Max == 0))//we must control if the current match is the correct match and not greatter
              {
                match_found = true;
              }
            }
            else // The current match is equal or greatter we must return false
            {
              return false;
            }
          }
        }
      }
      //No more than one match found, or (if the prize is 0) there are no matches.
      return true;
    }


    static public WC2_Pattern GetPattern(ArrayList Draw, int[] CardNumbers, Decimal Prize)
    {
      WC2_Pattern result_pattern;
      int idx_patterns;

      result_pattern = new WC2_Pattern();

      for (idx_patterns = p_list.Count - 1; idx_patterns >= 0; idx_patterns--)
      {
        result_pattern = (WC2_Pattern)p_list[idx_patterns];
        if (result_pattern.Match(Draw, CardNumbers))
        {
          return result_pattern;
        }
      }
      return (WC2_Pattern)p_list[0];
    }



    /// <summary>
    /// Randomizes a pattern list order
    /// </summary>
    /// <param name="pattern"></param>
    /// <returns></returns>
    static public ArrayList Rearrange(ArrayList pattern_list)
    {
      int idx_pattern;
      int current_pattern;
      int pattern_count;

      ArrayList reordered_pattern_list;
      Random pattern;
      pattern_count = pattern_list.Count;
      reordered_pattern_list = new ArrayList();
      pattern = new Random();
      for (idx_pattern = 0; idx_pattern < pattern_count; idx_pattern++)
      {
        current_pattern = pattern.Next(pattern_list.Count);
        reordered_pattern_list.Add(pattern_list[current_pattern]);
        pattern_list.Remove(pattern_list[current_pattern]);
      }
      return reordered_pattern_list;
    }


    /// <summary>
    /// Returns an array of patterns where every pattern fits the prize
    /// </summary>
    /// <param name="prize"></param>
    /// <returns></returns>
    static public ArrayList GetByPrize(Decimal Prize)
    {
      ArrayList winning_patterns = new ArrayList();
      foreach (WC2_Pattern patt in p_list)
      {
        if (((Prize > patt.Min) && (Prize <= patt.Max))
          || ((Prize == 0) && (Prize == patt.Max) && (Prize == patt.Min)))
        {
          winning_patterns.Add(patt);
        }
      }
      return Rearrange(winning_patterns);
    }


    /// <summary>
    /// Returns an array of patterns where every pattern fits the prize
    /// </summary>
    /// <param name="prize"></param>
    /// <returns></returns>
    static public WC2_Pattern GetByPrizeOnePlay(Decimal Prize)
    {
      ArrayList winning_patterns = new ArrayList();
      foreach (WC2_Pattern patt in p_list)
      {
        if (((Prize > patt.Min) && (Prize <= patt.Max))
          || ((Prize == 0) && (Prize == patt.Max) && (Prize == patt.Min)))
        {
          winning_patterns.Add(patt);
        }
      }
      return (WC2_Pattern)Rearrange(winning_patterns)[0];
    }
  }


  public class WC2_CardPrize
  {
    public int ResultIndex;
    public Int64 PrizeCredits;
    public int NumPositions;
    public String PositionList;

    public int[] GetPositions()
    {
      int[] positions;
      String[] positions_str;
      int idx_position;
      try
      {
        positions_str = PositionList.Split(' ');
        positions = new int[positions_str.Length];
        for (idx_position = 0; idx_position < positions_str.Length; idx_position++)
        {
          positions[idx_position] = Convert.ToInt32(positions_str[idx_position]);
        }
        return positions;
      }
      catch
      {
        Exception ex = new Exception("Error getting positions from position list");
        return null;
      }
    }
  }

  public class WC2_Prize
  {
    public String CardId;
    public String CardType;
    public int NumCardPrizes;
    public ArrayList CardPrizes; // typeof WC2_CardPrize 

    internal String ToXml()
    {
      MemoryStream  text_stream   = new MemoryStream();
      StreamReader  stream_reader;
      XmlTextWriter xml_writer    = new XmlTextWriter(text_stream, Encoding.UTF8);

      xml_writer.WriteStartElement("Prize");
      {
        xml_writer.WriteElementString("CardId", CardId.ToString());
        xml_writer.WriteElementString("CardType", CardType.ToString());

        xml_writer.WriteStartElement("CardPrizeList");
        {
          xml_writer.WriteElementString("NumCardPrizes", CardPrizes.Count.ToString());

          foreach (WC2_CardPrize card_prize in CardPrizes)
          {
            xml_writer.WriteStartElement("CardPrize");
            {
              xml_writer.WriteElementString("ResultIndex", card_prize.ResultIndex.ToString());
              xml_writer.WriteElementString("PrizeCredits", card_prize.PrizeCredits.ToString());

              xml_writer.WriteStartElement("PrizePattern");
              {
                xml_writer.WriteElementString("NumPositions", card_prize.NumPositions.ToString());
                xml_writer.WriteElementString("PositionList", card_prize.PositionList);
              }
              xml_writer.WriteEndElement();
            }
            xml_writer.WriteEndElement();
          }
        }
        xml_writer.WriteEndElement();
      }
      xml_writer.WriteEndElement();

      text_stream.Flush();
      text_stream.Position = 0;

      stream_reader = new StreamReader(text_stream);

      return stream_reader.ReadToEnd();
    }// ToXml()  


  
  }

  public class WC2_Draw
  {
    public Int32 NumBalls = 0;
    public Int32 MaxBallsForPrize = 0;
    public Int32 MinBallNumber = 0;
    public Int32 MaxBallNumber = 0;
    public String BallList = "";  
    
    public int[] GetBalls()
    {
      int[] numbers;
      String[] numbers_str;
      int idx_number;

      numbers_str = BallList.Split(' ');
      numbers = new int[numbers_str.Length];
      for (idx_number = 0; idx_number < numbers_str.Length; idx_number++)
      {
        try
        {
          numbers[idx_number] = Convert.ToInt32(numbers_str[idx_number]);
        }
        catch
        {
          return numbers;
        }

      }

      return numbers;
    } 

    internal String ToXml()
    {

      String OuterXml;
      string temp_list;
      int[] ball_list;
      ball_list = GetBalls();

      OuterXml = "";
      OuterXml += "<BallsList>";
      OuterXml += "<NumBalls>";
      OuterXml += NumBalls.ToString();
      OuterXml += "</NumBalls>";
      OuterXml += "<Balls>";
      temp_list = "";
      foreach (int ball in ball_list)
      {
        temp_list += " " + ball.ToString("00");
      }
      OuterXml += temp_list.Trim();
      OuterXml += "</Balls>";
      OuterXml += "</BallsList>";
      return OuterXml;
    }
  }

}
