//------------------------------------------------------------------------------
// Copyright © 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_MsgKeepAlive.cs
// 
//   DESCRIPTION: WC2_MsgKeepAlive class
// 
//        AUTHOR: Daniel Rodríguez
// 
// CREATION DATE: 28-JUL-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-JUL-2008 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using WSI.Common;

namespace WSI.WC2
{
    public class WC2_MsgKeepAlive : IXml
    {
        public String ToXml()
        {
          StringBuilder str_builder;

          str_builder = new StringBuilder();
          str_builder.Append("<WC2_MsgKeepAlive></WC2_MsgKeepAlive>");
          return str_builder.ToString();
        }

      public void LoadXml(XmlReader reader)
      {
        if (reader.Name != "WC2_MsgKeepAlive")
        {
          throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgKeepAlive not found in content area");
        }
      }

    }

    public class WC2_MsgKeepAliveReply : IXml
    {
        public String ToXml()
        {
          StringBuilder str_builder;

          str_builder = new StringBuilder();
          str_builder.Append("<WC2_MsgKeepAliveReply></WC2_MsgKeepAliveReply>");
          return str_builder.ToString();
        }

      public void LoadXml(XmlReader reader)
      {
        if (reader.Name != "WC2_MsgKeepAliveReply")
        {
          throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgKeepAliveReply not found in content area");
        }
      }
    }
}
