//------------------------------------------------------------------------------
// Copyright © 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_MsgKeepAlive.cs
// 
//   DESCRIPTION: WC2_MsgKeepAlive class
// 
//        AUTHOR: Daniel Rodríguez
// 
// CREATION DATE: 28-JUL-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-JUL-2008 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Net;
using WSI.Common;


namespace WSI.WC2
{
  public class WC2_MsgRequestService:IXml
  {
    public  String     ServiceName;
    public  IPEndPoint ReplyToUDPAddress;


    public String ToXml ()
    {

      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<WC2_MsgRequestService><ServiceName>");
      str_builder.Append(ServiceName);
      str_builder.Append("</ServiceName><ReplyToUDPAddress>");
      str_builder.Append(ReplyToUDPAddress.ToString());
      str_builder.Append("</ReplyToUDPAddress></WC2_MsgRequestService>");

      return str_builder.ToString();
    }


    public void LoadXml (XmlReader reader)
    {
      String reply_to_str;
      int idx;
      IPAddress ip;
      int port;
      if (reader.Name != "WC2_MsgRequestService")
      {
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgRequestService not found in content area");
      }
      else
      {
        ServiceName = XML.GetValue(reader, "ServiceName");
        reply_to_str = XML.GetValue(reader, "ReplyToUDPAddress");
        idx = reply_to_str.IndexOf(':');
        ip = IPAddress.Parse(reply_to_str.Substring(0, idx));
        port = int.Parse(reply_to_str.Substring(1 + idx));
        ReplyToUDPAddress = new IPEndPoint(ip, port);
      }
    }
  }

  public class WC2_MsgRequestServiceReply:IXml
  {
    public IPEndPoint ServiceAddress;

    public String ToXml ()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<WC2_MsgRequestServiceReply><ServiceAddress>");
      if (ServiceAddress != null)
      {
        str_builder.Append(ServiceAddress.ToString());
      }
      str_builder.Append("</ServiceAddress></WC2_MsgRequestServiceReply>");

      return str_builder.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      String ipe_str;
      int idx;
      IPAddress ip;
      int port;
      
      if (reader.Name != "WC2_MsgRequestServiceReply")
      {
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgRequestServiceReply not found in content area");
      }
      else
      {
        ipe_str = XML.GetValue(reader, "ServiceAddress");
        idx = ipe_str.IndexOf(':');
        ip = IPAddress.Parse(ipe_str.Substring(0, idx));
        port = int.Parse(ipe_str.Substring(1 + idx));
        ServiceAddress = new IPEndPoint(ip, port);
      }
    }
  }
}
