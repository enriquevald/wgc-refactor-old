//------------------------------------------------------------------------------
// Copyright © 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_MsgGetPatterns.cs
// 
//   DESCRIPTION: WC2_MsgGetPatterns class
// 
//        AUTHOR: Daniel Rodríguez
// 
// CREATION DATE: 28-JUL-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-JUL-2008 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.WC2
{
  public class WC2_MsgGetPatterns : IXml
  {
    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();

      str_builder.Append("<WC2_MsgGetPatterns>");
      str_builder.Append("</WC2_MsgGetPatterns>");
      return str_builder.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WC2_MsgGetPatterns")
      {
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgGetPatterns not found in content area");
      }
    }
  }

  public class WC2_MsgGetPatternsReply : IXml
  {
    public int NumPatterns;
    public ArrayList Patterns = new ArrayList();

    public String ToXml()
    {
      WC2_Pattern Pattern;
      StringBuilder str_builder;

      str_builder = new StringBuilder();

      str_builder.Append("<WC2_MsgGetPatternsReply><PatternList><NumPatterns>");
      str_builder.Append(NumPatterns);
      str_builder.Append("</NumPatterns>");
      for (int idx_patterns = 0; idx_patterns < Patterns.Count; idx_patterns++)
      {
        Pattern = (WC2_Pattern)Patterns[idx_patterns];
        str_builder.Append("<Pattern><Order>");
        str_builder.Append(Pattern.Order);
        str_builder.Append("</Order><Prize><Min>");
        str_builder.Append(Pattern.Min);
        str_builder.Append("</Min><Max>");
        str_builder.Append(Pattern.Max);
        str_builder.Append("</Max></Prize><NumPositions>");
        str_builder.Append(Pattern.NumPositions);
        str_builder.Append("</NumPositions><PositionList>");
        str_builder.Append(Pattern.PositionListProtocol);
        str_builder.Append("</PositionList></Pattern>");
      }

      str_builder.Append("</PatternList></WC2_MsgGetPatternsReply>");
      return str_builder.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      WC2_Pattern Pattern;
      if (reader.Name != "WC2_MsgGetPatternsReply")
      {
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgGetPatternsReply not found in content area");
      }
      else
      {
        NumPatterns = Int32.Parse(XML.GetValue(reader, "NumPatterns"));
        if (NumPatterns == 0)
        {
          return;
        }

        while (((reader.Name != "PatternList") || (reader.NodeType != XmlNodeType.EndElement)) && !reader.EOF)
        {
          Pattern = new WC2_Pattern();
          Pattern.Order = int.Parse(XML.GetValue(reader, "Order"));
          Pattern.Min = Int64.Parse(XML.GetValue(reader, "Min"));
          Pattern.Max = Int64.Parse(XML.GetValue(reader, "Max"));
          Pattern.NumPositions = int.Parse(XML.GetValue(reader, "NumPositions"));
          Pattern.PositionListProtocol = XML.GetValue(reader, "PositionList");
          Patterns.Add(Pattern);
          reader.Read(); //After this "read" the reader will be at the end of PositionList
          reader.Read(); //After this "read" the reader will be at the end of Pattern
          reader.Read(); //After this "read" the reader will be at the end of PatternList or the beginning of new a Pattern

          if (Patterns.Count >= NumPatterns)
          {
            break;
          }
        }

        if (Patterns.Count != NumPatterns)
        {
          throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgGetPatternsReply wrong NumPatterns");
        }
      }
    }
  }
}
