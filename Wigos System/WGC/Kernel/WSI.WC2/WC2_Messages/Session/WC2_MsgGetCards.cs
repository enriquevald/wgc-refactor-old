//------------------------------------------------------------------------------
// Copyright © 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_MsgGetCards.cs
// 
//   DESCRIPTION: WC2_MsgGetCards class
// 
//        AUTHOR: Daniel Rodríguez
// 
// CREATION DATE: 28-JUL-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-JUL-2008 DRG    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;
using System.Collections;

namespace WSI.WC2
{

  public class WC2_MsgGetCards:IXml
  {
    public int NumCards;
    public String CardType;
    public ArrayList Cards = new ArrayList();

    public String ToXml ()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();

      str_builder.Append("<WC2_MsgGetCards><NumCards>");
      str_builder.Append(NumCards);
      str_builder.Append("</NumCards><CardType>");
      str_builder.Append(CardType);
      str_builder.Append("</CardType></WC2_MsgGetCards>");
      return str_builder.ToString();
    }

    public void LoadXml(XmlReader reader)
    {

      if (reader.Name != "WC2_MsgGetCards")
      {
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgGetCards not found in content area");
      }
      else
      {
        NumCards = int.Parse(XML.GetValue(reader, "NumCards"));
        CardType = XML.GetValue(reader, "CardType");
      }
    }
  }

  public class WC2_MsgGetCardsReply:IXml
  {
    public int NumCards;
    public ArrayList Cards = new ArrayList();

    public String ToXml ()
    {

      WC2_Card Card;
      StringBuilder str_builder;

      str_builder = new StringBuilder();

      str_builder.Append("<WC2_MsgGetCardsReply><CardList><NumCards>");
      str_builder.Append(NumCards);
      str_builder.Append("</NumCards>");
      for (int idx_cards = 0; idx_cards < Cards.Count; idx_cards++)
      {
        Card = (WC2_Card)Cards[idx_cards];
        str_builder.Append("<Card><CardId>");
        str_builder.Append(Card.CardId);
        str_builder.Append("</CardId><CardType>");
        str_builder.Append(Card.CardType);
        str_builder.Append("</CardType><CardNumbers>");
        str_builder.Append(Card.CardNumbers);
        str_builder.Append("</CardNumbers></Card>");
      }
      
      str_builder.Append("</CardList></WC2_MsgGetCardsReply>");
      return str_builder.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      WC2_Card Card;

      if (reader.Name != "WC2_MsgGetCardsReply")
      {
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgGetCardsReply not found in content area");
      }
      else
      {

        NumCards = int.Parse(XML.GetValue(reader, "NumCards"));
        if ( NumCards == 0 )
        {
          return;
        }

        while (((reader.Name != "CardList") || (reader.NodeType != XmlNodeType.EndElement)) && !reader.EOF)
        {
          Card = new WC2_Card();
          Card.CardId = XML.GetValue(reader, "CardId");
          Card.CardType = XML.GetValue(reader, "CardType");
          Card.CardNumbers = XML.GetValue(reader, "CardNumbers");
          Cards.Add(Card);
          reader.Read(); //After this "read" the reader will be at the end of CardNumbers
          reader.Read(); //After this "read" the reader will be at the end of Card
          reader.Read(); //After this "read" the reader will be at the end of Cards or the beginning of new a Card

          if (Cards.Count >= NumCards)
          {
            break;
          }
        }

        if (Cards.Count != NumCards)
        {
          throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgGetCardsReply wrong NumCards");
        }
      }
    }
  }
}
