//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems Ltd.
//------------------------------------------------------------------------------
//
//   MODULE NAME : WC2_MsgSiteJackpotInfo.cs
//
//   DESCRIPTION : WC2_MsgSiteJackpotInfo class
//
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 17-NOV-2010 TJG    First release.
// 04-APR-2012 ACC    Add Player & Terminal Name into history Jackpots.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.WC2
{
  public class WC2_SiteJackpot
  {
    public int Index;
    public String Name;
    public Decimal Amount;
    public Decimal MinimumBetAmount;
  }

  public class WC2_SiteJackpot_History
  {
    public int Index;
    public String Name;
    public Decimal Amount;
    public DateTime Date;
  }

  public class WC2_MsgSiteJackpotInfo : IXml
  {
    public String TerminalId;
    public String GameId;
    public int Denomination;
    public Int64 TotalBetCredits;

    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();

      str_builder.Append("<WC2_MsgSiteJackpotInfo><TerminalId>");
      str_builder.Append(XML.StringToXmlValue(TerminalId));
      str_builder.Append("</TerminalId><GameId>");
      str_builder.Append(XML.StringToXmlValue(GameId));
      str_builder.Append("</GameId><Denomination>");
      str_builder.Append(Denomination);
      str_builder.Append("</Denomination><TotalBetCredits>");
      str_builder.Append(TotalBetCredits);
      str_builder.Append ("</TotalBetCredits></WC2_MsgSiteJackpotInfo>");
      return str_builder.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if ( reader.Name != "WC2_MsgSiteJackpotInfo" )
      {
        throw new WC2_Exception (WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgSiteJackpotInfo not found in content area");
      }
      else
      {
        TerminalId = XML.GetValue(reader, "TerminalId");
        GameId = XML.GetValue(reader, "GameId");
        Denomination = int.Parse(XML.GetValue(reader, "Denomination"));
        TotalBetCredits = Int64.Parse(XML.GetValue(reader, "TotalBetCredits"));
      }
    }
  }

  public class WC2_MsgSiteJackpotInfoReply : IXml
  {
    public int NumJackpots;
    public ArrayList Jackpots = new ArrayList();
    public int NumJackpotsHistory;
    public ArrayList JackpotsHistory = new ArrayList();
    public int NumMessages;
    public ArrayList Messages = new ArrayList();
    public String Mode;
    public int BlockInterval;
    public int RecentHistoryInterval;
    public int AnimationInterval01;
    public int AnimationInterval02;
    public Decimal MinBlockAmount;

    public String ToXml()
    {
      WC2_Jackpot Jackpot;
      WC2_Jackpot_History JackpotHistory;
      String Message;
      StringBuilder str_builder;
      string str;

      str_builder = new StringBuilder();

      str_builder.Append ("<WC2_MsgSiteJackpotInfoReply><JackpotList><NumJackpots>");
      str_builder.Append(NumJackpots);
      str_builder.Append("</NumJackpots>");
      for (int idx_jackpot = 0; idx_jackpot < Jackpots.Count; idx_jackpot++)
      {
        Jackpot = (WC2_Jackpot)Jackpots[idx_jackpot];
        str_builder.Append("<JackpotInfo><Index>");
        str_builder.Append(Jackpot.Index);
        str_builder.Append("</Index><Name>");
        str_builder.Append(XML.StringToXmlValue(Jackpot.Name));
        str_builder.Append("</Name><Amount>");
        str_builder.Append(XML.DoubleToXmlValue((double) (Decimal.Truncate(Jackpot.Amount * 100) / 100)));
        str_builder.Append("</Amount><MinimumBetAmount>");
        str_builder.Append(XML.DoubleToXmlValue((double)Jackpot.MinimumBetAmount));
        str_builder.Append("</MinimumBetAmount></JackpotInfo>");
      }
      str_builder.Append("</JackpotList><History><NumJackpotsHistory>");
      str_builder.Append(NumJackpotsHistory);
      str_builder.Append("</NumJackpotsHistory>");
      for (int idx_jackpot = 0; idx_jackpot < JackpotsHistory.Count; idx_jackpot++)
      {
        JackpotHistory = (WC2_Jackpot_History)JackpotsHistory[idx_jackpot];

        str = XML.XmlDateTimeString(JackpotHistory.Date);

        str_builder.Append("<JackpotAwarded><Index>");
        str_builder.Append(JackpotHistory.Index);
        str_builder.Append("</Index>");
        str_builder.Append("<Name>");
        str_builder.Append(XML.StringToXmlValue(JackpotHistory.Name));
        str_builder.Append("</Name>");
        str_builder.Append("<Amount>");
        str_builder.Append(XML.DoubleToXmlValue((double) (Decimal.Truncate(JackpotHistory.Amount * 100) / 100)));
        str_builder.Append("</Amount>");
        str_builder.Append("<Date>");
        str_builder.Append(str);
        str_builder.Append("</Date>");

        // 04-APR-2012 Add Player & Terminal Name
        str_builder.Append("<PlayerName>");
        str_builder.Append(XML.StringToXmlValue(JackpotHistory.PlayerName));
        str_builder.Append("</PlayerName>");
        str_builder.Append("<TerminalName>");
        str_builder.Append(XML.StringToXmlValue(JackpotHistory.TerminalName));
        str_builder.Append("</TerminalName>");

        str_builder.Append("</JackpotAwarded>");
      }

      str_builder.Append("</History><Messages><NumMessages>");
      str_builder.Append(NumMessages);
      str_builder.Append("</NumMessages>");
      for (int idx_messages = 0; idx_messages < Messages.Count; idx_messages++)
      {
        Message = (String)Messages[idx_messages];
        str_builder.Append("<Message>");
        str_builder.Append(XML.StringToXmlValue(Message));
        str_builder.Append("</Message>");
      }

      str_builder.Append("</Messages><Parameters><Blocking><Mode>");
      str_builder.Append(Mode);
      str_builder.Append("</Mode><BlockInterval>");
      str_builder.Append(BlockInterval);
      str_builder.Append("</BlockInterval><MinBlockAmount>");
      str_builder.Append(XML.DoubleToXmlValue((double)MinBlockAmount));
      str_builder.Append("</MinBlockAmount></Blocking><Animation><RecentHistoryInterval>");
      str_builder.Append(RecentHistoryInterval);
      str_builder.Append ("</RecentHistoryInterval>");

      str_builder.Append ("<AnimationInterval01>");
      str_builder.Append (AnimationInterval01);
      str_builder.Append ("</AnimationInterval01>");

      str_builder.Append ("<AnimationInterval02>");
      str_builder.Append (AnimationInterval02);
      str_builder.Append ("</AnimationInterval02>");

      str_builder.Append ("</Animation></Parameters></WC2_MsgSiteJackpotInfoReply>");

      return str_builder.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      WC2_Jackpot Jackpot;
      WC2_Jackpot_History JackpotHistory;
      DateTime JackpotHistoryDate;
      String Message;


      if ( reader.Name != "WC2_MsgSiteJackpotInfoReply" )
      {
        throw new WC2_Exception (WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgSiteJackpotInfoReply not found in content area");
      }
      else
      {
        NumJackpots = Int32.Parse(XML.GetValue(reader, "NumJackpots"));
        if (NumJackpots != 0)
        {
          while (((reader.Name != "JackpotList") || (reader.NodeType != XmlNodeType.EndElement)) && !reader.EOF)
          {
            Jackpot = new WC2_Jackpot();
            Jackpot.Index = int.Parse(XML.GetValue(reader, "Index"));
            Jackpot.Name = XML.GetValue(reader, "Name");
            Jackpot.Amount = (decimal)XML.ParseDouble(XML.GetValue(reader, "Amount"));
            Jackpot.MinimumBetAmount = (decimal)XML.ParseDouble(XML.GetValue(reader, "MinimumBetAmount"));
            Jackpots.Add(Jackpot);
            reader.Read(); //After this "read" the reader will be at the end of MinimumBetAmount
            reader.Read(); //After this "read" the reader will be at the end of Jackpot
            reader.Read(); //After this "read" the reader will be at the end of JackpotList or the beginning of new a Jackpot

            if (Jackpots.Count >= NumJackpots)
            {
              break;
            }
          }
          if (Jackpots.Count != NumJackpots)
          {
            throw new WC2_Exception (WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgSiteJackpotInfoReply wrong NumJackpots");
          }
        }


        NumJackpotsHistory = Int32.Parse(XML.GetValue(reader, "NumJackpotsHistory"));
        if (NumJackpotsHistory != 0)
        {

          while (((reader.Name != "History") || (reader.NodeType != XmlNodeType.EndElement)) && !reader.EOF)
          {
            JackpotHistory = new WC2_Jackpot_History();
            JackpotHistory.Index = int.Parse(XML.GetValue(reader, "Index"));
            JackpotHistory.Name = XML.GetValue(reader, "Name");
            JackpotHistory.Amount = (decimal)XML.ParseDouble(XML.GetValue(reader, "Amount"));
            DateTime.TryParse(XML.GetValue(reader, "Date"), out JackpotHistoryDate);
            JackpotHistory.Date = JackpotHistoryDate;

            reader.Read(); //After this "read" the reader will be at the end of Date
            reader.Read(); //After this "read" the reader will be at the end of JackpotAwarded or the beginning of PlayerName

            // 04-APR-2012 Add Player & Terminal Name
            if (reader.Name == "PlayerName")
            {
              JackpotHistory.PlayerName = XML.GetValue(reader, "PlayerName");
              JackpotHistory.TerminalName = XML.GetValue(reader, "TerminalName");

              reader.Read(); //After this "read" the reader will be at the end of TerminalName
              reader.Read(); //After this "read" the reader will be at the end of JackpotAwarded
            }
            else
            {
              // Assign empty value instead null value.
              JackpotHistory.PlayerName = "";
              JackpotHistory.TerminalName = "";
            }

            reader.Read(); //After this "read" the reader will be at the end of History or the beginning of new a JackpotAwarded

            JackpotsHistory.Add(JackpotHistory);

            if (JackpotsHistory.Count >= NumJackpotsHistory)
            {
              break;
            }
          }
          if (JackpotsHistory.Count != NumJackpotsHistory)
          {
            throw new WC2_Exception (WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgSiteJackpotInfoReply wrong NumJackpotsHistory");
          }
        }

        NumMessages = Int32.Parse(XML.GetValue(reader, "NumMessages"));
        if (NumMessages != 0)
        {
          while (((reader.Name != "Messages") || (reader.NodeType != XmlNodeType.EndElement)) && !reader.EOF)
          {
            Message = XML.GetValue(reader, "Message");
            Messages.Add(Message);
            reader.Read(); //After this "read" the reader will be at the end of Message
            reader.Read(); //After this "read" the reader will be at the end of Messages or the beginning of new a Message

            if (Messages.Count >= NumMessages)
            {
              break;
            }
          }
          if (Messages.Count != NumMessages)
          {
            throw new WC2_Exception (WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgSiteJackpotInfoReply wrong NumMessages");
          }
        }

        Mode = XML.GetValue(reader, "Mode");
        BlockInterval = int.Parse(XML.GetValue(reader, "BlockInterval"));
        MinBlockAmount = (decimal)XML.ParseDouble(XML.GetValue(reader, "MinBlockAmount"));
        RecentHistoryInterval = int.Parse(XML.GetValue(reader, "RecentHistoryInterval"));
        AnimationInterval01 = int.Parse (XML.GetValue (reader, "AnimationInterval01"));
        AnimationInterval02 = int.Parse (XML.GetValue (reader, "AnimationInterval02"));
      }
    }
  }
}
