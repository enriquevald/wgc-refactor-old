//------------------------------------------------------------------------------
// Copyright © 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_MsgCancelPlay.cs
// 
//   DESCRIPTION: WC2_MsgCancelPlay class
// 
//        AUTHOR: Daniel Rodríguez
// 
// CREATION DATE: 28-JUL-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-JUL-2008 DRG    First release.
//------------------------------------------------------------------------------
 
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using WSI.Common;

namespace WSI.WC2
{
  public class WC2_MsgCancelPlay : IXml
  {
    public Int64 TransactionId;

    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();
      str_builder.Append("<WC2_MsgCancelPlay><TransactionId>");
      str_builder.Append(TransactionId);
      str_builder.Append("</TransactionId></WC2_MsgCancelPlay>");
      return str_builder.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WC2_MsgCancelPlay")
      {
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgCancelPlay not found in content area");
      }
      else
      {
        TransactionId = Int64.Parse(XML.GetValue(reader, "TransactionId"));
      }
    }
  }

  public class WC2_MsgCancelPlayReply : IXml
  {
    public Int64 TransactionId;

    public String ToXml()
    {
      StringBuilder str_builder;

      str_builder = new StringBuilder();

      str_builder.Append("<WC2_MsgCancelPlayReply><TransactionId>");
      str_builder.Append(TransactionId);
      str_builder.Append("</TransactionId></WC2_MsgCancelPlayReply>");
      return str_builder.ToString();
    }

    public void LoadXml(XmlReader reader)
    {
      if (reader.Name != "WC2_MsgCancelPlayReply")
      {
        throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgCancelPlayReply not found in content area");
      }
      else
      {
        TransactionId = Int64.Parse(XML.GetValue(reader, "TransactionId"));
      }
    }
  }
}
