//------------------------------------------------------------------------------
// Copyright © 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_MsgPlay.cs
// 
//   DESCRIPTION: WC2_MsgPlay class
// 
//        AUTHOR: Daniel Rodríguez
// 
// CREATION DATE: 19-AUG-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 19-AUG-2008 DRG    First release.
//------------------------------------------------------------------------------

  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.Text;
  using System.Xml;
  using WSI.Common;

  namespace WSI.WC2
  {

    public class WC2_MsgPlay : IXml
    {
      public Int64 TransactionId;
      public String GameId;
      public int Denomination;
      public Int64 TotalPlayedCredits;
      public Int64 JackpotBoundCredits;
      public Int64 WonCredits;
      public int NumCards;
      public ArrayList Cards = new ArrayList();

      public String ToXml()
      {
        WC2_Card Card;
        StringBuilder str_builder;

        str_builder = new StringBuilder();

        str_builder.Append("<WC2_MsgPlay><TransactionId>");
        str_builder.Append(TransactionId);
        str_builder.Append("</TransactionId><GameId>");
        str_builder.Append(GameId);
        str_builder.Append("</GameId><Denomination>");
        str_builder.Append(Denomination);
        str_builder.Append("</Denomination><TotalPlayedCredits>");
        str_builder.Append(TotalPlayedCredits);
        str_builder.Append("</TotalPlayedCredits><JackpotBoundCredits>");
        str_builder.Append(JackpotBoundCredits);
        str_builder.Append("</JackpotBoundCredits><WonCredits>");
        str_builder.Append(WonCredits);
        str_builder.Append("</WonCredits><CardList>");
        str_builder.Append("<NumCards>");
        str_builder.Append(NumCards);
        str_builder.Append("</NumCards>");
        for (int idx_card = 0; idx_card < Cards.Count; idx_card++)
        {
          Card = (WC2_Card)Cards[idx_card];
          str_builder.Append("<Card><CardId>");
          str_builder.Append(Card.CardId);
          str_builder.Append("</CardId><CardType>"); 
          str_builder.Append(Card.CardType);
          str_builder.Append("</CardType><CardNumbers>"); 
          str_builder.Append(Card.CardNumbers);
          str_builder.Append("</CardNumbers></Card>");
        }

        str_builder.Append("</CardList></WC2_MsgPlay>");

        return str_builder.ToString();
      }

      public void LoadXml(XmlReader reader)
      {
        WC2_Card Card;

        if (reader.Name != "WC2_MsgPlay")
        {
          throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgPlay not found in content area");
        }
        else
        {
          TransactionId = Int64.Parse(XML.GetValue(reader, "TransactionId"));
          GameId = XML.GetValue(reader, "GameId");
          Denomination = int.Parse(XML.GetValue(reader, "Denomination"));
          TotalPlayedCredits = Int64.Parse(XML.GetValue(reader, "TotalPlayedCredits"));
          JackpotBoundCredits = Int64.Parse(XML.GetValue(reader, "JackpotBoundCredits"));
          WonCredits = Int64.Parse(XML.GetValue(reader, "WonCredits"));
          NumCards = int.Parse(XML.GetValue(reader, "NumCards"));
          if (NumCards > 0)
          {
            Cards = new ArrayList();
            while (((reader.Name != "CardList") || (reader.NodeType != XmlNodeType.EndElement)) && !reader.EOF)
            {
              Card = new WC2_Card();
              Card.CardId = XML.GetValue(reader, "CardId");
              Card.CardType = XML.GetValue(reader, "CardType");
              Card.CardNumbers = XML.GetValue(reader, "CardNumbers");
              Cards.Add(Card);
              reader.Read(); //After this "read" the reader will be at the end of CardNumbers
              reader.Read(); //After this "read" the reader will be at the end of Card
              reader.Read(); //After this "read" the reader will be at the end of Cards or the beginning of new a Card
            }
          }
        }
      }
    }

    public class WC2_MsgPlayReply : IXml
    {
      public Int64 TransactionId;
      public Int64 DrawId;
      public DateTime DrawDatetime = DateTime.Now;
      public Int64 TotalWonCredits;
      public WC2_Draw Draw = new WC2_Draw();
      public int NumPrizes;
      public ArrayList Prizes = new ArrayList();   // WC2_Prize
      public Boolean HasJackpotPrize = false;
      public Int64 JackpotId;
      public WC2_Jackpot JackpotData = new WC2_Jackpot();
      public int NumCards;
      public ArrayList Cards = new ArrayList();

      public String GetCardList()
      {
        WC2_Card Card;
        StringBuilder str_builder;

        str_builder = new StringBuilder();

        str_builder.Append("<CardList><NumCards>");
        str_builder.Append(NumCards);
        str_builder.Append("</NumCards>");
        for (int idx_card = 0; idx_card < Cards.Count; idx_card++)
        {
          Card = (WC2_Card)Cards[idx_card];
          str_builder.Append("<Card><CardId>");
          str_builder.Append(Card.CardId);
          str_builder.Append("</CardId><CardType>");
          str_builder.Append(Card.CardType);
          str_builder.Append("</CardType><CardNumbers>");
          str_builder.Append(Card.CardNumbers);
          str_builder.Append("</CardNumbers></Card>");
        }
        str_builder.Append("</CardList>");
        return str_builder.ToString();
      }
      public String GetPrizeList()
      {
        WC2_Prize Prize;
        WC2_CardPrize CardPrize;
        StringBuilder str_builder;

        str_builder = new StringBuilder();

        str_builder.Append("<PrizeList><NumPrizes>");
        str_builder.Append(NumPrizes);
        str_builder.Append("</NumPrizes>");
        for (int idx_prize = 0; idx_prize < Prizes.Count; idx_prize++)
        {
          Prize = (WC2_Prize)Prizes[idx_prize];
          str_builder.Append("<Prize><CardId>");
          str_builder.Append(Prize.CardId);
          str_builder.Append("</CardId><CardType>");
          str_builder.Append(Prize.CardType);
          str_builder.Append("</CardType><CardPrizeList><NumCardPrizes>");
          str_builder.Append(Prize.NumCardPrizes);
          str_builder.Append("</NumCardPrizes>");
          for (int idx_cardprize = 0; idx_cardprize < Prize.CardPrizes.Count; idx_cardprize++)
          {
            CardPrize = (WC2_CardPrize)Prize.CardPrizes[idx_cardprize];
            str_builder.Append("<CardPrize><ResultIndex>");
            str_builder.Append(CardPrize.ResultIndex);
            str_builder.Append("</ResultIndex><PrizeCredits>");
            str_builder.Append(CardPrize.PrizeCredits);
            str_builder.Append("</PrizeCredits><PrizePattern><NumPositions>");
            str_builder.Append(CardPrize.NumPositions);
            str_builder.Append("</NumPositions><PositionList>");
            str_builder.Append(CardPrize.PositionList);
            str_builder.Append("</PositionList></PrizePattern></CardPrize>");
          }
          str_builder.Append("</CardPrizeList></Prize>");
        }
        str_builder.Append("</PrizeList>");
        return str_builder.ToString();
      }

      public String ToXml()
      {
        WC2_Card Card;
        WC2_Prize Prize;
        WC2_CardPrize CardPrize;

        string str;

        StringBuilder str_builder;
        str_builder = new StringBuilder();

        str = XML.XmlDateTimeString(DrawDatetime);

        str_builder.Append("<WC2_MsgPlayReply><TransactionId>");
        str_builder.Append(TransactionId);
        str_builder.Append("</TransactionId><DrawId>");
        str_builder.Append(DrawId);
        str_builder.Append("</DrawId><DrawDatetime>");
        str_builder.Append(str);
        str_builder.Append("</DrawDatetime><TotalWonCredits>");
        str_builder.Append(TotalWonCredits);
        str_builder.Append("</TotalWonCredits>");
        str_builder.Append("<MaxBallsForPrize>");
        str_builder.Append(Draw.MaxBallsForPrize);
        str_builder.Append("</MaxBallsForPrize><BallNumbersRange><MinBallNumber>");
        str_builder.Append(Draw.MinBallNumber);
        str_builder.Append("</MinBallNumber><MaxBallNumber>");
        str_builder.Append(Draw.MaxBallNumber);
        str_builder.Append("</MaxBallNumber></BallNumbersRange><BallsList><NumBalls>");
        str_builder.Append(Draw.NumBalls);
        str_builder.Append("</NumBalls><Balls>");
        str_builder.Append(Draw.BallList);
        str_builder.Append("</Balls></BallsList><PrizeList><NumPrizes>");
        str_builder.Append(NumPrizes);
        str_builder.Append("</NumPrizes>");
        for (int idx_prize = 0; idx_prize < Prizes.Count; idx_prize++)
        {
          Prize = (WC2_Prize)Prizes[idx_prize];
          str_builder.Append("<Prize><CardId>");
          str_builder.Append(Prize.CardId);
          str_builder.Append("</CardId><CardType>");
          str_builder.Append(Prize.CardType);
          str_builder.Append("</CardType><CardPrizeList><NumCardPrizes>");
          str_builder.Append(Prize.NumCardPrizes);
          str_builder.Append("</NumCardPrizes>");
          for (int idx_cardprize = 0; idx_cardprize < Prize.CardPrizes.Count; idx_cardprize++)
          {
            CardPrize = (WC2_CardPrize)Prize.CardPrizes[idx_cardprize];
            str_builder.Append("<CardPrize><ResultIndex>");
            str_builder.Append(CardPrize.ResultIndex);
            str_builder.Append("</ResultIndex><PrizeCredits>");
            str_builder.Append(CardPrize.PrizeCredits);
            str_builder.Append("</PrizeCredits><PrizePattern><NumPositions>");
            str_builder.Append(CardPrize.NumPositions);
            str_builder.Append("</NumPositions><PositionList>");
            str_builder.Append(CardPrize.PositionList);
            str_builder.Append("</PositionList></PrizePattern></CardPrize>");
          }
          str_builder.Append("</CardPrizeList></Prize>");
        }
        str_builder.Append("</PrizeList>");

        if (HasJackpotPrize)
        {
          str_builder.Append("<Jackpot><JackpotId>");
          str_builder.Append(JackpotId);
          str_builder.Append("</JackpotId><Index>");
          str_builder.Append(JackpotData.Index);
          str_builder.Append("</Index><Name>");
          str_builder.Append(JackpotData.Name);
          str_builder.Append("</Name><Amount>");
          str_builder.Append(XML.DoubleToXmlValue((double)JackpotData.Amount));
          str_builder.Append("</Amount></Jackpot>");
        }

        str_builder.Append("<CardList><NumCards>");
        str_builder.Append(NumCards);
        str_builder.Append("</NumCards>");
        for (int idx_card = 0; idx_card < Cards.Count; idx_card++)
        {
          Card = (WC2_Card)Cards[idx_card];
          str_builder.Append("<Card><CardId>");
          str_builder.Append(Card.CardId);
          str_builder.Append("</CardId><CardType>");
          str_builder.Append(Card.CardType);
          str_builder.Append("</CardType><CardNumbers>");
          str_builder.Append(Card.CardNumbers);
          str_builder.Append("</CardNumbers></Card>");
        }
        str_builder.Append("</CardList></WC2_MsgPlayReply>");

        return str_builder.ToString();
      }

      public void LoadXml(XmlReader reader)
      {
        if (reader.Name != "WC2_MsgPlayReply")
        {
          throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_MSG_FORMAT_ERROR, "Tag WC2_MsgPlayReply not found in content area");
        }
        else
        {
          WC2_Card Card;
          WC2_Prize Prize;
          WC2_CardPrize CardPrize;

          Draw = new WC2_Draw();
          TransactionId = long.Parse(XML.GetValue(reader, "TransactionId"));
          DrawId = long.Parse(XML.GetValue(reader, "DrawId"));
          DrawDatetime = DateTime.Now;
          DateTime.TryParse(XML.GetValue(reader, "DrawDatetime"), out DrawDatetime);
          TotalWonCredits = long.Parse(XML.GetValue(reader, "TotalWonCredits"));
          Draw.MaxBallsForPrize = int.Parse(XML.GetValue(reader, "MaxBallsForPrize"));
          Draw.MinBallNumber = int.Parse(XML.GetValue(reader, "MinBallNumber"));
          Draw.MaxBallNumber = int.Parse(XML.GetValue(reader, "MaxBallNumber"));
          
          Draw.NumBalls = int.Parse(XML.GetValue(reader, "NumBalls")); 
          Draw.BallList = XML.GetValue(reader, "Balls");

          NumPrizes = int.Parse(XML.GetValue(reader, "NumPrizes"));

          if (NumPrizes > 0)
          {
            Prizes = new ArrayList();   // WC2_Prize
            while (((reader.Name != "PrizeList") || (reader.NodeType != XmlNodeType.EndElement)) && !reader.EOF)
            {
              Prize = new WC2_Prize();
              Prize.CardId = XML.GetValue(reader, "CardId");
              Prize.CardType = XML.GetValue(reader, "CardType");

              Prize.NumCardPrizes = int.Parse(XML.GetValue(reader, "NumCardPrizes"));
              if (Prize.NumCardPrizes > 0)
              {
                Prize.CardPrizes = new ArrayList();
                while (((reader.Name != "CardPrizeList") || (reader.NodeType != XmlNodeType.EndElement)) && !reader.EOF)
                {
                  CardPrize = new WC2_CardPrize();
                  CardPrize.ResultIndex = int.Parse(XML.GetValue(reader, "ResultIndex"));
                  CardPrize.PrizeCredits = long.Parse(XML.GetValue(reader, "PrizeCredits"));
                  CardPrize.NumPositions = int.Parse(XML.GetValue(reader, "NumPositions"));
                  if (CardPrize.NumPositions > 0)
                  {
                    CardPrize.PositionList = XML.GetValue(reader, "PositionList");
                  }
                  else
                  {
                    reader.Read(); //After this "read" the reader will be at the end of NumPositions
                    reader.Read(); //After this "read" the reader will be at the beginning of PositionList
                  }
                  Prize.CardPrizes.Add(CardPrize);
                  reader.Read(); //After this "read" the reader will be at the end of PositionList
                  reader.Read(); //After this "read" the reader will be at the end of PrizePattern
                  reader.Read(); //After this "read" the reader will be at the end of CardPrize
                  reader.Read(); //After this "read" the reader will be at the end of CardPrizeList or the beginning of new a CardPrize
                }
                Prizes.Add(Prize);
                reader.Read(); //After this "read" the reader will be at the end of Prize
                reader.Read(); //After this "read" the reader will be at the end of PrizeList or the beginning of new a Prize
              }
            }
          }
          else
          {
            reader.Read();
            reader.Read();
          }

          reader.Read(); //After this "read" the reader will be at the beginning of Jackpot or CardList

          // Init jackpot name
          JackpotData.Name = "";

          if ((reader.Name == "Jackpot") && (reader.NodeType == XmlNodeType.Element))
          {
            HasJackpotPrize = true;

            JackpotData = new WC2_Jackpot();

            JackpotId = long.Parse(XML.GetValue(reader, "JackpotId"));
            JackpotData.Index = int.Parse(XML.GetValue(reader, "Index"));
            JackpotData.Name = XML.GetValue(reader, "Name");
            JackpotData.Amount = (Decimal)XML.ParseDouble(XML.GetValue(reader, "Amount"));

          }
          else
          {
            HasJackpotPrize = false;
          }

          NumCards = int.Parse(XML.GetValue(reader, "NumCards"));
          if (NumCards > 0)
          {
            Cards = new ArrayList();
            while (((reader.Name != "CardList") || (reader.NodeType != XmlNodeType.EndElement)) && !reader.EOF)
            {
              Card = new WC2_Card();
              Card.CardId = XML.GetValue(reader, "CardId");
              Card.CardType = XML.GetValue(reader, "CardType");
              Card.CardNumbers = XML.GetValue(reader, "CardNumbers");
              Cards.Add(Card);
              reader.Read(); //After this "read" the reader will be at the end of CardNumbers
              reader.Read(); //After this "read" the reader will be at the end of Card
              reader.Read(); //After this "read" the reader will be at the end of CardList or the beginning of new a Card
            }
          }
        }
      }
    }
  }
