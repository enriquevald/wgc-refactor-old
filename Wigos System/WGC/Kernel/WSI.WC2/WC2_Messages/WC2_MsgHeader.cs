//------------------------------------------------------------------------------
// Copyright © 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_MsgHeader.cs
// 
//   DESCRIPTION: WC2 Message Header
// 
//        AUTHOR: Daniel Rodríguez
// 
// CREATION DATE: 29-JUL-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 28-JUL-2008 AJQ    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

using WSI.Common;


namespace WSI.WC2
{
  /// <summary>
  /// WC2 message types
  /// </summary>
  public enum WC2_MsgTypes
  { 
      // Request                  // Reply
      WC2_MsgRequestService,      WC2_MsgRequestServiceReply
    , WC2_MsgEnrollTerminal,      WC2_MsgEnrollTerminalReply
    , WC2_MsgGetParameters,       WC2_MsgGetParametersReply
    , WC2_MsgUnenrollTerminal,    WC2_MsgUnenrollTerminalReply
    , WC2_MsgKeepAlive,           WC2_MsgKeepAliveReply
    , WC2_MsgCancelPlay,          WC2_MsgCancelPlayReply
    , WC2_MsgGetCards,            WC2_MsgGetCardsReply
    , WC2_MsgGetJackpotInfo,      WC2_MsgGetJackpotInfoReply
    , WC2_MsgGetPatterns,         WC2_MsgGetPatternsReply
    , WC2_MsgPlay,                WC2_MsgPlayReply
    , WC2_MsgError
    , WC2_MsgUnknown
    , WC2_MsgSiteJackpotInfo,     WC2_MsgSiteJackpotInfoReply   // WC2_MsgSiteJackpotInfoReply sequence must match the ordinal value of 
                                                                // WC2_MSG_TYPE_SITE_JACKPOT_INFO_REPLY in Protocol_WC2_Transport.cpp (LKT's Protocol_WC2_API)
  }

  /// <summary>
  /// WC2 response codes
  /// </summary>
  public enum WC2_ResponseCodes
  {
      WC2_RC_OK
    , WC2_RC_ERROR
    , WC2_RC_DATABASE_OFFLINE
    , WC2_RC_SERVICE_NOT_AVAILABLE
    , WC2_RC_SERVICE_NOT_FOUND
    , WC2_RC_SERVER_NOT_AUTHORIZED
    , WC2_RC_SERVER_SESSION_NOT_VALID
    , WC2_RC_TERMINAL_NOT_AUTHORIZED
    , WC2_RC_TERMINAL_SESSION_NOT_VALID
    , WC2_RC_CARD_NOT_VALID
    , WC2_RC_CARD_BLOCKED
    , WC2_RC_CARD_EXPIRED
    , WC2_RC_CARD_ALREADY_IN_USE
    , WC2_RC_CARD_NOT_VALIDATED
    , WC2_RC_CARD_BALANCE_MISMATCH
    , WC2_RC_CARD_REMOTE_BALANCE_MISMATCH
    , WC2_RC_CARD_SESSION_NOT_CLOSED
    , WC2_RC_CARD_SESSION_NOT_VALID
    , WC2_RC_INVALID_SESSION_ID
    , WC2_RC_UNKNOWN_GAME_ID
    , WC2_RC_INVALID_AMOUNT
    , WC2_RC_EVENT_NOT_VALID
    , WC2_RC_CRC_MISMATCH
    , WC2_RC_NOT_ENOUGH_PLAYERS
    , WC2_RC_CANCEL_NOT_ALLOWED
    , WC2_RC_PLAY_DELAYED

    , WC2_RC_MSG_FORMAT_ERROR
    , WC2_RC_INVALID_MSG_TYPE
    , WC2_RC_INVALID_PROTOCOL_VERSION
    , WC2_RC_INVALID_SEQUENCE_ID
    , WC2_RC_INVALID_SERVER_ID
    , WC2_RC_INVALID_TERMINAL_ID
    , WC2_RC_INVALID_SERVER_SESSION_ID
    , WC2_RC_INVALID_TERMINAL_SESSION_ID
    , WC2_RC_INVALID_COMPRESSION_METHOD
    , WC2_RC_INVALID_RESPONSE_CODE

    , WC2_RC_NO_CARDS_AVAILABLE
    , WC2_RC_INVALID_CARD_TYPE

    , WC2_RC_UNKNOWN
    , WC2_RC_LAST
  }

  public enum WC2_GenericErrorCodes
  {
	    WC2_RC_ERROR
	  , WC2_RC_MSG_FORMAT_ERROR
	  , WC2_RC_INVALID_MSG_TYPE
	  , WC2_RC_INVALID_PROTOCOL_VERSION
	  , WC2_RC_INVALID_SEQUENCE_ID
	  , WC2_RC_INVALID_SERVER_ID
	  , WC2_RC_INVALID_TERMINAL_ID
	  , WC2_RC_INVALID_SERVER_SESSION_ID
	  , WC2_RC_INVALID_TERMINAL_SESSION_ID
	  , WC2_RC_INVALID_COMPRESSION_METHOD
	  , WC2_RC_INVALID_RESPONSE_CODE
  }

  public enum WC2_EventTypesCodes
  {
    WC2_EV_DOOR_OPENED = 101,
    WC2_EV_DOOR_CLOSED = 102,
    WC2_EV_NOTE_ACCEPTOR_JAM = 103,
    WC2_EV_NOTE_ACCEPTOR_FULL = 104,
    WC2_EV_NOTE_ACCEPTOR_ERROR = 105,
    WC2_EV_PRINTER_NO_PAPER = 106,
    WC2_EV_PRINTER_JAM = 107,
    WC2_EV_PRINTER_ERROR = 108,
    WC2_EV_POWER_FAILURE = 109,
    WC2_EV_LOST_CONNECTION = 110,
    WC2_EV_STARTUP  = 111, 
    WC2_EV_SHUTDOWN = 112,
    WC2_EV_CREDIT_LIMIT = 113,
    WC2_EV_DISPLAY_OFF = 114,
    WC2_EV_CARDREADER_OFF = 115,
  }

  public enum WC2_CardTypes
  { 
      CARD_TYPE_UNKNOWN
    , CARD_TYPE_PLAYER
  }


  public enum WC2_CashTypes
  {
    CASH_TYPE_CASH_IN   = 1
  , CASH_TYPE_CASH_OUT  = 2
  }


  public enum WC2_MoneyTypes
  {
    MONEY_TYPE_NOTE     = 1
  , MONEY_TYPE_COIN     = 2
  }

  public enum WC2_CommandTypes
  {
     WC2_CMD_SHUTDOWN
   , WC2_CMD_RESTART
   , WC2_CMD_RELOAD_PARAMETERS
   , WC2_CMD_RESET_METERS
   , WC2_CMD_REPORT_METERS
   , WC2_CMD_LOCK
   , WC2_CMD_DISCONNECT
   , WC2_CMD_INTEGRITY_CHECK
  }

  /// <summary>
  /// WC2 compression methods
  /// </summary>
  public enum WC2_CompressionMethod
  { 
    NONE
  }

  /// <summary>
  /// The WC2 message header
  /// </summary>
  public class WC2_MsgHeader  
  {
    #region CONSTANTS

    public const string PROTOCOL_VERSION = "1.0.0.3";
    const string COMPRESSION_METHOD = "NONE";
    
    #endregion // CONSTANTS

    #region MEMBERS

    private string ProtocolVersion;
    public  string CompressionMethod;
    private DateTime SystemTime;
    public string TerminalId;
    public Int64 TerminalSessionId;
    public WC2_MsgTypes MsgType;
    public Int64 SequenceId;
    public int MsgContentSize;
    public WC2_ResponseCodes ResponseCode;
    public string ResponseCodeText;

    #endregion // MEMBERS

    #region PUBLIC

    /// <summary>
    ///   Gets the Protocol Version
    /// </summary>
    public string GetProtocolVersion
    {
      get { return ProtocolVersion; }
    }

    /// <summary>
    ///   Gets the Compression Method
    /// </summary>
    WC2_CompressionMethod GetCompressionMethod
    {
      get { return WC2_CompressionMethod.NONE; }
    }

    /// <summary>
    /// Gets the System Time
    /// </summary>
    public DateTime GetSystemTime
    {
      get { return SystemTime; }
    }

    /// <summary>
    /// Creates the reply header.
    /// The following fields are copied from the request:
    /// - ServerId
    /// - TerminalId
    /// - ServerSessionId
    /// - TerminalSessionId
    /// - SequenceId
    /// </summary>
    /// <param name="ResponseCode">Response Code</param>
    /// <returns>The reply header</returns>
    public WC2_MsgHeader ReplyHeader (WC2_ResponseCodes ResponseCode)
    {
      WC2_MsgHeader reply;

      reply = new WC2_MsgHeader ();
      reply.TerminalId = TerminalId;
      reply.TerminalSessionId = TerminalSessionId;
      reply.MsgType = MsgType;
      reply.MsgType++;
      reply.SequenceId = SequenceId;
      reply.ResponseCode = ResponseCode;
      reply.ResponseCodeText = "";

      return reply;
    }

    public void PrepareReply (WC2_MsgHeader RequestHeader)
    {
      TerminalId = RequestHeader.TerminalId;
      TerminalSessionId = RequestHeader.TerminalSessionId;
      SequenceId = RequestHeader.SequenceId;

      if ( MsgType == WC2_MsgTypes.WC2_MsgUnknown )
      {
        MsgType = RequestHeader.MsgType + 1;
      }
    }


    /// <summary>
    /// Extracts an enum object value from a 
    /// Enum Object giving the string name of 
    /// the value and getting the index.
    /// </summary>
    /// <param name="ResponseCode">Response Code</param>
    /// <returns>The reply header</returns>
    public static Object Decode(String str, Object defaultValue)
    {
      foreach (String s in System.Enum.GetNames(defaultValue.GetType()))
      {
        if (s.Equals(str))
        {
          return System.Enum.Parse(defaultValue.GetType(), s);
        }
      }
      return defaultValue;
    }

    public void LoadXml(XmlReader reader)
    {
      ProtocolVersion = XML.GetValue(reader, "ProtocolVersion");
      CompressionMethod = XML.GetValue(reader, "CompressionMethod");
      DateTime.TryParse(XML.GetValue(reader, "SystemTime"), out SystemTime);
      TerminalId = XML.GetValue(reader, "TerminalId");
      long.TryParse(XML.GetValue(reader, "TerminalSessionId"), out TerminalSessionId);
      MsgType = (WC2_MsgTypes)Decode(XML.GetValue(reader, "MsgType"), WC2_MsgTypes.WC2_MsgUnknown);
      long.TryParse(XML.GetValue(reader, "SequenceId"), out SequenceId);
      int.TryParse(XML.GetValue(reader, "MsgContentSize"), out MsgContentSize);
      ResponseCode = (WC2_ResponseCodes)Decode(XML.GetValue(reader, "ResponseCode"), WC2_ResponseCodes.WC2_RC_UNKNOWN);
      ResponseCodeText = XML.GetValue(reader, "ResponseCodeText");
    }


    public String ToXml ()
    {

      DateTime now;
      string str;

      now = DateTime.Now;
      str = XML.XmlDateTimeString(now);

      StringBuilder str_builder;

      str_builder = new StringBuilder();

      str_builder.Append("<WC2_MsgHeader><ProtocolVersion>");

      str_builder.Append(ProtocolVersion);
      str_builder.Append("</ProtocolVersion><CompressionMethod>");
      str_builder.Append(CompressionMethod);
      str_builder.Append("</CompressionMethod><SystemTime>");
      str_builder.Append(str);
      str_builder.Append("</SystemTime><TerminalId>");
      str_builder.Append(XML.StringToXmlValue(TerminalId));
      str_builder.Append("</TerminalId><TerminalSessionId>");
      str_builder.Append(TerminalSessionId);
      str_builder.Append("</TerminalSessionId><MsgType>");
      str_builder.Append(MsgType);
      str_builder.Append("</MsgType><SequenceId>");
      str_builder.Append(SequenceId);
      str_builder.Append("</SequenceId><MsgContentSize>");
      str_builder.Append(MsgContentSize);
      str_builder.Append("</MsgContentSize><ResponseCode>");
      str_builder.Append(ResponseCode);
      str_builder.Append("</ResponseCode><ResponseCodeText>");
      str_builder.Append(XML.StringToXmlValue(ResponseCodeText));
      str_builder.Append("</ResponseCodeText></WC2_MsgHeader>");

      return str_builder.ToString();
    }

    /// <summary>
    /// Adds the header to the given Xml WC2 message
    /// </summary>
    /// <param name="XmlWC2Message">The message</param>
    public void ToXml (XmlDocument XmlWC2Message)
    {
      XmlNode root;
      XmlNode header;
      XmlNode old_header;
      DateTime now;
      string str;

      now = DateTime.Now;
      str = XML.XmlDateTimeString(now);

      root = XmlWC2Message.SelectSingleNode ("WC2_Message");
      header = XmlWC2Message.CreateNode (XmlNodeType.Element, "", "WC2_MsgHeader", "");

      XML.AppendChild (header, "ProtocolVersion", ProtocolVersion);
      XML.AppendChild (header, "CompressionMethod", CompressionMethod);
      XML.AppendChild (header, "SystemTime", str);
      XML.AppendChild (header, "TerminalId", TerminalId);
      XML.AppendChild (header, "TerminalSessionId", TerminalSessionId.ToString ());
      XML.AppendChild (header, "MsgType", MsgType.ToString ());
      XML.AppendChild (header, "SequenceId", SequenceId.ToString ());
      XML.AppendChild (header, "MsgContentSize", MsgContentSize.ToString ());
      XML.AppendChild (header, "ResponseCode", ResponseCode.ToString ());
      XML.AppendChild (header, "ResponseCodeText", ResponseCodeText);

      old_header = root.SelectSingleNode ("WC2_MsgHeader");
      if ( old_header != null )
      {
        root.RemoveChild (old_header);
      }

      if ( root.FirstChild != null )
      {
        root.InsertBefore (header, root.FirstChild);
      }
      else
      {
        root.AppendChild (header);
      }
    }

    /// <summary>
    /// Creates a header
    /// </summary>
    public WC2_MsgHeader ()
    {
      Init ();
    }

    #endregion // PUBLIC

    #region PRIVATE

    /// <summary>
    /// Initializes the header
    /// </summary>
    private void Init()
    {
      ProtocolVersion = PROTOCOL_VERSION;
      CompressionMethod = COMPRESSION_METHOD;
      SystemTime = System.DateTime.Now;
      TerminalId = "";
      TerminalSessionId = 0;
      MsgType = WC2_MsgTypes.WC2_MsgUnknown;
      SequenceId = 0;
      MsgContentSize = 0;
      ResponseCode = WC2_ResponseCodes.WC2_RC_OK;
      ResponseCodeText = "";
    }


    #endregion // PRIVATE
  }
}
