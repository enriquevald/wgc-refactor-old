using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.IO;
using System.Net;
using System.Xml;
using System.Data;
using System.Data.SqlClient;

using WSI.Common;


namespace WSI.WC2
{
  public static class WC2
  {
    static WC2_Server server;

    public static WC2_Server Server
    {
      get { return WC2.server; }
      set { WC2.server = value; }
    }
  }

  public class WC2_Server:SecureTcpServer
  {
    private const String WC2_STX = "<WC2_Message>";
    private const String WC2_ETX = "</WC2_Message>";
    private const Int32  WC2_MESSAGE_MAX_LENGTH = 32 * 1024; 
    private IXmlSink     sink;

    /// <summary>
    /// Send an Xml message
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <param name="SessionId"></param>
    /// <param name="Xml"></param>
    public bool SendTo (Int32 TerminalId, Int64 SessionId, String Xml)
    {
      m_clients_rw_lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
      try
      {
        foreach ( SecureTcpClient client in Clients )
        {
          if ( client.InternalId == TerminalId )
          {
            if ( SessionId == 0
                || SessionId == client.SessionId )
            {
              client.Send (Xml);

              return true;
            }
          }
        }
        return false;
      }
      finally
      {
        m_clients_rw_lock.ReleaseReaderLock();
      }
    }

    public IXmlSink Sink
    {
      set
      {
        sink = value;
      }
    }


    public WC2_Server (IPEndPoint IPEndPoint) : base (IPEndPoint)
    {
      this.SetXmlServerParameters (WC2_STX, WC2_ETX, WC2_MESSAGE_MAX_LENGTH, Encoding.UTF8);
    }

    public override void ClientConnected (SecureTcpClient SecureClient)
    {
      base.ClientConnected (SecureClient);
    }

    public override void ClientDisconnected (SecureTcpClient SecureClient)
    {
      Common.BatchUpdate.TerminalSession.CloseSession (SecureClient.InternalId,
                                                       SecureClient.SessionId,
                                                       (int) WC2_SessionStatus.Disconnected);

      base.ClientDisconnected(SecureClient);
    }

    public override void XmlMesageReceived(SecureTcpClient SecureTcpClient, string XmlMessage)
    {
      try
      {
        sink.Process(this, SecureTcpClient, XmlMessage);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Message("EXEP Msg: " + XmlMessage);
      }
    }
  } // Class SecureTcpServer
}
