using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;
using System.Timers;

namespace WSI.WC2
{
  internal enum CachedTable
  {
    terminals = 0,
  }

  public static class WC2_DbCache
  {
    static Thread thread_cache;
    //static Thread thread_cleaner;

    static internal System.Threading.AutoResetEvent[] event_reload;
    static internal System.Threading.ManualResetEvent[] event_reloaded;
    static SqlDataAdapter da;
    static DataSet ds;
    static ReaderWriterLock dbcache_lock;

    /// <summary>
    /// Initialize class attributes.
    /// </summary>
    public static void Init()
    {
      // Cache
      thread_cache = new Thread(DbCacheThread);
      thread_cache.Name = "WC2_DbCache";

      event_reload = new AutoResetEvent[1];
      event_reloaded = new ManualResetEvent[event_reload.Length];

      // Init events array
      for (int i = 0; i < event_reload.Length; i++)
      {
        event_reload[i] = new AutoResetEvent(true);
        event_reloaded[i] = new ManualResetEvent(false);
      }

      // Init database connection
      ds = new DataSet();
      da = new SqlDataAdapter();

      // Init ReaderWriterLock
      dbcache_lock = new ReaderWriterLock();

      //////// Init cleaner thread
      //////thread_cleaner = new Thread(DbCleanerThread);
      //////thread_cleaner.Name = "DbCleaner";
    }

    /// <summary>
    /// Start WC2_DbCache thread.
    /// </summary>
    public static void Start()
    {
      thread_cache.Start();
      //thread_cleaner.Start();
    }

    /// <summary>
    /// Set events to reload data.
    /// </summary>
    /// <param name="Table"></param>
    private static void ManageEvents(CachedTable Table)
    {
      WC2_DbCache.event_reloaded[(int)Table].Reset();
      WC2_DbCache.event_reload[(int)Table].Set();
      WC2_DbCache.event_reloaded[(int)Table].WaitOne();
    }

    /// <summary>
    /// Apply changes on database for a specific table.
    /// </summary>
    /// <param name="Table"></param>
    private static void Update(DataTable Table)
    {
      DataTable table_aux;

      lock (Table)
      {
        table_aux = Table.GetChanges();
      }

      if (table_aux != null)
      {
        if (table_aux.Rows.Count > 0)
        {
          da.InsertCommand.Connection = WGDB.Connection();
          da.InsertCommand.Transaction = da.InsertCommand.Connection.BeginTransaction();
          da.Update(table_aux);
          da.InsertCommand.Transaction.Commit();
        }
      }
    }

    /// <summary>
    /// Set event to reload Terminals table.
    /// </summary>
    /// <param name="TerminalId"></param>
    public static void ReloadTerminal(Int32 TerminalId)
    {
      WC2_DbCache.event_reload[(int)CachedTable.terminals].Set();      
    }

    /// <summary>
    /// Returns terminal data for a specific terminal Id.
    /// </summary>
    /// <param name="TerminalId"></param>
    /// <returns></returns>
    public static DataRow Terminal(Int32 TerminalId) 
    {
      DataTable table;
      DataRow[] terminals;

      for (int i = 0; i < 2; i++)
      {
        GetTableFromDataset("TERMINALS", out table);
        terminals = table.Select("TE_TERMINAL_ID = '" + TerminalId + "'");
        if (terminals.Length > 0)
        {
          DataRow r;

          r = table.NewRow();
          r.ItemArray = (Object[])terminals[0].ItemArray.Clone();
          return r;
        }

        if (i == 0)
        {
          ManageEvents(CachedTable.terminals);
        }
      } 
      return null;
    }

    /// <summary>
    /// Return terminal data for a specific external terminal id.
    /// </summary>
    /// <param name="ExternalId"></param>
    /// <returns></returns>
    public static DataRow Terminal(String ExternalId)
    {
      DataTable table;
      DataRow[] terminals;

      GetTableFromDataset("TERMINALS", out table);
      terminals = table.Select("TE_EXTERNAL_ID = '" + ExternalId + "'");
      if (terminals.Length > 0)
      {
        DataRow r;

        r = table.NewRow();
        r.ItemArray = (Object[])terminals[0].ItemArray.Clone();
        return r;
      }

      return null;
    }

    public static Boolean IsTerminalLocked(String ExternalId)
    {
      DataRow dr;
      Int32 ServerId;

      dr = WC2_DbCache.Terminal(ExternalId);
      if (dr == null)
      {
        return true;
      }
      if (((Boolean)dr["TE_BLOCKED"]))
      {
        return true;
      }

      if (dr.IsNull("TE_SERVER_ID"))
      {
        return false;
      }

      ServerId = (Int32)dr["TE_SERVER_ID"];

      dr = WC2_DbCache.Terminal(ServerId);
      if (dr == null)
      {
        return true;
      }
      if (((Boolean)dr["TE_BLOCKED"]))
      {
        return true;
      }

      return false;
    }

    /// <summary>
    /// Reload in memory a specific table.
    /// </summary>
    /// <param name="Table"></param>
    /// <param name="Connection"></param>
    /// <returns></returns>
    private static Boolean ReloadTable(CachedTable Table, SqlConnection Connection)
    {
      SqlCommand cmd;
      SqlDataAdapter da;
      DataTable dt_load;
      String table_name;

      dt_load = null;
      da = null;
      cmd = null;

      table_name = Table.ToString();
      try
      {
        dt_load = new DataTable (table_name);
        da = new SqlDataAdapter();
        cmd = new SqlCommand();
        cmd.Connection = Connection;
        da.SelectCommand = cmd;

        switch (Table)
        {
          case CachedTable.terminals:
          // Tables: Terminals
          default:
            cmd.CommandText = "SELECT * FROM " + table_name;
          break;
        }

        // Retrieve data
        da.Fill(dt_load);
        dbcache_lock.AcquireWriterLock(Timeout.Infinite);
        try
        {
          if (ds.Tables.IndexOf(table_name) >= 0)
          {
            ds.Tables.Remove(table_name);
          }

          ds.Tables.Add(dt_load);
        }
        finally
        {
          dbcache_lock.ReleaseWriterLock();
        }

        return true;
      }
      catch(Exception ex)
      {
        Log.Exception(ex);

        return false;
      }
      finally
      {
        if (da != null)
        {
          da.SelectCommand = null;
          da = null;
        }

        if (cmd != null)
        {
          cmd.Connection = null;
          cmd = null;
        }
      }
    }

    /// <summary>
    /// Get a table from dataset 'ds'. 
    /// Lock the ds for access.
    /// </summary>
    /// <param name="TableName"></param>
    /// <param name="Table"></param>
    private static void GetTableFromDataset(String TableName, out DataTable Table)
    {
      dbcache_lock.AcquireReaderLock(Timeout.Infinite);
      try
      {
        Table = ds.Tables[TableName];
      }
      finally
      {
        dbcache_lock.ReleaseReaderLock();
      }
    }

    /// <summary>
    /// Manage table reloads.
    /// </summary>
    static private void DbCacheThread()
    {
      TimeSpan elapsed;
      DateTime last_timeout;
      DateTime last_reload_terminals;
      Int32 timeout_seconds;
      Random rnd;

      rnd = new Random (Environment.TickCount);

      last_reload_terminals = DateTime.Now;

      last_timeout = DateTime.Now;
      timeout_seconds = 5 * 60 + rnd.Next(30);

      while (true)
      {
        SqlConnection conn;
        int idx_event;

        conn = null;

        try
        {
          idx_event = AutoResetEvent.WaitAny(event_reload, 2000, false);

          conn = WGDB.Connection ();

          if ( idx_event == AutoResetEvent.WaitTimeout )
          {
            //
            // Timeout
            //
            elapsed = DateTime.Now.Subtract(last_timeout);
            if ( elapsed.TotalSeconds >= timeout_seconds )
            {
              WC2_BusinessLogic.DB_UpdateTimeoutWc2Sessions(conn);
              last_timeout = DateTime.Now;
              timeout_seconds = 5 * 60 + rnd.Next(30);
            }
          }
          else
          {
            Boolean reloaded;

            reloaded = false;

            try
            {
              if (conn.State == ConnectionState.Open)
              {
                // Call the proper method depending on the the index
                reloaded = ReloadTable((CachedTable)idx_event, conn);

                if (reloaded)
                {
                  event_reloaded[idx_event].Set();
                }
              }
            }
            catch (Exception ex)
            {
              Log.Exception(ex);
            }
            finally
            {
              if (!reloaded)
              {
                event_reload[idx_event].Set();

                System.Threading.Thread.Sleep(1000);
              }
            }
          }

          if (((TimeSpan)DateTime.Now.Subtract(last_reload_terminals)).TotalSeconds >= 10)
          {
            event_reload[(Int32)CachedTable.terminals].Set();
            last_reload_terminals = DateTime.Now;
          }
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
        finally
        {
          if (conn != null)
          {
            conn.Close();
            conn = null;
          }
        }
      }
    }
  }
}
