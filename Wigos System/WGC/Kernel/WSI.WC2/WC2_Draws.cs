//------------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_DrawManager.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR:
// 
// CREATION DATE: 
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
//                    Initial Draft.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using WSI.Common;

namespace WSI.WC2
{
  public class WC2_DrawManager
  {
    //The minimum number of balls per draw.
    static public int MIN_BALLS = 30;
    //The minimum value of a ball
    static public int MIN_BALL_NUM = 1;
    //The maximum value of a ball
    static public int MAX_BALL_NUM = 75;

    /// <summary>
    /// Returns the draw for a card and a pattern given
    /// </summary>
    /// <param name="PatternNumbers"></param>
    /// <param name="CardNumbers"></param>
    /// <returns></returns>
    static public ArrayList GetMinimumDraw(int[] PatternNumbers, int[] CardNumbers)
    {
      ArrayList balls;
      balls = new ArrayList();
      int idx_pattern;
      for (idx_pattern = 0; idx_pattern < PatternNumbers.Length; idx_pattern++)
      {
        balls.Add(CardNumbers[PatternNumbers[idx_pattern]]);
      }
      return balls;
    }




    /// <summary>
    ///  This method extract three arrays and returns the draw sorted and without repeated balls.
    /// </summary>
    /// <param name="CardNumbers1"></param>
    /// <param name="CardNumbers2"></param>
    /// <param name="SafeNumbers">This array contains the numbers that aren't in either card</param>
    /// <param name="UnsafeNumbers">This array contains the numbers that are in cards but are not in the draw array</param>
    /// <param name="Draw">This array contains the draw</param>
    /// <returns>The method returns the draw without repeated balls and sorted</returns>
    static public ArrayList ExtractNumbers(int[] CardNumbers1
                                    , int[] CardNumbers2
                                    , out ArrayList SafeNumbers //The ArrayList are always references
                                    , out ArrayList UnsafeNumbers //The ArrayList are always references
                                    , ArrayList Draw) //The ArrayList are always references
    {
      int idx_numbers;
      int idx_balls;
      ArrayList new_draw;
      Byte[] found_numbers;
      found_numbers = new Byte[MAX_BALL_NUM];
      new_draw = new ArrayList();

      SafeNumbers = new ArrayList();
      UnsafeNumbers = new ArrayList();


      for (idx_numbers = 0; (idx_numbers < CardNumbers1.Length); idx_numbers++)
      {
        found_numbers[CardNumbers1[idx_numbers] - 1] = 1;
        found_numbers[CardNumbers2[idx_numbers] - 1] = 1;
      }
      for (idx_numbers = 0; (idx_numbers < Draw.Count); idx_numbers++)
      {
        found_numbers[((int)Draw[idx_numbers]) - 1] = 2;
      }

      for (idx_balls = 0; idx_balls < MAX_BALL_NUM; idx_balls++)
      {
        if (found_numbers[idx_balls] == 0)
        {
          SafeNumbers.Add(idx_balls + 1);
        }
        else
        {
          if (found_numbers[idx_balls] == 1)
          {
            UnsafeNumbers.Add(idx_balls + 1);
          }
          else
          {
            //Removes repeated balls in the draw and sort it
            new_draw.Add(idx_balls + 1);
          }
        }
      }
      return new_draw;
    }


    /// <summary>
    ///  This method extract three arrays and returns the draw sorted and without repeated balls.
    /// </summary>
    /// <param name="CardNumbers1"></param>
    /// <param name="SafeNumbers">This array contains the numbers that aren't in either card</param>
    /// <param name="UnsafeNumbers">This array contains the numbers that are in cards but are not in the draw array</param>
    /// <param name="Draw">This array contains the draw</param>
    /// <returns>The method returns the draw without repeated balls and sorted</returns>
    static public ArrayList ExtractNumbersOnePlay(int[] CardNumbers1
                                    , out ArrayList SafeNumbers //The ArrayList are always references
                                    , out ArrayList UnsafeNumbers //The ArrayList are always references
                                    , ArrayList Draw) //The ArrayList are always references
    {
      int idx_numbers;
      int idx_balls;
      ArrayList new_draw;
      Byte[] found_numbers;
      found_numbers = new Byte[MAX_BALL_NUM];
      new_draw = new ArrayList();

      SafeNumbers = new ArrayList();
      UnsafeNumbers = new ArrayList();


      for (idx_numbers = 0; (idx_numbers < CardNumbers1.Length); idx_numbers++)
      {
        found_numbers[CardNumbers1[idx_numbers] - 1] = 1;
      }
      for (idx_numbers = 0; (idx_numbers < Draw.Count); idx_numbers++)
      {
        found_numbers[((int)Draw[idx_numbers]) - 1] = 2;
      }

      for (idx_balls = 0; idx_balls < MAX_BALL_NUM; idx_balls++)
      {
        if (found_numbers[idx_balls] == 0)
        {
          SafeNumbers.Add(idx_balls + 1);
        }
        else
        {
          if (found_numbers[idx_balls] == 1)
          {
            UnsafeNumbers.Add(idx_balls + 1);
          }
          else
          {
            //Removes repeated balls in the draw and sort it
            new_draw.Add(idx_balls + 1);
          }
        }
      }
      return new_draw;
    }
    



    /// <summary>
    /// Fills the draw with N numbers from one of the numbers arrays (SafeNumbers, UnsafeNumbers)
    /// </summary>
    /// <param name="Numbers"></param>
    /// <param name="CurrentDraw"></param>
    /// <param name="NumBalls"></param>
    /// <returns></returns>
    static public ArrayList Fill(ArrayList Numbers, ArrayList CurrentDraw, int NumBalls)
    {
      Random ball;
      int ball_num;
      ArrayList temp_draw = new ArrayList(CurrentDraw);
      int idx_num_balls;
      try
      {
        ball = new Random();
        for (idx_num_balls = 0; idx_num_balls < NumBalls; idx_num_balls++)
        {
          if (Numbers.Count > 0)
          {
            ball_num = (int)Numbers[(ball.Next(Numbers.Count))];
            Numbers.Remove(ball_num);
            temp_draw.Add(ball_num);
          }
        }
        return temp_draw;
      }
      catch
      {
        Exception ex = new Exception("Fill Error)");
        return null;
      }
    }

    /// <summary>
    /// Randomizes a list order
    /// </summary>
    /// <param name="initial_list"></param>
    /// <returns></returns>
    static public ArrayList Rearrange(ArrayList initial_list)
    {
      int idx_element;
      int current_element;
      int initial_list_count;

      ArrayList end_list;
      Random element;

      initial_list_count = initial_list.Count;
      end_list = new ArrayList();
      element = new Random();

      for (idx_element = 0; idx_element < initial_list_count; idx_element++)
      {
        current_element = element.Next(initial_list.Count);
        end_list.Add(initial_list[current_element]);
        initial_list.Remove(initial_list[current_element]);
      }
      return end_list;
    }


    /// <summary>
    /// Returns the last ball that makes the pattern match
    /// </summary>
    /// <param name="CardNumbers"></param>
    /// <param name="PatternNumbers"></param>
    /// <param name="Draw"></param>
    /// <returns></returns>
    static public int GetResultIndex(int[] CardNumbers, int [] PatternNumbers, ArrayList Draw)
    {
      int idx_position;
      int max_position;
      max_position = 0;

      for (idx_position = 0; idx_position < PatternNumbers.Length; idx_position++)
      {
        if (Draw.IndexOf((int)CardNumbers[(int)PatternNumbers[idx_position]]) > max_position)
        {
          max_position = Draw.IndexOf((int)CardNumbers[(int)PatternNumbers[idx_position]]);
        }
      }
      return max_position;
    }

    /// <summary>
    /// Algorithm designed for 2 5x5 cards with 75 balls and extracting 30 of it:
    /// 
    /// Get all the patterns where the prize is between min and max
    /// for each one its, find a pattern combination that results OK with the extracted balls. 
    ///   (OK means that the extracted balls doesn't force another bigger or equal pattern in any card)
    ///   Generate a first draw having both patterns and both cards  -> (from 0 to 30)   (Draw_Balls)
    ///   Having this draw, the algorithm checks the prize 
    ///   (Checks if the balls extracted forces another prize greatter than the desired)
    ///   Extract the rest of the balls from both cards              -> (from 0 to 50)   (Unsafe_Balls)
    ///   Extract the balls that aren't in any Card                  -> (from 25 to 50)  (Safe_Balls)
    ///   The sum of the lenghts of all this extracted balls, including the draw, must be 75 (obiously)
    ///   Calculate the initial number of balls to be inserted. number_of_balls = (75 - drawed_balls) / 3
    ///   Until the number of balls in the draw was 30 or we don't have more balls to add to the draw, do the next:
    ///     With the previous calculation we will fill the rest of the draw with number_of_balls balls
    ///     So we must add number_of_balls of Unsafe_Balls to the current draw, that generates a new draw
    ///     With the new draw we must check the prizes again
    ///     If everything is still correct 
    ///       Save the current draw
    ///       Add the same number_of_balls to the draw but from Safe_Balls array 
    ///     If the check of the prizes returns that another greatter prize is given with that draw
    ///       Get the last good draw.
    ///       If we inserted more than one ball then recover the Unsafe_Balls that we have tried to insert 
    ///         (That's because we don't know which of these balls causes the check fails)
    ///       Add the same number_of_balls to the draw but from Safe_Balls array
    ///     Substract 2 from the number_of_balls
    ///     If the number_of_balls are lesser than 1 then the number_of_balls will be 1
    ///
    /// After extracting the 30 balls, if one or both cards has prize 0 we must continue drawing balls until a pattern matches...
    /// 
    /// </summary>
    /// <param name="CardsPlay1"></param>
    /// <param name="CardsPlay2"></param>
    /// <param name="Prize1"></param>
    /// <param name="Prize2"></param>
    /// <param name="PlayedCredits1"></param>
    /// <param name="PlayedCredits2"></param>
    /// <param name="DrawResult"></param>
    /// <param name="DrawDateTime"></param>
    /// <param name="PrizePlay1"></param>
    /// <param name="PrizePlay2"></param>
    /// <returns></returns>
    static public Boolean ProcessDrawTwoPlays(ArrayList CardsPlay1, 
                                      ArrayList CardsPlay2,
                                      Int64 Prize1,
                                      Int64 Prize2,
                                      Int64 PlayedCredits1,
                                      Int64 PlayedCredits2, 
                                      out WC2_Draw DrawResult,
                                      out DateTime DrawDateTime,
                                      out ArrayList PrizePlay1, 
                                      out ArrayList PrizePlay2)
    {
      ArrayList proposed_draw;
      ArrayList filled_proposed_draw;
      int[] card_numbers1;
      int[] card_numbers2;

      int balls_to_add;
      int idx_recover;

      Decimal multiplier1;
      Decimal multiplier2;

      WC2_Pattern pattern_1;
      WC2_Pattern pattern_2;
      int[] pattern_1_positions;
      int[] pattern_2_positions;

      WC2_Card card_1;
      WC2_Card card_2;

      WC2_CardPrize card_prize_1;
      WC2_CardPrize card_prize_2;

      WC2_Prize prize_1;
      WC2_Prize prize_2;

      ArrayList patterns_1;
      ArrayList patterns_2;

      bool prize_found;
      bool bad_result;

      ArrayList safe_numbers;
      ArrayList unsafe_numbers;
      ArrayList bad_numbers;

      pattern_1 = new WC2_Pattern();
      pattern_2 = new WC2_Pattern();


      multiplier1 = (Decimal)((Decimal)Prize1 / (Decimal)PlayedCredits1);
      multiplier2 = (Decimal)((Decimal)Prize2 / (Decimal)PlayedCredits2);

      DrawResult = new WC2_Draw();
      PrizePlay1 = new ArrayList();
      PrizePlay2 = new ArrayList();
      DrawDateTime = new DateTime();

      proposed_draw = new ArrayList();
      filled_proposed_draw = new ArrayList();
      bad_numbers = new ArrayList();
      safe_numbers = new ArrayList();
      unsafe_numbers = new ArrayList();

      prize_found = false;
      bad_result = true;


      //TODO: More than one card
      card_1 = (WC2_Card)CardsPlay1[0];
      card_numbers1 = card_1.GetNumbers();

      card_2 = (WC2_Card) CardsPlay2[0];
      card_numbers2 = card_2.GetNumbers();

      //Gets the patterns that returns the specified prize.
      patterns_1 = WC2_Patterns.GetByPrize(multiplier1);
      patterns_2 = WC2_Patterns.GetByPrize(multiplier2);

      pattern_1_positions = null;
      pattern_2_positions = null;

      foreach (WC2_Pattern pat1 in patterns_1)
      {
        pattern_1_positions = pat1.GetNumbers();

        foreach (WC2_Pattern pat2 in patterns_2)
        {
          prize_found = false;

          pattern_2_positions = pat2.GetNumbers();

          //Extract the numbers of the cards from the pattern positions to force the coincidence
          proposed_draw = GetMinimumDraw(pattern_1_positions, card_numbers1);
          proposed_draw.AddRange(GetMinimumDraw(pattern_2_positions, card_numbers2));

          //Checks if the extracted numbers makes the cards have more than one prize
          if (WC2_Patterns.CheckPrize(proposed_draw, card_numbers1, multiplier1) 
           && WC2_Patterns.CheckPrize(proposed_draw, card_numbers2, multiplier2))
          {
            //only if one prize is given (or the other prizes are lesser) the proposal numbers for the draw are correct
            prize_found = true;
            bad_result = true;
            pattern_1 = pat1;
            pattern_2 = pat2;
            //The draw result must have at least MIN_BALLS balls.
            if (proposed_draw.Count < MIN_BALLS)
            { 
              proposed_draw = ExtractNumbers(card_numbers1, card_numbers2, out safe_numbers, out unsafe_numbers, proposed_draw);
              // The sum of the safe_numbers + unsafe_numbers + proposed draw must be equal to NUM_BALLS

              balls_to_add = (int)((MIN_BALLS + 2 - proposed_draw.Count) / 3);
              while (proposed_draw.Count < MIN_BALLS && safe_numbers.Count > 0 && unsafe_numbers.Count > 0)
              {
                // temporary saves the new draw to be checked
                filled_proposed_draw = Fill(unsafe_numbers, proposed_draw, balls_to_add);
                //Checks if the current draw matches with the prizes and cards given
                if (!(WC2_Patterns.CheckPrize(filled_proposed_draw, card_numbers1, multiplier1)
                       && WC2_Patterns.CheckPrize(filled_proposed_draw, card_numbers2, multiplier2)))
                {
                  if (filled_proposed_draw.Count - proposed_draw.Count > 1)
                  {
                    for (idx_recover = 0; idx_recover < filled_proposed_draw.Count - proposed_draw.Count; idx_recover++)
                    {
                      // this recovers the removed numbers if the inserted numbers are more than 1
                      unsafe_numbers.Add(filled_proposed_draw[filled_proposed_draw.Count - idx_recover - 1]);
                    }
                  }
                  else
                  {
                    bad_numbers.Add(filled_proposed_draw[filled_proposed_draw.Count-1]);
                  }
                  //If the filled draw is not correct it fills the draw with safe balls (balls that are not in any card)
                  proposed_draw = Fill(safe_numbers, proposed_draw, balls_to_add);
                }
                else
                {
                  //The temporary draw is correct, so we must save it.
                  proposed_draw = filled_proposed_draw;
                  // if the balls to be added plus the current balls are more than MIN_BALLS we must adjust the balls_to_add to fit the to MIN_BALLS
                  if (proposed_draw.Count + balls_to_add > MIN_BALLS)
                  {
                    balls_to_add = MIN_BALLS - proposed_draw.Count;
                  }
                  proposed_draw = Fill(safe_numbers, proposed_draw,balls_to_add);
                }
                // Substract 2 from the balls to be added in the next iterance
                balls_to_add -= 2;

                if (balls_to_add < 1)
                {
                  balls_to_add = 1;
                }
                // if the balls to be added plus the current balls are greatter than MIN_BALLS we must adjust the balls_to_add to fit to MIN_BALLS
                if (proposed_draw.Count + balls_to_add > MIN_BALLS)
                {
                  balls_to_add = MIN_BALLS - proposed_draw.Count;
                }
              }
              if (!(proposed_draw.Count < MIN_BALLS))
              {
                bad_result = false;
                break;
              }
            }
            else
            {
              bad_result = false;
              break;
            }
          }
          //If the check returns false we must try with next patterns
        }
        if (prize_found && !bad_result)
        {
          break;
        }
      }

      if (!prize_found || bad_result)
      {
        return false;
      }

      //To create a more credible draw
      proposed_draw = Rearrange(proposed_draw);

      //recover all discarted balls
      unsafe_numbers.AddRange(bad_numbers);

      //if one of the prizes is 0 we must continue drawing balls until it fits a pattern (before the 30th ball)
      while ((pattern_1.Max == 0) || (pattern_2.Max == 0))
      {
        proposed_draw = Fill(safe_numbers, proposed_draw, 1);
        proposed_draw = Fill(unsafe_numbers, proposed_draw, 1);
        if (pattern_1.Max == 0)
        {
          pattern_1 = WC2_Patterns.GetPattern(proposed_draw, card_numbers1, multiplier1);
        }
        if (pattern_2.Max == 0)
        {
          pattern_2 = WC2_Patterns.GetPattern(proposed_draw, card_numbers2, multiplier2);
        }
      }

      //With the obtained data the output is filled
      DrawDateTime = DateTime.Now;

      DrawResult.BallList = "";
      int idx_balls;
      DrawResult.NumBalls = proposed_draw.Count;
      for (idx_balls = 0; idx_balls < DrawResult.NumBalls; idx_balls++)
      {
        DrawResult.BallList += " " + ((int)proposed_draw[idx_balls]).ToString("00");
      }
      DrawResult.BallList = DrawResult.BallList.Trim();

      DrawResult.MaxBallsForPrize = MIN_BALLS;
      DrawResult.MinBallNumber = MIN_BALL_NUM;
      DrawResult.MaxBallNumber = MAX_BALL_NUM;

      prize_1 = new WC2_Prize();
      prize_2 = new WC2_Prize();

      card_prize_1 = new WC2_CardPrize();
      card_prize_2 = new WC2_CardPrize();

      card_prize_1.NumPositions = pattern_1.NumPositions;
      card_prize_1.PositionList = pattern_1.GetStringList();
      card_prize_1.PrizeCredits = (Int64)(multiplier1 * PlayedCredits1);
      card_prize_1.ResultIndex = GetResultIndex(card_numbers1, pattern_1.GetNumbers(), proposed_draw);
      prize_1.CardId = card_1.CardId;
      prize_1.CardType = card_1.CardType;
      prize_1.CardPrizes = new ArrayList();
      prize_1.CardPrizes.Add(card_prize_1);
      prize_1.NumCardPrizes = prize_1.CardPrizes.Count;
      PrizePlay1.Add(prize_1);

      card_prize_2.NumPositions = pattern_2.NumPositions;
      card_prize_2.PositionList = pattern_2.GetStringList();
      card_prize_2.PrizeCredits = (Int64)(multiplier2 * PlayedCredits2);
      card_prize_2.ResultIndex = GetResultIndex(card_numbers2, pattern_2.GetNumbers(), proposed_draw); ;
      prize_2.CardId = card_2.CardId;
      prize_2.CardType = card_2.CardType;
      prize_2.CardPrizes = new ArrayList();
      prize_2.CardPrizes.Add(card_prize_2);
      prize_2.NumCardPrizes = prize_2.CardPrizes.Count;
      PrizePlay2.Add(prize_2);
      return true;
    } // ProcessDrawTwoPlays

    /// <summary>
    /// Algorithm designed for 1 5x5 cards with 75 balls and extracting 30 of it:
    /// 
    /// Get all the patterns where the prize is between min and max
    /// for each one its, find a pattern combination that results OK with the extracted balls. 
    ///   (OK means that the extracted balls doesn't force another bigger or equal pattern in any card)
    ///   Generate a first draw having both patterns and both cards  -> (from 0 to 30)   (Draw_Balls)
    ///   Having this draw, the algorithm checks the prize 
    ///   (Checks if the balls extracted forces another prize greatter than the desired)
    ///   Extract the rest of the balls from both cards              -> (from 0 to 50)   (Unsafe_Balls)
    ///   Extract the balls that aren't in any Card                  -> (from 25 to 50)  (Safe_Balls)
    ///   The sum of the lenghts of all this extracted balls, including the draw, must be 75 (obiously)
    ///   Calculate the initial number of balls to be inserted. number_of_balls = (75 - drawed_balls) / 3
    ///   Until the number of balls in the draw was 30 or we don't have more balls to add to the draw, do the next:
    ///     With the previous calculation we will fill the rest of the draw with number_of_balls balls
    ///     So we must add number_of_balls of Unsafe_Balls to the current draw, that generates a new draw
    ///     With the new draw we must check the prizes again
    ///     If everything is still correct 
    ///       Save the current draw
    ///       Add the same number_of_balls to the draw but from Safe_Balls array 
    ///     If the check of the prizes returns that another greatter prize is given with that draw
    ///       Get the last good draw.
    ///       If we inserted more than one ball then recover the Unsafe_Balls that we have tried to insert 
    ///         (That's because we don't know which of these balls causes the check fails)
    ///       Add the same number_of_balls to the draw but from Safe_Balls array
    ///     Substract 2 from the number_of_balls
    ///     If the number_of_balls are lesser than 1 then the number_of_balls will be 1
    ///
    /// After extracting the 30 balls, if one or both cards has prize 0 we must continue drawing balls until a pattern matches...
    /// 
    /// </summary>
    /// <param name="CardsPlay1"></param>
    /// <param name="CardsPlay2"></param>
    /// <param name="Prize1"></param>
    /// <param name="Prize2"></param>
    /// <param name="PlayedCredits1"></param>
    /// <param name="PlayedCredits2"></param>
    /// <param name="DrawResult"></param>
    /// <param name="DrawDateTime"></param>
    /// <param name="PrizePlay1"></param>
    /// <param name="PrizePlay2"></param>
    /// <returns></returns>
    static public Boolean ProcessDrawOnePlay(ArrayList CardsPlay1,
                                      Int64 Prize1,
                                      Int64 PlayedCredits1,
                                      out WC2_Draw DrawResult,
                                      out DateTime DrawDateTime,
                                      out ArrayList PrizePlay1)
    {
      ArrayList proposed_draw;
      ArrayList filled_proposed_draw;
      int[] card_numbers1;

      int balls_to_add;
      int idx_recover;

      Decimal multiplier1;

      WC2_Pattern pattern_1;
      int[] pattern_1_positions;

      WC2_Card card_1;

      WC2_CardPrize card_prize_1;

      WC2_Prize prize_1;

      ArrayList safe_numbers;
      ArrayList unsafe_numbers;
      ArrayList bad_numbers;

      pattern_1 = new WC2_Pattern();

      multiplier1 = (Decimal)((Decimal)Prize1 / (Decimal)PlayedCredits1);

      DrawResult = new WC2_Draw();
      PrizePlay1 = new ArrayList();
      DrawDateTime = new DateTime();

      proposed_draw = new ArrayList();
      filled_proposed_draw = new ArrayList();
      bad_numbers = new ArrayList();
      safe_numbers = new ArrayList();
      unsafe_numbers = new ArrayList();

      //TODO: More than one card
      card_1 = (WC2_Card)CardsPlay1[0];
      card_numbers1 = card_1.GetNumbers();

      //Gets the patterns that returns the specified prize.
      pattern_1 = WC2_Patterns.GetByPrizeOnePlay(multiplier1);

      pattern_1_positions = null;
      pattern_1_positions = pattern_1.GetNumbers();

      //Extract the numbers of the cards from the pattern positions to force the coincidence
      proposed_draw = GetMinimumDraw(pattern_1_positions, card_numbers1);
      proposed_draw = ExtractNumbersOnePlay(card_numbers1, out safe_numbers, out unsafe_numbers, proposed_draw);
      balls_to_add = (int)((MIN_BALLS + 2 - proposed_draw.Count) / 3);
      while (proposed_draw.Count < MIN_BALLS && safe_numbers.Count > 0 && unsafe_numbers.Count > 0)
      {
        filled_proposed_draw = Fill(unsafe_numbers, proposed_draw, balls_to_add);
        if (!(WC2_Patterns.CheckPrize(filled_proposed_draw, card_numbers1, multiplier1)))
        {
          if (filled_proposed_draw.Count - proposed_draw.Count > 1)
          {
            for (idx_recover = 0; idx_recover < filled_proposed_draw.Count - proposed_draw.Count; idx_recover++)
            {
              unsafe_numbers.Add(filled_proposed_draw[filled_proposed_draw.Count - idx_recover - 1]);
            }
          }
          else
          {
            bad_numbers.Add(filled_proposed_draw[filled_proposed_draw.Count - 1]);
          }
          proposed_draw = Fill(safe_numbers, proposed_draw, balls_to_add);
        }
        else
        {
          proposed_draw = filled_proposed_draw;
          if (proposed_draw.Count + balls_to_add > MIN_BALLS)
          {
            balls_to_add = MIN_BALLS - proposed_draw.Count;
          }
          proposed_draw = Fill(safe_numbers, proposed_draw, balls_to_add);
        }
        balls_to_add -= 2;

        if (balls_to_add < 1)
        {
          balls_to_add = 1;
        }
        if (proposed_draw.Count + balls_to_add > MIN_BALLS)
        {
          balls_to_add = MIN_BALLS - proposed_draw.Count;
        }
      }

      //To create a more credible draw
      proposed_draw = Rearrange(proposed_draw);

      //recover all discarted balls
      unsafe_numbers.AddRange(bad_numbers);

      //if one of the prizes is 0 we must continue drawing balls until it fits a pattern (after the 30th ball)
      while ((pattern_1.Max == 0))
      {
        proposed_draw = Fill(safe_numbers, proposed_draw, 1);
        proposed_draw = Fill(unsafe_numbers, proposed_draw, 1);
        if (pattern_1.Max == 0)
        {
          pattern_1 = WC2_Patterns.GetPattern(proposed_draw, card_numbers1, multiplier1);
        }
      }

      //With the obtained data the output is filled
      DrawDateTime = DateTime.Now;

      DrawResult.BallList = "";
      int idx_balls;
      DrawResult.NumBalls = proposed_draw.Count;
      for (idx_balls = 0; idx_balls < DrawResult.NumBalls; idx_balls++)
      {
        DrawResult.BallList += " " + ((int)proposed_draw[idx_balls]).ToString("00");
      }
      DrawResult.BallList = DrawResult.BallList.Trim();

      DrawResult.MaxBallsForPrize = MIN_BALLS;
      DrawResult.MinBallNumber = MIN_BALL_NUM;
      DrawResult.MaxBallNumber = MAX_BALL_NUM;

      prize_1 = new WC2_Prize();

      card_prize_1 = new WC2_CardPrize();

      card_prize_1.NumPositions = pattern_1.NumPositions;
      card_prize_1.PositionList = pattern_1.GetStringList();
      card_prize_1.PrizeCredits = (Int64)(multiplier1 * PlayedCredits1);
      card_prize_1.ResultIndex = GetResultIndex(card_numbers1, pattern_1.GetNumbers(), proposed_draw);
      prize_1.CardId = card_1.CardId;
      prize_1.CardType = card_1.CardType;
      prize_1.CardPrizes = new ArrayList();
      prize_1.CardPrizes.Add(card_prize_1);
      prize_1.NumCardPrizes = prize_1.CardPrizes.Count;
      PrizePlay1.Add(prize_1);

      return true;
    } // ProcessDrawOnePlay

  }
}
