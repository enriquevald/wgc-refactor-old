//------------------------------------------------------------------------------
// Copyright � 2007 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_Processor.cs
// 
//   DESCRIPTION: WC2_Processor class
// 
//        AUTHOR: Andreu Juli�
// 
// CREATION DATE: 14-MAR-2007
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-MAR-2007 AJQ    First release.
// 10-APR-2012 MPO    Set the values "terminal name" and "winner name" according to the parameters of GetSiteJackpotParameters
//------------------------------------------------------------------------------
using System;
using System.Messaging;
using System.Collections.Generic;
using System.Text;
using System.Net;
using WSI.Common;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using WSI.WC2;
using System.Collections;

namespace WSI.WC2
{
  public class Wc2InputMsgProcessTask : Task
  {
    Object m_wc2_listener;
    Object m_wc2_client;
    String m_xml_message;

    int m_tick_arrived;
    int m_tick_dequeued;
    int m_tick_processed;

    public Wc2InputMsgProcessTask(Object Wc2Listener, Object Wc2Client, String XmlMessage)
    {
      m_wc2_listener = Wc2Listener;
      m_wc2_client = Wc2Client;
      m_xml_message = XmlMessage;
      m_tick_arrived = Environment.TickCount;
      m_tick_dequeued = m_tick_arrived;
      m_tick_processed = m_tick_arrived;
    }

    public override void Execute()
    {
      long _interval;
      SecureTcpClient _tcp_client;
      SecureTcpServer _tcp_server;

      m_tick_dequeued = Environment.TickCount;
      m_tick_processed = m_tick_dequeued;
      _interval = Misc.GetElapsedTicks(m_tick_arrived, m_tick_dequeued);
      if (_interval >= 10000)
      {
        // Ignore the task     
        Log.Warning("WC2 Message Ignored: timeout. Queued: " + _interval.ToString() + " ms. " + m_xml_message);

        return;
      }

      try
      {
        _tcp_client = (SecureTcpClient)m_wc2_client;
        _tcp_server = (SecureTcpServer)m_wc2_listener;

        if (!_tcp_server.IsConnected(_tcp_client))
        {
          // Ignore the task     
          Log.Warning("WC2 Message Ignored: disconnected. Queued: " + _interval.ToString() + " ms. " + m_xml_message);

          return;
        }
      }
      catch
      {
      }

      try
      {
        WC2_Client.ProcessWc2InputMsg(m_wc2_listener, m_wc2_client, m_xml_message);
      }
      catch
      {
      }
      finally
      {
        long _tq;
        long _tx;
        long _tt;

        m_tick_processed = Environment.TickCount;

        _tq = Misc.GetElapsedTicks(m_tick_arrived, m_tick_dequeued);
        _tx = Misc.GetElapsedTicks(m_tick_dequeued, m_tick_processed);
        _tt = Misc.GetElapsedTicks(m_tick_arrived, m_tick_processed);

        if (_tq > 10000
            || _tx > 10000
            || _tt > 20000)
        {
          Log.Warning("WC2 Message times: Queued/Process/Total = " + _tq.ToString() + "/" + _tx.ToString() + "/" + _tt.ToString() + " ----> " + m_xml_message);
        }
      }
    }

    protected internal String XmlRequest
    {
      get
      {
        return m_xml_message;
      }
    }
    public Object Wc2Client
    {
      get
      {
        return m_wc2_client;
      }
    }

  }

  public enum WC2_Pool
  {
    Default = 0,  //  1 Worker
    Enroll = 1,  //  20 Worker
    Play = 2,  // 20 Workers
  };

  public static class WC2_WorkerPool
  {
    private static WorkerPool m_pool_default = null;
    private static WorkerPool m_pool_enroll = null;
    private static WorkerPool m_pool_play = null;

    private static WC2_Pool Pool(Wc2InputMsgProcessTask Task)
    {
      WC2_MsgTypes _msg_type;
      String _tag0;
      String _tag1;
      String _str_value;
      int _idx0;
      int _idx1;

      _msg_type = WC2_MsgTypes.WC2_MsgUnknown;

      _tag0 = "<MsgType>";
      _tag1 = "</MsgType>";
      _idx0 = Task.XmlRequest.IndexOf(_tag0);
      if (_idx0 >= 0)
      {
        _idx0 = _idx0 + _tag0.Length;
        _idx1 = Task.XmlRequest.IndexOf(_tag1, _idx0);
        if (_idx1 - _idx0 > 0)
        {
          _str_value = Task.XmlRequest.Substring(_idx0, _idx1 - _idx0);
          _str_value = _str_value.Trim();
          _msg_type = (WC2_MsgTypes)WC2_MsgHeader.Decode(_str_value, WC2_MsgTypes.WC2_MsgUnknown);
        }
      }

      switch (_msg_type)
      {
        case WC2_MsgTypes.WC2_MsgEnrollTerminal:
        case WC2_MsgTypes.WC2_MsgGetParameters:
        case WC2_MsgTypes.WC2_MsgKeepAlive:
        case WC2_MsgTypes.WC2_MsgUnenrollTerminal:
        case WC2_MsgTypes.WC2_MsgGetCards:
        case WC2_MsgTypes.WC2_MsgGetPatterns:
        case WC2_MsgTypes.WC2_MsgGetJackpotInfo:
        case WC2_MsgTypes.WC2_MsgSiteJackpotInfo:
          {
            return WC2_Pool.Enroll;
          }

        case WC2_MsgTypes.WC2_MsgPlay:
        case WC2_MsgTypes.WC2_MsgCancelPlay:
          {
            return WC2_Pool.Play;
          }

        case WC2_MsgTypes.WC2_MsgUnknown:
        default:
          {
            return WC2_Pool.Default;
          }
      } // switch
    }


    public static void Init()
    {
      int _pool_workers;

      _pool_workers = 1;
      if (Environment.GetEnvironmentVariable("WC2_POOL_DEFAULT") != null)
      {
        _pool_workers = int.Parse(Environment.GetEnvironmentVariable("WC2_POOL_DEFAULT"));
      }
      m_pool_default = new WorkerPool("WC2 Pool Default", _pool_workers);

      _pool_workers = 20;
      if (Environment.GetEnvironmentVariable("WC2_POOL_ENROLL") != null)
      {
        _pool_workers = int.Parse(Environment.GetEnvironmentVariable("WC2_POOL_ENROLL"));
      }
      m_pool_enroll = new WorkerPool("WC2 Pool Enroll", _pool_workers);

      _pool_workers = 20;
      if (Environment.GetEnvironmentVariable("WC2_POOL_PLAY") != null)
      {
        _pool_workers = int.Parse(Environment.GetEnvironmentVariable("WC2_POOL_PLAY"));
      }
      m_pool_play = new WorkerPool("WC2 Pool Play", _pool_workers);

    }

    public static void Enqueue(Wc2InputMsgProcessTask Task)
    {
      WC2_Pool _pool;

      _pool = Pool(Task);

      switch (_pool)
      {
        case WC2_Pool.Default:
          m_pool_default.EnqueueTask(Task);
          break;
        case WC2_Pool.Enroll:
          m_pool_enroll.EnqueueTask(Task);
          break;
        case WC2_Pool.Play:
          m_pool_play.EnqueueTask(Task);
          break;
        default:
          Log.Error("Unknown WC2 Pool: " + _pool.ToString());
          break;
      }
    }
  }

  public class WC2_Client : IXmlSink
  {
    //private Worker wc2_msg_recv_worker;
    //private WaitableQueue wc2_msg_recv_waitable_queue;

    private static WC2_Client wc2_client;

    public static void Init()
    {
      wc2_client = new WC2_Client();
    }

    public static WC2_Client GetInstance()
    {
      return wc2_client;
    }

    public void StartWorker()
    {
      WC2_WorkerPool.Init();

      ////// - Workers input queue
      ////wc2_msg_recv_waitable_queue = new WaitableQueue();

      ////// - Message received processing worker
      ////// Start workers for WC2 requests
      ////for (int i = 0; i < 20; i++)
      ////{
      ////  wc2_msg_recv_worker = new Worker(wc2_msg_recv_waitable_queue);
      ////  wc2_msg_recv_worker.Start();
      ////}
    }

    public static void Start()
    {
      // Workers start
      wc2_client.StartWorker();
    }

    public virtual void Process(Object Wc2Listener, Object Wc2Client, String XmlMessage)
    {
      Wc2InputMsgProcessTask msg_input_task;

      msg_input_task = new Wc2InputMsgProcessTask(Wc2Listener, Wc2Client, XmlMessage);

      WC2_WorkerPool.Enqueue(msg_input_task);

      //wc2_msg_recv_waitable_queue.Enqueue(msg_input_task);
    }

    public static void ProcessWc2InputMsg(Object Wc2Listener, Object Wc2Client, String XmlMessage)
    {
      WC2_Message wc2_request;
      WC2_Message wc2_response;
      SecureTcpClient client;
      SqlConnection sql_conn;
      SqlTransaction sql_trx;
      String msg_response_xml_text;
      Boolean bool_rc;
      Boolean error;
      Int64 response_session_id;
      Int32 response_terminal_id;
      Int64 wc2_message_id;
      Boolean save_msg;
      Boolean _show_terminal_name;
      Boolean _show_winner_name;


      client = (SecureTcpClient)Wc2Client;

      sql_conn = null;
      sql_trx = null;
      error = true;
      wc2_response = null;
      save_msg = true;

      try
      {
        wc2_request = WC2_Message.CreateMessage(XmlMessage);
        wc2_response = WC2_Message.CreateMessage(wc2_request.MsgHeader.MsgType + 1);

        wc2_response.MsgHeader.PrepareReply(wc2_request.MsgHeader);

        response_session_id = wc2_request.MsgHeader.TerminalSessionId;
        response_terminal_id = client.InternalId;

        // Do NOT log/store the KeepAlive messages
        switch (wc2_request.MsgHeader.MsgType)
        {
          case WC2_MsgTypes.WC2_MsgKeepAlive:
            {
              WC2_BusinessLogic.DB_UpdateWc2SessionMessageReceived(response_terminal_id, response_session_id, wc2_request.MsgHeader.SequenceId, wc2_request.TransactionId);

              client.Send(wc2_response.ToXml());

              error = false;

              return;
            }

          case WC2_MsgTypes.WC2_MsgPlay:
            {
              // ACC 28-MAY-2010
              // No update Wc2 Session
              //WC2_BusinessLogic.DB_UpdateWc2SessionMessageReceived(response_terminal_id, response_session_id, wc2_request.MsgHeader.SequenceId, wc2_request.TransactionId);

              save_msg = false;
            }
            break;

          case WC2_MsgTypes.WC2_MsgGetJackpotInfo:
          case WC2_MsgTypes.WC2_MsgSiteJackpotInfo:
          case WC2_MsgTypes.WC2_MsgGetParameters:
          case WC2_MsgTypes.WC2_MsgCancelPlay:
          case WC2_MsgTypes.WC2_MsgGetCards:
          case WC2_MsgTypes.WC2_MsgGetPatterns:
            {
              WC2_BusinessLogic.DB_UpdateWc2SessionMessageReceived(response_terminal_id, response_session_id, wc2_request.MsgHeader.SequenceId, wc2_request.TransactionId);

              save_msg = false;
            }
            break;

          // Unreached code. 
          // break;

          default:
            break;
        }

        NetLog.Add(NetEvent.MessageReceived, client.Identity, XmlMessage);

        // Get Sql Connection 
        sql_conn = WGDB.Connection();


        //
        // Request from Gaming processing:
        //

        // Begin Transaction
        sql_trx = sql_conn.BeginTransaction();

        if (save_msg)
        {
          // Insert WC2 message.
          WC2_BusinessLogic.DB_InsertWC2Message(client.InternalId, response_session_id, false, XmlMessage, wc2_request.MsgHeader.SequenceId, out wc2_message_id, sql_trx);
        }
        else
        {
          wc2_message_id = 0;
        }

        if (wc2_request.MsgHeader.MsgType != WC2_MsgTypes.WC2_MsgEnrollTerminal)
        {
          if (client.InternalId == 0)
          {
            throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_TERMINAL_NOT_AUTHORIZED, "Terminal not enrolled");
          }

          if (save_msg)
          {
            // Check for Gaming retry.
            bool_rc = WC2_BusinessLogic.DB_CheckMsgIsRetry(client.InternalId, wc2_request.MsgHeader.SequenceId, true, out msg_response_xml_text, sql_trx);
            if (bool_rc)
            {
              // Check to send again answer back to Gaming.
              if (msg_response_xml_text != null)
              {
                WC2_Message wc2_old_response;

                wc2_old_response = WC2_Message.CreateMessage(msg_response_xml_text);
                if (wc2_old_response.MsgHeader.MsgType == wc2_response.MsgHeader.MsgType)
                {
                  client.Send(msg_response_xml_text);
                  NetLog.Add(NetEvent.MessageSent, client.Identity, "RETRY: " + msg_response_xml_text);
                }
                else
                {
                  throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_INVALID_MSG_TYPE, "Retry: previous request had another type");
                }
              }
              else
              {
                // Answer is not available. 
                // AJQ 26-SEP-2007, It is not possible to send a retry to CJ. 
                //  - The CJ messages are not stored on the DB
                //  - The answer will be sent to the Gaming System on the arrival of the CJ response
                Log.Warning("A retry from WC2 do not have an available CJ request. TerminalId: " + client.InternalId + " - SeqId: " + wc2_request.MsgHeader.SequenceId.ToString());
              }

              return;
            }
          }

          if (!Common.BatchUpdate.TerminalSession.IsActive(response_session_id))
          {
            if (wc2_request.MsgHeader.TerminalId.Length > 0)
            {
              throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_TERMINAL_SESSION_NOT_VALID, "a.TerminalSessionId is not active");
            }
            else
            {
              throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_SERVER_SESSION_NOT_VALID, "a.ServerSessionId is not active");
            }
          }

          if (save_msg)
          {
            // Insert wc2 trx 
            WC2_BusinessLogic.DB_InsertWC2Transaction(client.InternalId, response_session_id, wc2_request.MsgHeader.SequenceId, true, wc2_request.TransactionId, wc2_message_id, sql_trx);
          }
        }

        if (wc2_request.MsgHeader.MsgType.ToString().IndexOf("Reply") >= 0)
        {
          return;
        }

        if (wc2_request.MsgHeader.MsgType == WC2_MsgTypes.WC2_MsgEnrollTerminal)
        {
          if (wc2_request.MsgHeader.TerminalId.Length == 0)
          {
            throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_INVALID_TERMINAL_ID, "TerminalId cannot be empty");
          }
          if (wc2_request.MsgHeader.TerminalSessionId != 0)
          {
            throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_INVALID_TERMINAL_SESSION_ID, "TerminalSessionId should be 0");
          }
        }
        else
        {
          if (wc2_request.MsgHeader.TerminalId.Length > 0)
          {
            if (wc2_request.MsgHeader.TerminalSessionId == 0)
            {
              throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_INVALID_TERMINAL_SESSION_ID, "TerminalSessionId cannot be 0");
            }

            if (!Common.BatchUpdate.TerminalSession.IsActive(wc2_request.MsgHeader.TerminalSessionId))
            {
              throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_TERMINAL_SESSION_NOT_VALID, "TerminalSessionId is not active");
            }
          }
        }

        if (wc2_request.MsgHeader.TerminalId.Length > 0)
        {
          if (wc2_request.MsgHeader.TerminalSessionId > 0)
          {
            if (!Common.BatchUpdate.TerminalSession.IsActive(wc2_request.MsgHeader.TerminalSessionId))
            {
              throw new WC2_Exception(WC2_ResponseCodes.WC2_RC_TERMINAL_NOT_AUTHORIZED, "TerminalSession is not active");
            }
          }
        }

        switch (wc2_request.MsgHeader.MsgType)
        {
          case WC2_MsgTypes.WC2_MsgEnrollTerminal:
            {
              // Enqueue enroll in pending enrolls queue.
              WC2_PendingEnrollsQueue.GetInstance().EnqueueEnroll(new EnrollQueueElement(Wc2Listener, Wc2Client, wc2_request));

              // NO RESPONSE IS NEEDED: The Enroll worker sends the response.

              // Commit the message & trx
              sql_trx.Commit();

              error = false;

              return;
            }
          // Unreachable code detected
          // break;

          case WC2_MsgTypes.WC2_MsgUnenrollTerminal:
            {
              // Enqueue enroll in pending enrolls queue.
              WC2_PendingEnrollsQueue.GetInstance().EnqueueEnroll(new EnrollQueueElement(Wc2Listener, Wc2Client, wc2_request));

              // NO RESPONSE IS NEEDED: The Enroll worker sends the response.

              // Commit the message & trx
              sql_trx.Commit();

              error = false;

              return;
            }
          // Unreachable code detected
          // break;

          case WC2_MsgTypes.WC2_MsgGetParameters:
            {
              WC2_MsgGetParameters request;
              WC2_MsgGetParametersReply response;
              ArrayList _ftp_locations;
              string _terminal_type;
              int _idx_ftp_location;
              string _ftp_location;

              request = (WC2_MsgGetParameters)wc2_request.MsgContent;
              response = (WC2_MsgGetParametersReply)wc2_response.MsgContent;

              WC2_BusinessLogic.DB_UpdateTerminalSoftwareVersion(client.InternalId, request.ClientId, request.BuildId, sql_trx);

              // response.ConnectionTimeout --> Default
              // response.KeepAliveInterval --> Default
              response.JackpotRequestInterval = 10;
              WC2_BusinessLogic.DB_GetSoftwareVersion(client.InternalId, out response.ClientId, out response.BuildId, out _terminal_type, sql_trx);
              _ftp_locations = WC2_BusinessLogic.DG_GetFtpLocations(sql_trx);
              for (_idx_ftp_location = 0; _idx_ftp_location < _ftp_locations.Count; _idx_ftp_location++)
              {
                _ftp_location = (string)_ftp_locations[_idx_ftp_location];

                if (_ftp_location.Length > 0)
                {
                  response.Locations.Add(_ftp_location);
                }
              }
              response.NumLocations = response.Locations.Count;
            }
            break;

          case WC2_MsgTypes.WC2_MsgGetCards:
            {
              WC2_MsgGetCards request;
              WC2_MsgGetCardsReply response;

              request = (WC2_MsgGetCards)wc2_request.MsgContent;
              response = (WC2_MsgGetCardsReply)wc2_response.MsgContent;

              response.Cards = WC2_BusinessLogic.DB_WC2_GetCards(client.InternalId, request.NumCards, request.CardType, sql_trx);
              response.NumCards = response.Cards.Count;
            }
            break;

          case WC2_MsgTypes.WC2_MsgGetPatterns:
            {
              WC2_MsgGetPatternsReply response;

              response = (WC2_MsgGetPatternsReply)wc2_response.MsgContent;

              response.Patterns = WC2_BusinessLogic.DB_WC2_GetPatterns(sql_trx);
              response.NumPatterns = response.Patterns.Count;
            }
            break;

          case WC2_MsgTypes.WC2_MsgPlay:
            {
              WC2_MsgPlay request;
              WC2_MsgPlayReply response;

              request = (WC2_MsgPlay)wc2_request.MsgContent;
              response = (WC2_MsgPlayReply)wc2_response.MsgContent;

              response.TransactionId = request.TransactionId;
              response.Cards = request.Cards;
              response.NumCards = request.NumCards;

              // Enqueue play in pending plays queue.
              if (!WC2_PendingPlaysQueue.GetInstance().EnqueuePlay(new PlayQueueElement(Wc2Listener, Wc2Client, wc2_request)))
              {
                // NO RESPONSE IS NEEDED: The Draw worker sends the response.

                // Commit the message & trx
                sql_trx.Commit();

                error = false;

                return;
              }

              wc2_response.MsgHeader.ResponseCode = WC2_ResponseCodes.WC2_RC_PLAY_DELAYED;
              wc2_response.MsgHeader.ResponseCodeText = "Play delayed";
            }
            break;

          case WC2_MsgTypes.WC2_MsgCancelPlay:
            {
              WC2_MsgCancelPlay request;
              WC2_MsgCancelPlayReply response;

              request = (WC2_MsgCancelPlay)wc2_request.MsgContent;
              response = (WC2_MsgCancelPlayReply)wc2_response.MsgContent;

              response.TransactionId = request.TransactionId;

              if (!WC2_PendingPlaysQueue.GetInstance().CancelPlay(client.InternalId, new PlayQueueElement(Wc2Listener, Wc2Client, wc2_request)))
              {
                wc2_response.MsgHeader.ResponseCode = WC2_ResponseCodes.WC2_RC_CANCEL_NOT_ALLOWED;
                wc2_response.MsgHeader.ResponseCodeText = "Cancel play not allowed";
              }
            }
            break;

          case WC2_MsgTypes.WC2_MsgGetJackpotInfo:
            {
              WC2_MsgGetJackpotInfo request;
              WC2_MsgGetJackpotInfoReply response;

              request = (WC2_MsgGetJackpotInfo)wc2_request.MsgContent;
              response = (WC2_MsgGetJackpotInfoReply)wc2_response.MsgContent;

              response.Jackpots = WC2_JackpotManager.WC2_GetJackpotInfo(client.InternalId,
                                                                        request.Denomination,
                                                                        request.TotalBetCredits);
              response.NumJackpots = response.Jackpots.Count;


              response.JackpotsHistory = WC2_JackpotManager.WC2_GetJackpotHistory();
              response.NumJackpotsHistory = response.JackpotsHistory.Count;


              response.Messages = WC2_JackpotManager.WC2_GetJackpotMessages();
              response.NumMessages = response.Messages.Count;

              WC2_JackpotManager.WC2_GetJackpotParameters(out response.Mode,
                                                          out response.BlockInterval,
                                                          out response.AnimationInterval,
                                                          out response.RecentHistoryInterval,
                                                          out response.MinBlockAmount);

            }
            break;

          case WC2_MsgTypes.WC2_MsgSiteJackpotInfo:
            {
              WC2_MsgSiteJackpotInfo request;
              WC2_MsgSiteJackpotInfoReply response;
              SiteJackpot[] _jackpot;
              WC2_Jackpot _wc2_jackpot;
              WC2_Jackpot_History _wc2_hist;
              ArrayList _sj_list;
              ArrayList _aux;


              request = (WC2_MsgSiteJackpotInfo)wc2_request.MsgContent;
              response = (WC2_MsgSiteJackpotInfoReply)wc2_response.MsgContent;

              _jackpot = WC2_SiteJackpotManager.Jackpot;
              _sj_list = new ArrayList();
              foreach (SiteJackpot _sj in _jackpot)
              {
                _wc2_jackpot = new WC2_Jackpot();
                _wc2_jackpot.Index = _sj.index;
                _wc2_jackpot.Name = _sj.name;
                _wc2_jackpot.Amount = _sj.accumulated;
                _wc2_jackpot.MinimumBetAmount = 0; // AJQ 11-DEC-2012, It is possible to send the minimum bet (_sj.minimum_bet)
                _sj_list.Add(_wc2_jackpot);
              }
              response.Jackpots = _sj_list;
              response.NumJackpots = _sj_list.Count;

              _show_terminal_name = false;
              _show_winner_name = false;

              // Get the site jackpot parameters
              WC2_SiteJackpotManager.GetSiteJackpotParameters(out response.Mode,
                                                              out response.BlockInterval,
                                                              out response.AnimationInterval01,
                                                              out response.AnimationInterval02,
                                                              out response.RecentHistoryInterval,
                                                              out response.MinBlockAmount,
                                                              out _show_winner_name,
                                                              out _show_terminal_name);

              _aux = WC2_SiteJackpotManager.History;
              _sj_list = new ArrayList();
              foreach (SiteJackpotHistory _sjh in _aux)
              {
                _wc2_hist = new WC2_Jackpot_History();
                _wc2_hist.Index = _sjh.index;
                _wc2_hist.Name = _sjh.name;
                _wc2_hist.Amount = _sjh.amount;
                _wc2_hist.Date = _sjh.awarded;
                _wc2_hist.PlayerName = _show_winner_name ? _sjh.player_name : String.Empty;
                _wc2_hist.TerminalName = _show_terminal_name ? _sjh.terminal_name : String.Empty;
                _sj_list.Add(_wc2_hist);
              }
              response.JackpotsHistory = _sj_list;
              response.NumJackpotsHistory = _sj_list.Count;


              response.Messages = WC2_SiteJackpotManager.Messages;
              response.NumMessages = response.Messages.Count;

            }
            break;

          default:
            break;
        }

        String str_response;

        str_response = wc2_response.ToXml();

        if (save_msg)
        {
          WC2_BusinessLogic.DB_InsertWC2Message(response_terminal_id, response_session_id, true, str_response, wc2_response.MsgHeader.SequenceId, out wc2_message_id, sql_trx);
        }

        if (wc2_request.MsgHeader.MsgType != WC2_MsgTypes.WC2_MsgEnrollTerminal)
        {
          if (save_msg)
          {
            // Update wc2 trx
            WC2_BusinessLogic.DB_UpdateWC2Transaction(response_terminal_id, wc2_request.MsgHeader.SequenceId, true, wc2_message_id, sql_trx);
          }
        }

        // End Transaction
        sql_trx.Commit();

        client.Send(str_response);
        NetLog.Add(NetEvent.MessageSent, client.Identity, str_response);

        error = false;
      }
      catch (WC2_Exception wc2_ex)
      {
        String wc2_error_response;

        wc2_error_response = null;
        try
        {
          if (wc2_response == null)
          {
            wc2_response = WC2_Message.CreateMessage(WC2_MsgTypes.WC2_MsgError);
          }
          wc2_response.MsgHeader.ResponseCode = wc2_ex.ResponseCode;
          wc2_response.MsgHeader.ResponseCodeText = wc2_ex.ResponseCodeText;

          // Create error response
          wc2_error_response = wc2_response.ToXml();

          client.Send(wc2_error_response);
          NetLog.Add(NetEvent.MessageSent, client.Identity, wc2_error_response);
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      }
      catch (Exception ex)
      {
        NetLog.Add(NetEvent.MessageFormatError, client.Identity + " a.1)", ex.Message);
        NetLog.Add(NetEvent.MessageFormatError, client.Identity + " b.1)", XmlMessage);

        Log.Exception(ex);

        return;
      }
      finally
      {
        if (sql_trx != null)
        {
          if (error && sql_trx.Connection != null)
          {
            sql_trx.Rollback();
          }

          sql_trx.Dispose();
          sql_trx = null;
        }

        // Close connection
        if (sql_conn != null)
        {
          sql_conn.Close();
          sql_conn = null;
        }
      }
    }
  }
}
