//------------------------------------------------------------------------------
// Copyright (c) 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_DirectoryService.cs
// 
//   DESCRIPTION: The WC2 directory service
// 
//        AUTHOR: Andreu Julia
// 
// CREATION DATE: 12-SEP-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 12-SEP-2008 AJQ    First release.
//------------------------------------------------------------------------------

using System;
using System.Text;
using System.Xml;
using System.Net;
using System.Net.Sockets;
using System.Data;
using System.Threading;

using WSI.Common;
using WSI.WC2;

namespace WSI.WC2
{
  /// <summary>
  /// The WC2 Directory Service
  /// </summary>
  public class WC2_DirectoryService : UdpServer
  {
    #region MEMBERS
    // ------------------------------------------------------------------------- MEMBERS
    IPEndPoint m_service_ip_endpoint;         // Service IP EndPoint

    #endregion // MEMBERS

    #region PUBLIC METHODS
    // ------------------------------------------------------------------------- PUBLIC METHODS
    /// <summary>
    /// Creates a new instance of a WC2 Directory Service
    /// </summary>
    public WC2_DirectoryService(IPEndPoint ServiceIPEndPoint)
      : base(Protocol.PORT_NUMBER)
    {
      m_service_ip_endpoint = ServiceIPEndPoint;
      
      base.Start();
    } // WC2_DirectoryService

    #endregion // PUBLIC METHODS

    #region PRIVATE METHODS
    // ------------------------------------------------------------------------- PRIVATE METHODS

    /// <summary>
    /// Process the received UPD datagram.
    /// - Only WC2_MsgRequestService is allowed
    /// - 
    /// </summary>
    /// <param name="RemoteIPEndPoint">Source IP endpoint</param>
    /// <param name="Datagram">Received Datagram</param>
    protected override void ProcessDataReceived(IPEndPoint RemoteIPEndPoint, Byte[] Datagram)
    {
      String xml_msg;
      WC2_Message wc2_request;
      WC2_Message wc2_response;
      WC2_MsgRequestService request;
      WC2_MsgRequestServiceReply response;

      try
      {
        // Data received?
        if (Datagram.Length == 0)
        {
          return;
        }

        // Datagram to String using the current Encoding
        xml_msg = this.Encoding.GetString(Datagram);

        // Build the WC2 Request
        wc2_request = WC2_Message.CreateMessage(xml_msg);

        // Only WC2_MsgRequestService allowed
        if (wc2_request.MsgHeader.MsgType != WC2_MsgTypes.WC2_MsgRequestService)
        {
          // Ignore message
          return;
        }
        //
        // Process Request: WC2_MsgRequestService
        //
        request = (WC2_MsgRequestService)wc2_request.MsgContent;

        // - Check service name
        if (String.Equals(request.ServiceName.Trim(), Protocol.SERVICE_NAME, StringComparison.InvariantCultureIgnoreCase) == false)
        {
          return;
        }

        wc2_response = WC2_Message.CreateMessage(WC2_MsgTypes.WC2_MsgRequestServiceReply);
        response = (WC2_MsgRequestServiceReply)wc2_response.MsgContent;
        wc2_response.MsgHeader.SequenceId = wc2_request.MsgHeader.SequenceId;
        response.ServiceAddress = m_service_ip_endpoint;
        wc2_response.MsgHeader.ResponseCode = WC2_ResponseCodes.WC2_RC_OK;

        // Send the response to the received ReplyTo address
        // AJQ 15-SEP-08, reply to the source IP (ignore the ReplyToAddress)
        Send(RemoteIPEndPoint, wc2_response.ToXml());
      }
      catch (Exception)
      {
      }
      finally
      {
      }
    }
    #endregion // PRIVATE METHODS

  } // class WC2_DirectoryService

  public class WC2_QueryService: UDP_QueryService 
  {
    protected override int RequestToPortNumber()
    {
      return Protocol.PORT_NUMBER;
    }
    protected override int ServicePortNumber()
    {
      return Protocol.SERVICE_PORT_NUMBER;
    }
    protected override string ProtocolName()
    {
      return Protocol.PROTOCOL_NAME;
    }

    protected override Byte [] GetRequestDatagram(IPEndPoint LocalEndPoint)
    {
      WC2_Message           wc2_request;
      WC2_MsgRequestService wc2_content;
      
      wc2_request = WC2_Message.CreateMessage(WC2_MsgTypes.WC2_MsgRequestService);
      
      wc2_content = (WC2_MsgRequestService) wc2_request.MsgContent;

      wc2_content.ServiceName = Protocol.SERVICE_NAME;
      wc2_content.ReplyToUDPAddress = LocalEndPoint;
      
      return Encoding.UTF8.GetBytes(wc2_request.ToXml());
    }
    
    protected override IPEndPoint GetResponseIPEndPoint(byte[] ReceivedDatagram)
    {
      String xml_response;
      WC2_Message wc2_response;
      WC2_MsgRequestServiceReply wc2_content;
      IPEndPoint ipe;
      
      ipe = null;

      try
      {
        xml_response = Encoding.UTF8.GetString(ReceivedDatagram);
        wc2_response = WC2_Message.CreateMessage(xml_response);
        if (wc2_response.MsgHeader.ResponseCode == WC2_ResponseCodes.WC2_RC_OK)
        {
          wc2_content = (WC2_MsgRequestServiceReply)wc2_response.MsgContent;

          ipe = wc2_content.ServiceAddress;
        }
      }
      catch (Exception)
      {
      }
      finally 
      {
      }
      
      wc2_content = null;
      wc2_response = null;
      xml_response = null;

      return ipe;

    }
  }

} // namespace WSI.WC2 
