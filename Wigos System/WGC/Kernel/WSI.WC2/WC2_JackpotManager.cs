//------------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_JackpotManager.cs
// 
//   DESCRIPTION: WC2_JackpotManager class
// 
//        AUTHOR: Alberto Cuesta
// 
// CREATION DATE: 10-SEP-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 10-SEP-2008 ACC    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;
using System.Timers;
using System.Security.Cryptography;
using System.Collections;

namespace WSI.WC2
{

  public static class SpecialJackpotLogger
  {
    private static Boolean m_logger_enabled = false;
    private static Int32 m_last_enabled_info_tick = 0;
    private static Int32 m_last_write_thread_tick = 0;
    private static Int32 m_last_write_check_tick = 0;

    //------------------------------------------------------------------------------
    // PURPOSE : Set Logger Enabled
    //
    //  PARAMS :
    //      - INPUT :  
    //          - Enabled
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void SetLoggerEnabled(Boolean Enabled)
    {
      m_logger_enabled = Enabled;
    } // JackpotEnabled

    //------------------------------------------------------------------------------
    // PURPOSE : Write in logger status
    //
    //  PARAMS :
    //      - INPUT :  
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void LoggerStatusInfo()
    {
      String _msg;

      if (m_logger_enabled)
      {
        _msg = "Special Logger Enabled";
      }
      else
      {
        _msg = "Special Logger Disabled";
      }

      // 1 hour without write in logger
      if (Misc.GetElapsedTicks(m_last_enabled_info_tick) < 7200000)  // 2 hour
      {
        return;
      }

      Log.Message("*** JACKPOT MANAGER *** " + _msg);

      m_last_enabled_info_tick = Environment.TickCount;

    } // LoggerStatusInfo

    //------------------------------------------------------------------------------
    // PURPOSE : Insert in logger string
    //
    //  PARAMS :
    //      - INPUT :  
    //          - IsThread
    //          - Message
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void LoggerInsert(ref String pLoggerStr, Boolean IsThread, String Message)
    {
      String _module;

      if (IsThread)
      {
        _module = "Thread";
      }
      else
      {
        _module = "CheckJackpot";
      }

      pLoggerStr += "*** JACKPOT MANAGER - " + _module + " *** " + Message + "\r\n";

    } // LoggerInsert

    //------------------------------------------------------------------------------
    // PURPOSE : Write in logger
    //
    //  PARAMS :
    //      - INPUT :  
    //          - IsThread
    //          - Message
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    public static void LoggerFlush(Boolean IsThread, String LoggerStr)
    {
      LoggerFlush(IsThread, LoggerStr, false);

    } // LoggerFlush

    public static void LoggerFlush(Boolean IsThread, String LoggerStr, Boolean ForceWrite)
    {
      Boolean _write_in_logger;

      if (!m_logger_enabled)
      {
        return;
      }

      // 1 hour without write in logger
      _write_in_logger = false;
      if (IsThread)
      {
        if (Misc.GetElapsedTicks(m_last_write_thread_tick) > 3600000)  // 1 hour
        {
          _write_in_logger = true;
        }
      }
      else
      {
        if (Misc.GetElapsedTicks(m_last_write_check_tick) > 3600000)  // 1 hour
        {
          _write_in_logger = true;
        }
      }

      if (_write_in_logger || ForceWrite)
      {
        Log.Message(LoggerStr);

        if (IsThread)
        {
          m_last_write_thread_tick = Environment.TickCount;
        }
        else
        {
          m_last_write_check_tick = Environment.TickCount;
        }
      }

    } // LoggerFlush

  } // SpecialJackpotLogger class

  public static class WC2_JackpotManager
  {
    static Thread m_jackpot_thread;
    static Boolean[] m_is_time_to_check = new Boolean[3];
    static Decimal[] m_jackpot_minimum_bet = new Decimal[3];
    static Decimal[] m_jackpot_minimum = new Decimal[3];
    static Decimal[] m_jackpot_maximum = new Decimal[3];
    static Decimal[] m_jackpot_average = new Decimal[3];
    static Int32[] m_fake_hits = new Int32[3];
    static Decimal[] m_jackpot_contribution_pct = new Decimal[3];
    static Decimal m_jackpot_contribution;
    static Int64[] m_timestamp_instance = new Int64[3];
    static Random m_random = new Random();

    static Decimal m_jackpot_contribution_amount_to_add = 0;
    static Mutex m_lock = new Mutex ();

    private static DataTable m_dt_jackpot_info = new DataTable("JACKPOT_INFO");
    private static ReaderWriterLock m_jackpot_info_lock = new ReaderWriterLock();

    private static DataTable m_dt_jackpot_history = new DataTable("JACKPOT_HISTORY");
    private static ReaderWriterLock m_jackpot_history_lock = new ReaderWriterLock();

    private static DataTable m_dt_jackpot_messages = new DataTable("JACKPOT_MESSAGES");
    private static ReaderWriterLock m_jackpot_messages_lock = new ReaderWriterLock();

    private static DataTable m_dt_jackpot_params = new DataTable("JACKPOT_PARAMS");
    private static ReaderWriterLock m_jackpot_params_lock = new ReaderWriterLock();

    /// <summary>
    /// Initialize class attributes.
    /// </summary>
    public static void Init()
    {
      int _seed;
      RNGCryptoServiceProvider _rng_csp;
      Byte[] _raw_bytes;

      m_jackpot_thread = new Thread(JackpotManagerThread);
      m_jackpot_thread.Name = "WC2_JackpotManager";

      m_is_time_to_check[0] = false;
      m_is_time_to_check[1] = false;
      m_is_time_to_check[2] = false;
      
      // AJQ 21-ABR-2010, Use the RNG_CSP to generate a seed for the 'normal' random.
      _seed = 0;

      try
      {
        _rng_csp = new RNGCryptoServiceProvider();
        _raw_bytes = new byte[4];
        _rng_csp.GetBytes(_raw_bytes);
        foreach (Byte _value in _raw_bytes)
        {
          _seed *= 8;
          _seed += _value;
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      _seed += Environment.TickCount;
      _seed += (int)DateTime.Now.Ticks;
      m_random = new Random(_seed);
    }

    /// <summary>
    /// Start WC2_JackpotManager thread.
    /// </summary>
    public static void Start()
    {
      m_jackpot_thread.Start();
    }

    /// <summary>
    /// Check if current play is suitable for jackpot award (enable, working day, working hour, ...)
    /// </summary>
    /// <returns></returns>
    public static Boolean IsTimeToCheckJackpot()
    {
      return m_is_time_to_check[0]
            || m_is_time_to_check[1]
            || m_is_time_to_check[2];
    }


    /// <summary>
    /// Gets the hit to determine if the jackpot is given
    /// </summary>
    /// <param name="BetAmount"></param>
    /// <returns></returns>
    public static int GetTheHit(Decimal BetAmount, int IdxJackpot)
    {
      Decimal total_contribution;
      Decimal individual_contribution;
      int     _hits;

      total_contribution = BetAmount * (m_jackpot_contribution / 100);
      individual_contribution = total_contribution * (m_jackpot_contribution_pct[IdxJackpot] / 100);
      _hits = GetTheIndividualHit(individual_contribution, IdxJackpot);

      if (_hits == 0)
      {
        Log.Warning("m_jackpot_hits[" + IdxJackpot.ToString() + "] is 0");
        Log.Warning("BetAmount: " + BetAmount.ToString());
        Log.Warning("m_jackpot_contribution:" + m_jackpot_contribution.ToString());
        Log.Warning("m_jackpot_contribution_pct["+IdxJackpot.ToString()+"]: "+m_jackpot_contribution_pct[IdxJackpot].ToString() );
        Log.Warning("m_jackpot_average[" + IdxJackpot + "]: " + m_jackpot_average[IdxJackpot].ToString());
        Log.Warning("");//blank line
      }
      return _hits;
    }

    /// <summary>
    /// Gets the hit to determine if the jackpot is given
    /// </summary>
    /// <param name="BetAmount"></param>
    /// <returns></returns>
    public static int GetTheIndividualHit(Decimal IndividualContribution, int IdxJackpot)
    {
      int _hit;
      
      if (IndividualContribution != 0)
      {
        _hit = (int)Math.Truncate(m_jackpot_average[IdxJackpot] / IndividualContribution);
        
        if ( m_fake_hits[IdxJackpot] != 0 )
        {
          if ( m_fake_hits[IdxJackpot] < _hit )
          {
            _hit = m_fake_hits[IdxJackpot];
          }
          else 
          {
            _hit = _hit / 10;   // Increase 10 times the probability
          }
        }
        if ( _hit < 10 )
        {
          _hit = 10; // Minimum 10%
        }
        return _hit;
      }
      else
      {
        Log.Warning("Individual Contribution is 0, this shuld not be possible");

        return 0;
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Compute the number of 'Hits' to award the jackpot in the next hour
    //
    //  PARAMS :
    //      - INPUT :  
    //          - IdxJackpot
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    private static void ComputeFakeHits(int IdxJackpot)
    {
      int _idx_row;
      Decimal _accumulated;
      Decimal _maximum;
      int     _hits;

      _hits = 0;
      
      m_jackpot_info_lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
      try
      {
        _idx_row = IdxJackpot;
        if ( m_dt_jackpot_info.Rows.Count > _idx_row )
        {
          _accumulated = (Decimal)m_dt_jackpot_info.Rows[_idx_row]["C2JC_ACCUMULATED"];
          _maximum = (Decimal)m_dt_jackpot_info.Rows[_idx_row]["C2JI_MAXIMUM"];

          if (_accumulated > _maximum)
          {
            _hits = 1000; // Probability = 0.1%
          }
        }
        return;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Computing FakeHits");

        return;
      }
      finally
      {
        m_fake_hits[IdxJackpot] = _hits; 
        
        m_jackpot_info_lock.ReleaseReaderLock();
      }
    }

    /// <summary>
    /// Gets the hit to determine if the jackpot is given
    /// </summary>
    /// <param name="BetAmount"></param>
    /// <returns></returns>
    public static Decimal IndividualCompensation(Decimal IndividualContribution, int IdxJackpot)
    {
      Decimal individual_compensation;
      int hit;

      hit = GetTheIndividualHit(IndividualContribution, IdxJackpot);
      if (hit != 0)
      {
        individual_compensation = m_jackpot_minimum[IdxJackpot] / hit;
      }
      else
      {
        Log.Warning("The hit can not be 0");

        individual_compensation = 0;
      }

      individual_compensation = Math.Min(individual_compensation, IndividualContribution);

      return individual_compensation;
    }

    /// <summary>
    /// Calculates the contribution of the individual play.
    /// </summary>
    /// <param name="Play"></param>
    /// <returns></returns>
    public static Decimal PlayJackpotContribution(WC2_MsgPlay Play)
    {
      Decimal bet_amount;
      Decimal pct;
      Decimal contribution;

      pct = (Decimal)m_jackpot_contribution;
      pct *= (Decimal)0.01;

      bet_amount = (Decimal)Play.Denomination;
      bet_amount *= (Decimal)0.01;
      bet_amount *= (Decimal)Play.TotalPlayedCredits;

      contribution = bet_amount * pct;

      return contribution;
    } // GetTotalContribution

    /// <summary>
    /// Add jackpot contribution
    /// </summary>
    /// <param name="JackpotAmount"></param>
    /// <returns></returns>
    public static void AddJackpotContribution(Decimal JackpotAmount)
    {
      try
      {
        m_lock.WaitOne();

        m_jackpot_contribution_amount_to_add += JackpotAmount;
      }
      finally
      {
        m_lock.ReleaseMutex();
      }

    } // AddJackpotContribution

    /// <summary>
    /// 
    /// </summary>
    /// <param name="JackpotAmount"></param>
    private static void JackpotContributionToDatabase ()
    {
      SqlConnection  _sql_conn;
      SqlTransaction _sql_trx;
      Boolean _error;
      Decimal _amount_to_add;

      try
      {
        m_lock.WaitOne();

        _amount_to_add = m_jackpot_contribution_amount_to_add;
        m_jackpot_contribution_amount_to_add = 0;
      }
      finally
      {
        m_lock.ReleaseMutex();
      }

      if (_amount_to_add == 0)
      {
        return;
      }

      _sql_conn = null;
      _sql_trx = null;
      _error = true;

      try
      {
        _sql_conn = WGDB.Connection();
        _sql_trx = _sql_conn.BeginTransaction();

        WC2_BusinessLogic.DB_WC2_SetJackpotContribution(_amount_to_add, _sql_trx);

        _sql_trx.Commit();

        _error = false;
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception: JackpotContributionUpdateDB.");
      }
      finally
      {
        // AJQ 11-JUN-2010, Restore the contribution amount on any error
        if ( _error )
        {
          AddJackpotContribution (_amount_to_add);
        }
        
        if ( _sql_trx != null )
        {
          if (_sql_trx.Connection != null)
          {
            try
            {
              _sql_trx.Rollback();
            }
            catch (Exception _ex)
            {
              Log.Exception(_ex);
            }
            if (!_error)
            {
              Log.Message("*** ERROR DB TRX ROLLBACK on JackpotContributionUpdateDB.");
            }
          }
          _sql_trx = null;
        }

        // Close connection
        if ( _sql_conn != null )
        {
          try 
          {
            _sql_conn.Close();
            _sql_conn = null;
          }
          catch 
          {
            ;
          }
        }
      }
    } // JackpotContributionUpdateDB


    /// <summary>
    /// Check for award the jackpot.
    /// </summary>
    /// <param name="BetAmount"></param>
    /// <param name="PlayId"></param>
    /// <param name="TerminalId"></param>
    /// <param name="TerminalName"></param>
    /// <param name="GameId"></param>
    /// <param name="GameName"></param>
    /// <param name="JackpotId"></param>
    /// <param name="JackpotIndex"></param>
    /// <param name="JackpotName"></param>
    /// <param name="JackpotAmount"></param>
    /// <returns>true if Jackpot is awarded</returns>
    public static Boolean CheckJackpot(Decimal BetAmount,
                                       Int64 PlayId,
                                       Int32 TerminalId,
                                       String GameName,
                                       out Int64 JackpotId,
                                       out Int32 JackpotIndex,
                                       out String JackpotName,
                                       out Decimal JackpotAmount,
                                       SqlTransaction SqlTrx)
    {
      Int32 idx_jackpot;
      SqlCommand sql_command;
      SqlParameter p;
      String sql_str;
      Int32 num_rows_updated;
      Int32 num_rows_inserted;
      Boolean jackpot_award;
      DataTable dt_instances;
      DataTable dt_counters;
      Decimal new_accumulated;
      Decimal add_to_compensate;
      Boolean awarded;
      int _hit;
      int _hit_jackpot;
      String _special_logger_msg;

      // Initialize variables
      awarded = false;
      dt_counters = null;
      JackpotId = 0;
      JackpotIndex = 0;
      JackpotName = "";
      JackpotAmount = 0;
      _hit = 0;
      _hit_jackpot = 0;
      jackpot_award = false;
      _special_logger_msg = "\r\n\r\n";

      try
      {
        SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, false, "Init CheckJackpot ... - OK");

        //
        // Check minimum bet for award jackpot.
        //
        for (idx_jackpot = 0; idx_jackpot < m_jackpot_minimum_bet.Length; idx_jackpot++)
        {
          if (!m_is_time_to_check[idx_jackpot])
          {
            SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, false, "Jackpot (" + idx_jackpot.ToString() + ") is time: FALSE - UNEXPECTED");

            continue;
          }
          if (BetAmount >= m_jackpot_minimum_bet[idx_jackpot])
          {
            _hit = GetTheHit(BetAmount, idx_jackpot);
            if (_hit > 1)
            {
              lock (m_random)
              {
                _hit_jackpot = (1 + _hit) / 2;

                if (m_random.Next(1, _hit) == _hit_jackpot)
                {
                  jackpot_award = true;

                  break;
                }
                else
                {
                  SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, false, "Jackpot (" + idx_jackpot.ToString() + ") Random <> hit - OK");
                }
              }
            }
            else
            {
              SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, false, "Jackpot (" + idx_jackpot.ToString() + ") hit <= 1 - UNEXPECTED");
            }
          }
          else
          {
            SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, false, "Jackpot (" + idx_jackpot.ToString() + ") BetAmount < m_jackpot_minimum_bet - UNEXPECTED");
          }
        } // for

        if (!jackpot_award)
        {
          SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, false, "Jackpot not awarded - OK");

          return false;
        }

        SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, false, "JACKPOT AWARDED !!!!! - OK");

        SqlTrx.Save("JACKPOT");

        try
        {
          //
          // Get Jackpot instances from C2_JACKPOT_INSTANCES
          // 
          dt_instances = new DataTable("C2_JACKPOT_INSTANCES");
          dt_instances = WC2_BusinessLogic.GetTable("C2_JACKPOT_INSTANCES", SqlTrx, "C2JI_INDEX");
          if (dt_instances == null)
          {
            SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, false, "Jackpot Instances is null !!! - UNEXPECTED");

            return false;
          }

          //
          //  ***   JACKPOT AWARD !!!!!!!!!!!
          //
          //  --> idx_jackpot
          //

          //
          // Lock table C2_JACKPOT_COUNTERS Updating C2JC_INDEX in C2_JACKPOT_COUNTERS table.
          //
          if (!WC2_BusinessLogic.LockTable("C2_JACKPOT_COUNTERS", "C2JC_INDEX", SqlTrx))
          {
            SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, false, "Lock C2_JACKPOT_COUNTERS failed !!! - UNEXPECTED");

            return false;
          }

          //
          // Get Jackpot coounters from C2_JACKPOT_COUNTERS
          // 
          dt_counters = new DataTable("C2_JACKPOT_COUNTERS");
          dt_counters = WC2_BusinessLogic.GetTable("C2_JACKPOT_COUNTERS", SqlTrx, "C2JC_INDEX");
          if (dt_counters == null)
          {
            SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, false, "Jackpot Counters is null !!! - UNEXPECTED");

            return false;
          }

          //
          // Assign jackpot amount
          //    - JackpotIndex
          //    - JackpotName
          //    - JackpotAmount = C2JC_ACCUMULATED
          //
          JackpotIndex = (Int32)dt_counters.Rows[idx_jackpot]["C2JC_INDEX"];
          JackpotName = (String)dt_instances.Rows[idx_jackpot]["C2JI_NAME"];
          JackpotAmount = (Decimal)dt_counters.Rows[idx_jackpot]["C2JC_ACCUMULATED"];

          JackpotAmount = Math.Truncate(JackpotAmount);

          //
          // INSERT IN JACKPOT_HISTORY
          //    - JackpotId = C2JH_JACKPOT_ID
          //

          sql_str = "INSERT INTO C2_JACKPOT_HISTORY " +
                      " ( C2JH_INDEX           " +
                      " , C2JH_NAME            " +
                      " , C2JH_AMOUNT          " +
                      " , C2JH_PLAY_ID         " +
                      " , C2JH_TERMINAL_ID     " +
                      " , C2JH_TERMINAL_NAME   " +
                      " , C2JH_GAME_ID         " +
                      " , C2JH_GAME_NAME )     " +
                  " VALUES (@p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8) " +
                     " SET @p9 = SCOPE_IDENTITY()";

          sql_command = new SqlCommand(sql_str);
          sql_command.Connection = SqlTrx.Connection;
          sql_command.Transaction = SqlTrx;

          sql_command.Parameters.Add("@p1", SqlDbType.Int, 4, "C2JH_INDEX").Value = (Int32)dt_counters.Rows[idx_jackpot]["C2JC_INDEX"];
          sql_command.Parameters.Add("@p2", SqlDbType.NVarChar, 20, "C2JH_NAME").Value = JackpotName;
          sql_command.Parameters.Add("@p3", SqlDbType.Money, 8, "C2JH_AMOUNT").Value = JackpotAmount;
          // ACC 27-MAY-2010 Trx MsgPlay revised and improved.
          sql_command.Parameters.Add("@p4", SqlDbType.BigInt, 8, "C2JH_PLAY_ID").Value = DBNull.Value;
          sql_command.Parameters.Add("@p5", SqlDbType.Int, 4, "C2JH_TERMINAL_ID").Value = TerminalId;
          sql_command.Parameters.Add("@p6", SqlDbType.VarChar, 50, "C2JH_TERMINAL_NAME").Value = WC2_BusinessLogic.DB_GetTerminalName(TerminalId, SqlTrx);
          sql_command.Parameters.Add("@p7", SqlDbType.Int, 4, "DAP_GAME_ID").Value = WC2_BusinessLogic.DB_GetGameId(GameName, SqlTrx);
          if (GameName.Length > 50)
          {
            sql_command.Parameters.Add("@p8", SqlDbType.VarChar, 50, "DAP_GAME_NAME").Value = GameName.Substring(0, 50);
          }
          else
          {
            sql_command.Parameters.Add("@p8", SqlDbType.VarChar, 50, "DAP_GAME_NAME").Value = GameName;
          }


          p = sql_command.Parameters.Add("@p9", SqlDbType.BigInt, 8, "C2JH_JACKPOT_ID");
          p.Direction = ParameterDirection.Output;

          num_rows_inserted = sql_command.ExecuteNonQuery();
          if (num_rows_inserted != 1)
          {
            Log.Warning("Exception throw: DrawAudit Not inserted.");
            SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, false, "DrawAudit Not inserted !!! - UNEXPECTED");

            return false;
          }

          JackpotId = (Int64)p.Value;

          // 
          // Update table C2_JACKPOT_COUNTERS
          //
          //     C2JC_ACCUMULATED = C2JC_ACCUMULATED - JackpotAmount.
          //     if (C2JC_ACCUMULATED < C2JC_MINIMUM)
          //        --> C2JC_TO_COMPENSATE = C2JI_MINIMUM - C2JC_ACCUMULATED.
          //        --> C2JC_ACCUMULATED   = C2JI_MINIMUM.
          //


          new_accumulated = (((Decimal)dt_counters.Rows[idx_jackpot]["C2JC_ACCUMULATED"]) - JackpotAmount) + ((Decimal)dt_instances.Rows[idx_jackpot]["C2JI_MINIMUM"]);
          add_to_compensate = ((Decimal)dt_instances.Rows[idx_jackpot]["C2JI_MINIMUM"]);

          sql_str = "UPDATE C2_JACKPOT_COUNTERS " +
                      " SET C2JC_ACCUMULATED = @p1 " +
                         ", C2JC_TO_COMPENSATE = C2JC_TO_COMPENSATE + @p2 " +
                     "WHERE C2JC_INDEX = @p3 ";

          sql_command = new SqlCommand(sql_str);
          sql_command.Connection = SqlTrx.Connection;
          sql_command.Transaction = SqlTrx;

          sql_command.Parameters.Add("@p1", SqlDbType.Decimal, 12, "C2JC_ACCUMULATED").Value = (Decimal)new_accumulated;
          sql_command.Parameters.Add("@p2", SqlDbType.Decimal, 12, "C2JC_TO_COMPENSATE").Value = (Decimal)add_to_compensate;
          sql_command.Parameters.Add("@p3", SqlDbType.Int, 4, "C2JC_INDEX").Value = (Int32)dt_counters.Rows[idx_jackpot]["C2JC_INDEX"];

          num_rows_updated = sql_command.ExecuteNonQuery();

          if (num_rows_updated != 1)
          {
            Log.Warning("JackpotCheck: Error updating jackpot counters...");
            SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, false, "Error updating jackpot counters ! - UNEXPECTED");

            return false;
          }

          SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, false, "JACKPOT AWARDED successfully completed. Return TRUE - OK");

          awarded = true;
          m_fake_hits[idx_jackpot] = 0;
          m_is_time_to_check[idx_jackpot] = false;
          m_timestamp_instance[idx_jackpot] = 0;

          return true;
        }
        catch (Exception ex)
        {
          Log.Exception(ex);

          return false;
        }
        finally
        {
          if (!awarded)
          {
            SqlTrx.Rollback("JACKPOT");
          }
        }
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);

        return false;
      }
      finally
      {
        SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, false, "");
        if (jackpot_award)
        {
          SpecialJackpotLogger.LoggerFlush(false, _special_logger_msg, true);
        }
        else
        {
          SpecialJackpotLogger.LoggerFlush(false, _special_logger_msg);
        }
      }

    } // CheckJackpot

    /// <summary>
    /// Update play id
    /// </summary>
    /// <param name="JackpotId"></param>
    /// <param name="PlayId"></param>
    /// <param name="SqlTrx"></param>
    public static void UpdateJackpotHistory(Int64 JackpotId,
                                            Int64 PlayId,
                                            SqlTransaction SqlTrx)
    {
      SqlCommand sql_command;
      String sql_str;
      Int32 num_rows_updated;

      try
      {
        sql_str = "UPDATE C2_JACKPOT_HISTORY " +
                    " SET C2JH_PLAY_ID = @p1 " +
                   "WHERE C2JH_JACKPOT_ID = @p2 ";

        sql_command = new SqlCommand(sql_str);
        sql_command.Connection = SqlTrx.Connection;
        sql_command.Transaction = SqlTrx;

        sql_command.Parameters.Add("@p1", SqlDbType.BigInt, 8, "C2JH_PLAY_ID").Value = PlayId;
        sql_command.Parameters.Add("@p2", SqlDbType.BigInt, 8, "C2JH_JACKPOT_ID").Value = JackpotId;

        num_rows_updated = sql_command.ExecuteNonQuery();

        if (num_rows_updated != 1)
        {
          Log.Warning("JackpotCheck: Error updating jackpot play id ...");
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

    } // UpdateJackpotHistory


    /// <summary>
    /// Jackpot Manager Thread.
    /// </summary>
    static private void ReadJackpotParameters()
    {
      SqlConnection _sql_conn;
      SqlTransaction _sql_trx;
      SqlCommand _sql_command;
      String sql_str;
      DataTable dt_parameters;
      DataTable dt_instances;
      DataTable dt_counters;
      Int64 _old_timestamp;
      Boolean[] is_time;
      Int32 idx_jackpot;
      Int32 bit_days;
      TimeSpan current_time;
      Object _obj;
      String _special_logger_msg;
      Boolean _enabled_special_jackpot;

      // Initialize variables
      dt_parameters = null;
      dt_instances = null;
      dt_counters = null;

      _special_logger_msg = "\r\n\r\n";
      _enabled_special_jackpot = false;

      _sql_conn = null;
      _sql_trx = null;
      is_time = new Boolean[3];
      is_time[0] = false;
      is_time[1] = false;
      is_time[2] = false;
      
      try
      {
        SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, true, "Init ReadJackpotParameters ... - OK");

        _sql_conn = WGDB.Connection();
        if ( _sql_conn == null )
        {
          SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, true, "Connection is null !!! - UNEXPECTED");

          return;
        }
        _sql_trx = _sql_conn.BeginTransaction();
        if ( _sql_trx == null )
        {
          SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, true, "Transaction is null !!! - UNEXPECTED");

          return;
        }
        //
        // Get Jackpot instances from C2_JACKPOT_PARAMETERS
        // 
        dt_parameters = new DataTable("C2_JACKPOT_PARAMETERS");
        dt_parameters = WC2_BusinessLogic.GetTable("C2_JACKPOT_PARAMETERS", _sql_trx);
        if (dt_parameters == null)
        {
          SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, true, "Jackpot Parameters is null !!! - UNEXPECTED");

          return;
        }
        // Check number of rows
        if (dt_parameters.Rows.Count != 1)
        {
          Log.Warning("Jackpot parameters missing.");

          return;
        }

        // Check that jackpot system is enabled
        if ((Boolean)dt_parameters.Rows[0]["C2JP_ENABLED"] != true)
        {
          SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, true, "Jackpot not Enabled !!! - UNEXPECTED");

          return;
        }

        // Check awarding day
        bit_days = 1;
        bit_days <<= (Int32)DateTime.Now.DayOfWeek;
        if ((((Int32)dt_parameters.Rows[0]["C2JP_AWARDING_DAYS"]) & bit_days) == 0)
        {
          SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, true, "AWARDING_DAYS Failed !!! - UNEXPECTED");

          return;
        }        
        //Get total contribution for bet e.g. 1%
        m_jackpot_contribution = ((Decimal)dt_parameters.Rows[0]["C2JP_CONTRIBUTION_PCT"]);
        if ((m_jackpot_contribution > 100) || (m_jackpot_contribution < 0))
        {
          SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, true, "CONTRIBUTION_PCT Failed !!! - UNEXPECTED");

          return;
        }

        //
        // Get Jackpot instances from C2_JACKPOT_INSTANCES
        // 
        dt_instances = new DataTable("C2_JACKPOT_INSTANCES");
        dt_instances = WC2_BusinessLogic.GetTable("C2_JACKPOT_INSTANCES", _sql_trx, "C2JI_INDEX");
        if (dt_instances == null)
        {
          SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, true, "Jackpot Instances is null !!! - UNEXPECTED");

          return;
        }

        //
        // Initialize arrays (first time or jackpot has been added)
        //
        if (m_timestamp_instance.Length != dt_instances.Rows.Count)
        {
          Log.Warning("Wrong number of Jackpot Instances (expected=3)");

          return;
        }

        //
        // Get Jackpot coounters from C2_JACKPOT_COUNTERS
        // 
        dt_counters = WC2_BusinessLogic.GetTable("C2_JACKPOT_COUNTERS", _sql_trx, "C2JC_INDEX");
        if (dt_counters == null)
        {
          SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, true, "Jackpot Counters is null !!! - UNEXPECTED");

          return;
        }

        SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, true, "Checking ... - OK");

        for (idx_jackpot = 0; idx_jackpot < dt_instances.Rows.Count; idx_jackpot++)
        {
          sql_str = " SELECT CAST(C2JI_TIMESTAMP AS BIGINT) ";
          sql_str += "  FROM C2_JACKPOT_INSTANCES ";
          sql_str += " WHERE C2JI_INDEX = @pC2JI";

          _sql_command = new SqlCommand(sql_str);
          _sql_command.Connection = _sql_trx.Connection;
          _sql_command.Transaction = _sql_trx;

          _sql_command.Parameters.Add("@pC2JI", SqlDbType.Int).Value = (Int32)dt_instances.Rows[idx_jackpot]["C2JI_INDEX"];

          _obj = _sql_command.ExecuteScalar();
          if (_obj == null)
          {
            SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, true, "C2JI_TIMESTAMP is null !!! - UNEXPECTED");

            return;
          }

          // Special Jackpot Logger 
          if ((Decimal)dt_counters.Rows[idx_jackpot]["C2JC_ACCUMULATED"] > m_jackpot_maximum[idx_jackpot])
          {
            _enabled_special_jackpot = true;
          }

          _old_timestamp = m_timestamp_instance[idx_jackpot];
          m_timestamp_instance[idx_jackpot] = (Int64)_obj;

          if (m_timestamp_instance[idx_jackpot] != _old_timestamp)
          {
            m_jackpot_minimum_bet[idx_jackpot] = ((Decimal)dt_instances.Rows[idx_jackpot]["C2JI_MINIMUM_BET"]);
            m_jackpot_minimum[idx_jackpot] = ((Decimal)dt_instances.Rows[idx_jackpot]["C2JI_MINIMUM"]);
            m_jackpot_maximum[idx_jackpot] = ((Decimal)dt_instances.Rows[idx_jackpot]["C2JI_MAXIMUM"]);
            m_jackpot_average[idx_jackpot] = ((Decimal)dt_instances.Rows[idx_jackpot]["C2JI_AVERAGE"]);
            m_jackpot_contribution_pct[idx_jackpot] = ((Decimal)dt_instances.Rows[idx_jackpot]["C2JI_CONTRIBUTION_PCT"]);

            Decimal acum;
            Decimal min;

            min = Math.Min(m_jackpot_minimum[idx_jackpot], m_jackpot_maximum[idx_jackpot]);

            acum = (Decimal)dt_counters.Rows[idx_jackpot]["C2JC_ACCUMULATED"];

            if (acum < min)
            {
              acum = min - acum;

              sql_str = "UPDATE   C2_JACKPOT_COUNTERS " +
                         "  SET   C2JC_TO_COMPENSATE = C2JC_TO_COMPENSATE + @p1 " +
                         "      , C2JC_ACCUMULATED   = C2JC_ACCUMULATED   + @p1 " +
                         "WHERE   C2JC_INDEX         = @p2 ";

              _sql_command = new SqlCommand(sql_str);
              _sql_command.Connection = _sql_trx.Connection;
              _sql_command.Transaction = _sql_trx;

              _sql_command.Parameters.Add("@p1", SqlDbType.Decimal, 12, "Qtty").Value = acum;
              _sql_command.Parameters.Add("@p2", SqlDbType.Int, 4, "C2JC_INDEX").Value = (Int32)dt_counters.Rows[idx_jackpot]["C2JC_INDEX"];

              _sql_command.ExecuteNonQuery();
            }
          }

          // Check awarding hours
          current_time = DateTime.Now.Subtract(DateTime.Now.Date);
          if (((Int32)dt_parameters.Rows[0]["C2JP_AWARDING_START"]) > current_time.TotalSeconds ||
              ((Int32)dt_parameters.Rows[0]["C2JP_AWARDING_END"]) < current_time.TotalSeconds)
          {
            SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, true, "AWARDING_START/END Failed !!! - UNEXPECTED");

            is_time[idx_jackpot] = false;
          }
          else
          {
            is_time[idx_jackpot] = true;
          }

          ComputeFakeHits(idx_jackpot);
        }

        _sql_trx.Commit();
      }
      catch (Exception _ex)
      {
        Log.Exception(_ex);
      }
      finally 
      {
        m_is_time_to_check[0] = is_time[0];
        m_is_time_to_check[1] = is_time[1];
        m_is_time_to_check[2] = is_time[2];

        SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, true, "");

        sql_str = (is_time[0] ? "TRUE" : "FALSE");
        SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, true, "Jackpot(0) is time: " + sql_str + " - OK");
        sql_str = (is_time[1] ? "TRUE" : "FALSE");
        SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, true, "Jackpot(1) is time: " + sql_str + " - OK");
        sql_str = (is_time[2] ? "TRUE" : "FALSE");
        SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, true, "Jackpot(2) is time: " + sql_str + " - OK");

        SpecialJackpotLogger.LoggerInsert(ref _special_logger_msg, true, "");

        SpecialJackpotLogger.LoggerFlush(true, _special_logger_msg);

        SpecialJackpotLogger.SetLoggerEnabled(_enabled_special_jackpot);

        if (_sql_trx != null)
        {
          if (_sql_trx.Connection != null)
          {
            _sql_trx.Rollback();
          }
        }

        if (_sql_conn != null)
        {
          _sql_conn.Close();
          _sql_conn = null;
        }
      }
    }

    //------------------------------------------------------------------------------
    // PURPOSE : Get general Jackpot Info.
    //
    //  PARAMS :
    //      - INPUT :
    //          - TerminalId
    //          - Denomination
    //          - TotalBetCredits
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - ArrayList of Jackpots
    //
    public static ArrayList WC2_GetJackpotInfo(Int32 TerminalId,
                                               int Denomination,
                                               Int64 TotalBetCredits)
    {
      WC2_Jackpot _jackpot;
      ArrayList _array_list;
      Double _total_bet_amount;
      int _idx_jackpot;

      _array_list = new ArrayList();
      _total_bet_amount = Denomination * 0.01 * TotalBetCredits;

      m_jackpot_info_lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
      try
      {
        for (_idx_jackpot = 0; _idx_jackpot < m_dt_jackpot_info.Rows.Count; _idx_jackpot++)
        {
          // ACC & AJQ 25-SEP-2008
          // Always send all jackpots.
          //if ( total_bet_amount >= ((Double)((Decimal)dt_jackpots.Rows[idx_jackpot]["C2JI_MINIMUM_BET"])))
          //{
          _jackpot = new WC2_Jackpot();
          _jackpot.Index = (int)m_dt_jackpot_info.Rows[_idx_jackpot]["C2JI_INDEX"];
          _jackpot.Name = (String)m_dt_jackpot_info.Rows[_idx_jackpot]["C2JI_NAME"];
          _jackpot.Amount = (Decimal)m_dt_jackpot_info.Rows[_idx_jackpot]["C2JC_ACCUMULATED"];
          _jackpot.MinimumBetAmount = (Decimal)m_dt_jackpot_info.Rows[_idx_jackpot]["C2JI_MINIMUM_BET"];
          _array_list.Add(_jackpot);
          //}
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error Getting data from JackpotInfo Datatable.");
      }
      finally
      {
        m_jackpot_info_lock.ReleaseReaderLock();
      }

      return _array_list;
    } // WC2_GetJackpotInfo

    //------------------------------------------------------------------------------
    // PURPOSE : Get Jackpot History.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - ArrayList of Jackpot History
    //
    public static ArrayList WC2_GetJackpotHistory()
    {
      ArrayList _array_list;
      WC2_Jackpot_History _jackpot_history;
      int _idx_jackpot;

      _array_list = new ArrayList();

      m_jackpot_history_lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
      try
      {
        for (_idx_jackpot = 0; _idx_jackpot < m_dt_jackpot_history.Rows.Count; _idx_jackpot++)
        {
          _jackpot_history = new WC2_Jackpot_History();
          _jackpot_history.Index = (int)m_dt_jackpot_history.Rows[_idx_jackpot]["C2JH_INDEX"];
          _jackpot_history.Name = (String)m_dt_jackpot_history.Rows[_idx_jackpot]["C2JH_NAME"];
          _jackpot_history.Amount = (Decimal)m_dt_jackpot_history.Rows[_idx_jackpot]["C2JH_AMOUNT"];
          _jackpot_history.Date = (DateTime)m_dt_jackpot_history.Rows[_idx_jackpot]["C2JH_AWARDED"];

          _array_list.Add(_jackpot_history);
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error Getting data from JackpotHistory Datatable.");
      }
      finally
      {
        m_jackpot_history_lock.ReleaseReaderLock();
      }

      return _array_list;
    } // WC2_GetJackpotHistory

    //------------------------------------------------------------------------------
    // PURPOSE : Get Jackpot Messages.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //      - ArrayList of Jackpot Messages 

    public static ArrayList WC2_GetJackpotMessages()
    {
      ArrayList _array_list;
      String _message;

      _array_list = new ArrayList();

      m_jackpot_messages_lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
      try
      {
        if (m_dt_jackpot_messages.Rows.Count > 0)
        {
          if (!m_dt_jackpot_messages.Rows[0].IsNull("C2JP_PROMO_MESSAGE1"))
          {
            if ((String)m_dt_jackpot_messages.Rows[0]["C2JP_PROMO_MESSAGE1"] != "")
            {
              _message = (String)m_dt_jackpot_messages.Rows[0]["C2JP_PROMO_MESSAGE1"];
              _array_list.Add(_message);
            }
          }

          if (!m_dt_jackpot_messages.Rows[0].IsNull("C2JP_PROMO_MESSAGE2"))
          {
            if ((String)m_dt_jackpot_messages.Rows[0]["C2JP_PROMO_MESSAGE2"] != "")
            {
              _message = (String)m_dt_jackpot_messages.Rows[0]["C2JP_PROMO_MESSAGE2"];
              _array_list.Add(_message);
            }
          }

          if (!m_dt_jackpot_messages.Rows[0].IsNull("C2JP_PROMO_MESSAGE3"))
          {
            if ((String)m_dt_jackpot_messages.Rows[0]["C2JP_PROMO_MESSAGE3"] != "")
            {
              _message = (String)m_dt_jackpot_messages.Rows[0]["C2JP_PROMO_MESSAGE3"];
              _array_list.Add(_message);
            }
          }
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error Getting data from JackpotMessages Datatable.");
      }
      finally
      {
        m_jackpot_messages_lock.ReleaseReaderLock();
      }

      return _array_list;
    } // WC2_GetJackpotMessages

    //------------------------------------------------------------------------------
    // PURPOSE : Get Jackpot Parameters.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //          - Mode
    //          - BlockInterval
    //          - AnimationInterval
    //          - RecentHistoryInterval
    //          - MinBlockAmount
    //
    // RETURNS :
    //
    public static void WC2_GetJackpotParameters(out String Mode, out int BlockInterval, out int AnimationInterval, out int RecentHistoryInterval, out Decimal MinBlockAmount)
    {

      Mode = "BLOCK_NONE";
      BlockInterval = 10;
      AnimationInterval = 15;
      MinBlockAmount = 0;
      RecentHistoryInterval = 60;

      m_jackpot_params_lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
      try
      {
        if (m_dt_jackpot_params.Rows.Count > 0)
        {
          switch ((int)m_dt_jackpot_params.Rows[0]["C2JP_BLOCK_MODE"])
          {
            case 0:
              Mode = "BLOCK_NONE";
            break;
            case 1:
              Mode = "BLOCK_UNLOCK_BY_TIME";
            break;
            case 2:
              Mode = "BLOCK_UNLOCK_BY_KEY";
            break;
          }
          BlockInterval = (int)m_dt_jackpot_params.Rows[0]["C2JP_BLOCK_INTERVAL"];
          AnimationInterval = (int)m_dt_jackpot_params.Rows[0]["C2JP_ANIMATION_INTERVAL"];
          RecentHistoryInterval = (int)m_dt_jackpot_params.Rows[0]["C2JP_RECENT_INTERVAL"];
          MinBlockAmount = (Decimal)m_dt_jackpot_params.Rows[0]["C2JP_BLOCK_MIN_AMOUNT"];
        }
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception throw: Error Getting data from JackpotParameters Datatable.");
      }
      finally
      {
        m_jackpot_params_lock.ReleaseReaderLock();
      }

    } // WC2_GetJackpotParameters

    //------------------------------------------------------------------------------
    // PURPOSE : Get all data from database for Jackpot Info transaction.
    //
    //  PARAMS :
    //      - INPUT :
    //
    //      - OUTPUT :
    //
    // RETURNS :
    //
    static private void GetDataTrxJackpotInfoFromDatabase()
    {
      SqlConnection _sql_conn;
      SqlCommand _sql_command;
      String _sql_str;
      SqlDataAdapter _da;
      DataTable _dt;

      _sql_conn = null;

      try
      {
        _sql_conn = WGDB.Connection();

        //
        // Jackpot Info
        //
        _dt = null;
        _da = null;
        _sql_command = null;
        try
        {
          _dt = new DataTable("JACKPOT_INFO");
          _da = new SqlDataAdapter();
          _sql_command = new SqlCommand();
          _sql_command.Connection = _sql_conn;
          _da.SelectCommand = _sql_command;

          _sql_str = "SELECT * " +
                      "FROM C2_JACKPOT_INSTANCES, C2_JACKPOT_COUNTERS " +
                      "WHERE C2JI_INDEX = C2JC_INDEX " +
                  "ORDER BY C2JI_INDEX ";

          _sql_command.CommandText = _sql_str;

          // Retrieve data
          _da.Fill(_dt);
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
          Log.Warning("Exception throw: Error Get jackpot info.");
        }

        m_jackpot_info_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
        try
        {
          if (_dt != null)
          {
            m_dt_jackpot_info = _dt;
          }
        }
        finally
        {
          m_jackpot_info_lock.ReleaseWriterLock();
        }

        //
        // Jackpot History
        //
        _dt = null;
        _da = null;
        _sql_command = null;
        try
        {
          _dt = new DataTable("JACKPOT_HISTORY");
          _da = new SqlDataAdapter();
          _sql_command = new SqlCommand();
          _sql_command.Connection = _sql_conn;
          _da.SelectCommand = _sql_command;

          _sql_str =  " SELECT DISTINCT C2JH_INDEX, C2JH_NAME, C2JH_AMOUNT, C2JH_AWARDED FROM (" +
                      " SELECT * FROM (SELECT TOP 3 * FROM C2_JACKPOT_HISTORY WHERE C2JH_STATUS = 0 ORDER BY 1 DESC) J0" +
                      " UNION" +
                      " SELECT * FROM (SELECT TOP 1 * FROM C2_JACKPOT_HISTORY WHERE C2JH_STATUS = 0 AND C2JH_INDEX = 1 ORDER BY 1 DESC) J1" +
                      " UNION" +
                      " SELECT * FROM (SELECT TOP 1 * FROM C2_JACKPOT_HISTORY WHERE C2JH_STATUS = 0 AND C2JH_INDEX = 2 ORDER BY 1 DESC) J2" +
                      " UNION" +
                      " SELECT * FROM (SELECT TOP 1 * FROM C2_JACKPOT_HISTORY WHERE C2JH_STATUS = 0 AND C2JH_INDEX = 3 ORDER BY 1 DESC) J3" +
                      " ) JX" +
                      " ORDER BY 4 DESC";

          _sql_command.CommandText = _sql_str;

          // Retrieve data
          _da.Fill(_dt);
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
          Log.Warning("Exception throw: Error Get jackpot history.");
        }

        m_jackpot_history_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
        try
        {
          if (_dt != null)
          {
            m_dt_jackpot_history = _dt;
          }
        }
        finally
        {
          m_jackpot_history_lock.ReleaseWriterLock();
        }

        //
        // Jackpot Messages
        //
        _dt = null;
        _da = null;
        _sql_command = null;
        try
        {
          _dt = new DataTable("JACKPOT_MESSAGES");
          _da = new SqlDataAdapter();
          _sql_command = new SqlCommand();
          _sql_command.Connection = _sql_conn;
          _da.SelectCommand = _sql_command;

          _sql_str = " SELECT  C2JP_PROMO_MESSAGE1 " +
                     "       , C2JP_PROMO_MESSAGE2 " +
                     "       , C2JP_PROMO_MESSAGE3 " +
                     " FROM C2_JACKPOT_PARAMETERS";

          _sql_command.CommandText = _sql_str;

          // Retrieve data
          _da.Fill(_dt);
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
          Log.Warning("Exception throw: Error Get jackpot messages.");
        }

        m_jackpot_messages_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
        try
        {
          if (_dt != null)
          {
            m_dt_jackpot_messages = _dt;
          }
        }
        finally
        {
          m_jackpot_messages_lock.ReleaseWriterLock();
        }

        //
        // Jackpot Parameters
        //
        _dt = null;
        _da = null;
        _sql_command = null;
        try
        {
          _dt = new DataTable("JACKPOT_PARAMS");
          _da = new SqlDataAdapter();
          _sql_command = new SqlCommand();
          _sql_command.Connection = _sql_conn;
          _da.SelectCommand = _sql_command;

          _sql_str = " SELECT C2JP_BLOCK_MODE, C2JP_BLOCK_INTERVAL, C2JP_ANIMATION_INTERVAL, C2JP_RECENT_INTERVAL, C2JP_BLOCK_MIN_AMOUNT " +
                     " FROM C2_JACKPOT_PARAMETERS";

          _sql_command.CommandText = _sql_str;

          // Retrieve data
          _da.Fill(_dt);
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
          Log.Warning("Exception throw: Error Get jackpot parameters.");
        }

        m_jackpot_params_lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
        try
        {
          if (_dt != null)
          {
            m_dt_jackpot_params = _dt;
          }
        }
        finally
        {
          m_jackpot_params_lock.ReleaseWriterLock();
        }

      }
      catch (Exception ex)
      {
        Log.Exception(ex);
        Log.Warning("Exception: GetDataTrxJackpotInfo.");
      }
      finally
      {
        // Close connection
        if (_sql_conn != null)
        {
          try
          {
            _sql_conn.Close();
            _sql_conn = null;
          }
          catch
          {
            ;
          }
        }
      }
    } // GetDataTrxJackpotInfo

    /// <summary>
    /// Jackpot Manager Thread.
    /// </summary>
    static private void JackpotManagerThread()
    {
      int _last_read_parameters;
      int _wait_hint;

      // Initialize variables
      _last_read_parameters = 0;
      _wait_hint = 0;
        
      while (true)
      {
        try 
        {
          System.Threading.Thread.Sleep(_wait_hint);
          _wait_hint = 2000;

          // Add accumulated amount for jackpot contribution into DB.
          JackpotContributionToDatabase();
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        try
        {
          // Read Data for Jackpot Info transaction
          GetDataTrxJackpotInfoFromDatabase();
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

        try 
        {
          if (Misc.GetElapsedTicks(_last_read_parameters, Environment.TickCount) >= 10000)
          {
            if (_last_read_parameters != 0)
            {
              SpecialJackpotLogger.LoggerStatusInfo();
            }

            // Read Jackpot Parameters
            ReadJackpotParameters();
            _last_read_parameters = Environment.TickCount;
          }
        }
        catch (Exception _ex)
        {
          Log.Exception(_ex);
        }

      } // while (true)
    } // JackpotManagerThread

  } // class WC2_JackpotManager

} // namespace WSI.WC2
