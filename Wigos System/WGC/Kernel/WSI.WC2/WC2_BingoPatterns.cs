using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;
using System.Xml;
using System.Data.SqlClient;
using System.Data;
using WSI.Common;

namespace WSI.WC2
{
  class WC2_BingoPatterns
  {
    //Struct to be 
    struct record {
      public Int64 min;
      public Int64 max;
      public Int64 positions;
    }
    private record[] pattern_list;

    /// <summary>
    /// Reverses a string
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public string Reverse(string Str, int Columns, int Rows)
    {
      char[] temp_positions_reversed;
      String str_reversed;
      int idx_positions;
      int idx_rows;
      int idx_columns;

      //first this transforms the files to columns
      temp_positions_reversed = new char[Str.Length];
      str_reversed = "";
      idx_positions = 0;
      for (idx_rows = 0; idx_rows < Rows; idx_rows++)
      {
        for (idx_columns = 0; idx_columns < Columns*Rows; idx_columns += Columns)
        {
          temp_positions_reversed[idx_positions] = Str[idx_rows+idx_columns];
          idx_positions++;
        }
      }

      //now, reverse the string
      for (idx_positions = 0; idx_positions < temp_positions_reversed.Length; idx_positions++)
      {
        str_reversed = str_reversed.Insert(idx_positions, temp_positions_reversed[(temp_positions_reversed.Length - 1) - idx_positions].ToString());
      }
      return str_reversed;
    }

    /// <summary>
    /// Checks the database integrity
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <param name="NumPatt"></param>
    /// <returns></returns>
    public Boolean CheckDBPatterns(SqlTransaction SqlTrx, int NumPatt)
    {
      String sql_str;
      SqlCommand sql_command;
      int patterns_stored = 0;

      sql_str = " SELECT COUNT(*) FROM C2_WINNING_PATTERNS ";

      sql_command = new SqlCommand(sql_str);
      sql_command.Connection = SqlTrx.Connection;
      sql_command.Transaction = SqlTrx;

      try
      {
        patterns_stored = ((int)sql_command.ExecuteScalar());
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      if (patterns_stored != NumPatt)
      {
        return false;
      }
      else
      {
        return true;
      }
    }
    /// <summary>
    /// This creates the pattern list.
    /// </summary>
    public void GeneratePatterns()
    {
      String temp_positions;

      pattern_list = new record[WC2_Patterns.NUM_PATTERNS];


      //Min defines the minimum prize for this pattern
      pattern_list[0].min = 0;
      //Max defines the maximum prize for this pattern
      pattern_list[0].max = 3;
      //The positions defines the blanks and the marks of a pattern
      temp_positions = "";
      temp_positions += "01100";
      temp_positions += "10000";
      temp_positions += "00000";
      temp_positions += "00000";
      temp_positions += "00000";
      //Due to the left zeroes are not significant we must reverse the binary string 
      //to ensure that the last string number is the first of the pattern.
      //moreover the columns are the files and the files are the columns, so the first file (01100) represents the first column
      pattern_list[0].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);

      pattern_list[1].min = 0;
      pattern_list[1].max = 3;
      temp_positions = "";
      temp_positions += "00000";
      temp_positions += "10000";
      temp_positions += "00000";
      temp_positions += "01010";
      temp_positions += "00000";
      pattern_list[1].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[2].min = 0;
      pattern_list[2].max = 3;
      temp_positions = "";
      temp_positions += "00000";
      temp_positions += "10100";
      temp_positions += "10000";
      temp_positions += "00000";
      temp_positions += "00000";
      pattern_list[2].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[3].min = 3;
      pattern_list[3].max = 6;
      temp_positions = "";
      temp_positions += "10000";
      temp_positions += "01000";
      temp_positions += "00000";
      temp_positions += "00010";
      temp_positions += "00001";
      pattern_list[3].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[4].min = 3;
      pattern_list[4].max = 6;
      temp_positions = "";
      temp_positions += "00000";
      temp_positions += "10001";
      temp_positions += "00000";
      temp_positions += "10001";
      temp_positions += "00000";
      pattern_list[4].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[5].min = 3;
      pattern_list[5].max = 6;
      temp_positions = "";
      temp_positions += "01010";
      temp_positions += "00000";
      temp_positions += "00000";
      temp_positions += "00000";
      temp_positions += "01010";
      pattern_list[5].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[6].min = 6;
      pattern_list[6].max = 16;
      temp_positions = "";
      temp_positions += "00000";
      temp_positions += "01000";
      temp_positions += "01100";
      temp_positions += "01000";
      temp_positions += "00000";
      pattern_list[6].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[7].min = 6;
      pattern_list[7].max = 16;
      temp_positions = "";
      temp_positions += "00110";
      temp_positions += "00001";
      temp_positions += "00001";
      temp_positions += "00000";
      temp_positions += "00000";
      pattern_list[7].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[8].min = 6;
      pattern_list[8].max = 16;
      temp_positions = "";
      temp_positions += "00000";
      temp_positions += "00000";
      temp_positions += "00001";
      temp_positions += "00001";
      temp_positions += "00110";
      pattern_list[8].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[9].min = 16;
      pattern_list[9].max = 31;
      temp_positions = "";
      temp_positions += "00000";
      temp_positions += "00000";
      temp_positions += "00000";
      temp_positions += "01110";
      temp_positions += "01010";
      pattern_list[9].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[10].min = 16;
      pattern_list[10].max = 31;
      temp_positions = "";
      temp_positions += "01010";
      temp_positions += "01110";
      temp_positions += "00000";
      temp_positions += "00000";
      temp_positions += "00000";
      pattern_list[10].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[11].min = 16;
      pattern_list[11].max = 31;
      temp_positions = "";
      temp_positions += "00110";
      temp_positions += "00100";
      temp_positions += "01100";
      temp_positions += "00000";
      temp_positions += "00000";
      pattern_list[11].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[12].min = 31;
      pattern_list[12].max = 46;
      temp_positions = "";
      temp_positions += "00001";
      temp_positions += "00010";
      temp_positions += "00100";
      temp_positions += "01000";
      temp_positions += "10000";
      pattern_list[12].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[13].min = 31;
      pattern_list[13].max = 46;
      temp_positions = "";
      temp_positions += "10000";
      temp_positions += "01000";
      temp_positions += "00100";
      temp_positions += "00010";
      temp_positions += "00001";
      pattern_list[13].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[14].min = 31;
      pattern_list[14].max = 46;
      temp_positions = "";
      temp_positions += "00100";
      temp_positions += "00100";
      temp_positions += "00100";
      temp_positions += "00100";
      temp_positions += "00100";
      pattern_list[14].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[15].min = 46;
      pattern_list[15].max = 81;
      temp_positions = "";
      temp_positions += "11100";
      temp_positions += "11100";
      temp_positions += "00000";
      temp_positions += "00000";
      temp_positions += "00000";
      pattern_list[15].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[16].min = 46;
      pattern_list[16].max = 81;
      temp_positions = "";
      temp_positions += "00000";
      temp_positions += "00000";
      temp_positions += "00000";
      temp_positions += "00111";
      temp_positions += "00111";
      pattern_list[16].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[17].min = 46;
      pattern_list[17].max = 81;
      temp_positions = "";
      temp_positions += "00000";
      temp_positions += "11100";
      temp_positions += "11100";
      temp_positions += "00000";
      temp_positions += "00000";
      pattern_list[17].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[18].min = 81;
      pattern_list[18].max = 101;
      temp_positions = "";
      temp_positions += "10101";
      temp_positions += "01010";
      temp_positions += "00100";
      temp_positions += "00000";
      temp_positions += "00000";
      pattern_list[18].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[19].min = 81;
      pattern_list[19].max = 101;
      temp_positions = "";
      temp_positions += "00000";
      temp_positions += "00000";
      temp_positions += "00100";
      temp_positions += "01010";
      temp_positions += "10101";
      pattern_list[19].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[20].min = 81;
      pattern_list[20].max = 101;
      temp_positions = "";
      temp_positions += "00000";
      temp_positions += "00100";
      temp_positions += "01010";
      temp_positions += "01110";
      temp_positions += "00000";
      pattern_list[20].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[21].min = 101;
      pattern_list[21].max = 141;
      temp_positions = "";
      temp_positions += "00000";
      temp_positions += "00000";
      temp_positions += "01110";
      temp_positions += "10001";
      temp_positions += "01110";
      pattern_list[21].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[22].min = 101;
      pattern_list[22].max = 141;
      temp_positions = "";
      temp_positions += "01110";
      temp_positions += "10001";
      temp_positions += "01110";
      temp_positions += "00000";
      temp_positions += "00000";
      pattern_list[22].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[23].min = 101;
      pattern_list[23].max = 141;
      temp_positions = "";
      temp_positions += "00110";
      temp_positions += "01000";
      temp_positions += "00100";
      temp_positions += "00010";
      temp_positions += "01100";
      pattern_list[23].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[24].min = 141;
      pattern_list[24].max = 181;
      temp_positions = "";
      temp_positions += "01010";
      temp_positions += "01010";
      temp_positions += "00100";
      temp_positions += "00100";
      temp_positions += "00100";
      pattern_list[24].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[25].min = 141;
      pattern_list[25].max = 181;
      temp_positions = "";
      temp_positions += "10000";
      temp_positions += "01000";
      temp_positions += "00100";
      temp_positions += "01010";
      temp_positions += "10001";
      pattern_list[25].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[26].min = 141;
      pattern_list[26].max = 181;
      temp_positions = "";
      temp_positions += "00001";
      temp_positions += "00010";
      temp_positions += "00100";
      temp_positions += "01010";
      temp_positions += "10001";
      pattern_list[26].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[27].min = 181;
      pattern_list[27].max = 251;
      temp_positions = "";
      temp_positions += "01000";
      temp_positions += "01000";
      temp_positions += "01000";
      temp_positions += "01000";
      temp_positions += "01110";
      pattern_list[27].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[28].min = 181;
      pattern_list[28].max = 251;
      temp_positions = "";
      temp_positions += "10001";
      temp_positions += "00010";
      temp_positions += "00100";
      temp_positions += "01000";
      temp_positions += "10001";
      pattern_list[28].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[29].min = 181;
      pattern_list[29].max = 251;
      temp_positions = "";
      temp_positions += "00001";
      temp_positions += "00010";
      temp_positions += "00100";
      temp_positions += "11000";
      temp_positions += "11000";
      pattern_list[29].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[30].min = 251;
      pattern_list[30].max = 301;
      temp_positions = "";
      temp_positions += "11100";
      temp_positions += "11000";
      temp_positions += "10100";
      temp_positions += "00000";
      temp_positions += "00000";
      pattern_list[30].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[31].min = 251;
      pattern_list[31].max = 301;
      temp_positions = "";
      temp_positions += "00111";
      temp_positions += "00011";
      temp_positions += "00101";
      temp_positions += "00000";
      temp_positions += "00000";
      pattern_list[31].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[32].min = 251;
      pattern_list[32].max = 301;
      temp_positions = "";
      temp_positions += "00001";
      temp_positions += "00010";
      temp_positions += "11100";
      temp_positions += "00010";
      temp_positions += "00001";
      pattern_list[32].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[33].min = 301;
      pattern_list[33].max = 401;
      temp_positions = "";
      temp_positions += "01000";
      temp_positions += "01000";
      temp_positions += "01000";
      temp_positions += "01000";
      temp_positions += "01111";
      pattern_list[33].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[34].min = 301;
      pattern_list[34].max = 401;
      temp_positions = "";
      temp_positions += "10001";
      temp_positions += "00100";
      temp_positions += "01010";
      temp_positions += "00100";
      temp_positions += "10001";
      pattern_list[34].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[35].min = 301;
      pattern_list[35].max = 401;
      temp_positions = "";
      temp_positions += "00100";
      temp_positions += "00100";
      temp_positions += "11011";
      temp_positions += "00100";
      temp_positions += "00100";
      pattern_list[35].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);



      pattern_list[36].min = 401;
      pattern_list[36].max = 601;
      temp_positions = "";
      temp_positions += "01010";
      temp_positions += "10001";
      temp_positions += "00000";
      temp_positions += "10001";
      temp_positions += "01010";
      pattern_list[36].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[37].min = 401;
      pattern_list[37].max = 601;
      temp_positions = "";
      temp_positions += "00010";
      temp_positions += "00011";
      temp_positions += "01010";
      temp_positions += "11000";
      temp_positions += "01000";
      pattern_list[37].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[38].min = 401;
      pattern_list[38].max = 601;
      temp_positions = "";
      temp_positions += "00010";
      temp_positions += "00101";
      temp_positions += "01010";
      temp_positions += "10100";
      temp_positions += "01000";
      pattern_list[38].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[39].min = 601;
      pattern_list[39].max = 851;
      temp_positions = "";
      temp_positions += "10000";
      temp_positions += "10000";
      temp_positions += "10000";
      temp_positions += "10000";
      temp_positions += "11111";
      pattern_list[39].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[40].min = 601;
      pattern_list[40].max = 851;
      temp_positions = "";
      temp_positions += "11111";
      temp_positions += "00001";
      temp_positions += "00001";
      temp_positions += "00001";
      temp_positions += "00001";
      pattern_list[40].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);

      pattern_list[41].min = 601;
      pattern_list[41].max = 851;
      temp_positions = "";
      temp_positions += "00001";
      temp_positions += "00001";
      temp_positions += "00001";
      temp_positions += "00001";
      temp_positions += "11111";
      pattern_list[41].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);

      pattern_list[42].min = 851;
      pattern_list[42].max = 1001;
      temp_positions = "";
      temp_positions += "11110";
      temp_positions += "10000";
      temp_positions += "11110";
      temp_positions += "10000";
      temp_positions += "10000";
      pattern_list[42].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[43].min = 851;
      pattern_list[43].max = 1001;
      temp_positions = "";
      temp_positions += "11111";
      temp_positions += "00101";
      temp_positions += "00101";
      temp_positions += "00101";
      temp_positions += "00000";
      pattern_list[43].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[44].min = 851;
      pattern_list[44].max = 1001;
      temp_positions = "";
      temp_positions += "01111";
      temp_positions += "00001";
      temp_positions += "01111";
      temp_positions += "00001";
      temp_positions += "00001";
      pattern_list[44].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[45].min = 1001;
      pattern_list[45].max = 1201;
      temp_positions = "";
      temp_positions += "11100";
      temp_positions += "11000";
      temp_positions += "10100";
      temp_positions += "00010";
      temp_positions += "00001";
      pattern_list[45].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[46].min = 1001;
      pattern_list[46].max = 1201;
      temp_positions = "";
      temp_positions += "10000";
      temp_positions += "01000";
      temp_positions += "00101";
      temp_positions += "00011";
      temp_positions += "00111";
      pattern_list[46].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[47].min = 1001;
      pattern_list[47].max = 1201;
      temp_positions = "";
      temp_positions += "00111";
      temp_positions += "00011";
      temp_positions += "00101";
      temp_positions += "01000";
      temp_positions += "10000";
      pattern_list[47].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[48].min = 1201;
      pattern_list[48].max = 2001;
      temp_positions = "";
      temp_positions += "10001";
      temp_positions += "10001";
      temp_positions += "10001";
      temp_positions += "10001";
      temp_positions += "10001";
      pattern_list[48].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[49].min = 1201;
      pattern_list[49].max = 2001;
      temp_positions = "";
      temp_positions += "11111";
      temp_positions += "00000";
      temp_positions += "00000";
      temp_positions += "00000";
      temp_positions += "11111";
      pattern_list[49].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[50].min = 1201;
      pattern_list[50].max = 2001;
      temp_positions = "";
      temp_positions += "00010";
      temp_positions += "10101";
      temp_positions += "01010";
      temp_positions += "10101";
      temp_positions += "01000";
      pattern_list[50].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[51].min = 2001;
      pattern_list[51].max = 3001;
      temp_positions = "";
      temp_positions += "00100";
      temp_positions += "01110";
      temp_positions += "01110";
      temp_positions += "01110";
      temp_positions += "00100";
      pattern_list[51].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[52].min = 2001;
      pattern_list[52].max = 3001;
      temp_positions = "";
      temp_positions += "10101";
      temp_positions += "01010";
      temp_positions += "10101";
      temp_positions += "01010";
      temp_positions += "00100";
      pattern_list[52].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[53].min = 2001;
      pattern_list[53].max = 3001;
      temp_positions = "";
      temp_positions += "10001";
      temp_positions += "10001";
      temp_positions += "10001";
      temp_positions += "10001";
      temp_positions += "01110";
      pattern_list[53].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[54].min = 3001;
      pattern_list[54].max = 5001;
      temp_positions = "";
      temp_positions += "10001";
      temp_positions += "11011";
      temp_positions += "10001";
      temp_positions += "10001";
      temp_positions += "10001";
      pattern_list[54].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);

      pattern_list[55].min = 3001;
      pattern_list[55].max = 5001;
      temp_positions = "";
      temp_positions += "11111";
      temp_positions += "01000";
      temp_positions += "00000";
      temp_positions += "00010";
      temp_positions += "11111";
      pattern_list[55].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[56].min = 3001;
      pattern_list[56].max = 5001;
      temp_positions = "";
      temp_positions += "11111";
      temp_positions += "11111";
      temp_positions += "00000";
      temp_positions += "00100";
      temp_positions += "00100";
      pattern_list[56].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[57].min = 5001;
      pattern_list[57].max = 7500;
      temp_positions = "";
      temp_positions += "01010";
      temp_positions += "10001";
      temp_positions += "10001";
      temp_positions += "10001";
      temp_positions += "11011";
      pattern_list[57].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[58].min = 5001;
      pattern_list[58].max = 7500;
      temp_positions = "";
      temp_positions += "11011";
      temp_positions += "01010";
      temp_positions += "01010";
      temp_positions += "01010";
      temp_positions += "01010";
      pattern_list[58].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[59].min = 5001;
      pattern_list[59].max = 7500;
      temp_positions = "";
      temp_positions += "00000";
      temp_positions += "01001";
      temp_positions += "00111";
      temp_positions += "00111";
      temp_positions += "01111";
      pattern_list[59].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[60].min = 7500;
      pattern_list[60].max = 10000;
      temp_positions = "";
      temp_positions += "01010";
      temp_positions += "01010";
      temp_positions += "11111";
      temp_positions += "01010";
      temp_positions += "01010";
      pattern_list[60].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[61].min = 7500;
      pattern_list[61].max = 10000;
      temp_positions = "";
      temp_positions += "11111";
      temp_positions += "01000";
      temp_positions += "00100";
      temp_positions += "00010";
      temp_positions += "11111";
      pattern_list[61].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[62].min = 7500;
      pattern_list[62].max = 10000;
      temp_positions = "";
      temp_positions += "10001";
      temp_positions += "10011";
      temp_positions += "10101";
      temp_positions += "11001";
      temp_positions += "10001";
      pattern_list[62].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);


      pattern_list[63].min = 10000;
      pattern_list[63].max = long.MaxValue;
      temp_positions = "";
      temp_positions += "11111";
      temp_positions += "00100";
      temp_positions += "10101";
      temp_positions += "00100";
      temp_positions += "11111";
      pattern_list[63].positions = Convert.ToInt64(Reverse(temp_positions,5,5), 2);

    }

    /// <summary>
    /// Insterts the array patterns previously created
    /// </summary>
    /// <param name="SqlTrx"></param>
    /// <returns></returns>
    public int InsertPatternsToDB(SqlTransaction SqlTrx)
    {

      int num_rows_inserted = 0;
      String sql_str;
      SqlCommand sql_command;

      if (pattern_list.Length == 0)
      {
        return 0;
      }
      sql_str = "";

      sql_str += " INSERT INTO C2_WINNING_PATTERNS (W2P_PATTERN_ID,W2P_MIN_PRIZE,W2P_MAX_PRIZE,W2P_POSITION_LIST) ";
      sql_str += "      VALUES          (@p1, @p2, @p3, @p4)                  ";

      sql_command = new SqlCommand(sql_str, SqlTrx.Connection, SqlTrx);

      sql_command.Parameters.Add("@p1", SqlDbType.Int, Int32.MaxValue, "W2P_PATTERN_ID");
      sql_command.Parameters.Add("@p2", SqlDbType.BigInt, Int32.MaxValue, "W2P_MIN_PRIZE");
      sql_command.Parameters.Add("@p3", SqlDbType.BigInt, Int32.MaxValue, "W2P_MAX_PRIZE");
      sql_command.Parameters.Add("@p4", SqlDbType.BigInt, Int32.MaxValue, "W2P_POSITION_LIST");

      for(int i = 0; i<pattern_list.Length;i++)
      {
        sql_command.Parameters[0].Value = i + 1;
        sql_command.Parameters[1].Value = pattern_list[i].min;
        sql_command.Parameters[2].Value = pattern_list[i].max;
        sql_command.Parameters[3].Value = pattern_list[i].positions;

        if (sql_command.ExecuteNonQuery() != 1)
        {
          Log.Error("InsertPatternToDB. Error while inserting new pattern to the database");

          return 0;
        }

        num_rows_inserted++;
      }

      return num_rows_inserted;
    }
  }
}
