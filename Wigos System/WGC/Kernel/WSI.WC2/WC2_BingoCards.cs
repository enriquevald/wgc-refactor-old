//------------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_BingoCards.cs
// 
//   DESCRIPTION: 
// 
//        AUTHOR: Armando Alva
// 
// CREATION DATE: 14-AUG-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-AUG-2008 AAV    First release.
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;
using System.Xml;
using System.Data.SqlClient;
using System.Data;
using WSI.Common;
using WSI.WC2.BingoCards;


namespace WSI.WC2.BingoCards
{
  struct Range
  {
    public Int32 Min;
    public Int32 Max;

    public Range(Int32 min, Int32 max)
    {
      Min = min;
      Max = max;
    }
  }

  struct CardToDB
  {
    string id;
    string xml_data;

    public string Id
    {
      get
      {
        return id;
      }
    }

    public string XmlData
    {
      get
      {
        return xml_data;
      }
    }

    public CardToDB(string Id, string Numbers)
    {
      id = Id;
      xml_data = Numbers;
    }
  }

  /// <summary>
  /// Represents a single Bingo Card data structure
  /// </summary>
  class Card
  {
    int num_rows;
    int num_cols;
    int col_total_numbers;
    Range[] col_ranges;

    int seed;
    int[,] numbers;

    public int[,] Numbers
    {
      get
      {
        return numbers;
      }
    }

    public int Seed
    {
      get
      {
        return seed;
      }
    }

    public Card(int Seed, int NumRows, int NumCols, int NumbersPerColumn)
    {
      seed = Seed;
      num_rows = NumRows;
      num_cols = NumCols;
      col_total_numbers = NumbersPerColumn;
      numbers = new int[num_rows, num_cols];

      InitCardRanges();
      GenerateNumbers();
    }

    private void InitCardRanges()
    {
      col_ranges = new Range[num_cols];

      int index_start = 1;

      for (int col = 0; col < num_cols; col++)
      {
        col_ranges[col] = new Range(index_start, col_total_numbers * (col + 1));
        index_start += col_total_numbers;
      }
    }

    private void GenerateNumbers()
    {

      Random rnd = new Random(seed);

      for (int col = 0; col < num_cols; col++)
      {
        int[] current_col = new int[num_rows];

        for (int row = 0; row < num_rows; row++)
        {
          int tmp_value = 0;

          do
          {
            tmp_value = rnd.Next(col_ranges[col].Min, col_ranges[col].Max + 1);
          } while (Contains(current_col, tmp_value));

          current_col[row] = tmp_value;
        }

        AssignCol(current_col, col);
      }
    }

    private void AssignCol(int[] ColumnData, int ColumnNumber)
    {
      Array.Sort(ColumnData);

      for (int row = 0; row < num_rows; row++)
      {
        numbers[ColumnNumber, row] = ColumnData[row];
      }
    }

    private bool Contains(int[] Array, int Value)
    {
      for (int n = 0; n < Array.GetLength(0); n++)
      {
        if (Array[n] == Value)
        {
          return true;
        }
      }

      return false;
    }

    public void ConsoleOutput()
    {      
      Console.WriteLine("BINGO CARD\n");
      Console.WriteLine("Seed: " + seed.ToString());
      for (int col = 0; col < num_cols; col++)
      {
        for (int row = 0; row < num_rows; row++)
        {
          if (numbers[row, col] < 10)
          {
            Console.Write(numbers[row, col].ToString() + "  | ");
          }
          else
          {
            Console.Write(numbers[row, col].ToString() + " | ");
          }
        }

        Console.Write("\n");
      }
      Console.WriteLine("---------------------------\n\n");
    }

    public string XmlOutput()
    {
      Stream xml_stream;
      XmlTextWriter xml_writer;
      StreamReader stream_reader;

      xml_stream = new MemoryStream();
      xml_writer = new XmlTextWriter(xml_stream, Encoding.UTF8);

      xml_writer.WriteStartElement("Card");
      {
        xml_writer.WriteElementString("CardId", CardIdToString());
        xml_writer.WriteElementString("CardType", "Card5x5");
        xml_writer.WriteElementString("CardNumbers", NumbersToString());
      }
      xml_writer.WriteEndElement();

      xml_writer.Flush();
      xml_stream.Position = 0;

      stream_reader = new StreamReader(xml_stream);

      return stream_reader.ReadToEnd();
    }

    private string CardIdToString()
    {
      return "C5x5-" + string.Format("{0:X8}", seed).ToString();
    }

    private string NumbersToString()
    {
      string numbers_to_string = "";

      foreach (int number in numbers)
      {
        if (numbers_to_string.Length > 0)
        {
          numbers_to_string += " ";
        }
        numbers_to_string += number.ToString("00");
      }

      return numbers_to_string;
    }

    internal CardToDB DbOutput()
    {
      return new CardToDB(CardIdToString(), XmlOutput());
    }
  }
}

namespace WSI.WC2
{
  /// <summary>
  /// Manages collections of Cards
  /// </summary>
  public class CardManager
  {
    #region  Class Members

    ArrayList card_list;

    int card_num_cols;
    int card_num_rows;
    int card_nums_per_col;
    float quality;

    const float starting_quality = 80f;
    const float quality_increment = 1f;
    const int max_tries = 500;
    int seed = 0;

    public const int MIN_CARDS_NEEDED = 1000;

    #endregion

    #region Public Methods

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="CardCols">How many columns the bingo card must have</param>
    /// <param name="CardRows">How many rows the bingo card must have</param>
    /// <param name="NumsPerCol">The numeric range that each column has ie. 1 to 15, 16 to 30, etc...</param>
    public CardManager(int CardCols, int CardRows, int NumsPerCol)
    {
      card_list = new ArrayList();

      card_num_cols = CardCols;
      card_num_rows = CardRows;
      card_nums_per_col = NumsPerCol;

      quality = starting_quality;
    }

    /// <summary>
    /// Debuggin purposes only: shows how many cards are currently stored in the card list
    /// </summary>
    public void ConsoleOutput()
    {
      Console.WriteLine("TOTAL CARDS STORED: " + card_list.Count.ToString());

      foreach (Card card in card_list)
      {
        card.ConsoleOutput();
      }
    }

    /// <summary>
    /// Adds a card with a specific seed 
    /// </summary>
    /// <param name="Seed">the seed to use for generating the card</param>
    public void AddCard(int Seed)
    {
      card_list.Add(CreateCard(Seed));
    }

    /// <summary>
    /// Adds a card with a specific seed and returns the card numbers
    /// </summary>
    /// <param name="Seed">the seed to use for generating the card</param>
    public int[,] GetNewCardNumbers(int Seed)
    {
      card_list.Add(CreateCard(Seed));
      return new Card(Seed, this.card_num_rows, this.card_num_cols, this.card_nums_per_col).Numbers;
    }

    /// <summary>
    /// Generates a card using the internal seed and then stores it to the card list
    /// </summary>
    public void AddCard()
    {
      Card card;
      int num_tries;

      num_tries = 0;

      float card_quality = 0;

      while (true)
      {
        card = CreateCard(seed++);
        card_quality = CalculateCardQuality(card);
        if (card_quality > quality)
        {
          num_tries++;
          if (num_tries % max_tries == 0)
          {
            quality += quality_increment;
          }
        }
        else
        {
          card_list.Add(card);
          break;
        }
      }

#if DEBUG
      Console.Clear();
      TotalCardsOutput();
      Console.WriteLine("Tries before add: " + num_tries.ToString());
      Console.WriteLine("Card Match: " + card_quality.ToString() + "%");
      Console.WriteLine("Max Match Allowed: " + quality.ToString() + "%");
      card.ConsoleOutput();
      Console.WriteLine(card.XmlOutput());
#endif
    }

    /// <summary>
    /// Clears all cards from the card list and resets the seed to 0
    /// </summary>
    public void Clear()
    {
      seed = 0;
      this.card_list.Clear();
    }

    /// <summary>
    /// Insert cards stored to the DB
    /// </summary>
    /// <param name="SqlTrx">The sql transaction to the current DB</param>
    /// <returns>The number of cards inserted</returns>
    public int InsertCardsToDB(SqlTransaction SqlTrx)
    {
      int num_rows_inserted = 0;
      String        sql_str;
      SqlCommand    sql_command;

      if (card_list.Count == 0)
      {
        return 0;
      }
      sql_str = "";

      sql_str += " INSERT INTO C2_CARDS (C2C_CARD_ID, C2C_CONTENTS) ";
      sql_str += "      VALUES          (@p1, @p2)                  ";

      sql_command = new SqlCommand(sql_str, SqlTrx.Connection, SqlTrx);      

      sql_command.Parameters.Add("@p1", SqlDbType.VarChar, 20, "C2C_CARD_ID");
      sql_command.Parameters.Add("@p2", SqlDbType.Xml,Int32.MaxValue, "C2C_CONTENTS");


      foreach (Card card in card_list)
      {
        CardToDB card_to_db = card.DbOutput();

        sql_command.Parameters[0].Value = card_to_db.Id;
        sql_command.Parameters[1].Value = card_to_db.XmlData;

        if (sql_command.ExecuteNonQuery() != 1)
        {
          Log.Error("InsertCardsToDB. Error while inserting new card to the database");

          return 0;
        }

        num_rows_inserted++;
      }

      return num_rows_inserted;
    }

    /// <summary>
    /// Checks if there are cards stored inside the database
    /// </summary>
    /// <param name="SqlTrx">The sql transaction to the current DB</param>
    /// <returns>True if there are 1000 cards or more, False otherwise</returns>
    static public Boolean CheckDBCards(SqlTransaction SqlTrx)
    {
      String        sql_str;
      SqlCommand    sql_command;
      int           cards_stored = 0;

      sql_str = "";
      sql_str = " SELECT COUNT(*) FROM C2_CARDS ";

      sql_command             = new SqlCommand(sql_str);
      sql_command.Connection  = SqlTrx.Connection;
      sql_command.Transaction = SqlTrx;

      #region Read from Database
      try
      {
        cards_stored = ((int)sql_command.ExecuteScalar());

      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }

      #endregion

      if (cards_stored < CardManager.MIN_CARDS_NEEDED)
      {
        return false;
      }
      else
      {
        return true;
      }
    }

    #endregion

    #region Private Methods

    private void TotalCardsOutput()
    {
      Console.WriteLine("TOTAL CARDS STORED: " + card_list.Count.ToString());
    }

    private Card CreateCard(int Seed)
    {
      return new Card(Seed, this.card_num_rows, card_num_cols, card_nums_per_col);
    }

    private float CalculateCardQuality(Card CardToCheck)
    {
      if (card_list.Count == 0)
      {
        return 0f; // no match %
      }

      int total_numbers = card_num_cols * card_num_rows;
      int highest_match = 0;

      foreach (Card card in card_list)
      {
        int current_match = 0;

        foreach (int list_number in card.Numbers)
        {
          foreach (int card_number in CardToCheck.Numbers)
          {
            if (list_number == card_number)
            {
              current_match++;
            }
          }
        }

        if (current_match > highest_match)
        {
          highest_match = current_match;
        }
      }

      return highest_match * 100.0f / (float)total_numbers;

    }

    #endregion
  }

}
