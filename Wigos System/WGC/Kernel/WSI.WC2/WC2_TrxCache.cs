//------------------------------------------------------------------------------
// Copyright � 2010 Win Systems International.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_TrxCache.cs
// 
//   DESCRIPTION: Implements Trx cache class
//
//        AUTHOR: 
// 
// CREATION DATE: 02-JUL-2010 
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 02-JUL-2010 MBF    First version
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Data.SqlClient;
using WSI.Common;
using System.Data;
using System.Timers;
using System.Collections;

namespace WSI.WC2
{
  public static class WC2_TrxCache
  {
    #region Members

    static Thread m_thread_cache;
    //static internal System.Threading.AutoResetEvent[] m_event_reload;
    //static internal System.Threading.ManualResetEvent[] m_event_reloaded;

    static ReaderWriterLock m_dbcache_lock;
    static DataSet m_ds;
    static SqlDataAdapter m_da;
    static Boolean m_tables_loaded;

    #endregion Members

    #region Public

    //------------------------------------------------------------------------------
    // PURPOSE: Initialize class attributes
    //
    // PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //        
    // NOTES :
    //
    public static void Init()
    {
      SqlConnection _conn;

      // Cache
      m_thread_cache = new Thread(TrxCacheThread);
      m_thread_cache.Name = "WC2_TrxCache";
  
      // Data Set & Data Adapter
      m_ds = new DataSet();
      m_da = new SqlDataAdapter();

      // Init ReaderWriterLock
      m_dbcache_lock = new ReaderWriterLock();

      m_tables_loaded = false;
      _conn = null;

      try
      {
        // Init tables and get info by first time
        _conn = WGDB.Connection();
        m_tables_loaded = InitTables(_conn);
      }
      catch (Exception ex)
      {
        Log.Exception(ex);
      }
      finally
      {
        if (_conn != null)
        {
          _conn.Close();
          _conn = null;
        }
      }
    } // Init

    //------------------------------------------------------------------------------
    // PURPOSE: Start thread
    //
    // PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //        
    // NOTES :
    //
    public static void Start()
    {
      m_thread_cache.Start();
    } // Start


    //------------------------------------------------------------------------------
    // PURPOSE: Get software version from the dataset
    //
    // PARAMS:
    //      - INPUT:
    //          - TerminalId 
    //
    //      - OUTPUT:
    //          - ClientId 
    //          - BuildId
    //          - TerminalType
    //
    // RETURNS: 
    //        
    // NOTES :
    //
    public static void GetSoftwareVersion(int TerminalId, out int ClientId, out int BuildId, out string TerminalType)
    {
      DataRow[] _row;
      Int16 _terminal_type;

      ClientId = 0;
      BuildId = 0;
      TerminalType = "";

      m_dbcache_lock.AcquireReaderLock(Timeout.Infinite);
      try
      {
        _row = m_ds.Tables["TERMINALS"].Select("TE_TERMINAL_ID = " + TerminalId);

        _terminal_type = (Int16)_row[0]["TE_TERMINAL_TYPE"];

        _row = m_ds.Tables["TERMINAL_SOFTWARE_VERSIONS"].Select("TSV_TERMINAL_TYPE = " + _terminal_type);

        ClientId = (int)_row[0]["TSV_CLIENT_ID"];
        BuildId = (int)_row[0]["TSV_BUILD_ID"];
        TerminalType = _row[0]["TSV_TERMINAL_TYPE"].ToString();
      }
      finally
      {
        m_dbcache_lock.ReleaseReaderLock();
      }

    } // GetSoftwareVersion

    //------------------------------------------------------------------------------
    // PURPOSE: Get ftp locations from the dataset
    //
    // PARAMS:
    //      - INPUT:
    //
    //      - OUTPUT:
    //
    // RETURNS: 
    //        
    // NOTES :
    //
    public static ArrayList GetFtpLocations()
    {
      DataRow[] _row_list;
      ArrayList _locations;
      
      _locations = new ArrayList();

      m_dbcache_lock.AcquireReaderLock(Timeout.Infinite);
      try
      {
        _row_list = m_ds.Tables["GENERAL_PARAMS"].Select("      GP_GROUP_KEY   = 'Software.Download' " +
                                                                    " AND (GP_SUBJECT_KEY = 'Location1' " +
                                                                    "   OR GP_SUBJECT_KEY = 'Location2')");

        foreach (DataRow _row in _row_list)
        {
          _locations.Add((string)_row["GP_KEY_VALUE"]);
        }
      }
      finally
      {
        m_dbcache_lock.ReleaseReaderLock();
      }

      return _locations;
    } // GetFtpLocations

    #endregion Public



    #region Private

    private static Boolean InitTables(SqlConnection Connection)
    {
      SqlCommand _sql_command;
      String _sql_str;

      _sql_command = new SqlCommand();
      _sql_command.Connection = Connection;
      m_da.SelectCommand = _sql_command;

      _sql_str = " SELECT TE_TERMINAL_ID " +
                      " , TE_CLIENT_ID " +
                      " , TE_BUILD_ID " +
                      " , TE_TERMINAL_TYPE " +
                 "   FROM TERMINALS ";
      _sql_str += " ; ";

      _sql_str += " SELECT MAX(TSV_CLIENT_ID), MAX(TSV_BUILD_ID), TSV_TERMINAL_TYPE" +
                  "   FROM TERMINAL_SOFTWARE_VERSIONS " +
                  " GROUP BY TSV_TERMINAL_TYPE ";
      _sql_str += " ; ";

      // TODO: Relacionar TE_TERMINAL_ID amb TSV_TERMINAL_TYPE ??

      _sql_str += " SELECT * " +
                  "   FROM GENERAL_PARAMS ";
      _sql_str += " ; ";

      _sql_command.CommandText = _sql_str;

      // read tables
      m_da.Fill(m_ds);

      m_ds.Tables[0].TableName = "TERMINALS";
      m_ds.Tables[1].TableName = "TERMINAL_SOFTWARE_VERSIONS";
      m_ds.Tables[2].TableName = "GENERAL_PARAMS";

      // Only read-only 
      //SqlCommandBuilder _sql_commandbuilder = new SqlCommandBuilder(m_da_en_plancha);
      //m_da.UpdateCommand = _sql_commandbuilder.GetUpdateCommand();

      return true;

    } // InitTables

    private static void ReloadTables(SqlConnection Connection)
    {
      DataSet _ds_temp;

      _ds_temp = new DataSet();

      m_da.SelectCommand.Connection = Connection;

      // read tables
      m_da.Fill(_ds_temp);

      // Critical section, lock.
      m_dbcache_lock.AcquireWriterLock(Timeout.Infinite);
      
      m_ds = _ds_temp;

      m_dbcache_lock.ReleaseWriterLock();

    } // ReloadTables

    static private void TrxCacheThread()
    {
      SqlConnection _conn;

      while (true)
      {

        if ( !m_tables_loaded )
        {
          continue;
        }

        _conn = null;

        try
        {
          // Wait one minute!
          System.Threading.Thread.Sleep(60000);

          _conn = WGDB.Connection();
          ReloadTables(_conn);
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
        finally
        {
          if (_conn != null)
          {
            _conn.Close();
            _conn = null;
          }
        }
      }
    } // TrxCacheThread

    #endregion Private
  }
}
