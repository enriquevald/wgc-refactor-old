//------------------------------------------------------------------------------
// Copyright � 2008 Win Systems Ltd.
//------------------------------------------------------------------------------
// 
//   MODULE NAME: WC2_PendingPlaysQueue.cs
// 
//   DESCRIPTION: WC2_PendingPlaysQueue class
// 
//        AUTHOR: Alberto Cuesta / Armando Alva
// 
// CREATION DATE: 14-AUG-2008
// 
// REVISION HISTORY:
// 
// Date        Author Description
// ----------- ------ ----------------------------------------------------------
// 14-AUG-2008 ACC/AAV    First release.
//------------------------------------------------------------------------------

using System;
using System.Messaging;
using System.Collections.Generic;
using System.Text;
using System.Net;
using WSI.Common;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using WSI.WC2;
using System.Collections;
using System.Threading;

namespace WSI.WC2
{
  /// <summary>
  /// Represents a play that is waiting to be processed 
  /// </summary>
  public class PlayQueueElement
  {
    private Object m_wc2_listener;
    private Object m_wc2_client;
    private WC2_Message m_xml_message_play;


    public PlayQueueElement(Object Wc2Listener, Object Wc2Client, WC2_Message XmlMessagePlay)
    {
      m_wc2_listener = Wc2Listener;
      m_wc2_client = Wc2Client;
      m_xml_message_play = XmlMessagePlay;
    }

    public Object Wc2Listener
    {
      get
      {
        return m_wc2_listener;
      }
      set
      {
        m_wc2_listener = value;
      }
    }

    public Object Wc2Client
    {
      get
      {
        return m_wc2_client;
      }
      set
      {
        m_wc2_client = value;
      }
    }

    public WC2_Message XmlMessagePlay
    {
      get
      {
        return m_xml_message_play;
      }
      set
      {
        m_xml_message_play = value;
      }
    }
  }

  public class DrawTaskElement : Task
  {
    int m_num_plays;
    Object m_wc2_listener_1;
    Object m_wc2_client_1;
    WC2_Message m_xml_message_play_1;
    Object m_wc2_listener_2;
    Object m_wc2_client_2;
    WC2_Message m_xml_message_play_2;

    public DrawTaskElement(Object Wc2Listener1, Object Wc2Client1, WC2_Message XmlMessagePlay1,
                           Object Wc2Listener2, Object Wc2Client2, WC2_Message XmlMessagePlay2)
    {
      m_num_plays = 2;
      m_wc2_listener_1 = Wc2Listener1;
      m_wc2_client_1 = Wc2Client1;
      m_xml_message_play_1 = XmlMessagePlay1;
      m_wc2_listener_2 = Wc2Listener2;
      m_wc2_client_2 = Wc2Client2;
      m_xml_message_play_2 = XmlMessagePlay2;
    }
    public DrawTaskElement(Object Wc2Listener1, Object Wc2Client1, WC2_Message XmlMessagePlay1)
    {
      m_num_plays = 1;
      m_wc2_listener_1 = Wc2Listener1;
      m_wc2_client_1 = Wc2Client1;
      m_xml_message_play_1 = XmlMessagePlay1;
      m_wc2_listener_2 = null;
      m_wc2_client_2 = null;
      m_xml_message_play_2 = null;
    }

    /// <summary>
    /// Depending on the num of plays it will call the corresponding draw
    /// </summary>
    public override void Execute()
    {
      if (m_num_plays == 1)
      {
        WC2_PendingPlaysQueue.DrawOnePlay(m_wc2_listener_1, m_wc2_client_1, m_xml_message_play_1);
      }
      else
      {
        WC2_PendingPlaysQueue.DrawTwoPlays(m_wc2_listener_1, m_wc2_client_1, m_xml_message_play_1,
                                           m_wc2_listener_2, m_wc2_client_2, m_xml_message_play_2);
      }
    }
  }

  /// <summary>
  /// Holds play data that awaits to be processed
  /// </summary>
  public class WC2_PendingPlaysQueue
  {
    private Worker wc2_draw_worker;
    private WaitableQueue wc2_single_play_waitable_queue;
    private WaitableQueue wc2_single_draw_waitable_queue;

    private Thread m_drawtype_thread;
    private ArrayList Plays2Players;
    private ArrayList Plays1Player;

    private const int MEDIUM_PLAYS_INTERVAL = 3000; //default interval between plays to allow another play. Less than the time of a play spin
    private const int HIGH_PLAYS_INTERVAL = 1200000; //interval between plays to allow another play very high, to allow many 1 player darws
    private const int MIN_DRAWS_2P_TO_ALLOW_DRAW_1P = 1;
    private const int MIN_DRAWS_1P_TO_ALLOW_DRAW_1P = 1;
    private const int CHECK_INTERVAL_FOR_DRAW_1P = 50; //delay interval to chek if allowed 1p draw
    private const int SEVERITY_REFRESH_INTERVAL = 60; // Seconds!

    private const int MEDIUM_MAX_WAIT = 200; //default time to wait for another player
    private const int LOW_MAX_WAIT = 50; //time to
    private const int NONE_MAX_WAIT = 0; //time to


    private int plays_interval = MEDIUM_PLAYS_INTERVAL; //default interval between plays to allow another play
    private int max_wait = MEDIUM_MAX_WAIT; //default milliseconds between 
    private WC2_Severity wc2_severity = WC2_Severity.Medium; //default severity value
    private DateTime last_enqued_play_time;
    private int m_draw_id = 0;

    public enum WC2_Severity
    {
      None,
      Medium,
      Draw2Players
    }


    private static WC2_PendingPlaysQueue wc2_pending_plays;

    /// <summary>
    /// Instatiates an instance of the WC2_PendingPlaysQueue class 
    /// </summary>
    public static void Init()
    {
      wc2_pending_plays = new WC2_PendingPlaysQueue();

      wc2_pending_plays.m_drawtype_thread = new Thread(DrawTypeThread);
      wc2_pending_plays.m_drawtype_thread.Name = "WC2_DrawTypeThread";
      wc2_pending_plays.Plays2Players = new ArrayList();
      wc2_pending_plays.Plays1Player = new ArrayList();
    }

    /// <summary>
    /// Returns the Current instance of the WC2_PendingPlaysQueue Class
    /// </summary>
    /// <returns>WC2_PendingPlaysQueue class</returns>
    public static WC2_PendingPlaysQueue GetInstance()
    {
      return wc2_pending_plays;
    }


    /// <summary>
    /// Start the worker
    /// </summary>
    private void StartWorker()
    {
      // Create queue plays
      wc2_single_play_waitable_queue = new WaitableQueue();

      // - Workers input queue (draws)
      wc2_single_draw_waitable_queue = new WaitableQueue();

      // - Message received processing worker
      // Start workers for WC2 requests
      for (int i = 0; i < 20; i++)
      {
        wc2_draw_worker = new Worker(wc2_single_draw_waitable_queue);
        wc2_draw_worker.Start();
      }
    }

    /// <summary>
    /// Starts the threads
    /// </summary>
    public static void Start()
    {
      // Workers start
      wc2_pending_plays.StartWorker();
      wc2_pending_plays.m_drawtype_thread.Start();
    }


    /// <summary>
    /// This method adds timestamp to an array when a draw with two plays occurr
    /// </summary>
    public void Draw2PlayerExecuted()
    {
      lock (wc2_pending_plays.Plays2Players)
      {
        Plays2Players.Add(DateTime.Now);
      }
    }

    /// <summary>
    /// This method adds timestamp to an array when a draw with one plays occurr
    /// </summary>
    public void Draw1PlayerExecuted()
    {
      lock (wc2_pending_plays.Plays1Player)
      {
        Plays1Player.Add(DateTime.Now);
      }
    }


    /// <summary>
    /// Determines if the status of the service permits the draws of one play
    /// </summary>
    /// <returns></returns>
    private static bool DrawOnePlayAllowed()
    {
      ArrayList temp_plays;
      DateTime now;
      Boolean allowed;

      TimeSpan elapsed;

      lock (wc2_pending_plays.Plays2Players)
      {
        temp_plays = new ArrayList(wc2_pending_plays.Plays2Players);
      }
      now = DateTime.Now;

      //Removes the plays that are not in the last "milis" time
      foreach (DateTime draw_2p_time in temp_plays)
      {
        elapsed = now - draw_2p_time;
        if (elapsed.TotalMilliseconds > wc2_pending_plays.plays_interval)
        {
          lock (wc2_pending_plays.Plays2Players)
          {
            wc2_pending_plays.Plays2Players.Remove(draw_2p_time);
          }
        }
      }

      lock (wc2_pending_plays.Plays1Player)
      {
        temp_plays = new ArrayList(wc2_pending_plays.Plays1Player);
      }
      now = DateTime.Now;

      //Removes the plays that are not in the last "milis" time
      foreach (DateTime draw_2p_time in temp_plays)
      {
        elapsed = now - draw_2p_time;
        if (elapsed.TotalMilliseconds > wc2_pending_plays.plays_interval)
        {
          lock (wc2_pending_plays.Plays1Player)
          {
            wc2_pending_plays.Plays1Player.Remove(draw_2p_time);
          }
        }
      }

      switch (wc2_pending_plays.wc2_severity)
      {
        case WC2_Severity.None:
          {
            wc2_pending_plays.max_wait = 0;
            allowed = true;
          }
          break;
        case WC2_Severity.Medium:
          {
            if ((wc2_pending_plays.Plays2Players.Count >= MIN_DRAWS_2P_TO_ALLOW_DRAW_1P)
               || (wc2_pending_plays.Plays1Player.Count >= MIN_DRAWS_1P_TO_ALLOW_DRAW_1P))
            {
              wc2_pending_plays.plays_interval = MEDIUM_PLAYS_INTERVAL;
              wc2_pending_plays.max_wait = MEDIUM_MAX_WAIT;
              allowed = true;
            }
            else
            {
              wc2_pending_plays.plays_interval = MEDIUM_PLAYS_INTERVAL;
              wc2_pending_plays.max_wait = MEDIUM_MAX_WAIT;
              allowed = false;
            }
          }
          break;
        case WC2_Severity.Draw2Players:
          {
            allowed = false;
          }
          break;
        default:
          {
            wc2_pending_plays.plays_interval = MEDIUM_PLAYS_INTERVAL;
            wc2_pending_plays.max_wait = MEDIUM_MAX_WAIT;
            allowed = true;
          }
          break;

      }
      return allowed;
    }

    /// <summary>
    /// Reads the serverity parameter in the database
    /// </summary>
    private static void ReadSeverity()
    {
      Int32 _severity_value;

      wc2_pending_plays.wc2_severity = WC2_Severity.Medium;
      if (Int32.TryParse(Misc.ReadGeneralParams("Class II", "Severity"), out _severity_value))
      {
        wc2_pending_plays.wc2_severity = (WC2_Severity)_severity_value;
      }
    }

    /// <summary>
    /// Determines if the enqued draw is enqueued too much time, and lunches a draw of a single play if necessary
    /// </summary>
    private static void DrawTypeThread()
    {
      int wait_hint;
      DrawTaskElement draw_task_element;
      PlayQueueElement enqueued_element;
      DateTime last_severity_read;

      System.TimeSpan elapsed_time_since_play_enqueued;

      wait_hint = CHECK_INTERVAL_FOR_DRAW_1P;

      last_severity_read = DateTime.Now.AddSeconds(-60);

      while (true)
      {
        System.Threading.Thread.Sleep(wait_hint);

        wait_hint = CHECK_INTERVAL_FOR_DRAW_1P;

        if (((TimeSpan)(DateTime.Now - last_severity_read)).TotalSeconds > SEVERITY_REFRESH_INTERVAL)
        {
          last_severity_read = DateTime.Now;
          ReadSeverity();
        }

        if (!DrawOnePlayAllowed())
        {
          continue;
        }

        lock (wc2_pending_plays.wc2_single_play_waitable_queue)
        {
          if (wc2_pending_plays.wc2_single_play_waitable_queue.Count == 0)
          {
            continue;
          }
          elapsed_time_since_play_enqueued = DateTime.Now - wc2_pending_plays.last_enqued_play_time;
          if (elapsed_time_since_play_enqueued.TotalMilliseconds < wc2_pending_plays.max_wait)
          {
            wait_hint = (int)(wc2_pending_plays.max_wait - elapsed_time_since_play_enqueued.TotalMilliseconds);
            wait_hint = Math.Min(wait_hint, CHECK_INTERVAL_FOR_DRAW_1P);

            continue;
          }

          enqueued_element = (PlayQueueElement)wc2_pending_plays.wc2_single_play_waitable_queue.Dequeue();
          draw_task_element = new DrawTaskElement(enqueued_element.Wc2Listener, enqueued_element.Wc2Client, enqueued_element.XmlMessagePlay);
          wc2_pending_plays.wc2_single_draw_waitable_queue.Enqueue(draw_task_element);
        }
      }
    }




    /// <summary>
    /// Enqueues a play element
    /// </summary>
    /// <param name="PlayElement">The play element to be stored in the queue</param>
    /// <returns>
    ///            - True: the play return PLAY DELAYED. 2 cases:   
    ///                                                     1.- Queue is empty.
    ///                                                     2.- The same Play (terminal_id & transaction_id) is already enqueued.
    ///            - False: the play does not have anything to return: There is other Play enqueued and the draw can be made.
    /// </returns>
    public Boolean EnqueuePlay(PlayQueueElement PlayElement)
    {
      DrawTaskElement draw_task_element;
      PlayQueueElement enqueued_element;

      // Current
      WC2_MsgPlay current_play;
      SecureTcpClient current_client;

      // Enqueued
      WC2_MsgPlay enqueued_play;
      SecureTcpClient enqueued_client;

      lock (wc2_single_play_waitable_queue)
      {
        if (wc2_single_play_waitable_queue.Count == 0)
        {

          wc2_single_play_waitable_queue.Enqueue(PlayElement);
          last_enqued_play_time = DateTime.Now;

          if (wc2_pending_plays.wc2_severity == WC2_Severity.None)
          {
            // NOT RETURN DELAYED
            return false;
          }

          // RETURN DELAYED
          return true;
        }
        else
        {
          enqueued_element = (PlayQueueElement)wc2_single_play_waitable_queue.Dequeue();

          enqueued_client = (SecureTcpClient)enqueued_element.Wc2Client;
          enqueued_play = (WC2_MsgPlay)enqueued_element.XmlMessagePlay.MsgContent;

          current_client = (SecureTcpClient)PlayElement.Wc2Client;
          current_play = (WC2_MsgPlay)PlayElement.XmlMessagePlay.MsgContent;

          if (current_client.InternalId == enqueued_client.InternalId)
          {
            // Play already exist in queue: Enqueue the play dequeued !!
            wc2_single_play_waitable_queue.Enqueue(PlayElement);

            // RETURN DELAYED
            return true;
          }
          else
          {
            // Enqueue DRAW
            draw_task_element = new DrawTaskElement(PlayElement.Wc2Listener, PlayElement.Wc2Client, PlayElement.XmlMessagePlay,
                                                    enqueued_element.Wc2Listener, enqueued_element.Wc2Client, enqueued_element.XmlMessagePlay);

            wc2_single_draw_waitable_queue.Enqueue(draw_task_element);

            // NOT RETURN DELAYED
            return false;
          }
        }
      } // lock Queue

    }

    /// <summary>
    /// Cancels a play from the queue list
    /// </summary>
    /// <param name="TerminalId">The terminal that issues the cancel play</param>
    /// <param name="PlayElement">The play data</param>
    /// <returns>True if cancel is succesful, False otherwise</returns>
    public Boolean CancelPlay(Int32 TerminalId, PlayQueueElement PlayElement)
    {
      WC2_MsgCancelPlay cancel_play;
      WC2_MsgPlay enqueued_play;
      PlayQueueElement enqueued_element;
      SecureTcpClient enqueued_client;

      cancel_play = (WC2_MsgCancelPlay)PlayElement.XmlMessagePlay.MsgContent;

      lock (wc2_single_play_waitable_queue)
      {
        if (wc2_single_play_waitable_queue.Count == 0)
        {
          return false;
        }

        enqueued_element = (PlayQueueElement)wc2_single_play_waitable_queue.Dequeue();
        enqueued_client = (SecureTcpClient)enqueued_element.Wc2Client;
        enqueued_play = (WC2_MsgPlay)enqueued_element.XmlMessagePlay.MsgContent;

        if (TerminalId == enqueued_client.InternalId && cancel_play.TransactionId == enqueued_play.TransactionId)
        {
          return true;
        }
        else
        {
          wc2_single_play_waitable_queue.Enqueue(enqueued_element);

          return false;
        }
      }
    }

    /// <summary>
    /// Process a draw of two enqued elements
    /// </summary>
    /// <param name="Wc2Listener1"></param>
    /// <param name="Wc2Client1"></param>
    /// <param name="XmlMessagePlay1"></param>
    /// <param name="Wc2Listener2"></param>
    /// <param name="Wc2Client2"></param>
    /// <param name="XmlMessagePlay2"></param>
    public static void DrawTwoPlays(Object Wc2Listener1, Object Wc2Client1, WC2_Message XmlMessagePlay1,
                                    Object Wc2Listener2, Object Wc2Client2, WC2_Message XmlMessagePlay2)
    {
      WC2_Message wc2_request_1;
      WC2_Message wc2_response_1;
      SecureTcpClient client_1;
      WC2_Message wc2_request_2;
      WC2_Message wc2_response_2;
      SecureTcpClient client_2;
      SqlConnection sql_conn;
      SqlTransaction sql_trx;
      String str_response_1;
      String str_response_2;
      // ACC 27-MAY-2010 Trx MsgPlay revised and improved.
      ////Int64 wc2_message_id;
      Boolean error;
      Boolean draw_found;

      WC2_Draw draw;
      DateTime draw_date;
      Int64 drawid;
      ArrayList prizes_1;
      ArrayList prizes_2;

      WC2_MsgPlay request_play_1;
      WC2_MsgPlay request_play_2;
      WC2_MsgPlayReply response_play_1;
      WC2_MsgPlayReply response_play_2;
      Decimal jackpot_contribution;
      Int64 play_id_1;
      Int64 play_id_2;
      Int64 jackpot_id;
      Int32 jackpot_index;
      String jackpot_name;
      Decimal jackpot_amount;

      wc2_request_1 = XmlMessagePlay1;
      wc2_request_2 = XmlMessagePlay2;

      client_1 = (SecureTcpClient)Wc2Client1;
      client_2 = (SecureTcpClient)Wc2Client2;

      sql_conn = null;
      sql_trx = null;
      error = true;
      wc2_response_1 = null;
      wc2_response_2 = null;

      try
      {
        // Play 1
        wc2_response_1 = WC2_Message.CreateMessage(WC2_MsgTypes.WC2_MsgPlayReply);
        wc2_response_1.MsgHeader.PrepareReply(wc2_request_1.MsgHeader);
        request_play_1 = (WC2_MsgPlay)wc2_request_1.MsgContent;
        response_play_1 = (WC2_MsgPlayReply)wc2_response_1.MsgContent;

        // Play 2
        wc2_response_2 = WC2_Message.CreateMessage(WC2_MsgTypes.WC2_MsgPlayReply);
        wc2_response_2.MsgHeader.PrepareReply(wc2_request_2.MsgHeader);
        request_play_2 = (WC2_MsgPlay)wc2_request_2.MsgContent;
        response_play_2 = (WC2_MsgPlayReply)wc2_response_2.MsgContent;

        // Get Sql Connection 
        sql_conn = WGDB.Connection();

        // Begin Transaction
        sql_trx = sql_conn.BeginTransaction();

        draw = null;
        draw_date = DateTime.Now;
        prizes_1 = null;
        prizes_2 = null;

        draw_found = false;

        while (!draw_found)
        {
          if (WC2_DrawManager.ProcessDrawTwoPlays(request_play_1.Cards
                                                  , request_play_2.Cards
                                                  , request_play_1.WonCredits
                                                  , request_play_2.WonCredits
                                                  , request_play_1.TotalPlayedCredits
                                                  , request_play_2.TotalPlayedCredits
                                                  , out draw
                                                  , out draw_date
                                                  , out prizes_1
                                                  , out prizes_2))
          {
            draw_found = true;
          }
          else
          {
            //
            // Change Card of the play who has a greather prize
            //

            // PROVISIONAL LOG
            Log.Message("CHANGE CARD !!");

            if (((double)request_play_1.WonCredits / request_play_1.TotalPlayedCredits) < ((double)request_play_2.WonCredits / request_play_2.TotalPlayedCredits))
            {
              request_play_1.Cards = WC2_BusinessLogic.DB_WC2_GetCards(client_1.InternalId, request_play_1.NumCards, "Card5x5", sql_trx);
            }
            else
            {
              request_play_2.Cards = WC2_BusinessLogic.DB_WC2_GetCards(client_2.InternalId, request_play_2.NumCards, "Card5x5", sql_trx);
            }
          }
        }

        // ACC 27-MAY-2010 Trx MsgPlay revised and improved.
        wc2_pending_plays.m_draw_id++;
        ////drawid = 0;
        ////WC2_BusinessLogic.DB_InsertDrawAudit(draw, draw_date, out drawid, sql_trx);

        response_play_1.NumCards = request_play_1.NumCards;
        response_play_1.Cards = request_play_1.Cards;
        response_play_1.TransactionId = request_play_1.TransactionId;
        response_play_1.Draw = draw;
        response_play_1.DrawDatetime = draw_date;
        response_play_1.NumPrizes = prizes_1.Count;
        response_play_1.Prizes = prizes_1;
        response_play_1.DrawId = wc2_pending_plays.m_draw_id;
        response_play_1.TotalWonCredits = request_play_1.WonCredits;


        response_play_2.NumCards = request_play_1.NumCards;
        response_play_2.Cards = request_play_2.Cards;
        response_play_2.TransactionId = request_play_2.TransactionId;
        response_play_2.Draw = draw;
        response_play_2.DrawDatetime = draw_date;
        response_play_2.NumPrizes = prizes_2.Count;
        response_play_2.Prizes = prizes_2;
        response_play_2.DrawId = wc2_pending_plays.m_draw_id;
        response_play_2.TotalWonCredits = request_play_2.WonCredits;

        // ACC 27-MAY-2010 Trx MsgPlay revised and improved.
        ////WC2_BusinessLogic.DB_InsertPlayAudit(client_1.InternalId, request_play_1, response_play_1, drawid, out play_id_1, sql_trx);
        ////WC2_BusinessLogic.DB_InsertPlayAudit(client_2.InternalId, request_play_2, response_play_2, drawid, out play_id_2, sql_trx);

        //This only depends on the time of the day.
        if (WC2_JackpotManager.IsTimeToCheckJackpot())
        {
          Decimal bet_amount_1;
          Decimal bet_amount_2;

          bet_amount_1 = request_play_1.Denomination;
          bet_amount_1 /= 100;
          bet_amount_1 *= request_play_1.TotalPlayedCredits;

          bet_amount_2 = request_play_2.Denomination;
          bet_amount_2 /= 100;
          bet_amount_2 *= request_play_2.TotalPlayedCredits;

          if (WC2_JackpotManager.CheckJackpot(bet_amount_1,
                                               0,
                                               client_1.InternalId,
                                               request_play_1.GameId,
                                               out jackpot_id,
                                               out jackpot_index,
                                               out jackpot_name,
                                               out jackpot_amount,
                                               sql_trx))
          {
            response_play_1.HasJackpotPrize = true;
            response_play_1.JackpotData.Index = jackpot_index;
            response_play_1.JackpotData.Name = jackpot_name;
            response_play_1.JackpotData.Amount = jackpot_amount;

            // ACC 27-MAY-2010 Trx MsgPlay revised and improved.
            WC2_BusinessLogic.DB_InsertDrawAudit(draw, draw_date, out drawid, sql_trx);
            WC2_BusinessLogic.DB_InsertPlayAudit(client_1.InternalId, request_play_1, response_play_1, drawid, out play_id_1, sql_trx);
            WC2_JackpotManager.UpdateJackpotHistory(jackpot_id, play_id_1, sql_trx);
          }
          else if (WC2_JackpotManager.CheckJackpot(bet_amount_2,
                                               0,
                                               client_2.InternalId,
                                               request_play_2.GameId,
                                               out jackpot_id,
                                               out jackpot_index,
                                               out jackpot_name,
                                               out jackpot_amount,
                                               sql_trx))
          {
            response_play_2.HasJackpotPrize = true;
            response_play_2.JackpotData.Index = jackpot_index;
            response_play_2.JackpotData.Name = jackpot_name;
            response_play_2.JackpotData.Amount = jackpot_amount;

            // ACC 27-MAY-2010 Trx MsgPlay revised and improved.
            WC2_BusinessLogic.DB_InsertDrawAudit(draw, draw_date, out drawid, sql_trx);
            WC2_BusinessLogic.DB_InsertPlayAudit(client_2.InternalId, request_play_2, response_play_2, drawid, out play_id_2, sql_trx);
            WC2_JackpotManager.UpdateJackpotHistory(jackpot_id, play_id_2, sql_trx);
          }
        }

        str_response_1 = wc2_response_1.ToXml();
        str_response_2 = wc2_response_2.ToXml();

        // ACC 27-MAY-2010 Trx MsgPlay revised and improved.
        //////// Play1: Insert wc2 message and Update wc2 trx
        //////WC2_BusinessLogic.DB_InsertWC2Message(client_1.InternalId, wc2_request_1.MsgHeader.TerminalSessionId, true, str_response_1, wc2_response_1.MsgHeader.SequenceId, out wc2_message_id, sql_trx);
        //////WC2_BusinessLogic.DB_UpdateWC2Transaction(client_1.InternalId, wc2_request_1.MsgHeader.SequenceId, true, wc2_message_id, sql_trx);

        // ACC 27-MAY-2010 Trx MsgPlay revised and improved.
        //////// Play2: Insert wc2 message and Update wc2 trx
        //////WC2_BusinessLogic.DB_InsertWC2Message(client_2.InternalId, wc2_request_2.MsgHeader.TerminalSessionId, true, str_response_2, wc2_response_2.MsgHeader.SequenceId, out wc2_message_id, sql_trx);
        //////WC2_BusinessLogic.DB_UpdateWC2Transaction(client_2.InternalId, wc2_request_2.MsgHeader.SequenceId, true, wc2_message_id, sql_trx);

        // Sets the jackpot contribution 
        jackpot_contribution = WC2_JackpotManager.PlayJackpotContribution(request_play_1);
        jackpot_contribution += WC2_JackpotManager.PlayJackpotContribution(request_play_2);
        ////WC2_BusinessLogic.DB_WC2_SetJackpotContribution(jackpot_contribution, sql_trx);
        WC2_JackpotManager.AddJackpotContribution(jackpot_contribution);

        // End Transaction
        sql_trx.Commit();

        // Send Play1
        client_1.Send(str_response_1);
        NetLog.Add(NetEvent.MessageSent, client_1.Identity, str_response_1);

        // Send Play2
        client_2.Send(str_response_2);
        NetLog.Add(NetEvent.MessageSent, client_2.Identity, str_response_2);

        error = false;
      }
      catch (WC2_Exception wc2_ex)
      {
        String wc2_error_response;
        wc2_error_response = null;
        try
        {
          if (wc2_response_1 == null)
          {
            wc2_response_1 = WC2_Message.CreateMessage(WC2_MsgTypes.WC2_MsgError);
          }
          wc2_response_1.MsgHeader.ResponseCode = wc2_ex.ResponseCode;
          wc2_response_1.MsgHeader.ResponseCodeText = wc2_ex.ResponseCodeText;

          // Create error response
          wc2_error_response = wc2_response_1.ToXml();

          client_1.Send(wc2_error_response);
          NetLog.Add(NetEvent.MessageSent, client_1.Identity, wc2_error_response);

          wc2_error_response = null;
          if (wc2_response_2 == null)
          {
            wc2_response_2 = WC2_Message.CreateMessage(WC2_MsgTypes.WC2_MsgError);
          }
          wc2_response_2.MsgHeader.ResponseCode = wc2_ex.ResponseCode;
          wc2_response_2.MsgHeader.ResponseCodeText = wc2_ex.ResponseCodeText;

          // Create error response
          wc2_error_response = wc2_response_2.ToXml();

          client_2.Send(wc2_error_response);
          NetLog.Add(NetEvent.MessageSent, client_2.Identity, wc2_error_response);
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      }
      catch (Exception ex)
      {
        NetLog.Add(NetEvent.MessageFormatError, client_1.Identity + " a.1)", ex.Message);
        NetLog.Add(NetEvent.MessageFormatError, client_1.Identity + " b.1)", XmlMessagePlay1.ToXml());
        NetLog.Add(NetEvent.MessageFormatError, client_2.Identity + " a.1)", ex.Message);
        NetLog.Add(NetEvent.MessageFormatError, client_2.Identity + " b.1)", XmlMessagePlay2.ToXml());

        Log.Exception(ex);

        return;
      }
      finally
      {
        if (sql_trx != null)
        {
          if (error && sql_trx.Connection != null)
          {
            sql_trx.Rollback();
          }

          sql_trx.Dispose();
          sql_trx = null;
        }

        // Close connection
        if (sql_conn != null)
        {
          sql_conn.Close();
          sql_conn = null;
        }
        if (!error)
        {
          //To determine if the frequency of plays is big enough to change the draw type to only one play
          wc2_pending_plays.Draw2PlayerExecuted();
        }
      } // try - finally

    } // DrawTwoPlays

    /// <summary>
    /// Process a draw of one enqued element
    /// </summary>
    /// <param name="Wc2Listener1"></param>
    /// <param name="Wc2Client1"></param>
    /// <param name="XmlMessagePlay1"></param>
    public static void DrawOnePlay(Object Wc2Listener1, Object Wc2Client1, WC2_Message XmlMessagePlay1)
    {
      WC2_Message wc2_request_1;
      WC2_Message wc2_response_1;
      SecureTcpClient client_1;
      SqlConnection sql_conn;
      SqlTransaction sql_trx;
      String str_response_1;
      ////Int64 wc2_message_id;
      Boolean error;
      Boolean draw_found;

      WC2_Draw draw;
      DateTime draw_date;
      Int64 drawid;
      ArrayList prizes_1;

      WC2_MsgPlay request_play_1;
      WC2_MsgPlayReply response_play_1;
      Decimal jackpot_contribution;
      Int64 play_id_1;
      Int64 jackpot_id;
      Int32 jackpot_index;
      String jackpot_name;
      Decimal jackpot_amount;

      wc2_request_1 = XmlMessagePlay1;

      client_1 = (SecureTcpClient)Wc2Client1;

      sql_conn = null;
      sql_trx = null;
      error = true;
      wc2_response_1 = null;

      try
      {
        // Play 1
        wc2_response_1 = WC2_Message.CreateMessage(WC2_MsgTypes.WC2_MsgPlayReply);
        wc2_response_1.MsgHeader.PrepareReply(wc2_request_1.MsgHeader);
        request_play_1 = (WC2_MsgPlay)wc2_request_1.MsgContent;
        response_play_1 = (WC2_MsgPlayReply)wc2_response_1.MsgContent;

        // Get Sql Connection 
        sql_conn = WGDB.Connection();

        // Begin Transaction
        sql_trx = sql_conn.BeginTransaction();

        draw = null;
        draw_date = DateTime.Now;
        prizes_1 = null;

        draw_found = false;

        while (!draw_found)
        {
          if (WC2_DrawManager.ProcessDrawOnePlay(request_play_1.Cards
                                        , request_play_1.WonCredits
                                        , request_play_1.TotalPlayedCredits
                                        , out draw
                                        , out draw_date
                                        , out prizes_1))
          {
            draw_found = true;
          }
          else
          {
            //
            // Change Card of the play who has a greather prize
            //

            // PROVISIONAL LOG
            Log.Message("CHANGE CARD !!");

            request_play_1.Cards = WC2_BusinessLogic.DB_WC2_GetCards(client_1.InternalId, request_play_1.NumCards, "Card5x5", sql_trx);
          }
        }

        // ACC 27-MAY-2010 Trx MsgPlay revised and improved.
        wc2_pending_plays.m_draw_id++;
        ////drawid = 0;
        ////WC2_BusinessLogic.DB_InsertDrawAudit(draw, draw_date, out drawid, sql_trx);

        response_play_1.NumCards = request_play_1.NumCards;
        response_play_1.Cards = request_play_1.Cards;
        response_play_1.TransactionId = request_play_1.TransactionId;
        response_play_1.Draw = draw;
        response_play_1.DrawDatetime = draw_date;
        response_play_1.NumPrizes = prizes_1.Count;
        response_play_1.Prizes = prizes_1;
        response_play_1.DrawId = wc2_pending_plays.m_draw_id;
        response_play_1.TotalWonCredits = request_play_1.WonCredits;

        // ACC 27-MAY-2010 Trx MsgPlay revised and improved.
        ////WC2_BusinessLogic.DB_InsertPlayAudit(client_1.InternalId, request_play_1, response_play_1, drawid, out play_id_1, sql_trx);

        //This only depends on the time of the day.
        if (WC2_JackpotManager.IsTimeToCheckJackpot())
        {
          Decimal bet_amount_1;

          bet_amount_1 = request_play_1.Denomination;
          bet_amount_1 /= 100;
          bet_amount_1 *= request_play_1.TotalPlayedCredits;

          if (WC2_JackpotManager.CheckJackpot(bet_amount_1,
                                               0,
                                               client_1.InternalId,
                                               request_play_1.GameId,
                                               out jackpot_id,
                                               out jackpot_index,
                                               out jackpot_name,
                                               out jackpot_amount,
                                               sql_trx))
          {
            response_play_1.HasJackpotPrize = true;
            response_play_1.JackpotData.Index = jackpot_index;
            response_play_1.JackpotData.Name = jackpot_name;
            response_play_1.JackpotData.Amount = jackpot_amount;

            // ACC 27-MAY-2010 Trx MsgPlay revised and improved.
            WC2_BusinessLogic.DB_InsertDrawAudit(draw, draw_date, out drawid, sql_trx);
            WC2_BusinessLogic.DB_InsertPlayAudit(client_1.InternalId, request_play_1, response_play_1, drawid, out play_id_1, sql_trx);
            WC2_JackpotManager.UpdateJackpotHistory(jackpot_id, play_id_1, sql_trx);
          }
        }

        str_response_1 = wc2_response_1.ToXml();

        // Play1: Insert wc2 message and Update wc2 trx
        // ACC 27-MAY-2010 Trx MsgPlay revised and improved.
        ////WC2_BusinessLogic.DB_InsertWC2Message(client_1.InternalId, wc2_request_1.MsgHeader.TerminalSessionId, true, str_response_1, wc2_response_1.MsgHeader.SequenceId, out wc2_message_id, sql_trx);
        ////WC2_BusinessLogic.DB_UpdateWC2Transaction(client_1.InternalId, wc2_request_1.MsgHeader.SequenceId, true, wc2_message_id, sql_trx);

        // Sets the jackpot contribution 
        jackpot_contribution = WC2_JackpotManager.PlayJackpotContribution(request_play_1);
        ////WC2_BusinessLogic.DB_WC2_SetJackpotContribution(jackpot_contribution, sql_trx);
        WC2_JackpotManager.AddJackpotContribution(jackpot_contribution);

        // End Transaction
        sql_trx.Commit();

        // Send Play1
        client_1.Send(str_response_1);
        NetLog.Add(NetEvent.MessageSent, client_1.Identity, str_response_1);

        error = false;
      }
      catch (WC2_Exception wc2_ex)
      {
        String wc2_error_response;
        wc2_error_response = null;
        try
        {
          if (wc2_response_1 == null)
          {
            wc2_response_1 = WC2_Message.CreateMessage(WC2_MsgTypes.WC2_MsgError);
          }
          wc2_response_1.MsgHeader.ResponseCode = wc2_ex.ResponseCode;
          wc2_response_1.MsgHeader.ResponseCodeText = wc2_ex.ResponseCodeText;

          // Create error response
          wc2_error_response = wc2_response_1.ToXml();

          client_1.Send(wc2_error_response);
          NetLog.Add(NetEvent.MessageSent, client_1.Identity, wc2_error_response);

          wc2_error_response = null;
        }
        catch (Exception ex)
        {
          Log.Exception(ex);
        }
      }
      catch (Exception ex)
      {
        NetLog.Add(NetEvent.MessageFormatError, client_1.Identity + " a.1)", ex.Message);
        NetLog.Add(NetEvent.MessageFormatError, client_1.Identity + " b.1)", XmlMessagePlay1.ToXml());

        Log.Exception(ex);

        return;
      }
      finally
      {
        if (sql_trx != null)
        {
          if (error && sql_trx.Connection != null)
          {
            sql_trx.Rollback();
          }

          sql_trx.Dispose();
          sql_trx = null;
        }

        // Close connection
        if (sql_conn != null)
        {
          sql_conn.Close();
          sql_conn = null;
        }
        if (!error)
        {
          //To determine if the frequency of plays is big enough to change the draw type to only one play
          wc2_pending_plays.Draw1PlayerExecuted();
        }

      } // try - finally

    } // DrawWorkerOnePlay

  } // classs WC2_PendingPlaysQueue
} // namespace WSI.WC2
