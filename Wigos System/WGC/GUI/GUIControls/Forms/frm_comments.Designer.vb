<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_comments
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.tb_comments = New System.Windows.Forms.TextBox
    Me.btn_ok = New System.Windows.Forms.Button
    Me.lbl_comment_reason = New System.Windows.Forms.Label
    Me.btn_cancel = New System.Windows.Forms.Button
    Me.SuspendLayout()
    '
    'tb_comments
    '
    Me.tb_comments.AcceptsReturn = True
    Me.tb_comments.Location = New System.Drawing.Point(8, 50)
    Me.tb_comments.MaxLength = 200
    Me.tb_comments.Multiline = True
    Me.tb_comments.Name = "tb_comments"
    Me.tb_comments.Size = New System.Drawing.Size(345, 114)
    Me.tb_comments.TabIndex = 0
    '
    'btn_ok
    '
    Me.btn_ok.Location = New System.Drawing.Point(195, 170)
    Me.btn_ok.Name = "btn_ok"
    Me.btn_ok.Size = New System.Drawing.Size(75, 23)
    Me.btn_ok.TabIndex = 1
    Me.btn_ok.Text = "XOK"
    Me.btn_ok.UseVisualStyleBackColor = True
    '
    'lbl_comment_reason
    '
    Me.lbl_comment_reason.AutoSize = True
    Me.lbl_comment_reason.Location = New System.Drawing.Point(8, 18)
    Me.lbl_comment_reason.Name = "lbl_comment_reason"
    Me.lbl_comment_reason.Size = New System.Drawing.Size(114, 13)
    Me.lbl_comment_reason.TabIndex = 3
    Me.lbl_comment_reason.Text = "XComment reason"
    '
    'btn_cancel
    '
    Me.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_cancel.Location = New System.Drawing.Point(278, 170)
    Me.btn_cancel.Name = "btn_cancel"
    Me.btn_cancel.Size = New System.Drawing.Size(75, 23)
    Me.btn_cancel.TabIndex = 2
    Me.btn_cancel.Text = "xCancel"
    Me.btn_cancel.UseVisualStyleBackColor = True
    '
    'frm_comments
    '
    Me.AcceptButton = Me.btn_ok
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.btn_cancel
    Me.ClientSize = New System.Drawing.Size(361, 199)
    Me.ControlBox = False
    Me.Controls.Add(Me.btn_cancel)
    Me.Controls.Add(Me.tb_comments)
    Me.Controls.Add(Me.lbl_comment_reason)
    Me.Controls.Add(Me.btn_ok)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_comments"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_comments"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents tb_comments As System.Windows.Forms.TextBox
  Friend WithEvents btn_ok As System.Windows.Forms.Button
  Friend WithEvents lbl_comment_reason As System.Windows.Forms.Label
  Friend WithEvents btn_cancel As System.Windows.Forms.Button
End Class
