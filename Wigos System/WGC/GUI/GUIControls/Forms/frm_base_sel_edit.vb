'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_base_sel
' DESCRIPTION:   Selection and edition base form
' AUTHOR:        Andreu Juli�
' CREATION DATE: 15-Nov-2002
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 15-Nov-2002 AJQ    First releaase (simple copy of frm_base_sel).
' 08-JUN-2012 JAB    Check if disposed.
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls

'XVV -> Add NameSpace Windows.Forms,solve error with IWin32Window variables definitions
Imports System.Windows.Forms

Public Class frm_base_sel_edit
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'pn_line
    '
    Me.pn_line.Location = New System.Drawing.Point(0, 9)
    Me.pn_line.Size = New System.Drawing.Size(672, 5)
    Me.pn_line.Visible = True
    '
    'panel_filter
    '
    Me.panel_filter.Size = New System.Drawing.Size(678, 92)
    Me.panel_filter.Visible = True
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 96)
    Me.panel_data.Size = New System.Drawing.Size(678, 321)
    Me.panel_data.TabIndex = 0
    Me.panel_data.Visible = True
    '
    'pn_separator_line
    '
    Me.pn_separator_line.DockPadding.Bottom = 9
    Me.pn_separator_line.DockPadding.Top = 9
    Me.pn_separator_line.Size = New System.Drawing.Size(672, 27)
    Me.pn_separator_line.TabIndex = 2
    Me.pn_separator_line.Visible = True
    '
    'frm_base_sel_edit
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(686, 421)
    Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.panel_data, Me.panel_filter})
    Me.DockPadding.All = 4
    Me.Name = "frm_base_sel_edit"
    Me.Text = "frm_base_sel_edit"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Members "
  Private Const MAIN_MODULE_NAME As String = "frm_base_sel_edit"
  Protected DbEditedObject As CLASS_BASE
  Protected DbReadObject As CLASS_BASE
  Private FunctionName As String
  Private WithEvents m_grid As uc_grid
#End Region 'Members

#Region " Overrides "

  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    Me.m_grid = Me.Grid

    Me.CancelButton = Nothing

    'btn_custom_0 is used as the 'Apply/Save' button
    Me.btn_custom_0.Text = GLB_NLS_GUI_CONTROLS.GetString(13)

    GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    'GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0.BUTTON_SELECT).Enabled = Me.Permissions.Write
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False

    'XVV 17/04/2007
    'Change ENUM_BUTTON.BUTTON_NEW.BUTTON_NEW for ENUM_BUTTON.BUTTON_NEW
    GUI_Button(ENUM_BUTTON.BUTTON_NEW).Enabled = Me.Permissions.Write


    If Me.btn_select.Visible Then
      Me.btn_select.Enabled = False
    End If

    If Me.btn_info.Visible Then
      Me.btn_info.Enabled = False
    End If

    ' LRS 10-MAR-2004, Only Selection Screens can sort the grid.
    Me.Grid.Sortable = False

  End Sub 'GUI_InitControls


  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        'Change control: if there are changes then request confirmation
        If Gui_ScreenDataChanged() = True Then
          If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(122), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
            Exit Sub
          End If
        End If

        Call MyBase.GUI_ButtonClick(ButtonId)

        ' JAB 08-JUN-2012: Check if disposed.
        If Me.IsDisposed Then
          Exit Sub
        End If

        If (Me.Grid.NumRows > 0) Then
          Me.btn_custom_0.Enabled = Me.Permissions.Write
        End If

        'Case ENUM_BUTTON.BUTTON_FILTER_RESET
        '  'Change control: if there are changes then request confirmation
        '  If Gui_ScreenDataChanged() = True Then
        '    If NLS_MsgBox(GLB_NLS.Id(127), _
        '                  ENUM_MB_TYPE.MB_TYPE_WARNING, _
        '                  ENUM_MB_BTN.MB_BTN_YES_NO, _
        '                  ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
        '      Exit Sub
        '    End If
        '  End If

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call CheckApplyChanges()
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)

    End Select


  End Sub 'GUI_ButtonClick

#End Region 'Overrides

#Region " Must Override "

  Protected Overridable Function Gui_ScreenDataChanged() As Boolean
    Return False
  End Function 'IsScreenDataChanged

  Protected Overridable Sub Gui_GetScreenData()
    DbReadObject = Nothing
    DbEditedObject = Nothing
  End Sub 'Gui_GetScreenData

  Protected Overridable Function Gui_IsScreenDataOk() As Boolean
    Return True
  End Function 'Gui_IsScreenDataOk

  Protected Overridable Sub Gui_CommitChanges()
  End Sub 'Gui_CommitChanges

  Protected Overridable Sub GUI_BeforeStartEditionEvent(ByVal Row As Integer, ByVal Column As Integer, ByRef EditionCanceled As Boolean)

  End Sub ' GUI_BeforeStartEditionEvent

  Protected Overridable Sub GUI_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer)

  End Sub ' GUI_CellDataChangedEvent

#End Region 'Must Override

#Region " Private Functions "

  'Private Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

  '  'If Me.Grid.ContainsFocus Then
  '  '  Return Me.Grid.KeyPressed(sender, e)
  '  'End If

  '  Return True

  'End Function

  Protected Overridable Sub GUI_UpdateError()

  End Sub ' GUI_UpdateError

  Protected Overridable Sub GUI_NoDataToUpdate()

  End Sub ' GUI_NoDataToUpdate

  Private Sub CheckApplyChanges()
    Dim rc As Integer
    FunctionName = "CheckApplyChanges"
    Try
      ' Restore cursor

      'XVV 16/04/2007
      'Change Me.Current.Cursor for Windows.Forms.Cursor.Current because compiler generate a warning
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      If Gui_IsScreenDataOk() Then
        If Gui_ScreenDataChanged() Then
          Call Gui_GetScreenData()
          If Not IsNothing(DbEditedObject) Then
            Dim ctx As Integer

            Try
              'XVV 17/04/2007
              'Comment this function, because are obsolete (Oracle version)
              'GUI_SqlCtxAlloc(ctx)

              rc = DbEditedObject.DB_Update(ctx)

              If rc = ENUM_STATUS.STATUS_OK Then
                Call DbEditedObject.AuditorData.Notify(CurrentUser.GuiId, _
                                                      CurrentUser.Id, _
                                                      CurrentUser.Name, _
                                                      CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                                      ctx, _
                                                      DbReadObject.AuditorData)
                Call Gui_CommitChanges()

              Else
                Call GUI_UpdateError()
              End If

            Finally

              'XVV 17/04/2007
              'Comment this function, because are obsolete (Oracle version)
              'GUI_SqlCtxRelease(ctx)

            End Try
          End If
        Else
          Call GUI_NoDataToUpdate()
        End If

      End If

    Catch exception As Exception

      'XVV 16/04/2007
      'Change Me.Current.Cursor for Windows.Forms.Cursor.Current because compiler generate a warning
      Windows.Forms.Cursor.Current = Cursors.Default

      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            MAIN_MODULE_NAME, _
                            FunctionName, _
                            "", _
                            "", _
                            "", _
                            exception.Message)
    End Try

    ' Restore cursor
    'XVV 16/04/2007
    'Change Me.Current.Cursor for Windows.Forms.Cursor.Current because compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub 'CheckApplyChanges

  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)

    'Change control: if there are changes then request confirmation
    If Gui_ScreenDataChanged() = True Then
      If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), _
                    ENUM_MB_TYPE.MB_TYPE_WARNING, _
                    ENUM_MB_BTN.MB_BTN_YES_NO, _
                    ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
        CloseCanceled = True
      Else
        CloseCanceled = False
      End If
    End If

  End Sub ' GUI_Closing

  Private Sub m_grid_BeforeStartEditionEvent(ByVal Row As Integer, ByVal Column As Integer, ByRef EditionCanceled As Boolean) Handles m_grid.BeforeStartEditionEvent

    Call GUI_BeforeStartEditionEvent(Row, Column, EditionCanceled)

  End Sub ' m_grid_BeforeStartEditionEvent

  Private Sub m_grid_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles m_grid.CellDataChangedEvent

    Call GUI_CellDataChangedEvent(Row, Column)

  End Sub ' m_grid_CellDataChangedEvent

  'Private Sub frm_base_sel_edit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
  '  If Not GUI_KeyPress(sender, e) Then
  '    Return
  '  End If

  '  Select Case e.KeyChar
  '    Case Chr(Keys.Enter)
  '      Call GUI_ButtonClick(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT)

  '    Case Chr(Keys.Escape)
  '      Call GUI_ButtonClick(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL)

  '    Case Else
  '  End Select

  'End Sub

#End Region 'Private Functions

End Class
