'-------------------------------------------------------------------
' Copyright � 2002-2008 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_base.vb  
'
' DESCRIPTION:   form base to all forms
'
' AUTHOR:        Toni Jord�
'
' CREATION DATE: 02-APR-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 02-APR-2002  TJ     Initial version
' 29-MAR-2012  MPO    User session: Set the last action OnLoad and OnUnload
' 08-JUN-2012  JAB    Add DoEvents() function.
' 28-JUN-2013  JCA    Add GetNumHeadersBase() to Generate Excel in forms
' 28-JUN-2013  DMR    Add GetFixedAlignment() to Generate Excel in forms
' 23-AUG-2013  CCG & RCI    Removed unneeded GetNumHeadersBase(), GetFixedAlignment() and GetCellColorBase()
' 14-OCT-2013  ANG    Fix Bug WIG-64 Add RightDbVersion() function.
' 15-NOV-2013  LJM    Changes for auditing forms
' 03-DEC-2013  LJM    Created a function to fill a TabControl with Tabs wich contains Grids
' 12-DEC-2013  LJM    Fixed Bug #WIG-480 Wrong message on Audit forms.
' 13-MAR-2014  AMF & RCI    Fixed Bug WIG-724: Unhandled exception when closing the window during a search
' 11-JUL-2014  DCS    In function CreateDatagridinTabControl disable title label if the parameter TitleText is empty
' 22-SEP-2014  RRR    Added new value (BUTTON_CUSTOM_5) to ENUM_BUTTON for cage report
' 20-JAN-2015  FJC    Set features form (size and autoscroll panels) when size's form exceeds desktop area
' 25-APR-2017  EOR    Bug 26744: Exceptions WigosGUI. When not having connection with the DB server
' 18-SEP-2017  DPC    WIGOS-5268: Cashier & GUI - Icons - Implement
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations

'XVV -> Add NameSpace Windows.Forms,solve error with IWin32Window variables definitions
Imports System.Windows.Forms
Imports System.Drawing
Imports System.IO

Public Class frm_base
  Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

  Public Sub New()
    Call MyBase.New()

    'This call is required by the Windows Form Designer.
    Call InitializeComponent()

    'Add any initialization after the InitializeComponent() call
    Call GUI_SetFormId()
    'Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
  End Sub

  Public Sub GUI_SetMinDbVersion(ByVal MinVersion As Integer)
    m_min_db_version = MinVersion
  End Sub

  Public Overridable Sub GUI_SetFormId()

    Me.FormId = ENUM_COMMON_FORM.COMMON_FORM_BASE

    '----------------------------------------------------------
    'XVV 13/04/2007
    'Assign Icon to active form 
    'OLD version, use GetExecutingAssembly, but in Class libary is necessary GetEntreAssembly to recover ICON Resource
    If DesignMode = False Then Exit Sub
    'Me.Icon = New Icon(System.Reflection.Assembly.GetEntryAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Private WithEvents tmr_permissions As System.Windows.Forms.Timer
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.tmr_permissions = New System.Windows.Forms.Timer(Me.components)
    '
    'frm_base
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(152, 101)
    Me.DockPadding.All = 4
    Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Name = "frm_base"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "frm_base"

  End Sub

#End Region

#Region "Constants"
  Private Const MAX_NUMBER_CHARS_FOR_AUDIT As Integer = 150
#End Region

#Region " Variables "

  Private m_form_id As Integer = ENUM_COMMON_FORM.COMMON_FORM_UNKNOWN
  Protected m_min_db_version As Integer = Integer.MinValue
  Private m_form_title As String = ""
  Private m_permissions_set As Boolean = False
  Private m_permissions As CLASS_GUI_USER.TYPE_PERMISSIONS

  Private m_initialized As Boolean = False
  Private m_loaded As Boolean = False
  Private m_unload As Boolean = False

  Private m_ignore_row_height_filter_in_excel_report As Boolean = False
  Private m_close_registered As Boolean = False

  'Private m_using_gui_display As Boolean = False

  Private Shared m_do_events_last_execution As Integer = WSI.Common.Misc.GetTickCount()

#End Region

#Region " Enumerates "

  Public Enum ENUM_SCREEN_TYPE
    MODE_NONE = 0
    MODE_SELECT = 1
    MODE_EDIT = 2
    MODE_NEW = 3
  End Enum

  Public Enum ENUM_BUTTON
    BUTTON_CANCEL = 0
    BUTTON_FILTER_APPLY = 1
    BUTTON_FILTER_RESET = 2
    BUTTON_INFO = 3
    BUTTON_NEW = 4
    BUTTON_PRINT = 5
    BUTTON_SELECT = 6
    BUTTON_GRID_INFO = 7
    BUTTON_EXCEL = 8
    BUTTON_OK = 9
    BUTTON_DELETE = 10
    BUTTON_CUSTOM_0 = 100
    BUTTON_CUSTOM_1 = 101
    BUTTON_CUSTOM_2 = 102
    BUTTON_CUSTOM_3 = 103
    BUTTON_CUSTOM_4 = 104
    BUTTON_CUSTOM_5 = 105
  End Enum

#End Region

#Region " Properties "

  ' Sets/Gets the FormId
  ' The FormId can only be set 1 time
  '
  Public Property FormId() As Integer
    Get
      Return m_form_id
    End Get
    Set(ByVal Value As Integer)
      If m_form_id = ENUM_COMMON_FORM.COMMON_FORM_UNKNOWN Then
        m_form_id = Value
      End If
    End Set
  End Property

  Public ReadOnly Property Permissions() As CLASS_GUI_USER.TYPE_PERMISSIONS
    Get
      Return m_permissions
    End Get
  End Property

  Public ReadOnly Property FormTitle() As String
    Get
      Return m_form_title
    End Get
  End Property

  Public Property IgnoreRowHeightFilterInExcelReport() As Boolean
    Get
      Return m_ignore_row_height_filter_in_excel_report
    End Get
    Set(ByVal value As Boolean)
      m_ignore_row_height_filter_in_excel_report = value
    End Set
  End Property

#End Region

#Region " Methods "

  'Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
  '  Call MyBase.OnLoad(e)
  '  '
  '  If Me.Permissions.Read = False Then
  '    Call Application.DoEvents()
  '    Call Me.Close()

  '    Return
  '  End If
  'End Sub

  'Protected Overrides Sub OnClosed(ByVal e As System.EventArgs)
  '  Call MyBase.OnClosed(e)
  'End Sub

#End Region

#Region " Overridable Methods "


  Protected Overridable Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)
  End Sub

  Private Sub LoadPermissions()
    Dim gui_perm As CLASS_GUI_USER.TYPE_PERMISSIONS

    If m_permissions_set = True Then
      Exit Sub
    End If

    m_permissions_set = True

    If CurrentUser.IsLogged Then

      ' Set the permission for the CurrentUser
      m_permissions = CurrentUser.Permissions(FormId)

      gui_perm.Read = m_permissions.Read
      gui_perm.Write = m_permissions.Write
      gui_perm.Delete = m_permissions.Delete
      gui_perm.Execute = m_permissions.Execute

      Call GUI_Permissions(gui_perm)
      ' Permissions = Permissions AND CodePerm
      m_permissions.Read = m_permissions.Read And gui_perm.Read
      m_permissions.Write = m_permissions.Write And gui_perm.Write
      m_permissions.Delete = m_permissions.Delete And gui_perm.Delete
      m_permissions.Execute = m_permissions.Execute And gui_perm.Execute
    Else
      ' By default NO PERMISSIONS
      m_permissions.Read = False
      m_permissions.Write = False
      m_permissions.Delete = False
      m_permissions.Execute = False
      ' Get the permissions set by the Programmer
      Call GUI_Permissions(m_permissions)
    End If


  End Sub

  Protected Overridable Sub GUI_InitControls()
    ' Initialize the Controls of the form

    'Call set form features resolutions when size's form exceeds desktop area
    Call Misc.GUI_AutoAdjustResolutionForm(Me, Nothing, Nothing)
  End Sub

  Protected Overridable Sub GUI_FirstActivation()
    ' Start any thread, timer, etc.
  End Sub

  Protected Overridable Sub GUI_SetInitialFocus()
    ' Set the focus
  End Sub

  Protected Overridable Sub GUI_Init()
    ' Read the permissions
    Call LoadPermissions()

  End Sub

  Protected Overridable Sub GUI_Exit()
    ' Perform any clean-up
    ' RCI 02-JUN-2010: Release used memory. Very usefull for cashier configuration form.
    Call Me.Dispose()
  End Sub

  Public Overridable Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    ' If dessired the Close can be Canceled here
  End Sub

  Public Overridable Function GUI_GetScreenType() As ENUM_SCREEN_TYPE
    Return ENUM_SCREEN_TYPE.MODE_NONE
  End Function

  ' PURPOSE: Given a type, the function returns if it needs to be audited
  '
  '  PARAMS:
  '     - INPUT:
  '         - AuditType: AUDIT_FLAGS that wants to be audited
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overridable Function GUI_HasToBeAudited(ByVal AuditType As AUDIT_FLAGS) As Boolean

    ' AMF & RCI 13-MAR-2014: If the Form is disposed, don't audit.
    If Me.IsDisposed Then
      Return False
    End If

    Select Case AuditType
      Case AUDIT_FLAGS.NONE
        Return False
      Case Else
        Return True
    End Select

  End Function
  ' PURPOSE: Given a type, the function returns if it needs to be audited
  '
  '  PARAMS:
  '     - INPUT:
  '         - AuditType: AUDIT_FLAGS that wants to be audited
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overridable Function GUI_HasToBeAudited(ByVal ButtonId As ENUM_BUTTON) As Boolean
    Dim _form_type As AUDIT_FLAGS

    _form_type = GUI_GetAuditFormType(ButtonId)

    Return GUI_HasToBeAudited(_form_type)

  End Function

  ' PURPOSE: Given a buton, the function returns the action to be audited
  '  PARAMS:
  '     - INPUT:
  '         - AuditType: AUDIT_FLAGS that wants to be audited
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overridable Function GUI_GetAuditFormType(ByVal ButtonId As ENUM_BUTTON) As AUDIT_FLAGS

    Dim _form_type As AUDIT_FLAGS

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        _form_type = AUDIT_FLAGS.SEARCH

      Case ENUM_BUTTON.BUTTON_EXCEL
        _form_type = AUDIT_FLAGS.EXCEL

      Case ENUM_BUTTON.BUTTON_PRINT
        _form_type = AUDIT_FLAGS.PRINT

      Case Else
        _form_type = AUDIT_FLAGS.NONE
    End Select

    Return _form_type

  End Function

#End Region

#Region "Private Methods"

  ' PURPOSE: Audits the form, if it has to be audited.
  '
  '  PARAMS:
  '     - INPUT:
  '           - ButtonId As ENUM_BUTTON
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     -
  Protected Sub AuditFormClick(ByVal ButtonId As ENUM_BUTTON, Optional ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA = Nothing)
    Dim _form_type As AUDIT_FLAGS

    _form_type = GUI_GetAuditFormType(ButtonId)

    If GUI_HasToBeAudited(_form_type) Then
      Call AuditForm(_form_type, IIf(String.IsNullOrEmpty(m_form_title), Me.Text, m_form_title), PrintData)
    End If

  End Sub

#End Region

  ' PURPOSE: Called when all forms are closed.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub frm_base_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Closed
    Call GUI_Exit()
    m_initialized = False
    m_loaded = False
  End Sub ' frm_base_Closed

  ' PURPOSE: Called when any form is closing.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub frm_base_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

    Call GUI_Closing(e.Cancel)

  End Sub ' frm_base_Closing


  ' PURPOSE: Base form general initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub Initialize()
    'Dim database_str As String

    If Not Me.DesignMode Then
      If Not m_initialized Then
        m_initialized = True
        ' Process events ...
        Call Application.DoEvents()
        ' Init Controls

        Try 'EOR 25-APR-2017
          Call GUI_InitControls()
        Catch _ex As Exception
          WSI.Common.Log.Exception(_ex)
        End Try

        ' Set form title 
        m_form_title = Me.Text

        ' Set prefix WGC on form title
        Me.Text = GLB_NLS_GUI_CONTROLS.GetString(272) & " " & Me.Text

#If DEBUG Then
        Me.Text = Me.Text & " - [" & Me.Name & "]"
#End If

        If GUI_HasToBeAudited(AUDIT_FLAGS.ACCESS) Then
          Call AuditForm(AUDIT_FLAGS.ACCESS, m_form_title)
        End If

        'If CurrentUser.IsLogged Then
        '  Me.Text = GLB_NLS_GUI_CONTROLS.GetString(272) & " " & Me.Text & " - " & CurrentUser.Name
        'Else
        '  Me.Text = GLB_NLS_GUI_CONTROLS.GetString(272) & " " & Me.Text
        'End If

        ' Wait until all the events have been processed (GUI_InitControls)
        Call Application.DoEvents()
        ' Allow the GUI's to perform an action when the form is activated
        Call GUI_FirstActivation()
        ' Process events ...
        Call Application.DoEvents()
        ' Set Focus the 1st control of the Form
        Call GUI_SetInitialFocus()
        ' Process events ...
        Call Application.DoEvents()
        '
      End If
    End If

    Me.Icon = My.Resources.WigosGUI

  End Sub ' Initialize

  ' PURPOSE: Base form general initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '       - DisplayModal: Flag to set if the screen will be displayed as modal.
  '       - ScreenHidden: Flag to set if form will be created but not displayed.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub Display(ByVal DisplayModal As Boolean, _
                     Optional ByVal ScreenHidden As Boolean = False)
    '
    Call GUI_Init()

    If m_permissions_set = False Then
      m_permissions.Read = False
    End If

    If Not m_permissions.Read Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
      Exit Sub
    End If


    If Not RightDbVersion() Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(163), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, m_min_db_version.ToString("000"))
      Exit Sub

    End If


    If ScreenHidden Then
      Call Initialize()
    Else
      If DisplayModal Then
        Call Me.ShowDialog()
      Else
        Call Me.Show()
      End If
    End If

  End Sub ' Display

  ' PURPOSE: Run Application.DoEvents().
  '
  '  PARAMS:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Function GUI_DoEvents(ByVal Grid As GUI_Controls.uc_grid) As Boolean

    Dim _prev_redraw As Boolean
    Dim _prev_cursor As Windows.Forms.Cursor

    If (WSI.Common.Misc.GetElapsedTicks(m_do_events_last_execution) >= 1000) Then
      _prev_cursor = Windows.Forms.Cursor.Current
      _prev_redraw = Grid.Redraw

      Grid.Redraw = True
      Call Application.DoEvents()
      If Me.IsDisposed Then
        Return False
      End If

      Windows.Forms.Cursor.Current = _prev_cursor
      Grid.Redraw = _prev_redraw

      m_do_events_last_execution = WSI.Common.Misc.GetTickCount()
    End If

    Return True
  End Function ' GUI_DoEvents

  '------------------------------------------------------------------------------
  ' PURPOSE : Create TabPage and Datagrids in Tabcontrol.
  '
  '  PARAMS :
  '      - INPUT :
  '           Grid Control uc_grid
  '           TabPage 
  '           TabPageText TabPage Text
  '           TabPageName TabPage Name
  '           ValuePanelRightVisible Boolean for view the right panel
  '           Title The Label
  '           TitleText The text to the label
  '
  '      - OUTPUT :
  '
  ' RETURNS :
  '      - 
  '   
  Protected Sub CreateDataGridInTabControl(ByVal Grid As GUI_Controls.uc_grid, _
                                           ByVal TabPageText As String, _
                                           ByVal TabPageName As String, _
                                           ByVal ValuePanelRightVisible As Boolean, _
                                           ByVal TitleText As String, _
                                           ByRef TabControl As TabControl)

    Dim _tab_page As TabPage
    Dim _title As Label

    _tab_page = New TabPage()
    TabControl.TabPages.Add(_tab_page)
    _title = New Label()

    '
    ' Tabpage
    '
    _tab_page.Controls.Add(Grid)
    'DCS 11-07-2014: Disable title label if this parameter is empty
    If Not String.IsNullOrEmpty(TitleText) Then
      _tab_page.Controls.Add(_title)
    End If
    _tab_page.Location = New System.Drawing.Point(4, 22)
    _tab_page.Name = TabPageName
    _tab_page.Size = New System.Drawing.Size(956, 268)
    _tab_page.Text = TabPageText
    _tab_page.UseVisualStyleBackColor = True

    '
    ' Title (label)
    '
    'DCS 11-07-2014: Disable title label if this parameter is empty
    If Not String.IsNullOrEmpty(TitleText) Then
      _title.AutoSize = False
      _title.BackColor = System.Drawing.SystemColors.ActiveCaption
      _title.ForeColor = System.Drawing.SystemColors.ActiveCaptionText

      _title.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
      _title.Anchor = AnchorStyles.Top
      _title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
      _title.Font = New System.Drawing.Font("Verdana", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), _
                                            System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      _title.Dock = System.Windows.Forms.DockStyle.Top
      _title.Location = New System.Drawing.Point(0, 0)
      _title.Name = "lbl_" & TabPageName
      _title.Size = New System.Drawing.Size(440, 25)
      _title.Text = TitleText
    End If
    '
    ' Grid
    '
    Grid.CurrentCol = -1
    Grid.CurrentRow = -1
    Grid.Dock = System.Windows.Forms.DockStyle.Fill
    Grid.Location = New System.Drawing.Point(0, 13)
    Grid.PanelRightVisible = ValuePanelRightVisible
    Grid.Redraw = True
    Grid.Size = New System.Drawing.Size(956, 560)
    Grid.Sortable = False
    Grid.SortAscending = True
    Grid.ToolTipped = True
    Grid.TopRow = -2

  End Sub ' CreateDataGridInTabControl

  ' PURPOSE: Form loading event.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
    Call Initialize()
    Call MyBase.OnLoad(e)

    If Not Me.DesignMode Then
      WSI.Common.Users.SetLastAction("Open", m_form_title)
    End If

  End Sub ' OnLoad

  Protected Overrides Sub OnClosed(ByVal e As System.EventArgs)
    MyBase.OnClosed(e)

    If Not m_close_registered Then
      m_close_registered = True
      If Not Me.DesignMode Then
        WSI.Common.Users.SetLastAction("Close", m_form_title)
      End If
    End If
  End Sub

  ' PURPOSE: Check for right database version.
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - TRUE If database version is equal or higher than expected version.
  '     - FALSE Otherwise
  Protected Function RightDbVersion() As Boolean
    Dim _min_db_version As Integer

    Select Case GLB_GuiMode
      Case ENUM_GUI.CASHDESK_DRAWS_GUI
        _min_db_version = 4

      Case ENUM_GUI.WIGOS_GUI
        _min_db_version = Math.Max(m_min_db_version, 143)

      Case ENUM_GUI.MULTISITE_GUI
        _min_db_version = 30

      Case Else
        _min_db_version = Integer.MinValue
    End Select

    If m_min_db_version <> Integer.MinValue Then
      Dim _ver1 As String
      Dim _ver2() As String
      Dim _version As Integer

      _version = 0

      Try
        _ver1 = WSI.Common.WGDB.GetDatabaseVersion()
        _ver2 = _ver1.Split(".")

        If _ver2.Length >= 2 Then
          If (Not Integer.TryParse(_ver2(1), _version)) Then
            Return False
          End If
        Else
          Return False
        End If

      Catch ex As Exception
        Return False
      End Try

      Return (_version >= _min_db_version)

    End If

    Return True

  End Function

End Class
