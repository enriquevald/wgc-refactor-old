'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_comments.vb
'
' DESCRIPTION : form to insert comments
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 02-JUN-2014  XCD    Initial version.
'-------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Windows.Forms

Public Class frm_comments
  Inherits frm_base

#Region "Variables"
  Dim m_message_can_be_empty As Boolean
  Dim m_message As String
  Dim m_save As Boolean
#End Region

#Region "Properties"

  'Protected Overrides ReadOnly Property CreateParams() As Windows.Forms.CreateParams
  '  Get
  '    Dim param As Windows.Forms.CreateParams = MyBase.CreateParams
  '    param.ClassStyle = param.ClassStyle Or &H200
  '    Return param
  '  End Get
  'End Property

  Public ReadOnly Property Comments()
    Get
      Return m_message
    End Get
  End Property

#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    'Me.FormId = ENUM_FORM.FORM_CARD_RECORD

    'Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Given a type, the function returns if it needs to be audited
  '
  '  PARAMS:
  '     - INPUT:
  '         - AuditType: AUDIT_FLAGS that wants to be audited
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_HasToBeAudited(ByVal AuditType As AUDIT_FLAGS) As Boolean

    Return False

  End Function

#End Region

#Region " Constructor "

  Public Sub New(ByVal Title As String, ByVal LabelString As String, ByVal TextOkButton As String, ByVal TextCancelButton As String, ByVal CanBeEmpty As Boolean)

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    Me.Text = Title
    Me.lbl_comment_reason.Text = LabelString
    Me.btn_ok.Text = TextOkButton

    If String.IsNullOrEmpty(TextCancelButton) Then
      Me.btn_cancel.Visible = False
      Me.btn_cancel.Enabled = False
      Me.btn_ok.Location = Me.btn_cancel.Location
    Else
      Me.btn_cancel.Text = TextCancelButton
    End If

    Me.m_message_can_be_empty = CanBeEmpty
    Me.m_save = False


  End Sub

#End Region

#Region "Public Functions"
  ' PURPOSE: Show dialog if general param is activated
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Dialog Result: OK or Cancel
  '
  Public Function ShowGPDialog() As DialogResult
    If WSI.Common.GeneralParam.GetBoolean("WigosGUI", "AskForChangeReason", True) Then
      Return Me.ShowDialog()
    Else
      Me.m_message = ""
      Me.DialogResult = Windows.Forms.DialogResult.OK
      Me.Close()

      Return Windows.Forms.DialogResult.OK

    End If
  End Function
#End Region

#Region "Events"
  Private Sub btn_ok_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_ok.Click

    If Not m_message_can_be_empty Then
      If tb_comments.Text.Trim.Length = 0 Then
        MsgBox(NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), Me.Text), MsgBoxStyle.Exclamation, "")
        Me.tb_comments.Focus()
        Return
      End If
    End If

    Me.m_message = tb_comments.Text.Trim
    Me.DialogResult = Windows.Forms.DialogResult.OK
    Me.Close()

  End Sub

  Private Sub btn_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancel.Click
    Me.m_message = ""
    Me.DialogResult = Windows.Forms.DialogResult.Cancel
    Me.Close()
  End Sub

#End Region

End Class