'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_base.vb  
'
' DESCRIPTION:   form base to all forms
'
' AUTHOR:        Luis Rodriguez
'
' CREATION DATE: 19-DIC-2003
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 19-DIC-2003  TJ    Initial version
'-------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations

Public Class frm_progress
  Inherits Windows.Forms.Form

  Dim m_opened As Date
  Dim m_count As Integer = 0

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
    m_opened = Now
    m_count = 1
    Me.pgb_bar.Value = Me.pgb_bar.Minimum

    Me.SetBounds((System.Windows.Forms.Screen.GetBounds(Me).Width - Me.Width) / 2, _
                  System.Windows.Forms.Screen.GetBounds(Me).Y, Me.Width, Me.Height)
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
  Friend WithEvents pgb_bar As System.Windows.Forms.ProgressBar
  Private WithEvents tmr_refresh As System.Windows.Forms.Timer
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Me.pgb_bar = New System.Windows.Forms.ProgressBar
    Me.tmr_refresh = New System.Windows.Forms.Timer(Me.components)
    Me.SuspendLayout()
    '
    'pgb_bar
    '
    Me.pgb_bar.Dock = System.Windows.Forms.DockStyle.Fill
    Me.pgb_bar.Location = New System.Drawing.Point(3, 3)
    Me.pgb_bar.Name = "pgb_bar"
    Me.pgb_bar.Size = New System.Drawing.Size(242, 17)
    Me.pgb_bar.TabIndex = 0
    '
    'tmr_refresh
    '
    Me.tmr_refresh.Enabled = True
    Me.tmr_refresh.Interval = 250
    '
    'frm_progress
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(248, 23)
    Me.ControlBox = False
    Me.Controls.Add(Me.pgb_bar)
    Me.DockPadding.All = 3
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_progress"
    Me.ShowInTaskbar = False
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "frm_progress"
    Me.TopMost = True
    Me.ResumeLayout(False)

  End Sub

#End Region

  Private Sub tmr_refresh_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmr_refresh.Tick

    Dim num_jobs As Integer
    'XVV 16/04/2007
    'Assign "" to SQL Trans because compiler generate warning for not have value
    Dim title As String = ""
    Dim started As Date
    Dim elapsed As TimeSpan
    Dim seconds As Integer
    Dim minutes As Integer




    If Me.pgb_bar.Value >= Me.pgb_bar.Maximum Then
      Me.pgb_bar.Value = Me.pgb_bar.Minimum
    End If

    Me.pgb_bar.Increment(1)

    RaiseEvent ProgressStatus(started, num_jobs, title)

    If num_jobs = 0 Then
      Me.Hide()
    Else
      elapsed = Now.Subtract(started)
      If num_jobs > 1 Then
        title = title & " (" & num_jobs & ")"
      End If

      seconds = CInt(elapsed.TotalSeconds) Mod 60
      minutes = (CInt(elapsed.TotalSeconds) - seconds) / 60
      Me.Text = "[" & Format(minutes, "00") & ":" & Format(seconds, "00") & "] " & title

    End If

  End Sub

  Public Event ProgressStatus(ByRef Started As Date, ByRef NumJobs As Integer, ByRef Title As String)



End Class
