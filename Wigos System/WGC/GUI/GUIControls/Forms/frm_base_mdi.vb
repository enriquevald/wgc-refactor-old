'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_base_mdi.vb  
'
' DESCRIPTION:   form base mdi
'
' AUTHOR:        Carlos Costa
'
' CREATION DATE: 03-APR-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-APR-2002  CAC    Initial version
' 12-FEB-2015  FJC    New GUI Menu
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls

'XVV -> Add NameSpace Windows.Forms,solve error with IWin32Window variables definitions
Imports System.Windows.Forms
Imports System.Drawing
'XVV -----------------------------------

Public Class frm_base_mdi
  Inherits GUI_Controls.frm_base

  Public m_mnu As CLASS_MENU

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.SuspendLayout()
    '
    'frm_base_mdi
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(152, 101)
    Me.IsMdiContainer = True
    Me.Name = "frm_base_mdi"
    Me.Padding = New System.Windows.Forms.Padding(2)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Must Override: frm_base_mdi "

  Protected Overrides Sub GUI_InitControls()
    ' Required by the base class
    Call MyBase.GUI_InitControls()

    m_mnu = New CLASS_MENU()
    Me.Controls.Add(m_mnu.Menu)
    Me.MainMenuStrip = m_mnu.Menu
    Call GUI_CreateMenu()

  End Sub

  Protected Overridable Sub GUI_CreateMenu()
  End Sub

#End Region

  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_COMMON_FORM.COMMON_FORM_BASE_MDI

    '----------------------------------------------------------
    'XVV 13/04/2007
    'Assign Icon to active form 
    'OLD version, use GetExecutingAssembly, but in Class libary is necessary GetEntreAssembly to recover ICON Resource
    If Not DesignMode Then Exit Sub
    'Me.Icon = New Icon(System.Reflection.Assembly.GetEntryAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))

    '----------------------------------------------------------

  End Sub

  Protected Overrides Sub GUI_Exit()
    Dim form_list As Form()
    Dim frm_idx As Integer

    form_list = Me.MdiChildren()
    If Not IsNothing(form_list) Then
      For frm_idx = LBound(form_list) To UBound(form_list)
        Call form_list(frm_idx).Close()
      Next
    End If

  End Sub

  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    Dim form_list As Form()
    Dim frm_idx As Integer
    Dim one_form As frm_base

    form_list = Me.MdiChildren()
    If UBound(form_list) >= LBound(form_list) Then
      For frm_idx = LBound(form_list) To UBound(form_list)
        one_form = form_list(frm_idx)
        Call one_form.GUI_Closing(CloseCanceled)
        If CloseCanceled = True Then
          Exit For
        End If
      Next
    End If

  End Sub

End Class
