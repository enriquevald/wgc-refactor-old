'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_print_account_info
'
' DESCRIPTION : Displays a form with a message and a checkbox.
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 30-JUL-2013  FBA    Initial version
' 09-DEC-2013  LJM    Changes to audit the printing
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Windows.Forms

#End Region

Public Class frm_print_account_info
  Inherits frm_base

#Region " Members "
  Dim m_holder_name As String
  Dim m_print As Boolean
  Dim m_include_scanned_documents As Boolean
#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    ' No need to have a FormId.

    'Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Given a type, the function returns if it needs to be audited
  '
  '  PARAMS:
  '     - INPUT:
  '         - AuditType: AUDIT_FLAGS that wants to be audited
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_HasToBeAudited(ByVal AuditType As AUDIT_FLAGS) As Boolean

    Select Case AuditType
      Case AUDIT_FLAGS.ACCESS
        Return False
      Case Else
        Return MyBase.GUI_HasToBeAudited(AuditType)
    End Select

  End Function


  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    Me.btn_cancel.Text = GLB_NLS_GUI_CONTROLS.GetString(443)
    Me.btn_print.Text = GLB_NLS_GUI_CONFIGURATION.GetString(230)
    Me.lbl_message.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2432, m_holder_Name)
    Me.chk_scan_doc.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2433)
    Me.chk_scan_doc.Checked = True
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(775)
    img.Image = System.Drawing.SystemIcons.Information.ToBitmap()

  End Sub 'GUI_InitControls


#End Region

#Region " Public Functions "

  Public Sub New(ByVal HolderName As String)

    m_holder_name = HolderName
    ' This call is required by the Windows Form Designer.
    InitializeComponent()

  End Sub

  Public Overloads Sub Show(ByRef Print As Boolean, ByRef IncludeScannedDocuments As Boolean)
    Me.ShowDialog()
    Print = m_print
    IncludeScannedDocuments = m_include_scanned_documents
  End Sub

#End Region

#Region " Events "

  Private Sub btn_cancel_ClickEvent() Handles btn_cancel.ClickEvent

    Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_CANCEL)

  End Sub

  Private Sub btn_print_ClickEvent() Handles btn_print.ClickEvent

    Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_PRINT)

  End Sub

  Private Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)
    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CANCEL
        m_print = False
        Me.Close()

      Case ENUM_BUTTON.BUTTON_PRINT
        m_print = True
        m_include_scanned_documents = Me.chk_scan_doc.Checked

        'LJM 09-DEC-2013 Direct call to the module, since with the form title is not enough and we need a diferent string
        Call AuditForm(AUDIT_FLAGS.PRINT, FormTitle & ": " & m_holder_name)

        Me.Close()
      Case Else
        '
    End Select
  End Sub

#End Region

End Class
