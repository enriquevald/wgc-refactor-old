<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_images
  Inherits GUI_Controls.frm_base

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.tab_main = New System.Windows.Forms.TabControl
    Me.tab_icon = New System.Windows.Forms.TabPage
    Me.img_icon = New GUI_Controls.uc_image
    Me.tab_image = New System.Windows.Forms.TabPage
    Me.img_image = New GUI_Controls.uc_image
    Me.btn_cancel = New GUI_Controls.uc_button
    Me.btn_accept = New GUI_Controls.uc_button
    Me.tab_main.SuspendLayout()
    Me.tab_icon.SuspendLayout()
    Me.tab_image.SuspendLayout()
    Me.SuspendLayout()
    '
    'tab_main
    '
    Me.tab_main.Controls.Add(Me.tab_icon)
    Me.tab_main.Controls.Add(Me.tab_image)
    Me.tab_main.Location = New System.Drawing.Point(5, 11)
    Me.tab_main.Name = "tab_main"
    Me.tab_main.SelectedIndex = 0
    Me.tab_main.Size = New System.Drawing.Size(340, 329)
    Me.tab_main.TabIndex = 0
    '
    'tab_icon
    '
    Me.tab_icon.Controls.Add(Me.img_icon)
    Me.tab_icon.Location = New System.Drawing.Point(4, 22)
    Me.tab_icon.Name = "tab_icon"
    Me.tab_icon.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_icon.Size = New System.Drawing.Size(332, 303)
    Me.tab_icon.TabIndex = 0
    Me.tab_icon.Text = "xIcon"
    Me.tab_icon.UseVisualStyleBackColor = True
    '
    'img_icon
    '
    Me.img_icon.AutoSize = True
    Me.img_icon.Image = Nothing
    Me.img_icon.ImageLayout = System.Windows.Forms.ImageLayout.Center
    Me.img_icon.Location = New System.Drawing.Point(79, 58)
    Me.img_icon.Margin = New System.Windows.Forms.Padding(0)
    Me.img_icon.Name = "img_icon"
    Me.img_icon.Size = New System.Drawing.Size(160, 176)
    Me.img_icon.TabIndex = 0
    '
    'tab_image
    '
    Me.tab_image.Controls.Add(Me.img_image)
    Me.tab_image.Location = New System.Drawing.Point(4, 22)
    Me.tab_image.Name = "tab_image"
    Me.tab_image.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_image.Size = New System.Drawing.Size(332, 303)
    Me.tab_image.TabIndex = 1
    Me.tab_image.Text = "xImage"
    Me.tab_image.UseVisualStyleBackColor = True
    '
    'img_image
    '
    Me.img_image.AutoSize = True
    Me.img_image.Image = Nothing
    Me.img_image.ImageLayout = System.Windows.Forms.ImageLayout.Center
    Me.img_image.Location = New System.Drawing.Point(7, 3)
    Me.img_image.Margin = New System.Windows.Forms.Padding(0)
    Me.img_image.Name = "img_image"
    Me.img_image.Size = New System.Drawing.Size(321, 297)
    Me.img_image.TabIndex = 1
    '
    'btn_cancel
    '
    Me.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_cancel.Location = New System.Drawing.Point(254, 347)
    Me.btn_cancel.Name = "btn_cancel"
    Me.btn_cancel.Size = New System.Drawing.Size(90, 30)
    Me.btn_cancel.TabIndex = 1
    Me.btn_cancel.ToolTipped = False
    Me.btn_cancel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_accept
    '
    Me.btn_accept.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_accept.Location = New System.Drawing.Point(158, 347)
    Me.btn_accept.Name = "btn_accept"
    Me.btn_accept.Size = New System.Drawing.Size(90, 30)
    Me.btn_accept.TabIndex = 2
    Me.btn_accept.ToolTipped = False
    Me.btn_accept.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'frm_images
    '
    Me.ClientSize = New System.Drawing.Size(350, 385)
    Me.Controls.Add(Me.btn_accept)
    Me.Controls.Add(Me.btn_cancel)
    Me.Controls.Add(Me.tab_main)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_images"
    Me.ShowInTaskbar = False
    Me.tab_main.ResumeLayout(False)
    Me.tab_icon.ResumeLayout(False)
    Me.tab_icon.PerformLayout()
    Me.tab_image.ResumeLayout(False)
    Me.tab_image.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents tab_main As System.Windows.Forms.TabControl
  Friend WithEvents tab_icon As System.Windows.Forms.TabPage
  Friend WithEvents tab_image As System.Windows.Forms.TabPage
  Friend WithEvents img_icon As GUI_Controls.uc_image
  Friend WithEvents img_image As GUI_Controls.uc_image
  Friend WithEvents btn_cancel As GUI_Controls.uc_button
  Friend WithEvents btn_accept As GUI_Controls.uc_button

End Class
