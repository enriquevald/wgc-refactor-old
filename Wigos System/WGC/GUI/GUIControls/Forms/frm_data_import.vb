'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_data_import.vb  
'
' DESCRIPTION:   Data import
'
' AUTHOR:        Adri�n Cuartas
'
' CREATION DATE: 07-OCT-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 08-OCT-2013  ACM     Initial version
' 20-OCT-2015  ECP     Backlog Item 1080 GUI - Improvements to Gaming Table Plays Sessions Import
' 29-JUL-2016  RAB     Product Backlog Item 15066: GamingTables (Phase 4): Adapt reports to MD (MultiCurrency)
' 22-SET-2016  XGJ     Bug Item 17664:No se realiza la importaci�n de puntos a la cuenta de un cliente si el formato de la hoja de c�lculo no es correcto
'-------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region "Imports"

Imports System.Windows.Forms
Imports WSI.Common
Imports SpreadsheetGear

#End Region

Public Class frm_data_import

#Region "Constants"

  Const UC_COMBO_WIDTH As Integer = 230
  Const UC_COMBO_TEXT_WIDTH As Integer = 150

#End Region

#Region "Members"

  Private m_total_columns As Integer
  Private m_start_column As Integer
  Public m_imported_data As DataTable
  Private m_excel_use_current_region As Boolean

#End Region

#Region "Properties"

  Private _excel_columns As Dictionary(Of String, String)
  Public Property ExcelColumns() As Dictionary(Of String, String)
    Get
      Return _excel_columns
    End Get
    Set(ByVal value As Dictionary(Of String, String))
      _excel_columns = value
    End Set
  End Property

  Private _excel_path As String
  Public Property ExcelPath() As String
    Get
      Return _excel_path
    End Get
    Set(ByVal value As String)
      _excel_path = value
    End Set
  End Property

  Public Property ExcelUseCurrentRegion() As Boolean
    Get
      Return m_excel_use_current_region
    End Get
    Set(ByVal value As Boolean)
      m_excel_use_current_region = value
    End Set
  End Property

#End Region

#Region "Public Functions"

  Public Sub New()

    m_imported_data = New DataTable()
    m_excel_use_current_region = True

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    InitControls()

  End Sub

  Public Sub ShowForm()

    'Set excel into workbook control
    LoadWorkbook()

    'Shows fields to import
    LoadCombos()
    
    'ShowForm
    Me.ShowDialog()

  End Sub

#End Region

#Region "Private Functions"
  ' PURPOSE: Own controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(841)
    Me.btn_ok.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4818)
    Me.btn_cancel.Text = GLB_NLS_GUI_CONTROLS.GetString(2)
    Me.lbl_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(842)
    Me.lbl_infomation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7619)

  End Sub ' InitControls


  ' PURPOSE: Set excel file to import into the workbook control and get the region with data
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub LoadWorkbook()

    Dim _excel_range As SpreadsheetGear.IRange = Nothing
    Dim _work_book_set As SpreadsheetGear.IWorkbookSet
    Dim _work_book As SpreadsheetGear.IWorkbook
    Dim _down_addresss As String
    
    Try
      _work_book_set = SpreadsheetGear.Factory.GetWorkbookSet()
      ' Create a new empty workbook in the workbook set. 
      _work_book = _work_book_set.Workbooks.Add()
      ' Open the saved workbook from memory. 
      _work_book = _work_book_set.Workbooks.Open(Me.ExcelPath)

      ' Only the first sheet is for data import. It's just for the control presentation.
      While _work_book.Sheets.Count() > 1
        _work_book.Sheets.Item(1).Delete()
      End While

      ' Get region to import
      _work_book_set.GetLock()

      ' DHA 01-APR-2015 set excel region to process
      If m_excel_use_current_region Then
        _excel_range = _work_book.Worksheets(0).UsedRange.CurrentRegion
      Else
        _excel_range = _work_book.Worksheets(0).UsedRange
      End If

      _down_addresss = _excel_range.GetAddress(False, False, ReferenceStyle.A1, False, Nothing)
      _excel_range = _work_book.Worksheets(0).Range(_down_addresss)

      If IsArray(_excel_range.Value) Then ' More than one cell filled
        m_total_columns = _excel_range.Value.GetLength(1)
      Else
        If Not IsNothing(_excel_range.Value) Then  ' Just one cell filled
          m_total_columns = 1
        End If
      End If

      ExcelConversion.ConvertCells(_work_book.Worksheets(0), _excel_range, Me.ExcelColumns) 'It is formatted according the data type cells
      m_start_column = _excel_range.Column
      _work_book_set.ReleaseLock()

      ' Unable cells to edit
      _work_book.Worksheets(0).ProtectContents = True

      ' Set to workbook control
      wb_excel_imported.ActiveWorkbook = _work_book

    Catch ex As Exception
      Log.Error(String.Format("frm_data_import. Error while reading file: {0}", ex.Message))
    End Try

  End Sub

  ' PURPOSE: Shows how many fields has been indicated to import (Labels)  and which columns has been identified (Combos)
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub LoadCombos()

    Dim _uc_combo As uc_combo
    'Generate columns list with name
    Dim _columns_letters() As String = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}
    Dim _letters As Dictionary(Of Integer, String)
    Dim _column_name As String
    Dim _column_sub_index As Integer
    ' used in two differents loops 
    Dim _int_index As Integer

    _letters = New Dictionary(Of Integer, String)
    _column_name = String.Empty
    _column_sub_index = 0

    ' More fields than columns
    If m_total_columns < Me.ExcelColumns.Count Then
      m_total_columns = Me.ExcelColumns.Count
    End If

    ' Supports for Columns A to ZZ -->  _columns_letters.Length * _columns_letters.Length + _columns_letters.Length
    If (m_start_column + 1) > (_columns_letters.Length * _columns_letters.Length + _columns_letters.Length) Or _
       (m_total_columns + m_start_column) > (_columns_letters.Length * _columns_letters.Length + _columns_letters.Length) Then

      m_start_column = 0
      m_total_columns = 0
    End If

    For _column_index As Integer = m_start_column To ((m_start_column + m_total_columns) - 1)

      _int_index = _column_index \ _columns_letters.Length
      ' If the column has two letters
      If _int_index > 0 Then
        _column_name = _columns_letters(_int_index - 1)
      End If

      _column_sub_index = _column_index - (_columns_letters.Length * _int_index)
      _letters.Add(_column_index, _column_name & _columns_letters(_column_sub_index))
    Next

    ' Combos controls generation
    _int_index = 0
    For Each _column As KeyValuePair(Of String, String) In Me.ExcelColumns
      _uc_combo = New uc_combo
      ' Combos properties
      _uc_combo.Text = _column.Key
      _uc_combo.Name = _column.Key
      _uc_combo.IsReadOnly = False
      _uc_combo.Width = UC_COMBO_WIDTH
      _uc_combo.TextWidth = UC_COMBO_TEXT_WIDTH
      ' Add columns list in Combo
      _uc_combo.Add(_letters)

      ' Select a column letter to the combo
      If _letters.Count > 0 Then
        _uc_combo.SelectedIndex = _int_index
      End If

      ' Add Combo control in FlowLayoutPanel control
      flp_fields_to_import.Controls.Add(_uc_combo)
      _int_index += 1
    Next

  End Sub

#End Region

#Region "Events"

  Private Sub btn_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cancel.Click

    Me.Close()
  End Sub

  Private Sub btn_ok_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_ok.Click
    Try
      Dim _fields_types_values As DataTable
      Dim _uc_combo As uc_combo

      ' DataTable definition
      _fields_types_values = New DataTable
      _fields_types_values.Columns.Add("ColumnName", Type.GetType("System.String")).AllowDBNull = False
      _fields_types_values.Columns.Add("ColumnType", Type.GetType("System.String")).AllowDBNull = False
      _fields_types_values.Columns.Add("ColumnIndex", Type.GetType("System.Int32")).AllowDBNull = False

      For Each _column As KeyValuePair(Of String, String) In Me.ExcelColumns
        ' Get Combo control
        _uc_combo = flp_fields_to_import.Controls(_column.Key)

        ' No selectable columns
        If _uc_combo.SelectedIndex = -1 Then

          Return
        End If

        ' Fill row with the combo selected value. Only adds rows with a value selected in the combo
        _fields_types_values.Rows.Add(_column.Key, _column.Value, _uc_combo.SelectedIndex)
      Next

      ' Import data:
      If _fields_types_values.Rows.Count > 0 Then
        ExcelConversion.ExcelToDataTable(Me.ExcelPath, _fields_types_values, m_excel_use_current_region, m_imported_data)

        'If Not (ModificatedColumnsAccordingToIsoCode()) Then
        '  Return
        'End If
      End If

    Catch ex As Exception

      Log.Exception(ex)
    Finally

      Me.Close()
    End Try

  End Sub

  ''' <summary>
  ''' Modification the columns of the final datatable according to iso code column. Set local currency when is empty
  ''' </summary>
  ''' <returns>true if it has changed, false if there are errors</returns>
  ''' <remarks></remarks>
  Private Function ModificatedColumnsAccordingToIsoCode() As Boolean
    Dim _has_modificated As Boolean
    Dim _iso_code_fieldname As String
    Dim _national_iso_code As String
    Dim _sell_chips_text As String
    Dim _sell_chips_fieldname As String

    _has_modificated = False

    If (m_imported_data.Rows.Count <= 0) Then
      Return _has_modificated
    End If

    _iso_code_fieldname = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5554)
    _national_iso_code = CurrencyExchange.GetNationalCurrency()
    _sell_chips_text = GeneralParam.GetString("GamingTable.PlayerTracking", "PlayerTracking.SellChips.Text", String.Empty)

    'Total Buy-IN
    If String.IsNullOrEmpty(_sell_chips_text) Then
      _sell_chips_fieldname = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5289)
    Else
      _sell_chips_fieldname = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5073, _sell_chips_text)
    End If

    For _idx_row As Integer = 0 To m_imported_data.Rows.Count - 1
      If (IsDBNull(m_imported_data.Rows(_idx_row).Item(_iso_code_fieldname))) Then
        m_imported_data.Rows(_idx_row).Item(_iso_code_fieldname) = _national_iso_code 'Iso code column

        _has_modificated = True
      ElseIf (m_imported_data.Rows(_idx_row).Item(_iso_code_fieldname) <> _national_iso_code) Then
        'Total Buy-IN
        m_imported_data.Rows(_idx_row).Item(_sell_chips_fieldname) = "0"

        _has_modificated = True
      End If
    Next

    Return _has_modificated
  End Function

  Private Sub wb_excel_imported_ShowError(ByVal sender As Object, ByVal e As SpreadsheetGear.Windows.Forms.ShowErrorEventArgs) Handles wb_excel_imported.ShowError

    ' Don't shows the SpreadSheetGear message for edit protection
    If e.Message.ToUpper() = "LOCKED CELLS CANNOT BE MODIFIED WHEN PROTECTION IS ENABLED." Then
      e.Handled = True
    End If
  End Sub

#End Region


End Class