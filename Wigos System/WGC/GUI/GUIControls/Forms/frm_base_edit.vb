'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_base_edit.vb  
'
' DESCRIPTION:   form base to edition forms
'
' AUTHOR:        Andreu Juli�
'
' CREATION DATE: 03-APR-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-APR-2002  AJQ    Initial version
' 12-MAR-2013  ICS    Fixed Bug #637: Now frm_base_edit_KeyPress() checks that ok button is enabled
' 14-OCT-2013  ANG    Fixed Bug WIG-64
' 02-DEC-2013  LJM    Created property CheckChanges(). Required for forms wich don't use the standard ButtonOkClick.
'					  Also functions for Excel and Print cases created.
' 23-APR-2014  JAB    Added button to use in cage session detail form.
' 20-JAN-2015  FJC    Set features form (size and autoscroll panels) when size's form exceeds desktop area
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls

'XVV -> Add NameSpace Windows.Forms,solve error with IWin32Window variables definitions
Imports System.Windows.Forms
Imports System.Drawing


Public Class frm_base_edit
  Inherits frm_base

  Protected Enum ENUM_SCREEN_MODE
    MODE_EDIT = 0
    MODE_NEW = 1
  End Enum

  Protected Enum ENUM_DB_OPERATION
    DB_OPERATION_CREATE = 1
    DB_OPERATION_DUPLICATE = 2
    DB_OPERATION_READ = 10
    DB_OPERATION_INSERT = 11
    DB_OPERATION_UPDATE = 12
    DB_OPERATION_DELETE = 13
    DB_OPERATION_BEFORE_READ = 20
    DB_OPERATION_BEFORE_INSERT = 21
    DB_OPERATION_BEFORE_UPDATE = 22
    DB_OPERATION_BEFORE_DELETE = 23
    DB_OPERATION_AFTER_READ = 30
    DB_OPERATION_AFTER_INSERT = 31
    DB_OPERATION_AFTER_UPDATE = 32
    DB_OPERATION_AFTER_DELETE = 33
    DB_OPERATION_AUDIT_INSERT = 41
    DB_OPERATION_AUDIT_UPDATE = 42
    DB_OPERATION_AUDIT_DELETE = 43
  End Enum

  Private m_screen_mode As ENUM_SCREEN_MODE
  Private m_output_data As CLASS_BASE
  Private m_input_data As CLASS_BASE
  Private m_db_status As ENUM_STATUS
  Private m_object_id As Object
  Private m_check_changes As Boolean

  ' AJQ, 18-NOV-2002 to allow DL Generation ... Chapuza
  Private m_close_on_ok As Boolean = True
  Private m_wrong_db_version As Boolean = False
  Private m_hide_panel_buttons As Boolean

  Public Property HidePanelButtons() As Boolean
    Get
      Return m_hide_panel_buttons
    End Get
    Set(ByVal Value As Boolean)
      m_hide_panel_buttons = Value

      If Value Then
        Me.GUI_HidePanelButtons()
      Else
        Me.GUI_ShowPanelButtons()
      End If

    End Set
  End Property

  Protected Property CloseOnOkClick() As Boolean
    Get
      Return m_close_on_ok
    End Get
    Set(ByVal Value As Boolean)
      m_close_on_ok = Value
    End Set
  End Property

  Protected Property CheckChanges() As Boolean
    Get
      Return m_check_changes
    End Get
    Set(ByVal Value As Boolean)
      m_check_changes = Value
    End Set
  End Property

  Protected Property DbStatus() As ENUM_STATUS
    Get
      Return m_db_status
    End Get
    Set(ByVal Value As ENUM_STATUS)
      m_db_status = Value
    End Set
  End Property

  Protected Property DbReadObject() As CLASS_BASE
    Get
      Return m_input_data
    End Get
    Set(ByVal Value As CLASS_BASE)
      m_input_data = Value
    End Set
  End Property

  Protected Property DbEditedObject() As CLASS_BASE
    Get
      Return m_output_data
    End Get
    Set(ByVal Value As CLASS_BASE)
      m_output_data = Value
    End Set
  End Property

  Protected Property ScreenMode() As ENUM_SCREEN_MODE
    Get
      Return m_screen_mode
    End Get
    Set(ByVal Value As ENUM_SCREEN_MODE)
      m_screen_mode = Value
    End Set
  End Property

  Public Property DbObjectId() As Object
    Get
      Return m_object_id
    End Get
    Set(ByVal Value As Object)
      m_object_id = Value
    End Set
  End Property

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call
  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Protected WithEvents panel_data As System.Windows.Forms.Panel
  Private WithEvents btn_delete As uc_button
  Private WithEvents btn_ok As uc_button
  Private WithEvents btn_cancel As uc_button
  Private WithEvents btn_print As GUI_Controls.uc_button
  Private WithEvents panel_buttons As System.Windows.Forms.Panel
  Private WithEvents panel_separator As System.Windows.Forms.Panel
  Friend WithEvents btn_custom_2 As GUI_Controls.uc_button
  Friend WithEvents btn_custom_1 As GUI_Controls.uc_button
  Friend WithEvents btn_custom_0 As GUI_Controls.uc_button
  Friend WithEvents panel_public As System.Windows.Forms.Panel
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.panel_buttons = New System.Windows.Forms.Panel()
    Me.panel_public = New System.Windows.Forms.Panel()
    Me.btn_custom_2 = New GUI_Controls.uc_button()
    Me.btn_custom_1 = New GUI_Controls.uc_button()
    Me.btn_custom_0 = New GUI_Controls.uc_button()
    Me.panel_separator = New System.Windows.Forms.Panel()
    Me.btn_print = New GUI_Controls.uc_button()
    Me.btn_delete = New GUI_Controls.uc_button()
    Me.btn_ok = New GUI_Controls.uc_button()
    Me.btn_cancel = New GUI_Controls.uc_button()
    Me.panel_data = New System.Windows.Forms.Panel()
    Me.panel_buttons.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_buttons
    '
    Me.panel_buttons.Controls.Add(Me.panel_public)
    Me.panel_buttons.Controls.Add(Me.btn_custom_2)
    Me.panel_buttons.Controls.Add(Me.btn_custom_1)
    Me.panel_buttons.Controls.Add(Me.btn_custom_0)
    Me.panel_buttons.Controls.Add(Me.panel_separator)
    Me.panel_buttons.Controls.Add(Me.btn_print)
    Me.panel_buttons.Controls.Add(Me.btn_delete)
    Me.panel_buttons.Controls.Add(Me.btn_ok)
    Me.panel_buttons.Controls.Add(Me.btn_cancel)
    Me.panel_buttons.Dock = System.Windows.Forms.DockStyle.Right
    Me.panel_buttons.Location = New System.Drawing.Point(300, 4)
    Me.panel_buttons.Name = "panel_buttons"
    Me.panel_buttons.Size = New System.Drawing.Size(88, 301)
    Me.panel_buttons.TabIndex = 1
    '
    'panel_public
    '
    Me.panel_public.Dock = System.Windows.Forms.DockStyle.Fill
    Me.panel_public.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.panel_public.Location = New System.Drawing.Point(0, 0)
    Me.panel_public.Name = "panel_public"
    Me.panel_public.Size = New System.Drawing.Size(88, 61)
    Me.panel_public.TabIndex = 0
    '
    'btn_custom_2
    '
    Me.btn_custom_2.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_custom_2.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_custom_2.Location = New System.Drawing.Point(0, 61)
    Me.btn_custom_2.Name = "btn_custom_2"
    Me.btn_custom_2.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_custom_2.Size = New System.Drawing.Size(90, 30)
    Me.btn_custom_2.TabIndex = 99
    Me.btn_custom_2.ToolTipped = False
    Me.btn_custom_2.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_custom_1
    '
    Me.btn_custom_1.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_custom_1.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_custom_1.Location = New System.Drawing.Point(0, 91)
    Me.btn_custom_1.Name = "btn_custom_1"
    Me.btn_custom_1.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_custom_1.Size = New System.Drawing.Size(90, 30)
    Me.btn_custom_1.TabIndex = 100
    Me.btn_custom_1.ToolTipped = False
    Me.btn_custom_1.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_custom_0
    '
    Me.btn_custom_0.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_custom_0.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_custom_0.Location = New System.Drawing.Point(0, 121)
    Me.btn_custom_0.Name = "btn_custom_0"
    Me.btn_custom_0.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_custom_0.Size = New System.Drawing.Size(90, 30)
    Me.btn_custom_0.TabIndex = 101
    Me.btn_custom_0.ToolTipped = False
    Me.btn_custom_0.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'panel_separator
    '
    Me.panel_separator.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.panel_separator.Location = New System.Drawing.Point(0, 151)
    Me.panel_separator.Name = "panel_separator"
    Me.panel_separator.Size = New System.Drawing.Size(88, 30)
    Me.panel_separator.TabIndex = 4
    '
    'btn_print
    '
    Me.btn_print.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_print.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_print.Location = New System.Drawing.Point(0, 181)
    Me.btn_print.Name = "btn_print"
    Me.btn_print.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_print.Size = New System.Drawing.Size(90, 30)
    Me.btn_print.TabIndex = 102
    Me.btn_print.ToolTipped = False
    Me.btn_print.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_delete
    '
    Me.btn_delete.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_delete.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_delete.Location = New System.Drawing.Point(0, 211)
    Me.btn_delete.Name = "btn_delete"
    Me.btn_delete.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_delete.Size = New System.Drawing.Size(90, 30)
    Me.btn_delete.TabIndex = 103
    Me.btn_delete.ToolTipped = False
    Me.btn_delete.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_ok
    '
    Me.btn_ok.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_ok.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_ok.Location = New System.Drawing.Point(0, 241)
    Me.btn_ok.Name = "btn_ok"
    Me.btn_ok.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_ok.Size = New System.Drawing.Size(90, 30)
    Me.btn_ok.TabIndex = 104
    Me.btn_ok.ToolTipped = False
    Me.btn_ok.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_cancel
    '
    Me.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_cancel.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_cancel.Location = New System.Drawing.Point(0, 271)
    Me.btn_cancel.Name = "btn_cancel"
    Me.btn_cancel.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_cancel.Size = New System.Drawing.Size(90, 30)
    Me.btn_cancel.TabIndex = 105
    Me.btn_cancel.ToolTipped = False
    Me.btn_cancel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
        Or System.Windows.Forms.AnchorStyles.Left) _
        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.panel_data.Location = New System.Drawing.Point(4, 4)
    Me.panel_data.Name = "panel_data"
    Me.panel_data.Size = New System.Drawing.Size(290, 300)
    Me.panel_data.TabIndex = 0
    '
    'frm_base_edit
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(392, 309)
    Me.Controls.Add(Me.panel_data)
    Me.Controls.Add(Me.panel_buttons)
    Me.KeyPreview = True
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_base_edit"
    Me.ShowInTaskbar = False
    Me.Text = "frm_base_edit"
    Me.panel_buttons.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Entry Points "
  Protected Overridable Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim ctx As Integer


    If m_wrong_db_version Then
      DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED

      Return
    End If

    If Not MyBase.RightDbVersion() Then

      '' 163  "Operation not available.\nMinimum Database version: %1."
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(163), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, m_min_db_version.ToString("000"))

      '' Flag to avoid other GUI_DB_Operation calls if database is wrong.
      m_wrong_db_version = True

      DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED
      Return
    End If

    Try
      'XVV 17/04/2007
      'Comment this function, because are obsolete (Oracle version)
      'GUI_SqlCtxAlloc(ctx)

      Select Case DbOperation
        Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
          ' Creates the object
          Err.Raise(100000, "You must create your data")
          DbReadObject = Nothing
          DbEditedObject = Nothing

        Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
          If Not DbEditedObject Is Nothing Then
            DbReadObject = DbEditedObject.Duplicate
          Else
            DbReadObject = Nothing
          End If

        Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ
        Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT
        Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE
        Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE

        Case ENUM_DB_OPERATION.DB_OPERATION_READ
          DbStatus = DbEditedObject.DB_Read(DbObjectId, ctx)
        Case ENUM_DB_OPERATION.DB_OPERATION_INSERT
          DbStatus = DbEditedObject.DB_Insert(ctx)
        Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
          DbStatus = DbEditedObject.DB_Update(ctx)
        Case ENUM_DB_OPERATION.DB_OPERATION_DELETE
          DbStatus = DbEditedObject.DB_Delete(ctx)

        Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE

          ' Audit Operations 
        Case ENUM_DB_OPERATION.DB_OPERATION_AUDIT_INSERT
          If Not DbEditedObject.AuditorData.Notify(CurrentUser.GuiId, _
                                            CurrentUser.Id, _
                                            CurrentUser.Name, _
                                            CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.INSERT, _
                                            ctx, _
                                            DbReadObject.AuditorData) Then
            ' Logger message 
          End If
        Case ENUM_DB_OPERATION.DB_OPERATION_AUDIT_UPDATE
          If Not DbEditedObject.AuditorData.Notify(CurrentUser.GuiId, _
                                            CurrentUser.Id, _
                                            CurrentUser.Name, _
                                            CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                            ctx, _
                                            DbReadObject.AuditorData) Then
            ' Logger message 
          End If

        Case ENUM_DB_OPERATION.DB_OPERATION_AUDIT_DELETE
          If Not DbEditedObject.AuditorData.Notify(CurrentUser.GuiId, _
                                            CurrentUser.Id, _
                                            CurrentUser.Name, _
                                            CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.DELETE, _
                                            ctx, _
                                            DbReadObject.AuditorData) Then
            ' Logger message 
          End If
        Case Else
      End Select


    Finally
      'XVV 17/04/2007
      'Comment this function, because are obsolete (Oracle version)
      'GUI_SqlCtxRelease(ctx)
    End Try

  End Sub

  Public Sub ShowSelectedItem(ByVal ObjectId As Object)
    ' Sets the screen mode
    m_screen_mode = ENUM_SCREEN_MODE.MODE_EDIT

    DbObjectId = ObjectId

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Me.Display(True)
    End If
  End Sub

  Public Sub ShowEditItem(ByVal ObjectId As Object)
    ' Sets the screen mode
    m_screen_mode = ENUM_SCREEN_MODE.MODE_EDIT

    DbObjectId = ObjectId

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Me.Display(True)
    End If
  End Sub

  Public Sub ShowNewItem()
    ' Sets the screen mode
    m_screen_mode = ENUM_SCREEN_MODE.MODE_NEW

    DbObjectId = Nothing

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Me.Display(True)
    End If

  End Sub

#End Region

#Region " Screen Data "

  Private Function DiscardChanges() As Boolean

    If m_check_changes Then
      Call GUI_GetScreenData()
    End If
    m_check_changes = True

    If DbReadObject Is Nothing Then
      Return True
    End If

    If Not DbReadObject.AuditorData.IsEqual(DbEditedObject.AuditorData) Then
      If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Return False
      End If
    End If

    Return True

  End Function

#End Region

#Region " Button Events "

  Private Sub ButtonOkClick()
    If Not Permissions.Write Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Exit Sub
    End If

    'added to cahnge the focus from the date picker
    Me.ActiveControl = Me.AcceptButton

    ' Check the data on the screen
    If Not GUI_IsScreenDataOk() Then
      Exit Sub
    End If

    Call GUI_GetScreenData()

    ' AJQ, 18-OCT-2002, The DataChanged is only checked 4 Update ...
    'If DbReadObject.AuditorData.IsEqual(DbEditedObject.AuditorData) Then
    '  Call Me.Close()

    '  Exit Sub
    'End If

    Select Case m_screen_mode
      Case ENUM_SCREEN_MODE.MODE_EDIT

        If DbReadObject.AuditorData.IsEqual(DbEditedObject.AuditorData) Then
          ' AJQ, 18-NOV-2002 ... Chapuza
          If Me.CloseOnOkClick Then
            Call Me.Close()
          End If

          Exit Sub
        End If

        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE)
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_UPDATE)
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE)
        If DbStatus = ENUM_STATUS.STATUS_OK Then
          Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AUDIT_UPDATE)
        End If


      Case ENUM_SCREEN_MODE.MODE_NEW
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT)
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_INSERT)
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT)
        If DbStatus = ENUM_STATUS.STATUS_OK Then
          Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AUDIT_INSERT)
        End If

      Case Else
        ' Error
    End Select

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
      ' AJQ, 18-NOV-2002 ... Chapuza
      If Me.CloseOnOkClick Then
        m_check_changes = False
        Call Me.Close()
      End If
    End If
  End Sub


  Private Sub ButtonDeleteClick()
    If Permissions.Delete Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DELETE)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE)
      If DbStatus = ENUM_STATUS.STATUS_OK Then
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AUDIT_DELETE)
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
        m_check_changes = False
        Call Me.Close()
      End If
    Else
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

    End If
  End Sub


#End Region







#Region " Must Override: frm_base_edit "

  Protected Overridable Function GUI_IsScreenDataOk() As Boolean
    Return False
  End Function

  Protected Overridable Sub GUI_GetScreenData()
    ' Copy the values that you are editing to the object
  End Sub

  Protected Overridable Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    ' Copy the values from the object to the controls
  End Sub

  Protected Overridable Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean
    Return False
    '
  End Function


#End Region

#Region " Must Override Methods: frm_base "

  Protected Overrides Sub GUI_InitControls()
    ' Required by the base class
    Call MyBase.GUI_InitControls()

    If Me.DesignMode Then
      Dim na As String()
      Dim va As Integer()
      Dim idx As Integer

      '------------------------------------------------------------------
      'XVV 17/04/2007
      'Change xx valur for [Enum], Enum recover value from Enum structure
      '------------------------------------------------------------------
      'Dim xx As ENUM_BUTTON
      'va = xx.GetValues(GetType(ENUM_BUTTON))
      'na = xx.GetNames(GetType(ENUM_BUTTON))

      Dim xx As ENUM_BUTTON
      va = [Enum].GetValues(GetType(ENUM_BUTTON))
      na = [Enum].GetNames(GetType(ENUM_BUTTON))
      '------------------------------------------------------------------
      For idx = 0 To va.GetLength(0) - 1
        xx = va(idx)
        GUI_Button(va(idx)).Text = Mid(xx.ToString, 8, 9)
      Next
      Exit Sub
    End If

    'Add any initialization after the MyBase.GUI_InitControls()() call
    Me.btn_print.Text = GLB_NLS_GUI_CONTROLS.GetString(5)     ' Imprimir
    Me.btn_cancel.Text = GLB_NLS_GUI_CONTROLS.GetString(2)
    Me.btn_delete.Text = GLB_NLS_GUI_CONTROLS.GetString(11)
    Me.btn_ok.Text = GLB_NLS_GUI_CONTROLS.GetString(1)
    m_check_changes = True

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = Me.Permissions.Write
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = Me.Permissions.Delete

    'Call set form features resolutions when size's form exceeds desktop area
    Call Misc.GUI_AutoAdjustResolutionForm(Me, Nothing, panel_data)
  End Sub

#End Region


  Private Sub frm_base_edit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
    If GUI_KeyPress(sender, e) Then
      Return
    End If

    Select Case e.KeyChar
      Case Chr(Keys.Enter)
        If GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled Then
          Call btn_ok_ClickEvent()
        End If
      Case Chr(Keys.Escape)
        Call btn_cancel_ClickEvent()
      Case Else
    End Select

  End Sub


#Region " Buttons "

  Protected Sub GUI_HidePanelButtons()

    If Me.panel_buttons.Visible Then
      Me.Size = New Size() With {.Height = Me.Size.Height, .Width = Me.Size.Width - Me.panel_buttons.Width}
      Me.panel_buttons.Visible = False
    End If

  End Sub

  Protected Sub GUI_ShowPanelButtons()

    If Not Me.panel_buttons.Visible Then
      Me.Size = New Size() With {.Height = Me.Size.Height, .Width = Me.Size.Width + Me.panel_buttons.Width}
      Me.panel_buttons.Visible = True
    End If

  End Sub

#Region " Buttons - Control "

  Protected Function GUI_Button(ByVal ButtonId As ENUM_BUTTON) As uc_button
    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CANCEL
        Return btn_cancel
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Return btn_custom_0
      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        Return btn_custom_1
      Case ENUM_BUTTON.BUTTON_CUSTOM_2
        Return btn_custom_2
      Case ENUM_BUTTON.BUTTON_DELETE
        Return btn_delete
      Case ENUM_BUTTON.BUTTON_OK
        Return btn_ok
      Case ENUM_BUTTON.BUTTON_PRINT
        Return btn_print
      Case Else
        Return Nothing
    End Select
  End Function

#End Region

#Region " Buttons - Actions "

  Protected Overridable Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)
    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CANCEL
        Call Me.Close()

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
      Case ENUM_BUTTON.BUTTON_CUSTOM_1
      Case ENUM_BUTTON.BUTTON_CUSTOM_2

      Case ENUM_BUTTON.BUTTON_DELETE
        Call ButtonDeleteClick()

      Case ENUM_BUTTON.BUTTON_OK
        Call ButtonOkClick()

      Case ENUM_BUTTON.BUTTON_EXCEL
        Call GUI_ExcelResultList()

      Case ENUM_BUTTON.BUTTON_PRINT
        Call GUI_PrintResultList()
      Case Else
        '
    End Select

    If GUI_HasToBeAudited(ButtonId) Then
      Call MyBase.AuditFormClick(ButtonId)
    End If

  End Sub

  ' PURPOSE: Generate Excel report
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overridable Sub GUI_ExcelResultList()
    'Override when needed
  End Sub

  ' PURPOSE: Generate Print report
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overridable Sub GUI_PrintResultList()
    'Override when needed
  End Sub
#End Region

#Region " Buttons - Events "

  Private Sub BaseButtonClick(ByVal ButtonId As ENUM_BUTTON)

    If Not GUI_Button(ButtonId).Enabled Then
      Common_LoggerMsg(ENUM_LOG_MSG.LOG_USER_MESSAGE, "frm_base_edit", "BaseButtonClick", , , , "Calling DISABLED button " + ButtonId.ToString())
    End If

    'XVV 16/04/2007
    'change Me.Cursor.Current for Windows.Forms.Cursor because compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    Call GUI_ButtonClick(ButtonId)

    'XVV 16/04/2007
    'change Me.Cursor.Current for Windows.Forms.Cursor because compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub btn_custom_0_ClickEvent() Handles btn_custom_0.ClickEvent
    Call BaseButtonClick(ENUM_BUTTON.BUTTON_CUSTOM_0)
  End Sub

  Private Sub btn_custom_1_ClickEvent() Handles btn_custom_1.ClickEvent
    Call BaseButtonClick(ENUM_BUTTON.BUTTON_CUSTOM_1)
  End Sub

  Private Sub btn_custom_2_ClickEvent() Handles btn_custom_2.ClickEvent
    Call BaseButtonClick(ENUM_BUTTON.BUTTON_CUSTOM_2)
  End Sub

  Private Sub btn_cancel_ClickEvent() Handles btn_cancel.ClickEvent
    Call BaseButtonClick(ENUM_BUTTON.BUTTON_CANCEL)
  End Sub

  Private Sub btn_ok_ClickEvent() Handles btn_ok.ClickEvent
    Call BaseButtonClick(ENUM_BUTTON.BUTTON_OK)
  End Sub

  Private Sub btn_delete_ClickEvent() Handles btn_delete.ClickEvent
    Call BaseButtonClick(ENUM_BUTTON.BUTTON_DELETE)
  End Sub

  Private Sub btn_print_ClickEvent() Handles btn_print.ClickEvent
    Call BaseButtonClick(ENUM_BUTTON.BUTTON_PRINT)
  End Sub

#End Region

#End Region


  Protected ReadOnly Property GUI_PublicPanel() As Panel
    Get
      Return Me.panel_public
    End Get
  End Property

  Protected Overrides Sub GUI_FirstActivation()
    Dim ctx As Integer

    Try
      'XVV 17/04/2007
      'Comment this function, because are obsolete (Oracle version)
      'GUI_SqlCtxAlloc(ctx)

      Call MyBase.GUI_FirstActivation()
      '
      If Me.Permissions.Read = False Then
        Call NLS_MsgBox(CInt(0), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly)
        Me.Hide()

        Exit Sub
      End If

      GUI_SetScreenData(ctx)

    Finally
      'XVV 17/04/2007
      'Comment this function, because are obsolete (Oracle version)
      'GUI_SqlCtxRelease(ctx)

    End Try

  End Sub

  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_COMMON_FORM.COMMON_FORM_BASE_EDIT

    '----------------------------------------------------------
    'XVV 13/04/2007
    'Assign Icon to active form 
    'OLD version, use GetExecutingAssembly, but in Class libary is necessary GetEntreAssembly to recover ICON Resource
    If Not DesignMode Then Exit Sub
    'Me.Icon = New Icon(System.Reflection.Assembly.GetEntryAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))

  End Sub

  'Private Sub frm_base_edit_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
  '  If DiscardChanges() Then
  '    e.Cancel = False
  '  Else
  '    e.Cancel = True
  '  End If
  'End Sub

  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    If DiscardChanges() Then
      CloseCanceled = False
    Else
      CloseCanceled = True
    End If
  End Sub

  Public Overrides Function GUI_GetScreenType() As ENUM_SCREEN_TYPE
    Select Case Me.ScreenMode
      Case ENUM_SCREEN_MODE.MODE_EDIT
        Return frm_base.ENUM_SCREEN_TYPE.MODE_EDIT

      Case ENUM_SCREEN_MODE.MODE_NEW
        Return frm_base.ENUM_SCREEN_TYPE.MODE_NEW

      Case Else
        Return frm_base.ENUM_SCREEN_TYPE.MODE_NONE
    End Select
  End Function

  Public Sub HideButtons()
    Me.Width -= Me.panel_buttons.Width
    Me.panel_buttons.Width = 0
  End Sub

End Class
