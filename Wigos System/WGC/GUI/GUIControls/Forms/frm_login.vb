'-------------------------------------------------------------------
' Copyright � 2002-2008 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_login.vb
'
' DESCRIPTION:   Login for all GUIs
'
' AUTHOR:        Andreu Juli�.
'
' CREATION DATE: 19-MAR-2007
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 19-MAR-2007  AJQ    Initial version.
' 26-AUG-2002  JSV    Add button to change password.
' 18-OCT-2011  JML    Add alarm for insert alarm MAX failed attemps of login.
' 20-OCT-2011  JML    If user is blank no action.
' 29-MAR-2012  MPO    User session: Not continue if not can logged.
' 29-AUG-2012  JCM    Added non alpha numeric chars for password 
'                     Added case sensitive password
' 31-JAN-2013  LEM    Changed LoginUser rutine to become similar Cashier.LoginUser 
' 10-JUL-2013  DHA    Changed the length from the field 'Password' from 15 to 22 characters
' 29-AUG-2013  JPJ    Fixed Bug WIG-165 Change password policy with corporate users
' 25-FEB-2014  AMF    Fixed Bug WIG-673: Audit when empty password
'-------------------------------------------------------------------
Option Strict On
Option Explicit On 

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Windows.Forms

Friend Class frm_login
  Inherits GUI_Controls.frm_base

#Region " Form Variables "
  ' LOCAL CONSTANTS
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Private Const MIN_PASSWORD_LENGTH As Integer = 1
  Private Const MIN_USERNAME_LENGTH As Integer = 1


  ' GLOBALS
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Used the hold the Module Id
  Private m_gui_id As ENUM_GUI
  Private m_user_name As String
  Private m_user_password As String
  Private m_gui_name As String
  Private m_pwd_policy As WSI.Common.PasswordPolicy

  ' holds the result of the process
  Private m_login_ok As Boolean

  ' Counter of successive login failed attempt with any username
  Private m_login_failures As Integer

#End Region

#Region " Windows Form Designer generated code "
  Public Sub New()

    MyBase.New()

    InitializeComponent()

    m_pwd_policy = New WSI.Common.PasswordPolicy()

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Private WithEvents panel_buttons As System.Windows.Forms.Panel
  Friend WithEvents ef_password As uc_entry_field
  Friend WithEvents ef_name As uc_entry_field
  Private WithEvents btn_ok As uc_button
  Private WithEvents btn_cancel As uc_button
  Friend WithEvents chk_change As System.Windows.Forms.CheckBox
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.panel_buttons = New System.Windows.Forms.Panel()
    Me.btn_ok = New GUI_Controls.uc_button()
    Me.btn_cancel = New GUI_Controls.uc_button()
    Me.ef_password = New GUI_Controls.uc_entry_field()
    Me.ef_name = New GUI_Controls.uc_entry_field()
    Me.chk_change = New System.Windows.Forms.CheckBox()
    Me.panel_buttons.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_buttons
    '
    Me.panel_buttons.Controls.Add(Me.btn_ok)
    Me.panel_buttons.Controls.Add(Me.btn_cancel)
    Me.panel_buttons.Dock = System.Windows.Forms.DockStyle.Right
    Me.panel_buttons.Location = New System.Drawing.Point(406, 4)
    Me.panel_buttons.Name = "panel_buttons"
    Me.panel_buttons.Size = New System.Drawing.Size(88, 129)
    Me.panel_buttons.TabIndex = 6
    '
    'btn_ok
    '
    Me.btn_ok.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_ok.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_ok.Location = New System.Drawing.Point(0, 69)
    Me.btn_ok.Name = "btn_ok"
    Me.btn_ok.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_ok.Size = New System.Drawing.Size(90, 30)
    Me.btn_ok.TabIndex = 3
    Me.btn_ok.ToolTipped = False
    Me.btn_ok.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_cancel
    '
    Me.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_cancel.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_cancel.Location = New System.Drawing.Point(0, 99)
    Me.btn_cancel.Name = "btn_cancel"
    Me.btn_cancel.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_cancel.Size = New System.Drawing.Size(90, 30)
    Me.btn_cancel.TabIndex = 4
    Me.btn_cancel.ToolTipped = False
    Me.btn_cancel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'ef_password
    '
    Me.ef_password.DoubleValue = 0.0R
    Me.ef_password.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_password.IntegerValue = 0
    Me.ef_password.IsReadOnly = False
    Me.ef_password.Location = New System.Drawing.Point(40, 65)
    Me.ef_password.Name = "ef_password"
    Me.ef_password.PlaceHolder = Nothing
    Me.ef_password.Size = New System.Drawing.Size(360, 33)
    Me.ef_password.SufixText = "Sufix Text"
    Me.ef_password.SufixTextVisible = True
    Me.ef_password.TabIndex = 2
    Me.ef_password.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_password.TextValue = ""
    Me.ef_password.TextWidth = 120
    Me.ef_password.Value = ""
    Me.ef_password.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_name
    '
    Me.ef_name.DoubleValue = 0.0R
    Me.ef_name.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_name.IntegerValue = 0
    Me.ef_name.IsReadOnly = False
    Me.ef_name.Location = New System.Drawing.Point(40, 20)
    Me.ef_name.Name = "ef_name"
    Me.ef_name.PlaceHolder = Nothing
    Me.ef_name.Size = New System.Drawing.Size(360, 33)
    Me.ef_name.SufixText = "Sufix Text"
    Me.ef_name.SufixTextVisible = True
    Me.ef_name.TabIndex = 1
    Me.ef_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name.TextValue = ""
    Me.ef_name.TextWidth = 120
    Me.ef_name.Value = ""
    Me.ef_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_change
    '
    Me.chk_change.Location = New System.Drawing.Point(152, 104)
    Me.chk_change.Name = "chk_change"
    Me.chk_change.Size = New System.Drawing.Size(176, 24)
    Me.chk_change.TabIndex = 7
    Me.chk_change.Text = "xCambio de Clave"
    '
    'frm_login
    '
    Me.AcceptButton = Me.btn_ok
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.CancelButton = Me.btn_cancel
    Me.ClientSize = New System.Drawing.Size(498, 137)
    Me.Controls.Add(Me.chk_change)
    Me.Controls.Add(Me.ef_name)
    Me.Controls.Add(Me.ef_password)
    Me.Controls.Add(Me.panel_buttons)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_login"
    Me.ShowInTaskbar = False
    Me.Text = "frm_login"
    Me.TopMost = True
    Me.panel_buttons.ResumeLayout(False)
    Me.ResumeLayout(False)

End Sub

#End Region

#Region " Login Form events "

  ' PURPOSE: Cancel login process.
  '
  '    - INPUT:
  '              None
  '    - OUTPUT:
  '              None
  '
  ' RETURNS:
  '          None
  ' NOTES
  '
  Private Sub btn_cancel_ClickEvent() Handles btn_cancel.ClickEvent
    Me.Hide()
  End Sub

  ' PURPOSE: Invoques login process and forces or not to change password.
  '         
  '    - INPUT:
  '              None
  '    - OUTPUT:
  '              None
  '
  ' RETURNS:
  '          None
  ' NOTES
  '
  Private Sub btn_ok_ClickEvent() Handles btn_ok.ClickEvent

    Dim _source_code As WSI.Common.AlarmSourceCode
    Dim _source_id As Integer
    Dim _source_name As String
    Dim _description As String
    Dim _severity As WSI.Common.AlarmSeverity
    Dim _generated_date_time As Date

    If ef_name.Value = "" Then
      Return
    End If

    LoginUser(chk_change.Checked)

    If Not m_login_ok Then
      m_login_failures = m_login_failures + 1
    End If

    ''check if login failure limit was reached
    ' After MAX failed attemps of login, application finalizes.
    If m_login_failures >= m_pwd_policy.MaxLoginAttempts Then
      ' insert alarm MAX failed attemps of login

      _source_code = WSI.Common.AlarmSourceCode.User
      _source_id = m_gui_id
      _source_name = ef_name.Value & "@" & Environment.MachineName
      _description = GLB_NLS_GUI_CONTROLS.GetString(154)             'Maximum number of login attempts exceeded.
      _severity = WSI.Common.AlarmSeverity.Warning
      _generated_date_time = WSI.Common.WGDB.Now

      WSI.Common.Alarm.Register(_source_code, _
                                   _source_id, _
                                   _source_name, _
                                   WSI.Common.AlarmCode.User_MaxLoginAttemps, _
                                   _description)

      Me.Hide()
    End If

  End Sub

#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the window title and all the
  '          entry fields
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  ' 
  Protected Overrides Sub GUI_InitControls()

    MyBase.GUI_InitControls()

    'Add any initialization after the MyBase.GUI_InitControls() call

    If m_gui_name = vbNullString Then
      Me.Text = GLB_NLS_GUI_CONTROLS.GetString(265)
    Else
      Me.Text = m_gui_name & " - " & GLB_NLS_GUI_CONTROLS.GetString(265)
    End If

    ' Set the text in the entry fields
    ef_name.Text = GLB_NLS_GUI_CONTROLS.GetString(260)
    ef_password.Text = GLB_NLS_GUI_CONTROLS.GetString(267)

    ' Set the filters and the properties of the entry fields
    ef_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC, WSI.Common.PasswordPolicy.MAX_LOGIN_LENGTH_CONST)
    ef_name.OnlyUpperCase = True

    ef_password.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, WSI.Common.PasswordPolicy.MAX_PASSWORD_LENGTH_CONST)
    ef_password.Password = True

    ef_password.OnlyUpperCase = False

    ' Init the buttons
    btn_ok.Text = GLB_NLS_GUI_CONTROLS.GetString(1)
    btn_cancel.Text = GLB_NLS_GUI_CONTROLS.GetString(2)

    ' Set the text in the check boxes
    chk_change.Text = GLB_NLS_GUI_CONTROLS.GetString(268)

    ' Cleans and sets all entry values
    Call ResetEntries()

  End Sub ' GUI_InitControls


  ' PURPOSE: First activation of dialog actions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_FirstActivation()

    If m_gui_name = vbNullString Then
      Me.Text = GLB_NLS_GUI_CONTROLS.GetString(265)
    Else
      Me.Text = m_gui_name & " - " & GLB_NLS_GUI_CONTROLS.GetString(265)
    End If

    If GLB_DbServerName = "" Then
      Me.Text = Me.Text
    Else
      Me.Text = Me.Text & " - @" & GLB_DbServerName
    End If

  End Sub ' GUI_FirstActivation


  ' PURPOSE: Called to be set the initial focus.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_SetInitialFocus()

    ef_password.Focus()

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Given a type, the function returns if it needs to be audited
  '
  '  PARAMS:
  '     - INPUT:
  '         - AuditType: AUDIT_FLAGS that wants to be audited
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_HasToBeAudited(ByVal AuditType As AUDIT_FLAGS) As Boolean

    Return False

  End Function

  ' PURPOSE: Set form permissions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_Permissions(ByRef Perm As GUI_Controls.CLASS_GUI_USER.TYPE_PERMISSIONS)
    Perm.Read = True
    Perm.Write = False
    Perm.Delete = False
    Perm.Execute = False

  End Sub ' GUI_Permissions

  ' PURPOSE: GUI form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_COMMON_FORM.COMMON_FORM_LOGIN

  End Sub ' GUI_SetFormId

#End Region

#Region " Public Functions "

  ' PURPOSE: Login entry point function.
  '          
  '    - INPUT:
  '              GUIId: Each GUI enters its GUI identifier. 
  '              GUIName: Name of the GUI. Used in the title of Login Window
  '              LastUserName: (opcional) The GUI may enter the Last user logged.
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '          TRUE:  Login was OK
  '          FALSE: Login Fails
  ' NOTES
  '
  Public Function Login(ByVal GUIId As ENUM_GUI, _
                        Optional ByVal GUIName As String = "", _
                        Optional ByRef LastUserName As String = "") As Boolean

    ' Check if the current user is logged in.
    ' The user has to be logged out before the next login
    If GLB_CurrentUser.IsLogged Then
      'An user is already logged in, login fails
      Return False
    End If

    ' init all globals
    m_login_ok = False
    m_gui_name = GUIName
    m_gui_id = GUIId
    m_user_name = UCase(LastUserName)
    m_user_password = ""
    m_login_failures = 0

#If DEBUG Then
    Dim _aux_value1 As String
    Dim _aux_value2 As String

    If Environment.GetEnvironmentVariable("LKS_VC_DEV") <> vbNullString Then
      _aux_value1 = Environment.GetEnvironmentVariable("LKS_VC_GUI_USER_NAME")
      _aux_value2 = Environment.GetEnvironmentVariable("LKS_VC_GUI_USER_PASSWORD")
      If _aux_value1 <> vbNullString And _aux_value2 <> vbNullString Then
        m_user_name = _aux_value1
        m_user_password = _aux_value2
      End If
    End If
#End If

    'Show login windows
    Call Me.Display(True)

    'Me.ShowDialog()

    ' Returns the last user only if the login is OK
    If m_login_ok Then
      LastUserName = ef_name.Value
    End If

    Return m_login_ok

  End Function ' Login

#End Region

#Region " Private functions "

  ' PURPOSE: Gets and checks the parameters entered by the user in the Login Form.
  '          Check if it is enabled, check if password is OK.
  '          Get user hierarchy and permissions.
  '          Check for password change and invoque change password form.
  '         
  '    - INPUT:
  '              Force password change: If true, even when password has not expired, force change password
  '              (Used in change password button)
  '    - OUTPUT:
  '              None
  '
  ' RETURNS:
  '          None
  ' NOTES
  '
  Private Sub LoginUser(Optional ByVal ForcePwdChange As Boolean = False)

    Dim rc As Boolean
    Dim pwd_rc As CLASS_GUI_USER.ENUM_PASSWORD_ERROR
    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim ctx As Integer

    Try

      'XVV 17/04/2007
      'Comment this function, because are obsolete (Oracle version)
      'GUI_SqlCtxAlloc(ctx)

      ' Pacifier
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_LOGIN_LOGOUT)

      'check if the user has entered at least one character as UserName
      If Len(ef_name.Value) < MIN_USERNAME_LENGTH _
        Or Len(ef_name.Value) > WSI.Common.PasswordPolicy.MAX_LOGIN_LENGTH_CONST Then
        'Invalid UserName, the user has to enter a valid password (length)
        GLB_NLS_GUI_CONTROLS.MsgBox(456, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

        'clean password entry field
        ef_password.Value = ""
        ef_name.Value = ""
        ef_name.Select()

        Return
      End If ' Len(ef_name.Value) < MIN_USERNAME_LENGTH 

      'check if the user has entered at least one character as password
      If Len(ef_password.TextValueWithoutTrim) < MIN_PASSWORD_LENGTH _
        Or Len(ef_password.TextValueWithoutTrim) > WSI.Common.PasswordPolicy.MAX_PASSWORD_LENGTH_CONST Then
        'Invalid password, the user has to enter a valid password (length)
        GLB_NLS_GUI_CONTROLS.MsgBox(456, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

        auditor_data.SetName(GLB_NLS_GUI_CONTROLS.Id(265), GLB_NLS_GUI_CONTROLS.GetString(107, GLB_NLS_GUI_CONTROLS.GetString(458)))
        If Not auditor_data.Notify(m_gui_id, _
                                  0, _
                                  ef_name.Value, _
                                  CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                                  ctx) Then
          ' Logger message 
        End If

        'clean password entry field
        ef_password.Value = ""
        ef_password.Select()

        Return
      End If ' Len(ef_password.Value) < MIN_PASSWORD_LENGTH 

      'check the user into the database
      rc = GLB_CurrentUser.GetUserInDb(ef_name.Value, ctx)
      If rc = False Then
        'User not found
        GLB_NLS_GUI_CONTROLS.MsgBox(456, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

        ef_password.Value = ""
        ef_name.Value = ef_name.Value
        ef_name.Select()

        auditor_data.SetName(GLB_NLS_GUI_CONTROLS.Id(265), GLB_NLS_GUI_CONTROLS.GetString(457, ef_name.Value))
        auditor_data.Notify(m_gui_id, 0, "SU", CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, ctx)

        Return
      End If

      'JPJ 29-AUG-2013: Defect WIG-165
      If (GLB_CurrentUser.CheckPasswordChange()) And _
        WSI.Common.GeneralParam.GetBoolean("Site", "MultiSiteMember", False) And _
        GLB_CurrentUser.IsMaster Then
        'There is a pending password change of a corporate user. The change must be done in the multisite.
        GLB_NLS_GUI_CONTROLS.MsgBox(566, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

        auditor_data.SetName(GLB_NLS_GUI_CONTROLS.Id(265), GLB_NLS_GUI_CONTROLS.GetString(107, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2380)))
        auditor_data.Notify(m_gui_id, CurrentUser.Id, CurrentUser.Name, CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, ctx)

        Return
      End If

      'JPJ 29-AUG-2013: Defect WIG-165
      If (ForcePwdChange) And _
        WSI.Common.GeneralParam.GetBoolean("Site", "MultiSiteMember", False) And _
        GLB_CurrentUser.IsMaster Then
        'A corporate user account can only be changed in the multisite.
        GLB_NLS_GUI_CONTROLS.MsgBox(565, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

        Return
      End If

      'check the password using the package
      pwd_rc = GLB_CurrentUser.CheckPasswordLoginPkg(ef_password.TextValueWithoutTrim, ctx)

      If pwd_rc = CLASS_GUI_USER.ENUM_PASSWORD_ERROR.USER_DISABLED Then
        'User is disabled now
        GLB_NLS_GUI_CONTROLS.MsgBox(456, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

        ef_password.Value = ""
        ef_name.Value = ef_name.Value
        ef_name.Select()

        auditor_data.SetName(GLB_NLS_GUI_CONTROLS.Id(265), GLB_NLS_GUI_CONTROLS.GetString(107, GLB_NLS_GUI_CONTROLS.GetString(106)))
        auditor_data.Notify(m_gui_id, CurrentUser.Id, CurrentUser.Name, CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, ctx)

        Me.Hide()

        Return

      End If

      If GLB_CurrentUser.IsEnabled = False Or GLB_CurrentUser.CheckStartValidDate = False Then
        'User is disabled

        If pwd_rc = CLASS_GUI_USER.ENUM_PASSWORD_ERROR.OK Then
          GLB_NLS_GUI_CONTROLS.MsgBox(455, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          auditor_data.SetName(GLB_NLS_GUI_CONTROLS.Id(265), GLB_NLS_GUI_CONTROLS.GetString(107, GLB_NLS_GUI_CONTROLS.GetString(455)))
        Else
          GLB_NLS_GUI_CONTROLS.MsgBox(456, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
          auditor_data.SetName(GLB_NLS_GUI_CONTROLS.Id(265), GLB_NLS_GUI_CONTROLS.GetString(107, GLB_NLS_GUI_CONTROLS.GetString(458)))
        End If

        ef_password.Value = ""
        ef_name.Value = ef_name.Value
        ef_name.Select()

        auditor_data.Notify(m_gui_id, CurrentUser.Id, CurrentUser.Name, CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, ctx)

        Return
      End If

      If pwd_rc = CLASS_GUI_USER.ENUM_PASSWORD_ERROR.ACCESS_DENIED Then
        'User not found
        GLB_NLS_GUI_CONTROLS.MsgBox(456, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

        ef_password.Value = ""
        ef_name.Value = ef_name.Value
        ef_name.Select()

        auditor_data.SetName(GLB_NLS_GUI_CONTROLS.Id(265), GLB_NLS_GUI_CONTROLS.GetString(107, GLB_NLS_GUI_CONTROLS.GetString(458)))
        If Not auditor_data.Notify(m_gui_id, _
                                  CurrentUser.Id, _
                                  CurrentUser.Name, _
                                  CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                                  ctx) Then
          ' Logger message 
        End If

        'get back to the login window
        Return
      End If

      'get user permissions
      If Not GLB_CurrentUser.GetUserPermissions(m_gui_id, ctx) Then
        'User not found
        GLB_NLS_GUI_CONTROLS.MsgBox(456, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

        ef_password.Value = ""
        ef_name.Value = ef_name.Value
        ef_name.Select()

        auditor_data.SetName(GLB_NLS_GUI_CONTROLS.Id(265), GLB_NLS_GUI_CONTROLS.GetString(107))
        If Not auditor_data.Notify(m_gui_id, _
                                  CurrentUser.Id, _
                                  CurrentUser.Name, _
                                  CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                                  ctx) Then
          ' Logger message 
        End If

        'get back to the login window
        Return
      End If

      'Sets the user as logged
      If Not GLB_CurrentUser.SetUserLogged(m_gui_id, Me) Then

        Return
      End If

      Me.Hide()

      ' Audit login
      auditor_data.SetName(GLB_NLS_GUI_CONTROLS.Id(265), GLB_NLS_GUI_CONTROLS.GetString(265))
      If Not auditor_data.Notify(m_gui_id, _
                                CurrentUser.Id, _
                                CurrentUser.Name, _
                                CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                                ctx) Then
        ' Logger message 
      End If

      'Force to change password setting expired as true
      If ForcePwdChange Then
        CurrentUser.PwdChangeRequested = True
      End If

      'Check if password has to be changed
      If GLB_CurrentUser.CheckPasswordChange() Then
        'User has to change password
        ' call password change 
        If Not UserPwdChange(m_gui_id, m_gui_name) Then
          'if password change fails then logout the user and return false
          Call GLB_CurrentUser.Logout()
          Return
        End If

        'Change of password OK
        GLB_NLS_GUI_CONTROLS.MsgBox(105, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      End If

      m_login_ok = True

      Me.Hide()
    Finally
      'XVV 17/04/2007
      'Comment this function, because are obsolete (Oracle version)
      'GUI_SqlCtxRelease(ctx)
    End Try

  End Sub ' LoginUser


  ' PURPOSE: Cleans all entries controls in the Login form. In addition,
  '          sets the initial values, in this case, the last user logged in
  '
  '    - INPUT:
  '              UserName. Last user connected, passed as input from the caller (GUIs)
  '                        when null, ask the login user to enter its UserName
  '    - OUTPUT:
  '
  '
  ' RETURNS :
  '
  ' NOTES

  Private Sub ResetEntries()

    'cleans the password field
    ef_password.Value = ""

    ' by default no force to change password
    chk_change.Checked = False

    ' Sets the initial value for the UserName entry field
    If m_user_name = vbNullString Then
      ef_name.Value = ""
      ef_name.Select()
    Else
      ef_name.Value = m_user_name
      If m_user_password <> vbNullString Then
        ef_password.Value = m_user_password
      End If
      ' Set focus
      ef_password.Select()
    End If
  End Sub ' ResetEntries

#End Region

End Class
