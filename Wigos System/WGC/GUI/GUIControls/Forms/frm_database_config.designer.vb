<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_database_config
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Me.ef_db_primary = New GUI_Controls.uc_entry_field
    Me.ef_db_secondary = New GUI_Controls.uc_entry_field
    Me.ef_db_id = New GUI_Controls.uc_entry_field
    Me.btn_exit = New GUI_Controls.uc_button
    Me.btn_edit = New GUI_Controls.uc_button
    Me.gb_database_config = New System.Windows.Forms.GroupBox
    Me.tf_init_app_name = New GUI_Controls.uc_text_field
    Me.tf_init_copyright = New GUI_Controls.uc_text_field
    Me.pb_init_connecting = New System.Windows.Forms.ProgressBar
    Me.tf_init_connecting = New GUI_Controls.uc_text_field
    Me.tmr_init_db_conn = New System.Windows.Forms.Timer(Me.components)
    Me.gb_database_config.SuspendLayout()
    Me.SuspendLayout()
    '
    'ef_db_primary
    '
    Me.ef_db_primary.DoubleValue = 0
    Me.ef_db_primary.Enabled = False
    Me.ef_db_primary.IntegerValue = 0
    Me.ef_db_primary.IsReadOnly = False
    Me.ef_db_primary.Location = New System.Drawing.Point(10, 21)
    Me.ef_db_primary.Name = "ef_db_primary"
    Me.ef_db_primary.Size = New System.Drawing.Size(302, 24)
    Me.ef_db_primary.SufixText = "Sufix Text"
    Me.ef_db_primary.SufixTextVisible = True
    Me.ef_db_primary.TabIndex = 0
    Me.ef_db_primary.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_db_primary.TextValue = ""
    Me.ef_db_primary.Value = ""
    '
    'ef_db_secondary
    '
    Me.ef_db_secondary.DoubleValue = 0
    Me.ef_db_secondary.IntegerValue = 0
    Me.ef_db_secondary.IsReadOnly = False
    Me.ef_db_secondary.Location = New System.Drawing.Point(10, 51)
    Me.ef_db_secondary.Name = "ef_db_secondary"
    Me.ef_db_secondary.Size = New System.Drawing.Size(302, 24)
    Me.ef_db_secondary.SufixText = "Sufix Text"
    Me.ef_db_secondary.SufixTextVisible = True
    Me.ef_db_secondary.TabIndex = 1
    Me.ef_db_secondary.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_db_secondary.TextValue = ""
    Me.ef_db_secondary.Value = ""
    '
    'ef_db_id
    '
    Me.ef_db_id.DoubleValue = 0
    Me.ef_db_id.IntegerValue = 0
    Me.ef_db_id.IsReadOnly = False
    Me.ef_db_id.Location = New System.Drawing.Point(10, 83)
    Me.ef_db_id.Name = "ef_db_id"
    Me.ef_db_id.Size = New System.Drawing.Size(115, 24)
    Me.ef_db_id.SufixText = "Sufix Text"
    Me.ef_db_id.SufixTextVisible = True
    Me.ef_db_id.TabIndex = 2
    Me.ef_db_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_db_id.TextValue = ""
    Me.ef_db_id.Value = ""
    '
    'btn_exit
    '
    Me.btn_exit.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_exit.Location = New System.Drawing.Point(280, 194)
    Me.btn_exit.Name = "btn_exit"
    Me.btn_exit.Size = New System.Drawing.Size(90, 30)
    Me.btn_exit.TabIndex = 4
    Me.btn_exit.ToolTipped = False
    Me.btn_exit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_edit
    '
    Me.btn_edit.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_edit.Location = New System.Drawing.Point(249, 126)
    Me.btn_edit.Name = "btn_edit"
    Me.btn_edit.Size = New System.Drawing.Size(90, 30)
    Me.btn_edit.TabIndex = 3
    Me.btn_edit.ToolTipped = False
    Me.btn_edit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'gb_database_config
    '
    Me.gb_database_config.Controls.Add(Me.ef_db_primary)
    Me.gb_database_config.Controls.Add(Me.btn_edit)
    Me.gb_database_config.Controls.Add(Me.ef_db_secondary)
    Me.gb_database_config.Controls.Add(Me.ef_db_id)
    Me.gb_database_config.Location = New System.Drawing.Point(8, 7)
    Me.gb_database_config.Name = "gb_database_config"
    Me.gb_database_config.Size = New System.Drawing.Size(346, 162)
    Me.gb_database_config.TabIndex = 5
    Me.gb_database_config.TabStop = False
    Me.gb_database_config.Text = " Database Configuration "
    '
    'tf_init_app_name
    '
    Me.tf_init_app_name.Font = New System.Drawing.Font("Verdana", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.tf_init_app_name.IsReadOnly = True
    Me.tf_init_app_name.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_init_app_name.LabelForeColor = System.Drawing.SystemColors.ControlText
    Me.tf_init_app_name.Location = New System.Drawing.Point(105, 246)
    Me.tf_init_app_name.Name = "tf_init_app_name"
    Me.tf_init_app_name.Size = New System.Drawing.Size(192, 44)
    Me.tf_init_app_name.SufixText = "Sufix Text"
    Me.tf_init_app_name.SufixTextVisible = True
    Me.tf_init_app_name.TabIndex = 6
    Me.tf_init_app_name.TextWidth = 0
    Me.tf_init_app_name.Value = "xWigosGUI"
    '
    'tf_init_copyright
    '
    Me.tf_init_copyright.IsReadOnly = True
    Me.tf_init_copyright.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_init_copyright.LabelForeColor = System.Drawing.SystemColors.ControlText
    Me.tf_init_copyright.Location = New System.Drawing.Point(58, 296)
    Me.tf_init_copyright.Name = "tf_init_copyright"
    Me.tf_init_copyright.Size = New System.Drawing.Size(275, 25)
    Me.tf_init_copyright.SufixText = "Sufix Text"
    Me.tf_init_copyright.SufixTextVisible = True
    Me.tf_init_copyright.TabIndex = 7
    Me.tf_init_copyright.TextWidth = 0
    Me.tf_init_copyright.Value = "xCopyright (c) WSI Gaming S.L. 2007-2008"
    '
    'pb_init_connecting
    '
    Me.pb_init_connecting.Location = New System.Drawing.Point(45, 378)
    Me.pb_init_connecting.MarqueeAnimationSpeed = 500
    Me.pb_init_connecting.Name = "pb_init_connecting"
    Me.pb_init_connecting.Size = New System.Drawing.Size(289, 22)
    Me.pb_init_connecting.Step = 1
    Me.pb_init_connecting.Style = System.Windows.Forms.ProgressBarStyle.Marquee
    Me.pb_init_connecting.TabIndex = 8
    '
    'tf_init_connecting
    '
    Me.tf_init_connecting.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.tf_init_connecting.IsReadOnly = True
    Me.tf_init_connecting.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_init_connecting.LabelForeColor = System.Drawing.SystemColors.ControlText
    Me.tf_init_connecting.Location = New System.Drawing.Point(46, 345)
    Me.tf_init_connecting.Name = "tf_init_connecting"
    Me.tf_init_connecting.Size = New System.Drawing.Size(265, 27)
    Me.tf_init_connecting.SufixText = "Sufix Text"
    Me.tf_init_connecting.SufixTextVisible = True
    Me.tf_init_connecting.TabIndex = 9
    Me.tf_init_connecting.TextWidth = 0
    Me.tf_init_connecting.Value = "xConnecting to Database..."
    '
    'tmr_init_db_conn
    '
    '
    'frm_database_config
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(379, 419)
    Me.Controls.Add(Me.tf_init_connecting)
    Me.Controls.Add(Me.pb_init_connecting)
    Me.Controls.Add(Me.tf_init_copyright)
    Me.Controls.Add(Me.tf_init_app_name)
    Me.Controls.Add(Me.gb_database_config)
    Me.Controls.Add(Me.btn_exit)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_database_config"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "Database Configuration"
    Me.TopMost = True
    Me.gb_database_config.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_db_primary As GUI_Controls.uc_entry_field
  Friend WithEvents ef_db_secondary As GUI_Controls.uc_entry_field
  Friend WithEvents ef_db_id As GUI_Controls.uc_entry_field
  Friend WithEvents btn_exit As GUI_Controls.uc_button
  Friend WithEvents btn_edit As GUI_Controls.uc_button
  Friend WithEvents gb_database_config As System.Windows.Forms.GroupBox
  Friend WithEvents tf_init_app_name As GUI_Controls.uc_text_field
  Friend WithEvents tf_init_copyright As GUI_Controls.uc_text_field
  Friend WithEvents pb_init_connecting As System.Windows.Forms.ProgressBar
  Friend WithEvents tf_init_connecting As GUI_Controls.uc_text_field
  Friend WithEvents tmr_init_db_conn As System.Windows.Forms.Timer
End Class
