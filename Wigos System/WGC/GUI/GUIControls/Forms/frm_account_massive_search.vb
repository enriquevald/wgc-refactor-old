'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_account_massive_search.vb
' DESCRIPTION:   Massive search for account numbers
' AUTHOR:        Carlos C�ceres Gonz�lez
' CREATION DATE: 09-SEP-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 09-SEP-2013  CCG    Initial version.
' 18-SEP-2013  CCG    Ask for delete duplicates and show these in red.
' 26-SEP-2013  CCG    Added Multitype massive search.
' 01-OCT-2013  CCG    Fixed Bug WIG-238: Appears 2 times the message to accept the cancellation if you press the "Esc"
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On

#Region " Imports "

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Windows.Forms
Imports System.Text

#End Region ' Imports

Public Class frm_account_massive_search
  Inherits GUI_Controls.frm_base

  Private uc_account_sel As uc_account_sel
  Private GLB_Accept As Boolean
  Private GLB_MaxDigits As Integer

#Region " Constants "

  Private Const SEPARATOR As Char = " "

#End Region ' Constants

#Region " Windows Form Designer generated code "

  Public Sub New(ByRef uc_account_sel1)

    Call MyBase.New()
    Me.uc_account_sel = uc_account_sel1
    Call InitializeComponent()
    Call Me.GUI_SetInitialFocus()

  End Sub

#End Region ' Windows Form Designer generated code

#Region " Overridable GUI function "

  ' PURPOSE: Initializes the window title and all the
  '          entry fields
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  ' 
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    If Me.uc_account_sel.MassiveSearchType = GUI_Controls.uc_account_sel.ENUM_MASSIVE_SEARCH_TYPE.ID_ACCOUNT Then
      Call Me.rb_account_number.Select()
      Me.GLB_MaxDigits = 10
    Else
      Call Me.rb_card_number.Select()
      Me.GLB_MaxDigits = 20
    End If

    ' Set the text in the entry fields
    Me.rtxt_accounts.Text = String.Empty

    If Not String.IsNullOrEmpty(Me.uc_account_sel.MassiveSearchNumbersToEdit) Then
      Call Me.SetAccountText(Me.uc_account_sel.MassiveSearchNumbersToEdit, False)
    End If

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2556) ' B�squeda masiva de cuentas

    ' Init the buttons text
    Me.btn_delete.Text = GLB_NLS_GUI_CONTROLS.GetString(11) ' Borrar
    Me.btn_accept.Text = GLB_NLS_GUI_CONTROLS.GetString(1) ' Aceptar
    Me.btn_exit.Text = GLB_NLS_GUI_CONTROLS.GetString(2) ' Cancelar
    Me.btn_import_excel.Text = GLB_NLS_GUI_CONFIGURATION.GetString(1) ' Importar

    ' Init the controls in the group box
    Me.rb_account_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1202)
    Me.rb_card_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2157)

    Me.gb_buscar_por.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1892)
  End Sub ' GUI_InitControls

  ' PURPOSE: GUI form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: None
  '
  ' NOTES
  ' 
  Public Overrides Sub GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE : Sets initial focus
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  ' 
  Protected Overrides Sub GUI_SetInitialFocus()
    Call Me.rtxt_accounts.Focus()
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE : Check is there is any changes in the accoun list, and if so, give the option to cancel the operation
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  ' 
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    CloseCanceled = Me.CheckForChanges()
  End Sub

#End Region ' Overridable GUI function

#Region " Private Functions "

  ' PURPOSE: Validate format of the introduced accounts, and save them if they are ok
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS:
  '       True - The validation is ok
  '       False - The validation is not ok
  '          
  ' NOTES
  '
  Private Function ValidateAndSaveAccounts() As Boolean
    Dim _number As String
    Dim _account_numbers As New StringBuilder
    Dim _numbers As ArrayList
    Dim _table_numbers As DataTable
    Dim _num As String


    _numbers = New ArrayList
    _number = String.Empty

    For Each _number In Me.GetRawAccountNumbers(Me.rtxt_accounts.Text)
      If Not _number.Length <= GLB_MaxDigits Or _
      Not WSI.Common.ValidateFormat.RawNumber(_number) Then
        ' Msg: El formato no es correcto 
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2557), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _number)

        Return False
      Else
        _account_numbers.Append(_number & SEPARATOR)
      End If
    Next

    _table_numbers = DetectDuplicateNumbers(_numbers, _account_numbers.ToString.TrimEnd(SEPARATOR))

    If _numbers.Count <> _table_numbers.Rows.Count Then
      ' Msg: Se han detectado cuentas duplicadas.�Desea eliminarlas?
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2635), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
      mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then
        _account_numbers.Length = 0
        For Each _num In _numbers
          _account_numbers.Append(_num & SEPARATOR)
        Next
      End If
    End If

    GLB_Accept = True
    _number = String.Empty

    For Each _num In _numbers
      _number &= _num & SEPARATOR
    Next

    Me.uc_account_sel.MassiveSearchNumbers() = _number.TrimEnd(SEPARATOR)
    Me.uc_account_sel.MassiveSearchNumbersToEdit() = _account_numbers.ToString.TrimEnd(SEPARATOR)

    Return True
  End Function ' ValidateAndSaveAccounts

  ' PURPOSE: Get a list of account numbers
  '          
  '    - INPUT: String with the text to "clean up"
  '
  '    - OUTPUT:
  '              
  ' RETURNS: A list of account numbers
  '          
  ' NOTES
  '
  Private Function GetRawAccountNumbers(ByVal AccountText As String) As String()
    Dim _account_numbers As String()
    Dim _sep(0 To 1) As Char
    Dim _tmp_accounts As String

    _account_numbers = AccountText.Split()
    _tmp_accounts = String.Join(";", _account_numbers)
    _sep(0) = ";"
    _sep(1) = ","
    _account_numbers = _tmp_accounts.Split(_sep, StringSplitOptions.RemoveEmptyEntries)

    Return _account_numbers
  End Function ' GetRawAccountNumbers

  ' PURPOSE: Get the data account formated in four columns
  '          
  '    - INPUT: 
  '            String with the text
  '            Integer with the index
  '
  '    - OUTPUT:
  '              
  ' RETURNS: String with the text formated
  '          
  ' NOTES
  '
  Private Function GetFormatedText(ByVal Idx As Integer, ByVal Num As String) As String
    Dim _formated_text As String
    Dim _Mod As Integer

    _Mod = 4
    If Me.rb_card_number.Checked Then
      _Mod = 2
    End If

    If Math.IEEERemainder(Idx + 1, _Mod) Then
      _formated_text = Num.PadLeft(GLB_MaxDigits, SEPARATOR) & SEPARATOR
    Else
      _formated_text = Num.PadLeft(GLB_MaxDigits, SEPARATOR) & vbCr
      Application.DoEvents()
    End If

    Return _formated_text
  End Function

  ' PURPOSE: Set the account text removing duplicates
  '          
  '    - INPUT: String with the text
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Private Sub SetAccountText(ByVal Text As String, ByVal AskForDuplicates As Boolean)
    Dim _table_numbers As DataTable
    Dim _account_numbers As New StringBuilder
    Dim _numbers As ArrayList
    Dim _idx_num As Integer
    Dim _num As String
    Dim _is_duplicated As Boolean
    Dim _show_duplicated As Boolean

    _show_duplicated = True
    _numbers = New ArrayList

    Try

      _table_numbers = DetectDuplicateNumbers(_numbers, Text)
      Call Me.EnableControls(False)

      If _numbers.Count <> _table_numbers.Rows.Count Then
        If AskForDuplicates Then
          ' Msg: Se han detectado cuentas duplicadas.�Desea eliminarlas?
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2635), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
          mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then
            _show_duplicated = False
          End If
        End If
      End If

      Me.rtxt_accounts.Text = String.Empty

      If Not _show_duplicated Then
        _idx_num = 0

        For Each _num In _numbers
          Me.rtxt_accounts.AppendText(GetFormatedText(_idx_num, _num))
          _idx_num += 1
        Next
      Else
        For _idx_num = 0 To _table_numbers.Rows.Count - 1
          _num = _table_numbers.Rows(_idx_num)("Account").ToString
          _is_duplicated = _table_numbers.Rows(_idx_num)("Duplicate")

          Me.rtxt_accounts.SelectionColor = Drawing.Color.Black

          If _is_duplicated Then
            Me.rtxt_accounts.SelectionColor = Drawing.Color.Red
          End If

          Me.rtxt_accounts.AppendText(GetFormatedText(_idx_num, _num))
        Next
      End If

      Me.rtxt_accounts.SelectionStart = 0
      Me.rtxt_accounts.SelectionLength = 0

    Finally
      Call Me.EnableControls(True)
    End Try
  End Sub ' SetAccountText

  ' PURPOSE: To Enable/Disable the form controls
  '          
  '    - INPUT: Enabled as Boolean to set the control state
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Private Sub EnableControls(ByVal Enabled As Boolean)
    Me.btn_import_excel.Enabled = Enabled
    Me.btn_delete.Enabled = Enabled
    Me.btn_accept.Enabled = Enabled
    Me.btn_exit.Enabled = Enabled
  End Sub

  ' PURPOSE: Detect the duplicate numbers in the String
  '          
  '    - INPUT: 
  '            String with the text
  '            ArrayList to store
  '
  '    - OUTPUT:
  '              
  ' RETURNS: Datatable with the numbers and if they are duplicated or not
  '          
  ' NOTES
  '
  Private Function DetectDuplicateNumbers(ByRef Numbers As ArrayList, ByVal Text As String) As DataTable
    Dim _table As DataTable
    Dim _num As String
    Dim _duplicate As Boolean

    _table = New DataTable

    _table.Columns.Add("Account", GetType(String))
    _table.Columns.Add("Duplicate", GetType(Boolean))

    For Each _num In Me.GetRawAccountNumbers(Text)
      _duplicate = False

      If Numbers.Contains(_num) Then
        _duplicate = True
      Else
        Numbers.Add(_num)
      End If

      _table.Rows.Add(_num, _duplicate)
    Next

    Return _table
  End Function ' DetectDuplicateNumbers

  ' PURPOSE: Check if there are changes in the account list
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: 
  '          True - If the close operation is canceled
  '          False - If the close operation is accepted
  '          
  ' NOTES
  '
  Private Function CheckForChanges() As Boolean
    Dim _before As String
    Dim _after As String

    _before = Me.uc_account_sel.MassiveSearchNumbersToEdit
    _after = String.Join(" ", Me.GetRawAccountNumbers(Me.rtxt_accounts.Text))

    If String.Equals(_before, _after) Or GLB_Accept Then

      Return False
    Else
      ' Msg: Los cambios realizados en la lista de cuentas se perder�n �Est� seguro que quiere cancelar?
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2579), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO, _
        , Me.rtxt_accounts.Text) = ENUM_MB_RESULT.MB_RESULT_YES Then

        Return False
      End If
    End If

    Call Me.rtxt_accounts.Focus()    

    Return True
  End Function ' CheckForChanges

  ' PURPOSE: Import account numbers from excel
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Private Sub ImportFromExcel()
    Dim _open_file_dialog As New OpenFileDialog
    Dim _accounts As String
    Dim _excel As GUI_Reports.CLASS_EXCEL_DATA

    _open_file_dialog.Filter = "Excel File (*.xls, *.xlsx )| *.xls; *.xlsx"

    If _open_file_dialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
      _excel = New GUI_Reports.CLASS_EXCEL_DATA()
      _accounts = _excel.GetFirstColumnExcelData(_open_file_dialog.FileName, SEPARATOR)

      If Not String.IsNullOrEmpty(Me.rtxt_accounts.Text) Then
        _accounts = Me.rtxt_accounts.Text & SEPARATOR & _accounts
      End If

      Call Me.SetAccountText(_accounts, True)
    End If
  End Sub ' ImportFromExcel

  ' PURPOSE: To delete the text in the Riched TextBox
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Private Sub DeleteText()
    If Not String.IsNullOrEmpty(Me.rtxt_accounts.Text) Then
      ' Msg: �Est� seguro que quiere borrar la lista de cuentas?
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2578), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO, _
        , Me.rtxt_accounts.Text) = ENUM_MB_RESULT.MB_RESULT_YES Then
        Me.rtxt_accounts.Text = String.Empty
      End If
    End If
  End Sub ' DeleteText

  ' PURPOSE: To set the type number for massive search
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Private Sub SaveSelectedType()
    If Me.uc_account_sel.MassiveSearchType = GUI_Controls.uc_account_sel.ENUM_MASSIVE_SEARCH_TYPE.ID_ACCOUNT Then
      Me.rb_account_number.Checked = True
      Me.GLB_MaxDigits = 10
    Else
      Me.rb_card_number.Checked = True
      Me.GLB_MaxDigits = 20
    End If
  End Sub ' SaveSelectedType

#End Region ' Private Functions

#Region " Events "

  ' PURPOSE: To check changes and close the form
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Private Sub btn_exit_ClickEvent() Handles btn_exit.ClickEvent
    Call Me.Close()
  End Sub ' btn_exit_ClickEvent

  ' PURPOSE: To Check de format and pass the results to the uc_entry_field
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Private Sub btn_accept_ClickEvent() Handles btn_accept.ClickEvent
    If ValidateAndSaveAccounts() Then
      Call Me.Close()
    End If
  End Sub ' btn_accept_ClickEvent

  ' PURPOSE: Limited to numeric, commas, semicolons, enters and spaces characters
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  Private Sub rtxt_accounts_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles rtxt_accounts.KeyPress
    Me.rtxt_accounts.SelectionColor = Drawing.Color.Black

    If Asc(e.KeyChar) <> 8 _
    And e.KeyChar <> Microsoft.VisualBasic.ChrW(Keys.Return) _
    And e.KeyChar <> Microsoft.VisualBasic.ChrW(Keys.Space) _
    And e.KeyChar <> "," _
    And e.KeyChar <> ";" _
    And (Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57) Then
      e.Handled = True
    End If
  End Sub ' rtxt_accounts_KeyPress

  ' PURPOSE: To close the form if ESC button is pressed
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Private Sub frm_account_massive_search_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
    If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Escape) Then
      Call Me.Close()
    End If
  End Sub ' frm_account_massive_search_KeyPress

  ' PURPOSE: Delete text in RichTextBox
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Private Sub btn_delete_ClickEvent() Handles btn_delete.ClickEvent
    Call Me.DeleteText()
    Call Me.rtxt_accounts.Focus()
  End Sub ' btn_delete_ClickEvent

  ' PURPOSE: To import data from an Excel
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Private Sub btn_import_excel_ClickEvent() Handles btn_import_excel.ClickEvent
    Call Me.ImportFromExcel()
    Call Me.rtxt_accounts.Focus()
  End Sub ' btn_import_excel_ClickEvent

  ' PURPOSE: To update type value for massive search
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Private Sub rb_account_number_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rb_account_number.CheckedChanged
    If Me.rb_account_number.Checked Then
      Me.uc_account_sel.MassiveSearchType = GUI_Controls.uc_account_sel.ENUM_MASSIVE_SEARCH_TYPE.ID_ACCOUNT
      Me.rb_account_number.Checked = True
      Me.GLB_MaxDigits = 10
    Else
      Me.uc_account_sel.MassiveSearchType = GUI_Controls.uc_account_sel.ENUM_MASSIVE_SEARCH_TYPE.CARD_NUMBER
      Me.rb_card_number.Checked = True
      Me.GLB_MaxDigits = 20
    End If
  End Sub ' rb_account_number_CheckedChanged

#End Region ' Events

End Class