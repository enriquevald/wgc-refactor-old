<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_message
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Me.img = New System.Windows.Forms.PictureBox
    Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
    Me.lbl_message = New System.Windows.Forms.Label
    Me.pnl_buttons = New System.Windows.Forms.Panel
    Me.lbl_seconds = New System.Windows.Forms.Label
    Me.btn_cancel = New GUI_Controls.uc_button
    Me.Timer = New System.Windows.Forms.Timer(Me.components)
    CType(Me.img, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.TableLayoutPanel1.SuspendLayout()
    Me.pnl_buttons.SuspendLayout()
    Me.SuspendLayout()
    '
    'img
    '
    Me.img.Dock = System.Windows.Forms.DockStyle.Fill
    Me.img.Location = New System.Drawing.Point(0, 0)
    Me.img.Margin = New System.Windows.Forms.Padding(0)
    Me.img.Name = "img"
    Me.img.Size = New System.Drawing.Size(58, 52)
    Me.img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
    Me.img.TabIndex = 4
    Me.img.TabStop = False
    '
    'TableLayoutPanel1
    '
    Me.TableLayoutPanel1.ColumnCount = 2
    Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
    Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
    Me.TableLayoutPanel1.Controls.Add(Me.img, 0, 0)
    Me.TableLayoutPanel1.Controls.Add(Me.lbl_message, 1, 0)
    Me.TableLayoutPanel1.Controls.Add(Me.pnl_buttons, 1, 1)
    Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.TableLayoutPanel1.Location = New System.Drawing.Point(4, 4)
    Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
    Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
    Me.TableLayoutPanel1.RowCount = 2
    Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
    Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35.0!))
    Me.TableLayoutPanel1.Size = New System.Drawing.Size(400, 87)
    Me.TableLayoutPanel1.TabIndex = 5
    '
    'lbl_message
    '
    Me.lbl_message.Anchor = System.Windows.Forms.AnchorStyles.Left
    Me.lbl_message.AutoSize = True
    Me.lbl_message.Location = New System.Drawing.Point(61, 19)
    Me.lbl_message.Name = "lbl_message"
    Me.lbl_message.Size = New System.Drawing.Size(39, 13)
    Me.lbl_message.TabIndex = 5
    Me.lbl_message.Text = "xText"
    Me.lbl_message.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'pnl_buttons
    '
    Me.pnl_buttons.Controls.Add(Me.lbl_seconds)
    Me.pnl_buttons.Controls.Add(Me.btn_cancel)
    Me.pnl_buttons.Dock = System.Windows.Forms.DockStyle.Fill
    Me.pnl_buttons.Location = New System.Drawing.Point(58, 52)
    Me.pnl_buttons.Margin = New System.Windows.Forms.Padding(0)
    Me.pnl_buttons.Name = "pnl_buttons"
    Me.pnl_buttons.Size = New System.Drawing.Size(342, 35)
    Me.pnl_buttons.TabIndex = 6
    '
    'lbl_seconds
    '
    Me.lbl_seconds.Anchor = System.Windows.Forms.AnchorStyles.Left
    Me.lbl_seconds.AutoSize = True
    Me.lbl_seconds.Location = New System.Drawing.Point(4, 11)
    Me.lbl_seconds.Name = "lbl_seconds"
    Me.lbl_seconds.Size = New System.Drawing.Size(62, 13)
    Me.lbl_seconds.TabIndex = 8
    Me.lbl_seconds.Text = "xSeconds"
    Me.lbl_seconds.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'btn_cancel
    '
    Me.btn_cancel.Anchor = System.Windows.Forms.AnchorStyles.Right
    Me.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_cancel.Location = New System.Drawing.Point(249, 2)
    Me.btn_cancel.Name = "btn_cancel"
    Me.btn_cancel.Size = New System.Drawing.Size(90, 30)
    Me.btn_cancel.TabIndex = 7
    Me.btn_cancel.ToolTipped = False
    Me.btn_cancel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'Timer
    '
    '
    'frm_message
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(408, 95)
    Me.ControlBox = False
    Me.Controls.Add(Me.TableLayoutPanel1)
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_message"
    Me.ShowIcon = False
    Me.ShowInTaskbar = False
    Me.Text = "frm_message"
    Me.TopMost = True
    CType(Me.img, System.ComponentModel.ISupportInitialize).EndInit()
    Me.TableLayoutPanel1.ResumeLayout(False)
    Me.TableLayoutPanel1.PerformLayout()
    Me.pnl_buttons.ResumeLayout(False)
    Me.pnl_buttons.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents img As System.Windows.Forms.PictureBox
  Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
  Friend WithEvents lbl_message As System.Windows.Forms.Label
  Friend WithEvents pnl_buttons As System.Windows.Forms.Panel
  Friend WithEvents btn_cancel As GUI_Controls.uc_button
  Friend WithEvents lbl_seconds As System.Windows.Forms.Label
  Friend WithEvents Timer As System.Windows.Forms.Timer
End Class
