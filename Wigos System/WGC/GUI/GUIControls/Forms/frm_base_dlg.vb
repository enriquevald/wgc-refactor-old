'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_base_dlg.vb  
'
' DESCRIPTION:   Base Dialog form
'
' AUTHOR:        Andreu Juli�
'
' CREATION DATE: 21-MAR-2003
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-APR-2002  AJQ    Initial version
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls



Public Class frm_base_dlg
  Inherits frm_base

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents panel_buttons As System.Windows.Forms.Panel
  Friend WithEvents panel_public As System.Windows.Forms.Panel
  Friend WithEvents btn_custom_2 As GUI_Controls.uc_button
  Friend WithEvents btn_custom_1 As GUI_Controls.uc_button
  Friend WithEvents btn_custom_0 As GUI_Controls.uc_button
  Friend WithEvents panel_separator As System.Windows.Forms.Panel
  Friend WithEvents btn_delete As GUI_Controls.uc_button
  Friend WithEvents btn_ok As GUI_Controls.uc_button
  Friend WithEvents btn_cancel As GUI_Controls.uc_button
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
Me.panel_buttons = New System.Windows.Forms.Panel
Me.panel_public = New System.Windows.Forms.Panel
Me.btn_custom_2 = New GUI_Controls.uc_button
Me.btn_custom_1 = New GUI_Controls.uc_button
Me.btn_custom_0 = New GUI_Controls.uc_button
Me.panel_separator = New System.Windows.Forms.Panel
Me.btn_delete = New GUI_Controls.uc_button
Me.btn_ok = New GUI_Controls.uc_button
Me.btn_cancel = New GUI_Controls.uc_button
Me.panel_buttons.SuspendLayout()
Me.SuspendLayout()
'
'panel_buttons
'
Me.panel_buttons.Controls.Add(Me.panel_public)
Me.panel_buttons.Controls.Add(Me.btn_custom_2)
Me.panel_buttons.Controls.Add(Me.btn_custom_1)
Me.panel_buttons.Controls.Add(Me.btn_custom_0)
Me.panel_buttons.Controls.Add(Me.panel_separator)
Me.panel_buttons.Controls.Add(Me.btn_delete)
Me.panel_buttons.Controls.Add(Me.btn_ok)
Me.panel_buttons.Controls.Add(Me.btn_cancel)
Me.panel_buttons.Dock = System.Windows.Forms.DockStyle.Right
Me.panel_buttons.Location = New System.Drawing.Point(420, 4)
Me.panel_buttons.Name = "panel_buttons"
Me.panel_buttons.Size = New System.Drawing.Size(88, 389)
Me.panel_buttons.TabIndex = 2
'
'panel_public
'
Me.panel_public.Dock = System.Windows.Forms.DockStyle.Fill
Me.panel_public.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.panel_public.Location = New System.Drawing.Point(0, 0)
Me.panel_public.Name = "panel_public"
Me.panel_public.Size = New System.Drawing.Size(88, 179)
Me.panel_public.TabIndex = 0
'
'btn_custom_2
'
Me.btn_custom_2.DialogResult = System.Windows.Forms.DialogResult.None
Me.btn_custom_2.Dock = System.Windows.Forms.DockStyle.Bottom
Me.btn_custom_2.Location = New System.Drawing.Point(0, 179)
Me.btn_custom_2.Name = "btn_custom_2"
Me.btn_custom_2.Padding = New System.Windows.Forms.Padding(2)
Me.btn_custom_2.Size = New System.Drawing.Size(90, 30)
Me.btn_custom_2.TabIndex = 1
Me.btn_custom_2.ToolTipped = False
Me.btn_custom_2.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
'
'btn_custom_1
'
Me.btn_custom_1.DialogResult = System.Windows.Forms.DialogResult.None
Me.btn_custom_1.Dock = System.Windows.Forms.DockStyle.Bottom
Me.btn_custom_1.Location = New System.Drawing.Point(0, 209)
Me.btn_custom_1.Name = "btn_custom_1"
Me.btn_custom_1.Padding = New System.Windows.Forms.Padding(2)
Me.btn_custom_1.Size = New System.Drawing.Size(90, 30)
Me.btn_custom_1.TabIndex = 2
Me.btn_custom_1.ToolTipped = False
Me.btn_custom_1.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
'
'btn_custom_0
'
Me.btn_custom_0.DialogResult = System.Windows.Forms.DialogResult.None
Me.btn_custom_0.Dock = System.Windows.Forms.DockStyle.Bottom
Me.btn_custom_0.Location = New System.Drawing.Point(0, 239)
Me.btn_custom_0.Name = "btn_custom_0"
Me.btn_custom_0.Padding = New System.Windows.Forms.Padding(2)
Me.btn_custom_0.Size = New System.Drawing.Size(90, 30)
Me.btn_custom_0.TabIndex = 3
Me.btn_custom_0.ToolTipped = False
Me.btn_custom_0.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
'
'panel_separator
'
Me.panel_separator.Dock = System.Windows.Forms.DockStyle.Bottom
Me.panel_separator.Location = New System.Drawing.Point(0, 269)
Me.panel_separator.Name = "panel_separator"
Me.panel_separator.Size = New System.Drawing.Size(88, 30)
Me.panel_separator.TabIndex = 4
'
'btn_delete
'
Me.btn_delete.DialogResult = System.Windows.Forms.DialogResult.None
Me.btn_delete.Dock = System.Windows.Forms.DockStyle.Bottom
Me.btn_delete.Location = New System.Drawing.Point(0, 299)
Me.btn_delete.Name = "btn_delete"
Me.btn_delete.Padding = New System.Windows.Forms.Padding(2)
Me.btn_delete.Size = New System.Drawing.Size(90, 30)
Me.btn_delete.TabIndex = 5
Me.btn_delete.ToolTipped = False
Me.btn_delete.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
'
'btn_ok
'
Me.btn_ok.DialogResult = System.Windows.Forms.DialogResult.None
Me.btn_ok.Dock = System.Windows.Forms.DockStyle.Bottom
Me.btn_ok.Location = New System.Drawing.Point(0, 329)
Me.btn_ok.Name = "btn_ok"
Me.btn_ok.Padding = New System.Windows.Forms.Padding(2)
Me.btn_ok.Size = New System.Drawing.Size(90, 30)
Me.btn_ok.TabIndex = 6
Me.btn_ok.ToolTipped = False
Me.btn_ok.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
'
'btn_cancel
'
Me.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.None
Me.btn_cancel.Dock = System.Windows.Forms.DockStyle.Bottom
Me.btn_cancel.Location = New System.Drawing.Point(0, 359)
Me.btn_cancel.Name = "btn_cancel"
Me.btn_cancel.Padding = New System.Windows.Forms.Padding(2)
Me.btn_cancel.Size = New System.Drawing.Size(90, 30)
Me.btn_cancel.TabIndex = 7
Me.btn_cancel.ToolTipped = False
Me.btn_cancel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
'
'frm_base_dlg
'
Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
Me.ClientSize = New System.Drawing.Size(512, 397)
Me.Controls.Add(Me.panel_buttons)
Me.Name = "frm_base_dlg"
Me.Text = "frm_base_dlg"
Me.panel_buttons.ResumeLayout(False)
Me.ResumeLayout(False)

End Sub

#End Region

End Class
