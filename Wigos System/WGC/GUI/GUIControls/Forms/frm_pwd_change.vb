'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_pwd_change.vb  
'
' DESCRIPTION:   form to change password
'
' AUTHOR:        Yumar
'
' CREATION DATE: 17-APR-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 17-APR-2002  Y--    Initial version
' 10-MAY-2012  JCM    Fixed Bug #191: The ef_passw allows '�' and frm_login don't allow
' 29-AUG-2012  JCM    Added non alpha numeric chars for password
'                     Added case sensitive password
' 10-JUL-2013  DHA    Changed the length from the field 'Password' from 15 to 22 characters
' 01-SEP-2017  DPC    WIGOS-4833: [Ticket #8311] Usuario de corporativo bloqueado
'-------------------------------------------------------------------

Option Strict On
Option Explicit On 

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports System.Drawing
Imports WSI.Common

Friend Class frm_pwd_change
  Inherits frm_base

#Region " Form Variables "
  ' LOCAL CONSTANTS
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Private Const MIN_PASSWORD_LENGTH As Integer = 1

  ' Form Variables
  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  ' Used the hold the Module Id
  Private m_gui_id As ENUM_GUI
  Private m_user_name As String
  Private m_gui_name As String
  Friend WithEvents ef_name As GUI_Controls.uc_entry_field

  ' holds the result of the process
  Private GLB_PwdChangeOk As Boolean
#End Region

#Region " Windows Form Designer generated code "

  Public Sub New()

    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Private WithEvents panel_buttons As System.Windows.Forms.Panel
  Private WithEvents btn_ok As GUI_Controls.uc_button
  Private WithEvents btn_cancel As GUI_Controls.uc_button
  Friend WithEvents ef_new_pass As GUI_Controls.uc_entry_field
  Friend WithEvents ef_new_pass_confirm As GUI_Controls.uc_entry_field
  Friend WithEvents ef_current_pass As GUI_Controls.uc_entry_field
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.panel_buttons = New System.Windows.Forms.Panel
    Me.btn_ok = New GUI_Controls.uc_button
    Me.btn_cancel = New GUI_Controls.uc_button
    Me.ef_new_pass = New GUI_Controls.uc_entry_field
    Me.ef_new_pass_confirm = New GUI_Controls.uc_entry_field
    Me.ef_current_pass = New GUI_Controls.uc_entry_field
    Me.ef_name = New GUI_Controls.uc_entry_field
    Me.panel_buttons.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_buttons
    '
    Me.panel_buttons.Controls.Add(Me.btn_ok)
    Me.panel_buttons.Controls.Add(Me.btn_cancel)
    Me.panel_buttons.Dock = System.Windows.Forms.DockStyle.Right
    Me.panel_buttons.Location = New System.Drawing.Point(406, 4)
    Me.panel_buttons.Name = "panel_buttons"
    Me.panel_buttons.Size = New System.Drawing.Size(88, 207)
    Me.panel_buttons.TabIndex = 6
    '
    'btn_ok
    '
    Me.btn_ok.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_ok.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_ok.Location = New System.Drawing.Point(0, 147)
    Me.btn_ok.Name = "btn_ok"
    Me.btn_ok.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_ok.Size = New System.Drawing.Size(90, 30)
    Me.btn_ok.TabIndex = 4
    Me.btn_ok.ToolTipped = False
    Me.btn_ok.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_cancel
    '
    Me.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_cancel.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_cancel.Location = New System.Drawing.Point(0, 177)
    Me.btn_cancel.Name = "btn_cancel"
    Me.btn_cancel.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_cancel.Size = New System.Drawing.Size(90, 30)
    Me.btn_cancel.TabIndex = 5
    Me.btn_cancel.ToolTipped = False
    Me.btn_cancel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'ef_new_pass
    '
    Me.ef_new_pass.DoubleValue = 0
    Me.ef_new_pass.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_new_pass.IntegerValue = 0
    Me.ef_new_pass.IsReadOnly = False
    Me.ef_new_pass.Location = New System.Drawing.Point(16, 110)
    Me.ef_new_pass.Name = "ef_new_pass"
    Me.ef_new_pass.Size = New System.Drawing.Size(384, 33)
    Me.ef_new_pass.SufixText = "Sufix Text"
    Me.ef_new_pass.SufixTextVisible = True
    Me.ef_new_pass.TabIndex = 2
    Me.ef_new_pass.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_new_pass.TextValue = ""
    Me.ef_new_pass.TextWidth = 170
    Me.ef_new_pass.Value = ""
    '
    'ef_new_pass_confirm
    '
    Me.ef_new_pass_confirm.DoubleValue = 0
    Me.ef_new_pass_confirm.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_new_pass_confirm.IntegerValue = 0
    Me.ef_new_pass_confirm.IsReadOnly = False
    Me.ef_new_pass_confirm.Location = New System.Drawing.Point(16, 155)
    Me.ef_new_pass_confirm.Name = "ef_new_pass_confirm"
    Me.ef_new_pass_confirm.Size = New System.Drawing.Size(384, 33)
    Me.ef_new_pass_confirm.SufixText = "Sufix Text"
    Me.ef_new_pass_confirm.SufixTextVisible = True
    Me.ef_new_pass_confirm.TabIndex = 3
    Me.ef_new_pass_confirm.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_new_pass_confirm.TextValue = ""
    Me.ef_new_pass_confirm.TextWidth = 170
    Me.ef_new_pass_confirm.Value = ""
    '
    'ef_current_pass
    '
    Me.ef_current_pass.DoubleValue = 0
    Me.ef_current_pass.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_current_pass.IntegerValue = 0
    Me.ef_current_pass.IsReadOnly = False
    Me.ef_current_pass.Location = New System.Drawing.Point(16, 65)
    Me.ef_current_pass.Name = "ef_current_pass"
    Me.ef_current_pass.Size = New System.Drawing.Size(384, 33)
    Me.ef_current_pass.SufixText = "Sufix Text"
    Me.ef_current_pass.SufixTextVisible = True
    Me.ef_current_pass.TabIndex = 1
    Me.ef_current_pass.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_current_pass.TextValue = ""
    Me.ef_current_pass.TextWidth = 170
    Me.ef_current_pass.Value = ""
    '
    'ef_name
    '
    Me.ef_name.DoubleValue = 0
    Me.ef_name.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_name.IntegerValue = 0
    Me.ef_name.IsReadOnly = True
    Me.ef_name.Location = New System.Drawing.Point(16, 26)
    Me.ef_name.Name = "ef_name"
    Me.ef_name.Size = New System.Drawing.Size(384, 33)
    Me.ef_name.SufixText = "Sufix Text"
    Me.ef_name.SufixTextVisible = True
    Me.ef_name.TabIndex = 7
    Me.ef_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name.TextValue = ""
    Me.ef_name.TextWidth = 170
    Me.ef_name.Value = ""
    '
    'frm_pwd_change
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.CancelButton = Me.btn_cancel
    Me.ClientSize = New System.Drawing.Size(498, 215)
    Me.Controls.Add(Me.ef_name)
    Me.Controls.Add(Me.ef_current_pass)
    Me.Controls.Add(Me.ef_new_pass_confirm)
    Me.Controls.Add(Me.ef_new_pass)
    Me.Controls.Add(Me.panel_buttons)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.KeyPreview = True
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_pwd_change"
    Me.ShowInTaskbar = False
    Me.Text = "frm_pwd_change"
    Me.TopMost = True
    Me.panel_buttons.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Events "

  ' PURPOSE: Cancel Password change process.
  '
  '    - INPUT:
  '              None
  '    - OUTPUT:
  '              None
  '
  ' RETURNS:
  '          None
  ' NOTES
  '
  Private Sub btn_cancel_ClickEvent() Handles btn_cancel.ClickEvent
    Me.Hide()
  End Sub

  ' PURPOSE: gets and checks the parameters entered by the user in the Password Change Form.
  '          Check the currrent and the new password via the function: Check_Gui_Pwd and Insert_gui_pwd
  '          in the Package LK_ENCRYPT_PKG
  '         
  '    - INPUT:
  '              None
  '    - OUTPUT:
  '              None
  '
  ' RETURNS:
  '          None
  ' NOTES
  '
  Private Sub btn_ok_ClickEvent() Handles btn_ok.ClickEvent
    'form input
    Dim current_password As String
    Dim new_password As String
    Dim new_password_confirm As String
    Dim rc As Boolean
    Dim cm_rc As Integer
    'Dim user As String
    Dim nls_param1 As String
    Dim ctx As Integer
    Dim _pwd_policy As PasswordPolicy
 
    _pwd_policy = New PasswordPolicy()
    

    'check if the user has entered at least one character as password
    nls_param1 = CStr(WSI.Common.PasswordPolicy.MAX_PASSWORD_LENGTH_CONST)
    If Len(ef_current_pass.TextValueWithoutTrim) < MIN_PASSWORD_LENGTH _
       Or Len(ef_current_pass.TextValueWithoutTrim) > WSI.Common.PasswordPolicy.MAX_PASSWORD_LENGTH_CONST Then
      '107. Invalid password
      GLB_NLS_GUI_CONTROLS.MsgBox(107, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      'set the focus here
      Call ResetEntries()
      ef_new_pass.Focus()
      Return

    End If

    ' Read NLS for PWD Not valid according PWD Policy 



    If Len(ef_new_pass.TextValueWithoutTrim) < MIN_PASSWORD_LENGTH _
       Or Len(ef_new_pass.TextValueWithoutTrim) > WSI.Common.PasswordPolicy.MAX_PASSWORD_LENGTH_CONST Then
      '104. Invalid new password
      Call NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1218), _pwd_policy.MessagePasswordPolicyInvalid(), ENUM_MB_TYPE.MB_TYPE_ERROR)

      'set the focus here
      Call ResetEntries()
      ef_new_pass.Focus()
      Return

    End If

    If Len(ef_new_pass_confirm.TextValueWithoutTrim) < MIN_PASSWORD_LENGTH _
       Or Len(ef_new_pass_confirm.TextValueWithoutTrim) > WSI.Common.PasswordPolicy.MAX_PASSWORD_LENGTH_CONST Then
      '104. Invalid new password
      Call NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1218), _pwd_policy.MessagePasswordPolicyInvalid(), ENUM_MB_TYPE.MB_TYPE_ERROR)

      'set the focus here
      Call ResetEntries()
      ef_new_pass.Focus()
      Return

    End If

    'get parameters entered by the user in the Login form
    current_password = ef_current_pass.TextValueWithoutTrim
    new_password = _pwd_policy.ParsePassword(ef_new_pass.TextValueWithoutTrim)
    new_password_confirm = _pwd_policy.ParsePassword(ef_new_pass_confirm.TextValueWithoutTrim)


    ' APB (12-MAR-2009)
    ' Restrictions on passwords are lifted (GUI allows setting any value anyway through User Edition screen)
    ''IRP 24-OCT-2002 Check sintaxi of new Pasword
    'user = tf_username.Value
    'If BadFormatPassword(user, new_password) Then
    '  'Invalid format of password
    '  GLB_NLS_GUI_CONTROLS.MsgBox(121, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

    '  'set the focus in the new password
    '  Call ResetEntries()
    '  Return

    'End If



    'check the password confirmation
    If StrComp(new_password, new_password_confirm, CompareMethod.Binary) <> 0 Then
      'Invalid password, New password and confirmation mismatch
      GLB_NLS_GUI_CONTROLS.MsgBox(108, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      'set the focus in the new password
      Call ResetEntries()
      ef_new_pass.Focus()
      Return

    End If


    ' check current password via pkg
    Try

      'XVV 17/04/2007
      'Comment this function, because are obsolete (Oracle version)
      'GUI_SqlCtxAlloc(ctx)


      Using _db_trx As New WSI.Common.DB_TRX
        If _pwd_policy.VerifyCurrentPassword(CurrentUser.Id, current_password, _db_trx.SqlTransaction) Then
          cm_rc = ENUM_COMMON_RC.COMMON_OK
        Else
          cm_rc = ENUM_COMMON_RC.COMMON_ERROR_PASSWORD
        End If

        _db_trx.Commit()
      End Using

      If cm_rc <> CInt(ENUM_COMMON_RC.COMMON_OK) Then
        'Invalid password, the user has to enter a valid password
        GLB_NLS_GUI_CONTROLS.MsgBox(107, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

        Call ResetEntries()
        ef_new_pass.Focus()
        Return
      End If

      ' check new password via pkg
      If _pwd_policy.CheckPasswordRules(Environment.MachineName, CurrentUser.Name, CurrentUser.Id, new_password) Then
        cm_rc = ENUM_COMMON_RC.COMMON_OK
      Else
        cm_rc = ENUM_COMMON_RC.COMMON_ERROR_PASSWORD
      End If

      If cm_rc <> CInt(ENUM_COMMON_RC.COMMON_OK) Then
        'Invalid password, the user has to enter a valid password
        Call NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1218), _pwd_policy.MessagePasswordPolicyInvalid(), ENUM_MB_TYPE.MB_TYPE_ERROR)

        Call ResetEntries()
        ef_new_pass.Focus()
        Return
      End If

      rc = GLB_CurrentUser.ChangeUserPassword(GLB_CurrentUser.Id, new_password, ctx)
      If rc = False Then
        '104. New password Invalid
        Call NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1218), _pwd_policy.MessagePasswordPolicyInvalid(), ENUM_MB_TYPE.MB_TYPE_ERROR)

        Call ResetEntries()
        ef_new_pass.Focus()
        Return

      End If

      GLB_PwdChangeOk = True

      If GLB_PwdChangeOk Then
        Dim auditor_data As CLASS_AUDITOR_DATA
        auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_LOGIN_LOGOUT)
        auditor_data.SetName(GLB_NLS_GUI_CONTROLS.Id(268), GLB_NLS_GUI_CONTROLS.GetString(268))
        auditor_data.Notify(CurrentUser.GuiId, CurrentUser.Id, CurrentUser.Name, CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, ctx)
      End If

      Me.Hide()

    Finally
      'XVV 17/04/2007
      'Comment this function, because are obsolete (Oracle version)
      'GUI_SqlCtxRelease(ctx)
    End Try

  End Sub

  Private Sub frm_pwd_change_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
    If e.KeyChar = Chr(13) And (ActiveControl.Name = ef_current_pass.Name Or ActiveControl.Name = ef_new_pass.Name Or ActiveControl.Name = ef_new_pass_confirm.Name) Then
      System.Windows.Forms.SendKeys.Send("{TAB}")
    End If
  End Sub

#End Region

#Region " Utilities "

  ' PURPOSE: Cleans all entries controls in the Login form. In addition,
  '          sets the initial values, in this case, the last user logged in
  '
  '    - INPUT:
  '              UserName. Last user connected, passed as input from the caller (GUIs)
  '                        when null, ask the login user to enter its UserName
  '    - OUTPUT:
  '              None
  '
  ' RETURNS:
  '          None
  ' NOTES
  ' setscreen
  Private Sub ResetEntries()

    ef_current_pass.Value = ""
    ef_new_pass.Value = ""
    ef_new_pass_confirm.Value = ""

    'check if current password entrance has to be skiped, after a login, the user is
    'requested to change the password, in this case there is no need to ask the user
    'to enter the current password again
    If GLB_CurrentUser.PwdChangeRequested = True Then
      ef_current_pass.IsReadOnly = True
      ef_current_pass.Value = GLB_CurrentUser.Password
      ef_new_pass.Value = ""
      ef_new_pass.Select()
    Else
      ef_current_pass.IsReadOnly = False
      ef_current_pass.Value = ""
      ef_current_pass.Select()
    End If

  End Sub

#End Region ' Utility Functions

#Region " Overrides "

  ' PURPOSE: Initializes the window title and all the
  '          entry fields
  '          
  '    - INPUT: None
  '
  '    - OUTPUT: None
  '              
  ' RETURNS: None
  '          
  ' NOTES
  ' 
  Protected Overrides Sub GUI_InitControls()

    MyBase.GUI_InitControls()

    'Add any initialization after the MyBase.GUI_InitControls() call

    'set the window title, adding the GUI name if that is possible
    ' format:  GUI Name - Form Name
    If m_gui_name = vbNullString Then
      Text = GLB_NLS_GUI_CONTROLS.GetString(268)
    Else
      Text = m_gui_name & " - " & GLB_NLS_GUI_CONTROLS.GetString(268)
    End If

    ef_name.Text = GLB_NLS_GUI_CONTROLS.GetString(260)
    ef_name.Value = m_user_name

    'cleans the passwords
    ef_current_pass.Text = GLB_NLS_GUI_CONTROLS.GetString(269)
    ef_current_pass.Password = True
    ef_current_pass.OnlyUpperCase = False
    ef_current_pass.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, WSI.Common.PasswordPolicy.MAX_PASSWORD_LENGTH_CONST)

    ef_new_pass.Text = GLB_NLS_GUI_CONTROLS.GetString(270)
    ef_new_pass.Password = True
    ef_new_pass.OnlyUpperCase = False
    ef_new_pass.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, WSI.Common.PasswordPolicy.MAX_PASSWORD_LENGTH_CONST)

    ef_new_pass_confirm.Text = GLB_NLS_GUI_CONTROLS.GetString(271)
    ef_new_pass_confirm.Password = True
    ef_new_pass_confirm.OnlyUpperCase = False
    ef_new_pass_confirm.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, WSI.Common.PasswordPolicy.MAX_PASSWORD_LENGTH_CONST)

    'Init buttons
    btn_ok.Text = GLB_NLS_GUI_CONTROLS.GetString(1)
    btn_cancel.Text = GLB_NLS_GUI_CONTROLS.GetString(2)

    ' reset fields
    Call ResetEntries()

  End Sub

  ' PURPOSE: Given a type, the function returns if it needs to be audited
  '
  '  PARAMS:
  '     - INPUT:
  '         - AuditType: AUDIT_FLAGS that wants to be audited
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_HasToBeAudited(ByVal AuditType As AUDIT_FLAGS) As Boolean

    Return False

  End Function
#End Region

#Region " Password Change Form Entry Point "

  ' PURPOSE: Cleans all entries controls in the Password change form. In addition,
  '          sets the initial values, that is the current user
  '
  '    - INPUT:
  '              GUIId. Identifier of the GUI
  '              GUIName. Current user
  '              Mode: 
  '                - ENUM_PWD_CHANGE_MODE.ASK_CURRENT_PWD: ask the user for current password
  '                                                        used when changing password from the menu
  '                - ENUM_PWD_CHANGE_MODE.SKIP_CURRENT_PWD: doesn't ask the user for the current password
  '                                                        used when changing password from the Login
  '
  '    - OUTPUT:
  '              None:
  '
  ' RETURNS:
  '          TRUE:  password change form returned OK, retrieving current and new password
  '          FALSE: password change aborted by the user.
  ' NOTES
  ' New, do not delete
  Public Function PwdChange(ByVal GUIId As ENUM_GUI, _
                            Optional ByVal GUIName As String = "") As Boolean



    ' check if the user is logged
    If CurrentUser.IsLogged = False Then
      Return False
    End If

    ' initializes the Globals
    GLB_PwdChangeOk = False
    m_gui_id = GUIId
    m_gui_name = GUIName
    m_user_name = GLB_CurrentUser.Name

    ' Opens the password change window
    Call Me.Display(True)

    Return GLB_PwdChangeOk

  End Function
  ' PURPOSE: Check format of Password
  '
  '    - INPUT:
  '              name
  '              password                
  '
  ' RETURNS:
  '          an boolean indicating if the function is bad
  ' NOTES
  '           1. the password is less than 6 characters in length.
  '           2. the password does not contain at least 2 numeric characters.
  '           3. the password does not contain at least 2 letter characters.
  '           4. the password contains the username.
  '           5. the username contains the password.
  '
  Private Function BadFormatPassword(ByVal name As String, _
                                     ByVal password As String) As Boolean


    If Len(password) < 6 _
       Or InStr(name, password) > 0 _
       Or InStr(password, name) > 0 _
       Or Not (password Like "*[!0-9]*[!0-9]*") _
       Or Not (password Like "*[0-9]*[0-9]*") Then

      BadFormatPassword = True
    Else
      BadFormatPassword = False
    End If


  End Function

#End Region ' Password Change Form Entry Point

  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_COMMON_FORM.COMMON_FORM_PWD_CHANGE

    '----------------------------------------------------------
    'XVV 13/04/2007
    'Assign Icon to active form 
    'OLD version, use GetExecutingAssembly, but in Class libary is necessary GetEntreAssembly to recover ICON Resource

  End Sub
End Class
