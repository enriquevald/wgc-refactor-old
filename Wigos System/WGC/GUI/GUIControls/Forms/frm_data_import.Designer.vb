<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_data_import
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_data_import))
    Me.wb_excel_imported = New SpreadsheetGear.Windows.Forms.WorkbookView()
    Me.flp_fields_to_import = New System.Windows.Forms.FlowLayoutPanel()
    Me.pnl_buttons = New System.Windows.Forms.Panel()
    Me.btn_cancel = New System.Windows.Forms.Button()
    Me.btn_ok = New System.Windows.Forms.Button()
    Me.lbl_description = New System.Windows.Forms.Label()
    Me.lbl_infomation = New System.Windows.Forms.Label()
    Me.pnl_buttons.SuspendLayout()
    Me.SuspendLayout()
    '
    'wb_excel_imported
    '
    Me.wb_excel_imported.AllowEditCommands = False
    Me.wb_excel_imported.FormulaBar = Nothing
    Me.wb_excel_imported.Location = New System.Drawing.Point(5, 7)
    Me.wb_excel_imported.Name = "wb_excel_imported"
    Me.wb_excel_imported.Size = New System.Drawing.Size(974, 473)
    Me.wb_excel_imported.TabIndex = 0
    Me.wb_excel_imported.WorkbookSetState = resources.GetString("wb_excel_imported.WorkbookSetState")
    '
    'flp_fields_to_import
    '
    Me.flp_fields_to_import.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.flp_fields_to_import.AutoScroll = True
    Me.flp_fields_to_import.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
    Me.flp_fields_to_import.Location = New System.Drawing.Point(5, 549)
    Me.flp_fields_to_import.Name = "flp_fields_to_import"
    Me.flp_fields_to_import.Size = New System.Drawing.Size(974, 135)
    Me.flp_fields_to_import.TabIndex = 1
    '
    'pnl_buttons
    '
    Me.pnl_buttons.Controls.Add(Me.btn_cancel)
    Me.pnl_buttons.Controls.Add(Me.btn_ok)
    Me.pnl_buttons.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.pnl_buttons.Location = New System.Drawing.Point(5, 716)
    Me.pnl_buttons.Name = "pnl_buttons"
    Me.pnl_buttons.Size = New System.Drawing.Size(974, 31)
    Me.pnl_buttons.TabIndex = 2
    '
    'btn_cancel
    '
    Me.btn_cancel.Anchor = System.Windows.Forms.AnchorStyles.Right
    Me.btn_cancel.Location = New System.Drawing.Point(881, 0)
    Me.btn_cancel.Name = "btn_cancel"
    Me.btn_cancel.Size = New System.Drawing.Size(90, 30)
    Me.btn_cancel.TabIndex = 1
    Me.btn_cancel.Text = "xCancel"
    Me.btn_cancel.UseVisualStyleBackColor = True
    '
    'btn_ok
    '
    Me.btn_ok.Anchor = System.Windows.Forms.AnchorStyles.Right
    Me.btn_ok.Location = New System.Drawing.Point(785, 0)
    Me.btn_ok.Name = "btn_ok"
    Me.btn_ok.Size = New System.Drawing.Size(90, 30)
    Me.btn_ok.TabIndex = 0
    Me.btn_ok.Text = "xOk"
    Me.btn_ok.UseVisualStyleBackColor = True
    '
    'lbl_description
    '
    Me.lbl_description.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_description.Location = New System.Drawing.Point(8, 519)
    Me.lbl_description.Name = "lbl_description"
    Me.lbl_description.Size = New System.Drawing.Size(968, 27)
    Me.lbl_description.TabIndex = 5
    Me.lbl_description.Text = "xFormat: "
    '
    'lbl_infomation
    '
    Me.lbl_infomation.AutoSize = True
    Me.lbl_infomation.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_infomation.Location = New System.Drawing.Point(8, 498)
    Me.lbl_infomation.Name = "lbl_infomation"
    Me.lbl_infomation.Size = New System.Drawing.Size(93, 13)
    Me.lbl_infomation.TabIndex = 6
    Me.lbl_infomation.Text = "xInformation"
    '
    'frm_data_import
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(984, 751)
    Me.Controls.Add(Me.lbl_infomation)
    Me.Controls.Add(Me.lbl_description)
    Me.Controls.Add(Me.pnl_buttons)
    Me.Controls.Add(Me.flp_fields_to_import)
    Me.Controls.Add(Me.wb_excel_imported)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_data_import"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.ShowInTaskbar = False
    Me.Text = "frm_data_import"
    Me.pnl_buttons.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents flp_fields_to_import As System.Windows.Forms.FlowLayoutPanel
  Friend WithEvents pnl_buttons As System.Windows.Forms.Panel
  Friend WithEvents btn_ok As System.Windows.Forms.Button
  Friend WithEvents btn_cancel As System.Windows.Forms.Button
  Friend WithEvents lbl_description As System.Windows.Forms.Label
  Private WithEvents wb_excel_imported As SpreadsheetGear.Windows.Forms.WorkbookView
  Friend WithEvents lbl_infomation As System.Windows.Forms.Label
End Class
