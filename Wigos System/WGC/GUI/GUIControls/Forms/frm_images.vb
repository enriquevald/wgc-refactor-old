'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_images.vb
' DESCRIPTION:   
' AUTHOR:        Marcos Piedra Osuna
' CREATION DATE: 21-MAY-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 21-MAY-2012  MPO    Initial version
' -------------------------------------------------------------------

Imports System.Drawing
Imports System.Windows.Forms

Public Class frm_images

#Region "Members"

  Dim m_result As System.Windows.Forms.DialogResult

#End Region

#Region "Properties"

  Public Property Image() As Image
    Get
      Return Me.img_image.Image
    End Get
    Set(ByVal value As Image)
      Me.img_image.Image = value
    End Set
  End Property

  Public Property IconImage() As Image
    Get
      Return Me.img_icon.Image
    End Get
    Set(ByVal value As Image)
      Me.img_icon.Image = value
    End Set
  End Property

#End Region

#Region "Private"

  Private Sub InitControl()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(720)
    Me.tab_icon.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(719)
    Me.tab_image.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(687)
    btn_accept.Text = GLB_NLS_GUI_CONTROLS.GetString(1)
    btn_cancel.Text = GLB_NLS_GUI_CONTROLS.GetString(2)

  End Sub

#End Region

#Region "Public"

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

    InitControl()

  End Sub

  Public Shadows Function ShowDialog() As System.Windows.Forms.DialogResult

    m_result = Windows.Forms.DialogResult.Cancel

    MyBase.ShowDialog()

    Return m_result

  End Function

  Public Sub SelectTabImage()
    tab_main.SelectedTab = Me.tab_image
  End Sub

  Public Sub SelectTabIcon()
    tab_main.SelectedTab = Me.tab_icon
  End Sub

#End Region

#Region "Events"

  Private Sub btn_cancel_ClickEvent() Handles btn_cancel.ClickEvent
    Me.Close()
  End Sub

  Private Sub btn_accept_ClickEvent() Handles btn_accept.ClickEvent
    Me.Close()
    m_result = Windows.Forms.DialogResult.OK
  End Sub

#End Region

End Class
