<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_account_massive_search
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.btn_import_excel = New GUI_Controls.uc_button
    Me.btn_accept = New GUI_Controls.uc_button
    Me.btn_exit = New GUI_Controls.uc_button
    Me.rtxt_accounts = New System.Windows.Forms.RichTextBox
    Me.btn_delete = New GUI_Controls.uc_button
    Me.rb_card_number = New System.Windows.Forms.RadioButton
    Me.rb_account_number = New System.Windows.Forms.RadioButton
    Me.gb_buscar_por = New System.Windows.Forms.GroupBox
    Me.gb_buscar_por.SuspendLayout()
    Me.SuspendLayout()
    '
    'btn_import_excel
    '
    Me.btn_import_excel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_import_excel.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_import_excel.Location = New System.Drawing.Point(350, 13)
    Me.btn_import_excel.Name = "btn_import_excel"
    Me.btn_import_excel.Size = New System.Drawing.Size(90, 30)
    Me.btn_import_excel.TabIndex = 2
    Me.btn_import_excel.ToolTipped = False
    Me.btn_import_excel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_accept
    '
    Me.btn_accept.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_accept.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_accept.Location = New System.Drawing.Point(350, 324)
    Me.btn_accept.Name = "btn_accept"
    Me.btn_accept.Size = New System.Drawing.Size(90, 30)
    Me.btn_accept.TabIndex = 4
    Me.btn_accept.ToolTipped = False
    Me.btn_accept.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_exit
    '
    Me.btn_exit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_exit.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_exit.Location = New System.Drawing.Point(350, 360)
    Me.btn_exit.Name = "btn_exit"
    Me.btn_exit.Size = New System.Drawing.Size(90, 30)
    Me.btn_exit.TabIndex = 5
    Me.btn_exit.ToolTipped = False
    Me.btn_exit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'rtxt_accounts
    '
    Me.rtxt_accounts.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.rtxt_accounts.Font = New System.Drawing.Font("Lucida Console", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.rtxt_accounts.Location = New System.Drawing.Point(0, -1)
    Me.rtxt_accounts.Name = "rtxt_accounts"
    Me.rtxt_accounts.Size = New System.Drawing.Size(335, 402)
    Me.rtxt_accounts.TabIndex = 1
    Me.rtxt_accounts.Text = ""
    '
    'btn_delete
    '
    Me.btn_delete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_delete.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_delete.Location = New System.Drawing.Point(350, 288)
    Me.btn_delete.Name = "btn_delete"
    Me.btn_delete.Size = New System.Drawing.Size(90, 30)
    Me.btn_delete.TabIndex = 3
    Me.btn_delete.ToolTipped = False
    Me.btn_delete.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'rb_card_number
    '
    Me.rb_card_number.AutoSize = True
    Me.rb_card_number.Location = New System.Drawing.Point(6, 46)
    Me.rb_card_number.Name = "rb_card_number"
    Me.rb_card_number.Size = New System.Drawing.Size(101, 17)
    Me.rb_card_number.TabIndex = 1
    Me.rb_card_number.TabStop = True
    Me.rb_card_number.Text = "RadioButton2"
    Me.rb_card_number.UseVisualStyleBackColor = True
    '
    'rb_account_number
    '
    Me.rb_account_number.AutoSize = True
    Me.rb_account_number.Location = New System.Drawing.Point(6, 22)
    Me.rb_account_number.Name = "rb_account_number"
    Me.rb_account_number.Size = New System.Drawing.Size(101, 17)
    Me.rb_account_number.TabIndex = 0
    Me.rb_account_number.TabStop = True
    Me.rb_account_number.Text = "RadioButton1"
    Me.rb_account_number.UseVisualStyleBackColor = True
    '
    'gb_buscar_por
    '
    Me.gb_buscar_por.Controls.Add(Me.rb_card_number)
    Me.gb_buscar_por.Controls.Add(Me.rb_account_number)
    Me.gb_buscar_por.Location = New System.Drawing.Point(350, 63)
    Me.gb_buscar_por.Name = "gb_buscar_por"
    Me.gb_buscar_por.Size = New System.Drawing.Size(90, 72)
    Me.gb_buscar_por.TabIndex = 6
    Me.gb_buscar_por.TabStop = False
    Me.gb_buscar_por.Text = "GroupBox1"
    '
    'frm_account_massive_search
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(454, 400)
    Me.Controls.Add(Me.gb_buscar_por)
    Me.Controls.Add(Me.btn_delete)
    Me.Controls.Add(Me.btn_import_excel)
    Me.Controls.Add(Me.btn_accept)
    Me.Controls.Add(Me.btn_exit)
    Me.Controls.Add(Me.rtxt_accounts)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.KeyPreview = True
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.MinimumSize = New System.Drawing.Size(460, 428)
    Me.Name = "frm_account_massive_search"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.ShowInTaskbar = False
    Me.Text = "frm_account_massive_search"
    Me.gb_buscar_por.ResumeLayout(False)
    Me.gb_buscar_por.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents btn_import_excel As GUI_Controls.uc_button
  Friend WithEvents btn_accept As GUI_Controls.uc_button
  Friend WithEvents btn_exit As GUI_Controls.uc_button
  Friend WithEvents rtxt_accounts As System.Windows.Forms.RichTextBox
  Friend WithEvents btn_delete As GUI_Controls.uc_button
  Friend WithEvents rb_card_number As System.Windows.Forms.RadioButton
  Friend WithEvents rb_account_number As System.Windows.Forms.RadioButton
  Friend WithEvents gb_buscar_por As System.Windows.Forms.GroupBox
End Class
