'-------------------------------------------------------------------
' Copyright � 2008 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_database_config
'
' DESCRIPTION : Allows to modify configuration settings to connect to database.
'
' REVISION HISTORY :
' 
' NOTES:
'        This form does not use NLS strings due to is necessary a connection to
'        database in order to get display language and it will be displayed if
'        there is no connection to database.
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 27-NOV-2008  RRT    Initial version
' 18-OCT-2013  RMS    Modified to ensure the visibility of this dialog for a while in BANNER mode
'--------------------------------------------------------------------

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.Windows.Forms
Imports GUI_Controls
Imports System.IO
Imports WSI.Common

#End Region

Public Class frm_database_config
  Inherits frm_base

#Region "Private Declarations"

  ' RMS 18-OCT-2013  Need to keep ticks on start to make it visible for a while
  Private Shared m_start_tickcount As Integer

#End Region

#Region " Windows Form Designer generated code "
  Public Sub New()

    MyBase.New()

    InitializeComponent()

  End Sub

  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  Friend WithEvents ef_db_primary As GUI_Controls.uc_entry_field
  Friend WithEvents ef_db_secondary As GUI_Controls.uc_entry_field
  Friend WithEvents ef_db_id As GUI_Controls.uc_entry_field
  Friend WithEvents btn_exit As GUI_Controls.uc_button
  Friend WithEvents btn_edit As GUI_Controls.uc_button
  Private WithEvents tf_init_connect As uc_text_field
  Friend WithEvents pb_init_connecting As System.Windows.Forms.ProgressBar
  Private WithEvents tf_init_copyright As uc_text_field
  Private WithEvents tf_init_app_name As uc_text_field
  Friend WithEvents tmr_init_db_conn As System.Windows.Forms.Timer
  Friend WithEvents gb_database_config As System.Windows.Forms.GroupBox

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Me.ef_db_primary = New GUI_Controls.uc_entry_field
    Me.ef_db_secondary = New GUI_Controls.uc_entry_field
    Me.ef_db_id = New GUI_Controls.uc_entry_field
    Me.btn_exit = New GUI_Controls.uc_button
    Me.btn_edit = New GUI_Controls.uc_button
    Me.gb_database_config = New System.Windows.Forms.GroupBox
    Me.tf_init_app_name = New GUI_Controls.uc_text_field
    Me.tf_init_copyright = New GUI_Controls.uc_text_field
    Me.pb_init_connecting = New System.Windows.Forms.ProgressBar
    Me.tf_init_connect = New uc_text_field
    Me.tmr_init_db_conn = New System.Windows.Forms.Timer(Me.components)
    Me.gb_database_config.SuspendLayout()
    Me.SuspendLayout()
    '
    'ef_db_primary
    '
    Me.ef_db_primary.DoubleValue = 0
    Me.ef_db_primary.Enabled = False
    Me.ef_db_primary.IntegerValue = 0
    Me.ef_db_primary.IsReadOnly = False
    Me.ef_db_primary.Location = New System.Drawing.Point(10, 21)
    Me.ef_db_primary.Name = "ef_db_primary"
    Me.ef_db_primary.Size = New System.Drawing.Size(302, 24)
    Me.ef_db_primary.SufixText = "Sufix Text"
    Me.ef_db_primary.SufixTextVisible = True
    Me.ef_db_primary.TabIndex = 0
    Me.ef_db_primary.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_db_primary.TextValue = ""
    Me.ef_db_primary.Value = ""
    '
    'ef_db_secondary
    '
    Me.ef_db_secondary.DoubleValue = 0
    Me.ef_db_secondary.IntegerValue = 0
    Me.ef_db_secondary.IsReadOnly = False
    Me.ef_db_secondary.Location = New System.Drawing.Point(10, 51)
    Me.ef_db_secondary.Name = "ef_db_secondary"
    Me.ef_db_secondary.Size = New System.Drawing.Size(302, 24)
    Me.ef_db_secondary.SufixText = "Sufix Text"
    Me.ef_db_secondary.SufixTextVisible = True
    Me.ef_db_secondary.TabIndex = 1
    Me.ef_db_secondary.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_db_secondary.TextValue = ""
    Me.ef_db_secondary.Value = ""
    '
    'ef_db_id
    '
    Me.ef_db_id.DoubleValue = 0
    Me.ef_db_id.IntegerValue = 0
    Me.ef_db_id.IsReadOnly = False
    Me.ef_db_id.Location = New System.Drawing.Point(10, 83)
    Me.ef_db_id.Name = "ef_db_id"
    Me.ef_db_id.Size = New System.Drawing.Size(115, 24)
    Me.ef_db_id.SufixText = "Sufix Text"
    Me.ef_db_id.SufixTextVisible = True
    Me.ef_db_id.TabIndex = 2
    Me.ef_db_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_db_id.TextValue = ""
    Me.ef_db_id.Value = ""
    '
    'btn_exit
    '
    Me.btn_exit.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_exit.Location = New System.Drawing.Point(280, 170)
    Me.btn_exit.Name = "btn_exit"
    Me.btn_exit.Size = New System.Drawing.Size(90, 30)
    Me.btn_exit.TabIndex = 4
    Me.btn_exit.ToolTipped = False
    Me.btn_exit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_edit
    '
    Me.btn_edit.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_edit.Location = New System.Drawing.Point(249, 115)
    Me.btn_edit.Name = "btn_edit"
    Me.btn_edit.Size = New System.Drawing.Size(90, 30)
    Me.btn_edit.TabIndex = 3
    Me.btn_edit.ToolTipped = False
    Me.btn_edit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'gb_database_config
    '
    Me.gb_database_config.Controls.Add(Me.ef_db_primary)
    Me.gb_database_config.Controls.Add(Me.btn_edit)
    Me.gb_database_config.Controls.Add(Me.ef_db_secondary)
    Me.gb_database_config.Controls.Add(Me.ef_db_id)
    Me.gb_database_config.Location = New System.Drawing.Point(8, 7)
    Me.gb_database_config.Name = "gb_database_config"
    Me.gb_database_config.Size = New System.Drawing.Size(346, 152)
    Me.gb_database_config.TabIndex = 5
    Me.gb_database_config.TabStop = False
    Me.gb_database_config.Text = " Database Configuration "
    '
    'tf_init_app_name
    '
    Me.tf_init_app_name.Font = New System.Drawing.Font("Verdana", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.tf_init_app_name.IsReadOnly = True
    Me.tf_init_app_name.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_init_app_name.LabelForeColor = System.Drawing.SystemColors.ControlText
    Me.tf_init_app_name.Location = New System.Drawing.Point(105, 246)
    Me.tf_init_app_name.Name = "tf_init_app_name"
    Me.tf_init_app_name.Size = New System.Drawing.Size(192, 44)
    Me.tf_init_app_name.SufixText = "Sufix Text"
    Me.tf_init_app_name.SufixTextVisible = True
    Me.tf_init_app_name.TabIndex = 6
    Me.tf_init_app_name.TextWidth = 0
    Me.tf_init_app_name.Value = "xWigosGUI"
    '
    'tf_init_copyright
    '
    Me.tf_init_copyright.IsReadOnly = True
    Me.tf_init_copyright.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_init_copyright.LabelForeColor = System.Drawing.SystemColors.ControlText
    Me.tf_init_copyright.Location = New System.Drawing.Point(58, 296)
    Me.tf_init_copyright.Name = "tf_init_copyright"
    Me.tf_init_copyright.Size = New System.Drawing.Size(275, 25)
    Me.tf_init_copyright.SufixText = "Sufix Text"
    Me.tf_init_copyright.SufixTextVisible = True
    Me.tf_init_copyright.TabIndex = 7
    Me.tf_init_copyright.TextWidth = 0
    Me.tf_init_copyright.Value = "xCopyright (c) WSI Gaming S.L. 2007-2009"
    '
    'pb_init_connecting
    '
    Me.pb_init_connecting.Location = New System.Drawing.Point(45, 378)
    Me.pb_init_connecting.MarqueeAnimationSpeed = 500
    Me.pb_init_connecting.Name = "pb_init_connecting"
    Me.pb_init_connecting.Size = New System.Drawing.Size(289, 22)
    Me.pb_init_connecting.Step = 1
    Me.pb_init_connecting.Style = System.Windows.Forms.ProgressBarStyle.Marquee
    Me.pb_init_connecting.TabIndex = 8
    '
    'tf_init_connecting
    '
    Me.tf_init_connect.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.tf_init_connect.IsReadOnly = True
    Me.tf_init_connect.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_init_connect.LabelForeColor = System.Drawing.SystemColors.ControlText
    Me.tf_init_connect.Location = New System.Drawing.Point(46, 345)
    Me.tf_init_connect.Name = "tf_init_connecting"
    Me.tf_init_connect.Size = New System.Drawing.Size(265, 27)
    Me.tf_init_connect.SufixText = "Sufix Text"
    Me.tf_init_connect.SufixTextVisible = True
    Me.tf_init_connect.TabIndex = 9
    Me.tf_init_connect.TextWidth = 0
    Me.tf_init_connect.Value = "xConnecting to Database..."
    '
    'tmr_init_db_conn
    '
    '
    'frm_database_config
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(379, 419)
    Me.Controls.Add(Me.tf_init_connect)
    Me.Controls.Add(Me.pb_init_connecting)
    Me.Controls.Add(Me.tf_init_copyright)
    Me.Controls.Add(Me.tf_init_app_name)
    Me.Controls.Add(Me.gb_database_config)
    Me.Controls.Add(Me.btn_exit)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    'Me.Location = New System.Drawing.Point(273, 405)
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_database_config"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "Database Configuration"
    Me.ControlBox = False
    Me.TopMost = True
    Me.gb_database_config.ResumeLayout(False)
    Me.ResumeLayout(False)
  End Sub
#End Region

#Region "External Prototypes"

#End Region

#Region "Constants"

  Private Const MAX_DATABASE_NAME_LENGTH As Integer = 30
  Private Const MAX_DATABASE_ID_LENGTH As Integer = 3

  ' RMS 18-OCT-2013  Minimum time visible
  Public Const MIN_TIME_VISIBLE As Integer = 2200

#End Region

#Region "Structures"

#End Region

#Region "Enumerates"

  Public Enum ENUM_DISPLAY_MODE
    MODE_BANNER = 0
    MODE_EDIT = 1
    MODE_SAVE = 2
  End Enum

#End Region

#Region "Members"

  Private Shared display_mode As ENUM_DISPLAY_MODE

#End Region

#Region "Delegates"

#End Region

#Region "Overrides"

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_DATABASE_CONFIG

    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Given a type, the function returns if it needs to be audited
  '
  '  PARAMS:
  '     - INPUT:
  '         - AuditType: AUDIT_FLAGS that wants to be audited
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_HasToBeAudited(ByVal AuditType As AUDIT_FLAGS) As Boolean

    Return False

  End Function

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' - Form
    ' Form name is added after 

    '- Buttons
    btn_exit.Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    btn_edit.Text = GLB_NLS_GUI_CONTROLS.GetString(25)

    '- Labels
    tf_init_app_name.Value = "WigosGUI" 'GLB_NLS_GUI_AUDITOR.GetString(100)
    tf_init_app_name.TextVisible = False
    tf_init_copyright.Value = "Copyright � WSI Gaming S.L. 2007-2016"
    tf_init_copyright.TextVisible = False
    ' "Connecting to Database..."
    tf_init_connect.Value = GLB_NLS_GUI_CONTROLS.GetString(371)
    tf_init_connect.TextVisible = False

    '- Entry fields
    ef_db_primary.Text = GLB_NLS_GUI_CONTROLS.GetString(373)
    ef_db_secondary.Text = GLB_NLS_GUI_CONTROLS.GetString(374)
    ef_db_id.Text = GLB_NLS_GUI_CONTROLS.GetString(375)

    ef_db_primary.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, MAX_DATABASE_NAME_LENGTH)
    ef_db_secondary.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, MAX_DATABASE_NAME_LENGTH)
    ef_db_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, MAX_DATABASE_ID_LENGTH)

  End Sub ' GUI_InitControls

  ' PURPOSE: Control to receive the initial focus.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    If display_mode = ENUM_DISPLAY_MODE.MODE_BANNER Then
      ' "Connecting to Database..."
      Me.Text = GLB_NLS_GUI_CONTROLS.GetString(371)
    Else
      ' "Database Configuration"
      Me.Text = GLB_NLS_GUI_CONTROLS.GetString(372)

      Me.Focus()
      btn_edit.Focus()
    End If

  End Sub ' GUI_SetInitialFocus

#End Region

#Region "Public Functions"

  ' PURPOSE: Form constructor method.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub LoadDialog(ByVal Connecting As Boolean)

    Dim frm_database_cfg As New frm_database_config

    If Not (frm_database_cfg Is Nothing) Then
      GLB_frm_db_config = frm_database_cfg
    End If

    If Connecting Then
      display_mode = ENUM_DISPLAY_MODE.MODE_BANNER

      ' Disable database configuration part
      frm_database_cfg.gb_database_config.Visible = False
      frm_database_cfg.ef_db_primary.Visible = False
      frm_database_cfg.ef_db_secondary.Visible = False
      frm_database_cfg.ef_db_id.Visible = False
      frm_database_cfg.btn_edit.Visible = False
      frm_database_cfg.btn_exit.Visible = False

      ' Enable connection part
      ' - Set controls visible
      frm_database_cfg.tf_init_app_name.Visible = True
      frm_database_cfg.tf_init_copyright.Visible = True
      frm_database_cfg.tf_init_connect.Visible = True
      frm_database_cfg.pb_init_connecting.Visible = True
      frm_database_cfg.pb_init_connecting.Value = 0

      ' Get ticks on show dialog
      m_start_tickcount = WSI.Common.Misc.GetTickCount()

      frm_database_cfg.tmr_init_db_conn.Start()

      ' - Move visible controls to upperside.
      frm_database_cfg.tf_init_app_name.Top = 30
      frm_database_cfg.tf_init_copyright.Top = 70
      frm_database_cfg.tf_init_connect.Top = 100
      frm_database_cfg.pb_init_connecting.Top = 130

      frm_database_cfg.Height = 230
    Else
      ' Not connected display configuration file modification
      display_mode = ENUM_DISPLAY_MODE.MODE_EDIT

      ' Hide initial connection dialog
      frm_database_cfg.tf_init_app_name.Visible = False
      frm_database_cfg.tf_init_copyright.Visible = False
      frm_database_cfg.tf_init_connect.Visible = False
      frm_database_cfg.pb_init_connecting.Visible = False

      ' Get configuration values
      frm_database_cfg.ef_db_primary.TextValue = WSI.Common.ConfigurationFile.GetSetting("DBPrincipal")
      frm_database_cfg.ef_db_secondary.TextValue = WSI.Common.ConfigurationFile.GetSetting("DBMirror")
      frm_database_cfg.ef_db_id.TextValue = WSI.Common.ConfigurationFile.GetSetting("DBId")

      frm_database_cfg.ef_db_primary.Enabled = False
      frm_database_cfg.ef_db_secondary.Enabled = False
      frm_database_cfg.ef_db_id.Enabled = False

      frm_database_cfg.Height = 243
    End If

    Call frm_database_cfg.ShowDialog()
    Call frm_database_cfg.Close()

  End Sub ' LoadDialog

  ' PURPOSE: Keeps the form visible for a while, filling the progressbar.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub EnsureVisible()
    Dim _step As Integer
    _step = (pb_init_connecting.Maximum - pb_init_connecting.Value) \ 10

    While (True)

      ' Progress until maximum
      If (pb_init_connecting.Value + _step) <= pb_init_connecting.Maximum Then
        pb_init_connecting.Value += _step
      Else
        pb_init_connecting.Value = pb_init_connecting.Maximum
      End If

      If (WSI.Common.Misc.GetTickCount() - m_start_tickcount) >= MIN_TIME_VISIBLE Then
        'Exit if minimum time visible reached
        Exit While
      End If

      System.Threading.Thread.Sleep(250)

    End While

    pb_init_connecting.Value = pb_init_connecting.Maximum

    System.Threading.Thread.Sleep(100)

  End Sub

#End Region

#Region "Private Functions"

  ' PURPOSE: Button edit click event handle.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub btn_edit_ClickEvent() Handles btn_edit.ClickEvent

    Dim default_xml As String

    ' Save edited fields into configuration file.
    If (display_mode = ENUM_DISPLAY_MODE.MODE_SAVE) Then

      default_xml = "<SiteConfig>" + _
                    "<DBPrincipal>" + ef_db_primary.Value.Trim() + "</DBPrincipal>" + _
                    "<DBMirror>" + ef_db_secondary.Value.Trim() + "</DBMirror>" + _
                    "<DBId>" + ef_db_id.Value.Trim() + "</DBId>" + _
                    "</SiteConfig>"

      If (ConfigurationFile.SaveConfigFile("WigosGUI.cfg", default_xml) = False) Then
        MsgBox("Configuration file could not be updated. Please check file attributes.")
      Else
        Me.Hide()
      End If

    End If

    If (display_mode = ENUM_DISPLAY_MODE.MODE_EDIT) Then

      ' Allow edit input fields.
      btn_edit.Text = "Save"
      display_mode = ENUM_DISPLAY_MODE.MODE_SAVE

      ef_db_primary.Enabled = True
      ef_db_secondary.Enabled = True
      ef_db_id.Enabled = True

      ef_db_primary.Focus()
    End If

  End Sub ' btn_edit_ClickEvent


  ' PURPOSE: Button exit click event handle.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub btn_exit_ClickEvent() Handles btn_exit.ClickEvent
    Environment.Exit(0)
  End Sub ' btn_edit_ClickEvent

  ' PURPOSE: Timer event on tick.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub tmr_init_db_conn_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmr_init_db_conn.Tick

    ' Ensure visible on connecting to database
    If display_mode = ENUM_DISPLAY_MODE.MODE_BANNER Then
      Call EnsureVisible()
    End If

  End Sub ' tmr_init_db_conn_Tick

#End Region

End Class