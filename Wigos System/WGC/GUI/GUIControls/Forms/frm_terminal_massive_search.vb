'-------------------------------------------------------------------
' Copyright � 2018 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_terminal_massive_search.vb
' DESCRIPTION:   Massive search for terminal
' AUTHOR:        Joan Benito Mart�nez
' CREATION DATE: 03-APR-2018
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-APR-2018  JBM    Initial version.
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On

#Region " Imports "

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Windows.Forms
Imports System.Text

#End Region ' Imports

Public Class frm_terminal_massive_search
  Inherits GUI_Controls.frm_base

  Private uc_provider_sel As uc_provider
  Private GLB_Accept As Boolean
  Private GLB_MaxDigits As Integer

#Region " Constants "

  'Private Const SEPARATOR As String = "r\n\t"

#End Region ' Constants

#Region " Windows Form Designer generated code "

  Public Sub New(ByRef uc_provider_sel1)

    Call MyBase.New()
    Me.uc_provider_sel = uc_provider_sel1
    Call InitializeComponent()
    Call Me.GUI_SetInitialFocus()

  End Sub

#End Region ' Windows Form Designer generated code

#Region " Overridable GUI function "

  ' PURPOSE: Initializes the window title and all the
  '          entry fields
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  ' 
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.GLB_MaxDigits = 30

    ' Set the text in the entry fields
    Me.rtxt_terminals.Text = String.Empty

    If Not String.IsNullOrEmpty(Me.uc_provider_sel.MassiveSearchNumbersToEdit) Then
      Call Me.SetTerminalsText(Me.uc_provider_sel.MassiveSearchNumbersToEdit, False)
    End If

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9045) ' B�squeda masiva de terminales

    ' Init the buttons text
    Me.btn_delete.Text = GLB_NLS_GUI_CONTROLS.GetString(11) ' Borrar
    Me.btn_accept.Text = GLB_NLS_GUI_CONTROLS.GetString(1) ' Aceptar
    Me.btn_exit.Text = GLB_NLS_GUI_CONTROLS.GetString(2) ' Cancelar
    Me.btn_import_excel.Text = GLB_NLS_GUI_CONFIGURATION.GetString(1) ' Importar

    Me.gb_buscar_por.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1892)
    Me.rb_terminal_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(367)
    Me.rb_terminal_name.Checked = True
  End Sub ' GUI_InitControls

  ' PURPOSE: GUI form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: None
  '
  ' NOTES
  ' 
  Public Overrides Sub GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE : Sets initial focus
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  ' 
  Protected Overrides Sub GUI_SetInitialFocus()
    Call Me.rtxt_terminals.Focus()
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE : Check is there is any changes in the accoun list, and if so, give the option to cancel the operation
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  ' 
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    CloseCanceled = Me.CheckForChanges()
  End Sub

#End Region ' Overridable GUI function

#Region " Private Functions "

  ' PURPOSE: Validate format of the introduced terminals, and save them if they are ok
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS:
  '       True - The validation is ok
  '       False - The validation is not ok
  '          
  ' NOTES
  '
  Private Function ValidateAndSaveTerminals() As Boolean
    Dim _number As String
    Dim _terminals_numbers As New StringBuilder
    Dim _numbers As ArrayList
    Dim _table_numbers As DataTable
    Dim _num As String


    _numbers = New ArrayList
    _number = String.Empty

    For Each _number In Me.GetRawTerminalsNumbers(Me.rtxt_terminals.Text)
      'If Not _number.Length <= GLB_MaxDigits Or _
      'Not WSI.Common.ValidateFormat.RawNumber(_number) Then
      If Not _number.Length <= GLB_MaxDigits Then
        ' Msg: El formato no es correcto 
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2557), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _number)

        Return False
      Else
        _terminals_numbers.Append(_number & Environment.NewLine)
      End If
    Next

    _table_numbers = DetectDuplicateNumbers(_numbers, _terminals_numbers.ToString.TrimEnd(vbLf))

    If _numbers.Count <> _table_numbers.Rows.Count Then
      ' Msg: Se han detectado cuentas duplicadas.�Desea eliminarlas?
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(9042), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
      mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then
        _terminals_numbers.Length = 0
        For Each _num In _numbers
          _terminals_numbers.Append(_num)
        Next
      End If
    End If

    GLB_Accept = True
    _number = String.Empty

    For Each _num In _numbers
      _number &= _num
    Next

    Me.uc_provider_sel.MassiveSearchNumbers() = _number.TrimEnd(Environment.NewLine)
    Me.uc_provider_sel.MassiveSearchNumbersToEdit() = _terminals_numbers.ToString.TrimEnd(Environment.NewLine)

    Return True
  End Function ' ValidateAndSaveAccounts

  ' PURPOSE: Get a list of account numbers
  '          
  '    - INPUT: String with the text to "clean up"
  '
  '    - OUTPUT:
  '              
  ' RETURNS: A list of account numbers
  '          
  ' NOTES
  '
  Private Function GetRawTerminalsNumbers(ByVal TerminalText As String) As String()
    Dim _terminals_numbers As String()
    Dim _sep(0 To 1) As Char
    Dim _tmp_accounts As String

    _terminals_numbers = TerminalText.Split(vbLf)
    _tmp_accounts = String.Join(";", _terminals_numbers)
    _sep(0) = ";"
    _sep(1) = ","
    _terminals_numbers = _tmp_accounts.Split(_sep, StringSplitOptions.RemoveEmptyEntries)

    Return _terminals_numbers
  End Function ' GetRawAccountNumbers

  ' PURPOSE: Get the data account formated in four columns
  '          
  '    - INPUT: 
  '            String with the text
  '            Integer with the index
  '
  '    - OUTPUT:
  '              
  ' RETURNS: String with the text formated
  '          
  ' NOTES
  '
  Private Function GetFormatedText(ByVal Idx As Integer, ByVal Num As String) As String
    Dim _formated_text As String
    Dim _Mod As Integer

    If Math.IEEERemainder(Idx + 1, _Mod) Then
      '_formated_text = Num.PadLeft(0, Environment.NewLine) & Environment.NewLine
      _formated_text = Num
    Else
      '_formated_text = Num.PadLeft(0, Environment.NewLine) & vbCr
      _formated_text = Num
      Application.DoEvents()
    End If

    Return _formated_text
  End Function

  ' PURPOSE: Set the account text removing duplicates
  '          
  '    - INPUT: String with the text
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Private Sub SetTerminalsText(ByVal Text As String, ByVal AskForDuplicates As Boolean)
    Dim _table_numbers As DataTable
    Dim _terminal_numbers As New StringBuilder
    Dim _numbers As ArrayList
    Dim _idx_num As Integer
    Dim _num As String
    Dim _is_duplicated As Boolean
    Dim _show_duplicated As Boolean

    _show_duplicated = True
    _numbers = New ArrayList

    Try

      _table_numbers = DetectDuplicateNumbers(_numbers, Text)
      Call Me.EnableControls(False)

      If _numbers.Count <> _table_numbers.Rows.Count Then
        If AskForDuplicates Then
          ' Msg: Se han detectado cuentas duplicadas.�Desea eliminarlas?
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2635), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
          mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then
            _show_duplicated = False
          End If
        End If
      End If

      Me.rtxt_terminals.Text = String.Empty

      If Not _show_duplicated Then
        _idx_num = 0

        For Each _num In _numbers
          Me.rtxt_terminals.AppendText(GetFormatedText(_idx_num, _num))
          _idx_num += 1
        Next
      Else
        For _idx_num = 0 To _table_numbers.Rows.Count - 1
          _num = _table_numbers.Rows(_idx_num)("Terminal").ToString
          _is_duplicated = _table_numbers.Rows(_idx_num)("Duplicate")

          Me.rtxt_terminals.SelectionColor = Drawing.Color.Black

          If _is_duplicated Then
            Me.rtxt_terminals.SelectionColor = Drawing.Color.Red
          End If

          Me.rtxt_terminals.AppendText(GetFormatedText(_idx_num, _num))
        Next
      End If

      Me.rtxt_terminals.SelectionStart = Me.rtxt_terminals.Text.Length + 1
      Me.rtxt_terminals.SelectionLength = 0

    Finally
      Call Me.EnableControls(True)
    End Try
  End Sub ' SetAccountText

  ' PURPOSE: To Enable/Disable the form controls
  '          
  '    - INPUT: Enabled as Boolean to set the control state
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Private Sub EnableControls(ByVal Enabled As Boolean)
    Me.btn_import_excel.Enabled = Enabled
    Me.btn_delete.Enabled = Enabled
    Me.btn_accept.Enabled = Enabled
    Me.btn_exit.Enabled = Enabled
  End Sub

  ' PURPOSE: Detect the duplicate numbers in the String
  '          
  '    - INPUT: 
  '            String with the text
  '            ArrayList to store
  '
  '    - OUTPUT:
  '              
  ' RETURNS: Datatable with the numbers and if they are duplicated or not
  '          
  ' NOTES
  '
  Private Function DetectDuplicateNumbers(ByRef Numbers As ArrayList, ByVal Text As String) As DataTable
    Dim _table As DataTable
    Dim _num As String
    Dim _duplicate As Boolean

    _table = New DataTable

    _table.Columns.Add("Terminal", GetType(String))
    _table.Columns.Add("Duplicate", GetType(Boolean))

    For Each _num In Me.GetRawTerminalsNumbers(Text)
      _duplicate = False

      If Numbers.Contains(_num) Then
        _duplicate = True
      Else
        Numbers.Add(_num)
      End If

      _table.Rows.Add(_num, _duplicate)
    Next

    Return _table
  End Function ' DetectDuplicateNumbers

  ' PURPOSE: Check if there are changes in the terminals list
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: 
  '          True - If the close operation is canceled
  '          False - If the close operation is accepted
  '          
  ' NOTES
  '
  Private Function CheckForChanges() As Boolean
    Dim _before As String
    Dim _after As String

    _before = Me.uc_provider_sel.MassiveSearchNumbersToEdit
    _after = String.Join(" ", Me.GetRawTerminalsNumbers(Me.rtxt_terminals.Text))

    If String.Equals(_before, _after) Or GLB_Accept Then

      Return False
    Else
      ' Msg: Los cambios realizados en la lista de cuentas se perder�n �Est� seguro que quiere cancelar?
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(9043), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO, _
        , Me.rtxt_terminals.Text) = ENUM_MB_RESULT.MB_RESULT_YES Then

        Return False
      End If
    End If

    Call Me.rtxt_terminals.Focus()

    Return True
  End Function ' CheckForChanges

  ' PURPOSE: Import account numbers from excel
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Private Sub ImportFromExcel()
    Dim _open_file_dialog As New OpenFileDialog
    Dim _terminals As String
    Dim _excel As GUI_Reports.CLASS_EXCEL_DATA

    _open_file_dialog.Filter = "Excel File (*.xls, *.xlsx )| *.xls; *.xlsx"

    If _open_file_dialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
      _excel = New GUI_Reports.CLASS_EXCEL_DATA()
      _terminals = _excel.GetFirstColumnExcelData(_open_file_dialog.FileName, Environment.NewLine, True)

      If Not String.IsNullOrEmpty(Me.rtxt_terminals.Text) Then
        _terminals = Me.rtxt_terminals.Text & Environment.NewLine & _terminals
      End If

      Call Me.SetTerminalsText(_terminals, True)
    End If
  End Sub ' ImportFromExcel

  ' PURPOSE: To delete the text in the Riched TextBox
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Private Sub DeleteText()
    If Not String.IsNullOrEmpty(Me.rtxt_terminals.Text) Then
      ' Msg: �Est� seguro que quiere borrar la lista de terminales?
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(9044), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO, _
        , Me.rtxt_terminals.Text) = ENUM_MB_RESULT.MB_RESULT_YES Then
        Me.rtxt_terminals.Text = String.Empty
      End If
    End If
  End Sub ' DeleteText


#End Region ' Private Functions

#Region " Events "

  ' PURPOSE: To check changes and close the form
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Private Sub btn_exit_ClickEvent() Handles btn_exit.ClickEvent
    Call Me.Close()
  End Sub ' btn_exit_ClickEvent

  ' PURPOSE: To Check de format and pass the results to the uc_entry_field
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Private Sub btn_accept_ClickEvent() Handles btn_accept.ClickEvent
    If ValidateAndSaveTerminals() Then
      Call Me.Close()
    End If
  End Sub ' btn_accept_ClickEvent

  ' PURPOSE: Limited to numeric, commas, semicolons, enters and spaces characters
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  Private Sub rtxt_accounts_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles rtxt_terminals.KeyPress
    Me.rtxt_terminals.SelectionColor = Drawing.Color.Black

    'If Asc(e.KeyChar) <> 8 _
    'And e.KeyChar <> Microsoft.VisualBasic.ChrW(Keys.Return) _
    'And e.KeyChar <> Microsoft.VisualBasic.ChrW(Keys.Space) _
    'And e.KeyChar <> "," _
    'And e.KeyChar <> ";" Then
    '  'And (Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57) 

    '  e.Handled = True
    'End If
  End Sub ' rtxt_accounts_KeyPress

  ' PURPOSE: To close the form if ESC button is pressed
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Private Sub frm_terminals_massive_search_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
    If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Escape) Then
      Call Me.Close()
    End If
  End Sub ' frm_account_massive_search_KeyPress

  ' PURPOSE: Delete text in RichTextBox
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Private Sub btn_delete_ClickEvent() Handles btn_delete.ClickEvent
    Call Me.DeleteText()
    Call Me.rtxt_terminals.Focus()
  End Sub ' btn_delete_ClickEvent

  ' PURPOSE: To import data from an Excel
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  ' NOTES
  '
  Private Sub btn_import_excel_ClickEvent() Handles btn_import_excel.ClickEvent
    Call Me.ImportFromExcel()
    Call Me.rtxt_terminals.Focus()
  End Sub ' btn_import_excel_ClickEvent

#End Region ' Events

End Class