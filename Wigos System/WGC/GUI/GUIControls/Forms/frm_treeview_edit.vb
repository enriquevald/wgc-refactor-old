'-------------------------------------------------------------------
' Copyright � 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_treeview_edit.vb  
'
' DESCRIPTION:   form base to select and edit item from treeview
'
' AUTHOR:        Javier Barea
'
' CREATION DATE: 29-MAR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 29-MAR-2017  JBP    Initial version
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls

Imports System.Windows.Forms
Imports System.Drawing
Imports WSI.Common
Imports WSI.Common.Jackpot
Imports System.ComponentModel

Public Class frm_treeview_edit
  Inherits frm_base

  Protected Enum ENUM_SCREEN_MODE
    MODE_EDIT = 0
    MODE_NEW = 1
  End Enum

  Protected Enum ENUM_DB_OPERATION
    DB_OPERATION_CREATE = 1
    DB_OPERATION_DUPLICATE = 2
    DB_OPERATION_READ = 10
    DB_OPERATION_INSERT = 11
    DB_OPERATION_UPDATE = 12
    DB_OPERATION_DELETE = 13
    DB_OPERATION_BEFORE_READ = 20
    DB_OPERATION_BEFORE_INSERT = 21
    DB_OPERATION_BEFORE_UPDATE = 22
    DB_OPERATION_BEFORE_DELETE = 23
    DB_OPERATION_AFTER_READ = 30
    DB_OPERATION_AFTER_INSERT = 31
    DB_OPERATION_AFTER_UPDATE = 32
    DB_OPERATION_AFTER_DELETE = 33
    DB_OPERATION_AUDIT_INSERT = 41
    DB_OPERATION_AUDIT_UPDATE = 42
    DB_OPERATION_AUDIT_DELETE = 43
  End Enum

  Public Enum ENUM_BUTTON_CLICK_EVENT_TYPE
    EVENT_AFFECTED_ACCOUNTS = 0
  End Enum

  Private Const DEFAULT_OBJECT_ID = -1

  Private m_screen_mode As ENUM_SCREEN_MODE
  Private m_output_data As CLASS_BASE
  Private m_input_data As CLASS_BASE
  Private m_db_status As ENUM_STATUS
  Private m_object_id As Object
  Private m_check_changes As Boolean
  Private m_node_in_edition As TreeNode
  Private m_abort_treeview_selection As Boolean
  Private m_special_data As Object
  Private m_selected_node As TreeNode
  Public m_current_uc_edit As uc_treeview_edit_base
  Private m_wrong_db_version As Boolean = False
  Private m_discard_changes As Boolean

  ' AJQ, 18-NOV-2002 to allow DL Generation ... Chapuza
  Private m_close_on_ok As Boolean = True
  Friend WithEvents pnl_margin_top_buttons As System.Windows.Forms.Panel
  Private WithEvents btn_cancel As GUI_Controls.uc_button
  Private WithEvents btn_new As GUI_Controls.uc_button
  Private WithEvents btn_ok As GUI_Controls.uc_button
  Private WithEvents btn_delete As GUI_Controls.uc_button
  Private WithEvents btn_print As GUI_Controls.uc_button
  Private WithEvents panel_separator As System.Windows.Forms.Panel
  Friend WithEvents pnl_custom_buttons As System.Windows.Forms.Panel
  Friend WithEvents btn_custom_2 As GUI_Controls.uc_button
  Friend WithEvents btn_custom_1 As GUI_Controls.uc_button
  Friend WithEvents btn_custom_0 As GUI_Controls.uc_button
  Friend WithEvents pnl_buttons As System.Windows.Forms.Panel
  Public WithEvents pnl_container As System.Windows.Forms.SplitContainer
  Private WithEvents m_treeview As uc_treeview

#Region "Properties"

  Public Property TreeView As uc_treeview
    Get
      Return Me.m_treeview
    End Get
    Set(value As uc_treeview)
      Me.m_treeview = value
    End Set
  End Property

  Public Property IsDiscardChanges As Boolean
    Get
      Return Me.m_discard_changes
    End Get
    Set(value As Boolean)
      Me.m_discard_changes = value
    End Set
  End Property

  Public Property NodeInEdition As TreeNode
    Get
      Return Me.m_node_in_edition
    End Get
    Set(value As TreeNode)
      Me.m_node_in_edition = value
    End Set
  End Property

  Public Property CustomButtonsPosition As DockStyle
    Get
      Return Me.pnl_custom_buttons.Dock
    End Get
    Set(value As DockStyle)
      Me.pnl_custom_buttons.Dock = value
    End Set
  End Property

  Public Property CurrentUcEdit As uc_treeview_edit_base
    Get
      Return Me.m_current_uc_edit
    End Get
    Set(value As uc_treeview_edit_base)
      Me.m_current_uc_edit = value
    End Set
  End Property

  Protected Property CloseOnOkClick() As Boolean
    Get
      Return m_close_on_ok
    End Get
    Set(ByVal Value As Boolean)
      m_close_on_ok = Value
    End Set
  End Property

  Protected Property CheckChanges() As Boolean
    Get
      Return m_check_changes
    End Get
    Set(ByVal Value As Boolean)
      m_check_changes = Value
    End Set
  End Property

  Protected Property DbStatus() As ENUM_STATUS
    Get
      Return m_db_status
    End Get
    Set(ByVal Value As ENUM_STATUS)
      m_db_status = Value
    End Set
  End Property

  Protected Property DbReadObject() As CLASS_BASE
    Get
      Return m_input_data
    End Get
    Set(ByVal Value As CLASS_BASE)
      m_input_data = Value
    End Set
  End Property

  Protected Property DbEditedObject() As CLASS_BASE
    Get
      Return m_output_data
    End Get
    Set(ByVal Value As CLASS_BASE)
      m_output_data = Value
    End Set
  End Property

  Protected Property ScreenMode() As ENUM_SCREEN_MODE
    Get
      Return m_screen_mode
    End Get
    Set(ByVal Value As ENUM_SCREEN_MODE)
      m_screen_mode = Value
    End Set
  End Property

  Public Property DbObjectId() As Object
    Get
      Return m_object_id
    End Get
    Set(ByVal Value As Object)
      m_object_id = Value
    End Set
  End Property

#End Region

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    Call Me.InitializeComponent()

    'Add any initialization after the InitializeComponent() call
  End Sub

  ''' <summary>
  ''' Check parent form type
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Shared Function CheckParentFormType(ByVal Control As UserControl) As Boolean
    Dim _type As Type

    If Not Control.ParentForm Is Nothing Then
      ' Check ParentForm type
      _type = Control.ParentForm.GetType().BaseType

      Return (Not _type Is Nothing AndAlso _type.Equals(GetType(frm_treeview_edit)))
    End If

    Return False

  End Function ' CheckParentFormType

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.pnl_margin_top_buttons = New System.Windows.Forms.Panel()
    Me.btn_cancel = New GUI_Controls.uc_button()
    Me.btn_new = New GUI_Controls.uc_button()
    Me.btn_ok = New GUI_Controls.uc_button()
    Me.btn_delete = New GUI_Controls.uc_button()
    Me.btn_print = New GUI_Controls.uc_button()
    Me.panel_separator = New System.Windows.Forms.Panel()
    Me.pnl_custom_buttons = New System.Windows.Forms.Panel()
    Me.btn_custom_2 = New GUI_Controls.uc_button()
    Me.btn_custom_1 = New GUI_Controls.uc_button()
    Me.btn_custom_0 = New GUI_Controls.uc_button()
    Me.pnl_buttons = New System.Windows.Forms.Panel()
    Me.pnl_container = New System.Windows.Forms.SplitContainer()
    Me.m_treeview = New GUI_Controls.uc_treeview()
    Me.pnl_custom_buttons.SuspendLayout()
    Me.pnl_buttons.SuspendLayout()
    Me.pnl_container.Panel1.SuspendLayout()
    Me.pnl_container.SuspendLayout()
    Me.SuspendLayout()
    '
    'pnl_margin_top_buttons
    '
    Me.pnl_margin_top_buttons.Dock = System.Windows.Forms.DockStyle.Top
    Me.pnl_margin_top_buttons.Location = New System.Drawing.Point(0, 0)
    Me.pnl_margin_top_buttons.Name = "pnl_margin_top_buttons"
    Me.pnl_margin_top_buttons.Size = New System.Drawing.Size(970, 10)
    Me.pnl_margin_top_buttons.TabIndex = 113
    '
    'btn_cancel
    '
    Me.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_cancel.Dock = System.Windows.Forms.DockStyle.Right
    Me.btn_cancel.Location = New System.Drawing.Point(880, 10)
    Me.btn_cancel.Margin = New System.Windows.Forms.Padding(3, 10, 3, 3)
    Me.btn_cancel.Name = "btn_cancel"
    Me.btn_cancel.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_cancel.Size = New System.Drawing.Size(90, 30)
    Me.btn_cancel.TabIndex = 120
    Me.btn_cancel.ToolTipped = False
    Me.btn_cancel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_new
    '
    Me.btn_new.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_new.Dock = System.Windows.Forms.DockStyle.Right
    Me.btn_new.Location = New System.Drawing.Point(790, 10)
    Me.btn_new.Margin = New System.Windows.Forms.Padding(3, 10, 3, 3)
    Me.btn_new.Name = "btn_new"
    Me.btn_new.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_new.Size = New System.Drawing.Size(90, 30)
    Me.btn_new.TabIndex = 121
    Me.btn_new.ToolTipped = False
    Me.btn_new.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_ok
    '
    Me.btn_ok.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_ok.Dock = System.Windows.Forms.DockStyle.Right
    Me.btn_ok.Location = New System.Drawing.Point(700, 10)
    Me.btn_ok.Margin = New System.Windows.Forms.Padding(3, 10, 3, 3)
    Me.btn_ok.Name = "btn_ok"
    Me.btn_ok.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_ok.Size = New System.Drawing.Size(90, 30)
    Me.btn_ok.TabIndex = 119
    Me.btn_ok.ToolTipped = False
    Me.btn_ok.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_delete
    '
    Me.btn_delete.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_delete.Dock = System.Windows.Forms.DockStyle.Right
    Me.btn_delete.Location = New System.Drawing.Point(610, 10)
    Me.btn_delete.Margin = New System.Windows.Forms.Padding(3, 10, 3, 3)
    Me.btn_delete.Name = "btn_delete"
    Me.btn_delete.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_delete.Size = New System.Drawing.Size(90, 30)
    Me.btn_delete.TabIndex = 118
    Me.btn_delete.ToolTipped = False
    Me.btn_delete.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_print
    '
    Me.btn_print.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_print.Dock = System.Windows.Forms.DockStyle.Right
    Me.btn_print.Location = New System.Drawing.Point(520, 10)
    Me.btn_print.Margin = New System.Windows.Forms.Padding(3, 10, 3, 3)
    Me.btn_print.Name = "btn_print"
    Me.btn_print.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_print.Size = New System.Drawing.Size(90, 30)
    Me.btn_print.TabIndex = 117
    Me.btn_print.ToolTipped = False
    Me.btn_print.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'panel_separator
    '
    Me.panel_separator.Dock = System.Windows.Forms.DockStyle.Right
    Me.panel_separator.Location = New System.Drawing.Point(495, 10)
    Me.panel_separator.Name = "panel_separator"
    Me.panel_separator.Size = New System.Drawing.Size(25, 30)
    Me.panel_separator.TabIndex = 122
    '
    'pnl_custom_buttons
    '
    Me.pnl_custom_buttons.Controls.Add(Me.btn_custom_2)
    Me.pnl_custom_buttons.Controls.Add(Me.btn_custom_1)
    Me.pnl_custom_buttons.Controls.Add(Me.btn_custom_0)
    Me.pnl_custom_buttons.Dock = System.Windows.Forms.DockStyle.Left
    Me.pnl_custom_buttons.Location = New System.Drawing.Point(0, 10)
    Me.pnl_custom_buttons.Name = "pnl_custom_buttons"
    Me.pnl_custom_buttons.Size = New System.Drawing.Size(273, 30)
    Me.pnl_custom_buttons.TabIndex = 123
    '
    'btn_custom_2
    '
    Me.btn_custom_2.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_custom_2.Dock = System.Windows.Forms.DockStyle.Left
    Me.btn_custom_2.Location = New System.Drawing.Point(180, 0)
    Me.btn_custom_2.Margin = New System.Windows.Forms.Padding(3, 10, 3, 3)
    Me.btn_custom_2.Name = "btn_custom_2"
    Me.btn_custom_2.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_custom_2.Size = New System.Drawing.Size(90, 30)
    Me.btn_custom_2.TabIndex = 126
    Me.btn_custom_2.ToolTipped = False
    Me.btn_custom_2.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_custom_1
    '
    Me.btn_custom_1.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_custom_1.Dock = System.Windows.Forms.DockStyle.Left
    Me.btn_custom_1.Location = New System.Drawing.Point(90, 0)
    Me.btn_custom_1.Margin = New System.Windows.Forms.Padding(3, 10, 3, 3)
    Me.btn_custom_1.Name = "btn_custom_1"
    Me.btn_custom_1.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_custom_1.Size = New System.Drawing.Size(90, 30)
    Me.btn_custom_1.TabIndex = 127
    Me.btn_custom_1.ToolTipped = False
    Me.btn_custom_1.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_custom_0
    '
    Me.btn_custom_0.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_custom_0.Dock = System.Windows.Forms.DockStyle.Left
    Me.btn_custom_0.Location = New System.Drawing.Point(0, 0)
    Me.btn_custom_0.Margin = New System.Windows.Forms.Padding(3, 10, 3, 3)
    Me.btn_custom_0.Name = "btn_custom_0"
    Me.btn_custom_0.Padding = New System.Windows.Forms.Padding(2)
    Me.btn_custom_0.Size = New System.Drawing.Size(90, 30)
    Me.btn_custom_0.TabIndex = 128
    Me.btn_custom_0.ToolTipped = False
    Me.btn_custom_0.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'pnl_buttons
    '
    Me.pnl_buttons.Controls.Add(Me.pnl_custom_buttons)
    Me.pnl_buttons.Controls.Add(Me.panel_separator)
    Me.pnl_buttons.Controls.Add(Me.btn_print)
    Me.pnl_buttons.Controls.Add(Me.btn_delete)
    Me.pnl_buttons.Controls.Add(Me.btn_ok)
    Me.pnl_buttons.Controls.Add(Me.btn_new)
    Me.pnl_buttons.Controls.Add(Me.btn_cancel)
    Me.pnl_buttons.Controls.Add(Me.pnl_margin_top_buttons)
    Me.pnl_buttons.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.pnl_buttons.Location = New System.Drawing.Point(4, 442)
    Me.pnl_buttons.Name = "pnl_buttons"
    Me.pnl_buttons.Size = New System.Drawing.Size(970, 40)
    Me.pnl_buttons.TabIndex = 1
    '
    'pnl_container
    '
    Me.pnl_container.Dock = System.Windows.Forms.DockStyle.Fill
    Me.pnl_container.Location = New System.Drawing.Point(4, 4)
    Me.pnl_container.Name = "pnl_container"
    '
    'pnl_container.Panel1
    '
    Me.pnl_container.Panel1.Controls.Add(Me.m_treeview)
    '
    'pnl_container.Panel2
    '
    Me.pnl_container.Panel2.AutoScroll = True
    Me.pnl_container.Size = New System.Drawing.Size(970, 438)
    Me.pnl_container.SplitterDistance = 323
    Me.pnl_container.TabIndex = 3
    '
    'm_treeview
    '
    Me.m_treeview.Dock = System.Windows.Forms.DockStyle.Fill
    Me.m_treeview.IconPerNode = False
    Me.m_treeview.ItemList = Nothing
    Me.m_treeview.Location = New System.Drawing.Point(0, 0)
    Me.m_treeview.Name = "m_treeview"
    Me.m_treeview.RootDescription = Nothing
    Me.m_treeview.Size = New System.Drawing.Size(323, 438)
    Me.m_treeview.TabIndex = 0
    '
    'frm_treeview_edit
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(978, 486)
    Me.Controls.Add(Me.pnl_container)
    Me.Controls.Add(Me.pnl_buttons)
    Me.KeyPreview = True
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.MinimumSize = New System.Drawing.Size(680, 300)
    Me.Name = "frm_treeview_edit"
    Me.ShowInTaskbar = False
    Me.Text = "frm_treeview_edit"
    Me.pnl_custom_buttons.ResumeLayout(False)
    Me.pnl_buttons.ResumeLayout(False)
    Me.pnl_container.Panel1.ResumeLayout(False)
    Me.pnl_container.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Entry Points "

  Protected Overridable Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim ctx As Integer


    If m_wrong_db_version Then
      DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED

      Return
    End If

    If Not MyBase.RightDbVersion() Then

      '' 163  "Operation not available.\nMinimum Database version: %1."
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(163), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, m_min_db_version.ToString("000"))

      '' Flag to avoid other GUI_DB_Operation calls if database is wrong.
      m_wrong_db_version = True

      DbStatus = ENUM_STATUS.STATUS_NOT_SUPPORTED
      Return
    End If

    Try
      'XVV 17/04/2007
      'Comment this function, because are obsolete (Oracle version)
      'GUI_SqlCtxAlloc(ctx)

      Select Case DbOperation
        Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
          ' Creates the object
          Err.Raise(100000, "You must create your data")
          DbReadObject = Nothing
          DbEditedObject = Nothing

        Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
          If Not DbEditedObject Is Nothing Then
            DbReadObject = DbEditedObject.Duplicate
          Else
            DbReadObject = Nothing
          End If

        Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ
        Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT
        Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE
        Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE

        Case ENUM_DB_OPERATION.DB_OPERATION_READ
          DbStatus = DbEditedObject.DB_Read(DbObjectId, ctx)
        Case ENUM_DB_OPERATION.DB_OPERATION_INSERT
          DbStatus = DbEditedObject.DB_Insert(ctx)
        Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
          DbStatus = DbEditedObject.DB_Update(ctx)
        Case ENUM_DB_OPERATION.DB_OPERATION_DELETE
          DbStatus = DbEditedObject.DB_Delete(ctx)

        Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE

          ' Audit Operations 
        Case ENUM_DB_OPERATION.DB_OPERATION_AUDIT_INSERT
          If Not DbEditedObject.AuditorData.Notify(CurrentUser.GuiId, _
                                            CurrentUser.Id, _
                                            CurrentUser.Name, _
                                            CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.INSERT, _
                                            ctx, _
                                            DbReadObject.AuditorData) Then
            ' Logger message 
          End If
        Case ENUM_DB_OPERATION.DB_OPERATION_AUDIT_UPDATE
          If Not DbEditedObject.AuditorData.Notify(CurrentUser.GuiId, _
                                            CurrentUser.Id, _
                                            CurrentUser.Name, _
                                            CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                            ctx, _
                                            DbReadObject.AuditorData) Then
            ' Logger message 
          End If

        Case ENUM_DB_OPERATION.DB_OPERATION_AUDIT_DELETE
          If Not DbEditedObject.AuditorData.Notify(CurrentUser.GuiId, _
                                            CurrentUser.Id, _
                                            CurrentUser.Name, _
                                            CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.DELETE, _
                                            ctx, _
                                            DbReadObject.AuditorData) Then
            ' Logger message 
          End If
        Case Else
      End Select


    Finally
      'XVV 17/04/2007
      'Comment this function, because are obsolete (Oracle version)
      'GUI_SqlCtxRelease(ctx)
    End Try

  End Sub ' GUI_DB_Operation

  ''' <summary>
  ''' Set show new item mode
  ''' </summary>
  ''' <remarks></remarks>
  Public Overridable Sub GUI_ShowNewItem()

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus <> ENUM_STATUS.STATUS_OK Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8087), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
    End If
  End Sub ' GUI_ShowNewItem

  ''' <summary>
  ''' Set show edit item mode
  ''' </summary>
  ''' <remarks></remarks>
  Public Overridable Sub GUI_ShowEditItem(ByVal ObjectId As Object)

    DbObjectId = ObjectId

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    Else
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8087), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
    End If

  End Sub ' GUI_ShowEditItem

#End Region

#Region " Screen Data "

  Private Function DiscardChanges() As Boolean

    If m_check_changes Then
      Call GUI_GetScreenData()
    End If

    Me.m_check_changes = True

    If DbReadObject Is Nothing Then
      Return True
    End If

    If Me.TreeView.IsNewNode() Or Not DbReadObject.AuditorData.IsEqual(DbEditedObject.AuditorData) Then

      Me.IsDiscardChanges = True

#If DEBUG Then
      If False Then
        Dim _differences As List(Of String)
        _differences = GetAuditorDataDifferences()
      End If
#End If

      ' User request
      If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Me.IsDiscardChanges = False

        Return False
      End If
    End If

    Return True

  End Function ' DiscardChanges

  ''' <summary>
  ''' Get auditor data differences
  ''' </summary>
  ''' <remarks></remarks>
  Private Function GetAuditorDataDifferences() As List(Of String)
    Dim _idx As Integer
    Dim _field_name_read As String
    Dim _field_name_edit As String
    Dim _fieldCountRead As Integer
    Dim _fieldCountedit As Integer
    Dim _field_value_read As String
    Dim _field_value_edit As String
    Dim _listChanges As List(Of String)
    Dim _changes_str As String

    _listChanges = New List(Of String)

    Try

      _fieldCountRead = DbReadObject.AuditorData.FieldCount
      _fieldCountedit = DbEditedObject.AuditorData.FieldCount

      For _idx = 0 To _fieldCountRead - 1

        _field_name_read = DbReadObject.AuditorData.FieldName(_idx)
        _field_name_edit = DbEditedObject.AuditorData.FieldName(_idx)

        If _field_name_edit = _field_name_read Then
          _field_value_read = DbReadObject.AuditorData.FieldValue(_idx)
          _field_value_edit = DbEditedObject.AuditorData.FieldValue(_idx)

          If Not _field_value_read = _field_value_edit Then
            _changes_str = "{{{" & _field_name_edit & "}}} -- read: " & _field_value_read & "<---->  edited: " & _field_value_edit
            _listChanges.Add(_changes_str)

          End If
        End If
      Next

    Catch

    End Try

    Return _listChanges

  End Function ' GetAuditorDataDifferences

#End Region

#Region " Button Events "

  Private Sub ButtonNewClick()
    If Not Permissions.Write Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Exit Sub
    End If

    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

  End Sub

  Private Sub ButtonOkClick()
    If Not Permissions.Write Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Exit Sub
    End If

    'added to cahnge the focus from the date picker
    Me.ActiveControl = Me.AcceptButton

    If DbReadObject Is Nothing OrElse DbEditedObject Is Nothing Then
      Return
    End If

    ' Check the data on the screen
    If Not GUI_IsScreenDataOk() Then
      Exit Sub
    End If

    Call GUI_GetScreenData()

    Select Case m_screen_mode

      Case ENUM_SCREEN_MODE.MODE_EDIT
        ' Check read & edit object

        If DbReadObject.AuditorData.IsEqual(DbEditedObject.AuditorData) Then
          Exit Sub
        End If

        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE)
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_UPDATE)
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE)
        If DbStatus = ENUM_STATUS.STATUS_OK Then
          Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AUDIT_UPDATE)
        End If

      Case ENUM_SCREEN_MODE.MODE_NEW
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT)
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_INSERT)
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT)
        If DbStatus = ENUM_STATUS.STATUS_OK Then
          Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AUDIT_INSERT)

        End If

      Case Else
        ' Error
    End Select

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
      If m_screen_mode = ENUM_SCREEN_MODE.MODE_NEW Then
        Me.RefreshTreeView()
      End If
    End If

  End Sub ' ButtonOkClick


  Private Sub ButtonDeleteClick()
    If Permissions.Delete Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DELETE)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE)
      If DbStatus = ENUM_STATUS.STATUS_OK Then
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AUDIT_DELETE)
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
        m_check_changes = False
        Call Me.Close()
      End If
    Else
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

    End If
  End Sub ' ButtonDeleteClick


#End Region

#Region " Must Override: frm_treeview_edit "

  Protected Overridable Function GUI_IsScreenDataOk() As Boolean
    Return False
  End Function

  Protected Overridable Sub GUI_GetScreenData()
    ' Copy the values that you are editing to the object
  End Sub

  Protected Overridable Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    ' Copy the values from the object to the controls
  End Sub

  Protected Overridable Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Return False
    '
  End Function

  Protected Overridable Function GUI_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) As Boolean

    Return False
    '
  End Function

#End Region

#Region " Must Override Methods: frm_base "

  Protected Overrides Sub GUI_InitControls()
    ' Required by the base class
    Call MyBase.GUI_InitControls()

    If Me.DesignMode Then
      Dim na As String()
      Dim va As Integer()
      Dim idx As Integer

      '------------------------------------------------------------------
      'XVV 17/04/2007
      'Change xx valur for [Enum], Enum recover value from Enum structure
      '------------------------------------------------------------------
      'Dim xx As ENUM_BUTTON
      'va = xx.GetValues(GetType(ENUM_BUTTON))
      'na = xx.GetNames(GetType(ENUM_BUTTON))

      Dim xx As ENUM_BUTTON
      va = [Enum].GetValues(GetType(ENUM_BUTTON))
      na = [Enum].GetNames(GetType(ENUM_BUTTON))
      '------------------------------------------------------------------
      For idx = 0 To va.GetLength(0) - 1
        xx = va(idx)
        GUI_Button(va(idx)).Text = Mid(xx.ToString, 8, 9)
      Next
      Exit Sub
    End If

    'Add any initialization after the MyBase.GUI_InitControls()() call
    Me.btn_print.Text = GLB_NLS_GUI_CONTROLS.GetString(5)     ' Imprimir
    Me.btn_new.Text = GLB_NLS_GUI_CONTROLS.GetString(6)
    Me.btn_cancel.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(600)
    Me.btn_delete.Text = GLB_NLS_GUI_CONTROLS.GetString(11)
    Me.btn_ok.Text = GLB_NLS_GUI_CONTROLS.GetString(31)
    m_check_changes = True

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = Me.Permissions.Delete
    GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled = Me.Permissions.Write

  End Sub

#End Region

#Region " Buttons "

#Region " Buttons - Control "

  Protected Function GUI_Button(ByVal ButtonId As ENUM_BUTTON) As uc_button
    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_NEW
        Return btn_new
      Case ENUM_BUTTON.BUTTON_CANCEL
        Return btn_cancel
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Return btn_custom_0
      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        Return btn_custom_1
      Case ENUM_BUTTON.BUTTON_CUSTOM_2
        Return btn_custom_2
      Case ENUM_BUTTON.BUTTON_DELETE
        Return btn_delete
      Case ENUM_BUTTON.BUTTON_OK
        Return btn_ok
      Case ENUM_BUTTON.BUTTON_PRINT
        Return btn_print
      Case Else
        Return Nothing
    End Select
  End Function

#End Region

#Region " Buttons - Actions "

  Protected Overridable Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)
    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CANCEL
        Call Me.Close()

      Case ENUM_BUTTON.BUTTON_CUSTOM_0
      Case ENUM_BUTTON.BUTTON_CUSTOM_1
      Case ENUM_BUTTON.BUTTON_CUSTOM_2

      Case ENUM_BUTTON.BUTTON_DELETE
        Call ButtonDeleteClick()

      Case ENUM_BUTTON.BUTTON_NEW
        Call ButtonNewClick()

      Case ENUM_BUTTON.BUTTON_OK
        Call ButtonOkClick()

      Case ENUM_BUTTON.BUTTON_PRINT
        Call GUI_PrintResultList()
      Case Else

    End Select

    If GUI_HasToBeAudited(ButtonId) Then
      Call MyBase.AuditFormClick(ButtonId)
    End If

  End Sub

  ' PURPOSE: Generate Print report
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overridable Sub GUI_PrintResultList()
    'Override when needed
  End Sub
#End Region

#Region " Buttons - Events "

  Private Sub BaseButtonClick(ByVal ButtonId As ENUM_BUTTON)

    If Not GUI_Button(ButtonId).Enabled Then
      Common_LoggerMsg(ENUM_LOG_MSG.LOG_USER_MESSAGE, "frm_treeview_edit", "BaseButtonClick", , , , "Calling DISABLED button " + ButtonId.ToString())
    End If

    ' If objects are empty, not save nothing. No node selected. 
    'If Me.DbEditedObject Is Nothing Or Me.DbReadObject Is Nothing Then
    ' Return
    ' End If

    'XVV 16/04/2007
    'change Me.Cursor.Current for Windows.Forms.Cursor because compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    Call GUI_ButtonClick(ButtonId)

    'XVV 16/04/2007
    'change Me.Cursor.Current for Windows.Forms.Cursor because compiler generate a warning
    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub btn_custom_0_ClickEvent() Handles btn_custom_0.ClickEvent
    Call BaseButtonClick(ENUM_BUTTON.BUTTON_CUSTOM_0)
  End Sub

  Private Sub btn_custom_1_ClickEvent() Handles btn_custom_1.ClickEvent
    Call BaseButtonClick(ENUM_BUTTON.BUTTON_CUSTOM_1)
  End Sub

  Private Sub btn_custom_2_ClickEvent() Handles btn_custom_2.ClickEvent
    Call BaseButtonClick(ENUM_BUTTON.BUTTON_CUSTOM_2)
  End Sub

  Private Sub btn_new_ClickEvent() Handles btn_new.ClickEvent
    Call BaseButtonClick(ENUM_BUTTON.BUTTON_NEW)
  End Sub

  Private Sub btn_cancel_ClickEvent() Handles btn_cancel.ClickEvent
    Call BaseButtonClick(ENUM_BUTTON.BUTTON_CANCEL)
  End Sub

  Private Sub btn_ok_ClickEvent() Handles btn_ok.ClickEvent
    Call BaseButtonClick(ENUM_BUTTON.BUTTON_OK)
  End Sub

  Private Sub btn_delete_ClickEvent() Handles btn_delete.ClickEvent
    Call BaseButtonClick(ENUM_BUTTON.BUTTON_DELETE)
  End Sub

  Private Sub btn_print_ClickEvent() Handles btn_print.ClickEvent
    Call BaseButtonClick(ENUM_BUTTON.BUTTON_PRINT)
  End Sub

#End Region

#End Region


  'Protected ReadOnly Property GUI_PublicPanel() As Panel
  '  Get
  '    Return Me.panel_public
  '  End Get
  'End Property

  Protected Overrides Sub GUI_FirstActivation()
    Dim ctx As Integer

    Try
      'XVV 17/04/2007
      'Comment this function, because are obsolete (Oracle version)
      'GUI_SqlCtxAlloc(ctx)

      Call MyBase.GUI_FirstActivation()
      '
      If Me.Permissions.Read = False Then
        Call NLS_MsgBox(CInt(0), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly)
        Me.Hide()

        Exit Sub
      End If

      Me.GUI_SetScreenData(ctx)

    Finally
      'XVV 17/04/2007
      'Comment this function, because are obsolete (Oracle version)
      'GUI_SqlCtxRelease(ctx)

    End Try

  End Sub

  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_COMMON_FORM.COMMON_FORM_BASE_EDIT

    '----------------------------------------------------------
    'XVV 13/04/2007
    'Assign Icon to active form 
    'OLD version, use GetExecutingAssembly, but in Class libary is necessary GetEntreAssembly to recover ICON Resource
    If Not DesignMode Then Exit Sub
    'Me.Icon = New Icon(System.Reflection.Assembly.GetEntryAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))

  End Sub

  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    If DiscardChanges() Then
      CloseCanceled = False
    Else
      CloseCanceled = True
    End If
  End Sub

  Public Overrides Function GUI_GetScreenType() As ENUM_SCREEN_TYPE
    Select Case Me.ScreenMode
      Case ENUM_SCREEN_MODE.MODE_EDIT
        Return frm_base.ENUM_SCREEN_TYPE.MODE_EDIT

      Case ENUM_SCREEN_MODE.MODE_NEW
        Return frm_base.ENUM_SCREEN_TYPE.MODE_NEW

      Case Else
        Return frm_base.ENUM_SCREEN_TYPE.MODE_NONE
    End Select
  End Function

#Region " Public Functions"

  Public Sub BindTreeNodeToEdit()
    BindTreeNodeToEdit(EditSelectionItemType.NONE)
  End Sub ' BindTreeNodeToEdit

  Public Sub BindTreeNodeToEdit(ByVal Type As EditSelectionItemType, Optional ByVal DBObject As Object = Nothing)

    Select Case Type

      Case EditSelectionItemType.JACKPOT_CONFIG
        Call Me.BindJackpotConfigNodeToEdit(DBObject)

      Case EditSelectionItemType.JACKPOT_AWARD_CONFIG
        Me.CurrentUcEdit = New uc_jackpot_award_config(DBObject)

      Case EditSelectionItemType.JACKPOT_AWARD_PRIZE_CONFIG
        Me.CurrentUcEdit = New uc_jackpot_award_prize_config(DBObject)

      Case EditSelectionItemType.JACKPOT_DASHBOARD
        Me.CurrentUcEdit = New uc_jackpot_dashboard()

      Case EditSelectionItemType.JACKPOT_VIEWER_CONFIG
        Call Me.BindJackpotViewerConfigNodeToEdit(DBObject)

      Case EditSelectionItemType.JACKPOT_VIEWER_MONITOR
        Me.CurrentUcEdit = New uc_jackpot_viewer_monitor(DBObject)

      Case EditSelectionItemType.JACKPOT_VIEWER_TERMINAL
        Me.CurrentUcEdit = New uc_jackpot_viewer_terminal_selection(DBObject)

      Case EditSelectionItemType.NONE
        Me.CurrentUcEdit = New uc_treeview_edit_base()
    End Select

    Me.CurrentUcEdit.Dock = DockStyle.Fill

    Me.pnl_container.Panel2.Controls.Clear()
    Me.pnl_container.Panel2.Controls.Add(Me.CurrentUcEdit)

  End Sub ' BindTreeNodeToEdit


  ''' <summary>
  ''' Initialize treeView in form
  ''' </summary>
  ''' <remarks></remarks>
  Public Overridable Sub InitTreeView(Optional ByVal RootDescription As String = uc_treeview.DEFAULT_TREEVIEW_ROOT_DESCRIPTION)

    Call m_treeview.Bind(RootDescription)

  End Sub ' InitTreeView

  Public Overridable Function GetObjectFromTreeNode(ByVal TreeNode As TreeNode)
    Return Nothing
  End Function

  ''' <summary>
  ''' Set selected node description
  ''' </summary>
  ''' <param name="Description"></param>
  ''' <remarks></remarks>
  Public Sub SetSelectedNodeDescription(ByVal Description As String, Optional ByVal IsParent As Boolean = False)

    If IsParent Then
      Me.TreeView.SelectedNode.Parent.Text = Me.TreeView.SetNodeDescriptionFormat(Description)
    Else
      Me.TreeView.SelectedNode.Text = Me.TreeView.SetNodeDescriptionFormat(Description)
    End If

  End Sub ' SetTreeNodeDescription

  ''' <summary>
  ''' Change color on enable disable jackpot config
  ''' </summary>
  ''' <param name="Status"></param>
  ''' <remarks></remarks>
  Public Sub EnableDisableNode(ByVal Status As NodeStatus)
    Me.TreeView.SetSelectedNodeStatus(Status)
  End Sub ' EnableDisableNode

#End Region

#Region "Private Functions"

  ''' <summary>
  ''' Screen data manager
  ''' </summary>
  ''' <param name="ObjectId"></param>
  ''' <remarks></remarks>
  Private Sub ScreenDataManager(Optional ByVal ObjectId As Integer = 0)
    Select Case Me.ScreenMode
      Case ENUM_SCREEN_MODE.MODE_EDIT
        Me.GUI_ShowEditItem(ObjectId)
      Case ENUM_SCREEN_MODE.MODE_NEW
        Me.GUI_ShowNewItem()
    End Select
  End Sub

  ''' <summary>
  ''' Select node 
  ''' </summary>
  ''' <param name="TreeNode"></param>
  ''' <remarks></remarks>
  Private Sub SelectNode(ByVal TreeNode As TreeNode)
    Dim _db_object As Object

    _db_object = Nothing

    Me.NodeInEdition = TreeNode

    Me.m_treeview.SelectNode(TreeNode)

    ' Check's: 
    '    - Node is compatible in current form.
    '    - ScreenMode = MODE_EDIT.
    '    - Is not RootNode.
    If Me.TreeView_CurrentNodeIsCompatible() AndAlso Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT AndAlso TreeNode.Name <> Me.TreeView.GetRootName() Then
      _db_object = Me.GetObjectFromTreeNode(TreeNode)

      If _db_object Is Nothing Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8087), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)

        Return
      End If
    End If

    Me.BindTreeNodeToEdit(TreeNode.Tag, _db_object)

  End Sub ' SelectNode

  ''' <summary>
  ''' check if is necessary check changes
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function MustBeCheckChanges(ByVal TreeNode As TreeNode) As Boolean

    ' Check if exist node selected previously
    If Me.TreeView.SelectedNode Is Nothing Then
      Return False
    End If

    ' Check if last selected is the same node 
    If Me.TreeView.SelectedNode.Equals(TreeNode) Then
      Return False
    End If

    ' Check if last selected is Root
    If Me.TreeView.SelectedNode.Name = Me.TreeView.GetRootName() Then
      Return False
    End If

    Return True

  End Function ' MustBeCheckChanges

  ''' <summary>
  ''' Bind jackpot config user control
  ''' </summary>
  ''' <param name="Jackpot"></param>
  ''' <remarks></remarks>
  Private Sub BindJackpotConfigNodeToEdit(ByRef Jackpot As Jackpot)

    Dim _jackpot_id As Integer

    _jackpot_id = -1

    If Not Jackpot Is Nothing Then
      _jackpot_id = Jackpot.Id
    End If

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      Me.CurrentUcEdit = New uc_jackpot_config(Me, IIf(_jackpot_id > 0, uc_treeview_edit_base.SHOW_MODE.CloneItem, uc_treeview_edit_base.SHOW_MODE.NewItem), Jackpot)
    Else
      ' Edit jackpot
      If Jackpot Is Nothing Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8087), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
      End If

      Me.CurrentUcEdit = New uc_jackpot_config(Me, uc_treeview_edit_base.SHOW_MODE.EditItem, Jackpot)
    End If
  End Sub ' BindJackpotConfigNodeToEdit

  ''' <summary>
  ''' Bind jackpot viewer config user control
  ''' </summary>
  ''' <param name="JackpotViewer"></param>
  ''' <remarks></remarks>
  Private Sub BindJackpotViewerConfigNodeToEdit(ByRef JackpotViewer As JackpotViewer)
    Dim _jackpot_viewer_id As Integer

    _jackpot_viewer_id = Jackpot.DEFAULT_ID_VALUE

    If Not JackpotViewer Is Nothing Then
      _jackpot_viewer_id = JackpotViewer.Id
    End If

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      Me.CurrentUcEdit = New uc_jackpot_viewer_config(Me, uc_treeview_edit_base.SHOW_MODE.NewItem, JackpotViewer)
    Else
      ' Edit jackpot
      If JackpotViewer Is Nothing Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8087), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
      End If

      Me.CurrentUcEdit = New uc_jackpot_viewer_config(Me, uc_treeview_edit_base.SHOW_MODE.EditItem, JackpotViewer)
    End If
  End Sub ' BindJackpotViewerConfigNodeToEdit

#End Region

#Region "Events"

  ''' <summary>
  ''' Cacth key press event
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub frm_treeview_edit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress


    Select Case e.KeyChar
      Case Chr(Keys.Enter)
        If GUI_Button(ENUM_BUTTON.BUTTON_OK).Enabled Then
          Call btn_ok_ClickEvent()
        End If
      Case Chr(Keys.Escape)
        Call btn_cancel_ClickEvent()
      Case Else
    End Select

  End Sub ' frm_treeview_edit_KeyPress

  ''' <summary>
  ''' Catch keyup event
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub frm_treeview_edit_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
    If GUI_KeyUp(sender, e) Then
      Return
    End If

  End Sub ' frm_treeview_edit_KeyUp

  ''' <summary>
  ''' Select node event
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Public Overridable Sub TreeView_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles m_treeview.AfterSelect

    If e.Node.Name <> Me.TreeView.GetRootName() Then
      Me.ScreenDataManager(CInt(e.Node.Name))
    End If

    Me.SelectNode(e.Node)
    Me.TreeView.Refresh()

  End Sub ' TreeView_AfterSelect


  Public Overridable Sub Treeview_BeforeSelect(sender As Object, e As TreeViewCancelEventArgs) Handles m_treeview.BeforeSelect

    ' Ignore changes if its same node
    If Me.MustBeCheckChanges(e.Node) Then

      ' Node selection manager
      If Me.DiscardChanges() Then
        ' Discard treeview changes only when user discard changes
        If Me.IsDiscardChanges Then
          Me.DiscardTreeViewChanges(e.Node)
        End If
      Else
        e.Cancel = True
      End If
    End If

  End Sub

  ''' <summary>
  ''' Treeview node moues click
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Public Overridable Sub TreeView_NodeMouseClick(sender As Object, e As TreeNodeMouseClickEventArgs) Handles m_treeview.NodeMouseClick

    ' Only if TreeNode clicked is not new
    If Not Me.TreeView.IsNewNode(e.Node) Then
      ' Only if button click is right button 
      If e.Button = MouseButtons.Right Then
        e.Node.ContextMenu = Me.TreeView_GetContextMenu(e.Node.Name, e.Node.Tag)
        Me.TreeView.SelectedNode = e.Node
      End If
    End If

  End Sub ' TreeView_NodeMouseClick

  ''' <summary>
  ''' Get specific context menu
  ''' </summary>
  ''' <param name="ObjectId"></param>
  ''' <param name="Type"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overridable Function TreeView_GetContextMenu(ByVal ObjectId As String, ByVal Type As EditSelectionItemType) As ContextMenu
    ' Default context menu
    Return Nothing
  End Function

  ''' <summary>
  ''' Discard treeview changes
  ''' </summary>
  ''' <param name="TreeNode"></param>
  ''' <remarks></remarks>
  Public Overridable Sub DiscardTreeViewChanges(ByRef TreeNode As TreeNode)

    Me.TreeView.RemoveNewNode()
    Me.IsDiscardChanges = False
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

  End Sub ' DiscardTreeViewChanges

  ''' <summary>
  ''' Current node is compatible with current form
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Overridable Function TreeView_CurrentNodeIsCompatible() As Boolean
    Return True
  End Function ' TreeView_CurrentNodeIsCompatible

  ''' <summary>
  ''' Click event manager
  ''' </summary>
  ''' <remarks></remarks>
  Public Overridable Sub ClickEventManager(ByVal Type As ENUM_BUTTON_CLICK_EVENT_TYPE)
    ' Nothing todo
  End Sub ' ClickEventManager

  ''' <summary>
  ''' Refresh Treeview base
  ''' </summary>
  ''' <remarks></remarks>
  Public Overridable Sub RefreshTreeView()

    ' Reload TreeView
    Me.InitTreeView()

    ' Select new node
    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
      Me.TreeView.SelectedNode = Me.TreeView.RootNode.LastNode
    End If

    Me.TreeView.SelectedNode.Expand()
  End Sub

#End Region

End Class
