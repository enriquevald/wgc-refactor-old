'-------------------------------------------------------------------
' Copyright � 2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_message
'
' DESCRIPTION : Displays a form with a message.
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-JAN-2009  RRT    Initial version
' 29-MAR-2012  MPO    User session: Added message box with timer --> ShowTimer
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region " Imports "

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Windows.Forms

#End Region

Public Class frm_message
  Inherits frm_base

#Region " External Prototypes "

  Public Declare Function Common_SystemShutdown Lib "CommonBase" (ByVal ShutdownType As Integer)

#End Region

#Region " Constants "

#End Region

#Region " Structures "

#End Region

#Region " Members "

  Dim m_title As String
  Dim m_message As String

  Private Shared m_frm_message As frm_message
  Private Shared m_seconds As Int16 = 0
  Private Shared m_canceled As Boolean = False

#End Region

#Region " Delegates "


#End Region

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    'Me.FormId = ENUM_FORM.FORM_CARD_RECORD

    'Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    Call MyBase.GUI_InitControls()

    Me.btn_cancel.Text = GLB_NLS_GUI_CONTROLS.GetString(443)
    Me.lbl_seconds.Text = String.Format("{0} {1} {2}", GLB_NLS_GUI_CONTROLS.GetString(441), m_seconds, GLB_NLS_GUI_CONTROLS.GetString(442))
    Me.lbl_message.Text = m_message
    ' RCI & MPO 02-APR-2012: If m_title is empty, the assignment to Me.Text closes the window, that's why we assign an space.
    If String.IsNullOrEmpty(m_title) Then
      m_title = " "
    End If
    Me.Text = m_title

  End Sub 'GUI_InitControls

  ' PURPOSE: Given a type, the function returns if it needs to be audited
  '
  '  PARAMS:
  '     - INPUT:
  '         - AuditType: AUDIT_FLAGS that wants to be audited
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_HasToBeAudited(ByVal AuditType As AUDIT_FLAGS) As Boolean

    Return False

  End Function

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

  End Sub ' GUI_SetInitialFocus

#End Region

#Region " Public Functions "

  ' PURPOSE: Form constructor method.
  '
  '  PARAMS:
  '     - INPUT:
  '         - mdiparent
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub New(ByVal Title As String, ByVal Message As String, ByVal ShowWaitCursor As Boolean)

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    SetMessage(Message, Title)

    pnl_buttons.Visible = False
    img.Visible = False

    If ShowWaitCursor Then
      Windows.Forms.Cursor.Current = Cursors.WaitCursor
    End If

  End Sub ' New

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub

  Public Shared Function ShowTimer(ByVal Owner As Form, ByVal TimeSecond As Int16, ByVal Message As String, ByVal Title As String) As System.Windows.Forms.DialogResult

    m_canceled = False
    m_seconds = TimeSecond

    m_frm_message = New frm_message()
    m_frm_message.img.Image = System.Drawing.SystemIcons.Warning.ToBitmap()
    m_frm_message.SetMessage(Message, Title)
    m_frm_message.Timer.Interval = 1000
    m_frm_message.Timer.Enabled = True

    m_frm_message.ShowDialog(Owner)

    If m_canceled Then
      Return Windows.Forms.DialogResult.Cancel
    Else
      Return Windows.Forms.DialogResult.None
    End If

  End Function

  Public Sub SetMessage(ByVal Message As String, ByVal Title As String)

    m_message = Message
    m_title = Title

  End Sub

#End Region

#Region " Private Functions "

#End Region

#Region " Events "

  Private Sub Timer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer.Tick

    m_seconds -= 1
    Me.lbl_seconds.Text = String.Format("{0} {1} {2}", GLB_NLS_GUI_CONTROLS.GetString(441), m_seconds, GLB_NLS_GUI_CONTROLS.GetString(442))
    If m_seconds = 0 Then
      Me.Close()
    End If

  End Sub

  Private Sub btn_cancel_ClickEvent() Handles btn_cancel.ClickEvent

    m_canceled = True
    Me.Timer.Enabled = False
    Me.Close()

  End Sub

#End Region

End Class
