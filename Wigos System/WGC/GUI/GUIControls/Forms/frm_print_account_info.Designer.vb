<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_print_account_info
  Inherits GUI_Controls.frm_base

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
    Me.imgaux = New System.Windows.Forms.PictureBox
    Me.img = New System.Windows.Forms.PictureBox
    Me.lbl_message = New System.Windows.Forms.Label
    Me.pnl_buttons = New System.Windows.Forms.Panel
    Me.btn_print = New GUI_Controls.uc_button
    Me.btn_cancel = New GUI_Controls.uc_button
    Me.chk_scan_doc = New System.Windows.Forms.CheckBox
    Me.TableLayoutPanel1.SuspendLayout()
    CType(Me.imgaux, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.img, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.pnl_buttons.SuspendLayout()
    Me.SuspendLayout()
    '
    'TableLayoutPanel1
    '
    Me.TableLayoutPanel1.BackColor = System.Drawing.SystemColors.Window
    Me.TableLayoutPanel1.ColumnCount = 2
    Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
    Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
    Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
    Me.TableLayoutPanel1.Controls.Add(Me.imgaux, 0, 2)
    Me.TableLayoutPanel1.Controls.Add(Me.img, 0, 0)
    Me.TableLayoutPanel1.Controls.Add(Me.lbl_message, 1, 0)
    Me.TableLayoutPanel1.Controls.Add(Me.pnl_buttons, 1, 2)
    Me.TableLayoutPanel1.Controls.Add(Me.chk_scan_doc, 1, 1)
    Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
    Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
    Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
    Me.TableLayoutPanel1.RowCount = 3
    Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
    Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37.0!))
    Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46.0!))
    Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
    Me.TableLayoutPanel1.Size = New System.Drawing.Size(441, 161)
    Me.TableLayoutPanel1.TabIndex = 6
    '
    'imgaux
    '
    Me.imgaux.BackColor = System.Drawing.SystemColors.Control
    Me.imgaux.Dock = System.Windows.Forms.DockStyle.Fill
    Me.imgaux.ErrorImage = Nothing
    Me.imgaux.InitialImage = Nothing
    Me.imgaux.Location = New System.Drawing.Point(0, 115)
    Me.imgaux.Margin = New System.Windows.Forms.Padding(0)
    Me.imgaux.Name = "imgaux"
    Me.imgaux.Size = New System.Drawing.Size(58, 46)
    Me.imgaux.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
    Me.imgaux.TabIndex = 8
    Me.imgaux.TabStop = False
    '
    'img
    '
    Me.img.Dock = System.Windows.Forms.DockStyle.Fill
    Me.img.Location = New System.Drawing.Point(0, 0)
    Me.img.Margin = New System.Windows.Forms.Padding(0)
    Me.img.Name = "img"
    Me.img.Size = New System.Drawing.Size(58, 78)
    Me.img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
    Me.img.TabIndex = 4
    Me.img.TabStop = False
    '
    'lbl_message
    '
    Me.lbl_message.AutoSize = True
    Me.lbl_message.Dock = System.Windows.Forms.DockStyle.Fill
    Me.lbl_message.Location = New System.Drawing.Point(61, 0)
    Me.lbl_message.Name = "lbl_message"
    Me.lbl_message.Size = New System.Drawing.Size(377, 78)
    Me.lbl_message.TabIndex = 3
    Me.lbl_message.Text = "xText"
    Me.lbl_message.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'pnl_buttons
    '
    Me.pnl_buttons.BackColor = System.Drawing.SystemColors.Control
    Me.pnl_buttons.Controls.Add(Me.btn_print)
    Me.pnl_buttons.Controls.Add(Me.btn_cancel)
    Me.pnl_buttons.Dock = System.Windows.Forms.DockStyle.Fill
    Me.pnl_buttons.Location = New System.Drawing.Point(58, 115)
    Me.pnl_buttons.Margin = New System.Windows.Forms.Padding(0)
    Me.pnl_buttons.Name = "pnl_buttons"
    Me.pnl_buttons.Size = New System.Drawing.Size(383, 46)
    Me.pnl_buttons.TabIndex = 6
    '
    'btn_print
    '
    Me.btn_print.Anchor = System.Windows.Forms.AnchorStyles.Right
    Me.btn_print.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_print.Location = New System.Drawing.Point(190, 10)
    Me.btn_print.Name = "btn_print"
    Me.btn_print.Size = New System.Drawing.Size(90, 30)
    Me.btn_print.TabIndex = 0
    Me.btn_print.ToolTipped = False
    Me.btn_print.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_cancel
    '
    Me.btn_cancel.Anchor = System.Windows.Forms.AnchorStyles.Right
    Me.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btn_cancel.Location = New System.Drawing.Point(286, 10)
    Me.btn_cancel.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.btn_cancel.Name = "btn_cancel"
    Me.btn_cancel.Size = New System.Drawing.Size(90, 30)
    Me.btn_cancel.TabIndex = 2
    Me.btn_cancel.ToolTipped = False
    Me.btn_cancel.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'chk_scan_doc
    '
    Me.chk_scan_doc.AutoSize = True
    Me.chk_scan_doc.Dock = System.Windows.Forms.DockStyle.Fill
    Me.chk_scan_doc.Location = New System.Drawing.Point(61, 81)
    Me.chk_scan_doc.Name = "chk_scan_doc"
    Me.chk_scan_doc.Size = New System.Drawing.Size(377, 31)
    Me.chk_scan_doc.TabIndex = 1
    Me.chk_scan_doc.Text = "xIncludeScannedDocuments"
    Me.chk_scan_doc.UseVisualStyleBackColor = True
    '
    'frm_print_account_info
    '
    Me.AcceptButton = Me.btn_print
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.CancelButton = Me.btn_cancel
    Me.ClientSize = New System.Drawing.Size(441, 165)
    Me.Controls.Add(Me.TableLayoutPanel1)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frm_print_account_info"
    Me.Padding = New System.Windows.Forms.Padding(0, 0, 0, 4)
    Me.ShowIcon = False
    Me.ShowInTaskbar = False
    Me.Text = "frm_print_account_info"
    Me.TopMost = True
    Me.TableLayoutPanel1.ResumeLayout(False)
    Me.TableLayoutPanel1.PerformLayout()
    CType(Me.imgaux, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.img, System.ComponentModel.ISupportInitialize).EndInit()
    Me.pnl_buttons.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
  Friend WithEvents img As System.Windows.Forms.PictureBox
  Friend WithEvents lbl_message As System.Windows.Forms.Label
  Friend WithEvents imgaux As System.Windows.Forms.PictureBox
  Friend WithEvents pnl_buttons As System.Windows.Forms.Panel
  Friend WithEvents btn_print As GUI_Controls.uc_button
  Friend WithEvents btn_cancel As GUI_Controls.uc_button
  Friend WithEvents chk_scan_doc As System.Windows.Forms.CheckBox
End Class
