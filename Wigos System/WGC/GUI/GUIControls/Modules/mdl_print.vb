Imports System.Drawing.Printing
Imports GUI_CommonMisc

'XVV -> Add NameSpace Windows.Forms,solve error with IWin32Window variables definitions
Imports System.Windows.Forms
Imports System.Drawing
'XVV ---------------------------------------

Public Module mdl_print

  Public Class CLASS_GRID_PRINT
    Private printFont As Font
    Private printFontHeader As Font
    Private printFontGridHeader As Font
    Private GridToPrint As uc_grid
    Private WithEvents pd As PrintDocument
    Private WithEvents pd2 As PrintDocument

    'XVV 17/04/2007
    'Change Function for Procedure because this function don't return value. And generate compile warnings
    'Public Function PrintGridPicture(ByVal Grid As uc_grid)
    Public Sub PrintGridPicture(ByVal Grid As uc_grid)
      Try
        Try
          GridToPrint = Grid
          printFont = New Font("Arial", 10, FontStyle.Regular)
          printFontHeader = New Font("Courier", 16, FontStyle.Bold)
          printFontGridHeader = New Font("Arial", 10, FontStyle.Bold)

          'printFont2 = New Font("Arial", 16, FontStyle.Bold)

          pd = New PrintDocument()
          pd2 = New PrintDocument()

          Dim ppd As New System.Windows.Forms.PrintPreviewDialog()

          ppd.Document = pd

          ppd.ShowDialog()

          ppd.Document = pd2
          ppd.ShowDialog()

        Finally

        End Try
      Catch ex As Exception
        MessageBox.Show(ex.Message)
      End Try
    End Sub

    ' The PrintPage event is raised for each page to be printed.
    Private Sub dg_PrintPage(ByVal sender As Object, ByVal ev As PrintPageEventArgs) Handles pd.PrintPage
      Dim linesPerPage As Single = 0
      Dim yPos As Single = 0
      Dim count As Integer = 0
      Dim leftMargin As Single = ev.MarginBounds.Left
      Dim topMargin As Single = ev.MarginBounds.Top
      Dim line As String = Nothing

      Static page As Integer = 0

      ' Calculate the number of lines per page.
      linesPerPage = ev.MarginBounds.Height / printFont.GetHeight(ev.Graphics)

      'Print Header
      line = "LYKOS"
      yPos = topMargin
      ev.Graphics.DrawString(line, printFontHeader, Brushes.Black, leftMargin, yPos, New StringFormat())

      topMargin = topMargin + printFont.GetHeight(ev.Graphics)

      'XVV 13/04/2007 
      'Add Nothing value to variable declaration 
      Dim img As System.Drawing.Image = Nothing

      '    Dim grf As System.Drawing.Graphics
      'img = GridToPrint.Picture
      'ev.Graphics.DrawImage(img, 50, topMargin + 100, New Rectangle(0, 0, 500, 600), GraphicsUnit.Pixel)
      ev.Graphics.DrawImage(img, 50, topMargin + 50)

      '' Print each line of the file.

      'Print Footer

    End Sub

    Private Sub dg_PrintGrid(ByVal sender As Object, ByVal ev As PrintPageEventArgs) Handles pd2.PrintPage
      Dim linesPerPage As Single = 0
      Dim yPos As Single = 0
      Dim count As Integer = 0
      Dim leftMargin As Single = ev.MarginBounds.Left
      Dim topMargin As Single = ev.MarginBounds.Top
      Dim idx_row As Integer = 0
      Dim line As String = Nothing
      Dim scale As Double = 5.0 / 64.0
      Dim Rows As Integer = 0
      Dim ColPosLeft() As Integer
      Dim idx_col As Integer
      Dim start_row As Integer
      Dim end_row As Integer

      'Print Header
      line = "LYKOS"
      yPos = topMargin
      ev.Graphics.DrawString(line, printFontHeader, Brushes.Black, leftMargin, yPos, New StringFormat())

      topMargin = topMargin + printFont.GetHeight(ev.Graphics)

      ReDim ColPosLeft(GridToPrint.NumColumns)
      ColPosLeft(0) = leftMargin
      For idx_col = 1 To GridToPrint.NumColumns - 1
        ColPosLeft(idx_col) = ColPosLeft(idx_col - 1) + GridToPrint.Column(idx_col - 1).Width * scale
      Next

      yPos = topMargin + printFontHeader.GetHeight(ev.Graphics)

      start_row = 0
      end_row = GridToPrint.NumRows - 1
      If end_row > 50 Then
        end_row = 50
      End If

      'Print grid headers
      For idx_col = 0 To GridToPrint.NumColumns - 1
        If GridToPrint.flx_grid.FixedRows > 0 Then

        End If
        If GridToPrint.Column(idx_col).Width > 0 Then
          line = GridToPrint.Column(idx_col).Header(0).Text

          ev.Graphics.DrawString(line, printFontGridHeader, Brushes.Black, ColPosLeft(idx_col), yPos, New StringFormat())
        End If
      Next

      yPos = yPos + printFontGridHeader.GetHeight(ev.Graphics)

      For idx_row = start_row To end_row
        For idx_col = 0 To GridToPrint.NumColumns - 1
          If GridToPrint.Column(idx_col).Width > 0 Then
            line = GridToPrint.Cell(idx_row, idx_col).Value
            ev.Graphics.DrawRectangle(New Pen(Brushes.Black), New Rectangle(ColPosLeft(idx_col), yPos, _
                                    GridToPrint.Column(idx_col).Width, printFont.Height))

            ev.Graphics.DrawString(line, printFont, Brushes.Black, ColPosLeft(idx_col), yPos, New StringFormat())
          End If

        Next
        yPos = yPos + printFont.GetHeight(ev.Graphics)

      Next

    End Sub

    '  Private Function DrawLine(ByVal ev As PrintPageEventArgs)

    '  End Function


    '  private function imprimir_grilla(ByRef inicioX As Integer, ByRef finY As Integer, ByVal titulo1 As String, Optional ByVal titulo2 As String)

    '    Dim inicioY As Integer
    '    Dim inicioFor As Integer
    '    Dim ColInicio(10) As Integer

    '    If GridToPrint.NumRows = 1 Then Exit Function

    '    Printer.Print()

    '    'defino la escala
    '    Printer.ScaleMode = vbMillimeters

    '    'leo el tipo de papel y defino los limites
    '    hoja = Printer.PaperSize
    '    Select Case hoja
    '      Case vbPRPSLetter
    '            Printer.Scale (0, 0)-(216, 279)
    '      Case vbPRPSLegal
    '            Printer.Scale (0, 0)-(216, 356)
    '      Case vbPRPSA4
    '            Printer.Scale (0, 0)-(210, 297)
    '      Case vbPRPSUser
    '            Printer.Scale (0, 0)-(Printer.Height, Printer.Width)
    '    End Select

    '    'defino las coordenadas de inicio
    '    inicioX = (Printer.ScaleWidth - Printer.ScaleLeft - 200) \ 2
    '    inicioY = 64
    '    finY = Printer.ScaleHeight - 30

    '    'inicializo el tama�o e inicio de cada columna
    '    ColInicio(0) = 0

    '    For i = 1 To GridToPrint.NumColumns - 1
    '      ColInicio(i) = 0
    '      For J = 1 To i
    '        ColInicio(i) = ColInicio(i) + GridToPrint.ColData(J - 1)
    '      Next
    '    Next

    '    'calculo el total de paginas
    '    '********************************* VER
    '    If GridToPrint.NumRows - 1 < 43 Then
    '      total_pag = 1
    '    Else
    '      Filas = GridToPrint.NumRows - 42
    '      AreaImpresion = finY - 18
    '      FilasXhoja = AreaImpresion \ 6
    '      total_pag = (Filas \ FilasXhoja) + 2
    '    End If

    '    pagina = 1

    '    GoSub cabecera

    '    GoSub semicab

    '    GridToPrint.Row = 0

    '    'imprimo los datos
    '    Printer.Print()
    '    With Printer
    '      .Font.Name = "tahoma"
    '      .Font.Size = 8
    '      .Font.Bold = False
    '      .Font.Italic = False
    '    End With
    '    i = 0
    '    Do While GridToPrint.Row < GridToPrint.NumRows - 1
    '      GridToPrint.Row = GridToPrint.Row + 1
    '      If inicioY + 17 + i > finY + 1 Then
    '            GoSub Separadores
    '        Printer.NewPage()
    '        inicioY = 10

    '            GoSub semicab
    '        i = 0
    '        pagina = pagina + 1

    '        'preparo la letra para seguir
    '        Printer.Print()
    '        With Printer
    '          .Font.Name = "tahoma"
    '          .Font.Size = 8
    '          .Font.Bold = False
    '          .Font.Italic = False
    '        End With
    '      End If


    '      For J = 1 To GridToPrint.NumColumns
    '        With GridToPrint
    '          .Col = J - 1
    '          Printer.Font.Bold = GridToPrint.CellFontBold
    '          Printer.Font.Italic = GridToPrint.CellFontItalic
    '          If .FixedAlignment(J - 1) <> 4 Then
    '            Printer.CurrentX = inicioX + ColInicio(J - 1) + 1
    '            Printer.CurrentY = inicioY + 10 + i
    '            If Printer.TextWidth(.Text) > GridToPrint.ColData(J - 1) Then
    '              Printer.Print(CortarTexto(.Text, GridToPrint.ColData(J - 1)))
    '            Else
    '              Printer.Print(.Text)
    '            End If
    '          Else
    '                        Printer.CurrentX = inicioX + ColInicio(J - 1) + 
    '(GridToPrint.ColData(J - 1) - 2 - Printer.TextWidth(.Text)) \ 2
    '            Printer.CurrentY = inicioY + 10 + i
    '            Printer.Print(.Text)
    '          End If
    '        End With
    '      Next
    '      i = i + 6
    '    Loop

    '    finY = inicioY + 8 + i
    '    GoSub Separadores

    '    'Final de la impresi�n
    '    Exit Function

    'cabecera:
    '    Printer.Print()

    '    'selecciono el tipo de letra para la cabecera
    '    Printer.Print()
    '    With Printer
    '      .Font.Name = "arial"
    '      .Font.Size = 10
    '      .Font.Bold = True
    '      .Font.Italic = False
    '    End With

    '    'imprimo el logo del sistema
    '    On Error Resume Next
    '    Printer.PaintPicture(LoadPicture(PathBase + "\bb.jpg"), inicioX + 10, inicioY - 55, 17, 17)
    '    Err.Clear()
    '    Printer.CurrentX = inicioX + 50
    '    Printer.CurrentY = inicioY - 54
    '    Printer.Print("Circulo de Suboficiales y Guardias")

    '    Printer.CurrentX = inicioX + 50
    '    Printer.CurrentY = inicioY - 49
    '    Printer.Print("57 N� 1031 e/15 y 16 - TE: 425-7656")

    '    Printer.CurrentX = inicioX + 50
    '    Printer.CurrentY = inicioY - 44
    '    Printer.Print("La Plata (1900)")

    '    Printer.CurrentX = inicioX + 170
    '    Printer.CurrentY = inicioY - 54
    '    Printer.Print(Format(GUI_GetDateTime, fmt_fe))

    '    Printer.CurrentX = inicioX + 170
    '    Printer.CurrentY = inicioY - 49
    '    Printer.Print(Format(GUI_GetDateTime, fmt_hs))

    '    'selecciono el tipo de letra para el titulo
    '    Printer.Print()
    '    With Printer
    '      .Font.Name = "arial"
    '      .Font.Size = 16
    '      .Font.Bold = True
    '      .Font.Italic = False
    '    End With

    '    Printer.CurrentX = inicioX + 10
    '    Printer.CurrentY = inicioY - 30
    '    Printer.Print(titulo1)

    '    If titulo2 <> "" Then
    '      Printer.CurrentX = inicioX + 10
    '      Printer.CurrentY = inicioY - 20
    '      Printer.Print(titulo2)
    '    End If
    '    Return

    'semicab:
    '    'defino el tipo de letra para la cabecera de la tabla
    '    Printer.Print()
    '    With Printer
    '      .Font.Name = "arial"
    '      .Font.Size = 10
    '      .Font.Bold = True
    '      .Font.Italic = False
    '    End With
    '    fila = GridToPrint.Row
    '    GridToPrint.Row = 0
    '    For i = 1 To GridToPrint.NumColumns
    '      With GridToPrint
    '        .Col = i - 1
    '        If .FixedAlignment(.Col) <> 4 Then
    '          Printer.CurrentX = inicioX + ColInicio(i - 1) + 1
    '        Else
    '          Printer.CurrentX = inicioX + ColInicio(i - 1) + (GridToPrint.ColData(i - 1) - 2 - Printer.TextWidth(.Text)) \ 2
    '        End If
    '        Printer.CurrentY = inicioY
    '        Printer.Print(.Text)
    '      End With
    '    Next
    '    GridToPrint.Row = fila
    '    Return

    'Separadores:
    '    'imprimo separadores

    '    Printer.DrawWidth = 4
    '    'horizontales
    '    Printer.Line (inicioX, inicioY - 3)-(inicioX + 200, inicioY - 3), vbBlack
    '    Printer.Line (inicioX, inicioY + 7)-(inicioX + 200, inicioY + 7), vbBlack

    '    Printer.DrawWidth = 1

    '    Printer.Print()

    '    With Printer
    '      .Font.Name = "ARIAL"
    '      .Font.Size = 10
    '      .Font.Bold = True
    '      .Font.Italic = False
    '    End With

    '    'imprime numero de pagina
    '    linea = "P�gina " + CStr(pagina)
    '    Printer.CurrentX = inicioX + (200 - Printer.TextWidth(linea)) \ 2
    '    Printer.CurrentY = Printer.ScaleHeight - 20

    '    Printer.Print(linea)

    '    Return


    '  End Function

  End Class

  Public Function SetReportOptions() As Boolean
    Dim report_dialog As New PrintDialog()
    Dim dialog_result As DialogResult

    report_dialog.PrinterSettings = GLB_PrinterSettings
    dialog_result = report_dialog.ShowDialog

    If dialog_result <> DialogResult.OK Then

      Return False
    End If

    GLB_PrinterSettings = report_dialog.PrinterSettings

    Return True

  End Function


End Module



