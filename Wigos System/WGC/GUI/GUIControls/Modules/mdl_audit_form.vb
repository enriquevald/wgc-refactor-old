'-------------------------------------------------------------------
' Copyright © 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_audit_form
' DESCRIPTION:   Module created to audit user activities on forms.
'                   
' AUTHOR:        Laura Jimenez Masagué
' CREATION DATE: 14-NOV-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 14-NOV-2013  LJM    Initial version
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_Controls.frm_base
Imports GUI_CommonMisc.mdl_log

Imports System.Text

Public Module mdl_audit_form

#Region "Enums"

  <Flags()> _
  Public Enum AUDIT_FLAGS
    NONE = 0
    ALL = &HFFFFFFFF
    ACCESS = 1
    SEARCH = 2
    PRINT = 4
    EXCEL = 8
  End Enum

#End Region

#Region "Public functions"

  ' PURPOSE: Audits the searches, excel, and prints on forms
  '
  '  PARAMS:
  '     - INPUT:
  '           - FormName: text of the form
  '           - ButtonType: Button it has been clicked
  '           - PrintData: Values of the filters in case of searches
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub AuditForm(ByVal FormType As AUDIT_FLAGS, ByVal FormName As String)
    Call AuditForm(FormType, FormName, Nothing, Nothing)
  End Sub

  Public Sub AuditForm(ByVal FormType As AUDIT_FLAGS, ByVal FormName As String, _
                     ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    Call AuditForm(FormType, FormName, PrintData, Nothing)

  End Sub

  Public Sub AuditForm(ByVal FormType As AUDIT_FLAGS, ByVal FormName As String, _
                       ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, ByVal ButtonName As String)
    Dim _audit_type As String

    _audit_type = ButtonName

    Try
      Select Case FormType
        Case AUDIT_FLAGS.SEARCH
          Call AuditForm(FormName, GLB_NLS_GUI_AUDITOR.GetString(478), PrintData)

        Case AUDIT_FLAGS.EXCEL
          Call AuditForm(FormName, IIf(_audit_type IsNot Nothing, _audit_type, GLB_NLS_GUI_AUDITOR.GetString(480)))

        Case AUDIT_FLAGS.PRINT
          Call AuditForm(FormName, IIf(_audit_type IsNot Nothing, _audit_type, GLB_NLS_GUI_AUDITOR.GetString(481)))

        Case AUDIT_FLAGS.ACCESS
          Call AuditForm(FormName, IIf(_audit_type IsNot Nothing, _audit_type, GLB_NLS_GUI_AUDITOR.GetString(479)))

        Case Else

      End Select
    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            FormName, _
                            "mdl_audit_form.AuditForm", _
                            "", _
                            "", _
                            "", _
                            ex.Message)
    End Try
  End Sub

#End Region

#Region "Private functions"

  ' PURPOSE: Checks if the filter is empty
  '
  '  PARAMS:
  '     - INPUT:
  '           - FilterValue
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - True: If the filter has any value
  '     - False: If the filter is empty
  Private Function FilterIsNull(ByVal FilterValue As Object) As Boolean

    If FilterValue Is Nothing OrElse IsDBNull(FilterValue) Then
      Return True
    ElseIf String.IsNullOrEmpty(FilterValue) Or FilterValue = "---" Then
      Return True
    End If

    Return False

  End Function

  ' PURPOSE: Audits actions on forms wich are not searches
  '
  '  PARAMS:
  '     - INPUT:
  '           - FormName: text of the form
  '           - AuditType: text of the action the user made
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AuditForm(ByVal FormName As String, ByVal AuditType As String)
    Dim _audit_object As CLASS_AUDITOR_DATA
    Dim _text As New StringBuilder

    _text.Append(AuditType)
    _text.Append(FormName)

    _audit_object = New CLASS_AUDITOR_DATA(AUDIT_CODE_USER_ACTIVITY)
    _audit_object.SetName(0, _text.ToString())

    _audit_object.Notify(CurrentUser.GuiId, CurrentUser.Id, CurrentUser.Name, _
                         CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, 0)
  End Sub

  ' PURPOSE: Audits the searches on a form
  '
  '  PARAMS:
  '     - INPUT:
  '           - FormName: text of the form
  '           - PrintData: A CLASS_PRINT_DATA wich contains all the filter values
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AuditForm(ByVal FormName As String, ByVal ActionName As String, ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    Dim _audit_object As CLASS_AUDITOR_DATA
    Dim _filter As GUI_Reports.PrintDataset.PrintFilterRow
    Dim _offset As Integer
    Dim _text As New System.Text.StringBuilder

    _filter = PrintData.Filter()
    _offset = PrintData.GetMaxNumFilters()

    _text.Append(ActionName)
    _text.Append(" ")
    _text.Append(FormName)

    _audit_object = New CLASS_AUDITOR_DATA(AUDIT_CODE_USER_ACTIVITY)
    _audit_object.SetName(0, _text.ToString())
    _text.Length = 0

    For _cursor As Integer = 0 To _offset - 1
      If Not FilterIsNull(_filter.ItemArray(_cursor + _offset)) Then

        _text.Append(_filter.ItemArray(_cursor))
        _text.Append(" ")
        _text.Append(_filter.ItemArray(_cursor + _offset))

        _audit_object.SetField(0, _text.ToString(), GLB_NLS_GUI_AUDITOR.GetString(477))
        _text.Length = 0

      End If
    Next

    _audit_object.Notify(CurrentUser.GuiId, _
                         CurrentUser.Id, _
                         CurrentUser.Name, _
                         CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.GENERIC, _
                         0)
  End Sub
#End Region

End Module
