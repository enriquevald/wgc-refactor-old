'-------------------------------------------------------------------
' Copyright � 2006 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_games.vb  
'
' DESCRIPTION:   
'
' AUTHOR:        Quim Morillo, Andreu Juli�
'
' CREATION DATE: 14-MAR-2006
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-MAR-2006  AJQ    Price Levels
' 14-MAR-2006  QMA    Initial version
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations

Public Module mdl_games


  Private GLB_TableGames As DataTable = Nothing
  Private GLB_TablePriceLevels As DataTable = Nothing

  Private GLB_GamesTime As DateTime
  Private GLB_PriceLevelsTime As DateTime

  Private GLB_GamesMutex As System.Threading.Mutex = New System.Threading.Mutex(False)
  Private GLB_PriceLevelsMutex As System.Threading.Mutex = New System.Threading.Mutex(False)


  Public Function GetGames() As DataTable
    Dim data_read As Boolean
    'XVV Variable not use Dim row As DataRow
    'XVV Variable not use Dim level As Integer
    Dim table As DataTable

    Try

      table = Nothing

      GLB_GamesMutex.WaitOne()

      data_read = True

      If Not IsNothing(GLB_TableGames) Then
        If Now.Subtract(GLB_GamesTime).TotalMinutes < 5 Then
          data_read = False
        End If
      End If

      If data_read Then
        If Not IsNothing(GLB_TableGames) Then
          GLB_TableGames.Dispose()
        End If
        GLB_TableGames = GUI_GetTableUsingSQL("SELECT * FROM GAMES", 1000)
        GLB_GamesTime = Now
      End If

      table = GLB_TableGames.Copy

    Finally
      GLB_GamesMutex.ReleaseMutex()
    End Try

    Return table

  End Function


  Public Function GetPriceLevels(Optional ByVal GameId As Integer = 0) As DataTable
    Dim data_read As Boolean
    Dim row As DataRow
    'XVV Variable not use Dim level As Integer
    Dim table As DataTable
    Dim sql As String
    Dim game_found As Boolean
    Dim force_read As Boolean
    Dim idx As Integer

    Try
      table = Nothing
      force_read = False

      GLB_PriceLevelsMutex.WaitOne()

      For idx = 0 To 1

        If Not (table Is Nothing) Then
          table.Dispose()
          table = Nothing
        End If

        data_read = True

        If Not IsNothing(GLB_TablePriceLevels) Then
          If Now.Subtract(GLB_PriceLevelsTime).TotalMinutes < 5 Then
            data_read = False
          End If
        End If

        If data_read Then
          If Not IsNothing(GLB_TablePriceLevels) Then
            GLB_TablePriceLevels.Dispose()
          End If

          sql = String.Empty
          sql = sql & "    SELECT   *"
          sql = sql & "      FROM   GAME_PRICE_LEVELS"
          sql = sql & "  ORDER BY   GPL_GAME_ID ASC"
          sql = sql & "           , GPL_PRICE_LEVEL_ID ASC"

          GLB_TablePriceLevels = GUI_GetTableUsingSQL(sql, 5000)
          GLB_PriceLevelsTime = Now
        End If

        If GameId = 0 Then
          table = GLB_TablePriceLevels.Copy

          Exit Try
        End If

        table = GLB_TablePriceLevels.Clone

        game_found = False
        For Each row In GLB_TablePriceLevels.Rows
          If row.Item("GPL_GAME_ID") = GameId Then
            table.Rows.Add(row.ItemArray())
            game_found = True
          Else
            If game_found Then
              Exit Try
            End If
          End If
        Next

        If game_found Or data_read Then
          Exit Try
        End If

        force_read = True

      Next

    Finally
      GLB_PriceLevelsMutex.ReleaseMutex()
    End Try

    Return table

  End Function



End Module