'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_permissions_multisite.vb
' DESCRIPTION:   
' AUTHOR:        Marcos Piedra Osuna
' CREATION DATE: 04-MAR-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-MAR-2013  MPO    Initial version
' 05-MAR-2013  MPO    Added InitProfilesMultiSite
' 05-MAY-2014  HBB    Fixed Bug: WIG-1018 It is not possible print or export to excel in multisite mode
' 25-AUG-2014  AMF    Fixed Bug WIG-1197: Feature of form
' 04-DEC-2014  DHA    Added LCD message form permissions
' 14-JAN-2015  DCS    Added permissions form site Jackpot History 
' 18-MAY-2015  JML    Backlog Item 1749: Multisite-Multicurrency permisions
' 21-AGO-2015  FJC    Product BackLog Item: 3702
' 11-MAR-2016  JRC    Product Backlog Item 5491 Multiple Buckets
' 15-SEP-2016  ETP    Fixed bugs 17585 - 17607: Generic reports: add permissions config.
' 03-APR-2017  ETP    WIGOS - 116: CreditLines - Multisite.
' 19-SEP-2017  ETP    PBI 29621: AGG Data transfer [WIGOS-4458]
' 29-OCT-2017  LTC    [WIGOS-6134] Multisite Terminal meter adjustment
' 17-NOV-2017  LTC    Product Backlog Item 30849:Multisite: Reporte gen�rico terminales
' 21-NOV-2017  JML    Product Backlog Item 29621:AGG Data transfer [WIGOS-4458]
' 30-NOV-2017  RGR    PBI 30932:WIGOS-6393 GUI - Menu: User Interface improvements
' 07-DEC-2017  FGB    WIGOS-6802: AGG - Handpay report
' 14-DEC-2017  DPC    PBI 31072:[WIGOS-7111] AGG - EGM connection control
' -------------------------------------------------------------------

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_CommonOperations.CLASS_REGISTRY_DATA
Imports GUI_CommonOperations.CLASS_REGISTRY_DEFINITION
Imports GUI_Controls
Imports WSI.Common

Public Module mdl_permissions_multisite

    Public Sub InitProfilesMultiSite(ByRef MultiSiteForms As CLASS_GUI_FORMS)

        Call MultiSiteForms.Add(ENUM_FORM.FORM_USER_PROFILE_SEL, GLB_NLS_GUI_CONFIGURATION.Id(383), ENUM_FEATURES.NONE)
        'Call MultiSiteForms.Add(ENUM_FORM.FORM_MASTER_PROFILE_SEL, GLB_NLS_GUI_PLAYER_TRACKING.Id(2098))
        Call MultiSiteForms.Add(ENUM_FORM.FORM_USER_EDIT, GLB_NLS_GUI_CONFIGURATION.Id(325), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_MASTER_USER_EDIT, GLB_NLS_GUI_PLAYER_TRACKING.Id(2360), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_PROFILE_EDIT, GLB_NLS_GUI_CONFIGURATION.Id(318), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_MASTER_PROFILE_EDIT, GLB_NLS_GUI_PLAYER_TRACKING.Id(2103), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_GUI_AUDITOR, GLB_NLS_GUI_AUDITOR.Id(327), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_GENERAL_PARAMS, GLB_NLS_GUI_AUDITOR.Id(460), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_CASHIER_CONFIGURATION, GLB_NLS_GUI_CONFIGURATION.Id(201), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_ACCOUNT_SUMMARY, GLB_NLS_GUI_INVOICING.Id(229), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_ACCOUNT_MOVEMENTS, GLB_NLS_GUI_INVOICING.Id(228), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_SITES_MONITOR, GLB_NLS_GUI_PLAYER_TRACKING.Id(1760), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_SERVICE_MONITOR, GLB_NLS_GUI_SYSTEM_MONITOR.Id(201), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_SW_DOWNLOAD_VERSIONS, GLB_NLS_GUI_SW_DOWNLOAD.Id(201), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_SW_DOWNLOAD_DETAILS, GLB_NLS_GUI_SW_DOWNLOAD.Id(300), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_MISC_SW_BY_BUILD, GLB_NLS_GUI_SW_DOWNLOAD.Id(239), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_LICENCE_VERSIONS, GLB_NLS_GUI_SW_DOWNLOAD.Id(229), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_LICENCE_DETAILS, GLB_NLS_GUI_SW_DOWNLOAD.Id(251), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_ALARMS, GLB_NLS_GUI_ALARMS.Id(201), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_SITES_ALARMS, GLB_NLS_GUI_ALARMS.Id(455), ENUM_FEATURES.NONE)
        ' JML 28-MAY-2014
        ' Call MultiSiteForms.Add(ENUM_FORM.FORM_ALARM_INQUIRY_SOURCE, GLB_NLS_GUI_ALARMS.Id(468))
        Call MultiSiteForms.Add(ENUM_FORM.FORM_ALARM_EXECUTE_PATTERNS, GLB_NLS_GUI_ALARMS.Id(469), ENUM_FEATURES.NONE)

        Call MultiSiteForms.Add(ENUM_FORM.FORM_MULTISITE_CENTER_CONFIG, GLB_NLS_GUI_PLAYER_TRACKING.Id(1793), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_PLAYER_EDIT, GLB_NLS_GUI_PLAYER_TRACKING.Id(1711), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_ACCOUNT_ADJUSTMENT, GLB_NLS_GUI_CONFIGURATION.Id(70), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_PLAYS_SESSIONS, GLB_NLS_GUI_STATISTICS.Id(357), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_DATE_TERMINAL, GLB_NLS_GUI_INVOICING.Id(249), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_PROVIDERS_GAMES_SEL, GLB_NLS_GUI_PLAYER_TRACKING.Id(1964), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_PROVIDERS_GAMES_EDIT, GLB_NLS_GUI_PLAYER_TRACKING.Id(1965), ENUM_FEATURES.NONE)

        Call MultiSiteForms.Add(ENUM_FORM.FORM_ACCOUNTS_IMPORT, GLB_NLS_GUI_CLASS_II.Id(410), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_ACCOUNTS_IMPORT_BALANCES, GLB_NLS_GUI_PLAYER_TRACKING.Id(1232), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_ACCOUNTS_IMPORT_CLIENT_DATA, GLB_NLS_GUI_PLAYER_TRACKING.Id(1234), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_ACCOUNTS_IMPORT_LEVEL, GLB_NLS_GUI_PLAYER_TRACKING.Id(1235), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_ACCOUNTS_IMPORT_POINTS, GLB_NLS_GUI_PLAYER_TRACKING.Id(1233), ENUM_FEATURES.NONE)

        ' RCI 11-JUN-2013
        Call MultiSiteForms.Add(ENUM_FORM.FORM_ACCOUNT_BLOCK, GLB_NLS_GUI_PLAYER_TRACKING.Id(1950), ENUM_FEATURES.NONE)
        Call MultiSiteForms.SetDefaultPermissions(ENUM_FORM.FORM_ACCOUNT_BLOCK, ENUM_FORM.FORM_ACCOUNT_SUMMARY)

        ' JML 26-JUL-2013
        Call MultiSiteForms.Add(ENUM_FORM.FORM_VIEWER_DIGITALIZED_DOCUMENTS, GLB_NLS_GUI_PLAYER_TRACKING.Id(2426), ENUM_FEATURES.NONE)
        Call MultiSiteForms.SetDefaultPermissions(ENUM_FORM.FORM_VIEWER_DIGITALIZED_DOCUMENTS, ENUM_FORM.FORM_PLAYER_EDIT)

        ' RRR 06-AUG-2013
        Call MultiSiteForms.Add(ENUM_FORM.FORM_MONEY_LAUNDERING_CONFIG, GLB_NLS_GUI_PLAYER_TRACKING.Id(2381), ENUM_FEATURES.NONE)

        ' AMF 13-AUG-2013
        Call MultiSiteForms.Add(ENUM_FORM.FORM_PLAYER_EDIT_CONFIG, GLB_NLS_GUI_PLAYER_TRACKING.Id(2477), ENUM_FEATURES.NONE)

        ' JBC 20-SEP-2013
        Call MultiSiteForms.Add(ENUM_FORM.FORM_POINTS_IMPORT, GLB_NLS_GUI_PLAYER_TRACKING.Id(205), ENUM_FEATURES.NONE)

        ' DHA 24-MAR-2014
        Call MultiSiteForms.Add(ENUM_FORM.FORM_CASH_SUMMARY, GLB_NLS_GUI_PLAYER_TRACKING.Id(1632), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_CASHIER_SESSION_DETAIL, GLB_NLS_GUI_INVOICING.Id(296), ENUM_FEATURES.NONE)

        ' AMF 25-AUG-2014
        Call MultiSiteForms.Add(ENUM_FORM.FORM_PATTERNS_SEL, GLB_NLS_GUI_PLAYER_TRACKING.Id(4924), ENUM_FEATURES.PATTERNS)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_PATTERNS_EDIT, GLB_NLS_GUI_PLAYER_TRACKING.Id(4926), ENUM_FEATURES.PATTERNS)

        ' JAB 29-MAY-2014
        Call MultiSiteForms.Add(ENUM_FORM.FORM_EXCEL_ENABLED, GLB_NLS_GUI_PLAYER_TRACKING.Id(4615), ENUM_FEATURES.NONE)
        Call MultiSiteForms.SetDefaultPermissions(ENUM_FORM.FORM_EXCEL_ENABLED, ENUM_FORM.FORM_EXCEL_ENABLED, 1, 1)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_PRINT_ENABLED, GLB_NLS_GUI_PLAYER_TRACKING.Id(4616), ENUM_FEATURES.NONE)
        Call MultiSiteForms.SetDefaultPermissions(ENUM_FORM.FORM_PRINT_ENABLED, ENUM_FORM.FORM_PRINT_ENABLED, 1, 1)

        ' DHA 01-DEC-2014
        Call MultiSiteForms.Add(ENUM_FORM.FORM_LCD_MESSAGE, GLB_NLS_GUI_PLAYER_TRACKING.Id(5770), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_LCD_MESSAGE_EDIT, GLB_NLS_GUI_PLAYER_TRACKING.Id(5771), ENUM_FEATURES.NONE)

        ' DCS 08-JAN-2015
        Call MultiSiteForms.Add(ENUM_FORM.FORM_SITE_JACKPOT_HISTORY, GLB_NLS_GUI_PLAYER_TRACKING.Id(5431), ENUM_FEATURES.NONE)

        ' ANM 23-APR-2015
        Call MultiSiteForms.Add(ENUM_FORM.FORM_MULTI_CURRENCY, GLB_NLS_GUI_PLAYER_TRACKING.Id(6239), ENUM_FEATURES.MULTISITE_MULTICURRENCY)

        ' FJC 21-AGO-2015
        Call MultiSiteForms.Add(ENUM_FORM.FORM_SHOW_HOLDER_DATA, GLB_NLS_GUI_PLAYER_TRACKING.Id(6665), ENUM_FEATURES.NONE)

        ' JML 29-SEP-2015
        Call MultiSiteForms.Add(ENUM_FORM.FORM_SALES_AND_RETIREMENTS_REPORT, GLB_NLS_GUI_PLAYER_TRACKING.Id(6736), ENUM_FEATURES.ELP_MODE_1)

        ' JRC 11-MAR-2016
        Call MultiSiteForms.Add(ENUM_FORM.FORM_BUCKET_SEL, GLB_NLS_GUI_PLAYER_TRACKING.Id(6967), ENUM_FEATURES.NONE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_BUCKET_EDIT, GLB_NLS_GUI_PLAYER_TRACKING.Id(6997), ENUM_FEATURES.NONE)

        'JRC 11-AGO-2016
        Call MultiSiteForms.Add(ENUM_FORM.FORM_TICKET_AUDIT_SEL_SEVERAL, GLB_NLS_GUI_PLAYER_TRACKING.Id(7674), ENUM_FEATURES.NONE)

        'ETP  31-MAR-2017
        Call MultiSiteForms.Add(ENUM_FORM.FORM_CREDITLINE_SEL, GLB_NLS_GUI_PLAYER_TRACKING.Id(7945), ENUM_FEATURES.CREDIT_LINE)
        Call MultiSiteForms.Add(ENUM_FORM.FORM_CREDITLINE_MOVEMENTS, GLB_NLS_GUI_PLAYER_TRACKING.Id(8003), ENUM_FEATURES.CREDIT_LINE)

        Call MultiSiteForms.Add(ENUM_FORM.FORM_TERMINAL_SAS_METERS_SEL, GLB_NLS_GUI_PLAYER_TRACKING.Id(2973), ENUM_FEATURES.AGG)

        'LTC  29-OCT-2017
        If (WSI.Common.Misc.IsAGGEnabled()) Then
            Call MultiSiteForms.Add(ENUM_FORM.FORM_TERMINALS_SELECTION, GLB_NLS_GUI_AUDITOR.Id(328), ENUM_FEATURES.NONE)
            Call MultiSiteForms.Add(ENUM_FORM.FORM_TERMINAL_SAS_METERS_EDIT, GLB_NLS_GUI_PLAYER_TRACKING.Id(6144), ENUM_FEATURES.NONE)
            Call MultiSiteForms.Add(ENUM_FORM.FORM_TERMINALS_STATUS, GLB_NLS_GUI_SW_DOWNLOAD.Id(453), ENUM_FEATURES.NONE)

            'JBM 18-DEC-2017
            Call MultiSiteForms.Add(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_WORKING_DAY, GLB_NLS_GUI_PLAYER_TRACKING.Id(8813), ENUM_FEATURES.NONE)
            Call MultiSiteForms.Add(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_TERMINALS, GLB_NLS_GUI_PLAYER_TRACKING.Id(8814), ENUM_FEATURES.NONE)
            Call MultiSiteForms.Add(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_METERS, GLB_NLS_GUI_PLAYER_TRACKING.Id(8815), ENUM_FEATURES.NONE)
            Call MultiSiteForms.Add(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_REOPEN_WORKING_DAY, GLB_NLS_GUI_PLAYER_TRACKING.Id(8817), ENUM_FEATURES.NONE)

            Call MultiSiteForms.Add(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_ADJUST_AND_CLOSE_WORKING_DAY, GLB_NLS_GUI_PLAYER_TRACKING.Id(8816), ENUM_FEATURES.NONE)
            Call MultiSiteForms.Add(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_MANAGE_CUT_POINTS, GLB_NLS_GUI_PLAYER_TRACKING.Id(8818), ENUM_FEATURES.NONE)
            Call MultiSiteForms.Add(ENUM_FORM.FORM_LOTTERY_METERS_ADJUSTMENT_TOTALS_VISIBLE, GLB_NLS_GUI_PLAYER_TRACKING.Id(8819), ENUM_FEATURES.NONE)

            Call MultiSiteForms.Add(ENUM_FORM.FORM_LOTTERY_REPORT_PROFIT_DAY, GLB_NLS_GUI_PLAYER_TRACKING.Id(8955), ENUM_FEATURES.NONE)
            Call MultiSiteForms.Add(ENUM_FORM.FORM_LOTTERY_REPORT_PROFIT_MONTH, GLB_NLS_GUI_PLAYER_TRACKING.Id(8956), ENUM_FEATURES.NONE)
            Call MultiSiteForms.Add(ENUM_FORM.FORM_LOTTERY_REPORT_PROFIT_MACHINE_DAY, GLB_NLS_GUI_PLAYER_TRACKING.Id(8976), ENUM_FEATURES.NONE)

            ' FGB 07-DEC-2017: Handpays
            Call MultiSiteForms.Add(ENUM_FORM.FORM_HANDPAYS, GLB_NLS_GUI_AUDITOR.Id(462), ENUM_FEATURES.NONE)

            'DPC  13-DEC-2017
            Call MultiSiteForms.Add(ENUM_FORM.FORM_TERMINALS_TIME_DISCONNECTED, GLB_NLS_GUI_PLAYER_TRACKING.Id(8812), ENUM_FEATURES.NONE)
        End If

        Call InitProfilesForGenericReportsMultiSite(MultiSiteForms)

    End Sub

    Public Sub InitProfilesForGenericReportsMultiSite(ByRef WigosForms As CLASS_GUI_FORMS)
        Dim _features As ENUM_FEATURES
        Dim _cons_request As GenericReport.ConstructorRequest
        Dim _obj_report_tool As GenericReport.IReportToolConfigService
        Dim _add_permission(9) As Boolean
        Dim _aux_title As String

        _cons_request = New GenericReport.ConstructorRequest()
        _cons_request.ProfileID = CurrentUser.ProfileId
        _cons_request.ShowAll = True
        _cons_request.OnlyReportsMailing = False
        _cons_request.ModeType = GetVisibleProfileForms(CurrentUser.GuiId)

        _obj_report_tool = New GenericReport.ReportToolConfigService(_cons_request)

        For Each _report As WSI.Common.GenericReport.ReportToolConfigDTO In _obj_report_tool.List_reports
            _features = _report.ModeType

            Select Case _report.LocationMenuMultiSite
                Case GenericReport.LocationMenuMultiSite.SystemMenu
                    _aux_title = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7333))
                    'Add the permission just one time
                    If Not _add_permission(1) Then
                        Call WigosForms.Add(ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_SYSTEM_MENU, GLB_NLS_GUI_PLAYER_TRACKING.Id(7333), _features)
                        _add_permission(1) = True
                    End If

                Case GenericReport.LocationMenuMultiSite.TerminalsMenu
                    _aux_title = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7338))
                    'Add the permission just one time
                    If Not _add_permission(2) Then
                        Call WigosForms.Add(ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_TERMINALS_MENU, GLB_NLS_GUI_PLAYER_TRACKING.Id(7338), _features)
                        _add_permission(2) = True
                    End If

                Case GenericReport.LocationMenuMultiSite.AuditMenu
                    _aux_title = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(8775))
                    'Add the permission just one time
                    If Not _add_permission(3) Then
                        Call WigosForms.Add(ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_AUDIT_MENU, GLB_NLS_GUI_PLAYER_TRACKING.Id(8775), _features)
                        _add_permission(3) = True
                    End If

                Case GenericReport.LocationMenuMultiSite.StatisticsMenu
                    _aux_title = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7341))
                    'Add the permission just one time
                    If Not _add_permission(4) Then
                        Call WigosForms.Add(ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_STATISTICS_MENU, GLB_NLS_GUI_PLAYER_TRACKING.Id(7341), _features)
                        _add_permission(4) = True
                    End If

                Case GenericReport.LocationMenuMultiSite.AccountingMenu
                    _aux_title = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7344))
                    'Add the permission just one time
                    If Not _add_permission(5) Then
                        Call WigosForms.Add(ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_ACCOUNTING_MENU, GLB_NLS_GUI_PLAYER_TRACKING.Id(7344), _features)
                        _add_permission(5) = True
                    End If

                Case GenericReport.LocationMenuMultiSite.CustomerMenu
                    _aux_title = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7345))
                    'Add the permission just one time
                    If Not _add_permission(6) Then
                        Call WigosForms.Add(ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_CUSTOMER_MENU, GLB_NLS_GUI_PLAYER_TRACKING.Id(7345), _features)
                        _add_permission(6) = True
                    End If

                Case GenericReport.LocationMenuMultiSite.MonitorMenu
                    _aux_title = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7339))
                    'Add the permission just one time
                    If Not _add_permission(7) Then
                        Call WigosForms.Add(ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_MONITOR_MENU, GLB_NLS_GUI_PLAYER_TRACKING.Id(7339), _features)
                        _add_permission(7) = True
                    End If

                Case GenericReport.LocationMenuMultiSite.JackpotsMenu
                    _aux_title = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(7347))
                    'Add the permission just one time
                    If Not _add_permission(8) Then
                        Call WigosForms.Add(ENUM_FORM.FORM_REPORT_TOOL_MULTISITE_JACKPOTS_MENU, GLB_NLS_GUI_PLAYER_TRACKING.Id(7347), _features)
                        _add_permission(8) = True
                    End If

                Case Else
                    _aux_title = vbNullString

            End Select

            Call WigosForms.Add(_report.FormID, 0, ENUM_FEATURES.NONE, _aux_title + ": " + _report.ReportName)
        Next

        _obj_report_tool.Dispose()

    End Sub

End Module
