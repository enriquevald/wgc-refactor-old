'-------------------------------------------------------------------
' Copyright � 2002-2008 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME : mdl_Misc.vb
'
'   DESCRIPTION : Functions associated to GUI_Controls
'
'        AUTHOR : Andreu Juli�
'
' CREATION DATE : 16-ABR-2007 AJQ
'
' REVISION HISTORY :
'
' Date        Author Description
' ----------- ------ -----------------------------------------------
' 16-ABR-2007 AJQ    Initial version
' 05-SEP-2002 JSV    Add rutine to check forms into DB after login (in GUI_Init).
' 26-JAN-2012 RCI    Add FORM_VIRTUAL_REPRINT_VOUCHERS functionality and 
'                    set Default permissions inherited from FORM_MAIN.
' 03-APR-2012 MPO & RCI    Init WSI.Common.Resources without initializazing language, only neutral resources.   
' 06-SEP-2012 RCI    Init HtmlPrinter. Needed for voucher browser form.
' 05-MAR-2013 MPO    GUI_Init:
'                        * Remove "first_intance", "mutex"
'                        * Call function to fill forms permissions
'                        * Update Comments
'                        * Remove GuiID conditions (GUID <> GUI_SETUP and GUID <> GUI_ALARMS)
'                        * Add permission after connection
' 26-JUN-2014 DLL    Fixed BUG WIG-1051: Detection of new features inform user an don't close application
' 11-JUL-2014 JMM    Added SortCheckBoxesByFirstDayOfWeek.  A function to sort weeks days depending on system's first day of week
' 12-AUG-2014 RCI    Fixed Bug WIG-1177: Load Kernel resources (language) using WigosGUI - Language general parameter (instead of Cashier - Language).
' 28-AUG-2014 AMF    Fixed Bug WIG-1197: Feature of form
' 20-JAN-2015 FJC    Set features form (size and autoscroll panels) when size's form exceeds desktop area
' 12-FEB-2015 FJC    New GUI Menu
' 29-SEP-2015 FAV    Fixed Bug 4542: Modal forms in secondary display. Resolved for DEBUG
' 05-AUG-2016 ETP    Fixed Bug 16468: Forms in secondary display. Resolved for RELEASE
' 14-OCT-2016 RAB    PBI 18098: General params: Automatically add the GP to the system
' 19-MAY-2017 RGR    Bug 27330: Wigos GUI - DatePickers Issues
' 25-JUL-2017 RGR    Bug 28949:WIGOS-3834 Cage - The general report of cage is taking too long to open the report
'-------------------------------------------------------------------
Option Explicit On

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_CommonOperations.CLASS_REGISTRY_DATA
Imports GUI_CommonOperations.CLASS_REGISTRY_DEFINITION
Imports GUI_Controls
Imports WSI.Common
Imports System.Text
Imports System.Data.SqlClient

Imports System.Runtime.InteropServices
Imports System.Threading.Mutex
Imports System.Globalization
Imports System.Windows.Forms

Public Module Misc

  Public Class CLASS_MODULE

#Region " Constants "

#End Region

#Region " Structures "

    <StructLayout(LayoutKind.Sequential)> _
    Private Structure TYPE_MODULE_ABOUT
      Private Const VERSION_LENGTH_VERSION As Integer = 16
      Private Const VERSION_LENGTH_COMPANY As Integer = 128
      Private Const VERSION_LENGTH_COPYRIGHT As Integer = 128

      Dim m_control_block As Integer
      Dim m_client_id As Integer
      Dim m_build As Integer
      Dim m_database_common_build As Integer
      Dim m_database_client_build As Integer
      <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=VERSION_LENGTH_VERSION + 1 + 3)> _
      Dim m_version As String
      <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=VERSION_LENGTH_COMPANY + 1 + 3)> _
      Dim m_company_name As String
      <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=VERSION_LENGTH_COPYRIGHT + 1 + 3)> _
      Dim m_copyright As String

      Public Sub Init()
        m_control_block = Marshal.SizeOf(Me)
        m_version = "Version (?)"
        m_company_name = "Company (?)"
        m_copyright = "Copyright (?)"
        m_client_id = 0
        m_build = 0
        m_database_common_build = 0
        m_database_client_build = 0
      End Sub

    End Structure

#End Region

#Region " External Prototypes "

    Private Declare Function Common_VersionGetModuleVersion Lib "CommonMisc" _
                               (ByVal ModuleName As String, ByRef ModuleAbout As TYPE_MODULE_ABOUT) As Integer

    Private Declare Function Common_VersionCheckModuleVersion Lib "CommonMisc" _
                               (ByVal ModuleName As String, ByRef ModuleAbout As TYPE_MODULE_ABOUT) As Integer

#End Region

#Region " Members "

    Private m_module_name As String
    Private m_version_status As Integer
    Private m_module_about As TYPE_MODULE_ABOUT
    Private m_client_id As Integer
    Private m_build_id As Integer
    Private m_last_user As String
    Private m_version_checked As Boolean

    'Menu Strip
    Private m_menustrip_show_icons As String
    Private m_menustrip_position As String
#End Region

#Region " Properties "

    Public ReadOnly Property Name() As String
      Get
        Return m_module_name
      End Get
    End Property

    Public ReadOnly Property Version() As String
      Get
        Return m_module_about.m_version
      End Get
    End Property

    Public ReadOnly Property Company() As String
      Get
        Return m_module_about.m_company_name
      End Get
    End Property

    Public ReadOnly Property Copyright() As String
      Get
        Return m_module_about.m_copyright
      End Get
    End Property

    Public ReadOnly Property IsVersionOk() As Integer
      Get
        If Not m_version_checked Then
          m_version_status = Common_VersionCheckModuleVersion(Me.m_module_name, Me.m_module_about)
          m_version_checked = True
        End If

        Return m_version_status
      End Get
    End Property

    Public ReadOnly Property ClientId() As Integer
      Get
        Return m_client_id
      End Get
    End Property

    Public ReadOnly Property BuildId() As Integer
      Get
        Return m_build_id
      End Get
    End Property

    Public ReadOnly Property DatabaseCommonBuild() As Integer
      Get
        Return m_module_about.m_database_common_build
      End Get
    End Property

    Public ReadOnly Property DatabaseClientBuild() As Integer
      Get
        Return m_module_about.m_database_client_build
      End Get
    End Property

    Public Property LastUser() As String
      Get
        Return m_last_user
      End Get
      Set(ByVal Value As String)
        m_last_user = Value
      End Set
    End Property

    Public Property MenuStripShowIcons() As String
      Get
        Return m_menustrip_show_icons
      End Get
      Set(ByVal Value As String)
        m_menustrip_show_icons = Value
      End Set
    End Property

    Public Property MenuStripPosition() As String
      Get
        Return m_menustrip_position
      End Get
      Set(ByVal Value As String)
        m_menustrip_position = Value
      End Set
    End Property

#End Region

#Region " Private Functions "

#End Region

#Region " Public Functions "
    Public Sub New()

    End Sub

    Public Sub New(ByVal ModuleName As String)
      m_module_name = ModuleName
      Call m_module_about.Init()
      Common_VersionGetModuleVersion(Me.m_module_name, Me.m_module_about)
      m_version_checked = False
      m_client_id = Me.m_module_about.m_client_id
      m_build_id = Me.m_module_about.m_build
    End Sub

#End Region

  End Class

#Region " Constants "

  Public Const AUDIT_NONE_STRING As String = "---"


  ' 02-JUL-2002, AJQ
  Private Const MODULE_NAME As String = "GUI_CONTROLS"

  ' TODO: Pass the following code to the GUI_CommonMisc
  Private Const IPC_CODE_WAIT_SEND_QUEUE_EMPTY As Short = 7
  Private Const IPC_CODE_MODULE_STOP As Short = 8

  Private Const REG_KEY_ROOT As String = "SOFTWARE\LK_SYSTEM"

#End Region

#Region " Structures "

  Private Structure TYPE_DATABASE_VERSION
    Dim m_control_block As Integer
    Dim m_client_id As Integer
    Dim m_common_build As Integer
    Dim m_client_build As Integer
    Public Sub Init()
      m_control_block = Marshal.SizeOf(Me)
      m_client_id = 0
      m_common_build = 0
      m_client_build = 0
    End Sub
  End Structure

  Private Structure TYPE_WEEK_DAY_CHECK_BOX_DATA
    Dim m_location As Drawing.Point
    Dim m_tab_index As Integer
  End Structure


#End Region

#Region " External Prototypes "

  Private Declare Function Common_Ipc Lib "CommonMisc" (ByVal Code As Short) As Short

  Private Declare Sub Common_RestartApplication Lib "CommonBase" (ByVal AppName As String)

#End Region

#Region " Members "

  Public GLB_NLS_GUI_SW_DOWNLOAD As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_SW_DOWNLOAD)
  Public GLB_frm_db_config As frm_database_config
  Public default_xml As String

  Private WithEvents GLB_QueryThread As New CLASS_WORKER_THREAD
  Private GLB_Config As CLASS_CENTER_CONFIGURATION
  Private db_conn_dialog_thread As System.Threading.Thread
  Private GLB_GUI_Forms As Dictionary(Of public_globals.ENUM_GUI, CLASS_GUI_FORMS)


#End Region

#Region " Delegates "

  Private Delegate Sub DelegatesNoParams()

#End Region

#Region " Properties "

#End Region

#Region " Private Functions "

  ' PURPOSE: GUI exiting actions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GUI_Finalize()

    ' AJQ 15-MAR-2006, Log removed
    '   - Log has no sense here, we are stopping the IPC subsystem ...
    If False Then
      ' AJQ 15-MAR-2006, TODO: avoid the IPC connect to DB on 'remote' GUI's
      Common_Ipc(IPC_CODE_WAIT_SEND_QUEUE_EMPTY)
      Common_Ipc(IPC_CODE_MODULE_STOP)
    Else
      System.Threading.Thread.Sleep(1000)
    End If

  End Sub ' GUI_Finalize

#End Region

#Region " Public Functions "

  ' PURPOSE:  Adjusts the size of the specified column to keep size to fit vertical scrollbar
  '  PARAMS:
  '     - INPUT:
  '           - Grid instance
  '           - Column index to resize
  '           - Number of grid header rows.
  '           - (Optional) OriginalSizeInTwips  
  '               If this param is set, grid will be able to restart to original size if scroll disappears
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub GUI_AdjustGridScrollbars(ByRef Grid As uc_grid, ByVal ColumnIndex As Byte, _
                                      ByVal NumHeaderRows As UInt16, Optional ByVal OriginalSizeInTwips As UInt16 = Nothing)

    Const _TWIPS_PER_PIXEL As Byte = 16 '' TODO http://goo.gl/eEWZjb
    Const _GRID_ROW_HEIGHT As Byte = 16
    Dim _max_visible_rows As Byte

    _max_visible_rows = Math.Truncate(Grid.Height / _GRID_ROW_HEIGHT - NumHeaderRows)

    If OriginalSizeInTwips <> Nothing Then
      Grid.Column(ColumnIndex).Width = OriginalSizeInTwips
    End If

    If Grid.NumRows > _max_visible_rows Then

      Grid.Column(ColumnIndex).Width = Grid.Column(ColumnIndex).Width _
                                        - System.Windows.Forms.SystemInformation.HorizontalScrollBarArrowWidth * _TWIPS_PER_PIXEL
    End If

  End Sub

  ' PURPOSE:  Sorts the week days check boxes depending on the system's first day of week
  '  PARAMS:
  '     - INPUT:
  '           - ChkWeekDaysArray:  Array of week days checkboxes.  It suposes to be sorted as Mon, Tue, Wed, Thu, Fri, Sat & Sun
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub SortCheckBoxesByFirstDayOfWeek(ByVal ChkWeekDaysArray() As Windows.Forms.CheckBox)
    Dim _aux_array(6) As TYPE_WEEK_DAY_CHECK_BOX_DATA
    Dim _first_day_of_week_check_box As Int32
    Dim _idx As Int32
    Dim _idx_chek_box As Int32

    ' Check the input array length
    If ChkWeekDaysArray.Length <> _aux_array.Length Then
      Return
    End If

    ' Check system's first day of week
    Select Case CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek()
      Case DayOfWeek.Monday ' Monday first day of week.  As we supose the input week day array start on monday, there is nothing to do
        Return
      Case DayOfWeek.Tuesday    ' Tuesday: first day of week will be second input array's day
        _first_day_of_week_check_box = 1
      Case DayOfWeek.Wednesday  ' Wednesday: first day of week will be third input array's day
        _first_day_of_week_check_box = 2
      Case DayOfWeek.Thursday   ' Thursday: first day of week will be fourth input array's day
        _first_day_of_week_check_box = 3
      Case DayOfWeek.Friday     ' Friday: first day of week will be fifth input array's day
        _first_day_of_week_check_box = 4
      Case DayOfWeek.Saturday   ' Saturday: first day of week will be sixth input array's day
        _first_day_of_week_check_box = 5
      Case DayOfWeek.Sunday     ' Sunday: first day of week will be seventh input array's day
        _first_day_of_week_check_box = 6
      Case Else
        Return
    End Select

    ' Store week days array into an temporary array
    For _idx = 0 To ChkWeekDaysArray.Length - 1
      _aux_array(_idx).m_location = ChkWeekDaysArray(_idx).Location
      _aux_array(_idx).m_tab_index = ChkWeekDaysArray(_idx).TabIndex
    Next

    ' Store the output week days array starting on the proper day
    For _idx = 0 To ChkWeekDaysArray.Length - 1
      _idx_chek_box = (_first_day_of_week_check_box + _idx) Mod 7

      ChkWeekDaysArray(_idx_chek_box).Location = _aux_array(_idx).m_location
      ChkWeekDaysArray(_idx_chek_box).TabIndex = _aux_array(_idx).m_tab_index
    Next

  End Sub

#End Region

  Public Delegate Sub GUI_MainRoutine()

  ' 11-12-2003, LRS: Overloaded 'GUI_Init' to provide at least NLS Support for "standalone" GUI�s
  Public Sub GUI_Init(ByVal GuiNlsId As Integer)
    Dim config As New CLASS_CENTER_CONFIGURATION
    Const function_name As String = "GUI_Init"
    Dim rc As CLASS_BASE.ENUM_STATUS

    Try
      rc = config.DB_BeforeRead(Nothing)

      If Not NLS_SetLanguage(config.Language) Then
        Call NLS_SetLanguage(ENUM_NLS_LANGUAGE.NLS_LANGUAGE_SPANISH)
      End If

      ' Application Name
      GLB_ApplicationName = NLS_GetString(GuiNlsId)



    Catch exception As Exception
      ' Unhandled exception ...
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                      GLB_ApplicationName, _
                      function_name, _
                      "", _
                      "", _
                      "", _
                      exception.Message)
    End Try
  End Sub

  ' PURPOSE: Get GuiFomrs by Gui Id
  '
  '  PARAMS:
  '     - INPUT:
  '       - GuiId
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - CLASS_GUI_FORMS
  '
  Public Function GetGUIForms(ByVal GuiId As ENUM_GUI) As CLASS_GUI_FORMS
    If GLB_GUI_Forms.ContainsKey(GuiId) Then
      Return GLB_GUI_Forms.Item(GuiId)
    End If

    Return Nothing
  End Function ' GetGUIForms

  ' PURPOSE: GUI Initialization actions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub GUI_Init(ByVal GuiNlsId As Integer, _
                      ByVal MainRoutine As GUI_MainRoutine, _
                      ByVal FtpConfig As DbConfig, _
                      ByVal SiteConfig As DbConfig)

    Const FUNCTION_NAME As String = "GUI_Init"

    Dim _site_forms As CLASS_GUI_FORMS
    Dim _cashier_forms As CLASS_GUI_FORMS
    Dim _istats_forms As CLASS_GUI_FORMS
    Dim _multisite_forms As CLASS_GUI_FORMS
    'JCA CashDeskDraws
    Dim _cashdesk_draws_forms As CLASS_GUI_FORMS

    'FJC 17-02-2015
    Dim _layout_forms As CLASS_GUI_FORMS

    Dim _database_expected_version As New TYPE_DATABASE_VERSION
    Dim _database_found_version As New TYPE_DATABASE_VERSION
    Dim _version_module As TYPE_VERSION_MODULE = Nothing
    Dim _gui_module As CLASS_MODULE
    Dim _version_string As String
    Dim _copyright As String = ""
    Dim _module_name As String = ""
    Dim _started As Boolean
    Dim _error_message_id As Integer = 117 ' Generic Error NLS Id Message
    Dim _error_param_1 As String = ""
    Dim _error_param_2 As String = ""
    Dim _aux_string As String = ""
    Dim _ctx As Integer
    Dim _rc As Short
    Dim _module_used As public_globals.ENUM_GUI
    Dim _notify As Boolean = False
    Dim _lang_name As String

    ' Initialization actions:
    '   - Check MultiSite
    '   - Initialize default language.
    '   - Get application name.
    '   - Initialize versions structure.
    '   - Display banner.
    '   - Load configuration values: Default values are used if no configuration file can be loaded.
    '   - Returning false means that even default values could not be taken.
    '   - While true
    '       - Start dialog for connection to database
    '       - Connect to database to obtain users.
    '       - Show database connection dialog and retry.
    '   - Init Connection
    '   - Initialize resources 
    '   - Check mode: MultiSite and Site
    '   - Wait for login credentials.
    '   - Add permissions for forms after connect
    '       - Check if is multisite
    '       - Check permissions
    '   - Hide banner.
    '   - Call GUI main routine.

    _version_string = FixedString(256)
    _copyright = FixedString(256)

    ' Initialization
    _started = False

    Try

      '   - Initialize default language.
      If NLS_SetLanguage(ENUM_NLS_LANGUAGE.NLS_LANGUAGE_ENGLISH) = False Then
        Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_GENERIC_ERROR, _
                              public_globals.GUI_ModuleName(ENUM_GUI.WIGOS_GUI), _
                              FUNCTION_NAME, _
                              _rc, _
                              "NLS_SetLanguage")
        Exit Try
      End If

      '   - Get application name.
      GLB_ApplicationName = NLS_GetString(GuiNlsId)

      '   - Initialize versions structure.
      _module_used = public_globals.ENUM_GUI.WIGOS_GUI
      Call GUI_Versions(_version_module)
      _module_name = public_globals.GUI_ModuleName(_module_used)
      _gui_module = New CLASS_MODULE(_module_name)
      Call GUI_SetClientId(_gui_module.ClientId)
      _version_module.component(0).version_found = NullTrim(_gui_module.Version)
      _version_module.component(0).copyright = NullTrim(_gui_module.Copyright)

      '   - Display banner
      Call ShowBanner(_version_module.component(0))

      Try
        ' - Start dialog for connection to database
        ' - Connect to database to obtain users.
        ' - Show database connection dialog and retry.
        While (True)

          ' - Start dialog for connection to database
          StartDBConnectionDialog()

          ' - Connect to database to obtain users.
          ' APB 12/11/2008: 'Client Identifier' is now taken from configuration file instead of from a hard-coded constant
          WSI.Common.WGDB.Init(SiteConfig.DbId, SiteConfig.Server1, SiteConfig.Server2)

          ' Wait to display screen
          System.Threading.Thread.Sleep(frm_database_config.MIN_TIME_VISIBLE)

          If (WGDB.Initialized) Then
            Exit While
          End If

          CloseDBConnectionDialog()

          ' - Show database connection dialog and retry.
          GLB_frm_db_config.LoadDialog(False)

        End While

        ' - Make a database connection as WGGUI user.
        Try
          ' Init Connection
          If GLB_GuiMode = public_globals.ENUM_GUI.MULTISITE_GUI Then
            WGDB.SetApplication("WigosMultiSiteGUI", _gui_module.Version)
          ElseIf GLB_GuiMode = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI Then
            WGDB.SetApplication("WigosDrawGUI", _gui_module.Version)
          Else
            WGDB.SetApplication("WigosGUI", _gui_module.Version)
          End If

          WSI.Common.WGDB.ConnectAs("WGGUI")


          CloseDBConnectionDialog()

        Catch ex As Exception
          MsgBox(ex.Message)
        End Try
        'Initialize resources 
        _lang_name = GetLanguageName()
        WSI.Common.Resource.Init(_lang_name)

        ' - Initialize language settings
        If (Language_Init() = False) Then
          MsgBox("Error setting application language. Application will stop")

          Environment.Exit(0)
        End If

        Call HtmlPrinter.Init()

        ' ICS 31-MAY-2013: Must be used only for Cashier and GUI processes
        Call WSI.Common.CommonCashierInformation.SetUnique()

        Call InitUserNLS()

      Catch ex As Exception
        MsgBox(ex.Message)
      End Try

      '   - Check versions
      Select Case _gui_module.IsVersionOk
        Case ENUM_COMMON_RC.COMMON_OK

        Case ENUM_COMMON_RC.COMMON_ERROR_DB
          _error_message_id = 130

          Exit Try

        Case Else
          _error_message_id = 127
          _error_param_1 = GLB_ApplicationName
          _error_param_2 = NullTrim(_gui_module.Version)

          Exit Try
      End Select

      '   - Keep database connection string to be displayed on forms.
      ' Set the ConnectString
      GLB_DbServerName = WGDB.DataSource

      '   - Get last user logged from registry.
      Call GetDataFromRegistry(_gui_module)

      ' DCS Enable Configuration Voucher Params
      WSI.Common.OperationVoucherParams.LoadData = True

      '   - Check mode: MultiSite and Site
      If Not CheckMode(GLB_GuiMode) Then

        Return
      End If

      If GLB_GuiMode = public_globals.ENUM_GUI.WIGOS_GUI Then
        Dim _current_version As String
        Dim _expected_version As String

        _current_version = WGDB.GetDatabaseVersion()
        _expected_version = "18.134.027"

        If _current_version < _expected_version Then
          NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(563), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, , _current_version, _expected_version)

          _started = True
          Exit Try
        End If
      End If

      '   - Wait for login credentials.
      If Not UserLogin(GLB_GuiMode, GLB_ApplicationName, _gui_module.LastUser) Then
        ' Set the started variable ... avoiding the error msg at the end.
        _started = True
        _error_message_id = 131

        Exit Try
      Else
        _gui_module.LastUser = GLB_CurrentUser.Name
        Call SetDataToRegistry(_gui_module)
      End If

      ' - Add permissions for forms after connect
      GLB_GUI_Forms = New Dictionary(Of public_globals.ENUM_GUI, CLASS_GUI_FORMS)

      ' - * Insert Site forms and cashier forms for SITE and MULTISITE mode
      _site_forms = New CLASS_GUI_FORMS
      _cashier_forms = New CLASS_GUI_FORMS


      Call InitProfilesWigosGUI(_site_forms)
      Call InitProfilesCashier(_cashier_forms)


      GLB_GUI_Forms.Add(public_globals.ENUM_GUI.WIGOS_GUI, _site_forms)
      GLB_GUI_Forms.Add(public_globals.ENUM_GUI.CASHIER, _cashier_forms)


      'FJC 17-02-2015
      If WSI.Common.Misc.IsIntelliaEnabled() Then
        _layout_forms = New CLASS_GUI_FORMS
        Call InitProfilesLayout(_layout_forms)

        GLB_GUI_Forms.Add(public_globals.ENUM_GUI.SMARTFLOOR, _layout_forms)
      End If

      ' DLL 11-FEB-2014 Istats permission allowed in multisite
      _istats_forms = New CLASS_GUI_FORMS
      Call InitProfilesIstats(_istats_forms)

      Select Case GLB_GuiMode

        Case public_globals.ENUM_GUI.WIGOS_GUI

          GLB_GUI_Forms.Add(public_globals.ENUM_GUI.ISTATS, _istats_forms)

        Case public_globals.ENUM_GUI.MULTISITE_GUI

          GLB_GUI_Forms.Add(public_globals.ENUM_GUI.ISTATS, _istats_forms)

          _multisite_forms = New CLASS_GUI_FORMS
          Call InitProfilesMultiSite(_multisite_forms)
          GLB_GUI_Forms.Add(public_globals.ENUM_GUI.MULTISITE_GUI, _multisite_forms)

          'JCA CashDeskDraws
        Case public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI
          GLB_GUI_Forms = New Dictionary(Of public_globals.ENUM_GUI, CLASS_GUI_FORMS)
          _cashdesk_draws_forms = New CLASS_GUI_FORMS
          Call InitProfilesCashDeskDraws(_cashdesk_draws_forms)
          GLB_GUI_Forms.Add(public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI, _cashdesk_draws_forms)

      End Select


      '
      ' - Check permissions
      '
      _rc = 0
      For Each _pair As KeyValuePair(Of public_globals.ENUM_GUI, CLASS_GUI_FORMS) In GLB_GUI_Forms
        _rc = _pair.Value.Check(_pair.Key, GLB_CurrentUser.IsSuperUser, _ctx)
        If _rc = CLASS_GUI_FORMS.ENUM_CHECK.CHECK_ERROR_DB Then
          _error_message_id = 116

          Exit Try
        End If
        If _rc = CLASS_GUI_FORMS.ENUM_CHECK.CHECK_ERROR Then
          _notify = True
        End If
      Next

      If _notify Then
        If Not GLB_StartedInSelectSiteMode OrElse GLB_GuiMode <> ENUM_GUI.WIGOS_GUI Then
          If GLB_CurrentUser.IsSuperUser Then
            ' SuperUser will be notified and allowed to update database.
            If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(114), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_YES Then

              Dim ok As Boolean

              ok = True

              For Each _pair As KeyValuePair(Of public_globals.ENUM_GUI, CLASS_GUI_FORMS) In GLB_GUI_Forms
                ok = ok And _pair.Value.Update(_pair.Key, GLB_CurrentUser.IsSuperUser, _ctx)
              Next

              If Not ok Then
                ' Error updating DB
                _error_message_id = 133
                Exit Try
              End If
            End If
          Else
            ' Other users Only will be notified.
            _error_message_id = 115

            Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(_error_message_id), _
                                    ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                    ENUM_MB_BTN.MB_BTN_OK, , _
                                    _error_param_1, _
                                    _error_param_2)

            _aux_string = GLB_NLS_GUI_CONTROLS.GetString(_error_message_id, _error_param_1, _error_param_2)
            Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_USER_MESSAGE, _
                                  _module_name, _
                                  FUNCTION_NAME, , , , _
                                  _aux_string)
          End If
        End If
      End If


      '
      ' Initialize THREAD NEW SOFTWARE VERSIONS
      '
      AddHandler WSI.Common.FtpVersions.OnNewVersionAvailable, AddressOf FtpVersions_OnNewVersionAvailable

      Dim _ftp_conn_string As String

      _ftp_conn_string = WGDB.InternalConnectionString(FtpConfig.DbId, FtpConfig.Server1, FtpConfig.Server2, "WGGUI")

      If GLB_StartedInSelectSiteMode Then
        WSI.Common.FtpVersions.Init(ENUM_TSV_PACKET.TSV_PACKET_WIN_MULTISITE_GUI, _gui_module.ClientId, _gui_module.BuildId, _ftp_conn_string)
      ElseIf GLB_GuiMode = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI Then
        WSI.Common.FtpVersions.Init(ENUM_TSV_PACKET.TSV_PACKET_WIN_CASHDESK_DRAWS_GUI, _gui_module.ClientId, _gui_module.BuildId, _ftp_conn_string)
      Else
        WSI.Common.FtpVersions.Init(ENUM_TSV_PACKET.TSV_PACKET_WIN_GUI, _gui_module.ClientId, _gui_module.BuildId, _ftp_conn_string)
      End If

      ' 
      _started = True

      '   - Hide banner.
      Call HideBanner()

      'FJC 17-02-2015
      If WSI.Common.Misc.IsIntelliaEnabled() Then
        Call GUI_SQLExecuteNonQuery("EXEC CreateDefaultProfilesLayout")
      End If

      '   - Call GUI main routine.
      Call MainRoutine()

    Catch exception As Exception
      ' Unhandled exception management
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(118), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            FUNCTION_NAME, _
                            "", _
                            "", _
                            "", _
                            exception.Message)

    End Try

    ' Hide Banner
    Call HideBanner()

    If Not _started Then
      ' Something went wrong and the application can not be started
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(_error_message_id), _
                              ENUM_MB_TYPE.MB_TYPE_ERROR, _
                              ENUM_MB_BTN.MB_BTN_OK, , _
                              _error_param_1, _
                              _error_param_2)

      _aux_string = GLB_NLS_GUI_CONTROLS.GetString(_error_message_id, _error_param_1, _error_param_2)
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_USER_MESSAGE, _
                            _module_name, _
                            FUNCTION_NAME, , , , _
                            _aux_string)
    End If

    ' Finalize routine
    Call GUI_Finalize()

    '' When password wrong
    '' When user cancel login form..
    '' Login with user without admin permisions and new database functionalities added.
    '' When user close login form
    If GLB_StartedInSelectSiteMode Then

      Call System.Windows.Forms.Application.Restart()

    End If


  End Sub

  ' PURPOSE: Obtain last logger user from registry.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '       - GuiModule: Used to keep retrieved data. 
  '
  ' RETURNS:
  Public Sub GetDataFromRegistry(ByVal GuiModule As CLASS_MODULE)
    Dim reg_data As CLASS_REGISTRY_DATA
    Dim reg_data_item As New CLASS_REGISTRY_DATA_ITEM
    Dim rc As Integer

    reg_data = New CLASS_REGISTRY_DATA

    rc = Common_GetRegistryData(REG_KEY_ROOT, REG_KEY_NAME_GUI_INIT_TYPE, REG_KEY_NAME_GUI_INIT, reg_data.RegistryDataList)
    If rc <> REG_RETURN_CODE_OK Then
      Exit Sub
    End If

    rc = Common_GetValue(REG_VALUE_NAME_LAST_USER, reg_data.RegistryDataList, reg_data_item)
    If rc = REG_RETURN_CODE_OK Then
      GuiModule.LastUser = reg_data_item.ValueData
    End If

    'MenuStrip features
    rc = Common_GetRegistryData(REG_KEY_ROOT, REG_KEY_NAME_GUI_INIT_TYPE, REG_KEY_NAME_GUI_MENU_STRIP, reg_data.RegistryDataList)
    If rc <> REG_RETURN_CODE_OK Then
      Exit Sub
    End If

    ' Show icons
    rc = Common_GetValue(REG_VALUE_NAME_GUI_MENU_STRIP_SHOW_ICONS, reg_data.RegistryDataList, reg_data_item)
    If rc = REG_RETURN_CODE_OK Then
      GuiModule.MenuStripShowIcons = reg_data_item.ValueData
    End If

    ' Position
    rc = Common_GetValue(REG_VALUE_NAME_GUI_MENU_STRIP_POSITION, reg_data.RegistryDataList, reg_data_item)
    If rc = REG_RETURN_CODE_OK Then
      GuiModule.MenuStripPosition = reg_data_item.ValueData
    End If

  End Sub ' GetDataFromRegistry

  ' PURPOSE: Save last logged user on registry.
  '
  '  PARAMS:
  '     - INPUT:
  '       - GuiModule: Contains last logged user data. 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub SetDataToRegistry(ByVal GuiModule As CLASS_MODULE)
    Dim reg_data As CLASS_REGISTRY_DATA
    Dim reg_data_item As New CLASS_REGISTRY_DATA_ITEM
    Dim rc As Integer

    reg_data = New CLASS_REGISTRY_DATA

    If Not GuiModule.LastUser Is Nothing Then

      reg_data_item.KeyType = REG_KEY_NAME_GUI_INIT_TYPE
      reg_data_item.KeyName = REG_KEY_NAME_GUI_INIT
      reg_data_item.ValueType = REG_TYPE_STRING
      reg_data_item.ValueName = REG_VALUE_NAME_LAST_USER
      reg_data_item.ValueData = GuiModule.LastUser
      reg_data_item.ValueLength = Len(reg_data_item.ValueData)
      reg_data.RegistryDataList.SetItem(reg_data.RegistryDataList.Count, reg_data_item)
      rc = Common_SetRegistryData(REG_KEY_ROOT, reg_data.RegistryDataList)
      If rc <> REG_RETURN_CODE_OK Then

      End If
    End If

    'Menu Strip 
    ' Show icons
    If GuiModule.MenuStripShowIcons = String.Empty Then
      GuiModule.MenuStripShowIcons = "1"
    End If
    reg_data_item.KeyType = REG_KEY_NAME_GUI_INIT_TYPE
    reg_data_item.KeyName = REG_KEY_NAME_GUI_MENU_STRIP
    reg_data_item.ValueType = REG_TYPE_STRING

    reg_data_item.ValueName = REG_VALUE_NAME_GUI_MENU_STRIP_SHOW_ICONS
    reg_data_item.ValueData = GuiModule.MenuStripShowIcons
    reg_data_item.ValueLength = Len(reg_data_item.ValueData)
    reg_data.RegistryDataList.SetItem(reg_data.RegistryDataList.Count, reg_data_item)
    rc = Common_SetRegistryData(REG_KEY_ROOT, reg_data.RegistryDataList)
    If rc <> REG_RETURN_CODE_OK Then

    End If

    ' Position 
    If GuiModule.MenuStripPosition = String.Empty Then
      GuiModule.MenuStripPosition = Windows.Forms.DockStyle.Top.ToString
    End If
    reg_data_item.KeyType = REG_KEY_NAME_GUI_INIT_TYPE
    reg_data_item.KeyName = REG_KEY_NAME_GUI_MENU_STRIP
    reg_data_item.ValueType = REG_TYPE_STRING

    reg_data_item.ValueName = REG_VALUE_NAME_GUI_MENU_STRIP_POSITION
    reg_data_item.ValueData = GuiModule.MenuStripPosition
    reg_data_item.ValueLength = Len(reg_data_item.ValueData)
    reg_data.RegistryDataList.SetItem(reg_data.RegistryDataList.Count, reg_data_item)
    rc = Common_SetRegistryData(REG_KEY_ROOT, reg_data.RegistryDataList)
    If rc <> REG_RETURN_CODE_OK Then

    End If

  End Sub ' SetDataToRegistry

  ' PURPOSE: Obtain versions from database.
  '
  '  PARAMS:
  '     - INPUT:
  '       - context: Database context.
  '
  '     - OUTPUT:
  '       - pVersion: Structure where versions are returned. 
  '
  ' RETURNS:
  Private Function Common_GetDatabaseVersion(ByRef pVersion As TYPE_DATABASE_VERSION, ByVal context As Integer) As Integer
    Dim str_sql As String
    Dim data_table As DataTable

    str_sql = "SELECT  DB_CLIENT_ID " & _
                   " , DB_COMMON_BUILD_ID " & _
                   " , DB_CLIENT_BUILD_ID " & _
               " FROM  DB_VERSION "

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)
    If IsNothing(data_table) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    pVersion.m_client_id = data_table.Rows(0).Item("DB_CLIENT_ID")
    pVersion.m_common_build = data_table.Rows(0).Item("DB_COMMON_BUILD_ID")
    pVersion.m_client_build = data_table.Rows(0).Item("DB_CLIENT_BUILD_ID")

    Return ENUM_COMMON_RC.COMMON_OK

  End Function ' Common_GetDatabaseVersion

  ' PURPOSE: Compare expected (Distribution list) and found (Database) for differences:
  '           - Client Id.
  '           - DB Common Build.
  '           - Client Build.
  '
  '  PARAMS:
  '     - INPUT:
  '       - pExpectedVersion: Expected client and common build.
  '       - pFoundVersion: Found client and common build.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function Common_VersionCheckDatabaseVersion(ByRef pExpectedVersion As TYPE_DATABASE_VERSION, ByRef pFoundVersion As TYPE_DATABASE_VERSION) As Integer

    ' RRT 10-DEC-2008 - Added checking for client id.
    If pExpectedVersion.m_client_id <> pFoundVersion.m_client_id Then

      Common_LoggerMsg(ENUM_LOG_MSG.LOG_UNEXPECTED_VALUE_ERROR, _
                       MODULE_NAME, _
                       "Common_VersionCheckDatabaseVersion", _
                       "Version.m_client_id", _
                       pExpectedVersion.m_client_id)

      Return ENUM_COMMON_RC.COMMON_ERROR
    End If

    If pExpectedVersion.m_client_build <> pFoundVersion.m_client_build Then

      Common_LoggerMsg(ENUM_LOG_MSG.LOG_UNEXPECTED_VALUE_ERROR, _
                       MODULE_NAME, _
                       "Common_VersionCheckDatabaseVersion", _
                       "Version.m_client_build", _
                       pExpectedVersion.m_client_build)

      Return ENUM_COMMON_RC.COMMON_ERROR
    End If

    If pExpectedVersion.m_common_build <> pFoundVersion.m_common_build Then

      Common_LoggerMsg(ENUM_LOG_MSG.LOG_UNEXPECTED_VALUE_ERROR, _
                       MODULE_NAME, _
                       "Common_VersionCheckDatabaseVersion", _
                       "Version.m_common_build", _
                       pExpectedVersion.m_common_build)

      Return ENUM_COMMON_RC.COMMON_ERROR
    End If

    Return ENUM_COMMON_RC.COMMON_OK

  End Function ' Common_VersionCheckDatabaseVersion

  ' PURPOSE : Initialize language:
  '           - Obtain language parameter from table GENERAL_PARAMS (WigosGUI.Language). Values: 
  '             - en-US (LanguageId: 9)
  '             - es (LanguageId: 10)
  '             - fr (LanguageId: 12)
  '           - Initialize NLS.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: Success
  '     - FALSE: Error
  '
  Private Function Language_Init() As Boolean

    Dim language_id As Integer
    Dim rc As Short
    Const function_name As String = "Language_Init"

    language_id = GetLanguageId()

    ' Check if database language is different than default language
    If language_id <> ENUM_NLS_LANGUAGE.NLS_LANGUAGE_ENGLISH Then
      ' Load library and language handle
      If NLS_SetLanguage(language_id) = False Then

        ' Something went wrong and the application can not be started
        Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_GENERIC_ERROR, _
                              public_globals.GUI_ModuleName(ENUM_GUI.WIGOS_GUI), _
                              function_name, _
                              rc, _
                              "NLS_SetLanguage")
        Return False
      End If
    End If

    Return True

  End Function ' Language_Init


  ' PURPOSE : Obtain language parameter from table GENERAL_PARAMS (WigosGUI.Language). Values: 
  '             - en-US (LanguageId: 9)
  '             - es    (LanguageId: 10)
  '             - fr    (LanguageId: 12)
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - LanguageId.
  '       - English: 9
  '       - Spanish: 10

  Private Function GetLanguageId() As Integer

    Dim key_value As String
    Dim language_id As Integer

    ' Initialization 
    ' Default language: English
    language_id = ENUM_NLS_LANGUAGE.NLS_LANGUAGE_ENGLISH

    key_value = GetLanguageName()

    ' Compare returned value with possible values:
    ' - en-US
    ' - es
    ' - fr
    If (String.Compare(key_value, "es", True) = 0) Then
      ' Set to spanish
      language_id = ENUM_NLS_LANGUAGE.NLS_LANGUAGE_SPANISH
    End If
    If (String.Compare(key_value, "fr", True) = 0) Then
      ' Set to spanish
      language_id = ENUM_NLS_LANGUAGE.NLS_LANGUAGE_FRENCH
    End If

    Return language_id

  End Function ' GetLanguageId

  Private Function GetLanguageName() As String

    Dim general_params As DataTable
    Dim sql_text As String
    Dim key_value As String
    Dim _lang_eng As String
    Dim _lang_esp As String
    Dim _lang_fr As String

    _lang_eng = "en-US"
    _lang_esp = "es"
    _lang_fr = "fr"

    ' Build query
    sql_text = "select GP_KEY_VALUE" _
              & " from GENERAL_PARAMS" _
              & " where GP_GROUP_KEY = 'WigosGUI'" _
              & "  AND  GP_SUBJECT_KEY = 'Language'"

    ' Check query results
    general_params = GUI_GetTableUsingSQL(sql_text, 1)
    If IsNothing(general_params) Then
      Return _lang_eng
    End If

    ' Check for rows received.
    If general_params.Rows.Count() < 1 Then
      Return _lang_eng
    End If

    ' Obtain data
    key_value = general_params.Rows.Item(0).Item("GP_KEY_VALUE").ToString.Trim.ToUpper

    ' Check for empty values
    If key_value = "" Then
      Return _lang_eng
    End If

    ' Compare returned value with possible values:
    ' - en-US
    ' - es
    ' - fr
    If (String.Compare(key_value, "es", True) = 0) Then
      ' Set to spanish
      Return _lang_esp
    End If
    If (String.Compare(key_value, "fr", True) = 0) Then
      ' Set to frenc
      Return _lang_fr
    End If

    Return _lang_eng

  End Function ' GetLanguageName


  ' PURPOSE : Start a new thread which load and display the db connection dialog.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub StartDBConnectionDialog()

    ' A new thread is neeed to show dialog in order to allow application dialog continues.
    db_conn_dialog_thread = New System.Threading.Thread(AddressOf LoadDBConnectionDialog)
    db_conn_dialog_thread.Start()

  End Sub ' StartDBConnectionDialog


  ' PURPOSE : Display pacifier related to database connection
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub LoadDBConnectionDialog()

    Try
      GLB_frm_db_config = New frm_database_config
      GLB_frm_db_config.LoadDialog(True)
    Catch
    End Try

  End Sub ' LoadDBConnectionDialog

  ' PURPOSE : Close the database connection form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub CloseDBConnectionDialog()

    If GLB_frm_db_config.InvokeRequired Then
      Call GLB_frm_db_config.Invoke(New DelegatesNoParams(AddressOf CloseDBConfigDialog))
    End If


  End Sub ' CloseDBConnectionDialog

  ' PURPOSE : Close the database connection form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub CloseDBConfigDialog()

    GLB_frm_db_config.Close()

  End Sub ' CloseDBConfigDialog



  ' PURPOSE: Obtain date time from database.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - DB Date time

  Public Function GUI_GetDBDateTime() As Date
    Static first_time As Boolean = True
    Static Dim diff As TimeSpan
    Static Dim read_sysdate As Date
    Static Dim last_query As Date

    Dim read_needed As Boolean
    Dim sysdate As Date

    read_needed = True

    If Not first_time Then
      If Now.Subtract(last_query).TotalMinutes < 15 Then
        read_needed = False
      End If
    End If

    If read_needed Then
      Dim table As DataTable

      table = GUI_GetTableUsingSQL("SELECT GETDATE()", 1)

      If IsNothing(table) Then
        If first_time Then
          Return Now
        End If
      Else
        read_sysdate = table.Rows.Item(0).Item(0)
        diff = Now.Subtract(read_sysdate)
        last_query = Now
        table.Dispose()
        table = Nothing
        first_time = False
      End If
    End If

    sysdate = Now.Subtract(diff)

    Return sysdate

  End Function 'GUI_GetDBDateTime

  ' PURPOSE: Obtain date time from computer.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - DB Date time
  Public Function GUI_GetDBDate() As Date
    Dim db_date_time As Date
    Dim aux_date As Date

    db_date_time = GUI_GetDBDateTime()
    aux_date = New DateTime(db_date_time.Year, db_date_time.Month, db_date_time.Day, 0, 0, 0)

    Return aux_date

  End Function ' GUI_GetDBDate

  ' PURPOSE: This is the sub that is called once a new application version
  '          is ready to be installed
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     
  Public Sub FtpVersions_OnNewVersionAvailable(ByVal VersionEvent As FtpVersions.NewVersionEvent)

    Dim rc As Integer
    Dim app_name As String
    Dim trimmed_app_name As String

    rc = NLS_MsgBox(GLB_NLS_GUI_SW_DOWNLOAD.Id(116), _
                mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, _
                mdl_NLS.ENUM_MB_BTN.MB_BTN_YES_NO, _
                ENUM_MB_DEF_BTN.MB_DEF_BTN_2)

    If rc = ENUM_MB_RESULT.MB_RESULT_YES Then
      app_name = Environment.CommandLine

      ' Replace double quotes by spaces
      trimmed_app_name = app_name.Replace(Chr(34), Chr(32))
      trimmed_app_name = trimmed_app_name.Trim

      WSI.Common.Users.SetUserLoggedOff(Users.EXIT_CODE.LOGGED_OUT)

      ' Actually restart application, not just exit
      Common_RestartApplication(trimmed_app_name)
    End If

  End Sub ' FtpVersions_OnNewVersionAvailable

  ' PURPOSE : Obtain Site Identifier from table GENERAL_PARAMS (Site.Identifier).
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Site Id

  Public Function GetSiteId() As String
    Return WSI.Common.GeneralParam.GetString("Site", "Identifier").Trim().ToUpper()
  End Function ' GetSiteId

  ' PURPOSE : Obtain Cashless server id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Site Id

  Public Function GetCashlessServerId() As Integer

    Dim str_sql As String
    Dim data_table_general_params As DataTable
    Dim CashlessServer As CashlessMode
    Dim temp_cashless_server As String
    ' Initialization 
    CashlessServer = CashlessMode.WIN

    ' Build query
    str_sql = "select GP_KEY_VALUE" _
              & " from GENERAL_PARAMS" _
              & " where GP_GROUP_KEY = 'Cashless'" _
              & "  AND  GP_SUBJECT_KEY = 'Server'"

    ' Check query results
    data_table_general_params = GUI_GetTableUsingSQL(str_sql, 1)

    If IsNothing(data_table_general_params) Then
      Return CashlessServer
    End If

    ' Check for rows received.
    If data_table_general_params.Rows.Count() < 1 Then
      Return CashlessServer
    End If

    temp_cashless_server = data_table_general_params.Rows(0).Item("GP_KEY_VALUE")
    Select Case temp_cashless_server
      Case GLB_NLS_GUI_CONFIGURATION.GetString(220)                     ' "WIN"
        CashlessServer = CashlessMode.WIN
      Case GLB_NLS_GUI_CONFIGURATION.GetString(221), "ALESIS"           ' "3GS", "ALESIS"
        CashlessServer = CashlessMode.ALESIS
    End Select

    Return CashlessServer

  End Function ' GetCashlessServerId

  ' PURPOSE : Obtain Cashier Data
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Site Id

  Public Function GetCashierData(ByVal SubjectKey As String) As String

    Dim str_sql As String
    Dim data_table_general_params As DataTable
    Dim Value As String
    Value = ""

    ' Build query
    str_sql = "select GP_KEY_VALUE" _
              & " from GENERAL_PARAMS" _
              & " where GP_GROUP_KEY = 'Cashier'" _
              & "  and  GP_SUBJECT_KEY = '" & SubjectKey & "'"

    ' Check query results
    data_table_general_params = GUI_GetTableUsingSQL(str_sql, 1)

    If IsNothing(data_table_general_params) Then
      Return Value
    End If

    ' Check for rows received.
    If data_table_general_params.Rows.Count() < 1 Then
      Return Value
    End If

    Value = data_table_general_params.Rows(0).Item("GP_KEY_VALUE")

    Return Value

  End Function ' GetCashierData


  ' PURPOSE : Obtain Witholding Data
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - String

  Public Function GetWitholdingData(ByVal SubjectKey As String) As String

    Dim _str_sql As String
    Dim _dt_general_params As DataTable

    ' Build query
    _str_sql = "SELECT   GP_KEY_VALUE" _
            & "   FROM   GENERAL_PARAMS" _
            & "  WHERE   GP_GROUP_KEY = 'Witholding.Document'" _
            & "    AND   GP_SUBJECT_KEY = '" & SubjectKey & "'"


    _dt_general_params = GUI_GetTableUsingSQL(_str_sql, 1)

    ' Check query results
    If _dt_general_params.Rows.Count() = 1 AndAlso _
      Not IsDBNull(_dt_general_params.Rows(0).Item(0)) AndAlso _
      Not String.IsNullOrEmpty(_dt_general_params.Rows(0).Item(0)) Then

      Return _dt_general_params.Rows(0).Item(0)

    Else

      Return ""

    End If

  End Function ' GetCashierData

  '----------------------------------------------------------------------------
  ' PURPOSE : Select records on general params.
  '
  ' PARAMS :
  '     - INPUT :   GroupKey = Value for Group
  '                 SubjectKey = Value for Subject
  '     - OUTPUT :  None
  '
  ' RETURNS :
  '     - String with value
  '
  ' NOTES :
  Public Function ReadGeneralParam(ByVal GroupKey As String, _
                                     ByVal SubjectKey As String) As String
    Dim _str_sql As String
    Dim _dt_general_params As DataTable

    _str_sql = "SELECT   GP_KEY_VALUE                           " & _
               "  FROM   GENERAL_PARAMS                         " & _
               " WHERE   GP_GROUP_KEY   = '" & GroupKey & "'    " & _
               "   AND   GP_SUBJECT_KEY = '" & SubjectKey & "'  "

    _dt_general_params = GUI_GetTableUsingSQL(_str_sql, 1)

    ' Check query results
    If _dt_general_params.Rows.Count() = 1 AndAlso _
      Not IsDBNull(_dt_general_params.Rows(0).Item(0)) AndAlso _
      Not String.IsNullOrEmpty(_dt_general_params.Rows(0).Item(0)) Then

      Return _dt_general_params.Rows(0).Item(0)

    Else

      Return String.Empty

    End If

  End Function ' ReadGeneralParam


  ' PURPOSE : Obtain Cashier Voucher Data
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Site Id

  Public Function GetCashierVoucherData(ByVal SubjectKey As String) As String

    Dim str_sql As String
    Dim data_table_general_params As DataTable
    Dim Value As String
    Value = ""

    ' Build query
    str_sql = "select GP_KEY_VALUE" _
              & " from GENERAL_PARAMS" _
              & " where GP_GROUP_KEY = 'Cashier.Voucher'" _
              & "  AND  GP_SUBJECT_KEY = '" & SubjectKey & "'"

    ' Check query results
    data_table_general_params = GUI_GetTableUsingSQL(str_sql, 1)

    If IsNothing(data_table_general_params) Then
      Return Value
    End If

    ' Check for rows received.
    If data_table_general_params.Rows.Count() < 1 Then
      Return Value
    End If

    Value = data_table_general_params.Rows(0).Item("GP_KEY_VALUE")

    Return Value

  End Function ' GetCashierVoucherData

  ' PURPOSE : Obtain Cashier CardPrint Data
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - CardPrint Data

  Public Function GetCashierCardPrintData(ByVal SubjectKey As String) As String

    Dim str_sql As String
    Dim data_table_general_params As DataTable
    Dim Value As String
    Value = ""

    ' Build query
    str_sql = "select GP_KEY_VALUE" _
              & " from GENERAL_PARAMS" _
              & " where GP_GROUP_KEY = 'Cashier.CardPrint'" _
              & "  AND  GP_SUBJECT_KEY = '" & SubjectKey & "'"

    ' Check query results
    data_table_general_params = GUI_GetTableUsingSQL(str_sql, 1)

    If IsNothing(data_table_general_params) Then
      Return Value
    End If

    ' Check for rows received.
    If data_table_general_params.Rows.Count() < 1 Then
      Return Value
    End If

    Value = data_table_general_params.Rows(0).Item("GP_KEY_VALUE")

    Return Value

  End Function ' GetCashierCardPrintData

  ' PURPOSE : Obtain Cashier's HandPays DAta
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Hand Pays Data

  Public Function GetCashierHandPaysData(ByVal SubjectKey As String) As String

    Dim str_sql As String
    Dim data_table_general_params As DataTable
    Dim Value As String
    Value = ""

    ' Build query
    str_sql = "select GP_KEY_VALUE" _
             & " from GENERAL_PARAMS" _
            & " where GP_GROUP_KEY = 'Cashier.HandPays'" _
            & "   and GP_SUBJECT_KEY = '" & SubjectKey & "'"

    ' Check query results
    data_table_general_params = GUI_GetTableUsingSQL(str_sql, 1)

    If IsNothing(data_table_general_params) Then
      Return Value
    End If

    ' Check for rows received.
    If data_table_general_params.Rows.Count() < 1 Then
      Return Value
    End If

    Value = data_table_general_params.Rows(0).Item("GP_KEY_VALUE")

    Return Value

  End Function ' GetCashierHandPaysData

  ' PURPOSE : Obtain Cashier's Player Tracking Data
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - CashIn Split Data

  Public Function GetCashierPlayerTrackingData(ByVal SubjectKey As String) As String

    Dim str_sql As String
    Dim data_table_general_params As DataTable
    Dim Value As String
    Value = ""

    ' Build query
    str_sql = "select GP_KEY_VALUE" _
             & " from GENERAL_PARAMS" _
            & " where GP_GROUP_KEY = 'PlayerTracking'" _
            & "   and GP_SUBJECT_KEY = '" & SubjectKey & "'"

    ' Check query results
    data_table_general_params = GUI_GetTableUsingSQL(str_sql, 1)

    If IsNothing(data_table_general_params) Then
      Return Value
    End If

    ' Check for rows received.
    If data_table_general_params.Rows.Count() < 1 Then
      Return Value
    End If

    Value = data_table_general_params.Rows(0).Item("GP_KEY_VALUE")

    Return Value

  End Function ' GetCashierPlayerTrackingData

  ' PURPOSE : Obtain Cashier's Player Tracking Do Not Award Points Data
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - CashIn Split Data

  Public Function GetCashierPlayerTrackingDoNotAwardPointsData(ByVal SubjectKey As String, Optional ByVal DefaultValue As String = "") As String

    Dim str_sql As String
    Dim data_table_general_params As DataTable

    ' Build query 
    str_sql = "select GP_KEY_VALUE FROM GENERAL_PARAMS WHERE GP_GROUP_KEY = 'PlayerTracking.DoNotAwardPoints' AND GP_SUBJECT_KEY = '" & SubjectKey & "'"
    ' Check query results
    data_table_general_params = GUI_GetTableUsingSQL(str_sql, 1)

    If IsNothing(data_table_general_params) Then
      Return DefaultValue
    End If

    ' Check for rows received.
    If data_table_general_params.Rows.Count() < 1 Then
      Return DefaultValue
    End If

    Return data_table_general_params.Rows(0).Item("GP_KEY_VALUE").ToString()

  End Function ' GetCashierPlayerTrackingDoNotAwardPointsData

  ' PURPOSE : Obtain Site Name from table GENERAL_PARAMS (Site.Name).
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Site Name

  Public Function GetSiteName() As String
    Return WSI.Common.GeneralParam.GetString("Site", "Name").Trim().ToUpper()
  End Function ' GetSiteName

  Public Function GetSiteOrMultiSiteName() As String

    Dim _site_name As String

    _site_name = ""

    If WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
      _site_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1791) ' MultiSite
    Else
      _site_name = GetSiteId() & " - " & GetSiteName()
    End If

    Return _site_name

  End Function

  ' PURPOSE : Obtain Super Center configuration
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SuperCenter Data

  Public Function GetSuperCenterData(ByVal SubjectKey As String) As String

    Dim str_sql As String
    Dim data_table_general_params As DataTable
    Dim Value As String
    Value = ""

    ' Build query
    str_sql = "select GP_KEY_VALUE" _
             & " from GENERAL_PARAMS" _
            & " where GP_GROUP_KEY = 'SuperCenter'" _
            & "   and GP_SUBJECT_KEY = '" & SubjectKey & "'"

    ' Check query results
    data_table_general_params = GUI_GetTableUsingSQL(str_sql, 1)

    If IsNothing(data_table_general_params) Then
      Return Value
    End If

    ' Check for rows received.
    If data_table_general_params.Rows.Count() < 1 Then
      Return Value
    End If

    Value = data_table_general_params.Rows(0).Item("GP_KEY_VALUE")

    Return Value

  End Function ' GetSuperCenterData

  Public Function DB_GeneralParam_Update(ByVal GroupKey As String, _
                                       ByVal SubjectKey As String, _
                                       ByVal NewValue As String, _
                                       ByVal Trx As SqlTransaction) As Boolean

    Return GeneralParam.GenerateGeneralParam(GroupKey, SubjectKey, NewValue, Trx)

  End Function

  Private Sub InitUserNLS()

    WSI.Common.Users.Init(GLB_NLS_GUI_PLAYER_TRACKING.GetString(591), _
                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(592), _
                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(593), _
                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(594), _
                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(595), _
                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(596), _
                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(597), _
                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(598), _
                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(599), _
                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(600), _
                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(601), _
                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(602), _
                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(603), _
                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(604), _
                          GLB_NLS_GUI_PLAYER_TRACKING.GetString(605))

  End Sub

  Private Function CheckMode(ByVal GuiId As ENUM_GUI) As Boolean

    '
    ' - Check if is site
    '
    If GuiId = public_globals.ENUM_GUI.WIGOS_GUI And WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(162), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1773))
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            "CheckMode", _
                            "", _
                            "", _
                            "", _
                            "Login with Site mode on database not enabled it")

      Return False

    End If

    '
    ' - Check if is multisite
    '
    If GuiId = public_globals.ENUM_GUI.MULTISITE_GUI And Not WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then

      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(162), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1697))
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            "CheckMode", _
                            "", _
                            "", _
                            "", _
                            "Login with Multisite mode on database not enabled it")

      Return False

    End If

    If GuiId = public_globals.ENUM_GUI.CASHDESK_DRAWS_GUI And Not WSI.Common.GeneralParam.GetBoolean("CashDesk.Draw", "IsCashDeskDraw", False) Then

      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(162), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2283))
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            GLB_ApplicationName, _
                            "CheckMode", _
                            "", _
                            "", _
                            "", _
                            "Login with CashDeskDrawGUI mode on database not enabled it")

      Return False

    End If

    Return True

  End Function

  ' PURPOSE : Set Features screen forms when resolution screen below from X Resolution.
  '
  '  PARAMS:
  '     - INPUT:
  '           FormToApply:    Form
  '           PanelFilter:    Panel
  '           PanelData:      Panel
  '           SetAutoScroll:  Boolean
  '           SetSize:        Boolean
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SuperCenter Data
  Public Sub GUI_AutoAdjustResolutionForm(ByRef FormToApply As System.Windows.Forms.Form, _
                                            ByRef PanelFilter As System.Windows.Forms.Panel, _
                                            ByRef PanelData As System.Windows.Forms.Panel)

    Dim _desktop_size As System.Drawing.Size
    Dim _off_set_height As Integer
    Dim _off_set_width As Integer
    Dim _menu_height As Integer
    Dim _menu_width As Integer
    Dim _primary_screen_width As Integer

    Try

      _off_set_height = 0
      _off_set_width = 0
      _menu_width = 0
      _menu_height = System.Windows.Forms.SystemInformation.MenuHeight

      'Get menuheight / menuwidth
      If Not FormToApply.MdiParent Is Nothing Then

        For Each _ctrl_mdi As Windows.Forms.Control In FormToApply.MdiParent.Controls
          If TypeOf _ctrl_mdi Is Windows.Forms.MenuStrip Then

            Select Case _ctrl_mdi.Dock
              Case Windows.Forms.DockStyle.Top, Windows.Forms.DockStyle.Bottom

                _menu_height = _ctrl_mdi.Height
                _menu_width = 0
              Case Windows.Forms.DockStyle.Left, Windows.Forms.DockStyle.Right

                _menu_height = 0
                _menu_width = _ctrl_mdi.Width
            End Select

            Exit For
          End If
        Next
      End If

      'Caculate offset
      If Not FormToApply.MdiParent Is Nothing AndAlso _
             FormToApply.Modal = False Then

        'Offset height
        _off_set_height = _menu_height + _
                          System.Windows.Forms.SystemInformation.CaptionHeight + _
                          (System.Windows.Forms.SystemInformation.Border3DSize.Height * 2) + _
                          FormToApply.MdiParent.Padding.Vertical

        'Offset width
        _off_set_width = _menu_width + _
                         (System.Windows.Forms.SystemInformation.Border3DSize.Width * 2) + _
                         FormToApply.MdiParent.Padding.Horizontal
      Else

        'Offset height
        _off_set_height = (System.Windows.Forms.SystemInformation.Border3DSize.Height * 2)

        'Offset width
        _off_set_width = (System.Windows.Forms.SystemInformation.Border3DSize.Width * 2)
      End If

      'Get Resolution Screen where is showing the Form
      _desktop_size = System.Windows.Forms.Screen.FromControl(FormToApply).WorkingArea.Size

      _primary_screen_width = System.Windows.Forms.SystemInformation.PrimaryMonitorSize.Width

      Dim _location_x As Integer
      _location_x = FormToApply.Location.X

      If Not (System.Windows.Forms.Screen.FromControl(FormToApply).Primary) Then
        If (_location_x < 0) Then
          _location_x += _primary_screen_width
        Else
          _location_x -= _primary_screen_width
        End If
      End If

      'Apply Form Size (if needed)
      ' Form Size Width
      If (FormToApply.Size.Width + _
          _off_set_width) + _
          _location_x > _desktop_size.Width Then

        FormToApply.Size = New System.Drawing.Size(_desktop_size.Width - _
                                                   _off_set_width - _
                                                   _location_x, _
                                                   FormToApply.Height)
      End If


      'Set AutoScroll to true
      If Not PanelFilter Is Nothing Then

        PanelFilter.AutoScroll = True
      End If

      If Not PanelData Is Nothing Then

        PanelData.AutoScroll = True
      End If

    Catch ex As Exception

      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            "mdl_Misc", _
                            "GUI_SetFormFeatures", _
                            ex.Message)
    End Try
  End Sub


End Module
