'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
' 
' MODULE NAME:   mdl_permissions_cashdesk_draws.vb
' DESCRIPTION:   
' AUTHOR:        Joaquin Calero Andujar
' CREATION DATE: 27-AUG-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 27-AUG-2013  JCA    Initial version
' 15-SEP-2016  ETP    Fixed bugs 17585 - 17607: Generic reports: add permissions config.
' -------------------------------------------------------------------

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_CommonOperations.CLASS_REGISTRY_DATA
Imports GUI_CommonOperations.CLASS_REGISTRY_DEFINITION
Imports GUI_Controls
Imports WSI.Common

Public Module mdl_permissions_cashdesk_draws

  Public Sub InitProfilesCashDeskDraws(ByRef WigosForms As CLASS_GUI_FORMS)

    Call WigosForms.Add(ENUM_FORM.FORM_USER_PROFILE_SEL, GLB_NLS_GUI_CONFIGURATION.Id(383), ENUM_FEATURES.NONE)
    Call WigosForms.Add(ENUM_FORM.FORM_USER_EDIT, GLB_NLS_GUI_CONFIGURATION.Id(325), ENUM_FEATURES.NONE)
    Call WigosForms.Add(ENUM_FORM.FORM_PROFILE_EDIT, GLB_NLS_GUI_CONFIGURATION.Id(318), ENUM_FEATURES.NONE)
    Call WigosForms.Add(ENUM_FORM.FORM_GUI_AUDITOR, GLB_NLS_GUI_AUDITOR.Id(327), ENUM_FEATURES.NONE)
    Call WigosForms.Add(ENUM_FORM.FORM_GENERAL_PARAMS, GLB_NLS_GUI_AUDITOR.Id(460), ENUM_FEATURES.NONE)

    Call WigosForms.Add(ENUM_FORM.FORM_SW_DOWNLOAD_VERSIONS, GLB_NLS_GUI_SW_DOWNLOAD.Id(201), ENUM_FEATURES.NONE)
    Call WigosForms.Add(ENUM_FORM.FORM_SW_DOWNLOAD_DETAILS, GLB_NLS_GUI_SW_DOWNLOAD.Id(300), ENUM_FEATURES.NONE)
    Call WigosForms.Add(ENUM_FORM.FORM_SERVICE_MONITOR, GLB_NLS_GUI_SYSTEM_MONITOR.Id(201), ENUM_FEATURES.NONE)
    Call WigosForms.Add(ENUM_FORM.FORM_MISC_SW_BY_BUILD, GLB_NLS_GUI_SW_DOWNLOAD.Id(239), ENUM_FEATURES.NONE)
    Call WigosForms.Add(ENUM_FORM.FORM_LICENCE_VERSIONS, GLB_NLS_GUI_SW_DOWNLOAD.Id(229), ENUM_FEATURES.NONE)
    Call WigosForms.Add(ENUM_FORM.FORM_LICENCE_DETAILS, GLB_NLS_GUI_SW_DOWNLOAD.Id(251), ENUM_FEATURES.NONE)

    ' RCI 15-JUL-2011: Alarms
    Call WigosForms.Add(ENUM_FORM.FORM_ALARMS, GLB_NLS_GUI_ALARMS.Id(201), ENUM_FEATURES.NONE)

    ' HBB 02-AGO-2012
    Call WigosForms.Add(ENUM_FORM.FORM_PASSWORD_CONFIGURATION, GLB_NLS_GUI_PLAYER_TRACKING.Id(1175), ENUM_FEATURES.NONE)

    Call WigosForms.Add(ENUM_FORM.FORM_DRAWS_CASHDESK_DETAIL, GLB_NLS_GUI_PLAYER_TRACKING.Id(2245), ENUM_FEATURES.NONE)

    Call WigosForms.Add(ENUM_FORM.FORM_DRAWS_CASHDESK_SUMMARY, GLB_NLS_GUI_PLAYER_TRACKING.Id(2364), ENUM_FEATURES.NONE)


  End Sub 'InitProfilesCashDeskDraws

End Module
