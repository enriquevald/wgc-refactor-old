'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_commands
' DESCRIPTION:   WCP commands common
'                   
' AUTHOR:        Miquel Beltran Febrer
' CREATION DATE: 16-JUN-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-JUN-2010  MBF    Initial version
' 12-NOV-2014  FJC    Add command: 'WCP_COMMAND_SITE_JACKPOT_AWARDED'
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports System.Data
Imports System.Data.SqlClient
Imports WSI.Common

Public Module mdl_commands

#Region " Enums "

  Public Enum ENUM_WCP_COMMANDS_STATUS

    STATUS_PENDING_SEND = 0
    STATUS_PENDING_REPLY = 1
    STATUS_OK = 2
    STATUS_ERROR = 3
    STATUS_TIMEOUT_DISCONNECTED = 4
    STATUS_TIMEOUT_NOT_REPLY = 5

  End Enum
#End Region ' Enums

#Region " Constants "
  Public TerminalReportType As ReportType = ReportType.Provider

  Private TERMINAL_DATA_COLUMNS As Int32

  ' Grid Columns
  Private GRID_COLUMN_INDEX As Integer
  Private GRID_COLUMN_CREATED As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_PROVIDER As Integer
  Private GRID_COLUMN_TERMINAL As Integer
  Private GRID_COLUMN_CODE As Integer
  Private GRID_COLUMN_STATUS As Integer
  Private GRID_COLUMN_COMMAND_ID As Integer

  Private GRID_COLUMNS As Integer
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_DATE As Integer = 2000
  Private Const GRID_WIDTH_PROVIDER As Integer = 2400
  Private Const GRID_WIDTH_TERMINAL As Integer = 2400
  Private Const GRID_WIDTH_CODE As Integer = 1450
  Private Const GRID_WIDTH_STATUS As Integer = 2400

  ' Counters
  Private Const COUNTER_CMD_PENDING As Integer = 1
  Private Const COUNTER_CMD_OK As Integer = 2
  Private Const COUNTER_CMD_ERROR As Integer = 3
  Private Const COUNTER_CMD_TIMEOUT As Integer = 4

  Private Const MAX_COUNTERS As Integer = 4

  ' Colors
  Private Const COLOR_CMD_PENDING_SEND_BACK = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
  Private Const COLOR_CMD_PENDING_SEND_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_CMD_PENDING_REPLY_BACK = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
  Private Const COLOR_CMD_PENDING_REPLY_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_CMD_OK_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_CMD_OK_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_CMD_ERROR_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  Private Const COLOR_CMD_ERROR_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_CMD_TIMEOUT_DISCONNECTED_BACK = ENUM_GUI_COLOR.GUI_COLOR_GREY_01
  Private Const COLOR_CMD_TIMEOUT_DISCONNECTED_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_CMD_TIMEOUT_NOT_REPLY_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  Private Const COLOR_CMD_TIMEOUT_NOT_REPLY_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00

#End Region ' Constants

#Region " Structures "

  Public Structure TYPE_COMMAND_ITEM
    Dim id As Long
    Dim terminal_id As Long
    Dim code As Integer
    Dim status As Integer
    Dim previous_status As Integer
    Dim date_send As DateTime
    Dim date_status_change As DateTime
    Dim terminal_name As String
    Dim provider_name As String
  End Structure

#End Region ' Structures

#Region " Public Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - Grid As uc_grid
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub WCP_Command_StyleSheet(ByVal Grid As uc_grid, ByVal CountersVisible As Boolean)
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .Counter(COUNTER_CMD_PENDING).Visible = CountersVisible
      .Counter(COUNTER_CMD_PENDING).BackColor = GetColor(COLOR_CMD_PENDING_REPLY_BACK)
      .Counter(COUNTER_CMD_PENDING).ForeColor = GetColor(COLOR_CMD_PENDING_REPLY_FORE)

      .Counter(COUNTER_CMD_OK).Visible = CountersVisible
      .Counter(COUNTER_CMD_OK).BackColor = GetColor(COLOR_CMD_OK_BACK)
      .Counter(COUNTER_CMD_OK).ForeColor = GetColor(COLOR_CMD_OK_FORE)

      .Counter(COUNTER_CMD_ERROR).Visible = CountersVisible
      .Counter(COUNTER_CMD_ERROR).BackColor = GetColor(COLOR_CMD_ERROR_BACK)
      .Counter(COUNTER_CMD_ERROR).ForeColor = GetColor(COLOR_CMD_ERROR_FORE)

      .Counter(COUNTER_CMD_TIMEOUT).Visible = CountersVisible
      .Counter(COUNTER_CMD_TIMEOUT).BackColor = GetColor(COLOR_CMD_TIMEOUT_DISCONNECTED_BACK)
      .Counter(COUNTER_CMD_TIMEOUT).ForeColor = GetColor(COLOR_CMD_TIMEOUT_DISCONNECTED_FORE)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Command ID
      .Column(GRID_COLUMN_COMMAND_ID).Width = 0

      ' Date Created
      .Column(GRID_COLUMN_CREATED).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(338)
      .Column(GRID_COLUMN_CREATED).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_CREATED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(TerminalReportType)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = _terminal_columns(_idx).Header0
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        If _terminal_columns(_idx).Column = ReportColumn.Provider Then
          GRID_COLUMN_PROVIDER = GRID_INIT_TERMINAL_DATA + _idx
        End If
        If _terminal_columns(_idx).Column = ReportColumn.Terminal Then
          GRID_COLUMN_TERMINAL = GRID_INIT_TERMINAL_DATA + _idx
        End If
      Next

      ' Command code
      .Column(GRID_COLUMN_CODE).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(308)
      .Column(GRID_COLUMN_CODE).Width = GRID_WIDTH_CODE
      .Column(GRID_COLUMN_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Status
      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(261)
      .Column(GRID_COLUMN_STATUS).Width = GRID_WIDTH_STATUS
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
    End With

  End Sub ' WCP_Command_StyleSheet

  ' PURPOSE: Get Color based on Command Status
  '
  '  PARAMS:
  '     - INPUT:
  '           - Status As ENUM_WCP_COMMANDS_STATUS
  '     - OUTPUT:
  '           - ForeColor As Drawing.Color
  '           - BackColor As Drawing.Color
  '
  ' RETURNS:
  '     - None
  Public Sub WCP_Command_GetUpdateStatusColor(ByVal Status As ENUM_WCP_COMMANDS_STATUS, _
                                              ByRef ForeColor As Drawing.Color, _
                                              ByRef BackColor As Drawing.Color)
    Select Case Status

      Case ENUM_WCP_COMMANDS_STATUS.STATUS_OK
        ForeColor = GetColor(COLOR_CMD_OK_FORE)
        BackColor = GetColor(COLOR_CMD_OK_BACK)

      Case ENUM_WCP_COMMANDS_STATUS.STATUS_PENDING_SEND
        ForeColor = GetColor(COLOR_CMD_PENDING_SEND_FORE)
        BackColor = GetColor(COLOR_CMD_PENDING_SEND_BACK)

      Case ENUM_WCP_COMMANDS_STATUS.STATUS_PENDING_REPLY
        ForeColor = GetColor(COLOR_CMD_PENDING_REPLY_FORE)
        BackColor = GetColor(COLOR_CMD_PENDING_REPLY_BACK)

      Case ENUM_WCP_COMMANDS_STATUS.STATUS_ERROR
        ForeColor = GetColor(COLOR_CMD_ERROR_FORE)
        BackColor = GetColor(COLOR_CMD_ERROR_BACK)

      Case ENUM_WCP_COMMANDS_STATUS.STATUS_TIMEOUT_DISCONNECTED
        ForeColor = GetColor(COLOR_CMD_TIMEOUT_DISCONNECTED_FORE)
        BackColor = GetColor(COLOR_CMD_TIMEOUT_DISCONNECTED_BACK)

      Case ENUM_WCP_COMMANDS_STATUS.STATUS_TIMEOUT_NOT_REPLY
        ForeColor = GetColor(COLOR_CMD_TIMEOUT_NOT_REPLY_FORE)
        BackColor = GetColor(COLOR_CMD_TIMEOUT_NOT_REPLY_BACK)

      Case Else
        ForeColor = GetColor(COLOR_CMD_ERROR_FORE)
        BackColor = GetColor(COLOR_CMD_ERROR_BACK)

    End Select

  End Sub 'WCP_Command_GetUpdateStatusColor

  ' PURPOSE: Get response from WCP Command
  '
  '  PARAMS:
  '     - INPUT:
  '           - CommandId
  '     - OUTPUT:
  '           - Response
  '
  ' RETURNS:
  '     - return code:
  '         0: Error
  '         1: OK
  '         2: Invalid command Id
  '         3: Command has no response

  Public Function WCP_Command_GetResponse(ByVal CommandId As Long, ByRef Response As String) As Integer

    Dim _sql_connection As SqlConnection
    Dim _sql_command As SqlCommand
    Dim _str_sql As String
    Dim _response_deflated As String
    Dim _reader As SqlDataReader

    Try
      _sql_connection = NewSQLConnection()

      If Not (_sql_connection Is Nothing) Then
        ' Connected
        If _sql_connection.State <> ConnectionState.Open Then
          ' Connecting ..
          _sql_connection.Open()
        End If

        If Not _sql_connection.State = ConnectionState.Open Then
          Exit Try
        End If
      End If

      _str_sql = "SELECT CMD_RESPONSE " & _
                " FROM WCP_COMMANDS " & _
                " WHERE CMD_ID    = @command_id "

      _sql_command = New SqlCommand(_str_sql)
      _sql_command.Connection = _sql_connection

      _sql_command.Parameters.Add("@command_id", SqlDbType.BigInt, 0).Value = CommandId

      _reader = _sql_command.ExecuteReader()

      If (_reader.Read()) Then
        _response_deflated = _reader(0)

        If (_response_deflated = "" _
          Or _response_deflated Is Nothing) Then
          Return 3
        End If

        WSI.Common.Misc.InflateString(_response_deflated, Response)

        Return 1
      Else
        Return 2
      End If

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

      Return 0
    Finally
    End Try

  End Function ' WCP_Command_GetResponse

  ' PURPOSE: Get status text from WCP Command
  '
  '  PARAMS:
  '     - INPUT:
  '           - Status
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - return the text equivalent

  Public Function WCP_Command_StatusText(ByVal Status As ENUM_WCP_COMMANDS_STATUS) As String

    Select Case Status
      Case ENUM_WCP_COMMANDS_STATUS.STATUS_PENDING_SEND
        Return GLB_NLS_GUI_CONTROLS.GetString(402)
      Case ENUM_WCP_COMMANDS_STATUS.STATUS_PENDING_REPLY
        Return GLB_NLS_GUI_CONTROLS.GetString(403)
      Case ENUM_WCP_COMMANDS_STATUS.STATUS_OK
        Return GLB_NLS_GUI_CONTROLS.GetString(404)
      Case ENUM_WCP_COMMANDS_STATUS.STATUS_ERROR
        Return GLB_NLS_GUI_CONTROLS.GetString(405)
      Case ENUM_WCP_COMMANDS_STATUS.STATUS_TIMEOUT_DISCONNECTED
        Return GLB_NLS_GUI_CONTROLS.GetString(406)
      Case ENUM_WCP_COMMANDS_STATUS.STATUS_TIMEOUT_NOT_REPLY
        Return GLB_NLS_GUI_CONTROLS.GetString(407)
      Case Else
        Return GLB_NLS_GUI_CONTROLS.GetString(408)  ' Unknown
    End Select

  End Function ' WCP_Command_StatusText

  ' PURPOSE: Get command text from WCP Command
  '
  '  PARAMS:
  '     - INPUT:
  '           - Command
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - return the text equivalent

  Public Function WCP_Command_Text(ByVal Command As WSI.Common.WCP_CommandCode) As String

    Select Case Command
      Case WSI.Common.WCP_CommandCode.GetLogger
        Return GLB_NLS_GUI_CONTROLS.GetString(415)
      Case WSI.Common.WCP_CommandCode.Restart
        Return GLB_NLS_GUI_CONTROLS.GetString(416)
      Case Else
        Return GLB_NLS_GUI_CONTROLS.GetString(408)  ' Unknown
    End Select

  End Function ' WCP_Command_Text

  ' PURPOSE: Init List Counter, and set Visible Property on Grid.Counters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - List of counters initialized

  Public Function WCP_Command_CounterStatusInit()
    Dim _counter_list(0 To MAX_COUNTERS + 1) As Integer

    _counter_list(COUNTER_CMD_PENDING) = 0
    _counter_list(COUNTER_CMD_OK) = 0
    _counter_list(COUNTER_CMD_ERROR) = 0
    _counter_list(COUNTER_CMD_TIMEOUT) = 0

    Return _counter_list
  End Function ' WCP_Command_CounterStatusInit

  ' PURPOSE: Increment counters of the CounterList based on Status 
  '
  '  PARAMS:
  '     - INPUT:
  '           - Status As Integer
  '           - CounterList() As Integer
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub WCP_Command_CounterStatusIncrement(ByVal Status As Integer, ByVal CounterList() As Integer)

    Select Case Status
      Case ENUM_WCP_COMMANDS_STATUS.STATUS_PENDING_SEND, _
           ENUM_WCP_COMMANDS_STATUS.STATUS_PENDING_REPLY
        CounterList(COUNTER_CMD_PENDING) = CounterList(COUNTER_CMD_PENDING) + 1
      Case ENUM_WCP_COMMANDS_STATUS.STATUS_OK
        CounterList(COUNTER_CMD_OK) = CounterList(COUNTER_CMD_OK) + 1
      Case ENUM_WCP_COMMANDS_STATUS.STATUS_ERROR, _
           ENUM_WCP_COMMANDS_STATUS.STATUS_TIMEOUT_NOT_REPLY
        CounterList(COUNTER_CMD_ERROR) = CounterList(COUNTER_CMD_ERROR) + 1
      Case ENUM_WCP_COMMANDS_STATUS.STATUS_TIMEOUT_DISCONNECTED
        CounterList(COUNTER_CMD_TIMEOUT) = CounterList(COUNTER_CMD_TIMEOUT) + 1
    End Select

  End Sub ' WCP_Command_CounterStatusIncrement

  ' PURPOSE: Set final values on the Grid.Counters based on the CounterList
  '
  '  PARAMS:
  '     - INPUT:
  '           - Grid As uc_grid
  '           - CounterList() As Integer
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub WCP_Command_CounterStatusUpdate(ByVal Grid As uc_grid, ByVal CounterList() As Integer)

    Grid.Counter(COUNTER_CMD_PENDING).Value = CounterList(COUNTER_CMD_PENDING)
    Grid.Counter(COUNTER_CMD_OK).Value = CounterList(COUNTER_CMD_OK)
    Grid.Counter(COUNTER_CMD_ERROR).Value = CounterList(COUNTER_CMD_ERROR)
    Grid.Counter(COUNTER_CMD_TIMEOUT).Value = CounterList(COUNTER_CMD_TIMEOUT)

  End Sub ' WCP_Command_CounterStatusUpdate

  ' PURPOSE: Set Grid.Cell values based on Command parameter. Also "paint" the row based on Command.status.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Grid As uc_grid
  '           - RowIndex As Integer
  '           - Command As TYPE_COMMAND_ITEM
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS: True (the row should be added) or False (the row can not be added)

  Public Function WCP_Command_SetupRow(ByVal Grid As uc_grid, ByVal RowIndex As Integer, ByVal Command As TYPE_COMMAND_ITEM) As Boolean

    Dim fore_color As Drawing.Color
    Dim back_color As Drawing.Color
    Dim _terminal_data As List(Of Object)

    ' Command ID
    Grid.Cell(RowIndex, GRID_COLUMN_COMMAND_ID).Value = Command.id

    ' Terminal Report
    _terminal_data = TerminalReport.GetReportDataList(Command.terminal_id, TerminalReportType)
    For _idx As Int32 = 0 To _terminal_data.Count - 1
      If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
        Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
      End If
    Next

    ' Code
    Grid.Cell(RowIndex, GRID_COLUMN_CODE).Value = WCP_Command_Text(Command.code)

    ' status
    Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = WCP_Command_StatusText(Command.status)

    ' Date created
    Grid.Cell(RowIndex, GRID_COLUMN_CREATED).Value = GUI_FormatDate(Command.date_send, _
                                                                        ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                        ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    WCP_Command_GetUpdateStatusColor(Command.status, fore_color, back_color)
    Grid.Row(RowIndex).ForeColor = fore_color
    Grid.Row(RowIndex).BackColor = back_color

    Return True
  End Function ' WCP_Command_SetupRow

  ' PURPOSE: Get the Command ID from the RowIndex of the Grid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Grid As uc_grid
  '           - RowIndex As Integer
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - UInt64: Command ID

  Public Function WCP_Command_GetRowId(ByVal Grid As uc_grid, ByVal RowIndex As Integer) As UInt64
    Dim _status As String
    Dim _code As String
    Dim _value As UInt64

    _value = 0
    _status = Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value
    _code = Grid.Cell(RowIndex, GRID_COLUMN_CODE).Value

    If _status.Equals(WCP_Command_StatusText(ENUM_WCP_COMMANDS_STATUS.STATUS_OK)) _
       And _code.Equals(WCP_Command_Text(WSI.Common.WCP_CommandCode.GetLogger)) Then

      _value = Grid.Cell(RowIndex, GRID_COLUMN_COMMAND_ID).Value
    End If

    Return _value
  End Function ' WCP_Command_GetRowId

  ' PURPOSE: Get the Command Provider and Terminal from the RowIndex of the Grid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Grid As uc_grid
  '           - RowIndex As Integer
  '     - OUTPUT:
  '           - Provider As String
  '           - Terminal As String
  '
  ' RETURNS:
  '     - None
  Public Sub WCP_Command_GetRowProviderTerminal(ByVal Grid As uc_grid, ByVal RowIndex As Integer, _
                                                ByRef Provider As String, ByRef Terminal As String)
    Provider = Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER).Value
    Terminal = Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL).Value
  End Sub ' WCP_Command_GetRowProviderTerminal

  ' PURPOSE: Get the Command Created Date from the RowIndex of the Grid.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Grid As uc_grid
  '           - RowIndex As Integer
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String: Command Created Date

  Public Function WCP_Command_GetRowCreatedDate(ByVal Grid As uc_grid, ByVal RowIndex As Integer) As String
    Return Grid.Cell(RowIndex, GRID_COLUMN_CREATED).Value
  End Function ' WCP_Command_GetRowCreatedDate

#End Region ' Public Functions

  Private Sub GridColumnReIndex()

    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(TerminalReportType)

    GRID_COLUMN_INDEX = 0
    GRID_COLUMN_CREATED = 1
    GRID_INIT_TERMINAL_DATA = 2
    GRID_COLUMN_PROVIDER = GRID_INIT_TERMINAL_DATA
    GRID_COLUMN_TERMINAL = GRID_INIT_TERMINAL_DATA + 1
    GRID_COLUMN_CODE = TERMINAL_DATA_COLUMNS + 2
    GRID_COLUMN_STATUS = TERMINAL_DATA_COLUMNS + 3
    GRID_COLUMN_COMMAND_ID = TERMINAL_DATA_COLUMNS + 4

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 5

  End Sub

End Module ' mdl_commands
