'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_software_validations
' DESCRIPTION:   Software validations common functions
'                   
' AUTHOR:        David Rigal Vall
' CREATION DATE: 07-ABR-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-ABR-2014  DRV    Initial version
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports WSI.Common
Imports System.Data.SqlClient

Public Module mdl_software_validations

#Region " Constants "

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_INSERTED_DATETIME As Integer = 1
  Private Const GRID_COLUMN_LAST_REQUEST_DATETIME As Integer = 2
  Private Const GRID_COLUMN_RECEIVED_DATETIME As Integer = 3
  Private Const GRID_COLUMN_PROVIDER As Integer = 4
  Private Const GRID_COLUMN_TERMINAL As Integer = 5
  Private Const GRID_COLUMN_STATUS As Integer = 6
  Private Const GRID_COLUMN_STATUS_CODE As Integer = 7
  Private Const GRID_COLUMN_LKT_STATUS As Integer = 8
  Private Const GRID_COLUMN_EXPECTED_SIGNATURE As Integer = 9

  Private Const GRID_COLUMNS As Integer = 10
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Width
  Private Const GRID_WIDTH_INDEX As Integer = 200
  Private Const GRID_WIDTH_INSERTED_DATETIME As Integer = 2000
  Private Const GRID_WIDTH_LAST_REQUEST_DATETIME As Integer = 2000
  Private Const GRID_WIDTH_RECEIVED_DATETIME As Integer = 2000
  Private Const GRID_WIDTH_PROVIDER As Integer = 2400
  Private Const GRID_WIDTH_TERMINAL As Integer = 2400
  Private Const GRID_WIDTH_STATUS As Integer = 2800
  Private Const GRID_WIDTH_STATUS_CODE As Integer = 1
  Private Const GRID_WIDTH_LKT_STATUS As Integer = 1
  Private Const GRID_WIDTH_EXPECTED_SIGNATURE As Integer = 1800

  ' Counters
  Private Const COUNTER_SV_UNKNOWN As Integer = 1
  Private Const COUNTER_SV_PENDING As Integer = 2
  Private Const CONUTER_SV_NOTIFIED As Integer = 3
  Private Const COUNTER_SV_OK As Integer = 4
  Private Const COUNTER_SV_CANCELED_OR_TIMEOUT As Integer = 5
  Private Const COUNTER_SV_ERROR As Integer = 6

  Private Const MAX_COUNTERS As Integer = 6

  ' Colors
  Private Const COLOR_SV_UNKNOWN_BACK = ENUM_GUI_COLOR.GUI_COLOR_ORANGE_00
  Private Const COLOR_SV_UNKNOWN_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_SV_ERROR_BACK = ENUM_GUI_COLOR.GUI_COLOR_ORANGE_00
  Private Const COLOR_SV_ERROR_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_SV_PENDING_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_SV_PENDING_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_SV_NOTIFIED_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_SV_NOTIFIED_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_SV_VALIDATING_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_SV_VALIDATING_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_SV_VALIDATE_OK_BACK = ENUM_GUI_COLOR.GUI_COLOR_GREEN_00
  Private Const COLOR_SV_VALIDATE_OK_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_SV_VALIDATE_ERROR_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_00
  Private Const COLOR_SV_VALIDATE_ERROR_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_SV_TIMEOUT_BACK = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
  Private Const COLOR_SV_TIMEOUT_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00

#End Region 'Constants

#Region " Structures "

  Public Structure TYPE_SV_ITEM
    Dim id As Long
    Dim inserted_datetime As DateTime
    Dim last_request As DateTime
    Dim received As DateTime
    Dim terminal_id As Long
    Dim terminal_name As String
    Dim provider_name As String
    Dim status As Integer
    Dim previous_status As Integer
    Dim sas_host_error As Integer
    Dim expected_signature As String
  End Structure

#End Region ' Structures

#Region " Public Functions "

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - Grid As uc_grid
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub SoftwareValidationStatus_StyleSheet(ByVal Grid As uc_grid, ByVal CountersVisible As Boolean)
    With Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      ' INDEX
      .Column(GRID_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Date Created
      .Column(GRID_COLUMN_INSERTED_DATETIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4826)
      .Column(GRID_COLUMN_INSERTED_DATETIME).Width = GRID_WIDTH_INSERTED_DATETIME
      .Column(GRID_COLUMN_INSERTED_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Date Requested
      .Column(GRID_COLUMN_LAST_REQUEST_DATETIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4827)
      .Column(GRID_COLUMN_LAST_REQUEST_DATETIME).Width = GRID_WIDTH_LAST_REQUEST_DATETIME
      .Column(GRID_COLUMN_LAST_REQUEST_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Date Received
      .Column(GRID_COLUMN_RECEIVED_DATETIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4828)
      .Column(GRID_COLUMN_RECEIVED_DATETIME).Width = GRID_WIDTH_RECEIVED_DATETIME
      .Column(GRID_COLUMN_RECEIVED_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Provider
      .Column(GRID_COLUMN_PROVIDER).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(315)
      .Column(GRID_COLUMN_PROVIDER).Width = GRID_WIDTH_PROVIDER
      .Column(GRID_COLUMN_PROVIDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal
      .Column(GRID_COLUMN_TERMINAL).Header(0).Text = GLB_NLS_GUI_STATISTICS.GetString(300)
      .Column(GRID_COLUMN_TERMINAL).Width = GRID_WIDTH_TERMINAL
      .Column(GRID_COLUMN_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Status
      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(261)
      .Column(GRID_COLUMN_STATUS).Width = GRID_WIDTH_STATUS
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Status Code
      .Column(GRID_COLUMN_STATUS_CODE).Header(0).Text = ""
      .Column(GRID_COLUMN_STATUS_CODE).Width = GRID_WIDTH_STATUS_CODE
      .Column(GRID_COLUMN_STATUS_CODE).IsColumnPrintable = False

      ' LKT 
      .Column(GRID_COLUMN_LKT_STATUS).Header(0).Text = ""
      .Column(GRID_COLUMN_LKT_STATUS).Width = GRID_WIDTH_LKT_STATUS
      .Column(GRID_COLUMN_LKT_STATUS).IsColumnPrintable = False

      'Expected Signature
      .Column(GRID_COLUMN_EXPECTED_SIGNATURE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4840)
      .Column(GRID_COLUMN_EXPECTED_SIGNATURE).Width = GRID_WIDTH_EXPECTED_SIGNATURE
      .Column(GRID_COLUMN_EXPECTED_SIGNATURE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With

  End Sub ' WCP_Command_StyleSheet

  ' PURPOSE: Get Color based on Command Status
  '
  '  PARAMS:
  '     - INPUT:
  '           - Status As ENUM_WCP_COMMANDS_STATUS
  '     - OUTPUT:
  '           - ForeColor As Drawing.Color
  '           - BackColor As Drawing.Color
  '
  ' RETURNS:
  '     - None
  Public Sub SoftwareValidationStatus_GetUpdateStatusColor(ByVal Status As SoftwareValidationStatus, _
                                              ByRef ForeColor As Drawing.Color, _
                                              ByRef BackColor As Drawing.Color)
    Select Case Status

      Case SoftwareValidationStatus.Unknown
        ForeColor = GetColor(COLOR_SV_UNKNOWN_FORE)
        BackColor = GetColor(COLOR_SV_UNKNOWN_BACK)

      Case SoftwareValidationStatus.Pending
        ForeColor = GetColor(COLOR_SV_PENDING_FORE)
        BackColor = GetColor(COLOR_SV_PENDING_BACK)

      Case SoftwareValidationStatus.Notified
        ForeColor = GetColor(COLOR_SV_NOTIFIED_FORE)
        BackColor = GetColor(COLOR_SV_NOTIFIED_BACK)

      Case SoftwareValidationStatus.ConfirmedNonValidated
        ForeColor = GetColor(COLOR_SV_VALIDATING_FORE)
        BackColor = GetColor(COLOR_SV_VALIDATING_BACK)

      Case SoftwareValidationStatus.ConfirmedOK
        ForeColor = GetColor(COLOR_SV_VALIDATE_OK_FORE)
        BackColor = GetColor(COLOR_SV_VALIDATE_OK_BACK)

      Case SoftwareValidationStatus.ConfirmedError
        ForeColor = GetColor(COLOR_SV_VALIDATE_ERROR_FORE)
        BackColor = GetColor(COLOR_SV_VALIDATE_ERROR_BACK)

      Case SoftwareValidationStatus.CanceledTimeout
        ForeColor = GetColor(COLOR_SV_TIMEOUT_FORE)
        BackColor = GetColor(COLOR_SV_TIMEOUT_BACK)

      Case SoftwareValidationStatus.Error
        ForeColor = GetColor(COLOR_SV_ERROR_FORE)
        BackColor = GetColor(COLOR_SV_ERROR_BACK)

      Case Else
        ForeColor = GetColor(COLOR_SV_UNKNOWN_FORE)
        BackColor = GetColor(COLOR_SV_UNKNOWN_BACK)

    End Select

  End Sub 'WCP_Command_GetUpdateStatusColor



  ' PURPOSE: Get status text from Software Validation
  '
  '  PARAMS:
  '     - INPUT:
  '           - Status
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - return the text equivalent

  Public Function SoftwareValidationStatus_StatusText(ByVal Status As SoftwareValidationStatus) As String

    Select Case Status
      Case SoftwareValidationStatus.CanceledTimeout
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4822)
      Case SoftwareValidationStatus.ConfirmedNonValidated
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4821)
      Case SoftwareValidationStatus.Error
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4823)
      Case SoftwareValidationStatus.Notified
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4820)
      Case SoftwareValidationStatus.Pending
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4819)
      Case SoftwareValidationStatus.Unknown
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4824)
      Case SoftwareValidationStatus.ConfirmedOK
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4829)
      Case SoftwareValidationStatus.ConfirmedError
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4830)
      Case Else
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4824)
    End Select

  End Function ' WCP_Command_StatusText


  ' PURPOSE: Get LKT status text from Software Validation
  '
  '  PARAMS:
  '     - INPUT:
  '           - Status
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - return the text equivalent

  Public Function SoftwareValidationStatus_SasHostStatusText(ByVal Status As SoftwareValidationStatusLKT) As String
    Select Case Status
      Case SoftwareValidationStatusLKT.ANSWERED
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4838)
      Case SoftwareValidationStatusLKT.ANSWERED_FROM_EGM
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4837)
      Case SoftwareValidationStatusLKT.PENDING
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4819)
      Case SoftwareValidationStatusLKT.REQUESTED_TO_EGM
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4832)
      Case SoftwareValidationStatusLKT.REQUESTED_TO_EGM_ACK
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4836)
      Case SoftwareValidationStatusLKT.REQUESTED_TO_EGM_NACK
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4835)
      Case SoftwareValidationStatusLKT.TIMEOUT
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4831)
      Case SoftwareValidationStatusLKT.UNKNOWN
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4824)
      Case Else
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(4824)
    End Select
  End Function

  ' PURPOSE: Set Grid.Cell values based on Command parameter. Also "paint" the row based on Command.status.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Grid As uc_grid
  '           - RowIndex As Integer
  '           - Command As TYPE_COMMAND_ITEM
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS: True (the row should be added) or False (the row can not be added)

  Public Function SoftwareValidationStatus_SetupRow(ByVal Grid As uc_grid, ByVal RowIndex As Integer, ByVal SoftwareValidation As TYPE_SV_ITEM) As Boolean

    Dim fore_color As Drawing.Color
    Dim back_color As Drawing.Color
    Dim _print_str As String


    SoftwareValidationStatus_GetUpdateStatusColor(SoftwareValidation.status, fore_color, back_color)

    ' Command ID
    Grid.Cell(RowIndex, GRID_COLUMN_INDEX).Value = SoftwareValidation.id
    Grid.Cell(RowIndex, GRID_COLUMN_INDEX).BackColor = back_color
    Grid.Cell(RowIndex, GRID_COLUMN_INDEX).ForeColor = back_color

    'Insert datetime
    Grid.Cell(RowIndex, GRID_COLUMN_INSERTED_DATETIME).Value = GUI_FormatDate(SoftwareValidation.inserted_datetime, _
                                                                              ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                              ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    'Last request datetime
    If SoftwareValidation.last_request <> DateTime.MinValue Then
      _print_str = GUI_FormatDate(SoftwareValidation.last_request, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Else
      _print_str = ""
    End If
    Grid.Cell(RowIndex, GRID_COLUMN_LAST_REQUEST_DATETIME).Value = _print_str

    'Received datetime
    If SoftwareValidation.received <> DateTime.MinValue Then
      _print_str = GUI_FormatDate(SoftwareValidation.received, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Else
      _print_str = ""
    End If
    Grid.Cell(RowIndex, GRID_COLUMN_RECEIVED_DATETIME).Value = _print_str

    ' Provider
    Grid.Cell(RowIndex, GRID_COLUMN_PROVIDER).Value = SoftwareValidation.provider_name

    ' Terminal
    Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL).Value = SoftwareValidation.terminal_name

    ' status
    Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = SoftwareValidationStatus_StatusText(SoftwareValidation.status)
    Grid.Cell(RowIndex, GRID_COLUMN_STATUS).BackColor = back_color
    Grid.Cell(RowIndex, GRID_COLUMN_STATUS).ForeColor = fore_color

    ' Status Code
    Grid.Cell(RowIndex, GRID_COLUMN_STATUS_CODE).Value = SoftwareValidation.status

    ' LKT status
    Grid.Cell(RowIndex, GRID_COLUMN_LKT_STATUS).Value = SoftwareValidation.sas_host_error

    ' Expected Signaure
    Grid.Cell(RowIndex, GRID_COLUMN_EXPECTED_SIGNATURE).Value = SoftwareValidation.expected_signature

    Return True
  End Function ' SoftwareValidationStatus_SetupRow

  Public Function SoftwareValidationStatus_GetToolTipText(ByRef Grid As uc_grid, ByVal RowIndex As Integer, ByVal ColumnIndex As Integer) As String
    Dim _txt As String

    If ShowDefaultToolTipText(Grid, RowIndex, ColumnIndex) Then
      _txt = Grid.Cell(RowIndex, ColumnIndex).Value.ToString()
    Else
      _txt = SoftwareValidationStatus_SasHostStatusText(Grid.Cell(RowIndex, GRID_COLUMN_LKT_STATUS).Value)
    End If

    Return _txt
  End Function

#End Region ' Public Functions

#Region " Private Functions "

  Private Function ShowDefaultToolTipText(ByRef Grid As uc_grid, ByVal RowIndex As Integer, ByVal ColumnIndex As Integer) As Boolean
    Dim _sv_status As SoftwareValidationStatus
    Dim _lkt_status As SoftwareValidationStatusLKT
    Dim _must_default_tool_tip_be_shown As Boolean

    _sv_status = Grid.Cell(RowIndex, GRID_COLUMN_STATUS_CODE).Value
    _lkt_status = Grid.Cell(RowIndex, GRID_COLUMN_LKT_STATUS).Value

    _must_default_tool_tip_be_shown = True
    If ColumnIndex = GRID_COLUMN_STATUS Then
      _must_default_tool_tip_be_shown = False
      If _lkt_status = SoftwareValidationStatusLKT.UNKNOWN And _
         (_sv_status = SoftwareValidationStatus.Pending Or _
          _sv_status = SoftwareValidationStatus.Notified Or _
          _sv_status = SoftwareValidationStatus.CanceledTimeout) Then

        _must_default_tool_tip_be_shown = True
      End If
    End If

    Return _must_default_tool_tip_be_shown

  End Function
#End Region ' Private Functions


End Module
