<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_identity_data
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gp_identity = New System.Windows.Forms.GroupBox()
    Me.ef_identity_last_name2 = New GUI_Controls.uc_entry_field()
    Me.ef_identity_last_name1 = New GUI_Controls.uc_entry_field()
    Me.ef_identity_id_1 = New GUI_Controls.uc_entry_field()
    Me.ef_identity_name = New GUI_Controls.uc_entry_field()
    Me.ef_identity_id_2 = New GUI_Controls.uc_entry_field()
    Me.flp_main = New System.Windows.Forms.FlowLayoutPanel()
    Me.pnl_parameters_withholding = New System.Windows.Forms.Panel()
    Me.lbl_generated = New System.Windows.Forms.Label()
    Me.uc_cmb_evidence_type = New GUI_Controls.uc_combo()
    Me.ef_editable_minutes = New GUI_Controls.uc_entry_field()
    Me.chk_enabled = New System.Windows.Forms.CheckBox()
    Me.ef_on_prize_grather_than = New GUI_Controls.uc_entry_field()
    Me.gp_company = New System.Windows.Forms.GroupBox()
    Me.ef_company_id_1 = New GUI_Controls.uc_entry_field()
    Me.ef_company_name = New GUI_Controls.uc_entry_field()
    Me.ef_company_id_2 = New GUI_Controls.uc_entry_field()
    Me.gp_representative = New System.Windows.Forms.GroupBox()
    Me.pnl_sign = New System.Windows.Forms.Panel()
    Me.btn_scale_down = New GUI_Controls.uc_button()
    Me.btn_scale_up = New GUI_Controls.uc_button()
    Me.btn_select_img = New GUI_Controls.uc_button()
    Me.lbl_signature = New System.Windows.Forms.Label()
    Me.pnl_signature = New System.Windows.Forms.Panel()
    Me.ef_representative_id1 = New GUI_Controls.uc_entry_field()
    Me.ef_representative_id2 = New GUI_Controls.uc_entry_field()
    Me.ef_representative_name = New GUI_Controls.uc_entry_field()
    Me.gp_identity.SuspendLayout()
    Me.flp_main.SuspendLayout()
    Me.pnl_parameters_withholding.SuspendLayout()
    Me.gp_company.SuspendLayout()
    Me.gp_representative.SuspendLayout()
    Me.pnl_sign.SuspendLayout()
    Me.SuspendLayout()
    '
    'gp_identity
    '
    Me.gp_identity.Controls.Add(Me.ef_identity_last_name2)
    Me.gp_identity.Controls.Add(Me.ef_identity_last_name1)
    Me.gp_identity.Controls.Add(Me.ef_identity_id_1)
    Me.gp_identity.Controls.Add(Me.ef_identity_name)
    Me.gp_identity.Controls.Add(Me.ef_identity_id_2)
    Me.gp_identity.Location = New System.Drawing.Point(0, 124)
    Me.gp_identity.Margin = New System.Windows.Forms.Padding(0, 0, 0, 5)
    Me.gp_identity.Name = "gp_identity"
    Me.gp_identity.Size = New System.Drawing.Size(876, 163)
    Me.gp_identity.TabIndex = 4
    Me.gp_identity.TabStop = False
    Me.gp_identity.Text = "xIdentity"
    '
    'ef_identity_last_name2
    '
    Me.ef_identity_last_name2.DoubleValue = 0.0R
    Me.ef_identity_last_name2.IntegerValue = 0
    Me.ef_identity_last_name2.IsReadOnly = True
    Me.ef_identity_last_name2.Location = New System.Drawing.Point(15, 103)
    Me.ef_identity_last_name2.Name = "ef_identity_last_name2"
    Me.ef_identity_last_name2.OnlyUpperCase = True
    Me.ef_identity_last_name2.PlaceHolder = Nothing
    Me.ef_identity_last_name2.Size = New System.Drawing.Size(435, 24)
    Me.ef_identity_last_name2.SufixText = "Sufix Text"
    Me.ef_identity_last_name2.SufixTextVisible = True
    Me.ef_identity_last_name2.TabIndex = 3
    Me.ef_identity_last_name2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_identity_last_name2.TextValue = ""
    Me.ef_identity_last_name2.TextWidth = 130
    Me.ef_identity_last_name2.Value = ""
    Me.ef_identity_last_name2.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_identity_last_name1
    '
    Me.ef_identity_last_name1.DoubleValue = 0.0R
    Me.ef_identity_last_name1.IntegerValue = 0
    Me.ef_identity_last_name1.IsReadOnly = True
    Me.ef_identity_last_name1.Location = New System.Drawing.Point(15, 73)
    Me.ef_identity_last_name1.Name = "ef_identity_last_name1"
    Me.ef_identity_last_name1.OnlyUpperCase = True
    Me.ef_identity_last_name1.PlaceHolder = Nothing
    Me.ef_identity_last_name1.Size = New System.Drawing.Size(435, 24)
    Me.ef_identity_last_name1.SufixText = "Sufix Text"
    Me.ef_identity_last_name1.SufixTextVisible = True
    Me.ef_identity_last_name1.TabIndex = 2
    Me.ef_identity_last_name1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_identity_last_name1.TextValue = ""
    Me.ef_identity_last_name1.TextWidth = 130
    Me.ef_identity_last_name1.Value = ""
    Me.ef_identity_last_name1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_identity_id_1
    '
    Me.ef_identity_id_1.DoubleValue = 0.0R
    Me.ef_identity_id_1.IntegerValue = 0
    Me.ef_identity_id_1.IsReadOnly = True
    Me.ef_identity_id_1.Location = New System.Drawing.Point(17, 18)
    Me.ef_identity_id_1.Name = "ef_identity_id_1"
    Me.ef_identity_id_1.OnlyUpperCase = True
    Me.ef_identity_id_1.PlaceHolder = Nothing
    Me.ef_identity_id_1.Size = New System.Drawing.Size(435, 24)
    Me.ef_identity_id_1.SufixText = "Sufix Text"
    Me.ef_identity_id_1.SufixTextVisible = True
    Me.ef_identity_id_1.TabIndex = 0
    Me.ef_identity_id_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_identity_id_1.TextValue = ""
    Me.ef_identity_id_1.TextWidth = 128
    Me.ef_identity_id_1.Value = ""
    Me.ef_identity_id_1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_identity_name
    '
    Me.ef_identity_name.DoubleValue = 0.0R
    Me.ef_identity_name.IntegerValue = 0
    Me.ef_identity_name.IsReadOnly = True
    Me.ef_identity_name.Location = New System.Drawing.Point(17, 133)
    Me.ef_identity_name.Name = "ef_identity_name"
    Me.ef_identity_name.OnlyUpperCase = True
    Me.ef_identity_name.PlaceHolder = Nothing
    Me.ef_identity_name.Size = New System.Drawing.Size(435, 24)
    Me.ef_identity_name.SufixText = "Sufix Text"
    Me.ef_identity_name.SufixTextVisible = True
    Me.ef_identity_name.TabIndex = 4
    Me.ef_identity_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_identity_name.TextValue = ""
    Me.ef_identity_name.TextWidth = 128
    Me.ef_identity_name.Value = ""
    Me.ef_identity_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_identity_id_2
    '
    Me.ef_identity_id_2.DoubleValue = 0.0R
    Me.ef_identity_id_2.IntegerValue = 0
    Me.ef_identity_id_2.IsReadOnly = True
    Me.ef_identity_id_2.Location = New System.Drawing.Point(17, 45)
    Me.ef_identity_id_2.Name = "ef_identity_id_2"
    Me.ef_identity_id_2.OnlyUpperCase = True
    Me.ef_identity_id_2.PlaceHolder = Nothing
    Me.ef_identity_id_2.Size = New System.Drawing.Size(435, 24)
    Me.ef_identity_id_2.SufixText = "Sufix Text"
    Me.ef_identity_id_2.SufixTextVisible = True
    Me.ef_identity_id_2.TabIndex = 1
    Me.ef_identity_id_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_identity_id_2.TextValue = ""
    Me.ef_identity_id_2.TextWidth = 128
    Me.ef_identity_id_2.Value = ""
    Me.ef_identity_id_2.ValueForeColor = System.Drawing.Color.Blue
    '
    'flp_main
    '
    Me.flp_main.AutoSize = True
    Me.flp_main.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.flp_main.Controls.Add(Me.pnl_parameters_withholding)
    Me.flp_main.Controls.Add(Me.gp_identity)
    Me.flp_main.Controls.Add(Me.gp_company)
    Me.flp_main.Controls.Add(Me.gp_representative)
    Me.flp_main.Dock = System.Windows.Forms.DockStyle.Fill
    Me.flp_main.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
    Me.flp_main.Location = New System.Drawing.Point(0, 0)
    Me.flp_main.Margin = New System.Windows.Forms.Padding(0)
    Me.flp_main.Name = "flp_main"
    Me.flp_main.Size = New System.Drawing.Size(876, 582)
    Me.flp_main.TabIndex = 13
    '
    'pnl_parameters_withholding
    '
    Me.pnl_parameters_withholding.Controls.Add(Me.lbl_generated)
    Me.pnl_parameters_withholding.Controls.Add(Me.uc_cmb_evidence_type)
    Me.pnl_parameters_withholding.Controls.Add(Me.ef_editable_minutes)
    Me.pnl_parameters_withholding.Controls.Add(Me.chk_enabled)
    Me.pnl_parameters_withholding.Controls.Add(Me.ef_on_prize_grather_than)
    Me.pnl_parameters_withholding.Location = New System.Drawing.Point(0, 0)
    Me.pnl_parameters_withholding.Margin = New System.Windows.Forms.Padding(0, 0, 0, 5)
    Me.pnl_parameters_withholding.Name = "pnl_parameters_withholding"
    Me.pnl_parameters_withholding.Size = New System.Drawing.Size(876, 119)
    Me.pnl_parameters_withholding.TabIndex = 1
    '
    'lbl_generated
    '
    Me.lbl_generated.AutoSize = True
    Me.lbl_generated.Location = New System.Drawing.Point(124, 100)
    Me.lbl_generated.Name = "lbl_generated"
    Me.lbl_generated.Size = New System.Drawing.Size(67, 13)
    Me.lbl_generated.TabIndex = 12
    Me.lbl_generated.Text = "xGenerate"
    '
    'uc_cmb_evidence_type
    '
    Me.uc_cmb_evidence_type.AllowUnlistedValues = False
    Me.uc_cmb_evidence_type.AutoCompleteMode = False
    Me.uc_cmb_evidence_type.IsReadOnly = False
    Me.uc_cmb_evidence_type.Location = New System.Drawing.Point(402, 89)
    Me.uc_cmb_evidence_type.Name = "uc_cmb_evidence_type"
    Me.uc_cmb_evidence_type.SelectedIndex = -1
    Me.uc_cmb_evidence_type.Size = New System.Drawing.Size(242, 24)
    Me.uc_cmb_evidence_type.SufixText = "Sufix Text"
    Me.uc_cmb_evidence_type.SufixTextVisible = True
    Me.uc_cmb_evidence_type.TabIndex = 11
    Me.uc_cmb_evidence_type.TextCombo = Nothing
    Me.uc_cmb_evidence_type.TextVisible = False
    Me.uc_cmb_evidence_type.TextWidth = 0
    '
    'ef_editable_minutes
    '
    Me.ef_editable_minutes.DoubleValue = 0.0R
    Me.ef_editable_minutes.IntegerValue = 0
    Me.ef_editable_minutes.IsReadOnly = True
    Me.ef_editable_minutes.Location = New System.Drawing.Point(150, 31)
    Me.ef_editable_minutes.Name = "ef_editable_minutes"
    Me.ef_editable_minutes.PlaceHolder = Nothing
    Me.ef_editable_minutes.Size = New System.Drawing.Size(379, 24)
    Me.ef_editable_minutes.SufixText = "min."
    Me.ef_editable_minutes.SufixTextVisible = True
    Me.ef_editable_minutes.SufixTextWidth = 32
    Me.ef_editable_minutes.TabIndex = 2
    Me.ef_editable_minutes.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_editable_minutes.TextValue = ""
    Me.ef_editable_minutes.TextWidth = 142
    Me.ef_editable_minutes.Value = ""
    Me.ef_editable_minutes.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(127, 9)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(78, 17)
    Me.chk_enabled.TabIndex = 1
    Me.chk_enabled.Text = "xEnabled"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'ef_on_prize_grather_than
    '
    Me.ef_on_prize_grather_than.DoubleValue = 0.0R
    Me.ef_on_prize_grather_than.IntegerValue = 0
    Me.ef_on_prize_grather_than.IsReadOnly = True
    Me.ef_on_prize_grather_than.Location = New System.Drawing.Point(150, 59)
    Me.ef_on_prize_grather_than.Name = "ef_on_prize_grather_than"
    Me.ef_on_prize_grather_than.PlaceHolder = Nothing
    Me.ef_on_prize_grather_than.Size = New System.Drawing.Size(379, 24)
    Me.ef_on_prize_grather_than.SufixText = "Sufix Text"
    Me.ef_on_prize_grather_than.SufixTextVisible = True
    Me.ef_on_prize_grather_than.TabIndex = 3
    Me.ef_on_prize_grather_than.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_on_prize_grather_than.TextValue = ""
    Me.ef_on_prize_grather_than.TextWidth = 142
    Me.ef_on_prize_grather_than.Value = ""
    Me.ef_on_prize_grather_than.ValueForeColor = System.Drawing.Color.Blue
    '
    'gp_company
    '
    Me.gp_company.Controls.Add(Me.ef_company_id_1)
    Me.gp_company.Controls.Add(Me.ef_company_name)
    Me.gp_company.Controls.Add(Me.ef_company_id_2)
    Me.gp_company.Location = New System.Drawing.Point(0, 292)
    Me.gp_company.Margin = New System.Windows.Forms.Padding(0, 0, 0, 5)
    Me.gp_company.Name = "gp_company"
    Me.gp_company.Size = New System.Drawing.Size(876, 104)
    Me.gp_company.TabIndex = 8
    Me.gp_company.TabStop = False
    Me.gp_company.Text = "xCompany"
    '
    'ef_company_id_1
    '
    Me.ef_company_id_1.DoubleValue = 0.0R
    Me.ef_company_id_1.IntegerValue = 0
    Me.ef_company_id_1.IsReadOnly = True
    Me.ef_company_id_1.Location = New System.Drawing.Point(17, 18)
    Me.ef_company_id_1.Name = "ef_company_id_1"
    Me.ef_company_id_1.OnlyUpperCase = True
    Me.ef_company_id_1.PlaceHolder = Nothing
    Me.ef_company_id_1.Size = New System.Drawing.Size(435, 24)
    Me.ef_company_id_1.SufixText = "Sufix Text"
    Me.ef_company_id_1.SufixTextVisible = True
    Me.ef_company_id_1.TabIndex = 5
    Me.ef_company_id_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_company_id_1.TextValue = ""
    Me.ef_company_id_1.TextWidth = 128
    Me.ef_company_id_1.Value = ""
    Me.ef_company_id_1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_company_name
    '
    Me.ef_company_name.DoubleValue = 0.0R
    Me.ef_company_name.IntegerValue = 0
    Me.ef_company_name.IsReadOnly = True
    Me.ef_company_name.Location = New System.Drawing.Point(17, 73)
    Me.ef_company_name.Name = "ef_company_name"
    Me.ef_company_name.OnlyUpperCase = True
    Me.ef_company_name.PlaceHolder = Nothing
    Me.ef_company_name.Size = New System.Drawing.Size(700, 25)
    Me.ef_company_name.SufixText = "Sufix Text"
    Me.ef_company_name.SufixTextVisible = True
    Me.ef_company_name.TabIndex = 7
    Me.ef_company_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_company_name.TextValue = ""
    Me.ef_company_name.TextWidth = 128
    Me.ef_company_name.Value = ""
    Me.ef_company_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_company_id_2
    '
    Me.ef_company_id_2.DoubleValue = 0.0R
    Me.ef_company_id_2.IntegerValue = 0
    Me.ef_company_id_2.IsReadOnly = True
    Me.ef_company_id_2.Location = New System.Drawing.Point(17, 45)
    Me.ef_company_id_2.Name = "ef_company_id_2"
    Me.ef_company_id_2.OnlyUpperCase = True
    Me.ef_company_id_2.PlaceHolder = Nothing
    Me.ef_company_id_2.Size = New System.Drawing.Size(435, 24)
    Me.ef_company_id_2.SufixText = "Sufix Text"
    Me.ef_company_id_2.SufixTextVisible = True
    Me.ef_company_id_2.TabIndex = 6
    Me.ef_company_id_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_company_id_2.TextValue = ""
    Me.ef_company_id_2.TextWidth = 128
    Me.ef_company_id_2.Value = ""
    Me.ef_company_id_2.ValueForeColor = System.Drawing.Color.Blue
    '
    'gp_representative
    '
    Me.gp_representative.Controls.Add(Me.pnl_sign)
    Me.gp_representative.Controls.Add(Me.ef_representative_id1)
    Me.gp_representative.Controls.Add(Me.ef_representative_id2)
    Me.gp_representative.Controls.Add(Me.ef_representative_name)
    Me.gp_representative.Location = New System.Drawing.Point(0, 401)
    Me.gp_representative.Margin = New System.Windows.Forms.Padding(0)
    Me.gp_representative.MinimumSize = New System.Drawing.Size(0, 104)
    Me.gp_representative.Name = "gp_representative"
    Me.gp_representative.Size = New System.Drawing.Size(876, 181)
    Me.gp_representative.TabIndex = 8
    Me.gp_representative.TabStop = False
    Me.gp_representative.Text = "xRepresentative"
    '
    'pnl_sign
    '
    Me.pnl_sign.Controls.Add(Me.btn_scale_down)
    Me.pnl_sign.Controls.Add(Me.btn_scale_up)
    Me.pnl_sign.Controls.Add(Me.btn_select_img)
    Me.pnl_sign.Controls.Add(Me.lbl_signature)
    Me.pnl_sign.Controls.Add(Me.pnl_signature)
    Me.pnl_sign.Location = New System.Drawing.Point(458, 14)
    Me.pnl_sign.Name = "pnl_sign"
    Me.pnl_sign.Size = New System.Drawing.Size(405, 160)
    Me.pnl_sign.TabIndex = 15
    '
    'btn_scale_down
    '
    Me.btn_scale_down.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_scale_down.Location = New System.Drawing.Point(311, 56)
    Me.btn_scale_down.Name = "btn_scale_down"
    Me.btn_scale_down.Size = New System.Drawing.Size(90, 30)
    Me.btn_scale_down.TabIndex = 19
    Me.btn_scale_down.ToolTipped = False
    Me.btn_scale_down.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_scale_up
    '
    Me.btn_scale_up.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_scale_up.Location = New System.Drawing.Point(311, 22)
    Me.btn_scale_up.Name = "btn_scale_up"
    Me.btn_scale_up.Size = New System.Drawing.Size(90, 30)
    Me.btn_scale_up.TabIndex = 18
    Me.btn_scale_up.ToolTipped = False
    Me.btn_scale_up.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_select_img
    '
    Me.btn_select_img.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_select_img.Location = New System.Drawing.Point(1, 113)
    Me.btn_select_img.Name = "btn_select_img"
    Me.btn_select_img.Size = New System.Drawing.Size(90, 30)
    Me.btn_select_img.TabIndex = 16
    Me.btn_select_img.ToolTipped = False
    Me.btn_select_img.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'lbl_signature
    '
    Me.lbl_signature.AutoSize = True
    Me.lbl_signature.Location = New System.Drawing.Point(96, 4)
    Me.lbl_signature.Name = "lbl_signature"
    Me.lbl_signature.Size = New System.Drawing.Size(69, 13)
    Me.lbl_signature.TabIndex = 15
    Me.lbl_signature.Text = "xSignature"
    '
    'pnl_signature
    '
    Me.pnl_signature.BackColor = System.Drawing.Color.White
    Me.pnl_signature.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
    Me.pnl_signature.Cursor = System.Windows.Forms.Cursors.Default
    Me.pnl_signature.Location = New System.Drawing.Point(97, 22)
    Me.pnl_signature.Name = "pnl_signature"
    Me.pnl_signature.Size = New System.Drawing.Size(210, 126)
    Me.pnl_signature.TabIndex = 17
    '
    'ef_representative_id1
    '
    Me.ef_representative_id1.DoubleValue = 0.0R
    Me.ef_representative_id1.IntegerValue = 0
    Me.ef_representative_id1.IsReadOnly = True
    Me.ef_representative_id1.Location = New System.Drawing.Point(17, 36)
    Me.ef_representative_id1.Name = "ef_representative_id1"
    Me.ef_representative_id1.OnlyUpperCase = True
    Me.ef_representative_id1.PlaceHolder = Nothing
    Me.ef_representative_id1.Size = New System.Drawing.Size(435, 24)
    Me.ef_representative_id1.SufixText = "Sufix Text"
    Me.ef_representative_id1.SufixTextVisible = True
    Me.ef_representative_id1.TabIndex = 9
    Me.ef_representative_id1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_representative_id1.TextValue = ""
    Me.ef_representative_id1.TextWidth = 128
    Me.ef_representative_id1.Value = ""
    Me.ef_representative_id1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_representative_id2
    '
    Me.ef_representative_id2.DoubleValue = 0.0R
    Me.ef_representative_id2.IntegerValue = 0
    Me.ef_representative_id2.IsReadOnly = True
    Me.ef_representative_id2.Location = New System.Drawing.Point(17, 62)
    Me.ef_representative_id2.Name = "ef_representative_id2"
    Me.ef_representative_id2.OnlyUpperCase = True
    Me.ef_representative_id2.PlaceHolder = Nothing
    Me.ef_representative_id2.Size = New System.Drawing.Size(435, 24)
    Me.ef_representative_id2.SufixText = "Sufix Text"
    Me.ef_representative_id2.SufixTextVisible = True
    Me.ef_representative_id2.TabIndex = 10
    Me.ef_representative_id2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_representative_id2.TextValue = ""
    Me.ef_representative_id2.TextWidth = 128
    Me.ef_representative_id2.Value = ""
    Me.ef_representative_id2.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_representative_name
    '
    Me.ef_representative_name.DoubleValue = 0.0R
    Me.ef_representative_name.IntegerValue = 0
    Me.ef_representative_name.IsReadOnly = True
    Me.ef_representative_name.Location = New System.Drawing.Point(17, 90)
    Me.ef_representative_name.Name = "ef_representative_name"
    Me.ef_representative_name.OnlyUpperCase = True
    Me.ef_representative_name.PlaceHolder = Nothing
    Me.ef_representative_name.Size = New System.Drawing.Size(435, 24)
    Me.ef_representative_name.SufixText = "Sufix Text"
    Me.ef_representative_name.SufixTextVisible = True
    Me.ef_representative_name.TabIndex = 11
    Me.ef_representative_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_representative_name.TextValue = ""
    Me.ef_representative_name.TextWidth = 128
    Me.ef_representative_name.Value = ""
    Me.ef_representative_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_identity_data
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.AutoSize = True
    Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.Controls.Add(Me.flp_main)
    Me.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.Margin = New System.Windows.Forms.Padding(0)
    Me.Name = "uc_identity_data"
    Me.Size = New System.Drawing.Size(876, 582)
    Me.gp_identity.ResumeLayout(False)
    Me.flp_main.ResumeLayout(False)
    Me.pnl_parameters_withholding.ResumeLayout(False)
    Me.pnl_parameters_withholding.PerformLayout()
    Me.gp_company.ResumeLayout(False)
    Me.gp_representative.ResumeLayout(False)
    Me.pnl_sign.ResumeLayout(False)
    Me.pnl_sign.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents ef_identity_id_1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_identity_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_identity_id_2 As GUI_Controls.uc_entry_field
  Friend WithEvents gp_identity As System.Windows.Forms.GroupBox
  Friend WithEvents flp_main As System.Windows.Forms.FlowLayoutPanel
  Friend WithEvents pnl_parameters_withholding As System.Windows.Forms.Panel
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents ef_on_prize_grather_than As GUI_Controls.uc_entry_field
  Friend WithEvents ef_editable_minutes As GUI_Controls.uc_entry_field
  Friend WithEvents gp_representative As System.Windows.Forms.GroupBox
  Friend WithEvents ef_representative_id1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_representative_id2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_representative_name As GUI_Controls.uc_entry_field
  Friend WithEvents gp_company As System.Windows.Forms.GroupBox
  Friend WithEvents ef_company_id_1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_company_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_company_id_2 As GUI_Controls.uc_entry_field
  Friend WithEvents pnl_sign As System.Windows.Forms.Panel
  Friend WithEvents btn_scale_down As GUI_Controls.uc_button
  Friend WithEvents btn_scale_up As GUI_Controls.uc_button
  Friend WithEvents btn_select_img As GUI_Controls.uc_button
  Friend WithEvents lbl_signature As System.Windows.Forms.Label
  Friend WithEvents pnl_signature As System.Windows.Forms.Panel
  Friend WithEvents ef_identity_last_name2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_identity_last_name1 As GUI_Controls.uc_entry_field
  Friend WithEvents uc_cmb_evidence_type As GUI_Controls.uc_combo
  Friend WithEvents lbl_generated As System.Windows.Forms.Label

End Class
