<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_frame_viewer
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_frame = New System.Windows.Forms.GroupBox
    Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
    Me.cb_hexa = New System.Windows.Forms.CheckBox
    Me.cb_binary = New System.Windows.Forms.CheckBox
    Me.lbl_frame = New System.Windows.Forms.Label
    Me.gb_frame.SuspendLayout()
    Me.SplitContainer1.Panel1.SuspendLayout()
    Me.SplitContainer1.Panel2.SuspendLayout()
    Me.SplitContainer1.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_frame
    '
    Me.gb_frame.Controls.Add(Me.cb_hexa)
    Me.gb_frame.Controls.Add(Me.SplitContainer1)
    Me.gb_frame.Dock = System.Windows.Forms.DockStyle.Fill
    Me.gb_frame.Location = New System.Drawing.Point(0, 0)
    Me.gb_frame.Name = "gb_frame"
    Me.gb_frame.Size = New System.Drawing.Size(682, 233)
    Me.gb_frame.TabIndex = 0
    Me.gb_frame.TabStop = False
    Me.gb_frame.Text = "xFrame"
    '
    'SplitContainer1
    '
    Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
    Me.SplitContainer1.IsSplitterFixed = True
    Me.SplitContainer1.Location = New System.Drawing.Point(3, 16)
    Me.SplitContainer1.Name = "SplitContainer1"
    Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
    '
    'SplitContainer1.Panel1
    '
    Me.SplitContainer1.Panel1.Controls.Add(Me.cb_binary)
    Me.SplitContainer1.Panel1MinSize = 20
    '
    'SplitContainer1.Panel2
    '
    Me.SplitContainer1.Panel2.AutoScroll = True
    Me.SplitContainer1.Panel2.Controls.Add(Me.lbl_frame)
    Me.SplitContainer1.Panel2MinSize = 20
    Me.SplitContainer1.Size = New System.Drawing.Size(676, 214)
    Me.SplitContainer1.SplitterDistance = 20
    Me.SplitContainer1.TabIndex = 1
    '
    'cb_hexa
    '
    Me.cb_hexa.AutoSize = True
    Me.cb_hexa.Location = New System.Drawing.Point(6, 16)
    Me.cb_hexa.Name = "cb_hexa"
    Me.cb_hexa.Size = New System.Drawing.Size(56, 17)
    Me.cb_hexa.TabIndex = 0
    Me.cb_hexa.Text = "xHexa"
    Me.cb_hexa.UseVisualStyleBackColor = True
    '
    'cb_binary
    '
    Me.cb_binary.AutoSize = True
    Me.cb_binary.Location = New System.Drawing.Point(110, 0)
    Me.cb_binary.Name = "cb_binary"
    Me.cb_binary.Size = New System.Drawing.Size(60, 17)
    Me.cb_binary.TabIndex = 1
    Me.cb_binary.Text = "xBinary"
    Me.cb_binary.UseVisualStyleBackColor = True
    '
    'lbl_frame
    '
    Me.lbl_frame.AutoSize = True
    Me.lbl_frame.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_frame.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_frame.Location = New System.Drawing.Point(3, 1)
    Me.lbl_frame.Name = "lbl_frame"
    Me.lbl_frame.Size = New System.Drawing.Size(84, 14)
    Me.lbl_frame.TabIndex = 0
    Me.lbl_frame.Text = "xLabelFrame"
    '
    'uc_frame_viewer
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.gb_frame)
    Me.Name = "uc_frame_viewer"
    Me.Size = New System.Drawing.Size(682, 233)
    Me.gb_frame.ResumeLayout(False)
    Me.gb_frame.PerformLayout()
    Me.SplitContainer1.Panel1.ResumeLayout(False)
    Me.SplitContainer1.Panel1.PerformLayout()
    Me.SplitContainer1.Panel2.ResumeLayout(False)
    Me.SplitContainer1.Panel2.PerformLayout()
    Me.SplitContainer1.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_frame As System.Windows.Forms.GroupBox
  Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
  Friend WithEvents cb_hexa As System.Windows.Forms.CheckBox
  Friend WithEvents cb_binary As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_frame As System.Windows.Forms.Label

End Class
