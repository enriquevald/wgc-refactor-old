'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_terminals_group_filter
' DESCRIPTION:   Tree to select and generate a list of elements that contain terminals.
' AUTHOR:        Rub�n Brea
' CREATION DATE: 30-JUL-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 30-JUL-2013  RBG    Initial version
' 14-OCT-2013  JBC    Added tooltip that shows Element List
' 27-MAR-2014  MPO    Performance improved for terminal generations
' 04-DEC-2014  DHA    Added personalization groups
' 18-DEC-2014  JML    Fixed bug WIG-1864:  LCD messages edit: No show terminals in multisite center
' 16-JUN-2015  DHA    Fixed bug WIG-2450
' 14-JUL-2015  SGB    Added property "ShowNodeAll". Is posible show group "All"
' 15-JUL-2015  SGB    Added function "GetTerminalIdSelected". Return in TerminalList only in terminals
' 15-JUL-2015  SGB    Added function "CheckAllElements". Check or uncheck all elements.
' 17-AUG-2015  YNM    Fixed Bug TFS-3580: added Terminal_id to m_elements from uc_terminals_group_filter
' 22-FEB-2017  ATB    Fixed some misfunctionality with booking terminal config
'--------------------------------------------------------------------

Imports System.Windows.Forms
Imports System.Text
Imports System.Drawing
Imports System.Data.SqlClient
Imports WSI.Common
Imports WSI.Common.Groups
Imports System.ComponentModel

Public Class uc_terminals_group_filter

#Region "Constants"
  Private Const DT_COLUMN_ID As String = "ID"
  Private Const DT_COLUMN_NAME As String = "NAME"
  Private Const DT_COLUMN_TYPE As String = "TYPE"
  Private Const DT_COLUMN_PROV As String = "PROV"
  Private Const DT_COLUMN_ZONE As String = "ZONE"
  Private Const DT_COLUMN_AREA As String = "AREA"
  Private Const DT_COLUMN_BANK As String = "BANK"
  Private Const DT_COLUMN_TERMINAL_ID As String = "TERMINAL_ID"
  Private Const MAX_ELEMENTS_TO_SHOW_IN_TOOL_TIP As Integer = 10
#End Region

#Region "Enums"

  Public Enum InitMode
    NormalMode = 0
    OnlyListed = 1
    OnlyNonListed = 2
  End Enum

  Public Enum DisplayMode
    NotCollapsedWithoutCollapseButton = 1
    NotCollapsedWithoutButtons = 2
    CollapsedWithButtons = 3
  End Enum

#End Region

#Region "Members"

  Private Const DEFAULT_FORM_WIDTH As Integer = 340
  Private Const DEFAULT_FORM_HEIGHT As Integer = 46

  Private STR_TO_COLLAPSE As String = System.Text.Encoding.Unicode.GetString(New Byte() {&HC4, &H2})
  Private STR_TO_EXPAND As String = System.Text.Encoding.Unicode.GetString(New Byte() {&HC5, &H2})

  Private m_allow_collapse As Boolean = False
  Private m_collapsed As Boolean = False
  Private m_three_state_check_mode As Boolean = False
  Private m_height_on_expanded = 300
  Private m_show_node_all As Boolean = True

  Private m_terminal_list As TerminalList

  Private m_expand_flag As String = ""            ' 2 Strings to control expand tree view nodes on user's click and doubleclick
  Private m_expand_manual_flag As String = ""

  Private m_dt_elements As DataTable
  Private m_first_expand As Boolean = False
  Private m_previous_height As Int16                ' When we collapse, saves the previous height control
  Private m_forbidden_groups As Collection          ' Groups that can't be showed

  Private m_cursor_point As Point                   ' Allows to save the Location of pointer
  Private m_default_tlp_position As Point           ' Initial position of tableLayout
  Private m_default_gb_provider_position As Point   ' Initial position of tableLayout
  Private m_default_cmb_provider_position As Point  ' Initial position of tableLayout
  Private m_default_tr_grouplist_position As Point  ' Initial position of tableLayout
  Private m_default_btn_collapse_position As Point  ' Initial position of tableLayout

  Private m_default_tlp_size As Size                ' Initial size of TableLayout
  Private m_default_form_size As Size               ' Initial size of the Form
  Private m_default_gb_provider_size As Size        ' Initial position of tableLayout
  Private m_default_cmb_provider_size As Size       ' Initial position of tableLayout
  Private m_default_tr_grouplist_size As Size       ' Initial position of tableLayout
  Private m_default_btn_collapse_size As Size       ' Initial position of tableLayout

  Private m_show_groupbox_line As Boolean           ' Show/Hide GroupBox

  Private m_display_mode As DisplayMode
  Private m_init_mode As InitMode = InitMode.NormalMode

  Private m_cmb_provider_list_type_enabled As Boolean
  Private m_list_enabled As Boolean
  Private m_ignore_click As Boolean

  ' DHA: 04-DEC-2014 
  Private m_show_groups As List(Of WSI.Common.Groups.GROUP_NODE_ELEMENT)

  Public Event TreeNodeDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs)
  Public Event TreeViewEventArgs(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs)

#End Region

#Region " Properties "

  Public Property ThreeStateCheckMode() As Boolean
    Get
      Return Me.m_three_state_check_mode
    End Get
    Set(ByVal Value As Boolean)
      Me.m_three_state_check_mode = Value
      Me.tr_group_list.ShowCheckBoxes = Value
    End Set
  End Property

  Public Property ShowNodeAll() As Boolean
    Get
      Return Me.m_show_node_all
    End Get
    Set(ByVal Value As Boolean)
      Me.m_show_node_all = Value
      tr_group_list.Nodes.Clear()
      Call UpdateDisplayMode()
    End Set
  End Property

  Public ReadOnly Property Tree() As TreeView
    Get
      Return Me.tr_group_list
    End Get

  End Property

  Public Property HeightOnExpanded() As Integer
    Get
      Return m_height_on_expanded
    End Get
    Set(ByVal Value As Integer)
      m_height_on_expanded = Value
    End Set
  End Property

  Public Property ForbiddenGroups() As Collection
    Get
      Return m_forbidden_groups
    End Get
    Set(ByVal Value As Collection)
      m_forbidden_groups = Value
    End Set
  End Property

  <Browsable(False), DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)> _
  Public Property SetGroupBoxText() As String
    Get
      Return Me.gb_provider.Text
    End Get
    Set(ByVal Value As String)
      Me.gb_provider.Text = Value
    End Set
  End Property

  Public WriteOnly Property ShowGroupBoxText() As Boolean
    Set(ByVal Value As Boolean)
      If (Value) Then
        Me.gb_provider.Text = GLB_NLS_GUI_CONTROLS.GetString(477)
      Else
        Me.gb_provider.Text = ""
      End If
    End Set
  End Property

  Public Property ShowGroupBoxLine() As Boolean
    Get
      Return m_show_groupbox_line
    End Get
    Set(ByVal Value As Boolean)
      Me.m_show_groupbox_line = Value
      Me.gb_provider.Visible = Me.m_show_groupbox_line

      If Not (m_show_groupbox_line) Then
        Dim _point As New Point
        Dim _size As New Size

        _point.X = 0
        _point.Y = 0
        _size.Height = Me.Size.Height
        _size.Width = Me.Size.Width

        Me.m_default_tlp_position = Me.tlp_controls.Location
        Me.m_default_tlp_size = Me.tlp_controls.Size
        Me.m_default_form_size = Me.Size
        Me.tlp_controls.Location = _point
        Me.tlp_controls.Size = _size
      Else
        Call RestoreDefaultValues()
      End If

    End Set
  End Property

  Public Property SetDisplayMode() As DisplayMode
    Get
      Return Me.m_display_mode
    End Get
    Set(ByVal value As DisplayMode)
      Me.m_display_mode = value

      Call UpdateDisplayMode()

    End Set
  End Property

  Public Property SetInitMode() As InitMode
    Get
      Return Me.m_init_mode
    End Get
    Set(ByVal value As InitMode)
      Me.m_init_mode = value
      Me.cmb_provider_list_type.Clear()

      Select Case m_init_mode
        Case InitMode.NormalMode
          Call Me.cmb_provider_list_type.Add(TerminalList.TerminalListType.All, GLB_NLS_GUI_CONFIGURATION.GetString(497))         'All
          Call Me.cmb_provider_list_type.Add(TerminalList.TerminalListType.Listed, GLB_NLS_GUI_CONFIGURATION.GetString(499))      'Listed
          Call Me.cmb_provider_list_type.Add(TerminalList.TerminalListType.NotListed, GLB_NLS_GUI_CONFIGURATION.GetString(498))   'Not Listed
          Me.cmb_provider_list_type.SelectedIndex = TerminalList.TerminalListType.All

        Case InitMode.OnlyListed
          Call Me.cmb_provider_list_type.Add(TerminalList.TerminalListType.Listed, GLB_NLS_GUI_CONFIGURATION.GetString(499))      'Listed
          Me.cmb_provider_list_type.Enabled = False

        Case InitMode.OnlyNonListed
          Call Me.cmb_provider_list_type.Add(TerminalList.TerminalListType.NotListed, GLB_NLS_GUI_CONFIGURATION.GetString(498))   'Not Listed
          Me.cmb_provider_list_type.Enabled = False
      End Select

    End Set
  End Property

  Public Property SelectedIndex() As Integer
    Get
      Return Me.cmb_provider_list_type.SelectedIndex
    End Get
    Set(ByVal Value As Integer)
      Me.cmb_provider_list_type.SelectedIndex = Value
    End Set
  End Property

  Public Property ShowGroups() As List(Of WSI.Common.Groups.GROUP_NODE_ELEMENT)
    Get
      Return Me.m_show_groups
    End Get
    Set(ByVal Value As List(Of WSI.Common.Groups.GROUP_NODE_ELEMENT))
      Me.m_show_groups = Value
    End Set
  End Property

  Public Property ListSize() As Size
    Get
      Return m_default_tr_grouplist_size
    End Get
    Set(ByVal value As Size)
      m_default_tr_grouplist_size = value
    End Set
  End Property


  Public Property Ignoreclick() As Boolean
    Get
      Return m_ignore_click
    End Get
    Set(ByVal value As Boolean)
      m_ignore_click = value
    End Set
  End Property


  Public Property ComboProviderListTypeEnabled() As Boolean
    Get
      Return m_cmb_provider_list_type_enabled
    End Get
    Set(ByVal value As Boolean)
      m_cmb_provider_list_type_enabled = value
    End Set
  End Property


  Public Property ListEnabled() As Boolean
    Get
      Return m_list_enabled
    End Get
    Set(ByVal value As Boolean)
      m_list_enabled = value
    End Set
  End Property




#End Region ' Properties

#Region " Public Functions "

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    Call InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControls()

    m_forbidden_groups = New Collection()

  End Sub ' New

  ' PURPOSE: Get the checked provider list that the control shows.
  '
  '  PARAMS:
  '     - INPUT:
  '         - Providers As TerminalList
  '
  '     - OUTPUT:
  '         - Providers As TerminalList
  '
  ' RETURNS:
  '
  Public Sub GetTerminalsFromControl(ByRef Terminals As TerminalList)

    Try

      Terminals.ListType = Me.cmb_provider_list_type.Value

      For Each _dr As DataRow In Me.m_dt_elements.Rows
        Terminals.AddElement(_dr(DT_COLUMN_NAME), _dr(DT_COLUMN_ID), _dr(DT_COLUMN_TYPE))
      Next

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' GetProvidersFromControl

  ' PURPOSE: Get the all terminals checked.
  '
  '  PARAMS:
  '     - INPUT:
  '         - TerminalList
  '
  '     - OUTPUT:
  '         - TerminalId As TerminalList
  '
  ' RETURNS:
  '
  Public Function GetTerminalIdSelected(ByRef TerminalsList As TerminalList) As DataTable

    Dim _tl_always_block As TerminalList

    _tl_always_block = New TerminalList()


    Try
      GetTerminalsFromControl(TerminalsList)

      If TerminalsList.ExistsElements(GROUP_ELEMENT_TYPE.AREA) OrElse _
         TerminalsList.ExistsElements(GROUP_ELEMENT_TYPE.BANK) OrElse _
         TerminalsList.ExistsElements(GROUP_ELEMENT_TYPE.GROUP) OrElse _
         TerminalsList.ExistsElements(GROUP_ELEMENT_TYPE.PROV) OrElse _
         TerminalsList.ExistsElements(GROUP_ELEMENT_TYPE.TERM) OrElse _
         TerminalsList.ExistsElements(GROUP_ELEMENT_TYPE.ZONE) Then
        Return GetTermianlId(TerminalsList)
      End If

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

    Return New DataTable

  End Function ' GetTerminalIdSelected

  ' PURPOSE: Shows if a node is a root node.
  '
  '  PARAMS:
  '     - INPUT:
  '         - Node As ItemGroup
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean.
  '
  Public Function IsRoot(ByVal Node As ItemGroup) As Boolean

    Return Node.ElementType = GROUP_NODE_ELEMENT.PROVIDER Or _
           Node.ElementType = GROUP_NODE_ELEMENT.GROUP Or _
           Node.ElementType = GROUP_NODE_ELEMENT.ZONE Or _
           Node.ElementType = GROUP_NODE_ELEMENT.ALL

  End Function ' IsRoot

  ' PURPOSE: Shows if a node is a leaf node.
  '
  '  PARAMS:
  '     - INPUT:
  '         - Node As ItemGroup
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean.
  '
  ' Set last level:
  '           - Terminals
  '           - Grupos
  '           - Providers in a center multisite
  ' If new group is added, it is necessary add here
  '
  Public Function IsLeaf(ByVal Node As ItemGroup) As Boolean

    Return Node.ElementType = GROUP_NODE_ELEMENT.TERMINALS _
        Or Node.ElementType = GROUP_NODE_ELEMENT.GROUPS _
        Or Node.ElementType = GROUP_NODE_ELEMENT.PROVIDERS And GeneralParam.GetBoolean("MultiSite", "IsCenter", False)

  End Function ' IsLeaf

  ' PURPOSE: Set TerminalList object and init controls
  '
  '  PARAMS:
  '     - INPUT:
  '         - Providers As TerminalList
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean.
  '
  Public Function SetTerminalList(ByVal Terminals As TerminalList) As Boolean

    Me.m_terminal_list = Terminals
    Me.cmb_provider_list_type.Value = Terminals.ListType

    Call EnableCheckTerminalListByListType()
    Call CreateDatableElements()
    Call RefreshToolTipText()

    If m_display_mode = DisplayMode.NotCollapsedWithoutCollapseButton Then
      Call InitChecks()
      Call ExpandOrCollapse()
    End If

  End Function ' SetTerminalList

  ' PURPOSE: Create tree with a datatable
  '
  '  PARAMS:
  '     - INPUT:
  '         - Providers As TerminalList
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean.
  '
  Public Function SetElementList(ByVal Elements As DataTable) As Boolean

    Dim _type As GROUP_NODE_ELEMENT
    Dim _id As Integer
    Dim _name As String

    m_dt_elements = CreateElementsTable()

    For Each _element As DataRow In Elements.Rows

      _id = _element(Groups.ELEMENTS_COLUMN_ID)
      _name = _element(Groups.ELEMENTS_COLUMN_NAME)
      _type = _element(Groups.ELEMENTS_COLUMN_TYPE)
      Call AddElement(_name, _id, _type)

    Next

  End Function ' SetTerminalList

  ' PURPOSE: Check it there are somo elements selected.
  '
  '  PARAMS:
  '     - INPUT:
  '         - Providers As TerminalList
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean.
  '
  Public Function CheckAtLeastOneSelected() As Boolean

    If Me.cmb_provider_list_type.Value = TerminalList.TerminalListType.All Then

      Return True
    End If

    If m_dt_elements.Rows.Count > 0 Then

      Return True
    End If

    Return False

  End Function ' CheckAtLeastOneSelected

  ' PURPOSE: Add all the childs of the ParentNode to the tree view
  '
  '    - INPUT:
  '         - ParentNode ItemGroup
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub AddChildNodes(ByVal ParentNode As ItemGroup)

    Dim _node_type As GROUP_NODE_ELEMENT
    Dim _node_type_element As GROUP_NODE_ELEMENT
    Dim _id_element As Int32
    Dim _id As Int32
    Dim _dt As DataTable
    Dim _enabled As Boolean
    Dim _search_column As String
    Dim _parent_checked As uc_treeview_three_state.CHECKED_STATE
    Dim _element_type As GROUP_ELEMENT_TYPE
    Dim _count_checked As Integer
    Dim _count_added As Integer
    Dim _count_mixed As Integer
    Dim _state_check As uc_treeview_three_state.CHECKED_STATE
    Dim _dv As DataView
    Dim _terminal_id As Int32

    _dt = Nothing
    _node_type = CType(ParentNode.ElementType, GROUP_NODE_ELEMENT)
    _id = Convert.ToInt32(ParentNode.ElementId)
    _count_checked = 0
    _count_added = 0
    _count_mixed = 0

    ' Node ALL don't have childs
    If _node_type = GROUP_NODE_ELEMENT.ALL Then
      Return
    End If

    ' Get all childs
    If Not GetElements(_node_type, _id, _dt) Then
      Return
    End If

    _dv = _dt.DefaultView
    _dv.Sort = ELEMENTS_COLUMN_NAME & " ASC"
    _dt = _dv.ToTable()

    ' Remove empty node 
    ParentNode.Nodes.Clear()

    For Each _dr_new_node As DataRow In _dt.Rows

      Dim _new_node As ItemGroup
      Dim _dr_to_find As DataRow

      ' Get the name of the node and create it.
      _node_type_element = _dr_new_node(ELEMENTS_COLUMN_NODE_TYPE)
      _id_element = CType(_dr_new_node(ELEMENTS_COLUMN_ID), Int32)
      _enabled = CType(_dr_new_node(ELEMENTS_COLUMN_ENABLED), Boolean)
      _terminal_id = CType(_dr_new_node(ELEMENTS_COLUMN_TERMINAL_ID), Int32)

      _new_node = New ItemGroup()
      _new_node.ElementType = _node_type_element
      _new_node.ElementId = _id_element
      _new_node.NodeFont = New Font(tr_group_list.Font, FontStyle.Regular)
      _new_node.ElementName = _dr_new_node(ELEMENTS_COLUMN_NAME)
      _new_node.ElementEnabled = False
      _new_node.ElementTerminalId = _terminal_id

      _element_type = _dr_new_node(ELEMENTS_COLUMN_TYPE)

      ' Do nothing for defined forbidden groups collection
      If (_element_type = GROUP_ELEMENT_TYPE.GROUP And m_forbidden_groups.Contains(_new_node.ElementId)) Then
        Continue For
      End If

      _state_check = uc_treeview_three_state.CHECKED_STATE.UNCHECKED
      _parent_checked = tr_group_list.GetNodeState(ParentNode)

      If _parent_checked = uc_treeview_three_state.CHECKED_STATE.CHECKED Then
        _state_check = _parent_checked
      End If

      _dr_to_find = m_dt_elements.Rows.Find(New Object() {_dr_new_node(ELEMENTS_COLUMN_ID), _element_type})

      If Not IsNothing(_dr_to_find) Then ' is leaf
        If m_three_state_check_mode Then
          _state_check = uc_treeview_three_state.CHECKED_STATE.CHECKED
          If _new_node.ElementType = GROUP_NODE_ELEMENT.GROUPS Then
            Call DisableGroup(_new_node)
          End If
        Else
          Call SelectTreeNode(_new_node)
        End If
      Else
        If m_three_state_check_mode Then
          If _new_node.ElementType = GROUP_NODE_ELEMENT.GROUPS Then
            Call DisableGroup(_new_node)
          End If
        Else
          Call DeselectTreeNode(_new_node)
        End If
      End If

      If IsNothing(_dr_to_find) And m_three_state_check_mode Then

        If Not IsLeaf(_new_node) AndAlso _parent_checked <> uc_treeview_three_state.CHECKED_STATE.CHECKED Then

          _search_column = ""
          Select Case _element_type
            Case GROUP_ELEMENT_TYPE.PROV
              _search_column = DT_COLUMN_PROV
            Case GROUP_ELEMENT_TYPE.ZONE
              _search_column = DT_COLUMN_ZONE
            Case GROUP_ELEMENT_TYPE.AREA
              _search_column = DT_COLUMN_AREA
            Case GROUP_ELEMENT_TYPE.BANK
              _search_column = DT_COLUMN_BANK
          End Select

          If Not String.IsNullOrEmpty(_search_column) Then
            If m_dt_elements.Select(_search_column & " = '" & _dr_new_node(ELEMENTS_COLUMN_ID) & "'").Length > 0 Then
              ' is parent of leaf 
              _state_check = uc_treeview_three_state.CHECKED_STATE.MIXED
            End If
          End If

        End If

      End If

      ' Add empty node if the current node isn't a leaf
      If Not IsLeaf(_new_node) Then
        Dim _node_empty As New ItemGroup()
        _node_empty.NodeFont = New Font(tr_group_list.Font, FontStyle.Regular)
        _new_node.Nodes.Add(_node_empty)
      End If

      Call tr_group_list.SetNodeState(_new_node, _state_check)

      If _state_check = uc_treeview_three_state.CHECKED_STATE.CHECKED Then
        _count_checked += 1
      ElseIf _state_check = uc_treeview_three_state.CHECKED_STATE.MIXED Then
        _count_mixed += 1
      End If

      _new_node.Name = _new_node.ElementId & "," & _new_node.ElementType

      ParentNode.Nodes.Add(_new_node)
      _count_added += 1

    Next

    ' UPDATE PARENT STATE
    If tr_group_list.GetNodeState(ParentNode) = uc_treeview_three_state.CHECKED_STATE.MIXED _
      OrElse tr_group_list.GetNodeState(ParentNode) = uc_treeview_three_state.CHECKED_STATE.UNINITIALISED Then

      ' ATB 22-FEB-2017
      ' Checkboxes behaviour:
      ' 1- If there's no child selected: unchecked
      ' 2- If every child is selected: checked
      ' 3- If there are less checked options than total: mixed checked
      If _count_checked = 0 And _count_mixed = 0 Then
        tr_group_list.SetNodeState(ParentNode, uc_treeview_three_state.CHECKED_STATE.UNCHECKED, True)
      ElseIf _count_added = _count_checked Then
        tr_group_list.SetNodeState(ParentNode, uc_treeview_three_state.CHECKED_STATE.CHECKED, True)
      ElseIf _count_checked < _count_added Then
        tr_group_list.SetNodeState(ParentNode, uc_treeview_three_state.CHECKED_STATE.MIXED, True)
      End If

    End If

    ParentNode.ElementExpanded = True

  End Sub ' AddChildNodes

  ' PURPOSE: Check all elements in tree
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub CheckAllElements(ByVal CheckState As uc_treeview_three_state.CHECKED_STATE)

    Dim _id As Integer
    Dim _type As Integer
    Dim _dr As DataRow

    Try

      For index As Integer = m_dt_elements.Rows.Count - 1 To 0 Step -1

        _dr = m_dt_elements.Rows(index)
        _id = Integer.Parse(_dr(DT_COLUMN_ID))
        _type = Integer.Parse(_dr(DT_COLUMN_TYPE))

        tr_group_list.SetNodeState(tr_group_list.Nodes(0), CheckState, False)

      Next

      If CheckState = uc_treeview_three_state.CHECKED_STATE.UNCHECKED Then
        DeselectAllElements()
      Else
        'TODO Safe the code for case checked
      End If

    Catch ex As Exception

    End Try

  End Sub ' DeselectAllElements

  ' PURPOSE: Deselect all elements in tree
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub DeselectAllElements()

    Dim _id As Integer
    Dim _type As Integer
    Dim _dr As DataRow

    For index As Integer = m_dt_elements.Rows.Count - 1 To 0 Step -1

      _dr = m_dt_elements.Rows(index)
      _id = Integer.Parse(_dr(DT_COLUMN_ID))
      _type = Integer.Parse(_dr(DT_COLUMN_TYPE))

      DeselectElement(_id, GroupConvert(_type, _id))

    Next

  End Sub ' DeselectAllElements

  ' PURPOSE: Deselect a element in tree
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub DeselectElement(ByVal Id As Int32, ByVal Type As GROUP_NODE_ELEMENT)

    Dim _find_nodes() As TreeNode
    Dim _dr_to_find As DataRow

    _find_nodes = Me.tr_group_list.Nodes.Find(Id & "," & Type, True)
    For Each _node As ItemGroup In _find_nodes
      Call Me.DeselectTreeNode(_node)
    Next

    _dr_to_find = m_dt_elements.Rows.Find(New Object() {Id, GroupConvert(Type)})
    If Not IsNothing(_dr_to_find) Then
      _dr_to_find.Delete()
    End If

  End Sub

  ' PURPOSE: Select a element in tree
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub SelectElement(ByVal Id As Int32, ByVal Type As GROUP_NODE_ELEMENT, ByVal Name As String)
    Dim _find_nodes() As TreeNode
    Dim _dr_to_find As DataRow

    _find_nodes = Me.tr_group_list.Nodes.Find(Id & "," & Type, True)
    If _find_nodes.Length > 0 Then
      For Each _node As ItemGroup In _find_nodes
        Call Me.SelectTreeNode(_node)
      Next
    End If

    _dr_to_find = m_dt_elements.Rows.Find(New Object() {Id, GroupConvert(Type)})
    If IsNothing(_dr_to_find) Then
      Call AddElement(Name, Id, GroupConvert(Type))
    End If

  End Sub

#End Region ' Public Functions

#Region " Public Shared Functions"

  ' PURPOSE: Get information about terminal
  '
  '    - INPUT:
  '       - TerminalId: Int32
  '
  '    - OUTPUT:
  '       - PoviderName: String
  '       - Path: String
  '
  ' RETURNS:
  '
  Public Shared Sub GetTerminalInfo(ByVal MasterId As Int32, _
                                    ByRef ProviderName As String, _
                                    ByRef BankName As String, _
                                    ByRef AreaName As String, _
                                    ByRef ZoneName As String, _
                                    ByRef TerminalId As Int32)

    Try

      Dim _sb As System.Text.StringBuilder

      _sb = New System.Text.StringBuilder()

      Using _db_trx As New WSI.Common.DB_TRX()

        _sb.AppendLine("SELECT   (SELECT A.AR_SMOKING FROM AREAS A WHERE A.AR_AREA_ID = (SELECT B.BK_AREA_ID FROM BANKS B WHERE B.BK_BANK_ID = T.TE_BANK_ID)) AS ZONE_NAME")
        _sb.AppendLine("     ,   (SELECT A.AR_NAME FROM AREAS A WHERE A.AR_AREA_ID = (SELECT B.BK_AREA_ID FROM BANKS B WHERE B.BK_BANK_ID = T.TE_BANK_ID)) AS AREA_NAME")
        _sb.AppendLine("     ,   (SELECT B.BK_NAME FROM BANKS B WHERE B.BK_BANK_ID = T.TE_BANK_ID) AS BANK_NAME")
        _sb.AppendLine("     ,   (SELECT P.PV_NAME FROM PROVIDERS P WHERE P.PV_ID = T.TE_PROV_ID) AS PROV_NAME")
        _sb.AppendLine("     ,   TE_TERMINAL_ID AS TERMINAL_ID")
        _sb.AppendLine("  FROM   TERMINALS T")
        _sb.AppendLine(" INNER JOIN TERMINALS_LAST_CHANGED TLC ON T.TE_TERMINAL_ID = TLC.TLC_TERMINAL_ID ")
        _sb.AppendLine(" WHERE   TLC.TLC_MASTER_ID = @pTerminalId")

        Using _sql_cmd As New SqlClient.SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = MasterId

          Using _reader As SqlClient.SqlDataReader = _sql_cmd.ExecuteReader()

            If (_reader.Read()) Then

              ZoneName = IIf(CType(_reader(0), Boolean), GLB_NLS_GUI_CONTROLS.GetString(428), GLB_NLS_GUI_CONTROLS.GetString(440))
              AreaName = _reader(1)
              BankName = _reader(2)
              ProviderName = _reader(3)
              TerminalId = _reader(4)

            End If

          End Using

        End Using

      End Using

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' GetTerminalInfo

  ' PURPOSE: Get a name representing the type
  '
  '    - INPUT:
  '       - Type GROUP_NODE_ELEMENT
  '       - NodeTitle Boolean: True if is for add concat with a tree node text or an element text
  '
  '    - OUTPUT:
  '
  ' RETURNS: String
  '
  Public Shared Function GetTypeTitle(ByVal Type As GROUP_NODE_ELEMENT, ByVal NodeTitle As Boolean) As String

    Select Case (Type)

      Case GROUP_NODE_ELEMENT.GROUP
        Return IIf(NodeTitle, "", GLB_NLS_GUI_CONTROLS.GetString(471))
      Case GROUP_NODE_ELEMENT.GROUPS
        Return GLB_NLS_GUI_CONTROLS.GetString(471) + IIf(NodeTitle, ": ", "")
      Case GROUP_NODE_ELEMENT.PROVIDER
        Return IIf(NodeTitle, "", GLB_NLS_GUI_CONTROLS.GetString(467))
      Case GROUP_NODE_ELEMENT.PROVIDERS
        Return GLB_NLS_GUI_CONTROLS.GetString(467) + IIf(NodeTitle, ": ", "")
      Case GROUP_NODE_ELEMENT.ZONE
        Return IIf(NodeTitle, "", GLB_NLS_GUI_CONTROLS.GetString(468))
      Case GROUP_NODE_ELEMENT.ZONES
        Return GLB_NLS_GUI_CONTROLS.GetString(468) + IIf(NodeTitle, ": ", "")
      Case GROUP_NODE_ELEMENT.AREAS
        Return GLB_NLS_GUI_CONTROLS.GetString(469) + IIf(NodeTitle, ": ", "")
      Case GROUP_NODE_ELEMENT.BANKS
        Return GLB_NLS_GUI_CONTROLS.GetString(470) + IIf(NodeTitle, ": ", "")
      Case GROUP_NODE_ELEMENT.TERMINALS
        Return GLB_NLS_GUI_CONTROLS.GetString(472) + IIf(NodeTitle, ": ", "")
      Case GROUP_NODE_ELEMENT.ALL
        Return IIf(NodeTitle, "", GLB_NLS_GUI_CONTROLS.GetString(474))
      Case Else
        Return ""

    End Select

  End Function ' GetTypeTitle

  ' PURPOSE: Get a name representing the type
  '
  '    - INPUT:
  '       - Type GROUP_NODE_ELEMENT
  '       - NodeTitle Boolean: True if is for add concat with a tree node text or an element text
  '
  '    - OUTPUT:
  '
  ' RETURNS: String
  '
  Public Shared Function GetTypeTitle(ByVal Type As GROUP_ELEMENT_TYPE, ByVal NodeTitle As Boolean) As String

    Select Case (Type)

      Case GROUP_ELEMENT_TYPE.GROUP
        Return GLB_NLS_GUI_CONTROLS.GetString(471) + IIf(NodeTitle, ": ", "")
      Case GROUP_ELEMENT_TYPE.PROV
        Return GLB_NLS_GUI_CONTROLS.GetString(467) + IIf(NodeTitle, ": ", "")
      Case GROUP_ELEMENT_TYPE.ZONE
        Return GLB_NLS_GUI_CONTROLS.GetString(468) + IIf(NodeTitle, ": ", "")
      Case GROUP_ELEMENT_TYPE.AREA
        Return GLB_NLS_GUI_CONTROLS.GetString(469) + IIf(NodeTitle, ": ", "")
      Case GROUP_ELEMENT_TYPE.BANK
        Return GLB_NLS_GUI_CONTROLS.GetString(470) + IIf(NodeTitle, ": ", "")
      Case GROUP_ELEMENT_TYPE.TERM
        Return GLB_NLS_GUI_CONTROLS.GetString(472) + IIf(NodeTitle, ": ", "")
      Case GROUP_ELEMENT_TYPE.ALL
        Return IIf(NodeTitle, "", GLB_NLS_GUI_CONTROLS.GetString(474))
      Case Else
        Return ""

    End Select

  End Function ' GetTypeTitle

#End Region ' Public Shared Functions

#Region " Private Functions "

  ' PURPOSE: Own controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean.
  '
  Private Sub InitControls()

    Me.gb_provider.Text = GLB_NLS_GUI_CONTROLS.GetString(477)

    With Me.tool_tip
      .InitialDelay = 2000
      .ReshowDelay = 1000
      .ShowAlways = True
      .AutomaticDelay = 1000
      .AutoPopDelay = 6000
      .ToolTipTitle = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2738)
    End With

    Call ProviderComboFill()
    Call EnableCheckTerminalListByListType()
    Call RefreshToolTipText()

    Me.m_show_groupbox_line = Me.gb_provider.Visible
    Me.m_cmb_provider_list_type_enabled = True

  End Sub ' InitControls

  ' PURPOSE: add text to the tooltip.
  '
  '  PARAMS:
  '
  ' RETURNS:
  '
  Private Sub RefreshToolTipText()
    Dim _tool_tip_text As String

    If Not m_dt_elements Is Nothing Then

      _tool_tip_text = getElementsList()

      With Me.tool_tip
        .Active = False
        .SetToolTip(Me.btn_collapse, _tool_tip_text)
        .SetToolTip(Me.tr_group_list, _tool_tip_text)
        .SetToolTip(Me.gb_provider, _tool_tip_text)
        Me.cmb_provider_list_type.SetToolBoxText(_tool_tip_text, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2738))
        .SetToolTip(Me.tlp_controls, _tool_tip_text)
        .Active = True
      End With
    End If

  End Sub 'addToolTipText

  ' PURPOSE: Get the List of all Terminals, with the Type
  '
  '  PARAMS:
  '
  ' RETURNS:
  '     - String.
  '
  Private Function getElementsList() As String
    Dim _tool_tip_text As StringBuilder
    Dim _max_values_to_display As Int32
    Dim _element_type As GROUP_ELEMENT_TYPE

    _tool_tip_text = New StringBuilder()
    _max_values_to_display = 0

    For Each _row As DataRow In m_dt_elements.Rows
      If _row.RowState <> DataRowState.Deleted Then
        If _max_values_to_display = MAX_ELEMENTS_TO_SHOW_IN_TOOL_TIP Then
          _tool_tip_text.AppendLine("...")

          Exit For
        End If
        _element_type = CType(_row(DT_COLUMN_TYPE), GROUP_ELEMENT_TYPE)
        _tool_tip_text.AppendLine(GetTypeTitle(_element_type, True) & _row(DT_COLUMN_NAME))
        _max_values_to_display += 1
      End If
    Next

    Return _tool_tip_text.ToString()

  End Function 'getListOfElements

  ' PURPOSE: Init provider combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub ProviderComboFill()

    Call Me.cmb_provider_list_type.Add(TerminalList.TerminalListType.All, GLB_NLS_GUI_CONFIGURATION.GetString(497)) 'All
    Call Me.cmb_provider_list_type.Add(TerminalList.TerminalListType.Listed, GLB_NLS_GUI_CONFIGURATION.GetString(499)) 'Listed
    Call Me.cmb_provider_list_type.Add(TerminalList.TerminalListType.NotListed, GLB_NLS_GUI_CONFIGURATION.GetString(498)) 'Not Listed

    Me.cmb_provider_list_type.SelectedIndex = TerminalList.TerminalListType.All

  End Sub ' ProviderComboFill

  ' PURPOSE: Fill de elements list with all the elements in TerminalList and its origin information
  '          Used to Fill Tree checkboxes
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub CreateDatableElements()

    m_dt_elements = CreateElementsTable()

    Call AddToElementsList(GROUP_ELEMENT_TYPE.PROV)
    Call AddToElementsList(GROUP_ELEMENT_TYPE.ZONE)
    Call AddToElementsList(GROUP_ELEMENT_TYPE.AREA)
    Call AddToElementsList(GROUP_ELEMENT_TYPE.BANK)
    Call AddToElementsList(GROUP_ELEMENT_TYPE.TERM)
    Call AddToElementsList(GROUP_ELEMENT_TYPE.GROUP)

  End Sub ' ElementsListFill

  ' PURPOSE: Create the table's structure.
  '
  '  PARAMS:
  '      - INPUT:
  '
  '      - OUTPUT:
  '          - DtElements: DataTable
  '
  ' RETURNS:
  '
  Private Function CreateElementsTable()

    Dim _dt_elements As New DataTable()

    _dt_elements.Columns.Add(DT_COLUMN_ID, Type.GetType("System.Int32"))
    _dt_elements.Columns.Add(DT_COLUMN_NAME, Type.GetType("System.String"))
    _dt_elements.Columns.Add(DT_COLUMN_TYPE, Type.GetType("System.Int32"))
    _dt_elements.Columns.Add(DT_COLUMN_PROV, Type.GetType("System.Int32"))
    _dt_elements.Columns.Add(DT_COLUMN_ZONE, Type.GetType("System.Int32"))
    _dt_elements.Columns.Add(DT_COLUMN_AREA, Type.GetType("System.Int32"))
    _dt_elements.Columns.Add(DT_COLUMN_BANK, Type.GetType("System.Int32"))
    _dt_elements.Columns.Add(DT_COLUMN_TERMINAL_ID, Type.GetType("System.Int32"))

    _dt_elements.PrimaryKey = New DataColumn() {_dt_elements.Columns(DT_COLUMN_ID), _dt_elements.Columns(DT_COLUMN_TYPE)}

    Return _dt_elements

  End Function ' CreateElementsTable

  ' PURPOSE: Fill de m_dt_elements table with information of all elements in the TerminalList
  '
  '    - INPUT:
  '       - Type: type of the elements to get from TeminalList 
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub AddToElementsList(ByVal Type As GROUP_ELEMENT_TYPE)
    Dim _dt As DataTable
    Dim _id As Int32
    Dim _name As String

    _dt = Me.m_terminal_list.GetElementsList(Type)

    For Each _row As DataRow In _dt.Rows

      _name = CType(_row(TerminalList.COLUMN_NAME), String)
      _id = CType(_row(TerminalList.COLUMN_ID), Int32)

      Call AddElement(_name, _id, Type)

    Next

  End Sub ' AddToElementsList

  ' PURPOSE: Note the information about an element in m_dt_elements table
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub AddElement(ByVal Name As String, ByVal Id As Int32, ByVal Type As GROUP_ELEMENT_TYPE)
    Dim _provider As Int32
    Dim _zone As Int32
    Dim _area As Int32
    Dim _bank As Int32
    Dim _terminal_id As Int32

    _provider = -1
    _zone = -1
    _area = -1
    _bank = -1
    _terminal_id = -1

    Select Case Type

      Case GROUP_ELEMENT_TYPE.TERM
        GetTerminalInfo(Id, _provider, _bank, _area, _zone, _terminal_id)

      Case GROUP_ELEMENT_TYPE.AREA
        GetAreaInfo(Id, _zone)

      Case GROUP_ELEMENT_TYPE.BANK
        GetBankInfo(Id, _area, _zone)

      Case Else

    End Select

    m_dt_elements.Rows.Add(Id, Name, Type, _provider, _zone, _area, _bank, _terminal_id)

  End Sub ' AddElement

  ' PURPOSE: Enable/Disable controls depending on provider type selected
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Sub EnableCheckTerminalListByListType()

    Select Case Me.cmb_provider_list_type.Value

      Case TerminalList.TerminalListType.All
        If m_display_mode = DisplayMode.NotCollapsedWithoutCollapseButton Then
          Me.tr_group_list.Enabled = False
          Me.m_collapsed = False
        Else
          Me.btn_collapse.Enabled = False
          Me.m_collapsed = True
        End If

      Case TerminalList.TerminalListType.NotListed
        Me.btn_collapse.Enabled = True
        Me.tr_group_list.Enabled = True

      Case TerminalList.TerminalListType.Listed
        Me.btn_collapse.Enabled = True
        Me.tr_group_list.Enabled = True

      Case Else
        Me.btn_collapse.Enabled = False

    End Select
    Call ExpandOrCollapse()
  End Sub ' EnableCheckTerminalListByListType

  ' PURPOSE: Init groups tree
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InitGroupTree()

    Dim _dt_root As DataTable
    Dim _type As GROUP_NODE_ELEMENT

    _dt_root = Nothing

    Try

      ' Fill tree
      If Not GetRootElements(WSI.Common.GROUP_CONTENT_TYPE.TERMINALS, _dt_root, m_show_groups) Then

        Return
      End If

      For Each _row As DataRow In _dt_root.Rows

        _type = CType(_row(ELEMENTS_COLUMN_NODE_TYPE), GROUP_NODE_ELEMENT)

        ' No insert node all
        If (Not m_show_node_all And _type = GROUP_NODE_ELEMENT.ALL) Then

          Continue For
        End If

        Dim _node As New ItemGroup()
        _node.ElementType = _type
        _node.ElementId = CType(_row(ELEMENTS_COLUMN_ID), Int32)
        _node.ElementEnabled = False
        _node.ElementName = _row(ELEMENTS_COLUMN_NAME)
        _node.NodeFont = New Font(tr_group_list.Font, FontStyle.Regular)

        If _type <> GROUP_NODE_ELEMENT.ALL Then

          Dim _node_empty As New ItemGroup()
          _node_empty.NodeFont = New Font(tr_group_list.Font, FontStyle.Regular)
          _node.Nodes.Add(_node_empty)

        End If

        tr_group_list.Nodes.Add(_node)

      Next

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' InitGroupTree

  ' PURPOSE: Init tree nodes state
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InitChecks()

    For Each _dr As DataRow In m_dt_elements.Rows

      Select Case CType(_dr(DT_COLUMN_TYPE), GROUP_ELEMENT_TYPE)

        Case GROUP_ELEMENT_TYPE.PROV

          tr_group_list.SetNodeState(tr_group_list.Nodes(0), uc_treeview_three_state.CHECKED_STATE.MIXED)

        Case GROUP_ELEMENT_TYPE.GROUP

          tr_group_list.SetNodeState(tr_group_list.Nodes(2), uc_treeview_three_state.CHECKED_STATE.MIXED)

        Case GROUP_ELEMENT_TYPE.ZONE

          tr_group_list.SetNodeState(tr_group_list.Nodes(1), uc_treeview_three_state.CHECKED_STATE.MIXED)

        Case GROUP_ELEMENT_TYPE.ALL

          tr_group_list.SetNodeState(tr_group_list.Nodes(3), uc_treeview_three_state.CHECKED_STATE.MIXED)

        Case GROUP_ELEMENT_TYPE.AREA, GROUP_ELEMENT_TYPE.BANK

          tr_group_list.SetNodeState(tr_group_list.Nodes(1), uc_treeview_three_state.CHECKED_STATE.MIXED)

        Case GROUP_ELEMENT_TYPE.TERM

          tr_group_list.SetNodeState(tr_group_list.Nodes(0), uc_treeview_three_state.CHECKED_STATE.MIXED)
          tr_group_list.SetNodeState(tr_group_list.Nodes(1), uc_treeview_three_state.CHECKED_STATE.MIXED)

      End Select

    Next

  End Sub ' InitChecks

  ' PURPOSE: Note in m_dt_elements that the element indicated in Node has been added to TerminalList and update other elements that could be afected 
  '
  '  PARAMS:
  '     - INPUT:
  '         - Node ItemGroup: Information about the element
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InsertNodeInElements(ByVal Node As ItemGroup)
    Dim _dr As DataRow

    _dr = m_dt_elements.Rows.Find(New Object() {Node.ElementId, GroupConvert(Node.ElementType)})
    If IsNothing(_dr) Then
      Call AddElement(Node.ElementName, Node.ElementId, GroupConvert(Node.ElementType))
    End If

    Call UpdateRelationsElements(Node)

  End Sub ' InsertNodeInElements

  ' PURPOSE: Note in m_dt_elements that the element indicated in Node has been deleted from TerminalList and update other elements that could be afected 
  '
  '  PARAMS:
  '     - INPUT:
  '         - Node ItemGroup: Information about the element
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub RemoveNodeInElements(ByVal Node As ItemGroup)
    Dim _dr As DataRow

    _dr = m_dt_elements.Rows.Find(New Object() {Node.ElementId, GroupConvert(Node.ElementType)})
    If Not IsNothing(_dr) Then
      _dr.Delete()
    End If

    Call UpdateRelationsElements(Node)

  End Sub ' RemoveNodeInElements

  ' PURPOSE: Update elements that could be afected beacouse the state of element indicated in Node has been changed  
  '
  '  PARAMS:
  '     - INPUT:
  '         - Node ItemGroup: Information about the element
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub UpdateRelationsElements(ByVal Node As ItemGroup)
    Dim _search_column As String
    Dim _rows() As DataRow
    Dim _dr As DataRow

    If IsLeaf(Node) Then

      Return
    End If

    _search_column = ""
    Select Case Node.ElementType

      Case GROUP_NODE_ELEMENT.AREAS
        _search_column = DT_COLUMN_AREA
      Case GROUP_NODE_ELEMENT.BANKS
        _search_column = DT_COLUMN_BANK
      Case GROUP_NODE_ELEMENT.ZONES
        _search_column = DT_COLUMN_ZONE
      Case GROUP_NODE_ELEMENT.PROVIDERS
        _search_column = DT_COLUMN_PROV

    End Select

    If Not String.IsNullOrEmpty(_search_column) Then
      _rows = m_dt_elements.Select(_search_column & " = '" & Node.ElementId & "'")
      If _rows.Length > 0 Then
        ' is parent of leaf 
        For Each _dr In _rows
          _dr.Delete()
        Next

      End If
    End If

  End Sub ' UpdateRelationsElements

  ' PURPOSE: Get information about terminal
  '
  '    - INPUT:
  '       - TerminalId: Int32
  '
  '    - OUTPUT:
  '       - PoviderName: String
  '       - Path: String
  '
  ' RETURNS:
  '
  Private Sub GetTerminalInfo(ByVal MasterId As Int32, _
                              ByRef ProviderId As Int32, _
                              ByRef BankId As Int32, _
                              ByRef AreaId As Int32, _
                              ByRef ZoneId As Int32, _
                              ByRef TerminalId As Int32)

    Try

      Dim _sb As System.Text.StringBuilder

      _sb = New System.Text.StringBuilder()

      Using _db_trx As New WSI.Common.DB_TRX()

        _sb.AppendLine("SELECT   (SELECT A.AR_SMOKING FROM AREAS     A WHERE A.AR_AREA_ID = (SELECT B.BK_AREA_ID FROM BANKS B WHERE B.BK_BANK_ID = T.TE_BANK_ID)) AS ZONE_NAME")
        _sb.AppendLine("     ,   (SELECT A.AR_AREA_ID FROM AREAS     A WHERE A.AR_AREA_ID = (SELECT B.BK_AREA_ID FROM BANKS B WHERE B.BK_BANK_ID = T.TE_BANK_ID)) AS AREA_NAME")
        _sb.AppendLine("     ,   (SELECT B.BK_BANK_ID FROM BANKS     B WHERE B.BK_BANK_ID = T.TE_BANK_ID) AS BANK_NAME")
        _sb.AppendLine("     ,   (SELECT P.PV_ID      FROM PROVIDERS P WHERE P.PV_ID      = T.TE_PROV_ID) AS PROV_NAME")
        _sb.AppendLine("     ,   TE_TERMINAL_ID AS TERMINAL_ID")
        _sb.AppendLine("  FROM   TERMINALS T")
        _sb.AppendLine(" INNER JOIN TERMINALS_LAST_CHANGED TLC ON T.TE_TERMINAL_ID = TLC.TLC_TERMINAL_ID ")
        _sb.AppendLine(" WHERE   TLC.TLC_MASTER_ID = @pTerminalId")

        Using _sql_cmd As New SqlClient.SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = MasterId

          Using _reader As SqlClient.SqlDataReader = _sql_cmd.ExecuteReader()

            If (_reader.Read()) Then

              ZoneId = IIf(_reader(0), 1, 0)
              AreaId = _reader(1)
              BankId = _reader(2)
              ProviderId = _reader(3)
              TerminalId = _reader(4)

            End If

          End Using

        End Using

      End Using

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' GetTerminalInfo

  ' PURPOSE: Get information about bank
  '
  '    - INPUT:
  '       - TerminalId: Int32
  '
  '    - OUTPUT:
  '       - AreaName: String
  '       - ZoneName: String
  '
  ' RETURNS:
  '
  Private Sub GetBankInfo(ByVal BankId As Int32, ByRef AreaId As Int32, ByRef ZoneId As Int32)

    Try

      Dim _sb As System.Text.StringBuilder

      _sb = New System.Text.StringBuilder()

      Using _db_trx As New WSI.Common.DB_TRX()

        _sb.AppendLine("SELECT   (SELECT A.AR_SMOKING FROM AREAS A WHERE A.AR_AREA_ID = B.BK_AREA_ID) AS ZONE_NAME")
        _sb.AppendLine("     ,   (SELECT A.AR_AREA_ID FROM AREAS A WHERE A.AR_AREA_ID = B.BK_AREA_ID) AS AREA_NAME")
        _sb.AppendLine("  FROM   BANKS B")
        _sb.AppendLine(" WHERE   BK_BANK_ID = @pBankId")

        Using _sql_cmd As New SqlClient.SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@pBankId", SqlDbType.Int).Value = BankId

          Using _reader As SqlClient.SqlDataReader = _sql_cmd.ExecuteReader()

            If (_reader.Read()) Then

              ZoneId = IIf(_reader(0), 1, 0)
              AreaId = _reader(1)

            End If

          End Using

        End Using

      End Using

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' GetTerminalInfo

  ' PURPOSE: Get information about area
  '
  '    - INPUT:
  '       - TerminalId: Int32
  '
  '    - OUTPUT:
  '       - ZoneName: String
  '
  ' RETURNS:
  '
  Private Sub GetAreaInfo(ByVal AreaId As Int32, ByRef ZoneName As Int32)

    Try

      Dim _sb As System.Text.StringBuilder

      _sb = New System.Text.StringBuilder()

      Using _db_trx As New WSI.Common.DB_TRX()

        _sb.AppendLine("SELECT AR_SMOKING FROM AREAS WHERE AR_AREA_ID = @pAreaId")

        Using _sql_cmd As New SqlClient.SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)

          _sql_cmd.Parameters.Add("@pAreaId", SqlDbType.Int).Value = AreaId

          Using _reader As SqlClient.SqlDataReader = _sql_cmd.ExecuteReader()

            If (_reader.Read()) Then

              ZoneName = IIf(_reader(0), 1, 0)

            End If

          End Using

        End Using

      End Using

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' GetTerminalInfo

  ' PURPOSE: Cheange the state of a Node
  '
  '    - INPUT:
  '       - Node ItemGroup:
  '       - State CHECKED_STATE:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub UpdateTerminalNode(ByVal Node As ItemGroup, ByVal State As uc_treeview_three_state.CHECKED_STATE)
    Dim _id As Integer
    Dim _type As Integer
    Dim _search_column As String
    Dim _drs() As DataRow
    Dim _prov As Integer
    Dim _zone As Integer
    Dim _area As Integer
    Dim _bank As Integer
    Dim _ls_nodes As List(Of TreeNode)

    _ls_nodes = New List(Of TreeNode)

    _search_column = ""
    Select Case Node.ElementType

      Case GROUP_NODE_ELEMENT.AREAS
        _search_column = DT_COLUMN_AREA
      Case GROUP_NODE_ELEMENT.BANKS
        _search_column = DT_COLUMN_BANK
      Case GROUP_NODE_ELEMENT.ZONES
        _search_column = DT_COLUMN_ZONE
      Case GROUP_NODE_ELEMENT.PROVIDERS
        _search_column = DT_COLUMN_PROV

    End Select

    Dim _str As String
    If Me.IsLeaf(Node) Then
      _str = DT_COLUMN_TYPE & " = " & GroupConvert(GROUP_NODE_ELEMENT.TERMINALS) & " AND " & DT_COLUMN_ID & " = " & Node.ElementId
    Else
      If String.IsNullOrEmpty(_search_column) Then
        _str = DT_COLUMN_TYPE & " = " & GroupConvert(GROUP_NODE_ELEMENT.TERMINALS)
      Else
        _str = DT_COLUMN_TYPE & " = " & GroupConvert(GROUP_NODE_ELEMENT.TERMINALS) & " AND " & _search_column & " = " & Node.ElementId
      End If
    End If
    _drs = m_dt_elements.Select(_str)

    For Each _dr As DataRow In _drs

      _id = Integer.Parse(_dr(DT_COLUMN_ID))
      _type = Integer.Parse(_dr(DT_COLUMN_TYPE))

      _prov = Integer.Parse(_dr(DT_COLUMN_PROV))
      _zone = Integer.Parse(_dr(DT_COLUMN_ZONE))
      _area = Integer.Parse(_dr(DT_COLUMN_AREA))
      _bank = Integer.Parse(_dr(DT_COLUMN_BANK))

      _ls_nodes.AddRange(Me.tr_group_list.Nodes.Find(_id & "," & GroupConvert(_type, _id), True))
      _ls_nodes.AddRange(Me.tr_group_list.Nodes.Find(_prov & "," & GROUP_NODE_ELEMENT.PROVIDERS, True))
      _ls_nodes.AddRange(Me.tr_group_list.Nodes.Find(_zone & "," & GROUP_NODE_ELEMENT.ZONES, True))
      _ls_nodes.AddRange(Me.tr_group_list.Nodes.Find(_area & "," & GROUP_NODE_ELEMENT.AREAS, True))
      _ls_nodes.AddRange(Me.tr_group_list.Nodes.Find(_bank & "," & GROUP_NODE_ELEMENT.BANKS, True))

      For Each _node As ItemGroup In _ls_nodes
        If IsLeaf(_node) Then
          Me.tr_group_list.SetNodeState(_node, State, False)
        Else
          If State = uc_treeview_three_state.CHECKED_STATE.CHECKED Then
            Me.tr_group_list.SetNodeState(_node, uc_treeview_three_state.CHECKED_STATE.MIXED, False)
          Else
            Me.tr_group_list.SetNodeState(_node, uc_treeview_three_state.CHECKED_STATE.UNCHECKED, False)
          End If
        End If
      Next

    Next

  End Sub ' UpdateTerminalNode

  ' PURPOSE: Add an array of ItemGroups to a List of ItemGroups
  '
  '    - INPUT:
  '       - List  List(Of ItemGroup):
  '       - ItemGroups ItemGroup():
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub AddNodeToList(ByVal List As List(Of ItemGroup), ByVal ItemGroups() As ItemGroup)

    For Each _item_group As ItemGroup In ItemGroups

      List.Add(_item_group)

    Next

  End Sub ' AddNodeToList

  ' PURPOSE: Changes the visualization of a tree node to show that is added to the element's list
  '
  '    - INPUT:
  '         - Node ItemGroup
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SelectTreeNode(ByVal Node As ItemGroup)

    Node.NodeFont = New Font(Me.Font, FontStyle.Bold)
    Node.ForeColor = IIf(Node.ElementEnabled, Color.Black, Color.Red)

  End Sub ' SelectTreeNode

  ' PURPOSE: Changes the visualization of a tree node to show that is not added to the element's list
  '
  '    - INPUT:
  '         - Node ItemGroup
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub DeselectTreeNode(ByVal Node As ItemGroup)

    Node.NodeFont = New Font(Me.Font, FontStyle.Regular)
    Node.ForeColor = IIf(Node.ElementEnabled, Color.Black, Color.Red)

  End Sub ' DeselectTreeNode

  ' PURPOSE: Changes the visualization of a tree node to show that is not added to the element's list
  '
  '    - INPUT:
  '         - Node ItemGroup
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub DisableGroup(ByVal Node As ItemGroup)

    If Not Node.ElementEnabled Then
      Node.ForeColor = Color.Red
    End If

  End Sub ' DisableGroup

  ' PURPOSE: Set all variables depending the display mode selected.
  '
  '    - INPUT:
  '         - Node ItemGroup
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub UpdateDisplayMode()

    Select Case m_display_mode

      Case DisplayMode.CollapsedWithButtons
        Me.m_collapsed = True
        Me.cmb_provider_list_type.Visible = True
        Me.btn_collapse.Visible = True
        Me.m_three_state_check_mode = True

        Me.m_first_expand = False

      Case DisplayMode.NotCollapsedWithoutButtons
        Me.m_allow_collapse = False
        Me.m_collapsed = False
        Me.btn_collapse.Visible = False
        Me.cmb_provider_list_type.Visible = False
        Me.tr_group_list.Visible = True

        Call InitGroupTree()
        Me.m_first_expand = True

      Case DisplayMode.NotCollapsedWithoutCollapseButton
        Me.m_collapsed = False
        Me.cmb_provider_list_type.Visible = True
        Me.btn_collapse.Visible = False
        Me.m_three_state_check_mode = True
        Me.tlp_controls.ColumnStyles(1).Width = 0

        Call InitGroupTree()
        Me.m_first_expand = True

      Case Else
    End Select

    Call ExpandOrCollapse()

  End Sub ' UpdateDisplayMode

  Private Sub ExpandOrCollapse()

    If Me.m_collapsed Then

      Me.tr_group_list.Visible = False

      If (m_show_groupbox_line) Then
        Me.Height = Me.tlp_controls.Top + Me.cmb_provider_list_type.Height + Me.cmb_provider_list_type.Margin.Bottom + 10

        Dim _size As Size
        _size.Height = Me.tlp_controls.Top + Me.cmb_provider_list_type.Height + Me.cmb_provider_list_type.Margin.Bottom - 15
        _size.Width = Me.tlp_controls.Width
        Me.tlp_controls.Size = _size
      Else
        Me.Height = Me.cmb_provider_list_type.Height
      End If 'm_show_groupbox_line

      Me.btn_collapse.Text = STR_TO_EXPAND
    Else
      Me.tr_group_list.Visible = True

      If Not m_display_mode = DisplayMode.NotCollapsedWithoutCollapseButton Then
        Me.Height = m_height_on_expanded
      End If

      Me.btn_collapse.Text = STR_TO_COLLAPSE
    End If

    Call RestoreDefaultValues()

  End Sub
  ' PURPOSE: Restore values in design time.
  '
  '    - INPUT:
  '         - Node ItemGroup
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub RestoreDefaultValues()

    'Locations
    Me.tlp_controls.Location = Me.m_default_tlp_position
    Me.gb_provider.Location = Me.m_default_gb_provider_position
    Me.cmb_provider_list_type.Location = Me.m_default_cmb_provider_position
    Me.tr_group_list.Location = Me.m_default_tr_grouplist_position
    Me.btn_collapse.Location = Me.m_default_btn_collapse_position

    'Size
    Me.tlp_controls.Size = Me.m_default_tlp_size
    Me.gb_provider.Size = Me.m_default_gb_provider_size
    Me.cmb_provider_list_type.Size = Me.m_default_cmb_provider_size
    Me.tr_group_list.Size = Me.m_default_tr_grouplist_size
    Me.btn_collapse.Size = Me.m_default_btn_collapse_size
    Me.cmb_provider_list_type.Enabled = m_cmb_provider_list_type_enabled
    If Me.m_list_enabled Then
      Me.tr_group_list.Enabled = True
      Me.tr_group_list.IgnoreClick = m_ignore_click
    Else
      Me.tr_group_list.IgnoreClick = False
    End If
  End Sub

  ' PURPOSE: Return Terminal Id and Terminal Name of selected in uc
  '
  '    - INPUT:
  '         - TerminalList
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function GetTermianlId(ByRef TerminalList As TerminalList) As DataTable

    Dim _sb As StringBuilder
    Dim _firstWhere As Boolean = True
    Dim _dt As DataTable

    _sb = New StringBuilder
    _dt = New DataTable

    _sb.AppendLine("     SELECT   DISTINCT TE.TE_TERMINAL_ID                                     ")
    _sb.AppendLine("            , TE.TE_NAME                                                     ")
    _sb.AppendLine("            , TE.TE_PROVIDER_ID                                              ")
    _sb.AppendLine("       FROM   TERMINALS AS TE                                                ")
    _sb.AppendLine("  LEFT JOIN   TERMINAL_GROUPS AS TG ON TG.TG_TERMINAL_ID = TE.TE_TERMINAL_ID ")
    _sb.AppendLine(" INNER JOIN   BANKS AS BK ON BK.BK_BANK_ID = TE.TE_BANK_ID                   ")
    _sb.AppendLine(" INNER JOIN   AREAS AS AR ON AR.AR_AREA_ID = BK.BK_AREA_ID                   ")
    _sb.AppendLine(WhereIn(GROUP_ELEMENT_TYPE.TERM, "TE.TE_TERMINAL_ID", TerminalList, _firstWhere))
    _sb.AppendLine(WhereIn(GROUP_ELEMENT_TYPE.PROV, "TE.TE_PROV_ID", TerminalList, _firstWhere))
    _sb.AppendLine(WhereIn(GROUP_ELEMENT_TYPE.BANK, "TE.TE_BANK_ID", TerminalList, _firstWhere))
    _sb.AppendLine(WhereIn(GROUP_ELEMENT_TYPE.AREA, "BK.BK_AREA_ID", TerminalList, _firstWhere))
    _sb.AppendLine(WhereIn(GROUP_ELEMENT_TYPE.ZONE, "AR.AR_AREA_ID", TerminalList, _firstWhere))
    _sb.AppendLine(WhereIn(GROUP_ELEMENT_TYPE.GROUP, "TG.TG_ELEMENT_ID", TerminalList, _firstWhere))
    _sb.AppendLine("        AND   TE_TYPE = 1                                                    ")
    _sb.AppendLine("        AND   ISNULL(TE_SERVER_ID, 0) =  0                                   ")
    _sb.AppendLine("        AND   TE_BLOCKED = 0                                                 ")
    _sb.AppendLine("        AND   TE_TERMINAL_TYPE = @pTerminalType                              ")

    Try

      Using _db_trx As New WSI.Common.DB_TRX()
        Using _sql_cmd As New SqlClient.SqlCommand(_sb.ToString, _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _sql_cmd.Parameters.Add("@pTerminalType", SqlDbType.Int).Value = WSI.Common.TerminalTypes.SAS_HOST
          Using _sql_da As New SqlDataAdapter(_sql_cmd)
            _db_trx.Fill(_sql_da, _dt)
          End Using
        End Using
      End Using

      Return _dt

    Catch _ex As Exception
      Log.Exception(_ex)

    End Try

    Return _dt

  End Function 'GetTermianlId

  ' PURPOSE: Prepare select where according to GroupElement
  '
  '    - INPUT:
  '         - Group Element
  '         - NameColumnBD
  '         - Terminal List
  '         - firstWhere
  '
  '    - OUTPUT:
  '         - Where
  '
  ' RETURNS:
  '
  Private Function WhereIn(ByVal GroupElement As GROUP_ELEMENT_TYPE, ByVal NameColumnBD As String, ByVal TerminalList As TerminalList, ByRef firstWhere As Boolean) As String

    Dim _whereIn As String = Nothing

    If TerminalList.GetElementsList(GroupElement).Rows.Count > 0 Then
      If Not firstWhere Then
        _whereIn = "            OR  "
      Else
        _whereIn = "          WHERE  "
        firstWhere = False
      End If

      _whereIn = _whereIn & NameColumnBD & " IN ("
      For Each _dr As DataRow In TerminalList.GetElementsList(GroupElement).Rows
        _whereIn += _dr.Item("Id") & ","
      Next
      _whereIn = Strings.Left(_whereIn, Len(_whereIn) - 1)
      _whereIn = _whereIn & ")"
    End If

    Return _whereIn

  End Function 'WhereIn

#End Region ' Private Functions

#Region " Events "

  ' PURPOSE: Handles event of changing values for provider list type combo
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub cmb_provider_list_type_ValueChangedEvent() Handles cmb_provider_list_type.ValueChangedEvent
    Call EnableCheckTerminalListByListType()
  End Sub ' cmb_provider_list_type_ValueChangedEvent

  ' PURPOSE: Handles event of collapsing the check listbox
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub btn_collapse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_collapse.Click

    If Me.m_collapsed And Not Me.m_first_expand Then
      Call InitGroupTree()
      Call InitChecks()
      Me.m_first_expand = True
    End If

    m_collapsed = Not m_collapsed

    Call ExpandOrCollapse()
  End Sub

  ' PURPOSE: Handles event after change the state of a node
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub tr_group_list_AfterCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tr_group_list.AfterCheck

    Dim _node As ItemGroup
    Dim _parent_node As ItemGroup
    Dim _nodes_to_change As TreeNodeCollection
    Dim _update_nodes As Boolean

    _node = CType(e.Node, ItemGroup)
    _parent_node = CType(_node.Parent, ItemGroup)
    _nodes_to_change = Nothing
    _update_nodes = True

    If IsRoot(_node) Then

      If Not _node.ElementExpanded Then
        Call AddChildNodes(_node)
      End If

      _nodes_to_change = _node.Nodes

    Else

      If Not IsRoot(_parent_node) Then
        If tr_group_list.GetNodeState(_parent_node) = uc_treeview_three_state.CHECKED_STATE.CHECKED Then
          Call InsertNodeInElements(_parent_node)
          _update_nodes = False
        ElseIf tr_group_list.GetNodeState(_parent_node) = uc_treeview_three_state.CHECKED_STATE.MIXED Then
          Call UpdateNodesMixed(_parent_node)
        Else
          Call RemoveNodeInElements(_parent_node)
        End If
      End If

      _nodes_to_change = _parent_node.Nodes

    End If

    If _update_nodes Then

      For Each _child As ItemGroup In _nodes_to_change
        If tr_group_list.GetNodeState(_child) = uc_treeview_three_state.CHECKED_STATE.CHECKED Then
          Call InsertNodeInElements(_child)
        ElseIf tr_group_list.GetNodeState(_child) = uc_treeview_three_state.CHECKED_STATE.UNCHECKED Then
          Call RemoveNodeInElements(_child)
        End If
      Next

    End If

    m_dt_elements.AcceptChanges()
    Call RefreshToolTipText()

    RaiseEvent TreeNodeMouseClick(Me, e)

  End Sub ' btn_collapse_Click

  Event TreeNodeMouseClick(uc_terminals_group_filter As uc_terminals_group_filter, e As Windows.Forms.TreeViewEventArgs)

  ' PURPOSE: Update the childs and parents of a node that has changed its state
  '
  '  PARAMS:
  '     - INPUT: 
  '         - Node ItemGroup
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub UpdateNodesMixed(ByVal Node As ItemGroup)
    Dim _child As ItemGroup
    Dim _dr As DataRow

    If IsRoot(Node) Then

      Exit Sub
    End If

    _dr = m_dt_elements.Rows.Find(New Object() {Node.ElementId, GroupConvert(Node.ElementType)})
    If Not IsNothing(_dr) Then
      _dr.Delete()
      m_dt_elements.AcceptChanges()
    End If

    For Each _child In Node.Parent.Nodes
      If tr_group_list.GetNodeState(_child) = uc_treeview_three_state.CHECKED_STATE.CHECKED Then
        Call InsertNodeInElements(_child)
      ElseIf tr_group_list.GetNodeState(_child) = uc_treeview_three_state.CHECKED_STATE.UNCHECKED Then
        Call RemoveNodeInElements(_child)
      End If
    Next

    For Each _child In Node.Parent.Nodes
      If tr_group_list.GetNodeState(_child) = uc_treeview_three_state.CHECKED_STATE.MIXED Then
        Call UpdateNodesMixed(_child.Parent)
      End If
    Next

  End Sub ' UpdateNodesMixed

  ' PURPOSE: Event handler routine when user expands a tree viwer node
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub tr_group_list_AfterExpand(ByVal sender As System.Object, ByVal e As TreeViewEventArgs) Handles tr_group_list.AfterExpand

    Dim _selected_node As ItemGroup

    Try

      _selected_node = CType(e.Node, ItemGroup)

      ' If the user is expanding the selected node and no child nodes haven't been added before to it
      If Not _selected_node.ElementExpanded Then
        Call AddChildNodes(_selected_node)
      End If

    Catch ex As Exception

      Debug.WriteLine(ex.Message)

    End Try

  End Sub ' tr_group_list_AfterExpand

  ' PURPOSE: 4 Event handler routines to control expand tree view nodes on user's click and doubleclick
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub tr_group_list_BeforeCollapse(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewCancelEventArgs) Handles tr_group_list.BeforeCollapse
    If m_expand_manual_flag <> e.Node.Text Then
      e.Cancel = True
      m_expand_flag = e.Node.Text
    Else
      m_expand_manual_flag = ""
    End If
  End Sub

  ' PURPOSE: Handles event of treeview before expand
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub tr_group_list_BeforeExpand(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewCancelEventArgs) Handles tr_group_list.BeforeExpand
    If m_expand_manual_flag <> e.Node.Text Then
      e.Cancel = True
      m_expand_flag = e.Node.Text
    Else
      m_expand_manual_flag = ""
    End If
  End Sub

  ' PURPOSE: Force to Hide the tool tip when we Click the button
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub tr_group_list_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tr_group_list.MouseClick
    tool_tip.Hide(tr_group_list)
  End Sub

  ' PURPOSE: Force to Hide the tool tip when move the mouse.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub tr_group_list_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tr_group_list.MouseMove
    If Not m_cursor_point.Equals(Cursor.Position) Then
      tool_tip.Hide(tr_group_list)
    End If
    m_cursor_point = Cursor.Position
  End Sub

  ' PURPOSE: Handles event of click on the Node
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub tr_group_list_NodeMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles tr_group_list.NodeMouseClick
    If m_expand_flag <> "" Then
      m_expand_manual_flag = m_expand_flag
      m_expand_flag = ""
      If e.Node.IsExpanded Then
        e.Node.Collapse()
      Else
        e.Node.Expand()
      End If
    End If
  End Sub

  ' PURPOSE: Handles event of double click on node. Allows to capture the event on external form.
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub tr_group_list_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles tr_group_list.NodeMouseDoubleClick

    ' Control expand tree view
    m_expand_flag = ""
    m_expand_manual_flag = ""

    RaiseEvent TreeNodeDoubleClick(Me, e)

  End Sub

  ' PURPOSE: Handles event of leave focus. Collapse the combo.
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub uc_terminals_group_filter_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Leave
    If m_display_mode = DisplayMode.CollapsedWithButtons Then
      Me.m_collapsed = True
      Call ExpandOrCollapse()
    End If
  End Sub

  ' PURPOSE: Handles event of resize. Save locations and size of controls in a variables.
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub uc_terminals_group_filter_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize

    Me.m_default_tlp_position = Me.tlp_controls.Location
    Me.m_default_gb_provider_position = Me.gb_provider.Location
    Me.m_default_cmb_provider_position = Me.cmb_provider_list_type.Location
    Me.m_default_tr_grouplist_position = Me.tr_group_list.Location
    Me.m_default_btn_collapse_position = Me.btn_collapse.Location

    Me.m_default_tlp_size = Me.tlp_controls.Size
    Me.m_default_form_size = Me.Size
    Me.m_default_gb_provider_size = Me.gb_provider.Size
    Me.m_default_cmb_provider_size = Me.cmb_provider_list_type.Size
    Me.m_default_tr_grouplist_size = Me.tr_group_list.Size
    Me.m_default_btn_collapse_size = Me.btn_collapse.Size

  End Sub

#End Region ' Events

#Region "Class"
  '
  ' PURPOSE: Extends properties of TreeNodes
  '
  Public Class ItemGroup
    Inherits TreeNode

    Private m_element_type As GROUP_NODE_ELEMENT
    Private m_element_id As Int32
    Private m_is_expanded As Boolean
    Private m_is_enabled As Boolean
    Private m_element_name As String
    Private m_element_terminal_id As String

    Sub New()
      MyBase.New()
    End Sub

    Public Property ElementId() As Int32
      Get
        Return Me.m_element_id
      End Get
      Set(ByVal Value As Int32)
        Me.m_element_id = Value
      End Set
    End Property

    Public Property ElementType() As GROUP_NODE_ELEMENT
      Get
        Return Me.m_element_type
      End Get
      Set(ByVal Value As GROUP_NODE_ELEMENT)
        Me.m_element_type = Value
      End Set
    End Property

    Public Property ElementName() As String
      Get
        Return Me.m_element_name
      End Get
      Set(ByVal Value As String)
        Me.m_element_name = Value
        Me.Text = GetTypeTitle(m_element_type, True) & Me.m_element_name
      End Set
    End Property

    Public Property ElementExpanded() As Boolean
      Get
        Return m_is_expanded
      End Get
      Set(ByVal value As Boolean)
        m_is_expanded = value
      End Set
    End Property

    Public Property ElementEnabled() As Boolean
      Get
        Return m_is_enabled
      End Get
      Set(ByVal value As Boolean)
        m_is_enabled = value
      End Set
    End Property

    Public Property ElementTerminalId() As Int32
      Get
        Return m_element_terminal_id
      End Get
      Set(ByVal value As Int32)
        m_element_terminal_id = value
      End Set
    End Property
    Public Overloads Function Clone() As Object
      Return MyBase.Clone()
    End Function

  End Class

#End Region

End Class ' uc_provider_filter

