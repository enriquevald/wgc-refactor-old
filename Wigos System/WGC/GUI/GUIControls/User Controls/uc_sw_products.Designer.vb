<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_sw_products
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.opt_all_types = New System.Windows.Forms.RadioButton
    Me.opt_several_types = New System.Windows.Forms.RadioButton
    Me.gb_sw = New System.Windows.Forms.GroupBox
    Me.dg_filter = New GUI_Controls.uc_grid
    Me.gb_sw.SuspendLayout()
    Me.SuspendLayout()
    '
    'opt_all_types
    '
    Me.opt_all_types.Location = New System.Drawing.Point(6, 44)
    Me.opt_all_types.Name = "opt_all_types"
    Me.opt_all_types.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_types.TabIndex = 5
    Me.opt_all_types.Text = "xAll"
    '
    'opt_several_types
    '
    Me.opt_several_types.AutoSize = True
    Me.opt_several_types.Location = New System.Drawing.Point(6, 14)
    Me.opt_several_types.Name = "opt_several_types"
    Me.opt_several_types.Size = New System.Drawing.Size(66, 17)
    Me.opt_several_types.TabIndex = 4
    Me.opt_several_types.Text = "xSeveral"
    '
    'gb_sw
    '
    Me.gb_sw.Controls.Add(Me.opt_several_types)
    Me.gb_sw.Controls.Add(Me.dg_filter)
    Me.gb_sw.Controls.Add(Me.opt_all_types)
    Me.gb_sw.Location = New System.Drawing.Point(3, 3)
    Me.gb_sw.Name = "gb_sw"
    Me.gb_sw.Size = New System.Drawing.Size(297, 235)
    Me.gb_sw.TabIndex = 6
    Me.gb_sw.TabStop = False
    Me.gb_sw.Text = "gb_sw"
    '
    'dg_filter
    '
    Me.dg_filter.CurrentCol = -1
    Me.dg_filter.CurrentRow = -1
    Me.dg_filter.Location = New System.Drawing.Point(76, 14)
    Me.dg_filter.Name = "dg_filter"
    Me.dg_filter.PanelRightVisible = False
    Me.dg_filter.Redraw = True
    Me.dg_filter.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_filter.Size = New System.Drawing.Size(215, 213)
    Me.dg_filter.Sortable = False
    Me.dg_filter.SortAscending = True
    Me.dg_filter.SortByCol = 0
    Me.dg_filter.TabIndex = 3
    Me.dg_filter.ToolTipped = True
    Me.dg_filter.TopRow = -2
    '
    'uc_sw_products
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.gb_sw)
    Me.Name = "uc_sw_products"
    Me.Size = New System.Drawing.Size(308, 240)
    Me.gb_sw.ResumeLayout(False)
    Me.gb_sw.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dg_filter As GUI_Controls.uc_grid
  Public WithEvents opt_all_types As System.Windows.Forms.RadioButton
  Public WithEvents opt_several_types As System.Windows.Forms.RadioButton
  Friend WithEvents gb_sw As System.Windows.Forms.GroupBox

End Class
