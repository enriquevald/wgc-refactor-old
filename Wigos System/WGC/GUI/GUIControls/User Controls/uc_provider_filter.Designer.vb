<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_provider_filter
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_provider = New System.Windows.Forms.GroupBox
    Me.btn_collapse = New System.Windows.Forms.Button
    Me.chklb_provider_list = New System.Windows.Forms.CheckedListBox
    Me.cmb_provider_list_type = New GUI_Controls.uc_combo
    Me.gb_provider.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_provider
    '
    Me.gb_provider.Controls.Add(Me.btn_collapse)
    Me.gb_provider.Controls.Add(Me.chklb_provider_list)
    Me.gb_provider.Controls.Add(Me.cmb_provider_list_type)
    Me.gb_provider.Dock = System.Windows.Forms.DockStyle.Fill
    Me.gb_provider.Location = New System.Drawing.Point(0, 0)
    Me.gb_provider.Name = "gb_provider"
    Me.gb_provider.Size = New System.Drawing.Size(283, 362)
    Me.gb_provider.TabIndex = 0
    Me.gb_provider.TabStop = False
    Me.gb_provider.Text = "xProviders"
    '
    'btn_collapse
    '
    Me.btn_collapse.Location = New System.Drawing.Point(250, 17)
    Me.btn_collapse.Name = "btn_collapse"
    Me.btn_collapse.Size = New System.Drawing.Size(26, 22)
    Me.btn_collapse.TabIndex = 2
    Me.btn_collapse.Text = "+"
    Me.btn_collapse.UseVisualStyleBackColor = True
    '
    'chklb_provider_list
    '
    Me.chklb_provider_list.CheckOnClick = True
    Me.chklb_provider_list.FormattingEnabled = True
    Me.chklb_provider_list.Location = New System.Drawing.Point(6, 48)
    Me.chklb_provider_list.Name = "chklb_provider_list"
    Me.chklb_provider_list.Size = New System.Drawing.Size(271, 319)
    Me.chklb_provider_list.TabIndex = 1
    '
    'cmb_provider_list_type
    '
    Me.cmb_provider_list_type.IsReadOnly = False
    Me.cmb_provider_list_type.Location = New System.Drawing.Point(4, 16)
    Me.cmb_provider_list_type.Name = "cmb_provider_list_type"
    Me.cmb_provider_list_type.SelectedIndex = -1
    Me.cmb_provider_list_type.Size = New System.Drawing.Size(241, 24)
    Me.cmb_provider_list_type.SufixText = "Sufix Text"
    Me.cmb_provider_list_type.SufixTextVisible = True
    Me.cmb_provider_list_type.TabIndex = 0
    Me.cmb_provider_list_type.TextWidth = 0
    '
    'uc_provider_filter
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
    Me.Controls.Add(Me.gb_provider)
    Me.Name = "uc_provider_filter"
    Me.Size = New System.Drawing.Size(283, 362)
    Me.gb_provider.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_provider As System.Windows.Forms.GroupBox
  Friend WithEvents chklb_provider_list As System.Windows.Forms.CheckedListBox
  Friend WithEvents cmb_provider_list_type As GUI_Controls.uc_combo
  Friend WithEvents btn_collapse As System.Windows.Forms.Button

End Class
