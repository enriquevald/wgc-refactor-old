'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_provider
' DESCRIPTION:   Control for providers and terminals selection 
' AUTHOR:        Santi L�pez
' CREATION DATE: 26-MAY-2010
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 26-MAY-2010 SLE    First Release
' 27-OCT-2011 RCI    Added function to get the IDs for the providers and terminals selected
' 28-OCT-2011 RCI    Added an event to raise when any value changes in this control: ValueChangedEvent()
' 25-APR-2012 JCM    Added a new filtering type: ONLY_ONE_TERMINAL, this can only check a unique Machine/Terminal
' 02-MAY-2012 JCM    Added new Terminal field: id_floor if has a value
' 30-APR-2012 JCM    Added customized Tool Tip for Terminals
' 03-MAY-2012 JCM    In ONLY_ONE_TERMINAL when the control is reset the provider check flickers and it doesn't have to.
' 24-MAR-2015 JML    672 - lOGRAND: SAS09 - Asset number
' 14-MAY-2015 DCS    Fixed Bug #WIG-2351: Terminals: Sending commands to terminals removed
' 30-DEC-2015 DDS    PBI 7885. Floor Dual Currency
' 21-APR-2016 ETP    Fixed Bug 12198: Added GetRowFilter for Monitor mode.  
' 18-OCT-2016 RGR    Bug 13086: TITO - Create stackers - unhandled exception
' 27-OCT-2016 JML    Fixed Bug 19727: Response time out, when consulting counters history
' 02-NOV-2016 ETP    Fixed Bug 19942: BBDD error when selecting terminals in type mode
' 29-OCT-2017 LTC    [WIGOS-6134] Multisite Terminal meter adjustment
' 06-JUN-2018 AGS    Bug 32919:WIGOS-12570 B�veda: Dos recaudaciones pendientes de la misma m�quina: falla
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.ComponentModel
Imports System.Text
Imports System.Drawing

Public Class uc_provider

#Region " Public Event "

  Dim m_terminals_massive_search As String
  Dim m_terminals_massive_search_to_edit As String

  Public Event ValueChangedEvent()

#End Region ' Public Event

#Region "Constants"
  Public Enum UC_FILTER_TYPE
    NONE = 0                'Usually function :  Can mark by Provider, Terminal with multiple selection
    ONLY_PROVIDERS = 1      'Only Providers   :  Only allow multiple providers selection, not terminals
    ONLY_ONE_TERMINAL = 2   'Only One Terminal:  Only can check a unique Terminal
    ALL_ACTIVE = 3
  End Enum
  Private Const SELECT_TERMINALS As String = "SELECT TLC_MASTER_ID, TLC_BASE_NAME FROM TERMINALS_LAST_CHANGED INNER JOIN TERMINALS ON TE_TERMINAL_ID = TLC_TERMINAL_ID "
  Private Const SELECT_TERMINALS_MULTISITE As String = "SELECT CAST(TLC_MASTER_ID AS VARCHAR(10)) +'#'+ CAST(TE_SITE_ID AS VARCHAR(10)), TLC_BASE_NAME FROM TERMINALS_LAST_CHANGED INNER JOIN TERMINALS ON TE_TERMINAL_ID = TLC_TERMINAL_ID AND TLC_SITE_ID = TE_SITE_ID "
  Private Const WHERE_TERMINALS As String = " WHERE TE_TYPE = '1' "
  Private Const ORDER_BY_TERMINALS As String = " ORDER BY TLC_BASE_NAME"

  Private Const SQL_COLUMN_CHK As Integer = 0
  Private Const SQL_COLUMN_TYPE As Integer = 1
  Private Const SQL_COLUMN_PROVIDER As Integer = 2
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 3
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 4
  Private Const SQL_COLUMN_TERMINAL_FLOOR_ID As Integer = 5

  Private Const GRID_2_COLUMN_ID As Integer = 0
  Private Const GRID_2_COLUMN_CHECKED As Integer = 1
  Private Const GRID_2_COLUMN_DESC As Integer = 2
  Private Const GRID_2_COLUMN_TYPE As Integer = 3
  Private Const GRID_2_COLUMN_CHK As Integer = 4
  Private Const GRID_2_COLUMN_TOOLTIP As Integer = 5

  Private Const GRID_2_COLUMNS As Integer = 6
  Private Const GRID_2_HEADER_ROWS As Integer = 0

  Private Const GRID_2_ROW_TYPE_HEADER = 0
  Private Const GRID_2_ROW_TYPE_DATA = 1

  Private Const GRID_2_TAB As String = "     "
  Private Const ALLOWS_EXPAND_SYMBOL As String = "[+] "
  Private Const ALLOWS_COLLAPSE_SYMBOL As String = "[-]  "


#End Region ' Constants

#Region " Members "

  Private Structure TYPE_PROVIDER_ITEM
    Dim name As String
    Dim idx As Integer
    Dim num_terminals As Integer
    Dim list_terminals() As TYPE_TERMINAL_ITEM
    Dim checked As String
  End Structure

  Private Structure TYPE_TERMINAL_ITEM
    Dim name As String
    Dim id As Integer
    Dim idx_provider As Integer
    Dim id_floor As String
    Dim checked As Boolean
  End Structure

  Private list_providers() As TYPE_PROVIDER_ITEM

  ' Used to avoid data-changed recursive calls when re-painting a data grid
  Private m_refreshing_grid As Boolean

  Private m_terminal_types() As Integer = Nothing

  Private m_search_type As UC_FILTER_TYPE

  Private m_filter_by_currency As Boolean
  Private m_floor_currency_enabled As Boolean
  Private m_selected_currency As String
  Private m_national_currency As String
  Private m_terminal_list_has_values As Boolean
  Private m_block_terminals As Boolean = False

  'LTC  29-OCT-2017
  Private m_site As String = Nothing

  ' AMF 20-NOV-2017
  Private m_is_multisite As Boolean = False

#End Region ' Members

#Region " Public properties"

  <DefaultValue(True), Description("At runtime AND's with GP FloorDualCurrency.Enabled")> _
  Public Property FilterByCurrency() As Boolean

    Get
      Return m_filter_by_currency
    End Get

    Set(value As Boolean)
      m_filter_by_currency = value
      Me.lbl_currency.Visible = value
      Me.cmb_currencies.Visible = value
    End Set

  End Property

  <Browsable(False)>
  Public ReadOnly Property AllSelectedChecked() As Boolean 'Encapsulates behavior of 'All' terminals selected only when terminal currency can be ignored
    Get
      Return Me.opt_all_types.Checked And Not UseCurrency()
    End Get
  End Property

  <Browsable(False)>
  Public ReadOnly Property OneTerminalChecked() As Boolean 'Encapsulates behavior of 'One terminal' selected
    Get
      Return Me.opt_one_terminal.Checked
    End Get
  End Property

  <Browsable(False)>
  Public ReadOnly Property SeveralTerminalsChecked() As Boolean 'Encapsulates behavior of 'Several terminals' selected
    Get
      Return Me.opt_several_types.Checked
    End Get
  End Property

  <Browsable(False)>
  Public ReadOnly Property OneTerminalSelectedText() As String
    Get
      Return Me.cmb_terminal.TextValue
    End Get
  End Property

  <Browsable(False)>
  Public Property TerminalListHasValues() As Boolean
    Get
      Return Me.m_terminal_list_has_values
    End Get
    Set(value As Boolean)
      Me.m_terminal_list_has_values = value
    End Set
  End Property

  Public Property ShowMassiveSearch() As Boolean
    Get
      Return Me.btn_massive_search.Visible
    End Get

    Set(ByVal value As Boolean)
      btn_massive_search.Visible = value
    End Set
  End Property

  Public Property MassiveSearchNumbers() As String
    Get
      Return m_terminals_massive_search
    End Get
    Set(ByVal value As String)
      m_terminals_massive_search = value
    End Set
  End Property ' MassiveSearchNumbers

  Public Property MassiveSearchNumbersToEdit() As String
    Get
      Return m_terminals_massive_search_to_edit
    End Get
    Set(ByVal value As String)
      m_terminals_massive_search_to_edit = value
    End Set
  End Property ' MassiveSearchNumbersToEdit


#End Region ' Public properties

#Region " Public Functions "

  Public Sub New()
    m_filter_by_currency = True ' Existing components didn't have that property and will have a False on design time
    ' This call is required by the Windows Form Designer.
    InitializeComponent()

  End Sub

  Public Function GetTerminalName(ByVal Id As Integer) As String

    For Each _provider As TYPE_PROVIDER_ITEM In list_providers
      For Each _terminal As TYPE_TERMINAL_ITEM In _provider.list_terminals
        If _terminal.id.Equals(Id) Then
          Return _terminal.name
        End If
      Next
    Next

    Return String.Empty

  End Function


  Public Function GetProviderIdListSelected(Optional ByVal returnWithSQLStatements As Boolean = True) As String
    Dim idx_provider As Integer
    Dim idx_terminal As Integer
    Dim list_providers_where As String = ""
    Dim list_terminals_where As String = ""
    Dim list_str_where As String = ""
    Dim list_str_select As String = ""
    Dim last_provider As String = ""
    Dim is_null As Boolean = False
    Dim _list_str_type As String = ""
    Dim _terminal_value As String = ""

    If (returnWithSQLStatements = False) Then

      Return GetTerminalTypes()
    End If

    list_str_select = " (SELECT TE_TERMINAL_ID FROM TERMINALS "
    _list_str_type = " TE_TERMINAL_TYPE IN ( " + GetTerminalTypes() + ") "

    If m_search_type = UC_FILTER_TYPE.ALL_ACTIVE Then
      _list_str_type += " AND TE_STATUS = 0 " ' WSI.Common.TerminalStatus.ACTIVE
    End If

    _list_str_type += Me.GetQueryFilteredByCurrency()

    If Me.opt_one_terminal.Checked And m_search_type <> UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then

      If m_is_multisite Then
        ' MultiSite: TLC_MASTER_ID + '.' + TE_SITE_ID. Get only MasterId
        _terminal_value = Me.cmb_terminal.ValueString.Split("#")(0)
      Else
        _terminal_value = Me.cmb_terminal.Value
      End If

      If m_search_type = UC_FILTER_TYPE.ALL_ACTIVE Then
        list_str_where = list_str_select & " WHERE TE_MASTER_ID = " & _terminal_value & " AND  " & _list_str_type & " ) "
      Else
        list_str_where = list_str_select & " WHERE TE_MASTER_ID = " & _terminal_value & " ) "
      End If

      Return list_str_where
    End If

    If (Me.opt_all_types.Checked Or list_providers Is Nothing) And m_search_type <> UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then
      list_str_where = list_str_select & " WHERE " & _list_str_type & ")"

      Return list_str_where
    End If

    'RGR 18-OCT-2016
    If Not IsNothing(list_providers) Then
      For idx_provider = 0 To list_providers.Length - 1
        If list_providers(idx_provider).checked = uc_grid.GRID_CHK_CHECKED Then
          last_provider = list_providers(idx_provider).name
          If last_provider = GLB_NLS_GUI_STATISTICS.GetString(213) Then
            is_null = True
          Else
            If list_providers_where.Length = 0 Then
              list_providers_where = last_provider.Replace("'", "''")
            Else
              list_providers_where = list_providers_where & "', '" & last_provider.Replace("'", "''")
            End If
          End If
        Else
          If list_providers(idx_provider).checked = uc_grid.GRID_CHK_CHECKED_DISABLED Then
            For idx_terminal = 0 To list_providers(idx_provider).num_terminals - 1
              If list_providers(idx_provider).list_terminals(idx_terminal).checked Then
                If list_terminals_where.Length = 0 Then
                  list_terminals_where = list_providers(idx_provider).list_terminals(idx_terminal).id
                Else
                  list_terminals_where = list_terminals_where & ", " & list_providers(idx_provider).list_terminals(idx_terminal).id
                End If
              End If
            Next
          End If
        End If
      Next
    End If

    If last_provider <> "" Then
      list_str_where = list_str_where & " (TE_PROVIDER_ID IN ('" & list_providers_where & "') "
      If is_null = True Then
        If list_str_where = "" Then
          list_str_where = " (TE_PROVIDER_ID is null"
        Else
          list_str_where = list_str_where & "OR TE_PROVIDER_ID is null) "
        End If
      Else
        list_str_where = list_str_where & ") "
      End If
      If list_terminals_where <> "" Then
        list_str_where = list_str_where & " OR TE_MASTER_ID IN (" & list_terminals_where & ") "
      End If
    Else
      If list_terminals_where <> "" Then
        list_str_where = list_str_where & " TE_MASTER_ID IN (" & list_terminals_where & ") "
      End If
    End If

    If list_str_where <> "" Then
      list_str_where = list_str_select & " WHERE (" & list_str_where & ") AND " & _list_str_type & ")"
    Else
      'If not items selected and filter type is Only_One_Terminal, return NONE terminal (condition 1 = 2)
      If m_search_type = UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then
        list_str_where = list_str_select & " WHERE " & "1 = 2" & ")"
      Else
        list_str_where = list_str_select & " WHERE " & _list_str_type & ")"
      End If
    End If

    Return list_str_where

  End Function 'GetProviderIdListSelected

  Public Function GetMasterIdListSelected(Optional ByVal returnWithSQLStatements As Boolean = True) As String
    Dim _idx_provider As Integer
    Dim _idx_terminal As Integer
    Dim _list_providers_where As String = ""
    Dim _list_terminals_where As String = ""
    Dim _list_str_where As String = ""
    Dim _list_str_select As String = ""
    Dim _last_provider As String = ""
    Dim _is_null As Boolean = False
    Dim _list_str_type As String = ""


    _list_str_where = " 1 = 1 "

    If (returnWithSQLStatements = False) Then

      _list_str_where = _list_str_where.Substring(4, _list_str_where.Length - 4)
      Return GetTerminalTypes()
    End If

    _list_str_select = " "
    _list_str_type = " TE_TERMINAL_TYPE IN ( " + GetTerminalTypes() + ") "

    If m_search_type = UC_FILTER_TYPE.ALL_ACTIVE Then
      _list_str_type += " AND TE_STATUS = 0 " ' WSI.Common.TerminalStatus.ACTIVE
    End If

    _list_str_type += Me.GetQueryFilteredByCurrency()

    If Me.opt_one_terminal.Checked And m_search_type <> UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then

      If m_search_type = UC_FILTER_TYPE.ALL_ACTIVE Then
        _list_str_where = _list_str_select & " AND TE_MASTER_ID = " & Me.cmb_terminal.Value & " " & _list_str_type & " "
      Else
        If Not m_is_multisite Then
          _list_str_where = _list_str_select & " AND TE_MASTER_ID = " & Me.cmb_terminal.Value & " "
        Else
          _list_str_where = _list_str_select & " AND TE_MASTER_ID = " & Me.cmb_terminal.ValueString.Split("#")(0) & " "
        End If

      End If

      _list_str_where = _list_str_where.Substring(5, _list_str_where.Length - 5)
      Return _list_str_where
    End If

    If (Me.opt_all_types.Checked Or list_providers Is Nothing) And m_search_type <> UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then
      _list_str_where = _list_str_select & " AND " & _list_str_type & " "

      _list_str_where = _list_str_where.Substring(5, _list_str_where.Length - 5)
      Return _list_str_where
    End If

    For _idx_provider = 0 To list_providers.Length - 1
      If list_providers(_idx_provider).checked = uc_grid.GRID_CHK_CHECKED Then
        _last_provider = list_providers(_idx_provider).name
        If _last_provider = GLB_NLS_GUI_STATISTICS.GetString(213) Then
          _is_null = True
        Else
          If _list_providers_where.Length = 0 Then
            _list_providers_where = _last_provider.Replace("'", "''")
          Else
            _list_providers_where = _list_providers_where & "', '" & _last_provider.Replace("'", "''")
          End If
        End If
      Else
        If list_providers(_idx_provider).checked = uc_grid.GRID_CHK_CHECKED_DISABLED Then
          For _idx_terminal = 0 To list_providers(_idx_provider).num_terminals - 1
            If list_providers(_idx_provider).list_terminals(_idx_terminal).checked Then
              If _list_terminals_where.Length = 0 Then
                _list_terminals_where = list_providers(_idx_provider).list_terminals(_idx_terminal).id
              Else
                _list_terminals_where = _list_terminals_where & ", " & list_providers(_idx_provider).list_terminals(_idx_terminal).id
              End If
            End If
          Next
        End If
      End If
    Next

    If _last_provider <> "" Then
      _list_str_where = _list_str_where & " AND TE_PROVIDER_ID IN ('" & _list_providers_where & "') "
      If _is_null = True Then
        If _list_str_where = "" Then
          _list_str_where = " (TE_PROVIDER_ID is null"
        Else
          _list_str_where = _list_str_where & "OR TE_PROVIDER_ID is null) "
        End If

      End If
      If _list_terminals_where <> "" Then
        _list_str_where = _list_str_where & " OR TE_MASTER_ID IN (" & _list_terminals_where & ") "
      End If
    Else
      If _list_terminals_where <> "" Then
        _list_str_where = _list_str_where & " AND TE_MASTER_ID IN (" & _list_terminals_where & ") "
      End If
    End If

    If _list_str_where <> "" Then
      _list_str_where = _list_str_select & " AND (" & _list_str_where & ") AND " & _list_str_type & " "
    Else
      'If not items selected and filter type is Only_One_Terminal, return NONE terminal (condition 1 = 2)
      If m_search_type = UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then
        _list_str_where = _list_str_select & " AND " & "1 = 2" & " "
      Else
        _list_str_where = _list_str_select & " AND " & _list_str_type & " "
      End If
    End If

    _list_str_where = _list_str_where.Substring(5, _list_str_where.Length - 5)
    Return _list_str_where

  End Function 'GetMasterIdListSelected


  Public Function GetProviderIdListSelectedWithVendor(ByVal vendor As String, Optional ByVal returnWithSQLStatements As Boolean = True) As String
    Dim idx_provider As Integer
    Dim idx_terminal As Integer
    Dim list_providers_where As String = ""
    Dim list_terminals_where As String = ""
    Dim list_str_where As String = ""
    Dim list_str_select As String = ""
    Dim last_provider As String = ""
    Dim is_null As Boolean = False
    Dim _list_str_type As String = ""

    If (returnWithSQLStatements = False) Then

      Return GetTerminalTypes()
    End If

    list_str_select = " (SELECT TE_TERMINAL_ID FROM TERMINALS "
    _list_str_type = " TE_TERMINAL_TYPE IN ( " + GetTerminalTypes() + ") "

    If m_search_type = UC_FILTER_TYPE.ALL_ACTIVE Then
      _list_str_type += " AND TE_STATUS = 0 " ' WSI.Common.TerminalStatus.ACTIVE
    End If

    _list_str_type += Me.GetQueryFilteredByCurrency()

    If Me.opt_one_terminal.Checked And m_search_type <> UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then

      If m_search_type = UC_FILTER_TYPE.ALL_ACTIVE Then
        list_str_where = list_str_select & " WHERE TE_VENDOR_ID LIKE '%" & vendor & "%'" & " AND  TE_MASTER_ID = " & Me.cmb_terminal.Value & " AND  " & _list_str_type & " ) "
      Else
        list_str_where = list_str_select & " WHERE TE_VENDOR_ID LIKE '%" & vendor & "%'" & " AND  TE_MASTER_ID = " & Me.cmb_terminal.Value & " ) "
      End If

      Return list_str_where
    End If

    If (Me.opt_all_types.Checked Or list_providers Is Nothing) And m_search_type <> UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then
      list_str_where = list_str_select & " WHERE TE_VENDOR_ID LIKE '%" & vendor & "%'" & " AND " & _list_str_type & ")"

      Return list_str_where
    End If

    For idx_provider = 0 To list_providers.Length - 1
      If list_providers(idx_provider).checked = uc_grid.GRID_CHK_CHECKED Then
        last_provider = list_providers(idx_provider).name
        If last_provider = GLB_NLS_GUI_STATISTICS.GetString(213) Then
          is_null = True
        Else
          If list_providers_where.Length = 0 Then
            list_providers_where = last_provider.Replace("'", "''")
          Else
            list_providers_where = list_providers_where & "', '" & last_provider.Replace("'", "''")
          End If
        End If
      Else
        If list_providers(idx_provider).checked = uc_grid.GRID_CHK_CHECKED_DISABLED Then
          For idx_terminal = 0 To list_providers(idx_provider).num_terminals - 1
            If list_providers(idx_provider).list_terminals(idx_terminal).checked Then
              If list_terminals_where.Length = 0 Then
                list_terminals_where = list_providers(idx_provider).list_terminals(idx_terminal).id
              Else
                list_terminals_where = list_terminals_where & ", " & list_providers(idx_provider).list_terminals(idx_terminal).id
              End If
            End If
          Next
        End If
      End If
    Next

    If last_provider <> "" Then
      list_str_where = list_str_where & " (TE_PROVIDER_ID IN ('" & list_providers_where & "') "
      If is_null = True Then
        If list_str_where = "" Then
          list_str_where = " (TE_PROVIDER_ID is null"
        Else
          list_str_where = list_str_where & "OR TE_PROVIDER_ID is null) "
        End If
      Else
        list_str_where = list_str_where & ") "
      End If
      If list_terminals_where <> "" Then
        list_str_where = list_str_where & " OR TE_MASTER_ID IN (" & list_terminals_where & ") "
      End If
    Else
      If list_terminals_where <> "" Then
        list_str_where = list_str_where & " TE_MASTER_ID IN (" & list_terminals_where & ") "
      End If
    End If

    If list_str_where <> "" Then
      list_str_where = list_str_select & " WHERE TE_VENDOR_ID LIKE '%" & vendor & "%'" & " AND  (" & list_str_where & ") AND " & _list_str_type & ")"
    Else
      'If not items selected and filter type is Only_One_Terminal, return NONE terminal (condition 1 = 2)
      If m_search_type = UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then
        list_str_where = list_str_select & " WHERE TE_VENDOR_ID LIKE '%" & vendor & "%'" & " AND " & "1 = 2" & ")"
      Else
        list_str_where = list_str_select & " WHERE TE_VENDOR_ID LIKE '%" & vendor & "%'" & " AND " & _list_str_type & ")"
      End If
    End If

    Return list_str_where

  End Function 'GetProviderIdListSelected

  ' PURPOSE: Get selected Terminal Id list
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Long()
  Public Function GetTerminalIdListSelected(ByVal Filter As UC_FILTER_TYPE) As Long()

    m_search_type = Filter

    Return GetTerminalIdListSelected()

  End Function

  Public Function GetTerminalIdListSelected() As Long()

    Dim list_terminal_id() As Long = Nothing
    Dim _idx As Integer
    Dim _tb_tprovider As DataTable = Nothing
    Dim _r_provider As DataRow

    _idx = 0

    _tb_tprovider = GUI_GetTableUsingSQL(GetProviderIdListSelected(), 5000)

    If _tb_tprovider.Rows.Count = 0 Then
      Me.TerminalListHasValues = UseCurrency()
      ReDim Preserve list_terminal_id(0)
      list_terminal_id(0) = -1
      Return list_terminal_id
    End If

    Me.TerminalListHasValues = True
    For Each _r_provider In _tb_tprovider.Rows
      ReDim Preserve list_terminal_id(0 To _idx)
      list_terminal_id(_idx) = _r_provider.Item(0)
      _idx = _idx + 1
    Next

    Return list_terminal_id

  End Function ' GetTerminalIdListSelected

  ' PURPOSE: Get terminal id coma separeted.
  '
  '  PARAMS:
  '     - INPUT:

  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '  id_list coma separeted.
  Public Function GetTerminalRowFilter() As String
    Dim _row_filter As StringBuilder
    Dim _list_terminal_id() As Long = Nothing
    _row_filter = New StringBuilder()

    _list_terminal_id = GetTerminalIdListSelected()

    For _aux As Long = 0 To _list_terminal_id.Length - 1
      If (_aux <> 0) Then

        _row_filter.Append(",")
      End If
      _row_filter.AppendLine(_list_terminal_id(_aux).ToString())

    Next

    Return _row_filter.ToString()
  End Function

  Public Sub BlockTerminals(ByVal status As Boolean)


    Dim idx_row As Integer
    Dim idx_provider As Integer

    m_block_terminals = status

    Me.dg_filter.Redraw = False

    SetProviderList()

    For idx_row = 0 To dg_filter.NumRows - 1
      dg_filter.Cell(idx_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next

    For idx_provider = 0 To list_providers.Length - 1
      list_providers(idx_provider).checked = uc_grid.GRID_CHK_UNCHECKED
      ChangeTerminalCheck(idx_provider, uc_grid.GRID_CHK_UNCHECKED)
    Next

    Me.dg_filter.Redraw = True

    Return
  End Sub

  ' PURPOSE: Set selected Terminal Id list
  '
  '  PARAMS:
  '     - INPUT:
  '           - Terminals Id List
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  Public Sub SetTerminalIdListSelected(ByVal IdTerminals() As Long)

    ' only one terminal
    Dim _idx_pr As Integer
    Dim _idx_te As Integer
    Dim _checked_all_terminals As Boolean     ' If all terminals of the same provider are selected

    For _idx_pr = 0 To list_providers.Length - 1

      _checked_all_terminals = False

      For _idx_te = 0 To list_providers(_idx_pr).list_terminals.Length - 1

        If list_providers(_idx_pr).list_terminals(_idx_te).id = IdTerminals(0) Then

          list_providers(_idx_pr).list_terminals(_idx_te).checked = True
          list_providers(_idx_pr).checked = uc_grid.GRID_CHK_CHECKED_DISABLED

          If m_search_type = UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then
            Return
          End If

          _checked_all_terminals = True

          Continue For
        End If

        _checked_all_terminals = False
      Next

      If _checked_all_terminals Then
        list_providers(_idx_pr).checked = uc_grid.GRID_CHK_CHECKED
      End If

    Next
  End Sub ' GetTerminalIdListSelected

  ' PURPOSE: Return a String containing the list of the terminal types initialized at Init time.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Public Function GetTerminalTypes() As String
    Dim _list_str_type As String
    Dim _idx As Integer

    _list_str_type = m_terminal_types(0).ToString()

    For _idx = 1 To m_terminal_types.Length - 1
      _list_str_type += ", " + m_terminal_types(_idx).ToString().Replace("'", "''")
      'list_providers_where = last_provider.Replace("'", "''")
    Next

    Return _list_str_type
  End Function ' GetTerminalTypes

  ' PURPOSE: Get only the selected Provider Id list (not terminals)
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String()
  Public Function GetOnlyProviderIdListSelected() As String()

    Dim list_provider_id As String() = Nothing
    Dim _idx As Integer

    _idx = 0

    For Each _provider As TYPE_PROVIDER_ITEM In list_providers
      If _provider.checked = uc_grid.GRID_CHK_CHECKED Or Me.opt_all_types.Checked Then
        ReDim Preserve list_provider_id(0 To _idx)
        list_provider_id(_idx) = DataTable_FilterValue(_provider.name)
        _idx = _idx + 1
      End If
    Next

    Return list_provider_id
  End Function ' GetOnlyProviderIdListSelected

  ' PURPOSE: Get the selected Provider Id list (when all terminals of the provider are selected)
  '          and the selected individual Terminal Id list (when NO all terminals of the provider are selected)
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - ProviderList
  '           - TerminalList
  '
  ' RETURNS:
  '     - None
  Private Sub GetProviderAndTerminalIdListSelected(ByRef ProviderList As String(), ByRef TerminalList As Integer())

    ' TODO: JML 1-APR-2015
    '   ES: Esta funcion se llama en las pantallas de seleccion y edicion de areas e islas.
    '       Estas pantallas no estan en uso.
    '   EN: This function is called on the windows of selection and editing areas and islands.
    '       These windows are not in use


    Dim _idx_prov As Integer
    Dim _idx_term As Integer
    Dim _idx_aux As Integer

    ProviderList = Nothing
    TerminalList = Nothing
    _idx_prov = 0
    _idx_term = 0


    'JCM 25-APR-2012 If Filter Type Only_One_Terminal, 
    '                then get Id_Terminal from Grid, else from Cbo_Terminal
    If m_search_type = UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then
      For _idx_aux = 0 To dg_filter.NumRows - 1
        If dg_filter.Cell(_idx_aux, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then

          ReDim Preserve TerminalList(0 To 0)
          TerminalList(0) = dg_filter.Cell(_idx_aux, GRID_2_COLUMN_ID).Value

          Return
        End If
      Next

      Return
    End If


    If Me.opt_one_terminal.Checked Then
      ReDim Preserve TerminalList(0 To 0)
      TerminalList(0) = Me.cmb_terminal.Value

      Return
    End If

    For Each _provider As TYPE_PROVIDER_ITEM In list_providers

      If _provider.checked = uc_grid.GRID_CHK_CHECKED_DISABLED Then

        ' Not all terminals are selected, must include terminals of the provider.
        For Each _terminal As TYPE_TERMINAL_ITEM In _provider.list_terminals
          If _terminal.checked Then
            ReDim Preserve TerminalList(0 To _idx_term)
            TerminalList(_idx_term) = _terminal.id
            _idx_term = _idx_term + 1
          End If
        Next

      ElseIf _provider.checked = uc_grid.GRID_CHK_CHECKED Or Me.opt_all_types.Checked Then
        ' Include the provider.
        ReDim Preserve ProviderList(0 To _idx_prov)
        ProviderList(_idx_prov) = _provider.name
        _idx_prov = _idx_prov + 1
      End If

    Next

  End Sub ' GetProviderAndTerminalIdListSelected

  ' PURPOSE: Init routine of the control
  '
  '  PARAMS:
  '     - INPUT:
  '           - TerminalTypes
  '           - Optional OnlyProviders
  '           - Optional Site
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  'LTC  29-OCT-2017
  Public Sub Init(ByVal TerminalTypes() As Integer, _
                  Optional ByVal SearchType As UC_FILTER_TYPE = UC_FILTER_TYPE.NONE, _
                  Optional ByVal Site As String = Nothing, _
                  Optional ByVal IsMultiSite As Boolean = False)

    m_search_type = SearchType
    m_site = Site
    m_is_multisite = IsMultiSite

    Call InitControls(TerminalTypes)
  End Sub ' Init


  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub SetDefaultValues()

    Dim idx_row As Integer
    Dim idx_provider As Integer

    If list_providers Is Nothing Then
      Return
    End If

    ' SLE 31-MAY-2010
    Me.dg_filter.Redraw = False

    SetProviderList()
    Me.opt_all_types.Checked = True

    If m_search_type = UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then
      For idx_row = 0 To dg_filter.NumRows - 1
        dg_filter.Cell(idx_row, GRID_2_COLUMN_CHECKED).Picture = Nothing
      Next
    Else
      For idx_row = 0 To dg_filter.NumRows - 1
        dg_filter.Cell(idx_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      Next
    End If


    For idx_provider = 0 To list_providers.Length - 1
      list_providers(idx_provider).checked = uc_grid.GRID_CHK_UNCHECKED
      ChangeTerminalCheck(idx_provider, uc_grid.GRID_CHK_UNCHECKED)
    Next

    Me.opt_several_types.Checked = False
    Me.opt_one_terminal.Checked = False
    Me.cmb_terminal.Enabled = False
    'If Me.ShowMassiveSearch Then
    '  Me.cmb_terminal.Size = New Size(181, cmb_terminal.Size.Height)
    'Else
    '  Me.cmb_terminal.Size = New Size(225, cmb_terminal.Size.Height)
    'End If

    If Not Me.cmb_currencies.Visible Then
      Me.btn_massive_search.Location = New System.Drawing.Point(Me.btn_massive_search.Location.X, 145)
    End If

    Me.ShowMassiveSearch = False

    If (Not IsNothing(Parent) AndAlso Not IsNothing(Parent.Parent) AndAlso Not IsNothing(Parent.Parent.Parent) _
          AndAlso (Me.Parent.Parent.Parent.Name.Equals("frm_terminal_sas_meters_sel") Or Me.Parent.Parent.Parent.Name.Equals("frm_report_profit_machine_and_day"))) _
      Or (Not IsNothing(Parent) AndAlso Not IsNothing(Parent.Parent) _
          AndAlso (Me.Parent.Parent.Name.Equals("frm_terminals_time_disconnected") Or Me.Parent.Parent.Name.Equals("frm_terminal_status"))) Then
      Me.ShowMassiveSearch = GeneralParam.GetBoolean("GUI", "TerminalMassiveSearch", False)
    End If

    Me.btn_massive_search.Enabled = False

    ' sle 31-MAY-2010
    Me.dg_filter.Redraw = True
  End Sub 'SetDefaultValues

  ' PURPOSE: Set GroupBox Title
  '
  '  PARAMS:
  '     - INPUT:
  '           - String with GroupBox Title
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub SetGroupBoxTitle(ByVal Title As String)
    Me.gb_terminal.Text = Title
  End Sub 'SetGroupBoxTitle

  ' PURPOSE: Expand the first node that contains a selected terminal
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ExpandSelectedTerminal()

    Dim _selected_provider As TYPE_PROVIDER_ITEM
    Dim _num_row As Integer
    Dim _idx_provider As Integer

    _selected_provider.idx = -1
    For Each _provider As TYPE_PROVIDER_ITEM In list_providers
      For Each _terminal As TYPE_TERMINAL_ITEM In _provider.list_terminals
        If _terminal.checked Then
          _selected_provider = _provider
        End If
      Next
    Next

    If _selected_provider.idx <> -1 Then
      With Me.dg_filter

        For _num_row = 0 To .NumRows - 1
          If RowIsHeader(_num_row, _idx_provider) Then
            If IsCollapsed(GRID_2_COLUMN_DESC, _num_row) And _idx_provider = _selected_provider.idx Then
              SetSignalAllowCollapse(GRID_2_COLUMN_DESC, _num_row)
              SetTerminalList(_idx_provider, _num_row)
              _num_row = .NumRows
            Else
            End If
          End If
        Next

      End With
    End If

  End Sub 'ExpandSelectedTerminal


  ' PURPOSE: Get Selected Terminals description for Reports
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Function GetTerminalReportText() As String
    Dim _report_terminals As String

    If Me.opt_several_types.Checked Then
      _report_terminals = Me.opt_several_types.Text
    ElseIf Me.opt_one_terminal.Checked Then
      _report_terminals = Me.OneTerminalSelectedText
    Else
      _report_terminals = Me.opt_all_types.Text
    End If

    If UseCurrency() Then
      _report_terminals = String.Format("{0} ({1})", _report_terminals, Me.m_selected_currency)
    End If

    Return _report_terminals
  End Function

  ' PURPOSE: Get Selected Terminals description for Reports
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Function GetTerminalReportAllOrSeveralText() As String
    Dim _report_terminals As String

    If Me.opt_several_types.Checked Or Me.opt_one_terminal.Checked Then
      _report_terminals = Me.opt_several_types.Text
    Else
      _report_terminals = Me.opt_all_types.Text
    End If

    If UseCurrency() Then
      _report_terminals = String.Format("{0} ({1})", _report_terminals, Me.m_selected_currency)
    End If

    Return _report_terminals
  End Function

  Public Function CheckScreenData()

    If opt_several_types.Checked Then
      If list_providers.Length = 0 Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8094), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' Selecciona como m�nimo un terminal

        Return False
      End If
    End If

    If (cmb_currencies.Visible) And String.IsNullOrEmpty(m_search_type) Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8095), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' Debe seleccionar como m�nimo una divisa

      Return False
    End If

    Return True
  End Function

#End Region ' Public functions

#Region " Private Functions "

  Private Sub InitControls(ByVal TerminalTypes() As Integer)
    Dim _idx As Integer
    Dim _sql_str As String

    ' Only First call to InitControls will be executed ( when TerminalTypes has value )
    If Not TerminalTypes Is Nothing Then

      ReDim Preserve m_terminal_types(0 To TerminalTypes.Length - 1)

      For _idx = 0 To TerminalTypes.Length - 1
        m_terminal_types(_idx) = TerminalTypes(_idx)
      Next

      ' Check if FloorDualCurrency is Enabled, and Combo must be shown. Show/Hide Currencies Combo
      m_floor_currency_enabled = WSI.Common.Misc.IsFloorDualCurrencyEnabled()

      lbl_currency.Visible = UseCurrency()
      cmb_currencies.Visible = UseCurrency()

      If (cmb_currencies.Visible) Then
        Call FillCurrenciesCombo()
        Me.lbl_currency.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1057)
        Me.cmb_currencies.Size = New System.Drawing.Size(60, 24)
        Me.cmb_currencies.AutoSize = False
      End If

    End If


    Select Case m_search_type
      Case UC_FILTER_TYPE.NONE, UC_FILTER_TYPE.ALL_ACTIVE
        Me.gb_terminal.Text = GLB_NLS_GUI_STATISTICS.GetString(470)
        Me.opt_one_terminal.Text = GLB_NLS_GUI_STATISTICS.GetString(216)
        Me.opt_several_types.Text = GLB_NLS_GUI_STATISTICS.GetString(217)
        Me.opt_all_types.Text = GLB_NLS_GUI_STATISTICS.GetString(218)

        ' Set combo with Terminals
        If m_is_multisite Then
          _sql_str = SELECT_TERMINALS_MULTISITE
        Else
          _sql_str = SELECT_TERMINALS
        End If

        _sql_str += WHERE_TERMINALS
        _sql_str += " AND TE_TERMINAL_TYPE IN ( " + GetTerminalTypes() + ") "
        If m_search_type = UC_FILTER_TYPE.ALL_ACTIVE Then
          _sql_str += " AND TE_STATUS = 0 " ' WSI.Common.TerminalStatus.ACTIVE
        End If

        _sql_str += Me.GetQueryFilteredByCurrency()

        _sql_str += ORDER_BY_TERMINALS

        Call SetCombo(Me.cmb_terminal, _sql_str)
        Me.cmb_terminal.Enabled = False
        Me.cmb_terminal.AutoSize = False
        Me.cmb_terminal.Size = New System.Drawing.Size(225, 24)
        Me.cmb_terminal.AutoCompleteMode = True
        Me.dg_filter.Size = New System.Drawing.Size(224, 133)
        Me.gb_terminal.Size = New System.Drawing.Size(322, 174)
        Me.gb_terminal.AutoSize = False
        Me.Size = New System.Drawing.Size(328, 180)
        Me.AutoSize = False

      Case UC_FILTER_TYPE.ONLY_PROVIDERS
        Me.gb_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(469)
        Me.opt_several_types.Text = GLB_NLS_GUI_STATISTICS.GetString(211)
        Me.opt_all_types.Text = GLB_NLS_GUI_STATISTICS.GetString(206)
        Me.opt_one_terminal.Visible = False
        Me.cmb_terminal.Visible = False

        Me.dg_filter.Size = New System.Drawing.Size(225, 133)
        Me.gb_terminal.Size = New System.Drawing.Size(325, 179)
        Me.gb_terminal.AutoSize = False
        Me.Size = New System.Drawing.Size(332, 194)
        Me.AutoSize = False

        Me.opt_several_types.Location = New System.Drawing.Point(Me.opt_several_types.Location.X, Me.opt_several_types.Location.Y - 20)
        Me.opt_all_types.Location = New System.Drawing.Point(Me.opt_all_types.Location.X, Me.opt_all_types.Location.Y - 20)
        Me.dg_filter.Location = New System.Drawing.Point(Me.dg_filter.Location.X, Me.dg_filter.Location.Y - 20)
        Me.gb_terminal.Size = New System.Drawing.Size(Me.gb_terminal.Size.Width, Me.gb_terminal.Size.Height - 20)

      Case UC_FILTER_TYPE.ONLY_ONE_TERMINAL
        Me.gb_terminal.Text = GLB_NLS_GUI_STATISTICS.GetString(470)
        Me.opt_one_terminal.Text = GLB_NLS_GUI_STATISTICS.GetString(216)

        Me.opt_all_types.Visible = False
        Me.opt_several_types.Visible = False
        Me.cmb_terminal.Visible = False

        Me.dg_filter.Size = New System.Drawing.Size(225, 150)
        Me.gb_terminal.Size = New System.Drawing.Size(Me.dg_filter.Size.Width + 15, Me.dg_filter.Size.Height + 25)
        Me.dg_filter.Location = New System.Drawing.Point(Me.gb_terminal.Location.X + 5, Me.gb_terminal.Location.Y + 15)
        Me.opt_one_terminal.Visible = False

        Me.gb_terminal.AutoSize = False
        Me.Size = New System.Drawing.Size(332, 194)
        Me.AutoSize = False
    End Select

    Me.dg_filter.IsToolTipped = True

    Call GUI_StyleSheetListProviders()
    GetProviderIdList()
    SetDefaultValues()

    If m_search_type = UC_FILTER_TYPE.ALL_ACTIVE Then
      m_search_type = UC_FILTER_TYPE.NONE
    End If

  End Sub

  Private Sub btn_massive_search_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_massive_search.Click
    Dim _frm_terminal_massive_search

    _frm_terminal_massive_search = New frm_terminal_massive_search(Me)

    Call _frm_terminal_massive_search.ShowDialog()

    Call Me.CheckTerminals()


  End Sub

  ' PURPOSE : Set combo with SQL SELECT
  '
  '  PARAMS :
  '     - INPUT :
  '           - uc_Combo
  '           - Datable
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  Private Sub SetCombo(ByVal combo As uc_combo, ByVal sql_select As String)
    Dim tabla As DataTable
    tabla = GUI_GetTableUsingSQL(sql_select, Integer.MaxValue)

    combo.Clear()

    combo.Add(tabla, m_is_multisite)
  End Sub ' SetCombo
  ' PURPOSE : Fill currencies combo with defined values
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  Private Sub FillCurrenciesCombo()

    Dim tabla As DataTable

    tabla = Nothing

    WSI.Common.CurrencyExchange.GetFloorDualCurrenciesForCombo(tabla)
    m_national_currency = tabla.Rows(0)("CE_CURRENCY_ISO_CODE")

    Me.cmb_currencies.Clear()
    Me.cmb_currencies.Add(tabla, "CURRENCY_ID", "CE_CURRENCY_ISO_CODE")

    Me.m_selected_currency = m_national_currency

  End Sub


  Private Function GetQueryFilteredByCurrency()
    Dim sql_result As String

    sql_result = ""
    If UseCurrency() Then
      sql_result = " AND TE_ISO_CODE = '" & m_selected_currency & "' "
    End If

    Return sql_result

  End Function

  Private Sub GetProviderIdList()

    Dim idx_provider As Integer
    Dim db_row As frm_base_sel.CLASS_DB_ROW
    Dim tprovider As DataTable = Nothing
    Dim rprovider As DataRow
    Dim idx_terminal As Integer

    idx_provider = 0
    idx_terminal = 0
    tprovider = GUI_GetTableUsingSQL(FilterGetSqlQueryProviders(), 5000)

    If tprovider.Rows.Count = 0 Then
      Me.TerminalListHasValues = UseCurrency()
      Return
    End If

    Me.TerminalListHasValues = True

    ReDim list_providers(1)
    'tprovider.BeginLoadData()
    For Each rprovider In tprovider.Rows
      db_row = New frm_base_sel.CLASS_DB_ROW(rprovider)

      If db_row.Value(SQL_COLUMN_TYPE) = "P" Then
        ReDim Preserve list_providers(0 To idx_provider)
        If db_row.IsNull(SQL_COLUMN_PROVIDER) Then
          list_providers(idx_provider).name = GLB_NLS_GUI_STATISTICS.GetString(213)
        Else
          list_providers(idx_provider).name = db_row.Value(SQL_COLUMN_PROVIDER)
        End If
        list_providers(idx_provider).idx = idx_provider
        list_providers(idx_provider).checked = uc_grid.GRID_CHK_UNCHECKED
        If idx_provider = 0 Then
          list_providers(idx_provider).num_terminals = 0
        Else
          list_providers(idx_provider - 1).num_terminals = idx_terminal
        End If
        idx_provider = idx_provider + 1
        idx_terminal = 0
        ReDim list_providers(idx_provider - 1).list_terminals(1)
      Else
        ReDim Preserve list_providers(idx_provider - 1).list_terminals(0 To idx_terminal)
        If db_row.IsNull(SQL_COLUMN_TERMINAL_FLOOR_ID) Then
          list_providers(idx_provider - 1).list_terminals(idx_terminal).id_floor = ""
        Else
          list_providers(idx_provider - 1).list_terminals(idx_terminal).id_floor = db_row.Value(SQL_COLUMN_TERMINAL_FLOOR_ID)
        End If
        list_providers(idx_provider - 1).list_terminals(idx_terminal).name = db_row.Value(SQL_COLUMN_TERMINAL_NAME)
        list_providers(idx_provider - 1).list_terminals(idx_terminal).id = db_row.Value(SQL_COLUMN_TERMINAL_ID)
        list_providers(idx_provider - 1).list_terminals(idx_terminal).checked = False
        list_providers(idx_provider - 1).list_terminals(idx_terminal).idx_provider = idx_provider - 1
        idx_terminal = idx_terminal + 1
      End If
    Next

    If idx_provider > 0 Then
      list_providers(idx_provider - 1).num_terminals = idx_terminal
    End If
    'tprovider.EndLoadData()
    'SetProviderList()

  End Sub ' GetProviderIdList


  ' PURPOSE: Shows providers by screen to select all or several
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetProviderList()
    Dim idx_provider As Integer
    Dim expand_symbol As String

    expand_symbol = IIf(m_search_type = UC_FILTER_TYPE.ONLY_PROVIDERS, "", ALLOWS_EXPAND_SYMBOL)

    Me.dg_filter.Redraw = False
    Me.dg_filter.Clear()
    Me.dg_filter.Enabled = True

    idx_provider = 0
    For idx_provider = 0 To list_providers.Length - 1
      dg_filter.AddRow()
      Me.dg_filter.Cell(idx_provider, GRID_2_COLUMN_ID).Value = list_providers(idx_provider).idx
      Me.dg_filter.Cell(idx_provider, GRID_2_COLUMN_DESC).Value = expand_symbol & list_providers(idx_provider).name
      ' JCM 03-MAY-2012: Provider Check flickers
      If m_search_type = UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then
        Me.dg_filter.Cell(idx_provider, GRID_2_COLUMN_CHECKED).Picture = Nothing
      Else
        Me.dg_filter.Cell(idx_provider, GRID_2_COLUMN_CHECKED).Value = list_providers(idx_provider).checked
      End If

      Me.dg_filter.Cell(idx_provider, GRID_2_COLUMN_TYPE).Value = GRID_2_ROW_TYPE_HEADER
      'Call dg_filter.Row(idx_provider).SetSignalAllowExpand(GRID_2_COLUMN_DESC)
    Next

    If Me.dg_filter.NumRows > 0 Then
      Me.dg_filter.CurrentRow = 0
    End If
    Me.dg_filter.Redraw = True

  End Sub ' SetProviderIdList

  Private Sub SetTerminalList(ByVal IdxProvider As Integer, ByVal Row As Integer)
    Dim idx_terminal As Integer
    Dim idx_row As Integer
    Dim _aux As String

    Me.dg_filter.Redraw = False

    idx_terminal = 0
    With list_providers(IdxProvider)
      For idx_terminal = 0 To .num_terminals - 1
        idx_row = Row + idx_terminal + 1
        dg_filter.InsertRow(idx_row)
        Me.dg_filter.Cell(idx_row, GRID_2_COLUMN_ID).Value = .list_terminals(idx_terminal).id
        'Only if Id_Floor has a value
        If .list_terminals(idx_terminal).id_floor = "" Then
          Me.dg_filter.Cell(idx_row, GRID_2_COLUMN_DESC).Value = GRID_2_TAB & .list_terminals(idx_terminal).name
        Else
          _aux = .list_terminals(idx_terminal).id_floor
          _aux = "[" & _aux & "]" & " "
          Me.dg_filter.Cell(idx_row, GRID_2_COLUMN_DESC).Value = GRID_2_TAB & _aux & .list_terminals(idx_terminal).name
          Me.dg_filter.Cell(idx_row, GRID_2_COLUMN_TOOLTIP).Value = .list_terminals(idx_terminal).id_floor & Environment.NewLine & .list_terminals(idx_terminal).name
        End If

        If .list_terminals(idx_terminal).checked Then
          Me.dg_filter.Cell(idx_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
        Else
          Me.dg_filter.Cell(idx_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
        End If
        Me.dg_filter.Cell(idx_row, GRID_2_COLUMN_CHK).Value = .list_terminals(idx_terminal).idx_provider
        Me.dg_filter.Cell(idx_row, GRID_2_COLUMN_TYPE).Value = GRID_2_ROW_TYPE_DATA
      Next
    End With

    If Me.dg_filter.NumRows > 0 Then
      Me.dg_filter.CurrentRow = Row
    End If
    Me.dg_filter.Redraw = True

  End Sub ' SetTerminalList



  Private Sub DeleteTerminalList(ByVal IdxProvider As Integer, ByVal Row As Integer)
    Dim idx_terminal As Integer
    Dim idx_row As Integer
    Dim m_redraw As Boolean

    m_redraw = Me.dg_filter.Redraw
    Me.dg_filter.Redraw = False

    idx_terminal = 0
    With list_providers(IdxProvider)
      For idx_terminal = .num_terminals - 1 To 0 Step -1
        idx_row = Row + idx_terminal + 1
        'Me.dg_filter.Redraw = False
        'dg_filter.DeleteRow(idx_row)
        dg_filter.DeleteRowFast(idx_row)
        'Me.dg_filter.Redraw = True
      Next
    End With

    If Me.dg_filter.NumRows > 0 Then
      Me.dg_filter.CurrentRow = Row
    End If
    Me.dg_filter.Redraw = m_redraw

  End Sub ' DeleteTerminalList

  ' PURPOSE: Get Sql Query to build Provider List
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String with SQL sentence
  Private Function FilterGetSqlQueryProviders() As String
    Dim _str_sql As StringBuilder
    Dim _where_te_terminal_type As String
    Dim _where_te_terminal_currency As String

    _str_sql = New StringBuilder()

    _where_te_terminal_type = " AND TE_TERMINAL_TYPE IN ( " + GetTerminalTypes() + ") "
    _where_te_terminal_currency = Me.GetQueryFilteredByCurrency()

    'LTC  29-OCT-2017
    ' MULTISITE
    If Not IsNothing(m_site) Then

      If m_site = "" Then

        _str_sql = New StringBuilder()

        _str_sql.AppendLine(" SELECT   'X' AS CHECKED     ")
        _str_sql.AppendLine(" , 'P' AS TIPUS              ")
        _str_sql.AppendLine(" , NULL TE_PROVIDER_ID       ")
        _str_sql.AppendLine(" , NULL                      ")
        _str_sql.AppendLine(" , NULL                      ")
        _str_sql.AppendLine(" , NULL                      ")

      Else

        _str_sql = New StringBuilder()

        _str_sql.AppendLine(" SELECT * FROM (             ")
        _str_sql.AppendLine("   SELECT    'X' AS CHECKED  ")
        _str_sql.AppendLine("           , 'T' AS TIPUS    ")
        _str_sql.AppendLine("           , TE_PROVIDER_ID  ")
        _str_sql.AppendLine("           , TE_TERMINAL_ID  ")
        _str_sql.AppendLine("           , TE_BASE_NAME    ")
        _str_sql.AppendLine("           , TE_FLOOR_ID     ")
        _str_sql.AppendLine("   FROM   TERMINALS          ")
        _str_sql.AppendLine("   WHERE  TE_TYPE = 1        " & _where_te_terminal_type + " " + _where_te_terminal_currency)
        _str_sql.AppendLine("   AND    TE_SITE_ID =       " & m_site)
        _str_sql.AppendLine("   UNION                     ")
        _str_sql.AppendLine("   SELECT    'X' AS CHECKED  ")
        _str_sql.AppendLine("           , 'P' AS TIPUS    ")
        _str_sql.AppendLine("           , TE_PROVIDER_ID  ")
        _str_sql.AppendLine("           , 0               ")
        _str_sql.AppendLine("           , TE_PROVIDER_ID  ")
        _str_sql.AppendLine("           , NULL            ")
        _str_sql.AppendLine("    FROM   TERMINALS         ")
        _str_sql.AppendLine("    WHERE  TE_TYPE = 1       " & _where_te_terminal_type + " " + _where_te_terminal_currency)
        _str_sql.AppendLine("    AND    TE_SITE_ID =      " & m_site)
        _str_sql.AppendLine(" ) X ORDER BY 3, 2, 5        ")

      End If

    Else

      If m_search_type = UC_FILTER_TYPE.ALL_ACTIVE Then
        _where_te_terminal_type += " AND TE_STATUS = 0 " ' WSI.Common.TerminalStatus.ACTIVE
      End If

      _str_sql = New StringBuilder()

      _str_sql.AppendLine(" SELECT * FROM (               ")
      _str_sql.AppendLine("   SELECT    'X' AS CHECKED    ")
      _str_sql.AppendLine("           , 'T' AS TIPUS      ")
      _str_sql.AppendLine("           , TE_PROVIDER_ID    ")
      _str_sql.AppendLine("           , TLC_MASTER_ID     ")
      _str_sql.AppendLine("           , TLC_BASE_NAME     ")
      _str_sql.AppendLine("           , TE_FLOOR_ID       ")
      _str_sql.AppendLine("   FROM   TERMINALS_LAST_CHANGED ")
      _str_sql.AppendLine("   INNER JOIN   TERMINALS ON TE_TERMINAL_ID = TLC_TERMINAL_ID ")
      _str_sql.AppendLine("   WHERE   TE_TYPE = 1  " & _where_te_terminal_type + " " + _where_te_terminal_currency)
      _str_sql.AppendLine("   UNION")
      _str_sql.AppendLine("   SELECT    'X' AS CHECKED    ")
      _str_sql.AppendLine("           , 'P' AS TIPUS      ")
      _str_sql.AppendLine("           , TE_PROVIDER_ID    ")
      _str_sql.AppendLine("           , 0                 ")
      _str_sql.AppendLine("           , TE_PROVIDER_ID    ")
      _str_sql.AppendLine("           , NULL              ")
      _str_sql.AppendLine("   FROM   TERMINALS_LAST_CHANGED ")
      _str_sql.AppendLine("   INNER JOIN   TERMINALS ON TE_TERMINAL_ID = TLC_TERMINAL_ID ")
      _str_sql.AppendLine("   WHERE   TE_TYPE = 1  " & _where_te_terminal_type + " " + _where_te_terminal_currency)
      _str_sql.AppendLine(") X ORDER BY 3, 2, 5           ")

    End If

    Return _str_sql.ToString()

  End Function ' FilterGetSqlQueryProviders

  Private Sub GUI_StyleSheetListProviders()
    With Me.dg_filter
      Call .Init(GRID_2_COLUMNS, GRID_2_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden colums
      .Column(GRID_2_COLUMN_ID).Header.Text = ""
      .Column(GRID_2_COLUMN_ID).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_2_COLUMN_CHECKED).Header.Text = ""
      .Column(GRID_2_COLUMN_CHECKED).WidthFixed = 300
      .Column(GRID_2_COLUMN_CHECKED).Fixed = True
      .Column(GRID_2_COLUMN_CHECKED).Editable = True
      .Column(GRID_2_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_2_COLUMN_DESC).Header.Text = ""
      .Column(GRID_2_COLUMN_DESC).WidthFixed = 2725
      .Column(GRID_2_COLUMN_DESC).Fixed = True
      .Column(GRID_2_COLUMN_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' GRID_COL_TYPE
      .Column(GRID_2_COLUMN_TYPE).Header.Text = ""
      .Column(GRID_2_COLUMN_TYPE).WidthFixed = 0

      ' GRID_COL_CHK
      .Column(GRID_2_COLUMN_CHK).Header.Text = ""
      .Column(GRID_2_COLUMN_CHK).WidthFixed = 0

      ' GRID_COL_TOOLTIP
      .Column(GRID_2_COLUMN_TOOLTIP).Header.Text = ""
      .Column(GRID_2_COLUMN_TOOLTIP).WidthFixed = 0

    End With

  End Sub ' GUI_StyleSheetListProviders

  Private Function IsCollapsed(ByVal ColumnForSignal As Integer, ByVal Row As Integer) As Boolean
    Dim cell_value As String

    cell_value = Me.dg_filter.Cell(Row, ColumnForSignal).Value

    If Mid(cell_value, 1, Len(ALLOWS_EXPAND_SYMBOL)) = ALLOWS_EXPAND_SYMBOL Then
      Return True
    Else
      Return False
    End If

  End Function

  Private Function IsExpanded(ByVal ColumnForSignal As Integer, ByVal Row As Integer) As Boolean
    Dim cell_value As String

    cell_value = Me.dg_filter.Cell(Row, ColumnForSignal).Value

    If Mid(cell_value, 1, Len(ALLOWS_COLLAPSE_SYMBOL)) = ALLOWS_COLLAPSE_SYMBOL Then
      Return True
    Else
      Return False
    End If

  End Function

  Private Sub SetSignalAllowCollapse(ByVal ColumnForSignal As Integer, ByVal Row As Integer)
    Dim cell_value_no_signal As String
    Dim cell_value As String

    cell_value = Me.dg_filter.Cell(Row, ColumnForSignal).Value
    cell_value_no_signal = Mid(cell_value, Len(ALLOWS_EXPAND_SYMBOL) + 1)
    Me.dg_filter.Cell(Row, ColumnForSignal).Value = ALLOWS_COLLAPSE_SYMBOL & cell_value_no_signal

  End Sub

  Private Sub SetSignalAllowExpand(ByVal ColumnForSignal As Integer, ByVal Row As Integer)
    Dim cell_value_no_signal As String
    Dim cell_value As String

    cell_value = Me.dg_filter.Cell(Row, ColumnForSignal).Value
    cell_value_no_signal = Mid(cell_value, Len(ALLOWS_COLLAPSE_SYMBOL) + 1)
    Me.dg_filter.Cell(Row, ColumnForSignal).Value = ALLOWS_EXPAND_SYMBOL & cell_value_no_signal

  End Sub

  Private Function RowIsHeader(ByVal RowIdx As Integer, ByRef ProviderIdx As Integer) As Boolean

    If Me.dg_filter.Cell(RowIdx, GRID_2_COLUMN_TYPE).Value = GRID_2_ROW_TYPE_HEADER Then
      ProviderIdx = Me.dg_filter.Cell(RowIdx, GRID_2_COLUMN_ID).Value
      Return True
    Else
      ProviderIdx = -1
      Return False
    End If

  End Function ' RowIsHeader

  ' PURPOSE: Update the checkbox state for all row dependants (for header rows)
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '           - ValueChecked
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ChangeCheckofRows(ByVal IdxProvider As Integer, ByVal FirstRow As Integer, ByVal ValueChecked As String)
    Dim idx As Integer

    'Call ChangeTerminalCheck(IdxProvider, ValueChecked)
    For idx = FirstRow + 1 To dg_filter.NumRows - 1
      If Me.dg_filter.Cell(idx, GRID_2_COLUMN_TYPE).Value = GRID_2_ROW_TYPE_HEADER Then

        Exit Sub
      End If

      Me.dg_filter.Cell(idx, GRID_2_COLUMN_CHECKED).Value = ValueChecked

    Next

  End Sub ' ChangeCheckofRows

  Private Sub ChangeTerminalCheck(ByVal IdxProvider As Integer, ByVal ValueChecked As String)
    Dim idx As Integer

    Select Case ValueChecked
      Case uc_grid.GRID_CHK_CHECKED
        For idx = 0 To list_providers(IdxProvider).num_terminals - 1
          list_providers(IdxProvider).list_terminals(idx).checked = True
        Next
      Case uc_grid.GRID_CHK_UNCHECKED
        For idx = 0 To list_providers(IdxProvider).num_terminals - 1
          list_providers(IdxProvider).list_terminals(idx).checked = False
        Next
      Case Else
        '
    End Select

  End Sub ' ChangeTerminalCheck

  ' PURPOSE: Update the check status of a header row when one of its dependant rows has been unchecked
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub TryToUncheckHeader(ByVal IdxProvider As Integer, ByVal FirstRow As Integer)
    Dim idx As Integer
    Dim idx_terminal As Integer
    Dim first_form_row As Integer
    Dim last_form_row As Integer
    Dim header_row As Integer
    Dim checked As Boolean = False

    idx_terminal = 0
    header_row = FirstRow
    first_form_row = header_row + 1
    last_form_row = header_row + list_providers(IdxProvider).num_terminals

    For idx = first_form_row To last_form_row
      If Me.dg_filter.Cell(idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        list_providers(IdxProvider).list_terminals(idx_terminal).checked = True
        checked = True
        'Exit For
      Else
        list_providers(IdxProvider).list_terminals(idx_terminal).checked = False
      End If
      idx_terminal = idx_terminal + 1
    Next

    If checked Then
      If m_search_type = UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then
        Me.dg_filter.Cell(header_row, GRID_2_COLUMN_CHECKED).Picture = Nothing
      Else
        Me.dg_filter.Cell(header_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
      End If
      list_providers(IdxProvider).checked = uc_grid.GRID_CHK_CHECKED_DISABLED
    Else
      If m_search_type = UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then
        Me.dg_filter.Cell(header_row, GRID_2_COLUMN_CHECKED).Picture = Nothing
      Else
        Me.dg_filter.Cell(header_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      End If
      list_providers(IdxProvider).checked = uc_grid.GRID_CHK_UNCHECKED
    End If

  End Sub ' TryToUncheckHeader

  ' PURPOSE: Update the check status of a header row when one of its dependant rows has been checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - FirstRow
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub TryToCheckHeader(ByVal IdxProvider As Integer, ByVal FirstRow As Integer)
    Dim idx As Integer
    Dim idx_terminal As Integer
    Dim first_form_row As Integer
    Dim last_form_row As Integer
    Dim header_row As Integer
    Dim unchecked As Boolean = False

    idx_terminal = 0
    header_row = FirstRow
    first_form_row = header_row + 1
    last_form_row = header_row + list_providers(IdxProvider).num_terminals

    For idx = first_form_row To last_form_row
      If Me.dg_filter.Cell(idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
        list_providers(IdxProvider).list_terminals(idx_terminal).checked = False
        unchecked = True
        'Exit For
      Else
        list_providers(IdxProvider).list_terminals(idx_terminal).checked = True

      End If
      idx_terminal = idx_terminal + 1
    Next

    If unchecked Then
      If m_search_type = UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then
        Me.dg_filter.Cell(header_row, GRID_2_COLUMN_CHECKED).Picture = Nothing
      Else
        Me.dg_filter.Cell(header_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
      End If
      list_providers(IdxProvider).checked = uc_grid.GRID_CHK_CHECKED_DISABLED
    Else
      If m_search_type = UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then
        Me.dg_filter.Cell(header_row, GRID_2_COLUMN_CHECKED).Picture = Nothing
      Else
        Me.dg_filter.Cell(header_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
      End If
      list_providers(IdxProvider).checked = uc_grid.GRID_CHK_CHECKED
    End If


  End Sub ' TryToCheckHeader

  ' PURPOSE: Set the tool tip text for a given row and column
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex As Integer
  '           - ColIndex As Integer
  '     - OUTPUT:
  '           - ToolTipTxt As String
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_SetToolTipText(ByVal RowIndex As Integer, _
                                 ByVal ColIndex As Integer, _
                                 ByRef ToolTipTxt As String)

    Dim _id_floor As String
    Dim _terminal As String
    Dim _nls_terminal As String
    Dim _nls_id_planta As String
    Dim _idx_provider As Integer
    Dim _str_desc_row As String
    Dim _idx_char_sep As Integer 'Position of separator char Enviroment.NewLine

    _idx_provider = 0
    _idx_char_sep = 0

    ' If Row is Header, exit.
    If RowIndex < 0 Then
      Exit Sub
    End If

    ' Only show Special ToolTip if Row Type is DETAILS.
    If RowIsHeader(RowIndex, _idx_provider) Then
      Exit Sub
    End If

    _nls_id_planta = GLB_NLS_GUI_CONFIGURATION.GetString(432) & ": "
    _nls_terminal = GLB_NLS_GUI_CONFIGURATION.GetString(210) & ": "

    _str_desc_row = Me.dg_filter.Cell(RowIndex, GRID_2_COLUMN_TOOLTIP).Value
    If String.IsNullOrEmpty(_str_desc_row) Then
      ToolTipTxt = _nls_terminal & Me.dg_filter.Cell(RowIndex, GRID_2_COLUMN_DESC).Value.Trim()

      Exit Sub
    End If

    _idx_char_sep = _str_desc_row.IndexOf(Environment.NewLine)
    If _idx_char_sep < 0 Then
      ToolTipTxt = _nls_terminal & Me.dg_filter.Cell(RowIndex, GRID_2_COLUMN_DESC).Value.Trim()

      Exit Sub
    End If

    _id_floor = _str_desc_row.Substring(0, _idx_char_sep).Trim()
    _terminal = _str_desc_row.Substring(_idx_char_sep + 1).Trim()

    ToolTipTxt = _nls_id_planta & _id_floor & Environment.NewLine & _nls_terminal & _terminal

  End Sub ' GUI_SetToolTipText

  ' PURPOSE: Check if DualCurrency is Enabled AND Control must fulter by currency
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '           
  '
  ' RETURNS:
  '     - True is must use currency. False otherwise
  Private Function UseCurrency()

    Return m_floor_currency_enabled And FilterByCurrency

  End Function


#Region "Events"

  Private Sub opt_all_types_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_types.Click
    Dim idx As Integer
    Dim idx_provider As Integer

    Me.cmb_terminal.Enabled = False
    'Me.opt_several_types.Checked = False
    'dg_filter.Column(GRID_2_COLUMN_CHECKED).Editable = False

    If dg_filter.NumRows = 0 Then
      Return
    End If

    Me.dg_filter.Redraw = False

    For idx = 0 To dg_filter.NumRows - 1
      Me.dg_filter.Cell(idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next

    For idx_provider = 0 To list_providers.Length - 1
      list_providers(idx_provider).checked = uc_grid.GRID_CHK_UNCHECKED
      ChangeTerminalCheck(idx_provider, uc_grid.GRID_CHK_UNCHECKED)
    Next

    Me.dg_filter.Redraw = True

    RaiseEvent ValueChangedEvent()

  End Sub ' opt_all_types_Click

  Private Sub opt_several_types_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_several_types.Click
    Me.opt_all_types.Checked = False
    Me.opt_one_terminal.Checked = False
    Me.cmb_terminal.Enabled = False

    If dg_filter.NumRows = 0 Then
      Return
    End If

    dg_filter.Column(GRID_2_COLUMN_CHECKED).Editable = True

    RaiseEvent ValueChangedEvent()

  End Sub

  Private Sub opt_one_terminal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_one_terminal.Click
    Dim idx As Integer
    Dim idx_provider As Integer

    Me.opt_all_types.Checked = False
    Me.opt_several_types.Checked = False
    Me.cmb_terminal.Enabled = True

    If dg_filter.NumRows = 0 Then
      Return
    End If

    Me.dg_filter.Redraw = False

    For idx = 0 To dg_filter.NumRows - 1
      Me.dg_filter.Cell(idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next

    For idx_provider = 0 To list_providers.Length - 1
      list_providers(idx_provider).checked = uc_grid.GRID_CHK_UNCHECKED
      ChangeTerminalCheck(idx_provider, uc_grid.GRID_CHK_UNCHECKED)
    Next

    Me.dg_filter.Redraw = True

    RaiseEvent ValueChangedEvent()

  End Sub

  Private Sub dg_filter_DataSelectedEvent() Handles dg_filter.DataSelectedEvent
    Dim idx_provider As Integer
    Dim num_rows As Integer
    Dim row_is_header As Boolean
    Dim current_row As Integer

    If m_search_type = UC_FILTER_TYPE.ONLY_PROVIDERS Then
      Return
    End If

    current_row = Me.dg_filter.CurrentRow

    If Me.dg_filter.CurrentCol <> GRID_2_COLUMN_DESC Then
      If Me.dg_filter.CurrentCol = GRID_2_COLUMN_CHECKED Then
        dg_filter_CellDataChangedEvent(Me.dg_filter.CurrentRow, Me.dg_filter.CurrentCol)
      End If
      Exit Sub
    End If

    If Me.opt_several_types.Checked = False And m_search_type <> UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then
      Me.opt_several_types.Checked = True
      Me.cmb_terminal.Enabled = False
      Me.dg_filter.Column(GRID_2_COLUMN_CHECKED).Editable = True
    End If

    With Me.dg_filter
      row_is_header = RowIsHeader(.CurrentRow, idx_provider)
      num_rows = .NumRows
      If row_is_header Then
        If IsCollapsed(GRID_2_COLUMN_DESC, .CurrentRow) Then
          'Call dg_filter.CollapseExpand(.CurrentRow, last_row, GRID_2_COLUMN_DESC)
          If (Not m_block_terminals) Then
            SetSignalAllowCollapse(GRID_2_COLUMN_DESC, .CurrentRow)
            SetTerminalList(idx_provider, .CurrentRow)
          End If

          num_rows = .NumRows
        Else
          'Call dg_filter.CollapseExpand(.CurrentRow, last_row, GRID_2_COLUMN_DESC)
          SetSignalAllowExpand(GRID_2_COLUMN_DESC, .CurrentRow)
          DeleteTerminalList(idx_provider, .CurrentRow)
        End If
      End If
      .CurrentRow = current_row
    End With
  End Sub ' dg_filter_DataSelectedEvent

  Private Sub dg_filter_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_filter.CellDataChangedEvent
    Dim current_row As Integer
    Dim idx_provider As Integer
    Dim provider_row As Integer
    Dim idx As Integer
    Dim _idx_aux As Integer
    Dim _idx_aux_pr As Integer
    'Dim _idx_provider As Integer
    'Dim _idx_terminal As Integer
    Try


      ' Prevent recursive calls
      If m_refreshing_grid = True Or Me.dg_filter.CurrentCol <> GRID_2_COLUMN_CHECKED Then
        If Me.dg_filter.CurrentCol = GRID_2_COLUMN_DESC Then
          dg_filter_DataSelectedEvent()
        End If
        Exit Sub
      End If


      Me.dg_filter.Redraw = False

      ' Update radio buttons accordingly
      If Me.opt_several_types.Checked = False And m_search_type <> UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then
        Me.opt_several_types.Checked = True
        Me.cmb_terminal.Enabled = False
        Me.dg_filter.Column(GRID_2_COLUMN_CHECKED).Editable = True
      End If

      With Me.dg_filter
        current_row = .CurrentRow
        m_refreshing_grid = True

        If RowIsHeader(.CurrentRow, idx_provider) Then
          'JCM 25-APR-2012 
          'If only can check Terminals, UnCheck actual click and do nothing else, because has clicked a Provider
          If m_search_type = UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then
            m_refreshing_grid = False
            'Me.dg_filter.Cell(.CurrentRow, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
            Me.dg_filter.Cell(.CurrentRow, GRID_2_COLUMN_CHECKED).Picture = Nothing
            'Updates Flag Redraw
            Me.dg_filter.Redraw = True

            Exit Sub
          End If

          list_providers(idx_provider).checked = .Cell(.CurrentRow, GRID_2_COLUMN_CHECKED).Value
          Call ChangeTerminalCheck(idx_provider, .Cell(.CurrentRow, GRID_2_COLUMN_CHECKED).Value)
          Call ChangeCheckofRows(idx_provider, .CurrentRow, .Cell(.CurrentRow, GRID_2_COLUMN_CHECKED).Value)
        Else
          'JCM 25-APR-2012 
          'If only can check Terminals, UnCheck all terminals and providers and Check the actual Terminal Row
          If m_search_type = UC_FILTER_TYPE.ONLY_ONE_TERMINAL Then

            ' Uncheck All from Grid
            For _idx_aux = 0 To dg_filter.NumRows - 1
              If RowIsHeader(_idx_aux, _idx_aux_pr) Then
                'Me.dg_filter.Cell(_idx_aux, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
                Me.dg_filter.Cell(_idx_aux, GRID_2_COLUMN_CHECKED).Picture = Nothing
              Else
                Me.dg_filter.Cell(_idx_aux, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
              End If
            Next

            ' Uncheck the intern list
            For _idx_aux_pr = 0 To list_providers.Length - 1
              If list_providers(_idx_aux_pr).checked = uc_grid.GRID_CHK_CHECKED Or list_providers(_idx_aux_pr).checked = uc_grid.GRID_CHK_CHECKED_DISABLED Then
                With list_providers(_idx_aux_pr)
                  For _idx_aux = 0 To .num_terminals - 1
                    If .list_terminals(_idx_aux).id.ToString = dg_filter.Cell(current_row, GRID_2_COLUMN_ID).Value Then
                      .list_terminals(_idx_aux).checked = True
                    Else
                      .list_terminals(_idx_aux).checked = False
                    End If
                  Next
                End With
                list_providers(_idx_aux_pr).checked = uc_grid.GRID_CHK_UNCHECKED_DISABLED
              End If

            Next

            dg_filter.Cell(current_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED

            m_refreshing_grid = False
            'Updates Flag Redraw, and Raise Updated Evented
            Me.dg_filter.Redraw = True
            RaiseEvent ValueChangedEvent()

            'Exit Sub
          End If

          idx_provider = Me.dg_filter.Cell(.CurrentRow, GRID_2_COLUMN_CHK).Value
          For idx = .CurrentRow To 0 Step -1
            If RowIsHeader(idx, provider_row) Then
              Exit For
            End If
          Next

          If .Cell(.CurrentRow, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
            Call TryToUncheckHeader(idx_provider, idx)
            If ShowMassiveSearch Then
              Dim _str_test As String
              _str_test = Me.MassiveSearchNumbersToEdit
              If dg_filter.Cell(current_row, GRID_2_COLUMN_TOOLTIP).Value.Length > 0 Then
                _str_test = _str_test.Replace(dg_filter.Cell(current_row, GRID_2_COLUMN_TOOLTIP).Value.Split(vbLf)(1), String.Empty)
              Else
                _str_test = _str_test.Replace(dg_filter.Cell(current_row, GRID_2_COLUMN_DESC).Value.TrimStart(" "), String.Empty)
              End If
              _str_test = _str_test.Trim(vbCrLf, vbLf, vbCr)
              Me.MassiveSearchNumbersToEdit = _str_test
            End If
          Else
            Call TryToCheckHeader(idx_provider, idx)
            If ShowMassiveSearch Then
              If String.IsNullOrEmpty(MassiveSearchNumbersToEdit) Then
                If dg_filter.Cell(current_row, GRID_2_COLUMN_TOOLTIP).Value.Length > 0 Then
                  Me.MassiveSearchNumbersToEdit = dg_filter.Cell(current_row, GRID_2_COLUMN_TOOLTIP).Value.Split(vbLf)(1).Trim(vbCrLf, vbLf, vbCr)
                Else
                  Me.MassiveSearchNumbersToEdit = dg_filter.Cell(current_row, GRID_2_COLUMN_DESC).Value.TrimStart(" ")
                End If

              Else
                If dg_filter.Cell(current_row, GRID_2_COLUMN_TOOLTIP).Value.Length > 0 Then
                  Me.MassiveSearchNumbersToEdit += Environment.NewLine + dg_filter.Cell(current_row, GRID_2_COLUMN_TOOLTIP).Value.Split(vbLf)(1).Trim(vbCrLf, vbLf, vbCr)
                Else
                  Me.MassiveSearchNumbersToEdit += Environment.NewLine + dg_filter.Cell(current_row, GRID_2_COLUMN_DESC).Value.TrimStart(" ")
                End If
              End If
            End If
          End If
        End If

        m_refreshing_grid = False
        .CurrentRow = current_row

      End With
      Me.dg_filter.Redraw = True

      RaiseEvent ValueChangedEvent()
    Catch _ex As Exception
      Log.Exception(_ex)

    End Try
  End Sub ' dg_types_CellDataChangedEvent

  Private Sub cmb_terminal_ValueChangedEvent() Handles cmb_terminal.ValueChangedEvent
    RaiseEvent ValueChangedEvent()

  End Sub ' cmb_terminal_ValueChangedEvent

  Private Sub dg_filter_SetToolTipTextEvent(ByVal GridRow As Integer, ByVal GridCol As Integer, ByRef Txt As String) Handles dg_filter.SetToolTipTextEvent
    Call GUI_SetToolTipText(GridRow, GridCol, Txt)
  End Sub ' dg_filter_SetToolTipTextEvent

  Private Sub cmb_currencies_ValueChangedEvent() Handles cmb_currencies.ValueChangedEvent
    Me.m_selected_currency = cmb_currencies.TextValue
    ReDim list_providers(-1)
    Me.InitControls(Nothing)
    RaiseEvent ValueChangedEvent()

  End Sub ' cmb_currencies_ValueChangedEvent

#End Region ' Events

#End Region 'Private Functions 

  Private Sub CheckTerminals()
    Dim _terminals_numbers As String()
    Dim _selected_provider As TYPE_PROVIDER_ITEM
    Dim _num_row As Integer
    Dim _idx_provider As Integer
    Dim _idx_terminal As Integer

    Dim _idx_aux As Integer
    Dim _idx_aux_pr As Integer

    Dim _found_terminal_str As List(Of String)

    _found_terminal_str = New List(Of String)

    ' Uncheck All from Grid
    For _idx_aux = 0 To dg_filter.NumRows - 1
      If RowIsHeader(_idx_aux, _idx_aux_pr) Then
        'Me.dg_filter.Cell(_idx_aux, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
        Me.dg_filter.Cell(_idx_aux, GRID_2_COLUMN_CHECKED).Picture = Nothing
      Else
        Me.dg_filter.Cell(_idx_aux, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      End If
    Next

    ' Uncheck the intern list
    For _idx_aux_pr = 0 To list_providers.Length - 1
      If list_providers(_idx_aux_pr).checked = uc_grid.GRID_CHK_CHECKED Or list_providers(_idx_aux_pr).checked = uc_grid.GRID_CHK_CHECKED_DISABLED Then
        With list_providers(_idx_aux_pr)
          For _idx_aux = 0 To .num_terminals - 1
            .list_terminals(_idx_aux).checked = False
          Next
        End With
      End If
    Next

    If MassiveSearchNumbersToEdit <> Nothing Then
      Me.MassiveSearchNumbersToEdit = Me.MassiveSearchNumbersToEdit.TrimEnd(vbLf)
      _terminals_numbers = Me.MassiveSearchNumbersToEdit.Split(vbLf)
      Me.opt_several_types.Checked = True
      Me.cmb_terminal.Enabled = False
      m_search_type = UC_FILTER_TYPE.NONE

      _selected_provider.idx = -1
      For Each _provider As TYPE_PROVIDER_ITEM In list_providers
        _idx_terminal = -1
        For Each _terminal As TYPE_TERMINAL_ITEM In _provider.list_terminals
          _idx_terminal += 1
          For Each _number As String In _terminals_numbers
            _number = _number.TrimEnd(vbCr)
            If _number.Equals(_terminal.name) Then
              _terminal.checked = True
              If Not _found_terminal_str.Contains(_terminal.name) Then
                _found_terminal_str.Add(_terminal.name)
              End If
              Exit For
            End If
          Next
          If _terminal.checked Then

            _selected_provider = _provider

            If _selected_provider.idx <> -1 Then
              With Me.dg_filter
                ' this discollapse the providers
                For _num_row = 0 To .NumRows - 1
                  If RowIsHeader(_num_row, _idx_provider) Then
                    If IsCollapsed(GRID_2_COLUMN_DESC, _num_row) And _idx_provider = _selected_provider.idx Then
                      SetSignalAllowCollapse(GRID_2_COLUMN_DESC, _num_row)
                      SetTerminalList(_idx_provider, _num_row)
                      Me.dg_filter.Cell(_num_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
                      list_providers(_idx_provider).checked = uc_grid.GRID_CHK_CHECKED_DISABLED
                    ElseIf _idx_provider = _selected_provider.idx Then
                      Me.dg_filter.Cell(_num_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
                      list_providers(_idx_provider).checked = uc_grid.GRID_CHK_CHECKED_DISABLED
                    End If
                  End If
                Next

                ' check terminals, we need two fors because the num of rows increase
                For _num_row = 0 To .NumRows - 1
                  If _terminal.id_floor.Length > 0 Then
                    If Me.dg_filter.Cell(_num_row, GRID_2_COLUMN_TOOLTIP).Value = _terminal.id_floor & Environment.NewLine & _terminal.name Then
                      Me.dg_filter.Cell(_num_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
                      list_providers(_selected_provider.idx).list_terminals(_idx_terminal).checked = True
                    End If
                  Else
                    If Me.dg_filter.Cell(_num_row, GRID_2_COLUMN_DESC).Value.TrimStart(" ") = _terminal.name Then
                      Me.dg_filter.Cell(_num_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
                      list_providers(_selected_provider.idx).list_terminals(_idx_terminal).checked = True
                    End If
                  End If
                Next
              End With
            End If

          End If

        Next
      Next

      If _found_terminal_str.Count < _terminals_numbers.Length Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(9065), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , )

      End If


    End If

    Me.dg_filter.Redraw = True
    RaiseEvent ValueChangedEvent()

  End Sub


  Protected Overrides Sub Finalize()
    MyBase.Finalize()
  End Sub

  Private Sub opt_several_types_CheckedChanged(sender As Object, e As EventArgs) Handles opt_several_types.CheckedChanged
    Me.btn_massive_search.Enabled = True
  End Sub

  Private Sub opt_all_types_CheckedChanged(sender As Object, e As EventArgs) Handles opt_all_types.CheckedChanged
    Me.btn_massive_search.Enabled = False
  End Sub

  Private Sub opt_one_terminal_CheckedChanged(sender As Object, e As EventArgs) Handles opt_one_terminal.CheckedChanged
    Me.btn_massive_search.Enabled = False
  End Sub
End Class
