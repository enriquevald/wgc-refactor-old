<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_date_select
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ef_day = New GUI_Controls.uc_entry_field
    Me.ef_month = New GUI_Controls.uc_entry_field
    Me.ef_year = New GUI_Controls.uc_entry_field
    Me.SuspendLayout()
    '
    'ef_day
    '
    Me.ef_day.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.ef_day.DoubleValue = 0
    Me.ef_day.IntegerValue = 0
    Me.ef_day.IsReadOnly = False
    Me.ef_day.Location = New System.Drawing.Point(0, 0)
    Me.ef_day.Margin = New System.Windows.Forms.Padding(0)
    Me.ef_day.Name = "ef_day"
    Me.ef_day.Size = New System.Drawing.Size(102, 24)
    Me.ef_day.SufixText = "Sufix Text"
    Me.ef_day.SufixTextVisible = True
    Me.ef_day.TabIndex = 0
    Me.ef_day.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    Me.ef_day.TextValue = ""
    Me.ef_day.TextWidth = 71
    Me.ef_day.Value = ""
    Me.ef_day.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_month
    '
    Me.ef_month.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.ef_month.DoubleValue = 0
    Me.ef_month.IntegerValue = 0
    Me.ef_month.IsReadOnly = False
    Me.ef_month.Location = New System.Drawing.Point(87, 0)
    Me.ef_month.Margin = New System.Windows.Forms.Padding(0)
    Me.ef_month.Name = "ef_month"
    Me.ef_month.Size = New System.Drawing.Size(73, 24)
    Me.ef_month.SufixText = "Sufix Text"
    Me.ef_month.SufixTextVisible = True
    Me.ef_month.TabIndex = 1
    Me.ef_month.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    Me.ef_month.TextValue = ""
    Me.ef_month.TextWidth = 40
    Me.ef_month.Value = ""
    Me.ef_month.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_year
    '
    Me.ef_year.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.ef_year.DoubleValue = 0
    Me.ef_year.IntegerValue = 0
    Me.ef_year.IsReadOnly = False
    Me.ef_year.Location = New System.Drawing.Point(145, 0)
    Me.ef_year.Margin = New System.Windows.Forms.Padding(0)
    Me.ef_year.Name = "ef_year"
    Me.ef_year.Size = New System.Drawing.Size(87, 24)
    Me.ef_year.SufixText = "Sufix Text"
    Me.ef_year.SufixTextVisible = True
    Me.ef_year.TabIndex = 2
    Me.ef_year.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    Me.ef_year.TextValue = ""
    Me.ef_year.TextWidth = 40
    Me.ef_year.Value = ""
    Me.ef_year.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_date_select
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.ef_day)
    Me.Controls.Add(Me.ef_month)
    Me.Controls.Add(Me.ef_year)
    Me.Name = "uc_date_select"
    Me.Size = New System.Drawing.Size(278, 25)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_day As GUI_Controls.uc_entry_field
  Friend WithEvents ef_month As GUI_Controls.uc_entry_field
  Friend WithEvents ef_year As GUI_Controls.uc_entry_field

End Class
