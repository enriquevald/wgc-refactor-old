<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_account_sel_extended
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_account = New System.Windows.Forms.GroupBox
    Me.chk_only_anonymous = New System.Windows.Forms.CheckBox
    Me.btn_massive_search = New System.Windows.Forms.Button
    Me.lbl_advanced_search = New System.Windows.Forms.LinkLabel
    Me.chk_anon = New System.Windows.Forms.CheckBox
    Me.gb_level = New System.Windows.Forms.GroupBox
    Me.chk_vip = New System.Windows.Forms.CheckBox
    Me.chk_level_04 = New System.Windows.Forms.CheckBox
    Me.chk_level_03 = New System.Windows.Forms.CheckBox
    Me.chk_level_02 = New System.Windows.Forms.CheckBox
    Me.chk_level_01 = New System.Windows.Forms.CheckBox
    Me.gb_points = New System.Windows.Forms.GroupBox
    Me.gb_last_activity = New System.Windows.Forms.GroupBox
    Me.rb_all = New System.Windows.Forms.RadioButton
    Me.rb_last_activity_by_date = New System.Windows.Forms.RadioButton
    Me.rb_last_activity_inactive = New System.Windows.Forms.RadioButton
    Me.rb_last_activity_active = New System.Windows.Forms.RadioButton
    Me.lbl_days_active = New System.Windows.Forms.Label
    Me.gp_other = New System.Windows.Forms.GroupBox
    Me.gb_date = New System.Windows.Forms.GroupBox
    Me.gb_gender = New System.Windows.Forms.GroupBox
    Me.chk_gender_female = New System.Windows.Forms.CheckBox
    Me.chk_gender_male = New System.Windows.Forms.CheckBox
    Me.fra_card_state = New System.Windows.Forms.GroupBox
    Me.chk_card_not_blocked = New System.Windows.Forms.CheckBox
    Me.chk_card_blocked = New System.Windows.Forms.CheckBox
    Me.fra_search_type = New System.Windows.Forms.GroupBox
    Me.chk_balance_non_redeemable = New System.Windows.Forms.CheckBox
    Me.chk_balance_redeemable = New System.Windows.Forms.CheckBox
    Me.dtp_to = New GUI_Controls.uc_date_picker
    Me.dtp_from = New GUI_Controls.uc_date_picker
    Me.dtp_wedding_date = New GUI_Controls.uc_date_picker
    Me.dtp_birth_date = New GUI_Controls.uc_date_picker
    Me.ef_telephone = New GUI_Controls.uc_entry_field
    Me.ef_account_id = New GUI_Controls.uc_entry_field
    Me.ef_card_track = New GUI_Controls.uc_entry_field
    Me.ef_card_holder = New GUI_Controls.uc_entry_field
    Me.ef_points_to = New GUI_Controls.uc_entry_field
    Me.ef_points_from = New GUI_Controls.uc_entry_field
    Me.dtp_last_to = New GUI_Controls.uc_date_picker
    Me.dtp_last_from = New GUI_Controls.uc_date_picker
    Me.gb_account.SuspendLayout()
    Me.gb_level.SuspendLayout()
    Me.gb_points.SuspendLayout()
    Me.gb_last_activity.SuspendLayout()
    Me.gp_other.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_gender.SuspendLayout()
    Me.fra_card_state.SuspendLayout()
    Me.fra_search_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_account
    '
    Me.gb_account.Controls.Add(Me.chk_only_anonymous)
    Me.gb_account.Controls.Add(Me.btn_massive_search)
    Me.gb_account.Controls.Add(Me.lbl_advanced_search)
    Me.gb_account.Controls.Add(Me.chk_anon)
    Me.gb_account.Controls.Add(Me.ef_account_id)
    Me.gb_account.Controls.Add(Me.ef_card_track)
    Me.gb_account.Controls.Add(Me.ef_card_holder)
    Me.gb_account.Location = New System.Drawing.Point(7, 9)
    Me.gb_account.Name = "gb_account"
    Me.gb_account.Size = New System.Drawing.Size(306, 165)
    Me.gb_account.TabIndex = 22
    Me.gb_account.TabStop = False
    Me.gb_account.Text = "xCuenta"
    '
    'chk_only_anonymous
    '
    Me.chk_only_anonymous.AutoSize = True
    Me.chk_only_anonymous.Location = New System.Drawing.Point(88, 140)
    Me.chk_only_anonymous.Name = "chk_only_anonymous"
    Me.chk_only_anonymous.Size = New System.Drawing.Size(130, 17)
    Me.chk_only_anonymous.TabIndex = 27
    Me.chk_only_anonymous.Text = "xOnly Anonymous"
    Me.chk_only_anonymous.UseVisualStyleBackColor = True
    '
    'btn_massive_search
    '
    Me.btn_massive_search.Location = New System.Drawing.Point(256, 18)
    Me.btn_massive_search.Name = "btn_massive_search"
    Me.btn_massive_search.Size = New System.Drawing.Size(38, 23)
    Me.btn_massive_search.TabIndex = 10
    Me.btn_massive_search.Text = "..."
    Me.btn_massive_search.UseVisualStyleBackColor = True
    Me.btn_massive_search.Visible = False
    '
    'lbl_advanced_search
    '
    Me.lbl_advanced_search.AutoSize = True
    Me.lbl_advanced_search.Location = New System.Drawing.Point(85, 108)
    Me.lbl_advanced_search.Name = "lbl_advanced_search"
    Me.lbl_advanced_search.Size = New System.Drawing.Size(141, 13)
    Me.lbl_advanced_search.TabIndex = 11
    Me.lbl_advanced_search.TabStop = True
    Me.lbl_advanced_search.Text = "xShowAdvancedSearch"
    '
    'chk_anon
    '
    Me.chk_anon.Location = New System.Drawing.Point(193, 25)
    Me.chk_anon.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.chk_anon.Name = "chk_anon"
    Me.chk_anon.Size = New System.Drawing.Size(91, 17)
    Me.chk_anon.TabIndex = 8
    Me.chk_anon.Text = "CheckBox1"
    Me.chk_anon.UseVisualStyleBackColor = True
    '
    'gb_level
    '
    Me.gb_level.Controls.Add(Me.chk_vip)
    Me.gb_level.Controls.Add(Me.chk_level_04)
    Me.gb_level.Controls.Add(Me.chk_level_03)
    Me.gb_level.Controls.Add(Me.chk_level_02)
    Me.gb_level.Controls.Add(Me.chk_level_01)
    Me.gb_level.Location = New System.Drawing.Point(319, 290)
    Me.gb_level.Name = "gb_level"
    Me.gb_level.Size = New System.Drawing.Size(207, 142)
    Me.gb_level.TabIndex = 17
    Me.gb_level.TabStop = False
    Me.gb_level.Text = "xLevel"
    '
    'chk_vip
    '
    Me.chk_vip.Location = New System.Drawing.Point(12, 21)
    Me.chk_vip.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.chk_vip.Name = "chk_vip"
    Me.chk_vip.Size = New System.Drawing.Size(133, 17)
    Me.chk_vip.TabIndex = 4
    Me.chk_vip.Text = "xVip"
    Me.chk_vip.UseVisualStyleBackColor = True
    '
    'chk_level_04
    '
    Me.chk_level_04.AutoSize = True
    Me.chk_level_04.Location = New System.Drawing.Point(12, 49)
    Me.chk_level_04.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_04.Name = "chk_level_04"
    Me.chk_level_04.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_04.TabIndex = 0
    Me.chk_level_04.Text = "xLevel 04"
    Me.chk_level_04.UseVisualStyleBackColor = True
    '
    'chk_level_03
    '
    Me.chk_level_03.AutoSize = True
    Me.chk_level_03.Location = New System.Drawing.Point(12, 72)
    Me.chk_level_03.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_03.Name = "chk_level_03"
    Me.chk_level_03.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_03.TabIndex = 1
    Me.chk_level_03.Text = "xLevel 03"
    Me.chk_level_03.UseVisualStyleBackColor = True
    '
    'chk_level_02
    '
    Me.chk_level_02.AutoSize = True
    Me.chk_level_02.Location = New System.Drawing.Point(12, 95)
    Me.chk_level_02.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_02.Name = "chk_level_02"
    Me.chk_level_02.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_02.TabIndex = 2
    Me.chk_level_02.Text = "xLevel 02"
    Me.chk_level_02.UseVisualStyleBackColor = True
    '
    'chk_level_01
    '
    Me.chk_level_01.AutoSize = True
    Me.chk_level_01.Location = New System.Drawing.Point(12, 118)
    Me.chk_level_01.MaximumSize = New System.Drawing.Size(150, 17)
    Me.chk_level_01.Name = "chk_level_01"
    Me.chk_level_01.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_01.TabIndex = 3
    Me.chk_level_01.Text = "xLevel 01"
    Me.chk_level_01.UseVisualStyleBackColor = True
    '
    'gb_points
    '
    Me.gb_points.Controls.Add(Me.ef_points_to)
    Me.gb_points.Controls.Add(Me.ef_points_from)
    Me.gb_points.Location = New System.Drawing.Point(319, 438)
    Me.gb_points.Name = "gb_points"
    Me.gb_points.Size = New System.Drawing.Size(207, 73)
    Me.gb_points.TabIndex = 18
    Me.gb_points.TabStop = False
    Me.gb_points.Text = "xPoints"
    '
    'gb_last_activity
    '
    Me.gb_last_activity.Controls.Add(Me.rb_all)
    Me.gb_last_activity.Controls.Add(Me.rb_last_activity_by_date)
    Me.gb_last_activity.Controls.Add(Me.rb_last_activity_inactive)
    Me.gb_last_activity.Controls.Add(Me.rb_last_activity_active)
    Me.gb_last_activity.Controls.Add(Me.lbl_days_active)
    Me.gb_last_activity.Controls.Add(Me.dtp_last_to)
    Me.gb_last_activity.Controls.Add(Me.dtp_last_from)
    Me.gb_last_activity.Location = New System.Drawing.Point(7, 302)
    Me.gb_last_activity.Name = "gb_last_activity"
    Me.gb_last_activity.Size = New System.Drawing.Size(306, 157)
    Me.gb_last_activity.TabIndex = 20
    Me.gb_last_activity.TabStop = False
    Me.gb_last_activity.Text = "xLastActivity"
    '
    'rb_all
    '
    Me.rb_all.AutoSize = True
    Me.rb_all.Location = New System.Drawing.Point(18, 128)
    Me.rb_all.Name = "rb_all"
    Me.rb_all.Size = New System.Drawing.Size(70, 17)
    Me.rb_all.TabIndex = 5
    Me.rb_all.Text = "x Todos"
    Me.rb_all.UseVisualStyleBackColor = True
    '
    'rb_last_activity_by_date
    '
    Me.rb_last_activity_by_date.AutoSize = True
    Me.rb_last_activity_by_date.Location = New System.Drawing.Point(18, 73)
    Me.rb_last_activity_by_date.Name = "rb_last_activity_by_date"
    Me.rb_last_activity_by_date.Size = New System.Drawing.Size(74, 17)
    Me.rb_last_activity_by_date.TabIndex = 2
    Me.rb_last_activity_by_date.Text = "xByDate"
    Me.rb_last_activity_by_date.UseVisualStyleBackColor = True
    '
    'rb_last_activity_inactive
    '
    Me.rb_last_activity_inactive.AutoSize = True
    Me.rb_last_activity_inactive.Location = New System.Drawing.Point(18, 48)
    Me.rb_last_activity_inactive.Name = "rb_last_activity_inactive"
    Me.rb_last_activity_inactive.Size = New System.Drawing.Size(78, 17)
    Me.rb_last_activity_inactive.TabIndex = 1
    Me.rb_last_activity_inactive.Text = "xInactive"
    Me.rb_last_activity_inactive.UseVisualStyleBackColor = True
    '
    'rb_last_activity_active
    '
    Me.rb_last_activity_active.AutoSize = True
    Me.rb_last_activity_active.Checked = True
    Me.rb_last_activity_active.Location = New System.Drawing.Point(18, 23)
    Me.rb_last_activity_active.Name = "rb_last_activity_active"
    Me.rb_last_activity_active.Size = New System.Drawing.Size(67, 17)
    Me.rb_last_activity_active.TabIndex = 0
    Me.rb_last_activity_active.TabStop = True
    Me.rb_last_activity_active.Text = "xActive"
    Me.rb_last_activity_active.UseVisualStyleBackColor = True
    '
    'lbl_days_active
    '
    Me.lbl_days_active.AutoSize = True
    Me.lbl_days_active.ForeColor = System.Drawing.Color.Blue
    Me.lbl_days_active.Location = New System.Drawing.Point(146, 25)
    Me.lbl_days_active.Name = "lbl_days_active"
    Me.lbl_days_active.Size = New System.Drawing.Size(72, 13)
    Me.lbl_days_active.TabIndex = 12
    Me.lbl_days_active.Text = "xLastXdays"
    '
    'gp_other
    '
    Me.gp_other.Controls.Add(Me.dtp_wedding_date)
    Me.gp_other.Controls.Add(Me.dtp_birth_date)
    Me.gp_other.Controls.Add(Me.ef_telephone)
    Me.gp_other.Location = New System.Drawing.Point(7, 180)
    Me.gp_other.Name = "gp_other"
    Me.gp_other.Size = New System.Drawing.Size(306, 116)
    Me.gp_other.TabIndex = 25
    Me.gp_other.TabStop = False
    Me.gp_other.Text = "xOther"
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(319, 9)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(207, 79)
    Me.gb_date.TabIndex = 26
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xCreationDate"
    '
    'gb_gender
    '
    Me.gb_gender.Controls.Add(Me.chk_gender_female)
    Me.gb_gender.Controls.Add(Me.chk_gender_male)
    Me.gb_gender.Location = New System.Drawing.Point(319, 162)
    Me.gb_gender.Name = "gb_gender"
    Me.gb_gender.Size = New System.Drawing.Size(207, 58)
    Me.gb_gender.TabIndex = 28
    Me.gb_gender.TabStop = False
    Me.gb_gender.Text = "xGender"
    '
    'chk_gender_female
    '
    Me.chk_gender_female.Location = New System.Drawing.Point(11, 35)
    Me.chk_gender_female.Name = "chk_gender_female"
    Me.chk_gender_female.Size = New System.Drawing.Size(109, 16)
    Me.chk_gender_female.TabIndex = 1
    Me.chk_gender_female.Text = "xWomen"
    '
    'chk_gender_male
    '
    Me.chk_gender_male.Location = New System.Drawing.Point(11, 18)
    Me.chk_gender_male.Name = "chk_gender_male"
    Me.chk_gender_male.Size = New System.Drawing.Size(109, 16)
    Me.chk_gender_male.TabIndex = 0
    Me.chk_gender_male.Text = "xMen"
    '
    'fra_card_state
    '
    Me.fra_card_state.Controls.Add(Me.chk_card_not_blocked)
    Me.fra_card_state.Controls.Add(Me.chk_card_blocked)
    Me.fra_card_state.Location = New System.Drawing.Point(319, 94)
    Me.fra_card_state.Name = "fra_card_state"
    Me.fra_card_state.Size = New System.Drawing.Size(207, 62)
    Me.fra_card_state.TabIndex = 27
    Me.fra_card_state.TabStop = False
    Me.fra_card_state.Text = "xState"
    '
    'chk_card_not_blocked
    '
    Me.chk_card_not_blocked.Location = New System.Drawing.Point(12, 40)
    Me.chk_card_not_blocked.Name = "chk_card_not_blocked"
    Me.chk_card_not_blocked.Size = New System.Drawing.Size(109, 16)
    Me.chk_card_not_blocked.TabIndex = 1
    Me.chk_card_not_blocked.Text = "xNot Blocked"
    '
    'chk_card_blocked
    '
    Me.chk_card_blocked.Location = New System.Drawing.Point(12, 20)
    Me.chk_card_blocked.Name = "chk_card_blocked"
    Me.chk_card_blocked.Size = New System.Drawing.Size(109, 16)
    Me.chk_card_blocked.TabIndex = 0
    Me.chk_card_blocked.Text = "xBlocked"
    '
    'fra_search_type
    '
    Me.fra_search_type.Controls.Add(Me.chk_balance_non_redeemable)
    Me.fra_search_type.Controls.Add(Me.chk_balance_redeemable)
    Me.fra_search_type.Location = New System.Drawing.Point(319, 226)
    Me.fra_search_type.Name = "fra_search_type"
    Me.fra_search_type.Size = New System.Drawing.Size(179, 58)
    Me.fra_search_type.TabIndex = 29
    Me.fra_search_type.TabStop = False
    Me.fra_search_type.Text = "xBalance "
    '
    'chk_balance_non_redeemable
    '
    Me.chk_balance_non_redeemable.AutoSize = True
    Me.chk_balance_non_redeemable.Location = New System.Drawing.Point(12, 35)
    Me.chk_balance_non_redeemable.Name = "chk_balance_non_redeemable"
    Me.chk_balance_non_redeemable.Size = New System.Drawing.Size(154, 17)
    Me.chk_balance_non_redeemable.TabIndex = 1
    Me.chk_balance_non_redeemable.Text = "xNon Redeemable > 0"
    Me.chk_balance_non_redeemable.UseVisualStyleBackColor = True
    '
    'chk_balance_redeemable
    '
    Me.chk_balance_redeemable.AutoSize = True
    Me.chk_balance_redeemable.Location = New System.Drawing.Point(12, 18)
    Me.chk_balance_redeemable.Name = "chk_balance_redeemable"
    Me.chk_balance_redeemable.Size = New System.Drawing.Size(128, 17)
    Me.chk_balance_redeemable.TabIndex = 0
    Me.chk_balance_redeemable.Text = "xRedeemable > 0"
    Me.chk_balance_redeemable.UseVisualStyleBackColor = True
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 48)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(191, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 42
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 20)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(191, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 42
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_wedding_date
    '
    Me.dtp_wedding_date.Checked = True
    Me.dtp_wedding_date.IsReadOnly = False
    Me.dtp_wedding_date.Location = New System.Drawing.Point(10, 50)
    Me.dtp_wedding_date.Margin = New System.Windows.Forms.Padding(3, 3, 3, 2)
    Me.dtp_wedding_date.Name = "dtp_wedding_date"
    Me.dtp_wedding_date.ShowCheckBox = True
    Me.dtp_wedding_date.ShowUpDown = False
    Me.dtp_wedding_date.Size = New System.Drawing.Size(200, 24)
    Me.dtp_wedding_date.SufixText = "Sufix Text"
    Me.dtp_wedding_date.SufixTextVisible = True
    Me.dtp_wedding_date.TabIndex = 14
    Me.dtp_wedding_date.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_birth_date
    '
    Me.dtp_birth_date.Checked = True
    Me.dtp_birth_date.IsReadOnly = False
    Me.dtp_birth_date.Location = New System.Drawing.Point(10, 21)
    Me.dtp_birth_date.Margin = New System.Windows.Forms.Padding(3, 3, 3, 2)
    Me.dtp_birth_date.Name = "dtp_birth_date"
    Me.dtp_birth_date.ShowCheckBox = True
    Me.dtp_birth_date.ShowUpDown = False
    Me.dtp_birth_date.Size = New System.Drawing.Size(200, 24)
    Me.dtp_birth_date.SufixText = "Sufix Text"
    Me.dtp_birth_date.SufixTextVisible = True
    Me.dtp_birth_date.TabIndex = 13
    Me.dtp_birth_date.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'ef_telephone
    '
    Me.ef_telephone.DoubleValue = 0
    Me.ef_telephone.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_telephone.IntegerValue = 0
    Me.ef_telephone.IsReadOnly = False
    Me.ef_telephone.Location = New System.Drawing.Point(10, 79)
    Me.ef_telephone.Margin = New System.Windows.Forms.Padding(3, 3, 3, 2)
    Me.ef_telephone.Name = "ef_telephone"
    Me.ef_telephone.Size = New System.Drawing.Size(284, 25)
    Me.ef_telephone.SufixText = "Sufix Text"
    Me.ef_telephone.SufixTextVisible = True
    Me.ef_telephone.TabIndex = 15
    Me.ef_telephone.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_telephone.TextValue = ""
    Me.ef_telephone.Value = ""
    '
    'ef_account_id
    '
    Me.ef_account_id.DoubleValue = 0
    Me.ef_account_id.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_account_id.IntegerValue = 0
    Me.ef_account_id.IsReadOnly = False
    Me.ef_account_id.Location = New System.Drawing.Point(6, 17)
    Me.ef_account_id.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_account_id.Name = "ef_account_id"
    Me.ef_account_id.Size = New System.Drawing.Size(181, 25)
    Me.ef_account_id.SufixText = "Sufix Text"
    Me.ef_account_id.SufixTextVisible = True
    Me.ef_account_id.TabIndex = 6
    Me.ef_account_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_account_id.TextValue = ""
    Me.ef_account_id.Value = ""
    '
    'ef_card_track
    '
    Me.ef_card_track.DoubleValue = 0
    Me.ef_card_track.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_card_track.IntegerValue = 0
    Me.ef_card_track.IsReadOnly = False
    Me.ef_card_track.Location = New System.Drawing.Point(6, 47)
    Me.ef_card_track.Margin = New System.Windows.Forms.Padding(3, 3, 3, 2)
    Me.ef_card_track.Name = "ef_card_track"
    Me.ef_card_track.Size = New System.Drawing.Size(291, 25)
    Me.ef_card_track.SufixText = "Sufix Text"
    Me.ef_card_track.SufixTextVisible = True
    Me.ef_card_track.TabIndex = 7
    Me.ef_card_track.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_card_track.TextValue = ""
    Me.ef_card_track.Value = ""
    '
    'ef_card_holder
    '
    Me.ef_card_holder.DoubleValue = 0
    Me.ef_card_holder.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_card_holder.IntegerValue = 0
    Me.ef_card_holder.IsReadOnly = False
    Me.ef_card_holder.Location = New System.Drawing.Point(6, 77)
    Me.ef_card_holder.Margin = New System.Windows.Forms.Padding(3, 3, 3, 2)
    Me.ef_card_holder.Name = "ef_card_holder"
    Me.ef_card_holder.OnlyUpperCase = True
    Me.ef_card_holder.Size = New System.Drawing.Size(291, 25)
    Me.ef_card_holder.SufixText = "Sufix Text"
    Me.ef_card_holder.SufixTextVisible = True
    Me.ef_card_holder.TabIndex = 9
    Me.ef_card_holder.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_card_holder.TextValue = ""
    Me.ef_card_holder.Value = ""
    '
    'ef_points_to
    '
    Me.ef_points_to.DoubleValue = 0
    Me.ef_points_to.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_points_to.IntegerValue = 0
    Me.ef_points_to.IsReadOnly = False
    Me.ef_points_to.Location = New System.Drawing.Point(6, 42)
    Me.ef_points_to.Margin = New System.Windows.Forms.Padding(3, 3, 3, 2)
    Me.ef_points_to.Name = "ef_points_to"
    Me.ef_points_to.OnlyUpperCase = True
    Me.ef_points_to.Size = New System.Drawing.Size(165, 25)
    Me.ef_points_to.SufixText = "xDesde"
    Me.ef_points_to.SufixTextVisible = True
    Me.ef_points_to.TabIndex = 5
    Me.ef_points_to.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_points_to.TextValue = ""
    Me.ef_points_to.TextWidth = 50
    Me.ef_points_to.Value = ""
    '
    'ef_points_from
    '
    Me.ef_points_from.DoubleValue = 0
    Me.ef_points_from.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_points_from.IntegerValue = 0
    Me.ef_points_from.IsReadOnly = False
    Me.ef_points_from.Location = New System.Drawing.Point(6, 14)
    Me.ef_points_from.Margin = New System.Windows.Forms.Padding(3, 3, 3, 2)
    Me.ef_points_from.Name = "ef_points_from"
    Me.ef_points_from.OnlyUpperCase = True
    Me.ef_points_from.Size = New System.Drawing.Size(165, 25)
    Me.ef_points_from.SufixText = "xDesde"
    Me.ef_points_from.SufixTextVisible = True
    Me.ef_points_from.TabIndex = 4
    Me.ef_points_from.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_points_from.TextValue = ""
    Me.ef_points_from.TextWidth = 50
    Me.ef_points_from.Value = ""
    '
    'dtp_last_to
    '
    Me.dtp_last_to.Checked = True
    Me.dtp_last_to.IsReadOnly = False
    Me.dtp_last_to.Location = New System.Drawing.Point(105, 99)
    Me.dtp_last_to.Name = "dtp_last_to"
    Me.dtp_last_to.ShowCheckBox = True
    Me.dtp_last_to.ShowUpDown = False
    Me.dtp_last_to.Size = New System.Drawing.Size(191, 24)
    Me.dtp_last_to.SufixText = "Sufix Text"
    Me.dtp_last_to.SufixTextVisible = True
    Me.dtp_last_to.TabIndex = 4
    Me.dtp_last_to.TextWidth = 42
    Me.dtp_last_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_last_from
    '
    Me.dtp_last_from.Checked = False
    Me.dtp_last_from.IsReadOnly = False
    Me.dtp_last_from.Location = New System.Drawing.Point(105, 69)
    Me.dtp_last_from.Name = "dtp_last_from"
    Me.dtp_last_from.ShowCheckBox = False
    Me.dtp_last_from.ShowUpDown = False
    Me.dtp_last_from.Size = New System.Drawing.Size(191, 24)
    Me.dtp_last_from.SufixText = "Sufix Text"
    Me.dtp_last_from.SufixTextVisible = True
    Me.dtp_last_from.TabIndex = 3
    Me.dtp_last_from.TextWidth = 42
    Me.dtp_last_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_account_sel_extended
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.AutoSize = True
    Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Controls.Add(Me.fra_search_type)
    Me.Controls.Add(Me.gb_gender)
    Me.Controls.Add(Me.fra_card_state)
    Me.Controls.Add(Me.gb_date)
    Me.Controls.Add(Me.gp_other)
    Me.Controls.Add(Me.gb_account)
    Me.Controls.Add(Me.gb_level)
    Me.Controls.Add(Me.gb_points)
    Me.Controls.Add(Me.gb_last_activity)
    Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Name = "uc_account_sel_extended"
    Me.Size = New System.Drawing.Size(529, 514)
    Me.gb_account.ResumeLayout(False)
    Me.gb_account.PerformLayout()
    Me.gb_level.ResumeLayout(False)
    Me.gb_level.PerformLayout()
    Me.gb_points.ResumeLayout(False)
    Me.gb_last_activity.ResumeLayout(False)
    Me.gb_last_activity.PerformLayout()
    Me.gp_other.ResumeLayout(False)
    Me.gb_date.ResumeLayout(False)
    Me.gb_gender.ResumeLayout(False)
    Me.fra_card_state.ResumeLayout(False)
    Me.fra_search_type.ResumeLayout(False)
    Me.fra_search_type.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_account As System.Windows.Forms.GroupBox
  Friend WithEvents btn_massive_search As System.Windows.Forms.Button
  Friend WithEvents lbl_advanced_search As System.Windows.Forms.LinkLabel
  Friend WithEvents chk_anon As System.Windows.Forms.CheckBox
  Friend WithEvents ef_account_id As GUI_Controls.uc_entry_field
  Friend WithEvents ef_card_track As GUI_Controls.uc_entry_field
  Friend WithEvents ef_card_holder As GUI_Controls.uc_entry_field
  Friend WithEvents gb_level As System.Windows.Forms.GroupBox
  Friend WithEvents chk_vip As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_04 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_03 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_02 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_01 As System.Windows.Forms.CheckBox
  Friend WithEvents gb_points As System.Windows.Forms.GroupBox
  Friend WithEvents ef_points_to As GUI_Controls.uc_entry_field
  Friend WithEvents ef_points_from As GUI_Controls.uc_entry_field
  Friend WithEvents gb_last_activity As System.Windows.Forms.GroupBox
  Friend WithEvents rb_all As System.Windows.Forms.RadioButton
  Friend WithEvents rb_last_activity_by_date As System.Windows.Forms.RadioButton
  Friend WithEvents rb_last_activity_inactive As System.Windows.Forms.RadioButton
  Friend WithEvents rb_last_activity_active As System.Windows.Forms.RadioButton
  Protected WithEvents lbl_days_active As System.Windows.Forms.Label
  Friend WithEvents dtp_last_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_last_from As GUI_Controls.uc_date_picker
  Friend WithEvents gp_other As System.Windows.Forms.GroupBox
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents chk_only_anonymous As System.Windows.Forms.CheckBox
  Friend WithEvents gb_gender As System.Windows.Forms.GroupBox
  Friend WithEvents chk_gender_female As System.Windows.Forms.CheckBox
  Friend WithEvents chk_gender_male As System.Windows.Forms.CheckBox
  Friend WithEvents fra_card_state As System.Windows.Forms.GroupBox
  Friend WithEvents chk_card_not_blocked As System.Windows.Forms.CheckBox
  Friend WithEvents chk_card_blocked As System.Windows.Forms.CheckBox
  Friend WithEvents fra_search_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_balance_non_redeemable As System.Windows.Forms.CheckBox
  Friend WithEvents chk_balance_redeemable As System.Windows.Forms.CheckBox
  Friend WithEvents dtp_wedding_date As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_birth_date As GUI_Controls.uc_date_picker
  Friend WithEvents ef_telephone As GUI_Controls.uc_entry_field

End Class
