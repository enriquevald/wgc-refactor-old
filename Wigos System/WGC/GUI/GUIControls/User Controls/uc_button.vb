'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_button.vb  
'
' DESCRIPTION:   control button
'
' AUTHOR:        Andreu Juli�
'
' CREATION DATE: 22-APR-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 22-APR-2002  AJQ    Initial version
'-------------------------------------------------------------------

Imports System.Windows.Forms

'XVV -> Add NameSpace Windows.Forms,solve error with IWin32Window variables definitions
Imports System.Drawing

Public Class uc_button
  Inherits UserControl
  Implements IButtonControl

#Region " Constants "

  Private Const NORMAL_WIDTH As Integer = 90
  Private Const NORMAL_HEIGHT As Integer = 30
  Private Const ADD_DELETE_WIDTH As Integer = 50
  Private Const ADD_DELETE_HEIGHT As Integer = NORMAL_HEIGHT * 0.7
  Private Const SMALL_WIDTH As Integer = NORMAL_WIDTH * 0.7
  Private Const SMALL_HEIGHT As Integer = NORMAL_HEIGHT * 0.7
  Private Const BIG_WIDTH As Integer = NORMAL_WIDTH * 1.3
  Private Const BIG_HEIGHT As Integer = NORMAL_HEIGHT * 1.3
  Private Const RESET_WIDTH As Integer = 30
  Private Const RESET_HEIGHT As Integer = 29
  Private Const SELECTION_WIDTH As Integer = 30
  Private Const SELECTION_HEIGHT As Integer = 29
  Private Const LAST_HEIGHT As Integer = 29
  Private Const LAST_WIDTH As Integer = 30
  Private Const WIDTH_INFO As Integer = 24
  Private Const HEIGHT_INFO As Integer = 24

  Private Const IDX_IMAGE_LIST_SELECTION As Integer = 0
  Private Const IDX_IMAGE_LIST_RESET As Integer = 2
  Private Const IDX_IMAGE_LIST_INFO As Integer = 1
  Private Const IDX_IMAGE_LIST_LAST As Integer = 3

  ' LRS 06-04-2004, A second image list, with smaller icons.
  Private Const IDX_IMAGE_SMALL_LIST_INFO As Integer = 0

#End Region

#Region " Members "

  ' GZ 17-DEC-2004, Add tooltip option
  Private m_last_tooltip As Date
  Private GLB_IsToolTipped As Boolean = False

  Private btn_type As ENUM_BUTTON_TYPE

#End Region

#Region " IButtonControl "
  Sub NotifyDefault(ByVal value As Boolean) Implements IButtonControl.NotifyDefault
    Me.btn_btn.NotifyDefault(value)
  End Sub
  Sub PerformClick() Implements IButtonControl.PerformClick
    Me.btn_btn.PerformClick()
  End Sub
  Public Property DialogResult() As DialogResult Implements IButtonControl.DialogResult
    Get
      Return Me.btn_btn.DialogResult
    End Get
    Set(ByVal Value As DialogResult)
      Me.btn_btn.DialogResult = Value
    End Set
  End Property
#End Region

  Public Event ClickEvent()

  Public Event SetToolTipTextEvent(ByRef Txt As String)

  Public Enum ENUM_BUTTON_TYPE
    NORMAL = 0
    SMALL = 1
    BIG = 2
    SELECTION = 3
    RESET = 4
    INFO = 5
    LAST_SELECTED = 6
    ADD_DELETE = 7
    USER = -1
  End Enum

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call
    Me.Type = ENUM_BUTTON_TYPE.NORMAL

  End Sub

  'UserControl overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Private WithEvents btn_btn As System.Windows.Forms.Button
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(uc_button))
    Me.btn_btn = New System.Windows.Forms.Button()
    Me.iml_buttons = New System.Windows.Forms.ImageList(Me.components)
    Me.iml_small_buttons = New System.Windows.Forms.ImageList(Me.components)
    Me.tool_tip = New System.Windows.Forms.ToolTip(Me.components)
    Me.tmr_mouse_move = New System.Windows.Forms.Timer(Me.components)
    Me.SuspendLayout()
    '
    'btn_btn
    '
    Me.btn_btn.Dock = System.Windows.Forms.DockStyle.Fill
    Me.btn_btn.ImageAlign = System.Drawing.ContentAlignment.TopLeft
    Me.btn_btn.ImageList = Me.iml_buttons
    Me.btn_btn.Name = "btn_btn"
    Me.btn_btn.Size = New System.Drawing.Size(70, 30)
    Me.btn_btn.TabIndex = 0
    Me.btn_btn.Text = "btn"
    '
    'iml_buttons
    '
    Me.iml_buttons.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
    Me.iml_buttons.ImageSize = New System.Drawing.Size(18, 18)
    Me.iml_buttons.ImageStream = CType(resources.GetObject("iml_buttons.ImageStream"), System.Windows.Forms.ImageListStreamer)
    Me.iml_buttons.TransparentColor = System.Drawing.Color.Transparent
    '
    'iml_small_buttons
    '
    Me.iml_small_buttons.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
    Me.iml_small_buttons.ImageSize = New System.Drawing.Size(16, 16)
    Me.iml_small_buttons.ImageStream = CType(resources.GetObject("iml_small_buttons.ImageStream"), System.Windows.Forms.ImageListStreamer)
    Me.iml_small_buttons.TransparentColor = System.Drawing.Color.Transparent
    '
    'tool_tip
    '
    Me.tool_tip.AutoPopDelay = 5000
    Me.tool_tip.InitialDelay = 500
    Me.tool_tip.ReshowDelay = 100
    '
    'tmr_mouse_move
    '
    Me.tmr_mouse_move.Interval = 1000
    '
    'uc_button
    '
    Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.btn_btn})
    Me.Name = "uc_button"
    Me.Size = New System.Drawing.Size(70, 30)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Public Properties "

  Public Overrides Property Text() As String
    Get
      Return Me.btn_btn.Text
    End Get
    Set(ByVal Value As String)
      Me.btn_btn.Text = Value
    End Set
  End Property

  Public Property Type() As ENUM_BUTTON_TYPE
    Get
      Return btn_type
    End Get
    Set(ByVal Value As ENUM_BUTTON_TYPE)
      Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(uc_button))
      btn_type = Value
      Me.btn_btn.ImageList = iml_buttons
      Select Case Value
        Case ENUM_BUTTON_TYPE.NORMAL
          Me.Width = NORMAL_WIDTH
          Me.Height = NORMAL_HEIGHT

        Case ENUM_BUTTON_TYPE.BIG
          Me.Width = BIG_WIDTH
          Me.Height = BIG_HEIGHT

        Case ENUM_BUTTON_TYPE.SMALL
          Me.Width = SMALL_WIDTH
          Me.Height = SMALL_HEIGHT

        Case ENUM_BUTTON_TYPE.ADD_DELETE
          Me.Width = ADD_DELETE_WIDTH
          Me.Height = ADD_DELETE_HEIGHT

        Case ENUM_BUTTON_TYPE.LAST_SELECTED
          Me.Width = LAST_WIDTH
          Me.Height = LAST_HEIGHT
          Me.btn_btn.ImageIndex = IDX_IMAGE_LIST_LAST
          Me.btn_btn.FlatStyle = FlatStyle.Flat
          Me.btn_btn.Text = ""

        Case ENUM_BUTTON_TYPE.RESET
          Me.Width = RESET_WIDTH
          Me.Height = RESET_HEIGHT
          Me.btn_btn.ImageIndex = IDX_IMAGE_LIST_RESET
          Me.btn_btn.FlatStyle = FlatStyle.Flat
          Me.btn_btn.Text = ""

        Case ENUM_BUTTON_TYPE.SELECTION
          Me.Width = SELECTION_WIDTH
          Me.Height = SELECTION_HEIGHT
          Me.btn_btn.ImageIndex = IDX_IMAGE_LIST_SELECTION
          Me.btn_btn.FlatStyle = FlatStyle.Flat
          Me.btn_btn.Text = ""

        Case ENUM_BUTTON_TYPE.INFO
          Me.btn_btn.Text = ""
          Me.Width = WIDTH_INFO
          Me.Height = HEIGHT_INFO
          Me.btn_btn.ImageList = iml_small_buttons
          Me.btn_btn.ImageIndex = IDX_IMAGE_SMALL_LIST_INFO
          Me.btn_btn.ImageAlign = ContentAlignment.BottomRight

        Case ENUM_BUTTON_TYPE.USER

        Case Else
      End Select
    End Set
  End Property

  Public WriteOnly Property IsToolTipped() As Boolean
    Set(ByVal Value As Boolean)
      GLB_IsToolTipped = Value
    End Set
  End Property

  Public Property ToolTipped() As Boolean
    Get
      Return GLB_IsToolTipped
    End Get
    Set(ByVal Value As Boolean)
      GLB_IsToolTipped = Value
    End Set
  End Property

#End Region

  Friend WithEvents iml_buttons As System.Windows.Forms.ImageList
  Friend WithEvents iml_small_buttons As System.Windows.Forms.ImageList
  Private WithEvents tool_tip As System.Windows.Forms.ToolTip
  Private WithEvents tmr_mouse_move As System.Windows.Forms.Timer

#Region " Private Events "

  Private Sub btn_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Resize
    If btn_type <> ENUM_BUTTON_TYPE.USER Then
      Me.Type = btn_type
    End If
  End Sub

  Private Sub btn_btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_btn.Click

    Try
      Dim _parent As Control
      Dim _path As String
      Dim _text As String

      _path = ""
      _parent = Me.Parent

      While Not (_parent Is Nothing)

        _text = ""
        Try
          _text = _parent.Text.Trim()
        Catch ex As Exception
        End Try

        If Not String.IsNullOrEmpty(_text) Then
          If _path = "" Then
            _path = _text
          Else
            _path = _text + "." + _path
          End If
        End If

        Try
          If _parent.Parent Is Nothing Then
            Exit While
          End If

          If _parent.Parent.Equals(_parent) Then
            Exit While
          End If

          _parent = _parent.Parent

          If TypeOf _parent Is Form Then
            Dim _frm As Form
            _frm = _parent

            If _frm.IsMdiContainer Then
              Exit While
            End If
          End If

        Catch ex As Exception
          Exit Sub
        End Try
      End While

      WSI.Common.Users.SetLastAction(Me.Text, _path)

    Catch ex As Exception

    End Try

    RaiseEvent ClickEvent()
  End Sub

  ' Start tooltip timer when mouse moves over btn
  Private Sub uc_button_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btn_btn.MouseMove
    Dim diff_ticks As Long

    If Not GLB_IsToolTipped Then
      Exit Sub

    End If

    ' exit if event was just launched
    diff_ticks = Now.Ticks - m_last_tooltip.Ticks
    If (diff_ticks < 150000) Then
      Exit Sub

    End If

    m_last_tooltip = Now

    Me.tool_tip.AutoPopDelay = 30000
    Me.tool_tip.InitialDelay = 500
    Me.tool_tip.ReshowDelay = 100

    ' start tooltip timer
    Call Me.tmr_mouse_move.Start()
  End Sub

  Private Sub tmr_mouse_move_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tmr_mouse_move.Tick
    Dim ctrl As System.Windows.Forms.Control
    Dim tooltip_txt As String

    Call Me.tmr_mouse_move.Stop()

    '  do not show tooltip if button cannot be tooltipped
    If Not Me.GLB_IsToolTipped Then
      Exit Sub

    End If

    ' do not show tooltip if not on btn
    'XVV 17/04/2007
    'Change Me.MousePosition for Control.MousePosition because produces compile warnings 
    ctrl = Me.GetChildAtPoint(Me.PointToClient(Control.MousePosition))
    If Not ctrl Is Me.btn_btn Then 
      Exit Sub
    End If

    Me.tool_tip.AutoPopDelay = 30000
    Me.tool_tip.InitialDelay = 500
    Me.tool_tip.ReshowDelay = 100

    ' retrieve last tooltip text
    tooltip_txt = Me.tool_tip.GetToolTip(btn_btn)

    RaiseEvent SetToolTipTextEvent(tooltip_txt)

    Me.tool_tip.SetToolTip(btn_btn, tooltip_txt)

    m_last_tooltip = Now

  End Sub

  Private Sub btn_btn_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_btn.MouseLeave
    Me.tmr_mouse_move.Stop()
  End Sub

#End Region

End Class
