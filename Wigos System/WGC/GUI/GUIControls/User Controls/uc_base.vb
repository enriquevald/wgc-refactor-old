'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_base.vb  
'
' DESCRIPTION:   control base
'
' AUTHOR:        Andreu Juli�
'
' CREATION DATE: 15-APR-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-APR-2002  AJQ    Initial version
'-------------------------------------------------------------------

Imports System.ComponentModel

'XVV -> Add NameSpace Windows.Forms,solve error with IWin32Window variables definitions
Imports System.Windows.Forms

'XVV -> Add NameSpace Windows.Forms,solve error with IWin32Window variables definitions
Imports System.Drawing

Public Class uc_base
  Inherits System.Windows.Forms.UserControl
  Private Const EXTRA_HEIGHT As Integer = 2
  Private Const EXTRA_WIDTH As Integer = 2

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call
    Call InitControls()
    Call ResizeControls()

  End Sub

  'UserControl overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Private WithEvents lbl_sufix As System.Windows.Forms.Label
  Private WithEvents lbl_text As System.Windows.Forms.Label
  Protected WithEvents panel_control As System.Windows.Forms.Panel
  Protected WithEvents lbl_read_only As System.Windows.Forms.Label
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.lbl_text = New System.Windows.Forms.Label()
    Me.lbl_sufix = New System.Windows.Forms.Label()
    Me.panel_control = New System.Windows.Forms.Panel()
    Me.lbl_read_only = New System.Windows.Forms.Label()
    Me.SuspendLayout()
    '
    'lbl_text
    '
    Me.lbl_text.Dock = System.Windows.Forms.DockStyle.Left
    Me.lbl_text.Name = "lbl_text"
    Me.lbl_text.Size = New System.Drawing.Size(100, 320)
    Me.lbl_text.TabIndex = 0
    Me.lbl_text.Text = "Text"
    Me.lbl_text.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_sufix
    '
    Me.lbl_sufix.Dock = System.Windows.Forms.DockStyle.Right
    Me.lbl_sufix.Location = New System.Drawing.Point(524, 0)
    Me.lbl_sufix.Name = "lbl_sufix"
    Me.lbl_sufix.Size = New System.Drawing.Size(100, 320)
    Me.lbl_sufix.TabIndex = 1
    Me.lbl_sufix.Text = "Sufix Text"
    Me.lbl_sufix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'panel_control
    '
    Me.panel_control.BackColor = System.Drawing.SystemColors.Control
    Me.panel_control.Dock = System.Windows.Forms.DockStyle.Fill
    Me.panel_control.Location = New System.Drawing.Point(100, 40)
    Me.panel_control.Name = "panel_control"
    Me.panel_control.Size = New System.Drawing.Size(424, 280)
    Me.panel_control.TabIndex = 6
    '
    'lbl_read_only
    '
    Me.lbl_read_only.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_read_only.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.lbl_read_only.Dock = System.Windows.Forms.DockStyle.Top
    Me.lbl_read_only.ForeColor = System.Drawing.Color.Blue
    Me.lbl_read_only.Location = New System.Drawing.Point(100, 0)
    Me.lbl_read_only.Name = "lbl_read_only"
    Me.lbl_read_only.Size = New System.Drawing.Size(424, 40)
    Me.lbl_read_only.TabIndex = 7
    Me.lbl_read_only.Text = "Read Only Text Value"
    Me.lbl_read_only.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'uc_base
    '
    Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.panel_control, Me.lbl_read_only, Me.lbl_sufix, Me.lbl_text})
    Me.Name = "uc_base"
    Me.Size = New System.Drawing.Size(624, 320)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Private Functions "

  Private Sub InitControls()
    Me.lbl_sufix.Visible = False
    Me.lbl_sufix.Width = 0
    Me.lbl_text.Width = 80
    Me.lbl_text.Visible = True
    Me.Width = 250
    Me.Height = 20
    Me.lbl_read_only.Visible = False
    Call ResizeControls()
  End Sub

  Private Sub ResizeControls()
    Dim ctrl As Control
    'XVV Variable not use Dim idx As Integer
    Dim aux_width As Integer
    'XVV Variable not use Dim aux_left As Integer
    Dim txt_box As TextBox

    'If Me.DesignMode Then
    'Exit Sub
    'End If

    If panel_control.Controls.Count = 0 Then
      Exit Sub
    End If

    If panel_control.Controls.Count > 1 Then
      Err.Raise(1000, "Too many Controls in the panel")
      Exit Sub
    End If

    txt_box = New TextBox()
    txt_box.Font = Me.Font
    Me.Height = txt_box.Height + 2 * EXTRA_HEIGHT

    aux_width = Me.Width
    aux_width = aux_width - lbl_text.Width
    aux_width = aux_width - lbl_sufix.Width
    aux_width = aux_width - 2 * EXTRA_WIDTH


    ctrl = Me.panel_control.Controls(0)

    ctrl.Left = lbl_text.Width + EXTRA_WIDTH
    ctrl.Width = aux_width
    ctrl.Height = txt_box.Height
    ctrl.Top = (Me.Height - ctrl.Height) / 2

    Call ctrl.Refresh()

    If lbl_read_only.Visible Then
      lbl_read_only.Dock = DockStyle.None
      lbl_read_only.Left = lbl_text.Width + EXTRA_WIDTH
      lbl_read_only.Width = aux_width
      lbl_read_only.Height = txt_box.Height
      lbl_read_only.Top = (Me.Height - ctrl.Height) / 2
    End If

    If True Then
      lbl_sufix.Dock = DockStyle.None
      lbl_sufix.Anchor = AnchorStyles.None
      lbl_sufix.Top = 0
      lbl_sufix.Height = Me.Height
      lbl_sufix.Left = lbl_text.Width + aux_width + EXTRA_WIDTH * 2
    End If

  End Sub

  Private Sub panel_control_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Call ResizeControls()
  End Sub

#End Region

#Region " Public Properties "

#Region " Text "

  <Category("EntryField"), Description("Text"), DefaultValue("")> _
  Public Overrides Property Text() As String
    Get
      Return lbl_text.Text
    End Get
    Set(ByVal Value As String)
      lbl_text.Text = Value
      lbl_text.Update()
    End Set
  End Property

  <Category("EntryField"), Description("TextWidth"), DefaultValue(80)> _
  Public Property TextWidth() As Integer
    Get
      Return Me.lbl_text.Width
    End Get
    Set(ByVal Value As Integer)
      Me.lbl_text.Width = Value
      Call ResizeControls()
    End Set
  End Property

  <Category("EntryField"), Description("TextVisible"), DefaultValue(True)> _
Public Property TextVisible() As Boolean
    Get
      Return Me.lbl_text.Visible
    End Get
    Set(ByVal Value As Boolean)
      Me.lbl_text.Visible = Value
    End Set
  End Property

#End Region

#Region " Sufix Text "
  <Category("EntryField"), Description("SufixTextWidth"), DefaultValue("")> _
Public Property SufixText() As String
    Get
      Return Me.lbl_sufix.Text
    End Get
    Set(ByVal Value As String)
      Me.lbl_sufix.Text = Value
    End Set
  End Property

  <Category("EntryField"), Description("SufixTextWidth"), DefaultValue(0)> _
  Public Property SufixTextWidth() As Integer
    Get
      Return Me.lbl_sufix.Width
    End Get
    Set(ByVal Value As Integer)
      Me.lbl_sufix.Width = Value
      Call ResizeControls()
    End Set
  End Property

  <Category("EntryField"), Description("SufixTextVisible"), DefaultValue(False)> _
  Public Property SufixTextVisible() As Boolean
    Get
      Return Me.lbl_sufix.Visible
    End Get
    Set(ByVal Value As Boolean)
      Me.lbl_sufix.Visible = Value
    End Set
  End Property
#End Region

#End Region

  Protected Overrides Sub OnResize(ByVal e As System.EventArgs)
    Call ResizeControls()
  End Sub

  Public Overridable Property IsReadOnly() As Boolean
    Get
      Return Me.lbl_read_only.Visible
    End Get
    Set(ByVal Value As Boolean)
      Me.lbl_read_only.Visible = Value
      Me.panel_control.Visible = Not Value
      Call Me.ResizeControls()
    End Set
  End Property

  Protected Property ReadOnlyText() As String
    Get
      Return Me.lbl_read_only.Text
    End Get
    Set(ByVal Value As String)
      Me.lbl_read_only.Text = Value
    End Set
  End Property

  Protected Property ReadOnlyAlign() As ContentAlignment
    Get
      Return Me.lbl_read_only.TextAlign
    End Get
    Set(ByVal Value As ContentAlignment)
      Me.lbl_read_only.TextAlign = Value
    End Set
  End Property
End Class
