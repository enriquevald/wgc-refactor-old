'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
' 
' MODULE NAME:   uc_combo.vb  
'
' DESCRIPTION:   combo control
'
' AUTHOR:        Andreu Juli�
'
' CREATION DATE: 15-APR-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-APR-2002  AJQ    Initial version
' 26-SEP-2012  XIT    Added Add from Dictionary
' 14-OCT-2013  JBC    Added ToolTip Control
' 15-SEP-2015  SGB    Bug 4502 Fixed - Don't show combos
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports System.ComponentModel
Public Class uc_combo
  Inherits uc_base
  Public Property Table() As DataTable
    Get
      Return m_table
    End Get
    Set(value As DataTable)
      m_table = value
    End Set
  End Property

  Dim m_table As New System.Data.DataTable()
  Dim m_raise_events As Boolean = True
  Dim m_last_index As Integer = -2
  Dim m_last_index_multisite As String = String.Empty
  Dim m_tool_tip As System.Windows.Forms.ToolTip
  Dim m_autocomplete_mode As Boolean
  Dim m_allow_unlisted_values As Boolean
  Dim m_text As String
  Dim m_is_multisite As Boolean = False

#Region " Combo Items "

  Private Sub InitControls(Optional IsMultiSite As Boolean = False)

    Dim col As DataColumn

    m_table = New DataTable()

    m_is_multisite = IsMultiSite

    col = New DataColumn()

    If m_is_multisite Then
      col.DataType = System.Type.GetType("System.String")
    Else
      col.DataType = System.Type.GetType("System.Decimal")
    End If

    col.AllowDBNull = False
    col.Caption = "Id"
    col.ColumnName = "Id"
    col.Unique = True

    m_table.Columns.Add(col)

    col = New DataColumn()
    col.DataType = System.Type.GetType("System.String")
    col.AllowDBNull = False
    col.Caption = "Name"
    col.ColumnName = "Name"
    ' AJQ 26-AUG-2002: Allow duplicates on the name.
    col.Unique = False

    m_table.Columns.Add(col)

    Me.combo.DataSource = m_table
    Me.combo.DisplayMember = "Name"

    Me.Width = 200
    Me.Height = 25

  End Sub

  Public Sub Clear()
    Call m_table.Rows.Clear()
  End Sub

  Public Function Count() As Integer
    Return m_table.Rows.Count
  End Function

  Public Sub Add(ByVal Id As String, ByVal NameId As String)
    Dim row As DataRow

    row = m_table.NewRow()
    row.Item(0) = Id
    row.Item(1) = NameId
    m_table.Rows.Add(row)
  End Sub

  Public Sub Add(ByVal Id As Integer, ByVal NameId As String)
    Dim row As DataRow

    row = m_table.NewRow()
    row.Item(0) = Id
    row.Item(1) = NameId
    m_table.Rows.Add(row)
  End Sub

  Public Sub Add(ByVal Id As Integer, ByVal NlsId As Integer)
    Call Add(Id, NLS_GetString(NlsId))
  End Sub

  Public Sub Add(ByVal NameId As String)
    Call Add(m_table.Rows.Count + 1, NameId)
  End Sub

  Public Sub Add(ByVal Table As DataTable, ByVal ColId As String, ByVal ColName As String)
    Dim idx As Integer
    Dim aux_id As Integer
    Dim aux_name As String

    Me.m_raise_events = False

    Me.combo.BeginUpdate()
    ' RCI 01-JUN-2010: {Begin,End}LoadData() is needed to speed up adding rows. No checks while adding.
    m_table.BeginLoadData()

    For idx = 0 To Table.Rows.Count - 1
      aux_id = CInt(Table.Rows.Item(idx).Item(ColId))
      aux_name = CStr(Table.Rows.Item(idx).Item(ColName))
      Call Add(aux_id, aux_name)
    Next

    m_table.EndLoadData()
    Me.combo.EndUpdate()

    Me.m_raise_events = True

  End Sub

  Public Sub Add(ByVal Table As DataTable, Optional IsMultiSite As Boolean = False)

    If IsMultiSite Then
      Call InitControls(True)
      Call AddWithStringKey(Table, 0, 1)
    Else
      Call Add(Table, 0, 1)
    End If

  End Sub

  Public Sub Add(ByVal Table As DataTable, _
                 ByVal IndexColId As Integer, _
                 ByVal IndexColName As Integer)
    Dim idx As Integer
    Dim aux_id As Integer
    Dim aux_name As String

    Me.combo.DataSource = Nothing
    Me.m_raise_events = False
    ' RCI 01-JUN-2010: {Begin,End}LoadData() is needed to speed up adding rows. No checks while adding.
    m_table.BeginLoadData()

    Try
      For idx = 0 To Table.Rows.Count - 1
        aux_id = CInt(Table.Rows.Item(idx).Item(IndexColId))
        aux_name = CStr(Table.Rows.Item(idx).Item(IndexColName).ToString)
        Call Add(aux_id, aux_name)
      Next

    Finally
      m_table.EndLoadData()
      Me.m_raise_events = True

      Me.combo.DataSource = m_table
      Me.combo.DisplayMember = "Name"

    End Try


  End Sub

  Public Sub AddWithStringKey(ByVal Table As DataTable, _
                              ByVal IndexColId As Integer, _
                              ByVal IndexColName As Integer)
    Dim idx As Integer
    Dim aux_id As String
    Dim aux_name As String

    Me.combo.DataSource = Nothing
    Me.m_raise_events = False

    m_table.BeginLoadData()

    Try
      For idx = 0 To Table.Rows.Count - 1
        aux_id = CStr(Table.Rows.Item(idx).Item(0))
        aux_name = CStr(Table.Rows.Item(idx).Item(1).ToString)
        Call Add(aux_id, aux_name)
      Next

    Finally
      m_table.EndLoadData()
      Me.m_raise_events = True

      Me.combo.DataSource = m_table
      Me.combo.DisplayMember = "Name"

    End Try
  End Sub

  Public Sub Add(ByVal Dic As Dictionary(Of Integer, String))

    Me.m_raise_events = False
    For Each _pair As KeyValuePair(Of Integer, String) In Dic
      Call Add(_pair.Key, _pair.Value)
    Next
    Me.m_raise_events = True
  End Sub

#End Region

#Region " Value "

  ''' <summary>
  ''' Return the key value in string format
  ''' </summary>
  ''' <param name="Index"></param>
  ''' <value></value>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Property ValueString(Optional ByVal Index As Integer = -1) As String
    Get
      Dim row As DataRowView

      If Index = -1 Then
        Index = combo.SelectedIndex
      End If

      If Index <> -1 Then
        row = combo.Items(Index)

        Return row.Item(0)
      Else
        Return 0
      End If
    End Get

    Set(ByVal NewValue As String)
      Dim idx As Integer

      If Index <> -1 Then
        Call Err.Raise(100000, "It is not possible to change the Value")
      End If

      For idx = 0 To m_table.Rows.Count - 1
        If m_table.Rows.Item(idx).Item(0) = NewValue Then
          combo.SelectedIndex = idx
          Me.ReadOnlyText = Me.TextValue

          Exit Property
        End If
      Next
      '
      combo.SelectedIndex = -1
    End Set
  End Property

  Public Property Value(Optional ByVal Index As Integer = -1) As Integer
    Get
      Dim row As DataRowView

      If Index = -1 Then
        Index = combo.SelectedIndex
      End If

      If Index <> -1 Then
        row = combo.Items(Index)

        Return row.Item(0)
      Else
        Return 0
      End If
    End Get

    Set(ByVal NewValue As Integer)
      Dim idx As Integer

      If Index <> -1 Then
        Call Err.Raise(100000, "It is not possible to change the Value")
      End If

      For idx = 0 To m_table.Rows.Count - 1
        If m_table.Rows.Item(idx).Item(0) = NewValue Then
          combo.SelectedIndex = idx
          Me.ReadOnlyText = Me.TextValue

          Exit Property
        End If
      Next
      '
      combo.SelectedIndex = -1
    End Set
  End Property

  Public Property TextValue(Optional ByVal Index As Integer = -1) As String

    Get
      Dim row As DataRowView

      If Index = -1 Then
        Index = combo.SelectedIndex
      End If

      If Index <> -1 Then
        row = combo.Items(Index)

        Return row.Item(1)
      Else
        Return vbNullString
      End If
    End Get

    Set(ByVal NewValue As String)
      If Index <> -1 Then
        Call Err.Raise(100000, "It is not possible to change the TextValue")
      End If
      If m_table.Rows.Count > 0 Then
        combo.SelectedIndex = combo.FindStringExact(NewValue)
        Me.ReadOnlyText = Me.TextValue
      End If
    End Set

  End Property

  Public Property SelectedIndex() As Integer
    Get
      Return combo.SelectedIndex
    End Get
    Set(ByVal Value As Integer)
      combo.SelectedIndex = Value
      Me.ReadOnlyText = Me.TextValue
    End Set
  End Property

  Public ReadOnly Property DataSource() As DataTable
    Get
      Return m_table
    End Get
  End Property

#End Region

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call
    Call InitControls()

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Private WithEvents combo As WSI.Common.uc_auto_combobox
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.combo = New WSI.Common.uc_auto_combobox()
    Me.panel_control.SuspendLayout()
    Me.SuspendLayout()
    '
    'lbl_read_only
    '
    Me.lbl_read_only.Dock = System.Windows.Forms.DockStyle.None
    Me.lbl_read_only.Location = New System.Drawing.Point(84, 4)
    Me.lbl_read_only.Size = New System.Drawing.Size(272, 20)
    '
    'panel_control
    '
    Me.panel_control.Controls.AddRange(New System.Windows.Forms.Control() {Me.combo})
    Me.panel_control.Location = New System.Drawing.Point(0, 0)
    Me.panel_control.Size = New System.Drawing.Size(360, 24)
    '
    'combo
    '
    Me.combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.combo.Location = New System.Drawing.Point(82, 2)
    Me.combo.MaxDropDownItems = 10
    Me.combo.Name = "combo"
    Me.combo.Size = New System.Drawing.Size(276, 21)
    Me.combo.TabIndex = 0
    '
    'uc_combo
    '
    Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.lbl_read_only, Me.panel_control})
    Me.IsReadOnly = True
    Me.Name = "uc_combo"
    Me.Size = New System.Drawing.Size(360, 24)
    Me.SufixTextVisible = True
    Me.panel_control.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

  Public Overrides Property IsReadOnly() As Boolean
    Get
      Return MyBase.IsReadOnly
    End Get
    Set(ByVal Value As Boolean)
      MyBase.ReadOnlyText = Me.TextValue
      MyBase.IsReadOnly = Value
    End Set
  End Property

  <BrowsableAttribute(True)> _
  Public Property AutoCompleteMode() As Boolean
    Get
      Return (Me.combo.DropDownStyle = Windows.Forms.ComboBoxStyle.DropDown)
    End Get
    Set(ByVal value As Boolean)
      If value Then
        Me.combo.DropDownStyle = Windows.Forms.ComboBoxStyle.DropDown
      Else
        Me.combo.DropDownStyle = Windows.Forms.ComboBoxStyle.DropDownList
      End If
    End Set
  End Property

  Public Property AllowUnlistedValues() As Boolean
    Get
      Return m_allow_unlisted_values
    End Get
    Set(ByVal value As Boolean)
      m_allow_unlisted_values = value
    End Set
  End Property

  Public Property TextCombo() As String
    Get
      Return m_text
    End Get
    Set(ByVal value As String)
      m_text = value
      combo.Text = m_text
    End Set
  End Property


  Public Sub ResetLastIndex()
    m_last_index = -2
    m_last_index_multisite = String.Empty
  End Sub

#Region " Events "
  Public Event ValueChangedEvent()
  Private Sub combo_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles combo.SelectedValueChanged
    If m_raise_events Then
      If Not m_is_multisite Then
        If Me.Value <> m_last_index Then
          m_last_index = Me.Value
          RaiseEvent ValueChangedEvent()
        End If
      Else
        If Me.ValueString <> m_last_index_multisite Then
          m_last_index_multisite = Me.ValueString
          RaiseEvent ValueChangedEvent()
        End If
      End If
    End If
  End Sub
  Public Sub SetToolBoxText(ByVal Text As String, ByVal Title As String)

    If m_tool_tip Is Nothing Then
      m_tool_tip = New System.Windows.Forms.ToolTip()

      With m_tool_tip
        .InitialDelay = 2000
        .ReshowDelay = 3000
        .ShowAlways = True
        .AutomaticDelay = 1000
        .AutoPopDelay = 6000
      End With
    End If
    m_tool_tip.ToolTipTitle = Title
    m_tool_tip.SetToolTip(Me.combo, Text)
  End Sub

#End Region

End Class
