﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_jackpot_type_sel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ch_hidden = New System.Windows.Forms.CheckBox()
    Me.ch_main = New System.Windows.Forms.CheckBox()
    Me.ch_prize_sharing = New System.Windows.Forms.CheckBox()
    Me.ch_happy_hour = New System.Windows.Forms.CheckBox()
    Me.gb_jackpot_type = New System.Windows.Forms.GroupBox()
    Me.gb_jackpot_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'ch_hidden
    '
    Me.ch_hidden.AutoSize = True
    Me.ch_hidden.Location = New System.Drawing.Point(6, 42)
    Me.ch_hidden.Name = "ch_hidden"
    Me.ch_hidden.Size = New System.Drawing.Size(65, 17)
    Me.ch_hidden.TabIndex = 1
    Me.ch_hidden.Text = "xHidden"
    Me.ch_hidden.UseVisualStyleBackColor = True
    '
    'ch_main
    '
    Me.ch_main.AutoSize = True
    Me.ch_main.Location = New System.Drawing.Point(6, 19)
    Me.ch_main.Name = "ch_main"
    Me.ch_main.Size = New System.Drawing.Size(54, 17)
    Me.ch_main.TabIndex = 0
    Me.ch_main.Text = "xMain"
    Me.ch_main.UseVisualStyleBackColor = True
    '
    'ch_prize_sharing
    '
    Me.ch_prize_sharing.AutoSize = True
    Me.ch_prize_sharing.Location = New System.Drawing.Point(6, 65)
    Me.ch_prize_sharing.Name = "ch_prize_sharing"
    Me.ch_prize_sharing.Size = New System.Drawing.Size(90, 17)
    Me.ch_prize_sharing.TabIndex = 2
    Me.ch_prize_sharing.Text = "xPrizeSharing"
    Me.ch_prize_sharing.UseVisualStyleBackColor = True
    '
    'ch_happy_hour
    '
    Me.ch_happy_hour.AutoSize = True
    Me.ch_happy_hour.Location = New System.Drawing.Point(6, 88)
    Me.ch_happy_hour.Name = "ch_happy_hour"
    Me.ch_happy_hour.Size = New System.Drawing.Size(85, 17)
    Me.ch_happy_hour.TabIndex = 3
    Me.ch_happy_hour.Text = "xHappyHour"
    Me.ch_happy_hour.UseVisualStyleBackColor = True
    '
    'gb_jackpot_type
    '
    Me.gb_jackpot_type.Controls.Add(Me.ch_hidden)
    Me.gb_jackpot_type.Controls.Add(Me.ch_happy_hour)
    Me.gb_jackpot_type.Controls.Add(Me.ch_main)
    Me.gb_jackpot_type.Controls.Add(Me.ch_prize_sharing)
    Me.gb_jackpot_type.Location = New System.Drawing.Point(4, 4)
    Me.gb_jackpot_type.Name = "gb_jackpot_type"
    Me.gb_jackpot_type.Size = New System.Drawing.Size(138, 120)
    Me.gb_jackpot_type.TabIndex = 4
    Me.gb_jackpot_type.TabStop = False
    Me.gb_jackpot_type.Text = "xJackpotType"
    '
    'uc_jackpot_type_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.gb_jackpot_type)
    Me.Name = "uc_jackpot_type_sel"
    Me.Size = New System.Drawing.Size(149, 129)
    Me.gb_jackpot_type.ResumeLayout(False)
    Me.gb_jackpot_type.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ch_hidden As System.Windows.Forms.CheckBox
  Friend WithEvents ch_main As System.Windows.Forms.CheckBox
  Friend WithEvents ch_prize_sharing As System.Windows.Forms.CheckBox
  Friend WithEvents ch_happy_hour As System.Windows.Forms.CheckBox
  Friend WithEvents gb_jackpot_type As System.Windows.Forms.GroupBox

End Class
