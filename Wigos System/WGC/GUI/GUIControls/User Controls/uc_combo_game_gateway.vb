'-------------------------------------------------------------------
' Copyright � 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_combo_game_gateway
' DESCRIPTION:   Filter of games 
' AUTHOR:        Fernando Jim�nez
' CREATION DATE: 05-JAN-2016
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 05-JAN-2016  FJC    Initial version
' 11-JAN-2016  FJC    Bug 8218:BonoPlay: No se puede acceder a ninguna pantalla
' 04-OCT-2016  JBP    PBI 18464:PariPlay - Ajustar pantallas de BonoPlay
'--------------------------------------------------------------------

Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports System.Windows.Forms

Public Class uc_combo_game_gateway

#Region " Members"

  Private m_datatable_games As DataTable
#End Region

#Region " Private functions"

  ' PURPOSE : Set combo with Dictionary
  '
  '  PARAMS :
  '     - INPUT :
  '           - uc_Combo
  '           - Datable
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  Public Sub SetCombo(ByVal combo As uc_combo, _
                      ByVal Dictionary As Dictionary(Of Integer, String))

    combo.Clear()
    If Dictionary.Count > 0 Then
      combo.Add(-1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353))

      'Me.m_raise_events = False
      For Each _pair As KeyValuePair(Of Integer, String) In Dictionary
        Call combo.Add(_pair.Key, _pair.Value)
      Next
      'Me.m_raise_events = True
    End If
  End Sub ' SetCombo

  ' PURPOSE : Set combo with Datatable
  '
  '  PARAMS :
  '     - INPUT :
  '           - uc_Combo
  '           - Datable
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  Public Sub SetCombo(ByVal combo As uc_combo, ByVal DataTable As DataTable)
    combo.Clear()

    If DataTable.Rows.Count > 0 Then
      combo.Add(-1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353))
      combo.Add(DataTable)
    End If

  End Sub ' SetCombo

  ' PURPOSE: Resize some controls for a correct visualization
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ResizeControl()
    Me.gb_providers_games.Size = New System.Drawing.Size(Me.Size.Width - 5, Me.Size.Height - 5)
    Me.uc_combo_providers.Size = New System.Drawing.Size(Me.gb_providers_games.Width - Me.uc_combo_providers.Location.X - 5, _
                                                Me.uc_combo_providers.Size.Height)

    Me.uc_combo_games.Size = Me.uc_combo_providers.Size

  End Sub ' ResizeControl

#End Region

#Region " Public funtions"

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    ResizeControl()
  End Sub

  ' PURPOSE: Initialize Control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub Init()

    Dim _partners As Dictionary(Of Integer, String)

    _partners = WSI.Common.Partner.Partners()

    'NLS
    Me.gb_providers_games.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7048)  ' 
    Me.uc_combo_providers.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5418)  ' Providers
    Me.uc_combo_games.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2008)      ' Games 

    ' Load Games
    m_datatable_games = New DataTable
    m_datatable_games = GUI_GetTableUsingSQL("SELECT GG_GAME_ID, GG_NAME, GG_PARTNER_ID FROM GAMEGATEWAY_GAMES " + GetGameWhere(_partners) + " ORDER BY GG_NAME", Integer.MaxValue)

    ' Load Providers and fill combo providers
    Call SetCombo(Me.uc_combo_providers, _partners)

    'Handlers
    AddHandler Me.Resize, AddressOf UcComboGamesResizing

    'Defaults
    Call Me.SetDefaultValues()

    'ResizeControl
    Call ResizeControl()

  End Sub ' Init

  ' PURPOSE: Set Default Values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub SetDefaultValues()
    If Me.uc_combo_providers.Count > 0 Then
      Me.uc_combo_providers.SelectedIndex = 0
    End If

    If Me.uc_combo_games.Count > 0 Then
      Me.uc_combo_games.SelectedIndex = 0
    End If
  End Sub ' SetDefaultValues

  ' PURPOSE: Get Id (Value) from selected value of combobox (PROVIDERS)
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Public Function GetSelectedProviderId() As String
    Try
      If Me.uc_combo_providers.Count > 0 AndAlso _
         Me.uc_combo_providers.SelectedIndex > 0 Then

        Return Me.uc_combo_providers.Value

      End If
    Catch ex As Exception

      WSI.Common.Log.Exception(ex)
    End Try

    Return String.Empty
  End Function ' GetSelectedProviderId

  ' PURPOSE: Get Text (TextValue) from selected value of combobox (PROVIDERS)
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Public Function GetSelectedProviderValue() As String
    Try
      If Me.uc_combo_providers.Count > 0 Then

        Return Me.uc_combo_providers.TextValue

      End If
    Catch ex As Exception

      WSI.Common.Log.Exception(ex)
    End Try

    Return String.Empty
  End Function  ' GetSelectedProviderValue

  ' PURPOSE: Get Id (Value) from selected value of combobox (GAMES)
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Public Function GetSelectedGameId() As String
    Try
      If Me.uc_combo_games.Count > 0 AndAlso _
         Me.uc_combo_games.SelectedIndex > 0 Then

        Return Me.uc_combo_games.Value

      End If
    Catch ex As Exception

      WSI.Common.Log.Exception(ex)
    End Try

    Return String.Empty
  End Function ' GetSelectedGameId

  ' PURPOSE: Get Text (TextValue) from selected value of combobox (GAMES)
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Public Function GetSelectedGameValue() As String
    Try
      If Me.uc_combo_games.Count > 0 Then

        Return Me.uc_combo_games.TextValue

      End If
    Catch ex As Exception

      WSI.Common.Log.Exception(ex)
    End Try

    Return String.Empty
  End Function  ' GetSelectedGameValue

#End Region

#Region " Events"

  ' PURPOSE: Controles events por resizing user control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub UcComboGamesResizing(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
    ResizeControl()
  End Sub

  ' PURPOSE: Controles events when combo providers changes selected value
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub uc_combo_providers_ValueChangedEvent() Handles uc_combo_providers.ValueChangedEvent
    Dim _str_sql As StringBuilder
    Dim _dt_rows() As DataRow
    Dim _dt_table_aux As DataTable

    _str_sql = New StringBuilder
    _dt_table_aux = New DataTable

    Try
      If uc_combo_providers.SelectedIndex > 0 Then
        _dt_table_aux = Me.m_datatable_games.Clone
        _dt_rows = Me.m_datatable_games.Select("GG_PARTNER_ID = " & uc_combo_providers.Value)
        For Each _dr_row As DataRow In _dt_rows
          _dt_table_aux.ImportRow(_dr_row)
        Next
      Else
        _dt_table_aux = Me.m_datatable_games.Copy()
      End If

      SetCombo(Me.uc_combo_games, _dt_table_aux)

    Catch _ex As Exception

      WSI.Common.Log.Exception(_ex)

    End Try
  End Sub

#End Region

  Private Function GetGameWhere(Dictionary As Dictionary(Of Integer, String)) As String
    Dim _where As String
    Dim _multiple_providers As Boolean
    Dim _separator As String

    _where = String.Empty
    _separator = String.Empty

    If Dictionary.Count > 0 Then
      _where = "WHERE GG_PARTNER_ID IN ("

      For Each _pair As KeyValuePair(Of Integer, String) In Dictionary

        If _multiple_providers Then
          _separator = ", "
        End If

        _where += _separator & _pair.Key.ToString()

        _multiple_providers = True
      Next
      _where += ")"
    End If

    Return _where
  End Function

End Class ' uc_combo_game_gateway
