﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_checked_several_all_list
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_tittle = New System.Windows.Forms.GroupBox()
    Me.opt_all = New System.Windows.Forms.RadioButton()
    Me.opt_several = New System.Windows.Forms.RadioButton()
    Me.dg_checked_list = New GUI_Controls.uc_grid()
    Me.gb_tittle.SuspendLayout
    Me.SuspendLayout
    '
    'gb_tittle
    '
    Me.gb_tittle.Controls.Add(Me.opt_all)
    Me.gb_tittle.Controls.Add(Me.opt_several)
    Me.gb_tittle.Controls.Add(Me.dg_checked_list)
    Me.gb_tittle.Dock = System.Windows.Forms.DockStyle.Fill
    Me.gb_tittle.Location = New System.Drawing.Point(0, 0)
    Me.gb_tittle.Name = "gb_tittle"
    Me.gb_tittle.Size = New System.Drawing.Size(352, 168)
    Me.gb_tittle.TabIndex = 0
    Me.gb_tittle.TabStop = false
    Me.gb_tittle.Text = "xTitle"
    '
    'opt_all
    '
    Me.opt_all.Location = New System.Drawing.Point(8, 45)
    Me.opt_all.Name = "opt_all"
    Me.opt_all.Size = New System.Drawing.Size(64, 24)
    Me.opt_all.TabIndex = 1
    Me.opt_all.Text = "xAll"
    '
    'opt_several
    '
    Me.opt_several.Location = New System.Drawing.Point(8, 17)
    Me.opt_several.Name = "opt_several"
    Me.opt_several.Size = New System.Drawing.Size(72, 24)
    Me.opt_several.TabIndex = 0
    Me.opt_several.Text = "xSeveral"
    '
    'dg_checked_list
    '
    Me.dg_checked_list.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
    Me.dg_checked_list.CurrentCol = -1
    Me.dg_checked_list.CurrentRow = -1
    Me.dg_checked_list.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_checked_list.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_checked_list.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_checked_list.Location = New System.Drawing.Point(86, 17)
    Me.dg_checked_list.Name = "dg_checked_list"
    Me.dg_checked_list.PanelRightVisible = false
    Me.dg_checked_list.Redraw = true
    Me.dg_checked_list.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_checked_list.Size = New System.Drawing.Size(260, 137)
    Me.dg_checked_list.Sortable = false
    Me.dg_checked_list.SortAscending = true
    Me.dg_checked_list.SortByCol = 0
    Me.dg_checked_list.TabIndex = 2
    Me.dg_checked_list.TabStop = false
    Me.dg_checked_list.ToolTipped = true
    Me.dg_checked_list.TopRow = -2
    Me.dg_checked_list.WordWrap = false
    '
    'uc_checked_several_all_list
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.gb_tittle)
    Me.Name = "uc_checked_several_all_list"
    Me.Size = New System.Drawing.Size(352, 168)
    Me.gb_tittle.ResumeLayout(false)
    Me.ResumeLayout(false)

End Sub
  Friend WithEvents gb_tittle As System.Windows.Forms.GroupBox
  Friend WithEvents opt_all As System.Windows.Forms.RadioButton
  Friend WithEvents opt_several As System.Windows.Forms.RadioButton
  Friend WithEvents dg_checked_list As GUI_Controls.uc_grid

End Class
