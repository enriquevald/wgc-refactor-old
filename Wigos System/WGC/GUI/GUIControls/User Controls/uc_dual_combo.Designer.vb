<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_dual_combo
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.chk_1 = New System.Windows.Forms.CheckBox
    Me.cmb_2 = New GUI_Controls.uc_combo
    Me.cmb_1 = New GUI_Controls.uc_combo
    Me.uc_ef_1 = New GUI_Controls.uc_entry_field
    Me.SuspendLayout()
    '
    'chk_1
    '
    Me.chk_1.Location = New System.Drawing.Point(50, 8)
    Me.chk_1.Name = "chk_1"
    Me.chk_1.Size = New System.Drawing.Size(15, 14)
    Me.chk_1.TabIndex = 0
    Me.chk_1.UseVisualStyleBackColor = True
    '
    'cmb_2
    '
    Me.cmb_2.AllowUnlistedValues = False
    Me.cmb_2.AutoCompleteMode = False
    Me.cmb_2.IsReadOnly = False
    Me.cmb_2.Location = New System.Drawing.Point(0, 3)
    Me.cmb_2.Name = "cmb_2"
    Me.cmb_2.SelectedIndex = -1
    Me.cmb_2.Size = New System.Drawing.Size(44, 24)
    Me.cmb_2.SufixText = "Sufix Text"
    Me.cmb_2.SufixTextVisible = True
    Me.cmb_2.TabIndex = 3
    Me.cmb_2.TextVisible = False
    Me.cmb_2.TextWidth = 0
    '
    'cmb_1
    '
    Me.cmb_1.AllowUnlistedValues = False
    Me.cmb_1.AutoCompleteMode = False
    Me.cmb_1.Enabled = False
    Me.cmb_1.IsReadOnly = False
    Me.cmb_1.Location = New System.Drawing.Point(179, 3)
    Me.cmb_1.Margin = New System.Windows.Forms.Padding(0)
    Me.cmb_1.Name = "cmb_1"
    Me.cmb_1.SelectedIndex = -1
    Me.cmb_1.Size = New System.Drawing.Size(218, 24)
    Me.cmb_1.SufixText = "Sufix Text"
    Me.cmb_1.SufixTextVisible = True
    Me.cmb_1.TabIndex = 2
    Me.cmb_1.TextWidth = 0
    '
    'uc_ef_1
    '
    Me.uc_ef_1.DoubleValue = 0
    Me.uc_ef_1.Enabled = False
    Me.uc_ef_1.IntegerValue = 0
    Me.uc_ef_1.IsReadOnly = False
    Me.uc_ef_1.Location = New System.Drawing.Point(68, 3)
    Me.uc_ef_1.Margin = New System.Windows.Forms.Padding(0)
    Me.uc_ef_1.Name = "uc_ef_1"
    Me.uc_ef_1.PlaceHolder = Nothing
    Me.uc_ef_1.Size = New System.Drawing.Size(111, 24)
    Me.uc_ef_1.SufixText = "Sufix Text"
    Me.uc_ef_1.SufixTextVisible = True
    Me.uc_ef_1.TabIndex = 1
    Me.uc_ef_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_ef_1.TextValue = ""
    Me.uc_ef_1.TextVisible = False
    Me.uc_ef_1.TextWidth = 0
    Me.uc_ef_1.Value = ""
    Me.uc_ef_1.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_dual_combo
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
    Me.Controls.Add(Me.cmb_2)
    Me.Controls.Add(Me.cmb_1)
    Me.Controls.Add(Me.uc_ef_1)
    Me.Controls.Add(Me.chk_1)
    Me.Name = "uc_dual_combo"
    Me.Size = New System.Drawing.Size(400, 30)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_ef_1 As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_1 As GUI_Controls.uc_combo
  Friend WithEvents chk_1 As System.Windows.Forms.CheckBox

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    m_control_mode = UC_MODE.MODE_ADVANCED

  End Sub
  Friend WithEvents cmb_2 As GUI_Controls.uc_combo
End Class
