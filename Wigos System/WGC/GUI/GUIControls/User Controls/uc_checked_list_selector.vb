' -------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd. 
' -------------------------------------------------------------------
'
' MODULE NAME:   uc_checked_list_selector.vb
'
' DESCRIPTION:   Check list control with order grid selector
'
' AUTHOR:        Didac Campanals Subirats
'
' CREATION DATE: 22-JUL-2014
'
' REVISION HISTORY:
'
' Date         Author Description 
' -----------  ------ -----------------------------------------------
' 22-JUL-2014  DCS    Initial version 
' -------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc

Public Class uc_checked_list_selector

#Region " Constants "
  ' Grid Constants
  Private Const SELECTED_ITEMS_GRID_COLUMNS As Integer = 2
  Private Const SELECTED_ITEMS_GRID_HEADER_ROWS As Integer = 0

  Private Const SELECTED_ITEMS_GRID_COLUMN_KEY = 0
  Private Const SELECTED_ITEMS_GRID_COLUMN_DESCRIPTION = 1

  Private Const DEFAULT_WIDTH_COLUMN_DESCRIPTION As Integer = 5000
  Private Const DEFAULT_WIDTH_COLUMN_KEY As Integer = 0

  ' DataTable Constants
  Private Const DATA_COLUMN_KEY As String = "KEY"
  Private Const DATA_COLUMN_LEVEL As String = "LEVEL"
  Private Const DATA_COLUMN_DESCRIPTION As String = "DESCRIPTION"
  Private Const DATA_COLUMN_SELECTED As String = "SELECTED"

#End Region ' Constants

#Region " Members "

  Dim m_dt_items As DataTable
  Dim m_deleted_selected_items As Boolean

#End Region ' Members

#Region " Public Properties "

  Public Sub New()

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControl()

  End Sub

  Public Property DeleteSelectedItems() As Boolean

    Get
      Return Me.m_deleted_selected_items
    End Get

    Set(ByVal Value As Boolean)
      Me.m_deleted_selected_items = Value
    End Set

  End Property

  Public Property GroupBoxLeftText() As String

    Get
      Return Me.uc_check_list.gb_checked_list.Text
    End Get

    Set(ByVal Value As String)
      Me.uc_check_list.gb_checked_list.Text = Value
    End Set

  End Property

  Public Property GroupBoxRightText() As String

    Get
      Return Me.gb_selected_items.Text
    End Get

    Set(ByVal Value As String)
      Me.gb_selected_items.Text = Value
    End Set

  End Property

  Public Property SetLevels() As Integer

    Get
      ' Not public get property
    End Get

    Set(ByVal Value As Integer)
      Me.uc_check_list.SetLevels = Value
    End Set

  End Property

  Public Property GetNumItemsSelected() As Integer

    Get
      Return Me.m_dt_items.Select(DATA_COLUMN_SELECTED & " = " & True).Length
    End Get

    Set(ByVal Value As Integer)
      ' Not public set property
    End Set

  End Property


  Public Property GetItemsSelected() As DataTable

    Get
      Return GetItems_SelectedList()
    End Get

    Set(ByVal Value As DataTable)
      ' Not public set property
    End Set

  End Property



#End Region ' Public Properties

#Region " Public Functions "

  Public Function AddItem(ByVal KeyItem As Integer, ByVal LevelItem As Integer, ByVal DescItem As String, Optional ByVal IsSelectedItem As Boolean = False) As Boolean
    Dim _row As DataRow

    Try

      ' Add Item in Datatable
      _row = m_dt_items.NewRow
      _row.Item(DATA_COLUMN_KEY) = KeyItem
      _row.Item(DATA_COLUMN_LEVEL) = LevelItem
      _row.Item(DATA_COLUMN_DESCRIPTION) = DescItem
      _row.Item(DATA_COLUMN_SELECTED) = IsSelectedItem

      If CheckValues(_row) Then

        ' Add item in datatable
        m_dt_items.Rows.Add(_row)

        'Add Item in Check Grid
        If Not m_deleted_selected_items Or Not IsSelectedItem Then

          AddItem_CheckList(LevelItem, KeyItem, DescItem)

        End If

        'Add Item in Selection Grid
        If IsSelectedItem Then

          AddItem_SelectedList(KeyItem, DescItem)

        End If

      Else
        Return False
      End If

      Return True

    Catch ex As Exception
      Return False
    End Try

  End Function ' AddItem

#End Region ' Public Functions

#Region " Private Functions "

  Private Sub InitControl()

    Me.btn_clear.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4969)           ' 4969 "Clear all"

    Me.uc_check_list.ColumnWidth(uc_checked_list.GRID_COLUMN_DESC, DEFAULT_WIDTH_COLUMN_DESCRIPTION)

    ' Default Group Box names
    Me.gb_selected_items.Text = String.Empty
    Me.uc_check_list.gb_checked_list.Text = String.Empty

    ' Initialize Grid
    Call GUI_DG_SelectedStyleSheet()

    Call CreateDataTable()

    ' By default no delete items when is selected
    m_deleted_selected_items = False

  End Sub ' InitControl

  ' PURPOSE: Create DataTable to mantain items in both grids
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Sub CreateDataTable()

    m_dt_items = New DataTable
    m_dt_items.Columns.Add(DATA_COLUMN_KEY, GetType(Integer))
    m_dt_items.Columns.Add(DATA_COLUMN_LEVEL, GetType(Integer))
    m_dt_items.Columns.Add(DATA_COLUMN_DESCRIPTION, GetType(String))
    m_dt_items.Columns.Add(DATA_COLUMN_SELECTED, GetType(Boolean))

  End Sub ' CreateDataTable


  ' PURPOSE: Format appearance of the selection data grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Sub GUI_DG_SelectedStyleSheet()

    With Me.dg_selected_list

      Call .Init(SELECTED_ITEMS_GRID_COLUMNS, SELECTED_ITEMS_GRID_HEADER_ROWS, False)

      .Sortable = False

      ' Key
      .Column(SELECTED_ITEMS_GRID_COLUMN_KEY).Width = DEFAULT_WIDTH_COLUMN_KEY
      .Column(SELECTED_ITEMS_GRID_COLUMN_KEY).HighLightWhenSelected = True
      .Column(SELECTED_ITEMS_GRID_COLUMN_KEY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Description
      .Column(SELECTED_ITEMS_GRID_COLUMN_DESCRIPTION).Width = DEFAULT_WIDTH_COLUMN_DESCRIPTION
      .Column(SELECTED_ITEMS_GRID_COLUMN_DESCRIPTION).HighLightWhenSelected = True
      .Column(SELECTED_ITEMS_GRID_COLUMN_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub 'GUI_DG_SelectedStyleSheet

  ' PURPOSE: Create datatable only with items are selected
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Function GetItems_SelectedList() As DataTable
    Dim _dt_items As DataTable
    Dim _dr As DataRow

    If Me.m_dt_items.Select(DATA_COLUMN_SELECTED & " = " & True).Length > 0 Then

      _dt_items = New DataTable

      For Each _dr In Me.m_dt_items.Select(DATA_COLUMN_SELECTED & " = " & True)

        _dt_items.Rows.Add(_dr(DATA_COLUMN_KEY), _dr(DATA_COLUMN_DESCRIPTION))

      Next
    Else
      _dt_items = Nothing
    End If

    Return _dt_items

  End Function
    
  ' PURPOSE: Add item in Check control
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Sub AddItem_CheckList(ByVal LevelItem As Integer, ByVal KeyItem As Integer, ByVal DescItem As String)

    Me.uc_check_list.ReDraw(False)

    Me.uc_check_list.Add(LevelItem, KeyItem, DescItem)

    If Me.uc_check_list.Count > 0 Then
      Me.uc_check_list.CurrentRow(0)
      Me.uc_check_list.ReDraw(True)
    End If

  End Sub ' AddItem_CheckList

  ' PURPOSE: Add Item in Selected grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - Key
  '           - Description
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Sub AddItem_SelectedList(ByVal KeyItem As Integer, ByVal DescItem As String)
    Dim _idx_row As Integer

    Me.dg_selected_list.Redraw = False

    _idx_row = Me.dg_selected_list.AddRow()

    Me.dg_selected_list.Cell(_idx_row, SELECTED_ITEMS_GRID_COLUMN_KEY).Value = KeyItem
    Me.dg_selected_list.Cell(_idx_row, SELECTED_ITEMS_GRID_COLUMN_DESCRIPTION).Value = DescItem

    If Me.dg_selected_list.NumRows > 0 Then
      Me.dg_selected_list.CurrentRow = _idx_row
      Me.dg_selected_list.Redraw = True
    End If

  End Sub ' AddItem_SelectedList

  ' PURPOSE: Delete Item in Selected grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxRow
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     

  Private Sub DeleteItem_SelectedList(ByVal IdxRow As Integer)
    Me.dg_selected_list.Redraw = False

    Me.dg_selected_list.DeleteRow(IdxRow)

    If Me.dg_selected_list.NumRows > 0 Then
      Me.dg_selected_list.CurrentRow = IdxRow
      Me.dg_selected_list.Redraw = True
    End If

  End Sub ' DeleteItem_SelectedList

  ' PURPOSE: Check if all data in that row is ok to add in both grids
  '
  '  PARAMS:
  '     - INPUT:
  '           - Datarow
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     - Boolean
  '     

  Private Function CheckValues(ByVal row As DataRow) As Boolean
    Dim _all_data_ok As Boolean
    _all_data_ok = True

    If _all_data_ok And row(DATA_COLUMN_KEY) > 0 Then
      _all_data_ok = False
    End If

    If _all_data_ok And row(DATA_COLUMN_LEVEL) > 0 Then
      _all_data_ok = False
    End If

    Return _all_data_ok

  End Function ' CheckValues

  ' PURPOSE: Switch position rows between both indexs in params
  '
  '  PARAMS:
  '     - INPUT:
  '           - Datarow
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     - Boolean
  '  

  Private Sub SelectedList_SwitchRows(ByVal IdxRowSource, ByVal IdxRowTarget)
    Dim _key As Integer
    Dim _desc As String

    Try
      _key = Me.dg_selected_list.Cell(IdxRowTarget, SELECTED_ITEMS_GRID_COLUMN_KEY).Value
      _desc = Me.dg_selected_list.Cell(IdxRowTarget, SELECTED_ITEMS_GRID_COLUMN_DESCRIPTION).Value

      Me.dg_selected_list.Cell(IdxRowTarget, SELECTED_ITEMS_GRID_COLUMN_KEY).Value = Me.dg_selected_list.Cell(IdxRowSource, SELECTED_ITEMS_GRID_COLUMN_KEY).Value
      Me.dg_selected_list.Cell(IdxRowTarget, SELECTED_ITEMS_GRID_COLUMN_DESCRIPTION).Value = Me.dg_selected_list.Cell(IdxRowSource, SELECTED_ITEMS_GRID_COLUMN_DESCRIPTION).Value

      Me.dg_selected_list.Cell(IdxRowSource, SELECTED_ITEMS_GRID_COLUMN_KEY).Value = _key
      Me.dg_selected_list.Cell(IdxRowSource, SELECTED_ITEMS_GRID_COLUMN_DESCRIPTION).Value = _desc

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
    End Try

  End Sub ' SelectedList_SwitchRows

#End Region ' Private Functions

#Region " Private Events "

  Private Sub btn_clear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_clear.Click
    Dim _idx_row As Integer

    If m_dt_items.Rows.Count > 0 Then
      For _idx_row = 0 To m_dt_items.Rows.Count
        m_dt_items.Rows(_idx_row).Item(DATA_COLUMN_SELECTED) = False
      Next
    End If

    Me.dg_selected_list.Clear()

  End Sub ' btn_clear_Click

  Private Sub btn_remove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_remove_element.Click
    Dim _idx_row As Integer
    Dim _idx As Integer
    Dim _key_item As Integer

    If Not Me.dg_selected_list.SelectedRows() Is Nothing Then

      For _idx = 0 To Me.dg_selected_list.SelectedRows.Length

        ' Update item in data table
        _key_item = Me.dg_selected_list.Cell(_idx_row, DATA_COLUMN_KEY).Value
        m_dt_items.Select(DATA_COLUMN_KEY & " = " & _key_item)(0).Item(DATA_COLUMN_SELECTED) = False

        ' Delete in selected grid
        _idx_row = Me.dg_selected_list.SelectedRows(0)
        Me.dg_selected_list.Row(_idx_row).IsSelected = False
        Me.dg_selected_list.DeleteRowFast(_idx_row)

        ' Refresh control checked


      Next

    End If

  End Sub ' btn_remove_Click

  Private Sub btn_add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_add_element.Click
    Dim _idx_row As Integer
    Dim _key_item As Integer
    Dim _row As DataRow


    For _idx_row = 0 To Me.uc_check_list.SelectedIndexes.Length - 1
      _key_item = Me.uc_check_list.SelectedIndexes(_idx_row)

      If _key_item <> 0 Then

        ' Get DataTable Row and mark is selected
        _row = m_dt_items.Select(DATA_COLUMN_KEY & " = " & _key_item)(0)
        _row(DATA_COLUMN_SELECTED) = True

        ' Add Item in selected grid
        AddItem_SelectedList(_row(DATA_COLUMN_KEY), _row(DATA_COLUMN_DESCRIPTION))

        ' Refresh control checked

      End If
    Next

  End Sub ' btn_add_Click

  Private Sub btn_up_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_move_up.Click
    Dim _selected_rows As Array
    Dim _idx_row As Integer
    Dim _idx As Integer

    _selected_rows = Me.dg_selected_list.SelectedRows()

    If _selected_rows Is Nothing Then

      Return
    End If

    For _idx = 0 To _selected_rows.Length - 1
      _idx_row = _selected_rows(_idx)

      If (_idx_row > 0) Then
        If Me.dg_selected_list.Row(_idx_row - 1).IsSelected = False Then

          SelectedList_SwitchRows(_idx_row, _idx_row - 1)
          Me.dg_selected_list.Row(_idx_row).IsSelected = False
          Call dg_selected_list.RepaintRow(_idx_row)
          Me.dg_selected_list.Row(_idx_row - 1).IsSelected = True
          Call dg_selected_list.RepaintRow(_idx_row - 1)
        End If
      End If
    Next
  End Sub ' btn_up_Click

  Private Sub btn_down_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_move_down.Click
    Dim _selected_rows As Array
    Dim _idx_row As Integer
    Dim _idx As Integer

    _selected_rows = Me.dg_selected_list.SelectedRows()

    If _selected_rows Is Nothing Then

      Return
    End If

    For _idx = _selected_rows.Length - 1 To 0 Step -1
      _idx_row = _selected_rows(_idx)

      If (_idx_row < Me.dg_selected_list.NumRows - 1) Then
        If Me.dg_selected_list.Row(_idx_row + 1).IsSelected = False Then
          SelectedList_SwitchRows(_idx_row, _idx_row + 1)
          Me.dg_selected_list.Row(_idx_row).IsSelected = False
          Call dg_selected_list.RepaintRow(_idx_row)
          Me.dg_selected_list.Row(_idx_row + 1).IsSelected = True
          Call dg_selected_list.RepaintRow(_idx_row + 1)
        End If
      End If

    Next
  End Sub ' btn_down_Click

#End Region ' Private Events

End Class
