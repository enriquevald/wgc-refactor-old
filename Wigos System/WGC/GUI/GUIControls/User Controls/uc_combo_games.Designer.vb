<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_combo_games
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_game = New System.Windows.Forms.GroupBox
    Me.opt_all_game = New System.Windows.Forms.RadioButton
    Me.opt_several_game = New System.Windows.Forms.RadioButton
    Me.cmb_games = New GUI_Controls.uc_combo
    Me.gb_game.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_game
    '
    Me.gb_game.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.gb_game.Controls.Add(Me.cmb_games)
    Me.gb_game.Controls.Add(Me.opt_all_game)
    Me.gb_game.Controls.Add(Me.opt_several_game)
    Me.gb_game.Location = New System.Drawing.Point(4, 4)
    Me.gb_game.Name = "gb_game"
    Me.gb_game.Size = New System.Drawing.Size(258, 65)
    Me.gb_game.TabIndex = 0
    Me.gb_game.TabStop = False
    Me.gb_game.Text = "xGames"
    '
    'opt_all_game
    '
    Me.opt_all_game.AutoSize = True
    Me.opt_all_game.Location = New System.Drawing.Point(6, 42)
    Me.opt_all_game.Name = "opt_all_game"
    Me.opt_all_game.Size = New System.Drawing.Size(41, 17)
    Me.opt_all_game.TabIndex = 1
    Me.opt_all_game.TabStop = True
    Me.opt_all_game.Text = "xAll"
    Me.opt_all_game.UseVisualStyleBackColor = True
    '
    'opt_several_game
    '
    Me.opt_several_game.AutoSize = True
    Me.opt_several_game.Location = New System.Drawing.Point(6, 18)
    Me.opt_several_game.Name = "opt_several_game"
    Me.opt_several_game.Size = New System.Drawing.Size(63, 17)
    Me.opt_several_game.TabIndex = 0
    Me.opt_several_game.TabStop = True
    Me.opt_several_game.Text = "xGames"
    Me.opt_several_game.UseVisualStyleBackColor = True
    '
    'cmb_games
    '
    Me.cmb_games.AllowUnlistedValues = False
    Me.cmb_games.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.cmb_games.AutoCompleteMode = False
    Me.cmb_games.IsReadOnly = False
    Me.cmb_games.Location = New System.Drawing.Point(75, 18)
    Me.cmb_games.Name = "cmb_games"
    Me.cmb_games.SelectedIndex = -1
    Me.cmb_games.Size = New System.Drawing.Size(177, 24)
    Me.cmb_games.SufixText = "Sufix Text"
    Me.cmb_games.SufixTextVisible = True
    Me.cmb_games.TabIndex = 7
    Me.cmb_games.TextVisible = False
    Me.cmb_games.TextWidth = 0
    '
    'uc_combo_games
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.gb_game)
    Me.Name = "uc_combo_games"
    Me.Size = New System.Drawing.Size(267, 72)
    Me.gb_game.ResumeLayout(False)
    Me.gb_game.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_game As System.Windows.Forms.GroupBox
  Public WithEvents opt_all_game As System.Windows.Forms.RadioButton
  Public WithEvents opt_several_game As System.Windows.Forms.RadioButton
  Public WithEvents cmb_games As GUI_Controls.uc_combo

End Class
