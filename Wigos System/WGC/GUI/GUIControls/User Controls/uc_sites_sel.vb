'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_sites_sel.vb
' DESCRIPTION:   Filter of sites to use in Multisite (or more)
' AUTHOR:        Alberto Marcos
' CREATION DATE: 22-MAR-2013
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 22-MAR-2013 AMF    First Release
' 03-MAY-2013 ANG    Add parameter to set all rows selected in Default values
' 03-MAY-2013 ANG    Add property to hide Multisite field
' 08-MAY-2013 AMF    Add property MultiSelect
' 09-JUL-2013 QMP    Changed standard buttons to uc_button 
' 07-OCT-2013 CCG    Added method to enable/disable the whole control
' 05-MAY-2014 DHA    Fixed Bug WIG-888: Changed message when only can select one site
' 04-DEC-2014 DHA    Added method to set the selected sites
' 07-MAY-2015 FOS    New column Site-ISO-CODE on grid
' 17-JUN-2015 FAV    WIG-2435: Audit: incorrect data when auditing a search.
' 22-JUN-2015 FAV    WIG-2431: Report "Cash summary" : Filters incorrect in Excel
' 29-OCT-2017 LTC    [WIGOS-6134] Multisite Terminal meter adjustment
' 07-NOV-2017 EOR    Bug 30635:WIGOS-6443 Cannot see data in "Meters adjustment" MultisiteGUI window when there is only one site
' 20-NOV-2017 OMC    PBI 30857: Enable Monitor mode for multisite (Add new method to return list of available sites loaded in the grid).
' 21-NOV-2017 JML    Product Backlog Item 29621:AGG Data transfer [WIGOS-4458]
' 07-DEC-2017 FGB    PBI 31058: WIGOS-6802: AGG - Handpay report - New public method: ResizeControlsToCurrentSize
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Windows.Forms

Public Class uc_sites_sel

#Region " Public Event "
  Public Event ValueChangedEvent()
  Public Event RowSelectedEvent()
  Public Event CellDataChangedEvent() 'LTC  29-OCT-2017
#End Region ' Public Event

#Region " Global variables "

  Private m_idsites_print As String = ""
  Private m_idsites_print_full_name As String = ""
  Private m_show_multisite_row As Boolean = True
  Private m_multiselect As Boolean = True
  Private m_cell_disabled_color As System.Drawing.Color = System.Drawing.Color.FromArgb(240, 240, 240)
  Private m_cell_text_disabled_color As System.Drawing.Color = System.Drawing.Color.FromArgb(109, 109, 109)
  Private m_cell_enable_color As System.Drawing.Color = Drawing.Color.White
  Private m_cell_text_enable_color As System.Drawing.Color = Drawing.Color.Black
  Private m_cell_no_currency As System.Drawing.Color = Drawing.Color.Red
  Private m_show_iso_code As Boolean

#End Region

#Region "Constants"

  Private Const SQL_COLUMN_SITE_ID As Integer = 0
  Private Const SQL_COLUMN_SITE_NAME As Integer = 1
  Private Const SQL_COLUMN_SITE_CURRENCY As Integer = 2


  Private Const GRID_2_COLUMN_CHECKED As Integer = 0
  Private Const GRID_2_COLUMN_ID As Integer = 1
  Private Const GRID_2_COLUMN_ID_FORMAT As Integer = 2
  Private Const GRID_2_COLUMN_ISO_CODE As Integer = 3
  Private Const GRID_2_COLUMN_NAME As Integer = 4


  Private Const GRID_2_COLUMNS As Integer = 5
  Private Const GRID_2_HEADER_ROWS As Integer = 0
  Private Const GRID_2_TAB As String = "    "

  Private Const GRID_COLUMN_ID_MULTISITE As Integer = 0

  Private Const FORMAT_ID As String = "00#"

  Private Const MAX_FILTER_REPORT = 5

  Private Const CONTROL_HEIGHT As Integer = 160
  Private Const CONTROL_HEIGHT_COLLAPSED As Integer = 128

#End Region ' Constants

#Region "Public Properties"

  Public Property ShowMultisiteRow() As Boolean
    Get
      Return m_show_multisite_row
    End Get

    Set(ByVal value As Boolean)
      m_show_multisite_row = value
    End Set
  End Property

  Public Property MultiSelect() As Boolean
    Get
      Return m_multiselect
    End Get

    Set(ByVal value As Boolean)
      m_multiselect = value

      If Me.m_multiselect Then
        Me.gb_sites.Height = CONTROL_HEIGHT
        Me.btn_check_all.Enabled = True
        Me.btn_check_all.Visible = True
        Me.btn_uncheck_all.Enabled = True
        Me.btn_uncheck_all.Visible = True
      Else
        Me.gb_sites.Height = CONTROL_HEIGHT_COLLAPSED
        Me.btn_check_all.Enabled = False
        Me.btn_check_all.Visible = False
        Me.btn_uncheck_all.Enabled = False
        Me.btn_uncheck_all.Visible = False
      End If

    End Set
  End Property

  Public Property ShowIsoCode() As Boolean
    Get
      Return m_show_iso_code
    End Get
    Set(ByVal value As Boolean)
      m_show_iso_code = value
    End Set
  End Property

#End Region

#Region " Public Functions "

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ShowMultisiteRow = True

    MultiSelect = True

    ShowIsoCode = False

  End Sub ' New

  ' PURPOSE: Init routine of the control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub Init()

    Call InitControls()
  End Sub ' Init

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - Optional Param to set all items selected or unselected.
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub SetDefaultValues(Optional ByVal SelectAllItems As Boolean = True)
    Dim _idx_rows As Integer

    For _idx_rows = 0 To dg_sites.NumRows - 1
      dg_sites.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = IIf(SelectAllItems, uc_grid.GRID_CHK_CHECKED, uc_grid.GRID_CHK_UNCHECKED)
    Next

  End Sub ' SetDefaultValues

  ''' <summary>
  ''' Check first value
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub SetFirstValue()

    If dg_sites.NumRows > 0 Then
      Call SetDefaultValues(False)

      dg_sites.Cell(0, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    End If

  End Sub ' SetFirstValue

  ' PURPOSE: select first site of the list
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Public Function GetFirstSiteId() As String
    Return dg_sites.Cell(0, GRID_2_COLUMN_ID).Value

  End Function

  ' PURPOSE: Check Filter, select at least one site
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean
  Public Function FilterCheckSites() As Boolean
    Dim _idx_rows As Integer
    Dim _all_unchecked As Boolean

    _all_unchecked = True

    For _idx_rows = 0 To dg_sites.NumRows - 1
      If dg_sites.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _all_unchecked = False
        Exit For
      End If
    Next

    If _all_unchecked Then
      ' DHA 05-MAY-2014: changed message when only can select one site
      If m_multiselect Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1806), ENUM_MB_TYPE.MB_TYPE_ERROR)
      Else
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4921), ENUM_MB_TYPE.MB_TYPE_ERROR)
      End If

      'FAV 17-JUN-2015
      m_idsites_print = ""
      m_idsites_print_full_name = ""

      Return False
    Else
      Return True
    End If

  End Function ' FilterCheckSites

  ' PURPOSE: Return a String containing the list of the checked sites.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Public Function GetSitesIdListSelected() As String
    Dim _idx_rows As Integer
    Dim _all_checked As Boolean
    Dim _first_row_checked As Boolean
    Dim _list_str_sites As String
    Dim _counter As Integer

    _all_checked = True
    _first_row_checked = True
    _list_str_sites = ""
    m_idsites_print = ""
    m_idsites_print_full_name = ""

    For _idx_rows = 0 To dg_sites.NumRows - 1

      If dg_sites.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
        If _all_checked Then
          _all_checked = False
        End If
      End If

      If dg_sites.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _counter += 1
        If (_first_row_checked) Then
          _first_row_checked = False
          _list_str_sites += dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ID).Value
          m_idsites_print += IIf((dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ID).Value = GRID_COLUMN_ID_MULTISITE), Trim(dg_sites.Cell(_idx_rows, GRID_2_COLUMN_NAME).Value), dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ID_FORMAT).Value)
          m_idsites_print_full_name += dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ID_FORMAT).Value + "-" + Trim(dg_sites.Cell(_idx_rows, GRID_2_COLUMN_NAME).Value)

        Else
          _list_str_sites += ", " + dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ID).Value
          m_idsites_print += ", " + IIf((dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ID).Value = GRID_COLUMN_ID_MULTISITE), Trim(dg_sites.Cell(_idx_rows, GRID_2_COLUMN_NAME).Value), dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ID_FORMAT).Value)
          m_idsites_print_full_name += ", " + dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ID_FORMAT).Value & "-" & Trim(dg_sites.Cell(_idx_rows, GRID_2_COLUMN_NAME).Value)
        End If
      End If
    Next

    If _all_checked Then
      m_idsites_print = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1807)
      Return ""
    Else
      If (_counter > MAX_FILTER_REPORT) Then
        m_idsites_print = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1808)
      End If
      Return _list_str_sites
    End If

  End Function ' GetSitesIdListSelected

  ' PURPOSE: Return a String containing the list of the checked sites, even if all are checked.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Public Function GetSitesIdListSelectedAll() As String
    Dim _idx_rows As Integer
    Dim _all_checked As Boolean
    Dim _first_row_checked As Boolean
    Dim _list_str_sites As String
    Dim _counter As Integer

    _all_checked = True
    _first_row_checked = True
    _list_str_sites = ""
    m_idsites_print = ""
    m_idsites_print_full_name = ""

    For _idx_rows = 0 To dg_sites.NumRows - 1

      If dg_sites.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED Then
        If _all_checked Then
          _all_checked = False
        End If
      End If

      If dg_sites.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _counter += 1
        If (_first_row_checked) Then
          _first_row_checked = False
          _list_str_sites += dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ID).Value
          m_idsites_print += IIf((dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ID).Value = GRID_COLUMN_ID_MULTISITE), Trim(dg_sites.Cell(_idx_rows, GRID_2_COLUMN_NAME).Value), dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ID_FORMAT).Value)
          m_idsites_print_full_name += dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ID_FORMAT).Value + "-" + Trim(dg_sites.Cell(_idx_rows, GRID_2_COLUMN_NAME).Value)
        Else
          _list_str_sites += ", " + dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ID).Value
          m_idsites_print += ", " + IIf((dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ID).Value = GRID_COLUMN_ID_MULTISITE), Trim(dg_sites.Cell(_idx_rows, GRID_2_COLUMN_NAME).Value), dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ID_FORMAT).Value)
          m_idsites_print_full_name += ", " + dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ID_FORMAT).Value & "-" & Trim(dg_sites.Cell(_idx_rows, GRID_2_COLUMN_NAME).Value)
        End If
      End If
    Next

    If _all_checked Then
      m_idsites_print = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1807)
      Return _list_str_sites
    Else
      If (_counter > MAX_FILTER_REPORT) Then
        m_idsites_print = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1808)
      End If
      Return _list_str_sites
    End If

  End Function ' GetSitesIdListSelectedAll

  ' PURPOSE: Return a String containing the checked site selected, even if all are checked.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Public Function GetSiteIdSelected() As String
    Dim _idx_rows As Integer
    Dim _site_id As String

    _site_id = ""

    If Not MultiSelect Then
      For _idx_rows = 0 To dg_sites.NumRows - 1
        If dg_sites.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
          _site_id = IIf((dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ID).Value = GRID_COLUMN_ID_MULTISITE), Trim(dg_sites.Cell(_idx_rows, GRID_2_COLUMN_NAME).Value), dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ID_FORMAT).Value)
          Exit For
        End If
      Next
    End If

    Return _site_id
  End Function ' GetSitesIdListSelectedAll

  ' PURPOSE: Return a String containing the list of the checked sites to print.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Public Function GetSitesIdListSelectedToPrint() As String

    GetSitesIdListSelectedAll()

    Return m_idsites_print

  End Function ' GetSitesIdListSelectedToPrint


  ' PURPOSE: Return a String containing the list of the checked sites to print (Id and name).
  '
  '  PARAMS:
  '     - INPUT:
  '           - Id_Name As Boolean: If is True returns 'ID-Name' list, if is False returns 'ID' list.
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Public Function GetSitesIdListSelectedToPrint(Id_Name As Boolean) As String

    If Id_Name Then
      Return m_idsites_print_full_name
    Else
      Return GetSitesIdListSelectedToPrint()
    End If

  End Function ' GetSitesIdNameListSelectedToPrint

  ' PURPOSE: Set Sites List in control
  '
  '  PARAMS:
  '     - INPUT:
  '           - String List
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Public Sub SetSitesIdListSelected(ByVal SiteList As String)
    Dim _site_list As List(Of Int64)
    Dim _sites_list_string As String()

    _site_list = New List(Of Int64)

    If Not String.IsNullOrEmpty(SiteList) Then
      _sites_list_string = SiteList.Split(",")
      If _sites_list_string.Length > 0 Then
        For _idx_site As Integer = 0 To _sites_list_string.Length - 1
          _site_list.Add(Int64.Parse(_sites_list_string(_idx_site).Trim))
        Next
        SetSitesIdListSelected(_site_list)
      End If
    End If

  End Sub

  ' PURPOSE: Set Sites List in control
  '
  '  PARAMS:
  '     - INPUT:
  '           - String List
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Public Sub SetSitesIdListSelected(ByVal SiteList As List(Of Int64))
    Dim _idx_rows As Int32

    For Each _item As Int64 In SiteList
      For _idx_rows = 0 To dg_sites.NumRows - 1
        If dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ID).Value = _item Then
          dg_sites.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
          Exit For
        End If
      Next
    Next

  End Sub

  ' PURPOSE: To set the whole control as enabled/disabled
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub EnableControl(ByVal Enabled As Boolean)
    Me.Enabled = Enabled

    If Not Enabled Then
      Call Me.SetGridColor(m_cell_disabled_color, m_cell_text_disabled_color)
    Else
      Call Me.SetGridColor(m_cell_enable_color, m_cell_text_enable_color)
    End If
  End Sub ' EnableControl

#End Region

#Region " Private Functions "

  ' PURPOSE: Init routine of the control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InitControls()

    Me.gb_sites.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1803)

    Me.btn_check_all.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1804)
    Me.btn_check_all.Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.btn_check_all.Width += 20

    Me.btn_uncheck_all.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1805)
    Me.btn_uncheck_all.Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.btn_uncheck_all.Width += 20

    Me.gb_sites.AutoSize = False
    Me.AutoSize = False

    Me.dg_sites.IsToolTipped = True

    Call GUI_StyleSheetListSites()
    Call GetSitesList()

  End Sub ' InitControls

  ' PURPOSE: Get Sites list
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GetSitesList()
    Dim _dt_sites As DataTable = Nothing

    _dt_sites = GUI_GetTableUsingSQL("SELECT ST_SITE_ID, ST_NAME FROM SITES ORDER BY ST_SITE_ID", 5000)

    Call GetSitesListPrint(_dt_sites)
  End Sub ' GetSitesList

  ' PURPOSE: Print sites list
  '
  '  PARAMS:
  '     - INPUT:
  '           - dataTable with a list with all sites
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub GetSitesListPrint(ByVal Sites As DataTable)
    Dim _db_row As frm_base_sel.CLASS_DB_ROW
    Dim _rsites As DataRow

    If Sites.Rows.Count = 0 Then
      Return
    End If

    Me.dg_sites.Clear()
    Me.dg_sites.Redraw = False

    'Insert the Multisite
    If ShowMultisiteRow Then
      Call AddOneFilterRowDevice(GRID_COLUMN_ID_MULTISITE, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1791), String.Empty)
    End If

    'Insert the Sites
    For Each _rsites In Sites.Rows
      _db_row = New frm_base_sel.CLASS_DB_ROW(_rsites)
      If m_show_iso_code AndAlso _rsites.ItemArray.Length > 2 AndAlso Not IsDBNull(_db_row.Value(SQL_COLUMN_SITE_CURRENCY)) Then
        Call AddOneFilterRowDevice(_db_row.Value(SQL_COLUMN_SITE_ID), _db_row.Value(SQL_COLUMN_SITE_NAME), _db_row.Value(SQL_COLUMN_SITE_CURRENCY))
      Else
        Call AddOneFilterRowDevice(_db_row.Value(SQL_COLUMN_SITE_ID), _db_row.Value(SQL_COLUMN_SITE_NAME), String.Empty)
      End If
    Next

    If Me.dg_sites.NumRows > 0 Then
      Me.dg_sites.CurrentRow = 0
    End If

    Me.dg_sites.Redraw = True
  End Sub ' GetSitesListPrint


  Public Function GetSitesDataTable() As DataTable
    Dim _tsites As DataTable = Nothing

    _tsites = GUI_GetTableUsingSQL("SELECT ST_SITE_ID, ST_NAME FROM SITES ORDER BY ST_SITE_ID", 5000)

    Return _tsites
  End Function 'GetSitesDataTable()

  ''' <summary>
  ''' Resize control to current size
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub ResizeControlsToCurrentSize()
    Dim _size As System.Drawing.Size
    Dim _offset_Y As Int32
    Dim _buttons_Y As Int32

    gb_sites.Size = New Drawing.Size(Me.Width - (gb_sites.Location.X * 2), Me.Height - (gb_sites.Location.Y * 2))

    'Vertical distance between grid and buttons
    _offset_Y = btn_check_all.Location.Y - (dg_sites.Location.Y + dg_sites.Size.Height)

    'New Y location for buttons
    _buttons_Y = gb_sites.Size.Height - (btn_check_all.Height + (_offset_Y * 2))

    'Position of button
    btn_check_all.Location = New Drawing.Point(btn_check_all.Location.X, _buttons_Y)
    btn_uncheck_all.Location = New Drawing.Point(btn_uncheck_all.Location.X, _buttons_Y)

    'Resize grid
    _size = New Drawing.Size(gb_sites.Width - (dg_sites.Location.X * 2), (_buttons_Y - _offset_Y - dg_sites.Location.Y))

    Me.dg_sites.Size = _size

    'Resize columns of grid
    Call ResizeLastColumnOfGrid()
  End Sub

  Public Sub ResizeLastColumnOfGrid()
    Call Me.dg_sites.ColumnFit(GRID_2_COLUMN_NAME)
  End Sub

  ' PURPOSE: Add a Row of a site to the Event grid according to 
  '          the given code
  '
  '  PARAMS:
  '     - INPUT:
  '           - Code 
  '   
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddOneFilterRowDevice(ByVal SiteId As Integer, ByVal SiteName As String, ByVal SiteCurrency As String)
    Dim _prev_redraw As Boolean

    _prev_redraw = Me.dg_sites.Redraw

    dg_sites.AddRow()

    dg_sites.Cell(dg_sites.NumRows - 1, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    dg_sites.Cell(dg_sites.NumRows - 1, GRID_2_COLUMN_ID).Value = SiteId
    dg_sites.Cell(dg_sites.NumRows - 1, GRID_2_COLUMN_ID_FORMAT).Value = IIf((SiteId = 0), "", Format(SiteId, FORMAT_ID))
    dg_sites.Cell(dg_sites.NumRows - 1, GRID_2_COLUMN_NAME).Value = GRID_2_TAB & SiteName

    If String.IsNullOrEmpty(SiteCurrency) AndAlso Me.Enabled Then
      dg_sites.Cell(dg_sites.NumRows - 1, GRID_2_COLUMN_ISO_CODE).BackColor = m_cell_no_currency
    End If
    dg_sites.Cell(dg_sites.NumRows - 1, GRID_2_COLUMN_ISO_CODE).Value = SiteCurrency

    Me.dg_sites.Redraw = _prev_redraw

  End Sub ' AddOneFilterRowDevice

  ' PURPOSE: Define all Grid Event Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheetListSites()

    With Me.dg_sites
      Call .Init(GRID_2_COLUMNS, GRID_2_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' GRID_COL_CHECKBOX
      .Column(GRID_2_COLUMN_CHECKED).Header.Text = ""
      .Column(GRID_2_COLUMN_CHECKED).WidthFixed = 400
      .Column(GRID_2_COLUMN_CHECKED).Fixed = True
      .Column(GRID_2_COLUMN_CHECKED).Editable = True
      .Column(GRID_2_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_ID Hidden
      .Column(GRID_2_COLUMN_ID).Header.Text = ""
      .Column(GRID_2_COLUMN_ID).WidthFixed = 0

      ' GRID_COL_ID_FORMAT
      .Column(GRID_2_COLUMN_ID_FORMAT).Header.Text = ""
      .Column(GRID_2_COLUMN_ID_FORMAT).WidthFixed = 400
      .Column(GRID_2_COLUMN_ID_FORMAT).Fixed = True
      .Column(GRID_2_COLUMN_ID_FORMAT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' GRID_COL_ISO_CODE
      .Column(GRID_2_COLUMN_ISO_CODE).Header.Text = ""
      If m_show_iso_code Then
        .Column(GRID_2_COLUMN_ISO_CODE).WidthFixed = 400
      Else
        .Column(GRID_2_COLUMN_ISO_CODE).WidthFixed = 0
      End If

      .Column(GRID_2_COLUMN_ISO_CODE).Fixed = True
      .Column(GRID_2_COLUMN_ISO_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' GRID_COL_NAME
      .Column(GRID_2_COLUMN_NAME).Header.Text = ""
      .Column(GRID_2_COLUMN_NAME).WidthFixed = 2900
      .Column(GRID_2_COLUMN_NAME).Fixed = True
      .Column(GRID_2_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    End With

  End Sub 'GUI_StyleSheetType

  ' PURPOSE: To set the color of the grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - BackColor: To set the Back color of the cell
  '           - ForeColor: To set the Fore color of the cell
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetGridColor(ByVal BackColor As Drawing.Color, ByVal ForeColor As Drawing.Color)
    Dim _idx_row As Integer

    For _idx_row = 0 To Me.dg_sites.NumRows - 1
      Me.dg_sites.Row(_idx_row).BackColor = BackColor
      Me.dg_sites.Row(_idx_row).ForeColor = ForeColor
    Next
  End Sub ' SetGridColor

#End Region 'Private Functions 

#Region "Events"

  ' PURPOSE: Execute proper actions when user presses the 'Check all' button
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Sub btn_check_all_ClickEvent() Handles btn_check_all.ClickEvent
    Dim _idx_rows As Integer
    For _idx_rows = 0 To dg_sites.NumRows - 1
      dg_sites.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    Next
  End Sub ' btn_check_all_ClickEvent

  ' PURPOSE: Execute proper actions when user presses the 'Uncheck All' button
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Sub btn_uncheck_all_ClickEvent() Handles btn_uncheck_all.ClickEvent
    Dim _idx_rows As Integer
    For _idx_rows = 0 To dg_sites.NumRows - 1
      dg_sites.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next
  End Sub ' btn_uncheck_all_ClickEvent

  ' PURPOSE: Execute proper actions when Cell Data Change
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  ' 
  Private Sub dg_sites_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_sites.CellDataChangedEvent
    Dim _idx_rows As Integer

    If Not MultiSelect Then
      For _idx_rows = 0 To dg_sites.NumRows - 1
        If dg_sites.SelectedRows(0) <> _idx_rows Then
          dg_sites.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
        End If
      Next
    End If

    RaiseEvent CellDataChangedEvent() 'LTC  29-OCT-2017
  End Sub ' dg_sites_CellDataChangedEvent

  ' PURPOSE: Execute proper actions when Cell Data is selected
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  ' 
  Private Sub dg_sites_RowSelectedEvent(ByVal SelectedRow As System.Int32) Handles dg_sites.RowSelectedEvent
    RaiseEvent RowSelectedEvent()
  End Sub ' dg_sites_RowSelectedEvent

#End Region ' Events


End Class
