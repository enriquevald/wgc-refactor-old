'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_taxes_config.vb  
'
' DESCRIPTION:   control schedule selection
'
' AUTHOR:        Miquel Beltran
'
' CREATION DATE: 27-SEP-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 27-SEP-2010  MBF    Initial version
' 23-DEC-2011  MPO    Add fields text_prize_coupon, txt_info_chashin, text_promotion
' 04-JAN-2012  JMM    Optionally, Hide Totals in A and B tickets. Remove field for the ticket Cash in.
' 20-MAR-2012  JAB    Add "Promo and CouponTogether" field
' 27-MAR-2012  JAB    Moved control "hide_info_cashin" on top of groupbox.
' 03-JUL-2012  JCM    Removed Cupon Prize CheckBox, now Cupon Prize text enabled/disabled dependends of Split B company value
'                     Devolution value removed
' 20-SEP-2012  JML    Get back Cupon Prize CheckBox
' 17-MAY-2013  AMF    Fixed Defect #787: Incorrect Error Message
'-------------------------------------------------------------------
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports System.Drawing

Public Class uc_taxes_config

#Region "Members"

  Dim m_business As String
  Dim m_tab_index As Integer
  Dim m_headers As Hashtable
  Dim m_split_b As Boolean

#End Region ' Members

#Region "Public"

  ' PURPOSE: Constructor method.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControls()
    ' Default value
    SetBusiness("A")

    Call CreateTabs()

  End Sub ' New

  ' PURPOSE: Provides visible controls and sets the text
  '         
  ' PARAMS:
  '    - INPUT:
  '           - Business: string
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Sub SetBusiness(ByVal Business As String)

    m_business = Business
    gb_business.Text = "" 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(361) & " " & m_business ' "Empresa " & m_business
    chk_hide_promo.Visible = (m_business = "A")
    chk_prize_coupon.Visible = (m_business = "A")
    uc_text_prize_coupon.Visible = (m_business = "A")
    uc_text_promotion.Visible = (m_business = "A")
    uc_text_PromoAndCouponTogether.Visible = (m_business = "A")
    chk_PromoAndCouponTogether.Visible = (m_business = "A")

  End Sub ' setBusiness

  Public Sub SetVoucherText(ByVal Id As String, ByVal Value As String)

    Dim _text_box As System.Windows.Forms.TextBox

    If Me.m_headers.Contains(Id) Then
      _text_box = Me.m_headers(Id)
      _text_box.Text = Value
    End If

  End Sub ' SetVoucherText

  Public Sub SetFocus(ByVal Nls As Integer)
    Select Case Nls
      Case 362
        Me.uc_entry_business_name.Focus()
      Case 363
        Me.uc_concept_name.Focus()
      Case 364
        Me.uc_cashin_pct.Focus()
      Case 366
        Me.uc_entry_tax_name.Focus()
      Case 368
        Me.uc_entry_tax_pct.Focus()

    End Select
  End Sub ' SetFocus

  Public Sub ShowBusinessAFields(ByVal Show As Boolean)
    chk_hide_promo.Visible = Show
    chk_prize_coupon.Visible = Show
  End Sub ' ShowBusinessAFields

  Public Function GetVoucherText(ByVal Id As String)

    Dim _text_box As System.Windows.Forms.TextBox

    If Me.m_headers.Contains(Id) Then
      _text_box = Me.m_headers(Id)
      Return _text_box.Text
    End If

    Return "Not Found"
  End Function ' GetVoucherText

  Public Function DataOk() As Integer
    Dim nls As Integer

    nls = 0
    If uc_entry_business_name.Value = "" Then
      nls = 362
    End If

    If uc_concept_name.Value = "" Then
      nls = 363
    End If

    If uc_entry_tax_name.Value = "" Then
      nls = 366
    End If

    If uc_cashin_pct.Value = "" Then
      nls = 364
    End If

    If uc_entry_tax_pct.Value = "" Then
      nls = 368
    End If

    Return nls
  End Function ' DataOk

  Public Overloads Property BusinessName() As String
    Get
      Return uc_entry_business_name.Value
    End Get

    Set(ByVal Value As String)
      uc_entry_business_name.Value = Value
    End Set

  End Property ' BusinessName

  Public Overloads Property ConceptName() As String
    Get
      Return uc_concept_name.Value
    End Get

    Set(ByVal Value As String)
      uc_concept_name.Value = Value
    End Set

  End Property ' ConceptName

  Public Overloads Property PayTitle() As String
    Get
      Return uc_pay_title.Value
    End Get

    Set(ByVal Value As String)
      uc_pay_title.Value = Value
    End Set

  End Property ' PayTitle

  Public Overloads Property DevTitle() As String
    Get
      Return uc_dev_title.Value
    End Get

    Set(ByVal Value As String)
      uc_dev_title.Value = Value
    End Set

  End Property ' DevTitle

  Public Overloads Property CashInPct() As String
    Get
      Return uc_cashin_pct.Value
    End Get

    Set(ByVal Value As String)
      uc_cashin_pct.Value = Value
    End Set

  End Property ' CashInPct

  Public Overloads Property TaxName() As String
    Get
      Return uc_entry_tax_name.Value
    End Get

    Set(ByVal Value As String)
      uc_entry_tax_name.Value = Value
    End Set

  End Property ' TaxName

  Public Overloads Property TaxPct() As String
    Get
      Return uc_entry_tax_pct.Value
    End Get

    Set(ByVal Value As String)
      uc_entry_tax_pct.Value = Value
    End Set

  End Property ' TaxPct

  Public Overloads Property PrizeCoupon() As Boolean
    Get
      Return chk_prize_coupon.Checked
    End Get

    Set(ByVal Value As Boolean)
      chk_prize_coupon.Checked = Value
    End Set

  End Property ' PrizeCoupon

  Public Overloads Property HidePromo() As Boolean
    Get
      Return chk_hide_promo.Checked
    End Get

    Set(ByVal Value As Boolean)
      chk_hide_promo.Checked = Value
    End Set

  End Property ' HidePromo

  Public Overloads Property TextPrizeCoupon() As String
    Get
      Return uc_text_prize_coupon.Value
    End Get
    Set(ByVal value As String)
      uc_text_prize_coupon.Value = value
    End Set
  End Property ' TextPrizeCoupon

  Public Overloads Property SplitB() As Boolean
    Get
      Return m_split_b
    End Get

    Set(ByVal Value As Boolean)
      m_split_b = Value
      uc_text_prize_coupon.Enabled = m_split_b And chk_prize_coupon.Checked And Not chk_PromoAndCouponTogether.Checked
      chk_prize_coupon.Enabled = m_split_b
    End Set
  End Property ' TaxSplit

  Public Overloads Property TextPromotion() As String
    Get
      Return uc_text_promotion.Value
    End Get
    Set(ByVal value As String)
      uc_text_promotion.Value = value
    End Set
  End Property ' TextPromotion

  Public Overloads Property PromoAndCouponTogether() As Boolean
    Get
      Return chk_PromoAndCouponTogether.Checked
    End Get
    Set(ByVal value As Boolean)
      chk_PromoAndCouponTogether.Checked = value
    End Set
  End Property ' PromoAndCouponTogether

  Public Overloads Property PromoAndCouponTogetherText() As String
    Get
      Return uc_text_PromoAndCouponTogether.Value
    End Get
    Set(ByVal value As String)
      uc_text_PromoAndCouponTogether.Value = value
    End Set
  End Property ' PromoAndCouponTogetherText

  Public Overloads Property HideTotalCashIn() As Boolean
    Get
      Return chk_hide_info_cashin.Checked
    End Get

    Set(ByVal Value As Boolean)
      chk_hide_info_cashin.Checked = Value
    End Set

  End Property ' HideTotalCashIn

#End Region ' Public

#Region "Private"

  ' PURPOSE: Initialize form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Private Sub InitControls()

    uc_entry_business_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(362) ' "Nombre"
    uc_concept_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(363) ' "Concepto"
    uc_cashin_pct.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(233) ' "Recarga"
    gb_voucher_texts.Text = GLB_NLS_GUI_CONFIGURATION.GetString(21) ' "Textos de los Recibos"
    uc_entry_tax_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(366) ' "Impuesto"
    uc_entry_tax_pct.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(368) ' "Porcentaje"
    uc_pay_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1243) ' "Titulo recarga"
    uc_dev_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1244) ' "Titulo retiro"

    uc_entry_business_name.SetFilter(FORMAT_TEXT, 200)
    uc_concept_name.SetFilter(FORMAT_TEXT, 200)
    uc_cashin_pct.SetFilter(FORMAT_NUMBER, 5, 2)
    uc_entry_tax_name.SetFilter(FORMAT_TEXT, 200)
    uc_entry_tax_pct.SetFilter(FORMAT_NUMBER, 5, 2)
    uc_pay_title.SetFilter(FORMAT_TEXT, 100)
    uc_dev_title.SetFilter(FORMAT_TEXT, 100)

    chk_hide_promo.Text = GLB_NLS_GUI_CONFIGURATION.GetString(336)
    chk_prize_coupon.Text = GLB_NLS_GUI_CONFIGURATION.GetString(337)

    '(561)   "Texto cup�n"
    '(562)   "Ocultar total"
    '(564)   "Texto promoci�n"
    '(20)    "�nico Rubro"
    uc_text_prize_coupon.SetFilter(FORMAT_TEXT, 200)
    uc_text_prize_coupon.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(561)
    'uc_text_prize_coupon.Enabled = chk_prize_coupon.Checked
    uc_text_prize_coupon.Enabled = chk_prize_coupon.Checked And SplitB And Not chk_PromoAndCouponTogether.Checked
    chk_hide_info_cashin.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(562)
    chk_hide_info_cashin.Visible = True
    uc_text_promotion.SetFilter(FORMAT_TEXT, 200)
    uc_text_promotion.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(564)
    uc_text_promotion.Enabled = Not chk_hide_promo.Checked
    uc_text_PromoAndCouponTogether.SetFilter(FORMAT_TEXT, 200)
    chk_PromoAndCouponTogether.Text = GLB_NLS_GUI_CONFIGURATION.GetString(20)

  End Sub ' InitControls

  ' PURPOSE: Calls the function that will create the specified tabs
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Private Sub CreateTabs()

    m_tab_index = 0
    m_headers = New Hashtable

    CreateTab(GLB_NLS_GUI_PLAYER_TRACKING.GetString(372), "Header")
    CreateTab(GLB_NLS_GUI_PLAYER_TRACKING.GetString(373), "Footer")
    CreateTab(GLB_NLS_GUI_PLAYER_TRACKING.GetString(374), "Footer.CashIn")
    CreateTab(GLB_NLS_GUI_PLAYER_TRACKING.GetString(375), "Footer.Dev")
    CreateTab(GLB_NLS_GUI_PLAYER_TRACKING.GetString(376), "Footer.Cancel")

  End Sub ' CreateTabs

  ' PURPOSE: Creates a tab
  '         
  ' PARAMS:
  '    - INPUT:
  '           - Name: string
  '           - Id: string
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Private Sub CreateTab(ByVal Name As String, ByVal Id As String)
    Dim _tab As System.Windows.Forms.TabPage
    Dim _text_box As System.Windows.Forms.TextBox

    _text_box = New System.Windows.Forms.TextBox

    _text_box.Dock = System.Windows.Forms.DockStyle.Fill
    _text_box.Multiline = True
    _text_box.Name = "TextBox1"
    _text_box.TabIndex = 0
    _text_box.MaxLength = 1024

    m_headers.Add(Id, _text_box)

    _tab = New System.Windows.Forms.TabPage

    _tab.Controls.Add(_text_box)
    _tab.Name = Id
    _tab.Padding = New System.Windows.Forms.Padding(3)
    _tab.TabIndex = m_tab_index + 1
    _tab.Text = Name
    _tab.UseVisualStyleBackColor = True

    m_tab_index = m_tab_index + 1

    Me.tab_voucher.Controls.Add(_tab)

  End Sub ' CreateTab

#End Region ' Private

#Region " Events "

  ' PURPOSE: Functions to be performed when the "Checked" the object "chk_prize_coupon" changes (Handler events)
  '         
  ' PARAMS:
  '    - INPUT:
  '           - Sender: System.Object
  '           - e: System.EventArgs
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Private Sub chk_prize_coupon_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_prize_coupon.CheckedChanged
    If Not chk_PromoAndCouponTogether.Checked Then
      uc_text_prize_coupon.Enabled = chk_prize_coupon.Checked And Me.SplitB
    End If
  End Sub ' chk_prize_coupon_CheckedChanged

  ' PURPOSE: Functions to be performed when the "Checked" the object "chk_hide_promo" changes (Handler events)
  '         
  ' PARAMS:
  '    - INPUT:
  '           - Sender: System.Object
  '           - e: System.EventArgs
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Private Sub chk_hide_promo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_hide_promo.CheckedChanged

    uc_text_promotion.Enabled = Not chk_hide_promo.Checked

  End Sub ' chk_hide_promo_CheckedChanged

  ' PURPOSE: Functions to be performed when the "Checked" the object "chk_PromoAndCouponTogether" changes (Handler events)
  '         
  ' PARAMS:
  '    - INPUT:
  '           - Sender: System.Object
  '           - e: System.EventArgs
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Private Sub chk_PromoAndCouponTogether_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_PromoAndCouponTogether.CheckedChanged
    If chk_PromoAndCouponTogether.Checked Then
      uc_text_PromoAndCouponTogether.Enabled = True
      uc_text_prize_coupon.Enabled = False
      chk_hide_promo.Enabled = False
      uc_text_promotion.Enabled = False
    Else
      'If chk_prize_coupon.Checked Then
      If chk_prize_coupon.Checked And Me.SplitB And Not chk_PromoAndCouponTogether.Checked Then
        uc_text_prize_coupon.Enabled = True
      Else
        uc_text_prize_coupon.Enabled = False
      End If
      uc_text_PromoAndCouponTogether.Enabled = False
      chk_hide_promo.Enabled = True
      If Not chk_hide_promo.Checked Then
        uc_text_promotion.Enabled = True
      End If
    End If
  End Sub ' chk_PromoAndCouponTogether_CheckedChanged

#End Region ' Events

End Class ' uc_taxes_config
