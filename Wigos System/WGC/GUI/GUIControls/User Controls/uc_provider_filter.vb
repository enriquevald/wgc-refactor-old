'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_provider_filter
' DESCRIPTION:   Filter of providers to use in draws or promotions (or more)
' AUTHOR:        Raul Cervera
' CREATION DATE: 18-JAN-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 18-JAN-2012  RCI    Initial version
'--------------------------------------------------------------------

Imports System.Data.SqlClient
Imports WSI.Common

Public Class uc_provider_filter

  Private Const CONTROL_WIDTH As Integer = 283
  Private Const CONTROL_HEIGHT As Integer = 362
  Private Const CONTROL_HEIGHT_COLLAPSED As Integer = 48

  Private STR_TO_COLLAPSE As String = System.Text.Encoding.Unicode.GetString(New Byte() {&HC4, &H2})
  Private STR_TO_EXPAND As String = System.Text.Encoding.Unicode.GetString(New Byte() {&HC5, &H2})

  Private m_allow_collapse As Boolean = False
  Private m_collapsed As Boolean = False

#Region " Properties "

  Public ReadOnly Property ComboValue(Optional ByVal Index As Integer = -1) As Integer
    Get
      Return Me.cmb_provider_list_type.Value(Index)
    End Get
  End Property

  Public Property AllowCollapse() As Boolean
    Get
      Return Me.m_allow_collapse
    End Get

    Set(ByVal value As Boolean)
      Me.m_allow_collapse = value
      Me.btn_collapse.Enabled = Me.m_allow_collapse
      If Not Me.m_allow_collapse Then
        Me.Collapsed = False
      End If
    End Set
  End Property

  Public Property Collapsed() As Boolean
    Get
      Return Me.m_collapsed
    End Get

    Set(ByVal value As Boolean)
      If value = True And Not Me.AllowCollapse Then
        Return
      End If
      Me.m_collapsed = value
      If Me.m_collapsed Then
        Me.Height = CONTROL_HEIGHT_COLLAPSED
        Me.btn_collapse.Text = STR_TO_EXPAND
      Else
        Me.Height = CONTROL_HEIGHT
        Me.btn_collapse.Text = STR_TO_COLLAPSE
      End If
    End Set
  End Property

#End Region ' Properties

#Region " Public Functions "

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    Call InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControls()

  End Sub ' New

  ' PURPOSE: Check if at least one provider has to be checked.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean.
  '
  Public Function CheckAtLeastOneSelected() As Boolean

    Return Me.cmb_provider_list_type.Value = ProviderList.ProviderListType.All Or _
           Me.chklb_provider_list.CheckedItems.Count > 0

  End Function ' CheckAtLeastOneSelected

  ' PURPOSE: Get the checked provider list that the control shows.
  '
  '  PARAMS:
  '     - INPUT:
  '         - Providers As ProviderList
  '
  '     - OUTPUT:
  '         - Providers As ProviderList
  '
  ' RETURNS:
  '
  Public Sub GetProvidersFromControl(ByRef Providers As ProviderList)
    Dim _idx As Integer

    Providers.ListType = Me.cmb_provider_list_type.Value

    If Providers.ListType <> ProviderList.ProviderListType.All Then
      For _idx = 0 To Me.chklb_provider_list.CheckedItems.Count - 1
        Call Providers.AddProvider(Me.chklb_provider_list.CheckedItems(_idx))
      Next _idx
    End If

  End Sub ' GetProvidersFromControl

  ' PURPOSE: Check the providers in the list in the control.
  '
  '  PARAMS:
  '     - INPUT:
  '         - Providers As ProviderList
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean.
  '
  Public Function SetProviderList(ByVal Providers As ProviderList) As Boolean
    Dim _current_provider As String
    Dim _str_sql As String

    Try
      Me.cmb_provider_list_type.Value = Providers.ListType
      Call EnableCheckProviderListByListType()

      _str_sql = "SELECT   DISTINCT TE_PROVIDER_ID " & _
                 "  FROM   TERMINALS " & _
                 " WHERE   TE_TYPE             = 1 " & _
                 "   AND   TE_TERMINAL_TYPE IN ( " & WSI.Common.Misc.GamingTerminalTypeListToString() & " ) " & _
                 "ORDER BY 1 "

      ' Get all providers from DB
      Using _db_trx As DB_TRX = New DB_TRX()
        Using _sql_cmd As SqlCommand = New SqlCommand(_str_sql)
          Using _sql_reader As SqlDataReader = _db_trx.ExecuteReader(_sql_cmd)

            Call Me.chklb_provider_list.Items.Clear()

            If Providers.ListType = ProviderList.ProviderListType.All Then
              While _sql_reader.Read()
                Call Me.chklb_provider_list.Items.Add(_sql_reader.GetString(0), True)
              End While

            ElseIf Providers.ListType = ProviderList.ProviderListType.Listed Then
              While _sql_reader.Read()
                _current_provider = _sql_reader.GetString(0)
                Call Me.chklb_provider_list.Items.Add(_current_provider, Providers.Contains(_current_provider))
              End While

            ElseIf Providers.ListType = ProviderList.ProviderListType.NotListed Then
              While _sql_reader.Read()
                _current_provider = _sql_reader.GetString(0)
                Call Me.chklb_provider_list.Items.Add(_current_provider, Not Providers.Contains(_current_provider))
              End While

            Else
              '
            End If

            Return True
          End Using
        End Using
      End Using

    Catch ex As Exception
      ' Do nothing
      Call Debug.WriteLine(ex.Message)

      Return False
    End Try
  End Function ' SetProviderList

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Own controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean.
  '
  Private Sub InitControls()

    Me.gb_provider.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(566)
    Call ProviderComboFill()
    Call EnableCheckProviderListByListType()

  End Sub ' InitControls

  ' PURPOSE: Init provider combo box
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub ProviderComboFill()

    Call Me.cmb_provider_list_type.Add(ProviderList.ProviderListType.All, _
                                       GLB_NLS_GUI_CONFIGURATION.GetString(497))
    Call Me.cmb_provider_list_type.Add(ProviderList.ProviderListType.Listed, _
                                       GLB_NLS_GUI_CONFIGURATION.GetString(499))
    Call Me.cmb_provider_list_type.Add(ProviderList.ProviderListType.NotListed, _
                                       GLB_NLS_GUI_CONFIGURATION.GetString(498))

    Me.cmb_provider_list_type.SelectedIndex = ProviderList.ProviderListType.All

  End Sub ' ProviderComboFill

  ' PURPOSE: Sets value for all elements in checkboxlist control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub ChkBoxListSetValueAll(ByVal checked As Boolean)

    For _idx As Integer = 0 To Me.chklb_provider_list.Items.Count - 1
      Call Me.chklb_provider_list.SetItemChecked(_idx, checked)
    Next

  End Sub ' ChkBoxListSetValueAll

  ' PURPOSE: Enable/Disable controls depending on provider type selected
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Sub EnableCheckProviderListByListType()

    Select Case Me.cmb_provider_list_type.Value

      Case ProviderList.ProviderListType.All
        Me.chklb_provider_list.Enabled = False
        Call ChkBoxListSetValueAll(True)

      Case ProviderList.ProviderListType.NotListed
        Me.chklb_provider_list.Enabled = True

      Case ProviderList.ProviderListType.Listed
        Me.chklb_provider_list.Enabled = True

      Case Else
        Me.chklb_provider_list.Enabled = False
        Call ChkBoxListSetValueAll(False)
    End Select

  End Sub ' EnableCheckProviderListByListType

#End Region ' Private Functions

#Region " Events "

  ' PURPOSE: Handles event of changing values for provider list type combo
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub cmb_provider_list_type_ValueChangedEvent() Handles cmb_provider_list_type.ValueChangedEvent
    Call EnableCheckProviderListByListType()
  End Sub ' cmb_provider_list_type_ValueChangedEvent

  ' PURPOSE: Handles event of collapsing the check listbox
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub btn_collapse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_collapse.Click
    Me.Collapsed = Not Me.Collapsed
  End Sub ' btn_collapse_Click

  ' PURPOSE: Denies to resize the control size
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub uc_provider_filter_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Resize
    If Me.Width <> CONTROL_WIDTH Then
      Me.Width = CONTROL_WIDTH
    End If
    If Me.Collapsed Then
      If Me.Height <> CONTROL_HEIGHT_COLLAPSED Then
        Me.Height = CONTROL_HEIGHT_COLLAPSED
      End If
    Else
      If Me.Height <> CONTROL_HEIGHT Then
        Me.Height = CONTROL_HEIGHT
      End If
    End If
  End Sub ' uc_provider_filter_Resize

#End Region ' Events

  Private Sub cmb_provider_list_type_MouseLeave(ByVal sender As System.Object, ByVal e As System.EventArgs)

  End Sub
End Class ' uc_provider_filter
