<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_wcp_cashier_config
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(uc_wcp_cashier_config))
    Me.gb_voucher_config = New System.Windows.Forms.GroupBox()
    Me.TabControl1 = New System.Windows.Forms.TabControl()
    Me.TabPage1 = New System.Windows.Forms.TabPage()
    Me.txt_voucher_header = New System.Windows.Forms.TextBox()
    Me.TabPage2 = New System.Windows.Forms.TabPage()
    Me.txt_voucher_footer = New System.Windows.Forms.TextBox()
    Me.TabPage3 = New System.Windows.Forms.TabPage()
    Me.txt_voucher_accounts_customization_footer = New System.Windows.Forms.TextBox()
    Me.gb_wcp_params = New System.Windows.Forms.GroupBox()
    Me.chk_allow_anon = New System.Windows.Forms.CheckBox()
    Me.lbl_entry_min_time_close_session_suffix = New System.Windows.Forms.Label()
    Me.gp_handpays = New System.Windows.Forms.GroupBox()
    Me.lbl_handpay_cancel_time_suffix = New System.Windows.Forms.Label()
    Me.lbl_handpay_time_period_suffix = New System.Windows.Forms.Label()
    Me.Label6 = New System.Windows.Forms.Label()
    Me.Label7 = New System.Windows.Forms.Label()
    Me.gb_wcp_card = New System.Windows.Forms.GroupBox()
    Me.chk_card_player_amount_to_company_b = New System.Windows.Forms.CheckBox()
    Me.Label3 = New System.Windows.Forms.Label()
    Me.Label4 = New System.Windows.Forms.Label()
    Me.pnl_global = New System.Windows.Forms.Panel()
    Me.gb_card_printer = New System.Windows.Forms.GroupBox()
    Me.chk_bold = New System.Windows.Forms.CheckBox()
    Me.uc_entry_printer_name = New GUI_Controls.uc_entry_field()
    Me.uc_entry_font_size = New GUI_Controls.uc_entry_field()
    Me.uc_entry_bottomy = New GUI_Controls.uc_entry_field()
    Me.uc_entry_bottomx = New GUI_Controls.uc_entry_field()
    Me.uc_entry_topy = New GUI_Controls.uc_entry_field()
    Me.uc_entry_topx = New GUI_Controls.uc_entry_field()
    Me.uc_entry_min_time_close_session = New GUI_Controls.uc_entry_field()
    Me.uc_entry_handpay_cancel_time = New GUI_Controls.uc_entry_field()
    Me.uc_entry_handpay_time_period = New GUI_Controls.uc_entry_field()
    Me.ef_card_player_concept = New GUI_Controls.uc_entry_field()
    Me.uc_entry_personal_card_charge_after_times = New GUI_Controls.uc_entry_field()
    Me.uc_entry_personal_card_price_in_points = New GUI_Controls.uc_entry_field()
    Me.Uc_combo_card_refundable = New GUI_Controls.uc_combo()
    Me.uc_entry_personal_card_replacement_price = New GUI_Controls.uc_entry_field()
    Me.uc_entry_personal_card_price = New GUI_Controls.uc_entry_field()
    Me.uc_entry_card_price = New GUI_Controls.uc_entry_field()
    Me.Uc_cmb_language = New GUI_Controls.uc_combo()
    Me.uc_cmb_cashier_mode = New GUI_Controls.uc_combo()
    Me.gb_voucher_config.SuspendLayout()
    Me.TabControl1.SuspendLayout()
    Me.TabPage1.SuspendLayout()
    Me.TabPage2.SuspendLayout()
    Me.TabPage3.SuspendLayout()
    Me.gb_wcp_params.SuspendLayout()
    Me.gp_handpays.SuspendLayout()
    Me.gb_wcp_card.SuspendLayout()
    Me.pnl_global.SuspendLayout()
    Me.gb_card_printer.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_voucher_config
    '
    Me.gb_voucher_config.BackColor = System.Drawing.SystemColors.Control
    Me.gb_voucher_config.Controls.Add(Me.TabControl1)
    Me.gb_voucher_config.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.gb_voucher_config.Location = New System.Drawing.Point(10, 264)
    Me.gb_voucher_config.Name = "gb_voucher_config"
    Me.gb_voucher_config.Size = New System.Drawing.Size(934, 170)
    Me.gb_voucher_config.TabIndex = 1
    Me.gb_voucher_config.TabStop = False
    Me.gb_voucher_config.Text = "xVoucherConfig"
    '
    'TabControl1
    '
    Me.TabControl1.Controls.Add(Me.TabPage1)
    Me.TabControl1.Controls.Add(Me.TabPage2)
    Me.TabControl1.Controls.Add(Me.TabPage3)
    Me.TabControl1.Location = New System.Drawing.Point(15, 17)
    Me.TabControl1.Name = "TabControl1"
    Me.TabControl1.SelectedIndex = 0
    Me.TabControl1.Size = New System.Drawing.Size(591, 147)
    Me.TabControl1.TabIndex = 0
    '
    'TabPage1
    '
    Me.TabPage1.Controls.Add(Me.txt_voucher_header)
    Me.TabPage1.Location = New System.Drawing.Point(4, 22)
    Me.TabPage1.Name = "TabPage1"
    Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage1.Size = New System.Drawing.Size(583, 121)
    Me.TabPage1.TabIndex = 0
    Me.TabPage1.Text = "TabPage1"
    Me.TabPage1.UseVisualStyleBackColor = True
    '
    'txt_voucher_header
    '
    Me.txt_voucher_header.Dock = System.Windows.Forms.DockStyle.Fill
    Me.txt_voucher_header.Location = New System.Drawing.Point(3, 3)
    Me.txt_voucher_header.MaxLength = 1024
    Me.txt_voucher_header.Multiline = True
    Me.txt_voucher_header.Name = "txt_voucher_header"
    Me.txt_voucher_header.Size = New System.Drawing.Size(577, 115)
    Me.txt_voucher_header.TabIndex = 0
    '
    'TabPage2
    '
    Me.TabPage2.Controls.Add(Me.txt_voucher_footer)
    Me.TabPage2.Location = New System.Drawing.Point(4, 22)
    Me.TabPage2.Name = "TabPage2"
    Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage2.Size = New System.Drawing.Size(583, 121)
    Me.TabPage2.TabIndex = 1
    Me.TabPage2.Text = "TabPage2"
    Me.TabPage2.UseVisualStyleBackColor = True
    '
    'txt_voucher_footer
    '
    Me.txt_voucher_footer.Dock = System.Windows.Forms.DockStyle.Fill
    Me.txt_voucher_footer.Location = New System.Drawing.Point(3, 3)
    Me.txt_voucher_footer.MaxLength = 1024
    Me.txt_voucher_footer.Multiline = True
    Me.txt_voucher_footer.Name = "txt_voucher_footer"
    Me.txt_voucher_footer.Size = New System.Drawing.Size(577, 115)
    Me.txt_voucher_footer.TabIndex = 0
    '
    'TabPage3
    '
    Me.TabPage3.Controls.Add(Me.txt_voucher_accounts_customization_footer)
    Me.TabPage3.Location = New System.Drawing.Point(4, 22)
    Me.TabPage3.Name = "TabPage3"
    Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage3.Size = New System.Drawing.Size(583, 121)
    Me.TabPage3.TabIndex = 2
    Me.TabPage3.Text = "TabPage3"
    Me.TabPage3.UseVisualStyleBackColor = True
    '
    'txt_voucher_accounts_customization_footer
    '
    Me.txt_voucher_accounts_customization_footer.Location = New System.Drawing.Point(3, 3)
    Me.txt_voucher_accounts_customization_footer.MaxLength = 1024
    Me.txt_voucher_accounts_customization_footer.Multiline = True
    Me.txt_voucher_accounts_customization_footer.Name = "txt_voucher_accounts_customization_footer"
    Me.txt_voucher_accounts_customization_footer.Size = New System.Drawing.Size(577, 86)
    Me.txt_voucher_accounts_customization_footer.TabIndex = 0
    '
    'gb_wcp_params
    '
    Me.gb_wcp_params.BackColor = System.Drawing.SystemColors.Control
    Me.gb_wcp_params.Controls.Add(Me.chk_allow_anon)
    Me.gb_wcp_params.Controls.Add(Me.lbl_entry_min_time_close_session_suffix)
    Me.gb_wcp_params.Controls.Add(Me.uc_entry_min_time_close_session)
    Me.gb_wcp_params.Controls.Add(Me.gp_handpays)
    Me.gb_wcp_params.Controls.Add(Me.gb_wcp_card)
    Me.gb_wcp_params.Controls.Add(Me.Uc_cmb_language)
    Me.gb_wcp_params.Controls.Add(Me.uc_cmb_cashier_mode)
    Me.gb_wcp_params.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.gb_wcp_params.Location = New System.Drawing.Point(10, 3)
    Me.gb_wcp_params.Name = "gb_wcp_params"
    Me.gb_wcp_params.Size = New System.Drawing.Size(934, 255)
    Me.gb_wcp_params.TabIndex = 0
    Me.gb_wcp_params.TabStop = False
    Me.gb_wcp_params.Text = "xCashierParams"
    '
    'chk_allow_anon
    '
    Me.chk_allow_anon.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_allow_anon.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_allow_anon.Location = New System.Drawing.Point(703, 144)
    Me.chk_allow_anon.Name = "chk_allow_anon"
    Me.chk_allow_anon.RightToLeft = System.Windows.Forms.RightToLeft.Yes
    Me.chk_allow_anon.Size = New System.Drawing.Size(225, 17)
    Me.chk_allow_anon.TabIndex = 21
    Me.chk_allow_anon.Text = "xAllowAnon"
    Me.chk_allow_anon.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_allow_anon.UseVisualStyleBackColor = True
    '
    'lbl_entry_min_time_close_session_suffix
    '
    Me.lbl_entry_min_time_close_session_suffix.Location = New System.Drawing.Point(556, 16)
    Me.lbl_entry_min_time_close_session_suffix.Name = "lbl_entry_min_time_close_session_suffix"
    Me.lbl_entry_min_time_close_session_suffix.Size = New System.Drawing.Size(36, 24)
    Me.lbl_entry_min_time_close_session_suffix.TabIndex = 20
    Me.lbl_entry_min_time_close_session_suffix.Text = "xsec"
    Me.lbl_entry_min_time_close_session_suffix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'gp_handpays
    '
    Me.gp_handpays.Controls.Add(Me.lbl_handpay_cancel_time_suffix)
    Me.gp_handpays.Controls.Add(Me.lbl_handpay_time_period_suffix)
    Me.gp_handpays.Controls.Add(Me.uc_entry_handpay_cancel_time)
    Me.gp_handpays.Controls.Add(Me.uc_entry_handpay_time_period)
    Me.gp_handpays.Controls.Add(Me.Label6)
    Me.gp_handpays.Controls.Add(Me.Label7)
    Me.gp_handpays.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.gp_handpays.Location = New System.Drawing.Point(703, 44)
    Me.gp_handpays.Name = "gp_handpays"
    Me.gp_handpays.Size = New System.Drawing.Size(225, 90)
    Me.gp_handpays.TabIndex = 5
    Me.gp_handpays.TabStop = False
    Me.gp_handpays.Text = "xHandPays"
    '
    'lbl_handpay_cancel_time_suffix
    '
    Me.lbl_handpay_cancel_time_suffix.Location = New System.Drawing.Point(183, 55)
    Me.lbl_handpay_cancel_time_suffix.Name = "lbl_handpay_cancel_time_suffix"
    Me.lbl_handpay_cancel_time_suffix.Size = New System.Drawing.Size(36, 24)
    Me.lbl_handpay_cancel_time_suffix.TabIndex = 15
    Me.lbl_handpay_cancel_time_suffix.Text = "xsec"
    Me.lbl_handpay_cancel_time_suffix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_handpay_time_period_suffix
    '
    Me.lbl_handpay_time_period_suffix.Location = New System.Drawing.Point(183, 25)
    Me.lbl_handpay_time_period_suffix.Name = "lbl_handpay_time_period_suffix"
    Me.lbl_handpay_time_period_suffix.Size = New System.Drawing.Size(36, 24)
    Me.lbl_handpay_time_period_suffix.TabIndex = 14
    Me.lbl_handpay_time_period_suffix.Text = "xsec"
    Me.lbl_handpay_time_period_suffix.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'Label6
    '
    Me.Label6.Location = New System.Drawing.Point(490, 234)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(15, 24)
    Me.Label6.TabIndex = 6
    Me.Label6.Text = "%"
    Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Label7
    '
    Me.Label7.Location = New System.Drawing.Point(490, 204)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(15, 24)
    Me.Label7.TabIndex = 5
    Me.Label7.Text = "%"
    Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'gb_wcp_card
    '
    Me.gb_wcp_card.Controls.Add(Me.ef_card_player_concept)
    Me.gb_wcp_card.Controls.Add(Me.chk_card_player_amount_to_company_b)
    Me.gb_wcp_card.Controls.Add(Me.uc_entry_personal_card_charge_after_times)
    Me.gb_wcp_card.Controls.Add(Me.uc_entry_personal_card_price_in_points)
    Me.gb_wcp_card.Controls.Add(Me.Uc_combo_card_refundable)
    Me.gb_wcp_card.Controls.Add(Me.uc_entry_personal_card_replacement_price)
    Me.gb_wcp_card.Controls.Add(Me.uc_entry_personal_card_price)
    Me.gb_wcp_card.Controls.Add(Me.uc_entry_card_price)
    Me.gb_wcp_card.Controls.Add(Me.Label3)
    Me.gb_wcp_card.Controls.Add(Me.Label4)
    Me.gb_wcp_card.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.gb_wcp_card.Location = New System.Drawing.Point(7, 44)
    Me.gb_wcp_card.Name = "gb_wcp_card"
    Me.gb_wcp_card.Size = New System.Drawing.Size(690, 205)
    Me.gb_wcp_card.TabIndex = 4
    Me.gb_wcp_card.TabStop = False
    Me.gb_wcp_card.Text = "xPrecioTarjetas"
    '
    'chk_card_player_amount_to_company_b
    '
    Me.chk_card_player_amount_to_company_b.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_card_player_amount_to_company_b.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_card_player_amount_to_company_b.Location = New System.Drawing.Point(461, 92)
    Me.chk_card_player_amount_to_company_b.Name = "chk_card_player_amount_to_company_b"
    Me.chk_card_player_amount_to_company_b.RightToLeft = System.Windows.Forms.RightToLeft.Yes
    Me.chk_card_player_amount_to_company_b.Size = New System.Drawing.Size(219, 17)
    Me.chk_card_player_amount_to_company_b.TabIndex = 6
    Me.chk_card_player_amount_to_company_b.Text = "xCompanyBAmount"
    Me.chk_card_player_amount_to_company_b.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_card_player_amount_to_company_b.UseVisualStyleBackColor = True
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(490, 234)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(15, 24)
    Me.Label3.TabIndex = 6
    Me.Label3.Text = "%"
    Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Label4
    '
    Me.Label4.Location = New System.Drawing.Point(490, 204)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(15, 24)
    Me.Label4.TabIndex = 5
    Me.Label4.Text = "%"
    Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'pnl_global
    '
    Me.pnl_global.AutoSize = True
    Me.pnl_global.BackColor = System.Drawing.SystemColors.Control
    Me.pnl_global.Controls.Add(Me.gb_card_printer)
    Me.pnl_global.Controls.Add(Me.gb_voucher_config)
    Me.pnl_global.Controls.Add(Me.gb_wcp_params)
    Me.pnl_global.Location = New System.Drawing.Point(3, 0)
    Me.pnl_global.Name = "pnl_global"
    Me.pnl_global.Size = New System.Drawing.Size(950, 582)
    Me.pnl_global.TabIndex = 0
    '
    'gb_card_printer
    '
    Me.gb_card_printer.Controls.Add(Me.chk_bold)
    Me.gb_card_printer.Controls.Add(Me.uc_entry_printer_name)
    Me.gb_card_printer.Controls.Add(Me.uc_entry_font_size)
    Me.gb_card_printer.Controls.Add(Me.uc_entry_bottomy)
    Me.gb_card_printer.Controls.Add(Me.uc_entry_bottomx)
    Me.gb_card_printer.Controls.Add(Me.uc_entry_topy)
    Me.gb_card_printer.Controls.Add(Me.uc_entry_topx)
    Me.gb_card_printer.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.gb_card_printer.Location = New System.Drawing.Point(10, 438)
    Me.gb_card_printer.Name = "gb_card_printer"
    Me.gb_card_printer.Size = New System.Drawing.Size(934, 141)
    Me.gb_card_printer.TabIndex = 2
    Me.gb_card_printer.TabStop = False
    Me.gb_card_printer.Text = "xCardPrinter"
    '
    'chk_bold
    '
    Me.chk_bold.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_bold.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_bold.Location = New System.Drawing.Point(191, 56)
    Me.chk_bold.Name = "chk_bold"
    Me.chk_bold.Size = New System.Drawing.Size(133, 17)
    Me.chk_bold.TabIndex = 5
    Me.chk_bold.Text = "chk_bold"
    Me.chk_bold.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_bold.UseVisualStyleBackColor = True
    '
    'uc_entry_printer_name
    '
    Me.uc_entry_printer_name.DoubleValue = 0.0R
    Me.uc_entry_printer_name.IntegerValue = 0
    Me.uc_entry_printer_name.IsReadOnly = False
    Me.uc_entry_printer_name.Location = New System.Drawing.Point(193, 80)
    Me.uc_entry_printer_name.Name = "uc_entry_printer_name"
    Me.uc_entry_printer_name.PlaceHolder = Nothing
    Me.uc_entry_printer_name.Size = New System.Drawing.Size(352, 24)
    Me.uc_entry_printer_name.SufixText = "Sufix Text"
    Me.uc_entry_printer_name.SufixTextVisible = True
    Me.uc_entry_printer_name.TabIndex = 6
    Me.uc_entry_printer_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_printer_name.TextValue = ""
    Me.uc_entry_printer_name.TextWidth = 115
    Me.uc_entry_printer_name.Value = ""
    Me.uc_entry_printer_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_font_size
    '
    Me.uc_entry_font_size.DoubleValue = 0.0R
    Me.uc_entry_font_size.IntegerValue = 0
    Me.uc_entry_font_size.IsReadOnly = False
    Me.uc_entry_font_size.Location = New System.Drawing.Point(193, 20)
    Me.uc_entry_font_size.Name = "uc_entry_font_size"
    Me.uc_entry_font_size.PlaceHolder = Nothing
    Me.uc_entry_font_size.Size = New System.Drawing.Size(189, 24)
    Me.uc_entry_font_size.SufixText = "Sufix Text"
    Me.uc_entry_font_size.SufixTextVisible = True
    Me.uc_entry_font_size.TabIndex = 4
    Me.uc_entry_font_size.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_font_size.TextValue = ""
    Me.uc_entry_font_size.TextWidth = 115
    Me.uc_entry_font_size.Value = ""
    Me.uc_entry_font_size.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_bottomy
    '
    Me.uc_entry_bottomy.DoubleValue = 0.0R
    Me.uc_entry_bottomy.IntegerValue = 0
    Me.uc_entry_bottomy.IsReadOnly = False
    Me.uc_entry_bottomy.Location = New System.Drawing.Point(7, 111)
    Me.uc_entry_bottomy.Name = "uc_entry_bottomy"
    Me.uc_entry_bottomy.PlaceHolder = Nothing
    Me.uc_entry_bottomy.Size = New System.Drawing.Size(180, 24)
    Me.uc_entry_bottomy.SufixText = "Sufix Text"
    Me.uc_entry_bottomy.SufixTextVisible = True
    Me.uc_entry_bottomy.TabIndex = 3
    Me.uc_entry_bottomy.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_bottomy.TextValue = ""
    Me.uc_entry_bottomy.TextWidth = 105
    Me.uc_entry_bottomy.Value = ""
    Me.uc_entry_bottomy.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_bottomx
    '
    Me.uc_entry_bottomx.DoubleValue = 0.0R
    Me.uc_entry_bottomx.IntegerValue = 0
    Me.uc_entry_bottomx.IsReadOnly = False
    Me.uc_entry_bottomx.Location = New System.Drawing.Point(7, 80)
    Me.uc_entry_bottomx.Name = "uc_entry_bottomx"
    Me.uc_entry_bottomx.PlaceHolder = Nothing
    Me.uc_entry_bottomx.Size = New System.Drawing.Size(180, 24)
    Me.uc_entry_bottomx.SufixText = "Sufix Text"
    Me.uc_entry_bottomx.SufixTextVisible = True
    Me.uc_entry_bottomx.TabIndex = 2
    Me.uc_entry_bottomx.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_bottomx.TextValue = ""
    Me.uc_entry_bottomx.TextWidth = 105
    Me.uc_entry_bottomx.Value = ""
    Me.uc_entry_bottomx.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_topy
    '
    Me.uc_entry_topy.DoubleValue = 0.0R
    Me.uc_entry_topy.IntegerValue = 0
    Me.uc_entry_topy.IsReadOnly = False
    Me.uc_entry_topy.Location = New System.Drawing.Point(7, 49)
    Me.uc_entry_topy.Name = "uc_entry_topy"
    Me.uc_entry_topy.PlaceHolder = Nothing
    Me.uc_entry_topy.Size = New System.Drawing.Size(180, 24)
    Me.uc_entry_topy.SufixText = "Sufix Text"
    Me.uc_entry_topy.SufixTextVisible = True
    Me.uc_entry_topy.TabIndex = 1
    Me.uc_entry_topy.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_topy.TextValue = ""
    Me.uc_entry_topy.TextWidth = 105
    Me.uc_entry_topy.Value = ""
    Me.uc_entry_topy.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_topx
    '
    Me.uc_entry_topx.DoubleValue = 0.0R
    Me.uc_entry_topx.IntegerValue = 0
    Me.uc_entry_topx.IsReadOnly = False
    Me.uc_entry_topx.Location = New System.Drawing.Point(7, 19)
    Me.uc_entry_topx.Name = "uc_entry_topx"
    Me.uc_entry_topx.PlaceHolder = Nothing
    Me.uc_entry_topx.Size = New System.Drawing.Size(180, 24)
    Me.uc_entry_topx.SufixText = "Sufix Text"
    Me.uc_entry_topx.SufixTextVisible = True
    Me.uc_entry_topx.TabIndex = 0
    Me.uc_entry_topx.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_topx.TextValue = ""
    Me.uc_entry_topx.TextWidth = 105
    Me.uc_entry_topx.Value = ""
    Me.uc_entry_topx.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_min_time_close_session
    '
    Me.uc_entry_min_time_close_session.DoubleValue = 0.0R
    Me.uc_entry_min_time_close_session.ForeColor = System.Drawing.SystemColors.ControlText
    Me.uc_entry_min_time_close_session.IntegerValue = 0
    Me.uc_entry_min_time_close_session.IsReadOnly = False
    Me.uc_entry_min_time_close_session.Location = New System.Drawing.Point(338, 15)
    Me.uc_entry_min_time_close_session.Name = "uc_entry_min_time_close_session"
    Me.uc_entry_min_time_close_session.PlaceHolder = Nothing
    Me.uc_entry_min_time_close_session.Size = New System.Drawing.Size(214, 24)
    Me.uc_entry_min_time_close_session.SufixText = "Sufix Text"
    Me.uc_entry_min_time_close_session.SufixTextVisible = True
    Me.uc_entry_min_time_close_session.TabIndex = 3
    Me.uc_entry_min_time_close_session.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_min_time_close_session.TextValue = ""
    Me.uc_entry_min_time_close_session.TextWidth = 172
    Me.uc_entry_min_time_close_session.Value = ""
    Me.uc_entry_min_time_close_session.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_handpay_cancel_time
    '
    Me.uc_entry_handpay_cancel_time.DoubleValue = 0.0R
    Me.uc_entry_handpay_cancel_time.ForeColor = System.Drawing.SystemColors.ControlText
    Me.uc_entry_handpay_cancel_time.IntegerValue = 0
    Me.uc_entry_handpay_cancel_time.IsReadOnly = False
    Me.uc_entry_handpay_cancel_time.Location = New System.Drawing.Point(10, 55)
    Me.uc_entry_handpay_cancel_time.Name = "uc_entry_handpay_cancel_time"
    Me.uc_entry_handpay_cancel_time.PlaceHolder = Nothing
    Me.uc_entry_handpay_cancel_time.Size = New System.Drawing.Size(169, 24)
    Me.uc_entry_handpay_cancel_time.SufixText = "Sufix Text"
    Me.uc_entry_handpay_cancel_time.SufixTextVisible = True
    Me.uc_entry_handpay_cancel_time.TabIndex = 1
    Me.uc_entry_handpay_cancel_time.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_handpay_cancel_time.TextValue = ""
    Me.uc_entry_handpay_cancel_time.TextWidth = 125
    Me.uc_entry_handpay_cancel_time.Value = ""
    Me.uc_entry_handpay_cancel_time.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_handpay_time_period
    '
    Me.uc_entry_handpay_time_period.DoubleValue = 0.0R
    Me.uc_entry_handpay_time_period.ForeColor = System.Drawing.SystemColors.ControlText
    Me.uc_entry_handpay_time_period.IntegerValue = 0
    Me.uc_entry_handpay_time_period.IsReadOnly = False
    Me.uc_entry_handpay_time_period.Location = New System.Drawing.Point(10, 25)
    Me.uc_entry_handpay_time_period.Name = "uc_entry_handpay_time_period"
    Me.uc_entry_handpay_time_period.PlaceHolder = Nothing
    Me.uc_entry_handpay_time_period.Size = New System.Drawing.Size(169, 24)
    Me.uc_entry_handpay_time_period.SufixText = "Sufix Text"
    Me.uc_entry_handpay_time_period.SufixTextVisible = True
    Me.uc_entry_handpay_time_period.TabIndex = 0
    Me.uc_entry_handpay_time_period.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_handpay_time_period.TextValue = ""
    Me.uc_entry_handpay_time_period.TextWidth = 125
    Me.uc_entry_handpay_time_period.Value = ""
    Me.uc_entry_handpay_time_period.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_card_player_concept
    '
    Me.ef_card_player_concept.DoubleValue = 0.0R
    Me.ef_card_player_concept.IntegerValue = 0
    Me.ef_card_player_concept.IsReadOnly = False
    Me.ef_card_player_concept.Location = New System.Drawing.Point(355, 55)
    Me.ef_card_player_concept.Name = "ef_card_player_concept"
    Me.ef_card_player_concept.PlaceHolder = Nothing
    Me.ef_card_player_concept.Size = New System.Drawing.Size(330, 24)
    Me.ef_card_player_concept.SufixText = "Sufix Text"
    Me.ef_card_player_concept.SufixTextVisible = True
    Me.ef_card_player_concept.TabIndex = 5
    Me.ef_card_player_concept.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_card_player_concept.TextValue = ""
    Me.ef_card_player_concept.TextWidth = 130
    Me.ef_card_player_concept.Value = ""
    Me.ef_card_player_concept.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_personal_card_charge_after_times
    '
    Me.uc_entry_personal_card_charge_after_times.DoubleValue = 0.0R
    Me.uc_entry_personal_card_charge_after_times.IntegerValue = 0
    Me.uc_entry_personal_card_charge_after_times.IsReadOnly = False
    Me.uc_entry_personal_card_charge_after_times.Location = New System.Drawing.Point(6, 85)
    Me.uc_entry_personal_card_charge_after_times.Name = "uc_entry_personal_card_charge_after_times"
    Me.uc_entry_personal_card_charge_after_times.PlaceHolder = Nothing
    Me.uc_entry_personal_card_charge_after_times.Size = New System.Drawing.Size(343, 24)
    Me.uc_entry_personal_card_charge_after_times.SufixText = "Sufix Text"
    Me.uc_entry_personal_card_charge_after_times.SufixTextVisible = True
    Me.uc_entry_personal_card_charge_after_times.TabIndex = 2
    Me.uc_entry_personal_card_charge_after_times.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_personal_card_charge_after_times.TextValue = ""
    Me.uc_entry_personal_card_charge_after_times.TextWidth = 263
    Me.uc_entry_personal_card_charge_after_times.Value = ""
    Me.uc_entry_personal_card_charge_after_times.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_personal_card_price_in_points
    '
    Me.uc_entry_personal_card_price_in_points.DoubleValue = 0.0R
    Me.uc_entry_personal_card_price_in_points.IntegerValue = 0
    Me.uc_entry_personal_card_price_in_points.IsReadOnly = False
    Me.uc_entry_personal_card_price_in_points.Location = New System.Drawing.Point(342, 115)
    Me.uc_entry_personal_card_price_in_points.Name = "uc_entry_personal_card_price_in_points"
    Me.uc_entry_personal_card_price_in_points.PlaceHolder = Nothing
    Me.uc_entry_personal_card_price_in_points.Size = New System.Drawing.Size(343, 24)
    Me.uc_entry_personal_card_price_in_points.SufixText = "Sufix Text"
    Me.uc_entry_personal_card_price_in_points.SufixTextVisible = True
    Me.uc_entry_personal_card_price_in_points.TabIndex = 7
    Me.uc_entry_personal_card_price_in_points.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_personal_card_price_in_points.TextValue = ""
    Me.uc_entry_personal_card_price_in_points.TextWidth = 263
    Me.uc_entry_personal_card_price_in_points.Value = ""
    Me.uc_entry_personal_card_price_in_points.ValueForeColor = System.Drawing.Color.Blue
    Me.uc_entry_personal_card_price_in_points.Visible = False
    '
    'Uc_combo_card_refundable
    '
    Me.Uc_combo_card_refundable.AllowUnlistedValues = False
    Me.Uc_combo_card_refundable.AutoCompleteMode = False
    Me.Uc_combo_card_refundable.IsReadOnly = False
    Me.Uc_combo_card_refundable.Location = New System.Drawing.Point(8, 114)
    Me.Uc_combo_card_refundable.Name = "Uc_combo_card_refundable"
    Me.Uc_combo_card_refundable.SelectedIndex = -1
    Me.Uc_combo_card_refundable.Size = New System.Drawing.Size(341, 24)
    Me.Uc_combo_card_refundable.SufixText = "Sufix Text"
    Me.Uc_combo_card_refundable.SufixTextVisible = True
    Me.Uc_combo_card_refundable.TabIndex = 3
    Me.Uc_combo_card_refundable.TextCombo = Nothing
    Me.Uc_combo_card_refundable.TextWidth = 155
    '
    'uc_entry_personal_card_replacement_price
    '
    Me.uc_entry_personal_card_replacement_price.DoubleValue = 0.0R
    Me.uc_entry_personal_card_replacement_price.IntegerValue = 0
    Me.uc_entry_personal_card_replacement_price.IsReadOnly = False
    Me.uc_entry_personal_card_replacement_price.Location = New System.Drawing.Point(6, 56)
    Me.uc_entry_personal_card_replacement_price.Name = "uc_entry_personal_card_replacement_price"
    Me.uc_entry_personal_card_replacement_price.PlaceHolder = Nothing
    Me.uc_entry_personal_card_replacement_price.Size = New System.Drawing.Size(343, 24)
    Me.uc_entry_personal_card_replacement_price.SufixText = "Sufix Text"
    Me.uc_entry_personal_card_replacement_price.SufixTextVisible = True
    Me.uc_entry_personal_card_replacement_price.TabIndex = 1
    Me.uc_entry_personal_card_replacement_price.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_personal_card_replacement_price.TextValue = ""
    Me.uc_entry_personal_card_replacement_price.TextWidth = 263
    Me.uc_entry_personal_card_replacement_price.Value = ""
    Me.uc_entry_personal_card_replacement_price.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_personal_card_price
    '
    Me.uc_entry_personal_card_price.DoubleValue = 0.0R
    Me.uc_entry_personal_card_price.IntegerValue = 0
    Me.uc_entry_personal_card_price.IsReadOnly = False
    Me.uc_entry_personal_card_price.Location = New System.Drawing.Point(6, 25)
    Me.uc_entry_personal_card_price.Name = "uc_entry_personal_card_price"
    Me.uc_entry_personal_card_price.PlaceHolder = Nothing
    Me.uc_entry_personal_card_price.Size = New System.Drawing.Size(343, 24)
    Me.uc_entry_personal_card_price.SufixText = "Sufix Text"
    Me.uc_entry_personal_card_price.SufixTextVisible = True
    Me.uc_entry_personal_card_price.TabIndex = 0
    Me.uc_entry_personal_card_price.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_personal_card_price.TextValue = ""
    Me.uc_entry_personal_card_price.TextWidth = 263
    Me.uc_entry_personal_card_price.Value = ""
    Me.uc_entry_personal_card_price.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_entry_card_price
    '
    Me.uc_entry_card_price.DoubleValue = 0.0R
    Me.uc_entry_card_price.IntegerValue = 0
    Me.uc_entry_card_price.IsReadOnly = False
    Me.uc_entry_card_price.Location = New System.Drawing.Point(342, 26)
    Me.uc_entry_card_price.Name = "uc_entry_card_price"
    Me.uc_entry_card_price.PlaceHolder = Nothing
    Me.uc_entry_card_price.Size = New System.Drawing.Size(343, 24)
    Me.uc_entry_card_price.SufixText = "Sufix Text"
    Me.uc_entry_card_price.SufixTextVisible = True
    Me.uc_entry_card_price.TabIndex = 4
    Me.uc_entry_card_price.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_card_price.TextValue = ""
    Me.uc_entry_card_price.TextWidth = 263
    Me.uc_entry_card_price.Value = ""
    Me.uc_entry_card_price.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_cmb_language
    '
    Me.Uc_cmb_language.AllowUnlistedValues = False
    Me.Uc_cmb_language.AutoCompleteMode = False
    Me.Uc_cmb_language.IsReadOnly = False
    Me.Uc_cmb_language.Location = New System.Drawing.Point(210, 14)
    Me.Uc_cmb_language.Name = "Uc_cmb_language"
    Me.Uc_cmb_language.SelectedIndex = -1
    Me.Uc_cmb_language.Size = New System.Drawing.Size(128, 24)
    Me.Uc_cmb_language.SufixText = "Sufix Text"
    Me.Uc_cmb_language.SufixTextVisible = True
    Me.Uc_cmb_language.TabIndex = 2
    Me.Uc_cmb_language.TextCombo = Nothing
    Me.Uc_cmb_language.TextWidth = 62
    '
    'uc_cmb_cashier_mode
    '
    Me.uc_cmb_cashier_mode.AllowUnlistedValues = False
    Me.uc_cmb_cashier_mode.AutoCompleteMode = False
    Me.uc_cmb_cashier_mode.IsReadOnly = False
    Me.uc_cmb_cashier_mode.Location = New System.Drawing.Point(7, 13)
    Me.uc_cmb_cashier_mode.Name = "uc_cmb_cashier_mode"
    Me.uc_cmb_cashier_mode.SelectedIndex = -1
    Me.uc_cmb_cashier_mode.Size = New System.Drawing.Size(203, 24)
    Me.uc_cmb_cashier_mode.SufixText = "Sufix Text"
    Me.uc_cmb_cashier_mode.SufixTextVisible = True
    Me.uc_cmb_cashier_mode.TabIndex = 0
    Me.uc_cmb_cashier_mode.TextCombo = Nothing
    Me.uc_cmb_cashier_mode.TextWidth = 100
    '
    'uc_wcp_cashier_config
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.pnl_global)
    Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Name = "uc_wcp_cashier_config"
    Me.Size = New System.Drawing.Size(958, 585)
    Me.gb_voucher_config.ResumeLayout(False)
    Me.TabControl1.ResumeLayout(False)
    Me.TabPage1.ResumeLayout(False)
    Me.TabPage1.PerformLayout()
    Me.TabPage2.ResumeLayout(False)
    Me.TabPage2.PerformLayout()
    Me.TabPage3.ResumeLayout(False)
    Me.TabPage3.PerformLayout()
    Me.gb_wcp_params.ResumeLayout(False)
    Me.gp_handpays.ResumeLayout(False)
    Me.gb_wcp_card.ResumeLayout(False)
    Me.pnl_global.ResumeLayout(False)
    Me.gb_card_printer.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents gb_voucher_config As System.Windows.Forms.GroupBox
  Friend WithEvents gb_wcp_params As System.Windows.Forms.GroupBox
  Friend WithEvents pnl_global As System.Windows.Forms.Panel
  Friend WithEvents uc_cmb_cashier_mode As GUI_Controls.uc_combo
  Friend WithEvents Uc_cmb_language As GUI_Controls.uc_combo
  Friend WithEvents gb_card_printer As System.Windows.Forms.GroupBox
  Friend WithEvents uc_entry_font_size As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_bottomy As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_bottomx As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_topy As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_topx As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_printer_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_bold As System.Windows.Forms.CheckBox
  Friend WithEvents gb_wcp_card As System.Windows.Forms.GroupBox
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents Label4 As System.Windows.Forms.Label
  Friend WithEvents uc_entry_personal_card_replacement_price As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_personal_card_price As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_card_price As GUI_Controls.uc_entry_field
  Friend WithEvents gp_handpays As System.Windows.Forms.GroupBox
  Friend WithEvents Label6 As System.Windows.Forms.Label
  Friend WithEvents Label7 As System.Windows.Forms.Label
  Friend WithEvents uc_entry_handpay_cancel_time As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_handpay_time_period As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_handpay_cancel_time_suffix As System.Windows.Forms.Label
  Friend WithEvents lbl_handpay_time_period_suffix As System.Windows.Forms.Label
  Friend WithEvents lbl_entry_min_time_close_session_suffix As System.Windows.Forms.Label
  Friend WithEvents uc_entry_min_time_close_session As GUI_Controls.uc_entry_field
  Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
  Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
  Friend WithEvents txt_voucher_header As System.Windows.Forms.TextBox
  Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
  Friend WithEvents txt_voucher_footer As System.Windows.Forms.TextBox
  Friend WithEvents chk_allow_anon As System.Windows.Forms.CheckBox
  Friend WithEvents Uc_combo_card_refundable As GUI_Controls.uc_combo
  Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
  Friend WithEvents txt_voucher_accounts_customization_footer As System.Windows.Forms.TextBox
  Friend WithEvents uc_entry_personal_card_price_in_points As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_personal_card_charge_after_times As GUI_Controls.uc_entry_field
  Friend WithEvents chk_card_player_amount_to_company_b As System.Windows.Forms.CheckBox
  Friend WithEvents ef_card_player_concept As GUI_Controls.uc_entry_field

End Class
