<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_combo_one_all
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_combo_one_all = New System.Windows.Forms.GroupBox
    Me.flp_container = New System.Windows.Forms.FlowLayoutPanel
    Me.opt_one = New System.Windows.Forms.RadioButton
    Me.opt_all = New System.Windows.Forms.RadioButton
    Me.cmb_sel = New GUI_Controls.uc_combo
    Me.gb_combo_one_all.SuspendLayout()
    Me.flp_container.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_combo_one_all
    '
    Me.gb_combo_one_all.Controls.Add(Me.flp_container)
    Me.gb_combo_one_all.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.gb_combo_one_all.Location = New System.Drawing.Point(0, 0)
    Me.gb_combo_one_all.Margin = New System.Windows.Forms.Padding(0)
    Me.gb_combo_one_all.Name = "gb_combo_one_all"
    Me.gb_combo_one_all.Size = New System.Drawing.Size(306, 81)
    Me.gb_combo_one_all.TabIndex = 5
    Me.gb_combo_one_all.TabStop = False
    '
    'flp_container
    '
    Me.flp_container.AutoSize = True
    Me.flp_container.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.flp_container.Controls.Add(Me.opt_one)
    Me.flp_container.Controls.Add(Me.opt_all)
    Me.flp_container.Controls.Add(Me.cmb_sel)
    Me.flp_container.Dock = System.Windows.Forms.DockStyle.Fill
    Me.flp_container.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
    Me.flp_container.Location = New System.Drawing.Point(3, 17)
    Me.flp_container.Margin = New System.Windows.Forms.Padding(0)
    Me.flp_container.Name = "flp_container"
    Me.flp_container.Size = New System.Drawing.Size(300, 61)
    Me.flp_container.TabIndex = 6
    '
    'opt_one
    '
    Me.opt_one.Location = New System.Drawing.Point(3, 3)
    Me.opt_one.Name = "opt_one"
    Me.opt_one.Size = New System.Drawing.Size(84, 24)
    Me.opt_one.TabIndex = 0
    Me.opt_one.Text = "xOne"
    '
    'opt_all
    '
    Me.opt_all.Location = New System.Drawing.Point(3, 33)
    Me.opt_all.Name = "opt_all"
    Me.opt_all.Size = New System.Drawing.Size(75, 24)
    Me.opt_all.TabIndex = 1
    Me.opt_all.Text = "xAll"
    '
    'cmb_sel
    '
    Me.cmb_sel.AllowUnlistedValues = False
    Me.cmb_sel.AutoCompleteMode = False
    Me.cmb_sel.IsReadOnly = False
    Me.cmb_sel.Location = New System.Drawing.Point(93, 3)
    Me.cmb_sel.Name = "cmb_sel"
    Me.cmb_sel.SelectedIndex = -1
    Me.cmb_sel.Size = New System.Drawing.Size(186, 25)
    Me.cmb_sel.SufixText = "Sufix Text"
    Me.cmb_sel.SufixTextVisible = True
    Me.cmb_sel.TabIndex = 2
    Me.cmb_sel.TextVisible = False
    Me.cmb_sel.TextWidth = 0
    '
    'uc_combo_one_all
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.AutoSize = True
    Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.Controls.Add(Me.gb_combo_one_all)
    Me.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.Name = "uc_combo_one_all"
    Me.Size = New System.Drawing.Size(306, 81)
    Me.gb_combo_one_all.ResumeLayout(False)
    Me.gb_combo_one_all.PerformLayout()
    Me.flp_container.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_combo_one_all As System.Windows.Forms.GroupBox
  Friend WithEvents opt_one As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_sel As GUI_Controls.uc_combo
  Friend WithEvents flp_container As System.Windows.Forms.FlowLayoutPanel

End Class
