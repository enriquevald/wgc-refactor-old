'-------------------------------------------------------------------
' Copyright © 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_identity_data.vb
' DESCRIPTION:   
' AUTHOR:        Marcos Piedra Osuna
' CREATION DATE: 20-APR-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 20-APR-2012  MPO    Initial version
' 10-AUG-2012  DDM    Added name, lat name1 and last name2, instead of full name
' 12-FEB-2014  FBA    Added account holder id type control via CardData class IdentificationTypes
' 30-SEP-2015  FOS    Generated documents external retention
' 20-JUN-2016  LTC    Product Backlog Item 13612,13613,13614: Witholding Evidence.
' 27-JUN-2016  LTC    Bug 14875:Witholding Evidence - Wrong Label in General Operations Configuration 
' 13-JUL-2016  LTC    Bug 15581: Witholding Documents: Change NLS for Codere
' 26-OCT-2016  LTC    Bug 18345:Witholding: Combobox "Generate witholding document evidence" shows a word without translate.
' 01-MAR-2017  FGB    Bug 24126: Withholding generation: The Excel data is incorrect
' -------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports WSI.Common
Imports System.Drawing
Imports System.Drawing.Imaging

Public Class uc_identity_data

  Public Enum Mode
    INDETITY_DATA = 0
    PARAMETERS_CONFIGURATION = 1
    REPRESENTATIVE = 2
    COMPANY = 3
  End Enum

#Region "Member"

  Private m_mode As Mode = Mode.INDETITY_DATA

  Private Const MAX_LAST_NAME_LEN As Integer = 150
  Private Const MAX_CURP_LEN As Integer = 20
  Private Const MAX_NAME As Integer = 50

  'Mouse Wheel
  Private Const SCALE_CONSTANT1 As Double = 0.01
  'Buttons
  Private Const SCALE_CONSTANT2 As Double = 0.05

  Private m_current_directory As String
  Private m_cropping_area As Boolean = False
  Private m_enter_panel_sign As Boolean = False
  Private m_moving_btn As Boolean = False
  Private m_img_sign As Image
  Private m_old_sign As Image
  Dim m_x As Integer
  Dim m_y As Integer
  Dim m_dx As Integer
  Dim m_dy As Integer
  Dim m_scale As Double
  Dim m_with As Integer
  Dim m_height As Integer

#End Region

#Region "Properties"

  Public Property RepresentationMode() As Mode
    Get
      Return m_mode
    End Get
    Set(ByVal value As Mode)
      m_mode = value
      Call ShowMode()
    End Set
  End Property

  Public Property TextGroupBox() As String
    Set(ByVal value As String)
      If Me.DesignMode Then
        Select Case Me.m_mode
          Case Mode.COMPANY
            Me.gp_company.Text = value
          Case Mode.INDETITY_DATA
            Me.gp_identity.Text = value
          Case Mode.REPRESENTATIVE
            Me.gp_representative.Text = value
        End Select
      End If
    End Set
    Get
      Select Case Me.m_mode
        Case Mode.COMPANY
          Return Me.gp_company.Text
        Case Mode.INDETITY_DATA
          Return Me.gp_identity.Text
        Case Mode.REPRESENTATIVE
          Return Me.gp_representative.Text
      End Select
      Return ""
    End Get
  End Property

  Public WriteOnly Property TextGroupBoxCompany() As String
    Set(ByVal value As String)
      If Me.DesignMode Then
        Me.gp_company.Text = value
      End If
    End Set
  End Property

  Public WriteOnly Property TextGroupBoxRepresentative() As String
    Set(ByVal value As String)
      If Me.DesignMode Then
        Me.gp_representative.Text = value
      End If
    End Set
  End Property

#Region "Identity"

  <ComponentModel.Browsable(False)> _
  Public Property IdentityID1() As String
    Get
      Return Me.ef_identity_id_1.Value
    End Get
    Set(ByVal value As String)
      Me.ef_identity_id_1.Value = value
    End Set
  End Property

  <ComponentModel.Browsable(False)> _
  Public ReadOnly Property IdentityID1Text() As String
    Get
      Return Me.ef_identity_id_1.Text
    End Get
  End Property

  <ComponentModel.Browsable(False)> _
  Public Property IdentityID2() As String
    Get
      Return Me.ef_identity_id_2.Value
    End Get
    Set(ByVal value As String)
      Me.ef_identity_id_2.Value = value
    End Set
  End Property

  <ComponentModel.Browsable(False)> _
  Public ReadOnly Property IdentityID2Text() As String
    Get
      Return Me.ef_identity_id_2.Text
    End Get
  End Property

  <ComponentModel.Browsable(False)> _
  Public Property IdentityName() As String
    Get
      Return Me.ef_identity_name.Value
    End Get
    Set(ByVal value As String)
      Me.ef_identity_name.Value = value
    End Set
  End Property

  <ComponentModel.Browsable(False)> _
  Public ReadOnly Property IdentityNameText() As String
    Get
      Return Me.ef_identity_name.Text
    End Get
  End Property

  <ComponentModel.Browsable(False)> _
  Public Property IdentityLastName1() As String
    Get
      Return Me.ef_identity_last_name1.Value
    End Get
    Set(ByVal value As String)
      Me.ef_identity_last_name1.Value = value
    End Set
  End Property

  <ComponentModel.Browsable(False)> _
  Public ReadOnly Property IdentityLastName1Text() As String
    Get
      Return Me.ef_identity_last_name1.Text
    End Get
  End Property

  <ComponentModel.Browsable(False)> _
  Public Property IdentityLastName2() As String
    Get
      Return Me.ef_identity_last_name2.Value
    End Get
    Set(ByVal value As String)
      Me.ef_identity_last_name2.Value = value
    End Set
  End Property

  <ComponentModel.Browsable(False)> _
  Public ReadOnly Property IdentityLastName2Text() As String
    Get
      Return Me.ef_identity_last_name2.Text
    End Get
  End Property

#End Region

#Region "Representative"

  <ComponentModel.Browsable(False)> _
  Public Property RepresentativeID1() As String
    Get
      Return Me.ef_representative_id1.Value
    End Get
    Set(ByVal value As String)
      Me.ef_representative_id1.Value = value
    End Set
  End Property

  <ComponentModel.Browsable(False)> _
  Public ReadOnly Property RepresentativeID1Text() As String
    Get
      Return Me.ef_representative_id1.Text
    End Get
  End Property

  <ComponentModel.Browsable(False)> _
  Public Property RepresentativeID2() As String
    Get
      Return Me.ef_representative_id2.Value
    End Get
    Set(ByVal value As String)
      Me.ef_representative_id2.Value = value
    End Set
  End Property

  <ComponentModel.Browsable(False)> _
  Public ReadOnly Property RepresentativeID2Text() As String
    Get
      Return Me.ef_representative_id2.Text
    End Get
  End Property

  <ComponentModel.Browsable(False)> _
  Public Property RepresentativeName() As String
    Get
      Return Me.ef_representative_name.Value
    End Get
    Set(ByVal value As String)
      Me.ef_representative_name.Value = value
    End Set
  End Property

  <ComponentModel.Browsable(False)> _
  Public ReadOnly Property RepresentativeNameText() As String
    Get
      Return Me.ef_representative_name.Text
    End Get
  End Property

#End Region

#Region "Company"

  <ComponentModel.Browsable(False)> _
  Public Overloads Property CompanyID1() As String
    Get
      Return Me.ef_company_id_1.Value
    End Get
    Set(ByVal value As String)
      Me.ef_company_id_1.Value = value
    End Set
  End Property

  <ComponentModel.Browsable(False)> _
  Public Overloads ReadOnly Property CompanyID1Text() As String
    Get
      Return Me.ef_company_id_1.Text
    End Get
  End Property

  <ComponentModel.Browsable(False)> _
  Public Overloads Property CompanyID2() As String
    Get
      Return Me.ef_company_id_2.Value
    End Get
    Set(ByVal value As String)
      Me.ef_company_id_2.Value = value
    End Set
  End Property

  <ComponentModel.Browsable(False)> _
  Public Overloads ReadOnly Property CompanyID2Text() As String
    Get
      Return Me.ef_company_id_2.Text
    End Get
  End Property

  <ComponentModel.Browsable(False)> _
  Public Overloads Property CompanyName() As String
    Get
      Return Me.ef_company_name.Value
    End Get
    Set(ByVal value As String)
      Me.ef_company_name.Value = value
    End Set
  End Property

  <ComponentModel.Browsable(False)> _
  Public Overloads ReadOnly Property CompanyNameText() As String
    Get
      Return Me.ef_company_name.Text
    End Get
  End Property

#End Region

  <ComponentModel.Browsable(False)> _
  Public Property EvidenceEnabled() As Boolean
    Get
      Return Me.chk_enabled.Checked
    End Get
    Set(ByVal value As Boolean)
      Me.chk_enabled.Checked = value
    End Set
  End Property

  <ComponentModel.Browsable(False)> _
  Public Property EditableMinutes() As Integer
    Get
      Return ConvertInt32(Me.ef_editable_minutes.Value)
    End Get
    Set(ByVal value As Integer)
      Me.ef_editable_minutes.Value = value
    End Set
  End Property

  <ComponentModel.Browsable(False)> _
  Public ReadOnly Property EditableMinutesIsNullOrEmpty() As Boolean
    Get
      Return String.IsNullOrEmpty(Me.ef_editable_minutes.Value)
    End Get
  End Property

  <ComponentModel.Browsable(False)> _
  Public ReadOnly Property EditableMinutesText() As String
    Get
      Return Me.ef_editable_minutes.Text
    End Get
  End Property

  <ComponentModel.Browsable(False)> _
  Public Property PrizeGratherThan() As Decimal
    Get
      Return ConvertDecimal(Me.ef_on_prize_grather_than.Value)
    End Get
    Set(ByVal value As Decimal)
      Me.ef_on_prize_grather_than.Value = value
    End Set
  End Property

  <ComponentModel.Browsable(False)> _
  Public ReadOnly Property PrizeGratherThanIsNullOrEmpty() As Boolean
    Get
      Return String.IsNullOrEmpty(Me.ef_on_prize_grather_than.Value)
    End Get
  End Property

  <ComponentModel.Browsable(False)> _
  Public ReadOnly Property PrizeGratherThanText() As String
    Get
      Return Me.ef_on_prize_grather_than.Text
    End Get
  End Property

  <ComponentModel.Browsable(False)> _
  Public Property RepresentativeSignature() As Image
    Get
      Me.AcceptSignature()
      Return Me.pnl_signature.BackgroundImage
    End Get
    Set(ByVal value As Image)
      Me.pnl_signature.BackgroundImage = value
      Me.pnl_signature.BackgroundImageLayout = Windows.Forms.ImageLayout.Zoom
    End Set
  End Property

  Public WriteOnly Property ReadOnlyCompany() As Boolean
    Set(ByVal value As Boolean)
      Me.ef_company_name.IsReadOnly = value
      Me.ef_company_id_1.IsReadOnly = value
      Me.ef_company_id_2.IsReadOnly = value
    End Set
  End Property

  Public WriteOnly Property ReadOnlyRepresentative() As Boolean
    Set(ByVal value As Boolean)
      Me.ef_representative_name.IsReadOnly = value
      Me.ef_representative_id1.IsReadOnly = value
      Me.ef_representative_id2.IsReadOnly = value
    End Set
  End Property

  Public WriteOnly Property ReadOnlyIdentity() As Boolean
    Set(ByVal value As Boolean)
      Me.ef_identity_name.IsReadOnly = value
      Me.ef_identity_last_name1.IsReadOnly = value
      Me.ef_identity_last_name2.IsReadOnly = value
      Me.ef_identity_id_1.IsReadOnly = value
      Me.ef_identity_id_2.IsReadOnly = value
    End Set
  End Property

  <ComponentModel.Browsable(False)> _
  Public Property GeneratedWitholdingDocument() As Integer 'LTC 22-JUN-2016
    Get
      Return Me.uc_cmb_evidence_type.Value
    End Get
    Set(ByVal value As Integer)
      Me.uc_cmb_evidence_type.Value = value
    End Set
  End Property

#End Region

#Region "Public"

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

    Call InitControl()

  End Sub

  Public Sub Clear()

  End Sub

  Public Sub SetFocusIdentityID1()
    Me.ef_identity_id_1.Focus()
  End Sub

  Public Sub SetFocusIdentityName()
    Me.ef_identity_name.Focus()
  End Sub

  Public Sub SetFocusIdentityLastName1()
    Me.ef_identity_last_name1.Focus()
  End Sub

  Public Sub SetFocusIdentityLastName2()
    Me.ef_identity_last_name2.Focus()
  End Sub

  Public Sub SetFocusEditableMinutes()
    Me.ef_editable_minutes.Focus()
  End Sub

  Public Sub SetFocusOnPrizeGratherThan()
    Me.ef_on_prize_grather_than.Focus()
  End Sub

  Public Sub SetFocusRepresentativeId1()
    Me.ef_representative_id1.Focus()
  End Sub

  Public Sub SetFocusRepresentativeName()
    Me.ef_representative_name.Focus()
  End Sub

  Public Sub SetFocusCompanyId1()
    Me.ef_company_id_1.Focus()
  End Sub

  Public Sub SetFocusCompanyName()
    Me.ef_company_name.Focus()
  End Sub

  Public Sub LoadRepresentativeData()
    Me.RepresentativeID1 = WSI.Common.Misc.ReadGeneralParams("Witholding.Document", "Representative.ID1").Trim()
    Me.RepresentativeID2 = WSI.Common.Misc.ReadGeneralParams("Witholding.Document", "Representative.ID2").Trim()
    Me.RepresentativeName = WSI.Common.Misc.ReadGeneralParams("Witholding.Document", "Representative.Name").Trim()
  End Sub

  Public Sub LoadCompanyData()
    Me.CompanyID1 = WSI.Common.Misc.ReadGeneralParams("Witholding.Document", "Business.ID1").Trim()
    Me.CompanyID2 = WSI.Common.Misc.ReadGeneralParams("Witholding.Document", "Business.ID2").Trim()
    Me.CompanyName = WSI.Common.Misc.ReadGeneralParams("Witholding.Document", "Business.Name").Trim()
  End Sub

  Public Function ValidateFormat() As Boolean

    If m_mode = Mode.COMPANY Or m_mode = Mode.PARAMETERS_CONFIGURATION Then
      If Not ef_company_id_1.ValidateFormat() Then
        Call ef_company_id_1.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
              mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_company_id_1.Text)
        Return False
      End If
      If Not ef_company_id_2.ValidateFormat() Then
        Call ef_company_id_2.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
              mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_company_id_2.Text)
        Return False
      End If
      If Not ef_company_name.ValidateFormat() Then
        Call ef_company_name.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
              mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_company_name.Text)
        Return False
      End If
    End If

    If m_mode = Mode.REPRESENTATIVE Or m_mode = Mode.PARAMETERS_CONFIGURATION Then
      If Not ef_representative_id1.ValidateFormat() Then
        Call ef_representative_id1.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
              mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_representative_id1.Text)
        Return False
      End If
      If Not ef_representative_id2.ValidateFormat() Then
        Call ef_representative_id2.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
              mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_representative_id2.Text)
        Return False
      End If
      If Not ef_representative_name.ValidateFormat() Then
        Call ef_representative_name.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
              mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_representative_name.Text)
        Return False
      End If
    End If

    If m_mode = Mode.INDETITY_DATA Or m_mode = Mode.PARAMETERS_CONFIGURATION Then
      If Not ef_identity_id_1.ValidateFormat() Then
        Call ef_identity_id_1.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
              mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_identity_id_1.Text)
        Return False
      End If
      If Not ef_identity_id_2.ValidateFormat() Then
        Call ef_identity_id_2.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
              mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_identity_id_2.Text)
        Return False
      End If
      If Not ef_identity_last_name1.ValidateFormat() Then
        Call ef_identity_last_name1.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
              mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_identity_last_name1.Text)
        Return False
      End If
      If Not ef_identity_last_name2.ValidateFormat() Then
        Call ef_identity_last_name2.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
              mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_identity_last_name2.Text)
        Return False
      End If
      If Not ef_identity_name.ValidateFormat() Then
        Call ef_identity_name.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
              mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_identity_name.Text)
        Return False
      End If
    End If

    Return True

  End Function

  Public Sub Init()

    Select Case Me.m_mode
      Case Mode.COMPANY
        Call LoadCompanyData()
      Case Mode.REPRESENTATIVE
        Call LoadRepresentativeData()
      Case Mode.PARAMETERS_CONFIGURATION
        Call LoadCompanyData()
        Call LoadRepresentativeData()
    End Select

  End Sub

#End Region

#Region "Private"

  Private Sub InitControl()

    AddHandler Me.flp_main.Resize, AddressOf flp_main_Resize
    Call ShowMode()

  End Sub

  Private Sub ShowMode()

    Select Case m_mode
      Case Mode.INDETITY_DATA
        Me.flp_main.Visible = True
        Me.pnl_parameters_withholding.Visible = False
        Me.gp_identity.Visible = True
        Me.gp_representative.Visible = False
        Me.gp_company.Visible = False

        Me.AutoSize = False
        Me.flp_main.AutoSize = False
        Me.Height = Me.gp_identity.Height

        Call InitControlsIdentityData()

      Case Mode.COMPANY
        Me.flp_main.Visible = True
        Me.pnl_parameters_withholding.Visible = False
        Me.gp_identity.Visible = False
        Me.gp_representative.Visible = False
        Me.gp_company.Visible = True

        Me.AutoSize = False
        Me.flp_main.AutoSize = False
        Me.Height = Me.gp_company.Height

        Call InitControlsCompanyData()

      Case Mode.REPRESENTATIVE
        Me.flp_main.Visible = True
        Me.pnl_parameters_withholding.Visible = False
        Me.gp_identity.Visible = False
        Me.gp_representative.Visible = True
        Me.pnl_sign.Visible = False
        Me.gp_company.Visible = False

        Me.AutoSize = False
        Me.flp_main.AutoSize = False

        Call InitControlsRepresentativeData()

      Case Mode.PARAMETERS_CONFIGURATION
        Me.flp_main.Visible = True
        Me.pnl_parameters_withholding.Visible = True
        Me.gp_identity.Visible = False
        Me.gp_representative.Visible = True
        Me.gp_company.Visible = True
        Me.pnl_sign.Visible = True

        Me.AutoSize = True
        Me.flp_main.AutoSize = True
        Me.gp_identity.Width = Me.gp_representative.Width

        Call InitControlsConfiguration()

      Case Else
        Me.flp_main.Visible = False
    End Select

  End Sub

  Private Sub InitControlsConfiguration()
    Dim _rfc As String
    Dim _curp As String

    _rfc = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC)
    _curp = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP)

    Me.btn_select_img.Size = New Size(90, 35)
    'Me.btn_select_img.Location = New Size(460, 127)

    Me.btn_scale_up.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(660)
    Me.btn_scale_down.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(661)
    Me.btn_scale_up.Enabled = False
    Me.btn_scale_down.Enabled = False

    Me.chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(655)
    Me.ef_editable_minutes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(656)
    Me.ef_editable_minutes.IsReadOnly = False
    Me.ef_editable_minutes.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 0)
    Me.ef_on_prize_grather_than.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(657)
    Me.ef_on_prize_grather_than.IsReadOnly = False
    Me.ef_on_prize_grather_than.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    Me.lbl_signature.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(658)
    Me.btn_select_img.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(659)

    'Representative data fields
    Me.gp_representative.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(581)
    'Representative Name
    Me.ef_representative_name.Text = GLB_NLS_GUI_STATISTICS.GetString(210)
    Me.ef_representative_name.IsReadOnly = False
    Me.ef_representative_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, MAX_LAST_NAME_LEN)
    'Representative RFC
    Me.ef_representative_id1.Text = _rfc
    Me.ef_representative_id1.IsReadOnly = False
    Me.ef_representative_id1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, WSI.Common.ValidateFormat.MAX_LENGHT_RFC)
    'Representative CURP
    Me.ef_representative_id2.Text = _curp
    Me.ef_representative_id2.IsReadOnly = False
    Me.ef_representative_id2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, MAX_CURP_LEN)

    ''Holder data fields
    Me.gp_identity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(201)
    'Name
    Me.ef_identity_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1186)
    Me.ef_identity_name.IsReadOnly = False
    Me.ef_identity_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, MAX_NAME)
    'RFC
    Me.ef_identity_id_1.Text = _rfc
    Me.ef_identity_id_1.IsReadOnly = False
    Me.ef_identity_id_1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, WSI.Common.ValidateFormat.MAX_LENGHT_RFC)
    'CURP
    Me.ef_identity_id_2.Text = _curp
    Me.ef_identity_id_2.IsReadOnly = False
    Me.ef_identity_id_2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, MAX_CURP_LEN)
    ' Last name 1
    Me.ef_identity_last_name1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1188)
    Me.ef_identity_last_name1.IsReadOnly = True
    Me.ef_identity_last_name1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, MAX_NAME)
    ' Last name 2
    Me.ef_identity_last_name2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1189)
    Me.ef_identity_last_name2.IsReadOnly = True
    Me.ef_identity_last_name2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, MAX_NAME)

    'Company
    Me.gp_company.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(580)
    'Company Name
    Me.ef_company_name.Text = GLB_NLS_GUI_STATISTICS.GetString(210)
    Me.ef_company_name.IsReadOnly = False
    Me.ef_company_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, MAX_LAST_NAME_LEN)
    'Company RFC
    Me.ef_company_id_1.Text = _rfc
    Me.ef_company_id_1.IsReadOnly = False
    Me.ef_company_id_1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, WSI.Common.ValidateFormat.MAX_LENGHT_RFC)
    'Company CURP
    Me.ef_company_id_2.Text = _curp
    Me.ef_company_id_2.IsReadOnly = False
    Me.ef_company_id_2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, MAX_CURP_LEN)

    'LTC 22-JUN-2016
    'Generar constancia de retención externa
    Me.lbl_generated.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6735)

    'Check chk_enabled
    Me.uc_cmb_evidence_type.Enabled = Me.chk_enabled.Checked

    Call FillEvidenceTypeCombo()

  End Sub

  Private Sub InitControlsIdentityData()

    Me.gp_identity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(201)
    'Identity Name
    Me.ef_identity_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1186)
    Me.ef_identity_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, MAX_NAME)
    Me.ef_identity_name.IsReadOnly = True
    'Identity last Name 1
    Me.ef_identity_last_name1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1188)
    Me.ef_identity_last_name1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, MAX_NAME)
    Me.ef_identity_last_name1.IsReadOnly = True
    'Identity last Name 2
    Me.ef_identity_last_name2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1189)
    Me.ef_identity_last_name2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, MAX_NAME)
    Me.ef_identity_last_name2.IsReadOnly = True
    'Identity RFC
    Me.ef_identity_id_1.Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC)
    Me.ef_identity_id_1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, WSI.Common.ValidateFormat.MAX_LENGHT_RFC)
    Me.ef_identity_id_1.IsReadOnly = True
    'Identity CURP
    Me.ef_identity_id_2.Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP)
    Me.ef_identity_id_2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, MAX_CURP_LEN)
    Me.ef_identity_id_2.IsReadOnly = True

  End Sub

  Private Sub InitControlsRepresentativeData()

    'Representative data fields
    Me.gp_representative.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(581)
    'Representative Name
    Me.ef_representative_name.Text = GLB_NLS_GUI_STATISTICS.GetString(210)
    Me.ef_representative_name.IsReadOnly = True
    Me.ef_representative_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE, MAX_LAST_NAME_LEN)
    Me.ef_representative_name.Top -= 15
    'Representative RFC
    Me.ef_representative_id1.Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC)
    Me.ef_representative_id1.IsReadOnly = True
    Me.ef_representative_id1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, WSI.Common.ValidateFormat.MAX_LENGHT_RFC)
    Me.ef_representative_id1.Top -= 15
    'Representative CURP
    Me.ef_representative_id2.Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP)
    Me.ef_representative_id2.IsReadOnly = True
    Me.ef_representative_id2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, MAX_CURP_LEN)
    Me.ef_representative_id2.Top -= 15

  End Sub

  Private Sub InitControlsCompanyData()

    'Company
    Me.gp_company.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(580)
    'Company Name
    Me.ef_company_name.Text = GLB_NLS_GUI_STATISTICS.GetString(210)
    Me.ef_company_name.IsReadOnly = True
    Me.ef_company_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, MAX_LAST_NAME_LEN)
    'Company RFC
    Me.ef_company_id_1.Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.RFC)
    Me.ef_company_id_1.IsReadOnly = True
    Me.ef_company_id_1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, WSI.Common.ValidateFormat.MAX_LENGHT_RFC)
    'Company CURP
    Me.ef_company_id_2.Text = IdentificationTypes.DocIdTypeString(ACCOUNT_HOLDER_ID_TYPE.CURP)
    Me.ef_company_id_2.IsReadOnly = True
    Me.ef_company_id_2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED, MAX_CURP_LEN)

  End Sub

  Private Function ConvertInt32(ByVal obj As Object) As Int32
    Dim _result As Int32

    If Int32.TryParse(obj.ToString, _result) Then
      Return _result
    Else
      Return 0
    End If

  End Function

  Private Function ConvertDecimal(ByVal obj As Object) As Decimal
    Dim _result As Decimal

    If Decimal.TryParse(obj.ToString, _result) Then
      Return _result
    Else
      Return 0
    End If

  End Function

  Private Sub BeginningCropScale()

    m_x = 0
    m_y = 0
    m_dx = 0
    m_dy = 0
    m_scale = 1
    m_with = m_img_sign.Width
    m_height = m_img_sign.Height

    m_old_sign = pnl_signature.BackgroundImage
    pnl_signature.BackgroundImage = Nothing
    pnl_signature.Cursor = Windows.Forms.Cursors.NoMove2D
    btn_scale_up.Enabled = True
    btn_scale_down.Enabled = True
    m_cropping_area = True

    AddHandler pnl_signature.Paint, AddressOf pnl_signature_Paint

    AddHandler pnl_signature.MouseDown, AddressOf pnl_signature_MouseDown
    AddHandler pnl_signature.MouseMove, AddressOf pnl_signature_MouseMove
    AddHandler pnl_signature.MouseUp, AddressOf pnl_signature_MouseUp

  End Sub

  Private Sub EndingCropScale()

    m_cropping_area = False

    btn_scale_up.Enabled = False
    btn_scale_down.Enabled = False

    pnl_signature.Cursor = Windows.Forms.Cursors.Default
    btn_select_img.Enabled = True

    RemoveHandler pnl_signature.Paint, AddressOf pnl_signature_Paint

    RemoveHandler pnl_signature.MouseDown, AddressOf pnl_signature_MouseDown
    RemoveHandler pnl_signature.MouseMove, AddressOf pnl_signature_MouseMove
    RemoveHandler pnl_signature.MouseUp, AddressOf pnl_signature_MouseUp

  End Sub

  Private Sub AcceptSignature()

    Dim _target_img As Bitmap
    Dim _target_gr As Graphics
    Dim _mm As New IO.MemoryStream
    Dim _ratio As Double

    If IsNothing(m_img_sign) Then Return

    If m_cropping_area Then

      Call EndingCropScale()

    ElseIf Not (pnl_signature.Width < m_img_sign.Width Or pnl_signature.Height < m_img_sign.Height) Then

      m_x = (pnl_signature.Width - m_img_sign.Width) / 2
      m_y = (pnl_signature.Height - m_img_sign.Height) / 2
      m_scale = 1
      m_with = m_img_sign.Width
      m_height = m_img_sign.Height

    End If

    _ratio = 2

    _target_img = New Bitmap(pnl_signature.Width * _ratio, pnl_signature.Height * _ratio, m_img_sign.PixelFormat)
    _target_img.SetResolution(90, 90)
    _target_gr = Graphics.FromImage(_target_img)

    _target_gr.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
    _target_gr.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
    _target_gr.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality
    _target_gr.CompositingQuality = Drawing2D.CompositingQuality.HighQuality

    _target_gr.Clear(Color.White)

    _target_gr.DrawImage(m_img_sign, _
                        Convert.ToInt32(m_x * _ratio), Convert.ToInt32(m_y * _ratio), _
                        Convert.ToInt32(m_with * m_scale * _ratio), Convert.ToInt32(m_height * m_scale * _ratio))

    Dim _info As ImageCodecInfo() = ImageCodecInfo.GetImageEncoders()
    Dim _encoderParameters As Imaging.EncoderParameters
    Dim _encoder As ImageCodecInfo

    _encoderParameters = New Imaging.EncoderParameters(1)
    _encoderParameters.Param(0) = New Imaging.EncoderParameter(Encoder.Quality, 100L)

    For Each _encoder In ImageCodecInfo.GetImageEncoders()

      If _encoder.FormatID = ImageFormat.Png.Guid Then
        Exit For
      End If

    Next

    _target_img.Save(_mm, _info(4), _encoderParameters)
    _target_img.Dispose()
    _target_gr.Dispose()

    Me.pnl_signature.BackgroundImage = Image.FromStream(_mm)
    Me.pnl_signature.BackgroundImageLayout = Windows.Forms.ImageLayout.Zoom
    Me.pnl_signature.Refresh()

  End Sub

  Public Shared Function ResizeToCanvas(ByVal Img As Image, ByVal Width As Integer, ByVal Height As Integer) As Image
    Dim CropRectangle As Rectangle = EnsureAspectRatio(Img, Width, Height)
    Dim CropedImage As Image = New Bitmap(Width, Height)

    Using G As Graphics = Graphics.FromImage(CropedImage)
      G.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
      G.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
      G.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality
      G.DrawImage(Img, New Rectangle(0, 0, Width, Height), CropRectangle, GraphicsUnit.Pixel)
    End Using

    Return CropedImage
  End Function

  Private Shared Function EnsureAspectRatio(ByVal Image As Image, ByVal Width As Integer, ByVal Height As Integer) As Rectangle
    Dim AspectRatio As Single = Width / CSng(Height)
    Dim CalculatedWidth As Single = Image.Width, CalculatedHeight As Single = Image.Height

    If Image.Width >= Image.Height Then
      If Width > Height Then
        CalculatedHeight = Image.Width / AspectRatio
        If CalculatedHeight > Image.Height Then
          CalculatedHeight = Image.Height
          CalculatedWidth = CalculatedHeight * AspectRatio
        End If
      Else
        CalculatedWidth = Image.Height * AspectRatio
        If CalculatedWidth > Image.Width Then
          CalculatedWidth = Image.Width
          CalculatedHeight = CalculatedWidth / AspectRatio
        End If
      End If
    Else
      If Width < Height Then
        CalculatedHeight = Image.Width / AspectRatio
        If CalculatedHeight > Image.Height Then
          CalculatedHeight = Image.Height
          CalculatedWidth = CalculatedHeight * AspectRatio
        End If
      Else
        CalculatedWidth = Image.Height * AspectRatio
        If CalculatedWidth > Image.Width Then
          CalculatedWidth = Image.Width
          CalculatedHeight = CalculatedWidth / AspectRatio
        End If
      End If
    End If
    Return Rectangle.Ceiling(New RectangleF((Image.Width - CalculatedWidth) / 2, 0, CalculatedWidth, CalculatedHeight))
  End Function

  Private Sub CancelSignature()

    If m_cropping_area Then

      Call EndingCropScale()

      Me.pnl_signature.BackgroundImage = m_old_sign
      Call pnl_signature.Refresh()

    End If

  End Sub

  'LTC 22-JUN-2016
  'LTC 26-OCT-2016
  Private Sub FillEvidenceTypeCombo()

    Me.uc_cmb_evidence_type.Add(0, GLB_NLS_GUI_PLAYER_TRACKING.Id(7689))

    For Each item As WSI.Common.CashierDocs.EVIDENCE_TYPE In WSI.Common.CashierDocs.GetListEvidenceType()
      Select Case (item)
        Case CashierDocs.EVIDENCE_TYPE.ESTATAL
          Me.uc_cmb_evidence_type.Add(1, GLB_NLS_GUI_PLAYER_TRACKING.Id(7690))
        Case CashierDocs.EVIDENCE_TYPE.ESTATAL_YUCATAN
          Me.uc_cmb_evidence_type.Add(2, GLB_NLS_GUI_PLAYER_TRACKING.Id(7691))
        Case CashierDocs.EVIDENCE_TYPE.ESTATAL_CAMPECHE
          Me.uc_cmb_evidence_type.Add(3, GLB_NLS_GUI_PLAYER_TRACKING.Id(7692))
        Case CashierDocs.EVIDENCE_TYPE.FEDERAL_CODERE
          Me.uc_cmb_evidence_type.Add(4, GLB_NLS_GUI_PLAYER_TRACKING.Id(7693)) ' LTC 13-JUL-2016
      End Select
    Next
  End Sub

#End Region

#Region "Events"

  Private Sub flp_main_Resize(ByVal sender As Object, ByVal e As EventArgs)

    Select Case Me.m_mode
      Case Mode.COMPANY
        Me.gp_company.Width = Me.flp_main.Width
      Case Mode.INDETITY_DATA
        Me.gp_identity.Width = Me.flp_main.Width
      Case Mode.REPRESENTATIVE
        Me.gp_representative.Width = Me.flp_main.Width
        Me.gp_representative.Height = Me.gp_company.Height
    End Select

  End Sub

  Private Sub pnl_signature_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pnl_signature.MouseDown
    m_moving_btn = True
    m_dx = e.X - m_x
    m_dy = e.Y - m_y
  End Sub

  Private Sub pnl_signature_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pnl_signature.MouseMove
    If m_moving_btn Then
      m_x = e.X - m_dx
      m_y = e.Y - m_dy
      pnl_signature.Refresh()
    End If
  End Sub

  Private Sub pnl_signature_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pnl_signature.MouseUp
    m_moving_btn = False
    m_x = e.X - m_dx
    m_y = e.Y - m_dy
  End Sub

  Private Sub pnl_signature_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles pnl_signature.MouseEnter
    m_enter_panel_sign = True
  End Sub

  Private Sub pnl_signature_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles pnl_signature.MouseLeave
    m_enter_panel_sign = False
  End Sub

  Private Sub uc_identity_data_MouseWheel(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseWheel
    If m_cropping_area AndAlso m_enter_panel_sign Then
      If e.Delta < 0 Then
        m_scale -= SCALE_CONSTANT1
        pnl_signature.Refresh()
      Else
        m_scale += SCALE_CONSTANT1
        pnl_signature.Refresh()
      End If
    End If
  End Sub

  Private Sub pnl_signature_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs)

    e.Graphics.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
    e.Graphics.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
    e.Graphics.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality
    e.Graphics.CompositingQuality = Drawing2D.CompositingQuality.HighQuality

    e.Graphics.DrawImage(m_img_sign, m_x, m_y, Convert.ToInt32(m_with * m_scale), Convert.ToInt32(m_height * m_scale))

  End Sub

  Private Sub btn_select_img_ClickEvent() Handles btn_select_img.ClickEvent

    Dim _filename As String
    Dim _open_file As System.Windows.Forms.OpenFileDialog

    _open_file = New System.Windows.Forms.OpenFileDialog

    If Not String.IsNullOrEmpty(m_current_directory) Then
      _open_file.InitialDirectory = m_current_directory
    End If

    _open_file.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(662)
    _open_file.Filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(663)
    If _open_file.ShowDialog() <> Windows.Forms.DialogResult.OK Then
      Return
    End If

    _filename = _open_file.FileName
    m_current_directory = System.IO.Path.GetDirectoryName(_filename)

    Using _img_tmp1 As Image = System.Drawing.Image.FromFile(_filename)
      m_img_sign = New Bitmap(_img_tmp1)
    End Using

    If m_img_sign.Width > pnl_signature.Width Or m_img_sign.Height > pnl_signature.Height Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(664))
      Call BeginningCropScale()
    Else
      If Me.m_cropping_area Then
        Call EndingCropScale()
      End If
      Me.pnl_signature.BackgroundImage = m_img_sign
      Me.pnl_signature.BackgroundImageLayout = Windows.Forms.ImageLayout.Zoom
    End If
    pnl_signature.Refresh()

  End Sub

  Private Sub btn_scale_up_ClickEvent() Handles btn_scale_up.ClickEvent
    m_scale += SCALE_CONSTANT2
    pnl_signature.Refresh()
  End Sub

  Private Sub btn_scale_down_ClickEvent() Handles btn_scale_down.ClickEvent
    m_scale -= SCALE_CONSTANT2
    pnl_signature.Refresh()
  End Sub

  Private Sub chk_enabled_CheckedChanged(sender As Object, e As EventArgs) Handles chk_enabled.CheckedChanged
    'LTC 22-JUN-2016
    Me.uc_cmb_evidence_type.Enabled = Me.chk_enabled.Checked
    If Not Me.uc_cmb_evidence_type.Enabled Then
      Me.uc_cmb_evidence_type.Value = CashierDocs.EVIDENCE_TYPE.NONE
    End If
  End Sub

#End Region


End Class

