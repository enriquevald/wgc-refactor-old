'-------------------------------------------------------------------
' Copyright � 2002-2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_wcp_cashier_config.vb  
'
' DESCRIPTION:   User control for the wcp cashier configuration
'
' AUTHOR:        Daniel Rodr�guez Gil
'
' CREATION DATE: 31-MAR-2009
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 31-MAR-2009  DRG    Initial version
' 28-OCT-2013  QMP    Always show "Account Customization Footer" tab
' 25-FEB-2013  JFC    Fixed bug WIG-420: MaxAllowedAccountBalance no longer used.
' 30-JUN-2014  AMF    Personal Card Replacement
' 17-JUL-2014  AMF    Fixed bug WIG-1093: Disable if price is $0
' 16-JUN-2015  ANM    Fixed Bug WIG-2441: Avoid unhandle exception when changing uc_entry_personal_card_replacement_price
' 01-JUL-2015  FOS    Created new parameters to amount input company b
' 27-APR-2016  FAV    Fixed Bug 8440: Enable 'Charge from (replacements)' when 'Personalized Card Replacement' is greater than 0
'-------------------------------------------------------------------
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports System.Data.SqlClient
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common

Public Class uc_wcp_cashier_config


  ' Grid Columns
  Private Const GRID_COLUMNS As Integer = 4

  Private Const GRID_HEADER_ROWS As Integer = 1
  Private Const GRID_COLUMN_ID As Integer = 0
  Private Const GRID_COLUMN_NAME As Integer = 1
  Private Const GRID_COLUMN_EXTERNAL_ID As Integer = 2
  Private Const GRID_COLUMN_MACHINEID As Integer = 3

  Private Const WCP_CASHIER_MODE_BY_TERMINAL As Integer = 1
  Private Const WCP_CASHIER_MODE_BY_USER As Integer = 2


  Private Const WCP_CASHIER_CARD_NOT_REFUNDABLE As Integer = 0
  Private Const WCP_CASHIER_CARD_REFUNDABLE As Integer = 1
  Private Const WCP_CASHIER_CARD_SINGLE_PAYMENT As Integer = 2

  Private Const WCP_CARD_PRINT_NO_BOLD As Integer = 0
  Private Const WCP_CARD_PRINT_BOLD As Integer = 1

  Private Const WCP_CASH_OUT_SPLIT_PRIZE_OFF As Integer = 0
  Private Const WCP_CASH_OUT_SPLIT_PRIZE_ON As Integer = 1

  Private Const TIME_PERIOD_LIMIT = 2500


  Public Overloads Property CardPrice() As String
    Get
      Return uc_entry_card_price.Value
    End Get

    Set(ByVal Value As String)
      uc_entry_card_price.Value = Value
    End Set

  End Property

  Public Overloads Property PersonalCardPrice() As String
    Get
      Return uc_entry_personal_card_price.Value
    End Get

    Set(ByVal Value As String)
      uc_entry_personal_card_price.Value = Value
    End Set

  End Property

  Public Overloads Property PersonalCardReplacementPriceInPoints() As String
    Get
      Return uc_entry_personal_card_price_in_points.Value
    End Get

    Set(ByVal Value As String)
      uc_entry_personal_card_price_in_points.Value = Value
    End Set

  End Property

  Public Overloads Property PersonalCardReplacementChargeAfterTimes() As String
    Get
      Return uc_entry_personal_card_charge_after_times.Value
    End Get

    Set(ByVal Value As String)
      uc_entry_personal_card_charge_after_times.Value = Value
    End Set

  End Property

  Public Overloads Property PersonalCardReplacementPrice() As String
    Get
      Return uc_entry_personal_card_replacement_price.Value
    End Get

    Set(ByVal Value As String)
      uc_entry_personal_card_replacement_price.Value = Value

      ' Update status for uc_entry_personal_card_charge_after_times
      uc_entry_personal_card_replacement_price_EntryFieldValueChanged()
    End Set

  End Property

  Public Overloads Property CardRefundable() As String
    Get
      Dim temp_result As String
      temp_result = ""
      Select Case Uc_combo_card_refundable.Value
        Case WCP_CASHIER_CARD_REFUNDABLE
          temp_result = "1"
        Case WCP_CASHIER_CARD_NOT_REFUNDABLE
          temp_result = "0"
        Case WCP_CASHIER_CARD_SINGLE_PAYMENT
          temp_result = "2"
      End Select
      Return temp_result
    End Get

    Set(ByVal Value As String)

      Select Case Value
        Case WCP_CASHIER_CARD_REFUNDABLE
          Uc_combo_card_refundable.Value = WCP_CASHIER_CARD_REFUNDABLE
        Case WCP_CASHIER_CARD_NOT_REFUNDABLE
          Uc_combo_card_refundable.Value = WCP_CASHIER_CARD_NOT_REFUNDABLE
        Case WCP_CASHIER_CARD_SINGLE_PAYMENT
          Uc_combo_card_refundable.Value = WCP_CASHIER_CARD_SINGLE_PAYMENT
      End Select
    End Set

  End Property

  Public Overloads Property CashierMode() As String
    Get
      Dim temp_result As String
      temp_result = ""
      Select Case uc_cmb_cashier_mode.Value
        Case WCP_CASHIER_MODE_BY_TERMINAL
          temp_result = GLB_NLS_GUI_CONFIGURATION.GetString(234)
        Case WCP_CASHIER_MODE_BY_USER
          temp_result = GLB_NLS_GUI_CONFIGURATION.GetString(235)
      End Select
      Return temp_result
    End Get

    Set(ByVal Value As String)
      Select Case Value
        Case GLB_NLS_GUI_CONFIGURATION.GetString(234)
          uc_cmb_cashier_mode.Value = WCP_CASHIER_MODE_BY_TERMINAL
        Case GLB_NLS_GUI_CONFIGURATION.GetString(235)
          uc_cmb_cashier_mode.Value = WCP_CASHIER_MODE_BY_USER
      End Select
    End Set

  End Property

  Public Overloads Property Language() As String
    Get
      Dim temp_result As String
      temp_result = ""
      Select Case Uc_cmb_language.Value
        Case 1
          temp_result = GLB_NLS_GUI_CONFIGURATION.GetString(233)

        Case 2
          temp_result = GLB_NLS_GUI_CONFIGURATION.GetString(245)

      End Select
      Return temp_result
    End Get

    Set(ByVal Value As String)
      Select Case Value
        Case GLB_NLS_GUI_CONFIGURATION.GetString(233)
          Uc_cmb_language.Value = 1

        Case GLB_NLS_GUI_CONFIGURATION.GetString(245)
          Uc_cmb_language.Value = 2

      End Select
    End Set

  End Property

  Public Overloads Property MinTimeToCloseSession() As String
    Get
      Return uc_entry_min_time_close_session.Value
    End Get

    Set(ByVal Value As String)
      uc_entry_min_time_close_session.Value = Value
    End Set

  End Property

  Public Overloads Property VoucherHeader() As String
    Get
      Return txt_voucher_header.Text

    End Get

    Set(ByVal Value As String)
      txt_voucher_header.Text = Value
    End Set

  End Property

  Public Overloads Property VoucherFooter() As String
    Get
      Return txt_voucher_footer.Text
    End Get

    Set(ByVal Value As String)
      txt_voucher_footer.Text = Value
    End Set

  End Property

  Public Overloads Property VoucherFooterAccountsCustomization() As String
    Get
      Return txt_voucher_accounts_customization_footer.Text
    End Get

    Set(ByVal Value As String)
      txt_voucher_accounts_customization_footer.Text = Value
    End Set

  End Property


  Public Overloads Property CardPrintTopX() As String
    Get
      Return uc_entry_topx.Value
    End Get

    Set(ByVal Value As String)
      uc_entry_topx.Value = Value
    End Set

  End Property

  Public Overloads Property CardPrintTopY() As String
    Get
      Return uc_entry_topy.Value
    End Get

    Set(ByVal Value As String)
      uc_entry_topy.Value = Value
    End Set

  End Property

  Public Overloads Property CardPrintBottomX() As String
    Get
      Return uc_entry_bottomx.Value
    End Get

    Set(ByVal Value As String)
      uc_entry_bottomx.Value = Value
    End Set

  End Property

  Public Overloads Property CardPrintBottomY() As String
    Get
      Return uc_entry_bottomy.Value
    End Get

    Set(ByVal Value As String)
      uc_entry_bottomy.Value = Value
    End Set

  End Property

  Public Overloads Property CardPrintFontSize() As String
    Get
      Return uc_entry_font_size.Value
    End Get

    Set(ByVal Value As String)
      uc_entry_font_size.Value = Value
    End Set

  End Property

  Public Overloads Property CardPrinterName() As String
    Get
      Return uc_entry_printer_name.Value
    End Get

    Set(ByVal Value As String)
      uc_entry_printer_name.Value = Value
    End Set

  End Property

  Public Overloads Property CardPrintBold() As String
    Get
      Dim temp_result As String
      Dim chk_bold_tmp As Integer
      If chk_bold.Checked Then
        chk_bold_tmp = 1
      Else
        chk_bold_tmp = 0
      End If
      temp_result = ""

      Select Case chk_bold_tmp
        Case WCP_CARD_PRINT_NO_BOLD
          temp_result = "0"
        Case WCP_CARD_PRINT_BOLD
          temp_result = "1"
      End Select
      Return temp_result
    End Get

    Set(ByVal Value As String)
      Select Case Value
        Case "1"
          chk_bold.Checked = True
        Case "0"
          chk_bold.Checked = False
      End Select
    End Set
  End Property

  Public Overloads Property AllowAnon() As String
    Get
      Dim temp_result As String
      If chk_allow_anon.Checked Then
        temp_result = "1"
      Else
        temp_result = "0"
      End If
      Return temp_result
    End Get

    Set(ByVal Value As String)
      Select Case Value
        Case "1"
          chk_allow_anon.Checked = True
        Case "0"
          chk_allow_anon.Checked = False
      End Select
    End Set
  End Property

  Public Overloads Property HandPayTimePeriod() As String
    Get
      Return uc_entry_handpay_time_period.Value
    End Get

    Set(ByVal Value As String)
      uc_entry_handpay_time_period.Value = Value
    End Set

  End Property

  Public Overloads Property HandPayCancelTime() As String
    Get
      Return uc_entry_handpay_cancel_time.Value
    End Get

    Set(ByVal Value As String)
      uc_entry_handpay_cancel_time.Value = Value
    End Set

  End Property

  Public Overloads Property CardPlayerConcept() As String
    Get
      Return ef_card_player_concept.Value
    End Get

    Set(ByVal Value As String)
      ef_card_player_concept.Value = Value
    End Set

  End Property

  Public Overloads Property CardPlayerAmountToCompanyB() As Boolean
    Get
      Return chk_card_player_amount_to_company_b.Checked
    End Get

    Set(ByVal Value As Boolean)
      chk_card_player_amount_to_company_b.Checked = Value
    End Set

  End Property


  Private Sub GUI_StyleView()
  End Sub ' GUI_StyleView


  Private Sub InitControls()

    gb_wcp_params.Text = GLB_NLS_GUI_CONFIGURATION.GetString(236)       ' 236 "Par�metros"
    gb_wcp_card.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5767)         ' 5767 "Tarjeta de jugador"
    gb_voucher_config.Text = GLB_NLS_GUI_CONFIGURATION.GetString(237)   ' 237 "Voucher"
    gp_handpays.Text = GLB_NLS_GUI_CONFIGURATION.GetString(261)         ' 261 "Pago Manual"

    uc_entry_card_price.Text = GLB_NLS_GUI_CONFIGURATION.GetString(224)                          ' 224 "Card price"
    uc_entry_personal_card_price.Text = GLB_NLS_GUI_CONFIGURATION.GetString(246)                 ' 246 "Personal Card Price"
    uc_entry_personal_card_replacement_price.Text = GLB_NLS_GUI_CONFIGURATION.GetString(247)     ' 247 "Personal Card Replacement Price"
    uc_entry_personal_card_price_in_points.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5055)    ' 5055 "Sustituci�n de tarjeta personalizada (puntos)"
    uc_entry_personal_card_charge_after_times.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5056) ' 5056 "Cobrar a partir de (sustituciones)"
    Uc_combo_card_refundable.Text = GLB_NLS_GUI_CONFIGURATION.GetString(248)                     ' 248 "" TODO
    uc_cmb_cashier_mode.Text = GLB_NLS_GUI_CONFIGURATION.GetString(225)                          ' 225 "Modo de caja"
    Uc_cmb_language.Text = GLB_NLS_GUI_CONFIGURATION.GetString(226)                              ' 226 "Idioma"
    uc_entry_min_time_close_session.Text = GLB_NLS_GUI_CONFIGURATION.GetString(284)              ' 284 "Tiempo M�nimo para Cerrar Sesi�n"
    TabPage1.Text = GLB_NLS_GUI_CONFIGURATION.GetString(228)                                     ' 228 "Cabecera"
    TabPage2.Text = GLB_NLS_GUI_CONFIGURATION.GetString(229)                                     ' 229 "Pie"
    TabPage3.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2192)                                  ' 2192 "Pie personalizaci�n cuenta"

    uc_entry_handpay_time_period.Text = GLB_NLS_GUI_CONFIGURATION.GetString(263)              ' 263 "Periodo de Tiempo"
    uc_entry_handpay_cancel_time.Text = GLB_NLS_GUI_CONFIGURATION.GetString(262)              ' 230 "Imprimir"

    lbl_entry_min_time_close_session_suffix.Text = GLB_NLS_GUI_CONFIGURATION.GetString(283)   ' 283 "seg."
    lbl_handpay_cancel_time_suffix.Text = GLB_NLS_GUI_CONFIGURATION.GetString(285)            ' 285 "min."
    lbl_handpay_time_period_suffix.Text = GLB_NLS_GUI_CONFIGURATION.GetString(285)            ' 285 "min."

    uc_entry_card_price.SetFilter(FORMAT_MONEY, 5, 2)
    uc_entry_personal_card_price.SetFilter(FORMAT_MONEY, 5, 2)
    uc_entry_personal_card_replacement_price.SetFilter(FORMAT_MONEY, 5, 2)
    uc_entry_personal_card_price_in_points.SetFilter(FORMAT_NUMBER, 5, 0)
    uc_entry_personal_card_charge_after_times.SetFilter(FORMAT_NUMBER, 3, 0)

    uc_cmb_cashier_mode.Add(WCP_CASHIER_MODE_BY_TERMINAL, GLB_NLS_GUI_CONFIGURATION.GetString(232))
    uc_cmb_cashier_mode.Add(WCP_CASHIER_MODE_BY_USER, GLB_NLS_GUI_CONFIGURATION.GetString(231))

    ' Anonymous deposit type
    Uc_combo_card_refundable.Add(WCP_CASHIER_CARD_NOT_REFUNDABLE, GLB_NLS_GUI_CONFIGURATION.GetString(445))
    Uc_combo_card_refundable.Add(WCP_CASHIER_CARD_REFUNDABLE, GLB_NLS_GUI_CONFIGURATION.GetString(446))
    Uc_combo_card_refundable.Add(WCP_CASHIER_CARD_SINGLE_PAYMENT, GLB_NLS_GUI_CONFIGURATION.GetString(447))

    Uc_cmb_language.Add(1, GLB_NLS_GUI_CONFIGURATION.GetString(233))
    Uc_cmb_language.Add(2, GLB_NLS_GUI_CONFIGURATION.GetString(245))

    uc_entry_min_time_close_session.SetFilter(FORMAT_NUMBER, 6, 0)
    uc_entry_handpay_time_period.SetFilter(FORMAT_NUMBER, 4, 0)
    uc_entry_handpay_cancel_time.SetFilter(FORMAT_NUMBER, 2, 0)

    'txt_voucher_header.SetFilter(FORMAT_TEXT, 200)
    'txt_voucher_footer.SetFilter(FORMAT_TEXT, 200)

    ' Card Printer
    gb_card_printer.Text = GLB_NLS_GUI_CONFIGURATION.GetString(266)
    uc_entry_topx.Text = GLB_NLS_GUI_CONFIGURATION.GetString(267)
    uc_entry_topy.Text = GLB_NLS_GUI_CONFIGURATION.GetString(268)
    uc_entry_bottomx.Text = GLB_NLS_GUI_CONFIGURATION.GetString(269)
    uc_entry_bottomy.Text = GLB_NLS_GUI_CONFIGURATION.GetString(270)
    uc_entry_font_size.Text = GLB_NLS_GUI_CONFIGURATION.GetString(271)
    uc_entry_printer_name.Text = GLB_NLS_GUI_CONFIGURATION.GetString(278)
    chk_bold.Text = GLB_NLS_GUI_CONFIGURATION.GetString(280)
    uc_entry_topx.SetFilter(FORMAT_NUMBER, 3, 0)
    uc_entry_topy.SetFilter(FORMAT_NUMBER, 3, 0)
    uc_entry_bottomx.SetFilter(FORMAT_NUMBER, 3, 0)
    uc_entry_bottomy.SetFilter(FORMAT_NUMBER, 3, 0)
    uc_entry_font_size.SetFilter(FORMAT_NUMBER, 2, 0)
    uc_entry_printer_name.SetFilter(FORMAT_TEXT, 250, 0)

    chk_allow_anon.Text = GLB_NLS_GUI_CONFIGURATION.GetString(483)

    chk_card_player_amount_to_company_b.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6523)
    ef_card_player_concept.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6524)
    ef_card_player_concept.SetFilter(FORMAT_TEXT, 100, 0)

    ' Format entry fields
    GUI_StyleView()
  End Sub

  ''' <summary>
  ''' Sets the focus of a component having the nls
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub SetFocus(ByVal Nls As Integer)
    Select Case Nls
      Case 224
        Me.uc_entry_card_price.Focus()
      Case 225
        Me.uc_cmb_cashier_mode.Focus()
      Case 226
        Me.Uc_cmb_language.Focus()
      Case 284
        Me.uc_entry_min_time_close_session.Focus()
      Case 228
        Me.txt_voucher_header.Focus()
      Case 229
        Me.txt_voucher_footer.Focus()
      Case 246
        Me.uc_entry_personal_card_price.Focus()
      Case 247
        Me.uc_entry_personal_card_replacement_price.Focus()
      Case 248
        Me.Uc_combo_card_refundable.Focus()
      Case 267
        Me.uc_entry_topx.Focus()
      Case 268
        Me.uc_entry_topy.Focus()
      Case 269
        Me.uc_entry_bottomx.Focus()
      Case 270
        Me.uc_entry_bottomy.Focus()
      Case 271
        Me.uc_entry_font_size.Focus()
      Case 278
        Me.uc_entry_printer_name.Focus()
      Case 280
        Me.chk_bold.Focus()
      Case 262
        Me.uc_entry_handpay_cancel_time.Focus()
      Case 263, 1421
        Me.uc_entry_handpay_time_period.Focus()
      Case 483
        Me.chk_allow_anon.Focus()
      Case 5055
        Me.uc_entry_personal_card_price_in_points.Focus()
      Case 5056
        Me.uc_entry_personal_card_charge_after_times.Focus()
    End Select
  End Sub

  ''' <summary>
  ''' Returns the NLS of the missing field, or 0 if ok
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function DataOk() As Integer
    Dim nls As Integer
    Dim _time_period_afeter_parse As Integer

    nls = 0
    If uc_entry_card_price.Value = "" Then
      nls = 224
    End If
    If uc_entry_personal_card_price.Value = "" Then
      nls = 246
    End If
    If uc_entry_personal_card_replacement_price.Value = "" Then
      nls = 247
    End If
    If Uc_combo_card_refundable.Value <> WCP_CASHIER_CARD_NOT_REFUNDABLE And Uc_combo_card_refundable.Value <> WCP_CASHIER_CARD_REFUNDABLE And Uc_combo_card_refundable.Value <> WCP_CASHIER_CARD_SINGLE_PAYMENT Then 'uc_opt_card_refundable_yes.Checked And Not uc_opt_card_refundable_no.Checked Then
      nls = 248
    End If
    If uc_cmb_cashier_mode.Value <> WCP_CASHIER_MODE_BY_TERMINAL And uc_cmb_cashier_mode.Value <> WCP_CASHIER_MODE_BY_USER Then
      nls = 225
    End If
    If Uc_cmb_language.Value <> 1 And Uc_cmb_language.Value <> 2 Then
      nls = 226
    End If

    If uc_entry_min_time_close_session.Value = "" Then
      nls = 284
    End If
    If txt_voucher_header.Text = "" Then
      nls = 228
    End If
    If txt_voucher_footer.Text = "" Then
      nls = 229
    End If
    If uc_entry_topx.Value = "" Then
      nls = 267
    End If
    If uc_entry_topy.Value = "" Then
      nls = 268
    End If
    If uc_entry_bottomx.Value = "" Then
      nls = 269
    End If
    If uc_entry_bottomy.Value = "" Then
      nls = 270
    End If
    If uc_entry_font_size.Value = "" Then
      nls = 271
    End If
    If uc_entry_printer_name.Value = "" Then
      nls = 278
    End If
    If uc_entry_handpay_time_period.Value = "" Then
      nls = 263
    End If
    If uc_entry_handpay_time_period.Value <> "" AndAlso (Not Integer.TryParse(uc_entry_handpay_time_period.Value, _time_period_afeter_parse) OrElse _time_period_afeter_parse > TIME_PERIOD_LIMIT) Then
      nls = 1421
    End If
    If uc_entry_handpay_cancel_time.Value = "" Then
      nls = 262
    End If

    Return nls
  End Function

  Public Function DesiredWidth() As Integer
    Return pnl_global.Width
  End Function

  Public Function DesiredHeight() As Integer
    Return pnl_global.Height
  End Function

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

    Call InitControls()

  End Sub

#Region " Events "
  Private Sub Uc_combo_card_refundable_ValueChangedEvent() Handles Uc_combo_card_refundable.ValueChangedEvent
    Select Case Uc_combo_card_refundable.Value
      Case WCP_CASHIER_CARD_NOT_REFUNDABLE
        uc_entry_card_price.Text = GLB_NLS_GUI_CONFIGURATION.GetString(224)
      Case WCP_CASHIER_CARD_REFUNDABLE
        uc_entry_card_price.Text = GLB_NLS_GUI_CONFIGURATION.GetString(224)
      Case WCP_CASHIER_CARD_SINGLE_PAYMENT
        uc_entry_card_price.Text = GLB_NLS_GUI_CONFIGURATION.GetString(444)
    End Select
  End Sub

  Private Sub uc_entry_personal_card_replacement_price_EntryFieldValueChanged() Handles uc_entry_personal_card_replacement_price.EntryFieldValueChanged
    If uc_entry_personal_card_replacement_price.Value = String.Empty OrElse uc_entry_personal_card_replacement_price.Value = 0.0 Then
      uc_entry_personal_card_charge_after_times.Enabled = False
    Else
      uc_entry_personal_card_charge_after_times.Enabled = True
    End If
  End Sub

#End Region
End Class
