'-------------------------------------------------------------------
' Copyright © 2002-2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_alesis_cashier_config.vb  
'
' DESCRIPTION:   User control for the alesis cashier configuration
'
' AUTHOR:        Daniel Rodríguez Gil
'
' CREATION DATE: 31-MAR-2009
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 31-MAR-2009  DRG    Initial version
'-------------------------------------------------------------------
Imports GUI_CommonOperations
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE

Public Class uc_alesis_cashier_config


  ' Grid Columns
  Private Const GRID_COLUMNS As Integer = 5

  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const GRID_COLUMN_GAP As Integer = 0
  Private Const GRID_COLUMN_NAME As Integer = 1
  Private Const GRID_COLUMN_EXTERNAL_ID As Integer = 2
  Private Const GRID_COLUMN_MACHINEID As Integer = 3
  Private Const GRID_COLUMN_IDX As Integer = 4

  Public Class TYPE_TERMINAL
    Public name As String
    Public id As Integer
    Public external_id As String
    Public current_machine_id As Integer
    Public initial_machine_id As Integer
    Public status As Integer
    Public backcolor As Drawing.Color
    Public forecolor As Drawing.Color
    Public visible As Boolean
  End Class

  Public configured_terminals As New List(Of TYPE_TERMINAL)

  Public Overloads Property DataBaseIp() As String
    Get
      Return Uc_entry_ip_address.Value
    End Get

    Set(ByVal Value As String)
      Uc_entry_ip_address.Value = Value
    End Set

  End Property

  Public Overloads Property DataBaseName() As String
    Get
      Return Uc_entry_database_name.Value
    End Get

    Set(ByVal Value As String)
      Uc_entry_database_name.Value = Value
    End Set

  End Property

  Public Overloads Property VendorId() As String
    Get
      Return Uc_entry_vendor_id.Value
    End Get

    Set(ByVal Value As String)
      Uc_entry_vendor_id.Value = Value
    End Set

  End Property

  Public Overloads Property Username() As String
    Get
      Return Uc_entry_sql_user.Value
    End Get

    Set(ByVal Value As String)
      Uc_entry_sql_user.Value = Value
    End Set

  End Property

  Public Overloads Property Password() As String
    Get
      Return Uc_entry_sql_password.Value
    End Get

    Set(ByVal Value As String)
      Uc_entry_sql_password.Value = Value
    End Set

  End Property

  Public Overloads ReadOnly Property TotalRows() As Integer
    Get
      Return configured_terminals.Count
    End Get
  End Property

  Public Sub ClearConfigured()
    configured_terminals.Clear()
    Uc_grid_configured.Clear()
  End Sub

  ' RCI 01-JUN-2010: Access to Redraw property from uc_grid to speed up adding rows.
  Public Sub RedrawConfigured(ByVal BoolRedraw As Boolean)
    Uc_grid_configured.Redraw = BoolRedraw
  End Sub

  Public Sub AddConfiguredRow(ByVal Id As Integer, ByVal Name As String, ByVal ExternalId As String, ByVal MachineId As Integer, ByVal Status As Integer, ByVal Idx As Integer)
    Dim temp_term As New TYPE_TERMINAL

    temp_term.id = Id
    temp_term.name = Name
    temp_term.external_id = ExternalId
    temp_term.current_machine_id = MachineId
    temp_term.initial_machine_id = MachineId
    temp_term.status = Status

    If Status = ENUM_TERMINAL_STATUS.TERMINAL_STATUS_OK Then
      If MachineId = 0 Then
        temp_term.backcolor = Drawing.Color.LightGray
        temp_term.forecolor = Drawing.Color.Black
      Else
        temp_term.backcolor = Drawing.Color.White
        temp_term.forecolor = Drawing.Color.Black
      End If
    End If

    If Status = ENUM_TERMINAL_STATUS.TERMINAL_STATUS_PENDING Then
      temp_term.backcolor = Drawing.Color.Khaki
      temp_term.forecolor = Drawing.Color.Black
    End If

    If Status = ENUM_TERMINAL_STATUS.TERMINAL_STATUS_ERROR Then
      temp_term.backcolor = Drawing.Color.Red
      temp_term.forecolor = Drawing.Color.White
    End If

    temp_term.visible = True

    configured_terminals.Add(temp_term)
    AddGridRow(temp_term, Idx)

  End Sub


  Private Sub AddGridRow(ByVal TempTerm As TYPE_TERMINAL, ByVal Idx As Integer)
    Uc_grid_configured.AddRow()
    Uc_grid_configured.Cell(Uc_grid_configured.NumRows - 1, GRID_COLUMN_NAME).Value = TempTerm.name
    Uc_grid_configured.Cell(Uc_grid_configured.NumRows - 1, GRID_COLUMN_EXTERNAL_ID).Value = TempTerm.external_id
    Uc_grid_configured.Row(Uc_grid_configured.NumRows - 1).BackColor = TempTerm.backcolor
    Uc_grid_configured.Row(Uc_grid_configured.NumRows - 1).ForeColor = TempTerm.forecolor
    If TempTerm.current_machine_id = 0 Then
      Uc_grid_configured.Cell(Uc_grid_configured.NumRows - 1, GRID_COLUMN_MACHINEID).Value = ""
    Else
      Uc_grid_configured.Cell(Uc_grid_configured.NumRows - 1, GRID_COLUMN_MACHINEID).Value = TempTerm.current_machine_id
    End If
    Uc_grid_configured.Cell(Uc_grid_configured.NumRows - 1, GRID_COLUMN_IDX).Value = Idx

  End Sub

  Public Sub GetConfiguredRow(ByVal idx As Integer, ByRef Id As Integer, ByRef Name As String, ByRef ExternalId As String, ByRef MachineId As Integer, ByVal Status As Integer)

    Id = configured_terminals(idx).id
    Name = configured_terminals(idx).name
    ExternalId = configured_terminals(idx).external_id
    MachineId = configured_terminals(idx).current_machine_id
    Status = configured_terminals(idx).status

  End Sub


  Private Sub GUI_StyleView()

    With Me.Uc_grid_configured

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

      ' Grid Columns
      ' Gap column
      .Column(GRID_COLUMN_GAP).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_GAP).Header.Text = ""
      .Column(GRID_COLUMN_GAP).Width = 350
      .Column(GRID_COLUMN_GAP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_GAP).WidthFixed = 350
      .Column(GRID_COLUMN_GAP).Fixed = True
      .Column(GRID_COLUMN_GAP).HighLightWhenSelected = False

      'Name
      .Column(GRID_COLUMN_NAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_NAME).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(210)
      .Column(GRID_COLUMN_NAME).Width = 2250
      .Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'External Id
      .Column(GRID_COLUMN_EXTERNAL_ID).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_EXTERNAL_ID).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(214)
      .Column(GRID_COLUMN_EXTERNAL_ID).Width = 2275
      .Column(GRID_COLUMN_EXTERNAL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'Machine ID
      .Column(GRID_COLUMN_MACHINEID).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_MACHINEID).Header.Text = GLB_NLS_GUI_CONFIGURATION.GetString(211)
      .Column(GRID_COLUMN_MACHINEID).Width = 1100
      .Column(GRID_COLUMN_MACHINEID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_MACHINEID).Editable = True
      .Column(GRID_COLUMN_MACHINEID).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_MACHINEID).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_COLUMN_MACHINEID).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 10, 0)

      'Idx
      .Column(GRID_COLUMN_IDX).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_IDX).Header.Text = ""
      .Column(GRID_COLUMN_IDX).Width = 0
      .Column(GRID_COLUMN_IDX).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_IDX).WidthFixed = 0
      .Column(GRID_COLUMN_IDX).Fixed = True


    End With




  End Sub ' GUI_StyleView



  Private Sub InitControls()
    Uc_entry_ip_address.Text = GLB_NLS_GUI_CONFIGURATION.GetString(202)
    Uc_entry_database_name.Text = GLB_NLS_GUI_CONFIGURATION.GetString(203)
    Uc_entry_vendor_id.Text = GLB_NLS_GUI_CONFIGURATION.GetString(205)
    Uc_entry_sql_user.Text = GLB_NLS_GUI_CONFIGURATION.GetString(206)
    Uc_entry_sql_password.Text = GLB_NLS_GUI_CONFIGURATION.GetString(207)

    gb_conn_params.Text = GLB_NLS_GUI_CONFIGURATION.GetString(216)
    gb_terminals.Text = GLB_NLS_GUI_CONFIGURATION.GetString(223)
    cb_configured_terminals.Text = GLB_NLS_GUI_CONFIGURATION.GetString(217)
    cb_not_configured_terminals.Text = GLB_NLS_GUI_CONFIGURATION.GetString(218)

    gb_captions.Text = GLB_NLS_GUI_CONFIGURATION.GetString(244)
    lbl_caption_ok.Text = GLB_NLS_GUI_CONFIGURATION.GetString(239)
    lbl_caption_error.Text = GLB_NLS_GUI_CONFIGURATION.GetString(240)
    lbl_caption_pending.Text = GLB_NLS_GUI_CONFIGURATION.GetString(242)
    lbl_caption_not_conf.Text = GLB_NLS_GUI_CONFIGURATION.GetString(243)


    Uc_entry_database_name.SetFilter(FORMAT_TEXT, 50)
    Uc_entry_ip_address.SetFilter(FORMAT_IP_ADDRESS, 15)
    Uc_entry_vendor_id.SetFilter(FORMAT_TEXT, 50)
    Uc_entry_sql_user.SetFilter(FORMAT_TEXT, 50)
    Uc_entry_sql_password.SetFilter(FORMAT_TEXT, 50)

    cb_configured_terminals.Checked = True
    cb_not_configured_terminals.Checked = True

    ' Format entry fields
    GUI_StyleView()
  End Sub

  ''' <summary>
  ''' Sets the focus of a component having the nls
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub SetFocus(ByVal Nls As Integer)
    Select Case Nls
      Case 202
        Me.Uc_entry_ip_address.Focus()
      Case 203
        Me.Uc_entry_database_name.Focus()
      Case 205
        Me.Uc_entry_vendor_id.Focus()
      Case 206
        Me.Uc_entry_sql_user.Focus()
      Case 207
        Me.Uc_entry_sql_password.Focus()

    End Select
  End Sub

  ''' <summary>
  ''' Returns the NLS of the missing field, or 0 if ok
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function DataOk() As Integer
    If Uc_entry_vendor_id.Value = "" Then
      Return 205
    End If
    If Uc_entry_ip_address.Value = "" Then
      Return 202
    End If
    If Uc_entry_database_name.Value = "" Then
      Return 203
    End If
    If Uc_entry_sql_user.Value = "" Then
      Return 206
    End If
    If Uc_entry_sql_password.Value = "" Then
      Return 207
    End If
    Return 0
  End Function

  Public Sub Clear()
    '    Call m_table.Rows.Clear()
  End Sub

  Public Function Count() As Integer
    '   Return m_table.Rows.Count
  End Function

  Public Function DesiredWidth() As Integer
    Return pnl_global.Width
  End Function

  Public Function DesiredHeight() As Integer
    Return pnl_global.Height
  End Function

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

    Call InitControls()

  End Sub



  Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' update grid, 
  End Sub

  Public Sub SetValues(ByVal a As Integer)

  End Sub

  Private Sub ResetColors()

    For Each temp_term As TYPE_TERMINAL In configured_terminals
      temp_term.forecolor = Drawing.Color.Black
      If temp_term.current_machine_id > 0 Then
        If temp_term.current_machine_id <> temp_term.initial_machine_id Then
          temp_term.backcolor = Drawing.Color.Khaki
        Else
          temp_term.backcolor = Drawing.Color.White
        End If
      Else
        temp_term.backcolor = Drawing.Color.LightGray
      End If


      If temp_term.status = ENUM_TERMINAL_STATUS.TERMINAL_STATUS_ERROR And  temp_term.current_machine_id = temp_term.initial_machine_id Then
        temp_term.forecolor = Drawing.Color.White
        temp_term.backcolor = Drawing.Color.Red
      End If


      If temp_term.status = ENUM_TERMINAL_STATUS.TERMINAL_STATUS_PENDING And temp_term.current_machine_id = temp_term.initial_machine_id Then
        temp_term.backcolor = Drawing.Color.Khaki
        temp_term.forecolor = Drawing.Color.Black
      End If

    Next
  End Sub

  Private Function FindResolveConflict() As Boolean
    Dim result As Boolean
    result = False

    ResetColors()

    For Each temp_term As TYPE_TERMINAL In configured_terminals
      For Each temp_term2 As TYPE_TERMINAL In configured_terminals
        If temp_term.current_machine_id = temp_term2.current_machine_id And temp_term.id <> temp_term2.id And temp_term.current_machine_id <> 0 Then
          temp_term.forecolor = Drawing.Color.White
          temp_term.backcolor = Drawing.Color.Red
          temp_term2.backcolor = Drawing.Color.Red
          temp_term2.forecolor = Drawing.Color.White
          result = True
        End If
      Next
    Next
    Return result

  End Function

  Private Sub RepaintGrid()
    Dim idx As Integer
    Uc_grid_configured.Redraw = False
    For idx = 0 To Uc_grid_configured.NumRows - 1
      If configured_terminals(Uc_grid_configured.Cell(idx, GRID_COLUMN_IDX).Value).visible And _
      Uc_grid_configured.Row(idx).BackColor <> configured_terminals(Uc_grid_configured.Cell(idx, GRID_COLUMN_IDX).Value).backcolor Then
        Uc_grid_configured.Row(idx).ForeColor = configured_terminals(Uc_grid_configured.Cell(idx, GRID_COLUMN_IDX).Value).forecolor
        Uc_grid_configured.Row(idx).BackColor = configured_terminals(Uc_grid_configured.Cell(idx, GRID_COLUMN_IDX).Value).backcolor
      End If
    Next
    Uc_grid_configured.Redraw = True
  End Sub


  Private Sub Uc_grid_configured_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles Uc_grid_configured.CellDataChangedEvent

    If (Uc_grid_configured.Cell(Row, GRID_COLUMN_MACHINEID).Value <> "") Then
      configured_terminals(Uc_grid_configured.Cell(Row, GRID_COLUMN_IDX).Value).current_machine_id = Uc_grid_configured.Cell(Row, GRID_COLUMN_MACHINEID).Value
      Uc_grid_configured.Cell(Row, GRID_COLUMN_MACHINEID).Value = GUI_FormatNumber(Uc_grid_configured.Cell(Row, GRID_COLUMN_MACHINEID).Value, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    Else
      configured_terminals(Uc_grid_configured.Cell(Row, GRID_COLUMN_IDX).Value).current_machine_id = 0
    End If

    FindResolveConflict()

    If (Uc_grid_configured.Cell(Row, GRID_COLUMN_MACHINEID).Value <> "") Then
      If (Uc_grid_configured.Cell(Row, GRID_COLUMN_MACHINEID).Value = 0) Then
        Uc_grid_configured.Cell(Row, GRID_COLUMN_MACHINEID).Value = ""
      End If
    End If
    RepaintGrid()

  End Sub

  Private Sub Uc_grid_configured_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Uc_grid_configured.Load

  End Sub


  Public Sub ShowHideTerminals()
    Dim idx As Integer
    Dim showing_rows As Integer
    showing_rows = 0
    Uc_grid_configured.Redraw = False
    Uc_grid_configured.Clear()
    Uc_grid_configured.Redraw = True

    Uc_grid_configured.Redraw = False

    idx = 0
    For Each temp_term As TYPE_TERMINAL In configured_terminals
      If cb_configured_terminals.Checked Then
        If temp_term.current_machine_id <> 0 Then
          temp_term.visible = True
        End If
      Else
        If temp_term.current_machine_id <> 0 Then
          temp_term.visible = False
        End If
      End If
      If cb_not_configured_terminals.Checked Then
        If temp_term.current_machine_id = 0 Then
          temp_term.visible = True
        End If
      Else
        If temp_term.current_machine_id = 0 Then
          temp_term.visible = False
        End If
      End If

      If temp_term.visible Then
        AddGridRow(temp_term, idx)
      End If

      idx = idx + 1
    Next
    Uc_grid_configured.Redraw = True
  End Sub

  Private Sub cb_configured_terminals_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_configured_terminals.CheckedChanged
    ShowHideTerminals()
  End Sub

  Private Sub cb_not_configured_terminals_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_not_configured_terminals.CheckedChanged
    ShowHideTerminals()
  End Sub

  Public Function KeyPressed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean
    If Me.Uc_grid_configured.ContainsFocus Then
      Return Me.Uc_grid_configured.KeyPressed(sender, e)
    End If
  End Function
End Class
