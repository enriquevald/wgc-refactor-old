<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_points_to_credit
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(uc_points_to_credit))
    Me.gb_credit_type = New System.Windows.Forms.GroupBox()
    Me.opt_credit_redeemable = New System.Windows.Forms.RadioButton()
    Me.opt_credit_non_redeemable = New System.Windows.Forms.RadioButton()
    Me.gb_enabled = New System.Windows.Forms.GroupBox()
    Me.opt_disabled = New System.Windows.Forms.RadioButton()
    Me.opt_enabled = New System.Windows.Forms.RadioButton()
    Me.gb_daily = New System.Windows.Forms.GroupBox()
    Me.chk_second_time = New System.Windows.Forms.CheckBox()
    Me.lbl_information = New System.Windows.Forms.Label()
    Me.lbl_terminals = New System.Windows.Forms.Label()
    Me.lbl_day = New System.Windows.Forms.Label()
    Me.chk_sunday = New System.Windows.Forms.CheckBox()
    Me.chk_saturday = New System.Windows.Forms.CheckBox()
    Me.chk_monday = New System.Windows.Forms.CheckBox()
    Me.chk_friday = New System.Windows.Forms.CheckBox()
    Me.chk_tuesday = New System.Windows.Forms.CheckBox()
    Me.chk_thursday = New System.Windows.Forms.CheckBox()
    Me.chk_wednesday = New System.Windows.Forms.CheckBox()
    Me.lbl_description = New System.Windows.Forms.Label()
    Me.txt_description = New System.Windows.Forms.TextBox()
    Me.gb_game_use = New System.Windows.Forms.GroupBox()
    Me.cmb_award_with_game = New System.Windows.Forms.ComboBox()
    Me.chk_award_with_game = New System.Windows.Forms.CheckBox()
    Me.ef_points_credit_sufix = New GUI_Controls.uc_entry_field()
    Me.uc_terminals_group_monday = New GUI_Controls.uc_terminals_group_filter()
    Me.uc_terminals_group_tuesday = New GUI_Controls.uc_terminals_group_filter()
    Me.uc_terminals_group_wednesday = New GUI_Controls.uc_terminals_group_filter()
    Me.uc_terminals_group_thursday = New GUI_Controls.uc_terminals_group_filter()
    Me.uc_terminals_group_friday = New GUI_Controls.uc_terminals_group_filter()
    Me.uc_terminals_group_saturday = New GUI_Controls.uc_terminals_group_filter()
    Me.uc_terminals_group_sunday = New GUI_Controls.uc_terminals_group_filter()
    Me.btn_GiftEdit = New GUI_Controls.uc_button()
    Me.dtp_second_time_to = New GUI_Controls.uc_date_picker()
    Me.dtp_second_time_from = New GUI_Controls.uc_date_picker()
    Me.dtp_time_to = New GUI_Controls.uc_date_picker()
    Me.dtp_time_from = New GUI_Controls.uc_date_picker()
    Me.ef_points_credit_name = New GUI_Controls.uc_entry_field()
    Me.uc_grid_table_points = New GUI_Controls.uc_grid()
    Me.gb_credit_type.SuspendLayout()
    Me.gb_enabled.SuspendLayout()
    Me.gb_daily.SuspendLayout()
    Me.gb_game_use.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_credit_type
    '
    Me.gb_credit_type.Controls.Add(Me.opt_credit_redeemable)
    Me.gb_credit_type.Controls.Add(Me.opt_credit_non_redeemable)
    Me.gb_credit_type.Location = New System.Drawing.Point(203, 33)
    Me.gb_credit_type.Name = "gb_credit_type"
    Me.gb_credit_type.Size = New System.Drawing.Size(197, 68)
    Me.gb_credit_type.TabIndex = 3
    Me.gb_credit_type.TabStop = False
    Me.gb_credit_type.Text = "xType"
    '
    'opt_credit_redeemable
    '
    Me.opt_credit_redeemable.Location = New System.Drawing.Point(15, 40)
    Me.opt_credit_redeemable.Name = "opt_credit_redeemable"
    Me.opt_credit_redeemable.Size = New System.Drawing.Size(135, 16)
    Me.opt_credit_redeemable.TabIndex = 2
    Me.opt_credit_redeemable.Text = "xRedeemable"
    '
    'opt_credit_non_redeemable
    '
    Me.opt_credit_non_redeemable.Checked = True
    Me.opt_credit_non_redeemable.Location = New System.Drawing.Point(15, 18)
    Me.opt_credit_non_redeemable.Name = "opt_credit_non_redeemable"
    Me.opt_credit_non_redeemable.Size = New System.Drawing.Size(135, 16)
    Me.opt_credit_non_redeemable.TabIndex = 1
    Me.opt_credit_non_redeemable.TabStop = True
    Me.opt_credit_non_redeemable.Text = "xNon-Redeemable"
    '
    'gb_enabled
    '
    Me.gb_enabled.Controls.Add(Me.opt_disabled)
    Me.gb_enabled.Controls.Add(Me.opt_enabled)
    Me.gb_enabled.Location = New System.Drawing.Point(3, 33)
    Me.gb_enabled.Name = "gb_enabled"
    Me.gb_enabled.Size = New System.Drawing.Size(194, 68)
    Me.gb_enabled.TabIndex = 2
    Me.gb_enabled.TabStop = False
    Me.gb_enabled.Text = "xEnabled"
    '
    'opt_disabled
    '
    Me.opt_disabled.Location = New System.Drawing.Point(15, 43)
    Me.opt_disabled.Name = "opt_disabled"
    Me.opt_disabled.Size = New System.Drawing.Size(70, 16)
    Me.opt_disabled.TabIndex = 1
    Me.opt_disabled.Text = "xNo"
    '
    'opt_enabled
    '
    Me.opt_enabled.Checked = True
    Me.opt_enabled.Location = New System.Drawing.Point(15, 21)
    Me.opt_enabled.Name = "opt_enabled"
    Me.opt_enabled.Size = New System.Drawing.Size(70, 16)
    Me.opt_enabled.TabIndex = 0
    Me.opt_enabled.TabStop = True
    Me.opt_enabled.Text = "xYes"
    '
    'gb_daily
    '
    Me.gb_daily.Controls.Add(Me.chk_second_time)
    Me.gb_daily.Controls.Add(Me.lbl_information)
    Me.gb_daily.Controls.Add(Me.lbl_terminals)
    Me.gb_daily.Controls.Add(Me.lbl_day)
    Me.gb_daily.Controls.Add(Me.chk_sunday)
    Me.gb_daily.Controls.Add(Me.dtp_second_time_to)
    Me.gb_daily.Controls.Add(Me.chk_saturday)
    Me.gb_daily.Controls.Add(Me.chk_monday)
    Me.gb_daily.Controls.Add(Me.dtp_second_time_from)
    Me.gb_daily.Controls.Add(Me.chk_friday)
    Me.gb_daily.Controls.Add(Me.dtp_time_to)
    Me.gb_daily.Controls.Add(Me.chk_tuesday)
    Me.gb_daily.Controls.Add(Me.chk_thursday)
    Me.gb_daily.Controls.Add(Me.dtp_time_from)
    Me.gb_daily.Controls.Add(Me.chk_wednesday)
    Me.gb_daily.Location = New System.Drawing.Point(6, 296)
    Me.gb_daily.Name = "gb_daily"
    Me.gb_daily.Size = New System.Drawing.Size(851, 240)
    Me.gb_daily.TabIndex = 6
    Me.gb_daily.TabStop = False
    Me.gb_daily.Text = "xDaily"
    '
    'chk_second_time
    '
    Me.chk_second_time.AutoSize = True
    Me.chk_second_time.Location = New System.Drawing.Point(7, 70)
    Me.chk_second_time.Name = "chk_second_time"
    Me.chk_second_time.Size = New System.Drawing.Size(15, 14)
    Me.chk_second_time.TabIndex = 3
    Me.chk_second_time.UseVisualStyleBackColor = True
    '
    'lbl_information
    '
    Me.lbl_information.AutoSize = True
    Me.lbl_information.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_information.Location = New System.Drawing.Point(295, 220)
    Me.lbl_information.Name = "lbl_information"
    Me.lbl_information.Size = New System.Drawing.Size(468, 13)
    Me.lbl_information.TabIndex = 25
    Me.lbl_information.Text = " xA los d�as no seleccionados se les aplica la restricci�n de terminales del �lti" & _
    "mo d�a seleccionado"
    '
    'lbl_terminals
    '
    Me.lbl_terminals.AutoSize = True
    Me.lbl_terminals.Location = New System.Drawing.Point(482, 24)
    Me.lbl_terminals.Name = "lbl_terminals"
    Me.lbl_terminals.Size = New System.Drawing.Size(248, 13)
    Me.lbl_terminals.TabIndex = 14
    Me.lbl_terminals.Text = " xTerminals that can use the non-redeemable credit"
    '
    'lbl_day
    '
    Me.lbl_day.AutoSize = True
    Me.lbl_day.Location = New System.Drawing.Point(402, 24)
    Me.lbl_day.Name = "lbl_day"
    Me.lbl_day.Size = New System.Drawing.Size(31, 13)
    Me.lbl_day.TabIndex = 13
    Me.lbl_day.Text = "xDay"
    '
    'chk_sunday
    '
    Me.chk_sunday.AutoSize = True
    Me.chk_sunday.Location = New System.Drawing.Point(402, 197)
    Me.chk_sunday.Name = "chk_sunday"
    Me.chk_sunday.Size = New System.Drawing.Size(67, 17)
    Me.chk_sunday.TabIndex = 12
    Me.chk_sunday.Text = "xSunday"
    Me.chk_sunday.UseVisualStyleBackColor = True
    '
    'chk_saturday
    '
    Me.chk_saturday.AutoSize = True
    Me.chk_saturday.Location = New System.Drawing.Point(402, 173)
    Me.chk_saturday.Name = "chk_saturday"
    Me.chk_saturday.Size = New System.Drawing.Size(73, 17)
    Me.chk_saturday.TabIndex = 11
    Me.chk_saturday.Text = "xSaturday"
    Me.chk_saturday.UseVisualStyleBackColor = True
    '
    'chk_monday
    '
    Me.chk_monday.AutoSize = True
    Me.chk_monday.Location = New System.Drawing.Point(402, 45)
    Me.chk_monday.Name = "chk_monday"
    Me.chk_monday.Size = New System.Drawing.Size(69, 17)
    Me.chk_monday.TabIndex = 6
    Me.chk_monday.Text = "xMonday"
    Me.chk_monday.UseVisualStyleBackColor = True
    '
    'chk_friday
    '
    Me.chk_friday.AutoSize = True
    Me.chk_friday.Location = New System.Drawing.Point(402, 149)
    Me.chk_friday.Name = "chk_friday"
    Me.chk_friday.Size = New System.Drawing.Size(59, 17)
    Me.chk_friday.TabIndex = 10
    Me.chk_friday.Text = "xFriday"
    Me.chk_friday.UseVisualStyleBackColor = True
    '
    'chk_tuesday
    '
    Me.chk_tuesday.AutoSize = True
    Me.chk_tuesday.Location = New System.Drawing.Point(402, 72)
    Me.chk_tuesday.Name = "chk_tuesday"
    Me.chk_tuesday.Size = New System.Drawing.Size(72, 17)
    Me.chk_tuesday.TabIndex = 7
    Me.chk_tuesday.Text = "xTuesday"
    Me.chk_tuesday.UseVisualStyleBackColor = True
    '
    'chk_thursday
    '
    Me.chk_thursday.AutoSize = True
    Me.chk_thursday.Location = New System.Drawing.Point(402, 125)
    Me.chk_thursday.Name = "chk_thursday"
    Me.chk_thursday.Size = New System.Drawing.Size(75, 17)
    Me.chk_thursday.TabIndex = 9
    Me.chk_thursday.Text = "xThursday"
    Me.chk_thursday.UseVisualStyleBackColor = True
    '
    'chk_wednesday
    '
    Me.chk_wednesday.AutoSize = True
    Me.chk_wednesday.Location = New System.Drawing.Point(402, 99)
    Me.chk_wednesday.Name = "chk_wednesday"
    Me.chk_wednesday.Size = New System.Drawing.Size(88, 17)
    Me.chk_wednesday.TabIndex = 8
    Me.chk_wednesday.Text = "xWednesday"
    Me.chk_wednesday.UseVisualStyleBackColor = True
    '
    'lbl_description
    '
    Me.lbl_description.AutoSize = True
    Me.lbl_description.Location = New System.Drawing.Point(3, 539)
    Me.lbl_description.Name = "lbl_description"
    Me.lbl_description.Size = New System.Drawing.Size(65, 13)
    Me.lbl_description.TabIndex = 23
    Me.lbl_description.Text = "xDescription"
    '
    'txt_description
    '
    Me.txt_description.Location = New System.Drawing.Point(4, 560)
    Me.txt_description.MaxLength = 200
    Me.txt_description.Multiline = True
    Me.txt_description.Name = "txt_description"
    Me.txt_description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.txt_description.Size = New System.Drawing.Size(852, 126)
    Me.txt_description.TabIndex = 14
    '
    'gb_game_use
    '
    Me.gb_game_use.Controls.Add(Me.cmb_award_with_game)
    Me.gb_game_use.Controls.Add(Me.chk_award_with_game)
    Me.gb_game_use.Location = New System.Drawing.Point(406, 33)
    Me.gb_game_use.Name = "gb_game_use"
    Me.gb_game_use.Size = New System.Drawing.Size(356, 68)
    Me.gb_game_use.TabIndex = 4
    Me.gb_game_use.TabStop = False
    Me.gb_game_use.Text = "xGameUse"
    '
    'cmb_award_with_game
    '
    Me.cmb_award_with_game.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cmb_award_with_game.FormattingEnabled = True
    Me.cmb_award_with_game.Location = New System.Drawing.Point(127, 27)
    Me.cmb_award_with_game.Name = "cmb_award_with_game"
    Me.cmb_award_with_game.Size = New System.Drawing.Size(223, 21)
    Me.cmb_award_with_game.TabIndex = 10
    '
    'chk_award_with_game
    '
    Me.chk_award_with_game.AutoSize = True
    Me.chk_award_with_game.Location = New System.Drawing.Point(6, 29)
    Me.chk_award_with_game.Name = "chk_award_with_game"
    Me.chk_award_with_game.Size = New System.Drawing.Size(111, 17)
    Me.chk_award_with_game.TabIndex = 9
    Me.chk_award_with_game.Text = "xAwardWithGame"
    Me.chk_award_with_game.UseVisualStyleBackColor = True
    '
    'ef_points_credit_sufix
    '
    Me.ef_points_credit_sufix.DoubleValue = 0.0R
    Me.ef_points_credit_sufix.IntegerValue = 0
    Me.ef_points_credit_sufix.IsReadOnly = False
    Me.ef_points_credit_sufix.Location = New System.Drawing.Point(410, 3)
    Me.ef_points_credit_sufix.Name = "ef_points_credit_sufix"
    Me.ef_points_credit_sufix.PlaceHolder = Nothing
    Me.ef_points_credit_sufix.Size = New System.Drawing.Size(164, 24)
    Me.ef_points_credit_sufix.SufixText = "Sufix Text"
    Me.ef_points_credit_sufix.SufixTextVisible = True
    Me.ef_points_credit_sufix.TabIndex = 1
    Me.ef_points_credit_sufix.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_points_credit_sufix.TextValue = ""
    Me.ef_points_credit_sufix.Value = ""
    Me.ef_points_credit_sufix.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_terminals_group_monday
    '
    Me.uc_terminals_group_monday.ComboProviderListTypeEnabled = True
    Me.uc_terminals_group_monday.Enabled = False
    Me.uc_terminals_group_monday.ForbiddenGroups = CType(resources.GetObject("uc_terminals_group_monday.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.uc_terminals_group_monday.HeightOnExpanded = 300
    Me.uc_terminals_group_monday.Ignoreclick = False
    Me.uc_terminals_group_monday.ListEnabled = False
    Me.uc_terminals_group_monday.ListSize = New System.Drawing.Size(237, 156)
    Me.uc_terminals_group_monday.Location = New System.Drawing.Point(488, 336)
    Me.uc_terminals_group_monday.Name = "uc_terminals_group_monday"
    Me.uc_terminals_group_monday.SelectedIndex = 0
    Me.uc_terminals_group_monday.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.CollapsedWithButtons
    Me.uc_terminals_group_monday.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.uc_terminals_group_monday.ShowGroupBoxLine = False
    Me.uc_terminals_group_monday.ShowGroups = Nothing
    Me.uc_terminals_group_monday.ShowNodeAll = True
    Me.uc_terminals_group_monday.Size = New System.Drawing.Size(359, 24)
    Me.uc_terminals_group_monday.TabIndex = 7
    Me.uc_terminals_group_monday.ThreeStateCheckMode = True
    '
    'uc_terminals_group_tuesday
    '
    Me.uc_terminals_group_tuesday.ComboProviderListTypeEnabled = True
    Me.uc_terminals_group_tuesday.Enabled = False
    Me.uc_terminals_group_tuesday.ForbiddenGroups = CType(resources.GetObject("uc_terminals_group_tuesday.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.uc_terminals_group_tuesday.HeightOnExpanded = 300
    Me.uc_terminals_group_tuesday.Ignoreclick = False
    Me.uc_terminals_group_tuesday.ListEnabled = False
    Me.uc_terminals_group_tuesday.ListSize = New System.Drawing.Size(237, 156)
    Me.uc_terminals_group_tuesday.Location = New System.Drawing.Point(488, 363)
    Me.uc_terminals_group_tuesday.Name = "uc_terminals_group_tuesday"
    Me.uc_terminals_group_tuesday.SelectedIndex = 0
    Me.uc_terminals_group_tuesday.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.CollapsedWithButtons
    Me.uc_terminals_group_tuesday.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.uc_terminals_group_tuesday.ShowGroupBoxLine = False
    Me.uc_terminals_group_tuesday.ShowGroups = Nothing
    Me.uc_terminals_group_tuesday.ShowNodeAll = True
    Me.uc_terminals_group_tuesday.Size = New System.Drawing.Size(359, 24)
    Me.uc_terminals_group_tuesday.TabIndex = 8
    Me.uc_terminals_group_tuesday.ThreeStateCheckMode = True
    '
    'uc_terminals_group_wednesday
    '
    Me.uc_terminals_group_wednesday.ComboProviderListTypeEnabled = True
    Me.uc_terminals_group_wednesday.Enabled = False
    Me.uc_terminals_group_wednesday.ForbiddenGroups = CType(resources.GetObject("uc_terminals_group_wednesday.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.uc_terminals_group_wednesday.HeightOnExpanded = 300
    Me.uc_terminals_group_wednesday.Ignoreclick = False
    Me.uc_terminals_group_wednesday.ListEnabled = False
    Me.uc_terminals_group_wednesday.ListSize = New System.Drawing.Size(237, 156)
    Me.uc_terminals_group_wednesday.Location = New System.Drawing.Point(488, 390)
    Me.uc_terminals_group_wednesday.Name = "uc_terminals_group_wednesday"
    Me.uc_terminals_group_wednesday.SelectedIndex = 0
    Me.uc_terminals_group_wednesday.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.CollapsedWithButtons
    Me.uc_terminals_group_wednesday.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.uc_terminals_group_wednesday.ShowGroupBoxLine = False
    Me.uc_terminals_group_wednesday.ShowGroups = Nothing
    Me.uc_terminals_group_wednesday.ShowNodeAll = True
    Me.uc_terminals_group_wednesday.Size = New System.Drawing.Size(359, 24)
    Me.uc_terminals_group_wednesday.TabIndex = 9
    Me.uc_terminals_group_wednesday.ThreeStateCheckMode = True
    '
    'uc_terminals_group_thursday
    '
    Me.uc_terminals_group_thursday.ComboProviderListTypeEnabled = True
    Me.uc_terminals_group_thursday.Enabled = False
    Me.uc_terminals_group_thursday.ForbiddenGroups = CType(resources.GetObject("uc_terminals_group_thursday.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.uc_terminals_group_thursday.HeightOnExpanded = 300
    Me.uc_terminals_group_thursday.Ignoreclick = False
    Me.uc_terminals_group_thursday.ListEnabled = False
    Me.uc_terminals_group_thursday.ListSize = New System.Drawing.Size(237, 156)
    Me.uc_terminals_group_thursday.Location = New System.Drawing.Point(488, 416)
    Me.uc_terminals_group_thursday.Name = "uc_terminals_group_thursday"
    Me.uc_terminals_group_thursday.SelectedIndex = 0
    Me.uc_terminals_group_thursday.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.CollapsedWithButtons
    Me.uc_terminals_group_thursday.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.uc_terminals_group_thursday.ShowGroupBoxLine = False
    Me.uc_terminals_group_thursday.ShowGroups = Nothing
    Me.uc_terminals_group_thursday.ShowNodeAll = True
    Me.uc_terminals_group_thursday.Size = New System.Drawing.Size(359, 24)
    Me.uc_terminals_group_thursday.TabIndex = 10
    Me.uc_terminals_group_thursday.ThreeStateCheckMode = True
    '
    'uc_terminals_group_friday
    '
    Me.uc_terminals_group_friday.ComboProviderListTypeEnabled = True
    Me.uc_terminals_group_friday.Enabled = False
    Me.uc_terminals_group_friday.ForbiddenGroups = CType(resources.GetObject("uc_terminals_group_friday.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.uc_terminals_group_friday.HeightOnExpanded = 243
    Me.uc_terminals_group_friday.Ignoreclick = False
    Me.uc_terminals_group_friday.ListEnabled = False
    Me.uc_terminals_group_friday.ListSize = New System.Drawing.Size(237, 156)
    Me.uc_terminals_group_friday.Location = New System.Drawing.Point(488, 440)
    Me.uc_terminals_group_friday.Name = "uc_terminals_group_friday"
    Me.uc_terminals_group_friday.SelectedIndex = 0
    Me.uc_terminals_group_friday.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.CollapsedWithButtons
    Me.uc_terminals_group_friday.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.uc_terminals_group_friday.ShowGroupBoxLine = False
    Me.uc_terminals_group_friday.ShowGroups = Nothing
    Me.uc_terminals_group_friday.ShowNodeAll = True
    Me.uc_terminals_group_friday.Size = New System.Drawing.Size(359, 24)
    Me.uc_terminals_group_friday.TabIndex = 11
    Me.uc_terminals_group_friday.ThreeStateCheckMode = True
    '
    'uc_terminals_group_saturday
    '
    Me.uc_terminals_group_saturday.ComboProviderListTypeEnabled = True
    Me.uc_terminals_group_saturday.Enabled = False
    Me.uc_terminals_group_saturday.ForbiddenGroups = CType(resources.GetObject("uc_terminals_group_saturday.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.uc_terminals_group_saturday.HeightOnExpanded = 221
    Me.uc_terminals_group_saturday.Ignoreclick = False
    Me.uc_terminals_group_saturday.ListEnabled = False
    Me.uc_terminals_group_saturday.ListSize = New System.Drawing.Size(237, 156)
    Me.uc_terminals_group_saturday.Location = New System.Drawing.Point(488, 464)
    Me.uc_terminals_group_saturday.Name = "uc_terminals_group_saturday"
    Me.uc_terminals_group_saturday.SelectedIndex = 0
    Me.uc_terminals_group_saturday.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.CollapsedWithButtons
    Me.uc_terminals_group_saturday.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.uc_terminals_group_saturday.ShowGroupBoxLine = False
    Me.uc_terminals_group_saturday.ShowGroups = Nothing
    Me.uc_terminals_group_saturday.ShowNodeAll = True
    Me.uc_terminals_group_saturday.Size = New System.Drawing.Size(359, 24)
    Me.uc_terminals_group_saturday.TabIndex = 12
    Me.uc_terminals_group_saturday.ThreeStateCheckMode = True
    '
    'uc_terminals_group_sunday
    '
    Me.uc_terminals_group_sunday.ComboProviderListTypeEnabled = True
    Me.uc_terminals_group_sunday.Enabled = False
    Me.uc_terminals_group_sunday.ForbiddenGroups = CType(resources.GetObject("uc_terminals_group_sunday.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.uc_terminals_group_sunday.HeightOnExpanded = 200
    Me.uc_terminals_group_sunday.Ignoreclick = False
    Me.uc_terminals_group_sunday.ListEnabled = False
    Me.uc_terminals_group_sunday.ListSize = New System.Drawing.Size(237, 156)
    Me.uc_terminals_group_sunday.Location = New System.Drawing.Point(488, 488)
    Me.uc_terminals_group_sunday.Name = "uc_terminals_group_sunday"
    Me.uc_terminals_group_sunday.SelectedIndex = 0
    Me.uc_terminals_group_sunday.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.CollapsedWithButtons
    Me.uc_terminals_group_sunday.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.uc_terminals_group_sunday.ShowGroupBoxLine = False
    Me.uc_terminals_group_sunday.ShowGroups = Nothing
    Me.uc_terminals_group_sunday.ShowNodeAll = True
    Me.uc_terminals_group_sunday.Size = New System.Drawing.Size(359, 24)
    Me.uc_terminals_group_sunday.TabIndex = 13
    Me.uc_terminals_group_sunday.ThreeStateCheckMode = True
    '
    'btn_GiftEdit
    '
    Me.btn_GiftEdit.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_GiftEdit.Location = New System.Drawing.Point(763, 138)
    Me.btn_GiftEdit.Name = "btn_GiftEdit"
    Me.btn_GiftEdit.Size = New System.Drawing.Size(90, 30)
    Me.btn_GiftEdit.TabIndex = 5
    Me.btn_GiftEdit.ToolTipped = False
    Me.btn_GiftEdit.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'dtp_second_time_to
    '
    Me.dtp_second_time_to.Checked = True
    Me.dtp_second_time_to.IsReadOnly = False
    Me.dtp_second_time_to.Location = New System.Drawing.Point(158, 65)
    Me.dtp_second_time_to.Name = "dtp_second_time_to"
    Me.dtp_second_time_to.ShowCheckBox = False
    Me.dtp_second_time_to.ShowUpDown = False
    Me.dtp_second_time_to.Size = New System.Drawing.Size(120, 24)
    Me.dtp_second_time_to.SufixText = "Sufix Text"
    Me.dtp_second_time_to.SufixTextVisible = True
    Me.dtp_second_time_to.TabIndex = 5
    Me.dtp_second_time_to.TextWidth = 48
    Me.dtp_second_time_to.Value = New Date(2010, 5, 12, 0, 0, 0, 0)
    '
    'dtp_second_time_from
    '
    Me.dtp_second_time_from.Checked = True
    Me.dtp_second_time_from.IsReadOnly = False
    Me.dtp_second_time_from.Location = New System.Drawing.Point(22, 65)
    Me.dtp_second_time_from.Name = "dtp_second_time_from"
    Me.dtp_second_time_from.ShowCheckBox = False
    Me.dtp_second_time_from.ShowUpDown = False
    Me.dtp_second_time_from.Size = New System.Drawing.Size(130, 24)
    Me.dtp_second_time_from.SufixText = "Sufix Text"
    Me.dtp_second_time_from.SufixTextVisible = True
    Me.dtp_second_time_from.TabIndex = 4
    Me.dtp_second_time_from.TextWidth = 58
    Me.dtp_second_time_from.Value = New Date(2010, 5, 12, 0, 0, 0, 0)
    '
    'dtp_time_to
    '
    Me.dtp_time_to.Checked = True
    Me.dtp_time_to.IsReadOnly = False
    Me.dtp_time_to.Location = New System.Drawing.Point(158, 40)
    Me.dtp_time_to.Name = "dtp_time_to"
    Me.dtp_time_to.ShowCheckBox = False
    Me.dtp_time_to.ShowUpDown = False
    Me.dtp_time_to.Size = New System.Drawing.Size(120, 24)
    Me.dtp_time_to.SufixText = "Sufix Text"
    Me.dtp_time_to.SufixTextVisible = True
    Me.dtp_time_to.TabIndex = 2
    Me.dtp_time_to.TextWidth = 48
    Me.dtp_time_to.Value = New Date(2010, 5, 12, 0, 0, 0, 0)
    '
    'dtp_time_from
    '
    Me.dtp_time_from.Checked = True
    Me.dtp_time_from.IsReadOnly = False
    Me.dtp_time_from.Location = New System.Drawing.Point(22, 40)
    Me.dtp_time_from.Name = "dtp_time_from"
    Me.dtp_time_from.ShowCheckBox = False
    Me.dtp_time_from.ShowUpDown = False
    Me.dtp_time_from.Size = New System.Drawing.Size(130, 24)
    Me.dtp_time_from.SufixText = "Sufix Text"
    Me.dtp_time_from.SufixTextVisible = True
    Me.dtp_time_from.TabIndex = 1
    Me.dtp_time_from.TextWidth = 58
    Me.dtp_time_from.Value = New Date(2010, 5, 12, 0, 0, 0, 0)
    '
    'ef_points_credit_name
    '
    Me.ef_points_credit_name.DoubleValue = 0.0R
    Me.ef_points_credit_name.IntegerValue = 0
    Me.ef_points_credit_name.IsReadOnly = False
    Me.ef_points_credit_name.Location = New System.Drawing.Point(7, 3)
    Me.ef_points_credit_name.Name = "ef_points_credit_name"
    Me.ef_points_credit_name.PlaceHolder = Nothing
    Me.ef_points_credit_name.Size = New System.Drawing.Size(397, 24)
    Me.ef_points_credit_name.SufixText = "Sufix Text"
    Me.ef_points_credit_name.SufixTextVisible = True
    Me.ef_points_credit_name.TabIndex = 0
    Me.ef_points_credit_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_points_credit_name.TextValue = ""
    Me.ef_points_credit_name.Value = ""
    Me.ef_points_credit_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_grid_table_points
    '
    Me.uc_grid_table_points.CurrentCol = -1
    Me.uc_grid_table_points.CurrentRow = -1
    Me.uc_grid_table_points.EditableCellBackColor = System.Drawing.Color.Empty
    Me.uc_grid_table_points.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.uc_grid_table_points.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.uc_grid_table_points.Location = New System.Drawing.Point(4, 110)
    Me.uc_grid_table_points.Name = "uc_grid_table_points"
    Me.uc_grid_table_points.PanelRightVisible = True
    Me.uc_grid_table_points.Redraw = True
    Me.uc_grid_table_points.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.uc_grid_table_points.Size = New System.Drawing.Size(825, 180)
    Me.uc_grid_table_points.Sortable = False
    Me.uc_grid_table_points.SortAscending = True
    Me.uc_grid_table_points.SortByCol = 0
    Me.uc_grid_table_points.TabIndex = 4
    Me.uc_grid_table_points.ToolTipped = True
    Me.uc_grid_table_points.TopRow = -2
    Me.uc_grid_table_points.WordWrap = False
    '
    'uc_points_to_credit
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
    Me.Controls.Add(Me.gb_game_use)
    Me.Controls.Add(Me.ef_points_credit_sufix)
    Me.Controls.Add(Me.uc_terminals_group_monday)
    Me.Controls.Add(Me.uc_terminals_group_tuesday)
    Me.Controls.Add(Me.uc_terminals_group_wednesday)
    Me.Controls.Add(Me.uc_terminals_group_thursday)
    Me.Controls.Add(Me.uc_terminals_group_friday)
    Me.Controls.Add(Me.uc_terminals_group_saturday)
    Me.Controls.Add(Me.uc_terminals_group_sunday)
    Me.Controls.Add(Me.btn_GiftEdit)
    Me.Controls.Add(Me.lbl_description)
    Me.Controls.Add(Me.txt_description)
    Me.Controls.Add(Me.gb_daily)
    Me.Controls.Add(Me.ef_points_credit_name)
    Me.Controls.Add(Me.gb_credit_type)
    Me.Controls.Add(Me.gb_enabled)
    Me.Controls.Add(Me.uc_grid_table_points)
    Me.Name = "uc_points_to_credit"
    Me.Size = New System.Drawing.Size(861, 691)
    Me.gb_credit_type.ResumeLayout(False)
    Me.gb_enabled.ResumeLayout(False)
    Me.gb_daily.ResumeLayout(False)
    Me.gb_daily.PerformLayout()
    Me.gb_game_use.ResumeLayout(False)
    Me.gb_game_use.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents gb_credit_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_credit_redeemable As System.Windows.Forms.RadioButton
  Friend WithEvents opt_credit_non_redeemable As System.Windows.Forms.RadioButton
  Friend WithEvents gb_enabled As System.Windows.Forms.GroupBox
  Friend WithEvents opt_disabled As System.Windows.Forms.RadioButton
  Friend WithEvents opt_enabled As System.Windows.Forms.RadioButton
  Friend WithEvents ef_points_credit_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_daily As System.Windows.Forms.GroupBox
  Friend WithEvents chk_sunday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_saturday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_monday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_friday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_tuesday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_thursday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_wednesday As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_day As System.Windows.Forms.Label
  Friend WithEvents lbl_terminals As System.Windows.Forms.Label
  Friend WithEvents uc_grid_table_points As GUI_Controls.uc_grid
  Friend WithEvents uc_terminals_group_monday As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents uc_terminals_group_thursday As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents uc_terminals_group_tuesday As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents uc_terminals_group_wednesday As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents uc_terminals_group_friday As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents uc_terminals_group_saturday As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents uc_terminals_group_sunday As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents chk_second_time As System.Windows.Forms.CheckBox
  Friend WithEvents dtp_second_time_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_second_time_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_time_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_time_from As GUI_Controls.uc_date_picker
  Friend WithEvents btn_GiftEdit As GUI_Controls.uc_button
  Friend WithEvents lbl_description As System.Windows.Forms.Label
  Friend WithEvents txt_description As System.Windows.Forms.TextBox
  Friend WithEvents lbl_information As System.Windows.Forms.Label
  Friend WithEvents ef_points_credit_sufix As GUI_Controls.uc_entry_field
  Friend WithEvents gb_game_use As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_award_with_game As System.Windows.Forms.ComboBox
  Friend WithEvents chk_award_with_game As System.Windows.Forms.CheckBox

End Class
