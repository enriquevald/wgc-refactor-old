<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_schedule
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_daily = New System.Windows.Forms.GroupBox()
    Me.chk_second_time = New System.Windows.Forms.CheckBox()
    Me.chk_sunday = New System.Windows.Forms.CheckBox()
    Me.chk_saturday = New System.Windows.Forms.CheckBox()
    Me.chk_monday = New System.Windows.Forms.CheckBox()
    Me.chk_friday = New System.Windows.Forms.CheckBox()
    Me.chk_tuesday = New System.Windows.Forms.CheckBox()
    Me.chk_thursday = New System.Windows.Forms.CheckBox()
    Me.chk_wednesday = New System.Windows.Forms.CheckBox()
    Me.dtp_second_time_to = New GUI_Controls.uc_date_picker()
    Me.dtp_second_time_from = New GUI_Controls.uc_date_picker()
    Me.dtp_time_to = New GUI_Controls.uc_date_picker()
    Me.dtp_time_from = New GUI_Controls.uc_date_picker()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.gb_daily.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_daily
    '
    Me.gb_daily.Controls.Add(Me.chk_second_time)
    Me.gb_daily.Controls.Add(Me.dtp_second_time_to)
    Me.gb_daily.Controls.Add(Me.dtp_second_time_from)
    Me.gb_daily.Controls.Add(Me.dtp_time_to)
    Me.gb_daily.Controls.Add(Me.dtp_time_from)
    Me.gb_daily.Controls.Add(Me.chk_sunday)
    Me.gb_daily.Controls.Add(Me.chk_saturday)
    Me.gb_daily.Controls.Add(Me.chk_monday)
    Me.gb_daily.Controls.Add(Me.chk_friday)
    Me.gb_daily.Controls.Add(Me.chk_tuesday)
    Me.gb_daily.Controls.Add(Me.chk_thursday)
    Me.gb_daily.Controls.Add(Me.chk_wednesday)
    Me.gb_daily.Location = New System.Drawing.Point(5, 37)
    Me.gb_daily.Name = "gb_daily"
    Me.gb_daily.Size = New System.Drawing.Size(397, 196)
    Me.gb_daily.TabIndex = 3
    Me.gb_daily.TabStop = False
    Me.gb_daily.Text = "xDaily"
    '
    'chk_second_time
    '
    Me.chk_second_time.AutoSize = True
    Me.chk_second_time.Location = New System.Drawing.Point(111, 109)
    Me.chk_second_time.Name = "chk_second_time"
    Me.chk_second_time.Size = New System.Drawing.Size(15, 14)
    Me.chk_second_time.TabIndex = 11
    Me.chk_second_time.UseVisualStyleBackColor = True
    '
    'chk_sunday
    '
    Me.chk_sunday.AutoSize = True
    Me.chk_sunday.Location = New System.Drawing.Point(6, 170)
    Me.chk_sunday.Name = "chk_sunday"
    Me.chk_sunday.Size = New System.Drawing.Size(76, 17)
    Me.chk_sunday.TabIndex = 8
    Me.chk_sunday.Text = "xSunday"
    Me.chk_sunday.UseVisualStyleBackColor = True
    '
    'chk_saturday
    '
    Me.chk_saturday.AutoSize = True
    Me.chk_saturday.Location = New System.Drawing.Point(6, 145)
    Me.chk_saturday.Name = "chk_saturday"
    Me.chk_saturday.Size = New System.Drawing.Size(85, 17)
    Me.chk_saturday.TabIndex = 7
    Me.chk_saturday.Text = "xSaturday"
    Me.chk_saturday.UseVisualStyleBackColor = True
    '
    'chk_monday
    '
    Me.chk_monday.AutoSize = True
    Me.chk_monday.Location = New System.Drawing.Point(6, 20)
    Me.chk_monday.Name = "chk_monday"
    Me.chk_monday.Size = New System.Drawing.Size(77, 17)
    Me.chk_monday.TabIndex = 2
    Me.chk_monday.Text = "xMonday"
    Me.chk_monday.UseVisualStyleBackColor = True
    '
    'chk_friday
    '
    Me.chk_friday.AutoSize = True
    Me.chk_friday.Location = New System.Drawing.Point(6, 120)
    Me.chk_friday.Name = "chk_friday"
    Me.chk_friday.Size = New System.Drawing.Size(68, 17)
    Me.chk_friday.TabIndex = 6
    Me.chk_friday.Text = "xFriday"
    Me.chk_friday.UseVisualStyleBackColor = True
    '
    'chk_tuesday
    '
    Me.chk_tuesday.AutoSize = True
    Me.chk_tuesday.Location = New System.Drawing.Point(6, 45)
    Me.chk_tuesday.Name = "chk_tuesday"
    Me.chk_tuesday.Size = New System.Drawing.Size(80, 17)
    Me.chk_tuesday.TabIndex = 3
    Me.chk_tuesday.Text = "xTuesday"
    Me.chk_tuesday.UseVisualStyleBackColor = True
    '
    'chk_thursday
    '
    Me.chk_thursday.AutoSize = True
    Me.chk_thursday.Location = New System.Drawing.Point(6, 95)
    Me.chk_thursday.Name = "chk_thursday"
    Me.chk_thursday.Size = New System.Drawing.Size(86, 17)
    Me.chk_thursday.TabIndex = 5
    Me.chk_thursday.Text = "xThursday"
    Me.chk_thursday.UseVisualStyleBackColor = True
    '
    'chk_wednesday
    '
    Me.chk_wednesday.AutoSize = True
    Me.chk_wednesday.Location = New System.Drawing.Point(6, 70)
    Me.chk_wednesday.Name = "chk_wednesday"
    Me.chk_wednesday.Size = New System.Drawing.Size(98, 17)
    Me.chk_wednesday.TabIndex = 4
    Me.chk_wednesday.Text = "xWednesday"
    Me.chk_wednesday.UseVisualStyleBackColor = True
    '
    'dtp_second_time_to
    '
    Me.dtp_second_time_to.Checked = True
    Me.dtp_second_time_to.IsReadOnly = False
    Me.dtp_second_time_to.Location = New System.Drawing.Point(262, 104)
    Me.dtp_second_time_to.Name = "dtp_second_time_to"
    Me.dtp_second_time_to.ShowCheckBox = False
    Me.dtp_second_time_to.ShowUpDown = False
    Me.dtp_second_time_to.Size = New System.Drawing.Size(120, 24)
    Me.dtp_second_time_to.SufixText = "Sufix Text"
    Me.dtp_second_time_to.SufixTextVisible = True
    Me.dtp_second_time_to.TabIndex = 10
    Me.dtp_second_time_to.TextWidth = 48
    Me.dtp_second_time_to.Value = New Date(2010, 5, 12, 0, 0, 0, 0)
    '
    'dtp_second_time_from
    '
    Me.dtp_second_time_from.Checked = True
    Me.dtp_second_time_from.IsReadOnly = False
    Me.dtp_second_time_from.Location = New System.Drawing.Point(126, 104)
    Me.dtp_second_time_from.Name = "dtp_second_time_from"
    Me.dtp_second_time_from.ShowCheckBox = False
    Me.dtp_second_time_from.ShowUpDown = False
    Me.dtp_second_time_from.Size = New System.Drawing.Size(130, 24)
    Me.dtp_second_time_from.SufixText = "Sufix Text"
    Me.dtp_second_time_from.SufixTextVisible = True
    Me.dtp_second_time_from.TabIndex = 9
    Me.dtp_second_time_from.TextWidth = 58
    Me.dtp_second_time_from.Value = New Date(2010, 5, 12, 0, 0, 0, 0)
    '
    'dtp_time_to
    '
    Me.dtp_time_to.Checked = True
    Me.dtp_time_to.IsReadOnly = False
    Me.dtp_time_to.Location = New System.Drawing.Point(262, 79)
    Me.dtp_time_to.Name = "dtp_time_to"
    Me.dtp_time_to.ShowCheckBox = False
    Me.dtp_time_to.ShowUpDown = False
    Me.dtp_time_to.Size = New System.Drawing.Size(120, 24)
    Me.dtp_time_to.SufixText = "Sufix Text"
    Me.dtp_time_to.SufixTextVisible = True
    Me.dtp_time_to.TabIndex = 1
    Me.dtp_time_to.TextWidth = 48
    Me.dtp_time_to.Value = New Date(2010, 5, 12, 0, 0, 0, 0)
    '
    'dtp_time_from
    '
    Me.dtp_time_from.Checked = True
    Me.dtp_time_from.IsReadOnly = False
    Me.dtp_time_from.Location = New System.Drawing.Point(126, 79)
    Me.dtp_time_from.Name = "dtp_time_from"
    Me.dtp_time_from.ShowCheckBox = False
    Me.dtp_time_from.ShowUpDown = False
    Me.dtp_time_from.Size = New System.Drawing.Size(130, 24)
    Me.dtp_time_from.SufixText = "Sufix Text"
    Me.dtp_time_from.SufixTextVisible = True
    Me.dtp_time_from.TabIndex = 0
    Me.dtp_time_from.TextWidth = 58
    Me.dtp_time_from.Value = New Date(2010, 5, 12, 0, 0, 0, 0)
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(235, 7)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = False
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(167, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 20
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(5, 7)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(226, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_schedule
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.gb_daily)
    Me.Controls.Add(Me.dtp_to)
    Me.Controls.Add(Me.dtp_from)
    Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Name = "uc_schedule"
    Me.Size = New System.Drawing.Size(410, 239)
    Me.gb_daily.ResumeLayout(False)
    Me.gb_daily.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents gb_daily As System.Windows.Forms.GroupBox
  Friend WithEvents chk_second_time As System.Windows.Forms.CheckBox
  Friend WithEvents dtp_second_time_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_second_time_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_time_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_time_from As GUI_Controls.uc_date_picker
  Friend WithEvents chk_sunday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_saturday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_monday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_friday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_tuesday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_thursday As System.Windows.Forms.CheckBox
  Friend WithEvents chk_wednesday As System.Windows.Forms.CheckBox

End Class
