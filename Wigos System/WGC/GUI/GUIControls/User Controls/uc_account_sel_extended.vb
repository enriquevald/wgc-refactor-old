'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:        uc_account_sel.vb  
'
' DESCRIPTION:        
'
' AUTHOR:             Javi Barea
'
' CREATION DATE:      28-April-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ ----------------------------------------------
' 28-APR-2014  JBP    Initial Version.
' 06-JAN-2016  SGB    Backlog Item 7910: Change column AC_POINTS to bucket 1.
'
'
'
'         REVISAR SI LAS PROPIEDADES Tal SON NECESARIAS 
'                         ..SINO ELIMINAR..
'
'
'
'
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common

Public Class uc_account_sel_extended

  Private Const SUBTRACT_TO_INITIAL_YEAR = 150
  Private Const SEPARATOR As Char = " "

  ' Type of card for where clause
  Private Const CARD_TYPE As Integer = 2

  Public Event OneAccountFilterChanged()

  Dim m_show_extended As Boolean
  Dim m_disabled_holder As Boolean
  ' CCG 26-09-2013: Mulitype Massive search
  Dim m_accounts_massive_search As String
  Dim m_accounts_massive_search_to_edit As String
  Dim m_accounts_massive_search_type As Integer

  Public m_days_without_activity As Int32
  Dim m_extended_query As Boolean

#Region "Enumerators"

  Public Enum ENUM_MASSIVE_SEARCH_TYPE
    ID_ACCOUNT = 0
    CARD_NUMBER = 1
  End Enum

#End Region ' Enumerators

#Region "Properties"

  Public Overloads Property Account() As String
    Get
      If ef_account_id.Enabled Then
        Return Me.ef_account_id.TextValue
      Else
        Return ""
      End If
    End Get

    Set(ByVal Value As String)
      Me.ef_account_id.TextValue = Value
      Call account_or_trackdata_EntryFieldValueChanged()
    End Set
  End Property


  Public Overloads Property AccountText() As String
    Get
      Return Me.ef_account_id.TextValue
    End Get
    Set(ByVal value As String)
      Me.ef_account_id.TextValue = value
    End Set
  End Property ' AccountText

  Public Overloads Property TrackData() As String
    Get
      If Me.ef_card_track.Enabled Then
        Return Me.ef_card_track.TextValue
      Else
        Return ""
      End If
    End Get

    Set(ByVal value As String)
      Me.ef_card_track.TextValue = value
    End Set
  End Property

  Public Overloads Property Holder() As String
    Get
      If Me.ef_card_holder.Enabled Then
        Return Me.ef_card_holder.TextValue
      Else
        Return ""
      End If
    End Get

    Set(ByVal value As String)
      Me.ef_card_holder.TextValue = value
    End Set
  End Property

  Public Overloads ReadOnly Property Date_From() As String
    Get
      ' Date created
      If Me.DateFromIsChecked Then
        Return GUI_FormatDate(Me.dtp_from.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      Else
        Return ""
      End If
    End Get

  End Property

  Public Overloads ReadOnly Property Date_To() As String
    Get
      If Me.DateToIsChecked Then
        Return GUI_FormatDate(Me.dtp_to.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      Else
        Return ""
      End If
    End Get
  End Property

  Public Overloads ReadOnly Property Date_Last_From() As String
    Get
      If Me.DateLastFromIsChecked Then
        Return GUI_FormatDate(Me.dtp_last_from.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      End If

      Return ""
    End Get
  End Property

  Public Overloads ReadOnly Property Date_Last_To() As String
    Get
      If Me.DateLastToIsChecked Then
        Return GUI_FormatDate(Me.dtp_last_to.Value, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      End If

      Return ""
    End Get

  End Property

  Public Overloads ReadOnly Property Points_From() As String
    Get
      'Points
      If Me.ef_points_from.Value <> String.Empty Then
        Return Me.ef_points_from.Text
      Else
        Return ""
      End If
    End Get
  End Property

  Public Overloads ReadOnly Property Points_To() As String
    Get
      'Points
      If Me.ef_points_to.Value <> String.Empty Then
        Return Me.ef_points_to.Text
      Else
        Return ""
      End If
    End Get
  End Property

  Public Overloads ReadOnly Property Level() As String
    Get
      Return GetLevelsSelected(False)
    End Get

  End Property

  Public Overloads ReadOnly Property Gender() As String
    Get
      Return GetGendersSelected(False)
    End Get

  End Property

  Public Overloads ReadOnly Property Only_Anonymous() As String
    Get
      Return IIf(Me.chk_only_anonymous.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279))
    End Get
  End Property

  Public Overloads ReadOnly Property Balance() As String
    Get
      Dim _balance As String

      _balance = String.Empty

      'Balance
      If Me.BalanceRedeemableIsChecked Then
        _balance = Me.chk_balance_redeemable.Text
      End If

      'Extended search
      If Me.BalanceNonRedeemableIsChecked Then
        If _balance.Length > 0 Then
          _balance = _balance & ", "
        End If

        _balance = _balance & Me.chk_balance_non_redeemable.Text
      End If

      Return _balance
    End Get

  End Property

  Public Overloads ReadOnly Property State() As String
    Get
      Dim _state As String

      _state = String.Empty

      'Card(state)
      If Me.chk_card_blocked.Checked Then
        _state = GLB_NLS_GUI_INVOICING.GetString(238) ' Locked
      End If

      If Me.chk_card_not_blocked.Checked Then
        If _state.Length > 0 Then
          _state = _state & ", "
        End If
        _state = _state & GLB_NLS_GUI_INVOICING.GetString(239)
      End If

      Return _state
    End Get
  End Property

  Public Overloads Property BirthDate() As Date
    Get
      Return Me.dtp_birth_date.Value
    End Get

    Set(ByVal Value As Date)
      Me.dtp_birth_date.Value = New Date(Value.Year, Value.Month, Value.Day, 0, 0, 0)
    End Set
  End Property

  Public Overloads Property WeddingDate() As Date
    Get
      Return Me.dtp_wedding_date.Value
    End Get

    Set(ByVal value As Date)
      Me.dtp_wedding_date.Value = New Date(value.Year, value.Month, value.Day, 0, 0, 0)
    End Set
  End Property

  Public Overloads Property Telephone() As String
    Get
      If Me.TelephoneIsVisible Then
        Return Me.ef_telephone.TextValue
      Else
        Return ""
      End If
    End Get
    Set(ByVal value As String)
      Me.ef_telephone.TextValue = value
    End Set
  End Property

  Public Overloads Property Vip() As Boolean
    Get
      If Me.chk_vip.Enabled Then
        Return Me.chk_vip.Checked
      Else
        Return ""
      End If
    End Get
    Set(ByVal value As Boolean)
      Me.chk_vip.Checked = value
    End Set
  End Property

  Public Overloads Property DisabledHolder() As Boolean
    Get
      Return m_disabled_holder
    End Get

    Set(ByVal Disabled As Boolean)
      m_disabled_holder = Disabled
      ef_card_holder.Enabled = Not m_disabled_holder
    End Set
  End Property

  ' Get Properties of Controls
  Public Overloads ReadOnly Property BirthDateIsVisible() As Boolean
    Get
      Return Me.dtp_birth_date.Visible
    End Get
  End Property

  Public Overloads ReadOnly Property BirthDateIsChecked() As Boolean
    Get
      If dtp_birth_date.Enabled Then
        Return Me.dtp_birth_date.Checked
      Else
        Return False
      End If
    End Get
  End Property

  Public Overloads ReadOnly Property WeddingDateIsVisible() As Boolean
    Get
      Return Me.dtp_wedding_date.Visible
    End Get
  End Property

  Public Overloads ReadOnly Property WeddingDateIsChecked() As Boolean
    Get
      If dtp_wedding_date.Enabled Then
        Return Me.dtp_wedding_date.Checked
      Else
        Return False
      End If
    End Get
  End Property

  Public Overloads ReadOnly Property TelephoneIsVisible() As Boolean
    Get
      Return Me.ef_telephone.Visible
    End Get
  End Property

  Public Overloads ReadOnly Property LastActivityActiveIsChecked() As Boolean
    Get
      Return Me.rb_last_activity_active.Checked
    End Get
  End Property

  Public Overloads ReadOnly Property LastActivityInactiveIsChecked() As Boolean
    Get
      Return Me.rb_last_activity_inactive.Checked
    End Get
  End Property

  Public Overloads ReadOnly Property AllChecked() As Boolean
    Get
      Return Me.rb_all.Checked
    End Get
  End Property

  Public Overloads ReadOnly Property LastActivityByDateIsChecked() As Boolean
    Get
      Return Me.rb_last_activity_by_date.Checked
    End Get
  End Property

  Public Overloads ReadOnly Property BalanceRedeemableIsChecked() As Boolean
    Get
      Return Me.chk_balance_redeemable.Checked
    End Get
  End Property

  Public Overloads ReadOnly Property OnlyAnonymousIsChecked() As Boolean
    Get
      Return Me.chk_only_anonymous.Checked
    End Get

  End Property

  Public Overloads ReadOnly Property BalanceNonRedeemableIsChecked() As Boolean
    Get
      Return Me.chk_balance_non_redeemable.Checked
    End Get
  End Property

  Public Overloads ReadOnly Property DateFromIsChecked() As Boolean
    Get
      Return Me.dtp_from.Checked
    End Get
  End Property

  Public Overloads ReadOnly Property DateToIsChecked() As Boolean
    Get
      Return Me.dtp_to.Checked
    End Get
  End Property

  Public Overloads ReadOnly Property DateLastFromIsChecked() As Boolean
    Get
      Return Me.dtp_last_from.Checked
    End Get
  End Property

  Public Overloads ReadOnly Property DateLastToIsChecked() As Boolean
    Get
      Return Me.dtp_last_to.Checked
    End Get
  End Property

  'CCG 02-SEP-2013: Massive Search
  Public Overloads Property ShowMassiveSearch() As Boolean
    Get
      Return btn_massive_search.Visible
    End Get

    Set(ByVal value As Boolean)
      btn_massive_search.Visible = value
    End Set

  End Property 'ShowMassiveSearch

  Public Overloads Property MassiveSearchNumbers() As String
    Get
      Return m_accounts_massive_search
    End Get
    Set(ByVal value As String)
      m_accounts_massive_search = value
    End Set
  End Property ' MassiveSearchNumbers

  Public Property MassiveSearchNumbersToEdit() As String
    Get
      Return m_accounts_massive_search_to_edit
    End Get
    Set(ByVal value As String)
      m_accounts_massive_search_to_edit = value
    End Set
  End Property ' MassiveSearchNumbersToEdit

  Public Property MassiveSearchType() As Integer
    Get
      Return m_accounts_massive_search_type
    End Get
    Set(ByVal value As Integer)
      m_accounts_massive_search_type = value
    End Set
  End Property ' MassiveSearchType

  Public Overloads Property Anon() As Boolean
    Get
      Return Me.chk_anon.Checked
    End Get

    Set(ByVal Value As Boolean)
      Me.chk_anon.Checked = Value
    End Set
  End Property

  Public Sub ShowAnonFilter()
    Me.chk_anon.Visible = True
  End Sub

#End Region

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControls()

  End Sub

  Private Sub InitControls()

    ' In TITO Mode set account filter not extended to hide virtual accounts
    If WSI.Common.TITO.Utils.IsTitoMode() Then
      Me.InitExtendedQuery(False)
    End If

    Me.gb_account.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1564)
    Me.gp_other.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2346)

    Me.ef_account_id.Text = GLB_NLS_GUI_STATISTICS.GetString(207)
    Me.ef_card_track.Text = GLB_NLS_GUI_STATISTICS.GetString(209)
    Me.ef_card_holder.Text = GLB_NLS_GUI_STATISTICS.GetString(210)
    Me.chk_anon.Text = GLB_NLS_GUI_STATISTICS.GetString(314)
    Me.chk_vip.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802)

    Call Me.ef_account_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 10)
    Call Me.ef_card_track.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TRACK_DATA, TRACK_DATA_LENGTH_WIN)
    Call Me.ef_card_holder.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    ' Creation Date
    Me.gb_date.Text = GLB_NLS_GUI_INVOICING.GetString(468)
    Me.dtp_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Last Activity Date
    Me.gb_last_activity.Text = GLB_NLS_GUI_INVOICING.GetString(467)
    Me.dtp_last_from.Text = GLB_NLS_GUI_INVOICING.GetString(202)
    Me.dtp_last_to.Text = GLB_NLS_GUI_INVOICING.GetString(203)
    Me.dtp_last_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_last_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.rb_last_activity_active.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2424)
    Me.rb_last_activity_inactive.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2425)
    Me.rb_last_activity_by_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2427)

    Me.m_days_without_activity = WSI.Common.GeneralParam.GetInt32("PlayerTracking", "MaxDaysWithoutActivity", 90)

    If Me.m_days_without_activity = 0 Then
      Me.m_days_without_activity = 90
    End If

    Me.lbl_days_active.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2428, m_days_without_activity.ToString())

    Me.rb_all.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)

    ' Gender
    Me.gb_gender.Text = GLB_NLS_GUI_INVOICING.GetString(403)
    Me.chk_gender_male.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(521)
    Me.chk_gender_female.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(522)

    ' Level
    Me.gb_level.Text = GLB_NLS_GUI_INVOICING.GetString(381)
    Me.chk_level_01.Text = GeneralParam.GetString("PlayerTracking", "Level01.Name", GLB_NLS_GUI_CONFIGURATION.GetString(289))
    Me.chk_level_02.Text = GeneralParam.GetString("PlayerTracking", "Level02.Name", GLB_NLS_GUI_CONFIGURATION.GetString(290))
    Me.chk_level_03.Text = GeneralParam.GetString("PlayerTracking", "Level03.Name", GLB_NLS_GUI_CONFIGURATION.GetString(291))
    Me.chk_level_04.Text = GeneralParam.GetString("PlayerTracking", "Level04.Name", GLB_NLS_GUI_CONFIGURATION.GetString(332))

    ' Only anonymous
    Me.chk_only_anonymous.Text = GLB_NLS_GUI_CONFIGURATION.GetString(66)

    ' State
    Me.fra_card_state.Text = GLB_NLS_GUI_INVOICING.GetString(238)
    ' Blocked
    Me.chk_card_blocked.Text = GLB_NLS_GUI_INVOICING.GetString(238)
    ' Not Blocked
    Me.chk_card_not_blocked.Text = GLB_NLS_GUI_INVOICING.GetString(239)

    ' Balance - Search type
    Me.fra_search_type.Text = GLB_NLS_GUI_INVOICING.GetString(240)
    ' Held > 0
    Me.chk_balance_redeemable.Text = GLB_NLS_GUI_INVOICING.GetString(283)
    ' Redeemable > 0
    Me.chk_balance_non_redeemable.Text = GLB_NLS_GUI_INVOICING.GetString(284)

    ' Points
    Me.gb_points.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(205) ' Points
    Me.ef_points_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(297) '  From
    Me.ef_points_from.IsReadOnly = False
    Call Me.ef_points_to.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8)
    Me.ef_points_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(298) '  To
    Me.ef_points_to.IsReadOnly = False
    Call Me.ef_points_from.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8)

    Me.chk_anon.Visible = False
    Me.ShowMassiveSearch = True

    ' Advanced search
    Me.m_show_extended = False

    ShowHideAdvancedSearch()

  End Sub

  Public Sub SetPosition(ByVal Location As Drawing.Point)
    Me.Location = Location
  End Sub

  Public Sub Clear()

    Me.ef_account_id.TextValue = ""
    Me.ef_card_track.TextValue = ""
    Me.ef_card_holder.TextValue = ""
    Me.ef_telephone.TextValue = ""
    Me.dtp_birth_date.Checked = False
    Me.dtp_wedding_date.Checked = False
    Me.chk_anon.Checked = False
    Me.chk_vip.Checked = False

    Call account_or_trackdata_EntryFieldValueChanged()
  End Sub


  ' PURPOSE : Create the necesary SQL IN condition query to do the account massive search filter
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT: String with the query
  '
  ' RETURNS:
  '
  Public Function GetMassiveSearchFilterSQL() As String
    Dim _sql_where_account As String

    _sql_where_account = Me.GetFilterSQL()

    If Not String.IsNullOrEmpty(Me.MassiveSearchNumbers) Then
      If Not String.IsNullOrEmpty(_sql_where_account) Then
        _sql_where_account &= " AND "
      End If
      _sql_where_account &= " AC_ACCOUNT_ID IN ("
      _sql_where_account &= Me.MassiveSearchNumbers.Replace(SEPARATOR, ",")
      _sql_where_account &= ")"
    End If

    Return _sql_where_account
  End Function ' GetMassiveSearchFilterSQL

  ' PURPOSE : Build WHERE part for SQL query from field values
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '         - String: WHERE sentence (require FROM or INNER with dbo.accounts table)
  '
  Public Function GetFilterSQL() As String

    Dim _str_where As String
    Dim _str_where_account As String
    Dim _internal_card_id As String
    Dim _str_aux1 As String
    Dim _rc As Boolean
    Dim _card_type As Integer

    _str_where = ""
    _str_where_account = ""

    ' Filter Card Id
    If Me.chk_anon.Checked Then
      _str_where = " AC_ACCOUNT_ID IS NULL "
    End If

    If String.IsNullOrEmpty(Me.MassiveSearchNumbers) Then

      If Me.ef_account_id.TextValue <> "" And ef_account_id.Enabled Then
        _str_where = " AC_ACCOUNT_ID = " & Me.ef_account_id.Value
      End If

      ' Filter Track
      If Me.ef_card_track.TextValue <> "" And ef_card_track.Enabled Then

        ' add and and
        If _str_where <> "" Then
          _str_where += " AND "
        End If

        ' First convert external (encrypted) card number into the internal id used in the database
        _internal_card_id = ""
        If Me.ef_card_track.TextValue.Length = TRACK_DATA_LENGTH_WIN Then
          _rc = WSI.Common.CardNumber.TrackDataToInternal(Me.ef_card_track.TextValue, _internal_card_id, _card_type)
          If _rc = True Then
            _str_where += " AC_TRACK_DATA = '" & _internal_card_id & "'"
          Else
            _str_where += " AC_TRACK_DATA = '" & Me.ef_card_track.TextValue & "'"
          End If
        Else
          _str_where += " AC_TRACK_DATA = '" & Me.ef_card_track.TextValue & "'"
        End If

      End If

      ' Filter Vip's
      If Me.chk_vip.Checked Then
        ' add and 
        If _str_where <> "" Then
          _str_where += " AND "
        End If

        _str_where += " AC_HOLDER_IS_VIP = 1"
      End If

    End If

    ' Filter Holder Name
    If Me.ef_card_holder.TextValue <> "" And Me.ef_card_holder.Enabled Then

      ' add and and
      If _str_where <> "" Then
        _str_where += " AND "
      End If

      _str_aux1 = UCase(Me.ef_card_holder.TextValue).Replace("'", "''")
      _str_where += "( UPPER(AC_HOLDER_NAME) LIKE '" & _str_aux1 & "%' OR"
      _str_where += " UPPER(AC_HOLDER_NAME) LIKE '% " & _str_aux1 & "%') "

    End If

    ' Filter Birth Date
    If Me.dtp_birth_date.Checked And Me.dtp_birth_date.Enabled Then

      ' add and and
      If _str_where <> "" Then
        _str_where += " AND "
      End If

      _str_where += "AC_HOLDER_BIRTH_DATE = " & GUI_FormatDateDB(Me.dtp_birth_date.Value)

    End If

    ' Filter Wedding Date
    If Me.dtp_wedding_date.Checked And Me.dtp_wedding_date.Enabled Then

      ' add and and
      If _str_where <> "" Then
        _str_where += " AND "
      End If

      _str_where += "AC_HOLDER_WEDDING_DATE = " & GUI_FormatDateDB(Me.dtp_wedding_date.Value)

    End If

    ' Filter Telephone
    If Me.ef_telephone.TextValue <> "" And Me.ef_telephone.Enabled Then

      ' add and and
      If _str_where <> "" Then
        _str_where += " AND "
      End If
      _str_aux1 = UCase(Me.ef_telephone.TextValue).Replace("'", "''")
      _str_where += "(REPLACE(AC_HOLDER_PHONE_NUMBER_01, ' ', '') LIKE REPLACE('%" + _str_aux1 + "%', ' ', '') OR REPLACE(AC_HOLDER_PHONE_NUMBER_02, ' ', '') LIKE REPLACE('%" + _str_aux1 + "%', ' ', ''))"
    End If

    If TITO.Utils.IsTitoMode Then

      ' If not in extended query mode and account id is blank then filter to don't show virtual accounts
      If Not m_extended_query And ef_account_id.Value = String.Empty Then
        If _str_where <> "" Then
          _str_where += " AND "
        End If

        _str_where += "(AC_TYPE NOT IN(" & AccountType.ACCOUNT_VIRTUAL_CASHIER & ", " & AccountType.ACCOUNT_VIRTUAL_TERMINAL & "))"
      End If

    End If

    Return _str_where

  End Function

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Public Sub SetDefaultValues()
    Dim _initial_time As Date

    _initial_time = WSI.Common.Misc.TodayOpening()

    Me.dtp_from.Value = _initial_time
    Me.dtp_from.Checked = False
    Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
    Me.dtp_to.Checked = False

    Me.dtp_last_from.Value = _initial_time.AddMonths(-1)
    Me.dtp_last_to.Value = _initial_time.AddDays(-1)
    Me.dtp_last_to.Checked = False

    Me.chk_card_blocked.Checked = False
    Me.chk_card_not_blocked.Checked = False
    Me.chk_balance_redeemable.Checked = False
    Me.chk_balance_non_redeemable.Checked = False
    Me.Clear()

    Me.rb_last_activity_active.Checked = True

    Me.chk_level_01.Checked = False
    Me.chk_level_02.Checked = False
    Me.chk_level_03.Checked = False
    Me.chk_level_04.Checked = False

    Me.chk_gender_male.Checked = False
    Me.chk_gender_female.Checked = False

    Me.chk_only_anonymous.Checked = False

    Me.ef_points_from.Value = String.Empty
    Me.ef_points_to.Value = String.Empty

  End Sub ' SetDefaultValues


  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the main SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Public Function GetSqlWhere() As String

    Dim _str_where As String = ""
    Dim _str_where_account As String = ""
    Dim _with_index As String
    Dim _date As Date


    _with_index = ""
    _date = WSI.Common.Misc.TodayOpening()

    If Me.BirthDateIsChecked Then
      _with_index = " WITH(INDEX(IX_ac_holder_birth_date)) "
    End If

    ' In TITO Mode, if the user introduces an account id, system will ignore ac_type to show information.
    If Not WSI.Common.TITO.Utils.IsTitoMode() OrElse String.IsNullOrEmpty(Me.Account) Then
      _str_where = _with_index & " AC_TYPE = " & CARD_TYPE
    End If

    _str_where_account = Me.GetFilterSQL()

    If _str_where_account <> "" Then
      _str_where = _str_where_account
    End If

    If String.IsNullOrEmpty(Me.Account) AndAlso Not Me.DisabledHolder Then

      ' Filter Dates - Creation
      If Me.dtp_from.Checked = True Then
        If _str_where <> String.Empty Then
          _str_where &= " AND "
        End If

        _str_where = _str_where & " (AC_CREATED >= " & GUI_FormatDateDB(Me.dtp_from.Value) & ") "
      End If

      If Me.dtp_to.Checked = True Then
        If _str_where <> String.Empty Then
          _str_where &= " AND "
        End If

        _str_where = _str_where & " (AC_CREATED < " & GUI_FormatDateDB(dtp_to.Value) & ") "
      End If

      _date = _date.AddDays(-1 * m_days_without_activity)
      ' Filter Dates - Last Activity

      If Me.rb_last_activity_active.Checked = True Then
        If _str_where <> String.Empty Then
          _str_where &= " AND "
        End If
        _str_where = _str_where & " (AC_LAST_ACTIVITY >= " & GUI_FormatDateDB(_date) & ") "
      ElseIf Me.rb_last_activity_inactive.Checked = True Then
        If _str_where <> String.Empty Then
          _str_where &= " AND "
        End If
        _str_where = _str_where & " (AC_LAST_ACTIVITY < " & GUI_FormatDateDB(_date) & ") "
      ElseIf Me.rb_last_activity_by_date.Checked Then
        If _str_where <> String.Empty Then
          _str_where &= " AND "
        End If
        _str_where = _str_where & " (AC_LAST_ACTIVITY >= " & GUI_FormatDateDB(dtp_last_from.Value) & ") "
        If Me.dtp_last_to.Checked = True Then
          _str_where = _str_where & " AND (AC_LAST_ACTIVITY < " & GUI_FormatDateDB(dtp_last_to.Value) & ") "
        End If
      End If

      ' Level
      If Me.chk_only_anonymous.Checked Then

        ' Only anonym accounts
        _str_where = _str_where & " AND ( AC_HOLDER_LEVEL = 0 )"
      Else
        ' Any kind of accounts
        If Me.chk_level_01.Checked Or _
           Me.chk_level_02.Checked Or _
           Me.chk_level_03.Checked Or _
           Me.chk_level_04.Checked Then

          _str_where = _str_where & " AND AC_HOLDER_LEVEL IN (" & GetLevelsSelected() & ") "
        End If

        ' Gender
        If Me.chk_gender_male.Checked Or _
           Me.chk_gender_female.Checked Then

          _str_where = _str_where & " AND AC_HOLDER_GENDER IN (" & GetGendersSelected() & ") "
        End If
      End If

      ' Filter State - Blocked 
      If Me.chk_card_blocked.Checked And Not Me.chk_card_not_blocked.Checked Then
        _str_where = _str_where & " AND (AC_BLOCKED = 1) "
      End If

      ' Filter State - Not Blocked 
      If Me.chk_card_not_blocked.Checked And Not Me.chk_card_blocked.Checked Then
        _str_where = _str_where & " AND (AC_BLOCKED = 0) "
      End If

      ' Balance search type - Total > 0 in any case
      If Me.chk_balance_redeemable.Checked Or Me.chk_balance_non_redeemable.Checked Then
        _str_where += " AND ( AC_BALANCE > 0 )"
      End If

      ' Points filter
      If Me.ef_points_from.Value <> String.Empty Then
        _str_where += " AND AC_HOLDER_LEVEL > 0 AND FLOOR(DBO.GETBUCKETVALUE(" & Buckets.BucketType.RedemptionPoints & ",AC_ACCOUNT_ID)) >= " & Me.ef_points_from.Value   '' Integer value don't work!!!
      End If

      If Me.ef_points_to.Value <> String.Empty Then
        _str_where += " AND AC_HOLDER_LEVEL > 0 AND FLOOR(DBO.GETBUCKETVALUE(" & Buckets.BucketType.RedemptionPoints & ",AC_ACCOUNT_ID)) <= " & Me.ef_points_to.Value
      End If

    End If

    Return IIf(_str_where = String.Empty, _str_where, " WHERE " & _str_where)

  End Function ' GetSqlWhere

  ' PURPOSE : Initializes fields for extended search from GENERAL_PARAMS
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  ' NOTES:
  '     - Check if params are active and show the fields in the control
  '
  Public Sub InitExtendedSearch()

    Dim _now As DateTime

    _now = WSI.Common.WGDB.Now

    Me.dtp_birth_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1651)
    Me.dtp_wedding_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1652)
    Me.ef_telephone.Text = GLB_NLS_GUI_CONTROLS.GetString(360)

    Me.ef_telephone.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 20)

    Me.dtp_birth_date.SetRange(New DateTime(_now.Year - SUBTRACT_TO_INITIAL_YEAR, 1, 1), _now)
    Me.dtp_birth_date.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Me.dtp_birth_date.Checked = False

    Me.dtp_wedding_date.SetRange(New DateTime(_now.Year - SUBTRACT_TO_INITIAL_YEAR, 1, 1), _now)
    Me.dtp_wedding_date.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Me.dtp_wedding_date.Checked = False

  End Sub ' InitExtendedFields

  ' PURPOSE : Validates format of all fields
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '         - TRUE:   If the format of all fields is correct
  '         - FALSE:  If there is a field that has incorrect format
  '
  Public Function ValidateFormat() As Boolean
    If String.IsNullOrEmpty(Me.MassiveSearchNumbers) Then
      If Me.ef_account_id.Enabled AndAlso Not Me.ef_account_id.Value = "" AndAlso Not Me.ef_account_id.ValidateFormat() Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
              mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_account_id.Text)
        Me.ef_account_id.Focus()

        Return False
      End If

      If Me.ef_card_track.Enabled AndAlso Not Me.ef_card_track.Value = "" AndAlso Not Me.ef_card_track.ValidateFormat() Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1655), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
              mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        Me.ef_card_track.Focus()

        Return False
      End If
    End If

    ' Creation date selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    ' Last Activity date selection 
    If Me.dtp_last_to.Checked Then
      If Me.dtp_last_from.Value > Me.dtp_last_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_last_to.Focus()

        Return False
      End If
    End If

    If Me.ef_points_from.Value <> String.Empty And Me.ef_points_to.Value <> String.Empty Then
      If CInt(Me.ef_points_from.Value) > CInt(Me.ef_points_to.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1896), _
        ENUM_MB_TYPE.MB_TYPE_WARNING, _
        ENUM_MB_BTN.MB_BTN_OK, _
        ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
        GLB_NLS_GUI_PLAYER_TRACKING.GetString(377), _
        GLB_NLS_GUI_PLAYER_TRACKING.GetString(378))

        Call Me.ef_points_from.Focus()

        Return False
      End If
    End If


    Return True

  End Function


  ' PURPOSE : Create SQL query to do the temporary table and insert rows for each account number
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT: String with the query
  '
  ' RETURNS:
  '
  Public Function CreateAndInsertAccountData() As String
    Dim _account_numbers As String()
    Dim _idx_numbers As Integer
    Dim _str_sql As System.Text.StringBuilder
    Dim _track_data As String

    _track_data = String.Empty
    _str_sql = New System.Text.StringBuilder()
    _account_numbers = Me.MassiveSearchNumbers.Split()

    ' create table    
    _str_sql.AppendLine("CREATE TABLE #NUMBERS_TEMP (")
    _str_sql.AppendLine("NUMBER NVARCHAR(20) )")

    ' insert(rows)
    _str_sql.AppendLine("INSERT INTO #NUMBERS_TEMP ")
    _str_sql.AppendLine(" (NUMBER) ")

    If Me.MassiveSearchType = ENUM_MASSIVE_SEARCH_TYPE.ID_ACCOUNT Then
      _str_sql.AppendLine(" SELECT '" & _account_numbers(0) & "'")
    Else
      Call WSI.Common.CardNumber.TrackDataToInternal(_account_numbers(0), _track_data, 0)
      _str_sql.AppendLine(" SELECT '" & _track_data & "'")
    End If

    For _idx_numbers = 1 To _account_numbers.Length() - 1
      _track_data = String.Empty
      _str_sql.AppendLine(" UNION ALL ")

      If Me.MassiveSearchType = ENUM_MASSIVE_SEARCH_TYPE.ID_ACCOUNT Then
        _str_sql.AppendLine(" SELECT '" & _account_numbers(_idx_numbers) & "'")
      Else
        Call WSI.Common.CardNumber.TrackDataToInternal(_account_numbers(_idx_numbers), _track_data, 0)
        _str_sql.AppendLine(" SELECT '" & _track_data & "'")
      End If
    Next

    Return _str_sql.ToString()
  End Function ' CreateAndInsertAccountData

  ' PURPOSE : Create the necesary SQL INNER JOIN query to do the account massive search filter
  '
  '  PARAMS:
  '     - INPUT: ConditionField as String with the field to do the join condition
  '
  '     - OUTPUT: String with the query
  '
  ' RETURNS:
  '
  Public Function GetInnerJoinAccountMassiveSearch(ByVal ConditionField As String) As String
    Dim _str_sql As System.Text.StringBuilder

    _str_sql = New System.Text.StringBuilder()

    _str_sql.AppendLine(" INNER JOIN (                                                               ")
    _str_sql.AppendLine("              SELECT  NUMBER Collate SQL_Latin1_General_CP1_CI_AS AS NUMBER ")
    _str_sql.AppendLine("                FROM  #NUMBERS_TEMP                                         ")
    _str_sql.AppendLine("            ) #NUMBERS_TEMP ON #NUMBERS_TEMP.NUMBER = " & ConditionField)

    Return _str_sql.ToString()
  End Function ' GetInnerJoinAccountMassiveSearch

  ' PURPOSE : Set value in "m_extended_query"
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub InitExtendedQuery(ByVal ExtendedQuery As Boolean)

    m_extended_query = ExtendedQuery

  End Sub ' InitExtendedQuery


  ' PURPOSE : Enable/Disable the filters
  '
  '  PARAMS :
  '     - INPUT  : Boolean indicating if it must enable (true) or disable (false) the filters
  '
  '     - OUTPUT :
  Private Sub EnableFilters(ByVal EnableFilters As Boolean)
    Me.gb_last_activity.Enabled = EnableFilters
    Me.gb_date.Enabled = EnableFilters
    Me.gb_level.Enabled = EnableFilters
    Me.gb_points.Enabled = EnableFilters
    Me.gb_gender.Enabled = EnableFilters
    Me.fra_card_state.Enabled = EnableFilters
    Me.fra_search_type.Enabled = EnableFilters
    Me.chk_only_anonymous.Enabled = EnableFilters

  End Sub ' EnableFilters

  ' PURPOSE : Set PrintData account filters
  '
  '  PARAMS :
  '     - INPUT  : Form PrintData
  '
  '     - OUTPUT :
  '
  Public Sub ReportFilter(ByRef PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), Me.Account)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), Me.TrackData)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), Me.Holder)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(468) & " " & GLB_NLS_GUI_INVOICING.GetString(202), Me.Date_From)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(468) & " " & GLB_NLS_GUI_INVOICING.GetString(203), Me.Date_To)

    If Me.LastActivityActiveIsChecked Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(467) & " ", GLB_NLS_GUI_PLAYER_TRACKING.GetString(2424))
    End If
    If Me.LastActivityInactiveIsChecked Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(467) & " ", GLB_NLS_GUI_PLAYER_TRACKING.GetString(2425))
    End If
    If Me.AllChecked Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(467) & " ", GLB_NLS_GUI_PLAYER_TRACKING.GetString(1671))
    End If
    If Me.LastActivityByDateIsChecked Then
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(467) & " " & GLB_NLS_GUI_INVOICING.GetString(202), Me.Date_Last_From)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(467) & " " & GLB_NLS_GUI_INVOICING.GetString(203), Me.Date_Last_To)
    End If

    PrintData.SetFilter("", "", True)

    If Me.OnlyAnonymousIsChecked Then
      PrintData.SetFilter(GLB_NLS_GUI_CONFIGURATION.GetString(66), Me.Only_Anonymous)
    Else
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(403), Me.Gender)
      PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(381), Me.Level)
    End If
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(240), Me.Balance)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(238), Me.State)

    If Me.OnlyAnonymousIsChecked Then
      PrintData.SetFilter("", "", True)
    End If
    PrintData.SetFilter("", "", True)

    If Me.BirthDateIsVisible Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1651), Me.BirthDate)
    End If
    If Me.WeddingDateIsVisible Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1652), Me.WeddingDate)
    End If
    If Me.TelephoneIsVisible Then
      PrintData.SetFilter(GLB_NLS_GUI_CONTROLS.GetString(360), Me.Telephone)
    End If

    ' Points from - to
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(377), Me.Points_From)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(378), Me.Points_To)

  End Sub

  ' PURPOSE: Show/hide controls. 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Sub ShowHideAdvancedSearch()

    Me.lbl_advanced_search.Text = IIf(Me.m_show_extended, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4934), GLB_NLS_GUI_PLAYER_TRACKING.GetString(4933))

    Me.gp_other.Visible = Me.m_show_extended
    Me.gb_last_activity.Visible = Me.m_show_extended
    Me.gb_date.Visible = Me.m_show_extended
    Me.fra_card_state.Visible = Me.m_show_extended
    Me.gb_gender.Visible = Me.m_show_extended
    Me.fra_search_type.Visible = Me.m_show_extended
    Me.gb_level.Visible = Me.m_show_extended
    Me.gb_points.Visible = Me.m_show_extended

  End Sub

  ' PURPOSE: Get Level list for selected levels
  '          Format list to build query: X, X, X
  '          X is the Id if GetIds = True
  '          otherwise, X is the Name of the control
  '  PARAMS:
  '     - INPUT:
  '           - GetIds As Boolean
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Private Function GetLevelsSelected(Optional ByVal GetIds As Boolean = True) As String
    Dim _list_type As String

    _list_type = ""

    If Me.chk_level_01.Checked Then
      _list_type = IIf(GetIds, "1", Me.chk_level_01.Text)
    End If

    If Me.chk_level_02.Checked Then
      If _list_type.Length > 0 Then
        _list_type = _list_type & ", "
      End If
      _list_type = _list_type & IIf(GetIds, "2", Me.chk_level_02.Text)
    End If

    If Me.chk_level_03.Checked Then
      If _list_type.Length > 0 Then
        _list_type = _list_type & ", "
      End If
      _list_type = _list_type & IIf(GetIds, "3", Me.chk_level_03.Text)
    End If

    If Me.chk_level_04.Checked Then
      If _list_type.Length > 0 Then
        _list_type = _list_type & ", "
      End If
      _list_type = _list_type & IIf(GetIds, "4", Me.chk_level_04.Text)
    End If

    Return _list_type
  End Function ' GetLevelsSelected


  ' PURPOSE: Get Gender list for selected genders
  '          Format list to build query: X, X, X
  '          X is the Id if GetIds = True
  '          otherwise, X is the Name of the control
  '  PARAMS:
  '     - INPUT:
  '           - GetIds As Boolean
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - String
  Private Function GetGendersSelected(Optional ByVal GetIds As Boolean = True) As String
    Dim _list_type As String

    _list_type = ""
    If Me.chk_gender_male.Checked Then
      _list_type = IIf(GetIds, "1", Me.chk_gender_male.Text)
    End If
    If Me.chk_gender_female.Checked Then
      If _list_type.Length > 0 Then
        _list_type = _list_type & ", "
      End If
      _list_type = _list_type & IIf(GetIds, "2", Me.chk_gender_female.Text)
    End If

    Return _list_type
  End Function ' GetGendersSelected


  ' PURPOSE : Create the SQL DROP query to delete the temporay table for account massive search
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT: String with the query
  '
  ' RETURNS:
  '
  Public Function DropTableAccountMassiveSearch() As String
    Return " DROP TABLE #NUMBERS_TEMP"
  End Function ' DropTableAccountMassiveSearch

  ' PURPOSE : Check if chk_anon is checked and enable/disable other controls 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub chk_anon_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    If Me.chk_anon.Checked Then
      Me.ef_account_id.Enabled = False
      Me.ef_card_holder.Enabled = False
      Me.ef_card_track.Enabled = False
      Me.chk_vip.Enabled = False
      Me.dtp_birth_date.Enabled = False
      Me.dtp_wedding_date.Enabled = False
      Me.ef_telephone.Enabled = False
    Else
      Call account_or_trackdata_EntryFieldValueChanged()
    End If

  End Sub

  ' PURPOSE : Check if not empty ef_account_id and ef_card_track values and enable/disable other controls 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub account_or_trackdata_EntryFieldValueChanged()

    Dim _enabled As Boolean

    If ef_account_id.TextValue <> "" Or ef_card_track.TextValue <> "" Then
      Me.ef_account_id.Enabled = (Me.ef_account_id.TextValue <> "")
      Me.ef_card_track.Enabled = (Me.ef_card_track.TextValue <> "")
      Me.btn_massive_search.Enabled = False

      _enabled = False
    Else
      Me.ef_account_id.Enabled = True
      Me.ef_account_id.IsReadOnly = False
      Me.ef_card_track.IsReadOnly = False
      Me.MassiveSearchNumbers = String.Empty
      Me.MassiveSearchNumbersToEdit = String.Empty
      Me.MassiveSearchType = uc_account_sel.ENUM_MASSIVE_SEARCH_TYPE.ID_ACCOUNT
      Me.ef_card_track.Enabled = True
      Me.btn_massive_search.Enabled = True

      _enabled = True
    End If

    Me.ef_card_holder.Enabled = _enabled And Not DisabledHolder
    Me.dtp_birth_date.Enabled = _enabled
    Me.dtp_wedding_date.Enabled = _enabled
    Me.ef_telephone.Enabled = _enabled
    Me.chk_vip.Enabled = _enabled

    RaiseEvent OneAccountFilterChanged()

  End Sub

  ' PURPOSE : Initialize the form and change the textbox 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub btn_massive_search_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_massive_search.Click
    Dim _frm_account_massive_search
    Dim _numbers_of_accounts As Integer

    _numbers_of_accounts = 0
    _frm_account_massive_search = New frm_account_massive_search(Me)

    Call _frm_account_massive_search.ShowDialog()

    If Not String.IsNullOrEmpty(MassiveSearchNumbers) Then
      _numbers_of_accounts = Strings.Split(MassiveSearchNumbers(), SEPARATOR).Length
      If Me.MassiveSearchType = uc_account_sel.ENUM_MASSIVE_SEARCH_TYPE.ID_ACCOUNT Then
        Me.ef_account_id.IsReadOnly = True
        Me.ef_account_id.TextValue = _numbers_of_accounts & " " & GLB_NLS_GUI_AUDITOR.GetString(79)
        If _numbers_of_accounts = 1 Then
          Me.ef_account_id.TextValue = _numbers_of_accounts & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(496)
        End If

        Me.ef_account_id.Enabled = True
        Me.ef_card_track.IsReadOnly = False
        Me.ef_card_track.TextValue = String.Empty
        Me.ef_card_track.Enabled = False
      Else
        Me.ef_card_track.IsReadOnly = True
        Me.ef_card_track.TextValue = _numbers_of_accounts & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1822)
        If _numbers_of_accounts = 1 Then
          Me.ef_card_track.TextValue = _numbers_of_accounts & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2157)
        End If

        Me.ef_card_track.Enabled = True
        Me.ef_account_id.IsReadOnly = False
        Me.ef_account_id.TextValue = String.Empty
        Me.ef_account_id.Enabled = False
      End If

      Me.ef_card_holder.Enabled = False
      Me.chk_vip.Enabled = False
    Else
      Me.ef_account_id.TextValue = String.Empty
      Me.ef_card_track.TextValue = String.Empty
      Me.ef_account_id.IsReadOnly = False
      Me.ef_card_track.IsReadOnly = False
      Me.ef_account_id.Enabled = True
      Me.chk_vip.Enabled = True
      Me.ef_card_track.Enabled = True
      Me.ef_card_holder.Enabled = True
    End If

  End Sub ' btn_massive_search_Click

  Private Sub chk_only_anonymous_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    gb_level.Enabled = Not chk_only_anonymous.Checked
    gb_gender.Enabled = Not chk_only_anonymous.Checked
    If chk_only_anonymous.Checked Then
      chk_level_01.Checked = False
      chk_level_02.Checked = False
      chk_level_03.Checked = False
      chk_level_04.Checked = False

      chk_gender_male.Checked = False
      chk_gender_female.Checked = False
    End If
  End Sub

  Private Sub lbl_advanced_search_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lbl_advanced_search.LinkClicked
    Me.m_show_extended = Not Me.m_show_extended

    ShowHideAdvancedSearch()
  End Sub

End Class
