<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_account_sel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_container = New System.Windows.Forms.GroupBox
    Me.flp_container = New System.Windows.Forms.FlowLayoutPanel
    Me.pnl_account = New System.Windows.Forms.Panel
    Me.btn_massive_search = New System.Windows.Forms.Button
    Me.chk_anon = New System.Windows.Forms.CheckBox
    Me.chk_vip = New System.Windows.Forms.CheckBox
    Me.ef_account_id = New GUI_Controls.uc_entry_field
    Me.ef_card_track = New GUI_Controls.uc_entry_field
    Me.ef_card_holder = New GUI_Controls.uc_entry_field
    Me.uc_ds_birthday = New GUI_Controls.uc_date_select
    Me.uc_ds_wedding = New GUI_Controls.uc_date_select
    Me.ef_telephone = New GUI_Controls.uc_entry_field
    Me.gb_container.SuspendLayout()
    Me.flp_container.SuspendLayout()
    Me.pnl_account.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_container
    '
    Me.gb_container.Controls.Add(Me.flp_container)
    Me.gb_container.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.gb_container.Location = New System.Drawing.Point(3, 3)
    Me.gb_container.Margin = New System.Windows.Forms.Padding(0)
    Me.gb_container.Name = "gb_container"
    Me.gb_container.Padding = New System.Windows.Forms.Padding(0)
    Me.gb_container.Size = New System.Drawing.Size(304, 221)
    Me.gb_container.TabIndex = 0
    Me.gb_container.TabStop = False
    '
    'flp_container
    '
    Me.flp_container.AutoSize = True
    Me.flp_container.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.flp_container.Controls.Add(Me.pnl_account)
    Me.flp_container.Controls.Add(Me.ef_card_track)
    Me.flp_container.Controls.Add(Me.ef_card_holder)
    Me.flp_container.Controls.Add(Me.chk_vip)
    Me.flp_container.Controls.Add(Me.uc_ds_birthday)
    Me.flp_container.Controls.Add(Me.uc_ds_wedding)
    Me.flp_container.Controls.Add(Me.ef_telephone)
    Me.flp_container.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
    Me.flp_container.Location = New System.Drawing.Point(4, 8)
    Me.flp_container.Margin = New System.Windows.Forms.Padding(0)
    Me.flp_container.Name = "flp_container"
    Me.flp_container.Size = New System.Drawing.Size(297, 200)
    Me.flp_container.TabIndex = 7
    '
    'pnl_account
    '
    Me.pnl_account.Controls.Add(Me.btn_massive_search)
    Me.pnl_account.Controls.Add(Me.chk_anon)
    Me.pnl_account.Controls.Add(Me.ef_account_id)
    Me.pnl_account.Location = New System.Drawing.Point(0, 0)
    Me.pnl_account.Margin = New System.Windows.Forms.Padding(0, 0, 0, 2)
    Me.pnl_account.Name = "pnl_account"
    Me.pnl_account.Size = New System.Drawing.Size(294, 28)
    Me.pnl_account.TabIndex = 0
    '
    'btn_massive_search
    '
    Me.btn_massive_search.Location = New System.Drawing.Point(253, 4)
    Me.btn_massive_search.Name = "btn_massive_search"
    Me.btn_massive_search.Size = New System.Drawing.Size(38, 23)
    Me.btn_massive_search.TabIndex = 2
    Me.btn_massive_search.Text = "..."
    Me.btn_massive_search.UseVisualStyleBackColor = True
    Me.btn_massive_search.Visible = False
    '
    'chk_anon
    '
    Me.chk_anon.Location = New System.Drawing.Point(190, 11)
    Me.chk_anon.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.chk_anon.Name = "chk_anon"
    Me.chk_anon.Size = New System.Drawing.Size(91, 17)
    Me.chk_anon.TabIndex = 1
    Me.chk_anon.Text = "CheckBox1"
    Me.chk_anon.UseVisualStyleBackColor = True
    '
    'chk_vip
    '
    Me.chk_vip.Dock = System.Windows.Forms.DockStyle.Right
    Me.chk_vip.Location = New System.Drawing.Point(86, 90)
    Me.chk_vip.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.chk_vip.Name = "chk_vip"
    Me.chk_vip.Size = New System.Drawing.Size(208, 30)
    Me.chk_vip.TabIndex = 5
    Me.chk_vip.Text = "Text"
    Me.chk_vip.UseVisualStyleBackColor = True
    '
    'ef_account_id
    '
    Me.ef_account_id.DoubleValue = 0
    Me.ef_account_id.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_account_id.IntegerValue = 0
    Me.ef_account_id.IsReadOnly = False
    Me.ef_account_id.Location = New System.Drawing.Point(3, 3)
    Me.ef_account_id.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
    Me.ef_account_id.Name = "ef_account_id"
    Me.ef_account_id.Size = New System.Drawing.Size(181, 25)
    Me.ef_account_id.SufixText = "Sufix Text"
    Me.ef_account_id.SufixTextVisible = True
    Me.ef_account_id.TabIndex = 0
    Me.ef_account_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_account_id.TextValue = ""
    Me.ef_account_id.Value = ""
    Me.ef_account_id.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_card_track
    '
    Me.ef_card_track.DoubleValue = 0
    Me.ef_card_track.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_card_track.IntegerValue = 0
    Me.ef_card_track.IsReadOnly = False
    Me.ef_card_track.Location = New System.Drawing.Point(3, 33)
    Me.ef_card_track.Margin = New System.Windows.Forms.Padding(3, 3, 3, 2)
    Me.ef_card_track.Name = "ef_card_track"
    Me.ef_card_track.Size = New System.Drawing.Size(291, 25)
    Me.ef_card_track.SufixText = "Sufix Text"
    Me.ef_card_track.SufixTextVisible = True
    Me.ef_card_track.TabIndex = 3
    Me.ef_card_track.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_card_track.TextValue = ""
    Me.ef_card_track.Value = ""
    Me.ef_card_track.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_card_holder
    '
    Me.ef_card_holder.DoubleValue = 0
    Me.ef_card_holder.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_card_holder.IntegerValue = 0
    Me.ef_card_holder.IsReadOnly = False
    Me.ef_card_holder.Location = New System.Drawing.Point(3, 63)
    Me.ef_card_holder.Margin = New System.Windows.Forms.Padding(3, 3, 3, 2)
    Me.ef_card_holder.Name = "ef_card_holder"
    Me.ef_card_holder.OnlyUpperCase = True
    Me.ef_card_holder.Size = New System.Drawing.Size(291, 25)
    Me.ef_card_holder.SufixText = "Sufix Text"
    Me.ef_card_holder.SufixTextVisible = True
    Me.ef_card_holder.TabIndex = 4
    Me.ef_card_holder.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_card_holder.TextValue = ""
    Me.ef_card_holder.Value = ""
    Me.ef_card_holder.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_ds_birthday
    '
    Me.uc_ds_birthday.Day = ""
    Me.uc_ds_birthday.Location = New System.Drawing.Point(0, 120)
    Me.uc_ds_birthday.Margin = New System.Windows.Forms.Padding(0)
    Me.uc_ds_birthday.Month = ""
    Me.uc_ds_birthday.Name = "uc_ds_birthday"
    Me.uc_ds_birthday.Size = New System.Drawing.Size(281, 25)
    Me.uc_ds_birthday.TabIndex = 6
    Me.uc_ds_birthday.Year = ""
    '
    'uc_ds_wedding
    '
    Me.uc_ds_wedding.Day = ""
    Me.uc_ds_wedding.Location = New System.Drawing.Point(0, 145)
    Me.uc_ds_wedding.Margin = New System.Windows.Forms.Padding(0)
    Me.uc_ds_wedding.Month = ""
    Me.uc_ds_wedding.Name = "uc_ds_wedding"
    Me.uc_ds_wedding.Size = New System.Drawing.Size(281, 25)
    Me.uc_ds_wedding.TabIndex = 7
    Me.uc_ds_wedding.Year = ""
    '
    'ef_telephone
    '
    Me.ef_telephone.DoubleValue = 0
    Me.ef_telephone.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_telephone.IntegerValue = 0
    Me.ef_telephone.IsReadOnly = False
    Me.ef_telephone.Location = New System.Drawing.Point(3, 173)
    Me.ef_telephone.Margin = New System.Windows.Forms.Padding(3, 3, 3, 2)
    Me.ef_telephone.Name = "ef_telephone"
    Me.ef_telephone.Size = New System.Drawing.Size(291, 25)
    Me.ef_telephone.SufixText = "Sufix Text"
    Me.ef_telephone.SufixTextVisible = True
    Me.ef_telephone.TabIndex = 8
    Me.ef_telephone.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_telephone.TextValue = ""
    Me.ef_telephone.Value = ""
    Me.ef_telephone.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_account_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.AutoSize = True
    Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.Controls.Add(Me.gb_container)
    Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Name = "uc_account_sel"
    Me.Size = New System.Drawing.Size(307, 224)
    Me.gb_container.ResumeLayout(False)
    Me.gb_container.PerformLayout()
    Me.flp_container.ResumeLayout(False)
    Me.pnl_account.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_card_holder As GUI_Controls.uc_entry_field
  Friend WithEvents ef_card_track As GUI_Controls.uc_entry_field
  Friend WithEvents gb_container As System.Windows.Forms.GroupBox
  Friend WithEvents chk_anon As System.Windows.Forms.CheckBox
  Friend WithEvents ef_telephone As GUI_Controls.uc_entry_field
  Friend WithEvents ef_account_id As GUI_Controls.uc_entry_field
  Friend WithEvents flp_container As System.Windows.Forms.FlowLayoutPanel
  Friend WithEvents btn_massive_search As System.Windows.Forms.Button
  Friend WithEvents pnl_account As System.Windows.Forms.Panel
  Friend WithEvents chk_vip As System.Windows.Forms.CheckBox
  Friend WithEvents uc_ds_birthday As GUI_Controls.uc_date_select
  Friend WithEvents uc_ds_wedding As GUI_Controls.uc_date_select

End Class
