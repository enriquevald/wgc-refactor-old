'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_points_to_credit.vb
' DESCRIPTION:   Manages the points to credit grid and the associated days and terminals.
' AUTHOR:        Joan Marc Pepi� Jamison
' CREATION DATE: 04-OCT-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-OCT-2013  JPJ    Initial version
' 25-OCT-2013  JPJ    Defect WIG-320 Activate only the terminal lists that have their day checked
' 28-OCT-2013  JPJ    Defect WIG-328 Correct second time validation
' 29-OCT-2013  JPJ    Defect WIG-338 Points don't have to contain decimals.
' 30-OCT-2013  JPJ    Defect WIG-340 Time schedule validation.
' 31-OCT-2013  JPJ    Defect WIG-349 Name swap.
' 04-NOV-2013  JPJ    Defect WIG-366 Permissions visible/invisible for enabled/disabled.
' 11-JUL-2014  JMM    Weeks days sorted depending on system's first day of week
' 08-AUG-2016  FAV    Bug 16349: Weeks days sorted depending on system's first day of week (uc_terminals_group controls)
'--------------------------------------------------------------------

Imports GUI_CommonOperations
Imports WSI.Common
Imports GUI_Controls

Public Class uc_points_to_credit

#Region " Constants "

  ' Grid Columns by Id
  Private Const GRID_COLUMN_POINTS_LEVEL1 As Integer = 0
  Private Const GRID_COLUMN_POINTS_LEVEL2 As Integer = 1
  Private Const GRID_COLUMN_POINTS_LEVEL3 As Integer = 2
  Private Const GRID_COLUMN_POINTS_LEVEL4 As Integer = 3
  Private Const GRID_COLUMN_IS_VIP As Integer = 4
  Private Const GRID_COLUMN_CREDITS As Integer = 5
  Private Const GRID_COLUMN_GIFT_ID As Integer = 6
  Private Const GRID_COLUMNS As Integer = 7
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Datatable Columns by Id
  Private Const GIFT_COLUMN_LEVEL1 As Integer = 0 '"LEVEL1"
  Private Const GIFT_COLUMN_LEVEL2 As Integer = 1 '"LEVEL2"
  Private Const GIFT_COLUMN_LEVEL3 As Integer = 2 '"LEVEL3"
  Private Const GIFT_COLUMN_LEVEL4 As Integer = 3 '"LEVEL4"
  Private Const GIFT_COLUMN_IS_VIP As Integer = 4  '"IS_VIP"
  Private Const GIFT_COLUMN_CREDITS As Integer = 5 ' "CREDITS"
  Private Const GIFT_COLUMN_GIFT_ID As Integer = 6 ' "GIFT_ID"

  ' Datatable Columns by name
  Private Const GIFT_COLUMN_LEVEL1_NAME As String = "LEVEL1"
  Private Const GIFT_COLUMN_LEVEL2_NAME As String = "LEVEL2"
  Private Const GIFT_COLUMN_LEVEL3_NAME As String = "LEVEL3"
  Private Const GIFT_COLUMN_LEVEL4_NAME As String = "LEVEL4"
  Private Const GIFT_COLUMN_IS_VIP_NAME As String = "IS_VIP"
  Private Const GIFT_COLUMN_CREDITS_NAME As String = "CREDITS"
  Private Const GIFT_COLUMN_GIFT_ID_NAME As String = "GIFT_ID"

  Private Const MAX_DATABASE_NAME_LENGTH As Integer = 14 ' Lenght of the name in the textbox
  Private Const MAX_DATABASE_SUFIX_LENGTH As Integer = 3 ' Lenght of the sufix in the textbox

  Private Const OFFSET_ON_EXPANDED As Integer = 5  'Height of the terminal control
  Private Const GRID_WIDTH As Integer = 1940       'Width of the columns in the grid
  Private Const MAX_NUMBER_OF_GRID_ROWS As Integer = 99

#End Region ' Constants

#Region " Enums "

  ' Enum type of buttons 
  Public Enum ENUM_BUTTON_TYPE
    TYPE_ADD = 1
    TYPE_DELETE = 2
    TYPE_EDIT = 3
  End Enum ' ENUM_BUTTON_TYPE

  ' Enum type of elements that can get focus
  Public Enum ENUM_FOCUS
    FOCUS_NAME = 1
    FOCUS_ENABLED = 2
    FOCUS_TYPE = 3
    FOCUS_DAY = 4
    FOCUS_FROM_SCHEDULE = 5
    FOCUS_TO_SCHEDULE = 6
    FOCUS_TABLE = 7
    FOCUS_TERMINALS_MONDAY = 8
    FOCUS_TERMINALS_TUESDAY = 9
    FOCUS_TERMINALS_WEDNESDAY = 10
    FOCUS_TERMINALS_THURSDAY = 11
    FOCUS_TERMINALS_FRIDAY = 12
    FOCUS_TERMINALS_SATURDAY = 13
    FOCUS_TERMINALS_SUNDAY = 14
    FOCUS_SUFIX = 15
    FOCUS_AWARD_GAME = 16
  End Enum ' ENUM_FOCUS

  ' Enum type of errors in the grid
  Public Enum ENUM_TABLE_ERRORS
    NO_ERRORS = 0
    LEVELS_EMPY = 1
    CREDIT_EMPTY = 2
    CREDIT_DUPLICATED = 3
  End Enum ' ENUM_TABLE_ERRORS

  ' Enum type of errors in time controls
  Public Enum ENUM_TIME_ERRORS
    NO_ERRORS = 0
    TIME1_FROM_TO = 1
    TIME2_FROM_TO = 2
    OVERLAPING_TIMES = 3
  End Enum ' ENUM_TIME_ERRORS

  ' Enum type of errors in the terminal lists
  Public Enum ENUM_TERMINAL_LIST_ERRORS
    NO_ERRORS = 0
    MONDAY_LIST = 1
    TUESDAY_LIST = 2
    WEDNESDAY_LIST = 3
    THURSDAY_LIST = 4
    FRIDAY_LIST = 5
    SATURDAY_LIST = 6
    SUNDAY_LIST = 7
  End Enum ' ENUM_TERMINAL_LIST_ERRORS

#End Region ' Enums

#Region " Member "

  Private m_gifts_original As DataTable ' Contains the original datatable
  Private m_temp_grid_value As String   ' Keeps old value of the grid
  Private m_promotion_id As Int64       ' Id of the promotion associated with the redemption table
  Private m_tab_id As Int16             ' Index of the tab wich is contained
  Private m_gift_permission As Boolean  ' Indicates if there is an edit permission for gifts
  Private m_is_cash_draw_2_enabled As Boolean = False
  Private m_game_type_list As List(Of PromoGame.GameType)

#End Region ' Member

#Region " Public Events declaration "

  'Controls the event when a button is clicked
  Public Event ButtonClickEvent(ByVal ButtonId As ENUM_BUTTON_TYPE, ByVal Gift_Id As Int64)
  'Controls when entry name is left
  Public Event TableNameLeaveEvent(ByVal TableName As String)

#End Region 'Public Events declaration

#Region " Properties "

  Public Property TableName() As String
    Get
      Return Me.ef_points_credit_name.Value
    End Get
    Set(ByVal value As String)
      Me.ef_points_credit_name.Value = value
    End Set
  End Property

  Public Property Description() As String
    Get
      Return Me.txt_description.Text
    End Get

    Set(ByVal Value As String)
      Me.txt_description.Text = Value
    End Set
  End Property

  Public Property Weekday() As Integer
    Get
      Return GetWeekday()
    End Get

    Set(ByVal Value As Integer)
      SetWeekday(Value)
    End Set
  End Property

  Public Property TimeFrom() As Integer
    Get
      Return Me.dtp_time_from.Value.TimeOfDay.TotalSeconds
    End Get

    Set(ByVal Value As Integer)
      Dim elapsed As TimeSpan

      elapsed = TimeSpan.FromSeconds(Value)
      Me.dtp_time_from.Value = DateTime.Today.Add(elapsed)
    End Set
  End Property

  Public Property TimeTo() As Integer
    Get
      Return Me.dtp_time_to.Value.TimeOfDay.TotalSeconds
    End Get

    Set(ByVal Value As Integer)
      Dim elapsed As TimeSpan

      elapsed = TimeSpan.FromSeconds(Value)
      Me.dtp_time_to.Value = DateTime.Today.Add(elapsed)
    End Set
  End Property

  Public Property SecondTime() As Boolean
    Get
      Return Me.chk_second_time.Checked
    End Get

    Set(ByVal Value As Boolean)
      Me.chk_second_time.Checked = Value
    End Set
  End Property

  Public Property SecondTimeFrom() As Integer
    Get
      Return Me.dtp_second_time_from.Value.TimeOfDay.TotalSeconds
    End Get

    Set(ByVal Value As Integer)
      Dim elapsed As TimeSpan

      elapsed = TimeSpan.FromSeconds(Value)
      Me.dtp_second_time_from.Value = DateTime.Today.Add(elapsed)
    End Set
  End Property

  Public Property SecondTimeTo() As Integer
    Get
      Return Me.dtp_second_time_to.Value.TimeOfDay.TotalSeconds
    End Get

    Set(ByVal Value As Integer)
      Dim elapsed As TimeSpan

      elapsed = TimeSpan.FromSeconds(Value)
      Me.dtp_second_time_to.Value = DateTime.Today.Add(elapsed)
    End Set
  End Property

  Public ReadOnly Property AllWeekDaysSelected() As Integer
    Get
      Return &H7F
    End Get
  End Property

  Public ReadOnly Property NoWeekDaysSelected() As Integer
    Get
      Return 0
    End Get
  End Property

  Public Property EnabledAddButton() As Boolean
    Get
      Return Me.uc_grid_table_points.btn_add_row.Enabled
    End Get
    Set(ByVal value As Boolean)
      Me.uc_grid_table_points.btn_add_row.Enabled = value
    End Set
  End Property

  Public Property EnabledDeleteButton() As Boolean
    Get
      Return Me.uc_grid_table_points.btn_delete_row.Enabled
    End Get
    Set(ByVal value As Boolean)
      Me.uc_grid_table_points.btn_delete_row.Enabled = value
    End Set
  End Property

  Public Property EnabledGiftButton() As Boolean
    Get
      Return Me.btn_GiftEdit.Enabled
    End Get
    Set(ByVal value As Boolean)
      m_gift_permission = value
      Me.btn_GiftEdit.Enabled = value
    End Set
  End Property

  Public Property PromotionId() As Int64
    Get
      Return Me.m_promotion_id
    End Get
    Set(ByVal value As Int64)
      Me.m_promotion_id = value
    End Set
  End Property

  Public Property TabId() As Int16
    Get
      Return Me.m_tab_id
    End Get
    Set(ByVal value As Int16)
      Me.m_tab_id = value
    End Set
  End Property

  Public Property Sufix() As String
    Get
      Return Me.ef_points_credit_sufix.Value.Trim
    End Get
    Set(ByVal value As String)
      Me.ef_points_credit_sufix.Value = value
    End Set
  End Property

#End Region ' Properties

#Region " Public Functions "

  ' PURPOSE: Sets the default properties to the controls
  '
  '    - INPUT:
  '       -ENUM_FOCUS: Element that is going to be focused
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Public Sub SetDefaults()

    Weekday = &H7F        ' All days
    TimeFrom = 0          ' 00:00:00
    TimeTo = 0            ' 00:00:00
    SecondTime = False
    SecondTimeFrom = 0    ' 00:00:00
    SecondTimeTo = 0      ' 00:00:00

  End Sub ' SetDefaults

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControls()

  End Sub ' New

  ' PURPOSE: 'Sets the focus to a control
  '
  '    - INPUT:
  '       -ENUM_FOCUS: Element that is going to be focused
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Public Sub SetFocus(ByVal ElementFocus As ENUM_FOCUS)

    Select Case ElementFocus
      Case ENUM_FOCUS.FOCUS_NAME
        Me.ef_points_credit_name.Focus()
      Case ENUM_FOCUS.FOCUS_ENABLED
        Me.opt_enabled.Focus()
      Case ENUM_FOCUS.FOCUS_TYPE
        Me.opt_credit_redeemable.Focus()
      Case ENUM_FOCUS.FOCUS_DAY
        Me.chk_monday.Focus()
      Case ENUM_FOCUS.FOCUS_TERMINALS_MONDAY
        Me.uc_terminals_group_monday.Focus()
      Case ENUM_FOCUS.FOCUS_TERMINALS_TUESDAY
        Me.uc_terminals_group_tuesday.Focus()
      Case ENUM_FOCUS.FOCUS_TERMINALS_WEDNESDAY
        Me.uc_terminals_group_wednesday.Focus()
      Case ENUM_FOCUS.FOCUS_TERMINALS_THURSDAY
        Me.uc_terminals_group_thursday.Focus()
      Case ENUM_FOCUS.FOCUS_TERMINALS_FRIDAY
        Me.uc_terminals_group_friday.Focus()
      Case ENUM_FOCUS.FOCUS_TERMINALS_SATURDAY
        Me.uc_terminals_group_saturday.Focus()
      Case ENUM_FOCUS.FOCUS_TERMINALS_SUNDAY
        Me.uc_terminals_group_sunday.Focus()
      Case ENUM_FOCUS.FOCUS_FROM_SCHEDULE
        ' Time from equal to time to
        Me.dtp_time_from.Focus()
      Case ENUM_FOCUS.FOCUS_TO_SCHEDULE
        ' No week day has been marked
        Me.dtp_time_to.Focus()
      Case ENUM_FOCUS.FOCUS_TABLE
        Me.uc_grid_table_points.Focus()
      Case ENUM_FOCUS.FOCUS_SUFIX
        Me.ef_points_credit_sufix.Focus()
      Case ENUM_FOCUS.FOCUS_AWARD_GAME
        Me.cmb_award_with_game.Focus()
    End Select

  End Sub ' SetFocus

  ' PURPOSE: 'Returns if the redemption table is enabled
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    - Boolean : True (Enabled) / False (Disabled)
  Public Function IsTableEnabled() As Boolean

    Return IIf(opt_enabled.Checked = True, True, False)

  End Function ' IsTableEnabled

  ' PURPOSE: 'Sets the points to table to enabled or disabled
  '
  '    - INPUT:
  '       -Enabled: True (Enabled) / False (Disabled)
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Public Sub SetTableEnabled(ByVal Enabled As Boolean)

    Me.opt_enabled.Checked = IIf(Enabled, True, False)
    Me.opt_disabled.Checked = IIf(Enabled, False, True)

  End Sub ' SetTableEnabled

  ' PURPOSE: 'Returns the type of credit selected
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    - Boolean : True (Redimible) / False (Non-redimible)
  Public Function IsRedimible() As Boolean

    Return Me.opt_credit_redeemable.Checked

  End Function ' IsRedimible

  ' PURPOSE: 'Sets the redimible credit enabled or disabled
  '
  '    - INPUT:
  '       -Enabled: True (Enabled) / False (Disabled)
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Public Sub SetRedimible(ByVal Enabled As Boolean)

    Me.opt_credit_redeemable.Checked = IIf(Enabled, True, False)
    Me.opt_credit_non_redeemable.Checked = IIf(Enabled, False, True)

  End Sub ' SetRedimible

  ' PURPOSE: Indicates if all provider controls are ok. When the option is listed or not listed, they must have at least one selected element.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    - Boolean : True (One or more than one selected) / False ()
  Public Function CheckAllAtLeastOneSelected() As ENUM_TERMINAL_LIST_ERRORS

    If Me.uc_terminals_group_sunday.Enabled AndAlso Not Me.uc_terminals_group_sunday.CheckAtLeastOneSelected Then
      Return ENUM_TERMINAL_LIST_ERRORS.SUNDAY_LIST
    End If
    If Me.uc_terminals_group_monday.Enabled AndAlso Not Me.uc_terminals_group_monday.CheckAtLeastOneSelected Then
      Return ENUM_TERMINAL_LIST_ERRORS.MONDAY_LIST
    End If
    If Me.uc_terminals_group_tuesday.Enabled AndAlso Not Me.uc_terminals_group_tuesday.CheckAtLeastOneSelected Then
      Return ENUM_TERMINAL_LIST_ERRORS.TUESDAY_LIST
    End If
    If Me.uc_terminals_group_wednesday.Enabled AndAlso Not Me.uc_terminals_group_wednesday.CheckAtLeastOneSelected Then
      Return ENUM_TERMINAL_LIST_ERRORS.WEDNESDAY_LIST
    End If
    If Me.uc_terminals_group_thursday.Enabled AndAlso Not Me.uc_terminals_group_thursday.CheckAtLeastOneSelected Then
      Return ENUM_TERMINAL_LIST_ERRORS.THURSDAY_LIST
    End If
    If Me.uc_terminals_group_friday.Enabled AndAlso Not Me.uc_terminals_group_friday.CheckAtLeastOneSelected Then
      Return ENUM_TERMINAL_LIST_ERRORS.FRIDAY_LIST
    End If
    If Me.uc_terminals_group_saturday.Enabled AndAlso Not Me.uc_terminals_group_saturday.CheckAtLeastOneSelected Then
      Return ENUM_TERMINAL_LIST_ERRORS.SATURDAY_LIST
    End If

    Return ENUM_TERMINAL_LIST_ERRORS.NO_ERRORS

  End Function ' CheckAllAtLeastOneSelected

  ' PURPOSE: Checks if at least one day is selected.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Boolean.
  '
  Public Function CheckAtLeastOneDaySelected() As Boolean

    If Not Me.chk_sunday.Checked And Not Me.chk_monday.Checked And _
       Not Me.chk_tuesday.Checked And Not Me.chk_wednesday.Checked And _
       Not Me.chk_thursday.Checked And Not Me.chk_friday.Checked And _
       Not Me.chk_saturday.Checked Then

      Return False

    End If

    Return True

  End Function ' CheckAtLeastOneDaySelected

  ' PURPOSE: Checks gift table.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_TABLE_ERRORS.
  '
  Public Function CheckGiftGrid() As ENUM_TABLE_ERRORS
    Dim _i As Int16
    Dim _row As DataRow
    Dim _gift As DataTable

    Try
      ' Add gifts to the grid
      _gift = Me.CreateGiftTable(True)

      'At least one of the level has to be informed
      For _i = 0 To Me.uc_grid_table_points.NumRows - 1
        If String.IsNullOrEmpty(Me.uc_grid_table_points.Cell(_i, GRID_COLUMN_POINTS_LEVEL1).Value.ToString) AndAlso _
           String.IsNullOrEmpty(Me.uc_grid_table_points.Cell(_i, GRID_COLUMN_POINTS_LEVEL2).Value.ToString) AndAlso _
           String.IsNullOrEmpty(Me.uc_grid_table_points.Cell(_i, GRID_COLUMN_POINTS_LEVEL3).Value.ToString) AndAlso _
           String.IsNullOrEmpty(Me.uc_grid_table_points.Cell(_i, GRID_COLUMN_POINTS_LEVEL4).Value.ToString) Then

          Return ENUM_TABLE_ERRORS.LEVELS_EMPY
        End If

        'Credit table must be informed
        If String.IsNullOrEmpty(Me.uc_grid_table_points.Cell(_i, GRID_COLUMN_CREDITS).Value.ToString) Then
          Return ENUM_TABLE_ERRORS.CREDIT_EMPTY
        End If

        'Shouldn't be duplicates
        _row = _gift.NewRow()

        If Not String.IsNullOrEmpty(Me.uc_grid_table_points.Cell(_i, GRID_COLUMN_CREDITS).Value.ToString) Then
          _row(GIFT_COLUMN_CREDITS) = GUI_CommonOperations.mdl_number_format.GUI_ParseCurrency(Me.uc_grid_table_points.Cell(_i, GRID_COLUMN_CREDITS).Value.ToString)
        End If

        If IsNothing(_gift.Rows.Find(_row(GRID_COLUMN_CREDITS))) Then
          _gift.Rows.Add(_row)
        Else
          Return ENUM_TABLE_ERRORS.CREDIT_DUPLICATED
        End If

      Next

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)
    End Try

    Return ENUM_TABLE_ERRORS.NO_ERRORS

  End Function ' CheckGiftGrid

  ' PURPOSE: Gets the terminal list for a given day.
  '
  '  PARAMS:
  '     - INPUT:
  '       -ENUM_TERMINAL_DAY: Terminal list day
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TerminalList.
  '
  Public Function GetTerminalList(ByVal Terminal_Day As DayOfWeek) As TerminalList
    Dim _terminal_list As New TerminalList

    Select Case Terminal_Day
      Case DayOfWeek.Sunday
        Me.uc_terminals_group_sunday.GetTerminalsFromControl(_terminal_list)
      Case DayOfWeek.Monday
        Me.uc_terminals_group_monday.GetTerminalsFromControl(_terminal_list)
      Case DayOfWeek.Tuesday
        Me.uc_terminals_group_tuesday.GetTerminalsFromControl(_terminal_list)
      Case DayOfWeek.Wednesday
        Me.uc_terminals_group_wednesday.GetTerminalsFromControl(_terminal_list)
      Case DayOfWeek.Thursday
        Me.uc_terminals_group_thursday.GetTerminalsFromControl(_terminal_list)
      Case DayOfWeek.Friday
        Me.uc_terminals_group_friday.GetTerminalsFromControl(_terminal_list)
      Case DayOfWeek.Saturday
        Me.uc_terminals_group_saturday.GetTerminalsFromControl(_terminal_list)
    End Select

    Return _terminal_list

  End Function ' GetTerminalList

  ' PURPOSE: Sets the terminal list for one of the controls
  '
  '  PARAMS:
  '     - INPUT:
  '       -TerminalList: Terminal list
  '       -ENUM_TERMINAL_DAY: Terminal list day
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_TABLE_ERRORS.
  '
  Public Sub SetTerminalList(ByVal TerminalList As TerminalList, ByVal Terminal_Day As DayOfWeek)

    Select Case Terminal_Day
      Case DayOfWeek.Sunday
        Me.uc_terminals_group_sunday.SetTerminalList(TerminalList)
      Case DayOfWeek.Monday
        Me.uc_terminals_group_monday.SetTerminalList(TerminalList)
      Case DayOfWeek.Tuesday
        Me.uc_terminals_group_tuesday.SetTerminalList(TerminalList)
      Case DayOfWeek.Wednesday
        Me.uc_terminals_group_wednesday.SetTerminalList(TerminalList)
      Case DayOfWeek.Thursday
        Me.uc_terminals_group_thursday.SetTerminalList(TerminalList)
      Case DayOfWeek.Friday
        Me.uc_terminals_group_friday.SetTerminalList(TerminalList)
      Case DayOfWeek.Saturday
        Me.uc_terminals_group_saturday.SetTerminalList(TerminalList)
    End Select

  End Sub ' SetTerminalList

  ' PURPOSE: Sets the gift list
  '
  '  PARAMS:
  '     - INPUT:
  '       -Datatable: Gifts table
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_TABLE_ERRORS.
  '
  Public Sub SetGiftTable(ByVal Gifts As DataTable)
    Dim _row_index As Int32

    ' Add gifts to the grid
    If Not IsNothing(Gifts) Then
      m_gifts_original = Gifts.Copy

      For Each _row As DataRow In Gifts.Rows

        _row_index = Me.uc_grid_table_points.AddRow()

        ' 29-OCT-2013  JPJ    Defect WIG-338 Points don't have to contain decimals.
        If Not _row(GIFT_COLUMN_LEVEL1) Is DBNull.Value Then
          Me.uc_grid_table_points.Cell(_row_index, GRID_COLUMN_POINTS_LEVEL1).Value = GUI_FormatNumber(_row(GIFT_COLUMN_LEVEL1), 0)
        End If

        If Not _row(GIFT_COLUMN_LEVEL2) Is DBNull.Value Then
          Me.uc_grid_table_points.Cell(_row_index, GRID_COLUMN_POINTS_LEVEL2).Value = GUI_FormatNumber(_row(GIFT_COLUMN_LEVEL2), 0)
        End If

        If Not _row(GIFT_COLUMN_LEVEL3) Is DBNull.Value Then
          Me.uc_grid_table_points.Cell(_row_index, GRID_COLUMN_POINTS_LEVEL3).Value = GUI_FormatNumber(_row(GIFT_COLUMN_LEVEL3), 0)
        End If

        If Not _row(GIFT_COLUMN_LEVEL4) Is DBNull.Value Then
          Me.uc_grid_table_points.Cell(_row_index, GRID_COLUMN_POINTS_LEVEL4).Value = GUI_FormatNumber(_row(GIFT_COLUMN_LEVEL4), 0)
        End If

        ' JBP
        If _row(GIFT_COLUMN_IS_VIP) Is DBNull.Value Then
          Me.uc_grid_table_points.Cell(_row_index, GRID_COLUMN_IS_VIP).Value = uc_grid.GRID_CHK_UNCHECKED
        Else
          Me.uc_grid_table_points.Cell(_row_index, GRID_COLUMN_IS_VIP).Value = BoolToGridValue(_row(GIFT_COLUMN_IS_VIP))
        End If

        If Not _row(GIFT_COLUMN_CREDITS) Is DBNull.Value Then
          Me.uc_grid_table_points.Cell(_row_index, GRID_COLUMN_CREDITS).Value = GUI_FormatCurrency(_row(GIFT_COLUMN_CREDITS))
        End If

        Me.uc_grid_table_points.Cell(_row_index, GRID_COLUMN_GIFT_ID).Value = CType(_row(GIFT_COLUMN_GIFT_ID), Int64)
      Next
    End If
    Me.uc_grid_table_points.SelectFirstRow(True)

  End Sub ' SetGiftTable

  ' PURPOSE: Sets the gift table.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Datatable: Gift table
  '
  Private Function GetGiftTable() As DataTable
    Dim _gift As DataTable
    Dim _i As Int16
    Dim _row As DataRow

    ' Add gifts to the grid
    _gift = Me.CreateGiftTable(False)

    Try

      For _i = 0 To Me.uc_grid_table_points.NumRows - 1
        _row = _gift.NewRow()

        If Not String.IsNullOrEmpty(Me.uc_grid_table_points.Cell(_i, GRID_COLUMN_POINTS_LEVEL1).Value.ToString) Then
          _row(GIFT_COLUMN_LEVEL1) = GUI_FormatNumber(Me.uc_grid_table_points.Cell(_i, GRID_COLUMN_POINTS_LEVEL1).Value.ToString)
        End If
        If Not String.IsNullOrEmpty(Me.uc_grid_table_points.Cell(_i, GRID_COLUMN_POINTS_LEVEL2).Value.ToString) Then
          _row(GIFT_COLUMN_LEVEL2) = GUI_FormatNumber(Me.uc_grid_table_points.Cell(_i, GRID_COLUMN_POINTS_LEVEL2).Value.ToString)
        End If
        If Not String.IsNullOrEmpty(Me.uc_grid_table_points.Cell(_i, GRID_COLUMN_POINTS_LEVEL3).Value.ToString) Then
          _row(GIFT_COLUMN_LEVEL3) = GUI_FormatNumber(Me.uc_grid_table_points.Cell(_i, GRID_COLUMN_POINTS_LEVEL3).Value.ToString)
        End If
        If Not String.IsNullOrEmpty(Me.uc_grid_table_points.Cell(_i, GRID_COLUMN_POINTS_LEVEL4).Value.ToString) Then
          _row(GIFT_COLUMN_LEVEL4) = GUI_FormatNumber(Me.uc_grid_table_points.Cell(_i, GRID_COLUMN_POINTS_LEVEL4).Value.ToString)
        End If
        If Not String.IsNullOrEmpty(Me.uc_grid_table_points.Cell(_i, GRID_COLUMN_CREDITS).Value.ToString) Then
          _row(GIFT_COLUMN_CREDITS) = GUI_CommonOperations.mdl_number_format.GUI_ParseCurrency(Me.uc_grid_table_points.Cell(_i, GRID_COLUMN_CREDITS).Value.ToString)
        End If
        If String.IsNullOrEmpty(Me.uc_grid_table_points.Cell(_i, GRID_COLUMN_GIFT_ID).Value.ToString) Then
          _row(GIFT_COLUMN_GIFT_ID) = 0
        Else
          _row(GIFT_COLUMN_GIFT_ID) = CType(Me.uc_grid_table_points.Cell(_i, GRID_COLUMN_GIFT_ID).Value.ToString, Int64)
        End If

        ' JBP - Always return true o false.
        _row(GRID_COLUMN_IS_VIP) = (Me.uc_grid_table_points.Cell(_i, GRID_COLUMN_IS_VIP).Value = uc_grid.GRID_CHK_CHECKED)

        _gift.Rows.Add(_row)

      Next

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)
    End Try

    Return _gift

  End Function ' SetGiftTable

  ' PURPOSE: Sets the week days.
  '
  '  PARAMS:
  '     - INPUT:
  '       -Days: Days to be selected
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Datatable: Gift table
  '
  Public Sub SetWeekday(ByVal Days As Integer)
    Dim _str_binary As String
    Dim _bit_array As Char()

    _str_binary = Convert.ToString(Days, 2)
    _str_binary = New String("0", 7 - _str_binary.Length) + _str_binary

    _bit_array = _str_binary.ToCharArray()

    If (_bit_array(6) = "0") Then Me.chk_monday.Checked = False Else Me.chk_monday.Checked = True ' Monday 
    If (_bit_array(5) = "0") Then Me.chk_tuesday.Checked = False Else Me.chk_tuesday.Checked = True ' Tuesday
    If (_bit_array(4) = "0") Then Me.chk_wednesday.Checked = False Else Me.chk_wednesday.Checked = True ' Wednesday
    If (_bit_array(3) = "0") Then Me.chk_thursday.Checked = False Else Me.chk_thursday.Checked = True ' Thursday
    If (_bit_array(2) = "0") Then Me.chk_friday.Checked = False Else Me.chk_friday.Checked = True ' Friday
    If (_bit_array(1) = "0") Then Me.chk_saturday.Checked = False Else Me.chk_saturday.Checked = True ' Saturday
    If (_bit_array(0) = "0") Then Me.chk_sunday.Checked = False Else Me.chk_sunday.Checked = True ' Sunday

  End Sub ' SetWeekday

  ' PURPOSE: Gets the week days.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Integer: Selected days
  '
  Public Function GetWeekday() As Integer
    Dim _bit_array As Char()

    _bit_array = New String("0000000").ToCharArray()

    _bit_array(6) = Convert.ToInt32(Me.chk_monday.Checked).ToString()  ' Monday 
    _bit_array(5) = Convert.ToInt32(Me.chk_tuesday.Checked).ToString()  ' Tuesday
    _bit_array(4) = Convert.ToInt32(Me.chk_wednesday.Checked).ToString()  ' Wednesday
    _bit_array(3) = Convert.ToInt32(Me.chk_thursday.Checked).ToString()  ' Thursday
    _bit_array(2) = Convert.ToInt32(Me.chk_friday.Checked).ToString()  ' Friday
    _bit_array(1) = Convert.ToInt32(Me.chk_saturday.Checked).ToString()  ' Saturday
    _bit_array(0) = Convert.ToInt32(Me.chk_sunday.Checked).ToString()  ' Sunday

    Return Convert.ToInt32(_bit_array, 2)

  End Function ' GetWeekday

  ' PURPOSE: Get new empty datatable
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    - DataTable : Gift datatable
  Public Function CreateGiftTable(ByVal primary As Boolean) As DataTable
    Dim _gift_data As DataTable = New DataTable()

    _gift_data.Columns.Add(GIFT_COLUMN_LEVEL1_NAME, Type.GetType("System.Decimal"))
    _gift_data.Columns.Add(GIFT_COLUMN_LEVEL2_NAME, Type.GetType("System.Decimal"))
    _gift_data.Columns.Add(GIFT_COLUMN_LEVEL3_NAME, Type.GetType("System.Decimal"))
    _gift_data.Columns.Add(GIFT_COLUMN_LEVEL4_NAME, Type.GetType("System.Decimal"))
    _gift_data.Columns.Add(GIFT_COLUMN_IS_VIP_NAME, Type.GetType("System.Boolean"))
    _gift_data.Columns.Add(GIFT_COLUMN_CREDITS_NAME, Type.GetType("System.Decimal"))
    _gift_data.Columns.Add(GIFT_COLUMN_GIFT_ID_NAME, Type.GetType("System.Int64"))

    If primary Then
      _gift_data.PrimaryKey = New DataColumn() {_gift_data.Columns(GIFT_COLUMN_CREDITS)}
    End If

    Return _gift_data

  End Function ' CreateGiftTable

  ' PURPOSE: Asks if focus is in an control that prevents return keypress
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    - Boolean: True (Focused in an element that prevents from return key)/ False
  Public Function PreventReturn(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    If Me.uc_grid_table_points.ContainsFocus Then

      Call Me.uc_grid_table_points.KeyPressed(sender, e)

      Return True
    End If

    If Me.txt_description.ContainsFocus Then
      Return True
    End If

    Return False

  End Function ' PreventReturn

  ' PURPOSE: Checks scheduled times.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_TABLE_ERRORS.
  '
  Public Function CheckScheduledTime() As ENUM_TIME_ERRORS
    '30-OCT-2013  JPJ    Defect WIG-340 We eliminate the time schedule validation
    Return ENUM_TIME_ERRORS.NO_ERRORS

  End Function ' CheckScheduledTime

  Public Function CheckAwardWithGame() As Boolean
    If Me.m_is_cash_draw_2_enabled _
      And Me.chk_award_with_game.Checked AndAlso Me.cmb_award_with_game.SelectedIndex = -1 Then

      Return False
    End If

    Return True
  End Function

  Public Function GetAwardGame() As Long
    If Me.chk_award_with_game.Enabled Then
      Return Me.cmb_award_with_game.SelectedValue
    Else
      Return -1
    End If
  End Function

  Public Sub SetAwardGame(AwardGame As Long)
    Dim _game_return_type_list As List(Of WSI.Common.PrizeType)

    _game_return_type_list = New List(Of WSI.Common.PrizeType)

    If Me.m_is_cash_draw_2_enabled Then
      If AwardGame > 0 Then

        If Me.opt_credit_non_redeemable.Checked Then
          _game_return_type_list.Add(WSI.Common.PrizeType.NoRedeemable)
        Else
          _game_return_type_list.Add(WSI.Common.PrizeType.Redeemable)
        End If

        Call Me.SetComboSource(_game_return_type_list)

        Me.chk_award_with_game.Checked = True
        Me.cmb_award_with_game.SelectedValue = AwardGame
      Else
        Me.chk_award_with_game.Checked = False
        Me.cmb_award_with_game.SelectedIndex = -1
        Me.cmb_award_with_game.Enabled = False
      End If
    End If
  End Sub

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Initializes the form controls
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InitControls()
    Dim _week_days_chk_array(6) As Windows.Forms.CheckBox
    Dim _tab_order As Integer

    ' Enabled control
    Me.gb_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(260)
    Me.opt_enabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(264)
    Me.opt_disabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(265)

    ' Type
    Me.gb_credit_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254)
    Me.opt_credit_non_redeemable.Text = GLB_NLS_GUI_INVOICING.GetString(338)
    Me.opt_credit_redeemable.Text = GLB_NLS_GUI_INVOICING.GetString(337)
    Me.lbl_description.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(688)

    'Scheduled week
    Me.gb_daily.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2845)

    'Scheduled day
    Me.lbl_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(258)
    Me.chk_monday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(301)
    Me.chk_tuesday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(302)
    Me.chk_wednesday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(303)
    Me.chk_thursday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(304)
    Me.chk_friday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(305)
    Me.chk_saturday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(306)
    Me.chk_sunday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(307)

    _tab_order = Me.chk_sunday.TabIndex

    _week_days_chk_array(0) = chk_monday
    _week_days_chk_array(1) = chk_tuesday
    _week_days_chk_array(2) = chk_wednesday
    _week_days_chk_array(3) = chk_thursday
    _week_days_chk_array(4) = chk_friday
    _week_days_chk_array(5) = chk_saturday
    _week_days_chk_array(6) = chk_sunday

    GUI_Controls.Misc.SortCheckBoxesByFirstDayOfWeek(_week_days_chk_array)

    Me.uc_terminals_group_monday.Top = chk_monday.Top + gb_daily.Top
    Me.uc_terminals_group_monday.TabIndex = gb_daily.TabIndex + chk_monday.TabIndex + _week_days_chk_array.Length - _tab_order
    Me.uc_terminals_group_tuesday.Top = chk_tuesday.Top + gb_daily.Top
    Me.uc_terminals_group_tuesday.TabIndex = gb_daily.TabIndex + chk_tuesday.TabIndex + _week_days_chk_array.Length - _tab_order
    Me.uc_terminals_group_wednesday.Top = chk_wednesday.Top + gb_daily.Top
    Me.uc_terminals_group_wednesday.TabIndex = gb_daily.TabIndex + chk_wednesday.TabIndex + _week_days_chk_array.Length - _tab_order
    Me.uc_terminals_group_thursday.Top = chk_thursday.Top + gb_daily.Top
    Me.uc_terminals_group_thursday.TabIndex = gb_daily.TabIndex + chk_thursday.TabIndex + _week_days_chk_array.Length - _tab_order
    Me.uc_terminals_group_friday.Top = chk_friday.Top + gb_daily.Top
    Me.uc_terminals_group_friday.TabIndex = gb_daily.TabIndex + chk_friday.TabIndex + _week_days_chk_array.Length - _tab_order
    Me.uc_terminals_group_saturday.Top = chk_saturday.Top + gb_daily.Top
    Me.uc_terminals_group_saturday.TabIndex = gb_daily.TabIndex + chk_saturday.TabIndex + _week_days_chk_array.Length - _tab_order
    Me.uc_terminals_group_sunday.Top = chk_sunday.Top + gb_daily.Top
    Me.uc_terminals_group_sunday.TabIndex = gb_daily.TabIndex + chk_sunday.TabIndex + _week_days_chk_array.Length - _tab_order

    BringToFrontControlsByFirstWeekDay()

    'Scheduled day and format
    Me.dtp_time_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(297)
    Me.dtp_time_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(298)
    Me.dtp_time_from.ShowUpDown = True
    Me.dtp_time_from.SetFormat(0, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_time_to.ShowUpDown = True
    Me.dtp_time_to.SetFormat(0, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.chk_second_time.Checked = False
    Me.dtp_second_time_from.Enabled = False
    Me.dtp_second_time_to.Enabled = False
    Me.dtp_second_time_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(209)
    Me.dtp_second_time_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(298)
    Me.dtp_second_time_from.ShowUpDown = True
    Me.dtp_second_time_from.SetFormat(0, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_second_time_to.ShowUpDown = True
    Me.dtp_second_time_to.SetFormat(0, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.btn_GiftEdit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(352) 'Edici�n de regalo
    Me.ef_points_credit_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253) 'Nombre
    Me.ef_points_credit_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, MAX_DATABASE_NAME_LENGTH)
    Me.ef_points_credit_sufix.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2851) 'Sufix
    Me.ef_points_credit_sufix.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, MAX_DATABASE_SUFIX_LENGTH)

    Me.lbl_terminals.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2794) 'Cr�dito no redimible restringido a estos terminales

    Me.uc_terminals_group_monday.ShowGroupBoxLine = False
    Me.uc_terminals_group_monday.HeightOnExpanded = Me.Height - uc_terminals_group_monday.Top - OFFSET_ON_EXPANDED
    Me.uc_terminals_group_tuesday.ShowGroupBoxLine = False
    Me.uc_terminals_group_tuesday.HeightOnExpanded = Me.Height - uc_terminals_group_tuesday.Top - OFFSET_ON_EXPANDED
    Me.uc_terminals_group_wednesday.ShowGroupBoxLine = False
    Me.uc_terminals_group_wednesday.HeightOnExpanded = Me.Height - uc_terminals_group_wednesday.Top - OFFSET_ON_EXPANDED
    Me.uc_terminals_group_thursday.ShowGroupBoxLine = False
    Me.uc_terminals_group_thursday.HeightOnExpanded = Me.Height - uc_terminals_group_thursday.Top - OFFSET_ON_EXPANDED
    Me.uc_terminals_group_friday.ShowGroupBoxLine = False
    Me.uc_terminals_group_friday.HeightOnExpanded = Me.Height - uc_terminals_group_friday.Top - OFFSET_ON_EXPANDED
    Me.uc_terminals_group_saturday.ShowGroupBoxLine = False
    Me.uc_terminals_group_saturday.HeightOnExpanded = Me.Height - uc_terminals_group_saturday.Top - OFFSET_ON_EXPANDED
    Me.uc_terminals_group_sunday.ShowGroupBoxLine = False
    Me.uc_terminals_group_sunday.HeightOnExpanded = Me.Height - uc_terminals_group_sunday.Top - OFFSET_ON_EXPANDED

    Me.lbl_information.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2846) 'A los d�as no seleccionados se les aplica la restricci�n de terminales del �ltimo d�a seleccionado
    Me.btn_GiftEdit.Size = New System.Drawing.Size(90, 35)

    With Me.uc_grid_table_points

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

      .btn_add_row.Visible = True
      .btn_delete_row.Visible = True

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      '   - Points Level1
      ' 2622 "Puntos por Nivel"
      .Column(GRID_COLUMN_POINTS_LEVEL1).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(350)    ' 350 "Puntos"
      .Column(GRID_COLUMN_POINTS_LEVEL1).Header(1).Text = GeneralParam.GetString("PlayerTracking", "Level01.Name")
      .Column(GRID_COLUMN_POINTS_LEVEL1).Width = GRID_WIDTH
      .Column(GRID_COLUMN_POINTS_LEVEL1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_POINTS_LEVEL1).IsColumnPrintable = True
      .Column(GRID_COLUMN_POINTS_LEVEL1).Editable = True
      .Column(GRID_COLUMN_POINTS_LEVEL1).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_POINTS_LEVEL1).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 0)

      '   - Points Level2
      .Column(GRID_COLUMN_POINTS_LEVEL2).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(350)    ' 350 "Puntos"
      .Column(GRID_COLUMN_POINTS_LEVEL2).Header(1).Text = GeneralParam.GetString("PlayerTracking", "Level02.Name")
      .Column(GRID_COLUMN_POINTS_LEVEL2).Width = GRID_WIDTH
      .Column(GRID_COLUMN_POINTS_LEVEL2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_POINTS_LEVEL2).IsColumnPrintable = True
      .Column(GRID_COLUMN_POINTS_LEVEL2).Editable = True
      .Column(GRID_COLUMN_POINTS_LEVEL2).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_POINTS_LEVEL2).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 0)

      '   - Points Level3
      .Column(GRID_COLUMN_POINTS_LEVEL3).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(350)    ' 350 "Puntos"
      .Column(GRID_COLUMN_POINTS_LEVEL3).Header(1).Text = GeneralParam.GetString("PlayerTracking", "Level03.Name")
      .Column(GRID_COLUMN_POINTS_LEVEL3).Width = GRID_WIDTH
      .Column(GRID_COLUMN_POINTS_LEVEL3).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_POINTS_LEVEL3).IsColumnPrintable = True
      .Column(GRID_COLUMN_POINTS_LEVEL3).Editable = True
      .Column(GRID_COLUMN_POINTS_LEVEL3).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_POINTS_LEVEL3).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 0)

      '   - Points Level4
      .Column(GRID_COLUMN_POINTS_LEVEL4).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(350)    ' 350 "Puntos"
      .Column(GRID_COLUMN_POINTS_LEVEL4).Header(1).Text = GeneralParam.GetString("PlayerTracking", "Level04.Name")
      .Column(GRID_COLUMN_POINTS_LEVEL4).Width = GRID_WIDTH
      .Column(GRID_COLUMN_POINTS_LEVEL4).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_POINTS_LEVEL4).IsColumnPrintable = True
      .Column(GRID_COLUMN_POINTS_LEVEL4).Editable = True
      .Column(GRID_COLUMN_POINTS_LEVEL4).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_POINTS_LEVEL4).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 0)

      '   - Vip
      .Column(GRID_COLUMN_IS_VIP).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_IS_VIP).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4803)
      .Column(GRID_COLUMN_IS_VIP).Width = 1420
      .Column(GRID_COLUMN_IS_VIP).IsMerged = False
      .Column(GRID_COLUMN_IS_VIP).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_IS_VIP).Editable = True
      .Column(GRID_COLUMN_IS_VIP).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      '   - Credits 
      .Column(GRID_COLUMN_CREDITS).Header(0).Text = GLB_NLS_GUI_CLASS_II.GetString(224)    ' 224 "Cr�ditos"
      If Me.opt_credit_non_redeemable.Checked = True Then
        .Column(GRID_COLUMN_CREDITS).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(338)
      Else
        .Column(GRID_COLUMN_CREDITS).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(337)
      End If
      .Column(GRID_COLUMN_CREDITS).Width = GRID_WIDTH
      .Column(GRID_COLUMN_CREDITS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_CREDITS).IsColumnPrintable = True
      .Column(GRID_COLUMN_CREDITS).Editable = True
      .Column(GRID_COLUMN_CREDITS).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_CREDITS).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)

      '   - GiftId 
      .Column(GRID_COLUMN_GIFT_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_GIFT_ID).Header(1).Text = "" 'Gift ID"
      .Column(GRID_COLUMN_GIFT_ID).Width = 0
      .Column(GRID_COLUMN_GIFT_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_GIFT_ID).IsColumnPrintable = False
      .Column(GRID_COLUMN_GIFT_ID).Editable = False
    End With

    Me.gb_game_use.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8676)
    Me.chk_award_with_game.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8613)

    ' TODO-UNRANKED TEAM: PENDING MARGE FROM MAIN-TELEVISA
    'Me.m_is_cash_draw_2_enabled = WSI.Common.GeneralParam.GetBoolean("Features", "CashDesk.Draw.02", False)

    'If Me.m_is_cash_draw_2_enabled Then
    '  ' Initialize Promogames combo
    '  Call InitAwardWithGameCombobox()
    'Else
    Me.gb_game_use.Visible = False
    'End If

  End Sub ' InitControls

  ' PURPOSE: Gets the chnages done in the original datatable
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '     -Datatable: New/Added and modified rows
  '
  Public Function GetChangedDatatable() As DataTable
    Dim _final_table As DataTable
    Dim _grid_table As DataTable
    Dim _table As New DataTable

    _final_table = Me.CreateGiftTable(False)

    _grid_table = GetGiftTable().Copy 'Datatable generated through the grid
    _final_table = _grid_table.Copy
    If Not m_gifts_original Is Nothing Then
      _table = m_gifts_original.Copy()

      For Each _row As DataRow In _table.Rows
        If Not ExistsInDatable(_grid_table, GIFT_COLUMN_GIFT_ID_NAME, _row(GIFT_COLUMN_GIFT_ID)) Then
          _row.Delete() 'We mark the row as deleted 
          _final_table.ImportRow(_row)
        End If
      Next
    End If

    Return _final_table
  End Function ' GetChangedDatatable

  ' PURPOSE: 'Indicates if an element is inside a datatable that doesn't have foreign key
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    - Boolean : True (Exists) / False (Doesn't exist)
  Private Function ExistsInDatable(ByVal Table As DataTable, ByVal Columname As String, ByVal Id As Int64) As Boolean
    Dim _row() As DataRow

    Try
      _row = Table.Select(Columname & " = " & Id)
      If _row.Length = 0 Then

        Return False
      End If

      Return True

    Catch ex As Exception

      Return False
    End Try

  End Function ' ExistsInDatable

  ' PURPOSE:  Cast from boolean to uc_grid checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function BoolToGridValue(ByVal enabled As Boolean) As String
    If enabled Then
      Return uc_grid.GRID_CHK_CHECKED
    Else
      Return uc_grid.GRID_CHK_UNCHECKED
    End If
  End Function ' BoolToGridValue

  ' PURPOSE:  Cast from uc_grid checked to boolean
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GridValueToBool(ByVal enabled As String) As Boolean

    If (enabled.Equals(uc_grid.GRID_CHK_CHECKED) Or enabled.Equals(uc_grid.GRID_CHK_CHECKED_DISABLED)) Then
      Return True
    Else
      Return False
    End If

  End Function     ' GridValueToBool


  ' PURPOSE:  Set the BringFront property for each uc_terminals_group control starting with
  '           the first day of the week
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub BringToFrontControlsByFirstWeekDay()
    Dim _list As New List(Of uc_terminals_group_filter)
    _list.Add(uc_terminals_group_monday)
    _list.Add(uc_terminals_group_tuesday)
    _list.Add(uc_terminals_group_wednesday)
    _list.Add(uc_terminals_group_thursday)
    _list.Add(uc_terminals_group_friday)
    _list.Add(uc_terminals_group_saturday)
    _list.Add(uc_terminals_group_sunday)

    _list.Sort(Function(p1, p2) p1.TabIndex.CompareTo(p2.TabIndex))
    _list.Reverse()

    For Each _item As uc_terminals_group_filter In _list
      _item.BringToFront()
    Next
  End Sub

  Private Sub InitAwardWithGameCombobox()
    Dim _result As List(Of PromoGame)
    Dim _game_return_type_list As List(Of WSI.Common.PrizeType)

    _result = New List(Of PromoGame)
    Me.m_game_type_list = New List(Of PromoGame.GameType)
    _game_return_type_list = New List(Of PrizeType)

    Me.m_game_type_list.Add(PromoGame.GameType.PlayCash)
    Me.m_game_type_list.Add(PromoGame.GameType.PlayReward)

    _game_return_type_list.Add(WSI.Common.PrizeType.NoRedeemable)

    Call Me.SetComboSource(_game_return_type_list)

    Me.cmb_award_with_game.Enabled = False
  End Sub

  Private Sub SetComboSource(PrizeUnitType As List(Of PrizeType))
    Dim _prize_type_dict As List(Of PromoGame)

    _prize_type_dict = New PromoGame().GetByGameTypeListAndPrizeTypeListAndPromotionId(Me.m_game_type_list, PrizeUnitType, Me.PromotionId)

    Me.cmb_award_with_game.DataSource = _prize_type_dict
    Me.cmb_award_with_game.DisplayMember = "Name"
    Me.cmb_award_with_game.ValueMember = "Id"
    Me.cmb_award_with_game.SelectedValue = -1
  End Sub

#End Region ' Private Functions

#Region " Events "

  ' PURPOSE: Enables or disbles the hour / minute entry field
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    
  Private Sub chk_second_time_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_second_time.CheckedChanged

    dtp_second_time_from.Enabled = chk_second_time.Checked
    dtp_second_time_to.Enabled = chk_second_time.Checked

  End Sub ' chk_second_time_CheckedChanged

  ' PURPOSE: Enables the associated terminal list of the day control
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    
  Private Sub chk_day_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _
              chk_monday.CheckedChanged, chk_tuesday.CheckedChanged, chk_wednesday.CheckedChanged, _
              chk_thursday.CheckedChanged, chk_friday.CheckedChanged, chk_saturday.CheckedChanged, chk_sunday.CheckedChanged

    If opt_credit_non_redeemable.Checked = True Then
      Select Case sender.name.ToString.ToLower
        Case chk_sunday.Name.ToString.ToLower
          uc_terminals_group_sunday.Enabled = sender.checked
        Case chk_monday.Name.ToString.ToLower
          uc_terminals_group_monday.Enabled = sender.checked
        Case chk_tuesday.Name.ToString.ToLower
          uc_terminals_group_tuesday.Enabled = sender.checked
        Case chk_wednesday.Name.ToString.ToLower
          uc_terminals_group_wednesday.Enabled = sender.checked
        Case chk_thursday.Name.ToString.ToLower
          uc_terminals_group_thursday.Enabled = sender.checked
        Case chk_friday.Name.ToString.ToLower
          uc_terminals_group_friday.Enabled = sender.checked
        Case chk_saturday.Name.ToString.ToLower
          uc_terminals_group_saturday.Enabled = sender.checked
      End Select
    End If

  End Sub ' chk_day_CheckedChanged

  ' PURPOSE: Controls grid buttons
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    
  Private Sub uc_grid_table_points_ButtonClickEvent(ByVal ButtonId As uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE) Handles uc_grid_table_points.ButtonClickEvent
    Dim _row_number As Int32

    Select Case ButtonId

      Case uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE.BUTTON_TYPE_ADD
        If Me.uc_grid_table_points.NumRows <= MAX_NUMBER_OF_GRID_ROWS Then
          If Not IsNothing(Me.uc_grid_table_points.SelectedRows) AndAlso Me.uc_grid_table_points.SelectedRows.Length > 0 Then
            Me.uc_grid_table_points.IsSelected(Me.uc_grid_table_points.SelectedRows(0)) = False
          End If
          _row_number = Me.uc_grid_table_points.AddRow()
          Me.uc_grid_table_points.IsSelected(_row_number) = True
          Me.uc_grid_table_points.TopRow = _row_number
        End If

      Case uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE.BUTTON_TYPE_DELETE
        If Not IsNothing(Me.uc_grid_table_points.SelectedRows) AndAlso Me.uc_grid_table_points.SelectedRows.Length > 0 Then
          Me.uc_grid_table_points.DeleteRow(Me.uc_grid_table_points.SelectedRows(0))
        End If

    End Select

  End Sub ' uc_grid_table_points_ButtonClickEvent

  ' PURPOSE: Controls gift button click event and raises and event
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    
  Private Sub btn_GiftEdit_ClickEvent() Handles btn_GiftEdit.ClickEvent
    Dim _idx_row As Int32
    Dim _gift_id As Int64

    ' Search the first row selected
    For _idx_row = 0 To Me.uc_grid_table_points.NumRows - 1
      If Me.uc_grid_table_points.Row(_idx_row).IsSelected Then

        Exit For
      End If
    Next

    If _idx_row = Me.uc_grid_table_points.NumRows Then
      _gift_id = 0
    Else
      ' Get the Gift ID
      _gift_id = IIf(String.IsNullOrEmpty(Me.uc_grid_table_points.Cell(_idx_row, GRID_COLUMN_GIFT_ID).Value), 0, Me.uc_grid_table_points.Cell(_idx_row, GRID_COLUMN_GIFT_ID).Value)
    End If

    'raise the event
    RaiseEvent ButtonClickEvent(ENUM_BUTTON_TYPE.TYPE_EDIT, _gift_id)

  End Sub ' btn_GiftEdit_ClickEvent

  ' PURPOSE: Controls the grid's start edition
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '   
  Private Sub uc_grid_table_points_BeforeStartEditionEvent(ByVal Row As Integer, ByVal Column As Integer, ByRef EditionCanceled As Boolean) Handles uc_grid_table_points.BeforeStartEditionEvent

    Try
      'We copy the old value
      m_temp_grid_value = Me.uc_grid_table_points.Cell(Row, Column).Value

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)
    End Try

  End Sub ' uc_grid_table_points_BeforeStartEditionEvent

  ' PURPOSE: Controls the grid's cell data edition changed
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '   
  Private Sub uc_grid_table_points_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles uc_grid_table_points.CellDataChangedEvent
    Dim _points As Double

    Try

      With Me.uc_grid_table_points

        If Column = GIFT_COLUMN_IS_VIP Then

          Return
        End If

        If String.IsNullOrEmpty(.Cell(Row, Column).Value) Then

          Return
        End If
        ' We don't allow negative points 
        _points = IIf(String.IsNullOrEmpty(.Cell(Row, Column).Value), 0, .Cell(Row, Column).Value)
        If _points <= 0 Then
          .Cell(Row, Column).Value = m_temp_grid_value 'We paste the old value

          Return
        End If
      End With

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)
    End Try

  End Sub ' uc_grid_table_points_CellDataChangedEvent

  ' PURPOSE: Enables or disables the gift button
  '
  '    - INPUT:
  '       -SelectedRow: Row that contains the gift.
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    
  Private Sub uc_grid_table_points_RowSelectedEvent(ByVal SelectedRow As Integer) Handles uc_grid_table_points.RowSelectedEvent
    ' 04-NOV-2013  JPJ    Defect WIG-366 Permissions visible/invisible for enabled/disabled.
    If m_gift_permission Then
      If String.IsNullOrEmpty(Me.uc_grid_table_points.Cell(SelectedRow, GRID_COLUMN_GIFT_ID).Value.ToString) Then
        Me.btn_GiftEdit.Enabled = False
      Else
        Me.btn_GiftEdit.Enabled = True
      End If
    End If

  End Sub ' uc_grid_table_points_RowSelectedEvent

  ' PURPOSE: Controls the selected credit and changes the grid header with acording name
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    
  Private Sub opt_credit_non_redeemable_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_credit_non_redeemable.CheckedChanged
    Dim _game_return_type_list As List(Of WSI.Common.PrizeType)

    _game_return_type_list = New List(Of WSI.Common.PrizeType)

    Try
      '   - Credits
      If Not (Me.uc_grid_table_points) Is Nothing Then
        If Me.uc_grid_table_points.NumColumns > 0 Then
          If Me.opt_credit_non_redeemable.Checked = True Then
            Me.uc_grid_table_points.Column(GRID_COLUMN_CREDITS).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(338)
            _game_return_type_list.Add(WSI.Common.PrizeType.NoRedeemable)
          Else
            Me.uc_grid_table_points.Column(GRID_COLUMN_CREDITS).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(337)
            _game_return_type_list.Add(WSI.Common.PrizeType.Redeemable)
          End If

          Call Me.SetComboSource(_game_return_type_list)
        End If
      End If

      lbl_terminals.Enabled = Me.opt_credit_non_redeemable.Checked
      Call ActivateTerminalLists(Me.opt_credit_non_redeemable.Checked)

    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)
    End Try

  End Sub ' opt_credit_non_redeemable_CheckedChanged

  ' PURPOSE: Controls leave event of the entry field and raises and event
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    
  Private Sub ef_points_credit_name_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles ef_points_credit_name.Leave

    RaiseEvent TableNameLeaveEvent(Me.ef_points_credit_name.TextValue.Trim)

  End Sub ' ef_points_credit_name_Leave

  ' PURPOSE: Activates or desactivates all terminal lists
  '
  '    - INPUT:
  '      - Activate: Inicates if the control has to be activated or not
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    
  Private Sub ActivateTerminalLists(ByVal Activate As Boolean)
    'Defect WIG-320 Activate only the terminal lists that have their day checked
    Me.uc_terminals_group_sunday.Enabled = IIf(Activate, Me.chk_sunday.Checked, False)
    Me.uc_terminals_group_monday.Enabled = IIf(Activate, Me.chk_monday.Checked, False)
    Me.uc_terminals_group_tuesday.Enabled = IIf(Activate, Me.chk_tuesday.Checked, False)
    Me.uc_terminals_group_wednesday.Enabled = IIf(Activate, Me.chk_wednesday.Checked, False)
    Me.uc_terminals_group_thursday.Enabled = IIf(Activate, Me.chk_thursday.Checked, False)
    Me.uc_terminals_group_friday.Enabled = IIf(Activate, Me.chk_friday.Checked, False)
    Me.uc_terminals_group_saturday.Enabled = IIf(Activate, Me.chk_saturday.Checked, False)

  End Sub ' ActivateTerminalLists

  Private Sub chk_award_with_game_CheckedChanged(sender As Object, e As EventArgs) Handles chk_award_with_game.CheckedChanged
    Me.cmb_award_with_game.Enabled = Me.chk_award_with_game.Checked

    If Not Me.chk_award_with_game.Checked Then
      Me.cmb_award_with_game.SelectedIndex = -1
    End If
  End Sub

#End Region 'Events


End Class
