<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_provider
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(uc_provider))
    Me.gb_terminal = New System.Windows.Forms.GroupBox()
    Me.btn_massive_search = New System.Windows.Forms.Button()
    Me.opt_one_terminal = New System.Windows.Forms.RadioButton()
    Me.opt_all_types = New System.Windows.Forms.RadioButton()
    Me.opt_several_types = New System.Windows.Forms.RadioButton()
    Me.dg_filter = New GUI_Controls.uc_grid()
    Me.cmb_terminal = New GUI_Controls.uc_combo()
    Me.cmb_currencies = New GUI_Controls.uc_combo()
    Me.lbl_currency = New System.Windows.Forms.Label()
    Me.gb_terminal.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_terminal
    '
    Me.gb_terminal.Controls.Add(Me.opt_one_terminal)
    Me.gb_terminal.Controls.Add(Me.opt_all_types)
    Me.gb_terminal.Controls.Add(Me.opt_several_types)
    Me.gb_terminal.Controls.Add(Me.dg_filter)
    Me.gb_terminal.Controls.Add(Me.cmb_terminal)
    Me.gb_terminal.Controls.Add(Me.cmb_currencies)
    Me.gb_terminal.Controls.Add(Me.lbl_currency)
    Me.gb_terminal.Location = New System.Drawing.Point(3, 4)
    Me.gb_terminal.Name = "gb_terminal"
    Me.gb_terminal.Size = New System.Drawing.Size(321, 174)
    Me.gb_terminal.TabIndex = 5
    Me.gb_terminal.TabStop = False
    Me.gb_terminal.Text = "xTerminals"
    '
    'btn_massive_search
    '
    Me.btn_massive_search.Location = New System.Drawing.Point(9, 107)
    Me.btn_massive_search.Name = "btn_massive_search"
    Me.btn_massive_search.Size = New System.Drawing.Size(38, 23)
    Me.btn_massive_search.TabIndex = 9
    Me.btn_massive_search.Text = "..."
    Me.btn_massive_search.UseVisualStyleBackColor = True
    Me.btn_massive_search.Visible = False
    '
    'opt_one_terminal
    '
    Me.opt_one_terminal.Location = New System.Drawing.Point(5, 13)
    Me.opt_one_terminal.Name = "opt_one_terminal"
    Me.opt_one_terminal.Size = New System.Drawing.Size(70, 24)
    Me.opt_one_terminal.TabIndex = 5
    Me.opt_one_terminal.Text = "xTerminal"
    '
    'opt_all_types
    '
    Me.opt_all_types.Location = New System.Drawing.Point(5, 73)
    Me.opt_all_types.Name = "opt_all_types"
    Me.opt_all_types.Size = New System.Drawing.Size(70, 24)
    Me.opt_all_types.TabIndex = 1
    Me.opt_all_types.Text = "xAll"
    '
    'opt_several_types
    '
    Me.opt_several_types.Location = New System.Drawing.Point(5, 43)
    Me.opt_several_types.Name = "opt_several_types"
    Me.opt_several_types.Size = New System.Drawing.Size(70, 24)
    Me.opt_several_types.TabIndex = 0
    Me.opt_several_types.Text = "xSeveral"
    '
    'dg_filter
    '
    Me.dg_filter.CurrentCol = -1
    Me.dg_filter.CurrentRow = -1
    Me.dg_filter.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_filter.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_filter.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_filter.Location = New System.Drawing.Point(80, 36)
    Me.dg_filter.Name = "dg_filter"
    Me.dg_filter.NoCountRowsWithSmallHeight = False
    Me.dg_filter.PanelRightVisible = False
    Me.dg_filter.Redraw = True
    Me.dg_filter.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_filter.Size = New System.Drawing.Size(225, 135)
    Me.dg_filter.Sortable = False
    Me.dg_filter.SortAscending = True
    Me.dg_filter.SortByCol = 0
    Me.dg_filter.TabIndex = 2
    Me.dg_filter.ToolTipped = True
    Me.dg_filter.TopRow = -2
    Me.dg_filter.WordWrap = False
    '
    'cmb_terminal
    '
    Me.cmb_terminal.AllowUnlistedValues = False
    Me.cmb_terminal.AutoCompleteMode = False
    Me.cmb_terminal.IsReadOnly = False
    Me.cmb_terminal.Location = New System.Drawing.Point(80, 10)
    Me.cmb_terminal.Name = "cmb_terminal"
    Me.cmb_terminal.SelectedIndex = -1
    Me.cmb_terminal.Size = New System.Drawing.Size(225, 24)
    Me.cmb_terminal.SufixText = "Sufix Text"
    Me.cmb_terminal.SufixTextVisible = True
    Me.cmb_terminal.TabIndex = 6
    Me.cmb_terminal.TextCombo = Nothing
    Me.cmb_terminal.TextVisible = False
    Me.cmb_terminal.TextWidth = 0
    '
    'cmb_currencies
    '
    Me.cmb_currencies.AllowUnlistedValues = False
    Me.cmb_currencies.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.cmb_currencies.AutoCompleteMode = False
    Me.cmb_currencies.IsReadOnly = False
    Me.cmb_currencies.Location = New System.Drawing.Point(5, 141)
    Me.cmb_currencies.Name = "cmb_currencies"
    Me.cmb_currencies.SelectedIndex = -1
    Me.cmb_currencies.Size = New System.Drawing.Size(60, 24)
    Me.cmb_currencies.SufixText = "Sufix Text"
    Me.cmb_currencies.SufixTextVisible = True
    Me.cmb_currencies.TabIndex = 8
    Me.cmb_currencies.TextCombo = Nothing
    Me.cmb_currencies.TextVisible = False
    Me.cmb_currencies.TextWidth = 0
    '
    'lbl_currency
    '
    Me.lbl_currency.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.lbl_currency.AutoSize = True
    Me.lbl_currency.Location = New System.Drawing.Point(8, 123)
    Me.lbl_currency.Name = "lbl_currency"
    Me.lbl_currency.Size = New System.Drawing.Size(54, 13)
    Me.lbl_currency.TabIndex = 7
    Me.lbl_currency.Text = "xCurrency"
    '
    'uc_provider
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.btn_massive_search)
    Me.Controls.Add(Me.gb_terminal)
    Me.Name = "uc_provider"
    Me.Size = New System.Drawing.Size(325, 179)
    Me.gb_terminal.ResumeLayout(False)
    Me.gb_terminal.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_terminal As System.Windows.Forms.GroupBox
  Friend WithEvents dg_filter As GUI_Controls.uc_grid
  Public WithEvents cmb_currencies As GUI_Controls.uc_combo
  Friend WithEvents lbl_currency As System.Windows.Forms.Label
  Protected WithEvents opt_all_types As System.Windows.Forms.RadioButton
  Protected WithEvents opt_several_types As System.Windows.Forms.RadioButton
  Protected WithEvents opt_one_terminal As System.Windows.Forms.RadioButton
  Public WithEvents cmb_terminal As GUI_Controls.uc_combo
  Friend WithEvents btn_massive_search As System.Windows.Forms.Button


End Class
