<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_alesis_cashier_config
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
Me.gb_terminals = New System.Windows.Forms.GroupBox
Me.gb_captions = New System.Windows.Forms.GroupBox
Me.lbl_caption_ok = New System.Windows.Forms.Label
Me.lbl_caption_error = New System.Windows.Forms.Label
Me.Panel1 = New System.Windows.Forms.Panel
Me.lbl_caption_not_conf = New System.Windows.Forms.Label
Me.Panel2 = New System.Windows.Forms.Panel
Me.lbl_caption_pending = New System.Windows.Forms.Label
Me.Panel3 = New System.Windows.Forms.Panel
Me.Panel4 = New System.Windows.Forms.Panel
Me.cb_not_configured_terminals = New System.Windows.Forms.CheckBox
Me.cb_configured_terminals = New System.Windows.Forms.CheckBox
Me.gb_conn_params = New System.Windows.Forms.GroupBox
Me.pnl_global = New System.Windows.Forms.Panel
Me.Uc_entry_vendor_id = New GUI_Controls.uc_entry_field
Me.Uc_entry_sql_password = New GUI_Controls.uc_entry_field
Me.Uc_entry_sql_user = New GUI_Controls.uc_entry_field
Me.Uc_entry_database_name = New GUI_Controls.uc_entry_field
Me.Uc_entry_ip_address = New GUI_Controls.uc_entry_field
Me.Uc_grid_configured = New GUI_Controls.uc_grid
Me.gb_terminals.SuspendLayout()
Me.gb_captions.SuspendLayout()
Me.gb_conn_params.SuspendLayout()
Me.pnl_global.SuspendLayout()
Me.SuspendLayout()
'
'gb_terminals
'
Me.gb_terminals.Controls.Add(Me.gb_captions)
Me.gb_terminals.Controls.Add(Me.cb_not_configured_terminals)
Me.gb_terminals.Controls.Add(Me.cb_configured_terminals)
Me.gb_terminals.Controls.Add(Me.Uc_grid_configured)
Me.gb_terminals.Location = New System.Drawing.Point(14, 188)
Me.gb_terminals.Name = "gb_terminals"
Me.gb_terminals.Size = New System.Drawing.Size(451, 399)
Me.gb_terminals.TabIndex = 1
Me.gb_terminals.TabStop = False
Me.gb_terminals.Text = "xTerminalesConfigurados"
'
'gb_captions
'
Me.gb_captions.Controls.Add(Me.lbl_caption_ok)
Me.gb_captions.Controls.Add(Me.lbl_caption_error)
Me.gb_captions.Controls.Add(Me.Panel1)
Me.gb_captions.Controls.Add(Me.lbl_caption_not_conf)
Me.gb_captions.Controls.Add(Me.Panel2)
Me.gb_captions.Controls.Add(Me.lbl_caption_pending)
Me.gb_captions.Controls.Add(Me.Panel3)
Me.gb_captions.Controls.Add(Me.Panel4)
Me.gb_captions.Location = New System.Drawing.Point(182, 14)
Me.gb_captions.Name = "gb_captions"
Me.gb_captions.Size = New System.Drawing.Size(251, 61)
Me.gb_captions.TabIndex = 7
Me.gb_captions.TabStop = False
Me.gb_captions.Text = "GroupBox1"
'
'lbl_caption_ok
'
Me.lbl_caption_ok.AutoSize = True
Me.lbl_caption_ok.Location = New System.Drawing.Point(39, 16)
Me.lbl_caption_ok.Name = "lbl_caption_ok"
Me.lbl_caption_ok.Size = New System.Drawing.Size(39, 13)
Me.lbl_caption_ok.TabIndex = 0
Me.lbl_caption_ok.Text = "Label3"
'
'lbl_caption_error
'
Me.lbl_caption_error.AutoSize = True
Me.lbl_caption_error.ForeColor = System.Drawing.Color.Black
Me.lbl_caption_error.Location = New System.Drawing.Point(145, 39)
Me.lbl_caption_error.Name = "lbl_caption_error"
Me.lbl_caption_error.Size = New System.Drawing.Size(39, 13)
Me.lbl_caption_error.TabIndex = 0
Me.lbl_caption_error.Text = "Label1"
'
'Panel1
'
Me.Panel1.BackColor = System.Drawing.Color.Red
Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
Me.Panel1.Location = New System.Drawing.Point(123, 35)
Me.Panel1.Name = "Panel1"
Me.Panel1.Size = New System.Drawing.Size(16, 16)
Me.Panel1.TabIndex = 3
'
'lbl_caption_not_conf
'
Me.lbl_caption_not_conf.AutoSize = True
Me.lbl_caption_not_conf.Location = New System.Drawing.Point(145, 16)
Me.lbl_caption_not_conf.Name = "lbl_caption_not_conf"
Me.lbl_caption_not_conf.Size = New System.Drawing.Size(39, 13)
Me.lbl_caption_not_conf.TabIndex = 0
Me.lbl_caption_not_conf.Text = "Label2"
'
'Panel2
'
Me.Panel2.BackColor = System.Drawing.Color.LightGray
Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
Me.Panel2.Location = New System.Drawing.Point(123, 13)
Me.Panel2.Name = "Panel2"
Me.Panel2.Size = New System.Drawing.Size(16, 16)
Me.Panel2.TabIndex = 4
'
'lbl_caption_pending
'
Me.lbl_caption_pending.AutoSize = True
Me.lbl_caption_pending.Location = New System.Drawing.Point(39, 38)
Me.lbl_caption_pending.Name = "lbl_caption_pending"
Me.lbl_caption_pending.Size = New System.Drawing.Size(39, 13)
Me.lbl_caption_pending.TabIndex = 0
Me.lbl_caption_pending.Text = "Label4"
'
'Panel3
'
Me.Panel3.BackColor = System.Drawing.Color.White
Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
Me.Panel3.Location = New System.Drawing.Point(19, 15)
Me.Panel3.Name = "Panel3"
Me.Panel3.Size = New System.Drawing.Size(16, 16)
Me.Panel3.TabIndex = 5
'
'Panel4
'
Me.Panel4.BackColor = System.Drawing.Color.Khaki
Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
Me.Panel4.Location = New System.Drawing.Point(19, 35)
Me.Panel4.Name = "Panel4"
Me.Panel4.Size = New System.Drawing.Size(16, 16)
Me.Panel4.TabIndex = 6
'
'cb_not_configured_terminals
'
Me.cb_not_configured_terminals.AutoSize = True
Me.cb_not_configured_terminals.Location = New System.Drawing.Point(6, 52)
Me.cb_not_configured_terminals.Name = "cb_not_configured_terminals"
Me.cb_not_configured_terminals.Size = New System.Drawing.Size(150, 17)
Me.cb_not_configured_terminals.TabIndex = 1
Me.cb_not_configured_terminals.Text = "xNot Configured Terminals"
Me.cb_not_configured_terminals.UseVisualStyleBackColor = True
'
'cb_configured_terminals
'
Me.cb_configured_terminals.AutoSize = True
Me.cb_configured_terminals.Location = New System.Drawing.Point(6, 29)
Me.cb_configured_terminals.Name = "cb_configured_terminals"
Me.cb_configured_terminals.Size = New System.Drawing.Size(130, 17)
Me.cb_configured_terminals.TabIndex = 0
Me.cb_configured_terminals.Text = "xConfigured Terminals"
Me.cb_configured_terminals.UseVisualStyleBackColor = True
'
'gb_conn_params
'
Me.gb_conn_params.Controls.Add(Me.Uc_entry_vendor_id)
Me.gb_conn_params.Controls.Add(Me.Uc_entry_sql_password)
Me.gb_conn_params.Controls.Add(Me.Uc_entry_sql_user)
Me.gb_conn_params.Controls.Add(Me.Uc_entry_database_name)
Me.gb_conn_params.Controls.Add(Me.Uc_entry_ip_address)
Me.gb_conn_params.Location = New System.Drawing.Point(14, 12)
Me.gb_conn_params.Name = "gb_conn_params"
Me.gb_conn_params.Size = New System.Drawing.Size(451, 170)
Me.gb_conn_params.TabIndex = 0
Me.gb_conn_params.TabStop = False
Me.gb_conn_params.Text = "xConnectionParams"
'
'pnl_global
'
Me.pnl_global.Controls.Add(Me.gb_conn_params)
Me.pnl_global.Controls.Add(Me.gb_terminals)
Me.pnl_global.Location = New System.Drawing.Point(3, 0)
Me.pnl_global.Name = "pnl_global"
Me.pnl_global.Size = New System.Drawing.Size(470, 590)
Me.pnl_global.TabIndex = 1
'
'Uc_entry_vendor_id
'
Me.Uc_entry_vendor_id.DoubleValue = 0
Me.Uc_entry_vendor_id.IntegerValue = 0
Me.Uc_entry_vendor_id.IsReadOnly = False
Me.Uc_entry_vendor_id.Location = New System.Drawing.Point(11, 19)
Me.Uc_entry_vendor_id.Name = "Uc_entry_vendor_id"
Me.Uc_entry_vendor_id.Size = New System.Drawing.Size(422, 24)
Me.Uc_entry_vendor_id.SufixText = "Sufix Text"
Me.Uc_entry_vendor_id.SufixTextVisible = True
Me.Uc_entry_vendor_id.TabIndex = 0
Me.Uc_entry_vendor_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
Me.Uc_entry_vendor_id.TextValue = ""
Me.Uc_entry_vendor_id.TextWidth = 200
Me.Uc_entry_vendor_id.Value = ""
'
'Uc_entry_sql_password
'
Me.Uc_entry_sql_password.DoubleValue = 0
Me.Uc_entry_sql_password.IntegerValue = 0
Me.Uc_entry_sql_password.IsReadOnly = False
Me.Uc_entry_sql_password.Location = New System.Drawing.Point(11, 139)
Me.Uc_entry_sql_password.Name = "Uc_entry_sql_password"
Me.Uc_entry_sql_password.Size = New System.Drawing.Size(422, 24)
Me.Uc_entry_sql_password.SufixText = "Sufix Text"
Me.Uc_entry_sql_password.SufixTextVisible = True
Me.Uc_entry_sql_password.TabIndex = 4
Me.Uc_entry_sql_password.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
Me.Uc_entry_sql_password.TextValue = ""
Me.Uc_entry_sql_password.TextWidth = 200
Me.Uc_entry_sql_password.Value = ""
'
'Uc_entry_sql_user
'
Me.Uc_entry_sql_user.DoubleValue = 0
Me.Uc_entry_sql_user.IntegerValue = 0
Me.Uc_entry_sql_user.IsReadOnly = False
Me.Uc_entry_sql_user.Location = New System.Drawing.Point(11, 109)
Me.Uc_entry_sql_user.Name = "Uc_entry_sql_user"
Me.Uc_entry_sql_user.Size = New System.Drawing.Size(422, 24)
Me.Uc_entry_sql_user.SufixText = "Sufix Text"
Me.Uc_entry_sql_user.SufixTextVisible = True
Me.Uc_entry_sql_user.TabIndex = 3
Me.Uc_entry_sql_user.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
Me.Uc_entry_sql_user.TextValue = ""
Me.Uc_entry_sql_user.TextWidth = 200
Me.Uc_entry_sql_user.Value = ""
'
'Uc_entry_database_name
'
Me.Uc_entry_database_name.DoubleValue = 0
Me.Uc_entry_database_name.IntegerValue = 0
Me.Uc_entry_database_name.IsReadOnly = False
Me.Uc_entry_database_name.Location = New System.Drawing.Point(11, 79)
Me.Uc_entry_database_name.Name = "Uc_entry_database_name"
Me.Uc_entry_database_name.Size = New System.Drawing.Size(422, 24)
Me.Uc_entry_database_name.SufixText = "Sufix Text"
Me.Uc_entry_database_name.SufixTextVisible = True
Me.Uc_entry_database_name.TabIndex = 2
Me.Uc_entry_database_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
Me.Uc_entry_database_name.TextValue = ""
Me.Uc_entry_database_name.TextWidth = 200
Me.Uc_entry_database_name.Value = ""
'
'Uc_entry_ip_address
'
Me.Uc_entry_ip_address.DoubleValue = 0
Me.Uc_entry_ip_address.IntegerValue = 0
Me.Uc_entry_ip_address.IsReadOnly = False
Me.Uc_entry_ip_address.Location = New System.Drawing.Point(11, 49)
Me.Uc_entry_ip_address.Name = "Uc_entry_ip_address"
Me.Uc_entry_ip_address.Size = New System.Drawing.Size(422, 24)
Me.Uc_entry_ip_address.SufixText = "Sufix Text"
Me.Uc_entry_ip_address.SufixTextVisible = True
Me.Uc_entry_ip_address.TabIndex = 1
Me.Uc_entry_ip_address.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
Me.Uc_entry_ip_address.TextValue = ""
Me.Uc_entry_ip_address.TextWidth = 200
Me.Uc_entry_ip_address.Value = ""
'
'Uc_grid_configured
'
Me.Uc_grid_configured.CurrentCol = -1
Me.Uc_grid_configured.CurrentRow = -1
Me.Uc_grid_configured.Location = New System.Drawing.Point(6, 81)
Me.Uc_grid_configured.Name = "Uc_grid_configured"
Me.Uc_grid_configured.PanelRightVisible = True
Me.Uc_grid_configured.Redraw = True
Me.Uc_grid_configured.Size = New System.Drawing.Size(427, 312)
Me.Uc_grid_configured.Sortable = False
Me.Uc_grid_configured.SortAscending = True
Me.Uc_grid_configured.SortByCol = 0
Me.Uc_grid_configured.TabIndex = 2
Me.Uc_grid_configured.ToolTipped = True
Me.Uc_grid_configured.TopRow = -2
'
'uc_alesis_cashier_config
'
Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
Me.Controls.Add(Me.pnl_global)
Me.Name = "uc_alesis_cashier_config"
Me.Size = New System.Drawing.Size(475, 593)
Me.gb_terminals.ResumeLayout(False)
Me.gb_terminals.PerformLayout()
Me.gb_captions.ResumeLayout(False)
Me.gb_captions.PerformLayout()
Me.gb_conn_params.ResumeLayout(False)
Me.pnl_global.ResumeLayout(False)
Me.ResumeLayout(False)

End Sub
  Friend WithEvents Uc_grid_configured As GUI_Controls.uc_grid
  Friend WithEvents gb_terminals As System.Windows.Forms.GroupBox
  Friend WithEvents gb_conn_params As System.Windows.Forms.GroupBox
  Friend WithEvents Uc_entry_sql_password As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_sql_user As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_database_name As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_ip_address As GUI_Controls.uc_entry_field
  Friend WithEvents Uc_entry_vendor_id As GUI_Controls.uc_entry_field
  Friend WithEvents pnl_global As System.Windows.Forms.Panel
  Friend WithEvents cb_not_configured_terminals As System.Windows.Forms.CheckBox
  Friend WithEvents cb_configured_terminals As System.Windows.Forms.CheckBox
  Friend WithEvents Panel4 As System.Windows.Forms.Panel
  Friend WithEvents Panel3 As System.Windows.Forms.Panel
  Friend WithEvents Panel2 As System.Windows.Forms.Panel
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
  Friend WithEvents lbl_caption_pending As System.Windows.Forms.Label
  Friend WithEvents lbl_caption_ok As System.Windows.Forms.Label
  Friend WithEvents lbl_caption_not_conf As System.Windows.Forms.Label
  Friend WithEvents lbl_caption_error As System.Windows.Forms.Label
  Friend WithEvents gb_captions As System.Windows.Forms.GroupBox

End Class
