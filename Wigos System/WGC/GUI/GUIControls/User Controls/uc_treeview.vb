'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd. 
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_treeview
' DESCRIPTION:   Tree view with mixed state (something selected but not all)
'                Recursive status aplication (childs and parents)
' AUTHOR:        Rafael Xandri
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 02-NOV-2012  RXM    Initial version
' 29-JUL-2013  RBG    Adapation to WigosGUI
'--------------------------------------------------------------------


Public Class uc_treeview

#Region " Enums "

  Public Enum CHECKED_STATE
    UNINITIALISED = -1
    UNCHECKED = 0
    CHECKED = 1
    MIXED = 2
  End Enum

  Public Enum TRISTATESTYLES
    STANDARD = 0
    INSTALLER = 1
  End Enum

#End Region

#Region " Members "

  Dim m_ignore_click_action As Integer = 0
  Dim m_tristate_style As TRISTATESTYLES = TRISTATESTYLES.STANDARD
  Dim m_checkboxes As Boolean

#End Region

#Region " Properties "

  Public Property TriStateStyleProperty() As TRISTATESTYLES
    Get
      Return m_tristate_style
    End Get
    Set(ByVal _value As TRISTATESTYLES)
      m_tristate_style = _value
    End Set
  End Property

  Public Property CheckBoxesActive() As Boolean
    Get
      Return m_checkboxes
    End Get
    Set(ByVal _value As Boolean)
      m_checkboxes = _value
    End Set
  End Property


#End Region

#Region " Constructor "

  Public Sub New(Optional ByVal ActivateCheckBoxes As Boolean = False)

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    m_checkboxes = ActivateCheckBoxes

    ' Add any initialization after the InitializeComponent() call.
    Me.LineColor = System.Drawing.Color.Empty
    Me.Location = New System.Drawing.Point(0, 0)
    Me.Size = New System.Drawing.Size(121, 97)
    Me.TabIndex = 0
    Me.ResumeLayout(False)

    Dim _bmp As System.Drawing.Bitmap
    Dim _chk_graphics As System.Drawing.Graphics
    StateImageList = New System.Windows.Forms.ImageList()
    For i As Integer = 0 To 2 Step 1
      _bmp = New System.Drawing.Bitmap(16, 16)
      _chk_graphics = System.Drawing.Graphics.FromImage(_bmp)
      Select Case i
        Case 0
          System.Windows.Forms.CheckBoxRenderer.DrawCheckBox(_chk_graphics, New System.Drawing.Point(0, 1), System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal)
        Case 1
          System.Windows.Forms.CheckBoxRenderer.DrawCheckBox(_chk_graphics, New System.Drawing.Point(0, 1), System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal)
        Case 2
          System.Windows.Forms.CheckBoxRenderer.DrawCheckBox(_chk_graphics, New System.Drawing.Point(0, 1), System.Windows.Forms.VisualStyles.CheckBoxState.MixedNormal)
      End Select
      StateImageList.Images.Add(_bmp)
    Next
  End Sub

#End Region

#Region " Overrides "

  Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)
    MyBase.OnPaint(e)

    'Add your custom paint code here
  End Sub

  Protected Overrides Sub OnCreateControl()
    MyBase.OnCreateControl()

    If (m_checkboxes) Then
      CheckBoxes = False
      m_ignore_click_action += 1
      UpdateChildState(Me.Nodes, CType(CHECKED_STATE.UNCHECKED, Integer), False, True)
      m_ignore_click_action -= 1
    End If

  End Sub

  Protected Overrides Sub OnAfterCheck(ByVal e As System.Windows.Forms.TreeViewEventArgs)
    Dim _tree_node As System.Windows.Forms.TreeNode
    MyBase.OnAfterCheck(e)

    If (m_checkboxes) Then
      If (m_ignore_click_action > 0) Then
        Return
      End If
      m_ignore_click_action += 1
      _tree_node = e.Node
      If _tree_node.Checked Then
        _tree_node.StateImageIndex = CType(CHECKED_STATE.CHECKED, Integer)
      Else
        _tree_node.StateImageIndex = CType(CHECKED_STATE.UNCHECKED, Integer)
      End If
      ' force all children to inherit the same state as the current node
      UpdateChildState(e.Node.Nodes, e.Node.StateImageIndex, e.Node.Checked, False)
      ' populate state up the tree, possibly resulting in parents with mixed state
      UpdateParentState(e.Node.Parent)
      m_ignore_click_action -= 1
    End If

  End Sub

  Protected Overrides Sub OnAfterExpand(ByVal e As System.Windows.Forms.TreeViewEventArgs)
    MyBase.OnAfterExpand(e)

    If (m_checkboxes) Then
      m_ignore_click_action += 1
      UpdateChildState(e.Node.Nodes, e.Node.StateImageIndex, e.Node.Checked, True)
      m_ignore_click_action -= 1
    End If

  End Sub

  Protected Overrides Sub OnKeyDown(ByVal e As System.Windows.Forms.KeyEventArgs)
    MyBase.OnKeyDown(e)
    If (m_checkboxes) Then
      If (e.KeyCode = System.Windows.Forms.Keys.Space) Then
        SelectedNode.Checked = Not SelectedNode.Checked
      End If
    End If
  End Sub

  Protected Overrides Sub OnNodeMouseClick(ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs)
    Dim _tree_view_info As System.Windows.Forms.TreeViewHitTestInfo
    Dim _tree_node As System.Windows.Forms.TreeNode
    MyBase.OnNodeMouseClick(e)

    If (m_checkboxes) Then
      _tree_view_info = HitTest(e.X, e.Y)
      If (IsNothing(_tree_view_info) Or _tree_view_info.Location <> System.Windows.Forms.TreeViewHitTestLocations.StateImage) Then
        Return
      End If
      _tree_node = e.Node
      _tree_node.Checked = Not _tree_node.Checked
    End If

  End Sub

#End Region

#Region " Public "

  Public Sub SelectedElementsFromXml(ByVal List As String)

  End Sub

  Public Function SelectedElementsToXml() As String

    Return ""

  End Function

#End Region

#Region " Protected "

  Protected Sub UpdateChildState(ByVal Nodes As System.Windows.Forms.TreeNodeCollection, ByVal StateImageIndex As Integer, ByVal Checked As Boolean, ByVal ChangeUninitialisedNodesOnly As Boolean)
    For Each _child As System.Windows.Forms.TreeNode In Nodes
      If (Not ChangeUninitialisedNodesOnly Or _child.StateImageIndex = -1) Then
        _child.StateImageIndex = StateImageIndex
        _child.Checked = Checked
        If (_child.Nodes.Count > 0) Then
          UpdateChildState(_child.Nodes, StateImageIndex, Checked, ChangeUninitialisedNodesOnly)
        End If
      End If
    Next
  End Sub

  Protected Sub UpdateParentState(ByVal TreeNode As System.Windows.Forms.TreeNode)
    Dim _original_state_image_index As Integer
    Dim _count_unchecked As Integer
    Dim _count_checked As Integer
    Dim _count_mixed As Integer

    ' Node needs to check all of it's children to see if any of them are ticked or mixed
    If (IsNothing(TreeNode)) Then
      Return
    End If

    _original_state_image_index = TreeNode.StateImageIndex
    _count_unchecked = 0
    _count_checked = 0
    _count_mixed = 0
    For Each _child As System.Windows.Forms.TreeNode In TreeNode.Nodes
      If (_child.StateImageIndex = CType(CHECKED_STATE.CHECKED, Integer)) Then
        _count_checked += 1
      ElseIf (_child.StateImageIndex = CType(CHECKED_STATE.MIXED, Integer)) Then
        _count_mixed += 1
      Else
        _count_unchecked += 1
      End If
    Next

    If (m_tristate_style = TRISTATESTYLES.INSTALLER) Then
      If (_count_mixed = 0) Then

        If (_count_unchecked = 0) Then

          TreeNode.Checked = True
        Else
          TreeNode.Checked = False
        End If
      End If
    End If

    If (_count_mixed > 0) Then
      TreeNode.StateImageIndex = CType(CHECKED_STATE.MIXED, Integer)
    ElseIf (_count_checked > 0 And _count_unchecked = 0) Then
      '    If (TreeNode.Checked) Then
      TreeNode.Checked = True
      TreeNode.StateImageIndex = CType(CHECKED_STATE.CHECKED, Integer)
      ' Else
      '  TreeNode.StateImageIndex = CType(CHECKED_STATE.MIXED, Integer)
      '   End If
    ElseIf (_count_checked > 0) Then
      TreeNode.StateImageIndex = CType(CHECKED_STATE.MIXED, Integer)
    Else
      If (_count_checked = 0 And _count_unchecked <> 0 And _count_mixed = 0) Then
        TreeNode.StateImageIndex = CType(CHECKED_STATE.UNCHECKED, Integer)
      End If
    End If

    If (_original_state_image_index <> TreeNode.StateImageIndex And Not IsNothing(TreeNode.Parent)) Then
      UpdateParentState(TreeNode.Parent)
    End If
  End Sub

#End Region

End Class
