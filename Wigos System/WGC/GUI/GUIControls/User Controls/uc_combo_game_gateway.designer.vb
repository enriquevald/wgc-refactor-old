<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_combo_game_gateway
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_providers_games = New System.Windows.Forms.GroupBox()
    Me.uc_combo_providers = New GUI_Controls.uc_combo()
    Me.uc_combo_games = New GUI_Controls.uc_combo()
    Me.gb_providers_games.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_providers_games
    '
    Me.gb_providers_games.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.gb_providers_games.Controls.Add(Me.uc_combo_providers)
    Me.gb_providers_games.Controls.Add(Me.uc_combo_games)
    Me.gb_providers_games.Location = New System.Drawing.Point(3, 3)
    Me.gb_providers_games.Name = "gb_providers_games"
    Me.gb_providers_games.Size = New System.Drawing.Size(309, 74)
    Me.gb_providers_games.TabIndex = 2
    Me.gb_providers_games.TabStop = False
    Me.gb_providers_games.Text = "xProvider/Game"
    '
    'uc_combo_providers
    '
    Me.uc_combo_providers.AllowUnlistedValues = False
    Me.uc_combo_providers.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.uc_combo_providers.AutoCompleteMode = False
    Me.uc_combo_providers.IsReadOnly = False
    Me.uc_combo_providers.Location = New System.Drawing.Point(7, 15)
    Me.uc_combo_providers.Name = "uc_combo_providers"
    Me.uc_combo_providers.SelectedIndex = -1
    Me.uc_combo_providers.Size = New System.Drawing.Size(297, 24)
    Me.uc_combo_providers.SufixText = "Sufix Text"
    Me.uc_combo_providers.SufixTextVisible = True
    Me.uc_combo_providers.TabIndex = 0
    Me.uc_combo_providers.TextCombo = ""
    '
    'uc_combo_games
    '
    Me.uc_combo_games.AllowUnlistedValues = False
    Me.uc_combo_games.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.uc_combo_games.AutoCompleteMode = False
    Me.uc_combo_games.IsReadOnly = False
    Me.uc_combo_games.Location = New System.Drawing.Point(7, 45)
    Me.uc_combo_games.Name = "uc_combo_games"
    Me.uc_combo_games.SelectedIndex = -1
    Me.uc_combo_games.Size = New System.Drawing.Size(297, 24)
    Me.uc_combo_games.SufixText = "Sufix Text"
    Me.uc_combo_games.SufixTextVisible = True
    Me.uc_combo_games.TabIndex = 1
    Me.uc_combo_games.TextCombo = Nothing
    '
    'uc_combo_game_gateway
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.gb_providers_games)
    Me.Name = "uc_combo_game_gateway"
    Me.Size = New System.Drawing.Size(317, 82)
    Me.gb_providers_games.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_combo_providers As GUI_Controls.uc_combo
  Friend WithEvents uc_combo_games As GUI_Controls.uc_combo
  Friend WithEvents gb_providers_games As System.Windows.Forms.GroupBox


End Class
