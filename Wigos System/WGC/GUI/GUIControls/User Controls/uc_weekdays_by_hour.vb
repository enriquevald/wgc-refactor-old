﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' UC NAME:       uc_weekdays_by_hour
' DESCRIPTION:   User control for control weekDays by hour every day
' AUTHOR:        Rubén Lama Ordóñez
' CREATION DATE: 30-MAR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 30-MAR-2017  RLO    Initial version
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports System.Data.OleDb
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Math
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports WSI.Common
Imports System.Collections
Imports System.Globalization
Imports System.Drawing

Public Class uc_weekdays_by_hour

#Region " Members "

  Public Delegate Sub CheckedChangedEventHandler()

  Private m_default_date_to As Date
  Private m_default_date_from As Date


#End Region

#Region " Properties "

  Public Property Days As uc_week_days
    Get
      Return Me.uc_days
    End Get
    Set(value As uc_week_days)
      Me.uc_days = value
    End Set
  End Property

  Public Property WorkingToMonday As uc_date_picker
    Get
      Return Me.dtp_working_to_monday
    End Get
    Set(value As uc_date_picker)
      Me.dtp_working_to_monday = value
    End Set
  End Property

  Public Property WorkingFromMonday As uc_date_picker
    Get
      Return Me.dtp_working_from_monday
    End Get
    Set(value As uc_date_picker)
      Me.dtp_working_from_monday = value
    End Set
  End Property

  Public Property WorkingToTuesday As uc_date_picker
    Get
      Return Me.dtp_working_to_tuesday
    End Get
    Set(value As uc_date_picker)
      Me.dtp_working_to_tuesday = value
    End Set
  End Property

  Public Property WorkingFromTuesday As uc_date_picker
    Get
      Return Me.dtp_working_from_tuesday
    End Get
    Set(value As uc_date_picker)
      Me.dtp_working_from_tuesday = value
    End Set
  End Property

  Public Property WorkingToWednesday As uc_date_picker
    Get
      Return Me.dtp_working_to_wednesday
    End Get
    Set(value As uc_date_picker)
      Me.dtp_working_to_wednesday = value
    End Set
  End Property

  Public Property WorkingFromWednesday As uc_date_picker
    Get
      Return Me.dtp_working_from_wednesday
    End Get
    Set(value As uc_date_picker)
      Me.dtp_working_from_wednesday = value
    End Set
  End Property

  Public Property WorkingToThursday As uc_date_picker
    Get
      Return Me.dtp_working_to_thursday
    End Get
    Set(value As uc_date_picker)
      Me.dtp_working_to_thursday = value
    End Set
  End Property

  Public Property WorkingFromThursday As uc_date_picker
    Get
      Return Me.dtp_working_from_thursday
    End Get
    Set(value As uc_date_picker)
      Me.dtp_working_from_thursday = value
    End Set
  End Property

  Public Property WorkingToFriday As uc_date_picker
    Get
      Return Me.dtp_working_to_friday
    End Get
    Set(value As uc_date_picker)
      Me.dtp_working_to_friday = value
    End Set
  End Property

  Public Property WorkingFromFriday As uc_date_picker
    Get
      Return Me.dtp_working_from_friday
    End Get
    Set(value As uc_date_picker)
      Me.dtp_working_from_friday = value
    End Set
  End Property

  Public Property WorkingToSaturday As uc_date_picker
    Get
      Return Me.dtp_working_to_saturday
    End Get
    Set(value As uc_date_picker)
      Me.dtp_working_to_saturday = value
    End Set
  End Property

  Public Property WorkingFromSaturday As uc_date_picker
    Get
      Return Me.dtp_working_from_saturday
    End Get
    Set(value As uc_date_picker)
      Me.dtp_working_from_saturday = value
    End Set
  End Property

  Public Property WorkingToSunday As uc_date_picker
    Get
      Return Me.dtp_working_to_sunday
    End Get
    Set(value As uc_date_picker)
      Me.dtp_working_to_sunday = value
    End Set
  End Property

  Public Property WorkingFromSunday As uc_date_picker
    Get
      Return Me.dtp_working_from_sunday
    End Get
    Set(value As uc_date_picker)
      Me.dtp_working_from_sunday = value
    End Set
  End Property

#End Region

#Region " Public Sub "

  ''' <summary>
  ''' Sub To init Controls
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub InitControls()

    dtp_working_from_monday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(204)
    dtp_working_to_monday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(205)

    dtp_working_from_tuesday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(204)
    dtp_working_to_tuesday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(205)

    dtp_working_from_wednesday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(204)
    dtp_working_to_wednesday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(205)

    dtp_working_from_thursday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(204)
    dtp_working_to_thursday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(205)

    dtp_working_from_friday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(204)
    dtp_working_to_friday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(205)

    dtp_working_from_saturday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(204)
    dtp_working_to_saturday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(205)

    dtp_working_from_sunday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(204)
    dtp_working_to_sunday.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(205)

    ' Set date picker obj properties
    Me.dtp_working_from_monday.SetFormat(ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_working_from_monday.ShowUpDown = True

    ' Set date picker obj properties
    Me.dtp_working_to_monday.SetFormat(ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_working_to_monday.ShowUpDown = True

    ' Set date picker obj properties
    Me.dtp_working_from_tuesday.SetFormat(ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_working_from_tuesday.ShowUpDown = True

    ' Set date picker obj properties
    Me.dtp_working_to_tuesday.SetFormat(ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_working_to_tuesday.ShowUpDown = True

    ' Set date picker obj properties
    Me.dtp_working_from_wednesday.SetFormat(ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_working_from_wednesday.ShowUpDown = True

    ' Set date picker obj properties
    Me.dtp_working_to_wednesday.SetFormat(ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_working_to_wednesday.ShowUpDown = True

    ' Set date picker obj properties
    Me.dtp_working_from_thursday.SetFormat(ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_working_from_thursday.ShowUpDown = True

    ' Set date picker obj properties
    Me.dtp_working_to_thursday.SetFormat(ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_working_to_thursday.ShowUpDown = True

    ' Set date picker obj properties
    Me.dtp_working_from_friday.SetFormat(ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_working_from_friday.ShowUpDown = True

    ' Set date picker obj properties
    Me.dtp_working_to_friday.SetFormat(ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_working_to_friday.ShowUpDown = True

    ' Set date picker obj properties
    Me.dtp_working_from_saturday.SetFormat(ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_working_from_saturday.ShowUpDown = True

    ' Set date picker obj properties
    Me.dtp_working_to_saturday.SetFormat(ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_working_to_saturday.ShowUpDown = True

    ' Set date picker obj properties
    Me.dtp_working_from_sunday.SetFormat(ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_working_from_sunday.ShowUpDown = True

    ' Set date picker obj properties
    Me.dtp_working_to_sunday.SetFormat(ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ModuleDateTimeFormats.ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_working_to_sunday.ShowUpDown = True

    'Events

    AddHandler Me.uc_days.chk_monday.CheckedChanged, AddressOf MondayChkCheckedChange
    AddHandler Me.uc_days.chk_tuesday.CheckedChanged, AddressOf TuesdayChkCheckedChange
    AddHandler Me.uc_days.chk_wednesday.CheckedChanged, AddressOf WednesdayChkCheckedChange
    AddHandler Me.uc_days.chk_thursday.CheckedChanged, AddressOf ThursdayChkCheckedChange
    AddHandler Me.uc_days.chk_friday.CheckedChanged, AddressOf FridayChkCheckedChange
    AddHandler Me.uc_days.chk_saturday.CheckedChanged, AddressOf SaturdayChkCheckedChange
    AddHandler Me.uc_days.chk_sunday.CheckedChanged, AddressOf SundayChkCheckedChange

    m_default_date_from = HourInMinutesToDate(Jackpot.Jackpot.DEFAULT_TIME_FROM_VALUE)
    m_default_date_to = HourInMinutesToDate(Jackpot.Jackpot.DEFAULT_TIME_TO_VALUE)

    Call SortByFirstDayOfWeek()

  End Sub








  ''' <summary>
  ''' Sub to format time 
  ''' </summary>
  ''' <param name="Seconds"></param>
  ''' <param name="Dd"></param>
  ''' <param name="Hh"></param>
  ''' <param name="Mm"></param>
  ''' <param name="Ss"></param>
  ''' <remarks></remarks>
  Public Sub Format_SecondsToDdHhMmSs(ByVal Seconds As Integer, _
                                    ByRef Dd As Integer, _
                                    ByRef Hh As Integer, _
                                    ByRef Mm As Integer, _
                                    ByRef Ss As Integer)
    Dim temp As Integer

    Dd = Seconds \ SECONDS_X_DAY
    temp = Seconds Mod SECONDS_X_DAY
    Hh = temp \ SECONDS_X_HOUR
    temp = temp Mod SECONDS_X_HOUR
    Mm = temp \ SECONDS_X_MINUTE
    Ss = temp Mod SECONDS_X_MINUTE

  End Sub

  ''' <summary>
  ''' Function to check if any day is selected
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function CheckIfAnySelected()
    If uc_days.GetNumDaysSelected() = 0 Then

      Return False
    End If

    Return True
  End Function

  ''' <summary>
  ''' Function to check values from datepickers
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function CheckDatePickerValues()
    If dtp_working_from_monday.Value = dtp_working_to_monday.Value And Days.chk_monday.Checked Then

      Return False
    End If

    If dtp_working_from_tuesday.Value = dtp_working_to_tuesday.Value And Days.chk_tuesday.Checked Then

      Return False
    End If

    If dtp_working_from_wednesday.Value = dtp_working_to_wednesday.Value And Days.chk_wednesday.Checked Then

      Return False
    End If

    If dtp_working_from_thursday.Value = dtp_working_to_thursday.Value And Days.chk_thursday.Checked Then

      Return False
    End If

    If dtp_working_from_friday.Value = dtp_working_to_friday.Value And Days.chk_friday.Checked Then

      Return False
    End If

    If dtp_working_from_saturday.Value = dtp_working_to_saturday.Value And Days.chk_saturday.Checked Then

      Return False
    End If

    If dtp_working_from_sunday.Value = dtp_working_to_sunday.Value And Days.chk_sunday.Checked Then

      Return False
    End If

    Return True
  End Function

  ''' <summary>
  ''' Function to check date from is before to date to
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function CheckFromDateIsBeforeToDate()
    If dtp_working_from_monday.Value >= dtp_working_to_monday.Value And Days.chk_monday.Checked Then

      Return False
    End If

    If dtp_working_from_tuesday.Value >= dtp_working_to_tuesday.Value And Days.chk_tuesday.Checked Then

      Return False
    End If

    If dtp_working_from_wednesday.Value >= dtp_working_to_wednesday.Value And Days.chk_wednesday.Checked Then

      Return False
    End If

    If dtp_working_from_thursday.Value >= dtp_working_to_thursday.Value And Days.chk_thursday.Checked Then

      Return False
    End If

    If dtp_working_from_friday.Value >= dtp_working_to_friday.Value And Days.chk_friday.Checked Then

      Return False
    End If

    If dtp_working_from_saturday.Value >= dtp_working_to_saturday.Value And Days.chk_saturday.Checked Then

      Return False
    End If

    If dtp_working_from_sunday.Value >= dtp_working_to_sunday.Value And Days.chk_sunday.Checked Then

      Return False
    End If

    Return True
  End Function


#End Region

#Region " Constants "

  Public Const SECONDS_X_DAY As Integer = 86400
  Public Const SECONDS_X_HOUR As Integer = 3600
  Public Const SECONDS_X_MINUTE As Integer = 60

#End Region

#Region " Members "


#End Region

#Region " IButtonControl "

#End Region

#Region " Private Events "



#End Region

  Private Sub MondayChkCheckedChange()
    Me.dtp_working_from_monday.Enabled = Me.uc_days.chk_monday.Checked
    Me.dtp_working_to_monday.Enabled = Me.uc_days.chk_monday.Checked

    If Me.uc_days.chk_monday.Checked Then
      Me.dtp_working_from_monday.Value = m_default_date_from
      Me.dtp_working_to_monday.Value = m_default_date_to
    End If

  End Sub

  Private Sub TuesdayChkCheckedChange()
    Me.dtp_working_from_tuesday.Enabled = Me.uc_days.chk_tuesday.Checked
    Me.dtp_working_to_tuesday.Enabled = Me.uc_days.chk_tuesday.Checked

    If Me.uc_days.chk_tuesday.Checked Then
      Me.dtp_working_from_tuesday.Value = m_default_date_from
      Me.dtp_working_to_tuesday.Value = m_default_date_to
    End If

  End Sub
  Private Sub WednesdayChkCheckedChange()
    Me.dtp_working_from_wednesday.Enabled = Me.uc_days.chk_wednesday.Checked
    Me.dtp_working_to_wednesday.Enabled = Me.uc_days.chk_wednesday.Checked

    If Me.uc_days.chk_wednesday.Checked Then
      Me.dtp_working_from_wednesday.Value = m_default_date_from
      Me.dtp_working_to_wednesday.Value = m_default_date_to
    End If

  End Sub
  Private Sub ThursdayChkCheckedChange()
    Me.dtp_working_from_thursday.Enabled = Me.uc_days.chk_thursday.Checked
    Me.dtp_working_to_thursday.Enabled = Me.uc_days.chk_thursday.Checked

    If Me.uc_days.chk_thursday.Checked Then
      Me.dtp_working_from_thursday.Value = m_default_date_from
      Me.dtp_working_to_thursday.Value = m_default_date_to
    End If

  End Sub
  Private Sub FridayChkCheckedChange()
    Me.dtp_working_from_friday.Enabled = Me.uc_days.chk_friday.Checked
    Me.dtp_working_to_friday.Enabled = Me.uc_days.chk_friday.Checked

    If Me.uc_days.chk_friday.Checked Then
      Me.dtp_working_from_friday.Value = m_default_date_from
      Me.dtp_working_to_friday.Value = m_default_date_to
    End If

  End Sub
  Private Sub SaturdayChkCheckedChange()
    Me.dtp_working_from_saturday.Enabled = Me.uc_days.chk_saturday.Checked
    Me.dtp_working_to_saturday.Enabled = Me.uc_days.chk_saturday.Checked

    If Me.uc_days.chk_saturday.Checked Then
      Me.dtp_working_from_saturday.Value = m_default_date_from
      Me.dtp_working_to_saturday.Value = m_default_date_to
    End If

  End Sub
  Private Sub SundayChkCheckedChange()
    Me.dtp_working_from_sunday.Enabled = Me.uc_days.chk_sunday.Checked
    Me.dtp_working_to_sunday.Enabled = Me.uc_days.chk_sunday.Checked


    If Me.uc_days.chk_sunday.Checked Then
      Me.dtp_working_from_sunday.Value = m_default_date_from
      Me.dtp_working_to_sunday.Value = m_default_date_to
    End If

  End Sub




  Private Function HourInMinutesToDate(ByVal Minutes As Integer) As Date
    Return New DateTime(WGDB.Now.Year, WGDB.Now.Month, WGDB.Now.Day).AddMinutes(Minutes)
  End Function


  Private Sub SortByFirstDayOfWeek()
    Dim _locations_from_array(6) As Drawing.Point
    Dim _locations_to_array(6) As Drawing.Point
    Dim _dtp_working_from_array(6) As uc_date_picker
    Dim _dtp_working_to_array(6) As uc_date_picker
    Dim _first_day_of_week_check_box As Int32
    Dim _idx_days As Int32
    Dim _idx_chek_box As Int32

    Select Case CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek()
      Case DayOfWeek.Monday
        Return
      Case DayOfWeek.Tuesday
        _first_day_of_week_check_box = 1
      Case DayOfWeek.Wednesday
        _first_day_of_week_check_box = 2
      Case DayOfWeek.Thursday
        _first_day_of_week_check_box = 3
      Case DayOfWeek.Friday
        _first_day_of_week_check_box = 4
      Case DayOfWeek.Saturday
        _first_day_of_week_check_box = 5
      Case DayOfWeek.Sunday
        _first_day_of_week_check_box = 6
      Case Else
        Return
    End Select

    _locations_from_array(0) = Me.dtp_working_from_monday.Location
    _locations_from_array(1) = Me.dtp_working_from_tuesday.Location
    _locations_from_array(2) = Me.dtp_working_from_wednesday.Location
    _locations_from_array(3) = Me.dtp_working_from_thursday.Location
    _locations_from_array(4) = Me.dtp_working_from_friday.Location
    _locations_from_array(5) = Me.dtp_working_from_saturday.Location
    _locations_from_array(6) = Me.dtp_working_from_sunday.Location


    _locations_to_array(0) = Me.dtp_working_to_monday.Location
    _locations_to_array(1) = Me.dtp_working_to_tuesday.Location
    _locations_to_array(2) = Me.dtp_working_to_wednesday.Location
    _locations_to_array(3) = Me.dtp_working_to_thursday.Location
    _locations_to_array(4) = Me.dtp_working_to_friday.Location
    _locations_to_array(5) = Me.dtp_working_to_saturday.Location
    _locations_to_array(6) = Me.dtp_working_to_sunday.Location


    _dtp_working_from_array(0) = Me.dtp_working_from_monday
    _dtp_working_from_array(1) = Me.dtp_working_from_tuesday
    _dtp_working_from_array(2) = Me.dtp_working_from_wednesday
    _dtp_working_from_array(3) = Me.dtp_working_from_thursday
    _dtp_working_from_array(4) = Me.dtp_working_from_friday
    _dtp_working_from_array(5) = Me.dtp_working_from_saturday
    _dtp_working_from_array(6) = Me.dtp_working_from_sunday

    _dtp_working_to_array(0) = Me.dtp_working_to_monday
    _dtp_working_to_array(1) = Me.dtp_working_to_tuesday
    _dtp_working_to_array(2) = Me.dtp_working_to_wednesday
    _dtp_working_to_array(3) = Me.dtp_working_to_thursday
    _dtp_working_to_array(4) = Me.dtp_working_to_friday
    _dtp_working_to_array(5) = Me.dtp_working_to_saturday
    _dtp_working_to_array(6) = Me.dtp_working_to_sunday

    For _idx_days = 0 To 6
      _idx_chek_box = (_first_day_of_week_check_box + _idx_days) Mod 7
      _dtp_working_from_array(_idx_chek_box).Location = _locations_from_array(_idx_days)
      _dtp_working_to_array(_idx_chek_box).Location = _locations_to_array(_idx_days)
    Next

   


  End Sub


End Class
