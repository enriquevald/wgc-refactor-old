﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_multi_currency_site_sel
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(uc_multi_currency_site_sel))
    Me.gb_multi_currency_site = New System.Windows.Forms.GroupBox()
    Me.tlp_multi_currency = New System.Windows.Forms.TableLayoutPanel()
    Me.rb_total_to_isocode = New System.Windows.Forms.RadioButton()
    Me.rb_by_currency = New System.Windows.Forms.RadioButton()
    Me.lbl_by_currency = New System.Windows.Forms.Label()
    Me.lbl_total_to_isocode = New System.Windows.Forms.Label()
    Me.cmb_multi_currency_site = New GUI_Controls.uc_combo()
    Me.gb_multi_currency_site.SuspendLayout()
    Me.tlp_multi_currency.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_multi_currency_site
    '
    Me.gb_multi_currency_site.Controls.Add(Me.tlp_multi_currency)
    Me.gb_multi_currency_site.Dock = System.Windows.Forms.DockStyle.Fill
    Me.gb_multi_currency_site.Location = New System.Drawing.Point(0, 0)
    Me.gb_multi_currency_site.Name = "gb_multi_currency_site"
    Me.gb_multi_currency_site.Size = New System.Drawing.Size(169, 77)
    Me.gb_multi_currency_site.TabIndex = 0
    Me.gb_multi_currency_site.TabStop = False
    Me.gb_multi_currency_site.Text = "xMultiCurrency"
    '
    'tlp_multi_currency
    '
    Me.tlp_multi_currency.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.tlp_multi_currency.BackColor = System.Drawing.Color.Transparent
    Me.tlp_multi_currency.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
    Me.tlp_multi_currency.ColumnCount = 3
    Me.tlp_multi_currency.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.0!))
    Me.tlp_multi_currency.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
    Me.tlp_multi_currency.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.0!))
    Me.tlp_multi_currency.Controls.Add(Me.rb_total_to_isocode, 0, 0)
    Me.tlp_multi_currency.Controls.Add(Me.rb_by_currency, 0, 1)
    Me.tlp_multi_currency.Controls.Add(Me.cmb_multi_currency_site, 2, 1)
    Me.tlp_multi_currency.Controls.Add(Me.lbl_by_currency, 1, 1)
    Me.tlp_multi_currency.Controls.Add(Me.lbl_total_to_isocode, 1, 0)
    Me.tlp_multi_currency.Dock = System.Windows.Forms.DockStyle.Fill
    Me.tlp_multi_currency.Location = New System.Drawing.Point(3, 16)
    Me.tlp_multi_currency.Name = "tlp_multi_currency"
    Me.tlp_multi_currency.RowCount = 2
    Me.tlp_multi_currency.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
    Me.tlp_multi_currency.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50.0!))
    Me.tlp_multi_currency.Size = New System.Drawing.Size(163, 58)
    Me.tlp_multi_currency.TabIndex = 1
    '
    'rb_total_to_isocode
    '
    Me.rb_total_to_isocode.Dock = System.Windows.Forms.DockStyle.Fill
    Me.rb_total_to_isocode.Location = New System.Drawing.Point(3, 3)
    Me.rb_total_to_isocode.Name = "rb_total_to_isocode"
    Me.rb_total_to_isocode.Size = New System.Drawing.Size(13, 19)
    Me.rb_total_to_isocode.TabIndex = 1
    Me.rb_total_to_isocode.TabStop = True
    Me.rb_total_to_isocode.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.rb_total_to_isocode.UseVisualStyleBackColor = True
    '
    'rb_by_currency
    '
    Me.rb_by_currency.Location = New System.Drawing.Point(3, 28)
    Me.rb_by_currency.Name = "rb_by_currency"
    Me.rb_by_currency.Size = New System.Drawing.Size(13, 27)
    Me.rb_by_currency.TabIndex = 2
    Me.rb_by_currency.TabStop = True
    Me.rb_by_currency.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.rb_by_currency.UseVisualStyleBackColor = True
    '
    'lbl_by_currency
    '
    Me.lbl_by_currency.Location = New System.Drawing.Point(22, 25)
    Me.lbl_by_currency.Name = "lbl_by_currency"
    Me.lbl_by_currency.Size = New System.Drawing.Size(75, 30)
    Me.lbl_by_currency.TabIndex = 7
    Me.lbl_by_currency.Text = "xByCurrency"
    Me.lbl_by_currency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_total_to_isocode
    '
    Me.lbl_total_to_isocode.Location = New System.Drawing.Point(22, 0)
    Me.lbl_total_to_isocode.Name = "lbl_total_to_isocode"
    Me.lbl_total_to_isocode.Size = New System.Drawing.Size(75, 25)
    Me.lbl_total_to_isocode.TabIndex = 6
    Me.lbl_total_to_isocode.Text = "xTotalTo"
    Me.lbl_total_to_isocode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'cmb_multi_currency_site
    '
    Me.cmb_multi_currency_site.AllowUnlistedValues = False
    Me.cmb_multi_currency_site.AutoCompleteMode = False
    Me.cmb_multi_currency_site.IsReadOnly = False
    Me.cmb_multi_currency_site.Location = New System.Drawing.Point(103, 28)
    Me.cmb_multi_currency_site.Name = "cmb_multi_currency_site"
    Me.cmb_multi_currency_site.SelectedIndex = -1
    Me.cmb_multi_currency_site.Size = New System.Drawing.Size(57, 24)
    Me.cmb_multi_currency_site.SufixText = "Sufix Text"
    Me.cmb_multi_currency_site.SufixTextVisible = True
    Me.cmb_multi_currency_site.TabIndex = 5
    Me.cmb_multi_currency_site.TextCombo = ""
    Me.cmb_multi_currency_site.TextVisible = False
    Me.cmb_multi_currency_site.TextWidth = 0
    '
    'uc_multi_currency_site_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.gb_multi_currency_site)
    Me.Name = "uc_multi_currency_site_sel"
    Me.Size = New System.Drawing.Size(169, 77)
    Me.gb_multi_currency_site.ResumeLayout(False)
    Me.tlp_multi_currency.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_multi_currency_site As System.Windows.Forms.GroupBox
  Friend WithEvents rb_total_to_isocode As System.Windows.Forms.RadioButton
  Friend WithEvents rb_by_currency As System.Windows.Forms.RadioButton
  Friend WithEvents tlp_multi_currency As System.Windows.Forms.TableLayoutPanel
  Friend WithEvents cmb_multi_currency_site As GUI_Controls.uc_combo
  Friend WithEvents lbl_by_currency As System.Windows.Forms.Label
  Friend WithEvents lbl_total_to_isocode As System.Windows.Forms.Label

End Class
