'-------------------------------------------------------------------
' Copyright © 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_sas_flags.vb  
'
' DESCRIPTION:   
'
' AUTHOR:        Javier Molina
'
' CREATION DATE: 24-DEC-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 24-DEC-2013  JMM    Initial version
' 24-JAN-2014  DRV    Added IsReadOnly function
' 29-SEP-2014  FJC    Add SAS_FLAG: SPECIAL_PROGRESSIVE_METER  
' 01-FEB-2016  JMV & RAB   Product Backlog Item 8857:GUI: Modificación de la pantalla de edición de terminales
' 16-JAN-2016  JMM    PBI 19744:Pago automático de handpays
' 16-FEB-2016  FAV    PBI 24711: Machine on reserve.
'--------------------------------------------------------------------

Imports System.Windows.Forms
Imports WSI.Common

Public Class uc_sas_flags
  Inherits System.Windows.Forms.UserControl

#Region " Enums "
  Public Enum ENUM_SAS_FLAGS_TYPE
    SITE_DEFAULT
    TERMINAL_EDIT
  End Enum ' ENUM_SAS_FLAGS_TYPE

  Public Enum ENUM_SAS_FLAGS_COMBOS
    EXTENDED_METERS
    PROMOTIONAL_CREDITS
    CMD1B_PROMOTIONAL_CREDITS
    PROGRESSIVE_METERS
  End Enum ' ENUM_SAS_FLAGS_COMBOS

#End Region ' Enums

#Region " Members "
  Private m_sas_flags_type As ENUM_SAS_FLAGS_TYPE

#End Region ' Members "

#Region " Constants "

#End Region  ' Constants "

#Region " Properties "

  Public Property ExtendedMeters() As ENUM_SAS_FLAGS
    Get
      Return Me.cmb_extended_meters.Value
    End Get
    Set(ByVal value As ENUM_SAS_FLAGS)
      Me.cmb_extended_meters.Value = value
    End Set
  End Property

  Public Property PromotionalCredits() As ENUM_SAS_FLAGS
    Get
      Return Me.cmb_promotional_credits.Value
    End Get
    Set(ByVal value As ENUM_SAS_FLAGS)
      Me.cmb_promotional_credits.Value = value
    End Set
  End Property

  Public Property Cmd1BHandPays() As ENUM_SAS_FLAGS
    Get
      Return Me.cmb_cmd1B_handpays.Value
    End Get
    Set(ByVal value As ENUM_SAS_FLAGS)
      Me.cmb_cmd1B_handpays.Value = value
    End Set
  End Property

  Public Property ProgressiveMeters() As ENUM_SAS_FLAGS
    Get
      Return Me.cmb_progressivemeters.Value
    End Get
    Set(ByVal value As ENUM_SAS_FLAGS)
      Me.cmb_progressivemeters.Value = value
    End Set
  End Property

  Public Property BCD4() As Integer
    Get
      'If canceled form when 4bcd value is empty
      If (Me.ef_4bcd.Value = "") Then
        Return 4
      End If
      Return Me.ef_4bcd.Value
    End Get
    Set(ByVal value As Integer)
      Me.ef_4bcd.Value = value
    End Set
  End Property

  Public Property BCD5() As Integer
    Get
      'If canceled form when 5bcd value is empty
      If (Me.ef_5bcd.Value = "") Then
        Return 5
      End If
      Return Me.ef_5bcd.Value
    End Get
    Set(ByVal value As Integer)
      Me.ef_5bcd.Value = value
    End Set
  End Property

  Public Property No_RTE() As Boolean
    Get
      Return Me.ckb_no_RTE.Checked
    End Get
    Set(ByVal value As Boolean)
      Me.ckb_no_RTE.Checked = value
    End Set
  End Property

  Public Property Single_byte() As Boolean
    Get
      Return Me.chk_single_byte.Checked
    End Get
    Set(ByVal value As Boolean)
      Me.chk_single_byte.Checked = value
    End Set
  End Property

  Public Property Ignore_no_bet_plays() As Boolean
    Get
      Return Me.chk_ignore_no_bet_plays.Checked
    End Get
    Set(ByVal value As Boolean)
      Me.chk_ignore_no_bet_plays.Checked = value
    End Set
  End Property

  Public Property AFT_although_lock_0() As Boolean
    Get
      Return Me.chk_aft_although_lock_0.Checked
    End Get
    Set(ByVal value As Boolean)
      Me.chk_aft_although_lock_0.Checked = value
    End Set
  End Property

  Public Property EnableDisableNoteAcceptor() As ENUM_SAS_FLAGS
    Get
      Return Me.cmb_enable_disable_note_acceptor.Value
    End Get
    Set(ByVal value As ENUM_SAS_FLAGS)
      Me.cmb_enable_disable_note_acceptor.Value = value
    End Set
  End Property

  Public Property ReserveTerminal() As ENUM_SAS_FLAGS

  Public Property LockEgmOnReserve() As ENUM_SAS_FLAGS
    Get
      If (chk_lock_egm_on_reserve.Checked) Then
        Return ENUM_SAS_FLAGS.ACTIVATED
      Else
        Return ENUM_SAS_FLAGS.NOT_ACTIVATED
      End If
    End Get
    Set(ByVal value As ENUM_SAS_FLAGS)
      Me.chk_lock_egm_on_reserve.Checked = (value = ENUM_SAS_FLAGS.ACTIVATED)
    End Set
  End Property

#End Region ' Properties

#Region " Public "

  Public Sub Init(ByVal SASFlagsType As ENUM_SAS_FLAGS_TYPE)

    m_sas_flags_type = SASFlagsType

    If m_sas_flags_type = ENUM_SAS_FLAGS_TYPE.TERMINAL_EDIT Then
      Me.cmb_extended_meters.Add(ENUM_SAS_FLAGS.SITE_DEFINED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4432)) 'Site defined"
      Me.cmb_promotional_credits.Add(ENUM_SAS_FLAGS.SITE_DEFINED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4432)) 'Site defined"
      Me.cmb_cmd1B_handpays.Add(ENUM_SAS_FLAGS.SITE_DEFINED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4432)) 'Site defined"
      Me.cmb_progressivemeters.Add(ENUM_SAS_FLAGS.SITE_DEFINED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4432)) 'Site defined"
      Me.cmb_enable_disable_note_acceptor.Add(ENUM_SAS_FLAGS.SITE_DEFINED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(4432)) '4432 "Definido por la sala"
    End If

    'Extended Counters -> changed JMV 01-FEB-2016
    Me.lbl_extended_meters.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2099) '2099 "Extended meters"
    Me.cmb_extended_meters.Add(ENUM_SAS_FLAGS.ACTIVATED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7081)) 'Extended meters
    Me.cmb_extended_meters.Add(ENUM_SAS_FLAGS.NOT_ACTIVATED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7082)) '1694 Non extended meters
    ' RAB 02-FEB-2016: Temporarily disabled
    'Me.cmb_extended_meters.Add(ENUM_SAS_FLAGS.EXTENDED_METERS_LENGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7083)) 'Extended meters length
    Me.cmb_extended_meters.Add(ENUM_SAS_FLAGS.NON_EXTENDED_METERS_LENGHT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7084)) 'Non extended meters length

    'Promotional Credits
    Me.lbl_promotions.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4434) '"xPromotional Credits"
    Me.cmb_promotional_credits.Add(ENUM_SAS_FLAGS.SYSTEM_DEFINED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1157)) 'System defined
    Me.cmb_promotional_credits.Add(ENUM_SAS_FLAGS.MACHINE_DEFINED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1158)) 'Machine defined

    'Progressive Meter
    Me.lbl_progressivemeters.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5590) 'Progressive Metters (Use EFT IN for progressive jackpots)"
    Me.cmb_progressivemeters.Add(ENUM_SAS_FLAGS.ACTIVATED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1696)) '1696 "Activated"
    Me.cmb_progressivemeters.Add(ENUM_SAS_FLAGS.NOT_ACTIVATED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1694)) '1694 "Not activated"

    '***************************************************************************************************
    'JMM 03-JAN-2014: PromotionalCredits combo readonly while SYSTEM_DEFINED is the only supported value
    '***************************************************************************************************
    Me.cmb_promotional_credits.IsReadOnly = True
    Me.cmb_promotional_credits.Value = ENUM_SAS_FLAGS.SYSTEM_DEFINED
    '***************************************************************************************************
    'JMM 03-JAN-2014: PromotionalCredits combo readonly while SYSTEM_DEFINED is the only supported value
    '***************************************************************************************************

    'SAS Command 0x1B 
    Me.lbl_handpays.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4433) '"xSAS Command 0x1B Handpays"
    Me.cmb_cmd1B_handpays.Add(ENUM_SAS_FLAGS.ACTIVATED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1696)) '1696 "Activated"
    Me.cmb_cmd1B_handpays.Add(ENUM_SAS_FLAGS.NOT_ACTIVATED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1694)) '1694 "Not activated"

    If Not TITO.Utils.IsTitoMode Or WSI.Common.Misc.IsMico2Mode Then
      Me.cmb_cmd1B_handpays.Add(ENUM_SAS_FLAGS.ACTIVATED_AND_AUTOMATICALLY, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7808)) '7808 "Activado & Automatic" 
    End If

    ' JMV 01-FEB-2016
    ef_4bcd.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 2, 0)
    ef_5bcd.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 2, 0)

    ef_4bcd.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7090) '4-BCD Digits
    ef_5bcd.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7091) '5-BCD Digits

    ' RAB: Product Backlog Item 8857:Bally 3.5 BCDs: GUI: Modificación de la pantalla de edición de terminales
    Me.ckb_no_RTE.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7088) 'No RTE
    Me.chk_single_byte.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7089) 'Single Byte Ex.
    Me.chk_ignore_no_bet_plays.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7111) 'Ignore no bet plays

    ' JMM 08-AUG-2016
    Me.chk_aft_although_lock_0.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7496)

    ' JMM 28-SEP-2016
    Me.lbl_enable_disable_note_acceptor.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7637) '7637 "Habilitar/Deshabilitar aceptador"
    Me.cmb_enable_disable_note_acceptor.Add(ENUM_SAS_FLAGS.ACTIVATED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)) '278 "Sí"
    Me.cmb_enable_disable_note_acceptor.Add(ENUM_SAS_FLAGS.NOT_ACTIVATED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)) '279 "No"

    ' FAV 16/02/2017
    Me.chk_lock_egm_on_reserve.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7914)

  End Sub ' Init

  ' PURPOSE: Sets comboboxes ReadOnly property to true
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub IsReadOnly(ByVal Combo As ENUM_SAS_FLAGS_COMBOS)

    Select Case Combo
      Case ENUM_SAS_FLAGS_COMBOS.CMD1B_PROMOTIONAL_CREDITS
        Me.cmb_cmd1B_handpays.IsReadOnly = True
      Case ENUM_SAS_FLAGS_COMBOS.EXTENDED_METERS
        Me.cmb_extended_meters.IsReadOnly = True
      Case ENUM_SAS_FLAGS_COMBOS.PROMOTIONAL_CREDITS
        Me.cmb_promotional_credits.IsReadOnly = True
      Case ENUM_SAS_FLAGS_COMBOS.PROGRESSIVE_METERS
        Me.cmb_progressivemeters.IsReadOnly = True
    End Select

  End Sub

#End Region ' Public

#Region " Private "

#End Region ' Private

#Region " Events "

#End Region ' Events


  Private Sub cmb_extended_meters_ValueChangedEvent() Handles cmb_extended_meters.ValueChangedEvent
    If (cmb_extended_meters.SelectedIndex = 3 Or cmb_extended_meters.SelectedIndex = 4) Then
      ef_4bcd.Visible = True
      ef_5bcd.Visible = True
    Else
      ef_4bcd.Visible = False
      ef_5bcd.Visible = False
    End If
  End Sub

End Class