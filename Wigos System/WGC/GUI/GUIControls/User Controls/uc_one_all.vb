'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_one_all.vb  
'
' DESCRIPTION:   
'
' AUTHOR:        Andreu Juli�
'
' CREATION DATE: 12-NOV-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 12-NOV-2002  AJQ    Initial version
'--------------------------------------------------------------------

Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE

'XVV -> Add NameSpace solve error with variables definitions
Imports System.Windows.Forms

Public Class uc_one_all
  Inherits System.Windows.Forms.UserControl

#Region " Members "
  Private m_selection_type As ENUM_SELECTION = ENUM_SELECTION.SELECTION_BY_COMBO
  Private m_init As Boolean = False


  Private m_last_opt As Integer = -1
  Private m_last_id As Integer = -1

#End Region

Public Enum ENUM_SELECTION
    SELECTION_BY_COMBO = 1
    SELECTION_BY_GRID = 2
  End Enum

#Region " Constants "
  Protected Const GRID_HEADER_ROWS = 0
  Protected Const GRID_COLUMNS = 3
  Protected Const GRID_COLUMN_GAME_ID As Integer = 0
  Protected Const GRID_COLUMN_CHECK_FLAG As Integer = 1
  Protected Const GRID_COLUMN_NAME As Integer = 2

#End Region


#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call
    Call Me.Init(0)

  End Sub

  'UserControl overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Private WithEvents gb_one_all As System.Windows.Forms.GroupBox
  Private WithEvents Panel1 As System.Windows.Forms.Panel
  Private WithEvents cmb_item As GUI_Controls.uc_combo
  Protected WithEvents pn_enabled_disabled As System.Windows.Forms.Panel
  Protected WithEvents opt_all As System.Windows.Forms.RadioButton
  Protected WithEvents opt_one As System.Windows.Forms.RadioButton
  Protected WithEvents dg_one_all As GUI_Controls.uc_grid
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.gb_one_all = New System.Windows.Forms.GroupBox()
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.dg_one_all = New GUI_Controls.uc_grid()
    Me.cmb_item = New GUI_Controls.uc_combo()
    Me.opt_all = New System.Windows.Forms.RadioButton()
    Me.opt_one = New System.Windows.Forms.RadioButton()
    Me.pn_enabled_disabled = New System.Windows.Forms.Panel()
    Me.gb_one_all.SuspendLayout()
    Me.Panel1.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_one_all
    '
    Me.gb_one_all.Controls.AddRange(New System.Windows.Forms.Control() {Me.Panel1, Me.pn_enabled_disabled})
    Me.gb_one_all.Dock = System.Windows.Forms.DockStyle.Top
    Me.gb_one_all.Name = "gb_one_all"
    Me.gb_one_all.Size = New System.Drawing.Size(488, 80)
    Me.gb_one_all.TabIndex = 0
    Me.gb_one_all.TabStop = False
    Me.gb_one_all.Text = "xOneAll"
    '
    'Panel1
    '
    Me.Panel1.Controls.AddRange(New System.Windows.Forms.Control() {Me.dg_one_all, Me.cmb_item, Me.opt_all, Me.opt_one})
    Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.Panel1.Location = New System.Drawing.Point(3, 16)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(342, 61)
    Me.Panel1.TabIndex = 0
    '
    'dg_one_all
    '
    Me.dg_one_all.Anchor = (((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right)
    Me.dg_one_all.AutoScroll = True
    Me.dg_one_all.CurrentCol = -1
    Me.dg_one_all.CurrentRow = -1
    Me.dg_one_all.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.dg_one_all.Location = New System.Drawing.Point(72, 0)
    Me.dg_one_all.Name = "dg_one_all"
    Me.dg_one_all.PanelRightVisible = False
    Me.dg_one_all.Redraw = True
    Me.dg_one_all.Size = New System.Drawing.Size(265, 52)
    Me.dg_one_all.TabIndex = 4
    Me.dg_one_all.Visible = False
    '
    'cmb_item
    '
    Me.cmb_item.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right)
    Me.cmb_item.Enabled = False
    Me.cmb_item.IsReadOnly = False
    Me.cmb_item.Location = New System.Drawing.Point(72, 4)
    Me.cmb_item.Name = "cmb_item"
    Me.cmb_item.SelectedIndex = -1
    Me.cmb_item.Size = New System.Drawing.Size(268, 24)
    Me.cmb_item.SufixText = "Sufix Text"
    Me.cmb_item.SufixTextVisible = True
    Me.cmb_item.TabIndex = 2
    Me.cmb_item.TextVisible = False
    Me.cmb_item.TextWidth = 0
    Me.cmb_item.Visible = False
    '
    'opt_all
    '
    Me.opt_all.Checked = True
    Me.opt_all.Location = New System.Drawing.Point(5, 32)
    Me.opt_all.Name = "opt_all"
    Me.opt_all.Size = New System.Drawing.Size(65, 24)
    Me.opt_all.TabIndex = 1
    Me.opt_all.TabStop = True
    Me.opt_all.Text = "xTodos"
    '
    'opt_one
    '
    Me.opt_one.Location = New System.Drawing.Point(5, 4)
    Me.opt_one.Name = "opt_one"
    Me.opt_one.Size = New System.Drawing.Size(65, 24)
    Me.opt_one.TabIndex = 0
    Me.opt_one.Text = "xUno"
    '
    'pn_enabled_disabled
    '
    Me.pn_enabled_disabled.Dock = System.Windows.Forms.DockStyle.Right
    Me.pn_enabled_disabled.Location = New System.Drawing.Point(345, 16)
    Me.pn_enabled_disabled.Name = "pn_enabled_disabled"
    Me.pn_enabled_disabled.Size = New System.Drawing.Size(140, 61)
    Me.pn_enabled_disabled.TabIndex = 1
    Me.pn_enabled_disabled.Visible = False
    '
    'uc_one_all
    '
    Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.gb_one_all})
    Me.Name = "uc_one_all"
    Me.Size = New System.Drawing.Size(488, 88)
    Me.gb_one_all.ResumeLayout(False)
    Me.Panel1.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Properties "
  Public Property SelectionType() As ENUM_SELECTION
    Get
      Return m_selection_type
    End Get
    Set(ByVal Value As ENUM_SELECTION)
      m_selection_type = Value
    End Set
  End Property

  Public Overrides Property Text() As String
    Get
      Return Me.gb_one_all.Text
    End Get
    Set(ByVal Value As String)
      Me.gb_one_all.Text = Value
    End Set
  End Property

  Public Property TextOne() As String
    Get
      Return Me.opt_one.Text
    End Get
    Set(ByVal Value As String)
      Me.opt_one.Text = Value
    End Set
  End Property

  Public Property TextAll() As String
    Get
      Return Me.opt_all.Text
    End Get
    Set(ByVal Value As String)
      Me.opt_all.Text = Value
    End Set
  End Property

  Public ReadOnly Property Combo() As uc_combo
    Get
      Return Me.cmb_item
    End Get
  End Property

  Public Property SelectedOne() As Boolean
    Get
      Return Me.opt_one.Checked
    End Get
    Set(ByVal Value As Boolean)
      If Value = Me.opt_one.Checked Then
        Exit Property
      End If
      Me.opt_one.Checked = Value
      Call UpdateControlStatus0(True)
    End Set
  End Property

  Public Property SelectedAll() As Boolean
    Get
      Return Me.opt_all.Checked
    End Get
    Set(ByVal Value As Boolean)
      If Value = Me.opt_all.Checked Then
        Exit Property
      End If
      Me.opt_all.Checked = Value
      Call UpdateControlStatus0(True)
    End Set
  End Property
#End Region
  '
  ' Updates the Status of the controls
  ' The status changes according the user input
  '
  Protected Overridable Sub UpdateControlStatus()
    If m_selection_type = ENUM_SELECTION.SELECTION_BY_COMBO Then
      If Me.cmb_item.Count <= 0 Then
        If Me.opt_all.Checked = False Then
          Me.opt_all.Checked = True
        End If
      End If
      Me.cmb_item.Enabled = Me.opt_one.Checked
    Else
      Me.dg_one_all.Enabled = Me.opt_one.Checked
    End If
  End Sub



  Private Sub RaiseEventSelectedOptionChanged()
    Dim sum As Integer

    sum = 0

    If Me.opt_one.Checked Then
      sum = sum + 1
    End If
    If Me.opt_all.Checked Then
      sum = sum + 2
    End If

    If sum <> m_last_opt Then
      m_last_opt = sum
      m_last_id = Me.cmb_item.Value
      RaiseEvent SelectedOptionChanged()
    End If
  End Sub

  Private Sub UpdateControlStatus0(ByVal RaiseEvents As Boolean)
    UpdateControlStatus()
    If RaiseEvents Then
      RaiseEventSelectedOptionChanged()
    End If
  End Sub

  Public Enum ENUM_ONE_ALL_GENDER
    ONE_ALL_GENDER_MALE = 0
    ONE_ALL_GENDER_FEMALE = 1
  End Enum

  Public Sub Init(ByVal NlsId As Integer, _
                  Optional ByVal Gender As ENUM_ONE_ALL_GENDER = ENUM_ONE_ALL_GENDER.ONE_ALL_GENDER_MALE, _
                  Optional ByVal SelectionType As ENUM_SELECTION = ENUM_SELECTION.SELECTION_BY_COMBO)

    Dim one_select_male_text As String
    Dim one_select_female_text As String

    Me.m_selection_type = SelectionType

    Me.dg_one_all.Visible = False
    Me.cmb_item.Visible = False
    Me.pn_enabled_disabled.Visible = False

    Me.dg_one_all.Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

    ' GAME_ID '
    'Me.dg_one_all.Column(GRID_COLUMN_GAME_ID).Header(0).Text = ""
    Me.dg_one_all.Column(GRID_COLUMN_GAME_ID).WidthFixed = 0

    'CHECK_FLAG'
    'Me.dg_one_all.Column(GRID_COLUMN_CHECK_FLAG).Header(0).Text = ""
    Me.dg_one_all.Column(GRID_COLUMN_CHECK_FLAG).WidthFixed = 300
    Me.dg_one_all.Column(GRID_COLUMN_CHECK_FLAG).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    Me.dg_one_all.Column(GRID_COLUMN_CHECK_FLAG).IsMerged = False
    Me.dg_one_all.Column(GRID_COLUMN_CHECK_FLAG).Editable = True
    Me.dg_one_all.Column(GRID_COLUMN_CHECK_FLAG).EditionControl.Type = CONTROL_TYPE_CHECK_BOX

    ' NAME '
    'Me.dg_one_all.Column(GRID_COLUMN_NAME).Header(0).Text = ""
    'Me.dg_one_all.Column(GRID_COLUMN_NAME).Width = 1000
    Me.dg_one_all.Column(GRID_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    Me.dg_one_all.Column(GRID_COLUMN_NAME).IsMerged = False


    If SelectionType = ENUM_SELECTION.SELECTION_BY_GRID Then
      Me.cmb_item.Enabled = False
      Me.cmb_item.Visible = False
      Me.dg_one_all.Enabled = Not opt_all.Checked
      Me.dg_one_all.Visible = True
      one_select_male_text = GLB_NLS_GUI_CONTROLS.GetString(291) ' "xVarios" 
      one_select_female_text = GLB_NLS_GUI_CONTROLS.GetString(292) ' "xVarias" 
    Else
      Me.cmb_item.Enabled = opt_one.Checked
      Me.cmb_item.Visible = True
      Me.dg_one_all.Enabled = False
      Me.dg_one_all.Visible = False
      one_select_male_text = GLB_NLS_GUI_CONTROLS.GetString(275)
      one_select_female_text = GLB_NLS_GUI_CONTROLS.GetString(277)
    End If

    If NlsId = 0 Then
      Me.Text = ""
    Else
      Me.Text = GUI_CommonMisc.NLS_GetString(NlsId)
    End If

    Select Case Gender
      Case ENUM_ONE_ALL_GENDER.ONE_ALL_GENDER_MALE
        Me.opt_one.Text = one_select_male_text
        Me.opt_all.Text = GLB_NLS_GUI_CONTROLS.GetString(276)

      Case ENUM_ONE_ALL_GENDER.ONE_ALL_GENDER_FEMALE
        Me.opt_one.Text = one_select_female_text
        Me.opt_all.Text = GLB_NLS_GUI_CONTROLS.GetString(278)

      Case Else
        Me.opt_one.Text = ""
        Me.opt_all.Text = ""

    End Select

    Call AddHandlers()
    m_init = True

  End Sub

#Region " Option Button Events "

  Private Sub OptionCheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim rb As RadioButton
    rb = sender
    If rb.Checked Then
      Call UpdateControlStatus0(True)
    End If
  End Sub

  Private Sub AddHandlers()
    AddHandler Me.opt_one.CheckedChanged, AddressOf OptionCheckedChanged
    AddHandler Me.opt_all.CheckedChanged, AddressOf OptionCheckedChanged
  End Sub


#End Region

  Protected Overridable Sub ComboSelectedItemChanged()
  End Sub

  Protected Overridable Sub GridSelectedItemChanged()
  End Sub


  Private Sub cmb_item_ValueChangedEvent() Handles cmb_item.ValueChangedEvent
    ComboSelectedItemChanged()
    RaiseEventSelectedOptionChanged()
  End Sub

  Public Event SelectedOptionChanged()

  Private Sub uc_one_all_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
    Call Me.UpdateControlStatus0(False)
  End Sub

  Private Sub dg_one_all_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_one_all.CellDataChangedEvent
    Call GridSelectedItemChanged()
  End Sub
End Class
