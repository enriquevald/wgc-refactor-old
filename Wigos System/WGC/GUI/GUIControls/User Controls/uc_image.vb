'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_image.vb
' DESCRIPTION:   Select an image 
' AUTHOR:        Marcos Piedra Osuna
' CREATION DATE: 07-MAY-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-MAY-2012  MPO    Initial version
' 28-AUG-2012  ANG    Add new resolution ( 600x480 ), Fix border size
' 30-AUG-2012  ANG    Add new property to change lbl_image_size font
' 09-SEP-2012  ANG    Add ImageChanged event and delete ImageDelete event
' 25-SEP-2014  JAB    Added new property to get or set video name.
' -------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations

Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Windows.Forms
Imports System.IO

Public Class uc_image

  Private Const NORMAL_WIDTH As Integer = 90
  Private Const NORMAL_HEIGHT As Integer = 30
  Private Const SMALL_WIDTH As Integer = NORMAL_WIDTH * 0.7
  Private Const SMALL_HEIGHT As Integer = NORMAL_HEIGHT * 0.7

  Public Enum MAXIMUN_RESOLUTION
    x200X150 = 1
    x400X300 = 2
    x1280X1024 = 3
  End Enum

  Public Event ImageSelected(ByRef [Continue] As Boolean, ByVal ImageSelected As Image)
  Public Event ImageChanged(ByVal OldImage As Image, ByVal NewImage As Image)

#Region "Members"

  Private m_current_directory As String
  Private m_image As Image = Nothing
  Private m_image_name As String
  Private m_list_size As List(Of Size)
  Private m_current_size As Size
  Private m_label_info As String
  Private m_size_button As Size = New Size(NORMAL_WIDTH, NORMAL_HEIGHT)
  Private m_size_small_button As Size = New Size(SMALL_WIDTH, SMALL_HEIGHT)
  Private m_internal_resolution As Size
  Private m_free_resize As Boolean = False
  Private m_transparent As Boolean = False
  Private m_label As Label

#End Region

#Region "Properties"


  ' PURPOSE: Get the current size of the panel
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - Size
  Public ReadOnly Property CurrenSize() As Size
    Get
      Return m_current_size
    End Get
  End Property

  ' PURPOSE: Get/Set the image into the panel.
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Property Image() As Image
    Get
      Return Me.pnl_image.BackgroundImage
    End Get
    Set(ByVal value As Image)
      SetImage(value)
    End Set
  End Property

  ' PURPOSE: Image Layout of the panel
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Property ImageLayout() As ImageLayout
    Get
      Return Me.pnl_image.BackgroundImageLayout
    End Get
    Set(ByVal value As ImageLayout)
      Me.pnl_image.BackgroundImageLayout = value
    End Set
  End Property

  Public Property FreeResize() As Boolean
    Get
      Return m_free_resize
    End Get
    Set(ByVal value As Boolean)
      m_free_resize = value
    End Set
  End Property
  Public Property Transparent() As Boolean
    Get
      Return m_transparent
    End Get
    Set(ByVal value As Boolean)
      m_transparent = value
    End Set
  End Property

  Public Property ButtonDeleteEnabled() As Boolean
    Get
      Return Me.btn_delete_img.Enabled
    End Get
    Set(ByVal value As Boolean)
      Me.btn_delete_img.Enabled = value
    End Set
  End Property

  Public Property ImageInfoFont() As Font
    Get
      Return Me.lbl_size_img.Font
    End Get
    Set(ByVal value As Font)
      Me.lbl_size_img.Font = value
    End Set
  End Property

  Public Property ImageName() As String
    Get
      Return m_image_name
    End Get
    Set(ByVal Value As String)
      m_image_name = Value
    End Set
  End Property

#End Region

#Region "Private"

  ' PURPOSE: Initialize controls
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none

  Private Sub InitControl()

    m_list_size = New List(Of Size)
    m_list_size.Add(New Size(600, 480))
    m_list_size.Add(New Size(600, 450))
    m_list_size.Add(New Size(400, 320))
    m_list_size.Add(New Size(400, 300))
    m_list_size.Add(New Size(300, 240))
    m_list_size.Add(New Size(200, 150))
    m_list_size.Add(New Size(175, 140))
    m_list_size.Add(New Size(150, 112))
    m_list_size.Add(New Size(100, 75))

    Me.lbl_size_img.Text = "xInfo"

    pnl_image.Cursor = Cursors.Hand

    m_label = Nothing
    m_label = New Label()
    m_label.Dock = DockStyle.Fill
    m_label.BackColor = Color.Transparent
    m_label.TextAlign = ContentAlignment.MiddleCenter
    Me.pnl_image.Controls.Clear()
    Me.pnl_image.Controls.Add(m_label)

  End Sub

  ' PURPOSE: Resize the controls of the uc_control
  '
  '  PARAMS:
  '     - INPUT:
  '           - SizeTo: Target Size 
  '           - SmallButton: If exists a small button
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none

  Private Sub ResizeTo(ByVal SizeTo As Size, ByVal ShowLabel As Boolean)

    Dim _height As Integer

    m_current_size = SizeTo

    Me.pnl_image.Size = SizeTo

    Me.lbl_size_img.Visible = ShowLabel
    If ShowLabel Then
      Me.lbl_size_img.Location = New Size(0, 0)
      Me.pnl_image.Top = Me.lbl_size_img.Height + 5
      Me.pnl_image.Left = 1
    Else
      Me.pnl_image.Top = 1
      Me.pnl_image.Left = 1
    End If
    Me.btn_select_img.Type = uc_button.ENUM_BUTTON_TYPE.SMALL
    Me.btn_select_img.Type = uc_button.ENUM_BUTTON_TYPE.USER
    Me.btn_select_img.Width += 15

    Me.btn_delete_img.Type = uc_button.ENUM_BUTTON_TYPE.SMALL
    Me.btn_delete_img.Type = uc_button.ENUM_BUTTON_TYPE.USER

    Me.btn_select_img.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(723)
    Me.btn_delete_img.Text = GLB_NLS_GUI_CONTROLS.GetString(11)

    _height = SizeTo.Width - Me.btn_select_img.Width - Me.btn_delete_img.Width - 4

    If _height < 0 Then
      Me.pnl_image.Left = Math.Abs(_height) / 2
      Me.lbl_size_img.Left = Math.Abs(_height) / 2
      _height = 0
    End If

    Me.btn_select_img.Location = New Size(_height / 2, Me.pnl_image.Top + SizeTo.Height + 5)
    Me.btn_delete_img.Location = New Size(_height / 2 + Me.btn_select_img.Width + 5, Me.pnl_image.Top + SizeTo.Height + 5)

    If Me.DesignMode Then
      Dim _str As String
      Dim _indx As Integer

      _str = SizeToString(m_current_size)

      If Not m_free_resize Then

        _indx = m_list_size.IndexOf(SizeTo)

        If _indx = 0 Then
          _str = "".PadRight(5, " ") & SizeToString(m_list_size.Item(_indx + 2)) & Environment.NewLine
          _str &= "".PadRight(5, " ") & SizeToString(m_list_size.Item(_indx + 1)) & Environment.NewLine
          _str &= "--> " & SizeToString(m_list_size.Item(_indx))
        ElseIf _indx = m_list_size.Count - 1 Then
          _str = "--> " & SizeToString(m_list_size.Item(_indx)) & Environment.NewLine
          _str &= "".PadRight(5, " ") & SizeToString(m_list_size.Item(_indx - 1)) & Environment.NewLine
          _str &= "".PadRight(5, " ") & SizeToString(m_list_size.Item(_indx - 2))
        Else
          _str = "".PadRight(5, " ") & SizeToString(m_list_size.Item(_indx + 1)) & Environment.NewLine
          _str &= "--> " & SizeToString(m_list_size.Item(_indx)) & Environment.NewLine
          _str &= "".PadRight(5, " ") & SizeToString(m_list_size.Item(_indx - 1))
        End If

      End If

      If Not IsNothing(m_label) Then
        m_label.Text = _str
      End If

    End If

    Call SetLabelImage()
    If ShowLabel Then
      Me.lbl_size_img.AutoSize = False
      Me.lbl_size_img.Height = 13
      Me.lbl_size_img.Width = Me.pnl_image.Width
    End If

  End Sub

  Private Function SizeToString(ByVal Size As Size) As String

    Return Size.Width & "x" & Size.Height

  End Function

  ' PURPOSE: Set into the panel the text "No image"
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none

  Private Sub SetImageNothing()
    Dim _label As Label

    _label = New Label()
    _label.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(724)
    _label.Dock = DockStyle.Fill
    _label.AutoSize = False
    _label.BackColor = Color.Transparent
    _label.TextAlign = ContentAlignment.MiddleCenter
    Me.pnl_image.BackgroundImage = Nothing
    Me.pnl_image.Controls.Clear()
    Me.pnl_image.Controls.Add(_label)
    Me.lbl_size_img.Text = String.Empty

    ImageName = ""

  End Sub

  ' PURPOSE: Show the image in full screen
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none

  Private Sub ViewFullImage()

    Dim _frm As Form
    Dim _pnl As Panel

    _frm = New Form
    _pnl = New Panel

    _pnl.Dock = DockStyle.Fill
    _pnl.BackgroundImage = Image
    _pnl.BackColor = Color.Black
    _pnl.BackgroundImageLayout = Windows.Forms.ImageLayout.Center
    _pnl.Cursor = Cursors.Hand

    _frm.Controls.Add(_pnl)
    _frm.Left = Screen.PrimaryScreen.Bounds.Width / 2
    _frm.BackColor = System.Drawing.Color.Black
    _frm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
    _frm.KeyPreview = True
    _frm.WindowState = System.Windows.Forms.FormWindowState.Maximized
    _frm.TopMost = True
    _frm.ShowInTaskbar = False

    AddHandler _pnl.Click, AddressOf DoCloseFormInPanel
    AddHandler _frm.KeyDown, AddressOf DoCloseForm

    _frm.Show()

  End Sub

  ' PURPOSE: Resize a image and save in png
  '
  '  PARAMS:
  '     - INPUT:
  '           - InternalImage: Tha image 
  '           - Resolution: Target resolution
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none

  Private Sub ToInternalResolution(ByRef InternalImage As Image, ByVal Resolution As Size)

    Dim _target_img As Bitmap
    Dim _target_gr As Graphics
    Dim _mm As New IO.MemoryStream
    Dim _ratio As Double
    Dim _info As ImageCodecInfo() = ImageCodecInfo.GetImageEncoders()
    Dim _encoderParameters As Imaging.EncoderParameters
    Dim _encoder As ImageCodecInfo
    Dim _result_size As Size

    _result_size = Size.Subtract(InternalImage.Size, Resolution)
    If _result_size.Width < 0 Or _result_size.Height < 0 Then

      Return
    End If

    Try

      _ratio = Math.Min(Resolution.Width / InternalImage.Width, Resolution.Height / InternalImage.Height)

      _target_img = New Bitmap(Convert.ToInt32(InternalImage.Width * _ratio), Convert.ToInt32(InternalImage.Height * _ratio), InternalImage.PixelFormat)
      _target_img.SetResolution(90, 90)
      _target_gr = Graphics.FromImage(_target_img)

      _target_gr.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
      _target_gr.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
      _target_gr.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality
      _target_gr.CompositingQuality = Drawing2D.CompositingQuality.HighQuality

      _target_gr.DrawImage(InternalImage, 0, 0, Convert.ToInt32(InternalImage.Width * _ratio), Convert.ToInt32(InternalImage.Height * _ratio))

      _encoderParameters = New Imaging.EncoderParameters(1)
      _encoderParameters.Param(0) = New Imaging.EncoderParameter(Encoder.Quality, 100L)

      For Each _encoder In ImageCodecInfo.GetImageEncoders()

        If _encoder.FormatID = ImageFormat.Png.Guid Then
          Exit For
        End If

      Next
      '_target_img.MakeTransparent()
      _target_img.Save(_mm, _info(4), _encoderParameters)
      _target_img.Dispose()
      _target_gr.Dispose()

      InternalImage = Image.FromStream(_mm)

    Catch ex As Exception

    End Try


  End Sub

  ' PURPOSE: Resize a image and save in png
  '
  '  PARAMS:
  '     - INPUT:
  '           - InternalImage: Tha image 
  '           - Resolution: Target resolution
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none

  Private Sub SetTransparent(ByRef InternalImage As Image)

    Dim _target_img As Bitmap
    Dim _target_gr As Graphics
    Dim _mm As New IO.MemoryStream
    Dim _info As ImageCodecInfo() = ImageCodecInfo.GetImageEncoders()
    Dim _encoderParameters As Imaging.EncoderParameters
    Dim _encoder As ImageCodecInfo

   

    Try

      _target_img = New Bitmap(Convert.ToInt32(InternalImage.Width), Convert.ToInt32(InternalImage.Height), InternalImage.PixelFormat)
      _target_img.SetResolution(90, 90)
      _target_gr = Graphics.FromImage(_target_img)

      _target_gr.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
      _target_gr.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
      _target_gr.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality
      _target_gr.CompositingQuality = Drawing2D.CompositingQuality.HighQuality

      _target_gr.DrawImage(InternalImage, 0, 0, Convert.ToInt32(InternalImage.Width), Convert.ToInt32(InternalImage.Height))

      _encoderParameters = New Imaging.EncoderParameters(1)
      _encoderParameters.Param(0) = New Imaging.EncoderParameter(Encoder.Quality, 100L)

      For Each _encoder In ImageCodecInfo.GetImageEncoders()

        If _encoder.FormatID = ImageFormat.Png.Guid Then
          Exit For
        End If

      Next
      _target_img.MakeTransparent()
      _target_img.Save(_mm, _info(4), _encoderParameters)
      _target_img.Dispose()
      _target_gr.Dispose()

      InternalImage = Image.FromStream(_mm)

    Catch ex As Exception

    End Try


  End Sub

  ' PURPOSE: Set image into the panel
  '
  '  PARAMS:
  '     - INPUT:
  '           - NewImage:
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none

  Private Sub SetImage(ByVal NewImage As Image)
    Dim _dummy_size As Size
    Dim _new_size As Size
    Dim _old_image As Image

    If Me.DesignMode Then Return

    If IsNothing(NewImage) Then
      SetImageNothing()
    Else
      Me.pnl_image.Controls.Clear()

      _new_size = New Size(NewImage.Width, NewImage.Height)
      _dummy_size = Size.Subtract(m_current_size, _new_size)

      If _dummy_size.Width > 0 And _dummy_size.Height > 0 Then
        Me.pnl_image.BackgroundImageLayout = Windows.Forms.ImageLayout.Center
      Else
        Me.pnl_image.BackgroundImageLayout = Windows.Forms.ImageLayout.Zoom
      End If

      Me.pnl_image.BackgroundImage = NewImage
      Me.pnl_image.Refresh()
    End If

    _old_image = m_image
    m_image = NewImage
    Call SetLabelImage()

    RaiseEvent ImageChanged(_old_image, NewImage)


  End Sub

  Private Sub SetLabelImage()
    Dim _wpx As String
    Dim _hpx As String
    'Dim _size_img As Size
    Dim _nls_adjst As String

    If Me.DesignMode Then
      Me.lbl_size_img.Text = "xInfo"

      Exit Sub
    End If

    If IsNothing(m_current_size) Then Exit Sub

    _wpx = m_current_size.Width.ToString()
    _hpx = m_current_size.Height.ToString()

    If String.IsNullOrEmpty(m_label_info) Then
      If IsNothing(m_image) Then
        lbl_size_img.Text = ""
      Else
        _nls_adjst = ""
        '_size_img = Size.Subtract(m_current_size, m_image.Size)
        'If _size_img.Width < 0 Or _size_img.Height < 0 Then
        '  _nls_adjst = GLB_NLS_GUI_PLAYER_TRACKING.GetString(778, _wpx, _hpx)
        'End If
        lbl_size_img.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(777, m_image.Width.ToString(), m_image.Height.ToString(), _nls_adjst)
      End If
    Else
      lbl_size_img.Text = m_label_info.Replace("%1", _wpx).Replace("%2", _hpx)
    End If

  End Sub

#End Region

#Region "Public"

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

    InitControl()

  End Sub

  'Public Sub SetImageHalf()
  '  Dim _target_img As Bitmap
  '  Dim _target_gr As Graphics
  '  Dim _mm As New IO.MemoryStream
  '  Dim _ratio As Double
  '  Dim _info As ImageCodecInfo() = ImageCodecInfo.GetImageEncoders()
  '  Dim _encoderParameters As Imaging.EncoderParameters
  '  Dim _encoder As ImageCodecInfo

  '  _ratio = (Me.Width / 2) / Me.m_image.Width

  '  _target_img = New Bitmap(Me.m_image.Width, Me.m_image.Height, Me.m_image.PixelFormat)
  '  _target_img.SetResolution(90, 90)
  '  _target_gr = Graphics.FromImage(_target_img)

  '  _target_gr.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
  '  _target_gr.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
  '  _target_gr.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality
  '  _target_gr.CompositingQuality = Drawing2D.CompositingQuality.HighQuality

  '  '_target_gr.Clear(Color.Transparent)

  '  _target_gr.DrawImage(m_image, 0, 0, Convert.ToInt32(Me.m_image.Width * _ratio), Convert.ToInt32(Me.m_image.Height * _ratio))

  '  _encoderParameters = New Imaging.EncoderParameters(1)
  '  _encoderParameters.Param(0) = New Imaging.EncoderParameter(Encoder.Quality, 100L)

  '  For Each _encoder In ImageCodecInfo.GetImageEncoders()

  '    If _encoder.FormatID = ImageFormat.Png.Guid Then
  '      Exit For
  '    End If

  '  Next

  '  _target_img.MakeTransparent()
  '  _target_img.Save(_mm, _info(4), _encoderParameters)
  '  _target_img.Dispose()
  '  _target_gr.Dispose()

  '  Me.pnl_image.BackgroundImage = Image.FromStream(_mm)
  '  Me.pnl_image.Refresh()

  'End Sub

  ' PURPOSE: Init control 
  '
  '  PARAMS:
  '     - INPUT:
  '           - InternalResolution: Resolution that it save the image
  '           - LabelInfoImage: String of label of the image information 
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none

  Public Sub Init(ByVal InternalResolution As MAXIMUN_RESOLUTION, Optional ByVal LabelInfoImage As String = "")

    m_label_info = LabelInfoImage
    If Not String.IsNullOrEmpty(m_label_info) Then
      lbl_size_img.Text = m_label_info.Replace("%1", Me.pnl_image.Width).Replace("%2", Me.pnl_image.Height)
    End If

    Select Case InternalResolution
      Case MAXIMUN_RESOLUTION.x200X150
        m_internal_resolution = New Size(200, 150)
      Case MAXIMUN_RESOLUTION.x400X300
        m_internal_resolution = New Size(400, 300)
      Case MAXIMUN_RESOLUTION.x1280X1024
        m_internal_resolution = New Size(1280, 1024)
    End Select

  End Sub

  ' PURPOSE: Init control 
  '
  '  PARAMS:
  '     - INPUT:
  '           - InternalResolution: Resolution that it save the image
  '           - LabelInfoImage: String of label of the image information 
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none

  Public Sub Init(ByVal FreeSizeMaxHeight As Integer, ByVal FreeSizeMaxWidth As Integer, Optional ByVal LabelInfoImage As String = "")

    m_label_info = LabelInfoImage
    If Not String.IsNullOrEmpty(m_label_info) Then
      lbl_size_img.Text = m_label_info.Replace("%1", Me.pnl_image.Width).Replace("%2", Me.pnl_image.Height)
    End If
    Me.m_internal_resolution = New Size(FreeSizeMaxWidth, FreeSizeMaxHeight)



  End Sub

  ' PURPOSE: GetImageWeight 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Long

  Public Function GetImageKBWeight() As Long
    Dim _image_weight As Long = 0
    If (Not String.IsNullOrEmpty(Me.m_current_directory)) And (Not String.IsNullOrEmpty(Me.m_image_name)) Then

      _image_weight = New FileInfo(Path.Combine(Me.m_current_directory, Me.m_image_name)).Length / 1024

    End If

    Return _image_weight

  End Function
#End Region

#Region "Events"

  Private Sub btn_select_img_ClickEvent() Handles btn_select_img.ClickEvent

    Dim _filename As String
    Dim _selected_file As System.Windows.Forms.OpenFileDialog
    Dim _ms As IO.MemoryStream
    Dim _continue_selected As Boolean
    Dim _new_image As System.Drawing.Image

    _selected_file = New System.Windows.Forms.OpenFileDialog

    If Not String.IsNullOrEmpty(m_current_directory) Then
      _selected_file.InitialDirectory = m_current_directory
    End If

    _selected_file.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(662)
    _selected_file.Filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(663)
    If _selected_file.ShowDialog() <> Windows.Forms.DialogResult.OK Then
      Return
    End If

    ImageName = _selected_file.SafeFileName
    _filename = _selected_file.FileName
    m_current_directory = Path.GetDirectoryName(_filename)
    If Not _selected_file.Filter.ToUpper.Contains(Path.GetExtension(_filename).ToUpper) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(797), ENUM_MB_TYPE.MB_TYPE_INFO)

      Call btn_select_img_ClickEvent()
      Return
    End If

    _ms = New IO.MemoryStream()
    Using _img_tmp1 As Image = System.Drawing.Image.FromFile(_filename)
      _img_tmp1.Save(_ms, System.Drawing.Imaging.ImageFormat.Png)
      _new_image = New Bitmap(_ms)
    End Using

    _continue_selected = True
    RaiseEvent ImageSelected(_continue_selected, _new_image)

    If _continue_selected Then
      ToInternalResolution(_new_image, Me.m_internal_resolution)
      If m_transparent Then
        SetTransparent(_new_image)
      End If
      SetImage(_new_image)

    End If

  End Sub

  Private Sub btn_delete_img_ClickEvent() Handles btn_delete_img.ClickEvent
    Dim _image As Image

    _image = My.Resources.transparent_logo()

    SetImage(_image)

  End Sub

  Private Sub uc_image_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize

    Dim _result_size As Size
    Dim _show_label As Boolean

    _show_label = True

    If IsNothing(m_list_size) Then Exit Sub

    If Me.m_free_resize Then
      _result_size.Width = Me.Width
      _result_size.Height = Me.Height
      _result_size.Height -= m_size_small_button.Height + 5
      If _result_size.Height < 150 Then
        _show_label = False
      Else
        _show_label = True
        _result_size.Height -= Me.lbl_size_img.Height
      End If
      Call ResizeTo(_result_size, _show_label)

      Exit Sub
    End If

    For Each _size As Size In m_list_size

      _result_size = Size.Subtract(Me.Size, _size)
      _result_size = Size.Subtract(_result_size, m_size_small_button)
      _result_size.Height -= 5
      If _result_size.Height >= 0 Then
        If _size.Height < 150 Then
          _show_label = False
        Else
          _show_label = True
        End If
        Call ResizeTo(_size, _show_label)

        Exit For
      End If

    Next

  End Sub

  Private Sub pnl_image_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles pnl_image.Click
    Call ViewFullImage()
  End Sub

  Private Sub ControlPaint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Me.Paint

    e.Graphics.DrawRectangle(Pens.Gray, pnl_image.Left - 1, pnl_image.Top - 1, pnl_image.Width + 1, pnl_image.Height + 1)

  End Sub


  ' PURPOSE: Close the full screen
  '
  '  PARAMS:
  '     - INPUT:
  '           - Obj:
  '           - e:
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none

  Private Sub DoCloseFormInPanel(ByVal Obj As Object, ByVal e As EventArgs)
    CType(Obj, Panel).FindForm.Close()
  End Sub

  ' PURPOSE: Close the full screen
  '
  '  PARAMS:
  '     - INPUT:
  '           - Obj:
  '           - e:
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none

  Private Sub DoCloseForm(ByVal Obj As Object, ByVal e As KeyEventArgs)
    CType(Obj, Form).Close()
  End Sub

#End Region

End Class
