'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_schedule.vb  
'
' DESCRIPTION:   control schedule selection
'
' AUTHOR:        Miquel Beltran
'
' CREATION DATE: 12-MAY-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 12-MAY-2010  MBF    Initial version
' 11-JUL-2014  JMM    Weeks days sorted depending on system's first day of week
' 05-DEC-2014  DHA    Added check to disable "DateTo" field
' 13-FEB-2015  DHA    Fixed Bug #2950: invalid NLS for "Valid from"
'-------------------------------------------------------------------

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.ComponentModel

Public Class uc_schedule

  Public Overloads Property DateFrom() As Date
    Get
      Return Me.dtp_from.Value
    End Get

    Set(ByVal Value As Date)
      Me.dtp_from.Value = Value
    End Set
  End Property

  Public Overloads Property DateTo() As Date
    Get
      Return Me.dtp_to.Value
    End Get

    Set(ByVal Value As Date)
      Me.dtp_to.Value = Value
    End Set
  End Property

  Public Overloads Property ShowCheckBoxDateTo() As Boolean
    Get
      Return Me.dtp_to.ShowCheckBox
    End Get

    Set(ByVal Value As Boolean)
      Me.dtp_to.ShowCheckBox = Value

      If Me.dtp_to.ShowCheckBox Then
        Me.dtp_to.Size = New System.Drawing.Size(Me.dtp_to.Width + 10, Me.dtp_to.Height)
      End If
    End Set
  End Property

  Public Overloads Property CheckedDateTo() As Boolean
    Get
      Return Me.dtp_to.Checked
    End Get

    Set(ByVal Value As Boolean)
      Me.dtp_to.Checked = Value
    End Set
  End Property

  Public Overloads Property Weekday() As Integer
    Get
      Return GetWeekday()
    End Get

    Set(ByVal Value As Integer)
      SetWeekday(Value)
    End Set
  End Property

  Public Overloads Property TimeFrom() As Integer
    Get
      Return Me.dtp_time_from.Value.TimeOfDay.TotalSeconds
    End Get

    Set(ByVal Value As Integer)
      Dim elapsed As TimeSpan

      elapsed = TimeSpan.FromSeconds(Value)
      Me.dtp_time_from.Value = DateTime.Today.Add(elapsed)
    End Set
  End Property

  ' DHA 15-DEC-2014
  <Browsable(False), DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)> _
  Public Overloads Property LabelDateFrom() As String
    Get
      Return Me.dtp_from.Text
    End Get

    Set(ByVal Value As String)
      Me.dtp_from.Text = Value
    End Set
  End Property

  Public Overloads Property TimeTo() As Integer
    Get
      Return Me.dtp_time_to.Value.TimeOfDay.TotalSeconds
    End Get

    Set(ByVal Value As Integer)
      Dim elapsed As TimeSpan

      elapsed = TimeSpan.FromSeconds(Value)
      Me.dtp_time_to.Value = DateTime.Today.Add(elapsed)
    End Set
  End Property

  Public Overloads Property SecondTime() As Boolean
    Get
      Return Me.chk_second_time.Checked
    End Get

    Set(ByVal Value As Boolean)
      Me.chk_second_time.Checked = Value
    End Set
  End Property

  Public Overloads Property SecondTimeFrom() As Integer
    Get
      Return Me.dtp_second_time_from.Value.TimeOfDay.TotalSeconds
    End Get

    Set(ByVal Value As Integer)
      Dim elapsed As TimeSpan

      elapsed = TimeSpan.FromSeconds(Value)
      Me.dtp_second_time_from.Value = DateTime.Today.Add(elapsed)
    End Set
  End Property

  Public Overloads Property SecondTimeTo() As Integer
    Get
      Return Me.dtp_second_time_to.Value.TimeOfDay.TotalSeconds
    End Get

    Set(ByVal Value As Integer)
      Dim elapsed As TimeSpan

      elapsed = TimeSpan.FromSeconds(Value)
      Me.dtp_second_time_to.Value = DateTime.Today.Add(elapsed)
    End Set
  End Property

  Public Overloads WriteOnly Property OnlyTimeToEnable() As Boolean
    Set(ByVal value As Boolean)
      Me.dtp_time_to.Visible = Not value
      Me.dtp_second_time_to.Visible = Not value
      Me.dtp_second_time_from.Visible = Not value
      Me.chk_second_time.Visible = Not value
    End Set
  End Property

  Public Overloads ReadOnly Property AllWeekDaysSelected() As Integer
    Get
      Return &H7F
    End Get
  End Property

  Public Overloads ReadOnly Property NoWeekDaysSelected() As Integer
    Get
      Return 0
    End Get
  End Property

  Public Sub SetDefaults()

    Dim aux_date As DateTime

    aux_date = Now.AddMonths(1)
    DateFrom = New DateTime(aux_date.Year, aux_date.Month, 1, 0, 0, 0)
    DateTo = DateFrom.AddMonths(1)
    DateTo = New DateTime(DateTo.Year, DateTo.Month, DateTo.Day, 0, 0, 0)

    Weekday = &H7F        ' All days
    TimeFrom = 0          ' 00:00:00
    TimeTo = 0            ' 00:00:00
    SecondTime = False
    SecondTimeFrom = 0    ' 00:00:00
    SecondTimeTo = 0      ' 00:00:00
    OnlyTimeToEnable = False
  End Sub ' SetDefaults

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControls()

  End Sub ' New

  Private Sub InitControls()
    Dim _week_days_chk_array(6) As Windows.Forms.CheckBox

    Me.gb_daily.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(292)

    Me.dtp_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(295)
    Me.dtp_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(296)

    Me.dtp_time_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(297)
    Me.dtp_time_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(298)

    Me.chk_monday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(301)
    Me.chk_tuesday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(302)
    Me.chk_wednesday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(303)
    Me.chk_thursday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(304)
    Me.chk_friday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(305)
    Me.chk_saturday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(306)
    Me.chk_sunday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(307)

    _week_days_chk_array(0) = chk_monday
    _week_days_chk_array(1) = chk_tuesday
    _week_days_chk_array(2) = chk_wednesday
    _week_days_chk_array(3) = chk_thursday
    _week_days_chk_array(4) = chk_friday
    _week_days_chk_array(5) = chk_saturday
    _week_days_chk_array(6) = chk_sunday

    GUI_Controls.Misc.SortCheckBoxesByFirstDayOfWeek(_week_days_chk_array)

    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.dtp_time_from.ShowUpDown = True
    Me.dtp_time_from.SetFormat(0, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_time_to.ShowUpDown = True
    Me.dtp_time_to.SetFormat(0, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.chk_second_time.Checked = False

    Me.dtp_second_time_from.Enabled = False
    Me.dtp_second_time_to.Enabled = False

    Me.dtp_second_time_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(209)
    Me.dtp_second_time_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(298)

    Me.dtp_second_time_from.ShowUpDown = True
    Me.dtp_second_time_from.SetFormat(0, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_second_time_to.ShowUpDown = True
    Me.dtp_second_time_to.SetFormat(0, ENUM_FORMAT_TIME.FORMAT_HHMM)

  End Sub ' InitControls

  Private Sub SetWeekday(ByVal Days As Integer)
    Dim str_binary As String
    Dim bit_array As Char()

    str_binary = Convert.ToString(Days, 2)
    str_binary = New String("0", 7 - str_binary.Length) + str_binary

    bit_array = str_binary.ToCharArray()

    If (bit_array(6) = "0") Then Me.chk_monday.Checked = False Else Me.chk_monday.Checked = True ' Monday 
    If (bit_array(5) = "0") Then Me.chk_tuesday.Checked = False Else Me.chk_tuesday.Checked = True ' Tuesday
    If (bit_array(4) = "0") Then Me.chk_wednesday.Checked = False Else Me.chk_wednesday.Checked = True ' Wednesday
    If (bit_array(3) = "0") Then Me.chk_thursday.Checked = False Else Me.chk_thursday.Checked = True ' Thursday
    If (bit_array(2) = "0") Then Me.chk_friday.Checked = False Else Me.chk_friday.Checked = True ' Friday
    If (bit_array(1) = "0") Then Me.chk_saturday.Checked = False Else Me.chk_saturday.Checked = True ' Saturday
    If (bit_array(0) = "0") Then Me.chk_sunday.Checked = False Else Me.chk_sunday.Checked = True ' Sunday

  End Sub ' SetWeekday

  Private Function GetWeekday() As Integer
    Dim bit_array As Char()

    bit_array = New String("0000000").ToCharArray()

    bit_array(6) = Convert.ToInt32(Me.chk_monday.Checked).ToString()  ' Monday 
    bit_array(5) = Convert.ToInt32(Me.chk_tuesday.Checked).ToString()  ' Tuesday
    bit_array(4) = Convert.ToInt32(Me.chk_wednesday.Checked).ToString()  ' Wednesday
    bit_array(3) = Convert.ToInt32(Me.chk_thursday.Checked).ToString()  ' Thursday
    bit_array(2) = Convert.ToInt32(Me.chk_friday.Checked).ToString()  ' Friday
    bit_array(1) = Convert.ToInt32(Me.chk_saturday.Checked).ToString()  ' Saturday
    bit_array(0) = Convert.ToInt32(Me.chk_sunday.Checked).ToString()  ' Sunday

    Return Convert.ToInt32(bit_array, 2)

  End Function ' GetWeekday

  Public Sub SetFocus(ByVal ErrorCode As Integer)
    Select Case ErrorCode
      Case 1
        ' Expiration date range mismatch
        Me.dtp_from.Focus()

      Case 2
        ' Time2 from equal to time2 to
        Me.dtp_second_time_from.Focus()

      Case 3
        ' Time from equal to time to
        Me.dtp_time_from.Focus()

      Case 4
        ' No week day has been marked
        Me.chk_monday.Focus()

      Case 5
        ' All week days must be selected (depending on other form options)
        Me.chk_monday.Focus()

    End Select
  End Sub ' SetFocus

  Private Sub chk_second_time_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_second_time.CheckedChanged

    dtp_second_time_from.Enabled = chk_second_time.Checked
    dtp_second_time_to.Enabled = chk_second_time.Checked

  End Sub ' chk_second_time_CheckedChanged

  Public Sub ProtectSystemPromotion()
    Me.Enabled = False

  End Sub ' ProtectSystemPromotion

End Class
