'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_account_sel.vb  
'
' DESCRIPTION:   
'
' AUTHOR:        
'
' CREATION DATE: 
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ ----------------------------------------------
' 08-MAY-2012  JAB    Fixed Defect #286: filter by holder
' 07-FEB-2013  ICS    Added fields for extended search
' 08-FEB-2013  LEM    Changed formats for telephone, name and card field
'                     Removed telephone2 field
'                     SQL filter by ac_holder_phone_number_01 or ac_holder_phone_number_02
' 11-FEB-2013  LEM    When account_id is not empty or card_track is not empty other fields 
'                       are disabled and are ignored in SQL query and returns empty values 
' 14-FEB-2013  MPO    Added SUBTRACT_TO_INITIAL_YEAR = 150
' 27-MAR-2013  RRB    Modified the query for filter birth and wedding date.
' 21-AUG-2013  JMM    Fixed Defect WIG-141: OneAccountFilterChanged event added to allow parent forms to disable related filters
' 02-SEP-2013  CCG    Added methods to managing the account massive search
' 05-SEP-2013  CCG    Added methods to return all account massive search querys
' 26-SEP-2013  CCG    Modified methods to return all massive search querys with Multitype
' 12-NOV-2013  JAB    Modified "GetFilterSQL" function. Accounts with ac_type equal to 'ACCOUNT_VIRTUAL_CASHIER' or 'ACCOUNT_VIRTUAL_TERMINAL' will be show or not, according m_extended_query (using "InitExtendedQuery" function).
' 16-DEC-2013  RMS    In TITO Mode, allow introduce an account id over the virtual accounts filter
' 10-APR-2014  JBP    Added VIP account filter.
' 06-MAY-2014  JBP    Added ShowVipClients().
' 09-JUL-2014  AMF    Added funcionality for account visible fields
' 24-JUL-2014  AMF    Tito Columns.
' 30-SEP-2014  DRV    Added kbdMsgEvent
' 06-OCT-2014  JBC    Fixed Bug WIG-1420: If SearchTrackDataasInternal is false, we haven't to check lenght of Track Data filter.
' 03-FEB-2015  OPC    Added new filter by day, month and year.
' 18-JUL-2017  RGR    Bug 28840:WIGOS-3599 Searches taking into account VIP Player check when it is disabled
'-------------------------------------------------------------------
Option Strict Off
Option Explicit On
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Windows.Forms

Public Class uc_account_sel

  Private Const SUBTRACT_TO_INITIAL_YEAR = 150
  Private Const SEPARATOR As Char = " "

  Public Event OneAccountFilterChanged()
  Public Event KbdMsgReceived()

  Dim m_disabled_holder As Boolean
  ' CCG 26-09-2013: Mulitype Massive search
  Dim m_accounts_massive_search As String
  Dim m_accounts_massive_search_to_edit As String
  Dim m_accounts_massive_search_type As Integer
  Dim m_extended_query As Boolean
  Dim m_kbdmsg_disabled As Boolean = False
  Dim m_kbdmsg_received As Boolean = False
  Dim WithEvents m_parent As Form
  Private m_barcode_handler As KbdMsgFilterHandler
  Private m_columns As New Dictionary(Of String, String)
  Private m_search_track_data_as_internal As Boolean = True

#Region "Enumerators"

  Public Enum ENUM_MASSIVE_SEARCH_TYPE
    ID_ACCOUNT = 0
    CARD_NUMBER = 1
  End Enum

  ' Enum type of elements that can get focus
  Public Enum ENUM_FOCUS
    FOCUS_ACCOUNT = 1
    FOCUS_CARDTRACK = 2
    FOCUS_CARDHOLDER = 3
    FOCUS_VIP = 4
    FOCUS_TELEPHONE = 5
    FOCUS_BIRTHDAY = 6
    FOCUS_WEDDINGDATE = 7
  End Enum ' ENUM_FOCUS

#End Region ' Enumerators

  Public Overloads Property Account() As String
    Get
      If ef_account_id.Enabled Then
        Return Me.ef_account_id.TextValue
      Else
        Return ""
      End If
    End Get

    Set(ByVal Value As String)
      Me.ef_account_id.TextValue = Value
      Call account_or_trackdata_EntryFieldValueChanged()
    End Set
  End Property

  Public Overloads Property Vip() As Boolean
    Get
      Return Me.chk_vip.Checked
    End Get

    Set(ByVal Value As Boolean)
      Me.chk_vip.Checked = Value
    End Set
  End Property

  Public Overloads Property TrackData() As String
    Get
      If ef_card_track.Enabled Then
        Return Me.ef_card_track.TextValue
      Else
        Return ""
      End If
    End Get

    Set(ByVal Value As String)
      Me.ef_card_track.TextValue = Value
      Call account_or_trackdata_EntryFieldValueChanged()
    End Set
  End Property

  Public Overloads Property Holder() As String
    Get
      If ef_card_holder.Enabled Then
        Return Me.ef_card_holder.TextValue
      Else
        Return ""
      End If
    End Get

    Set(ByVal Value As String)
      Me.ef_card_holder.TextValue = Value
    End Set
  End Property

  Public ReadOnly Property HolderIsVip() As String
    Get
      If Me.chk_vip.Checked Then
        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(278)
      Else
        Return String.Empty
      End If
    End Get
  End Property

  Public Overloads Property BirthDate() As Date
    Get
      Dim _date As DateTime
      If Not String.IsNullOrEmpty(Me.uc_ds_birthday.Day) Then
        _date = _date.AddDays(CInt(Me.uc_ds_birthday.Day) - 1)
      End If
      If Not String.IsNullOrEmpty(Me.uc_ds_birthday.Month) Then
        _date = _date.AddDays(CInt(Me.uc_ds_birthday.Month) - 1)
      End If
      If Not String.IsNullOrEmpty(Me.uc_ds_birthday.Year) Then
        _date = _date.AddDays(CInt(Me.uc_ds_birthday.Year) - 1)
      End If
      Return _date
    End Get

    Set(ByVal Value As Date)
      'Me.txt_day_birthday.Text = Value.Day.ToString()
      'Me.txt_month_birthday.Text = Value.Month.ToString()
      'Me.txt_year_birthday.Text = Value.Year.ToString()

      'Me.txt_day_birthday.Text = "D�a"
      'Me.txt_month_birthday.Text = "Mes"
      'Me.txt_year_birthday.Text = "A�o"

      'txt_day_birthday.ForeColor = Drawing.Color.Gray
      'txt_month_birthday.ForeColor = Drawing.Color.Gray
      'txt_year_birthday.ForeColor = Drawing.Color.Gray

    End Set
  End Property

  Public Overloads Property WeddingDate() As Date
    Get
      Dim _date As DateTime
      If Not String.IsNullOrEmpty(Me.uc_ds_wedding.Day) Then
        _date = _date.AddDays(CInt(Me.uc_ds_wedding.Day) - 1)
      End If
      If Not String.IsNullOrEmpty(Me.uc_ds_wedding.Month) Then
        _date = _date.AddDays(CInt(Me.uc_ds_wedding.Month) - 1)
      End If
      If Not String.IsNullOrEmpty(Me.uc_ds_wedding.Year) Then
        _date = _date.AddDays(CInt(Me.uc_ds_wedding.Year) - 1)
      End If
      Return _date
    End Get

    Set(ByVal Value As Date)

    End Set
  End Property

  Public Overloads Property Telephone() As String
    Get
      If ef_telephone.Enabled Then
        Return Me.ef_telephone.TextValue
      Else
        Return ""
      End If
    End Get

    Set(ByVal Value As String)
      Me.ef_telephone.TextValue = Value
    End Set
  End Property

  Public Overloads Property Anon() As Boolean
    Get
      Return Me.chk_anon.Checked
    End Get

    Set(ByVal Value As Boolean)
      Me.chk_anon.Checked = Value
    End Set
  End Property

  Public Overloads Property DisabledHolder() As Boolean
    Get
      Return m_disabled_holder
    End Get

    Set(ByVal Disabled As Boolean)
      m_disabled_holder = Disabled
      ef_card_holder.Enabled = Not m_disabled_holder
    End Set
  End Property

  Public Overloads ReadOnly Property BirthDateIsVisible() As Boolean
    Get
      Return Me.uc_ds_birthday.Visible
    End Get
  End Property

  Public Overloads ReadOnly Property BirthDateIsChecked() As Boolean
    Get
      Return Me.uc_ds_birthday.IsChecked()
    End Get
  End Property

  Public Overloads ReadOnly Property WeddingDateIsVisible() As Boolean
    Get
      Return Me.uc_ds_wedding.Visible
    End Get
  End Property

  Public Overloads ReadOnly Property WeddingDateIsChecked() As Boolean
    Get
      Return Me.uc_ds_wedding.IsChecked()
    End Get
  End Property

  Public Overloads ReadOnly Property TelephoneIsVisible() As Boolean
    Get
      Return Me.ef_telephone.Visible
    End Get
  End Property

  'CCG 02-SEP-2013: Massive Search
  Public Property ShowMassiveSearch() As Boolean
    Get
      Return btn_massive_search.Visible
    End Get

    Set(ByVal value As Boolean)
      btn_massive_search.Visible = value
    End Set

  End Property 'ShowMassiveSearch

  'JBP 06-MAY-2014: Show Vip Clients PROVISIONAL (ACABAR� DESAPARECIENDO ESTE CONTROL)
  Public Property ShowVipClients() As Boolean
    Get
      Return Me.chk_vip.Visible
    End Get

    Set(ByVal value As Boolean)

      Dim _size As System.Drawing.Size

      Me.chk_vip.Visible = value

      If (value) Then
        _size = New System.Drawing.Size(304, 131)
      Else
        _size = New System.Drawing.Size(304, 105)
      End If

      Me.gb_container.Size = _size

    End Set

  End Property 'ShowVipClients

  Public Property MassiveSearchNumbers() As String
    Get
      Return m_accounts_massive_search
    End Get
    Set(ByVal value As String)
      m_accounts_massive_search = value
    End Set
  End Property ' MassiveSearchNumbers

  Public Property MassiveSearchNumbersToEdit() As String
    Get
      Return m_accounts_massive_search_to_edit
    End Get
    Set(ByVal value As String)
      m_accounts_massive_search_to_edit = value
    End Set
  End Property ' MassiveSearchNumbersToEdit

  Public Property MassiveSearchType() As Integer
    Get
      Return m_accounts_massive_search_type
    End Get
    Set(ByVal value As Integer)
      m_accounts_massive_search_type = value
    End Set
  End Property ' MassiveSearchType

  Public Property AccountText() As String
    Get
      Return Me.ef_account_id.TextValue
    End Get
    Set(ByVal value As String)
      Me.ef_account_id.TextValue = value
    End Set
  End Property ' AccountText

  Public Property SearchTrackDataAsInternal() As Boolean
    Get
      Return m_search_track_data_as_internal
    End Get
    Set(ByVal value As Boolean)
      m_search_track_data_as_internal = value
    End Set
  End Property ' SearchTrackDataAsInternal

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControls()

  End Sub

  Private Sub InitControls()

    Me.ef_account_id.Text = GLB_NLS_GUI_STATISTICS.GetString(207)
    Me.ef_card_track.Text = GLB_NLS_GUI_STATISTICS.GetString(209)
    Me.ef_card_holder.Text = GLB_NLS_GUI_STATISTICS.GetString(210)
    Me.chk_anon.Text = GLB_NLS_GUI_STATISTICS.GetString(314)
    Me.chk_vip.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802)

    Call Me.ef_account_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 10)
    Call Me.ef_card_track.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TRACK_DATA, TRACK_DATA_LENGTH_WIN)
    Call Me.ef_card_holder.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    Me.chk_anon.Visible = False
    Me.uc_ds_birthday.Visible = False
    Me.uc_ds_wedding.Visible = False
    Me.ef_telephone.Visible = False

    Me.InitExtendedQuery(False)
    ' Can't use AutoSize in gb_container beause it puts a margin in the bottom
    Me.gb_container.Height = Me.flp_container.Height() + 11

  End Sub

  Public Sub Clear()

    Me.ef_account_id.TextValue = ""
    Me.ef_card_track.TextValue = ""
    Me.ef_card_holder.TextValue = ""
    Me.ef_telephone.TextValue = ""
    Me.chk_anon.Checked = False
    Me.chk_vip.Checked = False

    Call account_or_trackdata_EntryFieldValueChanged()
  End Sub

  Public Sub ShowAnonFilter()
    Me.chk_anon.Visible = True
  End Sub

  ' PURPOSE : Create the necesary SQL IN condition query to do the account massive search filter
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT: String with the query
  '
  ' RETURNS:
  '
  Public Function GetMassiveSearchFilterSQL() As String
    Dim _sql_where_account As String

    _sql_where_account = Me.GetFilterSQL()

    If Not String.IsNullOrEmpty(Me.MassiveSearchNumbers) Then
      If Not String.IsNullOrEmpty(_sql_where_account) Then
        _sql_where_account &= " AND "
      End If
      _sql_where_account &= " " & GetColumnName("AC_ACCOUNT_ID") & " IN ("
      _sql_where_account &= Me.MassiveSearchNumbers.Replace(SEPARATOR, ",")
      _sql_where_account &= ")"
    End If

    Return _sql_where_account
  End Function ' GetMassiveSearchFilterSQL

  ' PURPOSE : Build WHERE part for SQL query from field values
  '
  '  PARAMS:
  '     - INPUT:
  '         - Boolean: Include UPPER in holder name filter
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '         - String: WHERE sentence (require FROM or INNER with dbo.accounts table)
  '
  Public Function GetFilterSQL(Optional ByVal WithUpperHolder As Boolean = True) As String

    Dim str_sql As String
    Dim internal_card_id As String
    Dim str_aux1 As String
    Dim rc As Boolean
    Dim card_type As Integer

    str_sql = ""

    ' Filter Card Id
    If Me.chk_anon.Checked Then
      str_sql = " " & GetColumnName("AC_ACCOUNT_ID") & " IS NULL "
      Return str_sql
    End If

    If String.IsNullOrEmpty(Me.MassiveSearchNumbers) Then

      If Me.ef_account_id.TextValue <> "" And ef_account_id.Enabled Then
        str_sql = " " & GetColumnName("AC_ACCOUNT_ID") & " = " & Me.ef_account_id.Value
      End If

      ' Filter Track
      If Me.ef_card_track.TextValue <> "" And ef_card_track.Enabled Then

        ' add and and
        If str_sql <> "" Then
          str_sql += " AND "
        End If

        ' First convert external (encrypted) card number into the internal id used in the database
        internal_card_id = ""
        If Me.ef_card_track.TextValue.Length = TRACK_DATA_LENGTH_WIN Then
          If SearchTrackDataAsInternal Then
            rc = WSI.Common.CardNumber.TrackDataToInternal(Me.ef_card_track.TextValue, internal_card_id, card_type)
          Else
            rc = False
          End If
          If rc = True Then
            str_sql += " " & GetColumnName("AC_TRACK_DATA") & " = '" & internal_card_id & "'"
          Else
            str_sql += " " & GetColumnName("AC_TRACK_DATA") & " = '" & Me.ef_card_track.TextValue & "'"
          End If
        Else
          str_sql += " " & GetColumnName("AC_TRACK_DATA") & " = '" & Me.ef_card_track.TextValue & "'"
        End If

      End If

      ' Filter Vip's
      If Me.chk_vip.Checked Then
        ' add and 
        If str_sql <> "" Then
          str_sql += " AND "
        End If

        str_sql += " " & GetColumnName("AC_HOLDER_IS_VIP") & " = 1"
      End If

    End If

    ' Filter Holder Name
    If Me.ef_card_holder.TextValue <> "" And ef_card_holder.Enabled Then

      ' add and and
      If str_sql <> "" Then
        str_sql += " AND "
      End If

      str_aux1 = UCase(Me.ef_card_holder.TextValue).Replace("'", "''")
      str_sql += "(" & IIf(WithUpperHolder, "UPPER(", "(") & GetColumnName("AC_HOLDER_NAME") & ") LIKE '" & str_aux1 & "%' OR"
      str_sql += IIf(WithUpperHolder, " UPPER(", " (") & GetColumnName("AC_HOLDER_NAME") & ") LIKE '% " & str_aux1 & "%') "

    End If

    ' Filter Birth Date
    If Me.BirthDateIsVisible AndAlso Me.BirthDateIsChecked Then
      ' Day
      If Not String.IsNullOrEmpty(Me.uc_ds_birthday.Day) Then
        If str_sql <> "" Then
          str_sql += " AND "
        End If

        str_sql += "Day(" & GetColumnName("AC_HOLDER_BIRTH_DATE") & ") = " & Me.uc_ds_birthday.Day & " "
      End If

      ' Month
      If Not String.IsNullOrEmpty(Me.uc_ds_birthday.Month) Then
        If str_sql <> "" Then
          str_sql += " AND "
        End If

        str_sql += "Month(" & GetColumnName("AC_HOLDER_BIRTH_DATE") & ") = " & Me.uc_ds_birthday.Month & " "
      End If

      ' Year
      If Not String.IsNullOrEmpty(Me.uc_ds_birthday.Year) Then
        If str_sql <> "" Then
          str_sql += " AND "
        End If

        str_sql += "Year(" & GetColumnName("AC_HOLDER_BIRTH_DATE") & ") = " & Me.uc_ds_birthday.Year & " "
      End If
    End If

    '' Filter Wedding Date
    If Me.WeddingDateIsVisible AndAlso Me.WeddingDateIsChecked Then
      ' Day
      If Not String.IsNullOrEmpty(Me.uc_ds_wedding.Day) Then
        If str_sql <> "" Then
          str_sql += " AND "
        End If

        str_sql += "Day(" & GetColumnName("AC_HOLDER_WEDDING_DATE") & ") = " & Me.uc_ds_wedding.Day & " "
      End If

      ' Month
      If Not String.IsNullOrEmpty(Me.uc_ds_wedding.Month) Then
        If str_sql <> "" Then
          str_sql += " AND "
        End If

        str_sql += "Month(" & GetColumnName("AC_HOLDER_WEDDING_DATE") & ") = " & Me.uc_ds_wedding.Month & " "
      End If

      ' Year
      If Not String.IsNullOrEmpty(Me.uc_ds_wedding.Year) Then
        If str_sql <> "" Then
          str_sql += " AND "
        End If

        str_sql += "Year(" & GetColumnName("AC_HOLDER_WEDDING_DATE") & ") = " & Me.uc_ds_wedding.Year & " "
      End If
    End If

    ' Filter Telephone
    If Me.ef_telephone.Visible AndAlso Me.ef_telephone.TextValue <> "" And ef_telephone.Enabled Then

      ' add and and
      If str_sql <> "" Then
        str_sql += " AND "
      End If
      str_aux1 = UCase(Me.ef_telephone.TextValue).Replace("'", "''")
      str_sql += "(REPLACE(" & GetColumnName("AC_HOLDER_PHONE_NUMBER_01") & ", ' ', '') LIKE " & _
                  "REPLACE('%" & str_aux1 & "%', ' ', '') OR REPLACE(" & GetColumnName("AC_HOLDER_PHONE_NUMBER_02") & ", ' ', '') LIKE " & _
                  "REPLACE('%" + str_aux1 + "%', ' ', ''))"
    End If

    If TITO.Utils.IsTitoMode Then

      ' If not in extended query mode and account id is blank then filter to don't show virtual accounts
      If Not m_extended_query And ef_account_id.Value = String.Empty And GLB_GuiMode <> public_globals.ENUM_GUI.MULTISITE_GUI Then
        If str_sql <> "" Then
          str_sql += " AND "
        End If

        str_sql += "(" & GetColumnName("AC_TYPE") & " NOT IN(" & AccountType.ACCOUNT_VIRTUAL_CASHIER & ", " & AccountType.ACCOUNT_VIRTUAL_TERMINAL & "))"
      End If

    End If

    Return str_sql
  End Function

  ' PURPOSE : Initializes fields for extended search from GENERAL_PARAMS
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  ' NOTES:
  '     - Check if params are active and show the fields in the control
  '
  Public Sub InitExtendedSearch()

    Dim _now As DateTime

    _now = WSI.Common.WGDB.Now

    Me.uc_ds_birthday.Name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1651)
    Me.uc_ds_wedding.Name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1652)

    Me.ef_telephone.Text = GLB_NLS_GUI_CONTROLS.GetString(360)

    Me.ef_telephone.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 20)

    Me.uc_ds_birthday.Visible = GeneralParam.GetBoolean("Account.ExtendedSearch", "BirthDate", False) And GeneralParam.GetBoolean("Account.VisibleField", "BirthDate", True)
    Me.uc_ds_wedding.Visible = GeneralParam.GetBoolean("Account.ExtendedSearch", "WeddingDate", False) And GeneralParam.GetBoolean("Account.VisibleField", "WeddingDate", True)

    Me.ef_telephone.Visible = GeneralParam.GetBoolean("Account.ExtendedSearch", "PhoneNumber", False) And GeneralParam.GetBoolean("Account.VisibleField", "Phone1", True)

    ' Can't use AutoSize in gb_container beause it puts a margin in the bottom
    Me.gb_container.Height = Me.flp_container.Height() + 11

  End Sub ' InitExtendedFields

  ' PURPOSE : Validates format of all fields
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '         - TRUE:   If the format of all fields is correct
  '         - FALSE:  If there is a field that has incorrect format
  '
  Public Function ValidateFormat() As Boolean
    If String.IsNullOrEmpty(Me.MassiveSearchNumbers) Then
      If Me.ef_account_id.Enabled AndAlso Not Me.ef_account_id.Value = "" AndAlso Not Me.ef_account_id.ValidateFormat() Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
              mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_account_id.Text)
        Me.ef_account_id.Focus()

        Return False
      End If

      If Me.ef_card_track.Enabled AndAlso Not Me.ef_card_track.Value = "" AndAlso SearchTrackDataAsInternal AndAlso Not Me.ef_card_track.ValidateFormat() Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1655), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
              mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        Me.ef_card_track.Focus()

        Return False
      End If
    End If

    ' ICS 13-FEB-2013: It has decided not to validate these fields.
    '
    'If Me.ef_card_holder.Enabled AndAlso Not Me.ef_card_holder.Value = "" AndAlso Not Me.ef_card_holder.ValidateFormat() Then
    '  Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
    '        mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_card_holder.Text)
    '  Me.ef_card_holder.Focus()

    '  Return False
    'End If

    'If Me.ef_telephone.Enabled AndAlso Me.ef_telephone.Visible AndAlso Not Me.ef_telephone.Value = "" AndAlso Not Me.ef_telephone.ValidateFormat() Then
    '  Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
    '        mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, Me.ef_telephone.Text)
    '  Me.ef_telephone.Focus()

    '  Return False
    'End If

    Return True

  End Function

  ' PURPOSE : Create SQL query to do the temporary table and insert rows for each account number
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT: String with the query
  '
  ' RETURNS:
  '
  Public Function CreateAndInsertAccountData() As String
    Dim _account_numbers As String()
    Dim _idx_numbers As Integer
    Dim _str_sql As System.Text.StringBuilder
    Dim _track_data As String

    _track_data = String.Empty
    _str_sql = New System.Text.StringBuilder()
    _account_numbers = Me.MassiveSearchNumbers.Split()

    ' create table    
    _str_sql.AppendLine("CREATE TABLE #NUMBERS_TEMP (")
    _str_sql.AppendLine("NUMBER NVARCHAR(20) )")

    ' insert(rows)
    _str_sql.AppendLine("INSERT INTO #NUMBERS_TEMP ")
    _str_sql.AppendLine(" (NUMBER) ")

    If Me.MassiveSearchType = ENUM_MASSIVE_SEARCH_TYPE.ID_ACCOUNT Then
      _str_sql.AppendLine(" SELECT '" & _account_numbers(0) & "'")
    Else
      Call WSI.Common.CardNumber.TrackDataToInternal(_account_numbers(0), _track_data, 0)
      _str_sql.AppendLine(" SELECT '" & _track_data & "'")
    End If

    For _idx_numbers = 1 To _account_numbers.Length() - 1
      _track_data = String.Empty
      _str_sql.AppendLine(" UNION ALL ")

      If Me.MassiveSearchType = ENUM_MASSIVE_SEARCH_TYPE.ID_ACCOUNT Then
        _str_sql.AppendLine(" SELECT '" & _account_numbers(_idx_numbers) & "'")
      Else
        Call WSI.Common.CardNumber.TrackDataToInternal(_account_numbers(_idx_numbers), _track_data, 0)
        _str_sql.AppendLine(" SELECT '" & _track_data & "'")
      End If
    Next

    Return _str_sql.ToString()
  End Function ' CreateAndInsertAccountData

  ' PURPOSE : Create the necesary SQL INNER JOIN query to do the account massive search filter
  '
  '  PARAMS:
  '     - INPUT: ConditionField as String with the field to do the join condition
  '
  '     - OUTPUT: String with the query
  '
  ' RETURNS:
  '
  Public Function GetInnerJoinAccountMassiveSearch(ByVal ConditionField As String) As String
    Dim _str_sql As System.Text.StringBuilder

    _str_sql = New System.Text.StringBuilder()

    _str_sql.AppendLine(" INNER JOIN (                                                               ")
    _str_sql.AppendLine("              SELECT  NUMBER Collate SQL_Latin1_General_CP1_CI_AS AS NUMBER ")
    _str_sql.AppendLine("                FROM  #NUMBERS_TEMP                                         ")
    _str_sql.AppendLine("            ) #NUMBERS_TEMP ON #NUMBERS_TEMP.NUMBER = " & ConditionField)

    Return _str_sql.ToString()
  End Function ' GetInnerJoinAccountMassiveSearch

  ' PURPOSE : Create the SQL DROP query to delete the temporay table for account massive search
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT: String with the query
  '
  ' RETURNS:
  '
  Public Function DropTableAccountMassiveSearch() As String
    Return " DROP TABLE #NUMBERS_TEMP"
  End Function ' DropTableAccountMassiveSearch

  ' PURPOSE : Check if chk_anon is checked and enable/disable other controls 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub chk_anon_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_anon.CheckedChanged

    If Me.chk_anon.Checked Then
      Me.ef_account_id.Enabled = False
      Me.ef_card_holder.Enabled = False
      Me.ef_card_track.Enabled = False
      Me.chk_vip.Enabled = False
      Me.chk_vip.Checked = False
      Me.uc_ds_birthday.Enabled = False
      Me.uc_ds_wedding.Enabled = False
      Me.ef_telephone.Enabled = False
    Else
      Call account_or_trackdata_EntryFieldValueChanged()
    End If

  End Sub

  ' PURPOSE : Check if not empty ef_account_id and ef_card_track values and enable/disable other controls 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub account_or_trackdata_EntryFieldValueChanged() Handles ef_account_id.EntryFieldValueChanged, ef_card_track.EntryFieldValueChanged

    Dim _enabled As Boolean

    If ef_account_id.TextValue <> "" Or ef_card_track.TextValue <> "" Then
      Me.ef_account_id.Enabled = (Me.ef_account_id.TextValue <> "")
      Me.ef_card_track.Enabled = (Me.ef_card_track.TextValue <> "")
      Me.btn_massive_search.Enabled = False

      _enabled = False
    Else
      Me.ef_account_id.Enabled = True
      Me.ef_account_id.IsReadOnly = False
      Me.ef_card_track.IsReadOnly = False
      Me.MassiveSearchNumbers = String.Empty
      Me.MassiveSearchNumbersToEdit = String.Empty
      Me.MassiveSearchType = uc_account_sel.ENUM_MASSIVE_SEARCH_TYPE.ID_ACCOUNT
      Me.ef_card_track.Enabled = True
      Me.btn_massive_search.Enabled = True

      _enabled = True
    End If

    Me.ef_card_holder.Enabled = _enabled And Not DisabledHolder
    Me.uc_ds_birthday.Enabled = _enabled
    Me.uc_ds_wedding.Enabled = _enabled
    Me.ef_telephone.Enabled = _enabled
    Me.chk_vip.Enabled = _enabled
    Me.chk_vip.Checked = False

    RaiseEvent OneAccountFilterChanged()

    If m_kbdmsg_received Then
      RaiseEvent KbdMsgReceived()
      m_kbdmsg_received = False
    End If

  End Sub

  ' PURPOSE : Initialize the form and change the textbox 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub btn_massive_search_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_massive_search.Click
    Dim _frm_account_massive_search
    Dim _numbers_of_accounts As Integer

    _numbers_of_accounts = 0
    _frm_account_massive_search = New frm_account_massive_search(Me)

    Call _frm_account_massive_search.ShowDialog()

    If Not String.IsNullOrEmpty(MassiveSearchNumbers) Then
      _numbers_of_accounts = Strings.Split(MassiveSearchNumbers(), SEPARATOR).Length
      If Me.MassiveSearchType = uc_account_sel.ENUM_MASSIVE_SEARCH_TYPE.ID_ACCOUNT Then
        Me.ef_account_id.IsReadOnly = True
        Me.ef_account_id.TextValue = _numbers_of_accounts & " " & GLB_NLS_GUI_AUDITOR.GetString(79)
        If _numbers_of_accounts = 1 Then
          Me.ef_account_id.TextValue = _numbers_of_accounts & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(496)
        End If

        Me.ef_account_id.Enabled = True
        Me.ef_card_track.IsReadOnly = False
        Me.ef_card_track.TextValue = String.Empty
        Me.ef_card_track.Enabled = False
      Else
        Me.ef_card_track.IsReadOnly = True
        Me.ef_card_track.TextValue = _numbers_of_accounts & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1822)
        If _numbers_of_accounts = 1 Then
          Me.ef_card_track.TextValue = _numbers_of_accounts & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2157)
        End If

        Me.ef_card_track.Enabled = True
        Me.ef_account_id.IsReadOnly = False
        Me.ef_account_id.TextValue = String.Empty
        Me.ef_account_id.Enabled = False
      End If

      Me.ef_card_holder.Enabled = False
      Me.chk_vip.Enabled = False
    Else
      Me.ef_account_id.TextValue = String.Empty
      Me.ef_card_track.TextValue = String.Empty
      Me.ef_account_id.IsReadOnly = False
      Me.ef_card_track.IsReadOnly = False
      Me.ef_account_id.Enabled = True
      Me.chk_vip.Enabled = True
      Me.ef_card_track.Enabled = True
      Me.ef_card_holder.Enabled = True
    End If

  End Sub ' btn_massive_search_Click

  ' PURPOSE : Set value in "m_kbdmsg_disabled"
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub DisableKbdMessage()
    m_kbdmsg_disabled = True
  End Sub

  Public Sub InitColumns(ByVal Columns As Dictionary(Of String, String))
    m_columns = Columns
  End Sub

  Public Function GetColumnName(ByVal Column As String) As String
    Dim _column As String

    _column = Column
    If m_columns.ContainsKey(Column) Then
      _column = m_columns(Column)
    End If

    Return _column
  End Function

  ' PURPOSE : Set value in "m_extended_query"
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub InitExtendedQuery(ByVal ExtendedQuery As Boolean)

    m_extended_query = ExtendedQuery

  End Sub ' InitExtendedQuery

  ' PURPOSE: Manages barcode received events
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub BarcodeEvent(ByVal e As KbdMsgEvent, ByVal Data As Object)
    Try
      If Not Data Is Nothing Then
        Dim _data_read As WSI.Common.Barcode = DirectCast(Data, WSI.Common.Barcode)
        Me.ef_card_track.TextValue = _data_read.ExternalCode
        m_kbdmsg_received = True
        Call account_or_trackdata_EntryFieldValueChanged()
        Me.Focus()
        SendKeys.Send("{ENTER}")
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub FormActivated(ByVal sender As System.Object, ByVal e As System.EventArgs)
    If Not m_kbdmsg_disabled Then
      m_barcode_handler = AddressOf Me.BarcodeEvent
      KeyboardMessageFilter.RegisterHandler(Me, BarcodeType.Account, m_barcode_handler)
    End If
  End Sub

  Private Sub FormDeactivated(ByVal sender As System.Object, ByVal e As System.EventArgs)
    If Not m_kbdmsg_disabled Then
      KeyboardMessageFilter.UnregisterHandler(Me, m_barcode_handler)
    End If
  End Sub

  Private Sub uc_account_sel_VisibleChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.VisibleChanged
    If m_parent Is Nothing Then
      m_parent = Me.ParentForm
      KeyboardMessageFilter.Init()
      AddHandler m_parent.Activated, AddressOf Me.FormActivated
      AddHandler m_parent.Deactivate, AddressOf Me.FormDeactivated
    End If
  End Sub

  ' PURPOSE: Indicates if it's a correct wedding date
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '         - Boolean
  '
  ' RETURNS:
  '
  Public Function IsWeddingDateValid() As Boolean
    Return Me.uc_ds_wedding.IsValidDate()
  End Function

  ' PURPOSE: Indicates if it's a correct Birth date
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '         - Boolean
  '
  ' RETURNS:
  '
  Public Function IsBirthdayDateValid() As Boolean
    Return Me.uc_ds_birthday.IsValidDate()
  End Function

  ' PURPOSE: Sets the focus to a control
  '
  '    - INPUT:
  '       -ENUM_FOCUS: Element that is going to be focused
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Public Sub SetFocus(ByVal ElementFocus As ENUM_FOCUS)

    Select Case ElementFocus
      Case ENUM_FOCUS.FOCUS_ACCOUNT
        Me.ef_account_id.Focus()
      Case ENUM_FOCUS.FOCUS_CARDTRACK
        Me.ef_card_track.Focus()
      Case ENUM_FOCUS.FOCUS_CARDHOLDER
        Me.ef_card_holder.Focus()
      Case ENUM_FOCUS.FOCUS_VIP
        Me.chk_vip.Focus()
      Case ENUM_FOCUS.FOCUS_TELEPHONE
        Me.ef_telephone.Focus()
      Case ENUM_FOCUS.FOCUS_BIRTHDAY
        Me.uc_ds_birthday.Focus()
      Case ENUM_FOCUS.FOCUS_WEDDINGDATE
        Me.uc_ds_wedding.Focus()
    End Select

  End Sub

End Class
