﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_jackpot_type_sel.vb
' DESCRIPTION:   List Jackpot Meters History 
' AUTHOR:        Carlos Cáceres González
' CREATION DATE: 05-MAY-2017
'
' REVISION HISTORY:
'
' Date         Author  Description
' -----------  ------  -----------------------------------------------
' 05-MAY-2017   CCG     Initial version
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports WSI.Common.Jackpot.Jackpot

Public Class uc_jackpot_type_sel

#Region " Members "

  Public m_selected_type As List(Of JackpotType)

#End Region

#Region "Properties"

  Public Property SelectedTypes() As List(Of JackpotType)

    Get
      Return m_selected_type
    End Get

    Set(value As List(Of JackpotType))
      Me.m_selected_type = value
    End Set

  End Property


#End Region

  Public Sub New()

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControl()

  End Sub

#Region "Public Sub"

  Public Sub Init()
    Call InitControl()
  End Sub

  Public Sub SetDefaultValues()

    Me.ch_main.Checked = False
    Me.ch_hidden.Checked = False
    Me.ch_prize_sharing.Checked = False
    Me.ch_happy_hour.Checked = False

  End Sub

#End Region

#Region "Private Sub"

  Private Sub InitControl()

    Me.m_selected_type = New List(Of JackpotType)
    Me.gb_jackpot_type.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(559)
    Me.ch_main.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(555)
    Me.ch_hidden.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(556)
    Me.ch_prize_sharing.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(557)
    Me.ch_happy_hour.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(558)

  End Sub

  Private Sub ManageSelectedList(ByVal Checked As Boolean, ByVal Type As JackpotType)
    If Checked = True AndAlso Not Me.m_selected_type.Contains(Type) Then
      Me.m_selected_type.Add(Type)
    Else
      Me.m_selected_type.Remove(Type)
    End If
  End Sub

#End Region

#Region "Events"

  Private Sub ch_main_CheckedChanged(sender As Object, e As EventArgs) Handles ch_main.CheckedChanged
    Call ManageSelectedList(ch_main.Checked, JackpotType.Main)
  End Sub

  Private Sub ch_hidden_CheckedChanged(sender As Object, e As EventArgs) Handles ch_hidden.CheckedChanged
    Call ManageSelectedList(ch_hidden.Checked, JackpotType.Hidden)
  End Sub

  Private Sub ch_prize_sharing_CheckedChanged(sender As Object, e As EventArgs) Handles ch_prize_sharing.CheckedChanged
    Call ManageSelectedList(ch_prize_sharing.Checked, JackpotType.PrizeSharing)
  End Sub

  Private Sub ch_happy_hour_CheckedChanged(sender As Object, e As EventArgs) Handles ch_happy_hour.CheckedChanged
    Call ManageSelectedList(ch_happy_hour.Checked, JackpotType.HappyHour)
  End Sub

#End Region
End Class
