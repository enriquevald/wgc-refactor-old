'XVV -> Add NameSpace solve error with variables definitions
Imports System.Drawing

Public Class uc_monitor
  Inherits System.Windows.Forms.UserControl

  Dim pen_grid As New System.Drawing.Pen(Color.DarkGreen)
  Dim pen_label As New System.Drawing.Pen(Color.LightGreen)
  Dim pen_line1 As New System.Drawing.Pen(Color.Cyan)
  Dim pen_line2 As New System.Drawing.Pen(Color.Yellow)

  Dim pixels_per_step As Integer
  Dim pixels_per_big_step As Integer
  Dim pixels_per_label As Integer
  Dim seconds_per_step As Integer
  Dim origin300 As Date
  Dim value(3, 1000) As Single
  Dim num_values = 0
  Dim max(3) As Double

  Const SWAP_PAGE As Integer = 1
#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    Call Init("", 10)

  End Sub

  Public Sub InitPerSeconds()
    '
    'XVV Variable not use Dim aux_date As Date
    Dim just_now As Date

    pixels_per_step = 2
    pixels_per_big_step = pixels_per_step * 60
    pixels_per_label = pixels_per_big_step
    '
    just_now = Now
    If seconds_per_step = 1 Then
      origin300 = New Date(just_now.Year, just_now.Month, just_now.Day, just_now.Hour, just_now.Minute, just_now.Second)
    Else
      Dim second As Integer
      second = just_now.Second
      second = second - second Mod seconds_per_step
      origin300 = New Date(just_now.Year, just_now.Month, just_now.Day, just_now.Hour, just_now.Minute, second)
    End If
    origin300 = origin300.AddSeconds((-300 / pixels_per_step) * seconds_per_step)
    '
    Me.Panel1.Left = 60
  End Sub

  'UserControl1 overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
  Friend WithEvents Panel4 As System.Windows.Forms.Panel
  Friend WithEvents Panel3 As System.Windows.Forms.Panel
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.GroupBox1 = New System.Windows.Forms.GroupBox()
    Me.Panel4 = New System.Windows.Forms.Panel()
    Me.Panel3 = New System.Windows.Forms.Panel()
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.GroupBox1.SuspendLayout()
    Me.Panel4.SuspendLayout()
    Me.SuspendLayout()
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.AddRange(New System.Windows.Forms.Control() {Me.Panel4})
    Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.GroupBox1.Location = New System.Drawing.Point(4, 4)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(380, 304)
    Me.GroupBox1.TabIndex = 0
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "GroupBox1"
    '
    'Panel4
    '
    Me.Panel4.BackColor = System.Drawing.Color.Black
    Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel4.Controls.AddRange(New System.Windows.Forms.Control() {Me.Panel3, Me.Panel1})
    Me.Panel4.Location = New System.Drawing.Point(8, 16)
    Me.Panel4.Name = "Panel4"
    Me.Panel4.Size = New System.Drawing.Size(364, 280)
    Me.Panel4.TabIndex = 4
    '
    'Panel3
    '
    Me.Panel3.BackColor = System.Drawing.Color.Black
    Me.Panel3.Name = "Panel3"
    Me.Panel3.Size = New System.Drawing.Size(60, 300)
    Me.Panel3.TabIndex = 5
    '
    'Panel1
    '
    Me.Panel1.BackColor = System.Drawing.Color.Black
    Me.Panel1.Location = New System.Drawing.Point(60, 0)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(3000, 300)
    Me.Panel1.TabIndex = 4
    '
    'uc_monitor
    '
    Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.GroupBox1})
    Me.DockPadding.All = 4
    Me.Name = "uc_monitor"
    Me.Size = New System.Drawing.Size(388, 312)
    Me.GroupBox1.ResumeLayout(False)
    Me.Panel4.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

  Private Sub Panel1_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint
    Dim x, y As Integer
    Dim x0, x1 As Integer

    'XVV 16/04/2007
    'Assign "" to variable because compiler generate a warning
    Dim str_time As String = ""

    Dim y1, y2 As Integer

    If Me.DesignMode Then
      Exit Sub
    End If

    ' Compute x0 (left most) and x1 (right most)
    x0 = e.ClipRectangle.X '- 5 * pixels_per_step
    x1 = e.ClipRectangle.X + e.ClipRectangle.Width '+ 1 '5 * pixels_per_step
    x0 = x0 - x0 Mod pixels_per_step
    x1 = x1 + (pixels_per_step - x1 Mod pixels_per_step) Mod pixels_per_step
    If x0 < 0 Then x0 = 0
    If x1 > 1000 Then x1 = 1000

    ' Draw horizontal grid lines
    For y = 5 To 300 Step 60 / 2
      e.Graphics.DrawLine(pen_grid, x0, y, x1, y)
    Next
    ' Draw vertical grid lines & labels
    For x = 0 To Panel1.Width Step pixels_per_step
      'If x >= x0 And x <= x1 Then

      'XVV 16/04/2007
      'Assign nothing to variable because compiler generate a warning
      Dim pen As System.Drawing.Pen = Nothing

      If GetLabelX(x, str_time, pen) Then
        e.Graphics.DrawLine(pen, x, 0, x, 250)
        e.Graphics.DrawString(str_time, Me.Font, pen_label.Brush, x - e.Graphics.MeasureString(str_time, Me.Font).Width / 2, 260)
      End If
      'End If
    Next


    ' Draw value lines
    For x = x0 To x1 - 1 Step pixels_per_step
      If value(0, x) = 0 And value(0, x + pixels_per_step) = 0 Then
        y1 = 5 + 240 - 240 * (value(1, x) / max(1))
        y2 = 5 + 240 - 240 * (value(1, x + pixels_per_step) / max(1))
        e.Graphics.DrawLine(Me.pen_line1, x - 3, y1, x - 3 + pixels_per_step, y2)
        y1 = 5 + 240 - 240 * (value(2, x) / max(1))
        y2 = 5 + 240 - 240 * (value(2, x + pixels_per_step) / max(1))
        e.Graphics.DrawLine(Me.pen_line2, x - 3, y1, x - 3 + pixels_per_step, y2)
      End If
    Next
  End Sub


  Public Sub PushValue(ByVal NewValue1 As Double, ByVal NewValue2 As Double)
    Dim idx As Integer
    Dim x0, x1 As Integer
    Dim shift_time As Boolean
    Dim prev_max As Double
    Dim scale_changed As Boolean
    Dim rc As Rectangle

    Me.Panel1.SuspendLayout()

    Me.Panel1.Left -= pixels_per_step
    value(0, 60 - Me.Panel1.Left + 300) = 0
    value(1, 60 - Me.Panel1.Left + 300) = NewValue1
    value(2, 60 - Me.Panel1.Left + 300) = NewValue2

    rc.X = 60 - Me.Panel1.Left + 300 - 3 * pixels_per_step
    rc.Width = pixels_per_step * 3
    rc.Y = 0
    rc.Height = Me.Panel1.Height

    Me.Panel1.Invalidate(rc)


    scale_changed = Me.SetNewMax(NewValue1, NewValue2)
    shift_time = False
    If Me.Panel1.Left <= 60 - 300 Then
      '
      x0 = 60 - Me.Panel1.Left
      InitPerSeconds()
      shift_time = True
    End If

    If shift_time Then
      x1 = 60 - Me.Panel1.Left
      prev_max = Me.max(1)
      Me.max(1) = 1
      For idx = 0 To 300 Step pixels_per_step
        value(0, x1 + idx) = value(0, x0 + idx)
        value(1, x1 + idx) = value(1, x0 + idx)
        value(2, x1 + idx) = value(2, x0 + idx)
        Call Me.SetNewMax(value(1, x1 + idx), value(2, x1 + idx))
      Next
      If prev_max / 2 <= Me.max(1) Then
        Call Me.SetNewMax(prev_max, prev_max)
      End If
    End If

    If shift_time Or scale_changed Then
      Me.Refresh()
    End If
    Me.Panel1.ResumeLayout(False)
  End Sub

  Private Function SetNewMax(ByVal NewValue1 As Double, ByVal NewValue2 As Double) As Boolean
    Dim new_max As Double
    Dim v0 As Double
    Dim v1 As Double
    Dim idx As Integer

    new_max = Math.Max(NewValue1, NewValue2)
    new_max = Math.Max(new_max, 10)
    If new_max <= max(1) Then
      Return False
    End If

    v0 = Math.Floor(Math.Log10(new_max))
    v0 = Math.Pow(10, v0)
    For idx = 1 To 10
      v1 = v0 * idx
      If v1 >= new_max Then
        max(1) = v1
        Return True
      End If
    Next

    max(1) = v1

    Return True

  End Function

  Private Sub Panel3_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel3.Paint
    Dim y, y_step As Integer
    Dim str_val As String
    Dim v0, v1 As Double

    v0 = Math.Round(max(1), 2)
    v1 = Math.Round(v0 / 4, 2)
    y_step = 240 / 4

    For y = 6 To 300 Step y_step
      If Math.Ceiling(v0) <> v0 Then
        str_val = Format(v0, "0.0")
      Else
        str_val = Format(v0, "0")
      End If
      e.Graphics.DrawString(str_val, Me.Font, pen_label.Brush, 59 - e.Graphics.MeasureString(str_val, Me.Font).Width, y - e.Graphics.MeasureString(str_val, Me.Font).Height / 2)
      v0 -= v1
      v0 = Math.Round(v0, 2)
      If v0 < 0 Then
        v0 = 0
      End If
    Next
  End Sub

  Private Function GetLabelX(ByVal PosX As Integer, ByRef StrLabel As String, ByRef PenLabel As System.Drawing.Pen) As Boolean
    Dim label_date As Date

    label_date = origin300.AddSeconds((PosX / pixels_per_step) * seconds_per_step)

    If label_date.Second <> 0 Then
      Return False
    End If

    StrLabel = WSI.Common.Format.CustomFormatTime(label_date, False)
    If seconds_per_step = 1 Then
      If label_date.Minute Mod 5 = 0 Then
        PenLabel = Me.pen_label
      Else
        PenLabel = Me.pen_grid
      End If
    Else
      If label_date.Minute Mod 5 <> 0 Then
        Return False
      End If
      If label_date.Minute Mod 15 = 0 Then
        PenLabel = Me.pen_label
      Else
        PenLabel = Me.pen_grid
      End If
    End If

    Return True

  End Function

  Public Sub Init(ByVal Text As String, ByVal SecondsPerStepValue As Integer)
    Me.GroupBox1.Text = Text
    Me.max(1) = 10
    Me.seconds_per_step = SecondsPerStepValue
    Call Me.Reset()
  End Sub

  Public Sub Reset()
    Dim idx As Integer
    For idx = 0 To 1000
      value(0, idx) = 1
      value(1, idx) = 0
      value(2, idx) = 0
    Next
    Me.max(1) = 10
    Call InitPerSeconds()
    Me.Panel3.Refresh()
  End Sub

  Public Overrides Property Text() As String
    Get
      Return Me.GroupBox1.Text
    End Get
    Set(ByVal Value As String)
      Me.GroupBox1.Text = Value
    End Set
  End Property
End Class

