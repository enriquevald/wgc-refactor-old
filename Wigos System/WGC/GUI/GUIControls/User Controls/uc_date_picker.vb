'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_date_picker.vb  
'
' DESCRIPTION:   date picker control
'
' AUTHOR:        Andreu Juli�
'
' CREATION DATE: 18-APR-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 18-APR-2002  AJQ    Initial version
' 07-MAY-2015  FOS    Capture and Raise events CloseUp
'-------------------------------------------------------------------

Option Explicit On
Option Strict On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
'Imports GUI_CommonOperations.ModuleDateTimeFormats

'XVV -> Add NameSpace solve error with variables definitions
Imports System.Windows.Forms


Public Class uc_date_picker
  Inherits uc_base

  Private m_format_date As ENUM_FORMAT_DATE
  Private m_format_time As ENUM_FORMAT_TIME
  Private m_format_string As String

  Public Event DatePickerValueChanged()
  Public Event DatePickerCloseUp()

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

    Call Me.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                      ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    Me.Width = 200
    Me.Height = 30

    SetRange()

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents dt_picker As System.Windows.Forms.DateTimePicker
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.dt_picker = New System.Windows.Forms.DateTimePicker()
    Me.panel_control.SuspendLayout()
    Me.SuspendLayout()
    '
    'lbl_read_only
    '
    Me.lbl_read_only.Dock = System.Windows.Forms.DockStyle.None
    Me.lbl_read_only.Location = New System.Drawing.Point(84, 4)
    Me.lbl_read_only.Size = New System.Drawing.Size(216, 20)
    '
    'panel_control
    '
    Me.panel_control.Controls.AddRange(New System.Windows.Forms.Control() {Me.dt_picker})
    Me.panel_control.Location = New System.Drawing.Point(0, 0)
    Me.panel_control.Size = New System.Drawing.Size(304, 28)
    '
    'dt_picker
    '
    Me.dt_picker.Location = New System.Drawing.Point(84, 4)
    Me.dt_picker.Name = "dt_picker"
    Me.dt_picker.Size = New System.Drawing.Size(216, 20)
    Me.dt_picker.TabIndex = 0
    '
    'uc_date_picker
    '
    Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.lbl_read_only, Me.panel_control})
    Me.IsReadOnly = True
    Me.Name = "uc_date_picker"
    Me.Size = New System.Drawing.Size(304, 28)
    Me.SufixTextVisible = True
    Me.panel_control.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Value "

  Public Property Value() As Date
    Get
      Return Me.dt_picker.Value
    End Get
    Set(ByVal NewValue As Date)
      If (NewValue < Me.dt_picker.MinDate) Then
        NewValue = Me.dt_picker.MinDate
      End If
      If (NewValue > Me.dt_picker.MaxDate) Then
        NewValue = Me.dt_picker.MaxDate
      End If
      Me.dt_picker.Value = NewValue
      Me.ReadOnlyText = Format(NewValue, Me.dt_picker.CustomFormat)
    End Set
  End Property

  Public Sub SetRange(ByVal MinDate As Date, ByVal MaxDate As Date)

    Dim _now As DateTime

    ResetMinMaxDate(_now)
    dt_picker.MinDate = MinDate
    dt_picker.MaxDate = MaxDate
  End Sub

  Private Sub SetRange()

    Dim _now As DateTime

    ResetMinMaxDate(_now)
    dt_picker.Value = New DateTime(_now.Year, _now.Month, _now.Day)
  End Sub

  Private Sub ResetMinMaxDate(ByRef Now As DateTime)

    Now = WSI.Common.WGDB.Now
    dt_picker.MaxDate = New DateTime(Now.Year + 10, 1, 1)
    dt_picker.MinDate = New DateTime(2007, 1, 1)
  End Sub

#End Region


#Region " Format "

  Public Sub SetFormat(ByVal DateFormat As ENUM_FORMAT_DATE, ByVal TimeFormat As ENUM_FORMAT_TIME)
    m_format_date = DateFormat
    m_format_time = TimeFormat
    m_format_string = GUI_PatternDate(DateFormat, TimeFormat)

    dt_picker.Format = DateTimePickerFormat.Custom
    dt_picker.CustomFormat = m_format_string

    If IsDate(dt_picker.Value) Then
      dt_picker.Value = dt_picker.Value
    End If

    Call dt_picker.Refresh()
  End Sub

  Public Sub GetFormat(ByRef DateFormat As ENUM_FORMAT_DATE, ByRef TimeFormat As ENUM_FORMAT_TIME)
    DateFormat = m_format_date
    TimeFormat = m_format_time
  End Sub

#End Region

  'Public Property CustomFormat() As CLASS_DATE_TIME.ENUM_FORMAT
  '  Get
  '    Return m_format
  '  End Get
  '  Set(ByVal Value As CLASS_DATE_TIME.ENUM_FORMAT)
  '    m_format = Value
  '    Me.dt_picker.CustomFormat = CLASS_DATE_TIME.GetFormat(Value)
  '  End Set
  'End Property

  'Public Property FormatString() As String
  '  Get
  '    Return dt_picker.CustomFormat
  '  End Get
  '  Set(ByVal Value As String)
  '    dt_picker.CustomFormat = Value
  '  End Set
  'End Property

  Public Property ShowCheckBox() As Boolean
    Get
      Return Me.dt_picker.ShowCheckBox
    End Get
    Set(ByVal Value As Boolean)
      Me.dt_picker.ShowCheckBox = Value
    End Set
  End Property

  Public Property ShowUpDown() As Boolean
    Get
      Return Me.dt_picker.ShowUpDown
    End Get
    Set(ByVal Value As Boolean)
      Me.dt_picker.ShowUpDown = Value
    End Set
  End Property

  Public Overrides Property IsReadOnly() As Boolean
    Get
      Return MyBase.IsReadOnly
    End Get
    Set(ByVal Value As Boolean)
      Me.ReadOnlyText = GUI_FormatDate(dt_picker.Value, m_format_date, m_format_time)
      MyBase.IsReadOnly = Value
    End Set
  End Property

  Public Property Checked() As Boolean
    Get
      Return dt_picker.Checked
    End Get
    Set(ByVal Value As Boolean)
      dt_picker.Checked = Value
    End Set
  End Property

  Private Sub dt_picker_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dt_picker.ValueChanged
    RaiseEvent DatePickerValueChanged()
  End Sub

  Private Sub dt_picker_CloseUp(ByVal sender As Object, ByVal e As System.EventArgs) Handles dt_picker.CloseUp
    RaiseEvent DatePickerCloseUp()
  End Sub


  Public Function GetMinDate() As Date
    Return dt_picker.MinDate
  End Function

  Public Function GetMaxDate() As Date
    Return dt_picker.MaxDate
  End Function

End Class
