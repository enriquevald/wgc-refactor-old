'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_grid
' DESCRIPTION:   Grid Control
' AUTHOR:        Andreu Juli�
' CREATION DATE: 01-JUN-2002
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 01-JUN-2002 AJQ    First Version using the FlexGrid
' 02-OCT-2002 AJQ    Add Row.IsMerged
' 16-APR-2003 SCM    Add button Info
' 24-JAN-2012 RCI    Added routine ClearSelection()
' 24-JAN-2012 JMM    Added vertical alignment
' 18-APR-2012 RCI    Added events MultiSelectBeginEvent and MultiSelectEndEvent.
'                    They are raised when a multi-selection is done.
' 17-MAY-2012 JAB    Change AxMSFlexGrid to VSFlex8
' 21-MAY-2012 JAB    Changes to adapt VSFlex8
' 23-MAY-2012 JAB    Not to order by clicking on empty area
' 30-MAY-2012 JAB    Embed license component
' 31-MAY-2012 JAB    ocx file register
' 05-JUN-2012 JAB    Set sort property in the new component (vsFlexGrid8)
' 06-JUN-2012 JAB    Set col Type and add flx_grid_BeforeSort function. 
' 07-JUN-2012 JAB    Resize fonts.
' 13-JUN-2012 JAB    Added function GetTextMatrix: Return "" when value is Nothing (old behavior of get_texMatrix property).
' 28-JUN-2012 JAB    Added property to set row selection mode.
' 27-AUG-2012 JAB    Set font size of the secondary counters.
' 25-JAN-2013 RXM    assigned cell type before sorting for numeric cells to avoid error on sorting 
' 27-MAR-2013 RCI    mdl_color moved from GUI_Controls to GUI_CommonMisc
' 24-APR-2013 RBG    #746 forms that inherit from frm_base_sel can't do simple selection.
' 30-MAY-2013 JAB    When mouse is moved over grey area, without row or column, the ".MouseRow" or/and ".MouseCol" property are -1 and if we try to access it, fails.
' 30-MAY-2013 JAB    Modify "Height" property to not access to indexes out of range.
' 03-JUN-2013 JAB    Comment lines that contain "trace" clause (Debug.WriteLineIf(Trace...).
' 14-JUN-2013 DMR    Fixed bug #862 Change selected grid line through keyboard.
' 14-JUN-2013 JAB    Fixed bug #845 Some Excels not generated correctly.
' 07-NOV-2013 JAB    Fixed bug #393, don't show edit mark to prevent 'outofmemory' error when a lot of editable rows are shown.
' 22-NOV-2013 DLL    Added new function SetFont to set a specific Font in a Cell with the properties CellFontName and CellFontSize.
' 19-DEC-2013 JPJ    Added new property in the column to specify custom width, changed header property: now able to customize header text and alignment.
' 23-DEC-2013 JAB    Added property 'GridShowEditMode' to show mark or color in cell editable mode.
' 24-DEC-2013 JAB    Fixed bug, not initialize row height when init function is called other time with a different number of header rows.
' 05-DEC-2014 LRS    Add BeforeRowChange event
' 01-JUN-2015 DHA    Fixed Bug #WIG-2521
' 26-OCT-2016 FAV    PBI 19526:EGASA - CS Ticket 1459: The grid makes scrolling unnecessary
' 07-SEP-2017 EOR    Bug 29651: WIGOS-4450 Site Jackpot screen does not update the accumulated amounts tooltips
'------------------------------------------------------------------- 
' 
#Region " Imports "
Imports System.Data.OleDb
Imports GUI_CommonMisc
Imports GUI_CommonMisc.ControlColor.ENUM_GUI_COLOR
Imports GUI_CommonOperations
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_BUTTON
Imports GUI_CommonOperations.ModuleDateTimeFormats

'XVV -> Add NameSpace solve error with variables definitions
Imports System.Windows.Forms

#End Region

Public Class uc_grid
  Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

  Public Sub New()
    Call MyBase.New()

    ' This call is required by the Windows Form Designer.
    Call InitializeComponent()

    ' Add any initialization after the InitializeComponent() call

    ' JAB 30-MAY-2012: Embed license component
    FlexGridv8_Init()

  End Sub

  'UserControl overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        Call components.Dispose()
      End If
      If flx_grid IsNot Nothing Then
        flx_grid.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Private WithEvents panel_bottom As System.Windows.Forms.Panel
  Private WithEvents panel_top As System.Windows.Forms.Panel
  Private WithEvents panel_right As System.Windows.Forms.Panel
  Private WithEvents lbl_count0 As System.Windows.Forms.Label
  Private WithEvents lbl_count1 As System.Windows.Forms.Label
  Private WithEvents lbl_count2 As System.Windows.Forms.Label
  Private WithEvents lbl_count3 As System.Windows.Forms.Label
  Friend WithEvents lbl_count4 As System.Windows.Forms.Label
  Friend WithEvents btn_delete_row As GUI_Controls.uc_button
  Friend WithEvents btn_add_row As GUI_Controls.uc_button
  Friend WithEvents lbl_count5 As System.Windows.Forms.Label
  Friend WithEvents lbl_count6 As System.Windows.Forms.Label
  Friend WithEvents lbl_count7 As System.Windows.Forms.Label
  Friend WithEvents btn_info As GUI_Controls.uc_button
  Private WithEvents tool_tip As System.Windows.Forms.ToolTip
  Private WithEvents tmr_mouse_move As System.Windows.Forms.Timer
  Friend WithEvents flx_grid As AxVSFlex8.AxVSFlexGrid
  Friend WithEvents img_cell_edit As System.Windows.Forms.ImageList
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(uc_grid))
    Me.panel_bottom = New System.Windows.Forms.Panel()
    Me.panel_top = New System.Windows.Forms.Panel()
    Me.panel_right = New System.Windows.Forms.Panel()
    Me.btn_info = New GUI_Controls.uc_button()
    Me.lbl_count7 = New System.Windows.Forms.Label()
    Me.lbl_count6 = New System.Windows.Forms.Label()
    Me.lbl_count5 = New System.Windows.Forms.Label()
    Me.btn_add_row = New GUI_Controls.uc_button()
    Me.btn_delete_row = New GUI_Controls.uc_button()
    Me.lbl_count4 = New System.Windows.Forms.Label()
    Me.lbl_count3 = New System.Windows.Forms.Label()
    Me.lbl_count2 = New System.Windows.Forms.Label()
    Me.lbl_count1 = New System.Windows.Forms.Label()
    Me.lbl_count0 = New System.Windows.Forms.Label()
    Me.img_check = New System.Windows.Forms.ImageList(Me.components)
    Me.tool_tip = New System.Windows.Forms.ToolTip(Me.components)
    Me.tmr_mouse_move = New System.Windows.Forms.Timer(Me.components)
    Me.img_cell_edit = New System.Windows.Forms.ImageList(Me.components)
    Me.flx_grid = New AxVSFlex8.AxVSFlexGrid()
    Me.panel_right.SuspendLayout()
    CType(Me.flx_grid, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'panel_bottom
    '
    Me.panel_bottom.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.panel_bottom.Location = New System.Drawing.Point(0, 344)
    Me.panel_bottom.Name = "panel_bottom"
    Me.panel_bottom.Size = New System.Drawing.Size(152, 16)
    Me.panel_bottom.TabIndex = 0
    Me.panel_bottom.Visible = False
    '
    'panel_top
    '
    Me.panel_top.Dock = System.Windows.Forms.DockStyle.Top
    Me.panel_top.Location = New System.Drawing.Point(0, 0)
    Me.panel_top.Name = "panel_top"
    Me.panel_top.Size = New System.Drawing.Size(152, 56)
    Me.panel_top.TabIndex = 2
    Me.panel_top.Visible = False
    '
    'panel_right
    '
    Me.panel_right.Controls.Add(Me.btn_info)
    Me.panel_right.Controls.Add(Me.lbl_count7)
    Me.panel_right.Controls.Add(Me.lbl_count6)
    Me.panel_right.Controls.Add(Me.lbl_count5)
    Me.panel_right.Controls.Add(Me.btn_add_row)
    Me.panel_right.Controls.Add(Me.btn_delete_row)
    Me.panel_right.Controls.Add(Me.lbl_count4)
    Me.panel_right.Controls.Add(Me.lbl_count3)
    Me.panel_right.Controls.Add(Me.lbl_count2)
    Me.panel_right.Controls.Add(Me.lbl_count1)
    Me.panel_right.Controls.Add(Me.lbl_count0)
    Me.panel_right.Dock = System.Windows.Forms.DockStyle.Right
    Me.panel_right.Location = New System.Drawing.Point(87, 56)
    Me.panel_right.Name = "panel_right"
    Me.panel_right.Padding = New System.Windows.Forms.Padding(2, 0, 0, 0)
    Me.panel_right.Size = New System.Drawing.Size(65, 288)
    Me.panel_right.TabIndex = 3
    '
    'btn_info
    '
    Me.btn_info.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_info.Location = New System.Drawing.Point(2, 216)
    Me.btn_info.Name = "btn_info"
    Me.btn_info.Size = New System.Drawing.Size(24, 24)
    Me.btn_info.TabIndex = 11
    Me.btn_info.ToolTipped = False
    Me.btn_info.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.INFO
    Me.btn_info.Visible = False
    '
    'lbl_count7
    '
    Me.lbl_count7.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_count7.Location = New System.Drawing.Point(0, 191)
    Me.lbl_count7.Name = "lbl_count7"
    Me.lbl_count7.Size = New System.Drawing.Size(56, 23)
    Me.lbl_count7.TabIndex = 10
    Me.lbl_count7.Text = "0"
    Me.lbl_count7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_count7.Visible = False
    '
    'lbl_count6
    '
    Me.lbl_count6.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_count6.Location = New System.Drawing.Point(0, 164)
    Me.lbl_count6.Name = "lbl_count6"
    Me.lbl_count6.Size = New System.Drawing.Size(56, 23)
    Me.lbl_count6.TabIndex = 9
    Me.lbl_count6.Text = "0"
    Me.lbl_count6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_count6.Visible = False
    '
    'lbl_count5
    '
    Me.lbl_count5.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_count5.Location = New System.Drawing.Point(0, 137)
    Me.lbl_count5.Name = "lbl_count5"
    Me.lbl_count5.Size = New System.Drawing.Size(56, 23)
    Me.lbl_count5.TabIndex = 8
    Me.lbl_count5.Text = "0"
    Me.lbl_count5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_count5.Visible = False
    '
    'btn_add_row
    '
    Me.btn_add_row.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_add_row.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_add_row.Location = New System.Drawing.Point(2, 246)
    Me.btn_add_row.Name = "btn_add_row"
    Me.btn_add_row.Size = New System.Drawing.Size(50, 21)
    Me.btn_add_row.TabIndex = 1
    Me.btn_add_row.ToolTipped = False
    Me.btn_add_row.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    Me.btn_add_row.Visible = False
    '
    'btn_delete_row
    '
    Me.btn_delete_row.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_delete_row.Dock = System.Windows.Forms.DockStyle.Bottom
    Me.btn_delete_row.Location = New System.Drawing.Point(2, 267)
    Me.btn_delete_row.Name = "btn_delete_row"
    Me.btn_delete_row.Size = New System.Drawing.Size(50, 21)
    Me.btn_delete_row.TabIndex = 2
    Me.btn_delete_row.ToolTipped = False
    Me.btn_delete_row.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.ADD_DELETE
    Me.btn_delete_row.Visible = False
    '
    'lbl_count4
    '
    Me.lbl_count4.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_count4.Location = New System.Drawing.Point(0, 110)
    Me.lbl_count4.Name = "lbl_count4"
    Me.lbl_count4.Size = New System.Drawing.Size(56, 23)
    Me.lbl_count4.TabIndex = 4
    Me.lbl_count4.Text = "0"
    Me.lbl_count4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_count4.Visible = False
    '
    'lbl_count3
    '
    Me.lbl_count3.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_count3.Location = New System.Drawing.Point(0, 83)
    Me.lbl_count3.Name = "lbl_count3"
    Me.lbl_count3.Size = New System.Drawing.Size(56, 23)
    Me.lbl_count3.TabIndex = 3
    Me.lbl_count3.Text = "0"
    Me.lbl_count3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_count3.Visible = False
    '
    'lbl_count2
    '
    Me.lbl_count2.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_count2.Location = New System.Drawing.Point(0, 56)
    Me.lbl_count2.Name = "lbl_count2"
    Me.lbl_count2.Size = New System.Drawing.Size(56, 23)
    Me.lbl_count2.TabIndex = 2
    Me.lbl_count2.Text = "0"
    Me.lbl_count2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_count2.Visible = False
    '
    'lbl_count1
    '
    Me.lbl_count1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_count1.Location = New System.Drawing.Point(0, 29)
    Me.lbl_count1.Name = "lbl_count1"
    Me.lbl_count1.Size = New System.Drawing.Size(56, 23)
    Me.lbl_count1.TabIndex = 1
    Me.lbl_count1.Text = "0"
    Me.lbl_count1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_count1.Visible = False
    '
    'lbl_count0
    '
    Me.lbl_count0.BackColor = System.Drawing.Color.Blue
    Me.lbl_count0.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_count0.ForeColor = System.Drawing.Color.White
    Me.lbl_count0.Location = New System.Drawing.Point(0, 2)
    Me.lbl_count0.Name = "lbl_count0"
    Me.lbl_count0.Size = New System.Drawing.Size(65, 23)
    Me.lbl_count0.TabIndex = 0
    Me.lbl_count0.Text = "9999"
    Me.lbl_count0.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'img_check
    '
    Me.img_check.ImageStream = CType(resources.GetObject("img_check.ImageStream"), System.Windows.Forms.ImageListStreamer)
    Me.img_check.TransparentColor = System.Drawing.Color.Transparent
    Me.img_check.Images.SetKeyName(0, "")
    Me.img_check.Images.SetKeyName(1, "")
    Me.img_check.Images.SetKeyName(2, "")
    Me.img_check.Images.SetKeyName(3, "")
    '
    'tool_tip
    '
    Me.tool_tip.AutomaticDelay = 5000
    Me.tool_tip.AutoPopDelay = 500
    Me.tool_tip.InitialDelay = 5000
    Me.tool_tip.ReshowDelay = 5000
    '
    'tmr_mouse_move
    '
    Me.tmr_mouse_move.Interval = 1000
    '
    'img_cell_edit
    '
    Me.img_cell_edit.ImageStream = CType(resources.GetObject("img_cell_edit.ImageStream"), System.Windows.Forms.ImageListStreamer)
    Me.img_cell_edit.TransparentColor = System.Drawing.Color.Transparent
    Me.img_cell_edit.Images.SetKeyName(0, "")
    '
    'flx_grid
    '
    Me.flx_grid.DataSource = Nothing
    Me.flx_grid.Dock = System.Windows.Forms.DockStyle.Fill
    Me.flx_grid.Location = New System.Drawing.Point(0, 56)
    Me.flx_grid.Name = "flx_grid"
    Me.flx_grid.OcxState = CType(resources.GetObject("flx_grid.OcxState"), System.Windows.Forms.AxHost.State)
    Me.flx_grid.Size = New System.Drawing.Size(87, 288)
    Me.flx_grid.TabIndex = 0
    '
    'uc_grid
    '
    Me.Controls.Add(Me.flx_grid)
    Me.Controls.Add(Me.panel_right)
    Me.Controls.Add(Me.panel_top)
    Me.Controls.Add(Me.panel_bottom)
    Me.Name = "uc_grid"
    Me.Size = New System.Drawing.Size(152, 360)
    Me.panel_right.ResumeLayout(False)
    CType(Me.flx_grid, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Public Events declaration "

  Public Event DataSelectedEvent()

  Public Event CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer)

  Public Event BeforeStartEditionEvent(ByVal Row As Integer, ByVal Column As Integer, _
                                       ByRef EditionCanceled As Boolean)

  Public Event ButtonClickEvent(ByVal ButtonId As ENUM_BUTTON_TYPE)

  Public Event SetToolTipTextEvent(ByVal GridRowas As Integer, ByVal GridCol As Integer, ByRef Txt As String)

  Public Event DataUpdated()

  Public Event RowSelectedEvent(ByVal SelectedRow As Integer)

  Public Event BeforeRowChanged(ByVal OldRow As Integer, ByVal NewRow As Integer, ByRef CancelRowChange As Boolean)

  Public Event MultiSelectBeginEvent(ByVal BeginRow As Integer)
  Public Event MultiSelectEndEvent(ByVal EndRow As Integer)


#End Region

#Region " Members "

  Private m_init As Boolean = False
  Private m_rows As Integer = 0
  Private m_cols As Integer = 0
  Private m_row_data() As CLASS_ROW_DATA
  Private m_col_data() As CLASS_COL_DATA
  Private m_last_col As Integer = -1
  Private m_last_row As Integer = -1
  Private m_curr_row As Integer = -1
  Private m_curr_col As Integer = -1
  Private m_semaphore As Integer = 0
  Private m_semaphore1 As Integer = 0
  Private m_keep_selection As Boolean = False
  Friend m_multiple_selection_allowed = False
  Private m_init_fi As Boolean = False
  Private m_editable As Boolean = False

  Private m_ctrl_shift As Boolean = False
  Private m_all_selected As Boolean = False

  Private GLB_IsSortable As Boolean = False
  Private GLB_SortColumn As Integer
  Private GLB_SortAscending As Boolean = True

  Private GLB_IsToolTipped As Boolean = True

  ' tool tip members
  Private m_tt_last_mouse_row As Integer = -1
  Private m_tt_last_mouse_col As Integer = -1
  Private m_last_tooltip As Date

  ' JAB 30-MAY-2012: Embed license component 
  Public Shared m_flexgrid_v8_activated As Boolean = False
  ' JAB 06-JUN-2012: Set col Type.
  Private m_col_type_set As Boolean = False

  ' JAB 27-JUN-2012: ALIGNMENT MODE (multiple is default value)
  Private m_selection_mode = SELECTION_MODE.SELECTION_MODE_MULTIPLE

  ' JAB 23-DEC-2013: Show mark, backcolor or nothing in editable cells or
  Private m_editable_cell_show_mode As Int32

  ' Back Color (GRID_SHOW_EDIT_MODE.SHOW_BACKCOLOR) 2
  Private m_editable_cell_back_color As System.Drawing.Color

  ' Border (GRID_SHOW_EDIT_MODE.SHOW_BORDER) 3
  Private m_editable_cell_border_color As System.Drawing.Color
  Private m_editable_cell_border_size As TYPE_CELL_BORDER

  Private m_nocount_rows_with_small_height As Boolean = False

#End Region

#Region " Constants "

  '       - Column check states
  Public Const GRID_CHK_CHECKED As String = vbCrLf & "0"
  Public Const GRID_CHK_UNCHECKED As String = vbCrLf & "1"
  Public Const GRID_CHK_CHECKED_DISABLED As String = vbCrLf & "2"
  Public Const GRID_CHK_UNCHECKED_DISABLED As String = vbCrLf & "3"
  Public Const GRID_CHK_NONE As String = vbCrLf & "4"

  Private Const MAX_HEADERS As Integer = 3
  Private Const Trace As Boolean = False
  Private Const MAX_COUNTERS As Integer = 8
  Private Const MIN_ROWS As Integer = 5
  Private Const AVG_ROW_HEIGHT As Integer = 15
  Private Const DEFAULT_HEADER_HEIGHT As Integer = 240

  Private Const ALLOWS_EXPAND_SYMBOL As String = "[+] "
  Private Const ALLOWS_COLLAPSE_SYMBOL As String = "[-]  "

  Public Enum ENUM_GRID_EXPAND_COLLAPSE
    GRID_EXPAND_COLLAPSE_NOTHING = -1
    GRID_EXPAND_ALL = 0
    GRID_COLLAPSE_ALL = 1
  End Enum

  ' JAB 27-JUN-2012: ALIGNMENT MODE (multiple is default value)
  Public Enum SELECTION_MODE
    SELECTION_MODE_MULTIPLE = 1
    SELECTION_MODE_SINGLE = 2
  End Enum

  Public Enum GRID_SHOW_EDIT_MODE
    SHOW_NONE = 0
    SHOW_MARK = 1
    SHOW_BACKCOLOR = 2
    SHOW_BORDER = 3
  End Enum

  Public Structure TYPE_CELL_BORDER
    Dim m_left As Int16
    Dim m_top As Int16
    Dim m_right As Int16
    Dim m_bottom As Int16
    Dim m_vertical As Int16
    Dim m_horizontal As Int16
  End Structure

#End Region

#Region " Variables "
  ' Variable to handle event in the dynamic controls
  Private WithEvents ComboBoxEvents As uc_combo
  Private WithEvents EntryFieldEvents As uc_entry_field
  Private WithEvents DateTimeEvents As uc_date_picker


#End Region

#Region " SubClass "

  Public Class CLASS_ROW_DATA
    Private m_grid As uc_grid
    Private m_index As Integer
    Private m_is_empty As Boolean
    Private m_fore_color As System.Drawing.Color
    Private m_back_color As System.Drawing.Color
    Private m_fore_color_sel As System.Drawing.Color
    Private m_back_color_sel As System.Drawing.Color
    Private m_is_selected As Boolean
    Private m_cell_count As Integer = 0
    Private m_cell_color() As TYPE_CELL_COLOR
    Private m_col_index As Integer

    Private Structure TYPE_CELL_COLOR
      Dim m_col As Integer
      Dim m_fore_color As System.Drawing.Color
      Dim m_back_color As System.Drawing.Color
    End Structure

    Public Property IsEmpty() As Boolean
      Get
        Return m_is_empty
      End Get
      Set(ByVal Value As Boolean)
        m_is_empty = Value
      End Set
    End Property

    Private Function SeekCellColor(ByVal Col As Integer) As Integer
      'XVV Variable not use Dim idx As Integer

      'For idx = 0 To m_cell_count - 1
      '  If m_cell_color(idx).m_col = Col Then
      '    Return idx
      '  End If
      'Next
      If m_cell_count - 1 < Col Then
        Return -1
      Else
        Return Col
      End If

      'Return -1

    End Function

    Friend Sub SetCellColor(ByVal Col As Integer, ByVal CellForeColor As System.Drawing.Color, ByVal CellBackColor As System.Drawing.Color)
      Dim idx As Integer

      idx = SeekCellColor(Col)

      If idx < 0 Then
        m_cell_count = Col
        idx = m_cell_count
        m_cell_count = m_cell_count + 1
        ReDim Preserve m_cell_color(m_cell_count)
      End If

      If m_cell_color(idx).m_back_color.Equals(CellForeColor) And m_cell_color(idx).m_back_color.Equals(CellBackColor) Then
        Return
      End If

      m_cell_color(idx).m_col = Col
      m_cell_color(idx).m_back_color = CellBackColor
      m_cell_color(idx).m_fore_color = CellForeColor

    End Sub

    Friend Sub GetCellColor(ByVal Col As Integer, ByRef CellForeColor As System.Drawing.Color, ByRef CellBackColor As System.Drawing.Color)
      Dim idx As Integer

      idx = SeekCellColor(Col)

      If idx < 0 Then
        CellForeColor = Me.ForeColor
        CellBackColor = Me.BackColor
      Else
        CellForeColor = m_cell_color(idx).m_fore_color
        CellBackColor = m_cell_color(idx).m_back_color
      End If
    End Sub

    Public Property ForeColor() As System.Drawing.Color
      Get
        Return m_fore_color
      End Get
      Set(ByVal Value As System.Drawing.Color)
        m_fore_color = Value
        Call m_grid.RepaintRow(Me.m_grid.IndexToRow(Me.m_index))
      End Set
    End Property

    Public Property BackColor() As System.Drawing.Color
      Get
        Return m_back_color
      End Get
      Set(ByVal Value As System.Drawing.Color)
        m_back_color = Value
        Call m_grid.RepaintRow(Me.m_grid.IndexToRow(Me.m_index))
      End Set
    End Property

    Public Property ForeColorSel() As System.Drawing.Color
      Get
        Return m_fore_color_sel
      End Get
      Set(ByVal Value As System.Drawing.Color)
        m_fore_color_sel = Value
        Call m_grid.RepaintRow(Me.m_grid.IndexToRow(Me.m_index))
      End Set
    End Property

    Public Property IsSelected() As Boolean
      Get
        Return m_is_selected
      End Get
      Set(ByVal Value As Boolean)
        m_is_selected = Value
      End Set
    End Property

    Public Property BackColorSel() As System.Drawing.Color
      Get
        Return m_back_color_sel
      End Get
      Set(ByVal Value As System.Drawing.Color)
        m_back_color_sel = Value
        Call m_grid.RepaintRow(Me.m_grid.IndexToRow(Me.m_index))
      End Set
    End Property

    Public Sub New(ByVal Grid As uc_grid, ByVal Row As Integer)
      Me.m_grid = Grid
      Me.m_index = Row
      Me.m_is_empty = False
      Me.m_back_color = Grid.Settings.BackColor
      Me.m_fore_color = Grid.Settings.ForeColor
      Me.m_back_color_sel = Grid.Settings.BackColorSel
      Me.m_fore_color_sel = Grid.Settings.ForeColorSel

    End Sub

    Public Property IsMerged() As Boolean
      Get
        Return m_grid.flx_grid.get_MergeRow(Me.m_index + MAX_HEADERS)
      End Get
      Set(ByVal Value As Boolean)
        m_grid.flx_grid.set_MergeRow(Me.m_index + MAX_HEADERS, Value)
      End Set
    End Property

    Public Property Height() As Integer
      Get
        ' ATB 02-NOV-2016
        ' To avoid a problem when it takes the grid size, we ignore the out of range
        If (Me.m_index + m_grid.flx_grid.FixedRows >= m_grid.flx_grid.Rows) Then
          Call Debug.WriteLine("Index > Rows getting the height")
          Exit Property
        End If
        Return m_grid.flx_grid.get_RowHeight(Me.m_index + m_grid.flx_grid.FixedRows)
      End Get
      Set(ByVal Value As Integer)
        Try
          If (Me.m_index + m_grid.flx_grid.FixedRows >= m_grid.flx_grid.Rows) Then
            Call Debug.WriteLine("Index > Rows setting the height")
            Exit Property

          End If

          m_grid.flx_grid.set_RowHeight(Me.m_index + m_grid.flx_grid.FixedRows, Value)
          'Oc 29/04/04 -added to support collapse and expand
          For m_col_index = 1 To m_grid.flx_grid.Cols
            If m_grid.Column(m_col_index) Is Nothing Then
              'DO nothing
            Else
              If m_grid.Column(m_col_index).EditionControl Is Nothing Then
                'DO nothing
              Else
                If m_grid.Column(m_col_index).EditionControl.Type = CONTROL_TYPE_CHECK_BOX Then
                  Call m_grid.PaintCheckBox(Me.m_index + m_grid.flx_grid.FixedRows, m_col_index)
                End If
              End If
            End If

          Next

        Catch exception As Exception
          Call Debug.WriteLine(exception.ToString())
          Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                "UC_GRID", _
                                "Set Height", _
                                exception.Message)

        End Try
      End Set
    End Property

    Public Sub SetSignalAllowCollapse(ByVal ColumnForSignal As Integer)
      Me.m_grid.Cell(Me.m_grid.IndexToRow(m_index), ColumnForSignal).Value = ALLOWS_COLLAPSE_SYMBOL & _
                                                       Me.m_grid.Cell(Me.m_grid.IndexToRow(m_index), ColumnForSignal).Value
    End Sub

    Public Sub SetSignalAllowExpand(ByVal ColumnForSignal As Integer)
      Me.m_grid.Cell(Me.m_grid.IndexToRow(m_index), ColumnForSignal).Value = ALLOWS_EXPAND_SYMBOL & _
                                                       Me.m_grid.Cell(Me.m_grid.IndexToRow(m_index), ColumnForSignal).Value
    End Sub

    Public Function IsExpanded(ByVal ColumnForSignal As Integer) As Boolean
      Dim cell_value As String

      cell_value = Me.m_grid.Cell(Me.m_grid.IndexToRow(m_index), ColumnForSignal).Value

      If Mid(cell_value, 1, Len(ALLOWS_EXPAND_SYMBOL)) = ALLOWS_EXPAND_SYMBOL Then
        Return True
      Else
        Return False
      End If

    End Function

    Public Function IsCollapsed(ByVal ColumnForSignal As Integer) As Boolean
      Dim cell_value As String

      cell_value = Me.m_grid.Cell(Me.m_grid.IndexToRow(m_index), ColumnForSignal).Value

      If Mid(cell_value, 1, Len(ALLOWS_COLLAPSE_SYMBOL)) = ALLOWS_COLLAPSE_SYMBOL Then
        Return True
      Else
        Return False
      End If

    End Function

  End Class

  Public Class CLASS_CELL_DATA
    Public Enum ENUM_ALIGN
      ALIGN_LEFT = 1
      ALIGN_RIGHT = 2
      ALIGN_CENTER = 3
    End Enum

    Private m_grid As uc_grid
    Private m_row As Integer
    Private m_col As Integer

    Public Sub New(ByVal Grid As uc_grid, ByVal RowIndex As Integer, ByVal ColIndex As Integer)
      m_grid = Grid
      m_row = RowIndex
      m_col = ColIndex
    End Sub

    Public Property Alignment() As Integer
      Set(ByVal Value As Integer)
        Dim curr_row As Integer
        Dim curr_col As Integer
        Dim redraw As Boolean

        m_grid.m_semaphore = m_grid.m_semaphore + 1
        curr_row = m_grid.flx_grid.Row
        curr_col = m_grid.flx_grid.Col
        redraw = m_grid.flx_grid.Redraw

        m_grid.flx_grid.Redraw = False
        m_grid.flx_grid.Row = m_row + MAX_HEADERS
        m_grid.flx_grid.Col = m_col

        Select Case Value
          Case ENUM_ALIGN.ALIGN_CENTER
            m_grid.flx_grid.CellAlignment = VSFlex8.AlignmentSettings.flexAlignCenterCenter
          Case ENUM_ALIGN.ALIGN_LEFT
            m_grid.flx_grid.CellAlignment = VSFlex8.AlignmentSettings.flexAlignLeftCenter
          Case ENUM_ALIGN.ALIGN_RIGHT
            m_grid.flx_grid.CellAlignment = VSFlex8.AlignmentSettings.flexAlignRightCenter
          Case Else
            m_grid.flx_grid.CellAlignment = VSFlex8.AlignmentSettings.flexAlignLeftCenter
        End Select

        m_grid.flx_grid.Row = curr_row
        m_grid.flx_grid.Col = curr_col
        m_grid.flx_grid.Redraw = redraw
        m_grid.m_semaphore = m_grid.m_semaphore - 1
      End Set
      Get
        Dim control_cell_alignment As Short
        Dim cell_aligment_2_return As Short
        Dim cur_row As Integer
        Dim cur_col As Integer

        cur_row = m_grid.flx_grid.Row
        cur_col = m_grid.flx_grid.Col

        If cur_row <> m_row + MAX_HEADERS Then
          Exit Property
        End If
        If cur_col <> m_col Then
          Exit Property
        End If

        control_cell_alignment = m_grid.flx_grid.CellAlignment

        Select Case control_cell_alignment
          Case VSFlex8.AlignmentSettings.flexAlignCenterCenter
            cell_aligment_2_return = ENUM_ALIGN.ALIGN_CENTER
          Case VSFlex8.AlignmentSettings.flexAlignLeftCenter
            cell_aligment_2_return = ENUM_ALIGN.ALIGN_LEFT
          Case VSFlex8.AlignmentSettings.flexAlignRightCenter
            cell_aligment_2_return = ENUM_ALIGN.ALIGN_RIGHT
          Case Else
            cell_aligment_2_return = ENUM_ALIGN.ALIGN_LEFT
        End Select

        Return cell_aligment_2_return
      End Get
    End Property

    Public Property Value() As String
      Get
        Return m_grid.Value(m_row, m_col)
      End Get
      Set(ByVal NewValue As String)
        m_grid.Value(m_row, m_col) = NewValue
      End Set
    End Property

    Public WriteOnly Property ForeColor() As System.Drawing.Color
      Set(ByVal Value As System.Drawing.Color)
        m_grid.SetCellForeColor(m_row, m_col, Value)
      End Set
    End Property

    Public Property BackColor() As System.Drawing.Color
      Set(ByVal Value As System.Drawing.Color)
        m_grid.SetCellBackColor(m_row, m_col, Value)
      End Set
      Get
        Dim _fore_color As Drawing.Color
        Dim _back_color As Drawing.Color
        m_grid.Row(m_row).GetCellColor(m_col, _fore_color, _back_color)
        Return _back_color
      End Get
    End Property


    'IRP To insert image into Grid
    Public WriteOnly Property Picture() As System.Drawing.Image
      Set(ByVal Value As System.Drawing.Image)
        m_grid.InsertImage(MAX_HEADERS + m_row, m_col, Value)
      End Set
    End Property

  End Class

  Public Class CLASS_COL_DATA
    Private m_grid As uc_grid
    Private m_index As Integer
    Private m_format As String
    Private m_width As Integer
    Private m_db_row As Integer
    Private m_status As Boolean
    Private m_control_data As CLASS_CONTROL
    Private m_control_data_aux As CLASS_CONTROL
    Private m_fixed As Boolean
    Private m_width_fixed As Integer
    Private m_highlight As Boolean = True
    Private m_sortable As Boolean = True
    Private m_tooltipped As Boolean = True
    Private m_rows_own_control As SortedList
    Private m_printable As Boolean
    Private m_print_width As Integer
    Private m_excel_print_width As Decimal
    Private m_header() As CLASS_HEADER
    Private m_system_type As Type

    Public Enum ENUM_ALIGN
      ALIGN_LEFT = 1
      ALIGN_RIGHT = 2
      ALIGN_CENTER = 3
      ALIGN_LEFT_CENTER = 4
      ALIGN_RIGHT_CENTER = 5
      ALIGN_CENTER_CENTER = 6
      ALIGN_LEFT_BOTTOM = 7
      ALIGN_RIGHT_BOTTOM = 8
      ALIGN_CENTER_BOTTOM = 9
    End Enum

    Public Enum ENUM_COLDATATYPE
      FLEX_DT_EMPTY = 0
      FLEX_DT_NULL = 1
      FLEX_DT_SHORT = 2
      FLEX_DT_LONG = 3
      FLEX_DT_SINGLE = 4
      FLEX_DT_DOUBLE = 5
      FLEX_DT_CURRENCY = 6
      FLEX_DT_DATE = 7
      FLEX_DT_STRING = 8
      FLEX_DT_DISPATCH = 9
      FLEX_DT_ERROR = 10
      FLEX_DT_BOOLEAN = 11
      FLEX_DT_VARIANT = 12
      FLEX_DT_UNKNOWN = 13
      FLEX_DT_DECIMAL = 14
      FLEX_DT_LONG8 = 20
      FLEX_DT_STRINGC = 30
      FLEX_DT_STRINGW = 31
    End Enum

    Public Sub New(ByVal Grid As uc_grid, ByVal Index As Integer)
      m_grid = Grid
      m_index = Index
      m_db_row = -1
      m_control_data = New CLASS_CONTROL(Grid, Index)
      m_fixed = False
      m_rows_own_control = New SortedList()
      m_printable = True
      m_print_width = 0
      m_excel_print_width = 0
      m_system_type = Type.GetType("System.String")
    End Sub

    Public Property SystemType() As Type
      Get
        Return m_system_type
      End Get
      Set(ByVal Value As Type)
        m_system_type = Value
      End Set
    End Property

    Public Property Mapping() As Integer
      Get
        Return m_db_row
      End Get
      Set(ByVal Value As Integer)
        m_db_row = Value
      End Set
    End Property

    'Returns or sets the editable state of the current column
    Public Property Editable() As Boolean
      Get
        Return m_status
      End Get
      Set(ByVal Value As Boolean)
        ' Check if grid is editable
        If m_grid.flx_grid.SelectionMode = VSFlex8.SelModeSettings.flexSelectionFree Then
          m_status = Value
        Else
          m_status = False
        End If
      End Set
    End Property

    Public Property Fixed() As Boolean
      Get
        Return Me.m_fixed
      End Get
      Set(ByVal Value As Boolean)
        Me.m_fixed = Value
      End Set

    End Property

    Public Property WidthFixed() As Integer
      Get
        Return Me.m_width_fixed
      End Get
      Set(ByVal Value As Integer)
        Me.m_fixed = True
        Me.m_width_fixed = Value
        m_grid.flx_grid.set_ColWidth(m_index, Value)
      End Set
    End Property

    Public Property Width() As Integer
      Get
        Return m_grid.flx_grid.get_ColWidth(m_index)
      End Get
      Set(ByVal Value As Integer)
        'IRP
        If Me.m_fixed = False Then
          If Value = 0 Then
            Me.m_fixed = True
            Me.m_width_fixed = Value
          End If
          m_grid.flx_grid.set_ColWidth(m_index, Value)
        End If
      End Set
    End Property

    Public Property IsMerged() As Boolean
      Get
        Return m_grid.flx_grid.get_MergeCol(Me.m_index)
      End Get
      Set(ByVal Value As Boolean)
        m_grid.flx_grid.set_MergeCol(Me.m_index, Value)
      End Set
    End Property

    Public WriteOnly Property Alignment() As ENUM_ALIGN
      Set(ByVal Value As ENUM_ALIGN)
        Select Case Value
          Case ENUM_ALIGN.ALIGN_CENTER
            m_grid.flx_grid.set_ColAlignment(Me.m_index, VSFlex8.AlignmentSettings.flexAlignCenterTop)
          Case ENUM_ALIGN.ALIGN_LEFT
            m_grid.flx_grid.set_ColAlignment(Me.m_index, VSFlex8.AlignmentSettings.flexAlignLeftTop)
          Case ENUM_ALIGN.ALIGN_RIGHT
            m_grid.flx_grid.set_ColAlignment(Me.m_index, VSFlex8.AlignmentSettings.flexAlignRightTop)
          Case ENUM_ALIGN.ALIGN_CENTER_CENTER
            m_grid.flx_grid.set_ColAlignment(Me.m_index, VSFlex8.AlignmentSettings.flexAlignCenterCenter)
          Case ENUM_ALIGN.ALIGN_LEFT_CENTER
            m_grid.flx_grid.set_ColAlignment(Me.m_index, VSFlex8.AlignmentSettings.flexAlignLeftTop)
          Case ENUM_ALIGN.ALIGN_RIGHT_CENTER
            m_grid.flx_grid.set_ColAlignment(Me.m_index, VSFlex8.AlignmentSettings.flexAlignRightTop)
          Case ENUM_ALIGN.ALIGN_CENTER_BOTTOM
            m_grid.flx_grid.set_ColAlignment(Me.m_index, VSFlex8.AlignmentSettings.flexAlignCenterBottom)
          Case ENUM_ALIGN.ALIGN_LEFT_BOTTOM
            m_grid.flx_grid.set_ColAlignment(Me.m_index, VSFlex8.AlignmentSettings.flexAlignRightBottom)
          Case ENUM_ALIGN.ALIGN_RIGHT_BOTTOM
            m_grid.flx_grid.set_ColAlignment(Me.m_index, VSFlex8.AlignmentSettings.flexAlignRightBottom)
          Case Else
            m_grid.flx_grid.set_ColAlignment(Me.m_index, VSFlex8.AlignmentSettings.flexAlignLeftTop)
        End Select
      End Set
    End Property

    Public Property HighLightWhenSelected() As Boolean
      Get
        Return m_highlight
      End Get
      Set(ByVal Value As Boolean)
        m_highlight = Value
      End Set
    End Property

    Public Property IsColumnSortable() As Boolean
      Get
        Return m_sortable
      End Get
      Set(ByVal Value As Boolean)
        ' Check if grid is sortable
        If m_grid.Sortable Then
          m_sortable = Value
        Else
          m_sortable = False
        End If
      End Set
    End Property

    Public Property IsColumnToolTipped() As Boolean
      Get
        Return m_tooltipped
      End Get
      Set(ByVal Value As Boolean)
        ' Check if grid is tooltipped
        If m_grid.ToolTipped Then
          m_tooltipped = Value
        Else
          m_tooltipped = False
        End If
      End Set
    End Property

    Public Property IsColumnPrintable() As Boolean
      Get
        Return m_printable
      End Get
      Set(ByVal Value As Boolean)
        m_printable = Value
      End Set
    End Property

    Public Property PrintWidth() As Integer
      Get
        Return m_print_width
      End Get
      Set(ByVal Value As Integer)
        m_print_width = Value
      End Set
    End Property

    'Indicates the predifined length for one Excel column 
    Public Property ExcelWidth() As Decimal
      Get
        Return m_excel_print_width
      End Get
      Set(ByVal Value As Decimal)
        If Value < 0 OrElse Value > 255 Then
          Throw New Exception("Excel width value must be between 0 and 255")
        End If

        m_excel_print_width = Value
      End Set
    End Property

    Public Class CLASS_HEADER
      Private m_grid As uc_grid
      Private m_row As Integer
      Private m_col As Integer
      Private m_excel_text As String = ""
      Private m_excel_alignment As ENUM_ALIGN

      Public Sub New(ByVal Grid As uc_grid, ByVal RowIndex As Integer, ByVal ColIndex As Integer)
        m_grid = Grid
        m_row = RowIndex
        m_col = ColIndex
        m_excel_text = ""
        m_excel_alignment = ENUM_ALIGN.ALIGN_CENTER
      End Sub

      Public Property Text() As String
        Get
          Return m_grid.GetTextMatrix(m_row, m_col)
        End Get
        Set(ByVal Value As String)
          m_grid.flx_grid.set_TextMatrix(m_row, _
                                         m_col, _
                                         Value)
        End Set
      End Property

      Public Property ExcelText() As String
        Get
          Return m_excel_text
        End Get
        Set(ByVal Value As String)
          m_excel_text = Value
        End Set
      End Property

      Public Property ExcelAlignment() As ENUM_ALIGN
        Get
          Return m_excel_alignment
        End Get
        Set(ByVal Value As ENUM_ALIGN)
          m_excel_alignment = Value
        End Set
      End Property

      Public WriteOnly Property Alignment() As ENUM_ALIGN
        Set(ByVal Value As ENUM_ALIGN)
          Dim curr_row As Integer
          Dim curr_col As Integer
          Dim redraw As Boolean

          m_grid.m_semaphore = m_grid.m_semaphore + 1
          curr_row = m_grid.flx_grid.Row
          curr_col = m_grid.flx_grid.Col
          redraw = m_grid.flx_grid.Redraw

          m_grid.flx_grid.Redraw = False
          m_grid.flx_grid.Row = m_row
          m_grid.flx_grid.Col = m_col

          Select Case Value
            Case ENUM_ALIGN.ALIGN_CENTER
              m_grid.flx_grid.CellAlignment = VSFlex8.AlignmentSettings.flexAlignCenterCenter
              'Case ENUM_ALIGN.ALIGN_LEFT
              '  m_grid.flx_grid.CellAlignment = VSFlex8.AlignmentSettings.flexAlignLeftCenter
              'Case ENUM_ALIGN.ALIGN_RIGHT
              '  m_grid.flx_grid.CellAlignment = VSFlex8.AlignmentSettings.flexAlignRightCenter
            Case Else
              m_grid.flx_grid.CellAlignment = VSFlex8.AlignmentSettings.flexAlignCenterCenter
          End Select

          m_grid.flx_grid.Row = curr_row
          m_grid.flx_grid.Col = curr_col
          m_grid.flx_grid.Redraw = redraw
          m_grid.m_semaphore = m_grid.m_semaphore - 1

        End Set

      End Property

    End Class

    Public ReadOnly Property Header(Optional ByVal Row As Integer = 0) As CLASS_HEADER
      Get
        If m_header Is Nothing Then
          ReDim Preserve m_header(Row)
        Else
          If Row >= m_header.Length Then
            ReDim Preserve m_header(Row)
          End If
        End If

        If m_header(Row) Is Nothing Then
          m_header(Row) = New CLASS_HEADER(Me.m_grid, Row, Me.m_index)
        End If
        Return m_header(Row)
      End Get
    End Property

    Public Property RowWithOwnControl(ByVal Row As Integer) As CLASS_CONTROL.ENUM_CONTROL_TYPE
      Get
        If m_rows_own_control.ContainsKey(Row) Then
          Return m_rows_own_control.GetValueList(m_rows_own_control.IndexOfKey(Row))
        Else
          Return 0
        End If
      End Get
      Set(ByVal Value As CLASS_CONTROL.ENUM_CONTROL_TYPE)
        If m_rows_own_control.ContainsKey(Row) Then
          m_rows_own_control.Remove(Row)
        End If
        m_rows_own_control.Add(Row, Value)
        If Value = CONTROL_TYPE_CHECK_BOX Then
          Call m_grid.PaintCheckBox(Row + MAX_HEADERS, m_index)
        End If
      End Set
    End Property

    Public Sub ClearRowWithOwnControl()
      m_rows_own_control.Clear()
    End Sub

    ' Class by the controls edition
    Public Class CLASS_CONTROL

      Private m_grid As uc_grid
      Private m_col As Integer
      Private m_control As Object
      Private m_control_type As ENUM_CONTROL_TYPE

      ' Init local grid and local col
      Public Sub New(ByVal Grid As uc_grid, ByVal ColIndex As Integer)
        m_grid = Grid
        m_col = ColIndex
      End Sub

      ' Returns the control of the current column
      Friend ReadOnly Property Control() As Control
        Get
          Return m_control
        End Get
      End Property

      ' Enum type of controls 
      Public Enum ENUM_CONTROL_TYPE
        CONTROL_TYPE_COMBO = 1
        CONTROL_TYPE_ENTRY_FIELD = 2
        CONTROL_TYPE_DATE_TIME = 3
        CONTROL_TYPE_CHECK_BOX = 4
      End Enum

      Public Property Type() As ENUM_CONTROL_TYPE

        'Returns the type of the current column
        Get
          Return m_control_type
        End Get

        ' Create one control by the current colum
        Set(ByVal Value As ENUM_CONTROL_TYPE)
          Dim ctrl As Control

          ' Create a control 
          Select Case Value
            Case CONTROL_TYPE_COMBO
              m_control = New GUI_Controls.uc_combo()

            Case CONTROL_TYPE_ENTRY_FIELD
              m_control = New GUI_Controls.uc_entry_field()

            Case CONTROL_TYPE_DATE_TIME
              m_control = New GUI_Controls.uc_date_picker()

            Case CONTROL_TYPE_CHECK_BOX
              m_control = New CheckBox()

            Case Else
              'Error
          End Select

          ' Hide the Control
          ctrl = m_control
          ctrl.Visible = False

          ' Init control type
          m_control_type = Value

          ' Add created control to the grid
          ' Without this sentence the control is not visibled
          Me.m_grid.Controls.AddRange(New Control() {m_control})

        End Set

      End Property

      'Returns the ComboBox control
      Public ReadOnly Property ComboBox() As uc_combo
        Get
          If m_control_type <> CONTROL_TYPE_COMBO Then
            Return Nothing
          Else
            Return m_control
          End If
        End Get
      End Property

      'Returns the entry field control
      Public ReadOnly Property EntryField() As uc_entry_field
        Get
          If m_control_type <> CONTROL_TYPE_ENTRY_FIELD Then
            Return Nothing
          Else
            Return m_control
          End If
        End Get
      End Property

      'Returns the DateTime control
      Public ReadOnly Property DateTime() As uc_date_picker
        Get
          If m_control_type <> CONTROL_TYPE_DATE_TIME Then
            Return Nothing
          Else
            Return m_control
          End If
        End Get
      End Property

      'Returns the CheckBox control
      Public ReadOnly Property UserCheckBox() As CheckBox
        Get
          If m_control_type <> CONTROL_TYPE_CHECK_BOX Then
            Return Nothing
          Else
            Return m_control
          End If
        End Get
      End Property

    End Class

    ' Returns the control data of the current colum
    Public ReadOnly Property EditionControl(Optional ByVal Row As Integer = -1) As CLASS_CONTROL
      Get
        If Row >= 0 And RowWithOwnControl(Row) > 0 Then
          m_control_data_aux = New CLASS_CONTROL(m_grid, m_index)
          m_control_data_aux.Type = RowWithOwnControl(Row)

          Return m_control_data_aux
        End If

        Return m_control_data
      End Get
    End Property

    Public WriteOnly Property ColDataType() As ENUM_COLDATATYPE
      Set(ByVal Value As ENUM_COLDATATYPE)
        Select Case Value
          Case ENUM_COLDATATYPE.FLEX_DT_EMPTY
            m_grid.flx_grid.set_ColDataType(Me.m_index, VSFlex8.DataTypeSettings.flexDTEmpty)
          Case ENUM_COLDATATYPE.FLEX_DT_NULL
            m_grid.flx_grid.set_ColDataType(Me.m_index, VSFlex8.DataTypeSettings.flexDTNull)
          Case ENUM_COLDATATYPE.FLEX_DT_SHORT
            m_grid.flx_grid.set_ColDataType(Me.m_index, VSFlex8.DataTypeSettings.flexDTShort)
          Case ENUM_COLDATATYPE.FLEX_DT_LONG
            m_grid.flx_grid.set_ColDataType(Me.m_index, VSFlex8.DataTypeSettings.flexDTLong)
          Case ENUM_COLDATATYPE.FLEX_DT_SINGLE
            m_grid.flx_grid.set_ColDataType(Me.m_index, VSFlex8.DataTypeSettings.flexDTSingle)
          Case ENUM_COLDATATYPE.FLEX_DT_DOUBLE
            m_grid.flx_grid.set_ColDataType(Me.m_index, VSFlex8.DataTypeSettings.flexDTDouble)
          Case ENUM_COLDATATYPE.FLEX_DT_CURRENCY
            m_grid.flx_grid.set_ColDataType(Me.m_index, VSFlex8.DataTypeSettings.flexDTCurrency)
          Case ENUM_COLDATATYPE.FLEX_DT_DATE
            m_grid.flx_grid.set_ColDataType(Me.m_index, VSFlex8.DataTypeSettings.flexDTDate)
          Case ENUM_COLDATATYPE.FLEX_DT_STRING
            m_grid.flx_grid.set_ColDataType(Me.m_index, VSFlex8.DataTypeSettings.flexDTString)
          Case ENUM_COLDATATYPE.FLEX_DT_DISPATCH
            m_grid.flx_grid.set_ColDataType(Me.m_index, VSFlex8.DataTypeSettings.flexDTDispatch)
          Case ENUM_COLDATATYPE.FLEX_DT_ERROR
            m_grid.flx_grid.set_ColDataType(Me.m_index, VSFlex8.DataTypeSettings.flexDTError)
          Case ENUM_COLDATATYPE.FLEX_DT_BOOLEAN
            m_grid.flx_grid.set_ColDataType(Me.m_index, VSFlex8.DataTypeSettings.flexDTBoolean)
          Case ENUM_COLDATATYPE.FLEX_DT_VARIANT
            m_grid.flx_grid.set_ColDataType(Me.m_index, VSFlex8.DataTypeSettings.flexDTVariant)
          Case ENUM_COLDATATYPE.FLEX_DT_UNKNOWN
            m_grid.flx_grid.set_ColDataType(Me.m_index, VSFlex8.DataTypeSettings.flexDTUnknown)
          Case ENUM_COLDATATYPE.FLEX_DT_DECIMAL
            m_grid.flx_grid.set_ColDataType(Me.m_index, VSFlex8.DataTypeSettings.flexDTDecimal)
          Case ENUM_COLDATATYPE.FLEX_DT_LONG8
            m_grid.flx_grid.set_ColDataType(Me.m_index, VSFlex8.DataTypeSettings.flexDTLong8)
          Case ENUM_COLDATATYPE.FLEX_DT_STRINGC
            m_grid.flx_grid.set_ColDataType(Me.m_index, VSFlex8.DataTypeSettings.flexDTStringC)
          Case ENUM_COLDATATYPE.FLEX_DT_STRINGW
            m_grid.flx_grid.set_ColDataType(Me.m_index, VSFlex8.DataTypeSettings.flexDTStringW)
        End Select
      End Set
    End Property

  End Class

  Public Class CLASS_GRID_SETTINGS
    Private m_grid As uc_grid

    Public Sub New(ByVal Grid As uc_grid)
      m_grid = Grid
    End Sub

    Public Property MergeCells() As Boolean
      Get
        If m_grid.flx_grid.MergeCells = VSFlex8.MergeSettings.flexMergeRestrictAll Then
          Return True
        Else
          Return False
        End If
      End Get
      Set(ByVal Value As Boolean)
        If Value Then
          m_grid.flx_grid.MergeCells = VSFlex8.MergeSettings.flexMergeRestrictAll
        Else
          m_grid.flx_grid.MergeCells = VSFlex8.MergeSettings.flexMergeNever
        End If
      End Set
    End Property

    Public Property ForeColor() As System.Drawing.Color
      Get
        Return m_grid.flx_grid.ForeColor
      End Get
      Set(ByVal Value As System.Drawing.Color)
        m_grid.flx_grid.ForeColor = Value
      End Set
    End Property

    Public Property BackColor() As System.Drawing.Color
      Get
        Return m_grid.flx_grid.BackColor
      End Get
      Set(ByVal Value As System.Drawing.Color)
        m_grid.flx_grid.BackColor = Value
      End Set
    End Property

    Public Property ForeColorSel() As System.Drawing.Color
      Get
        Return m_grid.flx_grid.ForeColorSel
      End Get
      Set(ByVal Value As System.Drawing.Color)
        m_grid.flx_grid.ForeColorSel = Value
      End Set
    End Property

    Public Property BackColorSel() As System.Drawing.Color
      Get
        Return m_grid.flx_grid.BackColorSel
      End Get
      Set(ByVal Value As System.Drawing.Color)
        m_grid.flx_grid.BackColorSel = Value
      End Set
    End Property

    Public Property MultipleSelectionAllowed() As Boolean
      Get
        Return m_grid.m_multiple_selection_allowed
      End Get
      Set(ByVal Value As Boolean)
        m_grid.m_multiple_selection_allowed = Value
      End Set
    End Property

  End Class

  Public Class CLASS_COUNTER
    Private m_grid As uc_grid
    Private m_label As Label

    Public Sub New(ByVal Grid As uc_grid, ByVal Index As Integer)
      m_grid = Grid
      Select Case Index
        Case 0
          m_label = Me.m_grid.lbl_count0
        Case 1
          m_label = Me.m_grid.lbl_count1
        Case 2
          m_label = Me.m_grid.lbl_count2
        Case 3
          m_label = Me.m_grid.lbl_count3
        Case 4
          m_label = Me.m_grid.lbl_count4
        Case 5
          m_label = Me.m_grid.lbl_count5
        Case 6
          m_label = Me.m_grid.lbl_count6
        Case 7
          m_label = Me.m_grid.lbl_count7

        Case Else
          m_label = Nothing
      End Select
    End Sub

    Public Property ForeColor() As System.Drawing.Color
      Get
        Return m_label.ForeColor
      End Get
      Set(ByVal Value As System.Drawing.Color)
        m_label.ForeColor = Value
      End Set
    End Property

    Public Property BackColor() As System.Drawing.Color
      Get
        Return m_label.BackColor
      End Get
      Set(ByVal Value As System.Drawing.Color)
        m_label.BackColor = Value
      End Set
    End Property

    Public Property Value() As Integer
      Get
        Return CInt(GUI_ParseNumber(m_label.Text))
      End Get
      Set(ByVal Value As Integer)
        ' JAB 07-JUN-2012 Resize fonts.
        uc_grid.SetFontSize(m_label, m_label.Text.Length, Value.ToString().Length)
        m_label.Text = Value
      End Set
    End Property

    Public Property Visible() As Boolean
      Get
        Return m_label.Visible
      End Get
      Set(ByVal Value As Boolean)
        m_label.Visible = Value
        If Value = True Then
          Call m_grid.PosBtnInfo()

        End If
      End Set
    End Property

  End Class

  ' Class by buttons (Add, Delete and Info)
  Public Class CLASS_BUTTON
    Private m_grid As uc_grid
    Private m_buttons As uc_button

    ' Enum type of buttons 
    Public Enum ENUM_BUTTON_TYPE
      BUTTON_TYPE_ADD = 1
      BUTTON_TYPE_DELETE = 2
      BUTTON_TYPE_INFO = 3
    End Enum

    ' Init local buttons
    Public Sub New(ByVal Grid As uc_grid, ByVal Index As Integer)
      m_grid = Grid
      Select Case Index
        Case BUTTON_TYPE_ADD
          m_buttons = Me.m_grid.btn_add_row

        Case BUTTON_TYPE_DELETE
          m_buttons = Me.m_grid.btn_delete_row

        Case BUTTON_TYPE_INFO
          m_buttons = Me.m_grid.btn_info
          m_grid.btn_info.Enabled = False

        Case Else
          m_buttons = Nothing
      End Select
      '' Add created control to the grid
      '' Without this sentence the control is not visibled
      'Me.m_grid.Controls.AddRange(New uc_button() {m_buttons})

    End Sub

    ' Returns an sets the button title
    Public Property Value() As Integer
      Get
        Return CInt(GUI_ParseNumber(m_buttons.Text))
      End Get
      Set(ByVal Value As Integer)
        m_buttons.Text = Value
      End Set
    End Property

    ' Returns an sets the visible status
    Public Property Visible() As Boolean
      Get
        Return m_buttons.Visible
      End Get
      Set(ByVal Value As Boolean)
        m_buttons.Visible = Value
      End Set
    End Property

    ' Returns an sets the enabled status
    Public Property Enabled() As Boolean
      Get
        Return m_buttons.Enabled
      End Get
      Set(ByVal Value As Boolean)
        m_buttons.Enabled = Value
      End Set
    End Property

  End Class

#End Region

#Region " Public Funcions "

  Public Sub SetEditableCellBorderSize(ByVal Left As Int16, ByVal Top As Int16, _
                                       ByVal Right As Int16, ByVal Bottom As Int16, _
                                       ByVal Vertical As Int16, ByVal Horizontal As Int16)

    m_editable_cell_border_size.m_left = Left
    m_editable_cell_border_size.m_top = Top
    m_editable_cell_border_size.m_right = Right
    m_editable_cell_border_size.m_bottom = Bottom

  End Sub


  Public Sub Init(ByVal Cols As Integer, _
                  Optional ByVal Headers As Integer = 1, _
                  Optional ByVal Editable As Boolean = False)

    Dim idx As Integer
    Dim header As Integer

    Me.EditableCellShowMode = GRID_SHOW_EDIT_MODE.SHOW_BORDER
    Me.EditableCellBackColor = System.Drawing.Color.FromArgb(189, 236, 182)    ' "Verde Pastel"
    Me.EditableCellBorderColor = System.Drawing.Color.FromArgb(189, 236, 182)  ' "Verde Pastel"
    Me.SetEditableCellBorderSize(1, 1, 1, 1, 1, 1)

    Me.flx_grid.Rows = MAX_HEADERS + 1
    Me.flx_grid.Cols = Cols + 1
    Me.flx_grid.FixedCols = 0
    Me.flx_grid.FixedRows = MAX_HEADERS
    Me.flx_grid.Rows = MAX_HEADERS
    Me.flx_grid.Cols = Cols
    If Editable = True Then
      Me.flx_grid.SelectionMode = VSFlex8.SelModeSettings.flexSelectionFree
      Me.flx_grid.FocusRect = VSFlex8.FocusRectSettings.flexFocusLight
    Else
      Me.flx_grid.SelectionMode = VSFlex8.SelModeSettings.flexSelectionByRow
      Me.flx_grid.FocusRect = VSFlex8.FocusRectSettings.flexFocusNone
    End If

    'Debug.WriteLineIf(Trace, Me.Name & ": " & "Init" & " Redraw: " & Me.flx_grid.Redraw & " Semaphores:" & m_semaphore & "," & m_semaphore1)

    Me.m_init = True

    Me.flx_grid.MergeCells = VSFlex8.MergeSettings.flexMergeFree

    For idx = 0 To MAX_HEADERS
      If idx >= Headers + 1 Then
        Me.flx_grid.set_RowHeight(idx - 1, 0)
      Else
        Me.flx_grid.set_RowHeight(idx - 1, DEFAULT_HEADER_HEIGHT)
      End If
    Next

    ' ''For idx = Headers + 1 To MAX_HEADERS
    ' ''  Me.flx_grid.set_RowHeight(idx - 1, 0)
    ' ''Next

    ReDim m_col_data(Cols)

    For idx = 0 To Cols - 1
      m_col_data(idx) = New CLASS_COL_DATA(Me, idx)
      For header = 0 To MAX_HEADERS - 1
        Me.flx_grid.set_MergeRow(header, True)
        m_col_data(idx).Header(header).Text = "H" & idx & " / " & header
        'TODO Header SCM
        m_col_data(idx).Header(header).Alignment = CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
        m_col_data(idx).Editable = False
      Next
    Next

    Call Me.Clear()

    ' Set color
    Me.lbl_count0.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
    Me.lbl_count0.ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

    btn_add_row.Text = GLB_NLS_GUI_CONTROLS.GetString(12)
    btn_delete_row.Text = GLB_NLS_GUI_CONTROLS.GetString(11)
    ' Init btn_info
    Call PosBtnInfo()

    m_editable = Editable
    Me.m_init_fi = True

    ' tooltip parameters
    Me.tool_tip.InitialDelay = 1000
    Me.tool_tip.ReshowDelay = 1000

  End Sub

  ' PURPOSE: Return "" when value is Nothing (old behavior of get_texMatrix property).
  '
  '  PARAMS:
  '     - INPUT:
  '           - Row: number of record
  '           - Col: number of column
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - _value: content of cell
  '
  Public Function GetTextMatrix(ByVal Row As Integer, ByVal Col As Integer) As String
    Dim _value As String

    _value = Me.flx_grid.get_TextMatrix(Row, Col)
    If _value Is Nothing Then
      _value = ""
    End If

    Return _value
  End Function ' GetTextMatrix

  'Oc 29/04/04- this function renames the grid buttons to +,- buttons
  Public Sub RenameButtons(ByVal BtnAddTxt As String, ByVal BtnDeleteTxt As String)
    btn_add_row.Text = BtnAddTxt
    btn_delete_row.Text = BtnDeleteTxt
  End Sub

  'Oc 29/04/04- this function collapse/expands all rows between HeaderIndexRow and LastIndexRow
  Public Sub CollapseExpand(ByVal HeaderIndexRow As Integer, _
                            ByVal LastIndexRow As Integer, _
                            Optional ByVal ColumnForSignal As Integer = -1, _
                            Optional ByVal ActionExpand As ENUM_GRID_EXPAND_COLLAPSE = ENUM_GRID_EXPAND_COLLAPSE.GRID_EXPAND_COLLAPSE_NOTHING)

    Dim idx_row As Integer
    Dim first_row As Integer
    Dim header_row_height As Integer
    Dim first_row_height As Integer
    Dim redraw_status As Boolean
    Dim cell_value As String
    Dim cell_value_no_signal As String
    Dim is_now_expanded As Boolean
    Dim current_signal As String
    Dim new_signal As String
    Dim row_height_to_set As Integer
    Dim signal_exists As Boolean
    Dim col_index As Integer

    redraw_status = Me.Redraw
    Me.Redraw = False

    header_row_height = Me.flx_grid.get_RowHeight(HeaderIndexRow + flx_grid.FixedRows)

    ' GZ 07-DEC-2004 - if header height is zero, set it to default
    If header_row_height <= 0 Then
      Me.flx_grid.set_RowHeight(HeaderIndexRow + flx_grid.FixedRows, DEFAULT_HEADER_HEIGHT)
      For col_index = 1 To Me.flx_grid.Cols
        If Me.Column(col_index) Is Nothing Then
          'DO nothing
        Else
          If Me.Column(col_index).EditionControl Is Nothing Then
            'DO nothing
          Else
            If Me.Column(col_index).EditionControl.Type = CONTROL_TYPE_CHECK_BOX Then
              Call Me.PaintCheckBox(HeaderIndexRow + flx_grid.FixedRows, col_index)
            End If
          End If
        End If

      Next

    End If

    first_row = HeaderIndexRow + 1
    If (LastIndexRow <= HeaderIndexRow) Then
      'bad interval was sent
      Exit Sub
    End If

    Select Case ActionExpand
      Case ENUM_GRID_EXPAND_COLLAPSE.GRID_EXPAND_ALL
        is_now_expanded = False
      Case ENUM_GRID_EXPAND_COLLAPSE.GRID_COLLAPSE_ALL
        is_now_expanded = True
      Case Else
        first_row_height = Me.flx_grid.get_RowHeight(first_row + flx_grid.FixedRows)
        is_now_expanded = first_row_height <> 0
    End Select

    If is_now_expanded Then
      current_signal = ALLOWS_COLLAPSE_SYMBOL
      new_signal = ALLOWS_EXPAND_SYMBOL
      row_height_to_set = 0
    Else
      current_signal = ALLOWS_EXPAND_SYMBOL
      new_signal = ALLOWS_COLLAPSE_SYMBOL
      row_height_to_set = header_row_height
    End If

    For idx_row = first_row To LastIndexRow
      Me.flx_grid.set_RowHeight(idx_row + flx_grid.FixedRows, row_height_to_set)
      For col_index = 1 To Me.flx_grid.Cols
        If Me.Column(col_index) Is Nothing Then
          'DO nothing
        Else
          If Me.Column(col_index).EditionControl Is Nothing Then
            'DO nothing
          Else
            If Me.Column(col_index).EditionControl.Type = CONTROL_TYPE_CHECK_BOX Then
              Call Me.PaintCheckBox(idx_row + flx_grid.FixedRows, col_index)
            End If
          End If
        End If

      Next

    Next

    ' LRS 18-05-2004, Set the signal (+)/(-) for the column specified
    If ColumnForSignal <> -1 Then
      cell_value = Me.Cell(HeaderIndexRow, ColumnForSignal).Value

      If ActionExpand = ENUM_GRID_EXPAND_COLLAPSE.GRID_EXPAND_COLLAPSE_NOTHING Then
        signal_exists = IIf(Mid(cell_value, 1, Len(current_signal)) = current_signal, True, False)
      Else
        If Mid(cell_value, 1, Len(ALLOWS_EXPAND_SYMBOL)) = ALLOWS_EXPAND_SYMBOL Then
          signal_exists = True
          current_signal = ALLOWS_EXPAND_SYMBOL
        ElseIf Mid(cell_value, 1, Len(ALLOWS_COLLAPSE_SYMBOL)) = ALLOWS_COLLAPSE_SYMBOL Then
          signal_exists = True
          current_signal = ALLOWS_COLLAPSE_SYMBOL
        Else
          signal_exists = False
        End If

        If ActionExpand = ENUM_GRID_EXPAND_COLLAPSE.GRID_EXPAND_ALL Then
          new_signal = ALLOWS_COLLAPSE_SYMBOL
        Else
          new_signal = ALLOWS_EXPAND_SYMBOL
        End If
      End If

      cell_value_no_signal = IIf(signal_exists, _
                                 Mid(cell_value, Len(current_signal) + 1), _
                                 cell_value)
      Me.Cell(HeaderIndexRow, ColumnForSignal).Value = new_signal & cell_value_no_signal
    End If

    Me.Redraw = redraw_status

  End Sub

  Public Sub Clear()
    Dim idx As Integer

    If btn_info.Visible = True Then
      btn_info.Enabled = False
    End If

    'Debug.WriteLineIf(Trace, Me.Name & ": " & "Clear" & " Redraw: " & Me.flx_grid.Redraw & " Semaphores:" & m_semaphore & "," & m_semaphore1)

    Me.flx_grid.Rows = MAX_HEADERS
    m_rows = 0
    m_col_type_set = False

    Erase Me.m_row_data

    For idx = 0 To Me.NumColumns - 1
      m_col_data(idx).ClearRowWithOwnControl()
    Next


    Me.lbl_count0.Text = CStr(m_rows)
    Me.lbl_count1.Text = CStr(m_rows)
    Me.lbl_count2.Text = CStr(m_rows)
    Me.lbl_count3.Text = CStr(m_rows)
    Me.lbl_count4.Text = CStr(m_rows)
    Me.lbl_count5.Text = CStr(m_rows)
    Me.lbl_count6.Text = CStr(m_rows)
    Me.lbl_count7.Text = CStr(m_rows)

    ' JAB 07-JUN-2012: Resize fonts.
    SetFontSize(lbl_count0, 0, 1)
    SetFontSize(lbl_count1, 0, 1)
    SetFontSize(lbl_count2, 0, 1)
    SetFontSize(lbl_count3, 0, 1)
    SetFontSize(lbl_count4, 0, 1)
    SetFontSize(lbl_count5, 0, 1)
    SetFontSize(lbl_count6, 0, 1)
    SetFontSize(lbl_count7, 0, 1)

    Call XRefresh()
  End Sub

  Public Function AddRow() As Integer
    Dim idx_col As Integer

    If btn_info.Visible = True And btn_info.Enabled = False Then
      btn_info.Enabled = True
    End If

    Call HideEditionControl(Me.flx_grid.Row, Me.flx_grid.Col)

    ' Add the row data
    m_rows = m_rows + 1
    ReDim Preserve m_row_data(m_rows)

    m_row_data(m_rows - 1) = New CLASS_ROW_DATA(Me, m_rows - 1)
    ' Add the row to the flex grid
    Call Me.flx_grid.AddItem("")
    Me.flx_grid.set_RowData(Me.flx_grid.Rows - 1, m_rows - 1)
    If Me.flx_grid.Redraw Then
      ' JAB 07-JUN-2012 Resize fonts.
      SetFontSize(lbl_count0, Me.lbl_count0.Text.Length, CStr(Me.NumRows).Length)
      Me.lbl_count0.Text = CStr(Me.NumRows)
    End If

    ' Search in the row the column type is GRID_COL_TYPE_CHECK
    For idx_col = 0 To Me.flx_grid.Cols - 1
      Select Case m_col_data(idx_col).EditionControl.Type()
        Case CONTROL_TYPE_CHECK_BOX
          Call PaintCheckBox(Me.flx_grid.Rows - 1, idx_col)

        Case CONTROL_TYPE_ENTRY_FIELD, CONTROL_TYPE_COMBO, CONTROL_TYPE_DATE_TIME
          Call PaintEditMark(Me.flx_grid.Rows - 1, idx_col)

        Case Else

      End Select
    Next

    Return Me.NumRows - 1
  End Function

  Public Sub PaintRow(ByVal RowIndex As Integer)

    m_semaphore = m_semaphore + 1
    m_semaphore1 = m_semaphore1 + 1

    Call RowUnselect(RowIndex)

    m_semaphore1 = m_semaphore1 - 1
    m_semaphore = m_semaphore - 1
  End Sub

  Public Function IsVisible(ByVal IndexRow As Integer) As Boolean
    Return Me.flx_grid.get_RowIsVisible(IndexRow + MAX_HEADERS)
  End Function

  Public Sub DeleteRowFast(ByVal RowIndex As Integer)
    'XVV Variable not use Dim idx_row As Integer
    Dim status As Boolean

    Call HideEditionControl(Me.flx_grid.Row, Me.flx_grid.Col)


    If Me.NumRows = 1 And RowIndex = 0 Then
      status = Me.Redraw
      Call Me.Clear()
      Me.Redraw = status

      Exit Sub
    End If

    m_row_data(RowDataIndex(RowIndex)).IsEmpty = True
    Me.flx_grid.RemoveItem(RowIndex + MAX_HEADERS)
    If Me.flx_grid.Redraw Then
      ' JAB 07-JUN-2012 Resize fonts.
      SetFontSize(lbl_count0, Me.lbl_count0.Text.Length, CStr(Me.NumRows).Length)
      Me.lbl_count0.Text = CStr(Me.NumRows)
    End If


  End Sub

  Public Sub DeleteRow(ByVal RowIndex As Integer)

    Call DeleteRowFast(RowIndex)

    If Me.NumRows = 0 Then
      Exit Sub
    End If

    If Me.NumRows = RowIndex Then
      RowIndex = RowIndex - 1
    End If

    Me.CurrentRow = RowIndex
    Me.IsSelected(RowIndex) = True

  End Sub

  Public Function SelectedRows() As Integer()

    'XVV 16/04/2007
    'Assign "" to SQL Trans because compiler generate warning for not have value
    Dim selected_rows() As Integer = Nothing

    Dim idx_row As Integer
    Dim num_items As Integer

    num_items = 0
    For idx_row = 0 To Me.NumRows - 1
      If Me.Row(idx_row).IsSelected Then
        ReDim Preserve selected_rows(num_items)
        selected_rows(num_items) = idx_row
        num_items = num_items + 1
      End If
    Next

    Return selected_rows

  End Function

  Public Function InsertRow(ByVal RowIndex As Integer) As Integer
    Dim idx_col As Integer

    m_rows = m_rows + 1
    ReDim Preserve m_row_data(m_rows)

    m_row_data(m_rows - 1) = New CLASS_ROW_DATA(Me, m_rows - 1)

    Me.flx_grid.AddItem("", RowIndex + MAX_HEADERS)
    Me.flx_grid.set_RowData(RowIndex + MAX_HEADERS, m_rows - 1)
    If Me.flx_grid.Redraw Then
      ' JAB 07-JUN-2012 Resize fonts.
      SetFontSize(lbl_count0, Me.lbl_count0.Text.Length, CStr(Me.NumRows).Length)
      Me.lbl_count0.Text = CStr(Me.NumRows)
    End If

    ' Search in the row the column type is GRID_COL_TYPE_CHECK
    For idx_col = 0 To Me.flx_grid.Cols - 1
      Select Case m_col_data(idx_col).EditionControl.Type()

        Case CONTROL_TYPE_CHECK_BOX
          ' RXM 21-JAN-2013 paint correct index
          Call PaintCheckBox(RowIndex + MAX_HEADERS, idx_col)
        Case Else
          '
      End Select
    Next

    Return RowIndex

  End Function

  ' Handle the pressed key 
  Public Function KeyPressed(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean
    Dim current_control As Object
    'XVV Variable not use Dim current_text As String

    Select Case e.KeyChar

      ' Enter key
      Case Chr(Keys.Enter)
        If Not Me.m_editable Then
          Return False
        End If

        If Not m_col_data(CurrentCol).Editable Then
          Return True
        End If
        current_control = m_col_data(CurrentCol).EditionControl.Control()

        If current_control.ContainsFocus Then
          Call HideEditionControl(Me.flx_grid.Row, Me.flx_grid.Col)
          Return True
        Else
          Call ShowEditionControl(Me.flx_grid.Row, Me.flx_grid.Col)
          Return True
        End If

        ' Escape key
      Case Chr(Keys.Escape)
        ' Set current control
        If Not m_col_data(CurrentCol).Editable Then
          Return False
        End If
        current_control = m_col_data(CurrentCol).EditionControl.Control()

        If current_control.ContainsFocus Then
          ' Set before value
          Call HideEditionControl(Me.flx_grid.Row, Me.flx_grid.Col, True)
          Return True
        Else
          Return False
        End If

      Case Else
        Return True

    End Select

    Return False

  End Function

  'IRP To insert image
  Public Sub InsertImage(ByVal NumRow As Integer, ByVal NumCol As Integer, ByVal Image As System.Drawing.Image)
    ' Set current cell
    m_semaphore = m_semaphore + 1
    Me.flx_grid.Row = NumRow
    Me.flx_grid.Col = NumCol
    m_semaphore = m_semaphore - 1

    Me.flx_grid.CellPictureAlignment = VSFlex8.AlignmentSettings.flexAlignCenterCenter
    Me.flx_grid.CellPicture = Image

  End Sub

  Public Sub SelectFirstRow(ByVal SelectOption As Boolean)
    Dim redraw_state As Boolean

    redraw_state = Me.Redraw
    Me.Redraw = False

    If Me.NumRows > 0 Then
      If SelectOption Then
        IsSelected(0) = True
      Else
        'm_test = False
        IsSelected(0) = False
      End If
    End If

    Me.Redraw = redraw_state

  End Sub

  Public Sub ColumnFit(ByVal ColIndex As Integer)
    Dim idx As Integer
    Dim size As Integer

    size = 0
    For idx = 0 To flx_grid.Cols - 1
      If idx <> ColIndex Then
        size = size + flx_grid.get_ColWidth(idx)
      End If
    Next

    ' 12.619402985074627

    size = flx_grid.Width * 8.5074 - size - 300
    If size > 0 Then
      flx_grid.set_ColWidth(ColIndex, size)
    End If

  End Sub

  Public Sub SortGrid(Optional ByVal SortByCol As Integer = -1)

    If SortByCol > -1 Then
      GLB_SortColumn = SortByCol
    End If

    Call SortRoutine()

  End Sub

  Public Sub ClearSelection()
    Dim _idx As Integer

    For _idx = 0 To Me.NumRows - 1
      If IsSelected(_idx) Then
        IsSelected(_idx) = False
      End If
    Next

  End Sub

  Public Sub SetFont(ByVal Row As Integer, ByVal Col As Integer, ByVal Font As Drawing.Font)
    Me.flx_grid.Row = Row + MAX_HEADERS
    Me.flx_grid.Col = Col

    Me.flx_grid.CellFontName = Font.Name
    Me.flx_grid.CellFontSize = Font.Size
  End Sub

#End Region

#Region " Public Properties "

  Public Property EditableCellShowMode() As GRID_SHOW_EDIT_MODE
    Get
      Return Me.m_editable_cell_show_mode
    End Get
    Set(ByVal Value As GRID_SHOW_EDIT_MODE)
      Me.m_editable_cell_show_mode = Value
    End Set
  End Property

  Public Property EditableCellBorderColor() As System.Drawing.Color
    Get
      Return Me.m_editable_cell_border_color
    End Get
    Set(ByVal Value As System.Drawing.Color)
      Me.m_editable_cell_border_color = Value
    End Set
  End Property

  Public Property EditableCellBackColor() As System.Drawing.Color
    Get
      Return Me.m_editable_cell_back_color
    End Get
    Set(ByVal Value As System.Drawing.Color)
      Me.m_editable_cell_back_color = Value
    End Set
  End Property

  Public Property EditableCellBorderSize() As TYPE_CELL_BORDER
    Get
      Return Me.m_editable_cell_border_size
    End Get
    Set(ByVal Value As TYPE_CELL_BORDER)
      Me.m_editable_cell_border_size = Value
    End Set
  End Property

  Public ReadOnly Property Row(ByVal Index As Integer) As CLASS_ROW_DATA
    Get
      Return m_row_data(RowDataIndex(Index))
    End Get
  End Property

  Public ReadOnly Property Cell(ByVal Row As Integer, ByVal Column As Integer) As CLASS_CELL_DATA
    Get
      Return New CLASS_CELL_DATA(Me, Row, Column)
    End Get
  End Property

  Public ReadOnly Property Column(ByVal Index As Integer) As CLASS_COL_DATA
    Get
      Return m_col_data(Index)
    End Get
  End Property

  Public ReadOnly Property Settings() As CLASS_GRID_SETTINGS
    Get
      Return New CLASS_GRID_SETTINGS(Me)
    End Get
  End Property

  Public ReadOnly Property Counter(ByVal Index As Integer) As CLASS_COUNTER
    Get
      Return New CLASS_COUNTER(Me, Index)
    End Get
  End Property

  Public Property PanelRightVisible() As Boolean
    Get
      Return Me.panel_right.Visible
    End Get
    Set(ByVal Value As Boolean)
      Me.panel_right.Visible = Value
    End Set
  End Property

  ' Returns the control data of the current colum
  Public ReadOnly Property Button(ByVal ButtonType As ENUM_BUTTON_TYPE) As CLASS_BUTTON
    Get
      Return New CLASS_BUTTON(Me, ButtonType)
    End Get
  End Property

  Public Property Redraw() As Boolean
    Get
      Return Me.flx_grid.Redraw
    End Get
    Set(ByVal Value As Boolean)
      Me.flx_grid.Redraw = Value
      If Value Then
        ' JAB 07-JUN-2012 Resize fonts.
        SetFontSize(lbl_count0, Me.lbl_count0.Text.Length, CStr(Me.NumRows).Length)
        Me.lbl_count0.Text = CStr(Me.NumRows)
      End If
    End Set
  End Property

  Public ReadOnly Property NumColumns() As Integer
    Get
      If m_init Then
        Return Me.flx_grid.Cols
      Else
        Return -1
      End If
    End Get
  End Property

  Public ReadOnly Property NumRows() As Integer
    Get
      If m_init Then
        Return Me.flx_grid.Rows - MAX_HEADERS
      Else
        Return -1
      End If
    End Get
  End Property

  Public Property Value(ByVal Row As Integer, ByVal Col As Integer) As String
    Get

      Return GetTextMatrix(MAX_HEADERS + Row, Col)

    End Get
    Set(ByVal NewValue As String)
      Me.flx_grid.set_TextMatrix(MAX_HEADERS + Row, _
                                 Col, _
                                 NewValue)
      If m_col_data(Col).EditionControl(Row).Type() = CONTROL_TYPE_CHECK_BOX Then
        Call PaintCheckBox(MAX_HEADERS + Row, Col)
      End If

    End Set
  End Property

  Public Property CurrentRow() As Integer
    Get
      If m_init Then
        Return flx_grid.Row - MAX_HEADERS
      Else
        Return -1
      End If
    End Get

    Set(ByVal Value As Integer)
      If m_init Then
        flx_grid.Row = Value + MAX_HEADERS
      End If
    End Set
  End Property

  Public Property CurrentCol() As Integer
    Get
      If m_init Then
        Return flx_grid.Col
      Else
        Return -1
      End If
    End Get
    Set(ByVal Value As Integer)
      If m_init Then
        flx_grid.Col = Value
      End If
    End Set
  End Property

  Public Property IsSelected(ByVal Row As Integer) As Boolean
    Get
      If m_row_data Is Nothing Then
        Return False
      End If
      Return m_row_data(RowDataIndex(Row)).IsSelected
    End Get

    Set(ByVal Value As Boolean)

      If m_row_data Is Nothing Then
        Return
      End If

      'XVV Variable not use Dim idx As Integer
      m_row_data(RowDataIndex(Row)).IsSelected = Value

      m_semaphore1 = m_semaphore1 + 1
      If Value = True Then
        RowSelect(Row)
      Else
        RowUnselect(Row)
      End If
      m_semaphore1 = m_semaphore1 - 1

      'If m_multiple_selection_allowed = False Then
      '  If Value = True Then
      '    For idx = 0 To Me.NumRows - 1
      '      If idx <> Row Then
      '        If Me.Row(idx).IsSelected Then
      '          Me.Row(idx).IsSelected = False
      '        End If
      '      End If
      '    Next
      '  End If
      'End If

    End Set
  End Property

  'Public ReadOnly Property Picture() As Image
  '  Get
  '    Return Me.flx_grid.Picture
  '  End Get
  'End Property
  Public Property TopRow() As Integer
    Get
      ' The TopRow is the 1st not fixed.
      Return Me.flx_grid.TopRow - MAX_HEADERS
    End Get
    Set(ByVal Value As Integer)
      Me.flx_grid.TopRow = Value + MAX_HEADERS
    End Set
  End Property


  Public ReadOnly Property BottomRow() As Integer
    Get
      ' The TopRow is the 1st not fixed.
      Return Me.flx_grid.BottomRow - MAX_HEADERS
    End Get
  End Property


  Public WriteOnly Property IsSortable() As Boolean
    Set(ByVal Value As Boolean)
      GLB_IsSortable = Value

      ' JAB 05-JUN-2012: Set sort property in the new component (vsFlexGrid8)
      If Value Then
        flx_grid.ExplorerBar = VSFlex8.ExplorerBarSettings.flexExSort
      Else
        flx_grid.ExplorerBar = VSFlex8.ExplorerBarSettings.flexExNone
      End If

    End Set
  End Property

  ' LRS 07-APR-2004, Returns if the form which contains the grid inherits from frm_base_sel
  Public ReadOnly Property ParentFormIsSelectionType() As Boolean
    Get

      Dim parent_form As frm_base

      Try
        parent_form = Me.ParentForm
      Catch

        Return False
      End Try

      If Not Me.DesignMode Then

        Return parent_form.GUI_GetScreenType = frm_base.ENUM_SCREEN_TYPE.MODE_SELECT
      End If
    End Get
  End Property

  Public Property Sortable() As Boolean
    Get

      'XVV 17/04/2007
      'add compile IF, because this sentence produce exception in Form Design
      If Not Me.DesignMode Then
        ' LRS 07-APR-2004, It cannot be sortable if the grid is not contained into a selection screen
        Return ParentFormIsSelectionType And GLB_IsSortable
      End If

    End Get
    Set(ByVal Value As Boolean)
      GLB_IsSortable = Value

      ' JAB 05-JUN-2012: Set sort property in the new component (vsFlexGrid8) 
      If Value Then
        flx_grid.ExplorerBar = VSFlex8.ExplorerBarSettings.flexExSort
      Else
        flx_grid.ExplorerBar = VSFlex8.ExplorerBarSettings.flexExNone
      End If

    End Set
  End Property

  Public WriteOnly Property IsToolTipped() As Boolean
    Set(ByVal Value As Boolean)
      GLB_IsToolTipped = Value
    End Set
  End Property

  Public Property ToolTipped() As Boolean
    Get
      Return GLB_IsToolTipped
    End Get
    Set(ByVal Value As Boolean)
      GLB_IsToolTipped = Value
    End Set
  End Property

  Public WriteOnly Property KeepSelection() As Boolean
    Set(ByVal Value As Boolean)
      m_keep_selection = Value
    End Set
  End Property

  Public Property SortAscending() As Boolean
    Get
      Return GLB_SortAscending
    End Get
    Set(ByVal Value As Boolean)
      GLB_SortAscending = Value

      ' JAB 05-JUN-2012: Set sort property in the new component (vsFlexGrid8)
      If Value Then
        flx_grid.Sort = VSFlex8.SortSettings.flexSortGenericAscending
      Else
        flx_grid.Sort = VSFlex8.SortSettings.flexSortGenericDescending
      End If

    End Set
  End Property

  Public Property SortByCol() As Integer
    Get
      Return GLB_SortColumn
    End Get
    Set(ByVal Value As Integer)
      GLB_SortColumn = Value
    End Set
  End Property

  ' JAB 27-JUN-2012: uc_grid selection mode
  Public Property SelectionMode() As SELECTION_MODE
    Get
      Return m_selection_mode
    End Get
    Set(ByVal value As SELECTION_MODE)
      m_selection_mode = value
    End Set
  End Property

  ' JML 2-JUN-2016: uc_grid selection mode
  Public Property WordWrap() As Boolean
    Get
      Return flx_grid.WordWrap
    End Get
    Set(ByVal value As Boolean)
      flx_grid.WordWrap = value
    End Set
  End Property

  Public Property NoCountRowsWithSmallHeight As Boolean
    Get
      Return m_nocount_rows_with_small_height
    End Get
    Set(value As Boolean)
      m_nocount_rows_with_small_height = value
    End Set
  End Property

#End Region

#Region " Public Events "
  Protected Overrides Sub OnLostFocus(ByVal e As System.EventArgs)
    Me.tmr_mouse_move.Stop()
    Me.tool_tip.SetToolTip(flx_grid, "")
    MyBase.OnLostFocus(e)

  End Sub

#End Region

#Region " Private Functions"

  Private Function RowDataIndex(ByVal Index As Integer) As Integer

    Return Me.flx_grid.get_RowData(MAX_HEADERS + Index)

  End Function

  Private Sub RowSelect(ByVal RowIndex As Integer)
    Dim curr_row As Integer
    Dim curr_col As Integer
    Dim idx As Integer
    Dim row_data As CLASS_ROW_DATA
    Dim fore_color As System.Drawing.Color
    Dim back_color As System.Drawing.Color

    If RowIndex < 0 Or RowIndex >= Me.NumRows Then
      Return
    End If

    curr_row = flx_grid.Row
    curr_col = flx_grid.Col

    flx_grid.Row = MAX_HEADERS + RowIndex

    row_data = Me.Row(RowIndex)

    For idx = 0 To flx_grid.Cols - 1
      flx_grid.Col = idx
      If (Me.Column(idx).HighLightWhenSelected) Then
        flx_grid.CellBackColor = row_data.BackColorSel
        flx_grid.CellForeColor = row_data.ForeColorSel
      Else
        ' LRS 19-03-2004, When no HighLight, Repaint its own color..
        Call row_data.GetCellColor(idx, fore_color, back_color)
        flx_grid.CellBackColor = back_color
        flx_grid.CellForeColor = fore_color
      End If
    Next

    flx_grid.Row = curr_row
    flx_grid.Col = curr_col

  End Sub

  Private Sub RowUnselect(ByVal RowIndex As Integer)
    Dim curr_row As Integer
    Dim curr_col As Integer
    Dim fore_color As System.Drawing.Color
    Dim back_color As System.Drawing.Color
    Dim row_data As CLASS_ROW_DATA

    Dim idx As Integer

    If RowIndex < 0 Or RowIndex >= Me.NumRows Then
      Return
    End If

    curr_row = flx_grid.Row
    curr_col = flx_grid.Col


    row_data = Me.Row(RowIndex)
    flx_grid.Row = MAX_HEADERS + RowIndex


    For idx = 0 To flx_grid.Cols - 1
      flx_grid.Col = idx
      Call row_data.GetCellColor(idx, fore_color, back_color)

      If (m_col_data(idx).EditionControl.Type() = CONTROL_TYPE_ENTRY_FIELD Or _
         m_col_data(idx).EditionControl.Type() = CONTROL_TYPE_COMBO Or _
         m_col_data(idx).EditionControl.Type() = CONTROL_TYPE_DATE_TIME) And _
         EditableCellShowMode = GRID_SHOW_EDIT_MODE.SHOW_BACKCOLOR Then
        flx_grid.CellBackColor = EditableCellBackColor

      Else
        flx_grid.CellBackColor = back_color

      End If

      flx_grid.CellForeColor = fore_color

    Next

    flx_grid.Row = curr_row
    flx_grid.Col = curr_col

  End Sub

  Private Sub PerformSelection()
    Dim idx As Integer
    Dim kk As Integer

    If m_semaphore1 > 0 Then
      Exit Sub
    End If

    If Me.CurrentRow < 0 Then
      Exit Sub
    End If

    If Me.NumRows = 0 Then
      Exit Sub
    End If

    If flx_grid.MouseRow < MAX_HEADERS Then
      Exit Sub
    End If

    'Debug.WriteLineIf(Trace, Me.Name & ": " & "PerformSelection")

    m_semaphore1 = m_semaphore1 + 1
    m_semaphore = m_semaphore + 1

    flx_grid.Redraw = False

    'kk = Shift And 2

    If m_keep_selection = False Then
      For idx = 0 To Me.NumRows - 1
        If idx <> Me.CurrentRow Then
          If IsSelected(idx) Then
            IsSelected(idx) = False
          End If
        End If
      Next
    End If

    If IsSelected(Me.CurrentRow) Then
      IsSelected(Me.CurrentRow) = False
    Else
      IsSelected(Me.CurrentRow) = True
    End If

    If Not (m_keep_selection) Then
      kk = 0
      For idx = 0 To Me.NumRows - 1
        If IsSelected(idx) Then
          kk = kk + 1
        End If
      Next
      If kk = 0 Then
        IsSelected(Me.CurrentRow) = True
      End If
    End If


    flx_grid.Redraw = True
    m_semaphore = m_semaphore - 1
    m_semaphore1 = m_semaphore1 - 1

  End Sub

  Private Sub XRefresh()
    'Debug.WriteLineIf(Trace, Me.Name & ": " & "XRefresh" & " Redraw: " & Me.flx_grid.Redraw & " Semaphores:" & m_semaphore & "," & m_semaphore1)

    m_semaphore = 0
    m_semaphore1 = 0
    Me.flx_grid.Redraw = True
    Call Me.flx_grid.Refresh()
    Me.lbl_count0.Refresh()
    Me.lbl_count1.Refresh()
    Me.lbl_count2.Refresh()
    Me.lbl_count3.Refresh()
    Me.lbl_count4.Refresh()
    Me.lbl_count5.Refresh()
    Me.lbl_count6.Refresh()
    Me.lbl_count7.Refresh()
  End Sub

  'XVV 17/04/2007
  'Change Function for Procedure because this function don't return value. And generate compile warnings
  'Private Function ShowEditionControl(ByVal FlexRow As Integer, ByVal FlexCol As Integer)

  ' Shows the control in the current cell
  Private Sub ShowEditionControl(ByVal FlexRow As Integer, ByVal FlexCol As Integer)
    'XVV Variable not use Dim current_text As String
    Dim current_control As Object
    Dim twips_to_pixel As Double
    Dim idx_aux As Integer
    Dim format_date As ENUM_FORMAT_DATE
    Dim format_time As ENUM_FORMAT_DATE
    Dim edition_canceled As Boolean
    Dim kk As Integer

    If FlexRow < MAX_HEADERS Then
      Exit Sub
    End If

    If Not m_col_data(FlexCol).Editable Then
      Exit Sub
    End If

    edition_canceled = False
    RaiseEvent BeforeStartEditionEvent(FlexRow - MAX_HEADERS, FlexCol, edition_canceled)

    If edition_canceled Then
      Exit Sub
    End If

    With Me.flx_grid

      ' Set current control
      current_control = m_col_data(FlexCol).EditionControl.Control()

      ' Set position 
      twips_to_pixel = 96 / 1440 ' Temporary

      ' Calc Top position 
      kk = 0
      For idx_aux = 0 To FlexRow - 1
        If .get_RowIsVisible(idx_aux) Then
          kk = kk + .get_RowHeight(idx_aux)
        End If
      Next

      current_control.Top = kk * twips_to_pixel + 1 ' .get_RowPos(FlexRow) * twips_to_pixel + 1

      ' Calc Left position 
      kk = 0
      For idx_aux = 0 To FlexCol - 1
        If .get_ColIsVisible(idx_aux) Then
          kk = kk + .get_ColWidth(idx_aux)
        End If
      Next

      current_control.Left = kk * twips_to_pixel + 1
      current_control.Width = .get_ColWidth(FlexCol) * twips_to_pixel + 1
      current_control.Height = .get_RowHeight(FlexRow) * twips_to_pixel + 1

      ' Set color
      current_control.BackColor = GetColor(GUI_COLOR_WHITE_00)
      current_control.Forecolor = GetColor(GUI_COLOR_BLACK_00)
      current_control.Visible = True
      Call current_control.BringToFront()

      Select Case m_col_data(FlexCol).EditionControl(FlexRow - MAX_HEADERS).Type

        Case CONTROL_TYPE_COMBO
          ' Selected the item in the combo control
          ComboBoxEvents = current_control
          For idx_aux = 0 To ComboBoxEvents.Count - 1
            If CStr(ComboBoxEvents.TextValue(idx_aux)) = CStr(GetTextMatrix(FlexRow, FlexCol)) Then
              ComboBoxEvents.SelectedIndex = idx_aux
              Exit For
            End If
          Next
          ComboBoxEvents.Enabled = True
          ComboBoxEvents.TextWidth = 0
          ComboBoxEvents.SufixTextWidth = 0
          Call ComboBoxEvents.Focus()

        Case CONTROL_TYPE_ENTRY_FIELD
          EntryFieldEvents = current_control
          EntryFieldEvents.Value = GetTextMatrix(FlexRow, FlexCol)
          EntryFieldEvents.TextWidth = 0
          EntryFieldEvents.SufixTextWidth = 0
          Call EntryFieldEvents.Focus()

        Case CONTROL_TYPE_DATE_TIME
          DateTimeEvents = current_control
          DateTimeEvents.TextWidth = 0
          DateTimeEvents.SufixTextWidth = 0
          DateTimeEvents.GetFormat(format_date, format_time)
          DateTimeEvents.Value = GUI_ParseDateTime(GetTextMatrix(FlexRow, FlexCol), _
                                                   format_date, _
                                                   format_time)
          Call DateTimeEvents.Focus()

        Case CONTROL_TYPE_CHECK_BOX
          ' Special case so that the CheckBox is one bitmap
          current_control.Visible = False

      End Select

    End With


  End Sub

  ' Hide the current control
  Private Sub HideEditionControl(ByVal FlexRow As Integer, ByVal FlexCol As Integer, Optional ByVal EditionCancelled As Boolean = False)
    Dim current_control As Object
    Dim format_date As ENUM_FORMAT_DATE
    Dim format_time As ENUM_FORMAT_DATE
    Dim old_text As String

    'XVV 16/04/2007
    'Assign "" to SQL Trans because compiler generate warning for not have value
    Dim new_text As String = ""

    ' JAB 28-05-2013: First time is access is "nothing".
    If m_col_data Is Nothing Then
      Exit Sub
    End If

    If Not m_col_data(FlexCol).Editable Then
      Exit Sub
    End If

    ' Set current control
    current_control = m_col_data(FlexCol).EditionControl.Control()

    If current_control.visible = False Then
      Exit Sub
    End If

    If Not EditionCancelled Then
      With Me.flx_grid
        old_text = GetTextMatrix(FlexRow, FlexCol)

        ' Load info to grid and hide control
        Select Case m_col_data(FlexCol).EditionControl(FlexRow - MAX_HEADERS).Type

          Case CONTROL_TYPE_COMBO
            new_text = ComboBoxEvents.TextValue

          Case CONTROL_TYPE_ENTRY_FIELD
            new_text = EntryFieldEvents.GridValue

          Case CONTROL_TYPE_DATE_TIME
            DateTimeEvents.GetFormat(format_date, format_time)
            new_text = GUI_FormatDate(DateTimeEvents.Value, format_date, format_time)

          Case CONTROL_TYPE_CHECK_BOX
            ' Special case so that the CheckBox is one bitmap
            new_text = old_text

        End Select

        If new_text <> old_text Then
          Call .set_TextMatrix(FlexRow, FlexCol, new_text)
          RaiseEvent CellDataChangedEvent(FlexRow - MAX_HEADERS, FlexCol)
        End If

      End With
    End If

    current_control.Visible = False
    Me.flx_grid.Focus()

  End Sub

  ' Draw the CheckBox
  Public Sub PaintCheckBox(ByVal NumRow As Integer, ByVal NumCol As Integer)

    ' Set current cell
    m_semaphore = m_semaphore + 1
    Me.flx_grid.Row = NumRow
    Me.flx_grid.Col = NumCol
    m_semaphore = m_semaphore - 1

    ' If the data is unknown, set to GRID_CHK_UNCHECKED
    If GetTextMatrix(NumRow, NumCol) = "" Then
      Me.flx_grid.set_TextMatrix(NumRow, _
                                 NumCol, _
                                 GRID_CHK_UNCHECKED)
    End If

    ' Set fore as back color (then the text is hidden)
    Me.flx_grid.CellForeColor = Me.flx_grid.CellBackColor
    Me.flx_grid.CellPictureAlignment = VSFlex8.PictureAlignmentSettings.flexPicAlignCenterCenter

    ' Draw check box
    Me.flx_grid.CellPicture = Nothing

    'OC 29/04/04 - add to support collapse and expand
    If (Me.flx_grid.get_RowHeight(NumRow) = 0) Then
      Exit Sub
    End If

    'XVV -> Convert GUI_ParseNumber value return to Intenger (Images only accepts Interger value don't Double)
    Me.flx_grid.CellPicture = img_check.Images(CType(GUI_ParseNumber(GetTextMatrix(NumRow, NumCol)), Integer))

  End Sub

  ' Draw the editable mark 
  Public Sub PaintEditMark(ByVal NumRow As Integer, ByVal NumCol As Integer)

    ' Set current cell
    m_semaphore = m_semaphore + 1
    Me.flx_grid.Row = NumRow
    Me.flx_grid.Col = NumCol
    m_semaphore = m_semaphore - 1

    If Not m_col_data(Me.flx_grid.Col).Editable Then
      Exit Sub
    End If

    ' Draw check box
    Me.flx_grid.CellPicture = Nothing

    If (Me.flx_grid.get_RowHeight(NumRow) = 0) Then
      Exit Sub
    End If

    ' JAB 07-NOV-2013: Fixed bug #393, don't show edit mark to prevent 'outofmemory' error when a lot of editable rows are shown. 
    'Me.flx_grid.CellBackColor = m_editable_cell_color
    'Me.flx_grid.CellPicture = img_cell_edit.Images(0)
    Select Case EditableCellShowMode
      Case GRID_SHOW_EDIT_MODE.SHOW_MARK
        Me.flx_grid.CellPicture = img_cell_edit.Images(0)
      Case GRID_SHOW_EDIT_MODE.SHOW_BACKCOLOR
        Me.flx_grid.CellBackColor = EditableCellBackColor

      Case GRID_SHOW_EDIT_MODE.SHOW_BORDER
        Me.flx_grid.CellBorder(RGB(EditableCellBorderColor.R, EditableCellBorderColor.G, EditableCellBorderColor.B), _
                               EditableCellBorderSize.m_left, EditableCellBorderSize.m_top, _
                               EditableCellBorderSize.m_right, EditableCellBorderSize.m_bottom, _
                               EditableCellBorderSize.m_vertical, EditableCellBorderSize.m_horizontal)

      Case GRID_SHOW_EDIT_MODE.SHOW_NONE
        ' Don't show nothing - default mode

    End Select

  End Sub

  ' Change the CheckBox pictures
  Private Sub ChangeCheckBoxValue()
    Dim edition_canceled As Boolean
    If Not m_col_data(Me.flx_grid.Col).Editable Then
      Exit Sub
    End If

    If Me.flx_grid.Row < MAX_HEADERS Then
      Exit Sub
    End If

    ' JAB 30-MAY-2013: When mouse is moved over grey area, without row or column, 
    '                  the ".MouseRow" or/and ".MouseCol" property are -1 and if we try to access it, fails.
    If Me.flx_grid.MouseRow = -1 Or Me.flx_grid.MouseCol = -1 Then
      Exit Sub
    End If

    edition_canceled = False
    RaiseEvent BeforeStartEditionEvent(Me.flx_grid.Row - MAX_HEADERS, Me.flx_grid.Col, edition_canceled)

    If edition_canceled Then
      Exit Sub
    End If

    If m_col_data(Me.flx_grid.Col).EditionControl(Me.flx_grid.Row - MAX_HEADERS).Type = CONTROL_TYPE_CHECK_BOX Then
      If Me.flx_grid.get_TextMatrix(Me.flx_grid.Row, Me.flx_grid.Col) = GRID_CHK_CHECKED Then
        Me.flx_grid.set_TextMatrix(Me.flx_grid.Row, _
                                   Me.flx_grid.Col, _
                                   GRID_CHK_UNCHECKED)
      Else
        Me.flx_grid.set_TextMatrix(Me.flx_grid.Row, _
                                   Me.flx_grid.Col, _
                                   GRID_CHK_CHECKED)
      End If
      Call PaintCheckBox(Me.flx_grid.Row, Me.flx_grid.Col)
      RaiseEvent CellDataChangedEvent(Me.CurrentRow, Me.CurrentCol)

    End If
  End Sub

  Private Sub SortRoutine()
    'XVV Variable not use Dim row_idx As Integer
    Dim prev_screen_pointer As Long
    Dim prev_redraw As Boolean

    If Not Sortable Then
      Exit Sub
    End If

    If Not Me.Column(GLB_SortColumn).IsColumnSortable Then
      Exit Sub
    End If

    If flx_grid.Rows = flx_grid.FixedRows Then
      Exit Sub
    End If

    prev_screen_pointer = flx_grid.MousePointer
    prev_redraw = flx_grid.Redraw

    flx_grid.MousePointer = VSFlex8.MousePointerSettings.flexHourglass
    flx_grid.Redraw = False

    ' Do Sort
    flx_grid.Sort = 9

    flx_grid.MousePointer = prev_screen_pointer
    flx_grid.Redraw = prev_redraw

  End Sub

  Private Function CompareRowValue(ByVal RowValue1 As String, _
                                   ByVal RowValue2 As String) As Short
    Dim val1 As Double
    Dim val2 As Double
    Dim str_val1 As String
    Dim str_val2 As String
    Dim date1 As Date
    Dim date2 As Date

    If IsNumeric(RowValue1) And IsNumeric(RowValue2) Then
      ' Sort as number
      val1 = CDbl(RowValue1)
      val2 = CDbl(RowValue2)

      If val1 > val2 Then
        Return 1
      ElseIf val1 < val2 Then
        Return -1
      End If

      Return 0
    End If

    ' RCI 02-FEB-2011: Sort Percentage values as numeric.
    If RowValue1.EndsWith("%") And RowValue2.EndsWith("%") Then

      str_val1 = RowValue1.Substring(0, RowValue1.Length - 1)
      str_val2 = RowValue2.Substring(0, RowValue2.Length - 1)

      If IsNumeric(str_val1) And IsNumeric(str_val2) Then
        ' Sort as number
        val1 = CDbl(str_val1)
        val2 = CDbl(str_val2)

        If val1 > val2 Then
          Return 1
        ElseIf val1 < val2 Then
          Return -1
        End If

        Return 0
      End If
    End If

    ' RCI 02-FEB-2011: Both values must be Date.
    If IsDate(RowValue1) And IsDate(RowValue2) Then
      ' Sort as date
      If RowValue1.Length > 0 Then
        date1 = CDate(RowValue1)
      Else
        Return 1
      End If

      If RowValue2.Length > 0 Then
        date2 = CDate(RowValue2)
      Else
        Return -1
      End If

      If date1 > date2 Then
        Return 1
      ElseIf date1 < date2 Then
        Return -1
      End If

      Return 0
    End If

    ' Sort as string
    If RowValue1 > RowValue2 Then
      Return 1
    ElseIf RowValue1 < RowValue2 Then
      Return -1
    End If

    Return 0

  End Function

  ' PURPOSE: Set font size counter.
  '
  '  PARAMS:
  '     - INPUT:
  '			    - Label:		  System.Windows.Forms.Label
  ' 		    - DigCounter:	Integer
  '			    - Dig:			  Integer
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Private Shared Sub SetFontSize(ByRef Label As System.Windows.Forms.Label, ByRef DigCounter As Integer, ByVal Dig As Integer)
    Dim _default_size As Integer
    Dim _new_size As Integer

    _default_size = 12
    _new_size = _default_size

    If Dig > DigCounter Then

      Select Case Dig
        Case 1, 2, 3, 4  ' Default size
          Label.Font = New Drawing.Font(Label.Font.Name, _default_size, Label.Font.Style)

        Case 5 ' Spacial case
          If Label.Name <> "lbl_count0" Then
            _new_size = Label.Font.Size - Dig + 3
            Label.Font = New Drawing.Font(Label.Font.Name, _new_size, Label.Font.Style)
          Else
            Label.Font = New Drawing.Font(Label.Font.Name, _default_size, Label.Font.Style)
          End If

        Case Else ' Decrement size
          _new_size = Label.Font.Size - Dig + 4
          Label.Font = New Drawing.Font(Label.Font.Name, _new_size, Label.Font.Style)

      End Select

    End If

  End Sub

  Private Function CompareRows(ByVal Row1 As Long, ByVal Row2 As Long) As Short

    Dim rc As Short
    Dim idx_col As Integer

    rc = CompareRowValue(GetTextMatrix(Row1, GLB_SortColumn), _
                         GetTextMatrix(Row2, GLB_SortColumn))
    If rc <> 0 Then
      Return rc
    End If

    ' Rows are equal: let's see whether next sortable column can help to determine the order
    For idx_col = GLB_SortColumn + 1 To flx_grid.Cols - 1
      If flx_grid.get_ColWidth(idx_col) > 0 Then
        rc = CompareRowValue(GetTextMatrix(Row1, idx_col), _
                             GetTextMatrix(Row2, idx_col))
        If rc <> 0 Then
          Return rc
        End If
      End If
    Next

    Return rc
  End Function

  Private Function GetPosLastCounter() As Integer
    Dim idx As Integer
    Dim last_counter As Integer
    Dim pos_y As Integer

    last_counter = 0
    For idx = 0 To MAX_COUNTERS - 1
      If Me.Counter(idx).Visible Then
        last_counter = idx
      End If
    Next

    Select Case last_counter
      Case 0
        pos_y = Me.lbl_count0.Location.Y
      Case 1
        pos_y = Me.lbl_count1.Location.Y
      Case 2
        pos_y = Me.lbl_count2.Location.Y
      Case 3
        pos_y = Me.lbl_count3.Location.Y
      Case 4
        pos_y = Me.lbl_count4.Location.Y
      Case 5
        pos_y = Me.lbl_count5.Location.Y
      Case 6
        pos_y = Me.lbl_count6.Location.Y
      Case 7
        pos_y = Me.lbl_count7.Location.Y

      Case Else
        pos_y = Nothing
    End Select

    Return pos_y

  End Function

  Private Sub PosBtnInfo()
    Dim pos_y As Integer
    'XVV Variable not use Dim last_counter As Integer

    pos_y = GetPosLastCounter()

    Me.btn_info.Location = New System.Drawing.Point(0, pos_y + 30)

  End Sub

  ' PURPOSE : Activate license if it is not already and .ocx file register 
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '
  Private Sub FlexGridv8_Init()
    Dim _license_key As System.Reflection.FieldInfo

    If Not m_flexgrid_v8_activated Then
      Try
        _license_key = GetType(AxHost).GetField("licenseKey", Reflection.BindingFlags.NonPublic Or Reflection.BindingFlags.Instance)
        _license_key.SetValue(flx_grid, "O00O0C741")
        m_flexgrid_v8_activated = True

      Catch ex As Exception
        MsgBox("Error: vsFlex8 activation process. Application will stop")
        Environment.Exit(0)
      End Try
    End If

  End Sub ' FlexGridv8_Init

#End Region

#Region " Private Events "

  ' JAB 17-MAY-2012: Change AxMSFlexGrid to VSFlex8
  Private Sub flx_grid_LeaveCell(ByVal sender As Object, ByVal e As System.EventArgs) Handles flx_grid.LeaveCell
    'XVV Variable not use Dim curr_row As Integer
    'XVV Variable not use Dim curr_col As Integer
    'XVV Variable not use Dim idx As Integer

    If m_semaphore > 0 Then
      Return
    End If

    'Debug.WriteLineIf(Trace, Me.Name & ": " & "flx_grid_LeaveCell")

    m_semaphore = m_semaphore + 1
    Call HideEditionControl(Me.flx_grid.Row, Me.flx_grid.Col)
    m_last_col = flx_grid.Col
    m_last_row = flx_grid.Row
    m_semaphore = m_semaphore - 1
  End Sub ' flx_grid_LeaveCell

  Private Sub flx_grid_EnterCell(ByVal sender As Object, ByVal e As System.EventArgs) Handles flx_grid.EnterCell
    'XVV Variable not use Dim curr_row As Integer
    'XVV Variable not use Dim curr_col As Integer
    'XVV Variable not use Dim idx As Integer

    If m_semaphore > 0 Then
      Return
    End If

    RaiseEvent RowSelectedEvent(flx_grid.RowSel - MAX_HEADERS)

  End Sub ' flx_grid_EnterCell

  Private Sub flx_grid_BeforeRowColChange(ByVal sender As Object, ByVal e As AxVSFlex8._IVSFlexGridEvents_BeforeRowColChangeEvent) Handles flx_grid.BeforeRowColChange



    If e.newRow <> e.oldRow Then
      RaiseEvent BeforeRowChanged(e.oldRow, e.newRow, e.cancel)

    End If

  End Sub 'flx_grid_BeforeRowColChange

  Private Sub flx_grid_DblClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles flx_grid.DblClick
    'Debug.WriteLineIf(Trace, Me.Name & ": " & "flx_grid_DblClick" & " Redraw: " & Me.flx_grid.Redraw & " Semaphores:" & m_semaphore & "," & m_semaphore1)

    ' JAB 30-MAY-2013: When mouse is moved over grey area, without row or column, 
    '                  the ".MouseRow" or/and ".MouseCol" property are -1 and if we try to access it, fails.
    If Me.flx_grid.MouseRow = -1 Or Me.flx_grid.MouseCol = -1 Then
      Exit Sub
    End If

    If flx_grid.MouseRow < MAX_HEADERS Then
      Exit Sub
    End If

    'Hide last tooltip
    Me.tool_tip.SetToolTip(flx_grid, "")

    RaiseEvent DataSelectedEvent()
    Call ShowEditionControl(Me.flx_grid.Row, Me.flx_grid.Col)

  End Sub ' flx_grid_DblClick

  Private Sub flx_grid_KeyDownEvent(ByVal sender As Object, ByVal e As AxVSFlex8._IVSFlexGridEvents_KeyDownEvent) Handles flx_grid.KeyDownEvent
    Dim idx As Integer
    Dim redraw_state As Boolean

    'Debug.WriteLineIf(Trace, Me.Name & ": " & "flx_grid_KeyDownEvent " & e.keyCode & " " & e.shift & " " & m_all_selected)
    Select Case (e.keyCode)
      Case 17 ' ctrl
        If m_selection_mode = SELECTION_MODE.SELECTION_MODE_MULTIPLE Then
          If e.shift And 2 = 2 Then
            m_keep_selection = True
          Else
            m_keep_selection = False
          End If
        Else
          m_keep_selection = False
        End If

      Case 13
        RaiseEvent DataSelectedEvent()

      Case 16 ' shift
        If m_selection_mode = SELECTION_MODE.SELECTION_MODE_MULTIPLE Then
          If m_keep_selection Then
            m_ctrl_shift = True ' ctrl+shift
          End If
        End If

      Case 65 'CTRL+A option (select all rows)
        If m_selection_mode = SELECTION_MODE.SELECTION_MODE_MULTIPLE Then
          If e.shift = 2 And Not (m_all_selected) Then
            redraw_state = Me.Redraw
            Me.Redraw = False

            RaiseEvent MultiSelectBeginEvent(0)

            For idx = 0 To Me.NumRows - 1
              IsSelected(idx) = True
            Next

            RaiseEvent MultiSelectEndEvent(Me.NumRows - 1)

            m_all_selected = True
            Me.Redraw = redraw_state
          End If
        End If

      Case 40
        If Not Me.CurrentRow + 1 > Me.NumRows - 1 Then
          Me.IsSelected(Me.CurrentRow) = False
          Me.IsSelected(Me.CurrentRow + 1) = True
        End If

      Case 38
        If Not Me.CurrentRow < 1 Then
          Me.IsSelected(Me.CurrentRow) = False
          Me.IsSelected(Me.CurrentRow - 1) = True
        End If

      Case Else

    End Select

  End Sub ' flx_grid_KeyDownEvent

  Private Sub flx_grid_KeyUpEvent(ByVal sender As Object, ByVal e As AxVSFlex8._IVSFlexGridEvents_KeyUpEvent) Handles flx_grid.KeyUpEvent

    'Debug.WriteLineIf(Trace, Me.Name & ": " & "flx_grid_KeyUpEvent " & e.keyCode & " " & e.shift & " " & m_all_selected)
    Select Case (e.keyCode)
      Case 17
        m_keep_selection = False
        m_all_selected = False
        'Case 13
        '  RaiseEvent DataSelectedEvent()

      Case 16 ' shift
        m_ctrl_shift = False

      Case Else

    End Select
  End Sub ' flx_grid_KeyUpEvent

  Private Sub flx_grid_RowColChange(ByVal sender As Object, ByVal e As System.EventArgs) Handles flx_grid.RowColChange

    If m_semaphore > 0 Then
      Return
    End If

    If flx_grid.MouseRow < MAX_HEADERS Then
      Return
    End If

    'Debug.WriteLineIf(Trace, Me.Name & ": " & "flx_grid_RowColChange" & " Redraw: " & Me.flx_grid.Redraw & " Semaphores:" & m_semaphore & "," & m_semaphore1)
    Call PerformSelection()

    m_test = True

  End Sub ' flx_grid_RowColChange

  Private Sub flx_grid_ClickEvent(ByVal sender As Object, ByVal e As System.EventArgs) Handles flx_grid.ClickEvent
    Dim idx As Integer
    Dim cur_row As Integer
    Dim sel_row As Integer
    Dim redraw_state As Boolean

    'Debug.WriteLineIf(Trace, Me.Name & ": " & "flx_grid_ClickEvent" & " Redraw: " & Me.flx_grid.Redraw & " Semaphores:" & m_semaphore & "," & m_semaphore1)

    ' if a click is performed, m_all_selected must be false
    Me.m_all_selected = False

    ' JAB 30-MAY-2013: When mouse is moved over grey area, without row or column, 
    '                  the ".MouseRow" or/and ".MouseCol" property are -1 and if we try to access it, fails.
    If Me.flx_grid.MouseRow = -1 Or Me.flx_grid.MouseCol = -1 Then
      Exit Sub
    End If

    If flx_grid.MouseRow < MAX_HEADERS And flx_grid.MouseRow <> -1 Then

      ' Sort column
      If GLB_SortColumn = flx_grid.MouseCol Then
        GLB_SortAscending = Not GLB_SortAscending
      End If

      GLB_SortColumn = flx_grid.MouseCol
      Call SortRoutine()
      RaiseEvent DataUpdated()

      Exit Sub
    End If

    If Not m_test Then
      Call PerformSelection()
    End If
    m_test = False

    Call ChangeCheckBoxValue()

    ' CTRL+SHIFT selected - select from current row to clicked row
    If m_ctrl_shift Then
      Dim _begin_row As Integer
      Dim _end_row As Integer

      cur_row = Me.CurrentRow
      sel_row = flx_grid.MouseRow - MAX_HEADERS
      redraw_state = Me.Redraw
      Me.Redraw = False

      If cur_row < sel_row Then
        _begin_row = cur_row
        _end_row = sel_row
      Else
        _begin_row = sel_row
        _end_row = cur_row
      End If

      RaiseEvent MultiSelectBeginEvent(_begin_row)

      For idx = _begin_row To _end_row
        IsSelected(idx) = True
      Next

      RaiseEvent MultiSelectEndEvent(_end_row)

      Me.Redraw = redraw_state
    End If

  End Sub ' flx_grid_ClickEvent

  Private Sub flx_grid_KeyPressEvent(ByVal sender As Object, ByVal e As AxVSFlex8._IVSFlexGridEvents_KeyPressEvent) Handles flx_grid.KeyPressEvent

    If m_semaphore > 0 Then
      Return
    End If

    If Not m_col_data(Me.flx_grid.Col).Editable Then
      Exit Sub
    End If

    ' Events by controls
    Select Case m_col_data(Me.flx_grid.Col).EditionControl(Me.flx_grid.Row - MAX_HEADERS).Type

      ' Check box
      Case CONTROL_TYPE_CHECK_BOX
        If e.keyAscii = 32 Then
          Call ChangeCheckBoxValue()
        End If

      Case Else
        ' Others

    End Select

  End Sub ' flx_grid_KeyPressEvent

  Private Sub flx_grid_Scroll(ByVal sender As Object, ByVal e As AxVSFlex8._IVSFlexGridEvents_AfterScrollEvent) Handles flx_grid.AfterScroll
    Call HideEditionControl(Me.flx_grid.Row, Me.flx_grid.Col)
  End Sub ' flx_grid_Scroll

  Private Sub flx_grid_BeforeSort(ByVal sender As System.Object, ByVal e As AxVSFlex8._IVSFlexGridEvents_BeforeSortEvent) Handles flx_grid.BeforeSort
    Dim _str As String = ""

    If m_col_type_set Then
      Exit Sub
    End If

    If Me.NumRows <= 0 Then
      m_col_type_set = False
      Exit Sub
    End If

    For _col As Int32 = 0 To Me.NumColumns - 1

      For _row As Int32 = 0 To Math.Min(10, Me.NumRows - 1)
        _str = Me.Cell(_row, _col).Value

        ' 25-JAN-2013 RXM  
        If IsNumeric(_str) Then
          Me.flx_grid.set_ColDataType(_col, VSFlex8.DataTypeSettings.flexDTDecimal)
          Exit For
        End If

        If IsDate(_str) Then
          Me.flx_grid.set_ColDataType(_col, VSFlex8.DataTypeSettings.flexDTDate)
          Exit For
        End If

      Next
    Next

    m_col_type_set = True
  End Sub ' flx_grid_BeforeSort

  Private Sub EntryFieldEvents_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles EntryFieldEvents.Validated
    Call HideEditionControl(Me.flx_grid.Row, Me.flx_grid.Col)
  End Sub ' EntryFieldEvents_Validated

  Private Sub ComboBoxEvents_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBoxEvents.Validated
    Call HideEditionControl(Me.flx_grid.Row, Me.flx_grid.Col)
  End Sub ' ComboBoxEvents_Validated

  Private Sub DateTimeEvents_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles DateTimeEvents.Validated
    Call HideEditionControl(Me.flx_grid.Row, Me.flx_grid.Col)
  End Sub ' DateTimeEvents_Validated

  ' Delete row
  Private Sub btn_delete_row_ClickEvent() Handles btn_delete_row.ClickEvent
    If Me.CurrentRow < 0 Then
      Exit Sub
    End If

    RaiseEvent ButtonClickEvent(BUTTON_TYPE_DELETE)
  End Sub ' btn_delete_row_ClickEvent

  ' Add row 
  Private Sub btn_add_row_ClickEvent() Handles btn_add_row.ClickEvent
    RaiseEvent ButtonClickEvent(BUTTON_TYPE_ADD)
  End Sub ' btn_add_row_ClickEvent

  ' Info
  Public Sub btn_info_ClickEvent() Handles btn_info.ClickEvent
    RaiseEvent ButtonClickEvent(BUTTON_TYPE_INFO)
  End Sub ' btn_info_ClickEvent

  Private WithEvents img_check As System.Windows.Forms.ImageList

  ' Compare rows
  Private Sub flx_grid_Compare(ByVal sender As System.Object, ByVal e As AxVSFlex8._IVSFlexGridEvents_CompareEvent) Handles flx_grid.Compare
    e.cmp = CompareRows(e.row1, e.row2)
    If Not GLB_SortAscending Then
      e.cmp = -e.cmp
    End If
  End Sub

  ' Start tooltip timer when mouse moves over flex_grid
  Private Sub flx_grid_MouseMoveEvent(ByVal sender As Object, ByVal e As AxVSFlex8._IVSFlexGridEvents_MouseMoveEvent) Handles flx_grid.MouseMoveEvent
    Dim diff_ticks As Long
    Dim mouse_col As Integer
    Dim mouse_row As Integer

    Try

      ' JAB 30-MAY-2013: When mouse is moved over grey area, without row or column, 
      '                  the ".MouseRow" or/and ".MouseCol" property are -1 and if we try to access it, fails.
      If Me.flx_grid.MouseRow = -1 Or Me.flx_grid.MouseCol = -1 Then
        Exit Sub
      End If

      If e.shift <> 2 Then
        m_keep_selection = False
      End If

      If Not GLB_IsToolTipped Then
        Exit Sub

      End If

      ' exit if event was just launched
      diff_ticks = Now.Ticks - m_last_tooltip.Ticks
      If (diff_ticks < 1500000) Then
        Exit Sub

      End If

      m_last_tooltip = Now

      mouse_col = Me.flx_grid.MouseCol()
      mouse_row = Me.flx_grid.MouseRow()
      If (mouse_row <> m_tt_last_mouse_row) Or (mouse_col <> m_tt_last_mouse_col) Then
        ' close tooltip if already open when moving
        Me.tool_tip.AutoPopDelay = 0
      Else
        Me.tool_tip.AutoPopDelay = 30000
      End If

      '  Check if the column is "hidden"
      With Me.Column(mouse_col)
        If (.Fixed = True And .WidthFixed = 0) Or .Width = 0 Then

          Exit Sub
        End If
      End With

      ' start tooltip timer
      Call Me.tmr_mouse_move.Start()

    Catch ex As Exception

    End Try

  End Sub

  Private Sub flx_grid_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles flx_grid.SizeChanged

    Dim idx As Integer

    If m_col_data Is Nothing Then
      Exit Sub
    End If

    ' RCI 02-JUN-2011: Control Out of Range Error.
    If m_col_data.Length < Me.NumColumns Then
      Exit Sub
    End If

    If Me.m_init_fi Then
      For idx = 0 To Me.NumColumns - 1
        If Not m_col_data(idx) Is Nothing Then
          If (m_col_data(idx).Fixed = True) Then
            Me.flx_grid.set_ColWidth(idx, m_col_data(idx).WidthFixed)
          End If
        End If
      Next
    End If

  End Sub ' flx_grid_SizeChanged

  Private Sub tmr_mouse_move_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tmr_mouse_move.Tick
    Dim current_mouse_col As Integer
    Dim current_mouse_row As Integer
    Dim tooltip_txt As String
    'XVV Variable not use Dim point As System.Drawing.Point
    Dim ctrl As System.Windows.Forms.Control
    'XVV Variable not use Dim last_row_position As System.Drawing.Point
    Dim coeficient_aux As Double
    Dim size_width As Integer
    Dim size_height As Integer
    Dim idx As Integer
    Dim current_cell_value As String

    Try

      Call Me.tmr_mouse_move.Stop()

      ' do not show tooltip if not on flx_grid
      'XVV 17/04/2007
      'Change Me.MousePosition for Control.MousePosition because produces compile warnings 
      ctrl = Me.GetChildAtPoint(Me.PointToClient(Control.MousePosition))
      If Not ctrl Is Me.flx_grid Then
        Exit Sub

      End If

      ' JAB 30-MAY-2013: When mouse is moved over grey area, without row or column, 
      '                  the ".MouseRow" or/and ".MouseCol" property are -1 and if we try to access it, fails.
      If Me.flx_grid.MouseRow = -1 Or Me.flx_grid.MouseCol = -1 Then
        Exit Sub
      Else
        With Me.Column(flx_grid.MouseCol)
          If (.Fixed = True And .WidthFixed = 0) Or .Width = 0 Then

            Exit Sub
          End If
        End With
      End If

      '  do not show tooltip if column cannot be tooltipped
      If Not Me.Column(flx_grid.MouseCol).IsColumnToolTipped Then
        Exit Sub

      End If

      ' do not show tooltip on checkbox column
      If Me.Column(flx_grid.MouseCol).EditionControl(flx_grid.MouseRow - MAX_HEADERS).Type = CONTROL_TYPE_CHECK_BOX Then
        Exit Sub

      End If
      ''02-MAR-04-OC-added adjusments so tooltip will not be shown in the "gray zone".
      'If (Me.NumRows > 0) Then
      '  last_row_position.Y = Me.flx_grid.CellHeight - Me.flx_grid.get_RowHeight(Me.NumRows - 1)
      '  'do not show tooltip if we passed the last row of the grid(the y axle)
      '  If (PointToClient(MousePosition).Y > (((Math.Abs((last_row_position).Y)) * (Me.NumRows + 1) + MIN_ROWS))) Then
      '    Exit Sub
      '  End If
      '  'diffrent check if there are only a few rows
      '  If (Me.NumRows < MIN_ROWS) Then
      '    If (PointToClient(MousePosition).Y > ((Me.NumRows + MAX_HEADERS) * AVG_ROW_HEIGHT)) Then
      '      Exit Sub
      '    End If
      '  End If
      'End If

      ''  do not show tooltip if column is hidden
      'If Me.Column(current_mouse_col).WidthFixed = 0 Then
      '  If Me.Column(current_mouse_col).Width = 0 Then
      '    Exit Sub
      '  End If
      'End If
      ''end 02-MAR-04-OC

      size_width = 0
      For idx = 0 To flx_grid.Cols - 1
        size_width = size_width + flx_grid.get_ColWidth(idx)
      Next

      size_height = 0
      For idx = 0 To flx_grid.Rows - 1
        size_height = size_height + flx_grid.get_RowHeight(idx)
      Next

      coeficient_aux = 14.69

      If Me.PointToClient(MousePosition).Y > (size_height / coeficient_aux) Then

        Exit Sub
      End If

      coeficient_aux = 14.9

      If Me.PointToClient(MousePosition).X > (size_width / coeficient_aux) Then

        Exit Sub
      End If

      current_mouse_row = Me.flx_grid.MouseRow
      current_mouse_col = Me.flx_grid.MouseCol
      current_cell_value = NullTrim(Me.Cell(current_mouse_row - MAX_HEADERS, current_mouse_col).Value)

      If (current_mouse_row <> m_tt_last_mouse_row) Or (current_mouse_col <> m_tt_last_mouse_col) Then
        ' close tooltip if already open when moving
        Me.tool_tip.AutoPopDelay = 0
      Else
        Me.tool_tip.AutoPopDelay = 30000
      End If

      ' retrieve last tooltip text
      tooltip_txt = Me.tool_tip.GetToolTip(flx_grid)

      ' check if a new text needs to be put into tooltip (if on another cell)
      ' EOR 07-SEP-2017 if current cell value different
      If (current_mouse_row <> m_tt_last_mouse_row) Or (current_mouse_col <> m_tt_last_mouse_col) Or (current_cell_value <> tooltip_txt) Then
        ' do not show tooltip if no rows exist in grid
        If Me.NumRows = 0 Then
          tooltip_txt = ""
        Else
          tooltip_txt = NullTrim(Me.Cell(current_mouse_row - MAX_HEADERS, current_mouse_col).Value)
          m_tt_last_mouse_row = current_mouse_row
          m_tt_last_mouse_col = current_mouse_col

          Me.tool_tip.AutoPopDelay = 30000
          RaiseEvent SetToolTipTextEvent(current_mouse_row - MAX_HEADERS, current_mouse_col, tooltip_txt)
        End If
      End If

      Me.tool_tip.SetToolTip(flx_grid, tooltip_txt)
      m_last_tooltip = Now


    Catch ex As Exception

    End Try

  End Sub ' tmr_mouse_move_Tick

  Private Sub uc_grid_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Leave
    ' stop tooltip timer
    Me.tmr_mouse_move.Stop()
    Me.tool_tip.SetToolTip(flx_grid, "")

  End Sub

  Private Sub uc_grid_MouseWheel(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseWheel

    Dim num_lines As Integer
    Dim row_size As Integer
    Dim idx As Integer
    Dim scale As Double

    ' exit if no rows are shown in grid
    If Me.NumRows = 0 Then
      Exit Sub
    End If

    row_size = 0
    For idx = 0 To flx_grid.Rows - 1
      row_size = row_size + flx_grid.get_RowHeight(idx)
    Next

    scale = 15.157 '2880 / 190

    If Me.flx_grid.Height >= row_size / scale Then
      'All rows are showed
      Exit Sub
    End If

    ' Get num rows to scroll when press mouse wheel
    num_lines = SystemInformation.MouseWheelScrollLines()

    ' Is press roulette down
    If e.Delta < 0 Then
      If Me.TopRow + num_lines < Me.NumRows Then
        Me.TopRow = Me.TopRow + num_lines
      Else
        Me.TopRow = Me.NumRows - 1
      End If

      ' Is press roulette up
    Else
      If Me.TopRow - num_lines >= 0 Then
        Me.TopRow = Me.TopRow - num_lines
      Else
        Me.TopRow = 0
      End If
    End If
  End Sub ' uc_grid_MouseWheel


#End Region

#Region " Miscellaneous "

  Private m_test As Boolean = False

  Public Sub RepaintRow(ByVal Row As Integer)
    Dim redraw_enabled As Boolean
    'Debug.WriteLineIf(Trace, Me.Name & ": " & "RepaintRow")

    m_semaphore = m_semaphore + 1
    m_semaphore1 = m_semaphore1 + 1

    redraw_enabled = flx_grid.Redraw
    flx_grid.Redraw = False

    If IsSelected(Row) Then
      RowSelect(Row)
    Else
      RowUnselect(Row)
    End If

    flx_grid.Redraw = redraw_enabled
    m_semaphore = m_semaphore - 1
    m_semaphore1 = m_semaphore1 - 1
  End Sub

  Friend Function IndexToRow(ByVal Index As Integer) As Integer
    Dim idx_row As Integer

    If Index < Me.NumRows Then
      If Me.flx_grid.get_RowData(MAX_HEADERS + Index) = Index Then

        Return Index
      End If
    End If

    For idx_row = 0 To Me.NumRows - 1
      If Me.flx_grid.get_RowData(MAX_HEADERS + idx_row) = Index Then

        Exit For
      End If
    Next

    Return idx_row

  End Function

  Friend Sub SetCellBackColor(ByVal Row As Integer, ByVal Col As Integer, ByVal Color As System.Drawing.Color)
    Dim curr_row As Integer
    Dim curr_col As Integer
    'XVV Variable not use Dim idx As Integer
    Dim row_data As CLASS_ROW_DATA
    Dim fore_color As System.Drawing.Color
    Dim back_color As System.Drawing.Color

    If Row < 0 Or Row >= Me.NumRows Then
      Return
    End If

    m_semaphore = m_semaphore + 1

    curr_row = flx_grid.Row
    curr_col = flx_grid.Col

    flx_grid.Row = MAX_HEADERS + Row
    flx_grid.Col = Col

    row_data = Me.Row(Row)

    row_data.GetCellColor(Col, fore_color, back_color)
    row_data.SetCellColor(Col, fore_color, Color)

    flx_grid.CellBackColor = Color

    flx_grid.Row = curr_row
    flx_grid.Col = curr_col

    m_semaphore = m_semaphore - 1

  End Sub

  Friend Sub SetCellForeColor(ByVal Row As Integer, ByVal Col As Integer, ByVal Color As System.Drawing.Color)
    Dim curr_row As Integer
    Dim curr_col As Integer
    'XVV Variable not use Dim idx As Integer
    Dim row_data As CLASS_ROW_DATA
    Dim fore_color As System.Drawing.Color
    Dim back_color As System.Drawing.Color

    If Row < 0 Or Row >= Me.NumRows Then
      Return
    End If

    m_semaphore = m_semaphore + 1

    curr_row = flx_grid.Row
    curr_col = flx_grid.Col

    flx_grid.Row = MAX_HEADERS + Row
    flx_grid.Col = Col
    row_data = Me.Row(Row)

    row_data.GetCellColor(Col, fore_color, back_color)
    row_data.SetCellColor(Col, Color, back_color)

    flx_grid.CellForeColor = Color

    flx_grid.Row = curr_row
    flx_grid.Col = curr_col

    m_semaphore = m_semaphore - 1

  End Sub

#End Region

End Class
