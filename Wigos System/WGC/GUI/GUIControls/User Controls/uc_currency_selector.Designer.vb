﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_currency_selector
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.

  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.pnl_container = New System.Windows.Forms.Panel()
    Me.pnl_comission_promotion = New System.Windows.Forms.Panel()
    Me.lbl_fixed_foreign = New System.Windows.Forms.Label()
    Me.ef_fixed_commissions_foreign = New GUI_Controls.uc_entry_field()
    Me.ef_variable_commissions_foreign = New GUI_Controls.uc_entry_field()
    Me.ef_fixed_promotions_NR2_foreign = New GUI_Controls.uc_entry_field()
    Me.lbl_variable_foreign = New System.Windows.Forms.Label()
    Me.ef_variable_promotions_NR2_foreign = New GUI_Controls.uc_entry_field()
    Me.lbl_recharge_msg_foreign = New GUI_Controls.uc_text_field()
    Me.lbl_promotion_NR_msg_foreign = New GUI_Controls.uc_text_field()
    Me.lbl_national_amount_msg_foreign = New GUI_Controls.uc_text_field()
    Me.lbl_commissions_msg_foreign = New GUI_Controls.uc_text_field()
    Me.lbl_Net_amount_msg_foreign = New GUI_Controls.uc_text_field()
    Me.pnl_change = New System.Windows.Forms.Panel()
    Me.ef_change_foreign = New GUI_Controls.uc_entry_field()
    Me.lbl_decimals_msg_foreign = New GUI_Controls.uc_text_field()
    Me.cmb_decimals_foreign = New GUI_Controls.uc_combo()
    Me.cmb_foreign_currencies = New System.Windows.Forms.ComboBox()
    Me.ef_order_value = New GUI_Controls.uc_entry_field()
    Me.img_icon = New System.Windows.Forms.PictureBox()
    Me.chk_enable_currency = New System.Windows.Forms.CheckBox()
    Me.chk_foreign_currency = New System.Windows.Forms.CheckBox()
    Me.ef_currency = New GUI_Controls.uc_entry_field()
    Me.pnl_container.SuspendLayout()
    Me.pnl_comission_promotion.SuspendLayout()
    Me.pnl_change.SuspendLayout()
    CType(Me.img_icon, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'pnl_container
    '
    Me.pnl_container.Controls.Add(Me.pnl_comission_promotion)
    Me.pnl_container.Controls.Add(Me.pnl_change)
    Me.pnl_container.Controls.Add(Me.cmb_foreign_currencies)
    Me.pnl_container.Controls.Add(Me.ef_order_value)
    Me.pnl_container.Location = New System.Drawing.Point(3, 35)
    Me.pnl_container.Name = "pnl_container"
    Me.pnl_container.Size = New System.Drawing.Size(586, 294)
    Me.pnl_container.TabIndex = 0
    '
    'pnl_comission_promotion
    '
    Me.pnl_comission_promotion.Controls.Add(Me.lbl_fixed_foreign)
    Me.pnl_comission_promotion.Controls.Add(Me.ef_fixed_commissions_foreign)
    Me.pnl_comission_promotion.Controls.Add(Me.ef_variable_commissions_foreign)
    Me.pnl_comission_promotion.Controls.Add(Me.ef_fixed_promotions_NR2_foreign)
    Me.pnl_comission_promotion.Controls.Add(Me.lbl_variable_foreign)
    Me.pnl_comission_promotion.Controls.Add(Me.ef_variable_promotions_NR2_foreign)
    Me.pnl_comission_promotion.Controls.Add(Me.lbl_recharge_msg_foreign)
    Me.pnl_comission_promotion.Controls.Add(Me.lbl_promotion_NR_msg_foreign)
    Me.pnl_comission_promotion.Controls.Add(Me.lbl_national_amount_msg_foreign)
    Me.pnl_comission_promotion.Controls.Add(Me.lbl_commissions_msg_foreign)
    Me.pnl_comission_promotion.Controls.Add(Me.lbl_Net_amount_msg_foreign)
    Me.pnl_comission_promotion.Location = New System.Drawing.Point(3, 91)
    Me.pnl_comission_promotion.Name = "pnl_comission_promotion"
    Me.pnl_comission_promotion.Size = New System.Drawing.Size(583, 165)
    Me.pnl_comission_promotion.TabIndex = 36
    '
    'lbl_fixed_foreign
    '
    Me.lbl_fixed_foreign.AutoSize = True
    Me.lbl_fixed_foreign.Location = New System.Drawing.Point(173, 12)
    Me.lbl_fixed_foreign.Name = "lbl_fixed_foreign"
    Me.lbl_fixed_foreign.Size = New System.Drawing.Size(37, 13)
    Me.lbl_fixed_foreign.TabIndex = 23
    Me.lbl_fixed_foreign.Text = "xFixed"
    Me.lbl_fixed_foreign.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'ef_fixed_commissions_foreign
    '
    Me.ef_fixed_commissions_foreign.DoubleValue = 0.0R
    Me.ef_fixed_commissions_foreign.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_fixed_commissions_foreign.IntegerValue = 0
    Me.ef_fixed_commissions_foreign.IsReadOnly = False
    Me.ef_fixed_commissions_foreign.Location = New System.Drawing.Point(10, 30)
    Me.ef_fixed_commissions_foreign.Name = "ef_fixed_commissions_foreign"
    Me.ef_fixed_commissions_foreign.PlaceHolder = Nothing
    Me.ef_fixed_commissions_foreign.Size = New System.Drawing.Size(295, 25)
    Me.ef_fixed_commissions_foreign.SufixText = "Sufix Text"
    Me.ef_fixed_commissions_foreign.SufixTextVisible = True
    Me.ef_fixed_commissions_foreign.SufixTextWidth = 50
    Me.ef_fixed_commissions_foreign.TabIndex = 25
    Me.ef_fixed_commissions_foreign.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_fixed_commissions_foreign.TextValue = ""
    Me.ef_fixed_commissions_foreign.TextWidth = 120
    Me.ef_fixed_commissions_foreign.Value = ""
    Me.ef_fixed_commissions_foreign.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_variable_commissions_foreign
    '
    Me.ef_variable_commissions_foreign.DoubleValue = 0.0R
    Me.ef_variable_commissions_foreign.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_variable_commissions_foreign.IntegerValue = 0
    Me.ef_variable_commissions_foreign.IsReadOnly = False
    Me.ef_variable_commissions_foreign.Location = New System.Drawing.Point(329, 30)
    Me.ef_variable_commissions_foreign.Name = "ef_variable_commissions_foreign"
    Me.ef_variable_commissions_foreign.PlaceHolder = Nothing
    Me.ef_variable_commissions_foreign.Size = New System.Drawing.Size(173, 25)
    Me.ef_variable_commissions_foreign.SufixText = "Sufix Text"
    Me.ef_variable_commissions_foreign.SufixTextVisible = True
    Me.ef_variable_commissions_foreign.SufixTextWidth = 50
    Me.ef_variable_commissions_foreign.TabIndex = 26
    Me.ef_variable_commissions_foreign.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_variable_commissions_foreign.TextValue = ""
    Me.ef_variable_commissions_foreign.TextWidth = 0
    Me.ef_variable_commissions_foreign.Value = ""
    Me.ef_variable_commissions_foreign.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_fixed_promotions_NR2_foreign
    '
    Me.ef_fixed_promotions_NR2_foreign.DoubleValue = 0.0R
    Me.ef_fixed_promotions_NR2_foreign.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_fixed_promotions_NR2_foreign.IntegerValue = 0
    Me.ef_fixed_promotions_NR2_foreign.IsReadOnly = False
    Me.ef_fixed_promotions_NR2_foreign.Location = New System.Drawing.Point(10, 60)
    Me.ef_fixed_promotions_NR2_foreign.Name = "ef_fixed_promotions_NR2_foreign"
    Me.ef_fixed_promotions_NR2_foreign.PlaceHolder = Nothing
    Me.ef_fixed_promotions_NR2_foreign.Size = New System.Drawing.Size(295, 25)
    Me.ef_fixed_promotions_NR2_foreign.SufixText = "Sufix Text"
    Me.ef_fixed_promotions_NR2_foreign.SufixTextVisible = True
    Me.ef_fixed_promotions_NR2_foreign.SufixTextWidth = 50
    Me.ef_fixed_promotions_NR2_foreign.TabIndex = 27
    Me.ef_fixed_promotions_NR2_foreign.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_fixed_promotions_NR2_foreign.TextValue = ""
    Me.ef_fixed_promotions_NR2_foreign.TextWidth = 120
    Me.ef_fixed_promotions_NR2_foreign.Value = ""
    Me.ef_fixed_promotions_NR2_foreign.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_variable_foreign
    '
    Me.lbl_variable_foreign.AutoSize = True
    Me.lbl_variable_foreign.Location = New System.Drawing.Point(368, 11)
    Me.lbl_variable_foreign.Name = "lbl_variable_foreign"
    Me.lbl_variable_foreign.Size = New System.Drawing.Size(50, 13)
    Me.lbl_variable_foreign.TabIndex = 24
    Me.lbl_variable_foreign.Text = "xVariable"
    Me.lbl_variable_foreign.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'ef_variable_promotions_NR2_foreign
    '
    Me.ef_variable_promotions_NR2_foreign.DoubleValue = 0.0R
    Me.ef_variable_promotions_NR2_foreign.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_variable_promotions_NR2_foreign.IntegerValue = 0
    Me.ef_variable_promotions_NR2_foreign.IsReadOnly = False
    Me.ef_variable_promotions_NR2_foreign.Location = New System.Drawing.Point(329, 60)
    Me.ef_variable_promotions_NR2_foreign.Name = "ef_variable_promotions_NR2_foreign"
    Me.ef_variable_promotions_NR2_foreign.PlaceHolder = Nothing
    Me.ef_variable_promotions_NR2_foreign.Size = New System.Drawing.Size(173, 25)
    Me.ef_variable_promotions_NR2_foreign.SufixText = "Sufix Text"
    Me.ef_variable_promotions_NR2_foreign.SufixTextVisible = True
    Me.ef_variable_promotions_NR2_foreign.SufixTextWidth = 50
    Me.ef_variable_promotions_NR2_foreign.TabIndex = 28
    Me.ef_variable_promotions_NR2_foreign.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_variable_promotions_NR2_foreign.TextValue = ""
    Me.ef_variable_promotions_NR2_foreign.TextWidth = 0
    Me.ef_variable_promotions_NR2_foreign.Value = ""
    Me.ef_variable_promotions_NR2_foreign.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_recharge_msg_foreign
    '
    Me.lbl_recharge_msg_foreign.AutoSize = True
    Me.lbl_recharge_msg_foreign.Font = New System.Drawing.Font("Verdana", 7.25!)
    Me.lbl_recharge_msg_foreign.IsReadOnly = True
    Me.lbl_recharge_msg_foreign.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_recharge_msg_foreign.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_recharge_msg_foreign.Location = New System.Drawing.Point(51, 91)
    Me.lbl_recharge_msg_foreign.Name = "lbl_recharge_msg_foreign"
    Me.lbl_recharge_msg_foreign.Size = New System.Drawing.Size(230, 23)
    Me.lbl_recharge_msg_foreign.SufixText = "Sufix Text"
    Me.lbl_recharge_msg_foreign.SufixTextVisible = True
    Me.lbl_recharge_msg_foreign.TabIndex = 29
    Me.lbl_recharge_msg_foreign.TabStop = False
    Me.lbl_recharge_msg_foreign.TextWidth = 0
    Me.lbl_recharge_msg_foreign.Value = "xFor a recharge of 100 USD"
    '
    'lbl_promotion_NR_msg_foreign
    '
    Me.lbl_promotion_NR_msg_foreign.AutoSize = True
    Me.lbl_promotion_NR_msg_foreign.Font = New System.Drawing.Font("Verdana", 7.25!)
    Me.lbl_promotion_NR_msg_foreign.IsReadOnly = True
    Me.lbl_promotion_NR_msg_foreign.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_promotion_NR_msg_foreign.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_promotion_NR_msg_foreign.Location = New System.Drawing.Point(307, 132)
    Me.lbl_promotion_NR_msg_foreign.Name = "lbl_promotion_NR_msg_foreign"
    Me.lbl_promotion_NR_msg_foreign.Size = New System.Drawing.Size(211, 23)
    Me.lbl_promotion_NR_msg_foreign.SufixText = "Sufix Text"
    Me.lbl_promotion_NR_msg_foreign.SufixTextVisible = True
    Me.lbl_promotion_NR_msg_foreign.TabIndex = 33
    Me.lbl_promotion_NR_msg_foreign.TabStop = False
    Me.lbl_promotion_NR_msg_foreign.TextWidth = 0
    Me.lbl_promotion_NR_msg_foreign.Value = "xPromotion NR: [ ]"
    '
    'lbl_national_amount_msg_foreign
    '
    Me.lbl_national_amount_msg_foreign.AutoSize = True
    Me.lbl_national_amount_msg_foreign.Font = New System.Drawing.Font("Verdana", 7.25!)
    Me.lbl_national_amount_msg_foreign.IsReadOnly = True
    Me.lbl_national_amount_msg_foreign.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_national_amount_msg_foreign.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_national_amount_msg_foreign.Location = New System.Drawing.Point(70, 111)
    Me.lbl_national_amount_msg_foreign.Name = "lbl_national_amount_msg_foreign"
    Me.lbl_national_amount_msg_foreign.Size = New System.Drawing.Size(220, 23)
    Me.lbl_national_amount_msg_foreign.SufixText = "Sufix Text"
    Me.lbl_national_amount_msg_foreign.SufixTextVisible = True
    Me.lbl_national_amount_msg_foreign.TabIndex = 30
    Me.lbl_national_amount_msg_foreign.TabStop = False
    Me.lbl_national_amount_msg_foreign.TextWidth = 0
    Me.lbl_national_amount_msg_foreign.Value = "xNational Amount: [ ]"
    '
    'lbl_commissions_msg_foreign
    '
    Me.lbl_commissions_msg_foreign.AutoSize = True
    Me.lbl_commissions_msg_foreign.Font = New System.Drawing.Font("Verdana", 7.25!)
    Me.lbl_commissions_msg_foreign.IsReadOnly = True
    Me.lbl_commissions_msg_foreign.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_commissions_msg_foreign.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_commissions_msg_foreign.Location = New System.Drawing.Point(307, 111)
    Me.lbl_commissions_msg_foreign.Name = "lbl_commissions_msg_foreign"
    Me.lbl_commissions_msg_foreign.Size = New System.Drawing.Size(220, 23)
    Me.lbl_commissions_msg_foreign.SufixText = "Sufix Text"
    Me.lbl_commissions_msg_foreign.SufixTextVisible = True
    Me.lbl_commissions_msg_foreign.TabIndex = 32
    Me.lbl_commissions_msg_foreign.TabStop = False
    Me.lbl_commissions_msg_foreign.TextWidth = 0
    Me.lbl_commissions_msg_foreign.Value = "xCommissions: [ ]"
    '
    'lbl_Net_amount_msg_foreign
    '
    Me.lbl_Net_amount_msg_foreign.AutoSize = True
    Me.lbl_Net_amount_msg_foreign.Font = New System.Drawing.Font("Verdana", 7.25!)
    Me.lbl_Net_amount_msg_foreign.IsReadOnly = True
    Me.lbl_Net_amount_msg_foreign.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_Net_amount_msg_foreign.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_Net_amount_msg_foreign.Location = New System.Drawing.Point(70, 132)
    Me.lbl_Net_amount_msg_foreign.Name = "lbl_Net_amount_msg_foreign"
    Me.lbl_Net_amount_msg_foreign.Size = New System.Drawing.Size(211, 23)
    Me.lbl_Net_amount_msg_foreign.SufixText = "Sufix Text"
    Me.lbl_Net_amount_msg_foreign.SufixTextVisible = True
    Me.lbl_Net_amount_msg_foreign.TabIndex = 31
    Me.lbl_Net_amount_msg_foreign.TabStop = False
    Me.lbl_Net_amount_msg_foreign.TextWidth = 0
    Me.lbl_Net_amount_msg_foreign.Value = "xNet Amount: [ ]"
    '
    'pnl_change
    '
    Me.pnl_change.Controls.Add(Me.ef_change_foreign)
    Me.pnl_change.Controls.Add(Me.lbl_decimals_msg_foreign)
    Me.pnl_change.Controls.Add(Me.cmb_decimals_foreign)
    Me.pnl_change.Location = New System.Drawing.Point(3, 29)
    Me.pnl_change.Name = "pnl_change"
    Me.pnl_change.Size = New System.Drawing.Size(583, 60)
    Me.pnl_change.TabIndex = 35
    '
    'ef_change_foreign
    '
    Me.ef_change_foreign.DoubleValue = 0.0R
    Me.ef_change_foreign.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_change_foreign.IntegerValue = 0
    Me.ef_change_foreign.IsReadOnly = False
    Me.ef_change_foreign.Location = New System.Drawing.Point(56, 3)
    Me.ef_change_foreign.Name = "ef_change_foreign"
    Me.ef_change_foreign.PlaceHolder = Nothing
    Me.ef_change_foreign.Size = New System.Drawing.Size(203, 25)
    Me.ef_change_foreign.SufixText = "Sufix Text"
    Me.ef_change_foreign.SufixTextVisible = True
    Me.ef_change_foreign.TabIndex = 20
    Me.ef_change_foreign.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_change_foreign.TextValue = ""
    Me.ef_change_foreign.TextWidth = 90
    Me.ef_change_foreign.Value = ""
    Me.ef_change_foreign.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_decimals_msg_foreign
    '
    Me.lbl_decimals_msg_foreign.AutoSize = True
    Me.lbl_decimals_msg_foreign.Font = New System.Drawing.Font("Verdana", 7.25!)
    Me.lbl_decimals_msg_foreign.IsReadOnly = True
    Me.lbl_decimals_msg_foreign.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_decimals_msg_foreign.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_decimals_msg_foreign.Location = New System.Drawing.Point(136, 34)
    Me.lbl_decimals_msg_foreign.Name = "lbl_decimals_msg_foreign"
    Me.lbl_decimals_msg_foreign.Size = New System.Drawing.Size(367, 23)
    Me.lbl_decimals_msg_foreign.SufixText = "Sufix Text"
    Me.lbl_decimals_msg_foreign.SufixTextVisible = True
    Me.lbl_decimals_msg_foreign.TabIndex = 22
    Me.lbl_decimals_msg_foreign.TabStop = False
    Me.lbl_decimals_msg_foreign.TextWidth = 0
    Me.lbl_decimals_msg_foreign.Value = "xDecimals: Accuracy of the amounts resulting."
    '
    'cmb_decimals_foreign
    '
    Me.cmb_decimals_foreign.AllowUnlistedValues = False
    Me.cmb_decimals_foreign.AutoCompleteMode = False
    Me.cmb_decimals_foreign.IsReadOnly = False
    Me.cmb_decimals_foreign.Location = New System.Drawing.Point(266, 3)
    Me.cmb_decimals_foreign.Name = "cmb_decimals_foreign"
    Me.cmb_decimals_foreign.SelectedIndex = -1
    Me.cmb_decimals_foreign.Size = New System.Drawing.Size(189, 24)
    Me.cmb_decimals_foreign.SufixText = "Sufix Text"
    Me.cmb_decimals_foreign.SufixTextVisible = True
    Me.cmb_decimals_foreign.TabIndex = 21
    Me.cmb_decimals_foreign.TextCombo = Nothing
    Me.cmb_decimals_foreign.TextWidth = 70
    '
    'cmb_foreign_currencies
    '
    Me.cmb_foreign_currencies.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cmb_foreign_currencies.FormattingEnabled = True
    Me.cmb_foreign_currencies.Location = New System.Drawing.Point(203, 2)
    Me.cmb_foreign_currencies.Name = "cmb_foreign_currencies"
    Me.cmb_foreign_currencies.Size = New System.Drawing.Size(233, 21)
    Me.cmb_foreign_currencies.TabIndex = 19
    '
    'ef_order_value
    '
    Me.ef_order_value.DoubleValue = 0.0R
    Me.ef_order_value.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_order_value.IntegerValue = 0
    Me.ef_order_value.IsReadOnly = False
    Me.ef_order_value.Location = New System.Drawing.Point(339, 245)
    Me.ef_order_value.Name = "ef_order_value"
    Me.ef_order_value.PlaceHolder = Nothing
    Me.ef_order_value.Size = New System.Drawing.Size(203, 25)
    Me.ef_order_value.SufixText = "Sufix Text"
    Me.ef_order_value.SufixTextVisible = True
    Me.ef_order_value.TabIndex = 34
    Me.ef_order_value.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_order_value.TextValue = "30"
    Me.ef_order_value.TextWidth = 90
    Me.ef_order_value.Value = "30"
    Me.ef_order_value.ValueForeColor = System.Drawing.Color.Blue
    Me.ef_order_value.Visible = False
    '
    'img_icon
    '
    Me.img_icon.Location = New System.Drawing.Point(30, 4)
    Me.img_icon.Name = "img_icon"
    Me.img_icon.Size = New System.Drawing.Size(32, 32)
    Me.img_icon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
    Me.img_icon.TabIndex = 37
    Me.img_icon.TabStop = False
    '
    'chk_enable_currency
    '
    Me.chk_enable_currency.AutoSize = True
    Me.chk_enable_currency.Location = New System.Drawing.Point(9, 6)
    Me.chk_enable_currency.Name = "chk_enable_currency"
    Me.chk_enable_currency.Size = New System.Drawing.Size(106, 17)
    Me.chk_enable_currency.TabIndex = 35
    Me.chk_enable_currency.Text = "xEnableCurrency"
    Me.chk_enable_currency.UseVisualStyleBackColor = True
    '
    'chk_foreign_currency
    '
    Me.chk_foreign_currency.AutoSize = True
    Me.chk_foreign_currency.Location = New System.Drawing.Point(9, 6)
    Me.chk_foreign_currency.Name = "chk_foreign_currency"
    Me.chk_foreign_currency.Size = New System.Drawing.Size(108, 17)
    Me.chk_foreign_currency.TabIndex = 17
    Me.chk_foreign_currency.Text = "xForeignCurrency"
    Me.chk_foreign_currency.UseVisualStyleBackColor = True
    '
    'ef_currency
    '
    Me.ef_currency.DoubleValue = 0.0R
    Me.ef_currency.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_currency.IntegerValue = 0
    Me.ef_currency.IsReadOnly = True
    Me.ef_currency.Location = New System.Drawing.Point(95, 11)
    Me.ef_currency.Name = "ef_currency"
    Me.ef_currency.PlaceHolder = Nothing
    Me.ef_currency.Size = New System.Drawing.Size(323, 25)
    Me.ef_currency.SufixText = "Sufix Text"
    Me.ef_currency.SufixTextVisible = True
    Me.ef_currency.TabIndex = 35
    Me.ef_currency.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_currency.TextValue = ""
    Me.ef_currency.TextWidth = 110
    Me.ef_currency.Value = ""
    Me.ef_currency.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_currency_selector
    '
    Me.Controls.Add(Me.ef_currency)
    Me.Controls.Add(Me.img_icon)
    Me.Controls.Add(Me.chk_foreign_currency)
    Me.Controls.Add(Me.chk_enable_currency)
    Me.Controls.Add(Me.pnl_container)
    Me.Name = "uc_currency_selector"
    Me.Padding = New System.Windows.Forms.Padding(3)
    Me.Size = New System.Drawing.Size(598, 335)
    Me.pnl_container.ResumeLayout(False)
    Me.pnl_comission_promotion.ResumeLayout(False)
    Me.pnl_comission_promotion.PerformLayout()
    Me.pnl_change.ResumeLayout(False)
    Me.pnl_change.PerformLayout()
    CType(Me.img_icon, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents pnl_container As System.Windows.Forms.Panel
  Friend WithEvents chk_foreign_currency As System.Windows.Forms.CheckBox
  Friend WithEvents cmb_decimals_foreign As GUI_Controls.uc_combo
  Friend WithEvents cmb_foreign_currencies As System.Windows.Forms.ComboBox
  Friend WithEvents lbl_variable_foreign As System.Windows.Forms.Label
  Friend WithEvents lbl_fixed_foreign As System.Windows.Forms.Label
  Friend WithEvents lbl_promotion_NR_msg_foreign As GUI_Controls.uc_text_field
  Friend WithEvents lbl_commissions_msg_foreign As GUI_Controls.uc_text_field
  Friend WithEvents lbl_Net_amount_msg_foreign As GUI_Controls.uc_text_field
  Friend WithEvents lbl_national_amount_msg_foreign As GUI_Controls.uc_text_field
  Friend WithEvents lbl_recharge_msg_foreign As GUI_Controls.uc_text_field
  Friend WithEvents ef_variable_promotions_NR2_foreign As GUI_Controls.uc_entry_field
  Friend WithEvents ef_fixed_promotions_NR2_foreign As GUI_Controls.uc_entry_field
  Friend WithEvents ef_variable_commissions_foreign As GUI_Controls.uc_entry_field
  Friend WithEvents ef_fixed_commissions_foreign As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_decimals_msg_foreign As GUI_Controls.uc_text_field
  Friend WithEvents ef_change_foreign As GUI_Controls.uc_entry_field
  Friend WithEvents ef_order_value As GUI_Controls.uc_entry_field
  Friend WithEvents chk_enable_currency As System.Windows.Forms.CheckBox
  Friend WithEvents ef_currency As GUI_Controls.uc_entry_field
  Friend WithEvents img_icon As System.Windows.Forms.PictureBox
  Friend WithEvents pnl_comission_promotion As System.Windows.Forms.Panel
  Friend WithEvents pnl_change As System.Windows.Forms.Panel



End Class
