'-------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_week_days.vb  
'
' DESCRIPTION:   control week days selection
'
' AUTHOR:        Javier Molina
'
' CREATION DATE: 09-JUL-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 09-JUL-2014  JMM    Initial version
'-------------------------------------------------------------------

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_Controls.uc_weekdays_by_hour

Public Class uc_week_days
#Region " Public Functions "

  ' TODO UNRANKED - JBP Private m_checked_change_event As CheckedChangedEventHandler

#End Region

#Region " Public Functions "

  ' TODO UNRANKED - JBP 
  'Public Overloads Property CheckedChangeEvent() As CheckedChangedEventHandler
  '  Get
  '    Return Me.m_checked_change_event
  '  End Get

  '  Set(ByVal Value As CheckedChangedEventHandler)
  '    Me.m_checked_change_event = Value
  '  End Set
  'End Property

  Public Overloads Property Weekday() As Integer
    Get
      Return GetWeekday()
    End Get

    Set(ByVal Value As Integer)
      SetWeekday(Value)
    End Set
  End Property

  Public Overloads ReadOnly Property AllWeekDaysSelected() As Integer
    Get
      Return &H7F
    End Get
  End Property

  Public Overloads ReadOnly Property NoWeekDaysSelected() As Integer
    Get
      Return 0
    End Get
  End Property

  Public Sub SetDefaults()
    Weekday = &H7F        ' All days
  End Sub ' SetDefaults

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControls()

  End Sub ' New

  Public Sub SetFocus(ByVal ErrorCode As Integer)
    Select Case ErrorCode
      Case 4
        ' No week day has been marked
        Me.chk_monday.Focus()

      Case 5
        ' All week days must be selected (depending on other form options)
        Me.chk_monday.Focus()

    End Select
  End Sub ' SetFocus

  Public Sub ProtectSystemPromotion()
    Me.Enabled = False

  End Sub ' ProtectSystemPromotion
  Private Sub InitControls()
    Dim _week_days_chk_array(6) As Windows.Forms.CheckBox

    Me.chk_monday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(301)
    Me.chk_tuesday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(302)
    Me.chk_wednesday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(303)
    Me.chk_thursday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(304)
    Me.chk_friday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(305)
    Me.chk_saturday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(306)
    Me.chk_sunday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(307)

    _week_days_chk_array(0) = chk_monday
    _week_days_chk_array(1) = chk_tuesday
    _week_days_chk_array(2) = chk_wednesday
    _week_days_chk_array(3) = chk_thursday
    _week_days_chk_array(4) = chk_friday
    _week_days_chk_array(5) = chk_saturday
    _week_days_chk_array(6) = chk_sunday

    Call Misc.SortCheckBoxesByFirstDayOfWeek(_week_days_chk_array)

    'AddHandler chk_monday.CheckedChanged, AddressOf Me.

  End Sub ' InitControls  

  Public Function GetNumDaysSelected()
    Dim numDays As Integer = 0

    numDays += IIf(Me.chk_monday.Checked, 1, 0)
    numDays += IIf(Me.chk_tuesday.Checked, 1, 0)
    numDays += IIf(Me.chk_wednesday.Checked, 1, 0)
    numDays += IIf(Me.chk_thursday.Checked, 1, 0)
    numDays += IIf(Me.chk_friday.Checked, 1, 0)
    numDays += IIf(Me.chk_saturday.Checked, 1, 0)
    numDays += IIf(Me.chk_sunday.Checked, 1, 0)

    Return numDays
  End Function

#End Region ' Public Functions 

#Region " Private Functions "

  Public Sub SetWeekday(ByVal Days As Integer)
    Dim str_binary As String
    Dim bit_array As Char()

    str_binary = Convert.ToString(Days, 2)
    str_binary = New String("0", 7 - str_binary.Length) + str_binary

    bit_array = str_binary.ToCharArray()

    If (bit_array(6) = "0") Then Me.chk_monday.Checked = False Else Me.chk_monday.Checked = True ' Monday 
    If (bit_array(5) = "0") Then Me.chk_tuesday.Checked = False Else Me.chk_tuesday.Checked = True ' Tuesday
    If (bit_array(4) = "0") Then Me.chk_wednesday.Checked = False Else Me.chk_wednesday.Checked = True ' Wednesday
    If (bit_array(3) = "0") Then Me.chk_thursday.Checked = False Else Me.chk_thursday.Checked = True ' Thursday
    If (bit_array(2) = "0") Then Me.chk_friday.Checked = False Else Me.chk_friday.Checked = True ' Friday
    If (bit_array(1) = "0") Then Me.chk_saturday.Checked = False Else Me.chk_saturday.Checked = True ' Saturday
    If (bit_array(0) = "0") Then Me.chk_sunday.Checked = False Else Me.chk_sunday.Checked = True ' Sunday

  End Sub ' SetWeekday

  Private Function GetWeekday() As Integer
    Dim bit_array As Char()

    bit_array = New String("0000000").ToCharArray()

    bit_array(6) = Convert.ToInt32(Me.chk_monday.Checked).ToString()  ' Monday 
    bit_array(5) = Convert.ToInt32(Me.chk_tuesday.Checked).ToString()  ' Tuesday
    bit_array(4) = Convert.ToInt32(Me.chk_wednesday.Checked).ToString()  ' Wednesday
    bit_array(3) = Convert.ToInt32(Me.chk_thursday.Checked).ToString()  ' Thursday
    bit_array(2) = Convert.ToInt32(Me.chk_friday.Checked).ToString()  ' Friday
    bit_array(1) = Convert.ToInt32(Me.chk_saturday.Checked).ToString()  ' Saturday
    bit_array(0) = Convert.ToInt32(Me.chk_sunday.Checked).ToString()  ' Sunday

    Return Convert.ToInt32(bit_array, 2)

  End Function ' GetWeekday

  Private Sub SortDayWeekCheckBoxes(ByVal FirstDayOfWeek As DayOfWeek)
    Dim _locations_array(6) As Drawing.Point
    Dim _check_boxes_array(6) As Windows.Forms.CheckBox
    Dim _first_day_of_week_check_box As Int32
    Dim _idx_days As Int32
    Dim _idx_chek_box As Int32

    Select Case FirstDayOfWeek
      Case DayOfWeek.Monday
        Return
      Case DayOfWeek.Tuesday
        _first_day_of_week_check_box = 1
      Case DayOfWeek.Wednesday
        _first_day_of_week_check_box = 2
      Case DayOfWeek.Thursday
        _first_day_of_week_check_box = 3
      Case DayOfWeek.Friday
        _first_day_of_week_check_box = 4
      Case DayOfWeek.Saturday
        _first_day_of_week_check_box = 5
      Case DayOfWeek.Sunday
        _first_day_of_week_check_box = 6
      Case Else
        Return
    End Select

    _locations_array(0) = Me.chk_monday.Location
    _locations_array(1) = Me.chk_tuesday.Location
    _locations_array(2) = Me.chk_wednesday.Location
    _locations_array(3) = Me.chk_thursday.Location
    _locations_array(4) = Me.chk_friday.Location
    _locations_array(5) = Me.chk_saturday.Location
    _locations_array(6) = Me.chk_sunday.Location

    _check_boxes_array(0) = Me.chk_monday
    _check_boxes_array(1) = Me.chk_tuesday
    _check_boxes_array(2) = Me.chk_wednesday
    _check_boxes_array(3) = Me.chk_thursday
    _check_boxes_array(4) = Me.chk_friday
    _check_boxes_array(5) = Me.chk_saturday
    _check_boxes_array(6) = Me.chk_sunday

    For _idx_days = 0 To 6
      _idx_chek_box = (_first_day_of_week_check_box + _idx_days) Mod 7
      _check_boxes_array(_idx_chek_box).Location = _locations_array(_idx_days)
    Next

  End Sub ' MoveDayWeekCheckBoxes

#End Region ' Private Functions 

#Region "Events" 'Events


  Public Event CheckedChange()

  Public Sub MondayCheckedChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_monday.CheckedChanged
    RaiseEvent CheckedChange()
  End Sub

  Public Sub TuesdayCheckedChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_tuesday.CheckedChanged
    RaiseEvent CheckedChange()
  End Sub
  Public Sub WednesdayCheckedChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_wednesday.CheckedChanged
    RaiseEvent CheckedChange()
  End Sub
  Public Sub thursdayCheckedChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_thursday.CheckedChanged
    RaiseEvent CheckedChange()
  End Sub
  Public Sub fridayCheckedChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_friday.CheckedChanged
    RaiseEvent CheckedChange()
  End Sub
  Public Sub saturdayCheckedChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_saturday.CheckedChanged
    RaiseEvent CheckedChange()
  End Sub
  Public Sub sundayCheckedChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_sunday.CheckedChanged
    RaiseEvent CheckedChange()
  End Sub


#End Region ' Events

End Class
