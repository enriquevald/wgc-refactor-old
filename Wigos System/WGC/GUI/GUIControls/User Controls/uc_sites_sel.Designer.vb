<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_sites_sel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_sites = New System.Windows.Forms.GroupBox
    Me.btn_uncheck_all = New GUI_Controls.uc_button
    Me.btn_check_all = New GUI_Controls.uc_button
    Me.dg_sites = New GUI_Controls.uc_grid
    Me.gb_sites.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_sites
    '
    Me.gb_sites.Controls.Add(Me.btn_uncheck_all)
    Me.gb_sites.Controls.Add(Me.btn_check_all)
    Me.gb_sites.Controls.Add(Me.dg_sites)
    Me.gb_sites.Location = New System.Drawing.Point(1, 3)
    Me.gb_sites.Name = "gb_sites"
    Me.gb_sites.Size = New System.Drawing.Size(243, 160)
    Me.gb_sites.TabIndex = 0
    Me.gb_sites.TabStop = False
    Me.gb_sites.Text = "xSites"
    '
    'btn_uncheck_all
    '
    Me.btn_uncheck_all.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_uncheck_all.Location = New System.Drawing.Point(107, 122)
    Me.btn_uncheck_all.Name = "btn_uncheck_all"
    Me.btn_uncheck_all.Size = New System.Drawing.Size(90, 30)
    Me.btn_uncheck_all.TabIndex = 4
    Me.btn_uncheck_all.ToolTipped = False
    Me.btn_uncheck_all.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'btn_check_all
    '
    Me.btn_check_all.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_check_all.Location = New System.Drawing.Point(6, 122)
    Me.btn_check_all.Name = "btn_check_all"
    Me.btn_check_all.Size = New System.Drawing.Size(90, 30)
    Me.btn_check_all.TabIndex = 3
    Me.btn_check_all.ToolTipped = False
    Me.btn_check_all.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'dg_sites
    '
    Me.dg_sites.CurrentCol = -1
    Me.dg_sites.CurrentRow = -1
    Me.dg_sites.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_sites.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_sites.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_sites.Location = New System.Drawing.Point(6, 19)
    Me.dg_sites.Name = "dg_sites"
    Me.dg_sites.PanelRightVisible = False
    Me.dg_sites.Redraw = True
    Me.dg_sites.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_sites.Size = New System.Drawing.Size(231, 100)
    Me.dg_sites.Sortable = False
    Me.dg_sites.SortAscending = True
    Me.dg_sites.SortByCol = 0
    Me.dg_sites.TabIndex = 0
    Me.dg_sites.ToolTipped = True
    Me.dg_sites.TopRow = -2
    '
    'uc_sites_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.gb_sites)
    Me.Name = "uc_sites_sel"
    Me.Size = New System.Drawing.Size(245, 168)
    Me.gb_sites.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_sites As System.Windows.Forms.GroupBox
  Friend WithEvents dg_sites As GUI_Controls.uc_grid
  Friend WithEvents btn_uncheck_all As GUI_Controls.uc_button
  Friend WithEvents btn_check_all As GUI_Controls.uc_button

End Class
