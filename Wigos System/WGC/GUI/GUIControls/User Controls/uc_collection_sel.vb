'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_collection_sel
'
' DESCRIPTION:   User control for Import collection sel
'
' AUTHOR:        Alberto Marcos
'
' CREATION DATE: 05-MAY-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 05-MAY-2015  AMF    Initial version
' 30-JUN-2015  FJC    Fixed Bug WIG-2512 (doesn't load collections correctly, when: is in XML and not is in SYSTEM).
' 01-JUL-2015  FJC    Fixed Bug WIG-2518. 
'                     Fixed Bug WIG-2519.
' 01-JUL-2015  SGB    Fixed Bug WIG-2511 Error if you have not stackers in collection
' 15-JUL-2015  AMF    Fixed Bug WIG-2529. Error ChangeTotals
' 17-JUL-2015  AMF    Fixed Bug WIG-2603. Error Check/Uncheck
' 20-JUL-2015  FJC    Fixed Bug WIG-2511.
' 09-JUL-2018  FJC    WIGOS-13464 ID Stacker field not protected when Stacker Collection from file - exception error message
'-------------------------------------------------------------------

#Region " Imports "

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports WSI.Common.CollectionImport

#End Region ' Imports

Public Class uc_collection_sel

#Region " Constants "

  ' Grid Columns
  Private Const GRID_COLUMNS As Integer = 14
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_CHECKED As Integer = 1
  Private Const GRID_COLUMN_STACKER_ID As Integer = 2
  Private Const GRID_COLUMN_FLOOR_ID As Integer = 3
  Private Const GRID_COLUMN_COUNT_BILLS As Integer = 4
  Private Const GRID_COLUMN_COUNT_TICKETS As Integer = 5
  Private Const GRID_COLUMN_SYSTEM_BILLS As Integer = 6
  Private Const GRID_COLUMN_SYSTEM_TICKETS As Integer = 7
  Private Const GRID_COLUMN_DIFF_BILLS As Integer = 8
  Private Const GRID_COLUMN_DIFF_TICKETS As Integer = 9
  Private Const GRID_COLUMN_STACKER_COLLECTION_ID As Integer = 10
  Private Const GRID_COLUMN_TERMINAL_ID As Integer = 11
  Private Const GRID_COLUMN_IMPORT_INDEX As Integer = 12
  Private Const GRID_COLUMN_NON_IMPORT As Integer = 13

  ' SQL Columns
  Private Const SQL_COLUMN_STACKER_ID As Integer = 0
  Private Const SQL_COLUMN_FLOOR_ID As Integer = 1
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 2
  Private Const SQL_COLUMN_COUNT_BILLS As Integer = 3
  Private Const SQL_COLUMN_COUNT_TICKETS As Integer = 4
  Private Const SQL_COLUMN_SYSTEM_BILLS As Integer = 5
  Private Const SQL_COLUMN_SYSTEM_TICKETS As Integer = 6
  Private Const SQL_COLUMN_STACKER_COLLECTION_ID As Integer = 7
  Private Const SQL_COLUMN_NON_IMPORT As Integer = 8
  Private Const SQL_COLUMN_CHECKED As Integer = 9
  Private Const SQL_COLUMN_IMPORT_INDEX As Integer = 10
  Private Const SQL_COLUMN_COLLECTION_STATUS As Integer = 27

  ' Colors
  Private Const COLOR_BALANCED = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_UNBALANCED = ENUM_GUI_COLOR.GUI_COLOR_RED_03
  Private Const COLOR_ONLY_SYSTEM = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
  Private Const COLOR_ONLY_FILE = ENUM_GUI_COLOR.GUI_COLOR_RED_00

  'Counters
  Private Const COUNTER_BALANCED As Int32 = 1
  Private Const COUNTER_UNBALANCED As Int32 = 2
  Private Const COUNTER_ONLY_SYSTEM As Int32 = 3
  Private Const COUNTER_ONLY_FILE As Int32 = 4

#End Region ' Constants

#Region " Members "

  Private m_data_collections As DataTable
  Private m_complete As Boolean = True
  Private m_unbalanced As Boolean = True
  Private m_pending As Boolean = True
  Private m_collected As Boolean = True
  Private m_total_bills As Double
  Private m_total_tickets As Decimal
  Private m_count_id_load As Int32 = 0
  Private m_count_id_ok As Int32 = 0
  Private m_collection_mode As COLLECTION_MODE
  Private m_machine_id_type As ImportId
  Private m_old_value As String
  Private m_new_value As String
  Private m_import_index As Int32
  Private m_raise_cell_data_changed_event As Boolean
  Private m_have_changes As Boolean = False

#End Region ' Members

#Region " Properties "

  Public Property DataCollections() As DataTable
    Get
      Return m_data_collections
    End Get

    Set(ByVal Value As DataTable)
      m_data_collections = Value
    End Set
  End Property ' DataCollections

  Public Property Completed() As Boolean
    Get
      Return m_complete
    End Get

    Set(ByVal Value As Boolean)
      m_complete = Value
    End Set
  End Property ' Completed

  Public Property Unbalanced() As Boolean
    Get
      Return m_unbalanced
    End Get

    Set(ByVal Value As Boolean)
      m_unbalanced = Value
    End Set
  End Property ' Unbalanced

  Public Property Pending() As Boolean
    Get
      Return m_pending
    End Get

    Set(ByVal Value As Boolean)
      m_pending = Value
    End Set
  End Property ' Pending

  Public Property Collected() As Boolean
    Get
      Return m_collected
    End Get

    Set(ByVal Value As Boolean)
      m_collected = Value
    End Set
  End Property ' Collected

  Public Property CountIdLoad() As Int32
    Get
      Return m_count_id_load
    End Get

    Set(ByVal Value As Int32)
      m_count_id_load = Value
    End Set
  End Property ' CountIdLoad

  Public Property CountIdOk() As Int32
    Get
      Return m_count_id_ok
    End Get

    Set(ByVal Value As Int32)
      m_count_id_ok = Value
    End Set
  End Property ' CountIdOk

  Public Property CollectionMode() As COLLECTION_MODE
    Get
      Return m_collection_mode
    End Get

    Set(ByVal Value As COLLECTION_MODE)
      m_collection_mode = Value
    End Set
  End Property ' CollectionMode

  Public Property TotalBills() As Double
    Get
      Return m_total_bills
    End Get

    Set(ByVal Value As Double)
      m_total_bills = Value
    End Set
  End Property ' TotalBills

  Public Property TotalTickets() As Decimal
    Get
      Return m_total_tickets
    End Get

    Set(ByVal Value As Decimal)
      m_total_tickets = Value
    End Set
  End Property ' TotalTickets

  Public Property MachineIdType() As ImportId
    Get
      Return m_machine_id_type
    End Get

    Set(ByVal Value As ImportId)
      m_machine_id_type = Value
    End Set
  End Property ' MachineIdType

  Public Property OldValue() As String
    Get
      Return m_old_value
    End Get

    Set(ByVal Value As String)
      m_old_value = Value
    End Set
  End Property ' OldValue

  Public Property NewValue() As String
    Get
      Return m_new_value
    End Get

    Set(ByVal Value As String)
      m_new_value = Value
    End Set
  End Property ' NewValue

  Public Property ImportIndex() As Int32
    Get
      Return m_import_index
    End Get

    Set(ByVal Value As Int32)
      m_import_index = Value
    End Set
  End Property ' ImportIndex

  Public Property RaiseCellDataChangedEvent() As Boolean
    Get
      Return m_raise_cell_data_changed_event
    End Get

    Set(ByVal Value As Boolean)
      m_raise_cell_data_changed_event = Value
    End Set
  End Property ' RaiseCellDataChangedEvent

  Public Property HaveChanges() As Boolean
    Get
      Return m_have_changes
    End Get

    Set(ByVal Value As Boolean)
      m_have_changes = Value
    End Set
  End Property ' HaveChanges

#End Region ' Properties

#Region " Public Functions "

  ' PURPOSE : Create new instance of user control
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS :
  '
  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControls()

  End Sub ' New

  ' PURPOSE : Set data grid collection info
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS :
  '     - True or False
  '
  Public Function SetDataGridCollections() As Boolean

    Dim _row_index As Integer
    Dim _diff_bills As Double
    Dim _diff_tickets As Double
    Dim _color As ENUM_GUI_COLOR
    Dim _calculate_list_id_load As Boolean
    Dim _collection_id As Int64
    Dim _force_unchecked As Boolean       'Fixed BUG WIG-2511

    _collection_id = 0
    _force_unchecked = False

    Try

      Call RemoveHandlers()

      _row_index = 0
      Me.uc_grid_collections.Clear()
      Me.uc_grid_collections.Redraw = False
      _color = COLOR_BALANCED
      Me.TotalTickets = 0
      Me.TotalBills = 0
      _calculate_list_id_load = CountIdLoad = 0

      For Each _collection As DataRow In DataCollections.Rows

        ' Collection Id for error
        If Not _collection(SQL_COLUMN_STACKER_COLLECTION_ID) Is DBNull.Value Then
          _collection_id = _collection(SQL_COLUMN_STACKER_COLLECTION_ID)
        End If

        ' Calculate diff
        Call CalculateDiff(_collection, _diff_bills, _diff_tickets)

        ' Color
        If Not CalculateColor(_collection, _diff_bills, _diff_tickets, True, _color) Then
          Continue For
        End If

        ' Force unchecked
        _force_unchecked = (Me.m_collection_mode = COLLECTION_MODE.MODE_CANCEL AndAlso _
                            Not _collection(SQL_COLUMN_COLLECTION_STATUS) Is DBNull.Value AndAlso _
                            _collection(SQL_COLUMN_COLLECTION_STATUS) = WSI.Common.TITO_MONEY_COLLECTION_STATUS.PENDING)

        uc_grid_collections.AddRow()

        ' Index
        uc_grid_collections.Cell(_row_index, GRID_COLUMN_INDEX).BackColor = GetColor(_color)

        ' Check
        If _collection(SQL_COLUMN_NON_IMPORT) = STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_XML_IN_DB OrElse _
           _collection(SQL_COLUMN_NON_IMPORT) = STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_DB_IN_XML OrElse _
           _force_unchecked Then
          uc_grid_collections.Cell(_row_index, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
        Else
          uc_grid_collections.Cell(_row_index, GRID_COLUMN_CHECKED).Value = IIf(_collection(SQL_COLUMN_CHECKED), uc_grid.GRID_CHK_CHECKED, uc_grid.GRID_CHK_UNCHECKED)
          If _collection(SQL_COLUMN_CHECKED) AndAlso _calculate_list_id_load Then
            CountIdLoad += 1
          End If
        End If

        ' Stacker Id
        If Not _collection(SQL_COLUMN_STACKER_ID) Is DBNull.Value Then
          uc_grid_collections.Cell(_row_index, GRID_COLUMN_STACKER_ID).Value = GUI_FormatNumber(_collection(SQL_COLUMN_STACKER_ID), 0)
        End If

        ' Floor Id
        If Not _collection(SQL_COLUMN_FLOOR_ID) Is DBNull.Value Then
          uc_grid_collections.Cell(_row_index, GRID_COLUMN_FLOOR_ID).Value = _collection(SQL_COLUMN_FLOOR_ID)
        End If

        ' Count bills
        If _collection(SQL_COLUMN_NON_IMPORT) <> STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_XML_IN_DB Then
          uc_grid_collections.Cell(_row_index, GRID_COLUMN_COUNT_BILLS).Value = GUI_FormatCurrency(_collection(SQL_COLUMN_COUNT_BILLS))
          If Not _force_unchecked Then
            Me.TotalBills += IIf(_collection(SQL_COLUMN_CHECKED), _collection(SQL_COLUMN_COUNT_BILLS), 0)
          End If
        End If

        ' Count tickets
        If _collection(SQL_COLUMN_NON_IMPORT) <> STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_XML_IN_DB Then
          uc_grid_collections.Cell(_row_index, GRID_COLUMN_COUNT_TICKETS).Value = GUI_FormatCurrency(_collection(SQL_COLUMN_COUNT_TICKETS))
          If Not _force_unchecked Then
            Me.TotalTickets += IIf(_collection(SQL_COLUMN_CHECKED), _collection(SQL_COLUMN_COUNT_TICKETS), 0)
          End If
        End If

        ' System bills
        If _collection(SQL_COLUMN_NON_IMPORT) <> STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_DB_IN_XML Then
          uc_grid_collections.Cell(_row_index, GRID_COLUMN_SYSTEM_BILLS).Value = GUI_FormatCurrency(_collection(SQL_COLUMN_SYSTEM_BILLS))
        End If

        ' System tickets
        If _collection(SQL_COLUMN_NON_IMPORT) <> STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_DB_IN_XML Then
          uc_grid_collections.Cell(_row_index, GRID_COLUMN_SYSTEM_TICKETS).Value = GUI_FormatCurrency(_collection(SQL_COLUMN_SYSTEM_TICKETS))
        End If

        ' Diff bills
        uc_grid_collections.Cell(_row_index, GRID_COLUMN_DIFF_BILLS).Value = GUI_FormatCurrency(_diff_bills)

        ' Diff tickets
        uc_grid_collections.Cell(_row_index, GRID_COLUMN_DIFF_TICKETS).Value = GUI_FormatCurrency(_diff_tickets)

        ' Collection Id
        If Not _collection(SQL_COLUMN_STACKER_COLLECTION_ID) Is DBNull.Value Then
          uc_grid_collections.Cell(_row_index, GRID_COLUMN_STACKER_COLLECTION_ID).Value = _collection(SQL_COLUMN_STACKER_COLLECTION_ID)
        End If

        ' Terminal Id
        If Not _collection(SQL_COLUMN_TERMINAL_ID) Is DBNull.Value Then
          uc_grid_collections.Cell(_row_index, GRID_COLUMN_TERMINAL_ID).Value = _collection(SQL_COLUMN_TERMINAL_ID)
        End If

        ' Import Index
        If Not _collection(SQL_COLUMN_IMPORT_INDEX) Is DBNull.Value Then
          uc_grid_collections.Cell(_row_index, GRID_COLUMN_IMPORT_INDEX).Value = _collection(SQL_COLUMN_IMPORT_INDEX)
        End If

        ' Non Import
        If Not _collection(SQL_COLUMN_NON_IMPORT) Is DBNull.Value Then
          uc_grid_collections.Cell(_row_index, GRID_COLUMN_NON_IMPORT).Value = _collection(SQL_COLUMN_NON_IMPORT)
        End If

        _row_index += 1

      Next

      Me.uc_grid_collections.SelectFirstRow(True)

      Call uc_grid_collections_RowSelectedEvent(0)

      Me.uc_grid_collections.Redraw = True

      Call AddHandlers()

      Return True

    Catch _ex As Exception
      WSI.Common.Log.Message("Error in Money Collection Id: " + _collection_id.ToString())
      WSI.Common.Log.Exception(_ex)
    End Try

    Return False

  End Function ' SetDataGridCollections

  ' PURPOSE : Set change data grid collection info
  '
  '  PARAMS:
  '     - INPUT:
  '         - ChangeDataCollections
  '
  '     - OUTPUT:
  '
  ' RETURNS :
  '     - True or False
  '
  Public Function SetChangeDataGridCollections(ByVal ChangeDataCollections As DataTable) As Boolean

    Dim _diff_bills As Double
    Dim _diff_tickets As Double
    Dim _color As ENUM_GUI_COLOR

    Try

      Call RemoveHandlers()

      Me.uc_grid_collections.Redraw = False

      _color = COLOR_BALANCED
      _diff_bills = 0
      _diff_tickets = 0

      Me.HaveChanges = True

      For Each _change As DataRow In ChangeDataCollections.Rows

        For _idx As Integer = 0 To Me.uc_grid_collections.NumRows - 1

          If Me.uc_grid_collections.Cell(_idx, GRID_COLUMN_IMPORT_INDEX).Value = _change(SQL_COLUMN_IMPORT_INDEX) Then

            ' Calculate diff
            Call CalculateDiff(_change, _diff_bills, _diff_tickets)

            ' Color
            If Not CalculateColor(_change, _diff_bills, _diff_tickets, False, _color) Then
              Continue For
            End If

            ' Counters
            Select Case uc_grid_collections.Cell(_idx, GRID_COLUMN_NON_IMPORT).Value

              Case STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_OK
                If (uc_grid_collections.Cell(_idx, GRID_COLUMN_DIFF_BILLS).Value = 0 AndAlso uc_grid_collections.Cell(_idx, GRID_COLUMN_DIFF_BILLS).Value = 0) Then
                  Me.uc_grid_collections.Counter(COUNTER_BALANCED).Value -= 1
                Else
                  Me.uc_grid_collections.Counter(COUNTER_UNBALANCED).Value -= 1
                End If

              Case STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_XML_IN_DB
                Me.uc_grid_collections.Counter(COUNTER_ONLY_SYSTEM).Value -= 1

              Case STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_DB_IN_XML
                Me.uc_grid_collections.Counter(COUNTER_ONLY_FILE).Value -= 1

            End Select

            ' Index
            uc_grid_collections.Cell(_idx, GRID_COLUMN_INDEX).BackColor = GetColor(_color)

            ' Check
            ' Only System Or Only File --> Balanced Or Unbalanced
            If uc_grid_collections.Cell(_idx, GRID_COLUMN_NON_IMPORT).Value <> STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_OK AndAlso _
               _change(SQL_COLUMN_NON_IMPORT) = STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_OK Then
              uc_grid_collections.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
              Me.TotalBills += _change(SQL_COLUMN_COUNT_BILLS)
              Me.TotalTickets += _change(SQL_COLUMN_COUNT_TICKETS)
            End If
            ' Balanced Or Unbalanced --> Only System Or Only File
            If uc_grid_collections.Cell(_idx, GRID_COLUMN_NON_IMPORT).Value = STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_OK AndAlso _
              _change(SQL_COLUMN_NON_IMPORT) <> STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_OK Then
              uc_grid_collections.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
              Me.TotalBills -= _change(SQL_COLUMN_COUNT_BILLS)
              Me.TotalTickets -= _change(SQL_COLUMN_COUNT_TICKETS)
            End If

            ' Stacker Id
            If Not _change(SQL_COLUMN_STACKER_ID) Is DBNull.Value Then
              uc_grid_collections.Cell(_idx, GRID_COLUMN_STACKER_ID).Value = GUI_FormatNumber(_change(SQL_COLUMN_STACKER_ID), 0)
            Else
              uc_grid_collections.Cell(_idx, GRID_COLUMN_STACKER_ID).Value = String.Empty
            End If

            ' Floor Id
            If Not _change(SQL_COLUMN_FLOOR_ID) Is DBNull.Value Then
              uc_grid_collections.Cell(_idx, GRID_COLUMN_FLOOR_ID).Value = _change(SQL_COLUMN_FLOOR_ID)
            Else
              uc_grid_collections.Cell(_idx, GRID_COLUMN_FLOOR_ID).Value = String.Empty
            End If

            ' System bills
            If _change(SQL_COLUMN_NON_IMPORT) <> STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_DB_IN_XML Then
              uc_grid_collections.Cell(_idx, GRID_COLUMN_SYSTEM_BILLS).Value = GUI_FormatCurrency(_change(SQL_COLUMN_SYSTEM_BILLS))
            Else
              uc_grid_collections.Cell(_idx, GRID_COLUMN_SYSTEM_BILLS).Value = String.Empty
            End If

            ' System tickets
            If _change(SQL_COLUMN_NON_IMPORT) <> STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_DB_IN_XML Then
              uc_grid_collections.Cell(_idx, GRID_COLUMN_SYSTEM_TICKETS).Value = GUI_FormatCurrency(_change(SQL_COLUMN_SYSTEM_TICKETS))
            Else
              uc_grid_collections.Cell(_idx, GRID_COLUMN_SYSTEM_TICKETS).Value = String.Empty
            End If

            ' Diff bills
            uc_grid_collections.Cell(_idx, GRID_COLUMN_DIFF_BILLS).Value = GUI_FormatCurrency(_diff_bills)

            ' Diff tickets
            uc_grid_collections.Cell(_idx, GRID_COLUMN_DIFF_TICKETS).Value = GUI_FormatCurrency(_diff_tickets)

            ' Collection Id
            If Not _change(SQL_COLUMN_STACKER_COLLECTION_ID) Is DBNull.Value Then
              uc_grid_collections.Cell(_idx, GRID_COLUMN_STACKER_COLLECTION_ID).Value = _change(SQL_COLUMN_STACKER_COLLECTION_ID)
            Else
              uc_grid_collections.Cell(_idx, GRID_COLUMN_STACKER_COLLECTION_ID).Value = String.Empty
            End If

            ' Terminal Id
            If Not _change(SQL_COLUMN_TERMINAL_ID) Is DBNull.Value Then
              uc_grid_collections.Cell(_idx, GRID_COLUMN_TERMINAL_ID).Value = _change(SQL_COLUMN_TERMINAL_ID)
            Else
              uc_grid_collections.Cell(_idx, GRID_COLUMN_TERMINAL_ID).Value = String.Empty
            End If

            ' Non Import
            uc_grid_collections.Cell(_idx, GRID_COLUMN_NON_IMPORT).Value = _change(SQL_COLUMN_NON_IMPORT)

            ' Delete row
            If _change(SQL_COLUMN_NON_IMPORT) = STATUS_DATA_CENTRAL.STATUS_DELETED_ROW Then
              Me.uc_grid_collections.DeleteRowFast(_idx)
            End If

            Exit For

          End If

        Next

      Next

      Me.uc_grid_collections.Redraw = True

      RaiseEvent ChangeTotals()

      Call AddHandlers()

      Return True

    Catch _ex As Exception

      WSI.Common.Log.Exception(_ex)
    End Try

    Return False

  End Function ' SetChangeDataGridCollections

  ' PURPOSE : Get data grid collection selected
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '         - List(Of Int64)
  '
  ' RETURNS :
  '     - True or False
  '
  Public Function GetDataGridCollections(ByRef CollectionIdList As List(Of Int64)) As Boolean

    Try

      CollectionIdList = New List(Of Int64)
      CountIdOk = 0

      For _idx As Integer = 0 To Me.uc_grid_collections.NumRows - 1

        If Me.uc_grid_collections.Cell(_idx, GRID_COLUMN_NON_IMPORT).Value = STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_OK Then
          CountIdOk += 1
        End If

        If Me.uc_grid_collections.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
          CollectionIdList.Add(Me.uc_grid_collections.Cell(_idx, GRID_COLUMN_STACKER_COLLECTION_ID).Value)
        End If

      Next

      Return True

    Catch _ex As Exception

      WSI.Common.Log.Exception(_ex)
    End Try

    Return False

  End Function ' GetDataGridCollections

  ' PURPOSE : Get data info for search detail
  '
  '  PARAMS:
  '     - INPUT:
  '         - SelectedRow
  '
  '     - OUTPUT:
  '         - SOFT_COUNT_DATA_DETAIL
  '
  ' RETURNS :
  '     - True or False
  '
  Public Function GetSearchDetail(ByVal SelectedRow As Integer, ByRef SoftCountDataDetail As SofCountData.SOFT_COUNT_DATA_DETAIL) As Boolean

    Try

      If Me.uc_grid_collections.NumRows <> 0 Then
        If Me.uc_grid_collections.Cell(SelectedRow, GRID_COLUMN_STACKER_ID).Value <> "" Then
          SoftCountDataDetail.StackerId = Me.uc_grid_collections.Cell(SelectedRow, GRID_COLUMN_STACKER_ID).Value
        End If

        If Me.uc_grid_collections.Cell(SelectedRow, GRID_COLUMN_TERMINAL_ID).Value <> "" Then
          SoftCountDataDetail.TerminalId = Me.uc_grid_collections.Cell(SelectedRow, GRID_COLUMN_TERMINAL_ID).Value
        End If

        If Me.uc_grid_collections.Cell(SelectedRow, GRID_COLUMN_STACKER_COLLECTION_ID).Value <> "" Then
          SoftCountDataDetail.CollectionId = Me.uc_grid_collections.Cell(SelectedRow, GRID_COLUMN_STACKER_COLLECTION_ID).Value
        End If

        SoftCountDataDetail.ImportIndex = Me.uc_grid_collections.Cell(SelectedRow, GRID_COLUMN_IMPORT_INDEX).Value

        Return True

      End If

    Catch _ex As Exception

      WSI.Common.Log.Exception(_ex)
    End Try

    Return False

  End Function ' GetSearchDetail

  ' PURPOSE : Grid key pressed
  '
  '  PARAMS:
  '     - INPUT:
  '         - sender
  '         - e
  '     - OUTPUT:
  '
  ' RETURNS :
  '
  Public Sub CollectionKeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

    Me.uc_grid_collections.KeyPressed(sender, e)

  End Sub ' CollectionKeyPress

  ' PURPOSE : Load old value
  '
  '  PARAMS:
  '     - INPUT:
  '         - Row
  '         - Column
  '     - OUTPUT:
  '
  ' RETURNS :
  '
  Public Sub LoadOldValue(ByVal Row As Integer, ByVal Column As Integer)

    Call RemoveHandlers()

    If Column = GRID_COLUMN_STACKER_ID Then
      If String.IsNullOrEmpty(OldValue) Then
        uc_grid_collections.Cell(Row, GRID_COLUMN_STACKER_ID).Value = String.Empty
      Else
        uc_grid_collections.Cell(Row, GRID_COLUMN_STACKER_ID).Value = GUI_FormatNumber(OldValue, 0)
      End If

    ElseIf Column = GRID_COLUMN_FLOOR_ID Then
      uc_grid_collections.Cell(Row, GRID_COLUMN_FLOOR_ID).Value = OldValue

    ElseIf Column = GRID_COLUMN_FLOOR_ID Then
      If String.IsNullOrEmpty(OldValue) Then
        uc_grid_collections.Cell(Row, GRID_COLUMN_FLOOR_ID).Value = String.Empty
      Else
        uc_grid_collections.Cell(Row, GRID_COLUMN_FLOOR_ID).Value = OldValue
      End If
    End If

    Call AddHandlers()

  End Sub ' LoadOldValue

#End Region ' Public Functions

#Region " Public Events "

  Public Event RowSelectedEvent(ByVal SelectedRow As Integer)
  Public Event ChangeTotals()
  Public Event CollectionDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer)

#End Region ' Public Events

#Region " Private Functions "

  ' PURPOSE : Initialize controls
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS :
  '
  Private Sub InitControls()

    ' Data Info
    m_data_collections = New DataTable()

    Call RemoveHandlers()

    ' Format entry fields
    GUI_StyleSheet()

  End Sub ' InitControls

  ' PURPOSE : Define Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS :
  '
  Private Sub GUI_StyleSheet()

    With Me.uc_grid_collections

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Completed
      .Counter(COUNTER_BALANCED).Visible = True
      .Counter(COUNTER_BALANCED).BackColor = GetColor(COLOR_BALANCED)
      .Counter(COUNTER_BALANCED).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      ' Unbalanced
      .Counter(COUNTER_UNBALANCED).Visible = True
      .Counter(COUNTER_UNBALANCED).BackColor = GetColor(COLOR_UNBALANCED)
      .Counter(COUNTER_UNBALANCED).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      ' Pending
      .Counter(COUNTER_ONLY_SYSTEM).Visible = True
      .Counter(COUNTER_ONLY_SYSTEM).BackColor = GetColor(COLOR_ONLY_SYSTEM)
      .Counter(COUNTER_ONLY_SYSTEM).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      ' Collected
      .Counter(COUNTER_ONLY_FILE).Visible = True
      .Counter(COUNTER_ONLY_FILE).BackColor = GetColor(COLOR_ONLY_FILE)
      .Counter(COUNTER_ONLY_FILE).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = "  "
      .Column(GRID_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_COLUMN_INDEX).Width = 150
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False
      .Column(GRID_COLUMN_INDEX).Editable = False

      ' Check
      .Column(GRID_COLUMN_CHECKED).Header(0).Text = "  "
      .Column(GRID_COLUMN_CHECKED).Header(1).Text = "  "
      .Column(GRID_COLUMN_CHECKED).Width = 350
      .Column(GRID_COLUMN_CHECKED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_CHECKED).EditionControl.Type = CONTROL_TYPE_CHECK_BOX
      .Column(GRID_COLUMN_CHECKED).Editable = True

      ' Stacker Id
      .Column(GRID_COLUMN_STACKER_ID).Header(0).Text = "  "
      .Column(GRID_COLUMN_STACKER_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2252)
      .Column(GRID_COLUMN_STACKER_ID).Width = 1100
      .Column(GRID_COLUMN_STACKER_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_STACKER_ID).HighLightWhenSelected = False
      If MachineIdType = ImportId.StackerId Then
        .Column(GRID_COLUMN_STACKER_ID).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_STACKER_ID).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 7, 0)
        .Column(GRID_COLUMN_STACKER_ID).Editable = WSI.Common.Misc.ReadGeneralParams("Cage", "IsStackerIdEditable")
      End If

      ' Floor Id
      .Column(GRID_COLUMN_FLOOR_ID).Header(0).Text = "  "
      .Column(GRID_COLUMN_FLOOR_ID).Header(1).Text = GLB_NLS_GUI_CONFIGURATION.GetString(432)
      .Column(GRID_COLUMN_FLOOR_ID).Width = 1300
      .Column(GRID_COLUMN_FLOOR_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_FLOOR_ID).HighLightWhenSelected = False
      If MachineIdType = ImportId.EGMFloorId OrElse MachineIdType = ImportId.EGMBaseName Then
        .Column(GRID_COLUMN_FLOOR_ID).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_FLOOR_ID).EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 6)
        .Column(GRID_COLUMN_FLOOR_ID).Editable = True
      End If

      ' Count Bills
      .Column(GRID_COLUMN_COUNT_BILLS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5011)
      .Column(GRID_COLUMN_COUNT_BILLS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2266)
      .Column(GRID_COLUMN_COUNT_BILLS).Width = 1200
      .Column(GRID_COLUMN_COUNT_BILLS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Count Tickets
      .Column(GRID_COLUMN_COUNT_TICKETS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5011)
      .Column(GRID_COLUMN_COUNT_TICKETS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1825)
      .Column(GRID_COLUMN_COUNT_TICKETS).Width = 1200
      .Column(GRID_COLUMN_COUNT_TICKETS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' System Bills
      .Column(GRID_COLUMN_SYSTEM_BILLS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5010)
      .Column(GRID_COLUMN_SYSTEM_BILLS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2266)
      .Column(GRID_COLUMN_SYSTEM_BILLS).Width = 1200
      .Column(GRID_COLUMN_SYSTEM_BILLS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' System Tickets
      .Column(GRID_COLUMN_SYSTEM_TICKETS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5010)
      .Column(GRID_COLUMN_SYSTEM_TICKETS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1825)
      .Column(GRID_COLUMN_SYSTEM_TICKETS).Width = 1200
      .Column(GRID_COLUMN_SYSTEM_TICKETS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Diff Bills
      .Column(GRID_COLUMN_DIFF_BILLS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2610)
      .Column(GRID_COLUMN_DIFF_BILLS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2266)
      .Column(GRID_COLUMN_DIFF_BILLS).Width = 1200
      .Column(GRID_COLUMN_DIFF_BILLS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Diff Tickets
      .Column(GRID_COLUMN_DIFF_TICKETS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2610)
      .Column(GRID_COLUMN_DIFF_TICKETS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1825)
      .Column(GRID_COLUMN_DIFF_TICKETS).Width = 1200
      .Column(GRID_COLUMN_DIFF_TICKETS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Stacker collection Id
      .Column(GRID_COLUMN_STACKER_COLLECTION_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_STACKER_COLLECTION_ID).Header(1).Text = "Collection Id"
      .Column(GRID_COLUMN_STACKER_COLLECTION_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
#If DEBUG Then
      .Column(GRID_COLUMN_STACKER_COLLECTION_ID).Width = 1300
#Else
      .Column(GRID_COLUMN_STACKER_COLLECTION_ID).Width = 0
#End If

      ' Stacker collection Id
      .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_TERMINAL_ID).Header(1).Text = "Terminal Id"
      .Column(GRID_COLUMN_TERMINAL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
#If DEBUG Then
      .Column(GRID_COLUMN_TERMINAL_ID).Width = 1300
#Else
      .Column(GRID_COLUMN_TERMINAL_ID).Width = 0
#End If

      ' Import Index
      .Column(GRID_COLUMN_IMPORT_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_IMPORT_INDEX).Header(1).Text = "Import Index"
      .Column(GRID_COLUMN_IMPORT_INDEX).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
#If DEBUG Then
      .Column(GRID_COLUMN_IMPORT_INDEX).Width = 1500
#Else
      .Column(GRID_COLUMN_IMPORT_INDEX).Width = 0
#End If

      ' Non Import
      .Column(GRID_COLUMN_NON_IMPORT).Header(0).Text = ""
      .Column(GRID_COLUMN_NON_IMPORT).Header(1).Text = "Non Import"
      .Column(GRID_COLUMN_NON_IMPORT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
#If DEBUG Then
      .Column(GRID_COLUMN_NON_IMPORT).Width = 1500
#Else
      .Column(GRID_COLUMN_NON_IMPORT).Width = 0
#End If

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE : Update checked datatable DataCollections
  '
  '  PARAMS:
  '     - INPUT: 
  '         - ImportIndex
  '     - OUTPUT:
  '
  ' RETURNS :
  '
  Private Sub UpdateDataCollectionsChecked(ByVal ImportIndex As Int32, ByVal CheckValue As Boolean)

    Dim _row As DataRow

    _row = DataCollections.Select("IMPORT_INDEX =  " + ImportIndex.ToString())(0)

    _row(SQL_COLUMN_CHECKED) = CheckValue

  End Sub ' UpdateDataCollectionsChecked

  ' PURPOSE : remove handler uc_grid
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS :
  '
  Private Sub RemoveHandlers()

    RemoveHandler uc_grid_collections.RowSelectedEvent, AddressOf uc_grid_collections_RowSelectedEvent
    RemoveHandler uc_grid_collections.CellDataChangedEvent, AddressOf uc_grid_collections_CellDataChangedEvent

  End Sub ' RemoveHandlers

  ' PURPOSE : Add handler uc_grid
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT:
  '
  ' RETURNS :
  '
  Private Sub AddHandlers()

    AddHandler uc_grid_collections.RowSelectedEvent, AddressOf uc_grid_collections_RowSelectedEvent
    AddHandler uc_grid_collections.CellDataChangedEvent, AddressOf uc_grid_collections_CellDataChangedEvent

  End Sub ' AddHandlers

  ' PURPOSE : Calculate diff bills and tickets
  '
  '  PARAMS:
  '     - INPUT: 
  '         - Row
  '
  '     - OUTPUT:
  '         - DiffBills
  '         - DiffTickets
  '
  ' RETURNS :
  '
  Private Sub CalculateDiff(ByVal Row As DataRow, ByRef DiffBills As Double, ByRef DiffTickets As Double)
    ' Calculate diff
    If Row(SQL_COLUMN_COUNT_BILLS) Is DBNull.Value Then
      DiffBills = -Row(SQL_COLUMN_SYSTEM_BILLS)
    ElseIf Row(SQL_COLUMN_SYSTEM_BILLS) Is DBNull.Value Then
      DiffBills = Row(SQL_COLUMN_COUNT_BILLS)
    Else
      DiffBills = Row(SQL_COLUMN_COUNT_BILLS) - Row(SQL_COLUMN_SYSTEM_BILLS)
    End If

    If Row(SQL_COLUMN_COUNT_TICKETS) Is DBNull.Value Then
      DiffTickets = -Row(SQL_COLUMN_SYSTEM_TICKETS)
    ElseIf Row(SQL_COLUMN_SYSTEM_TICKETS) Is DBNull.Value Then
      DiffTickets = Row(SQL_COLUMN_COUNT_TICKETS)
    Else
      DiffTickets = Row(SQL_COLUMN_COUNT_TICKETS) - Row(SQL_COLUMN_SYSTEM_TICKETS)
    End If

  End Sub ' CalculateDiff

  ' PURPOSE : Calculate color
  '
  '  PARAMS:
  '     - INPUT: 
  '         - Row
  '         - DiffBills
  '         - DiffTickets
  '         - CheckStatus
  '
  '     - OUTPUT:
  '         - Color
  '
  ' RETURNS :
  '     True  : Ok
  '     False : Continue For
  '
  Private Function CalculateColor(ByVal Row As DataRow, ByVal DiffBills As Double, ByVal DiffTickets As Double, ByVal CheckStatus As Boolean, ByRef Color As ENUM_GUI_COLOR) As Boolean

    Select Case Row(SQL_COLUMN_NON_IMPORT)

      Case STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_OK
        If (DiffBills = 0 AndAlso DiffTickets = 0) Then
          If Not Completed AndAlso CheckStatus Then
            Return False
          End If
          Color = COLOR_BALANCED
          Me.uc_grid_collections.Counter(COUNTER_BALANCED).Value += 1
        Else
          If Not Unbalanced AndAlso CheckStatus Then
            Return False
          End If
          Color = COLOR_UNBALANCED
          Me.uc_grid_collections.Counter(COUNTER_UNBALANCED).Value += 1
        End If

      Case STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_XML_IN_DB
        If Not Pending AndAlso CheckStatus Then
          Return False
        End If
        Color = COLOR_ONLY_SYSTEM
        Me.uc_grid_collections.Counter(COUNTER_ONLY_SYSTEM).Value += 1

      Case STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_DB_IN_XML
        If Not Collected AndAlso CheckStatus Then
          Return False
        End If
        Color = COLOR_ONLY_FILE
        Me.uc_grid_collections.Counter(COUNTER_ONLY_FILE).Value += 1

    End Select

    Return True

  End Function ' CalculateColor

#End Region ' Private Functions

#Region " Private Events "

  ' PURPOSE : row selected event 
  '
  '  PARAMS:
  '     - INPUT:
  '         - SelectedRow
  '     - OUTPUT:
  '
  ' RETURNS :
  '
  Private Sub uc_grid_collections_RowSelectedEvent(ByVal SelectedRow As Integer) 'Handles uc_grid_collections.RowSelectedEvent

    RaiseEvent RowSelectedEvent(SelectedRow)

  End Sub ' uc_grid_collections_RowSelectedEvent

  ' PURPOSE : Before start edition event
  '
  '  PARAMS:
  '     - INPUT:
  '         - Row
  '         - Column
  '     - OUTPUT:
  '         - EditionCanceled
  '
  ' RETURNS :
  '
  Private Sub uc_grid_collections_BeforeStartEditionEvent(ByVal Row As Integer, ByVal Column As Integer, ByRef EditionCanceled As Boolean) Handles uc_grid_collections.BeforeStartEditionEvent

    If m_collection_mode = COLLECTION_MODE.MODE_CANCEL Then
      EditionCanceled = True

    ElseIf Column = GRID_COLUMN_CHECKED Then
      If uc_grid_collections.Cell(Row, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED Then
        EditionCanceled = True
      End If

    ElseIf Column = GRID_COLUMN_STACKER_ID Then
      If uc_grid_collections.Cell(Row, GRID_COLUMN_NON_IMPORT).Value = STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_XML_IN_DB Then
        EditionCanceled = True
      Else
        OldValue = uc_grid_collections.Cell(Row, GRID_COLUMN_STACKER_ID).Value
        RaiseCellDataChangedEvent = True
      End If

    ElseIf Column = GRID_COLUMN_FLOOR_ID Then
      If uc_grid_collections.Cell(Row, GRID_COLUMN_NON_IMPORT).Value = STATUS_DATA_CENTRAL.STATUS_DATA_CENTRAL_NOT_IN_XML_IN_DB Then
        EditionCanceled = True
      Else
        OldValue = uc_grid_collections.Cell(Row, GRID_COLUMN_FLOOR_ID).Value
        RaiseCellDataChangedEvent = True
      End If

    End If

  End Sub ' uc_grid_collections_BeforeStartEditionEvent

  ' PURPOSE : Cell data change event
  '
  '  PARAMS:
  '     - INPUT:
  '         - Row
  '         - Column
  '     - OUTPUT:
  '
  ' RETURNS :
  '
  Private Sub uc_grid_collections_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles uc_grid_collections.CellDataChangedEvent
    Select Case Column
      Case GRID_COLUMN_STACKER_ID
        If String.IsNullOrEmpty(uc_grid_collections.Cell(Row, GRID_COLUMN_STACKER_ID).Value) Then
          Me.LoadOldValue(Row, Column)

          Return
        End If
        NewValue = GUI_FormatNumber(uc_grid_collections.Cell(Row, GRID_COLUMN_STACKER_ID).Value, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE).ToString()
      Case GRID_COLUMN_FLOOR_ID
        NewValue = uc_grid_collections.Cell(Row, GRID_COLUMN_FLOOR_ID).Value
      Case GRID_COLUMN_CHECKED

        Select Case uc_grid_collections.Cell(Row, GRID_COLUMN_CHECKED).Value

          Case uc_grid.GRID_CHK_UNCHECKED
            Me.TotalBills -= uc_grid_collections.Cell(Row, GRID_COLUMN_COUNT_BILLS).Value
            Me.TotalTickets -= uc_grid_collections.Cell(Row, GRID_COLUMN_COUNT_TICKETS).Value
            Call Me.UpdateDataCollectionsChecked(uc_grid_collections.Cell(Row, GRID_COLUMN_IMPORT_INDEX).Value, False)

            RaiseEvent ChangeTotals()

          Case uc_grid.GRID_CHK_CHECKED
            Me.TotalBills += uc_grid_collections.Cell(Row, GRID_COLUMN_COUNT_BILLS).Value
            Me.TotalTickets += uc_grid_collections.Cell(Row, GRID_COLUMN_COUNT_TICKETS).Value
            Call Me.UpdateDataCollectionsChecked(uc_grid_collections.Cell(Row, GRID_COLUMN_IMPORT_INDEX).Value, True)

            RaiseEvent ChangeTotals()

        End Select

        Exit Sub
      Case Else

        Exit Sub
    End Select

    ImportIndex = uc_grid_collections.Cell(Row, GRID_COLUMN_IMPORT_INDEX).Value

    RaiseEvent CollectionDataChangedEvent(Row, Column)
    RaiseCellDataChangedEvent = False

  End Sub ' uc_grid_collections_CellDataChangedEvent

#End Region ' Private Events

End Class
