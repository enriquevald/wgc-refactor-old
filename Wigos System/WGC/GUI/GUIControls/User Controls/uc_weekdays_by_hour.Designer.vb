﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_weekdays_by_hour
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.dtp_working_to_sunday = New GUI_Controls.uc_date_picker()
    Me.dtp_working_to_saturday = New GUI_Controls.uc_date_picker()
    Me.dtp_working_to_friday = New GUI_Controls.uc_date_picker()
    Me.dtp_working_to_thursday = New GUI_Controls.uc_date_picker()
    Me.dtp_working_to_wednesday = New GUI_Controls.uc_date_picker()
    Me.dtp_working_to_tuesday = New GUI_Controls.uc_date_picker()
    Me.dtp_working_to_monday = New GUI_Controls.uc_date_picker()
    Me.dtp_working_from_sunday = New GUI_Controls.uc_date_picker()
    Me.dtp_working_from_saturday = New GUI_Controls.uc_date_picker()
    Me.dtp_working_from_friday = New GUI_Controls.uc_date_picker()
    Me.dtp_working_from_thursday = New GUI_Controls.uc_date_picker()
    Me.dtp_working_from_wednesday = New GUI_Controls.uc_date_picker()
    Me.dtp_working_from_tuesday = New GUI_Controls.uc_date_picker()
    Me.dtp_working_from_monday = New GUI_Controls.uc_date_picker()
    Me.uc_days = New GUI_Controls.uc_week_days()
    Me.SuspendLayout()
    '
    'dtp_working_to_sunday
    '
    Me.dtp_working_to_sunday.CausesValidation = False
    Me.dtp_working_to_sunday.Checked = False
    Me.dtp_working_to_sunday.IsReadOnly = False
    Me.dtp_working_to_sunday.Location = New System.Drawing.Point(252, 154)
    Me.dtp_working_to_sunday.Name = "dtp_working_to_sunday"
    Me.dtp_working_to_sunday.ShowCheckBox = False
    Me.dtp_working_to_sunday.ShowUpDown = True
    Me.dtp_working_to_sunday.Size = New System.Drawing.Size(138, 24)
    Me.dtp_working_to_sunday.SufixText = "Sufix Text"
    Me.dtp_working_to_sunday.SufixTextVisible = True
    Me.dtp_working_to_sunday.TabIndex = 14
    Me.dtp_working_to_sunday.TextWidth = 50
    Me.dtp_working_to_sunday.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_working_to_saturday
    '
    Me.dtp_working_to_saturday.CausesValidation = False
    Me.dtp_working_to_saturday.Checked = False
    Me.dtp_working_to_saturday.IsReadOnly = False
    Me.dtp_working_to_saturday.Location = New System.Drawing.Point(252, 129)
    Me.dtp_working_to_saturday.Name = "dtp_working_to_saturday"
    Me.dtp_working_to_saturday.ShowCheckBox = False
    Me.dtp_working_to_saturday.ShowUpDown = True
    Me.dtp_working_to_saturday.Size = New System.Drawing.Size(138, 24)
    Me.dtp_working_to_saturday.SufixText = "Sufix Text"
    Me.dtp_working_to_saturday.SufixTextVisible = True
    Me.dtp_working_to_saturday.TabIndex = 12
    Me.dtp_working_to_saturday.TextWidth = 50
    Me.dtp_working_to_saturday.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_working_to_friday
    '
    Me.dtp_working_to_friday.CausesValidation = False
    Me.dtp_working_to_friday.Checked = False
    Me.dtp_working_to_friday.IsReadOnly = False
    Me.dtp_working_to_friday.Location = New System.Drawing.Point(252, 104)
    Me.dtp_working_to_friday.Name = "dtp_working_to_friday"
    Me.dtp_working_to_friday.ShowCheckBox = False
    Me.dtp_working_to_friday.ShowUpDown = True
    Me.dtp_working_to_friday.Size = New System.Drawing.Size(138, 24)
    Me.dtp_working_to_friday.SufixText = "Sufix Text"
    Me.dtp_working_to_friday.SufixTextVisible = True
    Me.dtp_working_to_friday.TabIndex = 10
    Me.dtp_working_to_friday.TextWidth = 50
    Me.dtp_working_to_friday.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_working_to_thursday
    '
    Me.dtp_working_to_thursday.CausesValidation = False
    Me.dtp_working_to_thursday.Checked = False
    Me.dtp_working_to_thursday.IsReadOnly = False
    Me.dtp_working_to_thursday.Location = New System.Drawing.Point(252, 79)
    Me.dtp_working_to_thursday.Name = "dtp_working_to_thursday"
    Me.dtp_working_to_thursday.ShowCheckBox = False
    Me.dtp_working_to_thursday.ShowUpDown = True
    Me.dtp_working_to_thursday.Size = New System.Drawing.Size(138, 24)
    Me.dtp_working_to_thursday.SufixText = "Sufix Text"
    Me.dtp_working_to_thursday.SufixTextVisible = True
    Me.dtp_working_to_thursday.TabIndex = 8
    Me.dtp_working_to_thursday.TextWidth = 50
    Me.dtp_working_to_thursday.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_working_to_wednesday
    '
    Me.dtp_working_to_wednesday.CausesValidation = False
    Me.dtp_working_to_wednesday.Checked = False
    Me.dtp_working_to_wednesday.IsReadOnly = False
    Me.dtp_working_to_wednesday.Location = New System.Drawing.Point(252, 54)
    Me.dtp_working_to_wednesday.Name = "dtp_working_to_wednesday"
    Me.dtp_working_to_wednesday.ShowCheckBox = False
    Me.dtp_working_to_wednesday.ShowUpDown = True
    Me.dtp_working_to_wednesday.Size = New System.Drawing.Size(138, 24)
    Me.dtp_working_to_wednesday.SufixText = "Sufix Text"
    Me.dtp_working_to_wednesday.SufixTextVisible = True
    Me.dtp_working_to_wednesday.TabIndex = 6
    Me.dtp_working_to_wednesday.TextWidth = 50
    Me.dtp_working_to_wednesday.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_working_to_tuesday
    '
    Me.dtp_working_to_tuesday.CausesValidation = False
    Me.dtp_working_to_tuesday.Checked = False
    Me.dtp_working_to_tuesday.IsReadOnly = False
    Me.dtp_working_to_tuesday.Location = New System.Drawing.Point(252, 29)
    Me.dtp_working_to_tuesday.Name = "dtp_working_to_tuesday"
    Me.dtp_working_to_tuesday.ShowCheckBox = False
    Me.dtp_working_to_tuesday.ShowUpDown = True
    Me.dtp_working_to_tuesday.Size = New System.Drawing.Size(138, 24)
    Me.dtp_working_to_tuesday.SufixText = "Sufix Text"
    Me.dtp_working_to_tuesday.SufixTextVisible = True
    Me.dtp_working_to_tuesday.TabIndex = 4
    Me.dtp_working_to_tuesday.TextWidth = 50
    Me.dtp_working_to_tuesday.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_working_to_monday
    '
    Me.dtp_working_to_monday.CausesValidation = False
    Me.dtp_working_to_monday.Checked = False
    Me.dtp_working_to_monday.IsReadOnly = False
    Me.dtp_working_to_monday.Location = New System.Drawing.Point(252, 4)
    Me.dtp_working_to_monday.Name = "dtp_working_to_monday"
    Me.dtp_working_to_monday.ShowCheckBox = False
    Me.dtp_working_to_monday.ShowUpDown = True
    Me.dtp_working_to_monday.Size = New System.Drawing.Size(138, 24)
    Me.dtp_working_to_monday.SufixText = "Sufix Text"
    Me.dtp_working_to_monday.SufixTextVisible = True
    Me.dtp_working_to_monday.TabIndex = 2
    Me.dtp_working_to_monday.TextWidth = 50
    Me.dtp_working_to_monday.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_working_from_sunday
    '
    Me.dtp_working_from_sunday.CausesValidation = False
    Me.dtp_working_from_sunday.Checked = False
    Me.dtp_working_from_sunday.IsReadOnly = False
    Me.dtp_working_from_sunday.Location = New System.Drawing.Point(111, 154)
    Me.dtp_working_from_sunday.Name = "dtp_working_from_sunday"
    Me.dtp_working_from_sunday.ShowCheckBox = False
    Me.dtp_working_from_sunday.ShowUpDown = True
    Me.dtp_working_from_sunday.Size = New System.Drawing.Size(138, 24)
    Me.dtp_working_from_sunday.SufixText = "Sufix Text"
    Me.dtp_working_from_sunday.SufixTextVisible = True
    Me.dtp_working_from_sunday.TabIndex = 13
    Me.dtp_working_from_sunday.TextWidth = 50
    Me.dtp_working_from_sunday.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_working_from_saturday
    '
    Me.dtp_working_from_saturday.CausesValidation = False
    Me.dtp_working_from_saturday.Checked = False
    Me.dtp_working_from_saturday.IsReadOnly = False
    Me.dtp_working_from_saturday.Location = New System.Drawing.Point(111, 129)
    Me.dtp_working_from_saturday.Name = "dtp_working_from_saturday"
    Me.dtp_working_from_saturday.ShowCheckBox = False
    Me.dtp_working_from_saturday.ShowUpDown = True
    Me.dtp_working_from_saturday.Size = New System.Drawing.Size(138, 24)
    Me.dtp_working_from_saturday.SufixText = "Sufix Text"
    Me.dtp_working_from_saturday.SufixTextVisible = True
    Me.dtp_working_from_saturday.TabIndex = 11
    Me.dtp_working_from_saturday.TextWidth = 50
    Me.dtp_working_from_saturday.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_working_from_friday
    '
    Me.dtp_working_from_friday.CausesValidation = False
    Me.dtp_working_from_friday.Checked = False
    Me.dtp_working_from_friday.IsReadOnly = False
    Me.dtp_working_from_friday.Location = New System.Drawing.Point(111, 104)
    Me.dtp_working_from_friday.Name = "dtp_working_from_friday"
    Me.dtp_working_from_friday.ShowCheckBox = False
    Me.dtp_working_from_friday.ShowUpDown = True
    Me.dtp_working_from_friday.Size = New System.Drawing.Size(138, 24)
    Me.dtp_working_from_friday.SufixText = "Sufix Text"
    Me.dtp_working_from_friday.SufixTextVisible = True
    Me.dtp_working_from_friday.TabIndex = 9
    Me.dtp_working_from_friday.TextWidth = 50
    Me.dtp_working_from_friday.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_working_from_thursday
    '
    Me.dtp_working_from_thursday.CausesValidation = False
    Me.dtp_working_from_thursday.Checked = False
    Me.dtp_working_from_thursday.IsReadOnly = False
    Me.dtp_working_from_thursday.Location = New System.Drawing.Point(111, 79)
    Me.dtp_working_from_thursday.Name = "dtp_working_from_thursday"
    Me.dtp_working_from_thursday.ShowCheckBox = False
    Me.dtp_working_from_thursday.ShowUpDown = True
    Me.dtp_working_from_thursday.Size = New System.Drawing.Size(138, 24)
    Me.dtp_working_from_thursday.SufixText = "Sufix Text"
    Me.dtp_working_from_thursday.SufixTextVisible = True
    Me.dtp_working_from_thursday.TabIndex = 7
    Me.dtp_working_from_thursday.TextWidth = 50
    Me.dtp_working_from_thursday.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_working_from_wednesday
    '
    Me.dtp_working_from_wednesday.CausesValidation = False
    Me.dtp_working_from_wednesday.Checked = False
    Me.dtp_working_from_wednesday.IsReadOnly = False
    Me.dtp_working_from_wednesday.Location = New System.Drawing.Point(111, 54)
    Me.dtp_working_from_wednesday.Name = "dtp_working_from_wednesday"
    Me.dtp_working_from_wednesday.ShowCheckBox = False
    Me.dtp_working_from_wednesday.ShowUpDown = True
    Me.dtp_working_from_wednesday.Size = New System.Drawing.Size(138, 24)
    Me.dtp_working_from_wednesday.SufixText = "Sufix Text"
    Me.dtp_working_from_wednesday.SufixTextVisible = True
    Me.dtp_working_from_wednesday.TabIndex = 5
    Me.dtp_working_from_wednesday.TextWidth = 50
    Me.dtp_working_from_wednesday.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_working_from_tuesday
    '
    Me.dtp_working_from_tuesday.CausesValidation = False
    Me.dtp_working_from_tuesday.Checked = False
    Me.dtp_working_from_tuesday.IsReadOnly = False
    Me.dtp_working_from_tuesday.Location = New System.Drawing.Point(111, 29)
    Me.dtp_working_from_tuesday.Name = "dtp_working_from_tuesday"
    Me.dtp_working_from_tuesday.ShowCheckBox = False
    Me.dtp_working_from_tuesday.ShowUpDown = True
    Me.dtp_working_from_tuesday.Size = New System.Drawing.Size(138, 24)
    Me.dtp_working_from_tuesday.SufixText = "Sufix Text"
    Me.dtp_working_from_tuesday.SufixTextVisible = True
    Me.dtp_working_from_tuesday.TabIndex = 3
    Me.dtp_working_from_tuesday.TextWidth = 50
    Me.dtp_working_from_tuesday.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_working_from_monday
    '
    Me.dtp_working_from_monday.CausesValidation = False
    Me.dtp_working_from_monday.Checked = False
    Me.dtp_working_from_monday.IsReadOnly = False
    Me.dtp_working_from_monday.Location = New System.Drawing.Point(111, 4)
    Me.dtp_working_from_monday.Name = "dtp_working_from_monday"
    Me.dtp_working_from_monday.ShowCheckBox = False
    Me.dtp_working_from_monday.ShowUpDown = True
    Me.dtp_working_from_monday.Size = New System.Drawing.Size(138, 24)
    Me.dtp_working_from_monday.SufixText = "Sufix Text"
    Me.dtp_working_from_monday.SufixTextVisible = True
    Me.dtp_working_from_monday.TabIndex = 1
    Me.dtp_working_from_monday.TextWidth = 50
    Me.dtp_working_from_monday.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_days
    '
    Me.uc_days.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_days.Location = New System.Drawing.Point(3, 3)
    Me.uc_days.Name = "uc_days"
    Me.uc_days.Size = New System.Drawing.Size(125, 176)
    Me.uc_days.TabIndex = 0
    Me.uc_days.Weekday = 0
    '
    'uc_weekdays_by_hour
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
    Me.Controls.Add(Me.dtp_working_to_sunday)
    Me.Controls.Add(Me.dtp_working_to_saturday)
    Me.Controls.Add(Me.dtp_working_to_friday)
    Me.Controls.Add(Me.dtp_working_to_thursday)
    Me.Controls.Add(Me.dtp_working_to_wednesday)
    Me.Controls.Add(Me.dtp_working_to_tuesday)
    Me.Controls.Add(Me.dtp_working_to_monday)
    Me.Controls.Add(Me.dtp_working_from_sunday)
    Me.Controls.Add(Me.dtp_working_from_saturday)
    Me.Controls.Add(Me.dtp_working_from_friday)
    Me.Controls.Add(Me.dtp_working_from_thursday)
    Me.Controls.Add(Me.dtp_working_from_wednesday)
    Me.Controls.Add(Me.dtp_working_from_tuesday)
    Me.Controls.Add(Me.dtp_working_from_monday)
    Me.Controls.Add(Me.uc_days)
    Me.Name = "uc_weekdays_by_hour"
    Me.Size = New System.Drawing.Size(400, 178)
    Me.ResumeLayout(False)

  End Sub
  Public WithEvents uc_days As GUI_Controls.uc_week_days
  Public WithEvents dtp_working_from_monday As GUI_Controls.uc_date_picker
  Public WithEvents dtp_working_to_monday As GUI_Controls.uc_date_picker
  Public WithEvents dtp_working_from_tuesday As GUI_Controls.uc_date_picker
  Public WithEvents dtp_working_to_tuesday As GUI_Controls.uc_date_picker
  Public WithEvents dtp_working_from_wednesday As GUI_Controls.uc_date_picker
  Public WithEvents dtp_working_to_wednesday As GUI_Controls.uc_date_picker
  Public WithEvents dtp_working_from_thursday As GUI_Controls.uc_date_picker
  Public WithEvents dtp_working_to_thursday As GUI_Controls.uc_date_picker
  Public WithEvents dtp_working_from_friday As GUI_Controls.uc_date_picker
  Public WithEvents dtp_working_to_friday As GUI_Controls.uc_date_picker
  Public WithEvents dtp_working_from_saturday As GUI_Controls.uc_date_picker
  Public WithEvents dtp_working_to_saturday As GUI_Controls.uc_date_picker
  Public WithEvents dtp_working_from_sunday As GUI_Controls.uc_date_picker
  Public WithEvents dtp_working_to_sunday As GUI_Controls.uc_date_picker

End Class
