﻿'-------------------------------------------------------------------
' Copyright © 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_currency_selector
' DESCRIPTION:   Currency configuration for multicurrency.
'
' AUTHOR:        Enric Tomas
' CREATION DATE: 05/05/2016
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------- ------ -----------------------------------------------
' 05/MAY/2016 ETP    Initial version
' 30/MAY/2016 ETP    Fixed Bug 13812: Multidivisa - cambio de divisas: obliga a configurar todas las divisas
' 14-MAY-2016 ETP    PBI 14340: Added uc_currency_selector to bank card & check config.
' ----------- ------ -----------------------------------------------

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.Data.SqlClient
Imports WSI.Common
Imports System.Text
Imports WSI.Common.PinPad



Public Enum EntryMode
  OneCurrency = 0
  Multicurrency = 1
  CardCheck = 2
End Enum


Public Class uc_currency_selector


#Region " Constants "
  Private Const RECHARGE_VALUE_TO_CALCULATE As Integer = 100
  Private Const UC_CARD_HEIGH As Integer = 100

#End Region 'constants


#Region " Members "
  Private m_national_ISO_code As String
  Private m_foreign_ISO_code As String
  Private m_permissions As GUI_Controls.CLASS_GUI_USER.TYPE_PERMISSIONS
  Private m_currency As CURRENCIES_ENABLED
  Private m_foreign_decimal As Decimal
  Private m_entry_mode As EntryMode
#End Region ' Members

#Region " constructor "


  Public Sub New()

    Me.InitializeComponent()
    m_entry_mode = EntryMode.OneCurrency
  End Sub

  Public Sub New(ByRef EntryMode As EntryMode, ByRef Permissions As GUI_Controls.CLASS_GUI_USER.TYPE_PERMISSIONS)

    m_currency = New CURRENCIES_ENABLED()
    Me.InitializeComponent()
    m_entry_mode = EntryMode
    m_permissions = Permissions

  End Sub

#End Region 'constructor 

#Region " properties "
  Public Property ForeignCurrency() As CURRENCIES_ENABLED
    Get
      Return m_currency
    End Get
    Set(ByVal Value As CURRENCIES_ENABLED)
      m_currency = Value.Copy()
    End Set
  End Property

  Public Property ForeignDecimal() As Decimal

    Get
      Return m_foreign_decimal
    End Get
    Set(ByVal Value As Decimal)
      m_foreign_decimal = Value
    End Set
  End Property

  Public ReadOnly Property BankCard As bank_card
    Get
      Return m_currency.BankCard

    End Get
  End Property

#End Region 'properties

#Region " Public Functions "

  ' PURPOSE: Initialize resourses and data.
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Public Sub InitializeControl(ByVal Currency As CURRENCIES_ENABLED, ByVal Currencies As DataTable, ByVal Description As String, ByVal NationalIsoCode As String)
    GUI_SetScreenData()
    InitializeControlResources()
    SetCurrencyComboData(Currencies, Currency.foreign_currency_exchange.CurrencyCode, Description)
    SetForeignCurrencyData(Currency)

    Dim _properties As CurrencyExchangeProperties

    If (String.IsNullOrEmpty(Currency.foreign_currency_exchange.CurrencyCode)) Then
      _properties = CurrencyExchangeProperties.GetProperties(NationalIsoCode)
    Else
      _properties = CurrencyExchangeProperties.GetProperties(Currency.foreign_currency_exchange.CurrencyCode)
    End If

    img_icon.Image = _properties.GetFlag32x32()
    m_national_ISO_code = NationalIsoCode

    If (m_entry_mode = EntryMode.CardCheck) Then
      pnl_change.Visible = False
      pnl_comission_promotion.Location = New System.Drawing.Point(pnl_comission_promotion.Location.X, pnl_comission_promotion.Location.Y - UC_CARD_HEIGH)
      pnl_container.Height -= UC_CARD_HEIGH
      Me.Height -= UC_CARD_HEIGH
      img_icon.Visible = False
      ef_currency.Visible = False
      chk_enable_currency.Visible = False

      chk_foreign_currency.Visible = False
      If Not (BankCard.card_scope = CARD_SCOPE.NONE And BankCard.card_type = CARD_TYPE.NONE) Then
        chk_enable_currency.Visible = True
        chk_enable_currency.Checked = Currency.Enabled
        chk_enable_currency.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2525)
        chk_enable_currency.Location = New System.Drawing.Point(chk_enable_currency.Location.X, chk_enable_currency.Location.Y + 20)
      End If
    End If

  End Sub

  ' PURPOSE: Get S
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Public Sub GUI_GetScreenData()

    If String.IsNullOrEmpty(ef_change_foreign.Value) Then
      ForeignCurrency.foreign_currency_exchange.ChangeRate = 1
    Else
      ForeignCurrency.foreign_currency_exchange.ChangeRate = GUI_ParseNumber(ef_change_foreign.Value)
    End If

    ForeignCurrency.foreign_currency_exchange.Decimals = GUI_ParseNumber(cmb_decimals_foreign.Value)

    If String.IsNullOrEmpty(ef_fixed_commissions_foreign.Value) Then
      ForeignCurrency.foreign_currency_exchange.FixedComission = 0
    Else
      ForeignCurrency.foreign_currency_exchange.FixedComission = GUI_ParseCurrency(ef_fixed_commissions_foreign.Value)
    End If

    If String.IsNullOrEmpty(ef_fixed_promotions_NR2_foreign.Value) Then
      ForeignCurrency.foreign_currency_exchange.FixedNR2 = 0
    Else
      ForeignCurrency.foreign_currency_exchange.FixedNR2 = GUI_ParseCurrency(ef_fixed_promotions_NR2_foreign.Value)
    End If

    If String.IsNullOrEmpty(ef_variable_commissions_foreign.Value) Then
      ForeignCurrency.foreign_currency_exchange.VariableComission = 0
    Else
      ForeignCurrency.foreign_currency_exchange.VariableComission = GUI_ParseCurrency(ef_variable_commissions_foreign.Value)
    End If

    If String.IsNullOrEmpty(ef_variable_promotions_NR2_foreign.Value) Then
      ForeignCurrency.foreign_currency_exchange.VariableNR2 = 0
    Else
      ForeignCurrency.foreign_currency_exchange.VariableNR2 = GUI_ParseCurrency(ef_variable_promotions_NR2_foreign.Value)
    End If

  End Sub

  ' PURPOSE: Fill combo data 
  '
  '  PARAMS:
  '     - INPUT:
  '           - Currencies as datatable
  '           - IsoCodeGp as String
  '           - Description as String
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Public Sub SetCurrencyComboData(ByVal Currencies As DataTable, ByVal IsoCodeGP As String, ByVal Description As String)

    If (m_entry_mode = EntryMode.OneCurrency) Then

      cmb_foreign_currencies.DataSource = Currencies
      cmb_foreign_currencies.ValueMember = "ISO_CODE"
      cmb_foreign_currencies.DisplayMember = "DESCRIPTION"

      If Not String.IsNullOrEmpty(IsoCodeGP) Then
        cmb_foreign_currencies.SelectedValue = IsoCodeGP
        m_foreign_ISO_code = cmb_foreign_currencies.SelectedValue
      Else
        cmb_foreign_currencies.SelectedValue = ""
      End If

    Else

      ef_currency.Value = Description

    End If

  End Sub

  ' PURPOSE: Refresh Label Texts for foreign currency
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Public Sub RefreshLabelTextsForeignCurrency()

    Dim _current_currency As CurrencyExchange
    Dim _current_currency_result As CurrencyExchangeResult

    _current_currency = New CurrencyExchange()
    _current_currency_result = New CurrencyExchangeResult()

    _current_currency.CurrencyCode = m_foreign_ISO_code
    _current_currency.Description = cmb_foreign_currencies.Text

    If String.IsNullOrEmpty(ef_change_foreign.Value) Then
      _current_currency.ChangeRate = 1
    Else
      _current_currency.ChangeRate = GUI_ParseNumber(ef_change_foreign.Value)
    End If

    _current_currency.Decimals = GUI_ParseNumber(m_foreign_decimal)

    If String.IsNullOrEmpty(ef_fixed_commissions_foreign.Value) Then
      _current_currency.FixedComission = 0
    Else
      _current_currency.FixedComission = GUI_ParseCurrency(ef_fixed_commissions_foreign.Value)
    End If

    If String.IsNullOrEmpty(ef_fixed_promotions_NR2_foreign.Value) Then
      _current_currency.FixedNR2 = 0
    Else
      _current_currency.FixedNR2 = GUI_ParseCurrency(ef_fixed_promotions_NR2_foreign.Value)
    End If

    If String.IsNullOrEmpty(ef_variable_commissions_foreign.Value) Then
      _current_currency.VariableComission = 0
    Else
      _current_currency.VariableComission = GUI_ParseCurrency(ef_variable_commissions_foreign.Value)
    End If

    If String.IsNullOrEmpty(ef_variable_promotions_NR2_foreign.Value) Then
      _current_currency.VariableNR2 = 0
    Else
      _current_currency.VariableNR2 = GUI_ParseCurrency(ef_variable_promotions_NR2_foreign.Value)
    End If

    _current_currency.ApplyExchange(RECHARGE_VALUE_TO_CALCULATE, _current_currency_result)

    lbl_recharge_msg_foreign.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2151), Currency.Format(RECHARGE_VALUE_TO_CALCULATE, _current_currency.CurrencyCode))
    lbl_national_amount_msg_foreign.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2152), Currency.Format(_current_currency_result.GrossAmount, m_national_ISO_code))
    lbl_commissions_msg_foreign.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2153), Currency.Format(_current_currency_result.Comission, m_national_ISO_code))
    lbl_Net_amount_msg_foreign.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2154), Currency.Format(_current_currency_result.NetAmount, m_national_ISO_code))
    lbl_promotion_NR_msg_foreign.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2155), Currency.Format(_current_currency_result.NR2Amount, m_national_ISO_code))

  End Sub ' RefreshLabelTextsForeignCurrency

  ' PURPOSE: Refresh Label Texts for bank
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Public Sub RefreshLabelTextsBankCard()

    Dim _current_currency As CurrencyExchange
    Dim _current_currency_result As CurrencyExchangeResult

    _current_currency = New CurrencyExchange()
    _current_currency_result = New CurrencyExchangeResult()

    _current_currency.CurrencyCode = m_national_ISO_code
    _current_currency.Description = cmb_foreign_currencies.Text

    _current_currency.Type = CurrencyExchangeType.CARD

    If String.IsNullOrEmpty(ef_change_foreign.Value) Then
      _current_currency.ChangeRate = 1
    Else
      _current_currency.ChangeRate = GUI_ParseNumber(ef_change_foreign.Value)
    End If

    _current_currency.Decimals = GUI_ParseNumber(m_foreign_decimal)

    If String.IsNullOrEmpty(ef_fixed_commissions_foreign.Value) Then
      _current_currency.FixedComission = 0
    Else
      _current_currency.FixedComission = GUI_ParseCurrency(ef_fixed_commissions_foreign.Value)
    End If

    If String.IsNullOrEmpty(ef_fixed_promotions_NR2_foreign.Value) Then
      _current_currency.FixedNR2 = 0
    Else
      _current_currency.FixedNR2 = GUI_ParseCurrency(ef_fixed_promotions_NR2_foreign.Value)
    End If

    If String.IsNullOrEmpty(ef_variable_commissions_foreign.Value) Then
      _current_currency.VariableComission = 0
    Else
      _current_currency.VariableComission = GUI_ParseCurrency(ef_variable_commissions_foreign.Value)
    End If

    If String.IsNullOrEmpty(ef_variable_promotions_NR2_foreign.Value) Then
      _current_currency.VariableNR2 = 0
    Else
      _current_currency.VariableNR2 = GUI_ParseCurrency(ef_variable_promotions_NR2_foreign.Value)
    End If

    _current_currency.ApplyExchange(RECHARGE_VALUE_TO_CALCULATE, _current_currency_result)

    lbl_recharge_msg_foreign.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2534), GUI_FormatCurrency(RECHARGE_VALUE_TO_CALCULATE, , , True))
    lbl_national_amount_msg_foreign.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2532), GUI_FormatCurrency(_current_currency_result.GrossAmount, , , True))
    lbl_commissions_msg_foreign.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2153), GUI_FormatCurrency(_current_currency_result.Comission, , , True))
    lbl_Net_amount_msg_foreign.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2533), GUI_FormatCurrency(_current_currency_result.NetAmount, , , True))
    lbl_promotion_NR_msg_foreign.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2155), GUI_FormatCurrency(_current_currency_result.NR2Amount, , , True))

  End Sub ' RefreshLabelTextsBankCard

  ' PURPOSE: Refresh Label Texts for check
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Public Sub RefreshLabelTextsCheck()

    Dim _current_currency As CurrencyExchange
    Dim _current_currency_result As CurrencyExchangeResult

    _current_currency = New CurrencyExchange()
    _current_currency_result = New CurrencyExchangeResult()

    _current_currency.CurrencyCode = m_national_ISO_code
    _current_currency.Description = cmb_foreign_currencies.Text

    _current_currency.Type = CurrencyExchangeType.CHECK

    If String.IsNullOrEmpty(ef_change_foreign.Value) Then
      _current_currency.ChangeRate = 1
    Else
      _current_currency.ChangeRate = GUI_ParseNumber(ef_change_foreign.Value)
    End If

    _current_currency.Decimals = GUI_ParseNumber(m_foreign_decimal)

    If String.IsNullOrEmpty(ef_fixed_commissions_foreign.Value) Then
      _current_currency.FixedComission = 0
    Else
      _current_currency.FixedComission = GUI_ParseCurrency(ef_fixed_commissions_foreign.Value)
    End If

    If String.IsNullOrEmpty(ef_fixed_promotions_NR2_foreign.Value) Then
      _current_currency.FixedNR2 = 0
    Else
      _current_currency.FixedNR2 = GUI_ParseCurrency(ef_fixed_promotions_NR2_foreign.Value)
    End If

    If String.IsNullOrEmpty(ef_variable_commissions_foreign.Value) Then
      _current_currency.VariableComission = 0
    Else
      _current_currency.VariableComission = GUI_ParseCurrency(ef_variable_commissions_foreign.Value)
    End If

    If String.IsNullOrEmpty(ef_variable_promotions_NR2_foreign.Value) Then
      _current_currency.VariableNR2 = 0
    Else
      _current_currency.VariableNR2 = GUI_ParseCurrency(ef_variable_promotions_NR2_foreign.Value)
    End If

    _current_currency.ApplyExchange(RECHARGE_VALUE_TO_CALCULATE, _current_currency_result)

    lbl_recharge_msg_foreign.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2593), GUI_FormatCurrency(RECHARGE_VALUE_TO_CALCULATE, , , True))
    lbl_national_amount_msg_foreign.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2532), GUI_FormatCurrency(_current_currency_result.GrossAmount, , , True))
    lbl_commissions_msg_foreign.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2153), GUI_FormatCurrency(_current_currency_result.Comission, , , True))
    lbl_Net_amount_msg_foreign.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2533), GUI_FormatCurrency(_current_currency_result.NetAmount, , , True))
    lbl_promotion_NR_msg_foreign.Value = NLS_GetString(GLB_NLS_GUI_PLAYER_TRACKING.Id(2155), GUI_FormatCurrency(_current_currency_result.NR2Amount, , , True))


  End Sub ' RefreshLabelTextsCheck


  ' PURPOSE: Initialize currency data
  '
  '  PARAMS:
  '     - INPUT:
  '           - Currency as CURRENCIES_ENABLED
  '           - Optional Check_Currency as Boolean (If true enables currency and check )

  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Public Sub SetForeignCurrencyData(ByVal Currency As CURRENCIES_ENABLED, Optional ByVal Check_Currency As Boolean = False)
    Dim _iso_code As String
    Dim _write As Boolean
    m_currency = Currency


    _iso_code = Currency.foreign_currency_exchange.CurrencyCode
    m_foreign_ISO_code = _iso_code

    If Currency.foreign_currency_exchange.ChangeRate.HasValue Then
      ef_change_foreign.Value = Currency.foreign_currency_exchange.ChangeRate
      Currency.foreign_old_change_rate = Currency.foreign_currency_exchange.ChangeRate
      Currency.old_num_decimals = Currency.foreign_currency_exchange.Decimals
    Else
      ef_change_foreign.Value = "0"
      Currency.foreign_old_change_rate = 0
      Currency.old_num_decimals = 2
    End If

    If Currency.foreign_currency_exchange.Decimals.HasValue Then
      cmb_decimals_foreign.Value = Currency.foreign_currency_exchange.Decimals
    Else
      cmb_decimals_foreign.Value = 0
    End If
    m_foreign_decimal = cmb_decimals_foreign.Value

    If Currency.foreign_currency_exchange.FixedComission.HasValue Then
      ef_fixed_commissions_foreign.Value = Currency.foreign_currency_exchange.FixedComission
    Else
      ef_fixed_commissions_foreign.Value = "0"
    End If

    If Currency.foreign_currency_exchange.VariableComission.HasValue Then
      ef_variable_commissions_foreign.Value = Currency.foreign_currency_exchange.VariableComission
    Else
      ef_variable_commissions_foreign.Value = "0"
    End If

    If Currency.foreign_currency_exchange.FixedNR2.HasValue Then
      ef_fixed_promotions_NR2_foreign.Value = Currency.foreign_currency_exchange.FixedNR2
    Else
      ef_fixed_promotions_NR2_foreign.Value = "0"
    End If

    If Currency.foreign_currency_exchange.VariableNR2.HasValue Then
      ef_variable_promotions_NR2_foreign.Value = Currency.foreign_currency_exchange.VariableNR2
    Else
      ef_variable_promotions_NR2_foreign.Value = "0"
    End If

    If m_entry_mode = EntryMode.Multicurrency Then
      chk_enable_currency.Checked = Currency.Enabled Or Check_Currency
      chk_enable_currency_CheckedChanged(Nothing, Nothing)
      _write = chk_enable_currency.Checked
    Else
      chk_foreign_currency.Checked = Currency.Enabled Or Check_Currency
      chk_foreign_currency_CheckedChanged(Nothing, Nothing)
      _write = chk_foreign_currency.Checked

    End If


    If Not m_permissions.Write Then
      If Not _write Then
        ef_change_foreign.Enabled = False
      End If

      Return
    End If

  End Sub ' SetForeignCurrencyData

  ' PURPOSE: ValidateControlData
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - none
  '
  Public Function IsControlDataOk() As Boolean
    Dim _iso_code As String = ""


    If m_currency.Enabled Then
      Select Case (m_entry_mode)

        Case EntryMode.OneCurrency
          If cmb_foreign_currencies.SelectedValue <> "" Then
            If String.IsNullOrEmpty(ef_change_foreign.Value) Then
              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , ef_change_foreign.Text)
              Call Me.ef_change_foreign.Focus()

              Return False
            End If

            If GUI_ParseNumber(ef_change_foreign.TextValue) <= 0 Then
              NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_change_foreign.Text)
              Call Me.ef_change_foreign.Focus()

              Return False
            End If

            If Not ef_fixed_commissions_foreign.ValidateFormat Then
              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_fixed_commissions_foreign.Text & " " & lbl_fixed_foreign.Text)
              Call ef_fixed_commissions_foreign.Focus()

              Return False
            End If

            If Not ef_variable_commissions_foreign.ValidateFormat OrElse GUI_ParseNumber(ef_variable_commissions_foreign.TextValue) > 100 Then
              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_fixed_commissions_foreign.Text & " " & lbl_variable_foreign.Text, 0, 100)
              Call Me.ef_variable_commissions_foreign.Focus()

              Return False
            End If

            If Not ef_fixed_promotions_NR2_foreign.ValidateFormat Then
              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_fixed_promotions_NR2_foreign.Text & " " & lbl_fixed_foreign.Text)
              Call ef_fixed_promotions_NR2_foreign.Focus()

              Return False
            End If

            If Not ef_variable_promotions_NR2_foreign.ValidateFormat OrElse GUI_ParseNumber(ef_variable_promotions_NR2_foreign.TextValue) > 100 Then
              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_fixed_promotions_NR2_foreign.Text & " " & lbl_variable_foreign.Text, 0, 100)
              Call Me.ef_variable_promotions_NR2_foreign.Focus()

              Return False
            End If
          Else
            If chk_foreign_currency.Checked Then
              Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , cmb_foreign_currencies.Text, 0, 100)
              Call Me.cmb_foreign_currencies.Focus()

              Return False
            End If
          End If

        Case EntryMode.Multicurrency

          _iso_code = " - " & ef_currency.Value

          If String.IsNullOrEmpty(ef_change_foreign.Value) Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, , ef_change_foreign.Text & _iso_code)
            Call Me.ef_change_foreign.Focus()

            Return False
          End If

          If GUI_ParseNumber(ef_change_foreign.TextValue) <= 0 Then
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_change_foreign.Text & _iso_code)
            Call Me.ef_change_foreign.Focus()

            Return False
          End If

          If Not ef_fixed_commissions_foreign.ValidateFormat Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_fixed_commissions_foreign.Text & " " & lbl_fixed_foreign.Text & _iso_code)
            Call ef_fixed_commissions_foreign.Focus()

            Return False
          End If

          If Not ef_variable_commissions_foreign.ValidateFormat OrElse GUI_ParseNumber(ef_variable_commissions_foreign.TextValue) > 100 Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_fixed_commissions_foreign.Text & " " & lbl_variable_foreign.Text & _iso_code, 0, 100)
            Call Me.ef_variable_commissions_foreign.Focus()

            Return False
          End If

          If Not ef_fixed_promotions_NR2_foreign.ValidateFormat Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_fixed_promotions_NR2_foreign.Text & " " & lbl_fixed_foreign.Text & _iso_code)
            Call ef_fixed_promotions_NR2_foreign.Focus()

            Return False
          End If

          If Not ef_variable_promotions_NR2_foreign.ValidateFormat OrElse GUI_ParseNumber(ef_variable_promotions_NR2_foreign.TextValue) > 100 Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_fixed_promotions_NR2_foreign.Text & " " & lbl_variable_foreign.Text & _iso_code, 0, 100)
            Call Me.ef_variable_promotions_NR2_foreign.Focus()

            Return False
          End If


        Case EntryMode.CardCheck

          If Not ef_variable_commissions_foreign.ValidateFormat OrElse GUI_ParseNumber(ef_variable_commissions_foreign.TextValue) > 100 Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1190), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ef_fixed_commissions_foreign.Text & " " & lbl_variable_foreign.Text & _iso_code, 0, 100)
            Call Me.ef_variable_commissions_foreign.Focus()

            Return False
          End If


      End Select

    End If



    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE: Set enabled for controls.
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Public Sub set_enabled(Enabled As Boolean)

    HideLabelTextsForeignCurrency(Enabled)
    m_currency.Enabled = Enabled
    pnl_container.Enabled = Enabled

  End Sub

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Initialize resources
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Private Sub InitializeControlResources()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2429)

    ' Foreign Currency
    Me.ef_change_foreign.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2144)
    Me.lbl_decimals_msg_foreign.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2146)
    Me.cmb_decimals_foreign.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2145)
    Me.cmb_decimals_foreign.Add(0, "0")
    Me.cmb_decimals_foreign.Add(1, "1")
    Me.cmb_decimals_foreign.Add(2, "2")
    Me.lbl_fixed_foreign.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2147)
    Me.ef_order_value.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5778)
    Me.lbl_variable_foreign.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2148)
    Me.ef_fixed_commissions_foreign.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2509)
    Me.ef_fixed_commissions_foreign.SufixText = ""
    Me.ef_fixed_promotions_NR2_foreign.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2150)
    Me.ef_fixed_promotions_NR2_foreign.SufixText = ""
    Me.ef_variable_commissions_foreign.SufixText = GLB_NLS_GUI_STATISTICS.GetString(460)
    Me.ef_variable_promotions_NR2_foreign.SufixText = GLB_NLS_GUI_STATISTICS.GetString(460)
    Me.ef_change_foreign.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 16, 8)
    Me.ef_fixed_commissions_foreign.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    Me.ef_variable_commissions_foreign.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 2)
    Me.ef_fixed_promotions_NR2_foreign.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    Me.ef_variable_promotions_NR2_foreign.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 2)
    Me.ef_order_value.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 0)
    'Me.ef_currency_order.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 2)
    'Me.ef_currency_order.TextValue = 0
    Me.chk_foreign_currency.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2544)
    Me.chk_enable_currency.Text = "      "
    Me.ef_currency.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2430)

    If m_permissions.Execute And Not m_permissions.Write Then

      Me.cmb_foreign_currencies.Enabled = False
      Me.cmb_decimals_foreign.IsReadOnly = True
      Me.ef_fixed_commissions_foreign.IsReadOnly = True
      Me.ef_fixed_promotions_NR2_foreign.IsReadOnly = True
      Me.ef_variable_commissions_foreign.IsReadOnly = True
      Me.ef_variable_promotions_NR2_foreign.IsReadOnly = True
      Me.cmb_foreign_currencies.Enabled = False
      Me.chk_foreign_currency.Enabled = False

    End If


    ef_change_foreign.TextVisible = True

    ef_fixed_commissions_foreign.TextVisible = True
    ef_fixed_promotions_NR2_foreign.TextVisible = True

    ef_variable_commissions_foreign.TextVisible = True
    ef_variable_promotions_NR2_foreign.TextVisible = True

    'ef_currency.Visible = EntryMode = EntryMode.Multicurrency
    'ef_currency.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2430)
    'ef_currency.IsReadOnly = True

    If (m_entry_mode = EntryMode.OneCurrency) Then
      chk_enable_currency.Visible = False
      cmb_foreign_currencies.Visible = True
      img_icon.Visible = False
      ef_currency.Visible = False
    Else
      ef_currency.Visible = True
      chk_foreign_currency.Visible = False
      cmb_foreign_currencies.Visible = False
    End If




  End Sub

  ' PURPOSE: Set Screen data
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Private Sub GUI_SetScreenData()

    RefreshLabelTextsForeignCurrency()

    AddHandler chk_foreign_currency.CheckedChanged, AddressOf chk_foreign_currency_CheckedChanged
    AddHandler cmb_decimals_foreign.ValueChangedEvent, AddressOf cmb_decimals_foreign_ValueChangedEvent

  End Sub

  ' PURPOSE: HideLabelTextsForeignCurrency when is not enabled.
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Private Sub HideLabelTextsForeignCurrency(ByVal ShowExamples As Boolean)
    lbl_decimals_msg_foreign.Visible = ShowExamples
    lbl_recharge_msg_foreign.Visible = ShowExamples
    lbl_national_amount_msg_foreign.Visible = ShowExamples
    lbl_Net_amount_msg_foreign.Visible = ShowExamples
    lbl_commissions_msg_foreign.Visible = ShowExamples
    lbl_promotion_NR_msg_foreign.Visible = ShowExamples
  End Sub

#End Region 'Private Functions



#Region " Events "

  Private Sub chk_foreign_currency_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    set_enabled(chk_foreign_currency.Checked)

  End Sub ' chk_foreign_currency_CheckedChanged

  Private Sub chk_enable_currency_CheckedChanged(sender As Object, e As EventArgs) Handles chk_enable_currency.CheckedChanged

    set_enabled(chk_enable_currency.Checked)

  End Sub

  Private Sub cmb_decimals_foreign_ValueChangedEvent()
    'm_foreign_decimal = cmb_decimals_foreign.Value
  End Sub ' cmb_national_currencies_SelectedValueChanged

  ' cmb_national_currencies_SelectedValueChanged




  Private Sub cmb_foreign_currencies_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmb_foreign_currencies.SelectedIndexChanged

    Dim _currency_configuration As CURRENCIES_ENABLED
    _currency_configuration = New CURRENCIES_ENABLED()


    m_currency.Enabled = False ' Set false to last currency_configuration

    If Not cmb_foreign_currencies.Focused Then
      Return
    End If

    '_currency_configuration.Foreign_Currency_ISO_Code_GP = cmb_foreign_currencies.SelectedValue
    CurrencyExchange.ReadCurrencyExchange(CurrencyExchangeType.CURRENCY, cmb_foreign_currencies.SelectedValue, _currency_configuration.foreign_currency_exchange)

    _currency_configuration.SetOldData()

    'Set Data Foreign Currency
    SetForeignCurrencyData(_currency_configuration, True)

    m_foreign_ISO_code = cmb_foreign_currencies.SelectedValue

    'If _currency_configuration.Foreign_Currency_ISO_Code_GP = "" Then
    '  ef_change_foreign.Enabled = False
    '  cmb_decimals_foreign.Enabled = False
    '  ef_fixed_commissions_foreign.Enabled = False
    '  ef_fixed_promotions_NR2_foreign.Enabled = False
    '  ef_variable_commissions_foreign.Enabled = False
    '  ef_variable_promotions_NR2_foreign.Enabled = False
    'Else
    '  ef_change_foreign.Enabled = True
    '  cmb_decimals_foreign.Enabled = True
    '  ef_fixed_commissions_foreign.Enabled = True
    '  ef_fixed_promotions_NR2_foreign.Enabled = True
    '  ef_variable_commissions_foreign.Enabled = True
    '  ef_variable_promotions_NR2_foreign.Enabled = True
    'End If
    Call HideLabelTextsForeignCurrency(cmb_foreign_currencies.SelectedValue <> "")
  End Sub

#End Region ' Events





  Private Sub img_icon_Click(sender As Object, e As EventArgs) Handles img_icon.Click
    chk_enable_currency.Checked = Not chk_enable_currency.Checked
    'pnl_title.Location = New System.Drawing.Point(pnl_title.Location.X + 1, pnl_title.Location.Y)
  End Sub


End Class

Public Class CURRENCIES_ENABLED

#Region "Members"

  Public foreign_currency_exchange As CurrencyExchange
  Public old_num_decimals As Decimal
  Public foreign_old_change_rate As Decimal

#End Region 'Members


#Region "constructors"
  Sub New()
    foreign_currency_exchange = New CurrencyExchange()
    BankCard.Initialize(CARD_TYPE.NONE, CARD_SCOPE.NONE)
  End Sub

  Sub New(ByVal Currencies As CURRENCIES_ENABLED)
    foreign_currency_exchange = New CurrencyExchange()

    old_num_decimals = Currencies.old_num_decimals
    foreign_old_change_rate = Currencies.foreign_old_change_rate
    foreign_currency_exchange = Currencies.foreign_currency_exchange.Copy()

  End Sub
#End Region 'Constructors


#Region "properties"
  Public Property Enabled() As Boolean
    Get
      Return foreign_currency_exchange.Status
    End Get
    Set(ByVal Value As Boolean)
      foreign_currency_exchange.Status = Value
    End Set
  End Property

  Public ReadOnly Property BankCard As bank_card
    Get
      Return foreign_currency_exchange.BankCard
    End Get
  End Property

#End Region 'Properties

#Region "Public Functions"

  ' PURPOSE: Copy current object.
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Public Function Copy() As CURRENCIES_ENABLED

    Return Me.MemberwiseClone()

  End Function
  ' PURPOSE: Initialize old data for allow change audit
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Public Sub SetOldData()

    old_num_decimals = Me.foreign_currency_exchange.Decimals
    foreign_old_change_rate = Me.foreign_currency_exchange.ChangeRate

  End Sub

#End Region 'Public functions




End Class