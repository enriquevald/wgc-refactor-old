'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_game_sel.vb  
'
' DESCRIPTION:   game selector control
'
' AUTHOR:        Andreu Juli�
'
' CREATION DATE: 15-NOV-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-NOV-2002  AJQ    Initial version
' 27-MAR-2013  RCI    mdl_color moved from GUI_Controls to GUI_CommonMisc
'--------------------------------------------------------------------

Imports System.Data.OleDb
Imports GUI_CommonMisc

'XVV -> Add NameSpace solve error with variables definitions
Imports System.Drawing

Public Class uc_game_sel
  Inherits GUI_Controls.uc_one_all

#Region " variables "
  Dim color_fill_enabled_game As Color = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
  Dim color_fill_disabled_game As Color = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_GREY_00)
  Dim color_font_enabled_game As Color = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
  Dim color_font_disabled_game As Color = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
  Private m_updating As Boolean = False

  Private m_last_game_id As Integer = -3



#End Region


#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call
    Me.pn_enabled_disabled.Visible = True
  End Sub

  'UserControl overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Private WithEvents lbl_separator As System.Windows.Forms.Label
  Private WithEvents tri_game_enabled As GUI_Controls.uc_trioption
  Friend WithEvents label_enabled As System.Windows.Forms.Label
  Friend WithEvents label_disabled As System.Windows.Forms.Label
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.tri_game_enabled = New GUI_Controls.uc_trioption
    Me.lbl_separator = New System.Windows.Forms.Label
    Me.label_enabled = New System.Windows.Forms.Label
    Me.label_disabled = New System.Windows.Forms.Label
    Me.pn_enabled_disabled.SuspendLayout()
    Me.SuspendLayout()
    '
    'dg_one_all
    '
    Me.dg_one_all.Size = New System.Drawing.Size(268, 54)
    '
    'pn_enabled_disabled
    '
    Me.pn_enabled_disabled.Controls.AddRange(New System.Windows.Forms.Control() {Me.label_enabled, Me.label_disabled, Me.lbl_separator, Me.tri_game_enabled})
    Me.pn_enabled_disabled.Visible = True
    '
    'tri_game_enabled
    '
    Me.tri_game_enabled.Location = New System.Drawing.Point(8, 8)
    Me.tri_game_enabled.Name = "tri_game_enabled"
    Me.tri_game_enabled.SelectedOption = GUI_Controls.uc_trioption.ENUM_TRIOPTION.TRIOPTION_C
    Me.tri_game_enabled.Size = New System.Drawing.Size(100, 16)
    Me.tri_game_enabled.TabIndex = 1
    Me.tri_game_enabled.Vertical = True
    '
    'lbl_separator
    '
    Me.lbl_separator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.lbl_separator.Enabled = False
    Me.lbl_separator.Name = "lbl_separator"
    Me.lbl_separator.Size = New System.Drawing.Size(2, 56)
    Me.lbl_separator.TabIndex = 0
    '
    'label_enabled
    '
    Me.label_enabled.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right)
    Me.label_enabled.BackColor = color_fill_enabled_game
    Me.label_enabled.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.label_enabled.Location = New System.Drawing.Point(118, 6)
    Me.label_enabled.Name = "label_enabled"
    Me.label_enabled.Size = New System.Drawing.Size(16, 16)
    Me.label_enabled.TabIndex = 2
    '
    'label_disabled
    '
    Me.label_disabled.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right)
    Me.label_disabled.BackColor = color_fill_disabled_game
    Me.label_disabled.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.label_disabled.Location = New System.Drawing.Point(118, 25)
    Me.label_disabled.Name = "label_disabled"
    Me.label_disabled.Size = New System.Drawing.Size(16, 16)
    Me.label_disabled.TabIndex = 3
    '
    'uc_game_sel
    '
    Me.Name = "uc_game_sel"
    Me.Size = New System.Drawing.Size(450, 80)
    Me.pn_enabled_disabled.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region


  'PURPOSE : Returns the number of games displayed in the grid

  ' PARAMS :
  '    - INPUT :
  '          - none
  '    - OUTPUT :
  '          - none

  'RETURNS :
  '    - number of games displayed in the grid

  Public Function GamesCount() As Integer
    Dim games_count As Integer

    games_count = 0
    If SelectionType() = ENUM_SELECTION.SELECTION_BY_GRID Then
      games_count = Me.dg_one_all.NumRows
    End If

    Return games_count

  End Function

  'PURPOSE : Returns the requested game id

  ' PARAMS :
  '    - INPUT :
  '          - none
  '    - OUTPUT :
  '          - none

  'RETURNS :
  '    - Game Id

  Public Function GameId(ByVal Index As Integer) As Integer
    Dim game_id As Integer

    game_id = 0

    If (Index >= 0) And (Index < Me.dg_one_all.NumRows) Then
      game_id = CInt(Me.dg_one_all.Cell(Index, GRID_COLUMN_GAME_ID).Value)
    End If
    Return game_id
  End Function


  'PURPOSE : Returns the requested game name

  ' PARAMS :
  '    - INPUT :
  '          - none
  '    - OUTPUT :
  '          - none

  'RETURNS :
  '    - Game Id

  Public Function GameName(ByVal Index As Integer) As String
    Dim game_name As String

    game_name = ""

    If (Index >= 0) And (Index < Me.dg_one_all.NumRows) Then
      game_name = Me.dg_one_all.Cell(Index, GRID_COLUMN_NAME).Value
    End If
    Return game_name
  End Function


  'PURPOSE : Returns true if the requested row in the grid is selected

  ' PARAMS :
  '    - INPUT :
  '          - none
  '    - OUTPUT :
  '          - none

  'RETURNS :
  '    - True if selected

  Public Function IsSelected(ByVal Index As Integer) As Boolean
    Dim is_selected As Boolean

    is_selected = False

    If (Index >= 0) And (Index < Me.dg_one_all.NumRows) Then
      If Me.dg_one_all.Cell(Index, GRID_COLUMN_CHECK_FLAG).Value = uc_grid.GRID_CHK_CHECKED Then
        is_selected = True
      End If
    End If

    Return is_selected
  End Function

  'PURPOSE : Returns the list of selected games to use in a SQL-IN Clause

  ' PARAMS :
  '    - INPUT :
  '          - none
  '    - OUTPUT :
  '          - none

  'RETURNS :
  '    - sql list string
  Public Function GetListString(Optional ByVal Column_game As Integer = GRID_COLUMN_GAME_ID) As String
    Dim idx As Integer
    Dim number_of_games As Integer
    Dim in_sql_clause_String As String
    'XVV Variable not use dim string_length As Integer

    in_sql_clause_String = ""
    number_of_games = GamesCount()

    For idx = 0 To number_of_games - 1
      If IsSelected(idx) Then
        If in_sql_clause_String = "" Then
          in_sql_clause_String = Me.dg_one_all.Cell(idx, Column_game).Value
        Else
          in_sql_clause_String = in_sql_clause_String & ", " & Me.dg_one_all.Cell(idx, Column_game).Value
        End If
      End If
    Next

    Return in_sql_clause_String
  End Function


  'PURPOSE : Return the number of selected games

  ' PARAMS :
  '    - INPUT :
  '          - none
  '    - OUTPUT :
  '          - none

  'RETURNS :
  '    - number of selected games

  Public Function SelectedCount() As Integer
    Dim idx As Integer
    Dim num_games As Integer
    Dim num_selected As Integer
    num_games = GamesCount()

    num_selected = 0

    For idx = 0 To num_games - 1
      If IsSelected(idx) Then
        num_selected = num_selected + 1
      End If
    Next

    Return num_selected
  End Function

  ' PURPOSE : Fill the combobox with games names
  '
  '  PARAMS :
  '     - INPUT :
  '         - Opt     
  '
  '     - OUTPUT :
  '         - GameTable 
  '
  ' RETURNS :
  '
  Public Function GetFilteredGamesTable(ByVal Opt As uc_trioption.ENUM_TRIOPTION) As DataTable
    Dim table As DataTable
    Dim aux_table As DataTable
    Dim row As DataRow

    table = Nothing
    aux_table = Nothing

    Try
      table = GetGames()
      If IsNothing(table) Then
        Return Nothing
      End If

      Select Case Opt
        Case uc_trioption.ENUM_TRIOPTION.TRIOPTION_A
          aux_table = table.Clone
          For Each row In table.Rows
            If row.Item("GM_ENABLED") = 1 Then
              aux_table.Rows.Add(row.ItemArray)
            End If
          Next
          table.Dispose()
          table = Nothing
          If aux_table.Rows.Count <= 0 Then
            aux_table.Dispose()
            aux_table = Nothing
          End If
          Return aux_table

        Case uc_trioption.ENUM_TRIOPTION.TRIOPTION_B
          aux_table = table.Clone
          For Each row In table.Rows
            If row.Item("GM_ENABLED") = 0 Then
              aux_table.Rows.Add(row.ItemArray)
            End If
          Next
          table.Dispose()
          table = Nothing
          If aux_table.Rows.Count <= 0 Then
            aux_table.Dispose()
            aux_table = Nothing
          End If
          Return aux_table

        Case uc_trioption.ENUM_TRIOPTION.TRIOPTION_C
          Return table

        Case Else
          '
      End Select

      Return Nothing

    Catch exception As Exception
      Debug.WriteLine(exception.Message)

      If Not IsNothing(table) Then
        table.Dispose()
      End If
      If Not IsNothing(aux_table) Then
        aux_table.Dispose()
      End If

      Return Nothing
    End Try

    Return Nothing

  End Function

  Public Overloads Sub Init(Optional ByVal SelectionType As ENUM_SELECTION = ENUM_SELECTION.SELECTION_BY_COMBO)
    'XVV Variable not use im scale_screen As Integer

    m_updating = True

    Call MyBase.Init(GLB_NLS_GUI_CONTROLS.Id(280), _
                     uc_one_all.ENUM_ONE_ALL_GENDER.ONE_ALL_GENDER_MALE, _
                     SelectionType)

    MyBase.pn_enabled_disabled.Visible = True

    Call Me.tri_game_enabled.Init(uc_trioption.ENUM_TRIOPTION.TRIOPTION_A)

    If SelectionType = ENUM_SELECTION.SELECTION_BY_GRID Then
      label_enabled.Visible = True
      label_disabled.Visible = True
    Else
      label_enabled.Visible = False
      label_disabled.Visible = False
    End If

    Me.tri_game_enabled.OptionText(uc_trioption.ENUM_TRIOPTION.TRIOPTION_A) = GLB_NLS_GUI_CONTROLS.GetString(281)
    Me.tri_game_enabled.OptionText(uc_trioption.ENUM_TRIOPTION.TRIOPTION_B) = GLB_NLS_GUI_CONTROLS.GetString(282)

    Call Me.dg_one_all.ColumnFit(GRID_COLUMN_NAME)

    m_updating = False

    Call ChangeGames()


    AddHandler tri_game_enabled.TristateChangedEvent, AddressOf tri_game_enabled_TristateChangedEvent

    RaiseSelectedGameChangedEvent()


  End Sub

  Private Sub RaiseSelectedGameChangedEvent()
    Dim id As Integer

    If SelectionType() = uc_one_all.ENUM_SELECTION.SELECTION_BY_GRID Then
      RaiseEvent SelectedGameChangedEvent()

      Exit Sub
    End If

    If Me.opt_all.Checked Then
      id = -2
    Else
      id = Me.Combo.Value
    End If

    If id <> m_last_game_id Then
      m_last_game_id = id
      RaiseEvent SelectedGameChangedEvent()
    End If

  End Sub

  Private Sub tri_game_enabled_TristateChangedEvent() 'Handles tri_game_enabled.TristateChangedEvent
    Call ChangeGames()
    RaiseSelectedGameChangedEvent()
  End Sub

  Private Sub ChangeGames()
    Dim one_sel As Boolean
    Dim one_id As Integer
    Dim table As DataTable

    If m_updating Then
      Exit Sub
    End If

    Try
      m_updating = True

      If SelectionType() = uc_one_all.ENUM_SELECTION.SELECTION_BY_GRID And _
         opt_all.Checked Then
        Call Me.dg_one_all.Clear()
        Me.dg_one_all.Enabled = False
      End If

      If SelectionType() = ENUM_SELECTION.SELECTION_BY_COMBO Then
        one_sel = False
        If Me.opt_one.Checked Then
          one_sel = True
          one_id = Me.Combo.Value
        End If
      End If

      If Me.tri_game_enabled Is Nothing Then
        Exit Sub
      End If


      table = GetFilteredGamesTable(Me.tri_game_enabled.SelectedOption)
      If IsNothing(table) Then
        Call Me.Combo.Clear()
      Else
        Call Me.FillSelector(table)
        table.Dispose()
        table = Nothing
      End If




      If Me.SelectionType = ENUM_SELECTION.SELECTION_BY_COMBO Then
        If one_sel Then
          Me.Combo.Value = one_id
          If Me.Combo.SelectedIndex = -1 Then
            If Me.Combo.Count > 0 Then
              Me.Combo.SelectedIndex = 0
            End If
          End If
        End If
      End If

    Finally
      m_updating = False
    End Try

  End Sub

#Region " Private Functions "

  Private Sub FillSelector(ByVal Table As Data.DataTable)
    Dim idx As Integer
    Dim aux_id As Integer
    Dim aux_name As String
    Dim game_enabled As Integer
    Dim diff As Boolean

    If Me.SelectionType() = ENUM_SELECTION.SELECTION_BY_GRID Then
      If Me.opt_all.Checked = False Then
        If dg_one_all.NumRows > 0 Then
          If (dg_one_all.NumRows = Table.Rows.Count) Then
            diff = False
            For idx = 0 To Table.Rows.Count - 1
              If Not (dg_one_all.Cell(idx, GRID_COLUMN_GAME_ID).Value = CInt(Table.Rows(idx).Item("GM_GAME_ID"))) Then
                diff = True
                Exit For
              End If
            Next
            If Not diff Then
              Exit Sub
            End If
          End If
        End If

        Me.dg_one_all.Clear()
        Me.dg_one_all.Redraw = False
        For idx = 0 To Table.Rows.Count - 1
          aux_id = CInt(Table.Rows(idx).Item("GM_GAME_ID"))
          aux_name = CStr(Table.Rows(idx).Item("GM_NAME"))
          game_enabled = CInt(Table.Rows(idx).Item("GM_ENABLED"))
          Me.dg_one_all.AddRow()
          dg_one_all.Cell(idx, GRID_COLUMN_CHECK_FLAG).Value = uc_grid.GRID_CHK_UNCHECKED
          dg_one_all.Cell(idx, GRID_COLUMN_GAME_ID).Value = aux_id
          dg_one_all.Cell(idx, GRID_COLUMN_NAME).Value = aux_name
          If game_enabled = 0 Then
            dg_one_all.Row(idx).BackColor = color_fill_disabled_game
            dg_one_all.Row(idx).ForeColor = color_font_disabled_game
          Else
            dg_one_all.Row(idx).BackColor = color_fill_enabled_game
            dg_one_all.Row(idx).ForeColor = color_font_enabled_game
          End If
        Next
        Me.dg_one_all.Redraw = True
      End If

    Else
      Me.Combo.Clear()
      Me.Combo.Add(Table, "GM_GAME_ID", "GM_NAME")
    End If
  End Sub

#End Region


  Public Event SelectedGameChangedEvent()

  Public Enum ENUM_GAME_STATE
    GAME_DISABLED = 0
    GAME_ENABLED = 1
    GAME_ALL = 2
  End Enum

  Public Property SelectedGameState() As ENUM_GAME_STATE
    Get
      Select Case Me.tri_game_enabled.SelectedOption
        Case uc_trioption.ENUM_TRIOPTION.TRIOPTION_A
          Return ENUM_GAME_STATE.GAME_ENABLED
        Case uc_trioption.ENUM_TRIOPTION.TRIOPTION_B
          Return ENUM_GAME_STATE.GAME_DISABLED
        Case uc_trioption.ENUM_TRIOPTION.TRIOPTION_C
          Return ENUM_GAME_STATE.GAME_ALL
        Case Else
          Return ENUM_GAME_STATE.GAME_ALL
      End Select
    End Get
    Set(ByVal Value As ENUM_GAME_STATE)
      Select Case Value
        Case ENUM_GAME_STATE.GAME_ENABLED
          Me.tri_game_enabled.SelectedOption = uc_trioption.ENUM_TRIOPTION.TRIOPTION_A
        Case ENUM_GAME_STATE.GAME_DISABLED
          Me.tri_game_enabled.SelectedOption = uc_trioption.ENUM_TRIOPTION.TRIOPTION_B
        Case ENUM_GAME_STATE.GAME_ALL
          Me.tri_game_enabled.SelectedOption = uc_trioption.ENUM_TRIOPTION.TRIOPTION_C
        Case Else
          Me.tri_game_enabled.SelectedOption = uc_trioption.ENUM_TRIOPTION.TRIOPTION_C
      End Select
    End Set
  End Property

  Public ReadOnly Property GetSelectedFilterString() As String
    Get
      If Me.SelectedAll Then
        Select Case Me.tri_game_enabled.SelectedOption
          Case uc_trioption.ENUM_TRIOPTION.TRIOPTION_A
            Return Me.opt_all.Text & " (" & GLB_NLS_GUI_CONTROLS.GetString(293) & ")"
          Case uc_trioption.ENUM_TRIOPTION.TRIOPTION_B
            Return Me.opt_all.Text & " (" & GLB_NLS_GUI_CONTROLS.GetString(294) & ")"
          Case uc_trioption.ENUM_TRIOPTION.TRIOPTION_C
            Return Me.opt_all.Text
          Case Else
            Return Me.opt_all.Text
        End Select
      Else
        Return GetListString(uc_one_all.GRID_COLUMN_NAME)
      End If
    End Get
  End Property

  Protected Overrides Sub ComboSelectedItemChanged()
    If m_updating Then
      Exit Sub
    End If
    RaiseSelectedGameChangedEvent()
  End Sub

  Protected Overrides Sub GridSelectedItemChanged()
    If m_updating Then
      Exit Sub
    End If

    RaiseSelectedGameChangedEvent()
  End Sub


  Protected Overrides Sub UpdateControlStatus()
    Call MyBase.UpdateControlStatus()
    Call ChangeGames()
    RaiseSelectedGameChangedEvent()
  End Sub


  Private Sub uc_game_sel_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Resize
    Me.SuspendLayout()
    If Me.Width <> 375 Then
      Me.Width = 375
    End If
    If Me.Height <> 80 Then
      Me.Height = 80
    End If
    Call Me.ResumeLayout(False)
  End Sub
End Class
