<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_sas_flags
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(uc_sas_flags))
    Me.lbl_handpays = New System.Windows.Forms.Label()
    Me.lbl_promotions = New System.Windows.Forms.Label()
    Me.lbl_extended_meters = New System.Windows.Forms.Label()
    Me.lbl_progressivemeters = New System.Windows.Forms.Label()
    Me.ckb_no_RTE = New System.Windows.Forms.CheckBox()
    Me.chk_single_byte = New System.Windows.Forms.CheckBox()
    Me.chk_ignore_no_bet_plays = New System.Windows.Forms.CheckBox()
    Me.chk_aft_although_lock_0 = New System.Windows.Forms.CheckBox()
    Me.lbl_enable_disable_note_acceptor = New System.Windows.Forms.Label()
    Me.cmb_enable_disable_note_acceptor = New GUI_Controls.uc_combo()
    Me.cmb_progressivemeters = New GUI_Controls.uc_combo()
    Me.ef_4bcd = New GUI_Controls.uc_entry_field()
    Me.ef_5bcd = New GUI_Controls.uc_entry_field()
    Me.cmb_promotional_credits = New GUI_Controls.uc_combo()
    Me.cmb_extended_meters = New GUI_Controls.uc_combo()
    Me.cmb_cmd1B_handpays = New GUI_Controls.uc_combo()
    Me.chk_lock_egm_on_reserve = New System.Windows.Forms.CheckBox()
    Me.SuspendLayout()
    '
    'lbl_handpays
    '
    Me.lbl_handpays.AutoSize = True
    Me.lbl_handpays.Location = New System.Drawing.Point(4, 87)
    Me.lbl_handpays.Name = "lbl_handpays"
    Me.lbl_handpays.Size = New System.Drawing.Size(61, 13)
    Me.lbl_handpays.TabIndex = 6
    Me.lbl_handpays.Text = "xHandPays"
    '
    'lbl_promotions
    '
    Me.lbl_promotions.AutoSize = True
    Me.lbl_promotions.Location = New System.Drawing.Point(4, 42)
    Me.lbl_promotions.Name = "lbl_promotions"
    Me.lbl_promotions.Size = New System.Drawing.Size(64, 13)
    Me.lbl_promotions.TabIndex = 5
    Me.lbl_promotions.Text = "xPromotions"
    Me.lbl_promotions.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'lbl_extended_meters
    '
    Me.lbl_extended_meters.AutoSize = True
    Me.lbl_extended_meters.Location = New System.Drawing.Point(4, 1)
    Me.lbl_extended_meters.Name = "lbl_extended_meters"
    Me.lbl_extended_meters.Size = New System.Drawing.Size(94, 13)
    Me.lbl_extended_meters.TabIndex = 4
    Me.lbl_extended_meters.Text = "xExtended_meters"
    '
    'lbl_progressivemeters
    '
    Me.lbl_progressivemeters.AutoSize = True
    Me.lbl_progressivemeters.Location = New System.Drawing.Point(4, 129)
    Me.lbl_progressivemeters.Name = "lbl_progressivemeters"
    Me.lbl_progressivemeters.Size = New System.Drawing.Size(94, 13)
    Me.lbl_progressivemeters.TabIndex = 7
    Me.lbl_progressivemeters.Text = "xProgressiveMeter"
    '
    'ckb_no_RTE
    '
    Me.ckb_no_RTE.Location = New System.Drawing.Point(214, -2)
    Me.ckb_no_RTE.Name = "ckb_no_RTE"
    Me.ckb_no_RTE.Size = New System.Drawing.Size(137, 19)
    Me.ckb_no_RTE.TabIndex = 11
    Me.ckb_no_RTE.Text = "xNoRTE"
    Me.ckb_no_RTE.UseVisualStyleBackColor = True
    '
    'chk_single_byte
    '
    Me.chk_single_byte.Location = New System.Drawing.Point(214, 14)
    Me.chk_single_byte.Name = "chk_single_byte"
    Me.chk_single_byte.Size = New System.Drawing.Size(139, 18)
    Me.chk_single_byte.TabIndex = 12
    Me.chk_single_byte.Text = "xSingleByte"
    Me.chk_single_byte.UseVisualStyleBackColor = True
    '
    'chk_ignore_no_bet_plays
    '
    Me.chk_ignore_no_bet_plays.Location = New System.Drawing.Point(214, 30)
    Me.chk_ignore_no_bet_plays.Name = "chk_ignore_no_bet_plays"
    Me.chk_ignore_no_bet_plays.Size = New System.Drawing.Size(141, 18)
    Me.chk_ignore_no_bet_plays.TabIndex = 13
    Me.chk_ignore_no_bet_plays.Text = "xIgnoreNoBetPlays"
    Me.chk_ignore_no_bet_plays.UseVisualStyleBackColor = True
    '
    'chk_aft_although_lock_0
    '
    Me.chk_aft_although_lock_0.Location = New System.Drawing.Point(214, 106)
    Me.chk_aft_although_lock_0.Name = "chk_aft_although_lock_0"
    Me.chk_aft_although_lock_0.Size = New System.Drawing.Size(133, 18)
    Me.chk_aft_although_lock_0.TabIndex = 14
    Me.chk_aft_although_lock_0.Text = "xAFTAlthoughLock0"
    Me.chk_aft_although_lock_0.UseVisualStyleBackColor = True
    '
    'lbl_enable_disable_note_acceptor
    '
    Me.lbl_enable_disable_note_acceptor.AutoSize = True
    Me.lbl_enable_disable_note_acceptor.Location = New System.Drawing.Point(7, 169)
    Me.lbl_enable_disable_note_acceptor.Name = "lbl_enable_disable_note_acceptor"
    Me.lbl_enable_disable_note_acceptor.Size = New System.Drawing.Size(145, 13)
    Me.lbl_enable_disable_note_acceptor.TabIndex = 15
    Me.lbl_enable_disable_note_acceptor.Text = "xEnable/DisableNoteAceptor"
    '
    'cmb_enable_disable_note_acceptor
    '
    Me.cmb_enable_disable_note_acceptor.AllowUnlistedValues = False
    Me.cmb_enable_disable_note_acceptor.AutoCompleteMode = False
    Me.cmb_enable_disable_note_acceptor.IsReadOnly = False
    Me.cmb_enable_disable_note_acceptor.Location = New System.Drawing.Point(4, 183)
    Me.cmb_enable_disable_note_acceptor.Name = "cmb_enable_disable_note_acceptor"
    Me.cmb_enable_disable_note_acceptor.SelectedIndex = -1
    Me.cmb_enable_disable_note_acceptor.Size = New System.Drawing.Size(202, 24)
    Me.cmb_enable_disable_note_acceptor.SufixText = "Sufix Text"
    Me.cmb_enable_disable_note_acceptor.SufixTextVisible = True
    Me.cmb_enable_disable_note_acceptor.TabIndex = 16
    Me.cmb_enable_disable_note_acceptor.TextCombo = Nothing
    Me.cmb_enable_disable_note_acceptor.TextWidth = 0
    '
    'cmb_progressivemeters
    '
    Me.cmb_progressivemeters.AllowUnlistedValues = False
    Me.cmb_progressivemeters.AutoCompleteMode = False
    Me.cmb_progressivemeters.IsReadOnly = False
    Me.cmb_progressivemeters.Location = New System.Drawing.Point(4, 143)
    Me.cmb_progressivemeters.Name = "cmb_progressivemeters"
    Me.cmb_progressivemeters.SelectedIndex = -1
    Me.cmb_progressivemeters.Size = New System.Drawing.Size(202, 24)
    Me.cmb_progressivemeters.SufixText = "Sufix Text"
    Me.cmb_progressivemeters.SufixTextVisible = True
    Me.cmb_progressivemeters.TabIndex = 8
    Me.cmb_progressivemeters.TextCombo = Nothing
    Me.cmb_progressivemeters.TextWidth = 0
    '
    'ef_4bcd
    '
    Me.ef_4bcd.DoubleValue = 0.0R
    Me.ef_4bcd.IntegerValue = 0
    Me.ef_4bcd.IsReadOnly = False
    Me.ef_4bcd.Location = New System.Drawing.Point(207, 46)
    Me.ef_4bcd.Name = "ef_4bcd"
    Me.ef_4bcd.PlaceHolder = Nothing
    Me.ef_4bcd.ShortcutsEnabled = True
    Me.ef_4bcd.Size = New System.Drawing.Size(144, 24)
    Me.ef_4bcd.SufixText = "Sufix Text"
    Me.ef_4bcd.SufixTextVisible = True
    Me.ef_4bcd.TabIndex = 9
    Me.ef_4bcd.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_4bcd.TextValue = ""
    Me.ef_4bcd.TextWidth = 105
    Me.ef_4bcd.Value = ""
    Me.ef_4bcd.ValueForeColor = System.Drawing.Color.Blue
    Me.ef_4bcd.Visible = False
    '
    'ef_5bcd
    '
    Me.ef_5bcd.DoubleValue = 0.0R
    Me.ef_5bcd.IntegerValue = 0
    Me.ef_5bcd.IsReadOnly = False
    Me.ef_5bcd.Location = New System.Drawing.Point(207, 68)
    Me.ef_5bcd.Name = "ef_5bcd"
    Me.ef_5bcd.PlaceHolder = Nothing
    Me.ef_5bcd.ShortcutsEnabled = True
    Me.ef_5bcd.Size = New System.Drawing.Size(144, 24)
    Me.ef_5bcd.SufixText = "Sufix Text"
    Me.ef_5bcd.SufixTextVisible = True
    Me.ef_5bcd.TabIndex = 10
    Me.ef_5bcd.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_5bcd.TextValue = ""
    Me.ef_5bcd.TextWidth = 105
    Me.ef_5bcd.Value = ""
    Me.ef_5bcd.ValueForeColor = System.Drawing.Color.Blue
    Me.ef_5bcd.Visible = False
    '
    'cmb_promotional_credits
    '
    Me.cmb_promotional_credits.AllowUnlistedValues = False
    Me.cmb_promotional_credits.AutoCompleteMode = False
    Me.cmb_promotional_credits.IsReadOnly = False
    Me.cmb_promotional_credits.Location = New System.Drawing.Point(4, 58)
    Me.cmb_promotional_credits.Name = "cmb_promotional_credits"
    Me.cmb_promotional_credits.SelectedIndex = -1
    Me.cmb_promotional_credits.Size = New System.Drawing.Size(202, 24)
    Me.cmb_promotional_credits.SufixText = "Sufix Text"
    Me.cmb_promotional_credits.SufixTextVisible = True
    Me.cmb_promotional_credits.TabIndex = 2
    Me.cmb_promotional_credits.TextCombo = Nothing
    Me.cmb_promotional_credits.TextWidth = 0
    '
    'cmb_extended_meters
    '
    Me.cmb_extended_meters.AllowUnlistedValues = False
    Me.cmb_extended_meters.AutoCompleteMode = False
    Me.cmb_extended_meters.IsReadOnly = False
    Me.cmb_extended_meters.Location = New System.Drawing.Point(4, 17)
    Me.cmb_extended_meters.Name = "cmb_extended_meters"
    Me.cmb_extended_meters.SelectedIndex = -1
    Me.cmb_extended_meters.Size = New System.Drawing.Size(202, 24)
    Me.cmb_extended_meters.SufixText = "Sufix Text"
    Me.cmb_extended_meters.SufixTextVisible = True
    Me.cmb_extended_meters.TabIndex = 1
    Me.cmb_extended_meters.TextCombo = Nothing
    Me.cmb_extended_meters.TextWidth = 0
    '
    'cmb_cmd1B_handpays
    '
    Me.cmb_cmd1B_handpays.AllowUnlistedValues = False
    Me.cmb_cmd1B_handpays.AutoCompleteMode = False
    Me.cmb_cmd1B_handpays.IsReadOnly = False
    Me.cmb_cmd1B_handpays.Location = New System.Drawing.Point(4, 103)
    Me.cmb_cmd1B_handpays.Name = "cmb_cmd1B_handpays"
    Me.cmb_cmd1B_handpays.SelectedIndex = -1
    Me.cmb_cmd1B_handpays.Size = New System.Drawing.Size(202, 24)
    Me.cmb_cmd1B_handpays.SufixText = "Sufix Text"
    Me.cmb_cmd1B_handpays.SufixTextVisible = True
    Me.cmb_cmd1B_handpays.TabIndex = 3
    Me.cmb_cmd1B_handpays.TextCombo = Nothing
    Me.cmb_cmd1B_handpays.TextWidth = 0
    '
    'chk_lock_egm_on_reserve
    '
    Me.chk_lock_egm_on_reserve.CheckAlign = System.Drawing.ContentAlignment.TopLeft
    Me.chk_lock_egm_on_reserve.Location = New System.Drawing.Point(214, 144)
    Me.chk_lock_egm_on_reserve.Name = "chk_lock_egm_on_reserve"
    Me.chk_lock_egm_on_reserve.Size = New System.Drawing.Size(133, 38)
    Me.chk_lock_egm_on_reserve.TabIndex = 17
    Me.chk_lock_egm_on_reserve.Text = "xLockEgmOnReserve"
    Me.chk_lock_egm_on_reserve.TextAlign = System.Drawing.ContentAlignment.TopLeft
    Me.chk_lock_egm_on_reserve.UseVisualStyleBackColor = True
    '
    'uc_sas_flags
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
    Me.Controls.Add(Me.chk_lock_egm_on_reserve)
    Me.Controls.Add(Me.cmb_enable_disable_note_acceptor)
    Me.Controls.Add(Me.lbl_enable_disable_note_acceptor)
    Me.Controls.Add(Me.chk_aft_although_lock_0)
    Me.Controls.Add(Me.chk_ignore_no_bet_plays)
    Me.Controls.Add(Me.chk_single_byte)
    Me.Controls.Add(Me.ckb_no_RTE)
    Me.Controls.Add(Me.cmb_progressivemeters)
    Me.Controls.Add(Me.lbl_handpays)
    Me.Controls.Add(Me.ef_4bcd)
    Me.Controls.Add(Me.lbl_promotions)
    Me.Controls.Add(Me.ef_5bcd)
    Me.Controls.Add(Me.cmb_promotional_credits)
    Me.Controls.Add(Me.lbl_progressivemeters)
    Me.Controls.Add(Me.cmb_extended_meters)
    Me.Controls.Add(Me.cmb_cmd1B_handpays)
    Me.Controls.Add(Me.lbl_extended_meters)
    Me.Name = "uc_sas_flags"
    Me.Size = New System.Drawing.Size(350, 218)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Protected WithEvents cmb_extended_meters As GUI_Controls.uc_combo
  Protected WithEvents cmb_promotional_credits As GUI_Controls.uc_combo
  Protected WithEvents cmb_cmd1B_handpays As GUI_Controls.uc_combo
  Friend WithEvents lbl_handpays As System.Windows.Forms.Label
  Friend WithEvents lbl_promotions As System.Windows.Forms.Label
  Friend WithEvents lbl_extended_meters As System.Windows.Forms.Label
  Friend WithEvents lbl_progressivemeters As System.Windows.Forms.Label
  Protected WithEvents cmb_progressivemeters As GUI_Controls.uc_combo
  Friend WithEvents ef_4bcd As GUI_Controls.uc_entry_field
  Friend WithEvents ef_5bcd As GUI_Controls.uc_entry_field
  Friend WithEvents ckb_no_RTE As System.Windows.Forms.CheckBox
  Friend WithEvents chk_single_byte As System.Windows.Forms.CheckBox
  Friend WithEvents chk_ignore_no_bet_plays As System.Windows.Forms.CheckBox
  Friend WithEvents chk_aft_although_lock_0 As System.Windows.Forms.CheckBox
  Protected WithEvents cmb_enable_disable_note_acceptor As GUI_Controls.uc_combo
  Friend WithEvents lbl_enable_disable_note_acceptor As System.Windows.Forms.Label
  Friend WithEvents chk_lock_egm_on_reserve As System.Windows.Forms.CheckBox

End Class
