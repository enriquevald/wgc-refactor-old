'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_combo_games
' DESCRIPTION:   Filter of games 
' AUTHOR:        Fernando Jim�nez
' CREATION DATE: 14-APR-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 14-APR-2015  FJC    Initial version
'--------------------------------------------------------------------

Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports System.Windows.Forms

Public Class uc_combo_games

#Region " Private functions"

  ' PURPOSE: Execute Query for filling data to the combo
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddGames()
    Dim _sql_select As StringBuilder
    Dim _tabla As DataTable

    Try

      _sql_select = New StringBuilder()
      _sql_select.AppendLine("    SELECT   GAMES_V2.GM_GAME_ID                          ")
      _sql_select.AppendLine("           , PROVIDERS.PV_NAME + ' - ' + GAMES_V2.GM_NAME ")
      _sql_select.AppendLine("      FROM   GAMES_V2                                     ")
      _sql_select.AppendLine(" LEFT JOIN   PROVIDERS                                    ")
      _sql_select.AppendLine("        ON   PROVIDERS.PV_ID = GAMES_V2.PV_ID             ")
      _sql_select.AppendLine("     WHERE   GAMES_V2.GM_NAME <> 'UNKNOWN'                ")
      _sql_select.AppendLine("  ORDER BY   PV_NAME ASC                                  ")

      _tabla = GUI_GetTableUsingSQL(_sql_select.ToString(), Integer.MaxValue)

      Me.cmb_games.Clear()
      Me.cmb_games.Add(_tabla)

    Catch ex As Exception

      WSI.Common.Log.Exception(ex)
    End Try
  End Sub ' AddGames

  ' PURPOSE: Resize some controls for a correct visualization
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ResizeControl()
    Me.gb_game.Size = New System.Drawing.Size(Me.Size.Width - 5, Me.Size.Height - 5)
    Me.cmb_games.Size = New System.Drawing.Size(Me.gb_game.Width - Me.cmb_games.Location.X - 5, _
                                                Me.cmb_games.Size.Height)

  End Sub ' ResizeControl

#End Region

#Region " Public funtions"

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    ResizeControl()
  End Sub

  ' PURPOSE: Initialize Control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub Init()

    'NL's
    Me.gb_game.Text = GLB_NLS_GUI_STATISTICS.GetString(215)
    Me.opt_several_game.Text = GLB_NLS_GUI_STATISTICS.GetString(205)
    Me.opt_all_game.Text = GLB_NLS_GUI_STATISTICS.GetString(206)

    'Handlers
    AddHandler opt_all_game.CheckedChanged, AddressOf CheckOptSelect
    AddHandler opt_several_game.CheckedChanged, AddressOf CheckOptSelect
    AddHandler Me.Resize, AddressOf UcComboGamesResizing

    'Defaults
    Me.SetDefaultValues()

    'AddGames
    Call AddGames()

    'ResizeControl
    Call ResizeControl()

  End Sub ' Init

  ' PURPOSE: Get Id (Value) from selected value of combobox
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Public Function GetSelectedId() As String
    Try
      If Me.opt_several_game.Checked Then
        If Me.cmb_games.Count > 0 Then

          Return Me.cmb_games.Value
        End If

      End If
    Catch ex As Exception

      WSI.Common.Log.Exception(ex)
    End Try

    Return String.Empty
  End Function ' GetSelectedId

  ' PURPOSE: Get Text (TextValue) from selected value of combobox
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Public Function GetSelectedValue() As String
    Try
      If Me.opt_several_game.Checked Then
        If Me.cmb_games.Count > 0 Then

          Return Me.cmb_games.TextValue
        End If

      End If
    Catch ex As Exception

      WSI.Common.Log.Exception(ex)
    End Try

    Return String.Empty
  End Function  ' GetSelectedValue

  ' PURPOSE: Set Default Values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub SetDefaultValues()
    Me.opt_all_game.Checked = True
    Me.cmb_games.Enabled = False
  End Sub ' SetDefaultValues

#End Region

#Region " Events"

  ' PURPOSE: Controles events por resizing user control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub UcComboGamesResizing(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
    ResizeControl()
  End Sub

  ' PURPOSE: Controles events por radiobuttons
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub CheckOptSelect(ByVal sender As System.Object, ByVal e As System.EventArgs)
    If TypeOf sender Is RadioButton Then

      If sender.name = "opt_several_game" Then
        Me.cmb_games.Enabled = True
      Else
        Me.cmb_games.Enabled = False
      End If
    End If
  End Sub

#End Region

End Class ' uc_combo_games
