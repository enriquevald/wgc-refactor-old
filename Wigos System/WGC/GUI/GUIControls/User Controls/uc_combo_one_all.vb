'---------------------------------------------------------------------
' Copyright © 2014 Win Systems Ltd.
'---------------------------------------------------------------------
' 
' MODULE NAME:   uc_combo_one_all.vb  
'
' DESCRIPTION:   Control with combo and radio buttons "One" and "All"
'
' AUTHOR:        Javier Fernández
'
' CREATION DATE: 29-MAY-2014
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ ------------------------------------------------
' 29-MAY-2014  JFC    Initial version
'---------------------------------------------------------------------

Public Class uc_combo_one_all

#Region "Constructors"
  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    InitControls()

  End Sub
#End Region

#Region "Properties"

  Public ReadOnly Property Combo() As uc_combo
    Get
      Return Me.cmb_sel
    End Get
  End Property

  Public Overloads Property Text() As String
    Get
      Return Me.gb_combo_one_all.Name
    End Get
    Set(ByVal value As String)
      Me.gb_combo_one_all.Text = value
    End Set
  End Property

  Public Property SelectOne() As Integer
    Get
      Return Me.cmb_sel.SelectedIndex
    End Get
    Set(ByVal value As Integer)
      Me.opt_one.Checked = True
      Me.cmb_sel.SelectedIndex = value
    End Set
  End Property

#End Region

#Region "Private"

  Private Sub InitControls()

    Me.opt_one.Text = GLB_NLS_GUI_INVOICING.GetString(218)
    Me.opt_all.Text = GLB_NLS_GUI_INVOICING.GetString(219)

  End Sub

#End Region

#Region "Public"

  Public Sub SetDefaultValues()
    Me.opt_all.Checked = True
    Me.cmb_sel.Enabled = False
  End Sub

  Public Function IsOneChecked() As Boolean
    Return Me.opt_one.Checked
  End Function

#End Region

#Region "Events"

  Private Sub opt_one_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_one.Click
    Me.cmb_sel.Enabled = True
  End Sub

  Private Sub opt_all_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all.Click
    Me.cmb_sel.Enabled = False
  End Sub

#End Region

End Class
