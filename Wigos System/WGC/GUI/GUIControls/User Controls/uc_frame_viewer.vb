'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_uc_frame_viewer.vb  
'
' DESCRIPTION:   Frame viewer control
'
' AUTHOR:        Jos� Mart�nez
'
' CREATION DATE: 05-JUL-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 05-JUL-2012  JML    Initial version
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls

Public Class uc_frame_viewer

#Region " Members "
  Dim m_frame() As Byte = Nothing

#End Region ' Members

#Region " Public Functions "

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

  End Sub

  ' PURPOSE: Init the control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  ' 
  Public Sub Init(ByVal Title As String, ByVal ChkBox1Text As String, ByVal ChkBox2Text As String)

    Call InitControls(Title, ChkBox1Text, ChkBox2Text)

  End Sub ' Init

  ' PURPOSE: Set default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub SetDefaultValues()

    Me.cb_hexa.Checked = False
    Me.cb_binary.Checked = False
    Me.lbl_frame.Text = ""

  End Sub ' SetDefaultValues

  Public Sub SetFrame(ByVal Frame() As Byte)

    m_frame = Frame
    RefreshFrameValue()

  End Sub ' FrameValue

#End Region ' Public Functions

#Region " Private Functions "

  Private Sub InitControls(ByVal Title As String, ByVal ChkBox1Text As String, ByVal ChkBox2Text As String)

    Me.gb_frame.Text = Title
    Me.cb_hexa.Text = ChkBox1Text
    Me.cb_binary.Text = ChkBox2Text

    SetDefaultValues()

  End Sub ' InitControls

  Private Sub RefreshFrameValue()

    Me.Invalidate()

    If IsNothing(m_frame) Then
      Me.lbl_frame.Text = ""

      Exit Sub
    End If

    Me.lbl_frame.Text = WSI.Common.PeruFrameViewer.ToString(m_frame, Me.cb_hexa.Checked, Me.cb_binary.Checked)

  End Sub ' RefreshFrameValue

#End Region ' Private Functions

#Region "Events"

  Private Sub cb_hexa_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_hexa.CheckedChanged
    RefreshFrameValue()
  End Sub

  Private Sub cb_binary_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_binary.CheckedChanged
    RefreshFrameValue()
  End Sub

#End Region

End Class ' uc_frame_viewer
