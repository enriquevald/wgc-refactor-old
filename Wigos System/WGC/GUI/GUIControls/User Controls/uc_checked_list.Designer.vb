<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_checked_list
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.btn_uncheck_all = New System.Windows.Forms.Button()
    Me.gb_checked_list = New System.Windows.Forms.GroupBox()
    Me.panel_grid = New System.Windows.Forms.Panel()
    Me.dg_check_list = New GUI_Controls.uc_grid()
    Me.btn_check_all = New System.Windows.Forms.Button()
    Me.TimerTest = New System.Windows.Forms.Timer(Me.components)
    Me.gb_checked_list.SuspendLayout()
    Me.panel_grid.SuspendLayout()
    Me.SuspendLayout()
    '
    'btn_uncheck_all
    '
    Me.btn_uncheck_all.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_uncheck_all.Location = New System.Drawing.Point(170, 124)
    Me.btn_uncheck_all.Name = "btn_uncheck_all"
    Me.btn_uncheck_all.Size = New System.Drawing.Size(125, 29)
    Me.btn_uncheck_all.TabIndex = 2
    Me.btn_uncheck_all.Text = "xUncheckAll"
    Me.btn_uncheck_all.UseVisualStyleBackColor = True
    '
    'gb_checked_list
    '
    Me.gb_checked_list.AutoSize = True
    Me.gb_checked_list.Controls.Add(Me.panel_grid)
    Me.gb_checked_list.Controls.Add(Me.btn_uncheck_all)
    Me.gb_checked_list.Controls.Add(Me.btn_check_all)
    Me.gb_checked_list.Dock = System.Windows.Forms.DockStyle.Fill
    Me.gb_checked_list.Location = New System.Drawing.Point(0, 0)
    Me.gb_checked_list.Name = "gb_checked_list"
    Me.gb_checked_list.Size = New System.Drawing.Size(301, 159)
    Me.gb_checked_list.TabIndex = 0
    Me.gb_checked_list.TabStop = False
    Me.gb_checked_list.Text = "xCheckedList"
    '
    'panel_grid
    '
    Me.panel_grid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.panel_grid.AutoScroll = True
    Me.panel_grid.Controls.Add(Me.dg_check_list)
    Me.panel_grid.Location = New System.Drawing.Point(4, 15)
    Me.panel_grid.Margin = New System.Windows.Forms.Padding(0)
    Me.panel_grid.Name = "panel_grid"
    Me.panel_grid.Size = New System.Drawing.Size(292, 104)
    Me.panel_grid.TabIndex = 0
    '
    'dg_check_list
    '
    Me.dg_check_list.AutoScroll = True
    Me.dg_check_list.CurrentCol = -1
    Me.dg_check_list.CurrentRow = -1
    Me.dg_check_list.Dock = System.Windows.Forms.DockStyle.Fill
    Me.dg_check_list.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_check_list.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_check_list.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_check_list.Location = New System.Drawing.Point(0, 0)
    Me.dg_check_list.Margin = New System.Windows.Forms.Padding(0)
    Me.dg_check_list.MinimumSize = New System.Drawing.Size(292, 100)
    Me.dg_check_list.Name = "dg_check_list"
    Me.dg_check_list.PanelRightVisible = False
    Me.dg_check_list.Redraw = True
    Me.dg_check_list.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_check_list.Size = New System.Drawing.Size(292, 104)
    Me.dg_check_list.Sortable = False
    Me.dg_check_list.SortAscending = True
    Me.dg_check_list.SortByCol = 0
    Me.dg_check_list.TabIndex = 0
    Me.dg_check_list.ToolTipped = True
    Me.dg_check_list.TopRow = -2
    Me.dg_check_list.WordWrap = False
    '
    'btn_check_all
    '
    Me.btn_check_all.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btn_check_all.Location = New System.Drawing.Point(6, 124)
    Me.btn_check_all.Name = "btn_check_all"
    Me.btn_check_all.Size = New System.Drawing.Size(125, 29)
    Me.btn_check_all.TabIndex = 1
    Me.btn_check_all.Text = "xCheckAll"
    Me.btn_check_all.UseVisualStyleBackColor = True
    '
    'TimerTest
    '
    Me.TimerTest.Interval = 1
    '
    'uc_checked_list
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.gb_checked_list)
    Me.Name = "uc_checked_list"
    Me.Size = New System.Drawing.Size(301, 159)
    Me.gb_checked_list.ResumeLayout(False)
    Me.panel_grid.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents gb_checked_list As System.Windows.Forms.GroupBox
  Public WithEvents btn_uncheck_all As System.Windows.Forms.Button
  Public WithEvents btn_check_all As System.Windows.Forms.Button
  Friend WithEvents dg_check_list As GUI_Controls.uc_grid
  Friend WithEvents panel_grid As System.Windows.Forms.Panel
  Friend WithEvents TimerTest As System.Windows.Forms.Timer

End Class
