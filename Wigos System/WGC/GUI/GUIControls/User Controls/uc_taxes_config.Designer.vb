<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_taxes_config
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.tab_voucher = New System.Windows.Forms.TabControl
    Me.gb_voucher_texts = New System.Windows.Forms.GroupBox
    Me.uc_dev_title = New GUI_Controls.uc_entry_field
    Me.uc_pay_title = New GUI_Controls.uc_entry_field
    Me.chk_PromoAndCouponTogether = New System.Windows.Forms.CheckBox
    Me.uc_text_PromoAndCouponTogether = New GUI_Controls.uc_entry_field
    Me.uc_text_prize_coupon = New GUI_Controls.uc_entry_field
    Me.chk_hide_promo = New System.Windows.Forms.CheckBox
    Me.chk_hide_info_cashin = New System.Windows.Forms.CheckBox
    Me.uc_text_promotion = New GUI_Controls.uc_entry_field
    Me.Label3 = New System.Windows.Forms.Label
    Me.gb_business = New System.Windows.Forms.GroupBox
    Me.uc_entry_tax_pct = New GUI_Controls.uc_entry_field
    Me.uc_entry_tax_name = New GUI_Controls.uc_entry_field
    Me.uc_concept_name = New GUI_Controls.uc_entry_field
    Me.uc_entry_business_name = New GUI_Controls.uc_entry_field
    Me.uc_cashin_pct = New GUI_Controls.uc_entry_field
    Me.Label1 = New System.Windows.Forms.Label
    Me.Label2 = New System.Windows.Forms.Label
    Me.chk_prize_coupon = New System.Windows.Forms.CheckBox
    Me.gb_voucher_texts.SuspendLayout()
    Me.gb_business.SuspendLayout()
    Me.SuspendLayout()
    '
    'tab_voucher
    '
    Me.tab_voucher.Location = New System.Drawing.Point(445, 3)
    Me.tab_voucher.Name = "tab_voucher"
    Me.tab_voucher.SelectedIndex = 0
    Me.tab_voucher.Size = New System.Drawing.Size(435, 424)
    Me.tab_voucher.TabIndex = 1
    '
    'gb_voucher_texts
    '
    Me.gb_voucher_texts.Controls.Add(Me.uc_dev_title)
    Me.gb_voucher_texts.Controls.Add(Me.uc_pay_title)
    Me.gb_voucher_texts.Controls.Add(Me.chk_PromoAndCouponTogether)
    Me.gb_voucher_texts.Controls.Add(Me.uc_text_PromoAndCouponTogether)
    Me.gb_voucher_texts.Controls.Add(Me.uc_text_prize_coupon)
    Me.gb_voucher_texts.Controls.Add(Me.chk_hide_promo)
    Me.gb_voucher_texts.Controls.Add(Me.chk_hide_info_cashin)
    Me.gb_voucher_texts.Controls.Add(Me.uc_text_promotion)
    Me.gb_voucher_texts.Location = New System.Drawing.Point(23, 156)
    Me.gb_voucher_texts.Name = "gb_voucher_texts"
    Me.gb_voucher_texts.Size = New System.Drawing.Size(387, 244)
    Me.gb_voucher_texts.TabIndex = 10
    Me.gb_voucher_texts.TabStop = False
    Me.gb_voucher_texts.Text = "xVoucherTexts"
    '
    'uc_dev_title
    '
    Me.uc_dev_title.DoubleValue = 0
    Me.uc_dev_title.IntegerValue = 0
    Me.uc_dev_title.IsReadOnly = False
    Me.uc_dev_title.Location = New System.Drawing.Point(6, 49)
    Me.uc_dev_title.Name = "uc_dev_title"
    Me.uc_dev_title.Size = New System.Drawing.Size(370, 24)
    Me.uc_dev_title.SufixText = "Sufix Text"
    Me.uc_dev_title.SufixTextVisible = True
    Me.uc_dev_title.TabIndex = 7
    Me.uc_dev_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_dev_title.TextValue = ""
    Me.uc_dev_title.TextWidth = 120
    Me.uc_dev_title.Value = ""
    '
    'uc_pay_title
    '
    Me.uc_pay_title.DoubleValue = 0
    Me.uc_pay_title.IntegerValue = 0
    Me.uc_pay_title.IsReadOnly = False
    Me.uc_pay_title.Location = New System.Drawing.Point(6, 19)
    Me.uc_pay_title.Name = "uc_pay_title"
    Me.uc_pay_title.Size = New System.Drawing.Size(370, 24)
    Me.uc_pay_title.SufixText = "Sufix Text"
    Me.uc_pay_title.SufixTextVisible = True
    Me.uc_pay_title.TabIndex = 6
    Me.uc_pay_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_pay_title.TextValue = ""
    Me.uc_pay_title.TextWidth = 120
    Me.uc_pay_title.Value = ""
    '
    'chk_PromoAndCouponTogether
    '
    Me.chk_PromoAndCouponTogether.AutoSize = True
    Me.chk_PromoAndCouponTogether.Location = New System.Drawing.Point(23, 116)
    Me.chk_PromoAndCouponTogether.Name = "chk_PromoAndCouponTogether"
    Me.chk_PromoAndCouponTogether.Size = New System.Drawing.Size(74, 17)
    Me.chk_PromoAndCouponTogether.TabIndex = 1
    Me.chk_PromoAndCouponTogether.Text = "xTogether"
    Me.chk_PromoAndCouponTogether.UseVisualStyleBackColor = True
    '
    'uc_text_PromoAndCouponTogether
    '
    Me.uc_text_PromoAndCouponTogether.DoubleValue = 0
    Me.uc_text_PromoAndCouponTogether.Enabled = False
    Me.uc_text_PromoAndCouponTogether.ForeColor = System.Drawing.SystemColors.ControlText
    Me.uc_text_PromoAndCouponTogether.IntegerValue = 0
    Me.uc_text_PromoAndCouponTogether.IsReadOnly = False
    Me.uc_text_PromoAndCouponTogether.Location = New System.Drawing.Point(168, 112)
    Me.uc_text_PromoAndCouponTogether.Name = "uc_text_PromoAndCouponTogether"
    Me.uc_text_PromoAndCouponTogether.Size = New System.Drawing.Size(208, 24)
    Me.uc_text_PromoAndCouponTogether.SufixText = "Sufix Text"
    Me.uc_text_PromoAndCouponTogether.SufixTextVisible = True
    Me.uc_text_PromoAndCouponTogether.TabIndex = 2
    Me.uc_text_PromoAndCouponTogether.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_text_PromoAndCouponTogether.TextValue = ""
    Me.uc_text_PromoAndCouponTogether.TextWidth = 0
    Me.uc_text_PromoAndCouponTogether.Value = ""
    '
    'uc_text_prize_coupon
    '
    Me.uc_text_prize_coupon.DoubleValue = 0
    Me.uc_text_prize_coupon.Enabled = False
    Me.uc_text_prize_coupon.ForeColor = System.Drawing.SystemColors.ControlText
    Me.uc_text_prize_coupon.IntegerValue = 0
    Me.uc_text_prize_coupon.IsReadOnly = False
    Me.uc_text_prize_coupon.Location = New System.Drawing.Point(52, 150)
    Me.uc_text_prize_coupon.Name = "uc_text_prize_coupon"
    Me.uc_text_prize_coupon.Size = New System.Drawing.Size(324, 24)
    Me.uc_text_prize_coupon.SufixText = "Sufix Text"
    Me.uc_text_prize_coupon.SufixTextVisible = True
    Me.uc_text_prize_coupon.TabIndex = 3
    Me.uc_text_prize_coupon.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_text_prize_coupon.TextValue = ""
    Me.uc_text_prize_coupon.TextWidth = 115
    Me.uc_text_prize_coupon.Value = ""
    '
    'chk_hide_promo
    '
    Me.chk_hide_promo.AutoSize = True
    Me.chk_hide_promo.Location = New System.Drawing.Point(23, 182)
    Me.chk_hide_promo.Name = "chk_hide_promo"
    Me.chk_hide_promo.Size = New System.Drawing.Size(106, 17)
    Me.chk_hide_promo.TabIndex = 4
    Me.chk_hide_promo.Text = "xHide_Promotion"
    Me.chk_hide_promo.UseVisualStyleBackColor = True
    '
    'chk_hide_info_cashin
    '
    Me.chk_hide_info_cashin.AutoSize = True
    Me.chk_hide_info_cashin.Location = New System.Drawing.Point(23, 87)
    Me.chk_hide_info_cashin.Name = "chk_hide_info_cashin"
    Me.chk_hide_info_cashin.Size = New System.Drawing.Size(116, 17)
    Me.chk_hide_info_cashin.TabIndex = 0
    Me.chk_hide_info_cashin.Text = "xHide_Info_CashIn"
    Me.chk_hide_info_cashin.UseVisualStyleBackColor = True
    '
    'uc_text_promotion
    '
    Me.uc_text_promotion.DoubleValue = 0
    Me.uc_text_promotion.Enabled = False
    Me.uc_text_promotion.ForeColor = System.Drawing.SystemColors.ControlText
    Me.uc_text_promotion.IntegerValue = 0
    Me.uc_text_promotion.IsReadOnly = False
    Me.uc_text_promotion.Location = New System.Drawing.Point(52, 205)
    Me.uc_text_promotion.Name = "uc_text_promotion"
    Me.uc_text_promotion.Size = New System.Drawing.Size(324, 24)
    Me.uc_text_promotion.SufixText = "Sufix Text"
    Me.uc_text_promotion.SufixTextVisible = True
    Me.uc_text_promotion.TabIndex = 5
    Me.uc_text_promotion.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_text_promotion.TextValue = ""
    Me.uc_text_promotion.TextWidth = 115
    Me.uc_text_promotion.Value = ""
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(408, 110)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(15, 24)
    Me.Label3.TabIndex = 8
    Me.Label3.Text = "%"
    Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'gb_business
    '
    Me.gb_business.Controls.Add(Me.chk_prize_coupon)
    Me.gb_business.Controls.Add(Me.Label3)
    Me.gb_business.Controls.Add(Me.gb_voucher_texts)
    Me.gb_business.Controls.Add(Me.uc_entry_tax_pct)
    Me.gb_business.Controls.Add(Me.uc_entry_tax_name)
    Me.gb_business.Controls.Add(Me.uc_concept_name)
    Me.gb_business.Controls.Add(Me.uc_entry_business_name)
    Me.gb_business.Controls.Add(Me.uc_cashin_pct)
    Me.gb_business.Controls.Add(Me.Label1)
    Me.gb_business.Location = New System.Drawing.Point(4, 3)
    Me.gb_business.Name = "gb_business"
    Me.gb_business.Size = New System.Drawing.Size(435, 424)
    Me.gb_business.TabIndex = 0
    Me.gb_business.TabStop = False
    '
    'uc_entry_tax_pct
    '
    Me.uc_entry_tax_pct.DoubleValue = 0
    Me.uc_entry_tax_pct.ForeColor = System.Drawing.SystemColors.ControlText
    Me.uc_entry_tax_pct.IntegerValue = 0
    Me.uc_entry_tax_pct.IsReadOnly = False
    Me.uc_entry_tax_pct.Location = New System.Drawing.Point(295, 110)
    Me.uc_entry_tax_pct.Name = "uc_entry_tax_pct"
    Me.uc_entry_tax_pct.Size = New System.Drawing.Size(115, 24)
    Me.uc_entry_tax_pct.SufixText = "Sufix Text"
    Me.uc_entry_tax_pct.SufixTextVisible = True
    Me.uc_entry_tax_pct.TabIndex = 7
    Me.uc_entry_tax_pct.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_tax_pct.TextValue = ""
    Me.uc_entry_tax_pct.TextWidth = 65
    Me.uc_entry_tax_pct.Value = ""
    '
    'uc_entry_tax_name
    '
    Me.uc_entry_tax_name.DoubleValue = 0
    Me.uc_entry_tax_name.IntegerValue = 0
    Me.uc_entry_tax_name.IsReadOnly = False
    Me.uc_entry_tax_name.Location = New System.Drawing.Point(6, 110)
    Me.uc_entry_tax_name.Name = "uc_entry_tax_name"
    Me.uc_entry_tax_name.Size = New System.Drawing.Size(283, 24)
    Me.uc_entry_tax_name.SufixText = "Sufix Text"
    Me.uc_entry_tax_name.SufixTextVisible = True
    Me.uc_entry_tax_name.TabIndex = 6
    Me.uc_entry_tax_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_tax_name.TextValue = ""
    Me.uc_entry_tax_name.Value = ""
    '
    'uc_concept_name
    '
    Me.uc_concept_name.DoubleValue = 0
    Me.uc_concept_name.IntegerValue = 0
    Me.uc_concept_name.IsReadOnly = False
    Me.uc_concept_name.Location = New System.Drawing.Point(6, 50)
    Me.uc_concept_name.Name = "uc_concept_name"
    Me.uc_concept_name.Size = New System.Drawing.Size(423, 24)
    Me.uc_concept_name.SufixText = "Sufix Text"
    Me.uc_concept_name.SufixTextVisible = True
    Me.uc_concept_name.TabIndex = 1
    Me.uc_concept_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_concept_name.TextValue = ""
    Me.uc_concept_name.Value = ""
    '
    'uc_entry_business_name
    '
    Me.uc_entry_business_name.DoubleValue = 0
    Me.uc_entry_business_name.IntegerValue = 0
    Me.uc_entry_business_name.IsReadOnly = False
    Me.uc_entry_business_name.Location = New System.Drawing.Point(6, 20)
    Me.uc_entry_business_name.Name = "uc_entry_business_name"
    Me.uc_entry_business_name.Size = New System.Drawing.Size(423, 24)
    Me.uc_entry_business_name.SufixText = "Sufix Text"
    Me.uc_entry_business_name.SufixTextVisible = True
    Me.uc_entry_business_name.TabIndex = 0
    Me.uc_entry_business_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_entry_business_name.TextValue = ""
    Me.uc_entry_business_name.Value = ""
    '
    'uc_cashin_pct
    '
    Me.uc_cashin_pct.DoubleValue = 0
    Me.uc_cashin_pct.ForeColor = System.Drawing.SystemColors.ControlText
    Me.uc_cashin_pct.IntegerValue = 0
    Me.uc_cashin_pct.IsReadOnly = False
    Me.uc_cashin_pct.Location = New System.Drawing.Point(6, 80)
    Me.uc_cashin_pct.Name = "uc_cashin_pct"
    Me.uc_cashin_pct.Size = New System.Drawing.Size(155, 24)
    Me.uc_cashin_pct.SufixText = "Sufix Text"
    Me.uc_cashin_pct.SufixTextVisible = True
    Me.uc_cashin_pct.TabIndex = 2
    Me.uc_cashin_pct.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_cashin_pct.TextValue = ""
    Me.uc_cashin_pct.Value = ""
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(159, 80)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(15, 24)
    Me.Label1.TabIndex = 3
    Me.Label1.Text = "%"
    Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(341, 80)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(15, 24)
    Me.Label2.TabIndex = 5
    Me.Label2.Text = "%"
    Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'chk_prize_coupon
    '
    Me.chk_prize_coupon.AutoSize = True
    Me.chk_prize_coupon.Location = New System.Drawing.Point(295, 85)
    Me.chk_prize_coupon.Name = "chk_prize_coupon"
    Me.chk_prize_coupon.Size = New System.Drawing.Size(92, 17)
    Me.chk_prize_coupon.TabIndex = 11
    Me.chk_prize_coupon.Text = "xPriceCoupon"
    Me.chk_prize_coupon.UseVisualStyleBackColor = True
    '
    'uc_taxes_config
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
    Me.Controls.Add(Me.tab_voucher)
    Me.Controls.Add(Me.gb_business)
    Me.Name = "uc_taxes_config"
    Me.Size = New System.Drawing.Size(882, 430)
    Me.gb_voucher_texts.ResumeLayout(False)
    Me.gb_voucher_texts.PerformLayout()
    Me.gb_business.ResumeLayout(False)
    Me.gb_business.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents tab_voucher As System.Windows.Forms.TabControl
  Friend WithEvents gb_voucher_texts As System.Windows.Forms.GroupBox
  Friend WithEvents uc_entry_tax_pct As GUI_Controls.uc_entry_field
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents uc_entry_tax_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_business As System.Windows.Forms.GroupBox
  Friend WithEvents uc_concept_name As GUI_Controls.uc_entry_field
  Friend WithEvents uc_entry_business_name As GUI_Controls.uc_entry_field
  Friend WithEvents uc_cashin_pct As GUI_Controls.uc_entry_field
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents chk_hide_promo As System.Windows.Forms.CheckBox
  Friend WithEvents uc_text_prize_coupon As GUI_Controls.uc_entry_field
  Friend WithEvents chk_hide_info_cashin As System.Windows.Forms.CheckBox
  Friend WithEvents uc_text_promotion As GUI_Controls.uc_entry_field
  Friend WithEvents chk_PromoAndCouponTogether As System.Windows.Forms.CheckBox
  Friend WithEvents uc_text_PromoAndCouponTogether As GUI_Controls.uc_entry_field
  'Friend WithEvents uc_dev_pct As GUI_Controls.uc_entry_field
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents uc_dev_title As GUI_Controls.uc_entry_field
  Friend WithEvents uc_pay_title As GUI_Controls.uc_entry_field
  Friend WithEvents chk_prize_coupon As System.Windows.Forms.CheckBox

End Class
