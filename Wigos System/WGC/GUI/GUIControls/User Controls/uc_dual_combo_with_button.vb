'---------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'---------------------------------------------------------------------
' 
' MODULE NAME:   uc_dual_combo.vb  
'
' DESCRIPTION:   Control with 2 combos
'
' AUTHOR:        Xavier Cots
'
' CREATION DATE: 25-FEB-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ ------------------------------------------------
' 25-FEB-2015  XCD    Initial version
'---------------------------------------------------------------------

Public Class uc_dual_combo_with_button

#Region "Constants"
  Private Enum UC_MODE As Integer
    MODE_BASIC = 0
    MODE_ADVANCED = 1
  End Enum
#End Region

#Region "Properties"
  Public Property ControlMode() As Integer
    Get
      Return m_control_mode
    End Get
    Set(ByVal value As Integer)
      m_control_mode = value
      RelocateComponents()
    End Set
  End Property
#End Region

#Region "Members"
  Dim m_control_mode As UC_MODE
  Public Event ControlValueChangedEvent()
  Public Event Button1ClickEvent(ByVal sender As System.Object, ByVal e As System.EventArgs)
#End Region

#Region "uc_combos"
#Region "Properties"
  Public WriteOnly Property Combo1Text() As String
    Set(ByVal value As String)
      cmb_1.Text = value
    End Set
  End Property

  Public WriteOnly Property Combo2Text() As String
    Set(ByVal value As String)
      cmb_2.Text = value
    End Set
  End Property

  Public WriteOnly Property Combo1TextWidth() As Integer
    Set(ByVal value As Integer)
      cmb_1.TextWidth = value

    End Set
  End Property

  Public WriteOnly Property Combo2TextWidth() As Integer
    Set(ByVal value As Integer)
      cmb_2.TextWidth = value

    End Set
  End Property

  Public Property Combo1Value() As Integer
    Get
      Return cmb_1.Value
    End Get
    Set(ByVal value As Integer)
      cmb_1.Value = value
    End Set
  End Property

  Public Property Combo2Value() As Integer
    Get
      Return cmb_2.Value
    End Get
    Set(ByVal value As Integer)
      cmb_2.Value = value
    End Set
  End Property

  Public Property Combo1TextValue() As String
    Get
      Return cmb_1.TextValue(cmb_1.SelectedIndex)
    End Get
    Set(ByVal value As String)
      For _idx_combo As Integer = 0 To cmb_1.Count() - 1
        If String.Equals(cmb_1.TextValue(_idx_combo), value) Then
          cmb_1.SelectedIndex = _idx_combo
          Return
        End If
      Next
      cmb_1.SelectedIndex = -1
    End Set
  End Property

  Public Property Combo2TextValue() As String
    Get
      Return cmb_2.TextValue(cmb_2.SelectedIndex)
    End Get
    Set(ByVal value As String)
      For _idx_combo As Integer = 0 To cmb_2.Count() - 1
        If String.Equals(cmb_2.TextValue(_idx_combo), value) Then
          cmb_2.SelectedIndex = _idx_combo
          Return
        End If
      Next
      cmb_2.SelectedIndex = -1
    End Set
  End Property

  Public Property Combo1AutoCompleteMode() As Boolean
    Get
      Return cmb_1.AutoCompleteMode
    End Get
    Set(ByVal value As Boolean)
      cmb_1.AutoCompleteMode = value
    End Set
  End Property

  Public Property Combo2AutoCompleteMode() As Boolean
    Get
      Return cmb_2.AutoCompleteMode
    End Get
    Set(ByVal value As Boolean)
      cmb_2.AutoCompleteMode = value
    End Set
  End Property

  Public Property Combo1IsReadOnly() As Boolean
    Get
      Return cmb_1.IsReadOnly
    End Get
    Set(ByVal value As Boolean)
      cmb_1.IsReadOnly = value
    End Set
  End Property

  Public Property Combo2IsReadOnly() As Boolean
    Get
      Return cmb_2.IsReadOnly
    End Get
    Set(ByVal value As Boolean)
      cmb_2.IsReadOnly = value
    End Set
  End Property

  Public Property Combo1BackColor() As System.Drawing.Color
    Get
      Return cmb_1.BackColor
    End Get
    Set(ByVal value As System.Drawing.Color)
      cmb_1.BackColor = value
      Me.BackColor = value
    End Set
  End Property

  Public Property Combo2BackColor() As System.Drawing.Color
    Get
      Return cmb_2.BackColor
    End Get
    Set(ByVal value As System.Drawing.Color)
      cmb_2.BackColor = value
      Me.BackColor = value
    End Set
  End Property


  Public Sub Combo1Focus()
    cmb_1.Focus()
  End Sub

  Public Sub Combo2Focus()
    cmb_2.Focus()
  End Sub

#End Region

#Region "Public"
  Public Sub Combo1Add(ByVal table_combo1 As DataTable, Optional ByVal IsReadOnly As Boolean = False, Optional ByVal Value As Integer = -1)
    cmb_1.Add(table_combo1)
    If (Value <> -1) Then
      cmb_1.Value = Value
    Else
      cmb_1.SelectedIndex = -1
    End If

    cmb_1.IsReadOnly = IsReadOnly

  End Sub

  Public Sub Combo2Add(ByVal table_combo1 As DataTable, Optional ByVal IsReadOnly As Boolean = False, Optional ByVal Value As Integer = -1)
    cmb_2.Add(table_combo1)
    If (Value <> -1) Then
      cmb_2.Value = Value
    Else
      cmb_2.SelectedIndex = -1
    End If

    cmb_2.IsReadOnly = IsReadOnly

  End Sub

  'Public Sub Add(Of T)(ByVal value As T)
  '  Dim _table As DataTable
  '  If TypeOf value Is DataTable Then
  '    _table = CType(value, Integer)
  '    cmb_1.Add(_table)

  '  End If
  '  'cmb_1.Add((value))
  'End Sub

  'Public Sub Add(Of T1, T2)(ByVal value_1 As T1, ByVal value_2 As T2)
  '  'cmb_1.Add(value_1), Val(value_2))
  'End Sub

#End Region

#End Region

#Region "checkbox"
  Public Property Checkbox1Checked() As Boolean
    Get
      Return chk_1.Checked
    End Get
    Set(ByVal value As Boolean)
      chk_1.Checked = value
    End Set
  End Property

#Region "Events"
  Private Sub chk_1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chk_1.CheckedChanged

    btn_1.Enabled = chk_1.Checked

    If Not cmb_1.IsReadOnly Then
      cmb_1.Enabled = chk_1.Checked
    End If

    If Not cmb_2.IsReadOnly Then
      cmb_2.Enabled = chk_1.Checked
    End If

    RaiseEvent ControlValueChangedEvent()
  End Sub

#End Region

#End Region

#Region "Button"
  Public Property Button1BackImage() As Drawing.Image
    Get
      Return btn_1.BackgroundImage
    End Get
    Set(ByVal value As Drawing.Image)
      btn_1.BackgroundImage = value
    End Set
  End Property

  Public Property Button1Tag() As Object
    Get
      Return btn_1.Tag
    End Get
    Set(ByVal value As Object)
      btn_1.Tag = value
    End Set
  End Property
#End Region

#Region "Functions"

#Region "Public"

  Public Sub SwitchMode()
    If m_control_mode = UC_MODE.MODE_ADVANCED Then
      m_control_mode = UC_MODE.MODE_BASIC
    Else
      m_control_mode = UC_MODE.MODE_ADVANCED
    End If
    RelocateComponents()
  End Sub


#End Region

#Region "Private"
  Private Sub RelocateComponents()

    Dim _pos_ini_x As Integer
    Dim _pos_acc_x As Integer

    _pos_ini_x = 4
    _pos_acc_x = 0

    Select Case m_control_mode
      Case UC_MODE.MODE_ADVANCED
        _pos_acc_x += _pos_ini_x
        cmb_2.SetBounds(_pos_acc_x, cmb_2.Location.Y, cmb_2.Width - 15, cmb_2.Height)
        _pos_acc_x += cmb_2.Width + 8
        chk_1.SetBounds(_pos_acc_x, chk_1.Location.Y, chk_1.Width, chk_1.Height)
        _pos_acc_x += chk_1.Width + 8
        btn_1.SetBounds(_pos_acc_x, btn_1.Location.Y, btn_1.Width, btn_1.Height)
        _pos_acc_x += btn_1.Width + 8
        cmb_1.SetBounds(_pos_acc_x, cmb_1.Location.Y, cmb_1.Width + 15, cmb_1.Height)
        _pos_acc_x += cmb_1.Width + 8

        cmb_1.Enabled = chk_1.Checked
        cmb_2.Enabled = True

      Case UC_MODE.MODE_BASIC
        _pos_acc_x += _pos_ini_x
        cmb_1.SetBounds(_pos_acc_x, cmb_1.Location.Y, cmb_1.Width - 15, cmb_1.Height)
        _pos_acc_x += cmb_1.Width + 8
        chk_1.SetBounds(_pos_acc_x, chk_1.Location.Y, chk_1.Width, chk_1.Height)
        _pos_acc_x += chk_1.Width + 8
        btn_1.SetBounds(_pos_acc_x, btn_1.Location.Y, btn_1.Width, btn_1.Height)
        _pos_acc_x += btn_1.Width + 8
        cmb_2.SetBounds(_pos_acc_x, cmb_2.Location.Y, cmb_2.Width + 15, cmb_2.Height)
        _pos_acc_x += cmb_2.Width + 8

        cmb_1.Enabled = True
        cmb_2.Enabled = chk_1.Checked
    End Select
  End Sub


#End Region

#End Region

#Region "Events"
  Public Sub ValueChangedEvent() Handles cmb_1.ValueChangedEvent, cmb_2.ValueChangedEvent
    RaiseEvent ControlValueChangedEvent()
  End Sub

  Private Sub btn_1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_1.Click
    RaiseEvent Button1ClickEvent(sender, e)
    RaiseEvent ControlValueChangedEvent()
  End Sub

#End Region

End Class
