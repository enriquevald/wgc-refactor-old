'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_terminal_type.vb  
'
' DESCRIPTION:   terminal type picker control
'
' AUTHOR:        Miquel Beltran
'
' CREATION DATE: 16-MAR-2011
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-MAR-2011  MBF    Initial version
' 06-SEP-2012  DDM    Added terminal type PromoBOX 
' 04-APR-2013  DMR    Fixed Bug #361: Don't show WCP version in 3GS terminals
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls

Public Class uc_terminal_type

#Region "Constants"

  Private Const GRID_COLUMN_TYPE_ID As Integer = 0
  Private Const GRID_COLUMN_CHECKED As Integer = 1
  Private Const GRID_COLUMN_TYPE_NAME As Integer = 2
  Private Const GRID_COLUMN_TYPE_VERSION As Integer = 3

  Private Const GRID_COLUMNS As Integer = 4
  Private Const GRID_HEADER_ROWS As Integer = 0

#End Region ' Constants

#Region " Members "

  Private m_terminal_types() As Integer = Nothing

#End Region ' Members

#Region " Public Functions "

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

  End Sub

  ' PURPOSE: Init the control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Public Sub Init(ByVal TerminalTypes() As Integer)

    Call InitControls(TerminalTypes)
  End Sub ' Init

  ' PURPOSE: Get selected terminal types list
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Public Function GetTerminalTypesSelected(Optional ByVal AddSQL As Boolean = True) As String
    Dim _sql_string As String
    Dim _idx_row As Integer
    Dim _list_sw_types As String

    _sql_string = ""
    _list_sw_types = ""

    For _idx_row = 0 To Me.dg_filter.NumRows - 1
      If dg_filter.Cell(_idx_row, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        If _list_sw_types.Length = 0 Then
          _list_sw_types = dg_filter.Cell(_idx_row, GRID_COLUMN_TYPE_ID).Value
        Else
          _list_sw_types = _list_sw_types & ", " & dg_filter.Cell(_idx_row, GRID_COLUMN_TYPE_ID).Value
        End If
      End If
    Next

    If _list_sw_types.Length = 0 Then
      Return ""
    End If

    If (AddSQL = True) Then
      _sql_string = _sql_string & " AND TE_TERMINAL_TYPE IN (" & _list_sw_types & ") "
    Else
      _sql_string = _sql_string & _list_sw_types
    End If

    Return _sql_string

  End Function ' GetTerminalTypesSelected


  Public Function GetTerminalTypesSelectedNames() As String

    Dim _sql_string As String
    Dim _idx_row As Integer
    Dim _list_sw_types As String

    _sql_string = ""
    _list_sw_types = ""

    If opt_all_types.Checked Then
      Return opt_all_types.Text
    End If

    For _idx_row = 0 To Me.dg_filter.NumRows - 1
      If dg_filter.Cell(_idx_row, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        If _list_sw_types.Length = 0 Then
          _list_sw_types = dg_filter.Cell(_idx_row, GRID_COLUMN_TYPE_NAME).Value
        Else
          _list_sw_types = _list_sw_types & ", " & dg_filter.Cell(_idx_row, GRID_COLUMN_TYPE_NAME).Value
        End If
      End If
    Next

    Return _list_sw_types

  End Function ' GetTerminalTypesSelectedNames

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub SetDefaultValues()
    Dim _idx As Integer

    Me.dg_filter.Redraw = False

    For _idx = 0 To dg_filter.NumRows - 1
      Me.dg_filter.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next

    Me.opt_all_types.Checked = True

    Me.dg_filter.Redraw = True

  End Sub 'SetDefaultValues

  ' PURPOSE: Set filter values as specified
  '
  '  PARAMS:
  '     - INPUT:
  '           - 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub SetFilterValues(ByVal SelParams As TYPE_TERMINAL_SEL_PARAMS)
    Dim row_index As Integer
    Dim terminal_type_id As Integer
    Dim any_checked As Boolean

    any_checked = False

    For row_index = 0 To Me.dg_filter.NumRows - 1
      terminal_type_id = Me.dg_filter.Cell(row_index, GRID_COLUMN_TYPE_ID).Value

      If SelParams.FilterTypeValue(terminal_type_id) Then
        Me.dg_filter.Cell(row_index, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
        any_checked = True
      Else
        Me.dg_filter.Cell(row_index, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      End If

    Next

    If any_checked Then
      Me.opt_several_types.Checked = True
    End If

    Me.dg_filter.Enabled = False
    Me.opt_all_types.AutoCheck = False
    Me.opt_several_types.AutoCheck = False
  End Sub ' SetFilterValues

  ' PURPOSE: Refresh DataGrid Type Version Column
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Public Sub RefreshVersions()

    Dim current_client As Integer
    Dim current_build As Integer
    Dim rc As Boolean
    Dim row_index As Integer

    Try
      For row_index = 0 To Me.dg_filter.NumRows - 1

        rc = GetCurrentSoftwareBuild(current_client, current_build, Me.dg_filter.Cell(row_index, GRID_COLUMN_TYPE_ID).Value)
        If rc Then
          Me.dg_filter.Cell(row_index, GRID_COLUMN_TYPE_VERSION).Value = FormatBuild(current_client, current_build)
        Else
          Me.dg_filter.Cell(row_index, GRID_COLUMN_TYPE_VERSION).Value = ""
        End If
      Next
    Catch ex As Exception

    End Try
  End Sub ' RefreshVersions

  Public Function GetCurrentBuild(ByVal Type As WSI.Common.TerminalTypes) As Integer
    Dim _idx As Integer
    Dim version As String
    Dim build As Integer

    build = -1

    For _idx = 0 To dg_filter.NumRows - 1

      If Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_ID).Value = Type Then
        version = Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_VERSION).Value
        If Not Integer.TryParse(version.Substring(version.IndexOf(".") + 1), build) Then
          build = -1
        End If
        Exit For
      End If

    Next

    Return build
  End Function ' GetCurrentBuild

  ' PURPOSE : Formats the version as CC.BBB (Client.Build)
  '
  '  PARAMS:
  '     - INPUT:
  '         - BuildNumber 
  '
  '     - OUTPUT:
  '
  ' RETURNS : Version 
  '
  Public Shared Function FormatBuild(ByVal ClientNumber As Integer, ByVal BuildNumber As Integer) As String

    Return IIf(ClientNumber = 0 And BuildNumber = 0, "", ClientNumber.ToString("00") + "." + BuildNumber.ToString("000"))

  End Function ' FormatBuild

#End Region ' Public Functions

#Region " Private Functions "

  Private Sub InitControls(ByVal TerminalTypes() As Integer)
    Dim _idx As Integer

    ReDim Preserve m_terminal_types(0 To TerminalTypes.Length - 1)

    For _idx = 0 To TerminalTypes.Length - 1
      m_terminal_types(_idx) = TerminalTypes(_idx)
    Next

    Me.gb_sw.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(322)
    Me.opt_several_types.Text = GLB_NLS_GUI_STATISTICS.GetString(211)
    Me.opt_all_types.Text = GLB_NLS_GUI_STATISTICS.GetString(206)

    GUI_StyleSheetListTerminalTypes()

    GetTerminalTypesList()
    SetDefaultValues()

  End Sub ' InitControls

  Private Sub GUI_StyleSheetListTerminalTypes()
    With Me.dg_filter
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden colums
      ' GRID_COLUMN_TYPE_ID
      .Column(GRID_COLUMN_TYPE_ID).Header.Text = ""
      .Column(GRID_COLUMN_TYPE_ID).WidthFixed = 0

      ' GRID_COLUMN_CHECKED
      .Column(GRID_COLUMN_CHECKED).Header.Text = ""
      .Column(GRID_COLUMN_CHECKED).WidthFixed = 300
      .Column(GRID_COLUMN_CHECKED).Fixed = True
      .Column(GRID_COLUMN_CHECKED).Editable = True
      .Column(GRID_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COLUMN_TYPE_NAME
      .Column(GRID_COLUMN_TYPE_NAME).Header.Text = ""
      .Column(GRID_COLUMN_TYPE_NAME).WidthFixed = 2200
      .Column(GRID_COLUMN_TYPE_NAME).Fixed = True
      .Column(GRID_COLUMN_TYPE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' GRID_COLUMN_TYPE_VERSION
      .Column(GRID_COLUMN_TYPE_VERSION).Header.Text = ""
      .Column(GRID_COLUMN_TYPE_VERSION).WidthFixed = 800
      .Column(GRID_COLUMN_TYPE_VERSION).Fixed = True
      .Column(GRID_COLUMN_TYPE_VERSION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With
  End Sub ' GUI_StyleSheetListProviders

  Private Sub GetTerminalTypesList()
    Dim _idx As Integer

    ' Machines
    'WIN = 1,                // LKT 
    'T3GS = 3,               // 3GS
    'SAS_HOST = 5,           // SasHost

    ' Site 
    'SITE = 100,             // Site 
    'SITE_JACKPOT = 101,     // Site Jackpot
    'MOBILE_BANK = 102,      // MobileBank-SasHost
    'MOBILE_BANK_IMB = 103,  // MobileBank-IPod
    'ISTATS = 104, // IStats

    _idx = 0

    If IsTypeInList(WSI.Common.TerminalTypes.WIN) Then
      ' Terminal Type: WIN Kiosk
      dg_filter.AddRow()
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_ID).Value = WSI.Common.TerminalTypes.WIN
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_AUDITOR.GetString(379)
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_VERSION).Value = ""
      _idx = _idx + 1
    End If

    If IsTypeInList(WSI.Common.TerminalTypes.T3GS) Then
      ' Terminal Type: 3GS
      dg_filter.AddRow()
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_ID).Value = WSI.Common.TerminalTypes.T3GS
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_AUDITOR.GetString(381)
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_VERSION).Value = ""
      _idx = _idx + 1
    End If

    If IsTypeInList(WSI.Common.TerminalTypes.SAS_HOST) Then
      '' Terminal Type: SAS Host
      dg_filter.AddRow()
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_ID).Value = WSI.Common.TerminalTypes.SAS_HOST
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_AUDITOR.GetString(380)
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_VERSION).Value = ""
      _idx = _idx + 1
    End If

    If IsTypeInList(WSI.Common.TerminalTypes.SITE) Then
      '' Terminal Type: Site
      dg_filter.AddRow()
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_ID).Value = WSI.Common.TerminalTypes.SITE
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_AUDITOR.GetString(384)
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_VERSION).Value = ""
      _idx = _idx + 1
    End If

    If IsTypeInList(WSI.Common.TerminalTypes.SITE_JACKPOT) Then
      '' Terminal Type: Site Jackpot
      dg_filter.AddRow()
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_ID).Value = WSI.Common.TerminalTypes.SITE_JACKPOT
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_AUDITOR.GetString(382)
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_VERSION).Value = ""
      _idx = _idx + 1
    End If

    If IsTypeInList(WSI.Common.TerminalTypes.MOBILE_BANK) Then
      ' Terminal Type: Mobile Bank
      dg_filter.AddRow()
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_ID).Value = WSI.Common.TerminalTypes.MOBILE_BANK
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_AUDITOR.GetString(383)
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_VERSION).Value = ""
      _idx = _idx + 1
    End If

    If IsTypeInList(WSI.Common.TerminalTypes.MOBILE_BANK_IMB) Then
      ' Terminal Type: iMB
      dg_filter.AddRow()
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_ID).Value = WSI.Common.TerminalTypes.MOBILE_BANK_IMB
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_AUDITOR.GetString(385)
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_VERSION).Value = ""
      _idx = _idx + 1
    End If

    If IsTypeInList(WSI.Common.TerminalTypes.ISTATS) Then
      ' Terminal Type: iStats
      dg_filter.AddRow()
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_ID).Value = WSI.Common.TerminalTypes.ISTATS
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_AUDITOR.GetString(386)
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_VERSION).Value = ""
      _idx = _idx + 1
    End If

    If IsTypeInList(WSI.Common.TerminalTypes.PROMOBOX) Then
      ' Terminal Type: PromoBOX
      dg_filter.AddRow()
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_ID).Value = WSI.Common.TerminalTypes.PROMOBOX
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(722)
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_VERSION).Value = ""
      _idx = _idx + 1
    End If

    If IsTypeInList(WSI.Common.TerminalTypes.OFFLINE) Then
      ' Terminal Type: Offline
      dg_filter.AddRow()
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_ID).Value = WSI.Common.TerminalTypes.OFFLINE
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_NAME).Value = "OFFLINE"
      Me.dg_filter.Cell(_idx, GRID_COLUMN_TYPE_VERSION).Value = ""
      _idx = _idx + 1
    End If

    Call RefreshVersions()
  End Sub ' GetTerminalTypesList

  ' PURPOSE: Build the SQL query to obtain the relevant data from database
  '
  '  PARAMS:
  '     - INPUT:
  '           - NONE
  '     - OUTPUT:
  '           - NONE
  '
  ' RETURNS:
  '     
  Private Function GetCurrentSoftwareBuild(ByRef CurrentClient As Integer, ByRef CurrentBuild As Integer, ByVal TerminalType As WSI.Common.TerminalTypes) As Boolean

    Dim sql_str As String
    Dim data_table As DataTable
    Dim terminal_type As WSI.Common.TerminalTypes

    CurrentClient = 0
    CurrentBuild = 0

    ' DMR & RCI 04-APR-2013: There is no sense for Software Build for 3GS terminals.
    If TerminalType = WSI.Common.TerminalTypes.T3GS Then
      Return False
    End If

    terminal_type = TerminalType
    If TerminalType = WSI.Common.TerminalTypes.MOBILE_BANK Then
      terminal_type = WSI.Common.TerminalTypes.SAS_HOST
    End If

    sql_str = "   SELECT TOP 1 tsv_client_id, tsv_build_id " & _
              "     FROM terminal_software_versions " & _
              "    WHERE tsv_terminal_type = " & terminal_type & " " & _
              " ORDER BY tsv_insertion_date DESC"

    data_table = GUI_GetTableUsingSQL(sql_str, 1)

    If IsNothing(data_table) Then
      Return False
    End If

    If data_table.Rows.Count = 1 Then
      CurrentClient = data_table.Rows(0).Item(0)
      CurrentBuild = data_table.Rows(0).Item(1)
    End If

    Return True
  End Function ' GetCurrentSoftwareBuild

  Private Function IsTypeInList(ByVal Type As WSI.Common.TerminalTypes)
    Dim _idx As Integer

    For _idx = 0 To m_terminal_types.Length - 1
      If m_terminal_types(_idx) = Type Then
        Return True
      End If
    Next

    Return False
  End Function ' IsTypeInList

#End Region ' Private Functions

#Region "Events"

  Private Sub opt_several_types_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_several_types.CheckedChanged

    If opt_several_types.Checked Then
      Me.opt_all_types.Checked = False
    End If

  End Sub

  Private Sub opt_all_types_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_all_types.CheckedChanged
    Dim idx As Integer

    If opt_all_types.Checked Then
      Me.dg_filter.Redraw = False

      For idx = 0 To dg_filter.NumRows - 1
        Me.dg_filter.Cell(idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      Next

      Me.dg_filter.Redraw = True

      opt_several_types.Checked = False
    End If

  End Sub

  Private Sub dg_filter_DataSelectedEvent() Handles dg_filter.DataSelectedEvent

    If Me.opt_several_types.Checked = False Then
      Me.opt_several_types.Checked = True
    End If

  End Sub ' dg_filter_DataSelectedEvent

  Private Sub dg_filter_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_filter.CellDataChangedEvent
    ' Update radio buttons accordingly
    If Me.opt_several_types.Checked = False Then
      Me.opt_several_types.Checked = True
    End If

  End Sub ' dg_types_CellDataChangedEvent

#End Region ' Events

End Class ' uc_terminal_type
