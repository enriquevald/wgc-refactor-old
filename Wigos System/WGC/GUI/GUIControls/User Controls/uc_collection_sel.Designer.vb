<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_collection_sel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_grid_collections = New GUI_Controls.uc_grid
    Me.SuspendLayout()
    '
    'uc_grid_collections
    '
    Me.uc_grid_collections.CurrentCol = -1
    Me.uc_grid_collections.CurrentRow = -1
    Me.uc_grid_collections.EditableCellBackColor = System.Drawing.Color.Empty
    Me.uc_grid_collections.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.uc_grid_collections.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.uc_grid_collections.Location = New System.Drawing.Point(3, 3)
    Me.uc_grid_collections.Name = "uc_grid_collections"
    Me.uc_grid_collections.PanelRightVisible = True
    Me.uc_grid_collections.Redraw = True
    Me.uc_grid_collections.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.uc_grid_collections.Size = New System.Drawing.Size(660, 243)
    Me.uc_grid_collections.Sortable = False
    Me.uc_grid_collections.SortAscending = True
    Me.uc_grid_collections.SortByCol = 0
    Me.uc_grid_collections.TabIndex = 3
    Me.uc_grid_collections.ToolTipped = True
    Me.uc_grid_collections.TopRow = -2
    '
    'uc_collection_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.uc_grid_collections)
    Me.Name = "uc_collection_sel"
    Me.Size = New System.Drawing.Size(666, 249)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_grid_collections As GUI_Controls.uc_grid

End Class
