'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_collection_detail
'
' DESCRIPTION:   User control for Import collection detail
'
' AUTHOR:        Alberto Marcos
'
' CREATION DATE: 07-MAY-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-MAY-2015  AMF    Initial version
'-------------------------------------------------------------------

#Region " Imports "

Imports WSI.Common.CollectionImport
Imports WSI.Common.TITO
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT

#End Region ' Imports

Public Class uc_collection_detail

#Region " Constants "

  ' Grid Total Columns
  Private Const GRID_TOTAL_COLUMNS As Integer = 9
  Private Const GRID_TOTAL_HEADER_ROWS As Integer = 2

  Private Const GRID_TOTAL_COLUMN_INDEX As Integer = 0
  Private Const GRID_TOTAL_COLUMN_DENOMINATION_QUANTITY As Integer = 1
  Private Const GRID_TOTAL_COLUMN_DENOMINATION_TYPE As Integer = 2
  Private Const GRID_TOTAL_COLUMN_COLLECTED_QUANTITY As Integer = 3
  Private Const GRID_TOTAL_COLUMN_COLLECTED_TOTAL As Integer = 4
  Private Const GRID_TOTAL_COLUMN_EXPECTED_QUANTITY As Integer = 5
  Private Const GRID_TOTAL_COLUMN_EXPECTED_TOTAL As Integer = 6
  Private Const GRID_TOTAL_COLUMN_DIFF_QUANTITY As Integer = 7
  Private Const GRID_TOTAL_COLUMN_DIFF_TOTAL As Integer = 8

  ' SQL Total Columns
  Private Const SQL_TOTAL_COLUMN_DENOMINATION_QUANTITY As Integer = 5
  Private Const SQL_TOTAL_COLUMN_DENOMINATION_TYPE As Integer = 0
  Private Const SQL_TOTAL_COLUMN_COLLECTED_QUANTITY As Integer = 6
  Private Const SQL_TOTAL_COLUMN_COLLECTED_TOTAL As Integer = 7
  Private Const SQL_TOTAL_COLUMN_EXPECTED_QUANTITY As Integer = 8
  Private Const SQL_TOTAL_COLUMN_EXPECTED_TOTAL As Integer = 9



  ' Grid Tickets Columns
  Private Const GRID_TICKETS_COLUMNS As Integer = 3
  Private Const GRID_TICKETS_HEADER_ROWS As Integer = 2

  Private Const GRID_TICKETS_COLUMN_INDEX As Integer = 0
  Private Const GRID_TICKETS_COLUMN_TICKET_NUM As Integer = 1
  Private Const GRID_TICKETS_COLUMN_TICKET_QUANTITY As Integer = 2

  ' SQL Tickets Columns
  Private Const SQL_TICKETS_COLUMN_TICKET_NUM As Integer = 1
  Private Const SQL_TICKETS_COLUMN_TICKET_TYPE As Integer = 3
  Private Const SQL_TICKETS_COLUMN_TICKET_QUANTITY As Integer = 2
  Private Const SQL_TICKETS_COLUMN_TICKET_STATUS As Integer = 4

  ' Colors
  Private Const COLOR_NO_DIFFERENCE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_POSITIVE_DIFFERENCE = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
  Private Const COLOR_ERROR = ENUM_GUI_COLOR.GUI_COLOR_RED_03
  Private Const COLOR_PENDING = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00

#End Region ' Constants

#Region " Members "

  Private m_data_collection_detail As DataTable

#End Region ' Members

#Region " Properties "

  Public Property DataCollectionDetail() As DataTable
    Get
      Return m_data_collection_detail
    End Get

    Set(ByVal Value As DataTable)
      m_data_collection_detail = Value
    End Set
  End Property ' DataCollectionDetail

#End Region ' Properties

#Region " Public Functions "

  ' PURPOSE : Create new instance of user control
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS :
  '
  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControls()

  End Sub ' New

  ' PURPOSE : Set data grid collection total info
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS :
  '     - True or False
  '
  Public Function SetDataGridCollectionTotal() As Boolean

    Dim _row_index As Integer
    Dim _diff_quantity As Integer
    Dim _diff_total As Double

    Try

      _row_index = 0
      Me.uc_grid_collection_total.Clear()

      For Each _collection As DataRow In DataCollectionDetail.Rows

        _diff_quantity = 0
        _diff_total = 0

        If _collection(SQL_TOTAL_COLUMN_DENOMINATION_TYPE) = TYPE_ROW_TICKET_BILL.TICKET Then

          Continue For
        End If

        ' Calculate diff
        If _collection(SQL_TOTAL_COLUMN_COLLECTED_QUANTITY) Is DBNull.Value Then
          _diff_quantity = -_collection(SQL_TOTAL_COLUMN_EXPECTED_QUANTITY)
        ElseIf _collection(SQL_TOTAL_COLUMN_EXPECTED_QUANTITY) Is DBNull.Value Then
          _diff_quantity = _collection(SQL_TOTAL_COLUMN_COLLECTED_QUANTITY)
        Else
          _diff_quantity = _collection(SQL_TOTAL_COLUMN_COLLECTED_QUANTITY) - _collection(SQL_TOTAL_COLUMN_EXPECTED_QUANTITY)
        End If

        If _collection(SQL_TOTAL_COLUMN_COLLECTED_TOTAL) Is DBNull.Value Then
          If Not _collection(SQL_TOTAL_COLUMN_EXPECTED_TOTAL) Is DBNull.Value Then
            _diff_total = -_collection(SQL_TOTAL_COLUMN_EXPECTED_TOTAL)
          End If
        ElseIf _collection(SQL_TOTAL_COLUMN_EXPECTED_TOTAL) Is DBNull.Value Then
          If Not _collection(SQL_TOTAL_COLUMN_COLLECTED_TOTAL) Is DBNull.Value Then
            _diff_total = _collection(SQL_TOTAL_COLUMN_COLLECTED_TOTAL)
          End If
        Else
          _diff_total = _collection(SQL_TOTAL_COLUMN_COLLECTED_TOTAL) - _collection(SQL_TOTAL_COLUMN_EXPECTED_TOTAL)
        End If

        uc_grid_collection_total.AddRow()

        ' Denomination quantity
        If _collection(SQL_TOTAL_COLUMN_DENOMINATION_TYPE) = TYPE_ROW_TICKET_BILL.TOTAL_TICKET Then
          uc_grid_collection_total.Cell(_row_index, GRID_TOTAL_COLUMN_DENOMINATION_QUANTITY).Value = _collection(SQL_TOTAL_COLUMN_DENOMINATION_QUANTITY)
        Else
          uc_grid_collection_total.Cell(_row_index, GRID_TOTAL_COLUMN_DENOMINATION_QUANTITY).Value = GUI_FormatCurrency(_collection(SQL_TOTAL_COLUMN_DENOMINATION_QUANTITY))
        End If

        ' Denomination type
        Select Case _collection(SQL_TOTAL_COLUMN_DENOMINATION_TYPE)

          Case TYPE_ROW_TICKET_BILL.BILL
            If _collection(SQL_TOTAL_COLUMN_DENOMINATION_QUANTITY) = WSI.Common.Resource.String("STR_TITO_FILE_COLLECTION_TOTAL_TICKETS") Then
              uc_grid_collection_total.Cell(_row_index, GRID_TOTAL_COLUMN_DENOMINATION_TYPE).Value = ""
            Else
              uc_grid_collection_total.Cell(_row_index, GRID_TOTAL_COLUMN_DENOMINATION_TYPE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5675)
            End If

          Case Else
            uc_grid_collection_total.Cell(_row_index, GRID_TOTAL_COLUMN_DENOMINATION_TYPE).Value = ""

        End Select

        ' Collected quantity
        If Not _collection(SQL_TOTAL_COLUMN_COLLECTED_QUANTITY) Is DBNull.Value Then
          uc_grid_collection_total.Cell(_row_index, GRID_TOTAL_COLUMN_COLLECTED_QUANTITY).Value = GUI_FormatNumber(_collection(SQL_TOTAL_COLUMN_COLLECTED_QUANTITY), 0)
        End If

        ' Collected total
        If Not _collection(SQL_TOTAL_COLUMN_COLLECTED_TOTAL) Is DBNull.Value Then
          uc_grid_collection_total.Cell(_row_index, GRID_TOTAL_COLUMN_COLLECTED_TOTAL).Value = GUI_FormatCurrency(_collection(SQL_TOTAL_COLUMN_COLLECTED_TOTAL))
        End If

        ' Expected quantity
        If Not _collection(SQL_TOTAL_COLUMN_EXPECTED_QUANTITY) Is DBNull.Value Then
          uc_grid_collection_total.Cell(_row_index, GRID_TOTAL_COLUMN_EXPECTED_QUANTITY).Value = GUI_FormatNumber(_collection(SQL_TOTAL_COLUMN_EXPECTED_QUANTITY), 0)
        End If

        ' Expected total
        If Not _collection(SQL_TOTAL_COLUMN_EXPECTED_TOTAL) Is DBNull.Value Then
          uc_grid_collection_total.Cell(_row_index, GRID_TOTAL_COLUMN_EXPECTED_TOTAL).Value = GUI_FormatCurrency(_collection(SQL_TOTAL_COLUMN_EXPECTED_TOTAL))
        End If

        ' Diff quantity
        uc_grid_collection_total.Cell(_row_index, GRID_TOTAL_COLUMN_DIFF_QUANTITY).Value = GUI_FormatNumber(_diff_quantity, 0)
        Select Case _diff_quantity

          Case Is > 0
            uc_grid_collection_total.Cell(_row_index, GRID_TOTAL_COLUMN_DIFF_QUANTITY).BackColor = GetColor(COLOR_POSITIVE_DIFFERENCE)

          Case Is < 0
            uc_grid_collection_total.Cell(_row_index, GRID_TOTAL_COLUMN_DIFF_QUANTITY).BackColor = GetColor(COLOR_ERROR)

          Case Else
            uc_grid_collection_total.Cell(_row_index, GRID_TOTAL_COLUMN_DIFF_QUANTITY).BackColor = GetColor(COLOR_NO_DIFFERENCE)

        End Select

        ' Diff total
        If _collection(SQL_TOTAL_COLUMN_DENOMINATION_TYPE) = TYPE_ROW_TICKET_BILL.BILL Then
          uc_grid_collection_total.Cell(_row_index, GRID_TOTAL_COLUMN_DIFF_TOTAL).Value = GUI_FormatCurrency(_diff_total)
          Select Case _diff_total

            Case Is > 0
              uc_grid_collection_total.Cell(_row_index, GRID_TOTAL_COLUMN_DIFF_TOTAL).BackColor = GetColor(COLOR_POSITIVE_DIFFERENCE)

            Case Is < 0
              uc_grid_collection_total.Cell(_row_index, GRID_TOTAL_COLUMN_DIFF_TOTAL).BackColor = GetColor(COLOR_ERROR)

            Case Else
              uc_grid_collection_total.Cell(_row_index, GRID_TOTAL_COLUMN_DIFF_TOTAL).BackColor = GetColor(COLOR_NO_DIFFERENCE)

          End Select
        End If

        _row_index += 1

      Next

      Return True

    Catch _ex As Exception

      WSI.Common.Log.Exception(_ex)
    End Try

    Return False

  End Function ' SetDataGridCollectionTotal

  ' PURPOSE : Set data grid collection tickets info
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS :
  '     - True or False
  '
  Public Function SetDataGridCollectionTickets() As Boolean

    Dim _row_index As Integer

    Try

      _row_index = 0
      Me.uc_grid_collection_tickets.Clear()

      For Each _collection As DataRow In DataCollectionDetail.Rows

        If _collection(SQL_TOTAL_COLUMN_DENOMINATION_TYPE) = TYPE_ROW_TICKET_BILL.BILL OrElse _
           _collection(SQL_TOTAL_COLUMN_DENOMINATION_TYPE) = TYPE_ROW_TICKET_BILL.TOTAL_TICKET Then

          Continue For
        End If

        uc_grid_collection_tickets.AddRow()

        ' Ticket num
        uc_grid_collection_tickets.Cell(_row_index, GRID_TICKETS_COLUMN_TICKET_NUM).Value = ValidationNumberManager.FormatValidationNumber(_collection(SQL_TICKETS_COLUMN_TICKET_NUM), _collection(SQL_TICKETS_COLUMN_TICKET_TYPE), False)

        ' Ticket quantity
        uc_grid_collection_tickets.Cell(_row_index, GRID_TICKETS_COLUMN_TICKET_QUANTITY).Value = GUI_FormatCurrency(_collection(SQL_TICKETS_COLUMN_TICKET_QUANTITY))

        ' Status
        Select Case _collection(SQL_TICKETS_COLUMN_TICKET_STATUS)

          Case STATUS_TICKET.TICKET_STATUS_OK
            uc_grid_collection_tickets.Row(_row_index).BackColor = GetColor(COLOR_NO_DIFFERENCE)

          Case STATUS_TICKET.TICKET_STATUS_ERROR
            uc_grid_collection_tickets.Row(_row_index).BackColor = GetColor(COLOR_ERROR)

          Case STATUS_TICKET.TICKET_STATUS_PENDING
            uc_grid_collection_tickets.Row(_row_index).BackColor = GetColor(COLOR_PENDING)

        End Select

        _row_index += 1

      Next

      Return True

    Catch _ex As Exception

      WSI.Common.Log.Exception(_ex)
    End Try

    Return False

  End Function ' SetDataGridCollectionTickets

  ' PURPOSE : Grid key pressed
  '
  '  PARAMS:
  '     - INPUT:
  '         - sender
  '         - e
  '     - OUTPUT:
  '
  ' RETURNS :
  '
  Public Sub CollectionTotalKeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

    Me.uc_grid_collection_total.KeyPressed(sender, e)

  End Sub ' CollectionTotalKeyPress

#End Region ' Public Functions

#Region " Public Events "

  Public Event RowSelectedEvent(ByVal SelectedRow As Integer)
  Public Event TotalCellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer)

#End Region ' Public Events

#Region " Private Functions "

  ' PURPOSE : Initialize controls
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS :
  '
  Private Sub InitControls()

    ' Data Info
    m_data_collection_detail = New DataTable()

    ' Tabs
    Me.tp_total.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2271)
    Me.tp_tickets.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2269)

    ' Status
    Me.gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1971)
    Me.lbl_ok.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1972)
    Me.lbl_error.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1974)
    Me.lbl_pending.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5238)

    ' Format entry fields
    GUI_StyleSheetTotal()
    GUI_StyleSheetTickets()

  End Sub ' InitControls

  ' PURPOSE : Define Grid Total Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS :
  '
  Private Sub GUI_StyleSheetTotal()

    With Me.uc_grid_collection_total

      Call .Init(GRID_TOTAL_COLUMNS, GRID_TOTAL_HEADER_ROWS, True)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Index
      .Column(GRID_TOTAL_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_TOTAL_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_TOTAL_COLUMN_INDEX).Width = 150
      .Column(GRID_TOTAL_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_TOTAL_COLUMN_INDEX).IsColumnPrintable = False
      .Column(GRID_TOTAL_COLUMN_INDEX).Editable = False

      ' Denomination Quantity
      .Column(GRID_TOTAL_COLUMN_DENOMINATION_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2270)
      .Column(GRID_TOTAL_COLUMN_DENOMINATION_QUANTITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1060)
      .Column(GRID_TOTAL_COLUMN_DENOMINATION_QUANTITY).Width = 1580
      .Column(GRID_TOTAL_COLUMN_DENOMINATION_QUANTITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Denomination Type
      .Column(GRID_TOTAL_COLUMN_DENOMINATION_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2270)
      .Column(GRID_TOTAL_COLUMN_DENOMINATION_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5678)
      .Column(GRID_TOTAL_COLUMN_DENOMINATION_TYPE).Width = 900
      .Column(GRID_TOTAL_COLUMN_DENOMINATION_TYPE).HighLightWhenSelected = False
      .Column(GRID_TOTAL_COLUMN_DENOMINATION_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Collected Quantity
      .Column(GRID_TOTAL_COLUMN_COLLECTED_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5011)
      .Column(GRID_TOTAL_COLUMN_COLLECTED_QUANTITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5677)
      .Column(GRID_TOTAL_COLUMN_COLLECTED_QUANTITY).Width = 1005
      .Column(GRID_TOTAL_COLUMN_COLLECTED_QUANTITY).HighLightWhenSelected = False
      .Column(GRID_TOTAL_COLUMN_COLLECTED_QUANTITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_TOTAL_COLUMN_COLLECTED_QUANTITY).Editable = False
      '.Column(GRID_TOTAL_COLUMN_COLLECTED_QUANTITY).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      '.Column(GRID_TOTAL_COLUMN_COLLECTED_QUANTITY).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 7)
      '.Column(GRID_TOTAL_COLUMN_COLLECTED_QUANTITY).SystemType = Type.GetType("System.Int32")

      ' Collected Total
      .Column(GRID_TOTAL_COLUMN_COLLECTED_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5011)
      .Column(GRID_TOTAL_COLUMN_COLLECTED_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2936)
      .Column(GRID_TOTAL_COLUMN_COLLECTED_TOTAL).Width = 1630
      .Column(GRID_TOTAL_COLUMN_COLLECTED_TOTAL).HighLightWhenSelected = False
      .Column(GRID_TOTAL_COLUMN_COLLECTED_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Expected Quantity
      .Column(GRID_TOTAL_COLUMN_EXPECTED_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5010)
      .Column(GRID_TOTAL_COLUMN_EXPECTED_QUANTITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5677)
      .Column(GRID_TOTAL_COLUMN_EXPECTED_QUANTITY).Width = 1005
      .Column(GRID_TOTAL_COLUMN_EXPECTED_QUANTITY).HighLightWhenSelected = False
      .Column(GRID_TOTAL_COLUMN_EXPECTED_QUANTITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Expected Total
      .Column(GRID_TOTAL_COLUMN_EXPECTED_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5010)
      .Column(GRID_TOTAL_COLUMN_EXPECTED_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2936)
      .Column(GRID_TOTAL_COLUMN_EXPECTED_TOTAL).Width = 1630
      .Column(GRID_TOTAL_COLUMN_EXPECTED_TOTAL).HighLightWhenSelected = False
      .Column(GRID_TOTAL_COLUMN_EXPECTED_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Diff Quantity
      .Column(GRID_TOTAL_COLUMN_DIFF_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2610)
      .Column(GRID_TOTAL_COLUMN_DIFF_QUANTITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5677)
      .Column(GRID_TOTAL_COLUMN_DIFF_QUANTITY).Width = 1005
      .Column(GRID_TOTAL_COLUMN_DIFF_QUANTITY).HighLightWhenSelected = False
      .Column(GRID_TOTAL_COLUMN_DIFF_QUANTITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Diff Total
      .Column(GRID_TOTAL_COLUMN_DIFF_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2610)
      .Column(GRID_TOTAL_COLUMN_DIFF_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2936)
      .Column(GRID_TOTAL_COLUMN_DIFF_TOTAL).Width = 1630
      .Column(GRID_TOTAL_COLUMN_DIFF_TOTAL).HighLightWhenSelected = False
      .Column(GRID_TOTAL_COLUMN_DIFF_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub ' GUI_StyleSheetTotal

  ' PURPOSE : Define Grid Tickets Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS :
  '
  Private Sub GUI_StyleSheetTickets()

    With Me.uc_grid_collection_tickets

      Call .Init(GRID_TICKETS_COLUMNS, GRID_TICKETS_HEADER_ROWS, True)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Index
      .Column(GRID_TICKETS_COLUMN_INDEX).Header(0).Text = " "
      .Column(GRID_TICKETS_COLUMN_INDEX).Header(1).Text = " "
      .Column(GRID_TICKETS_COLUMN_INDEX).Width = 150
      .Column(GRID_TICKETS_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_TICKETS_COLUMN_INDEX).IsColumnPrintable = False
      .Column(GRID_TICKETS_COLUMN_INDEX).Editable = False

      ' Ticket num
      .Column(GRID_TICKETS_COLUMN_TICKET_NUM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2269)
      .Column(GRID_TICKETS_COLUMN_TICKET_NUM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2259)
      .Column(GRID_TICKETS_COLUMN_TICKET_NUM).Width = 3510
      .Column(GRID_TICKETS_COLUMN_TICKET_NUM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Ticket quantity
      .Column(GRID_TICKETS_COLUMN_TICKET_QUANTITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2269)
      .Column(GRID_TICKETS_COLUMN_TICKET_QUANTITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2260)
      .Column(GRID_TICKETS_COLUMN_TICKET_QUANTITY).Width = 2535
      .Column(GRID_TICKETS_COLUMN_TICKET_QUANTITY).HighLightWhenSelected = False
      .Column(GRID_TICKETS_COLUMN_TICKET_QUANTITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub ' GUI_StyleSheetTickets

  ' PURPOSE : row total selected event 
  '
  '  PARAMS:
  '     - INPUT:
  '         - SelectedRow
  '     - OUTPUT:
  '
  ' RETURNS :
  '
  Private Sub uc_grid_collection_total_RowSelectedEvent(ByVal SelectedRow As Integer) Handles uc_grid_collection_total.RowSelectedEvent

    RaiseEvent RowSelectedEvent(SelectedRow)

  End Sub ' uc_grid_collection_total_RowSelectedEvent

  ' PURPOSE : cell data changed event 
  '
  '  PARAMS:
  '     - INPUT:
  '         - SelectedRow
  '     - OUTPUT:
  '
  ' RETURNS :
  '
  Private Sub uc_grid_collection_total_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles uc_grid_collection_total.CellDataChangedEvent

    RaiseEvent TotalCellDataChangedEvent(Row, Column)

  End Sub ' uc_grid_collection_total_CellDataChangedEvent

#End Region ' Private Functions

End Class
