'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd. 
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_checked_list.vb
'
' DESCRIPTION:   Check list control
'
' AUTHOR:        Jos� Mart�nez L�pez
'
' CREATION DATE: 19-NOV-2013
'
' REVISION HISTORY:
'
' Date         Author Description 
' -----------  ------ -----------------------------------------------
' 19-NOV-2013  JML    Initial version (copied from PSA VS-2010 )
' 19-DEC-2013  JML    Add level 2 
' 22-JUL-2013  JBC    Now the tooltip shows the alarm description
' 04-MAR-2015  YNM    Fixed Bug WIG-2077: Fixed error with Alarm_Code filter in Alarms Form
' 17-NOV-2015  FAV    It has added the "IsAllOrNoneSelected" property to chekc if all is selected or nothing is selected
' 14-DEC-2015  ETP    Fixed Bug-7649 Modified Min width for meter columns and default values for meters filter.
' 14-DEC-2015  ETP    PBI 26184: Add Dictionary by level && SelectedValuesListLevel2
' 28-SEP-2017  ETP    Fixed Bug 29953:[WIGOS-4910] Denominations filter values do not match with terminal denomination values 
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc

Public Class uc_checked_list

#Region " Members "

  Dim m_raise_events As Boolean = True
  Dim m_levels As Int32 = 1
  Dim m_height As Int32 = 0
  Dim m_prefix As String = ""
  Dim m_semaphore As Boolean = False

  Dim m_define_levels As Integer

  Dim m_position_scroll As System.Drawing.Point

  Dim m_grid_size As System.Drawing.Size

  Private m_descriptions As New Dictionary(Of Integer, String)

  Public Property m_resize_width() As Integer
    Get
      Return Me.Width
    End Get
    Set(ByVal value As Integer)
      Me.Width = value
    End Set
  End Property

#End Region

#Region " Constants "

  Public Const GRID_COLUMN_CODE As Integer = 0
  Public Const GRID_COLUMN_CHECKED As Integer = 1
  Public Const GRID_COLUMN_COLOR As Integer = 2
  Public Const GRID_COLUMN_DESC As Integer = 3
  Public Const GRID_COLUMN_LEVEL As Integer = 4


  Private Const GRID_COLUMNS As Integer = 5
  Private Const GRID_HEADER_ROWS As Integer = 0
  Private Const GRID_TAB As String = "    "

  'Private Const CONTROL_WIDTH As Integer = 310
  'Private Const CONTROL_WIDTH_2 As Integer = 270
  'Private Const CONTROL_HEIGHT As Integer = 173
  'Private Const CONTROL_HEIGHT_2 As Integer = 190

  Private Const LEVEL_1_TEXT_PLUS As String = "[+] "
  Private Const LEVEL_1_TEXT_MINOR As String = "[-] "
  Private Const LEVEL_2_TEXT As String = "    "

  Private Const DEFAULT_LEVELS As Integer = 2

#End Region

  Public Sub New()

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControl()

  End Sub

#Region " Check list Items "

  Public Sub Init()
    Me.btn_check_all.PerformClick()
    If Not multiChoice Then
      Me.btn_check_all.Enabled = False
      Me.btn_uncheck_all.Enabled = False
    End If
  End Sub

  Private Sub InitControl()

    With Me.dg_check_list
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden colums
      .Column(GRID_COLUMN_CODE).Header.Text = ""
      .Column(GRID_COLUMN_CODE).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_COLUMN_CHECKED).Header.Text = ""
      .Column(GRID_COLUMN_CHECKED).WidthFixed = 400
      .Column(GRID_COLUMN_CHECKED).Fixed = True
      .Column(GRID_COLUMN_CHECKED).Editable = True
      .Column(GRID_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_COLOR
      .Column(GRID_COLUMN_COLOR).Header.Text = ""
      .Column(GRID_COLUMN_COLOR).WidthFixed = 0
      .Column(GRID_COLUMN_COLOR).Fixed = False
      .Column(GRID_COLUMN_COLOR).Editable = True

      ' GRID_COL_DESCRIPTION
      .Column(GRID_COLUMN_DESC).Header.Text = ""
      'If Me.Width = CONTROL_WIDTH_2 Then
      '  .Column(GRID_COLUMN_DESC).Width = 3050
      'ElseIf Me.Width <> CONTROL_WIDTH Then
      '  .Column(GRID_COLUMN_DESC).Width = 2200
      'Else
      .Column(GRID_COLUMN_DESC).Width = 3700
      '    End If
      .Column(GRID_COLUMN_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'GRID_COLUMN_LEVEL
      .Column(GRID_COLUMN_LEVEL).Header.Text = ""
      .Column(GRID_COLUMN_LEVEL).WidthFixed = 0

    End With

    Me.btn_check_all.Text = GLB_NLS_GUI_ALARMS.GetString(452)
    Me.btn_uncheck_all.Text = GLB_NLS_GUI_ALARMS.GetString(453)

    m_define_levels = DEFAULT_LEVELS

    Me.dg_check_list.flx_grid.BackColorSel = Drawing.Color.Transparent
    Me.dg_check_list.flx_grid.ForeColorSel = Drawing.Color.Black

  End Sub

  Public Sub Clear()
    Call Me.dg_check_list.Clear()
  End Sub

  Public Sub SetDefaultValue(ByVal Checked As Boolean)

    For _idx As Integer = 0 To Me.dg_check_list.NumRows - 1
      If Checked Then
        Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
      Else
        Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      End If
    Next

  End Sub

  Public Sub SetValue(ByVal Index As Integer)
    Me.dg_check_list.Cell(Index, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
  End Sub


  ' PURPOSE: Set Values by array and raises event for check childs.
  '
  '  PARAMS:
  '     - INPUT:
  '           - Values: array of values that has to be checked or uncheck.
  '           - Checked: defines if function checks or unchecks.
  '
  '     - OUTPUT: 
  '
  ' RETURNS: 
  Public Sub SetValuesByArray(ByVal Values As Integer(), Optional ByVal Checked As Boolean = True)

    For _idx As Integer = 0 To Values.Length - 1
      For _idx2 As Integer = 0 To Me.dg_check_list.NumRows - 1

        If Values(_idx) = Me.dg_check_list.Cell(_idx2, GRID_COLUMN_CODE).Value Then

          If Checked Then
            Me.dg_check_list.Cell(_idx2, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED

          Else
            Me.dg_check_list.Cell(_idx2, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED

          End If

          dg_check_list_CellDataChangedEvent(_idx2, GRID_COLUMN_CHECKED)

        End If

      Next
    Next

  End Sub

  ' PURPOSE: Set ColumnWidth
  '
  '  PARAMS:
  '     - INPUT:
  '           - Index: the index of column to resize
  '           - Width: the Size
  '
  '     - OUTPUT: 
  '
  ' RETURNS: 
  Public Sub ColumnWidth(ByVal Index As Integer, ByVal Width As Integer)
    If Me.dg_check_list.NumColumns >= Index Then
      Me.dg_check_list.Column(Index).Width = Width
    End If
  End Sub

  Public Sub ReDraw(ByVal bRedraw As Boolean)
    Me.dg_check_list.Redraw = bRedraw
  End Sub

  Public Sub CurrentRow(ByVal index As Integer)
    Me.dg_check_list.CurrentRow = index
  End Sub

  Public Function Count() As Integer
    Return dg_check_list.NumRows
  End Function

  Public Sub UpdateNameId(ByVal IndexRow As Integer, ByVal Text As String)
    If Me.dg_check_list.NumRows > IndexRow And IndexRow >= 0 Then
      Me.dg_check_list.Cell(IndexRow, GRID_COLUMN_DESC).Value = Text
    End If
  End Sub

  Public Sub Add(ByVal Id As Integer, ByVal NameId As String)

    Me.dg_check_list.Redraw = False

    Me.dg_check_list.AddRow()

    Me.dg_check_list.Redraw = False

    If multiChoice Or Me.dg_check_list.NumRows = 1 Then
      Me.dg_check_list.Cell(dg_check_list.NumRows - 1, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    Else
      Me.dg_check_list.Cell(dg_check_list.NumRows - 1, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    End If

    Me.dg_check_list.Cell(dg_check_list.NumRows - 1, GRID_COLUMN_CODE).Value = Id
    Me.dg_check_list.Cell(dg_check_list.NumRows - 1, GRID_COLUMN_DESC).Value = NameId
    Me.dg_check_list.Cell(dg_check_list.NumRows - 1, GRID_COLUMN_LEVEL).Value = 1

    Me.dg_check_list.Redraw = True

  End Sub

  Public Sub Add(ByVal Id As Integer, ByVal NameId As String, ByVal Color As Integer)

    Me.dg_check_list.Redraw = False

    Me.dg_check_list.AddRow()

    Me.dg_check_list.Redraw = False

    If multiChoice Or Me.dg_check_list.NumRows = 1 Then
      Me.dg_check_list.Cell(dg_check_list.NumRows - 1, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    Else
      Me.dg_check_list.Cell(dg_check_list.NumRows - 1, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    End If


    Me.dg_check_list.Cell(dg_check_list.NumRows - 1, GRID_COLUMN_CODE).Value = Id
    Me.dg_check_list.Cell(dg_check_list.NumRows - 1, GRID_COLUMN_DESC).Value = NameId
    Me.dg_check_list.Cell(dg_check_list.NumRows - 1, GRID_COLUMN_COLOR).BackColor = GetColor(Color)
    Me.dg_check_list.Cell(dg_check_list.NumRows - 1, GRID_COLUMN_LEVEL).Value = 1

    Me.dg_check_list.Redraw = True

  End Sub

  Public Sub Add(ByVal Table As DataTable)

    Call Add(Table, 0, 1)

  End Sub

  Public Sub Add(ByVal Table As DataTable, ByVal IndexColId As Integer, ByVal IndexColName As Integer)
    Dim idx As Integer
    Dim aux_id As Integer
    Dim aux_name As String

    Me.dg_check_list.Clear()
    Me.dg_check_list.Redraw = False

    For idx = 0 To Table.Rows.Count - 1
      aux_id = CInt(Table.Rows.Item(idx).Item(IndexColId))
      aux_name = CStr(Table.Rows.Item(idx).Item(IndexColName))
      Call Add(aux_id, aux_name)
    Next

    If Me.dg_check_list.NumRows > 0 Then
      Me.dg_check_list.CurrentRow = 0
    End If

    Me.dg_check_list.Redraw = True

  End Sub

  Public Sub Add(ByVal Table As DataTable, ByVal ColId As String, ByVal ColName As String)
    Dim idx As Integer
    Dim aux_id As Integer
    Dim aux_name As String

    Me.dg_check_list.Clear()
    Me.dg_check_list.Redraw = False

    For idx = 0 To Table.Rows.Count - 1
      aux_id = CInt(Table.Rows.Item(idx).Item(ColId))
      aux_name = CStr(Table.Rows.Item(idx).Item(ColName))
      Call Add(aux_id, aux_name)
    Next

    If Me.dg_check_list.NumRows > 0 Then
      Me.dg_check_list.CurrentRow = 0
    End If

    Me.dg_check_list.Redraw = True

  End Sub

  Public Sub Add(ByVal Id As Integer, ByVal NlsId As Integer)
    Call Add(Id, NLS_GetString(NlsId))
  End Sub

  Public Sub Add(ByVal NameList As List(Of String))
    For Each _name_id As String In NameList
      Call Add(Me.dg_check_list.NumRows + 1, _name_id)
    Next
  End Sub

  Public Sub Add(ByVal Dic As Dictionary(Of Integer, String))

    Me.m_raise_events = False
    For Each _pair As KeyValuePair(Of Integer, String) In Dic
      Call Add(_pair.Key, _pair.Value)
    Next
    Me.m_raise_events = True
  End Sub

  Public Sub Add(ByVal Level As Int32, ByVal Dic As Dictionary(Of Integer, String))

    Me.m_raise_events = False
    For Each _pair As KeyValuePair(Of Integer, String) In Dic
      Call Add(Level, _pair.Key, _pair.Value)
    Next
    Me.m_raise_events = True
  End Sub

  'DCS 17-SEP-2014: Add optional selected parameter for check items to add
  Public Sub Add(ByVal Level As Int32, ByVal Id As Integer, ByVal NameId As String, Optional ByVal Selected As Boolean = False)

    Me.dg_check_list.Redraw = False

    Me.dg_check_list.AddRow()

    Me.dg_check_list.Redraw = False

    m_prefix = ""

    If Level > 1 Then
      For _idx As Integer = 1 To Level - 1
        m_prefix += LEVEL_2_TEXT
      Next
    End If

    If Level < m_define_levels Then
      m_prefix += LEVEL_1_TEXT_MINOR
    Else
      m_prefix += ""
    End If

    Me.dg_check_list.Cell(dg_check_list.NumRows - 1, GRID_COLUMN_CODE).Value = Id
    Me.dg_check_list.Cell(dg_check_list.NumRows - 1, GRID_COLUMN_DESC).Value = m_prefix & NameId
    Me.dg_check_list.Cell(dg_check_list.NumRows - 1, GRID_COLUMN_LEVEL).Value = Level

    If Selected And (multiChoice Or Me.dg_check_list.NumRows = 1) Then
      Me.dg_check_list.Cell(dg_check_list.NumRows - 1, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
      If Selected Then
        dg_check_list_CellDataChangedEvent(dg_check_list.NumRows - 1, GRID_COLUMN_CHECKED)
      End If
    Else
      Me.dg_check_list.Cell(dg_check_list.NumRows - 1, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    End If

    Me.dg_check_list.Redraw = True

    m_levels = 2

  End Sub

  Public Sub Add(ByVal Level As Int32, ByVal Id As Integer, ByVal NameId As String, ByVal Description As String)

    Call Add(Level, Id, NameId)

    Me.m_descriptions.Add(dg_check_list.NumRows - 1, Description)
    Me.dg_check_list.Redraw = True

  End Sub

  Public Sub ResizeGrid()
    Dim _mide As Int32

    dg_check_list.MinimumSize() = New System.Drawing.Size(panel_grid.Width - 22, panel_grid.Height - 18)

    dg_check_list.Redraw = False

    _mide = 0
    For i As Int32 = 0 To dg_check_list.NumRows - 1
      _mide += dg_check_list.Row(i).Height
    Next
    dg_check_list.Height = (_mide / 15) + 5

    _mide = 0
    For i As Int32 = 0 To dg_check_list.NumColumns - 1
      _mide += dg_check_list.Column(i).Width
    Next
    dg_check_list.Width = (_mide / 15) + 5

    If dg_check_list.Height < panel_grid.Height And dg_check_list.Width < panel_grid.Width Then
      dg_check_list.Height = panel_grid.Height
      dg_check_list.Width = panel_grid.Width - 1
    Else
      If dg_check_list.Height < panel_grid.Height Then
        dg_check_list.Height = panel_grid.Height - 17
      End If
      If dg_check_list.Width < panel_grid.Width Then
        dg_check_list.Width = panel_grid.Width - 17
      End If
    End If

    If dg_check_list.Width <= panel_grid.Width Then
      dg_check_list.Column(3).Width = (dg_check_list.Width * 15) - ((_mide - dg_check_list.Column(3).Width) + 75)
    End If

    btn_uncheck_all.Location = New System.Drawing.Point(panel_grid.Location.X + panel_grid.Width - btn_uncheck_all.Width - 1, btn_uncheck_all.Location.Y)

    dg_check_list.Redraw = True

  End Sub

  Private Function SplitToolTip(ByVal StringValue As String) As String
    Dim _str_array As String()
    Dim _space As String = " "
    Dim _cr As String = vbCrLf
    Dim _str_one_word As String = String.Empty
    Dim _str_builder As String = String.Empty
    Dim _str_return As String = String.Empty


    _str_array = StringValue.Split(_space)

    For Each _str_one_word In _str_array
      _str_builder = _str_builder & _str_one_word & _space
      If Len(_str_builder) > 70 Then
        _str_return = _str_return & _str_builder & _cr
        _str_builder = ""
      End If
    Next

    If Len(_str_builder) < 8 Then
      _str_return = _str_return.Substring(0, _str_return.Length - 2)
    End If

    Return _str_return & _str_builder

  End Function

#End Region ' Check list Items 

#Region " SelectedIndex "

  Public Property SelectedIndexes() As Integer()
    Get
      Dim _length() As Integer = {}
      Dim _index As Integer = 0
      Dim _count_selected As Integer = 0

      For _idx As Integer = 0 To Me.dg_check_list.NumRows - 1
        If Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
          _count_selected += 1
        End If
      Next

      If _count_selected > 0 Then
        ReDim _length(_count_selected - 1)
      Else
        ReDim _length(Me.dg_check_list.NumRows - 1)
      End If

      For _idx As Integer = 0 To Me.dg_check_list.NumRows - 1

        If Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Or _count_selected = 0 Then
          _length(_index) = Me.dg_check_list.Cell(_idx, GRID_COLUMN_CODE).Value
          _index += 1
        End If

      Next

      Return _length

    End Get

    Set(ByVal NewValue As Integer())
      For idx As Integer = 0 To Me.dg_check_list.NumRows - 1

        For idx2 As Integer = 0 To NewValue.Length - 1
          If Me.dg_check_list.Cell(idx, 0).Value = NewValue(idx2) Then
            Me.dg_check_list.Cell(idx, 0).Value = uc_grid.GRID_CHK_CHECKED
            Exit For
          Else
            Me.dg_check_list.Cell(idx, 0).Value = uc_grid.GRID_CHK_UNCHECKED
          End If
        Next

      Next
    End Set

  End Property

  Public Property SelectedIndexesList() As String
    Get
      Dim _select_indexes As String = ""
      Dim _count_selected As Integer = 0

      For _idx As Integer = 0 To Me.dg_check_list.NumRows - 1
        If (Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED _
            Or dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED) _
          And ((dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value <> 2 And m_define_levels = 2) _
              Or (dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value > m_define_levels - 1 And m_define_levels > 2)) Then

          _count_selected += 1
        End If
      Next

      For _idx As Integer = 0 To Me.dg_check_list.NumRows - 1
        If (dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED _
            Or dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED _
            Or _count_selected = 0) _
          And ((dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value <> 2 And m_define_levels = 2) _
              Or (dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value > m_define_levels - 1 And m_define_levels > 2)) Then

          _select_indexes = _select_indexes & Me.dg_check_list.Cell(_idx, GRID_COLUMN_CODE).Value.ToString() & ", "
        End If
      Next
      If _select_indexes.Length > 2 Then
        _select_indexes = _select_indexes.Substring(0, _select_indexes.Length - 2)
      End If

      Return _select_indexes
    End Get

    Set(ByVal value As String)
    End Set

  End Property

  Public Property SelectedIndexesListLevel2() As String
    Get
      Dim _select_indexes As String = ""
      Dim _count_selected As Integer = 0

      For _idx As Integer = 0 To Me.dg_check_list.NumRows - 1
        If Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
          _count_selected += 1
        End If
      Next

      For _idx As Integer = 0 To Me.dg_check_list.NumRows - 1
        If (dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Or _count_selected = 0) _
          And dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value = 2 Then
          _select_indexes = _select_indexes & Me.dg_check_list.Cell(_idx, GRID_COLUMN_CODE).Value.ToString() & ", "
        End If
      Next
      If _select_indexes.Length > 2 Then
        _select_indexes = _select_indexes.Substring(0, _select_indexes.Length - 2)
      End If

      Return _select_indexes
    End Get

    Set(ByVal value As String)
    End Set

  End Property

  Public Property SelectedValuesList() As String
    Get
      Dim _select_indexes As String = ""
      Dim _count_selected As Integer = 0

      For _idx As Integer = 0 To Me.dg_check_list.NumRows - 1
        If Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED _
           Or Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED Then
          _count_selected += 1
        End If
      Next

      For _idx As Integer = 0 To Me.dg_check_list.NumRows - 1
        If dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED _
           Or Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED Or _count_selected = 0 Then
          If m_levels = 1 Then
            _select_indexes = _select_indexes & Me.dg_check_list.Cell(_idx, GRID_COLUMN_DESC).Value & ", "
          Else
            _select_indexes = _select_indexes & Me.dg_check_list.Cell(_idx, GRID_COLUMN_DESC).Value.Trim().Replace(LEVEL_1_TEXT_PLUS, "").Replace(LEVEL_1_TEXT_MINOR, "") & ", "
          End If
        End If
      Next

      If _select_indexes.Length > 2 Then
        _select_indexes = _select_indexes.Substring(0, _select_indexes.Length - 2)
      End If

      Return _select_indexes
    End Get

    Set(ByVal value As String)
    End Set

  End Property

  Public Property SelectedValuesListLevel2() As String
    Get
      Dim _select_indexes As String = ""
      Dim _count_selected As Integer = 0

      For _idx As Integer = 0 To Me.dg_check_list.NumRows - 1
        If Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED _
           Or Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED Then
          _count_selected += 1
        End If
      Next

      For _idx As Integer = 0 To Me.dg_check_list.NumRows - 1
        If (dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Or _count_selected = 0) _
          And dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value = 2 Then
          _select_indexes = _select_indexes & Me.dg_check_list.Cell(_idx, GRID_COLUMN_DESC).Value.Trim().Replace(LEVEL_1_TEXT_PLUS, "").Replace(LEVEL_1_TEXT_MINOR, "") & ", "
        End If
      Next

      If _select_indexes.Length > 2 Then
        _select_indexes = _select_indexes.Substring(0, _select_indexes.Length - 2)
      End If

      Return _select_indexes
    End Get

    Set(ByVal value As String)
    End Set

  End Property


  Public Property SelectedValuesArray() As String()
    Get
      Dim _select_indexes As String()
      Dim _count_selected As Integer = 0
      Dim _count As Integer = 0

      For _idx As Integer = 0 To Me.dg_check_list.NumRows - 1
        If Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED _
           Or Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED Then
          _count_selected += 1
        End If
      Next

      If _count_selected > 0 Then
        ReDim _select_indexes(_count_selected - 1)
      Else
        ReDim _select_indexes(Me.dg_check_list.NumRows - 1)
      End If

      For _idx As Integer = 0 To Me.dg_check_list.NumRows - 1
        If dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED _
           Or Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED Or _count_selected = 0 Then

          _select_indexes(_count) = Me.dg_check_list.Cell(_idx, GRID_COLUMN_DESC).Value.Trim().Replace(LEVEL_1_TEXT_PLUS, "").Replace(LEVEL_1_TEXT_MINOR, "")
          _count += 1
        End If
      Next

      Return _select_indexes
    End Get

    Set(ByVal value As String())

    End Set
  End Property

  Public Property ValuesArray() As String()
    Get
      Dim _text() As String = {}

      If Me.dg_check_list.NumRows = 0 Then
        Return _text
      End If

      ReDim _text(Me.dg_check_list.NumRows - 1)

      For _idx As Integer = 0 To Me.dg_check_list.NumRows - 1
        _text(_idx) = Me.dg_check_list.Cell(_idx, GRID_COLUMN_DESC).Value
      Next

      Return _text

    End Get

    Set(ByVal NewValue As String())
    End Set

  End Property

  Public Property GroupBoxText() As String

    Get
      Return Me.gb_checked_list.Text
    End Get

    Set(ByVal Value As String)
      Me.gb_checked_list.Text = Value
    End Set

  End Property

  Public Property SetLevels() As Integer

    Get
      Return m_define_levels
    End Get

    Set(ByVal Value As Integer)
      If Value > 0 And Value < 9 Then
        m_define_levels = Value
      Else
        m_define_levels = DEFAULT_LEVELS
      End If
    End Set

  End Property

  'Public Property multiChoice As Boolean = True

  'End Property

  Public Property multiChoice() As Boolean
    Get
      Return True
    End Get
    Set(ByVal value As Boolean)

    End Set
  End Property

  ' PURPOSE: Checks if all elements or none element is selected in the list
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:  True (All or None)
  Public ReadOnly Property IsAllOrNoneSelected() As Boolean
    Get
      Dim _count_selected As Integer = 0
      Dim _count_all As Integer = 0

      'It counts the selected elements
      For _idx As Integer = 0 To Me.dg_check_list.NumRows - 1
        If (Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED _
               Or dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED) _
            And ((dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value <> 2 And m_define_levels = 2) _
               Or (dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value > m_define_levels - 1 And m_define_levels > 2)) Then

          _count_selected += 1
        End If
      Next

      If (_count_selected = 0) Then
        'None selected
        Return True
      End If

      'It counts all elements in the list
      For _idx As Integer = 0 To Me.dg_check_list.NumRows - 1
        If ((dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value <> 2 And m_define_levels = 2) _
              Or (dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value > m_define_levels - 1 And m_define_levels > 2)) Then

          _count_all += 1
        End If
      Next

      Return (_count_selected = _count_all)
    End Get
  End Property

  Public ReadOnly Property IsNoneSelected() As Boolean
    Get
      Dim _count_selected As Integer = 0

      'It counts the selected elements
      For _idx As Integer = 0 To Me.dg_check_list.NumRows - 1
        If (Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED _
               Or dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED) _
            And ((dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value <> 2 And m_define_levels = 2) _
               Or (dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value > m_define_levels - 1 And m_define_levels > 2)) Then

          _count_selected += 1
        End If
      Next

      Return (_count_selected = 0)

    End Get
  End Property

#End Region ' SelectedIndex

#Region " Events "

  Private Sub dg_check_list_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_check_list.CellDataChangedEvent
    Dim _row As Int32
    Dim _checked As List(Of Boolean)
    Dim _unchecked As List(Of Boolean)
    Dim _row_level As Integer
    Dim _temp_level As Integer
    Dim _is_next_parent As Boolean = False
    Dim _parent_row As List(Of Integer)
    Dim _next_parent_row As List(Of Integer)


    _checked = New List(Of Boolean)
    _unchecked = New List(Of Boolean)
    _parent_row = New List(Of Integer)
    _next_parent_row = New List(Of Integer)


    For _idx As Integer = 0 To m_define_levels
      _checked.Add(False)
      _unchecked.Add(False)
      _parent_row.Add(-1)
      _next_parent_row.Add(-1)
    Next

    _row = Row
    _row_level = dg_check_list.Cell(Row, GRID_COLUMN_LEVEL).Value
    _temp_level = _row_level
    _next_parent_row(_row_level) = _row

    If Not multiChoice Then
      For _idx As Integer = 0 To dg_check_list.NumRows - 1
        dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      Next
      dg_check_list.Cell(Row, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    End If

    If m_levels = 1 Then
      Return
    End If

    If m_semaphore Then
      Return
    End If
    m_semaphore = True

    ' Per if only one item, enable/disable parents
    For _idx As Integer = _row To 1 Step -1
      If dg_check_list.Cell(_idx - 1, GRID_COLUMN_LEVEL).Value < dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value _
         And dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value <> uc_grid.GRID_CHK_CHECKED Then
        dg_check_list.Cell(_idx - 1, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      ElseIf dg_check_list.Cell(_idx - 1, GRID_COLUMN_LEVEL).Value < dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value _
         And dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        dg_check_list.Cell(_idx - 1, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
      Else
        Exit For
      End If
    Next

    ' If it has child, are put like the parent
    For _idx As Integer = _row To Me.dg_check_list.NumRows - 1
      If _idx > _row And dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value <= _row_level Then
        Exit For
      End If
      Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = Me.dg_check_list.Cell(_row, GRID_COLUMN_CHECKED).Value
    Next

    ' Find all parent nodes
    If dg_check_list.Cell(Row, GRID_COLUMN_LEVEL).Value <> 1 Then
      For _idx As Integer = Row To 0 Step -1
        If _temp_level >= dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value Then
          _temp_level = dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value
          If _parent_row(_temp_level) = -1 Then
            _parent_row(_temp_level) = _idx
          End If
        End If
        If dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value = 1 Then
          Row = _idx

          Exit For
        End If
      Next
    End If

    ' Find all following nodes
    _temp_level = dg_check_list.Cell(_row, GRID_COLUMN_LEVEL).Value
    For _idx As Integer = _row To Me.dg_check_list.NumRows - 1
      If _idx > _row And _temp_level > dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value Then
        For _level As Integer = _temp_level To dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value Step -1
          If _next_parent_row(_level) = -1 Then
            _next_parent_row(_level) = _idx
          End If
        Next
        _temp_level = dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value
      End If
      If dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value = 1 Then
        Exit For
      End If
    Next

    ' if no more nodes, set to last row
    For _idx As Integer = 1 To _row_level
      If _next_parent_row(_idx) = -1 Then
        _next_parent_row(_idx) = Me.dg_check_list.NumRows
      End If
    Next

    ' For each level look if it has children checked and unchecked
    For _idx As Integer = Row To Me.dg_check_list.NumRows - 1
      If _idx > _row And dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value <= 1 Then
        Exit For
      End If
      _temp_level = dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value

      For _level As Integer = _temp_level To 1 Step -1
        If _idx >= _parent_row(_level) And _idx < _next_parent_row(_level) Then
          Select Case Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value
            Case uc_grid.GRID_CHK_CHECKED
              _checked(_level) = True
            Case uc_grid.GRID_CHK_UNCHECKED
              _unchecked(_level) = True
          End Select
        End If
      Next
    Next

    ' Parental check
    For _idx As Integer = Row To Me.dg_check_list.NumRows - 1
      If _idx > _row And dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value <= _row_level Then
        Exit For
      End If
      _temp_level = dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value

      If _idx >= _parent_row(_temp_level) And _idx < _next_parent_row(_temp_level) Then
        Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
        If _checked(_temp_level) Then
          Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
          If _unchecked(_temp_level) Then
            Me.dg_check_list.Cell(_idx, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
          End If
        End If
      End If
    Next

    m_semaphore = False

    Return

  End Sub

  Private Sub dg_list_SetToolTipTextEvent(ByVal GridRowas As Integer, ByVal GridCol As Integer, ByRef Txt As String) Handles dg_check_list.SetToolTipTextEvent

    If m_descriptions.Count > 0 Then
      If m_descriptions.ContainsKey(GridRowas) Then
        Txt = m_descriptions(GridRowas)
      End If

      If Txt.Length > 75 Then
        Txt = SplitToolTip(Txt)
      End If
    End If

  End Sub

  Private Sub btn_check_all_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_check_all.Click
    Dim _idx_rows As Integer
    For _idx_rows = 0 To dg_check_list.NumRows - 1
      dg_check_list.Cell(_idx_rows, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    Next
  End Sub

  Private Sub btn_uncheck_all_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_uncheck_all.Click
    Dim _idx_rows As Integer
    For _idx_rows = 0 To dg_check_list.NumRows - 1
      dg_check_list.Cell(_idx_rows, GRID_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next
  End Sub

  Private Sub uc_checked_list_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Resize

    'If Me.Height <> CONTROL_HEIGHT And Me.Height <> CONTROL_HEIGHT_2 Then
    '  Me.Height = CONTROL_HEIGHT
    'End If
    Call InitControl()

  End Sub ' uc_provider_filter_Resize

  Public Sub CollapseAll()

    m_position_scroll = New System.Drawing.Point(panel_grid.AutoScrollPosition)

    For _idx As Integer = 0 To Me.dg_check_list.NumRows - 1
      If dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value = 1 Then
        Call CollapseItem(_idx)
      End If
    Next

    Call ResizeGrid()

  End Sub

  Public Sub ExpandAll()

    m_position_scroll = New System.Drawing.Point(panel_grid.AutoScrollPosition)

    For _idx As Integer = 0 To Me.dg_check_list.NumRows - 1
      If dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value = 1 Then
        Call ExpandItem(_idx)
      End If
    Next

    Call ResizeGrid()

  End Sub

  Private Sub CollapseItem(ByVal RowIndex As Int32)
    Dim _row_level As Integer

    m_position_scroll = New System.Drawing.Point(panel_grid.AutoScrollPosition)

    Me.dg_check_list.Cell(RowIndex, GRID_COLUMN_DESC).Value = Me.dg_check_list.Cell(RowIndex, GRID_COLUMN_DESC).Value.Replace(LEVEL_1_TEXT_MINOR, LEVEL_1_TEXT_PLUS)
    _row_level = Me.dg_check_list.Cell(RowIndex, GRID_COLUMN_LEVEL).Value

    If m_height = 0 And Me.dg_check_list.Row(RowIndex).Height > 0 Then
      m_height = Me.dg_check_list.Row(RowIndex).Height
    End If

    For _idx As Integer = RowIndex To Me.dg_check_list.NumRows - 1
      If RowIndex <> _idx And dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value <= _row_level Then

        Exit For
      End If

      If RowIndex <> _idx Then
        Me.dg_check_list.Cell(_idx, GRID_COLUMN_DESC).Value = Me.dg_check_list.Cell(_idx, GRID_COLUMN_DESC).Value.Replace(LEVEL_1_TEXT_MINOR, LEVEL_1_TEXT_PLUS)
        Me.dg_check_list.Row(_idx).Height = 0
      End If
    Next

    Call ResizeGrid()

  End Sub

  Private Sub ExpandItem(ByVal RowIndex As Int32)
    Dim _row_level_mother As Integer
    Dim _row_level_child As Integer

    m_position_scroll = New System.Drawing.Point(panel_grid.AutoScrollPosition)

    Me.dg_check_list.Cell(RowIndex, GRID_COLUMN_DESC).Value = Me.dg_check_list.Cell(RowIndex, GRID_COLUMN_DESC).Value.Replace(LEVEL_1_TEXT_PLUS, LEVEL_1_TEXT_MINOR)
    _row_level_mother = Me.dg_check_list.Cell(RowIndex, GRID_COLUMN_LEVEL).Value

    If m_height = 0 And Me.dg_check_list.Row(RowIndex).Height > 0 Then
      m_height = Me.dg_check_list.Row(RowIndex).Height
    End If

    For _idx As Integer = RowIndex To Me.dg_check_list.NumRows - 1
      _row_level_child = dg_check_list.Cell(_idx, GRID_COLUMN_LEVEL).Value
      If RowIndex <> _idx And _row_level_child <= _row_level_mother Then

        Exit For
      End If

      If RowIndex <> _idx And (_row_level_child = _row_level_mother + 1 Or (_row_level_mother + 1 = m_define_levels And _row_level_child > _row_level_mother)) Then
        Me.dg_check_list.Row(_idx).Height = m_height
      End If
    Next

    Call ResizeGrid()

  End Sub

  Private Sub dg_check_list_DataSelectedEvent() Handles dg_check_list.DataSelectedEvent
    Dim _row As Int32 = 0
    Dim _show As Boolean = True

    _row = dg_check_list.SelectedRows(0)

    If m_levels = 1 Then
      Return
    End If

    If dg_check_list.Cell(_row, GRID_COLUMN_LEVEL).Value = m_define_levels Then
      Return
    End If

    If dg_check_list.Cell(_row, GRID_COLUMN_DESC).Value.Contains(LEVEL_1_TEXT_PLUS) Then
      ExpandItem(_row)
    Else
      CollapseItem(_row)
    End If

    TimerTest.Start()

  End Sub

  Private Sub panel_grid_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles panel_grid.Scroll
    m_position_scroll = New System.Drawing.Point(panel_grid.AutoScrollPosition)

  End Sub

  Private Sub tmr_mouse_move_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TimerTest.Tick
    If Math.Abs(panel_grid.AutoScrollPosition.Y) <> Math.Min(Math.Abs(m_position_scroll.Y), panel_grid.VerticalScroll.Maximum) Then
      panel_grid.AutoScrollPosition = New System.Drawing.Point(Math.Abs(m_position_scroll.X), Math.Min(Math.Abs(m_position_scroll.Y), panel_grid.VerticalScroll.Maximum))
      m_position_scroll.Y = Math.Abs(panel_grid.AutoScrollPosition.Y)
    Else
      TimerTest.Stop()
    End If

  End Sub

  Private Sub dg_check_list_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dg_check_list.Enter
    m_position_scroll = New System.Drawing.Point(panel_grid.AutoScrollPosition)
    TimerTest.Start()
  End Sub

#End Region

End Class


