'-------------------------------------------------------------------
' Copyright � 2009 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_daily_session_selector.vb  
'
' DESCRIPTION:   
'
' AUTHOR:        Agust� Poch
'
' CREATION DATE: 12-NOV-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-APR-2009  APB    Initial version
' 07-MAY-2015  FOS    TASK 1289: Capture and Raise events CloseUp to datePicker
' 28-JUL-2015  DCS    Fixed Bug #TFS-3648: Error Calculating TERMINALS_CONNECTED, the date used included closing hour. Add new parameter for GetSqlFilterCondition
'--------------------------------------------------------------------

Imports System.Windows.Forms
Imports GUI_CommonOperations.ModuleDateTimeFormats
Imports WSI.Common

Public Class uc_daily_session_selector
  Inherits System.Windows.Forms.UserControl

#Region " Members "
  Private m_init As Boolean = False

  Public Event DatePickerValueChanged()
  Public Event DatePickerCloseUp()

#End Region

#Region " Constants "

#End Region

#Region " Windows Form Designer generated code "

  Protected WithEvents dtp_from As GUI_Controls.uc_date_picker
  Protected WithEvents dtp_to As GUI_Controls.uc_date_picker
  Public WithEvents cmb_closing_hour As GUI_Controls.uc_combo
  Public WithEvents lbl_to_hour As System.Windows.Forms.Label
  Protected WithEvents gb_dates As System.Windows.Forms.GroupBox
  Public WithEvents lbl_from_hour As System.Windows.Forms.Label

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call
    Call Me.Init(0)

  End Sub

  'UserControl overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Private WithEvents Panel1 As System.Windows.Forms.Panel
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(uc_daily_session_selector))
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.lbl_to_hour = New System.Windows.Forms.Label()
    Me.lbl_from_hour = New System.Windows.Forms.Label()
    Me.gb_dates = New System.Windows.Forms.GroupBox()
    Me.cmb_closing_hour = New GUI_Controls.uc_combo()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.Panel1.SuspendLayout()
    Me.gb_dates.SuspendLayout()
    Me.SuspendLayout()
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.lbl_to_hour)
    Me.Panel1.Controls.Add(Me.lbl_from_hour)
    Me.Panel1.Controls.Add(Me.cmb_closing_hour)
    Me.Panel1.Controls.Add(Me.dtp_to)
    Me.Panel1.Controls.Add(Me.dtp_from)
    Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
    Me.Panel1.Location = New System.Drawing.Point(3, 16)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(254, 101)
    Me.Panel1.TabIndex = 0
    '
    'lbl_to_hour
    '
    Me.lbl_to_hour.AutoSize = True
    Me.lbl_to_hour.ForeColor = System.Drawing.Color.Blue
    Me.lbl_to_hour.Location = New System.Drawing.Point(202, 37)
    Me.lbl_to_hour.Name = "lbl_to_hour"
    Me.lbl_to_hour.Size = New System.Drawing.Size(34, 13)
    Me.lbl_to_hour.TabIndex = 12
    Me.lbl_to_hour.Text = "00:00"
    '
    'lbl_from_hour
    '
    Me.lbl_from_hour.AutoSize = True
    Me.lbl_from_hour.ForeColor = System.Drawing.Color.Blue
    Me.lbl_from_hour.Location = New System.Drawing.Point(202, 9)
    Me.lbl_from_hour.Name = "lbl_from_hour"
    Me.lbl_from_hour.Size = New System.Drawing.Size(34, 13)
    Me.lbl_from_hour.TabIndex = 11
    Me.lbl_from_hour.Text = "00:00"
    '
    'gb_dates
    '
    Me.gb_dates.Controls.Add(Me.Panel1)
    Me.gb_dates.Dock = System.Windows.Forms.DockStyle.Fill
    Me.gb_dates.Location = New System.Drawing.Point(0, 0)
    Me.gb_dates.Name = "gb_dates"
    Me.gb_dates.Size = New System.Drawing.Size(260, 120)
    Me.gb_dates.TabIndex = 0
    Me.gb_dates.TabStop = False
    Me.gb_dates.Text = "xDate"
    '
    'cmb_closing_hour
    '
    Me.cmb_closing_hour.AllowUnlistedValues = False
    Me.cmb_closing_hour.AutoCompleteMode = False
    Me.cmb_closing_hour.IsReadOnly = False
    Me.cmb_closing_hour.Location = New System.Drawing.Point(26, 62)
    Me.cmb_closing_hour.Name = "cmb_closing_hour"
    Me.cmb_closing_hour.SelectedIndex = -1
    Me.cmb_closing_hour.Size = New System.Drawing.Size(173, 24)
    Me.cmb_closing_hour.SufixText = "Sufix Text"
    Me.cmb_closing_hour.SufixTextVisible = True
    Me.cmb_closing_hour.TabIndex = 5
    Me.cmb_closing_hour.TextCombo = Nothing
    Me.cmb_closing_hour.TextWidth = 110
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(12, 32)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(188, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 4
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(12, 4)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(188, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 3
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'uc_daily_session_selector
    '
    Me.Controls.Add(Me.gb_dates)
    Me.Name = "uc_daily_session_selector"
    Me.Size = New System.Drawing.Size(260, 120)
    Me.Panel1.ResumeLayout(False)
    Me.Panel1.PerformLayout()
    Me.gb_dates.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Properties "

  Public Property FromDate() As Date
    Get
      Return Me.dtp_from.Value
    End Get
    Set(ByVal Value As Date)
      If Not m_init Then
        Return
      End If
      Me.dtp_from.Value = New Date(Value.Year, Value.Month, Value.Day, 0, 0, 0)
    End Set
  End Property


  Public Property FromDateSelected() As Boolean
    Get
      Return Me.dtp_from.Checked
    End Get
    Set(ByVal Value As Boolean)
      Me.dtp_from.Checked = Value
    End Set
  End Property

  Public Property ToDate() As Date
    Get
      Return Me.dtp_to.Value
    End Get
    Set(ByVal Value As Date)
      If Not m_init Then
        Return
      End If
      Me.dtp_to.Value = New Date(Value.Year, Value.Month, Value.Day, 0, 0, 0)
    End Set
  End Property

  Public Property ToDateSelected() As Boolean
    Get
      Return Me.dtp_to.Checked
    End Get
    Set(ByVal Value As Boolean)
      Me.dtp_to.Checked = Value
    End Set
  End Property

  Public Property ClosingTime() As Integer
    Get
      Return Me.cmb_closing_hour.SelectedIndex
    End Get
    Set(ByVal Value As Integer)
      If Value >= 0 And Value <= 23 Then
        Me.cmb_closing_hour.SelectedIndex = Value
      End If
    End Set
  End Property

  Public Property ClosingTimeEnabled() As Boolean
    Get
      Return Me.cmb_closing_hour.Enabled
    End Get
    Set(ByVal value As Boolean)
      Me.cmb_closing_hour.Enabled = value
    End Set
  End Property

  Public Property ShowBorder() As Boolean
    Get
      Return Me.gb_dates.Visible
    End Get
    Set(ByVal Value As Boolean)
      If Value Then
        gb_dates.Controls.Add(Panel1)
        gb_dates.Visible = True
        dtp_from.Top = 4
        dtp_from.Left = 12
        lbl_from_hour.Top = 12
        lbl_from_hour.Left = 202
        dtp_to.Top = 32
        dtp_to.Left = 12
        lbl_to_hour.Top = 38
        lbl_to_hour.Left = 202
        cmb_closing_hour.Top = 62
        cmb_closing_hour.Left = 14
      Else
        Me.Controls.Add(Panel1)
        gb_dates.Visible = False
        dtp_from.Top = 21
        dtp_from.Left = 15
        lbl_from_hour.Top = 29
        lbl_from_hour.Left = 205
        dtp_to.Top = 49
        dtp_to.Left = 15
        lbl_to_hour.Top = 55
        lbl_to_hour.Left = 205
        cmb_closing_hour.Top = 79
        cmb_closing_hour.Left = 17
      End If

    End Set
  End Property

  Public Overrides Property Text() As String
    Get
      Return Me.gb_dates.Text
    End Get
    Set(ByVal Value As String)
      Me.gb_dates.Text = Value
    End Set
  End Property

  Public Property FromDateText() As String
    Get
      Return Me.dtp_from.Text
    End Get
    Set(ByVal Value As String)
      If Not m_init Then
        Return
      End If
      Me.dtp_from.Text = Value
    End Set
  End Property

  Public Property ToDateText() As String
    Get
      Return Me.dtp_to.Text
    End Get
    Set(ByVal Value As String)
      If Not m_init Then
        Return
      End If
      Me.dtp_to.Text = Value
    End Set
  End Property


#End Region ' Properties

#Region " Public "

  Public Sub Init(ByVal NlsId As Integer, Optional ByVal ForceFromFilter As Boolean = False, Optional ByVal DisableToFilter As Boolean = False)

    If NlsId = 0 Then
      Me.Text = ""
    Else
      Me.Text = GUI_CommonMisc.NLS_GetString(NlsId)
    End If

    If ForceFromFilter Then
      Me.dtp_from.ShowCheckBox = False
    End If

    ' RCI 10-AUG-2011: Flag to disable ToDate control. If disabled, it is updated automatically when FromDate is changed.
    If DisableToFilter Then
      Me.dtp_to.Enabled = False
    End If

    Me.dtp_from.Text = GLB_NLS_GUI_CONTROLS.GetString(339)
    Me.dtp_to.Text = GLB_NLS_GUI_CONTROLS.GetString(340)

    If Not m_init Then
      ' Date pickers
      Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, 0)
      Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, 0)

      ' Initialize combo with hours
      cmb_load()
      Me.cmb_closing_hour.SelectedIndex = 0

      Call AddHandlers()
      m_init = True
    End If

  End Sub ' Init

  Public Function GetSqlFilterCondition(ByVal FilterField As String, Optional AddTime As Boolean = True) As String
    Dim sql_filter As String = ""

    If FilterField.Length > 0 Then
      If Me.dtp_from.Checked Then
        sql_filter = "(" & FilterField & " >= " & GUI_FormatDateDB(IIf(AddTime, Me.dtp_from.Value.AddHours(Me.cmb_closing_hour.SelectedIndex), Me.dtp_from.Value)) & ")"
      End If

      If Me.dtp_to.Checked Then
        If sql_filter.Length > 0 Then
          sql_filter = sql_filter & " AND "
        End If
        sql_filter = sql_filter & "(" & FilterField & " < " & GUI_FormatDateDB(IIf(AddTime, Me.dtp_to.Value.AddHours(Me.cmb_closing_hour.SelectedIndex), Me.dtp_to.Value)) & ")"
      End If
    End If

    Return sql_filter
  End Function ' GetSqlFilterCondition

  Public Sub HideClosingHourCombo()
    Me.cmb_closing_hour.Hide()
  End Sub

#End Region ' Public

#Region "Private"
  Private Sub cmb_load()

    Dim _time As DateTime
    Dim _idx As Int32
    Dim _now As DateTime

    _now = WGDB.Now
    _time = New DateTime(_now.Year, _now.Month, _now.Day)

    Me.cmb_closing_hour.Text = GLB_NLS_GUI_CONTROLS.GetString(376)

    For _idx = 0 To 23
      Me.cmb_closing_hour.Add(_idx, Format.CustomFormatTime(_time, False))
      _time = _time.AddHours(1)
    Next

  End Sub ' cmb_load

#End Region ' Private

#Region " Events "

  Private Sub UpdateClosingTimeLabels()
    Dim aux_time As DateTime

    aux_time = New DateTime(Now.Year, Now.Month, Now.Day, Me.cmb_closing_hour.SelectedIndex, 0, 0)
    lbl_from_hour.Text = GUI_FormatTime(aux_time, ENUM_FORMAT_TIME.FORMAT_HHMM)
    lbl_to_hour.Text = GUI_FormatTime(aux_time, ENUM_FORMAT_TIME.FORMAT_HHMM)
  End Sub ' UpdateClosingTimeLabels

  Private Sub UpdateDisabledToDateValue()
    If Not Me.dtp_to.Enabled Then
      Me.dtp_to.Value = Me.dtp_from.Value.AddDays(1)
      Me.dtp_to.Checked = True
    End If
    RaiseEvent DatePickerValueChanged()
  End Sub ' UpdateDisabledToDateValue

  Private Sub UpdateDisabledFromDateValue()
    RaiseEvent DatePickerValueChanged()
  End Sub ' UpdateDisabledFromDateValue

  Private Sub dtp_from_DatePickerCloseUp() Handles dtp_from.DatePickerCloseUp, dtp_to.DatePickerCloseUp
    RaiseEvent DatePickerCloseUp()
  End Sub


  Private Sub AddHandlers()
    AddHandler Me.cmb_closing_hour.ValueChangedEvent, AddressOf UpdateClosingTimeLabels
    AddHandler Me.dtp_from.DatePickerValueChanged, AddressOf UpdateDisabledToDateValue
    AddHandler Me.dtp_to.DatePickerValueChanged, AddressOf UpdateDisabledFromDateValue
  End Sub ' AddHandlers



#End Region ' Events



End Class
