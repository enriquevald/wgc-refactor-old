Public Class uc_graph
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Chart As AxMSChart20Lib.AxMSChart
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(uc_graph))
        Me.Chart = New AxMSChart20Lib.AxMSChart()
        CType(Me.Chart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Chart
        '
        Me.Chart.DataSource = Nothing
        Me.Chart.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Chart.Name = "Chart"
        Me.Chart.OcxState = CType(resources.GetObject("Chart.OcxState"), System.Windows.Forms.AxHost.State)
        Me.Chart.Size = New System.Drawing.Size(456, 344)
        Me.Chart.TabIndex = 0
        '
        'uc_graph
        '
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.Chart})
        Me.Name = "uc_graph"
        Me.Size = New System.Drawing.Size(456, 344)
        CType(Me.Chart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


    Public WriteOnly Property Title() As String
        Set(ByVal Value As String)

        End Set
    End Property

    Private m_num_values As Integer
    Private m_num_series As Integer
    Private m_values As Double(,)
    Private m_series_name() As String
    Private m_value_name() As String

    Public Sub SeriesInit(ByVal NumSeries As Integer, ByVal NumValues As Integer)
        m_num_series = NumSeries
        m_num_values = NumValues
        ReDim m_values(m_num_series, m_num_values)
        ReDim m_series_name(m_num_series)
        ReDim m_value_name(m_num_values)
    End Sub

    Public WriteOnly Property SeriesName(ByVal SeriesId As Integer) As String
        Set(ByVal Value As String)
            m_series_name(SeriesId) = Value
        End Set
    End Property

    Public WriteOnly Property ValueName(ByVal ValueId As Integer) As String
        Set(ByVal Value As String)
            m_value_name(ValueId) = Value
        End Set
    End Property

    Public WriteOnly Property SeriesValue(ByVal SeriesId As Integer, ByVal ValueId As Integer) As Double
        Set(ByVal Value As Double)
            If SeriesId > 0 And SeriesId <= m_num_series Then
                If ValueId > 0 And ValueId <= m_num_values Then
                    m_values(SeriesId, ValueId) = Value
                End If
            End If
        End Set
    End Property

    Public Sub CopyToClipBoard()
        Call Me.Chart.EditCopy()

    End Sub

    Public Sub DataRefresh()
        Dim matrix As Object(,)
        ReDim matrix(m_num_values + 1, m_num_series + 1)

        Dim series_id As Integer
        Dim value_id As Integer

        Me.Chart.Repaint = False

        For series_id = 0 To m_num_series
            For value_id = 0 To m_num_values
                matrix(1 + value_id, 1 + series_id) = m_values(series_id, value_id)
            Next
        Next

        Me.Chart.ChartData = matrix
        Me.Chart.Refresh()

        Me.Chart.Repaint = True

    End Sub


    Public Sub hola()
        'In this example, the sales for two companies are shown for four months.
        'Create the data array and bind it to the ChartData property.


        'Dim chart_ds As New ChartDataSet()
        'Dim tbl_agency As ChartDataSet.TableAgencyDataTable
        'Dim tbl_ranges As ChartDataSet.TableRangeDataTable
        'Dim tbl_values As ChartDataSet.TableValuesDataTable

        'XVV Variable not use Dim ag As Integer

        'tbl_agency = chart_ds.TableAgency
        'tbl_ranges = chart_ds.TableRange
        'tbl_values = chart_ds.TableValues

        ''Dim row_agency As ChartDataSet.TableAgencyRow
        'Dim row_range As ChartDataSet.TableRangeRow
        'Dim row_value As ChartDataSet.TableValuesRow


        'For ag = 1 To Val(TextBox1.Text)
        '  row_agency = tbl_agency.NewRow()
        '  row_agency.InstanceId = ag
        '  row_agency.Name = "Agency " + CStr(row_agency.InstanceId)
        '  tbl_agency.Rows.Add(row_agency)
        'Next


        'Dim idx As Integer
        'For idx = 1 To Val(TextBox2.Text)
        '  row_range = tbl_ranges.NewRow()
        '  row_range.RangeId = idx
        '  row_range.Name = "s" & CStr(row_range.RangeId)
        '  tbl_ranges.Rows.Add(row_range)
        'Next

        'Dim kkk As Integer
        'kkk = tbl_agency.Rows.Count + tbl_ranges.Rows.Count
        'For ag = 0 To tbl_agency.Rows.Count - 1
        '  row_agency = tbl_agency.Rows.Item(ag)
        '  For idx = 0 To tbl_ranges.Rows.Count - 1
        '    row_range = tbl_ranges.Rows.Item(idx)

        '    row_value = tbl_values.NewRow()
        '    row_value.InstanceId = row_agency.InstanceId
        '    row_value.RangeId = row_range.RangeId
        '    row_value.Value_1 = 20 + (1 - Math.Exp(-idx / kkk)) * (10 * (ag + 1)) * Math.Sin(3.0 * (ag + 1) * Math.PI * (idx + ag) / (kkk + 1))
        '    row_value.Value_2 = 50
        '    row_value.Value_3 = 20
        '    tbl_values.Rows.Add(row_value)
        '  Next
        'Next

        'Dim Sales(,) As Object

        'If tbl_agency.Rows.Count <= 0 Then
        '  Exit Sub
        'End If

        'If tbl_ranges.Rows.Count <= 0 Then
        '  Exit Sub
        'End If

        'ReDim Sales(tbl_agency.Rows.Count, tbl_ranges.Rows.Count)


        'For idx = 0 To tbl_values.Rows.Count - 1
        '  row_value = tbl_values.Rows.Item(idx)

        '  Sales(row_value.InstanceId, row_value.RangeId) = row_value.Value_1

        'Next

        'For ag = 0 To tbl_agency.Rows.Count - 1
        '  row_agency = tbl_agency.Rows.Item(ag)
        '  Sales(ag + 1, 0) = row_agency.Name
        'Next

        'For idx = 0 To tbl_ranges.Rows.Count - 1
        '  row_range = tbl_ranges.Rows.Item(idx)
        '  Sales(0, idx + 1) = row_range.Name
        'Next

        Dim nSem As Integer


        nSem = 12

        Dim Sales(nSem, 3) As Object

        Sales(0, 0) = "Sales"

        Sales(0, 0) = 4 '"Sales"
        Sales(0, 1) = 1 '"Sales"
        'Sales(0, 2) = 2 '"Sales"
        'Sales(0, 3) = 3 '"Sales"
        'Sales(0, 4) = 10 '"Sales"

        Sales(1, 0) = 10 '"Sales"
        Sales(1, 1) = 11 '"Sales"
        'Sales(1, 2) = 12 '"Sales"
        'Sales(1, 3) = 13 '"Sales"
        'Sales(1, 4) = 20 '"Sales"

        Sales(2, 0) = 13 '"Sales"
        Sales(2, 1) = 11 '"Sales"
        'Sales(2, 2) = 12 '"Sales"
        'Sales(2, 3) = 10 '"Sales"
        'Sales(2, 4) = 30 '"Sales"

        Sales(3, 0) = 5 '"Sales"
        Sales(3, 1) = 8 '"Sales"
        'Sales(3, 2) = 6 '"Sales"
        'Sales(3, 3) = 9 '"Sales"
        'Sales(3, 4) = 40 '"Sales"

        Dim idx As Integer
        For idx = 0 To nSem
            Sales(idx, 0) = 150000 + 75000 * Math.Cos(idx)
            Sales(idx, 1) = 200000 + 50000 * Math.Sin(idx)
            'Sales(idx, 2) = 10 - idx
            'Sales(idx, 3) = Math.Sqrt(idx)
            'Sales(idx, 4) = 10 * Math.Abs(Math.Sin(idx))
        Next

        Sales(0, 2) = Sales(0, 0)
        Sales(0, 3) = Sales(0, 1)
        For idx = 1 To nSem
            Sales(idx, 2) = Sales(idx - 1, 2) + Sales(idx, 0)
            Sales(idx, 3) = Sales(idx - 1, 3) + Sales(idx, 1)
        Next


        Static Dim kkkkk As Integer = 0
        kkkkk = kkkkk + 1
        If kkkkk Mod 2 = 0 Then
            Chart.chartType = MSChart20Lib.VtChChartType.VtChChartType2dLine

        Else
            Chart.chartType = MSChart20Lib.VtChChartType.VtChChartType2dBar

        End If
        Chart.ChartData = Sales

        Chart.Repaint = True




        'Dim Sales(,) As Object = New Object(,) _
        '                   {{"Company", "Company A", "Company B"}, _
        '                   {"June", 20, 10}, _
        '                   {"July", 10, 5}, _
        '                   {"August", 30, 15}, _
        '                   {"September", 14, 7}}
        'Chart.ChartData = Sales
        'Chart.DataSource = Sales
        'Dim row As Integer
        'Dim column As Integer
        'Dim labelIndex As Integer
        'With Chart.DataGrid
        '  ' Set Chart parameters using methods.
        '  .RowLabelCount = 2
        '  .ColumnLabelCount = 2
        '  .RowCount = 6
        '  .ColumnCount = 6
        '  .SetSize(.RowLabelCount, .ColumnLabelCount, _
        '  .RowCount, .ColumnCount)
        '  ' Randomly fill in the data.
        '  .RandomDataFill()
        '  ' Then assign labels to second Level.
        '  labelIndex = 2
        '  column = 1
        '  .ColumnLabel(column, labelIndex) = "Product 1"
        '  column = 4
        '  .ColumnLabel(column, labelIndex) = "Product 2"
        '  row = 1
        '  .RowLabel(row, labelIndex) = "1994"
        '  row = 4
        '  .RowLabel(row, labelIndex) = "1995"
        'End With







        'Add titles to the axes.
        With Me.Chart.Plot
            .Axis(MSChart20Lib.VtChAxisId.VtChAxisIdX).AxisTitle.Text = "Semana"
            .Axis(MSChart20Lib.VtChAxisId.VtChAxisIdY).AxisTitle.Text = "Millones de �"
        End With

        ''Set custom colors for the bars.
        'With Me.Chart.Plot
        '  'Yellow for Company A
        '  ' -1 selects all the datapoints.
        '  .SeriesCollection(1).DataPoints(-1).Brush.FillColor.Set(250, 250, 0)
        '  'Purple for Company B
        '  .SeriesCollection(2).DataPoints(-1).Brush.FillColor.Set(200, 50, 200)
        'End With

        'With Me.Chart.Plot
        '  '.SeriesCollection(1).Position.StackOrder = 1
        '  '.SeriesCollection(1).Position.Excluded = False
        '  '.SeriesCollection(1).Position.Hidden = True
        '  .SeriesCollection(0).Position.StackOrder = 0
        '  .SeriesCollection(1).Position.StackOrder = 1
        '  .SeriesCollection(2).Position.StackOrder = 3
        '  .SeriesCollection(3).Position.StackOrder = 3


        'End With

        Dim ssi As Integer
        Dim ss As MSChart20Lib.Series

        ssi = 1
        For Each ss In Me.Chart.Plot.SeriesCollection
            ss.LegendText = "Series" & CStr(ssi)
            ss.ShowGuideLine(MSChart20Lib.VtChAxisId.VtChAxisIdY) = True



            With ss.StatLine
                .VtColor.Set(128, 128, 255)
                .Flag = MSChart20Lib.VtChStats.VtChStatsMean
                .Style(MSChart20Lib.VtChStats.VtChStatsMean) = MSChart20Lib.VtPenStyle.VtPenStyleDashDotDot
                .Width = 2
            End With


            'ss.Position.StackOrder = 0
            ssi = ssi + 1
            If ssi = 2 Then
                'ss.ShowLine = False
                'ss.StatLine = ss.Style(MSChart20Lib.VtChStats.VtChStatsMean)
            End If
        Next

    End Sub

    ' PURPOSE: Sets the Title
    '
    '  PARAMS:
    '     - INPUT:
    '     - OUTPUT:
    '
    ' RETURNS:
    '
    Public Sub SetTitle(ByVal Title As String)
        With Me.Chart
            ' Title
            .Title.Location.LocationType = MSChart20Lib.VtChLocationType.VtChLocationTypeTop
            .Title.Text = Title
            ' Legend
            .Legend.Location.LocationType = MSChart20Lib.VtChLocationType.VtChLocationTypeTop
            .Legend.Location.Visible = True

        End With
    End Sub

    ' PURPOSE: Sets the Data
    '
    '  PARAMS:
    '     - INPUT:
    '     - OUTPUT:
    '
    ' RETURNS:
    '
    Public Sub SetData(ByRef InData(,) As Double)

        Me.Chart.ChartData = InData

    End Sub

    ' PURPOSE: Sets the Titles to the Axes
    '
    '  PARAMS:
    '     - INPUT:
    '     - OUTPUT:
    '
    ' RETURNS:
    '
    Public Sub SetTitleToAxes(ByVal AxisXTitle As String, _
                              ByVal AxisYTitle As String)
        With Me.Chart.Plot
            .Axis(MSChart20Lib.VtChAxisId.VtChAxisIdX).AxisTitle.VtFont.Name = "Verdana"
            .Axis(MSChart20Lib.VtChAxisId.VtChAxisIdX).AxisTitle.Text = AxisXTitle
            .Axis(MSChart20Lib.VtChAxisId.VtChAxisIdY).AxisTitle.Text = AxisYTitle
            .Axis(MSChart20Lib.VtChAxisId.VtChAxisIdY).AxisTitle.VtFont.Name = "Verdana"
            '.Axis(MSChart20Lib.VtChAxisId.VtChAxisIdX).AxisTitle.VtFont.Size = 7

        End With
    End Sub

    ' PURPOSE: Plots series as 2-D Lines
    '
    '  PARAMS:
    '     - INPUT:
    '     - OUTPUT:
    '
    ' RETURNS:
    '
    Public Sub SetGraphModeTo2DLines()
        'XVV Variable not use Dim series As MSChart20Lib.Series

        Me.Chart.chartType = MSChart20Lib.VtChChartType.VtChChartType2dLine
        Call Me.SetShowMarkers(True)
        Call SetAxisXDivisions()
        Call SetAxisYDivisions()

        Call SetMaximum()

    End Sub

    ' PURPOSE: Plots series as 2-D Bars
    '
    '  PARAMS:
    '     - INPUT:
    '     - OUTPUT:
    '
    ' RETURNS:
    '
    Public Sub SetGraphModeTo2DBars()
        Me.Chart.chartType = MSChart20Lib.VtChChartType.VtChChartType2dBar
        Me.Chart.Plot.BarGap = 0
        Call Me.SetShowMarkers(False)
        Call SetAxisXDivisions()
        Call SetAxisYDivisions()
        'Call SetRowLabels(Nothing)
        Call SetMaximum()

    End Sub

    ' PURPOSE: Plots series as 2-D Lines
    '
    '  PARAMS:
    '     - INPUT:
    '     - OUTPUT:
    '
    ' RETURNS:
    '
    Public Sub SetShowMarkers(ByVal ShowMarker As Boolean)
        Dim series As MSChart20Lib.Series


        ShowMarker = False

        For Each series In Chart.Plot.SeriesCollection
            series.SeriesMarker.Auto = False
            series.SeriesMarker.Show = ShowMarker
            series.ShowLine = True
            If ShowMarker Then
                series.DataPoints(-1).Marker.Style = MSChart20Lib.VtMarkerStyle.VtMarkerStyleFilledCircle
                series.DataPoints(-1).Marker.Size = 60
                series.DataPoints(-1).Marker.FillColor.Set(series.Pen.VtColor.Red, series.Pen.VtColor.Green, series.Pen.VtColor.Blue)
                'series.DataPoints(-1).Marker.Pen.VtColor.Set(series.Pen.VtColor.Red, series.Pen.VtColor.Green, series.Pen.VtColor.Blue)
                series.DataPoints(-1).Marker.Pen.VtColor.Set(0, 0, 0)
                'series.DataPoints(-1).Marker.FillColor.Set(0, 0, 0)
                series.DataPoints(-1).Marker.Pen.Width = 1
            Else
                series.DataPoints(-1).Marker.Pen.Style = MSChart20Lib.VtPenStyle.VtPenStyleNull
            End If
        Next

        ' Use manual scale to display y axis (value axis).

        Chart.Plot.Axis(MSChart20Lib.VtChAxisId.VtChAxisIdX).Tick.Length = 1
        Chart.Plot.Axis(MSChart20Lib.VtChAxisId.VtChAxisIdX).Tick.Style = MSChart20Lib.VtChAxisTickStyle.VtChAxisTickStyleInside


        With Chart.Plot.Axis(MSChart20Lib.VtChAxisId.VtChAxisIdX).ValueScale
            .Auto = False
            .MajorDivision = 10
            .MinorDivision = 4
        End With

        With Chart.Plot.Axis(MSChart20Lib.VtChAxisId.VtChAxisIdX).CategoryScale
            .Auto = False
            .DivisionsPerLabel = 1
            .DivisionsPerTick = 1
        End With



    End Sub

    Public Sub SetAxisYDivisions(Optional ByVal MajorDivisions As Integer = 4, _
                                 Optional ByVal MinorDivisions As Integer = 4)

        'XVV Variable not use im maximum As Double

        With Chart.Plot.Axis(MSChart20Lib.VtChAxisId.VtChAxisIdY2).AxisGrid
            .MajorPen.Style = MSChart20Lib.VtPenStyle.VtPenStyleNull
            .MinorPen.Style = MSChart20Lib.VtPenStyle.VtPenStyleNull
        End With

        With Chart.Plot.Axis(MSChart20Lib.VtChAxisId.VtChAxisIdY).ValueScale
            .MajorDivision = MajorDivisions
            .MinorDivision = MinorDivisions
        End With

        With Chart.Plot.Axis(MSChart20Lib.VtChAxisId.VtChAxisIdY).AxisGrid
            .MajorPen.VtColor.Set(192, 192, 192) ' Black
            .MajorPen.Width = 2
            .MinorPen.VtColor.Set(192, 192, 192) ' Grey
            .MinorPen.Width = 2
            .MajorPen.Style = MSChart20Lib.VtPenStyle.VtPenStyleSolid
            .MinorPen.Style = MSChart20Lib.VtPenStyle.VtPenStyleDotted
        End With

    End Sub

    Public Sub SetAxisXDivisions(Optional ByVal MajorDivisions As Integer = 4, _
                                 Optional ByVal MinorDivisions As Integer = 4)

        With Chart.Plot.Axis(MSChart20Lib.VtChAxisId.VtChAxisIdX).ValueScale
            .Auto = False
            .MajorDivision = MajorDivisions
            .MinorDivision = MinorDivisions
        End With

        With Chart.Plot.Axis(MSChart20Lib.VtChAxisId.VtChAxisIdX).AxisGrid
            .MajorPen.VtColor.Set(240, 240, 240) ' Black
            .MajorPen.Width = 2
            .MinorPen.VtColor.Set(240, 240, 240) ' Grey
            .MinorPen.Width = 2
            .MajorPen.Style = MSChart20Lib.VtPenStyle.VtPenStyleSolid
            .MinorPen.Style = MSChart20Lib.VtPenStyle.VtPenStyleDotted
        End With

    End Sub

    ' PURPOSE: Plots series as 2-D Lines
    '
    '  PARAMS:
    '     - INPUT:
    '     - OUTPUT:
    '
    ' RETURNS:
    '
    Public Sub SetLegends(ByVal LegendText() As String)
        Dim series As MSChart20Lib.Series
        Dim index As Integer

        index = 0

        For Each series In Chart.Plot.SeriesCollection
            series.LegendText = LegendText(index)
            index = index + 1
        Next
    End Sub

    Public Sub SetRowLabels(ByVal RowLabel() As String, Optional ByVal RowLabel2() As String = Nothing)

        Dim row_idx As Integer


        With Chart.DataGrid
            For row_idx = 1 To .RowCount
                .RowLabel(row_idx, 1) = RowLabel(row_idx - 1)
            Next
        End With

        If Not IsNothing(RowLabel2) Then
            If Me.Chart.DataGrid.RowLabelCount < 2 Then
                Me.Chart.DataGrid.InsertRowLabels(2, 1)
            End If

            With Chart.DataGrid
                For row_idx = 1 To .RowCount
                    .RowLabel(row_idx, 2) = RowLabel2(row_idx - 1)
                Next
            End With
        Else
            Me.Chart.DataGrid.DeleteRowLabels(2, 1)
        End If

    End Sub

    Public Sub SetColLabels(ByVal ColLabel() As String)

        Dim row_idx As Integer

        With Chart.DataGrid
            For row_idx = 1 To .ColumnCount
                .ColumnLabel(row_idx, 1) = ColLabel(row_idx - 1)
            Next
        End With

    End Sub

    Private Sub SetMaximum()
        Dim maximum As Double
        Dim int_max As Integer

        Dim row_idx As Integer
        Dim jj As Integer
        Dim value As Double
        Dim isnull As Short

        maximum = 0.0
        With Me.Chart.DataGrid
            For jj = 1 To .ColumnCount
                For row_idx = 1 To .RowCount
                    .GetData(row_idx, jj, value, isnull)
                    If value > maximum Then
                        maximum = value
                    End If
                Next
            Next


        End With

        int_max = CInt(maximum * 1.05)
        int_max = (int_max \ 10) * 10
        int_max = int_max + 1
        While int_max Mod 10 <> 0
            int_max = int_max + 1
        End While
        maximum = int_max

        With Chart.Plot.Axis(MSChart20Lib.VtChAxisId.VtChAxisIdY).ValueScale
            .Maximum = maximum
            .Minimum = 0.0
        End With


    End Sub

End Class
