'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_price_levels.vb  
'
' DESCRIPTION:   game price levelsselector control
'
' AUTHOR:        Sergio Calleja
'
' CREATION DATE: 11-APR-2003
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 11-APR-2003  SCM    Initial version
'--------------------------------------------------------------------

Imports System.Data.OleDb
Imports GUI_CommonOperations
Imports GUI_CommonMisc

Public Class uc_price_levels
  Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'UserControl overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents dg_price_levels As GUI_Controls.uc_grid
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.dg_price_levels = New GUI_Controls.uc_grid()
    Me.SuspendLayout()
    '
    'dg_price_levels
    '
    Me.dg_price_levels.CurrentCol = -1
    Me.dg_price_levels.CurrentRow = -1
    Me.dg_price_levels.Name = "dg_price_levels"
    Me.dg_price_levels.PanelRightVisible = False
    Me.dg_price_levels.Redraw = True
    Me.dg_price_levels.Size = New System.Drawing.Size(232, 88)
    Me.dg_price_levels.TabIndex = 0
    '
    'uc_price_levels
    '
    Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.dg_price_levels})
    Me.Name = "uc_price_levels"
    Me.Size = New System.Drawing.Size(232, 88)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Members "

  Private m_game_id As Integer
  Private max_price_levels As Integer = GUI_CommonMisc.GetMaxPriceLevels()

#End Region

#Region " Constants "

  Private Const MAIN_MODULE_NAME As String = "uc_price_levels"

  Private Const GRID_COLUMNS As Integer = 4
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const GRID_COL_PRICE_LEVEL_ID As Integer = 0
  Private Const GRID_COL_CHECKBOX As Integer = 1
  Private Const GRID_COL_DESCRIPTION As Integer = 2
  Private Const GRID_COL_TICKET_PRICE As Integer = 3

  Private Const MAX_DECIMAL_LEN As Integer = 2
  Private Const MAX_NUMBER As Double = 99999999.99

  Public Const MAX_DESCRIPTION_LEN As Integer = 40
  Private Const MAX_PL_TICKET_PRICE_LEN As Integer = 11
  Private Const MAX_GC_NUM_BOOKS_LEN As Integer = 5
  Private Const MAX_GC_NUM_TICKETS_LEN As Integer = 3
  Private Const MAX_ST_MIN_BOOKS_LEN As Integer = 6


#End Region

#Region " Properties "

  Public Property GameId() As Integer
    Get
      Return m_game_id
    End Get
    Set(ByVal Value As Integer)
      m_game_id = Value
    End Set
  End Property

  Public ReadOnly Property GetMaxPriceLevels() As Integer
    Get
      Return max_price_levels
    End Get
  End Property

  ' PURPOSE: Returns the number of selected price levels within the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - number of selected price levels

  Public ReadOnly Property GetNumberSelectedPrices() As Integer
    Get
      Dim idx_row As Integer
      Dim number_selected As Integer

      number_selected = 0

      For idx_row = 0 To Me.dg_price_levels.NumRows - 1
        If Me.dg_price_levels.Cell(idx_row, GRID_COL_CHECKBOX).Value = GUI_Controls.uc_grid.GRID_CHK_CHECKED Then
          number_selected = number_selected + 1
        End If
      Next

      Return number_selected
    End Get
  End Property

  Public ReadOnly Property GetSelectedPricesLevelName() As String
    Get
      Dim num_selected As Integer
      Dim idx_row As Integer
      Dim aux_str As String

      aux_str = ""
      num_selected = Me.GetNumberSelectedPrices()
      If num_selected = 0 Then
        aux_str = GLB_NLS_GUI_CONTROLS.GetString(256)
      Else
        For idx_row = 0 To dg_price_levels.NumRows - 1
          If dg_price_levels.Cell(idx_row, GRID_COL_CHECKBOX).Value = GUI_Controls.uc_grid.GRID_CHK_CHECKED Then
            If aux_str = "" Then
              aux_str = dg_price_levels.Cell(idx_row, GRID_COL_DESCRIPTION).Value
            Else
              aux_str = aux_str & ", " & dg_price_levels.Cell(idx_row, GRID_COL_DESCRIPTION).Value

            End If
          End If
        Next
      End If
      Return aux_str
    End Get

  End Property

  Public ReadOnly Property IsSelected(ByVal Idx As Integer) As Boolean
    Get
      If Idx <= Me.dg_price_levels.NumRows - 1 Then
        Return (Me.dg_price_levels.Cell(Idx, GRID_COL_CHECKBOX).Value = uc_grid.GRID_CHK_CHECKED)
      Else
        Return False
      End If
    End Get
  End Property

  Public ReadOnly Property NumRows() As Integer
    Get
      Return Me.dg_price_levels.NumRows
    End Get
  End Property

  Public ReadOnly Property GetPriceLevelId(ByVal Idx As Integer) As String
    Get
      Return Me.dg_price_levels.Cell(Idx, GRID_COL_PRICE_LEVEL_ID).Value
    End Get
  End Property

  Public ReadOnly Property GetPriceLevelName(ByVal Idx As Integer) As String
    Get
      Return Me.dg_price_levels.Cell(Idx, GRID_COL_DESCRIPTION).Value
    End Get
  End Property

  Public ReadOnly Property GetPriceLevelAmount(ByVal Idx As Integer) As String
    Get
      Return Me.dg_price_levels.Cell(Idx, GRID_COL_TICKET_PRICE).Value
    End Get
  End Property

#End Region

  Public Sub Init()

    Me.GameId = -1

    Call Me.dg_price_levels.Init(GRID_COLUMNS, GRID_HEADER_ROWS, True)

    With Me.dg_price_levels

      ' Hiden colums
      ' GRID_COL_PRICE_LEVEL_ID
      .Column(GRID_COL_PRICE_LEVEL_ID).Header.Text = ""
      .Column(GRID_COL_PRICE_LEVEL_ID).WidthFixed = 0


      ' GRID_COL_CHECKBOX
      .Column(GRID_COL_CHECKBOX).Header(0).Text = ""
      .Column(GRID_COL_CHECKBOX).Header(1).Text = ""
      .Column(GRID_COL_CHECKBOX).WidthFixed = 400
      .Column(GRID_COL_CHECKBOX).Fixed = True
      .Column(GRID_COL_CHECKBOX).Editable = True
      .Column(GRID_COL_CHECKBOX).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_COL_DESCRIPTION).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(311)
      .Column(GRID_COL_DESCRIPTION).Header(0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COL_DESCRIPTION).Header(1).Text = GLB_NLS_GUI_CONTROLS.GetString(312)
      .Column(GRID_COL_DESCRIPTION).WidthFixed = 1480
      .Column(GRID_COL_DESCRIPTION).Fixed = True
      .Column(GRID_COL_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' GRID_COL_TICKET_PRICE
      .Column(GRID_COL_TICKET_PRICE).Header(0).Text = GLB_NLS_GUI_CONTROLS.GetString(311)
      .Column(GRID_COL_TICKET_PRICE).Header(0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COL_TICKET_PRICE).Header(1).Text = GLB_NLS_GUI_CONTROLS.GetString(313)
      .Column(GRID_COL_TICKET_PRICE).WidthFixed = 1200
      .Column(GRID_COL_TICKET_PRICE).Fixed = True
      .Column(GRID_COL_TICKET_PRICE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub

#Region " Events "

#End Region

#Region " Public "

  Public Sub UpdatePriceLevels(ByVal GameId As Integer)
    Dim row_index As Integer
    'XVV Variable not use Dim string_sql As String
    'XVV 13/04/2007 
    'Add Nothing value to variable declaration 
    Dim dt As DataTable = Nothing
    Dim row As DataRow

    Me.GameId = GameId

    Try
      Me.dg_price_levels.Clear()

      dt = GetPriceLevels(GameId)

      row_index = 0
      For Each row In dt.Rows
        Me.dg_price_levels.AddRow()
        Me.dg_price_levels.Cell(row_index, GRID_COL_CHECKBOX).Value = uc_grid.GRID_CHK_UNCHECKED
        Me.dg_price_levels.Cell(row_index, GRID_COL_PRICE_LEVEL_ID).Value = row.Item("GPL_PRICE_LEVEL_ID")
        Me.dg_price_levels.Cell(row_index, GRID_COL_TICKET_PRICE).Value = IIf(row.Item("GPL_TICKET_PRICE") <> 0, _
                                                                              GUI_FormatCurrency(row.Item("GPL_TICKET_PRICE"), , , True), _
                                                                              GLB_NLS_GUI_CONTROLS.GetString(256))
        Me.dg_price_levels.Cell(row_index, GRID_COL_DESCRIPTION).Value = row.Item("GPL_DESCRIPTION")

        row_index = row_index + 1
      Next

    Catch exception As Exception
      Call Trace.WriteLine(exception.ToString())
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, MAIN_MODULE_NAME, "UpdatePriceLevels", exception.Message)

    Finally
      If Not IsNothing(dt) Then
        dt.Dispose()
        dt = Nothing
      End If
    End Try

  End Sub

  Public Sub Clear()
    Me.dg_price_levels.Clear()

  End Sub

  Public Sub SelectAllPriceLevels()
    Dim idx As Integer

    For idx = 0 To Me.GetMaxPriceLevels - 1
      Me.dg_price_levels.Cell(idx, GRID_COL_CHECKBOX).Value = uc_grid.GRID_CHK_CHECKED

    Next

  End Sub

#End Region

End Class
