'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_text_field.vb
' DESCRIPTION:   Text Field User Control
' AUTHOR:        ---
' CREATION DATE: xx-xxx-xxxx
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' xx-xxx-xxxx  ---    Initial version
' 16-SEP-2002  JSV    Add LabelAlign Property and ENUM_ALIGN constants. Constructor set by default left alignment.
'-------------------------------------------------------------------

Imports System.ComponentModel
Imports GUI_CommonMisc

Public Class uc_text_field
  Inherits uc_base

#Region " Constants "

  Public Enum ENUM_ALIGN
    ALIGN_LEFT = System.Drawing.ContentAlignment.MiddleLeft
    ALIGN_CENTER = System.Drawing.ContentAlignment.MiddleCenter
    ALIGN_RIGTH = System.Drawing.ContentAlignment.MiddleRight
  End Enum

#End Region

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call
    Me.Width = 200
    Me.Height = 30
    Me.LabelAlign = ENUM_ALIGN.ALIGN_LEFT

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Private WithEvents lbl_value As System.Windows.Forms.Label
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.lbl_value = New System.Windows.Forms.Label()
    Me.panel_control.SuspendLayout()
    Me.SuspendLayout()
    '
    'lbl_read_only
    '
    Me.lbl_read_only.Dock = System.Windows.Forms.DockStyle.None
    Me.lbl_read_only.Location = New System.Drawing.Point(82, 2)
    Me.lbl_read_only.Size = New System.Drawing.Size(116, 20)
    '
    'panel_control
    '
    Me.panel_control.Controls.AddRange(New System.Windows.Forms.Control() {Me.lbl_value})
    Me.panel_control.Location = New System.Drawing.Point(80, 0)
    Me.panel_control.Size = New System.Drawing.Size(170, 20)
    '
    'lbl_value
    '
    Me.lbl_value.ForeColor = System.Drawing.Color.Blue
    Me.lbl_value.Location = New System.Drawing.Point(82, 2)
    Me.lbl_value.Name = "lbl_value"
    Me.lbl_value.Size = New System.Drawing.Size(116, 20)
    Me.lbl_value.TabIndex = 0
    Me.lbl_value.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'uc_text_field
    '
    Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.lbl_read_only, Me.panel_control})
    Me.IsReadOnly = True
    Me.Name = "uc_text_field"
    Me.Size = New System.Drawing.Size(200, 24)
    Me.SufixTextVisible = True
    Me.panel_control.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region


#Region "Properties"
  Public Property LabelForeColor() As System.Drawing.Color
    Get
      Return Me.lbl_value.ForeColor
    End Get
    Set(ByVal NewValue As System.Drawing.Color)
      Me.lbl_value.ForeColor = NewValue
      Me.lbl_value.Refresh()
    End Set
  End Property

  Public Property LabelAlign() As ENUM_ALIGN
    Get
      Return Me.lbl_value.TextAlign
    End Get
    Set(ByVal Value As ENUM_ALIGN)
      Me.lbl_value.TextAlign = Value
      Me.lbl_value.Refresh()
    End Set
  End Property

#End Region

#Region " Value "

  <Category("EntryField"), _
   Description("Value"), _
   DefaultValue("")> _
   Public Property Value() As String
    Get
      Return lbl_value.Text
    End Get

    Set(ByVal NewValue As String)
      lbl_value.Text = NewValue
    End Set
  End Property

#End Region

  <Category("EntryField"), _
   Description("Text"), _
   DefaultValue("")> _
Public Overrides Property Text() As String
    Get
      Return MyBase.Text
    End Get
    Set(ByVal Value As String)
      MyBase.Text = Value & ":"
    End Set
  End Property

  Public Overrides Property IsReadOnly() As Boolean
    Get
      Return True
    End Get
    Set(ByVal Value As Boolean)
      Exit Property
    End Set
  End Property
End Class
