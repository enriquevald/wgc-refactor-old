<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_data_import
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.btn_open_file_dialog = New System.Windows.Forms.Button
    Me.ef_file_path = New GUI_Controls.uc_entry_field
    Me.SuspendLayout()
    '
    'btn_open_file_dialog
    '
    Me.btn_open_file_dialog.Location = New System.Drawing.Point(438, 3)
    Me.btn_open_file_dialog.Name = "btn_open_file_dialog"
    Me.btn_open_file_dialog.Size = New System.Drawing.Size(27, 24)
    Me.btn_open_file_dialog.TabIndex = 3
    Me.btn_open_file_dialog.Text = "..."
    Me.btn_open_file_dialog.UseVisualStyleBackColor = True
    '
    'ef_file_path
    '
    Me.ef_file_path.DoubleValue = 0
    Me.ef_file_path.IntegerValue = 0
    Me.ef_file_path.IsReadOnly = False
    Me.ef_file_path.Location = New System.Drawing.Point(3, 3)
    Me.ef_file_path.Name = "ef_file_path"
    Me.ef_file_path.Size = New System.Drawing.Size(429, 24)
    Me.ef_file_path.SufixText = "Sufix Text"
    Me.ef_file_path.SufixTextVisible = True
    Me.ef_file_path.TabIndex = 2
    Me.ef_file_path.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_file_path.TextValue = ""
    Me.ef_file_path.TextWidth = 50
    Me.ef_file_path.Value = ""
    '
    'uc_data_import
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.Controls.Add(Me.ef_file_path)
    Me.Controls.Add(Me.btn_open_file_dialog)
    Me.Name = "uc_data_import"
    Me.Size = New System.Drawing.Size(468, 30)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_file_path As GUI_Controls.uc_entry_field
  Friend WithEvents btn_open_file_dialog As System.Windows.Forms.Button

End Class
