﻿'-------------------------------------------------------------------
' Copyright © 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_multi_currency_site_sel.vb  
'
' DESCRIPTION:   Manage currency in site
'
' AUTHOR:        Rafael Arjona
'
' CREATION DATE: 05-JUL-2016
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 05-JUL-2016  RAB    Initial version
' 26-SEP-2017  EOR    PBI 29907: WIGOS-4821 Table games analysis report
'--------------------------------------------------------------------
Imports WSI.Common

Public Class uc_multi_currency_site_sel

#Region "Members"
  Private m_selected_currency As String
  Private m_groubox_text As String
  Private m_open_mode As OPEN_MODE
  Private m_selected_option As MULTICURRENCY_OPTION
  Private m_height_control As Integer
  Private m_width_control As Integer
#End Region

#Region "Constants"
  Private Const HEIGHT_WITH_BY_CURRENCY As Integer = 55
  Private Const HEIGHT_WITH_BY_TOTAL As Integer = 55
  Private Const INITIAL_HEIGHT_CONTROL As Integer = 75
  Private Const INITIAL_WIDTH_CONTROL As Integer = 170
#End Region

#Region "Enum"
  ''' <summary>
  ''' Open mode to user control
  ''' </summary>
  ''' <remarks>
  ''' OnlyCurrency = 0
  ''' AllControls = 1
  ''' </remarks>
  Public Enum OPEN_MODE
    OnlyCurrency = 0 'Only visible control radio button 'by currency'
    OnlyTotal = 1 'Only visible control radio button 'total to'
    AllControls = 2 'All controls visible
  End Enum

  ''' <summary>
  ''' MultiCurrency selected option from user control
  ''' </summary>
  ''' <remarks>
  ''' TotalToIsoCode = 0
  ''' ByCurrency = 1
  ''' </remarks>
  Public Enum MULTICURRENCY_OPTION
    TotalToIsoCode = 0
    ByCurrency = 1
  End Enum

#End Region

#Region "Public Properties"
  Public Property SelectedCurrency() As String
    Get
      ' If m_selected_currency is empty and TotalToIsoCode then return NationalCurrency
      If ((String.IsNullOrEmpty(m_selected_currency)) AndAlso (SelectedOption = MULTICURRENCY_OPTION.TotalToIsoCode)) Then
        m_selected_currency = CurrencyExchange.GetNationalCurrency()
      End If

      Return m_selected_currency
    End Get

    Set(ByVal value As String)
      m_selected_currency = value
    End Set
  End Property

  Public Property GroupBoxText() As String
    Get
      Return m_groubox_text
    End Get

    Set(ByVal value As String)
      m_groubox_text = value
    End Set
  End Property

  Public Property OpenModeControl() As OPEN_MODE
    Get
      Return m_open_mode
    End Get

    Set(ByVal value As OPEN_MODE)
      m_open_mode = value
    End Set
  End Property

  Public Property SelectedOption() As MULTICURRENCY_OPTION
    Get
      Return m_selected_option
    End Get

    Set(value As MULTICURRENCY_OPTION)
      m_selected_option = value
    End Set
  End Property

  Public Property HeightControl() As Integer
    Get
      Return m_height_control
    End Get

    Set(value As Integer)
      m_height_control = value
    End Set
  End Property

  Public Property WidthControl() As Integer
    Get
      Return m_width_control
    End Get

    Set(value As Integer)
      m_width_control = value
    End Set
  End Property
#End Region

#Region "Public Functions"

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()
  End Sub ' New

  ''' <summary>
  ''' Init routine of the control
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub Init()
    Call InitControls()
  End Sub ' Init
#End Region

#Region "Private Functions"
  Private Sub InitControls()
    Me.Visible = True

    m_selected_option = MULTICURRENCY_OPTION.TotalToIsoCode
    m_height_control = IIf(m_height_control = Nothing, INITIAL_HEIGHT_CONTROL, m_height_control)
    m_width_control = IIf(m_width_control = Nothing, INITIAL_WIDTH_CONTROL, m_width_control)

    Me.Height = m_height_control
    Me.gb_multi_currency_site.Height = m_height_control
    Me.Width = m_width_control
    Me.gb_multi_currency_site.Width = m_width_control

    Me.gb_multi_currency_site.Text = IIf(m_groubox_text = Nothing, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7382), m_groubox_text)
    Me.lbl_total_to_isocode.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7397, CurrencyExchange.GetNationalCurrency())
    Me.rb_total_to_isocode.Checked = True
    Me.lbl_by_currency.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7398)

    If (m_open_mode = OPEN_MODE.OnlyCurrency) Then
      ViewComponentsOnlyCurrency()
    End If

    If (m_open_mode = OPEN_MODE.OnlyTotal) Then
      ViewComponentsOnlyTotal()
    End If

    If (m_open_mode <> OPEN_MODE.OnlyTotal) Then
      FillCurrenciesCombo()
    End If

  End Sub ' InitControls

  ''' <summary>
  ''' Show components 'only currency'
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ViewComponentsOnlyCurrency()
    Me.tlp_multi_currency.ColumnStyles(0).Width = 0
    Me.tlp_multi_currency.RowStyles(0).Height = 0
    Me.tlp_multi_currency.RowStyles(1).Height = HEIGHT_WITH_BY_CURRENCY
    Me.rb_by_currency.Dock = Windows.Forms.DockStyle.None
    Me.rb_by_currency.Checked = True
    Me.rb_by_currency.Visible = False
    Me.lbl_total_to_isocode.Visible = False
    Me.lbl_by_currency.Visible = True

    Me.Height = HEIGHT_WITH_BY_CURRENCY
    Me.gb_multi_currency_site.Height = HEIGHT_WITH_BY_CURRENCY

    m_selected_option = MULTICURRENCY_OPTION.ByCurrency
  End Sub

  ''' <summary>
  ''' Show components 'only total'
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ViewComponentsOnlyTotal()
    Me.tlp_multi_currency.ColumnStyles(0).Width = 0
    Me.tlp_multi_currency.RowStyles(0).Height = HEIGHT_WITH_BY_TOTAL
    Me.tlp_multi_currency.RowStyles(1).Height = 0
    Me.rb_total_to_isocode.Dock = Windows.Forms.DockStyle.None
    Me.rb_total_to_isocode.Checked = True
    Me.rb_total_to_isocode.Visible = False
    Me.rb_by_currency.Visible = False
    Me.lbl_total_to_isocode.Visible = True
    Me.lbl_by_currency.Visible = False

    Me.Height = HEIGHT_WITH_BY_TOTAL
    Me.gb_multi_currency_site.Height = HEIGHT_WITH_BY_TOTAL

    m_selected_option = MULTICURRENCY_OPTION.TotalToIsoCode
  End Sub

  Private Sub SetModeToOnlyTotal()
    m_open_mode = OPEN_MODE.OnlyTotal

    ViewComponentsOnlyTotal()
  End Sub

  ''' <summary>
  ''' Returns if there only is a currency to show
  ''' </summary>
  ''' <param name="DtCurrenciesUsedInChips"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function HasOnlyACurrency(DtCurrenciesUsedInChips As DataTable) As Boolean
    Return ((DtCurrenciesUsedInChips Is Nothing) OrElse (DtCurrenciesUsedInChips.Rows.Count <= 1))
  End Function

  ''' <summary>
  ''' Get all currencies used in chips
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub FillCurrenciesCombo()
    Dim _dt_currencies_used_in_chips As DataTable
    Dim _currency_iso_code As String

    ' Get currencies used in chips
    _dt_currencies_used_in_chips = Nothing
    CurrencyExchange.ReadCurrenciesUsedInChips(False, _dt_currencies_used_in_chips)

    Me.cmb_multi_currency_site.Clear()

    If HasOnlyACurrency(_dt_currencies_used_in_chips) Then
      'If there is enabled only a currency (the national currency) 
      'Show only the total option
      SelectedCurrency = String.Empty
      SetModeToOnlyTotal()

      Return
    End If

    For Each _data_row As DataRow In _dt_currencies_used_in_chips.Rows
      'Add currency to combo
      _currency_iso_code = _data_row("ISO_CODE")

      Me.cmb_multi_currency_site.Add(_currency_iso_code)
    Next

    SelectedCurrency = CurrencyExchange.GetNationalCurrency()

  End Sub 'FillCurrenciesCombo

#End Region

#Region "Events"
  Private Sub cmb_multi_currency_site_ValueChangedEvent() Handles cmb_multi_currency_site.ValueChangedEvent
    SelectedCurrency = cmb_multi_currency_site.TextValue
  End Sub

  Private Sub radio_button_CheckedChanged()
    If (Me.rb_total_to_isocode.Checked) Then
      m_selected_option = MULTICURRENCY_OPTION.TotalToIsoCode
      Me.cmb_multi_currency_site.Enabled = False
      SelectedCurrency = String.Empty
    Else
      m_selected_option = MULTICURRENCY_OPTION.ByCurrency
      Me.cmb_multi_currency_site.Enabled = True
      SelectedCurrency = cmb_multi_currency_site.TextValue
    End If
  End Sub

  Private Sub rb_by_currency_CheckedChanged(sender As Object, e As EventArgs) Handles rb_by_currency.CheckedChanged
    Call radio_button_CheckedChanged()
  End Sub

  Private Sub rb_total_to_isocode_CheckedChanged(sender As Object, e As EventArgs) Handles rb_total_to_isocode.CheckedChanged
    Call radio_button_CheckedChanged()
  End Sub
#End Region

  Private Sub lbl_by_currency_Click(sender As Object, e As EventArgs) Handles lbl_by_currency.Click
    'When cick label activate radiobutton
    If ((Not rb_by_currency.Checked) And (rb_by_currency.Visible)) Then
      rb_by_currency.Checked = True
    End If
  End Sub

  Private Sub lbl_total_to_isocode_Click(sender As Object, e As EventArgs) Handles lbl_total_to_isocode.Click
    'When cick label activate radiobutton
    If ((Not rb_total_to_isocode.Checked) And (rb_total_to_isocode.Visible)) Then
      rb_total_to_isocode.Checked = True
    End If
  End Sub
End Class