<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_checked_list_selector
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_selected_items = New System.Windows.Forms.GroupBox
    Me.btn_clear = New System.Windows.Forms.Button
    Me.btn_move_up = New System.Windows.Forms.Button
    Me.btn_move_down = New System.Windows.Forms.Button
    Me.dg_selected_list = New GUI_Controls.uc_grid
    Me.btn_add_element = New System.Windows.Forms.Button
    Me.btn_remove_element = New System.Windows.Forms.Button
    Me.uc_check_list = New GUI_Controls.uc_checked_list
    Me.gb_selected_items.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_selected_items
    '
    Me.gb_selected_items.Controls.Add(Me.btn_clear)
    Me.gb_selected_items.Controls.Add(Me.btn_move_up)
    Me.gb_selected_items.Controls.Add(Me.btn_move_down)
    Me.gb_selected_items.Controls.Add(Me.dg_selected_list)
    Me.gb_selected_items.Location = New System.Drawing.Point(384, 5)
    Me.gb_selected_items.Name = "gb_selected_items"
    Me.gb_selected_items.Size = New System.Drawing.Size(405, 239)
    Me.gb_selected_items.TabIndex = 4
    Me.gb_selected_items.TabStop = False
    Me.gb_selected_items.Text = "xSelectedItems"
    '
    'btn_clear
    '
    Me.btn_clear.Location = New System.Drawing.Point(10, 203)
    Me.btn_clear.Name = "btn_clear"
    Me.btn_clear.Size = New System.Drawing.Size(110, 28)
    Me.btn_clear.TabIndex = 9
    Me.btn_clear.Text = "xClear"
    Me.btn_clear.UseVisualStyleBackColor = True
    '
    'btn_move_up
    '
    Me.btn_move_up.Location = New System.Drawing.Point(365, 19)
    Me.btn_move_up.Name = "btn_move_up"
    Me.btn_move_up.Size = New System.Drawing.Size(35, 23)
    Me.btn_move_up.TabIndex = 5
    Me.btn_move_up.Text = "/\"
    Me.btn_move_up.UseVisualStyleBackColor = True
    '
    'btn_move_down
    '
    Me.btn_move_down.Location = New System.Drawing.Point(365, 48)
    Me.btn_move_down.Name = "btn_move_down"
    Me.btn_move_down.Size = New System.Drawing.Size(35, 23)
    Me.btn_move_down.TabIndex = 6
    Me.btn_move_down.Text = "\/"
    Me.btn_move_down.UseVisualStyleBackColor = True
    '
    'dg_selected_list
    '
    Me.dg_selected_list.CurrentCol = -1
    Me.dg_selected_list.CurrentRow = -1
    Me.dg_selected_list.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_selected_list.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_selected_list.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_selected_list.Location = New System.Drawing.Point(6, 16)
    Me.dg_selected_list.Name = "dg_selected_list"
    Me.dg_selected_list.PanelRightVisible = False
    Me.dg_selected_list.Redraw = True
    Me.dg_selected_list.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_selected_list.Size = New System.Drawing.Size(353, 181)
    Me.dg_selected_list.Sortable = False
    Me.dg_selected_list.SortAscending = False
    Me.dg_selected_list.SortByCol = 0
    Me.dg_selected_list.TabIndex = 1
    Me.dg_selected_list.ToolTipped = True
    Me.dg_selected_list.TopRow = -2
    '
    'btn_add_element
    '
    Me.btn_add_element.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.25!)
    Me.btn_add_element.Location = New System.Drawing.Point(345, 24)
    Me.btn_add_element.Name = "btn_add_element"
    Me.btn_add_element.Size = New System.Drawing.Size(33, 23)
    Me.btn_add_element.TabIndex = 7
    Me.btn_add_element.Text = ">"
    Me.btn_add_element.UseVisualStyleBackColor = True
    '
    'btn_remove_element
    '
    Me.btn_remove_element.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.25!)
    Me.btn_remove_element.Location = New System.Drawing.Point(345, 53)
    Me.btn_remove_element.Name = "btn_remove_element"
    Me.btn_remove_element.Size = New System.Drawing.Size(33, 23)
    Me.btn_remove_element.TabIndex = 8
    Me.btn_remove_element.Text = "<"
    Me.btn_remove_element.UseVisualStyleBackColor = True
    '
    'uc_check_list
    '
    Me.uc_check_list.GroupBoxText = "xCheckedList"
    Me.uc_check_list.Location = New System.Drawing.Point(7, 5)
    Me.uc_check_list.m_resize_width = 332
    Me.uc_check_list.multiChoice = True
    Me.uc_check_list.Name = "uc_check_list"
    Me.uc_check_list.SelectedIndexes = New Integer(-1) {}
    Me.uc_check_list.SelectedIndexesList = ""
    Me.uc_check_list.SelectedIndexesListLevel2 = ""
    Me.uc_check_list.SelectedValuesList = ""
    Me.uc_check_list.SetLevels = 2
    Me.uc_check_list.Size = New System.Drawing.Size(332, 239)
    Me.uc_check_list.TabIndex = 0
    Me.uc_check_list.ValuesArray = New String(-1) {}
    '
    'uc_checked_list_selector
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.btn_add_element)
    Me.Controls.Add(Me.btn_remove_element)
    Me.Controls.Add(Me.gb_selected_items)
    Me.Controls.Add(Me.uc_check_list)
    Me.Name = "uc_checked_list_selector"
    Me.Size = New System.Drawing.Size(797, 252)
    Me.gb_selected_items.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_check_list As GUI_Controls.uc_checked_list
  Friend WithEvents dg_selected_list As GUI_Controls.uc_grid
  Friend WithEvents gb_selected_items As System.Windows.Forms.GroupBox
  Friend WithEvents btn_move_up As System.Windows.Forms.Button
  Friend WithEvents btn_move_down As System.Windows.Forms.Button
  Friend WithEvents btn_add_element As System.Windows.Forms.Button
  Friend WithEvents btn_remove_element As System.Windows.Forms.Button
  Friend WithEvents btn_clear As System.Windows.Forms.Button

End Class
