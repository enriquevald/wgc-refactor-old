﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_jackpot_viewer_monitor
  Inherits GUI_Controls.uc_treeview_edit_base

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.gb_viewer_monitor_info = New System.Windows.Forms.GroupBox()
    Me.grid_viewer_jackpots_monitor = New GUI_Controls.uc_grid()
    Me.lbl_jackpot_name = New System.Windows.Forms.Label()
    Me.lbl_jackpot_status = New System.Windows.Forms.Label()
    Me.tim_reload_info_viewer = New System.Windows.Forms.Timer(Me.components)
    Me.lbl_jackpotViewer_status = New System.Windows.Forms.Label()
    Me.lbl_jackpotViewer_name = New System.Windows.Forms.Label()
    Me.lbl_last_update = New System.Windows.Forms.Label()
    Me.lbl_jackpot_viewer_last_update = New System.Windows.Forms.Label()
    Me.pnl_container.SuspendLayout()
    Me.gb_viewer_monitor_info.SuspendLayout()
    Me.SuspendLayout()
    '
    'pnl_container
    '
    Me.pnl_container.Controls.Add(Me.lbl_jackpot_viewer_last_update)
    Me.pnl_container.Controls.Add(Me.lbl_jackpotViewer_status)
    Me.pnl_container.Controls.Add(Me.lbl_jackpotViewer_name)
    Me.pnl_container.Controls.Add(Me.lbl_last_update)
    Me.pnl_container.Controls.Add(Me.lbl_jackpot_status)
    Me.pnl_container.Controls.Add(Me.lbl_jackpot_name)
    Me.pnl_container.Controls.Add(Me.gb_viewer_monitor_info)
    Me.pnl_container.Size = New System.Drawing.Size(664, 304)
    '
    'gb_viewer_monitor_info
    '
    Me.gb_viewer_monitor_info.Controls.Add(Me.grid_viewer_jackpots_monitor)
    Me.gb_viewer_monitor_info.Location = New System.Drawing.Point(14, 82)
    Me.gb_viewer_monitor_info.Name = "gb_viewer_monitor_info"
    Me.gb_viewer_monitor_info.Size = New System.Drawing.Size(635, 208)
    Me.gb_viewer_monitor_info.TabIndex = 4
    Me.gb_viewer_monitor_info.TabStop = False
    Me.gb_viewer_monitor_info.Text = "xViewerMonitorInfo"
    '
    'grid_viewer_jackpots_monitor
    '
    Me.grid_viewer_jackpots_monitor.CurrentCol = -1
    Me.grid_viewer_jackpots_monitor.CurrentRow = -1
    Me.grid_viewer_jackpots_monitor.EditableCellBackColor = System.Drawing.Color.Empty
    Me.grid_viewer_jackpots_monitor.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.grid_viewer_jackpots_monitor.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.grid_viewer_jackpots_monitor.Location = New System.Drawing.Point(7, 20)
    Me.grid_viewer_jackpots_monitor.Name = "grid_viewer_jackpots_monitor"
    Me.grid_viewer_jackpots_monitor.PanelRightVisible = False
    Me.grid_viewer_jackpots_monitor.Redraw = True
    Me.grid_viewer_jackpots_monitor.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.grid_viewer_jackpots_monitor.Size = New System.Drawing.Size(622, 182)
    Me.grid_viewer_jackpots_monitor.Sortable = False
    Me.grid_viewer_jackpots_monitor.SortAscending = True
    Me.grid_viewer_jackpots_monitor.SortByCol = 0
    Me.grid_viewer_jackpots_monitor.TabIndex = 0
    Me.grid_viewer_jackpots_monitor.ToolTipped = True
    Me.grid_viewer_jackpots_monitor.TopRow = -2
    Me.grid_viewer_jackpots_monitor.WordWrap = False
    '
    'lbl_jackpot_name
    '
    Me.lbl_jackpot_name.AutoSize = True
    Me.lbl_jackpot_name.ForeColor = System.Drawing.Color.Navy
    Me.lbl_jackpot_name.Location = New System.Drawing.Point(100, 19)
    Me.lbl_jackpot_name.Name = "lbl_jackpot_name"
    Me.lbl_jackpot_name.Size = New System.Drawing.Size(78, 13)
    Me.lbl_jackpot_name.TabIndex = 1
    Me.lbl_jackpot_name.Text = "xJackpotName"
    '
    'lbl_jackpot_status
    '
    Me.lbl_jackpot_status.AutoSize = True
    Me.lbl_jackpot_status.ForeColor = System.Drawing.Color.Navy
    Me.lbl_jackpot_status.Location = New System.Drawing.Point(100, 43)
    Me.lbl_jackpot_status.Name = "lbl_jackpot_status"
    Me.lbl_jackpot_status.Size = New System.Drawing.Size(80, 13)
    Me.lbl_jackpot_status.TabIndex = 3
    Me.lbl_jackpot_status.Text = "xJackpotStatus"
    '
    'tim_reload_info_viewer
    '
    Me.tim_reload_info_viewer.Enabled = True
    Me.tim_reload_info_viewer.Interval = 5000
    '
    'lbl_jackpotViewer_status
    '
    Me.lbl_jackpotViewer_status.AutoSize = True
    Me.lbl_jackpotViewer_status.ForeColor = System.Drawing.Color.Black
    Me.lbl_jackpotViewer_status.Location = New System.Drawing.Point(18, 43)
    Me.lbl_jackpotViewer_status.Name = "lbl_jackpotViewer_status"
    Me.lbl_jackpotViewer_status.Size = New System.Drawing.Size(80, 13)
    Me.lbl_jackpotViewer_status.TabIndex = 2
    Me.lbl_jackpotViewer_status.Text = "xJackpotStatus"
    '
    'lbl_jackpotViewer_name
    '
    Me.lbl_jackpotViewer_name.AutoSize = True
    Me.lbl_jackpotViewer_name.ForeColor = System.Drawing.Color.Black
    Me.lbl_jackpotViewer_name.Location = New System.Drawing.Point(18, 19)
    Me.lbl_jackpotViewer_name.Name = "lbl_jackpotViewer_name"
    Me.lbl_jackpotViewer_name.Size = New System.Drawing.Size(78, 13)
    Me.lbl_jackpotViewer_name.TabIndex = 0
    Me.lbl_jackpotViewer_name.Text = "xJackpotName"
    '
    'lbl_last_update
    '
    Me.lbl_last_update.AutoSize = True
    Me.lbl_last_update.ForeColor = System.Drawing.Color.Navy
    Me.lbl_last_update.Location = New System.Drawing.Point(524, 43)
    Me.lbl_last_update.Name = "lbl_last_update"
    Me.lbl_last_update.Size = New System.Drawing.Size(80, 13)
    Me.lbl_last_update.TabIndex = 3
    Me.lbl_last_update.Text = "xJackpotStatus"
    '
    'lbl_jackpot_viewer_last_update
    '
    Me.lbl_jackpot_viewer_last_update.AutoSize = True
    Me.lbl_jackpot_viewer_last_update.ForeColor = System.Drawing.Color.Black
    Me.lbl_jackpot_viewer_last_update.Location = New System.Drawing.Point(401, 43)
    Me.lbl_jackpot_viewer_last_update.Name = "lbl_jackpot_viewer_last_update"
    Me.lbl_jackpot_viewer_last_update.Size = New System.Drawing.Size(80, 13)
    Me.lbl_jackpot_viewer_last_update.TabIndex = 2
    Me.lbl_jackpot_viewer_last_update.Text = "xJackpotStatus"
    '
    'uc_jackpot_viewer_monitor
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
    Me.Name = "uc_jackpot_viewer_monitor"
    Me.Size = New System.Drawing.Size(664, 304)
    Me.pnl_container.ResumeLayout(False)
    Me.pnl_container.PerformLayout()
    Me.gb_viewer_monitor_info.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_viewer_monitor_info As System.Windows.Forms.GroupBox
  Friend WithEvents grid_viewer_jackpots_monitor As GUI_Controls.uc_grid
  Friend WithEvents lbl_jackpot_status As System.Windows.Forms.Label
  Friend WithEvents lbl_jackpot_name As System.Windows.Forms.Label
  Friend WithEvents tim_reload_info_viewer As System.Windows.Forms.Timer
  Friend WithEvents lbl_jackpotViewer_status As System.Windows.Forms.Label
  Friend WithEvents lbl_jackpotViewer_name As System.Windows.Forms.Label
  Friend WithEvents lbl_jackpot_viewer_last_update As System.Windows.Forms.Label
  Friend WithEvents lbl_last_update As System.Windows.Forms.Label

End Class
