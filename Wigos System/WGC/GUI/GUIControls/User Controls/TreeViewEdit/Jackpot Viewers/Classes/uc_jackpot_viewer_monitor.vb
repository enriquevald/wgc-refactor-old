﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' UC NAME:       uc_jackpot_award_happy_hour
' DESCRIPTION:   Jackpot User Control for happy hour configuration
' AUTHOR:        Rubén Lama Ordóñez
' CREATION DATE: 27-APR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 27-APR-2017  RLO    Initial version
'--------------------------------------------------------------------

#Region "Imports"

Imports GUI_CommonOperations
Imports System.Windows.Forms
Imports GUI_CommonMisc.mdl_NLS
Imports WSI.Common
Imports WSI.Common.Jackpot
Imports WSI.Common.Jackpot.Jackpot

#End Region

Public Class uc_jackpot_viewer_monitor
  Inherits uc_treeview_edit_base

#Region "Constants"

  'Timer
  Private Const TIMER_ELAPSE = 30000

  ' Grid setup
  Private Const GRID_HEADER_ROWS As Integer = 1
  Private GRID_COLUMNS As Integer = 7

  ' Grid Columns
  Private GRID_COLUMN_JACKPOT_ORDER As Integer = 0
  Private GRID_COLUMN_JACKPOT_STATUS As Integer = 1
  Private GRID_COLUMN_JACKPOT_NAME As Integer = 2
  Private GRID_COLUMN_JACKPOT_MAIN_AMOUNT As Integer = 3
  Private GRID_COLUMN_JACKPOT_HAPPY_AMOUNT As Integer = 4
  Private GRID_COLUMN_JACKPOT_SHARING_AMOUNT As Integer = 5
  Private GRID_COLUMN_JACKPOT_HIDDEN_AMOUNT As Integer = 6

  ' Width
  Private Const GRID_WIDTH_JACKPOT_ORDER As Integer = 700
  Private Const GRID_WIDTH_JACKPOT_STATUS As Integer = 1400
  Private Const GRID_WIDTH_JACKPOT_NAME As Integer = 1600
  Private Const GRID_WIDTH_JACKPOT_MAIN_AMOUNT As Integer = 1300
  Private Const GRID_WIDTH_JACKPOT_HAPPY_AMOUNT As Integer = 1300
  Private Const GRID_WIDTH_JACKPOT_SHARING_AMOUNT As Integer = 1300
  Private Const GRID_WIDTH_JACKPOT_HIDDEN_AMOUNT As Integer = 1300

#End Region

#Region "Members"

  Private m_jackpot_viewer As JackpotViewer

#End Region

#Region "Properties"

  Public Property JackpotViewer As JackpotViewer
    Get
      Return Me.m_jackpot_viewer
    End Get
    Set(value As JackpotViewer)
      Me.m_jackpot_viewer = value
    End Set
  End Property

#End Region

#Region "Sub"

  Public Sub New(Optional ByVal JackpotViewer As JackpotViewer = Nothing)

    Me.JackpotViewer = JackpotViewer

    If Not Me.tim_reload_info_viewer Is Nothing Then
      Me.tim_reload_info_viewer.Interval = TIMER_ELAPSE
    End If

    Call Me.Init()

  End Sub

  ''' <summary>
  ''' Init method
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub Init()

    ' This call is required by the designer.
    Call InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControls()

  End Sub

  ''' <summary>
  ''' Initialite uc controls
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub InitControls()

    'gb_happy_hour_config
    Me.gb_viewer_monitor_info.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(456)

    Me.tim_reload_info_viewer.Enabled = True

    Call GUI_StyleSheet()

    Call BindData()

  End Sub ' UC_InitControls

  ''' <summary>
  ''' Bind uc data
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub BindData()

    ' Bind all data to controls
    If Not Me.JackpotViewer Is Nothing Then

      'Bind labels from jackpot Viewer
      Me.lbl_jackpot_name.Text = Me.JackpotViewer.Name.ToString()
      Me.lbl_jackpot_status.Text = IIf(Me.JackpotViewer.Enabled = True, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3008), GLB_NLS_GUI_PLAYER_TRACKING.GetString(3359))
      Me.lbl_last_update.Text = Me.JackpotViewer.LastUpdate

      Me.lbl_jackpotViewer_name.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(560) & " :"
      Me.lbl_jackpotViewer_status.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(215) & " :"
      Me.lbl_jackpot_viewer_last_update.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7263) & " :"

      Me.BindDataGrid()

    End If

  End Sub ' BindData

#End Region

#Region "Public Functions "

#End Region

#Region "Private Functions"

  ''' <summary>
  ''' Bind data grid with jackpot viewer items
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub BindDataGrid()
    Dim _idx As Integer

    If Me.JackpotViewer Is Nothing OrElse Me.JackpotViewer.Jackpots Is Nothing Then
      Return
    End If

    For Each item As JackpotViewerItem In Me.JackpotViewer.Jackpots.Items
      _idx = Me.grid_viewer_jackpots_monitor.AddRow()
      Call Me.GUI_SetupRow(_idx, item)
    Next

  End Sub ' BindDataGrid

  ''' <summary>
  ''' Add row to data grid from JackpotViewerItem
  ''' </summary>
  ''' <param name="Item"></param>
  ''' <remarks></remarks>
  Private Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal Item As JackpotViewerItem) As Boolean

    Try

      With Me.grid_viewer_jackpots_monitor

        ' Jackpot order
        .Cell(RowIndex, GRID_COLUMN_JACKPOT_ORDER).Value = Item.Order

        ' Jackpot name
        .Cell(RowIndex, GRID_COLUMN_JACKPOT_NAME).Value = Item.Name

        ' Jackpot satus
        .Cell(RowIndex, GRID_COLUMN_JACKPOT_STATUS).Value = IIf(Item.Status = False, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3008), GLB_NLS_GUI_PLAYER_TRACKING.GetString(3359))

        ' Jackpot main
        .Cell(RowIndex, GRID_COLUMN_JACKPOT_MAIN_AMOUNT).Value = GUI_FormatCurrency(Item.Main)

        ' Jackpot happy
        .Cell(RowIndex, GRID_COLUMN_JACKPOT_HAPPY_AMOUNT).Value = GUI_FormatCurrency(Item.HappyHour)

        ' Jackpot sharing
        .Cell(RowIndex, GRID_COLUMN_JACKPOT_SHARING_AMOUNT).Value = GUI_FormatCurrency(Item.PrizeSharing)

        ' Jackpot hidden
        .Cell(RowIndex, GRID_COLUMN_JACKPOT_HIDDEN_AMOUNT).Value = GUI_FormatCurrency(Item.Hidden)

      End With

      Return True

    Catch ex As Exception
      Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(591), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' Error al configurar el grid
    End Try

    Return False

  End Function

  ''' <summary>
  ''' Set Style to the Grid
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()

    ' Grid StyleSheet
    With Me.grid_viewer_jackpots_monitor

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)

      ' Jackpot order
      .Column(GRID_COLUMN_JACKPOT_ORDER).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(592)
      .Column(GRID_COLUMN_JACKPOT_ORDER).Header(1).Text = ""
      .Column(GRID_COLUMN_JACKPOT_ORDER).Width = GRID_WIDTH_JACKPOT_ORDER
      .Column(GRID_COLUMN_JACKPOT_ORDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Jackpot status
      .Column(GRID_COLUMN_JACKPOT_STATUS).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(215)
      .Column(GRID_COLUMN_JACKPOT_STATUS).Header(1).Text = ""
      .Column(GRID_COLUMN_JACKPOT_STATUS).Width = GRID_WIDTH_JACKPOT_STATUS
      .Column(GRID_COLUMN_JACKPOT_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Jackpot name
      .Column(GRID_COLUMN_JACKPOT_NAME).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(282)
      .Column(GRID_COLUMN_JACKPOT_NAME).Header(1).Text = ""
      .Column(GRID_COLUMN_JACKPOT_NAME).Width = GRID_WIDTH_JACKPOT_NAME
      .Column(GRID_COLUMN_JACKPOT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Jackpot Accumulated
      .Column(GRID_COLUMN_JACKPOT_MAIN_AMOUNT).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(593)
      .Column(GRID_COLUMN_JACKPOT_MAIN_AMOUNT).Header(1).Text = ""
      .Column(GRID_COLUMN_JACKPOT_MAIN_AMOUNT).Width = GRID_WIDTH_JACKPOT_MAIN_AMOUNT
      .Column(GRID_COLUMN_JACKPOT_MAIN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Terminal happy amount
      .Column(GRID_COLUMN_JACKPOT_HAPPY_AMOUNT).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(594)
      .Column(GRID_COLUMN_JACKPOT_HAPPY_AMOUNT).Header(1).Text = ""
      .Column(GRID_COLUMN_JACKPOT_HAPPY_AMOUNT).Width = GRID_WIDTH_JACKPOT_HAPPY_AMOUNT
      .Column(GRID_COLUMN_JACKPOT_HAPPY_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Terminal sharing amount
      .Column(GRID_COLUMN_JACKPOT_SHARING_AMOUNT).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(595)
      .Column(GRID_COLUMN_JACKPOT_SHARING_AMOUNT).Header(1).Text = ""
      .Column(GRID_COLUMN_JACKPOT_SHARING_AMOUNT).Width = GRID_WIDTH_JACKPOT_SHARING_AMOUNT
      .Column(GRID_COLUMN_JACKPOT_SHARING_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Terminal hidden amount
      .Column(GRID_COLUMN_JACKPOT_HIDDEN_AMOUNT).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(596)
      .Column(GRID_COLUMN_JACKPOT_HIDDEN_AMOUNT).Header(1).Text = ""
      .Column(GRID_COLUMN_JACKPOT_HIDDEN_AMOUNT).Width = GRID_WIDTH_JACKPOT_HIDDEN_AMOUNT
      .Column(GRID_COLUMN_JACKPOT_HIDDEN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub ' GUI_StyleSheet


  Private Sub ReloadInfo()

    Me.grid_viewer_jackpots_monitor.Clear()
    Call BindDataGrid()
    Me.lbl_last_update.Text = WGDB.Now()

  End Sub

#End Region

#Region "Events"

  Private Sub tim_reload_info_viewer_Tick(sender As Object, e As EventArgs) Handles tim_reload_info_viewer.Tick

    Call ReloadInfo()

  End Sub


#End Region

End Class
