﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_jackpot_viewer_terminal_selection
  Inherits GUI_Controls.uc_treeview_edit_base

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_jackpots_viewer_terminals_sel = New System.Windows.Forms.GroupBox()
    Me.btn_remove_all = New System.Windows.Forms.Button()
    Me.btn_remove = New System.Windows.Forms.Button()
    Me.btn_add_all = New System.Windows.Forms.Button()
    Me.btn_add = New System.Windows.Forms.Button()
    Me.lb_available_terminals_title = New System.Windows.Forms.Label()
    Me.lb_jackpot_viewer_title = New System.Windows.Forms.Label()
    Me.lb_jackpot_viewer = New System.Windows.Forms.ListBox()
    Me.lb_available_jackpots = New System.Windows.Forms.ListBox()
    Me.lbl_jackpotViewer_name = New System.Windows.Forms.Label()
    Me.lbl_jackpot_name = New System.Windows.Forms.Label()
    Me.pnl_container.SuspendLayout()
    Me.gb_jackpots_viewer_terminals_sel.SuspendLayout()
    Me.SuspendLayout()
    '
    'pnl_container
    '
    Me.pnl_container.Controls.Add(Me.lbl_jackpotViewer_name)
    Me.pnl_container.Controls.Add(Me.lbl_jackpot_name)
    Me.pnl_container.Controls.Add(Me.gb_jackpots_viewer_terminals_sel)
    Me.pnl_container.Size = New System.Drawing.Size(670, 399)
    '
    'gb_jackpots_viewer_terminals_sel
    '
    Me.gb_jackpots_viewer_terminals_sel.Controls.Add(Me.btn_remove_all)
    Me.gb_jackpots_viewer_terminals_sel.Controls.Add(Me.btn_remove)
    Me.gb_jackpots_viewer_terminals_sel.Controls.Add(Me.btn_add_all)
    Me.gb_jackpots_viewer_terminals_sel.Controls.Add(Me.btn_add)
    Me.gb_jackpots_viewer_terminals_sel.Controls.Add(Me.lb_available_terminals_title)
    Me.gb_jackpots_viewer_terminals_sel.Controls.Add(Me.lb_jackpot_viewer_title)
    Me.gb_jackpots_viewer_terminals_sel.Controls.Add(Me.lb_jackpot_viewer)
    Me.gb_jackpots_viewer_terminals_sel.Controls.Add(Me.lb_available_jackpots)
    Me.gb_jackpots_viewer_terminals_sel.Location = New System.Drawing.Point(5, 41)
    Me.gb_jackpots_viewer_terminals_sel.Name = "gb_jackpots_viewer_terminals_sel"
    Me.gb_jackpots_viewer_terminals_sel.Size = New System.Drawing.Size(660, 354)
    Me.gb_jackpots_viewer_terminals_sel.TabIndex = 2
    Me.gb_jackpots_viewer_terminals_sel.TabStop = False
    Me.gb_jackpots_viewer_terminals_sel.Text = "xTerminalsSelection"
    '
    'btn_remove_all
    '
    Me.btn_remove_all.Location = New System.Drawing.Point(309, 218)
    Me.btn_remove_all.Name = "btn_remove_all"
    Me.btn_remove_all.Size = New System.Drawing.Size(39, 31)
    Me.btn_remove_all.TabIndex = 5
    Me.btn_remove_all.Text = "<<"
    Me.btn_remove_all.UseVisualStyleBackColor = True
    Me.btn_remove_all.Visible = False
    '
    'btn_remove
    '
    Me.btn_remove.Location = New System.Drawing.Point(309, 181)
    Me.btn_remove.Name = "btn_remove"
    Me.btn_remove.Size = New System.Drawing.Size(39, 31)
    Me.btn_remove.TabIndex = 4
    Me.btn_remove.Text = "<"
    Me.btn_remove.UseVisualStyleBackColor = True
    '
    'btn_add_all
    '
    Me.btn_add_all.Location = New System.Drawing.Point(309, 107)
    Me.btn_add_all.Name = "btn_add_all"
    Me.btn_add_all.Size = New System.Drawing.Size(39, 31)
    Me.btn_add_all.TabIndex = 2
    Me.btn_add_all.Text = ">>"
    Me.btn_add_all.UseVisualStyleBackColor = True
    Me.btn_add_all.Visible = False
    '
    'btn_add
    '
    Me.btn_add.Location = New System.Drawing.Point(309, 144)
    Me.btn_add.Name = "btn_add"
    Me.btn_add.Size = New System.Drawing.Size(39, 31)
    Me.btn_add.TabIndex = 3
    Me.btn_add.Text = ">"
    Me.btn_add.UseVisualStyleBackColor = True
    '
    'lb_available_terminals_title
    '
    Me.lb_available_terminals_title.Location = New System.Drawing.Point(60, 17)
    Me.lb_available_terminals_title.Name = "lb_available_terminals_title"
    Me.lb_available_terminals_title.Size = New System.Drawing.Size(243, 13)
    Me.lb_available_terminals_title.TabIndex = 0
    Me.lb_available_terminals_title.Text = "xAvailableTerminals"
    Me.lb_available_terminals_title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lb_jackpot_viewer_title
    '
    Me.lb_jackpot_viewer_title.Location = New System.Drawing.Point(351, 17)
    Me.lb_jackpot_viewer_title.Name = "lb_jackpot_viewer_title"
    Me.lb_jackpot_viewer_title.Size = New System.Drawing.Size(243, 13)
    Me.lb_jackpot_viewer_title.TabIndex = 6
    Me.lb_jackpot_viewer_title.Text = "xJackpotViewer"
    Me.lb_jackpot_viewer_title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lb_jackpot_viewer
    '
    Me.lb_jackpot_viewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lb_jackpot_viewer.FormattingEnabled = True
    Me.lb_jackpot_viewer.Location = New System.Drawing.Point(354, 36)
    Me.lb_jackpot_viewer.Name = "lb_jackpot_viewer"
    Me.lb_jackpot_viewer.Size = New System.Drawing.Size(240, 301)
    Me.lb_jackpot_viewer.TabIndex = 7
    '
    'lb_available_jackpots
    '
    Me.lb_available_jackpots.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lb_available_jackpots.FormattingEnabled = True
    Me.lb_available_jackpots.Location = New System.Drawing.Point(63, 36)
    Me.lb_available_jackpots.Name = "lb_available_jackpots"
    Me.lb_available_jackpots.Size = New System.Drawing.Size(240, 301)
    Me.lb_available_jackpots.TabIndex = 1
    '
    'lbl_jackpotViewer_name
    '
    Me.lbl_jackpotViewer_name.AutoSize = True
    Me.lbl_jackpotViewer_name.ForeColor = System.Drawing.Color.Black
    Me.lbl_jackpotViewer_name.Location = New System.Drawing.Point(11, 14)
    Me.lbl_jackpotViewer_name.Name = "lbl_jackpotViewer_name"
    Me.lbl_jackpotViewer_name.Size = New System.Drawing.Size(78, 13)
    Me.lbl_jackpotViewer_name.TabIndex = 0
    Me.lbl_jackpotViewer_name.Text = "xJackpotName"
    '
    'lbl_jackpot_name
    '
    Me.lbl_jackpot_name.AutoSize = True
    Me.lbl_jackpot_name.ForeColor = System.Drawing.Color.Navy
    Me.lbl_jackpot_name.Location = New System.Drawing.Point(69, 14)
    Me.lbl_jackpot_name.Name = "lbl_jackpot_name"
    Me.lbl_jackpot_name.Size = New System.Drawing.Size(78, 13)
    Me.lbl_jackpot_name.TabIndex = 1
    Me.lbl_jackpot_name.Text = "xJackpotName"
    '
    'uc_jackpot_viewer_terminal_selection
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
    Me.Name = "uc_jackpot_viewer_terminal_selection"
    Me.Size = New System.Drawing.Size(670, 399)
    Me.pnl_container.ResumeLayout(False)
    Me.pnl_container.PerformLayout()
    Me.gb_jackpots_viewer_terminals_sel.ResumeLayout(False)
    Me.ResumeLayout(false)

End Sub
  Friend WithEvents gb_jackpots_viewer_terminals_sel As System.Windows.Forms.GroupBox
  Friend WithEvents btn_remove As System.Windows.Forms.Button
  Friend WithEvents btn_add As System.Windows.Forms.Button
  Friend WithEvents lb_available_terminals_title As System.Windows.Forms.Label
  Friend WithEvents lb_jackpot_viewer_title As System.Windows.Forms.Label
  Friend WithEvents lb_jackpot_viewer As System.Windows.Forms.ListBox
  Friend WithEvents lb_available_jackpots As System.Windows.Forms.ListBox
  Friend WithEvents btn_remove_all As System.Windows.Forms.Button
  Friend WithEvents btn_add_all As System.Windows.Forms.Button
  Friend WithEvents lbl_jackpotViewer_name As System.Windows.Forms.Label
  Friend WithEvents lbl_jackpot_name As System.Windows.Forms.Label

End Class
