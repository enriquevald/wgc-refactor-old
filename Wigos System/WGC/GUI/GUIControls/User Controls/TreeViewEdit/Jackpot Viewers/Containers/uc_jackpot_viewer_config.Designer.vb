﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_jackpot_viewer_config
  Inherits GUI_Controls.uc_treeview_edit_base

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_viewer_config = New System.Windows.Forms.GroupBox()
    Me.ef_viewer_code = New GUI_Controls.uc_entry_field()
    Me.ef_viewer_name = New GUI_Controls.uc_entry_field()
    Me.chk_enabled = New System.Windows.Forms.CheckBox()
    Me.gb_viwer_info = New System.Windows.Forms.GroupBox()
    Me.grid_viewer_jackpots = New GUI_Controls.uc_grid()
    Me.gb_jackpots_viewer_sel = New System.Windows.Forms.GroupBox()
    Me.btn_down = New System.Windows.Forms.Button()
    Me.btn_up = New System.Windows.Forms.Button()
    Me.btn_remove = New System.Windows.Forms.Button()
    Me.btn_add = New System.Windows.Forms.Button()
    Me.lb_available_jackpots_title = New System.Windows.Forms.Label()
    Me.lb_jackpot_viewer_title = New System.Windows.Forms.Label()
    Me.lb_jackpot_viewer = New System.Windows.Forms.ListBox()
    Me.lb_available_jackpots = New System.Windows.Forms.ListBox()
    Me.pnl_container.SuspendLayout()
    Me.gb_viewer_config.SuspendLayout()
    Me.gb_viwer_info.SuspendLayout()
    Me.gb_jackpots_viewer_sel.SuspendLayout()
    Me.SuspendLayout()
    '
    'pnl_container
    '
    Me.pnl_container.Controls.Add(Me.gb_jackpots_viewer_sel)
    Me.pnl_container.Controls.Add(Me.gb_viwer_info)
    Me.pnl_container.Controls.Add(Me.gb_viewer_config)
    Me.pnl_container.Size = New System.Drawing.Size(670, 594)
    '
    'gb_viewer_config
    '
    Me.gb_viewer_config.Controls.Add(Me.ef_viewer_code)
    Me.gb_viewer_config.Controls.Add(Me.ef_viewer_name)
    Me.gb_viewer_config.Controls.Add(Me.chk_enabled)
    Me.gb_viewer_config.Location = New System.Drawing.Point(3, 3)
    Me.gb_viewer_config.Name = "gb_viewer_config"
    Me.gb_viewer_config.Size = New System.Drawing.Size(660, 56)
    Me.gb_viewer_config.TabIndex = 0
    Me.gb_viewer_config.TabStop = False
    Me.gb_viewer_config.Text = "xViewerConfig"
    '
    'ef_viewer_code
    '
    Me.ef_viewer_code.DoubleValue = 0.0R
    Me.ef_viewer_code.IntegerValue = 0
    Me.ef_viewer_code.IsReadOnly = False
    Me.ef_viewer_code.Location = New System.Drawing.Point(515, 23)
    Me.ef_viewer_code.Name = "ef_viewer_code"
    Me.ef_viewer_code.PlaceHolder = Nothing
    Me.ef_viewer_code.Size = New System.Drawing.Size(125, 24)
    Me.ef_viewer_code.SufixText = "Sufix Text"
    Me.ef_viewer_code.SufixTextVisible = True
    Me.ef_viewer_code.TabIndex = 2
    Me.ef_viewer_code.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_viewer_code.TextValue = ""
    Me.ef_viewer_code.TextWidth = 50
    Me.ef_viewer_code.Value = ""
    Me.ef_viewer_code.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_viewer_name
    '
    Me.ef_viewer_name.DoubleValue = 0.0R
    Me.ef_viewer_name.IntegerValue = 0
    Me.ef_viewer_name.IsReadOnly = False
    Me.ef_viewer_name.Location = New System.Drawing.Point(95, 23)
    Me.ef_viewer_name.Name = "ef_viewer_name"
    Me.ef_viewer_name.PlaceHolder = Nothing
    Me.ef_viewer_name.Size = New System.Drawing.Size(406, 24)
    Me.ef_viewer_name.SufixText = "Sufix Text"
    Me.ef_viewer_name.SufixTextVisible = True
    Me.ef_viewer_name.TabIndex = 1
    Me.ef_viewer_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_viewer_name.TextValue = ""
    Me.ef_viewer_name.Value = ""
    Me.ef_viewer_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_enabled
    '
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(7, 26)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(70, 17)
    Me.chk_enabled.TabIndex = 0
    Me.chk_enabled.Text = "xEnabled"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'gb_viwer_info
    '
    Me.gb_viwer_info.Controls.Add(Me.grid_viewer_jackpots)
    Me.gb_viwer_info.Location = New System.Drawing.Point(3, 65)
    Me.gb_viwer_info.Name = "gb_viwer_info"
    Me.gb_viwer_info.Size = New System.Drawing.Size(660, 160)
    Me.gb_viwer_info.TabIndex = 1
    Me.gb_viwer_info.TabStop = False
    Me.gb_viwer_info.Text = "xViewerInfo"
    '
    'grid_viewer_jackpots
    '
    Me.grid_viewer_jackpots.CurrentCol = -1
    Me.grid_viewer_jackpots.CurrentRow = -1
    Me.grid_viewer_jackpots.EditableCellBackColor = System.Drawing.Color.Empty
    Me.grid_viewer_jackpots.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.grid_viewer_jackpots.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.grid_viewer_jackpots.Location = New System.Drawing.Point(7, 20)
    Me.grid_viewer_jackpots.Name = "grid_viewer_jackpots"
    Me.grid_viewer_jackpots.PanelRightVisible = False
    Me.grid_viewer_jackpots.Redraw = True
    Me.grid_viewer_jackpots.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.grid_viewer_jackpots.Size = New System.Drawing.Size(647, 134)
    Me.grid_viewer_jackpots.Sortable = False
    Me.grid_viewer_jackpots.SortAscending = True
    Me.grid_viewer_jackpots.SortByCol = 0
    Me.grid_viewer_jackpots.TabIndex = 0
    Me.grid_viewer_jackpots.ToolTipped = True
    Me.grid_viewer_jackpots.TopRow = -2
    Me.grid_viewer_jackpots.WordWrap = False
    '
    'gb_jackpots_viewer_sel
    '
    Me.gb_jackpots_viewer_sel.Controls.Add(Me.btn_down)
    Me.gb_jackpots_viewer_sel.Controls.Add(Me.btn_up)
    Me.gb_jackpots_viewer_sel.Controls.Add(Me.btn_remove)
    Me.gb_jackpots_viewer_sel.Controls.Add(Me.btn_add)
    Me.gb_jackpots_viewer_sel.Controls.Add(Me.lb_available_jackpots_title)
    Me.gb_jackpots_viewer_sel.Controls.Add(Me.lb_jackpot_viewer_title)
    Me.gb_jackpots_viewer_sel.Controls.Add(Me.lb_jackpot_viewer)
    Me.gb_jackpots_viewer_sel.Controls.Add(Me.lb_available_jackpots)
    Me.gb_jackpots_viewer_sel.Location = New System.Drawing.Point(3, 237)
    Me.gb_jackpots_viewer_sel.Name = "gb_jackpots_viewer_sel"
    Me.gb_jackpots_viewer_sel.Size = New System.Drawing.Size(660, 354)
    Me.gb_jackpots_viewer_sel.TabIndex = 2
    Me.gb_jackpots_viewer_sel.TabStop = False
    Me.gb_jackpots_viewer_sel.Text = "xJackpotSelection"
    '
    'btn_down
    '
    Me.btn_down.Location = New System.Drawing.Point(600, 181)
    Me.btn_down.Name = "btn_down"
    Me.btn_down.Size = New System.Drawing.Size(54, 31)
    Me.btn_down.TabIndex = 7
    Me.btn_down.Text = "xDown"
    Me.btn_down.UseVisualStyleBackColor = True
    '
    'btn_up
    '
    Me.btn_up.Location = New System.Drawing.Point(600, 144)
    Me.btn_up.Name = "btn_up"
    Me.btn_up.Size = New System.Drawing.Size(54, 31)
    Me.btn_up.TabIndex = 6
    Me.btn_up.Text = "xUp"
    Me.btn_up.UseVisualStyleBackColor = True
    '
    'btn_remove
    '
    Me.btn_remove.Location = New System.Drawing.Point(309, 181)
    Me.btn_remove.Name = "btn_remove"
    Me.btn_remove.Size = New System.Drawing.Size(39, 31)
    Me.btn_remove.TabIndex = 3
    Me.btn_remove.Text = "<"
    Me.btn_remove.UseVisualStyleBackColor = True
    '
    'btn_add
    '
    Me.btn_add.Location = New System.Drawing.Point(309, 144)
    Me.btn_add.Name = "btn_add"
    Me.btn_add.Size = New System.Drawing.Size(39, 31)
    Me.btn_add.TabIndex = 2
    Me.btn_add.Text = ">"
    Me.btn_add.UseVisualStyleBackColor = True
    '
    'lb_available_jackpots_title
    '
    Me.lb_available_jackpots_title.Location = New System.Drawing.Point(60, 17)
    Me.lb_available_jackpots_title.Name = "lb_available_jackpots_title"
    Me.lb_available_jackpots_title.Size = New System.Drawing.Size(243, 13)
    Me.lb_available_jackpots_title.TabIndex = 0
    Me.lb_available_jackpots_title.Text = "xAvailableJackpots"
    Me.lb_available_jackpots_title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lb_jackpot_viewer_title
    '
    Me.lb_jackpot_viewer_title.Location = New System.Drawing.Point(351, 17)
    Me.lb_jackpot_viewer_title.Name = "lb_jackpot_viewer_title"
    Me.lb_jackpot_viewer_title.Size = New System.Drawing.Size(243, 13)
    Me.lb_jackpot_viewer_title.TabIndex = 4
    Me.lb_jackpot_viewer_title.Text = "xJackpotViewer"
    Me.lb_jackpot_viewer_title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lb_jackpot_viewer
    '
    Me.lb_jackpot_viewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lb_jackpot_viewer.FormattingEnabled = True
    Me.lb_jackpot_viewer.Location = New System.Drawing.Point(354, 36)
    Me.lb_jackpot_viewer.Name = "lb_jackpot_viewer"
    Me.lb_jackpot_viewer.Size = New System.Drawing.Size(240, 301)
    Me.lb_jackpot_viewer.TabIndex = 5
    '
    'lb_available_jackpots
    '
    Me.lb_available_jackpots.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lb_available_jackpots.FormattingEnabled = True
    Me.lb_available_jackpots.Location = New System.Drawing.Point(63, 36)
    Me.lb_available_jackpots.Name = "lb_available_jackpots"
    Me.lb_available_jackpots.Size = New System.Drawing.Size(240, 301)
    Me.lb_available_jackpots.TabIndex = 1
    '
    'uc_jackpot_viewer_config
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
    Me.Name = "uc_jackpot_viewer_config"
    Me.Size = New System.Drawing.Size(670, 594)
    Me.pnl_container.ResumeLayout(False)
    Me.gb_viewer_config.ResumeLayout(False)
    Me.gb_viewer_config.PerformLayout()
    Me.gb_viwer_info.ResumeLayout(False)
    Me.gb_jackpots_viewer_sel.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_viewer_config As System.Windows.Forms.GroupBox
  Friend WithEvents ef_viewer_code As GUI_Controls.uc_entry_field
  Public WithEvents ef_viewer_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents gb_jackpots_viewer_sel As System.Windows.Forms.GroupBox
  Friend WithEvents gb_viwer_info As System.Windows.Forms.GroupBox
  Friend WithEvents btn_remove As System.Windows.Forms.Button
  Friend WithEvents btn_add As System.Windows.Forms.Button
  Friend WithEvents lb_available_jackpots_title As System.Windows.Forms.Label
  Friend WithEvents lb_jackpot_viewer_title As System.Windows.Forms.Label
  Friend WithEvents lb_jackpot_viewer As System.Windows.Forms.ListBox
  Friend WithEvents lb_available_jackpots As System.Windows.Forms.ListBox
  Friend WithEvents grid_viewer_jackpots As GUI_Controls.uc_grid
  Friend WithEvents btn_down As System.Windows.Forms.Button
  Friend WithEvents btn_up As System.Windows.Forms.Button

End Class
