﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' UC NAME:       uc_jackpot_viewers_config
' DESCRIPTION:   Jackpot User Control For Configuration Jackpot Viewers
' AUTHOR:        Carlos Cáceres González
' CREATION DATE: 15-MAY-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-MAY-2017  CCG    Initial version
'--------------------------------------------------------------------

Imports WSI.Common.Jackpot
Imports GUI_Controls
Imports WSI.Common
Imports GUI_CommonMisc.mdl_NLS
Imports GUI_CommonOperations
Imports System.Windows.Forms

Public Class uc_jackpot_viewer_config
  Inherits uc_treeview_edit_base

#Region " Constants "

  Private Const LIST_DISPLAY_COLUMN As String = "Name"
  Private Const LIST_VALUE_COLUMN As String = "JackpotId"
  Private Const SQL_ISO_CODE_COLUMN As Integer = 10

  ' Grid setup
  Private Const GRID_HEADER_ROWS As Integer = 1
  Private GRID_COLUMNS As Integer = 7

  ' Grid Columns
  Private GRID_COLUMN_JACKPOT_ORDER As Integer = 0
  Private GRID_COLUMN_JACKPOT_NAME As Integer = 1
  Private GRID_COLUMN_JACKPOT_STATUS As Integer = 2
  Private GRID_COLUMN_JACKPOT_MAIN_AMOUNT As Integer = 3
  Private GRID_COLUMN_JACKPOT_HAPPY_AMOUNT As Integer = 4
  Private GRID_COLUMN_JACKPOT_SHARING_AMOUNT As Integer = 5
  Private GRID_COLUMN_JACKPOT_HIDDEN_AMOUNT As Integer = 6

  ' Width
  Private Const GRID_WIDTH_JACKPOT_ORDER As Integer = 600
  Private Const GRID_WIDTH_JACKPOT_NAME As Integer = 1800
  Private Const GRID_WIDTH_JACKPOT_STATUS As Integer = 1400
  Private Const GRID_WIDTH_JACKPOT_MAIN_AMOUNT As Integer = 1400
  Private Const GRID_WIDTH_JACKPOT_HAPPY_AMOUNT As Integer = 1400
  Private Const GRID_WIDTH_JACKPOT_SHARING_AMOUNT As Integer = 1400
  Private Const GRID_WIDTH_JACKPOT_HIDDEN_AMOUNT As Integer = 1400

#End Region ' Constants

#Region " Members "

  Private m_jackpot_viewer As JackpotViewer
  Private m_available_jackpots As JackpotViewerItemList
  Private m_included_jackpots As JackpotViewerItemList
  Private Const MAX_NAME_STRING_LENGHT = 50
  Private Const MAX_CODE_INT_LENGHT = 4

  Enum ListBoxMovementType
    Add = 0
    Remove = 1
    Up = 2
    Down = 3
  End Enum

#End Region

#Region "Properties"

  Public Property JackpotViewer As JackpotViewer
    Get
      Return Me.m_jackpot_viewer
    End Get
    Set(ByVal value As JackpotViewer)
      Me.m_jackpot_viewer = value
    End Set
  End Property

#End Region ' Properties

  Public Sub New(ByRef Form As frm_treeview_edit, ByVal ShowMode As SHOW_MODE, Optional ByRef JackpotViewer As JackpotViewer = Nothing)

    MyBase.New(Form, ShowMode, JackpotViewer)

    Select Case (ShowMode)
      Case SHOW_MODE.NewItem
        Me.JackpotViewer = New JackpotViewer()
      Case SHOW_MODE.EditItem
        Me.JackpotViewer = JackpotViewer
      Case SHOW_MODE.CloneItem
        Me.JackpotViewer = New JackpotViewer(JackpotViewer.Id)
        Me.JackpotViewer.Reset()
        Me.JackpotViewer.Name = String.Empty
    End Select

    Call Init()
  End Sub

  Public Sub Init()
    ' This call is required by the Windows Form Designer.
    Call InitializeComponent()

    Call GUI_StyleSheet()

    Call InitControls()
    Call BindData()
  End Sub

  Public Sub InitControls()
    Me.gb_viewer_config.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(585)
    Me.chk_enabled.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(218)
    Me.ef_viewer_name.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(282)
    Me.ef_viewer_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, MAX_NAME_STRING_LENGHT)

    Me.ef_viewer_code.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(586)
    Me.ef_viewer_code.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, MAX_CODE_INT_LENGHT)

    Me.gb_jackpots_viewer_sel.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(587)
    Me.lb_available_jackpots_title.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(588)
    Me.lb_jackpot_viewer_title.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(589)

    Me.gb_viwer_info.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(590)

    Me.btn_up.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8366)
    Me.btn_down.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8367)
  End Sub

#Region "Public Functions"

  ''' <summary>
  ''' Check screen data
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function IsScreenDataOk() As Boolean

    If String.IsNullOrEmpty(Me.ef_viewer_name.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8306), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK) ' Es obligatorio introducir el nombre del viewer.
      Me.ef_viewer_name.Focus()

      Return False
    End If

    If String.IsNullOrEmpty(Me.ef_viewer_code.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8307), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK) ' Es obligatorio introducir un código del viewer.
      Me.ef_viewer_code.Focus()

      Return False
    End If

    If Not Me.JackpotViewer.CheckIfCodeIsUnique(Me.ef_viewer_code.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8308), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK) ' El valor del código ha de ser único.
      Me.ef_viewer_code.Focus()

      Return False
    End If

    Return True

  End Function ' IsScreenDataOk

#End Region

#Region "Private Functions"

  ''' <summary>
  ''' Get Jackpot Viewer configuration
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetJackpotViewerConfig() As JackpotViewer

    Me.GetScreenData()

    Return Me.JackpotViewer

  End Function ' GetJackpotViewerConfig

  ''' <summary>
  ''' To bind the data to de form
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub BindData()

    Me.chk_enabled.Checked = Me.JackpotViewer.Enabled
    Me.ef_viewer_name.Value = Me.JackpotViewer.Name
    Me.ef_viewer_code.Value = Me.JackpotViewer.Code.ToString()

    Me.m_available_jackpots = Me.JackpotViewer.GetAvailableJackpots()
    Me.m_included_jackpots = Me.JackpotViewer.GetIncludedJackpots()

    Call Me.BindListBoxControls()

    ' To init the grid
    Call Me.BindJackpotsGrid()

  End Sub ' BindData


  ''' <summary>
  ''' Get all data 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GetScreenData()

    Me.JackpotViewer.Enabled = Me.chk_enabled.Checked
    Me.JackpotViewer.Name = Me.ef_viewer_name.Value
    Me.JackpotViewer.Code = CType(Me.ef_viewer_code.Value, Integer)
    Me.JackpotViewer.Jackpots = Me.m_included_jackpots
    Me.JackpotViewer.Jackpots.LastModification = WGDB.Now

  End Sub ' GetScreenData

  ''' <summary>
  ''' Rebind ListBox controls
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub RebindListBoxControls()

    ' Initialize controls
    Me.InitializeListBoxControls()

    ' Bind controls
    Me.BindListBoxControls()

    ' Refresh controls
    Me.RefreshListBoxControls()

  End Sub ' RebindListBoxControls

  ''' <summary>
  ''' Initialize ListBox DataSource
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitializeListBoxControls()

    Me.lb_available_jackpots.SuspendLayout()
    Me.lb_jackpot_viewer.SuspendLayout()

    Me.lb_available_jackpots.DataSource = Nothing
    Me.lb_jackpot_viewer.DataSource = Nothing

  End Sub ' InitializeListBoxControls

  ''' <summary>
  ''' To set the lists datasource
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub BindListBoxControls()
    Me.lb_available_jackpots.DataSource = Me.m_available_jackpots.Items
    Me.lb_available_jackpots.DisplayMember = LIST_DISPLAY_COLUMN
    Me.lb_available_jackpots.ValueMember = LIST_VALUE_COLUMN

    Me.lb_jackpot_viewer.DataSource = Me.m_included_jackpots.Items
    Me.lb_jackpot_viewer.DisplayMember = LIST_DISPLAY_COLUMN
    Me.lb_jackpot_viewer.ValueMember = LIST_VALUE_COLUMN

    ' Select last item if any item is selected
    Me.ListBoxLastItemSelectionManager(Me.lb_available_jackpots)
    Me.ListBoxLastItemSelectionManager(Me.lb_jackpot_viewer)

  End Sub ' BindListBoxControls

  ''' <summary>
  ''' Select last item if any item is selected
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ListBoxLastItemSelectionManager(ByRef ListBox As ListBox)

    If Not ListBox Is Nothing Then
      ' Required 1 item selected
      If ListBox.SelectedItem Is Nothing Then
        ListBox.SelectedIndex = ListBox.Items.Count - 1
      End If
    End If

  End Sub

  ''' <summary>
  ''' Refresh ListBox controls
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub RefreshListBoxControls()
    Me.lb_available_jackpots.Refresh()
    Me.lb_jackpot_viewer.Refresh()

  End Sub ' RefreshListBox

  ''' <summary>
  ''' To modify the list of jackpots included in the jackpot viewer
  ''' </summary>
  ''' <param name="Source"></param>
  ''' <param name="Destiny"></param>
  ''' <param name="Item"></param>
  ''' <remarks></remarks>
  Private Sub ModifyJackpotsInListView(ByVal Source As JackpotViewerItemList, ByVal Destiny As JackpotViewerItemList, ByVal Item As JackpotViewerItem)

    ' Check Jackpot IsoCode
    If Destiny.Items.Count > 0 AndAlso Not Destiny.Items(0).IsoCode.Equals(Item.IsoCode) Then
      ' Los jackpots han de tener la misma divisa para estar incluidos en el mismo viewer
      Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(591), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)

      Return
    End If

    ' Move Item
    If Not Item Is Nothing Then
      Destiny.Items.Add(Item)
      Source.Items.RemoveByJackpotId(Item.JackpotId)

      Me.RebindListBoxControls()

      Me.BindJackpotsGrid()

    End If

  End Sub

  ''' <summary>
  ''' Set Style to the Grid
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheet()

    ' Grid StyleSheet
    With Me.grid_viewer_jackpots

      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS, False)

      ' Jackpot order
      .Column(GRID_COLUMN_JACKPOT_ORDER).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(592)
      .Column(GRID_COLUMN_JACKPOT_ORDER).Header(1).Text = ""
      .Column(GRID_COLUMN_JACKPOT_ORDER).Width = GRID_WIDTH_JACKPOT_ORDER
      .Column(GRID_COLUMN_JACKPOT_ORDER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Jackpot name
      .Column(GRID_COLUMN_JACKPOT_NAME).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(282)
      .Column(GRID_COLUMN_JACKPOT_NAME).Header(1).Text = ""
      .Column(GRID_COLUMN_JACKPOT_NAME).Width = GRID_WIDTH_JACKPOT_NAME
      .Column(GRID_COLUMN_JACKPOT_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Jackpot status
      .Column(GRID_COLUMN_JACKPOT_STATUS).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(215)
      .Column(GRID_COLUMN_JACKPOT_STATUS).Header(1).Text = ""
      .Column(GRID_COLUMN_JACKPOT_STATUS).Width = GRID_WIDTH_JACKPOT_STATUS
      .Column(GRID_COLUMN_JACKPOT_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Jackpot main amount
      .Column(GRID_COLUMN_JACKPOT_MAIN_AMOUNT).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(593)
      .Column(GRID_COLUMN_JACKPOT_MAIN_AMOUNT).Header(1).Text = ""
      .Column(GRID_COLUMN_JACKPOT_MAIN_AMOUNT).Width = GRID_WIDTH_JACKPOT_MAIN_AMOUNT
      .Column(GRID_COLUMN_JACKPOT_MAIN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Terminal happy amount
      .Column(GRID_COLUMN_JACKPOT_HAPPY_AMOUNT).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(594)
      .Column(GRID_COLUMN_JACKPOT_HAPPY_AMOUNT).Header(1).Text = ""
      .Column(GRID_COLUMN_JACKPOT_HAPPY_AMOUNT).Width = GRID_WIDTH_JACKPOT_HAPPY_AMOUNT
      .Column(GRID_COLUMN_JACKPOT_HAPPY_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Terminal sharing amount
      .Column(GRID_COLUMN_JACKPOT_SHARING_AMOUNT).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(595)
      .Column(GRID_COLUMN_JACKPOT_SHARING_AMOUNT).Header(1).Text = ""
      .Column(GRID_COLUMN_JACKPOT_SHARING_AMOUNT).Width = GRID_WIDTH_JACKPOT_SHARING_AMOUNT
      .Column(GRID_COLUMN_JACKPOT_SHARING_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Terminal hidden amount
      .Column(GRID_COLUMN_JACKPOT_HIDDEN_AMOUNT).Header(0).Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(596)
      .Column(GRID_COLUMN_JACKPOT_HIDDEN_AMOUNT).Header(1).Text = ""
      .Column(GRID_COLUMN_JACKPOT_HIDDEN_AMOUNT).Width = GRID_WIDTH_JACKPOT_HIDDEN_AMOUNT
      .Column(GRID_COLUMN_JACKPOT_HIDDEN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With

  End Sub ' GUI_StyleSheet

  ''' <summary>
  ''' Sets the values of a row
  ''' </summary>
  ''' <param name="RowIndex"></param>
  ''' <param name="Item"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal Item As JackpotViewerItem) As Boolean
    Dim _result As Boolean

    _result = True

    With Me.grid_viewer_jackpots

      ' Jackpot order
      .Cell(RowIndex, GRID_COLUMN_JACKPOT_ORDER).Value = Item.Order

      ' Jackpot name
      .Cell(RowIndex, GRID_COLUMN_JACKPOT_NAME).Value = Item.Name

      ' Jackpot satus
      .Cell(RowIndex, GRID_COLUMN_JACKPOT_STATUS).Value = IIf(Item.Status = 0, GLB_NLS_GUI_JACKPOT_MGR.GetString(250), GLB_NLS_GUI_JACKPOT_MGR.GetString(218))

      ' Jackpot main
      .Cell(RowIndex, GRID_COLUMN_JACKPOT_MAIN_AMOUNT).Value = GUI_FormatCurrency(Item.Main)

      ' Jackpot happy
      .Cell(RowIndex, GRID_COLUMN_JACKPOT_HAPPY_AMOUNT).Value = GUI_FormatCurrency(Item.HappyHour)

      ' Jackpot sharing
      .Cell(RowIndex, GRID_COLUMN_JACKPOT_SHARING_AMOUNT).Value = GUI_FormatCurrency(Item.PrizeSharing)

      ' Jackpot hidden
      .Cell(RowIndex, GRID_COLUMN_JACKPOT_HIDDEN_AMOUNT).Value = GUI_FormatCurrency(Item.Hidden)

    End With

    Return _result

  End Function ' GUI_SetupRow

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub BindJackpotsGrid()

    ' Reorder list of items
    Me.m_included_jackpots.ReorderItems()

    If Not JackpotViewer.Jackpots Is Nothing Then

      Me.grid_viewer_jackpots.Clear()

      For Each _item As JackpotViewerItem In Me.m_included_jackpots.Items
        Call AddGridRow(_item)
      Next

    End If

  End Sub ' BindJackpotsGrid

  ''' <summary>
  ''' Add row into datagrid
  ''' </summary>
  ''' <param name="Item"></param>
  ''' <remarks></remarks>
  Private Sub AddGridRow(Item As JackpotViewerItem)
    Dim _count As Integer

    _count = Me.grid_viewer_jackpots.AddRow()

    Call GUI_SetupRow(_count, Item)

  End Sub ' AddGridRow

  ''' <summary>
  ''' Add or remove items between lists
  ''' </summary>
  ''' <param name="Type"></param>
  ''' <remarks></remarks>
  Private Sub AddRemoveJackpotToViewer(ByVal Type As ListBoxMovementType)

    Dim _jackpot_item As JackpotViewerItem
    Dim _source_items As JackpotViewerItemList
    Dim _destiny_items As JackpotViewerItemList
    Dim _selected_value As Object
    Dim _selected_item As Object

    _selected_item = Nothing
    _selected_value = Nothing
    _source_items = Nothing
    _destiny_items = Nothing

    Me.SetAddRemoveObjects(Type, _source_items, _destiny_items, _selected_item, _selected_value)

    If Not _selected_item Is Nothing Then
      _jackpot_item = _source_items.Items.GetJackpotViewerItemByJackpotId(_selected_value)
      Call Me.ModifyJackpotsInListView(_source_items, _destiny_items, _jackpot_item)
    End If

  End Sub ' AddRemoveJackpotToViewer

  ''' <summary>
  ''' Up or Down item in included Jackpots list
  ''' </summary>
  ''' <param name="Type"></param>
  ''' <remarks></remarks>
  Private Sub UpDownJackpotInViewer(ByVal Type As ListBoxMovementType)

    If Me.lb_jackpot_viewer.Items.Count > 0 Then

      ' Reorder items in controls
      Me.MoveUpDownJackpotsInListView(Type)

      ' Rebind list box controls
      Me.RebindListBoxControls()

      ' Rebind data grid
      Me.BindJackpotsGrid()

    End If

  End Sub ' AddRemoveJackpotToViewer

  ''' <summary>
  ''' Move up down jackpots in list view
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MoveUpDownJackpotsInListView(ByVal Type As ListBoxMovementType)
    Dim _selected_index As Object
    Dim _selected_item As JackpotViewerItem

    _selected_index = Me.lb_jackpot_viewer.SelectedIndex

    If (Type = ListBoxMovementType.Up AndAlso _selected_index = 0) OrElse _
      (Type = ListBoxMovementType.Down AndAlso _selected_index = Me.lb_jackpot_viewer.Items.Count - 1) Then
      Return
    End If

    _selected_item = Me.m_included_jackpots.Items(_selected_index)

    Me.m_included_jackpots.Items.Remove(_selected_item)

    Select Case Type
      Case ListBoxMovementType.Up
        _selected_index -= 1
      Case ListBoxMovementType.Down
        _selected_index += 1

    End Select

    Me.m_included_jackpots.Items.Insert(_selected_index, _selected_item)
    Me.lb_jackpot_viewer.SelectedIndex = _selected_index

  End Sub ' MoveUpDownJackpotsInListView

  ''' <summary>
  ''' Set objects to manage listboxes
  ''' </summary>
  ''' <param name="Type"></param>
  ''' <param name="SourceItems"></param>
  ''' <param name="DestinyItems"></param>
  ''' <param name="SelectedItem"></param>
  ''' <param name="SelectedValue"></param>
  ''' <remarks></remarks>
  Private Sub SetAddRemoveObjects(ByVal Type As ListBoxMovementType, ByRef SourceItems As JackpotViewerItemList, ByRef DestinyItems As JackpotViewerItemList, ByRef SelectedItem As Object, ByRef SelectedValue As Object)

    ' Set data to manage
    Select Case Type
      Case ListBoxMovementType.Add
        SelectedItem = Me.lb_available_jackpots.SelectedItem
        SelectedValue = Me.lb_available_jackpots.SelectedValue
        DestinyItems = Me.m_included_jackpots
        SourceItems = Me.m_available_jackpots

      Case ListBoxMovementType.Remove
        SelectedItem = Me.lb_jackpot_viewer.SelectedItem
        SelectedValue = Me.lb_jackpot_viewer.SelectedValue
        DestinyItems = Me.m_available_jackpots
        SourceItems = Me.m_included_jackpots
    End Select
  End Sub

#End Region

#Region "Events"

  Private Sub btn_add_Click(sender As Object, e As EventArgs) Handles btn_add.Click

    Me.AddRemoveJackpotToViewer(ListBoxMovementType.Add)

  End Sub

  Private Sub btn_remove_Click(sender As Object, e As EventArgs) Handles btn_remove.Click

    Me.AddRemoveJackpotToViewer(ListBoxMovementType.Remove)

  End Sub


  Private Sub btn_up_Click(sender As Object, e As EventArgs) Handles btn_up.Click

    Me.UpDownJackpotInViewer(ListBoxMovementType.Up)

  End Sub

  Private Sub btn_down_Click(sender As Object, e As EventArgs) Handles btn_down.Click
    Me.UpDownJackpotInViewer(ListBoxMovementType.Down)
  End Sub

#End Region

End Class
