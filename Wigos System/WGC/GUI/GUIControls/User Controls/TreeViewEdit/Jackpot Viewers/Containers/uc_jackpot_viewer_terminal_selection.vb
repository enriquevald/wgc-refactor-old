﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' UC NAME:       uc_jackpot_viewer_terminal_selection
' DESCRIPTION:   Jackpot User Control For Configuration Jackpot Viewers terminals
' AUTHOR:        Rubén Lama Ordóñez
' CREATION DATE: 26-MAY-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 26-MAY-2017  RLO    Initial version
'--------------------------------------------------------------------

Imports WSI.Common.Jackpot
Imports GUI_Controls
Imports WSI.Common
Imports GUI_CommonMisc.mdl_NLS
Imports GUI_CommonOperations

Public Class uc_jackpot_viewer_terminal_selection
  Inherits uc_treeview_edit_base

#Region " Constants "

  Private Const LIST_DISPLAY_COLUMN As String = "TerminalName"
  Private Const LIST_VALUE_COLUMN As String = "TerminalId"
  Private Const SQL_ISO_CODE_COLUMN As Integer = 10

#End Region ' Constants

#Region " Members "

  Private m_jackpot_viewer As JackpotViewer
  Private m_available_terminals As JackpotViewerTerminalList
  Private m_included_terminals As JackpotViewerTerminalList
  Private Const MAX_NAME_STRING_LENGHT = 50
  Private Const MAX_CODE_INT_LENGHT = 4

  Enum ListBoxMovementType
    Add = 0
    Remove = 1
    AddAll = 2
    RemoveAll = 3

  End Enum

#End Region

#Region "Properties"

  Public Property JackpotViewer As JackpotViewer
    Get
      Return Me.m_jackpot_viewer
    End Get
    Set(ByVal value As JackpotViewer)
      Me.m_jackpot_viewer = value
    End Set
  End Property

#End Region ' Properties

  Public Sub New(Optional ByVal JackpotViewer As JackpotViewer = Nothing)

    Me.JackpotViewer = JackpotViewer

    Call Init()
  End Sub

  Public Sub Init()
    ' This call is required by the Windows Form Designer.
    Call InitializeComponent()

    Call InitControls()
    Call BindData()
  End Sub

  Public Sub InitControls()

    'Name
    Me.lbl_jackpotViewer_name.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(560) & " :"

    'Group Box
    Me.gb_jackpots_viewer_terminals_sel.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8383)

    'Labels ListView
    Me.lb_available_terminals_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8313) ' Terminales disponibles
    Me.lb_jackpot_viewer_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8314) ' Terminales incluidos


  End Sub

#Region "Public Functions"

  Public Function IsScreenDataOk() As Boolean

    If Me.lb_jackpot_viewer.Items.Count = 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8343), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK) ' El viewer ha de tener como mínimo un terminal.
      Me.lb_jackpot_viewer.Focus()

      Return False
    End If

    Return True
  End Function ' IsScreenDataOk

#End Region

#Region "Private Functions"

  ''' <summary>
  ''' Get Jackpot Viewer configuration
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetJackpotViewerConfigTerminal() As JackpotViewer

    Me.GetScreenData()

    Return Me.JackpotViewer

  End Function ' GetJackpotViewerConfig

  ''' <summary>
  ''' To bind the data to de form
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub BindData()

    Me.lbl_jackpot_name.Text = Me.JackpotViewer.Name.ToString()

    Me.m_available_terminals = Me.JackpotViewer.GetAvailableTerminals
    Me.m_included_terminals = Me.JackpotViewer.GetIncludedTerminals

    Call Me.BindListBoxControls()



  End Sub ' BindData


  ''' <summary>
  ''' Get all data 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GetScreenData()

    Me.JackpotViewer.Terminals = Me.m_included_terminals
    Me.JackpotViewer.Terminals.LastModification = WGDB.Now

  End Sub ' GetScreenData

  ''' <summary>
  ''' Rebind ListBox controls
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub RebindListBoxControls()

    ' Suspend Layouts
    Me.SuspendLayoutListBoxControls()

    ' Initialize controls
    Me.InitializeListBoxControls()

    ' Bind controls
    Me.BindListBoxControls()

    ' Refresh controls
    Me.RefreshListBoxControls()

    ' Resume Layouts
    Me.ResumeLayoutListBoxControls()

  End Sub ' RebindListBoxControls

  ''' <summary>
  ''' Initialize ListBox DataSource
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitializeListBoxControls()

    Me.lb_available_jackpots.SuspendLayout()
    Me.lb_jackpot_viewer.SuspendLayout()

    Me.lb_available_jackpots.DataSource = Nothing
    Me.lb_jackpot_viewer.DataSource = Nothing

  End Sub ' InitializeListBoxControls

  ''' <summary>
  ''' To set the lists datasource
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub BindListBoxControls()
    Me.lb_available_jackpots.DataSource = Me.m_available_terminals.Items
    Me.lb_available_jackpots.DisplayMember = LIST_DISPLAY_COLUMN
    Me.lb_available_jackpots.ValueMember = LIST_VALUE_COLUMN

    Me.lb_jackpot_viewer.DataSource = Me.m_included_terminals.Items
    Me.lb_jackpot_viewer.DisplayMember = LIST_DISPLAY_COLUMN
    Me.lb_jackpot_viewer.ValueMember = LIST_VALUE_COLUMN

  End Sub ' BindListBoxControls

  ''' <summary>
  ''' Refresh ListBox controls
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub RefreshListBoxControls()
    Me.lb_available_jackpots.Refresh()
    Me.lb_jackpot_viewer.Refresh()

  End Sub ' RefreshListBox

  ''' <summary>
  ''' Suspend Layout ListBox controls
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SuspendLayoutListBoxControls()

    Me.lb_available_jackpots.SuspendLayout()
    Me.lb_jackpot_viewer.SuspendLayout()

  End Sub ' SuspendLayoutListBoxControls

  ''' <summary>
  ''' Resume Layout ListBox controls
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ResumeLayoutListBoxControls()

    Me.lb_available_jackpots.ResumeLayout()
    Me.lb_jackpot_viewer.ResumeLayout()

  End Sub ' ResumeLayoutListBoxControls

  ''' <summary>
  ''' To modify the list of jackpots included in the jackpot viewer
  ''' </summary>
  ''' <param name="Source"></param>
  ''' <param name="Destiny"></param>
  ''' <param name="Item"></param>
  ''' <remarks></remarks>
  Private Sub ModifyJackpotsInListView(ByVal Source As JackpotViewerTerminalList, ByVal Destiny As JackpotViewerTerminalList, ByVal Item As JackpotViewerTerminalItem)

    ' Move Item
    If Not Item Is Nothing Then
      Destiny.Items.Add(Item)
      Source.Items.RemoveByTerminalId(Item.TerminalId)

      Me.RebindListBoxControls()

    End If

  End Sub



  ''' <summary>
  ''' Add or remove items between lists
  ''' </summary>
  ''' <param name="Type"></param>
  ''' <remarks></remarks>
  Private Sub AddRemoveTerminalToViewer(ByVal Type As ListBoxMovementType)

    Dim _jackpot_item As JackpotViewerTerminalItem
    Dim _source_items As JackpotViewerTerminalList
    Dim _destiny_items As JackpotViewerTerminalList
    Dim _selected_value As Object
    Dim _selected_item As Object

    _selected_item = Nothing
    _selected_value = Nothing
    _source_items = Nothing
    _destiny_items = Nothing

    Me.SetAddRemoveObjects(Type, _source_items, _destiny_items, _selected_item, _selected_value)

    If Not _selected_item Is Nothing Then
      _jackpot_item = _source_items.Items.GetJackpotViewerItemByTerminalId(_selected_value)
      Call Me.ModifyJackpotsInListView(_source_items, _destiny_items, _jackpot_item)
    End If

  End Sub ' AddRemoveJackpotToViewer


  ''' <summary>
  ''' Set objects to manage listboxes
  ''' </summary>
  ''' <param name="Type"></param>
  ''' <param name="SourceItems"></param>
  ''' <param name="DestinyItems"></param>
  ''' <param name="SelectedItem"></param>
  ''' <param name="SelectedValue"></param>
  ''' <remarks></remarks>
  Private Sub SetAddRemoveObjects(ByVal Type As ListBoxMovementType, ByRef SourceItems As JackpotViewerTerminalList, ByRef DestinyItems As JackpotViewerTerminalList, ByRef SelectedItem As Object, ByRef SelectedValue As Object)

    ' Set data to manage
    Select Case Type
      Case ListBoxMovementType.Add
        SelectedItem = Me.lb_available_jackpots.SelectedItem
        SelectedValue = Me.lb_available_jackpots.SelectedValue
        DestinyItems = Me.m_included_terminals
        SourceItems = Me.m_available_terminals

      Case ListBoxMovementType.Remove
        SelectedItem = Me.lb_jackpot_viewer.SelectedItem
        SelectedValue = Me.lb_jackpot_viewer.SelectedValue
        DestinyItems = Me.m_available_terminals
        SourceItems = Me.m_included_terminals

      Case ListBoxMovementType.AddAll

      Case ListBoxMovementType.RemoveAll

    End Select
  End Sub

#End Region

#Region "Events"

  Private Sub btn_add_Click(sender As Object, e As EventArgs) Handles btn_add.Click

    Me.AddRemoveTerminalToViewer(ListBoxMovementType.Add)

  End Sub

  Private Sub btn_remove_Click(sender As Object, e As EventArgs) Handles btn_remove.Click

    Me.AddRemoveTerminalToViewer(ListBoxMovementType.Remove)

  End Sub
  Private Sub btn_add_all_Click(sender As Object, e As EventArgs) Handles btn_add_all.Click

  End Sub
  Private Sub btn_remove_all_Click(sender As Object, e As EventArgs) Handles btn_remove_all.Click

    Me.AddRemoveTerminalToViewer(ListBoxMovementType.Add)

  End Sub

#End Region



End Class
