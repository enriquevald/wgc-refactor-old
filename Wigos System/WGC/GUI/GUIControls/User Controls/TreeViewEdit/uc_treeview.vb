﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
' 
' MODULE NAME:   uc_treeview.vb  
'
' DESCRIPTION:   combo control
'
' AUTHOR:        Javier Barea
'
' CREATION DATE: 05-APR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 05-APR-2017  JBP    Initial version
'-------------------------------------------------------------------
Imports GUI_CommonMisc
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Drawing
Imports WSI.Common

Public Class uc_treeview
  Inherits TreeView

#Region " Constants "

  Public Const DEFAULT_TREEVIEW_ROOT_NAME = "Root"
  Public Const DEFAULT_TREEVIEW_ROOT_DESCRIPTION = "DEFAULT_DESCRIPTION"
  Public Const DEFAULT_TREEVIEW_NEW_ITEM_ID = -99
  Private Const DEFAULT_TREEVIEW_NODE_DESCRIPTION_PADDING = 5

#End Region

#Region " Members "

  Private m_root_description As String
  Private m_is_test_mode As Boolean
  Private m_treeview_item_list As List(Of TreeViewItem)
  Private m_icon_per_node As Boolean = False
  Private m_root_node As TreeNode
  Private m_new_treeview_item As TreeViewItem
  Private m_new_treenode As TreeNode

#End Region

#Region " Properties "

  Public Property RootNode() As TreeNode
    Get
      Return Me.m_root_node
    End Get
    Set(value As TreeNode)
      Me.m_root_node = value
    End Set
  End Property

  Public ReadOnly Property GetRootName() As String
    Get
      Return DEFAULT_TREEVIEW_ROOT_NAME
    End Get
  End Property

  Public ReadOnly Property GetNewItemName() As String
    Get
      Return DEFAULT_TREEVIEW_NEW_ITEM_ID
    End Get
  End Property

  <Category("IconPerNode"), Description("Icon per node"), DefaultValue("False")> _
  Public Property IconPerNode As Boolean
    Get
      Return Me.m_icon_per_node
    End Get
    Set(value As Boolean)
      Me.m_icon_per_node = value
    End Set
  End Property

  Public Property RootDescription() As String
    Get
      Return Me.m_root_description
    End Get

    Set(value As String)
      Me.m_root_description = value
    End Set
  End Property

  Protected Friend ReadOnly Property DefaultNodeFont() As Font
    Get
      Return Me.Parent.Font
    End Get
  End Property

  Protected Friend ReadOnly Property SelectedNodeFont() As Font
    Get
      Return New Font(Me.Font.FontFamily.Name, Me.Font.Size, FontStyle.Bold)
    End Get
  End Property

  Public Property ItemList As List(Of TreeViewItem)
    Get
      Return Me.m_treeview_item_list
    End Get
    Set(value As List(Of TreeViewItem))
      Me.m_treeview_item_list = value
    End Set
  End Property

#End Region

#Region "Public Functions"

  ''' <summary>
  ''' Bind tree view control
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub Bind(ByVal RootDescription As String, Optional ByVal RootType As EditSelectionItemType = EditSelectionItemType.NONE)
    Me.ResizeRedraw = True

    ' Set root description if it's necessary
    Me.SetRootDescription(RootDescription)

    Me.BindTreeView(RootType)
  End Sub ' Bind

  ''' <summary>
  ''' Add new node to treeview
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub AddNewNode(ByVal Description As String, ByVal Type As EditSelectionItemType)
    Me.AddNewNode(Me.GetNewItemName(), Description, Type)
  End Sub

  Public Sub AddNewNode(ByVal ObjectId As String, ByVal Description As String, ByVal Type As EditSelectionItemType)

    Me.m_new_treeview_item = New TreeViewItem(ObjectId, Description, Type, NodeStatus.Disabled)
    Me.m_new_treenode = Me.SetNewNode(ObjectId, Description, Type, NodeStatus.Disabled)

    ' Add new node
    Me.ItemList.Add(Me.m_new_treeview_item)
    Me.RootNode.Nodes.Add(Me.m_new_treenode)

    ' Select new node
    Me.SelectedNode = Me.RootNode.LastNode
  End Sub ' AddNewNode

  ''' <summary>
  ''' Remove new treeview node
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub RemoveNewNode()

    ' Check if selected node is new
    If Me.IsNewNode() Then
      ' Deleting New TreeNodeItem from ItemList
      Me.ItemList.Remove(Me.m_new_treeview_item)
      Me.m_new_treeview_item = Nothing

      ' Deleting new TreeNode from RootNode:
      '    - Me.IsNewNode() check's if selected node is equal than SelectedNode
      Me.SelectedNode.Remove()
      Me.m_new_treenode = Nothing

    End If

  End Sub ' RemoveNewNode

  ''' <summary>
  ''' Remove new treeview node
  ''' </summary>
  ''' <remarks></remarks>
  Public Function IsNewNode() As Boolean
    Return IsNewNode(Me.SelectedNode)
  End Function ' IsNewNode

  Public Function IsNewNode(ByVal TreeNode As TreeNode) As Boolean

    Return Me.TreeNodeIsEqual(Me.m_new_treenode, TreeNode)

  End Function ' IsNewNode

  ''' <summary>
  ''' Set main node of treeview
  ''' </summary>
  ''' <remarks></remarks>
  Public Function SetNewNode(ByVal Id As String, ByVal Description As String, ByVal Type As EditSelectionItemType, ByVal Status As NodeStatus) As TreeNode
    ' New tree node creation
    Return New TreeNode() With
    {
      .Name = Id,
      .Text = Me.SetNodeDescriptionFormat(Description),
      .Tag = Type,
      .ContextMenu = Nothing,
      .ForeColor = IIf(Status = NodeStatus.Enabled, SystemColors.WindowText, SystemColors.GrayText)
    }
  End Function ' SetNewNode

  ''' <summary>
  ''' Set node description format
  ''' </summary>
  ''' <param name="Description"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function SetNodeDescriptionFormat(ByVal Description As String) As String
    Return Description.PadRight(Description.Length + DEFAULT_TREEVIEW_NODE_DESCRIPTION_PADDING, " ")
  End Function ' SetNodeDescriptionFormat

  ''' <summary>
  ''' Set selected node status
  ''' </summary>
  ''' <param name="Status"></param>
  ''' <remarks></remarks>
  Public Sub SetSelectedNodeStatus(ByVal Status As NodeStatus)
    Me.SelectedNode.ForeColor = IIf(Status = NodeStatus.Enabled, SystemColors.WindowText, SystemColors.GrayText)
  End Sub ' SetSelectedNodeStatus

#End Region

#Region "Private Functions"

  ''' <summary>
  ''' Compare treenodes
  ''' </summary>
  ''' <param name="TreeNodeToCompare"></param>
  ''' <param name="TreeNode"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function TreeNodeIsEqual(ByVal TreeNode As TreeNode, ByVal TreeNodeToCompare As TreeNode) As Boolean

    ' If anyone is nothing...
    If TreeNode Is Nothing OrElse TreeNodeToCompare Is Nothing Then
      Return False
    End If

    ' Check if TreeNode properties is equal
    Return (TreeNodeToCompare.Name = TreeNode.Name And _
            TreeNodeToCompare.Text = TreeNode.Text And _
            TreeNodeToCompare.Tag = TreeNode.Tag)

  End Function

  ''' <summary>
  ''' Function that iterate a list of objects to show in a treeview
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub BindTreeView()
    ' Initialize TreeView nodes
    Me.InitializeTreeViewNodes()

    ' Bind TreeView
    Me.BindTreeViewNode(Me.ItemList, Me.RootNode)
    Me.RootNode.Expand()
    Me.Refresh()
  End Sub

  ''' <summary>
  ''' Bind treeview nodes
  ''' </summary>
  ''' <param name="RootType"></param>
  ''' <remarks></remarks>
  Private Sub BindTreeView(ByVal RootType As EditSelectionItemType)
    ' Set Root Node
    Me.RootNode = Me.SetRootNode(RootType)

    If Me.IconPerNode = False Then
      Me.ImageList = Nothing
    End If

    ' Bind TreeView
    Me.BindTreeView()
  End Sub

  ''' <summary>
  ''' Initialize TreeView nodes
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitializeTreeViewNodes()

    Me.Nodes.Clear()
    Me.RootNode.Nodes.Clear()

    Me.Nodes.Add(Me.RootNode)
  End Sub

  ''' <summary>
  ''' Function that iterate a list of objects to show in a treeview
  ''' </summary>
  ''' <param name="TreeViewList"></param>
  ''' <param name="ParentNode"></param>
  ''' <remarks></remarks>
  Private Sub BindTreeViewNode(ByVal TreeViewList As List(Of TreeViewItem), ByVal ParentNode As TreeNode)

    Dim _current_node As TreeNode

    For Each _item As TreeViewItem In TreeViewList
      _current_node = Me.SetNewNode(_item.Id.ToString(), _item.Description, _item.Type, _item.Status)

      If _item.Nodes.Count > 0 Then
        Me.BindTreeViewNode(_item.Nodes, _current_node)
      End If

      ParentNode.Nodes.Add(_current_node)
    Next

  End Sub ' BindTreeView

  ''' <summary>
  ''' Set root node of treeview
  ''' </summary>
  ''' <remarks></remarks>
  Private Function SetRootNode(ByVal RootType As EditSelectionItemType) As TreeNode
    Return Me.SetNewNode(Me.GetRootName(), Me.RootDescription, RootType, True)
  End Function ' SetRootNode

  ''' <summary>
  ''' Unselect all nodes
  ''' </summary>
  ''' <param name="ParentNode"></param>
  ''' <remarks></remarks>
  Private Sub SetDefaultFontToAllNodes(ByVal ParentNode As TreeNodeCollection)

    ' Set default font to all nodes
    For Each _node As TreeNode In ParentNode
      _node.NodeFont = Me.DefaultNodeFont
      Me.SetDefaultFontToAllNodes(_node.Nodes)
    Next

  End Sub ' SetDefaultFontToAllNodes

  ''' <summary>
  ''' Set root description
  ''' </summary>
  ''' <param name="RootDescription"></param>
  ''' <remarks></remarks>
  Private Sub SetRootDescription(ByVal RootDescription As String)

    ' Not first time
    If RootDescription = DEFAULT_TREEVIEW_ROOT_DESCRIPTION AndAlso Not String.IsNullOrEmpty(Me.RootDescription) Then
      Exit Sub
    End If

    ' Default Root description
    If String.IsNullOrEmpty(RootDescription) Then
      Me.RootDescription = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8065) ' Real default Root description
    Else
      Me.RootDescription = RootDescription
    End If
  End Sub

#End Region

#Region " Protected "

  ''' <summary>
  ''' Select specific node
  ''' </summary>
  ''' <param name="TreeNode"></param>
  ''' <remarks></remarks>
  Protected Friend Sub SelectNode(TreeNode As TreeNode)

    ' Reset font style to all nodes
    Me.SetDefaultFontToAllNodes(Me.Nodes)

    ' Set "Selected" Font to selected node. 
    TreeNode.NodeFont = Me.SelectedNodeFont

  End Sub ' SelectNode

#End Region

End Class
