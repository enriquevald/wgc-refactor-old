﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_jackpot_EGM_terminal_award
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(uc_jackpot_EGM_terminal_award))
    Me.gp_terminals_EGM = New System.Windows.Forms.GroupBox()
    Me.dg_providers = New GUI_Controls.uc_terminals_group_filter()
    Me.dg_providers.tr_group_list.Size = New Drawing.Size(254, 100)
    Me.rb_selected_terminals = New System.Windows.Forms.RadioButton()
    Me.rb_all_terminals = New System.Windows.Forms.RadioButton()
    Me.gp_terminals_EGM.SuspendLayout()
    Me.SuspendLayout()
    '
    'gp_terminals_EGM
    '
    Me.gp_terminals_EGM.Controls.Add(Me.dg_providers)
    Me.gp_terminals_EGM.Controls.Add(Me.rb_selected_terminals)
    Me.gp_terminals_EGM.Controls.Add(Me.rb_all_terminals)
    Me.gp_terminals_EGM.Location = New System.Drawing.Point(0, 0)
    Me.gp_terminals_EGM.Name = "gp_terminals_EGM"
    Me.gp_terminals_EGM.Size = New System.Drawing.Size(280, 210)
    Me.gp_terminals_EGM.TabIndex = 0
    Me.gp_terminals_EGM.TabStop = False
    Me.gp_terminals_EGM.Text = "GroupBox1"
    '
    'dg_providers
    '
    Me.dg_providers.ForbiddenGroups = CType(resources.GetObject("dg_providers.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.dg_providers.HeightOnExpanded = 50
    Me.dg_providers.Location = New System.Drawing.Point(7, 43)
    Me.dg_providers.MaximumSize = New System.Drawing.Size(267, 210)
    Me.dg_providers.Name = "dg_providers"
    Me.dg_providers.SelectedIndex = 0
    Me.dg_providers.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.NotCollapsedWithoutCollapseButton
    Me.dg_providers.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.dg_providers.ShowGroupBoxLine = True
    Me.dg_providers.ShowGroups = Nothing
    Me.dg_providers.ShowNodeAll = True
    Me.dg_providers.Size = New System.Drawing.Size(267, 161)
    Me.dg_providers.TabIndex = 2
    Me.dg_providers.ThreeStateCheckMode = True
    '
    'rb_selected_terminals
    '
    Me.rb_selected_terminals.AutoSize = True
    Me.rb_selected_terminals.Location = New System.Drawing.Point(163, 20)
    Me.rb_selected_terminals.Name = "rb_selected_terminals"
    Me.rb_selected_terminals.Size = New System.Drawing.Size(90, 17)
    Me.rb_selected_terminals.TabIndex = 1
    Me.rb_selected_terminals.TabStop = True
    Me.rb_selected_terminals.Text = "RadioButton1"
    Me.rb_selected_terminals.UseVisualStyleBackColor = True
    '
    'rb_all_terminals
    '
    Me.rb_all_terminals.AutoSize = True
    Me.rb_all_terminals.Location = New System.Drawing.Point(7, 20)
    Me.rb_all_terminals.Name = "rb_all_terminals"
    Me.rb_all_terminals.Size = New System.Drawing.Size(90, 17)
    Me.rb_all_terminals.TabIndex = 0
    Me.rb_all_terminals.TabStop = True
    Me.rb_all_terminals.Text = "RadioButton1"
    Me.rb_all_terminals.UseVisualStyleBackColor = True
    '
    'uc_jackpot_EGM_terminal_award
    '
    Me.Controls.Add(Me.gp_terminals_EGM)
    Me.Name = "uc_jackpot_EGM_terminal_award"
    Me.Size = New System.Drawing.Size(283, 212)
    Me.gp_terminals_EGM.ResumeLayout(False)
    Me.gp_terminals_EGM.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gp_terminals_EGM As System.Windows.Forms.GroupBox
  Friend WithEvents rb_selected_terminals As System.Windows.Forms.RadioButton
  Friend WithEvents rb_all_terminals As System.Windows.Forms.RadioButton
  Friend WithEvents dg_providers As GUI_Controls.uc_terminals_group_filter

End Class
