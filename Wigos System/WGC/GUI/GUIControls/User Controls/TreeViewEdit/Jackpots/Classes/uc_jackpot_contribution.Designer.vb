﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_jackpot_contribution
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(uc_jackpot_contribution))
    Me.pnl_week_contribution_percent = New System.Windows.Forms.Panel()
    Me.lbl_wednesday_contribution = New System.Windows.Forms.Label()
    Me.lbl_percentaje_sign = New System.Windows.Forms.Label()
    Me.ef_sunday_contribution = New GUI_Controls.uc_entry_field()
    Me.ef_monday_contribution = New GUI_Controls.uc_entry_field()
    Me.ef_saturday_contribution = New GUI_Controls.uc_entry_field()
    Me.ef_friday_contribution = New GUI_Controls.uc_entry_field()
    Me.ef_thursday_contribution = New GUI_Controls.uc_entry_field()
    Me.ef_wednesday_contribution = New GUI_Controls.uc_entry_field()
    Me.ef_tuesday_contribution = New GUI_Controls.uc_entry_field()
    Me.lbl_saturday_contribution = New System.Windows.Forms.Label()
    Me.lbl_friday_contribution = New System.Windows.Forms.Label()
    Me.lbl_thursday_contribution = New System.Windows.Forms.Label()
    Me.lbl_tuesday_contribution = New System.Windows.Forms.Label()
    Me.lbl_monday_contribution = New System.Windows.Forms.Label()
    Me.lbl_sunday_contribution = New System.Windows.Forms.Label()
    Me.lbl_all_contribution = New System.Windows.Forms.Label()
    Me.pnl_contribution_container = New System.Windows.Forms.Panel()
    Me.lbl_percentaje_sign_2 = New System.Windows.Forms.Label()
    Me.lbl_contribution = New System.Windows.Forms.Label()
    Me.cmb_contribution_type = New GUI_Controls.uc_combo()
    Me.ef_contribution_percent = New GUI_Controls.uc_entry_field()
    Me.dg_providers = New GUI_Controls.uc_terminals_group_filter()
    Me.pnl_delete_button = New System.Windows.Forms.Panel()
    Me.pnl_week_contribution_percent.SuspendLayout()
    Me.pnl_contribution_container.SuspendLayout()
    Me.SuspendLayout()
    '
    'pnl_week_contribution_percent
    '
    Me.pnl_week_contribution_percent.Controls.Add(Me.lbl_wednesday_contribution)
    Me.pnl_week_contribution_percent.Controls.Add(Me.lbl_percentaje_sign)
    Me.pnl_week_contribution_percent.Controls.Add(Me.ef_sunday_contribution)
    Me.pnl_week_contribution_percent.Controls.Add(Me.ef_monday_contribution)
    Me.pnl_week_contribution_percent.Controls.Add(Me.ef_saturday_contribution)
    Me.pnl_week_contribution_percent.Controls.Add(Me.ef_friday_contribution)
    Me.pnl_week_contribution_percent.Controls.Add(Me.ef_thursday_contribution)
    Me.pnl_week_contribution_percent.Controls.Add(Me.ef_wednesday_contribution)
    Me.pnl_week_contribution_percent.Controls.Add(Me.ef_tuesday_contribution)
    Me.pnl_week_contribution_percent.Controls.Add(Me.lbl_saturday_contribution)
    Me.pnl_week_contribution_percent.Controls.Add(Me.lbl_friday_contribution)
    Me.pnl_week_contribution_percent.Controls.Add(Me.lbl_thursday_contribution)
    Me.pnl_week_contribution_percent.Controls.Add(Me.lbl_tuesday_contribution)
    Me.pnl_week_contribution_percent.Controls.Add(Me.lbl_monday_contribution)
    Me.pnl_week_contribution_percent.Controls.Add(Me.lbl_sunday_contribution)
    Me.pnl_week_contribution_percent.Location = New System.Drawing.Point(114, 3)
    Me.pnl_week_contribution_percent.Name = "pnl_week_contribution_percent"
    Me.pnl_week_contribution_percent.Size = New System.Drawing.Size(483, 60)
    Me.pnl_week_contribution_percent.TabIndex = 1
    '
    'lbl_wednesday_contribution
    '
    Me.lbl_wednesday_contribution.BackColor = System.Drawing.Color.Transparent
    Me.lbl_wednesday_contribution.Location = New System.Drawing.Point(201, 12)
    Me.lbl_wednesday_contribution.Name = "lbl_wednesday_contribution"
    Me.lbl_wednesday_contribution.Size = New System.Drawing.Size(65, 13)
    Me.lbl_wednesday_contribution.TabIndex = 31
    Me.lbl_wednesday_contribution.Text = "xWednesday"
    Me.lbl_wednesday_contribution.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_percentaje_sign
    '
    Me.lbl_percentaje_sign.AutoSize = True
    Me.lbl_percentaje_sign.Location = New System.Drawing.Point(460, 32)
    Me.lbl_percentaje_sign.Name = "lbl_percentaje_sign"
    Me.lbl_percentaje_sign.Size = New System.Drawing.Size(15, 13)
    Me.lbl_percentaje_sign.TabIndex = 37
    Me.lbl_percentaje_sign.Text = "%"
    '
    'ef_sunday_contribution
    '
    Me.ef_sunday_contribution.DoubleValue = 0.0R
    Me.ef_sunday_contribution.IntegerValue = 0
    Me.ef_sunday_contribution.IsReadOnly = False
    Me.ef_sunday_contribution.Location = New System.Drawing.Point(6, 26)
    Me.ef_sunday_contribution.Name = "ef_sunday_contribution"
    Me.ef_sunday_contribution.PlaceHolder = Nothing
    Me.ef_sunday_contribution.Size = New System.Drawing.Size(65, 24)
    Me.ef_sunday_contribution.SufixText = "Sufix Text"
    Me.ef_sunday_contribution.SufixTextVisible = True
    Me.ef_sunday_contribution.TabIndex = 23
    Me.ef_sunday_contribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_sunday_contribution.TextValue = ""
    Me.ef_sunday_contribution.TextWidth = 0
    Me.ef_sunday_contribution.Value = ""
    Me.ef_sunday_contribution.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_monday_contribution
    '
    Me.ef_monday_contribution.DoubleValue = 0.0R
    Me.ef_monday_contribution.IntegerValue = 0
    Me.ef_monday_contribution.IsReadOnly = False
    Me.ef_monday_contribution.Location = New System.Drawing.Point(71, 26)
    Me.ef_monday_contribution.Name = "ef_monday_contribution"
    Me.ef_monday_contribution.PlaceHolder = Nothing
    Me.ef_monday_contribution.Size = New System.Drawing.Size(65, 24)
    Me.ef_monday_contribution.SufixText = "Sufix Text"
    Me.ef_monday_contribution.SufixTextVisible = True
    Me.ef_monday_contribution.TabIndex = 25
    Me.ef_monday_contribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_monday_contribution.TextValue = ""
    Me.ef_monday_contribution.TextWidth = 0
    Me.ef_monday_contribution.Value = ""
    Me.ef_monday_contribution.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_saturday_contribution
    '
    Me.ef_saturday_contribution.DoubleValue = 0.0R
    Me.ef_saturday_contribution.IntegerValue = 0
    Me.ef_saturday_contribution.IsReadOnly = False
    Me.ef_saturday_contribution.Location = New System.Drawing.Point(396, 26)
    Me.ef_saturday_contribution.Name = "ef_saturday_contribution"
    Me.ef_saturday_contribution.PlaceHolder = Nothing
    Me.ef_saturday_contribution.Size = New System.Drawing.Size(65, 24)
    Me.ef_saturday_contribution.SufixText = "Sufix Text"
    Me.ef_saturday_contribution.SufixTextVisible = True
    Me.ef_saturday_contribution.TabIndex = 33
    Me.ef_saturday_contribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_saturday_contribution.TextValue = ""
    Me.ef_saturday_contribution.TextWidth = 0
    Me.ef_saturday_contribution.Value = ""
    Me.ef_saturday_contribution.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_friday_contribution
    '
    Me.ef_friday_contribution.DoubleValue = 0.0R
    Me.ef_friday_contribution.IntegerValue = 0
    Me.ef_friday_contribution.IsReadOnly = False
    Me.ef_friday_contribution.Location = New System.Drawing.Point(331, 26)
    Me.ef_friday_contribution.Name = "ef_friday_contribution"
    Me.ef_friday_contribution.PlaceHolder = Nothing
    Me.ef_friday_contribution.Size = New System.Drawing.Size(65, 24)
    Me.ef_friday_contribution.SufixText = "Sufix Text"
    Me.ef_friday_contribution.SufixTextVisible = True
    Me.ef_friday_contribution.TabIndex = 32
    Me.ef_friday_contribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_friday_contribution.TextValue = ""
    Me.ef_friday_contribution.TextWidth = 0
    Me.ef_friday_contribution.Value = ""
    Me.ef_friday_contribution.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_thursday_contribution
    '
    Me.ef_thursday_contribution.DoubleValue = 0.0R
    Me.ef_thursday_contribution.IntegerValue = 0
    Me.ef_thursday_contribution.IsReadOnly = False
    Me.ef_thursday_contribution.Location = New System.Drawing.Point(266, 26)
    Me.ef_thursday_contribution.Name = "ef_thursday_contribution"
    Me.ef_thursday_contribution.PlaceHolder = Nothing
    Me.ef_thursday_contribution.Size = New System.Drawing.Size(65, 24)
    Me.ef_thursday_contribution.SufixText = "Sufix Text"
    Me.ef_thursday_contribution.SufixTextVisible = True
    Me.ef_thursday_contribution.TabIndex = 29
    Me.ef_thursday_contribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_thursday_contribution.TextValue = ""
    Me.ef_thursday_contribution.TextWidth = 0
    Me.ef_thursday_contribution.Value = ""
    Me.ef_thursday_contribution.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_wednesday_contribution
    '
    Me.ef_wednesday_contribution.DoubleValue = 0.0R
    Me.ef_wednesday_contribution.IntegerValue = 0
    Me.ef_wednesday_contribution.IsReadOnly = False
    Me.ef_wednesday_contribution.Location = New System.Drawing.Point(201, 26)
    Me.ef_wednesday_contribution.Name = "ef_wednesday_contribution"
    Me.ef_wednesday_contribution.PlaceHolder = Nothing
    Me.ef_wednesday_contribution.Size = New System.Drawing.Size(65, 24)
    Me.ef_wednesday_contribution.SufixText = "Sufix Text"
    Me.ef_wednesday_contribution.SufixTextVisible = True
    Me.ef_wednesday_contribution.TabIndex = 28
    Me.ef_wednesday_contribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_wednesday_contribution.TextValue = ""
    Me.ef_wednesday_contribution.TextWidth = 0
    Me.ef_wednesday_contribution.Value = ""
    Me.ef_wednesday_contribution.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_tuesday_contribution
    '
    Me.ef_tuesday_contribution.DoubleValue = 0.0R
    Me.ef_tuesday_contribution.IntegerValue = 0
    Me.ef_tuesday_contribution.IsReadOnly = False
    Me.ef_tuesday_contribution.Location = New System.Drawing.Point(136, 26)
    Me.ef_tuesday_contribution.Name = "ef_tuesday_contribution"
    Me.ef_tuesday_contribution.PlaceHolder = Nothing
    Me.ef_tuesday_contribution.Size = New System.Drawing.Size(65, 24)
    Me.ef_tuesday_contribution.SufixText = "Sufix Text"
    Me.ef_tuesday_contribution.SufixTextVisible = True
    Me.ef_tuesday_contribution.TabIndex = 27
    Me.ef_tuesday_contribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_tuesday_contribution.TextValue = ""
    Me.ef_tuesday_contribution.TextWidth = 0
    Me.ef_tuesday_contribution.Value = ""
    Me.ef_tuesday_contribution.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_saturday_contribution
    '
    Me.lbl_saturday_contribution.BackColor = System.Drawing.Color.Transparent
    Me.lbl_saturday_contribution.Location = New System.Drawing.Point(396, 12)
    Me.lbl_saturday_contribution.Name = "lbl_saturday_contribution"
    Me.lbl_saturday_contribution.Size = New System.Drawing.Size(65, 13)
    Me.lbl_saturday_contribution.TabIndex = 36
    Me.lbl_saturday_contribution.Text = "xSaturday"
    Me.lbl_saturday_contribution.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_friday_contribution
    '
    Me.lbl_friday_contribution.BackColor = System.Drawing.Color.Transparent
    Me.lbl_friday_contribution.Location = New System.Drawing.Point(331, 12)
    Me.lbl_friday_contribution.Name = "lbl_friday_contribution"
    Me.lbl_friday_contribution.Size = New System.Drawing.Size(65, 13)
    Me.lbl_friday_contribution.TabIndex = 35
    Me.lbl_friday_contribution.Text = "xFriday"
    Me.lbl_friday_contribution.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_thursday_contribution
    '
    Me.lbl_thursday_contribution.BackColor = System.Drawing.Color.Transparent
    Me.lbl_thursday_contribution.Location = New System.Drawing.Point(266, 12)
    Me.lbl_thursday_contribution.Name = "lbl_thursday_contribution"
    Me.lbl_thursday_contribution.Size = New System.Drawing.Size(65, 13)
    Me.lbl_thursday_contribution.TabIndex = 34
    Me.lbl_thursday_contribution.Text = "xThursday"
    Me.lbl_thursday_contribution.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_tuesday_contribution
    '
    Me.lbl_tuesday_contribution.BackColor = System.Drawing.Color.Transparent
    Me.lbl_tuesday_contribution.Location = New System.Drawing.Point(136, 12)
    Me.lbl_tuesday_contribution.Name = "lbl_tuesday_contribution"
    Me.lbl_tuesday_contribution.Size = New System.Drawing.Size(65, 13)
    Me.lbl_tuesday_contribution.TabIndex = 30
    Me.lbl_tuesday_contribution.Text = "xTuesday"
    Me.lbl_tuesday_contribution.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_monday_contribution
    '
    Me.lbl_monday_contribution.BackColor = System.Drawing.Color.Transparent
    Me.lbl_monday_contribution.Location = New System.Drawing.Point(71, 12)
    Me.lbl_monday_contribution.Name = "lbl_monday_contribution"
    Me.lbl_monday_contribution.Size = New System.Drawing.Size(65, 13)
    Me.lbl_monday_contribution.TabIndex = 26
    Me.lbl_monday_contribution.Text = "xMonday"
    Me.lbl_monday_contribution.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_sunday_contribution
    '
    Me.lbl_sunday_contribution.BackColor = System.Drawing.Color.Transparent
    Me.lbl_sunday_contribution.Location = New System.Drawing.Point(6, 12)
    Me.lbl_sunday_contribution.Name = "lbl_sunday_contribution"
    Me.lbl_sunday_contribution.Size = New System.Drawing.Size(65, 13)
    Me.lbl_sunday_contribution.TabIndex = 24
    Me.lbl_sunday_contribution.Text = "xSunday"
    Me.lbl_sunday_contribution.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_all_contribution
    '
    Me.lbl_all_contribution.BackColor = System.Drawing.Color.Transparent
    Me.lbl_all_contribution.Location = New System.Drawing.Point(120, 14)
    Me.lbl_all_contribution.Name = "lbl_all_contribution"
    Me.lbl_all_contribution.Size = New System.Drawing.Size(68, 12)
    Me.lbl_all_contribution.TabIndex = 25
    Me.lbl_all_contribution.Text = "x_all_contribution"
    Me.lbl_all_contribution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'pnl_contribution_container
    '
    Me.pnl_contribution_container.BackColor = System.Drawing.SystemColors.Control
    Me.pnl_contribution_container.Controls.Add(Me.lbl_percentaje_sign_2)
    Me.pnl_contribution_container.Controls.Add(Me.lbl_contribution)
    Me.pnl_contribution_container.Controls.Add(Me.cmb_contribution_type)
    Me.pnl_contribution_container.Controls.Add(Me.ef_contribution_percent)
    Me.pnl_contribution_container.Controls.Add(Me.dg_providers)
    Me.pnl_contribution_container.Controls.Add(Me.lbl_all_contribution)
    Me.pnl_contribution_container.Controls.Add(Me.pnl_week_contribution_percent)
    Me.pnl_contribution_container.Dock = System.Windows.Forms.DockStyle.Top
    Me.pnl_contribution_container.Location = New System.Drawing.Point(0, 0)
    Me.pnl_contribution_container.Name = "pnl_contribution_container"
    Me.pnl_contribution_container.Size = New System.Drawing.Size(597, 279)
    Me.pnl_contribution_container.TabIndex = 0
    '
    'lbl_percentaje_sign_2
    '
    Me.lbl_percentaje_sign_2.AutoSize = True
    Me.lbl_percentaje_sign_2.Location = New System.Drawing.Point(185, 35)
    Me.lbl_percentaje_sign_2.Name = "lbl_percentaje_sign_2"
    Me.lbl_percentaje_sign_2.Size = New System.Drawing.Size(15, 13)
    Me.lbl_percentaje_sign_2.TabIndex = 38
    Me.lbl_percentaje_sign_2.Text = "%"
    '
    'lbl_contribution
    '
    Me.lbl_contribution.BackColor = System.Drawing.Color.Transparent
    Me.lbl_contribution.Location = New System.Drawing.Point(3, 16)
    Me.lbl_contribution.Name = "lbl_contribution"
    Me.lbl_contribution.Size = New System.Drawing.Size(102, 12)
    Me.lbl_contribution.TabIndex = 0
    Me.lbl_contribution.Text = "xContribution"
    Me.lbl_contribution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'cmb_contribution_type
    '
    Me.cmb_contribution_type.AllowUnlistedValues = False
    Me.cmb_contribution_type.AutoCompleteMode = False
    Me.cmb_contribution_type.IsReadOnly = False
    Me.cmb_contribution_type.Location = New System.Drawing.Point(0, 29)
    Me.cmb_contribution_type.Name = "cmb_contribution_type"
    Me.cmb_contribution_type.SelectedIndex = -1
    Me.cmb_contribution_type.Size = New System.Drawing.Size(105, 24)
    Me.cmb_contribution_type.SufixText = "Sufix Text"
    Me.cmb_contribution_type.SufixTextVisible = True
    Me.cmb_contribution_type.TabIndex = 1
    Me.cmb_contribution_type.TextCombo = Nothing
    Me.cmb_contribution_type.TextWidth = 0
    '
    'ef_contribution_percent
    '
    Me.ef_contribution_percent.DoubleValue = 0.0R
    Me.ef_contribution_percent.IntegerValue = 0
    Me.ef_contribution_percent.IsReadOnly = False
    Me.ef_contribution_percent.Location = New System.Drawing.Point(120, 29)
    Me.ef_contribution_percent.Name = "ef_contribution_percent"
    Me.ef_contribution_percent.PlaceHolder = Nothing
    Me.ef_contribution_percent.Size = New System.Drawing.Size(65, 24)
    Me.ef_contribution_percent.SufixText = "Sufix Text"
    Me.ef_contribution_percent.SufixTextVisible = True
    Me.ef_contribution_percent.TabIndex = 2
    Me.ef_contribution_percent.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_contribution_percent.TextValue = ""
    Me.ef_contribution_percent.TextWidth = 0
    Me.ef_contribution_percent.Value = ""
    Me.ef_contribution_percent.ValueForeColor = System.Drawing.Color.Blue
    '
    'dg_providers
    '
    Me.dg_providers.ForbiddenGroups = CType(resources.GetObject("dg_providers.ForbiddenGroups"), Microsoft.VisualBasic.Collection)
    Me.dg_providers.HeightOnExpanded = 300
    Me.dg_providers.Location = New System.Drawing.Point(3, 60)
    Me.dg_providers.Name = "dg_providers"
    Me.dg_providers.SelectedIndex = 0
    Me.dg_providers.SetDisplayMode = GUI_Controls.uc_terminals_group_filter.DisplayMode.NotCollapsedWithoutCollapseButton
    Me.dg_providers.SetInitMode = GUI_Controls.uc_terminals_group_filter.InitMode.NormalMode
    Me.dg_providers.ShowGroupBoxLine = True
    Me.dg_providers.ShowGroups = Nothing
    Me.dg_providers.ShowNodeAll = True
    Me.dg_providers.Size = New System.Drawing.Size(591, 211)
    Me.dg_providers.TabIndex = 4
    Me.dg_providers.ThreeStateCheckMode = True
    '
    'pnl_delete_button
    '
    Me.pnl_delete_button.BackColor = System.Drawing.SystemColors.Control
    Me.pnl_delete_button.Dock = System.Windows.Forms.DockStyle.Fill
    Me.pnl_delete_button.Location = New System.Drawing.Point(0, 279)
    Me.pnl_delete_button.MinimumSize = New System.Drawing.Size(527, 38)
    Me.pnl_delete_button.Name = "pnl_delete_button"
    Me.pnl_delete_button.Size = New System.Drawing.Size(597, 38)
    Me.pnl_delete_button.TabIndex = 3
    '
    'uc_jackpot_contribution
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
    Me.Controls.Add(Me.pnl_delete_button)
    Me.Controls.Add(Me.pnl_contribution_container)
    Me.Name = "uc_jackpot_contribution"
    Me.Size = New System.Drawing.Size(597, 315)
    Me.pnl_week_contribution_percent.ResumeLayout(False)
    Me.pnl_week_contribution_percent.PerformLayout()
    Me.pnl_contribution_container.ResumeLayout(False)
    Me.pnl_contribution_container.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents pnl_week_contribution_percent As System.Windows.Forms.Panel
  Friend WithEvents lbl_wednesday_contribution As System.Windows.Forms.Label
  Friend WithEvents lbl_percentaje_sign As System.Windows.Forms.Label
  Friend WithEvents ef_saturday_contribution As GUI_Controls.uc_entry_field
  Friend WithEvents ef_friday_contribution As GUI_Controls.uc_entry_field
  Friend WithEvents ef_thursday_contribution As GUI_Controls.uc_entry_field
  Friend WithEvents ef_wednesday_contribution As GUI_Controls.uc_entry_field
  Friend WithEvents ef_tuesday_contribution As GUI_Controls.uc_entry_field
  Friend WithEvents ef_monday_contribution As GUI_Controls.uc_entry_field
  Friend WithEvents ef_sunday_contribution As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_saturday_contribution As System.Windows.Forms.Label
  Friend WithEvents lbl_friday_contribution As System.Windows.Forms.Label
  Friend WithEvents lbl_thursday_contribution As System.Windows.Forms.Label
  Friend WithEvents lbl_tuesday_contribution As System.Windows.Forms.Label
  Friend WithEvents lbl_monday_contribution As System.Windows.Forms.Label
  Friend WithEvents lbl_sunday_contribution As System.Windows.Forms.Label
  Friend WithEvents lbl_all_contribution As System.Windows.Forms.Label
  Friend WithEvents pnl_contribution_container As System.Windows.Forms.Panel
  Friend WithEvents pnl_delete_button As System.Windows.Forms.Panel
  Friend WithEvents ef_contribution_percent As GUI_Controls.uc_entry_field
  Friend WithEvents dg_providers As GUI_Controls.uc_terminals_group_filter
  Friend WithEvents cmb_contribution_type As GUI_Controls.uc_combo
  Friend WithEvents lbl_contribution As System.Windows.Forms.Label
  Friend WithEvents lbl_percentaje_sign_2 As System.Windows.Forms.Label

End Class
