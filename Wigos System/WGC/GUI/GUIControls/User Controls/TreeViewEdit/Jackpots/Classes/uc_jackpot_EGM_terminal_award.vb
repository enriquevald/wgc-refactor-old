﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' UC NAME:       uc_jackpot_EGM_terminal_award
' DESCRIPTION:   Jackpot User Control for jackpot EGM terminal award
' AUTHOR:        Ferran Ortner
' CREATION DATE: 31-MAY-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 31-MAY-2017  FOS    Initial version
'--------------------------------------------------------------------
Imports WSI.Common
Imports WSI.Common.Jackpot
Imports System.Text
Imports System.Xml
Imports System.Drawing

Public Class uc_jackpot_EGM_terminal_award

  Private m_providers As StringBuilder
  Private m_zone As StringBuilder
  Private m_group As StringBuilder
  Private m_area As StringBuilder
  Private m_terminal As StringBuilder
  Private m_bank As StringBuilder
  Private m_type_list As StringBuilder
  Private m_excluded As Integer

  Private m_is_selected_egm_type As Int32
  Private m_jackpot_contribution_list As JackpotContributionList
  Private m_award_filter As JackpotAwardFilter
  Private m_all_contributing_terminals As StringBuilder

  Public Property IsSelectedEGMType() As Boolean
    Get
      Return m_is_selected_egm_type
    End Get
    Set(ByVal value As Boolean)
      m_is_selected_egm_type = value
    End Set
  End Property


  Public Sub InitControls()

    rb_all_terminals.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(599)
    rb_selected_terminals.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(600)
    gp_terminals_EGM.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(598)

  End Sub

  Public Sub BindData(ByVal JackpotContributionsList As JackpotContributionList, ByVal AwardFilter As JackpotAwardFilter)
   

    m_jackpot_contribution_list = New JackpotContributionList
    m_award_filter = New JackpotAwardFilter

    m_jackpot_contribution_list = JackpotContributionsList
    m_award_filter = AwardFilter

   
    Dim _terminal_list As TerminalList
    Dim _terminal_list_xml As StringBuilder
    Dim _contribution As JackpotContribution
    _terminal_list = New TerminalList()
    _terminal_list_xml = New StringBuilder
    _contribution = New JackpotContribution

    rb_all_terminals.Checked = True

    If AwardFilter.SelectionEGMType = JackpotAwardFilter.JackpotSelectionEGMType.All Then
      rb_all_terminals.Checked = True
      rb_selected_terminals.Checked = False

      For i As Integer = 0 To JackpotContributionsList.Items.Count - 1
        _contribution = JackpotContributionsList.Items(i)

        If Not String.IsNullOrEmpty(_contribution.Terminals) Then
          CreateUniqueTerminalListXml(_contribution.Terminals)
        End If

      Next

      _terminal_list_xml.Append("<ProviderList>")
      _terminal_list_xml.Append(m_type_list)
      _terminal_list_xml.Append(m_providers)
      _terminal_list_xml.Append(m_group)
      _terminal_list_xml.Append(m_zone)
      _terminal_list_xml.Append(m_area)
      _terminal_list_xml.Append(m_terminal)
      _terminal_list_xml.Append(m_bank)
      _terminal_list_xml.Append("</ProviderList>")


      If Not _terminal_list_xml.ToString().Equals("<ProviderList></ProviderList>") Then
        _terminal_list.FromXml(_terminal_list_xml.ToString())
        m_all_contributing_terminals = _terminal_list_xml
      Else
        _terminal_list.FromXml(String.Empty)
        _terminal_list_xml.ToString()
      End If

      m_all_contributing_terminals = _terminal_list_xml

      Me.dg_providers.ListSize = New Drawing.Size(254, 95)
      Me.dg_providers.ComboProviderListTypeEnabled = False
      Me.dg_providers.ListEnabled = True
      Me.dg_providers.Ignoreclick = True

    Else
      rb_all_terminals.Checked = False
      rb_selected_terminals.Checked = True
      
      Me.dg_providers.ListSize = New Drawing.Size(254, 95)
      Me.dg_providers.ComboProviderListTypeEnabled = True
      Me.dg_providers.ListEnabled = False
      Me.dg_providers.Ignoreclick = False

      _terminal_list.FromXml(AwardFilter.SelectedEGM)
    End If



    Me.dg_providers.SetTerminalList(_terminal_list)

  End Sub

  Private Sub CreateUniqueTerminalListXml(ByVal TerminalListXml)
    Dim _list As XmlDocument

    _list = New XmlDocument

    _list.LoadXml(TerminalListXml)

    Call InitializeXmlVariables()


    For Each _node As XmlNode In _list.SelectNodes("ProviderList")

      Call GetListTypeXmlValues(_node)

      'Call GetProvidersXmlValues(_node)
      Call GetXmlValues(_node, "Provider", m_providers)

      'Call GetZonesXmlValues(_node)
      Call GetXmlValues(_node, "Zone", m_zone)

      'Call GetGroupXmlValues(_node)
      Call GetXmlValues(_node, "Group", m_group)

      'Call GetXmlValues(_node)
      Call GetXmlValues(_node, "Area", m_area)

      'Call GetTerminalXmlValues(_node)
      Call GetXmlValues(_node, "Terminal", m_terminal)

      'Call GetBankXmlValues(_node)
      Call GetXmlValues(_node, "Bank", m_bank)
    Next
  End Sub

  Private Sub InitializeXmlVariables()

    If m_providers Is Nothing Then
      m_providers = New StringBuilder
    End If

    If m_zone Is Nothing Then
      m_zone = New StringBuilder
    End If

    If m_group Is Nothing Then
      m_group = New StringBuilder
    End If

    If m_type_list Is Nothing Then
      m_type_list = New StringBuilder
    End If

    If m_area Is Nothing Then
      m_area = New StringBuilder
    End If

    If m_terminal Is Nothing Then
      m_terminal = New StringBuilder
    End If

    If m_bank Is Nothing Then
      m_bank = New StringBuilder
    End If

  End Sub

  Private Sub GetListTypeXmlValues(ByVal Node As XmlNode)

    If Node.SelectNodes("ListType").Count > 0 Then

      For Each _sub_node As XmlNode In Node.SelectNodes("ListType")
        If _sub_node.InnerXml.Contains("1") Then
          If m_excluded > 0 Then
            Return
          End If
        Else
          m_excluded += 1
          If m_excluded = 1 Then
            m_providers = New StringBuilder
            m_zone = New StringBuilder
            m_group = New StringBuilder
            m_area = New StringBuilder
            m_type_list = New StringBuilder
          End If
        End If
        If String.IsNullOrEmpty(m_type_list.ToString()) Then
          m_type_list.Append("<ListType>")
          m_type_list.Append(_sub_node.InnerXml.ToString())
          m_type_list.Append("</ListType>")
        End If
      Next
    End If
  End Sub

  Private Sub GetXmlValues(ByVal Node As XmlNode, ByVal Name As String, ByVal StringBuilderNode As StringBuilder)
    Dim _tag_name As String
    Dim _end_tag_name As String

    _tag_name = "<" & Name & ">"
    _end_tag_name = "</" & Name & ">"

    If Node.SelectNodes(Name).Count > 0 Then
      For Each _sub_node As XmlNode In Node.SelectNodes(Name)
        If Not m_group.ToString().Contains(_sub_node.InnerXml.ToString()) Then
          StringBuilderNode.Append(_tag_name)
          StringBuilderNode.Append(_sub_node.InnerXml.ToString())
          StringBuilderNode.Append(_end_tag_name)
        End If
      Next

    End If


  End Sub


#Region "TODO - Eliminado"
  'Private Sub GetTerminalXmlValues(ByVal Node As XmlNode)


  '  If Node.SelectNodes("Terminal").Count > 0 Then
  '    For Each _sub_node As XmlNode In Node.SelectNodes("Terminal")
  '      If Not m_area.ToString().Contains(_sub_node.InnerXml.ToString()) Then
  '        m_area.Append("<Terminal>")
  '        m_area.Append(_sub_node.InnerXml.ToString())
  '        m_area.Append("</Terminal>")
  '      End If
  '    Next

  '  End If
  'End Sub

  'Private Sub GetProvidersXmlValues(ByVal Node As XmlNode)

  '  If Node.SelectNodes("Provider").Count > 0 Then
  '    For Each _sub_node As XmlNode In Node.SelectNodes("Provider")
  '      If Not m_providers.ToString().Contains(_sub_node.InnerXml.ToString()) Then
  '        m_providers.Append("<Provider>")
  '        m_providers.Append(_sub_node.InnerXml.ToString())
  '        m_providers.Append("</Provider>")
  '      End If
  '    Next

  '  End If
  'End Sub

  'Private Sub GetZonesXmlValues(ByVal Node As XmlNode)

  '  If Node.SelectNodes("Zone").Count > 0 Then
  '    For Each _sub_node As XmlNode In Node.SelectNodes("Zone")
  '      If Not m_zone.ToString().Contains(_sub_node.InnerXml.ToString()) Then
  '        m_zone.Append("<Zone>")
  '        m_zone.Append(_sub_node.InnerXml.ToString())
  '        m_zone.Append("</Zone>")
  '      End If
  '    Next

  '  End If
  'End Sub

  'Private Sub GetGroupXmlValues(ByVal Node As XmlNode)

  '  If Node.SelectNodes("Group").Count > 0 Then
  '    For Each _sub_node As XmlNode In Node.SelectNodes("Group")
  '      If Not m_group.ToString().Contains(_sub_node.InnerXml.ToString()) Then
  '        m_group.Append("<Group>")
  '        m_group.Append(_sub_node.InnerXml.ToString())
  '        m_group.Append("</Group>")
  '      End If
  '    Next

  '  End If
  'End Sub

  'Private Sub GetAreaXmlValues(ByVal Node As XmlNode)

  '  If Node.SelectNodes("Area").Count > 0 Then
  '    For Each _sub_node As XmlNode In Node.SelectNodes("Area")
  '      If Not m_area.ToString().Contains(_sub_node.InnerXml.ToString()) Then
  '        m_area.Append("<Area>")
  '        m_area.Append(_sub_node.InnerXml.ToString())
  '        m_area.Append("</Area>")
  '      End If
  '    Next

  '  End If
  'End Sub

  'Private Sub GetBankXmlValues(ByVal Node As XmlNode)

  '  If Node.SelectNodes("Bank").Count > 0 Then
  '    For Each _sub_node As XmlNode In Node.SelectNodes("Bank")
  '      If Not m_area.ToString().Contains(_sub_node.InnerXml.ToString()) Then
  '        m_area.Append("<Bank>")
  '        m_area.Append(_sub_node.InnerXml.ToString())
  '        m_area.Append("</Bank>")
  '      End If
  '    Next

  '  End If
  'End Sub
#End Region

  Private Sub rb_all_terminals_CheckedChanged(sender As Object, e As EventArgs) Handles rb_all_terminals.CheckedChanged
    m_is_selected_egm_type = False
    Call RefreshComboValues()
  End Sub

  Private Sub rb_selected_terminals_CheckedChanged(sender As Object, e As EventArgs) Handles rb_selected_terminals.CheckedChanged
    m_is_selected_egm_type = True
    Call RefreshComboValues()
  End Sub

  Private Sub RefreshComboValues()
    Dim _terminal_list As TerminalList
    _terminal_list = New TerminalList()

    Me.dg_providers.tr_group_list.CollapseAll()

    If rb_selected_terminals.Checked Then


      _terminal_list.FromXml("")

      
      Me.dg_providers.ListSize = New Drawing.Size(254, 95)
      Me.dg_providers.ComboProviderListTypeEnabled = True
      Me.dg_providers.ListEnabled = False
      Me.dg_providers.Ignoreclick = False
      Me.dg_providers.SetTerminalList(_terminal_list)
      Me.dg_providers.Refresh()

    Else
      Me.dg_providers.ListSize = New Drawing.Size(254, 95)
      Me.dg_providers.ComboProviderListTypeEnabled = False
      Me.dg_providers.ListEnabled = True
      Me.dg_providers.Ignoreclick = True
      If m_all_contributing_terminals IsNot Nothing Then
        _terminal_list.FromXml(m_all_contributing_terminals.ToString())
      Else
        _terminal_list.FromXml(String.Empty)
      End If

      Me.dg_providers.SetTerminalList(_terminal_list)
      Me.dg_providers.Refresh()
      End If

  End Sub
End Class
