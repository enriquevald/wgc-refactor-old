﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_jackpot_flags
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_by_flags = New System.Windows.Forms.GroupBox()
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.chk_by_flag = New System.Windows.Forms.CheckBox()
    Me.btn_accounts_afected = New System.Windows.Forms.Button()
    Me.dg_flags = New GUI_Controls.uc_grid()
    Me.gb_by_flags.SuspendLayout()
    Me.Panel1.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_by_flags
    '
    Me.gb_by_flags.Controls.Add(Me.Panel1)
    Me.gb_by_flags.Controls.Add(Me.dg_flags)
    Me.gb_by_flags.Location = New System.Drawing.Point(0, 0)
    Me.gb_by_flags.Name = "gb_by_flags"
    Me.gb_by_flags.Size = New System.Drawing.Size(279, 157)
    Me.gb_by_flags.TabIndex = 0
    Me.gb_by_flags.TabStop = False
    Me.gb_by_flags.Text = " xByFlags"
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.chk_by_flag)
    Me.Panel1.Controls.Add(Me.btn_accounts_afected)
    Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
    Me.Panel1.Location = New System.Drawing.Point(3, 16)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(273, 25)
    Me.Panel1.TabIndex = 0
    '
    'chk_by_flag
    '
    Me.chk_by_flag.AutoSize = True
    Me.chk_by_flag.Location = New System.Drawing.Point(5, 5)
    Me.chk_by_flag.Name = "chk_by_flag"
    Me.chk_by_flag.Size = New System.Drawing.Size(61, 17)
    Me.chk_by_flag.TabIndex = 0
    Me.chk_by_flag.Text = "xActive"
    Me.chk_by_flag.UseVisualStyleBackColor = True
    '
    'btn_accounts_afected
    '
    Me.btn_accounts_afected.Location = New System.Drawing.Point(131, 3)
    Me.btn_accounts_afected.Name = "btn_accounts_afected"
    Me.btn_accounts_afected.Size = New System.Drawing.Size(135, 19)
    Me.btn_accounts_afected.TabIndex = 1
    Me.btn_accounts_afected.Text = "Cuentas Afectadas"
    Me.btn_accounts_afected.UseVisualStyleBackColor = True
    '
    'dg_flags
    '
    Me.dg_flags.CurrentCol = -1
    Me.dg_flags.CurrentRow = -1
    Me.dg_flags.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_flags.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_flags.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_flags.Location = New System.Drawing.Point(6, 44)
    Me.dg_flags.Name = "dg_flags"
    Me.dg_flags.PanelRightVisible = False
    Me.dg_flags.Redraw = True
    Me.dg_flags.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_flags.Size = New System.Drawing.Size(266, 107)
    Me.dg_flags.Sortable = False
    Me.dg_flags.SortAscending = True
    Me.dg_flags.SortByCol = 0
    Me.dg_flags.TabIndex = 1
    Me.dg_flags.ToolTipped = True
    Me.dg_flags.TopRow = -2
    Me.dg_flags.WordWrap = False
    '
    'uc_jackpot_flags
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
    Me.Controls.Add(Me.gb_by_flags)
    Me.Name = "uc_jackpot_flags"
    Me.Size = New System.Drawing.Size(285, 163)
    Me.gb_by_flags.ResumeLayout(False)
    Me.Panel1.ResumeLayout(False)
    Me.Panel1.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_by_flags As System.Windows.Forms.GroupBox
  Friend WithEvents dg_flags As GUI_Controls.uc_grid
  Friend WithEvents chk_by_flag As System.Windows.Forms.CheckBox
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
  Public WithEvents btn_accounts_afected As System.Windows.Forms.Button

End Class
