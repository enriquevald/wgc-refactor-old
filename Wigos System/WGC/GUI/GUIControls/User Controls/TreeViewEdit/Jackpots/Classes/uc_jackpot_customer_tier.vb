﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' UC NAME:       uc_jackpot_customer_tier
' DESCRIPTION:   Jackpot User Control For customer tier selection
' AUTHOR:        Carlos Cáceres González
' CREATION DATE: 28-ABR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 28-ABR-2017  CCG    Initial version
'--------------------------------------------------------------------

Imports GUI_CommonOperations
Imports System.Windows.Forms
Imports System.Globalization
Imports WSI.Common.Jackpot
Imports GUI_CommonMisc
Imports WSI.Common

Public Class uc_jackpot_customer_tier

#Region " Enums "

#End Region ' Enums 

#Region " Constants "

  Private Const DEFAULT_VIP_VALUE = Jackpot.AwardCustomerVipType.All

#End Region

#Region " Members "

  Private m_jackpot_award_customer_tier_object As JackpotAwardCustomerTier

#End Region

#Region "Properties"

  Public Property AwardCustomerTier As JackpotAwardCustomerTier
    Get
      Return Me.m_jackpot_award_customer_tier_object
    End Get
    Set(value As JackpotAwardCustomerTier)
      Me.m_jackpot_award_customer_tier_object = value
    End Set
  End Property


#End Region

  Public Sub New()

    Call Init()

  End Sub


#Region "Public Sub"

  ''' <summary>
  '''  Map controls to JackpotAwardCustomerTier
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub BindData(ByVal AwardCustomerTier As JackpotAwardCustomerTier)

    ' Bind all data to controls
    If Not AwardCustomerTier Is Nothing Then

      Me.AwardCustomerTier = AwardCustomerTier
      Call SelectCustomerLevels(AwardCustomerTier)
      Call SetSelectedVIP(AwardCustomerTier.Vip)

    End If

  End Sub

  ''' <summary>
  ''' Convert form data to JackpotAwardCustomerTier
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ToJackpotAwardCustomerTier() As JackpotAwardCustomerTier

    Me.AwardCustomerTier.ByLevel = Me.chk_by_level.Checked
    Me.AwardCustomerTier.Level1 = Me.chk_level_01.Checked
    Me.AwardCustomerTier.Level2 = Me.chk_level_02.Checked
    Me.AwardCustomerTier.Level3 = Me.chk_level_03.Checked
    Me.AwardCustomerTier.Level4 = Me.chk_level_04.Checked
    Me.AwardCustomerTier.Anonymous = Me.chk_level_anonymous.Checked
    Me.AwardCustomerTier.Vip = GetSelectedVIP()

    ' Set last modification
    Me.AwardCustomerTier.LastModification = WGDB.Now

    Return Me.AwardCustomerTier

  End Function

  ''' <summary>
  ''' To check if screen data is ok
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function IsScreenDataOk() As Boolean

    If Not CheckLevels() Then

      Return False
    End If

    Return True
  End Function

#End Region

#Region "Private Sub"

  ''' <summary>
  ''' To init GUI components
  ''' </summary>
  ''' <remarks></remarks>
  Protected Sub Init()

    ' This call is required by the designer.
    Call InitializeComponent()

    ' Initialize controls
    Call InitControls()

  End Sub

  ''' <summary>
  ''' To init components
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub InitControls()
    Me.gb_customer_sel.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(537)
    Call InitByLevel()
    Call InitVipSelector()
    'Call InitComboVIP()
    Call SetChecksState(Me.chk_by_level.Checked)
  End Sub

  ''' <summary>
  ''' To select configured levels
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SelectCustomerLevels(ByVal CustomerTier As JackpotAwardCustomerTier)
    Me.chk_by_level.Checked = CustomerTier.ByLevel
    Me.chk_level_anonymous.Checked = CustomerTier.Anonymous
    Me.chk_level_01.Checked = CustomerTier.Level1
    Me.chk_level_02.Checked = CustomerTier.Level2
    Me.chk_level_03.Checked = CustomerTier.Level3
    Me.chk_level_04.Checked = CustomerTier.Level4
  End Sub

  ''' <summary>
  ''' To initialize By Level checkboxs
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitByLevel()
    Me.chk_by_level.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(407)
    Me.chk_level_anonymous.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(405)

    If System.Reflection.Assembly.GetEntryAssembly() Is Nothing Then
      Return
    End If

    Me.chk_level_01.Text = GetCashierPlayerTrackingData("Level01.Name")
    If Me.chk_level_01.Text = "" Then
      Me.chk_level_01.Text = GLB_NLS_GUI_CONFIGURATION.GetString(289)
    End If

    Me.chk_level_02.Text = GetCashierPlayerTrackingData("Level02.Name")
    If Me.chk_level_02.Text = "" Then
      Me.chk_level_02.Text = GLB_NLS_GUI_CONFIGURATION.GetString(290)
    End If

    Me.chk_level_03.Text = GetCashierPlayerTrackingData("Level03.Name")
    If Me.chk_level_03.Text = "" Then
      Me.chk_level_03.Text = GLB_NLS_GUI_CONFIGURATION.GetString(291)
    End If

    Me.chk_level_04.Text = GetCashierPlayerTrackingData("Level04.Name")
    If Me.chk_level_04.Text = "" Then
      Me.chk_level_04.Text = GLB_NLS_GUI_CONFIGURATION.GetString(332)
    End If
  End Sub

  ''' <summary>
  ''' To load VIP radio buttons
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitVipSelector()
    Me.gb_vip_accounts.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(554)
    Me.rb_any_account.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(540)
    Me.rb_just_vip_accounts.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(539)
    Me.rb_just_no_vip_accounts.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(541)
  End Sub

  ''' <summary>
  ''' To check if levels are properly selected
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckLevels() As Boolean

    If Me.chk_by_level.Checked Then
      If Not (Me.chk_level_anonymous.Checked = True Or
        Me.chk_level_01.Checked = True Or
        Me.chk_level_02.Checked = True Or
        Me.chk_level_03.Checked = True Or
        Me.chk_level_04.Checked = True) Then

        NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(550), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' Debe seleccionar como mínimo un nivel
        Me.chk_level_anonymous.Focus()

        Return False
      End If
    End If

    Return True
  End Function

  ''' <summary>
  ''' To set Enabled/Disabled the level checkboxs
  ''' </summary>
  ''' <param name="Enabled"></param>
  ''' <remarks></remarks>
  Private Sub SetChecksState(Enabled As Boolean)
    Me.chk_level_anonymous.Enabled = Enabled
    Me.chk_level_01.Enabled = Enabled
    Me.chk_level_02.Enabled = Enabled
    Me.chk_level_03.Enabled = Enabled
    Me.chk_level_04.Enabled = Enabled

    Me.gb_vip_accounts.Enabled = Enabled
  End Sub ' SetChecksState

  Private Sub SetSelectedVIP(ByVal AwardCustomerVipType As Jackpot.AwardCustomerVipType)
    Select Case AwardCustomerVipType
      Case Jackpot.AwardCustomerVipType.All
        Me.rb_any_account.Checked = True

      Case Jackpot.AwardCustomerVipType.Vip
        Me.rb_just_vip_accounts.Checked = True

      Case Jackpot.AwardCustomerVipType.NoVip
        Me.rb_just_no_vip_accounts.Checked = True

      Case Else
        Me.rb_any_account.Checked = True

    End Select

  End Sub ' SetSelectedVIP

  Private Function GetSelectedVIP() As Jackpot.AwardCustomerVipType
    If Me.rb_any_account.Checked Then
      Return Jackpot.AwardCustomerVipType.All
    ElseIf Me.rb_just_vip_accounts.Checked Then
      Return Jackpot.AwardCustomerVipType.Vip
    ElseIf Me.rb_just_no_vip_accounts.Checked Then
      Return Jackpot.AwardCustomerVipType.NoVip
    End If
  End Function ' GetSelectedVIP

#End Region

#Region "Events"

  Private Sub chk_by_level_CheckedChanged(sender As Object, e As EventArgs) Handles chk_by_level.CheckedChanged
    Call SetChecksState(Me.chk_by_level.Checked)
  End Sub

#End Region

End Class
