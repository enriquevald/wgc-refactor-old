﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_jackpot_time_award
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_uc_jackpot_time_award = New System.Windows.Forms.GroupBox()
    Me.uc_week_days_by_hour = New GUI_Controls.uc_weekdays_by_hour()
    Me.gb_uc_jackpot_time_award.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_uc_jackpot_time_award
    '
    Me.gb_uc_jackpot_time_award.Controls.Add(Me.uc_week_days_by_hour)
    Me.gb_uc_jackpot_time_award.Location = New System.Drawing.Point(3, 3)
    Me.gb_uc_jackpot_time_award.Name = "gb_uc_jackpot_time_award"
    Me.gb_uc_jackpot_time_award.Size = New System.Drawing.Size(430, 210)
    Me.gb_uc_jackpot_time_award.TabIndex = 0
    Me.gb_uc_jackpot_time_award.TabStop = False
    Me.gb_uc_jackpot_time_award.Text = "xTimeAward"
    '
    'uc_week_days_by_hour
    '
    Me.uc_week_days_by_hour.Location = New System.Drawing.Point(2, 21)
    Me.uc_week_days_by_hour.Name = "uc_week_days_by_hour"
    Me.uc_week_days_by_hour.Size = New System.Drawing.Size(422, 178)
    Me.uc_week_days_by_hour.TabIndex = 0
    '
    'uc_jackpot_time_award
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
    Me.Controls.Add(Me.gb_uc_jackpot_time_award)
    Me.Name = "uc_jackpot_time_award"
    Me.Size = New System.Drawing.Size(436, 218)
    Me.gb_uc_jackpot_time_award.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_uc_jackpot_time_award As System.Windows.Forms.GroupBox
  Friend WithEvents uc_week_days_by_hour As GUI_Controls.uc_weekdays_by_hour

End Class
