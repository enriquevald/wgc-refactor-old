﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_jackpot_prize_sharing
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(uc_jackpot_prize_sharing))
    Me.gp_price_sharing = New System.Windows.Forms.GroupBox()
    Me.ef_maximum_amount = New GUI_Controls.uc_entry_field()
    Me.ef_minimum_amount = New GUI_Controls.uc_entry_field()
    Me.cmb_max_winners = New GUI_Controls.uc_combo()
    Me.gb_filter = New System.Windows.Forms.GroupBox()
    Me.rb_jackpot = New System.Windows.Forms.RadioButton()
    Me.rb_bank = New System.Windows.Forms.RadioButton()
    Me.rb_area = New System.Windows.Forms.RadioButton()
    Me.chk_winner = New System.Windows.Forms.CheckBox()
    Me.chk_prize_sharing = New System.Windows.Forms.CheckBox()
    Me.gp_price_sharing.SuspendLayout()
    Me.gb_filter.SuspendLayout()
    Me.SuspendLayout()
    '
    'gp_price_sharing
    '
    Me.gp_price_sharing.Controls.Add(Me.chk_prize_sharing)
    Me.gp_price_sharing.Controls.Add(Me.ef_maximum_amount)
    Me.gp_price_sharing.Controls.Add(Me.ef_minimum_amount)
    Me.gp_price_sharing.Controls.Add(Me.cmb_max_winners)
    Me.gp_price_sharing.Controls.Add(Me.gb_filter)
    Me.gp_price_sharing.Controls.Add(Me.chk_winner)
    Me.gp_price_sharing.Location = New System.Drawing.Point(6, 3)
    Me.gp_price_sharing.Name = "gp_price_sharing"
    Me.gp_price_sharing.Size = New System.Drawing.Size(291, 214)
    Me.gp_price_sharing.TabIndex = 0
    Me.gp_price_sharing.TabStop = False
    '
    'ef_maximum_amount
    '
    Me.ef_maximum_amount.DoubleValue = 0.0R
    Me.ef_maximum_amount.IntegerValue = 0
    Me.ef_maximum_amount.IsReadOnly = False
    Me.ef_maximum_amount.Location = New System.Drawing.Point(74, 58)
    Me.ef_maximum_amount.Name = "ef_maximum_amount"
    Me.ef_maximum_amount.PlaceHolder = Nothing
    Me.ef_maximum_amount.Size = New System.Drawing.Size(200, 24)
    Me.ef_maximum_amount.SufixText = "Sufix Text"
    Me.ef_maximum_amount.SufixTextVisible = True
    Me.ef_maximum_amount.TabIndex = 1
    Me.ef_maximum_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_maximum_amount.TextValue = ""
    Me.ef_maximum_amount.Value = ""
    Me.ef_maximum_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_minimum_amount
    '
    Me.ef_minimum_amount.DoubleValue = 0.0R
    Me.ef_minimum_amount.IntegerValue = 0
    Me.ef_minimum_amount.IsReadOnly = False
    Me.ef_minimum_amount.Location = New System.Drawing.Point(74, 34)
    Me.ef_minimum_amount.Name = "ef_minimum_amount"
    Me.ef_minimum_amount.PlaceHolder = Nothing
    Me.ef_minimum_amount.Size = New System.Drawing.Size(200, 24)
    Me.ef_minimum_amount.SufixText = "Sufix Text"
    Me.ef_minimum_amount.SufixTextVisible = True
    Me.ef_minimum_amount.TabIndex = 0
    Me.ef_minimum_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_minimum_amount.TextValue = ""
    Me.ef_minimum_amount.Value = ""
    Me.ef_minimum_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_max_winners
    '
    Me.cmb_max_winners.AllowUnlistedValues = False
    Me.cmb_max_winners.AutoCompleteMode = False
    Me.cmb_max_winners.IsReadOnly = False
    Me.cmb_max_winners.Location = New System.Drawing.Point(6, 85)
    Me.cmb_max_winners.Name = "cmb_max_winners"
    Me.cmb_max_winners.SelectedIndex = -1
    Me.cmb_max_winners.Size = New System.Drawing.Size(268, 24)
    Me.cmb_max_winners.SufixText = "Sufix Text"
    Me.cmb_max_winners.SufixTextVisible = True
    Me.cmb_max_winners.TabIndex = 2
    Me.cmb_max_winners.TextCombo = Nothing
    Me.cmb_max_winners.TextWidth = 148
    '
    'gb_filter
    '
    Me.gb_filter.Controls.Add(Me.rb_jackpot)
    Me.gb_filter.Controls.Add(Me.rb_bank)
    Me.gb_filter.Controls.Add(Me.rb_area)
    Me.gb_filter.Location = New System.Drawing.Point(18, 142)
    Me.gb_filter.Name = "gb_filter"
    Me.gb_filter.Size = New System.Drawing.Size(257, 61)
    Me.gb_filter.TabIndex = 4
    Me.gb_filter.TabStop = False
    '
    'rb_jackpot
    '
    Me.rb_jackpot.AutoSize = True
    Me.rb_jackpot.Location = New System.Drawing.Point(170, 25)
    Me.rb_jackpot.Name = "rb_jackpot"
    Me.rb_jackpot.Size = New System.Drawing.Size(60, 17)
    Me.rb_jackpot.TabIndex = 2
    Me.rb_jackpot.TabStop = True
    Me.rb_jackpot.Text = "jackpot"
    Me.rb_jackpot.UseVisualStyleBackColor = True
    '
    'rb_bank
    '
    Me.rb_bank.AutoSize = True
    Me.rb_bank.Location = New System.Drawing.Point(93, 25)
    Me.rb_bank.Name = "rb_bank"
    Me.rb_bank.Size = New System.Drawing.Size(50, 17)
    Me.rb_bank.TabIndex = 1
    Me.rb_bank.TabStop = True
    Me.rb_bank.Text = "Bank"
    Me.rb_bank.UseVisualStyleBackColor = True
    '
    'rb_area
    '
    Me.rb_area.AutoSize = True
    Me.rb_area.Location = New System.Drawing.Point(13, 25)
    Me.rb_area.Name = "rb_area"
    Me.rb_area.Size = New System.Drawing.Size(47, 17)
    Me.rb_area.TabIndex = 0
    Me.rb_area.TabStop = True
    Me.rb_area.Text = "Area"
    Me.rb_area.UseVisualStyleBackColor = True
    '
    'chk_winner
    '
    Me.chk_winner.AutoSize = True
    Me.chk_winner.Location = New System.Drawing.Point(30, 118)
    Me.chk_winner.Name = "chk_winner"
    Me.chk_winner.Size = New System.Drawing.Size(81, 17)
    Me.chk_winner.TabIndex = 3
    Me.chk_winner.Text = "chk_winner"
    Me.chk_winner.UseVisualStyleBackColor = True
    '
    'chk_prize_sharing
    '
    Me.chk_prize_sharing.AutoSize = True
    Me.chk_prize_sharing.Location = New System.Drawing.Point(6, 19)
    Me.chk_prize_sharing.Name = "chk_prize_sharing"
    Me.chk_prize_sharing.Size = New System.Drawing.Size(90, 17)
    Me.chk_prize_sharing.TabIndex = 0
    Me.chk_prize_sharing.Text = "xPrizeSharing"
    Me.chk_prize_sharing.UseVisualStyleBackColor = True
    '
    'uc_jackpot_prize_sharing
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
    Me.Controls.Add(Me.gp_price_sharing)
    Me.MaximumSize = New System.Drawing.Size(330, 225)
    Me.Name = "uc_jackpot_prize_sharing"
    Me.Size = New System.Drawing.Size(300, 225)
    Me.gp_price_sharing.ResumeLayout(False)
    Me.gp_price_sharing.PerformLayout()
    Me.gb_filter.ResumeLayout(False)
    Me.gb_filter.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gp_price_sharing As System.Windows.Forms.GroupBox
  Friend WithEvents chk_prize_sharing As System.Windows.Forms.CheckBox
  Friend WithEvents chk_winner As System.Windows.Forms.CheckBox
  Friend WithEvents gb_filter As System.Windows.Forms.GroupBox
  Friend WithEvents rb_bank As System.Windows.Forms.RadioButton
  Friend WithEvents rb_area As System.Windows.Forms.RadioButton
  Friend WithEvents rb_jackpot As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_max_winners As GUI_Controls.uc_combo
  Friend WithEvents ef_maximum_amount As GUI_Controls.uc_entry_field
  Friend WithEvents ef_minimum_amount As GUI_Controls.uc_entry_field

End Class
