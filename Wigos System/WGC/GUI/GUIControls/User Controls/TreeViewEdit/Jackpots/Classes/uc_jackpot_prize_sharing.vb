﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' UC NAME:       uc_jackpot_prize_sharing
' DESCRIPTION:   Jackpot User Control for jackpot prize sharing configuration
' AUTHOR:        Ferran Ortner
' CREATION DATE: 02-MAY-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 02-MAY-2017  FOS    Initial version
'--------------------------------------------------------------------
Imports WSI.Common.Jackpot
Imports GUI_CommonMisc.mdl_NLS
Imports WSI.Common
Imports GUI_CommonOperations


Public Class uc_jackpot_prize_sharing

#Region "Members"

  Private m_jackpot_award_prize_sharing As JackpotAwardPrizeSharing

#End Region


#Region "Properties"

  Public Property AwardPrizeSharing As JackpotAwardPrizeSharing
    Get
      Return Me.m_jackpot_award_prize_sharing
    End Get
    Set(value As JackpotAwardPrizeSharing)
      Me.m_jackpot_award_prize_sharing = value
    End Set
  End Property

  Public Property SharedMinimum As uc_entry_field
    Get
      Return Me.ef_minimum_amount
    End Get
    Set(value As uc_entry_field)
      Me.ef_minimum_amount = value
    End Set
  End Property

  Public Property SharedMaximum As uc_entry_field
    Get
      Return Me.ef_maximum_amount
    End Get
    Set(value As uc_entry_field)
      Me.ef_maximum_amount = value
    End Set
  End Property

#End Region

  Public Sub New()

    Call Init()

  End Sub

#Region "Private Sub"

  Protected Sub Init()

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControls()

  End Sub ' Init

  Protected Sub Init(ByVal PrizeSharing As JackpotAwardPrizeSharing)

    Me.AwardPrizeSharing = PrizeSharing

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControls()

  End Sub ' Init

#End Region

#Region "Public Sub"

  ''' <summary>
  ''' ToJackpotAwardPrizeSharing
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ToJackpotAwardPrizeSharing() As JackpotAwardPrizeSharing

    Me.AwardPrizeSharing.Enabled = Me.chk_prize_sharing.Checked
    Me.AwardPrizeSharing.MaxNumWinner = Me.cmb_max_winners.Value
    Me.AwardPrizeSharing.IncludeWinner = Me.chk_winner.Checked
    Me.AwardPrizeSharing.SameArea = Me.rb_area.Checked
    Me.AwardPrizeSharing.SameBank = Me.rb_bank.Checked
    Me.AwardPrizeSharing.SameJackpot = Me.rb_jackpot.Checked
    Me.AwardPrizeSharing.Minimum = GUI_ParseNumberDecimal(Me.ef_minimum_amount.Value)
    Me.AwardPrizeSharing.Maximum = GUI_ParseNumberDecimal(Me.ef_maximum_amount.Value)

    ' Set last modification
    Me.AwardPrizeSharing.LastModification = WGDB.Now

    Return Me.AwardPrizeSharing
  End Function ' ToJackpotAwardPrizeSharing

  Public Sub InitControls()


    Me.rb_area.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(544)
    Me.rb_bank.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(545)
    Me.rb_jackpot.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(546)
    Me.gb_filter.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(547)
    Me.chk_prize_sharing.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)
    Me.chk_winner.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(549)
    Me.gp_price_sharing.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(548)

    InitCombo()

    'Minimum and maximum
    Me.ef_minimum_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8023)
    Me.ef_minimum_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    Me.ef_maximum_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8025)
    Me.ef_maximum_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)

    Me.ef_minimum_amount.Enabled = Me.chk_prize_sharing.Checked
    Me.ef_maximum_amount.Enabled = Me.ef_minimum_amount.Enabled

    Me.SharedMinimum.Value = Jackpot.DEFAULT_DECIMAL_VALUE
    Me.SharedMaximum.Value = Jackpot.DEFAULT_DECIMAL_VALUE

  End Sub ' InitControls

  ''' <summary>
  ''' Bind data from the db into form controls
  ''' </summary>
  ''' <param name="AwardPrizeSharing"></param>
  ''' <remarks></remarks>
  Public Sub BindData(ByVal AwardPrizeSharing As JackpotAwardPrizeSharing)

    Me.chk_winner.Checked = True ' AwardPrizeSharing.IncludeWinner

    If AwardPrizeSharing Is Nothing Then
      Call DisableControls()
      Return
    End If

    Me.AwardPrizeSharing = AwardPrizeSharing

    If AwardPrizeSharing.Enabled Then

      Me.cmb_max_winners.Value = AwardPrizeSharing.MaxNumWinner
      Me.rb_area.Checked = AwardPrizeSharing.SameArea
      Me.rb_bank.Checked = AwardPrizeSharing.SameBank
      Me.rb_jackpot.Checked = AwardPrizeSharing.SameJackpot
      Me.chk_prize_sharing.Checked = AwardPrizeSharing.Enabled
      Me.chk_winner.Enabled = False
      Me.ef_minimum_amount.Enabled = AwardPrizeSharing.Enabled
      Me.ef_maximum_amount.Enabled = AwardPrizeSharing.Enabled

      Me.SharedMinimum.Value = Me.AwardPrizeSharing.Minimum
      Me.SharedMaximum.Value = Me.AwardPrizeSharing.Maximum


    Else
      DisableControls()
    End If

  End Sub ' BindData

  ''' <summary>
  ''' Disable the all the controls 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub DisableControls()

    Me.cmb_max_winners.Value = True
    Me.rb_area.Checked = False
    Me.rb_bank.Checked = False
    Me.rb_jackpot.Checked = True
    Me.cmb_max_winners.Enabled = False
    Me.gb_filter.Enabled = False
    Me.chk_prize_sharing.Enabled = True
    Me.chk_winner.Enabled = False
    Me.ef_minimum_amount.Enabled = False
    Me.ef_maximum_amount.Enabled = False

    InitCombo()

  End Sub 'DisableControls

  ''' <summary>
  ''' Enable all the controls of the form
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub EnableControls()

    Me.chk_winner.Checked = True
    Me.cmb_max_winners.Enabled = True
    Me.ef_minimum_amount.Enabled = True
    Me.ef_maximum_amount.Enabled = True

    Me.chk_winner.Enabled = False
    Me.gb_filter.Enabled = True

    If Me.AwardPrizeSharing.SameJackpot Then
      Me.rb_jackpot.Checked = True
    ElseIf Me.AwardPrizeSharing.SameArea Then
      Me.rb_area.Checked = True
    ElseIf Me.AwardPrizeSharing.SameBank Then
      Me.rb_bank.Checked = True
    Else
      Me.rb_jackpot.Checked = True
    End If

    InitCombo()

  End Sub ' EnableControls

  ''' <summary>
  ''' Function to check if the screen values are correct
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function IsScreenDataOk() As Boolean

    '' Minimum Empty
    If String.IsNullOrEmpty(Me.ef_minimum_amount.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8098), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
      Call Me.ef_minimum_amount.Focus()

      Return False
    End If

    '' Minimum Empty
    If String.IsNullOrEmpty(Me.ef_maximum_amount.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8099), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
      Call Me.ef_maximum_amount.Focus()

      Return False
    End If

    If Me.chk_prize_sharing.Checked Then
      If GUI_ParseNumberDecimal(Me.SharedMinimum.Value) > GUI_ParseNumberDecimal(Me.SharedMaximum.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8354), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
        Return False
      End If
      If GUI_ParseNumberDecimal(Me.SharedMinimum.Value) = GUI_ParseNumberDecimal(Me.SharedMaximum.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8355), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
        Return False
      End If
    End If

    Return True

  End Function ' IsScreenDataOk

#End Region


  Private Sub InitCombo()
    Dim _idx As Integer

    Me.cmb_max_winners.Clear()
    Me.cmb_max_winners.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(543)

    For _idx = 1 To 10
      cmb_max_winners.Add(_idx.ToString())

    Next

    Me.cmb_max_winners.SelectedIndex = 0
  End Sub


#Region "Events"

  Public Event CheckedChange()

  Private Sub chk_prize_sharing_Click(sender As Object, e As EventArgs) Handles chk_prize_sharing.Click
    If chk_prize_sharing.Checked Then
      Call EnableControls()
    Else
      Call DisableControls()
    End If

    RaiseEvent CheckedChange()

  End Sub

#End Region

  Private Sub chk_prize_sharing_CheckedChanged(sender As Object, e As EventArgs) Handles chk_prize_sharing.CheckedChanged

  End Sub
End Class
