﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' UC NAME:       uc_jackpot_contribution_compensation
' DESCRIPTION:   Jackpot User Control to configure the compensate contribution
' AUTHOR:        Ferran Ortner
' CREATION DATE: 15-MAY-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-MAY-2017  FOS    Initial version
'--------------------------------------------------------------------
Imports WSI.Common.Jackpot
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Collections
Imports System.Globalization
Imports System.Windows.Forms
Imports WSI.Common.Jackpot.Jackpot

Public Class uc_jackpot_contribution_compensation

  Private m_jackpot As Jackpot
  Private m_compensate_pct As Decimal

  Private compensationPCT As Decimal
  Private compensationType As Jackpot.ContributionCompensationType


#Region "Properties"
  Public Property Compensation_PCT() As Decimal
    Get
      Return compensationPCT
    End Get
    Set(ByVal value As Decimal)
      compensationPCT = value
    End Set
  End Property

  Public Property Compensation_Type() As Jackpot.ContributionCompensationType
    Get
      Return compensationType
    End Get
    Set(ByVal value As Jackpot.ContributionCompensationType)
      compensationType = value
    End Set
  End Property
#End Region

#Region "Public"
  Public Sub New()

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControls()
  End Sub

  Public Sub Init(ByVal jackpot As Jackpot)

    Me.m_jackpot = jackpot

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControls()

  End Sub ' Init

  Public Sub InitControls()

    txt_compensate_0.Text = GUI_ParseNumberDecimal(0, 2)

    Me.ef_compensate.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)
    Me.ef_compensate.TextAlign = Windows.Forms.HorizontalAlignment.Right
    Me.gb_compensation.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(584)
    Me.lbl_c2_warning.Text = String.Empty


  End Sub ' InitControls

  Public Function GetScreenData() As Jackpot

    Me.Compensation_PCT = GUI_ParseNumberDecimal(Me.ef_compensate.Value, 2)

    If rb_compensate0.Checked Then
      Me.Compensation_Type = Jackpot.ContributionCompensationType.C0
    End If

    If rb_compensate1.Checked Then
      Me.Compensation_Type = Jackpot.ContributionCompensationType.C1
    End If

    If rb_compensate2.Checked Then
      Me.Compensation_Type = Jackpot.ContributionCompensationType.C2
    End If

    Return Me.m_jackpot

  End Function ' GetScreenData

  Public Function BindData(jackpotSettings As Jackpot)
    Dim _compensate_value As Decimal

    ef_compensate.Value = GUI_ParseNumberDecimal(jackpotSettings.CompensationFixedPct, 2)

    Select Case jackpotSettings.CompensationType
      Case Jackpot.ContributionCompensationType.C0
        rb_compensate0.Checked = True
        rb_compensate1.Checked = False
        rb_compensate2.Checked = False
      Case Jackpot.ContributionCompensationType.C1
        rb_compensate0.Checked = False
        rb_compensate1.Checked = True
        rb_compensate2.Checked = False
      Case Jackpot.ContributionCompensationType.C2
        rb_compensate0.Checked = False
        rb_compensate1.Checked = False
        rb_compensate2.Checked = True
      Case Else
        rb_compensate0.Checked = False
        rb_compensate1.Checked = True
        rb_compensate2.Checked = False

    End Select

    Call CalculateC1Value(jackpotSettings.Minimum, jackpotSettings.Average)

    _compensate_value = JackpotBusinessLogic.GetCompensationPct(jackpotSettings.ContributionMeters.ToCompensateAmount, jackpotSettings.Minimum, jackpotSettings.Average)
    txt_compensate_2.Text = GUI_ParseNumberDecimal(_compensate_value, 2)

    If rb_compensate2.Checked Then
      lbl_c2_warning.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8323)
    End If

    Return True
  End Function

  ''' <summary>
  ''' Function to check if the screen values are correct
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function IsScreenDataOk() As Boolean

    'function  to validate the screen values
    If rb_compensate0.Checked Then
      m_compensate_pct = GUI_ParseNumberDecimal(ef_compensate.Value, 2)
    End If

    If ef_compensate.Value = String.Empty Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1302), ENUM_MB_TYPE.MB_TYPE_ERROR, , , GLB_NLS_GUI_JACKPOT_MGR.GetString(584)) ' el campo contribucion no puede estar en blanco
      ef_compensate.Focus()

      Return False

    End If

    If rb_compensate1.Checked Then
      m_compensate_pct = ef_compensate.Value + GUI_ParseNumberDecimal(txt_compensate_1.Text, 2)
    End If

    If m_compensate_pct > 100 Then
      Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(581), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' La suma de porcentages de contribución no puede ser mayor que 100
      ef_compensate.Focus()

      Return False

    End If

    If rb_compensate2.Checked Then
      Call CheckC2CompensateValue()
    End If

    Return True

  End Function ' IsScreenDataOk

  ''' <summary>
  ''' Function to inform the value into C1 textbox
  ''' </summary>
  ''' <param name="Minimum">Minimum value to the jackpot</param>
  ''' <param name="Average">Average value to the jackpot</param>
  ''' <remarks></remarks>
  Public Sub CalculateC1Value(Minimum As Decimal, Average As Decimal)
    Dim _calculate_value As Decimal

    If (Average > 0) Then
      _calculate_value = (Minimum / Average) * 100
    Else
      _calculate_value = 0
    End If
    txt_compensate_1.Text = GUI_ParseNumberDecimal(_calculate_value, 2)

  End Sub 'CalculateC1Value

#End Region

#Region "Private"
  Private Sub CheckC2CompensateValue()
    Dim _compensate_pct_sum As Decimal

    _compensate_pct_sum = ef_compensate.Value + GUI_ParseNumberDecimal(txt_compensate_2.Text, 2)

    m_compensate_pct = GUI_ParseNumberDecimal(txt_compensate_2.Text, 2)

    If _compensate_pct_sum > 100 Then
      lbl_c2_warning.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8323) ' La maxima compensación disponible es 100%

    Else
      lbl_c2_warning.Text = String.Empty
    End If
  End Sub
#End Region

#Region "Events"
  Private Sub rb_compensate0_CheckedChanged(sender As Object, e As EventArgs) Handles rb_compensate0.CheckedChanged
    Me.lbl_c2_warning.Visible = False
  End Sub

  Private Sub rb_compensate1_CheckedChanged(sender As Object, e As EventArgs) Handles rb_compensate1.CheckedChanged
    Me.lbl_c2_warning.Visible = False
  End Sub

  Private Sub rb_compensate2_CheckedChanged(sender As Object, e As EventArgs) Handles rb_compensate2.CheckedChanged
    Me.lbl_c2_warning.Visible = True

    Call CheckC2CompensateValue()

  End Sub

  Private Sub ef_compensate_Leave(sender As Object, e As EventArgs) Handles ef_compensate.Leave
    If rb_compensate2.Checked Then
      Call CheckC2CompensateValue()
    End If
  End Sub

#End Region
End Class
