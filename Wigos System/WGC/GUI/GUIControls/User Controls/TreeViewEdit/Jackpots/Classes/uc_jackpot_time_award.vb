﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' UC NAME:       uc_jackpot_time_award_config
' DESCRIPTION:   Jackpot User Control For time award configuration
' AUTHOR:        Rubén Lama Ordóñez
' CREATION DATE: 30-MAR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 30-MAR-2017  RLO    Initial version
'--------------------------------------------------------------------
Imports GUI_Controls.uc_weekdays_by_hour
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports System.Windows.Forms
Imports GUI_CommonMisc
Imports WSI.Common.Jackpot
Imports WSI.Common

Public Class uc_jackpot_time_award

#Region " Members "

  Private m_week_days As JackpotAwardDays

#End Region

#Region " Properties "

  Public Property WeekDays As JackpotAwardDays
    Get
      Return Me.m_week_days
    End Get
    Set(value As JackpotAwardDays)
      Me.m_week_days = value
    End Set
  End Property

  Public Property HoursByDay As uc_weekdays_by_hour
    Get
      Return Me.uc_week_days_by_hour
    End Get
    Set(value As uc_weekdays_by_hour)
      Me.uc_week_days_by_hour = value
    End Set
  End Property

#End Region

#Region " Sub "

  Public Sub New()

    Call Me.Init()

  End Sub

  Public Sub Init()

    ' This call is required by the designer.
    Call InitializeComponent()

  End Sub

  Public Sub InitControls()

    Me.uc_week_days_by_hour.InitControls()

    gb_uc_jackpot_time_award.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8113)

  End Sub

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub BindData(ByVal AwardDays As JackpotAwardDays)

    ' Bind all data to controls
    If Not AwardDays Is Nothing Then

      Me.WeekDays = AwardDays

      Me.HoursByDay.WorkingFromMonday.Value = Me.WeekDays.MondayFrom.HourInMinutesToDate()
      Me.HoursByDay.WorkingFromTuesday.Value = Me.WeekDays.TuesdayFrom.HourInMinutesToDate()
      Me.HoursByDay.WorkingFromWednesday.Value = Me.WeekDays.WednesdayFrom.HourInMinutesToDate()
      Me.HoursByDay.WorkingFromThursday.Value = Me.WeekDays.ThursdayFrom.HourInMinutesToDate()
      Me.HoursByDay.WorkingFromFriday.Value = Me.WeekDays.FridayFrom.HourInMinutesToDate()
      Me.HoursByDay.WorkingFromSaturday.Value = Me.WeekDays.SaturdayFrom.HourInMinutesToDate()
      Me.HoursByDay.WorkingFromSunday.Value = Me.WeekDays.SundayFrom.HourInMinutesToDate()

      If Me.WeekDays.IsConfigured Then
        Me.HoursByDay.WorkingToMonday.Value = Me.WeekDays.MondayTo.HourInMinutesToDate()
        Me.HoursByDay.WorkingToTuesday.Value = Me.WeekDays.TuesdayTo.HourInMinutesToDate()
        Me.HoursByDay.WorkingToWednesday.Value = Me.WeekDays.WednesdayTo.HourInMinutesToDate()
        Me.HoursByDay.WorkingToThursday.Value = Me.WeekDays.ThursdayTo.HourInMinutesToDate()
        Me.HoursByDay.WorkingToFriday.Value = Me.WeekDays.FridayTo.HourInMinutesToDate()
        Me.HoursByDay.WorkingToSaturday.Value = Me.WeekDays.SaturdayTo.HourInMinutesToDate()
        Me.HoursByDay.WorkingToSunday.Value = Me.WeekDays.SundayTo.HourInMinutesToDate()
      Else

        Dim _last_hour_data As Date
        _last_hour_data = Me.WeekDays.MondayTo.HourInMinutesToDate().AddSeconds(59)

        Me.HoursByDay.WorkingToMonday.Value = _last_hour_data
        Me.HoursByDay.WorkingToTuesday.Value = _last_hour_data
        Me.HoursByDay.WorkingToWednesday.Value = _last_hour_data
        Me.HoursByDay.WorkingToThursday.Value = _last_hour_data
        Me.HoursByDay.WorkingToFriday.Value = _last_hour_data
        Me.HoursByDay.WorkingToSaturday.Value = _last_hour_data
        Me.HoursByDay.WorkingToSunday.Value = _last_hour_data

      End If

      Call ManageDaysStored()
    End If

  End Sub

  Private Sub ManageDaysStored()

    If Me.WeekDays.IsConfigured Then

      Me.HoursByDay.Days.chk_monday.Checked = IIf(Me.WeekDays.MondayFrom >= 0, True, False)
      If Not (Me.HoursByDay.Days.chk_monday.Checked) Then
        Me.uc_week_days_by_hour.dtp_working_to_monday.Enabled = False
        Me.uc_week_days_by_hour.dtp_working_from_monday.Enabled = False
      End If

      Me.HoursByDay.Days.chk_tuesday.Checked = IIf(Me.WeekDays.TuesdayFrom >= 0, True, False)
      If Not (Me.HoursByDay.Days.chk_tuesday.Checked) Then
        Me.uc_week_days_by_hour.dtp_working_to_tuesday.Enabled = False
        Me.uc_week_days_by_hour.dtp_working_from_tuesday.Enabled = False
      End If

      Me.HoursByDay.Days.chk_wednesday.Checked = IIf(Me.WeekDays.WednesdayFrom >= 0, True, False)
      If Not (Me.HoursByDay.Days.chk_wednesday.Checked) Then
        Me.uc_week_days_by_hour.dtp_working_to_wednesday.Enabled = False
        Me.uc_week_days_by_hour.dtp_working_from_wednesday.Enabled = False
      End If

      Me.HoursByDay.Days.chk_thursday.Checked = IIf(Me.WeekDays.ThursdayFrom >= 0, True, False)
      If Not (Me.HoursByDay.Days.chk_thursday.Checked) Then
        Me.uc_week_days_by_hour.dtp_working_to_thursday.Enabled = False
        Me.uc_week_days_by_hour.dtp_working_from_thursday.Enabled = False
      End If

      Me.HoursByDay.Days.chk_friday.Checked = IIf(Me.WeekDays.FridayFrom >= 0, True, False)
      If Not (Me.HoursByDay.Days.chk_friday.Checked) Then
        Me.uc_week_days_by_hour.dtp_working_to_friday.Enabled = False
        Me.uc_week_days_by_hour.dtp_working_from_friday.Enabled = False
      End If


      Me.HoursByDay.Days.chk_saturday.Checked = IIf(Me.WeekDays.SaturdayFrom >= 0, True, False)
      If Not (Me.HoursByDay.Days.chk_saturday.Checked) Then
        Me.uc_week_days_by_hour.dtp_working_to_saturday.Enabled = False
        Me.uc_week_days_by_hour.dtp_working_from_saturday.Enabled = False
      End If


      Me.HoursByDay.Days.chk_sunday.Checked = IIf(Me.WeekDays.SundayFrom >= 0, True, False)

      If Not (Me.HoursByDay.Days.chk_sunday.Checked) Then
        Me.uc_week_days_by_hour.dtp_working_to_sunday.Enabled = False
        Me.uc_week_days_by_hour.dtp_working_from_sunday.Enabled = False
      End If

    Else

      Me.HoursByDay.Days.chk_monday.Checked = True
      Me.HoursByDay.Days.chk_tuesday.Checked = True
      Me.HoursByDay.Days.chk_wednesday.Checked = True
      Me.HoursByDay.Days.chk_thursday.Checked = True
      Me.HoursByDay.Days.chk_friday.Checked = True
      Me.HoursByDay.Days.chk_saturday.Checked = True
      Me.HoursByDay.Days.chk_sunday.Checked = True

    End If

  End Sub

#End Region

#Region " Function"

  Public Function IsScreenDataOk() As Boolean

    If Not uc_week_days_by_hour.CheckIfAnySelected() Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8108), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' Necesita seleccionar almenos un día de la semana
      Call Me.uc_week_days_by_hour.Focus()

      Return False
    End If

    Return True

  End Function

  ''' <summary>
  ''' Map control values to JackpotAwardDays class
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ToJackpotAwardDays() As JackpotAwardDays
    Dim _working_to_monday As Integer
    Dim _working_from_monday As Integer
    Dim _working_to_tuesday As Integer
    Dim _working_from_tuesday As Integer
    Dim _working_to_wednesday As Integer
    Dim _working_from_wednesday As Integer
    Dim _working_to_thursday As Integer
    Dim _working_from_thursday As Integer
    Dim _working_to_friday As Integer
    Dim _working_from_friday As Integer
    Dim _working_to_saturday As Integer
    Dim _working_from_saturday As Integer
    Dim _working_to_sunday As Integer
    Dim _working_from_sunday As Integer

    _working_to_monday = Me.DateToMinutes(Me.HoursByDay.WorkingToMonday.Value)
    _working_from_monday = Me.DateToMinutes(Me.HoursByDay.WorkingFromMonday.Value)

    _working_to_tuesday = Me.DateToMinutes(Me.HoursByDay.WorkingToTuesday.Value)
    _working_from_tuesday = Me.DateToMinutes(Me.HoursByDay.WorkingFromTuesday.Value)

    _working_to_wednesday = Me.DateToMinutes(Me.HoursByDay.WorkingToWednesday.Value)
    _working_from_wednesday = Me.DateToMinutes(Me.HoursByDay.WorkingFromWednesday.Value)

    _working_to_thursday = Me.DateToMinutes(Me.HoursByDay.WorkingToThursday.Value)
    _working_from_thursday = Me.DateToMinutes(Me.HoursByDay.WorkingFromThursday.Value)

    _working_to_friday = Me.DateToMinutes(Me.HoursByDay.WorkingToFriday.Value)
    _working_from_friday = Me.DateToMinutes(Me.HoursByDay.WorkingFromFriday.Value)

    _working_to_saturday = Me.DateToMinutes(Me.HoursByDay.WorkingToSaturday.Value)
    _working_from_saturday = Me.DateToMinutes(Me.HoursByDay.WorkingFromSaturday.Value)

    _working_to_sunday = Me.DateToMinutes(Me.HoursByDay.WorkingToSunday.Value)
    _working_from_sunday = Me.DateToMinutes(Me.HoursByDay.WorkingFromSunday.Value)


    ' Provisional: Simple mode. 
    If (Me.HoursByDay.Days.chk_monday.Checked) Then
      Me.WeekDays.MondayFrom = _working_from_monday
      Me.WeekDays.MondayTo = _working_to_monday
    Else
      Me.WeekDays.MondayFrom = Jackpot.DEFAULT_TIME_VALUE
      Me.WeekDays.MondayTo = Jackpot.DEFAULT_TIME_VALUE
    End If

    If (Me.HoursByDay.Days.chk_tuesday.Checked) Then
      Me.WeekDays.TuesdayFrom = _working_from_tuesday
      Me.WeekDays.TuesdayTo = _working_to_tuesday
    Else
      Me.WeekDays.TuesdayFrom = Jackpot.DEFAULT_TIME_VALUE
      Me.WeekDays.TuesdayTo = Jackpot.DEFAULT_TIME_VALUE

    End If

    If (Me.HoursByDay.Days.chk_wednesday.Checked) Then
      Me.WeekDays.WednesdayFrom = _working_from_wednesday
      Me.WeekDays.WednesdayTo = _working_to_wednesday
    Else
      Me.WeekDays.WednesdayFrom = Jackpot.DEFAULT_TIME_VALUE
      Me.WeekDays.WednesdayTo = Jackpot.DEFAULT_TIME_VALUE

    End If

    If (Me.HoursByDay.Days.chk_thursday.Checked) Then
      Me.WeekDays.ThursdayFrom = _working_from_thursday
      Me.WeekDays.ThursdayTo = _working_to_thursday
    Else
      Me.WeekDays.ThursdayFrom = Jackpot.DEFAULT_TIME_VALUE
      Me.WeekDays.ThursdayTo = Jackpot.DEFAULT_TIME_VALUE

    End If

    If (Me.HoursByDay.Days.chk_friday.Checked) Then
      Me.WeekDays.FridayFrom = _working_from_friday
      Me.WeekDays.FridayTo = _working_to_friday
    Else
      Me.WeekDays.FridayFrom = Jackpot.DEFAULT_TIME_VALUE
      Me.WeekDays.FridayTo = Jackpot.DEFAULT_TIME_VALUE

    End If

    If (Me.HoursByDay.Days.chk_saturday.Checked) Then
      Me.WeekDays.SaturdayFrom = _working_from_saturday
      Me.WeekDays.SaturdayTo = _working_to_saturday
    Else
      Me.WeekDays.SaturdayFrom = Jackpot.DEFAULT_TIME_VALUE
      Me.WeekDays.SaturdayTo = Jackpot.DEFAULT_TIME_VALUE

    End If

    If (Me.HoursByDay.Days.chk_sunday.Checked) Then
      Me.WeekDays.SundayFrom = _working_from_sunday
      Me.WeekDays.SundayTo = _working_to_sunday
    Else
      Me.WeekDays.SundayFrom = Jackpot.DEFAULT_TIME_VALUE
      Me.WeekDays.SundayTo = Jackpot.DEFAULT_TIME_VALUE

    End If

    ' Set last modification
    Me.WeekDays.LastModification = WGDB.Now

    Return Me.WeekDays

  End Function ' JackpotAwardDays

  Private Function DateToMinutes(ByVal WorkingDate As Date) As Integer

    Dim _hours_in_minutes As Integer
    Dim _minutes As Integer

    _hours_in_minutes = (WorkingDate.Hour() * 60)
    _minutes = WorkingDate.Minute()

    Return _hours_in_minutes + _minutes

  End Function ' DateToMinutes

#End Region

#Region " Events"



#End Region

End Class
