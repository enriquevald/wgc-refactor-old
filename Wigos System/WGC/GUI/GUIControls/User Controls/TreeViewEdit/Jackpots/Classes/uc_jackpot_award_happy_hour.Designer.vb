﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_jackpot_award_happy_hour
  Inherits GUI_Controls.uc_treeview_edit_base

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(uc_jackpot_award_happy_hour))
    Me.chk_before_jackpot = New System.Windows.Forms.CheckBox()
    Me.chk_after_jackpot = New System.Windows.Forms.CheckBox()
    Me.ef_time_before = New GUI_Controls.uc_entry_field()
    Me.ef_awards_before = New GUI_Controls.uc_entry_field()
    Me.cmb_amount_be = New GUI_Controls.uc_combo()
    Me.ef_time_after = New GUI_Controls.uc_entry_field()
    Me.ef_awards_after = New GUI_Controls.uc_entry_field()
    Me.cmb_amount_after = New GUI_Controls.uc_combo()
    Me.gb_happy_hour_config = New System.Windows.Forms.GroupBox()
    Me.ef_maximum_amount = New GUI_Controls.uc_entry_field()
    Me.pnl_after_jackpot = New System.Windows.Forms.Panel()
    Me.ef_minimum_amount = New GUI_Controls.uc_entry_field()
    Me.pnl_before_jackpot = New System.Windows.Forms.Panel()
    Me.pnl_container.SuspendLayout()
    Me.gb_happy_hour_config.SuspendLayout()
    Me.pnl_after_jackpot.SuspendLayout()
    Me.pnl_before_jackpot.SuspendLayout()
    Me.SuspendLayout()
    '
    'pnl_container
    '
    Me.pnl_container.Controls.Add(Me.gb_happy_hour_config)
    Me.pnl_container.Size = New System.Drawing.Size(560, 187)
    '
    'chk_before_jackpot
    '
    Me.chk_before_jackpot.AutoSize = True
    Me.chk_before_jackpot.Location = New System.Drawing.Point(22, 50)
    Me.chk_before_jackpot.Name = "chk_before_jackpot"
    Me.chk_before_jackpot.Size = New System.Drawing.Size(100, 17)
    Me.chk_before_jackpot.TabIndex = 2
    Me.chk_before_jackpot.Text = "xBeforeJackpot"
    Me.chk_before_jackpot.UseVisualStyleBackColor = True
    '
    'chk_after_jackpot
    '
    Me.chk_after_jackpot.AutoSize = True
    Me.chk_after_jackpot.Location = New System.Drawing.Point(279, 50)
    Me.chk_after_jackpot.Name = "chk_after_jackpot"
    Me.chk_after_jackpot.Size = New System.Drawing.Size(91, 17)
    Me.chk_after_jackpot.TabIndex = 4
    Me.chk_after_jackpot.Text = "xAfterJackpot"
    Me.chk_after_jackpot.UseVisualStyleBackColor = True
    '
    'ef_time_before
    '
    Me.ef_time_before.DoubleValue = 0.0R
    Me.ef_time_before.IntegerValue = 0
    Me.ef_time_before.IsReadOnly = False
    Me.ef_time_before.Location = New System.Drawing.Point(6, 3)
    Me.ef_time_before.Name = "ef_time_before"
    Me.ef_time_before.PlaceHolder = Nothing
    Me.ef_time_before.Size = New System.Drawing.Size(232, 24)
    Me.ef_time_before.SufixText = "Sufix Text"
    Me.ef_time_before.SufixTextVisible = True
    Me.ef_time_before.TabIndex = 0
    Me.ef_time_before.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_time_before.TextValue = ""
    Me.ef_time_before.TextWidth = 120
    Me.ef_time_before.Value = ""
    Me.ef_time_before.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_awards_before
    '
    Me.ef_awards_before.DoubleValue = 0.0R
    Me.ef_awards_before.IntegerValue = 0
    Me.ef_awards_before.IsReadOnly = False
    Me.ef_awards_before.Location = New System.Drawing.Point(6, 29)
    Me.ef_awards_before.Name = "ef_awards_before"
    Me.ef_awards_before.PlaceHolder = Nothing
    Me.ef_awards_before.Size = New System.Drawing.Size(232, 24)
    Me.ef_awards_before.SufixText = "Sufix Text"
    Me.ef_awards_before.SufixTextVisible = True
    Me.ef_awards_before.TabIndex = 1
    Me.ef_awards_before.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_awards_before.TextValue = ""
    Me.ef_awards_before.TextWidth = 120
    Me.ef_awards_before.Value = ""
    Me.ef_awards_before.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_amount_be
    '
    Me.cmb_amount_be.AllowUnlistedValues = False
    Me.cmb_amount_be.AutoCompleteMode = False
    Me.cmb_amount_be.IsReadOnly = False
    Me.cmb_amount_be.Location = New System.Drawing.Point(6, 55)
    Me.cmb_amount_be.Name = "cmb_amount_be"
    Me.cmb_amount_be.SelectedIndex = -1
    Me.cmb_amount_be.Size = New System.Drawing.Size(232, 24)
    Me.cmb_amount_be.SufixText = "Sufix Text"
    Me.cmb_amount_be.SufixTextVisible = True
    Me.cmb_amount_be.TabIndex = 2
    Me.cmb_amount_be.TextCombo = Nothing
    Me.cmb_amount_be.TextWidth = 120
    '
    'ef_time_after
    '
    Me.ef_time_after.DoubleValue = 0.0R
    Me.ef_time_after.IntegerValue = 0
    Me.ef_time_after.IsReadOnly = False
    Me.ef_time_after.Location = New System.Drawing.Point(5, 3)
    Me.ef_time_after.Name = "ef_time_after"
    Me.ef_time_after.PlaceHolder = Nothing
    Me.ef_time_after.Size = New System.Drawing.Size(234, 24)
    Me.ef_time_after.SufixText = "Sufix Text"
    Me.ef_time_after.SufixTextVisible = True
    Me.ef_time_after.TabIndex = 0
    Me.ef_time_after.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_time_after.TextValue = ""
    Me.ef_time_after.TextWidth = 120
    Me.ef_time_after.Value = ""
    Me.ef_time_after.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_awards_after
    '
    Me.ef_awards_after.DoubleValue = 0.0R
    Me.ef_awards_after.IntegerValue = 0
    Me.ef_awards_after.IsReadOnly = False
    Me.ef_awards_after.Location = New System.Drawing.Point(5, 28)
    Me.ef_awards_after.Name = "ef_awards_after"
    Me.ef_awards_after.PlaceHolder = Nothing
    Me.ef_awards_after.Size = New System.Drawing.Size(234, 24)
    Me.ef_awards_after.SufixText = "Sufix Text"
    Me.ef_awards_after.SufixTextVisible = True
    Me.ef_awards_after.TabIndex = 1
    Me.ef_awards_after.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_awards_after.TextValue = ""
    Me.ef_awards_after.TextWidth = 120
    Me.ef_awards_after.Value = ""
    Me.ef_awards_after.ValueForeColor = System.Drawing.Color.Blue
    '
    'cmb_amount_after
    '
    Me.cmb_amount_after.AllowUnlistedValues = False
    Me.cmb_amount_after.AutoCompleteMode = False
    Me.cmb_amount_after.IsReadOnly = False
    Me.cmb_amount_after.Location = New System.Drawing.Point(6, 55)
    Me.cmb_amount_after.Name = "cmb_amount_after"
    Me.cmb_amount_after.SelectedIndex = -1
    Me.cmb_amount_after.Size = New System.Drawing.Size(234, 24)
    Me.cmb_amount_after.SufixText = "Sufix Text"
    Me.cmb_amount_after.SufixTextVisible = True
    Me.cmb_amount_after.TabIndex = 2
    Me.cmb_amount_after.TextCombo = Nothing
    Me.cmb_amount_after.TextWidth = 120
    '
    'gb_happy_hour_config
    '
    Me.gb_happy_hour_config.Controls.Add(Me.ef_maximum_amount)
    Me.gb_happy_hour_config.Controls.Add(Me.pnl_after_jackpot)
    Me.gb_happy_hour_config.Controls.Add(Me.ef_minimum_amount)
    Me.gb_happy_hour_config.Controls.Add(Me.pnl_before_jackpot)
    Me.gb_happy_hour_config.Controls.Add(Me.chk_before_jackpot)
    Me.gb_happy_hour_config.Controls.Add(Me.chk_after_jackpot)
    Me.gb_happy_hour_config.Location = New System.Drawing.Point(3, 3)
    Me.gb_happy_hour_config.Name = "gb_happy_hour_config"
    Me.gb_happy_hour_config.Size = New System.Drawing.Size(554, 175)
    Me.gb_happy_hour_config.TabIndex = 0
    Me.gb_happy_hour_config.TabStop = False
    Me.gb_happy_hour_config.Text = "xHappyHour"
    '
    'ef_maximum_amount
    '
    Me.ef_maximum_amount.DoubleValue = 0.0R
    Me.ef_maximum_amount.IntegerValue = 0
    Me.ef_maximum_amount.IsReadOnly = False
    Me.ef_maximum_amount.Location = New System.Drawing.Point(322, 12)
    Me.ef_maximum_amount.Name = "ef_maximum_amount"
    Me.ef_maximum_amount.PlaceHolder = Nothing
    Me.ef_maximum_amount.Size = New System.Drawing.Size(200, 24)
    Me.ef_maximum_amount.SufixText = "Sufix Text"
    Me.ef_maximum_amount.SufixTextVisible = True
    Me.ef_maximum_amount.TabIndex = 1
    Me.ef_maximum_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_maximum_amount.TextValue = ""
    Me.ef_maximum_amount.Value = ""
    Me.ef_maximum_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'pnl_after_jackpot
    '
    Me.pnl_after_jackpot.Controls.Add(Me.ef_time_after)
    Me.pnl_after_jackpot.Controls.Add(Me.ef_awards_after)
    Me.pnl_after_jackpot.Controls.Add(Me.cmb_amount_after)
    Me.pnl_after_jackpot.Location = New System.Drawing.Point(279, 72)
    Me.pnl_after_jackpot.Name = "pnl_after_jackpot"
    Me.pnl_after_jackpot.Size = New System.Drawing.Size(243, 89)
    Me.pnl_after_jackpot.TabIndex = 5
    '
    'ef_minimum_amount
    '
    Me.ef_minimum_amount.DoubleValue = 0.0R
    Me.ef_minimum_amount.IntegerValue = 0
    Me.ef_minimum_amount.IsReadOnly = False
    Me.ef_minimum_amount.Location = New System.Drawing.Point(65, 12)
    Me.ef_minimum_amount.Name = "ef_minimum_amount"
    Me.ef_minimum_amount.PlaceHolder = Nothing
    Me.ef_minimum_amount.Size = New System.Drawing.Size(200, 24)
    Me.ef_minimum_amount.SufixText = "Sufix Text"
    Me.ef_minimum_amount.SufixTextVisible = True
    Me.ef_minimum_amount.TabIndex = 0
    Me.ef_minimum_amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_minimum_amount.TextValue = ""
    Me.ef_minimum_amount.Value = ""
    Me.ef_minimum_amount.ValueForeColor = System.Drawing.Color.Blue
    '
    'pnl_before_jackpot
    '
    Me.pnl_before_jackpot.Controls.Add(Me.ef_time_before)
    Me.pnl_before_jackpot.Controls.Add(Me.ef_awards_before)
    Me.pnl_before_jackpot.Controls.Add(Me.cmb_amount_be)
    Me.pnl_before_jackpot.Location = New System.Drawing.Point(22, 72)
    Me.pnl_before_jackpot.Name = "pnl_before_jackpot"
    Me.pnl_before_jackpot.Size = New System.Drawing.Size(243, 89)
    Me.pnl_before_jackpot.TabIndex = 3
    '
    'uc_jackpot_award_happy_hour
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
    Me.Name = "uc_jackpot_award_happy_hour"
    Me.Size = New System.Drawing.Size(560, 187)
    Me.pnl_container.ResumeLayout(False)
    Me.gb_happy_hour_config.ResumeLayout(False)
    Me.gb_happy_hour_config.PerformLayout()
    Me.pnl_after_jackpot.ResumeLayout(False)
    Me.pnl_before_jackpot.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents cmb_amount_after As GUI_Controls.uc_combo
  Friend WithEvents cmb_amount_be As GUI_Controls.uc_combo
  Friend WithEvents ef_awards_after As GUI_Controls.uc_entry_field
  Friend WithEvents ef_awards_before As GUI_Controls.uc_entry_field
  Friend WithEvents ef_time_after As GUI_Controls.uc_entry_field
  Friend WithEvents ef_time_before As GUI_Controls.uc_entry_field
  Friend WithEvents chk_after_jackpot As System.Windows.Forms.CheckBox
  Friend WithEvents chk_before_jackpot As System.Windows.Forms.CheckBox
  Friend WithEvents gb_happy_hour_config As System.Windows.Forms.GroupBox
  Friend WithEvents pnl_after_jackpot As System.Windows.Forms.Panel
  Friend WithEvents pnl_before_jackpot As System.Windows.Forms.Panel
  Friend WithEvents ef_maximum_amount As GUI_Controls.uc_entry_field
  Friend WithEvents ef_minimum_amount As GUI_Controls.uc_entry_field

End Class
