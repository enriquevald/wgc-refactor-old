﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' UC NAME:       uc_jackpot_contribution
' DESCRIPTION:   Jackpot User Control For show  contribution
' AUTHOR:        Ferran Ortner
' CREATION DATE: 05-ABR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 05-ABR-2017  FOS    Initial version
'--------------------------------------------------------------------

Imports GUI_CommonOperations
Imports System.Windows.Forms
Imports System.Globalization
Imports WSI.Common.Jackpot
Imports GUI_CommonMisc
Imports WSI.Common

Public Class uc_jackpot_contribution

#Region " Constants "

  Private Const DEFAULT_NAME_VALUE = "1"

#End Region

#Region " Members "

  Dim m_week_days_lbl_array(6) As Label
  Dim m_week_days_ef_array(6) As uc_entry_field

  Private m_contribution As JackpotContribution

  Dim m_monday_contribution_pct As Decimal
  Dim m_tuesday_contribution_pct As Decimal
  Dim m_wednesday_contribution_pct As Decimal
  Dim m_thursday_contribution_pct As Decimal
  Dim m_friday_contribution_pct As Decimal
  Dim m_saturday_contribution_pct As Decimal
  Dim m_sunday_contribution_pct As Decimal

#End Region

#Region " Properties "

  Public Property Contribution As JackpotContribution
    Get
      Return Me.m_contribution
    End Get
    Set(value As JackpotContribution)
      Me.m_contribution = value
    End Set
  End Property

#End Region

#Region "Public Sub"

  Public Sub New()
    Call Init(New JackpotContribution())
  End Sub

  Public Sub New(Contribution As JackpotContribution)
    Call Init(Contribution)
  End Sub

  Public Property PanelDeleteButton As Panel
    Get
      Return Me.pnl_delete_button
    End Get
    Set(value As Panel)
      Me.pnl_delete_button = value
    End Set
  End Property


  ''' <summary>
  '''  Map controls to JackpotContribution
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub BindData(ByVal Contribution As JackpotContribution)
    Dim _terminal_list As TerminalList

    _terminal_list = New TerminalList()
    Me.Contribution = Contribution

    Me.ef_monday_contribution.Value = Contribution.Monday
    Me.ef_tuesday_contribution.Value = Contribution.Tuesday
    Me.ef_wednesday_contribution.Value = Contribution.Wednesday
    Me.ef_thursday_contribution.Value = Contribution.Thursday
    Me.ef_friday_contribution.Value = Contribution.Friday
    Me.ef_saturday_contribution.Value = Contribution.Saturday
    Me.ef_sunday_contribution.Value = Contribution.Sunday
    Me.ef_contribution_percent.Value = Contribution.FixedContribution
    Me.cmb_contribution_type.SelectedIndex = Contribution.Type
    Call cmb_contribution_type_ValueChangedEvent()


    _terminal_list.FromXml(Contribution.Terminals)
    Me.dg_providers.SetTerminalList(_terminal_list)

  End Sub

  Public Function ToJackpotContribution(ByVal JackpotId As Integer) As JackpotContribution
    'Dim _pair As KeyValuePair(Of Integer, String)
    Dim _terminal_list As TerminalList

    _terminal_list = New TerminalList()
    dg_providers.GetTerminalsFromControl(_terminal_list)

    If Me.Contribution.ContributionState = Jackpot.ContributionGroupState.InUse Then
      Me.Contribution.JackpotId = JackpotId
    Else
      Me.Contribution.JackpotId = Jackpot.DEFAULT_ID_VALUE
    End If

    If _terminal_list.ToXml() Is Nothing Then
      Me.Contribution.Terminals = ""
    Else
      Me.Contribution.Terminals = _terminal_list.ToXml().Replace(vbCr, "").Replace(vbLf, "").Replace(">  <", "><").Replace(">    <", "><")
    End If

    If Me.cmb_contribution_type.SelectedIndex = Jackpot.ContributionType.Fixed Then
      Me.Contribution.FixedContribution = Me.ef_contribution_percent.Value
      Me.Contribution.Monday = Nothing
      Me.Contribution.Tuesday = Nothing
      Me.Contribution.Wednesday = Nothing
      Me.Contribution.Thursday = Nothing
      Me.Contribution.Friday = Nothing
      Me.Contribution.Saturday = Nothing
      Me.Contribution.Sunday = Nothing
    Else
      Me.Contribution.Monday = GUI_ParseNumberDecimal(Me.ef_monday_contribution.Value)
      Me.Contribution.Tuesday = GUI_ParseNumberDecimal(Me.ef_tuesday_contribution.Value)
      Me.Contribution.Wednesday = GUI_ParseNumberDecimal(Me.ef_wednesday_contribution.Value)
      Me.Contribution.Thursday = GUI_ParseNumberDecimal(Me.ef_thursday_contribution.Value)
      Me.Contribution.Friday = GUI_ParseNumberDecimal(Me.ef_friday_contribution.Value)
      Me.Contribution.Saturday = GUI_ParseNumberDecimal(Me.ef_saturday_contribution.Value)
      Me.Contribution.Sunday = GUI_ParseNumberDecimal(Me.ef_sunday_contribution.Value)
      Me.Contribution.FixedContribution = Nothing

    End If

    Me.Contribution.Type = cmb_contribution_type.Value
    Me.Contribution.ContributionState = Jackpot.ContributionGroupState.InUse

    ' Set last modification
    Me.Contribution.LastModification = WGDB.Now

    Return Me.Contribution

  End Function


  Public Function IsScreenDataOk()

    If Me.cmb_contribution_type.SelectedIndex = Jackpot.ContributionType.Fixed Then

      ' Check Fixed contribution
      If Not Me.CheckFixedContribution() Then
        Return False
      End If

    ElseIf Me.cmb_contribution_type.SelectedIndex = Jackpot.ContributionType.Weekly Then

      ' Check Weekly contribution
      If Not Me.CheckWeeklyContribution() Then
        Return False
      End If

    End If

    If Not Me.dg_providers.CheckAtLeastOneSelected() Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(172))
      Call dg_providers.Focus()

      Return False
    End If

    Return True
  End Function

#End Region

#Region "Private Sub"

  Protected Sub GUI_InitControls()
    Me.ef_friday_contribution.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 4)
    Me.ef_monday_contribution.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 4)
    Me.ef_saturday_contribution.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 4)
    Me.ef_thursday_contribution.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 4)
    Me.ef_tuesday_contribution.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 4)
    Me.ef_wednesday_contribution.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 4)
    Me.ef_sunday_contribution.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 4)
    Me.ef_contribution_percent.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 4)

    Call LoadComboContribution()

    lbl_contribution.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8086)
    lbl_all_contribution.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8083)
    lbl_sunday_contribution.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(295)
    lbl_monday_contribution.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(289)
    lbl_tuesday_contribution.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(290)
    lbl_wednesday_contribution.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(291)
    lbl_thursday_contribution.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(292)
    lbl_friday_contribution.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(293)
    lbl_saturday_contribution.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(294)

    Call SetWeekDaysControl()
    dg_providers.gb_provider.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8205)
  End Sub

  Private Sub SetWeekDaysControl()
    m_week_days_lbl_array(0) = lbl_sunday_contribution
    m_week_days_lbl_array(1) = lbl_monday_contribution
    m_week_days_lbl_array(2) = lbl_tuesday_contribution
    m_week_days_lbl_array(3) = lbl_wednesday_contribution
    m_week_days_lbl_array(4) = lbl_thursday_contribution
    m_week_days_lbl_array(5) = lbl_friday_contribution
    m_week_days_lbl_array(6) = lbl_saturday_contribution
    Call SortControlByFirstDayOfWeek(m_week_days_lbl_array)
    m_week_days_ef_array(0) = ef_sunday_contribution
    m_week_days_ef_array(1) = ef_monday_contribution
    m_week_days_ef_array(2) = ef_tuesday_contribution
    m_week_days_ef_array(3) = ef_wednesday_contribution
    m_week_days_ef_array(4) = ef_thursday_contribution
    m_week_days_ef_array(5) = ef_friday_contribution
    m_week_days_ef_array(6) = ef_saturday_contribution
    Call SortControlByFirstDayOfWeek(m_week_days_ef_array)
  End Sub


  Private Sub CheckWeeklyContributionValues()
    m_monday_contribution_pct = GUI_ParseNumberDecimal(Me.ef_monday_contribution.Value)
    m_tuesday_contribution_pct = GUI_ParseNumberDecimal(Me.ef_tuesday_contribution.Value)
    m_wednesday_contribution_pct = GUI_ParseNumberDecimal(Me.ef_wednesday_contribution.Value)
    m_thursday_contribution_pct = GUI_ParseNumberDecimal(Me.ef_thursday_contribution.Value)
    m_friday_contribution_pct = GUI_ParseNumberDecimal(Me.ef_friday_contribution.Value)
    m_saturday_contribution_pct = GUI_ParseNumberDecimal(Me.ef_saturday_contribution.Value)
    m_sunday_contribution_pct = GUI_ParseNumberDecimal(Me.ef_sunday_contribution.Value)

  End Sub


  Private Function CheckWeeklyContributionUnderZeroError() As Boolean
    Dim _has_error As Boolean
    _has_error = False

    If m_monday_contribution_pct < 0 Then
      _has_error = True
      ef_monday_contribution.Focus()
    End If

    If m_tuesday_contribution_pct < 0 Then
      _has_error = True
      ef_tuesday_contribution.Focus()
    End If
    If m_wednesday_contribution_pct < 0 Then
      _has_error = True
      ef_wednesday_contribution.Focus()
    End If
    If m_thursday_contribution_pct < 0 Then
      _has_error = True
      ef_thursday_contribution.Focus()
    End If
    If m_friday_contribution_pct < 0 Then
      _has_error = True
      ef_friday_contribution.Focus()
    End If
    If m_saturday_contribution_pct < 0 Then
      _has_error = True
      ef_saturday_contribution.Focus()
    End If

    If m_sunday_contribution_pct < 0 Then
      _has_error = True
      ef_sunday_contribution.Focus()
    End If

    Return _has_error
  End Function

  Private Function CheckWeeklyContributionOverOneError() As Boolean

    Dim _has_error As Boolean
    _has_error = False

    If m_monday_contribution_pct > 1 Then
      _has_error = True
      ef_monday_contribution.Focus()
    End If

    If m_tuesday_contribution_pct > 1 Then
      _has_error = True
      ef_tuesday_contribution.Focus()
    End If
    If m_wednesday_contribution_pct > 1 Then
      _has_error = True
      ef_wednesday_contribution.Focus()
    End If
    If m_thursday_contribution_pct > 1 Then
      _has_error = True
      ef_thursday_contribution.Focus()
    End If
    If m_friday_contribution_pct > 1 Then
      _has_error = True
      ef_friday_contribution.Focus()
    End If
    If m_saturday_contribution_pct > 1 Then
      _has_error = True
      ef_saturday_contribution.Focus()
    End If

    If m_sunday_contribution_pct > 1 Then
      _has_error = True
      ef_sunday_contribution.Focus()
    End If

    Return _has_error

    Return False
  End Function

  Private Function CheckWeeklyBlankError() As Boolean
    Dim _has_error As Boolean
    _has_error = False


    If ef_monday_contribution.Value = String.Empty Then
      _has_error = True
      ef_monday_contribution.Focus()
    End If

    If ef_tuesday_contribution.Value = String.Empty Then
      _has_error = True
      ef_tuesday_contribution.Focus()
    End If
    If ef_wednesday_contribution.Value = String.Empty Then
      _has_error = True
      ef_wednesday_contribution.Focus()
    End If
    If ef_thursday_contribution.Value = String.Empty Then
      _has_error = True
      ef_thursday_contribution.Focus()
    End If
    If ef_friday_contribution.Value = String.Empty Then
      _has_error = True
      ef_friday_contribution.Focus()
    End If
    If ef_saturday_contribution.Value = String.Empty Then
      _has_error = True
      ef_saturday_contribution.Focus()
    End If

    If ef_sunday_contribution.Value = String.Empty Then
      _has_error = True
      ef_sunday_contribution.Focus()
    End If

    Return _has_error

  End Function

  Private Sub Init(ByVal Contribution As JackpotContribution)
    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call GUI_InitControls()

    Me.Contribution = Contribution
    Call BindData(Contribution)

  End Sub

  ' PURPOSE:  Sorts the week days control depending on the system's first day of week
  '           It's private because is used only here. Adapt it and convert it in public if needed in other places
  '           It's a modified copy of mdl_Misc.vb/SortCheckBoxesByFirstDayOfWeek
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SortControlByFirstDayOfWeek(ByVal WeekDaysArray() As Control)
    Dim _aux_array(6) As Control
    Dim _first_day_of_week As Int32
    Dim _idx As Int32
    Dim _idx_index As Int32

    ' Check the input array length
    If WeekDaysArray.Length <> _aux_array.Length Then
      Return
    End If

    ' Check system's first day of week
    _first_day_of_week = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek()
    If _first_day_of_week = 0 Then
      Return
    End If

    ' Store week days arrays into a temporary arrays
    For _idx = 0 To WeekDaysArray.Length - 1
      If TypeOf WeekDaysArray(0) Is Label Then
        _aux_array(_idx) = New Label
      ElseIf TypeOf WeekDaysArray(0) Is uc_entry_field Then
        _aux_array(_idx) = New uc_entry_field
      End If
      _aux_array(_idx).Location = WeekDaysArray(_idx).Location
      _aux_array(_idx).TabIndex = WeekDaysArray(_idx).TabIndex
    Next

    ' Store the output week days array starting on the proper day
    For _idx = 0 To WeekDaysArray.Length - 1
      _idx_index = (_first_day_of_week + _idx) Mod 7

      WeekDaysArray(_idx_index).Location = _aux_array(_idx).Location
      WeekDaysArray(_idx_index).TabIndex = _aux_array(_idx).TabIndex
    Next

  End Sub

  Private Function CheckWeeklyContribution(ByRef DayError As String)

    If Not CheckDayContribution(ef_monday_contribution.Value) Then
      DayError = GLB_NLS_GUI_JACKPOT_MGR.GetString(289)

      Return False
    End If

    If Not CheckDayContribution(ef_tuesday_contribution.Value) Then
      DayError = GLB_NLS_GUI_JACKPOT_MGR.GetString(290)

      Return False
    End If

    If Not CheckDayContribution(ef_wednesday_contribution.Value) Then
      DayError = GLB_NLS_GUI_JACKPOT_MGR.GetString(291)

      Return False
    End If

    If Not CheckDayContribution(ef_thursday_contribution.Value) Then
      DayError = GLB_NLS_GUI_JACKPOT_MGR.GetString(292)

      Return False
    End If

    If Not CheckDayContribution(ef_friday_contribution.Value) Then
      DayError = GLB_NLS_GUI_JACKPOT_MGR.GetString(293)

      Return False
    End If

    If Not CheckDayContribution(ef_saturday_contribution.Value) Then
      DayError = GLB_NLS_GUI_JACKPOT_MGR.GetString(294)

      Return False
    End If

    If Not CheckDayContribution(ef_sunday_contribution.Value) Then
      DayError = GLB_NLS_GUI_JACKPOT_MGR.GetString(295)

      Return False
    End If

    Return True
  End Function

  Private Function CheckDayContribution(ContributionValue As String)
    Dim _contr_value As Decimal

    _contr_value = GUI_ParseNumberDecimal(ContributionValue)
    If _contr_value < 0 Or _contr_value > 100 Then
      Return False
    End If

    Return True
  End Function

  Private Sub LoadComboContribution()

    Dim _str_text As String
    Dim dict As New Dictionary(Of Integer, String)
    _str_text = String.Empty

    cmb_contribution_type.Clear()

    For Each item As Jackpot.ContributionType In [Enum].GetValues(GetType(Jackpot.ContributionType))

      Select Case item
        Case Jackpot.ContributionType.Fixed
          _str_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2147)
        Case Jackpot.ContributionType.Weekly
          _str_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8033)
      End Select
      dict.Add((CInt(item)), _str_text)
    Next

    cmb_contribution_type.Add(dict)

  End Sub ' LoadComboContribution

  ''' <summary>
  ''' Check fixed contribution
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckFixedContribution() As Boolean
    Dim _contribution_pct As Decimal

    If String.IsNullOrEmpty(Me.ef_contribution_percent.Value) Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8105), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' Debe introducir una contribución mínima
      Me.ef_contribution_percent.Focus()

      Return False
    End If

    _contribution_pct = GUI_ParseNumberDecimal(Me.ef_contribution_percent.Value)
    If _contribution_pct < 0 And _contribution_pct > 100 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8106), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El valor de contribución ha de estar entre 0 y 100
      Call Me.ef_contribution_percent.Focus()

      Return False
    End If

    _contribution_pct = GUI_ParseNumberDecimal(Me.ef_contribution_percent.Value)
    If _contribution_pct <= 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8234), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' La contribución ha de ser mayor que 0
      Call Me.ef_contribution_percent.Focus()

      Return False
    End If


    If _contribution_pct > 1 Then
      Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(579), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK) ' La contribución no puede ser mayor de 1%
      Call Me.ef_contribution_percent.Focus()

      Return False
    End If

    If ef_contribution_percent.Value = String.Empty Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8234), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK) ' La contribución ha de ser mayor que 0
      Call Me.ef_contribution_percent.Focus()
    End If

    Return True

  End Function ' CheckFixedContribution

  ''' <summary>
  ''' Check fixed contribution
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckWeeklyContribution() As Boolean
    Dim _day_error As String

    _day_error = String.Empty
    If Not CheckWeeklyContribution(_day_error) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8107), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El valor de contribución del día {0} ha de estar entre 0 y 100

      Return False
    End If

    Me.CheckWeeklyContributionValues()

    If Me.CheckWeeklyContributionUnderZeroError() Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8234), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' La contribución ha de ser mayor que 0

      Return False
    End If

    If Me.CheckWeeklyContributionOverOneError() Then
      Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(579), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK) ' La contribución no puede ser mayor de 1%

      Return False
    End If

    If Me.CheckWeeklyBlankError() Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8234), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK) ' La contribución ha de ser mayor que 0

      Return False
    End If

    Return True

  End Function ' CheckWeeklyContribution

#End Region

#Region "Events"

  Private Sub cmb_contribution_type_ValueChangedEvent() Handles cmb_contribution_type.ValueChangedEvent

    If cmb_contribution_type.TextValue.Equals(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2147)) Then
      Me.ef_contribution_percent.Visible = True
      pnl_week_contribution_percent.Visible = False
      lbl_all_contribution.Visible = True
      lbl_percentaje_sign_2.Visible = True
    Else
      Me.ef_contribution_percent.Visible = False
      pnl_week_contribution_percent.Visible = True
      lbl_all_contribution.Visible = False
      lbl_percentaje_sign_2.Visible = False
    End If

  End Sub

#End Region

End Class
