﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' UC NAME:       uc_jackpot_flags
' DESCRIPTION:   Jackpot User Control For config flags
' AUTHOR:        Ferran Ortner
' CREATION DATE: 12-APR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 12-APR-2017  RLO    Initial version
'--------------------------------------------------------------------

Imports GUI_CommonOperations
Imports System.Windows.Forms
Imports System.Globalization
Imports WSI.Common.Jackpot
Imports GUI_CommonMisc
Imports WSI.Common
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Drawing
Imports GUI_Controls.frm_treeview_edit


Public Class uc_jackpot_flags

#Region " Constants "

  Private Const DEFAULT_NAME_VALUE = "1"

  ' SELECT FLAG GRID
  Private Const GRID_FLAG_IDX As Integer = 0
  Private Const GRID_FLAG_CHECKED As Integer = 1
  Private Const GRID_FLAG_COLOR As Integer = 2
  Private Const GRID_FLAG_NAME As Integer = 3
  Private Const GRID_FLAG_COUNT As Integer = 4
  Private Const GRID_FLAG_ID_COLOR As Integer = 5
  Private Const GRID_FLAG_TYPE As Integer = 6

  Private Const GRID_SELECT_FLAG_COLUMNS As Integer = 6
  Private Const GRID_SELECT_FLAG_HEADER_ROWS As Integer = 1
  Private Const FORM_DB_MIN_VERSION As Short = 158

  Private Const LEN_FLAG_AWARDED_DIGITS As Integer = 2
  Private Const LEN_FLAG_REQUIRED_DIGITS As Integer = 6

#End Region

#Region "Enums"

  Private Enum GRID_DATA_STATUS
    OK = 1
    VALUE_NOT_VALID = 2
    NO_ONE_CHECKED = 3
    MAX_FLAGS_EXCEEDED = 4
  End Enum

#End Region

#Region " Members "

  Public Delegate Sub ClickButton()

  Private m_flags As JackpotAwardFilterFlagList
  Private m_isConfigured As Boolean

#End Region

#Region " Properties "

  Public Property Flags As JackpotAwardFilterFlagList
    Get
      Return Me.m_flags
    End Get
    Set(value As JackpotAwardFilterFlagList)
      Me.m_flags = value
    End Set
  End Property
  Public Property IsConfigured As Boolean
    Get
      Return Me.m_isConfigured
    End Get
    Set(value As Boolean)
      Me.m_isConfigured = value
    End Set
  End Property


#End Region

#Region "Public Sub"

  Public Sub New()
    Call Init()
  End Sub

  ''' <summary>
  '''  Map controls to AwardFilter
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub BindData(ByVal Flags As JackpotAwardFilterFlagList)

    If Flags Is Nothing Then
      Return ' TODO UNRANKED - RLO: Show error message
    End If

    Me.Flags = Flags

    ' Init promo flags
    If CheckFlagRequired() Then
      chk_by_flag.Checked = True
    Else
      chk_by_flag.Checked = False

    End If

    Call InitFlagGrid(dg_flags)

    If (dg_flags.NumRows <= 0) Then
      Me.Enabled = False
    End If

    Me.FlagsCheckManager()

  End Sub

  ''' <summary>
  ''' Check if any flag is information
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function CheckFlagRequired() As Boolean

    Me.GetFlagsGrid()
    For Each flag As JackpotAwardFilterFlag In Flags.Items

      If flag.FlagCount > 0 Then
        Return True
      End If

    Next

  End Function

  Public Function ToJackpotFlags() As JackpotAwardFilterFlagList

    If Not chk_by_flag.Checked Then
      Me.Flags.MarkAllToDeleted()
    Else
      Me.GetFlagsGrid()
    End If

    ' Set last modification
    Me.Flags.LastModification = WGDB.Now

    Return Me.Flags

  End Function

  Public Function IsScreenDataOk()

    If chk_by_flag.Checked Then
      If Not Me.CheckFlagGridData() Then
        Return False
      End If
    End If

    Call GetFlagsGrid()

    Return True

  End Function

  ''' <summary>
  ''' Flag ckeck and button manager
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub FlagsCheckManager()
    SetFlagsGridStatus(Me.dg_flags, chk_by_flag.Checked)
    btn_accounts_afected.Enabled = chk_by_flag.Checked
  End Sub ' FlagsCheckManager

  ' PURPOSE: Check if flag grid has correct values
  '
  '    - INPUT:
  '       - FlagGrid : grid to check
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '       - Grid Status (OK, VALUE_NOT_VALID or NO_ONE_CHECKED)
  '
  Private Function IsFlagGridDataOK(ByRef FlagGrid As uc_grid, Optional ByRef FlagList As ArrayList = Nothing, Optional ByVal MaxFlags As Integer = -1) As Integer

    Dim _idx_row As Integer
    Dim _count_checked As Integer
    Dim _sum_count_flags As Integer
    Dim _info_flag As JackpotAwardFilterFlag
    Dim _flag_count As Integer

    _count_checked = 0 ' Number of rows checked
    _sum_count_flags = 0 ' Number of flags required/assigned
    _info_flag = New JackpotAwardFilterFlag()
    FlagList = New ArrayList()

    If FlagGrid.NumRows > 0 Then

      For _idx_row = 0 To FlagGrid.NumRows() - 1

        _flag_count = GUI_ParseNumber(FlagGrid.Cell(_idx_row, GRID_FLAG_COUNT).Value)

        If FlagGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
          _count_checked = _count_checked + 1

          If String.IsNullOrEmpty(_flag_count) Or GUI_ParseNumber(_flag_count) < 1 Then
            ' Flag checked without a value or lower than 1
            SetupGridWithError(FlagGrid, _idx_row)
            Return GRID_DATA_STATUS.VALUE_NOT_VALID
          ElseIf MaxFlags <> -1 Then
            _sum_count_flags = _sum_count_flags + GUI_ParseNumber(_flag_count)
            If _sum_count_flags > MaxFlags Then
              ' overall number exceeded
              SetupGridWithError(FlagGrid, _idx_row)
              Return GRID_DATA_STATUS.MAX_FLAGS_EXCEEDED
            End If
          End If


          SetupInfoFlag(_info_flag, FlagList, FlagGrid, _idx_row, _flag_count)


        End If
      Next

      If _count_checked = 0 Then
        ' Active but without a flag selected
        SetupGridWithError(FlagGrid, _idx_row)
        Return GRID_DATA_STATUS.NO_ONE_CHECKED
      End If

    Else
      Return GRID_DATA_STATUS.VALUE_NOT_VALID
    End If


    Return GRID_DATA_STATUS.OK
  End Function

  ''' <summary>
  ''' Sub to setup info flag 
  ''' </summary>
  ''' <param name="FlagGrid"></param>
  ''' <remarks></remarks>
  Public Sub SetupInfoFlag(ByRef _info_flag As JackpotAwardFilterFlag, ByRef FlagList As ArrayList, FlagGrid As uc_grid, ByVal _idx_row As Integer, ByVal _flag_count As Integer)
    _info_flag.FlagId = FlagGrid.Cell(_idx_row, GRID_FLAG_IDX).Value
    _info_flag.FlagCount = _flag_count
    _info_flag.Name = FlagGrid.Cell(_idx_row, GRID_FLAG_NAME).Value
    _info_flag.Color = IIf(FlagGrid.Cell(_idx_row, GRID_FLAG_ID_COLOR).Value.Length > 0, FlagGrid.Cell(_idx_row, GRID_FLAG_ID_COLOR).Value, 0)
    FlagList.Add(_info_flag)

  End Sub

  ''' <summary>
  ''' Sub to setup grid if was an error 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetupGridWithError(ByRef FlagGrid As uc_grid, ByVal _idx_row As Integer)

    FlagGrid.Redraw = False
    FlagGrid.ClearSelection()
    'FlagGrid.IsSelected(_idx_row) = True
    FlagGrid.Redraw = True
    FlagGrid.Focus()

  End Sub

  ''' <summary>
  ''' Init grid flags
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitializeFlags()

    Call Me.dg_flags.Init(GRID_SELECT_FLAG_COLUMNS, GRID_SELECT_FLAG_HEADER_ROWS, True)

    ' FLAG_ID ( NOT VISIBLE )
    With Me.dg_flags.Column(GRID_FLAG_IDX)
      .Width = 0
      .IsColumnPrintable = False
    End With

    ' FLAG CHECKED
    With Me.dg_flags.Column(GRID_FLAG_CHECKED)
      .Header.Text = ""
      .WidthFixed = 300
      .Fixed = True
      .Editable = True
      .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .HighLightWhenSelected = False
    End With

    ' FLAG COLOR
    With Me.dg_flags.Column(GRID_FLAG_COLOR)
      .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) 'Bandera
      .WidthFixed = 200
      .HighLightWhenSelected = False
    End With

    ' COLUMN_FLAG_NAME
    With Me.dg_flags.Column(GRID_FLAG_NAME)
      .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) 'Bandera
      .Width = 1800
      .Editable = False
      .HighLightWhenSelected = False
    End With

    ' COLUMN_FLAG_COUNT
    With Me.dg_flags.Column(GRID_FLAG_COUNT)
      .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1416) 'Requeridas
      .WidthFixed = 1100
      .Fixed = True
      .Editable = True
      .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
      .EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, LEN_FLAG_REQUIRED_DIGITS)
    End With

    ' COLUMN_FLAG_ID_COLOR
    With Me.dg_flags.Column(GRID_FLAG_ID_COLOR)
      .Width = 0
      .IsColumnPrintable = False

    End With

  End Sub ' InitializeFlags

  ''' <summary>
  ''' Get flags form grid
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GetFlagsGrid()

    Dim _row As Integer
    Dim _flag As JackpotAwardFilterFlag

    For _row = 0 To dg_flags.NumRows() - 1

      _flag = Flags.Items.Find(Function(_flg) _flg.FlagId = dg_flags.Cell(_row, GRID_FLAG_IDX).Value)

      If Not _flag Is Nothing Then

        If dg_flags.Cell(_row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
          _flag.FlagCount = GUI_ParseNumber(dg_flags.Cell(_row, GRID_FLAG_COUNT).Value)
          _flag.IsDeleted = False
        Else
          _flag.FlagCount = 0
          _flag.IsDeleted = True
        End If

        _flag.LastModification = WGDB.Now

      End If

    Next

  End Sub ' GetFlagsGrid

#End Region

#Region "Private Sub"


  Protected Sub GUI_InitControls()

    'Group Box
    gb_by_flags.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1397)

    'Check Activo
    chk_by_flag.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)

    'Botón activar cuentas
    btn_accounts_afected.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2834)

    'If chk_by_flag.Checked And Me.IsConfigured Then
    '  btn_accounts_afected.Enabled = True
    'Else
    '  btn_accounts_afected.Enabled = False
    'End If

  End Sub

  Private Sub Init()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    Call InitializeFlags()

    Call GUI_InitControls()

    'Me.btn_accounts_afected.Enabled = Me.IsConfigured

  End Sub

  ' PURPOSE: Set grid behaviour
  '
  '    - INPUT:
  '       - FlagGrid : grid to be setted
  '       - IsChecked: status of drid ( active or no active)
  '
  '    - OUTPUT:
  '
  Private Sub SetFlagsGridStatus(ByRef FlagsGrid As uc_grid, ByVal IsChecked As Boolean)
    Dim _idx_row As Integer

    If IsChecked Then
      ' Current grid is active
      FlagsGrid.Enabled = True
      FlagsGrid.Settings.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

      For _idx_row = 0 To FlagsGrid.NumRows - 1
        ' Checked color
        Select Case FlagsGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value
          Case uc_grid.GRID_CHK_UNCHECKED_DISABLED
            FlagsGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
          Case uc_grid.GRID_CHK_CHECKED_DISABLED
            FlagsGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
        End Select
        ' Name and count color
        FlagsGrid.Cell(_idx_row, GRID_FLAG_NAME).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        FlagsGrid.Cell(_idx_row, GRID_FLAG_COUNT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        FlagsGrid.Cell(_idx_row, GRID_FLAG_COUNT).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      Next

    Else
      ' Current grid is inactive
      FlagsGrid.Enabled = False
      FlagsGrid.Settings.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)

      For _idx_row = 0 To FlagsGrid.NumRows - 1
        ' Checked color
        Select Case FlagsGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value
          Case uc_grid.GRID_CHK_UNCHECKED
            FlagsGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
          Case uc_grid.GRID_CHK_CHECKED
            FlagsGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
        End Select
        ' Name and count color
        FlagsGrid.Cell(_idx_row, GRID_FLAG_NAME).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
        FlagsGrid.Cell(_idx_row, GRID_FLAG_COUNT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
        FlagsGrid.Cell(_idx_row, GRID_FLAG_COUNT).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
        'FlagsGrid.Cell(_idx_row, GRID_FLAG_COUNT).Value = ""
      Next
    End If
  End Sub

  ''' <summary>
  ''' Manage parent controls to search form and call function OnClick_AfectedAccountsByFlags
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub OpenAffectedAccountsByFlags()

    ' Check if ParentForm is of specific type and FlagsGrid is ok
    If Not frm_treeview_edit.CheckParentFormType(Me) Then
      ' "Error al cargar la pantalla de 'Cuentas afectadas'" 
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8294), ENUM_MB_TYPE.MB_TYPE_ERROR)

    End If

    If Me.CheckFlagGridData() Then

      Call Me.GetFlagsGrid()

      ' Call a method to show Affected Accounts 
      CType(Me.ParentForm, frm_treeview_edit).ClickEventManager(ENUM_BUTTON_CLICK_EVENT_TYPE.EVENT_AFFECTED_ACCOUNTS)

    End If

  End Sub ' OpenAffectedAccountsByFlags

  ''' <summary>
  ''' Check flag Grid ok
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckFlagGridData() As Boolean

    Select Case IsFlagGridDataOK(dg_flags)
      Case GRID_DATA_STATUS.VALUE_NOT_VALID ' Flag checked without a count
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1398), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Return False
      Case GRID_DATA_STATUS.NO_ONE_CHECKED ' No one flag checked
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1333), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        'btn_accounts_afected.Enabled = chk_by_flag.Checked
        Return False
      Case GRID_DATA_STATUS.MAX_FLAGS_EXCEEDED
      Case GRID_DATA_STATUS.OK
        Return True
      Case Else
        Return True
    End Select

  End Function

#End Region

#Region "Events"

  ' ''' <summary>
  ' ''' Before edit check if the automatic flag, if it is stop the edition
  ' ''' </summary>
  ' ''' <param name="Row"></param>
  ' ''' <param name="Column"></param>
  ' ''' <param name="EditionCanceled"></param>
  ' ''' <remarks></remarks>
  'Private Sub dg_flags_BeforeStartEditionEvent(Row As Integer, Column As Integer, ByRef EditionCanceled As Boolean) Handles dg_flags.BeforeStartEditionEvent

  '  If dg_flags.Cell(Row, GRID_FLAG_TYPE).Value <> 0 Then
  '    EditionCanceled = True
  '  End If

  'End Sub ' dg_flags_BeforeStartEditionEvent

  ''' <summary>
  ''' Event of change state of checkbox of grid
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="Column"></param>
  ''' <remarks></remarks>
  Private Sub dg_flags_CellDataChangedEvent(Row As Integer, Column As Integer) Handles dg_flags.CellDataChangedEvent

    If dg_flags.Redraw = False Then
      Return
    End If

    If Column = GRID_FLAG_COUNT Then
      If String.IsNullOrEmpty(dg_flags.Cell(Row, GRID_FLAG_COUNT).Value) Then
        dg_flags.Cell(Row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      Else
        dg_flags.Cell(Row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
      End If
    End If

  End Sub ' dg_flags_CellDataChangedEvent

  ''' <summary>
  ''' Event to open Affected accounts screen
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub btn_accounts_afected_Click(sender As Object, e As EventArgs) Handles btn_accounts_afected.Click
    Me.OpenAffectedAccountsByFlags()
  End Sub ' btn_accounts_afected_Click

  ''' <summary>
  ''' Event on checked change chk_by_flag control
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub chk_by_flag_CheckedChanged(sender As Object, e As EventArgs) Handles chk_by_flag.CheckedChanged 'chk_by_flag_CheckedChanged
    Me.FlagsCheckManager()
  End Sub 'chk_by_flag_CheckedChanged

#End Region

#Region "Table Flags"

  ''' <summary>
  ''' Sub to init flag grid
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitFlagGrid(ByRef FlagsGrid As uc_grid) 'InitFlagGrid

    Dim _idx As Integer

    ' Init Grid
    FlagsGrid.Clear()
    If Not CheckFlagRequired() Then
      FlagsGrid.Settings.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
      FlagsGrid.Enabled = False
    Else
      FlagsGrid.Settings.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      FlagsGrid.Enabled = True
    End If

    ' Add flags to grid
    For _idx = 0 To Flags.Items.Count - 1
      Me.AddFlagToGrid(FlagsGrid, Flags.Items(_idx), _idx)
    Next

  End Sub 'InitFlagGrid

  ''' <summary>
  ''' Add flag to datagrid
  ''' </summary>
  ''' <param name="FlagsGrid"></param>
  ''' <param name="FlagItem"></param>
  ''' <param name="FlagIndex"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function AddFlagToGrid(ByRef FlagsGrid As uc_grid, ByVal FlagItem As JackpotAwardFilterFlag, ByVal FlagIndex As Integer) As Integer

    FlagsGrid.AddRow()

    ' Promotion_Flag_Id
    FlagsGrid.Cell(FlagIndex, GRID_FLAG_IDX).Value = FlagItem.FlagId

    ' Flag Name
    FlagsGrid.Cell(FlagIndex, GRID_FLAG_NAME).Value = FlagItem.Name

    ' Paint cell
    Me.PaintDataGridCell(FlagsGrid, FlagItem.Color, FlagIndex)

    'Flag Count
    Me.SetFlagCount(FlagsGrid, FlagItem, FlagIndex)

  End Function ' AddFlagToGrid

  ''' <summary>
  ''' Paint datagrid cell
  ''' </summary>
  ''' <param name="FlagsGrid"></param>
  ''' <param name="FlagColor"></param>
  ''' <param name="FlagIndex"></param>
  ''' <remarks></remarks>
  Private Sub PaintDataGridCell(ByRef FlagsGrid As uc_grid, ByVal FlagColor As Integer, FlagIndex As Integer)
    Dim _color As Color
    ' Flag Color
    FlagsGrid.Cell(FlagIndex, GRID_FLAG_ID_COLOR).Value = 0
    _color = Color.FromArgb(FlagColor)

    ' Check if its black color. (Chapuza) TODO UNRANKED - JBP: Buscar alternativa. 
    If Me.IsBlackColor(_color) Then
      _color = Color.FromArgb(_color.A, _color.R, _color.G, _color.B + 1)
    End If

    FlagsGrid.Cell(FlagIndex, GRID_FLAG_COLOR).BackColor = _color

  End Sub ' PaintDataGridCell

  ''' <summary>
  ''' Check if color is black
  ''' </summary>
  ''' <param name="FlagColor"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function IsBlackColor(ByVal FlagColor As Color) As Boolean
    Return (FlagColor.A = Color.Black.A And FlagColor.R = Color.Black.R And FlagColor.G = Color.Black.G And FlagColor.B = Color.Black.B)
  End Function ' IsBlackColor

  ''' <summary>
  ''' Set flag count
  ''' </summary>
  ''' <param name="FlagsGrid"></param>
  ''' <param name="FlagItem"></param>
  ''' <param name="FlagIndex"></param>
  ''' <remarks></remarks>
  Private Sub SetFlagCount(ByRef FlagsGrid As uc_grid, ByVal FlagItem As JackpotAwardFilterFlag, ByVal FlagIndex As Integer)

    ' TODO UNRANKED: REFACTOR
    If (String.IsNullOrEmpty(FlagItem.FlagCount) OrElse FlagItem.FlagCount = 0) Then

      SetupCheckedColumnGrid(FlagsGrid, FlagItem, True, FlagIndex)
    Else

      SetupCheckedColumnGrid(FlagsGrid, FlagItem, False, FlagIndex)
    End If

  End Sub ' SetFlagCount

  ''' <summary>
  ''' setup checked column grid on set flag count
  ''' </summary>
  ''' <param name="FlagsGrid"></param>
  ''' <param name="flagCount"></param>
  ''' <param name="FlagIndex"></param>
  ''' <remarks></remarks>
  Private Sub SetupCheckedColumnGrid(ByRef FlagsGrid As uc_grid, ByVal FlagItem As JackpotAwardFilterFlag, ByVal flagCount As Boolean, FlagIndex As Integer)

    If flagCount Then

      FlagsGrid.Cell(FlagIndex, GRID_FLAG_COUNT).Value = String.Empty

      If FlagsGrid.Enabled Then

        FlagsGrid.Cell(FlagIndex, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED

      Else
        FlagsGrid.Cell(FlagIndex, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED

      End If
    Else

      FlagsGrid.Cell(FlagIndex, GRID_FLAG_COUNT).Value = GUI_FormatNumber(FlagItem.FlagCount, 0)

      If FlagsGrid.Enabled Then

        FlagsGrid.Cell(FlagIndex, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED

      Else

        FlagsGrid.Cell(FlagIndex, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED

      End If

    End If

  End Sub

#End Region

End Class
