﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_jackpot_contribution_compensation
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_compensation = New System.Windows.Forms.GroupBox()
    Me.lbl_pct = New System.Windows.Forms.Label()
    Me.lbl_pct_2 = New System.Windows.Forms.Label()
    Me.lbl_pct_1 = New System.Windows.Forms.Label()
    Me.lbl_pct_0 = New System.Windows.Forms.Label()
    Me.lbl_add = New System.Windows.Forms.Label()
    Me.txt_compensate_2 = New System.Windows.Forms.TextBox()
    Me.txt_compensate_1 = New System.Windows.Forms.TextBox()
    Me.txt_compensate_0 = New System.Windows.Forms.TextBox()
    Me.rb_compensate2 = New System.Windows.Forms.RadioButton()
    Me.rb_compensate1 = New System.Windows.Forms.RadioButton()
    Me.rb_compensate0 = New System.Windows.Forms.RadioButton()
    Me.lbl_c2_warning = New System.Windows.Forms.Label()
    Me.ef_compensate = New GUI_Controls.uc_entry_field()
    Me.gb_compensation.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_compensation
    '
    Me.gb_compensation.Controls.Add(Me.ef_compensate)
    Me.gb_compensation.Controls.Add(Me.lbl_pct)
    Me.gb_compensation.Controls.Add(Me.lbl_pct_2)
    Me.gb_compensation.Controls.Add(Me.lbl_pct_1)
    Me.gb_compensation.Controls.Add(Me.lbl_pct_0)
    Me.gb_compensation.Controls.Add(Me.lbl_add)
    Me.gb_compensation.Controls.Add(Me.txt_compensate_2)
    Me.gb_compensation.Controls.Add(Me.txt_compensate_1)
    Me.gb_compensation.Controls.Add(Me.txt_compensate_0)
    Me.gb_compensation.Controls.Add(Me.rb_compensate2)
    Me.gb_compensation.Controls.Add(Me.rb_compensate1)
    Me.gb_compensation.Controls.Add(Me.rb_compensate0)
    Me.gb_compensation.Location = New System.Drawing.Point(0, 0)
    Me.gb_compensation.Name = "gb_compensation"
    Me.gb_compensation.Size = New System.Drawing.Size(246, 104)
    Me.gb_compensation.TabIndex = 0
    Me.gb_compensation.TabStop = False
    '
    'lbl_pct
    '
    Me.lbl_pct.AutoSize = True
    Me.lbl_pct.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_pct.Location = New System.Drawing.Point(222, 52)
    Me.lbl_pct.Name = "lbl_pct"
    Me.lbl_pct.Size = New System.Drawing.Size(18, 15)
    Me.lbl_pct.TabIndex = 19
    Me.lbl_pct.Text = "%"
    '
    'lbl_pct_2
    '
    Me.lbl_pct_2.AutoSize = True
    Me.lbl_pct_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_pct_2.Location = New System.Drawing.Point(106, 77)
    Me.lbl_pct_2.Name = "lbl_pct_2"
    Me.lbl_pct_2.Size = New System.Drawing.Size(18, 15)
    Me.lbl_pct_2.TabIndex = 18
    Me.lbl_pct_2.Text = "%"
    '
    'lbl_pct_1
    '
    Me.lbl_pct_1.AutoSize = True
    Me.lbl_pct_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_pct_1.Location = New System.Drawing.Point(106, 50)
    Me.lbl_pct_1.Name = "lbl_pct_1"
    Me.lbl_pct_1.Size = New System.Drawing.Size(18, 15)
    Me.lbl_pct_1.TabIndex = 17
    Me.lbl_pct_1.Text = "%"
    '
    'lbl_pct_0
    '
    Me.lbl_pct_0.AutoSize = True
    Me.lbl_pct_0.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_pct_0.Location = New System.Drawing.Point(108, 21)
    Me.lbl_pct_0.Name = "lbl_pct_0"
    Me.lbl_pct_0.Size = New System.Drawing.Size(18, 15)
    Me.lbl_pct_0.TabIndex = 16
    Me.lbl_pct_0.Text = "%"
    '
    'lbl_add
    '
    Me.lbl_add.AutoSize = True
    Me.lbl_add.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_add.Location = New System.Drawing.Point(125, 47)
    Me.lbl_add.Name = "lbl_add"
    Me.lbl_add.Size = New System.Drawing.Size(25, 26)
    Me.lbl_add.TabIndex = 6
    Me.lbl_add.Text = "+"
    '
    'txt_compensate_2
    '
    Me.txt_compensate_2.Location = New System.Drawing.Point(56, 75)
    Me.txt_compensate_2.MaxLength = 6
    Me.txt_compensate_2.Name = "txt_compensate_2"
    Me.txt_compensate_2.ReadOnly = True
    Me.txt_compensate_2.Size = New System.Drawing.Size(47, 20)
    Me.txt_compensate_2.TabIndex = 5
    Me.txt_compensate_2.TabStop = False
    Me.txt_compensate_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'txt_compensate_1
    '
    Me.txt_compensate_1.Location = New System.Drawing.Point(56, 47)
    Me.txt_compensate_1.MaxLength = 6
    Me.txt_compensate_1.Name = "txt_compensate_1"
    Me.txt_compensate_1.ReadOnly = True
    Me.txt_compensate_1.Size = New System.Drawing.Size(47, 20)
    Me.txt_compensate_1.TabIndex = 3
    Me.txt_compensate_1.TabStop = False
    Me.txt_compensate_1.Text = "100,00"
    Me.txt_compensate_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'txt_compensate_0
    '
    Me.txt_compensate_0.Location = New System.Drawing.Point(56, 19)
    Me.txt_compensate_0.MaxLength = 6
    Me.txt_compensate_0.Name = "txt_compensate_0"
    Me.txt_compensate_0.ReadOnly = True
    Me.txt_compensate_0.Size = New System.Drawing.Size(47, 20)
    Me.txt_compensate_0.TabIndex = 1
    Me.txt_compensate_0.TabStop = False
    Me.txt_compensate_0.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    '
    'rb_compensate2
    '
    Me.rb_compensate2.AutoSize = True
    Me.rb_compensate2.Location = New System.Drawing.Point(16, 79)
    Me.rb_compensate2.Name = "rb_compensate2"
    Me.rb_compensate2.Size = New System.Drawing.Size(38, 17)
    Me.rb_compensate2.TabIndex = 4
    Me.rb_compensate2.TabStop = True
    Me.rb_compensate2.Text = "C2"
    Me.rb_compensate2.UseVisualStyleBackColor = True
    '
    'rb_compensate1
    '
    Me.rb_compensate1.AutoSize = True
    Me.rb_compensate1.Location = New System.Drawing.Point(16, 49)
    Me.rb_compensate1.Name = "rb_compensate1"
    Me.rb_compensate1.Size = New System.Drawing.Size(38, 17)
    Me.rb_compensate1.TabIndex = 2
    Me.rb_compensate1.TabStop = True
    Me.rb_compensate1.Text = "C1"
    Me.rb_compensate1.UseVisualStyleBackColor = True
    '
    'rb_compensate0
    '
    Me.rb_compensate0.AutoSize = True
    Me.rb_compensate0.Location = New System.Drawing.Point(16, 19)
    Me.rb_compensate0.Name = "rb_compensate0"
    Me.rb_compensate0.Size = New System.Drawing.Size(38, 17)
    Me.rb_compensate0.TabIndex = 0
    Me.rb_compensate0.TabStop = True
    Me.rb_compensate0.Text = "C0"
    Me.rb_compensate0.UseVisualStyleBackColor = True
    '
    'lbl_c2_warning
    '
    Me.lbl_c2_warning.AutoSize = True
    Me.lbl_c2_warning.ForeColor = System.Drawing.Color.Navy
    Me.lbl_c2_warning.Location = New System.Drawing.Point(4, 111)
    Me.lbl_c2_warning.Name = "lbl_c2_warning"
    Me.lbl_c2_warning.Size = New System.Drawing.Size(86, 13)
    Me.lbl_c2_warning.TabIndex = 1
    Me.lbl_c2_warning.Text = " xlbl_c2_warning"
    '
    'ef_compensate
    '
    Me.ef_compensate.DoubleValue = 0.0R
    Me.ef_compensate.IntegerValue = 0
    Me.ef_compensate.IsReadOnly = False
    Me.ef_compensate.Location = New System.Drawing.Point(153, 47)
    Me.ef_compensate.Name = "ef_compensate"
    Me.ef_compensate.PlaceHolder = Nothing
    Me.ef_compensate.Size = New System.Drawing.Size(65, 24)
    Me.ef_compensate.SufixText = "Sufix Text"
    Me.ef_compensate.SufixTextVisible = True
    Me.ef_compensate.TabIndex = 20
    Me.ef_compensate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    Me.ef_compensate.TextValue = ""
    Me.ef_compensate.TextWidth = 1
    Me.ef_compensate.Value = ""
    Me.ef_compensate.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_jackpot_contribution_compensation
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
    Me.Controls.Add(Me.lbl_c2_warning)
    Me.Controls.Add(Me.gb_compensation)
    Me.Name = "uc_jackpot_contribution_compensation"
    Me.Size = New System.Drawing.Size(252, 146)
    Me.gb_compensation.ResumeLayout(False)
    Me.gb_compensation.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents gb_compensation As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_add As System.Windows.Forms.Label
  Friend WithEvents txt_compensate_2 As System.Windows.Forms.TextBox
  Friend WithEvents txt_compensate_1 As System.Windows.Forms.TextBox
  Friend WithEvents txt_compensate_0 As System.Windows.Forms.TextBox
  Friend WithEvents rb_compensate2 As System.Windows.Forms.RadioButton
  Friend WithEvents rb_compensate1 As System.Windows.Forms.RadioButton
  Friend WithEvents rb_compensate0 As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_pct_2 As System.Windows.Forms.Label
  Friend WithEvents lbl_pct_1 As System.Windows.Forms.Label
  Friend WithEvents lbl_pct_0 As System.Windows.Forms.Label
  Friend WithEvents lbl_pct As System.Windows.Forms.Label
  Friend WithEvents ef_compensate As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_c2_warning As System.Windows.Forms.Label

End Class
