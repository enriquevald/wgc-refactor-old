﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' UC NAME:       uc_jackpot_award_happy_hour
' DESCRIPTION:   Jackpot User Control for happy hour configuration
' AUTHOR:        Rubén Lama Ordóñez
' CREATION DATE: 27-APR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 27-APR-2017  RLO    Initial version
'--------------------------------------------------------------------

#Region "Imports"

Imports GUI_CommonOperations
Imports System.Windows.Forms
Imports GUI_CommonMisc.mdl_NLS
Imports WSI.Common
Imports WSI.Common.Jackpot
Imports WSI.Common.Jackpot.Jackpot

#End Region


Public Class uc_jackpot_award_happy_hour
  Inherits uc_treeview_edit_base

#Region "Members"

  Private m_happy_hour As JackpotAwardHappyHour

  Private Const MAX_TIME = 60
  Private Const MIN_TIME = 1
  Private Const MAX_AWARD = 99
  Private Const MIN_AWARD = 1

#End Region

#Region "Properties"

  Public Property HappyHour As JackpotAwardHappyHour
    Get
      Return Me.m_happy_hour
    End Get
    Set(value As JackpotAwardHappyHour)
      Me.m_happy_hour = value
    End Set
  End Property

  Public Property BeforeJackpot As CheckBox
    Get
      Return Me.chk_before_jackpot
    End Get
    Set(value As CheckBox)
      Me.chk_before_jackpot = value
    End Set
  End Property

  Public Property AfterJackpot As CheckBox
    Get
      Return Me.chk_after_jackpot
    End Get
    Set(value As CheckBox)
      Me.chk_after_jackpot = value
    End Set
  End Property

  Public Property HappyHourTimeBefore As uc_entry_field
    Get
      Return Me.ef_time_before
    End Get
    Set(value As uc_entry_field)
      Me.ef_time_before = value

    End Set
  End Property

  Public Property HappyHourTimeAfter As uc_entry_field
    Get
      Return Me.ef_time_after
    End Get
    Set(value As uc_entry_field)
      Me.ef_time_after = value
    End Set
  End Property

  Public Property HappyHourAwardBefore As uc_entry_field
    Get
      Return Me.ef_awards_before
    End Get
    Set(value As uc_entry_field)
      Me.ef_awards_before = value
    End Set
  End Property

  Public Property HappyHourAwardAfter As uc_entry_field
    Get
      Return Me.ef_awards_after
    End Get
    Set(value As uc_entry_field)
      Me.ef_awards_after = value
    End Set
  End Property

  Public Property HappyHourMinimum As uc_entry_field
    Get
      Return Me.ef_minimum_amount
    End Get
    Set(value As uc_entry_field)
      Me.ef_minimum_amount = value
    End Set
  End Property

  Public Property HappyHourMaximum As uc_entry_field
    Get
      Return Me.ef_maximum_amount
    End Get
    Set(value As uc_entry_field)
      Me.ef_maximum_amount = value
    End Set
  End Property

  Public Property HappyHourAmountBefore As uc_combo
    Get
      Return Me.cmb_amount_be
    End Get
    Set(value As uc_combo)
      Me.cmb_amount_be = value
    End Set
  End Property

  Public Property HappyHourAmountAfter As uc_combo
    Get
      Return Me.cmb_amount_after
    End Get
    Set(value As uc_combo)
      Me.cmb_amount_after = value
    End Set
  End Property


#End Region

#Region "Sub"

  Public Sub New()

    Call Me.Init()

  End Sub

  ''' <summary>
  ''' Init method
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub Init()

    ' This call is required by the designer.
    Call InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControls()

  End Sub

  ''' <summary>
  ''' Initialite uc controls
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub InitControls()

    'Before fields
    Me.chk_before_jackpot.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8213)
    Me.cmb_amount_be.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8216)
    Me.ef_awards_before.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8215)
    Me.ef_awards_before.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_time_before.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8214)
    Me.ef_time_before.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)


    'After Fields
    Me.chk_after_jackpot.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8217)
    Me.cmb_amount_after.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8216)
    Me.ef_awards_after.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8215)
    Me.ef_awards_after.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_time_after.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8214)
    Me.ef_time_after.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)

    'Minimum and maximum
    Me.ef_minimum_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8023)
    Me.ef_minimum_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    Me.ef_maximum_amount.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8025)
    Me.ef_maximum_amount.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)

    'gb_happy_hour_config
    Me.gb_happy_hour_config.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(558) 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)

    Call LoadComboAmountDistribution()

    Call EnableMinMaxAmountFields()

  End Sub ' UC_InitControls

  ''' <summary>
  ''' Enable Before Fields
  ''' </summary>
  ''' <param name="enabled"></param>
  ''' <remarks></remarks>
  Private Sub EnableBeforeFields(ByVal enabled As Boolean)

    Me.pnl_before_jackpot.Enabled = enabled

  End Sub ' EnableBeforeFields

  ''' <summary>
  ''' Enable After Fields
  ''' </summary>
  ''' <param name="enabled"></param>
  ''' <remarks></remarks>
  Private Sub EnableAfterFields(ByVal enabled As Boolean)

    pnl_after_jackpot.Enabled = enabled

  End Sub ' EnableBeforeFields

  ''' <summary>
  ''' EnableMinMaxAmountFields
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub EnableMinMaxAmountFields()
    Me.ef_minimum_amount.Enabled = (Me.chk_after_jackpot.Checked Or Me.chk_before_jackpot.Checked)

    Me.ef_maximum_amount.Enabled = Me.ef_minimum_amount.Enabled

  End Sub 'EnableMinMaxAmountFields

  ''' <summary>
  ''' Bind uc data
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub BindData(ByVal AwardHappyHour As JackpotAwardHappyHour)

    ' Bind all data to controls
    If Not AwardHappyHour Is Nothing Then

      Me.HappyHour = AwardHappyHour

      Me.HappyHourAmountAfter.Value = Me.HappyHour.AmountDistributionAfter
      Me.HappyHourAmountBefore.Value = Me.HappyHour.AmountDistributionBefore
      Me.HappyHourAwardAfter.Value = Me.HappyHour.AwardsAfter
      Me.HappyHourAwardBefore.Value = Me.HappyHour.AwardsBefore

      Call BindEventType()

      Me.HappyHourTimeAfter.Value = Me.HappyHour.TimeAfter
      Me.HappyHourTimeBefore.Value = Me.HappyHour.TimeBefore

      Me.HappyHourMinimum.Value = Me.HappyHour.Minimum
      Me.HappyHourMaximum.Value = Me.HappyHour.Maximum

    End If

  End Sub ' BindData

  Private Sub LoadComboAmountDistribution()
    Dim _str_text As String
    Dim dict As New Dictionary(Of Integer, String)

    _str_text = String.Empty

    cmb_amount_after.Clear()
    cmb_amount_be.Clear()
    For Each item As Object In [Enum].GetValues(GetType(Jackpot.AwardFilterAmountDistribution))
      Select Case item
        Case Jackpot.AwardFilterAmountDistribution.HighToLow
          _str_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8238)
        Case Jackpot.AwardFilterAmountDistribution.LowToHigh
          _str_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8237)
        Case Jackpot.AwardFilterAmountDistribution.Uniform
          _str_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8236)

      End Select

      dict.Add((CInt(item)), _str_text)
    Next

    cmb_amount_after.Add(dict)
    cmb_amount_be.Add(dict)

  End Sub

#End Region

#Region "Public Functions "

  ''' <summary>
  ''' Sub to check screen data
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function IsScreenDataOk() As Boolean

    'null fields
    If String.IsNullOrEmpty(ef_awards_before.Value.ToString) Then
      ef_awards_before.Value = Jackpot.DEFAULT_INT_VALUE
    End If

    If String.IsNullOrEmpty(ef_time_before.Value.ToString) Then
      ef_time_before.Value = Jackpot.DEFAULT_INT_VALUE
    End If

    If String.IsNullOrEmpty(ef_awards_after.Value.ToString) Then
      ef_awards_after.Value = Jackpot.DEFAULT_INT_VALUE
    End If

    If String.IsNullOrEmpty(ef_time_after.Value.ToString) Then
      ef_time_after.Value = Jackpot.DEFAULT_INT_VALUE
    End If

    '' Minimum Empty
    If String.IsNullOrEmpty(Me.ef_minimum_amount.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8098), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
      Call Me.ef_minimum_amount.Focus()

      Return False
    End If

    '' Minimum Empty
    If String.IsNullOrEmpty(Me.ef_maximum_amount.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8099), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
      Call Me.ef_maximum_amount.Focus()

      Return False
    End If

    ' Before
    If Me.chk_before_jackpot.Checked Then
      If Not Me.CheckHappyHourGroup(GUI_ParseNumber(Me.ef_time_before.Value), GUI_ParseNumber(Me.ef_awards_before.Value)) Then
        Return False
      End If
    End If


    ' After
    If Me.chk_after_jackpot.Checked Then
      If Not Me.CheckHappyHourGroup(GUI_ParseNumber(Me.ef_time_after.Value), GUI_ParseNumber(Me.ef_awards_after.Value)) Then
        Return False
      End If
    End If

    If Me.ef_minimum_amount.Enabled Then
      If GUI_ParseNumberDecimal(Me.HappyHourMinimum.Value) > GUI_ParseNumberDecimal(Me.HappyHourMaximum.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8344), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
        Return False
      End If
      If GUI_ParseNumberDecimal(Me.HappyHourMinimum.Value) = GUI_ParseNumberDecimal(Me.HappyHourMaximum.Value) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8356), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)
        Return False
      End If
    End If

    Return True

  End Function ' IsScreenDataOk

  ''' <summary>
  ''' Get all data 
  ''' </summary>
  ''' <remarks></remarks>
  Public Function ToJackpotAwardHappyHour() As JackpotAwardHappyHour

    Me.HappyHour.AmountDistributionAfter = cmb_amount_after.Value
    Me.HappyHour.AmountDistributionBefore = cmb_amount_be.Value
    Me.HappyHour.AwardsAfter = ef_awards_after.Value
    Me.HappyHour.AwardsBefore = ef_awards_before.Value

    Call SetEventType()

    Me.HappyHour.TimeAfter = ef_time_after.Value
    Me.HappyHour.TimeBefore = ef_time_before.Value

    Me.HappyHour.Minimum = GUI_ParseNumberDecimal(ef_minimum_amount.Value)
    Me.HappyHour.Maximum = GUI_ParseNumberDecimal(ef_maximum_amount.Value)

    ' Set last modification
    Me.HappyHour.LastModification = WGDB.Now

    Return Me.HappyHour

  End Function

  ''' <summary>
  ''' Sub to set event type 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub SetEventType()

    If chk_after_jackpot.Checked And chk_before_jackpot.Checked Then
      Me.HappyHour.EventType = AwardHappyHourEvents.BothEvents

    ElseIf chk_after_jackpot.Checked Then
      Me.HappyHour.EventType = AwardHappyHourEvents.AfterJackpot

    ElseIf chk_before_jackpot.Checked Then
      Me.HappyHour.EventType = AwardHappyHourEvents.BeforeJackpot

    Else
      Me.HappyHour.EventType = AwardHappyHourEvents.Disabled
    End If

  End Sub

  ''' <summary>
  ''' Sub to bind event type
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub BindEventType()

    Select Case Me.HappyHour.EventType

      Case AwardHappyHourEvents.Disabled
        chk_after_jackpot.Checked = False
        chk_before_jackpot.Checked = False

      Case AwardHappyHourEvents.AfterJackpot
        chk_after_jackpot.Checked = True
        chk_before_jackpot.Checked = False

      Case AwardHappyHourEvents.BeforeJackpot
        chk_after_jackpot.Checked = False
        chk_before_jackpot.Checked = True

      Case AwardHappyHourEvents.BothEvents
        chk_after_jackpot.Checked = True
        chk_before_jackpot.Checked = True

    End Select

    Call DisableControls(Me.chk_before_jackpot.Checked, Me.chk_after_jackpot.Checked)

  End Sub ' BindEventType

  ''' <summary>
  ''' Sub to disable controls 
  ''' </summary>
  ''' <param name="after"></param>
  ''' <param name="before"></param>
  ''' <remarks></remarks>
  Private Sub DisableControls(before As Boolean, after As Boolean)

    Call EnableAfterFields(after)
    Call EnableBeforeFields(before)

  End Sub

  ''' <summary>
  ''' Check Happy Hour group
  ''' </summary>
  ''' <param name="Time"></param>
  ''' <param name="Awards"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckHappyHourGroup(ByVal Time As Double, ByVal Awards As Double) As Boolean

    'Time
    If Time < MIN_TIME Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8276), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El valor de tiempo ha de ser mayor que 0
      Call Me.ef_time_after.Focus()

      Return False
    End If

    If Time > MAX_TIME Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8277), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El valor de tiempo ha de ser menor que 60
      Call Me.ef_time_after.Focus()

      Return False
    End If

    'Awards 
    If Awards < MIN_AWARD Or Awards > MAX_AWARD Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8275), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El valor de premios ha de estar entre 1 y 99
      Call Me.ef_awards_after.Focus()

      Return False
    End If

    Return True

  End Function ' CheckHappyHourGroup

#End Region

#Region "Events"

  Public Event CheckedChanged()

  Private Sub chk_before_jackpot_CheckedChanged(sender As Object, e As EventArgs) Handles chk_before_jackpot.CheckedChanged

    EnableBeforeFields(chk_before_jackpot.Checked)
    EnableMinMaxAmountFields()

    RaiseEvent CheckedChanged()

  End Sub

  Private Sub chk_after_jackpot_CheckedChanged(sender As Object, e As EventArgs) Handles chk_after_jackpot.CheckedChanged

    EnableAfterFields(chk_after_jackpot.Checked)
    EnableMinMaxAmountFields()

    RaiseEvent CheckedChanged()

  End Sub

#End Region

End Class
