﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' UC NAME:       uc_jackpot_contribution_group
' DESCRIPTION:   Jackpot User Control For show groups of contribution
' AUTHOR:        Ferran Ortner
' CREATION DATE: 05-ABR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 05-ABR-2017  FOS    Initial version
'--------------------------------------------------------------------

Imports System.Windows.Forms
Imports WSI.Common
Imports WSI.Common.Jackpot
Imports GUI_CommonMisc
Imports System.Drawing

Public Class uc_jackpot_contribution_groups

  Dim m_max_tab As Integer
  Dim m_tab_name As String
  Dim m_uc_jackpot_contribution_list As List(Of uc_jackpot_contribution)

  Dim m_jackpot_contribution_list As JackpotContributionList

#Region " Public Methods "

  Public Sub New()
    ' This call is required by the designer.
    Call InitializeComponent()
    Call InitControls()
  End Sub

  Public Function IsScreenDataOk() As Boolean
    Dim _uc_jackpot_contr As uc_jackpot_contribution
    Dim _tab_page_index As Integer

    For _tab_page_index = 0 To tb_group.TabPages.Count - 1
      For Each _ctrl As Control In Me.tb_group.TabPages(_tab_page_index).Controls()

        If TypeOf _ctrl Is uc_jackpot_contribution Then
          _uc_jackpot_contr = CType(_ctrl, uc_jackpot_contribution)

          If Not _uc_jackpot_contr.IsScreenDataOk() Then
            Me.tb_group.SelectTab(_tab_page_index)

            Return False
          End If
        End If

      Next
    Next

    Return True

  End Function

  ''' <summary>
  ''' Map controls to JackpotContributionList class
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function ToJackpotContributionList(ByVal JackpotId As Integer) As JackpotContributionList
    Dim _current_contribution As uc_jackpot_contribution
    Dim _contribution_list As JackpotContributionList
    Dim _index As Integer

    Call RemovePlusTub()

    For Each _tab_page As TabPage In Me.tb_group.TabPages
      _current_contribution = CType(_tab_page.Controls(0), uc_jackpot_contribution)

      If JackpotContributionAlreadyInTabControl(_current_contribution.Contribution) Then ' Contribution In use
        _index = Convert.ToInt32(_current_contribution.Contribution.Name) - 1
        Me.m_jackpot_contribution_list.Items(_index) = _current_contribution.ToJackpotContribution(JackpotId)
      End If

    Next

    Call AddPlusTab()

    _contribution_list = New JackpotContributionList()
    _contribution_list.JackpotId = JackpotId

    For Each _contribution As JackpotContribution In Me.m_jackpot_contribution_list.Items
      If _contribution.ContributionState = Jackpot.ContributionGroupState.InUse Or
         _contribution.ContributionState = Jackpot.ContributionGroupState.Deleted Then

        _contribution_list.Items.Add(_contribution)
      End If
    Next

    Return _contribution_list
  End Function

  Public Function BindData(ContributionList As JackpotContributionList)

    Try

      Call InitJackpotContributionList(ContributionList) ' To fill the rest of the list with NotInUser jackpot contribution groups
      Call UpdateTabControl()

    Catch ex As Exception
      Log.Error("uc_jackpot_contribution_groups BindData: There was an error Binding data")
      Return False
    End Try

    Return True
  End Function

#End Region

  Private Sub InitJackpotContributionList(ContributionList As JackpotContributionList)
    Dim _contribution As JackpotContribution

    Me.m_jackpot_contribution_list = New JackpotContributionList()

    ' Init list with empty JackpotContribution objects
    For i As Integer = 0 To Me.m_max_tab - 1
      Me.m_jackpot_contribution_list.Items.Add(New JackpotContribution())
    Next

    For i As Integer = 0 To Me.m_max_tab - 1
      _contribution = GetSavedJackpotContributionIfExist(i, ContributionList)

      If Not _contribution Is Nothing Then
        Me.m_jackpot_contribution_list.Items(i) = _contribution
        ContributionList.Items.Remove(_contribution)
      Else
        Me.m_jackpot_contribution_list.Items(i) = New JackpotContribution()
        Me.m_jackpot_contribution_list.Items(i).Name = (i + 1).ToString()
      End If

    Next

    If ContributionList.Items.Count = 0 Then
      Me.m_jackpot_contribution_list.Items(0).ContributionState = Jackpot.ContributionGroupState.InUse
    End If

  End Sub

  Private Function GetSavedJackpotContributionIfExist(CurrentPosition As Integer, ContributionList As JackpotContributionList) As JackpotContribution

    For Each _current_contribution As JackpotContribution In ContributionList.Items

      If Convert.ToInt32(_current_contribution.Name) - 1 = CurrentPosition Then
        Return _current_contribution
      End If

    Next

    Return Nothing
  End Function

  Private Sub UpdateTabControl(Optional TabIndexToDelete As Integer = -1, Optional SelectLastTab As Boolean = False)
    Dim _current_tab As TabPage

    Call RemovePlusTub()

    For Each _jackpot_contribution As JackpotContribution In Me.m_jackpot_contribution_list.Items

      If _jackpot_contribution.ContributionState = Jackpot.ContributionGroupState.InUse Then
        If Not JackpotContributionAlreadyInTabControl(_jackpot_contribution) Then

          _current_tab = InitTabPage(_jackpot_contribution)
          Me.tb_group.TabPages.Add(_current_tab)

        End If
      End If

    Next

    If TabIndexToDelete > -1 Then
      Me.tb_group.TabPages.RemoveAt(TabIndexToDelete)
    End If

    If SelectLastTab Then
      Me.tb_group.SelectTab(tb_group.TabCount - 1)
    End If

    Call AddPlusTab()

  End Sub

  ''' <summary>
  ''' To Initialize a tab page
  ''' </summary>
  ''' <param name="JackpotContribution"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function InitTabPage(JackpotContribution As JackpotContribution) As TabPage
    Dim _new_tab_page As TabPage
    Dim _uc_jackpot_contribution As uc_jackpot_contribution

    _new_tab_page = New TabPage(String.Format(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8120), JackpotContribution.Name))
    _uc_jackpot_contribution = New uc_jackpot_contribution(JackpotContribution)
    Call InitButtonPanel(_uc_jackpot_contribution)
    _new_tab_page.Controls.Add(_uc_jackpot_contribution)

    Return _new_tab_page
  End Function ' InitTabPage

  Private Sub InitControls()
    Me.m_max_tab = 5
  End Sub

  Private Function JackpotContributionAlreadyInTabControl(JackpotContribution As JackpotContribution)
    Dim _uc_jackpot_contribution As uc_jackpot_contribution

    For Each _tab_page As TabPage In Me.tb_group.TabPages

      If TypeOf (_tab_page.Controls(0)) Is uc_jackpot_contribution Then
        _uc_jackpot_contribution = CType(_tab_page.Controls(0), uc_jackpot_contribution)

        If _uc_jackpot_contribution.Contribution.Name.Equals(JackpotContribution.Name) Then

          Return True
        End If
      End If

    Next

    Return False
  End Function

  ''' <summary>
  ''' To set the delete button for the contribution
  ''' </summary>
  ''' <param name="UcJackpotContribution"></param>
  ''' <remarks></remarks>
  Private Sub InitButtonPanel(ByRef UcJackpotContribution As uc_jackpot_contribution)
    Dim _btn_delete As Button

    _btn_delete = New Button()
    _btn_delete.MaximumSize = New Size(100, 25)
    _btn_delete.Text = GLB_NLS_GUI_CONTROLS.GetString(11)

    AddHandler _btn_delete.Click, AddressOf RemoveTab
    UcJackpotContribution.PanelDeleteButton.Controls.Add(_btn_delete)
    UcJackpotContribution.PanelDeleteButton.Controls(0).Location = New Point(519, 1)

  End Sub

  ''' <summary>
  ''' To add a tab with plus text
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub AddPlusTab()
    If Me.m_jackpot_contribution_list.GetContributionsInUse < Me.m_max_tab Then
      Me.tb_group.TabPages.Add(" + ")
    End If
  End Sub

  ''' <summary>
  ''' To remove plus tab
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub RemovePlusTub()
    Dim _last_index As Integer

    RemoveHandler Me.tb_group.Selected, AddressOf TabControl1_Selected

    _last_index = Me.tb_group.TabPages.Count - 1

    If Me.tb_group.TabPages.Count > 0 AndAlso
      Me.tb_group.TabPages(_last_index).Text.Equals(" + ") Then

      Me.tb_group.TabPages.RemoveAt(_last_index)

    End If

    AddHandler Me.tb_group.Selected, AddressOf TabControl1_Selected
  End Sub

  Private Sub SearchTab()

  End Sub

#Region "Events"

  Private Sub TabControl1_Selected(sender As Object, e As Windows.Forms.TabControlEventArgs) Handles tb_group.Selected

    If Not e.TabPage Is Nothing AndAlso e.TabPage.Text = " + " Then
      If Me.m_jackpot_contribution_list.AddNewContribution() Then
        Call UpdateTabControl(-1, True)
      End If
    End If

  End Sub

  Public Sub RemoveTab()
    Dim _index As Integer

    _index = 0

    If Me.tb_group.TabPages.Count > 2 Then

      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, , Me.tb_group.SelectedTab.Text) = ENUM_MB_RESULT.MB_RESULT_YES Then
        _index = Convert.ToInt32(Me.tb_group.SelectedTab.Text.Replace(GLB_NLS_GUI_PLAYER_TRACKING.GetString(8120).Replace("{0}", String.Empty), String.Empty)) - 1
        Me.m_jackpot_contribution_list.Items(_index).ContributionState = Jackpot.ContributionGroupState.Deleted
        Call UpdateTabControl(Me.tb_group.SelectedIndex)

      End If

    Else
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8127), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_OK)
    End If

  End Sub

#End Region ' Events 


End Class
