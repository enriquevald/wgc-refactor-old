﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_jackpot_customer_tier
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.gb_customer_sel = New System.Windows.Forms.GroupBox()
    Me.gb_vip_accounts = New System.Windows.Forms.GroupBox()
    Me.rb_just_no_vip_accounts = New System.Windows.Forms.RadioButton()
    Me.rb_just_vip_accounts = New System.Windows.Forms.RadioButton()
    Me.rb_any_account = New System.Windows.Forms.RadioButton()
    Me.chk_level_anonymous = New System.Windows.Forms.CheckBox()
    Me.chk_level_04 = New System.Windows.Forms.CheckBox()
    Me.chk_level_03 = New System.Windows.Forms.CheckBox()
    Me.chk_level_02 = New System.Windows.Forms.CheckBox()
    Me.chk_level_01 = New System.Windows.Forms.CheckBox()
    Me.chk_by_level = New System.Windows.Forms.CheckBox()
    Me.gb_customer_sel.SuspendLayout()
    Me.gb_vip_accounts.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_customer_sel
    '
    Me.gb_customer_sel.Controls.Add(Me.gb_vip_accounts)
    Me.gb_customer_sel.Controls.Add(Me.chk_level_anonymous)
    Me.gb_customer_sel.Controls.Add(Me.chk_level_04)
    Me.gb_customer_sel.Controls.Add(Me.chk_level_03)
    Me.gb_customer_sel.Controls.Add(Me.chk_level_02)
    Me.gb_customer_sel.Controls.Add(Me.chk_level_01)
    Me.gb_customer_sel.Controls.Add(Me.chk_by_level)
    Me.gb_customer_sel.Location = New System.Drawing.Point(4, 4)
    Me.gb_customer_sel.Name = "gb_customer_sel"
    Me.gb_customer_sel.Size = New System.Drawing.Size(254, 214)
    Me.gb_customer_sel.TabIndex = 0
    Me.gb_customer_sel.TabStop = False
    Me.gb_customer_sel.Text = "xCustomerTier"
    '
    'gb_vip_accounts
    '
    Me.gb_vip_accounts.Controls.Add(Me.rb_just_no_vip_accounts)
    Me.gb_vip_accounts.Controls.Add(Me.rb_just_vip_accounts)
    Me.gb_vip_accounts.Controls.Add(Me.rb_any_account)
    Me.gb_vip_accounts.Location = New System.Drawing.Point(6, 158)
    Me.gb_vip_accounts.Name = "gb_vip_accounts"
    Me.gb_vip_accounts.Size = New System.Drawing.Size(242, 50)
    Me.gb_vip_accounts.TabIndex = 7
    Me.gb_vip_accounts.TabStop = False
    Me.gb_vip_accounts.Text = "xVIPAccounts"
    '
    'rb_just_no_vip_accounts
    '
    Me.rb_just_no_vip_accounts.AutoSize = True
    Me.rb_just_no_vip_accounts.Location = New System.Drawing.Point(158, 20)
    Me.rb_just_no_vip_accounts.Name = "rb_just_no_vip_accounts"
    Me.rb_just_no_vip_accounts.Size = New System.Drawing.Size(78, 17)
    Me.rb_just_no_vip_accounts.TabIndex = 2
    Me.rb_just_no_vip_accounts.TabStop = True
    Me.rb_just_no_vip_accounts.Text = "xJustNoVip"
    Me.rb_just_no_vip_accounts.UseVisualStyleBackColor = True
    '
    'rb_just_vip_accounts
    '
    Me.rb_just_vip_accounts.AutoSize = True
    Me.rb_just_vip_accounts.Location = New System.Drawing.Point(76, 20)
    Me.rb_just_vip_accounts.Name = "rb_just_vip_accounts"
    Me.rb_just_vip_accounts.Size = New System.Drawing.Size(64, 17)
    Me.rb_just_vip_accounts.TabIndex = 1
    Me.rb_just_vip_accounts.TabStop = True
    Me.rb_just_vip_accounts.Text = "xJustVip"
    Me.rb_just_vip_accounts.UseVisualStyleBackColor = True
    '
    'rb_any_account
    '
    Me.rb_any_account.AutoSize = True
    Me.rb_any_account.Location = New System.Drawing.Point(7, 20)
    Me.rb_any_account.Name = "rb_any_account"
    Me.rb_any_account.Size = New System.Drawing.Size(88, 17)
    Me.rb_any_account.TabIndex = 0
    Me.rb_any_account.TabStop = True
    Me.rb_any_account.Text = "xAnyAccount"
    Me.rb_any_account.UseVisualStyleBackColor = True
    '
    'chk_level_anonymous
    '
    Me.chk_level_anonymous.AutoSize = True
    Me.chk_level_anonymous.Location = New System.Drawing.Point(30, 43)
    Me.chk_level_anonymous.Name = "chk_level_anonymous"
    Me.chk_level_anonymous.Size = New System.Drawing.Size(86, 17)
    Me.chk_level_anonymous.TabIndex = 1
    Me.chk_level_anonymous.Text = "xAnonymous"
    Me.chk_level_anonymous.UseVisualStyleBackColor = True
    '
    'chk_level_04
    '
    Me.chk_level_04.AutoSize = True
    Me.chk_level_04.Location = New System.Drawing.Point(30, 135)
    Me.chk_level_04.Name = "chk_level_04"
    Me.chk_level_04.Size = New System.Drawing.Size(72, 17)
    Me.chk_level_04.TabIndex = 5
    Me.chk_level_04.Text = "xLevel 04"
    Me.chk_level_04.UseVisualStyleBackColor = True
    '
    'chk_level_03
    '
    Me.chk_level_03.AutoSize = True
    Me.chk_level_03.Location = New System.Drawing.Point(30, 112)
    Me.chk_level_03.Name = "chk_level_03"
    Me.chk_level_03.Size = New System.Drawing.Size(72, 17)
    Me.chk_level_03.TabIndex = 4
    Me.chk_level_03.Text = "xLevel 03"
    Me.chk_level_03.UseVisualStyleBackColor = True
    '
    'chk_level_02
    '
    Me.chk_level_02.AutoSize = True
    Me.chk_level_02.Location = New System.Drawing.Point(30, 89)
    Me.chk_level_02.Name = "chk_level_02"
    Me.chk_level_02.Size = New System.Drawing.Size(72, 17)
    Me.chk_level_02.TabIndex = 3
    Me.chk_level_02.Text = "xLevel 02"
    Me.chk_level_02.UseVisualStyleBackColor = True
    '
    'chk_level_01
    '
    Me.chk_level_01.AutoSize = True
    Me.chk_level_01.Location = New System.Drawing.Point(30, 66)
    Me.chk_level_01.Name = "chk_level_01"
    Me.chk_level_01.Size = New System.Drawing.Size(72, 17)
    Me.chk_level_01.TabIndex = 2
    Me.chk_level_01.Text = "xLevel 01"
    Me.chk_level_01.UseVisualStyleBackColor = True
    '
    'chk_by_level
    '
    Me.chk_by_level.AutoSize = True
    Me.chk_by_level.Location = New System.Drawing.Point(10, 20)
    Me.chk_by_level.Name = "chk_by_level"
    Me.chk_by_level.Size = New System.Drawing.Size(61, 17)
    Me.chk_by_level.TabIndex = 0
    Me.chk_by_level.Text = "xActive"
    Me.chk_by_level.UseVisualStyleBackColor = True
    '
    'uc_jackpot_customer_tier
    '
    Me.Controls.Add(Me.gb_customer_sel)
    Me.Name = "uc_jackpot_customer_tier"
    Me.Size = New System.Drawing.Size(265, 225)
    Me.gb_customer_sel.ResumeLayout(False)
    Me.gb_customer_sel.PerformLayout()
    Me.gb_vip_accounts.ResumeLayout(False)
    Me.gb_vip_accounts.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_customer_sel As System.Windows.Forms.GroupBox
  Friend WithEvents chk_level_anonymous As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_04 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_03 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_02 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_01 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_by_level As System.Windows.Forms.CheckBox
  Friend WithEvents gb_vip_accounts As System.Windows.Forms.GroupBox
  Friend WithEvents rb_just_no_vip_accounts As System.Windows.Forms.RadioButton
  Friend WithEvents rb_just_vip_accounts As System.Windows.Forms.RadioButton
  Friend WithEvents rb_any_account As System.Windows.Forms.RadioButton

End Class
