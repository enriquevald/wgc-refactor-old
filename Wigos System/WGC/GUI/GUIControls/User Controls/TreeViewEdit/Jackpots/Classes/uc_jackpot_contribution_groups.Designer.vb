﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_jackpot_contribution_groups
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.tb_group = New System.Windows.Forms.TabControl()
    Me.gp_contribution = New System.Windows.Forms.GroupBox()
    Me.gp_contribution.SuspendLayout()
    Me.SuspendLayout()
    '
    'tb_group
    '
    Me.tb_group.Location = New System.Drawing.Point(6, 19)
    Me.tb_group.MaximumSize = New System.Drawing.Size(630, 438)
    Me.tb_group.Name = "tb_group"
    Me.tb_group.SelectedIndex = 0
    Me.tb_group.Size = New System.Drawing.Size(630, 330)
    Me.tb_group.TabIndex = 1
    '
    'gp_contribution
    '
    Me.gp_contribution.Controls.Add(Me.tb_group)
    Me.gp_contribution.Location = New System.Drawing.Point(0, 0)
    Me.gp_contribution.Name = "gp_contribution"
    Me.gp_contribution.Size = New System.Drawing.Size(643, 357)
    Me.gp_contribution.TabIndex = 2
    Me.gp_contribution.TabStop = False
    Me.gp_contribution.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(536)
    '
    'uc_jackpot_contribution_groups
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
    Me.Controls.Add(Me.gp_contribution)
    Me.Name = "uc_jackpot_contribution_groups"
    Me.Size = New System.Drawing.Size(643, 360)
    Me.gp_contribution.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents tb_group As System.Windows.Forms.TabControl
  Friend WithEvents gp_contribution As System.Windows.Forms.GroupBox

End Class
