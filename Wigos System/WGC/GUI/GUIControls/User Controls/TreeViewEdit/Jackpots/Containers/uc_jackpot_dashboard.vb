﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' UC NAME:       uc_jackpot_dashboard
' DESCRIPTION:   Jackpot User Control For Dashboard Jackpot
' AUTHOR:        Rubén Lama Ordóñez
' CREATION DATE: 30-MAR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 30-MAR-2017  RLO    Initial version
'--------------------------------------------------------------------

Public Class uc_jackpot_dashboard
  Inherits uc_treeview_edit_base

#Region " Members "

#End Region

#Region "Properties"

#End Region

#Region "Public Sub"

  Public Sub New()

    MyBase.New()

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call UC_InitControls()

  End Sub

  Public Function IsScreenDataOk() As Boolean

  End Function

#End Region

#Region "Private Sub"

  Public Sub UC_InitControls()

    'Call UC_InitControls()

  End Sub


#End Region

End Class
