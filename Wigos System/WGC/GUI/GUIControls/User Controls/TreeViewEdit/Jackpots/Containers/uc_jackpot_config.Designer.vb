﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_jackpot_config
  Inherits GUI_Controls.uc_treeview_edit_base
  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(uc_jackpot_config))
    Me.pnl_jackpot_payout_mode = New System.Windows.Forms.Panel()
    Me.gb_adv_notification = New System.Windows.Forms.GroupBox()
    Me.uc_cmb_adv_notification_minutes = New GUI_Controls.uc_combo()
    Me.chk_adv_notification_enabled = New System.Windows.Forms.CheckBox()
    Me.ef_average = New GUI_Controls.uc_entry_field()
    Me.Uc_contribution_groups = New GUI_Controls.uc_jackpot_contribution_groups()
    Me.cb_iso_code = New GUI_Controls.uc_combo()
    Me.cmb_type_contribution = New GUI_Controls.uc_combo()
    Me.cmb_jackpot_type = New GUI_Controls.uc_combo()
    Me.ef_maximum = New GUI_Controls.uc_entry_field()
    Me.ef_minimum = New GUI_Controls.uc_entry_field()
    Me.gb_payout_mode = New System.Windows.Forms.GroupBox()
    Me.uc_trioption_payout_mode = New GUI_Controls.uc_trioption()
    Me.pnl_pm_margin = New System.Windows.Forms.Panel()
    Me.gb_other_fields = New System.Windows.Forms.GroupBox()
    Me.chk_enabled = New System.Windows.Forms.CheckBox()
    Me.pnl_jackpot_basic_info = New System.Windows.Forms.Panel()
    Me.ef_name = New GUI_Controls.uc_entry_field()
    Me.uc_jackpot_contribution_compensation = New GUI_Controls.uc_jackpot_contribution_compensation()
    Me.gb_jackpot_behaviour = New System.Windows.Forms.GroupBox()
    Me.lbl_num_pending = New System.Windows.Forms.Label()
    Me.cmb_NumPending = New System.Windows.Forms.ComboBox()
    Me.chk_probability_increase = New System.Windows.Forms.CheckBox()
    Me.pnl_container.SuspendLayout()
    Me.pnl_jackpot_payout_mode.SuspendLayout()
    Me.gb_adv_notification.SuspendLayout()
    Me.gb_payout_mode.SuspendLayout()
    Me.gb_other_fields.SuspendLayout()
    Me.pnl_jackpot_basic_info.SuspendLayout()
    Me.gb_jackpot_behaviour.SuspendLayout()
    Me.SuspendLayout()
    '
    'pnl_container
    '
    Me.pnl_container.Controls.Add(Me.gb_jackpot_behaviour)
    Me.pnl_container.Controls.Add(Me.uc_jackpot_contribution_compensation)
    Me.pnl_container.Controls.Add(Me.gb_other_fields)
    Me.pnl_container.Controls.Add(Me.pnl_jackpot_basic_info)
    Me.pnl_container.Size = New System.Drawing.Size(690, 744)
    '
    'pnl_jackpot_payout_mode
    '
    Me.pnl_jackpot_payout_mode.Controls.Add(Me.gb_adv_notification)
    Me.pnl_jackpot_payout_mode.Controls.Add(Me.ef_average)
    Me.pnl_jackpot_payout_mode.Controls.Add(Me.Uc_contribution_groups)
    Me.pnl_jackpot_payout_mode.Controls.Add(Me.cb_iso_code)
    Me.pnl_jackpot_payout_mode.Controls.Add(Me.cmb_type_contribution)
    Me.pnl_jackpot_payout_mode.Controls.Add(Me.cmb_jackpot_type)
    Me.pnl_jackpot_payout_mode.Controls.Add(Me.ef_maximum)
    Me.pnl_jackpot_payout_mode.Controls.Add(Me.ef_minimum)
    Me.pnl_jackpot_payout_mode.Controls.Add(Me.gb_payout_mode)
    Me.pnl_jackpot_payout_mode.Location = New System.Drawing.Point(9, 19)
    Me.pnl_jackpot_payout_mode.Name = "pnl_jackpot_payout_mode"
    Me.pnl_jackpot_payout_mode.Size = New System.Drawing.Size(659, 489)
    Me.pnl_jackpot_payout_mode.TabIndex = 0
    '
    'gb_adv_notification
    '
    Me.gb_adv_notification.Controls.Add(Me.uc_cmb_adv_notification_minutes)
    Me.gb_adv_notification.Controls.Add(Me.chk_adv_notification_enabled)
    Me.gb_adv_notification.Location = New System.Drawing.Point(411, 42)
    Me.gb_adv_notification.Name = "gb_adv_notification"
    Me.gb_adv_notification.Size = New System.Drawing.Size(245, 40)
    Me.gb_adv_notification.TabIndex = 4
    Me.gb_adv_notification.TabStop = False
    Me.gb_adv_notification.Text = "xAdvanceNotification"
    '
    'uc_cmb_adv_notification_minutes
    '
    Me.uc_cmb_adv_notification_minutes.AllowUnlistedValues = False
    Me.uc_cmb_adv_notification_minutes.AutoCompleteMode = False
    Me.uc_cmb_adv_notification_minutes.IsReadOnly = False
    Me.uc_cmb_adv_notification_minutes.Location = New System.Drawing.Point(76, 12)
    Me.uc_cmb_adv_notification_minutes.Name = "uc_cmb_adv_notification_minutes"
    Me.uc_cmb_adv_notification_minutes.SelectedIndex = -1
    Me.uc_cmb_adv_notification_minutes.Size = New System.Drawing.Size(159, 24)
    Me.uc_cmb_adv_notification_minutes.SufixText = "Sufix Text"
    Me.uc_cmb_adv_notification_minutes.SufixTextVisible = True
    Me.uc_cmb_adv_notification_minutes.TabIndex = 1
    Me.uc_cmb_adv_notification_minutes.TextCombo = Nothing
    '
    'chk_adv_notification_enabled
    '
    Me.chk_adv_notification_enabled.AutoSize = True
    Me.chk_adv_notification_enabled.Location = New System.Drawing.Point(10, 17)
    Me.chk_adv_notification_enabled.Name = "chk_adv_notification_enabled"
    Me.chk_adv_notification_enabled.Size = New System.Drawing.Size(61, 17)
    Me.chk_adv_notification_enabled.TabIndex = 0
    Me.chk_adv_notification_enabled.Text = "xActive"
    Me.chk_adv_notification_enabled.UseVisualStyleBackColor = True
    '
    'ef_average
    '
    Me.ef_average.DoubleValue = 0.0R
    Me.ef_average.IntegerValue = 0
    Me.ef_average.IsReadOnly = False
    Me.ef_average.Location = New System.Drawing.Point(213, 93)
    Me.ef_average.Name = "ef_average"
    Me.ef_average.PlaceHolder = Nothing
    Me.ef_average.Size = New System.Drawing.Size(198, 24)
    Me.ef_average.SufixText = "Sufix Text"
    Me.ef_average.SufixTextVisible = True
    Me.ef_average.TabIndex = 6
    Me.ef_average.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_average.TextValue = ""
    Me.ef_average.TextWidth = 85
    Me.ef_average.Value = ""
    Me.ef_average.ValueForeColor = System.Drawing.Color.Blue
    '
    'Uc_contribution_groups
    '
    Me.Uc_contribution_groups.Location = New System.Drawing.Point(12, 124)
    Me.Uc_contribution_groups.Name = "Uc_contribution_groups"
    Me.Uc_contribution_groups.Size = New System.Drawing.Size(644, 362)
    Me.Uc_contribution_groups.TabIndex = 8
    '
    'cb_iso_code
    '
    Me.cb_iso_code.AllowUnlistedValues = False
    Me.cb_iso_code.AutoCompleteMode = False
    Me.cb_iso_code.IsReadOnly = False
    Me.cb_iso_code.Location = New System.Drawing.Point(474, 7)
    Me.cb_iso_code.Name = "cb_iso_code"
    Me.cb_iso_code.SelectedIndex = -1
    Me.cb_iso_code.Size = New System.Drawing.Size(170, 24)
    Me.cb_iso_code.SufixText = "Sufix Text"
    Me.cb_iso_code.SufixTextVisible = True
    Me.cb_iso_code.TabIndex = 2
    Me.cb_iso_code.TextCombo = Nothing
    Me.cb_iso_code.TextWidth = 70
    '
    'cmb_type_contribution
    '
    Me.cmb_type_contribution.AllowUnlistedValues = False
    Me.cmb_type_contribution.AutoCompleteMode = False
    Me.cmb_type_contribution.IsReadOnly = False
    Me.cmb_type_contribution.Location = New System.Drawing.Point(202, 7)
    Me.cmb_type_contribution.Name = "cmb_type_contribution"
    Me.cmb_type_contribution.SelectedIndex = -1
    Me.cmb_type_contribution.Size = New System.Drawing.Size(203, 24)
    Me.cmb_type_contribution.SufixText = "Sufix Text"
    Me.cmb_type_contribution.SufixTextVisible = True
    Me.cmb_type_contribution.TabIndex = 1
    Me.cmb_type_contribution.TextCombo = Nothing
    '
    'cmb_jackpot_type
    '
    Me.cmb_jackpot_type.AllowUnlistedValues = False
    Me.cmb_jackpot_type.AutoCompleteMode = False
    Me.cmb_jackpot_type.IsReadOnly = False
    Me.cmb_jackpot_type.Location = New System.Drawing.Point(12, 7)
    Me.cmb_jackpot_type.Name = "cmb_jackpot_type"
    Me.cmb_jackpot_type.SelectedIndex = -1
    Me.cmb_jackpot_type.Size = New System.Drawing.Size(184, 24)
    Me.cmb_jackpot_type.SufixText = "Sufix Text"
    Me.cmb_jackpot_type.SufixTextVisible = True
    Me.cmb_jackpot_type.TabIndex = 0
    Me.cmb_jackpot_type.TextCombo = Nothing
    Me.cmb_jackpot_type.TextWidth = 40
    '
    'ef_maximum
    '
    Me.ef_maximum.DoubleValue = 0.0R
    Me.ef_maximum.IntegerValue = 0
    Me.ef_maximum.IsReadOnly = False
    Me.ef_maximum.Location = New System.Drawing.Point(453, 93)
    Me.ef_maximum.Name = "ef_maximum"
    Me.ef_maximum.PlaceHolder = Nothing
    Me.ef_maximum.Size = New System.Drawing.Size(189, 24)
    Me.ef_maximum.SufixText = "Sufix Text"
    Me.ef_maximum.SufixTextVisible = True
    Me.ef_maximum.TabIndex = 7
    Me.ef_maximum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_maximum.TextValue = ""
    Me.ef_maximum.TextWidth = 70
    Me.ef_maximum.Value = ""
    Me.ef_maximum.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_minimum
    '
    Me.ef_minimum.DoubleValue = 0.0R
    Me.ef_minimum.IntegerValue = 0
    Me.ef_minimum.IsReadOnly = False
    Me.ef_minimum.Location = New System.Drawing.Point(11, 93)
    Me.ef_minimum.Name = "ef_minimum"
    Me.ef_minimum.PlaceHolder = Nothing
    Me.ef_minimum.Size = New System.Drawing.Size(189, 24)
    Me.ef_minimum.SufixText = "Sufix Text"
    Me.ef_minimum.SufixTextVisible = True
    Me.ef_minimum.TabIndex = 5
    Me.ef_minimum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_minimum.TextValue = ""
    Me.ef_minimum.TextWidth = 70
    Me.ef_minimum.Value = ""
    Me.ef_minimum.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_payout_mode
    '
    Me.gb_payout_mode.Controls.Add(Me.uc_trioption_payout_mode)
    Me.gb_payout_mode.Controls.Add(Me.pnl_pm_margin)
    Me.gb_payout_mode.Location = New System.Drawing.Point(12, 42)
    Me.gb_payout_mode.Name = "gb_payout_mode"
    Me.gb_payout_mode.Size = New System.Drawing.Size(393, 40)
    Me.gb_payout_mode.TabIndex = 3
    Me.gb_payout_mode.TabStop = False
    Me.gb_payout_mode.Text = "XPayoutMode"
    '
    'uc_trioption_payout_mode
    '
    Me.uc_trioption_payout_mode.Location = New System.Drawing.Point(10, 16)
    Me.uc_trioption_payout_mode.Name = "uc_trioption_payout_mode"
    Me.uc_trioption_payout_mode.SelectedOption = GUI_Controls.uc_trioption.ENUM_TRIOPTION.TRIOPTION_C
    Me.uc_trioption_payout_mode.Size = New System.Drawing.Size(377, 16)
    Me.uc_trioption_payout_mode.TabIndex = 1
    Me.uc_trioption_payout_mode.Vertical = False
    '
    'pnl_pm_margin
    '
    Me.pnl_pm_margin.Dock = System.Windows.Forms.DockStyle.Left
    Me.pnl_pm_margin.Location = New System.Drawing.Point(3, 16)
    Me.pnl_pm_margin.Name = "pnl_pm_margin"
    Me.pnl_pm_margin.Size = New System.Drawing.Size(7, 21)
    Me.pnl_pm_margin.TabIndex = 0
    '
    'gb_other_fields
    '
    Me.gb_other_fields.Controls.Add(Me.pnl_jackpot_payout_mode)
    Me.gb_other_fields.Location = New System.Drawing.Point(10, 44)
    Me.gb_other_fields.Name = "gb_other_fields"
    Me.gb_other_fields.Size = New System.Drawing.Size(674, 516)
    Me.gb_other_fields.TabIndex = 1
    Me.gb_other_fields.TabStop = False
    Me.gb_other_fields.Text = "xJackpotConfiguration"
    '
    'chk_enabled
    '
    Me.chk_enabled.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.chk_enabled.AutoSize = True
    Me.chk_enabled.Location = New System.Drawing.Point(18, 19)
    Me.chk_enabled.Name = "chk_enabled"
    Me.chk_enabled.Size = New System.Drawing.Size(65, 17)
    Me.chk_enabled.TabIndex = 0
    Me.chk_enabled.Text = "Enabled"
    Me.chk_enabled.UseVisualStyleBackColor = True
    '
    'pnl_jackpot_basic_info
    '
    Me.pnl_jackpot_basic_info.Controls.Add(Me.ef_name)
    Me.pnl_jackpot_basic_info.Controls.Add(Me.chk_enabled)
    Me.pnl_jackpot_basic_info.Dock = System.Windows.Forms.DockStyle.Top
    Me.pnl_jackpot_basic_info.Location = New System.Drawing.Point(0, 0)
    Me.pnl_jackpot_basic_info.Name = "pnl_jackpot_basic_info"
    Me.pnl_jackpot_basic_info.Size = New System.Drawing.Size(690, 43)
    Me.pnl_jackpot_basic_info.TabIndex = 0
    '
    'ef_name
    '
    Me.ef_name.DoubleValue = 0.0R
    Me.ef_name.IntegerValue = 0
    Me.ef_name.IsReadOnly = False
    Me.ef_name.Location = New System.Drawing.Point(198, 15)
    Me.ef_name.Name = "ef_name"
    Me.ef_name.PlaceHolder = Nothing
    Me.ef_name.Size = New System.Drawing.Size(462, 24)
    Me.ef_name.SufixText = "Sufix Text"
    Me.ef_name.SufixTextVisible = True
    Me.ef_name.TabIndex = 1
    Me.ef_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_name.TextValue = ""
    Me.ef_name.Value = ""
    Me.ef_name.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_jackpot_contribution_compensation
    '
    Me.uc_jackpot_contribution_compensation.Compensation_PCT = New Decimal(New Integer() {0, 0, 0, 0})
    Me.uc_jackpot_contribution_compensation.Compensation_Type = WSI.Common.Jackpot.JackpotBase.ContributionCompensationType.C0
    Me.uc_jackpot_contribution_compensation.Location = New System.Drawing.Point(10, 561)
    Me.uc_jackpot_contribution_compensation.Name = "uc_jackpot_contribution_compensation"
    Me.uc_jackpot_contribution_compensation.Size = New System.Drawing.Size(252, 150)
    Me.uc_jackpot_contribution_compensation.TabIndex = 2
    '
    'gb_jackpot_behaviour
    '
    Me.gb_jackpot_behaviour.Controls.Add(Me.lbl_num_pending)
    Me.gb_jackpot_behaviour.Controls.Add(Me.cmb_NumPending)
    Me.gb_jackpot_behaviour.Controls.Add(Me.chk_probability_increase)
    Me.gb_jackpot_behaviour.Location = New System.Drawing.Point(269, 562)
    Me.gb_jackpot_behaviour.Name = "gb_jackpot_behaviour"
    Me.gb_jackpot_behaviour.Size = New System.Drawing.Size(414, 103)
    Me.gb_jackpot_behaviour.TabIndex = 3
    Me.gb_jackpot_behaviour.TabStop = False
    Me.gb_jackpot_behaviour.Text = "xComportamiento del Jackpot"
    '
    'lbl_num_pending
    '
    Me.lbl_num_pending.AutoSize = True
    Me.lbl_num_pending.Location = New System.Drawing.Point(84, 67)
    Me.lbl_num_pending.Name = "lbl_num_pending"
    Me.lbl_num_pending.Size = New System.Drawing.Size(215, 13)
    Me.lbl_num_pending.TabIndex = 2
    Me.lbl_num_pending.Text = "xNúmero máximo pendiente de adjudicación"
    '
    'cmb_NumPending
    '
    Me.cmb_NumPending.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cmb_NumPending.FormattingEnabled = True
    Me.cmb_NumPending.Location = New System.Drawing.Point(10, 63)
    Me.cmb_NumPending.Name = "cmb_NumPending"
    Me.cmb_NumPending.Size = New System.Drawing.Size(60, 21)
    Me.cmb_NumPending.TabIndex = 1
    '
    'chk_probability_increase
    '
    Me.chk_probability_increase.AutoSize = True
    Me.chk_probability_increase.Location = New System.Drawing.Point(10, 29)
    Me.chk_probability_increase.Name = "chk_probability_increase"
    Me.chk_probability_increase.Padding = New System.Windows.Forms.Padding(0, 0, 100, 0)
    Me.chk_probability_increase.Size = New System.Drawing.Size(229, 17)
    Me.chk_probability_increase.TabIndex = 0
    Me.chk_probability_increase.Text = "xProbabilidad de éxito"
    Me.chk_probability_increase.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_probability_increase.UseVisualStyleBackColor = True
    '
    'uc_jackpot_config
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
    Me.AutoScroll = True
    Me.Name = "uc_jackpot_config"
    Me.Size = New System.Drawing.Size(690, 744)
    Me.pnl_container.ResumeLayout(False)
    Me.pnl_jackpot_payout_mode.ResumeLayout(False)
    Me.gb_adv_notification.ResumeLayout(False)
    Me.gb_adv_notification.PerformLayout()
    Me.gb_payout_mode.ResumeLayout(False)
    Me.gb_other_fields.ResumeLayout(False)
    Me.pnl_jackpot_basic_info.ResumeLayout(False)
    Me.pnl_jackpot_basic_info.PerformLayout()
    Me.gb_jackpot_behaviour.ResumeLayout(False)
    Me.gb_jackpot_behaviour.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_other_fields As System.Windows.Forms.GroupBox
  Friend WithEvents pnl_jackpot_payout_mode As System.Windows.Forms.Panel
  Friend WithEvents pnl_jackpot_basic_info As System.Windows.Forms.Panel
  Friend WithEvents chk_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents gb_payout_mode As System.Windows.Forms.GroupBox
  Friend WithEvents uc_trioption_payout_mode As GUI_Controls.uc_trioption
  Friend WithEvents pnl_pm_margin As System.Windows.Forms.Panel
  Friend WithEvents ef_minimum As GUI_Controls.uc_entry_field
  Friend WithEvents ef_maximum As GUI_Controls.uc_entry_field
  Friend WithEvents cmb_jackpot_type As GUI_Controls.uc_combo
  Friend WithEvents cmb_type_contribution As GUI_Controls.uc_combo
  Friend WithEvents cb_iso_code As GUI_Controls.uc_combo
  Friend WithEvents Uc_contribution_groups As GUI_Controls.uc_jackpot_contribution_groups
  Public WithEvents ef_name As GUI_Controls.uc_entry_field
  Friend WithEvents uc_jackpot_contribution_compensation As GUI_Controls.uc_jackpot_contribution_compensation
  Friend WithEvents ef_average As GUI_Controls.uc_entry_field
  Friend WithEvents gb_adv_notification As System.Windows.Forms.GroupBox
  Friend WithEvents uc_cmb_adv_notification_minutes As GUI_Controls.uc_combo
  Friend WithEvents chk_adv_notification_enabled As System.Windows.Forms.CheckBox
  Friend WithEvents gb_jackpot_behaviour As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_num_pending As System.Windows.Forms.Label
  Friend WithEvents cmb_NumPending As System.Windows.Forms.ComboBox
  Friend WithEvents chk_probability_increase As System.Windows.Forms.CheckBox

End Class
