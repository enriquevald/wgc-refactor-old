﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' UC NAME:       uc_jackpot_config
' DESCRIPTION:   Jackpot User Control For Configuration Jackpot
' AUTHOR:        Rubén Lama Ordóñez
' CREATION DATE: 30-MAR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 30-MAR-2017  RLO    Initial version
' 20-JUN-2017  MS     PBI 28255 & 28256: Jackpots Probability & Num Pending
'--------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common
Imports System.Collections
Imports System.Globalization
Imports WSI.Common.Jackpot
Imports System.Windows.Forms
Imports WSI.Common.Jackpot.Jackpot
Imports System.Text

Public Class uc_jackpot_config
  Inherits uc_treeview_edit_base

#Region " Members "

  Private m_jackpot As Jackpot
  Private Const MAX_NAME_STRING_LENGHT = 50
  Private Const ADV_NOT_CMB_DEFAULT_VALUE = 30

  ' For Advance notification configuration
  Private Const m_adv_notification_range_start As Integer = 10
  Private Const m_adv_notification_range_end As Integer = 120
  Private Const m_adv_notification_step As Integer = 10
  Private Const m_max_num_pending As Integer = 10

#End Region ' Members

#Region "Properties"

  Public Property Contributions As uc_jackpot_contribution_groups
    Get
      Return Me.Uc_contribution_groups
    End Get
    Set(value As uc_jackpot_contribution_groups)
      Me.Uc_contribution_groups = value
    End Set
  End Property

  Public Property Jackpot As Jackpot
    Get
      Return Me.m_jackpot
    End Get
    Set(ByVal value As Jackpot)
      Me.m_jackpot = value
    End Set
  End Property

#End Region ' Properties

#Region "Public Sub"

  Public Sub New(ByRef Form As frm_treeview_edit, ByVal ShowMode As SHOW_MODE, Optional ByRef Jackpot As Jackpot = Nothing)

    MyBase.New(Form, ShowMode, Jackpot)

    Select Case (ShowMode)
      Case SHOW_MODE.NewItem
        Me.Jackpot = New Jackpot()
      Case SHOW_MODE.EditItem
        Me.Jackpot = Jackpot
      Case SHOW_MODE.CloneItem
        Me.Jackpot = New Jackpot(Jackpot.Id)
        Me.Jackpot.Reset()
        Me.Jackpot.Name = String.Empty
    End Select

    ' This call is required by the Windows Form Designer.
    Call InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call UC_InitControls()

  End Sub

  Public Function IsScreenDataOk() As Boolean
    ' Empty Name
    If String.IsNullOrEmpty(ef_name.Value) Then

      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8096), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El nombre del jackpot no puede estar vacío
      Call Me.ef_name.Focus()

      Return False
    End If



    '' Empty Minimum
    If String.IsNullOrEmpty(Me.ef_minimum.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8098), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El campo mínima no puede estar vacío
      Call Me.ef_minimum.Focus()

      Return False
    End If

    ' Empty Maximum
    If String.IsNullOrEmpty(Me.ef_maximum.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8099), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El campo máximo no puede estar vacío
      Call Me.ef_maximum.Focus()

      Return False
    End If

    ' Empty Average
    If String.IsNullOrEmpty(Me.ef_average.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8100), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El campo media no puede estar vacío
      Call Me.ef_average.Focus()

      Return False
    End If

    ' Average not between minimum and maximum
    If GUI_ParseNumberDecimal(Me.ef_average.Value) <= GUI_ParseNumberDecimal(Me.ef_minimum.Value) OrElse GUI_ParseNumberDecimal(Me.ef_average.Value) >= GUI_ParseNumberDecimal(Me.ef_maximum.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(583), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El campo media ha de estar entre el mínimo y el máximo
      Call Me.ef_average.Focus()

      Return False
    End If


    ' Minimum lower than 0
    If GUI_ParseNumberDecimal(Me.ef_minimum.Value) < 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8102), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El campo mínima no puede ser menor que 0
      Call Me.ef_minimum.Focus()

      Return False
    End If

    ' Maximum lower than 0
    If GUI_ParseNumberDecimal(Me.ef_maximum.Value) < 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8103), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El campo máxima no puede ser menor que 0
      Call Me.ef_maximum.Focus()

      Return False
    End If

    ' Average lower than 0
    If GUI_ParseNumberDecimal(Me.ef_average.Value) < 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8104), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El campo media no puede ser menor que 0
      Call Me.ef_average.Focus()

      Return False
    End If

    ' Minimum lower than maximum
    If GUI_ParseNumberDecimal(Me.ef_maximum.Value) <= GUI_ParseNumberDecimal(Me.ef_minimum.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8199), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El campo máximo no puede ser menor que el campo mínimo
      Call Me.ef_maximum.Focus()

      Return False
    End If

    'Check if jackpotCurrentAmount is under Max. jackpot amount.
    Me.Jackpot.ContributionMeters = New JackpotContributionMeters(Me.Jackpot.Id)
    If Me.Jackpot.ContributionMeters.IsMaxJackpotAmountUnderCurrentAmount(JackpotType.Main, GUI_ParseNumberDecimal(Me.ef_maximum.Value)) _
       AndAlso Me.chk_enabled.Checked Then

      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8386), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, , GLB_NLS_GUI_JACKPOT_MGR.GetString(555), GLB_NLS_GUI_PLAYER_TRACKING.GetString(8387)) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Me.ef_maximum.Focus()

        Return False
      End If

    End If


    ' Check uc_controls
    If Not Me.Contributions.IsScreenDataOk() Then

      Return False
    End If

    If Not Me.uc_jackpot_contribution_compensation.IsScreenDataOk() Then

      Return False
    End If

    Return True

  End Function ' IsScreenData

  ''' <summary>
  ''' Get Jackpot config
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetJackpotConfig() As Jackpot

    Me.GetScreenData()

    Return Me.Jackpot
  End Function ' GetJackpotConfig

#End Region ' Public Sub

#Region "Private Sub"

  Protected Sub UC_InitControls()

    ' Jackpot Enabled
    Me.chk_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8016)

    ' Jackpot Name
    Me.ef_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8017)
    Me.ef_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, MAX_NAME_STRING_LENGHT)

    ' gb Payout Mode
    Me.gb_payout_mode.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8018)
    Me.gb_other_fields.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8119)

    ' Initialize Trioption
    Call Me.uc_trioption_payout_mode.Init(120, 140, 120, uc_trioption.ENUM_TRIOPTION.TRIOPTION_A)

    ' Uc_trioption_payout_mode
    Me.uc_trioption_payout_mode.OptionText(uc_trioption.ENUM_TRIOPTION.TRIOPTION_A) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8019)
    Me.uc_trioption_payout_mode.OptionText(uc_trioption.ENUM_TRIOPTION.TRIOPTION_B) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8020)
    Me.uc_trioption_payout_mode.OptionText(uc_trioption.ENUM_TRIOPTION.TRIOPTION_C) = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8021)
    ' Jackpot Type
    Call LoadComboTypeJackpot()

    ' Jackpot Type Of Contribution
    Call LoadComboTypeOfContributionJackpot()

    ' Jackpot Iso Code
    Call LoadComboIsoCode()

    ' ef Minimum bet
    'Me.ef_minimum_bet.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8022)
    'Me.ef_minimum_bet.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 16, 2)

    ' ef Minimum
    Me.ef_minimum.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8023)
    Me.ef_minimum.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)

    ' ef_average
    Me.ef_average.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8024)
    Me.ef_average.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)

    ' ef_maximum
    Me.ef_maximum.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8025)
    Me.ef_maximum.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)

    'cmb_iso_code
    Me.cb_iso_code.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8085)

    'cmb_jackpotyye
    Me.cmb_jackpot_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8084)

    'cmb_type_contrib
    Me.cmb_type_contribution.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8086)

    ' Advance Notification
    Me.gb_adv_notification.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(566)
    Me.chk_adv_notification_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)
    Me.uc_cmb_adv_notification_minutes.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(312)

    ' Jackpot Behaviour
    Me.chk_probability_increase.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8423)
    Me.lbl_num_pending.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8424)
    Me.gb_jackpot_behaviour.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8425)

    Call InitComboAdvNotificationMinutes()

    Call InitComboMaxNumberPending()

    Call BindJackpot()

  End Sub


  ''' <summary>
  ''' Remove handlers
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub RemoveHandlers()

    RemoveHandler Me.chk_enabled.CheckedChanged, AddressOf Me.chk_enabled_CheckedChanged

  End Sub ' RemoveHandlers

  ''' <summary>
  ''' Add handlers
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub AddHandlers()

    AddHandler Me.chk_enabled.CheckedChanged, AddressOf Me.chk_enabled_CheckedChanged
    AddHandler Me.chk_probability_increase.CheckedChanged, AddressOf Me.chk_probability_increase_CheckedChanged

  End Sub ' AddHandlers

  Private Sub LoadComboTypeJackpot()

    Dim _str_text As String
    Dim dict As New Dictionary(Of Integer, String)
    _str_text = String.Empty

    cmb_jackpot_type.Clear()


    For Each item As Jackpot.AreaType In [Enum].GetValues(GetType(Jackpot.AreaType))

      Select Case item
        Case Jackpot.AreaType.LocalMistery
          _str_text = GLB_NLS_GUI_JACKPOT_MGR.GetString(576)
        Case Jackpot.AreaType.LocalFixed
          _str_text = GLB_NLS_GUI_JACKPOT_MGR.GetString(577)
          'Case Jackpot.AreaMisteryType.Wide
          '  _str_text = GLB_NLS_GUI_JACKPOT_MGR.GetString(578)

      End Select
      dict.Add((CInt(item)), _str_text)
    Next

    cmb_jackpot_type.Add(dict)

  End Sub

  Private Sub LoadComboTypeOfContributionJackpot()
    Dim _str_text As String
    Dim dict As New Dictionary(Of Integer, String)

    _str_text = String.Empty

    cmb_type_contribution.Clear()

    For Each item As Object In [Enum].GetValues(GetType(Jackpot.ContributionCreditType))
      Select Case item
        Case Jackpot.ContributionCreditType.Redeemable
          _str_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8032)

      End Select

      dict.Add((CInt(item)), _str_text)
    Next

    cmb_type_contribution.Add(dict)

  End Sub

  Private Sub LoadComboIsoCode()

    Dim _currency_isocode As String
    Dim _dict As New Dictionary(Of Integer, String)

    cb_iso_code.Clear()
    _currency_isocode = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "MXN")

    _dict.Add(0, _currency_isocode)
    cb_iso_code.Add(_dict)

  End Sub

  ''' <summary>
  ''' Get all data 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GetScreenData()
    Dim _original_jackpot_type As AreaType

    _original_jackpot_type = Me.Jackpot.Type

    Me.Jackpot.Name = Me.ef_name.Value
    Me.Jackpot.Enabled = Me.chk_enabled.Checked
    Me.Jackpot.IsoCode = Me.cb_iso_code.TextValue
    Me.Jackpot.CreditPrizeType = cmb_type_contribution.Value
    Me.Jackpot.Type = Me.cmb_jackpot_type.Value
    Me.Jackpot.Minimum = GUI_ParseNumberDecimal(Me.ef_minimum.Value)
    Me.Jackpot.Maximum = GUI_ParseNumberDecimal(Me.ef_maximum.Value)
    Me.Jackpot.Average = GUI_ParseNumberDecimal(Me.ef_average.Value)
    Me.Jackpot.PayoutMode = CType(Me.uc_trioption_payout_mode.SelectedOption(), PayoutModeType)
    Me.Jackpot.AdvNotificationEnabled = Me.chk_adv_notification_enabled.Checked
    Me.Jackpot.AdvNotificationTime = Me.uc_cmb_adv_notification_minutes.Value

    ' Get Award Days data
    Me.Jackpot.Contributions = Me.Contributions.ToJackpotContributionList(Me.Jackpot.Id)

    'Get Compensation Contribution
    Me.uc_jackpot_contribution_compensation.GetScreenData()
    Me.Jackpot.CompensationFixedPct = Me.uc_jackpot_contribution_compensation.Compensation_PCT
    Me.Jackpot.CompensationType = Me.uc_jackpot_contribution_compensation.Compensation_Type

    ' Reset Num Pendings if jackpot type is new or modified
    If Me.Jackpot.Id <= 0 OrElse Me.Jackpot.Type <> _original_jackpot_type Then
      Me.ResetNumPendings()
    End If

    ' Jackpot Behaviour
    Me.Jackpot.MaximumNumPending = Convert.ToInt32(Me.cmb_NumPending.SelectedItem)
    Me.Jackpot.ProbabilityWithMax = chk_probability_increase.Checked


  End Sub ' GetScreenData

  ''' <summary>
  ''' Bind Jackpot Settings
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub BindJackpot()

    Me.RemoveHandlers()

    Me.ef_name.Value = Jackpot.Name
    Me.chk_enabled.Checked = Jackpot.Enabled
    Me.ef_maximum.Value = Jackpot.Maximum
    Me.ef_minimum.Value = Jackpot.Minimum
    Me.ef_average.Value = Jackpot.Average

    Me.cmb_jackpot_type.SelectedIndex = CInt(Me.Jackpot.Type)
    Me.cmb_type_contribution.SelectedIndex = CInt(Me.Jackpot.CreditPrizeType)
    Me.cb_iso_code.SelectedIndex = 0 ' TODO CHANGE WHEN FLOORDUALCURRENCY INTEGRATED IN JACKPOT CONFIG

    'Me.uc_time_award_config.BindData(Jackpot.AwardDays) TODO RLO
    Me.Uc_contribution_groups.BindData(Jackpot.Contributions)

    Me.uc_trioption_payout_mode.SelectedOption = Jackpot.PayoutMode

    Me.uc_jackpot_contribution_compensation.BindData(Me.Jackpot)

    Me.chk_probability_increase.Checked = Jackpot.ProbabilityWithMax
    Me.cmb_NumPending.SelectedIndex = Jackpot.MaximumNumPending - 1

    Call BindAdvNotification()

    Me.AddHandlers()

  End Sub ' BindJackpot

  ''' <summary>
  ''' Bind ADV Notification
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub BindAdvNotification()
    Me.chk_adv_notification_enabled.Checked = Me.m_jackpot.AdvNotificationEnabled
    Me.uc_cmb_adv_notification_minutes.Value = IIf(Me.m_jackpot.AdvNotificationTime > Jackpot.DEFAULT_INT_VALUE, Me.m_jackpot.AdvNotificationTime, ADV_NOT_CMB_DEFAULT_VALUE)
    Me.uc_cmb_adv_notification_minutes.Enabled = Me.chk_adv_notification_enabled.Checked
  End Sub ' BindAdvNotification

  ''' <summary>
  ''' Enable Jackpot
  ''' </summary>
  ''' <param name="IsEnabled"></param>
  ''' <remarks></remarks>
  Private Sub EnableJackpot(ByVal IsEnabled As Boolean)
    Dim _status As NodeStatus

    If IsEnabled AndAlso Not Me.Jackpot.IsConfigured Then
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8312), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2, Jackpot.Name) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Me.chk_enabled.Checked = False
      End If
    End If

    ' Check if ParentForm is of specific type and FlagsGrid is ok
    If Not frm_treeview_edit.CheckParentFormType(Me) Then
      ' "Error al cargar la pantalla de 'Cuentas afectadas'" 
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8294), ENUM_MB_TYPE.MB_TYPE_ERROR)

    End If

    _status = IIf(IsEnabled, NodeStatus.Enabled, NodeStatus.Disabled)

    ' Call a method to enable disable node
    CType(Me.ParentForm, frm_treeview_edit).EnableDisableNode(_status)

  End Sub ' EnableJackpot

  ''' <summary>
  ''' To load Advance notification combo values
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitComboAdvNotificationMinutes()
    Dim _str_text As String
    Dim dict As New Dictionary(Of Integer, String)
    _str_text = String.Empty

    Me.uc_cmb_adv_notification_minutes.Clear()

    For i As Integer = m_adv_notification_range_start To m_adv_notification_range_end Step m_adv_notification_step
      dict.Add(i, i.ToString)
    Next

    Me.uc_cmb_adv_notification_minutes.Add(dict)

  End Sub

  Private Sub InitComboMaxNumberPending()
    Dim _str_text As String
    Dim dict As New Dictionary(Of Integer, String)
    _str_text = String.Empty

    Me.cmb_NumPending.Items.Clear()

    For i As Integer = 1 To m_max_num_pending
      Me.cmb_NumPending.Items.Add(i.ToString)
    Next


  End Sub

  ''' <summary>
  ''' Reset Jackpot Num Pendings
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub ResetNumPendings()

    Select Case Me.Jackpot.Type
      Case AreaType.LocalMistery
        Me.Jackpot.NumPending = 0
      Case AreaType.LocalFixed
        Me.Jackpot.NumPending = 1
    End Select

  End Sub ' ResetNumPendings

  ''' <summary>
  ''' Calculate C1
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub CalculateC1()
    Dim _minimum As String
    Dim _average As String

    _minimum = IIf(String.IsNullOrEmpty(Me.ef_minimum.Value), Jackpot.DEFAULT_INT_VALUE, Me.ef_minimum.Value)
    _average = IIf(String.IsNullOrEmpty(Me.ef_average.Value), Jackpot.DEFAULT_INT_VALUE, Me.ef_average.Value)

    Me.uc_jackpot_contribution_compensation.CalculateC1Value(GUI_FormatCurrency(_minimum, 2), GUI_FormatCurrency(_average, 2))

  End Sub ' CalculateC1

#End Region 'Private sub

#Region "Events"

  Private Sub chk_enabled_CheckedChanged(sender As Object, e As EventArgs) Handles chk_enabled.CheckedChanged

    Me.EnableJackpot(Me.chk_enabled.Checked)

  End Sub

  ''' <summary>
  ''' Actions when Hit Probaility Check Changes
  ''' </summary>
  ''' <param name="sender"></param>
  ''' <param name="e"></param>
  ''' <remarks></remarks>
  Private Sub chk_probability_increase_CheckedChanged(sender As Object, e As EventArgs) Handles chk_probability_increase.CheckedChanged

    If Not Me.chk_probability_increase.Checked Then
      cmb_NumPending.SelectedIndex = 0
      cmb_NumPending.Enabled = False
    End If

    If Me.chk_probability_increase.Checked Then
      cmb_NumPending.Enabled = True
      Me.cmb_NumPending.SelectedIndex = Jackpot.MaximumNumPending - 1
    End If

  End Sub

  Private Sub chk_adv_notification_enabled_CheckedChanged(sender As Object, e As EventArgs) Handles chk_adv_notification_enabled.CheckedChanged
    Me.uc_cmb_adv_notification_minutes.Enabled = Me.chk_adv_notification_enabled.Checked
  End Sub


  Private Sub ef_minimum_Leave(sender As Object, e As EventArgs) Handles ef_minimum.Leave

    Me.CalculateC1()

  End Sub

  Private Sub ef_average_Leave(sender As Object, e As EventArgs) Handles ef_average.Leave

    Me.CalculateC1()

  End Sub

  ''' <summary>
  ''' Actions when Jackpot Type Changes
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub cmb_jackpot_type_ValueChangedEvent() Handles cmb_jackpot_type.ValueChangedEvent

    Select Case (cmb_jackpot_type.SelectedIndex)
      Case Jackpot.AreaType.LocalFixed
        cmb_NumPending.SelectedIndex = 0
        cmb_NumPending.Enabled = False
        chk_probability_increase.Enabled = False
        chk_probability_increase.Checked = False
      Case Else
        cmb_NumPending.Enabled = True
        Me.cmb_NumPending.SelectedIndex = Jackpot.MaximumNumPending - 1
        chk_probability_increase.Enabled = True
        chk_probability_increase.Checked = Jackpot.ProbabilityWithMax
    End Select

  End Sub

#End Region ' Events




End Class











