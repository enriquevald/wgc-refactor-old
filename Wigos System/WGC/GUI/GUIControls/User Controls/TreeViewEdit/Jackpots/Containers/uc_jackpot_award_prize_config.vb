﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' UC NAME:       uc_jackpot_award_prize
' DESCRIPTION:   Jackpot User Control For award prize selection
' AUTHOR:        Carlos Cáceres González
' CREATION DATE: 28-ABR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 02-MAY-2017  CCG    Initial version
'--------------------------------------------------------------------

#Region "Imports"

Imports GUI_CommonOperations
Imports System.Windows.Forms
Imports GUI_CommonMisc.mdl_NLS
Imports WSI.Common
Imports WSI.Common.Jackpot
Imports System.Text

#End Region

Public Class uc_jackpot_award_prize_config
  Inherits uc_treeview_edit_base

#Region " Members "

  Private m_jackpot_config As Jackpot
  Private m_jackpot_award_prize As JackpotAwardPrizeConfig
  Private m_happy_hours_disabled As Boolean
  Private m_prize_sharing_disabled As Boolean

#End Region ' Members

#Region " Properties"

  Public Property JackpotConfig As Jackpot
    Get
      Return Me.m_jackpot_config
    End Get
    Set(value As Jackpot)
      Me.m_jackpot_config = value
    End Set
  End Property

  Public Property AwardPrizeConfig As JackpotAwardPrizeConfig
    Get
      Return Me.m_jackpot_award_prize
    End Get
    Set(value As JackpotAwardPrizeConfig)
      Me.m_jackpot_award_prize = value
    End Set
  End Property

  Public Property AwardHappyhour As uc_jackpot_award_happy_hour
    Get
      Return Me.uc_jackpot_award_happy_hour
    End Get
    Set(value As uc_jackpot_award_happy_hour)
      Me.uc_jackpot_award_happy_hour = value
    End Set
  End Property

  Public Property HappyHoursDisabled As Boolean
    Get
      Return Me.m_happy_hours_disabled
    End Get
    Set(value As Boolean)
      Me.m_happy_hours_disabled = value
    End Set
  End Property

  Public Property PrizeSharingDisabled As Boolean
    Get
      Return Me.m_prize_sharing_disabled
    End Get
    Set(value As Boolean)
      Me.m_prize_sharing_disabled = value
    End Set
  End Property

  Public Property CustomerTier As uc_jackpot_customer_tier
    Get
      Return Me.uc_jackpot_customer_tier
    End Get
    Set(value As uc_jackpot_customer_tier)
      Me.uc_jackpot_customer_tier = value
    End Set
  End Property

  Public Property PrizeSharing As uc_jackpot_prize_sharing
    Get
      Return Me.uc_jackpot_prize_sharing
    End Get
    Set(value As uc_jackpot_prize_sharing)
      Me.uc_jackpot_prize_sharing = value
    End Set
  End Property

#End Region ' Properties

#Region " Public Functions "

  ''' <summary>
  ''' Constructor
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub New()
    Call Me.Init()
  End Sub

  ''' <summary>
  ''' Constructor
  ''' </summary>
  ''' <param name="Jackpot"></param>
  ''' <remarks></remarks>
  Public Sub New(Optional ByVal Jackpot As Jackpot = Nothing)

    Me.JackpotConfig = Jackpot
    Me.AwardPrizeConfig = Jackpot.AwardPrizeConfig

    If Jackpot Is Nothing OrElse Me.AwardPrizeConfig Is Nothing Then
      NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(575), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)

      Return
    End If

    Call Me.Init()

  End Sub

  ''' <summary>
  ''' Init method
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub Init()

    ' This call is required by the designer.
    Call InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControls()

  End Sub ' Init

  ''' <summary>
  ''' Check if data is ok
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function IsScreenDataOk() As Boolean

    If Not CheckPctContribution() Then
      Return False
    End If

    If Not Me.CheckAllPctValues() Then
      Return False
    End If

    ' When the sum of percentatges is greater than 100, the property returns a negative value.
    If Me.m_jackpot_config.MainPct < 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(581), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' La suma de porcentages no puede mayor que 100.
      Return False
    End If

    ' Call uc methods of IsScreenDataOk
    If Not Me.uc_jackpot_prize_sharing.IsScreenDataOk() Then
      Return False
    End If

    'Check if jackpotCurrentAmount is under Max. jackpot amount.
    Me.m_jackpot_config.ContributionMeters = New JackpotContributionMeters(Me.m_jackpot_config.Id)
    If Me.m_jackpot_config.ContributionMeters.IsMaxJackpotAmountUnderCurrentAmount(Jackpot.JackpotType.PrizeSharing, GUI_ParseNumberDecimal(Me.uc_jackpot_prize_sharing.ef_maximum_amount.Value)) _
       AndAlso Me.uc_jackpot_prize_sharing.chk_prize_sharing.Checked Then

      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8386), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, , GLB_NLS_GUI_JACKPOT_MGR.GetString(557), String.Empty) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Me.uc_jackpot_prize_sharing.ef_maximum_amount.Focus()

        Return False
      End If
    End If


    If Not Me.uc_jackpot_award_happy_hour.IsScreenDataOk() Then
      Return False
    End If

    'Check if jackpotCurrentAmount is under Max. jackpot amount.
    If Me.m_jackpot_config.ContributionMeters.IsMaxJackpotAmountUnderCurrentAmount(Jackpot.JackpotType.HappyHour, GUI_ParseNumberDecimal(Me.uc_jackpot_award_happy_hour.ef_maximum_amount.Value)) _
       AndAlso (Me.uc_jackpot_award_happy_hour.chk_after_jackpot.Checked OrElse Me.uc_jackpot_award_happy_hour.chk_before_jackpot.Checked) Then

      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8386), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, , GLB_NLS_GUI_JACKPOT_MGR.GetString(558), String.Empty) = ENUM_MB_RESULT.MB_RESULT_NO Then
        Me.uc_jackpot_award_happy_hour.ef_maximum_amount.Focus()

        Return False
      End If

    End If



    If Not Me.uc_jackpot_customer_tier.IsScreenDataOk() Then
      Return False
    End If

    Return True

  End Function ' IsScreenDataOk

  ''' <summary>
  ''' Get jackpot award prize configuration
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetJackpotAwardPrizeConfig() As JackpotAwardPrizeConfig

    Me.GetScreenData()

    Return Me.AwardPrizeConfig
  End Function ' GetJackpotAwardPrizeConfig

#End Region

#Region "Private Subs"

  Private Sub InitControls()

    Call UC_InitControls()

    Me.gb_contribution_config.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(580)

    Me.ef_pct_main.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(555)
    Me.ef_pct_main.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)

    Me.ef_pct_prize_sharing.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(557)
    Me.ef_pct_prize_sharing.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)

    Me.ef_pct_hidden.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(556)
    Me.ef_pct_hidden.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)

    Me.ef_pct_happy_hour.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(558)
    Me.ef_pct_happy_hour.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 2)

    AddHandler Me.uc_jackpot_prize_sharing.chk_prize_sharing.CheckedChanged, AddressOf PrizeSharingCheckedChanged
    AddHandler Me.uc_jackpot_award_happy_hour.chk_before_jackpot.CheckedChanged, AddressOf HappyHourCheckedChanged
    AddHandler Me.uc_jackpot_award_happy_hour.chk_after_jackpot.CheckedChanged, AddressOf HappyHourCheckedChanged
  End Sub ' InitControls

  ''' <summary>
  ''' Initialize container
  ''' </summary>
  ''' <remarks></remarks>
  Protected Sub UC_InitControls()
    Me.uc_jackpot_customer_tier.InitControls()
    Me.uc_jackpot_award_happy_hour.InitControls()
    Me.uc_jackpot_prize_sharing.InitControls()

    Call BindData()
  End Sub ' UC_InitControls

  ''' <summary>
  ''' To bind the data in each user control
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub BindData()
    Me.uc_jackpot_customer_tier.BindData(m_jackpot_award_prize.CustomerTier)
    Me.uc_jackpot_prize_sharing.BindData(m_jackpot_award_prize.PrizeSharing)
    Me.uc_jackpot_award_happy_hour.BindData(m_jackpot_award_prize.HappyHour)

    Call BindPrizePctData()
  End Sub ' BindData

  ''' <summary>
  ''' To bind the prize percentage data
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub BindPrizePctData()
    Me.ef_pct_main.Value = Me.m_jackpot_config.MainPct

    Me.ef_pct_prize_sharing.Value = Me.m_jackpot_config.PrizeSharingPct
    Me.ef_pct_prize_sharing.Enabled = Me.uc_jackpot_prize_sharing.chk_prize_sharing.Checked

    Me.ef_pct_hidden.Value = Me.m_jackpot_config.HiddenPct
    Me.ef_pct_hidden.Enabled = True

    Me.ef_pct_happy_hour.Value = Me.m_jackpot_config.HappyHourPct
    Me.ef_pct_happy_hour.Enabled = Me.uc_jackpot_award_happy_hour.chk_before_jackpot.Checked Or Me.uc_jackpot_award_happy_hour.chk_after_jackpot.Checked
  End Sub ' BindPrizePctData

  ''' <summary>
  ''' Get all data 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GetScreenData()

    ' Get Award Days data
    Me.AwardPrizeConfig.CustomerTier = Me.uc_jackpot_customer_tier.ToJackpotAwardCustomerTier()

    ' Get Happy hour data
    Me.AwardPrizeConfig.HappyHour = Me.uc_jackpot_award_happy_hour.ToJackpotAwardHappyHour()

    ' Get Prize Sharing
    Me.AwardPrizeConfig.PrizeSharing = Me.uc_jackpot_prize_sharing.ToJackpotAwardPrizeSharing()

  End Sub ' GetScreenData

  ''' <summary>
  ''' To check if the % contributions are well configurated
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckPctContribution() As Boolean
    If Me.ef_pct_prize_sharing.Enabled And Me.m_jackpot_config.PrizeSharingPct = 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(582), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El porcentaje a de ser mayor que 0.
      Me.ef_pct_prize_sharing.Focus()
      Return False
    End If

    If Me.ef_pct_happy_hour.Enabled And Me.m_jackpot_config.HappyHourPct = 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(582), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El porcentaje a de ser mayor que 0.
      Me.ef_pct_happy_hour.Focus()
      Return False
    End If

    If Me.ef_pct_hidden.Enabled And Me.m_jackpot_config.HiddenPct < 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8315), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El porcentaje a de ser mayor o igual que 0.
      Me.ef_pct_hidden.Focus()
      Return False
    End If

    Return True

  End Function ' CheckPctContribution

  Private Function CheckAllPctValues() As Boolean

    Return Me.CheckPctValuesNotGreater(Me.ef_pct_prize_sharing) AndAlso _
           Me.CheckPctValuesNotGreater(Me.ef_pct_hidden) AndAlso _
           Me.CheckPctValuesNotGreater(Me.ef_pct_happy_hour)
  End Function

  Private Function CheckPctValuesNotGreater(ByRef EntryField As uc_entry_field) As Boolean
    Dim _value As Decimal
    _value = GUI_ParseNumberDecimal(EntryField.Value)

    If _value > 100 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8369), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' Percentage value must not be greater than 100.
      _value = 0
      EntryField.Focus()

      Return False
    End If

    EntryField.Value = _value.ToString()

    Return True

  End Function

#End Region

#Region "Events"

  ''' <summary>
  ''' Event to update prize sharing percentage 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub PrizeSharingCheckedChanged()
    Dim _enabled As Boolean

    _enabled = Me.uc_jackpot_prize_sharing.chk_prize_sharing.Checked
    Me.ef_pct_prize_sharing.Enabled = _enabled

    Me.PrizeSharingDisabled = Not _enabled

    If Not _enabled Then
      Me.ef_pct_prize_sharing.Value = 0
      Me.m_jackpot_config.PrizeSharingPct = 0
      Me.ef_pct_main.Value = Math.Max(Me.m_jackpot_config.MainPct, 0)
    End If

  End Sub ' PrizeSharingCheckedChanged

  ''' <summary>
  ''' Event to update happy hour percentage 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub HappyHourCheckedChanged()
    Dim _enabled As Boolean

    _enabled = (Me.uc_jackpot_award_happy_hour.chk_before_jackpot.Checked Or Me.uc_jackpot_award_happy_hour.chk_after_jackpot.Checked)
    Me.ef_pct_happy_hour.Enabled = _enabled

    Me.HappyHoursDisabled = Not _enabled

    If Not _enabled Then
      Me.ef_pct_happy_hour.Value = 0
      Me.m_jackpot_config.HappyHourPct = 0
      Me.ef_pct_main.Value = Math.Max(Me.m_jackpot_config.MainPct, 0)
    End If

  End Sub ' HappyHourCheckedChanged

  'Private Sub ef_pct_prize_sharing_Leave(sender As Object, e As EventArgs) Handles ef_pct_prize_sharing.Leave
  '  Me.CheckPctValuesNotGreater(Me.ef_pct_prize_sharing)
  'End Sub

  'Private Sub ef_pct_hidden_Leave(sender As Object, e As EventArgs) Handles ef_pct_hidden.Leave
  '  Me.CheckPctValuesNotGreater(Me.ef_pct_hidden)
  'End Sub

  'Private Sub ef_pct_happy_hour_Leave(sender As Object, e As EventArgs) Handles ef_pct_happy_hour.Leave
  '  Me.CheckPctValuesNotGreater(Me.ef_pct_happy_hour)
  'End Sub

#End Region

End Class
