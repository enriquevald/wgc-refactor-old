﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_jackpot_award_prize_config
  Inherits GUI_Controls.uc_treeview_edit_base

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.uc_jackpot_prize_sharing = New GUI_Controls.uc_jackpot_prize_sharing()
    Me.uc_jackpot_customer_tier = New GUI_Controls.uc_jackpot_customer_tier()
    Me.uc_jackpot_award_happy_hour = New GUI_Controls.uc_jackpot_award_happy_hour()
    Me.gb_contribution_config = New System.Windows.Forms.GroupBox()
    Me.lbl_pct_04 = New System.Windows.Forms.Label()
    Me.lbl_pct_03 = New System.Windows.Forms.Label()
    Me.lbl_pct_02 = New System.Windows.Forms.Label()
    Me.lbl_pct_01 = New System.Windows.Forms.Label()
    Me.ef_pct_happy_hour = New GUI_Controls.uc_entry_field()
    Me.ef_pct_hidden = New GUI_Controls.uc_entry_field()
    Me.ef_pct_prize_sharing = New GUI_Controls.uc_entry_field()
    Me.ef_pct_main = New GUI_Controls.uc_entry_field()
    Me.pnl_container.SuspendLayout()
    Me.gb_contribution_config.SuspendLayout()
    Me.SuspendLayout()
    '
    'pnl_container
    '
    Me.pnl_container.Controls.Add(Me.gb_contribution_config)
    Me.pnl_container.Controls.Add(Me.uc_jackpot_award_happy_hour)
    Me.pnl_container.Controls.Add(Me.uc_jackpot_customer_tier)
    Me.pnl_container.Controls.Add(Me.uc_jackpot_prize_sharing)
    Me.pnl_container.Size = New System.Drawing.Size(566, 460)
    '
    'uc_jackpot_prize_sharing
    '
    Me.uc_jackpot_prize_sharing.AwardPrizeSharing = Nothing
    Me.uc_jackpot_prize_sharing.Location = New System.Drawing.Point(0, 58)
    Me.uc_jackpot_prize_sharing.MaximumSize = New System.Drawing.Size(300, 235)
    Me.uc_jackpot_prize_sharing.Name = "uc_jackpot_prize_sharing"
    Me.uc_jackpot_prize_sharing.Size = New System.Drawing.Size(300, 222)
    Me.uc_jackpot_prize_sharing.TabIndex = 1
    '
    'uc_jackpot_customer_tier
    '
    Me.uc_jackpot_customer_tier.AwardCustomerTier = Nothing
    Me.uc_jackpot_customer_tier.Location = New System.Drawing.Point(302, 58)
    Me.uc_jackpot_customer_tier.MaximumSize = New System.Drawing.Size(265, 235)
    Me.uc_jackpot_customer_tier.Name = "uc_jackpot_customer_tier"
    Me.uc_jackpot_customer_tier.Size = New System.Drawing.Size(265, 222)
    Me.uc_jackpot_customer_tier.TabIndex = 2
    '
    'uc_jackpot_award_happy_hour
    '
    Me.uc_jackpot_award_happy_hour.HappyHour = Nothing
    Me.uc_jackpot_award_happy_hour.Location = New System.Drawing.Point(5, 279)
    Me.uc_jackpot_award_happy_hour.Name = "uc_jackpot_award_happy_hour"
    Me.uc_jackpot_award_happy_hour.Size = New System.Drawing.Size(561, 182)
    Me.uc_jackpot_award_happy_hour.TabIndex = 3
    '
    'gb_contribution_config
    '
    Me.gb_contribution_config.Controls.Add(Me.lbl_pct_04)
    Me.gb_contribution_config.Controls.Add(Me.lbl_pct_03)
    Me.gb_contribution_config.Controls.Add(Me.lbl_pct_02)
    Me.gb_contribution_config.Controls.Add(Me.lbl_pct_01)
    Me.gb_contribution_config.Controls.Add(Me.ef_pct_happy_hour)
    Me.gb_contribution_config.Controls.Add(Me.ef_pct_hidden)
    Me.gb_contribution_config.Controls.Add(Me.ef_pct_prize_sharing)
    Me.gb_contribution_config.Controls.Add(Me.ef_pct_main)
    Me.gb_contribution_config.Location = New System.Drawing.Point(4, 4)
    Me.gb_contribution_config.Name = "gb_contribution_config"
    Me.gb_contribution_config.Size = New System.Drawing.Size(556, 53)
    Me.gb_contribution_config.TabIndex = 0
    Me.gb_contribution_config.TabStop = False
    Me.gb_contribution_config.Text = "xGbContributionConfig"
    '
    'lbl_pct_04
    '
    Me.lbl_pct_04.AutoSize = True
    Me.lbl_pct_04.Location = New System.Drawing.Point(538, 25)
    Me.lbl_pct_04.Name = "lbl_pct_04"
    Me.lbl_pct_04.Size = New System.Drawing.Size(15, 13)
    Me.lbl_pct_04.TabIndex = 7
    Me.lbl_pct_04.Text = "%"
    '
    'lbl_pct_03
    '
    Me.lbl_pct_03.AutoSize = True
    Me.lbl_pct_03.Location = New System.Drawing.Point(403, 25)
    Me.lbl_pct_03.Name = "lbl_pct_03"
    Me.lbl_pct_03.Size = New System.Drawing.Size(15, 13)
    Me.lbl_pct_03.TabIndex = 6
    Me.lbl_pct_03.Text = "%"
    '
    'lbl_pct_02
    '
    Me.lbl_pct_02.AutoSize = True
    Me.lbl_pct_02.Location = New System.Drawing.Point(263, 25)
    Me.lbl_pct_02.Name = "lbl_pct_02"
    Me.lbl_pct_02.Size = New System.Drawing.Size(15, 13)
    Me.lbl_pct_02.TabIndex = 5
    Me.lbl_pct_02.Text = "%"
    '
    'lbl_pct_01
    '
    Me.lbl_pct_01.AutoSize = True
    Me.lbl_pct_01.Location = New System.Drawing.Point(125, 25)
    Me.lbl_pct_01.Name = "lbl_pct_01"
    Me.lbl_pct_01.Size = New System.Drawing.Size(15, 13)
    Me.lbl_pct_01.TabIndex = 4
    Me.lbl_pct_01.Text = "%"
    '
    'ef_pct_happy_hour
    '
    Me.ef_pct_happy_hour.DoubleValue = 0.0R
    Me.ef_pct_happy_hour.IntegerValue = 0
    Me.ef_pct_happy_hour.IsReadOnly = False
    Me.ef_pct_happy_hour.Location = New System.Drawing.Point(418, 19)
    Me.ef_pct_happy_hour.Name = "ef_pct_happy_hour"
    Me.ef_pct_happy_hour.PlaceHolder = Nothing
    Me.ef_pct_happy_hour.Size = New System.Drawing.Size(120, 24)
    Me.ef_pct_happy_hour.SufixText = "Sufix Text"
    Me.ef_pct_happy_hour.SufixTextVisible = True
    Me.ef_pct_happy_hour.TabIndex = 3
    Me.ef_pct_happy_hour.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_pct_happy_hour.TextValue = ""
    Me.ef_pct_happy_hour.TextWidth = 60
    Me.ef_pct_happy_hour.Value = ""
    Me.ef_pct_happy_hour.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_pct_hidden
    '
    Me.ef_pct_hidden.DoubleValue = 0.0R
    Me.ef_pct_hidden.IntegerValue = 0
    Me.ef_pct_hidden.IsReadOnly = False
    Me.ef_pct_hidden.Location = New System.Drawing.Point(282, 19)
    Me.ef_pct_hidden.Name = "ef_pct_hidden"
    Me.ef_pct_hidden.PlaceHolder = Nothing
    Me.ef_pct_hidden.Size = New System.Drawing.Size(120, 24)
    Me.ef_pct_hidden.SufixText = "Sufix Text"
    Me.ef_pct_hidden.SufixTextVisible = True
    Me.ef_pct_hidden.TabIndex = 2
    Me.ef_pct_hidden.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_pct_hidden.TextValue = ""
    Me.ef_pct_hidden.TextWidth = 60
    Me.ef_pct_hidden.Value = ""
    Me.ef_pct_hidden.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_pct_prize_sharing
    '
    Me.ef_pct_prize_sharing.DoubleValue = 0.0R
    Me.ef_pct_prize_sharing.IntegerValue = 0
    Me.ef_pct_prize_sharing.IsReadOnly = False
    Me.ef_pct_prize_sharing.Location = New System.Drawing.Point(142, 19)
    Me.ef_pct_prize_sharing.Name = "ef_pct_prize_sharing"
    Me.ef_pct_prize_sharing.PlaceHolder = Nothing
    Me.ef_pct_prize_sharing.Size = New System.Drawing.Size(120, 24)
    Me.ef_pct_prize_sharing.SufixText = "Sufix Text"
    Me.ef_pct_prize_sharing.SufixTextVisible = True
    Me.ef_pct_prize_sharing.TabIndex = 1
    Me.ef_pct_prize_sharing.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_pct_prize_sharing.TextValue = ""
    Me.ef_pct_prize_sharing.TextWidth = 60
    Me.ef_pct_prize_sharing.Value = ""
    Me.ef_pct_prize_sharing.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_pct_main
    '
    Me.ef_pct_main.DoubleValue = 0.0R
    Me.ef_pct_main.IntegerValue = 0
    Me.ef_pct_main.IsReadOnly = True
    Me.ef_pct_main.Location = New System.Drawing.Point(3, 19)
    Me.ef_pct_main.Name = "ef_pct_main"
    Me.ef_pct_main.PlaceHolder = Nothing
    Me.ef_pct_main.Size = New System.Drawing.Size(120, 24)
    Me.ef_pct_main.SufixText = "Sufix Text"
    Me.ef_pct_main.SufixTextVisible = True
    Me.ef_pct_main.TabIndex = 0
    Me.ef_pct_main.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_pct_main.TextValue = ""
    Me.ef_pct_main.TextWidth = 60
    Me.ef_pct_main.Value = ""
    Me.ef_pct_main.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_jackpot_award_prize_config
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
    Me.MaximumSize = New System.Drawing.Size(566, 460)
    Me.Name = "uc_jackpot_award_prize_config"
    Me.Size = New System.Drawing.Size(566, 460)
    Me.pnl_container.ResumeLayout(False)
    Me.gb_contribution_config.ResumeLayout(False)
    Me.gb_contribution_config.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_jackpot_prize_sharing As GUI_Controls.uc_jackpot_prize_sharing
  Friend WithEvents uc_jackpot_customer_tier As GUI_Controls.uc_jackpot_customer_tier
  Friend WithEvents uc_jackpot_award_happy_hour As GUI_Controls.uc_jackpot_award_happy_hour
  Friend WithEvents gb_contribution_config As System.Windows.Forms.GroupBox
  Public WithEvents ef_pct_happy_hour As GUI_Controls.uc_entry_field
  Public WithEvents ef_pct_hidden As GUI_Controls.uc_entry_field
  Public WithEvents ef_pct_prize_sharing As GUI_Controls.uc_entry_field
  Public WithEvents ef_pct_main As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_pct_04 As System.Windows.Forms.Label
  Friend WithEvents lbl_pct_03 As System.Windows.Forms.Label
  Friend WithEvents lbl_pct_02 As System.Windows.Forms.Label
  Friend WithEvents lbl_pct_01 As System.Windows.Forms.Label


End Class
