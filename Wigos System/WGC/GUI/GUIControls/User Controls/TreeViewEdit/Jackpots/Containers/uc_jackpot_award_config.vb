﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' UC NAME:       uc_jackpot_award_config
' DESCRIPTION:   Jackpot User Control for award configuration
' AUTHOR:        Rubén Lama Ordóñez
' CREATION DATE: 18-APR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 18-APR-2017  RLO    Initial version
'--------------------------------------------------------------------

#Region "Imports"

Imports GUI_CommonOperations
Imports System.Windows.Forms
Imports GUI_CommonMisc.mdl_NLS
Imports WSI.Common
Imports WSI.Common.Jackpot
Imports WSI.Common.Jackpot.Jackpot

#End Region

Public Class uc_jackpot_award_config
  Inherits uc_treeview_edit_base

#Region "Members"

  Private m_award_config As JackpotAwardConfig
  Private m_jackpot As Jackpot

#End Region

#Region "Enums"

#End Region

#Region "Properties"

  Public Property AwardConfig As JackpotAwardConfig
    Get
      Return Me.m_award_config
    End Get
    Set(value As JackpotAwardConfig)
      Me.m_award_config = value
    End Set
  End Property

  Public Property AwardDays As uc_jackpot_time_award
    Get
      Return Me.Uc_jackpot_time_award_config
    End Get
    Set(value As uc_jackpot_time_award)
      Me.Uc_jackpot_time_award_config = value
    End Set
  End Property

  Public Property Flags As uc_jackpot_flags
    Get
      Return Me.uc_jackpot_flags
    End Get
    Set(value As uc_jackpot_flags)
      Me.uc_jackpot_flags = value
    End Set
  End Property

  Public Property ExcludeAccount As CheckBox
    Get
      Return Me.chk_exclude_account_with_promotion
    End Get
    Set(value As CheckBox)
      Me.chk_exclude_account_with_promotion = value
    End Set
  End Property

  Public Property ExcludeAnonymousAccount As CheckBox
    Get
      Return Me.chk_exclude_anonymous_accounts
    End Get
    Set(value As CheckBox)
      Me.chk_exclude_anonymous_accounts = value
    End Set
  End Property

  Public Property MinOccupation As uc_entry_field
    Get
      Return Me.ef_min_ocupancy
    End Get
    Set(value As uc_entry_field)
      Me.ef_min_ocupancy = value

    End Set
  End Property

  Public Property ByGender As CheckBox
    Get
      Return Me.chk_by_gender
    End Get
    Set(value As CheckBox)
      Me.chk_by_gender = value
    End Set
  End Property

  Public Property ByDateOfBirth As CheckBox
    Get
      Return Me.chk_by_birth_date
    End Get
    Set(value As CheckBox)
      Me.chk_by_birth_date = value
    End Set
  End Property

  Public Property AgeRange As CheckBox
    Get
      Return Me.chk_by_age
    End Get
    Set(value As CheckBox)
      Me.chk_by_age = value
    End Set
  End Property

  Public Property OlderThan As uc_entry_field
    Get
      Return Me.ef_age_older_than
    End Get
    Set(value As uc_entry_field)
      Me.ef_age_older_than = value
    End Set
  End Property

  Public Property AndSmallerThan As uc_entry_field
    Get
      Return Me.ef_age_and_smaller_than
    End Get
    Set(value As uc_entry_field)
      Me.ef_age_and_smaller_than = value
    End Set
  End Property

  Public Property SmallerThan As uc_entry_field
    Get
      Return Me.ef_age_smaller_than
    End Get
    Set(value As uc_entry_field)
      Me.ef_age_smaller_than = value
    End Set
  End Property


  Public Property OrOlderThan As uc_entry_field
    Get
      Return Me.ef_age_or_older_than
    End Get
    Set(value As uc_entry_field)
      Me.ef_age_or_older_than = value
    End Set
  End Property

  Public Property ByCreationDate As CheckBox
    Get
      Return Me.chk_by_created_account
    End Get
    Set(value As CheckBox)
      Me.chk_by_created_account = value
    End Set
  End Property

  Public Property JustDayOfCreation As RadioButton
    Get
      Return Me.opt_created_account_anniversary_day_month
    End Get
    Set(value As RadioButton)
      Me.opt_created_account_anniversary_day_month = value
    End Set
  End Property

  Public Property DayOfCreationPlus As uc_entry_field
    Get
      Return Me.ef_created_account_working_day_plus
    End Get
    Set(value As uc_entry_field)
      Me.ef_created_account_working_day_plus = value
    End Set
  End Property

  Public Property AnyDayOfCreation As RadioButton
    Get
      Return Me.opt_created_account_anniversary_whole_month
    End Get
    Set(value As RadioButton)
      Me.opt_created_account_anniversary_whole_month = value
    End Set
  End Property

  Public Property DayAndMonth As RadioButton
    Get
      Return Me.opt_created_account_anniversary_day_month
    End Get
    Set(value As RadioButton)
      Me.opt_created_account_anniversary_day_month = value
    End Set
  End Property

  Public Property CreationBetweenThis As uc_entry_field
    Get
      Return Me.ef_created_account_anniversary_year_from
    End Get
    Set(value As uc_entry_field)
      Me.ef_created_account_anniversary_year_from = value
    End Set
  End Property

  Public Property CreationBetweenThat As uc_entry_field
    Get
      Return Me.ef_created_account_anniversary_year_to
    End Get
    Set(value As uc_entry_field)
      Me.ef_created_account_anniversary_year_to = value
    End Set
  End Property

#End Region

#Region "Sub"

  Public Sub New()

    Call Me.Init()

  End Sub

  ''' <summary>
  '''  uc constructor
  ''' </summary>
  ''' <param name="Jackpot"></param>
  ''' <remarks></remarks>
  Public Sub New(ByRef Jackpot As Jackpot)

    m_jackpot = Jackpot

    If Jackpot Is Nothing OrElse Jackpot.AwardConfig Is Nothing Then
      ' TODO JBP: MESSAGE: Error al cargar la configuración de concesión. 
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(0), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK)

      Return
    End If

    Me.AwardConfig = Jackpot.AwardConfig

    Call Me.Init()

  End Sub

  ''' <summary>
  ''' Init method
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub Init(Optional ByVal AllFlags As DataTable = Nothing)

    ' This call is required by the designer.
    Call InitializeComponent()
    ' Add any initialization after the InitializeComponent() call.
    Call UC_InitControls(AllFlags)

  End Sub

  ''' <summary>
  ''' Initialite uc controls
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub UC_InitControls(ByVal AllFlags As DataTable)

    ' ef Minimum bet
    Me.ef_minimum_bet.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8022)
    Me.ef_minimum_bet.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 6, 2)
    Me.ef_minimum_bet.SufixText = String.Empty

    Me.chk_by_gender.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)
    Me.opt_gender_male.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(214)
    Me.opt_gender_female.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(215)
    Me.chk_by_birth_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)
    Me.opt_birth_date_only.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(217)
    Me.opt_birth_month_only.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(218)
    Me.chk_by_age.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2730)
    Me.opt_birth_include_age.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2702)
    Me.opt_birth_exclude_age.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2703)
    Me.ef_age_and_smaller_than.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2789)
    Me.ef_age_and_smaller_than.TextVisible = True
    Me.ef_age_or_older_than.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2790)
    Me.ef_age_or_older_than.TextVisible = True
    Me.ef_age_older_than.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    Me.ef_age_smaller_than.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    Me.ef_age_and_smaller_than.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    Me.ef_age_or_older_than.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    Me.gb_by_creation_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6720)
    Me.gb_by_gender.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(213)
    Me.gb_date_of_birth.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(216)
    Me.ef_min_ocupancy.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(346)
    Me.ef_min_ocupancy.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 6, 2)
    Me.chk_exclude_account_with_promotion.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(340)
    Me.chk_exclude_anonymous_accounts.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(347)

    Me.chk_by_created_account.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)
    Me.opt_created_account_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6721)
    Me.opt_created_account_working_day_only.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6722)
    Me.opt_created_account_working_day_plus.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6723)
    Me.ef_created_account_working_day_plus.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6724)
    Me.ef_created_account_working_day_plus.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_created_account_working_day_plus.TextVisible = True

    Me.opt_created_account_anniversary.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6725)
    Me.opt_created_account_anniversary_whole_month.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6726)
    Me.opt_created_account_anniversary_day_month.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6727)
    Me.chk_by_anniversary_range_of_years.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6728)
    Me.ef_created_account_anniversary_year_from.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6729)
    Me.ef_created_account_anniversary_year_from.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_created_account_anniversary_year_from.TextVisible = True
    Me.ef_created_account_anniversary_year_to.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6730)
    Me.ef_created_account_anniversary_year_to.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_created_account_anniversary_year_to.TextVisible = True

    Me.gb_misc.Text = GLB_NLS_GUI_JACKPOT_MGR.GetString(572)

    'Default values
    Me.opt_gender_male.Checked = True
    Me.opt_created_account_date.Checked = True
    Me.opt_created_account_working_day_only.Checked = True
    Me.opt_birth_month_only.Checked = True
    Me.opt_birth_include_age.Checked = True
    Me.opt_created_account_anniversary_whole_month.Checked = True

    Me.BindData()

    Me.Uc_jackpot_time_award_config.InitControls()

    Me.uc_jackpot_EGM_terminal_award.InitControls()

  End Sub ' UC_InitControls

  ''' <summary>
  ''' Enable created account date options
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub EnableCreatedAccountDateOptions()

    opt_created_account_working_day_only.Enabled = opt_created_account_date.Checked
    opt_created_account_working_day_plus.Enabled = opt_created_account_date.Checked
    ef_created_account_working_day_plus.Enabled = opt_created_account_date.Checked
    opt_created_account_anniversary_whole_month.Enabled = opt_created_account_anniversary.Checked
    opt_created_account_anniversary_day_month.Enabled = opt_created_account_anniversary.Checked
    chk_by_anniversary_range_of_years.Enabled = opt_created_account_anniversary.Checked
    ef_created_account_anniversary_year_from.Enabled = opt_created_account_anniversary.Checked And chk_by_anniversary_range_of_years.Checked
    ef_created_account_anniversary_year_to.Enabled = opt_created_account_anniversary.Checked And chk_by_anniversary_range_of_years.Checked

  End Sub ' EnableCreatedAccountDateOptions

  ''' <summary>
  ''' Sub to bind controls By Gender
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub BindByGender()

    'By gender
    Select Case Me.AwardConfig.Filter.ByGender

      Case GENDER.UNKNOWN
        Me.chk_by_gender.Checked = False

      Case GENDER.MALE
        Me.chk_by_gender.Checked = True
        Me.opt_gender_female.Checked = False
        Me.opt_gender_male.Checked = True

      Case GENDER.FEMALE
        Me.chk_by_gender.Checked = True
        Me.opt_gender_female.Checked = True
        Me.opt_gender_male.Checked = False

    End Select


  End Sub

  ''' <summary>
  ''' Sub to bind controls by CreationDAte
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub BindByCreationDate()

    'by creation Date
    Select Case Me.AwardConfig.Filter.ByCreationDate

      Case AwardFilterCreationDate.Disabled
        Me.chk_by_created_account.Checked = False

      Case AwardFilterCreationDate.Creation
        Me.chk_by_created_account.Checked = True
        Me.opt_created_account_date.Checked = True
        Me.opt_created_account_anniversary.Checked = False
        Me.opt_created_account_working_day_only.Checked = Me.AwardConfig.Filter.JustDayOfCreation

        If Me.AwardConfig.Filter.DayOfCreationPlus > 0 Then
          Me.opt_created_account_working_day_plus.Checked = True
          Me.ef_created_account_working_day_plus.Value = GUI_ParseNumberDecimal(Me.AwardConfig.Filter.DayOfCreationPlus)

        End If

      Case AwardFilterCreationDate.CreationBirthdate
        Me.chk_by_created_account.Checked = True
        Me.opt_created_account_date.Checked = False
        Me.opt_created_account_anniversary.Checked = True

        Me.opt_created_account_anniversary_whole_month.Checked = Me.AwardConfig.Filter.AnyDayOfMonth
        Me.opt_created_account_anniversary_day_month.Checked = Me.AwardConfig.Filter.DayAndMonth

        If (Me.AwardConfig.Filter.CreationBetweenThis > 0 And Me.AwardConfig.Filter.CreationBetweenThat > 0) Then

          Me.chk_by_anniversary_range_of_years.Checked = True
          Me.ef_created_account_anniversary_year_from.Value = Me.AwardConfig.Filter.CreationBetweenThis
          Me.ef_created_account_anniversary_year_to.Value = Me.AwardConfig.Filter.CreationBetweenThat

        End If

    End Select

  End Sub

  ''' <summary>
  ''' Sub to bind controls by date of birth
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub BindByDateOfBirth()

    Select Case Me.AwardConfig.Filter.ByDateOfBirth

      Case AwardFilterDateOfBirth.Disabled
        Me.chk_by_birth_date.Checked = False

      Case AwardFilterDateOfBirth.Birthday
        Me.chk_by_birth_date.Checked = True
        Me.opt_birth_month_only.Checked = True
        Me.opt_birth_date_only.Checked = False

      Case AwardFilterDateOfBirth.DayAndMonth
        chk_by_birth_date.Checked = True
        Me.opt_birth_month_only.Checked = False
        Me.opt_birth_date_only.Checked = True

    End Select


    If Me.AwardConfig.Filter.AgeRange Then

      If Me.AwardConfig.Filter.OlderThan > 0 And Me.AwardConfig.Filter.AndSmallerThan > 0 Then

        Me.opt_birth_include_age.Checked = True
        Me.opt_birth_exclude_age.Checked = False
        Me.ef_age_older_than.Value = Me.AwardConfig.Filter.OlderThan
        Me.ef_age_and_smaller_than.Value = Me.AwardConfig.Filter.AndSmallerThan



      ElseIf Me.AwardConfig.Filter.SmallerThan > 0 And Me.AwardConfig.Filter.OrOlderThan > 0 Then

        Me.opt_birth_include_age.Checked = False
        Me.opt_birth_exclude_age.Checked = True
        Me.ef_age_smaller_than.Value = Me.AwardConfig.Filter.SmallerThan
        Me.ef_age_or_older_than.Value = Me.AwardConfig.Filter.OrOlderThan

      Else

        Me.chk_by_age.Checked = False
        Me.opt_birth_exclude_age.Checked = False

      End If

    Else

      Me.chk_by_age.Checked = False

    End If

  End Sub

  ''' <summary>
  ''' Bind uc data
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub BindData()

    Dim _terminal_list As TerminalList
    Dim _is_selected_egm_terminals As Boolean

    _terminal_list = New TerminalList()
    _is_selected_egm_terminals = False

    If Me.AwardConfig Is Nothing Then
      Return
    End If

    Me.ExcludeAccount.Checked = Me.AwardConfig.Filter.ExcludeAccountsWithPromo
    Me.ExcludeAnonymousAccount.Checked = Me.AwardConfig.Filter.ExcludeAnonymousAccount
    Me.MinOccupation.Value = Me.AwardConfig.Filter.MinOccupancy
    Me.ef_minimum_bet.Value = Me.AwardConfig.Filter.MinimumBet

    Call BindByGender()
    Call BindByCreationDate()
    Call BindByDateOfBirth()

    Me.Uc_jackpot_time_award_config.BindData(Me.AwardConfig.WeekDays)
    Me.uc_jackpot_flags.IsConfigured = Me.AwardConfig.Filter.IsConfigured
    Me.uc_jackpot_flags.BindData(AwardConfig.Filter.Flags)


    Me.uc_jackpot_EGM_terminal_award.BindData(m_jackpot.Contributions, AwardConfig.Filter)

  End Sub ' BindData

#End Region

#Region "Function"

  Public Function IsScreenDataOk() As Boolean

    'Minimum Bet
    If Not CheckMinimumBet() Then
      Return False
    End If

    'Min Occupancy
    If Not CheckMinOccupancy() Then
      Return False
    End If

    ' Age created account 
    If Not CheckCreatedAccount() Then
      Return False
    End If

    'Age older range
    If Not CheckOlderRange() Then
      Return False
    End If

    'Age younger range
    If Not CheckYoungerRange() Then
      Return False
    End If

    'Anniversay range
    If Not CheckAnniversaryRange() Then
      Return False
    End If

    If Not Me.AwardDays.IsScreenDataOk() Then
      Return False
    End If

    If Not Me.Flags.IsScreenDataOk() Then
      Return False
    End If

    Return True

  End Function

  ''' <summary>
  ''' Check minimum bet in isScreenDataOk
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function CheckMinimumBet() As Boolean

    ' Empty Minimum Bet
    If String.IsNullOrEmpty(Me.ef_minimum_bet.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8097), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El campo apuesta mínima no puede estar vacío
      Call Me.ef_minimum_bet.Focus()

      Return False
    End If

    ' Minimum Bet lower than 0
    If GUI_ParseNumberDecimal(Me.ef_minimum_bet.Value) < 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8101), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El campo apuesta mínima no puede ser menor que 0
      Call Me.ef_minimum_bet.Focus()

      Return False
    End If

    Return True

  End Function
  ''' <summary>
  ''' Check created account in isScreenDataOk
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function CheckCreatedAccount() As Boolean 'CheckCreatedAccount

    If chk_by_created_account.Checked AndAlso opt_created_account_date.Checked AndAlso opt_created_account_working_day_plus.Checked Then

      If ef_created_account_working_day_plus.Value = String.Empty Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706))
        ef_created_account_working_day_plus.Focus()

        Return False
      ElseIf GUI_ParseNumberDecimal(ef_created_account_working_day_plus.Value) = Jackpot.DEFAULT_INT_VALUE Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5335), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706), Jackpot.DEFAULT_INT_VALUE)
        ef_created_account_working_day_plus.Focus()

        Return False
      End If

    End If

    Return True

  End Function 'CheckCreatedAccount

  ''' <summary>
  ''' Check anniversay range in isScreenDataOk
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckAnniversaryRange() As Boolean 'CheckAnniversaryRange

    Dim _year_from As Integer
    Dim _year_to As Integer

    If chk_by_created_account.Checked Then

      _year_from = GUI_ParseNumber(Me.ef_created_account_anniversary_year_from.Value)
      _year_to = GUI_ParseNumber(Me.ef_created_account_anniversary_year_to.Value)

      If _year_from > _year_to Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1195), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2707), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706))
        Call Me.ef_created_account_anniversary_year_from.Focus()

        Return False
      End If

    End If

    Return True

  End Function 'CheckAnniversaryRange

  ''' <summary>
  ''' Check min occupancy in isScreenDataOk
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckMinOccupancy() As Boolean 'CheckMinOccupancy

    Dim _min_occupancy_value As Decimal
    _min_occupancy_value = GUI_ParseNumberDecimal(Me.ef_min_ocupancy.Value)

    If Me.ef_min_ocupancy.Value = String.Empty OrElse _min_occupancy_value < 0 Or _min_occupancy_value > 100 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8111), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El valor de ocupación mínima ha de estar entre 0 y 100
      Call Me.ef_min_ocupancy.Focus()

      Return False
    End If

    If _min_occupancy_value < 0 And _min_occupancy_value > 100 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(8111), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK) ' El valor de ocupación mínima ha de estar entre 0 y 100
      Call Me.ef_min_ocupancy.Focus()

      Return False
    End If

    Return True

  End Function 'CheckMinOccupancy

  ''' <summary>
  ''' Check Age values in screen data ok
  ''' </summary>
  ''' <remarks></remarks>
  Private Function CheckOlderRange() As Boolean 'CheckOlderRange

    Dim _and_smaller_than As Integer
    Dim _older_than As Integer
    Dim _legal_age As Integer

    _older_than = Jackpot.DEFAULT_INT_VALUE
    _and_smaller_than = Jackpot.DEFAULT_INT_VALUE


    Integer.TryParse(WSI.Common.Misc.ReadGeneralParams("Cashier", "LegalAge"), _legal_age)

    If chk_by_age.Checked Then
      If Me.opt_birth_include_age.Checked Then
        _older_than = GUI_ParseNumber(Me.ef_age_older_than.Value)
        _and_smaller_than = GUI_ParseNumber(Me.ef_age_and_smaller_than.Value)

        If _older_than > _and_smaller_than Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1195), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2707), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706))
          Call Me.ef_age_older_than.Focus()

          Return False
        End If

        If ef_age_older_than.Value = String.Empty Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706))
          ef_age_older_than.Focus()
          Return False
        End If

        If ef_age_and_smaller_than.Value = String.Empty Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706))
          ef_age_and_smaller_than.Focus()
          Return False
        End If

        If ef_age_older_than.Value < _legal_age Then
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(124), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706), _legal_age)
          ef_age_older_than.Focus()
          Return False
        End If
      End If

    End If

    Return True

  End Function 'CheckOlderRange

  ''' <summary>
  ''' 
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckYoungerRange() As Boolean 'CheckYoungerRange

    Dim _smaller_than As Integer
    Dim _or_older_than As Integer
    Dim _legal_age As Integer

    If chk_by_created_account.Checked Then

      _smaller_than = Jackpot.DEFAULT_INT_VALUE
      _or_older_than = Jackpot.DEFAULT_INT_VALUE
      Integer.TryParse(WSI.Common.Misc.ReadGeneralParams("Cashier", "LegalAge"), _legal_age)

      If Me.opt_birth_exclude_age.Checked And Me.opt_birth_exclude_age.Enabled Then
        _smaller_than = GUI_ParseNumber(Me.ef_age_smaller_than.Value)
        _or_older_than = GUI_ParseNumber(Me.ef_age_or_older_than.Value)

        If _smaller_than > _or_older_than Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1195), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2707), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706))
          Call Me.ef_age_smaller_than.Focus()

          Return False
        End If

        If ef_age_smaller_than.Value = String.Empty Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706))
          ef_age_smaller_than.Focus()
          Return False
        End If

        If ef_age_or_older_than.Value = String.Empty Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706))
          ef_age_or_older_than.Focus()
          Return False
        End If

        If ef_age_smaller_than.Value < _legal_age Then
          Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(124), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2706), _legal_age)
          ef_age_smaller_than.Focus()
          Return False
        End If

      End If

      If opt_created_account_anniversary.Checked And chk_by_anniversary_range_of_years.Checked Then
        If ef_created_account_anniversary_year_from.TextValue = String.Empty Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6732))
          Call ef_created_account_anniversary_year_from.Focus()
          Return False
        ElseIf ef_created_account_anniversary_year_from.Value = Jackpot.DEFAULT_INT_VALUE Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5335), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6732), Jackpot.DEFAULT_INT_VALUE)
          ef_created_account_anniversary_year_from.Focus()
          Return False
        End If

        If ef_created_account_anniversary_year_to.TextValue = String.Empty Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6733))
          Call ef_created_account_anniversary_year_to.Focus()
          Return False
        End If

        If Integer.Parse(ef_created_account_anniversary_year_from.Value) > Integer.Parse(ef_created_account_anniversary_year_to.Value) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6782), ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(6732), GLB_NLS_GUI_PLAYER_TRACKING.GetString(6733))
          ef_created_account_anniversary_year_to.Focus()
          Return False
        End If
      End If

    End If


    Return True

  End Function 'CheckYoungerRange

  ''' <summary>
  ''' Get all data 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GetScreenData()

    ' Get Award Filters
    Call Me.GetAwardFilters()

    ' Get Award Days data
    Me.AwardConfig.WeekDays = Me.AwardDays.ToJackpotAwardDays()

  End Sub

  ''' <summary>
  ''' Get award filters screen data
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GetAwardFilters()

    Dim _terminal_list As TerminalList
    Dim _str As String

    ' Get By Gender data
    Call Me.MapByGender()

    ' Get By Date Of Birth
    Call Me.MapByDateOfBirth()

    'Get By Creation Date
    Call Me.MapByCreationDate()

    ' Other data
    Me.AwardConfig.Filter.ExcludeAccountsWithPromo = Me.chk_exclude_account_with_promotion.Checked
    Me.AwardConfig.Filter.ExcludeAnonymousAccount = Me.chk_exclude_anonymous_accounts.Checked
    Me.AwardConfig.Filter.MinOccupancy = GUI_ParseNumberDecimal(Me.ef_min_ocupancy.Value)
    Me.AwardConfig.Filter.MinimumBet = GUI_ParseNumberDecimal(Me.ef_minimum_bet.Value)

    ' Terminal list
    _terminal_list = New TerminalList()
    Me.uc_jackpot_EGM_terminal_award.dg_providers.GetTerminalsFromControl(_terminal_list)

    If Not _terminal_list.ToXml() Is Nothing Then
      _str = _terminal_list.ToXml().Replace(vbCr, "").Replace(vbLf, "")

      Me.AwardConfig.Filter.SelectedEGM = _str.Replace(">  <", "><").Replace(">    <", "><")
    Else
      Me.AwardConfig.Filter.SelectedEGM = ""
    End If

    If (Me.uc_jackpot_EGM_terminal_award.IsSelectedEGMType) Then
      Me.AwardConfig.Filter.SelectionEGMType = JackpotAwardFilter.JackpotSelectionEGMType.Selected
    Else
      Me.AwardConfig.Filter.SelectionEGMType = JackpotAwardFilter.JackpotSelectionEGMType.All
    End If

    'Flags
    Me.AwardConfig.Filter.Flags = Me.Flags.ToJackpotFlags()

    ' Set last modification
    Me.AwardConfig.Filter.LastModification = WGDB.Now

  End Sub ' GetAwardFilters

  ''' <summary>
  ''' Sub to map By Gender data
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MapByGender()

    ' By Gender is checked
    If Me.chk_by_gender.Checked Then

      ' Male is checked
      If opt_gender_male.Checked Then
        Me.AwardConfig.Filter.ByGender = GENDER.MALE
      Else
        ' Female is checked
        Me.AwardConfig.Filter.ByGender = GENDER.FEMALE
      End If

    Else
      ' By Gender is not checked
      Me.AwardConfig.Filter.ByGender = GENDER.UNKNOWN
    End If

  End Sub

  ''' <summary>
  ''' Sub to map by Date Of Birth Data
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MapByDateOfBirth()
    'By date of birth checked
    If chk_by_birth_date.Checked Then

      ' Applies any day of day
      If opt_birth_month_only.Checked Then
        Me.AwardConfig.Filter.ByDateOfBirth = AwardFilterDateOfBirth.Birthday

      Else
        ' Applies day and month 
        Me.AwardConfig.Filter.ByDateOfBirth = AwardFilterDateOfBirth.DayAndMonth
      End If

    Else
      ' not checked
      Me.AwardConfig.Filter.ByDateOfBirth = AwardFilterDateOfBirth.Disabled
    End If

    ' Age Range checked
    If chk_by_age.Checked Then

      'Incluede checked
      If opt_birth_include_age.Checked Then
        Me.AwardConfig.Filter.AgeRange = True
        Me.AwardConfig.Filter.OlderThan = GUI_ParseNumber(Me.ef_age_older_than.Value)
        Me.AwardConfig.Filter.AndSmallerThan = GUI_ParseNumber(Me.ef_age_and_smaller_than.Value)
        Me.AwardConfig.Filter.SmallerThan = Jackpot.DEFAULT_INT_VALUE
        Me.AwardConfig.Filter.OrOlderThan = Jackpot.DEFAULT_INT_VALUE

      Else
        ' Exclude checked
        Me.AwardConfig.Filter.AgeRange = True
        Me.AwardConfig.Filter.SmallerThan = GUI_ParseNumber(Me.ef_age_smaller_than.Value)
        Me.AwardConfig.Filter.OrOlderThan = GUI_ParseNumber(Me.ef_age_or_older_than.Value)
        Me.AwardConfig.Filter.OlderThan = Jackpot.DEFAULT_INT_VALUE
        Me.AwardConfig.Filter.AndSmallerThan = Jackpot.DEFAULT_INT_VALUE
      End If

    Else

      ' Age Range Not Checked
      Me.AwardConfig.Filter.AgeRange = False
      Me.AwardConfig.Filter.SmallerThan = Jackpot.DEFAULT_INT_VALUE
      Me.AwardConfig.Filter.OrOlderThan = Jackpot.DEFAULT_INT_VALUE
      Me.AwardConfig.Filter.OlderThan = Jackpot.DEFAULT_INT_VALUE
      Me.AwardConfig.Filter.AndSmallerThan = Jackpot.DEFAULT_INT_VALUE
    End If

  End Sub

  ''' <summary>
  ''' Sub to map By CreationDate Data
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub MapByCreationDate()

    ' By Creation date Checked
    If chk_by_created_account.Checked Then

      ' Alta is checked
      If opt_created_account_date.Checked Then

        ' Just day of creation checked
        If opt_created_account_working_day_only.Checked Then
          Me.AwardConfig.Filter.JustDayOfCreation = True
          Me.AwardConfig.Filter.ByCreationDate = AwardFilterCreationDate.Creation
          Me.AwardConfig.Filter.DayOfCreationPlus = Jackpot.DEFAULT_INT_VALUE

        Else
          ' Day of creation plus
          Me.AwardConfig.Filter.ByCreationDate = AwardFilterCreationDate.Creation
          Me.AwardConfig.Filter.DayOfCreationPlus = GUI_ParseNumberDecimal(Me.ef_created_account_working_day_plus.Value)
          Me.AwardConfig.Filter.JustDayOfCreation = False

        End If

      Else

        ' Anniversary checked
        If opt_created_account_anniversary_whole_month.Checked Then
          Me.AwardConfig.Filter.AnyDayOfMonth = True
          Me.AwardConfig.Filter.DayAndMonth = False

          If chk_by_anniversary_range_of_years.Checked Then
            Me.AwardConfig.Filter.CreationBetweenThis = GUI_ParseNumberDecimal(Me.ef_created_account_anniversary_year_from.Value)
            Me.AwardConfig.Filter.CreationBetweenThat = GUI_ParseNumberDecimal(Me.ef_created_account_anniversary_year_to.Value)

          Else
            Me.AwardConfig.Filter.CreationBetweenThis = Jackpot.DEFAULT_INT_VALUE
            Me.AwardConfig.Filter.CreationBetweenThat = Jackpot.DEFAULT_INT_VALUE

          End If

          Me.AwardConfig.Filter.DayOfCreationPlus = Jackpot.DEFAULT_INT_VALUE
          Me.AwardConfig.Filter.JustDayOfCreation = False
          Me.AwardConfig.Filter.ByCreationDate = AwardFilterCreationDate.CreationBirthdate

          'Any day of month is checked
        ElseIf opt_created_account_anniversary_day_month.Checked Then
          Me.AwardConfig.Filter.AnyDayOfMonth = False
          Me.AwardConfig.Filter.DayAndMonth = True

          ' Range checked
          If chk_by_anniversary_range_of_years.Checked Then
            Me.AwardConfig.Filter.CreationBetweenThis = GUI_ParseNumber(Me.ef_created_account_anniversary_year_from.Value)
            Me.AwardConfig.Filter.CreationBetweenThat = GUI_ParseNumber(Me.ef_created_account_anniversary_year_to.Value)

          Else
            Me.AwardConfig.Filter.CreationBetweenThis = Jackpot.DEFAULT_INT_VALUE
            Me.AwardConfig.Filter.CreationBetweenThat = Jackpot.DEFAULT_INT_VALUE

          End If

          Me.AwardConfig.Filter.ByCreationDate = AwardFilterCreationDate.CreationBirthdate

        Else
          'Range checked
          Me.AwardConfig.Filter.AnyDayOfMonth = False
          Me.AwardConfig.Filter.DayAndMonth = False
          Me.AwardConfig.Filter.CreationBetweenThis = GUI_ParseNumber(Me.ef_created_account_anniversary_year_from.Value)
          Me.AwardConfig.Filter.CreationBetweenThat = GUI_ParseNumber(Me.ef_created_account_anniversary_year_to.Value)
          Me.AwardConfig.Filter.ByCreationDate = AwardFilterCreationDate.CreationBirthdate

        End If

      End If

    Else
      ' Nothing checked
      Me.AwardConfig.Filter.ByCreationDate = AwardFilterCreationDate.Disabled
      Me.AwardConfig.Filter.DayOfCreationPlus = Jackpot.DEFAULT_INT_VALUE
      Me.AwardConfig.Filter.JustDayOfCreation = False
      Me.AwardConfig.Filter.AnyDayOfMonth = False
      Me.AwardConfig.Filter.DayAndMonth = False
      Me.AwardConfig.Filter.CreationBetweenThis = Jackpot.DEFAULT_INT_VALUE
      Me.AwardConfig.Filter.CreationBetweenThat = Jackpot.DEFAULT_INT_VALUE

    End If

  End Sub

  Public Function GetJackpotAwardConfig() As JackpotAwardConfig

    Me.GetScreenData()

    Return Me.AwardConfig
  End Function ' GetJackpotAwardConfig

#End Region

#Region "Events"

  Private Sub chk_by_gender_CheckedChanged(sender As Object, e As EventArgs) Handles chk_by_gender.CheckedChanged
    opt_gender_male.Enabled = chk_by_gender.Checked
    opt_gender_female.Enabled = chk_by_gender.Checked
  End Sub

  Private Sub chk_by_birth_date_CheckedChanged(sender As Object, e As EventArgs) Handles chk_by_birth_date.CheckedChanged
    opt_birth_date_only.Enabled = chk_by_birth_date.Checked
    opt_birth_month_only.Enabled = chk_by_birth_date.Checked
  End Sub

  Private Sub chk_by_age_CheckedChanged(sender As Object, e As EventArgs) Handles chk_by_age.CheckedChanged
    opt_birth_include_age.Enabled = chk_by_age.Checked
    opt_birth_exclude_age.Enabled = chk_by_age.Checked
    If Not chk_by_age.Checked Then
      ef_age_smaller_than.Enabled = False
      ef_age_older_than.Enabled = False
      ef_age_or_older_than.Enabled = False
      ef_age_and_smaller_than.Enabled = False
      ef_age_smaller_than.Value = String.Empty
      ef_age_older_than.Value = String.Empty
      ef_age_or_older_than.Value = String.Empty
      ef_age_and_smaller_than.Value = String.Empty
    ElseIf opt_birth_include_age.Checked Then
      ef_age_older_than.Enabled = True
      ef_age_and_smaller_than.Enabled = True
    ElseIf opt_birth_exclude_age.Checked Then
      ef_age_smaller_than.Enabled = True
      ef_age_or_older_than.Enabled = True
    End If
  End Sub

  Private Sub chk_by_created_account_CheckedChanged(sender As Object, e As EventArgs) Handles chk_by_created_account.CheckedChanged
    opt_created_account_date.Enabled = chk_by_created_account.Checked
    opt_created_account_anniversary.Enabled = chk_by_created_account.Checked
    If chk_by_created_account.Checked Then
      EnableCreatedAccountDateOptions()
    Else
      opt_created_account_working_day_only.Enabled = False
      opt_created_account_working_day_plus.Enabled = False
      ef_created_account_working_day_plus.Enabled = False
      opt_created_account_anniversary_whole_month.Enabled = False
      opt_created_account_anniversary_day_month.Enabled = False
      chk_by_anniversary_range_of_years.Enabled = False
      ef_created_account_anniversary_year_from.Enabled = False
      ef_created_account_anniversary_year_to.Enabled = False
    End If
  End Sub

  Private Sub opt_created_account_date_CheckedChanged(sender As Object, e As EventArgs) Handles opt_created_account_date.CheckedChanged
    EnableCreatedAccountDateOptions()
  End Sub

  Private Sub opt_created_account_anniversary_CheckedChanged(sender As Object, e As EventArgs) Handles opt_created_account_anniversary.CheckedChanged
    EnableCreatedAccountDateOptions()
  End Sub

  Private Sub opt_include_exclude_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_birth_include_age.CheckedChanged, opt_birth_exclude_age.CheckedChanged
    ef_age_smaller_than.Enabled = opt_birth_exclude_age.Checked
    ef_age_older_than.Enabled = opt_birth_include_age.Checked
    ef_age_or_older_than.Enabled = opt_birth_exclude_age.Checked
    ef_age_and_smaller_than.Enabled = opt_birth_include_age.Checked
    If Not ef_age_smaller_than.Enabled And Not ef_age_or_older_than.Enabled And Not ef_age_older_than.Enabled And Not ef_age_and_smaller_than.Enabled Then
      ef_age_older_than.Value = String.Empty
      ef_age_and_smaller_than.Value = String.Empty
      ef_age_smaller_than.Value = String.Empty
      ef_age_or_older_than.Value = String.Empty
    ElseIf Not ef_age_smaller_than.Enabled And Not ef_age_or_older_than.Enabled Then
      ef_age_smaller_than.Value = String.Empty
      ef_age_or_older_than.Value = String.Empty
    ElseIf Not ef_age_older_than.Enabled And Not ef_age_and_smaller_than.Enabled Then
      ef_age_older_than.Value = String.Empty
      ef_age_and_smaller_than.Value = String.Empty
    End If
  End Sub
  Private Sub chk_by_anniversary_range_of_years_CheckedChanged(sender As Object, e As EventArgs) Handles chk_by_anniversary_range_of_years.CheckedChanged
    ef_created_account_anniversary_year_from.Enabled = chk_by_anniversary_range_of_years.Checked
    ef_created_account_anniversary_year_to.Enabled = chk_by_anniversary_range_of_years.Checked
  End Sub
#End Region

End Class
