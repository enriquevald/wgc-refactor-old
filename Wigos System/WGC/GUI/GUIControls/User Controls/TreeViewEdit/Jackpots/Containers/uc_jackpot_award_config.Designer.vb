﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_jackpot_award_config
  Inherits GUI_Controls.uc_treeview_edit_base

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  '<System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.chk_exclude_anonymous_accounts = New System.Windows.Forms.CheckBox()
    Me.chk_exclude_account_with_promotion = New System.Windows.Forms.CheckBox()
    Me.gb_by_creation_date = New System.Windows.Forms.GroupBox()
    Me.Panel3 = New System.Windows.Forms.Panel()
    Me.ef_created_account_anniversary_year_to = New GUI_Controls.uc_entry_field()
    Me.chk_by_anniversary_range_of_years = New System.Windows.Forms.CheckBox()
    Me.opt_created_account_anniversary_day_month = New System.Windows.Forms.RadioButton()
    Me.opt_created_account_anniversary_whole_month = New System.Windows.Forms.RadioButton()
    Me.ef_created_account_anniversary_year_from = New GUI_Controls.uc_entry_field()
    Me.Panel2 = New System.Windows.Forms.Panel()
    Me.opt_created_account_working_day_plus = New System.Windows.Forms.RadioButton()
    Me.opt_created_account_working_day_only = New System.Windows.Forms.RadioButton()
    Me.ef_created_account_working_day_plus = New GUI_Controls.uc_entry_field()
    Me.opt_created_account_anniversary = New System.Windows.Forms.RadioButton()
    Me.opt_created_account_date = New System.Windows.Forms.RadioButton()
    Me.chk_by_created_account = New System.Windows.Forms.CheckBox()
    Me.gb_date_of_birth = New System.Windows.Forms.GroupBox()
    Me.Panel1 = New System.Windows.Forms.Panel()
    Me.ef_age_older_than = New GUI_Controls.uc_entry_field()
    Me.ef_age_and_smaller_than = New GUI_Controls.uc_entry_field()
    Me.chk_by_age = New System.Windows.Forms.CheckBox()
    Me.ef_age_smaller_than = New GUI_Controls.uc_entry_field()
    Me.opt_birth_exclude_age = New System.Windows.Forms.RadioButton()
    Me.opt_birth_include_age = New System.Windows.Forms.RadioButton()
    Me.ef_age_or_older_than = New GUI_Controls.uc_entry_field()
    Me.opt_birth_date_only = New System.Windows.Forms.RadioButton()
    Me.chk_by_birth_date = New System.Windows.Forms.CheckBox()
    Me.opt_birth_month_only = New System.Windows.Forms.RadioButton()
    Me.gb_by_gender = New System.Windows.Forms.GroupBox()
    Me.opt_gender_male = New System.Windows.Forms.RadioButton()
    Me.opt_gender_female = New System.Windows.Forms.RadioButton()
    Me.chk_by_gender = New System.Windows.Forms.CheckBox()
    Me.ef_min_ocupancy = New GUI_Controls.uc_entry_field()
    Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
    Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
    Me.ContextMenuStrip2 = New System.Windows.Forms.ContextMenuStrip(Me.components)
    Me.BackgroundWorker2 = New System.ComponentModel.BackgroundWorker()
    Me.BackgroundWorker3 = New System.ComponentModel.BackgroundWorker()
    Me.Uc_jackpot_time_award_config = New GUI_Controls.uc_jackpot_time_award()
    Me.gb_misc = New System.Windows.Forms.GroupBox()
    Me.ef_minimum_bet = New GUI_Controls.uc_entry_field()
    Me.uc_jackpot_flags = New GUI_Controls.uc_jackpot_flags()
    Me.uc_jackpot_EGM_terminal_award = New GUI_Controls.uc_jackpot_EGM_terminal_award()
    Me.pnl_container.SuspendLayout()
    Me.gb_by_creation_date.SuspendLayout()
    Me.Panel3.SuspendLayout()
    Me.Panel2.SuspendLayout()
    Me.gb_date_of_birth.SuspendLayout()
    Me.Panel1.SuspendLayout()
    Me.gb_by_gender.SuspendLayout()
    Me.gb_misc.SuspendLayout()
    Me.SuspendLayout()
    '
    'pnl_container
    '
    Me.pnl_container.BackColor = System.Drawing.SystemColors.Control
    Me.pnl_container.Controls.Add(Me.uc_jackpot_EGM_terminal_award)
    Me.pnl_container.Controls.Add(Me.uc_jackpot_flags)
    Me.pnl_container.Controls.Add(Me.gb_misc)
    Me.pnl_container.Controls.Add(Me.Uc_jackpot_time_award_config)
    Me.pnl_container.Controls.Add(Me.gb_by_creation_date)
    Me.pnl_container.Controls.Add(Me.gb_date_of_birth)
    Me.pnl_container.Controls.Add(Me.gb_by_gender)
    Me.pnl_container.Size = New System.Drawing.Size(839, 694)
    '
    'chk_exclude_anonymous_accounts
    '
    Me.chk_exclude_anonymous_accounts.AutoSize = True
    Me.chk_exclude_anonymous_accounts.Location = New System.Drawing.Point(28, 42)
    Me.chk_exclude_anonymous_accounts.Name = "chk_exclude_anonymous_accounts"
    Me.chk_exclude_anonymous_accounts.Size = New System.Drawing.Size(169, 17)
    Me.chk_exclude_anonymous_accounts.TabIndex = 1
    Me.chk_exclude_anonymous_accounts.Text = "xExcludeAnonymousAccounts"
    Me.chk_exclude_anonymous_accounts.UseVisualStyleBackColor = True
    '
    'chk_exclude_account_with_promotion
    '
    Me.chk_exclude_account_with_promotion.AutoSize = True
    Me.chk_exclude_account_with_promotion.Location = New System.Drawing.Point(28, 19)
    Me.chk_exclude_account_with_promotion.Name = "chk_exclude_account_with_promotion"
    Me.chk_exclude_account_with_promotion.Size = New System.Drawing.Size(183, 17)
    Me.chk_exclude_account_with_promotion.TabIndex = 0
    Me.chk_exclude_account_with_promotion.Text = "xExcludeAccountsWithPromotion"
    Me.chk_exclude_account_with_promotion.UseVisualStyleBackColor = True
    '
    'gb_by_creation_date
    '
    Me.gb_by_creation_date.Controls.Add(Me.Panel3)
    Me.gb_by_creation_date.Controls.Add(Me.Panel2)
    Me.gb_by_creation_date.Controls.Add(Me.opt_created_account_anniversary)
    Me.gb_by_creation_date.Controls.Add(Me.opt_created_account_date)
    Me.gb_by_creation_date.Controls.Add(Me.chk_by_created_account)
    Me.gb_by_creation_date.Location = New System.Drawing.Point(7, 221)
    Me.gb_by_creation_date.Name = "gb_by_creation_date"
    Me.gb_by_creation_date.Size = New System.Drawing.Size(430, 204)
    Me.gb_by_creation_date.TabIndex = 1
    Me.gb_by_creation_date.TabStop = False
    Me.gb_by_creation_date.Text = "xCreationDate"
    '
    'Panel3
    '
    Me.Panel3.Controls.Add(Me.ef_created_account_anniversary_year_to)
    Me.Panel3.Controls.Add(Me.chk_by_anniversary_range_of_years)
    Me.Panel3.Controls.Add(Me.opt_created_account_anniversary_day_month)
    Me.Panel3.Controls.Add(Me.opt_created_account_anniversary_whole_month)
    Me.Panel3.Controls.Add(Me.ef_created_account_anniversary_year_from)
    Me.Panel3.Location = New System.Drawing.Point(47, 119)
    Me.Panel3.Name = "Panel3"
    Me.Panel3.Size = New System.Drawing.Size(374, 80)
    Me.Panel3.TabIndex = 4
    '
    'ef_created_account_anniversary_year_to
    '
    Me.ef_created_account_anniversary_year_to.DoubleValue = 0.0R
    Me.ef_created_account_anniversary_year_to.IntegerValue = 0
    Me.ef_created_account_anniversary_year_to.IsReadOnly = False
    Me.ef_created_account_anniversary_year_to.Location = New System.Drawing.Point(207, 51)
    Me.ef_created_account_anniversary_year_to.Name = "ef_created_account_anniversary_year_to"
    Me.ef_created_account_anniversary_year_to.PlaceHolder = Nothing
    Me.ef_created_account_anniversary_year_to.Size = New System.Drawing.Size(155, 24)
    Me.ef_created_account_anniversary_year_to.SufixText = "xYear_to"
    Me.ef_created_account_anniversary_year_to.SufixTextVisible = True
    Me.ef_created_account_anniversary_year_to.SufixTextWidth = 130
    Me.ef_created_account_anniversary_year_to.TabIndex = 4
    Me.ef_created_account_anniversary_year_to.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_created_account_anniversary_year_to.TextValue = "0"
    Me.ef_created_account_anniversary_year_to.TextVisible = False
    Me.ef_created_account_anniversary_year_to.TextWidth = 0
    Me.ef_created_account_anniversary_year_to.Value = "0"
    Me.ef_created_account_anniversary_year_to.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_by_anniversary_range_of_years
    '
    Me.chk_by_anniversary_range_of_years.Location = New System.Drawing.Point(7, 56)
    Me.chk_by_anniversary_range_of_years.Name = "chk_by_anniversary_range_of_years"
    Me.chk_by_anniversary_range_of_years.Size = New System.Drawing.Size(150, 17)
    Me.chk_by_anniversary_range_of_years.TabIndex = 2
    Me.chk_by_anniversary_range_of_years.Text = "xBy_anniversary_range_of_years"
    Me.chk_by_anniversary_range_of_years.UseVisualStyleBackColor = True
    '
    'opt_created_account_anniversary_day_month
    '
    Me.opt_created_account_anniversary_day_month.AccessibleDescription = ""
    Me.opt_created_account_anniversary_day_month.Location = New System.Drawing.Point(7, 27)
    Me.opt_created_account_anniversary_day_month.Name = "opt_created_account_anniversary_day_month"
    Me.opt_created_account_anniversary_day_month.Size = New System.Drawing.Size(337, 17)
    Me.opt_created_account_anniversary_day_month.TabIndex = 1
    Me.opt_created_account_anniversary_day_month.TabStop = True
    Me.opt_created_account_anniversary_day_month.Tag = ""
    Me.opt_created_account_anniversary_day_month.Text = "xCreated_account_anniversary_day_month"
    '
    'opt_created_account_anniversary_whole_month
    '
    Me.opt_created_account_anniversary_whole_month.Location = New System.Drawing.Point(7, 3)
    Me.opt_created_account_anniversary_whole_month.Name = "opt_created_account_anniversary_whole_month"
    Me.opt_created_account_anniversary_whole_month.Size = New System.Drawing.Size(337, 17)
    Me.opt_created_account_anniversary_whole_month.TabIndex = 0
    Me.opt_created_account_anniversary_whole_month.TabStop = True
    Me.opt_created_account_anniversary_whole_month.Tag = ""
    Me.opt_created_account_anniversary_whole_month.Text = "xCreated_account_anniversary_whole_month"
    '
    'ef_created_account_anniversary_year_from
    '
    Me.ef_created_account_anniversary_year_from.DoubleValue = 0.0R
    Me.ef_created_account_anniversary_year_from.IntegerValue = 0
    Me.ef_created_account_anniversary_year_from.IsReadOnly = False
    Me.ef_created_account_anniversary_year_from.Location = New System.Drawing.Point(155, 52)
    Me.ef_created_account_anniversary_year_from.Name = "ef_created_account_anniversary_year_from"
    Me.ef_created_account_anniversary_year_from.PlaceHolder = Nothing
    Me.ef_created_account_anniversary_year_from.Size = New System.Drawing.Size(56, 24)
    Me.ef_created_account_anniversary_year_from.SufixText = "xYear_from"
    Me.ef_created_account_anniversary_year_from.SufixTextVisible = True
    Me.ef_created_account_anniversary_year_from.SufixTextWidth = 30
    Me.ef_created_account_anniversary_year_from.TabIndex = 3
    Me.ef_created_account_anniversary_year_from.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_created_account_anniversary_year_from.TextValue = "0"
    Me.ef_created_account_anniversary_year_from.TextVisible = False
    Me.ef_created_account_anniversary_year_from.TextWidth = 0
    Me.ef_created_account_anniversary_year_from.Value = "0"
    Me.ef_created_account_anniversary_year_from.ValueForeColor = System.Drawing.Color.Blue
    '
    'Panel2
    '
    Me.Panel2.Controls.Add(Me.opt_created_account_working_day_plus)
    Me.Panel2.Controls.Add(Me.opt_created_account_working_day_only)
    Me.Panel2.Controls.Add(Me.ef_created_account_working_day_plus)
    Me.Panel2.Location = New System.Drawing.Point(47, 55)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(374, 47)
    Me.Panel2.TabIndex = 2
    '
    'opt_created_account_working_day_plus
    '
    Me.opt_created_account_working_day_plus.Location = New System.Drawing.Point(7, 24)
    Me.opt_created_account_working_day_plus.Name = "opt_created_account_working_day_plus"
    Me.opt_created_account_working_day_plus.Size = New System.Drawing.Size(213, 17)
    Me.opt_created_account_working_day_plus.TabIndex = 1
    Me.opt_created_account_working_day_plus.TabStop = True
    Me.opt_created_account_working_day_plus.Tag = ""
    Me.opt_created_account_working_day_plus.Text = "xCreated_account_working_day_plus"
    '
    'opt_created_account_working_day_only
    '
    Me.opt_created_account_working_day_only.Location = New System.Drawing.Point(7, 3)
    Me.opt_created_account_working_day_only.Name = "opt_created_account_working_day_only"
    Me.opt_created_account_working_day_only.Size = New System.Drawing.Size(337, 17)
    Me.opt_created_account_working_day_only.TabIndex = 0
    Me.opt_created_account_working_day_only.TabStop = True
    Me.opt_created_account_working_day_only.Tag = ""
    Me.opt_created_account_working_day_only.Text = "xCreated_account_working_day_only"
    '
    'ef_created_account_working_day_plus
    '
    Me.ef_created_account_working_day_plus.DoubleValue = 0.0R
    Me.ef_created_account_working_day_plus.IntegerValue = 0
    Me.ef_created_account_working_day_plus.IsReadOnly = False
    Me.ef_created_account_working_day_plus.Location = New System.Drawing.Point(218, 21)
    Me.ef_created_account_working_day_plus.Name = "ef_created_account_working_day_plus"
    Me.ef_created_account_working_day_plus.PlaceHolder = Nothing
    Me.ef_created_account_working_day_plus.Size = New System.Drawing.Size(132, 24)
    Me.ef_created_account_working_day_plus.SufixText = "xWorkingDay(s) to apply, after created account working day"
    Me.ef_created_account_working_day_plus.SufixTextVisible = True
    Me.ef_created_account_working_day_plus.SufixTextWidth = 100
    Me.ef_created_account_working_day_plus.TabIndex = 2
    Me.ef_created_account_working_day_plus.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_created_account_working_day_plus.TextValue = ""
    Me.ef_created_account_working_day_plus.TextVisible = False
    Me.ef_created_account_working_day_plus.TextWidth = 0
    Me.ef_created_account_working_day_plus.Value = ""
    Me.ef_created_account_working_day_plus.ValueForeColor = System.Drawing.Color.Blue
    '
    'opt_created_account_anniversary
    '
    Me.opt_created_account_anniversary.Location = New System.Drawing.Point(36, 101)
    Me.opt_created_account_anniversary.Name = "opt_created_account_anniversary"
    Me.opt_created_account_anniversary.Size = New System.Drawing.Size(344, 17)
    Me.opt_created_account_anniversary.TabIndex = 3
    Me.opt_created_account_anniversary.TabStop = True
    Me.opt_created_account_anniversary.Tag = ""
    Me.opt_created_account_anniversary.Text = "xCreated_account_anniversary"
    '
    'opt_created_account_date
    '
    Me.opt_created_account_date.Location = New System.Drawing.Point(36, 36)
    Me.opt_created_account_date.Name = "opt_created_account_date"
    Me.opt_created_account_date.Size = New System.Drawing.Size(344, 17)
    Me.opt_created_account_date.TabIndex = 1
    Me.opt_created_account_date.TabStop = True
    Me.opt_created_account_date.Text = "xCreated_account_date"
    '
    'chk_by_created_account
    '
    Me.chk_by_created_account.AutoSize = True
    Me.chk_by_created_account.Checked = True
    Me.chk_by_created_account.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_by_created_account.Location = New System.Drawing.Point(20, 19)
    Me.chk_by_created_account.Name = "chk_by_created_account"
    Me.chk_by_created_account.Size = New System.Drawing.Size(61, 17)
    Me.chk_by_created_account.TabIndex = 0
    Me.chk_by_created_account.Text = "xActive"
    Me.chk_by_created_account.UseVisualStyleBackColor = True
    '
    'gb_date_of_birth
    '
    Me.gb_date_of_birth.Controls.Add(Me.Panel1)
    Me.gb_date_of_birth.Controls.Add(Me.opt_birth_date_only)
    Me.gb_date_of_birth.Controls.Add(Me.chk_by_birth_date)
    Me.gb_date_of_birth.Controls.Add(Me.opt_birth_month_only)
    Me.gb_date_of_birth.Location = New System.Drawing.Point(7, 427)
    Me.gb_date_of_birth.Name = "gb_date_of_birth"
    Me.gb_date_of_birth.Size = New System.Drawing.Size(430, 157)
    Me.gb_date_of_birth.TabIndex = 2
    Me.gb_date_of_birth.TabStop = False
    Me.gb_date_of_birth.Text = "xDateOfBirth"
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.ef_age_older_than)
    Me.Panel1.Controls.Add(Me.ef_age_and_smaller_than)
    Me.Panel1.Controls.Add(Me.chk_by_age)
    Me.Panel1.Controls.Add(Me.ef_age_smaller_than)
    Me.Panel1.Controls.Add(Me.opt_birth_exclude_age)
    Me.Panel1.Controls.Add(Me.opt_birth_include_age)
    Me.Panel1.Controls.Add(Me.ef_age_or_older_than)
    Me.Panel1.Location = New System.Drawing.Point(14, 82)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(407, 71)
    Me.Panel1.TabIndex = 3
    '
    'ef_age_older_than
    '
    Me.ef_age_older_than.DoubleValue = 0.0R
    Me.ef_age_older_than.IntegerValue = 0
    Me.ef_age_older_than.IsReadOnly = False
    Me.ef_age_older_than.Location = New System.Drawing.Point(138, 22)
    Me.ef_age_older_than.Name = "ef_age_older_than"
    Me.ef_age_older_than.PlaceHolder = Nothing
    Me.ef_age_older_than.Size = New System.Drawing.Size(39, 24)
    Me.ef_age_older_than.SufixText = "Sufix Text"
    Me.ef_age_older_than.SufixTextVisible = True
    Me.ef_age_older_than.TabIndex = 3
    Me.ef_age_older_than.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_age_older_than.TextValue = ""
    Me.ef_age_older_than.TextVisible = False
    Me.ef_age_older_than.TextWidth = 1
    Me.ef_age_older_than.Value = ""
    Me.ef_age_older_than.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_age_and_smaller_than
    '
    Me.ef_age_and_smaller_than.DoubleValue = 0.0R
    Me.ef_age_and_smaller_than.IntegerValue = 0
    Me.ef_age_and_smaller_than.IsReadOnly = False
    Me.ef_age_and_smaller_than.Location = New System.Drawing.Point(183, 22)
    Me.ef_age_and_smaller_than.Name = "ef_age_and_smaller_than"
    Me.ef_age_and_smaller_than.PlaceHolder = Nothing
    Me.ef_age_and_smaller_than.Size = New System.Drawing.Size(153, 24)
    Me.ef_age_and_smaller_than.SufixText = "Sufix Text"
    Me.ef_age_and_smaller_than.SufixTextVisible = True
    Me.ef_age_and_smaller_than.TabIndex = 4
    Me.ef_age_and_smaller_than.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_age_and_smaller_than.TextValue = ""
    Me.ef_age_and_smaller_than.TextVisible = False
    Me.ef_age_and_smaller_than.TextWidth = 115
    Me.ef_age_and_smaller_than.Value = ""
    Me.ef_age_and_smaller_than.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_by_age
    '
    Me.chk_by_age.AutoSize = True
    Me.chk_by_age.Checked = True
    Me.chk_by_age.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_by_age.Location = New System.Drawing.Point(3, 3)
    Me.chk_by_age.Name = "chk_by_age"
    Me.chk_by_age.Size = New System.Drawing.Size(61, 17)
    Me.chk_by_age.TabIndex = 0
    Me.chk_by_age.Text = "xActive"
    Me.chk_by_age.UseVisualStyleBackColor = True
    '
    'ef_age_smaller_than
    '
    Me.ef_age_smaller_than.DoubleValue = 0.0R
    Me.ef_age_smaller_than.IntegerValue = 0
    Me.ef_age_smaller_than.IsReadOnly = False
    Me.ef_age_smaller_than.Location = New System.Drawing.Point(138, 44)
    Me.ef_age_smaller_than.Name = "ef_age_smaller_than"
    Me.ef_age_smaller_than.PlaceHolder = Nothing
    Me.ef_age_smaller_than.Size = New System.Drawing.Size(39, 24)
    Me.ef_age_smaller_than.SufixText = "Sufix Text"
    Me.ef_age_smaller_than.SufixTextVisible = True
    Me.ef_age_smaller_than.TabIndex = 5
    Me.ef_age_smaller_than.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_age_smaller_than.TextValue = ""
    Me.ef_age_smaller_than.TextVisible = False
    Me.ef_age_smaller_than.TextWidth = 1
    Me.ef_age_smaller_than.Value = ""
    Me.ef_age_smaller_than.ValueForeColor = System.Drawing.Color.Blue
    '
    'opt_birth_exclude_age
    '
    Me.opt_birth_exclude_age.Location = New System.Drawing.Point(23, 48)
    Me.opt_birth_exclude_age.Name = "opt_birth_exclude_age"
    Me.opt_birth_exclude_age.Size = New System.Drawing.Size(110, 17)
    Me.opt_birth_exclude_age.TabIndex = 2
    Me.opt_birth_exclude_age.Text = "xYounger than"
    '
    'opt_birth_include_age
    '
    Me.opt_birth_include_age.Location = New System.Drawing.Point(23, 26)
    Me.opt_birth_include_age.Name = "opt_birth_include_age"
    Me.opt_birth_include_age.Size = New System.Drawing.Size(110, 17)
    Me.opt_birth_include_age.TabIndex = 1
    Me.opt_birth_include_age.Text = "xOlder than"
    '
    'ef_age_or_older_than
    '
    Me.ef_age_or_older_than.DoubleValue = 0.0R
    Me.ef_age_or_older_than.IntegerValue = 0
    Me.ef_age_or_older_than.IsReadOnly = False
    Me.ef_age_or_older_than.Location = New System.Drawing.Point(183, 44)
    Me.ef_age_or_older_than.Name = "ef_age_or_older_than"
    Me.ef_age_or_older_than.PlaceHolder = Nothing
    Me.ef_age_or_older_than.Size = New System.Drawing.Size(153, 24)
    Me.ef_age_or_older_than.SufixText = "Sufix Text"
    Me.ef_age_or_older_than.SufixTextVisible = True
    Me.ef_age_or_older_than.TabIndex = 6
    Me.ef_age_or_older_than.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_age_or_older_than.TextValue = ""
    Me.ef_age_or_older_than.TextVisible = False
    Me.ef_age_or_older_than.TextWidth = 115
    Me.ef_age_or_older_than.Value = ""
    Me.ef_age_or_older_than.ValueForeColor = System.Drawing.Color.Blue
    '
    'opt_birth_date_only
    '
    Me.opt_birth_date_only.Location = New System.Drawing.Point(37, 61)
    Me.opt_birth_date_only.Name = "opt_birth_date_only"
    Me.opt_birth_date_only.Size = New System.Drawing.Size(344, 17)
    Me.opt_birth_date_only.TabIndex = 2
    Me.opt_birth_date_only.Text = "xBirth day and Month"
    '
    'chk_by_birth_date
    '
    Me.chk_by_birth_date.AutoSize = True
    Me.chk_by_birth_date.Checked = True
    Me.chk_by_birth_date.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_by_birth_date.Location = New System.Drawing.Point(17, 19)
    Me.chk_by_birth_date.Name = "chk_by_birth_date"
    Me.chk_by_birth_date.Size = New System.Drawing.Size(61, 17)
    Me.chk_by_birth_date.TabIndex = 0
    Me.chk_by_birth_date.Text = "xActive"
    Me.chk_by_birth_date.UseVisualStyleBackColor = True
    '
    'opt_birth_month_only
    '
    Me.opt_birth_month_only.Location = New System.Drawing.Point(37, 42)
    Me.opt_birth_month_only.Name = "opt_birth_month_only"
    Me.opt_birth_month_only.Size = New System.Drawing.Size(344, 17)
    Me.opt_birth_month_only.TabIndex = 1
    Me.opt_birth_month_only.Text = "xAny day within birthday's month"
    '
    'gb_by_gender
    '
    Me.gb_by_gender.Controls.Add(Me.opt_gender_male)
    Me.gb_by_gender.Controls.Add(Me.opt_gender_female)
    Me.gb_by_gender.Controls.Add(Me.chk_by_gender)
    Me.gb_by_gender.Location = New System.Drawing.Point(443, 221)
    Me.gb_by_gender.Name = "gb_by_gender"
    Me.gb_by_gender.Size = New System.Drawing.Size(280, 87)
    Me.gb_by_gender.TabIndex = 4
    Me.gb_by_gender.TabStop = False
    Me.gb_by_gender.Text = "xByGender"
    '
    'opt_gender_male
    '
    Me.opt_gender_male.Location = New System.Drawing.Point(28, 41)
    Me.opt_gender_male.Name = "opt_gender_male"
    Me.opt_gender_male.Size = New System.Drawing.Size(191, 20)
    Me.opt_gender_male.TabIndex = 1
    Me.opt_gender_male.Text = "xApplies to Men Only"
    '
    'opt_gender_female
    '
    Me.opt_gender_female.Location = New System.Drawing.Point(28, 58)
    Me.opt_gender_female.Name = "opt_gender_female"
    Me.opt_gender_female.Size = New System.Drawing.Size(191, 25)
    Me.opt_gender_female.TabIndex = 2
    Me.opt_gender_female.Text = "xApplies to Women Only"
    '
    'chk_by_gender
    '
    Me.chk_by_gender.AutoSize = True
    Me.chk_by_gender.Checked = True
    Me.chk_by_gender.CheckState = System.Windows.Forms.CheckState.Checked
    Me.chk_by_gender.Location = New System.Drawing.Point(8, 19)
    Me.chk_by_gender.Name = "chk_by_gender"
    Me.chk_by_gender.Size = New System.Drawing.Size(61, 17)
    Me.chk_by_gender.TabIndex = 0
    Me.chk_by_gender.Text = "xActive"
    Me.chk_by_gender.UseVisualStyleBackColor = True
    '
    'ef_min_ocupancy
    '
    Me.ef_min_ocupancy.DoubleValue = 0.0R
    Me.ef_min_ocupancy.IntegerValue = 0
    Me.ef_min_ocupancy.IsReadOnly = False
    Me.ef_min_ocupancy.Location = New System.Drawing.Point(28, 62)
    Me.ef_min_ocupancy.Name = "ef_min_ocupancy"
    Me.ef_min_ocupancy.PlaceHolder = Nothing
    Me.ef_min_ocupancy.Size = New System.Drawing.Size(226, 24)
    Me.ef_min_ocupancy.SufixText = "%"
    Me.ef_min_ocupancy.SufixTextVisible = True
    Me.ef_min_ocupancy.SufixTextWidth = 15
    Me.ef_min_ocupancy.TabIndex = 2
    Me.ef_min_ocupancy.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_min_ocupancy.TextValue = ""
    Me.ef_min_ocupancy.TextWidth = 125
    Me.ef_min_ocupancy.Value = ""
    Me.ef_min_ocupancy.ValueForeColor = System.Drawing.Color.Blue
    '
    'ContextMenuStrip1
    '
    Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
    Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
    '
    'ContextMenuStrip2
    '
    Me.ContextMenuStrip2.Name = "ContextMenuStrip2"
    Me.ContextMenuStrip2.Size = New System.Drawing.Size(61, 4)
    '
    'Uc_jackpot_time_award_config
    '
    Me.Uc_jackpot_time_award_config.Location = New System.Drawing.Point(4, 3)
    Me.Uc_jackpot_time_award_config.Name = "Uc_jackpot_time_award_config"
    Me.Uc_jackpot_time_award_config.Size = New System.Drawing.Size(433, 218)
    Me.Uc_jackpot_time_award_config.TabIndex = 0
    Me.Uc_jackpot_time_award_config.WeekDays = Nothing
    '
    'gb_misc
    '
    Me.gb_misc.Controls.Add(Me.ef_minimum_bet)
    Me.gb_misc.Controls.Add(Me.chk_exclude_account_with_promotion)
    Me.gb_misc.Controls.Add(Me.chk_exclude_anonymous_accounts)
    Me.gb_misc.Controls.Add(Me.ef_min_ocupancy)
    Me.gb_misc.Location = New System.Drawing.Point(443, 308)
    Me.gb_misc.Name = "gb_misc"
    Me.gb_misc.Size = New System.Drawing.Size(280, 117)
    Me.gb_misc.TabIndex = 5
    Me.gb_misc.TabStop = False
    Me.gb_misc.Text = "xgbmisc"
    '
    'ef_minimum_bet
    '
    Me.ef_minimum_bet.DoubleValue = 0.0R
    Me.ef_minimum_bet.IntegerValue = 0
    Me.ef_minimum_bet.IsReadOnly = False
    Me.ef_minimum_bet.Location = New System.Drawing.Point(28, 86)
    Me.ef_minimum_bet.Name = "ef_minimum_bet"
    Me.ef_minimum_bet.PlaceHolder = Nothing
    Me.ef_minimum_bet.Size = New System.Drawing.Size(226, 24)
    Me.ef_minimum_bet.SufixText = "Sufix Text"
    Me.ef_minimum_bet.SufixTextVisible = True
    Me.ef_minimum_bet.SufixTextWidth = 15
    Me.ef_minimum_bet.TabIndex = 3
    Me.ef_minimum_bet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_minimum_bet.TextValue = ""
    Me.ef_minimum_bet.TextWidth = 125
    Me.ef_minimum_bet.Value = ""
    Me.ef_minimum_bet.ValueForeColor = System.Drawing.Color.Blue
    '
    'uc_jackpot_flags
    '
    Me.uc_jackpot_flags.Flags = Nothing
    Me.uc_jackpot_flags.IsConfigured = False
    Me.uc_jackpot_flags.Location = New System.Drawing.Point(443, 427)
    Me.uc_jackpot_flags.Name = "uc_jackpot_flags"
    Me.uc_jackpot_flags.Size = New System.Drawing.Size(279, 157)
    Me.uc_jackpot_flags.TabIndex = 6
    '
    'uc_jackpot_EGM_terminal_award
    '
    Me.uc_jackpot_EGM_terminal_award.IsSelectedEGMType = False
    Me.uc_jackpot_EGM_terminal_award.Location = New System.Drawing.Point(443, 6)
    Me.uc_jackpot_EGM_terminal_award.Name = "uc_jackpot_EGM_terminal_award"
    Me.uc_jackpot_EGM_terminal_award.Size = New System.Drawing.Size(283, 212)
    Me.uc_jackpot_EGM_terminal_award.TabIndex = 3
    '
    'uc_jackpot_award_config
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
    Me.Name = "uc_jackpot_award_config"
    Me.Size = New System.Drawing.Size(839, 694)
    Me.Controls.SetChildIndex(Me.pnl_container, 0)
    Me.pnl_container.ResumeLayout(False)
    Me.gb_by_creation_date.ResumeLayout(False)
    Me.gb_by_creation_date.PerformLayout()
    Me.Panel3.ResumeLayout(False)
    Me.Panel2.ResumeLayout(False)
    Me.gb_date_of_birth.ResumeLayout(False)
    Me.gb_date_of_birth.PerformLayout()
    Me.Panel1.ResumeLayout(False)
    Me.Panel1.PerformLayout()
    Me.gb_by_gender.ResumeLayout(False)
    Me.gb_by_gender.PerformLayout()
    Me.gb_misc.ResumeLayout(False)
    Me.gb_misc.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Public WithEvents chk_exclude_anonymous_accounts As System.Windows.Forms.CheckBox
  Public WithEvents chk_exclude_account_with_promotion As System.Windows.Forms.CheckBox
  Friend WithEvents gb_by_creation_date As System.Windows.Forms.GroupBox
  Friend WithEvents Panel3 As System.Windows.Forms.Panel
  Friend WithEvents ef_created_account_anniversary_year_to As GUI_Controls.uc_entry_field
  Friend WithEvents chk_by_anniversary_range_of_years As System.Windows.Forms.CheckBox
  Friend WithEvents opt_created_account_anniversary_day_month As System.Windows.Forms.RadioButton
  Friend WithEvents opt_created_account_anniversary_whole_month As System.Windows.Forms.RadioButton
  Friend WithEvents ef_created_account_anniversary_year_from As GUI_Controls.uc_entry_field
  Friend WithEvents Panel2 As System.Windows.Forms.Panel
  Friend WithEvents opt_created_account_working_day_plus As System.Windows.Forms.RadioButton
  Friend WithEvents opt_created_account_working_day_only As System.Windows.Forms.RadioButton
  Friend WithEvents ef_created_account_working_day_plus As GUI_Controls.uc_entry_field
  Friend WithEvents opt_created_account_anniversary As System.Windows.Forms.RadioButton
  Friend WithEvents opt_created_account_date As System.Windows.Forms.RadioButton
  Friend WithEvents chk_by_created_account As System.Windows.Forms.CheckBox
  Friend WithEvents gb_date_of_birth As System.Windows.Forms.GroupBox
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
  Friend WithEvents ef_age_older_than As GUI_Controls.uc_entry_field
  Friend WithEvents ef_age_and_smaller_than As GUI_Controls.uc_entry_field
  Friend WithEvents chk_by_age As System.Windows.Forms.CheckBox
  Friend WithEvents ef_age_smaller_than As GUI_Controls.uc_entry_field
  Friend WithEvents opt_birth_exclude_age As System.Windows.Forms.RadioButton
  Friend WithEvents opt_birth_include_age As System.Windows.Forms.RadioButton
  Friend WithEvents ef_age_or_older_than As GUI_Controls.uc_entry_field
  Friend WithEvents opt_birth_date_only As System.Windows.Forms.RadioButton
  Friend WithEvents chk_by_birth_date As System.Windows.Forms.CheckBox
  Friend WithEvents opt_birth_month_only As System.Windows.Forms.RadioButton
  Friend WithEvents gb_by_gender As System.Windows.Forms.GroupBox
  Friend WithEvents opt_gender_male As System.Windows.Forms.RadioButton
  Friend WithEvents opt_gender_female As System.Windows.Forms.RadioButton
  Friend WithEvents chk_by_gender As System.Windows.Forms.CheckBox
  Friend WithEvents ef_min_ocupancy As GUI_Controls.uc_entry_field
  Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
  Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
  Friend WithEvents ContextMenuStrip2 As System.Windows.Forms.ContextMenuStrip
  Friend WithEvents BackgroundWorker2 As System.ComponentModel.BackgroundWorker
  Friend WithEvents BackgroundWorker3 As System.ComponentModel.BackgroundWorker
  Friend WithEvents Uc_jackpot_time_award_config As GUI_Controls.uc_jackpot_time_award
  Friend WithEvents gb_misc As System.Windows.Forms.GroupBox
  Public WithEvents uc_jackpot_flags As GUI_Controls.uc_jackpot_flags
  Friend WithEvents uc_jackpot_EGM_terminal_award As GUI_Controls.uc_jackpot_EGM_terminal_award
  Friend WithEvents ef_minimum_bet As GUI_Controls.uc_entry_field

End Class
