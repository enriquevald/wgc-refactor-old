﻿Imports System.Windows.Forms

'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' UC NAME:       uc_treeview_edit_base
' DESCRIPTION:   User control base for treeview item edit 
' AUTHOR:        Javi Barea
' CREATION DATE: 03-APR-2017
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 03-APR-2017  JBP    Initial version
'--------------------------------------------------------------------


Public Class uc_treeview_edit_base
  Inherits UserControl

#Region " Enum "

  Public Enum SHOW_MODE
    NewItem = 0
    EditItem = 1
    CloneItem = 2
  End Enum

#End Region

#Region " Members "

  Private m_parent_form As frm_treeview_edit

#End Region

#Region " Properties "

#End Region

#Region " Public Sub "


  Public Sub New()

    ' This call is required by the designer.
    InitializeComponent()

  End Sub

  Public Sub New(ByRef Form As frm_treeview_edit, ByVal ShowMode As SHOW_MODE, Optional ByVal DBObject As Object = Nothing)

    Me.m_parent_form = Form

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    'Select Case (ShowMode)
    '  Case SHOW_MODE.NewItem
    '    Me.m_parent_form.GUI_ShowNewItem()
    '  Case SHOW_MODE.EditItem
    '    If DBObject Is Nothing Then ' TODO JBP: REVISE. Está para controlar errores en desarrollo. Eliminar cuando esté solucionado.
    '      MessageBox.Show("JBP: Error (Test)")
    '    End If
    '    Me.m_parent_form.GUI_ShowEditItem(DBObject)
    '  Case SHOW_MODE.CloneItem

    'End Select

  End Sub

#End Region

End Class
