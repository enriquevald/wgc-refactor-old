'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_date_select.vb  
'
' DESCRIPTION:   
'
' AUTHOR:        Omar P�rez
'
' CREATION DATE: 29-JAN-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ ----------------------------------------------
' 29-JAN-2015  OPC    Initial version
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Threading
Imports System.Windows.Forms
Imports System.Globalization

Public Class uc_date_select

#Region " Enum "

  Private Enum FORMAT_TIME

    DAY_MONTH_YEAR = 1
    DAY_YEAR_MONTH = 2
    MONTH_DAY_YEAR = 3
    MONTH_YEAR_DAY = 4
    YEAR_DAY_MONTH = 5
    YEAR_MONTH_DAY = 6

  End Enum

#End Region

#Region " Members "

  Private DAY_NAME As String = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5878)                            ' Day
  Private MONTH_NAME As String = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5879)                          ' Month
  Private YEAR_NAME As String = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5880)                           ' Year
  Private DATE_SEPARATOR As String = Thread.CurrentThread.CurrentCulture.DateTimeFormat.DateSeparator ' Separator
  Private FORMAT_DATE_TIME As FORMAT_TIME                                                             ' Day / Month / Year

#End Region

#Region " Properties "

  Public Overloads Property Day() As String
    Get
      Return IIf(Me.ef_day.TextValue = DAY_NAME, String.Empty, Me.ef_day.TextValue)
    End Get
    Set(ByVal value As String)
      If Not String.IsNullOrEmpty(value) Then
        Me.ef_day.TextValue = value
      End If
    End Set
  End Property

  Public Overloads Property Month() As String
    Get
      Return IIf(Me.ef_month.TextValue = MONTH_NAME, String.Empty, Me.ef_month.TextValue)
    End Get
    Set(ByVal value As String)
      If Not String.IsNullOrEmpty(value) Then
        Me.ef_month.TextValue = value
      End If
    End Set
  End Property

  Public Overloads Property Year() As String
    Get
      Return IIf(Me.ef_year.TextValue = YEAR_NAME, String.Empty, Me.ef_year.TextValue)
    End Get
    Set(ByVal value As String)
      If Not String.IsNullOrEmpty(value) Then
        Me.ef_year.TextValue = value
      End If
    End Set
  End Property

  Public Overloads Property Name() As String
    Get
      Select Case (FORMAT_DATE_TIME)
        Case FORMAT_TIME.DAY_MONTH_YEAR, FORMAT_TIME.DAY_YEAR_MONTH
          Return Me.ef_day.Text
        Case FORMAT_TIME.MONTH_DAY_YEAR, FORMAT_TIME.MONTH_YEAR_DAY
          Return Me.ef_month.Text
        Case FORMAT_TIME.YEAR_DAY_MONTH, FORMAT_TIME.YEAR_MONTH_DAY
          Return Me.ef_year.Text
        Case Else
          Return Me.ef_day.Text
      End Select
    End Get
    Set(ByVal value As String)
      Select Case (FORMAT_DATE_TIME)
        Case FORMAT_TIME.DAY_MONTH_YEAR, FORMAT_TIME.DAY_YEAR_MONTH
          Me.ef_day.Text = value
        Case FORMAT_TIME.MONTH_DAY_YEAR, FORMAT_TIME.MONTH_YEAR_DAY
          Me.ef_month.Text = value
        Case FORMAT_TIME.YEAR_DAY_MONTH, FORMAT_TIME.YEAR_MONTH_DAY
          Me.ef_year.Text = value
        Case Else
          Me.ef_day.Text = value
      End Select
    End Set
  End Property

#End Region

#Region " Public Methods "

  Public Sub New()
    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    Call SetInitialValues()

  End Sub ' New

  Public Function IsChecked() As Boolean
    Dim _status As Boolean

    _status = False

    If Not String.IsNullOrEmpty(ef_day.TextValue) AndAlso IsNumeric(ef_day.TextValue) Or _
       Not String.IsNullOrEmpty(ef_month.TextValue) AndAlso IsNumeric(ef_month.TextValue) Or _
       Not String.IsNullOrEmpty(ef_year.TextValue) AndAlso IsNumeric(ef_year.TextValue) Then
      _status = True
    End If

    Return _status
  End Function ' IsChecked

  Public Function IsValidDate() As Boolean
    Dim _day As Integer
    Dim _month As Integer
    Dim _year As Integer
    Dim _date As DateTime

    If IsNumeric(Day) AndAlso Not IsNumeric(Month) AndAlso Not IsNumeric(Year) Then

      Return True
    End If

    If IsNumeric(Month) AndAlso Not IsNumeric(Day) AndAlso Not IsNumeric(Year) Then

      Return True
    End If

    If IsNumeric(Year) AndAlso Not IsNumeric(Month) AndAlso Not IsNumeric(Day) AndAlso Year.Length = 4 Then

      Return True
    End If

    If IsNumeric(Day) AndAlso Day = 29 Then
      If IsNumeric(Month) AndAlso Month = 2 Then
        If (IsNumeric(Year) AndAlso DateTime.IsLeapYear(Year)) Then

          Return True
        Else
          If String.IsNullOrEmpty(Year) Then

            Return True
          End If

          Return False
        End If
      Else

        Return True
      End If
    End If

    If Not String.IsNullOrEmpty(Day) AndAlso IsNumeric(Day) Then
      _day = CInt(Day)
    Else
      _day = 1
    End If

    If Not String.IsNullOrEmpty(Month) AndAlso IsNumeric(Month) Then
      _month = CInt(Month)
    Else
      _month = 1
    End If

    If Not String.IsNullOrEmpty(Year) AndAlso IsNumeric(Year) Then
      If Year.Length = 4 Then
        _year = CInt(Year)
      Else
        Return False
      End If
    Else
      _year = Now.Year
    End If

    Try
      _date = New DateTime(_year, _month, _day)

      Return True
    Catch ex As System.ArgumentOutOfRangeException

      Return False
    End Try

  End Function ' IsValidDate

#End Region

#Region " Private Methods"

  Private Sub SetInitialValues()
    With ef_day
      .PlaceHolder = DAY_NAME
      .SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_DATE_DAY)
    End With

    With ef_month
      .Text = DATE_SEPARATOR
      .PlaceHolder = MONTH_NAME
      .SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_DATE_MONTH)
    End With

    With ef_year
      .Text = DATE_SEPARATOR
      .PlaceHolder = YEAR_NAME
      .SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_DATE_YEAR)
    End With

    For Each _obj As Object In ef_month.Controls
      If TypeOf (_obj) Is Label Then
        DirectCast(_obj, Label).TextAlign = Drawing.ContentAlignment.MiddleCenter
        DirectCast(_obj, Label).Padding = New Padding(15, 0, 0, 0)
      End If
    Next

    For Each _obj As Object In ef_year.Controls
      If TypeOf (_obj) Is Label Then
        DirectCast(_obj, Label).TextAlign = Drawing.ContentAlignment.MiddleCenter
        DirectCast(_obj, Label).Padding = New Padding(15, 0, 0, 0)
      End If
    Next

    SetDateCulture()
  End Sub ' SetInitialValues

  Private Sub ChangePadding(ByVal EntryField As uc_entry_field, ByVal Add As Boolean)
    For Each _obj As Object In EntryField.Controls
      If TypeOf (_obj) Is Label Then
        If Add Then
          DirectCast(_obj, Label).Padding = New Padding(15, 0, 0, 0)
          DirectCast(_obj, Label).TextAlign = Drawing.ContentAlignment.MiddleCenter
        Else
          DirectCast(_obj, Label).Padding = New Padding(0, 0, 0, 0)
          DirectCast(_obj, Label).TextAlign = Drawing.ContentAlignment.MiddleRight
        End If
      End If
    Next
  End Sub ' ChangePadding

  Private Sub SetDateCulture()
    Dim _format_datetime As String
    Dim _days As String()
    Dim _cu As CultureInfo

    _cu = Thread.CurrentThread.CurrentCulture
    _cu.ClearCachedData()
    _format_datetime = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern
    _days = _format_datetime.ToUpper.Split(DATE_SEPARATOR)

    If _days(0).Contains("D") AndAlso _days(1).Contains("M") AndAlso _days(2).Contains("Y") Then
      FORMAT_DATE_TIME = FORMAT_TIME.DAY_MONTH_YEAR
    ElseIf _days(0).Contains("D") AndAlso _days(1).Contains("Y") AndAlso _days(2).Contains("M") Then
      FORMAT_DATE_TIME = FORMAT_TIME.DAY_YEAR_MONTH
      ReorderDates(Me.ef_month, Me.ef_year)
      BringTo(Me.ef_day, Me.ef_year, Me.ef_month)
    ElseIf _days(0).Contains("M") AndAlso _days(1).Contains("D") AndAlso _days(2).Contains("Y") Then
      FORMAT_DATE_TIME = FORMAT_TIME.MONTH_DAY_YEAR
      ReorderDates(Me.ef_day, Me.ef_month)
      ChangePadding(Me.ef_month, False)
      ChangePadding(Me.ef_day, True)
      BringTo(Me.ef_month, Me.ef_day, Me.ef_year)
    ElseIf _days(0).Contains("M") AndAlso _days(1).Contains("Y") AndAlso _days(2).Contains("D") Then
      FORMAT_DATE_TIME = FORMAT_TIME.MONTH_YEAR_DAY
      ReorderDates(Me.ef_day, Me.ef_month)
      ReorderDates(Me.ef_day, Me.ef_year)
      ChangePadding(Me.ef_day, False)
      ChangePadding(Me.ef_month, True)
      ChangePadding(Me.ef_year, False)
      BringTo(Me.ef_month, Me.ef_year, Me.ef_day)
    ElseIf _days(0).Contains("Y") AndAlso _days(1).Contains("D") AndAlso _days(2).Contains("M") Then
      FORMAT_DATE_TIME = FORMAT_TIME.YEAR_DAY_MONTH
      ReorderDates(Me.ef_day, Me.ef_year)
      ReorderDates(Me.ef_month, Me.ef_day)
      ChangePadding(Me.ef_day, False)
      ChangePadding(Me.ef_year, True)
      ChangePadding(Me.ef_month, False)
      BringTo(Me.ef_year, Me.ef_day, Me.ef_month)
    ElseIf _days(0).Contains("Y") AndAlso _days(1).Contains("M") AndAlso _days(2).Contains("D") Then
      FORMAT_DATE_TIME = FORMAT_TIME.YEAR_MONTH_DAY
      ReorderDates(Me.ef_day, Me.ef_year)
      ChangePadding(Me.ef_day, False)
      ChangePadding(Me.ef_year, True)
      BringTo(Me.ef_year, Me.ef_month, Me.ef_day)
    Else
      FORMAT_DATE_TIME = FORMAT_TIME.DAY_MONTH_YEAR
    End If
  End Sub ' SetDateCulture

  Private Sub BringTo(ByVal First As uc_entry_field, ByVal Second As uc_entry_field, ByVal Third As uc_entry_field)
    Third.BringToFront()
    Second.BringToFront()
    First.BringToFront()
  End Sub ' BringTo

  Private Sub ReorderDates(ByVal Origin As uc_entry_field, ByVal Destiny As uc_entry_field)
    Dim _text_width As Integer
    Dim _location As System.Drawing.Point
    Dim _size As System.Drawing.Size
    Dim _text As String
    Dim _tab_index As Integer

    _text_width = Origin.TextWidth
    _location = Origin.Location
    _size = Origin.Size
    _text = Origin.Text
    _tab_index = Origin.TabIndex

    Origin.TextWidth = Destiny.TextWidth
    Origin.Location = Destiny.Location
    Origin.Size = Destiny.Size
    Origin.Text = Destiny.Text
    Origin.TabIndex = Destiny.TabIndex

    Destiny.Location = _location
    Destiny.TextWidth = _text_width
    Destiny.Size = _size
    Destiny.Text = _text
    Destiny.TabIndex = _tab_index
  End Sub ' MoveDayToMonth

#End Region

#Region " Events "

  Private Sub ef_Enter_Generic(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ef_day.Enter, ef_month.Enter, ef_year.Enter
    Dim _ef As uc_entry_field

    _ef = DirectCast(sender, uc_entry_field)

    If Not _ef Is Nothing Then
      Select Case (_ef.TextValue)
        Case DAY_NAME, MONTH_NAME
          _ef.PlaceHolder = String.Empty
        Case YEAR_NAME
          _ef.PlaceHolder = String.Empty
      End Select
    End If

  End Sub ' ef_Enter_Generic

  Private Sub ef_Leave_Generic(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ef_day.Leave, ef_month.Leave, ef_year.Leave
    Dim _ef As uc_entry_field

    _ef = DirectCast(sender, uc_entry_field)

    If Not _ef Is Nothing Then
      If String.IsNullOrEmpty(_ef.TextValue) Or Not IsNumeric(_ef.TextValue) Then
        Select Case (_ef.Name)
          Case "ef_day"
            _ef.PlaceHolder = DAY_NAME
          Case "ef_month"
            _ef.PlaceHolder = MONTH_NAME
          Case "ef_year"
            _ef.PlaceHolder = YEAR_NAME
        End Select
      Else
        Select Case (_ef.Name)
          Case "ef_day"
            If CInt(_ef.TextValue) = 0 Then
              _ef.PlaceHolder = DAY_NAME
            End If
          Case "ef_month"
            If CInt(_ef.TextValue) = 0 Then
              _ef.PlaceHolder = MONTH_NAME
            End If
          Case "ef_year"
            ''If (CInt(_ef.TextValue) <= 1900) Then
            ''  _ef.PlaceHolder = YEAR_NAME
            ''End If
        End Select
        
      End If
    End If
  End Sub ' ef_Leave_Generic

#End Region

End Class
