'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_sw_products.vb  
'
' DESCRIPTION:   software products picker control
'
' AUTHOR:        Miquel Beltran
'
' CREATION DATE: 16-MAR-2011
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-MAR-2011  MBF    Initial version
' 06-SEP-2012  DDM    Added terminal type PromoBOX 
' 28-AUG-2013  JCA    Added CashDeskDraws GUI
' 11-MAR-2016  JCA    Added "SmartFloor" in package name.
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common

Public Class uc_sw_products

#Region "Constants"

  Private Const GRID_2_COLUMN_ID As Integer = 0
  Private Const GRID_2_COLUMN_CHECKED As Integer = 1
  Private Const GRID_2_COLUMN_DESC As Integer = 2
  'Private Const GRID_2_COLUMN_TYPE As Integer = 3
  'Private Const GRID_2_COLUMN_CHK As Integer = 4

  Private Const GRID_2_COLUMNS As Integer = 3
  Private Const GRID_2_HEADER_ROWS As Integer = 0

  Private Const GRID_2_ROW_TYPE_HEADER = 0
  Private Const GRID_2_ROW_TYPE_DATA = 1

  Private Const GRID_2_TAB As String = "     "

#End Region ' Constants

#Region " Public Functions "

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

  End Sub

  Public Sub Init()
    Call InitControls()
  End Sub

  ' PURPOSE: Get selected softwre products list
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Public Function GetSwProductsSelected() As String

    Dim _sql_string As String
    Dim _idx_row As Integer
    Dim _list_sw_types As String

    _sql_string = ""
    _list_sw_types = ""

    For _idx_row = 0 To Me.dg_filter.NumRows - 1
      If dg_filter.Cell(_idx_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        If _list_sw_types.Length = 0 Then
          _list_sw_types = dg_filter.Cell(_idx_row, GRID_2_COLUMN_ID).Value
        Else
          _list_sw_types = _list_sw_types & ", " & dg_filter.Cell(_idx_row, GRID_2_COLUMN_ID).Value
        End If
      End If
    Next

    If _list_sw_types.Length = 0 Then
      Return ""
    End If

    _sql_string = _sql_string & " AND (TSV_TERMINAL_TYPE IN (" & _list_sw_types & "))"

    Return _sql_string

  End Function ' GetSwProductsSelected

  Public Function GetSwProductsSelectedNames() As String

    Dim _sql_string As String
    Dim _idx_row As Integer
    Dim _list_sw_types As String

    _sql_string = ""
    _list_sw_types = ""

    If opt_all_types.Checked Then
      Return opt_all_types.Text
    End If

    For _idx_row = 0 To Me.dg_filter.NumRows - 1
      If dg_filter.Cell(_idx_row, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        If _list_sw_types.Length = 0 Then
          _list_sw_types = dg_filter.Cell(_idx_row, GRID_2_COLUMN_DESC).Value
        Else
          _list_sw_types = _list_sw_types & ", " & dg_filter.Cell(_idx_row, GRID_2_COLUMN_DESC).Value
        End If
      End If
    Next

    Return _list_sw_types

  End Function ' GetSwProductsSelectedNames


  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub SetDefaultValues()
    Dim _idx As Integer

    Me.dg_filter.Redraw = False

    For _idx = 0 To dg_filter.NumRows - 1
      Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next

    Me.opt_all_types.Checked = True


    Me.dg_filter.Redraw = True

  End Sub 'SetDefaultValues

#End Region ' Public Functions

#Region " Private Functions "

  Private Sub InitControls()

    Me.gb_sw.Text = GLB_NLS_GUI_STATISTICS.GetString(470)
    Me.opt_several_types.Text = GLB_NLS_GUI_STATISTICS.GetString(217)
    Me.opt_all_types.Text = GLB_NLS_GUI_STATISTICS.GetString(218)

    GUI_StyleSheetListSwProducts()

    GetSoftwareProductsList()
    SetDefaultValues()

  End Sub

  Private Sub GetSoftwareProductsList()
    'JCA CashDeskDraws
    If GLB_GuiMode = ENUM_GUI.CASHDESK_DRAWS_GUI Then
      Call AddCashDeskDrawsSwProducts()
    Else
      If WSI.Common.GeneralParam.GetBoolean("MultiSite", "IsCenter", False) Then
        Call AddMultiSiteSwProducts()
      Else
        Call AddSiteSwProducts()
      End If
    End If
  End Sub

  Private Sub AddCashDeskDrawsSwProducts()
    'JCA CashDeskDraws
    Dim _idx As Integer

    _idx = 0

    '' Terminal Type: WIN CASHDESK DRAWS GUI
    dg_filter.AddRow()
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_ID).Value = ENUM_TSV_PACKET.TSV_PACKET_WIN_CASHDESK_DRAWS_GUI
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_DESC).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(ENUM_TSV_PACKET.TSV_PACKET_WIN_CASHDESK_DRAWS_GUI)
    ' Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = False
    _idx = _idx + 1

    'TODO: Add Service when it created
  End Sub
  Private Sub AddMultiSiteSwProducts()

    Dim _idx As Integer

    _idx = 0

    '' Terminal Type: WIN MultiSite GUI
    dg_filter.AddRow()
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_ID).Value = ENUM_TSV_PACKET.TSV_PACKET_WIN_MULTISITE_GUI
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_DESC).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(ENUM_TSV_PACKET.TSV_PACKET_WIN_MULTISITE_GUI)
    ' Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = False
    _idx = _idx + 1

    '' Terminal Type: WWP Center Service
    dg_filter.AddRow()
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_ID).Value = ENUM_TSV_PACKET.TSV_PACKET_WWP_CENTER_SERVICE
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_DESC).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(ENUM_TSV_PACKET.TSV_PACKET_WWP_CENTER_SERVICE)
    ' Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = False
    _idx = _idx + 1

  End Sub

  Private Sub AddSiteSwProducts()

    Dim _idx As Integer

    _idx = 0

    ' Terminal Type: WIN Kiosk
    dg_filter.AddRow()
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_ID).Value = ENUM_TSV_PACKET.TSV_PACKET_WIN_KIOSK
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_DESC).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(ENUM_TSV_PACKET.TSV_PACKET_WIN_KIOSK)
    ' Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = False
    _idx = _idx + 1

    '' Terminal Type: WIN Cashier
    dg_filter.AddRow()
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_ID).Value = ENUM_TSV_PACKET.TSV_PACKET_WIN_CASHIER
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_DESC).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(ENUM_TSV_PACKET.TSV_PACKET_WIN_CASHIER)
    ' Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = False
    _idx = _idx + 1

    '' Terminal Type: WIN GUI
    dg_filter.AddRow()
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_ID).Value = ENUM_TSV_PACKET.TSV_PACKET_WIN_GUI
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_DESC).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(ENUM_TSV_PACKET.TSV_PACKET_WIN_GUI)
    ' Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = False
    _idx = _idx + 1

    '' Terminal Type: WCP Service
    dg_filter.AddRow()
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_ID).Value = ENUM_TSV_PACKET.TSV_PACKET_WCP_SERVICE
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_DESC).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(ENUM_TSV_PACKET.TSV_PACKET_WCP_SERVICE)
    ' Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = False
    _idx = _idx + 1

    '' Terminal Type: WC2 Service
    dg_filter.AddRow()
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_ID).Value = ENUM_TSV_PACKET.TSV_PACKET_WC2_SERVICE
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_DESC).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(ENUM_TSV_PACKET.TSV_PACKET_WC2_SERVICE)
    ' Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = False
    _idx = _idx + 1

    '' Terminal Type: WXP Service
    dg_filter.AddRow()
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_ID).Value = ENUM_TSV_PACKET.TSV_PACKET_WXP_SERVICE
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_DESC).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(ENUM_TSV_PACKET.TSV_PACKET_WXP_SERVICE)
    ' Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = False
    _idx = _idx + 1

    '' Terminal Type: WWP Site Service
    dg_filter.AddRow()
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_ID).Value = ENUM_TSV_PACKET.TSV_PACKET_WWP_SITE_SERVICE
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_DESC).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(ENUM_TSV_PACKET.TSV_PACKET_WWP_SITE_SERVICE)
    ' Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = False
    _idx = _idx + 1

    '' Terminal Type: PSA_Client
    dg_filter.AddRow()
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_ID).Value = ENUM_TSV_PACKET.TSV_PACKET_PSA_CLIENT
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_DESC).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(ENUM_TSV_PACKET.TSV_PACKET_PSA_CLIENT)
    ' Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = False
    _idx = _idx + 1

    '' Terminal Type: AFIP_Client
    dg_filter.AddRow()
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_ID).Value = ENUM_TSV_PACKET.TSV_PACKET_AFIP_CLIENT
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_DESC).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(ENUM_TSV_PACKET.TSV_PACKET_AFIP_CLIENT)
    ' Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = False
    _idx = _idx + 1

    '' Terminal Type: WSI.Protocols
    dg_filter.AddRow()
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_ID).Value = ENUM_TSV_PACKET.TSV_PACKET_PROTOCOLS_SERVICE
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_DESC).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(ENUM_TSV_PACKET.TSV_PACKET_PROTOCOLS_SERVICE)
    ' Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = False
    _idx = _idx + 1

    '' Terminal Type: SmartFloor
    dg_filter.AddRow()
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_ID).Value = ENUM_TSV_PACKET.TSV_PACKET_SMARTFLOOR_DATA_SERVICE
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_DESC).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(ENUM_TSV_PACKET.TSV_PACKET_SMARTFLOOR_DATA_SERVICE)
    ' Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = False
    _idx = _idx + 1

    '' Terminal Type: SAS Host
    dg_filter.AddRow()
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_ID).Value = ENUM_TSV_PACKET.TSV_PACKET_SAS_HOST
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_DESC).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(ENUM_TSV_PACKET.TSV_PACKET_SAS_HOST)
    ' Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = False
    _idx = _idx + 1

    '' Terminal Type: Site Jackpot
    dg_filter.AddRow()
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_ID).Value = ENUM_TSV_PACKET.TSV_PACKET_SITE_JACKPOT
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_DESC).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(ENUM_TSV_PACKET.TSV_PACKET_SITE_JACKPOT)
    ' Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = False
    _idx = _idx + 1

    '' Terminal Type: iMB
    dg_filter.AddRow()
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_ID).Value = ENUM_TSV_PACKET.TSV_PACKET_MOBILE_BANK_IMB
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_DESC).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(ENUM_TSV_PACKET.TSV_PACKET_MOBILE_BANK_IMB)
    ' Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = False
    _idx = _idx + 1

    '' Terminal Type: iStats
    dg_filter.AddRow()
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_ID).Value = ENUM_TSV_PACKET.TSV_PACKET_ISTATS
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_DESC).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(ENUM_TSV_PACKET.TSV_PACKET_ISTATS)
    ' Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = False
    _idx = _idx + 1

    '' Terminal Type: PromoBOX
    dg_filter.AddRow()
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_ID).Value = ENUM_TSV_PACKET.TSV_PACKET_PROMOBOX
    Me.dg_filter.Cell(_idx, GRID_2_COLUMN_DESC).Value = CLASS_SW_PACKAGE.GetTerminalTypeName(ENUM_TSV_PACKET.TSV_PACKET_PROMOBOX)
    ' Me.dg_filter.Cell(_idx, GRID_2_COLUMN_CHECKED).Value = False
    _idx = _idx + 1


  End Sub

  Private Sub GUI_StyleSheetListSwProducts()
    With Me.dg_filter
      Call .Init(GRID_2_COLUMNS, GRID_2_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden colums
      .Column(GRID_2_COLUMN_ID).Header.Text = ""
      .Column(GRID_2_COLUMN_ID).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_2_COLUMN_CHECKED).Header.Text = ""
      .Column(GRID_2_COLUMN_CHECKED).WidthFixed = 300
      .Column(GRID_2_COLUMN_CHECKED).Fixed = True
      .Column(GRID_2_COLUMN_CHECKED).Editable = True
      .Column(GRID_2_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_2_COLUMN_DESC).Header.Text = ""
      .Column(GRID_2_COLUMN_DESC).WidthFixed = 3397
      .Column(GRID_2_COLUMN_DESC).Fixed = True
      .Column(GRID_2_COLUMN_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      '' GRID_COL_TYPE
      '.Column(GRID_2_COLUMN_TYPE).Header.Text = ""
      '.Column(GRID_2_COLUMN_TYPE).WidthFixed = 0

      '' GRID_COL_CHK
      '.Column(GRID_2_COLUMN_CHK).Header.Text = ""
      '.Column(GRID_2_COLUMN_CHK).WidthFixed = 0

    End With

  End Sub ' GUI_StyleSheetListProviders

#End Region ' Private Functions

  Private Sub opt_several_types_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_several_types.CheckedChanged

    If opt_several_types.Checked Then
      Me.opt_all_types.Checked = False
    End If

  End Sub

  Private Sub opt_all_types_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_all_types.CheckedChanged
    Dim idx As Integer

    If opt_all_types.Checked Then
      Me.dg_filter.Redraw = False

      For idx = 0 To dg_filter.NumRows - 1
        Me.dg_filter.Cell(idx, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      Next

      Me.dg_filter.Redraw = True

      opt_several_types.Checked = False
    End If

  End Sub

  Private Sub dg_filter_DataSelectedEvent() Handles dg_filter.DataSelectedEvent

    If Me.opt_several_types.Checked = False Then
      Me.opt_several_types.Checked = True
      'Exit Sub
    End If

  End Sub ' dg_filter_DataSelectedEvent

  Private Sub dg_filter_CellDataChangedEvent(ByVal Row As Integer, ByVal Column As Integer) Handles dg_filter.CellDataChangedEvent
    ' Update radio buttons accordingly
    If Me.opt_several_types.Checked = False Then
      Me.opt_several_types.Checked = True
    End If

  End Sub ' dg_types_CellDataChangedEvent

End Class
