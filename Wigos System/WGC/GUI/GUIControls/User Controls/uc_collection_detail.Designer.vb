<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_collection_detail
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.tb_detail = New System.Windows.Forms.TabControl
    Me.tp_total = New System.Windows.Forms.TabPage
    Me.uc_grid_collection_total = New GUI_Controls.uc_grid
    Me.tp_tickets = New System.Windows.Forms.TabPage
    Me.gb_status = New System.Windows.Forms.GroupBox
    Me.lbl_pending = New System.Windows.Forms.Label
    Me.lbl_error = New System.Windows.Forms.Label
    Me.lbl_ok = New System.Windows.Forms.Label
    Me.lbl_pending_color = New System.Windows.Forms.Label
    Me.lbl_error_color = New System.Windows.Forms.Label
    Me.lbl_ok_color = New System.Windows.Forms.Label
    Me.uc_grid_collection_tickets = New GUI_Controls.uc_grid
    Me.tb_detail.SuspendLayout()
    Me.tp_total.SuspendLayout()
    Me.tp_tickets.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'tb_detail
    '
    Me.tb_detail.Controls.Add(Me.tp_total)
    Me.tb_detail.Controls.Add(Me.tp_tickets)
    Me.tb_detail.Location = New System.Drawing.Point(3, 3)
    Me.tb_detail.Name = "tb_detail"
    Me.tb_detail.SelectedIndex = 0
    Me.tb_detail.Size = New System.Drawing.Size(660, 258)
    Me.tb_detail.TabIndex = 0
    '
    'tp_total
    '
    Me.tp_total.Controls.Add(Me.uc_grid_collection_total)
    Me.tp_total.Location = New System.Drawing.Point(4, 22)
    Me.tp_total.Name = "tp_total"
    Me.tp_total.Padding = New System.Windows.Forms.Padding(3)
    Me.tp_total.Size = New System.Drawing.Size(652, 232)
    Me.tp_total.TabIndex = 0
    Me.tp_total.Text = "xTotal"
    Me.tp_total.UseVisualStyleBackColor = True
    '
    'uc_grid_collection_total
    '
    Me.uc_grid_collection_total.CurrentCol = -1
    Me.uc_grid_collection_total.CurrentRow = -1
    Me.uc_grid_collection_total.EditableCellBackColor = System.Drawing.Color.Empty
    Me.uc_grid_collection_total.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.uc_grid_collection_total.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.uc_grid_collection_total.Location = New System.Drawing.Point(6, 6)
    Me.uc_grid_collection_total.Name = "uc_grid_collection_total"
    Me.uc_grid_collection_total.PanelRightVisible = False
    Me.uc_grid_collection_total.Redraw = True
    Me.uc_grid_collection_total.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.uc_grid_collection_total.Size = New System.Drawing.Size(643, 223)
    Me.uc_grid_collection_total.Sortable = False
    Me.uc_grid_collection_total.SortAscending = True
    Me.uc_grid_collection_total.SortByCol = 0
    Me.uc_grid_collection_total.TabIndex = 4
    Me.uc_grid_collection_total.ToolTipped = True
    Me.uc_grid_collection_total.TopRow = -2
    '
    'tp_tickets
    '
    Me.tp_tickets.Controls.Add(Me.gb_status)
    Me.tp_tickets.Controls.Add(Me.uc_grid_collection_tickets)
    Me.tp_tickets.Location = New System.Drawing.Point(4, 22)
    Me.tp_tickets.Name = "tp_tickets"
    Me.tp_tickets.Padding = New System.Windows.Forms.Padding(3)
    Me.tp_tickets.Size = New System.Drawing.Size(652, 232)
    Me.tp_tickets.TabIndex = 1
    Me.tp_tickets.Text = "xTickets"
    Me.tp_tickets.UseVisualStyleBackColor = True
    '
    'gb_status
    '
    Me.gb_status.BackColor = System.Drawing.SystemColors.Control
    Me.gb_status.Controls.Add(Me.lbl_pending)
    Me.gb_status.Controls.Add(Me.lbl_error)
    Me.gb_status.Controls.Add(Me.lbl_ok)
    Me.gb_status.Controls.Add(Me.lbl_pending_color)
    Me.gb_status.Controls.Add(Me.lbl_error_color)
    Me.gb_status.Controls.Add(Me.lbl_ok_color)
    Me.gb_status.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.gb_status.Location = New System.Drawing.Point(495, 6)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(123, 98)
    Me.gb_status.TabIndex = 112
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'lbl_pending
    '
    Me.lbl_pending.AutoSize = True
    Me.lbl_pending.Location = New System.Drawing.Point(32, 69)
    Me.lbl_pending.Name = "lbl_pending"
    Me.lbl_pending.Size = New System.Drawing.Size(59, 13)
    Me.lbl_pending.TabIndex = 113
    Me.lbl_pending.Text = "xPending"
    '
    'lbl_error
    '
    Me.lbl_error.AutoSize = True
    Me.lbl_error.Location = New System.Drawing.Point(32, 44)
    Me.lbl_error.Name = "lbl_error"
    Me.lbl_error.Size = New System.Drawing.Size(43, 13)
    Me.lbl_error.TabIndex = 113
    Me.lbl_error.Text = "xError"
    '
    'lbl_ok
    '
    Me.lbl_ok.AutoSize = True
    Me.lbl_ok.Location = New System.Drawing.Point(32, 19)
    Me.lbl_ok.Name = "lbl_ok"
    Me.lbl_ok.Size = New System.Drawing.Size(30, 13)
    Me.lbl_ok.TabIndex = 113
    Me.lbl_ok.Text = "xOk"
    '
    'lbl_pending_color
    '
    Me.lbl_pending_color.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(94, Byte), Integer))
    Me.lbl_pending_color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_pending_color.Location = New System.Drawing.Point(10, 69)
    Me.lbl_pending_color.Name = "lbl_pending_color"
    Me.lbl_pending_color.Size = New System.Drawing.Size(16, 16)
    Me.lbl_pending_color.TabIndex = 112
    '
    'lbl_error_color
    '
    Me.lbl_error_color.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
    Me.lbl_error_color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_error_color.Location = New System.Drawing.Point(10, 44)
    Me.lbl_error_color.Name = "lbl_error_color"
    Me.lbl_error_color.Size = New System.Drawing.Size(16, 16)
    Me.lbl_error_color.TabIndex = 110
    '
    'lbl_ok_color
    '
    Me.lbl_ok_color.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_ok_color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_ok_color.Location = New System.Drawing.Point(10, 19)
    Me.lbl_ok_color.Name = "lbl_ok_color"
    Me.lbl_ok_color.Size = New System.Drawing.Size(16, 16)
    Me.lbl_ok_color.TabIndex = 106
    '
    'uc_grid_collection_tickets
    '
    Me.uc_grid_collection_tickets.CurrentCol = -1
    Me.uc_grid_collection_tickets.CurrentRow = -1
    Me.uc_grid_collection_tickets.EditableCellBackColor = System.Drawing.Color.Empty
    Me.uc_grid_collection_tickets.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.uc_grid_collection_tickets.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.uc_grid_collection_tickets.Location = New System.Drawing.Point(6, 6)
    Me.uc_grid_collection_tickets.Name = "uc_grid_collection_tickets"
    Me.uc_grid_collection_tickets.PanelRightVisible = False
    Me.uc_grid_collection_tickets.Redraw = True
    Me.uc_grid_collection_tickets.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.uc_grid_collection_tickets.Size = New System.Drawing.Size(451, 223)
    Me.uc_grid_collection_tickets.Sortable = False
    Me.uc_grid_collection_tickets.SortAscending = True
    Me.uc_grid_collection_tickets.SortByCol = 0
    Me.uc_grid_collection_tickets.TabIndex = 5
    Me.uc_grid_collection_tickets.ToolTipped = True
    Me.uc_grid_collection_tickets.TopRow = -2
    '
    'uc_collection_detail
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.tb_detail)
    Me.Name = "uc_collection_detail"
    Me.Size = New System.Drawing.Size(666, 263)
    Me.tb_detail.ResumeLayout(False)
    Me.tp_total.ResumeLayout(False)
    Me.tp_tickets.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents tb_detail As System.Windows.Forms.TabControl
  Friend WithEvents tp_total As System.Windows.Forms.TabPage
  Friend WithEvents tp_tickets As System.Windows.Forms.TabPage
  Friend WithEvents uc_grid_collection_total As GUI_Controls.uc_grid
  Friend WithEvents uc_grid_collection_tickets As GUI_Controls.uc_grid
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_pending_color As System.Windows.Forms.Label
  Friend WithEvents lbl_error_color As System.Windows.Forms.Label
  Friend WithEvents lbl_ok_color As System.Windows.Forms.Label
  Friend WithEvents lbl_pending As System.Windows.Forms.Label
  Friend WithEvents lbl_error As System.Windows.Forms.Label
  Friend WithEvents lbl_ok As System.Windows.Forms.Label
  'Friend WithEvents tf_ok1 As System.Windows.Forms.Label 'GUI_Controls.uc_text_field
  'Friend WithEvents tf_error1 As System.Windows.Forms.Label 'GUI_Controls.uc_text_field
  'Friend WithEvents tf_pending As System.Windows.Forms.Label 'GUI_Controls.uc_text_field

End Class
