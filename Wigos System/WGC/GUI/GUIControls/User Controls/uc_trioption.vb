#Region " Copyright "
'------------------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'------------------------------------------------------------------------------
' 
'   MODULE NAME: uc_trioption.vb
'   DESCRIPTION: User control 
'        AUTHOR: Andreu Juli� Quiles
' CREATION DATE: 15-NOV-2002
' 
' REVISION HISTORY:
' 
' Date        Author Description
' ----------- ------ -----------------------------------------------------------
' 15-NOV-2002 AJQ    Initial draft.
'-------------------------------------------------------------------------------
#End Region

'XVV -> Add NameSpace solve error with variables definitions
Imports System.Windows.Forms

Public Class uc_trioption
  Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

  Private Const OPTION_HEIGHT As Integer = 16
  Private m_horizontal As Boolean = True
  Private m_init As Boolean = False

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call
  End Sub

  'UserControl overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Private WithEvents opt_c As System.Windows.Forms.RadioButton
  Private WithEvents opt_b As System.Windows.Forms.RadioButton
  Private WithEvents opt_a As System.Windows.Forms.RadioButton
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.opt_c = New System.Windows.Forms.RadioButton()
    Me.opt_b = New System.Windows.Forms.RadioButton()
    Me.opt_a = New System.Windows.Forms.RadioButton()
    Me.SuspendLayout()
    '
    'opt_c
    '
    Me.opt_c.Checked = True
    Me.opt_c.Dock = System.Windows.Forms.DockStyle.Left
    Me.opt_c.Location = New System.Drawing.Point(128, 0)
    Me.opt_c.Name = "opt_c"
    Me.opt_c.Size = New System.Drawing.Size(64, 16)
    Me.opt_c.TabIndex = 5
    Me.opt_c.TabStop = True
    Me.opt_c.Text = "xAmbos"
    '
    'opt_b
    '
    Me.opt_b.Dock = System.Windows.Forms.DockStyle.Left
    Me.opt_b.Location = New System.Drawing.Point(64, 0)
    Me.opt_b.Name = "opt_b"
    Me.opt_b.Size = New System.Drawing.Size(64, 16)
    Me.opt_b.TabIndex = 4
    Me.opt_b.Text = "xNo"
    '
    'opt_a
    '
    Me.opt_a.Dock = System.Windows.Forms.DockStyle.Left
    Me.opt_a.Location = New System.Drawing.Point(0, 0)
    Me.opt_a.Name = "opt_a"
    Me.opt_a.Size = New System.Drawing.Size(64, 16)
    Me.opt_a.TabIndex = 3
    Me.opt_a.Text = "xS�"
    '
    'uc_trioption
    '
    Me.Controls.Add(Me.opt_c)
    Me.Controls.Add(Me.opt_b)
    Me.Controls.Add(Me.opt_a)
    Me.Name = "uc_trioption"
    Me.Size = New System.Drawing.Size(192, 16)
    Me.ResumeLayout(False)

  End Sub

#End Region

  Public Enum ENUM_TRIOPTION
    TRIOPTION_A = 0
    TRIOPTION_B = 1
    TRIOPTION_C = 2
  End Enum

  ' PURPOSE: Gets/Sets the selected option
  ' NOTES:
  '
  Public Property SelectedOption() As ENUM_TRIOPTION
    Get
      If Me.opt_a.Checked Then
        Return ENUM_TRIOPTION.TRIOPTION_A
      End If

      If Me.opt_b.Checked Then
        Return ENUM_TRIOPTION.TRIOPTION_B
      End If

      Return ENUM_TRIOPTION.TRIOPTION_C
    End Get
    Set(ByVal Value As ENUM_TRIOPTION)
      Me.opt_a.Checked = False
      Me.opt_b.Checked = False
      Me.opt_c.Checked = False
      Select Case Value
        Case ENUM_TRIOPTION.TRIOPTION_A
          Me.opt_a.Checked = True
        Case ENUM_TRIOPTION.TRIOPTION_B
          Me.opt_b.Checked = True
        Case ENUM_TRIOPTION.TRIOPTION_C
          Me.opt_c.Checked = True
        Case Else
          '
      End Select
    End Set
  End Property


  ' PURPOSE: This event is raised whenever the selected stated is changed
  Public Event TristateChangedEvent()

  Private Sub AddEventHandlers()
    AddHandler opt_a.CheckedChanged, AddressOf TristateChangeHandlerA
    AddHandler opt_b.CheckedChanged, AddressOf TristateChangeHandlerB
    AddHandler opt_c.CheckedChanged, AddressOf TristateChangeHandlerC
  End Sub

  Private Sub TristateChangeHandlerA(ByVal sender As Object, ByVal e As System.EventArgs)
    If opt_a.Checked Then
      RaiseEvent TristateChangedEvent()
    End If
  End Sub

  Private Sub TristateChangeHandlerB(ByVal sender As Object, ByVal e As System.EventArgs)
    If opt_b.Checked Then
      RaiseEvent TristateChangedEvent()
    End If
  End Sub

  Private Sub TristateChangeHandlerC(ByVal sender As Object, ByVal e As System.EventArgs)
    If opt_c.Checked Then
      RaiseEvent TristateChangedEvent()
    End If
  End Sub

  ' PURPOSE: Sets whether or not the control is displayed vertical or horizontal
  Public Property Vertical() As Boolean
    Get
      Return Not m_horizontal
    End Get
    Set(ByVal Value As Boolean)
      Dim ww As Integer
      If Not m_horizontal <> Value Then
        ww = Me.opt_a.Width
        m_horizontal = Not Value

        If Not m_horizontal Then
          Me.opt_a.Dock = DockStyle.Top
          Me.opt_b.Dock = DockStyle.Top
          Me.opt_c.Dock = DockStyle.Top
        Else
          Me.opt_a.Dock = DockStyle.Left
          Me.opt_b.Dock = DockStyle.Left
          Me.opt_c.Dock = DockStyle.Left
        End If

      End If
    End Set
  End Property


  ' PURPOSE:
  '   - INPUT:
  '   - OUTPUT:
  '   - RETURNS:
  ' NOTES:
  Private Sub SizeChangedHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.SizeChanged
    If Me.DesignMode Then
      Call RedrawOptions(Me.opt_a.Width, Me.opt_b.Width, Me.opt_c.Width)
    End If

  End Sub

  Private Sub RedrawOptions(ByVal SizeA As Integer, ByVal SizeB As Integer, ByVal SizeC As Integer)
    Dim new_size As Integer

    If Not m_horizontal Then
      Me.opt_a.Height = OPTION_HEIGHT
      Me.opt_b.Height = OPTION_HEIGHT
      Me.opt_c.Height = OPTION_HEIGHT
      If Me.Height <> 3 * OPTION_HEIGHT Then
        Me.Height = 3 * OPTION_HEIGHT
      End If
    Else
      new_size = Me.Size.Width / 3
      Me.opt_a.Width = new_size
      Me.opt_b.Width = new_size
      Me.opt_c.Width = new_size
      If Me.Height <> OPTION_HEIGHT Then
        Me.Height = OPTION_HEIGHT
      End If
    End If

    ' TODO REVISE
    If Me.opt_a.Width <> SizeA Then
      Me.opt_a.Width = SizeA
    End If

    If Me.opt_b.Width <> SizeB Then
      Me.opt_b.Width = SizeB
    End If

    If Me.opt_c.Width <> SizeC Then
      Me.opt_c.Width = SizeC
    End If

  End Sub

  ' PURPOSE: Initializes the control and sets the initial selected option
  '   - INPUT:
  '     - InitialState, the initial selected option
  ' NOTES:
  '   - Assigns the NLS strings to the radio buttons.
  '   - Adds the event handlers to the radio buttons.
  '
  Public Sub Init(Optional ByVal InitialOption As ENUM_TRIOPTION = ENUM_TRIOPTION.TRIOPTION_C)
    Me.Init(Me.opt_a.Width, Me.opt_b.Width, Me.opt_c.Width, InitialOption)
  End Sub
  Public Sub Init(ByVal SizeA As Integer, ByVal SizeB As Integer, ByVal SizeC As Integer, Optional ByVal InitialOption As ENUM_TRIOPTION = ENUM_TRIOPTION.TRIOPTION_C)
    If Not Me.DesignMode Then
      Me.SelectedOption = InitialOption

      Me.opt_a.Text = GLB_NLS_GUI_CONTROLS.GetString(283)  ' * Yes
      Me.opt_b.Text = GLB_NLS_GUI_CONTROLS.GetString(284)  ' * No
      Me.opt_c.Text = GLB_NLS_GUI_CONTROLS.GetString(285)  ' * Both

      ' Add the Event Handler for each radio button
      Call AddEventHandlers()
      ' 
      Call RedrawOptions(SizeA, SizeB, SizeC)
    End If
  End Sub

#Region " Properties "

  ' PURPOSE: Sets the desired text to the given radio button
  '   - INPUT: 
  '     - TrisateOption, the option to change its text
  '     - Value, the text to be set
  '
  Public WriteOnly Property OptionText(ByVal TristateOption As ENUM_TRIOPTION) As String
    Set(ByVal Value As String)
      Select Case TristateOption
        Case ENUM_TRIOPTION.TRIOPTION_A
          Me.opt_a.Text = Value
        Case ENUM_TRIOPTION.TRIOPTION_B
          Me.opt_b.Text = Value
        Case ENUM_TRIOPTION.TRIOPTION_C
          Me.opt_c.Text = Value
        Case Else
          '
      End Select
    End Set
  End Property

#End Region

End Class
