'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_multi_average_currency.vb  
'
' DESCRIPTION:   print average values
'
' AUTHOR:        Ferran Ortner
'
' CREATION DATE: 15-APR-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-APR-2015  FOS    Initial version
' 07-MAY-2015  FOS    Add new controls to show
' 25-MAY-2015  FOS    Show "--" when currency is not defined
' 06-JUN-2015  FOS    ExchangeDecimals with GP, default 4 decimals
'--------------------------------------------------------------------
Imports System.Windows.Forms
Imports System.Drawing
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports WSI.Common

Public Class uc_multi_average_currency

#Region "Members"
  Private m_average_table As DataTable
  Private m_average_list As String

  Private m_lbl_currency As Label
  Private m_lbl_currency_definition As Label
  Private m_lbl_currency_site As Label
  Private m_last_iso_code As String
  Private m_is_iso_code_in_list As Boolean
  Private m_main_currency As String
  Private m_default_location_x As Integer
  Private m_default_location_y As Integer


  Public Property GetAverageList() As String
    Get
      Return m_average_list
    End Get
    Set(ByVal value As String)
      m_average_list = value
    End Set
  End Property

#End Region

#Region "Constants"

  Private Const CONVERSION_VALUE As String = "1 "

#End Region

  ' PURPOSE: Display information from a table that contains the media data exchange by currency
  '
  '  PARAMS:
  '     - INPUT:
  '           - AverageCurrencies: DataTable that contains the media data exchange by currency
  '           - MainCurrency: ISO_CODE of multisite
  '           - DisplayMode:  General Param to display the format currency
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub RedrawCurrenciesValues(ByVal AverageCurrencies As DataTable, ByVal MainCurrency As String, ByVal DisplayMode As Boolean)

    Dim _row_sites_currency_average As DataRow
    Dim _lbl_actual_currency As Label
    Dim _hide_control As Boolean
    _lbl_actual_currency = New Label()
    _lbl_actual_currency.Text = String.Empty


    Call CleanPanel()

    Call InitializeValues(AverageCurrencies, MainCurrency)

    For Each _row_sites_currency_average In m_average_table.Rows

      _hide_control = m_last_iso_code.Contains(MainCurrency)

      If Not _hide_control Then

        m_lbl_currency = New Label()
        m_lbl_currency_definition = New Label()
        m_lbl_currency_site = New Label()

        If DisplayMode = False Then
          Call PrepareRowSiteCurrencyToMainCurrency(_row_sites_currency_average)
        Else
          Call PrepareRowMainCurrencyToSiteCurrency(_row_sites_currency_average)
        End If


        If Not m_is_iso_code_in_list Then

          Call PrintAverageCurrencyLabel()

          If String.IsNullOrEmpty(m_average_list) Then
            m_average_list = m_lbl_currency_definition.Text & " " & m_lbl_currency.Text & " " & m_lbl_currency_site.Text
          Else
            m_average_list = m_average_list & ";" & m_lbl_currency_definition.Text & " " & m_lbl_currency.Text & " " & m_lbl_currency_site.Text
          End If
        End If
      End If
    Next
  End Sub ' RedrawCurrenciesValues

  ' PURPOSE: Initialize globlas values to prepare for print values
  '
  '  PARAMS:
  '     - INPUT:
  '           - AverageCurrencies: DataTable that contains the media data exchange by currency
  '           - MainCurrency: ISO_CODE of multisite
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InitializeValues(ByVal AverageCurrencies As DataTable, ByVal MainCurrency As String)
    m_main_currency = MainCurrency
    m_average_list = String.Empty
    m_default_location_x = 2
    m_default_location_y = 0
    m_last_iso_code = String.Empty

    If AverageCurrencies Is Nothing Then
      Return
    End If

    m_average_table = New DataTable
    m_average_table = AverageCurrencies

    If m_average_table.Rows.Count <= 0 Then
      CurrencyMultisite.GetSiteCurrencies(m_average_table)
    End If

  End Sub ' InitializeValues

  ' PURPOSE: Prepare the information of display currency
  '
  '  PARAMS:
  '     - INPUT:
  '           - _row_sites_currency_average: DataRow with the values to print
  '     - OUTPUT:
  '           - None
  '     - FORMAT DISPLAY = 1MXN 0,0323 USD
  '                        1PEN 0,0521 USD
  ' RETURNS:
  '     - None
  Private Sub PrepareRowMainCurrencyToSiteCurrency(ByVal _row_sites_currency_average As DataRow)
    m_lbl_currency_definition.Text = CONVERSION_VALUE & m_main_currency
    If (m_average_table.Columns.Count > 2) AndAlso (_row_sites_currency_average(2) = 0) Then
      m_lbl_currency.Text = " --- "
    Else
      Dim _change As Decimal
      _change = 1 / _row_sites_currency_average(1)
      m_lbl_currency.Text = GUI_MultiCurrencyValue_Format(_change, False)
    End If
    m_lbl_currency_site.Text = _row_sites_currency_average(0).ToString()
    m_is_iso_code_in_list = m_lbl_currency_site.Text.Equals(m_last_iso_code)
    m_last_iso_code = m_lbl_currency_site.Text
  End Sub ' PrepareRowMainCurrencyToSiteCurrency

  ' PURPOSE: Prepare the information of display currency
  '
  '  PARAMS:
  '     - INPUT:
  '           - _row_sites_currency_average: DataRow with the values to print
  '     - OUTPUT:
  '           - None
  '     - FORMAT DISPLAY = 1USD 1,3457 MXN
  '                        1USD 10,521 PEN
  ' RETURNS:
  '     - None
  Private Sub PrepareRowSiteCurrencyToMainCurrency(ByVal _row_sites_currency_average As DataRow)

    m_lbl_currency_definition.Text = CONVERSION_VALUE & _row_sites_currency_average(0).ToString()
    If (m_average_table.Columns.Count > 2) AndAlso (_row_sites_currency_average(2) = 0) Then
      m_lbl_currency.Text = " --- "
    Else
      m_lbl_currency.Text = GUI_MultiCurrencyValue_Format(_row_sites_currency_average(1).ToString(), False)
    End If

    m_lbl_currency_site.Text = m_main_currency
    m_is_iso_code_in_list = m_lbl_currency_definition.Text.Equals(m_last_iso_code)
    m_last_iso_code = m_lbl_currency_definition.Text

  End Sub  ' PrepareRowSiteCurrencyToMainCurrency

  ' PURPOSE: Print row to display in the panel
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  ' RETURNS:
  '     - None
  Private Sub PrintAverageCurrencyLabel()

    m_lbl_currency.TextAlign = ContentAlignment.MiddleCenter

    m_lbl_currency_definition.Location = New Point(m_default_location_x, m_default_location_y)
    m_lbl_currency_definition.Height = 15
    m_lbl_currency_definition.Width = 44

    m_lbl_currency.Height = 15
    m_lbl_currency.Width = 75
    m_lbl_currency.Location = New Point(m_default_location_x + m_lbl_currency_definition.Width + 2, m_default_location_y)
    m_lbl_currency.ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLUE_03)

    m_lbl_currency_site.Location = New Point(m_default_location_x + m_lbl_currency_definition.Width + m_lbl_currency.Width + 2, m_default_location_y)
    m_lbl_currency_site.Width = 31
    m_lbl_currency_site.Height = 15

    Me.Controls.Add(m_lbl_currency_definition)
    Me.Controls.Add(m_lbl_currency)
    Me.Controls.Add(m_lbl_currency_site)
    m_default_location_y = m_default_location_y + 20
  End Sub ' PrintAverageCurrencyLabel

  ' PURPOSE: Remove all controls of user control 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub CleanPanel()
    Dim _ctl As Control

    For _idx As Integer = Me.Controls.Count - 1 To 0 Step -1
      _ctl = Controls.Item(_idx)
      Controls.Remove(_ctl)
      _ctl.Dispose()
    Next

  End Sub ' CleanControls 

End Class
