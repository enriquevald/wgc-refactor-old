'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_data_import.vb  
'
' DESCRIPTION:   Data import
'
' AUTHOR:        Adri�n Cuartas
'
' CREATION DATE: 07-OCT-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 07-OCT-2013  ACM    Initial version
' 27-OCT-2015  ECP    Backlog Item 1080 GUI - Improvements to Gaming Table Plays Sessions Import
' 22-SET-2016  XGJ    Bug Item : 17664:No se realiza la importaci�n de puntos a la cuenta de un cliente si el formato de la hoja de c�lculo no es correcto
'-------------------------------------------------------------------

#Region "Imports"

Imports System.Windows.Forms
Imports WSI.Common
Imports System.Text

#End Region

Public Class uc_data_import

#Region "Properties"

  Public Property Value() As String
    Get
      Return ef_file_path.Value
    End Get
    Set(ByVal NewValue As String)
      ef_file_path.Value = NewValue
    End Set
  End Property

  Public Property LogFile() As String
    Get
      Return m_log_file
    End Get
    Set(ByVal value As String)
      m_log_file = value
    End Set
  End Property

  Public Property ExcelUseCurrentRegion() As Boolean
    Get
      Return m_excel_use_current_region
    End Get
    Set(ByVal value As Boolean)
      m_excel_use_current_region = value
    End Set
  End Property

#End Region

  Private Delegate Sub ImportData()

#Region "Constants"

#End Region

#Region "Members"

  Private m_fields_to_import As Dictionary(Of String, String)
  Private m_tool_tip As ToolTip
  Private m_excel_use_current_region As Boolean
  Private m_log_file As String

#End Region

#Region "Properties"

#End Region

#Region "Public Functions"

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    m_fields_to_import = New Dictionary(Of String, String)
    m_tool_tip = New ToolTip
    With m_tool_tip
      .InitialDelay = 500
      .ReshowDelay = 1000
      .ShowAlways = True
      .AutomaticDelay = 1000
      .AutoPopDelay = 6000
    End With

    ' Add any initialization after the InitializeComponent() call.
    InitControls()

  End Sub

  ' PURPOSE: Filling a dictionary with the fields and their data types
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub AddColumnsMapping(ByVal Field As String, ByVal DataType As String)
    Dim _tool_tip_text As StringBuilder

    _tool_tip_text = New StringBuilder()

    ' Fill dic
    If Not IsNothing(Type.GetType(DataType)) And Not m_fields_to_import.ContainsKey(Field) Then
      m_fields_to_import.Add(Field, DataType)
    End If

    ' Add fields to tooltip
    If m_fields_to_import.Count > 0 Then
      For Each _field As KeyValuePair(Of String, String) In m_fields_to_import
        _tool_tip_text.AppendLine(_field.Key)
      Next
    End If

    ' Set the tooltip text
    m_tool_tip.ToolTipTitle = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1400)
    m_tool_tip.SetToolTip(Me.btn_open_file_dialog, _tool_tip_text.ToString)

  End Sub

#End Region

#Region "Private Functions"

  ' PURPOSE: Own controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub InitControls()

    m_excel_use_current_region = True

    Me.ef_file_path.Text = GLB_NLS_GUI_CLASS_II.GetString(428)
    Me.ef_file_path.IsReadOnly = True
    Me.ef_file_path.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT)

  End Sub ' InitControls

#End Region

#Region "Events"

  Public Event FileImported(ByRef ImportedDataTable As DataTable)

  ' PURPOSE: Opens a File Dialog to choose the document to import
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Private Sub btn_open_file_dialog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_open_file_dialog.Click

    'Fields and data type are ok
    If m_fields_to_import.Count > 0 Then
      Dim _file_dialog As OpenFileDialog
      Dim _frm_data_import As frm_data_import

      _file_dialog = New OpenFileDialog()

      _file_dialog.Multiselect = False
      _file_dialog.Filter = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1242)
      _file_dialog.ShowDialog()

      ' Maybe cancel click on dialog
      If Not String.IsNullOrEmpty(_file_dialog.FileName) Then

        Me.ef_file_path.Enabled = False

        ' Shows data import form
        _frm_data_import = New frm_data_import()
        _frm_data_import.ExcelPath = _file_dialog.FileName
        _frm_data_import.ExcelColumns = m_fields_to_import
        _frm_data_import.ExcelUseCurrentRegion = m_excel_use_current_region
        _frm_data_import.ShowForm()

        'If _frm_data_import.m_imported_data.Rows.Count > 0 Then
        ' Get imported DataTable
        Me.ef_file_path.Value = System.IO.Path.GetFileName(_file_dialog.FileName)
        Me.LogFile = _frm_data_import.ExcelPath

        RaiseEvent FileImported(_frm_data_import.m_imported_data)
        'End If

        Me.ef_file_path.Enabled = True
      End If

    Else
      Me.ef_file_path.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(840) ' "You should specify the fields to be imported"
    End If

  End Sub

#End Region

End Class
