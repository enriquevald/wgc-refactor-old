<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_week_days
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.chk_sunday = New System.Windows.Forms.CheckBox()
    Me.chk_saturday = New System.Windows.Forms.CheckBox()
    Me.chk_monday = New System.Windows.Forms.CheckBox()
    Me.chk_friday = New System.Windows.Forms.CheckBox()
    Me.chk_tuesday = New System.Windows.Forms.CheckBox()
    Me.chk_thursday = New System.Windows.Forms.CheckBox()
    Me.chk_wednesday = New System.Windows.Forms.CheckBox()
    Me.SuspendLayout()
    '
    'chk_sunday
    '
    Me.chk_sunday.AutoSize = True
    Me.chk_sunday.Location = New System.Drawing.Point(6, 154)
    Me.chk_sunday.Name = "chk_sunday"
    Me.chk_sunday.Size = New System.Drawing.Size(76, 17)
    Me.chk_sunday.TabIndex = 15
    Me.chk_sunday.Text = "xSunday"
    Me.chk_sunday.UseVisualStyleBackColor = True
    '
    'chk_saturday
    '
    Me.chk_saturday.AutoSize = True
    Me.chk_saturday.Location = New System.Drawing.Point(6, 129)
    Me.chk_saturday.Name = "chk_saturday"
    Me.chk_saturday.Size = New System.Drawing.Size(85, 17)
    Me.chk_saturday.TabIndex = 14
    Me.chk_saturday.Text = "xSaturday"
    Me.chk_saturday.UseVisualStyleBackColor = True
    '
    'chk_monday
    '
    Me.chk_monday.AutoSize = True
    Me.chk_monday.Location = New System.Drawing.Point(6, 4)
    Me.chk_monday.Name = "chk_monday"
    Me.chk_monday.Size = New System.Drawing.Size(77, 17)
    Me.chk_monday.TabIndex = 9
    Me.chk_monday.Text = "xMonday"
    Me.chk_monday.UseVisualStyleBackColor = True
    '
    'chk_friday
    '
    Me.chk_friday.AutoSize = True
    Me.chk_friday.Location = New System.Drawing.Point(6, 104)
    Me.chk_friday.Name = "chk_friday"
    Me.chk_friday.Size = New System.Drawing.Size(68, 17)
    Me.chk_friday.TabIndex = 13
    Me.chk_friday.Text = "xFriday"
    Me.chk_friday.UseVisualStyleBackColor = True
    '
    'chk_tuesday
    '
    Me.chk_tuesday.AutoSize = True
    Me.chk_tuesday.Location = New System.Drawing.Point(6, 29)
    Me.chk_tuesday.Name = "chk_tuesday"
    Me.chk_tuesday.Size = New System.Drawing.Size(80, 17)
    Me.chk_tuesday.TabIndex = 10
    Me.chk_tuesday.Text = "xTuesday"
    Me.chk_tuesday.UseVisualStyleBackColor = True
    '
    'chk_thursday
    '
    Me.chk_thursday.AutoSize = True
    Me.chk_thursday.Location = New System.Drawing.Point(6, 79)
    Me.chk_thursday.Name = "chk_thursday"
    Me.chk_thursday.Size = New System.Drawing.Size(86, 17)
    Me.chk_thursday.TabIndex = 12
    Me.chk_thursday.Text = "xThursday"
    Me.chk_thursday.UseVisualStyleBackColor = True
    '
    'chk_wednesday
    '
    Me.chk_wednesday.AutoSize = True
    Me.chk_wednesday.Location = New System.Drawing.Point(6, 54)
    Me.chk_wednesday.Name = "chk_wednesday"
    Me.chk_wednesday.Size = New System.Drawing.Size(98, 17)
    Me.chk_wednesday.TabIndex = 11
    Me.chk_wednesday.Text = "xWednesday"
    Me.chk_wednesday.UseVisualStyleBackColor = True
    '
    'uc_week_days
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.chk_sunday)
    Me.Controls.Add(Me.chk_saturday)
    Me.Controls.Add(Me.chk_monday)
    Me.Controls.Add(Me.chk_friday)
    Me.Controls.Add(Me.chk_tuesday)
    Me.Controls.Add(Me.chk_thursday)
    Me.Controls.Add(Me.chk_wednesday)
    Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Name = "uc_week_days"
    Me.Size = New System.Drawing.Size(108, 176)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Public WithEvents chk_sunday As System.Windows.Forms.CheckBox
  Public WithEvents chk_saturday As System.Windows.Forms.CheckBox
  Public WithEvents chk_monday As System.Windows.Forms.CheckBox
  Public WithEvents chk_friday As System.Windows.Forms.CheckBox
  Public WithEvents chk_tuesday As System.Windows.Forms.CheckBox
  Public WithEvents chk_thursday As System.Windows.Forms.CheckBox
  Public WithEvents chk_wednesday As System.Windows.Forms.CheckBox

End Class
