﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_checked_all_several_list.vb
' DESCRIPTION:   Checked all several list
' AUTHOR:        Enric Tomas Parisi
' CREATION DATE: 26-JUN-2017
'
' REVISION HISTORY:
'
' Date         Author  Description
' -----------  ------  -----------------------------------------------
' 26-JUN-2017   ETP     Initial version
'--------------------------------------------------------------------

Public Class uc_checked_several_all_list

#Region " Const "

  Private Const GRID_FILTER_COLUMN_ID As Integer = 0
  Private Const GRID_FILTER_COLUMN_CHECKED As Integer = 1
  Private Const GRID_FILTER_COLUMN_DESC As Integer = 2

  Private Const GRID_FILTER_HEADER = 0
  Private Const GRID_FILTER_DATA = 1

  Private Const GRID_FILTER_COLUMNS As Integer = 3
  Private Const GRID_FILTER_HEADER_ROWS As Integer = 1

#End Region ' Const


#Region " Properties "


  Public Property GroupBoxText() As String

    Get
      Return Me.gb_tittle.Text
    End Get

    Set(ByVal Value As String)
      Me.gb_tittle.Text = Value
    End Set

  End Property

#End Region


#Region " Constructor "

  Public Sub New()

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControl()

  End Sub
#End Region ' Constructor


#Region " Private methods"

  ''' <summary>
  ''' Init 
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitControl()
    Call GUI_StyleSheetFilter()

    If System.Reflection.Assembly.GetEntryAssembly() Is Nothing Then
      Return
    End If

    Me.opt_several.Text = GLB_NLS_GUI_STATISTICS.GetString(217)
    Me.opt_all.Text = GLB_NLS_GUI_STATISTICS.GetString(218)

  End Sub ' Init Control

  ''' <summary>
  ''' AddRowDataFilter
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <remarks></remarks>
  Private Sub AddRowDataFilter(ByRef Row As DataRow)

    Dim prev_redraw As Boolean

    prev_redraw = dg_checked_list.Redraw
    dg_checked_list.Redraw = False

    dg_checked_list.AddRow()

    dg_checked_list.Redraw = False

    dg_checked_list.Cell(dg_checked_list.NumRows - 1, GRID_FILTER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    dg_checked_list.Cell(dg_checked_list.NumRows - 1, GRID_FILTER_COLUMN_ID).Value = Row(0)
    dg_checked_list.Cell(dg_checked_list.NumRows - 1, GRID_FILTER_COLUMN_DESC).Value = Row(1)

    dg_checked_list.Redraw = prev_redraw

  End Sub ' AddRowDataFilter

  ''' <summary>
  ''' Set style
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_StyleSheetFilter()

    With dg_checked_list
      Call .Init(GRID_FILTER_COLUMNS, GRID_FILTER_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden columns
      .Column(GRID_FILTER_COLUMN_ID).Header.Text = ""
      .Column(GRID_FILTER_COLUMN_ID).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_FILTER_COLUMN_CHECKED).Header.Text = ""
      .Column(GRID_FILTER_COLUMN_CHECKED).WidthFixed = 400
      .Column(GRID_FILTER_COLUMN_CHECKED).Fixed = True
      .Column(GRID_FILTER_COLUMN_CHECKED).Editable = True
      .Column(GRID_FILTER_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_FILTER_COLUMN_DESC).Header.Text = ""
      .Column(GRID_FILTER_COLUMN_DESC).Fixed = True
      .Column(GRID_FILTER_COLUMN_DESC).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub ' GUI_StyleSheetFilter

  ''' <summary>
  ''' Cell change event
  ''' </summary>
  ''' <param name="Row"></param>
  ''' <param name="Column"></param>
  ''' <remarks></remarks>
  Private Sub dg_checked_list_CellDataChangedEvent(Row As Integer, Column As Integer) Handles dg_checked_list.CellDataChangedEvent

    ' Update radio buttons accordingly
    If Me.opt_several.Checked = False Then
      Me.opt_several.Checked = True
    End If
  End Sub ' dg_checked_list_CellDataChangedEvent

#End Region ' Private methods


#Region " Public methods"

  ''' <summary>
  ''' Add DataTable
  ''' </summary>
  ''' <param name="Results"></param>
  ''' <remarks></remarks>
  Public Sub Add(ByVal Results As DataTable)

    For Each _row As DataRow In Results.Rows
      AddRowDataFilter(_row)
    Next

  End Sub ' Add

  ''' <summary>
  ''' Set Default Values
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub SetDefaultValues()
    opt_all.Checked = True
  End Sub ' SetDefaultValues

  ''' <summary>
  ''' Set column width
  ''' </summary>
  ''' <param name="Index"></param>
  ''' <param name="Width"></param>
  ''' <remarks></remarks>
  Public Sub ColumnWidth(ByVal Index As Integer, ByVal Width As Integer)
    If Me.dg_checked_list.NumColumns >= Index Then
      Me.dg_checked_list.Column(Index).WidthFixed = Width
    End If
  End Sub ' ColumnWidth

  ''' <summary>
  ''' Is All selected
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function IsAllSelected() As Boolean

    Dim _count As Int32

    _count = 0

    If opt_all.Checked Then
      Return True
    End If

    For i As Int32 = 0 To dg_checked_list.NumRows - 1
      If dg_checked_list.Cell(i, GRID_FILTER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _count += 1
      End If
    Next

    Return (_count = dg_checked_list.NumRows Or _count = 0)

  End Function ' IsAllSelected

  ''' <summary>
  ''' Get Index list
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function SelectedIndexesList() As String

    Dim _list_id As List(Of String)

    _list_id = New List(Of String)

    If opt_all.Checked Then
      Return String.Empty
    End If

    For i As Int32 = 0 To dg_checked_list.NumRows - 1
      If dg_checked_list.Cell(i, GRID_FILTER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _list_id.Add(dg_checked_list.Cell(i, GRID_FILTER_COLUMN_ID).Value.ToString)
      End If
    Next

    Return String.Join(", ", _list_id.ToArray())

  End Function ' SelectedIndexesList

  ''' <summary>
  ''' Get Values list
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function SelectedValuesList() As String

    Dim _list_id As List(Of String)

    _list_id = New List(Of String)

    If opt_all.Checked Then
      Return String.Empty
    End If

    For i As Int32 = 0 To dg_checked_list.NumRows - 1
      If dg_checked_list.Cell(i, GRID_FILTER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _list_id.Add(dg_checked_list.Cell(i, GRID_FILTER_COLUMN_DESC).Value.ToString)
      End If
    Next

    Return String.Join(", ", _list_id.ToArray())

  End Function ' SelectedValuesList

#End Region ' Public methods 


  Private Sub opt_all_CheckedChanged(sender As Object, e As EventArgs) Handles opt_all.CheckedChanged
    Dim _idx_row As Int32

    If opt_all.Checked = True Then

      For _idx_row = 0 To dg_checked_list.NumRows - 1
        dg_checked_list.Cell(_idx_row, GRID_FILTER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      Next

    End If

  End Sub
End Class
