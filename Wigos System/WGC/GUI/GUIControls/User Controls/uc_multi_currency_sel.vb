'-------------------------------------------------------------------
' Copyright � 2015 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_multi_currency_sel.vb  
'
' DESCRIPTION:   Manage currency site
'
' AUTHOR:        Ferran Ortner
'
' CREATION DATE: 15-APR-2015
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-APR-2015  FOS    Initial version
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports System.Drawing
Imports WSI.Common

Public Class uc_multi_currency_sel

#Region " Members "

  Private m_idsites_print As String = ""
  Private m_show_multisite_row As Boolean = False
  Private m_multiselect As Boolean = False
  Private m_cell_disabled_color As System.Drawing.Color = System.Drawing.Color.FromArgb(240, 240, 240)
  Private m_cell_text_disabled_color As System.Drawing.Color = System.Drawing.Color.FromArgb(109, 109, 109)
  Private m_cell_enable_color As System.Drawing.Color = Drawing.Color.White
  Private m_cell_text_enable_color As System.Drawing.Color = Drawing.Color.Black
  Private m_date_from As DateTime
  Private m_date_to As DateTime
  Private m_currencies_values As DataTable
  Private m_main_currency As String
  Private m_cell_no_currency As System.Drawing.Color = Drawing.Color.Red
  Private m_display_mode As Boolean '0 -> moneda sala a moneda multisite ; 1 -> moneda multisite a moneda sala
  Private m_has_sites_without_currency As Boolean
  Private m_is_grid_apply_changes As Boolean
  Private m_is_sites_checked As Boolean
  Private m_delegate As WSI.Common.CurrencyMultisiteThread.CurrencyMultiSiteChangedHandler
  Private m_iso_code_selected As String
  Private m_show_iso_code As Boolean
  Private m_id_sites_checked As String
  Private m_num_rows_checked As Integer
  Private m_sites_detail As DataTable
  Private m_average_currency_list As String

#End Region ' Members

#Region "Constants"

  Private Const GRID_2_COLUMN_CHECKED As Integer = 0
  Private Const GRID_2_COLUMN_ID As Integer = 1
  Private Const GRID_2_COLUMN_ID_FORMAT As Integer = 2
  Private Const GRID_2_COLUMN_ISO_CODE As Integer = 3
  Private Const GRID_2_COLUMN_NAME As Integer = 4

  Private Const GRID_2_COLUMNS As Integer = 5
  Private Const GRID_2_HEADER_ROWS As Integer = 0

  Private Const CONTROL_HEIGHT As Integer = 160
  Private Const CONTROL_HEIGHT_COLLAPSED As Integer = 128

#End Region ' Constants

#Region "Public Properties"

  Public Property ShowMultisiteRow() As Boolean
    Get
      Return m_show_multisite_row
    End Get

    Set(ByVal value As Boolean)
      m_show_multisite_row = value
    End Set
  End Property 'ShowMultisiteRow

  Public Property MultiSelect() As Boolean
    Get
      Return m_multiselect
    End Get
    Set(ByVal value As Boolean)
      m_multiselect = value

      If Me.m_multiselect Then
        Me.uc_site_sel.gb_sites.Height = CONTROL_HEIGHT
        Me.uc_site_sel.btn_check_all.Enabled = True
        Me.uc_site_sel.btn_check_all.Visible = True
        Me.uc_site_sel.btn_uncheck_all.Enabled = True
        Me.uc_site_sel.btn_uncheck_all.Visible = True
      Else
        Me.uc_site_sel.gb_sites.Height = CONTROL_HEIGHT_COLLAPSED
        Me.uc_site_sel.btn_check_all.Enabled = False
        Me.uc_site_sel.btn_check_all.Visible = False
        Me.uc_site_sel.btn_uncheck_all.Enabled = False
        Me.uc_site_sel.btn_uncheck_all.Visible = False
      End If

    End Set
  End Property 'MultiSelect

  Public Property DateFrom() As DateTime
    Get
      Return m_date_from
    End Get
    Set(ByVal value As DateTime)
      m_date_from = value
      RefreshChangeTable()
    End Set
  End Property ' DateFrom

  Public Property DateTo() As DateTime
    Get
      Return m_date_to
    End Get
    Set(ByVal value As DateTime)
      m_date_to = value
      RefreshChangeTable()
    End Set
  End Property ' DateTo

  Public Property CurrenciesTable() As DataTable
    Get
      Return m_currencies_values
    End Get
    Set(ByVal value As DataTable)
      m_currencies_values = value
    End Set
  End Property ' CurrenciesTable

  Public Property MultiSiteCurrency() As String
    Get
      Return m_main_currency
    End Get
    Set(ByVal value As String)
      m_main_currency = value
    End Set
  End Property ' MultiSiteCurrency

  Public Property HasSitesWithoutCurrency() As Boolean
    Get
      Return m_has_sites_without_currency
    End Get

    Set(ByVal value As Boolean)
      m_has_sites_without_currency = value
    End Set
  End Property 'HasSitesWithoutCurrency

  Public Property IsGridApplyChanges() As Boolean
    Get
      Return m_is_grid_apply_changes
    End Get

    Set(ByVal value As Boolean)
      m_is_grid_apply_changes = value
    End Set
  End Property 'CanChangeFormatCurrency

  Public Property IsSitesChecked() As Boolean
    Get
      Return m_is_sites_checked
    End Get
    Set(ByVal value As Boolean)
      m_is_sites_checked = value
    End Set
  End Property ' IsSitesChecked

  Public Property IsoCodeSelected() As String
    Get
      Return m_iso_code_selected
    End Get
    Set(ByVal value As String)
      m_iso_code_selected = value
    End Set
  End Property ' IsoCodeSelected

  Public Property ShowIsoCode() As Boolean
    Get
      Return m_show_iso_code
    End Get
    Set(ByVal value As Boolean)
      m_show_iso_code = value
    End Set
  End Property ' ShowIsoCode

  Public Property GetSitesChecked() As String
    Get
      Return m_id_sites_checked
    End Get
    Set(ByVal value As String)
      m_id_sites_checked = value
    End Set
  End Property ' GetSitesChecked

  Public Property NumRowsChecked() As Integer
    Get
      Return m_num_rows_checked
    End Get
    Set(ByVal value As Integer)
      m_num_rows_checked = value
    End Set
  End Property ' NumRowsChecked

  Public Property GetAverageCurrencyList() As String
    Get
      Return m_average_currency_list
    End Get
    Set(ByVal value As String)
      m_average_currency_list = value
    End Set
  End Property ' GetAverageCurrencyList

  Private Delegate Sub GetSiteCurrencyAverageCallback()

#End Region

#Region " Public Functions "

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ShowMultisiteRow = False

    MultiSelect = False

    ShowIsoCode = True

  End Sub ' New

  ' PURPOSE: Init routine of the control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub Init()

    Call InitControls()
  End Sub ' Init

  ' PURPOSE: Set default values to filters for multicurrency
  '
  '  PARAMS:
  '     - INPUT:
  '           - Optional Param to set all items selected or unselected.
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub SetDefaultValuesMulticurrency(Optional ByVal SelectAllItems As Boolean = True)
    Dim _idx_rows As Integer

    m_has_sites_without_currency = False

    Call GetNumRowsSelected()

    If m_num_rows_checked > 1 Then

      For _idx_rows = 0 To uc_site_sel.dg_sites.NumRows - 1
        uc_site_sel.dg_sites.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = IIf(SelectAllItems, uc_grid.GRID_CHK_CHECKED, uc_grid.GRID_CHK_UNCHECKED)

        If Not String.IsNullOrEmpty(uc_site_sel.dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ISO_CODE).Value) Then
          Call PrintColorGridSiteCurrency(_idx_rows)
        Else
          Call PrintColorGridSiteNoCurrency(_idx_rows)
        End If

        uc_site_sel.dg_sites.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).BackColor = m_cell_enable_color
      Next
    End If

  End Sub ' SetDefaultValuesMulticurrency

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - Optional Param to set all items selected or unselected.
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub SetDefaultValues(Optional ByVal SelectAllItems As Boolean = True)
    Dim _idx_rows As Integer


    For _idx_rows = 0 To uc_site_sel.dg_sites.NumRows - 1
      uc_site_sel.dg_sites.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      uc_site_sel.dg_sites.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).BackColor = m_cell_enable_color

      If Not String.IsNullOrEmpty(uc_site_sel.dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ISO_CODE).Value) Then
        Call PrintColorGridSiteCurrency(_idx_rows)
      Else
        Call PrintColorGridSiteNoCurrency(_idx_rows)
      End If
    Next

    rb_multisite_currency.Checked = True

  End Sub ' SetDefaultValues

  ' PURPOSE: Check Filter, select at least one site
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean
  Public Function FilterCheckSites() As Boolean



    If m_currencies_values.Rows.Count <= 0 AndAlso rb_multisite_currency.Checked Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6347), ENUM_MB_TYPE.MB_TYPE_ERROR)
      Return False
    End If

    Call HasSitesSelectedWhenMultiSiteCurrencyChecked()

    If m_is_sites_checked = False Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1806), ENUM_MB_TYPE.MB_TYPE_ERROR)
      Return False
    End If

    Call HasSitesSelectedWithoutCurrency()

    If m_has_sites_without_currency Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(6285), ENUM_MB_TYPE.MB_TYPE_ERROR)
      Return False
    End If

    m_idsites_print = String.Empty


    Call WSI.Common.CurrencyMultisiteThread.RefreshData(m_date_from, m_date_to, Me, m_delegate)


    Return PrintSites()


  End Function ' FilterCheckSites

  ' PURPOSE: Return a String containing the list of the checked sites to print.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Public Function GetSitesIdListSelectedToPrint() As String

    Return m_idsites_print

  End Function ' GetSitesIdListSelectedToPrint

  ' PURPOSE: Return a String that contains the list of all sites selected.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String
  Public Function GetSitesIdListSelectedAll() As String

    Return uc_site_sel.GetSitesIdListSelectedAll()

  End Function ' GetSitesIdListSelectedAll

  ' PURPOSE: Get Site currency Average to show on the form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub PrintSiteCurrencyAverage()

    Dim _dt_group As DataTable
    Dim _wait_is_thread_alive As GetSiteCurrencyAverageCallback

    _dt_group = New DataTable()
    _dt_group = m_currencies_values

    If uc_multi_average_currency.InvokeRequired Then
      _wait_is_thread_alive = New GetSiteCurrencyAverageCallback(AddressOf PrintSiteCurrencyAverage)
      uc_multi_average_currency.Invoke(_wait_is_thread_alive)
    Else
      If _dt_group Is Nothing Then
        uc_multi_average_currency.CleanPanel()
      Else
        uc_multi_average_currency.RedrawCurrenciesValues(_dt_group, m_main_currency, m_display_mode)
        m_average_currency_list = Me.uc_multi_average_currency.GetAverageList()
      End If
    End If

  End Sub ' PrintSiteCurrencyAverage

  ' PURPOSE: DataTable with values to show
  '
  '  PARAMS:
  '     - INPUT:
  '           - AverageCurrency: DataTable with values filtered by dates
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub GetAverageSiteCurrencies(ByVal AverageCurrency As DataTable)
    Dim _dv As DataView
    'getting distinct values for group column
    Dim _dt_group As DataTable

    'If AverageCurrency.Rows.Count = 0 Then
    '  m_currencies_values = AverageCurrency
    'End If

    _dv = New DataView(AverageCurrency)
    _dt_group = _dv.ToTable(True, New String() {"ER_ISO_CODE"})

    'adding column for the row average
    _dt_group.Columns.Add("AVERAGE", GetType(Decimal))

    'looping thru distinct values for the group, counting
    For Each _dr As DataRow In _dt_group.Rows
      _dr("AVERAGE") = AverageCurrency.Compute("SUM(ER_CHANGE)/COUNT(ER_CHANGE)", "ER_ISO_CODE  = '" & _dr("ER_ISO_CODE") & "'")
    Next

    m_currencies_values = _dt_group

  End Sub ' GetAverageSiteCurrencies

  ' Metodo de escucha de eventos
  Public Function RedrawCurrenciesRate(ByVal DataTableThread As DataTable) As Boolean

    If Not DataTableThread Is Nothing Then
      GetAverageSiteCurrencies(DataTableThread)
      PrintSiteCurrencyAverage()
    End If
    Return True
  End Function ' RedrawCurrenciesRate

  ' PURPOSE: Obtain a string that shows the currency on excel sheet
  '
  '  PARAMS:
  '     - INPUT:
  '           - If is multicurrency or not
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - String with currency that shows
  Public Function GetTextCurrenciesSelected(ByVal currencyOption As String) As String
    Dim _text As String
    Dim _apply_changes As Boolean

    _apply_changes = (Not String.IsNullOrEmpty(currencyOption) AndAlso currencyOption.Equals("True"))
    _text = String.Empty

    If _apply_changes Then
      _text = m_main_currency
    Else
      _text = m_iso_code_selected & " (" & GLB_NLS_GUI_PLAYER_TRACKING.GetString(6283) & ")"
    End If

    Return _text
  End Function ' GetTextCurrenciesSelected

  ' PURPOSE: Get string with sites grouped by Iso-code
  '
  '  PARAMS:
  '     - INPUT:
  '           - Site that find his iso-code
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - string with sites grouped by Iso-code
  Public Function GetIsoCodeBySiteId(ByVal siteId As String) As String
    Dim _old_iso_code As String
    Dim _iso_code As String
    Dim _sites_with_iso_code As String

    Dim result() As DataRow = m_sites_detail.Select("ST_SITE_ID IN (" & siteId & ")", "SC_ISO_CODE ASC")

    _iso_code = String.Empty
    _old_iso_code = String.Empty
    _sites_with_iso_code = String.Empty

    For Each row As DataRow In result
      _iso_code = row(2)
      If Not _old_iso_code = _iso_code Then
        If String.IsNullOrEmpty(_sites_with_iso_code) Then
          _sites_with_iso_code = _sites_with_iso_code & row(2) & ": " & row(0)
        Else
          _sites_with_iso_code = _sites_with_iso_code & " ; " & row(2) & ": " & row(0)
        End If
      Else
        _sites_with_iso_code = _sites_with_iso_code & ", " & row(0)
      End If
      _old_iso_code = _iso_code
    Next

    Return _sites_with_iso_code
  End Function ' GetIsoCodeBySiteId


  ' PURPOSE: Get string with sites id and name 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - string with id and name sites 
  Public Function GetSitesIdAndNamesSelected() As String
    Dim _str_sites As String
    Dim _temp_string As String

    _str_sites = String.Empty

    For Each _temp_string In GetSitesIdListSelectedAll().Split(",")
      If m_sites_detail.Select("ST_SITE_ID = " & _temp_string).Length > 0 Then
        _str_sites = IIf(String.IsNullOrEmpty(_str_sites), _temp_string & " " & m_sites_detail.Select("ST_SITE_ID = " & _temp_string)(0)("ST_NAME"), _str_sites & "; " & _temp_string & " " & m_sites_detail.Select("ST_SITE_ID = " & _temp_string)(0)("ST_NAME"))
      End If
    Next

    Return _str_sites
  End Function


  Public Function GetSitesName() As String
    Return String.Empty
  End Function

#End Region

#Region " Private Functions "

  ' PURPOSE: Init routine of the control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub InitControls()

    Me.rb_multisite_currency.Checked = True
    Me.rb_site_currency.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6283)
    Me.uc_button_redraw.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6284)
    Me.gp_multi_currency_table.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6282)

    Me.uc_site_sel.gb_sites.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1803)
    Me.uc_site_sel.gb_sites.AutoSize = False
    Me.Width = 470
    Me.Height = 200
    Me.AutoSize = False
    Me.ShowMultisiteRow = False

    Me.pn_multi_currency.Height = 195
    Me.pn_multi_currency.Width = 480

    Me.gp_multi_currency_table.Width = 182
    Me.gp_multi_currency_table.Height = 161

    Me.uc_multi_average_currency.Width = 175
    Me.uc_multi_average_currency.Height = 75
    Me.uc_multi_average_currency.Location = New Point(1, Me.rb_multisite_currency.Location.Y + 20)
    Me.uc_multi_average_currency.VerticalScroll.SmallChange = 20
    Me.uc_multi_average_currency.VerticalScroll.LargeChange = 20

    Me.uc_button_redraw.Location = New Point(50, Me.uc_multi_average_currency.Height + Me.uc_multi_average_currency.Location.Y + 5)

    Me.uc_site_sel.dg_sites.IsToolTipped = True
    Me.uc_site_sel.dg_sites.Height = 132
    Me.uc_site_sel.gb_sites.Height = 161
    Me.uc_site_sel.Height = 164
    Me.uc_site_sel.btn_check_all.Visible = False
    Me.uc_site_sel.btn_uncheck_all.Visible = False

    Me.uc_site_sel.Location = New Point(gp_multi_currency_table.Width + 10, -4)
    Me.gp_multi_currency_table.Location = New Point(0, Me.uc_site_sel.Location.Y + 3)

    Call GUI_StyleSheetListSites()

    Call GetSitesList()

    m_main_currency = WSI.Common.CurrencyMultisite.GetMainCurrency()
    m_display_mode = WSI.Common.CurrencyMultisite.GetDisplayMode()
    m_is_sites_checked = False
    uc_button_redraw.Visible = False

    Me.rb_multisite_currency.Text = m_main_currency
    Me.rb_multisite_currency.Location = New Point(5, 40)
    Me.rb_site_currency.Location = New Point(5, 20)


    Call SetDefaultValuesMulticurrency(False)

    Call AddHandlers()

    If (WSI.Common.CurrencyMultisite.IsCenterAndMultiCurrency()) Then
      m_delegate = New WSI.Common.CurrencyMultisiteThread.CurrencyMultiSiteChangedHandler(AddressOf RedrawCurrenciesRate)
      RefreshChangeTable()
    End If

    m_average_currency_list = Me.uc_multi_average_currency.GetAverageList()

  End Sub ' InitControls

  ' PURPOSE: Get Sites list
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GetSitesList()

    Dim _sql_query As StringBuilder

    m_sites_detail = Nothing

    _sql_query = New StringBuilder
    _sql_query.AppendLine("   SELECT   ST_SITE_ID")
    _sql_query.AppendLine("           ,ST_NAME")
    _sql_query.AppendLine("           ,SC_ISO_CODE")
    _sql_query.AppendLine("     FROM   SITES")
    _sql_query.AppendLine("LEFT JOIN   SITE_CURRENCIES ")
    _sql_query.AppendLine("       ON   ST_SITE_ID = SC_SITE_ID")
    _sql_query.AppendLine("      AND   SC_TYPE = 1")
    _sql_query.AppendLine(" ORDER BY   ST_SITE_ID ")

    m_sites_detail = GUI_GetTableUsingSQL(_sql_query.ToString(), 5000)

    Me.uc_site_sel.GetSitesListPrint(m_sites_detail)

  End Sub ' GetSitesList

  ' PURPOSE: Define all Grid Event Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheetListSites()

    With Me.uc_site_sel.dg_sites
      Call .Init(GRID_2_COLUMNS, GRID_2_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' GRID_COL_CHECKBOX
      .Column(GRID_2_COLUMN_CHECKED).Header.Text = ""
      .Column(GRID_2_COLUMN_CHECKED).WidthFixed = 400
      .Column(GRID_2_COLUMN_CHECKED).Fixed = True
      .Column(GRID_2_COLUMN_CHECKED).Editable = True
      .Column(GRID_2_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_ID Hidden
      .Column(GRID_2_COLUMN_ID).Header.Text = ""
      .Column(GRID_2_COLUMN_ID).WidthFixed = 0

      ' GRID_COL_ID_FORMAT
      .Column(GRID_2_COLUMN_ID_FORMAT).Header.Text = ""
      .Column(GRID_2_COLUMN_ID_FORMAT).WidthFixed = 400
      .Column(GRID_2_COLUMN_ID_FORMAT).Fixed = True
      .Column(GRID_2_COLUMN_ID_FORMAT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER


      ' GRID_COL_ISO_CODE
      .Column(GRID_2_COLUMN_ISO_CODE).Header.Text = ""
      .Column(GRID_2_COLUMN_ISO_CODE).WidthFixed = 600
      .Column(GRID_2_COLUMN_ISO_CODE).Fixed = True
      .Column(GRID_2_COLUMN_ISO_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER


      ' GRID_COL_NAME
      .Column(GRID_2_COLUMN_NAME).Header.Text = ""
      .Column(GRID_2_COLUMN_NAME).WidthFixed = 2300
      .Column(GRID_2_COLUMN_NAME).Fixed = True
      .Column(GRID_2_COLUMN_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    End With

  End Sub 'GUI_StyleSheetType

  ' PURPOSE: Print multi currency average panel
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub RedrawValues()

    If m_display_mode Then
      m_display_mode = False
    Else
      m_display_mode = True
    End If
    uc_multi_average_currency.RedrawCurrenciesValues(m_currencies_values, m_main_currency, m_display_mode)
    m_average_currency_list = Me.uc_multi_average_currency.GetAverageList()

  End Sub ' RedrawValues

  ' PURPOSE: Check if exist any problem to print results
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub HasSitesSelectedWithoutCurrency()

    If rb_site_currency.Checked Then
      m_has_sites_without_currency = False
    Else
      Dim _idx_rows As Integer
      m_is_sites_checked = False
      For _idx_rows = 0 To uc_site_sel.dg_sites.NumRows - 1
        If uc_site_sel.dg_sites.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
          m_is_sites_checked = True

          If String.IsNullOrEmpty(uc_site_sel.dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ISO_CODE).Value) Then
            m_has_sites_without_currency = True

            Exit For
          Else
            m_has_sites_without_currency = False
          End If
        End If
      Next
    End If

  End Sub ' HasSitesSelectedWithoutCurrency

  ' PURPOSE: Check if not selected any sites
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub HasSitesSelectedWhenMultiSiteCurrencyChecked()

    If rb_multisite_currency.Checked Then
      Dim _idx_rows As Integer
      m_is_sites_checked = False
      For _idx_rows = 0 To uc_site_sel.dg_sites.NumRows - 1
        If uc_site_sel.dg_sites.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
          m_is_sites_checked = True
          Exit For
        End If
      Next
    Else
      m_is_sites_checked = True
    End If

  End Sub ' HasSitesSelectedWhenMultiSiteCurrencyChecked

  ' PURPOSE: Print grid cells with red when site don't have currency defined
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub PrintColorGridSiteNoCurrency(ByVal rowNumber As Integer)
    uc_site_sel.dg_sites.Cell(rowNumber, GRID_2_COLUMN_ISO_CODE).BackColor = m_cell_no_currency
    uc_site_sel.dg_sites.Cell(rowNumber, GRID_2_COLUMN_ID_FORMAT).BackColor = m_cell_no_currency
    uc_site_sel.dg_sites.Cell(rowNumber, GRID_2_COLUMN_NAME).BackColor = m_cell_no_currency
    uc_site_sel.dg_sites.Cell(rowNumber, GRID_2_COLUMN_ISO_CODE).ForeColor = m_cell_enable_color
    uc_site_sel.dg_sites.Cell(rowNumber, GRID_2_COLUMN_ID_FORMAT).ForeColor = m_cell_enable_color
    uc_site_sel.dg_sites.Cell(rowNumber, GRID_2_COLUMN_NAME).ForeColor = m_cell_enable_color
  End Sub ' PrintColorGridSiteNoCurrency

  ' PURPOSE: Print grid cells with white when site have currency defined
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub PrintColorGridSiteCurrency(ByVal rowNumber As Integer)
    uc_site_sel.dg_sites.Cell(rowNumber, GRID_2_COLUMN_ISO_CODE).BackColor = m_cell_enable_color
    uc_site_sel.dg_sites.Cell(rowNumber, GRID_2_COLUMN_ID_FORMAT).BackColor = m_cell_enable_color
    uc_site_sel.dg_sites.Cell(rowNumber, GRID_2_COLUMN_NAME).BackColor = m_cell_enable_color
    uc_site_sel.dg_sites.Cell(rowNumber, GRID_2_COLUMN_ISO_CODE).ForeColor = m_cell_text_enable_color
    uc_site_sel.dg_sites.Cell(rowNumber, GRID_2_COLUMN_ID_FORMAT).ForeColor = m_cell_text_enable_color
    uc_site_sel.dg_sites.Cell(rowNumber, GRID_2_COLUMN_NAME).ForeColor = m_cell_text_enable_color
  End Sub ' PrintColorGridSiteCurrency

  ' PURPOSE: Redefine highlight on grid in function site currency
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub RowSelected()
    Dim _idx_rows As Integer
    Dim _row_selected As Integer


    _row_selected = Me.uc_site_sel.dg_sites.CurrentRow
    For _idx_rows = 0 To uc_site_sel.dg_sites.NumRows - 1
      If _row_selected = _idx_rows Then
        If uc_site_sel.dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ISO_CODE).Value.Length <= 0 Then
          Me.uc_site_sel.dg_sites.Column(GRID_2_COLUMN_ISO_CODE).HighLightWhenSelected = False
        Else
          Me.uc_site_sel.dg_sites.Column(GRID_2_COLUMN_ISO_CODE).HighLightWhenSelected = True
        End If
        If rb_site_currency.Checked Then
          m_iso_code_selected = uc_site_sel.dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ISO_CODE).Value.ToString()
        End If
        Exit For
      End If
    Next

  End Sub ' RowSelected

  ' PURPOSE: Obtain the number of rows selected
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GetNumRowsSelected()
    Dim _idx_rows As Integer
    Dim _row_selected As Integer

    m_num_rows_checked = 0

    _row_selected = Me.uc_site_sel.dg_sites.CurrentRow
    For _idx_rows = 0 To uc_site_sel.dg_sites.NumRows - 1
      If uc_site_sel.dg_sites.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        m_num_rows_checked = m_num_rows_checked + 1
      End If
    Next
  End Sub ' GetNumRowsSelected

  ' PURPOSE: Print Sites that shows in the excel sheet
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function PrintSites() As Boolean
    Dim _idx_rows As Integer
    Dim _all_unchecked As Boolean
    Dim _all_checked As Boolean
    Dim _site_id As String

    _all_unchecked = True
    _all_checked = True

    For _idx_rows = 0 To uc_site_sel.dg_sites.NumRows - 1
      If uc_site_sel.dg_sites.Cell(_idx_rows, GRID_2_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _all_unchecked = False
        _site_id = uc_site_sel.dg_sites.Cell(_idx_rows, GRID_2_COLUMN_ID).Value

        If m_sites_detail.Select("ST_SITE_ID = " & _site_id).Length > 0 Then
          If String.IsNullOrEmpty(m_idsites_print) Then
            m_idsites_print = _site_id
          Else
            m_idsites_print = m_idsites_print & "," & _site_id
          End If
        End If

      Else
        _all_checked = False
      End If
    Next

    m_id_sites_checked = m_idsites_print

    If _all_checked Then
      m_idsites_print = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1807)
    End If

    If rb_multisite_currency.Checked Then
      m_idsites_print = GetIsoCodeBySiteId(m_id_sites_checked)
    End If

    If rb_multisite_currency.Checked Then

      Return True
    End If

    If _all_unchecked Then
      If m_multiselect Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1806), ENUM_MB_TYPE.MB_TYPE_ERROR)
      Else
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4921), ENUM_MB_TYPE.MB_TYPE_ERROR)
      End If

      Return False
    Else

      Return True
    End If

  End Function ' PrintSites

  Private Sub AddHandlers()
    AddHandler uc_site_sel.RowSelectedEvent, AddressOf RowSelected
  End Sub ' AddHandlers

  ' PURPOSE: Refresh change Table (uc_multi_avarage_currency)
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub RefreshChangeTable()
    If (WSI.Common.CurrencyMultisite.IsCenterAndMultiCurrency()) Then
      WSI.Common.CurrencyMultisiteThread.RefreshData(m_date_from, m_date_to, Me, m_delegate)
    End If ' RefreshChangeTable
  End Sub

#End Region 'Private Functions 

#Region "Events"

  Private Sub uc_button_redraw_ClickEvent() Handles uc_button_redraw.ClickEvent
    Call RedrawValues()
  End Sub ' uc_button_redraw_ClickEvent

  Private Sub rb_site_currency_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_site_currency.CheckedChanged
    Call SetDefaultValuesMulticurrency(False)
    Me.uc_site_sel.MultiSelect = False
    m_is_grid_apply_changes = False
    Me.uc_site_sel.ShowIsoCode = True
    Me.uc_site_sel.gb_sites.Height = 161
    Me.uc_site_sel.dg_sites.Height = 132
    Me.uc_site_sel.Height = 165
    Me.uc_multi_average_currency.Enabled = False
  End Sub ' rb_site_currency_CheckedChanged

  Private Sub rb_multisite_currency_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_multisite_currency.CheckedChanged
    Call SetDefaultValuesMulticurrency(False)
    Me.uc_site_sel.MultiSelect = True
    Me.uc_site_sel.ShowIsoCode = True
    Me.uc_site_sel.btn_check_all.Visible = False
    Me.uc_site_sel.btn_uncheck_all.Visible = False
    m_is_grid_apply_changes = True
    Me.uc_multi_average_currency.Enabled = True
  End Sub ' rb_multisite_currency_CheckedChanged

#End Region ' Events

End Class
