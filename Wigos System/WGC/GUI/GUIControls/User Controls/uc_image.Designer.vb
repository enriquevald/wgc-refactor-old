<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_image
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.lbl_size_img = New System.Windows.Forms.Label
    Me.pnl_image = New System.Windows.Forms.Panel
    Me.btn_delete_img = New GUI_Controls.uc_button
    Me.btn_select_img = New GUI_Controls.uc_button
    Me.SuspendLayout()
    '
    'lbl_size_img
    '
    Me.lbl_size_img.AutoSize = True
    Me.lbl_size_img.Location = New System.Drawing.Point(3, 0)
    Me.lbl_size_img.Name = "lbl_size_img"
    Me.lbl_size_img.Size = New System.Drawing.Size(32, 13)
    Me.lbl_size_img.TabIndex = 9
    Me.lbl_size_img.Text = "xSize"
    '
    'pnl_image
    '
    Me.pnl_image.BackColor = System.Drawing.SystemColors.Control
    Me.pnl_image.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
    Me.pnl_image.ForeColor = System.Drawing.SystemColors.ControlText
    Me.pnl_image.Location = New System.Drawing.Point(0, 16)
    Me.pnl_image.Margin = New System.Windows.Forms.Padding(0)
    Me.pnl_image.Name = "pnl_image"
    Me.pnl_image.Size = New System.Drawing.Size(160, 120)
    Me.pnl_image.TabIndex = 0
    '
    'btn_delete_img
    '
    Me.btn_delete_img.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_delete_img.Location = New System.Drawing.Point(14, 141)
    Me.btn_delete_img.Margin = New System.Windows.Forms.Padding(0)
    Me.btn_delete_img.Name = "btn_delete_img"
    Me.btn_delete_img.Size = New System.Drawing.Size(63, 21)
    Me.btn_delete_img.TabIndex = 10
    Me.btn_delete_img.ToolTipped = False
    Me.btn_delete_img.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SMALL
    '
    'btn_select_img
    '
    Me.btn_select_img.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_select_img.Location = New System.Drawing.Point(85, 141)
    Me.btn_select_img.Margin = New System.Windows.Forms.Padding(0)
    Me.btn_select_img.Name = "btn_select_img"
    Me.btn_select_img.Size = New System.Drawing.Size(63, 21)
    Me.btn_select_img.TabIndex = 1
    Me.btn_select_img.ToolTipped = False
    Me.btn_select_img.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.SMALL
    '
    'uc_image
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.AutoSize = True
    Me.Controls.Add(Me.btn_delete_img)
    Me.Controls.Add(Me.pnl_image)
    Me.Controls.Add(Me.lbl_size_img)
    Me.Controls.Add(Me.btn_select_img)
    Me.Margin = New System.Windows.Forms.Padding(0)
    Me.Name = "uc_image"
    Me.Size = New System.Drawing.Size(160, 168)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents btn_select_img As GUI_Controls.uc_button
  Friend WithEvents lbl_size_img As System.Windows.Forms.Label
  Friend WithEvents pnl_image As System.Windows.Forms.Panel
  Friend WithEvents btn_delete_img As GUI_Controls.uc_button

End Class
