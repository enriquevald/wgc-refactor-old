﻿'-------------------------------------------------------------------
' Copyright © 2017 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   uc_jackpot_award_status_sel.vb
' DESCRIPTION:   List Jackpot Award Status
' AUTHOR:        Enric Tomas Parisi
' CREATION DATE: 26-JUN-2017
'
' REVISION HISTORY:
'
' Date         Author  Description
' -----------  ------  -----------------------------------------------
' 26-jun-2017   ETP     Initial version
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports WSI.Common.Jackpot.Jackpot
Imports WSI.Common.Jackpot
Imports System.Text
Imports System.Windows.Forms

Public Class uc_jackpot_award_status_sel

#Region " Members "

#End Region ' Members

#Region "Properties"

#End Region ' Properties 

#Region " Constructor "

  Public Sub New()

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.
    Call InitControl()

  End Sub ' New

#End Region ' Constructor 

#Region " Public "

  ''' <summary>
  ''' Init control
  ''' </summary>
  ''' <remarks></remarks>
  Public Sub Init()
    Call InitControl()
  End Sub ' Init

  ''' <summary>
  ''' Set Default Values 
  ''' </summary>
  ''' <param name="Check"></param>
  ''' <remarks></remarks>
  Public Sub SetDefaultValues(Check As Boolean)

    Dim _chk As CheckBox

    For Each _ctrl As Control In Me.gb_jackpot_award_status.Controls

      If TypeOf _ctrl Is CheckBox Then

        _chk = _ctrl
        _chk.Checked = Check

      End If

    Next _ctrl

  End Sub

  ''' <summary>
  ''' Get current sql filter
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetCurrentFilter() As String
    Dim _status As StringBuilder
    Dim _count As Int32

    _status = New StringBuilder()

    _count = GetStatusCount()

    If (_count = 0 Or _count = gb_jackpot_award_status.Controls.Count) Then

      Return GLB_NLS_GUI_CONTROLS.GetString(276)
    End If

    If (ch_pending.Checked) Then
      _status.Append(IIf(_status.Length = 0, "", ", "))
      _status.Append(ch_pending.Text)
    End If

    If (ch_cantaward.Checked) Then
      _status.Append(IIf(_status.Length = 0, "", ", "))
      _status.Append(ch_cantaward.Text)

    End If

    If (ch_award.Checked) Then
      _status.Append(IIf(_status.Length = 0, "", ", "))
      _status.Append(ch_award.Text)
    End If

    If (ch_without.Checked) Then
      _status.Append(IIf(_status.Length = 0, "", ", "))
      _status.Append(ch_without.Text)
    End If

    If (ch_canceled.Checked) Then
      _status.Append(IIf(_status.Length = 0, "", ", "))
      _status.Append(ch_canceled.Text)
    End If

    If (ch_done.Checked) Then
      _status.Append(IIf(_status.Length = 0, "", ", "))
      _status.Append(ch_canceled.Text)
    End If

    Return _status.ToString()

  End Function ' GetCurrentFilter

  ''' <summary>
  ''' Get Sql Conditions
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Public Function GetSqlWhere() As String

    Dim _sql_where As StringBuilder
    Dim _count As Int32

    _sql_where = New StringBuilder()

    _count = GetStatusCount()

    If (_count = 0 Or _count = gb_jackpot_award_status.Controls.Count) Then

      Return String.Empty
    End If


    If (_count = 1) Then

      _sql_where.Append(StatusCheckedOne())
    Else

      _sql_where.Append(String.Format("AND   JAD.JAD_STATUS & {0} > 0 ", StatusChecked()))
    End If

    Return _sql_where.ToString

  End Function ' GetSqlWhere


#End Region ' Public 

#Region " Private "

  ''' <summary>
  ''' Init control Resources
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub InitControl()

    ' Prevent design errors.
    If System.Reflection.Assembly.GetEntryAssembly() Is Nothing Then
      Return
    End If

    gb_jackpot_award_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7955)
    ch_pending.Text = JackpotAwardEvents.AwardStatusToString(JackpotAwardEvents.JackpotStatusFlag.Pending)
    ch_cantaward.Text = JackpotAwardEvents.AwardStatusToString(JackpotAwardEvents.JackpotStatusFlag.CantBeAwarded)
    ch_award.Text = JackpotAwardEvents.AwardStatusToString(JackpotAwardEvents.JackpotStatusFlag.Awarded)
    ch_without.Text = JackpotAwardEvents.AwardStatusToString(JackpotAwardEvents.JackpotStatusFlag.AwardedWithoutConditions)
    ch_canceled.Text = JackpotAwardEvents.AwardStatusToString(JackpotAwardEvents.JackpotStatusFlag.Cancelled)
    ch_done.Text = JackpotAwardEvents.AwardStatusToString(JackpotAwardEvents.JackpotStatusFlag.Done)

  End Sub

  ''' <summary>
  ''' Get status checked.
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function StatusChecked() As Int32

    Dim _status As Int32

    _status = JackpotAwardEvents.JackpotStatusFlag.None


    If (ch_pending.Checked) Then
      _status = JackpotBusinessLogic.UpdateBitmask(_status, JackpotAwardEvents.JackpotStatusFlag.Pending, True)
    End If

    If (ch_cantaward.Checked) Then
      _status = JackpotBusinessLogic.UpdateBitmask(_status, JackpotAwardEvents.JackpotStatusFlag.CantBeAwarded, True)
    End If

    If (ch_award.Checked) Then
      _status = JackpotBusinessLogic.UpdateBitmask(_status, JackpotAwardEvents.JackpotStatusFlag.Awarded, True)
    End If

    If (ch_without.Checked) Then
      _status = JackpotBusinessLogic.UpdateBitmask(_status, JackpotAwardEvents.JackpotStatusFlag.AwardedWithoutConditions, True)
    End If

    If (ch_canceled.Checked) Then
      _status = JackpotBusinessLogic.UpdateBitmask(_status, JackpotAwardEvents.JackpotStatusFlag.Cancelled, True)
    End If

    If (ch_done.Checked) Then
      _status = JackpotBusinessLogic.UpdateBitmask(_status, JackpotAwardEvents.JackpotStatusFlag.Done, True)
    End If

    Return _status

  End Function


  Private Function StatusCheckedOne() As String

    Dim _sql_where As StringBuilder
    Dim _status As Int32
    _sql_where = New StringBuilder

    If (ch_pending.Checked) Then

      _sql_where.Append(String.Format("AND   JAD.JAD_STATUS = {0} ", JackpotAwardEvents.JackpotStatusFlag.Pending.GetHashCode.ToString))

    ElseIf (ch_cantaward.Checked) Then

      _sql_where.Append(String.Format("AND   JAD.JAD_STATUS & {0} > 0 ", JackpotAwardEvents.JackpotStatusFlag.CantBeAwarded.GetHashCode.ToString))

    ElseIf (ch_award.Checked) Then

      _status = JackpotBusinessLogic.UpdateBitmask(JackpotAwardEvents.JackpotStatusFlag.Pending, JackpotAwardEvents.JackpotStatusFlag.Awarded, True)
      _sql_where.Append(String.Format("AND   JAD.JAD_STATUS = {0} ", _status))

    ElseIf (ch_without.Checked) Then

      _status = JackpotBusinessLogic.UpdateBitmask(JackpotAwardEvents.JackpotStatusFlag.Pending, JackpotAwardEvents.JackpotStatusFlag.AwardedWithoutConditions, True)
      _sql_where.Append(String.Format("AND   JAD.JAD_STATUS = {0} ", _status))

    ElseIf (ch_canceled.Checked) Then

      _sql_where.Append(String.Format("AND   JAD.JAD_STATUS & {0} > 0 ", JackpotAwardEvents.JackpotStatusFlag.Cancelled.GetHashCode.ToString))

    ElseIf (ch_done.Checked) Then
      _sql_where.Append(String.Format("AND   JAD.JAD_STATUS & {0} > 0 ", JackpotAwardEvents.JackpotStatusFlag.Done.GetHashCode.ToString))
    End If

    Return _sql_where.ToString

  End Function

  ''' <summary>
  ''' Count status
  ''' </summary>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function GetStatusCount() As Int32

    Dim _chk As CheckBox
    Dim _count As Int32

    _count = 0

    For Each _ctrl As Control In Me.gb_jackpot_award_status.Controls

      If TypeOf _ctrl Is CheckBox Then

        _chk = _ctrl
        If _chk.Checked Then
          _count += 1
        End If

      End If

    Next _ctrl

    Return _count

  End Function

#End Region



End Class
