<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_multi_currency_sel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.rb_site_currency = New System.Windows.Forms.RadioButton
    Me.rb_multisite_currency = New System.Windows.Forms.RadioButton
    Me.pn_multi_currency = New System.Windows.Forms.Panel
    Me.gp_multi_currency_table = New System.Windows.Forms.GroupBox
    Me.uc_multi_average_currency = New GUI_Controls.uc_multi_average_currency
    Me.uc_button_redraw = New GUI_Controls.uc_button
    Me.uc_site_sel = New GUI_Controls.uc_sites_sel
    Me.pn_multi_currency.SuspendLayout()
    Me.gp_multi_currency_table.SuspendLayout()
    Me.SuspendLayout()
    '
    'rb_site_currency
    '
    Me.rb_site_currency.AutoSize = True
    Me.rb_site_currency.Location = New System.Drawing.Point(22, 19)
    Me.rb_site_currency.Name = "rb_site_currency"
    Me.rb_site_currency.Size = New System.Drawing.Size(133, 17)
    Me.rb_site_currency.TabIndex = 0
    Me.rb_site_currency.TabStop = True
    Me.rb_site_currency.Text = "Moneda Nacional Sala"
    Me.rb_site_currency.UseVisualStyleBackColor = True
    '
    'rb_multisite_currency
    '
    Me.rb_multisite_currency.AutoSize = True
    Me.rb_multisite_currency.Location = New System.Drawing.Point(22, 42)
    Me.rb_multisite_currency.Name = "rb_multisite_currency"
    Me.rb_multisite_currency.Size = New System.Drawing.Size(124, 17)
    Me.rb_multisite_currency.TabIndex = 1
    Me.rb_multisite_currency.TabStop = True
    Me.rb_multisite_currency.Text = "rb_multisite_currency"
    Me.rb_multisite_currency.UseVisualStyleBackColor = True
    '
    'pn_multi_currency
    '
    Me.pn_multi_currency.BackColor = System.Drawing.SystemColors.Control
    Me.pn_multi_currency.Controls.Add(Me.gp_multi_currency_table)
    Me.pn_multi_currency.Controls.Add(Me.uc_site_sel)
    Me.pn_multi_currency.Location = New System.Drawing.Point(3, 3)
    Me.pn_multi_currency.Name = "pn_multi_currency"
    Me.pn_multi_currency.Size = New System.Drawing.Size(603, 271)
    Me.pn_multi_currency.TabIndex = 1
    '
    'gp_multi_currency_table
    '
    Me.gp_multi_currency_table.BackColor = System.Drawing.SystemColors.Control
    Me.gp_multi_currency_table.Controls.Add(Me.rb_site_currency)
    Me.gp_multi_currency_table.Controls.Add(Me.rb_multisite_currency)
    Me.gp_multi_currency_table.Controls.Add(Me.uc_multi_average_currency)
    Me.gp_multi_currency_table.Controls.Add(Me.uc_button_redraw)
    Me.gp_multi_currency_table.Location = New System.Drawing.Point(0, 21)
    Me.gp_multi_currency_table.Name = "gp_multi_currency_table"
    Me.gp_multi_currency_table.Size = New System.Drawing.Size(322, 231)
    Me.gp_multi_currency_table.TabIndex = 0
    Me.gp_multi_currency_table.TabStop = False
    '
    'uc_multi_average_currency
    '
    Me.uc_multi_average_currency.AutoScroll = True
    Me.uc_multi_average_currency.BackColor = System.Drawing.SystemColors.Control
    Me.uc_multi_average_currency.Location = New System.Drawing.Point(6, 63)
    Me.uc_multi_average_currency.Name = "uc_multi_average_currency"
    Me.uc_multi_average_currency.Size = New System.Drawing.Size(256, 131)
    Me.uc_multi_average_currency.TabIndex = 10
    '
    'uc_button_redraw
    '
    Me.uc_button_redraw.BackColor = System.Drawing.SystemColors.Control
    Me.uc_button_redraw.DialogResult = System.Windows.Forms.DialogResult.None
    Me.uc_button_redraw.Location = New System.Drawing.Point(65, 194)
    Me.uc_button_redraw.Name = "uc_button_redraw"
    Me.uc_button_redraw.Size = New System.Drawing.Size(90, 30)
    Me.uc_button_redraw.TabIndex = 11
    Me.uc_button_redraw.TabStop = False
    Me.uc_button_redraw.ToolTipped = False
    Me.uc_button_redraw.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'uc_site_sel
    '
    Me.uc_site_sel.BackColor = System.Drawing.SystemColors.Control
    Me.uc_site_sel.Location = New System.Drawing.Point(347, 20)
    Me.uc_site_sel.MultiSelect = False
    Me.uc_site_sel.Name = "uc_site_sel"
    Me.uc_site_sel.ShowIsoCode = False
    Me.uc_site_sel.ShowMultisiteRow = False
    Me.uc_site_sel.Size = New System.Drawing.Size(245, 170)
    Me.uc_site_sel.TabIndex = 1
    '
    'uc_multi_currency_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.BackColor = System.Drawing.SystemColors.Control
    Me.Controls.Add(Me.pn_multi_currency)
    Me.Name = "uc_multi_currency_sel"
    Me.Size = New System.Drawing.Size(629, 284)
    Me.pn_multi_currency.ResumeLayout(False)
    Me.gp_multi_currency_table.ResumeLayout(False)
    Me.gp_multi_currency_table.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents rb_site_currency As System.Windows.Forms.RadioButton
  Friend WithEvents rb_multisite_currency As System.Windows.Forms.RadioButton
  Friend WithEvents pn_multi_currency As System.Windows.Forms.Panel
  Friend WithEvents gp_multi_currency_table As System.Windows.Forms.GroupBox
  Friend WithEvents uc_multi_average_currency As GUI_Controls.uc_multi_average_currency
  Friend WithEvents uc_button_redraw As GUI_Controls.uc_button
  Friend WithEvents uc_site_sel As GUI_Controls.uc_sites_sel

End Class
