<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_terminals_group_filter
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Me.tool_tip = New System.Windows.Forms.ToolTip(Me.components)
    Me.gb_provider = New System.Windows.Forms.GroupBox
    Me.tlp_controls = New System.Windows.Forms.TableLayoutPanel
    Me.btn_collapse = New System.Windows.Forms.Button
    Me.tr_group_list = New GUI_Controls.uc_treeview_three_state
    Me.cmb_provider_list_type = New GUI_Controls.uc_combo
    Me.tlp_controls.SuspendLayout()
    Me.SuspendLayout()
    '
    'gb_provider
    '
    Me.gb_provider.Dock = System.Windows.Forms.DockStyle.Fill
    Me.gb_provider.ForeColor = System.Drawing.SystemColors.ControlText
    Me.gb_provider.Location = New System.Drawing.Point(0, 0)
    Me.gb_provider.Name = "gb_provider"
    Me.gb_provider.Size = New System.Drawing.Size(250, 173)
    Me.gb_provider.TabIndex = 1
    Me.gb_provider.TabStop = False
    Me.gb_provider.Text = "xList"
    '
    'tlp_controls
    '
    Me.tlp_controls.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.tlp_controls.ColumnCount = 2
    Me.tlp_controls.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
    Me.tlp_controls.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
    Me.tlp_controls.Controls.Add(Me.btn_collapse, 1, 0)
    Me.tlp_controls.Controls.Add(Me.tr_group_list, 0, 1)
    Me.tlp_controls.Controls.Add(Me.cmb_provider_list_type, 0, 0)
    Me.tlp_controls.Location = New System.Drawing.Point(6, 17)
    Me.tlp_controls.Margin = New System.Windows.Forms.Padding(0)
    Me.tlp_controls.Name = "tlp_controls"
    Me.tlp_controls.RowCount = 2
    Me.tlp_controls.RowStyles.Add(New System.Windows.Forms.RowStyle)
    Me.tlp_controls.RowStyles.Add(New System.Windows.Forms.RowStyle)
    Me.tlp_controls.Size = New System.Drawing.Size(237, 147)
    Me.tlp_controls.TabIndex = 4
    '
    'btn_collapse
    '
    Me.btn_collapse.Dock = System.Windows.Forms.DockStyle.Fill
    Me.btn_collapse.Location = New System.Drawing.Point(212, 0)
    Me.btn_collapse.Margin = New System.Windows.Forms.Padding(0, 0, 0, 3)
    Me.btn_collapse.Name = "btn_collapse"
    Me.btn_collapse.Size = New System.Drawing.Size(25, 24)
    Me.btn_collapse.TabIndex = 3
    Me.btn_collapse.Text = "+"
    Me.btn_collapse.UseVisualStyleBackColor = True
    '
    'tr_group_list
    '
    Me.tlp_controls.SetColumnSpan(Me.tr_group_list, 2)
    Me.tr_group_list.Dock = System.Windows.Forms.DockStyle.Fill
    Me.tr_group_list.Location = New System.Drawing.Point(0, 27)
    Me.tr_group_list.Margin = New System.Windows.Forms.Padding(0)
    Me.tr_group_list.Name = "tr_group_list"
    Me.tr_group_list.ShowCheckBoxes = True
    Me.tr_group_list.Size = New System.Drawing.Size(237, 156)
    Me.tr_group_list.TabIndex = 4
    Me.tr_group_list.TriStateStyleProperty = GUI_Controls.uc_treeview_three_state.TRISTATESTYLES.STANDARD
    '
    'cmb_provider_list_type
    '
    Me.cmb_provider_list_type.AllowUnlistedValues = False
    Me.cmb_provider_list_type.AutoCompleteMode = False
    Me.cmb_provider_list_type.Dock = System.Windows.Forms.DockStyle.Fill
    Me.cmb_provider_list_type.IsReadOnly = False
    Me.cmb_provider_list_type.Location = New System.Drawing.Point(0, 0)
    Me.cmb_provider_list_type.Margin = New System.Windows.Forms.Padding(0, 0, 0, 3)
    Me.cmb_provider_list_type.Name = "cmb_provider_list_type"
    Me.cmb_provider_list_type.SelectedIndex = -1
    Me.cmb_provider_list_type.Size = New System.Drawing.Size(212, 24)
    Me.cmb_provider_list_type.SufixText = "Sufix Text"
    Me.cmb_provider_list_type.SufixTextVisible = True
    Me.cmb_provider_list_type.TabIndex = 2
    Me.cmb_provider_list_type.TextWidth = 0
    '
    'uc_terminals_group_filter
    '
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
    Me.Controls.Add(Me.tlp_controls)
    Me.Controls.Add(Me.gb_provider)
    Me.Name = "uc_terminals_group_filter"
    Me.Size = New System.Drawing.Size(250, 173)
    Me.tlp_controls.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents tool_tip As System.Windows.Forms.ToolTip
  Friend WithEvents gb_provider As System.Windows.Forms.GroupBox
  Friend WithEvents tlp_controls As System.Windows.Forms.TableLayoutPanel
  Friend WithEvents btn_collapse As System.Windows.Forms.Button
  Friend WithEvents tr_group_list As GUI_Controls.uc_treeview_three_state
  Friend WithEvents cmb_provider_list_type As GUI_Controls.uc_combo

End Class
