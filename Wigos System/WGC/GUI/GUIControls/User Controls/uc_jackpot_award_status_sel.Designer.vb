﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class uc_jackpot_award_status_sel
  Inherits System.Windows.Forms.UserControl

  'UserControl overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.ch_award = New System.Windows.Forms.CheckBox()
    Me.ch_pending = New System.Windows.Forms.CheckBox()
    Me.ch_without = New System.Windows.Forms.CheckBox()
    Me.ch_canceled = New System.Windows.Forms.CheckBox()
    Me.gb_jackpot_award_status = New System.Windows.Forms.GroupBox()
    Me.ch_done = New System.Windows.Forms.CheckBox()
    Me.ch_cantaward = New System.Windows.Forms.CheckBox()
    Me.gb_jackpot_award_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'ch_award
    '
    Me.ch_award.AutoSize = True
    Me.ch_award.Location = New System.Drawing.Point(6, 63)
    Me.ch_award.Name = "ch_award"
    Me.ch_award.Size = New System.Drawing.Size(61, 17)
    Me.ch_award.TabIndex = 2
    Me.ch_award.Text = "xAward"
    Me.ch_award.UseVisualStyleBackColor = True
    '
    'ch_pending
    '
    Me.ch_pending.AutoSize = True
    Me.ch_pending.Location = New System.Drawing.Point(6, 19)
    Me.ch_pending.Name = "ch_pending"
    Me.ch_pending.Size = New System.Drawing.Size(70, 17)
    Me.ch_pending.TabIndex = 0
    Me.ch_pending.Text = "xPending"
    Me.ch_pending.UseVisualStyleBackColor = True
    '
    'ch_without
    '
    Me.ch_without.AutoSize = True
    Me.ch_without.Location = New System.Drawing.Point(6, 85)
    Me.ch_without.Name = "ch_without"
    Me.ch_without.Size = New System.Drawing.Size(98, 17)
    Me.ch_without.TabIndex = 3
    Me.ch_without.Text = "xAwardWithout"
    Me.ch_without.UseVisualStyleBackColor = True
    '
    'ch_canceled
    '
    Me.ch_canceled.AutoSize = True
    Me.ch_canceled.Location = New System.Drawing.Point(6, 107)
    Me.ch_canceled.Name = "ch_canceled"
    Me.ch_canceled.Size = New System.Drawing.Size(76, 17)
    Me.ch_canceled.TabIndex = 4
    Me.ch_canceled.Text = "xCanceled"
    Me.ch_canceled.UseVisualStyleBackColor = True
    '
    'gb_jackpot_award_status
    '
    Me.gb_jackpot_award_status.Controls.Add(Me.ch_done)
    Me.gb_jackpot_award_status.Controls.Add(Me.ch_cantaward)
    Me.gb_jackpot_award_status.Controls.Add(Me.ch_award)
    Me.gb_jackpot_award_status.Controls.Add(Me.ch_canceled)
    Me.gb_jackpot_award_status.Controls.Add(Me.ch_pending)
    Me.gb_jackpot_award_status.Controls.Add(Me.ch_without)
    Me.gb_jackpot_award_status.Dock = System.Windows.Forms.DockStyle.Fill
    Me.gb_jackpot_award_status.Location = New System.Drawing.Point(0, 0)
    Me.gb_jackpot_award_status.Name = "gb_jackpot_award_status"
    Me.gb_jackpot_award_status.Size = New System.Drawing.Size(123, 156)
    Me.gb_jackpot_award_status.TabIndex = 0
    Me.gb_jackpot_award_status.TabStop = False
    Me.gb_jackpot_award_status.Text = "xJackpotStatus"
    '
    'ch_done
    '
    Me.ch_done.AutoSize = True
    Me.ch_done.Location = New System.Drawing.Point(6, 129)
    Me.ch_done.Name = "ch_done"
    Me.ch_done.Size = New System.Drawing.Size(57, 17)
    Me.ch_done.TabIndex = 5
    Me.ch_done.Text = "xDone"
    Me.ch_done.UseVisualStyleBackColor = True
    '
    'ch_cantaward
    '
    Me.ch_cantaward.AutoSize = True
    Me.ch_cantaward.Location = New System.Drawing.Point(6, 41)
    Me.ch_cantaward.Name = "ch_cantaward"
    Me.ch_cantaward.Size = New System.Drawing.Size(83, 17)
    Me.ch_cantaward.TabIndex = 1
    Me.ch_cantaward.Text = "xCantAward"
    Me.ch_cantaward.UseVisualStyleBackColor = True
    '
    'uc_jackpot_award_status_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.Controls.Add(Me.gb_jackpot_award_status)
    Me.Name = "uc_jackpot_award_status_sel"
    Me.Size = New System.Drawing.Size(123, 156)
    Me.gb_jackpot_award_status.ResumeLayout(False)
    Me.gb_jackpot_award_status.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ch_award As System.Windows.Forms.CheckBox
  Friend WithEvents ch_pending As System.Windows.Forms.CheckBox
  Friend WithEvents ch_without As System.Windows.Forms.CheckBox
  Friend WithEvents ch_canceled As System.Windows.Forms.CheckBox
  Friend WithEvents gb_jackpot_award_status As System.Windows.Forms.GroupBox
  Friend WithEvents ch_cantaward As System.Windows.Forms.CheckBox
  Friend WithEvents ch_done As System.Windows.Forms.CheckBox

End Class
