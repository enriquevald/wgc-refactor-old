'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
' 
' MODULE NAME:   uc_entry_field.vb  
'
' DESCRIPTION:   entry field control
'
' AUTHOR:        Toni Jord�
'
' CREATION DATE: 02-APR-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------------------------------------------
' 02-APR-2002  TJ     Initial version
' 05-MAR-2013  ICS    Added new types in ValidateFormat():
'                     - FORMAT_ALPHA_NUMERIC, FORMAT_MONEY
' 27-MAR-2013  LEM    Set MaxLength according SetFilter parameters
'                     Check text value length in ValidateFormat 
' 08-AUG-2013  MMG    Added new types in ValidateFormat():
'                     - FORMAT_NUMBER_ALSO_NEGATIVES, FORMAT_MONEY_ALSO_NEGATIVES
' 16-AUG-2013  CCG    Fixed Bug WIG-123: Incorrect validate format for phone numbers 
' 21-AUG-2013  MMG    Added the following functions:
'                     - ValidateNumber_AlsoDecimalsAndNegatives    
'                     - ValidateCurrency_AlsoDecimalsAndNegatives
' 05-SEP-2013  ICS    Added suport to validate numbers with decimals and negatives numbers
'
' 04-OCT-2013  MMG    Modified  txt_value_KeyPress method for accepting the same quantity of digits 
'                     than in a positive number when is a negative one 
' 15-SEP-2014  JMM    Added ValueForeColor to change value fore color (only on read only mode)
' 02-OCT-2014  DRV    Added Focus property
' 10-DEC-2014  DRV    Decimal currency formatting: DUT 201411 - Tito Chile - Currency formatting without decimals
' 03-FEB-2015  OPC    Added Placeholder functionality (sample text)
' 11-FEB-2015  DRV    Fixed Bug WIG-2034: Some entry fields must allow decimals despite currency doesn't allow it
' 04-MAY-2015  JPJ    Added TextBackColor functionality
' 03-NOV-2017  EOR    Added ShortcutsEnabled functionality
'--------------------------------------------------------------------------------------------------------

Imports System.ComponentModel
Imports GUI_CommonOperations
'XVV -> Add NameSpace Windows.Forms,solve error with IWin32Window variables definitions
Imports System.Windows.Forms
Imports System.Drawing
Imports System.Threading
'XVV--------------------------------

Public Class uc_entry_field
  Inherits uc_base

  Private Const PASSWORD_CHAR As Char = "*"c
  Private m_filter As CLASS_FILTER
  Private WithEvents txt_value As System.Windows.Forms.TextBox
  Private m_last_text As String
  Private m_force_value As Boolean = False
  Private m_double_value As Integer
  Private m_integer_value As String
  Private m_ISO_code As String
  Private m_place_holder As String
  Private m_ignore_currency_format As Boolean

  'MMG 04-OCT-2013
  Private m_max_lenght_increased As Boolean = False

  Public Event EntryFieldValueChanged()

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call
    m_filter = New CLASS_FILTER(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT)
    Me.Width = 200
    Me.Height = 30
    m_double_value = 0
    m_integer_value = 0
  End Sub

  'UserControl overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.txt_value = New System.Windows.Forms.TextBox()
    Me.panel_control.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_control
    '
    Me.panel_control.Controls.AddRange(New System.Windows.Forms.Control() {Me.txt_value})
    Me.panel_control.Location = New System.Drawing.Point(0, 0)
    Me.panel_control.Size = New System.Drawing.Size(440, 28)
    '
    'lbl_read_only
    '
    Me.lbl_read_only.Dock = System.Windows.Forms.DockStyle.None
    Me.lbl_read_only.Location = New System.Drawing.Point(84, 4)
    Me.lbl_read_only.Size = New System.Drawing.Size(352, 20)
    '
    'txt_value
    '
    Me.txt_value.Location = New System.Drawing.Point(84, 4)
    Me.txt_value.Name = "txt_value"
    Me.txt_value.Size = New System.Drawing.Size(352, 20)
    Me.txt_value.TabIndex = 0
    Me.txt_value.Text = ""
    '
    'uc_entry_field
    '
    Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.lbl_read_only, Me.panel_control})
    Me.Name = "uc_entry_field"
    Me.Size = New System.Drawing.Size(200, 30)
    Me.SufixTextVisible = True
    Me.panel_control.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region


#Region "Properties"

  Public Sub SetFilter(ByVal FormatType As CLASS_FILTER.ENUM_FORMAT, _
                       Optional ByVal FormatLength As Integer = 0, _
                       Optional ByVal FormatDecimals As Integer = mdl_number_format.DEFAULT_DECIMAL_DIGITS_NUMBER, _
                       Optional ByVal ISOCode As String = "", _
                       Optional ByVal IgnoreCurrencyFormat As Boolean = False)

    Call m_filter.Init(FormatType, FormatLength, FormatDecimals)

    m_ISO_code = ISOCode

    txt_value.MaxLength = FormatLength

    m_ignore_currency_format = IgnoreCurrencyFormat

    If FormatType = CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER _
      Or FormatType = CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY _
      Or FormatType = CLASS_FILTER.ENUM_FORMAT.FORMAT_HEXA _
      Or FormatType = CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES _
      Or FormatType = CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY_ALSO_NEGATIVES Then

      txt_value.TextAlign = HorizontalAlignment.Right
      MyBase.ReadOnlyAlign = ContentAlignment.MiddleRight

    End If

  End Sub

  <Category("EntryField"), _
   Description("The input is masked with '*'"), _
   DefaultValue(False)> _
  Public Property Password() As Boolean
    Get
      txt_value.PasswordChar = PASSWORD_CHAR
    End Get
    Set(ByVal Value As Boolean)
      If Value = True Then
        txt_value.PasswordChar = PASSWORD_CHAR
      Else
        txt_value.PasswordChar = vbNullChar
      End If
    End Set
  End Property

  <Category("EntryField"), _
   Description("Only Upper Case Characters ara allowed"), _
   DefaultValue(False)> _
  Public Property OnlyUpperCase() As Boolean
    Get
      Return txt_value.CharacterCasing = CharacterCasing.Upper
    End Get
    Set(ByVal Value As Boolean)
      If Value Then
        txt_value.CharacterCasing = CharacterCasing.Upper
      Else
        txt_value.CharacterCasing = CharacterCasing.Normal
      End If
    End Set
  End Property

  Public Property TextAlign() As HorizontalAlignment
    Get
      Return txt_value.TextAlign
    End Get
    Set(ByVal Value As HorizontalAlignment)
      txt_value.TextAlign = Value
      Select Case Value
        Case HorizontalAlignment.Center
          lbl_read_only.TextAlign = ContentAlignment.MiddleCenter
        Case HorizontalAlignment.Left
          lbl_read_only.TextAlign = ContentAlignment.MiddleLeft
        Case HorizontalAlignment.Right
          lbl_read_only.TextAlign = ContentAlignment.MiddleRight
      End Select
    End Set
  End Property

  Public Property ValueForeColor() As Color
    Get
      Return lbl_read_only.ForeColor
    End Get
    Set(ByVal Value As Color)
      lbl_read_only.ForeColor = Value
    End Set
  End Property

  Public ReadOnly Property IsFocused() As Boolean
    Get
      Return txt_value.Focused
    End Get
  End Property

  Public Property PlaceHolder() As String
    Get
      Return m_place_holder
    End Get
    Set(ByVal value As String)
      Me.m_place_holder = value
      txt_value.Text = Me.m_place_holder

      If String.IsNullOrEmpty(value) Then
        txt_value.ForeColor = Color.Black
      Else
        txt_value.ForeColor = Color.Gray
      End If

    End Set
  End Property

  ' PURPOSE: Sets the background color of the textbox control.
  '
  '  PARAMS:
  '     - INPUT: Color to be set
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  <Category("EntryField"), _
 Description("Changes the textbox background color"), _
 DefaultValue(GetType(Color), "Color.White"), _
 Browsable(False), DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)> _
  Public Property TextBackColor() As Color
    Get
      Return txt_value.BackColor
    End Get
    Set(ByVal Value As Color)
      txt_value.BackColor = Value
    End Set
  End Property

  <Category("EntryField"), _
   Description("Enabled or disabled the key combinations and the control�s shortcut menu"), _
   DefaultValue(False)> _
  Public Property ShortcutsEnabled() As Boolean
    Get
      Return txt_value.ShortcutsEnabled
    End Get
    Set(ByVal Value As Boolean)
      txt_value.ShortcutsEnabled = Value
    End Set
  End Property

#End Region

  Private SkipKeyPress As Boolean = False

  Private Sub txt_value_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_value.KeyPress
    Dim _selected_text As Boolean
    Dim _filter_negatives_passed As Boolean

    _selected_text = (Me.txt_value.SelectionLength > 0)
    _filter_negatives_passed = Me.m_filter.FilterKeyCharNegative(e.KeyChar, Me.txt_value.Text, _selected_text, Me.txt_value.SelectionStart)

    '04-OCT-2013 MMG: Do it only for FORMAT_MONEY_ALSO_NEGATIVES or FORMAT_NUMBER_ALSO_NEGATIVES formats
    If Me.m_filter.FormatType = CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY_ALSO_NEGATIVES Or _
       Me.m_filter.FormatType = CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES Then

      'If a negative number has been writed in the past, but the negative sign has been removed, the max lenght must be decreased 
      If Me.m_max_lenght_increased And Not InStr(Me.txt_value.Text, "-"c) > 0 Then
        Me.txt_value.MaxLength = Me.txt_value.MaxLength - 1
        Me.m_filter.FormatLen = Me.m_filter.FormatLen - 1
        m_max_lenght_increased = False

      End If

      'If it is a valid negative number, the max lenght must be increased 
      If (e.KeyChar = "-"c Or InStr(Me.txt_value.Text, "-"c) > 0) And _
        _filter_negatives_passed And _
        Not Me.m_max_lenght_increased Then

        Me.txt_value.MaxLength = Me.txt_value.MaxLength + 1
        Me.m_filter.FormatLen = Me.m_filter.FormatLen + 1
        m_max_lenght_increased = True

      End If

    End If

    If Me.m_filter.FilterKeyChar(e.KeyChar, Me.txt_value.Text, _selected_text, Me.txt_value.SelectionStart) And _
       _filter_negatives_passed Then

      e.Handled = False
    Else
      If SkipKeyPress Then
        SkipKeyPress = False
      Else
        Call Beep()
      End If
      e.Handled = True
    End If

  End Sub

  Public Property Value() As String
    Get
      Return GetFormatedValue(Me.txt_value.Text.Trim, mdl_number_format.ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    End Get
    Set(ByVal NewValue As String)
      If Me.txt_value.PasswordChar = PASSWORD_CHAR Then
        m_force_value = True
        Me.txt_value.Text = NewValue
        m_force_value = False
        Me.ReadOnlyText = New String(PASSWORD_CHAR, Me.m_filter.FormatLen)
      Else
        m_force_value = True
        Me.txt_value.Text = GetFormatedValue(NewValue, mdl_number_format.ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
        m_force_value = False
        Me.ReadOnlyText = GetFormatedValue(NewValue, mdl_number_format.ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      End If
    End Set
  End Property

  Public ReadOnly Property GridValue() As String
    Get
      Return GetFormatedValue(Me.txt_value.Text.Trim, mdl_number_format.ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    End Get
  End Property

  Public Property TextValue() As String
    Get
      Return Me.txt_value.Text.Trim
    End Get

    Set(ByVal NewValue As String)
      m_force_value = True
      Me.txt_value.Text = NewValue
      m_force_value = False
      Me.ReadOnlyText = NewValue
    End Set

  End Property

  Public ReadOnly Property TextValueWithoutTrim() As String
    Get
      Return Me.txt_value.Text
    End Get
  End Property

  Public Property DoubleValue() As Double
    Get
      Return m_double_value
    End Get
    Set(ByVal NewValue As Double)
      m_double_value = NewValue
      m_force_value = True
      Me.txt_value.Text = GetFormatedValueNew(NewValue)
      m_force_value = False
      Me.ReadOnlyText = Me.txt_value.Text
    End Set
  End Property

  Public Property IntegerValue() As Integer
    Get
      Return m_integer_value
    End Get
    Set(ByVal NewValue As Integer)
      m_integer_value = NewValue
      m_force_value = True
      Me.txt_value.Text = GetFormatedValueNew(NewValue)
      m_force_value = False
      Me.ReadOnlyText = Me.txt_value.Text

    End Set
  End Property

  Public Overrides Property IsReadOnly() As Boolean
    Get
      Return MyBase.IsReadOnly
    End Get
    Set(ByVal Value As Boolean)
      MyBase.ReadOnlyText = Me.Value
      MyBase.IsReadOnly = Value
      ' Force display '*'
      Me.Value = Me.Value
    End Set
  End Property

  Private Sub txt_value_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_value.GotFocus
    m_force_value = True
    Me.txt_value.Text = GetFormatedValue(Me.txt_value.Text, mdl_number_format.ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    Call Me.txt_value.SelectAll()
    m_force_value = False
  End Sub

  Private Sub txt_value_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_value.LostFocus
    m_force_value = True
    Me.txt_value.Text = GetFormatedValue(Me.txt_value.Text, mdl_number_format.ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    m_force_value = False
  End Sub

  Private Sub txt_value_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt_value.KeyDown

    ' 27-JUN-2002 AJQ When the Decimal Point is pressed in the KeyPad it is translated to the current DecimalSeparator
    If e.KeyCode = Keys.Decimal Then
      If Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator <> "." Then
        SkipKeyPress = True
        Call SendKeys.Send(Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator)
        Call SendKeys.Flush()
      End If
    End If

  End Sub

  Private Sub txt_value_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_value.TextChanged

    If m_force_value Then
      Return
    End If

    RaiseEvent EntryFieldValueChanged()

  End Sub

  Private Function GetFormatedValue(ByVal StringValue As String, _
                                    ByVal GroupDigits As ENUM_GROUP_DIGITS) As String

    Dim str_simbol As String

    str_simbol = ""

    If StringValue = "" Then
      Return ""
    End If

    Select Case m_filter.FormatType
      Case CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY
        If GroupDigits = mdl_number_format.ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE Then
          Return GUI_FormatCurrency(GUI_ParseCurrency(StringValue), m_filter.FormatDecimals, GroupDigits, False, m_ISO_code, m_ignore_currency_format)
        Else
          Return GUI_FormatCurrency(GUI_ParseCurrency(StringValue), m_filter.FormatDecimals, GroupDigits, True, m_ISO_code, m_ignore_currency_format)
        End If

      Case CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER
        Return GUI_FormatNumber(GUI_ParseNumber(StringValue), m_filter.FormatDecimals, GroupDigits)

        ' 09-AUG-2013 MMG Added new enums cases (FORMAT_MONEY_ALSO_NEGATIVES and FORMAT_NUMBER_ALSO_NEGATIVES) 
      Case CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY_ALSO_NEGATIVES
        If InStr(StringValue, "-"c) > 0 Then
          StringValue = IIf(StringValue.Replace("-"c, "") = "", "0", StringValue.Replace("-"c, ""))
          str_simbol = IIf(CType(StringValue, Double) <> 0, "-", "")
        End If
        If GroupDigits = mdl_number_format.ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE Then
          Return (str_simbol & GUI_FormatCurrency(GUI_ParseCurrency(StringValue), m_filter.FormatDecimals, GroupDigits, False, m_ISO_code)).Trim()
        Else
          Return (str_simbol & GUI_FormatCurrency(GUI_ParseCurrency(StringValue), m_filter.FormatDecimals, GroupDigits, True, m_ISO_code)).Trim()
        End If

      Case CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES
        If InStr(StringValue, "-"c) > 0 Then
          StringValue = IIf(StringValue.Replace("-"c, "") = "", "0", StringValue.Replace("-"c, ""))
          str_simbol = IIf(CType(StringValue, Double) <> 0, "-", "")
        End If
        Return (str_simbol & GUI_FormatNumber(GUI_ParseNumber(StringValue), m_filter.FormatDecimals, GroupDigits)).Trim()

      Case Else
        Return StringValue
    End Select

  End Function

  Private Function GetFormatedValueNew(ByVal NumberValue As Double) As String

    Select Case m_filter.FormatType
      Case CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY
        Return GUI_FormatCurrency(NumberValue, m_filter.FormatDecimals, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, True, m_ISO_code)

      Case CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER
        Return GUI_FormatNumber(NumberValue, m_filter.FormatDecimals, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

        ' 09-AUG-2013 MMG Added new enums cases (FORMAT_MONEY_ALSO_NEGATIVES and FORMAT_NUMBER_ALSO_NEGATIVES) 
      Case CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY_ALSO_NEGATIVES
        Return GUI_FormatCurrency(NumberValue, m_filter.FormatDecimals, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT, True, m_ISO_code)

      Case CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES
        Return GUI_FormatNumber(NumberValue, m_filter.FormatDecimals, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

      Case Else
        Return GUI_FormatNumber(0, m_filter.FormatDecimals, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)

    End Select

  End Function

  Public Function ValidateFormat() As Boolean
    Dim _number_decimal_separator As String
    Dim _currency_decimal_separator As String
    Dim _length_allowed As Integer

    _number_decimal_separator = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator
    _currency_decimal_separator = Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator

    _length_allowed = Me.m_filter.FormatLen

    If Me.m_filter.FormatType = CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY_ALSO_NEGATIVES Or _
      Me.m_filter.FormatType = CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES Then
      'If it's a valid negative number, must increment by one the allowed length (for the negative sign)
      If InStr(Me.txt_value.Text, "-"c) > 0 Then
        _length_allowed = m_filter.FormatLen + 1

      End If

    End If

    If Not String.IsNullOrEmpty(Me.Value) AndAlso Me.Value.Length > _length_allowed Then
      Return False
    End If

    Try
      Select Case Me.m_filter.FormatType

        Case CLASS_FILTER.ENUM_FORMAT.FORMAT_PHONE_NUMBER
          'CCG 16-08-2013: Allways check as not filter search
          Return WSI.Common.ValidateFormat.PhoneNumber(Me.Value, False)

        Case CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_BASE
          Return WSI.Common.ValidateFormat.TextWithoutNumbers(Me.Value)

        Case CLASS_FILTER.ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED
          Return WSI.Common.ValidateFormat.TextWithNumbers(Me.Value)

        Case CLASS_FILTER.ENUM_FORMAT.FORMAT_TRACK_DATA
          Return WSI.Common.ValidateFormat.TrackData(Me.Value)

        Case CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER
          Return WSI.Common.ValidateFormat.RawNumber(Me.Value)

        Case CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA_NUMERIC
          Return WSI.Common.ValidateFormat.AlphaNumeric(Me.Value)

        Case CLASS_FILTER.ENUM_FORMAT.FORMAT_ALPHA
          Return WSI.Common.ValidateFormat.Alpha(Me.Value)

        Case CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY
          Return ValidateCurrency(Me.Value)

        Case CLASS_FILTER.ENUM_FORMAT.FORMAT_PROVIDERS_NAME
          Return WSI.Common.ValidateFormat.CheckProviderName(Me.Value)

        Case CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES
          Return WSI.Common.ValidateFormat.Number(Me.Value, _number_decimal_separator, Me.m_filter.FormatDecimals, True)

        Case CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY_ALSO_NEGATIVES
          Return WSI.Common.ValidateFormat.Number(Me.Value, _currency_decimal_separator, Me.m_filter.FormatDecimals, True)

        Case CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER
          Return WSI.Common.ValidateFormat.Number(Me.Value, _number_decimal_separator, Me.m_filter.FormatDecimals, False)

      End Select

    Catch
      Return False
    End Try

    Return True

  End Function

  ' PURPOSE: Validates de format of a currency value.
  '
  '  PARAMS:
  '     - INPUT: String of the Value
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '         - True: if the format is correct.
  '         - False: Otherwise.
  '
  Private Function ValidateCurrency(ByVal Value As String) As Boolean
    Dim _decimal_separator_currency As String
    Dim _no_decimals As Boolean

    _decimal_separator_currency = Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator
    _no_decimals = False

    For Each _char As Char In Value

      If Not (_char = _decimal_separator_currency) And _
         Not (_char >= "0"c And _char <= "9"c) Then
        Return False
      End If

      If (_char = _decimal_separator_currency And _
          m_filter.FormatDecimals = 0) Then
        _no_decimals = True
      End If

      ' Check that decimals are 0
      If (_char > "0"c And _no_decimals) Then
        Return False
      End If
    Next

    Return True
  End Function

End Class
