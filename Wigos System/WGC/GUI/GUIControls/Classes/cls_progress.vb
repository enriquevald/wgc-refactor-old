'XVV -> Add NameSpace Windows.Forms,solve error with IWin32Window variables definitions
Imports System.Windows.Forms

Public Class cls_progress
    Private m_thread As System.Threading.Thread
    Private WithEvents m_form As frm_progress
    Private m_stop As Boolean
    Private m_new_job As System.Threading.AutoResetEvent

    Private m_mutex As System.Threading.Mutex
    Private m_sequencial As Integer = 0
    Private m_num_jobs As Integer = 0

    Private m_job_started As Date
    Private m_title As String

    Private m_location As System.Drawing.Point


    Public Sub New()
        m_form = Nothing
        m_stop = False
        m_new_job = New System.Threading.AutoResetEvent(False)
        m_mutex = New System.Threading.Mutex(False)
        m_thread = New System.Threading.Thread(AddressOf Me.Start)
        m_thread.Start()
    End Sub

    Private Sub Start()

        m_form = New frm_progress
        m_form.StartPosition = FormStartPosition.CenterScreen
        m_form.Hide()
        m_location = m_form.Location
        m_location.Y = 0
        m_form.StartPosition = FormStartPosition.Manual

        AddHandler m_form.ProgressStatus, AddressOf ProgressStatus

        While Not m_stop

            ' Wait till new job and/or timeout
            m_new_job.WaitOne(200, False)

            If m_num_jobs > 0 Then
                ' At least one job is running
                If Now.Subtract(m_job_started).TotalSeconds > 2 Then
                    ' Job started long time ago
                    If Not (m_form Is Nothing) Then
                        ' 
                        m_form.BringToFront()
                        m_form.Location = m_location
                        m_form.ShowDialog()
                        m_form.Hide()
                        m_location = m_form.Location

                    End If
                End If

            End If

        End While

    End Sub

#Region "PRIVATE"

    Private Sub ProgressStatus(ByRef Started As Date, ByRef NumJobs As Integer, ByRef Title As String)
        Started = m_job_started
        NumJobs = m_num_jobs
        Title = m_title
    End Sub

#End Region


#Region "PUBLIC"

    '-----------------------------------------------------------------------------
    ' PURPOSE:
    '-----------------------------------------------------------------------------
    Public Sub Done()
        m_stop = True
        m_thread.Abort()

    End Sub

    Public Function JobStart(ByVal Name As String) As Integer

        Try
            m_mutex.WaitOne()

            m_sequencial += 1
            m_num_jobs += 1

            If m_num_jobs = 1 Then
                m_title = Name
                m_job_started = Now
                m_new_job.Set()
            End If

            Return m_sequencial

        Finally
            m_mutex.ReleaseMutex()
        End Try

    End Function

    '-----------------------------------------------------------------------------
    ' PURPOSE:
    '-----------------------------------------------------------------------------
    Public Sub JobEnd(ByVal JobId As Integer)

        Try
            m_mutex.WaitOne()

            If m_num_jobs > 0 Then
                m_num_jobs -= 1
            End If

            If m_num_jobs = 0 Then
                m_new_job.Reset()
            End If

        Finally
            m_mutex.ReleaseMutex()
        End Try


    End Sub

    Public Sub SetStatus(ByVal JobId As Integer, ByVal Status As String)
        m_form.Text = Status
    End Sub
#End Region

End Class
