'-------------------------------------------------------------------
' Copyright � 2008 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : cls_sw_package
'
' DESCRIPTION : Class to manage software packages
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 09-SEP-2008  TJG    Initial version
' 06-SEP-2012  DDM    Added PromoBOX in package name.
' 27-AUG-2013  JCA    Added "Wigos Cashdesk Draws GUI"  in package name.
' 11-MAR-2016  JCA    Added "SmartFloor" in package name.
' 19-OCT-2017  ETP    Added "Protocols service" in package name.
'--------------------------------------------------------------------

Imports System.IO
Imports System.Text
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports WSI.Common

Public Class CLASS_SW_PACKAGE
  Inherits CLASS_BASE

  Public Enum ENUM_PACKAGE_STATUS
    STATUS_OK = 0
    STATUS_ERROR = 1
  End Enum

#Region " Constants "

  Public Const MAX_PATH_LEN = 260

  Public Const BUILD_TYPE_FULL As Integer = 0
  Public Const BUILD_TYPE_UPGRADE As Integer = 1

  ' Fields length
  Public Const MAX_LOCATIONS As Integer = 2
  Public Const MAX_PACKAGE_MODULES As Integer = 200
  Public Const PATH_LENGTH As Integer = 256
  Public Const MODULE_NAME_LENGTH As Integer = 256
  Public Const MODULE_VERSION_LENGTH As Integer = 32

  ' Ftp Site Credentials
  Private Const FTP_USERNAME As String = "WGPrvWriter"
  Private Const FTP_PASSWORD As String = "WG14Restricted"

#End Region ' Constants

#Region " GUI_Configuration.dll "

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_SW_PACKAGE_MODULE
    Public control_block As Integer
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PATH_LEN)> _
    Public name As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MODULE_VERSION_LENGTH)> _
    Public version As String
    Public size As Integer

    Public Sub New()
      MyBase.New()
      control_block = Marshal.SizeOf(Me)
    End Sub
  End Class

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_SW_PACKAGE
    Public control_block As Integer
    Public file_name As String
    Public full_file_name As String
    Public client_id As Integer
    Public build_id As Integer
    Public build_type As Integer '0: Full; 1: Upgrade
    Public terminal_type As Integer
    Public insertion_date As New TYPE_DATE_TIME
    Public num_locations As Integer
    Public locations(0 To MAX_LOCATIONS - 1) As String
    Public copy_status(0 To MAX_LOCATIONS - 1) As Integer

    ' Only set when loaded from a package file 
    Public num_modules As Integer
    Public modules() As TYPE_SW_PACKAGE_MODULE

    Public Sub New()
      MyBase.New()
      control_block = Marshal.SizeOf(Me)

      num_locations = 0
      num_modules = 0
    End Sub
  End Class

  ' Referenced by Common_CheckOutSwPackage & Common_GetSwPackageFile functions from CommonBase.dll
  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_SW_PACKAGE_FILE
    Public control_block As Integer
    Public client_id As Integer
    Public build_id As Integer
    Public build_type As Integer '0: Full; 1: Upgrade
    Public terminal_type As Integer

    Public packer_version As Integer
    Public compressed As Integer

    Public num_modules As Integer

    Public Sub New()
      MyBase.New()
      control_block = Marshal.SizeOf(Me)
    End Sub

  End Class

#End Region 'Structures

#Region " Function Prototypes "

  Public Declare Function Common_GetPackageFileDetails Lib "CommonBase" (ByVal pFileName As String, _
                                                                     <[In](), [Out]()> _
                                                                     ByVal SwPackageFile As CLASS_SW_PACKAGE.TYPE_SW_PACKAGE_FILE) As Integer

  Public Declare Function Common_GetSwPackageFile Lib "CommonBase" (ByVal Index As Integer, _
                                                                    ByVal pFileName As String, _
                                                                    ByVal pVersion As String, _
                                                                    ByRef pFileSize As Integer) As Boolean

  Public Declare Function Common_ExpandPackageFile Lib "CommonBase" (ByVal PackageFileName As String, _
                                                                    <[In](), [Out]()> _
                                                                     ByVal TemporaryPath As String) As Integer

  Public Declare Function Common_CopyFileList Lib "CommonBase" (ByVal SourcePath As String, _
                                                                ByVal TargetPath As String) As Boolean

  Public Declare Function Common_CleanPath Lib "CommonBase" (ByVal TargetPath As String, _
                                                             ByVal DeletePath As Boolean) As Boolean

  Public Declare Function Common_FtpConnect Lib "CommonBase" (ByVal FtpHost As String, _
                                                              ByVal FtpUser As String, _
                                                              ByVal FtpPassword As String, _
                                                              ByRef SessionHandle As Integer, _
                                                              ByRef Context As Integer) As Boolean

  Public Declare Function Common_FtpSetPath Lib "CommonBase" (ByVal SessionHandle As Integer, _
                                                              ByVal TargetPath As String) As Boolean

  Public Declare Function Common_FtpPutFile Lib "CommonBase" (ByVal SessionHandle As Integer, _
                                                              ByRef Context As Integer, _
                                                              ByVal SourceFile As String) As Boolean

  Public Declare Function Common_CleanFtpPath Lib "CommonBase" (ByVal SessionHandle As Integer, _
                                                                ByVal Path As String, _
                                                                ByVal DeletePath As Boolean) As Boolean

  Public Declare Function Common_FtpRenamePath Lib "CommonBase" (ByVal SessionHandle As Integer, _
                                                                 ByVal SourcePath As String, _
                                                                 ByVal TargetPath As String) As Boolean

  Public Declare Function Common_FtpDisconnect Lib "CommonBase" (ByVal SessionHandle As Integer) As Boolean

  Public Declare Function Common_FtpGetFileDetails Lib "CommonBase" (ByVal SessionHandle As Integer, _
                                                                     ByRef SearchHandle As Integer, _
                                                                     ByVal Path As String, _
                                                                      <[In](), [Out]()> _
                                                                     ByVal ModuleInfo As CLASS_SW_PACKAGE.TYPE_SW_PACKAGE_MODULE) As Boolean

#End Region

#End Region ' GUI_Configuration.dll

#Region " Members "

  Protected m_sw_package As New TYPE_SW_PACKAGE

#End Region 'Members

#Region " Properties "

  Public Property FileName() As String
    Get
      Return m_sw_package.file_name
    End Get

    Set(ByVal Value As String)
      m_sw_package.file_name = Value
    End Set
  End Property

  Public Property FullFileName() As String
    Get
      Return m_sw_package.full_file_name
    End Get

    Set(ByVal Value As String)
      m_sw_package.full_file_name = Value
    End Set
  End Property

  Public Property ClientId() As Integer
    Get
      Return m_sw_package.client_id
    End Get

    Set(ByVal Value As Integer)
      m_sw_package.client_id = Value
    End Set
  End Property

  Public Property BuildId() As Integer
    Get
      Return m_sw_package.build_id
    End Get

    Set(ByVal Value As Integer)
      m_sw_package.build_id = Value
    End Set
  End Property

  Public Property BuildType() As Integer
    Get
      Return m_sw_package.build_type
    End Get

    Set(ByVal Value As Integer)
      m_sw_package.build_type = Value
    End Set
  End Property

  Public Property Version() As String
    Get
      Return FormatBuild(m_sw_package.client_id, m_sw_package.build_id)
    End Get

    Set(ByVal Value As String)
    End Set
  End Property

  Public Property TerminalType() As Integer
    Get
      Return m_sw_package.terminal_type
    End Get

    Set(ByVal Value As Integer)
      m_sw_package.terminal_type = Value
    End Set
  End Property

  Public Property InsertionDate() As Date
    Get
      Return m_sw_package.insertion_date.Value
    End Get

    Set(ByVal Value As Date)
      m_sw_package.insertion_date.Value = Value
    End Set
  End Property

  Public Property PackageName() As String
    Get
      Dim aux_text As String

      Select Case m_sw_package.terminal_type
        Case ENUM_TSV_PACKET.TSV_PACKET_WIN_KIOSK
          aux_text = "LKT"

        Case ENUM_TSV_PACKET.TSV_PACKET_SAS_HOST
          aux_text = "SAS_HOST"

        Case ENUM_TSV_PACKET.TSV_PACKET_SITE_JACKPOT
          aux_text = "SITE_JACKPOT"

        Case ENUM_TSV_PACKET.TSV_PACKET_WIN_CASHIER
          aux_text = "CT"

        Case ENUM_TSV_PACKET.TSV_PACKET_WIN_GUI
          aux_text = "GUI"

        Case ENUM_TSV_PACKET.TSV_PACKET_WIN_MULTISITE_GUI
          aux_text = "MULTISITE_GUI"

          'JCA CashDeskDraws
        Case ENUM_TSV_PACKET.TSV_PACKET_WIN_CASHDESK_DRAWS_GUI
          aux_text = "CASHDESK_DRAWS_GUI"

        Case ENUM_TSV_PACKET.TSV_PACKET_WCP_SERVICE
          aux_text = "WCP"

        Case ENUM_TSV_PACKET.TSV_PACKET_WC2_SERVICE
          aux_text = "WC2"

        Case ENUM_TSV_PACKET.TSV_PACKET_WXP_SERVICE
          aux_text = "WXP"

        Case ENUM_TSV_PACKET.TSV_PACKET_WWP_SITE_SERVICE
          aux_text = "WWP_SITE"

        Case ENUM_TSV_PACKET.TSV_PACKET_WWP_CENTER_SERVICE
          aux_text = "WWP_CENTER"

        Case ENUM_TSV_PACKET.TSV_PACKET_MOBILE_BANK_IMB
          aux_text = "IMB"

        Case ENUM_TSV_PACKET.TSV_PACKET_ISTATS
          aux_text = "ISTATS"

        Case ENUM_TSV_PACKET.TSV_PACKET_PROMOBOX
          aux_text = "PROMO_BOX"

        Case ENUM_TSV_PACKET.TSV_PACKET_PSA_CLIENT
          aux_text = "PSA_CLIENT"

        Case ENUM_TSV_PACKET.TSV_PACKET_AFIP_CLIENT
          aux_text = "AFIP_CLIENT"

        Case ENUM_TSV_PACKET.TSV_PACKET_SMARTFLOOR_DATA_SERVICE
          aux_text = "SMARTFLOOR_DATA_SERVICE"

        Case ENUM_TSV_PACKET.TSV_PACKET_PROTOCOLS_SERVICE
          aux_text = "PROTOCOLS_SERVICE"

        Case Else
          Return "XXX" + "_" + m_sw_package.terminal_type.ToString("000") _
                       + "_" + FormatBuild(m_sw_package.client_id, m_sw_package.build_id)

      End Select

      aux_text = aux_text + "_" + FormatBuild(m_sw_package.client_id, m_sw_package.build_id) _
                          + "_" + IIf(m_sw_package.build_type = BUILD_TYPE_FULL, "FULL", "UPGRADE")

      Return aux_text
    End Get

    Set(ByVal Value As String)
    End Set
  End Property

  Public ReadOnly Property LocationPath() As String
    Get
      Return "\" & m_sw_package.terminal_type.ToString("000") & "\" & FormatBuild(m_sw_package.client_id, m_sw_package.build_id)
    End Get
  End Property

  Public Property NumLocations() As Integer
    Get
      Return m_sw_package.num_locations
    End Get

    Set(ByVal Value As Integer)
      m_sw_package.num_locations = Value
    End Set
  End Property

  Public Property Location(ByVal IdxLocation As Integer) As String
    Get
      Return m_sw_package.locations(IdxLocation)
    End Get

    Set(ByVal Value As String)
      m_sw_package.locations(IdxLocation) = Value
    End Set
  End Property

  Public Property CopyStatus(ByVal IdxLocation As Integer) As ENUM_PACKAGE_STATUS
    Get
      Return m_sw_package.copy_status(IdxLocation)
    End Get

    Set(ByVal Value As ENUM_PACKAGE_STATUS)
      m_sw_package.copy_status(IdxLocation) = Value
    End Set
  End Property

  Public Property NumModules() As Integer
    Get
      Return m_sw_package.num_modules
    End Get

    Set(ByVal Value As Integer)
      m_sw_package.num_modules = Value

      If Value > 0 Then
        Dim idx_module As Integer

        ReDim m_sw_package.modules(Value)

        For idx_module = 0 To Value - 1
          m_sw_package.modules(idx_module) = New TYPE_SW_PACKAGE_MODULE

        Next

      End If
    End Set

  End Property

  Public Property PackageModule(ByVal IdxModule As Integer) As TYPE_SW_PACKAGE_MODULE
    Get
      Return m_sw_package.modules(IdxModule)
    End Get

    Set(ByVal Value As TYPE_SW_PACKAGE_MODULE)
      m_sw_package.modules(IdxModule).name = Value.name
      m_sw_package.modules(IdxModule).version = Value.version
      m_sw_package.modules(IdxModule).size = Value.size
    End Set

  End Property

#End Region ' Properties

#Region " Overrides functions "

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    rc = ReadSwPackage(m_sw_package, SqlCtx)

    Select Case rc

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Read

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    m_sw_package.control_block = Marshal.SizeOf(m_sw_package)

    ' Update user data
    rc = UpdateSwPackage(m_sw_package, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Update

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    m_sw_package.control_block = Marshal.SizeOf(m_sw_package)

    ' Insert user data
    rc = InsertSwPackage(m_sw_package, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DUPLICATE_KEY
        Return ENUM_STATUS.STATUS_DUPLICATE_KEY

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Insert

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As GUI_CommonOperations.CLASS_BASE.ENUM_STATUS

    Dim rc As Integer

    m_sw_package.control_block = Marshal.SizeOf(m_sw_package)

    ' Delete user data
    rc = DeleteSwPackage(m_sw_package, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function ' DB_Delete

  Public Overrides Function AuditorData() As GUI_CommonOperations.CLASS_AUDITOR_DATA

    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim idx_location As Integer

    ' Create a new auditor_code for Software Download
    auditor_data = New CLASS_AUDITOR_DATA(23)

    ' 235 "Package "
    Call auditor_data.SetName(GLB_NLS_GUI_SW_DOWNLOAD.Id(235), Me.PackageName)

    ' 230 "Version"
    Call auditor_data.SetField(GLB_NLS_GUI_SW_DOWNLOAD.Id(230), FormatBuild(Me.ClientId, Me.BuildId))

    ' 233 "Type"
    Call auditor_data.SetField(GLB_NLS_GUI_SW_DOWNLOAD.Id(233), CLASS_SW_PACKAGE.GetTerminalTypeName(Me.TerminalType))

    ' Copy status
    For idx_location = 0 To Me.NumLocations - 1
      If Me.CopyStatus(idx_location) = ENUM_PACKAGE_STATUS.STATUS_OK Then
        ' 237 "Copy OK"
        Call auditor_data.SetField(GLB_NLS_GUI_SW_DOWNLOAD.Id(237), Me.Location(idx_location))
      Else
        ' 238 "Copy Not OK"
        Call auditor_data.SetField(GLB_NLS_GUI_SW_DOWNLOAD.Id(238), Me.Location(idx_location))
      End If
    Next

    Return auditor_data

  End Function ' AuditorData

  Public Overrides Function Duplicate() As GUI_CommonOperations.CLASS_BASE

    Dim temp_package As CLASS_SW_PACKAGE

    temp_package = New CLASS_SW_PACKAGE
    temp_package.m_sw_package = m_sw_package

    Return temp_package

  End Function 'Duplicate

#End Region 'Overrides functions

#Region " Private Functions "

  Private Function ReadSwPackage(ByRef SwPackage As TYPE_SW_PACKAGE, _
                                 ByVal Context As Integer) As Integer
    Dim str_sql As String
    Dim data_table As DataTable
    Dim aux_sw_package As New CLASS_SW_PACKAGE

    If SwPackage.client_id < 1 Or SwPackage.build_id < 1 _
    Or SwPackage.terminal_type < 0 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
    End If

    str_sql = "select TSV_INSERTION_DATE" _
             & " from TERMINAL_SOFTWARE_VERSIONS" _
            & " where TSV_CLIENT_ID = " & SwPackage.client_id.ToString _
              & " and TSV_BUILD_ID = " & SwPackage.build_id.ToString _
              & " and TSV_TERMINAL_TYPE = " & SwPackage.terminal_type.ToString

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)
    If IsNothing(data_table) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If data_table.Rows.Count() <> 1 Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    ' Parse the read data
    '     - Insertion Datetime
    SwPackage.insertion_date.Value = data_table.Rows(0).Item("TSV_INSERTION_DATE")

    '     - Locations
    aux_sw_package.ClientId = SwPackage.client_id
    aux_sw_package.BuildId = SwPackage.build_id
    aux_sw_package.TerminalType = SwPackage.terminal_type

    If Not CLASS_SW_PACKAGE.GetPackageLocations(aux_sw_package) Then
      ' Wrong package locations
      Exit Function
    End If

    SwPackage.num_locations = aux_sw_package.NumLocations
    SwPackage.locations(0) = aux_sw_package.Location(0)
    SwPackage.locations(1) = aux_sw_package.Location(1)

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' ReadSwPackage

  Private Function UpdateSwPackage(ByVal SwPackage As TYPE_SW_PACKAGE, _
                                   ByVal Context As Integer) As Integer

    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function ' UpdateSwPackage

  Private Function InsertSwPackage(ByVal SwPackage As TYPE_SW_PACKAGE, _
                                   ByVal Context As Integer) As Integer

    Dim str_sql As String
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing

    If Not GUI_BeginSQLTransaction(SqlTrans) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    str_sql = "INSERT INTO   TERMINAL_SOFTWARE_VERSIONS " & _
                         " ( TSV_CLIENT_ID " & _
                         " , TSV_BUILD_ID " & _
                         " , TSV_TERMINAL_TYPE " & _
                         " ) " & _
                  " VALUES " & _
                         " ( " & SwPackage.client_id & _
                         " , " & SwPackage.build_id & _
                         " , " & SwPackage.terminal_type & _
                         " )"

    If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If Not GUI_EndSQLTransaction(SqlTrans, True) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK
  End Function ' InsertSwPackage

  Private Function DeleteSwPackage(ByVal SwPackage As TYPE_SW_PACKAGE, _
                                   ByVal Context As Integer) As Integer

    Dim str_sql As String
    Dim SqlTrans As System.Data.SqlClient.SqlTransaction = Nothing

    If Not GUI_BeginSQLTransaction(SqlTrans) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    str_sql = "DELETE FROM TERMINAL_SOFTWARE_VERSIONS " & _
                   " WHERE TSV_CLIENT_ID = " & SwPackage.client_id & _
                     " AND TSV_BUILD_ID = " & SwPackage.build_id & _
                     " AND TSV_TERMINAL_TYPE = " & SwPackage.terminal_type

    If Not GUI_SQLExecuteNonQuery(str_sql, SqlTrans) Then
      GUI_EndSQLTransaction(SqlTrans, False)

      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    If Not GUI_EndSQLTransaction(SqlTrans, True) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Return ENUM_CONFIGURATION_RC.CONFIGURATION_OK

  End Function ' DeleteSwPackage

  Private Shared Function Plist_FtpPutFile(ByVal SessionHandle As Integer, _
                                                            ByRef Context As Integer, _
                                                             ByVal RepositoryAddress As String, _
                                                            ByRef SwPackage As CLASS_SW_PACKAGE, _
                                                            ByVal SourcePath As String) As Boolean

    Dim strm As System.IO.Stream
    Dim writeStream As FileStream
    Dim _str As String
    Dim _file_path As String
    Dim _builder As New StringBuilder
    Dim _file_name As String

    If SwPackage.TerminalType = ENUM_TSV_PACKET.TSV_PACKET_MOBILE_BANK_IMB Then
      _file_name = "imb.plist"
    ElseIf SwPackage.TerminalType = ENUM_TSV_PACKET.TSV_PACKET_ISTATS Then
      _file_name = "istats.plist"
    Else
      Return False
    End If

    _builder.Append(SourcePath.Replace(Chr(0), ""))
    _builder.Append("\")
    _builder.Append(_file_name)

    ' Create imb.plist file
    _file_path = _builder.ToString()
    writeStream = New FileStream(_file_path, FileMode.Create, FileAccess.Write)

    strm = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("GUI_Controls." & _file_name)

    Using reader As New IO.StreamReader(strm)
      ' Read the contents in MsgBox for example
      _str = reader.ReadToEnd
    End Using

    ' Substitute @STR_IP_WEB_SERVER
    If SwPackage.TerminalType = ENUM_TSV_PACKET.TSV_PACKET_MOBILE_BANK_IMB Then
      _str = _str.Replace("@STR_IP_WEB_SERVER", RepositoryAddress)
    ElseIf SwPackage.TerminalType = ENUM_TSV_PACKET.TSV_PACKET_ISTATS Then
      _str = _str.Replace("@STR_IP_WEB_SERVER", "ios.winsystemsintl.com")
    Else
      Return False
    End If

    ' Substitute @STR_VERSION
    _str = _str.Replace("@STR_VERSION", SwPackage.Version)

    Using outfile As New StreamWriter(writeStream)
      outfile.Write(_str)
    End Using

    writeStream.Close()

    ' Upload
    Return Common_FtpPutFile(SessionHandle, Context, _file_path)

  End Function


  Private Shared Function FtpToLocation(ByVal SourcePath As String, ByVal RepositoryAddress As String, ByRef SwPackage As CLASS_SW_PACKAGE) As Boolean

    Dim rc As Boolean
    Dim ftp_handle As Integer
    Dim ftp_context As Integer
    Dim idx_module As Integer
    Dim ftp_path As String
    Dim full_file_path As String
    Dim temp_path As String

    ' These handles are used within the FTP functions
    ftp_handle = 0
    ftp_context = 0

    Try

      ' Establish connection to FTP site
      rc = Common_FtpConnect(RepositoryAddress, FTP_USERNAME, FTP_PASSWORD, ftp_handle, ftp_context)
      If Not rc Then

        Return False
      End If

      ' Prepare temporary path in FTP site
      ' - Create it if it does not exist
      ' - Remove any existing files from it
      ' - Make it the current path
      ftp_path = SwPackage.LocationPath
      temp_path = ftp_path & "TMP"
      rc = Common_FtpSetPath(ftp_handle, temp_path)
      If Not rc Then

        Return False
      End If

      ' Copy all files
      For idx_module = 0 To SwPackage.NumModules - 1
        full_file_path = NullTrim(SourcePath) & "\" & NullTrim(SwPackage.PackageModule(idx_module).name)
        rc = Common_FtpPutFile(ftp_handle, ftp_context, full_file_path)
        If Not rc Then

          Return False
        End If
      Next

      ' Copy .plist if iMB
      If SwPackage.TerminalType = ENUM_TSV_PACKET.TSV_PACKET_MOBILE_BANK_IMB _
      Or SwPackage.TerminalType = ENUM_TSV_PACKET.TSV_PACKET_ISTATS Then
        rc = Plist_FtpPutFile(ftp_handle, ftp_context, RepositoryAddress, SwPackage, SourcePath)
        If Not rc Then
          Return False
        End If
      End If


      ' Rename temporary path to the final one
      rc = Common_FtpRenamePath(ftp_handle, temp_path, ftp_path)
      If Not rc Then

        Return False
      End If

    Finally
      ' Close ftp session
      Common_FtpDisconnect(ftp_handle)

    End Try

    Return True
  End Function ' FtpToLocation

#End Region 'Private Functions

#Region " Public Functions "

  ' PURPOSE : 
  '
  '  PARAMS:
  '     - INPUT:
  '         - FileName
  '         - SwPackage
  '
  '     - OUTPUT:
  '
  ' RETURNS : Version 

  Public Shared Function GetPackageFileDetails(ByVal FileName As String, _
                                               ByRef SwPackage As CLASS_SW_PACKAGE) As Boolean

    Dim idx_file As Integer
    Dim file_name As String
    Dim file_version As String
    Dim file_size As Integer
    Dim sw_package_file As New CLASS_SW_PACKAGE.TYPE_SW_PACKAGE_FILE
    Dim sw_package_module As New CLASS_SW_PACKAGE.TYPE_SW_PACKAGE_MODULE
    Dim file_info As System.IO.FileInfo

    If Common_GetPackageFileDetails(FileName, sw_package_file) <> 0 Then
      Return False
    End If

    file_info = My.Computer.FileSystem.GetFileInfo(FileName)

    ' Transfer information retrieved from package file
    SwPackage.FullFileName = FileName
    SwPackage.FileName = file_info.Name
    SwPackage.ClientId = sw_package_file.client_id
    SwPackage.BuildId = sw_package_file.build_id
    SwPackage.BuildType = sw_package_file.build_type
    SwPackage.TerminalType = sw_package_file.terminal_type
    SwPackage.NumModules = sw_package_file.num_modules

    file_name = New String(Chr(0), MODULE_NAME_LENGTH)
    file_version = New String(Chr(0), MODULE_VERSION_LENGTH)

    For idx_file = 0 To sw_package_file.num_modules - 1
      If Not Common_GetSwPackageFile(idx_file, file_name, file_version, file_size) Then
        Continue For
      End If

      sw_package_module.name = NullTrim(file_name)
      sw_package_module.version = NullTrim(file_version)
      sw_package_module.size = file_size
      SwPackage.PackageModule(idx_file) = sw_package_module
    Next

    Return True

  End Function ' GetPackageFileDetails

  ' PURPOSE : Formats the version as CC.BBB (Client.Build)
  '
  '  PARAMS:
  '     - INPUT:
  '         - BuildNumber 
  '
  '     - OUTPUT:
  '
  ' RETURNS : Version 

  Public Shared Function FormatBuild(ByVal ClientNumber As Integer, ByVal BuildNumber As Integer) As String

    Return ClientNumber.ToString("00") + "." + BuildNumber.ToString("000")

  End Function ' FormatBuild

  ' PURPOSE : Get the terminal type name
  '
  '  PARAMS:
  '     - INPUT:
  '         - Terminal Type
  '
  '     - OUTPUT:
  '
  ' RETURNS : Terminal type name

  Public Shared Function GetTerminalTypeName(ByVal TerminalType As Integer) As String

    Select Case TerminalType
      Case ENUM_TSV_PACKET.TSV_PACKET_WIN_KIOSK
        ' 210 - "Win Kiosk"
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(210)

      Case ENUM_TSV_PACKET.TSV_PACKET_WIN_CASHIER
        ' 211 - "Win Cashier"
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(211)

      Case ENUM_TSV_PACKET.TSV_PACKET_WIN_GUI
        ' 212 - "Win GUI"
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(212)

      Case ENUM_TSV_PACKET.TSV_PACKET_WCP_SERVICE
        ' 226 - "WCP Service"
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(226)

      Case ENUM_TSV_PACKET.TSV_PACKET_WC2_SERVICE
        ' 227 - "WC2 Service"
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(227)

      Case ENUM_TSV_PACKET.TSV_PACKET_WXP_SERVICE
        ' 335 - "WXP Service"
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(335)

      Case ENUM_TSV_PACKET.TSV_PACKET_SAS_HOST
        ' 321 - "SAS Host"
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(321)

      Case ENUM_TSV_PACKET.TSV_PACKET_SITE_JACKPOT
        ' 328 - "Site Jackpot"
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(328)

      Case ENUM_TSV_PACKET.TSV_PACKET_MOBILE_BANK
        ' 329 - "Mobile Bank"
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(329)

      Case ENUM_TSV_PACKET.TSV_PACKET_MOBILE_BANK_IMB
        ' 330 - "iMB"
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(330)

      Case ENUM_TSV_PACKET.TSV_PACKET_ISTATS
        ' 331 - "iStats"
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(331)

      Case ENUM_TSV_PACKET.TSV_PACKET_WWP_SITE_SERVICE
        ' 333 - "WWP Site Service"
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(333)

      Case ENUM_TSV_PACKET.TSV_PACKET_WWP_CENTER_SERVICE
        ' 334 - "WWP Center Service"
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(334)
      Case ENUM_TSV_PACKET.TSV_PACKET_PROMOBOX
        ' 336 - "PromoBOX"
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(336)

      Case ENUM_TSV_PACKET.TSV_PACKET_WIN_MULTISITE_GUI
        ' 337 - "Wigos Multisite GUI"
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(337)

      Case ENUM_TSV_PACKET.TSV_PACKET_PSA_CLIENT
        ' 338 - "Wigos Multisite GUI"
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(338)

        'JCA CashDeskDraws
      Case ENUM_TSV_PACKET.TSV_PACKET_WIN_CASHDESK_DRAWS_GUI
        ' 339 - "Wigos Cashdesk Draws GUI"
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(339)

      Case ENUM_TSV_PACKET.TSV_PACKET_SMARTFLOOR_DATA_SERVICE
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(340)

      Case ENUM_TSV_PACKET.TSV_PACKET_AFIP_CLIENT
        ' 341 - "AFIP Client"
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(341)

      Case ENUM_TSV_PACKET.TSV_PACKET_PROTOCOLS_SERVICE
        ' 342 - "Protocols Service"
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(342)

      Case Else
        ' 332 - "Type: %1"
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(332, TerminalType.ToString())

    End Select

  End Function ' GetTerminalTypeName

  ' PURPOSE : Get the status description
  '
  '  PARAMS:
  '     - INPUT:
  '         - Terminal Type
  '
  '     - OUTPUT:
  '
  ' RETURNS : Terminal type name

  Public Shared Function GetStatusDescription(ByVal Status As ENUM_PACKAGE_STATUS) As String

    Select Case Status
      Case CLASS_SW_PACKAGE.ENUM_PACKAGE_STATUS.STATUS_OK
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(313)

      Case CLASS_SW_PACKAGE.ENUM_PACKAGE_STATUS.STATUS_ERROR
        Return GLB_NLS_GUI_SW_DOWNLOAD.GetString(314)

      Case Else
        Return ""

    End Select

  End Function ' GetStatusDescription

  ' PURPOSE : Get the software package destination locations (2) from the GENERAL_PARAMS table
  '
  '  PARAMS:
  '     - INPUT:
  '         - Location1
  '         - Location2
  '
  '     - OUTPUT:
  '
  ' RETURNS : TRUE on success or FALSE on error

  Public Shared Function GetPackageLocations(ByRef SwPackage As CLASS_SW_PACKAGE) As Boolean

    Dim general_params As DataTable
    Dim sql_text As String
    Dim key_name As String
    Dim key_value As String
    Dim idx_row As Integer
    Dim idx_location As Integer

    ' Get the package locations 
    '     - Take base locations from the GENERAL_PARAMS
    '     - Build the destination location full path 

    SwPackage.NumLocations = 0
    SwPackage.Location(0) = ""
    SwPackage.Location(1) = ""

    '     - Take base locations from the GENERAL_PARAMS
    sql_text = "select GP_SUBJECT_KEY, GP_KEY_VALUE" _
              & " from GENERAL_PARAMS" _
             & " where GP_GROUP_KEY = 'Software.Download'"

    general_params = GUI_GetTableUsingSQL(sql_text, 2)

    If IsNothing(general_params) Then
      Return False
    End If

    If general_params.Rows.Count() < 1 Then
      Return False
    End If

    For idx_row = 0 To general_params.Rows.Count - 1
      key_name = general_params.Rows.Item(idx_row).Item("GP_SUBJECT_KEY").ToString.ToUpper
      key_value = general_params.Rows.Item(idx_row).Item("GP_KEY_VALUE").ToString.Trim.ToUpper

      ' Check for empty values
      If key_value = "" Then
        Continue For
      End If

      If key_name = "LOCATION1" Then
        SwPackage.Location(0) = key_value
      ElseIf key_name = "LOCATION2" Then
        SwPackage.Location(1) = key_value
      End If
    Next

    SwPackage.NumLocations = 0
    For idx_location = 0 To MAX_LOCATIONS - 1
      If SwPackage.Location(idx_location) <> "" Then
        SwPackage.NumLocations += 1
      End If
    Next

    ' Check that both entries are actually different
    If SwPackage.NumLocations > 1 Then
      If SwPackage.Location(0) = SwPackage.Location(1) Then
        SwPackage.NumLocations = 1
        SwPackage.Location(1) = ""
      End If
    End If

    If SwPackage.Location(0) = "" Then
      SwPackage.Location(0) = SwPackage.Location(1)
      SwPackage.Location(1) = ""
    End If

    Return True

  End Function ' GetPackageLocations

  ' PURPOSE : Try to copy the extracted files to both locations
  '
  '  PARAMS:
  '     - INPUT:
  '         - SwPackage
  '
  '     - OUTPUT:
  '
  ' RETURNS : Number of successful copies

  Public Shared Function CopyToLocations(ByRef SwPackage As CLASS_SW_PACKAGE) As Integer

    Dim idx_location As Integer
    Dim bool_rc As Boolean
    Dim num_copies As Integer = 0
    Dim rc As Integer
    Dim temp_path As String

    temp_path = New String(Chr(0), PATH_LENGTH)
    rc = Common_ExpandPackageFile(SwPackage.FullFileName, temp_path)

    If rc = 0 Then

      ' Expansion process was successful: proceed with copy process
      For idx_location = 0 To SwPackage.NumLocations - 1
        bool_rc = FtpToLocation(temp_path, SwPackage.Location(idx_location), SwPackage)

        If (bool_rc) Then
          num_copies += 1
          SwPackage.CopyStatus(idx_location) = ENUM_PACKAGE_STATUS.STATUS_OK
        Else
          SwPackage.CopyStatus(idx_location) = ENUM_PACKAGE_STATUS.STATUS_ERROR
        End If
      Next

      ' Clean temporary path and remove it
      Common_CleanPath(temp_path, True)
    End If

    Return num_copies

  End Function ' CopyToLocations

  ' PURPOSE : Retrieve list of files and information related to a package from the locations
  '
  '  PARAMS:
  '     - INPUT:
  '         - SwPackage
  '
  '     - OUTPUT:
  '
  ' RETURNS : TRUE if successful, FALSE otherwise
  ' 
  ' NOTES :
  '     It will always try to retrieve it from the first location, if it cannot locate the path 
  ' or there are no files inside it will try the second location

  Public Shared Function RetrievePackageInfoFromFtpLocation(ByRef SwPackage As CLASS_SW_PACKAGE) As Boolean

    Dim idx_location As Integer
    Dim idx_file As Integer
    Dim rc As Boolean
    Dim ftp_handle As Integer
    Dim search_handle As Integer
    Dim ftp_context As Integer
    Dim ftp_location As String
    Dim target_path As String
    Dim module_info As New CLASS_SW_PACKAGE.TYPE_SW_PACKAGE_MODULE
    Dim module_info_array(0 To MAX_PACKAGE_MODULES - 1) As CLASS_SW_PACKAGE.TYPE_SW_PACKAGE_MODULE

    ' These handles are used within the FTP functions
    ftp_handle = 0
    ftp_context = 0
    search_handle = 0

    Try

      SwPackage.NumModules = 0
      For idx_location = 0 To SwPackage.NumLocations - 1

        SwPackage.CopyStatus(idx_location) = ENUM_PACKAGE_STATUS.STATUS_ERROR

        ' Establish connection to FTP site
        ftp_location = SwPackage.Location(idx_location)
        rc = Common_FtpConnect(ftp_location, FTP_USERNAME, FTP_PASSWORD, ftp_handle, ftp_context)
        If Not rc Then

          Continue For
          'Return False
        End If

        target_path = SwPackage.LocationPath
        idx_file = 0
        rc = Common_FtpGetFileDetails(ftp_handle, search_handle, target_path, module_info)

        While rc
          module_info_array(idx_file) = New CLASS_SW_PACKAGE.TYPE_SW_PACKAGE_MODULE
          module_info_array(idx_file).name = module_info.name
          module_info_array(idx_file).size = module_info.size
          module_info_array(idx_file).version = module_info.version
          idx_file += 1

          rc = Common_FtpGetFileDetails(ftp_handle, search_handle, target_path, module_info)
        End While

        SwPackage.NumModules = idx_file

        ' No modules information could be retrieved
        If idx_file = 0 Then
          Continue For
          'Return False
        End If

        For idx_file = 0 To SwPackage.NumModules - 1
          SwPackage.PackageModule(idx_file) = module_info_array(idx_file)
        Next

        ' Update package status
        SwPackage.CopyStatus(idx_location) = ENUM_PACKAGE_STATUS.STATUS_OK

        ' There is no need to proceed with next location if files are already copied
        Exit For
      Next

      If SwPackage.NumModules > 0 Then
        Return True
      Else
        Return False
      End If

    Finally
      ' Close ftp session
      Common_FtpDisconnect(ftp_handle)

    End Try

  End Function ' RetrievePackageInfoFromFtpLocation

  Public Shared Function CheckSwPackageExists(ByRef SwPackage As CLASS_SW_PACKAGE, _
                                              ByVal Context As Integer) As Boolean
    Dim str_sql As String
    Dim data_table As DataTable

    str_sql = "select count(*)" _
             & " from TERMINAL_SOFTWARE_VERSIONS" _
            & " where TSV_CLIENT_ID = " & SwPackage.ClientId.ToString _
              & " and TSV_BUILD_ID = " & SwPackage.BuildId.ToString _
              & " and TSV_TERMINAL_TYPE = " & SwPackage.TerminalType.ToString

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)
    If IsNothing(data_table) Then
      Return False
    End If

    ' Parse the read data
    If data_table.Rows(0).Item(0) > 0 Then
      Return True
    End If

    Return False

  End Function ' CheckSwPackage

  Public Shared Function CleanFtpLocation(ByRef SwPackage As CLASS_SW_PACKAGE) As Integer

    Dim idx_location As Integer
    Dim rc As Boolean
    Dim num_successful As Integer = 0
    Dim ftp_handle As Integer
    Dim ftp_context As Integer
    Dim ftp_location As String
    Dim ftp_path As String

    ftp_handle = 0
    ftp_context = 0

    ' Try to delete files from locations, including the container directory
    For idx_location = 0 To SwPackage.NumLocations - 1
      SwPackage.CopyStatus(idx_location) = ENUM_PACKAGE_STATUS.STATUS_ERROR

      ftp_location = SwPackage.Location(idx_location)
      rc = Common_FtpConnect(ftp_location, FTP_USERNAME, FTP_PASSWORD, ftp_handle, ftp_context)

      If rc Then
        ftp_path = SwPackage.LocationPath
        rc = Common_CleanFtpPath(ftp_handle, ftp_path, True)

        If rc Then
          ' Update package status
          SwPackage.CopyStatus(idx_location) = ENUM_PACKAGE_STATUS.STATUS_OK
          num_successful += 1
        End If
      End If
    Next

    ' Close ftp session
    Common_FtpDisconnect(ftp_handle)

    Return num_successful

  End Function 'CleanFtpLocation

#End Region ' Public Functions

End Class ' CLASS_SW_PACKAGE
