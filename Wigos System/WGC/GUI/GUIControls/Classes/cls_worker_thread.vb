'-------------------------------------------------------------------
' Copyright � 2003 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_worker_thread.vb
' DESCRIPTION:   Class that implements a worker thread. Any application
'                that wants to use it must define a Task derived from 
'                CLASS_TASK
' AUTHOR:        Andreu Juli�
' CREATION DATE: 06-AUG-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 06-AUG-2002  AJQ    Initial version
'-------------------------------------------------------------------
Imports GUI_CommonMisc

Public MustInherit Class CLASS_TASK

  Public Event StatusChanged()

  Public Enum Status
    NotQueued = 0
    Pending = 1
    Running = 2
    Executed = 3
    Canceled = 4
    Failed = 5
  End Enum

  Private m_status As Status
  Private m_error As String

  Public MustOverride Sub Execute()

  Public ReadOnly Property State() As CLASS_TASK.Status
    Get
      Return m_status
    End Get
  End Property

  Protected Sub New()
    m_status = Status.NotQueued
    m_error = String.Empty
  End Sub

  Public Sub Cancel()
    Me.SetStatus(Status.Canceled)
  End Sub

  Friend Sub SetStatus(ByVal NewStatus As Status)
    If m_status <> NewStatus Then
      m_status = NewStatus
      RaiseEvent StatusChanged()
    End If
  End Sub


  Friend Sub Start()
    Dim execute_task As Boolean

    Select Case m_status
      Case Status.Pending
        execute_task = True
      Case Else
        execute_task = False
    End Select

    If execute_task Then
      Try
        Me.SetStatus(Status.Running)
        Me.Execute()
        Me.SetStatus(Status.Executed)

      Catch ex As Exception
        m_error = ex.Message
        Me.SetStatus(Status.Failed)

      Finally
        If m_status <> Status.Executed Then
          If Me.m_status <> Status.Failed Then
            m_error = "Unknown Error"
          End If
          Me.SetStatus(Status.Failed)
        End If
      End Try
    End If

  End Sub
End Class

Public Class CLASS_WORKER_THREAD

  Public Event TaskBeginEvent(ByRef Task As CLASS_TASK)
  Public Event TaskEndEvent(ByRef Task As CLASS_TASK)
  Public Event TaskEvent(ByRef Task As CLASS_TASK)

  Private m_init As Boolean = False
  ' The Thread itself
  Private m_thread As System.Threading.Thread
  ' The Events
  Private m_event_stop As System.Threading.ManualResetEvent
  Private m_event_new_task As System.Threading.ManualResetEvent

  Private m_running_tasks As Integer = 0

  ' The TaskQueue
  Private m_queue As Queue
  Private m_sync_queue As Queue

  Private Sub Run()
    Dim continue_working As Boolean
    Dim wait_handles() As System.Threading.WaitHandle
    Dim idx_handle As Integer
    Const IDX_HANDLE_STOP As Integer = 0
    Const IDX_HANDLE_TASK As Integer = 1
    Dim task As CLASS_TASK
    'XVV Variable not use Dim task_executed As Boolean

    ReDim wait_handles(1)

    continue_working = True
    wait_handles(IDX_HANDLE_STOP) = m_event_stop
    wait_handles(IDX_HANDLE_TASK) = m_event_new_task

    Try
      '
      While continue_working
        idx_handle = System.Threading.WaitHandle.WaitAny(wait_handles)

        Select Case idx_handle
          Case IDX_HANDLE_TASK

            If m_sync_queue.Count > 0 Then

              task = m_sync_queue.Dequeue

              If Not (task Is Nothing) Then

                Try
                  RaiseEvent TaskBeginEvent(task)
                  m_running_tasks += 1
                  task.Start()

                Catch ex As Exception
                  Debug.WriteLine("*** EXCEPTION ***")
                  Debug.WriteLine(ex.Message)

                Finally
                  If m_running_tasks > 0 Then
                    m_running_tasks -= 1
                  End If

                  RaiseEvent TaskEndEvent(task)

                End Try

              End If

            End If

            If m_sync_queue.Count = 0 Then
              Call m_event_new_task.Reset()
            End If

          Case IDX_HANDLE_STOP
            continue_working = False

          Case Else
            '
        End Select
      End While

    Catch ex As Exception
      Debug.WriteLine("*** EXCEPTION ***")
      Debug.WriteLine(ex.Message)

    Finally
      '
    End Try

  End Sub

  Public Sub EnqueueTask(ByVal Task As CLASS_TASK)
    Me.m_sync_queue.Enqueue(Task)
    Task.SetStatus(CLASS_TASK.Status.Pending)
    Me.m_event_new_task.Set()
  End Sub

  Public Sub RemovePendingTasks()
    Call m_sync_queue.Clear()
  End Sub


  Public Sub Init()
    ' Create the thread
    m_thread = New Threading.Thread(AddressOf Me.Run)
    ' Assign a name ... 
    m_thread.Name = "WORKER THREAD"
    ' Set the inizialization flag
    m_init = True
    '
    m_queue = New Queue
    m_sync_queue = Queue.Synchronized(m_queue)
    Call m_sync_queue.Clear()
    ' Create the events
    m_event_stop = New System.Threading.ManualResetEvent(False)
    m_event_new_task = New System.Threading.ManualResetEvent(False)
    '  Start the thread
    Call m_thread.Start()

  End Sub

  Public Sub Done()
    If Not m_init Then
      Exit Sub
    End If

    m_init = False

    ' Signal the StopEvent
    Call m_event_stop.Set()
    ' Wait thread termination
    '' Changed by Agusti on 09-SEP-2002 to allow immediate closing
    'Call m_thread.Join()
    ''Call m_thread.Abort()

    '' Release Resources
    'm_event_stop = Nothing
    'm_event_new_task = Nothing
    'm_sync_queue = Nothing
    'm_queue = Nothing
    'm_thread = Nothing
  End Sub

  Public Sub RunningTasks(ByRef Pending As Integer, ByRef Running As Integer)
    Pending = m_sync_queue.Count
    Running = m_running_tasks
  End Sub

End Class
