'-------------------------------------------------------------------
' Copyright � 2003 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_filter.vb
' DESCRIPTION:   The filter used by all the EntryFields.
' AUTHOR:        Andreu Juli�
' CREATION DATE: 11-APR-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ ------------------------------------------------------------------
' 11-APR-2002  AJQ    Initial version
' 08-FEB-2013  LEM    Added new format FORMAT_PHONE_NUMBER
' 13-FEB-2013  LEM    Restricted special characters to FORMAT_NO_NUMBERS
' 30-MAY-2013  JCA    Added new ENUM FORMAT_PROVIDERS_NAME to validate providers name
' 08-AUG-2013  MMG    Added new ENUMS FORMAT_NUMBER_ALSO_NEGATIVES  and FORMAT_MONEY_ALSO_NEGATIVES
' 22-AUG-2013  MMG    Modified FilterKeyCharNegative function
' 03-FEB-2015  OPC    Added new filter by day, month and year.
' 10-NOV-2017  LTC    WIGOS-6496: warning in log file for MultisiteGUI - Invalid object name 'CURRENCY_EXCHANGE'
'---------------------------------------------------------------------------------------
Imports System.Threading

Public Class CLASS_FILTER
  Private format_type As ENUM_FORMAT
  Private format_len As Integer
  Private format_decimals As Integer
  Private format_name As String

  Private Const LEN_MAX_TEXT As Integer = 512
  Private Const LEN_MIN_TEXT As Integer = 0
  Private Const LEN_MAX_NUMBER As Integer = 16
  Private Const LEN_MIN_NUMBER As Integer = 1
  Private Const LEN_MAX_MONEY As Integer = 17
  Private Const LEN_MIN_MONEY As Integer = 1
  Private Const LEN_MAX_HEXA As Integer = 8
  Private Const LEN_MIN_HEXA As Integer = 1
  Private Const LEN_MAX_TIME_ZONE As Integer = 6

  Private Const LEN_MIN_DATE_DAY As Integer = 1
  Private Const LEN_MAX_DATE_DAY As Integer = 2
  Private Const LEN_MIN_DATE_MONTH As Integer = 1
  Private Const LEN_MAX_DATE_MONTH As Integer = 2
  Private Const LEN_MIN_DATE_YEAR As Integer = 4
  Private Const LEN_MAX_DATE_YEAR As Integer = 4

  Public Enum ENUM_FORMAT
    FORMAT_TEXT = 1
    FORMAT_NUMBER = 2
    FORMAT_MONEY = 3
    FORMAT_ALPHA = 4
    FORMAT_ALPHA_NUMERIC = 5
    FORMAT_HEXA = 6
    FORMAT_IP_ADDRESS = 7
    FORMAT_DIRECTORY = 8
    FORMAT_MODULE_NAME = 9
    FORMAT_TIME_ZONE = 10
    FORMAT_CHARACTER_BASE = 11
    FORMAT_PHONE_NUMBER = 12
    FORMAT_TRACK_DATA = 13
    FORMAT_CHARACTER_EXTENDED = 14
    FORMAT_RAW_NUMBER = 15         'Number without separators
    FORMAT_PROVIDERS_NAME = 16     'Letters, Numbers and  � - _ # . / ( ) +
    FORMAT_NUMBER_ALSO_NEGATIVES = 17     'positive and negative numbers
    FORMAT_MONEY_ALSO_NEGATIVES = 18     'Money accepting negative sign 
    FORMAT_DATE_DAY = 19                 'Positive numbers from 1 to 31
    FORMAT_DATE_MONTH = 20               'Positive numbers from 1 to 12
    FORMAT_DATE_YEAR = 21                'Positive numbers from 1900 to 9999
  End Enum

  Public Sub Init(ByVal FormatType As ENUM_FORMAT, _
                  Optional ByVal FormatLength As Integer = 0, _
                  Optional ByVal FormatDecimals As Integer = GUI_CommonOperations.mdl_number_format.DEFAULT_DECIMAL_DIGITS_NUMBER)

    Dim len_max As Integer
    Dim len_min As Integer

    format_type = FormatType
    format_len = FormatLength
    format_decimals = IIf(FormatDecimals = GUI_CommonOperations.mdl_number_format.DEFAULT_DECIMAL_DIGITS_NUMBER, 0, FormatDecimals)

    Select Case FormatType
      Case ENUM_FORMAT.FORMAT_TEXT _
        , ENUM_FORMAT.FORMAT_DIRECTORY
        format_name = "FORMAT_TEXT"
        len_max = LEN_MAX_TEXT
        len_min = LEN_MIN_TEXT

      Case ENUM_FORMAT.FORMAT_NUMBER
        format_name = "FORMAT_NUMBER"
        len_max = LEN_MAX_NUMBER
        len_min = LEN_MIN_NUMBER

      Case ENUM_FORMAT.FORMAT_MONEY
        format_name = "FORMAT_MONEY"
        len_max = LEN_MAX_MONEY
        len_min = LEN_MIN_MONEY

        ' LTC 10-NOV-2017
        If WSI.Common.Misc.IsMultisiteCenter() Then
          ' In order to avoid currency_exchange validation - No currency exchange in Multisite
          format_decimals = GUI_CommonOperations.mdl_number_format.DECIMAL_DIGITS_CURRENCY
        Else
          format_decimals = IIf(FormatDecimals = GUI_CommonOperations.mdl_number_format.DEFAULT_DECIMAL_DIGITS_NUMBER, WSI.Common.Format.GetFormattedDecimals(WSI.Common.CurrencyExchange.GetNationalCurrency()), FormatDecimals)
        End If

      Case ENUM_FORMAT.FORMAT_HEXA
        format_name = "FORMAT_HEXA"
        len_max = LEN_MAX_HEXA
        len_min = LEN_MIN_HEXA

      Case ENUM_FORMAT.FORMAT_TIME_ZONE
        format_name = "FORMAT_TIME_ZONE"
        len_max = LEN_MAX_TIME_ZONE
        len_min = LEN_MAX_TIME_ZONE
        format_len = LEN_MAX_TIME_ZONE

      Case ENUM_FORMAT.FORMAT_PHONE_NUMBER
        format_name = "FORMAT_PHONE_NUMBER"

      Case ENUM_FORMAT.FORMAT_CHARACTER_BASE
        format_name = "FORMAT_CHARACTER_BASE"

      Case ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED
        format_name = "FORMAT_CHARACTER_EXTENDED"

      Case ENUM_FORMAT.FORMAT_TRACK_DATA
        format_name = "FORMAT_TRACK_DATA"

      Case ENUM_FORMAT.FORMAT_RAW_NUMBER
        format_name = "FORMAT_RAW_NUMBER"

      Case ENUM_FORMAT.FORMAT_PROVIDERS_NAME
        format_name = "FORMAT_PROVIDERS_NAME"

      Case ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES
        format_name = "FORMAT_NUMBER_ALSO_NEGATIVES"
        len_max = LEN_MAX_NUMBER
        len_min = LEN_MIN_NUMBER

      Case ENUM_FORMAT.FORMAT_MONEY_ALSO_NEGATIVES
        format_name = "FORMAT_MONEY_ALSO_NEGATIVES"
        len_max = LEN_MAX_MONEY
        len_min = LEN_MIN_MONEY

        ' LTC 10-NOV-2017
        If WSI.Common.Misc.IsMultisiteCenter() Then
          ' In order to avoid currency_exchange validation - No currency exchange in Multisite
          format_decimals = GUI_CommonOperations.mdl_number_format.DECIMAL_DIGITS_CURRENCY
        Else
          format_decimals = IIf(FormatDecimals = GUI_CommonOperations.mdl_number_format.DEFAULT_DECIMAL_DIGITS_NUMBER, WSI.Common.Format.GetFormattedDecimals(WSI.Common.CurrencyExchange.GetNationalCurrency()), FormatDecimals)
        End If

      Case ENUM_FORMAT.FORMAT_DATE_DAY
        format_name = "FORMAT_MONEY_ALSO_NEGATIVES"
        len_max = LEN_MAX_DATE_DAY
        len_min = LEN_MIN_DATE_DAY
        format_len = LEN_MAX_DATE_DAY

      Case ENUM_FORMAT.FORMAT_DATE_MONTH
        format_name = "FORMAT_MONEY_ALSO_NEGATIVES"
        len_max = LEN_MAX_DATE_MONTH
        len_min = LEN_MIN_DATE_MONTH
        format_len = LEN_MAX_DATE_MONTH

      Case ENUM_FORMAT.FORMAT_DATE_YEAR
        format_name = "FORMAT_MONEY_ALSO_NEGATIVES"
        len_max = LEN_MAX_DATE_YEAR
        len_min = LEN_MIN_DATE_YEAR
        format_len = LEN_MAX_DATE_YEAR

      Case Else
        format_name = "FORMAT_XXX"

    End Select

    If format_len > 0 Then
      format_name = format_name & " " & CStr(format_len)
    End If
    If format_decimals > 0 Then
      format_name = format_name & "." & CStr(format_decimals)
    End If

  End Sub

  Public Sub New(Optional ByVal FilterFormatType As ENUM_FORMAT = ENUM_FORMAT.FORMAT_TEXT, _
                 Optional ByVal FilterFormatLength As Integer = 0, _
                 Optional ByVal FilterFormatDecimals As Integer = 0)

    Init(FilterFormatType, FilterFormatLength, FilterFormatDecimals)

  End Sub

  Public Property FormatType() As ENUM_FORMAT
    Get
      Return format_type
    End Get
    Set(ByVal Value As ENUM_FORMAT)
      format_type = Value
    End Set
  End Property

  Public Property FormatLen() As Integer
    Get
      Return format_len
    End Get
    Set(ByVal Value As Integer)
      format_len = Value
    End Set
  End Property

  Public Property FormatDecimals() As Integer
    Get
      Return format_decimals
    End Get
    Set(ByVal Value As Integer)
      format_decimals = Value
    End Set
  End Property

  Public ReadOnly Property FormatName() As String
    Get
      Return format_name
    End Get
  End Property

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '        - KeyChar
  '        - CurrentText
  '        - SelectedText
  '        - IndexPosition
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Public Function FilterKeyChar(ByVal KeyChar As Char, _
                                ByVal CurrentText As String, _
                                ByVal SelectedText As Boolean, _
                                ByVal IndexPosition As Integer) As Boolean
    'XVV Variable not use Dim restrict As Boolean
    Dim aux_str As String
    Dim num_points As Integer
    'XVV Variable not use Dim not_valid As Boolean

    Dim decimal_separator_currency As String
    Dim decimal_separator_number As String


    decimal_separator_currency = Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator
    decimal_separator_number = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator

    ' Delete is always allowed
    If KeyChar = vbBack Then
      Return True
    End If

    ' Check Length
    If Len(CurrentText) >= format_len Then
      If Not SelectedText Then
        Return False
      End If
    End If

    Select Case Me.format_type
      Case ENUM_FORMAT.FORMAT_ALPHA
        If (KeyChar >= "A"c And KeyChar <= "Z"c) Or _
           (KeyChar >= "a"c And KeyChar <= "z"c) Then
          Return True
        Else
          Return False
        End If

      Case ENUM_FORMAT.FORMAT_ALPHA_NUMERIC
        If (KeyChar >= "A"c And KeyChar <= "Z"c) Or _
           (KeyChar >= "a"c And KeyChar <= "z"c) Or _
           (KeyChar >= "0"c And KeyChar <= "9"c) Then
          Return True
        Else
          Return False
        End If

      Case ENUM_FORMAT.FORMAT_HEXA
        If (KeyChar >= "A"c And KeyChar <= "F"c) Or _
           (KeyChar >= "a"c And KeyChar <= "f"c) Or _
           (KeyChar >= "0"c And KeyChar <= "9"c) Then
          Return True
        Else
          Return False
        End If

      Case ENUM_FORMAT.FORMAT_IP_ADDRESS
        If (KeyChar >= "."c) Or _
           (KeyChar >= "0"c And KeyChar <= "9"c) Then

          aux_str = CurrentText
          num_points = 0
          Do While InStr(aux_str, ".") > 0
            num_points = num_points + 1
            aux_str = Mid$(aux_str, InStr(aux_str, ".") + 1)
          Loop

          If num_points >= 3 And Not (KeyChar >= "0"c And KeyChar <= "9"c) Then
            Return False
          End If

          Return True
        Else
          Return False
        End If

      Case ENUM_FORMAT.FORMAT_MONEY
        If (KeyChar = decimal_separator_currency) Or _
           (KeyChar >= "0"c And KeyChar <= "9"c) Then

          If (KeyChar = decimal_separator_currency) Then
            If (Me.format_decimals = 0) Then
              Return False
            End If
            If InStr(CurrentText, decimal_separator_currency) > 0 Then
              Return False
            End If
          End If

          Return True
        Else
          Return False
        End If

      Case ENUM_FORMAT.FORMAT_MONEY_ALSO_NEGATIVES

        Return True

      Case ENUM_FORMAT.FORMAT_NUMBER
        If (KeyChar = decimal_separator_number) Or _
           (KeyChar >= "0"c And KeyChar <= "9"c) Then

          If (KeyChar = decimal_separator_number) Then
            If (Me.format_decimals = 0) Then
              Return False
            End If
            If InStr(CurrentText, decimal_separator_number) > 0 Then
              Return False
            End If
          End If

          Return True
        Else
          Return False
        End If

      Case ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES

        Return True

      Case ENUM_FORMAT.FORMAT_TEXT
        Return True


      Case ENUM_FORMAT.FORMAT_MODULE_NAME
        If (KeyChar >= "A"c And KeyChar <= "Z"c) Or _
           (KeyChar >= "a"c And KeyChar <= "z"c) Or _
           (KeyChar >= "0"c And KeyChar <= "9"c) Or _
           (KeyChar = "_") Then
          Return True
        Else
          Return False
        End If


      Case ENUM_FORMAT.FORMAT_DIRECTORY
        If (KeyChar = ">"c) Or _
           (KeyChar = "<"c) Or _
           (KeyChar = "?"c) Or _
           (KeyChar = "/"c) Or _
           (KeyChar = "*"c) Or _
           (KeyChar = Chr(34)) Or _
           (KeyChar = "|"c) Then
          Return False
        End If

        Return True

      Case ENUM_FORMAT.FORMAT_TIME_ZONE
        If (KeyChar >= "0"c And KeyChar <= "9"c) Or _
           (KeyChar = "+"c) Or _
           (KeyChar = "-"c) Or _
           (KeyChar = ":"c) Then

          If KeyChar = "+"c Or KeyChar = "-"c Then
            If InStr(CurrentText, "+") > 0 Then
              Return False
            End If
            If InStr(CurrentText, "-") > 0 Then
              Return False
            End If
            Return True
          End If

          If KeyChar = ":"c Then
            If InStr(CurrentText, ":") > 0 Then
              Return False
            End If
            Return True
          End If

          Return True

        End If

      Case ENUM_FORMAT.FORMAT_CHARACTER_BASE
        Return WSI.Common.ValidateFormat.CheckCharacter(KeyChar, False)

      Case ENUM_FORMAT.FORMAT_CHARACTER_EXTENDED
        Return WSI.Common.ValidateFormat.CheckCharacter(KeyChar, True)

      Case ENUM_FORMAT.FORMAT_PHONE_NUMBER
        Return WSI.Common.ValidateFormat.CheckPhoneNumber(KeyChar)

      Case ENUM_FORMAT.FORMAT_RAW_NUMBER _
         , ENUM_FORMAT.FORMAT_TRACK_DATA
        Return Char.IsDigit(KeyChar)

      Case ENUM_FORMAT.FORMAT_PROVIDERS_NAME
        Return WSI.Common.ValidateFormat.CheckProviderCharacter(KeyChar, True)

      Case ENUM_FORMAT.FORMAT_DATE_DAY
        If (KeyChar >= "0"c And KeyChar <= "9"c) Then
          Dim _concat As String

          If String.IsNullOrEmpty(CurrentText) Or SelectedText Then
            Return True
          End If

          If IndexPosition = 0 Then
            _concat = String.Concat(KeyChar.ToString(), CurrentText)
          Else
            _concat = String.Concat(CurrentText, KeyChar.ToString())
          End If

          If CInt(_concat) <= 31 Then
            Return True
          Else
            Return False
          End If

          Return True
        Else
          Return False
        End If

      Case ENUM_FORMAT.FORMAT_DATE_MONTH
        If (KeyChar >= "0"c And KeyChar <= "9"c) Then
          Dim _concat As String

          If String.IsNullOrEmpty(CurrentText) Or SelectedText Then
            Return True
          End If

          If IndexPosition = 0 Then
            _concat = String.Concat(KeyChar.ToString(), CurrentText)
          Else
            _concat = String.Concat(CurrentText, KeyChar.ToString())
          End If

          If CInt(_concat) <= 12 Then
            Return True
          Else
            Return False
          End If

          Return True
        Else
          Return False
        End If

      Case ENUM_FORMAT.FORMAT_DATE_YEAR
        If (KeyChar >= "0"c And KeyChar <= "9"c) Then
          Return True
        Else
          Return False
        End If

      Case Else
        Return False

    End Select

    Return False

  End Function   ' FilterKeyChar


  ' 09-AUG-2013 MMG
  ' PURPOSE: Validates the KeyChar for ENUM_FORMAT.FORMAT_MONEY_ALSO_NEGATIVES and ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - KeyChar to validate.
  '           - Text that will contains the KeyChar.
  '           - Boolean indicating if the text is selected.
  '           - Cursor position in the text.
  '     - OUTPUT:
  '           Boolean idicating if the KeyChar passed or not the validation.
  '             -Returns "True" in the following cases:
  '                 *The entry field format is ENUM_FORMAT.FORMAT_MONEY_ALSO_NEGATIVES or ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES
  '                  and also the KeyChar is a valid character in one of these two formats.
  '                 *The entry field format is another format of the ENUM_FORMAT.
  '
  '             -Returns "False" if the entry field format is ENUM_FORMAT.FORMAT_MONEY_ALSO_NEGATIVES or ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES
  '              and the KeyChar is not a valid character for these formats.
  '
  ' RETURNS:
  '     - None
  Public Function FilterKeyCharNegative(ByVal KeyChar As Char, _
                                ByVal CurrentText As String, _
                                ByVal SelectedText As Boolean, _
                                ByVal CursorPosition As Integer) As Boolean

    Dim decimal_separator_currency As String
    Dim decimal_separator_number As String


    decimal_separator_currency = Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator
    decimal_separator_number = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator

    ' Delete is always allowed
    If KeyChar = vbBack Then

      Return True
    End If

    Select Case Me.format_type

      Case ENUM_FORMAT.FORMAT_MONEY_ALSO_NEGATIVES
        If (KeyChar = decimal_separator_currency) Or _
           (KeyChar = "-"c) Or _
           (KeyChar >= "0"c And KeyChar <= "9"c) Then

          If (KeyChar = decimal_separator_currency) Then
            'Don't let to write decimal separator if Me.format_decimals = 0
            If (Me.format_decimals = 0) Then

              Return False
            End If

            'Don't let to write decimal separator if it already exists
            If InStr(CurrentText, decimal_separator_currency) > 0 Then

              Return False
            End If

            'Don't let to write decimal separator if there are no numbers between the "-" sign and it
            If CurrentText = "-" Then

              Return False
            End If

          End If

          'Don't let  to write "-" sign if it already exists nor if cursor position is not the first one
          If (KeyChar = "-"c) And Not SelectedText Then
            If (InStr(CurrentText, "-"c) > 0) Or _
               (CursorPosition > 0) Then

              Return False
            End If
          End If

          'Don't let  to write any number on the left of the "-" sign
          If InStr(CurrentText, "-"c) > 0 And Not SelectedText Then
            If (CursorPosition < InStr(CurrentText, "-"c)) Then

              Return False
            End If
          End If

          Return True

        Else

          Return False

        End If

      Case ENUM_FORMAT.FORMAT_NUMBER_ALSO_NEGATIVES
        If (KeyChar = decimal_separator_number) Or _
           (KeyChar = "-"c) Or _
           (KeyChar >= "0"c And KeyChar <= "9"c) Then

          If (KeyChar = decimal_separator_number) Then
            'Don't let to write decimal separator if Me.format_decimals = 0
            If (Me.format_decimals = 0) Then

              Return False
            End If

            'Don't let to write decimal separator if it already exists
            If InStr(CurrentText, decimal_separator_number) > 0 Then

              Return False
            End If

            'Don't let to write decimal separator if there are no numbers between the "-" sign and it
            If CurrentText = "-" Then

              Return False
            End If

          End If

          'Don't let  to write "-" sign if it already exists nor if cursor position is not the first one
          If (KeyChar = "-"c) And Not SelectedText Then
            If InStr(CurrentText, "-"c) > 0 Or _
               (CursorPosition > 0) Then

              Return False
            End If
          End If

          'Don't let  to write any number on the left of the "-" sign
          If InStr(CurrentText, "-"c) > 0 And Not SelectedText Then
            If (CursorPosition < InStr(CurrentText, "-"c)) Then

              Return False
            End If
          End If

          Return True

        Else

          Return False

        End If

      Case Else

        Return True

    End Select

    Return True

  End Function ' FilterKeyCharNegative

End Class

