'-------------------------------------------------------------------
' Copyright � 2007-2008 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_host_user.vb
' DESCRIPTION:   Host User
' AUTHOR:        AJQ
' CREATION DATE: 16-APR-2007
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 16-APR-2007  AJQ    Initial version
' 26-AUG-2002  JSV    Rename property IsPwdExpired to PwdChangeRequested and make it read-write.
' 05-SEP-2002  JSV    Replace private constant COMMONMISC_USER_MAX_FORMS_BY_GUI for that in CLASS_GUI_FORMS.MAX_FORMS_BY_GUI 
' 26-JAN-2012  RCI & AJQ    Type TYPE_PERMISSIONS is "overriden" in GUIControls - cls_gui.vb.
' 15-MAR-2012  RCI & AJQ    Added property Reports to access the user allowed reports. Used in frm_reports.
' 29-MAR-2012  MPO    User session: New feature in "SetUserLogged" and "Logout"
' 16-AUG-2012  XCD    Adapted disable user with GU_ENABLED to GU_BLOCK_REASON
' 23-AUG-2012  JAB    Check MaxLoginAttempts parameter
' 29-AUG-2012  JCM    Added non alpha numeric chars for password
'                     Added case sensitive password
' 29-JUL-2013  DHA    Changed the length from the field 'Password' from 15 to 22 characters
' 10-SEP-2013  JPJ    Defect WIG-196 User is never disabled if user is master and in a multiSite
'-------------------------------------------------------------------

Option Explicit On
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports System.Runtime.InteropServices
Imports System.Windows.Forms
Imports System.Data.SqlClient

Public Class CLASS_GUI_USER

#Region " CommonMisc "

#Region " CommonMisc Constants "
  Private Const COMMONMISC_USER_FULL_NAME_LENGTH As Integer = 20
  Private Const COMMONMISC_USER_VB_DATE_LENGTH As Integer = 25  'VB: Visual Basic Format, 'month  dd, yyyy'
#End Region ' CommonMisc Constants

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
  Public Class TYPE_COMMONMISC_USER_INFO
    Public control_block As Integer
    Public user_id As Integer
    Public profile_id As Integer
    Public enabled As Integer
    Public not_valid_before As New TYPE_DATE_TIME
    Public not_valid_after As New TYPE_DATE_TIME
    Public last_changed As New TYPE_DATE_TIME()
    Public password_exp As Integer
    Public pwd_chg_req As Integer
    Public login_failures As Integer
    Public priv_level As Integer
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=COMMONMISC_USER_FULL_NAME_LENGTH + 1)> _
    Public full_name As String
    Public current_date As New TYPE_DATE_TIME()
    Public gu_master As Boolean
  End Class

  'Structure TYPE_COMMONMISC_USER_INFO
  '  Dim control_block As Integer
  '  Dim user_id As Integer
  '  Dim profile_id As Integer
  '  Dim enabled As Integer
  '  Dim h_inst_id As Integer
  '  Dim last_changed As String         'length COMMONMISC_USER_VB_DATE_LENGTH + 1
  '  Dim password_exp As Integer
  '  Dim pwd_chg_req As Integer
  '  Dim login_failures As Integer
  '  Dim priv_level As Integer
  '  Dim full_name As String            'length COMMONMISC_USER_FULL_NAME_LENGTH + 1
  '  Dim current_date As String         'length COMMONMISC_USER_VB_DATE_LENGTH + 1
  'End Structure

  Structure TYPE_COMMONMISC_USER_PERMISSIONS_INFO
    Dim control_block As Integer
    Dim form_id As Integer
    Dim read As Integer
    Dim write As Integer
    Dim delete As Integer
    Dim execute As Integer
  End Structure

#End Region ' CommonMisc Structures

#End Region ' CommonMisc

#Region " Constants "

  Private Const DB_USER_DISABLED As Boolean = False
  Private Const PASSWORD_CHANGE_REQUESTED As Boolean = True
  Private Const PASSWORD_NEVER_EXPIRES As Integer = 0


  Private Const GUI_SUPER_USER_ID As Integer = 0

#End Region ' Constants

#Region " Enumerates "

  Friend Enum ENUM_PASSWORD_ERROR
    OK = 0
    ACCESS_DENIED = 1
    USER_DISABLED = 2
  End Enum

  'Mode of password checking through the Package
  'CURRENT_ONLY: the Package checks only the current password, user in login
  'CURRENT_AND_HISTORY: Pkg checks password both Current and History, used in Password Change

#End Region ' Enumerates

#Region " Structures "
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  '' RCI & AJQ 26-JAN-2012: This Type is "overriden" in GUIControls - cls_gui.vb.
  ''                        Take it in account in case of modifcation in any of the two files.
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  Public Structure TYPE_PERMISSIONS
    Dim Read As Boolean
    Dim Write As Boolean
    Dim Delete As Boolean
    Dim Execute As Boolean
  End Structure

  Private Structure TYPE_USER_FORM_PERMISSIONS
    Dim form_id As Integer
    Dim permission As TYPE_PERMISSIONS
  End Structure

#End Region ' Structures

#Region " Members "
  Private m_user_id As Integer = -1
  Private m_gui_id As Integer = -1
  Private m_profile_id As Integer = -1
  Private m_user_password As String
  Private m_enabled As Boolean = False
  Private m_user_name As String
  'Private m_priv_level As Integer
  Private m_user_full_name As String
  Private m_num_permissions As Integer
  Private m_user_permissions() As TYPE_USER_FORM_PERMISSIONS
  Private m_not_valid_before As Date
  Private m_not_valid_after As Date
  Private m_password_change_requested As Boolean
  Private m_last_changed_date As Date
  Private m_center_date As Date
  Private m_password_expiration_days As Integer
  Private m_login_failures As Integer
  Private m_user_logged As Boolean
  Private m_gu_master As Boolean
#End Region ' Members

#Region " Properties "

  Public Property Id() As Integer
    Get
      Return m_user_id
    End Get
    Set(ByVal Value As Integer)
      m_user_id = Value
    End Set
  End Property

  Public ReadOnly Property IsSuperUser() As Boolean
    Get
      Return (m_user_id = GUI_SUPER_USER_ID)
    End Get
  End Property

  Public ReadOnly Property Name() As String
    Get
      Return m_user_name
    End Get
  End Property

  Public ReadOnly Property Password() As String
    Get
      Return m_user_password
    End Get
  End Property

  Public ReadOnly Property FullName() As String
    Get
      Return m_user_full_name
    End Get
  End Property

  Public ReadOnly Property GuiId() As Integer
    Get
      Return m_gui_id
    End Get
  End Property

  Public ReadOnly Property IsMaster() As Boolean
    Get
      Return m_gu_master
    End Get
  End Property

  Public ReadOnly Property Permissions(ByVal FormId As Integer) As TYPE_PERMISSIONS
    Get
      Dim local_permissions As TYPE_PERMISSIONS
      Dim idx As Integer

      ' The Super User allways have permisions for all actions
      If CurrentUser.IsSuperUser Then

        local_permissions.Read = True
        local_permissions.Write = True
        local_permissions.Delete = True
        local_permissions.Execute = True

        Return local_permissions

      End If

      ' look for the FormId
      For idx = 0 To m_num_permissions - 1
        If m_user_permissions(idx).form_id = FormId Then
          Return m_user_permissions(idx).permission
        End If
      Next

      ' All Users can access to the change password form if no permisions set for this form on that gui.
      If FormId = ENUM_COMMON_FORM.COMMON_FORM_PWD_CHANGE _
       Or FormId = ENUM_FORM.FORM_MAIN Then

        local_permissions.Read = True
        local_permissions.Write = True
        local_permissions.Delete = True
        local_permissions.Execute = True

        Return local_permissions

      End If

      'if no form found then return no permissions
      local_permissions.Read = False
      local_permissions.Write = False
      local_permissions.Delete = False
      local_permissions.Execute = False

      Return local_permissions

    End Get
  End Property

  Public ReadOnly Property PermissionsOtherUser(ByVal FormId As Integer) As TYPE_PERMISSIONS
    Get
      Dim local_permissions As TYPE_PERMISSIONS
      Dim idx As Integer

      ' look for the FormId
      For idx = 0 To m_num_permissions - 1
        If m_user_permissions(idx).form_id = FormId Then
          Return m_user_permissions(idx).permission
        End If
      Next

      ' All Users can access to the change password form if no permisions set for this form on that gui.
      If FormId = ENUM_COMMON_FORM.COMMON_FORM_PWD_CHANGE _
       Or FormId = ENUM_FORM.FORM_MAIN Then

        local_permissions.Read = True
        local_permissions.Write = True
        local_permissions.Delete = True
        local_permissions.Execute = True

        Return local_permissions

      End If

      'if no form found then return no permissions
      local_permissions.Read = False
      local_permissions.Write = False
      local_permissions.Delete = False
      local_permissions.Execute = False

      Return local_permissions

    End Get
  End Property

  Public ReadOnly Property IsLogged() As Boolean
    Get
      Return m_user_logged
    End Get
  End Property

  Public ReadOnly Property ProfileId() As Integer
    Get
      Return m_profile_id
    End Get
  End Property

  Public ReadOnly Property IsEnabled() As Boolean
    Get
      Return m_enabled
    End Get
  End Property

  Public Property PwdChangeRequested() As Boolean
    Get
      Return m_password_change_requested
    End Get
    Set(ByVal Value As Boolean)
      m_password_change_requested = Value
    End Set
  End Property

  Public ReadOnly Property Reports() As List(Of Report)
    Get
      Dim _user_list As New List(Of Report)

      For Each _report As Report In Report.Reports
        If _report.ReportPermissions.Read Then
          _user_list.Add(_report)
        End If
      Next

      Return _user_list
    End Get
  End Property

#End Region 'Properties

#Region " Public Methods "

  Public Sub New()
  End Sub

#End Region ' Public Methods

#Region " Private Methods "

  ' PURPOSE: Initializes fixed length strings in the structure for the output
  '          data when calling Common_GetHostUserInfo function in CommonMisc.dll
  '          and inits the control block
  '   
  '    - INPUT:
  '
  '    - OUTPUT:
  '       - CommonMiscUserInfo: Structure to set control block.
  '
  ' RETURNS:
  ' 
  '
  ' NOTES
  '
  Private Sub InitCommonMiscUserInfoStruct(ByRef CommonMiscUserInfo As TYPE_COMMONMISC_USER_INFO)

    ' Inits control block
    CommonMiscUserInfo.control_block = Marshal.SizeOf(CommonMiscUserInfo)

  End Sub 'InitCommonMiscUserInfoStruct

  ' PURPOSE: Saves User Forms Permissions from database structure to User Control structure 
  '          
  '    - INPUT:
  '       - NumPermissions: Number of forms with permissions found in database
  '       - DbUserPermissions: User permissions in Database format
  '    - OUTPUT:
  '       - UserPermissions: Permissions of the User Class
  '
  ' RETURNS:
  '
  ' NOTES:
  '    
  Private Sub SavePermissionData(ByRef UserPermissions() As TYPE_USER_FORM_PERMISSIONS, _
                                 ByVal NumPermissions As Integer, _
                                 ByVal DbUserPermissions() As TYPE_COMMONMISC_USER_PERMISSIONS_INFO)
    Dim idx As Integer
    ReDim UserPermissions(NumPermissions)
    m_num_permissions = NumPermissions

    For idx = 0 To NumPermissions - 1
      UserPermissions(idx).form_id = DbUserPermissions(idx).form_id
      UserPermissions(idx).permission.Read = CBool(DbUserPermissions(idx).read)
      UserPermissions(idx).permission.Write = CBool(DbUserPermissions(idx).write)
      UserPermissions(idx).permission.Delete = CBool(DbUserPermissions(idx).delete)
      UserPermissions(idx).permission.Execute = CBool(DbUserPermissions(idx).execute)
    Next

  End Sub ' SavePermissionData

  ' PURPOSE: Obtain user data from database based on username.
  '   
  '    - INPUT:
  '       - User: User string.
  '       - context: Database context.
  '
  '    - OUTPUT:
  '        - UserInfo: Data related to user.
  '
  ' RETURNS:
  '   - COMMON_OK
  '   - COMMON_ERROR_DB
  '
  ' NOTES:
  '
  Private Function Common_GetHostUserInfo(ByVal User As String, _
                                          ByVal UserInfo As TYPE_COMMONMISC_USER_INFO, _
                                          ByVal context As Integer) As Integer
    Dim str_sql As String
    Dim str_upper_first_letter As String
    Dim str_lower_first_letter As String
    Dim data_table As DataTable
    Dim date_exp As Date

    str_upper_first_letter = Left(User, 1) & "%"
    str_lower_first_letter = LCase(str_upper_first_letter) & "%"

    str_sql = "SELECT   GU_USER_ID " & _
                     ", GU_PROFILE_ID " & _
                     ", GU_BLOCK_REASON " & _
                     ", ISNULL (GU_NOT_VALID_BEFORE, '1900/01/01 00:00:00' ) AS GU_NOT_VALID_BEFORE " & _
                     ", ISNULL (GU_NOT_VALID_AFTER, '1900/01/01 00:00:00' ) AS GU_NOT_VALID_AFTER " & _
                     ", ISNULL (GU_LAST_CHANGED, '1900/01/01 00:00:00' ) AS GU_LAST_CHANGED " & _
                     ", ISNULL(GU_PASSWORD_EXP, '1900/01/01 00:00:00') AS GU_PASSWORD_EXP " & _
                     ", GU_PWD_CHG_REQ " & _
                     ", ISNULL(GU_LOGIN_FAILURES, 0) AS GU_LOGIN_FAILURES " & _
                     ", ISNULL(GU_FULL_NAME, ' ') AS GU_FULL_NAME " & _
                     ", CASE WHEN GU_MASTER_ID IS NULL THEN 0 ELSE 1 END AS GU_MASTER " & _
     " FROM GUI_USERS " & _
              " WHERE   UPPER(GU_USERNAME) = '" & User & "' " & _
                " AND   (   GU_USERNAME LIKE '" & str_upper_first_letter & "' " & _
                       " OR GU_USERNAME LIKE '" & str_lower_first_letter & "' " & " ) "

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)
    If IsNothing(data_table) Then
      Return ENUM_COMMON_RC.COMMON_ERROR_DB
    End If

    If data_table.Rows.Count() > 1 Then
      Return ENUM_COMMON_RC.COMMON_ERROR_DB
    End If

    If data_table.Rows.Count() = 0 Then
      Return ENUM_COMMON_RC.COMMON_ERROR_DB
    End If

    UserInfo.user_id = data_table.Rows(0).Item("GU_USER_ID")
    UserInfo.profile_id = data_table.Rows(0).Item("GU_PROFILE_ID")
    '' XCD 16-Aug-2012 GU_ENABLED DEPRECATED, USE GU_BLOCK REASON
    'UserInfo.enabled = IIf(data_table.Rows(0).Item("GU_ENABLED"), 1, 0)
    UserInfo.enabled = IIf(data_table.Rows(0).Item("GU_BLOCK_REASON"), 0, 1)
    UserInfo.not_valid_before.Value = data_table.Rows(0).Item("GU_NOT_VALID_BEFORE")
    UserInfo.not_valid_after.Value = data_table.Rows(0).Item("GU_NOT_VALID_AFTER")
    If UserInfo.not_valid_after.Value = "1900/01/01 00:00:00" Then
      UserInfo.not_valid_after.Value = GUI_GetDateTime()
      UserInfo.not_valid_after.Value = UserInfo.not_valid_after.Value.AddDays(1)
    End If
    date_exp = data_table.Rows(0).Item("GU_PASSWORD_EXP")
    If date_exp = "1900/01/01 00:00:00" Then
      UserInfo.password_exp = 0
    Else
      UserInfo.password_exp = date_exp.Subtract(UserInfo.not_valid_before.Value).Days()
    End If

    UserInfo.pwd_chg_req = data_table.Rows(0).Item("GU_PWD_CHG_REQ")
    UserInfo.login_failures = data_table.Rows(0).Item("GU_LOGIN_FAILURES")
    UserInfo.current_date.Value = GUI_GetDateTime()
    UserInfo.last_changed.Value = data_table.Rows(0).Item("GU_LAST_CHANGED")
    UserInfo.last_changed.Value = UserInfo.last_changed.Value.Date
    UserInfo.full_name = NullTrim(data_table.Rows(0).Item("GU_FULL_NAME"))
    UserInfo.gu_master = data_table.Rows(0).Item("GU_MASTER")
    ' priv_level not used
    UserInfo.priv_level = 0

    Return ENUM_COMMON_RC.COMMON_OK

  End Function ' Common_GetHostUserInfo


  ' PURPOSE: Obtain form permissions for a specific profile.
  '   
  '    - INPUT:
  '       - User: User string.
  '       - ProfileId: profile.
  '       - GUIId: 
  '       - context: Database context.
  '
  '    - OUTPUT:
  '        - NumPermissions: Number of permissions retrieved.
  '        - pUserPermisInfo: Permissions data.
  '
  ' RETURNS:
  '   - COMMON_OK
  '   - COMMON_ERROR_DB
  '
  ' NOTES:
  '
  Private Function Common_GetUserPermissions(ByVal User As String, _
                                             ByVal ProfileId As Integer, _
                                             ByVal GUIId As Integer, _
                                             ByRef NumPermissions As Integer, _
                                             ByRef pUserPermisInfo() As TYPE_COMMONMISC_USER_PERMISSIONS_INFO, _
                                             ByVal context As Integer) As Integer
    Dim db_profile_id As Integer
    Dim rc As Integer

    'XVV 16/04/2007
    'Assign nothing because compiler generate warning for not have value
    Dim user_info As TYPE_COMMONMISC_USER_INFO = Nothing

    Dim str_sql As String
    Dim idx_db_row As Integer
    Dim num_db_rows As Integer = 0
    Dim data_table As DataTable

    If User = vbNullString Then
      db_profile_id = ProfileId
    Else
      rc = Common_GetHostUserInfo(User, user_info, context)
      If rc <> ENUM_COMMON_RC.COMMON_OK Then
        Return rc
      Else
        db_profile_id = user_info.profile_id
      End If
    End If

    str_sql = "SELECT GPF_FORM_ID " & _
                   ", GPF_READ_PERM " & _
                   ", GPF_WRITE_PERM " & _
                   ", GPF_DELETE_PERM " & _
                   ", GPF_EXECUTE_PERM " & _
               " FROM GUI_PROFILE_FORMS " & _
              " WHERE GPF_PROFILE_ID = " & db_profile_id & _
                " AND GPF_GUI_ID = " & GUIId

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)

    If IsNothing(data_table) Then
      Return ENUM_COMMON_RC.COMMON_ERROR_DB
    End If

    num_db_rows = data_table.Rows.Count()

    For idx_db_row = 0 To num_db_rows - 1

      pUserPermisInfo(idx_db_row).form_id = data_table.Rows(idx_db_row).Item("GPF_FORM_ID")
      pUserPermisInfo(idx_db_row).read = IIf(data_table.Rows(idx_db_row).Item("GPF_READ_PERM"), 1, 0)
      pUserPermisInfo(idx_db_row).write = IIf(data_table.Rows(idx_db_row).Item("GPF_WRITE_PERM"), 1, 0)
      pUserPermisInfo(idx_db_row).delete = IIf(data_table.Rows(idx_db_row).Item("GPF_DELETE_PERM"), 1, 0)
      pUserPermisInfo(idx_db_row).execute = IIf(data_table.Rows(idx_db_row).Item("GPF_EXECUTE_PERM"), 1, 0)

    Next

    NumPermissions = num_db_rows

    Return ENUM_COMMON_RC.COMMON_OK

  End Function ' Common_GetUserPermissions

  ' PURPOSE: Obtain form permissions for a specific profile.
  '   
  '    - INPUT:
  '       - ProfileId: profile.
  '       - context: Database context.
  '
  '    - OUTPUT:
  '        - NumPermissions: Number of permissions retrieved.
  '        - pUserPermisInfo: Permissions data.
  '
  ' RETURNS:
  '   - COMMON_OK
  '   - COMMON_ERROR_DB
  '
  ' NOTES:
  ' JBM 23-NOV-2017
  '
  Private Function Common_GetProfilePermissions(ByVal ProfileId As Integer, _
                                             ByRef NumPermissions As Integer, _
                                             ByRef pUserPermisInfo() As TYPE_COMMONMISC_USER_PERMISSIONS_INFO, _
                                             ByVal GUIId As Integer, _
                                             ByVal context As Integer)

    Dim str_sql As String
    Dim idx_db_row As Integer
    Dim num_db_rows As Integer = 0
    Dim data_table As DataTable

    str_sql = "SELECT GPF_FORM_ID " & _
               ", GPF_READ_PERM " & _
               ", GPF_WRITE_PERM " & _
               ", GPF_DELETE_PERM " & _
               ", GPF_EXECUTE_PERM " & _
           " FROM GUI_PROFILE_FORMS " & _
          " WHERE GPF_PROFILE_ID = " & ProfileId &
          " AND GPF_GUI_ID = " & GUIId

    data_table = GUI_GetTableUsingSQL(str_sql, 5000)

    If IsNothing(data_table) Then
      Return ENUM_COMMON_RC.COMMON_ERROR_DB
    End If

    num_db_rows = data_table.Rows.Count()

    For idx_db_row = 0 To num_db_rows - 1

      pUserPermisInfo(idx_db_row).form_id = data_table.Rows(idx_db_row).Item("GPF_FORM_ID")
      pUserPermisInfo(idx_db_row).read = IIf(data_table.Rows(idx_db_row).Item("GPF_READ_PERM"), 1, 0)
      pUserPermisInfo(idx_db_row).write = IIf(data_table.Rows(idx_db_row).Item("GPF_WRITE_PERM"), 1, 0)
      pUserPermisInfo(idx_db_row).delete = IIf(data_table.Rows(idx_db_row).Item("GPF_DELETE_PERM"), 1, 0)
      pUserPermisInfo(idx_db_row).execute = IIf(data_table.Rows(idx_db_row).Item("GPF_EXECUTE_PERM"), 1, 0)

    Next

    NumPermissions = num_db_rows

    Return ENUM_COMMON_RC.COMMON_OK

  End Function

  ' PURPOSE: get permissions of specific user (not current)
  '   
  '    - INPUT:
  '       - GUIId:
  '       - context: Database context.
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '   - COMMON_OK
  '   - COMMON_ERROR_DB
  '
  ' NOTES:
  Public Function GetSpecificUserPermissions(ByVal GUIId As Integer, SqlCtx As Integer)
    If GetUserPermissions(GUIId, SqlCtx) Then
      Return ENUM_COMMON_RC.COMMON_OK
    Else
      Return ENUM_COMMON_RC.COMMON_ERROR_DB
    End If

  End Function

  ' PURPOSE: get permissions of specific profile (not current)
  '   
  '    - INPUT:
  '       - GUIId:
  '       - ProfileId: Internal profile identifier.
  '       - context: Database context.
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '   - COMMON_OK
  '   - COMMON_ERROR_DB
  '
  ' NOTES:
  ' JBM 23-NOV-2017
  Public Function GetSpecificProfilePermissions(ByVal GUIId As Integer, ByVal ProfileId As Integer, SqlCtx As Integer)
    If GetProfilePermissions(GUIId, ProfileId, SqlCtx) Then
      Return ENUM_COMMON_RC.COMMON_OK
    Else
      Return ENUM_COMMON_RC.COMMON_ERROR_DB
    End If

  End Function


  ' PURPOSE: Increment login failures.
  '   
  '    - INPUT:
  '       - UserId: Internal user identifier.
  '       - context: Database context.
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '   - COMMON_OK
  '   - CONFIGURATION_ERROR_DB
  '
  ' NOTES:
  Private Function Common_IncreaseLoginFailure(ByVal UserId As Integer, _
                                               ByVal context As Integer) As Integer
    Dim str_sql As String

    str_sql = "UPDATE GUI_USERS " & _
                " SET GU_LOGIN_FAILURES = ISNULL(GU_LOGIN_FAILURES, 0) + 1 " & _
              " WHERE GU_USER_ID = " & UserId

    If Not GUI_SQLExecuteNonQuery(str_sql) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Return ENUM_COMMON_RC.COMMON_OK

  End Function ' Common_IncreaseLoginFailure

  ' PURPOSE: Set user as not enabled.
  '   
  '    - INPUT:
  '       - UserId: Internal user identifier.
  '       - context: Database context.
  '       - BlockReason: Block reason
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '   - COMMON_OK
  '   - CONFIGURATION_ERROR_DB
  '
  ' NOTES:
  Private Function Common_DisableUserInDb(ByVal UserId As Integer, _
                                          ByVal context As Integer, _
                                          ByVal BlockReason As WSI.Common.GUI_USER_BLOCK_REASON) As Integer
    Dim str_sql As String

    str_sql = "UPDATE GUI_USERS " & _
                " SET GU_BLOCK_REASON = GU_BLOCK_REASON | " & BlockReason

    If BlockReason.Equals(WSI.Common.GUI_USER_BLOCK_REASON.WRONG_PASSWORD) Then
      str_sql = str_sql & " , GU_LOGIN_FAILURES = 0 "
    End If

    str_sql = str_sql & " WHERE GU_USER_ID = " & UserId

    If Not GUI_SQLExecuteNonQuery(str_sql) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Return ENUM_COMMON_RC.COMMON_OK

  End Function ' Common_DisableUserInDb

  ' PURPOSE: Reset user login failures to zero.
  '   
  '    - INPUT:
  '       - UserId: Internal user identifier.
  '       - context: Database context.
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '   - COMMON_OK
  '   - CONFIGURATION_ERROR_DB
  '
  ' NOTES:
  Private Function Common_ResetLoginFailures(ByVal UserId As Integer, _
                                             ByVal context As Integer) As Integer
    Dim str_sql As String

    str_sql = "UPDATE GUI_USERS " & _
                " SET GU_LOGIN_FAILURES = 0 " & _
              " WHERE GU_USER_ID = " & UserId

    If Not GUI_SQLExecuteNonQuery(str_sql) Then
      Return ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DB
    End If

    Return ENUM_COMMON_RC.COMMON_OK

  End Function ' Common_ResetLoginFailures

#End Region ' Private Methods

#Region " CommonMisc Interfaces "

  ' PURPOSE: Disables the user into the Database via CommonMisc.dll 
  '   
  '    - INPUT:
  '             UserName: user to be disabled
  '
  '    - OUTPUT:
  '              None
  '
  ' RETURNS:
  '          TRUE: User disabled OK
  '          FALSE: Error disabling the user
  ' NOTES
  '
  Private Function DisableUserInDB(ByVal UserId As Integer, ByRef SqlCtx As Integer, ByVal BlockReason As WSI.Common.GUI_USER_BLOCK_REASON) As Boolean
    Dim rc As Integer

    ' call CommonMisc to disable the user
    rc = Common_DisableUserInDb(UserId, SqlCtx, BlockReason)
    If rc <> CInt(ENUM_COMMON_RC.COMMON_OK) Then
      'database error
      GLB_NLS_GUI_CONTROLS.MsgBox(107, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Return False

    End If

    Return True
  End Function

  ' PURPOSE: resets the login failure into the Database via CommonMisc.dll 
  '   
  '    - INPUT:
  '             UserName: user to be cleaned
  '    - OUTPUT:
  '              None
  '
  ' RETURNS:
  '          TRUE: database updated OK
  '          FALSE: Error updating the database
  ' NOTES
  '
  Private Function ResetLoginFailures(ByVal UserId As Integer, ByRef SqlCtx As Integer) As Boolean
    Dim rc As Integer

    rc = Common_ResetLoginFailures(UserId, SqlCtx)
    If rc <> CInt(ENUM_COMMON_RC.COMMON_OK) Then
      'database error
      GLB_NLS_GUI_CONTROLS.MsgBox(107, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Return False
    End If

    Return True
  End Function

#End Region ' CommonMisc Intefaces

#Region " New Functions to be Rearranged"

  ' PURPOSE: Check if the user is valid into database 
  '          saves the user info into a temporary global variable
  '          
  '    - INPUT:
  '              UserName: 
  '
  '    - OUTPUT:
  '              None
  '
  ' RETURNS:
  '          TRUE:   User OK
  '          FALSE:  Invalid UserFails.
  '          
  ' NOTES
  '
  Public Function GetUserInDb(ByVal UserName As String, ByRef SqlCtx As Integer) As Boolean
    Dim user_db_info As New TYPE_COMMONMISC_USER_INFO
    Dim rc As Integer

    ' initializes the fixed length strings for the CommonMisc Output data
    InitCommonMiscUserInfoStruct(user_db_info)

    'get user info from database calling the CommonMisc
    rc = Common_GetHostUserInfo(UserName, user_db_info, SqlCtx)
    If rc <> CInt(ENUM_COMMON_RC.COMMON_OK) Then
      'User not found
      Return False

    End If

    'Save into local variables
    m_enabled = CBool(user_db_info.enabled)
    m_user_full_name = user_db_info.full_name
    m_user_name = UCase(UserName)
    m_user_id = user_db_info.user_id
    m_login_failures = user_db_info.login_failures
    m_profile_id = user_db_info.profile_id
    m_gu_master = user_db_info.gu_master

    'saves dates to check expiration
    m_password_expiration_days = user_db_info.password_exp
    If Not user_db_info.last_changed.IsNull Then
      m_last_changed_date = user_db_info.last_changed.Value
    Else
      m_last_changed_date = user_db_info.not_valid_before.Value
    End If
    m_center_date = user_db_info.current_date.Value
    m_password_change_requested = CBool(user_db_info.pwd_chg_req)

    If Not user_db_info.not_valid_before.IsNull Then
      m_not_valid_before = user_db_info.not_valid_before.Value
    Else
      m_not_valid_before = user_db_info.current_date.Value
    End If

    If Not user_db_info.not_valid_after.IsNull Then
      m_not_valid_after = user_db_info.not_valid_after.Value
    Else
      m_not_valid_after = user_db_info.current_date.Value
    End If

    ' user ok
    Return True

  End Function

  ' PURPOSE: Checks the User's Password via CommonMisc using the 
  '          function: Check_Gui_Pwd in the Package LK_ENCRYPT_PKG
  '          This package handles the passwords encryption.
  '          
  '    - INPUT:
  '              Password: password to be checked through the package
  '
  '    - OUTPUT: None
  '
  ' RETURNS:
  '         ENUM_PASSWORD_ERROR
  '                            OK:             Password OK
  '                            ACCESS_DENIED:  Password checking Fails or database error
  '                            USER_DISABLED:  User was disabled due to password failures
  ' NOTES
  '
  Friend Function CheckPasswordLoginPkg(ByVal Password As String, ByRef SqlCtx As Integer) As ENUM_PASSWORD_ERROR
    Dim common_misc_rc As Integer
    Dim rc As Boolean
    Dim _pwd_policy As WSI.Common.PasswordPolicy
    Dim _pwd_rc As ENUM_PASSWORD_ERROR

    _pwd_policy = New WSI.Common.PasswordPolicy()

    Using _db_trx As New WSI.Common.DB_TRX()
      If _pwd_policy.VerifyCurrentPassword(m_user_id, Password, _db_trx.SqlTransaction) Then
        common_misc_rc = ENUM_COMMON_RC.COMMON_OK
      Else
        common_misc_rc = ENUM_COMMON_RC.COMMON_ERROR_PASSWORD
      End If

      _db_trx.Commit()
    End Using

    If common_misc_rc <> CInt(ENUM_COMMON_RC.COMMON_OK) Then

      ' Super user acount never is disabled
      If Not IsSuperUser Then
        '//JPJ 10-SEP-2013: Defect WIG-196 Never is disabled if user is master and in a multiSite
        If Not (WSI.Common.GeneralParam.GetBoolean("Site", "MultiSiteMember", False) AndAlso IsMaster) Then
          'increase login failure counter
          m_login_failures = m_login_failures + 1

          'updated login failures into database
          common_misc_rc = Common_IncreaseLoginFailure(m_user_id, SqlCtx)
          If common_misc_rc <> CInt(ENUM_COMMON_RC.COMMON_OK) Then
            Return ENUM_PASSWORD_ERROR.ACCESS_DENIED
          End If

          ''check if login failure limit was reached
          If m_login_failures >= _pwd_policy.MaxLoginAttempts Then
            If m_enabled = False Then
              _pwd_rc = ENUM_PASSWORD_ERROR.ACCESS_DENIED
            Else
              _pwd_rc = ENUM_PASSWORD_ERROR.USER_DISABLED
            End If

            m_enabled = DB_USER_DISABLED

            'aborts login and disable the user
            rc = DisableUserInDB(m_user_id, SqlCtx, WSI.Common.GUI_USER_BLOCK_REASON.WRONG_PASSWORD)
            If rc = False Then
              'database error, aborts login
              Return ENUM_PASSWORD_ERROR.ACCESS_DENIED

            End If ' DisableUserInDB(UserName)

            'user disabled
            Return _pwd_rc

          End If ' GLB_UserDbInfo.login_failures
        End If ' IsMaster
      End If ' IsSuperUser

      'returns
      Return ENUM_PASSWORD_ERROR.ACCESS_DENIED

    End If

    'reset login failures
    rc = ResetLoginFailures(m_user_id, SqlCtx)
    If rc = False Then
      'database error

      Return ENUM_PASSWORD_ERROR.ACCESS_DENIED
    End If

    'pwd ok
    m_user_password = Password

    Return ENUM_PASSWORD_ERROR.OK

  End Function ' CheckPasswordLoginPkg

  ' PURPOSE: Checks the User's Password via CommonMisc using the 
  '          function: Check_Gui_Pwd in the Package LK_ENCRYPT_PKG
  '          This package handles the passwords encryption.
  '          
  '    - INPUT:
  '              UserId:   Gui user identifier related to GUI_USERS.GU_USER_ID
  '              Password: password to be checked through the package
  '
  '    - OUTPUT: None
  '
  ' RETURNS:
  '          TRUE:   Password insertion OK
  '          FALSE:  Password insertion Fails.
  '          
  ' NOTES
  '
  Public Function ChangeUserPassword(ByVal UserId As Integer, _
                                    ByVal Password As String, _
                                    ByRef SqlCtx As Integer) As Boolean

    Dim _pwd As WSI.Common.PasswordPolicy

    Using _db_trx As New WSI.Common.DB_TRX()
      _pwd = New WSI.Common.PasswordPolicy()
      If (_pwd.ChangeUserPassword(UserId, Password, _db_trx.SqlTransaction)) Then
        _db_trx.Commit()

        m_user_password = Password
        m_password_change_requested = False

        Return True
      End If
    End Using


    GLB_NLS_GUI_CONTROLS.MsgBox(104, mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

    Return False

  End Function ' InsertGuiPassword

  ' PURPOSE: Update a user password.
  '   
  '    - INPUT:
  '       - UserId: Internal user identifier.
  '       - Password: Password to be checked.
  '       - SqlTrans: Database context.
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '   - COMMON_ERROR_DB
  '   - COMMON_OK
  '
  ' NOTES:
  Shared Function Common_SetNewPassword(ByVal UserId As Integer, _
                                        ByVal Password As String, _
                                        ByVal Trx As SqlTransaction) As Integer
    Dim _pwd As WSI.Common.PasswordPolicy

    _pwd = New WSI.Common.PasswordPolicy()

    If (_pwd.SetUserPassword(UserId, Password, Trx)) Then
      Return ENUM_COMMON_RC.COMMON_OK
    End If

    Return ENUM_COMMON_RC.COMMON_ERROR_DB

  End Function ' Common_SetNewPassword

  ' PURPOSE: Gets User permissions from the Database via CommonMisc.dll 
  '   
  '    - INPUT:
  '             PrifileId: User profile Id
  '
  '    - OUTPUT:
  '             UserPermissions: Permissions obtained from database
  '
  ' RETURNS:
  '          TRUE: User permissions retrieved OK
  '          FALSE: Error getting the User permissions retrieved
  ' NOTES
  '       SuperUser hasn't got permisions in database
  '
  Friend Function GetUserPermissions(ByVal GUIId As ENUM_GUI, ByRef SqlCtx As Integer) As Boolean
    Dim cm_user_permissions() As TYPE_COMMONMISC_USER_PERMISSIONS_INFO
    Dim num_of_permission As Integer
    Dim rc As Integer
    Dim database_gui_id As Integer
    ReDim cm_user_permissions(CLASS_GUI_FORMS.MAX_FORMS_BY_GUI)

    ' the superuser hasn'h got permisions in database
    If IsSuperUser Then
      m_num_permissions = 0
      m_user_permissions = Nothing
      Return True
    End If

    database_gui_id = CInt(GUIId)
    'gets user permissions info from the database via CommonMisc
    cm_user_permissions(0).control_block = Len(cm_user_permissions(0))

    rc = Common_GetUserPermissions(vbNullString, m_profile_id, database_gui_id, num_of_permission, cm_user_permissions, SqlCtx)
    If rc <> CInt(ENUM_COMMON_RC.COMMON_OK) Then
      'Permissions not found
      Return False
    End If

    'saves user permissions from datbase
    m_num_permissions = num_of_permission
    ReDim m_user_permissions(m_num_permissions)

    'save permissions into the User Class
    SavePermissionData(m_user_permissions, m_num_permissions, cm_user_permissions)

    Return True

  End Function

  ' PURPOSE: Gets profile permissions from the Database via CommonMisc.dll 
  '   
  '    - INPUT:
  '             ProfileId: Profile Id
  '
  '    - OUTPUT:
  '             UserPermissions: Permissions obtained from database
  '
  ' RETURNS:
  '          TRUE: Profile permissions retrieved OK
  '          FALSE: Error getting the profile permissions retrieved
  ' NOTES
  '       SuperUser hasn't got permisions in database
  ' JBM 23-NOV-2017
  '
  Friend Function GetProfilePermissions(ByVal GUIId As ENUM_GUI, ByVal ProfileId As Integer, ByRef SqlCtx As Integer) As Boolean
    Dim cm_user_permissions() As TYPE_COMMONMISC_USER_PERMISSIONS_INFO
    Dim num_of_permission As Integer
    Dim rc As Integer
    Dim database_gui_id As Integer
    ReDim cm_user_permissions(CLASS_GUI_FORMS.MAX_FORMS_BY_GUI)

    ' the superuser hasn'h got permisions in database
    If IsSuperUser Then
      m_num_permissions = 0
      m_user_permissions = Nothing
      Return True
    End If

    database_gui_id = CInt(GUIId)
    'gets user permissions info from the database via CommonMisc
    cm_user_permissions(0).control_block = Len(cm_user_permissions(0))

    rc = Common_GetProfilePermissions(ProfileId, num_of_permission, cm_user_permissions, database_gui_id, SqlCtx)
    If rc <> CInt(ENUM_COMMON_RC.COMMON_OK) Then
      'Permissions not found
      Return False
    End If

    'saves user permissions from datbase
    m_num_permissions = num_of_permission
    ReDim m_user_permissions(m_num_permissions)

    'save permissions into the User Class
    SavePermissionData(m_user_permissions, m_num_permissions, cm_user_permissions)

    Return True

  End Function

  ' PURPOSE: Checks if the user has to change the password
  '   
  '    - INPUT: None
  '
  '    - OUTPUT: None
  '
  ' RETURNS:
  '          TRUE: User has to change the password
  '          FALSE: User doesn't have to change the password
  ' NOTES
  '
  Friend Function CheckPasswordChange() As Boolean
    Dim rc As Boolean

    If m_password_change_requested = PASSWORD_CHANGE_REQUESTED Then
      'password has to be changed
      Return True
    End If

    If m_password_expiration_days <> PASSWORD_NEVER_EXPIRES Then
      rc = CheckPasswordExpiration()
      If rc = True Then
        ' password has expired, ask the user to change it
        m_password_change_requested = PASSWORD_CHANGE_REQUESTED

        Return True
      End If
    End If ' GLB_UserDbInfo.password_exp <> 

    'User doesn't have to change the password
    Return False

  End Function ' CheckPasswordChange

  ' PURPOSE: Checks if the valid date is not in a future date
  '   
  '    - INPUT: None
  '
  '    - OUTPUT: None
  '
  ' RETURNS:
  '          TRUE: Is not a future date. User acount is active (Can be active but not enabled).
  '          FALSE: Is a future date. User can not login until that date.
  ' NOTES
  '
  Friend Function CheckStartValidDate() As Boolean
    Dim date_diff As System.TimeSpan
    Dim total_days As Double

    If IsSuperUser Then
      Return True
    End If

    'Subtracts today minus start date and gets the whole and fractional days
    date_diff = m_center_date.Subtract(m_not_valid_before)
    total_days = date_diff.TotalDays

    'check if is a future date
    If total_days < 0.0 Then
      'User can not login until start date.
      Return False
    End If

    'Subtracts today minus start date and gets the whole and fractional days
    date_diff = m_not_valid_after.Subtract(m_center_date)
    total_days = date_diff.TotalDays

    'check if is a future date
    If total_days < 0.0 Then
      'User can not login after this date.
      Return False
    End If

    Return True

  End Function ' CheckStartValidDate

  ' PURPOSE: Check if the number of days for password expiration
  '          has been reached.
  '          
  '    - INPUT: None
  '              
  '    - OUTPUT: None
  '              
  ' RETURNS:
  '              TRUE: password has expired
  '              FALSE: password has NOT expired
  ' NOTES
  '     We obtain the current date from the server in the format
  '     'month  dd, yyyy' because it is independent of the local computer
  '     format that could not be understanded (dd/mm/yyyy) or (mm/dd/yyyy)
  '     The visualbasic understands english and spanish name of the month.
  '
  Friend Function CheckPasswordExpiration() As Boolean
    Dim date_diff As System.TimeSpan
    Dim num_of_days As Integer
    Dim center_date_only As Date

    If IsSuperUser Then
      ' password has NOT expired
      Return False
    End If

    'Subtracts today minus last changed and get the number of days
    center_date_only = m_center_date.Date
    date_diff = center_date_only.Subtract(m_last_changed_date)
    num_of_days = date_diff.Days

    'check if limit has been reached
    If num_of_days >= m_password_expiration_days Then
      'ask the user to change the password
      Return True
    End If

    ' password has NOT expired
    Return False

  End Function ' CheckPasswordExpiration

  ' PURPOSE: Sets the user as logged
  '   
  '    - INPUT: None
  '             
  '    - OUTPUT: None
  '
  ' RETURNS: None
  '             
  ' NOTES
  Friend Function SetUserLogged(ByVal GuiId As Integer, Optional ByVal FormOwner As Form = Nothing) As Boolean

    Dim _message As String
    Dim _title As String
    Dim _state As WSI.Common.Users.USER_SESSION_STATE
    Dim _machine_name As String
    Dim _logged_in As Boolean
    Dim _msg_icon As MessageBoxIcon
    Dim _show_msg As Boolean

    _machine_name = Environment.MachineName
    m_gui_id = GuiId
    m_user_logged = True

    _message = ""
    _title = ""
    _logged_in = True
    _show_msg = False
    _msg_icon = MessageBoxIcon.Information
    _state = WSI.Common.Users.GetStateUserSession("WigosGUI", m_user_id, _machine_name, _message, _title)

    Select Case _state

      Case WSI.Common.Users.USER_SESSION_STATE.OPENED

        If m_user_id <> 0 Then
          _show_msg = True
          _msg_icon = MessageBoxIcon.Exclamation
          _logged_in = False
        End If

      Case WSI.Common.Users.USER_SESSION_STATE.EXPIRED, WSI.Common.Users.USER_SESSION_STATE.UNEXPECTED

        _show_msg = True

    End Select

    If _logged_in Then
      WSI.Common.WGDB.SetUserLoggedIn(m_user_id, m_user_name, _machine_name)
    End If

    If _show_msg Then
      MessageBox.Show(FormOwner, _message, _title, MessageBoxButtons.OK, _msg_icon)
    End If

    Return _logged_in

  End Function

  ' PURPOSE: Performs a Logout, it cleans all properties of the 
  '          Current Logged User
  '          
  '    - INPUT: None
  '
  '    - OUTPUT: None
  '
  ' RETURNS: None
  '
  ' NOTES
  '
  Public Sub Logout()

    m_user_id = 0
    m_profile_id = 0
    m_user_password = ""
    m_enabled = False
    m_user_name = ""
    'm_priv_level = 0
    m_user_full_name = ""

    'reset User permissions
    Erase m_user_permissions
    ReDim m_user_permissions(0)

    m_password_change_requested = False
    m_user_logged = False
    WSI.Common.WGDB.SetUserLoggedOff()

  End Sub

#End Region

End Class
