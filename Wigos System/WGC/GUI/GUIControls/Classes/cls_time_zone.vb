'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_time_zone.vb
' DESCRIPTION:   User class for time zone
' AUTHOR:        gabi Zutel
' CREATION DATE: 05-JUL-2004
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 05-JUL-2004  GZ     Initial version
' 04-DEC-2004  GZ     Moved to Gui Controls
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Runtime.InteropServices

Public Class CLASS_TIME_ZONE
  Inherits CLASS_BASE

#Region " Constants "

  Public Const MAX_TIME_ZONE_LEN As Integer = 20

  Public Const DEFAULT_STANDARD_BIAS As Integer = 0
  Public Const DEFAULT_DAYLIGHT_BIAS As Integer = -60

  Public Const DEFAULT_STANDARD_MONTH As Integer = 10
  Public Const DEFAULT_STANDARD_WEEKDAY As Integer = 0
  Public Const DEFAULT_STANDARD_DAY As Integer = 5
  Public Const DEFAULT_STANDARD_HHMM As Integer = 180

  Public Const DEFAULT_DAYLIGHT_MONTH As Integer = 3
  Public Const DEFAULT_DAYLIGHT_WEEKDAY As Integer = 0
  Public Const DEFAULT_DAYLIGHT_DAY As Integer = 5
  Public Const DEFAULT_DAYLIGHT_HHMM As Integer = 120

  Public Const YES As Integer = 1
  Public Const NO As Integer = 0

  Public Const MAX_TIME_ZONES As Integer = 99

#End Region

#Region " GUI_Configuration.dll "

#Region " Structures "

  <StructLayout(LayoutKind.Sequential)> _
   Public Class TYPE_TIME_ZONE
    Public control_block As Integer
    Public time_zone_id As Integer
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_TIME_ZONE_LEN + 1 + 3)> _
    Public name As String
    Public bias As Integer
    Public daylight_saving As Integer
    Public standard_bias As Integer
    Public standard_month As Integer
    Public standard_weekday As Integer
    Public standard_day As Integer
    Public standard_hhmm As Integer
    Public daylight_bias As Integer
    Public daylight_month As Integer
    Public daylight_weekday As Integer
    Public daylight_day As Integer
    Public daylight_hhmm As Integer
  End Class

#End Region

#Region " Prototypes "

  Protected Declare Function ReadTimeZone Lib "GUI_Configuration" (<[In](), [Out]()> _
                                                                   ByVal pTimeZone As TYPE_TIME_ZONE, _
                                                                   ByVal Context As Integer) As Integer
  Protected Declare Function UpdateTimeZone Lib "GUI_Configuration" (<[In](), [Out]()> _
                                                                     ByVal pTimeZone As TYPE_TIME_ZONE, _
                                                                     ByVal Context As Integer) As Integer
  Protected Declare Function InsertTimeZone Lib "GUI_Configuration" (<[In](), [Out]()> _
                                                                     ByVal pTimeZone As TYPE_TIME_ZONE, _
                                                                     ByVal Context As Integer) As Integer
  Protected Declare Function DeleteTimeZone Lib "GUI_Configuration" (ByVal TimeZoneId As Integer, _
                                                                     ByVal Context As Integer) As Integer

#End Region

#Region " Members "

  Protected m_time_zone As New TYPE_TIME_ZONE()

#End Region

#Region " Constructor and Destructor "

  Public Sub New()
    MyBase.New()
    m_time_zone.control_block = Marshal.SizeOf(GetType(TYPE_TIME_ZONE))

    m_time_zone.daylight_saving = YES

    m_time_zone.daylight_bias = DEFAULT_DAYLIGHT_BIAS
    m_time_zone.standard_bias = DEFAULT_STANDARD_BIAS

    m_time_zone.daylight_day = DEFAULT_DAYLIGHT_DAY
    m_time_zone.daylight_hhmm = DEFAULT_DAYLIGHT_HHMM
    m_time_zone.daylight_month = DEFAULT_DAYLIGHT_MONTH
    m_time_zone.daylight_weekday = DEFAULT_DAYLIGHT_WEEKDAY

    m_time_zone.standard_day = DEFAULT_STANDARD_DAY
    m_time_zone.standard_hhmm = DEFAULT_STANDARD_HHMM
    m_time_zone.standard_month = DEFAULT_STANDARD_MONTH
    m_time_zone.standard_weekday = DEFAULT_STANDARD_WEEKDAY

  End Sub

  Protected Overrides Sub Finalize()
    Call MyBase.Finalize()
  End Sub
#End Region

#Region " Properties "

  Public Property TimeZoneId() As Integer
    Get
      Return m_time_zone.time_zone_id
    End Get
    Set(ByVal Value As Integer)
      m_time_zone.time_zone_id = Value
    End Set
  End Property

  Public Property Name() As String
    Get
      Return m_time_zone.name
    End Get
    Set(ByVal Value As String)
      m_time_zone.name = Value
    End Set
  End Property

  Public Property Bias() As Integer
    Get
      Return m_time_zone.bias
    End Get
    Set(ByVal Value As Integer)
      m_time_zone.bias = Value
    End Set
  End Property

  Public Property DaylightSaving() As Integer
    Get
      Return m_time_zone.daylight_saving
    End Get
    Set(ByVal Value As Integer)
      m_time_zone.daylight_saving = Value
    End Set
  End Property

  Public Property StandardBias() As Integer
    Get
      Return m_time_zone.standard_bias
    End Get
    Set(ByVal Value As Integer)
      m_time_zone.standard_bias = Value
    End Set
  End Property

  Public Property StandardMonth() As Integer
    Get
      Return m_time_zone.standard_month
    End Get
    Set(ByVal Value As Integer)
      m_time_zone.standard_month = Value
    End Set
  End Property

  Public Property StandardWeekday() As Integer
    Get
      Return m_time_zone.standard_weekday
    End Get
    Set(ByVal Value As Integer)
      m_time_zone.standard_weekday = Value
    End Set
  End Property

  Public Property StandardDay() As Integer
    Get
      Return m_time_zone.standard_day
    End Get
    Set(ByVal Value As Integer)
      m_time_zone.standard_day = Value
    End Set
  End Property

  Public Property StandardHhmm() As Integer
    Get
      Return m_time_zone.standard_hhmm
    End Get
    Set(ByVal Value As Integer)
      m_time_zone.standard_hhmm = Value
    End Set
  End Property

  Public Property DaylightBias() As Integer
    Get
      Return m_time_zone.daylight_bias
    End Get
    Set(ByVal Value As Integer)
      m_time_zone.daylight_bias = Value
    End Set
  End Property

  Public Property DaylightMonth() As Integer
    Get
      Return m_time_zone.daylight_month
    End Get
    Set(ByVal Value As Integer)
      m_time_zone.daylight_month = Value
    End Set
  End Property

  Public Property DaylightWeekday() As Integer
    Get
      Return m_time_zone.daylight_weekday
    End Get
    Set(ByVal Value As Integer)
      m_time_zone.daylight_weekday = Value
    End Set
  End Property

  Public Property DaylightDay() As Integer
    Get
      Return m_time_zone.daylight_day
    End Get
    Set(ByVal Value As Integer)
      m_time_zone.daylight_day = Value
    End Set
  End Property

  Public Property DaylightHhmm() As Integer
    Get
      Return m_time_zone.daylight_hhmm
    End Get
    Set(ByVal Value As Integer)
      m_time_zone.daylight_hhmm = Value
    End Set
  End Property

#End Region

#End Region

#Region " Overrides functions "

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As ENUM_STATUS
    Dim rc As Integer

    Me.TimeZoneId = ObjectId

    m_time_zone.control_block = Marshal.SizeOf(m_time_zone)

    ' Read time zone data
    rc = ReadTimeZone(m_time_zone, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK

        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As ENUM_STATUS
    Dim rc As Integer

    m_time_zone.control_block = Marshal.SizeOf(m_time_zone)

    ' Insert terminal data
    rc = InsertTimeZone(m_time_zone, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As ENUM_STATUS
    Dim rc As Integer

    m_time_zone.control_block = Marshal.SizeOf(m_time_zone)

    ' Update terminal data
    rc = UpdateTimeZone(m_time_zone, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_NOT_FOUND
        Return ENUM_STATUS.STATUS_NOT_FOUND

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As ENUM_STATUS
    Dim rc As Integer

    ' Delete operator/distributor data
    rc = DeleteTimeZone(Me.TimeZoneId, SqlCtx)

    Select Case rc
      Case ENUM_CONFIGURATION_RC.CONFIGURATION_OK
        Return CLASS_BASE.ENUM_STATUS.STATUS_OK

      Case ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR_DEPENDENCIES
        Return CLASS_BASE.ENUM_STATUS.STATUS_DEPENDENCIES

      Case Else
        Return ENUM_STATUS.STATUS_ERROR

    End Select

  End Function

  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA
    Dim auditor_data As CLASS_AUDITOR_DATA
    Dim base_field_name As String
    Dim aux_field_name As String

    auditor_data = New CLASS_AUDITOR_DATA(AUDIT_NAME_TIME_ZONE)
    Call auditor_data.SetName(GLB_NLS_GUI_CONFIGURATION.Id(487), Me.Name)

    Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(487), Me.Name)
    Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(488), GUI_FormatTimeZone(-Me.Bias))

    Call auditor_data.SetField(GLB_NLS_GUI_CONFIGURATION.Id(489), _
         IIf(Me.DaylightSaving, GLB_NLS_GUI_CONFIGURATION.GetString(264), GLB_NLS_GUI_CONFIGURATION.GetString(265)))

    ' Daylight Saving Start Date
    base_field_name = GLB_NLS_GUI_CONFIGURATION.GetString(490)

    aux_field_name = base_field_name & "." & GLB_NLS_GUI_CONFIGURATION.GetString(493)
    Call auditor_data.SetField(0, GLB_NLS_GUI_COMMONMISC.GetString(11 + Me.DaylightMonth), aux_field_name)

    aux_field_name = base_field_name & "." & GLB_NLS_GUI_CONFIGURATION.GetString(495)
    Call auditor_data.SetField(0, GLB_NLS_GUI_COMMONMISC.GetString(30 + Me.DaylightDay), aux_field_name)

    aux_field_name = base_field_name & "." & GLB_NLS_GUI_CONFIGURATION.GetString(494)
    Call auditor_data.SetField(0, GLB_NLS_GUI_COMMONMISC.GetString(24 + Me.DaylightWeekday), aux_field_name)

    aux_field_name = base_field_name & "." & GLB_NLS_GUI_CONFIGURATION.GetString(492)
    Call auditor_data.SetField(0, GetHhmmFromMin(Me.DaylightHhmm), aux_field_name)

    ' Daylight Saving End Date
    base_field_name = GLB_NLS_GUI_CONFIGURATION.GetString(491)

    aux_field_name = base_field_name & "." & GLB_NLS_GUI_CONFIGURATION.GetString(493)
    Call auditor_data.SetField(0, GLB_NLS_GUI_COMMONMISC.GetString(11 + Me.StandardMonth), aux_field_name)

    aux_field_name = base_field_name & "." & GLB_NLS_GUI_CONFIGURATION.GetString(495)
    Call auditor_data.SetField(0, GLB_NLS_GUI_COMMONMISC.GetString(30 + Me.StandardDay), aux_field_name)

    aux_field_name = base_field_name & "." & GLB_NLS_GUI_CONFIGURATION.GetString(494)
    Call auditor_data.SetField(0, GLB_NLS_GUI_COMMONMISC.GetString(24 + Me.StandardWeekday), aux_field_name)

    aux_field_name = base_field_name & "." & GLB_NLS_GUI_CONFIGURATION.GetString(492)
    Call auditor_data.SetField(0, GetHhmmFromMin(Me.StandardHhmm), aux_field_name)

    Return auditor_data

  End Function

  Public Overrides Function Duplicate() As CLASS_BASE
    Dim temp_time_zone As CLASS_TIME_ZONE

    temp_time_zone = New CLASS_TIME_ZONE
    temp_time_zone.m_time_zone.control_block = Me.m_time_zone.control_block
    temp_time_zone.m_time_zone.bias = Me.m_time_zone.bias
    temp_time_zone.m_time_zone.daylight_bias = Me.m_time_zone.daylight_bias
    temp_time_zone.m_time_zone.daylight_day = Me.m_time_zone.daylight_day
    temp_time_zone.m_time_zone.daylight_hhmm = Me.m_time_zone.daylight_hhmm
    temp_time_zone.m_time_zone.daylight_month = Me.m_time_zone.daylight_month
    temp_time_zone.m_time_zone.daylight_saving = Me.m_time_zone.daylight_saving
    temp_time_zone.m_time_zone.daylight_weekday = Me.m_time_zone.daylight_weekday
    temp_time_zone.m_time_zone.name = Me.m_time_zone.name
    temp_time_zone.m_time_zone.standard_bias = Me.m_time_zone.standard_bias
    temp_time_zone.m_time_zone.standard_day = Me.m_time_zone.standard_day
    temp_time_zone.m_time_zone.standard_hhmm = Me.m_time_zone.standard_hhmm
    temp_time_zone.m_time_zone.standard_month = Me.m_time_zone.standard_month
    temp_time_zone.m_time_zone.standard_weekday = Me.m_time_zone.standard_weekday
    temp_time_zone.m_time_zone.time_zone_id = Me.m_time_zone.time_zone_id

    Return temp_time_zone

  End Function

#End Region

End Class
