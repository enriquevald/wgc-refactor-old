'-------------------------------------------------------------------
' Copyright � 2012 Win Systems International Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_report
' DESCRIPTION:   Class to define reports for the form frm_reports.
'                For each report, we have: ReportId, FormId, ReportName, Description, ReportPermissions.
'                It is also provided a property to access all the defined reports in the system.
'
' AUTHOR:        Ra�l Cervera
' CREATION DATE: 15-MAR-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-MAR-2012  RCI    Initial version.
' 15-SEP-2016  ETP    Fixed bugs 17585 - 17607: Generic reports: add permissions config.
'--------------------------------------------------------------------

Option Explicit On

Public Class Report

#Region " Enums "

  Public Enum ENUM_REPORT_ID
    REPORT_ACCOUNT_MOVEMENTS = 1
    REPORT_ACCOUNT_MOVEMENTS_SAT = 2
    REPORT_CASHIER_MOVEMENTS = 3
    REPORT_CASH_SUMMARY = 4
    REPORT_CONNECTED_TERMINALS = 5
  End Enum

#End Region ' Enums

#Region " Members "

  Private m_report_id As ENUM_REPORT_ID

#End Region ' Members

#Region " Public Properties "

  ' PURPOSE: Return the ReportId. It is an enum value.
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_REPORT_ID
  '
  Public ReadOnly Property ReportId() As ENUM_REPORT_ID
    Get
      Return m_report_id
    End Get
  End Property ' ReportId

  ' PURPOSE: Return the FormId defined as FORM_REPORTS_REPORT_000 + ReportId.
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_FORM
  '
  Public ReadOnly Property FormId() As ENUM_FORM
    Get
      Return ENUM_FORM.FORM_REPORTS_REPORT_000 + m_report_id
    End Get
  End Property ' FormId

  ' PURPOSE: Return the Report Name, obtained from the NLS with index ReportId.
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - String
  '
  Public ReadOnly Property ReportName() As String
    Get
      Return GLB_NLS_REPORT.GetString(m_report_id)
    End Get
  End Property ' ReportName

  ' PURPOSE: Return the Report Description, obtained from the NLS with index 1000 + ReportId.
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - String
  '
  Public ReadOnly Property Description() As String
    Get
      Return GLB_NLS_REPORT.GetString(1000 + m_report_id)
    End Get
  End Property ' Description

  ' PURPOSE: Return the Report Permissions, obtained from the form defined as FORM_REPORTS_REPORT_000 + ReportId.
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - CLASS_GUI_USER.TYPE_PERMISSIONS
  '
  Public ReadOnly Property ReportPermissions() As CLASS_GUI_USER.TYPE_PERMISSIONS
    Get
      Return CurrentUser.Permissions(ENUM_FORM.FORM_REPORTS_REPORT_000 + m_report_id)
    End Get
  End Property ' ReportPermissions

  ' PURPOSE: Return all the defined reports in the system.
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - List(Of Report)
  '
  Public Shared ReadOnly Property Reports() As List(Of Report)
    Get
      Dim _list As New List(Of Report)

      For Each _enum As ENUM_REPORT_ID In System.Enum.GetValues(GetType(ENUM_REPORT_ID))
        _list.Add(GetReport(_enum))
      Next

      Return _list
    End Get
  End Property ' Reports

#End Region ' Public Properties

#Region " Public Functions "

  ' PURPOSE: Create a new instance of class Report.
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - Report
  '
  Public Shared Function GetReport(ByVal ReportId As ENUM_REPORT_ID) As Report
    Dim _report As Report

    _report = New Report()
    _report.m_report_id = ReportId

    Return _report
  End Function ' GetReport

  ' PURPOSE: Add the report functionalities to the GUI.
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Shared Sub CreateFunctionalities(ByRef WigosForms As GUI_CommonOperations.CLASS_GUI_FORMS)
    For Each _report As Report In Report.Reports
      Call WigosForms.Add(_report.FormId, GLB_NLS_REPORT.Id(_report.m_report_id), WSI.Common.ENUM_FEATURES.NONE)
    Next
  End Sub ' CreateFunctionalities

#End Region ' Public Functions

End Class ' Report
