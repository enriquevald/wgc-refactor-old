'-------------------------------------------------------------------
' Copyright � 2004 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_browser_folder.vb
' DESCRIPTION:   Browser folder
' AUTHOR:        Xavier Perez Serret
' CREATION DATE: 30-JAN-2004
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 30-JAN-2004  XPS    Initial version
'-------------------------------------------------------------------
Option Explicit On 
Option Strict On

Imports GUI_CommonMisc
Imports GUI_CommonOperations

'XVV -> Add NameSpace Windows.Forms,solve error with IWin32Window variables definitions
Imports System.Windows.Forms

Public Class CLASS_FOLDER_BROWSER
  Inherits CLASS_BASE

#Region " Constants"
  Private Const MAX_PATH As Int32 = 260

#End Region

  Public Enum ENUM_FORMAT_BROWSE_FOR_TYPES
    'Browse for computers..
    COMPUTERS = &H1000
    'Browse for directories.
    DIRECTORIES = &H1
    'Browse for files and directories.
    'Only works on shell version 4.71 and higher!
    FILES_AND_DIRECTORIES = &H4000 ' Version 4.71
    'Browse for file system ancestors (an ancestor is a subfolder that is beneath the root folder in the namespace hierarchy.).
    FILE_SYSTEM_ANCESTORS = &H8
    ' Show Textbox to edit path
    EDITBOX = &H10

  End Enum

#Region "Structures"

  Private Structure BROWSEINFO

    Public hWndOwner As IntPtr
    Public pIDLRoot As Integer
    Public pszDisplayName As String
    Public lpszTitle As String
    Public ulFlags As Integer
    Public lpfnCallback As Integer
    Public lParam As Integer
    Public iImage As Integer

  End Structure

#End Region

#Region " Prototypes "

  Private Declare Function CoTaskMemFree Lib "ole32.dll" (ByVal hMem As IntPtr) As Integer
  Private Declare Function SHBrowseForFolder Lib "shell32.dll" (ByRef lpbi As BROWSEINFO) As IntPtr
  Private Declare Function SHGetPathFromIDList Lib "shell32.dll" (ByVal pidList As IntPtr, ByVal lpBuffer As String) As Integer

#End Region

#Region " CLASS_BD Methods "
  Public Overrides Function DB_Insert(ByRef SqlCtx As Integer) As ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function

  Public Overrides Function DB_Update(ByRef SqlCtx As Integer) As ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function

  Public Overrides Function DB_Delete(ByRef SqlCtx As Integer) As ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function

  Public Overrides Function DB_Read(ByVal ObjectId As Object, ByRef SqlCtx As Integer) As CLASS_BASE.ENUM_STATUS
    Return CLASS_BASE.ENUM_STATUS.STATUS_ERROR
  End Function
  '
#End Region

  Public Overrides Function AuditorData() As CLASS_AUDITOR_DATA
    Return Nothing
  End Function

  Public Overrides Function Duplicate() As CLASS_BASE
    Return Nothing
  End Function

  ' The owner of the folder dialog.
  Protected Function RunDialog(ByVal hWndOwner As IntPtr) As Boolean
    Dim udtBI As New BROWSEINFO
    Dim lpIDList As IntPtr
    'XVV Variable not use Dim buffer As String
    Dim path As String

    ' set the owner of the window
    udtBI.hWndOwner = hWndOwner
    ' set the owner of the window
    udtBI.lpszTitle = Title
    ' set the owner of the window
    udtBI.ulFlags = CType((BrowseFor), Integer)

    ' show the 'Browse for folder' dialog
    lpIDList = SHBrowseForFolder(udtBI)
    If lpIDList.ToInt64() <> 0 Then
      If BrowseFor = ENUM_FORMAT_BROWSE_FOR_TYPES.COMPUTERS Then
        m_Selected = udtBI.pszDisplayName.Trim()
      Else
        ' get the path from the IDList
        path = FixedString(MAX_PATH)
        SHGetPathFromIDList(lpIDList, path)
        m_Selected = path.ToString()
      End If
      ' free the block of memory
      CoTaskMemFree(lpIDList)
    Else
      Return False
    End If
    Return True
  End Function
  'Shows the common folder dialog
  Public Function ShowDialog() As Boolean
    Return ShowDialog(Nothing)
  End Function

  'Shows the common folder dialog
  'The owner of the folder dialog
    Public Function ShowDialog(ByVal owner As IWin32Window) As Boolean
        Dim handle As IntPtr

        If Not owner Is Nothing Then
            handle = owner.Handle
        Else
            handle = IntPtr.Zero
        End If
        If RunDialog(handle) Then
            Return True
        Else
            Return False
        End If
    End Function

  'Specifies the title of the dialog
  Public Property Title() As String
    Get
      Return m_Title
    End Get
    Set(ByVal value As String)
      If value Is Nothing Then
        m_Title = ""
      Else
        m_Title = value
      End If
    End Set
  End Property

  'Returns the selected item.
  Public ReadOnly Property Selected() As String
    Get
      Return m_Selected
    End Get
  End Property

  'Specifies what to browse for
  Public Property BrowseFor() As ENUM_FORMAT_BROWSE_FOR_TYPES
    Get
      Return m_BrowseFor
    End Get
    Set(ByVal value As ENUM_FORMAT_BROWSE_FOR_TYPES)
      m_BrowseFor = value
    End Set
  End Property

  ' private declares
  Private m_BrowseFor As ENUM_FORMAT_BROWSE_FOR_TYPES = ENUM_FORMAT_BROWSE_FOR_TYPES.DIRECTORIES
  Private m_Title As String = ""
  Private m_Selected As String = ""

End Class