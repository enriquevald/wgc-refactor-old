'-------------------------------------------------------------------
' Copyright � 2003 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_ipc_linstener.vb
' DESCRIPTION:   The GUI interface to communicate via IPC.
' AUTHOR:        Andreu Juli�
' CREATION DATE: 19-JUN-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 19-JUN-2002  AJQ    Initial version
' 21-MAR-2004  AJQ    IPC buffer size increased to 5 KB. 
'-------------------------------------------------------------------
Imports System.Runtime.InteropServices

#Region " IPC_OUD_RECEIVE "

<StructLayout(LayoutKind.Sequential)> _
Public Class TYPE_IPC_RECV_HEADER
  Friend control_block As Integer
  Public source_node_inst_id As Integer
  Public msg_cmd As Integer
  Public msg_status As Integer
  Public msg_id As Integer
  Public user_dword As Integer
  Public user_buffer_length As Integer
End Class

<StructLayout(LayoutKind.Sequential)> _
Public Class TYPE_API_OUTPUT_PARAMS
  Friend control_block As Integer
  Public output_1 As Short
  Public output_2 As Short
End Class

<StructLayout(LayoutKind.Sequential)> _
Public Class IPC_OUD_RECEIVE
  Private Const IPC_USER_BUFFER_SIZE As Integer = 5120
  Public m_header As TYPE_IPC_RECV_HEADER
  <MarshalAs(UnmanagedType.ByValArray, SizeConst:=IPC_USER_BUFFER_SIZE)> _
  Public m_user_buffer() As Byte
End Class

#End Region

#Region " IPC_IUD_SEND "

<StructLayout(LayoutKind.Sequential)> _
Public Class TYPE_IPC_SEND_HEADER
  Friend control_block As Integer
  Public target_node_inst_id As Integer
  Friend source_node_inst_id As Integer
  Public msg_cmd As Integer
  Public msg_status As Integer
  Public msg_id As Integer
  Public user_dword As Integer
  Friend user_buffer_length As Integer
End Class

<StructLayout(LayoutKind.Sequential)> _
Public Class IPC_IUD_SEND
  Private Const IPC_USER_BUFFER_SIZE As Integer = 5120
  Public m_header As TYPE_IPC_RECV_HEADER
  <MarshalAs(UnmanagedType.ByValArray, SizeConst:=IPC_USER_BUFFER_SIZE)> _
  Public m_user_buffer() As Byte
End Class

#End Region

Public Class CLASS_IPC_LISTENER

#Region " Public Event "
  ' The Message Received Event
  Public Event MessageReceivedEvent(ByVal MessageHeader As TYPE_IPC_RECV_HEADER, _
                                    ByVal UserBuffer As System.IntPtr)

#End Region

#Region " Constants "

  Private Const IPC_CODE_MODULE_INIT As Short = 1
  Private Const IPC_CODE_MODULE_STOP As Short = 8
  Private Const IPC_CODE_WAIT_SEND_QUEUE_EMPTY As Short = 7
  Private Const IPC_NODE_UNKNOWN As Integer = &HFFFFFFFF

#End Region

#Region " Variables "
  ' IPC Node Name
  Private m_node_name As String
  Private m_node_instance_id As Integer

  ' The IPC thread itself
  Private m_thread As System.Threading.Thread
  Private m_ev_init As New System.Threading.ManualResetEvent(False)
  Private m_ev_stop As New System.Threading.ManualResetEvent(False)
  Private m_ev_msg As New System.Threading.ManualResetEvent(False)

  Private m_init As Boolean = False
  Private m_raise_events As Boolean = True


  ' Recv Buffer
  Private m_ptr_recv_msg As System.IntPtr
  Private m_ptr_recv_buf As System.IntPtr
  Private m_ptr_output_params As System.IntPtr

  ' Send Buffer
  Private m_ptr_send_msg As System.IntPtr
  Private m_ptr_send_buf As System.IntPtr

#End Region

#Region " External Procedures CommonMisc.dll"

  Private Declare Function Common_Ipc Lib "CommonMisc" (ByVal Code As Short) As Short

  Private Declare Function Common_IpcReceive Lib "CommonMisc" (ByVal NodeId As Integer, _
                                                               ByVal MsgPointer As System.IntPtr) As Short

  Private Declare Function Common_IpcReceiveAsync Lib "CommonMisc" (ByVal NodeId As Integer, _
                                                                    ByVal Handle As System.IntPtr, _
                                                                    ByVal OutputParamsPointer As System.IntPtr, _
                                                                    ByVal MsgPointer As System.IntPtr) As Short

  Private Declare Function Common_IpcSend Lib "CommonMisc" (ByVal MsgPointer As System.IntPtr) As Short

  Private Declare Function Common_IpcNodeInit Lib "CommonMisc" (ByVal NodeName As String, _
                                                                ByRef NodeInstanceId As Integer, _
                                                                Optional ByVal BufferSize As Integer = 0) As Short

  Private Declare Function Common_IpcGetNodeInstanceId Lib "CommonMisc" (ByVal NodeName As String, _
                                                                         ByRef NodeInstanceId As Integer, _
                                                                         ByVal GetLocalInstance As Short) As Short

#End Region

#Region " Private Functions "

  ' PURPOSE: Run IPC listener
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Private Sub Run()
    Dim continue_receiving As Boolean
    Dim msg_header As New TYPE_IPC_RECV_HEADER()
    Dim api_output_params As New TYPE_API_OUTPUT_PARAMS()
    'XVV Variable not use Dim rc As Short
    Dim wait_handles() As System.Threading.WaitHandle
    Dim idx_handle As Integer
    Const IDX_HANDLE_MESSAGE As Integer = 0
    Const IDX_HANDLE_STOP As Integer = 1

    ' Signal that the IPC has been initialized
    m_init = True
    Call m_ev_init.Set()

    ' Allocate Receive Buffer
    m_ptr_recv_msg = Marshal.AllocCoTaskMem(Marshal.SizeOf(GetType(IPC_OUD_RECEIVE)))
    ' - User Buffer
    m_ptr_recv_buf = New IntPtr(m_ptr_recv_msg.ToInt64 + Marshal.SizeOf(GetType(TYPE_IPC_RECV_HEADER)))
    ' Api Output Params
    m_ptr_output_params = Marshal.AllocCoTaskMem(Marshal.SizeOf(GetType(TYPE_API_OUTPUT_PARAMS)))

    continue_receiving = True
    ReDim wait_handles(1)
    wait_handles(IDX_HANDLE_MESSAGE) = m_ev_msg
    wait_handles(IDX_HANDLE_STOP) = m_ev_stop

    ' Post a recevie
    Call m_ev_msg.Reset()

    '----------------------------------------------------------------
    'XVV 13/04/2007 ********** CAUTION ***********
    'Comment this sentence, because Handle property is obsolete, actually use SafeWaitHandle but
    'If we change this param,  we need change the DLL librarie. In this project this funcion don't 
    'use, if you need this functionality, uncomment the sentence.
    'Call Common_IpcReceiveAsync(m_node_instance_id, _
    '                            m_ev_msg.Handle, _
    '                            m_ptr_output_params, _
    '                            m_ptr_recv_msg)

    '----------------------------------------------------------------

    While continue_receiving
      idx_handle = System.Threading.WaitHandle.WaitAny(wait_handles)

      Select Case idx_handle
        Case IDX_HANDLE_MESSAGE
          ' Message Received
          Marshal.PtrToStructure(m_ptr_output_params, api_output_params)
          If api_output_params.output_1 = 0 Then
            ' - Header
            Marshal.PtrToStructure(m_ptr_recv_msg, msg_header)
            ' - Event
            If m_raise_events Then
              RaiseEvent MessageReceivedEvent(msg_header, m_ptr_recv_buf)
            End If
          Else
            ' Log ...
          End If
          ' Post a new receive ...
          Call m_ev_msg.Reset()

          '----------------------------------------------------------------
          'XVV 13/04/2007 ********** CAUTION ***********
          'Comment this sentence, because Handle property is obsolete, actually use SafeWaitHandle but
          'If we change this param,  we need change the DLL librarie. In this project this funcion don't 
          'use, if you need this functionality, uncomment the sentence.
          'Call Common_IpcReceiveAsync(m_node_instance_id, _
          '                            m_ev_msg.Handle, _
          '                            m_ptr_output_params, _
          '                            m_ptr_recv_msg)
          '----------------------------------------------------------------
        Case IDX_HANDLE_STOP
          continue_receiving = False

        Case Else
          continue_receiving = False
      End Select

    End While

  End Sub

#End Region

#Region " Public Functions "

  ' PURPOSE: Send user message to defined node
  '
  '  PARAMS:
  '     - INPUT:
  '           - MessageHeader : Node identifier and command
  '           - UserData      : Optional parameters
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  '
  Public Sub SendMessage(ByVal MessageHeader As TYPE_IPC_SEND_HEADER, _
                         ByVal UserData As Object)

    If m_init = False Then
      Exit Sub
    End If

    If m_ev_init.WaitOne Then

      'XVV 17/04/2007
      'Change m_ptr_send_msg.Zero for InPtr.Zero
      If m_ptr_send_buf.Equals(IntPtr.Zero) Then
        ' Allocate Send Buffer
        m_ptr_send_msg = Marshal.AllocCoTaskMem(Marshal.SizeOf(GetType(IPC_IUD_SEND)))
        ' - User Buffer
        m_ptr_send_buf = New IntPtr(m_ptr_send_msg.ToInt32 + Marshal.SizeOf(GetType(TYPE_IPC_SEND_HEADER)))
      End If

      ' Control Block
      MessageHeader.control_block = Marshal.SizeOf(GetType(IPC_IUD_SEND))
      ' Set the source node
      MessageHeader.source_node_inst_id = m_node_instance_id

      ' User Buffer Length
      If IsNothing(UserData) Then
        MessageHeader.user_buffer_length = 0
      Else
        MessageHeader.user_buffer_length = Marshal.SizeOf(UserData)
      End If

      ' Message Header
      Call Marshal.StructureToPtr(MessageHeader, m_ptr_send_msg, False)

      If Not IsNothing(UserData) Then
        ' Message User Data
        Call Marshal.StructureToPtr(UserData, m_ptr_send_buf, False)
      End If

      Common_IpcSend(m_ptr_send_msg)

    End If

  End Sub

  ' PURPOSE: Get the instance identifier of one node type
  '
  '  PARAMS:
  '     - INPUT:
  '           - NodeName 
  '           - GetLocalInstance
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - The node instance identifier
  '
  Public Function GetNodeInstanceId(ByVal NodeName As String, Optional ByVal GetLocalInstace As Boolean = True) As Integer
    Dim node_instance_id As Integer
    'XVV Variable not use Dim rc As Short


    If Common_IpcGetNodeInstanceId(NodeName, node_instance_id, GetLocalInstace) = 0 Then
      node_instance_id = IPC_NODE_UNKNOWN
    End If

    Return node_instance_id

  End Function

  ' PURPOSE: Flush listener
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '
  Public Sub Flush()
    If m_init Then
      If m_ev_init.WaitOne Then
        Call Common_Ipc(IPC_CODE_WAIT_SEND_QUEUE_EMPTY)
      End If
    End If
  End Sub


  ' PURPOSE: Set the UserBuffer(message received) to target object
  '
  '  PARAMS:
  '     - INPUT:
  '           - UserBuffer
  '     - OUTPUT:
  '           - TargetObject
  '
  ' RETURNS:
  '
  Public Sub UserBufferToObject(ByVal UserBuffer As System.IntPtr, _
                                ByVal TargetObject As Object)

    Marshal.PtrToStructure(UserBuffer, TargetObject)

  End Sub

  ' PURPOSE: Initializes IPC node 
  '
  '  PARAMS:
  '     - INPUT:
  '           - NodeName
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE   : Init Ok 
  '     - FALSE  : Error initializing 
  '
  Public Function Init(ByVal NodeName As String) As Boolean
    ' Init IPC
    If Common_Ipc(IPC_CODE_MODULE_INIT) = 0 Then
      Return False
    End If

    ' Init IPC Node
    If Common_IpcNodeInit(NodeName, m_node_instance_id, 0) = 0 Then
      Return False
    End If

    ' Create the thread
    m_thread = New Threading.Thread(AddressOf Me.Run)
    ' Assign a name ... 
    m_thread.Name = "IPC THREAD"
    ' Store the Ipc Node Name
    m_node_name = NodeName
    ' Start the thread
    Call m_thread.Start()

    Return True

  End Function

  ' PURPOSE: Listener stop
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '
  Public Sub Done()

    If Not m_init Then
      Exit Sub
    End If

    m_init = False

    Call m_ev_stop.Set()
    Call m_thread.Join()

    '--------------------------------------------------------------------
    'XVV 17/04/2007
    'Change m_ptr_recv_msg.Zero for InPtr.Zero
    '--------------------------------------------------------------------
    ' Release the Receive Buffer
    If Not m_ptr_recv_msg.Equals(IntPtr.Zero) Then
      Call Marshal.FreeCoTaskMem(m_ptr_recv_msg)
      m_ptr_recv_msg = IntPtr.Zero
    End If

    ' Release the Send Buffer
    If Not m_ptr_send_msg.Equals(IntPtr.Zero) Then
      Call Marshal.FreeCoTaskMem(m_ptr_send_msg)
      m_ptr_send_msg = IntPtr.Zero
    End If
    '--------------------------------------------------------------------

  End Sub

#End Region

  Public Property RaiseEvents() As Boolean
    Get
      Return m_raise_events
    End Get
    Set(ByVal Value As Boolean)
      m_raise_events = Value
    End Set
  End Property

  Protected Overrides Sub Finalize()
    ' Stop ...
    Call Done()

    MyBase.Finalize()
  End Sub

End Class
