'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_menues.vb
' DESCRIPTION:   Interface for menu base class
' AUTHOR:        Carlos A. Costa
' CREATION DATE: 17-05-2002
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 17-MAY-2002  CAC    Initial version
' 11-NOV-2013  JBC    Added overload to AddItem. Allows String parameter.
' 12-FEB-2015  FJC    New GUI Menu
' 07-APR-2017  FAV    New GUI Menu (Tournaments)
' 30-NOV-2017  RGR    PBI 30932:WIGOS-6393 GUI - Menu: User Interface improvements
' 08-JAN-2018  RGR    PBI 30932:WIGOS-6393 GUI - Menu: User Interface improvements 
'-------------------------------------------------------------------

Imports GUI_CommonMisc
Imports GUI_CommonOperations
'XVV -> Add NameSpace Windows.Forms,solve error with IWin32Window variables definitions
Imports System.Windows.Forms
Imports System.Drawing
Imports System.Drawing.Drawing2D


Public Class CLASS_MENU_CUSTOMIZE

#Region " Constants"
    'Key Menu GUI Options
    Public Const KEY_ID_MENU_ITEM = "WS"
    Public Const KEY_ID_MENU_ITEM_WINDOW = "WSW"

    'Resolution Minimum for font
    Private Const WITH_1280px = 1280

    'Font
    Private Const FONT_NAME = "Segoe UI"

    Private Const FONT_SIZE_VERTICAL_SEPARATOR_WITH_ICONS = 25
    Private Const FONT_SIZE_VERTICAL_SEPARATOR_WITHOUT_ICONS = 10
    Private Const FONT_SIZE_HORIZONTAL_SEPARATOR = 0.10000000000000001

    'Size (Vertical)
    Private Const SIZE_HEIGHT_VERTICAL_SEPARATOR = 1
    Private Const SIZE_WIDTH_VERTICAL_SEPARATOR = 1

    'Size (Horizontal)
    Private Const SIZE_HEIGHT_HORIZONTAL_SEPARATOR = 1
    Private Const SIZE_WIDTH_HORIZONTAL_SEPARATOR = 18

    'Color

#End Region

#Region " Members"

    'Font for all items from menu
    Public Shared m_font As System.Drawing.Font

    'Font & Size for vertical separator items from menu
    Public Shared m_font_vertical_separator_with_icons As System.Drawing.Font
    Public Shared m_font_vertical_separator_without_icons As System.Drawing.Font
    Public Shared m_size_vertical_separator As System.Drawing.Size

    'Font & Size for horizontal separator items from menu
    Public Shared m_font_horizontal_separator As System.Drawing.Font
    Public Shared m_size_horizontal_separator As System.Drawing.Size

    'Color 
    Public Shared m_plain_color As System.Drawing.Color

    'Padding Menu
    Public Shared m_padding_menu As System.Windows.Forms.Padding

    'Show / Hide Icons
    Public Shared m_show_icons As Boolean

    ' Position (Top, Bottom , Left, Right)
    Public Shared m_position As System.Windows.Forms.DockStyle
#End Region

#Region " Constructor"

    Shared Sub New()

        ' Color plain   
        m_plain_color = Color.White

        'Padding Menu Strip
        m_padding_menu = New System.Windows.Forms.Padding(-10)

        'General Font for all items
        Select Case Screen.PrimaryScreen.WorkingArea.Width
            Case Is < WITH_1280px

                m_font = New System.Drawing.Font(FONT_NAME, 8)
            Case Else

                m_font = New System.Drawing.Font(FONT_NAME, 10)
        End Select

        If Not m_font.Name = FONT_NAME Then
            m_font = Nothing
        End If

        'Font & Size for vertical separator (With icons)
        m_font_vertical_separator_with_icons = New System.Drawing.Font(FONT_NAME, FONT_SIZE_VERTICAL_SEPARATOR_WITH_ICONS)
        m_size_vertical_separator = New System.Drawing.Size(SIZE_WIDTH_VERTICAL_SEPARATOR, SIZE_HEIGHT_VERTICAL_SEPARATOR)

        'Font for vertical separator (WithOut icons)
        m_font_vertical_separator_without_icons = New System.Drawing.Font(FONT_NAME, FONT_SIZE_VERTICAL_SEPARATOR_WITHOUT_ICONS)

        'Font & Size for horizontal separator
        m_font_horizontal_separator = New System.Drawing.Font(FONT_NAME, FONT_SIZE_HORIZONTAL_SEPARATOR)
        m_size_horizontal_separator = New System.Drawing.Size(SIZE_WIDTH_HORIZONTAL_SEPARATOR, SIZE_HEIGHT_HORIZONTAL_SEPARATOR)

    End Sub

#End Region

End Class

Public Class CLASS_MENU_COLOR
    Inherits ProfessionalColorTable


    Public Overrides ReadOnly Property OverflowButtonGradientBegin As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property OverflowButtonGradientEnd As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property OverflowButtonGradientMiddle As Color
        Get
            Return Color.White
        End Get
    End Property


    Public Overrides ReadOnly Property MenuBorder As Color
        Get
            Return Color.LightGray
        End Get

    End Property

    Public Overrides ReadOnly Property MenuItemPressedGradientBegin As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property MenuItemPressedGradientEnd As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property ImageMarginGradientBegin As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property ImageMarginGradientEnd As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property ImageMarginGradientMiddle As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property ImageMarginRevealedGradientBegin As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property ImageMarginRevealedGradientEnd As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property ImageMarginRevealedGradientMiddle As Color
        Get
            Return Color.White
        End Get
    End Property


    Public Overrides ReadOnly Property ButtonCheckedGradientBegin As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property ButtonCheckedGradientEnd As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property ButtonCheckedGradientMiddle As Color
        Get
            Return Color.White
        End Get
    End Property


    Public Overrides ReadOnly Property ButtonCheckedHighlight As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property ButtonCheckedHighlightBorder As Color
        Get
            Return Color.White
        End Get
    End Property


    Public Overrides ReadOnly Property ButtonPressedBorder As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property ButtonPressedGradientBegin As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property ButtonPressedGradientEnd As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property ButtonPressedGradientMiddle As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property ButtonPressedHighlight As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property ButtonPressedHighlightBorder As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property ButtonSelectedBorder As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property ButtonSelectedGradientBegin As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property ButtonSelectedGradientEnd As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property ButtonSelectedGradientMiddle As Color
        Get
            Return Color.White
        End Get
    End Property

    Public Overrides ReadOnly Property ButtonSelectedHighlight As Color
        Get
            Return MyBase.ButtonSelectedHighlight
        End Get
    End Property

    Public Overrides ReadOnly Property ButtonSelectedHighlightBorder As Color
        Get
            Return MyBase.ButtonSelectedHighlightBorder
        End Get
    End Property

    Public Overrides ReadOnly Property ToolStripBorder As Color
        Get
            Return MyBase.ToolStripBorder
        End Get
    End Property

    Public Overrides ReadOnly Property ToolStripContentPanelGradientBegin As Color
        Get
            Return MyBase.ToolStripContentPanelGradientBegin
        End Get
    End Property

    Public Overrides ReadOnly Property ToolStripContentPanelGradientEnd As Color
        Get
            Return MyBase.ToolStripContentPanelGradientEnd
        End Get
    End Property

    Public Overrides ReadOnly Property ToolStripGradientBegin As Color
        Get
            Return MyBase.ToolStripGradientBegin
        End Get
    End Property

    Public Overrides ReadOnly Property ToolStripGradientEnd As Color
        Get
            Return MyBase.ToolStripGradientEnd
        End Get
    End Property

    Public Overrides ReadOnly Property ToolStripGradientMiddle As Color
        Get
            Return MyBase.ToolStripGradientMiddle
        End Get
    End Property

    Public Overrides ReadOnly Property ToolStripPanelGradientBegin As Color
        Get
            Return MyBase.ToolStripPanelGradientBegin
        End Get
    End Property

    Public Overrides ReadOnly Property ToolStripPanelGradientEnd As Color
        Get
            Return MyBase.ToolStripPanelGradientEnd
        End Get
    End Property

    Public Overrides ReadOnly Property ToolStripDropDownBackground As Color
        Get
            Return Color.White
        End Get
    End Property


    Public Overrides ReadOnly Property MenuItemBorder As Color
        Get
            Return MyBase.MenuItemBorder
        End Get
    End Property

    Public Overrides ReadOnly Property MenuStripGradientBegin As Color
        Get
            Return MyBase.MenuStripGradientBegin
        End Get
    End Property

    Public Overrides ReadOnly Property MenuStripGradientEnd As Color
        Get
            Return MyBase.MenuStripGradientEnd
        End Get
    End Property

    Public Overrides ReadOnly Property MenuItemPressedGradientMiddle As Color
        Get
            Return MyBase.MenuItemPressedGradientMiddle
        End Get
    End Property

    Public Overrides ReadOnly Property MenuItemSelected As Color
        Get
            Return MyBase.MenuItemSelected
        End Get
    End Property

    Public Overrides ReadOnly Property MenuItemSelectedGradientBegin As Color
        Get
            Return MyBase.MenuItemSelectedGradientBegin
        End Get
    End Property

    Public Overrides ReadOnly Property MenuItemSelectedGradientEnd As Color
        Get
            Return MyBase.MenuItemSelectedGradientEnd
        End Get
    End Property

    Public Overrides ReadOnly Property StatusStripGradientBegin As Color
        Get
            Return MyBase.StatusStripGradientBegin
        End Get
    End Property

    Public Overrides ReadOnly Property StatusStripGradientEnd As Color
        Get
            Return MyBase.StatusStripGradientEnd
        End Get
    End Property

    Public Overrides ReadOnly Property RaftingContainerGradientBegin As Color
        Get
            Return MyBase.RaftingContainerGradientBegin
        End Get
    End Property

    Public Overrides ReadOnly Property RaftingContainerGradientEnd As Color
        Get
            Return MyBase.RaftingContainerGradientEnd
        End Get
    End Property

    Public Overrides ReadOnly Property CheckBackground As Color
        Get
            Return MyBase.CheckBackground
        End Get
    End Property

    Public Overrides ReadOnly Property CheckPressedBackground As Color
        Get
            Return MyBase.CheckPressedBackground

        End Get
    End Property

    Public Overrides ReadOnly Property CheckSelectedBackground As Color
        Get
            Return MyBase.CheckSelectedBackground
        End Get
    End Property

    Public Overrides ReadOnly Property GripDark As Color
        Get
            Return MyBase.GripDark
        End Get
    End Property


    Public Overrides ReadOnly Property GripLight As Color
        Get
            Return MyBase.GripLight
        End Get
    End Property

    Public Overrides ReadOnly Property SeparatorDark As Color
        Get
            Return MyBase.SeparatorDark
        End Get
    End Property

    Public Overrides ReadOnly Property SeparatorLight As Color
        Get
            Return MyBase.SeparatorLight
        End Get
    End Property


End Class

Public Class CLASS_MENU_RENDERER
    Inherits ToolStripProfessionalRenderer

    Public Sub New()
        MyBase.New(New CLASS_MENU_COLOR())

    End Sub

End Class

Public Class CLASS_MENU

#Region " Enums"

    Public Enum ENUM_MENU_ICON_NAMES
        SYSTEM = 0
        TERMINALS = 1
        MONITOR = 2
        ALARMS = 3
        STATISTICS = 4
        CASH = 5
        CASH_CAGE = 6
        ACCOUNTING = 7
        CUSTOMERS = 8
        MARKETING = 9
        JACKPOT = 10
        GAMBLING_TABLES = 11
        SOFTWARE_VERSIONS = 12
        TOOLS = 13
        WINDOW = 14

        'Only for MultiSite
        AUDIT = 15
        EVENTS = 16

        MONEY_REQUEST = 17

        INTELLIA = 18
        GAMEGATEWAY = 19

        APPMAZING = 20

        WIGOS_LOGO = 22

    End Enum

    Public Enum ENUM_POSITION_MENU_STRIP
        VERTICAL = 0
        HORIZONTAL = 1

    End Enum

#End Region

#Region " Members"
    Private m_main_menu As MenuStrip
    Private m_items As Collection
    Private m_context_main_menu As CLASS_MENU_CONTEXT
    Private m_position_separator As ENUM_POSITION_MENU_STRIP
    Private m_gui_module As CLASS_MODULE

#End Region

#Region " Constructor / Destructor"

    Public Sub New()

        'Instances
        m_items = New Collection()
        m_main_menu = New MenuStrip()

        m_main_menu.RenderMode = ToolStripRenderMode.Professional
        m_main_menu.Renderer = New CLASS_MENU_RENDERER()
        m_gui_module = New CLASS_MODULE

        '**GET Parameters Data from Registry
        GetDataFromRegistry(m_gui_module)

        'Showicons
        CLASS_MENU_CUSTOMIZE.m_show_icons = IIf(m_gui_module.MenuStripShowIcons = String.Empty, True, m_gui_module.MenuStripShowIcons)

        'Position
        Select Case m_gui_module.MenuStripPosition.Substring(0, 1).ToUpper
            Case "L"
                CLASS_MENU_CUSTOMIZE.m_position = System.Windows.Forms.DockStyle.Left
            Case "R"
                CLASS_MENU_CUSTOMIZE.m_position = System.Windows.Forms.DockStyle.Right
            Case "T"
                CLASS_MENU_CUSTOMIZE.m_position = System.Windows.Forms.DockStyle.Top
            Case "B"
                CLASS_MENU_CUSTOMIZE.m_position = System.Windows.Forms.DockStyle.Bottom
            Case Else
                CLASS_MENU_CUSTOMIZE.m_position = System.Windows.Forms.DockStyle.Top
        End Select
        '**

        'Customize (style, color, etc.)
        m_main_menu.CanOverflow = True
        m_main_menu.BackColor = CLASS_MENU_CUSTOMIZE.m_plain_color
        m_main_menu.Padding = CLASS_MENU_CUSTOMIZE.m_padding_menu

        'Orientation for Vertical Separator (internal)
        m_position_separator = GetVerticalSeparatorOrientation(CLASS_MENU_CUSTOMIZE.m_position)

        ' Position
        'Set orientation of the menu
        Call SetPositionMenuStrip(CLASS_MENU_CUSTOMIZE.m_position)

        'Create Context Menu
        m_context_main_menu = New CLASS_MENU_CONTEXT(m_main_menu)
        m_main_menu.ContextMenuStrip = m_context_main_menu.ContextMenuStrp
        AddHandler m_context_main_menu.MenuContext_OnClick, AddressOf MenuContextOnClick

    End Sub

#End Region

#Region " Properties"

    Public Property Menu() As MenuStrip
        Get
            Return m_main_menu
        End Get
        Set(ByVal Value As MenuStrip)
            m_main_menu = Value
        End Set
    End Property

#End Region

#Region " Private Methods"

    ' PURPOSE: Sets Position of the menu (Top, Bottom, Left, Right)
    '
    '  PARAMS:
    '     - INPUT: 
    '               CtrlMenuStrip:  MenuStrip
    '               Position:       Windows.Forms.DockStyle (Top, Bottom, Left, Right)
    '     - OUTPUT:
    '
    ' RETURNS:
    Private Sub SetPositionMenuStrip(ByVal Position As System.Windows.Forms.DockStyle)

        Try

            Select Case Position

                Case DockStyle.Top, _
                     DockStyle.Bottom

                    'Set orientation of the menu
                    m_main_menu.Dock = Position

                    'Set Orientation of Verticals Separators
                    Me.SetOrientarionVerticalSeparator(ENUM_POSITION_MENU_STRIP.HORIZONTAL)

                Case DockStyle.Left, _
                     DockStyle.Right

                    'Set orientation of the menu
                    m_main_menu.Dock = Position

                    'Set Orientation of Verticals Separators
                    Me.SetOrientarionVerticalSeparator(ENUM_POSITION_MENU_STRIP.VERTICAL)

            End Select

            'Get Orientation Separators
            m_position_separator = GetVerticalSeparatorOrientation(Position)

        Catch ex As Exception

            WSI.Common.Log.Exception(ex)
        End Try
    End Sub ' SetPositionMenuStrip

    ' PURPOSE: Configure separators if position menu it is on Vertical or Horizontal
    '
    '  PARAMS:
    '     - INPUT: 
    '               CtrlMenuStrip:  MenuStrip
    '               Position:       Vertical / Horizontal
    '               ItemWidth:      Item width who has the maximum width
    '     - OUTPUT:
    '
    ' RETURNS:
    Private Sub SetOrientarionVerticalSeparator(ByVal Position As ENUM_POSITION_MENU_STRIP)

        Try
            For _idx As Integer = 0 To m_main_menu.Items.Count - 1

                Select Case True
                    Case TypeOf m_main_menu.Items(_idx) Is ToolStripTextBox

                        If Position = ENUM_POSITION_MENU_STRIP.VERTICAL Then

                            ' Vertical 
                            m_main_menu.Items.Item(_idx).Font = CLASS_MENU_CUSTOMIZE.m_font_horizontal_separator

                        Else
                            ' Horizontal
                            If CLASS_MENU_CUSTOMIZE.m_show_icons Then
                                m_main_menu.Items.Item(_idx).Font = CLASS_MENU_CUSTOMIZE.m_font_vertical_separator_with_icons
                            Else
                                m_main_menu.Items.Item(_idx).Font = CLASS_MENU_CUSTOMIZE.m_font_vertical_separator_without_icons
                            End If

                            m_main_menu.Items.Item(_idx).Size = New System.Drawing.Size(CLASS_MENU_CUSTOMIZE.m_size_vertical_separator.Width, _
                                                                                        m_main_menu.Items.Item(_idx).Size.Height)
                        End If

                End Select

            Next

        Catch ex As Exception

            WSI.Common.Log.Exception(ex)
        End Try

    End Sub ' SetOrientarionVerticalSeparator

    ' PURPOSE: Shows or hides Icons from menu
    '
    '  PARAMS:
    '     - INPUT: 
    '               ShowIcons  : boolean
    '     - OUTPUT:
    '
    ' RETURNS:
    Private Sub ShowHideMenuIcons(ByVal ShowIcons As Boolean)

        Try

            For _idx As Integer = 0 To m_main_menu.Items.Count - 1
                If TypeOf m_main_menu.Items(_idx) Is ToolStripMenuItem Then
                    If m_main_menu.Items(_idx).Name = CLASS_MENU_CUSTOMIZE.KEY_ID_MENU_ITEM Then
                        If ShowIcons Then
                            m_main_menu.Items(_idx).Image = m_main_menu.Items(_idx).Tag
                        Else
                            m_main_menu.Items(_idx).Image = Nothing
                        End If
                    End If
                End If
            Next

        Catch ex As Exception

            WSI.Common.Log.Exception(ex)
        End Try
    End Sub ' ShowHideMenuIcons

    ' PURPOSE: Control of events from ContextMenuStrip
    '
    '  PARAMS:
    '     - INPUT: 
    '               sender  : Object
    '               e       : System.Windows.Forms.ToolStripItemClickedEventArgs
    '               position: System.Windows.Forms.DockStyle
    '     - OUTPUT:
    '
    ' RETURNS:
    Private Sub MenuContextOnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs)

        Dim _tool_strip_item As ToolStripMenuItem
        Dim _tool_strip_menu_item As ToolStripMenuItem
        Dim _context_menu_strip As ContextMenuStrip
        Dim _idx As Integer
        Dim _gui_module As CLASS_MODULE
        Dim _save_to_registry As Boolean

        Try
            _gui_module = New CLASS_MODULE
            _save_to_registry = False
            _idx = 0

            Select Case True
                Case TypeOf sender Is ContextMenuStrip

                    '*SHOW / HIDE ICONS
                    _context_menu_strip = CType(sender, ContextMenuStrip)
                    _tool_strip_menu_item = CType(e.ClickedItem, ToolStripMenuItem)
                    _idx = _context_menu_strip.Items.IndexOf(_tool_strip_menu_item)

                    Select Case _idx
                        Case 1 '1: Show Icons (checked: show icons; not checked: hide icons)

                            'Set the check icon
                            _tool_strip_menu_item.Checked = Not _tool_strip_menu_item.Checked
                            CLASS_MENU_CUSTOMIZE.m_show_icons = _tool_strip_menu_item.Checked

                            'Show Icons or not 
                            ShowHideMenuIcons(CLASS_MENU_CUSTOMIZE.m_show_icons)

                            'Customize menu aspect
                            SetPositionMenuStrip(m_main_menu.Dock)

                            'Flag for saving menu parameters to registry 
                            _save_to_registry = True

                    End Select

                Case TypeOf sender Is ToolStripMenuItem

                    '*POSITION
                    If TypeOf e.ClickedItem Is ToolStripMenuItem Then
                        _tool_strip_menu_item = CType(e.ClickedItem, ToolStripMenuItem)

                        If Not _tool_strip_menu_item.OwnerItem Is Nothing Then

                            _tool_strip_item = _tool_strip_menu_item.OwnerItem
                            _idx = _tool_strip_item.DropDownItems.IndexOf(_tool_strip_menu_item)

                            Select Case _idx
                                Case 0 'Top

                                    SetPositionMenuStrip(DockStyle.Top)
                                Case 1 'Bottom

                                    SetPositionMenuStrip(DockStyle.Bottom)
                                Case 2 'Left

                                    SetPositionMenuStrip(DockStyle.Left)
                                Case 3 'Right

                                    SetPositionMenuStrip(DockStyle.Right)
                            End Select

                            'Change check position
                            _tool_strip_menu_item.Checked = True

                            'Flag for saving menu parameters to registry 
                            _save_to_registry = True

                        End If
                    End If
            End Select

            'Save menu parameters to resgistry 
            If _save_to_registry Then

                _gui_module.MenuStripShowIcons = IIf(CLASS_MENU_CUSTOMIZE.m_show_icons, "1", "0")
                _gui_module.MenuStripPosition = Me.m_main_menu.Dock.ToString
                Call SetDataToRegistry(_gui_module)
            End If

        Catch ex As Exception

            WSI.Common.Log.Exception(ex)
        End Try
    End Sub ' MenuContextOnClick

    ' PURPOSE: Gets Vertical Separator orientation
    '
    '  PARAMS:
    '     - INPUT: 
    '               position  : dockstyle
    '     - OUTPUT:
    '
    ' RETURNS:
    Private Function GetVerticalSeparatorOrientation(ByVal Position As DockStyle) As ENUM_POSITION_MENU_STRIP
        Select Case Position
            Case DockStyle.Top, _
                 DockStyle.Bottom

                Return ENUM_POSITION_MENU_STRIP.HORIZONTAL

            Case DockStyle.Left, _
                 DockStyle.Right

                Return ENUM_POSITION_MENU_STRIP.VERTICAL

        End Select

        Return ENUM_POSITION_MENU_STRIP.HORIZONTAL

    End Function ' GetVerticalSeparatorOrientation

#End Region

#Region " Public Methods"

    Public Sub AddPopupItem(ByVal Item As CLASS_MENU_ITEM)

        Call m_main_menu.Items.Add(Item.MenuItem)
        Call m_items.Add(Item)

    End Sub

    Public Function AddItem(ByVal NlsId As Integer, _
                            ByVal FrmId As Integer, _
                            Optional ByVal OnClick As EventHandler = Nothing, _
                            Optional ByVal ImageItem As System.Drawing.Image = Nothing, _
                            Optional ByVal Alignment As System.Windows.Forms.ToolStripItemAlignment = ToolStripItemAlignment.Left, _
                            Optional ByVal IsRoot As Boolean = False) As CLASS_MENU_ITEM

        Return AddItem(NLS_GetString(NlsId), FrmId, OnClick, ImageItem, Alignment, IsRoot)
    End Function

    Public Function AddItem(ByVal NlsStr As String, _
                          ByVal FrmId As Integer, _
                          Optional ByVal OnClick As EventHandler = Nothing, _
                          Optional ByVal ImageItem As System.Drawing.Image = Nothing, _
                          Optional ByVal Alignment As System.Windows.Forms.ToolStripItemAlignment = ToolStripItemAlignment.Left, _
                          Optional ByVal IsRoot As Boolean = False) As CLASS_MENU_ITEM

        Dim itm As CLASS_MENU_ITEM
        Dim _type As CLASS_MENU_ITEM.ENUM_MENU_ITEM_TYPE

        _type = CLASS_MENU_ITEM.ENUM_MENU_ITEM_TYPE.NORMAL_ITEM
        If IsRoot Then
            _type = CLASS_MENU_ITEM.ENUM_MENU_ITEM_TYPE.ROOT_ITEM
        End If

        itm = New CLASS_MENU_ITEM(_type)


        If Not ImageItem Is Nothing Then
            itm.MenuItem.Tag = ImageItem
            If CLASS_MENU_CUSTOMIZE.m_show_icons Then
                itm.MenuItem.Image = ImageItem
            End If
        End If

        itm.MenuItem.Text = NlsStr
        itm.MenuItem.Enabled = True

        itm.MenuItem.Alignment = Alignment

        itm.FormId = FrmId
        AddHandler itm.MenuItem.Click, OnClick

        Call m_main_menu.Items.Add(itm.MenuItem)
        Call m_items.Add(itm)

        Return itm
    End Function

    Public Function AddItem(ByVal Menu As CLASS_MENU_ITEM, _
                            ByVal NlsId As Integer, _
                            ByVal FrmId As Integer, _
                            Optional ByVal OnClick As EventHandler = Nothing) As CLASS_MENU_ITEM

        Dim itm As CLASS_MENU_ITEM

        itm = New CLASS_MENU_ITEM(CLASS_MENU_ITEM.ENUM_MENU_ITEM_TYPE.NORMAL_ITEM)

        itm.MenuItem.Text = NLS_GetString(NlsId)
        itm.MenuItem.Enabled = True
        itm.FormId = FrmId
        AddHandler itm.MenuItem.Click, OnClick

        Menu.MenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {itm.MenuItem})
        Call m_items.Add(itm)

        Return itm
    End Function

    Public Function AddItem(ByVal Menu As CLASS_MENU_ITEM, _
                          ByVal StringNls As String, _
                          ByVal FrmId As Integer, _
                          Optional ByVal OnClick As EventHandler = Nothing) As CLASS_MENU_ITEM

        Dim itm As CLASS_MENU_ITEM

        itm = New CLASS_MENU_ITEM(CLASS_MENU_ITEM.ENUM_MENU_ITEM_TYPE.NORMAL_ITEM)

        itm.MenuItem.Text = StringNls
        itm.MenuItem.Enabled = True
        itm.FormId = FrmId
        AddHandler itm.MenuItem.Click, OnClick

        Menu.MenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {itm.MenuItem})
        Call m_items.Add(itm)

        Return itm
    End Function

    Public Sub AddBreak(ByVal menu As CLASS_MENU_ITEM)
        Dim itm As CLASS_MENU_ITEM

        itm = New CLASS_MENU_ITEM(CLASS_MENU_ITEM.ENUM_MENU_ITEM_TYPE.SEPARATOR_ITEM)

        If Not IsNothing(menu) Then
            menu.MenuItem.DropDownItems.Add(itm.MenuDropDownItemSeparator)
        End If
    End Sub

    ' PURPOSE: Insert a vertical separator between principal options from menu
    '           - Set form title
    '
    '  PARAMS:
    '     - INPUT:
    '
    '     - OUTPUT:
    '
    ' RETURNS:
    Public Sub AddVerticalBreak()
        Dim itm_vertical_sep As CLASS_MENU_ITEM

        itm_vertical_sep = New CLASS_MENU_ITEM(CLASS_MENU_ITEM.ENUM_MENU_ITEM_TYPE.VERTICAL_SEPARATOR_ITEM)

        m_main_menu.Items.Add(itm_vertical_sep.MenuVerticalSeparator)

        'Refresh Position of vertical separators
        Call Me.SetOrientarionVerticalSeparator(m_position_separator)
    End Sub

    ' PURPOSE: Make visible the item menu according Nls id
    '
    '  PARAMS:
    '     - INPUT:
    '             Id NLS
    '             Visible
    '
    '     - OUTPUT:
    '
    ' RETURNS:
    Public Sub ItemVisible(ByVal NlsId As Integer, ByVal Visible As Boolean)
        For _idx As Integer = 0 To m_main_menu.Items.Count - 1
            If m_main_menu.Items(_idx).Text = NLS_GetString(NlsId) Then
                m_main_menu.Items(_idx).Visible = Visible
            End If
        Next
    End Sub

    ' PURPOSE: Make efect blinking in text item main menu according Nls id
    '
    '  PARAMS:
    '     - INPUT:
    '             Id NLS
    '
    '     - OUTPUT:
    '
    ' RETURNS:
    Public Sub ItemTextBlinking(ByVal NlsId As Integer)
        For _idx As Integer = 0 To m_main_menu.Items.Count - 1
            If m_main_menu.Items(_idx).Text = NLS_GetString(NlsId) Then
                If m_main_menu.Items(_idx).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00) Then
                    m_main_menu.Items(_idx).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREEN_02)
                Else
                    m_main_menu.Items(_idx).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
                End If
            End If
        Next
    End Sub

    Public Sub SetHeight(ByVal height As Integer)
        m_main_menu.AutoSize = False
        m_main_menu.Size = New Size(m_main_menu.Size.Width, height)

    End Sub

#End Region

#Region " Events"

#End Region

End Class

Public Class ToolStripMenuItemCustom
    Inherits ToolStripMenuItem

    Public Overrides ReadOnly Property CanSelect As Boolean
        Get
            Return False
        End Get
    End Property

End Class

Public Class CLASS_MENU_ITEM

#Region " Enums"

    Public Enum ENUM_MENU_ITEM_TYPE
        NORMAL_ITEM = 0
        SEPARATOR_ITEM = 1
        VERTICAL_SEPARATOR_ITEM = 2
        ROOT_ITEM = 3
    End Enum

#End Region

#Region " Members"
    Private m_frm_id As Integer
    Private m_mnu As ToolStripMenuItem
    Private m_mnu_dropdownitem_separator As ToolStripSeparator
    Private m_mnu_vertical_separator As ToolStripTextBox
    Private m_enter_image As Image
    Private m_leave_image As Image
    Private m_is_opened As Boolean
#End Region

#Region " Constructor / Destructor"

    Public Sub New(ByVal TypeItem As ENUM_MENU_ITEM_TYPE)

        Select Case TypeItem
            Case ENUM_MENU_ITEM_TYPE.NORMAL_ITEM, ENUM_MENU_ITEM_TYPE.ROOT_ITEM
                'Instance 
                m_mnu = New ToolStripMenuItem()

                If TypeItem = ENUM_MENU_ITEM_TYPE.ROOT_ITEM Then
                    m_mnu = New ToolStripMenuItemCustom()
                    m_mnu.ForeColor = Color.FromArgb(15, 44, 74)

                End If
                m_mnu.Name = CLASS_MENU_CUSTOMIZE.KEY_ID_MENU_ITEM 'Important
                m_mnu.AutoSize = True


                'Customize
                If Not CLASS_MENU_CUSTOMIZE.m_font Is Nothing Then
                    m_mnu.Font = CLASS_MENU_CUSTOMIZE.m_font
                End If

                m_mnu.TextImageRelation = TextImageRelation.ImageAboveText
                m_mnu.ImageTransparentColor = Drawing.Color.White
                m_mnu.ImageScaling = ToolStripItemImageScaling.None
                m_mnu.Overflow = ToolStripItemOverflow.AsNeeded
                m_mnu.ImageAlign = ContentAlignment.MiddleCenter
                m_mnu.BackColor = Color.White

            Case ENUM_MENU_ITEM_TYPE.SEPARATOR_ITEM
                'Instance 
                m_mnu_dropdownitem_separator = New ToolStripSeparator
                m_mnu_dropdownitem_separator.Name = CLASS_MENU_CUSTOMIZE.KEY_ID_MENU_ITEM

            Case ENUM_MENU_ITEM_TYPE.VERTICAL_SEPARATOR_ITEM
                'Instance 
                m_mnu_vertical_separator = New ToolStripTextBox
                m_mnu_vertical_separator.Name = CLASS_MENU_CUSTOMIZE.KEY_ID_MENU_ITEM

                'Customize
                m_mnu_vertical_separator.Enabled = False
                m_mnu_vertical_separator.BorderStyle = BorderStyle.None
                m_mnu_vertical_separator.BackColor = Drawing.Color.Gray

                If Not CLASS_MENU_CUSTOMIZE.m_font Is Nothing Then
                    If CLASS_MENU_CUSTOMIZE.m_show_icons Then
                        m_mnu_vertical_separator.Font = CLASS_MENU_CUSTOMIZE.m_font_vertical_separator_with_icons
                    Else
                        m_mnu_vertical_separator.Font = CLASS_MENU_CUSTOMIZE.m_font_vertical_separator_without_icons
                    End If
                End If
                m_mnu_vertical_separator.Size = CLASS_MENU_CUSTOMIZE.m_size_vertical_separator 'By Default: Vertical

        End Select

    End Sub

#End Region

#Region " Properties"
    Public Property MenuItem() As ToolStripMenuItem
        Get
            Return m_mnu
        End Get
        Set(ByVal Value As ToolStripMenuItem)
            m_mnu = Value
        End Set
    End Property

    Public Property MenuVerticalSeparator() As ToolStripTextBox
        Get
            Return m_mnu_vertical_separator
        End Get
        Set(ByVal Value As ToolStripTextBox)
            m_mnu_vertical_separator = Value
        End Set
    End Property

    Public Property MenuDropDownItemSeparator() As ToolStripSeparator
        Get
            Return m_mnu_dropdownitem_separator
        End Get
        Set(ByVal Value As ToolStripSeparator)
            m_mnu_dropdownitem_separator = Value
        End Set
    End Property

    Public Property FormId() As Integer
        Get
            Return m_frm_id
        End Get
        Set(ByVal Value As Integer)
            m_frm_id = Value
        End Set
    End Property

    Public Property Status() As Boolean
        Get
            Return m_mnu.Enabled
        End Get
        Set(ByVal Value As Boolean)
            m_mnu.Enabled = Value
        End Set
    End Property

    Public Property Visible() As Boolean
        Get
            Return m_mnu.Visible
        End Get
        Set(ByVal Value As Boolean)
            m_mnu.Visible = Value
        End Set
    End Property

#End Region

#Region " Public Methods"
    Public Sub AddEvents(ByVal enterImage As Image)
        m_enter_image = enterImage
        m_leave_image = m_mnu.Image
        m_mnu.AutoSize = False

        If Not enterImage Is Nothing AndAlso Not m_mnu.Image Is Nothing Then
            AddHandler m_mnu.MouseEnter, AddressOf OnMouseEnter
            AddHandler m_mnu.MouseLeave, AddressOf OnMouseLeave
            AddHandler m_mnu.MouseUp, AddressOf OnMouseUp
            AddHandler m_mnu.DropDownOpened, AddressOf OnDropDownOpened
            AddHandler m_mnu.DropDownClosed, AddressOf OnDropDownClosed

        End If

    End Sub
#End Region

#Region " Events"
    Public Sub OnMouseEnter(sender As Object, e As EventArgs)

        Dim _menuHeader As MenuStrip

        If (Not m_enter_image Is Nothing) Then
            If (Not m_is_opened AndAlso Me.m_mnu.OwnerItem Is Nothing) Then
                Me.m_mnu.Image = m_enter_image
            End If
        End If


        If (Me.m_mnu.OwnerItem Is Nothing) Then
            If (m_mnu.GetType() Is GetType(ToolStripMenuItemCustom)) Then

                _menuHeader = Me.m_mnu.Owner

                For Each _item As ToolStripMenuItem In _menuHeader.Items
                    If (_item.Text <> Me.m_mnu.Text) Then

                        If _item.Pressed Then
                            Me.m_mnu.ShowDropDown()
                            Exit For

                        End If
                    End If
                Next

            End If
        Else
            _menuHeader = Me.m_mnu.Owner
            For Each _item As ToolStripMenuItem In _menuHeader.Items
                If (_item.Text <> Me.m_mnu.Text) Then
                    If (_item.IsOnDropDown) Then
                        _item.HideDropDown()
                    End If

                End If
            Next

            Me.m_mnu.ShowDropDown()
        End If



    End Sub

    Public Sub OnMouseLeave(sender As Object, e As EventArgs)
        If (Not m_leave_image Is Nothing) Then
            If (Not m_is_opened) Then
                Me.m_mnu.Image = m_leave_image
            End If
        End If

    End Sub

    Public Sub OnMouseUp(sender As Object, e As EventArgs)
        If (Not m_enter_image Is Nothing) Then
            Me.m_mnu.Image = m_enter_image

        End If

    End Sub

    Public Sub OnDropDownOpened(sender As Object, e As EventArgs)
        If (Not m_enter_image Is Nothing) Then
            Me.m_mnu.Image = m_enter_image
            m_is_opened = True
        End If

    End Sub

    Public Sub OnDropDownClosed(sender As Object, e As EventArgs)
        If (Not m_leave_image Is Nothing) Then
            Me.m_mnu.Image = m_leave_image
            m_is_opened = False
        End If

    End Sub


#End Region

End Class

Public Class CLASS_MENU_CONTEXT

#Region " Members"

    Private m_context_menu As ContextMenuStrip
#End Region

#Region " Constructor / Destructor"

    Public Sub New(ByRef MenuStrip As MenuStrip)

        'Instance Context Menu
        m_context_menu = New ContextMenuStrip

        'Create MenuItems for Context Menu
        Call CreatePositionMenuItems()

    End Sub
#End Region

#Region " Properties"
    Public Property ContextMenuStrp() As ContextMenuStrip
        Get
            Return m_context_menu
        End Get
        Set(ByVal value As ContextMenuStrip)
            m_context_menu = value
        End Set
    End Property


#End Region

#Region " Public Methods"

#End Region

#Region " Private Methods"

    ' PURPOSE: Create Menu Items for context Menu (TOP, LEFT, RIGHT, LEFT)
    '
    '  PARAMS:
    '     - INPUT: 
    '     - OUTPUT:
    ' RETURNS:
    Private Sub CreatePositionMenuItems()
        Dim _tool_strip_itm As ToolStripMenuItem
        Dim _tool_strip_sub_itm As ToolStripMenuItem

        '*Main Item
        _tool_strip_itm = m_context_menu.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5828))
        AddHandler m_context_menu.ItemClicked, AddressOf MenuContext

        'Link event
        AddHandler _tool_strip_itm.DropDownItemClicked, AddressOf MenuContext

        '*SubItems (Position Top, Left, etc.)
        _tool_strip_sub_itm = _tool_strip_itm.DropDownItems.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5829)) 'Position = > TOP
        If CLASS_MENU_CUSTOMIZE.m_position = DockStyle.Top Then
            _tool_strip_sub_itm.Checked = True
        End If
        _tool_strip_sub_itm = _tool_strip_itm.DropDownItems.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5830)) 'Position = > BOTTOM
        If CLASS_MENU_CUSTOMIZE.m_position = DockStyle.Bottom Then
            _tool_strip_sub_itm.Checked = True
        End If
        _tool_strip_sub_itm = _tool_strip_itm.DropDownItems.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5831)) 'Position = > LEFT
        If CLASS_MENU_CUSTOMIZE.m_position = DockStyle.Left Then
            _tool_strip_sub_itm.Checked = True
        End If
        _tool_strip_sub_itm = _tool_strip_itm.DropDownItems.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5832)) 'Position = > RIGHT
        If CLASS_MENU_CUSTOMIZE.m_position = DockStyle.Right Then
            _tool_strip_sub_itm.Checked = True
        End If

        'Show / Hide Icons
        _tool_strip_itm = m_context_menu.Items.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5851))  ' Show Icons NLS
        _tool_strip_itm.Checked = CLASS_MENU_CUSTOMIZE.m_show_icons

    End Sub ' CreatePositionMenuItems

    ' PURPOSE: Event for MenuStripItems control (contextual menu)
    '
    '  PARAMS:
    '     - INPUT: 
    '               Sender (Object)
    '               e      (System.Windows.Forms.ToolStripItemClickedEventArgs)
    '     - OUTPUT:
    '
    ' RETURNS:
    Private Sub MenuContext(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs)
        'Reset Check State if necessary
        ResetCheckContextMenu(e)

        'Raise Event
        RaiseEvent MenuContext_OnClick(sender, e)
    End Sub ' MenuContext

    ' PURPOSE: Reset Check state on context menu strip
    '
    '  PARAMS:
    '     - INPUT: 
    '               Sender (Object)
    '               e      (System.Windows.Forms.ToolStripItemClickedEventArgs)
    '     - OUTPUT:
    '
    ' RETURNS:
    Private Sub ResetCheckContextMenu(ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs)
        Dim _tool_strip_menu_item_aux As ToolStripMenuItem
        Dim _continue_change_check_status As Boolean
        Dim _owner_item As Object

        _continue_change_check_status = True
        _owner_item = Nothing

        'Check class item for processing or not
        If TypeOf e.ClickedItem Is ToolStripMenuItem Then
            _tool_strip_menu_item_aux = CType(e.ClickedItem, ToolStripMenuItem)

            If _tool_strip_menu_item_aux.HasDropDownItems Then

                _continue_change_check_status = False
            Else

                If Not _tool_strip_menu_item_aux.Owner Is Nothing AndAlso _
                   Not TypeOf _tool_strip_menu_item_aux.Owner Is ContextMenuStrip Then

                    _owner_item = _tool_strip_menu_item_aux.Owner
                Else

                    _continue_change_check_status = False
                End If
            End If
        End If

        'Change check status if needed
        If _continue_change_check_status Then
            For intidn As Integer = 0 To _owner_item.Items.Count - 1
                If TypeOf _owner_item.Items(intidn) Is ToolStripMenuItem Then

                    _tool_strip_menu_item_aux = CType(_owner_item.Items(intidn), ToolStripMenuItem)
                    _tool_strip_menu_item_aux.Checked = False
                End If
            Next
        End If
    End Sub ' ResetCheckContextMenu

#End Region

#Region " Events"

    Public Event MenuContext_OnClick(ByVal sender As Object, _
                                     ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs)
#End Region

End Class