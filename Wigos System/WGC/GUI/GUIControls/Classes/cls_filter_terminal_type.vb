'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   cls_filter_terminal_type.vb  
'
' DESCRIPTION:   terminal type filter management
'
' AUTHOR:        Raul Cervera
'
' CREATION DATE: 21-MAR-2011
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 21-MAR-2011  RCI    Initial version
'-------------------------------------------------------------------

Option Explicit On

Public Class TYPE_TERMINAL_SEL_PARAMS

  'IMPORTANT! NUM_CHECKBOXES indicates the number of terminal types.
  '           If you add a new terminal type, increment this value!!
  Protected Const NUM_CHECKBOXES = 12

  Private Class TYPE_TERMINAL_SEL_PARAMS_TYPE

    Public default_filter As Boolean
    Public id(0 To NUM_CHECKBOXES - 1) As Integer
    Public value(0 To NUM_CHECKBOXES - 1) As Boolean
    Public enabled(0 To NUM_CHECKBOXES - 1) As Boolean
    Public locked(0 To NUM_CHECKBOXES - 1) As Boolean

    Public Sub New()

      Dim _idx As Integer

      default_filter = True

      Me.id = WSI.Common.Misc.AllTerminalTypes()

      ' Default values: all unchecked, enabled and unlocked
      For _idx = 0 To NUM_CHECKBOXES - 1
        Me.value(_idx) = False
        Me.enabled(_idx) = True
        Me.locked(_idx) = False
      Next

    End Sub

    Public Function SearchIdx(ByVal Id As Integer) As Integer

      Dim _idx As Integer

      For _idx = 0 To NUM_CHECKBOXES - 1
        If Me.id(_idx) = Id Then
          Return _idx
        End If
      Next

      Return -1   ' Not found

    End Function  ' SearchIdx

  End Class   ' Class TYPE_TERMINAL_SEL_PARAMS_TYPE

  Private filter_type As TYPE_TERMINAL_SEL_PARAMS_TYPE

  Public Sub New()

    Me.filter_type = New TYPE_TERMINAL_SEL_PARAMS_TYPE

  End Sub

  Public Property FilterTypeDefault() As Boolean
    Get
      Return filter_type.default_filter
    End Get

    Set(ByVal Value As Boolean)
      filter_type.default_filter = Value
    End Set

  End Property

  Public Property FilterTypeValue(ByVal Id As Integer) As Boolean
    Get
      Dim _idx As Integer

      _idx = Me.filter_type.SearchIdx(Id)

      If _idx = -1 Then
        Return False
      End If

      Return Me.filter_type.value(_idx)
    End Get

    Set(ByVal Value As Boolean)
      Dim _idx As Integer

      If Id = 0 Then
        For _idx = 0 To NUM_CHECKBOXES - 1
          Me.filter_type.value(_idx) = Value
        Next
      Else
        _idx = Me.filter_type.SearchIdx(Id)

        If _idx <> -1 Then
          Me.filter_type.value(_idx) = Value
        End If
      End If
    End Set

  End Property

  Public Property FilterTypeEnable(ByVal Id As Integer) As Boolean
    Get
      Dim _idx As Integer

      _idx = Me.filter_type.SearchIdx(Id)

      If _idx = -1 Then
        Return False
      End If

      Return Me.filter_type.enabled(_idx)
    End Get

    Set(ByVal Value As Boolean)
      Dim _idx As Integer

      If Id = 0 Then
        For _idx = 0 To NUM_CHECKBOXES - 1
          Me.filter_type.enabled(_idx) = Value
        Next
      Else
        _idx = Me.filter_type.SearchIdx(Id)

        If _idx <> -1 Then
          Me.filter_type.enabled(_idx) = Value
        End If
      End If
    End Set

  End Property

  Public Property FilterTypeLocked(ByVal Id As Integer) As Boolean
    Get
      Dim _idx As Integer

      _idx = Me.filter_type.SearchIdx(Id)

      If _idx = -1 Then
        Return False
      End If

      Return Me.filter_type.locked(_idx)
    End Get

    Set(ByVal Value As Boolean)
      Dim _idx As Integer

      If Id = 0 Then
        For _idx = 0 To NUM_CHECKBOXES - 1
          Me.filter_type.locked(_idx) = Value
        Next
      Else
        _idx = Me.filter_type.SearchIdx(Id)

        If _idx <> -1 Then
          Me.filter_type.locked(_idx) = Value
        End If
      End If
    End Set

  End Property

End Class     ' Class TYPE_TERMINAL_SEL_PARAMS
