'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : mdl_globals.vb
'
' DESCRIPTION : Global definitions on GUI_CommonMisc.dll
'
'      AUTHOR : AJQ
'
' CREATION DATE: 
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
'                    Initial version
' 05-MAR-2013 MPO    Added MULTISITE_GUI = 200
' 09-MAY-2013 JCA    Added ENUM_TABLES_MASTER_MODE
' 27-AUG-2013 JCA    Added CASHDESK_DRAWS_GUI = 201
'-------------------------------------------------------------------

Module mdl_globals
  Public GLB_NLS_GUI_COMMONMISC As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_GUI_COMMONMISC)
End Module

Public Module public_globals

  Public GLB_ApplicationName As String = ""
  Public GLB_DbServerName As String = ""
  'Public GLB_SiteId As String = ""
  'Public GLB_SiteName As String = ""
  Public GLB_GuiMode As ENUM_GUI
  Public GLB_StartedInSelectSiteMode As Boolean = False


  ' Audit names
  ' Must match with Audit constants on CommonDataAuditor.cpp (CommonData.dll)
  ' Please check also on mdl_globals on GUI_Controls and GUI_CommonMisc for names related to classes defined there.
  ' Changes on the current codes will make incompatible audit codes with older versions display.
  Public AUDIT_NAME_GENERIC As Integer = 0

  ' Track data length used in WSI's cards
  Public Const TRACK_DATA_LENGTH_WIN As Integer = 20

  Public GLB_PrinterSettings As New Drawing.Printing.PrinterSettings()

  Public Enum ENUM_GUI
    GUI_SETUP = 0
    GUI_ALARMS = 1
    GUI_VERSION_CONTROL = 2
    GUI_AUDITOR = 3
    GUI_CONFIGURATION = 4
    GUI_COMMUNICATIONS = 5
    GUI_CONTROL = 6
    GUI_TICKETSMANAGER = 7
    GUI_STATISTICS = 8
    GUI_JACKPOTMANAGER = 9
    GUI_SYSTEMMONITOR = 10
    GUI_INVOICE_SYSTEM = 11
    GUI_SOFTWARE_DOWNLOAD = 12
    GUI_DATECODE = 13
    WIGOS_GUI = 14
    CASHIER = 15
    WINUP = 16
    MOBIBANK = 17
    ISTATS = 104
    MULTISITE_GUI = 200
    CASHDESK_DRAWS_GUI = 201
    CHILE_SIOC = 202
    SMARTFLOOR = 203
    SMARTFLOOR_APP = 204
  End Enum

  Public Enum ENUM_TABLES_MASTER_MODE
    MODE_DEFAULT = 0
    MODE_MULTISITE_CENTER = 1
    MODE_MULTISITE_SITE = 2
  End Enum

  ' PURPOSE: Gets GUI name
  '
  '  PARAMS:
  '     - INPUT:
  '           - GuiId
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - string:  Gui name
  '
  Public Function GUI_ModuleName(ByVal GuiId As ENUM_GUI) As String
    'XVV 13/04/2007
    'FALTA COMMENT
    Return GuiId.ToString() '  GetName(GetType(ENUM_GUI), GuiId)
  End Function

End Module