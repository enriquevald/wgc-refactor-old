' AJQ Aix� ja no s'utilitza


''''''''''''-------------------------------------------------------------------
'''''''''''' Copyright � 2002 Win Systems Ltd.
''''''''''''-------------------------------------------------------------------
''''''''''''
'''''''''''' MODULE NAME:   mdl_logg.vb
'''''''''''' DESCRIPTION:   Handle writes in log file
'''''''''''' AUTHOR:        Carlos A. Costa
'''''''''''' CREATION DATE: 25-03-2002
''''''''''''
'''''''''''' REVISION HISTORY:
''''''''''''
'''''''''''' Date        Author Description
'''''''''''' ----------  ------ -----------------------------------------------
'''''''''''' 25-03-2002  CAC    Initial version
''''''''''''-------------------------------------------------------------------
'''''''''''Option Strict Off
'''''''''''Option Explicit On 
'''''''''''Imports System.Runtime.InteropServices

'''''''''''Public Module mdl_pipes

'''''''''''  <StructLayout(LayoutKind.Sequential)> _
'''''''''''     Public Class CLASS_DB_PIPE_MESSAGE
'''''''''''    ' Constants
'''''''''''    Private Const MAX_PIPE_NAME_LEN As Integer = 30
'''''''''''    Private Const MAX_PIPE_FIELD_LEN As Integer = 30

'''''''''''    Private control_block As Integer
'''''''''''    Private context As Integer
'''''''''''    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PIPE_NAME_LEN + 1 + 1)> _
'''''''''''    Private pipe_name As String
'''''''''''    Private num_fields As Integer
'''''''''''    Private field_count As Integer
'''''''''''    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PIPE_FIELD_LEN + 1 + 1)> _
'''''''''''    Private field0 As String
'''''''''''    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PIPE_FIELD_LEN + 1 + 1)> _
'''''''''''    Private field1 As String
'''''''''''    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PIPE_FIELD_LEN + 1 + 1)> _
'''''''''''    Private field2 As String
'''''''''''    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PIPE_FIELD_LEN + 1 + 1)> _
'''''''''''    Private field3 As String
'''''''''''    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PIPE_FIELD_LEN + 1 + 1)> _
'''''''''''    Private field4 As String
'''''''''''    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PIPE_FIELD_LEN + 1 + 1)> _
'''''''''''    Private field5 As String
'''''''''''    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PIPE_FIELD_LEN + 1 + 1)> _
'''''''''''    Private field6 As String
'''''''''''    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PIPE_FIELD_LEN + 1 + 1)> _
'''''''''''    Private field7 As String
'''''''''''    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PIPE_FIELD_LEN + 1 + 1)> _
'''''''''''    Private field8 As String
'''''''''''    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PIPE_FIELD_LEN + 1 + 1)> _
'''''''''''    Private field9 As String

'''''''''''    Private Declare Function Common_ReadDbPipe Lib "CommonMisc" (<[In](), [Out]()> ByVal PipeMessage As CLASS_DB_PIPE_MESSAGE) As Integer
'''''''''''    Private Declare Function Common_NameDbPipe Lib "CommonMisc" (ByVal BaseName As String, ByVal PipeName As String, ByVal Context As Integer) As Integer

'''''''''''    Public Sub New(ByVal PipeName As String, _
'''''''''''                   ByVal NumFields As Integer, _
'''''''''''                   ByVal Context As Integer, _
'''''''''''                   Optional ByVal UseSequence As Boolean = True)

'''''''''''      Me.control_block = Marshal.SizeOf(Me)
'''''''''''      Me.context = Context
'''''''''''      Me.num_fields = NumFields

'''''''''''      If UseSequence = True Then
'''''''''''        Me.pipe_name = FixedString(128)
'''''''''''        Call Common_NameDbPipe(PipeName, Me.pipe_name, Context)
'''''''''''        Call NullTrim(Me.pipe_name)
'''''''''''      Else
'''''''''''        Me.pipe_name = PipeName
'''''''''''      End If

'''''''''''    End Sub

'''''''''''    Public ReadOnly Property Name() As String
'''''''''''      Get
'''''''''''        Return pipe_name
'''''''''''      End Get
'''''''''''    End Property

'''''''''''    Public ReadOnly Property FieldCount() As Integer
'''''''''''      Get
'''''''''''        Return field_count
'''''''''''      End Get
'''''''''''    End Property

'''''''''''    Public ReadOnly Property Field(ByVal Index As Integer) As String
'''''''''''      Get
'''''''''''        Select Case Index
'''''''''''          Case 0
'''''''''''            If field_count > Index Then
'''''''''''              Return Me.field0
'''''''''''            End If
'''''''''''          Case 1
'''''''''''            If field_count > Index Then
'''''''''''              Return Me.field1
'''''''''''            End If
'''''''''''          Case 2
'''''''''''            If field_count > Index Then
'''''''''''              Return Me.field2
'''''''''''            End If
'''''''''''          Case 3
'''''''''''            If field_count > Index Then
'''''''''''              Return Me.field3
'''''''''''            End If
'''''''''''          Case 4
'''''''''''            If field_count > Index Then
'''''''''''              Return Me.field4
'''''''''''            End If
'''''''''''          Case 5
'''''''''''            If field_count > Index Then
'''''''''''              Return Me.field5
'''''''''''            End If
'''''''''''          Case 6
'''''''''''            If field_count > Index Then
'''''''''''              Return Me.field6
'''''''''''            End If
'''''''''''          Case 7
'''''''''''            If field_count > Index Then
'''''''''''              Return Me.field7
'''''''''''            End If
'''''''''''          Case 8
'''''''''''            If field_count > Index Then
'''''''''''              Return Me.field8
'''''''''''            End If
'''''''''''          Case 9
'''''''''''            If field_count > Index Then
'''''''''''              Return Me.field9
'''''''''''            End If
'''''''''''          Case Else
'''''''''''            '
'''''''''''        End Select

'''''''''''        Return Nothing

'''''''''''      End Get
'''''''''''    End Property

'''''''''''    Function Read() As Boolean
'''''''''''      If Common_ReadDbPipe(Me) = 0 Then
'''''''''''        If field_count = num_fields Then
'''''''''''          Return True
'''''''''''        End If
'''''''''''      End If
'''''''''''      Return False
'''''''''''    End Function

'''''''''''  End Class

'''''''''''End Module
