'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_logg.vb
' DESCRIPTION:   Handle writes in log file
' AUTHOR:        Carlos A. Costa
' CREATION DATE: 25-03-2002
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 25-03-2002  CAC    Initial version
'-------------------------------------------------------------------
Option Strict Off
Option Explicit On 

Public Module mdl_log

  ' Standard API for write in log file
  Private Declare Sub DllCommon_LoggerMsg Lib "CommonMisc" Alias "Common_LoggerMsg" (ByVal Index As Integer, _
                                                                                     ByVal Param1 As String , _
                                                                                     ByVal Param2 As String , _
                                                                                     ByVal Param3 As String , _                                                                                      
                                                                                     ByVal Param4 As String , _
                                                                                     ByVal Param5 As String , _
                                                                                     ByVal ExtraMsg As String)

  Public Enum ENUM_LOG_MSG

    ' %1 Nls Id
    LOG_NLS_NOT_FOUND = 1             'Texto NLS_ID=%1 no encontrado.

    ' %1 Error Code
    ' %2 Function call
    LOG_GENERIC_ERROR = 2             'Error=%1 llamando a %2.

    ' %1 Error Code
    ' %2 Expected size
    ' %3 Received size
    LOG_UNEXPECTED_ERROR = 3          'Error=%1. Esperado=%2. Recibido=%3.

        ' %1 SQL Error Code
    ' %2%3 Error description
    LOG_DATABASE_ERROR = 4            'Error=%1 llamando a BD. %2%3

    ' %1%2%3 Exception description
    LOG_EXCEPTION_ERROR = 5           'Excepcion:%1%2%3

    ' %1%2%3 Sentence
    LOG_SENTENCE = 6                  'Sentencia: %1%2%3

    ' %1 Variable
    ' %2 Value
    LOG_UNEXPECTED_VALUE_ERROR = 7    'Valor inexperado para: %1: %2.

    ' %1%2%3 User message
    LOG_USER_MESSAGE = 8           '%1%2%3

    ' %1 Error Code
    ' %2 Function call
    ' %3 Additional parameter
    LOG_SPECIFIC_ERROR = 9         'Error=%1 llamando a %2: %3

  End Enum

  ' PURPOSE: Parse and write log information
  '
  '  PARAMS:
  '     - INPUT:
  '           - Config: Registry configuration
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE if the connection is openned correctly
  '     - FALSE in another case
  Public Sub Common_LoggerMsg(ByVal LogMsgCode As ENUM_LOG_MSG, _
                              Optional ByVal Param1 As String = "", _
                              Optional ByVal Param2 As String = "", _
                              Optional ByVal Param3 As String = "", _
                              Optional ByVal Param4 As String = "", _
                              Optional ByVal Param5 As String = "", _
                              Optional ByVal ExtraMsg As String = "")



    Dim nls As New CLASS_MODULE_NLS(ENUM_NLS_MODULES.COMMON_MODULE_LOGGER)
    'XVV Variable not use Dim string_message As String
    Dim nls_param(5) As String

    Select Case LogMsgCode
      Case ENUM_LOG_MSG.LOG_EXCEPTION_ERROR
        Call NLS_SplitString(Param3, _
                             nls_param(0), _
                             nls_param(1), _
                             nls_param(2), _
                             nls_param(3), _
                             nls_param(4))

        Call DllCommon_LoggerMsg(nls.Id(LogMsgCode), _
                                  Param1, _
                                  Param2, _
                                  nls_param(0), _
                                  nls_param(1), _
                                  nls_param(2), _
                                  ExtraMsg)
      Case ENUM_LOG_MSG.LOG_USER_MESSAGE
        Call NLS_SplitString(ExtraMsg, _
                             nls_param(0), _
                             nls_param(1), _
                             nls_param(2), _
                             nls_param(3), _
                             nls_param(4))

        Call DllCommon_LoggerMsg(nls.Id(LogMsgCode), _
                                  Param1, _
                                  Param2, _
                                  nls_param(0), _
                                  nls_param(1), _
                                  nls_param(2), _
                                  nls_param(3))

      Case Else
        Call DllCommon_LoggerMsg(nls.Id(LogMsgCode), _
                         Param1, _
                         Param2, _
                         Param3, _
                         Param4, _
                         Param5, _
                         ExtraMsg)
    End Select

  End Sub
End Module
