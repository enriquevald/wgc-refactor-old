'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_node_definition.vb
' DESCRIPTION:   To read all node definitions
' AUTHOR:        
' CREATION DATE: 
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
'                    Initial version
'-------------------------------------------------------------------

Imports System.Runtime.InteropServices

Public Module mdl_node_definition

#Region " CommonData.dll "

#Region "Structure"

  <StructLayout(LayoutKind.Sequential)> _
Public Class CLASS_NODE_DEFINITION
    Public control_block As Integer
    Public internal_id As Integer
    Public node_type As Integer
    Public trx_type As Integer
    Public type As Integer
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_IPC_NODE_NAME + 1 + 1)> _
    Public name As String
    <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_IPC_DISPLAY_NAME + 1 + 3)> _
    Public display_name As String

    Public Sub New()
      control_block = Marshal.SizeOf(Me)

    End Sub

  End Class

#End Region

#Region "Prototypes "

  Declare Function Common_IpcDefNumNodes Lib "CommonData" () As Integer

  Declare Function Common_IpcDefGetNode Lib "CommonData" (ByVal NodeNumber As Integer, _
                                                          <[In](), [Out]()> _
                                                          ByVal NodeDefinition As CLASS_NODE_DEFINITION) As Boolean
#End Region

#End Region

#Region "Constants"

  Public Const MAX_IPC_DISPLAY_NAME As Integer = 40
  Public Const MAX_IPC_NODE_NAME As Integer = 30

#End Region

#Region "Public Functions "

  ' PURPOSE: Load node definition from CommonData
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - CLASS_NODE_DEFINITION(): array with node definitions
  '
  Public Function GetAllNodeDefinitions() As CLASS_NODE_DEFINITION()
    Dim idx As Integer
    Dim nodenumber As Integer
    Dim tmp_node_def As CLASS_NODE_DEFINITION
    Dim node_def() As CLASS_NODE_DEFINITION
    Dim exists_node As Boolean

    tmp_node_def = New CLASS_NODE_DEFINITION()

    ReDim node_def(0)
    nodenumber = Common_IpcDefNumNodes()
    For idx = 0 To nodenumber - 1
      exists_node = Common_IpcDefGetNode(idx, tmp_node_def)
      If exists_node = True Then
        ReDim Preserve node_def(tmp_node_def.internal_id + 1)
        node_def(tmp_node_def.internal_id) = New CLASS_NODE_DEFINITION()
        With node_def(tmp_node_def.internal_id)
          .control_block = tmp_node_def.control_block
          .internal_id = tmp_node_def.internal_id
          .type = tmp_node_def.type
          .trx_type = tmp_node_def.trx_type
          .name = tmp_node_def.name
          .display_name = tmp_node_def.display_name

        End With
      End If
    Next

    tmp_node_def = Nothing

    Return node_def

  End Function

#End Region

End Module