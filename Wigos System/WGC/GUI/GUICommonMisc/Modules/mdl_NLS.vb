'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_NLS.vb
' DESCRIPTION:   module to manage NLS strings
' AUTHOR:        
' CREATION DATE: 
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
'                    Initial version
'-------------------------------------------------------------------


Public Class CLASS_MODULE_NLS
  Private m_nls_module_id As ENUM_NLS_MODULES

  Public Sub New()
  End Sub

  Public Sub New(ByVal ModuleId As ENUM_NLS_MODULES)
    m_nls_module_id = ModuleId
  End Sub

  Public ReadOnly Property ModuleName() As String
    Get
      Return Format(m_nls_module_id, "G")
    End Get
  End Property

  Public Property ModuleId() As ENUM_NLS_MODULES
    Get
      Return m_nls_module_id
    End Get
    Set(ByVal Value As ENUM_NLS_MODULES)
      m_nls_module_id = Value
    End Set
  End Property


  Public ReadOnly Property Id(ByVal Index As Integer) As Integer
    Get
      Return CInt(m_nls_module_id) + Index
    End Get
  End Property

  Public Function GetString(ByVal Index As Integer, _
                            Optional ByVal Param1 As String = vbNullString, _
                            Optional ByVal Param2 As String = vbNullString, _
                            Optional ByVal Param3 As String = vbNullString, _
                            Optional ByVal Param4 As String = vbNullString, _
                            Optional ByVal Param5 As String = vbNullString) As String

    Return NLS_GetString(CInt(m_nls_module_id) + Index, _
                         Param1, _
                         Param2, _
                         Param3, _
                         Param4, _
                         Param5)

  End Function

  Public Function GetString(ByRef Exists As Boolean, _
                            ByVal Index As Integer, _
                            Optional ByVal Param1 As String = vbNullString, _
                            Optional ByVal Param2 As String = vbNullString, _
                            Optional ByVal Param3 As String = vbNullString, _
                            Optional ByVal Param4 As String = vbNullString, _
                            Optional ByVal Param5 As String = vbNullString) As String

    Return NLS_GetString(Exists, _
                         CInt(m_nls_module_id) + Index, _
                         Param1, _
                         Param2, _
                         Param3, _
                         Param4, _
                         Param5)

  End Function

  Public Function MsgBox(ByVal Index As Integer, _
                         Optional ByVal Type As ENUM_MB_TYPE = mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, _
                         Optional ByVal Buttons As ENUM_MB_BTN = mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, _
                         Optional ByVal Param1 As String = vbNullString, _
                         Optional ByVal Param2 As String = vbNullString, _
                         Optional ByVal Param3 As String = vbNullString, _
                         Optional ByVal Param4 As String = vbNullString, _
                         Optional ByVal Param5 As String = vbNullString) As ENUM_MB_RESULT
    Return NLS_MsgBox(CInt(m_nls_module_id) + Index, _
                      Type, _
                      Buttons, _
                      , _
                      Param1, _
                      Param2, _
                      Param3, _
                      Param4, _
                      Param5)
  End Function
End Class

Public Module mdl_NLS

#Region " NLS_API "
  ' Return Codes
  Private Const NLS_STATUS_OK As Short = 0
  Private Const NLS_STATUS_ERROR As Short = 1
  Private Const NLS_STATUS_DLL_NOT_LOADED As Short = 2
  Private Const NLS_STATUS_STRING_NOT_EXIST As Short = 3

  ' Max Len of the output string
  Private Const NLS_MAX_LEN As Short = 512

  ' Max len of the parameters
  Public Const NLS_MAX_LEN_PARAM As Short = 40

  Private NLS_Dictionary As Dictionary(Of Int32, String) = New Dictionary(Of Int32, String)


  Public Enum ENUM_NLS_MODULES
    COMMON_MODULE_ALARMS = 1000                  ' Alarms
    COMMON_MODULE_VERSION_CONTROL = 1500         ' Version Control
    COMMON_MODULE_GUI_ALARMS = 2000              ' GUI Alarms
    COMMON_MODULE_GUI_COMM = 2500                ' GUI Communications
    COMMON_MODULE_GUI_CONTROL = 3000             ' GUI Control
    COMMON_MODULE_GUI_CONF = 3500                ' GUI Configuration
    COMMON_MODULE_GUI_VERSION_CONTROL = 4000     ' GUI Version Control
    COMMON_MODULE_LOGGER = 4500                  ' Logger Messages
    COMMON_MODULE_GUI_CONTROLS = 5000            ' GUI Controls
    'COMMON_MODULE_GUI_SETUP = 5500               ' GUI Setup
    COMMON_MODULE_GUI_AUDITOR = 6000             ' GUI Auditor
    COMMON_MODULE_GUI_COMMONMISC = 6500          ' GUI CommonMisc
    'COMMON_CLIENT_START = 10000                  ' Client modules start their NLS id's from this 
    COMMON_TICKETS_MANAGER = 8000                ' GUI Tickets manager 
    COMMON_TRX_AUDIT = 9500                      ' Transaction auditory 
    COMMON_MODULE_GUI_STATISTICS = 11000         ' GUI Statistics, Old GUI Sales Control
    COMMON_MODULE_GUI_JACKPOT_MGR = 11500        ' GUI Jackpot Manager
    COMMON_MODULE_GUI_SYSTEM_MONITOR = 12000     ' GUI System Monitor
    COMMON_MODULE_GUI_INVOICE_SYSTEM = 13000     ' GUI Invoice System
    COMMON_MODULE_GUI_SW_DOWNLOAD = 13500        ' GUI Software Download
    COMMON_MODULE_GUI_LAUNCH_BAR = 14000         ' GUI Launch Bar 
    COMMON_MODULE_GUI_DATE_CODE = 14500          ' GUI DATE CODE
    COMMON_MODULE_LKAS_FUNCTIONS = 15000         ' LKAS Functions
    COMMON_MODULE_GUI_CLASS_II = 15500           ' GUI Class II
    COMMON_MODULE_GUI_PLAYER_TRACKING = 16000    ' Player Tracking
    COMMON_MODULE_REPORT = 50000                 ' Reports
  End Enum

  Public Enum ENUM_NLS_LANGUAGE
    NLS_LANGUAGE_ENGLISH = 9
    NLS_LANGUAGE_SPANISH = 10
    NLS_LANGUAGE_FRENCH = 12
  End Enum

  Public Const NLS_MAX_PARAMS As Integer = 5

  Private Declare Function NLSSetLanguage Lib "NLS_API" Alias "NLS_SetLanguage" (ByVal LanguageCode As Short) As Short

  Private Declare Function NLSGetString Lib "NLS_API" Alias "NLS_GetString" (ByVal StringId As Integer, _
                                                        ByVal ResultString As String, _
                                                        ByVal Param1 As String, _
                                                        ByVal Param2 As String, _
                                                        ByVal Param3 As String, _
                                                        ByVal Param4 As String, _
                                                        ByVal Param5 As String) As Short

  Private Declare Function NLSSplitString Lib "NLS_API" Alias "NLS_SplitString" (ByVal InputString As String, _
                                                                                 ByVal Param1 As String, _
                                                                                 ByVal Param2 As String, _
                                                                                 ByVal Param3 As String, _
                                                                                 ByVal Param4 As String, _
                                                                                 ByVal Param5 As String) As Short

#End Region

#Region " Const Format Strings "
  Private Const FORMAT_STRING_NOT_FOUND As String = "\#000\, string not found."
  Private Const FORMAT_STRING_NO_DLL As String = "\#000"
#End Region

  Public Enum ENUM_MB_BTN
    MB_BTN_OK = MsgBoxStyle.OKOnly
    MB_BTN_YES_NO = MsgBoxStyle.YesNo
    MB_BTN_YES_NO_CANCEL = MsgBoxStyle.YesNoCancel
  End Enum

  Public Enum ENUM_MB_DEF_BTN
    MB_DEF_BTN_1 = MsgBoxStyle.DefaultButton1
    MB_DEF_BTN_2 = MsgBoxStyle.DefaultButton2
    MB_DEF_BTN_3 = MsgBoxStyle.DefaultButton3
  End Enum

  Public Enum ENUM_MB_TYPE
    MB_TYPE_ERROR = MsgBoxStyle.Critical
    MB_TYPE_WARNING = MsgBoxStyle.Exclamation
    MB_TYPE_INFO = MsgBoxStyle.Information
  End Enum

  Public Enum ENUM_MB_RESULT
    MB_RESULT_OK = MsgBoxResult.OK
    MB_RESULT_YES = MsgBoxResult.Yes
    MB_RESULT_NO = MsgBoxResult.No
    MB_RESULT_CANCEL = MsgBoxResult.Cancel
  End Enum

  Public Function NLS_MsgBox(ByVal NlsId As Integer, _
                             Optional ByVal Type As ENUM_MB_TYPE = ENUM_MB_TYPE.MB_TYPE_INFO, _
                             Optional ByVal Buttons As ENUM_MB_BTN = ENUM_MB_BTN.MB_BTN_OK, _
                             Optional ByVal DefaultButton As ENUM_MB_DEF_BTN = ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                             Optional ByVal Param1 As String = vbNullString, _
                             Optional ByVal Param2 As String = vbNullString, _
                             Optional ByVal Param3 As String = vbNullString, _
                             Optional ByVal Param4 As String = vbNullString, _
                             Optional ByVal Param5 As String = vbNullString) As ENUM_MB_RESULT
    Dim rc As MsgBoxResult
    Dim prompt As String
    Dim nls_id As Integer

    prompt = NLS_GetString(NlsId, Param1, Param2, Param3, Param4, Param5)
    nls_id = NLS_GetId(272, ENUM_NLS_MODULES.COMMON_MODULE_GUI_CONTROLS)
    rc = MsgBox("[" & NLS_GetString(nls_id) & " - " & Format(NlsId, "00000") & "]  " & prompt, _
                Type + Buttons + DefaultButton, _
                NLS_GetString(nls_id) & " - " & GLB_ApplicationName)

    Return rc

  End Function

  Public Function NLS_MountedMsgBox(ByVal NlsId As Integer, _
                                    ByVal Param1 As String, _
                                    Optional ByVal Type As ENUM_MB_TYPE = ENUM_MB_TYPE.MB_TYPE_INFO, _
                                    Optional ByVal Buttons As ENUM_MB_BTN = ENUM_MB_BTN.MB_BTN_OK, _
                                    Optional ByVal DefaultButton As ENUM_MB_DEF_BTN = ENUM_MB_DEF_BTN.MB_DEF_BTN_1) As ENUM_MB_RESULT
    Dim rc As MsgBoxResult
    Dim nls_id As Integer

    nls_id = NLS_GetId(272, ENUM_NLS_MODULES.COMMON_MODULE_GUI_CONTROLS)
    rc = MsgBox("[" & NLS_GetString(nls_id) & " - " & Format(NlsId, "00000") & "]  " & Param1, _
                Type + Buttons + DefaultButton, _
                NLS_GetString(nls_id) & " - " & GLB_ApplicationName)

    Return rc

  End Function

  Public Function NLS_SetLanguage(ByVal LanguageCode As ENUM_NLS_LANGUAGE) As Boolean
    Dim rc As Short

    On Error GoTo ErrorHandler

    rc = NLSSetLanguage(CShort(LanguageCode))
    Select Case rc
      Case NLS_STATUS_OK

        ' Clear the dictionary we just changed the language
        NLS_Dictionary.Clear()

        Return True
      Case Else
    End Select

    Return False

ErrorHandler:  ' DLL not found
    Return False

  End Function



  Public Function NLS_GetString(ByVal NlsId As Integer, _
                                Optional ByVal Param1 As String = vbNullString, _
                                Optional ByVal Param2 As String = vbNullString, _
                                Optional ByVal Param3 As String = vbNullString, _
                                Optional ByVal Param4 As String = vbNullString, _
                                Optional ByVal Param5 As String = vbNullString) As String
    Dim _exists As Boolean

    Return NLS_GetString(_exists, NlsId, Param1, Param2, Param3, Param4, Param5)

  End Function

  Public Function NLS_GetString(ByRef Exists As Boolean, _
                                ByVal NlsId As Integer, _
                                Optional ByVal Param1 As String = vbNullString, _
                                Optional ByVal Param2 As String = vbNullString, _
                                Optional ByVal Param3 As String = vbNullString, _
                                Optional ByVal Param4 As String = vbNullString, _
                                Optional ByVal Param5 As String = vbNullString) As String
    Dim _tmp_str As String

    On Error GoTo ErrorHandler

    Exists = False

    If Not NLS_Dictionary.ContainsKey(NlsId) Then
      _tmp_str = FixedString(NLS_MAX_LEN)
      If NLSGetString(NlsId, _tmp_str, "{@p1}", "{@p2}", "{@p3}", "{@p4}", "{@p5}") = NLS_STATUS_OK Then
        ' Found:
        '   Remove null chars
        '   Add to the dictionary
        _tmp_str = Replace(_tmp_str, vbNullChar, "")
        _tmp_str = Replace(_tmp_str, "�", "")
        NLS_Dictionary.Add(NlsId, _tmp_str)
        Exists = True
      End If
    Else
      _tmp_str = NLS_Dictionary(NlsId)
      Exists = True
    End If

    If Exists Then
      _tmp_str = _tmp_str.Replace("{@p1}", Param1)
      _tmp_str = _tmp_str.Replace("{@p2}", Param2)
      _tmp_str = _tmp_str.Replace("{@p3}", Param3)
      _tmp_str = _tmp_str.Replace("{@p4}", Param4)
      _tmp_str = _tmp_str.Replace("{@p5}", Param5)

      Return _tmp_str
    End If

    Return Format(NlsId, FORMAT_STRING_NOT_FOUND)

ErrorHandler:
    Return Format(NlsId, FORMAT_STRING_NO_DLL)

  End Function

  Public Function NLS_GetId(ByVal Index As Integer, ByVal ModuleId As ENUM_NLS_MODULES) As Integer
    Return CInt(ModuleId) + Index
  End Function

  Public Function NLS_SplitString(ByVal InputString As String, _
                                  ByRef Param1 As String, _
                                  ByRef Param2 As String, _
                                  ByRef Param3 As String, _
                                  ByRef Param4 As String, _
                                  ByRef Param5 As String) As String
    Dim rc As Short

    Param1 = FixedString(NLS_MAX_LEN)
    Param2 = FixedString(NLS_MAX_LEN)
    Param3 = FixedString(NLS_MAX_LEN)
    Param4 = FixedString(NLS_MAX_LEN)
    Param5 = FixedString(NLS_MAX_LEN)

    rc = NLSSplitString(InputString, _
                        Param1, _
                        Param2, _
                        Param3, _
                        Param4, _
                        Param5)

    Return rc
  End Function

End Module

