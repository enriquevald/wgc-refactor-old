'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : mdl_misc.vb
'
' DESCRIPTION : module miscellaneous
'
'      AUTHOR :
'
' CREATION DATE :
'
' REVISION HISTORY :
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
'                    Initial version
' 02-APR-2012 JCM    Added method CutString.
' 08-JAN-2013 HBB    Moved method CheckEmailAddress to WSI.Common.Mailing
' 30-MAY-2013 RRR    Added method DataTable_FilterValue
'-------------------------------------------------------------------
Imports System.Text
Imports System.Data.SqlClient

Public Module Misc

#Region " Constants "

  Private Const FORMAT_HIERARCHY_INTERNAL_ID As String = "#0"
  Private Const FORMAT_HIERARCHY_EXTERN_ID As String = "#0"
  Private Const MAX_HIERARCHY_NAME As Integer = 20
  Private Const IP_PARTS As Integer = 4

  Public Enum ENUM_WRONG_IP_ADDRESS
    NONE = 0
    BAD_FORMAT = 1
    NO_VALID = 2
  End Enum

#End Region

#Region " Miscellaneous Functions "

  Public Function NullTrim(ByVal str As String) As String

    Return Trim(Replace(str, vbNullChar, ""))

  End Function

  Public Function FixedString(ByVal Len As Integer) As String

    Return New String(Chr(0), Len)

  End Function

  ' PURPOSE: Aux function, only cuts string if is bigger than maxlength, else returns entire string
  '
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS :

  Public Function CutString(ByVal Word As String, ByVal MaxLength As Integer) As String
    If String.IsNullOrEmpty(Word) Then
      Return String.Empty
    End If

    If Word.Length < MaxLength Then

      Return Word
    Else

      Return Word.Substring(0, MaxLength)
    End If
  End Function 'CutString

  ' PURPOSE: Format the hierarchy for show 
  '
  '  PARAMS:
  '     - INPUT:
  '           - InternalId: internal id of the hierarchy
  '           - ExternId: extern id of the hierarchy
  '           - Name: name of the hierarchy
  '           - PutInternalIds: if TRUE return internal id's in string
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - string formatted
  '
  Public Function GetHierarchyString(ByVal InternalId As Integer, _
                                      ByVal ExternId As Integer, _
                                      ByVal Name As String, _
                                      ByVal PutInternalIds As Boolean) As String
    Dim name_len As Integer

    If PutInternalIds Then
      name_len = Name.Length
      If name_len > MAX_HIERARCHY_NAME Then
        name_len = MAX_HIERARCHY_NAME
      End If
      GetHierarchyString = "( ID = " & Format(InternalId, FORMAT_HIERARCHY_INTERNAL_ID) & ") " _
                         & Format(ExternId, FORMAT_HIERARCHY_EXTERN_ID) _
                         & " - " & Name.Substring(0, MAX_HIERARCHY_NAME)
    Else
      name_len = Name.Length
      If name_len > MAX_HIERARCHY_NAME Then
        name_len = MAX_HIERARCHY_NAME
      End If
      GetHierarchyString = Format(ExternId, FORMAT_HIERARCHY_EXTERN_ID) _
                         & " - " & Name.Substring(0, name_len)
    End If

  End Function

  ' PURPOSE: Format the string for SQL filter
  '          Replace: ' -> '' 
  '                   \ -> \\
  '                   % -> \% 
  '                   _ -> \_
  '
  '  PARAMS:
  '     - INPUT:
  '           - FieldName: field name
  '           - FieldPattern: pattern string to format
  '           - CaseSensitive: upper case
  '           - MatchExact: insert a wildcard '%' to end of string
  '           - LeftWildcard: insert a wildcard '%' to start of string
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - string formatted
  Public Function GUI_FilterField(ByVal FieldName As String, ByVal FieldPattern As String, _
                                  Optional ByVal CaseSensitive As Boolean = False, _
                                  Optional ByVal MatchExact As Boolean = False, _
                                  Optional ByVal LeftWildcard As Boolean = False) As String
    Const FORMAT_ESCAPE_SQL As String = "\"
    Const FORMAT_QUOTED_SQL As String = "''"
    Const FORMAT_QUOTED As String = "'"
    Const FORMAT_WILDCARD As String = "%"
    Const FORMAT_UNDERLINE As String = "_"
    Dim str_name As String
    Dim str_pattern As String

    str_pattern = Replace(FieldPattern, FORMAT_QUOTED, FORMAT_QUOTED_SQL)
    str_pattern = Replace(str_pattern, FORMAT_ESCAPE_SQL, FORMAT_ESCAPE_SQL & FORMAT_ESCAPE_SQL)
    str_pattern = Replace(str_pattern, FORMAT_WILDCARD, FORMAT_ESCAPE_SQL & FORMAT_WILDCARD)
    str_pattern = Replace(str_pattern, FORMAT_UNDERLINE, FORMAT_ESCAPE_SQL & FORMAT_UNDERLINE)

    If CaseSensitive = False Then
      str_name = " UPPER (" & FieldName & ")"
      str_pattern = UCase(str_pattern)
    Else
      str_name = " " & FieldName
    End If

    If MatchExact = False Then
      If LeftWildcard Then
        str_pattern = FORMAT_WILDCARD & str_pattern & FORMAT_WILDCARD
      Else
        str_pattern = str_pattern & FORMAT_WILDCARD
      End If
    End If

    str_pattern = str_name & " LIKE " & FORMAT_QUOTED & str_pattern & FORMAT_QUOTED & _
                  " ESCAPE " & FORMAT_QUOTED & FORMAT_ESCAPE_SQL & FORMAT_QUOTED
    Return str_pattern

  End Function

  'PURPOSE: Obtains the max number of price levels of a game
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - max price level
  Public Function GetMaxPriceLevels() As Integer
    Return 5
  End Function

  ' GZ 22-APR-2004
  ' Corrected and Simplified the GUI_CheckIpAddress function
  ' PURPOSE: Check if the ip is valid
  '
  '  PARAMS:
  '     - INPUT:
  '           - ip, Format: a.b.c.d
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - false if:
  '         - a, b, c, d: not in [0,255]
  '     - true otherwise
  Public Function GUI_CheckIpAddress(ByVal IpStringValue As String, ByRef reason As ENUM_WRONG_IP_ADDRESS) As Boolean
    Dim dot_position As Integer
    Dim ip_part As String
    Dim ip_part_number As Integer = IP_PARTS
    reason = ENUM_WRONG_IP_ADDRESS.BAD_FORMAT

    While ip_part_number
      dot_position = IpStringValue.IndexOf(".")

      If ip_part_number > 1 Then
        If dot_position > 3 Or dot_position < 1 Then
          Return False
        End If
        ip_part = IpStringValue.Substring(0, dot_position)
      Else
        If dot_position >= 0 Then
          Return False
        End If
        ip_part = IpStringValue
      End If

      If IsNumeric(ip_part) Then
        If (CInt(ip_part) < 0 Or _
            CInt(ip_part) > 255) Then

          Return False
        End If
      Else
        Return False
      End If

      IpStringValue = IpStringValue.Substring(dot_position + 1)
      ip_part_number = ip_part_number - 1

    End While



    Return True

  End Function


  ' PURPOSE: Format the string for DataTable's Select method filter
  '          Replace: ' -> '' 
  '                   % -> [%]
  '                   * -> [*] 
  '                   [ -> [[]
  '                   [ -> []]
  '
  '  PARAMS:
  '     - INPUT:
  '           - StringValue: string to format
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - string formatted
  Public Function DataTable_FilterValue(ByVal StringValue As String) As String
    Dim _str_builder As StringBuilder
    Dim _idx_char As Integer
    Dim _chr As Char

    _str_builder = New StringBuilder(StringValue.Length)

    For _idx_char = 0 To StringValue.Length - 1
      _chr = StringValue(_idx_char)
      Select Case _chr
        Case "]", "[", "%", "*"
          _str_builder.Append("[").Append(_chr).Append("]")
        Case Else
          _str_builder.Append(_chr)
      End Select
    Next

    Return _str_builder.ToString().Replace("'", "''")

  End Function

  '' PURPOSE: Check if the ip is valid
  ''
  ''  PARAMS:
  ''     - INPUT:
  ''           - ip, Format: a.b.c.d
  ''     - OUTPUT:
  ''           - none
  ''
  '' RETURNS:
  ''     - false if:
  ''         - a: not in [1,223] or equals to 127
  ''         - b, c, d: not in [1,254]
  ''     - true otherwise
  'Public Function GUI_CheckIpAddress(ByVal IpStringValue As String, ByRef reason As ENUM_WRONG_IP_ADDRESS) As Boolean
  '  Dim tmp_char As Char
  '  Dim dot_position As Integer
  '  Dim ip_a As String
  '  Dim ip_b As String
  '  Dim ip_c As String
  '  Dim ip_d As String
  '  Dim number As String = ""

  '  reason = ENUM_WRONG_IP_ADDRESS.BAD_FORMAT

  '  dot_position = IpStringValue.IndexOf(".")
  '  If dot_position > 3 Or dot_position < 1 Then
  '    Return False
  '  Else
  '    ip_a = IpStringValue.Substring(0, dot_position)
  '    If IsNumeric(ip_a) Then
  '      If (CInt(ip_a) < 1 Or _
  '          CInt(ip_a) > 223 Or _
  '          CInt(ip_a) = 127) Then
  '        reason = ENUM_WRONG_IP_ADDRESS.NO_VALID

  '        Return False
  '      End If
  '    Else
  '      Return False
  '    End If

  '  End If

  '  IpStringValue = IpStringValue.Substring(dot_position + 1)
  '  dot_position = IpStringValue.IndexOf(".")
  '  If dot_position > 3 Or dot_position < 1 Then
  '    Return False
  '  Else
  '    ip_b = IpStringValue.Substring(0, dot_position)
  '    If IsNumeric(ip_b) Then
  '      If (CInt(ip_b) < 1 Or _
  '          CInt(ip_b) > 255) Then
  '        Return False

  '      End If
  '    Else
  '      Return False
  '    End If

  '  End If

  '  IpStringValue = IpStringValue.Substring(dot_position + 1)
  '  dot_position = IpStringValue.IndexOf(".")
  '  If dot_position > 3 Or dot_position < 1 Then
  '    Return False
  '  Else
  '    ip_c = IpStringValue.Substring(0, dot_position)
  '    If IsNumeric(ip_c) Then
  '      If (CInt(ip_c) < 1 Or _
  '          CInt(ip_c) > 255) Then
  '        Return False

  '      End If
  '    Else
  '      Return False
  '    End If


  '  End If

  '  IpStringValue = IpStringValue.Substring(dot_position + 1)
  '  dot_position = IpStringValue.IndexOf(".")
  '  If dot_position >= 0 Then
  '    Return False
  '  End If

  '  ip_d = IpStringValue
  '  If IsNumeric(ip_d) Then
  '    If (CInt(ip_d) < 1 Or _
  '        CInt(ip_d) > 255) Then
  '      Return False

  '    End If
  '  Else
  '    Return False
  '  End If

  '  Return True

  'End Function

 

#End Region

#Region " Return Codes "

  Public Enum ENUM_COMMON_RC
    COMMON_OK = 0
    COMMON_ERROR = 1
    COMMON_ERROR_CONTROL_BLOCK = 2
    COMMON_ERROR_DB_NOT_CONNECTED = 3
    COMMON_ERROR_DB = 4
    COMMON_ERROR_SAFEARRAY_ACCESS = 5
    COMMON_ERROR_PARAMETER = 6
    COMMON_ERROR_NOT_FOUND = 7
    COMMON_ERROR_REGISTRY = 8
    COMMON_ERROR_BAD_FORMAT_FILE = 9
    COMMON_ERROR_CONTEXT = 10
    COMMON_ERROR_PASSWORD = 11
    COMMON_ERROR_DUPLICATE_KEY = 12
    COMMON_ERROR_ARRAY_OVERFLOW = 13
    COMMON_ERROR_DUPLICATED_PASSWORD = 14
    COMMON_ERROR_ALREADY_EXISTS = 15
    COMMON_ERROR_DB_FATAL = 16
    COMMON_ERROR_NOT_SUPORTED = 17
    COMMON_ERROR_NOT_EXIST = 18
  End Enum

#End Region

End Module
