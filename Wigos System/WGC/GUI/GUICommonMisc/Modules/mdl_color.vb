'-------------------------------------------------------------------
' Copyright � 2002 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   mdl_color.vb  
'
' DESCRIPTION:   Module to manage colors
'
' AUTHOR:        Toni Jord�
'
' CREATION DATE: 02-APR-2002
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 02-APR-2002  TJ     Initial version
' 28-MAR-2013  JAB    Added new function "GetColorId" that return a ENUM_GUI_COLOR.
' 29-MAR-2013  JAB    Added new ENUM_GUI_COLOR.GUI_COLOR_GREY_03.
'--------------------------------------------------------------------

Imports System.Drawing

Public Module ControlColor


  Public Enum ENUM_GUI_COLOR
    GUI_COLOR_BLACK_00
    GUI_COLOR_WHITE_00
    GUI_COLOR_RED_00
    GUI_COLOR_RED_01
    GUI_COLOR_RED_02
    GUI_COLOR_RED_03
    GUI_COLOR_YELLOW_00
    GUI_COLOR_YELLOW_01
    GUI_COLOR_OCHRE_00
    GUI_COLOR_BLUE_00
    GUI_COLOR_BLUE_01
    GUI_COLOR_BLUE_02
    GUI_COLOR_BLUE_03
    GUI_COLOR_BROWN_00
    GUI_COLOR_GREEN_00
    GUI_COLOR_GREEN_01
    GUI_COLOR_GREEN_02
    GUI_COLOR_GREY_00
    GUI_COLOR_ORANGE_00
    GUI_COLOR_ORANGE_01
    GUI_COLOR_GREY_01
    GUI_COLOR_GREY_02
    GUI_COLOR_GREY_03
  End Enum

  ' PURPOSE: Get colors by LKC-GUI system
  '
  '  PARAMS:
  '     - INPUT:
  '           - ColorId
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - Color
  '
  Public Function GetColor(ByVal ColorId As ENUM_GUI_COLOR) As Color

    Select Case (ColorId)

      Case ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
        Return Color.FromArgb(255, 255, 255)

      Case ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
        Return Color.FromArgb(0, 0, 0)

      Case ENUM_GUI_COLOR.GUI_COLOR_RED_00
        Return Color.FromArgb(204, 0, 0)

      Case ENUM_GUI_COLOR.GUI_COLOR_RED_01
        Return Color.FromArgb(255, 77, 77)

      Case ENUM_GUI_COLOR.GUI_COLOR_RED_02
        Return Color.FromArgb(192, 0, 0)

      Case ENUM_GUI_COLOR.GUI_COLOR_RED_03
        Return Color.FromArgb(255, 192, 192)

      Case ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
        Return Color.FromArgb(255, 235, 94)

      Case ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01
        Return Color.FromArgb(255, 255, 208)

      Case ENUM_GUI_COLOR.GUI_COLOR_OCHRE_00
        Return Color.FromArgb(255, 224, 192)

      Case ENUM_GUI_COLOR.GUI_COLOR_BLUE_00
        Return Color.FromArgb(45, 129, 255)

      Case ENUM_GUI_COLOR.GUI_COLOR_BLUE_01
        Return Color.FromArgb(130, 180, 255)

      Case ENUM_GUI_COLOR.GUI_COLOR_BLUE_02
        Return Color.FromArgb(192, 255, 255)

      Case ENUM_GUI_COLOR.GUI_COLOR_BLUE_03
        Return Color.FromArgb(55, 60, 255)

      Case ENUM_GUI_COLOR.GUI_COLOR_BROWN_00
        Return Color.FromArgb(255, 149, 43)

      Case ENUM_GUI_COLOR.GUI_COLOR_GREEN_00
        Return Color.FromArgb(95, 186, 160)

      Case ENUM_GUI_COLOR.GUI_COLOR_GREEN_01
        Return Color.FromArgb(190, 230, 220)

      Case ENUM_GUI_COLOR.GUI_COLOR_GREEN_02
        Return Color.FromArgb(0, 128, 0)

      Case ENUM_GUI_COLOR.GUI_COLOR_GREY_00
        Return Color.FromArgb(170, 170, 170)

      Case ENUM_GUI_COLOR.GUI_COLOR_ORANGE_00
        Return Color.FromArgb(255, 128, 0)

      Case ENUM_GUI_COLOR.GUI_COLOR_ORANGE_01
        Return Color.FromArgb(239, 167, 58)

      Case ENUM_GUI_COLOR.GUI_COLOR_GREY_01
        Return Color.FromArgb(225, 225, 225)

      Case ENUM_GUI_COLOR.GUI_COLOR_GREY_02
        Return Color.FromArgb(200, 200, 200)

      Case ENUM_GUI_COLOR.GUI_COLOR_GREY_03
        Return Color.FromArgb(192, 192, 192)

      Case Else
        'Error
        Return Color.FromArgb(0, 0, 0)

    End Select

  End Function

  ' PURPOSE: Get colorsId by LKC-GUI system
  '
  '  PARAMS:
  '     - INPUT:
  '           - Color: System.Drawing.color
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ColorId: ENUM_GUI_COLOR
  '
  Public Function GetColorId(ByVal Color As System.Drawing.Color) As ENUM_GUI_COLOR

    Select Case Color.ToArgb

      Case Color.FromArgb(255, 255, 255).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_WHITE_00

      Case Color.FromArgb(0, 0, 0).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_BLACK_00

      Case Color.FromArgb(204, 0, 0).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_RED_00

      Case Color.FromArgb(255, 77, 77).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_RED_01

      Case Color.FromArgb(192, 0, 0).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_RED_02

      Case Color.FromArgb(255, 192, 192).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_RED_03

      Case Color.FromArgb(255, 235, 94).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00

      Case Color.FromArgb(255, 255, 208).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01

      Case Color.FromArgb(255, 224, 192).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_OCHRE_00

      Case Color.FromArgb(45, 129, 255).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_BLUE_00

      Case Color.FromArgb(130, 180, 255).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_BLUE_01

      Case Color.FromArgb(192, 255, 255).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_BLUE_02

      Case Color.FromArgb(55, 60, 255).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_BLUE_03

      Case Color.FromArgb(255, 149, 43).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_BROWN_00

      Case Color.FromArgb(95, 186, 160).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_GREEN_00

      Case Color.FromArgb(190, 230, 220).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_GREEN_01

      Case Color.FromArgb(0, 128, 0).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_GREEN_02

      Case Color.FromArgb(255, 128, 0).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_ORANGE_00

      Case Color.FromArgb(239, 167, 58).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_ORANGE_01

      Case Color.FromArgb(170, 170, 170).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_GREY_00

      Case Color.FromArgb(225, 225, 225).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_GREY_01

      Case Color.FromArgb(200, 200, 200).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_GREY_02

      Case Color.FromArgb(192, 192, 192).ToArgb
        Return ENUM_GUI_COLOR.GUI_COLOR_GREY_03

      Case Else
        'Error
        Return ENUM_GUI_COLOR.GUI_COLOR_BLACK_00

    End Select

  End Function

End Module
