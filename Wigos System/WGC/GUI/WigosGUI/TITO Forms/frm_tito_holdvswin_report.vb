'-------------------------------------------------------------------
' Copyright � 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_flags_sel.vb
' DESCRIPTION:   Hold vs Win report
' AUTHOR:        Marcos Piedra
' CREATION DATE: 16-MAR-2016
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 24-MAR-2016  XCD    Initial version
' 24-AUG-2016  RGR    Bug 14549: HOLD TITO VS WIN - add excel filter
' 12-MAR-2018  AGS    Bug 31896: WIGOS-8815 The exported excel from the report TITO hold vs Win is not displaying all the terminals displayed in the report in GUI
' 04-ABR-2018  JGC    Bug 32163:[WIGOS-9683]: In mico the aft aren't considered for the report 
'--------------------------------------------------------------------

Option Strict Off
Option Explicit On

#Region " Imports "

Imports System.Data.OleDb
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports System.Data.SqlClient
Imports WSI.Common

#End Region

Public Class frm_tito_holdvswin_report
  Inherits GUI_Controls.frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub
  Friend WithEvents dt_datetime As GUI_Controls.uc_date_picker
  Friend WithEvents chk_group_by_prov As System.Windows.Forms.CheckBox

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.dt_datetime = New GUI_Controls.uc_date_picker()
    Me.chk_group_by_prov = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_group_by_prov)
    Me.panel_filter.Controls.Add(Me.dt_datetime)
    Me.panel_filter.Size = New System.Drawing.Size(1226, 105)
    Me.panel_filter.Controls.SetChildIndex(Me.dt_datetime, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_group_by_prov, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 109)
    Me.panel_data.Size = New System.Drawing.Size(1226, 599)
    Me.panel_data.TabIndex = 3
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1220, 17)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1220, 4)
    '
    'dt_datetime
    '
    Me.dt_datetime.Checked = False
    Me.dt_datetime.IsReadOnly = False
    Me.dt_datetime.Location = New System.Drawing.Point(17, 16)
    Me.dt_datetime.Name = "dt_datetime"
    Me.dt_datetime.ShowCheckBox = False
    Me.dt_datetime.ShowUpDown = False
    Me.dt_datetime.Size = New System.Drawing.Size(200, 24)
    Me.dt_datetime.SufixText = "Sufix Text"
    Me.dt_datetime.SufixTextVisible = True
    Me.dt_datetime.TabIndex = 14
    Me.dt_datetime.Value = New Date(2016, 3, 24, 0, 0, 0, 0)
    '
    'chk_group_by_prov
    '
    Me.chk_group_by_prov.AutoSize = True
    Me.chk_group_by_prov.Location = New System.Drawing.Point(235, 20)
    Me.chk_group_by_prov.Name = "chk_group_by_prov"
    Me.chk_group_by_prov.Size = New System.Drawing.Size(109, 17)
    Me.chk_group_by_prov.TabIndex = 15
    Me.chk_group_by_prov.Text = "xGroupByProv"
    Me.chk_group_by_prov.UseVisualStyleBackColor = True
    '
    'frm_tito_holdvswin_report
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1234, 712)
    Me.Name = "frm_tito_holdvswin_report"
    Me.ShowInTaskbar = False
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  ' Grid Rows
  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_TERMINAL_NAME As Integer = 1
  Private Const GRID_COLUMN_MANUFACTURER As Integer = 2
  Private Const GRID_COLUMN_COININ As Integer = 3
  Private Const GRID_COLUMN_TOTALWON As Integer = 4
  Private Const GRID_COLUMN_HOLD As Integer = 5
  Private Const GRID_COLUMN_GAMES As Integer = 6
  Private Const GRID_COLUMN_WON As Integer = 7
  Private Const GRID_COLUMN_BETAVERAGE As Integer = 8
  Private Const GRID_COLUMN_BILLIN As Integer = 9
  Private Const GRID_COLUMN_CASHABLETICKETIN As Integer = 10
  Private Const GRID_COLUMN_RESTRICTEDTICKETIN As Integer = 11
  Private Const GRID_COLUMN_NONRESTRICTEDTICKETIN As Integer = 12
  Private Const GRID_COLUMN_CASHABLETICKETOUT As Integer = 13
  Private Const GRID_COLUMN_RESTRICTEDTICKETOUT As Integer = 14
  Private Const GRID_COLUMN_CASHABLECARDSIN As Integer = 15
  Private Const GRID_COLUMN_RESTRICTEDCARDSIN As Integer = 16
  Private Const GRID_COLUMN_NONRESTRICTEDCARDSIN As Integer = 17
  Private Const GRID_COLUMN_CASHABLECARDSOUT As Integer = 18
  Private Const GRID_COLUMN_RESTRICTEDCARDSOUT As Integer = 19
  Private Const GRID_COLUMN_NONRESTRICTEDCARDSOUT As Integer = 20
  Private Const GRID_COLUMN_JACKPOTS As Integer = 21
  Private Const GRID_COLUMN_HANDPAIDCANCELEDCREDITS As Integer = 22
  Private Const GRID_COLUMN_TOTALIN As Integer = 23
  Private Const GRID_COLUMN_TOTALOUT As Integer = 24
  Private Const GRID_COLUMN_TOTALWIN As Integer = 25
  Private Const GRID_COLUMN_TOTALREIN As Integer = 26
  Private Const GRID_COLUMN_TOTALREOUT As Integer = 27
  Private Const GRID_COLUMN_TOTALREWIN As Integer = 28
  Private Const GRID_COLUMN_HOLDVSWIN As Integer = 29

  'Grid width
  Private Const GRID_COLUMN_INDEX_WIDTH As Integer = 200
  Private Const GRID_COLUMN_TERMINAL_NAME_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_MANUFACTURER_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_COININ_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_TOTALWON_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_HOLD_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_GAMES_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_WON_WIDTH As Integer = 0
  Private Const GRID_COLUMN_BETAVERAGE_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_BILLIN_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_CASHABLETICKETIN_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_RESTRICTEDTICKETIN_WIDTH As Integer = 2300
  Private Const GRID_COLUMN_NONRESTRICTEDTICKETIN_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_CASHABLETICKETOUT_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_RESTRICTEDTICKETOUT_WIDTH As Integer = 2300
  Private Const GRID_COLUMN_CASHABLECARDSIN_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_RESTRICTEDCARDSIN_WIDTH As Integer = 2300
  Private Const GRID_COLUMN_NONRESTRICTEDCARDSIN_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_CASHABLECARDSOUT_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_RESTRICTEDCARDSOUT_WIDTH As Integer = 2300
  Private Const GRID_COLUMN_NONRESTRICTEDCARDSOUT_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_JACKPOTS_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_HANDPAIDCANCELEDCREDITS_WIDTH As Integer = 2800
  Private Const GRID_COLUMN_TOTALIN_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_TOTALOUT_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_TOTALWIN_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_TOTALREIN_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_TOTALREOUT_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_TOTALREWIN_WIDTH As Integer = 1700
  Private Const GRID_COLUMN_HOLDVSWIN_WIDTH As Integer = 1700

  'SQL Columns
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 0
  Private Const SQL_COLUMN_MANUFACTURER As Integer = 1
  Private Const SQL_COLUMN_COININ As Integer = 2
  Private Const SQL_COLUMN_TOTALWON As Integer = 3
  Private Const SQL_COLUMN_HOLD As Integer = 4
  Private Const SQL_COLUMN_BILLIN As Integer = 5
  Private Const SQL_COLUMN_CASHABLETICKETIN As Integer = 7
  Private Const SQL_COLUMN_RESTRICTEDTICKETIN As Integer = 8
  Private Const SQL_COLUMN_NONRESTRICTEDTICKETIN As Integer = 9
  Private Const SQL_COLUMN_CASHABLETICKETOUT As Integer = 10
  Private Const SQL_COLUMN_RESTRICTEDTICKETOUT As Integer = 11
  Private Const SQL_COLUMN_JACKPOTS As Integer = 12
  Private Const SQL_COLUMN_HANDPAIDCANCELEDCREDITS As Integer = 13
  Private Const SQL_COLUMN_TOTALIN As Integer = 14
  Private Const SQL_COLUMN_TOTALOUT As Integer = 16
  Private Const SQL_COLUMN_TOTALWIN As Integer = 17
  Private Const SQL_COLUMN_TOTALREIN As Integer = 18
  Private Const SQL_COLUMN_TOTALREOUT As Integer = 19
  Private Const SQL_COLUMN_TOTALREWIN As Integer = 20
  Private Const SQL_COLUMN_GAMES As Integer = 21
  Private Const SQL_COLUMN_WON As Integer = 22
  Private Const SQL_COLUMN_CASHABLECARDSIN As Integer = 23
  Private Const SQL_COLUMN_RESTRICTEDCARDSIN As Integer = 24
  Private Const SQL_COLUMN_NONRESTRICTEDCARDSIN As Integer = 25
  Private Const SQL_COLUMN_CASHABLECARDSOUT As Integer = 26
  Private Const SQL_COLUMN_RESTRICTEDCARDSOUT As Integer = 27
  Private Const SQL_COLUMN_NONRESTRICTEDCARDSOUT As Integer = 28
  ' Obsolete:
  'Private Const SQL_COLUMN_HOLDVSWIN As Integer = 20


  Private Const GRID_COLUMNS As Integer = 30
  Private Const GRID_HEADER_ROWS As Integer = 2

  'Others
  Private Const PANEL_FILTER_HEIGHT = 70

#End Region

#Region " Members "

  ' For report
  Private m_date As String

  Private Class AcumValues
    Public TerminalName As String = ""
    Public Manufacturer As String = ""
    Public CoinIn As Decimal = 0
    Public TotalWon As Decimal = 0
    Public Hold As Decimal = 0
    Public Games As Decimal = 0
    Public Won As Decimal = 0
    Public BetAverage As Decimal = 0
    Public BillIn As Decimal = 0
    Public CashableTicketIn As Decimal = 0
    Public RestrictedTicketIn As Decimal = 0
    Public NonRestrictedTicketIn As Decimal = 0
    Public CashableTicketOut As Decimal = 0
    Public RestrictedTicketOut As Decimal = 0
    Public CashableCardsIn As Decimal = 0
    Public RestrictedCardsIn As Decimal = 0
    Public NonRestrictedCardsIn As Decimal = 0
    Public CashableCardsOut As Decimal = 0
    Public RestrictedCardsOut As Decimal = 0
    Public NonRestrictedCardsOut As Decimal = 0
    Public Jackpots As Decimal = 0
    Public HandPaidCanceledCredits As Decimal = 0
    Public TotalIn As Decimal = 0
    Public TotalOut As Decimal = 0
    Public TotalWin As Decimal = 0
    Public TotalReIn As Decimal = 0
    Public TotalReOut As Decimal = 0
    Public TotalReWin As Decimal = 0
    Public HoldvsWin As Decimal = 0
    Public NumEgms As Integer = 0

    Public Sub Clear()
      TerminalName = ""
      Manufacturer = ""
      CoinIn = 0
      TotalWon = 0
      Hold = 0
      Games = 0
      Won = 0
      BetAverage = 0
      BillIn = 0
      CashableTicketIn = 0
      RestrictedTicketIn = 0
      NonRestrictedTicketIn = 0
      CashableTicketOut = 0
      RestrictedTicketOut = 0
      CashableCardsIn = 0
      RestrictedCardsIn = 0
      NonRestrictedCardsIn = 0
      CashableCardsOut = 0
      RestrictedCardsOut = 0
      NonRestrictedCardsOut = 0
      Jackpots = 0
      HandPaidCanceledCredits = 0
      TotalIn = 0
      TotalOut = 0
      TotalWin = 0
      TotalReIn = 0
      TotalReOut = 0
      TotalReWin = 0
      NumEgms = 0
      HoldvsWin = 0
    End Sub
  End Class

  Private m_acum_prov As New AcumValues
  Private m_acum_totals As New AcumValues

  Private m_show_totals As Boolean

#End Region

#Region " Properties "

#End Region

#Region " Overridable GUI function "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TITO_HOLD_VS_WIN_REPORT
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize frm_tito_holdvswin_report form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7193)

    chk_group_by_prov.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5415)

    ' GUI Buttons - Visible
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Visible = False

    ' Text
    ' Cancel button text =>  "Exit"
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    ' Date
    Me.dt_datetime.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5723)

    ' Panel filter
    Me.panel_filter.Height = PANEL_FILTER_HEIGHT

    ' Default values
    SetDefaultValues()

    ' Grid style View
    Call GUI_StyleView()

  End Sub ' GUI_InitControls

  ' PURPOSE: Overridable routine to check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean
    Return True
  End Function 'GUI_FilterCheck

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  Protected Overrides Function GUI_GetQueryType() As GUI_Controls.frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function 'GUI_GetQueryType

  ' PURPOSE: Build an SQL Command query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL Command query ready to send to the database
  Protected Overrides Function GUI_GetSqlCommand() As System.Data.SqlClient.SqlCommand
    Dim _sql_cmd As SqlCommand
    Dim _datetime As DateTime
    Dim _group_by As Integer
    Dim _order_by As Integer

    _group_by = 0
    _order_by = 0
    _sql_cmd = New SqlCommand("TITO_HoldvsWin")
    _sql_cmd.CommandType = CommandType.StoredProcedure

    _datetime = dt_datetime.Value

    If (chk_group_by_prov.Checked) Then
      _order_by = 1
    End If

    _sql_cmd.Parameters.Add("@pIniDate", SqlDbType.DateTime).Value = _datetime
    _sql_cmd.Parameters.Add("@pGroupByOption", SqlDbType.Bit).Value = _group_by
    _sql_cmd.Parameters.Add("@pOrderGroup0", SqlDbType.Bit).Value = _order_by

    Return _sql_cmd
  End Function 'GUI_GetSqlCommand

  ' PURPOSE: Reset filters from form
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     
  Protected Overrides Sub GUI_FilterReset()
    SetDefaultValues()
  End Sub 'GUI_FilterReset

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Call MyBase.GUI_SetupRow(RowIndex, DbRow)
    Dim _idx_row = 0
    Dim _average_value As Decimal = 0.0

    If (chk_group_by_prov.Checked) Then
      If Not String.IsNullOrEmpty(m_acum_prov.Manufacturer) And Not m_acum_prov.Manufacturer.Equals(DbRow.Value(SQL_COLUMN_MANUFACTURER)) Then
        DrawTotal(m_acum_prov, True, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
        m_acum_prov.Clear()
      End If
      AcumulateIn(DbRow, m_acum_prov)
    End If


    AcumulateInTotal(DbRow, m_acum_totals)

    'Returning SP indexs, with SP group parameter = 0 (WARNING, indexes will change depending on parameter)
    '0-Asset number             
    '1-Manufacturer
    '2-CoinIn x
    '3-TotalWon x
    '4-Hold x
    '6-BillIn x
    '7-CashableTicketIn x
    '8-RestrictedTicketIn
    '9-NonRestrictedTicketIn
    '10-CashableTicketOut x
    '11-RestrictedTicketOut
    '12-Jackpots
    '13-HandPaidCanceledCredits
    '14-TotalIn x
    '15-TotalOut x
    '16-TotalWin x
    '17-TotalReIn
    '18-TotalReOut
    '19-TotalReWin x
    '20-Games
    '21-Won
    '22-CashableCardsIn,
    '23-RestrictedCardsIn,
    '24-NonRestrictedCardsIn,
    '25-CashableCardsOut,
    '26-RestrictedCardsOut,
    '27-NonRestrictedCardsOut

    With Me.Grid
      .AddRow()
      _idx_row = Me.Grid.NumRows() - 1
      .Cell(_idx_row, GRID_COLUMN_TERMINAL_NAME).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
      .Cell(_idx_row, GRID_COLUMN_MANUFACTURER).Value = DbRow.Value(SQL_COLUMN_MANUFACTURER)
      .Cell(_idx_row, GRID_COLUMN_COININ).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COININ))
      .Cell(_idx_row, GRID_COLUMN_TOTALWON).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTALWON))
      .Cell(_idx_row, GRID_COLUMN_HOLD).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_HOLD))
      .Cell(_idx_row, GRID_COLUMN_GAMES).Value = DbRow.Value(SQL_COLUMN_GAMES)
      .Cell(_idx_row, GRID_COLUMN_WON).Value = DbRow.Value(SQL_COLUMN_WON)

      If Not DbRow.IsNull(SQL_COLUMN_GAMES) AndAlso DbRow.Value(SQL_COLUMN_GAMES) > 0 Then
        _average_value = DbRow.Value(SQL_COLUMN_COININ) / DbRow.Value(SQL_COLUMN_GAMES)
      End If

      .Cell(_idx_row, GRID_COLUMN_BETAVERAGE).Value = GUI_FormatCurrency(_average_value, 2)

      .Cell(_idx_row, GRID_COLUMN_BILLIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_BILLIN))
      .Cell(_idx_row, GRID_COLUMN_CASHABLETICKETIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CASHABLETICKETIN))
      .Cell(_idx_row, GRID_COLUMN_RESTRICTEDTICKETIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_RESTRICTEDTICKETIN))
      .Cell(_idx_row, GRID_COLUMN_NONRESTRICTEDTICKETIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NONRESTRICTEDTICKETIN))
      .Cell(_idx_row, GRID_COLUMN_CASHABLETICKETOUT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CASHABLETICKETOUT))
      .Cell(_idx_row, GRID_COLUMN_RESTRICTEDTICKETOUT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_RESTRICTEDTICKETOUT))
      .Cell(_idx_row, GRID_COLUMN_CASHABLECARDSIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CASHABLECARDSIN))
      .Cell(_idx_row, GRID_COLUMN_RESTRICTEDCARDSIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_RESTRICTEDCARDSIN))
      .Cell(_idx_row, GRID_COLUMN_NONRESTRICTEDCARDSIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NONRESTRICTEDCARDSIN))
      .Cell(_idx_row, GRID_COLUMN_CASHABLECARDSOUT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CASHABLECARDSOUT))
      .Cell(_idx_row, GRID_COLUMN_RESTRICTEDCARDSOUT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_RESTRICTEDCARDSOUT))
      .Cell(_idx_row, GRID_COLUMN_NONRESTRICTEDCARDSOUT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_NONRESTRICTEDCARDSOUT))
      .Cell(_idx_row, GRID_COLUMN_JACKPOTS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_JACKPOTS))
      .Cell(_idx_row, GRID_COLUMN_HANDPAIDCANCELEDCREDITS).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_HANDPAIDCANCELEDCREDITS))
      .Cell(_idx_row, GRID_COLUMN_TOTALIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTALIN))
      .Cell(_idx_row, GRID_COLUMN_TOTALOUT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTALOUT))
      .Cell(_idx_row, GRID_COLUMN_TOTALWIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTALWIN))
      .Cell(_idx_row, GRID_COLUMN_TOTALREIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTALREIN))
      .Cell(_idx_row, GRID_COLUMN_TOTALREOUT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTALREOUT))
      .Cell(_idx_row, GRID_COLUMN_TOTALREWIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTALREWIN))
      .Cell(_idx_row, GRID_COLUMN_HOLDVSWIN).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_HOLD) - GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTALWIN)))
    End With

  End Function ' GUI_SetupRow

  ' PURPOSE : Process button actions in order to branch to a child screen
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Call MyBase.GUI_ButtonClick(ButtonId)
  End Sub 'GUI_ButtonClick

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_AfterLastRow()
    MyBase.GUI_AfterLastRow()
    If (chk_group_by_prov.Checked) Then
      DrawTotal(m_acum_prov, True, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
      m_acum_prov.Clear()
    End If
    DrawTotal(m_acum_totals, False, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    m_acum_totals.Clear()

  End Sub 'GUI_AfterLastRow

#Region " GUI Reports "

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(6994), m_date)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5415),
                          IIf(m_show_totals, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))

  End Sub

  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)
    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1261)
    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_PORTRAIT

  End Sub

  Protected Overrides Sub GUI_ReportUpdateFilters()
    m_date = dt_datetime.Value.ToString
    m_show_totals = chk_group_by_prov.Checked
  End Sub

#End Region ' GUI Reports

#End Region

#Region " Private Functions "

  Private Sub GUI_StyleView()

    Call Me.Grid.Init(GRID_COLUMNS, GRID_HEADER_ROWS)

    ' COLUMN_INDEX
    Me.Grid.Column(GRID_COLUMN_INDEX).Header(0).Text = ""
    Me.Grid.Column(GRID_COLUMN_INDEX).Header(1).Text = ""
    Me.Grid.Column(GRID_COLUMN_INDEX).Width = GRID_COLUMN_INDEX_WIDTH
    Me.Grid.Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
    Me.Grid.Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

    Me.Grid.Column(GRID_COLUMN_TERMINAL_NAME).Header(0).Text = ""
    Me.Grid.Column(GRID_COLUMN_TERMINAL_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1968)
    Me.Grid.Column(GRID_COLUMN_TERMINAL_NAME).Width = GRID_COLUMN_TERMINAL_NAME_WIDTH
    Me.Grid.Column(GRID_COLUMN_TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    Me.Grid.Column(GRID_COLUMN_TERMINAL_NAME).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_MANUFACTURER).Header(0).Text = ""
    Me.Grid.Column(GRID_COLUMN_MANUFACTURER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7195)
    Me.Grid.Column(GRID_COLUMN_MANUFACTURER).Width = GRID_COLUMN_MANUFACTURER_WIDTH
    Me.Grid.Column(GRID_COLUMN_MANUFACTURER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    Me.Grid.Column(GRID_COLUMN_MANUFACTURER).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_COININ).Header(0).Text = ""
    Me.Grid.Column(GRID_COLUMN_COININ).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7214)
    Me.Grid.Column(GRID_COLUMN_COININ).Width = GRID_COLUMN_COININ_WIDTH
    Me.Grid.Column(GRID_COLUMN_COININ).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_COININ).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_TOTALWON).Header(0).Text = ""
    Me.Grid.Column(GRID_COLUMN_TOTALWON).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7196)
    Me.Grid.Column(GRID_COLUMN_TOTALWON).Width = GRID_COLUMN_TOTALWON_WIDTH
    Me.Grid.Column(GRID_COLUMN_TOTALWON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_TOTALWON).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_HOLD).Header(0).Text = ""
    Me.Grid.Column(GRID_COLUMN_HOLD).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7197)
    Me.Grid.Column(GRID_COLUMN_HOLD).Width = GRID_COLUMN_HOLD_WIDTH
    Me.Grid.Column(GRID_COLUMN_HOLD).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_HOLD).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_GAMES).Header(0).Text = ""
    Me.Grid.Column(GRID_COLUMN_GAMES).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1114)
    Me.Grid.Column(GRID_COLUMN_GAMES).Width = GRID_COLUMN_GAMES_WIDTH
    Me.Grid.Column(GRID_COLUMN_GAMES).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_GAMES).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_WON).Header(0).Text = ""
    Me.Grid.Column(GRID_COLUMN_WON).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1115)
    Me.Grid.Column(GRID_COLUMN_WON).Width = GRID_COLUMN_WON_WIDTH
    Me.Grid.Column(GRID_COLUMN_WON).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_WON).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_BETAVERAGE).Header(0).Text = ""
    Me.Grid.Column(GRID_COLUMN_BETAVERAGE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6893)
    Me.Grid.Column(GRID_COLUMN_BETAVERAGE).Width = GRID_COLUMN_BETAVERAGE_WIDTH
    Me.Grid.Column(GRID_COLUMN_BETAVERAGE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_BETAVERAGE).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_BILLIN).Header(0).Text = ""
    Me.Grid.Column(GRID_COLUMN_BILLIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7198)
    Me.Grid.Column(GRID_COLUMN_BILLIN).Width = GRID_COLUMN_BILLIN_WIDTH
    Me.Grid.Column(GRID_COLUMN_BILLIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_BILLIN).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_CASHABLETICKETIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9024)
    Me.Grid.Column(GRID_COLUMN_CASHABLETICKETIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
    Me.Grid.Column(GRID_COLUMN_CASHABLETICKETIN).Width = GRID_COLUMN_CASHABLETICKETIN_WIDTH
    Me.Grid.Column(GRID_COLUMN_CASHABLETICKETIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_CASHABLETICKETIN).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_RESTRICTEDTICKETIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9024)
    Me.Grid.Column(GRID_COLUMN_RESTRICTEDTICKETIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9022)
    Me.Grid.Column(GRID_COLUMN_RESTRICTEDTICKETIN).Width = GRID_COLUMN_RESTRICTEDTICKETIN_WIDTH
    Me.Grid.Column(GRID_COLUMN_RESTRICTEDTICKETIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_RESTRICTEDTICKETIN).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_NONRESTRICTEDTICKETIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9024)
    Me.Grid.Column(GRID_COLUMN_NONRESTRICTEDTICKETIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9023)
    Me.Grid.Column(GRID_COLUMN_NONRESTRICTEDTICKETIN).Width = GRID_COLUMN_NONRESTRICTEDTICKETIN_WIDTH
    Me.Grid.Column(GRID_COLUMN_NONRESTRICTEDTICKETIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_NONRESTRICTEDTICKETIN).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_CASHABLETICKETOUT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9025)
    Me.Grid.Column(GRID_COLUMN_CASHABLETICKETOUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
    Me.Grid.Column(GRID_COLUMN_CASHABLETICKETOUT).Width = GRID_COLUMN_CASHABLETICKETOUT_WIDTH
    Me.Grid.Column(GRID_COLUMN_CASHABLETICKETOUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_CASHABLETICKETOUT).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_RESTRICTEDTICKETOUT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9025)
    Me.Grid.Column(GRID_COLUMN_RESTRICTEDTICKETOUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9022)
    Me.Grid.Column(GRID_COLUMN_RESTRICTEDTICKETOUT).Width = GRID_COLUMN_RESTRICTEDTICKETOUT_WIDTH
    Me.Grid.Column(GRID_COLUMN_RESTRICTEDTICKETOUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_RESTRICTEDTICKETOUT).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_CASHABLECARDSIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9026)
    Me.Grid.Column(GRID_COLUMN_CASHABLECARDSIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
    Me.Grid.Column(GRID_COLUMN_CASHABLECARDSIN).Width = GRID_COLUMN_CASHABLECARDSIN_WIDTH
    Me.Grid.Column(GRID_COLUMN_CASHABLECARDSIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_CASHABLECARDSIN).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_RESTRICTEDCARDSIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9026)
    Me.Grid.Column(GRID_COLUMN_RESTRICTEDCARDSIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9022)
    Me.Grid.Column(GRID_COLUMN_RESTRICTEDCARDSIN).Width = GRID_COLUMN_RESTRICTEDCARDSIN_WIDTH
    Me.Grid.Column(GRID_COLUMN_RESTRICTEDCARDSIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_RESTRICTEDCARDSIN).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_NONRESTRICTEDCARDSIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9026)
    Me.Grid.Column(GRID_COLUMN_NONRESTRICTEDCARDSIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9023)
    Me.Grid.Column(GRID_COLUMN_NONRESTRICTEDCARDSIN).Width = GRID_COLUMN_NONRESTRICTEDCARDSIN_WIDTH
    Me.Grid.Column(GRID_COLUMN_NONRESTRICTEDCARDSIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_NONRESTRICTEDCARDSIN).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_CASHABLECARDSOUT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9027)
    Me.Grid.Column(GRID_COLUMN_CASHABLECARDSOUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
    Me.Grid.Column(GRID_COLUMN_CASHABLECARDSOUT).Width = GRID_COLUMN_CASHABLECARDSOUT_WIDTH
    Me.Grid.Column(GRID_COLUMN_CASHABLECARDSOUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_CASHABLECARDSOUT).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_RESTRICTEDCARDSOUT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9027)
    Me.Grid.Column(GRID_COLUMN_RESTRICTEDCARDSOUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9022)
    Me.Grid.Column(GRID_COLUMN_RESTRICTEDCARDSOUT).Width = GRID_COLUMN_RESTRICTEDCARDSOUT_WIDTH
    Me.Grid.Column(GRID_COLUMN_RESTRICTEDCARDSOUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_RESTRICTEDCARDSOUT).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_NONRESTRICTEDCARDSOUT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9027)
    Me.Grid.Column(GRID_COLUMN_NONRESTRICTEDCARDSOUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(9023)
    Me.Grid.Column(GRID_COLUMN_NONRESTRICTEDCARDSOUT).Width = GRID_COLUMN_NONRESTRICTEDCARDSOUT_WIDTH
    Me.Grid.Column(GRID_COLUMN_NONRESTRICTEDCARDSOUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_NONRESTRICTEDCARDSOUT).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_JACKPOTS).Header(0).Text = ""
    Me.Grid.Column(GRID_COLUMN_JACKPOTS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7205)
    Me.Grid.Column(GRID_COLUMN_JACKPOTS).Width = GRID_COLUMN_JACKPOTS_WIDTH
    Me.Grid.Column(GRID_COLUMN_JACKPOTS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_JACKPOTS).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_HANDPAIDCANCELEDCREDITS).Header(0).Text = ""
    Me.Grid.Column(GRID_COLUMN_HANDPAIDCANCELEDCREDITS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7206)
    Me.Grid.Column(GRID_COLUMN_HANDPAIDCANCELEDCREDITS).Width = GRID_COLUMN_HANDPAIDCANCELEDCREDITS_WIDTH
    Me.Grid.Column(GRID_COLUMN_HANDPAIDCANCELEDCREDITS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_HANDPAIDCANCELEDCREDITS).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_TOTALIN).Header(0).Text = ""
    Me.Grid.Column(GRID_COLUMN_TOTALIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7207)
    Me.Grid.Column(GRID_COLUMN_TOTALIN).Width = GRID_COLUMN_TOTALIN_WIDTH
    Me.Grid.Column(GRID_COLUMN_TOTALIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_TOTALIN).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_TOTALOUT).Header(0).Text = ""
    Me.Grid.Column(GRID_COLUMN_TOTALOUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7209)
    Me.Grid.Column(GRID_COLUMN_TOTALOUT).Width = GRID_COLUMN_TOTALOUT_WIDTH
    Me.Grid.Column(GRID_COLUMN_TOTALOUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_TOTALOUT).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_TOTALWIN).Header(0).Text = ""
    Me.Grid.Column(GRID_COLUMN_TOTALWIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7210)
    Me.Grid.Column(GRID_COLUMN_TOTALWIN).Width = GRID_COLUMN_TOTALWIN_WIDTH
    Me.Grid.Column(GRID_COLUMN_TOTALWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_TOTALWIN).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_TOTALREIN).Header(0).Text = ""
    Me.Grid.Column(GRID_COLUMN_TOTALREIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7211)
    Me.Grid.Column(GRID_COLUMN_TOTALREIN).Width = GRID_COLUMN_TOTALREIN_WIDTH
    Me.Grid.Column(GRID_COLUMN_TOTALREIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_TOTALREIN).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_TOTALREOUT).Header(0).Text = ""
    Me.Grid.Column(GRID_COLUMN_TOTALREOUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7212)
    Me.Grid.Column(GRID_COLUMN_TOTALREOUT).Width = GRID_COLUMN_TOTALREOUT_WIDTH
    Me.Grid.Column(GRID_COLUMN_TOTALREOUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_TOTALREOUT).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_TOTALREWIN).Header(0).Text = ""
    Me.Grid.Column(GRID_COLUMN_TOTALREWIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7213)
    Me.Grid.Column(GRID_COLUMN_TOTALREWIN).Width = GRID_COLUMN_TOTALREWIN_WIDTH
    Me.Grid.Column(GRID_COLUMN_TOTALREWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_TOTALREWIN).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    Me.Grid.Column(GRID_COLUMN_HOLDVSWIN).Header(0).Text = ""
    Me.Grid.Column(GRID_COLUMN_HOLDVSWIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7216)
    Me.Grid.Column(GRID_COLUMN_HOLDVSWIN).Width = GRID_COLUMN_HOLDVSWIN_WIDTH
    Me.Grid.Column(GRID_COLUMN_HOLDVSWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    Me.Grid.Column(GRID_COLUMN_HOLDVSWIN).Header.Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

  End Sub 'GUI_StyleView

  Private Sub SetDefaultValues()

    Me.dt_datetime.Value = WSI.Common.Misc.TodayOpening()
    IgnoreRowHeightFilterInExcelReport = True

  End Sub ' SetDefaultValues

  ' PURPOSE: Show results to screen drawing the total counter data in a new row with the specified color
  '
  '  PARAMS:
  '     - INPUT:
  '          - LastTotal: Indicates the kind of total
  '     - OUTPUT:
  '
  ' RETURNS:
  '   - True

  Private Function DrawTotal(ByVal Acum As AcumValues, IsProvider As Boolean, ByVal Color As ENUM_GUI_COLOR) As Boolean
    Dim _idx_row As Integer

    ' Add the totals row & data
    With Me.Grid
      .AddRow()
      _idx_row = Me.Grid.NumRows - 1

      If IsProvider Then
        .Cell(_idx_row, GRID_COLUMN_MANUFACTURER).Value = Acum.Manufacturer
      Else
        .Cell(_idx_row, GRID_COLUMN_MANUFACTURER).Value = GLB_NLS_GUI_INVOICING.GetString(205)
      End If

      .Cell(_idx_row, GRID_COLUMN_COININ).Value = GUI_FormatCurrency(Acum.CoinIn)
      .Cell(_idx_row, GRID_COLUMN_TOTALWON).Value = GUI_FormatCurrency(Acum.TotalWon)
      .Cell(_idx_row, GRID_COLUMN_HOLD).Value = GUI_FormatCurrency(Acum.Hold)
      .Cell(_idx_row, GRID_COLUMN_GAMES).Value = Acum.Games
      .Cell(_idx_row, GRID_COLUMN_WON).Value = Acum.Won
      .Cell(_idx_row, GRID_COLUMN_BETAVERAGE).Value = GUI_FormatCurrency(Acum.BetAverage, 2)
      .Cell(_idx_row, GRID_COLUMN_BILLIN).Value = GUI_FormatCurrency(Acum.BillIn)
      .Cell(_idx_row, GRID_COLUMN_CASHABLETICKETIN).Value = GUI_FormatCurrency(Acum.CashableTicketIn)
      .Cell(_idx_row, GRID_COLUMN_RESTRICTEDTICKETIN).Value = GUI_FormatCurrency(Acum.RestrictedTicketIn)
      .Cell(_idx_row, GRID_COLUMN_NONRESTRICTEDTICKETIN).Value = GUI_FormatCurrency(Acum.NonRestrictedTicketIn)
      .Cell(_idx_row, GRID_COLUMN_CASHABLETICKETOUT).Value = GUI_FormatCurrency(Acum.CashableTicketOut)
      .Cell(_idx_row, GRID_COLUMN_RESTRICTEDTICKETOUT).Value = GUI_FormatCurrency(Acum.RestrictedTicketOut)
      .Cell(_idx_row, GRID_COLUMN_CASHABLECARDSIN).Value = GUI_FormatCurrency(Acum.CashableCardsIn)
      .Cell(_idx_row, GRID_COLUMN_RESTRICTEDCARDSIN).Value = GUI_FormatCurrency(Acum.RestrictedCardsIn)
      .Cell(_idx_row, GRID_COLUMN_NONRESTRICTEDCARDSIN).Value = GUI_FormatCurrency(Acum.NonRestrictedCardsIn)
      .Cell(_idx_row, GRID_COLUMN_CASHABLECARDSOUT).Value = GUI_FormatCurrency(Acum.CashableCardsOut)
      .Cell(_idx_row, GRID_COLUMN_RESTRICTEDCARDSOUT).Value = GUI_FormatCurrency(Acum.RestrictedCardsOut)
      .Cell(_idx_row, GRID_COLUMN_NONRESTRICTEDCARDSOUT).Value = GUI_FormatCurrency(Acum.NonRestrictedCardsOut)
      .Cell(_idx_row, GRID_COLUMN_JACKPOTS).Value = GUI_FormatCurrency(Acum.Jackpots)
      .Cell(_idx_row, GRID_COLUMN_HANDPAIDCANCELEDCREDITS).Value = GUI_FormatCurrency(Acum.HandPaidCanceledCredits)
      .Cell(_idx_row, GRID_COLUMN_TOTALIN).Value = GUI_FormatCurrency(Acum.TotalIn)
      .Cell(_idx_row, GRID_COLUMN_TOTALOUT).Value = GUI_FormatCurrency(Acum.TotalOut)
      .Cell(_idx_row, GRID_COLUMN_TOTALWIN).Value = GUI_FormatCurrency(Acum.TotalWin)
      .Cell(_idx_row, GRID_COLUMN_TOTALREIN).Value = GUI_FormatCurrency(Acum.TotalReIn)
      .Cell(_idx_row, GRID_COLUMN_TOTALREOUT).Value = GUI_FormatCurrency(Acum.TotalReOut)
      .Cell(_idx_row, GRID_COLUMN_TOTALREWIN).Value = GUI_FormatCurrency(Acum.TotalReWin)
      .Cell(_idx_row, GRID_COLUMN_HOLDVSWIN).Value = GUI_FormatCurrency(Acum.HoldvsWin)

      .Row(_idx_row).BackColor = GetColor(Color)

    End With
  End Function

  Private Sub AcumulateIn(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW, Acum As AcumValues)
    Dim _average_value As Decimal = 0.0

    m_acum_prov.TerminalName = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
    m_acum_prov.Manufacturer = DbRow.Value(SQL_COLUMN_MANUFACTURER)
    m_acum_prov.CoinIn += DbRow.Value(SQL_COLUMN_COININ)
    m_acum_prov.TotalWon += DbRow.Value(SQL_COLUMN_TOTALWON)
    m_acum_prov.Hold += DbRow.Value(SQL_COLUMN_HOLD)
    m_acum_prov.Games += DbRow.Value(SQL_COLUMN_GAMES)
    m_acum_prov.Won += DbRow.Value(SQL_COLUMN_WON)

    If Not DbRow.IsNull(SQL_COLUMN_GAMES) AndAlso DbRow.Value(SQL_COLUMN_GAMES) > 0 Then
      _average_value = DbRow.Value(SQL_COLUMN_COININ) / DbRow.Value(SQL_COLUMN_GAMES)
    End If
    m_acum_prov.BetAverage += GUI_FormatCurrency(_average_value, 2)

    m_acum_prov.BillIn += DbRow.Value(SQL_COLUMN_BILLIN)
    m_acum_prov.CashableTicketIn += DbRow.Value(SQL_COLUMN_CASHABLETICKETIN)
    m_acum_prov.RestrictedTicketIn += DbRow.Value(SQL_COLUMN_RESTRICTEDTICKETIN)
    m_acum_prov.NonRestrictedTicketIn += DbRow.Value(SQL_COLUMN_NONRESTRICTEDTICKETIN)
    m_acum_prov.CashableTicketOut += DbRow.Value(SQL_COLUMN_CASHABLETICKETOUT)
    m_acum_prov.RestrictedTicketOut += DbRow.Value(SQL_COLUMN_RESTRICTEDTICKETOUT)
    m_acum_prov.CashableCardsIn += DbRow.Value(SQL_COLUMN_CASHABLECARDSIN)
    m_acum_prov.RestrictedCardsIn += DbRow.Value(SQL_COLUMN_RESTRICTEDCARDSIN)
    m_acum_prov.NonRestrictedCardsIn += DbRow.Value(SQL_COLUMN_NONRESTRICTEDCARDSIN)
    m_acum_prov.CashableCardsOut += DbRow.Value(SQL_COLUMN_CASHABLECARDSOUT)
    m_acum_prov.RestrictedCardsOut += DbRow.Value(SQL_COLUMN_RESTRICTEDCARDSOUT)
    m_acum_prov.NonRestrictedCardsOut += DbRow.Value(SQL_COLUMN_NONRESTRICTEDCARDSOUT)
    m_acum_prov.Jackpots += DbRow.Value(SQL_COLUMN_JACKPOTS)
    m_acum_prov.HandPaidCanceledCredits += DbRow.Value(SQL_COLUMN_HANDPAIDCANCELEDCREDITS)
    m_acum_prov.TotalIn += DbRow.Value(SQL_COLUMN_TOTALIN)
    m_acum_prov.TotalOut += DbRow.Value(SQL_COLUMN_TOTALOUT)
    m_acum_prov.TotalWin += DbRow.Value(SQL_COLUMN_TOTALWIN)
    m_acum_prov.TotalReIn += DbRow.Value(SQL_COLUMN_TOTALREIN)
    m_acum_prov.TotalReOut += DbRow.Value(SQL_COLUMN_TOTALREOUT)
    m_acum_prov.TotalReWin += DbRow.Value(SQL_COLUMN_TOTALREWIN)
    m_acum_prov.HoldvsWin += DbRow.Value(SQL_COLUMN_HOLD) - DbRow.Value(SQL_COLUMN_TOTALWIN)
  End Sub

  Private Sub AcumulateInTotal(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW, Acum As AcumValues)
    Dim _average_value As Decimal = 0.0

    m_acum_totals.TerminalName = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
    m_acum_totals.Manufacturer = DbRow.Value(SQL_COLUMN_MANUFACTURER)
    m_acum_totals.CoinIn += DbRow.Value(SQL_COLUMN_COININ)
    m_acum_totals.TotalWon += DbRow.Value(SQL_COLUMN_TOTALWON)
    m_acum_totals.Hold += DbRow.Value(SQL_COLUMN_HOLD)
    m_acum_totals.Games += DbRow.Value(SQL_COLUMN_GAMES)
    m_acum_totals.Won += DbRow.Value(SQL_COLUMN_WON)

    If Not DbRow.IsNull(SQL_COLUMN_GAMES) AndAlso DbRow.Value(SQL_COLUMN_GAMES) > 0 Then
      _average_value = DbRow.Value(SQL_COLUMN_COININ) / DbRow.Value(SQL_COLUMN_GAMES)
    End If

    m_acum_totals.BetAverage += GUI_FormatCurrency(_average_value, 2)

    m_acum_totals.BillIn += DbRow.Value(SQL_COLUMN_BILLIN)
    m_acum_totals.CashableTicketIn += DbRow.Value(SQL_COLUMN_CASHABLETICKETIN)
    m_acum_totals.RestrictedTicketIn += DbRow.Value(SQL_COLUMN_RESTRICTEDTICKETIN)
    m_acum_totals.NonRestrictedTicketIn += DbRow.Value(SQL_COLUMN_NONRESTRICTEDTICKETIN)
    m_acum_totals.CashableTicketOut += DbRow.Value(SQL_COLUMN_CASHABLETICKETOUT)
    m_acum_totals.RestrictedTicketOut += DbRow.Value(SQL_COLUMN_RESTRICTEDTICKETOUT)
    m_acum_totals.CashableCardsIn += DbRow.Value(SQL_COLUMN_CASHABLECARDSIN)
    m_acum_totals.RestrictedCardsIn += DbRow.Value(SQL_COLUMN_RESTRICTEDCARDSIN)
    m_acum_totals.NonRestrictedCardsIn += DbRow.Value(SQL_COLUMN_NONRESTRICTEDCARDSIN)
    m_acum_totals.CashableCardsOut += DbRow.Value(SQL_COLUMN_CASHABLECARDSOUT)
    m_acum_totals.RestrictedCardsOut += DbRow.Value(SQL_COLUMN_RESTRICTEDCARDSOUT)
    m_acum_totals.NonRestrictedCardsOut += DbRow.Value(SQL_COLUMN_NONRESTRICTEDCARDSOUT)
    m_acum_totals.Jackpots += DbRow.Value(SQL_COLUMN_JACKPOTS)
    m_acum_totals.HandPaidCanceledCredits += DbRow.Value(SQL_COLUMN_HANDPAIDCANCELEDCREDITS)
    m_acum_totals.TotalIn += DbRow.Value(SQL_COLUMN_TOTALIN)
    m_acum_totals.TotalOut += DbRow.Value(SQL_COLUMN_TOTALOUT)
    m_acum_totals.TotalWin += DbRow.Value(SQL_COLUMN_TOTALWIN)
    m_acum_totals.TotalReIn += DbRow.Value(SQL_COLUMN_TOTALREIN)
    m_acum_totals.TotalReOut += DbRow.Value(SQL_COLUMN_TOTALREOUT)
    m_acum_totals.TotalReWin += DbRow.Value(SQL_COLUMN_TOTALREWIN)
    m_acum_totals.HoldvsWin += DbRow.Value(SQL_COLUMN_HOLD) - DbRow.Value(SQL_COLUMN_TOTALWIN)
  End Sub

#End Region

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region

#Region " Events "

#End Region

End Class
