<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_tito_cash_sel_date
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.uc_dates = New GUI_Controls.uc_daily_session_selector
    Me.cb_rows = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.cb_rows)
    Me.panel_filter.Controls.Add(Me.uc_dates)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1197, 186)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dates, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.cb_rows, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 190)
    Me.panel_data.Size = New System.Drawing.Size(1197, 445)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1191, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1191, 4)
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(269, 0)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(384, 217)
    Me.uc_pr_list.TabIndex = 14
    '
    'uc_dates
    '
    Me.uc_dates.ClosingTime = 0
    Me.uc_dates.ClosingTimeEnabled = True
    Me.uc_dates.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dates.FromDateSelected = True
    Me.uc_dates.Location = New System.Drawing.Point(8, 3)
    Me.uc_dates.Name = "uc_dates"
    Me.uc_dates.Size = New System.Drawing.Size(255, 83)
    Me.uc_dates.TabIndex = 22
    Me.uc_dates.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dates.ToDateSelected = True
    '
    'cb_rows
    '
    Me.cb_rows.AutoSize = True
    Me.cb_rows.Location = New System.Drawing.Point(8, 101)
    Me.cb_rows.Name = "cb_rows"
    Me.cb_rows.Size = New System.Drawing.Size(63, 17)
    Me.cb_rows.TabIndex = 23
    Me.cb_rows.Text = "xRows"
    Me.cb_rows.UseVisualStyleBackColor = True
    '
    'frm_tito_cash_sel_date
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1207, 639)
    Me.Name = "frm_tito_cash_sel_date"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_cash_tito_sel_date"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents uc_dates As GUI_Controls.uc_daily_session_selector
  Friend WithEvents cb_rows As System.Windows.Forms.CheckBox
End Class
