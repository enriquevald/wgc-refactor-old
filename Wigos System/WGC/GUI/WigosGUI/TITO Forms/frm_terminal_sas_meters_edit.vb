'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_terminal_sas_meters_edit
' DESCRIPTION:   
' AUTHOR:        David Lasdiez
' CREATION DATE: 11-NOV-2014
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 11-NOV-2014 DLL    First Release
' 16-DEC-2014 DLL    Fixed Bug WIG-1838: Different errors
' 30-MAR-2015 DLL    Fixed Bug WIG-2186: Incorrect message
'-------------------------------------------------------------------


Option Strict Off
Option Explicit On
Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_CommonMisc
Imports System.Text
Imports WSI.Common


Public Class frm_terminal_sas_meters_edit
  Inherits frm_base_print

#Region "Members"
  Dim m_date_from As DateTime
  Dim m_date_to As DateTime
  Dim m_date_to_filter As DateTime
  Dim m_cls_terminal_sas_meters As cls_terminal_sas_meters
  Dim m_terminals As String
  Dim m_meters As String
  Dim m_options As String
  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_selected_row As Integer

#End Region

#Region "Constants"
  Private Const GRID_HEADERS_COUNT As Integer = 2
  Private GRID_FIXED_COLUMNS As Integer = 9
  Private GRID_ALARMS_COLUMNS As Integer = 4

  'SQL Meter Columns
  Private Const SQL_DATETIME_LONG As Integer = 1
  Private Const SQL_DATETIME As Integer = 2
  Private Const SQL_TERMINAL_ID As Integer = 3
  Private Const SQL_COLUMN_TERMINAL_PROVIDER As Integer = 4
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 5
  Private Const SQL_COLUMN_TERMINAL_AREA As Integer = 6
  Private Const SQL_COLUMN_TERMINAL_BANK As Integer = 7
  Private Const SQL_COLUMN_TERMINAL_POSITION As Integer = 8
  Private Const SQL_FIRST_METER As Integer = 9

  'SQL Alarms Columns
  Private Const SQL_ALARMS_DATETIME As Integer = 0
  Private Const SQL_ALARMS_CODE As Integer = 1
  Private Const SQL_ALARMS_DESCRIPTION As Integer = 2

  'Grid Meter Columns
  Private Const GRID_SELECT As Integer = 0
  Private Const GRID_DATETIME_LONG As Integer = 1
  Private Const GRID_DATETIME As Integer = 2
  Private Const GRID_TERMINAL_ID As Integer = 3
  Private Const GRID_COLUMN_TERMINAL_PROVIDER As Integer = 4
  Private Const GRID_TERMINAL_NAME As Integer = 5
  Private Const GRID_COLUMN_TERMINAL_AREA As Integer = 6
  Private Const GRID_COLUMN_TERMINAL_BANK As Integer = 7
  Private Const GRID_COLUMN_TERMINAL_POSITION As Integer = 8
  Private Const GRID_FIRST_METER As Integer = 9

  'Grid Alarms Columns
  Private Const GRID_ALARMS_SELECT As Integer = 0
  Private Const GRID_ALARMS_DATETIME As Integer = 1
  Private Const GRID_ALARMS_CODE As Integer = 2
  Private Const GRID_ALARMS_DESCRIPTION As Integer = 3

  'Datatable Columns
  Private Const DT_DATETIME As Integer = 0
  Private Const DT_TERMINAL_ID As Integer = 1
  Private Const DT_TERMINAL_NAME As Integer = 2
  Private Const DT_METER_CODE As Integer = 3
  Private Const DT_INIT_VALUE As Integer = 4
  Private Const DT_FINAL_VALUE As Integer = 5
  Private Const DT_INCREMENT_VALUE As Integer = 6

  'Width
  Private Const GRID_WIDTH_DATE As Integer = 1200
  Private Const GRID_WIDTH_DATE_LONG As Integer = 2200
  Private Const WIDTH_START_VALUE As Int32 = 5000
  Private Const LENGHT_START_VALUE As Int32 = 17
  Private Const WIDTH_ALARM_DESCRIPTION As Int32 = 10000

  Private Const VISIBLE_CODE_LENGTH = 5

#End Region

#Region "Overrides"

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TERMINAL_SAS_METERS_EDIT
    Call MyBase.GUI_SetFormId()

  End Sub 'GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5727)
    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    ' Providers - Terminals
    Call Me.uc_provider.Init(WSI.Common.Misc.GamingTerminalTypeList())

    'Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Text = GLB_NLS_GUI_CONTROLS.GetString(13)      'APPLY
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Enabled = Me.Permissions.Write
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    ' Dates
    gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2118)
    dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    ' Groups
    uc_checked_list_groups.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4928)
    uc_checked_list_groups.btn_check_all.Width = 100
    uc_checked_list_groups.btn_uncheck_all.Width = 115
    uc_checked_list_groups.btn_uncheck_all.Location = New System.Drawing.Point(117, 138)

    ' Info
    Call SetLabelsInfo()

    'Filter
    gb_filter_values.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4421) ' Opciones
    rb_all.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1337) ' Todas
    rb_without_values.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5787) ' Excluir filas sin actividad
    rb_with_values.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4738) ' Solo filas con actividad
    rb_all.Checked = True

    m_cls_terminal_sas_meters = New cls_terminal_sas_meters()
    SetDefaultValues()

    GUI_StyleSheet()

    GUI_FilterReset()

    AddHandler dg_meters.CellDataChangedEvent, AddressOf CellDataChangedEvent

  End Sub

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)
    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call Button_Search()

        _print_data = New GUI_Reports.CLASS_PRINT_DATA
        Call GUI_ReportFilter(_print_data)
        Call MyBase.AuditFormClick(ButtonId, _print_data)

      Case ENUM_BUTTON.BUTTON_FILTER_RESET
        Call GUI_FilterReset()
      Case ENUM_BUTTON.BUTTON_NEW
        SaveChanges()
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    ' The keypress event is done in uc_grid control
    If Me.dg_meters.ContainsFocus Then
      Return Me.dg_meters.KeyPressed(sender, e)
    End If

    Return MyBase.GUI_KeyPress(sender, e)

  End Function ' GUI_KeyPress


  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    Dim _rows_to_update As DataTable
    Dim _old_rows As DataTable

    _rows_to_update = Nothing
    _old_rows = Nothing


    If (Not m_cls_terminal_sas_meters Is Nothing AndAlso TestChanged(m_cls_terminal_sas_meters.TerminalSasMeters, True, _rows_to_update, _old_rows)) Then
      If NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(156), _
                                ENUM_MB_TYPE.MB_TYPE_INFO, _
                                ENUM_MB_BTN.MB_BTN_YES_NO, _
                                ENUM_MB_DEF_BTN.MB_DEF_BTN_1) = ENUM_MB_RESULT.MB_RESULT_NO Then
        CloseCanceled = True
      End If
    End If
    MyBase.GUI_Closing(CloseCanceled)
  End Sub
#End Region

#Region "Public"
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit
#End Region

#Region "Private"
  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' SetDefaultValues

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), m_cls_terminal_sas_meters.DateFrom)
    PrintData.SetFilter(GLB_NLS_GUI_AUDITOR.GetString(261) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), m_cls_terminal_sas_meters.DateTo)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2642), m_meters)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4421), m_options)

  End Sub ' GUI_ReportFilter

  Private Sub GUI_StyleSheet()
    Dim _idx As Int32
    Dim _terminal_columns As List(Of ColumnSettings)
    Dim _enum_format As GUI_Controls.CLASS_FILTER.ENUM_FORMAT

    RemoveHandler dg_meters.RowSelectedEvent, AddressOf dg_meters_RowSelectedEvent

    _idx = 0
    _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)

    With Me.dg_meters

      .Clear()
      .Init(GRID_FIXED_COLUMNS + (m_cls_terminal_sas_meters.MeterList.Count * 3), GRID_HEADERS_COUNT, True)
      .Sortable = False
      .PanelRightVisible = True

      .Column(GRID_SELECT).Header(0).Text = ""
      .Column(GRID_SELECT).Header(1).Text = ""
      .Column(GRID_SELECT).Width = 200
      .Column(GRID_SELECT).IsColumnPrintable = False
      .Column(GRID_SELECT).HighLightWhenSelected = False

      .Column(GRID_DATETIME_LONG).Header(0).Text = ""
      .Column(GRID_DATETIME_LONG).Header(1).Text = ""
      .Column(GRID_DATETIME_LONG).Width = 0
      .Column(GRID_DATETIME_LONG).IsColumnPrintable = False
      .Column(GRID_DATETIME_LONG).HighLightWhenSelected = False

      .Column(GRID_DATETIME).Header(0).Text = ""
      .Column(GRID_DATETIME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268)
      .Column(GRID_DATETIME).Width = GRID_WIDTH_DATE
      .Column(GRID_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_DATETIME).HighLightWhenSelected = True

      .Column(GRID_TERMINAL_ID).Header(0).Text = ""
      .Column(GRID_TERMINAL_ID).Header(1).Text = GLB_NLS_GUI_CLASS_II.GetString(302)    ' "xFecha"
      .Column(GRID_TERMINAL_ID).Width = 0
      .Column(GRID_TERMINAL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_TERMINAL_ID).HighLightWhenSelected = False

      .Column(GRID_COLUMN_TERMINAL_AREA).Width = 1
      .Column(GRID_COLUMN_TERMINAL_BANK).Width = 1
      .Column(GRID_COLUMN_TERMINAL_POSITION).Width = 1

      '  Terminal Report
      For _idx = 0 To _terminal_columns.Count - 1
        .Column(GRID_COLUMN_TERMINAL_PROVIDER + _idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1968)
        .Column(GRID_COLUMN_TERMINAL_PROVIDER + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        .Column(GRID_COLUMN_TERMINAL_PROVIDER + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_COLUMN_TERMINAL_PROVIDER + _idx).Alignment = _terminal_columns(_idx).Alignment
        .Column(GRID_COLUMN_TERMINAL_PROVIDER + _idx).HighLightWhenSelected = True
      Next

      _idx = 0
      For Each _meter As cls_terminal_sas_meters.Meter In m_cls_terminal_sas_meters.MeterList
        .Column(GRID_FIRST_METER + _idx).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4794)
        .Column(GRID_FIRST_METER + _idx).Header(0).Text = _meter.CatalogName
        .Column(GRID_FIRST_METER + _idx).Editable = True
        .Column(GRID_FIRST_METER + _idx).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        FormatMeterValue(_meter.Code, 0, _enum_format)
        If _enum_format = FORMAT_MONEY Then
          .Column(GRID_FIRST_METER + _idx).EditionControl.EntryField.SetFilter(_enum_format, 11, 2)
        Else
          .Column(GRID_FIRST_METER + _idx).EditionControl.EntryField.SetFilter(_enum_format, 10)
        End If
        .Column(GRID_FIRST_METER + _idx).Width = WIDTH_START_VALUE / 3 'the header0 width is calculated
        .Column(GRID_FIRST_METER + _idx).HighLightWhenSelected = False
        _idx += 1

        .Column(GRID_FIRST_METER + _idx).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4795)
        .Column(GRID_FIRST_METER + _idx).Header(0).Text = _meter.CatalogName
        .Column(GRID_FIRST_METER + _idx).Editable = True
        .Column(GRID_FIRST_METER + _idx).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        FormatMeterValue(_meter.Code, 0, _enum_format)
        If _enum_format = FORMAT_MONEY Then
          .Column(GRID_FIRST_METER + _idx).EditionControl.EntryField.SetFilter(_enum_format, 11, 2)
        Else
          .Column(GRID_FIRST_METER + _idx).EditionControl.EntryField.SetFilter(_enum_format, 10)
        End If
        .Column(GRID_FIRST_METER + _idx).Width = WIDTH_START_VALUE / 3 'the header0 width is calculated
        .Column(GRID_FIRST_METER + _idx).HighLightWhenSelected = False
        _idx += 1

        .Column(GRID_FIRST_METER + _idx).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4796)
        .Column(GRID_FIRST_METER + _idx).Header(0).Text = _meter.CatalogName
        .Column(GRID_FIRST_METER + _idx).Editable = True
        .Column(GRID_FIRST_METER + _idx).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        FormatMeterValue(_meter.Code, 0, _enum_format)
        If _enum_format = FORMAT_MONEY Then
          .Column(GRID_FIRST_METER + _idx).EditionControl.EntryField.SetFilter(_enum_format, 11, 2)
        Else
          .Column(GRID_FIRST_METER + _idx).EditionControl.EntryField.SetFilter(_enum_format, 10)
        End If
        .Column(GRID_FIRST_METER + _idx).Width = WIDTH_START_VALUE / 3 'the header0 width is calculated
        .Column(GRID_FIRST_METER + _idx).HighLightWhenSelected = False
        _idx += 1
      Next
    End With

    GUI_StyleSheetAlarms()

    AddHandler dg_meters.RowSelectedEvent, AddressOf dg_meters_RowSelectedEvent

  End Sub ' GUI_StyleSheet

  Private Sub GUI_StyleSheetAlarms()

    With Me.dg_alarms

      .Clear()
      .Init(GRID_ALARMS_COLUMNS, 2)
      .Sortable = False

      .Column(GRID_ALARMS_SELECT).Header(0).Text = ""
      .Column(GRID_ALARMS_SELECT).Header(1).Text = ""
      .Column(GRID_ALARMS_SELECT).Width = 200
      .Column(GRID_ALARMS_SELECT).IsColumnPrintable = False
      .Column(GRID_ALARMS_SELECT).HighLightWhenSelected = False

      .Column(GRID_ALARMS_DATETIME).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(212)
      .Column(GRID_ALARMS_DATETIME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268)
      .Column(GRID_ALARMS_DATETIME).Width = GRID_WIDTH_DATE_LONG
      .Column(GRID_ALARMS_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_ALARMS_DATETIME).HighLightWhenSelected = False

      .Column(GRID_ALARMS_CODE).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(212)
      .Column(GRID_ALARMS_CODE).Header(1).Text = GLB_NLS_GUI_ALARMS.GetString(463)
      .Column(GRID_ALARMS_CODE).Width = GRID_WIDTH_DATE_LONG
      .Column(GRID_ALARMS_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_ALARMS_CODE).HighLightWhenSelected = False

      .Column(GRID_ALARMS_DESCRIPTION).Header(0).Text = GLB_NLS_GUI_ALARMS.GetString(212)
      .Column(GRID_ALARMS_DESCRIPTION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5711)
      .Column(GRID_ALARMS_DESCRIPTION).Width = WIDTH_ALARM_DESCRIPTION
      .Column(GRID_ALARMS_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_ALARMS_DESCRIPTION).HighLightWhenSelected = False
    End With

  End Sub ' GUI_StyleSheetMaxMeters

  Private Sub Button_Search()
    'Dim _rows_modified() As DataRow
    Dim _today_opening As Date
    Dim _date_from As DateTime
    Dim _date_to As DateTime
    Dim _meters_selected As String
    Dim _provider_selected As Long()

    _today_opening = WSI.Common.Misc.TodayOpening()

    m_selected_row = -1

    _date_from = dtp_from.Value
    _date_from = New DateTime(_date_from.Year, _date_from.Month, _date_from.Day, _today_opening.Hour, _today_opening.Minute, 0)
    _date_to = IIf(Me.dtp_to.Checked, dtp_to.Value, _today_opening.AddDays(1))
    _date_to = New DateTime(_date_to.Year, _date_to.Month, _date_to.Day, _today_opening.Hour, _today_opening.Minute, 0)

    Call SetLabelsInfo()

    If _date_from > _date_to Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(136), _
                    mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Return
    End If

    _meters_selected = ""
    If uc_checked_list_groups.SelectedIndexesListLevel2.Length > 0 Then
      _meters_selected = uc_checked_list_groups.SelectedIndexesListLevel2
    End If
    m_cls_terminal_sas_meters.MetersSelected = _meters_selected

    'Date filters
    m_cls_terminal_sas_meters.DateFrom = _date_from
    m_cls_terminal_sas_meters.DateTo = _date_to

    'Filtro
    If rb_all.Checked Then
      m_cls_terminal_sas_meters.FilterTypeValue = cls_terminal_sas_meters.FilterType.All
      m_options = rb_all.Text
    End If

    If rb_with_values.Checked Then
      m_cls_terminal_sas_meters.FilterTypeValue = cls_terminal_sas_meters.FilterType.WithValue
      m_options = rb_with_values.Text
    End If

    If rb_without_values.Checked Then
      m_cls_terminal_sas_meters.FilterTypeValue = cls_terminal_sas_meters.FilterType.WithoutValue
      m_options = rb_without_values.Text
    End If

    'Terminal Filter
    m_terminals = Me.uc_provider.GetTerminalReportText()

    m_meters = Me.uc_checked_list_groups.SelectedValuesList()

    _provider_selected = uc_provider.GetTerminalIdListSelected()
    m_cls_terminal_sas_meters.TerminalsSelected = _provider_selected
    
    m_cls_terminal_sas_meters.LoadMeters()
    m_cls_terminal_sas_meters.ReadTerminalSasMeters()

    GUI_StyleSheet()

    SetupRow()

  End Sub

  Private Sub SaveChanges()
    Dim _rows_to_update As DataTable
    Dim _old_rows As DataTable
    Dim _dt_copy As DataTable

    _rows_to_update = Nothing
    _old_rows = Nothing

    If (Not TestChanged(m_cls_terminal_sas_meters.TerminalSasMeters, False, _rows_to_update, _old_rows)) Then
      ' No Changes --> Success
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1393), _
                mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)

      Return
    End If

    ' Audit
    If Not AuditChanges(_rows_to_update, _old_rows) Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(103), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
      Return
    End If

    ' Save
    If Not m_cls_terminal_sas_meters.Db_UpdateTerminalSasMeters(_rows_to_update) Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(103), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)
      Return
    End If

    _dt_copy = m_cls_terminal_sas_meters.TerminalSasMeters.Copy()
    m_cls_terminal_sas_meters.TerminalSasMeters = _dt_copy.Clone()

    For i As Int32 = 0 To _dt_copy.Rows.Count - 1
      m_cls_terminal_sas_meters.TerminalSasMeters.ImportRow(_dt_copy.Rows(i))
    Next
    m_cls_terminal_sas_meters.TerminalSasMeters.AcceptChanges()

    Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(108), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO)

  End Sub

  Private Function TestChanged(ByVal Datas As DataTable, ByVal TestOnly As Boolean, ByRef RowsToUpdate As DataTable, ByRef OldRows As DataTable) As Boolean

    Dim _any_changed As Boolean
    Dim _unchanged_found As Boolean
    Dim _modified_rows() As DataRow

    RowsToUpdate = New DataTable()
    OldRows = New DataTable()

    If Datas Is Nothing Then

      Return False
    End If

    If Not TestOnly Then
      RowsToUpdate = CreateColumnsDataTable()
      OldRows = CreateColumnsDataTable()
    End If

    _modified_rows = Datas.Select("", "", DataViewRowState.ModifiedCurrent)

    _any_changed = False
    _unchanged_found = False

    For Each _row As DataRow In _modified_rows
      For Each _column As DataColumn In _row.Table.Columns
        If Not _row.IsNull(_column) Then
          If (Not _row.IsNull(_column) AndAlso _row.Item(_column, DataRowVersion.Original) Is DBNull.Value) OrElse _
              (_row.Item(_column) <> _row.Item(_column, DataRowVersion.Original)) Then
            _any_changed = True
            If (TestOnly) Then

              Return True
            Else
              FillDatatableToUpdate(_row, _column, RowsToUpdate, OldRows)
            End If
          End If
        End If
      Next
    Next

    If (TestOnly) Then
      Return False
    End If

    Return _any_changed

  End Function

  Private Sub FillDatatableToUpdate(ByVal Row As DataRow, ByVal Column As DataColumn, ByRef RowsToUpdate As DataTable, ByRef OldRows As DataTable)
    Dim _meter_code As String
    Dim _selected_rows() As DataRow
    Dim _selected_old_rows() As DataRow
    Dim _filter As String
    Dim _base_time As DateTime
    Dim _terminal_id As Int32
    Dim _terminal_name As String

    _meter_code = Column.ColumnName.Substring(1)

    _base_time = Row(GRID_DATETIME_LONG)
    _terminal_id = Row(GRID_TERMINAL_ID)
    _terminal_name = Row(GRID_TERMINAL_NAME)
    _filter = String.Format("BASE_TIME = {0} AND TERMINAL_ID = {1} AND METER_CODE = {2}", FormatDateToStringDataTableFilter(_base_time), _terminal_id, _meter_code)

    _selected_rows = RowsToUpdate.Select(_filter)
    _selected_old_rows = OldRows.Select(_filter)

    If _selected_rows.Length = 0 Then
      RowsToUpdate.Rows.Add(_base_time, _terminal_id, _terminal_name, _meter_code)
      OldRows.Rows.Add(_base_time, _terminal_id, _terminal_name, _meter_code)
      _selected_rows = RowsToUpdate.Select(_filter)
      _selected_old_rows = OldRows.Select(_filter)
    End If

    Select Case Column.ColumnName.Substring(0, 1)
      Case "A"
        _selected_rows(0).Item(4) = Row.Item(Column)
        _selected_old_rows(0).Item(4) = Row.Item(Column, DataRowVersion.Original)
      Case "B"
        _selected_rows(0).Item(5) = Row.Item(Column)
        _selected_old_rows(0).Item(5) = Row.Item(Column, DataRowVersion.Original)
      Case "C"
        _selected_rows(0).Item(6) = Row.Item(Column)
        _selected_old_rows(0).Item(6) = Row.Item(Column, DataRowVersion.Original)
    End Select

  End Sub

  Private Function CreateColumnsDataTable() As DataTable
    Dim _dt As DataTable

    _dt = New DataTable()
    With _dt
      .Columns.Add("BASE_TIME", System.Type.GetType("System.DateTime"))
      .Columns.Add("TERMINAL_ID", System.Type.GetType("System.Int32"))
      .Columns.Add("TERMINAL_NAME", System.Type.GetType("System.String"))
      .Columns.Add("METER_CODE", System.Type.GetType("System.Int32"))
      .Columns.Add("INIT_VALUE", System.Type.GetType("System.Decimal"))
      .Columns.Add("FINAL_VALUE", System.Type.GetType("System.Decimal"))
      .Columns.Add("INCREMENT_VALUE", System.Type.GetType("System.Decimal"))
    End With

    Return _dt
  End Function

  Private Function AuditChanges(ByVal ModifiedRows As DataTable, ByVal OldRows As DataTable) As Boolean
    Dim _old_value As CLASS_AUDITOR_DATA
    Dim _new_value As CLASS_AUDITOR_DATA
    Dim _base_time As DateTime
    Dim _sb As StringBuilder
    Dim _terminal_name As String

    _base_time = Nothing
    _sb = New StringBuilder()

    _terminal_name = ""
    _old_value = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
    _new_value = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)

    For _idx As Int32 = 0 To ModifiedRows.Rows.Count - 1

      If _terminal_name <> ModifiedRows.Rows(_idx)(DT_TERMINAL_NAME) Then
        If _terminal_name <> "" Then
          If Not _new_value.Notify(GLB_CurrentUser.GuiId, _
                                           GLB_CurrentUser.Id, _
                                           GLB_CurrentUser.Name, _
                                           CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                           0, _
                                           _old_value) Then

            ' Error!!
            Return False
          End If
        End If
        _terminal_name = ModifiedRows.Rows(_idx)(DT_TERMINAL_NAME)
        _old_value = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
        _new_value = New CLASS_AUDITOR_DATA(AUDIT_CODE_TERMINALS)
      End If
      _old_value.SetName(0, "", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5727) + " " + _terminal_name)
      _new_value.SetName(0, "", GLB_NLS_GUI_PLAYER_TRACKING.GetString(5727) + " " + _terminal_name)

      OldRows.Rows(_idx)(DT_METER_CODE) = ModifiedRows.Rows(_idx)(DT_METER_CODE)

      CreateAudit(OldRows.Rows(_idx), _old_value)
      CreateAudit(ModifiedRows.Rows(_idx), _new_value)
    Next

    If _terminal_name <> "" AndAlso Not _new_value.Notify(GLB_CurrentUser.GuiId, _
                                         GLB_CurrentUser.Id, _
                                         GLB_CurrentUser.Name, _
                                         CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.UPDATE, _
                                         0, _
                                         _old_value) Then

      ' Error!!
      Return False
    End If

    Return True

  End Function

  Private Sub CreateAudit(ByVal Row As DataRow, ByVal AuditorData As CLASS_AUDITOR_DATA)

    Dim _meter_code As Int32
    Dim _meter_name As String
    Dim _initial As String
    Dim _final As String
    Dim _increment As String
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    _meter_code = Row.Item(DT_METER_CODE)
    _meter_name = m_cls_terminal_sas_meters.MeterName(_meter_code)

    _initial = ""
    _final = ""
    _increment = ""
    If Not Row.IsNull(DT_INIT_VALUE) Then
      _initial = FormatMeterValue(_meter_code, Row.Item(DT_INIT_VALUE), Nothing)
      _sb.Append(";" + GLB_NLS_GUI_PLAYER_TRACKING.GetString(4794) + ": " + _initial)
    End If

    If Not Row.IsNull(DT_FINAL_VALUE) Then
      _final = FormatMeterValue(_meter_code, Row.Item(DT_FINAL_VALUE), Nothing)
      _sb.Append(";" + GLB_NLS_GUI_PLAYER_TRACKING.GetString(4795) + ": " + _final)
    End If

    If Not Row.IsNull(DT_INCREMENT_VALUE) Then
      _increment = FormatMeterValue(_meter_code, Row.Item(DT_INCREMENT_VALUE), Nothing)
      _sb.Append(";" + GLB_NLS_GUI_PLAYER_TRACKING.GetString(4796) + ": " + _increment)
    End If

    AuditorData.SetField(0, _sb.ToString().TrimStart(";"), "(" + GUI_FormatDate(Row(DT_DATETIME), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS) + ") " + _meter_name)

  End Sub

  Private Function FormatDateToStringDataTableFilter(ByVal Day As DateTime) As String
    Return String.Format(Global.System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat, "#{0}#", Day)
  End Function

  ' PURPOSE: Sets the defaul date values to the daily_session_selector control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim _today_opening As Date

    m_selected_row = -1

    _today_opening = WSI.Common.Misc.TodayOpening()

    ' Dates
    dtp_from.Value = _today_opening
    dtp_to.Value = _today_opening.AddDays(1)

    dtp_to.Checked = False

    Call m_cls_terminal_sas_meters.LoadGroupMeters()

    ' Groups
    Call uc_checked_list_groups.Clear()
    Call FillGroups()
    Call Me.uc_checked_list_groups.SetDefaultValue(True)
    Call Me.uc_checked_list_groups.CollapseAll()

    ' Terminals
    Call Me.uc_provider.SetDefaultValues()

    Me.chk_terminal_location.Checked = False

    Me.rb_all.Checked = True

  End Sub ' SetDefaultValues

  ' PURPOSE: Fill uc_checked_list control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub FillGroups()
    Dim _group_name As String
    Dim _exists As Boolean

    ' Fill CheckedList control with groups
    Call Me.uc_checked_list_groups.Clear()
    Call Me.uc_checked_list_groups.ReDraw(False)

    For Each _group As cls_terminal_sas_meters.MetersGroup In m_cls_terminal_sas_meters.GroupMeterList
      _group_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_exists, 3080 + _group.Id - 10000)
      If Not _exists Then
        _group_name = _group.GroupName
      End If
      uc_checked_list_groups.Add(1, _group.Id, _group_name)
      For Each _meter As cls_terminal_sas_meters.Meter In _group.MeterList
        uc_checked_list_groups.Add(2, _meter.Code, "  " + VisibleMeterCode(_meter.HexCode) + "-" + _meter.CatalogName)
      Next
    Next

    If Me.uc_checked_list_groups.Count > 0 Then
      Call Me.uc_checked_list_groups.ColumnWidth(3, 6500)
      Call Me.uc_checked_list_groups.CurrentRow(0)
      Call Me.uc_checked_list_groups.ReDraw(True)
    End If

  End Sub ' FillGroups

  Private Function VisibleMeterCode(ByVal HexCode As String) As String
    Dim _code As String

    _code = HexCode.PadLeft(5, "0").Trim()
    _code = _code.Substring(_code.Length - VISIBLE_CODE_LENGTH)

    Return _code
  End Function

  Private Sub SetupRow()
    Dim _num_row As Int32
    Dim _idx As Int32
    Dim _terminal_data As List(Of Object)

    RemoveHandler dg_meters.RowSelectedEvent, AddressOf dg_meters_RowSelectedEvent

    _idx = 0
    For Each _row As DataRow In m_cls_terminal_sas_meters.TerminalSasMeters.Rows
      With dg_meters
        _num_row = .AddRow()

        .Cell(_num_row, GRID_DATETIME_LONG).Value = GUI_FormatDate(_row(SQL_DATETIME), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
        .Cell(_num_row, GRID_DATETIME).Value = GUI_FormatDate(_row(SQL_DATETIME), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
        .Cell(_num_row, GRID_TERMINAL_ID).Value = _row(SQL_TERMINAL_ID)

        _terminal_data = TerminalReport.GetReportDataList(_row(SQL_TERMINAL_ID), m_terminal_report_type)

        If _terminal_data.Count > 0 Then
          .Cell(_num_row, GRID_COLUMN_TERMINAL_PROVIDER).Value = _terminal_data(0)
          _row(SQL_COLUMN_TERMINAL_PROVIDER) = _terminal_data(0)
          .Cell(_num_row, GRID_TERMINAL_NAME).Value = _terminal_data(1)
          _row(SQL_COLUMN_TERMINAL_NAME) = _terminal_data(1)

          If chk_terminal_location.Checked Then
            .Cell(_num_row, GRID_COLUMN_TERMINAL_AREA).Value = _terminal_data(2)
            .Cell(_num_row, GRID_COLUMN_TERMINAL_BANK).Value = _terminal_data(3)
            .Cell(_num_row, GRID_COLUMN_TERMINAL_POSITION).Value = IIf(IsDBNull(_terminal_data(4)), "", _terminal_data(4))

            _row(SQL_COLUMN_TERMINAL_AREA) = _terminal_data(2)
            _row(SQL_COLUMN_TERMINAL_BANK) = _terminal_data(3)
            _row(SQL_COLUMN_TERMINAL_POSITION) = _terminal_data(4)
          End If

        Else
          .Cell(_num_row, GRID_COLUMN_TERMINAL_PROVIDER).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4650)
          .Cell(_num_row, GRID_TERMINAL_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4650)
          .Cell(_num_row, GRID_COLUMN_TERMINAL_AREA).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4650)
          .Cell(_num_row, GRID_COLUMN_TERMINAL_BANK).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4650)
          .Cell(_num_row, GRID_COLUMN_TERMINAL_POSITION).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4650)

        End If
        _idx = 0
        For Each _meter As cls_terminal_sas_meters.Meter In m_cls_terminal_sas_meters.MeterList
          If Not IsDBNull(_row(SQL_FIRST_METER + _idx)) Then
            .Cell(_num_row, GRID_FIRST_METER + _idx).Value = FormatMeterValue(_meter.Code, _row(SQL_FIRST_METER + _idx), Nothing)
          End If
          _idx += 1

          If Not IsDBNull(_row(SQL_FIRST_METER + _idx)) Then
            .Cell(_num_row, GRID_FIRST_METER + _idx).Value = FormatMeterValue(_meter.Code, _row(SQL_FIRST_METER + _idx), Nothing)
          End If
          _idx += 1

          If Not IsDBNull(_row(SQL_FIRST_METER + _idx)) Then
            .Cell(_num_row, GRID_FIRST_METER + _idx).Value = FormatMeterValue(_meter.Code, _row(SQL_FIRST_METER + _idx), Nothing)
          End If
          _idx += 1
        Next
      End With
    Next

    m_cls_terminal_sas_meters.TerminalSasMeters.AcceptChanges()

    AddHandler dg_meters.RowSelectedEvent, AddressOf dg_meters_RowSelectedEvent

  End Sub

  Private Sub SetLabelsInfo()
    gb_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5790) ' Contador del terminal
    Me.lbl_meter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5791) ' Contador:
    Me.lbl_max_value.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5792) ' Valor m�ximo:
    Me.lbl_max_increment.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5793) ' * Incremento m�ximo aceptado:
    Me.lbl_information.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5794) ' * No aplica a las modificaciones manuales de incrementos. Valor m�ximo antes de descartar de forma autom�tica todo el incremento.
  End Sub

  Private Function FormatMeterValue(ByVal MeterCode As Int32, ByVal MeterValue As Double, ByRef EnumFormat As GUI_Controls.CLASS_FILTER.ENUM_FORMAT) As String
    Dim _value As String

    Select Case MeterCode
      Case &H0, &H1, &H2, &H3, &H4, &H8, &H9, &HA, &HB, &HC, &HD, &HE _
         , &HF, &H10, &H15, &H16, &H17, &H18, &H19, &H1A, &H1B, &H1C, &H1D, &H1E _
         , &H1F, &H20, &H21, &H22, &H23, &H24, &H27, &H28, &H29, &H2A, &H2B, &H2C _
         , &H2D, &H2E, &H2F, &H30, &H31, &H32, &H33, &H34, &H3F, &H58, &H63, &H6E _
         , &H80, &H82, &H84, &H86, &H88, &H8A, &H8C, &H8E, &H90, &H92, &HA0, &HA2 _
         , &HA4, &HA6, &HA8, &HAA, &HAC, &HAE, &HB0, &HB8, &HBA, &HBC, &H1000

        _value = GUI_FormatCurrency(MeterValue / 100.0, 2)
        EnumFormat = FORMAT_MONEY
      Case Else

        _value = GUI_FormatNumber(MeterValue, 0)
        EnumFormat = FORMAT_NUMBER
    End Select

    Return _value
  End Function

#End Region

#Region "Events"

  Private Sub CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32)
    Dim _base_hour As DateTime
    Dim _terminal_id As Int64
    Dim _provider_id As String
    Dim _filter As String
    Dim _meter As Decimal
    Dim _meter_code As Int32
    Dim _enum_format As GUI_Controls.CLASS_FILTER.ENUM_FORMAT

    If (Row < 0 Or dg_meters.NumRows < 0) Then
      Return
    End If

    RemoveHandler dg_meters.CellDataChangedEvent, AddressOf CellDataChangedEvent

    _base_hour = dg_meters.Cell(Row, GRID_DATETIME_LONG).Value
    _terminal_id = dg_meters.Cell(Row, GRID_TERMINAL_ID).Value
    _provider_id = dg_meters.Cell(Row, GRID_COLUMN_TERMINAL_PROVIDER).Value

    _filter = String.Format("TSMH_DATETIME = {0} AND TSMH_TERMINAL_ID = {1} AND TE_PROVIDER = '{2}'", FormatDateToStringDataTableFilter(_base_hour), _terminal_id, _provider_id)

    If String.IsNullOrEmpty(dg_meters.Cell(Row, Column).Value) Then
      _meter = 0
    Else
      _meter = dg_meters.Cell(Row, Column).Value
    End If

    _meter_code = m_cls_terminal_sas_meters.TerminalSasMeters.Columns(Column).ColumnName.Substring(1)

    FormatMeterValue(_meter_code, 0, _enum_format)

    If _enum_format = FORMAT_MONEY Then
      If _meter > 99999999.99 And _
        Not m_cls_terminal_sas_meters.TerminalSasMeters.Rows(Row).Item(Column, DataRowVersion.Original) Is DBNull.Value Then
        'Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2879), ENUM_MB_TYPE.MB_TYPE_WARNING, , , "$99,999,999.99", _
        '                dg_meters.Column(Column).Header(0).Text & " - " & dg_meters.Column(Column).Header(1).Text)
        dg_meters.Cell(Row, Column).Value = GUI_FormatCurrency(m_cls_terminal_sas_meters.TerminalSasMeters.Rows(Row).Item(Column, DataRowVersion.Original) / 100.0, 2)
      Else
        m_cls_terminal_sas_meters.TerminalSasMeters.Select(_filter)(0).Item(Column) = _meter * 100
      End If
    Else
      m_cls_terminal_sas_meters.TerminalSasMeters.Select(_filter)(0).Item(Column) = _meter
    End If

    AddHandler dg_meters.CellDataChangedEvent, AddressOf CellDataChangedEvent
  End Sub

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If
  End Sub

  Private Sub dg_meters_RowSelectedEvent(ByVal SelectedRow As System.Int32)
    Dim _terminal_id As Int32
    Dim _dt_alarms As DataTable
    Dim _num_row As Int32
    Dim _base_hour As DateTime

    If dg_meters.NumRows <= 0 Then
      Return
    End If

    If m_selected_row = SelectedRow Then
      Return
    End If

    m_selected_row = SelectedRow

    Call SetLabelsInfo()

    If String.IsNullOrEmpty(Me.dg_meters.Cell(SelectedRow, GRID_TERMINAL_ID).Value) Then
      Return
    End If

    _base_hour = dg_meters.Cell(SelectedRow, GRID_DATETIME).Value
    _terminal_id = Me.dg_meters.Cell(SelectedRow, GRID_TERMINAL_ID).Value
    _dt_alarms = Me.m_cls_terminal_sas_meters.GetAlarms(_terminal_id, _base_hour)

    With Me.dg_alarms
      .Clear()
      For Each _alarm As DataRow In _dt_alarms.Rows
        _num_row = .AddRow()
        .Cell(_num_row, GRID_ALARMS_DATETIME).Value = GUI_FormatDate(_alarm(SQL_ALARMS_DATETIME), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
        .Cell(_num_row, GRID_ALARMS_CODE).Value = _alarm(SQL_ALARMS_CODE)
        .Cell(_num_row, GRID_ALARMS_DESCRIPTION).Value = _alarm(SQL_ALARMS_DESCRIPTION)
      Next

    End With
  End Sub

  Private Sub dg_meters_BeforeStartEditionEvent(ByVal Row As System.Int32, ByVal Column As System.Int32, ByRef EditionCanceled As System.Boolean) Handles dg_meters.BeforeStartEditionEvent
    Dim _terminal_id As Int32
    Dim _meter_code As Int32
    Dim _max_units_increment As Long

    Dim _list_max_meter As List(Of cls_terminal_sas_meters.MaxMeter)

    If Column < GRID_FIRST_METER Then

      Return
    End If

    Call SetLabelsInfo()

    _terminal_id = Me.dg_meters.Cell(Row, GRID_TERMINAL_ID).Value

    gb_info.Text += Me.dg_meters.Cell(Row, GRID_TERMINAL_NAME).Value
    Me.lbl_meter.Text += Me.dg_meters.Column(Column).Header.Text

    _meter_code = m_cls_terminal_sas_meters.TerminalSasMeters.Columns(Column).ColumnName.Substring(1)

    If Me.m_cls_terminal_sas_meters.TerminalMaxMeters.ContainsKey(_terminal_id) Then
      _list_max_meter = Me.m_cls_terminal_sas_meters.TerminalMaxMeters(_terminal_id)
      For _idx As Int32 = 0 To _list_max_meter.Count
        If _list_max_meter(_idx).Code = _meter_code Then
          Me.lbl_max_value.Text += FormatMeterValue(_meter_code, _list_max_meter(_idx).MaxValue, Nothing)
          Exit For
        End If
      Next
    End If

    _max_units_increment = 0
    Call SAS_Meter.GetBigIncrement(_meter_code, _max_units_increment, 0)
    Me.lbl_max_increment.Text += FormatMeterValue(_meter_code, _max_units_increment, Nothing)
  End Sub

#End Region ' Events

End Class