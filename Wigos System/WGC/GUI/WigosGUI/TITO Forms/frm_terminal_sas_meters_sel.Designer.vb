<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminal_sas_meters_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_terminal_sas_meters_sel))
    Me.pnl_filters_standard = New System.Windows.Forms.Panel()
    Me.uc_sites_sel = New GUI_Controls.uc_sites_sel()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.chk_hide_no_activity = New System.Windows.Forms.CheckBox()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.uc_checked_list_groups = New GUI_Controls.uc_checked_list()
    Me.gb_order_by = New System.Windows.Forms.GroupBox()
    Me.opt_terminal = New System.Windows.Forms.RadioButton()
    Me.opt_date = New System.Windows.Forms.RadioButton()
    Me.uc_provider = New GUI_Controls.uc_provider()
    Me.gb_counter_type = New System.Windows.Forms.GroupBox()
    Me.chk_denom_change = New System.Windows.Forms.CheckBox()
    Me.chk_first_time = New System.Windows.Forms.CheckBox()
    Me.opt_types = New System.Windows.Forms.RadioButton()
    Me.opt_currents = New System.Windows.Forms.RadioButton()
    Me.chk_rollover = New System.Windows.Forms.CheckBox()
    Me.chk_reset = New System.Windows.Forms.CheckBox()
    Me.chk_daily = New System.Windows.Forms.CheckBox()
    Me.chk_hourly = New System.Windows.Forms.CheckBox()
    Me.pnl_filters_simplified = New System.Windows.Forms.Panel()
    Me.gb_report_simplified = New System.Windows.Forms.GroupBox()
    Me.chk_show_events_simplified = New System.Windows.Forms.CheckBox()
    Me.opt_daily_one_day_simplified = New System.Windows.Forms.RadioButton()
    Me.opt_daily_one_month_simplified = New System.Windows.Forms.RadioButton()
    Me.opt_hourly_simplified = New System.Windows.Forms.RadioButton()
    Me.chk_terminal_location_simplified = New System.Windows.Forms.CheckBox()
    Me.chk_hide_no_activity_simplified = New System.Windows.Forms.CheckBox()
    Me.gb_date_simplified = New System.Windows.Forms.GroupBox()
    Me.dtp_to_simplified = New GUI_Controls.uc_date_picker()
    Me.dtp_from_simplified = New GUI_Controls.uc_date_picker()
    Me.uc_checked_list_groups_simplified = New GUI_Controls.uc_checked_list()
    Me.uc_provider_simplified = New GUI_Controls.uc_provider()
    Me.tf_last_update = New GUI_Controls.uc_text_field()
    Me.tmr_monitor = New System.Windows.Forms.Timer(Me.components)
    Me.gb_mode = New System.Windows.Forms.GroupBox()
    Me.opt_monitor_mode = New System.Windows.Forms.RadioButton()
    Me.opt_history_mode = New System.Windows.Forms.RadioButton()
    Me.gb_currency = New System.Windows.Forms.GroupBox()
    Me.opt_credits = New System.Windows.Forms.RadioButton()
    Me.opt_currency = New System.Windows.Forms.RadioButton()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.pnl_filters_standard.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gb_order_by.SuspendLayout()
    Me.gb_counter_type.SuspendLayout()
    Me.pnl_filters_simplified.SuspendLayout()
    Me.gb_report_simplified.SuspendLayout()
    Me.gb_date_simplified.SuspendLayout()
    Me.gb_mode.SuspendLayout()
    Me.gb_currency.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_currency)
    Me.panel_filter.Controls.Add(Me.pnl_filters_standard)
    Me.panel_filter.Controls.Add(Me.tf_last_update)
    Me.panel_filter.Controls.Add(Me.gb_mode)
    Me.panel_filter.Controls.Add(Me.pnl_filters_simplified)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1626, 209)
    Me.panel_filter.Controls.SetChildIndex(Me.pnl_filters_simplified, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_mode, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.tf_last_update, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.pnl_filters_standard, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_currency, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 213)
    Me.panel_data.Size = New System.Drawing.Size(1626, 459)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1620, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1620, 4)
    '
    'pnl_filters_standard
    '
    Me.pnl_filters_standard.Controls.Add(Me.uc_sites_sel)
    Me.pnl_filters_standard.Controls.Add(Me.chk_terminal_location)
    Me.pnl_filters_standard.Controls.Add(Me.chk_hide_no_activity)
    Me.pnl_filters_standard.Controls.Add(Me.gb_date)
    Me.pnl_filters_standard.Controls.Add(Me.uc_checked_list_groups)
    Me.pnl_filters_standard.Controls.Add(Me.gb_order_by)
    Me.pnl_filters_standard.Controls.Add(Me.uc_provider)
    Me.pnl_filters_standard.Controls.Add(Me.gb_counter_type)
    Me.pnl_filters_standard.Location = New System.Drawing.Point(6, 6)
    Me.pnl_filters_standard.Name = "pnl_filters_standard"
    Me.pnl_filters_standard.Size = New System.Drawing.Size(1199, 200)
    Me.pnl_filters_standard.TabIndex = 11
    '
    'uc_sites_sel
    '
    Me.uc_sites_sel.Location = New System.Drawing.Point(154, 135)
    Me.uc_sites_sel.MultiSelect = False
    Me.uc_sites_sel.Name = "uc_sites_sel"
    Me.uc_sites_sel.ShowIsoCode = False
    Me.uc_sites_sel.ShowMultisiteRow = False
    Me.uc_sites_sel.Size = New System.Drawing.Size(100, 62)
    Me.uc_sites_sel.TabIndex = 7
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(392, 183)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(296, 16)
    Me.chk_terminal_location.TabIndex = 5
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'chk_hide_no_activity
    '
    Me.chk_hide_no_activity.AutoSize = True
    Me.chk_hide_no_activity.Location = New System.Drawing.Point(156, 183)
    Me.chk_hide_no_activity.Name = "chk_hide_no_activity"
    Me.chk_hide_no_activity.Size = New System.Drawing.Size(114, 17)
    Me.chk_hide_no_activity.TabIndex = 3
    Me.chk_hide_no_activity.Text = "xHidenoActivity"
    Me.chk_hide_no_activity.UseVisualStyleBackColor = True
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(148, 5)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(240, 80)
    Me.gb_date.TabIndex = 1
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 43)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(228, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 18)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(228, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'uc_checked_list_groups
    '
    Me.uc_checked_list_groups.GroupBoxText = "xCheckedList"
    Me.uc_checked_list_groups.Location = New System.Drawing.Point(392, 5)
    Me.uc_checked_list_groups.m_resize_width = 421
    Me.uc_checked_list_groups.multiChoice = True
    Me.uc_checked_list_groups.Name = "uc_checked_list_groups"
    Me.uc_checked_list_groups.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list_groups.SelectedIndexesList = ""
    Me.uc_checked_list_groups.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list_groups.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_list_groups.SelectedValuesList = ""
    Me.uc_checked_list_groups.SelectedValuesListLevel2 = ""
    Me.uc_checked_list_groups.SetLevels = 2
    Me.uc_checked_list_groups.Size = New System.Drawing.Size(421, 173)
    Me.uc_checked_list_groups.TabIndex = 4
    Me.uc_checked_list_groups.ValuesArray = New String(-1) {}
    '
    'gb_order_by
    '
    Me.gb_order_by.Controls.Add(Me.opt_terminal)
    Me.gb_order_by.Controls.Add(Me.opt_date)
    Me.gb_order_by.Location = New System.Drawing.Point(148, 91)
    Me.gb_order_by.Name = "gb_order_by"
    Me.gb_order_by.Size = New System.Drawing.Size(240, 46)
    Me.gb_order_by.TabIndex = 2
    Me.gb_order_by.TabStop = False
    Me.gb_order_by.Text = "xOrder By"
    '
    'opt_terminal
    '
    Me.opt_terminal.AutoSize = True
    Me.opt_terminal.Location = New System.Drawing.Point(121, 19)
    Me.opt_terminal.Name = "opt_terminal"
    Me.opt_terminal.Size = New System.Drawing.Size(81, 17)
    Me.opt_terminal.TabIndex = 1
    Me.opt_terminal.TabStop = True
    Me.opt_terminal.Text = "xTerminal"
    Me.opt_terminal.UseVisualStyleBackColor = True
    '
    'opt_date
    '
    Me.opt_date.AutoSize = True
    Me.opt_date.Location = New System.Drawing.Point(18, 19)
    Me.opt_date.Name = "opt_date"
    Me.opt_date.Size = New System.Drawing.Size(59, 17)
    Me.opt_date.TabIndex = 0
    Me.opt_date.TabStop = True
    Me.opt_date.Text = "xDate"
    Me.opt_date.UseVisualStyleBackColor = True
    '
    'uc_provider
    '
    Me.uc_provider.Location = New System.Drawing.Point(817, 0)
    Me.uc_provider.Name = "uc_provider"
    Me.uc_provider.Size = New System.Drawing.Size(381, 187)
    Me.uc_provider.TabIndex = 7
    Me.uc_provider.TerminalListHasValues = False
    '
    'gb_counter_type
    '
    Me.gb_counter_type.Controls.Add(Me.chk_denom_change)
    Me.gb_counter_type.Controls.Add(Me.chk_first_time)
    Me.gb_counter_type.Controls.Add(Me.opt_types)
    Me.gb_counter_type.Controls.Add(Me.opt_currents)
    Me.gb_counter_type.Controls.Add(Me.chk_rollover)
    Me.gb_counter_type.Controls.Add(Me.chk_reset)
    Me.gb_counter_type.Controls.Add(Me.chk_daily)
    Me.gb_counter_type.Controls.Add(Me.chk_hourly)
    Me.gb_counter_type.Location = New System.Drawing.Point(0, 5)
    Me.gb_counter_type.Name = "gb_counter_type"
    Me.gb_counter_type.Size = New System.Drawing.Size(143, 194)
    Me.gb_counter_type.TabIndex = 0
    Me.gb_counter_type.TabStop = False
    Me.gb_counter_type.Text = "xCounters"
    '
    'chk_denom_change
    '
    Me.chk_denom_change.Location = New System.Drawing.Point(21, 171)
    Me.chk_denom_change.Name = "chk_denom_change"
    Me.chk_denom_change.Size = New System.Drawing.Size(115, 17)
    Me.chk_denom_change.TabIndex = 7
    Me.chk_denom_change.Text = "xDenomChange"
    Me.chk_denom_change.UseVisualStyleBackColor = True
    '
    'chk_first_time
    '
    Me.chk_first_time.AutoSize = True
    Me.chk_first_time.Location = New System.Drawing.Point(21, 149)
    Me.chk_first_time.Name = "chk_first_time"
    Me.chk_first_time.Size = New System.Drawing.Size(85, 17)
    Me.chk_first_time.TabIndex = 6
    Me.chk_first_time.Text = "xFirstTime"
    Me.chk_first_time.UseVisualStyleBackColor = True
    '
    'opt_types
    '
    Me.opt_types.AutoSize = True
    Me.opt_types.Location = New System.Drawing.Point(8, 41)
    Me.opt_types.Name = "opt_types"
    Me.opt_types.Size = New System.Drawing.Size(78, 17)
    Me.opt_types.TabIndex = 1
    Me.opt_types.TabStop = True
    Me.opt_types.Text = "xBy Type"
    Me.opt_types.UseVisualStyleBackColor = True
    '
    'opt_currents
    '
    Me.opt_currents.AutoSize = True
    Me.opt_currents.Location = New System.Drawing.Point(8, 18)
    Me.opt_currents.Name = "opt_currents"
    Me.opt_currents.Size = New System.Drawing.Size(82, 17)
    Me.opt_currents.TabIndex = 0
    Me.opt_currents.TabStop = True
    Me.opt_currents.Text = "xCurrents"
    Me.opt_currents.UseVisualStyleBackColor = True
    '
    'chk_rollover
    '
    Me.chk_rollover.AutoSize = True
    Me.chk_rollover.Location = New System.Drawing.Point(21, 127)
    Me.chk_rollover.Name = "chk_rollover"
    Me.chk_rollover.Size = New System.Drawing.Size(80, 17)
    Me.chk_rollover.TabIndex = 5
    Me.chk_rollover.Text = "xRollover"
    Me.chk_rollover.UseVisualStyleBackColor = True
    '
    'chk_reset
    '
    Me.chk_reset.AutoSize = True
    Me.chk_reset.Location = New System.Drawing.Point(21, 105)
    Me.chk_reset.Name = "chk_reset"
    Me.chk_reset.Size = New System.Drawing.Size(65, 17)
    Me.chk_reset.TabIndex = 4
    Me.chk_reset.Text = "xReset"
    Me.chk_reset.UseVisualStyleBackColor = True
    '
    'chk_daily
    '
    Me.chk_daily.AutoSize = True
    Me.chk_daily.Location = New System.Drawing.Point(21, 83)
    Me.chk_daily.Name = "chk_daily"
    Me.chk_daily.Size = New System.Drawing.Size(62, 17)
    Me.chk_daily.TabIndex = 3
    Me.chk_daily.Text = "xDaily"
    Me.chk_daily.UseVisualStyleBackColor = True
    '
    'chk_hourly
    '
    Me.chk_hourly.AutoSize = True
    Me.chk_hourly.Location = New System.Drawing.Point(21, 61)
    Me.chk_hourly.Name = "chk_hourly"
    Me.chk_hourly.Size = New System.Drawing.Size(70, 17)
    Me.chk_hourly.TabIndex = 2
    Me.chk_hourly.Text = "xHourly"
    Me.chk_hourly.UseVisualStyleBackColor = True
    '
    'pnl_filters_simplified
    '
    Me.pnl_filters_simplified.Controls.Add(Me.gb_report_simplified)
    Me.pnl_filters_simplified.Controls.Add(Me.chk_terminal_location_simplified)
    Me.pnl_filters_simplified.Controls.Add(Me.chk_hide_no_activity_simplified)
    Me.pnl_filters_simplified.Controls.Add(Me.gb_date_simplified)
    Me.pnl_filters_simplified.Controls.Add(Me.uc_checked_list_groups_simplified)
    Me.pnl_filters_simplified.Controls.Add(Me.uc_provider_simplified)
    Me.pnl_filters_simplified.Location = New System.Drawing.Point(0, 0)
    Me.pnl_filters_simplified.Name = "pnl_filters_simplified"
    Me.pnl_filters_simplified.Size = New System.Drawing.Size(1208, 206)
    Me.pnl_filters_simplified.TabIndex = 1
    '
    'gb_report_simplified
    '
    Me.gb_report_simplified.Controls.Add(Me.chk_show_events_simplified)
    Me.gb_report_simplified.Controls.Add(Me.opt_daily_one_day_simplified)
    Me.gb_report_simplified.Controls.Add(Me.opt_daily_one_month_simplified)
    Me.gb_report_simplified.Controls.Add(Me.opt_hourly_simplified)
    Me.gb_report_simplified.Location = New System.Drawing.Point(332, 6)
    Me.gb_report_simplified.Name = "gb_report_simplified"
    Me.gb_report_simplified.Size = New System.Drawing.Size(178, 119)
    Me.gb_report_simplified.TabIndex = 1
    Me.gb_report_simplified.TabStop = False
    Me.gb_report_simplified.Text = "xReport"
    '
    'chk_show_events_simplified
    '
    Me.chk_show_events_simplified.AutoSize = True
    Me.chk_show_events_simplified.Location = New System.Drawing.Point(31, 96)
    Me.chk_show_events_simplified.Name = "chk_show_events_simplified"
    Me.chk_show_events_simplified.Size = New System.Drawing.Size(102, 17)
    Me.chk_show_events_simplified.TabIndex = 3
    Me.chk_show_events_simplified.Text = "xShowEvents"
    Me.chk_show_events_simplified.UseVisualStyleBackColor = True
    '
    'opt_daily_one_day_simplified
    '
    Me.opt_daily_one_day_simplified.AutoSize = True
    Me.opt_daily_one_day_simplified.Location = New System.Drawing.Point(15, 24)
    Me.opt_daily_one_day_simplified.Name = "opt_daily_one_day_simplified"
    Me.opt_daily_one_day_simplified.Size = New System.Drawing.Size(163, 17)
    Me.opt_daily_one_day_simplified.TabIndex = 0
    Me.opt_daily_one_day_simplified.TabStop = True
    Me.opt_daily_one_day_simplified.Text = "xDailyOneDaySimplified"
    Me.opt_daily_one_day_simplified.UseVisualStyleBackColor = True
    '
    'opt_daily_one_month_simplified
    '
    Me.opt_daily_one_month_simplified.AutoSize = True
    Me.opt_daily_one_month_simplified.Location = New System.Drawing.Point(15, 50)
    Me.opt_daily_one_month_simplified.Name = "opt_daily_one_month_simplified"
    Me.opt_daily_one_month_simplified.Size = New System.Drawing.Size(174, 17)
    Me.opt_daily_one_month_simplified.TabIndex = 1
    Me.opt_daily_one_month_simplified.TabStop = True
    Me.opt_daily_one_month_simplified.Text = "xDailyOneMonthSimplified"
    Me.opt_daily_one_month_simplified.UseVisualStyleBackColor = True
    '
    'opt_hourly_simplified
    '
    Me.opt_hourly_simplified.AutoSize = True
    Me.opt_hourly_simplified.Location = New System.Drawing.Point(15, 76)
    Me.opt_hourly_simplified.Name = "opt_hourly_simplified"
    Me.opt_hourly_simplified.Size = New System.Drawing.Size(125, 17)
    Me.opt_hourly_simplified.TabIndex = 2
    Me.opt_hourly_simplified.TabStop = True
    Me.opt_hourly_simplified.Text = "xHourlySimplified"
    Me.opt_hourly_simplified.UseVisualStyleBackColor = True
    '
    'chk_terminal_location_simplified
    '
    Me.chk_terminal_location_simplified.Location = New System.Drawing.Point(516, 109)
    Me.chk_terminal_location_simplified.Name = "chk_terminal_location_simplified"
    Me.chk_terminal_location_simplified.Size = New System.Drawing.Size(233, 16)
    Me.chk_terminal_location_simplified.TabIndex = 4
    Me.chk_terminal_location_simplified.Text = "xShow terminals location"
    '
    'chk_hide_no_activity_simplified
    '
    Me.chk_hide_no_activity_simplified.AutoSize = True
    Me.chk_hide_no_activity_simplified.Location = New System.Drawing.Point(516, 91)
    Me.chk_hide_no_activity_simplified.Name = "chk_hide_no_activity_simplified"
    Me.chk_hide_no_activity_simplified.Size = New System.Drawing.Size(114, 17)
    Me.chk_hide_no_activity_simplified.TabIndex = 3
    Me.chk_hide_no_activity_simplified.Text = "xHidenoActivity"
    Me.chk_hide_no_activity_simplified.UseVisualStyleBackColor = True
    '
    'gb_date_simplified
    '
    Me.gb_date_simplified.Controls.Add(Me.dtp_to_simplified)
    Me.gb_date_simplified.Controls.Add(Me.dtp_from_simplified)
    Me.gb_date_simplified.Location = New System.Drawing.Point(516, 6)
    Me.gb_date_simplified.Name = "gb_date_simplified"
    Me.gb_date_simplified.Size = New System.Drawing.Size(252, 80)
    Me.gb_date_simplified.TabIndex = 2
    Me.gb_date_simplified.TabStop = False
    Me.gb_date_simplified.Text = "xDate"
    '
    'dtp_to_simplified
    '
    Me.dtp_to_simplified.Checked = True
    Me.dtp_to_simplified.IsReadOnly = False
    Me.dtp_to_simplified.Location = New System.Drawing.Point(6, 43)
    Me.dtp_to_simplified.Name = "dtp_to_simplified"
    Me.dtp_to_simplified.ShowCheckBox = False
    Me.dtp_to_simplified.ShowUpDown = False
    Me.dtp_to_simplified.Size = New System.Drawing.Size(240, 24)
    Me.dtp_to_simplified.SufixText = "Sufix Text"
    Me.dtp_to_simplified.SufixTextVisible = True
    Me.dtp_to_simplified.TabIndex = 1
    Me.dtp_to_simplified.TextWidth = 50
    Me.dtp_to_simplified.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'dtp_from_simplified
    '
    Me.dtp_from_simplified.Checked = True
    Me.dtp_from_simplified.IsReadOnly = False
    Me.dtp_from_simplified.Location = New System.Drawing.Point(6, 18)
    Me.dtp_from_simplified.Name = "dtp_from_simplified"
    Me.dtp_from_simplified.ShowCheckBox = False
    Me.dtp_from_simplified.ShowUpDown = False
    Me.dtp_from_simplified.Size = New System.Drawing.Size(240, 24)
    Me.dtp_from_simplified.SufixText = "Sufix Text"
    Me.dtp_from_simplified.SufixTextVisible = True
    Me.dtp_from_simplified.TabIndex = 0
    Me.dtp_from_simplified.TextWidth = 50
    Me.dtp_from_simplified.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'uc_checked_list_groups_simplified
    '
    Me.uc_checked_list_groups_simplified.GroupBoxText = "xCheckedList"
    Me.uc_checked_list_groups_simplified.Location = New System.Drawing.Point(773, 6)
    Me.uc_checked_list_groups_simplified.m_resize_width = 422
    Me.uc_checked_list_groups_simplified.multiChoice = True
    Me.uc_checked_list_groups_simplified.Name = "uc_checked_list_groups_simplified"
    Me.uc_checked_list_groups_simplified.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list_groups_simplified.SelectedIndexesList = ""
    Me.uc_checked_list_groups_simplified.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list_groups_simplified.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_list_groups_simplified.SelectedValuesList = ""
    Me.uc_checked_list_groups_simplified.SelectedValuesListLevel2 = ""
    Me.uc_checked_list_groups_simplified.SetLevels = 2
    Me.uc_checked_list_groups_simplified.Size = New System.Drawing.Size(422, 198)
    Me.uc_checked_list_groups_simplified.TabIndex = 5
    Me.uc_checked_list_groups_simplified.ValuesArray = New String(-1) {}
    '
    'uc_provider_simplified
    '
    Me.uc_provider_simplified.Location = New System.Drawing.Point(3, 2)
    Me.uc_provider_simplified.Name = "uc_provider_simplified"
    Me.uc_provider_simplified.Size = New System.Drawing.Size(320, 203)
    Me.uc_provider_simplified.TabIndex = 0
    Me.uc_provider_simplified.TerminalListHasValues = False
    '
    'tf_last_update
    '
    Me.tf_last_update.IsReadOnly = True
    Me.tf_last_update.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_last_update.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_last_update.Location = New System.Drawing.Point(1211, 160)
    Me.tf_last_update.Name = "tf_last_update"
    Me.tf_last_update.Size = New System.Drawing.Size(230, 24)
    Me.tf_last_update.SufixText = "Sufix Text"
    Me.tf_last_update.SufixTextVisible = True
    Me.tf_last_update.TabIndex = 132
    Me.tf_last_update.TabStop = False
    Me.tf_last_update.TextWidth = 140
    Me.tf_last_update.Value = "xLastUpdate"
    '
    'tmr_monitor
    '
    '
    'gb_mode
    '
    Me.gb_mode.Controls.Add(Me.opt_monitor_mode)
    Me.gb_mode.Controls.Add(Me.opt_history_mode)
    Me.gb_mode.Location = New System.Drawing.Point(1214, 9)
    Me.gb_mode.Name = "gb_mode"
    Me.gb_mode.Size = New System.Drawing.Size(100, 67)
    Me.gb_mode.TabIndex = 131
    Me.gb_mode.TabStop = False
    Me.gb_mode.Text = "xMode"
    '
    'opt_monitor_mode
    '
    Me.opt_monitor_mode.AutoSize = True
    Me.opt_monitor_mode.Location = New System.Drawing.Point(6, 19)
    Me.opt_monitor_mode.Name = "opt_monitor_mode"
    Me.opt_monitor_mode.Size = New System.Drawing.Size(74, 17)
    Me.opt_monitor_mode.TabIndex = 0
    Me.opt_monitor_mode.Text = "xMonitor"
    Me.opt_monitor_mode.UseVisualStyleBackColor = True
    '
    'opt_history_mode
    '
    Me.opt_history_mode.AutoSize = True
    Me.opt_history_mode.Location = New System.Drawing.Point(6, 42)
    Me.opt_history_mode.Name = "opt_history_mode"
    Me.opt_history_mode.Size = New System.Drawing.Size(72, 17)
    Me.opt_history_mode.TabIndex = 1
    Me.opt_history_mode.Text = "xHistory"
    Me.opt_history_mode.UseVisualStyleBackColor = True
    '
    'gb_currency
    '
    Me.gb_currency.Controls.Add(Me.opt_credits)
    Me.gb_currency.Controls.Add(Me.opt_currency)
    Me.gb_currency.Cursor = System.Windows.Forms.Cursors.Default
    Me.gb_currency.Location = New System.Drawing.Point(1214, 82)
    Me.gb_currency.Name = "gb_currency"
    Me.gb_currency.Size = New System.Drawing.Size(142, 67)
    Me.gb_currency.TabIndex = 133
    Me.gb_currency.TabStop = False
    Me.gb_currency.Text = "xCurrency"
    '
    'opt_credits
    '
    Me.opt_credits.AutoSize = True
    Me.opt_credits.Location = New System.Drawing.Point(6, 19)
    Me.opt_credits.Name = "opt_credits"
    Me.opt_credits.Size = New System.Drawing.Size(73, 17)
    Me.opt_credits.TabIndex = 0
    Me.opt_credits.Text = "xCredits"
    Me.opt_credits.UseVisualStyleBackColor = True
    '
    'opt_currency
    '
    Me.opt_currency.AutoSize = True
    Me.opt_currency.Location = New System.Drawing.Point(6, 42)
    Me.opt_currency.Name = "opt_currency"
    Me.opt_currency.Size = New System.Drawing.Size(85, 17)
    Me.opt_currency.TabIndex = 1
    Me.opt_currency.Text = "xCurrency"
    Me.opt_currency.UseVisualStyleBackColor = True
    '
    'frm_terminal_sas_meters_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1636, 676)
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "frm_terminal_sas_meters_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_terminal_sas_meters_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.pnl_filters_standard.ResumeLayout(False)
    Me.pnl_filters_standard.PerformLayout()
    Me.gb_date.ResumeLayout(False)
    Me.gb_order_by.ResumeLayout(False)
    Me.gb_order_by.PerformLayout()
    Me.gb_counter_type.ResumeLayout(False)
    Me.gb_counter_type.PerformLayout()
    Me.pnl_filters_simplified.ResumeLayout(False)
    Me.pnl_filters_simplified.PerformLayout()
    Me.gb_report_simplified.ResumeLayout(False)
    Me.gb_report_simplified.PerformLayout()
    Me.gb_date_simplified.ResumeLayout(False)
    Me.gb_mode.ResumeLayout(False)
    Me.gb_mode.PerformLayout()
    Me.gb_currency.ResumeLayout(False)
    Me.gb_currency.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents pnl_filters_standard As System.Windows.Forms.Panel
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents chk_hide_no_activity As System.Windows.Forms.CheckBox
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents uc_checked_list_groups As GUI_Controls.uc_checked_list
  Friend WithEvents gb_order_by As System.Windows.Forms.GroupBox
  Friend WithEvents opt_terminal As System.Windows.Forms.RadioButton
  Friend WithEvents opt_date As System.Windows.Forms.RadioButton
  Friend WithEvents uc_provider As GUI_Controls.uc_provider
  Friend WithEvents gb_counter_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_denom_change As System.Windows.Forms.CheckBox
  Friend WithEvents chk_first_time As System.Windows.Forms.CheckBox
  Friend WithEvents opt_types As System.Windows.Forms.RadioButton
  Friend WithEvents opt_currents As System.Windows.Forms.RadioButton
  Friend WithEvents chk_rollover As System.Windows.Forms.CheckBox
  Friend WithEvents chk_reset As System.Windows.Forms.CheckBox
  Friend WithEvents chk_daily As System.Windows.Forms.CheckBox
  Friend WithEvents chk_hourly As System.Windows.Forms.CheckBox
  Friend WithEvents pnl_filters_simplified As System.Windows.Forms.Panel
  Friend WithEvents chk_terminal_location_simplified As System.Windows.Forms.CheckBox
  Friend WithEvents chk_hide_no_activity_simplified As System.Windows.Forms.CheckBox
  Friend WithEvents gb_date_simplified As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to_simplified As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from_simplified As GUI_Controls.uc_date_picker
  Friend WithEvents uc_checked_list_groups_simplified As GUI_Controls.uc_checked_list
  Friend WithEvents uc_provider_simplified As GUI_Controls.uc_provider
  Friend WithEvents gb_report_simplified As System.Windows.Forms.GroupBox
  Friend WithEvents opt_daily_one_day_simplified As System.Windows.Forms.RadioButton
  Friend WithEvents opt_daily_one_month_simplified As System.Windows.Forms.RadioButton
  Friend WithEvents opt_hourly_simplified As System.Windows.Forms.RadioButton
  Friend WithEvents chk_show_events_simplified As System.Windows.Forms.CheckBox
  Friend WithEvents uc_sites_sel As GUI_Controls.uc_sites_sel
  Friend WithEvents tmr_monitor As System.Windows.Forms.Timer
  Friend WithEvents tf_last_update As GUI_Controls.uc_text_field
  Friend WithEvents gb_mode As System.Windows.Forms.GroupBox
  Friend WithEvents opt_monitor_mode As System.Windows.Forms.RadioButton
  Friend WithEvents opt_history_mode As System.Windows.Forms.RadioButton
  Friend WithEvents gb_currency As System.Windows.Forms.GroupBox
  Friend WithEvents opt_credits As System.Windows.Forms.RadioButton
  Friend WithEvents opt_currency As System.Windows.Forms.RadioButton
End Class
