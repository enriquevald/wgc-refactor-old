<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_stackers_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.xGroupStacker = New System.Windows.Forms.GroupBox
    Me.ef_stacker_model = New GUI_Controls.uc_entry_field
    Me.ef_stacker_indentifier = New GUI_Controls.uc_entry_field
    Me.gb_terminal = New System.Windows.Forms.GroupBox
    Me.ef_terminal_name = New GUI_Controls.uc_entry_field
    Me.gb_status = New System.Windows.Forms.GroupBox
    Me.chk_collection_pending = New System.Windows.Forms.CheckBox
    Me.chk_inserted_validated = New System.Windows.Forms.CheckBox
    Me.chk_inserted_pending = New System.Windows.Forms.CheckBox
    Me.chk_available = New System.Windows.Forms.CheckBox
    Me.chk_inactive = New System.Windows.Forms.CheckBox
    Me.ef_provider_name = New GUI_Controls.uc_entry_field
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.xGroupStacker.SuspendLayout()
    Me.gb_terminal.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Controls.Add(Me.xGroupStacker)
    Me.panel_filter.Controls.Add(Me.gb_terminal)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(929, 86)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_terminal, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.xGroupStacker, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 90)
    Me.panel_data.Size = New System.Drawing.Size(929, 501)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(923, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(923, 4)
    '
    'xGroupStacker
    '
    Me.xGroupStacker.Controls.Add(Me.ef_stacker_model)
    Me.xGroupStacker.Controls.Add(Me.ef_stacker_indentifier)
    Me.xGroupStacker.Location = New System.Drawing.Point(3, 3)
    Me.xGroupStacker.Name = "xGroupStacker"
    Me.xGroupStacker.Size = New System.Drawing.Size(213, 81)
    Me.xGroupStacker.TabIndex = 0
    Me.xGroupStacker.TabStop = False
    Me.xGroupStacker.Text = "xGroupStacker"
    '
    'ef_stacker_model
    '
    Me.ef_stacker_model.DoubleValue = 0
    Me.ef_stacker_model.IntegerValue = 0
    Me.ef_stacker_model.IsReadOnly = False
    Me.ef_stacker_model.Location = New System.Drawing.Point(6, 47)
    Me.ef_stacker_model.Name = "ef_stacker_model"
    Me.ef_stacker_model.Size = New System.Drawing.Size(182, 24)
    Me.ef_stacker_model.SufixText = "Sufix Text"
    Me.ef_stacker_model.SufixTextVisible = True
    Me.ef_stacker_model.TabIndex = 1
    Me.ef_stacker_model.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_stacker_model.TextValue = ""
    Me.ef_stacker_model.TextWidth = 70
    Me.ef_stacker_model.Value = ""
    '
    'ef_stacker_indentifier
    '
    Me.ef_stacker_indentifier.DoubleValue = 0
    Me.ef_stacker_indentifier.IntegerValue = 0
    Me.ef_stacker_indentifier.IsReadOnly = False
    Me.ef_stacker_indentifier.Location = New System.Drawing.Point(6, 16)
    Me.ef_stacker_indentifier.Name = "ef_stacker_indentifier"
    Me.ef_stacker_indentifier.Size = New System.Drawing.Size(182, 24)
    Me.ef_stacker_indentifier.SufixText = "Sufix Text"
    Me.ef_stacker_indentifier.SufixTextVisible = True
    Me.ef_stacker_indentifier.TabIndex = 0
    Me.ef_stacker_indentifier.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_stacker_indentifier.TextValue = ""
    Me.ef_stacker_indentifier.TextWidth = 70
    Me.ef_stacker_indentifier.Value = ""
    '
    'gb_terminal
    '
    Me.gb_terminal.Controls.Add(Me.ef_provider_name)
    Me.gb_terminal.Controls.Add(Me.ef_terminal_name)
    Me.gb_terminal.Location = New System.Drawing.Point(494, 3)
    Me.gb_terminal.Name = "gb_terminal"
    Me.gb_terminal.Size = New System.Drawing.Size(242, 81)
    Me.gb_terminal.TabIndex = 2
    Me.gb_terminal.TabStop = False
    Me.gb_terminal.Text = "xGroup Terminal"
    '
    'ef_terminal_name
    '
    Me.ef_terminal_name.DoubleValue = 0
    Me.ef_terminal_name.IntegerValue = 0
    Me.ef_terminal_name.IsReadOnly = False
    Me.ef_terminal_name.Location = New System.Drawing.Point(17, 16)
    Me.ef_terminal_name.Name = "ef_terminal_name"
    Me.ef_terminal_name.Size = New System.Drawing.Size(206, 25)
    Me.ef_terminal_name.SufixText = "Sufix Text"
    Me.ef_terminal_name.SufixTextVisible = True
    Me.ef_terminal_name.TabIndex = 0
    Me.ef_terminal_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal_name.TextValue = ""
    Me.ef_terminal_name.TextWidth = 70
    Me.ef_terminal_name.Value = ""
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.chk_collection_pending)
    Me.gb_status.Controls.Add(Me.chk_inserted_validated)
    Me.gb_status.Controls.Add(Me.chk_inserted_pending)
    Me.gb_status.Controls.Add(Me.chk_available)
    Me.gb_status.Controls.Add(Me.chk_inactive)
    Me.gb_status.Location = New System.Drawing.Point(222, 3)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(266, 81)
    Me.gb_status.TabIndex = 1
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xGroup Status"
    '
    'chk_collection_pending
    '
    Me.chk_collection_pending.AutoSize = True
    Me.chk_collection_pending.Location = New System.Drawing.Point(113, 58)
    Me.chk_collection_pending.Name = "chk_collection_pending"
    Me.chk_collection_pending.Size = New System.Drawing.Size(138, 17)
    Me.chk_collection_pending.TabIndex = 4
    Me.chk_collection_pending.Text = "xCollection Pending"
    Me.chk_collection_pending.UseVisualStyleBackColor = True
    '
    'chk_inserted_validated
    '
    Me.chk_inserted_validated.AutoSize = True
    Me.chk_inserted_validated.Location = New System.Drawing.Point(113, 37)
    Me.chk_inserted_validated.Name = "chk_inserted_validated"
    Me.chk_inserted_validated.Size = New System.Drawing.Size(138, 17)
    Me.chk_inserted_validated.TabIndex = 3
    Me.chk_inserted_validated.Text = "xInserted Validated"
    Me.chk_inserted_validated.UseVisualStyleBackColor = True
    '
    'chk_inserted_pending
    '
    Me.chk_inserted_pending.AutoSize = True
    Me.chk_inserted_pending.Location = New System.Drawing.Point(113, 16)
    Me.chk_inserted_pending.Name = "chk_inserted_pending"
    Me.chk_inserted_pending.Size = New System.Drawing.Size(130, 17)
    Me.chk_inserted_pending.TabIndex = 2
    Me.chk_inserted_pending.Text = "xInserted Pending"
    Me.chk_inserted_pending.UseVisualStyleBackColor = True
    '
    'chk_available
    '
    Me.chk_available.AutoSize = True
    Me.chk_available.Location = New System.Drawing.Point(12, 47)
    Me.chk_available.Name = "chk_available"
    Me.chk_available.Size = New System.Drawing.Size(85, 17)
    Me.chk_available.TabIndex = 1
    Me.chk_available.Text = "xAvailable"
    Me.chk_available.UseVisualStyleBackColor = True
    '
    'chk_inactive
    '
    Me.chk_inactive.AutoSize = True
    Me.chk_inactive.Location = New System.Drawing.Point(12, 23)
    Me.chk_inactive.Name = "chk_inactive"
    Me.chk_inactive.Size = New System.Drawing.Size(79, 17)
    Me.chk_inactive.TabIndex = 0
    Me.chk_inactive.Text = "xInactive"
    Me.chk_inactive.UseVisualStyleBackColor = True
    '
    'ef_provider_name
    '
    Me.ef_provider_name.DoubleValue = 0
    Me.ef_provider_name.IntegerValue = 0
    Me.ef_provider_name.IsReadOnly = False
    Me.ef_provider_name.Location = New System.Drawing.Point(17, 46)
    Me.ef_provider_name.Name = "ef_provider_name"
    Me.ef_provider_name.Size = New System.Drawing.Size(206, 25)
    Me.ef_provider_name.SufixText = "Sufix Text"
    Me.ef_provider_name.SufixTextVisible = True
    Me.ef_provider_name.TabIndex = 1
    Me.ef_provider_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_provider_name.TextValue = ""
    Me.ef_provider_name.TextWidth = 70
    Me.ef_provider_name.Value = ""
    '
    'frm_stackers_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(939, 595)
    Me.Name = "frm_stackers_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_stackers_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.xGroupStacker.ResumeLayout(False)
    Me.gb_terminal.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents xGroupStacker As System.Windows.Forms.GroupBox
  Friend WithEvents ef_stacker_indentifier As GUI_Controls.uc_entry_field
  Friend WithEvents ef_stacker_model As GUI_Controls.uc_entry_field
  Friend WithEvents gb_terminal As System.Windows.Forms.GroupBox
  Friend WithEvents ef_terminal_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_combo_terminal As GUI_Controls.uc_combo
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_available As System.Windows.Forms.CheckBox
  Friend WithEvents chk_inactive As System.Windows.Forms.CheckBox
  Friend WithEvents chk_collection_pending As System.Windows.Forms.CheckBox
  Friend WithEvents chk_inserted_validated As System.Windows.Forms.CheckBox
  Friend WithEvents chk_inserted_pending As System.Windows.Forms.CheckBox
  Friend WithEvents ef_provider_name As GUI_Controls.uc_entry_field
End Class
