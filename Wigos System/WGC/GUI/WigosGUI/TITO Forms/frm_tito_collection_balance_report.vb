'-------------------------------------------------------------------
' Copyright � 2011 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_tito_collection_balance_report
' DESCRIPTION:   Collection balance report 
' AUTHOR:        Jos� Mart�nez
' CREATION DATE: 04-DEC-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 04-DEC-2013  JML    Initial version
' 11-DEC-2013  DRV    Hide Cashier Ticket Collections
' 18-DEC-2013  ACM    Fixed Bug WIGOSTITO-864: Set only Sas-Host terminals to terminals filter control.
' 26-MAR-2014  ICS    Fixed Bug WIGOSTITO-1168: The filter 'Some' requires selecting at least one
' 09-APR-2014  LEM    Fixed Bug WIGOSTITO-1195: Bad painting cell with unbalance
' 09-APR-2014  LEM    Fixed Bug WIGOSTITO-1196: Grid cleaning on Reset click
' 13-MAY-2014  LEM    Fixed Bug WIGOSTITO-1222: Wrong bills columns's name
' 15-OCT-2014  SGB    Add in subtotal columns provider & terminal
' 22-OCT-2014  LEM    Use FORM_CAGE_SHOW_EXPECTED_AMOUNTS permission to Show/Hide expected/difference columns reports.
' 04-NOV-2014  DLL    Fixed Bug WIGOSTITO-1259: Separate bills and coins
' 18-NOV-2014  SGB    Fixed Bug WIG-1697: Incorrect percentage result.
' 18-NOV-2014  LEM    Fixed Bug WIG-1696: Must add "Show terminal location" filter.
' 09-JUN-2015  YNM    TFS #674: Add Tickets columns. 
' 15-SEP-2015  DCS    Backlog Item 3707 - Coin Collection
' 17-AUG-2017  MS     Bug 29375:[WIGOS-3714] Missing selection criterion for exporting data in Collection balance report screen
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports WSI.Common

Public Class frm_tito_collection_balance_report
  Inherits frm_base_sel

#Region " Constants "
  Private TERMINAL_DATA_COLUMNS As Int32

  ' Grid configuration
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private GRID_COLUMN_SELECT As Integer
  Private GRID_COLUMN_MC_COLLECTION_ID As Integer
  Private GRID_INIT_TERMINAL_DATA As Integer
  Private GRID_COLUMN_TE_PROVIDER As Integer
  Private GRID_COLUMN_TE_NAME As Integer
  Private GRID_COLUMN_MC_STATUS As Integer
  Private GRID_COLUMN_MC_STACKER_ID As Integer
  Private GRID_COLUMN_MC_COLLECTION_DATETIME As Integer
  Private GRID_COLUMN_CAGE_SESSION_DATE As Int32
  Private GRID_COLUMN_MC_DATETIME As Integer
  Private GRID_COLUMN_MC_EXTRACTION_DATETIME As Integer
  Private GRID_COLUMN_MC_CASHIER_SESSION_ID As Integer
  Private GRID_COLUMN_USER_NM As Integer
  Private GRID_COLUMN_MC_NOTES As Integer
  Private GRID_COLUMN_TI_CASHABLE_AMOUNT As Integer
  Private GRID_COLUMN_TI_CASHABLE_COUNT As Integer
  Private GRID_COLUMN_TI_PROMO_RE_AMOUNT As Integer
  Private GRID_COLUMN_TI_PROMO_RE_COUNT As Integer
  Private GRID_COLUMN_TI_PROMO_NR_AMOUNT As Integer
  Private GRID_COLUMN_TI_PROMO_NR_COUNT As Integer
  Private GRID_COLUMN_TI_TOTAL_AMOUNT As Integer
  Private GRID_COLUMN_TI_TOTAL_COUNT As Integer
  Private GRID_COLUMN_MCD_NUM As Integer
  Private GRID_COLUMN_COLLECTED_BILL_FIRST As Integer
  Private GRID_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT As Integer
  Private GRID_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT As Integer
  Private GRID_COLUMN_MCD_TOTAL_CASH As Integer
  Private GRID_COLUMN_MCM_MONEY_COLLECTION_ID As Integer
  Private GRID_COLUMN_MCM_SESSION_ID As Integer
  Private GRID_COLUMN_MCM_TERMINAL_ID As Integer
  Private GRID_COLUMN_MCM_STARTED As Integer
  Private GRID_COLUMN_MCM_FINISHED As Integer
  Private GRID_COLUMN_MCM_ERROR_REPORTING As Integer
  Private GRID_COLUMN_MCM_CASHABLE_NUM As Integer
  Private GRID_COLUMN_MCM_CASHABLE_AMOUNT As Integer
  Private GRID_COLUMN_MCM_PROMO_RE_NUM As Integer
  Private GRID_COLUMN_MCM_PROMO_RE_AMOUNT As Integer
  Private GRID_COLUMN_MCM_PROMO_NR_NUM As Integer
  Private GRID_COLUMN_MCM_PROMO_NR_AMOUNT As Integer
  Private GRID_COLUMN_EXPECTED_BILL_FIRST As Integer
  Private GRID_COLUMN_MCM_BILL_TOTAL_AMOUNT As Integer
  Private GRID_COLUMN_TE_TERMINAL_ID As Integer
  Private GRID_COLUMN_DIFF_BILL_AMOUNT As Integer
  Private GRID_COLUMN_DIFF_BILL_PERCENTAJE As Integer
  Private GRID_COLUMN_DIFF_TICKET_AMOUNT As Integer
  Private GRID_COLUMN_DIFF_TICKET_PERCENTAJE As Integer
  Private GRID_COLUMN_MCM_TOTAL_COIN_AMOUNT As Integer
  Private GRID_COLUMN_MCM_TOTAL_CASH As Integer
  Private GRID_COLUMN_DIFF_COIN_AMOUNT As Integer
  Private GRID_COLUMN_DIFF_COIN_PERCENTAJE As Integer
  Private GRID_COLUMN_DIFF_CASH_AMOUNT As Integer
  Private GRID_COLUMN_DIFF_CASH_PERCENTAJE As Integer

  Private GRID_COLUMNS_COUNT As Integer

  ' QUERY columns
  Private Const SQL_COLUMN_TE_TERMINAL_ID As Integer = 0
  Private Const SQL_COLUMN_MC_COLLECTION_ID As Integer = 1
  Private Const SQL_COLUMN_MC_STACKER_ID As Integer = 2
  Private Const SQL_COLUMN_TE_NAME As Integer = 3
  Private Const SQL_COLUMN_MC_STATUS As Integer = 4
  Private Const SQL_COLUMN_USER_NM As Integer = 5
  Private Const SQL_COLUMN_MC_COLLECTION_DATETIME As Integer = 6
  Private Const SQL_COLUMN_MC_DATETIME As Integer = 7
  Private Const SQL_COLUMN_MC_EXTRACTION_DATETIME As Integer = 8
  Private Const SQL_COLUMN_CAGE_DATE_TIME As Integer = 9
  Private Const SQL_COLUMN_MC_CASHIER_SESSION_ID As Integer = 10
  Private Const SQL_COLUMN_MC_NOTES As Integer = 11
  Private Const SQL_COLUMN_TI_CASHABLE_COUNT As Integer = 12
  Private Const SQL_COLUMN_TI_CASHABLE_AMOUNT As Integer = 13
  Private Const SQL_COLUMN_TI_PROMO_RE_COUNT As Integer = 14
  Private Const SQL_COLUMN_TI_PROMO_RE_AMOUNT As Integer = 15
  Private Const SQL_COLUMN_TI_PROMO_NR_COUNT As Integer = 16
  Private Const SQL_COLUMN_TI_PROMO_NR_AMOUNT As Integer = 17
  Private Const SQL_COLUMN_MCD_NUM As Integer = 18

  Private SQL_COLUMN_COLLECTED_BILL_FIRST As Integer
  Private SQL_COLUMN_MCM_MONEY_COLLECTION_ID As Integer
  Private SQL_COLUMN_MCM_SESSION_ID As Integer
  Private SQL_COLUMN_MCM_TERMINAL_ID As Integer
  Private SQL_COLUMN_MCM_STARTED As Integer
  Private SQL_COLUMN_MCM_FINISHED As Integer
  Private SQL_COLUMN_MCM_ERROR_REPORTING As Integer
  Private SQL_COLUMN_EXPECTED_BILL_FIRST As Integer
  Private SQL_COLUMN_MCM_CASHABLE_NUM As Integer
  Private SQL_COLUMN_MCM_CASHABLE_AMOUNT As Integer
  Private SQL_COLUMN_MCM_PROMO_RE_NUM As Integer
  Private SQL_COLUMN_MCM_PROMO_RE_AMOUNT As Integer
  Private SQL_COLUMN_MCM_PROMO_NR_NUM As Integer
  Private SQL_COLUMN_MCM_PROMO_NR_AMOUNT As Integer
  Private SQL_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT As Integer
  Private SQL_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT As Integer
  Private SQL_COLUMN_MCM_BILL_TOTAL_AMOUNT As Integer
  Private SQL_COLUMN_PROVIDER_ID As Integer
  Private SQL_COLUMN_MCM_TOTAL_COIN_AMOUNT As Integer

  ' Counter
  Private Const COUNTER_OK As Integer = 1
  Private Const COUNTER_DIFF As Integer = 2
  'Private Const COUNTER_ERROR As Integer = 3

  Private WIDTH_BILLS_DET As Integer = 1100
  Private WIDTH_BILLS_TOT As Integer = 1800
  Private WIDTH_COINS_TOT As Integer = 1800
  Private WIDTH_TICKETS As Integer = 2100

#End Region   ' Constants

#Region " Members "

  Private m_date_from As String
  Private m_date_to As String
  Private m_terminals As String
  Private m_row_deleted As Boolean
  Private m_list_of_index_with_erors As List(Of Int32)

  Private m_ti_cashable_count As Integer
  Private m_ti_cashable_amount As Decimal
  Private m_ti_promo_re_count As Integer
  Private m_ti_promo_re_amount As Decimal
  Private m_ti_promo_nr_count As Integer
  Private m_ti_promo_nr_amount As Decimal
  Private m_ti_total_count As Integer
  Private m_ti_total_amount As Decimal
  Private m_mcd_num As Integer

  Private m_mcm_cashable_num As Integer
  Private m_mcm_cashable_amount As Decimal
  Private m_mcm_promo_nr_num As Integer
  Private m_mcm_promo_nr_amount As Decimal
  Private m_mcm_promo_re_num As Integer
  Private m_mcm_promo_re_amount As Decimal
  Private m_mcd_collected_total_amount As Decimal
  Private m_mcm_bill_total_amount As Decimal
  Private m_mcd_collected_total_coin_amount As Decimal
  Private m_mcm_total_coin_amount As Decimal

  Private m_subtotal_provider_id As String
  Private m_subtotal_te_name As String
  Private m_subtotal_date As Date

  Private m_subtotal_ti_cashable_count As Integer
  Private m_subtotal_ti_cashable_amount As Decimal
  Private m_subtotal_ti_promo_re_count As Integer
  Private m_subtotal_ti_promo_re_amount As Decimal
  Private m_subtotal_ti_promo_nr_count As Integer
  Private m_subtotal_ti_promo_nr_amount As Decimal
  Private m_subtotal_ti_total_count As Integer
  Private m_subtotal_ti_total_amount As Decimal
  Private m_subtotal_mcd_num As Integer

  Private m_subtotal_mcm_cashable_num As Integer
  Private m_subtotal_mcm_cashable_amount As Decimal
  Private m_subtotal_mcm_promo_nr_num As Integer
  Private m_subtotal_mcm_promo_nr_amount As Decimal
  Private m_subtotal_mcm_promo_re_num As Integer
  Private m_subtotal_mcm_promo_re_amount As Decimal
  Private m_subtotal_mcd_collected_total_amount As Decimal
  Private m_subtotal_mcm_bill_total_amount As Decimal

  Private m_subtotal_mcd_collected_total_coin_amount As Decimal
  Private m_subtotal_mcm_total_coin_amount As Decimal

  Private m_first_row As Boolean
  Private m_has_row As Boolean
  Private m_bill_data As List(Of TYPE_BILL_DATA)
  Private m_show_expected_data As Boolean
  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_options As String
  Private m_show_details As Boolean
  Private m_index_to_print_subtotal_date As Int32
  Private m_index_of_date_to_calculate_subtotal As Int32
  Private m_insert_last_row As Boolean

#End Region  ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TITO_COLLECTION_BALANCE_REPORT
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3335) ' Collection balance report

    Me.m_show_expected_data = CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_SHOW_EXPECTED_AMOUNTS).Read

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_AUDITOR.GetString(12)

    Me.cb_show_only_out_of_balance.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(648)
    Me.cb_show_all_denominations.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5416)
    Me.cb_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)
    Me.ds_from_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4415)

    ' Group by groupbox
    gb_group_by.Text = GLB_NLS_GUI_INVOICING.GetString(369)
    chk_group_by.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2990)
    opt_group_by_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2251)
    opt_provider.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(469)
    chk_show_detail.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5721)
    opt_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5730)

    'group box date
    opt_insertion_datetime.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5734)
    opt_collection_datetime.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5735)
    opt_cage_session_datetime.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5715)
    gb_date_filter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2118)
    opt_insertion_datetime.Checked = True


    opt_group_by_day.Enabled = False
    opt_provider.Enabled = False
    opt_terminal.Enabled = False
    opt_group_by_day.Checked = True
    chk_show_detail.Enabled = False
    chk_show_detail.Checked = True

    ' Terminals
    Dim _terminal_types() As Int32
    _terminal_types = WSI.Common.Misc.AcceptorTerminalTypeList()

    Call Me.uc_provider.Init(_terminal_types)

    gb_group_by.Location = New Point(uc_provider.Location.X + uc_provider.Width + 10, gb_group_by.Location.Y)
    ' Set filter default values
    Call SetDefaultValues()

    ' Get bill denominations
    Call GetBillData()

    'Grid
    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates
    If Me.ds_from_to.FromDateSelected And Me.ds_from_to.ToDateSelected Then
      If Me.ds_from_to.FromDate > Me.ds_from_to.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.ds_from_to.Focus()

        Return False
      End If
    End If

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_where As String
    Dim _str_bills As String
    Dim _str_sql As StringBuilder
    Dim _order_by_date As String

    _order_by_date = ""
    If opt_insertion_datetime.Checked Then
      _order_by_date = " MC_DATETIME DESC"
    ElseIf opt_collection_datetime.Checked Then
      _order_by_date = " MC_COLLECTION_DATETIME DESC"
    Else
      _order_by_date = " CGS_WORKING_DAY DESC"
    End If

    _str_sql = New StringBuilder()

    _str_sql.AppendLine("DECLARE @pTicketStatusCashable as Int ")
    _str_sql.AppendLine("DECLARE @pTicketStatusPromoRedeem as Int ")
    _str_sql.AppendLine("DECLARE @pTicketStatusPromoNonRedeem as Int ")

    _str_sql.AppendLine("SET @pTicketStatusCashable = " & TITO_TICKET_TYPE.CASHABLE & " ")
    _str_sql.AppendLine("SET @pTicketStatusPromoRedeem = " & TITO_TICKET_TYPE.PROMO_REDEEM & " ")
    _str_sql.AppendLine("SET @pTicketStatusPromoNonRedeem = " & TITO_TICKET_TYPE.PROMO_NONREDEEM & " ")

    _str_sql.AppendLine("   SELECT   MC_COLLECTION_ID ")
    _str_sql.AppendLine("          , MC_STACKER_ID ")
    _str_sql.AppendLine("          , MC_STATUS ")
    _str_sql.AppendLine("          , MC_COLLECTION_DATETIME ")
    _str_sql.AppendLine("          , MC_DATETIME ")
    _str_sql.AppendLine("          , MC_EXTRACTION_DATETIME ")
    _str_sql.AppendLine("          , MC_CASHIER_SESSION_ID ")
    _str_sql.AppendLine("          , ISNULL(MC_NOTES, '') AS MC_NOTES ")
    _str_sql.AppendLine("          , MC_TERMINAL_ID ")
    _str_sql.AppendLine("          , MC_USER_ID ")
    _str_sql.AppendLine("          , ISNULL(MC_COLLECTED_RE_TICKET_AMOUNT, 0) AS MC_COLLECTED_RE_TICKET_AMOUNT ")
    _str_sql.AppendLine("          , ISNULL(MC_COLLECTED_PROMO_RE_TICKET_AMOUNT, 0) AS MC_COLLECTED_PROMO_RE_TICKET_AMOUNT ")
    _str_sql.AppendLine("          , ISNULL(MC_COLLECTED_PROMO_NR_TICKET_AMOUNT, 0) AS MC_COLLECTED_PROMO_NR_TICKET_AMOUNT ")
    _str_sql.AppendLine("          , ISNULL(MC_COLLECTED_BILL_AMOUNT, 0) AS MC_COLLECTED_BILL_AMOUNT ")
    _str_sql.AppendLine("          , CGS_WORKING_DAY                                                 ")
    _str_sql.AppendLine("          , MC_COLLECTED_RE_TICKET_COUNT                                    ")
    _str_sql.AppendLine("          , MC_COLLECTED_PROMO_RE_TICKET_COUNT                              ")
    _str_sql.AppendLine("          , MC_COLLECTED_PROMO_NR_TICKET_COUNT                              ")
    _str_sql.AppendLine("          , MC_COLLECTED_COIN_AMOUNT                                        ")
    _str_sql.AppendLine("     INTO   #MONEY_COLLECTIONS ")
    _str_sql.AppendLine("     FROM   MONEY_COLLECTIONS ")
    _str_sql.AppendLine("INNER JOIN   CAGE_MOVEMENTS ON MC_CASHIER_SESSION_ID = CGM_CASHIER_SESSION_ID")
    _str_sql.AppendLine("INNER JOIN   CAGE_SESSIONS  ON CGM_CAGE_SESSION_ID = CGS_CAGE_SESSION_ID")
    _str_sql.AppendLine("    WHERE   MC_COLLECTION_DATETIME IS NOT NULL ")
    _str_sql.AppendLine("      AND   MC_CASHIER_ID IS NULL")

    _str_where = GetSqlWhere()

    If Not String.IsNullOrEmpty(_str_where) Then
      _str_sql.AppendLine(_str_where)
    End If

    _str_sql.AppendLine("    SELECT   MCD_COLLECTION_ID ")
    _str_sql.AppendLine("           , SUM(ISNULL(MCD_NUM_COLLECTED, 0)) AS MCD_NUM ")


    For Each _bill_data As TYPE_BILL_DATA In m_bill_data
      _str_sql.AppendLine("           , SUM(CASE WHEN MCD_FACE_VALUE=" + _bill_data.denomination + " AND MCD_CAGE_CURRENCY_TYPE= " + CStr(CageCurrencyType.Bill) + " ")
      _str_sql.AppendLine("             THEN (ISNULL(MCD_NUM_COLLECTED, 0)) ELSE 0 END ) AS MCD_NUM_" + _bill_data.denomination + " ")
    Next
    ' Coins are only in Money_collections not in money_collection_details
    '' DLL 03-10-2014 Only coins
    '_str_sql.AppendLine("           , SUM(CASE WHEN MCD_CAGE_CURRENCY_TYPE= " + CStr(CageCurrencyType.Coin) + " ")
    '_str_sql.AppendLine("             THEN (ISNULL(MCD_NUM_COLLECTED, 0)  * MCD_FACE_VALUE) ELSE 0 END ) AS MCD_TOTAL_COINS ")

    _str_sql.AppendLine("      INTO   #MONEY_COLLECTION_DETAILS ")
    _str_sql.AppendLine("      FROM   MONEY_COLLECTION_DETAILS ")
    _str_sql.AppendLine("INNER JOIN   #MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID ")
    _str_sql.AppendLine("  GROUP BY   MCD_COLLECTION_ID ")

    _str_sql.AppendLine("    SELECT   MCM_MONEY_COLLECTION_ID ")
    _str_sql.AppendLine("           , ISNULL(MCM_SESSION_ID      , 0) AS MCM_SESSION_ID ")
    _str_sql.AppendLine("           , ISNULL(MCM_TERMINAL_ID     , 0) AS MCM_TERMINAL_ID ")
    _str_sql.AppendLine("           , ISNULL(MCM_STARTED         , 0) AS MCM_STARTED ")
    _str_sql.AppendLine("           , ISNULL(MCM_FINISHED        , 0) AS MCM_FINISHED ")
    _str_sql.AppendLine("           , ISNULL(MCM_ERROR_REPORTING , 0) AS MCM_ERROR_REPORTING ")

    For Each _bill_data As TYPE_BILL_DATA In m_bill_data
      _str_sql.AppendLine("           , MCM_" + _bill_data.denomination + "_BILL_NUM ")
    Next

    _str_sql.AppendLine("           , MCM_CASHABLE_NUM ")
    _str_sql.AppendLine("           , MCM_CASHABLE_AMOUNT ")
    _str_sql.AppendLine("           , MCM_PROMO_RE_NUM ")
    _str_sql.AppendLine("           , MCM_PROMO_RE_AMOUNT ")
    _str_sql.AppendLine("           , MCM_PROMO_NR_NUM ")
    _str_sql.AppendLine("           , MCM_PROMO_NR_AMOUNT ")
    _str_sql.AppendLine("           , MCM_TOTAL_COIN_AMOUNT ")
    _str_sql.AppendLine("      INTO   #MONEY_COLLECTION_METERS ")
    _str_sql.AppendLine("      FROM   MONEY_COLLECTION_METERS ")
    _str_sql.AppendLine("INNER JOIN   #MONEY_COLLECTIONS ON MCM_MONEY_COLLECTION_ID = MC_COLLECTION_ID ")
    '_str_sql.AppendLine("  --GROUP BY   MCM_MONEY_COLLECTION_ID ")

    _str_sql.AppendLine("   SELECT   TE_TERMINAL_ID ")
    _str_sql.AppendLine("          , MC_COLLECTION_ID ")
    _str_sql.AppendLine("          , MC_STACKER_ID ")
    _str_sql.AppendLine("          , ISNULL(TE_NAME, '') AS TE_NAME ")
    _str_sql.AppendLine("          , MC_STATUS ")
    _str_sql.AppendLine("          , ISNULL(GU_USERNAME, '' ) AS USER_NM ")
    _str_sql.AppendLine("          , MC_COLLECTION_DATETIME ")
    _str_sql.AppendLine("          , MC_DATETIME ")
    _str_sql.AppendLine("          , MC_EXTRACTION_DATETIME ")
    _str_sql.AppendLine("          , CGS_WORKING_DAY                                             ")
    _str_sql.AppendLine("          , MC_CASHIER_SESSION_ID ")
    _str_sql.AppendLine("          , ISNULL(MC_NOTES, '') AS MC_NOTES ")

    _str_sql.AppendLine("          , MC_COLLECTED_RE_TICKET_COUNT                                    ")
    _str_sql.AppendLine("          , MC_COLLECTED_RE_TICKET_AMOUNT ")
    _str_sql.AppendLine("          , MC_COLLECTED_PROMO_RE_TICKET_COUNT                              ")
    _str_sql.AppendLine("          , MC_COLLECTED_PROMO_RE_TICKET_AMOUNT ")
    _str_sql.AppendLine("          , MC_COLLECTED_PROMO_NR_TICKET_COUNT                              ")
    _str_sql.AppendLine("          , MC_COLLECTED_PROMO_NR_TICKET_AMOUNT")

    _str_sql.AppendLine("          , ISNULL(MCD_NUM         , 0) AS MCD_NUM ")

    For Each _bill_data As TYPE_BILL_DATA In m_bill_data
      _str_sql.AppendLine("          , ISNULL(MCD_NUM_" + _bill_data.denomination + "       , 0) AS MCD_NUM_" + _bill_data.denomination + " ")
    Next

    _str_sql.AppendLine("          , ISNULL(MC_COLLECTED_COIN_AMOUNT, 0) AS MCD_TOTAL_COINS ")
    _str_sql.AppendLine("          , MCM_MONEY_COLLECTION_ID ")
    _str_sql.AppendLine("          , ISNULL(MCM_SESSION_ID      , 0) AS MCM_SESSION_ID ")
    _str_sql.AppendLine("          , ISNULL(MCM_TERMINAL_ID     , 0) AS MCM_TERMINAL_ID ")
    _str_sql.AppendLine("          , ISNULL(MCM_STARTED         , 0) AS MCM_STARTED ")
    _str_sql.AppendLine("          , ISNULL(MCM_FINISHED        , 0) AS MCM_FINISHED ")
    _str_sql.AppendLine("          , ISNULL(MCM_ERROR_REPORTING , 0) AS MCM_ERROR_REPORTING ")

    For Each _bill_data As TYPE_BILL_DATA In m_bill_data
      _str_sql.AppendLine("          , ISNULL(MCM_" + _bill_data.denomination + "_BILL_NUM       , 0) AS MCM_" + _bill_data.denomination + "_BILL_NUM ")
    Next

    _str_sql.AppendLine("          , ISNULL(MCM_CASHABLE_NUM     , 0) AS MCM_CASHABLE_NUM ")
    _str_sql.AppendLine("          , ISNULL(MCM_CASHABLE_AMOUNT  , 0) AS MCM_CASHABLE_AMOUNT ")
    _str_sql.AppendLine("          , ISNULL(MCM_PROMO_RE_NUM     , 0) AS MCM_PROMO_RE_NUM ")
    _str_sql.AppendLine("          , ISNULL(MCM_PROMO_RE_AMOUNT  , 0) AS MCM_PROMO_RE_AMOUNT ")
    _str_sql.AppendLine("          , ISNULL(MCM_PROMO_NR_NUM     , 0) AS MCM_PROMO_NR_NUM ")
    _str_sql.AppendLine("          , ISNULL(MCM_PROMO_NR_AMOUNT  , 0) AS MCM_PROMO_NR_AMOUNT ")
    _str_sql.AppendLine("          , MC_COLLECTED_BILL_AMOUNT ")

    _str_bills = ""
    For Each _bill_data As TYPE_BILL_DATA In m_bill_data
      _str_bills &= "+ ISNULL(MCM_" + _bill_data.denomination + "_BILL_NUM, 0) * " + _bill_data.denomination
    Next

    If _str_bills.Length > 0 Then
      _str_sql.AppendFormat("          , {0}  AS MCM_BILL_TOTAL_AMOUNT", _str_bills.Remove(0, 1))
    Else
      _str_sql.AppendLine("          , ' '  AS MCM_BILL_TOTAL_AMOUNT")
    End If

    _str_sql.AppendLine("          , ISNULL(TE_PROVIDER_ID, '') AS TE_PROVIDER ")

    _str_sql.AppendLine("          , ISNULL(MCM_TOTAL_COIN_AMOUNT  , 0) AS MCM_TOTAL_COIN_AMOUNT ")
    _str_sql.AppendLine("     FROM   #MONEY_COLLECTIONS ")
    _str_sql.AppendLine("     LEFT   JOIN TERMINALS ON MC_TERMINAL_ID = TE_TERMINAL_ID           ")
    _str_sql.AppendLine("     LEFT   JOIN GUI_USERS ON MC_USER_ID = GU_USER_ID                   ")
    _str_sql.AppendLine("     LEFT   JOIN #MONEY_COLLECTION_DETAILS ON MCD_COLLECTION_ID = MC_COLLECTION_ID ")
    _str_sql.AppendLine("     LEFT   JOIN #MONEY_COLLECTION_METERS  ON MCM_MONEY_COLLECTION_ID = MC_COLLECTION_ID ")

    If chk_group_by.Checked Then
      If opt_provider.Checked Then
        _str_sql.AppendLine(" ORDER BY   TE_PROVIDER_ID ASC, " & _order_by_date)
      ElseIf opt_terminal.Checked Then
        _str_sql.AppendLine(" ORDER BY   TE_NAME ASC,  " & _order_by_date)
      Else
        _str_sql.AppendLine(" ORDER BY " & _order_by_date)
      End If
    Else
      _str_sql.AppendLine(" ORDER BY " & _order_by_date)
    End If

    _str_sql.AppendLine("DROP TABLE #MONEY_COLLECTIONS ")
    _str_sql.AppendLine("DROP TABLE #MONEY_COLLECTION_DETAILS ")
    _str_sql.AppendLine("DROP TABLE #MONEY_COLLECTION_METERS ")

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  Protected Overrides Sub GUI_BeforeFirstRow()

    Me.Grid.Clear()

    Call GUI_StyleSheet()

    Call ResetSubtotals()

    Call ResetTotals()

    m_first_row = True

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _terminal_data As List(Of Object)
    Dim m_ti_cash_count As Integer
    Dim m_ti_cash_amount As Decimal
    Dim m_ti_prom_re_count As Integer
    Dim m_ti_prom_re_amount As Decimal
    Dim m_ti_prom_nr_count As Integer
    Dim m_ti_prom_nr_amount As Decimal

    With Me.Grid
      .Cell(RowIndex, GRID_COLUMN_TE_TERMINAL_ID).Value = mdl_tito.GetDefaultNumber(DbRow.Value(SQL_COLUMN_TE_TERMINAL_ID))
      .Cell(RowIndex, GRID_COLUMN_MC_COLLECTION_ID).Value = mdl_tito.GetDefaultNumber(DbRow.Value(SQL_COLUMN_MC_COLLECTION_ID))
      .Cell(RowIndex, GRID_COLUMN_MC_STACKER_ID).Value = mdl_tito.GetValueForGrid(DbRow.Value(SQL_COLUMN_MC_STACKER_ID))
      .Cell(RowIndex, GRID_COLUMN_MC_STATUS).Value = CollectionStatusAsString(mdl_tito.GetDefaultNumber(DbRow.Value(SQL_COLUMN_MC_STATUS)))

      _terminal_data = TerminalReport.GetReportDataList(mdl_tito.GetDefaultNumber(DbRow.Value(SQL_COLUMN_TE_TERMINAL_ID)), m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_data.Count - 1
        If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
          .Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
        End If
      Next

      .Cell(RowIndex, GRID_COLUMN_USER_NM).Value = DbRow.Value(SQL_COLUMN_USER_NM)
      .Cell(RowIndex, GRID_COLUMN_MC_COLLECTION_DATETIME).Value = mdl_tito.GetFormatedDate(DbRow.Value(SQL_COLUMN_MC_COLLECTION_DATETIME))
      .Cell(RowIndex, GRID_COLUMN_MC_DATETIME).Value = mdl_tito.GetFormatedDate(DbRow.Value(SQL_COLUMN_MC_DATETIME))
      .Cell(RowIndex, GRID_COLUMN_MC_EXTRACTION_DATETIME).Value = mdl_tito.GetFormatedDate(DbRow.Value(SQL_COLUMN_MC_EXTRACTION_DATETIME))
      .Cell(RowIndex, GRID_COLUMN_CAGE_SESSION_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_CAGE_DATE_TIME), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT) ' mdl_tito.GetFormatedDate(DbRow.Value(SQL_COLUMN_CAGE_DATE_TIME))
      .Cell(RowIndex, GRID_COLUMN_MC_CASHIER_SESSION_ID).Value = mdl_tito.GetDefaultNumber(DbRow.Value(SQL_COLUMN_MC_CASHIER_SESSION_ID))
      .Cell(RowIndex, GRID_COLUMN_MC_NOTES).Value = DbRow.Value(SQL_COLUMN_MC_NOTES)

      m_ti_cash_count = DbRow.Value(SQL_COLUMN_TI_CASHABLE_COUNT)
      m_ti_cash_amount = DbRow.Value(SQL_COLUMN_TI_CASHABLE_AMOUNT)
      m_ti_prom_re_count = DbRow.Value(SQL_COLUMN_TI_PROMO_RE_COUNT)
      m_ti_prom_re_amount = DbRow.Value(SQL_COLUMN_TI_PROMO_RE_AMOUNT)
      m_ti_prom_nr_count = DbRow.Value(SQL_COLUMN_TI_PROMO_NR_COUNT)
      m_ti_prom_nr_amount = DbRow.Value(SQL_COLUMN_TI_PROMO_NR_AMOUNT)

      .Cell(RowIndex, GRID_COLUMN_TI_CASHABLE_COUNT).Value = mdl_tito.GetDefaultNumber(m_ti_cash_count)
      .Cell(RowIndex, GRID_COLUMN_TI_CASHABLE_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_ti_cash_amount)
      .Cell(RowIndex, GRID_COLUMN_TI_PROMO_RE_COUNT).Value = mdl_tito.GetDefaultNumber(m_ti_prom_re_count)
      .Cell(RowIndex, GRID_COLUMN_TI_PROMO_RE_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_ti_prom_re_amount)
      .Cell(RowIndex, GRID_COLUMN_TI_PROMO_NR_COUNT).Value = mdl_tito.GetDefaultNumber(m_ti_prom_nr_count)
      .Cell(RowIndex, GRID_COLUMN_TI_PROMO_NR_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_ti_prom_nr_amount)
      .Cell(RowIndex, GRID_COLUMN_TI_TOTAL_COUNT).Value = mdl_tito.GetDefaultNumber(m_ti_cash_count + m_ti_prom_re_count + m_ti_prom_nr_count)
      .Cell(RowIndex, GRID_COLUMN_TI_TOTAL_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_ti_cash_amount + m_ti_prom_re_amount + m_ti_prom_nr_amount)
      .Cell(RowIndex, GRID_COLUMN_MCD_NUM).Value = GUI_FormatNumber(mdl_tito.GetDefaultNumber(DbRow.Value(SQL_COLUMN_MCD_NUM)), 0)


      For _idx As Int32 = 0 To m_bill_data.Count - 1
        .Cell(RowIndex, GRID_COLUMN_COLLECTED_BILL_FIRST + _idx).Value = GUI_FormatNumber(mdl_tito.GetDefaultNumber(DbRow.Value(SQL_COLUMN_COLLECTED_BILL_FIRST + _idx)), 0)
      Next

      .Cell(RowIndex, GRID_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT).Value = mdl_tito.GetFormatedCurrency(DbRow.Value(SQL_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT))

      .Cell(RowIndex, GRID_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT).Value = mdl_tito.GetFormatedCurrency(DbRow.Value(SQL_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT))

      .Cell(RowIndex, GRID_COLUMN_MCD_TOTAL_CASH).Value = mdl_tito.GetFormatedCurrency(DbRow.Value(SQL_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT) + DbRow.Value(SQL_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT))


      .Cell(RowIndex, GRID_COLUMN_MCM_MONEY_COLLECTION_ID).Value = mdl_tito.GetDefaultNumber(DbRow.Value(SQL_COLUMN_MCM_MONEY_COLLECTION_ID))
      .Cell(RowIndex, GRID_COLUMN_MCM_SESSION_ID).Value = mdl_tito.GetDefaultNumber(DbRow.Value(SQL_COLUMN_MCM_SESSION_ID))
      .Cell(RowIndex, GRID_COLUMN_MCM_TERMINAL_ID).Value = mdl_tito.GetDefaultNumber(DbRow.Value(SQL_COLUMN_MCM_TERMINAL_ID))
      .Cell(RowIndex, GRID_COLUMN_MCM_STARTED).Value = mdl_tito.GetFormatedDate(DbRow.Value(SQL_COLUMN_MCM_STARTED))
      .Cell(RowIndex, GRID_COLUMN_MCM_FINISHED).Value = mdl_tito.GetFormatedDate(DbRow.Value(SQL_COLUMN_MCM_FINISHED))
      .Cell(RowIndex, GRID_COLUMN_MCM_ERROR_REPORTING).Value = DbRow.Value(SQL_COLUMN_MCM_ERROR_REPORTING)
      .Cell(RowIndex, GRID_COLUMN_MCM_CASHABLE_AMOUNT).Value = mdl_tito.GetFormatedCurrency(DbRow.Value(SQL_COLUMN_MCM_CASHABLE_AMOUNT))
      .Cell(RowIndex, GRID_COLUMN_MCM_PROMO_RE_NUM).Value = mdl_tito.GetDefaultNumber(DbRow.Value(SQL_COLUMN_MCM_PROMO_RE_NUM))
      .Cell(RowIndex, GRID_COLUMN_MCM_PROMO_RE_AMOUNT).Value = mdl_tito.GetFormatedCurrency(DbRow.Value(SQL_COLUMN_MCM_PROMO_RE_AMOUNT))
      .Cell(RowIndex, GRID_COLUMN_MCM_PROMO_NR_NUM).Value = mdl_tito.GetDefaultNumber(DbRow.Value(SQL_COLUMN_MCM_PROMO_NR_NUM))
      .Cell(RowIndex, GRID_COLUMN_MCM_PROMO_NR_AMOUNT).Value = mdl_tito.GetFormatedCurrency(DbRow.Value(SQL_COLUMN_MCM_PROMO_NR_AMOUNT))

      For _idx As Int32 = 0 To m_bill_data.Count - 1
        .Cell(RowIndex, GRID_COLUMN_EXPECTED_BILL_FIRST + _idx).Value = GUI_FormatNumber(mdl_tito.GetDefaultNumber(DbRow.Value(SQL_COLUMN_EXPECTED_BILL_FIRST + _idx)), 0)
      Next

      .Cell(RowIndex, GRID_COLUMN_DIFF_BILL_AMOUNT).Value = GUI_FormatCurrency(CalculateDifferenceAmount(DbRow, CageCurrencyType.Bill))
      .Cell(RowIndex, GRID_COLUMN_DIFF_TICKET_AMOUNT).Value = GUI_FormatCurrency(CalculateDifferenceAmount(DbRow, CageCurrencyType.Others))
      .Cell(RowIndex, GRID_COLUMN_DIFF_COIN_AMOUNT).Value = GUI_FormatCurrency(CalculateDifferenceAmount(DbRow, CageCurrencyType.Coin))
      .Cell(RowIndex, GRID_COLUMN_DIFF_CASH_AMOUNT).Value = GUI_FormatCurrency(CalculateDifferenceAmount(DbRow, CageCurrencyType.Others, True))

      .Cell(RowIndex, GRID_COLUMN_DIFF_BILL_PERCENTAJE).Value = GUI_FormatNumber(CalculateDifferencePercentaje(DbRow, CageCurrencyType.Bill), 2) & "%"
      .Cell(RowIndex, GRID_COLUMN_DIFF_TICKET_PERCENTAJE).Value = GUI_FormatNumber(CalculateDifferencePercentaje(DbRow, CageCurrencyType.Others), 2) & "%"
      .Cell(RowIndex, GRID_COLUMN_DIFF_COIN_PERCENTAJE).Value = GUI_FormatNumber(CalculateDifferencePercentaje(DbRow, CageCurrencyType.Coin), 2) & "%"
      .Cell(RowIndex, GRID_COLUMN_DIFF_CASH_PERCENTAJE).Value = GUI_FormatNumber(CalculateDifferencePercentaje(DbRow, CageCurrencyType.Others, True), 2) & "%"

      .Cell(RowIndex, GRID_COLUMN_MCM_CASHABLE_NUM).Value = mdl_tito.GetDefaultNumber(DbRow.Value(SQL_COLUMN_MCM_CASHABLE_NUM))
      .Cell(RowIndex, GRID_COLUMN_MCM_BILL_TOTAL_AMOUNT).Value = mdl_tito.GetFormatedCurrency(DbRow.Value(SQL_COLUMN_MCM_BILL_TOTAL_AMOUNT))
      .Cell(RowIndex, GRID_COLUMN_MCM_TOTAL_COIN_AMOUNT).Value = mdl_tito.GetFormatedCurrency(DbRow.Value(SQL_COLUMN_MCM_TOTAL_COIN_AMOUNT))
      .Cell(RowIndex, GRID_COLUMN_MCM_TOTAL_CASH).Value = mdl_tito.GetFormatedCurrency(DbRow.Value(SQL_COLUMN_MCM_BILL_TOTAL_AMOUNT) + DbRow.Value(SQL_COLUMN_MCM_TOTAL_COIN_AMOUNT))

    End With

    Call CheckAndPaintDataRow(RowIndex, DbRow)

    'If Not m_row_deleted Then
    '  Call SumToSubTotals(DbRow)
    '  Call SumToTotals(DbRow)
    'End If

    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_AfterLastRow()

    Dim _row_index As Integer
    Dim _bill_data As TYPE_BILL_DATA

    m_row_deleted = False


    If chk_group_by.Checked AndAlso m_insert_last_row Then
      If opt_group_by_day.Checked Then
        Call PrintSubtotal(ENUM_SUBTOTAL_SHOW_TYPE.SHOW_SUBTOTAL_BY_DATE, m_index_to_print_subtotal_date)
      ElseIf opt_provider.Checked Then
        Call PrintSubtotal(ENUM_SUBTOTAL_SHOW_TYPE.SHOW_SUBTOTAL_BY_PROVIDER)
      Else
        Call PrintSubtotal(ENUM_SUBTOTAL_SHOW_TYPE.SHOW_SUBTOTAL_BY_TERMINAL)
      End If
    End If


    Me.Grid.AddRow()
    _row_index = Me.Grid.NumRows - 1

    With Me.Grid

      .Cell(_row_index, GRID_COLUMN_TI_CASHABLE_COUNT).Value = mdl_tito.GetDefaultNumber(m_ti_cashable_count)
      .Cell(_row_index, GRID_COLUMN_TI_CASHABLE_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_ti_cashable_amount)
      .Cell(_row_index, GRID_COLUMN_TI_PROMO_RE_COUNT).Value = mdl_tito.GetDefaultNumber(m_ti_promo_re_count)
      .Cell(_row_index, GRID_COLUMN_TI_PROMO_RE_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_ti_promo_re_amount)
      .Cell(_row_index, GRID_COLUMN_TI_PROMO_NR_COUNT).Value = mdl_tito.GetDefaultNumber(m_ti_promo_nr_count)
      .Cell(_row_index, GRID_COLUMN_TI_PROMO_NR_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_ti_promo_nr_amount)
      .Cell(_row_index, GRID_COLUMN_TI_TOTAL_COUNT).Value = mdl_tito.GetDefaultNumber(m_ti_total_count)
      .Cell(_row_index, GRID_COLUMN_TI_TOTAL_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_ti_total_amount)
      .Cell(_row_index, GRID_COLUMN_MCD_NUM).Value = mdl_tito.GetDefaultNumber(m_mcd_num)

      For _idx As Int32 = 0 To m_bill_data.Count - 1
        _bill_data = m_bill_data(_idx)
        .Cell(_row_index, GRID_COLUMN_COLLECTED_BILL_FIRST + _idx).Value = mdl_tito.GetDefaultNumber(_bill_data.total_collected)
        .Cell(_row_index, GRID_COLUMN_EXPECTED_BILL_FIRST + _idx).Value = mdl_tito.GetDefaultNumber(_bill_data.total_expected)
      Next

      .Cell(_row_index, GRID_COLUMN_MCM_CASHABLE_NUM).Value = mdl_tito.GetDefaultNumber(m_mcm_cashable_num)
      .Cell(_row_index, GRID_COLUMN_MCM_CASHABLE_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_mcm_cashable_amount)
      .Cell(_row_index, GRID_COLUMN_MCM_PROMO_NR_NUM).Value = mdl_tito.GetDefaultNumber(m_mcm_promo_nr_num)
      .Cell(_row_index, GRID_COLUMN_MCM_PROMO_NR_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_mcm_promo_nr_amount)
      .Cell(_row_index, GRID_COLUMN_MCM_PROMO_RE_NUM).Value = mdl_tito.GetDefaultNumber(m_mcm_promo_re_num)
      .Cell(_row_index, GRID_COLUMN_MCM_PROMO_RE_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_mcm_promo_re_amount)
      .Cell(_row_index, GRID_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_mcd_collected_total_amount)
      .Cell(_row_index, GRID_COLUMN_MCM_BILL_TOTAL_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_mcm_bill_total_amount)

      .Cell(_row_index, GRID_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_mcd_collected_total_coin_amount)

      .Cell(_row_index, GRID_COLUMN_MCD_TOTAL_CASH).Value = mdl_tito.GetFormatedCurrency(m_mcd_collected_total_amount + m_mcd_collected_total_coin_amount)

      .Cell(_row_index, GRID_COLUMN_MCM_TOTAL_COIN_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_mcm_total_coin_amount)

      .Cell(_row_index, GRID_COLUMN_MCM_TOTAL_CASH).Value = mdl_tito.GetFormatedCurrency(m_mcm_bill_total_amount + m_mcm_total_coin_amount)

      .Cell(_row_index, GRID_COLUMN_DIFF_TICKET_AMOUNT).Value = GUI_FormatCurrency(CalculateTicketDifferenceTotalAmount(m_ti_cashable_amount, m_ti_promo_re_amount, m_ti_promo_nr_amount, _
                                                                                                                m_mcm_cashable_amount, m_mcm_promo_nr_amount, m_mcm_promo_re_amount))
      .Cell(_row_index, GRID_COLUMN_DIFF_TICKET_PERCENTAJE).Value = GUI_FormatNumber(CalculateDifferenceTotalPercentaje(m_ti_cashable_amount + m_ti_promo_re_amount + m_ti_promo_nr_amount, _
                                                                                                                        m_mcm_cashable_amount + m_mcm_promo_nr_amount + m_mcm_promo_re_amount), _
                                                                                     2) & "%"
      .Cell(_row_index, GRID_COLUMN_DIFF_BILL_AMOUNT).Value = GUI_FormatCurrency(m_mcd_collected_total_amount - m_mcm_bill_total_amount)
      .Cell(_row_index, GRID_COLUMN_DIFF_BILL_PERCENTAJE).Value = GUI_FormatNumber(CalculateDifferenceTotalPercentaje(m_mcd_collected_total_amount, m_mcm_bill_total_amount), _
                                                                                   2) & "%"

      .Cell(_row_index, GRID_COLUMN_DIFF_COIN_AMOUNT).Value = GUI_FormatCurrency(m_mcd_collected_total_coin_amount - m_mcm_total_coin_amount)
      .Cell(_row_index, GRID_COLUMN_DIFF_COIN_PERCENTAJE).Value = GUI_FormatNumber(CalculateDifferenceTotalPercentaje(m_mcd_collected_total_coin_amount, m_mcm_total_coin_amount), _
                                                                                   2) & "%"

      .Cell(_row_index, GRID_COLUMN_DIFF_CASH_AMOUNT).Value = GUI_FormatCurrency(m_mcd_collected_total_amount + m_mcd_collected_total_coin_amount - (m_mcm_bill_total_amount + m_mcm_total_coin_amount))
      .Cell(_row_index, GRID_COLUMN_DIFF_CASH_PERCENTAJE).Value = GUI_FormatNumber(CalculateDifferenceTotalPercentaje(m_mcd_collected_total_amount + m_mcd_collected_total_coin_amount, m_mcm_bill_total_amount + m_mcm_total_coin_amount), _
                                                                                   2) & "%"

    End With

    Me.Grid.Row(_row_index).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    If Not Me.cb_show_all_denominations.Checked Then

      With Me.Grid

        For _idx As Int32 = 0 To m_bill_data.Count - 1
          _bill_data = m_bill_data(_idx)
          If _bill_data.total_collected = 0 Then
            .Column(GRID_COLUMN_COLLECTED_BILL_FIRST + _idx).Header(1).Text = ""
            .Column(GRID_COLUMN_COLLECTED_BILL_FIRST + _idx).Width = 0
          End If
          If _bill_data.total_expected = 0 Then
            .Column(GRID_COLUMN_EXPECTED_BILL_FIRST + _idx).Header(1).Text = ""
            .Column(GRID_COLUMN_EXPECTED_BILL_FIRST + _idx).Width = 0
          End If
        Next

      End With
    End If 'If Me.Grid.NumRows > 0 Then

    Me.Grid.ClearSelection()
    Me.Grid.SelectFirstRow(True)

  End Sub  ' GUI_AfterLastRow

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean
    Dim _show_normal_row As Boolean
    Dim _insert_row As Boolean
    _insert_row = False

    m_row_deleted = False
    _show_normal_row = True

    _insert_row = RowHasToBeAdded(DbRow)
    If _insert_row Then
      If chk_group_by.Checked Then
        _show_normal_row = chk_show_detail.Checked
        Call GetIndexDateSubtotal()
        If Not m_first_row Then
          If opt_group_by_day.Checked AndAlso m_subtotal_date <> CType(DbRow.Value(m_index_of_date_to_calculate_subtotal), Date).Date Then
            Call PrintSubtotal(ENUM_SUBTOTAL_SHOW_TYPE.SHOW_SUBTOTAL_BY_DATE, m_index_to_print_subtotal_date)
          ElseIf opt_provider.Checked AndAlso m_subtotal_provider_id <> DbRow.Value(SQL_COLUMN_PROVIDER_ID).ToString() Then
            Call PrintSubtotal(ENUM_SUBTOTAL_SHOW_TYPE.SHOW_SUBTOTAL_BY_PROVIDER)
          ElseIf opt_terminal.Checked AndAlso m_subtotal_te_name <> DbRow.Value(SQL_COLUMN_TE_NAME).ToString() Then
            Call PrintSubtotal(ENUM_SUBTOTAL_SHOW_TYPE.SHOW_SUBTOTAL_BY_TERMINAL)
          End If
        Else
          m_first_row = False
        End If
        Call SumToSubTotals(DbRow)
        _insert_row = chk_show_detail.Checked
      End If
      Call SumToTotals(DbRow)
    End If

    Return _insert_row
  End Function

#Region " GUI Reports "

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""
    m_options = ""
    m_show_details = False
    m_insert_last_row = False

    If Me.ds_from_to.FromDateSelected Then
      m_date_from = GUI_FormatDate(ds_from_to.FromDate.AddHours(ds_from_to.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.ds_from_to.ToDateSelected Then
      m_date_to = GUI_FormatDate(ds_from_to.ToDate.AddHours(ds_from_to.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Terminals 
    m_terminals = Me.uc_provider.GetTerminalReportText()

    ' Options
    If Me.chk_group_by.Checked Then
      m_options = chk_group_by.Text
      If opt_group_by_day.Checked Then
        m_options = m_options & " " & opt_group_by_day.Text
      ElseIf opt_provider.Checked Then
        m_options = m_options & " " & opt_provider.Text
      Else
        m_options = m_options & " " & opt_terminal.Text
      End If
      m_show_details = Me.chk_show_detail.Checked

    End If

  End Sub ' GUI_ReportUpdateFilters

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    'dates
    'PrintData.SetFilter(GLB_NLS_GUI_CLASS_II.GetString(230), m_date_from)
    'PrintData.SetFilter(GLB_NLS_GUI_CLASS_II.GetString(231), m_date_to)

    If opt_insertion_datetime.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5739) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5737), m_date_from)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5739) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5738), m_date_to)
    ElseIf opt_collection_datetime.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5740) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5737), m_date_from)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5740) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5738), m_date_to)
    ElseIf opt_cage_session_datetime.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5741) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5737), m_date_from)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5741) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5738), m_date_to)
    End If

    If cb_show_only_out_of_balance.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(648), GLB_NLS_GUI_PLAYER_TRACKING.GetString(278))
    Else
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(648), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279))
    End If

    If cb_show_all_denominations.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5416), GLB_NLS_GUI_PLAYER_TRACKING.GetString(278))
    Else
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5416), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279))
    End If

    If cb_terminal_location.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237), GLB_NLS_GUI_PLAYER_TRACKING.GetString(278))
    Else
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279))
    End If

    ' Terminals
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)

    'options
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(369), m_options)
    If Not String.IsNullOrEmpty(m_options) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5721), IIf(m_show_details, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))
    End If


    PrintData.FilterHeaderWidth(1) = 2000
    PrintData.FilterHeaderWidth(2) = 2500
    PrintData.FilterHeaderWidth(3) = 2500
    PrintData.FilterValueWidth(2) = 2100
    PrintData.FilterValueWidth(4) = 2100

  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

  End Sub ' GUI_ReportParams

#End Region ' GUI Reports

#End Region ' OVERRIDES

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions and Methods "

  Private Function CalculateDifferencePercentaje(ByVal Row As CLASS_DB_ROW, ByVal CurrencyType As CageCurrencyType, Optional ByVal IsTotalCash As Boolean = False) As Decimal
    Dim _percentage As Decimal
    Dim _difference As Decimal

    _difference = CalculateDifferenceAmount(Row, CurrencyType, IsTotalCash)

    'SGB 18-NOV-2014 Check show correct percentage
    If IsTotalCash Then
      Dim _amounts As Decimal
      _amounts = 0

      If Not IsDBNull(Row.Value(SQL_COLUMN_MCM_BILL_TOTAL_AMOUNT)) AndAlso Row.Value(SQL_COLUMN_MCM_BILL_TOTAL_AMOUNT) > 0 Then
        _amounts += Row.Value(SQL_COLUMN_MCM_BILL_TOTAL_AMOUNT)
      End If

      If Not IsDBNull(Row.Value(SQL_COLUMN_MCM_TOTAL_COIN_AMOUNT)) AndAlso Row.Value(SQL_COLUMN_MCM_TOTAL_COIN_AMOUNT) > 0 Then
        _amounts += Row.Value(SQL_COLUMN_MCM_TOTAL_COIN_AMOUNT)
      End If

      If _amounts > 0 Then
        _percentage = (_difference / _amounts) * 100
      Else
        If _difference > 0 Then
          _percentage = 100
        Else
          _percentage = 0
        End If
      End If

    Else
      Select Case CurrencyType
        Case CageCurrencyType.Bill
          If Not IsDBNull(Row.Value(SQL_COLUMN_MCM_BILL_TOTAL_AMOUNT)) AndAlso Row.Value(SQL_COLUMN_MCM_BILL_TOTAL_AMOUNT) > 0 Then
            _percentage = (_difference / Row.Value(SQL_COLUMN_MCM_BILL_TOTAL_AMOUNT)) * 100
          End If
        Case CageCurrencyType.Coin
          If Not IsDBNull(Row.Value(SQL_COLUMN_MCM_TOTAL_COIN_AMOUNT)) AndAlso Row.Value(SQL_COLUMN_MCM_TOTAL_COIN_AMOUNT) > 0 Then
            _percentage = (_difference / Row.Value(SQL_COLUMN_MCM_TOTAL_COIN_AMOUNT)) * 100
          End If
        Case Else
          If SumCountersTicketAmounts(Row) <> 0 Then
            _percentage = (_difference / SumCountersTicketAmounts(Row)) * 100
          End If
      End Select

      If _percentage = 0 And (Not IsDBNull(Row.Value(SQL_COLUMN_MCM_BILL_TOTAL_AMOUNT)) Or Not IsDBNull(Row.Value(SQL_COLUMN_MCM_TOTAL_COIN_AMOUNT))) Then
        If _difference > 0 Then
          _percentage = 100
        End If
      End If

    End If

    Return _percentage
  End Function

  Private Function CalculateDifferenceAmount(ByVal Row As CLASS_DB_ROW, ByVal CurrencyType As CageCurrencyType, Optional ByVal IsTotalCash As Boolean = False) As Decimal
    Dim collected_amount As Decimal
    Dim counter_amount As Decimal

    collected_amount = 0
    counter_amount = 0

    If IsTotalCash Then
      'Sum Bills
      If Not IsDBNull(Row.Value(SQL_COLUMN_MCM_BILL_TOTAL_AMOUNT)) Then
        counter_amount += Row.Value(SQL_COLUMN_MCM_BILL_TOTAL_AMOUNT)
      End If
      If Not IsDBNull(Row.Value(SQL_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT)) Then
        collected_amount += Row.Value(SQL_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT)
      End If

      'Sum Coins
      If Not IsDBNull(Row.Value(SQL_COLUMN_MCM_TOTAL_COIN_AMOUNT)) Then
        counter_amount += Row.Value(SQL_COLUMN_MCM_TOTAL_COIN_AMOUNT)
      End If
      If Not IsDBNull(Row.Value(SQL_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT)) Then
        collected_amount += Row.Value(SQL_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT)
      End If

      Return collected_amount - counter_amount
    End If


    Select Case CurrencyType
      Case WSI.Common.CageCurrencyType.Bill
        If Not IsDBNull(Row.Value(SQL_COLUMN_MCM_BILL_TOTAL_AMOUNT)) Then
          counter_amount = Row.Value(SQL_COLUMN_MCM_BILL_TOTAL_AMOUNT)
        End If
        If Not IsDBNull(Row.Value(SQL_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT)) Then
          collected_amount = Row.Value(SQL_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT)
        End If
      Case WSI.Common.CageCurrencyType.Coin
        If Not IsDBNull(Row.Value(SQL_COLUMN_MCM_TOTAL_COIN_AMOUNT)) Then
          counter_amount = Row.Value(SQL_COLUMN_MCM_TOTAL_COIN_AMOUNT)
        End If
        If Not IsDBNull(Row.Value(SQL_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT)) Then
          collected_amount = Row.Value(SQL_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT)
        End If
      Case Else
        'Tickets
        collected_amount = SumCollectedTicketAmounts(Row)
        counter_amount = SumCountersTicketAmounts(Row)
    End Select

    Return collected_amount - counter_amount
  End Function

  Private Function SumCollectedTicketAmounts(ByVal row As CLASS_DB_ROW)
    Dim _amount As Decimal

    _amount = 0
    If Not IsDBNull(row.Value(SQL_COLUMN_TI_CASHABLE_AMOUNT)) Then
      _amount = row.Value(SQL_COLUMN_TI_CASHABLE_AMOUNT)
    End If
    If Not IsDBNull(row.Value(SQL_COLUMN_TI_PROMO_RE_AMOUNT)) Then
      _amount = _amount + row.Value(SQL_COLUMN_TI_PROMO_RE_AMOUNT)
    End If
    If Not IsDBNull(row.Value(SQL_COLUMN_TI_PROMO_NR_AMOUNT)) Then
      _amount = _amount + row.Value(SQL_COLUMN_TI_PROMO_NR_AMOUNT)
    End If

    Return _amount
  End Function

  Private Function SumCountersTicketAmounts(ByVal row As CLASS_DB_ROW)
    Dim _amount As Decimal

    _amount = 0
    If Not IsDBNull(row.Value(SQL_COLUMN_MCM_CASHABLE_AMOUNT)) Then
      _amount = row.Value(SQL_COLUMN_MCM_CASHABLE_AMOUNT)
    End If
    If Not IsDBNull(row.Value(SQL_COLUMN_MCM_PROMO_RE_AMOUNT)) Then
      _amount = _amount + row.Value(SQL_COLUMN_MCM_PROMO_RE_AMOUNT)
    End If
    If Not IsDBNull(row.Value(SQL_COLUMN_MCM_PROMO_NR_AMOUNT)) Then
      _amount = _amount + row.Value(SQL_COLUMN_MCM_PROMO_NR_AMOUNT)
    End If

    Return _amount
  End Function

  Private Function CalculateDifferenceTotalPercentaje(ByVal CollectedValue As Decimal, ByVal CounterValue As Decimal) As Decimal
    Dim _percentaje As Decimal
    If CounterValue > 0 Then
      _percentaje = ((CollectedValue - CounterValue) / CounterValue) * 100
    Else
      If CollectedValue > 0 Then
        _percentaje = 100
      Else
        _percentaje = 0
      End If
    End If

    Return _percentaje
  End Function

  Private Function CalculateTicketDifferenceTotalAmount(ByVal CollectedTicketRe As Decimal, ByVal CollectedTicketPromoRe As Decimal, ByVal CollectedTicketPromoNR As Decimal, _
                                                ByVal CounterTicketRe As Decimal, ByVal CounterTicketPromoRe As Decimal, ByVal CounterTicketPromoNR As Decimal) As Decimal
    Dim _collected_ticket_amount As Decimal
    Dim _counter_ticket_amount As Decimal

    _collected_ticket_amount = CollectedTicketPromoNR + CollectedTicketPromoRe + CollectedTicketRe
    _counter_ticket_amount = CounterTicketPromoNR + CounterTicketPromoRe + CounterTicketRe


    Return _collected_ticket_amount - _counter_ticket_amount
  End Function
  ' PURPOSE: Set default values to filters
  '-
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim _today_opening As Date

    'Dates
    _today_opening = WSI.Common.Misc.TodayOpening()
    ds_from_to.FromDate = _today_opening
    ds_from_to.FromDateSelected = True
    ds_from_to.ToDate = _today_opening.AddDays(1)
    ds_from_to.ToDateSelected = True
    ds_from_to.ClosingTime = _today_opening.Hour

    ' Terminals
    Call Me.uc_provider.SetDefaultValues()

    ' Show only out of balance
    cb_show_only_out_of_balance.Checked = False

    ' Show all columns
    cb_show_all_denominations.Checked = False

    cb_terminal_location.Checked = False

    chk_group_by.Checked = False
    opt_group_by_day.Checked = True
    chk_show_detail.Checked = True
    opt_insertion_datetime.Checked = True

  End Sub ' SetDefaultValues

  ' PURPOSE : Define layout of all Main Grid Columns
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  Private Sub GUI_StyleSheet()
    Dim _bill_data As TYPE_BILL_DATA
    Dim _terminal_columns As List(Of ColumnSettings)

    Call UpdateGridColumns()

    With Me.Grid
      Call .Init(GRID_COLUMNS_COUNT, GRID_HEADER_ROWS, True)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = False

      .Counter(COUNTER_OK).Visible = True
      .Counter(COUNTER_OK).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      .Counter(COUNTER_OK).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      .Counter(COUNTER_DIFF).Visible = True
      .Counter(COUNTER_DIFF).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_00)
      .Counter(COUNTER_DIFF).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      '.Counter(COUNTER_ERROR).Visible = True
      '.Counter(COUNTER_ERROR).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_ORANGE_00)
      '.Counter(COUNTER_ERROR).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      .Column(GRID_COLUMN_SELECT).Header(0).Text = ""
      .Column(GRID_COLUMN_SELECT).Header(1).Text = ""
      .Column(GRID_COLUMN_SELECT).Width = 200
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False
      .Column(GRID_COLUMN_SELECT).HighLightWhenSelected = False

      .Column(GRID_COLUMN_MC_COLLECTION_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3336)
      .Column(GRID_COLUMN_MC_COLLECTION_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3343)
      .Column(GRID_COLUMN_MC_COLLECTION_ID).Width = 0

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To Math.Min(_terminal_columns.Count - 1, TERMINAL_DATA_COLUMNS - 1)
        '.if it is terminal, use header 0
        If _terminal_columns(_idx).Column = ReportColumn.Terminal Then
          GRID_COLUMN_TE_NAME = GRID_INIT_TERMINAL_DATA + _idx
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header0
        Else
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        End If
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3336)
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        If _terminal_columns(_idx).Column = ReportColumn.Provider Then
          GRID_COLUMN_TE_PROVIDER = GRID_INIT_TERMINAL_DATA + _idx
        End If
      Next

      .Column(GRID_COLUMN_MC_STACKER_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3336)
      .Column(GRID_COLUMN_MC_STACKER_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3344)
      .Column(GRID_COLUMN_MC_STACKER_ID).Width = 1200

      .Column(GRID_COLUMN_MC_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3336)
      .Column(GRID_COLUMN_MC_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3346)
      .Column(GRID_COLUMN_MC_STATUS).Width = 2400
      .Column(GRID_COLUMN_MC_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_USER_NM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3336)
      .Column(GRID_COLUMN_USER_NM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3347)
      .Column(GRID_COLUMN_USER_NM).Width = 2000
      .Column(GRID_COLUMN_USER_NM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_MC_DATETIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3336)
      .Column(GRID_COLUMN_MC_DATETIME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2251)
      .Column(GRID_COLUMN_MC_DATETIME).Width = 2000
      .Column(GRID_COLUMN_MC_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_MC_EXTRACTION_DATETIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3336)
      .Column(GRID_COLUMN_MC_EXTRACTION_DATETIME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2263)
      .Column(GRID_COLUMN_MC_EXTRACTION_DATETIME).Width = 2000
      .Column(GRID_COLUMN_MC_EXTRACTION_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER


      .Column(GRID_COLUMN_MC_COLLECTION_DATETIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3336)
      .Column(GRID_COLUMN_MC_COLLECTION_DATETIME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2206)
      .Column(GRID_COLUMN_MC_COLLECTION_DATETIME).Width = 2000
      .Column(GRID_COLUMN_MC_COLLECTION_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_CAGE_SESSION_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3336)
      .Column(GRID_COLUMN_CAGE_SESSION_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5715)
      .Column(GRID_COLUMN_CAGE_SESSION_DATE).Width = 2600
      .Column(GRID_COLUMN_CAGE_SESSION_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_MC_CASHIER_SESSION_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3336)
      .Column(GRID_COLUMN_MC_CASHIER_SESSION_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3348)
      .Column(GRID_COLUMN_MC_CASHIER_SESSION_ID).Width = 0

      .Column(GRID_COLUMN_MC_NOTES).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3336)
      .Column(GRID_COLUMN_MC_NOTES).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3349)
      .Column(GRID_COLUMN_MC_NOTES).Width = 5000
      .Column(GRID_COLUMN_MC_NOTES).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_TI_CASHABLE_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3337)
      .Column(GRID_COLUMN_TI_CASHABLE_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6137)
      .Column(GRID_COLUMN_TI_CASHABLE_COUNT).Width = WIDTH_TICKETS

      .Column(GRID_COLUMN_TI_CASHABLE_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3337)
      .Column(GRID_COLUMN_TI_CASHABLE_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3350)
      .Column(GRID_COLUMN_TI_CASHABLE_AMOUNT).Width = WIDTH_TICKETS

      .Column(GRID_COLUMN_TI_PROMO_RE_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3337)
      .Column(GRID_COLUMN_TI_PROMO_RE_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6138)
      .Column(GRID_COLUMN_TI_PROMO_RE_COUNT).Width = WIDTH_TICKETS

      .Column(GRID_COLUMN_TI_PROMO_RE_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3337)
      .Column(GRID_COLUMN_TI_PROMO_RE_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3351)
      .Column(GRID_COLUMN_TI_PROMO_RE_AMOUNT).Width = WIDTH_TICKETS

      .Column(GRID_COLUMN_TI_PROMO_NR_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3337)
      .Column(GRID_COLUMN_TI_PROMO_NR_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6139)
      .Column(GRID_COLUMN_TI_PROMO_NR_COUNT).Width = WIDTH_TICKETS

      .Column(GRID_COLUMN_TI_PROMO_NR_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3337)
      .Column(GRID_COLUMN_TI_PROMO_NR_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3352)
      .Column(GRID_COLUMN_TI_PROMO_NR_AMOUNT).Width = WIDTH_TICKETS

      .Column(GRID_COLUMN_TI_TOTAL_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3337)
      .Column(GRID_COLUMN_TI_TOTAL_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6140)
      .Column(GRID_COLUMN_TI_TOTAL_COUNT).Width = WIDTH_TICKETS

      .Column(GRID_COLUMN_TI_TOTAL_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3337)
      .Column(GRID_COLUMN_TI_TOTAL_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6141)
      .Column(GRID_COLUMN_TI_TOTAL_AMOUNT).Width = WIDTH_TICKETS

      .Column(GRID_COLUMN_MCD_NUM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3337)
      .Column(GRID_COLUMN_MCD_NUM).Header(1).Text = ""
      .Column(GRID_COLUMN_MCD_NUM).Width = 0

      For _idx As Int32 = 0 To m_bill_data.Count - 1
        _bill_data = m_bill_data(_idx)
        .Column(GRID_COLUMN_COLLECTED_BILL_FIRST + _idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3337)
        .Column(GRID_COLUMN_COLLECTED_BILL_FIRST + _idx).Header(1).Text = _bill_data.denomination_with_symbol
        .Column(GRID_COLUMN_COLLECTED_BILL_FIRST + _idx).Width = WIDTH_BILLS_DET
      Next

      .Column(GRID_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3337)
      .Column(GRID_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4333)
      .Column(GRID_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT).Width = WIDTH_BILLS_TOT

      .Column(GRID_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3337)
      .Column(GRID_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5616)
      .Column(GRID_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT).Width = WIDTH_BILLS_TOT

      .Column(GRID_COLUMN_MCD_TOTAL_CASH).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3337)
      .Column(GRID_COLUMN_MCD_TOTAL_CASH).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5612)
      .Column(GRID_COLUMN_MCD_TOTAL_CASH).Width = WIDTH_BILLS_TOT

      .Column(GRID_COLUMN_MCM_MONEY_COLLECTION_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3338)
      .Column(GRID_COLUMN_MCM_MONEY_COLLECTION_ID).Header(1).Text = "" 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(3343)
      .Column(GRID_COLUMN_MCM_MONEY_COLLECTION_ID).Width = 0

      .Column(GRID_COLUMN_MCM_SESSION_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3338)
      .Column(GRID_COLUMN_MCM_SESSION_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3354)
      .Column(GRID_COLUMN_MCM_SESSION_ID).Width = 0

      .Column(GRID_COLUMN_MCM_TERMINAL_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3338)
      .Column(GRID_COLUMN_MCM_TERMINAL_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_MCM_TERMINAL_ID).Width = 0
      .Column(GRID_COLUMN_MCM_TERMINAL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_MCM_STARTED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3338)
      .Column(GRID_COLUMN_MCM_STARTED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3355)
      .Column(GRID_COLUMN_MCM_STARTED).Width = 0
      .Column(GRID_COLUMN_MCM_STARTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_MCM_FINISHED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3338)
      .Column(GRID_COLUMN_MCM_FINISHED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3356)
      .Column(GRID_COLUMN_MCM_FINISHED).Width = 0
      .Column(GRID_COLUMN_MCM_FINISHED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_MCM_ERROR_REPORTING).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3338)
      .Column(GRID_COLUMN_MCM_ERROR_REPORTING).Header(1).Text = ""
      .Column(GRID_COLUMN_MCM_ERROR_REPORTING).Width = 0
      .Column(GRID_COLUMN_MCM_ERROR_REPORTING).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_MCM_CASHABLE_NUM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3338)
      .Column(GRID_COLUMN_MCM_CASHABLE_NUM).Header(1).Text = ""
      .Column(GRID_COLUMN_MCM_CASHABLE_NUM).Width = 0

      .Column(GRID_COLUMN_MCM_CASHABLE_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3338)
      .Column(GRID_COLUMN_MCM_CASHABLE_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3350)
      .Column(GRID_COLUMN_MCM_CASHABLE_AMOUNT).Width = IIf(m_show_expected_data, WIDTH_TICKETS, 0)

      .Column(GRID_COLUMN_MCM_PROMO_RE_NUM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3338)
      .Column(GRID_COLUMN_MCM_PROMO_RE_NUM).Header(1).Text = ""
      .Column(GRID_COLUMN_MCM_PROMO_RE_NUM).Width = 0

      .Column(GRID_COLUMN_MCM_PROMO_RE_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3338)
      .Column(GRID_COLUMN_MCM_PROMO_RE_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3351)
      .Column(GRID_COLUMN_MCM_PROMO_RE_AMOUNT).Width = IIf(m_show_expected_data, WIDTH_TICKETS, 0)

      .Column(GRID_COLUMN_MCM_PROMO_NR_NUM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3338)
      .Column(GRID_COLUMN_MCM_PROMO_NR_NUM).Header(1).Text = ""
      .Column(GRID_COLUMN_MCM_PROMO_NR_NUM).Width = 0

      .Column(GRID_COLUMN_MCM_PROMO_NR_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3338)
      .Column(GRID_COLUMN_MCM_PROMO_NR_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3352)
      .Column(GRID_COLUMN_MCM_PROMO_NR_AMOUNT).Width = IIf(m_show_expected_data, WIDTH_TICKETS, 0)

      For _idx As Int32 = 0 To m_bill_data.Count - 1
        _bill_data = m_bill_data(_idx)
        .Column(GRID_COLUMN_EXPECTED_BILL_FIRST + _idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3338)
        .Column(GRID_COLUMN_EXPECTED_BILL_FIRST + _idx).Header(1).Text = _bill_data.denomination_with_symbol
        .Column(GRID_COLUMN_EXPECTED_BILL_FIRST + _idx).Width = IIf(m_show_expected_data, WIDTH_BILLS_DET, 0)
      Next

      .Column(GRID_COLUMN_MCM_BILL_TOTAL_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3338)
      .Column(GRID_COLUMN_MCM_BILL_TOTAL_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4333)
      .Column(GRID_COLUMN_MCM_BILL_TOTAL_AMOUNT).Width = IIf(m_show_expected_data, WIDTH_BILLS_TOT, 0)

      .Column(GRID_COLUMN_MCM_TOTAL_COIN_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3338)
      .Column(GRID_COLUMN_MCM_TOTAL_COIN_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5616)
      .Column(GRID_COLUMN_MCM_TOTAL_COIN_AMOUNT).Width = IIf(m_show_expected_data, WIDTH_COINS_TOT, 0)

      .Column(GRID_COLUMN_MCM_TOTAL_CASH).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3338)
      .Column(GRID_COLUMN_MCM_TOTAL_CASH).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5612)
      .Column(GRID_COLUMN_MCM_TOTAL_CASH).Width = IIf(m_show_expected_data, WIDTH_COINS_TOT, 0)

      .Column(GRID_COLUMN_DIFF_BILL_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5047)
      .Column(GRID_COLUMN_DIFF_BILL_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5048)
      .Column(GRID_COLUMN_DIFF_BILL_AMOUNT).Width = IIf(m_show_expected_data, WIDTH_BILLS_TOT, 0)

      .Column(GRID_COLUMN_DIFF_BILL_PERCENTAJE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5047)
      .Column(GRID_COLUMN_DIFF_BILL_PERCENTAJE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5042)
      .Column(GRID_COLUMN_DIFF_BILL_PERCENTAJE).Width = IIf(m_show_expected_data, WIDTH_BILLS_DET, 0)

      .Column(GRID_COLUMN_DIFF_TICKET_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5046)
      .Column(GRID_COLUMN_DIFF_TICKET_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5048)
      .Column(GRID_COLUMN_DIFF_TICKET_AMOUNT).Width = IIf(m_show_expected_data, WIDTH_BILLS_TOT, 0)

      .Column(GRID_COLUMN_DIFF_TICKET_PERCENTAJE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5046)
      .Column(GRID_COLUMN_DIFF_TICKET_PERCENTAJE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5042)
      .Column(GRID_COLUMN_DIFF_TICKET_PERCENTAJE).Width = IIf(m_show_expected_data, WIDTH_BILLS_DET, 0)

      .Column(GRID_COLUMN_DIFF_COIN_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5692)
      .Column(GRID_COLUMN_DIFF_COIN_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5048)
      .Column(GRID_COLUMN_DIFF_COIN_AMOUNT).Width = IIf(m_show_expected_data, WIDTH_BILLS_TOT, 0)

      .Column(GRID_COLUMN_DIFF_COIN_PERCENTAJE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5692)
      .Column(GRID_COLUMN_DIFF_COIN_PERCENTAJE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5042)
      .Column(GRID_COLUMN_DIFF_COIN_PERCENTAJE).Width = IIf(m_show_expected_data, WIDTH_BILLS_DET, 0)

      .Column(GRID_COLUMN_DIFF_CASH_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5693)
      .Column(GRID_COLUMN_DIFF_CASH_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5048)
      .Column(GRID_COLUMN_DIFF_CASH_AMOUNT).Width = IIf(m_show_expected_data, WIDTH_BILLS_TOT, 0)

      .Column(GRID_COLUMN_DIFF_CASH_PERCENTAJE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5693)
      .Column(GRID_COLUMN_DIFF_CASH_PERCENTAJE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5042)
      .Column(GRID_COLUMN_DIFF_CASH_PERCENTAJE).Width = IIf(m_show_expected_data, WIDTH_BILLS_DET, 0)

      .Column(GRID_COLUMN_TE_TERMINAL_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_TE_TERMINAL_ID).Header(1).Text = ""
      .Column(GRID_COLUMN_TE_TERMINAL_ID).Width = 0

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Get the sql 'where' statement
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - an string with the 'where' statement
  Private Function GetSqlWhere() As String

    Dim _str_where As String
    Dim _str_terminals As String
    Dim _selected_terminals As Long()

    _str_where = String.Empty
    _str_terminals = String.Empty

    ' Dates filter
    If opt_insertion_datetime.Checked Then
      _str_where = ds_from_to.GetSqlFilterCondition("MC_DATETIME")
    ElseIf opt_collection_datetime.Checked Then
      _str_where = ds_from_to.GetSqlFilterCondition("MC_COLLECTION_DATETIME")
    Else
      _str_where = ds_from_to.GetSqlFilterCondition("CGS_WORKING_DAY")
    End If

    If Not String.IsNullOrEmpty(_str_where) Then
      _str_where = "     AND " & _str_where & " "
    End If

    ' Terminal filter
    If Not Me.uc_provider.AllSelectedChecked Then
      _selected_terminals = Me.uc_provider.GetTerminalIdListSelected()
      If Me.uc_provider.TerminalListHasValues Then
        For Each _terminal_id As Long In _selected_terminals
          _str_terminals &= _terminal_id & ","
        Next
        _str_terminals = _str_terminals.Substring(0, (_str_terminals.Length - 1))
      End If
    End If

    If _str_terminals.Length > 0 Then
      If _str_where.Length > 0 Then
        _str_where = String.Format("{0} AND MC_TERMINAL_ID IN ({1})", _str_where, _str_terminals)
      Else
        _str_where = String.Format(" AND MC_TERMINAL_ID IN ({0})", _str_terminals)
      End If
    End If

    Return IIf(_str_where.Length > 0, _str_where, String.Empty)

  End Function  ' GetSqlWhere

  ' PURPOSE : Set Totals to zero
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  Private Sub ResetTotals()
    Dim _bill_data As TYPE_BILL_DATA

    m_ti_cashable_amount = 0
    m_ti_cashable_count = 0
    m_ti_promo_re_amount = 0
    m_ti_promo_re_count = 0
    m_ti_promo_nr_amount = 0
    m_ti_promo_nr_count = 0
    m_ti_total_amount = 0
    m_ti_total_count = 0
    m_mcd_num = 0
    For _idx As Int32 = 0 To m_bill_data.Count - 1
      _bill_data = m_bill_data(_idx)
      _bill_data.total_expected = 0
      _bill_data.total_collected = 0
    Next

    m_mcm_cashable_num = 0
    m_mcm_cashable_amount = 0
    m_mcm_promo_nr_num = 0
    m_mcm_promo_nr_amount = 0
    m_mcm_promo_re_num = 0
    m_mcm_promo_re_amount = 0
    m_mcm_bill_total_amount = 0
    m_mcd_collected_total_amount = 0

    m_mcm_total_coin_amount = 0
    m_mcd_collected_total_coin_amount = 0

  End Sub

  ' PURPOSE : Sum detail to totals
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  Private Sub SumToTotals(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW)
    Dim _bill_data As TYPE_BILL_DATA
    Dim m_ti_cash_count As Integer
    Dim m_ti_cash_amount As Decimal
    Dim m_ti_prom_re_count As Integer
    Dim m_ti_prom_re_amount As Decimal
    Dim m_ti_prom_nr_count As Integer
    Dim m_ti_prom_nr_amount As Decimal

    m_ti_cash_count = DbRow.Value(SQL_COLUMN_TI_CASHABLE_COUNT)
    m_ti_cash_amount = DbRow.Value(SQL_COLUMN_TI_CASHABLE_AMOUNT)
    m_ti_prom_re_count = DbRow.Value(SQL_COLUMN_TI_PROMO_RE_COUNT)
    m_ti_prom_re_amount = DbRow.Value(SQL_COLUMN_TI_PROMO_RE_AMOUNT)
    m_ti_prom_nr_count = DbRow.Value(SQL_COLUMN_TI_PROMO_NR_COUNT)
    m_ti_prom_nr_amount = DbRow.Value(SQL_COLUMN_TI_PROMO_NR_AMOUNT)


    m_insert_last_row = True
    m_ti_cashable_amount += m_ti_cash_amount
    m_ti_cashable_count += m_ti_cash_count
    m_ti_promo_re_amount += m_ti_prom_re_amount
    m_ti_promo_re_count += m_ti_prom_re_count
    m_ti_promo_nr_amount += m_ti_prom_nr_amount
    m_ti_promo_nr_count += m_ti_prom_nr_count
    m_ti_total_amount += m_ti_cash_amount + m_ti_prom_re_amount + m_ti_prom_nr_amount
    m_ti_total_count += m_ti_cash_count + m_ti_prom_re_count + m_ti_prom_nr_count
    m_mcd_num += DbRow.Value(SQL_COLUMN_MCD_NUM)

    For _idx As Int32 = 0 To m_bill_data.Count - 1
      _bill_data = m_bill_data(_idx)
      _bill_data.total_expected += DbRow.Value(SQL_COLUMN_EXPECTED_BILL_FIRST + _idx)
      _bill_data.total_collected += DbRow.Value(SQL_COLUMN_COLLECTED_BILL_FIRST + _idx)
    Next

    m_mcm_cashable_num += DbRow.Value(SQL_COLUMN_MCM_CASHABLE_NUM)
    m_mcm_cashable_amount += DbRow.Value(SQL_COLUMN_MCM_CASHABLE_AMOUNT)
    m_mcm_promo_nr_num += DbRow.Value(SQL_COLUMN_MCM_PROMO_NR_NUM)
    m_mcm_promo_nr_amount += DbRow.Value(SQL_COLUMN_MCM_PROMO_NR_AMOUNT)
    m_mcm_promo_re_num += DbRow.Value(SQL_COLUMN_MCM_PROMO_RE_NUM)
    m_mcm_promo_re_amount += DbRow.Value(SQL_COLUMN_MCM_PROMO_RE_AMOUNT)
    m_mcm_bill_total_amount += DbRow.Value(SQL_COLUMN_MCM_BILL_TOTAL_AMOUNT)
    m_mcd_collected_total_amount += DbRow.Value(SQL_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT)

    m_mcm_total_coin_amount += DbRow.Value(SQL_COLUMN_MCM_TOTAL_COIN_AMOUNT)
    m_mcd_collected_total_coin_amount += DbRow.Value(SQL_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT)

  End Sub

  ' PURPOSE : Sum detail to subtotals
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  Private Sub SumToSubTotals(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW)
    Dim _bill_data As TYPE_BILL_DATA
    Dim m_ti_cash_count As Integer
    Dim m_ti_cash_amount As Decimal
    Dim m_ti_prom_re_count As Integer
    Dim m_ti_prom_re_amount As Decimal
    Dim m_ti_prom_nr_count As Integer
    Dim m_ti_prom_nr_amount As Decimal
    'SGB 15-OCT-2014: Add in subtotal columns provider & terminal
    m_subtotal_provider_id = DbRow.Value(SQL_COLUMN_PROVIDER_ID).ToString()
    m_subtotal_te_name = DbRow.Value(SQL_COLUMN_TE_NAME).ToString()
    Call GetIndexDateSubtotal()
    m_subtotal_date = CType(DbRow.Value(m_index_of_date_to_calculate_subtotal), Date).Date

    m_ti_cash_count = DbRow.Value(SQL_COLUMN_TI_CASHABLE_COUNT)
    m_ti_cash_amount = DbRow.Value(SQL_COLUMN_TI_CASHABLE_AMOUNT)
    m_ti_prom_re_count = DbRow.Value(SQL_COLUMN_TI_PROMO_RE_COUNT)
    m_ti_prom_re_amount = DbRow.Value(SQL_COLUMN_TI_PROMO_RE_AMOUNT)
    m_ti_prom_nr_count = DbRow.Value(SQL_COLUMN_TI_PROMO_NR_COUNT)
    m_ti_prom_nr_amount = DbRow.Value(SQL_COLUMN_TI_PROMO_NR_AMOUNT)

    m_subtotal_ti_cashable_amount += m_ti_cash_amount
    m_subtotal_ti_cashable_count += m_ti_cash_count
    m_subtotal_ti_promo_re_amount += m_ti_prom_re_amount
    m_subtotal_ti_promo_re_count += m_ti_prom_re_count
    m_subtotal_ti_promo_nr_amount += m_ti_prom_nr_amount
    m_subtotal_ti_promo_nr_count += m_ti_prom_nr_count
    m_subtotal_ti_total_amount += m_ti_cash_amount + m_ti_prom_re_amount + m_ti_prom_nr_amount
    m_subtotal_ti_total_count += m_ti_cash_count + m_ti_prom_re_count + m_ti_prom_nr_count
    m_subtotal_mcd_num += DbRow.Value(SQL_COLUMN_MCD_NUM)

    For _idx As Int32 = 0 To m_bill_data.Count - 1
      _bill_data = m_bill_data(_idx)
      _bill_data.subtotal_expected += DbRow.Value(SQL_COLUMN_EXPECTED_BILL_FIRST + _idx)
      _bill_data.subtotal_collected += DbRow.Value(SQL_COLUMN_COLLECTED_BILL_FIRST + _idx)
    Next

    m_subtotal_mcm_cashable_num += DbRow.Value(SQL_COLUMN_MCM_CASHABLE_NUM)
    m_subtotal_mcm_cashable_amount += DbRow.Value(SQL_COLUMN_MCM_CASHABLE_AMOUNT)
    m_subtotal_mcm_promo_nr_num += DbRow.Value(SQL_COLUMN_MCM_PROMO_NR_NUM)
    m_subtotal_mcm_promo_nr_amount += DbRow.Value(SQL_COLUMN_MCM_PROMO_NR_AMOUNT)
    m_subtotal_mcm_promo_re_num += DbRow.Value(SQL_COLUMN_MCM_PROMO_RE_NUM)
    m_subtotal_mcm_promo_re_amount += DbRow.Value(SQL_COLUMN_MCM_PROMO_RE_AMOUNT)
    m_subtotal_mcd_collected_total_amount += DbRow.Value(SQL_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT)
    m_subtotal_mcm_bill_total_amount += DbRow.Value(SQL_COLUMN_MCM_BILL_TOTAL_AMOUNT)
    m_subtotal_mcd_collected_total_coin_amount += DbRow.Value(SQL_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT)
    m_subtotal_mcm_total_coin_amount += DbRow.Value(SQL_COLUMN_MCM_TOTAL_COIN_AMOUNT)

  End Sub

  ' PURPOSE : Sum detail to subtotals
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  Private Sub ResetSubtotals()
    Dim _bill_data As TYPE_BILL_DATA

    'SGB 15-OCT-2014: reset subtotal columns provider & terminal
    m_subtotal_te_name = ""
    m_subtotal_provider_id = ""

    m_subtotal_ti_cashable_amount = 0
    m_subtotal_ti_cashable_count = 0
    m_subtotal_ti_promo_re_amount = 0
    m_subtotal_ti_promo_re_count = 0
    m_subtotal_ti_promo_nr_amount = 0
    m_subtotal_ti_promo_nr_count = 0
    m_subtotal_ti_total_amount = 0
    m_subtotal_ti_total_count = 0
    m_subtotal_mcd_num = 0

    For _idx As Int32 = 0 To m_bill_data.Count - 1
      _bill_data = m_bill_data(_idx)
      _bill_data.subtotal_expected = 0
      _bill_data.subtotal_collected = 0
    Next

    m_subtotal_mcm_cashable_num = 0
    m_subtotal_mcm_cashable_amount = 0
    m_subtotal_mcm_promo_nr_num = 0
    m_subtotal_mcm_promo_nr_amount = 0
    m_subtotal_mcm_promo_re_num = 0
    m_subtotal_mcm_promo_re_amount = 0
    m_subtotal_mcd_collected_total_amount = 0
    m_subtotal_mcd_collected_total_coin_amount = 0
    m_subtotal_mcm_total_coin_amount = 0
    m_subtotal_mcm_bill_total_amount = 0

  End Sub

  ' PURPOSE : Print Subtotal
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  Private Sub PrintSubtotal(ByVal ShowBy As ENUM_SUBTOTAL_SHOW_TYPE, Optional ByVal IndexShowDate As Int32 = 0)
    Dim _row_index As Integer
    Dim _bill_data As TYPE_BILL_DATA

    If m_row_deleted Then

      Return
    End If

    Me.Grid.AddRow()
    _row_index = Me.Grid.NumRows - 1

    With Me.Grid
      'SGB 15-OCT-2014: Print in subtotal colums provider or terminal or date
      'HBB 19-NOV-2014
      Select Case ShowBy
        Case ENUM_SUBTOTAL_SHOW_TYPE.SHOW_SUBTOTAL_BY_DATE
          .Cell(_row_index, IndexShowDate).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5733) & ": " & m_subtotal_date
        Case ENUM_SUBTOTAL_SHOW_TYPE.SHOW_SUBTOTAL_BY_PROVIDER
          .Cell(_row_index, GRID_COLUMN_TE_PROVIDER).Value = m_subtotal_provider_id
        Case ENUM_SUBTOTAL_SHOW_TYPE.SHOW_SUBTOTAL_BY_TERMINAL
          .Cell(_row_index, GRID_COLUMN_TE_NAME).Value = m_subtotal_te_name

      End Select
      .Cell(_row_index, GRID_COLUMN_MC_NOTES).Value = GLB_NLS_GUI_INVOICING.GetString(375) ' GLB_NLS_GUI_STATISTICS.GetString(203)

      .Cell(_row_index, GRID_COLUMN_TI_CASHABLE_COUNT).Value = mdl_tito.GetDefaultNumber(m_subtotal_ti_cashable_count)
      .Cell(_row_index, GRID_COLUMN_TI_CASHABLE_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_subtotal_ti_cashable_amount)
      .Cell(_row_index, GRID_COLUMN_TI_PROMO_RE_COUNT).Value = mdl_tito.GetDefaultNumber(m_subtotal_ti_promo_re_count)
      .Cell(_row_index, GRID_COLUMN_TI_PROMO_RE_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_subtotal_ti_promo_re_amount)
      .Cell(_row_index, GRID_COLUMN_TI_PROMO_NR_COUNT).Value = mdl_tito.GetDefaultNumber(m_subtotal_ti_promo_nr_count)
      .Cell(_row_index, GRID_COLUMN_TI_PROMO_NR_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_subtotal_ti_promo_nr_amount)
      .Cell(_row_index, GRID_COLUMN_TI_TOTAL_COUNT).Value = mdl_tito.GetDefaultNumber(m_subtotal_ti_total_count)
      .Cell(_row_index, GRID_COLUMN_TI_TOTAL_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_subtotal_ti_total_amount)
      .Cell(_row_index, GRID_COLUMN_MCD_NUM).Value = mdl_tito.GetDefaultNumber(m_subtotal_mcd_num)
      For _idx As Int32 = 0 To m_bill_data.Count - 1
        _bill_data = m_bill_data(_idx)
        .Cell(_row_index, GRID_COLUMN_COLLECTED_BILL_FIRST + _idx).Value = mdl_tito.GetDefaultNumber(_bill_data.subtotal_collected)
        .Cell(_row_index, GRID_COLUMN_EXPECTED_BILL_FIRST + _idx).Value = mdl_tito.GetDefaultNumber(_bill_data.subtotal_expected)
      Next

      .Cell(_row_index, GRID_COLUMN_MCM_CASHABLE_NUM).Value = mdl_tito.GetDefaultNumber(m_subtotal_mcm_cashable_num)
      .Cell(_row_index, GRID_COLUMN_MCM_CASHABLE_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_subtotal_mcm_cashable_amount)
      .Cell(_row_index, GRID_COLUMN_MCM_PROMO_NR_NUM).Value = mdl_tito.GetDefaultNumber(m_subtotal_mcm_promo_nr_num)
      .Cell(_row_index, GRID_COLUMN_MCM_PROMO_NR_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_subtotal_mcm_promo_nr_amount)
      .Cell(_row_index, GRID_COLUMN_MCM_PROMO_RE_NUM).Value = mdl_tito.GetDefaultNumber(m_subtotal_mcm_promo_re_num)
      .Cell(_row_index, GRID_COLUMN_MCM_PROMO_RE_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_subtotal_mcm_promo_re_amount)
      .Cell(_row_index, GRID_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_subtotal_mcd_collected_total_amount)
      .Cell(_row_index, GRID_COLUMN_MCM_BILL_TOTAL_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_subtotal_mcm_bill_total_amount)

      .Cell(_row_index, GRID_COLUMN_MCM_TOTAL_COIN_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_subtotal_mcm_total_coin_amount)
      .Cell(_row_index, GRID_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT).Value = mdl_tito.GetFormatedCurrency(m_subtotal_mcd_collected_total_coin_amount)
      .Cell(_row_index, GRID_COLUMN_MCD_TOTAL_CASH).Value = mdl_tito.GetFormatedCurrency(m_subtotal_mcd_collected_total_amount + m_subtotal_mcd_collected_total_coin_amount)

      .Cell(_row_index, GRID_COLUMN_MCM_TOTAL_CASH).Value = mdl_tito.GetFormatedCurrency(m_subtotal_mcm_bill_total_amount + m_subtotal_mcm_total_coin_amount)

      .Cell(_row_index, GRID_COLUMN_DIFF_TICKET_AMOUNT).Value = GUI_FormatCurrency(CalculateTicketDifferenceTotalAmount(m_subtotal_ti_cashable_amount, m_subtotal_ti_promo_re_amount, m_subtotal_ti_promo_nr_amount, _
                                                                                                                        m_subtotal_mcm_cashable_amount, m_subtotal_mcm_promo_re_amount, m_subtotal_mcm_promo_nr_amount))
      .Cell(_row_index, GRID_COLUMN_DIFF_TICKET_PERCENTAJE).Value = GUI_FormatNumber(CalculateDifferenceTotalPercentaje(m_subtotal_ti_cashable_amount + m_subtotal_ti_promo_re_amount + m_subtotal_ti_promo_nr_amount, _
                                                                                                                        m_subtotal_mcm_cashable_amount + m_subtotal_mcm_promo_re_amount + m_subtotal_mcm_promo_nr_amount), _
                                                                                     2) & "%"
      .Cell(_row_index, GRID_COLUMN_DIFF_BILL_AMOUNT).Value = GUI_FormatCurrency(m_subtotal_mcd_collected_total_amount - m_subtotal_mcm_bill_total_amount)
      .Cell(_row_index, GRID_COLUMN_DIFF_BILL_PERCENTAJE).Value = GUI_FormatNumber(CalculateDifferenceTotalPercentaje(m_subtotal_mcd_collected_total_amount, m_subtotal_mcm_bill_total_amount), _
                                                                                   2) & "%"

      .Cell(_row_index, GRID_COLUMN_DIFF_COIN_AMOUNT).Value = GUI_FormatCurrency(m_subtotal_mcd_collected_total_coin_amount - m_subtotal_mcm_total_coin_amount)
      .Cell(_row_index, GRID_COLUMN_DIFF_COIN_PERCENTAJE).Value = GUI_FormatNumber(CalculateDifferenceTotalPercentaje(m_subtotal_mcd_collected_total_coin_amount, m_subtotal_mcm_total_coin_amount), _
                                                                                   2) & "%"

      .Cell(_row_index, GRID_COLUMN_DIFF_CASH_AMOUNT).Value = GUI_FormatCurrency(m_subtotal_mcd_collected_total_amount + m_subtotal_mcd_collected_total_coin_amount - (m_subtotal_mcm_bill_total_amount + m_subtotal_mcm_total_coin_amount))
      .Cell(_row_index, GRID_COLUMN_DIFF_CASH_PERCENTAJE).Value = GUI_FormatNumber(CalculateDifferenceTotalPercentaje(m_subtotal_mcd_collected_total_amount + m_subtotal_mcd_collected_total_coin_amount, m_subtotal_mcm_bill_total_amount + m_subtotal_mcm_total_coin_amount), _
                                                                                   2) & "%"

    End With

    Me.Grid.Row(_row_index).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
    Call ResetSubtotals()
  End Sub

  ' PURPOSE: Gets all Bill types from the current currency
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GetBillData()
    Dim _sb As StringBuilder
    Dim _table As DataTable
    Dim _bill_data As TYPE_BILL_DATA

    _sb = New StringBuilder()

    _sb.AppendLine("   SELECT   DISTINCT(MCD_FACE_VALUE) AS DENOMINATION   ")
    _sb.AppendLine("     FROM   MONEY_COLLECTION_DETAILS                   ")
    _sb.AppendLine("    WHERE   MCD_FACE_VALUE > 0                         ")
    _sb.AppendLine("      AND   MCD_CAGE_CURRENCY_TYPE = 0                 ")
    _sb.AppendLine("    UNION                                              ")
    _sb.AppendLine("   SELECT   CUD_DENOMINATION AS DENOMINATION           ")
    _sb.AppendLine("     FROM   CURRENCY_DENOMINATIONS                     ")
    _sb.AppendLine("    WHERE   CUD_TYPE = 0                               ")
    _sb.AppendLine("      AND   CUD_REJECTED = 0                           ")
    _sb.AppendLine(" ORDER BY   DENOMINATION ASC                           ")

    _table = GUI_GetTableUsingSQL(_sb.ToString(), 100)
    m_bill_data = New List(Of TYPE_BILL_DATA)()

    For Each _dr As DataRow In _table.Rows
      _bill_data = New TYPE_BILL_DATA()
      _bill_data.value = _dr("DENOMINATION")


      _bill_data.denomination = Decimal.ToInt32(_bill_data.value).ToString()
      If _bill_data.value >= 1 Then
        _bill_data.denomination_with_symbol = GUI_FormatCurrency(_bill_data.value, 0)
      Else
        _bill_data.denomination_with_symbol = GUI_FormatCurrency(_bill_data.value, 2)
      End If


      _bill_data.subtotal_collected = 0
      _bill_data.subtotal_expected = 0
      _bill_data.total_collected = 0
      _bill_data.total_expected = 0

      m_bill_data.Add(_bill_data)
    Next


    Call UpdateGridColumns()
    Call UpdateSQLColumns()

  End Sub ' GetBillTypes

  Private Sub UpdateSQLColumns()
    SQL_COLUMN_COLLECTED_BILL_FIRST = SQL_COLUMN_MCD_NUM + 1
    SQL_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT = SQL_COLUMN_COLLECTED_BILL_FIRST + m_bill_data.Count
    SQL_COLUMN_MCM_MONEY_COLLECTION_ID = SQL_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT + 1
    SQL_COLUMN_MCM_SESSION_ID = SQL_COLUMN_MCM_MONEY_COLLECTION_ID + 1
    SQL_COLUMN_MCM_TERMINAL_ID = SQL_COLUMN_MCM_SESSION_ID + 1
    SQL_COLUMN_MCM_STARTED = SQL_COLUMN_MCM_TERMINAL_ID + 1
    SQL_COLUMN_MCM_FINISHED = SQL_COLUMN_MCM_STARTED + 1
    SQL_COLUMN_MCM_ERROR_REPORTING = SQL_COLUMN_MCM_FINISHED + 1
    SQL_COLUMN_EXPECTED_BILL_FIRST = SQL_COLUMN_MCM_ERROR_REPORTING + 1
    SQL_COLUMN_MCM_CASHABLE_NUM = SQL_COLUMN_EXPECTED_BILL_FIRST + m_bill_data.Count
    SQL_COLUMN_MCM_CASHABLE_AMOUNT = SQL_COLUMN_MCM_CASHABLE_NUM + 1
    SQL_COLUMN_MCM_PROMO_RE_NUM = SQL_COLUMN_MCM_CASHABLE_AMOUNT + 1
    SQL_COLUMN_MCM_PROMO_RE_AMOUNT = SQL_COLUMN_MCM_PROMO_RE_NUM + 1
    SQL_COLUMN_MCM_PROMO_NR_NUM = SQL_COLUMN_MCM_PROMO_RE_AMOUNT + 1
    SQL_COLUMN_MCM_PROMO_NR_AMOUNT = SQL_COLUMN_MCM_PROMO_NR_NUM + 1
    SQL_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT = SQL_COLUMN_MCM_PROMO_NR_AMOUNT + 1
    SQL_COLUMN_MCM_BILL_TOTAL_AMOUNT = SQL_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT + 1
    SQL_COLUMN_PROVIDER_ID = SQL_COLUMN_MCM_BILL_TOTAL_AMOUNT + 1
    SQL_COLUMN_MCM_TOTAL_COIN_AMOUNT = SQL_COLUMN_PROVIDER_ID + 1
  End Sub

  Private Sub UpdateGridColumns()
    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_COLUMN_SELECT = 0
    GRID_COLUMN_MC_COLLECTION_ID = 1
    GRID_INIT_TERMINAL_DATA = 2

    GRID_COLUMN_MC_STATUS = TERMINAL_DATA_COLUMNS + 2
    GRID_COLUMN_MC_STACKER_ID = TERMINAL_DATA_COLUMNS + 3
    GRID_COLUMN_MC_DATETIME = TERMINAL_DATA_COLUMNS + 4
    GRID_COLUMN_MC_EXTRACTION_DATETIME = TERMINAL_DATA_COLUMNS + 5
    GRID_COLUMN_MC_COLLECTION_DATETIME = TERMINAL_DATA_COLUMNS + 6
    GRID_COLUMN_CAGE_SESSION_DATE = TERMINAL_DATA_COLUMNS + 7
    GRID_COLUMN_MC_CASHIER_SESSION_ID = TERMINAL_DATA_COLUMNS + 8
    GRID_COLUMN_USER_NM = TERMINAL_DATA_COLUMNS + 9
    GRID_COLUMN_MC_NOTES = TERMINAL_DATA_COLUMNS + 10
    GRID_COLUMN_TI_CASHABLE_COUNT = TERMINAL_DATA_COLUMNS + 11
    GRID_COLUMN_TI_CASHABLE_AMOUNT = TERMINAL_DATA_COLUMNS + 12
    GRID_COLUMN_TI_PROMO_RE_COUNT = TERMINAL_DATA_COLUMNS + 13
    GRID_COLUMN_TI_PROMO_RE_AMOUNT = TERMINAL_DATA_COLUMNS + 14
    GRID_COLUMN_TI_PROMO_NR_COUNT = TERMINAL_DATA_COLUMNS + 15
    GRID_COLUMN_TI_PROMO_NR_AMOUNT = TERMINAL_DATA_COLUMNS + 16
    GRID_COLUMN_TI_TOTAL_COUNT = TERMINAL_DATA_COLUMNS + 17
    GRID_COLUMN_TI_TOTAL_AMOUNT = TERMINAL_DATA_COLUMNS + 18
    GRID_COLUMN_MCD_NUM = TERMINAL_DATA_COLUMNS + 19
    GRID_COLUMN_COLLECTED_BILL_FIRST = GRID_COLUMN_MCD_NUM + 1
    GRID_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT = GRID_COLUMN_COLLECTED_BILL_FIRST + m_bill_data.Count
    GRID_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT = GRID_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT + 1
    GRID_COLUMN_MCD_TOTAL_CASH = GRID_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT + 1
    GRID_COLUMN_MCM_MONEY_COLLECTION_ID = GRID_COLUMN_MCD_TOTAL_CASH + 1
    GRID_COLUMN_MCM_SESSION_ID = GRID_COLUMN_MCM_MONEY_COLLECTION_ID + 1
    GRID_COLUMN_MCM_TERMINAL_ID = GRID_COLUMN_MCM_SESSION_ID + 1
    GRID_COLUMN_MCM_STARTED = GRID_COLUMN_MCM_TERMINAL_ID + 1
    GRID_COLUMN_MCM_FINISHED = GRID_COLUMN_MCM_STARTED + 1
    GRID_COLUMN_MCM_ERROR_REPORTING = GRID_COLUMN_MCM_FINISHED + 1
    GRID_COLUMN_MCM_CASHABLE_NUM = GRID_COLUMN_MCM_ERROR_REPORTING + 1
    GRID_COLUMN_MCM_CASHABLE_AMOUNT = GRID_COLUMN_MCM_CASHABLE_NUM + 1
    GRID_COLUMN_MCM_PROMO_RE_NUM = GRID_COLUMN_MCM_CASHABLE_AMOUNT + 1
    GRID_COLUMN_MCM_PROMO_RE_AMOUNT = GRID_COLUMN_MCM_PROMO_RE_NUM + 1
    GRID_COLUMN_MCM_PROMO_NR_NUM = GRID_COLUMN_MCM_PROMO_RE_AMOUNT + 1
    GRID_COLUMN_MCM_PROMO_NR_AMOUNT = GRID_COLUMN_MCM_PROMO_NR_NUM + 1
    GRID_COLUMN_EXPECTED_BILL_FIRST = GRID_COLUMN_MCM_PROMO_NR_AMOUNT + 1
    GRID_COLUMN_MCM_BILL_TOTAL_AMOUNT = GRID_COLUMN_EXPECTED_BILL_FIRST + m_bill_data.Count
    GRID_COLUMN_MCM_TOTAL_COIN_AMOUNT = GRID_COLUMN_MCM_BILL_TOTAL_AMOUNT + 1
    GRID_COLUMN_MCM_TOTAL_CASH = GRID_COLUMN_MCM_TOTAL_COIN_AMOUNT + 1
    GRID_COLUMN_DIFF_TICKET_AMOUNT = GRID_COLUMN_MCM_TOTAL_CASH + 1
    GRID_COLUMN_DIFF_TICKET_PERCENTAJE = GRID_COLUMN_DIFF_TICKET_AMOUNT + 1
    GRID_COLUMN_DIFF_BILL_AMOUNT = GRID_COLUMN_DIFF_TICKET_PERCENTAJE + 1
    GRID_COLUMN_DIFF_BILL_PERCENTAJE = GRID_COLUMN_DIFF_BILL_AMOUNT + 1
    GRID_COLUMN_TE_TERMINAL_ID = GRID_COLUMN_DIFF_BILL_PERCENTAJE + 1
    GRID_COLUMN_DIFF_COIN_AMOUNT = GRID_COLUMN_TE_TERMINAL_ID + 1
    GRID_COLUMN_DIFF_COIN_PERCENTAJE = GRID_COLUMN_DIFF_COIN_AMOUNT + 1
    GRID_COLUMN_DIFF_CASH_AMOUNT = GRID_COLUMN_DIFF_COIN_PERCENTAJE + 1
    GRID_COLUMN_DIFF_CASH_PERCENTAJE = GRID_COLUMN_DIFF_CASH_AMOUNT + 1

    GRID_COLUMNS_COUNT = GRID_COLUMN_DIFF_CASH_PERCENTAJE + 1
  End Sub

  ' PURPOSE : Check and paint data row
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  Private Sub CheckAndPaintDataRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW)

    Dim _has_error As Boolean
    Dim _cell_color_red As Color
    Dim _cell_color_white As Color

    ' Error reporting
    If DbRow.Value(SQL_COLUMN_MCM_ERROR_REPORTING) Then
      Me.Grid.Row(RowIndex).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_ORANGE_00)
      'Me.Grid.Counter(COUNTER_ERROR).Value += 1
    End If


    _has_error = False
    _cell_color_red = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
    _cell_color_white = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

    If m_list_of_index_with_erors.Count > 0 Then
      _has_error = True
      m_has_row = True
      For Each _idx As Int32 In m_list_of_index_with_erors
        Me.Grid.Cell(RowIndex, _idx).BackColor = _cell_color_red
        Me.Grid.Cell(RowIndex, _idx).ForeColor = _cell_color_white
      Next
    Else
      Me.Grid.Counter(COUNTER_OK).Value += 1
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MC_STATUS).Value = CollectionStatusAsString(TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED)
    End If
    '''''''''' Check rows looking for errors. If there's an error, paint red color in cell
    '''''''''If DbRow.Value(SQL_COLUMN_TI_CASHABLE_AMOUNT) <> DbRow.Value(SQL_COLUMN_MCM_CASHABLE_AMOUNT) Then
    '''''''''  Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_CASHABLE_AMOUNT).BackColor = _cell_color_red
    '''''''''  Me.Grid.Cell(RowIndex, GRID_COLUMN_MCM_CASHABLE_AMOUNT).BackColor = _cell_color_red
    '''''''''  Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_CASHABLE_AMOUNT).ForeColor = _cell_color_white
    '''''''''  Me.Grid.Cell(RowIndex, GRID_COLUMN_MCM_CASHABLE_AMOUNT).ForeColor = _cell_color_white
    '''''''''  _has_error = True
    '''''''''End If

    '''''''''If DbRow.Value(SQL_COLUMN_TI_PROMO_RE_AMOUNT) <> DbRow.Value(SQL_COLUMN_MCM_PROMO_RE_AMOUNT) Then
    '''''''''  Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_PROMO_RE_AMOUNT).BackColor = _cell_color_red
    '''''''''  Me.Grid.Cell(RowIndex, GRID_COLUMN_MCM_PROMO_RE_AMOUNT).BackColor = _cell_color_red
    '''''''''  Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_PROMO_RE_AMOUNT).ForeColor = _cell_color_white
    '''''''''  Me.Grid.Cell(RowIndex, GRID_COLUMN_MCM_PROMO_RE_AMOUNT).ForeColor = _cell_color_white
    '''''''''  _has_error = True
    '''''''''End If

    '''''''''If DbRow.Value(SQL_COLUMN_TI_PROMO_NR_AMOUNT) <> DbRow.Value(SQL_COLUMN_MCM_PROMO_NR_AMOUNT) Then
    '''''''''  Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_PROMO_NR_AMOUNT).BackColor = _cell_color_red
    '''''''''  Me.Grid.Cell(RowIndex, GRID_COLUMN_MCM_PROMO_NR_AMOUNT).BackColor = _cell_color_red
    '''''''''  Me.Grid.Cell(RowIndex, GRID_COLUMN_TI_PROMO_NR_AMOUNT).ForeColor = _cell_color_white
    '''''''''  Me.Grid.Cell(RowIndex, GRID_COLUMN_MCM_PROMO_NR_AMOUNT).ForeColor = _cell_color_white
    '''''''''  _has_error = True
    '''''''''End If

    '''''''''For _idx As Int32 = 0 To m_bill_data.Count - 1
    '''''''''  If DbRow.Value(SQL_COLUMN_EXPECTED_BILL_FIRST + _idx) <> DbRow.Value(SQL_COLUMN_COLLECTED_BILL_FIRST + _idx) Then
    '''''''''    Me.Grid.Cell(RowIndex, GRID_COLUMN_COLLECTED_BILL_FIRST + _idx).BackColor = _cell_color_red
    '''''''''    Me.Grid.Cell(RowIndex, GRID_COLUMN_EXPECTED_BILL_FIRST + _idx).BackColor = _cell_color_red
    '''''''''    Me.Grid.Cell(RowIndex, GRID_COLUMN_COLLECTED_BILL_FIRST + _idx).ForeColor = _cell_color_white
    '''''''''    Me.Grid.Cell(RowIndex, GRID_COLUMN_EXPECTED_BILL_FIRST + _idx).ForeColor = _cell_color_white
    '''''''''    _has_error = True
    '''''''''  End If
    '''''''''Next

    'If there's an error, paint red in the first column ,else check checkbox only out of balance and remove row when there's no error.
    If _has_error Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MC_STATUS).BackColor = _cell_color_red
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MC_STATUS).Value = CollectionStatusAsString(TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MC_STATUS).ForeColor = _cell_color_white
      Me.Grid.Counter(COUNTER_DIFF).Value += 1
      m_has_row = True
      m_first_row = False
      ''''Else
      ''''  If cb_show_only_out_of_balance.Checked Then
      ''''    Me.Grid.DeleteRow(RowIndex)
      ''''    m_row_deleted = True
      ''''    If Me.Grid.NumRows = 1 Then
      ''''      m_first_row = True
      ''''    End If
      ''''  Else
      ''''    m_has_row = True
      ''''    Me.Grid.Counter(COUNTER_OK).Value += 1
      ''''    Me.Grid.Cell(RowIndex, GRID_COLUMN_MC_STATUS).Value = CollectionStatusAsString(TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED)
      ''''    m_first_row = False
      ''''  End If

    End If

  End Sub

  Private Function RowHasToBeAdded(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _insert_row As Boolean

    _insert_row = False
    m_list_of_index_with_erors = New List(Of Int32)

    ' Check bill columns for differences.
    For _idx As Int32 = 0 To m_bill_data.Count - 1
      If DbRow.Value(SQL_COLUMN_EXPECTED_BILL_FIRST + _idx) <> DbRow.Value(SQL_COLUMN_COLLECTED_BILL_FIRST + _idx) Then
        _insert_row = True
        m_list_of_index_with_erors.Add(GRID_COLUMN_COLLECTED_BILL_FIRST + _idx)
        m_list_of_index_with_erors.Add(GRID_COLUMN_EXPECTED_BILL_FIRST + _idx)
      End If
    Next

    ' Check coin columns for differences.
    If DbRow.Value(SQL_COLUMN_MCM_TOTAL_COIN_AMOUNT) <> DbRow.Value(SQL_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT) Then
      _insert_row = True
      m_list_of_index_with_erors.Add(GRID_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT)
      m_list_of_index_with_erors.Add(GRID_COLUMN_MCM_TOTAL_COIN_AMOUNT)
    End If

    ' Check total bills columns for differences.
    If DbRow.Value(SQL_COLUMN_MCM_BILL_TOTAL_AMOUNT) <> DbRow.Value(SQL_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT) Then
      _insert_row = True
      m_list_of_index_with_erors.Add(GRID_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT)
      m_list_of_index_with_erors.Add(GRID_COLUMN_MCM_BILL_TOTAL_AMOUNT)
    End If

    ' Check total cash columns for differences.
    If (DbRow.Value(SQL_COLUMN_MCM_BILL_TOTAL_AMOUNT) + DbRow.Value(SQL_COLUMN_MCM_TOTAL_COIN_AMOUNT)) <>
       (DbRow.Value(SQL_COLUMN_MCD_COLLECTED_TOTAL_AMOUNT) + DbRow.Value(SQL_COLUMN_MCD_COLLECTED_TOTAL_COINS_AMOUNT)) Then
      _insert_row = True
      m_list_of_index_with_erors.Add(GRID_COLUMN_MCD_TOTAL_CASH)
      m_list_of_index_with_erors.Add(GRID_COLUMN_MCM_TOTAL_CASH)
    End If

    ' Check ticket columns for differences.
    If DbRow.Value(SQL_COLUMN_TI_CASHABLE_AMOUNT) <> DbRow.Value(SQL_COLUMN_MCM_CASHABLE_AMOUNT) Then
      m_list_of_index_with_erors.Add(GRID_COLUMN_TI_CASHABLE_AMOUNT)
      m_list_of_index_with_erors.Add(GRID_COLUMN_MCM_CASHABLE_AMOUNT)
      _insert_row = True
    End If
    If DbRow.Value(SQL_COLUMN_TI_PROMO_RE_AMOUNT) <> DbRow.Value(SQL_COLUMN_MCM_PROMO_RE_AMOUNT) Then
      m_list_of_index_with_erors.Add(GRID_COLUMN_TI_PROMO_RE_AMOUNT)
      m_list_of_index_with_erors.Add(GRID_COLUMN_MCM_PROMO_RE_AMOUNT)
      _insert_row = True
    End If
    If DbRow.Value(SQL_COLUMN_TI_PROMO_NR_AMOUNT) <> DbRow.Value(SQL_COLUMN_MCM_PROMO_NR_AMOUNT) Then
      m_list_of_index_with_erors.Add(GRID_COLUMN_TI_PROMO_NR_AMOUNT)
      m_list_of_index_with_erors.Add(GRID_COLUMN_MCM_PROMO_NR_AMOUNT)
      _insert_row = True
    End If

    _insert_row = Not cb_show_only_out_of_balance.Checked Or _insert_row

    Return _insert_row
  End Function

  ' PURPOSE: Return a string representation of TITO_MONEY_COLLECTION_STATUS
  '
  ' PARAMS:
  '   - INPUT:
  '     - Model: TITO_MONEY_COLLECTION_STATUS
  '
  ' RETURNS:
  '     - String
  '
  Public Function PrintCollectionStatus(ByVal Status As TITO_MONEY_COLLECTION_STATUS) As String

    Dim _status_str As String

    Select Case Status
      Case TITO_MONEY_COLLECTION_STATUS.NONE
        _status_str = ""
      Case TITO_MONEY_COLLECTION_STATUS.OPEN
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2742)
      Case TITO_MONEY_COLLECTION_STATUS.PENDING
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2171)
      Case TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2233)
      Case TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2232)
      Case Else
        _status_str = ""
    End Select

    Return _status_str

  End Function

  Private Function GetIndexDateSubtotal() As Int32

    If opt_insertion_datetime.Checked Then
      m_index_of_date_to_calculate_subtotal = SQL_COLUMN_MC_DATETIME
      m_index_to_print_subtotal_date = GRID_COLUMN_MC_DATETIME
    ElseIf opt_collection_datetime.Checked Then
      m_index_of_date_to_calculate_subtotal = SQL_COLUMN_MC_COLLECTION_DATETIME
      m_index_to_print_subtotal_date = GRID_COLUMN_MC_COLLECTION_DATETIME
    Else
      m_index_of_date_to_calculate_subtotal = SQL_COLUMN_CAGE_DATE_TIME
      m_index_to_print_subtotal_date = GRID_COLUMN_CAGE_SESSION_DATE
    End If

  End Function

#End Region  ' Private Functions and Methods

#Region " Events "

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_terminal_location.CheckedChanged
    If cb_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If
  End Sub

  Private Sub chk_group_by_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_group_by.CheckedChanged
    opt_group_by_day.Enabled = chk_group_by.Checked
    opt_provider.Enabled = chk_group_by.Checked
    opt_terminal.Enabled = chk_group_by.Checked
    chk_show_detail.Enabled = chk_group_by.Checked
  End Sub

  Private Sub opt_insertion_datetime_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles opt_insertion_datetime.MouseClick
    opt_group_by_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2251)
  End Sub

  Private Sub opt_collection_datetime_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles opt_collection_datetime.MouseClick
    opt_group_by_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2206)
  End Sub

  Private Sub opt_cage_session_datetime_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles opt_cage_session_datetime.MouseClick
    opt_group_by_day.Text = opt_cage_session_datetime.Text
  End Sub

#End Region ' Events

  Protected Overrides Sub Finalize()
    MyBase.Finalize()
  End Sub

  Class TYPE_BILL_DATA
    Public denomination As String
    Public denomination_with_symbol As String
    Public value As Decimal
    Public subtotal_collected As Int32
    Public subtotal_expected As Int32
    Public total_collected As Int32
    Public total_expected As Int32
  End Class
End Class  ' frm_tito_collection_balance_report