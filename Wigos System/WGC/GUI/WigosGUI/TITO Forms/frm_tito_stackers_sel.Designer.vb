<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_tito_stackers_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.xGroupStacker = New System.Windows.Forms.GroupBox
    Me.ef_stacker_model = New GUI_Controls.uc_entry_field
    Me.ef_stacker_indentifier = New GUI_Controls.uc_entry_field
    Me.chk_without_terminal = New System.Windows.Forms.CheckBox
    Me.gb_status = New System.Windows.Forms.GroupBox
    Me.chk_inserted = New System.Windows.Forms.CheckBox
    Me.chk_collection_pending = New System.Windows.Forms.CheckBox
    Me.chk_available = New System.Windows.Forms.CheckBox
    Me.chk_inactive = New System.Windows.Forms.CheckBox
    Me.uc_provider = New GUI_Controls.uc_provider
    Me.gb_group_by = New System.Windows.Forms.GroupBox
    Me.opt_terminal_preasigned = New System.Windows.Forms.RadioButton
    Me.opt_terminal_inserted = New System.Windows.Forms.RadioButton
    Me.chk_group_by = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.xGroupStacker.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.gb_group_by.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_group_by)
    Me.panel_filter.Controls.Add(Me.uc_provider)
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Controls.Add(Me.chk_without_terminal)
    Me.panel_filter.Controls.Add(Me.xGroupStacker)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(934, 202)
    Me.panel_filter.Controls.SetChildIndex(Me.xGroupStacker, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_without_terminal, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_provider, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_group_by, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 206)
    Me.panel_data.Size = New System.Drawing.Size(934, 377)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(928, 17)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(928, 4)
    '
    'xGroupStacker
    '
    Me.xGroupStacker.Controls.Add(Me.ef_stacker_model)
    Me.xGroupStacker.Controls.Add(Me.ef_stacker_indentifier)
    Me.xGroupStacker.Location = New System.Drawing.Point(3, 6)
    Me.xGroupStacker.Name = "xGroupStacker"
    Me.xGroupStacker.Size = New System.Drawing.Size(185, 86)
    Me.xGroupStacker.TabIndex = 0
    Me.xGroupStacker.TabStop = False
    Me.xGroupStacker.Text = "xGroupStacker"
    '
    'ef_stacker_model
    '
    Me.ef_stacker_model.DoubleValue = 0
    Me.ef_stacker_model.IntegerValue = 0
    Me.ef_stacker_model.IsReadOnly = False
    Me.ef_stacker_model.Location = New System.Drawing.Point(13, 50)
    Me.ef_stacker_model.Name = "ef_stacker_model"
    Me.ef_stacker_model.Size = New System.Drawing.Size(157, 24)
    Me.ef_stacker_model.SufixText = "Sufix Text"
    Me.ef_stacker_model.SufixTextVisible = True
    Me.ef_stacker_model.TabIndex = 1
    Me.ef_stacker_model.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_stacker_model.TextValue = ""
    Me.ef_stacker_model.TextWidth = 45
    Me.ef_stacker_model.Value = ""
    Me.ef_stacker_model.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_stacker_indentifier
    '
    Me.ef_stacker_indentifier.DoubleValue = 0
    Me.ef_stacker_indentifier.IntegerValue = 0
    Me.ef_stacker_indentifier.IsReadOnly = False
    Me.ef_stacker_indentifier.Location = New System.Drawing.Point(13, 20)
    Me.ef_stacker_indentifier.Name = "ef_stacker_indentifier"
    Me.ef_stacker_indentifier.Size = New System.Drawing.Size(157, 24)
    Me.ef_stacker_indentifier.SufixText = "Sufix Text"
    Me.ef_stacker_indentifier.SufixTextVisible = True
    Me.ef_stacker_indentifier.TabIndex = 0
    Me.ef_stacker_indentifier.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_stacker_indentifier.TextValue = ""
    Me.ef_stacker_indentifier.TextWidth = 45
    Me.ef_stacker_indentifier.Value = ""
    Me.ef_stacker_indentifier.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_without_terminal
    '
    Me.chk_without_terminal.AutoSize = True
    Me.chk_without_terminal.Location = New System.Drawing.Point(201, 137)
    Me.chk_without_terminal.Name = "chk_without_terminal"
    Me.chk_without_terminal.Size = New System.Drawing.Size(126, 17)
    Me.chk_without_terminal.TabIndex = 3
    Me.chk_without_terminal.Text = "xWithoutTerminal"
    Me.chk_without_terminal.UseVisualStyleBackColor = True
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.chk_inserted)
    Me.gb_status.Controls.Add(Me.chk_collection_pending)
    Me.gb_status.Controls.Add(Me.chk_available)
    Me.gb_status.Controls.Add(Me.chk_inactive)
    Me.gb_status.Location = New System.Drawing.Point(195, 6)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(203, 114)
    Me.gb_status.TabIndex = 2
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xGroup Status"
    '
    'chk_inserted
    '
    Me.chk_inserted.AutoSize = True
    Me.chk_inserted.Location = New System.Drawing.Point(6, 43)
    Me.chk_inserted.Name = "chk_inserted"
    Me.chk_inserted.Size = New System.Drawing.Size(81, 17)
    Me.chk_inserted.TabIndex = 1
    Me.chk_inserted.Text = "xInserted"
    Me.chk_inserted.UseVisualStyleBackColor = True
    '
    'chk_collection_pending
    '
    Me.chk_collection_pending.AutoSize = True
    Me.chk_collection_pending.Location = New System.Drawing.Point(6, 66)
    Me.chk_collection_pending.Name = "chk_collection_pending"
    Me.chk_collection_pending.Size = New System.Drawing.Size(138, 17)
    Me.chk_collection_pending.TabIndex = 2
    Me.chk_collection_pending.Text = "xCollection Pending"
    Me.chk_collection_pending.UseVisualStyleBackColor = True
    '
    'chk_available
    '
    Me.chk_available.AutoSize = True
    Me.chk_available.Location = New System.Drawing.Point(6, 20)
    Me.chk_available.Name = "chk_available"
    Me.chk_available.Size = New System.Drawing.Size(85, 17)
    Me.chk_available.TabIndex = 0
    Me.chk_available.Text = "xAvailable"
    Me.chk_available.UseVisualStyleBackColor = True
    '
    'chk_inactive
    '
    Me.chk_inactive.AutoSize = True
    Me.chk_inactive.Location = New System.Drawing.Point(6, 89)
    Me.chk_inactive.Name = "chk_inactive"
    Me.chk_inactive.Size = New System.Drawing.Size(79, 17)
    Me.chk_inactive.TabIndex = 3
    Me.chk_inactive.Text = "xInactive"
    Me.chk_inactive.UseVisualStyleBackColor = True
    '
    'uc_provider
    '
    Me.uc_provider.Location = New System.Drawing.Point(402, 3)
    Me.uc_provider.Name = "uc_provider"
    Me.uc_provider.Size = New System.Drawing.Size(370, 190)
    Me.uc_provider.TabIndex = 4
    '
    'gb_group_by
    '
    Me.gb_group_by.Controls.Add(Me.opt_terminal_preasigned)
    Me.gb_group_by.Controls.Add(Me.opt_terminal_inserted)
    Me.gb_group_by.Controls.Add(Me.chk_group_by)
    Me.gb_group_by.Location = New System.Drawing.Point(3, 95)
    Me.gb_group_by.Name = "gb_group_by"
    Me.gb_group_by.Size = New System.Drawing.Size(185, 98)
    Me.gb_group_by.TabIndex = 1
    Me.gb_group_by.TabStop = False
    Me.gb_group_by.Text = "xOptions"
    '
    'opt_terminal_preasigned
    '
    Me.opt_terminal_preasigned.AutoSize = True
    Me.opt_terminal_preasigned.Location = New System.Drawing.Point(6, 66)
    Me.opt_terminal_preasigned.Name = "opt_terminal_preasigned"
    Me.opt_terminal_preasigned.Size = New System.Drawing.Size(129, 17)
    Me.opt_terminal_preasigned.TabIndex = 2
    Me.opt_terminal_preasigned.TabStop = True
    Me.opt_terminal_preasigned.Text = "xPreasignedTerm."
    Me.opt_terminal_preasigned.UseVisualStyleBackColor = True
    '
    'opt_terminal_inserted
    '
    Me.opt_terminal_inserted.AutoSize = True
    Me.opt_terminal_inserted.Location = New System.Drawing.Point(6, 43)
    Me.opt_terminal_inserted.Name = "opt_terminal_inserted"
    Me.opt_terminal_inserted.Size = New System.Drawing.Size(114, 17)
    Me.opt_terminal_inserted.TabIndex = 1
    Me.opt_terminal_inserted.Text = "xInsertedTerm."
    Me.opt_terminal_inserted.UseVisualStyleBackColor = True
    '
    'chk_group_by
    '
    Me.chk_group_by.AutoSize = True
    Me.chk_group_by.Location = New System.Drawing.Point(6, 20)
    Me.chk_group_by.Name = "chk_group_by"
    Me.chk_group_by.Size = New System.Drawing.Size(83, 17)
    Me.chk_group_by.TabIndex = 0
    Me.chk_group_by.Text = "xGroupBy"
    Me.chk_group_by.UseVisualStyleBackColor = True
    '
    'frm_tito_stackers_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(944, 587)
    Me.Name = "frm_tito_stackers_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_stackers_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.xGroupStacker.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.gb_group_by.ResumeLayout(False)
    Me.gb_group_by.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents xGroupStacker As System.Windows.Forms.GroupBox
  Friend WithEvents ef_stacker_indentifier As GUI_Controls.uc_entry_field
  Friend WithEvents ef_stacker_model As GUI_Controls.uc_entry_field
  Friend WithEvents ef_combo_terminal As GUI_Controls.uc_combo
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_available As System.Windows.Forms.CheckBox
  Friend WithEvents chk_inactive As System.Windows.Forms.CheckBox
  Friend WithEvents chk_collection_pending As System.Windows.Forms.CheckBox
  Friend WithEvents chk_inserted As System.Windows.Forms.CheckBox
  Friend WithEvents chk_without_terminal As System.Windows.Forms.CheckBox
  Friend WithEvents uc_provider As GUI_Controls.uc_provider
  Friend WithEvents gb_group_by As System.Windows.Forms.GroupBox
  Friend WithEvents opt_terminal_preasigned As System.Windows.Forms.RadioButton
  Friend WithEvents opt_terminal_inserted As System.Windows.Forms.RadioButton
  Friend WithEvents chk_group_by As System.Windows.Forms.CheckBox
End Class
