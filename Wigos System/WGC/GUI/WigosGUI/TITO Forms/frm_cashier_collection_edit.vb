Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common.Misc
Imports System.Data.SqlClient
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common.Users
Imports GUI_Reports.PrintDataset
Imports System.Text
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports GUI_Controls.frm_base_sel
Imports WSI.Common.TITO

Public Class frm_cashier_collection_edit
  Inherits frm_base_edit
#Region " Constants "

  Const GRID_TICKETS_COLUMNS_COUNT As Int32 = 4
  Const GRID_HEADERS_COUNT As Int32 = 2

  'Index Columns for tickets
  Const GRID_COLUMN_TICKET_ID_CORRECT_FOMAT As Int32 = 0
  Const GRID_COLUMN_TICKET_AMOUNT As Int32 = 1
  Const GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF As Int32 = 2
  Const GRID_COLUMN_TICKET_ID_ORIGINAL_FOMAT As Int32 = 3

  'Width FOR COLLECTION_DETAILS
  Const GRID_WIDTH_TICKET_OR_NOTE As Int32 = 0
  Const GRID_WIDTH_TICKET_VALIDATION_NUMBER As Int32 = 2500
  Const GRID_WIDTH_TICKET_AMOUNT As Int32 = 1725
  Const GRID_WIDTH_TICKET_REAL_VS_THEO_DIFF As Int32 = 900
  Const GRID_WIDTH_TICKET_VALIDATION_TYPE As Int32 = 0

  'Counters
  Private Const COUNTER_TOTAL_ITEMS As Integer = 0
  Private Const COUNTER_INFO As Integer = 1
  Private Const COUNTER_WARNING As Integer = 2
  Private Const COUNTER_ERROR As Integer = 3
  Private Const MAX_COUNTERS As Integer = 4

  'Counters colors
  Private Const COLOR_ERROR_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  Private Const COLOR_ERROR_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_WARNING_BACK = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
  Private Const COLOR_WARNING_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_INFO_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_INFO_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_ALL_ITEMS_BACK = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_ALL_ITEMS_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00

#End Region

#Region " Members "

  Dim m_new_collecion_mode As Boolean
  Dim m_session_id As Integer
  Dim m_real_ticket_sum As Double
  Dim m_real_ticket_count As Integer
  

#End Region

#Region " Overrides functions "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_NEW_COLLECTION

    MyBase.GUI_SetFormId()
  End Sub

  Protected Overrides Sub GUI_InitControls()

    MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3308)

    btn_save_ticket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2262)
    ef_ticket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2259)
    gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1971)
    tf_ok.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1972)
    tf_error.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1974)
    lbl_notes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2188)
    ef_cashier_collection_datetime.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3309)
    ef_cashier_terminal_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3310)
    gb_collection_details.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3311)
    ef_collection_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)
    ef_collection_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(603)



    Me.gb_theoretical_values.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2837)
    Me.ef_theoretical_count.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2259)
    Me.ef_theoretical_sum.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2271)

    Me.gb_real_values.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2836)
    Me.ef_real_count.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2259)
    Me.ef_real_sum.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2271)

    lbl_ok.BackColor = GetColor(COLOR_INFO_BACK)
    lbl_error.BackColor = GetColor(COLOR_ERROR_BACK)

    ef_cashier_collection_datetime.IsReadOnly = True
    ef_cashier_terminal_name.IsReadOnly = True

    ef_theoretical_count.IsReadOnly = True
    ef_theoretical_sum.IsReadOnly = True
    ef_real_count.IsReadOnly = True
    ef_real_sum.IsReadOnly = True

    ef_ticket.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 18)
    ef_theoretical_count.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8, 0)
    ef_theoretical_sum.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    ef_real_count.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER)
    ef_real_sum.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)

    ef_collection_status.Visible = False
    ef_collection_user.Visible = False
    ef_collection_status.IsReadOnly = True
    ef_collection_user.IsReadOnly = True

    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(27)
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True

    Call GUI_StyleSheet()

  End Sub


  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)
    Dim _collection As NEW_CASHIER_COLLECTION

    _collection = DbReadObject
    ef_cashier_terminal_name.Value = _collection.CashierName
    If Not _collection.CollectionDate.IsNull Then
      ef_cashier_collection_datetime.Value = GUI_FormatDate(_collection.CollectionDate.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    End If
    tb_notes.Text = _collection.Notes
    For Each _detail As NEW_CASHIER_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS In _collection.CollectionDetails
      Call GUI_SetupRow(_detail, Not _collection.AlreadyCollected)
    Next
    'Total thoretical rows must be shown after GUI_SetupRow
    If _collection.AlreadyCollected Or GeneralParam.GetInt32("TITO", "NoteAcCollectionType", -1) <> ENUM_TITO_COLLECTION_SHOW_TYPE.NOTHING Then
      ef_theoretical_count.Value = _collection.TheoreticalTicketCount
      ef_theoretical_sum.Value = _collection.TheoreticalTicketSum
    End If
    ef_real_count.Value = _collection.RealTicketCount
    ef_real_sum.Value = _collection.RealTicketSum
    m_new_collecion_mode = True
    If _collection.AlreadyCollected Then
      ef_ticket.IsReadOnly = True
      tb_notes.BackColor = ef_real_sum.BackColor
      tb_notes.ForeColor = Color.Blue
      tb_notes.ReadOnly = True
      btn_save_ticket.Enabled = False
      ef_collection_status.TextValue = IIf(_collection.Balanced, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2372), GLB_NLS_GUI_PLAYER_TRACKING.GetString(2373))
      ef_collection_status.Visible = True
      ef_collection_user.TextValue = _collection.UserName
      ef_collection_user.Visible = True
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
      GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
      If dg_tickets.NumRows <> 0 Then
        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True
      End If
      m_new_collecion_mode = False
    End If


  End Sub

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '       - DrawId
  '       - DrawName
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowEditItem(ByVal CashierCollectionId As Int64)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    'Me.m_new_collecion_mode = False
    Me.DbObjectId = CashierCollectionId
    'ef_ticket.Enabled = False

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Me.Display(True)
    End If



  End Sub



  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Dim _new_collection As NEW_CASHIER_COLLECTION

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New NEW_CASHIER_COLLECTION()
        _new_collection = DbEditedObject
        '_new_collection.CollectionDate = New TYPE_DATE_TIME()
        _new_collection.CollectionDetails = New List(Of NEW_CASHIER_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)()
        _new_collection.IsNewCollection = Me.m_new_collecion_mode
        _new_collection.UserId = CurrentUser.Id
        _new_collection.UserName = CurrentUser.Name
        _new_collection.CashierSessionId = m_session_id

        'Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        '  DbReadObject = DbEditedObject.Duplicate()

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        If DbStatus = ENUM_STATUS.STATUS_OK Then 'UPDATE_REQUIRED
          Call MyBase.GUI_DB_Operation(DbOperation)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT _
         , ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE

        If DbStatus <> ENUM_STATUS.STATUS_ERROR Then
          If Me.dg_tickets.NumRows <> 0 Then
            Call ExcelPrintDetails(True, True)
          End If
          _new_collection = DbEditedObject
          If Not _new_collection.Balanced Then
            Alarm.Register(AlarmSourceCode.User, _
                           CurrentUser.Id, _
                           CurrentUser.Name, _
                           AlarmCode.User_CashierSessionClosedFromGUI, _
                           GLB_NLS_GUI_PLAYER_TRACKING.GetString(3313, _new_collection.CashierName))
          End If
        Else
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2535), _
                ENUM_MB_TYPE.MB_TYPE_ERROR, _
                ENUM_MB_BTN.MB_BTN_OK, _
                ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        End If
        Call MyBase.GUI_DB_Operation(DbOperation)

      Case ENUM_DB_OPERATION.DB_OPERATION_AUDIT_UPDATE
          DbEditedObject.AuditorData.Notify(CurrentUser.GuiId, _
                                              CurrentUser.Id, _
                                              CurrentUser.Name, _
                                              CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.INSERT, _
                                              0, _
                                              DbReadObject.AuditorData)

      Case Else
          Call MyBase.GUI_DB_Operation(DbOperation)

    End Select
  End Sub

  Public Sub GUI_StyleSheet()
    Dim _collection As NEW_CASHIER_COLLECTION

    _collection = DbReadObject

    With Me.dg_tickets

      Call .Init(GRID_TICKETS_COLUMNS_COUNT, GRID_HEADERS_COUNT)

      .Counter(COUNTER_TOTAL_ITEMS).Visible = False
      .Counter(COUNTER_TOTAL_ITEMS).BackColor = GetColor(COLOR_ALL_ITEMS_BACK)
      .Counter(COUNTER_TOTAL_ITEMS).ForeColor = GetColor(COLOR_ALL_ITEMS_FORE)

      .Counter(COUNTER_INFO).Visible = False
      .Counter(COUNTER_INFO).BackColor = GetColor(COLOR_INFO_BACK)
      .Counter(COUNTER_INFO).ForeColor = GetColor(COLOR_INFO_FORE)

      .Counter(COUNTER_WARNING).Visible = False
      .Counter(COUNTER_WARNING).BackColor = GetColor(COLOR_WARNING_BACK)
      .Counter(COUNTER_WARNING).ForeColor = GetColor(COLOR_WARNING_FORE)

      .Counter(COUNTER_ERROR).Visible = False
      .Counter(COUNTER_ERROR).BackColor = GetColor(COLOR_ERROR_BACK)
      .Counter(COUNTER_ERROR).ForeColor = GetColor(COLOR_ERROR_FORE)

      ' Ticket number
      .Column(GRID_COLUMN_TICKET_ID_CORRECT_FOMAT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2269)
      .Column(GRID_COLUMN_TICKET_ID_CORRECT_FOMAT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2259)
      .Column(GRID_COLUMN_TICKET_ID_CORRECT_FOMAT).Width = GRID_WIDTH_TICKET_VALIDATION_NUMBER
      .Column(GRID_COLUMN_TICKET_ID_CORRECT_FOMAT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Ticket value
      .Column(GRID_COLUMN_TICKET_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2269)
      .Column(GRID_COLUMN_TICKET_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2260)
      .Column(GRID_COLUMN_TICKET_AMOUNT).Width = GRID_WIDTH_TICKET_AMOUNT
      .Column(GRID_COLUMN_TICKET_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Balanced
      .Column(GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Header(0).Text = "" 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(2274)
      .Column(GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2278)
      .Column(GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Width = GRID_WIDTH_TICKET_REAL_VS_THEO_DIFF
      .Column(GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      If Not _collection.AlreadyCollected And GeneralParam.GetInt32("TITO", "NoteAcCollectionType", -1) <> ENUM_TITO_COLLECTION_SHOW_TYPE.DETAILS Then
        .Column(GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Width = 0
      End If

      'Validation Type
      .Column(GRID_COLUMN_TICKET_ID_ORIGINAL_FOMAT).Width = GRID_WIDTH_TICKET_VALIDATION_TYPE

    End With
  End Sub

  Public Function GUI_SetupRow(ByVal CollectionDetail As NEW_CASHIER_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS, ByVal EditMode As Boolean) As Boolean
    Dim _idxBackgroundColor As Int16
    Dim _idxForeColor As Int16

    If CollectionDetail.ticket_validation_number > 0 AndAlso (GeneralParam.GetInt32("TITO", "NoteAcCollectionType", -1) = ENUM_TITO_COLLECTION_SHOW_TYPE.DETAILS Or Not EditMode) Then
      dg_tickets.AddRow()

      'setting the validation number to the cell
      Me.dg_tickets.Cell(dg_tickets.NumRows - 1, GRID_COLUMN_TICKET_ID_CORRECT_FOMAT).Value = ValidationNumberManager.FormatValidationNumber(CollectionDetail.ticket_validation_number, CollectionDetail.ticket_validation_type, False)
      Me.dg_tickets.Cell(dg_tickets.NumRows - 1, GRID_COLUMN_TICKET_ID_ORIGINAL_FOMAT).Value = CollectionDetail.ticket_validation_number
      Me.dg_tickets.Cell(dg_tickets.NumRows - 1, GRID_COLUMN_TICKET_AMOUNT).Value = GUI_FormatCurrency(CollectionDetail.ticket_amount)

      'the ticket collection could be taken as they are all collected, so they will appear as collected since the begining of the collection
      'the difference column will be in blank
      If CollectionDetail.ticket_collected Then
        _idxBackgroundColor = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
        _idxForeColor = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
        Me.dg_tickets.Counter(COUNTER_INFO).Value += 1

      Else
        _idxBackgroundColor = ENUM_GUI_COLOR.GUI_COLOR_RED_00
        _idxForeColor = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
        Me.dg_tickets.Cell(dg_tickets.NumRows - 1, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = -1
        dg_tickets.Counter(COUNTER_WARNING).Value += 1
      End If

      dg_tickets.Cell(dg_tickets.NumRows - 1, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).BackColor = GetColor(_idxBackgroundColor)
      dg_tickets.Cell(dg_tickets.NumRows - 1, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).ForeColor = GetColor(_idxForeColor)
    End If

    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean
    If e.KeyChar = Chr(Keys.Enter) Then

      Select Case Me.ActiveControl.Name
        Case ef_ticket.Name
          Call btn_save_ticket_ClickEvent()
        Case Else
          MyBase.GUI_KeyPress(sender, e)
      End Select

      Return True
    End If

  End Function ' GUI_ButtonClick


  ' PURPOSE: It gets the values from the entry field and set them to a PASSWORD_CONFIGURATION object
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim _new_collection As NEW_CASHIER_COLLECTION

    _new_collection = DbEditedObject

    _new_collection.CollectionDate.Value = WGDB.Now
    _new_collection.CollectionDetails = FillDetailsList()
    _new_collection.Notes = tb_notes.Text
    _new_collection.RealTicketCount = ef_real_count.Value
    _new_collection.RealTicketSum = ef_real_sum.Value

    If Math.Round(_new_collection.TheoreticalTicketSum, 2) = Math.Round(_new_collection.RealTicketSum, 2) AndAlso _new_collection.RealTicketCount = _new_collection.TheoreticalTicketCount Then
      _new_collection.Balanced = True
    Else
      _new_collection.Balanced = False
    End If

    For Each _detail As NEW_CASHIER_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS In _new_collection.CollectionDetails
      _new_collection.Balanced = (_new_collection.Balanced And _detail.matched)
    Next

    Try

    Catch

    End Try

  End Sub



  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)
    Dim _collection As NEW_CASHIER_COLLECTION
    Select Case ButtonId
      'Excel button
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call ExcelPrintDetails(False, False)

      Case ENUM_BUTTON.BUTTON_OK
        _collection = DbEditedObject
        If Not _collection.AlreadyCollected Then
          If NLS_MountedMsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2368), _
                                           GetStringToMessageBox(DbEditedObject), _
                                         ENUM_MB_TYPE.MB_TYPE_INFO, _
                                         ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_YES Then
            Call MyBase.GUI_ButtonClick(ButtonId)
          End If
        Else
          Call MyBase.GUI_ButtonClick(ButtonId)
        End If
      Case Else
          Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Return True
  End Function

#End Region

#Region " Private Functions "
  Private Sub AddTicketToCollection()

    Dim _row As Int32
    Dim _ticket_value As Double
    Dim _source_name As String
    Dim _description As String
    Dim _validation_number As Int64
    Dim _collection As NEW_CASHIER_COLLECTION

    _collection = DbReadObject
    If _collection.Status = TITO_MONEY_COLLECTION_STATUS.OPEN Then

    End If

    _ticket_value = TicketAmount(ef_ticket.Value)
    _validation_number = 0

    If _ticket_value < 0 Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2280), _
                                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                      ENUM_MB_BTN.MB_BTN_OK)
      ef_ticket.TextValue = ""
      Return

    End If

    If Int64.TryParse(ef_ticket.Value, _validation_number) Then
      _row = SearchRowOfTicket(_validation_number, GRID_COLUMN_TICKET_ID_ORIGINAL_FOMAT)
    End If

    'if row = -1 it means that there is no theoretical ticket with this number
    If _row >= 0 Then
      If dg_tickets.Cell(_row, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = "-1" Then
        dg_tickets.Cell(_row, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = ""

        m_real_ticket_sum = m_real_ticket_sum + CDbl(_ticket_value)
        m_real_ticket_count = m_real_ticket_count + 1

        Call UpdateDifferenceAndColor(_row, 0, False)

      ElseIf dg_tickets.Cell(_row, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = "" Then

        'generate an alarm if the ticket has been inserted before
        Using _db_trx As DB_TRX = New DB_TRX()
          _source_name = GLB_CurrentUser.Name & "@" & Environment.MachineName
          _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2263, ef_ticket.TextValue)

          If Alarm.Register(AlarmSourceCode.User, GLB_CurrentUser.Id, _source_name, _
                                AlarmCode.User_CashierSessionClosedFromGUI, _
                                _description, AlarmSeverity.Info, DateTime.MinValue, _db_trx.SqlTransaction) Then
            _db_trx.Commit()
          End If
        End Using

        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2274), _
                                        mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                        ENUM_MB_BTN.MB_BTN_OK, , _
                                        ef_ticket.TextValue)
        ef_ticket.TextValue = ""
        Return

      End If

    ElseIf (GeneralParam.GetInt32("TITO", "NoteAcCollectionType", -1) <> 2 AndAlso _ticket_value > 0) Then
      dg_tickets.AddRow()
      Me.dg_tickets.Cell(dg_tickets.NumRows - 1, GRID_COLUMN_TICKET_ID_CORRECT_FOMAT).Value = ValidationNumberManager.FormatValidationNumber(ef_ticket.TextValue, 1, False)
      Me.dg_tickets.Cell(dg_tickets.NumRows - 1, GRID_COLUMN_TICKET_ID_ORIGINAL_FOMAT).Value = ef_ticket.TextValue
      Me.dg_tickets.Cell(dg_tickets.NumRows - 1, GRID_COLUMN_TICKET_AMOUNT).Value = GUI_FormatCurrency(_ticket_value)
      m_real_ticket_sum = m_real_ticket_sum + CDbl(_ticket_value)
      m_real_ticket_count = m_real_ticket_count + 1

    End If

    Me.ef_real_sum.TextValue = GUI_FormatCurrency(m_real_ticket_sum)
    Me.ef_real_count.TextValue = GUI_FormatNumber(m_real_ticket_count, 0)

    ef_ticket.TextValue = ""

  End Sub

  Private Function FillDetailsList() As List(Of NEW_CASHIER_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)

    Dim _current_collection_detail As NEW_CASHIER_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS
    Dim _list_of_collection_details As List(Of NEW_CASHIER_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)
    Dim _idx As Int32

    _list_of_collection_details = New List(Of NEW_CASHIER_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)()

    For _idx = 0 To dg_tickets.NumRows - 1
      If dg_tickets.Value(_idx, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF) <> "-1" Then
        _current_collection_detail = New NEW_CASHIER_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS()
        _current_collection_detail.ticket_validation_number = dg_tickets.Value(_idx, GRID_COLUMN_TICKET_ID_ORIGINAL_FOMAT)
        _current_collection_detail.ticket_amount = GUI_ParseCurrency(dg_tickets.Value(_idx, GRID_COLUMN_TICKET_AMOUNT))
        _current_collection_detail.matched = (dg_tickets.Value(_idx, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF) = "")
        _list_of_collection_details.Add(_current_collection_detail)

      End If
    Next

    Return _list_of_collection_details

  End Function

  Private Function TicketAmount(ByVal TicketNumber As String) As Double
    Dim _sb As StringBuilder
    Dim _obj As Object
    Dim _ticket_number As Int64

    If TicketNumber = "" Or Not Int64.TryParse(TicketNumber, _ticket_number) Then
      Return -1
    End If

    _sb = New StringBuilder()

    _sb.AppendLine("    SELECT   TI_AMOUNT                             ")
    _sb.AppendLine("      FROM   TICKETS                               ")
    _sb.AppendLine("INNER JOIN   MONEY_COLLECTIONS ON TI_MONEY_COLLECTION_ID = MC_COLLECTION_ID  ")
    _sb.AppendLine("     WHERE   TI_VALIDATION_NUMBER = @pTicketNumber ")
    _sb.AppendLine("       AND   MC_STATUS = @pStatus       ")

    Using _db_trx As New DB_TRX()
      Using _sql_cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
        _sql_cmd.Parameters.Add("@pTicketNumber", SqlDbType.BigInt).Value = TicketNumber
        _sql_cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.PENDING
        _obj = _sql_cmd.ExecuteScalar

        If Not IsNothing(_obj) AndAlso IsNumeric(_obj) Then
          Return CDbl(_obj)
        End If
      End Using
    End Using

    Return -1

  End Function

  Private Function SearchRowOfTicket(ByVal Number As Int64, ByVal Column As Int32) As Int32

    Dim _idx As Int32

    For _idx = 0 To dg_tickets.NumRows - 1
      If Number = dg_tickets.Cell(_idx, Column).Value Then
        Return _idx
      End If
    Next

    Return -1

  End Function

  Private Sub UpdateDifferenceAndColor(ByVal RowIndex As Int32, ByVal NewDiff As Int32, ByVal IsBill As Boolean)

    Dim _idxBackgroundColor As Int16
    Dim _idxForeColor As Int16

    _idxBackgroundColor = IIf(NewDiff = 0, ENUM_GUI_COLOR.GUI_COLOR_WHITE_00, ENUM_GUI_COLOR.GUI_COLOR_RED_02)
    _idxForeColor = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00


    If RowIndex >= 0 Then
      dg_tickets.Cell(RowIndex, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = IIf(NewDiff = 0, "", NewDiff)
      dg_tickets.Cell(RowIndex, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).BackColor = GetColor(_idxBackgroundColor)
      dg_tickets.Cell(RowIndex, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).ForeColor = GetColor(_idxForeColor)
    End If

  End Sub

  'End Sub ' ExcelWithDetails

  ' PURPOSE: Print report with Table Details
  '
  '  PARAMS:
  '     - INPUT:
  '       - Title: report tittle
  '       - Grid: source grid
  '
  '     - OUTPUT:
  '
  Private Sub ExcelPrintDetails(Optional ByVal ToPrint As Boolean = False, Optional ByVal WithPreview As Boolean = False)

    Dim _print_excel_data As GUI_Reports.CLASS_PRINT_DATA
    Dim _report_layout As ExcelReportLayout
    Dim _sheet_layout As ExcelReportLayout.SheetLayout
    Dim _uc_grid_def As ExcelReportLayout.SheetLayout.UcGridDefinition
    Dim _new_collection As NEW_CASHIER_COLLECTION

    _new_collection = DbEditedObject

    'GUI_FormatDate(PrintDateTime, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    Try
      _print_excel_data = New GUI_Reports.CLASS_PRINT_DATA()
      _print_excel_data = LoadReportFilters()
      _report_layout = New ExcelReportLayout()

      With _report_layout
        .PrintData = _print_excel_data
        .PrintDateTime = frm_base_sel.GUI_GetPrintDateTime()
        _sheet_layout = New ExcelReportLayout.SheetLayout()
        _sheet_layout.Title = Me.FormTitle
        _sheet_layout.Name = Me.Text


        If dg_tickets.NumRows > 0 Then
          _uc_grid_def = New ExcelReportLayout.SheetLayout.UcGridDefinition()
          _uc_grid_def.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2259)
          _uc_grid_def.UcGrid = dg_tickets
          _sheet_layout.ListOfUcGrids.Add(_uc_grid_def)
        End If

        .ListOfSheets.Add(_sheet_layout)
      End With

      Call _report_layout.GUI_GenerateExcel()

    Catch

    End Try

  End Sub ' PrintDetails


  ' PURPOSE: Save the selected filter for print/excel
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  Private Function LoadReportFilters()

    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA

    _print_data = New GUI_Reports.CLASS_PRINT_DATA()

    _print_data.SetFilter(ef_cashier_terminal_name.Text, ef_cashier_terminal_name.TextValue)
    If m_new_collecion_mode Then
      _print_data.SetFilter(ef_collection_user.Text, CurrentUser.Name)
      _print_data.SetFilter(ef_cashier_collection_datetime.Text, GUI_FormatDate(WGDB.Now, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM))
    Else
      _print_data.SetFilter(ef_collection_user.Text, ef_collection_user.TextValue)
      _print_data.SetFilter(ef_cashier_collection_datetime.Text, ef_cashier_collection_datetime.TextValue)
    End If

    _print_data.SetFilter(lbl_notes.Text, tb_notes.Text)

    _print_data.SetFilter(gb_theoretical_values.Text, ef_theoretical_sum.TextValue)

    _print_data.FilterHeaderWidth(1) = 2000
    _print_data.FilterHeaderWidth(2) = 2500

    Return _print_data

  End Function ' LoadReportFilters

  Private Function GetStringToMessageBox(ByVal NewCollection As NEW_CASHIER_COLLECTION)

    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    _sb.AppendLine("")
    _sb.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2368))
    _sb.AppendLine("")
    If WSI.Common.GeneralParam.GetInt32("TITO", "TicketsCollection") = 0 Then
      _sb.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2370, GUI_FormatCurrency(NewCollection.TheoreticalTicketSum, 2)))
    Else
      _sb.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2370, GUI_FormatCurrency(m_real_ticket_sum)))
    End If
    If Math.Round(NewCollection.TheoreticalTicketSum, 2) = Math.Round(m_real_ticket_sum, 2) AndAlso NewCollection.TheoreticalTicketCount = m_real_ticket_count Then
      _sb.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2371, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2372)))
    Else
      _sb.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2371, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2373)))
    End If

    Return _sb.ToString()
  End Function
#End Region
#Region " Events "

  Private Sub btn_save_ticket_ClickEvent() Handles btn_save_ticket.ClickEvent

    Call AddTicketToCollection()
  End Sub

#End Region
End Class
