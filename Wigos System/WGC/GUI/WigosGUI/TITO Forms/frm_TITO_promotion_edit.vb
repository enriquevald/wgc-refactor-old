'-------------------------------------------------------------------
' Copyright � 2010 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_promotion_edit
' DESCRIPTION:   New or Edit Promotion
' AUTHOR:        Raul Cervera
' CREATION DATE: 10-MAY-2010
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 12-JUL-2010  FBA    Initial version.
' 12-SEP-2013  DHA    Added Cash Desk Draw promotions
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common
Imports System.Text

Public Class frm_TITO_promotion_edit
  Inherits frm_base_edit

#Region " Constants "

  Private Const LEN_PROMO_NAME As Integer = 20
  Private Const LEN_FLAG_REQUIRED_DIGITS As Integer = 6
  Private Const LEN_FLAG_AWARDED_DIGITS As Integer = 2
  Private Const MAX_FLAGS_AWARDED As Integer = 10

  ' SELECT FLAG GRID
  Private Const GRID_FLAG_IDX As Integer = 0
  Private Const GRID_FLAG_CHECKED As Integer = 1
  Private Const GRID_FLAG_COLOR As Integer = 2
  Private Const GRID_FLAG_NAME As Integer = 3
  Private Const GRID_FLAG_COUNT As Integer = 4

  Private Const GRID_SELECT_FLAG_COLUMNS As Integer = 5
  Private Const GRID_SELECT_FLAG_HEADER_ROWS As Integer = 1

#End Region ' Constants

#Region " Enums "
  Private Enum GRID_DATA_STATUS
    OK = 1
    VALUE_NOT_VALID = 2
    NO_ONE_CHECKED = 3
    MAX_FLAGS_EXCEEDED = 4
  End Enum
#End Region

#Region " Members "

  Private m_promotion_copied_id As Long
  Private m_promotion_copy_operation As Boolean
  Private m_input_promo_name As String
  Private DeleteOperation As Boolean
  'Private m_closing_time As Integer

  ' RCI 17-NOV-2010: Used to show label indicating permissions not applicable in Mobile Bank.
  Private m_mb_auto_promotion As Boolean

  Private m_new_promo As Boolean

#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = 0

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    Dim closing_time As Integer
    Dim mb_auto_promotion As Integer

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    'checks if the app is running in tito mode
    If Not GeneralParam.GetBoolean("TITO", "TITOMode") Then
      Me.gb_tito.Visible = False
    End If
    'Add any initialization after the MyBase.GUI_InitControls() call    
    closing_time = GetDefaultClosingTime()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(257)

    ' Hide the delete button if ScreenMode is New
    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Else
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = True
    End If

    ' Enabled control
    gb_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(260)
    opt_enabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(264)
    opt_disabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(265)

    ' Special Permission
    gb_special_permission.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(400)
    chk_special_permission.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(401)
    opt_special_permission_a.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(402)
    opt_special_permission_b.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(403)
    lbl_not_applicable_mb1.Visible = False
    lbl_not_applicable_mb1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(428)

    mb_auto_promotion = 0
    Integer.TryParse(WSI.Common.Misc.ReadGeneralParams("MobileBank", "AutomaticallyAssignPromotion"), mb_auto_promotion)
    m_mb_auto_promotion = (mb_auto_promotion = 1)

    ' Name
    ef_promo_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253)
    ef_promo_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_PROMO_NAME)

    ' Type
    gb_credit_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254)
    opt_credit_non_redeemable.Text = GLB_NLS_GUI_INVOICING.GetString(338)
    opt_credit_redeemable.Text = GLB_NLS_GUI_INVOICING.GetString(337)
    opt_point.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354)

    ' Images
    lbl_icon.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(719)
    lbl_image.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(687)
    Call img_icon.Init(uc_image.MAXIMUN_RESOLUTION.x200X150)
    Call img_image.Init(uc_image.MAXIMUN_RESOLUTION.x1280X1024)

    ' uc_chedule control
    uc_promo_schedule.SetDefaults()

    ' Filters
    tab_filters.TabPages(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(213)
    tab_filters.TabPages(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(216)
    tab_filters.TabPages(2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(404)
    tab_filters.TabPages(3).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(408)
    tab_flags_required.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1397) 'By flag
    chk_by_gender.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)
    opt_gender_male.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(214)
    opt_gender_female.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(215)
    chk_by_birth_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)
    opt_birth_date_only.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(217)
    opt_birth_month_only.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(218)
    chk_by_level.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(407)
    chk_level_anonymous.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(405)
    chk_level_01.Text = GetCashierPlayerTrackingData("Level01.Name")
    If chk_level_01.Text = "" Then
      chk_level_01.Text = GLB_NLS_GUI_CONFIGURATION.GetString(289)
    End If
    chk_level_02.Text = GetCashierPlayerTrackingData("Level02.Name")
    If chk_level_02.Text = "" Then
      chk_level_02.Text = GLB_NLS_GUI_CONFIGURATION.GetString(290)
    End If
    chk_level_03.Text = GetCashierPlayerTrackingData("Level03.Name")
    If chk_level_03.Text = "" Then
      chk_level_03.Text = GLB_NLS_GUI_CONFIGURATION.GetString(291)
    End If
    chk_level_04.Text = GetCashierPlayerTrackingData("Level04.Name")
    If chk_level_04.Text = "" Then
      chk_level_04.Text = GLB_NLS_GUI_CONFIGURATION.GetString(332)
    End If
    chk_by_freq.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)
    ef_freq_last_days.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(409)
    ef_freq_last_days.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    ef_freq_min_days.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(410)
    ef_freq_min_days.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    ef_freq_min_cash_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(411)
    ef_freq_min_cash_in.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)
    chk_by_flag.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)

    ' Expiration
    'gb_expiration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(223)
    tp_expiration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(223)
    opt_expiration_days.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(219)
    opt_expiration_hours.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(219)
    ef_expiration_days.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(220, GUI_FormatDate(DateTime.Today.AddHours(closing_time), ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMM))
    ef_expiration_days.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    ef_expiration_hours.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(221)
    ef_expiration_hours.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)

    ' DHA 12-APR-2013: Setting the visibilty of the controls
    Me.lbl_credits_non_redeemable.Enabled = False
    Me.lbl_credits_non_redeemable.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1901)

    Me.chk_expiration_limit.Enabled = False
    Me.chk_expiration_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1902)

    Me.dtp_expiration_limit.Enabled = False
    Me.dtp_expiration_limit.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.dtp_expiration_limit.Value = DateTime.MinValue

    ' Ticket Footer
    tp_ticketfooter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1284)

    ' Cash in
    tab_chash_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(417)
    ' Minimum cash in
    ef_min_cash_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(284)
    ef_min_cash_in.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)
    ef_min_cash_in_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)

    ' Additional cash in
    ef_cash_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(226)
    ef_cash_in.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)
    ef_cash_in_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)

    ' Spent
    tab_spent.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(418)
    ' Minimum spent
    ef_min_spent.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(420)
    ef_min_spent.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)
    ef_min_spent_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)

    ' Additional spent
    ef_spent.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(226)
    ef_spent.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)
    ef_spent_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)

    ' Token rewards
    tab_tokens.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(419)
    ef_num_tokens.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(228)
    ef_num_tokens.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    ef_token_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
    ef_token_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(261)
    ef_token_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_PROMO_NAME)

    ' Flags awarded
    tab_flags_awarded.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) 'Bandera
    chk_flags_awarded.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(406)

    'CUSTOMER LIMIT
    tab_by_customer.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(523)
    ' Daily limit
    ef_daily_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(230)

    ' Monthly limit
    ef_monthly_limit.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(235)

    'PROMOTION LIMIT
    tab_by_promotion.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(524)
    ' Daily limit
    ef_promotion_limit_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(230)

    ' Monthly limit
    ef_promotion_limit_month.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(235)

    ' JAB 20-AUG-2012 Added Promotion Global Limit
    ' Global limit
    ef_promotion_limit_global.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1247)

    ' Won Lock
    gb_won_lock.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(531) '531 Lock
    ef_won_lock.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(532) '532 Quantity
    ef_won_lock.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5)

    ' Test
    gb_test.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(232)
    ef_test_cash_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(233)
    ef_test_cash_in.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)
    ef_test_spent.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(421)
    ef_test_spent.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)
    ef_test_num_tokens.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(228)
    ef_test_num_tokens.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)

    ' RCI 16-NOV-2010: BUG Text Visible
    ef_freq_last_days.TextVisible = True
    ef_freq_min_days.TextVisible = True
    ef_freq_min_cash_in.TextVisible = True

    ef_daily_limit.TextVisible = True
    ef_monthly_limit.TextVisible = True
    ef_promotion_limit_day.TextVisible = True
    ef_promotion_limit_month.TextVisible = True
    ' JAB 20-AUG-2012 Added Promotion Global Limit
    ef_promotion_limit_global.TextVisible = True

    ef_min_cash_in.TextVisible = True
    ef_min_cash_in_reward.TextVisible = True
    ef_cash_in.TextVisible = True
    ef_cash_in_reward.TextVisible = True
    ef_min_spent.TextVisible = True
    ef_min_spent_reward.TextVisible = True
    ef_spent.TextVisible = True
    ef_spent_reward.TextVisible = True
    ef_num_tokens.TextVisible = True
    ef_token_reward.TextVisible = True
    ef_token_name.TextVisible = True

    Me.lbl_icon_optimun_size.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(733, "200", "150")
    Me.lbl_image_optimun_size.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(733, "1280", "1024")


    Me.img_image.ImageInfoFont = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))

    ' Promotion Categories
    Me.cmb_categories.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1336)
    Me.cmb_categories.Add(PromotionCategories.Categories())

    Me.chk_visible_on_PromoBOX.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1434) '1434 "Visible on PromoBOX"

    Me.img_icon.Enabled = Me.chk_visible_on_PromoBOX.Checked
    Me.img_image.Enabled = Me.chk_visible_on_PromoBOX.Checked

    Me.Timer1.Interval = 1000
    Me.Timer1.Start()

    ' promotion TITO
    Me.gb_tito.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2303)
    Me.chk_allow_tickets_tito.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2355)
    Me.ef_quantity_tickets_tito.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2354)
    Me.ef_quantity_tickets_tito.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5)
    Me.ef_generated_tickets_tito.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2356)
    Me.ef_generated_tickets_tito.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5)
    Me.ef_generated_tickets_tito.Enabled = False

    ' Promotion Flags
    Call InitializeRequiredFlags()
    Call InitializeAwardedFlags()

  End Sub ' GUI_InitControls

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim nls_param1 As String
    Dim nls_param2 As String
    Dim rc As mdl_NLS.ENUM_MB_RESULT
    Dim promo_item As CLASS_TITO_PROMOTION
    Dim freq_last_days As Integer
    Dim freq_min_days As Integer
    Dim _control_limit As UserControl
    Dim _selected_index As Integer

    _control_limit = Nothing
    _selected_index = 0

    promo_item = DbReadObject

    ' Name
    If ef_promo_name.Value = "" Then
      nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(261)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
      Call ef_promo_name.Focus()

      Return False
    End If

    ' Special permission
    If chk_special_permission.Checked Then
      If opt_special_permission_a.Checked = False And opt_special_permission_b.Checked = False Then
        ' One option must be selected
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call opt_special_permission_a.Focus()

        Return False
      End If
    End If

    ' Expiration dates range
    If uc_promo_schedule.DateFrom > uc_promo_schedule.DateTo Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call uc_promo_schedule.SetFocus(1)

      Return False
    End If

    ' No Week days selected
    If uc_promo_schedule.Weekday = uc_promo_schedule.NoWeekDaysSelected() Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(110), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call uc_promo_schedule.SetFocus(4)

      Return False
    End If

    ' Gender filter
    If chk_by_gender.Checked Then
      If opt_gender_male.Checked = False And opt_gender_female.Checked = False Then
        ' One option must be selected
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tab_filters.SelectedIndex = 0
        Call opt_gender_male.Focus()

        Return False
      End If
    End If

    ' Birth Date filter
    If chk_by_birth_date.Checked Then
      If opt_birth_date_only.Checked = False And opt_birth_month_only.Checked = False Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tab_filters.SelectedIndex = 1
        Call opt_birth_date_only.Focus()

        Return False
      End If

      If opt_birth_date_only.Checked Then
        ' Daily limit mandatory for birthday filter
        If GUI_ParseCurrency(ef_daily_limit.Value) <= 0 _
        And GUI_ParseCurrency(ef_promotion_limit_day.Value) <= 0 Then
          nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(230)
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
          tab_filters.SelectedIndex = 1
          tab_limit.SelectedIndex = 0
          Call ef_daily_limit.Focus()

          Return False
        End If

        ' All weekdays must be marked if birthday-day only option is selected
        If uc_promo_schedule.Weekday <> uc_promo_schedule.AllWeekDaysSelected() Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(120), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
          tab_filters.SelectedIndex = 1
          Call uc_promo_schedule.SetFocus(5)

          Return False
        End If
      Else
        ' Monthly limit mandatory for birthday filter
        If GUI_ParseCurrency(ef_monthly_limit.Value) <= 0 _
        And GUI_ParseCurrency(Me.ef_promotion_limit_month.Value) <= 0 Then
          nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(235)
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
          tab_filters.SelectedIndex = 1
          tab_limit.SelectedIndex = 0
          Call ef_monthly_limit.Focus()

          Return False
        End If
      End If
    End If

    ' Level filter
    If chk_by_level.Checked Then
      If ((chk_level_anonymous.Checked = False) Or (opt_point.Checked = True)) _
         And chk_level_01.Checked = False _
         And chk_level_02.Checked = False _
         And chk_level_03.Checked = False _
         And chk_level_04.Checked = False Then
        ' One option must be selected
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tab_filters.SelectedIndex = 2
        Call chk_level_anonymous.Focus()

        Return False
      End If
    End If

    ' Frequency filter
    If chk_by_freq.Checked Then
      If ef_freq_last_days.Value = "" Then
        nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(409)
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
        tab_filters.SelectedIndex = 3
        Call ef_freq_last_days.Focus()

        Return False
      End If
      If ef_freq_min_days.Value = "" Then
        nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(410)
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
        tab_filters.SelectedIndex = 3
        Call ef_freq_min_days.Focus()

        Return False
      End If
      If ef_freq_min_cash_in.Value = "" Then
        nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(411)
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
        tab_filters.SelectedIndex = 3
        Call ef_freq_min_cash_in.Focus()

        Return False
      End If

      freq_last_days = GUI_ParseNumber(ef_freq_last_days.Value)
      freq_min_days = GUI_ParseNumber(ef_freq_min_days.Value)
      If freq_last_days < freq_min_days Then
        nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(409)
        nls_param2 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(410)
        Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1, nls_param2)
        tab_filters.SelectedIndex = 3
        Call ef_freq_last_days.Focus()

        Return False
      End If
    End If

    ' Expiration 
    'If gb_expiration.Enabled Then
    If pa_expiration.Enabled Then

      If opt_expiration_days.Checked = False And opt_expiration_hours.Checked = False Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(117), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call opt_expiration_days.Focus()

        Return False
      End If

      If opt_expiration_days.Checked Then
        If GUI_ParseNumber(ef_expiration_days.Value) <= 0 Then
          nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(223)
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
          Call ef_expiration_days.Focus()

          Return False
        End If
      End If

      If opt_expiration_hours.Checked Then
        If GUI_ParseNumber(ef_expiration_hours.Value) <= 0 Then
          nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(223)
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
          Call ef_expiration_hours.Focus()

          Return False
        End If
      End If

      ' DHA 12-APR-2013: Event of chk_expiration_limit changed checked
      If chk_expiration_limit.Checked And dtp_expiration_limit.Value < uc_promo_schedule.DateTo Then
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1904), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        dtp_expiration_limit.Focus()
        Return False
      End If
    End If

    ' Amounts
    ' Cash In: all fields must be provided if at least one is present
    If GUI_ParseNumber(ef_cash_in.Value) > 0 Or GUI_ParseCurrency(ef_cash_in_reward.Value) > 0 Then

      If GUI_ParseNumber(ef_cash_in.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tabPromotions.SelectedIndex = 0
        Call ef_cash_in.Focus()

        Return False
      End If

      If GUI_ParseCurrency(ef_cash_in_reward.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tabPromotions.SelectedIndex = 0
        Call ef_cash_in_reward.Focus()

        Return False
      End If
    End If

    ' Min Spent: all fields must be provided if at least one is present
    If GUI_ParseNumber(ef_min_spent.Value) > 0 Or GUI_ParseCurrency(ef_min_spent_reward.Value) > 0 Then

      If GUI_ParseNumber(ef_min_spent.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tabPromotions.SelectedIndex = 1
        Call ef_min_spent.Focus()

        Return False
      End If

      If GUI_ParseCurrency(ef_min_spent_reward.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tabPromotions.SelectedIndex = 1
        Call ef_min_spent_reward.Focus()

        Return False
      End If
    End If

    ' Spent: all fields must be provided if at least one is present
    If GUI_ParseNumber(ef_spent.Value) > 0 Or GUI_ParseCurrency(ef_spent_reward.Value) > 0 Then

      If GUI_ParseNumber(ef_spent.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tabPromotions.SelectedIndex = 1
        Call ef_spent.Focus()

        Return False
      End If

      If GUI_ParseCurrency(ef_spent_reward.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tabPromotions.SelectedIndex = 1
        Call ef_spent_reward.Focus()

        Return False
      End If
    End If

    ' Providers (if listed or not listed must have at least one selected)
    If Not uc_provider_filter1.CheckAtLeastOneSelected() Then
      nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(566)
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
      Call uc_provider_filter1.Focus()

      Return False
    End If

    ' Tokens: all fields must be provided if at least one is present
    If GUI_ParseNumber(ef_num_tokens.Value) > 0 Or GUI_ParseCurrency(ef_token_reward.Value) > 0 Or ef_token_name.Value <> "" Then

      If GUI_ParseNumber(ef_num_tokens.Value) <= 0 Then
        nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(228)
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
        tabPromotions.SelectedIndex = 2
        Call ef_num_tokens.Focus()

        Return False
      End If


      'RXM 29 AGO 2012 added "Points" option to nls_param1

      If GUI_ParseCurrency(ef_token_reward.Value) <= 0 Then
        If opt_credit_redeemable.Checked Then
          nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
        ElseIf opt_credit_non_redeemable.Checked Then
          nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        Else
          nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(205) ' Points
        End If
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
        tabPromotions.SelectedIndex = 2
        Call ef_token_reward.Focus()

        Return False
      End If

      If ef_token_name.Value = "" Then
        nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(261)
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
        tabPromotions.SelectedIndex = 2
        Call ef_token_name.Focus()

        Return False
      End If

    End If

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      Select Case promo_item.Type
        Case WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_COVER_COUPON, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_PER_PRIZE, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_PRIZE_COUPON, _
             WSI.Common.Promotion.PROMOTION_TYPE.PER_POINTS_NOT_REDEEMABLE, _
             WSI.Common.Promotion.PROMOTION_TYPE.PER_POINTS_REDEEMABLE, _
             WSI.Common.Promotion.PROMOTION_TYPE.EXTERNAL_NON_REDEEM, _
             WSI.Common.Promotion.PROMOTION_TYPE.EXTERNAL_REDEEM, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_PER_RECHARGE_WITH_CURRENCY_EXCHANGE, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_RE, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_RE, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_PLAYER_SAID_NO, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_SERVER_DOWN, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_WINNER_NR2, _
             WSI.Common.Promotion.PROMOTION_TYPE.AUTOMATIC_CASH_DESK_DRAW_LOSER_NR2

          Return True

      End Select
    End If


    ' At least a reward amount must be specified: either cash_in, spent or tokens.
    If GUI_ParseCurrency(ef_min_cash_in_reward.Value) <= 0 And _
       GUI_ParseCurrency(ef_cash_in_reward.Value) <= 0 And _
       GUI_ParseCurrency(ef_min_spent_reward.Value) <= 0 And _
       GUI_ParseCurrency(ef_spent_reward.Value) <= 0 And _
       GUI_ParseCurrency(ef_token_reward.Value) <= 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(119), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      tabPromotions.SelectedIndex = 0
      Call ef_min_cash_in_reward.Focus()

      Return False
    End If

    ' Daily limit mandatory if no minimum cash in is set
    If GUI_ParseCurrency(ef_min_cash_in.Value) <= 0 And _
       GUI_ParseCurrency(ef_min_cash_in_reward.Value) > 0 And _
       GUI_ParseCurrency(ef_daily_limit.Value) <= 0 And _
       GUI_ParseCurrency(Me.ef_promotion_limit_day.Value) <= 0 Then
      nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(230)
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)

      If GUI_ParseCurrency(Me.ef_daily_limit.Value) <= 0 Then
        Call ef_daily_limit.Focus()
        tab_limit.SelectedIndex = 0
      ElseIf GUI_ParseCurrency(Me.ef_promotion_limit_day.Value) <= 0 Then
        Call ef_promotion_limit_day.Focus()
        tab_limit.SelectedIndex = 1
      End If

      Return False
    End If

    '
    'LIMITS
    '

    ' Check minimum rewards do not exceed limit GLOBAL
    If GUI_ParseCurrency(ef_promotion_limit_global.Value) > 0 Then
      If GUI_ParseCurrency(ef_promotion_limit_global.Value) < GUI_ParseCurrency(ef_min_cash_in_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_global.Value) < GUI_ParseCurrency(ef_cash_in_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_global.Value) < GUI_ParseCurrency(ef_min_spent_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_global.Value) < GUI_ParseCurrency(ef_spent_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_global.Value) < GUI_ParseCurrency(ef_token_reward.Value) Then

        _selected_index = 1
        _control_limit = ef_promotion_limit_global

      End If
    End If

    ' Check minimum rewards do not exceed monthly limit GLOBAL
    If GUI_ParseCurrency(ef_promotion_limit_month.Value) > 0 Then
      If GUI_ParseCurrency(ef_promotion_limit_month.Value) < GUI_ParseCurrency(ef_min_cash_in_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_month.Value) < GUI_ParseCurrency(ef_cash_in_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_month.Value) < GUI_ParseCurrency(ef_min_spent_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_month.Value) < GUI_ParseCurrency(ef_spent_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_month.Value) < GUI_ParseCurrency(ef_token_reward.Value) Then

        _selected_index = 1
        _control_limit = ef_promotion_limit_month

      End If
    End If

    ' Check minimum rewards do not exceed daily limit GLOBAL
    If GUI_ParseCurrency(ef_promotion_limit_day.Value) > 0 Then
      If GUI_ParseCurrency(ef_promotion_limit_day.Value) < GUI_ParseCurrency(ef_min_cash_in_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_day.Value) < GUI_ParseCurrency(ef_cash_in_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_day.Value) < GUI_ParseCurrency(ef_min_spent_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_day.Value) < GUI_ParseCurrency(ef_spent_reward.Value) _
      Or GUI_ParseCurrency(ef_promotion_limit_day.Value) < GUI_ParseCurrency(ef_token_reward.Value) Then

        _selected_index = 1
        _control_limit = ef_promotion_limit_day

      End If
    End If

    ' Check minimum rewards do not exceed monthly limit
    If GUI_ParseCurrency(ef_monthly_limit.Value) > 0 Then
      If GUI_ParseCurrency(ef_monthly_limit.Value) < GUI_ParseCurrency(ef_min_cash_in_reward.Value) _
      Or GUI_ParseCurrency(ef_monthly_limit.Value) < GUI_ParseCurrency(ef_cash_in_reward.Value) _
      Or GUI_ParseCurrency(ef_monthly_limit.Value) < GUI_ParseCurrency(ef_min_spent_reward.Value) _
      Or GUI_ParseCurrency(ef_monthly_limit.Value) < GUI_ParseCurrency(ef_spent_reward.Value) _
      Or GUI_ParseCurrency(ef_monthly_limit.Value) < GUI_ParseCurrency(ef_token_reward.Value) Then

        _selected_index = 0
        _control_limit = ef_monthly_limit

      End If
    End If

    ' Check minimum rewards do not exceed daily limit
    If GUI_ParseCurrency(ef_daily_limit.Value) > 0 Then
      If GUI_ParseCurrency(ef_daily_limit.Value) < GUI_ParseCurrency(ef_min_cash_in_reward.Value) _
          Or GUI_ParseCurrency(ef_daily_limit.Value) < GUI_ParseCurrency(ef_cash_in_reward.Value) _
          Or GUI_ParseCurrency(ef_daily_limit.Value) < GUI_ParseCurrency(ef_min_spent_reward.Value) _
          Or GUI_ParseCurrency(ef_daily_limit.Value) < GUI_ParseCurrency(ef_spent_reward.Value) _
          Or GUI_ParseCurrency(ef_daily_limit.Value) < GUI_ParseCurrency(ef_token_reward.Value) Then

        _selected_index = 0
        _control_limit = ef_daily_limit

      End If
    End If

    '
    'LIMITS
    '

    ' Check fields for Won Lock
    If chk_won_lock.Checked Then

      If ef_won_lock.Value = "" Then
        nls_param1 = ef_won_lock.Text
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , nls_param1)
        Call ef_won_lock.Focus()

        Return False
      End If

      ' At least a reward amount must be specified
      If GUI_ParseCurrency(ef_min_cash_in_reward.Value) <= 0 And GUI_ParseCurrency(ef_cash_in_reward.Value) <= 0 And GUI_ParseCurrency(ef_token_reward.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(119), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        tabPromotions.SelectedIndex = 0
        Call ef_min_cash_in_reward.Focus()

        Return False
      End If

    End If

    If Not IsNothing(_control_limit) Then

      rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(122), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2)

      If rc = ENUM_MB_RESULT.MB_RESULT_NO Then
        tab_limit.SelectedIndex = _selected_index
        Call _control_limit.Focus()

        Return False
      End If

    End If

    If chk_by_flag.Checked Then
      Select Case IsFlagGridDataOK(dg_flags_required)
        Case GRID_DATA_STATUS.VALUE_NOT_VALID ' Flag checked without a count
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1398), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
          Return False
        Case GRID_DATA_STATUS.NO_ONE_CHECKED ' No one flag checked
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1333), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
          Return False
        Case GRID_DATA_STATUS.MAX_FLAGS_EXCEEDED
        Case GRID_DATA_STATUS.OK
        Case Else
      End Select
    End If

    If chk_flags_awarded.Checked Then
      Select Case IsFlagGridDataOK(dg_flags_awarded, MAX_FLAGS_AWARDED)
        Case GRID_DATA_STATUS.VALUE_NOT_VALID ' Flag checked without a count
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1399), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
          Return False
        Case GRID_DATA_STATUS.NO_ONE_CHECKED ' No one flag checked
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1333), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
          Return False
        Case GRID_DATA_STATUS.MAX_FLAGS_EXCEEDED
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1417), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(1414).ToLower, MAX_FLAGS_AWARDED)
          Return False
        Case GRID_DATA_STATUS.OK
        Case Else
      End Select
    End If

    ' Visible on PromoBOX

    ' Only warns user in case that have activated Visible-On-PromoBOX check and image not defined.    
    If chk_visible_on_PromoBOX.Checked And Me.img_image.Image Is Nothing Then

      ' And is a new promotion 
      ' or previously read promotion hasn't Visible-On-PromoBOX checked
      ' or previously read promotion had an image
      If m_new_promo _
         Or Not promo_item.VisibleOnPromoBOX _
         Or Not promo_item.Image Is Nothing Then

        ' Ask user to continue.
        ' 1435 "Ha indicado que la promoci�n sea visible en PromoBOX pero no ha asociado la imagen a la promoci�n, por lo que la promoci�n no se mostrar� en el PromoBOX.\n\n�Desea continuar?"
        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1435), _
                      ENUM_MB_TYPE.MB_TYPE_WARNING, _
                      ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then

          Return False
        End If
      End If
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim promo_item As CLASS_TITO_PROMOTION

    promo_item = DbEditedObject

    promo_item.Name = ef_promo_name.Value
    promo_item.Enabled = opt_enabled.Checked

    If chk_special_permission.Checked Then
      If opt_special_permission_a.Checked Then
        promo_item.SpecialPermission = CLASS_TITO_PROMOTION.ENUM_SPECIAL_PERMISSION.PERMISSION_A
      Else
        promo_item.SpecialPermission = CLASS_TITO_PROMOTION.ENUM_SPECIAL_PERMISSION.PERMISSION_B
      End If
    Else
      promo_item.SpecialPermission = CLASS_TITO_PROMOTION.ENUM_SPECIAL_PERMISSION.NOT_SET
    End If

    promo_item.DateStart = uc_promo_schedule.DateFrom
    promo_item.DateFinish = uc_promo_schedule.DateTo

    promo_item.ScheduleWeekday = uc_promo_schedule.Weekday
    promo_item.Schedule1TimeFrom = uc_promo_schedule.TimeFrom
    promo_item.Schedule1TimeTo = uc_promo_schedule.TimeTo
    promo_item.Schedule2Enabled = uc_promo_schedule.SecondTime
    If promo_item.Schedule2Enabled Then
      promo_item.Schedule2TimeFrom = uc_promo_schedule.SecondTimeFrom
      promo_item.Schedule2TimeTo = uc_promo_schedule.SecondTimeTo
    Else
      promo_item.Schedule2TimeFrom = 0
      promo_item.Schedule2TimeTo = 0
    End If

    promo_item.GenderFilter = CLASS_TITO_PROMOTION.ENUM_GENDER_FILTER.NOT_SET
    If chk_by_gender.Checked Then
      If opt_gender_male.Checked Then
        promo_item.GenderFilter = CLASS_TITO_PROMOTION.ENUM_GENDER_FILTER.MEN_ONLY
      ElseIf opt_gender_female.Checked Then
        promo_item.GenderFilter = CLASS_TITO_PROMOTION.ENUM_GENDER_FILTER.WOMEN_ONLY
      End If
    End If

    promo_item.BirthdayFilter = CLASS_TITO_PROMOTION.ENUM_BIRTHDAY_FILTER.NOT_SET
    If chk_by_birth_date.Checked Then
      If opt_birth_date_only.Checked Then
        promo_item.BirthdayFilter = CLASS_TITO_PROMOTION.ENUM_BIRTHDAY_FILTER.DAY_ONLY
      ElseIf opt_birth_month_only.Checked Then
        promo_item.BirthdayFilter = CLASS_TITO_PROMOTION.ENUM_BIRTHDAY_FILTER.WHOLE_MONTH
      End If
    End If

    promo_item.LevelFilter = GetLevelFilter()

    If chk_by_freq.Checked Then
      promo_item.FreqFilterLastDays = GUI_ParseNumber(ef_freq_last_days.Value)
      promo_item.FreqFilterMinDays = GUI_ParseNumber(ef_freq_min_days.Value)
      promo_item.FreqFilterMinCashIn = GUI_ParseCurrency(ef_freq_min_cash_in.Value)
    Else
      promo_item.FreqFilterLastDays = 0
      promo_item.FreqFilterMinDays = 0
      promo_item.FreqFilterMinCashIn = 0
    End If

    'If gb_expiration.Enabled Then
    If pa_expiration.Enabled Then
      If opt_expiration_days.Checked Then
        promo_item.ExpirationType = CLASS_TITO_PROMOTION.ENUM_EXPIRATION_TYPE.DAYS
        promo_item.ExpirationValue = GUI_ParseNumber(ef_expiration_days.Value)
      Else
        promo_item.ExpirationType = CLASS_TITO_PROMOTION.ENUM_EXPIRATION_TYPE.HOURS
        promo_item.ExpirationValue = GUI_ParseNumber(ef_expiration_hours.Value)
      End If

      ' DHA 09-APR-2013: 'Expiration Limit' field added
      If chk_expiration_limit.Checked Then
        promo_item.ExpirationLimit = Me.dtp_expiration_limit.Value
      Else
        promo_item.ExpirationLimit = DateTime.MinValue
      End If

    End If

    promo_item.MinCashIn = GUI_ParseCurrency(ef_min_cash_in.Value)
    promo_item.MinCashInReward = GUI_ParseCurrency(ef_min_cash_in_reward.Value)

    promo_item.CashIn = GUI_ParseCurrency(ef_cash_in.Value)
    promo_item.CashInReward = GUI_ParseCurrency(ef_cash_in_reward.Value)

    promo_item.MinSpent = GUI_ParseCurrency(ef_min_spent.Value)
    promo_item.MinSpentReward = GUI_ParseCurrency(ef_min_spent_reward.Value)

    promo_item.Spent = GUI_ParseCurrency(ef_spent.Value)
    promo_item.SpentReward = GUI_ParseCurrency(ef_spent_reward.Value)

    ' Get the checked providers from the control and copy them in the promo_item.ProviderList.
    uc_provider_filter1.GetProvidersFromControl(promo_item.ProviderList)

    promo_item.NumTokens = GUI_ParseNumber(ef_num_tokens.Value)
    promo_item.TokenReward = GUI_ParseCurrency(ef_token_reward.Value)
    promo_item.TokenName = ef_token_name.Value

    promo_item.DailyLimit = GUI_ParseCurrency(ef_daily_limit.Value)
    promo_item.MonthlyLimit = GUI_ParseCurrency(ef_monthly_limit.Value)

    promo_item.GlobalDailyLimit = GUI_ParseCurrency(Me.ef_promotion_limit_day.Value)
    promo_item.GlobalMonthlyLimit = GUI_ParseCurrency(Me.ef_promotion_limit_month.Value)

    ' JAB 20-AUG-2012 Added Promotion Global Limit
    promo_item.GlobalLimit = GUI_ParseCurrency(Me.ef_promotion_limit_global.Value)

    If promo_item.Type = WSI.Common.Promotion.PROMOTION_TYPE.MANUAL Then
      If opt_credit_non_redeemable.Checked Then
        promo_item.CreditType = WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1
      ElseIf opt_credit_redeemable.Checked Then
        promo_item.CreditType = WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
      ElseIf opt_point.Checked Then
        promo_item.CreditType = WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.POINT
      Else
        promo_item.CreditType = WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1
      End If
    End If

    promo_item.WonLockEnabled = False
    promo_item.WonLock = 0
    If opt_credit_non_redeemable.Checked Then
      promo_item.WonLockEnabled = chk_won_lock.Checked
      promo_item.WonLock = GUI_ParseCurrency(ef_won_lock.Value)
    End If

    promo_item.Icon = img_icon.Image
    promo_item.Image = img_image.Image

    'HBB: get the ef_ticket_footer and set it to the ticket_footer property
    promo_item.TicketFooter = tb_ticket_footer.Text

    promo_item.CategoryId = cmb_categories.Value

    ' XCD 03-OCT-2012
    If chk_flags_awarded.Checked Then
      promo_item.FlagsAwarded = GetFlagsDatatableFromGrid(dg_flags_awarded)
    Else
      promo_item.FlagsAwarded.Clear()
    End If

    If chk_by_flag.Checked Then
      promo_item.FlagsRequired = GetFlagsDatatableFromGrid(dg_flags_required)
    Else
      promo_item.FlagsRequired.Clear()
    End If

    ' JMM 14-NOV-2012
    promo_item.VisibleOnPromoBOX = Me.chk_visible_on_PromoBOX.Checked

    'TITO
    promo_item.AllowTickets = Me.chk_allow_tickets_tito.Checked
    promo_item.TicketQuantity = Me.ef_quantity_tickets_tito.Value
    promo_item.GeneratedTickets = Me.ef_generated_tickets_tito.Value

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim promo_item As CLASS_TITO_PROMOTION

    promo_item = DbReadObject

    ef_promo_name.Value = promo_item.Name

    If promo_item.Enabled Then
      opt_enabled.Checked = True
      opt_disabled.Checked = False
    Else
      opt_enabled.Checked = False
      opt_disabled.Checked = True
    End If

    Select Case promo_item.SpecialPermission
      Case CLASS_TITO_PROMOTION.ENUM_SPECIAL_PERMISSION.NOT_SET
        chk_special_permission.Checked = False
        opt_special_permission_a.Enabled = False
        opt_special_permission_a.Checked = False
        opt_special_permission_b.Enabled = False
        opt_special_permission_b.Checked = False
        lbl_not_applicable_mb1.Visible = False

      Case CLASS_TITO_PROMOTION.ENUM_SPECIAL_PERMISSION.PERMISSION_A
        chk_special_permission.Checked = True
        opt_special_permission_a.Checked = True
        opt_special_permission_b.Checked = False
        lbl_not_applicable_mb1.Visible = m_mb_auto_promotion

      Case CLASS_TITO_PROMOTION.ENUM_SPECIAL_PERMISSION.PERMISSION_B
        chk_special_permission.Checked = True
        opt_special_permission_a.Checked = False
        opt_special_permission_b.Checked = True
        lbl_not_applicable_mb1.Visible = m_mb_auto_promotion

    End Select

    uc_promo_schedule.DateFrom = promo_item.DateStart
    uc_promo_schedule.DateTo = promo_item.DateFinish
    uc_promo_schedule.Weekday = promo_item.ScheduleWeekday
    uc_promo_schedule.TimeFrom = promo_item.Schedule1TimeFrom
    uc_promo_schedule.TimeTo = promo_item.Schedule1TimeTo
    uc_promo_schedule.SecondTime = promo_item.Schedule2Enabled
    uc_promo_schedule.SecondTimeFrom = promo_item.Schedule2TimeFrom
    uc_promo_schedule.SecondTimeTo = promo_item.Schedule2TimeTo

    ' Gender Filter
    Select Case promo_item.GenderFilter
      Case CLASS_TITO_PROMOTION.ENUM_GENDER_FILTER.NOT_SET
        chk_by_gender.Checked = False
        opt_gender_male.Enabled = False
        opt_gender_male.Checked = False
        opt_gender_female.Enabled = False
        opt_gender_female.Checked = False

      Case CLASS_TITO_PROMOTION.ENUM_GENDER_FILTER.MEN_ONLY
        chk_by_gender.Checked = True
        opt_gender_male.Checked = True
        opt_gender_female.Checked = False

      Case CLASS_TITO_PROMOTION.ENUM_GENDER_FILTER.WOMEN_ONLY
        chk_by_gender.Checked = True
        opt_gender_male.Checked = False
        opt_gender_female.Checked = True

    End Select

    ' Birthday Filter
    Select Case promo_item.BirthdayFilter
      Case CLASS_TITO_PROMOTION.ENUM_BIRTHDAY_FILTER.NOT_SET
        chk_by_birth_date.Checked = False
        opt_birth_date_only.Checked = False
        opt_birth_date_only.Enabled = False
        opt_birth_month_only.Checked = False
        opt_birth_month_only.Enabled = False

      Case CLASS_TITO_PROMOTION.ENUM_BIRTHDAY_FILTER.DAY_ONLY
        chk_by_birth_date.Checked = True
        opt_birth_date_only.Checked = True
        opt_birth_month_only.Checked = False

      Case CLASS_TITO_PROMOTION.ENUM_BIRTHDAY_FILTER.WHOLE_MONTH
        chk_by_birth_date.Checked = True
        opt_birth_date_only.Checked = False
        opt_birth_month_only.Checked = True

    End Select

    ' Level Filter
    SetLevelFilter(promo_item.LevelFilter)

    ' Frequency Filter
    If promo_item.FreqFilterLastDays = 0 Then
      chk_by_freq.Checked = False
      ef_freq_last_days.Enabled = False
      ef_freq_min_days.Enabled = False
      ef_freq_min_cash_in.Enabled = False
    Else
      chk_by_freq.Checked = True
      ef_freq_last_days.Enabled = True
      ef_freq_min_days.Enabled = True
      ef_freq_min_cash_in.Enabled = True

      ef_freq_last_days.Value = promo_item.FreqFilterLastDays
      ef_freq_min_days.Value = promo_item.FreqFilterMinDays
      ef_freq_min_cash_in.Value = promo_item.FreqFilterMinCashIn
    End If

    ' Select first activated tab, in order.
    tab_filters.SelectedIndex = 0
    If chk_by_gender.Checked = False Then
      If chk_by_birth_date.Checked = False Then
        If chk_by_level.Checked = False Then
          If chk_by_freq.Checked = True Then
            tab_filters.SelectedIndex = 3
          End If
        Else
          tab_filters.SelectedIndex = 2
        End If
      Else
        tab_filters.SelectedIndex = 1
      End If
    End If

    ' Expiration type and value
    Select Case promo_item.ExpirationType
      Case CLASS_TITO_PROMOTION.ENUM_EXPIRATION_TYPE.NOT_SET
        opt_expiration_days.Checked = False
        opt_expiration_hours.Checked = False
        ef_expiration_days.Enabled = False
        ef_expiration_hours.Enabled = False
        chk_expiration_limit.Enabled = False
        dtp_expiration_limit.Enabled = False
        Me.lbl_credits_non_redeemable.Enabled = False

      Case CLASS_TITO_PROMOTION.ENUM_EXPIRATION_TYPE.DAYS
        opt_expiration_days.Checked = True
        opt_expiration_hours.Checked = False
        If promo_item.ExpirationValue > 0 Then
          ef_expiration_days.Value = promo_item.ExpirationValue
        End If
        ef_expiration_hours.Enabled = False
        chk_expiration_limit.Enabled = True
        dtp_expiration_limit.Enabled = False
        Me.lbl_credits_non_redeemable.Enabled = True

      Case CLASS_TITO_PROMOTION.ENUM_EXPIRATION_TYPE.HOURS
        opt_expiration_days.Checked = False
        opt_expiration_hours.Checked = True
        If promo_item.ExpirationValue > 0 Then
          ef_expiration_hours.Value = promo_item.ExpirationValue
        End If
        ef_expiration_days.Enabled = False
        chk_expiration_limit.Enabled = True
        dtp_expiration_limit.Enabled = False
        Me.lbl_credits_non_redeemable.Enabled = True

    End Select

    If (Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW) Then
      opt_credit_non_redeemable.Checked = True
      WonLockEnable(False)
    End If

    If (Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT) Or (Me.m_promotion_copy_operation) Then
      If promo_item.MinCashIn > 0 Then
        ef_min_cash_in.Value = promo_item.MinCashIn
      End If
      If promo_item.MinCashInReward > 0 Then
        ef_min_cash_in_reward.Value = promo_item.MinCashInReward
      End If

      If promo_item.CashIn > 0 Then
        ef_cash_in.Value = promo_item.CashIn
      End If
      If promo_item.CashInReward > 0 Then
        ef_cash_in_reward.Value = promo_item.CashInReward
      End If

      If promo_item.MinSpent > 0 Then
        ef_min_spent.Value = promo_item.MinSpent
      End If
      If promo_item.MinSpentReward > 0 Then
        ef_min_spent_reward.Value = promo_item.MinSpentReward
      End If

      If promo_item.Spent > 0 Then
        ef_spent.Value = promo_item.Spent
      End If
      If promo_item.SpentReward > 0 Then
        ef_spent_reward.Value = promo_item.SpentReward
      End If

      If promo_item.NumTokens > 0 Then
        ef_num_tokens.Value = promo_item.NumTokens
      End If
      If promo_item.TokenReward > 0 Then
        ef_token_reward.Value = promo_item.TokenReward
      End If
      ef_token_name.Value = promo_item.TokenName

      If promo_item.DailyLimit > 0 Then
        ef_daily_limit.Value = promo_item.DailyLimit
      End If
      If promo_item.MonthlyLimit > 0 Then
        ef_monthly_limit.Value = promo_item.MonthlyLimit
      End If

      If promo_item.GlobalDailyLimit > 0 Then
        ef_promotion_limit_day.Value = promo_item.GlobalDailyLimit
      End If
      If promo_item.GlobalMonthlyLimit > 0 Then
        ef_promotion_limit_month.Value = promo_item.GlobalMonthlyLimit
      End If

      ' JAB 20-AUG-2012 Added Promotion Global Limit
      If promo_item.GlobalLimit > 0 Then
        ef_promotion_limit_global.Value = promo_item.GlobalLimit
      End If

      If promo_item.WonLock >= 0 Then
        chk_won_lock.Checked = promo_item.WonLockEnabled
        ef_won_lock.Value = promo_item.WonLock
        WonLockEnable(chk_won_lock.Checked)
      End If
    End If

    ' Set the provider filter control with the data from the promo_item.ProviderList.
    uc_provider_filter1.SetProviderList(promo_item.ProviderList)

    ' System-type promotions are protected
    If promo_item.Type <> WSI.Common.Promotion.PROMOTION_TYPE.MANUAL Then
      Call uc_promo_schedule.ProtectSystemPromotion()
      gb_credit_type.Enabled = False
      gb_enabled.Enabled = False
      gb_special_permission.Enabled = False
      tab_filters.Enabled = False
      gb_won_lock.Enabled = False
      gb_test.Enabled = False
      tabPromotions.Enabled = False
      tab_limit.Enabled = False
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    End If

    ' Initialize test fields
    ef_test_cash_in.Value = GUI_FormatCurrency(0)
    ef_test_num_tokens.Value = GUI_FormatNumber(0)

    img_icon.Image = promo_item.Icon
    img_image.Image = promo_item.Image

    Select Case promo_item.CreditType
      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR2, WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1
        opt_credit_non_redeemable.Checked = True
      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.POINT
        opt_point.Checked = True
      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
        opt_credit_redeemable.Checked = True
      Case Else
        opt_credit_non_redeemable.Checked = True
    End Select


    If opt_point.Checked Or opt_credit_redeemable.Checked Then
      pa_expiration.Enabled = False

    End If

    'HBB: Initialize ticket_footer field
    tb_ticket_footer.Text = promo_item.TicketFooter

    cmb_categories.Value = promo_item.CategoryId

    ' Initialize text labels that use values from entry fields
    Call RefreshLabelTexts()

    ' Init promo flags
    If promo_item.FlagsRequired.Rows.Count > 0 Then
      chk_by_flag.Checked = True
    Else
      chk_by_flag.Checked = False

    End If
    Call FillFlagsGrid(Me.dg_flags_required, promo_item.FlagsRequired)

    If promo_item.FlagsAwarded.Rows.Count > 0 Then
      chk_flags_awarded.Checked = True
    Else
      chk_flags_awarded.Checked = False
    End If

    Call FillFlagsGrid(Me.dg_flags_awarded, promo_item.FlagsAwarded)

    chk_visible_on_PromoBOX.Checked = promo_item.VisibleOnPromoBOX

    ' DHA 09-APR-2013: 'Expiration Limit' field added
    If promo_item.ExpirationLimit = DateTime.MinValue Then
      chk_expiration_limit.Checked = False
      dtp_expiration_limit.Value = promo_item.DateFinish
    Else
      chk_expiration_limit.Checked = True
      dtp_expiration_limit.Value = promo_item.ExpirationLimit
    End If

    'TITO
    Me.chk_allow_tickets_tito.Checked = promo_item.AllowTickets
    Me.ef_quantity_tickets_tito.Value = promo_item.TicketQuantity
    Me.ef_generated_tickets_tito.Value = promo_item.GeneratedTickets '  NumberOfGeneratedTickets(promo_item.PromotionId)

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim promo_class As CLASS_TITO_PROMOTION
    Dim nls_param1 As String
    Dim rc As mdl_NLS.ENUM_MB_RESULT
    Dim aux_date As DateTime

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_TITO_PROMOTION
        promo_class = DbEditedObject
        promo_class.PromotionId = 0
        promo_class.Enabled = True
        promo_class.SpecialPermission = CLASS_TITO_PROMOTION.ENUM_SPECIAL_PERMISSION.NOT_SET
        promo_class.CreditType = WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1
        promo_class.Type = WSI.Common.Promotion.PROMOTION_TYPE.MANUAL
        aux_date = Now.AddMonths(1)
        promo_class.DateStart = New DateTime(aux_date.Year, aux_date.Month, 1, GetDefaultClosingTime(), 0, 0)
        promo_class.DateFinish = promo_class.DateStart
        promo_class.DateFinish = promo_class.DateStart.AddMonths(1)
        promo_class.ScheduleWeekday = uc_promo_schedule.AllWeekDaysSelected()     ' All days
        promo_class.Schedule1TimeFrom = 0     ' 00:00:00
        promo_class.Schedule1TimeTo = 0       ' 00:00:00
        promo_class.Schedule2Enabled = False
        promo_class.Schedule2TimeFrom = 0     ' 00:00:00
        promo_class.Schedule2TimeTo = 0       ' 00:00:00
        promo_class.GenderFilter = CLASS_TITO_PROMOTION.ENUM_GENDER_FILTER.NOT_SET
        promo_class.BirthdayFilter = CLASS_TITO_PROMOTION.ENUM_BIRTHDAY_FILTER.NOT_SET
        promo_class.LevelFilter = 0
        promo_class.ExpirationType = CLASS_TITO_PROMOTION.ENUM_EXPIRATION_TYPE.DAYS
        promo_class.CategoryId = 0
        Select Case Me.ScreenMode
          Case ENUM_SCREEN_MODE.MODE_NEW
            promo_class.PromoScreenMode = CLASS_TITO_PROMOTION.ENUM_PROMO_SCREEN_MODE.MODE_NEW
          Case ENUM_SCREEN_MODE.MODE_EDIT
            promo_class.PromoScreenMode = CLASS_TITO_PROMOTION.ENUM_PROMO_SCREEN_MODE.MODE_EDIT
        End Select
        promo_class.VisibleOnPromoBOX = True
        DbStatus = ENUM_STATUS.STATUS_OK

        m_new_promo = True

        If Me.m_promotion_copied_id > 0 And Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
          If promo_class.DB_Read(Me.m_promotion_copied_id, 0) = ENUM_STATUS.STATUS_OK Then
            Me.m_promotion_copy_operation = True
            promo_class.PromotionId = 0
            promo_class.Name = String.Empty
            promo_class.ResetImages()
          End If
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          promo_class = Me.DbEditedObject
          nls_param1 = m_input_promo_name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(104), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)
        End If

        m_new_promo = False

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          promo_class = Me.DbEditedObject
          nls_param1 = promo_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(102), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)
          Call ef_promo_name.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          promo_class = Me.DbEditedObject
          nls_param1 = promo_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(105), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          promo_class = Me.DbEditedObject
          nls_param1 = promo_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(102), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)
          Call ef_promo_name.Focus()
        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          promo_class = Me.DbEditedObject
          nls_param1 = promo_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(106), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus = ENUM_STATUS.STATUS_DEPENDENCIES And DeleteOperation Then
          promo_class = Me.DbEditedObject
          nls_param1 = promo_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(103), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)

        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK And DeleteOperation Then
          promo_class = Me.DbEditedObject
          nls_param1 = promo_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(107), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          nls_param1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        DeleteOperation = False
        promo_class = Me.DbEditedObject
        nls_param1 = promo_class.Name
        rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(108), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        nls_param1)
        If rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          DeleteOperation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If DeleteOperation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Manage permissions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub ' GUI_Permissions

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_promo_name

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Init form in new mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowNewItem(Optional ByVal PromotionCopiedId As Long = 0)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    Me.m_promotion_copy_operation = False
    Me.m_promotion_copied_id = PromotionCopiedId

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '       - UserId
  '       - Username
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowEditItem(ByVal PromoId As Integer, ByVal PromoName As String)
    'Dim _promo_item As CLASS_TITO_PROMOTION

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.m_promotion_copy_operation = False
    Me.m_promotion_copied_id = 0

    Me.DbObjectId = PromoId
    Me.m_input_promo_name = PromoName

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus <> ENUM_STATUS.STATUS_OK Then

      Return
    End If

    Call Me.Display(True)

  End Sub ' ShowEditItem

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter)
        If Me.tb_ticket_footer.ContainsFocus Then

          Return True
        End If

        If Me.dg_flags_awarded.ContainsFocus() Then
          Return Me.dg_flags_awarded.KeyPressed(sender, e)
        End If

        If Me.dg_flags_required.ContainsFocus() Then
          Return Me.dg_flags_required.KeyPressed(sender, e)
        End If
      Case Chr(Keys.Escape)
        If Me.dg_flags_awarded.ContainsFocus() Then
          Return Me.dg_flags_awarded.KeyPressed(sender, e)
        End If

        If Me.dg_flags_required.ContainsFocus() Then
          Return Me.dg_flags_required.KeyPressed(sender, e)
        End If

      Case Else
        Return MyBase.GUI_KeyPress(sender, e)
    End Select

    ' The key Enter event is done in tb_ticket_footer_automatic_promotion

  End Function ' GUI_KeyPress

#End Region ' Overrides

#Region " Private "

  Private Sub SetLevelFilter(ByVal LevelFilter As Integer)
    Dim str_binary As String
    Dim bit_array As Char()

    chk_by_level.Checked = LevelFilter <> 0
    chk_by_level_CheckedChanged(Nothing, Nothing)

    str_binary = Convert.ToString(LevelFilter, 2)
    str_binary = New String("0", 5 - str_binary.Length) + str_binary

    bit_array = str_binary.ToCharArray()

    If (bit_array(4) = "0") Then chk_level_anonymous.Checked = False Else chk_level_anonymous.Checked = True ' Anonymous Player
    If (bit_array(3) = "0") Then chk_level_01.Checked = False Else chk_level_01.Checked = True ' Level 01
    If (bit_array(2) = "0") Then chk_level_02.Checked = False Else chk_level_02.Checked = True ' Level 02
    If (bit_array(1) = "0") Then chk_level_03.Checked = False Else chk_level_03.Checked = True ' Level 03
    If (bit_array(0) = "0") Then chk_level_04.Checked = False Else chk_level_04.Checked = True ' Level 04

  End Sub ' SetLevelFilter

  Private Function GetLevelFilter() As Integer
    Dim bit_array As Char()

    If Not chk_by_level.Checked Then
      Return 0
    End If

    bit_array = New String("00000").ToCharArray()


    If (Not opt_point.Checked) Then
      bit_array(4) = Convert.ToInt32(chk_level_anonymous.Checked).ToString()  ' Anonymous Player
    End If


    bit_array(3) = Convert.ToInt32(chk_level_01.Checked).ToString()  ' Level 01
    bit_array(2) = Convert.ToInt32(chk_level_02.Checked).ToString()  ' Level 02
    bit_array(1) = Convert.ToInt32(chk_level_03.Checked).ToString()  ' Level 03
    bit_array(0) = Convert.ToInt32(chk_level_04.Checked).ToString()  ' Level 04

    Return Convert.ToInt32(bit_array, 2)

  End Function ' GetLevelFilter

  Private Function CalculateReward(ByVal CashIn As Double, ByVal Spent As Double, ByVal NumTokens As Integer) As Double
    Dim accumulated_reward As Double

    accumulated_reward = 0

    ' Min Cash In reward
    If GUI_ParseCurrency(ef_min_cash_in_reward.Value) > 0 Then
      If CashIn >= GUI_ParseCurrency(ef_min_cash_in.Value) Then
        accumulated_reward = GUI_ParseCurrency(ef_min_cash_in_reward.Value)
      End If
    End If

    ' Cash In reward
    If GUI_ParseCurrency(ef_cash_in_reward.Value) > 0 And GUI_ParseCurrency(ef_cash_in.Value) > 0 Then
      If CashIn >= GUI_ParseCurrency(ef_cash_in.Value) And CashIn >= GUI_ParseCurrency(ef_min_cash_in.Value) Then
        accumulated_reward = accumulated_reward + (CashIn \ GUI_ParseCurrency(ef_cash_in.Value)) * GUI_ParseCurrency(ef_cash_in_reward.Value)
      End If
    End If

    ' Min Spent reward
    If GUI_ParseCurrency(ef_min_spent_reward.Value) > 0 Then
      If Spent >= GUI_ParseCurrency(ef_min_spent.Value) Then
        accumulated_reward = accumulated_reward + GUI_ParseCurrency(ef_min_spent_reward.Value)
      End If
    End If

    ' Spent reward
    If GUI_ParseCurrency(ef_spent_reward.Value) > 0 And GUI_ParseCurrency(ef_spent.Value) > 0 Then
      If Spent >= GUI_ParseCurrency(ef_spent.Value) And Spent >= GUI_ParseCurrency(ef_min_spent.Value) Then
        accumulated_reward = accumulated_reward + (Spent \ GUI_ParseCurrency(ef_spent.Value)) * GUI_ParseCurrency(ef_spent_reward.Value)
      End If
    End If

    ' Token reward
    If GUI_ParseCurrency(ef_token_reward.Value) > 0 And GUI_ParseNumber(ef_num_tokens.Value) > 0 Then
      If NumTokens >= GUI_ParseNumber(ef_num_tokens.Value) And CashIn >= GUI_ParseCurrency(ef_min_cash_in.Value) Then
        accumulated_reward = accumulated_reward + (NumTokens \ GUI_ParseNumber(ef_num_tokens.Value)) * GUI_ParseCurrency(ef_token_reward.Value)
      End If
    End If

    Return accumulated_reward
  End Function ' CalculateReward

  Private Sub RefreshLabelTexts()
    Dim empty_placeholder As String
    Dim freq_last_days As String
    Dim freq_min_days As String
    Dim freq_min_cash_in As String
    Dim str_promo_credit_type As String

    Dim _min_chash_in_reward As String
    Dim _cash_in_reward As String
    Dim _min_spent_reward As String
    Dim _spent_reward As String
    Dim _token_reward As String
    Dim _daily_limit As String
    Dim _monthly_limit As String
    Dim _promotion_limit_day As String
    Dim _promotion_limit_month As String
    Dim _promotion_limit_global As String

    empty_placeholder = "[ ]"

    If Me.opt_credit_redeemable.Checked Then
      str_promo_credit_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(518)
    ElseIf Me.opt_credit_non_redeemable.Checked Then
      str_promo_credit_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(517)
    ElseIf Me.opt_point.Checked Then
      str_promo_credit_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354).ToLower
    Else
      str_promo_credit_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(517)
    End If

    '
    ' Initialize Rewards vars
    '
    If Me.opt_point.Checked Then
      _min_chash_in_reward = GUI_FormatNumber(GUI_ParseCurrency(ef_min_cash_in_reward.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      _cash_in_reward = GUI_FormatNumber(GUI_ParseCurrency(ef_cash_in_reward.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      _min_spent_reward = GUI_FormatNumber(GUI_ParseCurrency(ef_min_spent_reward.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      _spent_reward = GUI_FormatNumber(GUI_ParseCurrency(ef_spent_reward.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      _token_reward = GUI_FormatNumber(GUI_ParseCurrency(ef_token_reward.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    Else
      _min_chash_in_reward = GUI_FormatCurrency(GUI_ParseCurrency(ef_min_cash_in_reward.Value))
      _cash_in_reward = GUI_FormatCurrency(GUI_ParseCurrency(ef_cash_in_reward.Value))
      _min_spent_reward = GUI_FormatCurrency(GUI_ParseCurrency(ef_min_spent_reward.Value))
      _spent_reward = GUI_FormatCurrency(GUI_ParseCurrency(ef_spent_reward.Value))
      _token_reward = GUI_FormatCurrency(GUI_ParseCurrency(ef_token_reward.Value))
    End If

    ' Initialize text labels that use values from entry fields
    If GUI_ParseCurrency(ef_min_cash_in_reward.Value) > 0 Or GUI_ParseCurrency(ef_min_cash_in.Value) > 0 Then
      lbl_min_cash_in_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(225, _min_chash_in_reward, _
                                                                              GUI_FormatCurrency(GUI_ParseCurrency(ef_min_cash_in.Value)), _
                                                                              str_promo_credit_type)
    Else
      lbl_min_cash_in_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(225, empty_placeholder, empty_placeholder, str_promo_credit_type)
    End If

    If GUI_ParseCurrency(ef_cash_in_reward.Value) > 0 Or GUI_ParseCurrency(ef_cash_in.Value) > 0 Then
      lbl_cash_in_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(227, _cash_in_reward, _
                                                                          GUI_FormatCurrency(GUI_ParseCurrency(ef_cash_in.Value)), _
                                                                          str_promo_credit_type)
    Else
      lbl_cash_in_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(227, empty_placeholder, empty_placeholder, str_promo_credit_type)
    End If

    If GUI_ParseCurrency(ef_min_spent_reward.Value) > 0 Or GUI_ParseCurrency(ef_min_spent.Value) > 0 Then
      lbl_min_spent_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(422, _min_spent_reward, _
                                                                            GUI_FormatCurrency(GUI_ParseCurrency(ef_min_spent.Value)), _
                                                                            str_promo_credit_type)
    Else
      lbl_min_spent_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(422, empty_placeholder, empty_placeholder, str_promo_credit_type)
    End If

    If GUI_ParseCurrency(ef_spent_reward.Value) > 0 Or GUI_ParseCurrency(ef_spent.Value) > 0 Then
      lbl_spent_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(423, _spent_reward, _
                                                                        GUI_FormatCurrency(GUI_ParseCurrency(ef_spent.Value)), _
                                                                        str_promo_credit_type)
    Else
      lbl_spent_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(423, empty_placeholder, empty_placeholder, str_promo_credit_type)
    End If

    If GUI_ParseCurrency(ef_token_reward.Value) > 0 Or GUI_ParseNumber(ef_num_tokens.Value) > 0 Then
      lbl_token_reward.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(229, _token_reward, _
                                                                     GUI_FormatNumber(GUI_ParseNumber(ef_num_tokens.Value), 0), _
                                                                     ef_token_name.Value, str_promo_credit_type)
    Else
      lbl_token_reward.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(229, empty_placeholder, empty_placeholder, _
                                                                     empty_placeholder, str_promo_credit_type)
    End If

    '
    'TAB CUSTOMER
    '

    If Me.opt_point.Checked Then
      _daily_limit = GUI_FormatNumber(GUI_ParseNumber(ef_daily_limit.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      _monthly_limit = GUI_FormatNumber(GUI_ParseNumber(ef_monthly_limit.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    Else
      _daily_limit = GUI_FormatCurrency(GUI_ParseCurrency(ef_daily_limit.Value))
      _monthly_limit = GUI_FormatCurrency(GUI_ParseCurrency(ef_monthly_limit.Value))
    End If

    If GUI_ParseCurrency(ef_daily_limit.Value) > 0 Then
      lbl_daily_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(231, _daily_limit, _
                                                                    str_promo_credit_type)
    Else
      lbl_daily_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(237)
    End If
    If GUI_ParseCurrency(ef_monthly_limit.Value) > 0 Then
      lbl_monthly_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(236, _monthly_limit, _
                                                                      str_promo_credit_type)
    Else
      lbl_monthly_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(238)
    End If

    '
    'TAB PROMOTION
    '
    If Me.opt_point.Checked Then
      _promotion_limit_day = GUI_FormatNumber(GUI_ParseNumber(ef_promotion_limit_day.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      _promotion_limit_month = GUI_FormatNumber(GUI_ParseNumber(ef_promotion_limit_month.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      _promotion_limit_global = GUI_FormatNumber(GUI_ParseNumber(ef_promotion_limit_global.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    Else
      _promotion_limit_day = GUI_FormatCurrency(GUI_ParseCurrency(ef_promotion_limit_day.Value))
      _promotion_limit_month = GUI_FormatCurrency(GUI_ParseCurrency(ef_promotion_limit_month.Value))
      _promotion_limit_global = GUI_FormatCurrency(GUI_ParseCurrency(ef_promotion_limit_global.Value))
    End If

    If GUI_ParseCurrency(ef_promotion_limit_day.Value) > 0 Then
      lbl_promotion_daily_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(525, _promotion_limit_day, str_promo_credit_type)
    Else
      lbl_promotion_daily_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(527)
    End If
    If GUI_ParseCurrency(ef_promotion_limit_month.Value) > 0 Then
      lbl_promotion_monthly_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(526, _promotion_limit_month, str_promo_credit_type)
    Else
      lbl_promotion_monthly_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(528)
    End If
    If GUI_ParseCurrency(ef_promotion_limit_global.Value) > 0 Then
      lbl_promotion_global_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1248, _promotion_limit_global, str_promo_credit_type)
    Else
      lbl_promotion_global_limit.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1249)
    End If

    '
    'WON LOCK
    '
    If chk_won_lock.Checked Then
      If GUI_ParseCurrency(ef_won_lock.Value) >= 0 Then
        lbl_won_lock.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(249, ef_won_lock.Value)
      Else
        lbl_won_lock.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(249, empty_placeholder)
      End If
    Else
      lbl_won_lock.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(249, empty_placeholder)
    End If

    '
    ' TEST
    '
    If Me.opt_point.Checked Then
      lbl_test_result.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(234, GUI_FormatNumber(CalculateReward(GUI_ParseCurrency(ef_test_cash_in.Value), _
                                                                    GUI_ParseCurrency(ef_test_spent.Value), _
                                                                    GUI_ParseNumber(ef_test_num_tokens.Value)), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE), _
                                                                    str_promo_credit_type)
    Else
      lbl_test_result.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(234, GUI_FormatCurrency(CalculateReward(GUI_ParseCurrency(ef_test_cash_in.Value), _
                                                                    GUI_ParseCurrency(ef_test_spent.Value), _
                                                                    GUI_ParseNumber(ef_test_num_tokens.Value))), _
                                                                    str_promo_credit_type)
    End If

    freq_last_days = empty_placeholder
    freq_min_days = empty_placeholder
    freq_min_cash_in = empty_placeholder
    If chk_by_freq.Checked Then
      If GUI_ParseNumber(ef_freq_last_days.Value) > 0 Then
        freq_last_days = GUI_FormatNumber(GUI_ParseNumber(ef_freq_last_days.Value), 0)
      End If
      If GUI_ParseNumber(ef_freq_min_days.Value) > 0 Then
        freq_min_days = GUI_FormatNumber(GUI_ParseNumber(ef_freq_min_days.Value), 0)
      End If
      If GUI_ParseNumber(ef_freq_min_cash_in.Value) > 0 Then
        freq_min_cash_in = GUI_FormatCurrency(GUI_ParseCurrency(ef_freq_min_cash_in.Value))
      End If
    End If
    lbl_freq_filter_01.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(412, freq_last_days, freq_min_days)
    lbl_freq_filter_02.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(413, freq_min_cash_in)

  End Sub ' RefreshLabelTexts

  Private Sub WonLockEnable(ByVal enabled As Boolean)

    ef_won_lock.Value = IIf(enabled, ef_won_lock.Value, "")
    ef_won_lock.Enabled = enabled

    ' MBF 01-OCT-2010 - Do not disable anything

    ' RCI 02-JUN-2011: Disable min cash-in and cash-in.
    ef_min_cash_in.Value = IIf(enabled, "", ef_min_cash_in.Value)
    ef_min_cash_in.Enabled = Not enabled

    ef_cash_in.Value = IIf(enabled, "", ef_cash_in.Value)
    ef_cash_in.Enabled = Not enabled

    ef_cash_in_reward.Value = IIf(enabled, "", ef_cash_in_reward.Value)
    ef_cash_in_reward.Enabled = Not enabled

    'ef_num_tokens.Value = IIf(enabled, "0", ef_num_tokens.Value)
    'ef_num_tokens.Enabled = Not enabled
    'ef_token_name.Value = IIf(enabled, "", ef_token_name.Value)
    'ef_token_name.Enabled = Not enabled
    'ef_token_reward.Value = IIf(enabled, "0", ef_token_reward.Value)
    'ef_token_reward.Enabled = Not enabled

  End Sub ' WonLockEnable

  Private Sub SwitchCredit(ByVal CreditType As WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE)

    Select Case CreditType
      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1, WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR2
        ef_min_cash_in_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_cash_in_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_min_spent_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_spent_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_token_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        If chk_by_level.Checked Then
          chk_level_anonymous.Enabled = Not Me.opt_point.Checked
        End If
        chk_won_lock.Enabled = True
        WonLockEnable(chk_won_lock.Checked)
        lbl_won_lock.Enabled = True
        pa_expiration.Enabled = True

      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.POINT
        ef_min_cash_in_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354)
        ef_cash_in_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354)
        ef_min_spent_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354)
        ef_spent_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354)
        ef_token_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354)
        If chk_by_level.Checked Then
          chk_level_anonymous.Enabled = Not Me.opt_point.Checked
        End If
        chk_won_lock.Checked = False
        chk_won_lock.Enabled = False
        WonLockEnable(False)
        lbl_won_lock.Enabled = False
        pa_expiration.Enabled = False

      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
        ef_min_cash_in_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
        ef_cash_in_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
        ef_min_spent_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
        ef_spent_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
        ef_token_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
        If chk_by_level.Checked Then
          chk_level_anonymous.Enabled = Not Me.opt_point.Checked
        End If
        chk_won_lock.Checked = False
        chk_won_lock.Enabled = False
        WonLockEnable(False)
        lbl_won_lock.Enabled = False
        pa_expiration.Enabled = False

      Case Else
        ef_min_cash_in_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_cash_in_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_min_spent_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_spent_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_token_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        chk_won_lock.Enabled = True
        WonLockEnable(chk_won_lock.Checked)
        lbl_won_lock.Enabled = True
        pa_expiration.Enabled = False

    End Select

    Dim ls_ef As List(Of GUI_Controls.uc_entry_field)
    Dim ls_ef_limits As List(Of GUI_Controls.uc_entry_field)

    Dim _value As Object

    _value = Nothing

    ls_ef_limits = New List(Of GUI_Controls.uc_entry_field)
    ls_ef = New List(Of GUI_Controls.uc_entry_field)

    ls_ef_limits.Add(ef_promotion_limit_month)
    ls_ef_limits.Add(ef_promotion_limit_day)
    ls_ef_limits.Add(ef_promotion_limit_global)
    ls_ef_limits.Add(ef_monthly_limit)
    ls_ef_limits.Add(ef_daily_limit)

    ls_ef.Add(ef_min_spent_reward)
    ls_ef.Add(ef_cash_in_reward)
    ls_ef.Add(ef_min_cash_in_reward)
    ls_ef.Add(ef_spent_reward)
    ls_ef.Add(ef_token_reward)

    Select Case CreditType

      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.POINT

        For Each _uc As uc_entry_field In ls_ef_limits
          _value = _uc.Value
          _uc.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 8)
          _uc.Value = _value
        Next
        For Each _uc As uc_entry_field In ls_ef
          _value = _uc.Value
          _uc.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5)
          _uc.Value = _value
        Next

      Case Else
        For Each _uc As uc_entry_field In ls_ef_limits
          _value = _uc.Value
          _uc.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8)
          _uc.Value = _value
        Next

        For Each _uc As uc_entry_field In ls_ef
          _value = _uc.Value
          _uc.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)
          _uc.Value = _value
        Next

    End Select

  End Sub

  ' XCD 2-OCT-2012
  Private Sub InitializeRequiredFlags()

    Call Me.dg_flags_required.Init(GRID_SELECT_FLAG_COLUMNS, GRID_SELECT_FLAG_HEADER_ROWS, True)

    ' FLAG_ID ( NOT VISIBLE )
    With Me.dg_flags_required.Column(GRID_FLAG_IDX)
      .Width = 0
      .IsColumnPrintable = False
    End With

    ' FLAG CHECKED
    With Me.dg_flags_required.Column(GRID_FLAG_CHECKED)
      .Header.Text = ""
      .WidthFixed = 300
      .Fixed = True
      .Editable = True
      .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .HighLightWhenSelected = False
    End With

    ' FLAG COLOR
    With Me.dg_flags_required.Column(GRID_FLAG_COLOR)
      .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) 'Bandera
      .WidthFixed = 200
      .HighLightWhenSelected = False
    End With

    ' COLUMN_FLAG_NAME
    With Me.dg_flags_required.Column(GRID_FLAG_NAME)
      .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) 'Bandera
      .Width = 3000
      .Editable = False
      .HighLightWhenSelected = False
    End With

    ' COLUMN_FLAG_COUNT
    With Me.dg_flags_required.Column(GRID_FLAG_COUNT)
      .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1416) 'Requeridas
      .WidthFixed = 1300
      .Fixed = True
      .Editable = True
      .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
      .EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, LEN_FLAG_REQUIRED_DIGITS)
    End With

  End Sub

  Private Sub InitializeAwardedFlags()

    Call Me.dg_flags_awarded.Init(GRID_SELECT_FLAG_COLUMNS, GRID_SELECT_FLAG_HEADER_ROWS, True)

    ' FLAG_ID ( NOT VISIBLE )
    With Me.dg_flags_awarded.Column(GRID_FLAG_IDX)
      .Width = 0
      .IsColumnPrintable = False
    End With

    ' FLAG CHECKED
    With Me.dg_flags_awarded.Column(GRID_FLAG_CHECKED)
      .Header.Text = ""
      .WidthFixed = 300
      .Fixed = True
      .Editable = True
      .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX
      .HighLightWhenSelected = False
    End With

    ' FLAG COLOR
    With Me.dg_flags_awarded.Column(GRID_FLAG_COLOR)
      .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) 'Bandera
      .WidthFixed = 200
      .HighLightWhenSelected = False
    End With

    ' COLUMN_FLAG_NAME
    With Me.dg_flags_awarded.Column(GRID_FLAG_NAME)
      .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1286) 'Bandera
      .Width = 3000
      .Editable = False
      .HighLightWhenSelected = False
    End With

    ' COLUMN_FLAG_COUNT
    With Me.dg_flags_awarded.Column(GRID_FLAG_COUNT)
      .Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1414) 'Otorgadas
      .WidthFixed = 1300
      .Fixed = True
      .Editable = True
      .EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_ENTRY_FIELD
      .EditionControl.EntryField.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, LEN_FLAG_AWARDED_DIGITS)
    End With

  End Sub

  ' PURPOSE: Get flag grid data and convert to datatable
  '
  '    - INPUT:
  '       - FlagGrid : grid to get flag data
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    - DataTable : Datatable with flags data
  Private Function GetFlagsDatatableFromGrid(ByRef FlagGrid As uc_grid) As DataTable
    Dim _flag_data As DataTable = New DataTable()
    Dim _flag_row As DataRow
    Dim _view As DataView
    Dim _row As Integer

    ' init datatable
    _flag_data.Columns.Add("PF_FLAG_ID", Type.GetType("System.Int64"))
    _flag_data.Columns.Add("PF_FLAG_COUNT", Type.GetType("System.Int32"))
    _flag_data.PrimaryKey = New DataColumn() {_flag_data.Columns("PF_FLAG_ID")}

    For _row = 0 To FlagGrid.NumRows() - 1
      If FlagGrid.Cell(_row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _flag_row = _flag_data.NewRow()
        _flag_row("PF_FLAG_ID") = FlagGrid.Cell(_row, GRID_FLAG_IDX).Value

        _flag_row("PF_FLAG_COUNT") = GUI_ParseNumber(FlagGrid.Cell(_row, GRID_FLAG_COUNT).Value)
        _flag_data.Rows.Add(_flag_row)
      End If
    Next

    ' Sort datable
    _view = _flag_data.DefaultView
    _view.Sort = "PF_FLAG_ID ASC"

    Return _view.ToTable()

  End Function

  ' PURPOSE: Fill grid with rows and data
  '
  '    - INPUT:
  '       - FlagGrid : grid to add data
  '       - FlagsData: flag count to be inserted
  '
  '    - OUTPUT:
  '
  Private Sub FillFlagsGrid(ByRef FlagsGrid As uc_grid, ByRef FlagsData As DataTable)

    Dim _idx_flags_data As Integer
    Dim _idx_all_flags As Integer
    Dim _all_flags As DataTable = Nothing
    Dim _view As DataView
    Dim _sql_tx As SqlClient.SqlTransaction = Nothing
    Dim _flag_color As Color

    ' Get all flags
    If Not GUI_BeginSQLTransaction(_sql_tx) Then
      FlagsGrid.Clear()
      Exit Sub
    Else
      CLASS_FLAG.GetAllFlags(_sql_tx, _all_flags)
      Call GUI_EndSQLTransaction(_sql_tx, False)
    End If

    ' Add flag count to all flags
    _all_flags.Columns.Add("PF_FLAG_COUNT", Type.GetType("System.Int32"))

    Dim _found As Boolean
    Dim _row As DataRow


    For _idx_flags_data = 0 To FlagsData.Rows.Count - 1
      _found = False
      For _idx_all_flags = 0 To _all_flags.Rows.Count - 1
        If FlagsData.Rows(_idx_flags_data).Item("PF_FLAG_ID") = _all_flags.Rows(_idx_all_flags).Item("FL_FLAG_ID") Then
          _all_flags.Rows(_idx_all_flags).Item("PF_FLAG_COUNT") = FlagsData.Rows(_idx_flags_data).Item("PF_FLAG_COUNT")
          _found = True
          Exit For
        End If
      Next
      If Not _found Then
        _row = _all_flags.NewRow()
        _row("FL_FLAG_ID") = FlagsData.Rows(_idx_flags_data).Item("PF_FLAG_ID")
        _row("PF_FLAG_COUNT") = FlagsData.Rows(_idx_flags_data).Item("PF_FLAG_COUNT")
        _row("FL_NAME") = "##-" + FlagsData.Rows(_idx_flags_data).Item("PF_FLAG_ID").ToString()
        _row("FL_COLOR") = -1
        _all_flags.Rows.Add(_row)
      End If
    Next

    ' Get active flags or flags with count > 0
    _view = _all_flags.DefaultView()
    ' free resources
    _all_flags.Dispose()

    _view.RowFilter = "      ( FL_STATUS = " & CLASS_FLAG.STATUS.ENABLED & _
                      " AND  ( FL_EXPIRATION_TYPE <> " & FLAG_EXPIRATION_TYPE.DATETIME & _
                      " OR   ( FL_EXPIRATION_TYPE = " & FLAG_EXPIRATION_TYPE.DATETIME & _
                      " AND FL_EXPIRATION_DATE > '" & GUI_FormatDate(GUI_GetDateTime(), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS) & "' )))" & _
                      " OR   ( PF_FLAG_COUNT > 0 ) "

    '_view.Sort = "CONVERT(PF_FLAG_COUNT,'System.Int32') DESC, FL_NAME"
    _view.Sort = "PF_FLAG_COUNT DESC, FL_NAME"

    ' Save filter flags
    _all_flags = _view.ToTable()
    ' free resources
    _view.Dispose()
    ' Remove irrelevant data
    _all_flags.Columns.Remove("FL_STATUS")
    _all_flags.Columns.Remove("FL_EXPIRATION_TYPE")
    _all_flags.Columns.Remove("FL_EXPIRATION_DATE")

    ' Change column name
    _all_flags.Columns("FL_FLAG_ID").ColumnName = "PF_FLAG_ID"

    ' Result - _all_flags datatable
    '-------------------------------------------------'
    ' PF_FLAG_ID | FL_NAME | FL_COLOR | PF_FLAG_COUNT '
    '-------------------------------------------------'

    ' Init Grid
    FlagsGrid.Clear()
    If (FlagsData.Rows.Count = 0) Then
      FlagsGrid.Settings.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
      FlagsGrid.Enabled = False
    Else
      FlagsGrid.Settings.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      FlagsGrid.Enabled = True
    End If

    For _idx_all_flags = 0 To _all_flags.Rows.Count - 1
      FlagsGrid.AddRow()

      ' Promotion_Flag_Id
      FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_IDX).Value = _all_flags.Rows(_idx_all_flags).Item("PF_FLAG_ID").ToString

      ' Flag Color
      _flag_color = Color.FromArgb(_all_flags.Rows(_idx_all_flags).Item("FL_COLOR"))
      ' CHAPUZA para pintar la celda de color negro
      If (_flag_color.A = Color.Black.A And _
          _flag_color.R = Color.Black.R And _
          _flag_color.G = Color.Black.G And _
          _flag_color.B = Color.Black.B) Then
        _flag_color = Color.FromArgb(_flag_color.A, _flag_color.R, _flag_color.G, _flag_color.B + 1)
      End If
      FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_COLOR).BackColor = _flag_color

      ' Flag Name
      FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_NAME).Value = _all_flags.Rows(_idx_all_flags).Item("FL_NAME").ToString

      'Flag Count
      If (String.IsNullOrEmpty(_all_flags.Rows(_idx_all_flags).Item("PF_FLAG_COUNT").ToString)) Then
        FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_COUNT).Value = ""
        If FlagsGrid.Enabled Then
          FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
        Else
          FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
        End If
      Else
        FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_COUNT).Value = GUI_FormatNumber(_all_flags.Rows(_idx_all_flags).Item("PF_FLAG_COUNT"), 0)
        If FlagsGrid.Enabled Then
          FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
        Else
          FlagsGrid.Cell(_idx_all_flags, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
        End If
      End If
    Next

  End Sub

  ' PURPOSE: Set grid behaviour
  '
  '    - INPUT:
  '       - FlagGrid : grid to be setted
  '       - IsChecked: status of drid ( active or no active)
  '
  '    - OUTPUT:
  '
  Private Sub SetFlagsGridStatus(ByRef FlagsGrid As uc_grid, ByVal IsChecked As Boolean)
    Dim _idx_row As Integer

    If IsChecked Then
      ' Current grid is active
      FlagsGrid.Enabled = True
      FlagsGrid.Settings.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

      For _idx_row = 0 To FlagsGrid.NumRows - 1
        ' Checked color
        Select Case FlagsGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value
          Case uc_grid.GRID_CHK_UNCHECKED_DISABLED
            FlagsGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
          Case uc_grid.GRID_CHK_CHECKED_DISABLED
            FlagsGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
        End Select
        ' Name and count color
        FlagsGrid.Cell(_idx_row, GRID_FLAG_NAME).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        FlagsGrid.Cell(_idx_row, GRID_FLAG_COUNT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        FlagsGrid.Cell(_idx_row, GRID_FLAG_COUNT).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      Next

    Else
      ' Current grid is inactive
      FlagsGrid.Enabled = False
      FlagsGrid.Settings.BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)

      For _idx_row = 0 To FlagsGrid.NumRows - 1
        ' Checked color
        Select Case FlagsGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value
          Case uc_grid.GRID_CHK_UNCHECKED
            FlagsGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED_DISABLED
          Case uc_grid.GRID_CHK_CHECKED
            FlagsGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED_DISABLED
        End Select
        ' Name and count color
        FlagsGrid.Cell(_idx_row, GRID_FLAG_NAME).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
        FlagsGrid.Cell(_idx_row, GRID_FLAG_COUNT).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_01)
        FlagsGrid.Cell(_idx_row, GRID_FLAG_COUNT).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      Next
    End If
  End Sub

  ' PURPOSE: Check if flag grid has correct values
  '
  '    - INPUT:
  '       - FlagGrid : grid to check
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '       - Grid Status (OK, VALUE_NOT_VALID or NO_ONE_CHECKED)
  '
  Private Function IsFlagGridDataOK(ByRef FlagGrid As uc_grid, Optional ByVal MaxFlags As Integer = -1) As Integer
    Dim _idx_row As Integer
    Dim _count_checked As Integer
    Dim _sum_count_flags As Integer

    _count_checked = 0 ' Number of rows checked
    _sum_count_flags = 0 ' Number of flags required/assigned

    For _idx_row = 0 To FlagGrid.NumRows() - 1

      If FlagGrid.Cell(_idx_row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _count_checked = _count_checked + 1

        If String.IsNullOrEmpty(FlagGrid.Cell(_idx_row, GRID_FLAG_COUNT).Value) Then
          ' Flag checked without a value
          FlagGrid.Redraw = False
          FlagGrid.ClearSelection()
          FlagGrid.IsSelected(_idx_row) = True
          FlagGrid.Redraw = True
          FlagGrid.Focus()
          Return GRID_DATA_STATUS.VALUE_NOT_VALID
        ElseIf GUI_ParseNumber(FlagGrid.Cell(_idx_row, GRID_FLAG_COUNT).Value) = 0 Then
          ' Flag checked with value 0
          FlagGrid.Redraw = False
          FlagGrid.ClearSelection()
          FlagGrid.IsSelected(_idx_row) = True
          FlagGrid.Redraw = True
          FlagGrid.Focus()
          Return GRID_DATA_STATUS.VALUE_NOT_VALID
        ElseIf MaxFlags <> -1 Then
          _sum_count_flags = _sum_count_flags + GUI_ParseNumber(FlagGrid.Cell(_idx_row, GRID_FLAG_COUNT).Value)
          If _sum_count_flags > MaxFlags Then
            ' overall number exceeded
            FlagGrid.Redraw = False
            FlagGrid.ClearSelection()
            FlagGrid.IsSelected(_idx_row) = True
            FlagGrid.Redraw = True
            FlagGrid.Focus()
            Return GRID_DATA_STATUS.MAX_FLAGS_EXCEEDED
          End If
        End If
      End If
    Next

    If _count_checked = 0 Then
      ' Active but without a flag selected
      FlagGrid.Redraw = False
      FlagGrid.ClearSelection()
      FlagGrid.SelectFirstRow(True)
      FlagGrid.Redraw = True
      Return GRID_DATA_STATUS.NO_ONE_CHECKED
    End If

    Return GRID_DATA_STATUS.OK
  End Function

  ' PURPOSE: Returns the number of tickets generated for this promotion
  '
  '    - INPUT:
  '     
  '    - OUTPUT:
  '
  ' RETURNS:
  '       - The number of tickets generated for this promotion
  '
  Private Function NumberOfGeneratedTickets(ByVal pPromotionID As Integer) As Integer

    Dim _sb As StringBuilder
    Dim _obj As Object

    NumberOfGeneratedTickets = 0

    _sb = New StringBuilder()
    Try
      Using _trx As New DB_TRX()
        _sb.AppendLine("SELECT   COUNT(*)                                     ")
        _sb.AppendLine("  FROM   TICKETS                                      ")
        _sb.AppendLine(" WHERE   TI_PROMOTION_ID = @pIDPromo       ")

        Using _cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(_sb.ToString(), _trx.SqlTransaction.Connection, _trx.SqlTransaction)
          _cmd.Parameters.Add("@pIDPromo", SqlDbType.NVarChar).Value = pPromotionID
          _obj = _cmd.ExecuteScalar()
          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            Integer.TryParse(CInt(_obj), NumberOfGeneratedTickets)
          End If

          Return NumberOfGeneratedTickets
        End Using
      End Using
    Catch _ex As Exception
      Return 0
    End Try

  End Function   'NumberOfGeneratedTickets

#End Region ' Private

#Region " Events "

  Private Sub chk_special_permission_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_special_permission.CheckedChanged
    opt_special_permission_a.Enabled = chk_special_permission.Checked
    opt_special_permission_b.Enabled = chk_special_permission.Checked

    If Not chk_special_permission.Checked Then
      lbl_not_applicable_mb1.Visible = False
    Else
      If opt_special_permission_a.Checked Or _
         opt_special_permission_b.Checked Then
        lbl_not_applicable_mb1.Visible = m_mb_auto_promotion
      End If
    End If
  End Sub

  Private Sub opt_special_permission_a_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_special_permission_a.CheckedChanged
    lbl_not_applicable_mb1.Visible = m_mb_auto_promotion
  End Sub

  Private Sub opt_special_permission_b_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_special_permission_b.CheckedChanged
    lbl_not_applicable_mb1.Visible = m_mb_auto_promotion
  End Sub

  Private Sub chk_by_gender_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_by_gender.CheckedChanged
    opt_gender_male.Enabled = chk_by_gender.Checked
    opt_gender_female.Enabled = chk_by_gender.Checked
  End Sub

  Private Sub chk_by_birth_date_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_by_birth_date.CheckedChanged
    opt_birth_date_only.Enabled = chk_by_birth_date.Checked
    opt_birth_month_only.Enabled = chk_by_birth_date.Checked
  End Sub

  Private Sub chk_by_level_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_by_level.CheckedChanged

    If chk_by_level.Checked Then
      chk_level_anonymous.Enabled = Not Me.opt_point.Checked
    Else
      chk_level_anonymous.Enabled = False
    End If

    chk_level_01.Enabled = chk_by_level.Checked
    chk_level_02.Enabled = chk_by_level.Checked
    chk_level_03.Enabled = chk_by_level.Checked
    chk_level_04.Enabled = chk_by_level.Checked
  End Sub

  Private Sub chk_by_freq_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_by_freq.CheckedChanged
    ef_freq_last_days.Enabled = chk_by_freq.Checked
    ef_freq_min_days.Enabled = chk_by_freq.Checked
    ef_freq_min_cash_in.Enabled = chk_by_freq.Checked
  End Sub

  Private Sub opt_expiration_days_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_expiration_days.CheckedChanged
    ef_expiration_days.Enabled = opt_expiration_days.Checked
  End Sub

  Private Sub opt_expiration_hours_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_expiration_hours.CheckedChanged
    ef_expiration_hours.Enabled = opt_expiration_hours.Checked
  End Sub

  ' PURPOSE: Timer operation to refresh the test area.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:

  Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
    Call RefreshLabelTexts()
  End Sub

  ' PURPOSE: Enables the text boxes related with the Won Lock
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:

  Private Sub chk_won_lock_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_won_lock.CheckedChanged

    WonLockEnable(chk_won_lock.Checked)

  End Sub

  Private Sub opt_credit_non_redeemable_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_credit_non_redeemable.CheckedChanged

    Call SwitchCredit(WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1)

  End Sub

  Private Sub opt_credit_redeemable_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_credit_redeemable.CheckedChanged

    Call SwitchCredit(WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE)

  End Sub

  Private Sub opt_point_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_point.CheckedChanged

    Call SwitchCredit(WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.POINT)

  End Sub

  Private Sub tabPromotions_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tabPromotions.SelectedIndexChanged
    uc_provider_filter1.Collapsed = True
    Select Case tabPromotions.SelectedIndex
      Case 0, 2
        uc_provider_filter1.Visible = False
      Case 1
        uc_provider_filter1.Visible = True
      Case Else
        uc_provider_filter1.Visible = False
    End Select
  End Sub

  Private Sub uc_provider_filter1_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uc_provider_filter1.Leave
    uc_provider_filter1.Collapsed = True
  End Sub

  Private Sub chk_flags_awarded_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_flags_awarded.CheckedChanged
    SetFlagsGridStatus(Me.dg_flags_awarded, chk_flags_awarded.Checked)
  End Sub

  Private Sub chk_by_flag_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_by_flag.CheckedChanged
    SetFlagsGridStatus(Me.dg_flags_required, chk_by_flag.Checked)
  End Sub

  ' PURPOSE: Set checked/unchecked when grid flag count change value
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub dg_flags_awarded_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_flags_awarded.CellDataChangedEvent

    If dg_flags_awarded.Redraw = False Then
      Return
    End If

    If Column = GRID_FLAG_COUNT Then
      If String.IsNullOrEmpty(dg_flags_awarded.Cell(Row, GRID_FLAG_COUNT).Value) Then
        dg_flags_awarded.Cell(Row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      Else
        dg_flags_awarded.Cell(Row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
      End If
    End If

  End Sub

  ' PURPOSE: Set checked/unchecked when grid flag count change value
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub dg_flags_required_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_flags_required.CellDataChangedEvent

    If dg_flags_required.Redraw = False Then
      Return
    End If

    If Column = GRID_FLAG_COUNT Then
      If String.IsNullOrEmpty(dg_flags_required.Cell(Row, GRID_FLAG_COUNT).Value) Then
        dg_flags_required.Cell(Row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
      Else
        dg_flags_required.Cell(Row, GRID_FLAG_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
      End If
    End If
  End Sub

  Private Sub chk_visible_on_PromoBOX_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_visible_on_PromoBOX.CheckedChanged

    Me.img_icon.Enabled = Me.chk_visible_on_PromoBOX.Checked
    Me.img_image.Enabled = Me.chk_visible_on_PromoBOX.Checked

  End Sub

  Private Sub chk_expiration_limit_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_expiration_limit.CheckedChanged
    dtp_expiration_limit.Enabled = chk_expiration_limit.Checked
  End Sub

#End Region ' Events

End Class
