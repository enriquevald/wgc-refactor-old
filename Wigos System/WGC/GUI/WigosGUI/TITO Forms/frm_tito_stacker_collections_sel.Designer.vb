<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_stacker_collections_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_provider = New GUI_Controls.uc_provider
    Me.gb_collections_status = New System.Windows.Forms.GroupBox
    Me.chk_open_with_activity = New System.Windows.Forms.CheckBox
    Me.lbl_open_with_activity = New System.Windows.Forms.Label
    Me.lbl_imbalance = New System.Windows.Forms.Label
    Me.chk_pending_collection = New System.Windows.Forms.CheckBox
    Me.chk_inserted_validated = New System.Windows.Forms.CheckBox
    Me.lbl_balanced = New System.Windows.Forms.Label
    Me.chk_collection_status_incorrect = New System.Windows.Forms.CheckBox
    Me.chk_collection_status_correct = New System.Windows.Forms.CheckBox
    Me.lbl_pending = New System.Windows.Forms.Label
    Me.lbl_open = New System.Windows.Forms.Label
    Me.ds_from_to = New GUI_Controls.uc_daily_session_selector
    Me.gb_user = New System.Windows.Forms.GroupBox
    Me.opt_one_employee = New System.Windows.Forms.RadioButton
    Me.opt_all_employee = New System.Windows.Forms.RadioButton
    Me.cmb_employee = New GUI_Controls.uc_combo
    Me.bt_check_all = New System.Windows.Forms.Button
    Me.dg_stackers = New GUI_Controls.uc_grid
    Me.bt_uncheck_all = New System.Windows.Forms.Button
    Me.gb_stacker = New System.Windows.Forms.GroupBox
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox
    Me.gb_group_by = New System.Windows.Forms.GroupBox
    Me.chk_show_detail = New System.Windows.Forms.CheckBox
    Me.opt_provider = New System.Windows.Forms.RadioButton
    Me.opt_group_by_day = New System.Windows.Forms.RadioButton
    Me.chk_group_by = New System.Windows.Forms.CheckBox
    Me.gb_date_filter = New System.Windows.Forms.GroupBox
    Me.opt_cage_session_datetime = New System.Windows.Forms.RadioButton
    Me.opt_collection_datetime = New System.Windows.Forms.RadioButton
    Me.opt_insertion_datetime = New System.Windows.Forms.RadioButton
    Me.opt_terminal = New System.Windows.Forms.RadioButton
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_collections_status.SuspendLayout()
    Me.gb_user.SuspendLayout()
    Me.gb_stacker.SuspendLayout()
    Me.gb_group_by.SuspendLayout()
    Me.gb_date_filter.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_date_filter)
    Me.panel_filter.Controls.Add(Me.gb_group_by)
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.gb_stacker)
    Me.panel_filter.Controls.Add(Me.gb_user)
    Me.panel_filter.Controls.Add(Me.uc_provider)
    Me.panel_filter.Controls.Add(Me.gb_collections_status)
    Me.panel_filter.Size = New System.Drawing.Size(1327, 301)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_collections_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_provider, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_user, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_stacker, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_group_by, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date_filter, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 305)
    Me.panel_data.Size = New System.Drawing.Size(1327, 413)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1321, 13)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1321, 4)
    '
    'uc_provider
    '
    Me.uc_provider.Location = New System.Drawing.Point(528, -1)
    Me.uc_provider.Name = "uc_provider"
    Me.uc_provider.Size = New System.Drawing.Size(328, 200)
    Me.uc_provider.TabIndex = 4
    '
    'gb_collections_status
    '
    Me.gb_collections_status.Controls.Add(Me.chk_open_with_activity)
    Me.gb_collections_status.Controls.Add(Me.lbl_open_with_activity)
    Me.gb_collections_status.Controls.Add(Me.lbl_imbalance)
    Me.gb_collections_status.Controls.Add(Me.chk_pending_collection)
    Me.gb_collections_status.Controls.Add(Me.chk_inserted_validated)
    Me.gb_collections_status.Controls.Add(Me.lbl_balanced)
    Me.gb_collections_status.Controls.Add(Me.chk_collection_status_incorrect)
    Me.gb_collections_status.Controls.Add(Me.chk_collection_status_correct)
    Me.gb_collections_status.Controls.Add(Me.lbl_pending)
    Me.gb_collections_status.Controls.Add(Me.lbl_open)
    Me.gb_collections_status.Location = New System.Drawing.Point(6, 163)
    Me.gb_collections_status.Name = "gb_collections_status"
    Me.gb_collections_status.Size = New System.Drawing.Size(266, 128)
    Me.gb_collections_status.TabIndex = 2
    Me.gb_collections_status.TabStop = False
    Me.gb_collections_status.Text = "xCollectionsState"
    '
    'chk_open_with_activity
    '
    Me.chk_open_with_activity.AutoSize = True
    Me.chk_open_with_activity.Location = New System.Drawing.Point(6, 40)
    Me.chk_open_with_activity.Name = "chk_open_with_activity"
    Me.chk_open_with_activity.Size = New System.Drawing.Size(130, 17)
    Me.chk_open_with_activity.TabIndex = 1
    Me.chk_open_with_activity.Text = "xOpenWithActivity"
    Me.chk_open_with_activity.UseVisualStyleBackColor = True
    '
    'lbl_open_with_activity
    '
    Me.lbl_open_with_activity.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
    Me.lbl_open_with_activity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_open_with_activity.ForeColor = System.Drawing.Color.Silver
    Me.lbl_open_with_activity.Location = New System.Drawing.Point(239, 38)
    Me.lbl_open_with_activity.Name = "lbl_open_with_activity"
    Me.lbl_open_with_activity.Size = New System.Drawing.Size(16, 16)
    Me.lbl_open_with_activity.TabIndex = 112
    '
    'lbl_imbalance
    '
    Me.lbl_imbalance.BackColor = System.Drawing.Color.Red
    Me.lbl_imbalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_imbalance.Location = New System.Drawing.Point(239, 102)
    Me.lbl_imbalance.Name = "lbl_imbalance"
    Me.lbl_imbalance.Size = New System.Drawing.Size(16, 16)
    Me.lbl_imbalance.TabIndex = 110
    '
    'chk_pending_collection
    '
    Me.chk_pending_collection.AutoSize = True
    Me.chk_pending_collection.Location = New System.Drawing.Point(6, 61)
    Me.chk_pending_collection.Name = "chk_pending_collection"
    Me.chk_pending_collection.Size = New System.Drawing.Size(134, 17)
    Me.chk_pending_collection.TabIndex = 2
    Me.chk_pending_collection.Text = "xPendingCollection"
    Me.chk_pending_collection.UseVisualStyleBackColor = True
    '
    'chk_inserted_validated
    '
    Me.chk_inserted_validated.AutoSize = True
    Me.chk_inserted_validated.Location = New System.Drawing.Point(6, 19)
    Me.chk_inserted_validated.Name = "chk_inserted_validated"
    Me.chk_inserted_validated.Size = New System.Drawing.Size(63, 17)
    Me.chk_inserted_validated.TabIndex = 0
    Me.chk_inserted_validated.Text = "xOpen"
    Me.chk_inserted_validated.UseVisualStyleBackColor = True
    '
    'lbl_balanced
    '
    Me.lbl_balanced.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
    Me.lbl_balanced.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_balanced.Location = New System.Drawing.Point(239, 81)
    Me.lbl_balanced.Name = "lbl_balanced"
    Me.lbl_balanced.Size = New System.Drawing.Size(16, 16)
    Me.lbl_balanced.TabIndex = 108
    '
    'chk_collection_status_incorrect
    '
    Me.chk_collection_status_incorrect.AutoSize = True
    Me.chk_collection_status_incorrect.Location = New System.Drawing.Point(6, 103)
    Me.chk_collection_status_incorrect.Name = "chk_collection_status_incorrect"
    Me.chk_collection_status_incorrect.Size = New System.Drawing.Size(85, 17)
    Me.chk_collection_status_incorrect.TabIndex = 4
    Me.chk_collection_status_incorrect.Text = "xIncorrect"
    Me.chk_collection_status_incorrect.UseVisualStyleBackColor = True
    '
    'chk_collection_status_correct
    '
    Me.chk_collection_status_correct.AutoSize = True
    Me.chk_collection_status_correct.Location = New System.Drawing.Point(6, 82)
    Me.chk_collection_status_correct.Name = "chk_collection_status_correct"
    Me.chk_collection_status_correct.Size = New System.Drawing.Size(76, 17)
    Me.chk_collection_status_correct.TabIndex = 3
    Me.chk_collection_status_correct.Text = "xCorrect"
    Me.chk_collection_status_correct.UseVisualStyleBackColor = True
    '
    'lbl_pending
    '
    Me.lbl_pending.BackColor = System.Drawing.Color.FromArgb(CType(CType(130, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(255, Byte), Integer))
    Me.lbl_pending.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_pending.Location = New System.Drawing.Point(239, 59)
    Me.lbl_pending.Name = "lbl_pending"
    Me.lbl_pending.Size = New System.Drawing.Size(16, 16)
    Me.lbl_pending.TabIndex = 106
    '
    'lbl_open
    '
    Me.lbl_open.BackColor = System.Drawing.Color.White
    Me.lbl_open.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_open.Location = New System.Drawing.Point(239, 16)
    Me.lbl_open.Name = "lbl_open"
    Me.lbl_open.Size = New System.Drawing.Size(16, 16)
    Me.lbl_open.TabIndex = 105
    '
    'ds_from_to
    '
    Me.ds_from_to.ClosingTime = 0
    Me.ds_from_to.ClosingTimeEnabled = False
    Me.ds_from_to.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.ds_from_to.FromDateSelected = True
    Me.ds_from_to.Location = New System.Drawing.Point(3, 71)
    Me.ds_from_to.Name = "ds_from_to"
    Me.ds_from_to.ShowBorder = False
    Me.ds_from_to.Size = New System.Drawing.Size(254, 76)
    Me.ds_from_to.TabIndex = 0
    Me.ds_from_to.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.ds_from_to.ToDateSelected = True
    '
    'gb_user
    '
    Me.gb_user.Controls.Add(Me.opt_one_employee)
    Me.gb_user.Controls.Add(Me.opt_all_employee)
    Me.gb_user.Controls.Add(Me.cmb_employee)
    Me.gb_user.Location = New System.Drawing.Point(531, 180)
    Me.gb_user.Name = "gb_user"
    Me.gb_user.Size = New System.Drawing.Size(323, 71)
    Me.gb_user.TabIndex = 5
    Me.gb_user.TabStop = False
    Me.gb_user.Text = "xUser"
    '
    'opt_one_employee
    '
    Me.opt_one_employee.Location = New System.Drawing.Point(6, 17)
    Me.opt_one_employee.Name = "opt_one_employee"
    Me.opt_one_employee.Size = New System.Drawing.Size(72, 24)
    Me.opt_one_employee.TabIndex = 0
    Me.opt_one_employee.Text = "xOne"
    '
    'opt_all_employee
    '
    Me.opt_all_employee.Location = New System.Drawing.Point(6, 44)
    Me.opt_all_employee.Name = "opt_all_employee"
    Me.opt_all_employee.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_employee.TabIndex = 2
    Me.opt_all_employee.Text = "xAll"
    '
    'cmb_employee
    '
    Me.cmb_employee.AllowUnlistedValues = False
    Me.cmb_employee.AutoCompleteMode = False
    Me.cmb_employee.IsReadOnly = False
    Me.cmb_employee.Location = New System.Drawing.Point(84, 16)
    Me.cmb_employee.Name = "cmb_employee"
    Me.cmb_employee.SelectedIndex = -1
    Me.cmb_employee.Size = New System.Drawing.Size(235, 24)
    Me.cmb_employee.SufixText = "Sufix Text"
    Me.cmb_employee.SufixTextVisible = True
    Me.cmb_employee.TabIndex = 1
    Me.cmb_employee.TextVisible = False
    Me.cmb_employee.TextWidth = 0
    '
    'bt_check_all
    '
    Me.bt_check_all.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.bt_check_all.Location = New System.Drawing.Point(8, 243)
    Me.bt_check_all.Name = "bt_check_all"
    Me.bt_check_all.Size = New System.Drawing.Size(100, 29)
    Me.bt_check_all.TabIndex = 0
    Me.bt_check_all.Text = "xMarcar Todas"
    Me.bt_check_all.UseVisualStyleBackColor = True
    '
    'dg_stackers
    '
    Me.dg_stackers.CurrentCol = -1
    Me.dg_stackers.CurrentRow = -1
    Me.dg_stackers.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_stackers.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_stackers.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_stackers.Location = New System.Drawing.Point(8, 20)
    Me.dg_stackers.Name = "dg_stackers"
    Me.dg_stackers.PanelRightVisible = True
    Me.dg_stackers.Redraw = True
    Me.dg_stackers.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_stackers.Size = New System.Drawing.Size(223, 217)
    Me.dg_stackers.Sortable = False
    Me.dg_stackers.SortAscending = True
    Me.dg_stackers.SortByCol = 0
    Me.dg_stackers.TabIndex = 0
    Me.dg_stackers.ToolTipped = True
    Me.dg_stackers.TopRow = -2
    '
    'bt_uncheck_all
    '
    Me.bt_uncheck_all.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.bt_uncheck_all.Location = New System.Drawing.Point(114, 243)
    Me.bt_uncheck_all.Name = "bt_uncheck_all"
    Me.bt_uncheck_all.Size = New System.Drawing.Size(115, 29)
    Me.bt_uncheck_all.TabIndex = 1
    Me.bt_uncheck_all.Text = "xDesmarcar todas"
    Me.bt_uncheck_all.UseVisualStyleBackColor = True
    '
    'gb_stacker
    '
    Me.gb_stacker.Controls.Add(Me.bt_uncheck_all)
    Me.gb_stacker.Controls.Add(Me.bt_check_all)
    Me.gb_stacker.Controls.Add(Me.dg_stackers)
    Me.gb_stacker.Location = New System.Drawing.Point(278, 3)
    Me.gb_stacker.Name = "gb_stacker"
    Me.gb_stacker.Size = New System.Drawing.Size(245, 289)
    Me.gb_stacker.TabIndex = 3
    Me.gb_stacker.TabStop = False
    Me.gb_stacker.Text = "xstackers"
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.ImeMode = System.Windows.Forms.ImeMode.NoControl
    Me.chk_terminal_location.Location = New System.Drawing.Point(868, 8)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(250, 19)
    Me.chk_terminal_location.TabIndex = 6
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'gb_group_by
    '
    Me.gb_group_by.Controls.Add(Me.opt_terminal)
    Me.gb_group_by.Controls.Add(Me.chk_show_detail)
    Me.gb_group_by.Controls.Add(Me.opt_provider)
    Me.gb_group_by.Controls.Add(Me.opt_group_by_day)
    Me.gb_group_by.Controls.Add(Me.chk_group_by)
    Me.gb_group_by.Location = New System.Drawing.Point(868, 31)
    Me.gb_group_by.Name = "gb_group_by"
    Me.gb_group_by.Size = New System.Drawing.Size(234, 149)
    Me.gb_group_by.TabIndex = 7
    Me.gb_group_by.TabStop = False
    Me.gb_group_by.Text = "xOptions"
    '
    'chk_show_detail
    '
    Me.chk_show_detail.AutoSize = True
    Me.chk_show_detail.Location = New System.Drawing.Point(21, 121)
    Me.chk_show_detail.Name = "chk_show_detail"
    Me.chk_show_detail.Size = New System.Drawing.Size(98, 17)
    Me.chk_show_detail.TabIndex = 3
    Me.chk_show_detail.Text = "Show details"
    Me.chk_show_detail.UseVisualStyleBackColor = True
    '
    'opt_provider
    '
    Me.opt_provider.AutoSize = True
    Me.opt_provider.Location = New System.Drawing.Point(21, 69)
    Me.opt_provider.Name = "opt_provider"
    Me.opt_provider.Size = New System.Drawing.Size(80, 17)
    Me.opt_provider.TabIndex = 2
    Me.opt_provider.TabStop = True
    Me.opt_provider.Text = "xProvider"
    Me.opt_provider.UseVisualStyleBackColor = True
    '
    'opt_group_by_day
    '
    Me.opt_group_by_day.AutoSize = True
    Me.opt_group_by_day.Location = New System.Drawing.Point(21, 43)
    Me.opt_group_by_day.Name = "opt_group_by_day"
    Me.opt_group_by_day.Size = New System.Drawing.Size(108, 17)
    Me.opt_group_by_day.TabIndex = 1
    Me.opt_group_by_day.TabStop = True
    Me.opt_group_by_day.Text = "xCreationDate"
    Me.opt_group_by_day.UseVisualStyleBackColor = True
    '
    'chk_group_by
    '
    Me.chk_group_by.AutoSize = True
    Me.chk_group_by.Location = New System.Drawing.Point(6, 20)
    Me.chk_group_by.Name = "chk_group_by"
    Me.chk_group_by.Size = New System.Drawing.Size(83, 17)
    Me.chk_group_by.TabIndex = 0
    Me.chk_group_by.Text = "xGroupBy"
    Me.chk_group_by.UseVisualStyleBackColor = True
    '
    'gb_date_filter
    '
    Me.gb_date_filter.Controls.Add(Me.opt_cage_session_datetime)
    Me.gb_date_filter.Controls.Add(Me.opt_collection_datetime)
    Me.gb_date_filter.Controls.Add(Me.opt_insertion_datetime)
    Me.gb_date_filter.Controls.Add(Me.ds_from_to)
    Me.gb_date_filter.Location = New System.Drawing.Point(6, 3)
    Me.gb_date_filter.Name = "gb_date_filter"
    Me.gb_date_filter.Size = New System.Drawing.Size(266, 154)
    Me.gb_date_filter.TabIndex = 12
    Me.gb_date_filter.TabStop = False
    Me.gb_date_filter.Text = "xFiltro fecha"
    '
    'opt_cage_session_datetime
    '
    Me.opt_cage_session_datetime.AutoSize = True
    Me.opt_cage_session_datetime.Location = New System.Drawing.Point(6, 65)
    Me.opt_cage_session_datetime.Name = "opt_cage_session_datetime"
    Me.opt_cage_session_datetime.Size = New System.Drawing.Size(106, 17)
    Me.opt_cage_session_datetime.TabIndex = 6
    Me.opt_cage_session_datetime.TabStop = True
    Me.opt_cage_session_datetime.Text = "xCageSession"
    Me.opt_cage_session_datetime.UseVisualStyleBackColor = True
    '
    'opt_collection_datetime
    '
    Me.opt_collection_datetime.AutoSize = True
    Me.opt_collection_datetime.Location = New System.Drawing.Point(6, 42)
    Me.opt_collection_datetime.Name = "opt_collection_datetime"
    Me.opt_collection_datetime.Size = New System.Drawing.Size(88, 17)
    Me.opt_collection_datetime.TabIndex = 5
    Me.opt_collection_datetime.TabStop = True
    Me.opt_collection_datetime.Text = "xCollection"
    Me.opt_collection_datetime.UseVisualStyleBackColor = True
    '
    'opt_insertion_datetime
    '
    Me.opt_insertion_datetime.AutoSize = True
    Me.opt_insertion_datetime.Location = New System.Drawing.Point(6, 19)
    Me.opt_insertion_datetime.Name = "opt_insertion_datetime"
    Me.opt_insertion_datetime.Size = New System.Drawing.Size(83, 17)
    Me.opt_insertion_datetime.TabIndex = 4
    Me.opt_insertion_datetime.TabStop = True
    Me.opt_insertion_datetime.Text = "xInsertion"
    Me.opt_insertion_datetime.UseVisualStyleBackColor = True
    '
    'opt_terminal
    '
    Me.opt_terminal.AutoSize = True
    Me.opt_terminal.Location = New System.Drawing.Point(21, 95)
    Me.opt_terminal.Name = "opt_terminal"
    Me.opt_terminal.Size = New System.Drawing.Size(82, 17)
    Me.opt_terminal.TabIndex = 4
    Me.opt_terminal.TabStop = True
    Me.opt_terminal.Text = "xTerminal"
    Me.opt_terminal.UseVisualStyleBackColor = True
    '
    'frm_stacker_collections_sel
    '
    Me.ClientSize = New System.Drawing.Size(1335, 722)
    Me.Name = "frm_stacker_collections_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_collections_status.ResumeLayout(False)
    Me.gb_collections_status.PerformLayout()
    Me.gb_user.ResumeLayout(False)
    Me.gb_stacker.ResumeLayout(False)
    Me.gb_group_by.ResumeLayout(False)
    Me.gb_group_by.PerformLayout()
    Me.gb_date_filter.ResumeLayout(False)
    Me.gb_date_filter.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_provider As GUI_Controls.uc_provider
  Friend WithEvents gb_collections_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_collection_status_incorrect As System.Windows.Forms.CheckBox
  Friend WithEvents chk_collection_status_correct As System.Windows.Forms.CheckBox
  Friend WithEvents ds_from_to As GUI_Controls.uc_daily_session_selector
  Friend WithEvents gb_user As System.Windows.Forms.GroupBox
  Friend WithEvents opt_one_employee As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_employee As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_employee As GUI_Controls.uc_combo
  Friend WithEvents chk_inserted_validated As System.Windows.Forms.CheckBox
  Friend WithEvents chk_pending_collection As System.Windows.Forms.CheckBox
  Friend WithEvents gb_stacker As System.Windows.Forms.GroupBox
  Friend WithEvents bt_uncheck_all As System.Windows.Forms.Button
  Friend WithEvents bt_check_all As System.Windows.Forms.Button
  Friend WithEvents dg_stackers As GUI_Controls.uc_grid
  Friend WithEvents lbl_imbalance As System.Windows.Forms.Label
  Friend WithEvents lbl_balanced As System.Windows.Forms.Label
  Friend WithEvents lbl_pending As System.Windows.Forms.Label
  Friend WithEvents lbl_open As System.Windows.Forms.Label
  Friend WithEvents chk_open_with_activity As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_open_with_activity As System.Windows.Forms.Label
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents gb_group_by As System.Windows.Forms.GroupBox
  Friend WithEvents opt_provider As System.Windows.Forms.RadioButton
  Friend WithEvents opt_group_by_day As System.Windows.Forms.RadioButton
  Friend WithEvents chk_group_by As System.Windows.Forms.CheckBox
  Friend WithEvents chk_show_detail As System.Windows.Forms.CheckBox
  Friend WithEvents gb_date_filter As System.Windows.Forms.GroupBox
  Friend WithEvents opt_cage_session_datetime As System.Windows.Forms.RadioButton
  Friend WithEvents opt_collection_datetime As System.Windows.Forms.RadioButton
  Friend WithEvents opt_insertion_datetime As System.Windows.Forms.RadioButton
  Friend WithEvents opt_terminal As System.Windows.Forms.RadioButton
End Class
