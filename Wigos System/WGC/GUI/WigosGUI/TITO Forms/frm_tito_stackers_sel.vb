'--------------------------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'--------------------------------------------------------------------------------------
'
'   MODULE NAME :  frm_tito_stackers_sel
'
'   DESCRIPTION :  Stackers browser
'
'        AUTHOR :  Nelson Madrigal Reyes
'
' CREATION DATE :  21-JUN-2013
'
' REVISION HISTORY
'
' Date         Author Description
' -----------  ------ ------------------------------------------------------------------
' 21-JUN-2013  NMR    Initial version
' 23-AUG-2013  JRM    Code revision and refactoring
' 02-DEC-2013  AMF    Reorder form
' 18-DEC-2013  ACM    Fixed Bug WIGOSTITO-864: Set only Sas-Host terminals to terminals filter control.
' 09-JAN-2014  DRV    Removed unused columns
' 14-FEB-2014  ICS    Added group by terminal checkbox and subtotals for each terminal
' 19-FEB-2014  ICS    Fixed Bug WIGOSTITO-1081: Unhandled exception when selecting a subtotals row
' 28-MAR-2014  LEM    Fixed Bug WIGOSTITO-1178: Not the default value for all filters is set by the Reset button
' 01-ABR-2014  DRV    Fixed Bug WIGOSTITO-1182: "Without Terminal" filter doesn't filter correctly  && Code refactoring
' 11-APR-2014  LEM    Fixed Bug WIGOSTITO-1191: Machine filter is not enabled for all status
' 27-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
' 15-MAR-2016  FAV    Fixed Bug 9552: Validate ST_INSERTED to checks if the Stacker is assigned
'---------------------------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports System.Text
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common

Public Class frm_tito_stackers_sel
  Inherits GUI_Controls.frm_base_sel

#Region " Members "

  ' al introducir datos en la tabla, se insertar� un prefijo num�rico para conservar el orden
  ' ese prefijo se deber� quitar al momento de enviarlo a las rutinas de impresi�n
  Dim m_filter_params As New Hashtable()
  Private m_group_by_terminal As String

  Private m_terminals As String
  Public StackerIdSelected As Int64

#End Region ' Members

#Region " Constants "

  ' Grid configuration
  Private Const GRID_COLUMNS_COUNT As Integer = 7
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Grid Columns
  Private Const GRID_COLUMN_SELECT As Integer = 0
  Private Const GRID_COLUMN_ID As Integer = 1
  Private Const GRID_COLUMN_MODEL As Integer = 2
  Private Const GRID_COLUMN_STACKER_STATUS As Integer = 3
  Private Const GRID_COLUMN_INSERTED_NAME As Integer = 4
  Private Const GRID_COLUMN_PREASSIGNED_NAME As Integer = 5
  Private Const GRID_COLUMN_STACKER_INSERTED As Integer = 6

  ' QUERY columns
  Private Const SQL_COLUMN_ID As Integer = 0
  Private Const SQL_COLUMN_MODEL As Integer = 1
  Private Const SQL_COLUMN_STATUS As Integer = 2
  Private Const SQL_COLUMN_PREASSIGNED_NAME As Integer = 3
  Private Const SQL_COLUMN_INSERTED_NAME As Integer = 4
  Private Const SQL_COLUMN_MC_STATUS As Integer = 5
  Private Const SQL_COLUMN_STACKER_INSERTED As Integer = 6

  ' Columns Width
  Private Const GRID_COLUMN_WIDTH_SELECT As Integer = 200
  Private Const GRID_COLUMN_WIDTH_ID As Integer = 1100
  Private Const GRID_COLUMN_WIDTH_MODEL As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_STATUS As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_MC_STATUS As Integer = 0
  Private Const GRID_COLUMN_WIDTH_PREASSIGNED As Integer = 3000
  Private Const GRID_COLUMN_WIDTH_INSERTED As Integer = 3000
  Private Const GRID_COLUMN_WIDTH_STACKER_INSERTED As Integer = 0

#End Region ' Constants

#Region " Private Functions and Methods "

  ' PURPOSE : Define layout of all Main Grid Columns
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS_COUNT, GRID_HEADER_ROWS)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = False

      .Column(GRID_COLUMN_SELECT).Header(0).Text = ""
      .Column(GRID_COLUMN_SELECT).Width = GRID_COLUMN_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False
      .Column(GRID_COLUMN_SELECT).HighLightWhenSelected = False

      .Column(GRID_COLUMN_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1407)
      .Column(GRID_COLUMN_ID).Width = GRID_COLUMN_WIDTH_ID

      .Column(GRID_COLUMN_MODEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2166)
      .Column(GRID_COLUMN_MODEL).Width = GRID_COLUMN_WIDTH_MODEL
      .Column(GRID_COLUMN_MODEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_PREASSIGNED_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2165)
      .Column(GRID_COLUMN_PREASSIGNED_NAME).Width = GRID_COLUMN_WIDTH_PREASSIGNED

      .Column(GRID_COLUMN_INSERTED_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2164)
      .Column(GRID_COLUMN_INSERTED_NAME).Width = GRID_COLUMN_WIDTH_INSERTED

      .Column(GRID_COLUMN_STACKER_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)
      .Column(GRID_COLUMN_STACKER_STATUS).Width = GRID_COLUMN_WIDTH_STATUS
      .Column(GRID_COLUMN_STACKER_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_STACKER_INSERTED).Header(0).Text = ""
      .Column(GRID_COLUMN_STACKER_INSERTED).Width = GRID_COLUMN_WIDTH_STACKER_INSERTED
    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE : Concatenation of condition for stacker searching
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - Nullable String
  '
  Private Function GetSqlWhere() As String

    Dim _str_filter As String
    Dim _filter_parts As List(Of String)
    Dim _filter_status_stacker As String
    Dim _terminals_where As String
    Dim _none_selected_status As Boolean

    _none_selected_status = True

    ' edit controls
    _filter_status_stacker = String.Empty
    _filter_parts = New List(Of String)
    _filter_parts.Add("(")

    If ef_stacker_indentifier.Value <> "" Then
      _filter_parts.Add("ST.ST_STACKER_ID = " & Me.ef_stacker_indentifier.Value)
    End If

    If Me.ef_stacker_model.Value <> "" Then
      If _filter_parts.Count > 1 Then
        _filter_parts.Add(" AND ")
      End If
      _filter_parts.Add("ST.ST_MODEL LIKE '%" & Me.ef_stacker_model.Value.Replace("'", "''") & "%'")
    End If

    _filter_parts.Add(")")

    _str_filter = ""

    If _filter_parts.Count > 2 Then
      _str_filter &= String.Join(" ", _filter_parts.ToArray())
    End If

    ' Checks for stacker status
    _filter_parts.Clear()

    '           Stacker Status  / MC Status
    ' Avaible:  ACTIVE          / NULL
    ' Inserted: ACTIVE          / OPEN
    ' Pending:  ACTIVE          / PENDING
    ' Disable:  DISABLED        / NULL
    '           DEACTIVATED     / NULL

    _terminals_where = GetFilterForTerminal()

    If chk_available.Checked Then
      _none_selected_status = False
      If Not String.IsNullOrEmpty(_filter_status_stacker) Then
        _filter_status_stacker &= " OR "
      End If
      _filter_status_stacker &= " ST.ST_STATUS = " & TITO_STACKER_STATUS.ACTIVE
      _filter_status_stacker &= " AND ST.ST_INSERTED IS NULL "
      '_terminals_where &= " AND "
    End If

    If chk_inserted.Checked Then
      _none_selected_status = False
      If Not String.IsNullOrEmpty(_filter_status_stacker) Then
        _filter_status_stacker &= " OR "
      End If
      _filter_status_stacker &= " (ST.ST_STATUS = " & TITO_STACKER_STATUS.ACTIVE
      _filter_status_stacker &= " AND NOT ST.ST_INSERTED IS NULL "   ' " AND MC_STATUS = " & TITO_MONEY_COLLECTION_STATUS.OPEN
      _filter_status_stacker &= IIf(String.IsNullOrEmpty(_terminals_where), "", " AND " & _terminals_where)
      _filter_status_stacker &= " ) "
    End If

    If chk_collection_pending.Checked Then
      _none_selected_status = False
      If Not String.IsNullOrEmpty(_filter_status_stacker) Then
        _filter_status_stacker &= " OR "
      End If
      _filter_status_stacker &= " (ST.ST_STATUS = " & TITO_STACKER_STATUS.ACTIVE
      _filter_status_stacker &= " AND MC_STATUS = " & TITO_MONEY_COLLECTION_STATUS.PENDING
      _filter_status_stacker &= IIf(String.IsNullOrEmpty(_terminals_where), "", " AND " & _terminals_where)
      _filter_status_stacker &= " ) "
    End If

    If chk_inactive.Checked Then
      _none_selected_status = False
      If Not String.IsNullOrEmpty(_filter_status_stacker) Then
        _filter_status_stacker &= " OR "
      End If
      _filter_status_stacker &= " ST.ST_STATUS IN ( " & TITO_STACKER_STATUS.DISABLED & ", " & TITO_STACKER_STATUS.DEACTIVATED & " ) "
    End If

    If chk_without_terminal.Checked Then
      If Not String.IsNullOrEmpty(_filter_status_stacker) Then
        _filter_status_stacker &= " AND "
      End If
      _filter_status_stacker &= " ( MC_STATUS <> " & TITO_MONEY_COLLECTION_STATUS.OPEN
      _filter_status_stacker &= " OR ST.ST_INSERTED IS NULL"
      _filter_status_stacker &= " ) "
    End If

    If _none_selected_status Then
      _filter_status_stacker &= IIf(String.IsNullOrEmpty(_terminals_where), "", _terminals_where & " OR TE1.TE_NAME IS NULL")
    End If

    If Not String.IsNullOrEmpty(_filter_status_stacker) Then
      If Not String.IsNullOrEmpty(_str_filter) Then
        _str_filter &= " AND "
      End If
      _str_filter &= " ( " & _filter_status_stacker & " ) "
    End If

    Return _str_filter

  End Function ' GetFilterForStacker

  ' PURPOSE : Concatenation of condition for stacker searching
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - Nullable String
  '
  Private Function GetFilterForTerminal() As String

    Dim _str_filter As String
    Dim _str_filter_tmp As String
    Dim _str_filter_by_terminal As String
    Dim _str_filter_by_provider As String
    Dim _terminals_selected As Long()

    _str_filter_by_terminal = ""
    _str_filter_by_provider = ""
    _str_filter_tmp = ""

    _str_filter = ""

    If Not Me.chk_without_terminal.Checked Then
      ' Filter Terminals
      _str_filter_tmp = ""
      If Me.uc_provider.OneTerminalChecked Then
        _str_filter_tmp = " TE1.TE_TERMINAL_ID IN " & Me.uc_provider.GetProviderIdListSelected()
      ElseIf Me.uc_provider.SeveralTerminalsChecked Then
        _terminals_selected = Me.uc_provider.GetTerminalIdListSelected()
        If Me.uc_provider.TerminalListHasValues Then
          _str_filter_tmp = " TE1.TE_TERMINAL_ID IN " & Me.uc_provider.GetProviderIdListSelected()
        End If
      ElseIf Not Me.uc_provider.AllSelectedChecked Then
        _str_filter_tmp = " TE1.TE_TERMINAL_ID IN " & Me.uc_provider.GetProviderIdListSelected()
      End If
    End If

    Return _str_filter_tmp

  End Function ' GetFilterForTerminal

  ' PURPOSE: Add new filter item form export/print purpouses
  '
  '  PARAMS:
  '     - INPUT :
  '       - Label: string with label to print
  '       - Value: text value to print
  '
  '     - OUTPUT :
  '
  Private Sub AddFilterItem(ByVal Label As String, ByVal Value As String)

    m_filter_params.Add(m_filter_params.Count, Label & ";" & Value)

  End Sub ' AddFilterItem


  ' PURPOSE: Checks whether the specified row has valid data or not
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxRow: row to check
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - TRUE: selected row contains valid data
  '     - FALSE: selected row does not exist or contains no valid data
  '
  Private Function IsValidDataRow(ByVal IdxRow As Integer) As Boolean

    If IdxRow >= 0 And IdxRow < Me.Grid.NumRows Then
      ' All rows with data have a non-empty value in column SESSION_ID.

      Return Not String.IsNullOrEmpty(Me.Grid.Cell(IdxRow, GRID_COLUMN_ID).Value)
    End If

    Return False
  End Function ' IsValidDataRow

  ' PURPOSE: Set default values to the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SetDefaultValues()

    ' Options
    Me.chk_group_by.Checked = False
    Me.opt_terminal_inserted.Checked = True
    Me.opt_terminal_inserted.Enabled = False
    Me.opt_terminal_preasigned.Enabled = False

    ' Stacker
    Me.ef_stacker_indentifier.Value = ""
    Me.ef_stacker_model.Value = ""

    ' Status
    For Each _ctrl As Control In gb_status.Controls
      If TypeOf _ctrl Is CheckBox Then
        CType(_ctrl, CheckBox).Checked = False
      End If
    Next

    ' Terminal
    Call Me.uc_provider.SetDefaultValues()

    ' Without terminal
    chk_without_terminal.Checked = False

  End Sub ' SetDefaultValues

#End Region

#Region " OVERRIDES "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_STACKERS_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2162)
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Enabled = Me.Permissions.Write
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_INVOICING.GetString(2)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False

    ' Stacker Group
    Me.xGroupStacker.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4533)
    Me.ef_stacker_indentifier.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1407)
    Me.ef_stacker_model.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2166)
    Me.chk_inactive.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4591)
    Me.chk_available.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4589)
    Me.chk_inserted.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4590)
    Me.chk_collection_pending.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2171)
    Me.chk_without_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2900)
    Me.gb_status.Text = GLB_NLS_GUI_CONTROLS.GetString(261)

    ' Group by Group

    Me.gb_group_by.Text = GLB_NLS_GUI_INVOICING.GetString(369)
    Me.chk_group_by.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2990)
    Me.opt_terminal_inserted.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2164)
    Me.opt_terminal_preasigned.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2165)

    ' edit controls activation
    Me.ef_stacker_indentifier.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 10)
    Me.ef_stacker_model.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    Me.uc_provider.Enabled = True
    Me.uc_provider.FilterByCurrency = False

    ' Define layout of all Main Grid Columns
    Call GUI_StyleSheet()

    ' Set filter default values
    Call GUI_FilterReset()

    ' Fill up uc_provider
    Dim _terminal_types() As Integer
    _terminal_types = WSI.Common.Misc.AcceptorTerminalTypeList()

    Call Me.uc_provider.Init(_terminal_types)

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Dim _frm_stacker_history As frm_tito_stacker_history_sel
    Dim _id_stacker As Int64

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        If IsValidDataRow(Me.Grid.SelectedRows(0)) Then
          _frm_stacker_history = New frm_tito_stacker_history_sel
          _id_stacker = GUI_FormatNumber(Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_ID).Value)
          _frm_stacker_history.ShowForEdit(Me.MdiParent, _id_stacker)
          _frm_stacker_history = Nothing
        End If
      Case ENUM_BUTTON.BUTTON_SELECT
        If IsValidDataRow(Me.Grid.SelectedRows(0)) Then
          MyBase.GUI_ButtonClick(ButtonId)
        End If
      Case Else
        MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub

  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Int32
    Dim _id_stacker As Int64
    Dim _frm_edit As frm_tito_stacker_edit

    If Grid.SelectedRows.Length < 1 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2040), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Return
    End If

    _idx_row = Me.Grid.SelectedRows(0)
    _id_stacker = Me.Grid.Cell(_idx_row, GRID_COLUMN_ID).Value
    _frm_edit = New frm_tito_stacker_edit
    Call _frm_edit.ShowEditItem(_id_stacker)

    _frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub ' GUI_EditSelectedItem

  Protected Overrides Sub GUI_EditNewItem()

    Dim _frm_edit As frm_tito_stacker_edit

    _frm_edit = New frm_tito_stacker_edit
    Call _frm_edit.ShowNewItem()

    _frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub ' GUI_EditNewItem

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _str_where As String
    Dim _str_sql_builder As StringBuilder

    _str_sql_builder = New StringBuilder()
    _str_sql_builder.AppendLine("    SELECT   ST.ST_STACKER_ID        AS STACKER_ID           ")
    _str_sql_builder.AppendLine("           , ST.ST_MODEL             AS MODEL                ")
    _str_sql_builder.AppendLine("           , ST.ST_STATUS            AS STACKER_STATUS       ")
    _str_sql_builder.AppendLine("           , ISNULL(TE2.TE_NAME, '') AS PREASSIGNED_TERMINAL ")
    _str_sql_builder.AppendLine("           , ISNULL(TE1.TE_NAME, '') AS INSERTED_TERMINAL    ")
    _str_sql_builder.AppendLine("           , MC.MC_STATUS            AS MC_STATUS            ")
    _str_sql_builder.AppendLine("           , ST.ST_INSERTED          AS STACKER_INSERTED     ")
    _str_sql_builder.AppendLine("      FROM   STACKERS ST                                     ")
    _str_sql_builder.AppendLine(" LEFT JOIN   MONEY_COLLECTIONS MC    ON    MC_STACKER_ID = ST_STACKER_ID  ")
    _str_sql_builder.AppendLine("  							                     AND    MC_COLLECTION_ID = (SELECT   MAX(MC_COLLECTION_ID)            ")
    _str_sql_builder.AppendLine("  													                                      FROM   MONEY_COLLECTIONS                ")
    _str_sql_builder.AppendLine("  													                                     WHERE   MC_TERMINAL_ID = ST.ST_INSERTED  ")
    _str_sql_builder.AppendLine("  													                                       AND   MC_STATUS IN (0, 1))             ")
    _str_sql_builder.AppendLine(" LEFT JOIN   TERMINALS TE1 ON TE1.TE_TERMINAL_ID = ST.ST_INSERTED             ")
    _str_sql_builder.AppendLine(" LEFT JOIN   TERMINALS TE2 ON TE2.TE_TERMINAL_ID = ST.ST_TERMINAL_PREASSIGNED ")

    ' where clausules; don't show disabled stackers
    _str_sql_builder.AppendLine("WHERE (ST.ST_STATUS <> " & TITO_STACKER_STATUS.DISABLED & ")")

    _str_where = GetSqlWhere()

    If Not String.IsNullOrEmpty(_str_where) Then
      _str_sql_builder.AppendLine(" AND " & _str_where)
    End If

    If (chk_group_by.Checked) Then
      If (opt_terminal_inserted.Checked) Then
        _str_sql_builder.AppendLine("ORDER BY (CASE WHEN TE1.TE_NAME IS NULL THEN 1 ELSE 0 END), ISNULL(TE1.TE_NAME, '')")
      Else
        _str_sql_builder.AppendLine("ORDER BY (CASE WHEN TE2.TE_NAME IS NULL THEN 1 ELSE 0 END), ISNULL(TE2.TE_NAME, '')")
      End If
    Else
      _str_sql_builder.AppendLine("ORDER BY STACKER_ID")
    End If

    Return _str_sql_builder.ToString()
  End Function ' GUI_FilterGetSqlQuery

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_ID)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_ID).Value = DbRow.Value(SQL_COLUMN_ID)
    End If
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_MODEL)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MODEL).Value = DbRow.Value(SQL_COLUMN_MODEL)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_PREASSIGNED_NAME)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_PREASSIGNED_NAME).Value = DbRow.Value(SQL_COLUMN_PREASSIGNED_NAME)
    End If
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_INSERTED_NAME)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_INSERTED_NAME).Value = DbRow.Value(SQL_COLUMN_INSERTED_NAME)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_STACKER_INSERTED)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STACKER_INSERTED).Value = DbRow.Value(SQL_COLUMN_STACKER_INSERTED)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_STATUS)) Then
      Select Case DbRow.Value(SQL_COLUMN_STATUS)
        Case TITO_STACKER_STATUS.DEACTIVATED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STACKER_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4591) ' Disabled
        Case TITO_STACKER_STATUS.DISABLED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_STACKER_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4591) ' Disabled
        Case TITO_STACKER_STATUS.ACTIVE
          If IsDBNull(DbRow.Value(SQL_COLUMN_MC_STATUS)) Then
            If IsDBNull(DbRow.Value(SQL_COLUMN_STACKER_INSERTED)) Then
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STACKER_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4589) ' Available
            Else
              Me.Grid.Cell(RowIndex, GRID_COLUMN_STACKER_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4590) ' Inserted
            End If
          ElseIf DbRow.Value(SQL_COLUMN_MC_STATUS) = TITO_MONEY_COLLECTION_STATUS.OPEN Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_STACKER_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4590) ' Inserted
          ElseIf DbRow.Value(SQL_COLUMN_MC_STATUS) = TITO_MONEY_COLLECTION_STATUS.PENDING Then
            Me.Grid.Cell(RowIndex, GRID_COLUMN_STACKER_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2171) ' Pending collection
          End If
      End Select
    End If

    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim _status As String
    Dim _idx_order As Integer
    Dim _all_checked As Boolean
    Dim _check As CheckBox

    _idx_order = 0
    m_filter_params.Clear()
    Call AddFilterItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4533), ef_stacker_indentifier.Value)
    Call AddFilterItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2166), ef_stacker_model.Value)
    Call AddFilterItem("", "")
    Call AddFilterItem("", "")
    Call AddFilterItem("", "")

    ' Stacker Status Filter
    _status = ""
    _all_checked = True

    For Each _ctrl As Control In gb_status.Controls
      If TypeOf _ctrl Is CheckBox Then
        _check = _ctrl
        _status &= IIf(_check.Checked, _check.Text & ",", "")
        _all_checked = _all_checked And _check.Checked
      End If
    Next

    If _all_checked Then
      _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
    ElseIf _status.Length > 0 Then
      _status = _status.Substring(0, _status.Length - 1)
    End If

    Call AddFilterItem(GLB_NLS_GUI_CONTROLS.GetString(261), _status)
    Call AddFilterItem("", "")
    Call AddFilterItem("", "")
    Call AddFilterItem("", "")

    Call AddFilterItem("", "")
    Call AddFilterItem("", "")
    Call AddFilterItem("", "")
    Call AddFilterItem("", "")


    _status = ""
    _all_checked = True

    _status &= IIf(chk_without_terminal.Checked, GLB_NLS_GUI_AUDITOR.GetString(336) & ",", GLB_NLS_GUI_AUDITOR.GetString(337) & ",")

    If _status.Length > 0 Then
      _status = _status.Substring(0, _status.Length - 1)
    End If

    Call AddFilterItem(chk_without_terminal.Text(), _status)

    ' Providers - Terminals
    If Me.chk_without_terminal.Checked Then
      Me.m_terminals = ""
    Else
      m_terminals = Me.uc_provider.GetTerminalReportText()
    End If

    ' Group by terminal
    m_group_by_terminal = ""

    If Me.chk_group_by.Checked AndAlso (opt_terminal_inserted.Checked Or opt_terminal_preasigned.Checked) Then
      m_group_by_terminal = chk_group_by.Text
      If opt_terminal_inserted.Checked Then
        m_group_by_terminal = m_group_by_terminal & " " & opt_terminal_inserted.Text
      Else
        m_group_by_terminal = m_group_by_terminal & " " & opt_terminal_preasigned.Text
      End If
    End If

  End Sub ' GUI_ReportUpdateFilters

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    Dim _text As String
    Dim _keys() As Integer
    Dim _values() As String

    ReDim _keys(m_filter_params.Count - 1)

    m_filter_params.Keys.CopyTo(_keys, 0)
    Array.Sort(_keys)

    For Each _key As Integer In _keys
      _text = m_filter_params(_key)
      _values = _text.Split(";")

      If String.IsNullOrEmpty(_values(0)) Then
        PrintData.SetFilter("", "", True)
      Else
        PrintData.SetFilter(_values(0), _values(1))
      End If
    Next

    'terminals
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)

    ' Group by terminal
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(369), m_group_by_terminal)

    PrintData.FilterHeaderWidth(1) = 2000
    PrintData.FilterHeaderWidth(2) = 2500
    PrintData.FilterHeaderWidth(3) = 2500
    PrintData.FilterValueWidth(2) = 2100
    PrintData.FilterValueWidth(4) = 2100

  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ExcelData.SetColumnFormat(GRID_COLUMN_MODEL, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)

  End Sub ' GUI_ReportParams


  Protected Overrides Sub GUI_AfterLastRow()
    If Me.Grid.NumRows <> 0 Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = True

    Else

      Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = False
    End If

    MyBase.GUI_AfterLastRow()
  End Sub ' GUI_AfterLastRow

  Protected Overrides Sub GUI_GetSelectedItems()

    If Grid.SelectedRows.Length < 1 Then
      Return
    End If

    StackerIdSelected = Me.Grid.Cell(Me.Grid.SelectedRows(0), GRID_COLUMN_ID).Value

  End Sub


#End Region ' OVERRIDES

#Region " Public Functions "

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit


#End Region ' Public Functions

#Region " Events "
  ' PURPOSE: Check/Uncheck controls
  '
  '  PARAMS:
  '     - INPUT :
  '       - Value: boolean to set in property Checked
  '
  '     - OUTPUT :
  '
  Private Sub CheckOrUncheckControls(ByVal Value As Boolean)

    Dim _ctrl As Control
    Dim _check As CheckBox

    For Each _ctrl In gb_status.Controls
      If TypeOf _ctrl Is CheckBox Then
        _check = _ctrl
        _check.Checked = Value
      End If
    Next

  End Sub ' CheckOrUncheckControls

  Private Function CheckAllStatusUnchecked() As Boolean

    For Each _ctrl As Control In gb_status.Controls
      If TypeOf (_ctrl) Is CheckBox AndAlso CType(_ctrl, CheckBox).Checked Then

        Return False
      End If
    Next

    Return True

  End Function

  ' PURPOSE : Check if chk_without_terminal is checked and enable/disable other controls 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub chk_anon_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_without_terminal.CheckedChanged
    Me.uc_provider.Enabled = Not Me.chk_without_terminal.Checked
    Me.chk_inserted.Checked = False
    Me.chk_inserted.Enabled = Not Me.chk_without_terminal.Checked
  End Sub

  Private Sub chk_group_by_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_group_by.CheckedChanged
    ' Enable or dissable options 
    opt_terminal_preasigned.Enabled = chk_group_by.Checked
    opt_terminal_inserted.Enabled = chk_group_by.Checked

  End Sub

  Private Sub gb_status_chk_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_available.CheckedChanged, chk_inserted.CheckedChanged, chk_inactive.CheckedChanged, chk_collection_pending.CheckedChanged

    If chk_without_terminal.Checked Then
      uc_provider.Enabled = False
    Else
      uc_provider.Enabled = chk_collection_pending.Checked OrElse _
                            chk_inserted.Checked OrElse _
                            CheckAllStatusUnchecked()
    End If
  End Sub

#End Region

End Class