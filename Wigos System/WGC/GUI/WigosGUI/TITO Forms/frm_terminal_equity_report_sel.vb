﻿'--------------------------------------------------------------------------------------
' Copyright © 2015 Win Systems Ltd.
'--------------------------------------------------------------------------------------
'
'   MODULE NAME :  frm_terminal_equity_report_sel.vb
'
'   DESCRIPTION :  Equity Percentage Terminal Report
'
'        AUTHOR :  Rodrigo Gonzalez Rodriguez
'
' CREATION DATE :  02-DIC-2015
'
' REVISION HISTORY
'
' Date         Author Description
' -----------  ------ ------------------------------------------------------------------
' 30-DEC-2015  RGR    Initial version - Product Backlog Item 6451:Garcia River-03: equity report
' 24-FEB-2016  ESE    Task 9344:6451 Garcia River-03: equity report
' 01-MAR-2016  ESE    Product Backlog Item 10174:Sprint Review 20
' 24-MAY-2016  ETP    Fixed Bug 13630: Error when null value is returned
' 26-MAY-2016  FAV    Bug 13704: Validate information that returns null value
' 27-OCT-2016  FAV    PBI 19521: EGASA - CS Ticket 1486: Invalid value for EQUITY column 
' 22-NOV-2016  PDM    PBI 20566:Cage Str Team 1 - Cambios Review Sprint 32
' 07-FEB-2017  RAB    Bug 24283: Reports without properly informing the account denomination
' 19-OCT-2017  RAB    Bug 30320: Garcia River - Participation by Machine Report - 03.005.103 - Errores encontrados
' 24-JAN-2018  DHA    Bug 31318:WIGOS-6678 [Ticket #10528] Release 03.005.103 - Maquinas Retiradas ya no aparecen en el Participation by machine report
'---------------------------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports System.Text
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common.TITO
Imports WSI.Common
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections.Generic

Public Class frm_terminal_equity_report_sel
  Inherits frm_base_sel

#Region "Structures"

  Private Structure SUB_TOTAL
    Dim index As Integer
    Dim provider As String
    Dim title As String
    Dim coin_in As Decimal
    Dim ticket_in As Decimal
    Dim ticket_out As Decimal
    Dim net_win As Decimal
    Dim equity_net_win As Decimal
  End Structure

#End Region

#Region "Members"
  Private m_from_date As DateTime
  Private m_to_date As DateTime
  Private m_is_to_date As Boolean
  Private m_credit_type As DictionaryEntry
  Private m_name_select As String
  Private m_terminals_id_select As String
  Private m_default_equity As Decimal
  Private m_show_total As Boolean
  Private m_change_provider As String
  Private m_row_index As Long
  Private m_sub_totals As List(Of SUB_TOTAL)
  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False

#Region "Grid"
  Private GRID_COLUMNS As Integer
  Private GRID_COLUMN_PROVIDER As Integer
  Private GRID_COLUMN_DENOMINATION As Integer
  Private GRID_COLUMN_COIN_IN As Integer
  Private GRID_COLUMN_TOTAL_IN As Integer
  Private GRID_COLUMN_TOTAL_OUT As Integer
  Private GRID_COLUMN_NETWIN As Integer
  Private GRID_COLUMN_EQUITY As Integer
  Private GRID_COLUMN_EQUITY_NETWIN As Integer

  Private GRID_COLUMN_COIN_IN_VALUE As Integer
  Private GRID_COLUMN_TOTAL_IN_VALUE As Integer
  Private GRID_COLUMN_TOTAL_OUT_VALUE As Integer
  Private GRID_COLUMN_NETWIN_VALUE As Integer
  Private GRID_COLUMN_EQUITY_VALUE As Integer
  Private GRID_COLUMN_EQUITY_NETWIN_VALUE As Integer
#End Region

#End Region

#Region "Constants"

#Region "Central Grid"

  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const COUNTER_DETAILS As Integer = 1
  Private Const COUNTER_SUBTOTALS As Integer = 2

  Private Const GRID_COLUMN_SELECT As Integer = 0
  Private Const GRID_INIT_TERMINAL_DATA As Integer = 1

  Private Const GRID_WIDTH_SELECT As Integer = 200
  Private Const GRID_WIDTH_DENOMINATION As Integer = 2300
  Private Const GRID_WIDTH_COIN_IN As Integer = 2300
  Private Const GRID_WIDTH_TICKET_IN As Integer = 2300
  Private Const GRID_WIDTH_TICKET_OUT As Integer = 2300
  Private Const GRID_WIDTH_NETWIN As Integer = 2300
  Private Const GRID_WIDTH_EQUITY As Integer = 1700
  Private Const GRID_WIDTH_EQUITY_NETWIN As Integer = 2300

  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 0
  Private Const SQL_COLUMN_DENOMINATION As Integer = 1
  Private Const SQL_COLUMN_EQUITY As Integer = 2
  Private Const SQL_COLUMN_COIN_IN As Integer = 3
  Private Const SQL_COLUMN_TOTAL_IN As Integer = 4
  Private Const SQL_COLUMN_TOTAL_OUT As Integer = 5
  Private Const SQL_COLUMN_CHK_EQUITY As Integer = 6

#End Region

#End Region

#Region "Constructors"
  Public Sub New()
    InitializeComponent()
    Me.m_default_equity = CLASS_TERMINAL.DefaultEquityPercentage()
    Me.m_row_index = 0
    Me.m_show_total = True
    Me.m_sub_totals = New List(Of SUB_TOTAL)()
  End Sub
#End Region

#Region "Overrides"

  Protected Overrides Sub GUI_InitControls()
    MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6958)
    MyBase.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    MyBase.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False

    Me.uc_dsl.Init(GLB_NLS_GUI_INVOICING.Id(201), True, False)

    Me.cmb_credit_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1163)

    Call Me.GUI_StyleSheet()

    Call Me.SetCreditTypeCombo()
    Call Me.uc_pr_list.Init(WSI.Common.Misc.AllTerminalTypes(), uc_provider.UC_FILTER_TYPE.NONE)

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    Call Me.SetDefaultValues()

  End Sub

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TERMINAL_EQUITY_REPORT_SEL
    Call MyBase.GUI_SetFormId()

  End Sub

  Protected Overrides Sub GUI_FilterReset()
    Me.SetDefaultValues()

  End Sub

  Protected Overrides Function GUI_FilterCheck() As Boolean
    Dim _terminal_select As Long()

    If Not Me.uc_dsl.FromDateSelected Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(114), ENUM_MB_TYPE.MB_TYPE_WARNING, Nothing, Nothing, Me.uc_dsl.Text)
      Me.uc_dsl.Focus()
      Return False

    End If

    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_INVOICING.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Me.uc_dsl.Focus()
        Return False

      End If
    End If

    If Me.uc_pr_list.SeveralTerminalsChecked Then
      _terminal_select = Me.uc_pr_list.GetTerminalIdListSelected()
      If Not Me.uc_pr_list.TerminalListHasValues Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(102), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, Nothing, Nothing, Me.uc_pr_list.Controls(0).Text)
        Me.uc_pr_list.Focus()
        Return False

      End If
    End If
    Me.m_row_index = 0

    Return True

  End Function

  Protected Overrides Sub GUI_ReportUpdateFilters()

    Me.m_from_date = Me.GetDate(Me.uc_dsl.FromDate)

    If Me.uc_dsl.ToDateSelected Then
      Me.m_to_date = Me.GetDate(Me.uc_dsl.ToDate)
      Me.m_is_to_date = True
    Else
      Me.m_to_date = Me.GetWGDBDateTime().AddDays(1)
      Me.m_is_to_date = False
    End If

    Me.m_credit_type = New DictionaryEntry(Me.cmb_credit_type.Value, Me.cmb_credit_type.TextValue)

    Me.m_name_select = Me.uc_pr_list.GetTerminalReportText()

    If Me.uc_pr_list.OneTerminalChecked Then
      Me.m_terminals_id_select = Me.uc_pr_list.cmb_terminal.Value
    Else
      Me.m_terminals_id_select = Me.GetTerminalsIDSelect()
    End If

  End Sub

  Protected Overrides Function GUI_GetQueryType() As frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_COMMAND
  End Function

  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand
    Dim _sql_command As SqlCommand = New SqlCommand()
    _sql_command.CommandText = "GetEquityPercentageMachine"
    _sql_command.CommandType = CommandType.StoredProcedure
    _sql_command.Parameters.Add("@pStarDateTime", SqlDbType.DateTime).Value = Me.m_from_date
    _sql_command.Parameters.Add("@pEndDateTime", SqlDbType.DateTime).Value = Me.m_to_date
    _sql_command.Parameters.Add("@pCreditType", SqlDbType.Int).Value = Me.m_credit_type.Key
    _sql_command.Parameters.Add("@pTerminalsID", SqlDbType.NVarChar).Value = Me.m_terminals_id_select
    Return _sql_command
  End Function

  Public Overrides Function GUI_CheckOutRowBeforeAdd(DbRow As frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _terminal_data As List(Of Object)
    Dim _provider_name As String

    'get the information of the terminal in a list 
    _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)
    'obtains the name of the provider
    _provider_name = _terminal_data(0)

    Dim _index As Integer

    If Me.m_show_total Then
      If _provider_name = "" Then
        Return False
      End If
      If Me.m_row_index = 0 Then
        Me.m_change_provider = _provider_name
      End If

      'Check if the provider has changed in order to calculate the subtotal
      If Me.m_change_provider <> _provider_name Then
        _index = MyBase.Grid.AddRow()
        Me.m_sub_totals.Add(New SUB_TOTAL With {.index = _index, .provider = Me.m_change_provider,
                                                .title = String.Concat(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046), ": ", Me.m_change_provider)})
        Me.m_change_provider = _provider_name
      End If
    End If

    Return True

  End Function

  Public Overrides Function GUI_SetupRow(RowIndex As Integer, DbRow As frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _total_in As Decimal
    Dim _total_out As Decimal
    Dim _net_win As Decimal
    Dim _equity As Decimal
    Dim _terminal_data As List(Of Object)

    With MyBase.Grid
      'Terminal Report
      _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_data.Count - 1
        If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
          Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
        Else
          Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = ""
        End If
      Next

      'Denomination
      If Not DbRow.IsNull(SQL_COLUMN_DENOMINATION) Then
        .Cell(RowIndex, GRID_COLUMN_DENOMINATION).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_DENOMINATION))
      End If

      'Coin IN
      .Cell(RowIndex, GRID_COLUMN_COIN_IN).Value = If(DbRow.IsNull(SQL_COLUMN_COIN_IN), 0, GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COIN_IN)))
      .Cell(RowIndex, GRID_COLUMN_COIN_IN_VALUE).Value = If(DbRow.IsNull(SQL_COLUMN_COIN_IN), 0, DbRow.Value(SQL_COLUMN_COIN_IN))

      'Total IN
      _total_in = If(DbRow.IsNull(SQL_COLUMN_TOTAL_IN), 0, DbRow.Value(SQL_COLUMN_TOTAL_IN))
      .Cell(RowIndex, GRID_COLUMN_TOTAL_IN).Value = If(DbRow.IsNull(SQL_COLUMN_TOTAL_IN), 0, GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTAL_IN)))
      .Cell(RowIndex, GRID_COLUMN_TOTAL_IN_VALUE).Value = If(DbRow.IsNull(SQL_COLUMN_TOTAL_IN), 0, DbRow.Value(SQL_COLUMN_TOTAL_IN))

      'Total OUT
      _total_out = If(DbRow.IsNull(SQL_COLUMN_TOTAL_OUT), 0, DbRow.Value(SQL_COLUMN_TOTAL_OUT))
      .Cell(RowIndex, GRID_COLUMN_TOTAL_OUT).Value = If(DbRow.IsNull(SQL_COLUMN_TOTAL_OUT), 0, GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TOTAL_OUT)))
      .Cell(RowIndex, GRID_COLUMN_TOTAL_OUT_VALUE).Value = If(DbRow.IsNull(SQL_COLUMN_TOTAL_OUT), 0, DbRow.Value(SQL_COLUMN_TOTAL_OUT))

      'Netwin
      _net_win = _total_in - _total_out
      .Cell(RowIndex, GRID_COLUMN_NETWIN).Value = GUI_FormatCurrency(_net_win)
      .Cell(RowIndex, GRID_COLUMN_NETWIN_VALUE).Value = _net_win

      'Equity
      'BUG 30320 - Changed Participation Logic
      If DbRow.IsNull(SQL_COLUMN_CHK_EQUITY) Then
        _equity = Me.m_default_equity
      ElseIf DbRow.Value(SQL_COLUMN_CHK_EQUITY) = False Then
        _equity = Me.m_default_equity
      Else
        If DbRow.IsNull(SQL_COLUMN_EQUITY) Then
          _equity = 0
        Else
          _equity = DbRow.Value(SQL_COLUMN_EQUITY)
        End If
      End If

      .Cell(RowIndex, GRID_COLUMN_EQUITY).Value = GUI_FormatNumber(_equity, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      .Cell(RowIndex, GRID_COLUMN_EQUITY_VALUE).Value = _equity

      'Equity netwin
      .Cell(RowIndex, GRID_COLUMN_EQUITY_NETWIN).Value = GUI_FormatCurrency(_net_win * (_equity / 100))
      .Cell(RowIndex, GRID_COLUMN_EQUITY_NETWIN_VALUE).Value = _net_win * (_equity / 100)
    End With

    Me.m_row_index = RowIndex + 1

    Return True

  End Function

  Protected Overrides Sub GUI_AfterLastRow()

    'We check if there were matches before add the totals and subtotals in order to avoid a wrong counting of rows
    If MyBase.Grid.NumRows <> 0 Then
      Dim _index As Integer
      Dim _ls_sum As List(Of SUB_TOTAL)

      _index = MyBase.Grid.AddRow()
      Me.m_sub_totals.Add(New SUB_TOTAL With {.index = _index, .provider = MyBase.Grid.Cell(_index - 1, SQL_COLUMN_TERMINAL_ID + 1).Value,
                                             .title = String.Concat(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046), ": ", MyBase.Grid.Cell(_index - 1, SQL_COLUMN_TERMINAL_ID + 1).Value)})

      _ls_sum = Me.SumSubTotal()
      Me.m_sub_totals.Clear()

      Call Me.SetSubTotalsGrid(_ls_sum)

      _index = MyBase.Grid.AddRow()
      Call Me.SetTotalGrid(_index, _ls_sum)

      MyBase.Grid.Counter(COUNTER_DETAILS).Value = MyBase.Grid.NumRows - (_ls_sum.Count + 1)
      MyBase.Grid.Counter(COUNTER_SUBTOTALS).Value = _ls_sum.Count
    End If
  End Sub

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    MyBase.GUI_ReportFilter(PrintData)

    Call PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(202), GUI_FormatDate(Me.m_from_date, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))

    If Me.m_is_to_date Then
      Call PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), GUI_FormatDate(Me.m_to_date, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS))
    Else
      Call PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(201) & " " & GLB_NLS_GUI_INVOICING.GetString(203), String.Empty)
    End If

    Call PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1163), Me.m_credit_type.Value)

    Call PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), Me.m_name_select)

  End Sub

  Protected Overrides Sub GUI_BeforeFirstRow()

    'If chk_terminal_location was checked/uncheked, we need to refresh the grid
    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If

    m_refresh_grid = False
  End Sub

#End Region

#Region "Public Functions"
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)
    MyBase.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    MyBase.MdiParent = MdiParent
    MyBase.Display(False)
  End Sub
#End Region

#Region "Private Functions"

  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With MyBase.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .Sortable = False
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      .Counter(COUNTER_DETAILS).Visible = True
      .Counter(COUNTER_DETAILS).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      .Counter(COUNTER_DETAILS).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      .Counter(COUNTER_SUBTOTALS).Visible = True
      .Counter(COUNTER_SUBTOTALS).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
      .Counter(COUNTER_SUBTOTALS).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)

      .Column(GRID_COLUMN_SELECT).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_SELECT).Header(1).Text = String.Empty
      .Column(GRID_COLUMN_SELECT).Width = GRID_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).HighLightWhenSelected = False
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False

      'Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1

        'Set the main header
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)

        'In order to put in the second header 'MACHINE' instead of 'NAME'
        If _idx = 1 Then
          'set the second header
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(592)
        Else
          'set the second header
          .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        End If

        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        If _terminal_columns(_idx).Column = ReportColumn.Provider Then
          GRID_COLUMN_PROVIDER = GRID_INIT_TERMINAL_DATA + _idx
        End If
      Next
      .Column(GRID_COLUMN_DENOMINATION).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(232)
      .Column(GRID_COLUMN_DENOMINATION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2270)
      .Column(GRID_COLUMN_DENOMINATION).Width = GRID_WIDTH_DENOMINATION
      .Column(GRID_COLUMN_DENOMINATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      'Coin In
      .Column(GRID_COLUMN_COIN_IN).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_COIN_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6960)
      .Column(GRID_COLUMN_COIN_IN).Width = GRID_WIDTH_COIN_IN
      .Column(GRID_COLUMN_COIN_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      'Total IN
      .Column(GRID_COLUMN_TOTAL_IN).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_TOTAL_IN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6920)
      .Column(GRID_COLUMN_TOTAL_IN).Width = GRID_WIDTH_TICKET_IN
      .Column(GRID_COLUMN_TOTAL_IN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      'Total OUT
      .Column(GRID_COLUMN_TOTAL_OUT).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_TOTAL_OUT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3109)
      .Column(GRID_COLUMN_TOTAL_OUT).Width = GRID_WIDTH_TICKET_OUT
      .Column(GRID_COLUMN_TOTAL_OUT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      'NetWin
      .Column(GRID_COLUMN_NETWIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)
      .Column(GRID_COLUMN_NETWIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)
      .Column(GRID_COLUMN_NETWIN).Width = GRID_WIDTH_NETWIN
      .Column(GRID_COLUMN_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      .Column(GRID_COLUMN_EQUITY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)
      .Column(GRID_COLUMN_EQUITY).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6950)
      .Column(GRID_COLUMN_EQUITY).Width = GRID_WIDTH_EQUITY
      .Column(GRID_COLUMN_EQUITY).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      .Column(GRID_COLUMN_EQUITY_NETWIN).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(539)
      .Column(GRID_COLUMN_EQUITY_NETWIN).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6961)
      .Column(GRID_COLUMN_EQUITY_NETWIN).Width = GRID_WIDTH_EQUITY_NETWIN
      .Column(GRID_COLUMN_EQUITY_NETWIN).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT_CENTER

      'Coin In Value   
      .Column(GRID_COLUMN_COIN_IN_VALUE).Width = 0

      'Total In Value    
      .Column(GRID_COLUMN_TOTAL_IN_VALUE).Width = 0

      'Ticket Out Value    
      .Column(GRID_COLUMN_TOTAL_OUT_VALUE).Width = 0

      'NetWin Value    
      .Column(GRID_COLUMN_NETWIN_VALUE).Width = 0
      .Column(GRID_COLUMN_EQUITY_VALUE).Width = 0
      .Column(GRID_COLUMN_EQUITY_NETWIN_VALUE).Width = 0

    End With
  End Sub

  Private Sub SetDefaultValues()
    Call Me.SetDefaultDate()
    Me.cmb_credit_type.SelectedIndex = 0
    Me.uc_pr_list.SetDefaultValues()
    Me.chk_terminal_location.Checked = False
  End Sub

  Private Sub SetDefaultDate()

    Dim _init_date As DateTime
    Dim _closing_time As Integer

    _init_date = Me.GetWGDBDateTime()
    _closing_time = Me.GetDefaultClosingTime()

    Me.uc_dsl.FromDate = _init_date
    Me.uc_dsl.FromDateSelected = True

    Me.uc_dsl.ToDate = _init_date.AddDays(1)
    Me.uc_dsl.ToDateSelected = False

    Me.uc_dsl.ClosingTime = _closing_time

  End Sub

  Private Function GetWGDBDateTime() As DateTime
    Dim _now As DateTime
    Dim _init_date As DateTime
    Dim _closing_time As Integer

    _now = WGDB.Now
    _closing_time = Me.GetDefaultClosingTime()
    _init_date = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)

    If _init_date > _now Then
      _init_date = _init_date.AddDays(-1)
    End If

    Return _init_date

  End Function

  Private Function GetDate(ByVal select_date As DateTime) As DateTime
    Dim _closing_time As Integer
    Dim _result_date As DateTime

    _closing_time = Me.GetDefaultClosingTime()
    _result_date = New DateTime(select_date.Year, select_date.Month, select_date.Day, _closing_time, 0, 0)

    Return _result_date
  End Function

  Private Sub SetCreditTypeCombo()
    Me.cmb_credit_type.Add(1, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125))
    Me.cmb_credit_type.Add(2, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2126))
    Me.cmb_credit_type.Add(3, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2645))

  End Sub

  Private Function GetTerminalsIDSelect() As String
    Dim _select As List(Of String)

    _select = New List(Of String)()
    For Each _item As Long In Me.uc_pr_list.GetTerminalIdListSelected()
      _select.Add(_item)
    Next

    Return String.Join(",", _select.ToArray())

  End Function

  Private Function SumSubTotal() As List(Of SUB_TOTAL)
    Dim _group_index As Integer
    Dim _sum_subtotal As SUB_TOTAL
    Dim _ls As List(Of SUB_TOTAL)

    _group_index = 0
    _ls = New List(Of SUB_TOTAL)()

    With MyBase.Grid

      For Each _item As SUB_TOTAL In Me.m_sub_totals
        _sum_subtotal = New SUB_TOTAL()
        _sum_subtotal.index = _item.index
        _sum_subtotal.provider = _item.provider
        _sum_subtotal.title = _item.title

        For _row_index As Integer = _group_index To _item.index - 1

          _sum_subtotal.coin_in += .Cell(_row_index, GRID_COLUMN_COIN_IN_VALUE).Value

          _sum_subtotal.ticket_in += .Cell(_row_index, GRID_COLUMN_TOTAL_IN_VALUE).Value

          _sum_subtotal.ticket_out += .Cell(_row_index, GRID_COLUMN_TOTAL_OUT_VALUE).Value
          _sum_subtotal.net_win += .Cell(_row_index, GRID_COLUMN_NETWIN_VALUE).Value
          _sum_subtotal.equity_net_win += .Cell(_row_index, GRID_COLUMN_EQUITY_NETWIN_VALUE).Value

        Next
        _ls.Add(_sum_subtotal)
        _group_index = _item.index + 1
      Next

    End With

    Return _ls

  End Function

  Private Sub SetSubTotalsGrid(ByRef ls As List(Of SUB_TOTAL))

    For Each _item As SUB_TOTAL In ls

      Call Me.SetRowGrid(_item, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    Next

  End Sub

  Private Sub SetTotalGrid(index As Integer, ls_sum As List(Of SUB_TOTAL))
    Dim _item As SUB_TOTAL

    _item = Me.GetTotalGrid(index, ls_sum)

    Call Me.SetRowGrid(_item, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

  End Sub

  Private Function GetTotalGrid(index As Integer, ls_sum As List(Of SUB_TOTAL)) As SUB_TOTAL
    Dim _item_sum As SUB_TOTAL

    _item_sum = New SUB_TOTAL()
    _item_sum.index = index
    _item_sum.title = GLB_NLS_GUI_INVOICING.GetString(205)

    For Each _index As SUB_TOTAL In ls_sum

      _item_sum.coin_in += _index.coin_in
      _item_sum.ticket_in += _index.ticket_in
      _item_sum.ticket_out += _index.ticket_out
      _item_sum.net_win += _index.net_win
      _item_sum.equity_net_win += _index.equity_net_win

    Next

    Return _item_sum

  End Function

  Private Sub SetRowGrid(ByVal total As SUB_TOTAL, ByVal color As ENUM_GUI_COLOR)

    'In order to draw color to all terminal columns
    For _idx As Int32 = 0 To GRID_COLUMNS - 14
      If (GRID_COLUMN_PROVIDER + _idx) = 2 Then
        'If is the column TERMINAL, we set the title
        Call Me.SetCellValueGrid(total.index, GRID_COLUMN_PROVIDER + _idx, total.title, color)
      Else
        Call Me.SetCellValueGrid(total.index, GRID_COLUMN_PROVIDER + _idx, String.Empty, color)
      End If
    Next
    Call Me.SetCellValueGrid(total.index, GRID_COLUMN_DENOMINATION, String.Empty, color)
    Call Me.SetCellValueGrid(total.index, GRID_COLUMN_COIN_IN, GUI_FormatCurrency(total.coin_in), color)
    Call Me.SetCellValueGrid(total.index, GRID_COLUMN_TOTAL_IN, GUI_FormatCurrency(total.ticket_in), color)
    Call Me.SetCellValueGrid(total.index, GRID_COLUMN_TOTAL_OUT, GUI_FormatCurrency(total.ticket_out), color)
    Call Me.SetCellValueGrid(total.index, GRID_COLUMN_NETWIN, GUI_FormatCurrency(total.net_win), color)
    Call Me.SetCellValueGrid(total.index, GRID_COLUMN_EQUITY, String.Empty, color)
    Call Me.SetCellValueGrid(total.index, GRID_COLUMN_EQUITY_NETWIN, GUI_FormatCurrency(total.equity_net_win), color)

  End Sub

  Private Sub SetCellValueGrid(ByVal index As Integer, ByVal column As Integer, ByRef value As String, ByVal color As ENUM_GUI_COLOR)

    With MyBase.Grid
      .Cell(index, column).Value = value
      .Cell(index, column).BackColor = GetColor(color)
      .Cell(index, column).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
    End With

  End Sub

  Private Function GetDefaultClosingTime() As Integer
    Return GeneralParam.GetInt32("WigosGUI", "ClosingTime", 8)
  End Function

  Private Sub GridColumnReIndex()

    'The number an position of columns depends on "chk_terminal_location" 
    If chk_terminal_location.Checked Then
      GRID_COLUMNS = 20

      GRID_COLUMN_DENOMINATION = 7
      GRID_COLUMN_COIN_IN = 8
      GRID_COLUMN_TOTAL_IN = 9
      GRID_COLUMN_TOTAL_OUT = 10
      GRID_COLUMN_NETWIN = 11
      GRID_COLUMN_EQUITY = 12
      GRID_COLUMN_EQUITY_NETWIN = 13

      GRID_COLUMN_COIN_IN_VALUE = 14
      GRID_COLUMN_TOTAL_IN_VALUE = 15
      GRID_COLUMN_TOTAL_OUT_VALUE = 16
      GRID_COLUMN_NETWIN_VALUE = 17
      GRID_COLUMN_EQUITY_VALUE = 18
      GRID_COLUMN_EQUITY_NETWIN_VALUE = 19
    Else
      GRID_COLUMNS = 17

      GRID_COLUMN_DENOMINATION = 4
      GRID_COLUMN_COIN_IN = 5
      GRID_COLUMN_TOTAL_IN = 6
      GRID_COLUMN_TOTAL_OUT = 7
      GRID_COLUMN_NETWIN = 8
      GRID_COLUMN_EQUITY = 9
      GRID_COLUMN_EQUITY_NETWIN = 10

      GRID_COLUMN_COIN_IN_VALUE = 11
      GRID_COLUMN_TOTAL_IN_VALUE = 12
      GRID_COLUMN_TOTAL_OUT_VALUE = 13
      GRID_COLUMN_NETWIN_VALUE = 14
      GRID_COLUMN_EQUITY_VALUE = 15
      GRID_COLUMN_EQUITY_NETWIN_VALUE = 16
    End If

  End Sub

#End Region

#Region "Events"

  Private Sub chk_terminal_location_CheckedChanged(sender As Object, e As EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

#End Region

End Class