'--------------------------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'--------------------------------------------------------------------------------------
'
'   MODULE NAME :  frm_cash_tito_sel.vb
'
'   DESCRIPTION :  Total Cash in site (TITO)
'
'        AUTHOR :  Nelson Madrigal Reyes
'
' CREATION DATE :  02-JUL-2013
'
' REVISION HISTORY
'
' Date         Author Description
' -----------  ------ ------------------------------------------------------------------
' 02-JUL-2013  NMR    Initial version
' 23-AUG-2013  JRM    Code revision and refactoring
' 06-SEP-2013  NMR    Fixed error: JIRA-WIGOSTITO-90
' 16-SEP-2013  NMR    Fixed error: JIRA-WIGOSTITO-140
'---------------------------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports System.Text
Imports System.Data.SqlClient
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common

Public Class frm_cash_tito_sel
  Inherits GUI_Controls.frm_base_sel

#Region " Constants "

  ' Grid configuration
  Private Const GRID_COLUMNS_COUNT As Integer = 8
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Columns Index
  Private Const START_COLUMN_MONEY As Integer = 2
  Private Const GRID_COLUMN_SELECT As Integer = 0
  Private Const GRID_COLUMN_TERMINAL_NAME As Integer = 1
  Private Const GRID_COLUMN_TERM_REDIM As Integer = 2
  Private Const GRID_COLUMN_TERM_NOREDIM As Integer = 3
  Private Const GRID_COLUMN_TICKET_REDIM As Integer = 4
  Private Const GRID_COLUMN_TICKET_NOREDIM As Integer = 5
  Private Const GRID_COLUMN_BILLS_COUNT As Integer = 6
  Private Const GRID_COLUMN_BILLS_TOTAL As Integer = 7

  ' SQL Columns
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 1
  Private Const SQL_COLUMN_TERM_REDIM As Integer = 2
  Private Const SQL_COLUMN_TERM_NOREDIM As Integer = 3
  Private Const SQL_COLUMN_TICKET_REDIM As Integer = 4
  Private Const SQL_COLUMN_TICKET_NOREDIM As Integer = 5
  Private Const SQL_COLUMN_BILLS_COUNT As Integer = 6
  Private Const SQL_COLUMN_BILLS_TOTAL As Integer = 7
  Private Const SQL_COLUMN_TERMINAL_TYPE As Integer = 10

  ' Columns Width
  Private Const GRID_COLUMN_WIDTH_SELECT As Integer = 200
  Private Const GRID_COLUMN_WIDTH_TERMINAL As Integer = 2200
  Private Const GRID_COLUMN_WIDTH_TERM_REDIM As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_TERM_NOREDIM As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_TICKET_REDIM As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_TICKET_NOREDIM As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_BILLS_COUNT As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_BILLS_TOTAL As Integer = 2000

  'Cashier Grid
  Private Const GRID_CASHIER_HEADER_ROWS As Integer = 0
  Private Const GRID_CASHIER_COLUMNS As Integer = 3
  Private Const GRID_CASHIER_ID As Integer = 0
  Private Const GRID_CASHIER_COLUMN_CHECKED As Integer = 1
  Private Const GRID_CASHIER_COLUMN_DESCRIPTION As Integer = 2
  Private Const GRID_CASHIER_WIDTH_COLUMN_DESCRIPTION As Integer = 3630
  Private Const GRID_CASHIER_WIDTH_COLUMN_CHECKED As Integer = 300

#End Region ' Constants

#Region " Members"

  Private m_prev_terminal_type As Integer
  Private Const m_terminal_type_totals As Integer = 9999

  ' Terminal names
  Private m_terminals As New Hashtable()

  ' List of SUMs of the columns grouped by cashier/terminal
  Private m_terminals_by_type As New Hashtable()

  ' When introducing data in the table, we will insert a numeric prefix to maintain the order. This prefix will be 
  ' removed before sending to the printing routines
  Private m_filter_params As New Hashtable()

#End Region ' Members

#Region " Private Functions and Methods "

  ' PURPOSE : Define layout of all Main Grid Columns
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS_COUNT, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = False

      .Column(GRID_COLUMN_SELECT).Header(0).Text = ""
      .Column(GRID_COLUMN_SELECT).Header(1).Text = ""
      .Column(GRID_COLUMN_SELECT).Width = GRID_COLUMN_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False

      .Column(GRID_COLUMN_TERMINAL_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(492)
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253)
      .Column(GRID_COLUMN_TERMINAL_NAME).Width = GRID_COLUMN_WIDTH_TERMINAL

      .Column(GRID_COLUMN_TERM_REDIM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2310)
      .Column(GRID_COLUMN_TERM_REDIM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125)
      .Column(GRID_COLUMN_TERM_REDIM).Width = GRID_COLUMN_WIDTH_TERM_REDIM

      .Column(GRID_COLUMN_TERM_NOREDIM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2310)
      .Column(GRID_COLUMN_TERM_NOREDIM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2126)
      .Column(GRID_COLUMN_TERM_NOREDIM).Width = GRID_COLUMN_WIDTH_TERM_NOREDIM

      .Column(GRID_COLUMN_TICKET_REDIM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2311)
      .Column(GRID_COLUMN_TICKET_REDIM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125)
      .Column(GRID_COLUMN_TICKET_REDIM).Width = GRID_COLUMN_WIDTH_TICKET_REDIM

      .Column(GRID_COLUMN_TICKET_NOREDIM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2311)
      .Column(GRID_COLUMN_TICKET_NOREDIM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2126)
      .Column(GRID_COLUMN_TICKET_NOREDIM).Width = GRID_COLUMN_WIDTH_TICKET_NOREDIM

      .Column(GRID_COLUMN_BILLS_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2208)
      .Column(GRID_COLUMN_BILLS_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(795)
      .Column(GRID_COLUMN_BILLS_COUNT).Width = GRID_COLUMN_WIDTH_BILLS_COUNT

      .Column(GRID_COLUMN_BILLS_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2208)
      .Column(GRID_COLUMN_BILLS_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340)
      .Column(GRID_COLUMN_BILLS_TOTAL).Width = GRID_COLUMN_WIDTH_BILLS_TOTAL
    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Define all Grid Account Alias columns
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  Private Sub GUI_StyleSheetCashiers()

    With Me.dg_cashiers
      Call .Init(GRID_CASHIER_COLUMNS, GRID_CASHIER_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden colums
      .Column(GRID_CASHIER_ID).Header(1).Text = ""
      .Column(GRID_CASHIER_ID).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_CASHIER_COLUMN_CHECKED).Header(1).Text = ""
      .Column(GRID_CASHIER_COLUMN_CHECKED).WidthFixed = GRID_CASHIER_WIDTH_COLUMN_CHECKED
      .Column(GRID_CASHIER_COLUMN_CHECKED).Fixed = True
      .Column(GRID_CASHIER_COLUMN_CHECKED).Editable = True
      .Column(GRID_CASHIER_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_CASHIER_COLUMN_DESCRIPTION).Header(1).Text = ""
      .Column(GRID_CASHIER_COLUMN_DESCRIPTION).WidthFixed = GRID_CASHIER_WIDTH_COLUMN_DESCRIPTION
      .Column(GRID_CASHIER_COLUMN_DESCRIPTION).Fixed = True
      .Column(GRID_CASHIER_COLUMN_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub ' GUI_StyleSheetCashiers

  ' PURPOSE : Acumulates values for totals and SubTotals
  '
  '  PARAMS :
  '     - INPUT :
  '           - Type: type of Terminal/Cashier
  '           - DbRow: data of actual row
  '     - OUTPUT :
  '           - None
  '
  Private Sub SumValuesInArray(ByVal Type As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW)

    Dim _idx As Integer
    Dim _array(0) As Double
    Dim _array_totals(0) As Double

    _array = m_terminals_by_type(Type)

    If _array Is Nothing Then
      ReDim _array(GRID_COLUMNS_COUNT)
      m_terminals_by_type.Add(Type, _array)
    End If

    If Not DbRow Is Nothing Then
      _array_totals = m_terminals_by_type(m_terminal_type_totals)
      For _idx = GRID_COLUMN_TERM_REDIM To GRID_COLUMN_BILLS_TOTAL
        _array(_idx) += DbRow.Value(_idx)
        _array_totals(_idx) += DbRow.Value(_idx)
      Next _idx
    End If

  End Sub ' SumValuesInArray

  ' PURPOSE : Shows totals and SubTotals values in the grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - Type: type of Terminal/Cashier
  '           - RowIndex: index of the row-grid
  '     - OUTPUT :
  '           - None
  '
  Private Sub SumToGrid(ByVal Type As Integer, ByVal RowIndex As Integer)

    Dim _idx As Integer
    Dim _value As Object
    Dim _color As Integer
    Dim _array(0) As Double

    _array = m_terminals_by_type(Type)
    _color = IIf(Type = m_terminal_type_totals, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00, ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    For _idx = GRID_COLUMN_SELECT To GRID_COLUMN_BILLS_TOTAL
      Me.Grid.Cell(RowIndex, _idx).BackColor = GetColor(_color)
      If _idx >= GRID_COLUMN_TERM_REDIM Then
        _value = _array(_idx)
        Me.Grid.Cell(RowIndex, _idx).Value = GetFormatedValue(_idx, _value)
      End If
    Next

  End Sub ' SumToGrid

  ' PURPOSE : Return a condition to filter by ColumnName param
  '
  '  PARAMS :
  '     - INPUT :
  '           - ColumnName: name of BD column to filter by
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - Composed string for filter
  '
  Private Function GetSelectedTerminals(ByVal ColumnName As String) As String

    Dim _result As String
    Dim _terminals() As Long

    _result = ""

    _terminals = uc_pr_list.GetTerminalIdListSelected()

    If Not _terminals Is Nothing Then
      For Each _idx As Long In _terminals
        _result &= IIf(_result.Length > 0, ",", "")
        _result &= _idx.ToString()
      Next
      If Not String.IsNullOrEmpty(_result) Then
        _result = ColumnName & " IN (" & _result & ")"
      End If
    End If

    Return _result

  End Function ' GetSelectedTerminals

  ' PURPOSE: Add new filter item form export/print purpouses
  '
  '  PARAMS:
  '     - INPUT :
  '       - Label: string with label to print
  '       - Value: text value to print
  '
  Private Sub AddFilterItem(ByVal Label As String, ByVal Value As String)

    m_filter_params.Add(m_filter_params.Count, Label & ";" & Value)

  End Sub ' AddFilterItem

  ' PURPOSE: Initialize grid with Cashiers names
  '
  '  PARAMS:
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  Private Sub FillCashiersGrid()

    Dim _sb As StringBuilder
    Dim _actual_row As Int32
    Dim _adapter As SqlDataAdapter
    Dim _table As DataTable

    _adapter = New SqlDataAdapter()
    _table = New DataTable()
    _sb = New StringBuilder()

    Try
      Using _db_trx As DB_TRX = New DB_TRX()
        _sb.AppendLine("  SELECT   CT_CASHIER_ID")
        _sb.AppendLine("         , CT_NAME")
        _sb.AppendLine("    FROM   CASHIER_TERMINALS")
        _sb.AppendLine("   WHERE   CT_TERMINAL_ID IS NULL")
        _sb.AppendLine("ORDER BY   CT_NAME")

        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _adapter = New SqlDataAdapter(_cmd)
          _adapter.Fill(_table)

          _actual_row = 0

          For Each _row As DataRow In _table.Rows
            Me.dg_cashiers.AddRow()
            Me.dg_cashiers.Cell(_actual_row, 0).Value = _row(0)
            Me.dg_cashiers.Cell(_actual_row, 2).Value = _row(1)
            _actual_row += 1
          Next
        End Using
      End Using
    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
    End Try

  End Sub ' FillCashiersGrid

  ' PURPOSE: get selected cashiers/terminals from grids, for filter purpouses
  '
  '  PARAMS:
  '     - INPUT:
  '           - ColumnName: column for query construction
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - it could be a list of Ids or a list of alias
  Private Function GetSelectedCashiersOrTerminals(ByVal ColumnName As String) As String

    Dim _result As String
    Dim _idx_row As Integer
    Dim _id_list As String
    Dim _id_list_all As String

    ' Terminales
    _result = GetSelectedTerminals(ColumnName)

    ' Cashiers
    _id_list = ""
    _id_list_all = ""

    For _idx_row = 0 To dg_cashiers.NumRows - 1
      If dg_cashiers.Cell(_idx_row, GRID_CASHIER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        _id_list &= IIf(_id_list.Length > 0, ",", "")
        _id_list &= dg_cashiers.Cell(_idx_row, GRID_CASHIER_ID).Value
      End If
      _id_list_all &= IIf(_idx_row > 0, ",", "")
      _id_list_all &= dg_cashiers.Cell(_idx_row, GRID_CASHIER_ID).Value
    Next

    ' TODO: activar si se ve necesario filtrar s�lo por los cajeros 'visibles'
    '_id_list = IIf(String.IsNullOrEmpty(_id_list), _id_list_all, _id_list)

    If Not String.IsNullOrEmpty(_id_list) Then
      _id_list = ColumnName & " IN (" & _id_list & ")"
      If Not String.IsNullOrEmpty(_result) Then
        _result &= " OR "
      End If
      _result &= _id_list
    End If

    If Not String.IsNullOrEmpty(_result) Then
      _result = "(" & _result & ")"
    End If

    Return _result

  End Function ' GetSelectedCashiersOrTerminals

  ' PURPOSE: Check/Uncheck cashiers in theGrid
  '
  '  PARAMS:
  '     - INPUT :
  '       - Value: boolean to set in property Checked
  '
  '     - OUTPUT :
  '
  Private Sub CheckOrUncheckCashiers(ByVal Value As Boolean)

    Dim _idx_row As Integer

    For _idx_row = 0 To dg_cashiers.NumRows - 1
      dg_cashiers.Cell(_idx_row, GRID_CASHIER_COLUMN_CHECKED).Value = IIf(Value, uc_grid.GRID_CHK_CHECKED, uc_grid.GRID_CHK_UNCHECKED)
    Next

  End Sub ' CheckOrUncheckCashiers

  ' PURPOSE: return formated value to pu in grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxCol: index of column in the grid
  '           - Value: valur to be formated
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - A formated string or Empty string
  '
  Public Function GetFormatedValue(ByVal IdxCol As Integer, ByVal Value As Object) As String
    Dim _value As String = ""

    If Not DBNull.Value.Equals(Value) Then
      If IdxCol >= GRID_COLUMN_TERM_REDIM And IdxCol <> GRID_COLUMN_BILLS_COUNT Then
        _value = GUI_FormatCurrency(Value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Else
        _value = Value
      End If
    End If

    Return _value

  End Function ' GetFormatedValue

#End Region ' Private Functions and Methods

#Region " OVERRIDES "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CASH_TITO_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2407)
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION

    gb_cashiers.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(491)
    gb_all_or_actives.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2136)
    cb_only_active.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(255)
    btn_check_all.Text = GLB_NLS_GUI_ALARMS.GetString(452)
    btn_uncheck_all.Text = GLB_NLS_GUI_ALARMS.GetString(453)
    ' soluciona un problema de dimensionamiento gr�fico de los botones
    btn_check_all.Width = 120
    btn_check_all.Height = 25
    btn_uncheck_all.Width = 120
    btn_uncheck_all.Height = 25

    ' Hide/Show some controls
    GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False

    ' Define layout of all Main Grid Columns
    Call GUI_StyleSheet()

    ' Cashiers grid
    Call GUI_StyleSheetCashiers()
    Call FillCashiersGrid()

    Call uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())

    ' Set filter default values
    Call GUI_FilterReset()

    ' Date time an other filters
    uc_dates.ClosingTimeEnabled = False
    uc_dates.Init(GLB_NLS_GUI_PLAYER_TRACKING.Id(268))

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FilterReset()

    Dim _closing_time As Integer
    Dim _final_time As Date

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(Now.Year, Now.Month, Now.Day, _closing_time, 0, 0)
    _final_time = _final_time.AddDays(1)
    uc_dates.ToDate = _final_time.Date
    uc_dates.ToDateSelected = True
    uc_dates.FromDate = _final_time.AddDays(-2)
    uc_dates.FromDateSelected = True
    uc_dates.ClosingTime = _closing_time

    cb_only_active.Checked = True

    Call uc_pr_list.SetDefaultValues()

    CheckOrUncheckCashiers(False)

  End Sub ' GUI_FilterReset

  Protected Overrides Function GUI_FilterCheck() As Boolean

    Dim _result As Boolean
    Dim _terminals() As Long

    _result = True

    ' se controla el rango de fechas
    If uc_dates.FromDateSelected And uc_dates.ToDateSelected Then
      If uc_dates.FromDate > uc_dates.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        _result = False
      End If
    End If

    If _result Then
      ' al marcar varios, al menos debe haber un elemento seleccionado
      _terminals = uc_pr_list.GetTerminalIdListSelected()
      If uc_pr_list.opt_several_types.Checked And (_terminals Is Nothing) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1411), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        _result = False
      End If
    End If

    Return _result

  End Function 'GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_condition As String
    Dim _str_terminals As String
    Dim _str_sql_builder As StringBuilder

    _str_sql_builder = New StringBuilder()
    ' en juego (1)
    _str_sql_builder.AppendLine("   SELECT   '' NONE") ' para aprovechar los ciclos de escritura
    _str_sql_builder.AppendLine("          , CASE WHEN TI.TI_LAST_ACTION_TERMINAL_TYPE = 0 THEN CT.CT_NAME ELSE TE.TE_NAME END AS Name")
    _str_sql_builder.AppendLine("          , CASE WHEN TP.TT_REDEEMABLE = 1 THEN TI_AMOUNT ELSE 0 END AS AmountRedeem")
    _str_sql_builder.AppendLine("          , CASE WHEN TP.TT_REDEEMABLE = 0 THEN TI_AMOUNT ELSE 0 END AS AmountNoredeem")
    _str_sql_builder.AppendLine("          , '0' CirculantRedeem")
    _str_sql_builder.AppendLine("          , '0' CirculantNoredeem")
    _str_sql_builder.AppendLine("          , '0' CountBn")
    _str_sql_builder.AppendLine("          , '0' AmountBn")
    _str_sql_builder.AppendLine("          , TI.TI_STATUS Status")
    _str_sql_builder.AppendLine("          , TI.TI_LAST_ACTION_TERMINAL_ID TerminalId")
    _str_sql_builder.AppendLine("          , TI.TI_LAST_ACTION_TERMINAL_TYPE TerminalType")
    _str_sql_builder.AppendLine("     FROM   TICKETS TI")
    _str_sql_builder.AppendLine("LEFT JOIN   TICKET_TYPES TP ON (TI.TI_TYPE_ID = TT_TYPE_ID)")
    _str_sql_builder.AppendLine("LEFT JOIN   TERMINALS TE ON (TE_TERMINAL_ID = TI.TI_LAST_ACTION_TERMINAL_ID)")
    _str_sql_builder.AppendLine("LEFT JOIN   CASHIER_TERMINALS CT ON (CT_CASHIER_ID = TI.TI_LAST_ACTION_TERMINAL_ID)")
    _str_sql_builder.AppendLine("LEFT JOIN   MONEY_COLLECTIONS MC ON (MC_COLLECTION_ID = TI.TI_MONEY_COLLECTION_ID)")
    _str_sql_builder.AppendLine("    WHERE   TI.TI_STATUS IN (1)")

    If cb_only_active.Checked Then
      _str_sql_builder.AppendLine("    AND   (MC.MC_EXTRACTION_DATETIME IS NULL)")
    Else
      _str_condition = uc_dates.GetSqlFilterCondition("TI.TI_LAST_ACTION_DATETIME")
      If Not String.IsNullOrEmpty(_str_condition) Then
        _str_sql_builder.AppendLine("  AND   " & _str_condition)
      End If
    End If

    _str_terminals = GetSelectedTerminals("TI.TI_LAST_ACTION_TERMINAL_ID")
    If Not String.IsNullOrEmpty(_str_terminals) Then
      _str_sql_builder.AppendLine("    AND   " & _str_terminals)
    End If

    _str_sql_builder.AppendLine("UNION ALL")

    ' circulante (0)
    _str_sql_builder.AppendLine("   SELECT   '' NONE") ' para aprovechar los ciclos de escritura
    _str_sql_builder.AppendLine("          , CASE WHEN TI.TI_CREATED_TERMINAL_TYPE = 0 THEN CT.CT_NAME ELSE TE.TE_NAME END AS Name")
    _str_sql_builder.AppendLine("          , '0' AmountRedeem")
    _str_sql_builder.AppendLine("          , '0' AmountNoredeem")
    _str_sql_builder.AppendLine("          , CASE WHEN TP.TT_REDEEMABLE = 1 THEN TI_AMOUNT ELSE 0 END AS CirculantRedeem")
    _str_sql_builder.AppendLine("          , CASE WHEN TP.TT_REDEEMABLE = 0 THEN TI_AMOUNT ELSE 0 END AS CirculantNoredeem")
    _str_sql_builder.AppendLine("          , '0' CountBn")
    _str_sql_builder.AppendLine("          , '0' AmountBn")
    _str_sql_builder.AppendLine("          , TI.TI_STATUS Status")
    _str_sql_builder.AppendLine("          , TI.TI_CREATED_TERMINAL_ID TerminalId")
    _str_sql_builder.AppendLine("          , TI.TI_CREATED_TERMINAL_TYPE TerminalType")
    _str_sql_builder.AppendLine("     FROM   TICKETS TI")
    _str_sql_builder.AppendLine("LEFT JOIN   TICKET_TYPES TP ON (TI.TI_TYPE_ID = TT_TYPE_ID)")
    _str_sql_builder.AppendLine("LEFT JOIN   TERMINALS TE ON (TE_TERMINAL_ID = TI.TI_CREATED_TERMINAL_ID)")
    _str_sql_builder.AppendLine("LEFT JOIN   CASHIER_TERMINALS CT ON (CT_CASHIER_ID = TI.TI_CREATED_TERMINAL_ID)")
    _str_sql_builder.AppendLine("LEFT JOIN   MONEY_COLLECTIONS MC ON (MC_COLLECTION_ID = TI.TI_MONEY_COLLECTION_ID)")
    _str_sql_builder.AppendLine("    WHERE   TI.TI_STATUS IN (0)")

    If cb_only_active.Checked Then
      _str_sql_builder.AppendLine("    AND   (MC.MC_EXTRACTION_DATETIME IS NULL)")
    Else
      _str_condition = uc_dates.GetSqlFilterCondition("TI.TI_CREATED_DATETIME")

      If Not String.IsNullOrEmpty(_str_condition) Then
        _str_sql_builder.AppendLine("  AND   " & _str_condition)
      End If
    End If

    _str_terminals = GetSelectedCashiersOrTerminals("TI.TI_CREATED_TERMINAL_ID")

    If Not String.IsNullOrEmpty(_str_terminals) Then
      _str_sql_builder.AppendLine("    AND   " & _str_terminals)
    End If

    _str_sql_builder.AppendLine("UNION ALL")

    ' dinero en los terminales
    _str_sql_builder.AppendLine(" SELECT    '' NONE")
    _str_sql_builder.AppendLine("         , TE.TE_NAME Name ")
    _str_sql_builder.AppendLine("         , '0' AmountRedeem ")
    _str_sql_builder.AppendLine("         , '0' AmountNoredeem ")
    _str_sql_builder.AppendLine("         , '0' CirculantRedeem ")
    _str_sql_builder.AppendLine("         , '0' CirculantNoredeem ")
    _str_sql_builder.AppendLine("         , QUANTITY ")
    _str_sql_builder.AppendLine("         , (SELECT COUNT (tm_transaction_id) * TM.TM_AMOUNT) AmountBn ")
    _str_sql_builder.AppendLine("         , '99' Status ")
    _str_sql_builder.AppendLine("         , MC.MC_TERMINAL_ID TerminalId ")
    _str_sql_builder.AppendLine("         , TE.TE_TYPE TerminalType ")
    _str_sql_builder.AppendLine("   FROM    TERMINAL_MONEY TM ")
    _str_sql_builder.AppendLine("LEFT JOIN  MONEY_COLLECTIONS MC ON (MC_COLLECTION_ID = TM.TM_COLLECTION_ID)")
    _str_sql_builder.AppendLine("LEFT JOIN  TERMINALS TE ON (TE_TERMINAL_ID = MC.MC_TERMINAL_ID) ")
    _str_sql_builder.AppendLine("RIGHT JOIN (SELECT COUNT(*) QUANTITY,TA.TM_TERMINAL_ID,TA.TM_COLLECTION_ID,TA.TM_AMOUNT FROM TERMINAL_MONEY TA GROUP BY TA.TM_TERMINAL_ID,TA.TM_AMOUNT,TA.TM_COLLECTION_ID)  TCOUNTS ")
    _str_sql_builder.AppendLine("      ON   TM.TM_COLLECTION_ID = TCOUNTS.TM_COLLECTION_ID and TM.TM_TERMINAL_ID = TCOUNTS.TM_TERMINAL_ID and TM.TM_AMOUNT = TCOUNTS.TM_AMOUNT ")

    ' la condici�n significa que s�lo se quieren ver los datos 'at this time', es decir
    ' los que no han sido recaudados y/o extra�dos
    If cb_only_active.Checked Then
      _str_condition = "           WHERE   (MC.MC_EXTRACTION_DATETIME IS NULL)"
      _str_sql_builder.AppendLine(_str_condition)
    Else
      _str_condition = uc_dates.GetSqlFilterCondition("MC.MC_DATETIME")
      If Not String.IsNullOrEmpty(_str_condition) Then
        _str_sql_builder.AppendLine("    WHERE   (" & _str_condition & ")")
      End If
    End If

    _str_terminals = GetSelectedTerminals("MC.MC_TERMINAL_ID")

    If Not String.IsNullOrEmpty(_str_terminals) Then
      If String.IsNullOrEmpty(_str_condition) Then
        _str_sql_builder.AppendLine("    WHERE   " & _str_terminals)
      Else
        _str_sql_builder.AppendLine("      AND   " & _str_terminals)
      End If
    End If

    '_str_sql_builder.AppendLine(" GROUP BY   TM.TM_COLLECTION_ID, TM.TM_AMOUNT, TE_NAME,MC.MC_TERMINAL_ID,TE.TE_TYPE")
    _str_sql_builder.AppendLine("GROUP BY TM.TM_TERMINAL_ID, TM.TM_AMOUNT, TM.TM_COLLECTION_ID,TCOUNTS.QUANTITY,TE.TE_NAME,MC.MC_TERMINAL_ID,TE.TE_TYPE")

    _str_sql_builder.AppendLine(" ORDER BY   TerminalType ")
    _str_sql_builder.AppendLine("          , TerminalId")

    Return _str_sql_builder.ToString()

  End Function ' GUI_FilterGetSqlQuery

  Protected Overrides Sub GUI_BeforeFirstRow()

    m_terminals.Clear()
    m_prev_terminal_type = -1

    m_terminals_by_type.Clear()
    SumValuesInArray(m_terminal_type_totals, Nothing) ' aqu� estar� la suma Total

  End Sub ' GUI_BeforeFirstRow

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _idx_col As Integer
    Dim _value As Object
    Dim _terminal_name As String
    Dim _terminal_type As Integer
    Dim _idx_row As Integer
    Dim _is_clear_row As Boolean
    Dim _sum_by_terminal(GRID_COLUMNS_COUNT) As Double

    _terminal_type = DbRow.Value(SQL_COLUMN_TERMINAL_TYPE)

    Call SumValuesInArray(_terminal_type, DbRow)  ' se acumulan los Subtotales y Totales

    ' se averigua si hay que a�adir una fila de Subtotales
    If m_prev_terminal_type >= 0 And m_prev_terminal_type <> _terminal_type Then

      Call SumToGrid(m_prev_terminal_type, RowIndex)

      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046)
      Me.Grid.AddRow()
      RowIndex = Me.Grid.NumRows - 1
    End If

    m_prev_terminal_type = _terminal_type

    ' se averigua si el terminal/cajero ya existe en el grid, entonces se acumula en las columnas
    _is_clear_row = True
    _terminal_name = mdl_tito.GetValueForGrid(DbRow.Value(SQL_COLUMN_TERMINAL_NAME))

    If Not m_terminals.ContainsKey(_terminal_name) Then
      _idx_row = RowIndex
      _sum_by_terminal(GRID_COLUMN_SELECT) = _idx_row
      m_terminals.Add(_terminal_name, _sum_by_terminal)
    Else
      _sum_by_terminal = m_terminals(_terminal_name)
      _idx_row = _sum_by_terminal(GRID_COLUMN_SELECT)
      _is_clear_row = False
    End If

    ' por fin se escriben los datos de la fila
    For _idx_col = GRID_COLUMN_SELECT To GRID_COLUMN_BILLS_TOTAL
      _value = DbRow.Value(_idx_col)
      If _idx_col >= GRID_COLUMN_TERM_REDIM Then
        _value += _sum_by_terminal(_idx_col)
        _sum_by_terminal(_idx_col) = _value
      End If
      Me.Grid.Cell(_idx_row, _idx_col).Value = GetFormatedValue(_idx_col, _value)
    Next

    Return _is_clear_row

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_AfterLastRow()

    Dim _idx_row As Integer

    If m_prev_terminal_type >= 0 Then

      ' se a�aden filas de Subtotales
      Me.Grid.AddRow()
      _idx_row = Me.Grid.NumRows - 1

      Call SumToGrid(m_prev_terminal_type, _idx_row)

      Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046)

      ' se a�aden filas de Totales
      Me.Grid.AddRow()
      _idx_row = Me.Grid.NumRows - 1

      Call SumToGrid(m_terminal_type_totals, _idx_row)

      Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL_NAME).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1047)

    End If

  End Sub ' GUI_AfterLastRow

  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim _terminal As String
    Dim _date_temp As String
    Dim _selected_cashiers As String
    Dim _idx_row As Integer
    Dim _checked_counter As Integer

    m_filter_params.Clear()

    ' Dates
    _date_temp = IIf(uc_dates.FromDateSelected, mdl_tito.GetFormatedDate(uc_dates.FromDate.AddHours(uc_dates.ClosingTime)), "")
    Call AddFilterItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(297), _date_temp)
    _date_temp = IIf(uc_dates.ToDateSelected, mdl_tito.GetFormatedDate(uc_dates.ToDate.AddHours(uc_dates.ClosingTime)), "")
    Call AddFilterItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(298), _date_temp)
    Call AddFilterItem(gb_all_or_actives.Text, IIf(cb_only_active.Checked, cb_only_active.Text, ""))
    Call AddFilterItem("", "")
    Call AddFilterItem("", "")

    ' Cashiers
    _checked_counter = 0
    _selected_cashiers = ""

    If dg_cashiers.NumRows > 0 Then
      For _idx_row = 0 To dg_cashiers.NumRows - 1
        If dg_cashiers.Cell(_idx_row, GRID_CASHIER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
          _checked_counter += 1
        End If
      Next

      If _checked_counter > 0 And _checked_counter < dg_cashiers.NumRows - 1 Then
        _selected_cashiers = GLB_NLS_GUI_CONTROLS.GetString(291)
      ElseIf _checked_counter > 0 Then
        _selected_cashiers = GLB_NLS_GUI_ALARMS.GetString(277)
      End If
    End If

    Call AddFilterItem(GLB_NLS_GUI_ALARMS.GetString(217), _selected_cashiers)
    Call AddFilterItem("", "")
    Call AddFilterItem("", "")
    Call AddFilterItem("", "")
    Call AddFilterItem("", "")

    ' Terminals 
    If Me.uc_pr_list.opt_all_types.Checked Then
      _terminal = Me.uc_pr_list.opt_all_types.Text
    ElseIf Me.uc_pr_list.opt_several_types.Checked Then
      _terminal = Me.uc_pr_list.opt_several_types.Text
    Else
      _terminal = Me.uc_pr_list.cmb_terminal.TextValue
    End If

    Call AddFilterItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(592), _terminal)

  End Sub ' GUI_ReportUpdateFilters

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    Dim _text As String
    Dim _keys() As Integer
    Dim _values() As String

    ReDim _keys(m_filter_params.Count - 1)

    m_filter_params.Keys.CopyTo(_keys, 0)
    Array.Sort(_keys)

    For Each _key As Integer In _keys
      _text = m_filter_params(_key)
      _values = _text.Split(";")

      If String.IsNullOrEmpty(_values(0)) Then
        PrintData.SetFilter("", "", True)
      Else
        PrintData.SetFilter(_values(0), _values(1))
      End If
    Next

    PrintData.FilterHeaderWidth(1) = 2000
    PrintData.FilterHeaderWidth(2) = 2500
    PrintData.FilterHeaderWidth(3) = 2500
    PrintData.FilterValueWidth(2) = 2100
    PrintData.FilterValueWidth(4) = 2100

  End Sub ' GUI_ReportFilter

#End Region ' OVERRIDES

#Region " Public functions and methods "

  ' TODO: tbd
  Public Overloads Sub Show(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent

    Call GUI_Init()
    'TODO:
    'Me.Display(False)
    Call Show()

  End Sub ' Show

#End Region ' Public

#Region " Events "

  Private Sub btn_check_all_ClickEvent() Handles btn_check_all.ClickEvent
    Call CheckOrUncheckCashiers(True)
  End Sub

  Private Sub btn_uncheck_all_ClickEvent() Handles btn_uncheck_all.ClickEvent
    Call CheckOrUncheckCashiers(False)
  End Sub

#End Region ' Events

End Class