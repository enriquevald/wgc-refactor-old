<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_TITO_session_detail
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.gb_detail = New System.Windows.Forms.GroupBox
    Me.lbl_currency_exchange_amount = New System.Windows.Forms.Label
    Me.Panel9 = New System.Windows.Forms.Panel
    Me.lbl_liabilities_total_increment_value = New System.Windows.Forms.Label
    Me.lbl_liabilities_total_final_value = New System.Windows.Forms.Label
    Me.lbl_liabilities_total_initial_value = New System.Windows.Forms.Label
    Me.lbl_liabilities_increment_promo_nr_value = New System.Windows.Forms.Label
    Me.lbl_liabilities_increment_promo_re_value = New System.Windows.Forms.Label
    Me.lbl_liabilities_increment_re_value = New System.Windows.Forms.Label
    Me.lbl_liabilities_final_promo_nr_value = New System.Windows.Forms.Label
    Me.lbl_liabilities_final_promo_re_value = New System.Windows.Forms.Label
    Me.lbl_liabilities_final_re_value = New System.Windows.Forms.Label
    Me.lbl_liabilities_initial_promo_nr_value = New System.Windows.Forms.Label
    Me.lbl_liabilities_initial_promo_re_value = New System.Windows.Forms.Label
    Me.lbl_liabilities_initial_re_value = New System.Windows.Forms.Label
    Me.lbl_liabilities_increment_name = New System.Windows.Forms.Label
    Me.lbl_liabilities_final_name = New System.Windows.Forms.Label
    Me.lbl_liabilities_initial_name = New System.Windows.Forms.Label
    Me.Label14 = New System.Windows.Forms.Label
    Me.lbl_liabilities_title_name = New System.Windows.Forms.Label
    Me.lbl_liabilities_total_name = New System.Windows.Forms.Label
    Me.lbl_liabilities_promo_nr_name = New System.Windows.Forms.Label
    Me.lbl_liabilities_promo_re_name = New System.Windows.Forms.Label
    Me.lbl_liabilities_re_name = New System.Windows.Forms.Label
    Me.lbl_currency_exchange_name = New System.Windows.Forms.Label
    Me.Panel8 = New System.Windows.Forms.Panel
    Me.lbl_tax_on_prize_1_value = New System.Windows.Forms.Label
    Me.lbl_tax_on_prize_1_name = New System.Windows.Forms.Label
    Me.lbl_taxes_amount_3 = New System.Windows.Forms.Label
    Me.lbl_tax_on_prize_2_value = New System.Windows.Forms.Label
    Me.Label12 = New System.Windows.Forms.Label
    Me.lbl_total_taxes_name_2 = New System.Windows.Forms.Label
    Me.lbl_taxes_title = New System.Windows.Forms.Label
    Me.lbl_tax_on_prize_2_name = New System.Windows.Forms.Label
    Me.lbl_acceptors_deposit_value = New System.Windows.Forms.Label
    Me.lbl_acceptors_deposit_name = New System.Windows.Forms.Label
    Me.panel7 = New System.Windows.Forms.Panel
    Me.lbl_cancel_promo_nr_amount = New System.Windows.Forms.Label
    Me.lbl_cancel_promo_re_amount = New System.Windows.Forms.Label
    Me.lbl_promo_re_amount = New System.Windows.Forms.Label
    Me.lbl_nr_to_re_amount = New System.Windows.Forms.Label
    Me.lbl_promo_nr_amount = New System.Windows.Forms.Label
    Me.lbl_handpay_total = New System.Windows.Forms.Label
    Me.lbl_handpays_amount = New System.Windows.Forms.Label
    Me.lbl_handpay_cancellations_amount = New System.Windows.Forms.Label
    Me.lbl_cancel_promo_nr_name = New System.Windows.Forms.Label
    Me.lbl_cancel_promo_re_name = New System.Windows.Forms.Label
    Me.lbl_promo_re_name = New System.Windows.Forms.Label
    Me.lbl_promo_nr_name = New System.Windows.Forms.Label
    Me.lbl_nr_to_re_name = New System.Windows.Forms.Label
    Me.label5 = New System.Windows.Forms.Label
    Me.lbl_total_3_name = New System.Windows.Forms.Label
    Me.lbl_handypays_name = New System.Windows.Forms.Label
    Me.lbl_handpays_payment_name = New System.Windows.Forms.Label
    Me.lbl_handpay_cancellations_name = New System.Windows.Forms.Label
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.lbl_expiration_dev_name = New System.Windows.Forms.Label
    Me.lbl_expiration_dev_amount = New System.Windows.Forms.Label
    Me.lbl_expiration_nr_name = New System.Windows.Forms.Label
    Me.lbl_expiration_prize_name = New System.Windows.Forms.Label
    Me.lbl_expiration_nr_amount = New System.Windows.Forms.Label
    Me.lbl_expiration_prize_amount = New System.Windows.Forms.Label
    Me.lbl_expirations_name = New System.Windows.Forms.Label
    Me.panel6 = New System.Windows.Forms.Panel
    Me.Label1 = New System.Windows.Forms.Label
    Me.lbl_cash_in_total_3 = New System.Windows.Forms.Label
    Me.lbl_cash_out_total_3 = New System.Windows.Forms.Label
    Me.lbl_final_balance = New System.Windows.Forms.Label
    Me.lbl_taxes_amount = New System.Windows.Forms.Label
    Me.lbl_entry_name_3 = New System.Windows.Forms.Label
    Me.lbl_exits_name_3 = New System.Windows.Forms.Label
    Me.lbl_taxes_name = New System.Windows.Forms.Label
    Me.lbl_total_name_2 = New System.Windows.Forms.Label
    Me.panel2 = New System.Windows.Forms.Panel
    Me.lbl_credit_card_payment_amount = New System.Windows.Forms.Label
    Me.lbl_credit_card_payment_name = New System.Windows.Forms.Label
    Me.lbl_balance_total_currency_value = New System.Windows.Forms.Label
    Me.label2 = New System.Windows.Forms.Label
    Me.lbl_check_payments_amount = New System.Windows.Forms.Label
    Me.lbl_cash_out_total_2 = New System.Windows.Forms.Label
    Me.lbl_check_payments_name = New System.Windows.Forms.Label
    Me.lbl_cash_in_total_2 = New System.Windows.Forms.Label
    Me.lbl_balance_total_value_2 = New System.Windows.Forms.Label
    Me.lbl_deposit_amount = New System.Windows.Forms.Label
    Me.lbl_pending_amount = New System.Windows.Forms.Label
    Me.lbl_withdraw_amount = New System.Windows.Forms.Label
    Me.lbl_balance_total_name_2 = New System.Windows.Forms.Label
    Me.lbl_exits_name_2 = New System.Windows.Forms.Label
    Me.lbl_pending_amount_name = New System.Windows.Forms.Label
    Me.lbl_entry_name_2 = New System.Windows.Forms.Label
    Me.lbl_deposit_name = New System.Windows.Forms.Label
    Me.lbl_withdraw_name = New System.Windows.Forms.Label
    Me.panel5 = New System.Windows.Forms.Panel
    Me.lbl_b_total_dev = New System.Windows.Forms.Label
    Me.lbl_decimal_rounding = New System.Windows.Forms.Label
    Me.lbl_decimal_rounding_name = New System.Windows.Forms.Label
    Me.lbl_refund_card_usage = New System.Windows.Forms.Label
    Me.lbl_a_total_dev = New System.Windows.Forms.Label
    Me.lbl_cash_out_total = New System.Windows.Forms.Label
    Me.lbl_prizes_amount = New System.Windows.Forms.Label
    Me.label4 = New System.Windows.Forms.Label
    Me.lbl_refund_card_usage_name = New System.Windows.Forms.Label
    Me.lbl_a_total_dev_name = New System.Windows.Forms.Label
    Me.lbl_b_total_dev_name = New System.Windows.Forms.Label
    Me.lbl_exits_name = New System.Windows.Forms.Label
    Me.lbl_total_1 = New System.Windows.Forms.Label
    Me.lbl_total_prizes_name = New System.Windows.Forms.Label
    Me.panel4 = New System.Windows.Forms.Panel
    Me.lbl_commission_value = New System.Windows.Forms.Label
    Me.lbl_commission_name = New System.Windows.Forms.Label
    Me.lbl_b_total_in = New System.Windows.Forms.Label
    Me.lbl_service_charge = New System.Windows.Forms.Label
    Me.lbl_service_charge_name = New System.Windows.Forms.Label
    Me.lbl_total_in_total = New System.Windows.Forms.Label
    Me.lbl_card_usage = New System.Windows.Forms.Label
    Me.lbl_a_total_in = New System.Windows.Forms.Label
    Me.label3 = New System.Windows.Forms.Label
    Me.lbl_entry_name = New System.Windows.Forms.Label
    Me.lbl_total_0 = New System.Windows.Forms.Label
    Me.lbl_card_usage_name = New System.Windows.Forms.Label
    Me.lbl_b_total_in_name = New System.Windows.Forms.Label
    Me.lbl_a_total_in_name = New System.Windows.Forms.Label
    Me.panel3 = New System.Windows.Forms.Panel
    Me.lbl_tax_return_value = New System.Windows.Forms.Label
    Me.lbl_tax_return_name = New System.Windows.Forms.Label
    Me.lbl_prizes_2 = New System.Windows.Forms.Label
    Me.lbl_prizes_gross = New System.Windows.Forms.Label
    Me.lbl_taxes_amount_2 = New System.Windows.Forms.Label
    Me.label6 = New System.Windows.Forms.Label
    Me.lbl_total_prizes1_name = New System.Windows.Forms.Label
    Me.lbl_total_prizes_gross_name = New System.Windows.Forms.Label
    Me.lbl_total_taxes_name = New System.Windows.Forms.Label
    Me.gb_header = New System.Windows.Forms.GroupBox
    Me.tf_cm_to_date = New GUI_Controls.uc_text_field
    Me.tf_cm_from_date = New GUI_Controls.uc_text_field
    Me.tf_cs_current_balance = New GUI_Controls.uc_text_field
    Me.tf_cs_close_date = New GUI_Controls.uc_text_field
    Me.tf_cs_open_date = New GUI_Controls.uc_text_field
    Me.tf_cs_user_name = New GUI_Controls.uc_text_field
    Me.tf_cs_cash_desk_name = New GUI_Controls.uc_text_field
    Me.tf_cs_status = New GUI_Controls.uc_text_field
    Me.lbl_cs_name = New GUI_Controls.uc_text_field
    Me.panel_data.SuspendLayout()
    Me.gb_detail.SuspendLayout()
    Me.Panel9.SuspendLayout()
    Me.Panel8.SuspendLayout()
    Me.panel7.SuspendLayout()
    Me.Panel1.SuspendLayout()
    Me.panel6.SuspendLayout()
    Me.panel2.SuspendLayout()
    Me.panel5.SuspendLayout()
    Me.panel4.SuspendLayout()
    Me.panel3.SuspendLayout()
    Me.gb_header.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.panel_data.Controls.Add(Me.gb_detail)
    Me.panel_data.Controls.Add(Me.gb_header)
    Me.panel_data.Size = New System.Drawing.Size(784, 749)
    '
    'gb_detail
    '
    Me.gb_detail.Controls.Add(Me.lbl_currency_exchange_amount)
    Me.gb_detail.Controls.Add(Me.Panel9)
    Me.gb_detail.Controls.Add(Me.lbl_currency_exchange_name)
    Me.gb_detail.Controls.Add(Me.Panel8)
    Me.gb_detail.Controls.Add(Me.lbl_acceptors_deposit_value)
    Me.gb_detail.Controls.Add(Me.lbl_acceptors_deposit_name)
    Me.gb_detail.Controls.Add(Me.panel7)
    Me.gb_detail.Controls.Add(Me.Panel1)
    Me.gb_detail.Controls.Add(Me.panel6)
    Me.gb_detail.Controls.Add(Me.panel2)
    Me.gb_detail.Controls.Add(Me.panel5)
    Me.gb_detail.Controls.Add(Me.panel4)
    Me.gb_detail.Controls.Add(Me.panel3)
    Me.gb_detail.Location = New System.Drawing.Point(3, 139)
    Me.gb_detail.Name = "gb_detail"
    Me.gb_detail.Size = New System.Drawing.Size(781, 597)
    Me.gb_detail.TabIndex = 11
    Me.gb_detail.TabStop = False
    '
    'lbl_currency_exchange_amount
    '
    Me.lbl_currency_exchange_amount.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_currency_exchange_amount.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_currency_exchange_amount.ForeColor = System.Drawing.Color.Black
    Me.lbl_currency_exchange_amount.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_currency_exchange_amount.Location = New System.Drawing.Point(203, 549)
    Me.lbl_currency_exchange_amount.Name = "lbl_currency_exchange_amount"
    Me.lbl_currency_exchange_amount.Size = New System.Drawing.Size(167, 16)
    Me.lbl_currency_exchange_amount.TabIndex = 215
    Me.lbl_currency_exchange_amount.Text = "xxxxxxx.xx"
    Me.lbl_currency_exchange_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_currency_exchange_amount.Visible = False
    '
    'Panel9
    '
    Me.Panel9.Controls.Add(Me.lbl_liabilities_total_increment_value)
    Me.Panel9.Controls.Add(Me.lbl_liabilities_total_final_value)
    Me.Panel9.Controls.Add(Me.lbl_liabilities_total_initial_value)
    Me.Panel9.Controls.Add(Me.lbl_liabilities_increment_promo_nr_value)
    Me.Panel9.Controls.Add(Me.lbl_liabilities_increment_promo_re_value)
    Me.Panel9.Controls.Add(Me.lbl_liabilities_increment_re_value)
    Me.Panel9.Controls.Add(Me.lbl_liabilities_final_promo_nr_value)
    Me.Panel9.Controls.Add(Me.lbl_liabilities_final_promo_re_value)
    Me.Panel9.Controls.Add(Me.lbl_liabilities_final_re_value)
    Me.Panel9.Controls.Add(Me.lbl_liabilities_initial_promo_nr_value)
    Me.Panel9.Controls.Add(Me.lbl_liabilities_initial_promo_re_value)
    Me.Panel9.Controls.Add(Me.lbl_liabilities_initial_re_value)
    Me.Panel9.Controls.Add(Me.lbl_liabilities_increment_name)
    Me.Panel9.Controls.Add(Me.lbl_liabilities_final_name)
    Me.Panel9.Controls.Add(Me.lbl_liabilities_initial_name)
    Me.Panel9.Controls.Add(Me.Label14)
    Me.Panel9.Controls.Add(Me.lbl_liabilities_title_name)
    Me.Panel9.Controls.Add(Me.lbl_liabilities_total_name)
    Me.Panel9.Controls.Add(Me.lbl_liabilities_promo_nr_name)
    Me.Panel9.Controls.Add(Me.lbl_liabilities_promo_re_name)
    Me.Panel9.Controls.Add(Me.lbl_liabilities_re_name)
    Me.Panel9.Location = New System.Drawing.Point(393, 465)
    Me.Panel9.Name = "Panel9"
    Me.Panel9.Size = New System.Drawing.Size(380, 103)
    Me.Panel9.TabIndex = 186
    Me.Panel9.Visible = False
    '
    'lbl_liabilities_total_increment_value
    '
    Me.lbl_liabilities_total_increment_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_liabilities_total_increment_value.ForeColor = System.Drawing.Color.Black
    Me.lbl_liabilities_total_increment_value.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_liabilities_total_increment_value.Location = New System.Drawing.Point(286, 85)
    Me.lbl_liabilities_total_increment_value.Name = "lbl_liabilities_total_increment_value"
    Me.lbl_liabilities_total_increment_value.Size = New System.Drawing.Size(86, 16)
    Me.lbl_liabilities_total_increment_value.TabIndex = 185
    Me.lbl_liabilities_total_increment_value.Text = "xxxxxxx.xx"
    Me.lbl_liabilities_total_increment_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_liabilities_total_final_value
    '
    Me.lbl_liabilities_total_final_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_liabilities_total_final_value.ForeColor = System.Drawing.Color.Black
    Me.lbl_liabilities_total_final_value.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_liabilities_total_final_value.Location = New System.Drawing.Point(181, 85)
    Me.lbl_liabilities_total_final_value.Name = "lbl_liabilities_total_final_value"
    Me.lbl_liabilities_total_final_value.Size = New System.Drawing.Size(94, 16)
    Me.lbl_liabilities_total_final_value.TabIndex = 184
    Me.lbl_liabilities_total_final_value.Text = "xxxxxxx.xx"
    Me.lbl_liabilities_total_final_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_liabilities_total_initial_value
    '
    Me.lbl_liabilities_total_initial_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_liabilities_total_initial_value.ForeColor = System.Drawing.Color.Black
    Me.lbl_liabilities_total_initial_value.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_liabilities_total_initial_value.Location = New System.Drawing.Point(73, 85)
    Me.lbl_liabilities_total_initial_value.Name = "lbl_liabilities_total_initial_value"
    Me.lbl_liabilities_total_initial_value.Size = New System.Drawing.Size(98, 16)
    Me.lbl_liabilities_total_initial_value.TabIndex = 183
    Me.lbl_liabilities_total_initial_value.Text = "xxxxxxx.xx"
    Me.lbl_liabilities_total_initial_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_liabilities_increment_promo_nr_value
    '
    Me.lbl_liabilities_increment_promo_nr_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_liabilities_increment_promo_nr_value.ForeColor = System.Drawing.Color.Black
    Me.lbl_liabilities_increment_promo_nr_value.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_liabilities_increment_promo_nr_value.Location = New System.Drawing.Point(283, 62)
    Me.lbl_liabilities_increment_promo_nr_value.Name = "lbl_liabilities_increment_promo_nr_value"
    Me.lbl_liabilities_increment_promo_nr_value.Size = New System.Drawing.Size(88, 16)
    Me.lbl_liabilities_increment_promo_nr_value.TabIndex = 182
    Me.lbl_liabilities_increment_promo_nr_value.Text = "xxxxxxx.xx"
    Me.lbl_liabilities_increment_promo_nr_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_liabilities_increment_promo_re_value
    '
    Me.lbl_liabilities_increment_promo_re_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_liabilities_increment_promo_re_value.ForeColor = System.Drawing.Color.Black
    Me.lbl_liabilities_increment_promo_re_value.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_liabilities_increment_promo_re_value.Location = New System.Drawing.Point(283, 42)
    Me.lbl_liabilities_increment_promo_re_value.Name = "lbl_liabilities_increment_promo_re_value"
    Me.lbl_liabilities_increment_promo_re_value.Size = New System.Drawing.Size(88, 16)
    Me.lbl_liabilities_increment_promo_re_value.TabIndex = 181
    Me.lbl_liabilities_increment_promo_re_value.Text = "xxxxxxx.xx"
    Me.lbl_liabilities_increment_promo_re_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_liabilities_increment_re_value
    '
    Me.lbl_liabilities_increment_re_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_liabilities_increment_re_value.ForeColor = System.Drawing.Color.Black
    Me.lbl_liabilities_increment_re_value.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_liabilities_increment_re_value.Location = New System.Drawing.Point(280, 23)
    Me.lbl_liabilities_increment_re_value.Name = "lbl_liabilities_increment_re_value"
    Me.lbl_liabilities_increment_re_value.Size = New System.Drawing.Size(91, 16)
    Me.lbl_liabilities_increment_re_value.TabIndex = 180
    Me.lbl_liabilities_increment_re_value.Text = "xxxxxxx.xx"
    Me.lbl_liabilities_increment_re_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_liabilities_final_promo_nr_value
    '
    Me.lbl_liabilities_final_promo_nr_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_liabilities_final_promo_nr_value.ForeColor = System.Drawing.Color.Black
    Me.lbl_liabilities_final_promo_nr_value.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_liabilities_final_promo_nr_value.Location = New System.Drawing.Point(181, 60)
    Me.lbl_liabilities_final_promo_nr_value.Name = "lbl_liabilities_final_promo_nr_value"
    Me.lbl_liabilities_final_promo_nr_value.Size = New System.Drawing.Size(93, 20)
    Me.lbl_liabilities_final_promo_nr_value.TabIndex = 179
    Me.lbl_liabilities_final_promo_nr_value.Text = "xxxxxxx.xx"
    Me.lbl_liabilities_final_promo_nr_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_liabilities_final_promo_re_value
    '
    Me.lbl_liabilities_final_promo_re_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_liabilities_final_promo_re_value.ForeColor = System.Drawing.Color.Black
    Me.lbl_liabilities_final_promo_re_value.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_liabilities_final_promo_re_value.Location = New System.Drawing.Point(178, 40)
    Me.lbl_liabilities_final_promo_re_value.Name = "lbl_liabilities_final_promo_re_value"
    Me.lbl_liabilities_final_promo_re_value.Size = New System.Drawing.Size(96, 20)
    Me.lbl_liabilities_final_promo_re_value.TabIndex = 178
    Me.lbl_liabilities_final_promo_re_value.Text = "xxxxxxx.xx"
    Me.lbl_liabilities_final_promo_re_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_liabilities_final_re_value
    '
    Me.lbl_liabilities_final_re_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_liabilities_final_re_value.ForeColor = System.Drawing.Color.Black
    Me.lbl_liabilities_final_re_value.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_liabilities_final_re_value.Location = New System.Drawing.Point(178, 21)
    Me.lbl_liabilities_final_re_value.Name = "lbl_liabilities_final_re_value"
    Me.lbl_liabilities_final_re_value.Size = New System.Drawing.Size(96, 20)
    Me.lbl_liabilities_final_re_value.TabIndex = 177
    Me.lbl_liabilities_final_re_value.Text = "xxxxxxx.xx"
    Me.lbl_liabilities_final_re_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_liabilities_initial_promo_nr_value
    '
    Me.lbl_liabilities_initial_promo_nr_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_liabilities_initial_promo_nr_value.ForeColor = System.Drawing.Color.Black
    Me.lbl_liabilities_initial_promo_nr_value.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_liabilities_initial_promo_nr_value.Location = New System.Drawing.Point(76, 62)
    Me.lbl_liabilities_initial_promo_nr_value.Name = "lbl_liabilities_initial_promo_nr_value"
    Me.lbl_liabilities_initial_promo_nr_value.Size = New System.Drawing.Size(94, 16)
    Me.lbl_liabilities_initial_promo_nr_value.TabIndex = 176
    Me.lbl_liabilities_initial_promo_nr_value.Text = "xxxxxxx.xx"
    Me.lbl_liabilities_initial_promo_nr_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_liabilities_initial_promo_re_value
    '
    Me.lbl_liabilities_initial_promo_re_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_liabilities_initial_promo_re_value.ForeColor = System.Drawing.Color.Black
    Me.lbl_liabilities_initial_promo_re_value.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_liabilities_initial_promo_re_value.Location = New System.Drawing.Point(73, 42)
    Me.lbl_liabilities_initial_promo_re_value.Name = "lbl_liabilities_initial_promo_re_value"
    Me.lbl_liabilities_initial_promo_re_value.Size = New System.Drawing.Size(97, 16)
    Me.lbl_liabilities_initial_promo_re_value.TabIndex = 175
    Me.lbl_liabilities_initial_promo_re_value.Text = "xxxxxxx.xx"
    Me.lbl_liabilities_initial_promo_re_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_liabilities_initial_re_value
    '
    Me.lbl_liabilities_initial_re_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_liabilities_initial_re_value.ForeColor = System.Drawing.Color.Black
    Me.lbl_liabilities_initial_re_value.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_liabilities_initial_re_value.Location = New System.Drawing.Point(70, 23)
    Me.lbl_liabilities_initial_re_value.Name = "lbl_liabilities_initial_re_value"
    Me.lbl_liabilities_initial_re_value.Size = New System.Drawing.Size(100, 16)
    Me.lbl_liabilities_initial_re_value.TabIndex = 174
    Me.lbl_liabilities_initial_re_value.Text = "xxxxxxx.xx"
    Me.lbl_liabilities_initial_re_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_liabilities_increment_name
    '
    Me.lbl_liabilities_increment_name.AutoSize = True
    Me.lbl_liabilities_increment_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_liabilities_increment_name.Location = New System.Drawing.Point(312, 5)
    Me.lbl_liabilities_increment_name.Name = "lbl_liabilities_increment_name"
    Me.lbl_liabilities_increment_name.Size = New System.Drawing.Size(65, 13)
    Me.lbl_liabilities_increment_name.TabIndex = 161
    Me.lbl_liabilities_increment_name.Text = "xIncremento"
    Me.lbl_liabilities_increment_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_liabilities_final_name
    '
    Me.lbl_liabilities_final_name.AutoSize = True
    Me.lbl_liabilities_final_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_liabilities_final_name.Location = New System.Drawing.Point(246, 5)
    Me.lbl_liabilities_final_name.Name = "lbl_liabilities_final_name"
    Me.lbl_liabilities_final_name.Size = New System.Drawing.Size(34, 13)
    Me.lbl_liabilities_final_name.TabIndex = 160
    Me.lbl_liabilities_final_name.Text = "xFinal"
    Me.lbl_liabilities_final_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_liabilities_initial_name
    '
    Me.lbl_liabilities_initial_name.AutoSize = True
    Me.lbl_liabilities_initial_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_liabilities_initial_name.Location = New System.Drawing.Point(137, 5)
    Me.lbl_liabilities_initial_name.Name = "lbl_liabilities_initial_name"
    Me.lbl_liabilities_initial_name.Size = New System.Drawing.Size(39, 13)
    Me.lbl_liabilities_initial_name.TabIndex = 159
    Me.lbl_liabilities_initial_name.Text = "xInicial"
    Me.lbl_liabilities_initial_name.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label14
    '
    Me.Label14.BackColor = System.Drawing.Color.Black
    Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
    Me.Label14.Location = New System.Drawing.Point(2, 82)
    Me.Label14.Name = "Label14"
    Me.Label14.Size = New System.Drawing.Size(373, 1)
    Me.Label14.TabIndex = 158
    Me.Label14.Text = "0,00"
    Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_liabilities_title_name
    '
    Me.lbl_liabilities_title_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_liabilities_title_name.Location = New System.Drawing.Point(5, 3)
    Me.lbl_liabilities_title_name.Name = "lbl_liabilities_title_name"
    Me.lbl_liabilities_title_name.Size = New System.Drawing.Size(87, 16)
    Me.lbl_liabilities_title_name.TabIndex = 135
    Me.lbl_liabilities_title_name.Text = "xPasivo"
    Me.lbl_liabilities_title_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_liabilities_total_name
    '
    Me.lbl_liabilities_total_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_liabilities_total_name.Location = New System.Drawing.Point(5, 85)
    Me.lbl_liabilities_total_name.Name = "lbl_liabilities_total_name"
    Me.lbl_liabilities_total_name.Size = New System.Drawing.Size(196, 16)
    Me.lbl_liabilities_total_name.TabIndex = 134
    Me.lbl_liabilities_total_name.Text = "xTotal"
    Me.lbl_liabilities_total_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_liabilities_promo_nr_name
    '
    Me.lbl_liabilities_promo_nr_name.AutoSize = True
    Me.lbl_liabilities_promo_nr_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_liabilities_promo_nr_name.Location = New System.Drawing.Point(13, 63)
    Me.lbl_liabilities_promo_nr_name.Name = "lbl_liabilities_promo_nr_name"
    Me.lbl_liabilities_promo_nr_name.Size = New System.Drawing.Size(61, 13)
    Me.lbl_liabilities_promo_nr_name.TabIndex = 130
    Me.lbl_liabilities_promo_nr_name.Text = "xPromo NR"
    Me.lbl_liabilities_promo_nr_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_liabilities_promo_re_name
    '
    Me.lbl_liabilities_promo_re_name.AutoSize = True
    Me.lbl_liabilities_promo_re_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_liabilities_promo_re_name.Location = New System.Drawing.Point(13, 43)
    Me.lbl_liabilities_promo_re_name.Name = "lbl_liabilities_promo_re_name"
    Me.lbl_liabilities_promo_re_name.Size = New System.Drawing.Size(60, 13)
    Me.lbl_liabilities_promo_re_name.TabIndex = 118
    Me.lbl_liabilities_promo_re_name.Text = "xPromo RE"
    Me.lbl_liabilities_promo_re_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_liabilities_re_name
    '
    Me.lbl_liabilities_re_name.AutoSize = True
    Me.lbl_liabilities_re_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_liabilities_re_name.Location = New System.Drawing.Point(13, 23)
    Me.lbl_liabilities_re_name.Name = "lbl_liabilities_re_name"
    Me.lbl_liabilities_re_name.Size = New System.Drawing.Size(58, 13)
    Me.lbl_liabilities_re_name.TabIndex = 114
    Me.lbl_liabilities_re_name.Text = "xRedimible"
    Me.lbl_liabilities_re_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_currency_exchange_name
    '
    Me.lbl_currency_exchange_name.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_currency_exchange_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_currency_exchange_name.Location = New System.Drawing.Point(11, 549)
    Me.lbl_currency_exchange_name.Name = "lbl_currency_exchange_name"
    Me.lbl_currency_exchange_name.Size = New System.Drawing.Size(229, 16)
    Me.lbl_currency_exchange_name.TabIndex = 214
    Me.lbl_currency_exchange_name.Text = "xCambio divisa"
    Me.lbl_currency_exchange_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.lbl_currency_exchange_name.Visible = False
    '
    'Panel8
    '
    Me.Panel8.Controls.Add(Me.lbl_tax_on_prize_1_value)
    Me.Panel8.Controls.Add(Me.lbl_tax_on_prize_1_name)
    Me.Panel8.Controls.Add(Me.lbl_taxes_amount_3)
    Me.Panel8.Controls.Add(Me.lbl_tax_on_prize_2_value)
    Me.Panel8.Controls.Add(Me.Label12)
    Me.Panel8.Controls.Add(Me.lbl_total_taxes_name_2)
    Me.Panel8.Controls.Add(Me.lbl_taxes_title)
    Me.Panel8.Controls.Add(Me.lbl_tax_on_prize_2_name)
    Me.Panel8.Location = New System.Drawing.Point(6, 354)
    Me.Panel8.Name = "Panel8"
    Me.Panel8.Size = New System.Drawing.Size(380, 69)
    Me.Panel8.TabIndex = 185
    '
    'lbl_tax_on_prize_1_value
    '
    Me.lbl_tax_on_prize_1_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_tax_on_prize_1_value.ForeColor = System.Drawing.Color.Black
    Me.lbl_tax_on_prize_1_value.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_tax_on_prize_1_value.Location = New System.Drawing.Point(224, 13)
    Me.lbl_tax_on_prize_1_value.Name = "lbl_tax_on_prize_1_value"
    Me.lbl_tax_on_prize_1_value.Size = New System.Drawing.Size(144, 16)
    Me.lbl_tax_on_prize_1_value.TabIndex = 173
    Me.lbl_tax_on_prize_1_value.Text = "xxxxxxx.xx"
    Me.lbl_tax_on_prize_1_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_tax_on_prize_1_name
    '
    Me.lbl_tax_on_prize_1_name.AutoSize = True
    Me.lbl_tax_on_prize_1_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_tax_on_prize_1_name.Location = New System.Drawing.Point(23, 16)
    Me.lbl_tax_on_prize_1_name.Name = "lbl_tax_on_prize_1_name"
    Me.lbl_tax_on_prize_1_name.Size = New System.Drawing.Size(47, 13)
    Me.lbl_tax_on_prize_1_name.TabIndex = 172
    Me.lbl_tax_on_prize_1_name.Text = "xFederal"
    Me.lbl_tax_on_prize_1_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_taxes_amount_3
    '
    Me.lbl_taxes_amount_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_taxes_amount_3.ForeColor = System.Drawing.Color.Black
    Me.lbl_taxes_amount_3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_taxes_amount_3.Location = New System.Drawing.Point(221, 52)
    Me.lbl_taxes_amount_3.Name = "lbl_taxes_amount_3"
    Me.lbl_taxes_amount_3.Size = New System.Drawing.Size(147, 16)
    Me.lbl_taxes_amount_3.TabIndex = 155
    Me.lbl_taxes_amount_3.Text = "xxxxxxx.xx"
    Me.lbl_taxes_amount_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_tax_on_prize_2_value
    '
    Me.lbl_tax_on_prize_2_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_tax_on_prize_2_value.ForeColor = System.Drawing.Color.Black
    Me.lbl_tax_on_prize_2_value.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_tax_on_prize_2_value.Location = New System.Drawing.Point(224, 28)
    Me.lbl_tax_on_prize_2_value.Name = "lbl_tax_on_prize_2_value"
    Me.lbl_tax_on_prize_2_value.Size = New System.Drawing.Size(144, 16)
    Me.lbl_tax_on_prize_2_value.TabIndex = 153
    Me.lbl_tax_on_prize_2_value.Text = "xxxxxxx.xx"
    Me.lbl_tax_on_prize_2_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label12
    '
    Me.Label12.BackColor = System.Drawing.Color.Black
    Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
    Me.Label12.Location = New System.Drawing.Point(2, 49)
    Me.Label12.Name = "Label12"
    Me.Label12.Size = New System.Drawing.Size(373, 1)
    Me.Label12.TabIndex = 171
    Me.Label12.Text = "0,00"
    Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_total_taxes_name_2
    '
    Me.lbl_total_taxes_name_2.AutoSize = True
    Me.lbl_total_taxes_name_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_total_taxes_name_2.Location = New System.Drawing.Point(5, 52)
    Me.lbl_total_taxes_name_2.Name = "lbl_total_taxes_name_2"
    Me.lbl_total_taxes_name_2.Size = New System.Drawing.Size(36, 13)
    Me.lbl_total_taxes_name_2.TabIndex = 154
    Me.lbl_total_taxes_name_2.Text = "xTotal"
    Me.lbl_total_taxes_name_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_taxes_title
    '
    Me.lbl_taxes_title.AutoSize = True
    Me.lbl_taxes_title.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_taxes_title.Location = New System.Drawing.Point(5, 0)
    Me.lbl_taxes_title.Name = "lbl_taxes_title"
    Me.lbl_taxes_title.Size = New System.Drawing.Size(41, 13)
    Me.lbl_taxes_title.TabIndex = 152
    Me.lbl_taxes_title.Text = "xTaxes"
    Me.lbl_taxes_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_tax_on_prize_2_name
    '
    Me.lbl_tax_on_prize_2_name.AutoSize = True
    Me.lbl_tax_on_prize_2_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_tax_on_prize_2_name.Location = New System.Drawing.Point(23, 31)
    Me.lbl_tax_on_prize_2_name.Name = "lbl_tax_on_prize_2_name"
    Me.lbl_tax_on_prize_2_name.Size = New System.Drawing.Size(44, 13)
    Me.lbl_tax_on_prize_2_name.TabIndex = 121
    Me.lbl_tax_on_prize_2_name.Text = "xEstatal"
    Me.lbl_tax_on_prize_2_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_acceptors_deposit_value
    '
    Me.lbl_acceptors_deposit_value.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_acceptors_deposit_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_acceptors_deposit_value.ForeColor = System.Drawing.Color.Black
    Me.lbl_acceptors_deposit_value.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_acceptors_deposit_value.Location = New System.Drawing.Point(642, 239)
    Me.lbl_acceptors_deposit_value.Name = "lbl_acceptors_deposit_value"
    Me.lbl_acceptors_deposit_value.Size = New System.Drawing.Size(118, 16)
    Me.lbl_acceptors_deposit_value.TabIndex = 204
    Me.lbl_acceptors_deposit_value.Text = "xxxxxxx.xx"
    Me.lbl_acceptors_deposit_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_acceptors_deposit_name
    '
    Me.lbl_acceptors_deposit_name.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_acceptors_deposit_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
    Me.lbl_acceptors_deposit_name.Location = New System.Drawing.Point(400, 239)
    Me.lbl_acceptors_deposit_name.Name = "lbl_acceptors_deposit_name"
    Me.lbl_acceptors_deposit_name.Size = New System.Drawing.Size(229, 16)
    Me.lbl_acceptors_deposit_name.TabIndex = 203
    Me.lbl_acceptors_deposit_name.Text = "xAcceptors deposit"
    Me.lbl_acceptors_deposit_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'panel7
    '
    Me.panel7.Controls.Add(Me.lbl_cancel_promo_nr_amount)
    Me.panel7.Controls.Add(Me.lbl_cancel_promo_re_amount)
    Me.panel7.Controls.Add(Me.lbl_promo_re_amount)
    Me.panel7.Controls.Add(Me.lbl_nr_to_re_amount)
    Me.panel7.Controls.Add(Me.lbl_promo_nr_amount)
    Me.panel7.Controls.Add(Me.lbl_handpay_total)
    Me.panel7.Controls.Add(Me.lbl_handpays_amount)
    Me.panel7.Controls.Add(Me.lbl_handpay_cancellations_amount)
    Me.panel7.Controls.Add(Me.lbl_cancel_promo_nr_name)
    Me.panel7.Controls.Add(Me.lbl_cancel_promo_re_name)
    Me.panel7.Controls.Add(Me.lbl_promo_re_name)
    Me.panel7.Controls.Add(Me.lbl_promo_nr_name)
    Me.panel7.Controls.Add(Me.lbl_nr_to_re_name)
    Me.panel7.Controls.Add(Me.label5)
    Me.panel7.Controls.Add(Me.lbl_total_3_name)
    Me.panel7.Controls.Add(Me.lbl_handypays_name)
    Me.panel7.Controls.Add(Me.lbl_handpays_payment_name)
    Me.panel7.Controls.Add(Me.lbl_handpay_cancellations_name)
    Me.panel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.panel7.Location = New System.Drawing.Point(392, 13)
    Me.panel7.Name = "panel7"
    Me.panel7.Size = New System.Drawing.Size(380, 147)
    Me.panel7.TabIndex = 187
    '
    'lbl_cancel_promo_nr_amount
    '
    Me.lbl_cancel_promo_nr_amount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_cancel_promo_nr_amount.ForeColor = System.Drawing.Color.Black
    Me.lbl_cancel_promo_nr_amount.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_cancel_promo_nr_amount.Location = New System.Drawing.Point(254, 130)
    Me.lbl_cancel_promo_nr_amount.Name = "lbl_cancel_promo_nr_amount"
    Me.lbl_cancel_promo_nr_amount.Size = New System.Drawing.Size(113, 16)
    Me.lbl_cancel_promo_nr_amount.TabIndex = 213
    Me.lbl_cancel_promo_nr_amount.Text = "xxxxxxx.xx"
    Me.lbl_cancel_promo_nr_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_cancel_promo_re_amount
    '
    Me.lbl_cancel_promo_re_amount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_cancel_promo_re_amount.ForeColor = System.Drawing.Color.Black
    Me.lbl_cancel_promo_re_amount.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_cancel_promo_re_amount.Location = New System.Drawing.Point(254, 115)
    Me.lbl_cancel_promo_re_amount.Name = "lbl_cancel_promo_re_amount"
    Me.lbl_cancel_promo_re_amount.Size = New System.Drawing.Size(113, 16)
    Me.lbl_cancel_promo_re_amount.TabIndex = 211
    Me.lbl_cancel_promo_re_amount.Text = "xxxxxxx.xx"
    Me.lbl_cancel_promo_re_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_promo_re_amount
    '
    Me.lbl_promo_re_amount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_promo_re_amount.ForeColor = System.Drawing.Color.Black
    Me.lbl_promo_re_amount.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_promo_re_amount.Location = New System.Drawing.Point(254, 70)
    Me.lbl_promo_re_amount.Name = "lbl_promo_re_amount"
    Me.lbl_promo_re_amount.Size = New System.Drawing.Size(113, 16)
    Me.lbl_promo_re_amount.TabIndex = 209
    Me.lbl_promo_re_amount.Text = "xxxxxxx.xx"
    Me.lbl_promo_re_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_nr_to_re_amount
    '
    Me.lbl_nr_to_re_amount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_nr_to_re_amount.ForeColor = System.Drawing.Color.Black
    Me.lbl_nr_to_re_amount.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_nr_to_re_amount.Location = New System.Drawing.Point(254, 100)
    Me.lbl_nr_to_re_amount.Name = "lbl_nr_to_re_amount"
    Me.lbl_nr_to_re_amount.Size = New System.Drawing.Size(113, 16)
    Me.lbl_nr_to_re_amount.TabIndex = 207
    Me.lbl_nr_to_re_amount.Text = "xxxxxxx.xx"
    Me.lbl_nr_to_re_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_promo_nr_amount
    '
    Me.lbl_promo_nr_amount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_promo_nr_amount.ForeColor = System.Drawing.Color.Black
    Me.lbl_promo_nr_amount.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_promo_nr_amount.Location = New System.Drawing.Point(254, 85)
    Me.lbl_promo_nr_amount.Name = "lbl_promo_nr_amount"
    Me.lbl_promo_nr_amount.Size = New System.Drawing.Size(113, 16)
    Me.lbl_promo_nr_amount.TabIndex = 205
    Me.lbl_promo_nr_amount.Text = "xxxxxxx.xx"
    Me.lbl_promo_nr_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_handpay_total
    '
    Me.lbl_handpay_total.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_handpay_total.ForeColor = System.Drawing.Color.Black
    Me.lbl_handpay_total.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_handpay_total.Location = New System.Drawing.Point(220, 50)
    Me.lbl_handpay_total.Name = "lbl_handpay_total"
    Me.lbl_handpay_total.Size = New System.Drawing.Size(147, 16)
    Me.lbl_handpay_total.TabIndex = 178
    Me.lbl_handpay_total.Text = "xxxxxxx.xx"
    Me.lbl_handpay_total.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_handpays_amount
    '
    Me.lbl_handpays_amount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_handpays_amount.ForeColor = System.Drawing.Color.Black
    Me.lbl_handpays_amount.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_handpays_amount.Location = New System.Drawing.Point(220, 15)
    Me.lbl_handpays_amount.Name = "lbl_handpays_amount"
    Me.lbl_handpays_amount.Size = New System.Drawing.Size(147, 16)
    Me.lbl_handpays_amount.TabIndex = 172
    Me.lbl_handpays_amount.Text = "xxxxxxx.xx"
    Me.lbl_handpays_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_handpay_cancellations_amount
    '
    Me.lbl_handpay_cancellations_amount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_handpay_cancellations_amount.ForeColor = System.Drawing.Color.Black
    Me.lbl_handpay_cancellations_amount.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_handpay_cancellations_amount.Location = New System.Drawing.Point(220, 30)
    Me.lbl_handpay_cancellations_amount.Name = "lbl_handpay_cancellations_amount"
    Me.lbl_handpay_cancellations_amount.Size = New System.Drawing.Size(147, 18)
    Me.lbl_handpay_cancellations_amount.TabIndex = 175
    Me.lbl_handpay_cancellations_amount.Text = "xxxxxxx.xx"
    Me.lbl_handpay_cancellations_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_cancel_promo_nr_name
    '
    Me.lbl_cancel_promo_nr_name.AutoSize = True
    Me.lbl_cancel_promo_nr_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_cancel_promo_nr_name.ForeColor = System.Drawing.Color.Black
    Me.lbl_cancel_promo_nr_name.Location = New System.Drawing.Point(7, 130)
    Me.lbl_cancel_promo_nr_name.Name = "lbl_cancel_promo_nr_name"
    Me.lbl_cancel_promo_nr_name.Size = New System.Drawing.Size(91, 13)
    Me.lbl_cancel_promo_nr_name.TabIndex = 212
    Me.lbl_cancel_promo_nr_name.Text = "xCancelPromoNR"
    Me.lbl_cancel_promo_nr_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_cancel_promo_re_name
    '
    Me.lbl_cancel_promo_re_name.AutoSize = True
    Me.lbl_cancel_promo_re_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_cancel_promo_re_name.ForeColor = System.Drawing.Color.Black
    Me.lbl_cancel_promo_re_name.Location = New System.Drawing.Point(7, 115)
    Me.lbl_cancel_promo_re_name.Name = "lbl_cancel_promo_re_name"
    Me.lbl_cancel_promo_re_name.Size = New System.Drawing.Size(90, 13)
    Me.lbl_cancel_promo_re_name.TabIndex = 210
    Me.lbl_cancel_promo_re_name.Text = "xCancelPromoRE"
    Me.lbl_cancel_promo_re_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_promo_re_name
    '
    Me.lbl_promo_re_name.AutoSize = True
    Me.lbl_promo_re_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_promo_re_name.ForeColor = System.Drawing.Color.Black
    Me.lbl_promo_re_name.Location = New System.Drawing.Point(7, 70)
    Me.lbl_promo_re_name.Name = "lbl_promo_re_name"
    Me.lbl_promo_re_name.Size = New System.Drawing.Size(57, 13)
    Me.lbl_promo_re_name.TabIndex = 208
    Me.lbl_promo_re_name.Text = "xPromoRE"
    Me.lbl_promo_re_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_promo_nr_name
    '
    Me.lbl_promo_nr_name.AutoSize = True
    Me.lbl_promo_nr_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_promo_nr_name.ForeColor = System.Drawing.Color.Black
    Me.lbl_promo_nr_name.Location = New System.Drawing.Point(7, 85)
    Me.lbl_promo_nr_name.Name = "lbl_promo_nr_name"
    Me.lbl_promo_nr_name.Size = New System.Drawing.Size(58, 13)
    Me.lbl_promo_nr_name.TabIndex = 204
    Me.lbl_promo_nr_name.Text = "xPromoNR"
    Me.lbl_promo_nr_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_nr_to_re_name
    '
    Me.lbl_nr_to_re_name.AutoSize = True
    Me.lbl_nr_to_re_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_nr_to_re_name.ForeColor = System.Drawing.Color.Black
    Me.lbl_nr_to_re_name.Location = New System.Drawing.Point(7, 100)
    Me.lbl_nr_to_re_name.Name = "lbl_nr_to_re_name"
    Me.lbl_nr_to_re_name.Size = New System.Drawing.Size(92, 13)
    Me.lbl_nr_to_re_name.TabIndex = 206
    Me.lbl_nr_to_re_name.Text = "xCandadoNRaRE"
    Me.lbl_nr_to_re_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'label5
    '
    Me.label5.BackColor = System.Drawing.Color.Black
    Me.label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
    Me.label5.Location = New System.Drawing.Point(2, 48)
    Me.label5.Name = "label5"
    Me.label5.Size = New System.Drawing.Size(373, 1)
    Me.label5.TabIndex = 179
    Me.label5.Text = "0,00"
    Me.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_total_3_name
    '
    Me.lbl_total_3_name.AutoSize = True
    Me.lbl_total_3_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_total_3_name.Location = New System.Drawing.Point(7, 50)
    Me.lbl_total_3_name.Name = "lbl_total_3_name"
    Me.lbl_total_3_name.Size = New System.Drawing.Size(36, 13)
    Me.lbl_total_3_name.TabIndex = 177
    Me.lbl_total_3_name.Text = "xTotal"
    Me.lbl_total_3_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_handypays_name
    '
    Me.lbl_handypays_name.AutoSize = True
    Me.lbl_handypays_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_handypays_name.Location = New System.Drawing.Point(7, 0)
    Me.lbl_handypays_name.Name = "lbl_handypays_name"
    Me.lbl_handypays_name.Size = New System.Drawing.Size(60, 13)
    Me.lbl_handypays_name.TabIndex = 176
    Me.lbl_handypays_name.Text = "xHandpays"
    Me.lbl_handypays_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_handpays_payment_name
    '
    Me.lbl_handpays_payment_name.AutoSize = True
    Me.lbl_handpays_payment_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_handpays_payment_name.Location = New System.Drawing.Point(22, 15)
    Me.lbl_handpays_payment_name.Name = "lbl_handpays_payment_name"
    Me.lbl_handpays_payment_name.Size = New System.Drawing.Size(60, 13)
    Me.lbl_handpays_payment_name.TabIndex = 171
    Me.lbl_handpays_payment_name.Text = "xHandpays"
    Me.lbl_handpays_payment_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_handpay_cancellations_name
    '
    Me.lbl_handpay_cancellations_name.AutoSize = True
    Me.lbl_handpay_cancellations_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_handpay_cancellations_name.Location = New System.Drawing.Point(22, 30)
    Me.lbl_handpay_cancellations_name.Name = "lbl_handpay_cancellations_name"
    Me.lbl_handpay_cancellations_name.Size = New System.Drawing.Size(121, 13)
    Me.lbl_handpay_cancellations_name.TabIndex = 174
    Me.lbl_handpay_cancellations_name.Text = "xHandpay Cancellations"
    Me.lbl_handpay_cancellations_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.lbl_expiration_dev_name)
    Me.Panel1.Controls.Add(Me.lbl_expiration_dev_amount)
    Me.Panel1.Controls.Add(Me.lbl_expiration_nr_name)
    Me.Panel1.Controls.Add(Me.lbl_expiration_prize_name)
    Me.Panel1.Controls.Add(Me.lbl_expiration_nr_amount)
    Me.Panel1.Controls.Add(Me.lbl_expiration_prize_amount)
    Me.Panel1.Controls.Add(Me.lbl_expirations_name)
    Me.Panel1.Location = New System.Drawing.Point(393, 164)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(380, 67)
    Me.Panel1.TabIndex = 200
    '
    'lbl_expiration_dev_name
    '
    Me.lbl_expiration_dev_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_expiration_dev_name.Location = New System.Drawing.Point(22, 17)
    Me.lbl_expiration_dev_name.Name = "lbl_expiration_dev_name"
    Me.lbl_expiration_dev_name.Size = New System.Drawing.Size(214, 16)
    Me.lbl_expiration_dev_name.TabIndex = 171
    Me.lbl_expiration_dev_name.Text = "xRedeemableDevExpired"
    Me.lbl_expiration_dev_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_expiration_dev_amount
    '
    Me.lbl_expiration_dev_amount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_expiration_dev_amount.ForeColor = System.Drawing.Color.Black
    Me.lbl_expiration_dev_amount.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_expiration_dev_amount.Location = New System.Drawing.Point(228, 17)
    Me.lbl_expiration_dev_amount.Name = "lbl_expiration_dev_amount"
    Me.lbl_expiration_dev_amount.Size = New System.Drawing.Size(138, 16)
    Me.lbl_expiration_dev_amount.TabIndex = 172
    Me.lbl_expiration_dev_amount.Text = "xxxxxxx.xx"
    Me.lbl_expiration_dev_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_expiration_nr_name
    '
    Me.lbl_expiration_nr_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_expiration_nr_name.Location = New System.Drawing.Point(22, 50)
    Me.lbl_expiration_nr_name.Name = "lbl_expiration_nr_name"
    Me.lbl_expiration_nr_name.Size = New System.Drawing.Size(214, 16)
    Me.lbl_expiration_nr_name.TabIndex = 177
    Me.lbl_expiration_nr_name.Text = "xNotRedeemableExpired"
    Me.lbl_expiration_nr_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_expiration_prize_name
    '
    Me.lbl_expiration_prize_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_expiration_prize_name.Location = New System.Drawing.Point(22, 33)
    Me.lbl_expiration_prize_name.Name = "lbl_expiration_prize_name"
    Me.lbl_expiration_prize_name.Size = New System.Drawing.Size(214, 18)
    Me.lbl_expiration_prize_name.TabIndex = 174
    Me.lbl_expiration_prize_name.Text = "xRedeemablePrizeExpired"
    Me.lbl_expiration_prize_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_expiration_nr_amount
    '
    Me.lbl_expiration_nr_amount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_expiration_nr_amount.ForeColor = System.Drawing.Color.Black
    Me.lbl_expiration_nr_amount.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_expiration_nr_amount.Location = New System.Drawing.Point(222, 50)
    Me.lbl_expiration_nr_amount.Name = "lbl_expiration_nr_amount"
    Me.lbl_expiration_nr_amount.Size = New System.Drawing.Size(144, 16)
    Me.lbl_expiration_nr_amount.TabIndex = 178
    Me.lbl_expiration_nr_amount.Text = "xxxxxxx.xx"
    Me.lbl_expiration_nr_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_expiration_prize_amount
    '
    Me.lbl_expiration_prize_amount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_expiration_prize_amount.ForeColor = System.Drawing.Color.Black
    Me.lbl_expiration_prize_amount.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_expiration_prize_amount.Location = New System.Drawing.Point(225, 33)
    Me.lbl_expiration_prize_amount.Name = "lbl_expiration_prize_amount"
    Me.lbl_expiration_prize_amount.Size = New System.Drawing.Size(141, 18)
    Me.lbl_expiration_prize_amount.TabIndex = 175
    Me.lbl_expiration_prize_amount.Text = "xxxxxxx.xx"
    Me.lbl_expiration_prize_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_expirations_name
    '
    Me.lbl_expirations_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_expirations_name.Location = New System.Drawing.Point(7, 1)
    Me.lbl_expirations_name.Name = "lbl_expirations_name"
    Me.lbl_expirations_name.Size = New System.Drawing.Size(219, 16)
    Me.lbl_expirations_name.TabIndex = 176
    Me.lbl_expirations_name.Text = "xExpirations"
    Me.lbl_expirations_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'panel6
    '
    Me.panel6.Controls.Add(Me.Label1)
    Me.panel6.Controls.Add(Me.lbl_cash_in_total_3)
    Me.panel6.Controls.Add(Me.lbl_cash_out_total_3)
    Me.panel6.Controls.Add(Me.lbl_final_balance)
    Me.panel6.Controls.Add(Me.lbl_taxes_amount)
    Me.panel6.Controls.Add(Me.lbl_entry_name_3)
    Me.panel6.Controls.Add(Me.lbl_exits_name_3)
    Me.panel6.Controls.Add(Me.lbl_taxes_name)
    Me.panel6.Controls.Add(Me.lbl_total_name_2)
    Me.panel6.Location = New System.Drawing.Point(6, 429)
    Me.panel6.Name = "panel6"
    Me.panel6.Size = New System.Drawing.Size(380, 96)
    Me.panel6.TabIndex = 194
    '
    'Label1
    '
    Me.Label1.BackColor = System.Drawing.Color.Black
    Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
    Me.Label1.Location = New System.Drawing.Point(3, 66)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(373, 1)
    Me.Label1.TabIndex = 174
    Me.Label1.Text = "0,00"
    Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_cash_in_total_3
    '
    Me.lbl_cash_in_total_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_cash_in_total_3.ForeColor = System.Drawing.Color.Black
    Me.lbl_cash_in_total_3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_cash_in_total_3.Location = New System.Drawing.Point(187, 5)
    Me.lbl_cash_in_total_3.Name = "lbl_cash_in_total_3"
    Me.lbl_cash_in_total_3.Size = New System.Drawing.Size(181, 16)
    Me.lbl_cash_in_total_3.TabIndex = 155
    Me.lbl_cash_in_total_3.Text = "xxxxxxx.xx"
    Me.lbl_cash_in_total_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_cash_out_total_3
    '
    Me.lbl_cash_out_total_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_cash_out_total_3.ForeColor = System.Drawing.Color.Black
    Me.lbl_cash_out_total_3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_cash_out_total_3.Location = New System.Drawing.Point(186, 25)
    Me.lbl_cash_out_total_3.Name = "lbl_cash_out_total_3"
    Me.lbl_cash_out_total_3.Size = New System.Drawing.Size(181, 16)
    Me.lbl_cash_out_total_3.TabIndex = 156
    Me.lbl_cash_out_total_3.Text = "xxxxxxx.xx"
    Me.lbl_cash_out_total_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_final_balance
    '
    Me.lbl_final_balance.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_final_balance.ForeColor = System.Drawing.Color.Black
    Me.lbl_final_balance.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_final_balance.Location = New System.Drawing.Point(159, 67)
    Me.lbl_final_balance.Name = "lbl_final_balance"
    Me.lbl_final_balance.Size = New System.Drawing.Size(209, 26)
    Me.lbl_final_balance.TabIndex = 191
    Me.lbl_final_balance.Text = "xxxxxxx.xx"
    Me.lbl_final_balance.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_taxes_amount
    '
    Me.lbl_taxes_amount.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
    Me.lbl_taxes_amount.ForeColor = System.Drawing.Color.Black
    Me.lbl_taxes_amount.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_taxes_amount.Location = New System.Drawing.Point(186, 45)
    Me.lbl_taxes_amount.Name = "lbl_taxes_amount"
    Me.lbl_taxes_amount.Size = New System.Drawing.Size(181, 16)
    Me.lbl_taxes_amount.TabIndex = 147
    Me.lbl_taxes_amount.Text = "xxxxxxx.xx"
    Me.lbl_taxes_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_entry_name_3
    '
    Me.lbl_entry_name_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_entry_name_3.Location = New System.Drawing.Point(7, 5)
    Me.lbl_entry_name_3.Name = "lbl_entry_name_3"
    Me.lbl_entry_name_3.Size = New System.Drawing.Size(155, 16)
    Me.lbl_entry_name_3.TabIndex = 152
    Me.lbl_entry_name_3.Text = "xEntradas"
    Me.lbl_entry_name_3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_exits_name_3
    '
    Me.lbl_exits_name_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_exits_name_3.Location = New System.Drawing.Point(7, 25)
    Me.lbl_exits_name_3.Name = "lbl_exits_name_3"
    Me.lbl_exits_name_3.Size = New System.Drawing.Size(155, 16)
    Me.lbl_exits_name_3.TabIndex = 154
    Me.lbl_exits_name_3.Text = "xSalidas"
    Me.lbl_exits_name_3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_taxes_name
    '
    Me.lbl_taxes_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_taxes_name.Location = New System.Drawing.Point(7, 45)
    Me.lbl_taxes_name.Name = "lbl_taxes_name"
    Me.lbl_taxes_name.Size = New System.Drawing.Size(171, 16)
    Me.lbl_taxes_name.TabIndex = 142
    Me.lbl_taxes_name.Text = "xTaxes"
    Me.lbl_taxes_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_total_name_2
    '
    Me.lbl_total_name_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_total_name_2.Location = New System.Drawing.Point(7, 74)
    Me.lbl_total_name_2.Name = "lbl_total_name_2"
    Me.lbl_total_name_2.Size = New System.Drawing.Size(129, 16)
    Me.lbl_total_name_2.TabIndex = 190
    Me.lbl_total_name_2.Text = "xCash Desk Result"
    Me.lbl_total_name_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'panel2
    '
    Me.panel2.Controls.Add(Me.lbl_credit_card_payment_amount)
    Me.panel2.Controls.Add(Me.lbl_credit_card_payment_name)
    Me.panel2.Controls.Add(Me.lbl_balance_total_currency_value)
    Me.panel2.Controls.Add(Me.label2)
    Me.panel2.Controls.Add(Me.lbl_check_payments_amount)
    Me.panel2.Controls.Add(Me.lbl_cash_out_total_2)
    Me.panel2.Controls.Add(Me.lbl_check_payments_name)
    Me.panel2.Controls.Add(Me.lbl_cash_in_total_2)
    Me.panel2.Controls.Add(Me.lbl_balance_total_value_2)
    Me.panel2.Controls.Add(Me.lbl_deposit_amount)
    Me.panel2.Controls.Add(Me.lbl_pending_amount)
    Me.panel2.Controls.Add(Me.lbl_withdraw_amount)
    Me.panel2.Controls.Add(Me.lbl_balance_total_name_2)
    Me.panel2.Controls.Add(Me.lbl_exits_name_2)
    Me.panel2.Controls.Add(Me.lbl_pending_amount_name)
    Me.panel2.Controls.Add(Me.lbl_entry_name_2)
    Me.panel2.Controls.Add(Me.lbl_deposit_name)
    Me.panel2.Controls.Add(Me.lbl_withdraw_name)
    Me.panel2.Location = New System.Drawing.Point(393, 258)
    Me.panel2.Name = "panel2"
    Me.panel2.Size = New System.Drawing.Size(380, 200)
    Me.panel2.TabIndex = 193
    '
    'lbl_credit_card_payment_amount
    '
    Me.lbl_credit_card_payment_amount.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_credit_card_payment_amount.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_credit_card_payment_amount.ForeColor = System.Drawing.Color.Black
    Me.lbl_credit_card_payment_amount.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_credit_card_payment_amount.Location = New System.Drawing.Point(199, 122)
    Me.lbl_credit_card_payment_amount.Name = "lbl_credit_card_payment_amount"
    Me.lbl_credit_card_payment_amount.Size = New System.Drawing.Size(167, 16)
    Me.lbl_credit_card_payment_amount.TabIndex = 213
    Me.lbl_credit_card_payment_amount.Text = "xxxxxxx.xx"
    Me.lbl_credit_card_payment_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_credit_card_payment_name
    '
    Me.lbl_credit_card_payment_name.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_credit_card_payment_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_credit_card_payment_name.Location = New System.Drawing.Point(7, 122)
    Me.lbl_credit_card_payment_name.Name = "lbl_credit_card_payment_name"
    Me.lbl_credit_card_payment_name.Size = New System.Drawing.Size(229, 16)
    Me.lbl_credit_card_payment_name.TabIndex = 212
    Me.lbl_credit_card_payment_name.Text = "xPago tarjeta"
    Me.lbl_credit_card_payment_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_balance_total_currency_value
    '
    Me.lbl_balance_total_currency_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_balance_total_currency_value.ForeColor = System.Drawing.Color.Black
    Me.lbl_balance_total_currency_value.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_balance_total_currency_value.Location = New System.Drawing.Point(9, 172)
    Me.lbl_balance_total_currency_value.Name = "lbl_balance_total_currency_value"
    Me.lbl_balance_total_currency_value.Size = New System.Drawing.Size(359, 26)
    Me.lbl_balance_total_currency_value.TabIndex = 211
    Me.lbl_balance_total_currency_value.Text = "xxxxxxx.xx"
    Me.lbl_balance_total_currency_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'label2
    '
    Me.label2.BackColor = System.Drawing.Color.Black
    Me.label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
    Me.label2.Location = New System.Drawing.Point(3, 145)
    Me.label2.Name = "label2"
    Me.label2.Size = New System.Drawing.Size(370, 1)
    Me.label2.TabIndex = 206
    Me.label2.Text = "0,00"
    Me.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_check_payments_amount
    '
    Me.lbl_check_payments_amount.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_check_payments_amount.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_check_payments_amount.ForeColor = System.Drawing.Color.Black
    Me.lbl_check_payments_amount.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_check_payments_amount.Location = New System.Drawing.Point(199, 101)
    Me.lbl_check_payments_amount.Name = "lbl_check_payments_amount"
    Me.lbl_check_payments_amount.Size = New System.Drawing.Size(167, 16)
    Me.lbl_check_payments_amount.TabIndex = 210
    Me.lbl_check_payments_amount.Text = "xxxxxxx.xx"
    Me.lbl_check_payments_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_cash_out_total_2
    '
    Me.lbl_cash_out_total_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
    Me.lbl_cash_out_total_2.ForeColor = System.Drawing.Color.Black
    Me.lbl_cash_out_total_2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_cash_out_total_2.Location = New System.Drawing.Point(199, 61)
    Me.lbl_cash_out_total_2.Name = "lbl_cash_out_total_2"
    Me.lbl_cash_out_total_2.Size = New System.Drawing.Size(167, 16)
    Me.lbl_cash_out_total_2.TabIndex = 159
    Me.lbl_cash_out_total_2.Text = "xxxxxxx.xx"
    Me.lbl_cash_out_total_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_check_payments_name
    '
    Me.lbl_check_payments_name.BackColor = System.Drawing.SystemColors.Control
    Me.lbl_check_payments_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_check_payments_name.Location = New System.Drawing.Point(7, 101)
    Me.lbl_check_payments_name.Name = "lbl_check_payments_name"
    Me.lbl_check_payments_name.Size = New System.Drawing.Size(229, 16)
    Me.lbl_check_payments_name.TabIndex = 209
    Me.lbl_check_payments_name.Text = "xPagos en Cheque"
    Me.lbl_check_payments_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_cash_in_total_2
    '
    Me.lbl_cash_in_total_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
    Me.lbl_cash_in_total_2.ForeColor = System.Drawing.Color.Black
    Me.lbl_cash_in_total_2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_cash_in_total_2.Location = New System.Drawing.Point(199, 41)
    Me.lbl_cash_in_total_2.Name = "lbl_cash_in_total_2"
    Me.lbl_cash_in_total_2.Size = New System.Drawing.Size(167, 16)
    Me.lbl_cash_in_total_2.TabIndex = 139
    Me.lbl_cash_in_total_2.Text = "xxxxxxx.xx"
    Me.lbl_cash_in_total_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_balance_total_value_2
    '
    Me.lbl_balance_total_value_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_balance_total_value_2.ForeColor = System.Drawing.Color.Black
    Me.lbl_balance_total_value_2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_balance_total_value_2.Location = New System.Drawing.Point(158, 149)
    Me.lbl_balance_total_value_2.Name = "lbl_balance_total_value_2"
    Me.lbl_balance_total_value_2.Size = New System.Drawing.Size(210, 26)
    Me.lbl_balance_total_value_2.TabIndex = 208
    Me.lbl_balance_total_value_2.Text = "xxxxxxx.xx"
    Me.lbl_balance_total_value_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_deposit_amount
    '
    Me.lbl_deposit_amount.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
    Me.lbl_deposit_amount.ForeColor = System.Drawing.Color.Black
    Me.lbl_deposit_amount.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_deposit_amount.Location = New System.Drawing.Point(199, 1)
    Me.lbl_deposit_amount.Name = "lbl_deposit_amount"
    Me.lbl_deposit_amount.Size = New System.Drawing.Size(167, 16)
    Me.lbl_deposit_amount.TabIndex = 128
    Me.lbl_deposit_amount.Text = "xxxxxxx.xx"
    Me.lbl_deposit_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_pending_amount
    '
    Me.lbl_pending_amount.BackColor = System.Drawing.Color.Yellow
    Me.lbl_pending_amount.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_pending_amount.ForeColor = System.Drawing.Color.Black
    Me.lbl_pending_amount.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_pending_amount.Location = New System.Drawing.Point(199, 82)
    Me.lbl_pending_amount.Name = "lbl_pending_amount"
    Me.lbl_pending_amount.Size = New System.Drawing.Size(167, 16)
    Me.lbl_pending_amount.TabIndex = 206
    Me.lbl_pending_amount.Text = "xxxxxxx.xx"
    Me.lbl_pending_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_withdraw_amount
    '
    Me.lbl_withdraw_amount.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
    Me.lbl_withdraw_amount.ForeColor = System.Drawing.Color.Black
    Me.lbl_withdraw_amount.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_withdraw_amount.Location = New System.Drawing.Point(199, 21)
    Me.lbl_withdraw_amount.Name = "lbl_withdraw_amount"
    Me.lbl_withdraw_amount.Size = New System.Drawing.Size(167, 16)
    Me.lbl_withdraw_amount.TabIndex = 130
    Me.lbl_withdraw_amount.Text = "xxxxxxx.xx"
    Me.lbl_withdraw_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_balance_total_name_2
    '
    Me.lbl_balance_total_name_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_balance_total_name_2.Location = New System.Drawing.Point(7, 156)
    Me.lbl_balance_total_name_2.Name = "lbl_balance_total_name_2"
    Me.lbl_balance_total_name_2.Size = New System.Drawing.Size(129, 16)
    Me.lbl_balance_total_name_2.TabIndex = 207
    Me.lbl_balance_total_name_2.Text = "xEfectivo En Caja"
    Me.lbl_balance_total_name_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_exits_name_2
    '
    Me.lbl_exits_name_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_exits_name_2.Location = New System.Drawing.Point(7, 61)
    Me.lbl_exits_name_2.Name = "lbl_exits_name_2"
    Me.lbl_exits_name_2.Size = New System.Drawing.Size(153, 16)
    Me.lbl_exits_name_2.TabIndex = 138
    Me.lbl_exits_name_2.Text = "xSalidas"
    Me.lbl_exits_name_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_pending_amount_name
    '
    Me.lbl_pending_amount_name.BackColor = System.Drawing.Color.Yellow
    Me.lbl_pending_amount_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_pending_amount_name.Location = New System.Drawing.Point(7, 82)
    Me.lbl_pending_amount_name.Name = "lbl_pending_amount_name"
    Me.lbl_pending_amount_name.Size = New System.Drawing.Size(229, 16)
    Me.lbl_pending_amount_name.TabIndex = 205
    Me.lbl_pending_amount_name.Text = "xPending Amount"
    Me.lbl_pending_amount_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_entry_name_2
    '
    Me.lbl_entry_name_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_entry_name_2.Location = New System.Drawing.Point(7, 41)
    Me.lbl_entry_name_2.Name = "lbl_entry_name_2"
    Me.lbl_entry_name_2.Size = New System.Drawing.Size(153, 16)
    Me.lbl_entry_name_2.TabIndex = 136
    Me.lbl_entry_name_2.Text = "xEntradas"
    Me.lbl_entry_name_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_deposit_name
    '
    Me.lbl_deposit_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_deposit_name.Location = New System.Drawing.Point(7, 1)
    Me.lbl_deposit_name.Name = "lbl_deposit_name"
    Me.lbl_deposit_name.Size = New System.Drawing.Size(153, 16)
    Me.lbl_deposit_name.TabIndex = 116
    Me.lbl_deposit_name.Text = "xDeposit"
    Me.lbl_deposit_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_withdraw_name
    '
    Me.lbl_withdraw_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_withdraw_name.Location = New System.Drawing.Point(7, 21)
    Me.lbl_withdraw_name.Name = "lbl_withdraw_name"
    Me.lbl_withdraw_name.Size = New System.Drawing.Size(153, 16)
    Me.lbl_withdraw_name.TabIndex = 117
    Me.lbl_withdraw_name.Text = "xWithdraw"
    Me.lbl_withdraw_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'panel5
    '
    Me.panel5.Controls.Add(Me.lbl_b_total_dev)
    Me.panel5.Controls.Add(Me.lbl_decimal_rounding)
    Me.panel5.Controls.Add(Me.lbl_decimal_rounding_name)
    Me.panel5.Controls.Add(Me.lbl_refund_card_usage)
    Me.panel5.Controls.Add(Me.lbl_a_total_dev)
    Me.panel5.Controls.Add(Me.lbl_cash_out_total)
    Me.panel5.Controls.Add(Me.lbl_prizes_amount)
    Me.panel5.Controls.Add(Me.label4)
    Me.panel5.Controls.Add(Me.lbl_refund_card_usage_name)
    Me.panel5.Controls.Add(Me.lbl_a_total_dev_name)
    Me.panel5.Controls.Add(Me.lbl_b_total_dev_name)
    Me.panel5.Controls.Add(Me.lbl_exits_name)
    Me.panel5.Controls.Add(Me.lbl_total_1)
    Me.panel5.Controls.Add(Me.lbl_total_prizes_name)
    Me.panel5.Location = New System.Drawing.Point(6, 147)
    Me.panel5.Name = "panel5"
    Me.panel5.Size = New System.Drawing.Size(380, 124)
    Me.panel5.TabIndex = 186
    '
    'lbl_b_total_dev
    '
    Me.lbl_b_total_dev.BackColor = System.Drawing.Color.Transparent
    Me.lbl_b_total_dev.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_b_total_dev.ForeColor = System.Drawing.Color.Black
    Me.lbl_b_total_dev.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_b_total_dev.Location = New System.Drawing.Point(255, 33)
    Me.lbl_b_total_dev.Name = "lbl_b_total_dev"
    Me.lbl_b_total_dev.Size = New System.Drawing.Size(113, 16)
    Me.lbl_b_total_dev.TabIndex = 153
    Me.lbl_b_total_dev.Text = "xxxxxxx.xx"
    Me.lbl_b_total_dev.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_decimal_rounding
    '
    Me.lbl_decimal_rounding.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_decimal_rounding.ForeColor = System.Drawing.Color.Black
    Me.lbl_decimal_rounding.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_decimal_rounding.Location = New System.Drawing.Point(218, 82)
    Me.lbl_decimal_rounding.Name = "lbl_decimal_rounding"
    Me.lbl_decimal_rounding.Size = New System.Drawing.Size(150, 16)
    Me.lbl_decimal_rounding.TabIndex = 161
    Me.lbl_decimal_rounding.Text = "xxxxxxx.xx"
    Me.lbl_decimal_rounding.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_decimal_rounding_name
    '
    Me.lbl_decimal_rounding_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_decimal_rounding_name.Location = New System.Drawing.Point(23, 82)
    Me.lbl_decimal_rounding_name.Name = "lbl_decimal_rounding_name"
    Me.lbl_decimal_rounding_name.Size = New System.Drawing.Size(188, 16)
    Me.lbl_decimal_rounding_name.TabIndex = 160
    Me.lbl_decimal_rounding_name.Text = "xDecimalRounding"
    Me.lbl_decimal_rounding_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_refund_card_usage
    '
    Me.lbl_refund_card_usage.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_refund_card_usage.ForeColor = System.Drawing.Color.Black
    Me.lbl_refund_card_usage.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_refund_card_usage.Location = New System.Drawing.Point(218, 49)
    Me.lbl_refund_card_usage.Name = "lbl_refund_card_usage"
    Me.lbl_refund_card_usage.Size = New System.Drawing.Size(150, 16)
    Me.lbl_refund_card_usage.TabIndex = 157
    Me.lbl_refund_card_usage.Text = "xxxxxxx.xx"
    Me.lbl_refund_card_usage.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_a_total_dev
    '
    Me.lbl_a_total_dev.BackColor = System.Drawing.Color.Transparent
    Me.lbl_a_total_dev.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_a_total_dev.ForeColor = System.Drawing.Color.Black
    Me.lbl_a_total_dev.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_a_total_dev.Location = New System.Drawing.Point(218, 15)
    Me.lbl_a_total_dev.Name = "lbl_a_total_dev"
    Me.lbl_a_total_dev.Size = New System.Drawing.Size(150, 16)
    Me.lbl_a_total_dev.TabIndex = 155
    Me.lbl_a_total_dev.Text = "xxxxxxx.xx"
    Me.lbl_a_total_dev.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_cash_out_total
    '
    Me.lbl_cash_out_total.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_cash_out_total.ForeColor = System.Drawing.Color.Black
    Me.lbl_cash_out_total.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_cash_out_total.Location = New System.Drawing.Point(218, 106)
    Me.lbl_cash_out_total.Name = "lbl_cash_out_total"
    Me.lbl_cash_out_total.Size = New System.Drawing.Size(150, 16)
    Me.lbl_cash_out_total.TabIndex = 158
    Me.lbl_cash_out_total.Text = "xxxxxxx.xx"
    Me.lbl_cash_out_total.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_prizes_amount
    '
    Me.lbl_prizes_amount.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_prizes_amount.ForeColor = System.Drawing.Color.Black
    Me.lbl_prizes_amount.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_prizes_amount.Location = New System.Drawing.Point(218, 66)
    Me.lbl_prizes_amount.Name = "lbl_prizes_amount"
    Me.lbl_prizes_amount.Size = New System.Drawing.Size(150, 16)
    Me.lbl_prizes_amount.TabIndex = 151
    Me.lbl_prizes_amount.Text = "xxxxxxx.xx"
    Me.lbl_prizes_amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'label4
    '
    Me.label4.BackColor = System.Drawing.Color.Black
    Me.label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
    Me.label4.Location = New System.Drawing.Point(2, 104)
    Me.label4.Name = "label4"
    Me.label4.Size = New System.Drawing.Size(373, 1)
    Me.label4.TabIndex = 159
    Me.label4.Text = "0,00"
    Me.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_refund_card_usage_name
    '
    Me.lbl_refund_card_usage_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_refund_card_usage_name.Location = New System.Drawing.Point(23, 49)
    Me.lbl_refund_card_usage_name.Name = "lbl_refund_card_usage_name"
    Me.lbl_refund_card_usage_name.Size = New System.Drawing.Size(188, 16)
    Me.lbl_refund_card_usage_name.TabIndex = 156
    Me.lbl_refund_card_usage_name.Text = "xDevoluci�n-tarjetas"
    Me.lbl_refund_card_usage_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_a_total_dev_name
    '
    Me.lbl_a_total_dev_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_a_total_dev_name.Location = New System.Drawing.Point(23, 15)
    Me.lbl_a_total_dev_name.Name = "lbl_a_total_dev_name"
    Me.lbl_a_total_dev_name.Size = New System.Drawing.Size(188, 16)
    Me.lbl_a_total_dev_name.TabIndex = 154
    Me.lbl_a_total_dev_name.Text = "xDevoluci�n-Recarga"
    Me.lbl_a_total_dev_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_b_total_dev_name
    '
    Me.lbl_b_total_dev_name.AutoSize = True
    Me.lbl_b_total_dev_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_b_total_dev_name.Location = New System.Drawing.Point(23, 31)
    Me.lbl_b_total_dev_name.Name = "lbl_b_total_dev_name"
    Me.lbl_b_total_dev_name.Size = New System.Drawing.Size(123, 13)
    Me.lbl_b_total_dev_name.TabIndex = 152
    Me.lbl_b_total_dev_name.Text = "xDevoluci�n-UsoDeSala"
    Me.lbl_b_total_dev_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_exits_name
    '
    Me.lbl_exits_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_exits_name.Location = New System.Drawing.Point(5, 2)
    Me.lbl_exits_name.Name = "lbl_exits_name"
    Me.lbl_exits_name.Size = New System.Drawing.Size(219, 16)
    Me.lbl_exits_name.TabIndex = 137
    Me.lbl_exits_name.Text = "xSalidas"
    Me.lbl_exits_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_total_1
    '
    Me.lbl_total_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_total_1.Location = New System.Drawing.Point(5, 106)
    Me.lbl_total_1.Name = "lbl_total_1"
    Me.lbl_total_1.Size = New System.Drawing.Size(219, 16)
    Me.lbl_total_1.TabIndex = 136
    Me.lbl_total_1.Text = "xTotal"
    Me.lbl_total_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_total_prizes_name
    '
    Me.lbl_total_prizes_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_total_prizes_name.Location = New System.Drawing.Point(23, 66)
    Me.lbl_total_prizes_name.Name = "lbl_total_prizes_name"
    Me.lbl_total_prizes_name.Size = New System.Drawing.Size(188, 16)
    Me.lbl_total_prizes_name.TabIndex = 119
    Me.lbl_total_prizes_name.Text = "xPrizes"
    Me.lbl_total_prizes_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'panel4
    '
    Me.panel4.Controls.Add(Me.lbl_commission_value)
    Me.panel4.Controls.Add(Me.lbl_commission_name)
    Me.panel4.Controls.Add(Me.lbl_b_total_in)
    Me.panel4.Controls.Add(Me.lbl_service_charge)
    Me.panel4.Controls.Add(Me.lbl_service_charge_name)
    Me.panel4.Controls.Add(Me.lbl_total_in_total)
    Me.panel4.Controls.Add(Me.lbl_card_usage)
    Me.panel4.Controls.Add(Me.lbl_a_total_in)
    Me.panel4.Controls.Add(Me.label3)
    Me.panel4.Controls.Add(Me.lbl_entry_name)
    Me.panel4.Controls.Add(Me.lbl_total_0)
    Me.panel4.Controls.Add(Me.lbl_card_usage_name)
    Me.panel4.Controls.Add(Me.lbl_b_total_in_name)
    Me.panel4.Controls.Add(Me.lbl_a_total_in_name)
    Me.panel4.Location = New System.Drawing.Point(6, 11)
    Me.panel4.Name = "panel4"
    Me.panel4.Size = New System.Drawing.Size(380, 130)
    Me.panel4.TabIndex = 185
    '
    'lbl_commission_value
    '
    Me.lbl_commission_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_commission_value.ForeColor = System.Drawing.Color.Black
    Me.lbl_commission_value.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_commission_value.Location = New System.Drawing.Point(217, 84)
    Me.lbl_commission_value.Name = "lbl_commission_value"
    Me.lbl_commission_value.Size = New System.Drawing.Size(150, 16)
    Me.lbl_commission_value.TabIndex = 162
    Me.lbl_commission_value.Text = "xxxxxxx.xx"
    Me.lbl_commission_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_commission_name
    '
    Me.lbl_commission_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_commission_name.Location = New System.Drawing.Point(23, 84)
    Me.lbl_commission_name.Name = "lbl_commission_name"
    Me.lbl_commission_name.Size = New System.Drawing.Size(188, 16)
    Me.lbl_commission_name.TabIndex = 161
    Me.lbl_commission_name.Text = "xCommission"
    Me.lbl_commission_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_b_total_in
    '
    Me.lbl_b_total_in.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_b_total_in.ForeColor = System.Drawing.Color.Black
    Me.lbl_b_total_in.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_b_total_in.Location = New System.Drawing.Point(254, 32)
    Me.lbl_b_total_in.Name = "lbl_b_total_in"
    Me.lbl_b_total_in.Size = New System.Drawing.Size(113, 16)
    Me.lbl_b_total_in.TabIndex = 129
    Me.lbl_b_total_in.Text = "xxxxxxx.xx"
    Me.lbl_b_total_in.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_service_charge
    '
    Me.lbl_service_charge.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_service_charge.ForeColor = System.Drawing.Color.Black
    Me.lbl_service_charge.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_service_charge.Location = New System.Drawing.Point(217, 66)
    Me.lbl_service_charge.Name = "lbl_service_charge"
    Me.lbl_service_charge.Size = New System.Drawing.Size(150, 16)
    Me.lbl_service_charge.TabIndex = 160
    Me.lbl_service_charge.Text = "xxxxxxx.xx"
    Me.lbl_service_charge.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_service_charge_name
    '
    Me.lbl_service_charge_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_service_charge_name.Location = New System.Drawing.Point(23, 66)
    Me.lbl_service_charge_name.Name = "lbl_service_charge_name"
    Me.lbl_service_charge_name.Size = New System.Drawing.Size(188, 16)
    Me.lbl_service_charge_name.TabIndex = 159
    Me.lbl_service_charge_name.Text = "xServiceCharge"
    Me.lbl_service_charge_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_total_in_total
    '
    Me.lbl_total_in_total.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_total_in_total.ForeColor = System.Drawing.Color.Black
    Me.lbl_total_in_total.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_total_in_total.Location = New System.Drawing.Point(217, 109)
    Me.lbl_total_in_total.Name = "lbl_total_in_total"
    Me.lbl_total_in_total.Size = New System.Drawing.Size(150, 16)
    Me.lbl_total_in_total.TabIndex = 133
    Me.lbl_total_in_total.Text = "xxxxxxx.xx"
    Me.lbl_total_in_total.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_card_usage
    '
    Me.lbl_card_usage.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_card_usage.ForeColor = System.Drawing.Color.Black
    Me.lbl_card_usage.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_card_usage.Location = New System.Drawing.Point(217, 49)
    Me.lbl_card_usage.Name = "lbl_card_usage"
    Me.lbl_card_usage.Size = New System.Drawing.Size(150, 16)
    Me.lbl_card_usage.TabIndex = 131
    Me.lbl_card_usage.Text = "xxxxxxx.xx"
    Me.lbl_card_usage.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_a_total_in
    '
    Me.lbl_a_total_in.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_a_total_in.ForeColor = System.Drawing.Color.Black
    Me.lbl_a_total_in.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_a_total_in.Location = New System.Drawing.Point(217, 15)
    Me.lbl_a_total_in.Name = "lbl_a_total_in"
    Me.lbl_a_total_in.Size = New System.Drawing.Size(150, 16)
    Me.lbl_a_total_in.TabIndex = 124
    Me.lbl_a_total_in.Text = "xxxxxxx.xx"
    Me.lbl_a_total_in.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'label3
    '
    Me.label3.BackColor = System.Drawing.Color.Black
    Me.label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
    Me.label3.Location = New System.Drawing.Point(2, 107)
    Me.label3.Name = "label3"
    Me.label3.Size = New System.Drawing.Size(373, 1)
    Me.label3.TabIndex = 158
    Me.label3.Text = "0,00"
    Me.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_entry_name
    '
    Me.lbl_entry_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_entry_name.Location = New System.Drawing.Point(5, 1)
    Me.lbl_entry_name.Name = "lbl_entry_name"
    Me.lbl_entry_name.Size = New System.Drawing.Size(219, 16)
    Me.lbl_entry_name.TabIndex = 135
    Me.lbl_entry_name.Text = "xEntradas"
    Me.lbl_entry_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_total_0
    '
    Me.lbl_total_0.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_total_0.Location = New System.Drawing.Point(5, 109)
    Me.lbl_total_0.Name = "lbl_total_0"
    Me.lbl_total_0.Size = New System.Drawing.Size(196, 16)
    Me.lbl_total_0.TabIndex = 134
    Me.lbl_total_0.Text = "xTotal"
    Me.lbl_total_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_card_usage_name
    '
    Me.lbl_card_usage_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_card_usage_name.Location = New System.Drawing.Point(23, 49)
    Me.lbl_card_usage_name.Name = "lbl_card_usage_name"
    Me.lbl_card_usage_name.Size = New System.Drawing.Size(188, 16)
    Me.lbl_card_usage_name.TabIndex = 130
    Me.lbl_card_usage_name.Text = "xTarjetas"
    Me.lbl_card_usage_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_b_total_in_name
    '
    Me.lbl_b_total_in_name.AutoSize = True
    Me.lbl_b_total_in_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_b_total_in_name.Location = New System.Drawing.Point(23, 31)
    Me.lbl_b_total_in_name.Name = "lbl_b_total_in_name"
    Me.lbl_b_total_in_name.Size = New System.Drawing.Size(66, 13)
    Me.lbl_b_total_in_name.TabIndex = 118
    Me.lbl_b_total_in_name.Text = "xUsoDeSala"
    Me.lbl_b_total_in_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_a_total_in_name
    '
    Me.lbl_a_total_in_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    Me.lbl_a_total_in_name.Location = New System.Drawing.Point(23, 15)
    Me.lbl_a_total_in_name.Name = "lbl_a_total_in_name"
    Me.lbl_a_total_in_name.Size = New System.Drawing.Size(188, 16)
    Me.lbl_a_total_in_name.TabIndex = 114
    Me.lbl_a_total_in_name.Text = "xInputs"
    Me.lbl_a_total_in_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'panel3
    '
    Me.panel3.Controls.Add(Me.lbl_tax_return_value)
    Me.panel3.Controls.Add(Me.lbl_tax_return_name)
    Me.panel3.Controls.Add(Me.lbl_prizes_2)
    Me.panel3.Controls.Add(Me.lbl_prizes_gross)
    Me.panel3.Controls.Add(Me.lbl_taxes_amount_2)
    Me.panel3.Controls.Add(Me.label6)
    Me.panel3.Controls.Add(Me.lbl_total_prizes1_name)
    Me.panel3.Controls.Add(Me.lbl_total_prizes_gross_name)
    Me.panel3.Controls.Add(Me.lbl_total_taxes_name)
    Me.panel3.Location = New System.Drawing.Point(6, 277)
    Me.panel3.Name = "panel3"
    Me.panel3.Size = New System.Drawing.Size(380, 71)
    Me.panel3.TabIndex = 184
    '
    'lbl_tax_return_value
    '
    Me.lbl_tax_return_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_tax_return_value.ForeColor = System.Drawing.Color.Black
    Me.lbl_tax_return_value.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_tax_return_value.Location = New System.Drawing.Point(224, 30)
    Me.lbl_tax_return_value.Name = "lbl_tax_return_value"
    Me.lbl_tax_return_value.Size = New System.Drawing.Size(144, 16)
    Me.lbl_tax_return_value.TabIndex = 173
    Me.lbl_tax_return_value.Text = "xxxxxxx.xx"
    Me.lbl_tax_return_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_tax_return_name
    '
    Me.lbl_tax_return_name.AutoSize = True
    Me.lbl_tax_return_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_tax_return_name.Location = New System.Drawing.Point(5, 30)
    Me.lbl_tax_return_name.Name = "lbl_tax_return_name"
    Me.lbl_tax_return_name.Size = New System.Drawing.Size(62, 13)
    Me.lbl_tax_return_name.TabIndex = 172
    Me.lbl_tax_return_name.Text = "xTaxReturn"
    Me.lbl_tax_return_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_prizes_2
    '
    Me.lbl_prizes_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_prizes_2.ForeColor = System.Drawing.Color.Black
    Me.lbl_prizes_2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_prizes_2.Location = New System.Drawing.Point(221, 50)
    Me.lbl_prizes_2.Name = "lbl_prizes_2"
    Me.lbl_prizes_2.Size = New System.Drawing.Size(147, 16)
    Me.lbl_prizes_2.TabIndex = 155
    Me.lbl_prizes_2.Text = "xxxxxxx.xx"
    Me.lbl_prizes_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_prizes_gross
    '
    Me.lbl_prizes_gross.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_prizes_gross.ForeColor = System.Drawing.Color.Black
    Me.lbl_prizes_gross.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_prizes_gross.Location = New System.Drawing.Point(221, 1)
    Me.lbl_prizes_gross.Name = "lbl_prizes_gross"
    Me.lbl_prizes_gross.Size = New System.Drawing.Size(147, 16)
    Me.lbl_prizes_gross.TabIndex = 153
    Me.lbl_prizes_gross.Text = "xxxxxxx.xx"
    Me.lbl_prizes_gross.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_taxes_amount_2
    '
    Me.lbl_taxes_amount_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_taxes_amount_2.ForeColor = System.Drawing.Color.Black
    Me.lbl_taxes_amount_2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.lbl_taxes_amount_2.Location = New System.Drawing.Point(224, 15)
    Me.lbl_taxes_amount_2.Name = "lbl_taxes_amount_2"
    Me.lbl_taxes_amount_2.Size = New System.Drawing.Size(144, 16)
    Me.lbl_taxes_amount_2.TabIndex = 153
    Me.lbl_taxes_amount_2.Text = "xxxxxxx.xx"
    Me.lbl_taxes_amount_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'label6
    '
    Me.label6.BackColor = System.Drawing.Color.Black
    Me.label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
    Me.label6.Location = New System.Drawing.Point(2, 47)
    Me.label6.Name = "label6"
    Me.label6.Size = New System.Drawing.Size(373, 1)
    Me.label6.TabIndex = 171
    Me.label6.Text = "0,00"
    Me.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'lbl_total_prizes1_name
    '
    Me.lbl_total_prizes1_name.AutoSize = True
    Me.lbl_total_prizes1_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_total_prizes1_name.Location = New System.Drawing.Point(5, 50)
    Me.lbl_total_prizes1_name.Name = "lbl_total_prizes1_name"
    Me.lbl_total_prizes1_name.Size = New System.Drawing.Size(40, 13)
    Me.lbl_total_prizes1_name.TabIndex = 154
    Me.lbl_total_prizes1_name.Text = "xPrizes"
    Me.lbl_total_prizes1_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_total_prizes_gross_name
    '
    Me.lbl_total_prizes_gross_name.AutoSize = True
    Me.lbl_total_prizes_gross_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_total_prizes_gross_name.Location = New System.Drawing.Point(5, 0)
    Me.lbl_total_prizes_gross_name.Name = "lbl_total_prizes_gross_name"
    Me.lbl_total_prizes_gross_name.Size = New System.Drawing.Size(40, 13)
    Me.lbl_total_prizes_gross_name.TabIndex = 152
    Me.lbl_total_prizes_gross_name.Text = "xPrizes"
    Me.lbl_total_prizes_gross_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lbl_total_taxes_name
    '
    Me.lbl_total_taxes_name.AutoSize = True
    Me.lbl_total_taxes_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_total_taxes_name.Location = New System.Drawing.Point(5, 15)
    Me.lbl_total_taxes_name.Name = "lbl_total_taxes_name"
    Me.lbl_total_taxes_name.Size = New System.Drawing.Size(41, 13)
    Me.lbl_total_taxes_name.TabIndex = 121
    Me.lbl_total_taxes_name.Text = "xTaxes"
    Me.lbl_total_taxes_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'gb_header
    '
    Me.gb_header.Controls.Add(Me.tf_cm_to_date)
    Me.gb_header.Controls.Add(Me.tf_cm_from_date)
    Me.gb_header.Controls.Add(Me.tf_cs_current_balance)
    Me.gb_header.Controls.Add(Me.tf_cs_close_date)
    Me.gb_header.Controls.Add(Me.tf_cs_open_date)
    Me.gb_header.Controls.Add(Me.tf_cs_user_name)
    Me.gb_header.Controls.Add(Me.tf_cs_cash_desk_name)
    Me.gb_header.Controls.Add(Me.tf_cs_status)
    Me.gb_header.Controls.Add(Me.lbl_cs_name)
    Me.gb_header.Location = New System.Drawing.Point(3, 3)
    Me.gb_header.Name = "gb_header"
    Me.gb_header.Size = New System.Drawing.Size(781, 130)
    Me.gb_header.TabIndex = 10
    Me.gb_header.TabStop = False
    '
    'tf_cm_to_date
    '
    Me.tf_cm_to_date.IsReadOnly = True
    Me.tf_cm_to_date.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_cm_to_date.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_cm_to_date.Location = New System.Drawing.Point(550, 70)
    Me.tf_cm_to_date.Name = "tf_cm_to_date"
    Me.tf_cm_to_date.Size = New System.Drawing.Size(216, 24)
    Me.tf_cm_to_date.SufixText = "Sufix Text"
    Me.tf_cm_to_date.SufixTextVisible = True
    Me.tf_cm_to_date.TabIndex = 132
    Me.tf_cm_to_date.TabStop = False
    Me.tf_cm_to_date.Value = "22/09/2202 22:20:58"
    Me.tf_cm_to_date.Visible = False
    '
    'tf_cm_from_date
    '
    Me.tf_cm_from_date.IsReadOnly = True
    Me.tf_cm_from_date.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_cm_from_date.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_cm_from_date.Location = New System.Drawing.Point(550, 45)
    Me.tf_cm_from_date.Name = "tf_cm_from_date"
    Me.tf_cm_from_date.Size = New System.Drawing.Size(217, 24)
    Me.tf_cm_from_date.SufixText = "Sufix Text"
    Me.tf_cm_from_date.SufixTextVisible = True
    Me.tf_cm_from_date.TabIndex = 131
    Me.tf_cm_from_date.TabStop = False
    Me.tf_cm_from_date.Value = "22/09/2202 22:20:58"
    Me.tf_cm_from_date.Visible = False
    '
    'tf_cs_current_balance
    '
    Me.tf_cs_current_balance.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.tf_cs_current_balance.IsReadOnly = True
    Me.tf_cs_current_balance.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_RIGTH
    Me.tf_cs_current_balance.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_cs_current_balance.Location = New System.Drawing.Point(14, 93)
    Me.tf_cs_current_balance.Name = "tf_cs_current_balance"
    Me.tf_cs_current_balance.Size = New System.Drawing.Size(421, 31)
    Me.tf_cs_current_balance.SufixText = "Sufix Text"
    Me.tf_cs_current_balance.SufixTextVisible = True
    Me.tf_cs_current_balance.TabIndex = 130
    Me.tf_cs_current_balance.TabStop = False
    Me.tf_cs_current_balance.TextWidth = 200
    Me.tf_cs_current_balance.Value = "999.999.999,99"
    '
    'tf_cs_close_date
    '
    Me.tf_cs_close_date.IsReadOnly = True
    Me.tf_cs_close_date.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_cs_close_date.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_cs_close_date.Location = New System.Drawing.Point(314, 68)
    Me.tf_cs_close_date.Name = "tf_cs_close_date"
    Me.tf_cs_close_date.Size = New System.Drawing.Size(216, 24)
    Me.tf_cs_close_date.SufixText = "Sufix Text"
    Me.tf_cs_close_date.SufixTextVisible = True
    Me.tf_cs_close_date.TabIndex = 126
    Me.tf_cs_close_date.TabStop = False
    Me.tf_cs_close_date.Value = "22/09/2202 22:20:58"
    '
    'tf_cs_open_date
    '
    Me.tf_cs_open_date.IsReadOnly = True
    Me.tf_cs_open_date.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_cs_open_date.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_cs_open_date.Location = New System.Drawing.Point(6, 68)
    Me.tf_cs_open_date.Name = "tf_cs_open_date"
    Me.tf_cs_open_date.Size = New System.Drawing.Size(222, 24)
    Me.tf_cs_open_date.SufixText = "Sufix Text"
    Me.tf_cs_open_date.SufixTextVisible = True
    Me.tf_cs_open_date.TabIndex = 125
    Me.tf_cs_open_date.TabStop = False
    Me.tf_cs_open_date.Value = "22/09/2202 22:20:58"
    '
    'tf_cs_user_name
    '
    Me.tf_cs_user_name.IsReadOnly = True
    Me.tf_cs_user_name.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_cs_user_name.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_cs_user_name.Location = New System.Drawing.Point(294, 43)
    Me.tf_cs_user_name.Name = "tf_cs_user_name"
    Me.tf_cs_user_name.Size = New System.Drawing.Size(250, 24)
    Me.tf_cs_user_name.SufixText = "Sufix Text"
    Me.tf_cs_user_name.SufixTextVisible = True
    Me.tf_cs_user_name.TabIndex = 124
    Me.tf_cs_user_name.TabStop = False
    Me.tf_cs_user_name.TextWidth = 100
    Me.tf_cs_user_name.Value = "xUser Name"
    '
    'tf_cs_cash_desk_name
    '
    Me.tf_cs_cash_desk_name.IsReadOnly = True
    Me.tf_cs_cash_desk_name.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_cs_cash_desk_name.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_cs_cash_desk_name.Location = New System.Drawing.Point(6, 43)
    Me.tf_cs_cash_desk_name.Name = "tf_cs_cash_desk_name"
    Me.tf_cs_cash_desk_name.Size = New System.Drawing.Size(302, 24)
    Me.tf_cs_cash_desk_name.SufixText = "Sufix Text"
    Me.tf_cs_cash_desk_name.SufixTextVisible = True
    Me.tf_cs_cash_desk_name.TabIndex = 123
    Me.tf_cs_cash_desk_name.TabStop = False
    Me.tf_cs_cash_desk_name.Value = "xCash Desk Name"
    '
    'tf_cs_status
    '
    Me.tf_cs_status.IsReadOnly = True
    Me.tf_cs_status.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_cs_status.LabelForeColor = System.Drawing.Color.Blue
    Me.tf_cs_status.Location = New System.Drawing.Point(294, 18)
    Me.tf_cs_status.Name = "tf_cs_status"
    Me.tf_cs_status.Size = New System.Drawing.Size(250, 24)
    Me.tf_cs_status.SufixText = "Sufix Text"
    Me.tf_cs_status.SufixTextVisible = True
    Me.tf_cs_status.TabIndex = 122
    Me.tf_cs_status.TabStop = False
    Me.tf_cs_status.TextWidth = 100
    Me.tf_cs_status.Value = "xStatus"
    '
    'lbl_cs_name
    '
    Me.lbl_cs_name.IsReadOnly = True
    Me.lbl_cs_name.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_cs_name.LabelForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_cs_name.Location = New System.Drawing.Point(6, 18)
    Me.lbl_cs_name.Name = "lbl_cs_name"
    Me.lbl_cs_name.Size = New System.Drawing.Size(297, 24)
    Me.lbl_cs_name.SufixText = "Sufix Text"
    Me.lbl_cs_name.SufixTextVisible = True
    Me.lbl_cs_name.TabIndex = 10
    Me.lbl_cs_name.TextWidth = 0
    Me.lbl_cs_name.Value = "xCashier Session Name"
    '
    'frm_TITO_session_detail
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(891, 773)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_TITO_session_detail"
    Me.Text = "frm_TITO_session_details"
    Me.panel_data.ResumeLayout(False)
    Me.gb_detail.ResumeLayout(False)
    Me.Panel9.ResumeLayout(False)
    Me.Panel9.PerformLayout()
    Me.Panel8.ResumeLayout(False)
    Me.Panel8.PerformLayout()
    Me.panel7.ResumeLayout(False)
    Me.panel7.PerformLayout()
    Me.Panel1.ResumeLayout(False)
    Me.panel6.ResumeLayout(False)
    Me.panel2.ResumeLayout(False)
    Me.panel5.ResumeLayout(False)
    Me.panel5.PerformLayout()
    Me.panel4.ResumeLayout(False)
    Me.panel4.PerformLayout()
    Me.panel3.ResumeLayout(False)
    Me.panel3.PerformLayout()
    Me.gb_header.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_detail As System.Windows.Forms.GroupBox
  Private WithEvents lbl_currency_exchange_amount As System.Windows.Forms.Label
  Private WithEvents Panel9 As System.Windows.Forms.Panel
  Private WithEvents lbl_liabilities_total_increment_value As System.Windows.Forms.Label
  Private WithEvents lbl_liabilities_total_final_value As System.Windows.Forms.Label
  Private WithEvents lbl_liabilities_total_initial_value As System.Windows.Forms.Label
  Private WithEvents lbl_liabilities_increment_promo_nr_value As System.Windows.Forms.Label
  Private WithEvents lbl_liabilities_increment_promo_re_value As System.Windows.Forms.Label
  Private WithEvents lbl_liabilities_increment_re_value As System.Windows.Forms.Label
  Private WithEvents lbl_liabilities_final_promo_nr_value As System.Windows.Forms.Label
  Private WithEvents lbl_liabilities_final_promo_re_value As System.Windows.Forms.Label
  Private WithEvents lbl_liabilities_final_re_value As System.Windows.Forms.Label
  Private WithEvents lbl_liabilities_initial_promo_nr_value As System.Windows.Forms.Label
  Private WithEvents lbl_liabilities_initial_promo_re_value As System.Windows.Forms.Label
  Private WithEvents lbl_liabilities_initial_re_value As System.Windows.Forms.Label
  Private WithEvents lbl_liabilities_increment_name As System.Windows.Forms.Label
  Private WithEvents lbl_liabilities_final_name As System.Windows.Forms.Label
  Private WithEvents lbl_liabilities_initial_name As System.Windows.Forms.Label
  Private WithEvents Label14 As System.Windows.Forms.Label
  Private WithEvents lbl_liabilities_title_name As System.Windows.Forms.Label
  Private WithEvents lbl_liabilities_total_name As System.Windows.Forms.Label
  Private WithEvents lbl_liabilities_promo_nr_name As System.Windows.Forms.Label
  Private WithEvents lbl_liabilities_promo_re_name As System.Windows.Forms.Label
  Private WithEvents lbl_liabilities_re_name As System.Windows.Forms.Label
  Private WithEvents lbl_currency_exchange_name As System.Windows.Forms.Label
  Private WithEvents Panel8 As System.Windows.Forms.Panel
  Private WithEvents lbl_tax_on_prize_1_value As System.Windows.Forms.Label
  Private WithEvents lbl_tax_on_prize_1_name As System.Windows.Forms.Label
  Private WithEvents lbl_taxes_amount_3 As System.Windows.Forms.Label
  Private WithEvents lbl_tax_on_prize_2_value As System.Windows.Forms.Label
  Private WithEvents Label12 As System.Windows.Forms.Label
  Private WithEvents lbl_total_taxes_name_2 As System.Windows.Forms.Label
  Private WithEvents lbl_taxes_title As System.Windows.Forms.Label
  Private WithEvents lbl_tax_on_prize_2_name As System.Windows.Forms.Label
  Private WithEvents lbl_acceptors_deposit_value As System.Windows.Forms.Label
  Private WithEvents lbl_acceptors_deposit_name As System.Windows.Forms.Label
  Private WithEvents panel7 As System.Windows.Forms.Panel
  Private WithEvents lbl_cancel_promo_nr_amount As System.Windows.Forms.Label
  Private WithEvents lbl_cancel_promo_re_amount As System.Windows.Forms.Label
  Private WithEvents lbl_promo_re_amount As System.Windows.Forms.Label
  Private WithEvents lbl_nr_to_re_amount As System.Windows.Forms.Label
  Private WithEvents lbl_promo_nr_amount As System.Windows.Forms.Label
  Private WithEvents lbl_handpay_total As System.Windows.Forms.Label
  Private WithEvents lbl_handpays_amount As System.Windows.Forms.Label
  Private WithEvents lbl_handpay_cancellations_amount As System.Windows.Forms.Label
  Private WithEvents lbl_cancel_promo_nr_name As System.Windows.Forms.Label
  Private WithEvents lbl_cancel_promo_re_name As System.Windows.Forms.Label
  Private WithEvents lbl_promo_re_name As System.Windows.Forms.Label
  Private WithEvents lbl_promo_nr_name As System.Windows.Forms.Label
  Private WithEvents lbl_nr_to_re_name As System.Windows.Forms.Label
  Private WithEvents label5 As System.Windows.Forms.Label
  Private WithEvents lbl_total_3_name As System.Windows.Forms.Label
  Private WithEvents lbl_handypays_name As System.Windows.Forms.Label
  Private WithEvents lbl_handpays_payment_name As System.Windows.Forms.Label
  Private WithEvents lbl_handpay_cancellations_name As System.Windows.Forms.Label
  Private WithEvents Panel1 As System.Windows.Forms.Panel
  Private WithEvents lbl_expiration_dev_name As System.Windows.Forms.Label
  Private WithEvents lbl_expiration_dev_amount As System.Windows.Forms.Label
  Private WithEvents lbl_expiration_nr_name As System.Windows.Forms.Label
  Private WithEvents lbl_expiration_prize_name As System.Windows.Forms.Label
  Private WithEvents lbl_expiration_nr_amount As System.Windows.Forms.Label
  Private WithEvents lbl_expiration_prize_amount As System.Windows.Forms.Label
  Private WithEvents lbl_expirations_name As System.Windows.Forms.Label
  Private WithEvents panel6 As System.Windows.Forms.Panel
  Private WithEvents Label1 As System.Windows.Forms.Label
  Private WithEvents lbl_cash_in_total_3 As System.Windows.Forms.Label
  Private WithEvents lbl_cash_out_total_3 As System.Windows.Forms.Label
  Private WithEvents lbl_final_balance As System.Windows.Forms.Label
  Private WithEvents lbl_taxes_amount As System.Windows.Forms.Label
  Private WithEvents lbl_entry_name_3 As System.Windows.Forms.Label
  Private WithEvents lbl_exits_name_3 As System.Windows.Forms.Label
  Private WithEvents lbl_taxes_name As System.Windows.Forms.Label
  Private WithEvents lbl_total_name_2 As System.Windows.Forms.Label
  Private WithEvents panel2 As System.Windows.Forms.Panel
  Private WithEvents lbl_credit_card_payment_amount As System.Windows.Forms.Label
  Private WithEvents lbl_credit_card_payment_name As System.Windows.Forms.Label
  Private WithEvents lbl_balance_total_currency_value As System.Windows.Forms.Label
  Private WithEvents label2 As System.Windows.Forms.Label
  Private WithEvents lbl_check_payments_amount As System.Windows.Forms.Label
  Private WithEvents lbl_cash_out_total_2 As System.Windows.Forms.Label
  Private WithEvents lbl_check_payments_name As System.Windows.Forms.Label
  Private WithEvents lbl_cash_in_total_2 As System.Windows.Forms.Label
  Private WithEvents lbl_balance_total_value_2 As System.Windows.Forms.Label
  Private WithEvents lbl_deposit_amount As System.Windows.Forms.Label
  Private WithEvents lbl_pending_amount As System.Windows.Forms.Label
  Private WithEvents lbl_withdraw_amount As System.Windows.Forms.Label
  Private WithEvents lbl_balance_total_name_2 As System.Windows.Forms.Label
  Private WithEvents lbl_exits_name_2 As System.Windows.Forms.Label
  Private WithEvents lbl_pending_amount_name As System.Windows.Forms.Label
  Private WithEvents lbl_entry_name_2 As System.Windows.Forms.Label
  Private WithEvents lbl_deposit_name As System.Windows.Forms.Label
  Private WithEvents lbl_withdraw_name As System.Windows.Forms.Label
  Private WithEvents panel5 As System.Windows.Forms.Panel
  Private WithEvents lbl_b_total_dev As System.Windows.Forms.Label
  Private WithEvents lbl_decimal_rounding As System.Windows.Forms.Label
  Private WithEvents lbl_decimal_rounding_name As System.Windows.Forms.Label
  Private WithEvents lbl_refund_card_usage As System.Windows.Forms.Label
  Private WithEvents lbl_a_total_dev As System.Windows.Forms.Label
  Private WithEvents lbl_cash_out_total As System.Windows.Forms.Label
  Private WithEvents lbl_prizes_amount As System.Windows.Forms.Label
  Private WithEvents label4 As System.Windows.Forms.Label
  Private WithEvents lbl_refund_card_usage_name As System.Windows.Forms.Label
  Private WithEvents lbl_a_total_dev_name As System.Windows.Forms.Label
  Private WithEvents lbl_b_total_dev_name As System.Windows.Forms.Label
  Private WithEvents lbl_exits_name As System.Windows.Forms.Label
  Private WithEvents lbl_total_1 As System.Windows.Forms.Label
  Private WithEvents lbl_total_prizes_name As System.Windows.Forms.Label
  Private WithEvents panel4 As System.Windows.Forms.Panel
  Private WithEvents lbl_commission_value As System.Windows.Forms.Label
  Private WithEvents lbl_commission_name As System.Windows.Forms.Label
  Private WithEvents lbl_b_total_in As System.Windows.Forms.Label
  Private WithEvents lbl_service_charge As System.Windows.Forms.Label
  Private WithEvents lbl_service_charge_name As System.Windows.Forms.Label
  Private WithEvents lbl_total_in_total As System.Windows.Forms.Label
  Private WithEvents lbl_card_usage As System.Windows.Forms.Label
  Private WithEvents lbl_a_total_in As System.Windows.Forms.Label
  Private WithEvents label3 As System.Windows.Forms.Label
  Private WithEvents lbl_entry_name As System.Windows.Forms.Label
  Private WithEvents lbl_total_0 As System.Windows.Forms.Label
  Private WithEvents lbl_card_usage_name As System.Windows.Forms.Label
  Private WithEvents lbl_b_total_in_name As System.Windows.Forms.Label
  Private WithEvents lbl_a_total_in_name As System.Windows.Forms.Label
  Private WithEvents panel3 As System.Windows.Forms.Panel
  Private WithEvents lbl_tax_return_value As System.Windows.Forms.Label
  Private WithEvents lbl_tax_return_name As System.Windows.Forms.Label
  Private WithEvents lbl_prizes_2 As System.Windows.Forms.Label
  Private WithEvents lbl_prizes_gross As System.Windows.Forms.Label
  Private WithEvents lbl_taxes_amount_2 As System.Windows.Forms.Label
  Private WithEvents label6 As System.Windows.Forms.Label
  Private WithEvents lbl_total_prizes1_name As System.Windows.Forms.Label
  Private WithEvents lbl_total_prizes_gross_name As System.Windows.Forms.Label
  Private WithEvents lbl_total_taxes_name As System.Windows.Forms.Label
  Friend WithEvents gb_header As System.Windows.Forms.GroupBox
  Friend WithEvents tf_cm_to_date As GUI_Controls.uc_text_field
  Friend WithEvents tf_cm_from_date As GUI_Controls.uc_text_field
  Friend WithEvents tf_cs_current_balance As GUI_Controls.uc_text_field
  Friend WithEvents tf_cs_close_date As GUI_Controls.uc_text_field
  Friend WithEvents tf_cs_open_date As GUI_Controls.uc_text_field
  Friend WithEvents tf_cs_user_name As GUI_Controls.uc_text_field
  Friend WithEvents tf_cs_cash_desk_name As GUI_Controls.uc_text_field
  Friend WithEvents tf_cs_status As GUI_Controls.uc_text_field
  Friend WithEvents lbl_cs_name As GUI_Controls.uc_text_field
End Class
