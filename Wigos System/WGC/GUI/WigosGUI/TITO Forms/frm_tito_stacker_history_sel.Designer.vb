<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_tito_stacker_history_sel
    Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_date = New GUI_Controls.uc_daily_session_selector
    Me.ef_stacker = New GUI_Controls.uc_entry_field
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ef_stacker)
    Me.panel_filter.Controls.Add(Me.uc_date)
    Me.panel_filter.Size = New System.Drawing.Size(921, 95)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_stacker, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 99)
    Me.panel_data.Size = New System.Drawing.Size(921, 426)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(915, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(915, 4)
    '
    'uc_date
    '
    Me.uc_date.ClosingTime = 0
    Me.uc_date.ClosingTimeEnabled = True
    Me.uc_date.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_date.FromDateSelected = True
    Me.uc_date.Location = New System.Drawing.Point(6, 6)
    Me.uc_date.Name = "uc_date"
    Me.uc_date.Size = New System.Drawing.Size(260, 82)
    Me.uc_date.TabIndex = 11
    Me.uc_date.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_date.ToDateSelected = True
    '
    'ef_stacker
    '
    Me.ef_stacker.DoubleValue = 0
    Me.ef_stacker.IntegerValue = 0
    Me.ef_stacker.IsReadOnly = False
    Me.ef_stacker.Location = New System.Drawing.Point(272, 15)
    Me.ef_stacker.Name = "ef_stacker"
    Me.ef_stacker.Size = New System.Drawing.Size(200, 24)
    Me.ef_stacker.SufixText = "Sufix Text"
    Me.ef_stacker.SufixTextVisible = True
    Me.ef_stacker.TabIndex = 12
    Me.ef_stacker.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_stacker.TextValue = ""
    Me.ef_stacker.Value = ""
    '
    'frm_tito_stacker_history_sel
    '
    Me.ClientSize = New System.Drawing.Size(929, 529)
    Me.Name = "frm_tito_stacker_history_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_date As GUI_Controls.uc_daily_session_selector
  Friend WithEvents ef_stacker As GUI_Controls.uc_entry_field

End Class
