<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_tito_stacker_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.chk_select_pre_term = New System.Windows.Forms.CheckBox
    Me.txt_notes = New System.Windows.Forms.TextBox
    Me.chk_deactivated = New System.Windows.Forms.CheckBox
    Me.ef_model = New GUI_Controls.uc_entry_field
    Me.ef_stacker_id = New GUI_Controls.uc_entry_field
    Me.uc_preassigned_term = New GUI_Controls.uc_provider
    Me.lbl_notes = New System.Windows.Forms.Label
    Me.lbl_status = New System.Windows.Forms.Label
    Me.lbl_status_collection = New System.Windows.Forms.Label
    Me.gb_collection = New System.Windows.Forms.GroupBox
    Me.uc_unassign_terminal = New GUI_Controls.uc_button
    Me.lbl_terminal_collection = New System.Windows.Forms.Label
    Me.lbl_terminal = New System.Windows.Forms.Label
    Me.panel_data.SuspendLayout()
    Me.gb_collection.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_collection)
    Me.panel_data.Controls.Add(Me.lbl_notes)
    Me.panel_data.Controls.Add(Me.chk_select_pre_term)
    Me.panel_data.Controls.Add(Me.txt_notes)
    Me.panel_data.Controls.Add(Me.chk_deactivated)
    Me.panel_data.Controls.Add(Me.ef_model)
    Me.panel_data.Controls.Add(Me.ef_stacker_id)
    Me.panel_data.Controls.Add(Me.uc_preassigned_term)
    Me.panel_data.Size = New System.Drawing.Size(523, 343)
    '
    'chk_select_pre_term
    '
    Me.chk_select_pre_term.AutoSize = True
    Me.chk_select_pre_term.Location = New System.Drawing.Point(9, 47)
    Me.chk_select_pre_term.Name = "chk_select_pre_term"
    Me.chk_select_pre_term.Size = New System.Drawing.Size(100, 17)
    Me.chk_select_pre_term.TabIndex = 3
    Me.chk_select_pre_term.Text = "xSelPreTerm"
    Me.chk_select_pre_term.UseVisualStyleBackColor = True
    '
    'txt_notes
    '
    Me.txt_notes.Location = New System.Drawing.Point(54, 269)
    Me.txt_notes.MaxLength = 200
    Me.txt_notes.Multiline = True
    Me.txt_notes.Name = "txt_notes"
    Me.txt_notes.Size = New System.Drawing.Size(457, 68)
    Me.txt_notes.TabIndex = 5
    '
    'chk_deactivated
    '
    Me.chk_deactivated.AutoSize = True
    Me.chk_deactivated.Location = New System.Drawing.Point(420, 19)
    Me.chk_deactivated.Name = "chk_deactivated"
    Me.chk_deactivated.Size = New System.Drawing.Size(68, 17)
    Me.chk_deactivated.TabIndex = 2
    Me.chk_deactivated.Text = "xActive"
    Me.chk_deactivated.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_deactivated.UseVisualStyleBackColor = True
    '
    'ef_model
    '
    Me.ef_model.DoubleValue = 0
    Me.ef_model.IntegerValue = 0
    Me.ef_model.IsReadOnly = False
    Me.ef_model.Location = New System.Drawing.Point(186, 13)
    Me.ef_model.Name = "ef_model"
    Me.ef_model.Size = New System.Drawing.Size(210, 24)
    Me.ef_model.SufixText = "Sufix Text"
    Me.ef_model.SufixTextVisible = True
    Me.ef_model.TabIndex = 1
    Me.ef_model.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_model.TextValue = ""
    Me.ef_model.TextWidth = 50
    Me.ef_model.Value = ""
    '
    'ef_stacker_id
    '
    Me.ef_stacker_id.DoubleValue = 0
    Me.ef_stacker_id.IntegerValue = 0
    Me.ef_stacker_id.IsReadOnly = False
    Me.ef_stacker_id.Location = New System.Drawing.Point(3, 13)
    Me.ef_stacker_id.Name = "ef_stacker_id"
    Me.ef_stacker_id.Size = New System.Drawing.Size(173, 24)
    Me.ef_stacker_id.SufixText = "Sufix Text"
    Me.ef_stacker_id.SufixTextVisible = True
    Me.ef_stacker_id.TabIndex = 0
    Me.ef_stacker_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_stacker_id.TextValue = ""
    Me.ef_stacker_id.Value = ""
    '
    'uc_preassigned_term
    '
    Me.uc_preassigned_term.FilterByCurrency = False
    Me.uc_preassigned_term.Location = New System.Drawing.Point(6, 71)
    Me.uc_preassigned_term.Name = "uc_preassigned_term"
    Me.uc_preassigned_term.Size = New System.Drawing.Size(190, 192)
    Me.uc_preassigned_term.TabIndex = 4
    '
    'lbl_notes
    '
    Me.lbl_notes.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_notes.AutoSize = True
    Me.lbl_notes.Location = New System.Drawing.Point(9, 269)
    Me.lbl_notes.Name = "lbl_notes"
    Me.lbl_notes.Size = New System.Drawing.Size(39, 13)
    Me.lbl_notes.TabIndex = 8
    Me.lbl_notes.Text = "Notas"
    Me.lbl_notes.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'lbl_status
    '
    Me.lbl_status.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_status.AutoSize = True
    Me.lbl_status.Location = New System.Drawing.Point(22, 62)
    Me.lbl_status.Name = "lbl_status"
    Me.lbl_status.Size = New System.Drawing.Size(50, 13)
    Me.lbl_status.TabIndex = 44
    Me.lbl_status.Text = "xStatus"
    Me.lbl_status.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'lbl_status_collection
    '
    Me.lbl_status_collection.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_status_collection.AutoSize = True
    Me.lbl_status_collection.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_status_collection.Location = New System.Drawing.Point(87, 63)
    Me.lbl_status_collection.Name = "lbl_status_collection"
    Me.lbl_status_collection.Size = New System.Drawing.Size(106, 13)
    Me.lbl_status_collection.TabIndex = 45
    Me.lbl_status_collection.Text = "xStatusCollection"
    Me.lbl_status_collection.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'gb_collection
    '
    Me.gb_collection.Controls.Add(Me.uc_unassign_terminal)
    Me.gb_collection.Controls.Add(Me.lbl_status_collection)
    Me.gb_collection.Controls.Add(Me.lbl_terminal_collection)
    Me.gb_collection.Controls.Add(Me.lbl_terminal)
    Me.gb_collection.Controls.Add(Me.lbl_status)
    Me.gb_collection.Location = New System.Drawing.Point(270, 48)
    Me.gb_collection.Name = "gb_collection"
    Me.gb_collection.Size = New System.Drawing.Size(241, 139)
    Me.gb_collection.TabIndex = 46
    Me.gb_collection.TabStop = False
    Me.gb_collection.Text = "xCollection"
    '
    'uc_unassign_terminal
    '
    Me.uc_unassign_terminal.DialogResult = System.Windows.Forms.DialogResult.None
    Me.uc_unassign_terminal.Location = New System.Drawing.Point(75, 94)
    Me.uc_unassign_terminal.Name = "uc_unassign_terminal"
    Me.uc_unassign_terminal.Size = New System.Drawing.Size(90, 30)
    Me.uc_unassign_terminal.TabIndex = 48
    Me.uc_unassign_terminal.ToolTipped = False
    Me.uc_unassign_terminal.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'lbl_terminal_collection
    '
    Me.lbl_terminal_collection.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_terminal_collection.AutoSize = True
    Me.lbl_terminal_collection.ForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_terminal_collection.Location = New System.Drawing.Point(87, 37)
    Me.lbl_terminal_collection.Name = "lbl_terminal_collection"
    Me.lbl_terminal_collection.Size = New System.Drawing.Size(120, 13)
    Me.lbl_terminal_collection.TabIndex = 47
    Me.lbl_terminal_collection.Text = "xTerminalCollection"
    Me.lbl_terminal_collection.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'lbl_terminal
    '
    Me.lbl_terminal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_terminal.AutoSize = True
    Me.lbl_terminal.Location = New System.Drawing.Point(22, 37)
    Me.lbl_terminal.Name = "lbl_terminal"
    Me.lbl_terminal.Size = New System.Drawing.Size(64, 13)
    Me.lbl_terminal.TabIndex = 46
    Me.lbl_terminal.Text = "xTerminal"
    Me.lbl_terminal.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'frm_tito_stacker_edit
    '
    Me.ClientSize = New System.Drawing.Size(625, 354)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_tito_stacker_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_collection.ResumeLayout(False)
    Me.gb_collection.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents chk_select_pre_term As System.Windows.Forms.CheckBox
  Friend WithEvents txt_notes As System.Windows.Forms.TextBox
  Friend WithEvents chk_deactivated As System.Windows.Forms.CheckBox
  Friend WithEvents ef_model As GUI_Controls.uc_entry_field
  Friend WithEvents ef_stacker_id As GUI_Controls.uc_entry_field
  Friend WithEvents uc_preassigned_term As GUI_Controls.uc_provider
  Friend WithEvents lbl_notes As System.Windows.Forms.Label
  Friend WithEvents lbl_status_collection As System.Windows.Forms.Label
  Friend WithEvents lbl_status As System.Windows.Forms.Label
  Friend WithEvents gb_collection As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_terminal_collection As System.Windows.Forms.Label
  Friend WithEvents lbl_terminal As System.Windows.Forms.Label
  Friend WithEvents uc_unassign_terminal As GUI_Controls.uc_button

End Class
