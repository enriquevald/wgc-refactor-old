<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_stackers_edit
  Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ef_stacker_id = New GUI_Controls.uc_entry_field
    Me.ef_model = New GUI_Controls.uc_entry_field
    Me.chk_deactivated = New System.Windows.Forms.CheckBox
    Me.lbl_preassigned = New System.Windows.Forms.Label
    Me.cmb_preassigned_prov = New System.Windows.Forms.ComboBox
    Me.cmb_inserted_term = New System.Windows.Forms.ComboBox
    Me.lbl_inserted = New System.Windows.Forms.Label
    Me.ef_stacker_status = New GUI_Controls.uc_entry_field
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.grid_collection_history = New GUI_Controls.uc_grid
    Me.lbl_notes = New System.Windows.Forms.Label
    Me.txt_notes = New System.Windows.Forms.TextBox
    Me.uc_dates = New GUI_Controls.uc_daily_session_selector
    Me.Panel2 = New System.Windows.Forms.Panel
    Me.btn_search_collections = New GUI_Controls.uc_button
    Me.gb_status = New System.Windows.Forms.GroupBox
    Me.opt_available = New System.Windows.Forms.RadioButton
    Me.opt_collection_pending = New System.Windows.Forms.RadioButton
    Me.opt_inserted_validated = New System.Windows.Forms.RadioButton
    Me.opt_inserted_pending = New System.Windows.Forms.RadioButton
    Me.lbl_grid_title = New System.Windows.Forms.Label
    Me.lbl_warning = New System.Windows.Forms.Label
    Me.panel_data.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.lbl_warning)
    Me.panel_data.Controls.Add(Me.lbl_grid_title)
    Me.panel_data.Controls.Add(Me.gb_status)
    Me.panel_data.Controls.Add(Me.btn_search_collections)
    Me.panel_data.Controls.Add(Me.Panel2)
    Me.panel_data.Controls.Add(Me.uc_dates)
    Me.panel_data.Controls.Add(Me.txt_notes)
    Me.panel_data.Controls.Add(Me.lbl_notes)
    Me.panel_data.Controls.Add(Me.grid_collection_history)
    Me.panel_data.Controls.Add(Me.Panel1)
    Me.panel_data.Controls.Add(Me.ef_stacker_status)
    Me.panel_data.Controls.Add(Me.cmb_inserted_term)
    Me.panel_data.Controls.Add(Me.lbl_inserted)
    Me.panel_data.Controls.Add(Me.cmb_preassigned_prov)
    Me.panel_data.Controls.Add(Me.lbl_preassigned)
    Me.panel_data.Controls.Add(Me.chk_deactivated)
    Me.panel_data.Controls.Add(Me.ef_model)
    Me.panel_data.Controls.Add(Me.ef_stacker_id)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(955, 593)
    '
    'ef_stacker_id
    '
    Me.ef_stacker_id.DoubleValue = 0
    Me.ef_stacker_id.IntegerValue = 0
    Me.ef_stacker_id.IsReadOnly = False
    Me.ef_stacker_id.Location = New System.Drawing.Point(7, 15)
    Me.ef_stacker_id.Name = "ef_stacker_id"
    Me.ef_stacker_id.Size = New System.Drawing.Size(320, 24)
    Me.ef_stacker_id.SufixText = "Sufix Text"
    Me.ef_stacker_id.SufixTextVisible = True
    Me.ef_stacker_id.TabIndex = 0
    Me.ef_stacker_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_stacker_id.TextValue = ""
    Me.ef_stacker_id.TextWidth = 120
    Me.ef_stacker_id.Value = ""
    '
    'ef_model
    '
    Me.ef_model.DoubleValue = 0
    Me.ef_model.IntegerValue = 0
    Me.ef_model.IsReadOnly = False
    Me.ef_model.Location = New System.Drawing.Point(7, 45)
    Me.ef_model.Name = "ef_model"
    Me.ef_model.Size = New System.Drawing.Size(320, 24)
    Me.ef_model.SufixText = "Sufix Text"
    Me.ef_model.SufixTextVisible = True
    Me.ef_model.TabIndex = 1
    Me.ef_model.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_model.TextValue = ""
    Me.ef_model.TextWidth = 120
    Me.ef_model.Value = ""
    '
    'chk_deactivated
    '
    Me.chk_deactivated.AutoSize = True
    Me.chk_deactivated.Location = New System.Drawing.Point(342, 15)
    Me.chk_deactivated.Name = "chk_deactivated"
    Me.chk_deactivated.Size = New System.Drawing.Size(68, 17)
    Me.chk_deactivated.TabIndex = 4
    Me.chk_deactivated.Text = "xActivo"
    Me.chk_deactivated.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.chk_deactivated.UseVisualStyleBackColor = True
    '
    'lbl_preassigned
    '
    Me.lbl_preassigned.Location = New System.Drawing.Point(10, 86)
    Me.lbl_preassigned.Name = "lbl_preassigned"
    Me.lbl_preassigned.Size = New System.Drawing.Size(116, 19)
    Me.lbl_preassigned.TabIndex = 4
    Me.lbl_preassigned.Text = "xPreassigned"
    Me.lbl_preassigned.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'cmb_preassigned_prov
    '
    Me.cmb_preassigned_prov.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cmb_preassigned_prov.FormattingEnabled = True
    Me.cmb_preassigned_prov.Location = New System.Drawing.Point(129, 84)
    Me.cmb_preassigned_prov.Name = "cmb_preassigned_prov"
    Me.cmb_preassigned_prov.Size = New System.Drawing.Size(198, 21)
    Me.cmb_preassigned_prov.TabIndex = 2
    '
    'cmb_inserted_term
    '
    Me.cmb_inserted_term.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cmb_inserted_term.FormattingEnabled = True
    Me.cmb_inserted_term.Location = New System.Drawing.Point(129, 111)
    Me.cmb_inserted_term.Name = "cmb_inserted_term"
    Me.cmb_inserted_term.Size = New System.Drawing.Size(198, 21)
    Me.cmb_inserted_term.TabIndex = 3
    '
    'lbl_inserted
    '
    Me.lbl_inserted.Location = New System.Drawing.Point(15, 111)
    Me.lbl_inserted.Name = "lbl_inserted"
    Me.lbl_inserted.Size = New System.Drawing.Size(110, 19)
    Me.lbl_inserted.TabIndex = 8
    Me.lbl_inserted.Text = "xInserted"
    Me.lbl_inserted.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'ef_stacker_status
    '
    Me.ef_stacker_status.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.ef_stacker_status.DoubleValue = 0
    Me.ef_stacker_status.Enabled = False
    Me.ef_stacker_status.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.ef_stacker_status.IntegerValue = 0
    Me.ef_stacker_status.IsReadOnly = False
    Me.ef_stacker_status.Location = New System.Drawing.Point(484, 15)
    Me.ef_stacker_status.Name = "ef_stacker_status"
    Me.ef_stacker_status.Size = New System.Drawing.Size(299, 25)
    Me.ef_stacker_status.SufixText = "Sufix Text"
    Me.ef_stacker_status.SufixTextVisible = True
    Me.ef_stacker_status.TabIndex = 6
    Me.ef_stacker_status.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_stacker_status.TextValue = ""
    Me.ef_stacker_status.TextWidth = 110
    Me.ef_stacker_status.Value = ""
    '
    'Panel1
    '
    Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel1.Location = New System.Drawing.Point(7, 194)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(952, 4)
    Me.Panel1.TabIndex = 17
    '
    'grid_collection_history
    '
    Me.grid_collection_history.CurrentCol = -1
    Me.grid_collection_history.CurrentRow = -1
    Me.grid_collection_history.Location = New System.Drawing.Point(7, 325)
    Me.grid_collection_history.Name = "grid_collection_history"
    Me.grid_collection_history.PanelRightVisible = False
    Me.grid_collection_history.Redraw = True
    Me.grid_collection_history.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.grid_collection_history.Size = New System.Drawing.Size(948, 268)
    Me.grid_collection_history.Sortable = False
    Me.grid_collection_history.SortAscending = True
    Me.grid_collection_history.SortByCol = 0
    Me.grid_collection_history.TabIndex = 11
    Me.grid_collection_history.ToolTipped = True
    Me.grid_collection_history.TopRow = -2
    '
    'lbl_notes
    '
    Me.lbl_notes.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_notes.AutoSize = True
    Me.lbl_notes.Location = New System.Drawing.Point(557, 45)
    Me.lbl_notes.Name = "lbl_notes"
    Me.lbl_notes.Size = New System.Drawing.Size(39, 13)
    Me.lbl_notes.TabIndex = 7
    Me.lbl_notes.Text = "Notas"
    Me.lbl_notes.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'txt_notes
    '
    Me.txt_notes.Location = New System.Drawing.Point(596, 42)
    Me.txt_notes.MaxLength = 500
    Me.txt_notes.Multiline = True
    Me.txt_notes.Name = "txt_notes"
    Me.txt_notes.Size = New System.Drawing.Size(185, 116)
    Me.txt_notes.TabIndex = 7
    '
    'uc_dates
    '
    Me.uc_dates.ClosingTime = 0
    Me.uc_dates.ClosingTimeEnabled = True
    Me.uc_dates.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dates.FromDateSelected = True
    Me.uc_dates.Location = New System.Drawing.Point(7, 220)
    Me.uc_dates.Name = "uc_dates"
    Me.uc_dates.Size = New System.Drawing.Size(257, 80)
    Me.uc_dates.TabIndex = 8
    Me.uc_dates.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dates.ToDateSelected = True
    '
    'Panel2
    '
    Me.Panel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel2.Location = New System.Drawing.Point(7, 315)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(952, 4)
    Me.Panel2.TabIndex = 21
    '
    'btn_search_collections
    '
    Me.btn_search_collections.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_search_collections.Location = New System.Drawing.Point(270, 270)
    Me.btn_search_collections.Name = "btn_search_collections"
    Me.btn_search_collections.Size = New System.Drawing.Size(90, 30)
    Me.btn_search_collections.TabIndex = 9
    Me.btn_search_collections.ToolTipped = False
    Me.btn_search_collections.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.opt_available)
    Me.gb_status.Controls.Add(Me.opt_collection_pending)
    Me.gb_status.Controls.Add(Me.opt_inserted_validated)
    Me.gb_status.Controls.Add(Me.opt_inserted_pending)
    Me.gb_status.Location = New System.Drawing.Point(342, 74)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(200, 106)
    Me.gb_status.TabIndex = 5
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xGroup Status"
    '
    'opt_available
    '
    Me.opt_available.AutoSize = True
    Me.opt_available.Location = New System.Drawing.Point(17, 20)
    Me.opt_available.Name = "opt_available"
    Me.opt_available.Size = New System.Drawing.Size(124, 17)
    Me.opt_available.TabIndex = 0
    Me.opt_available.TabStop = True
    Me.opt_available.Text = "xStatus Available"
    Me.opt_available.UseVisualStyleBackColor = True
    '
    'opt_collection_pending
    '
    Me.opt_collection_pending.AutoSize = True
    Me.opt_collection_pending.Location = New System.Drawing.Point(17, 80)
    Me.opt_collection_pending.Name = "opt_collection_pending"
    Me.opt_collection_pending.Size = New System.Drawing.Size(177, 17)
    Me.opt_collection_pending.TabIndex = 3
    Me.opt_collection_pending.TabStop = True
    Me.opt_collection_pending.Text = "xStatus Collection Pending"
    Me.opt_collection_pending.UseVisualStyleBackColor = True
    '
    'opt_inserted_validated
    '
    Me.opt_inserted_validated.AutoSize = True
    Me.opt_inserted_validated.Location = New System.Drawing.Point(17, 60)
    Me.opt_inserted_validated.Name = "opt_inserted_validated"
    Me.opt_inserted_validated.Size = New System.Drawing.Size(177, 17)
    Me.opt_inserted_validated.TabIndex = 2
    Me.opt_inserted_validated.TabStop = True
    Me.opt_inserted_validated.Text = "xStatus Inserted Validated"
    Me.opt_inserted_validated.UseVisualStyleBackColor = True
    '
    'opt_inserted_pending
    '
    Me.opt_inserted_pending.AutoSize = True
    Me.opt_inserted_pending.Location = New System.Drawing.Point(17, 40)
    Me.opt_inserted_pending.Name = "opt_inserted_pending"
    Me.opt_inserted_pending.Size = New System.Drawing.Size(169, 17)
    Me.opt_inserted_pending.TabIndex = 1
    Me.opt_inserted_pending.TabStop = True
    Me.opt_inserted_pending.Text = "xStatus Inserted Pending"
    Me.opt_inserted_pending.UseVisualStyleBackColor = True
    '
    'lbl_grid_title
    '
    Me.lbl_grid_title.AutoSize = True
    Me.lbl_grid_title.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_grid_title.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
    Me.lbl_grid_title.Location = New System.Drawing.Point(10, 201)
    Me.lbl_grid_title.Name = "lbl_grid_title"
    Me.lbl_grid_title.Size = New System.Drawing.Size(126, 13)
    Me.lbl_grid_title.TabIndex = 25
    Me.lbl_grid_title.Text = "xCollectionHistory"
    Me.lbl_grid_title.Visible = False
    '
    'lbl_warning
    '
    Me.lbl_warning.ForeColor = System.Drawing.Color.Navy
    Me.lbl_warning.Location = New System.Drawing.Point(17, 141)
    Me.lbl_warning.Name = "lbl_warning"
    Me.lbl_warning.Size = New System.Drawing.Size(310, 30)
    Me.lbl_warning.TabIndex = 27
    Me.lbl_warning.Text = "xWarning"
    '
    'frm_stackers_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1059, 604)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Name = "frm_stackers_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_stackers_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_model As GUI_Controls.uc_entry_field
  Friend WithEvents ef_stacker_id As GUI_Controls.uc_entry_field
  Friend WithEvents chk_deactivated As System.Windows.Forms.CheckBox
  Friend WithEvents cmb_preassigned_prov As System.Windows.Forms.ComboBox
  Friend WithEvents lbl_preassigned As System.Windows.Forms.Label
  Friend WithEvents cmb_inserted_term As System.Windows.Forms.ComboBox
  Friend WithEvents lbl_inserted As System.Windows.Forms.Label
  Friend WithEvents ef_stacker_status As GUI_Controls.uc_entry_field
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
  Friend WithEvents grid_collection_history As GUI_Controls.uc_grid
  Friend WithEvents lbl_notes As System.Windows.Forms.Label
  Friend WithEvents txt_notes As System.Windows.Forms.TextBox
  Friend WithEvents uc_dates As GUI_Controls.uc_daily_session_selector
  Friend WithEvents Panel2 As System.Windows.Forms.Panel
  Friend WithEvents btn_search_collections As GUI_Controls.uc_button
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents opt_collection_pending As System.Windows.Forms.RadioButton
  Friend WithEvents opt_inserted_validated As System.Windows.Forms.RadioButton
  Friend WithEvents opt_inserted_pending As System.Windows.Forms.RadioButton
  Friend WithEvents opt_available As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_grid_title As System.Windows.Forms.Label
  Friend WithEvents lbl_warning As System.Windows.Forms.Label
End Class
