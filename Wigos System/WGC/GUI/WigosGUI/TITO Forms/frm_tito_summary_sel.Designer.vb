<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_tito_summary_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ds_from_to = New GUI_Controls.uc_daily_session_selector
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ds_from_to)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1160, 98)
    Me.panel_filter.Controls.SetChildIndex(Me.ds_from_to, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 102)
    Me.panel_data.Size = New System.Drawing.Size(1160, 502)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1154, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1154, 4)
    '
    'ds_from_to
    '
    Me.ds_from_to.ClosingTime = 0
    Me.ds_from_to.ClosingTimeEnabled = False
    Me.ds_from_to.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.ds_from_to.FromDateSelected = True
    Me.ds_from_to.Location = New System.Drawing.Point(6, 6)
    Me.ds_from_to.Name = "ds_from_to"
    Me.ds_from_to.Size = New System.Drawing.Size(250, 84)
    Me.ds_from_to.TabIndex = 11
    Me.ds_from_to.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.ds_from_to.ToDateSelected = True
    '
    'frm_tito_summary_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1170, 608)
    Me.Name = "frm_tito_summary_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_tito_summary_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ds_from_to As GUI_Controls.uc_daily_session_selector
End Class
