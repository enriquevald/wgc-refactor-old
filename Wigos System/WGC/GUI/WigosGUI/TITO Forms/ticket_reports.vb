'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_bank_ticket_report_sel
' DESCRIPTION:   This screen allows to view the payment orders.
' AUTHOR:        Humberto Braojos
' CREATION DATE: 14-JAN-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 21-JUN-2013  HBB    Initial version

'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports WSI.Common
Imports System.Data.SqlClient

Public Class frm_ticket_reports
  Inherits frm_base_sel

#Region "Constants"
  Const GRID_COLUMNS_COUNT As Int32 = 13
  Const GRID_HEADERS_COUNT As Int32 = 2

  'Index Columns
  Const GRID_COLUMN_TITO_CREATION_DATE As Int32 = 0
  Const GRID_COLUMN_TITO_TICKET_NUMBER As Int32 = 1
  Const GRID_COLUMN_TITO_TYPE As Int32 = 2
  Const GRID_COLUMN_TITO_CREDIT As Int32 = 3
  Const GRID_COLUMN_TITO_CREATION_TERMINAL As Int32 = 4
  Const GRID_COLUMN_TITO_CANCEL_OR_REDEEM_TERMINAL As Int32 = 5
  Const GRID_COLUMN_TITO_USER_CREATION As Int32 = 6
  Const GRID_COLUMN_TITO_USER_CANCEL_OR_REDEEM As Int32 = 7
  Const GRID_COLUMN_TITO_STATE As Int32 = 8
  Const GRID_COLUMN_TITO_CANCEL_OR_REDEEM_DATE As Int32 = 9
  Const GRID_COLUMN_TITO_EXPIRATION_DATE As Int32 = 10
  Const GRID_COLUMN_TITO_COLLECTION_ID As Int32 = 11
  Const GRID_COLUMN_TITO_ASSOCIATED_PROMOTION As Int32 = 12

  'Width
  Const GRID_WIDTH_TITO_CREATION_DATE As Int32 = 2200
  Const GRID_WIDTH_TITO_TICKET_NUMBER As Int32 = 1600
  Const GRID_WIDTH_TITO_TYPE As Int32 = 1600
  Const GRID_WIDTH_TITO_CREDIT As Int32 = 1600
  Const GRID_WIDTH_TITO_CREATION_TERMINAL As Int32 = 1600
  Const GRID_WIDTH_TITO_CREATION_TERMINAL_TYPE As Int32 = 1600
  Const GRID_WIDTH_TITO_CANCEL_OR_REDEEM_TERMINAL As Int32 = 1600
  Const GRID_WIDTH_TITO_CANCEL_OR_REDEEM_TERMINAL_TYPE As Int32 = 1600
  Const GRID_WIDTH_TITO_USER_CREATION As Int32 = 2500
  Const GRID_WIDTH_TITO_USER_CANCEL_OR_REDEEM As Int32 = 2500
  Const GRID_WIDTH_TITO_STATE As Int32 = 1600
  Const GRID_WIDTH_TITO_CANCEL_OR_REDEEM_DATE As Int32 = 2200
  Const GRID_WIDTH_TITO_EXPIRATION_DATE As Int32 = 2200
  Const GRID_WIDTH_TITO_COLLECTION_ID As Int32 = 1600
  Const GRID_WIDTH_TITO_ASSOCIATED_PROMOTION As Int32 = 2500

  'SQL COLUMNS
  Const SQL_COLUMN_TITO_TICKET_NUMBER As Int32 = 1
  Const SQL_COLUMN_TITO_CREDIT As Int32 = 2
  Const SQL_COLUMN_TITO_STATE As Int32 = 3
  Const SQL_COLUMN_TITO_TYPE As Int32 = 4
  Const SQL_COLUMN_TITO_CREATION_DATE As Int32 = 5
  Const SQL_COLUMN_TITO_CREATION_TERMINAL As Int32 = 6
  Const SQL_COLUMN_TITO_CREATION_TERMINAL_TYPE As Int32 = 7
  Const SQL_COLUMN_TITO_EXPIRATION_DATE As Int32 = 8
  Const SQL_COLUMN_TITO_COLLECTION_ID As Int32 = 9
  Const SQL_COLUMN_TITO_USER_CREATION As Int32 = 10
  Const SQL_COLUMN_TITO_CANCEL_OR_REDEEM_TERMINAL As Int32 = 11
  Const SQL_COLUMN_TITO_CANCEL_OR_REDEEM_DATE As Int32 = 12
  Const SQL_COLUMN_TITO_USER_CANCEL_OR_REDEEM As Int32 = 13
  Const SQL_COLUMN_TITO_CANCEL_OR_REDEEM_TERMINAL_TYPE As Int32 = 14
  Const SQL_COLUMN_TITO_ASSOCIATED_PROMOTION As Int32 = 15

  'Cshier Grid
  Private Const GRID_CASHIER_HEADER_ROWS As Integer = 0
  Private Const GRID_CASHIER_COLUMNS As Integer = 3
  Private Const GRID_CASHIER_ID As Integer = 0
  Private Const GRID_CASHIER_COLUMN_CHECKED As Integer = 1
  Private Const GRID_CASHIER_COLUMN_DESCRIPTION As Integer = 2

  'Bank Grid Width
  Private Const GRID_CASHIER_WIDTH_COLUMN_DESCRIPTION As Integer = 3355
  Private Const GRID_CASHIER_WIDTH_COLUMN_CHECKED As Integer = 300

#End Region

#Region "MEMBERS"
  Private m_date_from As String
  Private m_date_to As String
  Private m_filter_by_date As String
  Private m_state As String
  Private m_ticket_type As String
  Private m_promotion_type As String
  Private m_promotion_name As String
  Private m_terminal As String
  Private m_cashiers As String
#End Region

#Region "Overrides Functions"

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2124)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False

    gb_associated_promotion.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2122)
    chb_redeem_promotion.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125)
    chb_non_redeemable_promotion.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2126)
    chb_points_promotion.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2127)
    chb_cashout_ticket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2129)
    chb_playable_only.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2130)
    chb_jackpot_receipt.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2131)
    chb_hand_pay_receipt.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2132)
    chb_purchase_ticket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2133)
    gb_tocket_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2134)
    gb_state.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2135)
    chb_valid.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2136)
    chb_expired.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2137)
    chb_redeemed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2138)
    ef_promotion_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2111)
    ef_promotion_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 20)
    chb_creation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2108)
    chb_cancelation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2120)
    chb_expiration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2123)
    gb_date_filter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2118)
    gb_cashiers.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2200)
    bt_check_all.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2201)
    bt_uncheck_all.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2202)
    chb_canceled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2116)
    dg_cashiers.PanelRightVisible = False
    dg_cashiers.Sortable = False
    dg_cashiers.TopRow = -2

    Call GUI_StyleSheet()
    Call SetDefaultDate()
    GUI_StyleSheetCashiers()
    GUI_GetSqlQueryCashiers()

    ' Providers - Terminals
    Call Me.uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())


  End Sub ' GUI_InitControls

  ' PURPOSE: Call GUI_ButtonClick with the Cursor in Wait Mode. Finally restore Cursor to Default.
  '
  '  PARAMS:
  '     - INPUT:
  '           - ButtonId As ENUM_BUTTON
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     -
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call GUI_StyleSheet()
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    _sb.AppendLine("    SELECT    TI_TICKET_ID                                                                  ")
    _sb.AppendLine("             ,TI_VALIDATION_NUMBER                                                          ")
    _sb.AppendLine("             ,TI_AMOUNT                                                                     ")
    _sb.AppendLine("             ,TI_STATE                                                                      ")
    _sb.AppendLine("             ,TT_NAME                                                                       ")
    _sb.AppendLine("             ,TI_CREATED_DATETIME                                                           ")
    _sb.AppendLine("             ,CASE WHEN TI_CREATED_TERMINAL_TYPE = 0 THEN CASHIER_TERMINAL_CREATION.CT_NAME ELSE TERMINAL_CREATION.TE_NAME  END AS TE_TERMINAL_NAME ")
    _sb.AppendLine("             ,TI_CREATED_TERMINAL_TYPE                                                      ")
    _sb.AppendLine("             ,TI_EXPIRATION_DATETIME                                                        ")
    _sb.AppendLine("             ,TI_STACKER_COLLECTION_ID                                                      ")
    _sb.AppendLine("             ,ACCOUNTS_CREATION.AC_HOLDER_NAME                                                                ")
    _sb.AppendLine("             ,CASE WHEN TI_LAST_TERMINAL_TYPE = 0 THEN CASHIER_TERMINALS_LAST_ACTION.CT_NAME ELSE TERMINAL_LAST_ACTION.TE_NAME  END AS TI_LAST_TERMINAL_ID                                                           ")
    _sb.AppendLine("             ,TI_LAST_DATETIME                                                              ")
    _sb.AppendLine("             ,ACCOUNTS_LAST_ACTION.AC_HOLDER_NAME                                           ")
    _sb.AppendLine("             ,TI_LAST_TERMINAL_TYPE                                                         ")
    _sb.AppendLine("             ,PM_NAME                                                                       ")
    _sb.AppendLine("      FROM    TICKETS                                                                       ")
    _sb.AppendLine("INNER JOIN    TICKET_TYPES ON TICKETS.TI_TYPE_ID = TICKET_TYPES.TT_TYPE_ID                  ")
    _sb.AppendLine(" LEFT JOIN    TERMINALS TERMINAL_CREATION ON TICKETS.TI_CREATED_TERMINAL_ID = TERMINAL_CREATION.TE_TERMINAL_ID")
    _sb.AppendLine(" LEFT JOIN    CASHIER_TERMINALS CASHIER_TERMINAL_CREATION ON  TICKETS.TI_CREATED_TERMINAL_ID = CASHIER_TERMINAL_CREATION.CT_CASHIER_ID")
    _sb.AppendLine(" LEFT JOIN    TERMINALS TERMINAL_LAST_ACTION  ON TICKETS.TI_LAST_TERMINAL_ID = TERMINAL_LAST_ACTION.TE_TERMINAL_ID")
    _sb.AppendLine(" LEFT JOIN    CASHIER_TERMINALS CASHIER_TERMINALS_LAST_ACTION ON  TICKETS.TI_LAST_TERMINAL_ID = CASHIER_TERMINALS_LAST_ACTION.CT_CASHIER_ID     ")
    _sb.AppendLine(" LEFT JOIN    ACCOUNTS ACCOUNTS_CREATION ON TICKETS.TI_CREATED_ACCOUNT_ID = ACCOUNTS_CREATION.AC_ACCOUNT_ID")
    _sb.AppendLine(" LEFT JOIN    ACCOUNTS ACCOUNTS_LAST_ACTION ON TICKETS.TI_LAST_ACCOUNT_ID = ACCOUNTS_LAST_ACTION.AC_ACCOUNT_ID")
    _sb.AppendLine(" LEFT JOIN    PROMOTIONS ON PROMOTIONS.PM_PROMOTION_ID = TICKETS.TI_ASSOCIATED_PROMOTION_ID ")
    _sb.AppendLine(GetSqlWhere())
    _sb.AppendLine("  ORDER BY    TI_CREATED_DATETIME DESC                                                      ")

    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    SetDefaultValues()
    Call Me.uc_pr_list.SetDefaultValues()
  End Sub 'GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    If Me.ds_from_to.FromDateSelected And Me.ds_from_to.ToDateSelected Then
      If Me.ds_from_to.FromDate > Me.ds_from_to.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.ds_from_to.Focus()

        Return False
      End If
    End If

    Return True

  End Function ' GUI_FilterCheck


  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.ds_from_to
  End Sub ' GUI_SetInitialFocus


  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_CREATION_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_TITO_CREATION_DATE), _
                                                                          ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                          ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_TICKET_NUMBER).Value = DbRow.Value(SQL_COLUMN_TITO_TICKET_NUMBER)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_TYPE).Value = DbRow.Value(SQL_COLUMN_TITO_TYPE)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_CREDIT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TITO_CREDIT))

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_CREATION_TERMINAL).Value = DbRow.Value(SQL_COLUMN_TITO_CREATION_TERMINAL)

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TITO_CANCEL_OR_REDEEM_TERMINAL)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_CANCEL_OR_REDEEM_TERMINAL).Value = DbRow.Value(SQL_COLUMN_TITO_CANCEL_OR_REDEEM_TERMINAL)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TITO_USER_CREATION)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_USER_CREATION).Value = DbRow.Value(SQL_COLUMN_TITO_USER_CREATION)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TITO_USER_CANCEL_OR_REDEEM)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_USER_CANCEL_OR_REDEEM).Value = DbRow.Value(SQL_COLUMN_TITO_USER_CANCEL_OR_REDEEM)
    End If

    'state
    Select Case (DbRow.Value(SQL_COLUMN_TITO_STATE))
      Case WSI.Common.TITO_STATES.VALID
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_STATE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2136)
      Case WSI.Common.TITO_STATES.CANCELED
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_STATE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2116)
      Case WSI.Common.TITO_STATES.REDEEMED
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_STATE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2138)
      Case WSI.Common.TITO_STATES.EXPIRED
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_STATE).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2137)

    End Select

    'Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_STATE).Value = DbRow.Value(SQL_COLUMN_TITO_STATE)

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TITO_CANCEL_OR_REDEEM_DATE)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_CANCEL_OR_REDEEM_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_TITO_CANCEL_OR_REDEEM_DATE), _
                                                                          ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                          ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TITO_EXPIRATION_DATE)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_EXPIRATION_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_TITO_EXPIRATION_DATE), _
                                                                          ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                          ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TITO_COLLECTION_ID)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_COLLECTION_ID).Value = DbRow.Value(SQL_COLUMN_TITO_COLLECTION_ID)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TITO_ASSOCIATED_PROMOTION)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_ASSOCIATED_PROMOTION).Value = DbRow.Value(SQL_COLUMN_TITO_ASSOCIATED_PROMOTION)
    End If

    Return True

  End Function

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim _no_one As Boolean
    Dim _all As Boolean

    m_date_from = ""
    m_date_to = ""
    m_filter_by_date = ""
    m_state = ""
    m_ticket_type = ""
    m_promotion_type = ""
    m_promotion_name = ""
    m_terminal = ""

    'date from
    If Me.ds_from_to.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.ds_from_to.FromDate.AddHours(ds_from_to.ClosingTime), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    'date to
    If Me.ds_from_to.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.ds_from_to.ToDate.AddHours(ds_from_to.ClosingTime), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' filter by date
    _no_one = True
    _all = True
    If chb_creation.Checked Then
      m_filter_by_date = m_filter_by_date & Me.chb_creation.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chb_cancelation.Checked Then
      m_filter_by_date = m_filter_by_date & Me.chb_cancelation.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chb_expiration.Checked Then
      m_filter_by_date = m_filter_by_date & Me.chb_expiration.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If m_filter_by_date.Length <> 0 Then
      m_filter_by_date = Strings.Left(m_filter_by_date, Len(m_filter_by_date) - 2)
    End If

    If _no_one Or _all Then
      m_filter_by_date = GLB_NLS_GUI_ALARMS.GetString(277)
    End If

    ' state 
    _no_one = True
    _all = True
    If chb_valid.Checked Then
      m_state = m_state & Me.chb_valid.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chb_canceled.Checked Then
      m_state = m_state & Me.chb_canceled.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chb_expired.Checked Then
      m_state = m_state & Me.chb_expired.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chb_redeemed.Checked Then
      m_state = m_state & Me.chb_redeemed.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If m_state.Length <> 0 Then
      m_state = Strings.Left(m_state, Len(m_state) - 2)
    End If

    If _no_one Or _all Then
      m_state = GLB_NLS_GUI_ALARMS.GetString(277)
    End If

    'ticket type
    _no_one = True
    _all = True
    If chb_cashout_ticket.Checked Then
      m_ticket_type = m_ticket_type & Me.chb_cashout_ticket.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chb_playable_only.Checked Then
      m_ticket_type = m_ticket_type & Me.chb_playable_only.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chb_jackpot_receipt.Checked Then
      m_ticket_type = m_ticket_type & Me.chb_jackpot_receipt.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chb_hand_pay_receipt.Checked Then
      m_ticket_type = m_ticket_type & Me.chb_hand_pay_receipt.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chb_purchase_ticket.Checked Then
      m_ticket_type = m_ticket_type & Me.chb_purchase_ticket.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If m_ticket_type.Length <> 0 Then
      m_ticket_type = Strings.Left(m_ticket_type, Len(m_ticket_type) - 2)
    End If

    If _no_one Or _all Then
      m_ticket_type = GLB_NLS_GUI_ALARMS.GetString(277)
    End If

    ' promotion name
    If ef_promotion_name.TextValue.Trim.Length > 0 Then
      m_promotion_name = ef_promotion_name.TextValue.Trim
    End If

    ' promotion type
    _no_one = True
    _all = True
    If chb_redeem_promotion.Checked And chb_redeem_promotion.Enabled Then
      m_promotion_type = m_promotion_type & Me.chb_redeem_promotion.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chb_non_redeemable_promotion.Checked And chb_non_redeemable_promotion.Enabled Then
      m_promotion_type = m_promotion_type & Me.chb_non_redeemable_promotion.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chb_points_promotion.Checked And chb_points_promotion.Enabled Then
      m_promotion_type = m_promotion_type & Me.chb_points_promotion.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If m_promotion_type.Length <> 0 Then
      m_promotion_type = Strings.Left(m_promotion_type, Len(m_promotion_type) - 2)
    End If

    If _no_one Or _all Then
      m_promotion_type = GLB_NLS_GUI_ALARMS.GetString(277)
    End If

    ' Terminals 
    If Me.uc_pr_list.opt_all_types.Checked Then
      m_terminal = Me.uc_pr_list.opt_all_types.Text
    ElseIf Me.uc_pr_list.opt_several_types.Checked Then
      m_terminal = Me.uc_pr_list.opt_several_types.Text
    Else
      m_terminal = Me.uc_pr_list.cmb_terminal.TextValue
    End If

  End Sub 'GUI_ReportUpdateFilters


  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1328), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1329), m_date_to)
    'PrintData.SetFilter("", "", True)
    'PrintData.SetFilter("", "", True)
    'PrintData.SetFilter("", "", True)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2118), m_filter_by_date)
    'PrintData.SetFilter("", "", True)
    'PrintData.SetFilter("", "", True)
    'PrintData.SetFilter("", "", True)
    'PrintData.SetFilter("", "", True)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2135), m_state)
    'PrintData.SetFilter("", "", True)
    'PrintData.SetFilter("", "", True)
    'PrintData.SetFilter("", "", True)
    'PrintData.SetFilter("", "", True)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2134), m_ticket_type)
    'PrintData.SetFilter("", "", True)
    'PrintData.SetFilter("", "", True)
    'PrintData.SetFilter("", "", True)
    'PrintData.SetFilter("", "", True)
    PrintData.FilterHeaderWidth(2) = 2000
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2195), m_promotion_type)
    'PrintData.SetFilter("", "", True)

    'PrintData.SetFilter("", "", True)
    'PrintData.SetFilter("", "", True)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2194), m_promotion_name)
    'PrintData.SetFilter("", "", True)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminal)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2200), m_cashiers)

    'PrintData.FilterHeaderWidth(2) = 1500
    'PrintData.FilterValueWidth(2) = 4000
  End Sub


#End Region

#Region "Private Functions"

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS_COUNT, GRID_HEADERS_COUNT)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' CREATION DATE
      .Column(GRID_COLUMN_TITO_CREATION_DATE).Header(0).Text = ""
      .Column(GRID_COLUMN_TITO_CREATION_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2108)
      .Column(GRID_COLUMN_TITO_CREATION_DATE).Width = GRID_WIDTH_TITO_CREATION_DATE
      .Column(GRID_COLUMN_TITO_CREATION_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' TICKET NUMBER
      .Column(GRID_COLUMN_TITO_TICKET_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2109)
      .Column(GRID_COLUMN_TITO_TICKET_NUMBER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2110)
      .Column(GRID_COLUMN_TITO_TICKET_NUMBER).Width = GRID_WIDTH_TITO_TICKET_NUMBER
      .Column(GRID_COLUMN_TITO_TICKET_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' TYPE
      .Column(GRID_COLUMN_TITO_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2109)
      .Column(GRID_COLUMN_TITO_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2112)
      .Column(GRID_COLUMN_TITO_TYPE).Width = GRID_WIDTH_TITO_TYPE
      .Column(GRID_COLUMN_TITO_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' CREDIT
      .Column(GRID_COLUMN_TITO_CREDIT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2109)
      .Column(GRID_COLUMN_TITO_CREDIT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2113)
      .Column(GRID_COLUMN_TITO_CREDIT).Width = GRID_WIDTH_TITO_CREDIT
      .Column(GRID_COLUMN_TITO_CREDIT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' GENERATION TERMINAL
      .Column(GRID_COLUMN_TITO_CREATION_TERMINAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2106)
      .Column(GRID_COLUMN_TITO_CREATION_TERMINAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2119)
      .Column(GRID_COLUMN_TITO_CREATION_TERMINAL).Width = GRID_WIDTH_TITO_CREATION_TERMINAL
      .Column(GRID_COLUMN_TITO_CREATION_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' CANCEL/REDEEM TERMINAL
      .Column(GRID_COLUMN_TITO_CANCEL_OR_REDEEM_TERMINAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2106)
      .Column(GRID_COLUMN_TITO_CANCEL_OR_REDEEM_TERMINAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2120)
      .Column(GRID_COLUMN_TITO_CANCEL_OR_REDEEM_TERMINAL).Width = GRID_WIDTH_TITO_CANCEL_OR_REDEEM_TERMINAL
      .Column(GRID_COLUMN_TITO_CANCEL_OR_REDEEM_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' CREATION USER
      .Column(GRID_COLUMN_TITO_USER_CREATION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2107)
      .Column(GRID_COLUMN_TITO_USER_CREATION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2119)
      .Column(GRID_COLUMN_TITO_USER_CREATION).Width = GRID_WIDTH_TITO_USER_CREATION
      .Column(GRID_COLUMN_TITO_USER_CREATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' CANCEL/REDEEM USER 
      .Column(GRID_COLUMN_TITO_USER_CANCEL_OR_REDEEM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2116)
      .Column(GRID_COLUMN_TITO_USER_CANCEL_OR_REDEEM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2120)
      .Column(GRID_COLUMN_TITO_USER_CANCEL_OR_REDEEM).Width = GRID_WIDTH_TITO_USER_CANCEL_OR_REDEEM
      .Column(GRID_COLUMN_TITO_USER_CANCEL_OR_REDEEM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' STATE 
      .Column(GRID_COLUMN_TITO_STATE).Header(0).Text = ""
      .Column(GRID_COLUMN_TITO_STATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2117)
      .Column(GRID_COLUMN_TITO_STATE).Width = GRID_WIDTH_TITO_STATE
      .Column(GRID_COLUMN_TITO_STATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' DATE REDEEMED/CANCELED
      .Column(GRID_COLUMN_TITO_CANCEL_OR_REDEEM_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2118)
      .Column(GRID_COLUMN_TITO_CANCEL_OR_REDEEM_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2120)
      .Column(GRID_COLUMN_TITO_CANCEL_OR_REDEEM_DATE).Width = GRID_WIDTH_TITO_USER_CANCEL_OR_REDEEM
      .Column(GRID_COLUMN_TITO_CANCEL_OR_REDEEM_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' DATE EXPIRATION
      .Column(GRID_COLUMN_TITO_EXPIRATION_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2118)
      .Column(GRID_COLUMN_TITO_EXPIRATION_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2123)
      .Column(GRID_COLUMN_TITO_EXPIRATION_DATE).Width = GRID_WIDTH_TITO_EXPIRATION_DATE
      .Column(GRID_COLUMN_TITO_EXPIRATION_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' COLLECTION ID
      .Column(GRID_COLUMN_TITO_COLLECTION_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_TITO_COLLECTION_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2121)
      .Column(GRID_COLUMN_TITO_COLLECTION_ID).Width = GRID_WIDTH_TITO_COLLECTION_ID
      .Column(GRID_COLUMN_TITO_COLLECTION_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' ASSOCIATED PROMOTION
      .Column(GRID_COLUMN_TITO_ASSOCIATED_PROMOTION).Header(0).Text = ""
      .Column(GRID_COLUMN_TITO_ASSOCIATED_PROMOTION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2122)
      .Column(GRID_COLUMN_TITO_ASSOCIATED_PROMOTION).Width = GRID_WIDTH_TITO_ASSOCIATED_PROMOTION
      .Column(GRID_COLUMN_TITO_ASSOCIATED_PROMOTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub

  ' PURPOSE: Sets the defaul date values to the daily_session_selector control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultDate()

    Dim _today_opening As Date

    _today_opening = WSI.Common.Misc.TodayOpening()

    ds_from_to.FromDate = _today_opening
    ds_from_to.FromDateSelected = True
    ds_from_to.ToDate = _today_opening.AddDays(1)
    ds_from_to.ToDateSelected = True
    ds_from_to.ClosingTime = _today_opening.Hour

  End Sub ' SetDefaultDate

  ' PURPOSE: Set default values to the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SetDefaultValues()

    ef_promotion_name.Value = ""
    chb_redeem_promotion.Checked = False
    chb_non_redeemable_promotion.Checked = False
    chb_points_promotion.Checked = False
    chb_cashout_ticket.Checked = False
    chb_playable_only.Checked = False
    chb_jackpot_receipt.Checked = False
    chb_hand_pay_receipt.Checked = False
    chb_purchase_ticket.Checked = False
    chb_valid.Checked = False
    chb_expired.Checked = False
    chb_redeemed.Checked = False
    chb_creation.Checked = True
    Call SetDefaultDate()
    Call UncheckAllCahiers()

  End Sub ' SetDefaultValues

  ' PURPOSE: Get the sql 'where' statement
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - an string with the 'where' statement
  Private Function GetSqlWhere() As String

    Dim _str_where As String
    Dim _provider_list As String
    Dim _state As String
    Dim _ticket_type As String
    Dim _associated_promotion As String
    Dim _cashiers_list As String
    Dim _cashiers_and_terminals As String

    _str_where = ""
    _state = ""
    _ticket_type = ""
    _provider_list = ""
    _associated_promotion = ""
    _cashiers_list = ""
    _cashiers_and_terminals = ""

    'date filter
    If Not (chb_creation.Checked Or chb_cancelation.Checked Or chb_expiration.Checked) Then
      _str_where = ds_from_to.GetSqlFilterCondition("TI_CREATED_DATETIME")
    End If
    If chb_creation.Checked Then
      _str_where = ds_from_to.GetSqlFilterCondition("TI_CREATED_DATETIME")
    End If
    If chb_cancelation.Checked Then
      If _str_where.Length > 0 Then
        _str_where = _str_where & " OR " & ds_from_to.GetSqlFilterCondition("TI_EXPIRATION_DATETIME")
      Else
        _str_where = ds_from_to.GetSqlFilterCondition("TI_EXPIRATION_DATETIME")
      End If
    End If
    If chb_expiration.Checked Then
      If _str_where.Length > 0 Then
        _str_where = _str_where & " OR " & ds_from_to.GetSqlFilterCondition("TI_LAST_DATETIME")
      Else
        _str_where = ds_from_to.GetSqlFilterCondition("TI_LAST_DATETIME")
      End If
    End If

    If _str_where.Length > 0 Then
      _str_where = "(" & _str_where & ") AND "
    End If

    'State filters
    If chb_valid.Checked Then
      _state = " TI_STATE = 0"
    End If
    If chb_canceled.Checked Then
      If _state.Length > 0 Then
        _state = _state & " OR TI_STATE = 1"
      Else
        _state = _state & " TI_STATE = 1"
      End If
    End If
    If chb_redeemed.Checked Then
      If _state.Length > 0 Then
        _state = _state & " OR TI_STATE = 2"
      Else
        _state = " TI_STATE = 2"
      End If
    End If
    If chb_expired.Checked Then
      If _state.Length > 0 Then
        _state = _state & " OR TI_STATE = 3"
      Else
        _state = _state & " TI_STATE = 3"
      End If
    End If

    If _state.Length > 0 Then
      _str_where = _str_where & "(" & _state & ")AND"
    End If

    'Ticket Type filters
    If chb_cashout_ticket.Checked Then
      _ticket_type = _ticket_type & " TI_TYPE_ID = 2"
    End If

    If chb_playable_only.Checked Then
      If _ticket_type.Length > 0 Then
        _ticket_type = _ticket_type & " OR TI_TYPE_ID = 1"
      Else
        _ticket_type = " TI_TYPE_ID = 1"
      End If
    End If

    If chb_jackpot_receipt.Checked Then
      If _ticket_type.Length > 0 Then
        _ticket_type = _ticket_type & " OR TI_TYPE_ID = 7"
      Else
        _ticket_type = " TI_TYPE_ID = 7"
      End If

    End If

    If chb_hand_pay_receipt.Checked Then
      If _ticket_type.Length > 0 Then
        _ticket_type = _ticket_type & " OR TI_TYPE_ID = 8"
      Else
        _ticket_type = " TI_TYPE_ID = 8"
      End If

    End If

    If chb_purchase_ticket.Checked Then
      If _ticket_type.Length > 0 Then
        _ticket_type = _ticket_type & " OR TI_TYPE_ID = 9"
      Else
        _ticket_type = " TI_TYPE_ID = 9"
      End If

    End If

    If _ticket_type.Length > 0 Then
      _str_where = _str_where & "(" & _ticket_type & ") AND"
    End If

    'Associated Promotion filters

    If ef_promotion_name.TextValue.Trim <> "" Then
      _str_where = _str_where & " PM_NAME like'%" & ef_promotion_name.TextValue.Trim & "%' AND"
    End If

    If chb_redeem_promotion.Checked And chb_redeem_promotion.Enabled Then
      _associated_promotion = " PM_CREDIT_TYPE = " & WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
    End If
    If chb_non_redeemable_promotion.Checked And chb_non_redeemable_promotion.Enabled Then
      If _associated_promotion.Length > 0 Then
        _associated_promotion = _associated_promotion & " OR PM_CREDIT_TYPE = " & WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR2 & " OR PM_CREDIT_TYPE = " & WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1
      Else
        _associated_promotion = " PM_CREDIT_TYPE = " & WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR2 & " OR PM_CREDIT_TYPE = " & WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1
      End If
    End If
    If chb_points_promotion.Checked And chb_points_promotion.Enabled Then
      If _associated_promotion.Length > 0 Then
        _associated_promotion = _associated_promotion & " OR PM_CREDIT_TYPE = " & WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.POINT
      Else
        _associated_promotion = " PM_CREDIT_TYPE = " & WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.POINT
      End If
    End If
    If _associated_promotion.Length > 0 Then
      _str_where = _str_where & "(" & _associated_promotion & ") AND "
    End If

    ' Filter Terminal/Cashiers

    'filter cashiers
    _cashiers_list = GetCashiersSelected(GRID_CASHIER_ID)

    If _cashiers_list.Length > 0 Then
      _cashiers_and_terminals = " TI_CREATED_TERMINAL_ID IN (" & _cashiers_list & ") OR TI_LAST_TERMINAL_ID IN (" & _cashiers_list & " ) OR "
    End If

    _provider_list = Me.uc_pr_list.GetProviderIdListSelected()
    If Me.uc_pr_list.opt_one_terminal.Checked Then
      _cashiers_and_terminals = _cashiers_and_terminals & "  TI_CREATED_TERMINAL_ID = " & _provider_list & " OR TI_LAST_TERMINAL_ID = " & _provider_list
    Else
      _cashiers_and_terminals = _cashiers_and_terminals & "  TI_CREATED_TERMINAL_ID IN " & _provider_list & " OR TI_LAST_TERMINAL_ID IN " & _provider_list
    End If

    If _cashiers_and_terminals.Length > 0 Then
      _str_where = _str_where & "(" & _cashiers_and_terminals & ") "
    End If

    Return _str_where.Insert(0, " Where ")

  End Function


  Private Sub GUI_GetSqlQueryCashiers()

    Dim _sb As StringBuilder
    Dim _adapter As SqlDataAdapter
    Dim _table As DataTable

    _adapter = New SqlDataAdapter()
    _table = New DataTable()
    _sb = New StringBuilder()
    Try
      Using _db_trx As DB_TRX = New DB_TRX()
        _sb.AppendLine("  SELECT   CT_CASHIER_ID      ")
        _sb.AppendLine("          ,CT_NAME            ")
        _sb.AppendLine("    FROM   CASHIER_TERMINALS  ")
        _sb.AppendLine("ORDER BY   CT_NAME            ")
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _adapter = New SqlDataAdapter(_cmd)
          _adapter.Fill(_table)
          Call GUI_SetupRowsCashiersGrid(_table)
        End Using
      End Using
    Catch ex As Exception
    End Try
  End Sub

  Private Sub UncheckAllCahiers()

    Dim idx_rows As Integer
    For idx_rows = 0 To dg_cashiers.NumRows - 1
      dg_cashiers.Cell(idx_rows, GRID_CASHIER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next

  End Sub

#End Region

#Region "Public Functions"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  Private Sub ef_promotion_name_EntryFieldValueChanged() Handles ef_promotion_name.EntryFieldValueChanged
    If ef_promotion_name.TextValue.Length <> 0 Then
      chb_redeem_promotion.Enabled = False
      chb_non_redeemable_promotion.Enabled = False
      chb_points_promotion.Enabled = False
    Else
      chb_redeem_promotion.Enabled = True
      chb_non_redeemable_promotion.Enabled = True
      chb_points_promotion.Enabled = True
    End If
  End Sub

  ' PURPOSE: Define all Grid Account Alias columns
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheetCashiers()

    With Me.dg_cashiers
      Call .Init(GRID_CASHIER_COLUMNS, GRID_CASHIER_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden colums
      .Column(GRID_CASHIER_ID).Header(1).Text = ""
      .Column(GRID_CASHIER_ID).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_CASHIER_COLUMN_CHECKED).Header(1).Text = ""
      .Column(GRID_CASHIER_COLUMN_CHECKED).WidthFixed = GRID_CASHIER_WIDTH_COLUMN_CHECKED
      .Column(GRID_CASHIER_COLUMN_CHECKED).Fixed = True
      .Column(GRID_CASHIER_COLUMN_CHECKED).Editable = True
      .Column(GRID_CASHIER_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_CASHIER_COLUMN_DESCRIPTION).Header(1).Text = ""
      .Column(GRID_CASHIER_COLUMN_DESCRIPTION).WidthFixed = GRID_CASHIER_WIDTH_COLUMN_DESCRIPTION
      .Column(GRID_CASHIER_COLUMN_DESCRIPTION).Fixed = True
      .Column(GRID_CASHIER_COLUMN_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub

  ' PURPOSE: Fill the grid with the bank account alias
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_SetupRowsCashiersGrid(ByVal Table As DataTable)

    Dim _actual_row As Int32

    _actual_row = 0

    For Each r As DataRow In Table.Rows
      dg_cashiers.AddRow()
      Me.dg_cashiers.Cell(_actual_row, 0).Value = r(0)
      Me.dg_cashiers.Cell(_actual_row, 2).Value = r(1)

      _actual_row += 1
    Next

  End Sub ' GUI_SetupRowCashierGrid

  ' PURPOSE: get the bank accounts selected
  '
  '  PARAMS:
  '     - INPUT:
  '           - ColumnNeeded : It could be the Id column or alias column
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - it could be a list of Ids or a list of alias
  Private Function GetCashiersSelected(ByVal ColumnNeeded As Int16) As String

    Dim _idx_row As Integer
    Dim _bank_list As String
    Dim _all As Boolean
    Dim _at_least_one As Boolean

    _bank_list = ""
    _all = True
    _at_least_one = False

    For _idx_row = 0 To Me.dg_cashiers.NumRows - 1
      If dg_cashiers.Cell(_idx_row, GRID_CASHIER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        If _bank_list.Length = 0 Then
          _bank_list = dg_cashiers.Cell(_idx_row, ColumnNeeded).Value
        Else
          _bank_list = _bank_list & ", " & dg_cashiers.Cell(_idx_row, ColumnNeeded).Value
        End If
        _at_least_one = True
      Else
        _all = False
      End If

    Next

    If _all = True Then
      m_cashiers = GLB_NLS_GUI_ALARMS.GetString(277)
    End If
    If _all = False And _at_least_one = True Then
      m_cashiers = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2203)
    End If

    Return _bank_list

  End Function ' GetBankListSelected

#End Region

#Region "Events"
  Private Sub bt_pick_all_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_check_all.Click

    Dim idx_rows As Integer
    For idx_rows = 0 To dg_cashiers.NumRows - 1
      dg_cashiers.Cell(idx_rows, GRID_CASHIER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    Next

  End Sub


  Private Sub bt_uncheck_all_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_uncheck_all.Click
    Call UncheckAllCahiers()
    'Dim idx_rows As Integer
    'For idx_rows = 0 To dg_cashiers.NumRows - 1
    '  dg_cashiers.Cell(idx_rows, GRID_CASHIER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    'Next

  End Sub

#End Region
End Class