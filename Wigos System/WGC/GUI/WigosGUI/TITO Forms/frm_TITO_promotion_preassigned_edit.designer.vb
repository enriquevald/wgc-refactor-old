<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_TITO_promotion_preassigned_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.ef_promo_name = New GUI_Controls.uc_entry_field
    Me.gb_credit_type = New System.Windows.Forms.GroupBox
    Me.opt_point = New System.Windows.Forms.RadioButton
    Me.opt_credit_redeemable = New System.Windows.Forms.RadioButton
    Me.opt_credit_non_redeemable = New System.Windows.Forms.RadioButton
    Me.gb_enabled = New System.Windows.Forms.GroupBox
    Me.opt_disabled = New System.Windows.Forms.RadioButton
    Me.opt_enabled = New System.Windows.Forms.RadioButton
    Me.uc_promo_schedule = New GUI_Controls.uc_schedule
    Me.gb_account = New System.Windows.Forms.GroupBox
    Me.opt_account_summary = New System.Windows.Forms.RadioButton
    Me.opt_all_account = New System.Windows.Forms.RadioButton
    Me.rtb_report = New System.Windows.Forms.RichTextBox
    Me.btn_import = New GUI_Controls.uc_button
    Me.ef_expiration_days = New GUI_Controls.uc_entry_field
    Me.gb_ticket_footer_preassigned_promotion = New System.Windows.Forms.GroupBox
    Me.tb_ticket_footer_prasigned_promotion = New System.Windows.Forms.TextBox
    Me.cmb_categories = New GUI_Controls.uc_combo
    Me.gb_tito = New System.Windows.Forms.GroupBox
    Me.ef_generated_tickets_tito = New GUI_Controls.uc_entry_field
    Me.chk_allow_tickets_tito = New System.Windows.Forms.CheckBox
    Me.ef_quantity_tickets_tito = New GUI_Controls.uc_entry_field
    Me.panel_data.SuspendLayout()
    Me.gb_credit_type.SuspendLayout()
    Me.gb_enabled.SuspendLayout()
    Me.gb_account.SuspendLayout()
    Me.gb_ticket_footer_preassigned_promotion.SuspendLayout()
    Me.gb_tito.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_tito)
    Me.panel_data.Controls.Add(Me.cmb_categories)
    Me.panel_data.Controls.Add(Me.gb_ticket_footer_preassigned_promotion)
    Me.panel_data.Controls.Add(Me.ef_expiration_days)
    Me.panel_data.Controls.Add(Me.gb_account)
    Me.panel_data.Controls.Add(Me.uc_promo_schedule)
    Me.panel_data.Controls.Add(Me.gb_credit_type)
    Me.panel_data.Controls.Add(Me.gb_enabled)
    Me.panel_data.Controls.Add(Me.ef_promo_name)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(872, 565)
    '
    'ef_promo_name
    '
    Me.ef_promo_name.DoubleValue = 0
    Me.ef_promo_name.IntegerValue = 0
    Me.ef_promo_name.IsReadOnly = False
    Me.ef_promo_name.Location = New System.Drawing.Point(11, 5)
    Me.ef_promo_name.Name = "ef_promo_name"
    Me.ef_promo_name.Size = New System.Drawing.Size(288, 24)
    Me.ef_promo_name.SufixText = "Sufix Text"
    Me.ef_promo_name.SufixTextVisible = True
    Me.ef_promo_name.TabIndex = 0
    Me.ef_promo_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_promo_name.TextValue = ""
    Me.ef_promo_name.TextWidth = 87
    Me.ef_promo_name.Value = ""
    '
    'gb_credit_type
    '
    Me.gb_credit_type.Controls.Add(Me.opt_point)
    Me.gb_credit_type.Controls.Add(Me.opt_credit_redeemable)
    Me.gb_credit_type.Controls.Add(Me.opt_credit_non_redeemable)
    Me.gb_credit_type.Location = New System.Drawing.Point(127, 65)
    Me.gb_credit_type.Name = "gb_credit_type"
    Me.gb_credit_type.Size = New System.Drawing.Size(178, 94)
    Me.gb_credit_type.TabIndex = 3
    Me.gb_credit_type.TabStop = False
    Me.gb_credit_type.Text = "xCreditType"
    '
    'opt_point
    '
    Me.opt_point.Location = New System.Drawing.Point(18, 68)
    Me.opt_point.Name = "opt_point"
    Me.opt_point.Size = New System.Drawing.Size(135, 16)
    Me.opt_point.TabIndex = 2
    Me.opt_point.Text = "xPoint"
    '
    'opt_credit_redeemable
    '
    Me.opt_credit_redeemable.Location = New System.Drawing.Point(18, 45)
    Me.opt_credit_redeemable.Name = "opt_credit_redeemable"
    Me.opt_credit_redeemable.Size = New System.Drawing.Size(135, 16)
    Me.opt_credit_redeemable.TabIndex = 1
    Me.opt_credit_redeemable.Text = "xRedeemable"
    '
    'opt_credit_non_redeemable
    '
    Me.opt_credit_non_redeemable.Location = New System.Drawing.Point(18, 22)
    Me.opt_credit_non_redeemable.Name = "opt_credit_non_redeemable"
    Me.opt_credit_non_redeemable.Size = New System.Drawing.Size(135, 16)
    Me.opt_credit_non_redeemable.TabIndex = 0
    Me.opt_credit_non_redeemable.Text = "xNon-Redeemable"
    '
    'gb_enabled
    '
    Me.gb_enabled.Controls.Add(Me.opt_disabled)
    Me.gb_enabled.Controls.Add(Me.opt_enabled)
    Me.gb_enabled.Location = New System.Drawing.Point(15, 65)
    Me.gb_enabled.Name = "gb_enabled"
    Me.gb_enabled.Size = New System.Drawing.Size(94, 94)
    Me.gb_enabled.TabIndex = 2
    Me.gb_enabled.TabStop = False
    Me.gb_enabled.Text = "xEnabled"
    '
    'opt_disabled
    '
    Me.opt_disabled.Location = New System.Drawing.Point(19, 57)
    Me.opt_disabled.Name = "opt_disabled"
    Me.opt_disabled.Size = New System.Drawing.Size(70, 16)
    Me.opt_disabled.TabIndex = 1
    Me.opt_disabled.Text = "xNo"
    '
    'opt_enabled
    '
    Me.opt_enabled.Location = New System.Drawing.Point(19, 31)
    Me.opt_enabled.Name = "opt_enabled"
    Me.opt_enabled.Size = New System.Drawing.Size(70, 16)
    Me.opt_enabled.TabIndex = 0
    Me.opt_enabled.Text = "xYes"
    '
    'uc_promo_schedule
    '
    Me.uc_promo_schedule.DateFrom = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_promo_schedule.DateTo = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_promo_schedule.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_promo_schedule.Location = New System.Drawing.Point(8, 165)
    Me.uc_promo_schedule.Name = "uc_promo_schedule"
    Me.uc_promo_schedule.SecondTime = False
    Me.uc_promo_schedule.SecondTimeFrom = 0
    Me.uc_promo_schedule.SecondTimeTo = 0
    Me.uc_promo_schedule.Size = New System.Drawing.Size(410, 239)
    Me.uc_promo_schedule.TabIndex = 4
    Me.uc_promo_schedule.TimeFrom = 0
    Me.uc_promo_schedule.TimeTo = 0
    Me.uc_promo_schedule.Weekday = 0
    '
    'gb_account
    '
    Me.gb_account.Controls.Add(Me.opt_account_summary)
    Me.gb_account.Controls.Add(Me.opt_all_account)
    Me.gb_account.Controls.Add(Me.rtb_report)
    Me.gb_account.Controls.Add(Me.btn_import)
    Me.gb_account.Location = New System.Drawing.Point(422, 15)
    Me.gb_account.Name = "gb_account"
    Me.gb_account.Size = New System.Drawing.Size(448, 452)
    Me.gb_account.TabIndex = 7
    Me.gb_account.TabStop = False
    Me.gb_account.Text = "xAccounts"
    '
    'opt_account_summary
    '
    Me.opt_account_summary.Appearance = System.Windows.Forms.Appearance.Button
    Me.opt_account_summary.Checked = True
    Me.opt_account_summary.Location = New System.Drawing.Point(12, 58)
    Me.opt_account_summary.Name = "opt_account_summary"
    Me.opt_account_summary.Size = New System.Drawing.Size(133, 30)
    Me.opt_account_summary.TabIndex = 1
    Me.opt_account_summary.TabStop = True
    Me.opt_account_summary.Text = "xAccount Summary"
    Me.opt_account_summary.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.opt_account_summary.UseVisualStyleBackColor = True
    '
    'opt_all_account
    '
    Me.opt_all_account.Appearance = System.Windows.Forms.Appearance.Button
    Me.opt_all_account.Location = New System.Drawing.Point(151, 57)
    Me.opt_all_account.Name = "opt_all_account"
    Me.opt_all_account.Size = New System.Drawing.Size(133, 31)
    Me.opt_all_account.TabIndex = 2
    Me.opt_all_account.Text = "xAll Accounts"
    Me.opt_all_account.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.opt_all_account.UseVisualStyleBackColor = True
    '
    'rtb_report
    '
    Me.rtb_report.Location = New System.Drawing.Point(11, 96)
    Me.rtb_report.Name = "rtb_report"
    Me.rtb_report.ReadOnly = True
    Me.rtb_report.Size = New System.Drawing.Size(428, 345)
    Me.rtb_report.TabIndex = 3
    Me.rtb_report.Text = ""
    '
    'btn_import
    '
    Me.btn_import.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_import.Location = New System.Drawing.Point(11, 20)
    Me.btn_import.Name = "btn_import"
    Me.btn_import.Size = New System.Drawing.Size(90, 30)
    Me.btn_import.TabIndex = 0
    Me.btn_import.ToolTipped = False
    Me.btn_import.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'ef_expiration_days
    '
    Me.ef_expiration_days.DoubleValue = 0
    Me.ef_expiration_days.IntegerValue = 0
    Me.ef_expiration_days.IsReadOnly = False
    Me.ef_expiration_days.Location = New System.Drawing.Point(20, 418)
    Me.ef_expiration_days.Name = "ef_expiration_days"
    Me.ef_expiration_days.Size = New System.Drawing.Size(233, 24)
    Me.ef_expiration_days.SufixText = "xday(s)"
    Me.ef_expiration_days.SufixTextVisible = True
    Me.ef_expiration_days.SufixTextWidth = 80
    Me.ef_expiration_days.TabIndex = 5
    Me.ef_expiration_days.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_expiration_days.TextValue = ""
    Me.ef_expiration_days.TextWidth = 110
    Me.ef_expiration_days.Value = ""
    '
    'gb_ticket_footer_preassigned_promotion
    '
    Me.gb_ticket_footer_preassigned_promotion.Controls.Add(Me.tb_ticket_footer_prasigned_promotion)
    Me.gb_ticket_footer_preassigned_promotion.Location = New System.Drawing.Point(20, 448)
    Me.gb_ticket_footer_preassigned_promotion.Name = "gb_ticket_footer_preassigned_promotion"
    Me.gb_ticket_footer_preassigned_promotion.Size = New System.Drawing.Size(367, 110)
    Me.gb_ticket_footer_preassigned_promotion.TabIndex = 6
    Me.gb_ticket_footer_preassigned_promotion.TabStop = False
    Me.gb_ticket_footer_preassigned_promotion.Text = "xTicket Footer"
    '
    'tb_ticket_footer_prasigned_promotion
    '
    Me.tb_ticket_footer_prasigned_promotion.Location = New System.Drawing.Point(14, 19)
    Me.tb_ticket_footer_prasigned_promotion.MaxLength = 250
    Me.tb_ticket_footer_prasigned_promotion.Multiline = True
    Me.tb_ticket_footer_prasigned_promotion.Name = "tb_ticket_footer_prasigned_promotion"
    Me.tb_ticket_footer_prasigned_promotion.Size = New System.Drawing.Size(342, 85)
    Me.tb_ticket_footer_prasigned_promotion.TabIndex = 1
    '
    'cmb_categories
    '
    Me.cmb_categories.IsReadOnly = False
    Me.cmb_categories.Location = New System.Drawing.Point(11, 35)
    Me.cmb_categories.Name = "cmb_categories"
    Me.cmb_categories.SelectedIndex = -1
    Me.cmb_categories.Size = New System.Drawing.Size(268, 24)
    Me.cmb_categories.SufixText = "Sufix Text"
    Me.cmb_categories.SufixTextVisible = True
    Me.cmb_categories.TabIndex = 1
    Me.cmb_categories.TextWidth = 87
    '
    'gb_tito
    '
    Me.gb_tito.Controls.Add(Me.ef_generated_tickets_tito)
    Me.gb_tito.Controls.Add(Me.chk_allow_tickets_tito)
    Me.gb_tito.Controls.Add(Me.ef_quantity_tickets_tito)
    Me.gb_tito.Location = New System.Drawing.Point(422, 478)
    Me.gb_tito.Name = "gb_tito"
    Me.gb_tito.Size = New System.Drawing.Size(448, 74)
    Me.gb_tito.TabIndex = 21
    Me.gb_tito.TabStop = False
    Me.gb_tito.Text = "xTITO"
    '
    'ef_generated_tickets_tito
    '
    Me.ef_generated_tickets_tito.DoubleValue = 0
    Me.ef_generated_tickets_tito.IntegerValue = 0
    Me.ef_generated_tickets_tito.IsReadOnly = False
    Me.ef_generated_tickets_tito.Location = New System.Drawing.Point(270, 44)
    Me.ef_generated_tickets_tito.Name = "ef_generated_tickets_tito"
    Me.ef_generated_tickets_tito.OnlyUpperCase = True
    Me.ef_generated_tickets_tito.Size = New System.Drawing.Size(169, 24)
    Me.ef_generated_tickets_tito.SufixText = "Sufix Text"
    Me.ef_generated_tickets_tito.SufixTextVisible = True
    Me.ef_generated_tickets_tito.TabIndex = 6
    Me.ef_generated_tickets_tito.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_generated_tickets_tito.TextValue = ""
    Me.ef_generated_tickets_tito.TextWidth = 110
    Me.ef_generated_tickets_tito.Value = ""
    '
    'chk_allow_tickets_tito
    '
    Me.chk_allow_tickets_tito.AutoSize = True
    Me.chk_allow_tickets_tito.Location = New System.Drawing.Point(16, 22)
    Me.chk_allow_tickets_tito.Name = "chk_allow_tickets_tito"
    Me.chk_allow_tickets_tito.Size = New System.Drawing.Size(131, 17)
    Me.chk_allow_tickets_tito.TabIndex = 4
    Me.chk_allow_tickets_tito.Text = "xAllowTicketsTITO"
    Me.chk_allow_tickets_tito.UseVisualStyleBackColor = True
    '
    'ef_quantity_tickets_tito
    '
    Me.ef_quantity_tickets_tito.DoubleValue = 0
    Me.ef_quantity_tickets_tito.IntegerValue = 0
    Me.ef_quantity_tickets_tito.IsReadOnly = False
    Me.ef_quantity_tickets_tito.Location = New System.Drawing.Point(16, 44)
    Me.ef_quantity_tickets_tito.Name = "ef_quantity_tickets_tito"
    Me.ef_quantity_tickets_tito.OnlyUpperCase = True
    Me.ef_quantity_tickets_tito.Size = New System.Drawing.Size(174, 24)
    Me.ef_quantity_tickets_tito.SufixText = "Sufix Text"
    Me.ef_quantity_tickets_tito.SufixTextVisible = True
    Me.ef_quantity_tickets_tito.TabIndex = 5
    Me.ef_quantity_tickets_tito.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_quantity_tickets_tito.TextValue = ""
    Me.ef_quantity_tickets_tito.TextWidth = 110
    Me.ef_quantity_tickets_tito.Value = ""
    '
    'frm_TITO_promotion_preassigned_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(991, 576)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_TITO_promotion_preassigned_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_promotion_preassigned_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gb_credit_type.ResumeLayout(False)
    Me.gb_enabled.ResumeLayout(False)
    Me.gb_account.ResumeLayout(False)
    Me.gb_ticket_footer_preassigned_promotion.ResumeLayout(False)
    Me.gb_ticket_footer_preassigned_promotion.PerformLayout()
    Me.gb_tito.ResumeLayout(False)
    Me.gb_tito.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_promo_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_credit_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_point As System.Windows.Forms.RadioButton
  Friend WithEvents opt_credit_redeemable As System.Windows.Forms.RadioButton
  Friend WithEvents opt_credit_non_redeemable As System.Windows.Forms.RadioButton
  Friend WithEvents gb_enabled As System.Windows.Forms.GroupBox
  Friend WithEvents opt_disabled As System.Windows.Forms.RadioButton
  Friend WithEvents opt_enabled As System.Windows.Forms.RadioButton
  Friend WithEvents uc_promo_schedule As GUI_Controls.uc_schedule
  Friend WithEvents gb_account As System.Windows.Forms.GroupBox
  Friend WithEvents btn_import As GUI_Controls.uc_button
  Friend WithEvents rtb_report As System.Windows.Forms.RichTextBox
  Friend WithEvents opt_account_summary As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_account As System.Windows.Forms.RadioButton
  Friend WithEvents ef_expiration_days As GUI_Controls.uc_entry_field
  Friend WithEvents gb_ticket_footer_preassigned_promotion As System.Windows.Forms.GroupBox
  Friend WithEvents tb_ticket_footer_prasigned_promotion As System.Windows.Forms.TextBox
  Friend WithEvents cmb_categories As GUI_Controls.uc_combo
  Friend WithEvents gb_tito As System.Windows.Forms.GroupBox
  Friend WithEvents ef_generated_tickets_tito As GUI_Controls.uc_entry_field
  Friend WithEvents chk_allow_tickets_tito As System.Windows.Forms.CheckBox
  Friend WithEvents ef_quantity_tickets_tito As GUI_Controls.uc_entry_field
End Class
