'--------------------------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'--------------------------------------------------------------------------------------
'
'   MODULE NAME :  frm_stackers_sel
'
'   DESCRIPTION :  Stackers browser
'
'        AUTHOR :  Nelson Madrigal Reyes
'
' CREATION DATE :  21-JUN-2013
'
' REVISION HISTORY
'
' Date         Author Description
' -----------  ------ ------------------------------------------------------------------
' 21-JUN-2013  NMR    Initial version
' 23-AUG-2013  JRM    Code revision and refactoring
'---------------------------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports System.Text
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls

Public Class frm_stackers_sel
  Inherits GUI_Controls.frm_base_sel

#Region " Members "

  ' al introducir datos en la tabla, se insertar� un prefijo num�rico para conservar el orden
  ' ese prefijo se deber� quitar al momento de enviarlo a las rutinas de impresi�n
  Dim m_filter_params As New Hashtable()

#End Region ' Members

#Region " Constants "

  ' Grid configuration
  Private Const GRID_COLUMNS_COUNT As Integer = 8
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Grid Columns
  Private Const GRID_COLUMN_SELECT As Integer = 0
  Private Const GRID_COLUMN_ID As Integer = 1
  Private Const GRID_COLUMN_MODEL As Integer = 2
  Private Const GRID_COLUMN_STATUS As Integer = 3
  Private Const GRID_COLUMN_PREASSIGNED As Integer = 4
  Private Const GRID_COLUMN_PREASSIGNED_NAME As Integer = 5
  Private Const GRID_COLUMN_INSERTED As Integer = 6
  Private Const GRID_COLUMN_INSERTED_NAME As Integer = 7

  ' QUERY columns
  Private Const SQL_COLUMN_ID As Integer = 0
  Private Const SQL_COLUMN_MODEL As Integer = 1
  Private Const SQL_COLUMN_STATUS As Integer = 2
  Private Const SQL_COLUMN_PREASSIGNED_NAME As Integer = 4
  Private Const SQL_COLUMN_INSERTED_NAME As Integer = 6

  ' Columns Width
  Private Const GRID_COLUMN_WIDTH_SELECT As Integer = 200
  Private Const GRID_COLUMN_WIDTH_ID As Integer = 1000
  Private Const GRID_COLUMN_WIDTH_MODEL As Integer = 2200
  Private Const GRID_COLUMN_WIDTH_STATUS As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_PREASSIGNED As Integer = 2900
  Private Const GRID_COLUMN_WIDTH_INSERTED As Integer = 2900

#End Region ' Constants

#Region " Private Functions and Methods "

  ' PURPOSE : Define layout of all Main Grid Columns
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS_COUNT, GRID_HEADER_ROWS)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = False

      .Column(GRID_COLUMN_SELECT).Header(0).Text = ""
      .Column(GRID_COLUMN_SELECT).Header(1).Text = ""
      .Column(GRID_COLUMN_SELECT).Width = GRID_COLUMN_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False

      .Column(GRID_COLUMN_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2162)
      .Column(GRID_COLUMN_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1407)
      .Column(GRID_COLUMN_ID).Width = GRID_COLUMN_WIDTH_ID

      .Column(GRID_COLUMN_MODEL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2162)
      .Column(GRID_COLUMN_MODEL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2166)
      .Column(GRID_COLUMN_MODEL).Width = GRID_COLUMN_WIDTH_MODEL
      .Column(GRID_COLUMN_MODEL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2162)
      .Column(GRID_COLUMN_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)
      .Column(GRID_COLUMN_STATUS).Width = GRID_COLUMN_WIDTH_STATUS
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_INSERTED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2164)
      .Column(GRID_COLUMN_INSERTED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1407)
      .Column(GRID_COLUMN_INSERTED).Width = 0

      .Column(GRID_COLUMN_INSERTED_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2164)
      .Column(GRID_COLUMN_INSERTED_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253)
      .Column(GRID_COLUMN_INSERTED_NAME).Width = GRID_COLUMN_WIDTH_INSERTED

      .Column(GRID_COLUMN_PREASSIGNED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2165)
      .Column(GRID_COLUMN_PREASSIGNED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1407)
      .Column(GRID_COLUMN_PREASSIGNED).Width = 0

      .Column(GRID_COLUMN_PREASSIGNED_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2165)
      .Column(GRID_COLUMN_PREASSIGNED_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253)
      .Column(GRID_COLUMN_PREASSIGNED_NAME).Width = GRID_COLUMN_WIDTH_PREASSIGNED

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE : Concatenation of condition for stacker searching
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - StringBuilder with Select query
  '
  Private Function GetMainStackerQuery() As StringBuilder

    Dim _str_sql_builder As StringBuilder
    _str_sql_builder = New StringBuilder()

    _str_sql_builder.AppendLine("   SELECT   ST.ST_STACKER_ID")
    _str_sql_builder.AppendLine("          , ST.ST_MODEL")
    _str_sql_builder.AppendLine("          , ST.ST_STATUS")
    _str_sql_builder.AppendLine("          , ISNULL(ST.ST_PREASSIGNED, 0)")
    _str_sql_builder.AppendLine("          , ISNULL(PV.PV_NAME, '')")
    _str_sql_builder.AppendLine("          , ISNULL(ST.ST_INSERTED, 0)")
    _str_sql_builder.AppendLine("          , ISNULL(TE2.TE_NAME, '')")
    _str_sql_builder.AppendLine("     FROM   STACKERS AS ST ")
    _str_sql_builder.AppendLine("LEFT JOIN   TERMINALS AS TE2 ON ST.ST_INSERTED = TE2.TE_TERMINAL_ID ")
    _str_sql_builder.AppendLine("LEFT JOIN   PROVIDERS AS PV  ON ST.ST_PREASSIGNED = PV.PV_ID ")

    Return _str_sql_builder

  End Function ' GetMainStackerQuery

  ' PURPOSE : Concatenation of condition for stacker searching
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - Nullable String
  '
  Private Function GetFilterForStacker() As String

    Dim _str_filter As String
    Dim _filter_parts As List(Of String)

    ' edit controls
    _filter_parts = New List(Of String)
    _filter_parts.Add("(")

    If ef_stacker_indentifier.Value <> "" Then
      _filter_parts.Add("ST.ST_STACKER_ID LIKE '%" & Me.ef_stacker_indentifier.Value & "%'")
    End If

    If Me.ef_stacker_model.Value <> "" Then
      If _filter_parts.Count > 1 Then
        _filter_parts.Add(" AND ")
      End If
      _filter_parts.Add("ST.ST_MODEL LIKE '%" & Me.ef_stacker_model.Value.Replace("'", "''") & "%'")
    End If

    _filter_parts.Add(")")

    _str_filter = ""

    If _filter_parts.Count > 2 Then
      _str_filter &= String.Join(" ", _filter_parts.ToArray())
    End If

    ' Checks for stacker status
    _filter_parts.Clear()
    _filter_parts.Add(" ST.ST_STATUS IN (")

    If chk_inactive.Checked Then
      _filter_parts.Add(TITO_STACKER_STATUS.DEACTIVATED)
    End If

    If chk_available.Checked Then
      If _filter_parts.Count > 1 Then
        _filter_parts.Add(",")
      End If
      _filter_parts.Add(TITO_STACKER_STATUS.AVAILABLE)
    End If

    If chk_inserted_pending.Checked Then
      If _filter_parts.Count > 1 Then
        _filter_parts.Add(",")
      End If
      _filter_parts.Add(TITO_STACKER_STATUS.INSERTED_PENDING)
    End If

    If chk_inserted_validated.Checked Then
      If _filter_parts.Count > 1 Then
        _filter_parts.Add(",")
      End If
      _filter_parts.Add(TITO_STACKER_STATUS.INSERTED_VALIDATED)
    End If

    If chk_collection_pending.Checked Then
      If _filter_parts.Count > 1 Then
        _filter_parts.Add(",")
      End If
      _filter_parts.Add(TITO_STACKER_STATUS.COLLECTION_PENDING)
    End If

    _filter_parts.Add(")")

    ' se compone la cadena final
    If _filter_parts.Count > 2 Then
      If Not String.IsNullOrEmpty(_str_filter) Then
        _str_filter &= " AND "
      End If
      _str_filter &= String.Join(" ", _filter_parts.ToArray())
    End If

    Return _str_filter

  End Function ' GetFilterForStacker

  ' PURPOSE : Concatenation of condition for stacker searching
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - Nullable String
  '
  Private Function GetFilterForTerminal() As String

    Dim _str_filter As String
    Dim _str_filter_tmp As String
    Dim _str_filter_by_terminal As String
    Dim _str_filter_by_provider As String

    _str_filter_by_terminal = ""
    _str_filter_by_provider = ""
    _str_filter_tmp = ""

    _str_filter = ""

    If Not String.IsNullOrEmpty(ef_terminal_name.TextValue) Then
      _str_filter_by_terminal = "(" _
                            + " SELECT   TE_TERMINAL_ID " _
                            + "   FROM   TERMINALS" _
                            + "  WHERE   TE_NAME LIKE '%" & ef_terminal_name.Value.Replace("'", "''") & "%'" _
                            + ")"
    End If

    If Not String.IsNullOrEmpty(ef_provider_name.TextValue) Then
      _str_filter_by_provider = "(" _
                               + " SELECT   PV_ID " _
                               + "   FROM   PROVIDERS " _
                               + "  WHERE   PV_NAME LIKE '%" & ef_provider_name.Value.Replace("'", "''") & "%'" _
                               + ")"

    End If

    If Not String.IsNullOrEmpty(_str_filter_by_terminal) Then
      _str_filter_tmp &= "((ST.ST_INSERTED IN " & _str_filter_by_terminal & "))"
    End If

    If Not String.IsNullOrEmpty(_str_filter_by_provider) Then
      If Not String.IsNullOrEmpty(_str_filter_tmp) Then
        _str_filter_tmp &= "OR (ST.ST_PREASSIGNED IN " & _str_filter_by_provider & ")"
      Else
        _str_filter_tmp &= "(ST.ST_PREASSIGNED IN " & _str_filter_by_provider & ")"
      End If
    End If

    Return _str_filter_tmp

  End Function ' GetFilterForTerminal

  ' PURPOSE: Add new filter item form export/print purpouses
  '
  '  PARAMS:
  '     - INPUT :
  '       - Label: string with label to print
  '       - Value: text value to print
  '
  '     - OUTPUT :
  '
  Private Sub AddFilterItem(ByVal Label As String, ByVal Value As String)

    m_filter_params.Add(m_filter_params.Count, Label & ";" & Value)

  End Sub ' AddFilterItem

  ' PURPOSE: Check/Uncheck controls
  '
  '  PARAMS:
  '     - INPUT :
  '       - Value: boolean to set in property Checked
  '
  '     - OUTPUT :
  '
  Private Sub CheckOrUncheckControls(ByVal Value As Boolean)

    Dim _ctrl As Control
    Dim _check As CheckBox

    For Each _ctrl In gb_status.Controls
      If TypeOf _ctrl Is CheckBox Then
        _check = _ctrl
        _check.Checked = Value
      End If
    Next

  End Sub ' CheckOrUncheckControls

#End Region

#Region " OVERRIDES "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_STACKERS_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2162)
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Enabled = Me.Permissions.Write

    ' Stacker Group
    Me.xGroupStacker.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2162)
    Me.ef_stacker_indentifier.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1407)
    Me.ef_stacker_model.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2166)
    Me.chk_inactive.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2167)
    Me.chk_available.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(347)
    Me.chk_inserted_pending.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2169)
    Me.chk_inserted_validated.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2170)
    Me.chk_collection_pending.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2171)
    Me.gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)

    ' Terminal group
    Me.gb_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2225)
    Me.ef_terminal_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2106)
    Me.ef_provider_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(469)
    'Me.ef_combo_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2168)

    ' edit controls activation
    Me.ef_stacker_indentifier.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 10)
    Me.ef_stacker_model.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)
    Me.ef_terminal_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 10)
    Me.ef_provider_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 10)

    ' Define layout of all Main Grid Columns
    Call GUI_StyleSheet()

    ' Set filter default values
    Call GUI_FilterReset()

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Int32
    Dim _id_stacker As Int64
    Dim _frm_edit As frm_stackers_edit

    If Grid.SelectedRows.Length < 1 Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(2040), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Return
    End If

    _idx_row = Me.Grid.SelectedRows(0)
    _id_stacker = Me.Grid.Cell(_idx_row, GRID_COLUMN_ID).Value
    _frm_edit = New frm_stackers_edit
    Call _frm_edit.ShowEditItem(_id_stacker)

    _frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub ' GUI_EditSelectedItem

  Protected Overrides Sub GUI_EditNewItem()

    Dim _frm_edit As frm_stackers_edit

    _frm_edit = New frm_stackers_edit
    Call _frm_edit.ShowNewItem()

    _frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub ' GUI_EditNewItem

  Protected Overrides Sub GUI_FilterReset()

    Dim _check As CheckBox

    ' Stacker
    Me.ef_stacker_indentifier.Value = ""
    Me.ef_stacker_model.Value = ""

    For Each _ctrl As Control In gb_status.Controls
      If TypeOf _ctrl Is CheckBox Then
        _check = _ctrl
        _check.Checked = False
      End If
    Next

    ' Terminal
    Me.ef_terminal_name.Value = ""
    Me.ef_provider_name.Value = ""

    For Each _ctrl As Control In gb_terminal.Controls
      If TypeOf _ctrl Is CheckBox Then
        _check = _ctrl
        _check.Checked = False
      End If
    Next

  End Sub ' GUI_FilterReset

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_where As String
    Dim _str_where_tmp As String
    Dim _str_sql_builder As StringBuilder

    _str_sql_builder = GetMainStackerQuery()

    ' where clausules; don't show disabled stackers
    _str_sql_builder.AppendLine("WHERE (ST.ST_STATUS <> " & TITO_STACKER_STATUS.DISABLED & ")")

    ' Stacker group
    _str_where = ""
    _str_where_tmp = GetFilterForStacker()

    If Not String.IsNullOrEmpty(_str_where_tmp) Then
      _str_where &= " AND " & _str_where_tmp
    End If

    ' Terminal/Provider group
    _str_where_tmp = GetFilterForTerminal()

    If Not String.IsNullOrEmpty(_str_where_tmp) Then
      _str_where &= " AND " & _str_where_tmp
    End If

    _str_sql_builder.AppendLine(_str_where)
    _str_sql_builder.AppendLine("ORDER BY ST.ST_STACKER_ID")

    Return _str_sql_builder.ToString()

  End Function ' GUI_FilterGetSqlQuery

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Me.Grid.Cell(RowIndex, GRID_COLUMN_ID).Value = DbRow.Value(SQL_COLUMN_ID)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_MODEL).Value = DbRow.Value(SQL_COLUMN_MODEL)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = mdl_tito.StackerStatusAsString(DbRow.Value(SQL_COLUMN_STATUS))
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PREASSIGNED_NAME).Value = DbRow.Value(SQL_COLUMN_PREASSIGNED_NAME)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_INSERTED_NAME).Value = DbRow.Value(SQL_COLUMN_INSERTED_NAME)

    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim _status As String
    Dim _idx_order As Integer
    Dim _all_checked As Boolean
    Dim _check As CheckBox

    _idx_order = 0
    m_filter_params.Clear()
    Call AddFilterItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2104), ef_stacker_indentifier.Value)
    Call AddFilterItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2166), ef_stacker_model.Value)
    Call AddFilterItem("", "")
    Call AddFilterItem("", "")
    Call AddFilterItem("", "")

    _status = ""
    _all_checked = True

    For Each _ctrl As Control In gb_status.Controls
      If TypeOf _ctrl Is CheckBox Then
        _check = _ctrl
        _status &= IIf(_check.Checked, _check.Text & ",", "")
        _all_checked = _all_checked And _check.Checked
      End If
    Next

    If _all_checked Then
      _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
    ElseIf _status.Length > 0 Then
      _status = _status.Substring(0, _status.Length - 1)
    End If

    Call AddFilterItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(262), _status)
    Call AddFilterItem("", "")
    Call AddFilterItem("", "")
    Call AddFilterItem("", "")
    Call AddFilterItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2106), ef_terminal_name.Value)
    Call AddFilterItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(469), ef_provider_name.Value)

    _status = ""
    _all_checked = True

    For Each _ctrl As Control In gb_terminal.Controls
      If TypeOf _ctrl Is CheckBox Then
        _check = _ctrl
        _status &= IIf(_check.Checked, _check.Text & ",", "")
        _all_checked = _all_checked And _check.Checked
      End If
    Next

    If _all_checked Then
      _status = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1353)
    ElseIf _status.Length > 0 Then
      _status = _status.Substring(0, _status.Length - 1)
    End If

    Call AddFilterItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(492), _status)

  End Sub ' GUI_ReportUpdateFilters

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    Dim _text As String
    Dim _keys() As Integer
    Dim _values() As String

    ReDim _keys(m_filter_params.Count - 1)

    m_filter_params.Keys.CopyTo(_keys, 0)
    Array.Sort(_keys)

    For Each _key As Integer In _keys
      _text = m_filter_params(_key)
      _values = _text.Split(";")

      If String.IsNullOrEmpty(_values(0)) Then
        PrintData.SetFilter("", "", True)
      Else
        PrintData.SetFilter(_values(0), _values(1))
      End If
    Next

    PrintData.FilterHeaderWidth(1) = 2000
    PrintData.FilterHeaderWidth(2) = 2500
    PrintData.FilterHeaderWidth(3) = 2500
    PrintData.FilterValueWidth(2) = 2100
    PrintData.FilterValueWidth(4) = 2100

  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ExcelData.SetColumnFormat(GRID_COLUMN_MODEL, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)

  End Sub ' GUI_ReportParams

#End Region ' OVERRIDES

#Region " Public Functions "

  ' TODO: tbd
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

End Class