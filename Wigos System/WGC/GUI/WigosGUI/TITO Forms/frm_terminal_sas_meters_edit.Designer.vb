<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_terminal_sas_meters_edit
  Inherits GUI_Controls.frm_base_print

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_provider = New GUI_Controls.uc_provider()
    Me.uc_checked_list_groups = New GUI_Controls.uc_checked_list()
    Me.dg_meters = New GUI_Controls.uc_grid()
    Me.chk_terminal_location = New System.Windows.Forms.CheckBox()
    Me.gb_filter_values = New System.Windows.Forms.GroupBox()
    Me.rb_with_values = New System.Windows.Forms.RadioButton()
    Me.rb_without_values = New System.Windows.Forms.RadioButton()
    Me.rb_all = New System.Windows.Forms.RadioButton()
    Me.dg_alarms = New GUI_Controls.uc_grid()
    Me.gb_info = New System.Windows.Forms.GroupBox()
    Me.lbl_max_increment = New System.Windows.Forms.Label()
    Me.lbl_max_value = New System.Windows.Forms.Label()
    Me.lbl_meter = New System.Windows.Forms.Label()
    Me.lbl_information = New System.Windows.Forms.Label()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.panel_grids.SuspendLayout()
    Me.panel_filter.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_filter_values.SuspendLayout()
    Me.gb_info.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_grids
    '
    Me.panel_grids.Controls.Add(Me.dg_alarms)
    Me.panel_grids.Controls.Add(Me.dg_meters)
    Me.panel_grids.Location = New System.Drawing.Point(5, 243)
    Me.panel_grids.Size = New System.Drawing.Size(1273, 429)
    Me.panel_grids.Controls.SetChildIndex(Me.dg_meters, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.panel_buttons, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.dg_alarms, 0)
    '
    'panel_buttons
    '
    Me.panel_buttons.Location = New System.Drawing.Point(1185, 0)
    Me.panel_buttons.Size = New System.Drawing.Size(88, 429)
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.lbl_information)
    Me.panel_filter.Controls.Add(Me.gb_info)
    Me.panel_filter.Controls.Add(Me.gb_filter_values)
    Me.panel_filter.Controls.Add(Me.chk_terminal_location)
    Me.panel_filter.Controls.Add(Me.uc_checked_list_groups)
    Me.panel_filter.Controls.Add(Me.uc_provider)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1273, 216)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_provider, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_checked_list_groups, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_terminal_location, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_filter_values, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_info, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_information, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(5, 220)
    Me.pn_separator_line.Size = New System.Drawing.Size(1273, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1273, 4)
    '
    'uc_provider
    '
    Me.uc_provider.Location = New System.Drawing.Point(203, 14)
    Me.uc_provider.Name = "uc_provider"
    Me.uc_provider.Size = New System.Drawing.Size(383, 173)
    Me.uc_provider.TabIndex = 2
    Me.uc_provider.TerminalListHasValues = False
    '
    'uc_checked_list_groups
    '
    Me.uc_checked_list_groups.GroupBoxText = "xCheckedList"
    Me.uc_checked_list_groups.Location = New System.Drawing.Point(535, 18)
    Me.uc_checked_list_groups.m_resize_width = 352
    Me.uc_checked_list_groups.multiChoice = True
    Me.uc_checked_list_groups.Name = "uc_checked_list_groups"
    Me.uc_checked_list_groups.SelectedIndexes = New Integer(-1) {}
    Me.uc_checked_list_groups.SelectedIndexesList = ""
    Me.uc_checked_list_groups.SelectedIndexesListLevel2 = ""
    Me.uc_checked_list_groups.SelectedValuesArray = New String(-1) {}
    Me.uc_checked_list_groups.SelectedValuesList = ""
    Me.uc_checked_list_groups.SetLevels = 2
    Me.uc_checked_list_groups.Size = New System.Drawing.Size(352, 173)
    Me.uc_checked_list_groups.TabIndex = 4
    Me.uc_checked_list_groups.ValuesArray = New String(-1) {}
    '
    'dg_meters
    '
    Me.dg_meters.CurrentCol = -1
    Me.dg_meters.CurrentRow = -1
    Me.dg_meters.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_meters.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_meters.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_meters.Location = New System.Drawing.Point(6, 0)
    Me.dg_meters.Name = "dg_meters"
    Me.dg_meters.PanelRightVisible = True
    Me.dg_meters.Redraw = True
    Me.dg_meters.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_meters.Size = New System.Drawing.Size(1174, 315)
    Me.dg_meters.Sortable = False
    Me.dg_meters.SortAscending = True
    Me.dg_meters.SortByCol = 0
    Me.dg_meters.TabIndex = 10
    Me.dg_meters.ToolTipped = True
    Me.dg_meters.TopRow = -2
    Me.dg_meters.WordWrap = False
    '
    'chk_terminal_location
    '
    Me.chk_terminal_location.Location = New System.Drawing.Point(213, 197)
    Me.chk_terminal_location.Name = "chk_terminal_location"
    Me.chk_terminal_location.Size = New System.Drawing.Size(296, 16)
    Me.chk_terminal_location.TabIndex = 3
    Me.chk_terminal_location.Text = "xShow terminals location"
    '
    'gb_filter_values
    '
    Me.gb_filter_values.Controls.Add(Me.rb_with_values)
    Me.gb_filter_values.Controls.Add(Me.rb_without_values)
    Me.gb_filter_values.Controls.Add(Me.rb_all)
    Me.gb_filter_values.Location = New System.Drawing.Point(6, 108)
    Me.gb_filter_values.Name = "gb_filter_values"
    Me.gb_filter_values.Size = New System.Drawing.Size(194, 83)
    Me.gb_filter_values.TabIndex = 1
    Me.gb_filter_values.TabStop = False
    '
    'rb_with_values
    '
    Me.rb_with_values.AutoSize = True
    Me.rb_with_values.Location = New System.Drawing.Point(6, 61)
    Me.rb_with_values.Name = "rb_with_values"
    Me.rb_with_values.Size = New System.Drawing.Size(94, 17)
    Me.rb_with_values.TabIndex = 2
    Me.rb_with_values.TabStop = True
    Me.rb_with_values.Text = "xWithValues"
    Me.rb_with_values.UseVisualStyleBackColor = True
    '
    'rb_without_values
    '
    Me.rb_without_values.AutoSize = True
    Me.rb_without_values.Location = New System.Drawing.Point(6, 38)
    Me.rb_without_values.Name = "rb_without_values"
    Me.rb_without_values.Size = New System.Drawing.Size(112, 17)
    Me.rb_without_values.TabIndex = 1
    Me.rb_without_values.TabStop = True
    Me.rb_without_values.Text = "xWithoutValues"
    Me.rb_without_values.UseVisualStyleBackColor = True
    '
    'rb_all
    '
    Me.rb_all.AutoSize = True
    Me.rb_all.Location = New System.Drawing.Point(6, 15)
    Me.rb_all.Name = "rb_all"
    Me.rb_all.Size = New System.Drawing.Size(65, 17)
    Me.rb_all.TabIndex = 0
    Me.rb_all.TabStop = True
    Me.rb_all.Text = "xTodos"
    Me.rb_all.UseVisualStyleBackColor = True
    '
    'dg_alarms
    '
    Me.dg_alarms.CurrentCol = -1
    Me.dg_alarms.CurrentRow = -1
    Me.dg_alarms.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_alarms.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_alarms.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_alarms.Location = New System.Drawing.Point(6, 321)
    Me.dg_alarms.Name = "dg_alarms"
    Me.dg_alarms.PanelRightVisible = True
    Me.dg_alarms.Redraw = True
    Me.dg_alarms.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
    Me.dg_alarms.Size = New System.Drawing.Size(1174, 103)
    Me.dg_alarms.Sortable = False
    Me.dg_alarms.SortAscending = True
    Me.dg_alarms.SortByCol = 0
    Me.dg_alarms.TabIndex = 11
    Me.dg_alarms.ToolTipped = True
    Me.dg_alarms.TopRow = -2
    Me.dg_alarms.WordWrap = False
    '
    'gb_info
    '
    Me.gb_info.Controls.Add(Me.lbl_max_increment)
    Me.gb_info.Controls.Add(Me.lbl_max_value)
    Me.gb_info.Controls.Add(Me.lbl_meter)
    Me.gb_info.Location = New System.Drawing.Point(893, 18)
    Me.gb_info.Name = "gb_info"
    Me.gb_info.Size = New System.Drawing.Size(287, 114)
    Me.gb_info.TabIndex = 5
    Me.gb_info.TabStop = False
    Me.gb_info.Text = "xInfo"
    '
    'lbl_max_increment
    '
    Me.lbl_max_increment.AutoSize = True
    Me.lbl_max_increment.Location = New System.Drawing.Point(6, 90)
    Me.lbl_max_increment.Name = "lbl_max_increment"
    Me.lbl_max_increment.Size = New System.Drawing.Size(96, 13)
    Me.lbl_max_increment.TabIndex = 2
    Me.lbl_max_increment.Text = "xMaxIncrement"
    '
    'lbl_max_value
    '
    Me.lbl_max_value.AutoSize = True
    Me.lbl_max_value.Location = New System.Drawing.Point(6, 68)
    Me.lbl_max_value.Name = "lbl_max_value"
    Me.lbl_max_value.Size = New System.Drawing.Size(68, 13)
    Me.lbl_max_value.TabIndex = 1
    Me.lbl_max_value.Text = "xMaxValue"
    '
    'lbl_meter
    '
    Me.lbl_meter.Location = New System.Drawing.Point(6, 26)
    Me.lbl_meter.Name = "lbl_meter"
    Me.lbl_meter.Size = New System.Drawing.Size(275, 41)
    Me.lbl_meter.TabIndex = 0
    Me.lbl_meter.Text = "xMeter"
    '
    'lbl_information
    '
    Me.lbl_information.Location = New System.Drawing.Point(893, 135)
    Me.lbl_information.Name = "lbl_information"
    Me.lbl_information.Size = New System.Drawing.Size(276, 51)
    Me.lbl_information.TabIndex = 6
    Me.lbl_information.Text = "xInformation"
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 18)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(194, 80)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 43)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(184, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 18)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(184, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2010, 5, 10, 0, 0, 0, 0)
    '
    'frm_terminal_sas_meters_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1283, 676)
    Me.Name = "frm_terminal_sas_meters_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_terminal_sas_meters_edit"
    Me.panel_grids.ResumeLayout(False)
    Me.panel_filter.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_filter_values.ResumeLayout(False)
    Me.gb_filter_values.PerformLayout()
    Me.gb_info.ResumeLayout(False)
    Me.gb_info.PerformLayout()
    Me.gb_date.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_provider As GUI_Controls.uc_provider
  Friend WithEvents uc_checked_list_groups As GUI_Controls.uc_checked_list
  Friend WithEvents dg_meters As GUI_Controls.uc_grid
  Friend WithEvents chk_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents gb_filter_values As System.Windows.Forms.GroupBox
  Friend WithEvents rb_with_values As System.Windows.Forms.RadioButton
  Friend WithEvents rb_without_values As System.Windows.Forms.RadioButton
  Friend WithEvents rb_all As System.Windows.Forms.RadioButton
  Friend WithEvents dg_alarms As GUI_Controls.uc_grid
  Friend WithEvents gb_info As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_meter As System.Windows.Forms.Label
  Friend WithEvents lbl_max_value As System.Windows.Forms.Label
  Friend WithEvents lbl_max_increment As System.Windows.Forms.Label
  Friend WithEvents lbl_information As System.Windows.Forms.Label
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
End Class
