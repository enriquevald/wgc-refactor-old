<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cashier_collection_edit
    Inherits GUI_Controls.frm_base_edit

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.dg_tickets = New GUI_Controls.uc_grid
    Me.gb_status = New System.Windows.Forms.GroupBox
    Me.lbl_error = New System.Windows.Forms.Label
    Me.tf_error = New GUI_Controls.uc_text_field
    Me.lbl_ok = New System.Windows.Forms.Label
    Me.tf_ok = New GUI_Controls.uc_text_field
    Me.ef_ticket = New GUI_Controls.uc_entry_field
    Me.btn_save_ticket = New GUI_Controls.uc_button
    Me.ef_cashier_collection_datetime = New GUI_Controls.uc_entry_field
    Me.ef_cashier_terminal_name = New GUI_Controls.uc_entry_field
    Me.gb_theoretical_values = New System.Windows.Forms.GroupBox
    Me.ef_theoretical_sum = New GUI_Controls.uc_entry_field
    Me.ef_theoretical_count = New GUI_Controls.uc_entry_field
    Me.gb_real_values = New System.Windows.Forms.GroupBox
    Me.ef_real_sum = New GUI_Controls.uc_entry_field
    Me.ef_real_count = New GUI_Controls.uc_entry_field
    Me.gb_collection_details = New System.Windows.Forms.GroupBox
    Me.lbl_notes = New System.Windows.Forms.Label
    Me.tb_notes = New System.Windows.Forms.TextBox
    Me.ef_collection_user = New GUI_Controls.uc_entry_field
    Me.ef_collection_status = New GUI_Controls.uc_entry_field
    Me.panel_data.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.gb_theoretical_values.SuspendLayout()
    Me.gb_real_values.SuspendLayout()
    Me.gb_collection_details.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_collection_details)
    Me.panel_data.Controls.Add(Me.gb_real_values)
    Me.panel_data.Controls.Add(Me.gb_theoretical_values)
    Me.panel_data.Controls.Add(Me.gb_status)
    Me.panel_data.Controls.Add(Me.ef_ticket)
    Me.panel_data.Controls.Add(Me.btn_save_ticket)
    Me.panel_data.Controls.Add(Me.dg_tickets)
    Me.panel_data.Size = New System.Drawing.Size(500, 476)
    '
    'dg_tickets
    '
    Me.dg_tickets.CurrentCol = -1
    Me.dg_tickets.CurrentRow = -1
    Me.dg_tickets.Location = New System.Drawing.Point(3, 260)
    Me.dg_tickets.Name = "dg_tickets"
    Me.dg_tickets.PanelRightVisible = False
    Me.dg_tickets.Redraw = True
    Me.dg_tickets.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_tickets.Size = New System.Drawing.Size(364, 211)
    Me.dg_tickets.Sortable = False
    Me.dg_tickets.SortAscending = True
    Me.dg_tickets.SortByCol = 0
    Me.dg_tickets.TabIndex = 0
    Me.dg_tickets.ToolTipped = True
    Me.dg_tickets.TopRow = -2
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.lbl_error)
    Me.gb_status.Controls.Add(Me.tf_error)
    Me.gb_status.Controls.Add(Me.lbl_ok)
    Me.gb_status.Controls.Add(Me.tf_ok)
    Me.gb_status.Location = New System.Drawing.Point(376, 251)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(123, 70)
    Me.gb_status.TabIndex = 114
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'lbl_error
    '
    Me.lbl_error.BackColor = System.Drawing.SystemColors.GrayText
    Me.lbl_error.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_error.Location = New System.Drawing.Point(10, 44)
    Me.lbl_error.Name = "lbl_error"
    Me.lbl_error.Size = New System.Drawing.Size(16, 16)
    Me.lbl_error.TabIndex = 110
    '
    'tf_error
    '
    Me.tf_error.IsReadOnly = True
    Me.tf_error.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_error.LabelForeColor = System.Drawing.Color.Black
    Me.tf_error.Location = New System.Drawing.Point(4, 40)
    Me.tf_error.Name = "tf_error"
    Me.tf_error.Size = New System.Drawing.Size(116, 24)
    Me.tf_error.SufixText = "Sufix Text"
    Me.tf_error.SufixTextVisible = True
    Me.tf_error.TabIndex = 109
    Me.tf_error.TabStop = False
    Me.tf_error.TextVisible = False
    Me.tf_error.TextWidth = 30
    Me.tf_error.Value = "xInactive"
    '
    'lbl_ok
    '
    Me.lbl_ok.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_ok.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_ok.Location = New System.Drawing.Point(10, 19)
    Me.lbl_ok.Name = "lbl_ok"
    Me.lbl_ok.Size = New System.Drawing.Size(16, 16)
    Me.lbl_ok.TabIndex = 106
    '
    'tf_ok
    '
    Me.tf_ok.IsReadOnly = True
    Me.tf_ok.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_ok.LabelForeColor = System.Drawing.Color.Black
    Me.tf_ok.Location = New System.Drawing.Point(4, 15)
    Me.tf_ok.Name = "tf_ok"
    Me.tf_ok.Size = New System.Drawing.Size(114, 24)
    Me.tf_ok.SufixText = "Sufix Text"
    Me.tf_ok.SufixTextVisible = True
    Me.tf_ok.TabIndex = 104
    Me.tf_ok.TabStop = False
    Me.tf_ok.TextVisible = False
    Me.tf_ok.TextWidth = 30
    Me.tf_ok.Value = "xRunning"
    '
    'ef_ticket
    '
    Me.ef_ticket.DoubleValue = 0
    Me.ef_ticket.IntegerValue = 0
    Me.ef_ticket.IsReadOnly = False
    Me.ef_ticket.Location = New System.Drawing.Point(3, 222)
    Me.ef_ticket.Name = "ef_ticket"
    Me.ef_ticket.Size = New System.Drawing.Size(248, 24)
    Me.ef_ticket.SufixText = "Sufix Text"
    Me.ef_ticket.SufixTextVisible = True
    Me.ef_ticket.TabIndex = 112
    Me.ef_ticket.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_ticket.TextValue = ""
    Me.ef_ticket.Value = ""
    '
    'btn_save_ticket
    '
    Me.btn_save_ticket.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_save_ticket.Location = New System.Drawing.Point(277, 222)
    Me.btn_save_ticket.Name = "btn_save_ticket"
    Me.btn_save_ticket.Size = New System.Drawing.Size(90, 30)
    Me.btn_save_ticket.TabIndex = 113
    Me.btn_save_ticket.ToolTipped = False
    Me.btn_save_ticket.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'ef_cashier_collection_datetime
    '
    Me.ef_cashier_collection_datetime.DoubleValue = 0
    Me.ef_cashier_collection_datetime.IntegerValue = 0
    Me.ef_cashier_collection_datetime.IsReadOnly = False
    Me.ef_cashier_collection_datetime.Location = New System.Drawing.Point(5, 14)
    Me.ef_cashier_collection_datetime.Name = "ef_cashier_collection_datetime"
    Me.ef_cashier_collection_datetime.Size = New System.Drawing.Size(238, 24)
    Me.ef_cashier_collection_datetime.SufixText = "Sufix Text"
    Me.ef_cashier_collection_datetime.SufixTextVisible = True
    Me.ef_cashier_collection_datetime.TabIndex = 115
    Me.ef_cashier_collection_datetime.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cashier_collection_datetime.TextValue = ""
    Me.ef_cashier_collection_datetime.Value = ""
    '
    'ef_cashier_terminal_name
    '
    Me.ef_cashier_terminal_name.DoubleValue = 0
    Me.ef_cashier_terminal_name.IntegerValue = 0
    Me.ef_cashier_terminal_name.IsReadOnly = False
    Me.ef_cashier_terminal_name.Location = New System.Drawing.Point(6, 45)
    Me.ef_cashier_terminal_name.Name = "ef_cashier_terminal_name"
    Me.ef_cashier_terminal_name.Size = New System.Drawing.Size(237, 24)
    Me.ef_cashier_terminal_name.SufixText = "Sufix Text"
    Me.ef_cashier_terminal_name.SufixTextVisible = True
    Me.ef_cashier_terminal_name.TabIndex = 116
    Me.ef_cashier_terminal_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cashier_terminal_name.TextValue = ""
    Me.ef_cashier_terminal_name.Value = ""
    '
    'gb_theoretical_values
    '
    Me.gb_theoretical_values.Controls.Add(Me.ef_theoretical_sum)
    Me.gb_theoretical_values.Controls.Add(Me.ef_theoretical_count)
    Me.gb_theoretical_values.Location = New System.Drawing.Point(3, 131)
    Me.gb_theoretical_values.Name = "gb_theoretical_values"
    Me.gb_theoretical_values.Size = New System.Drawing.Size(243, 84)
    Me.gb_theoretical_values.TabIndex = 118
    Me.gb_theoretical_values.TabStop = False
    Me.gb_theoretical_values.Text = "xTheoreticalValues"
    '
    'ef_theoretical_sum
    '
    Me.ef_theoretical_sum.DoubleValue = 0
    Me.ef_theoretical_sum.IntegerValue = 0
    Me.ef_theoretical_sum.IsReadOnly = False
    Me.ef_theoretical_sum.Location = New System.Drawing.Point(37, 52)
    Me.ef_theoretical_sum.Name = "ef_theoretical_sum"
    Me.ef_theoretical_sum.Size = New System.Drawing.Size(200, 24)
    Me.ef_theoretical_sum.SufixText = "Sufix Text"
    Me.ef_theoretical_sum.SufixTextVisible = True
    Me.ef_theoretical_sum.TabIndex = 1
    Me.ef_theoretical_sum.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_theoretical_sum.TextValue = ""
    Me.ef_theoretical_sum.Value = ""
    '
    'ef_theoretical_count
    '
    Me.ef_theoretical_count.DoubleValue = 0
    Me.ef_theoretical_count.IntegerValue = 0
    Me.ef_theoretical_count.IsReadOnly = False
    Me.ef_theoretical_count.Location = New System.Drawing.Point(37, 22)
    Me.ef_theoretical_count.Name = "ef_theoretical_count"
    Me.ef_theoretical_count.Size = New System.Drawing.Size(200, 24)
    Me.ef_theoretical_count.SufixText = "Sufix Text"
    Me.ef_theoretical_count.SufixTextVisible = True
    Me.ef_theoretical_count.TabIndex = 0
    Me.ef_theoretical_count.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_theoretical_count.TextValue = ""
    Me.ef_theoretical_count.Value = ""
    '
    'gb_real_values
    '
    Me.gb_real_values.Controls.Add(Me.ef_real_sum)
    Me.gb_real_values.Controls.Add(Me.ef_real_count)
    Me.gb_real_values.Location = New System.Drawing.Point(254, 131)
    Me.gb_real_values.Name = "gb_real_values"
    Me.gb_real_values.Size = New System.Drawing.Size(243, 84)
    Me.gb_real_values.TabIndex = 119
    Me.gb_real_values.TabStop = False
    Me.gb_real_values.Text = "xRealValues"
    '
    'ef_real_sum
    '
    Me.ef_real_sum.DoubleValue = 0
    Me.ef_real_sum.IntegerValue = 0
    Me.ef_real_sum.IsReadOnly = False
    Me.ef_real_sum.Location = New System.Drawing.Point(37, 51)
    Me.ef_real_sum.Name = "ef_real_sum"
    Me.ef_real_sum.Size = New System.Drawing.Size(200, 24)
    Me.ef_real_sum.SufixText = "Sufix Text"
    Me.ef_real_sum.SufixTextVisible = True
    Me.ef_real_sum.TabIndex = 1
    Me.ef_real_sum.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_real_sum.TextValue = ""
    Me.ef_real_sum.Value = ""
    '
    'ef_real_count
    '
    Me.ef_real_count.DoubleValue = 0
    Me.ef_real_count.IntegerValue = 0
    Me.ef_real_count.IsReadOnly = False
    Me.ef_real_count.Location = New System.Drawing.Point(37, 21)
    Me.ef_real_count.Name = "ef_real_count"
    Me.ef_real_count.Size = New System.Drawing.Size(200, 24)
    Me.ef_real_count.SufixText = "Sufix Text"
    Me.ef_real_count.SufixTextVisible = True
    Me.ef_real_count.TabIndex = 0
    Me.ef_real_count.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_real_count.TextValue = ""
    Me.ef_real_count.Value = ""
    '
    'gb_collection_details
    '
    Me.gb_collection_details.Controls.Add(Me.lbl_notes)
    Me.gb_collection_details.Controls.Add(Me.tb_notes)
    Me.gb_collection_details.Controls.Add(Me.ef_collection_user)
    Me.gb_collection_details.Controls.Add(Me.ef_collection_status)
    Me.gb_collection_details.Controls.Add(Me.ef_cashier_collection_datetime)
    Me.gb_collection_details.Controls.Add(Me.ef_cashier_terminal_name)
    Me.gb_collection_details.Location = New System.Drawing.Point(3, 4)
    Me.gb_collection_details.Name = "gb_collection_details"
    Me.gb_collection_details.Size = New System.Drawing.Size(497, 121)
    Me.gb_collection_details.TabIndex = 120
    Me.gb_collection_details.TabStop = False
    Me.gb_collection_details.Text = "xCollectionDetails"
    '
    'lbl_notes
    '
    Me.lbl_notes.AutoSize = True
    Me.lbl_notes.Location = New System.Drawing.Point(42, 76)
    Me.lbl_notes.Name = "lbl_notes"
    Me.lbl_notes.Size = New System.Drawing.Size(46, 13)
    Me.lbl_notes.TabIndex = 120
    Me.lbl_notes.Text = "xNotes"
    '
    'tb_notes
    '
    Me.tb_notes.Location = New System.Drawing.Point(88, 76)
    Me.tb_notes.MaxLength = 199
    Me.tb_notes.Multiline = True
    Me.tb_notes.Name = "tb_notes"
    Me.tb_notes.Size = New System.Drawing.Size(398, 37)
    Me.tb_notes.TabIndex = 119
    '
    'ef_collection_user
    '
    Me.ef_collection_user.DoubleValue = 0
    Me.ef_collection_user.IntegerValue = 0
    Me.ef_collection_user.IsReadOnly = False
    Me.ef_collection_user.Location = New System.Drawing.Point(251, 14)
    Me.ef_collection_user.Name = "ef_collection_user"
    Me.ef_collection_user.Size = New System.Drawing.Size(237, 24)
    Me.ef_collection_user.SufixText = "Sufix Text"
    Me.ef_collection_user.SufixTextVisible = True
    Me.ef_collection_user.TabIndex = 117
    Me.ef_collection_user.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_collection_user.TextValue = ""
    Me.ef_collection_user.Value = ""
    '
    'ef_collection_status
    '
    Me.ef_collection_status.DoubleValue = 0
    Me.ef_collection_status.IntegerValue = 0
    Me.ef_collection_status.IsReadOnly = False
    Me.ef_collection_status.Location = New System.Drawing.Point(249, 45)
    Me.ef_collection_status.Name = "ef_collection_status"
    Me.ef_collection_status.Size = New System.Drawing.Size(238, 24)
    Me.ef_collection_status.SufixText = "Sufix Text"
    Me.ef_collection_status.SufixTextVisible = True
    Me.ef_collection_status.TabIndex = 118
    Me.ef_collection_status.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_collection_status.TextValue = ""
    Me.ef_collection_status.Value = ""
    '
    'frm_cashier_collection_edit
    '
    Me.ClientSize = New System.Drawing.Size(602, 485)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_cashier_collection_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_theoretical_values.ResumeLayout(False)
    Me.gb_real_values.ResumeLayout(False)
    Me.gb_collection_details.ResumeLayout(False)
    Me.gb_collection_details.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents dg_tickets As GUI_Controls.uc_grid
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_error As System.Windows.Forms.Label
  Friend WithEvents tf_error As GUI_Controls.uc_text_field
  Friend WithEvents lbl_ok As System.Windows.Forms.Label
  Friend WithEvents tf_ok As GUI_Controls.uc_text_field
  Friend WithEvents ef_ticket As GUI_Controls.uc_entry_field
  Friend WithEvents btn_save_ticket As GUI_Controls.uc_button
  Friend WithEvents ef_cashier_terminal_name As GUI_Controls.uc_entry_field
  Friend WithEvents ef_cashier_collection_datetime As GUI_Controls.uc_entry_field
  Friend WithEvents gb_real_values As System.Windows.Forms.GroupBox
  Friend WithEvents ef_real_sum As GUI_Controls.uc_entry_field
  Friend WithEvents ef_real_count As GUI_Controls.uc_entry_field
  Friend WithEvents gb_theoretical_values As System.Windows.Forms.GroupBox
  Friend WithEvents ef_theoretical_sum As GUI_Controls.uc_entry_field
  Friend WithEvents ef_theoretical_count As GUI_Controls.uc_entry_field
  Friend WithEvents gb_collection_details As System.Windows.Forms.GroupBox
  Friend WithEvents ef_collection_user As GUI_Controls.uc_entry_field
  Friend WithEvents ef_collection_status As GUI_Controls.uc_entry_field
  Friend WithEvents tb_notes As System.Windows.Forms.TextBox
  Friend WithEvents lbl_notes As System.Windows.Forms.Label

End Class
