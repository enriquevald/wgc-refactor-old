'--------------------------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'--------------------------------------------------------------------------------------
'
'   MODULE NAME :  frm_cash_tito_sel_date.vb
'
'   DESCRIPTION :  Total Cash in site grouped by date(TITO)
'
'        AUTHOR :  Ignasi Carre�o
'
' CREATION DATE :  02-DEC-2013
'
' REVISION HISTORY
'
' Date         Author Description
' -----------  ------ ------------------------------------------------------------------
' 02-DEC-2013  ICS    Initial version
'---------------------------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports System.Text
Imports System.Data.SqlClient
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common

Public Class frm_tito_cash_sel_date
  Inherits GUI_Controls.frm_base_sel

#Region " Constants "

  ' Grid configuration
  Private Const GRID_STATIC_COLUMNS_COUNT As Integer = 8
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Columns Index
  Private Const START_COLUMN_MONEY As Integer = 2
  Private Const GRID_COLUMN_START_PRINT As Integer = 1
  Private Const GRID_COLUMN_SELECT As Integer = 0
  Private Const GRID_COLUMN_DATE_SINCE As Integer = 1
  Private Const GRID_COLUMN_DATE_UNTIL As Integer = 2
  Private Const GRID_COLUMN_TERM_REDIM As Integer = 3
  Private Const GRID_COLUMN_TERM_NOREDIM As Integer = 4
  Private Const GRID_COLUMN_TICKET_REDIM As Integer = 5
  Private Const GRID_COLUMN_TICKET_NOREDIM As Integer = 6
  Private Const GRID_COLUMN_BILLS_TOTAL As Integer = 7
  Private Const GRID_COLUMN_BILLS_COUNT As Integer = 8
  Private Const GRID_COLUMN_FIRST_BILL As Integer = 9

  ' SQL Columns
  Private Const SQL_COLUMN_DATE_SINCE As Integer = 0
  Private Const SQL_COLUMN_DATE_UNTIL As Integer = 1
  Private Const SQL_COLUMN_TERM_REDIM As Integer = 2
  Private Const SQL_COLUMN_TERM_NOREDIM As Integer = 3
  Private Const SQL_COLUMN_TICKET_REDIM As Integer = 4
  Private Const SQL_COLUMN_TICKET_NOREDIM As Integer = 5
  Private Const SQL_COLUMN_BILLS_TOTAL As Integer = 6
  Private Const SQL_COLUMN_BILLS_COUNT As Integer = 7

  ' Columns Width
  Private Const GRID_COLUMN_WIDTH_SELECT As Integer = 200
  Private Const GRID_COLUMN_WIDTH_TERM_REDIM As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_TERM_NOREDIM As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_TICKET_REDIM As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_TICKET_NOREDIM As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_BILLS_TOTAL As Integer = 1500
  Private Const GRID_COLUMN_WIDTH_BILLS_COUNT As Integer = 1420

  'Cashier Grid
  Private Const GRID_CASHIER_HEADER_ROWS As Integer = 0
  Private Const GRID_CASHIER_COLUMNS As Integer = 3
  Private Const GRID_CASHIER_ID As Integer = 0
  Private Const GRID_CASHIER_COLUMN_CHECKED As Integer = 1
  Private Const GRID_CASHIER_COLUMN_DESCRIPTION As Integer = 2
  Private Const GRID_CASHIER_WIDTH_COLUMN_DESCRIPTION As Integer = 3630
  Private Const GRID_CASHIER_WIDTH_COLUMN_CHECKED As Integer = 300

#End Region ' Constants

#Region " Members"

  Private m_prev_terminal_type As Integer
  Private Const m_terminal_type_totals As Integer = 9999
  Private m_grid_total_columns As Integer
  Private m_bill_types As DataTable
  Private m_bill_index As New Hashtable()

  Private m_rows As String

  ' Dates
  Private m_dates As Dictionary(Of Date, Double())

  ' List of SUMs of the columns
  Private m_total_dates As Array

  ' When introducing data in the table, we will insert a numeric prefix to maintain the order. This prefix will be 
  ' removed before sending to the printing routines
  Private m_filter_params As New Hashtable()

#End Region ' Members

#Region " Private Functions and Methods "

  ' PURPOSE : Define layout of all Main Grid Columns
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  Private Sub GUI_StyleSheet()

    Dim _idx As Integer

    m_grid_total_columns = GRID_STATIC_COLUMNS_COUNT + m_bill_types.Rows.Count + 1
    With Me.Grid
      Call .Init(m_grid_total_columns, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = True

      .Column(GRID_COLUMN_SELECT).Header(0).Text = ""
      .Column(GRID_COLUMN_SELECT).Header(1).Text = ""
      .Column(GRID_COLUMN_SELECT).Width = GRID_COLUMN_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False

      .Column(GRID_COLUMN_DATE_SINCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_COLUMN_DATE_SINCE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(202)
      .Column(GRID_COLUMN_DATE_SINCE).Width = 1700

      .Column(GRID_COLUMN_DATE_UNTIL).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_COLUMN_DATE_UNTIL).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(203)
      .Column(GRID_COLUMN_DATE_UNTIL).Width = 1700

      .Column(GRID_COLUMN_TERM_REDIM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2310)
      .Column(GRID_COLUMN_TERM_REDIM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125)
      .Column(GRID_COLUMN_TERM_REDIM).Width = GRID_COLUMN_WIDTH_TERM_REDIM

      .Column(GRID_COLUMN_TERM_NOREDIM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2310)
      .Column(GRID_COLUMN_TERM_NOREDIM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2126)
      .Column(GRID_COLUMN_TERM_NOREDIM).Width = GRID_COLUMN_WIDTH_TERM_NOREDIM

      .Column(GRID_COLUMN_TICKET_REDIM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2311)
      .Column(GRID_COLUMN_TICKET_REDIM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125)
      .Column(GRID_COLUMN_TICKET_REDIM).Width = GRID_COLUMN_WIDTH_TICKET_REDIM

      .Column(GRID_COLUMN_TICKET_NOREDIM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2311)
      .Column(GRID_COLUMN_TICKET_NOREDIM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2126)
      .Column(GRID_COLUMN_TICKET_NOREDIM).Width = GRID_COLUMN_WIDTH_TICKET_NOREDIM

      .Column(GRID_COLUMN_BILLS_TOTAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2208)
      .Column(GRID_COLUMN_BILLS_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1573)
      .Column(GRID_COLUMN_BILLS_TOTAL).Width = GRID_COLUMN_WIDTH_BILLS_TOTAL

      .Column(GRID_COLUMN_BILLS_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2208)
      .Column(GRID_COLUMN_BILLS_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2265)
      .Column(GRID_COLUMN_BILLS_COUNT).Width = GRID_COLUMN_WIDTH_BILLS_COUNT

      For _idx = 0 To m_bill_types.Rows.Count - 1
        .Column(GRID_COLUMN_FIRST_BILL + _idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2208)
        .Column(GRID_COLUMN_FIRST_BILL + _idx).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2888, GUI_FormatCurrency(m_bill_types.Rows(_idx).Item("CTM_DENOMINATION"), 0))
        .Column(GRID_COLUMN_FIRST_BILL + _idx).Width = GRID_COLUMN_WIDTH_BILLS_COUNT

        m_bill_index.Add(GUI_FormatCurrency(m_bill_types.Rows(_idx).Item("CTM_DENOMINATION")), GRID_COLUMN_FIRST_BILL + _idx)
      Next
    End With

  End Sub ' GUI_StyleSheet


  ' PURPOSE : Acumulates values for totals
  '
  '  PARAMS :
  '     - INPUT :
  '           - DbRow: data of actual row
  '     - OUTPUT :
  '           - None
  '
  Private Sub SumValuesInArray(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW)

    Dim _idx As Integer
    Dim _array_totals(0) As Double

    If Not DbRow Is Nothing Then

      _array_totals = m_total_dates

      If _array_totals Is Nothing Then
        ReDim _array_totals(m_grid_total_columns)
        m_total_dates = _array_totals
      End If

      For _idx = GRID_COLUMN_TERM_REDIM To GRID_COLUMN_TICKET_NOREDIM
        _array_totals(_idx) += DbRow.Value(_idx)
      Next _idx

      If DbRow.Value(SQL_COLUMN_BILLS_COUNT) <> 0 And m_bill_index.ContainsKey(GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_BILLS_TOTAL))) Then
        _array_totals(GRID_COLUMN_BILLS_COUNT) += DbRow.Value(SQL_COLUMN_BILLS_COUNT)
        _array_totals(GRID_COLUMN_BILLS_TOTAL) += DbRow.Value(SQL_COLUMN_BILLS_COUNT) * DbRow.Value(SQL_COLUMN_BILLS_TOTAL)
        _idx = m_bill_index.Item(GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_BILLS_TOTAL)))
        _array_totals(_idx) += DbRow.Value(SQL_COLUMN_BILLS_TOTAL) * DbRow.Value(SQL_COLUMN_BILLS_COUNT)
      End If
    End If

  End Sub ' SumValuesInArray

  ' PURPOSE : Shows totals and SubTotals values in the grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex: index of the row-grid
  '     - OUTPUT :
  '           - None
  '
  Private Sub SumToGrid(ByVal RowIndex As Integer)

    Dim _idx As Integer
    Dim _value As Object
    Dim _color As Integer
    Dim _array(0) As Double

    _array = m_total_dates
    _color = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00

    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_SINCE).Value = GLB_NLS_GUI_INVOICING.GetString(205)  '"TOTAL: "

    For _idx = GRID_COLUMN_SELECT To m_grid_total_columns - 1
      Me.Grid.Cell(RowIndex, _idx).BackColor = GetColor(_color)
      If _idx >= GRID_COLUMN_TERM_REDIM Then
        If _array Is Nothing Then
          _value = 0
        Else
          _value = _array(_idx)
        End If
        If _idx <> GRID_COLUMN_BILLS_COUNT Then
          Me.Grid.Cell(RowIndex, _idx).Value = GetFormatedValue(_idx, _value)
        Else
          Me.Grid.Cell(RowIndex, _idx).Value = GUI_FormatNumber(_value, 0)
        End If
        If m_bill_index.ContainsValue(_idx) Then
          Me.Grid.Cell(RowIndex, _idx).Value = GUI_FormatCurrency(_value, 0)
        End If
      End If
    Next

  End Sub ' SumToGrid

  ' PURPOSE : Return a condition to filter by ColumnName param
  '
  '  PARAMS :
  '     - INPUT :
  '           - ColumnName: name of BD column to filter by
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - Composed string for filter
  '
  Private Function GetSelectedTerminals(ByVal ColumnName As String) As String

    Dim _result As String
    Dim _terminals() As Long

    _result = ""

    _terminals = uc_pr_list.GetTerminalIdListSelected()

    If Not _terminals Is Nothing Then
      For Each _idx As Long In _terminals
        _result &= IIf(_result.Length > 0, ",", "")
        _result &= _idx.ToString()
      Next
      If Not String.IsNullOrEmpty(_result) Then
        _result = ColumnName & " IN (" & _result & ")"
      End If
    End If

    Return _result

  End Function ' GetSelectedTerminals

  ' PURPOSE: Add new filter item form export/print purpouses
  '
  '  PARAMS:
  '     - INPUT :
  '       - Label: string with label to print
  '       - Value: text value to print
  '
  Private Sub AddFilterItem(ByVal Label As String, ByVal Value As String)

    m_filter_params.Add(m_filter_params.Count, Label & ";" & Value)

  End Sub ' AddFilterItem


  ' PURPOSE: get selected cashiers/terminals from grids, for filter purpouses
  '
  '  PARAMS:
  '     - INPUT:
  '           - ColumnName: column for query construction
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - it could be a list of Ids or a list of alias
  Private Function GetSelectedCashiersOrTerminals(ByVal ColumnName As String) As String

    Dim _result As String
    '  Dim _idx_row As Integer
    Dim _id_list As String
    Dim _id_list_all As String

    ' Terminales
    _result = GetSelectedTerminals(ColumnName)

    ' Cashiers
    _id_list = ""
    _id_list_all = ""

    If Not String.IsNullOrEmpty(_id_list) Then
      _id_list = ColumnName & " IN (" & _id_list & ")"
      If Not String.IsNullOrEmpty(_result) Then
        _result &= " OR "
      End If
      _result &= _id_list
    End If

    If Not String.IsNullOrEmpty(_result) Then
      _result = "(" & _result & ")"
    End If

    Return _result

  End Function ' GetSelectedCashiersOrTerminals


  ' PURPOSE: return formated value to pu in grid
  '
  '  PARAMS:
  '     - INPUT:
  '           - IdxCol: index of column in the grid
  '           - Value: valur to be formated
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - A formated string or Empty string
  '
  Public Function GetFormatedValue(ByVal IdxCol As Integer, ByVal Value As Object) As String
    Dim _value As String = ""

    If Not DBNull.Value.Equals(Value) Then
      If IdxCol >= GRID_COLUMN_TERM_REDIM Then
        _value = GUI_FormatCurrency(Value, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      Else
        _value = Value
      End If
    End If

    Return _value

  End Function ' GetFormatedValue

  ' PURPOSE: Gets all Bill types from the current currency
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GetBillTypes()
    Dim _sql_query As String

    _sql_query = " SELECT    DISTINCT(CTM_DENOMINATION) " _
               + " FROM      CASHIER_TERMINAL_MONEY "

    m_bill_types = GUI_GetTableUsingSQL(_sql_query, 100)

  End Sub ' GetBillTypes

  ' PURPOSE: Add grid days without activity
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddDaysGrid()

    Dim _idx_day As Integer
    Dim _idx_row As Integer
    Dim _date_add As Date
    Dim _date_grid As Date
    Dim _interval_days As Integer
    Dim _day_exists As Boolean
    Dim _filter_from As Date
    Dim _filter_to As Date
    Dim _aux_date_to As Date

    _filter_from = uc_dates.FromDate
    _filter_to = uc_dates.ToDate
    If (uc_dates.FromDateSelected = False) Then
      If (Me.Grid.NumRows > 0) Then
        _filter_from = Me.Grid.Cell(0, GRID_COLUMN_DATE_SINCE).Value
      Else
        If (uc_dates.ToDateSelected = False) Then
          _filter_from = Date.Now
        Else
          _filter_from = _filter_to.AddDays(-1)
        End If
      End If
    End If
    If (uc_dates.ToDateSelected = False) Then
      _filter_to = Now()
      ' Make sure that interval containing current session is included
      _aux_date_to = _filter_to.Date.AddHours(uc_dates.ClosingTime)
      If _aux_date_to <= _filter_to Then
        _filter_to = _filter_to.AddDays(1)
      End If
    End If
    _filter_from = _filter_from.Date()
    _filter_to = _filter_to.Date()

    _day_exists = False
    _interval_days = DateDiff(DateInterval.Day, _filter_from, _filter_to)

    _date_add = _filter_from

    For _idx_day = 0 To _interval_days - 1
      For _idx_row = 0 To Me.Grid.NumRows - 1
        _date_grid = Me.Grid.Cell(_idx_row, GRID_COLUMN_DATE_SINCE).Value
        _date_grid = _date_grid.Date
        If (_date_add = _date_grid Or _date_add < _filter_from Or _date_add > _filter_to) Then
          _day_exists = True
        End If
      Next

      If _day_exists = False Then
        Call AddRowNullGrid(_idx_row, _date_add, _date_add)

      End If

      _day_exists = False
      _date_add = DateAdd(DateInterval.Day, 1, _date_add)
    Next

  End Sub ' AddDaysGrid

  ' PURPOSE: Add grid rows without activity 
  '
  '  PARAMS:
  '     - INPUT:
  '           - idx_row
  '           - date1
  '           - date2
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub AddRowNullGrid(ByVal idx_row As Integer, ByVal date_since As Date, ByVal date_until As Date)

    Dim _init_hour As Double
    Dim _idx_col As Integer

    Me.Grid.AddRow()

    _init_hour = uc_dates.ClosingTime

    ' Since DateTime
    date_since = date_since.AddHours(_init_hour)
    Me.Grid.Cell(idx_row, GRID_COLUMN_DATE_SINCE).Value = GUI_FormatDate(date_since, , ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Until DateTime
    date_until = date_until.AddDays(1)
    date_until = date_until.AddHours(_init_hour)
    Me.Grid.Cell(idx_row, GRID_COLUMN_DATE_UNTIL).Value = GUI_FormatDate(date_until, , ENUM_FORMAT_TIME.FORMAT_HHMM)

    ' Other Columns
    For _idx_col = GRID_COLUMN_TERM_REDIM To GRID_COLUMN_BILLS_TOTAL
      Me.Grid.Cell(idx_row, _idx_col).Value = GUI_FormatCurrency(0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
    Next

    ' Bills Columns
    For _idx_col = GRID_COLUMN_BILLS_COUNT To m_grid_total_columns - 1
      Me.Grid.Cell(idx_row, _idx_col).Value = GUI_FormatNumber(0, 0)
    Next

  End Sub ' AddRowNullGrid

#End Region ' Private Functions and Methods

#Region " OVERRIDES "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CASH_TITO_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3326)
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION

    ' Hide/Show some controls
    GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False

    Call GetBillTypes()
    ' Define layout of all Main Grid Columns
    Call GUI_StyleSheet()

    Call uc_pr_list.Init(WSI.Common.Misc.GamingTerminalTypeList())

    ' Set filter default values
    Call GUI_FilterReset()

    ' Date time an other filters
    uc_dates.ClosingTimeEnabled = False
    uc_dates.Init(GLB_NLS_GUI_PLAYER_TRACKING.Id(268))

    ' Checkbox rows
    Me.cb_rows.Text = GLB_NLS_GUI_STATISTICS.GetString(370)

    cb_rows.Checked = False

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FilterReset()

    Dim _closing_time As Integer
    Dim _final_time As Date

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(Now.Year, Now.Month, Now.Day, _closing_time, 0, 0)
    uc_dates.ToDate = _final_time.Date
    uc_dates.ToDateSelected = True
    uc_dates.FromDate = _final_time.AddDays(-1)
    uc_dates.FromDateSelected = True
    uc_dates.ClosingTime = _closing_time

    cb_rows.Checked = False

    Call uc_pr_list.SetDefaultValues()

  End Sub ' GUI_FilterReset

  Protected Overrides Function GUI_FilterCheck() As Boolean

    Dim _result As Boolean
    Dim _terminals() As Long

    _result = True

    ' se controla el rango de fechas
    If uc_dates.FromDateSelected And uc_dates.ToDateSelected Then
      If uc_dates.FromDate > uc_dates.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        _result = False
      End If
    End If

    If _result Then
      ' al marcar varios, al menos debe haber un elemento seleccionado
      _terminals = uc_pr_list.GetTerminalIdListSelected()
      If uc_pr_list.opt_several_types.Checked And (_terminals Is Nothing) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1411), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        _result = False
      End If
    End If

    Return _result

  End Function 'GUI_FilterCheck

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sb As StringBuilder
    Dim _type_redeem As String
    Dim _type_no_redeem As String
    Dim _type_handpay As String
    Dim _type_jackpot As String
    Dim _type_cashable As String
    Dim _init_hour As Integer
    Dim _str_date_time As String
    Dim _str_since As String
    Dim _str_until As String

    _sb = New StringBuilder()

    _type_cashable = CShort(TITO_TICKET_TYPE.CASHABLE).ToString()
    _type_redeem = CShort(TITO_TICKET_TYPE.PROMO_REDEEM).ToString()
    _type_no_redeem = CShort(TITO_TICKET_TYPE.PROMO_NONREDEEM).ToString()
    _type_handpay = CShort(TITO_TICKET_TYPE.HANDPAY).ToString()
    _type_jackpot = CShort(TITO_TICKET_TYPE.JACKPOT).ToString()

    _init_hour = WSI.Common.Misc.TodayOpening().Hour
    _str_date_time = "01-01-2007 " + _init_hour.ToString("00") + ":00:00"
    _str_since = "DATEADD(DAY, DATEDIFF(HOUR,'" & _str_date_time & "',T.DateDay)/24, '" + _str_date_time + "')"
    _str_until = "DATEADD(HOUR, 24, " & _str_since & ")"

    ' en juego (1)

    '-- TICKET IN
    _sb.AppendLine("SELECT   " & _str_since & "       AS SINCE                                                                              ")
    _sb.AppendLine("  	   , " & _str_until & "       AS UNTIL                                                                              ")
    _sb.AppendLine("  	   , SUM(T.AmountRedeem)		  AS AmountRedeem																			                                  ")
    _sb.AppendLine("  	   , SUM(T.AmountNoredeem)		AS AmountNoredeem																		                                  ")
    _sb.AppendLine("	     , SUM(T.CirculantRedeem)		AS CirculantRedeem																		                                ")
    _sb.AppendLine("	     , SUM(T.CirculantNoredeem)	AS CirculantNoredeem																	                                ")
    _sb.AppendLine("	     , T.AmountBn					      AS AmountBn																				                                    ")
    _sb.AppendLine("  	   , SUM(T.CountBn)				    AS CountBn																			                              	      ")
    _sb.AppendLine("  FROM (																														                                                    ")
    _sb.AppendLine("  		  SELECT                                                                                                          ")
    _sb.AppendLine("				          CASE WHEN TI.TI_TYPE_ID IN (" & _type_cashable & ", " & _type_redeem & ", " & _type_handpay & ", " & _type_jackpot & ") THEN TI.TI_AMOUNT ELSE 0 END AS AmountRedeem ")
    _sb.AppendLine("			  	      , CASE WHEN (TI.TI_TYPE_ID = " & _type_no_redeem & ") THEN TI.TI_AMOUNT ELSE 0 END AS AmountNoredeem		")
    _sb.AppendLine("			  	      , '0' CirculantRedeem																						                                        ")
    _sb.AppendLine("			  	      , '0' CirculantNoredeem																						                                      ")
    _sb.AppendLine("			  	      , '0' AmountBn																							                                            ")
    _sb.AppendLine("			  	      , '0' CountBn	    																						                                          ")
    _sb.AppendLine("			  	      , MC.MC_DATETIME DateDay  			            				    										                            ")

    _sb.AppendLine("			    FROM	  TICKETS			       TI																						                                      ")
    _sb.AppendLine("		 LEFT JOIN    TERMINALS		       TE ON (TE.TE_TERMINAL_ID         = TI.TI_LAST_ACTION_TERMINAL_ID)     							")
    _sb.AppendLine("		 LEFT JOIN    MONEY_COLLECTIONS  MC ON (TI.TI_MONEY_COLLECTION_ID = MC.MC_COLLECTION_ID)									          ")
    _sb.AppendLine("         WHERE    (MC.MC_STATUS = " & CShort(TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED).ToString() & "               ")
    _sb.AppendLine("                OR MC.MC_STATUS = " & CShort(TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE).ToString() & ")          ")
    _sb.AppendLine("			     AND	TI.TI_COLLECTED = 1																							                                        ")
    _sb.AppendLine("			     AND    " & GetSelectedTerminals("MC.MC_TERMINAL_ID"))

    If uc_dates.FromDateSelected OrElse uc_dates.ToDateSelected Then
      _sb.AppendLine("         AND   " & uc_dates.GetSqlFilterCondition("MC.MC_DATETIME"))
    End If

    '-- TICKET OUT
    _sb.AppendLine("		 UNION ALL	  																											                                                ")

    _sb.AppendLine("		    SELECT    '0' AmountRedeem																							                                        ")
    _sb.AppendLine("				        , '0' AmountNoredeem																							                                      ")
    _sb.AppendLine("				        , CASE WHEN TI.TI_TYPE_ID IN (" & _type_cashable & ", " & _type_redeem & ", " & _type_handpay & ", " & _type_jackpot & ") THEN TI.TI_AMOUNT ELSE 0 END AS CirculantRedeem")
    _sb.AppendLine("                , CASE WHEN (TI.TI_TYPE_ID = " & _type_no_redeem & ") THEN TI.TI_AMOUNT ELSE 0 END AS CirculantNoredeem ")
    _sb.AppendLine("				        , '0' AmountBn																							                                            ")
    _sb.AppendLine("				        , '0' CountBn	  																							                                          ")
    _sb.AppendLine("			  	      , TI.TI_CREATED_DATETIME DateDay       							    										                            ")

    _sb.AppendLine("			    FROM	  TICKETS			TI                                                                                        ")
    _sb.AppendLine("		 LEFT JOIN    TERMINALS   TE ON (TE.TE_TERMINAL_ID = TI.TI_CREATED_TERMINAL_ID)                                     ")
    _sb.AppendLine("		     WHERE    TI.TI_CREATED_TERMINAL_TYPE = 1																			                                  ")
    _sb.AppendLine("			     AND    " & GetSelectedTerminals("TE.TE_TERMINAL_ID"))

    If uc_dates.FromDateSelected OrElse uc_dates.ToDateSelected Then
      _sb.AppendLine("         AND    " & uc_dates.GetSqlFilterCondition("TI.TI_CREATED_DATETIME"))
    End If


    _sb.AppendLine("		 UNION ALL																												                                                  ")

    '-- BILLETES
    _sb.AppendLine("		    SELECT    '0' AmountRedeem																							                                        ")
    _sb.AppendLine("				        , '0' AmountNoredeem																							                                      ")
    _sb.AppendLine("				        , '0' CirculantRedeem																                                                    ")
    _sb.AppendLine("				        , '0' CirculantNoredeem																                                                  ")
    _sb.AppendLine("				        , MCD.MCD_FACE_VALUE  AmountBn														                                              ")
    _sb.AppendLine("				        , MCD.MCD_NUM_COLLECTED CountBn														                                              ")
    _sb.AppendLine("				        , MC.MC_DATETIME DateDay                               		                                              ")

    _sb.AppendLine("			    FROM	  MONEY_COLLECTION_DETAILS  MCD														                                              ")
    _sb.AppendLine("		 LEFT JOIN    MONEY_COLLECTIONS	         MC ON (MCD.MCD_COLLECTION_ID = MC.MC_COLLECTION_ID)			                  ")
    _sb.AppendLine("		 LEFT JOIN    TERMINALS			             TE ON ( TE.TE_TERMINAL_ID	  = MC.MC_TERMINAL_ID)			                    ")

    _sb.AppendLine("          WHERE   " & GetSelectedTerminals("TE.TE_TERMINAL_ID"))

    If uc_dates.FromDateSelected OrElse uc_dates.ToDateSelected Then
      _sb.AppendLine("            AND   " & uc_dates.GetSqlFilterCondition("MC.MC_DATETIME"))
    End If

    _sb.AppendLine("    ) AS  T																													                                                     ")
    _sb.AppendLine("GROUP BY  T.AmountBn, DATEDIFF(HOUR,'" & _str_date_time & "', T.DateDay) / 24                                            ")

    Return _sb.ToString()

  End Function

  Protected Overrides Sub GUI_BeforeFirstRow()

    If m_dates Is Nothing Then
      m_dates = New Dictionary(Of Date, Double())
    Else
      m_dates.Clear()
    End If

    m_prev_terminal_type = -1

    m_total_dates = Nothing

  End Sub ' GUI_BeforeFirstRow

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _idx_col As Integer
    Dim _value As Object
    Dim _date_since As Date
    Dim _date_until As Date
    Dim _idx_row As Integer
    Dim _idx_bill_row As Integer
    Dim _is_clear_row As Boolean
    Dim _sum_by_date(GRID_STATIC_COLUMNS_COUNT) As Double

    Call SumValuesInArray(DbRow)  ' se acumulan los Totales

    ' se averigua si la fecha ya existe en el grid, entonces se acumula en las columnas
    _is_clear_row = True
    _date_since = DbRow.Value(SQL_COLUMN_DATE_SINCE)
    _date_until = DbRow.Value(SQL_COLUMN_DATE_UNTIL)

    If Not m_dates.ContainsKey(_date_since) Then
      _idx_row = RowIndex
      _sum_by_date(GRID_COLUMN_SELECT) = _idx_row
      m_dates.Add(_date_since, _sum_by_date)

      _idx_bill_row = 0
      For _idx_col = GRID_COLUMN_FIRST_BILL To (m_grid_total_columns - 1)
        Me.Grid.Cell(RowIndex, _idx_col).Value = 0
        _idx_bill_row += 1
      Next
    Else
      _sum_by_date = m_dates(_date_since)
      _idx_row = _sum_by_date(GRID_COLUMN_SELECT)
      _is_clear_row = False
    End If

    ' por fin se escriben los datos de la fila
    For _idx_col = GRID_COLUMN_SELECT To GRID_COLUMN_TICKET_NOREDIM
      _value = DbRow.Value(_idx_col)
      If _idx_col >= GRID_COLUMN_TERM_REDIM Then
        _value += _sum_by_date(_idx_col)
        _sum_by_date(_idx_col) = _value
      End If

      If _idx_col > GRID_COLUMN_DATE_UNTIL Then
        Me.Grid.Cell(_idx_row, _idx_col).Value = GetFormatedValue(_idx_col, _value)
      End If
    Next

    ' Set date values
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_SINCE).Value = GUI_FormatDate(_date_since, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_UNTIL).Value = GUI_FormatDate(_date_until, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.Grid.Cell(_idx_row, GRID_COLUMN_BILLS_COUNT).Value = GUI_FormatNumber(_sum_by_date(GRID_COLUMN_BILLS_COUNT), 0)
    _value = DbRow.Value(SQL_COLUMN_BILLS_TOTAL)
    _sum_by_date(GRID_COLUMN_BILLS_TOTAL) += _value * DbRow.Value(SQL_COLUMN_BILLS_COUNT)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_BILLS_TOTAL).Value = GUI_FormatCurrency(_sum_by_date(GRID_COLUMN_BILLS_TOTAL))

    _value = DbRow.Value(SQL_COLUMN_BILLS_COUNT)
    _sum_by_date(GRID_COLUMN_BILLS_COUNT) += _value
    Me.Grid.Cell(_idx_row, GRID_COLUMN_BILLS_COUNT).Value = GUI_FormatNumber(_sum_by_date(GRID_COLUMN_BILLS_COUNT), 0)
    _value = DbRow.Value(SQL_COLUMN_BILLS_TOTAL)
    _sum_by_date(GRID_COLUMN_BILLS_TOTAL) += _value * DbRow.Value(SQL_COLUMN_BILLS_COUNT)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_BILLS_TOTAL).Value = GUI_FormatCurrency(_sum_by_date(GRID_COLUMN_BILLS_TOTAL))

    If DbRow.Value(SQL_COLUMN_BILLS_COUNT) <> 0 And m_bill_index.ContainsKey(GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_BILLS_TOTAL))) Then
      _idx_col = m_bill_index(GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_BILLS_TOTAL)))
      Me.Grid.Cell(_idx_row, _idx_col).Value = DbRow.Value(SQL_COLUMN_BILLS_COUNT)
    End If


    Return _is_clear_row

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_AfterLastRow()

    Dim _idx_row As Integer

    ' Add empty rows
    If Me.cb_rows.Checked = False AndAlso Me.Grid.NumRows > 0 Then
      AddDaysGrid()
      Me.Grid.SortGrid(GRID_COLUMN_DATE_SINCE)
    End If

    ' se a�ade fila de Totales
    Me.Grid.AddRow()
    _idx_row = Me.Grid.NumRows - 1

    Call SumToGrid(_idx_row)

  End Sub ' GUI_AfterLastRow

  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim _terminal As String
    Dim _date_temp As String
    Dim _selected_cashiers As String
    '  Dim _idx_row As Integer
    Dim _checked_counter As Integer

    m_filter_params.Clear()

    If cb_rows.Checked = True Then
      m_rows = GLB_NLS_GUI_INVOICING.GetString(479)
    Else
      m_rows = GLB_NLS_GUI_INVOICING.GetString(480)
    End If

    ' Dates
    _date_temp = IIf(uc_dates.FromDateSelected, mdl_tito.GetFormatedDate(uc_dates.FromDate.AddHours(uc_dates.ClosingTime)), "")
    Call AddFilterItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(297), _date_temp)
    _date_temp = IIf(uc_dates.ToDateSelected, mdl_tito.GetFormatedDate(uc_dates.ToDate.AddHours(uc_dates.ClosingTime)), "")
    Call AddFilterItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(298), _date_temp)
    Call AddFilterItem(cb_rows.Text, m_rows)
    Call AddFilterItem("", "")

    ' Cashiers
    _checked_counter = 0
    _selected_cashiers = ""

    Call AddFilterItem("", "")
    Call AddFilterItem("", "")
    Call AddFilterItem("", "")
    Call AddFilterItem("", "")

    ' Terminals 
    If Me.uc_pr_list.opt_all_types.Checked Then
      _terminal = Me.uc_pr_list.opt_all_types.Text
    ElseIf Me.uc_pr_list.opt_several_types.Checked Then
      _terminal = Me.uc_pr_list.opt_several_types.Text
    Else
      _terminal = Me.uc_pr_list.cmb_terminal.TextValue
    End If

    Call AddFilterItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(592), _terminal)

  End Sub ' GUI_ReportUpdateFilters

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    Dim _text As String
    Dim _keys() As Integer
    Dim _values() As String

    ReDim _keys(m_filter_params.Count - 1)

    m_filter_params.Keys.CopyTo(_keys, 0)
    Array.Sort(_keys)

    For Each _key As Integer In _keys
      _text = m_filter_params(_key)
      _values = _text.Split(";")

      If String.IsNullOrEmpty(_values(0)) Then
        PrintData.SetFilter("", "", True)
      Else
        PrintData.SetFilter(_values(0), _values(1))
      End If
    Next

    PrintData.FilterHeaderWidth(1) = 2000
    PrintData.FilterHeaderWidth(2) = 2500
    PrintData.FilterHeaderWidth(3) = 2500
    PrintData.FilterValueWidth(2) = 2100
    PrintData.FilterValueWidth(4) = 2100

  End Sub ' GUI_ReportFilter

#End Region ' OVERRIDES

#Region " Public functions and methods "

  Public Overloads Sub Show(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent

    Call GUI_Init()
    Call Show()

  End Sub ' Show

#End Region ' Public

#Region " Events "

#End Region ' Events

End Class