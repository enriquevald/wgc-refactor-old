'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_tito_stacker_edit
' DESCRIPTION:   
' AUTHOR:        David Rigal
' CREATION DATE: 08-Nov-2013
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 08-NOV-2013 DRV    First Release
' 19-NOV-2013 DRV    Disabled insertion terminal selection
' 02-DEC-2013 AMF    Reorder form
' 18-DEC-2013 ACM    Fixed Bug #WIGOSTITO-864. Set only Sas-Host terminals to terminals filter control.
' 19-DEC-2013 DRV    Limited the stacker id entry field length
' 30-DEC-2013 JRM    Fixed Bug WIGOSTITO-919: Auditing error
' 09-JAN-2014 DRV    Remove Inserted terminal and added unassign stacker button
' 12-MAY-2015 YNM    Fixed Bug WIG-2282: With GP Terminal.UseStackerID = 1, It was allowing to change stacker id = 0. 
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common.Misc
Imports System.Data.SqlClient
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common.Users
Imports System.Text
Imports WSI.Common

Public Class frm_tito_stacker_edit
  Inherits GUI_Controls.frm_base_edit

#Region " Members "

  Private m_terminal_types() As Integer
  Private m_inserted_terminal As Integer

#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_STACKERS_EDIT
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2179) 'Edici�n de Stacker
    ef_stacker_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2180) 'Identificador
    ef_model.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2181) 'Modelo"
    chk_deactivated.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3358) 'Disable
    lbl_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2992) & ":" 'Terminal

    lbl_notes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2188) 'Notas

    chk_select_pre_term.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3340)

    ' Delete button is only available when editing, not while creating
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    uc_unassign_terminal.Type = uc_button.ENUM_BUTTON_TYPE.BIG
    uc_unassign_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4439) 'Unassign Stacker

    ef_stacker_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 7)
    ef_model.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    gb_collection.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2121)
    lbl_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262) & ":"

    m_terminal_types = Nothing
    'ReDim Preserve m_terminal_types(0 To 0)
    'm_terminal_types(0) = TerminalTypes.SAS_HOST

    m_terminal_types = AcceptorTerminalTypeList()

    uc_preassigned_term.Init(m_terminal_types, uc_provider.UC_FILTER_TYPE.ONLY_ONE_TERMINAL)
    uc_preassigned_term.SetGroupBoxTitle(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2165))

    chk_select_pre_term.Checked = True

    ' Activate/Show controls
    uc_unassign_terminal.Enabled = False
    ef_stacker_id.Enabled = ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

  End Sub 'GUI_InitControls

  ' PURPOSE: It sets the read values from the database to the entry fields
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  'Note: If DbReadObject has some properties = -1, it means that there were errors reading the database
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _stacker As CLASS_STACKER
    Dim _terminals(0) As Long
    Dim _terminal_name As String
    Dim _status_collection As String

    _stacker = DbReadObject

    m_inserted_terminal = _stacker.InsertedTerminal
    ef_stacker_id.Value = IIf(_stacker.StackerId = 0, String.Empty, _stacker.StackerId)
    ef_model.Value = _stacker.Model
    txt_notes.Text = _stacker.Notes
    If _stacker.StatusCollection = TITO_MONEY_COLLECTION_STATUS.OPEN And Permissions.Execute _
             And Not GeneralParam.GetBoolean("WCP", "Terminal.UseStackerId") Then
      uc_unassign_terminal.Enabled = True
    End If

    If _stacker.PreassignedTerminal <> -1 And _stacker.PreassignedTerminal <> 0 Then
      chk_select_pre_term.Checked = True
      _terminals(0) = _stacker.PreassignedTerminal
      uc_preassigned_term.SetTerminalIdListSelected(_terminals)
      uc_preassigned_term.ExpandSelectedTerminal()
    Else
      chk_select_pre_term.Checked = False
    End If

    If _stacker.InsertedTerminal = -1 And _stacker.InsertedTerminal = 0 Then
      lbl_terminal_collection.Text = ""
    End If

    _terminal_name = mdl_tito.GetTerminalNameFromMoneyCollection(_stacker.StackerId)
    _status_collection = mdl_tito.CollectionStatusAsString(_stacker.StatusCollection)

    lbl_terminal_collection.Text = _terminal_name
    lbl_status_collection.Text = _status_collection

    RefreshScreenByStatus(_stacker.Status, False)

    EnableDeactivatedCheckBox(String.IsNullOrEmpty(_terminal_name), String.IsNullOrEmpty(_status_collection))

  End Sub ' GUI_SetScreenData

  ' PURPOSE: It gets the values from the entry field and set them to a TITO_CONFIGURATION object
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()
    Dim _stacker As CLASS_STACKER
    Dim _terminals() As Long

    _stacker = DbEditedObject

    _stacker.PreassignedTerminal = 0
    _stacker.InsertedTerminal = m_inserted_terminal
    _stacker.StackerId = 0
    _stacker.Notes = Nothing
    _stacker.Model = Nothing

    If String.IsNullOrEmpty(ef_stacker_id.Value) OrElse Not ef_stacker_id.ValidateFormat() OrElse Int64.Parse(ef_stacker_id.Value) <= 0 Then
      _stacker.StackerId = 0
    Else
      _stacker.StackerId = GUI_FormatNumber(ef_stacker_id.Value)
    End If

    If Not String.IsNullOrEmpty(ef_model.Value) Then
      _stacker.Model = ef_model.Value
    End If

    If Not String.IsNullOrEmpty(txt_notes.Text) Then
      _stacker.Notes = txt_notes.Text
    Else
      _stacker.Notes = ""
    End If

    If chk_select_pre_term.Checked Then
      _terminals = uc_preassigned_term.GetTerminalIdListSelected()
      If uc_preassigned_term.TerminalListHasValues Then
        If _terminals.Length > 0 Then
          _stacker.PreassignedTerminal = mdl_tito.GetSmartTerminal(mdl_tito.GetTerminalName(_terminals(0))).terminal_id
        End If
      End If
    End If

    If chk_deactivated.Checked Then
      If chk_deactivated.Enabled Then
        _stacker.Status = TITO_STACKER_STATUS.DEACTIVATED
      Else
        _stacker.Status = TITO_STACKER_STATUS.DISABLED
      End If
    Else
      _stacker.Status = TITO_STACKER_STATUS.ACTIVE
    End If

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Database overridable operations.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)
    Dim _stacker As CLASS_STACKER
    Dim _stacker_read As CLASS_STACKER
    Dim _stacker_edit As CLASS_STACKER

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_STACKER()
        _stacker = DbEditedObject
        _stacker.StackerId = 0
        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2853), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE _
         , ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT
        _stacker_read = DbReadObject
        _stacker_edit = DbEditedObject

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        Call MyBase.GUI_DB_Operation(DbOperation)

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE _
         , ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT

        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          _stacker_edit = DbEditedObject
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2605), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          ef_stacker_id.Text, " '" & _stacker_edit.StackerId & "'")
        End If
      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: It checks if the entry data are ok calling the function CheckEntryData
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _pre_terminal() As Long
    Dim _stacker As CLASS_STACKER
    Dim _preasig_terminal As SmartTerminal

    _stacker = DbReadObject
    _pre_terminal = uc_preassigned_term.GetTerminalIdListSelected()
    _preasig_terminal = Nothing
    If uc_preassigned_term.TerminalListHasValues Then
      _preasig_terminal = mdl_tito.GetSmartTerminal(mdl_tito.GetTerminalName(_pre_terminal(0)))
    End If

    ' Allways allow deactivate option
    If chk_deactivated.Checked Then

      Return True
    End If

    ' Stacker Id not valid
    If ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      If String.IsNullOrEmpty(ef_stacker_id.Value) OrElse Not ef_stacker_id.ValidateFormat() OrElse Int64.Parse(ef_stacker_id.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2196), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2180))
        Call ef_stacker_id.Focus()

        Return False
      End If
    End If

    ' Empty Model
    If String.IsNullOrEmpty(ef_model.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2197), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_model.Value)
      Call ef_model.Focus()

      Return False
    End If

    ' If select preassigned terminal is checked, a terminal must be selected on the user control
    If chk_select_pre_term.Checked And _pre_terminal Is Nothing Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2902), ENUM_MB_TYPE.MB_TYPE_WARNING, , , )
      Call uc_preassigned_term.Focus()

      Return False
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE: Controls the form button click events
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_DELETE
        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2198), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_YES Then
          chk_deactivated.Checked = True
          chk_deactivated.Enabled = False
          Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_OK)
        End If

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick
#End Region ' Overrides

#Region " Private Functions "
  ' PURPOSE: Enable or disable controls for each kind of status
  '         
  ' PARAMS:
  '    - INPUT: Status, IgnoreDeactivatedCheck
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub EnableDeactivatedCheckBox(ByVal TerminalNameEmpty As Boolean, ByVal StatusCollectionEmpty As Boolean)

    If ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT _
      And TerminalNameEmpty _
        And StatusCollectionEmpty Then
      chk_deactivated.Enabled = True
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = True
    Else
      chk_deactivated.Enabled = False
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    End If

  End Sub

  ' PURPOSE: Enable or disable controls for each kind of status
  '         
  ' PARAMS:
  '    - INPUT: Status, IgnoreDeactivatedCheck
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub RefreshScreenByStatus(ByVal Status As TITO_STACKER_STATUS, ByVal IgnoreDeactivatedCheck As Boolean)

    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = (ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT And _
                                                     Status = TITO_STACKER_STATUS.ACTIVE) And Me.Permissions.Delete

    chk_select_pre_term.Enabled = True

    If Status = TITO_STACKER_STATUS.DISABLED Then
      chk_select_pre_term.Enabled = False
      chk_deactivated.Enabled = False
      ef_model.Enabled = False
      txt_notes.Enabled = False
      chk_deactivated.Checked = True

      Return
    End If

    If Status = TITO_STACKER_STATUS.DEACTIVATED Then
      chk_select_pre_term.Checked = False
      chk_select_pre_term.Enabled = False
      chk_deactivated.Enabled = True
      If Not IgnoreDeactivatedCheck Then
        chk_deactivated.Checked = True
      End If

      Return
    End If

    'chk_deactivated.Enabled = (ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT And _
    '                           Status <= TITO_STACKER_STATUS.ACTIVE)

    chk_deactivated.Enabled = Me.Permissions.Execute

  End Sub ' RefreshScreenByStatus

#End Region ' Private functions

#Region " Events "

  Private Sub chk_select_pre_term_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_select_pre_term.CheckedChanged
    If chk_select_pre_term.Checked Then
      uc_preassigned_term.Enabled = True
    Else
      uc_preassigned_term.Enabled = False
      uc_preassigned_term.Init(m_terminal_types, uc_provider.UC_FILTER_TYPE.ONLY_ONE_TERMINAL)
      uc_preassigned_term.SetGroupBoxTitle(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2165))
    End If
  End Sub ' chk_select_pre_term_CheckedChanged

  Private Sub chk_deactivated_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_deactivated.CheckedChanged
    If chk_deactivated.Checked Then
      RefreshScreenByStatus(TITO_STACKER_STATUS.DEACTIVATED, False)
    Else
      RefreshScreenByStatus(TITO_STACKER_STATUS.ACTIVE, False)
    End If
  End Sub ' chk_deactivated_CheckedChanged

  Private Sub uc_unassign_terminal_ClickEvent() Handles uc_unassign_terminal.ClickEvent
    m_inserted_terminal = 0
    uc_unassign_terminal.Enabled = False
  End Sub

#End Region ' Events

End Class
