'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_tito_params_edit
' DESCRIPTION:   
' AUTHOR:        Francesc Borrell
' CREATION DATE: 03-Jul-2013
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 03-JUL-2013 FBA    First Release
' 20-AUG-2013 JRM    Removed combobox on change event.
' 23-AUG-2013 JRM    Code revision
' 25-SEP-2013 NMR    Renamed general TITO Params for Redeem control
' 14-OCT-2013 DRV    Fixed errors getting and setting data
' 17-OCT-2013 DRV    Deleted emission, redemption and expiration properties for each ticket and added general checkboxes, also added permissions
' 25-OCT-2013 DRV    Deleted ticket configuration for different tickets and make it unique for all tickets
' 31-DEC-2013 JRM    Fixed bug WIGOSTITO-776, form forced used to enter redeem limit even when redem option was not selected
' 31-DEC-2013 JRM    Fixed bug WIGOSTITO-942, Wrong form header name
' 07-JAN-2014 ICS    Added the stacker capacity entry field
' 14-JAN-2014 DRV    Fixed Bug WIGOSTITO-776, form forced used to enter redeem limit even when redem option was not selected
' 10-FEB-2014 ICS    Added the jackpot limit before prize entry field
' 17-SEP-2015 FAV    Fixed bug TFS 4532: Tito ticket configuration, max values (ticket in / ticket out) should accept 0.
' 17-OCT-2016 PDM    PBI 18245:eBox - Ticket Amount Not Multiple of Machine Denomination: GP - monto m�nimo aceptado por la m�quina (GUI y BD)
' 26-OCT-2016  FAV   PBI 19597:eBox - Ticket Amount Not Multiple of Machine Denomination: Review
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common.Misc
Imports System.Data.SqlClient
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common.Users
Imports System.Text
Imports WSI.Common

Public Class frm_tito_params_edit
  Inherits frm_base_edit

#Region "Constants"

  'ID combos
  Private Const CMB_ID_NOTHING As Integer = 0
  Private Const CMB_ID_TOTALS As Integer = 1
  Private Const CMB_ID_DETAILS As Integer = 2

#End Region ' Constants

#Region " Overrides"

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TITO_PARAMS_EDIT
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    MyBase.GUI_InitControls()

    'setting the Window Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4428)

    'Labels
    Me.gb_prizes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2522)

    'Entry fields -> text
    Me.gb_income.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2312)
    Me.gb_tickets.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2327)
    Me.gb_appearance.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2866)
    Me.gb_expiration_dates.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4342)
    Me.ef_max_allowed_ATM_reedem.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2293)
    Me.ef_max_allowed_creation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2864)
    Me.lbl_max_creation_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2865)
    Me.ef_shown_numbers.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2873)
    Me.ef_shown_numbers.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2874)
    Me.ef_tickets_limit_before_prize.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4347)
    Me.ef_jackpots_limit_before_prize.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4382)
    Me.ef_stacker_capacity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4438)

    Me.ef_min_allowed_ticket_in.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7676)
    Me.chk_allow_truncate.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7677)
    Me.lbl_allow_truncate_info.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7678)
    Me.ef_min_denomination_multiple.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7679)

    If WSI.Common.GamingTableBusinessLogic.IsGamingTablesEnabled Then
      Me.lbl_also_applies_to_chips.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3016)
      Me.lbl_also_applies_to_chips.Visible = True
      Me.ef_tickets_limit_before_prize.Text &= "*"
    End If

    ' Entry fields of ticket personalization
    Me.gb_ticket_personalization.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2319) ' Personalizaci�n Ticket
    Me.ef_promo_redim_ticket_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2320, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3032)) ' Promotional Redeemable Ticket Title
    Me.ef_debit_ticket_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2320, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2325)) ' Debit Ticket Title
    Me.ef_cashable_ticket_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2320, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3033)) ' Cashable ticket title
    Me.ef_promo_non_redim_ticket_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2320, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2326)) 'Promotional Non-Redeemable Ticket Title
    Me.ef_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2321) ' localizacion 
    Me.ef_address_1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2322) ' direcci�n 1
    Me.ef_address_2.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2323) ' direcci�n 2

    'Entry Field text visibles 
    Me.ef_promo_redim_ticket_title.TextVisible = True
    Me.ef_debit_ticket_title.TextVisible = True
    Me.ef_promo_non_redim_ticket_title.TextVisible = True
    Me.ef_cashable_ticket_title.TextVisible = True
    Me.ef_location.TextVisible = True
    Me.ef_address_1.TextVisible = True
    Me.ef_address_2.TextVisible = True

    ' Combos
    Me.cmb_detail_collection_level.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2298)
    Me.cmb_detail_collection_level.IsReadOnly = False

    ' Checkboxes
    Me.chk_ticket_collection_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2481)
    Me.chk_allow_emission.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2717)
    Me.chk_cashable_redemption.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2718) & "*"
    Me.chk_promotional_redemption.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2719) & "*"
    Me.chk_ticket_collection_cashier.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4346)

    ' Expiration days
    Me.ef_cashable_ticket_expiration_days.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4343)
    Me.ef_ticket_promotional_nonredeemable_expiration_days.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4345)
    Me.ef_ticket_promotional_redeemable_expiration_days.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4344)
    Me.ef_cashable_ticket_expiration_days.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    Me.ef_ticket_promotional_nonredeemable_expiration_days.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    Me.ef_ticket_promotional_redeemable_expiration_days.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)

    ' Entry Fields: lenght
    Me.ef_max_allowed_ATM_reedem.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9)
    Me.ef_max_allowed_creation.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9)
    Me.ef_min_allowed_ticket_in.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9)
    Me.ef_min_denomination_multiple.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9)
    Me.ef_shown_numbers.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2)
    Me.ef_tickets_limit_before_prize.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9)
    Me.ef_jackpots_limit_before_prize.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 9)
    Me.ef_stacker_capacity.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 9)

    ' Entry fields: lenght TICKET PERSONALIZATION
    Me.ef_location.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 40)
    Me.ef_address_1.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 40)
    Me.ef_address_2.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 40)
    Me.ef_debit_ticket_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 16)
    Me.ef_promo_redim_ticket_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 16)
    Me.ef_promo_non_redim_ticket_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 16)
    Me.ef_cashable_ticket_title.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 16)

    'Execute Permissions
    If Not Permissions.Execute Then
      gb_income.Enabled = False
      cmb_detail_collection_level.Enabled = False
      chk_ticket_collection_terminal.Enabled = False
    End If

    'Write Permissions
    If Not Permissions.Write Then
      Me.gb_income.Enabled = False
      Me.gb_tickets.Enabled = False
      Me.gb_appearance.Enabled = False
      Me.gb_expiration_dates.Enabled = False

      Me.ef_max_allowed_ATM_reedem.Enabled = False
      Me.ef_min_allowed_ticket_in.Enabled = False
      Me.ef_min_denomination_multiple.Enabled = False
      Me.ef_max_allowed_creation.Enabled = False
      Me.ef_debit_ticket_title.Enabled = False
      Me.ef_promo_redim_ticket_title.Enabled = False
      Me.ef_cashable_ticket_title.Enabled = False
      Me.ef_promo_non_redim_ticket_title.Enabled = False
      Me.ef_location.Enabled = False
      Me.ef_address_1.Enabled = False
      Me.ef_address_2.Enabled = False

      Me.cmb_detail_collection_level.IsReadOnly = True
      Me.chk_allow_emission.Enabled = False
      Me.chk_cashable_redemption.Enabled = False
      Me.chk_promotional_redemption.Enabled = False
      Me.chk_allow_truncate.Enabled = False
      Me.chk_hide_validation_number.Enabled = False
      Me.ef_shown_numbers.Enabled = False

      Me.ef_stacker_capacity.Enabled = False

    End If

    'Fill combos
    Call FillCombos()

    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

  End Sub 'GUI_InitControls

  ' PURPOSE: It sets the read values from the database to the entry fields
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  'Note: If DbReadObject has some properties = -1, it means that there were errors reading the database
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _tito_params As CLASS_TITO_PARAMS

    _tito_params = DbReadObject

    Me.ef_max_allowed_ATM_reedem.Value = _tito_params.MaxAllowedRedeem
    Me.ef_max_allowed_creation.Value = _tito_params.MaxAllowedCreation
    Me.chk_ticket_collection_terminal.Checked = _tito_params.TicketCollection

    Me.ef_min_allowed_ticket_in.Value = _tito_params.MinAllowedTicketIn
    Me.chk_allow_truncate.Checked = _tito_params.AllowTrucate
    Me.ef_min_denomination_multiple.Value = _tito_params.MinDenominationMultiple

    If Not Integer.TryParse(_tito_params.DetailCollectionLevel, Me.cmb_detail_collection_level.Value) Then
      Me.cmb_detail_collection_level.Value = 0
    End If

    chk_allow_emission.Checked = _tito_params.AllowTicketRedemption
    chk_cashable_redemption.Checked = _tito_params.AllowCashableCreation
    chk_promotional_redemption.Checked = _tito_params.AllowPromotionalCreation

    Me.ef_address_1.Value = _tito_params.TicketAddress1
    Me.ef_address_2.Value = _tito_params.TicketAddress2
    Me.ef_location.Value = _tito_params.TicketLocation
    Me.ef_promo_redim_ticket_title.Value = _tito_params.PromoRedimTicketTitle
    Me.ef_promo_non_redim_ticket_title.Value = _tito_params.PromoNonRedimTicketTitle
    Me.ef_cashable_ticket_title.Value = _tito_params.CashableTicketTitle
    Me.ef_debit_ticket_title.Value = _tito_params.DebitTicketTitle

    Me.chk_hide_validation_number.Checked = _tito_params.HideTicketNumber
    If Not chk_hide_validation_number.Checked Then
      Me.ef_shown_numbers.Enabled = False
    End If
    Me.ef_shown_numbers.Value = _tito_params.ShowedNumbers

    Me.ef_cashable_ticket_expiration_days.Value = _tito_params.CashableTicketExpirationDays
    Me.ef_ticket_promotional_nonredeemable_expiration_days.Value = _tito_params.PromotionalNonCashableExpirationDays
    Me.ef_ticket_promotional_redeemable_expiration_days.Value = _tito_params.PromotionalCashableExpirationDays
    Me.chk_ticket_collection_cashier.Checked = _tito_params.TicketAllowCollectionInCashier
    Me.ef_tickets_limit_before_prize.Value = _tito_params.TicketConsideredPrizeFrom
    Me.ef_jackpots_limit_before_prize.Value = _tito_params.JackpotConsideredPrizeFrom

    Me.ef_stacker_capacity.Value = _tito_params.StackerCapacity

  End Sub ' GUI_SetScreenData

  ' PURPOSE: It gets the values from the entry field and set them to a TITO_CONFIGURATION object
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim _tito_params As CLASS_TITO_PARAMS

    _tito_params = DbEditedObject
    _tito_params.MaxAllowedRedeem = GUI_ParseCurrency(Me.ef_max_allowed_ATM_reedem.Value)
    _tito_params.MaxAllowedCreation = GUI_ParseCurrency(Me.ef_max_allowed_creation.Value)

    _tito_params.AllowTrucate = Me.chk_allow_truncate.Checked
    _tito_params.MinAllowedTicketIn = GUI_ParseCurrency(Me.ef_min_allowed_ticket_in.Value)
    _tito_params.MinDenominationMultiple = GUI_ParseCurrency(Me.ef_min_denomination_multiple.Value)

    If Not Integer.TryParse(Me.cmb_detail_collection_level.Value, _tito_params.DetailCollectionLevel) Then
      _tito_params.DetailCollectionLevel = 0
    End If

    _tito_params.AllowTicketRedemption = Me.chk_allow_emission.Checked
    _tito_params.AllowPromotionalCreation = Me.chk_promotional_redemption.Checked
    _tito_params.AllowCashableCreation = Me.chk_cashable_redemption.Checked

    _tito_params.TicketLocation = Me.ef_location.Value
    _tito_params.TicketAddress1 = Me.ef_address_1.Value
    _tito_params.TicketAddress2 = Me.ef_address_2.Value
    _tito_params.DebitTicketTitle = Me.ef_debit_ticket_title.Value
    _tito_params.PromoRedimTicketTitle = Me.ef_promo_redim_ticket_title.Value
    _tito_params.PromoNonRedimTicketTitle = Me.ef_promo_non_redim_ticket_title.Value
    _tito_params.CashableTicketTitle = Me.ef_cashable_ticket_title.Value

    _tito_params.TicketCollection = Me.chk_ticket_collection_terminal.Checked

    _tito_params.HideTicketNumber = Me.chk_hide_validation_number.Checked
    _tito_params.ShowedNumbers = IIf(Me.chk_hide_validation_number.Checked, GUI_ParseNumber(Me.ef_shown_numbers.Value), 0)

    _tito_params.CashableTicketExpirationDays = GUI_ParseNumber(Me.ef_cashable_ticket_expiration_days.Value)
    _tito_params.PromotionalNonCashableExpirationDays = GUI_ParseNumber(Me.ef_ticket_promotional_nonredeemable_expiration_days.Value)
    _tito_params.PromotionalCashableExpirationDays = GUI_ParseNumber(Me.ef_ticket_promotional_redeemable_expiration_days.Value)
    _tito_params.TicketAllowCollectionInCashier = Me.chk_ticket_collection_cashier.Checked
    _tito_params.TicketConsideredPrizeFrom = GUI_ParseNumber(Me.ef_tickets_limit_before_prize.Value)
    _tito_params.JackpotConsideredPrizeFrom = GUI_ParseNumber(Me.ef_jackpots_limit_before_prize.Value)

    _tito_params.StackerCapacity = GUI_ParseNumber(Me.ef_stacker_capacity.Value)

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Database overridable operations.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)
    Dim _tito_params As CLASS_TITO_PARAMS

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_TITO_PARAMS()
        _tito_params = DbEditedObject

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        DbReadObject = DbEditedObject.Duplicate()

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2853), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE
        DbStatus = ENUM_STATUS.STATUS_OK

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        Call MyBase.GUI_DB_Operation(DbOperation)

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2854), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        End If
      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: It checks if the entry data are ok calling the function CheckEntryData
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _nls_param1 As String
    Dim _promotional_redeemable_ticket_expiration_days As Int32
    Dim _promotional_non_redeemable_ticket_expiration_days As Int32
    Dim _ticket_expiration_days As Int32

    _ticket_expiration_days = 0

    If GUI_ParseNumber(Me.ef_max_allowed_creation.Value) < 0 And chk_cashable_redemption.Checked Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2864)
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2951), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_max_allowed_creation.Focus()

      Return False
    End If

    If GUI_ParseNumber(Me.ef_max_allowed_ATM_reedem.Value) < 0 And chk_allow_emission.Checked Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2293)
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2951), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_max_allowed_ATM_reedem.Focus()

      Return False
    End If

    If Me.ef_shown_numbers.Enabled And GUI_ParseNumber(ef_shown_numbers.Value) < 0 Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2875)
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_shown_numbers.Focus()

      Return False
    End If

    'If Me.ef_shown_numbers.Enabled And GUI_ParseNumber(ef_shown_numbers.Value) <= 0 Then
    '  _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2874)
    '  Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
    '  Call ef_shown_numbers.Focus()

    '  Return False
    'End If

    If GUI_ParseNumber(ef_shown_numbers.Value) >= 18 Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2875)
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2879), ENUM_MB_TYPE.MB_TYPE_WARNING, , , "18", _nls_param1)
      Call ef_shown_numbers.Focus()

      Return False
    End If

    If Not Int32.TryParse(ef_cashable_ticket_expiration_days.Value, _ticket_expiration_days) OrElse _ticket_expiration_days <= 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_cashable_ticket_expiration_days.Text)
      Call ef_cashable_ticket_expiration_days.Focus()

      Return False
    End If

    If Not Int32.TryParse(ef_ticket_promotional_redeemable_expiration_days.Value, _promotional_redeemable_ticket_expiration_days) OrElse _
    (_promotional_redeemable_ticket_expiration_days <= 0 And chk_cashable_redemption.Checked) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_ticket_promotional_redeemable_expiration_days.Text)
      Call ef_ticket_promotional_redeemable_expiration_days.Focus()

      Return False
    End If

    If Not Int32.TryParse(ef_ticket_promotional_nonredeemable_expiration_days.Value, _promotional_non_redeemable_ticket_expiration_days) OrElse _
    (_promotional_non_redeemable_ticket_expiration_days <= 0 And chk_promotional_redemption.Checked) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_ticket_promotional_nonredeemable_expiration_days.Text)
      Call ef_ticket_promotional_nonredeemable_expiration_days.Focus()

      Return False
    End If


    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE:
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Return False

  End Function ' GUI_KeyPress

#End Region ' Overrides

#Region "Private Functions"

  ' PURPOSE: Fill the combos with their values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub FillCombos()

    'bill info (nothing/totals/details) 
    Me.cmb_detail_collection_level.Add(CMB_ID_NOTHING, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2305))
    Me.cmb_detail_collection_level.Add(CMB_ID_TOTALS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2306))
    Me.cmb_detail_collection_level.Add(CMB_ID_DETAILS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2307))

  End Sub 'FillCombos

  Private Function GetIntegerValue(ByVal Text As String) As Integer
    Dim _result As Integer

    If Not Integer.TryParse(Text, _result) Then
      _result = 0
    End If

    Return _result

  End Function

#End Region ' Private functions and methods

#Region "Events"

  Private Sub chk_hide_validation_number_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_hide_validation_number.CheckedChanged
    If chk_hide_validation_number.Checked Then
      Me.ef_shown_numbers.Enabled = True
    Else
      Me.ef_shown_numbers.Value = ""
      Me.ef_shown_numbers.Enabled = False
    End If
  End Sub

  Private Sub chk_allow_truncate_CheckedChanged(sender As Object, e As EventArgs) Handles chk_allow_truncate.CheckedChanged
    ef_min_denomination_multiple.Enabled = Not chk_allow_truncate.Checked
  End Sub

#End Region ' Events

End Class