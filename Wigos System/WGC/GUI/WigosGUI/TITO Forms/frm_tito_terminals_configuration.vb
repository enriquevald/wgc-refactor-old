'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_terminals_configuration
'
' DESCRIPTION:   This screen allows to view all terminals and it's configuration
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 10-OCT-2013  DRV    Initial Version
' 11-OCT-2013  DRV    Deleted the Validation Type grid column
' 14-OCT-2013  DRV    Added Allowed Bills column
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Imports WSI.Common
Imports System.Text

Public Class frm_tito_terminals_configuration
  Inherits GUI_Controls.frm_base_sel

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_ID As Integer = 0
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 1
  Private Const SQL_COLUMN_STATUS As Integer = 2
  Private Const SQL_COLUMN_VALIDATION As Integer = 3
  Private Const SQL_COLUMN_CASHABLE As Integer = 4
  Private Const SQL_COLUMN_PROMOTION As Integer = 5
  Private Const SQL_COLUMN_CREATION As Integer = 6
  Private Const SQL_COLUMN_BILLS As Integer = 7


  ' Grid Columns
  Private Const GRID_COLUMN_ID As Integer = 0
  Private Const GRID_COLUMN_TERMINAL_NAME As Integer = 1
  Private Const GRID_COLUMN_STATUS As Integer = 2
  Private Const GRID_COLUMN_CASHABLE As Integer = 3
  Private Const GRID_COLUMN_PROMOTION As Integer = 4
  Private Const GRID_COLUMN_CREATION As Integer = 5
  Private Const GRID_COLUMN_BILLS As Integer = 6

  ' Width
  Private Const GRID_WIDTH_ID As Integer = 0
  Private Const GRID_WIDTH_TERMINAL_NAME As Integer = 3000
  Private Const GRID_WIDTH_STATUS As Integer = 1700
  Private Const GRID_WIDTH_CASHABLE As Integer = 1400
  Private Const GRID_WIDTH_PROMOTION As Integer = 1400
  Private Const GRID_WIDTH_CREATION As Integer = 1400
  Private Const GRID_WIDTH_BILLS As Integer = 1400

  Private Const GRID_COLUMNS As Integer = 7
  Private Const GRID_HEADERS As Integer = 2

#End Region ' Constants

#Region " Enums "

#End Region ' Enums

#Region " Members "

  ' Used to avoid data-changed recursive calls when re-painting a data grid
  Private m_refreshing_grid As Boolean

  ' For report filters 
  Private m_providers As String
  Private m_status As String


#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Set a different value for the maximum number of rows that can be showed
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Function GUI_MaxRows() As Integer
    Return 10000
  End Function ' GUI_MaxRows

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()
    Me.FormId = 0 'ENUM_FORM.FORM_TERMINALS_CONFIGURATION

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' RRB 27-AUG-2012 Form name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2698)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(3)

    ' Status
    Me.gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1090)
    Me.chk_active.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(397)
    Me.chk_inactive.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(398)
    Me.chk_retired.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(399)

    ' Terminals
    Me.uc_terminals.Init(WSI.Common.Misc.GamingTerminalTypeList())

    GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = True

    ' Grid
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    Return True

  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_sql As StringBuilder
    _str_sql = New StringBuilder()

    ' Get Select and from
    _str_sql = New StringBuilder()
    _str_sql.AppendLine("   SELECT   TE_TERMINAL_ID                   AS ID         ")
    _str_sql.AppendLine("          , TE_NAME                          AS NAME       ")
    _str_sql.AppendLine("          , TE_STATUS                        AS STATUS     ")
    _str_sql.AppendLine("          , TE_VALIDATION_TYPE               AS TYPE       ")
    _str_sql.AppendLine("          , TE_ALLOWED_CASHABLE_REDEMPTION   AS CASHABLE   ")
    _str_sql.AppendLine("          , TE_ALLOWED_PROMO_REDEMPTION      AS REDEMPTION ")
    _str_sql.AppendLine("          , TE_ALLOWED_EMISSION              AS EMISSION   ")
    _str_sql.AppendLine("          , TE_ALLOWED_BILLS                 AS BILLS      ")
    _str_sql.AppendLine("     FROM   TERMINALS                                      ")

    ' RRB 27-AUG-2012

    _str_sql = _str_sql.AppendLine(GetSqlWhere())

    _str_sql.AppendLine(" ORDER BY   TE_NAME                                        ")

    Return _str_sql.ToString()

  End Function ' GUI_FilterGetSqlQuery

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    Call GUI_StyleSheet()

  End Sub ' GUI_BeforeFirstRow

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()


  End Sub ' GUI_AfterLastRow

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _aux_string As String = ""
    ' terminal ID
    Me.Grid.Cell(RowIndex, GRID_COLUMN_ID).Value = DbRow.Value(SQL_COLUMN_ID)

    ' terminal Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)

    ' Status
    Select Case DbRow.Value(SQL_COLUMN_STATUS)
      Case WSI.Common.TerminalStatus.ACTIVE
        _aux_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(397)
      Case WSI.Common.TerminalStatus.OUT_OF_SERVICE
        _aux_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(398)
      Case WSI.Common.TerminalStatus.RETIRED
        _aux_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(399)
    End Select

    Me.Grid.Cell(RowIndex, GRID_COLUMN_STATUS).Value = _aux_string


    ' Cashable Ticket
    _aux_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(316)
    If Not DbRow.Value(SQL_COLUMN_CASHABLE) Is DBNull.Value Then
      If DbRow.Value(SQL_COLUMN_CASHABLE) Then
        _aux_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(532)
      End If
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CASHABLE).Value = _aux_string

    ' Promotional Tickets
    _aux_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(316)
    If Not DbRow.Value(SQL_COLUMN_PROMOTION) Is DBNull.Value Then
      If DbRow.Value(SQL_COLUMN_PROMOTION) Then
        _aux_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(532)
      End If
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_PROMOTION).Value = _aux_string

    ' Promotional Tickets
    _aux_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(316)
    If Not DbRow.Value(SQL_COLUMN_CREATION) Is DBNull.Value Then
      If DbRow.Value(SQL_COLUMN_CREATION) Then
        _aux_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(532)
      End If
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CREATION).Value = _aux_string

    ' Bills
    _aux_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(316)
    If Not DbRow.Value(SQL_COLUMN_BILLS) Is DBNull.Value Then
      If DbRow.Value(SQL_COLUMN_BILLS) Then
        _aux_string = GLB_NLS_GUI_PLAYER_TRACKING.GetString(532)
      End If
    End If
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BILLS).Value = _aux_string

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Set focus to a control when first entering the form
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.chk_active
  End Sub ' GUI_SetInitialFocus

#Region " GUI Reports "

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter("", "")
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1090), m_status)
    PrintData.SetFilter("", "")
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(370), m_providers)
    PrintData.SetFilter("", "", True)


    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)


    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    '' Allow enough space for terminal type labels in the report
    'PrintData.FilterHeaderWidth(3) = 2000
  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set form specific requirements/parameters forthe report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '           - FirstColIndex
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    ' Prevent date/time column from being squeezed too much and drop the time part to the following line

    Call MyBase.GUI_ReportParams(PrintData)
    PrintData.Params.Title = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2698)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_status = ""
    m_providers = ""

    ' Status
    If Me.chk_active.Checked Then
      m_status = m_status & GLB_NLS_GUI_PLAYER_TRACKING.GetString(397) & ", "
    End If
    If Me.chk_inactive.Checked Then
      m_status = m_status & GLB_NLS_GUI_PLAYER_TRACKING.GetString(398) & ", "
    End If
    If Me.chk_retired.Checked Then
      m_status = m_status & GLB_NLS_GUI_PLAYER_TRACKING.GetString(399) & ", "
    End If
    If m_status <> "" Then
      m_status = m_status.Substring(0, m_status.Length - 2)
    End If
    ' Terminals
    If Me.uc_terminals.opt_all_types.Checked Then
      m_providers = Me.uc_terminals.opt_all_types.Text
    End If
    If Me.uc_terminals.opt_one_terminal.Checked Then
      m_providers = Me.uc_terminals.cmb_terminal.TextValue
    End If
    If Me.uc_terminals.opt_several_types.Checked Then
      m_providers = Me.uc_terminals.opt_several_types.Text
    End If

  End Sub ' GUI_ReportUpdateFilters

  ' PURPOSE : Activated when a row of the grid is double clicked or selected, so it can be edited
  '
  '  PARAMS :
  '     - INPUT :
  '               
  '     - OUTPUT :
  '          
  ' RETURNS :
  Protected Overrides Sub GUI_EditSelectedItem()
    Dim _idx_row As Int32
    Dim _terminal_configuration_id As Integer
    Dim _terminal_configuration_name As String
    Dim _frm_edit As Object

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the Promotion ID and Name, and launch the editor
    _terminal_configuration_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_ID).Value
    _terminal_configuration_name = Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL_NAME).Value

    _frm_edit = New frm_tito_terminals_configuration_edit()

    Call _frm_edit.ShowEditItem(_terminal_configuration_id, _terminal_configuration_name)

    _frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub

#End Region ' GUI Reports

#End Region ' Overrides

#Region " Public Functions "

  ' PURPOSE : Opens dialog with default settings for edit mode
  '
  '  PARAMS :
  '     - INPUT :

  '     - OUTPUT :

  '
  ' RETURNS :
  Public Overloads Sub Show(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent

    Call GUI_Init()
    'TODO:
    'Me.Display(False)
    Call Show()

  End Sub ' Show

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADERS)

      ' ID
      .Column(GRID_COLUMN_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2285) '2285 "ID"
      .Column(GRID_COLUMN_ID).Width = GRID_WIDTH_ID
      .Column(GRID_COLUMN_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal Name
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(0).Text = ""
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1097) '1097 "Terminal"
      .Column(GRID_COLUMN_TERMINAL_NAME).Width = GRID_WIDTH_TERMINAL_NAME
      .Column(GRID_COLUMN_TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal Status
      .Column(GRID_COLUMN_STATUS).Header(0).Text = ""
      .Column(GRID_COLUMN_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1090) '1090 "Estado"
      .Column(GRID_COLUMN_STATUS).Width = GRID_WIDTH_STATUS
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Cashable Tickets
      .Column(GRID_COLUMN_CASHABLE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2699) '2699 "Tickets"
      .Column(GRID_COLUMN_CASHABLE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2700) '2700 "Cashable"
      .Column(GRID_COLUMN_CASHABLE).Width = GRID_WIDTH_CASHABLE
      .Column(GRID_COLUMN_CASHABLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Promotional Tickets
      .Column(GRID_COLUMN_PROMOTION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2699) '2699 "Tickets"
      .Column(GRID_COLUMN_PROMOTION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2708) '2708 "Promocionales"
      .Column(GRID_COLUMN_PROMOTION).Width = GRID_WIDTH_PROMOTION
      .Column(GRID_COLUMN_PROMOTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Ticket Creation
      .Column(GRID_COLUMN_CREATION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2699) '2699 "Tickets"
      .Column(GRID_COLUMN_CREATION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2709) '2709 "Creaci�n"
      .Column(GRID_COLUMN_CREATION).Width = GRID_WIDTH_CREATION
      .Column(GRID_COLUMN_CREATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Bills
      .Column(GRID_COLUMN_BILLS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2740) ' "Allow"
      .Column(GRID_COLUMN_BILLS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2741) ' "Bills"
      .Column(GRID_COLUMN_BILLS).Width = GRID_WIDTH_BILLS
      .Column(GRID_COLUMN_BILLS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SetDefaultValues()

    Dim _closing_time As Integer
    Dim _initial_time As Date
    Dim _now As Date

    Me.uc_terminals.opt_all_types.Checked = True

    _now = WGDB.Now

    _closing_time = GetDefaultClosingTime()
    _initial_time = New DateTime(_now.Year, _now.Month, _now.Day, _closing_time, 0, 0)
    If _initial_time > _now Then
      _initial_time = _initial_time.AddDays(-1)
    End If

    Me.chk_active.Checked = True
    Me.chk_inactive.Checked = False
    Me.chk_retired.Checked = False


  End Sub ' SetDefaultValues

  ' PURPOSE: Build the variable part of the WHERE clause (the one that depends on filter values) for the
  '          main SQL Query.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Function GetSqlWhere() As String

    Dim _str_where As String = ""
    Dim _aux_str As String = ""

    If Me.uc_terminals.opt_one_terminal.Checked = True Then
      _str_where = _str_where & " AND TE_TERMINAL_ID = " & Me.uc_terminals.GetProviderIdListSelected()
    Else
      If Not Me.uc_terminals.opt_all_types.Checked Then
        _str_where = _str_where & " AND TE_TERMINAL_ID IN ( " & Me.uc_terminals.GetProviderIdListSelected() & " ) "
      End If
    End If

    If Not (chk_active.Checked = chk_inactive.Checked And chk_active.Checked = chk_retired.Checked) Then
      _str_where = _str_where & " AND TE_STATUS IN ( "
      If chk_active.Checked = True Then
        _str_where = _str_where & " 0,"
      End If

      If chk_inactive.Checked = True Then
        _str_where = _str_where & " 1,"
      End If

      If chk_retired.Checked = True Then
        _str_where = _str_where & " 2,"
      End If
      _str_where = _str_where.Substring(0, _str_where.Length - 1) & " ) "
    End If

    If _str_where <> "" Then
      _str_where = Strings.Right(_str_where, Len(_str_where) - 5)
      _str_where = " WHERE " & _str_where
    End If


    Return _str_where
  End Function ' GetSqlWhere

#End Region ' Private Functions

#Region " Events "

#End Region ' Events
End Class
