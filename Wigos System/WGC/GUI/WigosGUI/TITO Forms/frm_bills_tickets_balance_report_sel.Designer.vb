<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_bills_tickets_balance_report_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.lbl_cs_name = New GUI_Controls.uc_text_field
    Me.gb_status = New System.Windows.Forms.GroupBox
    Me.lbl_color_completed = New System.Windows.Forms.Label
    Me.lbl_color_pending = New System.Windows.Forms.Label
    Me.chk_completed = New System.Windows.Forms.CheckBox
    Me.lbl_color_gap = New System.Windows.Forms.Label
    Me.chk_pending = New System.Windows.Forms.CheckBox
    Me.chk_gap = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lbl_cs_name)
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Size = New System.Drawing.Size(978, 100)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_cs_name, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 104)
    Me.panel_data.Size = New System.Drawing.Size(978, 449)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(972, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(972, 4)
    '
    'lbl_cs_name
    '
    Me.lbl_cs_name.IsReadOnly = True
    Me.lbl_cs_name.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_cs_name.LabelForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_cs_name.Location = New System.Drawing.Point(156, 12)
    Me.lbl_cs_name.Name = "lbl_cs_name"
    Me.lbl_cs_name.Size = New System.Drawing.Size(297, 24)
    Me.lbl_cs_name.SufixText = "Sufix Text"
    Me.lbl_cs_name.SufixTextVisible = True
    Me.lbl_cs_name.TabIndex = 12
    Me.lbl_cs_name.TextWidth = 0
    Me.lbl_cs_name.Value = "xCashier Session Name"
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.lbl_color_completed)
    Me.gb_status.Controls.Add(Me.lbl_color_pending)
    Me.gb_status.Controls.Add(Me.chk_completed)
    Me.gb_status.Controls.Add(Me.lbl_color_gap)
    Me.gb_status.Controls.Add(Me.chk_pending)
    Me.gb_status.Controls.Add(Me.chk_gap)
    Me.gb_status.Location = New System.Drawing.Point(6, 2)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(144, 88)
    Me.gb_status.TabIndex = 11
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'lbl_color_completed
    '
    Me.lbl_color_completed.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_completed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_completed.Location = New System.Drawing.Point(6, 18)
    Me.lbl_color_completed.Name = "lbl_color_completed"
    Me.lbl_color_completed.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_completed.TabIndex = 0
    '
    'lbl_color_pending
    '
    Me.lbl_color_pending.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_pending.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_pending.Location = New System.Drawing.Point(6, 40)
    Me.lbl_color_pending.Name = "lbl_color_pending"
    Me.lbl_color_pending.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_pending.TabIndex = 2
    '
    'chk_completed
    '
    Me.chk_completed.AutoSize = True
    Me.chk_completed.Location = New System.Drawing.Point(27, 18)
    Me.chk_completed.Name = "chk_completed"
    Me.chk_completed.Size = New System.Drawing.Size(95, 17)
    Me.chk_completed.TabIndex = 1
    Me.chk_completed.Text = "xCompleted"
    Me.chk_completed.UseVisualStyleBackColor = True
    '
    'lbl_color_gap
    '
    Me.lbl_color_gap.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_gap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_gap.Location = New System.Drawing.Point(6, 63)
    Me.lbl_color_gap.Name = "lbl_color_gap"
    Me.lbl_color_gap.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_gap.TabIndex = 4
    '
    'chk_pending
    '
    Me.chk_pending.AutoSize = True
    Me.chk_pending.Location = New System.Drawing.Point(27, 40)
    Me.chk_pending.Name = "chk_pending"
    Me.chk_pending.Size = New System.Drawing.Size(78, 17)
    Me.chk_pending.TabIndex = 3
    Me.chk_pending.Text = "xPending"
    Me.chk_pending.UseVisualStyleBackColor = True
    '
    'chk_gap
    '
    Me.chk_gap.AutoSize = True
    Me.chk_gap.Location = New System.Drawing.Point(27, 63)
    Me.chk_gap.Name = "chk_gap"
    Me.chk_gap.Size = New System.Drawing.Size(56, 17)
    Me.chk_gap.TabIndex = 5
    Me.chk_gap.Text = "xGap"
    Me.chk_gap.UseVisualStyleBackColor = True
    '
    'frm_bills_tickets_balance_report_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(986, 557)
    Me.Name = "frm_bills_tickets_balance_report_sel"
    Me.Text = "frm_bills_tickets_balance_report_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents lbl_cs_name As GUI_Controls.uc_text_field
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_color_completed As System.Windows.Forms.Label
  Friend WithEvents lbl_color_pending As System.Windows.Forms.Label
  Friend WithEvents chk_completed As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_color_gap As System.Windows.Forms.Label
  Friend WithEvents chk_pending As System.Windows.Forms.CheckBox
  Friend WithEvents chk_gap As System.Windows.Forms.CheckBox
End Class
