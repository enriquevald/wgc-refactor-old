<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cash_tito_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_dates = New GUI_Controls.uc_daily_session_selector
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.gb_cashiers = New System.Windows.Forms.GroupBox
    Me.btn_check_all = New GUI_Controls.uc_button
    Me.btn_uncheck_all = New GUI_Controls.uc_button
    Me.dg_cashiers = New GUI_Controls.uc_grid
    Me.gb_all_or_actives = New System.Windows.Forms.GroupBox
    Me.cb_only_active = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_cashiers.SuspendLayout()
    Me.gb_all_or_actives.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_all_or_actives)
    Me.panel_filter.Controls.Add(Me.gb_cashiers)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Controls.Add(Me.uc_dates)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1122, 226)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dates, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_cashiers, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_all_or_actives, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 230)
    Me.panel_data.Size = New System.Drawing.Size(1122, 405)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1116, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1116, 4)
    '
    'uc_dates
    '
    Me.uc_dates.ClosingTime = 0
    Me.uc_dates.ClosingTimeEnabled = True
    Me.uc_dates.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dates.FromDateSelected = True
    Me.uc_dates.Location = New System.Drawing.Point(6, 3)
    Me.uc_dates.Name = "uc_dates"
    Me.uc_dates.Size = New System.Drawing.Size(257, 82)
    Me.uc_dates.TabIndex = 0
    Me.uc_dates.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dates.ToDateSelected = True
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(591, 0)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(384, 217)
    Me.uc_pr_list.TabIndex = 14
    '
    'gb_cashiers
    '
    Me.gb_cashiers.Controls.Add(Me.btn_check_all)
    Me.gb_cashiers.Controls.Add(Me.btn_uncheck_all)
    Me.gb_cashiers.Controls.Add(Me.dg_cashiers)
    Me.gb_cashiers.Location = New System.Drawing.Point(269, 3)
    Me.gb_cashiers.Name = "gb_cashiers"
    Me.gb_cashiers.Size = New System.Drawing.Size(316, 217)
    Me.gb_cashiers.TabIndex = 18
    Me.gb_cashiers.TabStop = False
    Me.gb_cashiers.Text = "xCashiers"
    '
    'btn_check_all
    '
    Me.btn_check_all.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_check_all.Location = New System.Drawing.Point(32, 186)
    Me.btn_check_all.Name = "btn_check_all"
    Me.btn_check_all.Size = New System.Drawing.Size(90, 30)
    Me.btn_check_all.TabIndex = 1
    Me.btn_check_all.ToolTipped = False
    Me.btn_check_all.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'btn_uncheck_all
    '
    Me.btn_uncheck_all.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_uncheck_all.Location = New System.Drawing.Point(168, 186)
    Me.btn_uncheck_all.Name = "btn_uncheck_all"
    Me.btn_uncheck_all.Size = New System.Drawing.Size(90, 30)
    Me.btn_uncheck_all.TabIndex = 2
    Me.btn_uncheck_all.ToolTipped = False
    Me.btn_uncheck_all.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.USER
    '
    'dg_cashiers
    '
    Me.dg_cashiers.CurrentCol = -1
    Me.dg_cashiers.CurrentRow = -1
    Me.dg_cashiers.Location = New System.Drawing.Point(17, 20)
    Me.dg_cashiers.Name = "dg_cashiers"
    Me.dg_cashiers.PanelRightVisible = False
    Me.dg_cashiers.Redraw = True
    Me.dg_cashiers.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_cashiers.Size = New System.Drawing.Size(284, 160)
    Me.dg_cashiers.Sortable = False
    Me.dg_cashiers.SortAscending = True
    Me.dg_cashiers.SortByCol = 0
    Me.dg_cashiers.TabIndex = 0
    Me.dg_cashiers.ToolTipped = False
    Me.dg_cashiers.TopRow = -2
    '
    'gb_all_or_actives
    '
    Me.gb_all_or_actives.Controls.Add(Me.cb_only_active)
    Me.gb_all_or_actives.Location = New System.Drawing.Point(6, 91)
    Me.gb_all_or_actives.Name = "gb_all_or_actives"
    Me.gb_all_or_actives.Size = New System.Drawing.Size(257, 55)
    Me.gb_all_or_actives.TabIndex = 20
    Me.gb_all_or_actives.TabStop = False
    Me.gb_all_or_actives.Text = "xGroup All/Actives"
    '
    'cb_only_active
    '
    Me.cb_only_active.AutoSize = True
    Me.cb_only_active.Location = New System.Drawing.Point(65, 24)
    Me.cb_only_active.Name = "cb_only_active"
    Me.cb_only_active.Size = New System.Drawing.Size(68, 17)
    Me.cb_only_active.TabIndex = 20
    Me.cb_only_active.Text = "xActive"
    Me.cb_only_active.UseVisualStyleBackColor = True
    '
    'frm_cash_tito_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1132, 639)
    Me.Name = "frm_cash_tito_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_cash_tito_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_cashiers.ResumeLayout(False)
    Me.gb_all_or_actives.ResumeLayout(False)
    Me.gb_all_or_actives.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_dates As GUI_Controls.uc_daily_session_selector
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents gb_cashiers As System.Windows.Forms.GroupBox
  Friend WithEvents dg_cashiers As GUI_Controls.uc_grid
  Friend WithEvents btn_check_all As GUI_Controls.uc_button
  Friend WithEvents btn_uncheck_all As GUI_Controls.uc_button
  Friend WithEvents gb_all_or_actives As System.Windows.Forms.GroupBox
  Friend WithEvents cb_only_active As System.Windows.Forms.CheckBox
End Class
