'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_tito_report_sel
' DESCRIPTION:   This screen allows to view the TITO reports.
' AUTHOR:        Humberto Braojos
' CREATION DATE: 21-06-2013
'
' REVISION HISTORY:
' 
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 21-JUN-2013  HBB    Initial version
' 06-SEP-2013  NMR    Fixed bug JIRA-WIGOSTITO-91
' 13-AUG-2013  JRM    Refactor search Where section
' 20-SEP-2013  NMR    Renaming TITO types
' 23-SEP-2013  JRM    Refactor search SQL
' 25-SEP-2013  NMR    Changed name of TITO enum
' 14-OCT-2013  DRV    Removed ID Column from the grid
' 22-OCT-2013  DRV    Fixed SQL Query for search by user
' 12-NOV-2013  JAB    Using "InitExtendedQuery" function to show Accounts with ac_type equal to 'ACCOUNT_VIRTUAL_CASHIER' or 'ACCOUNT_VIRTUAL_TERMINAL'.
' 19-NOV-2013  DRV    Added Machine Number column
' 21-NOV-2013  DRV    Added Total and subtotal rows, and discarded ticket status
' 28-NOV-2013  ACM    Added new tickets status
' 09-DEC-2013  AMF    Check controls to radiobutton
' 11-DEC-2013  ACM    Filter error solved. 
' 18-DEC-2013  ACM    Fixed Bug WIGOSTITO-864: Set only Sas-Host terminals to terminals filter control.
' 19-DEC-2013  DRV    Filter error with terminals and cashiers solved. Also deactivate terminals and cashiers filters when date filter is by expiration.
' 07-DEC-2014  JFC    Filter error with cashiers solved, now not to show game tables. Also inproved filters, deactivating the other filters when id account is informed.
' 21-FEB-2014  ICS    Fixed Bug WIGOSTITO-1884: Expiration date of valid tickets is not displayed
' 25-FEB-2014  DRV    Fixed Bug WIGOSTITO-1090: No account information is displayed on Tickets Report
' 25-MAR-2014  ICS    Fixed Bug WIGOSTITO-1166: Some mistakes generating Excel
' 26-MAR-2014  ICS    Fixed Bug WIGOSTITO-1168: The filter 'Some' requires selecting at least one
' 28-MAR-2014  LEM    Fixed Bug WIGOSTITO-1178: Not the default value for all filters is set by the Reset button
' 06-MAY-2014  JBP    Added Vip client filter at reports.
' 02-OCT-2014  DRV    Added kbdMsgEvent
' 12-FEB-2015  DCS    Fixed Bug WIGOS-1995: Don't show empty Totals
' 08-MAR-2016  EOR    Product Backlog Item 10235:TITA: Reporte de tickets
' 30-JUN-2016  AMF    Bug 14943:Countr: en "reporte de tickets" los pagados no salen pagados por el terminal Countr
' 21-FEB-2017  LTC    PBI 22283:Winpot - Change Promotions NLS
' 28-MAR-2017  FOR    Bug 25289:Reportes contables muestran discrepancias en tickets TITO
' 07-FEB-2017  RAB    Bug 23848: No applies filter slope of payment / with date expired in ticket report
' 07-APR-2017  RAB    Bug 26700: Tito tickets report shows a BBDD error when the filter is "redimible" with "Pend. pago" or "Pend. pago caducado"
' 11-APR-2017  RAB    Bug 26737: TITO Ticket report error when the search is without date filter
' 21-JUL-2017  DHA    Bug 28891:WIGOS-3909 Accounting - TITO Ticket report does not filter correctly for ticket status
' 24-JUL-2018  JGC    Bug 33704:WIGOS-13375 [Ticket #15410] CountR - Ticket Report
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports WSI.Common
Imports System.Data.SqlClient
Imports WSI.Common.TITO

Public Class frm_tito_reports_sel
  Inherits frm_base_sel

#Region "Constants"
  Const GRID_COLUMNS_COUNT As Int32 = 15
  Const GRID_HEADERS_COUNT As Int32 = 2

  ' Selector column
  Private Const GRID_COLUMN_SELECTOR As Integer = 0             ' Empty column 
  Private Const GRID_WIDTH_SELECTOR As Integer = 200            ' Empty column width 

  ' Grouped by Creation Date Column order
  Const GRID_COLUMN_TITO_BY_DATE_CREATION_DATE As Int32 = 1
  Const GRID_COLUMN_TITO_BY_DATE_CREATION_TERMINAL As Int32 = 2
  Const GRID_COLUMN_TITO_BY_DATE_USER_CREATION As Int32 = 3
  Const GRID_COLUMN_TITO_BY_DATE_TICKET_NUMBER As Int32 = 4
  Const GRID_COLUMN_TITO_BY_DATE_MACHINE_NUMBER As Int32 = 5
  Const GRID_COLUMN_TITO_BY_DATE_TYPE As Int32 = 6
  Const GRID_COLUMN_TITO_BY_DATE_ASSOCIATED_PROMOTION As Int32 = 7
  Const GRID_COLUMN_TITO_BY_DATE_AMT0 As Int32 = 8
  Const GRID_COLUMN_TITO_BY_DATE_AMOUNT As Int32 = 9
  Const GRID_COLUMN_TITO_BY_DATE_STATE As Int32 = 10
  Const GRID_COLUMN_TITO_BY_DATE_CANCEL_OR_REDEEM_TERMINAL As Int32 = 11
  Const GRID_COLUMN_TITO_BY_DATE_CANCEL_OR_REDEEM_DATE As Int32 = 12
  Const GRID_COLUMN_TITO_BY_DATE_USER_CANCEL_OR_REDEEM As Int32 = 13
  Const GRID_COLUMN_TITO_BY_DATE_EXPIRATION_DATE As Int32 = 14

  ' Grouped by Creation Terminal Column order
  Const GRID_COLUMN_TITO_BY_TERM_CREATION_TERMINAL As Int32 = 1
  Const GRID_COLUMN_TITO_BY_TERM_USER_CREATION As Int32 = 2
  Const GRID_COLUMN_TITO_BY_TERM_CREATION_DATE As Int32 = 3
  Const GRID_COLUMN_TITO_BY_TERM_TICKET_NUMBER As Int32 = 4
  Const GRID_COLUMN_TITO_BY_TERM_MACHINE_NUMBER As Int32 = 5
  Const GRID_COLUMN_TITO_BY_TERM_TYPE As Int32 = 6
  Const GRID_COLUMN_TITO_BY_TERM_ASSOCIATED_PROMOTION As Int32 = 7
  Const GRID_COLUMN_TITO_BY_TERM_AMT0 As Int32 = 8
  Const GRID_COLUMN_TITO_BY_TERM_AMOUNT As Int32 = 9
  Const GRID_COLUMN_TITO_BY_TERM_STATE As Int32 = 10
  Const GRID_COLUMN_TITO_BY_TERM_CANCEL_OR_REDEEM_TERMINAL As Int32 = 11
  Const GRID_COLUMN_TITO_BY_TERM_CANCEL_OR_REDEEM_DATE As Int32 = 12
  Const GRID_COLUMN_TITO_BY_TERM_USER_CANCEL_OR_REDEEM As Int32 = 13
  Const GRID_COLUMN_TITO_BY_TERM_EXPIRATION_DATE As Int32 = 14

  ' Column order without group by
  Const GRID_COLUMN_TITO_NO_GROUP_TICKET_NUMBER As Int32 = 1
  Const GRID_COLUMN_TITO_NO_GROUP_MACHINE_NUMBER As Int32 = 2
  Const GRID_COLUMN_TITO_NO_GROUP_TYPE As Int32 = 3
  Const GRID_COLUMN_TITO_NO_GROUP_ASSOCIATED_PROMOTION As Int32 = 4
  Const GRID_COLUMN_TITO_NO_GROUP_AMT0 As Int32 = 5
  Const GRID_COLUMN_TITO_NO_GROUP_AMOUNT As Int32 = 6
  Const GRID_COLUMN_TITO_NO_GROUP_STATE As Int32 = 7
  Const GRID_COLUMN_TITO_NO_GROUP_CREATION_TERMINAL As Int32 = 8
  Const GRID_COLUMN_TITO_NO_GROUP_USER_CREATION As Int32 = 9
  Const GRID_COLUMN_TITO_NO_GROUP_CREATION_DATE As Int32 = 10
  Const GRID_COLUMN_TITO_NO_GROUP_CANCEL_OR_REDEEM_TERMINAL As Int32 = 11
  Const GRID_COLUMN_TITO_NO_GROUP_CANCEL_OR_REDEEM_DATE As Int32 = 12
  Const GRID_COLUMN_TITO_NO_GROUP_USER_CANCEL_OR_REDEEM As Int32 = 13
  Const GRID_COLUMN_TITO_NO_GROUP_EXPIRATION_DATE As Int32 = 14

  'Width
  Const GRID_WIDTH_TITO_CREATION_DATE As Int32 = 2200
  Const GRID_WIDTH_TITO_MACHINE_NUMBER As Int32 = 1400
  Const GRID_WIDTH_TITO_TICKET_NUMBER As Int32 = 2400
  Const GRID_WIDTH_TITO_TYPE As Int32 = 1650
  Const GRID_WIDTH_TITO_AMT0 As Int32 = 1900
  Const GRID_WIDTH_TITO_AMOUNT As Int32 = 1600
  Const GRID_WIDTH_TITO_CREATION_TERMINAL As Int32 = 1600
  Const GRID_WIDTH_TITO_CREATION_TERMINAL_TYPE As Int32 = 1600
  Const GRID_WIDTH_TITO_CANCEL_OR_REDEEM_TERMINAL As Int32 = 1600
  Const GRID_WIDTH_TITO_CANCEL_OR_REDEEM_TERMINAL_TYPE As Int32 = 1600
  Const GRID_WIDTH_TITO_USER_CREATION As Int32 = 2500
  Const GRID_WIDTH_TITO_USER_CANCEL_OR_REDEEM As Int32 = 2500
  Const GRID_WIDTH_TITO_STATE As Int32 = 2865 'EOR 08-MAR-2016 Expands Column Width
  Const GRID_WIDTH_TITO_CANCEL_OR_REDEEM_DATE As Int32 = 2200
  Const GRID_WIDTH_TITO_EXPIRATION_DATE As Int32 = 2200
  Const GRID_WIDTH_TITO_ASSOCIATED_PROMOTION As Int32 = 2500

  'SQL COLUMNS
  Const SQL_COLUMN_TITO_TICKET_VALIDATION_NUMBER As Int32 = 0
  Const SQL_COLUMN_TITO_AMT0 As Int32 = 1
  Const SQL_COLUMN_TITO_CUR0 As Int32 = 2
  Const SQL_COLUMN_TITO_AMOUNT As Int32 = 3
  Const SQL_COLUMN_TITO_STATE As Int32 = 4
  Const SQL_COLUMN_TITO_TYPE As Int32 = 5
  Const SQL_COLUMN_TITO_CREATION_DATE As Int32 = 6
  Const SQL_COLUMN_TITO_CREATION_TERMINAL As Int32 = 7
  Const SQL_COLUMN_TITO_CREATION_TERMINAL_TYPE As Int32 = 8
  Const SQL_COLUMN_TITO_EXPIRATION_DATE As Int32 = 9
  Const SQL_COLUMN_TITO_COLLECTION_ID As Int32 = 10
  Const SQL_COLUMN_TITO_USER_CREATION As Int32 = 11
  Const SQL_COLUMN_TITO_CANCEL_OR_REDEEM_TERMINAL As Int32 = 12
  Const SQL_COLUMN_TITO_CANCEL_OR_REDEEM_DATE As Int32 = 13
  Const SQL_COLUMN_TITO_CANCEL_OR_REDEEM_TERMINAL_TYPE As Int32 = 14
  Const SQL_COLUMN_TITO_ASSOCIATED_PROMOTION As Int32 = 15
  Const SQL_COLUMN_TITO_VALIDATION_NUMBER_TYPE As Int32 = 16
  Const SQL_COLUMN_TITO_MACHINE_NUMBER As Int32 = 17



  'Cshier Grid
  Private Const GRID_CASHIER_HEADER_ROWS As Integer = 0
  Private Const GRID_CASHIER_COLUMNS As Integer = 3
  Private Const GRID_CASHIER_ID As Integer = 0
  Private Const GRID_CASHIER_COLUMN_CHECKED As Integer = 1
  Private Const GRID_CASHIER_COLUMN_DESCRIPTION As Integer = 2

  'Bank Grid Width
  Private Const GRID_CASHIER_WIDTH_COLUMN_DESCRIPTION As Integer = 3355
  Private Const GRID_CASHIER_WIDTH_COLUMN_CHECKED As Integer = 300

  Private Const TITO_TICKET_TYPES As Integer = 5
  Private Const MAX_FILTER_REPORT As Integer = 4

#End Region

#Region "MEMBERS"

  ' Filter members
  Private m_date_from As String
  Private m_date_to As String
  Private m_filter_by_date As String
  Private m_state As String
  Private m_ticket_type As String
  Private m_terminals As String
  Private m_cashiers As String
  Private m_options As String
  Private m_subtotal_current_termnal As String
  Private m_subtotal_current_date As DateTime
  Private m_subtotal_values As Decimal()
  Private m_total_values As Decimal()
  Private m_created As Boolean
  Private m_cancelled As Boolean
  Private m_account_id As String
  Private m_account_trackdata As String
  Private m_account_holder As String
  Private m_account_holder_is_vip As String
  Private m_cashiers_terminals As String
  Private m_search_cashiers As Boolean
  Private m_search_terminals As Boolean
  Private m_detailed As Boolean
  Private m_barcode_handler As KbdMsgFilterHandler

  ' Columns order members
  Private m_grid_column_tito_ticket_number As Int32
  Private m_grid_column_tito_machine_number As Int32
  Private m_grid_column_tito_type As Int32
  Private m_grid_column_tito_associated_promotion As Int32
  Private m_grid_column_tito_amt0 As Int32
  Private m_grid_column_tito_amount As Int32
  Private m_grid_column_tito_state As Int32
  Private m_grid_column_tito_creation_terminal As Int32
  Private m_grid_column_tito_user_creation As Int32
  Private m_grid_column_tito_creation_date As Int32
  Private m_grid_column_tito_cancel_or_redeem_terminal As Int32
  Private m_grid_column_tito_cancel_or_redeem_date As Int32
  Private m_grid_column_tito_user_cancel_or_redeem As Int32
  Private m_grid_column_tito_expiration_date As Int32



#End Region

#Region "Overrides Functions"

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TITO_TICKET_REPORT

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()
    Dim _size_gb_state As Size

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2124)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = True 'auditoria


    chk_redeemable_ticket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125)
    chk_promo_redeem_ticket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2701, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125))
    chk_promo_nonredeem_ticket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2701, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2126))
    chk_handpay.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2685)
    chk_jackpot.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2686)
    chk_offline.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3327)


    gb_ticket_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2134)
    gb_state.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2135)

    chk_valid.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3010)
    chk_canceled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7671)
    chk_redeemed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2138)
    chk_expired.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2137)
    chk_discarded.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2989)
    chk_pending_cancel.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7672)
    chk_pending_print.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7648)
    chk_expired_redeemed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2138) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2137)
    chk_swap_chips.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7145)
    chk_pending_payment.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7278)
    chk_pending_payment_after_expiration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7279)


    opt_creation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2108)
    opt_cancelation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2120)
    opt_expiration.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2123)
    gb_date_filter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2118)
    gb_cashiers.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(491)
    btn_check_all.Text = GLB_NLS_GUI_ALARMS.GetString(452)
    btn_uncheck_all.Text = GLB_NLS_GUI_ALARMS.GetString(453)
    gb_machine_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2827)
    chk_cashiers.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(491)
    chk_terminals.Text = GLB_NLS_GUI_STATISTICS.GetString(470)
    ef_ticket_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(795)
    ef_ticket_number.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 18)

    chk_detailed_report.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3341)

    'EOR 22-MAR-2016 Text for Swap Chips
    If GeneralParam.GetInt32("TITA", "Enabled") = 0 Then
      chk_swap_chips.Visible = False
    End If

    ' Group by groupbox
    gb_group_by.Text = GLB_NLS_GUI_INVOICING.GetString(369)
    chk_group_by.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2990)
    opt_date_creation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2991)
    opt_terminal_creation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2992)

    dg_cashiers.PanelRightVisible = False
    dg_cashiers.Sortable = False
    dg_cashiers.TopRow = -2

    If GeneralParam.GetInt32("TITO", "Payment.GracePeriod.Mode") = 0 Then
      Me.chk_expired_redeemed.Visible = False
      _size_gb_state.Height = Me.gb_state.Size.Height - 27
      _size_gb_state.Width = Me.gb_state.Size.Width

      Me.gb_state.Size = _size_gb_state

    End If

    Call GUI_FilterReset()
    Call GUI_StyleSheet()
    Call GUI_StyleSheetCashiers()
    Call GUI_GetSqlQueryCashiers()

    ' Providers - Terminals
    Dim _terminal_types() As Integer = Nothing
    ReDim Preserve _terminal_types(0 To 0)
    _terminal_types(0) = TerminalTypes.SAS_HOST

    Call Me.uc_terminals_list.Init(_terminal_types)
    Call Me.uc_account_sel.DisableKbdMessage()

    'XIT KeyboardMessageFilter Barcode Events
    KeyboardMessageFilter.Init()
    m_barcode_handler = AddressOf Me.BarcodeEvent
    KeyboardMessageFilter.RegisterHandler(Me, BarcodeType.FilterAnyBarcode, m_barcode_handler)

  End Sub ' GUI_InitControls

  ' PURPOSE: Call GUI_ButtonClick with the Cursor in Wait Mode. Finally restore Cursor to Default.
  '
  '  PARAMS:
  '     - INPUT:
  '           - ButtonId As ENUM_BUTTON
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     -
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call GUI_StyleSheet()
        Call MyBase.GUI_ButtonClick(ButtonId)

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
    EnableTicketAudit()
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sb As StringBuilder
    Dim _nat_curr As String

    _nat_curr = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "   ")

    _sb = New StringBuilder()

    _sb.AppendLine("    SELECT   TI_VALIDATION_NUMBER                                                                                                                                          ")
    _sb.AppendLine("           , isNull(TI_AMT0, TI_AMOUNT) AS TI_AMT0                                                                                                                         ")
    _sb.AppendLine("           , isNull(TI_CUR0, '" & _nat_curr & "') as TI_CUR0                                                                                                               ")
    _sb.AppendLine("           , TI_AMOUNT                                                                                                                                                     ")
    _sb.AppendLine("           , TI_STATUS                                                                                                                                                     ")
    _sb.AppendLine("           , TI_TYPE_ID                                                                                                                                                    ")
    _sb.AppendLine("           , TI_CREATED_DATETIME                                                                                                                                           ")
    _sb.AppendLine("           , CASE WHEN TI_CREATED_TERMINAL_TYPE IN (0,2) THEN CASHIER_TERMINAL_CREATION.CT_NAME ELSE TERMINAL_CREATION.TE_NAME  END AS TE_TERMINAL_NAME                    ")
    _sb.AppendLine("           , TI_CREATED_TERMINAL_TYPE                                                                                                                                      ")
    _sb.AppendLine("           , TI_EXPIRATION_DATETIME                                                                                                                                        ")
    _sb.AppendLine("           , TI_MONEY_COLLECTION_ID                                                                                                                                        ")
    _sb.AppendLine("           , ACCOUNTS_CREATION.AC_HOLDER_NAME                                                                                                                              ")
    _sb.AppendLine("           , CASE WHEN TI_LAST_ACTION_TERMINAL_TYPE IN (0,2) THEN CASHIER_TERMINALS_LAST_ACTION.CT_NAME ELSE TERMINAL_LAST_ACTION.TE_NAME  END AS TI_LAST_ACTION_TERMINAL_ID    ")
    _sb.AppendLine("           , TI_LAST_ACTION_DATETIME                                                                                                                                       ")
    _sb.AppendLine("           , TI_LAST_ACTION_TERMINAL_TYPE                                                                                                                                  ")
    _sb.AppendLine("           , PM_NAME                                                                                                                                                       ")
    _sb.AppendLine("           , TI_VALIDATION_TYPE                                                                                                                                            ")
    _sb.AppendLine("           , TI_MACHINE_NUMBER                                                                                                                                             ")
    _sb.AppendLine("           , TI_TICKET_ID                                                                                                                                                  ")
    _sb.AppendLine("      FROM   TICKETS                                                                                                                                                       ")
    _sb.AppendLine(" LEFT JOIN   TERMINALS TERMINAL_CREATION ON TICKETS.TI_CREATED_TERMINAL_ID = TERMINAL_CREATION.TE_TERMINAL_ID                                                              ")
    _sb.AppendLine(" LEFT JOIN   CASHIER_TERMINALS CASHIER_TERMINAL_CREATION ON  TICKETS.TI_CREATED_TERMINAL_ID = CASHIER_TERMINAL_CREATION.CT_CASHIER_ID                                      ")
    _sb.AppendLine(" LEFT JOIN   TERMINALS TERMINAL_LAST_ACTION  ON TICKETS.TI_LAST_ACTION_TERMINAL_ID = TERMINAL_LAST_ACTION.TE_TERMINAL_ID                                                   ")
    _sb.AppendLine(" LEFT JOIN   CASHIER_TERMINALS CASHIER_TERMINALS_LAST_ACTION ON  TICKETS.TI_LAST_ACTION_TERMINAL_ID = CASHIER_TERMINALS_LAST_ACTION.CT_CASHIER_ID                          ")
    _sb.AppendLine(" LEFT JOIN   ACCOUNTS ACCOUNTS_CREATION ON TICKETS.TI_CREATED_ACCOUNT_ID = ACCOUNTS_CREATION.AC_ACCOUNT_ID                                                                 ")
    _sb.AppendLine(" LEFT JOIN   PROMOTIONS ON PROMOTIONS.PM_PROMOTION_ID = TICKETS.TI_PROMOTION_ID                                                                                            ")
    _sb.AppendLine(GetSqlWhere())
    If (chk_group_by.Checked And opt_terminal_creation.Checked) Then
      _sb.AppendLine("  ORDER BY   TE_TERMINAL_NAME                                                                                                                                               ")
    Else
      _sb.AppendLine("  ORDER BY   TI_CREATED_DATETIME DESC                                                                                                                                       ")
    End If

    Return _sb.ToString()
  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    SetDefaultValues()
  End Sub 'GUI_FilterReset

  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _ticket_validation_number As String

    Dim _ticket_type As String
    Dim _ticket_status As String
    Dim _ticket_amount As String
    Dim _ticket_terminal As String
    Dim _ticket_creation_date_time As String
    Dim _ticket_account As String



    Dim _frm_ticket_audits_sel_unique As frm_ticket_audits_sel_unique
    Dim _idx_row As Integer


    _idx_row = Me.Grid.SelectedRows(0)

    'total o subtotal
    If (Me.Grid.Cell(_idx_row, m_grid_column_tito_ticket_number).Value = GLB_NLS_GUI_INVOICING.GetString(375)) Or
      (Me.Grid.Cell(_idx_row, m_grid_column_tito_ticket_number).Value = GLB_NLS_GUI_INVOICING.GetString(205)) Then
      Return
    End If




    If (Me.Grid.SelectedRows.Length < 1) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2040), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Return
    End If

    _ticket_validation_number = Me.Grid.Cell(_idx_row, m_grid_column_tito_ticket_number).Value

    _ticket_type = Me.Grid.Cell(_idx_row, m_grid_column_tito_type).Value
    _ticket_status = Me.Grid.Cell(_idx_row, m_grid_column_tito_state).Value
    _ticket_amount = Me.Grid.Cell(_idx_row, m_grid_column_tito_amount).Value
    _ticket_terminal = Me.Grid.Cell(_idx_row, m_grid_column_tito_creation_terminal).Value
    _ticket_creation_date_time = Me.Grid.Cell(_idx_row, m_grid_column_tito_creation_date).Value
    _ticket_account = Me.Grid.Cell(_idx_row, m_grid_column_tito_user_creation).Value   '//TODO





    _frm_ticket_audits_sel_unique = New frm_ticket_audits_sel_unique
    Call _frm_ticket_audits_sel_unique.ShowForEdit(Me.MdiParent, _ticket_validation_number, _ticket_status, _ticket_amount, _ticket_type, _ticket_terminal, _ticket_creation_date_time, _ticket_account)
    _frm_ticket_audits_sel_unique = Nothing
    Call Me.Grid.Focus()


  End Sub ' GUI_EditSelectedItem



  ' PURPOSE: Enable button in selected row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)

    Dim _ticket_validation_number As Int64

    _ticket_validation_number = 0

    If IsNumeric(Me.Grid.Cell(SelectedRow, m_grid_column_tito_ticket_number).Value) Then
      _ticket_validation_number = CLng(Me.Grid.Cell(SelectedRow, m_grid_column_tito_ticket_number).Value)
    End If

    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = _ticket_validation_number > 0


  End Sub  ' GUI_RowSelectedEvent



  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    If Me.ds_from_to.FromDateSelected And Me.ds_from_to.ToDateSelected Then
      If Me.ds_from_to.FromDate > Me.ds_from_to.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.ds_from_to.Focus()

        Return False
      End If
    End If

    If Not String.IsNullOrEmpty(ef_ticket_number.TextValue.Trim()) AndAlso Not ef_ticket_number.ValidateFormat() Then
      Call ef_ticket_number.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1654), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                      mdl_NLS.ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1, ef_ticket_number.Text)

      Return False
    End If

    If Not Me.uc_account_sel.ValidateFormat() Then
      Return False
    End If

    Return True

  End Function ' GUI_FilterCheck


  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.ds_from_to
  End Sub ' GUI_SetInitialFocus


  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _tito_validation_number_type As Int32
    Dim _is_special As Boolean

    Me.Grid.Cell(RowIndex, m_grid_column_tito_creation_date).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_TITO_CREATION_DATE), _
                                                                          ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                          ENUM_FORMAT_TIME.FORMAT_HHMMSS)


    If IsDBNull(DbRow.Value(SQL_COLUMN_TITO_TICKET_VALIDATION_NUMBER)) Then
      Return False
    End If

    ' JRM TODO: Determine if this will be the default value
    _tito_validation_number_type = 1

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TITO_VALIDATION_NUMBER_TYPE)) Then
      _tito_validation_number_type = DbRow.Value(SQL_COLUMN_TITO_VALIDATION_NUMBER_TYPE)
    End If

    Me.Grid.Cell(RowIndex, m_grid_column_tito_ticket_number).Value = ValidationNumberManager.FormatValidationNumber(DbRow.Value(SQL_COLUMN_TITO_TICKET_VALIDATION_NUMBER), _tito_validation_number_type, True)

    Select Case DbRow.Value(SQL_COLUMN_TITO_TYPE)
      Case TITO_TICKET_TYPE.CASHABLE
        Me.Grid.Cell(RowIndex, m_grid_column_tito_type).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125)

      Case TITO_TICKET_TYPE.PROMO_REDEEM
        Me.Grid.Cell(RowIndex, m_grid_column_tito_type).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2701, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3320))

      Case TITO_TICKET_TYPE.PROMO_NONREDEEM
        Me.Grid.Cell(RowIndex, m_grid_column_tito_type).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2701, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3321))

      Case TITO_TICKET_TYPE.HANDPAY
        Me.Grid.Cell(RowIndex, m_grid_column_tito_type).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2685)

      Case TITO_TICKET_TYPE.JACKPOT
        Me.Grid.Cell(RowIndex, m_grid_column_tito_type).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2686)

      Case TITO_TICKET_TYPE.OFFLINE
        Me.Grid.Cell(RowIndex, m_grid_column_tito_type).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3327)

    End Select

    Me.Grid.Cell(RowIndex, m_grid_column_tito_amt0).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_TITO_AMT0), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & " " & DbRow.Value(SQL_COLUMN_TITO_CUR0)
    Me.Grid.Cell(RowIndex, m_grid_column_tito_amount).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TITO_AMOUNT))

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TITO_CREATION_TERMINAL)) Then
      Me.Grid.Cell(RowIndex, m_grid_column_tito_creation_terminal).Value = DbRow.Value(SQL_COLUMN_TITO_CREATION_TERMINAL)
    End If

    'state
    _is_special = (DbRow.Value(SQL_COLUMN_TITO_TYPE) = TITO_TICKET_TYPE.PROMO_NONREDEEM)
    Me.Grid.Cell(RowIndex, m_grid_column_tito_state).Value = TicketStatusAsString(DbRow.Value(SQL_COLUMN_TITO_STATE), _is_special)

    'Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_STATE).Value = DbRow.Value(SQL_COLUMN_TITO_STATE)
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TITO_USER_CREATION)) Then
      Me.Grid.Cell(RowIndex, m_grid_column_tito_user_creation).Value = DbRow.Value(SQL_COLUMN_TITO_USER_CREATION)
    End If

    If Not DbRow.Value(SQL_COLUMN_TITO_STATE) = WSI.Common.TITO_TICKET_STATUS.VALID Then

      If Not IsDBNull(DbRow.Value(SQL_COLUMN_TITO_CANCEL_OR_REDEEM_DATE)) Then
        Me.Grid.Cell(RowIndex, m_grid_column_tito_cancel_or_redeem_date).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_TITO_CANCEL_OR_REDEEM_DATE), _
                                                                            ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                            ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If

      If Not IsDBNull(DbRow.Value(SQL_COLUMN_TITO_CANCEL_OR_REDEEM_TERMINAL)) Then
        Me.Grid.Cell(RowIndex, m_grid_column_tito_cancel_or_redeem_terminal).Value = DbRow.Value(SQL_COLUMN_TITO_CANCEL_OR_REDEEM_TERMINAL)
      End If
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TITO_EXPIRATION_DATE)) Then
      Me.Grid.Cell(RowIndex, m_grid_column_tito_expiration_date).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_TITO_EXPIRATION_DATE), _
                                                                          ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                          ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TITO_ASSOCIATED_PROMOTION)) Then
      Me.Grid.Cell(RowIndex, m_grid_column_tito_associated_promotion).Value = DbRow.Value(SQL_COLUMN_TITO_ASSOCIATED_PROMOTION)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TITO_MACHINE_NUMBER)) Then
      Me.Grid.Cell(RowIndex, m_grid_column_tito_machine_number).Value = DbRow.Value(SQL_COLUMN_TITO_MACHINE_NUMBER)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TITO_TICKET_VALIDATION_NUMBER)) Then
      Me.Grid.Cell(RowIndex, m_grid_column_tito_ticket_number).Value = DbRow.Value(SQL_COLUMN_TITO_TICKET_VALIDATION_NUMBER)
    End If


    Return True

  End Function


  ' PURPOSE: Sets report filters values
  '
  '  PARAMS:
  '     - INPUT:
  '           - ExcelData 
  '           - FirstColIndex  
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ExcelData.SetColumnFormat(m_grid_column_tito_ticket_number - 1, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) 'ticket number

  End Sub

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim _no_one As Boolean
    Dim _all As Boolean

    m_date_from = ""
    m_date_to = ""
    m_filter_by_date = ""
    m_state = ""
    m_ticket_type = ""
    m_terminals = ""
    m_options = ""
    m_account_id = ""
    m_account_trackdata = ""
    m_account_holder = ""
    m_account_holder_is_vip = ""
    m_cashiers_terminals = ""
    m_cashiers = ""

    'if there is a validation number defined the other filters must to be discarded
    If ef_ticket_number.Value <> "" Then
      m_ticket_type = ef_ticket_number.Value

      Return
    End If

    If chk_detailed_report.Enabled Then
      m_detailed = chk_detailed_report.Checked
    Else
      m_detailed = True
    End If

    m_created = Me.opt_creation.Checked
    m_cancelled = Me.opt_cancelation.Checked

    'date from
    If Me.ds_from_to.FromDateSelected Then
      m_date_from = GUI_FormatDate(Me.ds_from_to.FromDate.AddHours(ds_from_to.ClosingTime), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    'date to
    If Me.ds_from_to.ToDateSelected Then
      m_date_to = GUI_FormatDate(Me.ds_from_to.ToDate.AddHours(ds_from_to.ClosingTime), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' filter by date
    If opt_creation.Checked Then
      m_filter_by_date = Me.opt_creation.Text
    ElseIf opt_cancelation.Checked Then
      m_filter_by_date = Me.opt_cancelation.Text
    ElseIf opt_expiration.Checked Then
      m_filter_by_date = Me.opt_expiration.Text
    End If

    ' state 
    _no_one = True
    _all = True

    If chk_valid.Checked Then
      m_state = m_state & Me.chk_valid.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_canceled.Checked Then
      m_state = m_state & Me.chk_canceled.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_pending_cancel.Checked Then
      m_state = m_state & Me.chk_pending_cancel.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_expired.Checked Then
      m_state = m_state & Me.chk_expired.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_pending_print.Checked Then
      m_state = m_state & Me.chk_pending_print.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_redeemed.Checked Then
      m_state = m_state & Me.chk_redeemed.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_expired_redeemed.Checked Then
      m_state = m_state & Me.chk_expired_redeemed.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_discarded.Checked Then
      m_state = m_state & Me.chk_discarded.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_pending_payment.Checked Then
      m_state = m_state & Me.chk_pending_payment.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_pending_payment_after_expiration.Checked Then
      m_state = m_state & Me.chk_pending_payment_after_expiration.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    'EOR 08-MAR-2016 Update Filter Type Swap
    If chk_swap_chips.Checked Then
      m_state = m_state & Me.chk_swap_chips.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If m_state.Length <> 0 Then
      m_state = Strings.Left(m_state, Len(m_state) - 2)
    End If

    If _no_one Or _all Then
      m_state = GLB_NLS_GUI_ALARMS.GetString(277)
    End If

    'ticket type
    _no_one = True
    _all = True

    If chk_redeemable_ticket.Checked Then
      m_ticket_type = m_ticket_type & Me.chk_redeemable_ticket.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_promo_redeem_ticket.Checked Then
      m_ticket_type = m_ticket_type & Me.chk_promo_redeem_ticket.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_promo_nonredeem_ticket.Checked Then
      m_ticket_type = m_ticket_type & Me.chk_promo_nonredeem_ticket.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_jackpot.Checked Then
      m_ticket_type = m_ticket_type & Me.chk_jackpot.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_handpay.Checked Then
      m_ticket_type = m_ticket_type & Me.chk_handpay.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_offline.Checked Then
      m_ticket_type = m_ticket_type & Me.chk_offline.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If m_ticket_type.Length <> 0 Then
      m_ticket_type = Strings.Left(m_ticket_type, Len(m_ticket_type) - 2)
    End If

    If _no_one Or _all Then
      m_ticket_type = GLB_NLS_GUI_ALARMS.GetString(277)
    End If

    ' Terminals 
    m_search_terminals = Me.chk_terminals.Checked

    m_terminals = Me.uc_terminals_list.GetTerminalReportText()

    ' Options
    If Me.chk_group_by.Checked And (opt_date_creation.Checked Or opt_terminal_creation.Checked) Then
      m_options = chk_group_by.Text
      If opt_date_creation.Checked Then
        m_options = m_options & " " & opt_date_creation.Text
      Else
        m_options = m_options & " " & opt_terminal_creation.Text
      End If
    End If

    ' Cashiers
    m_search_cashiers = Me.chk_cashiers.Checked

    ' Cashiers and terminals
    m_cashiers_terminals = IIf(Me.chk_cashiers.Checked, Me.chk_cashiers.Text, String.Empty)
    m_cashiers_terminals &= IIf(Me.chk_cashiers.Checked And Me.chk_terminals.Checked, ", ", String.Empty) & IIf(Me.chk_terminals.Checked, Me.chk_terminals.Text, String.Empty)

    m_cashiers = GetCashiersSelected(GRID_CASHIER_COLUMN_DESCRIPTION)

    ' Account
    m_account_id = Me.uc_account_sel.Account()
    m_account_trackdata = Me.uc_account_sel.TrackData()
    m_account_holder = Me.uc_account_sel.Holder()
    m_account_holder_is_vip = Me.uc_account_sel.HolderIsVip

  End Sub 'GUI_ReportUpdateFilters


  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1328), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1329), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2118), m_filter_by_date)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2135), m_state)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2134), m_ticket_type)
    PrintData.FilterHeaderWidth(2) = 2000
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2827), m_cashiers_terminals)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(491), m_cashiers)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(230), m_account_id)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(212), m_account_trackdata)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(235), m_account_holder)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4802), m_account_holder_is_vip)
    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(369), m_options)

    If Not String.IsNullOrEmpty(m_options) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(3341), IIf(m_detailed, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))
    End If
    PrintData.FilterHeaderWidth(2) = 2500
    PrintData.FilterHeaderWidth(3) = 2500
    'PrintData.FilterValueWidth(2) = 4000
  End Sub


  ' PURPOSE : Checks out the values of a db row before adding it to the grid
  '           We use it here to add total data before adding "normal" data.
  '
  '  PARAMS :
  '     - INPUT :
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the db row should be added to the grid
  '     - False: the db row should NOT be added to the grid
  '
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean

    Dim _date As DateTime

    If (DbRow.Value(SQL_COLUMN_TITO_TYPE) = TITO_TICKET_TYPE.CHIPS_DEALER_COPY) Then
      Return False
    End If

    If Not (IsDBNull(DbRow.Value(SQL_COLUMN_TITO_CREATION_TERMINAL)) Or IsDBNull(DbRow.Value(SQL_COLUMN_TITO_TYPE)) Or IsDBNull(DbRow.Value(SQL_COLUMN_TITO_AMOUNT))) Then

      If chk_group_by.Checked And opt_terminal_creation.Checked Then
        'Calculates the subtotals for each terminal
        If m_subtotal_current_termnal = Nothing Then
          m_subtotal_current_termnal = DbRow.Value(SQL_COLUMN_TITO_CREATION_TERMINAL)
          Call ClearSubtotal()
        End If

        If m_subtotal_current_termnal.Equals(DbRow.Value(SQL_COLUMN_TITO_CREATION_TERMINAL)) Then
          m_subtotal_values(DbRow.Value(SQL_COLUMN_TITO_TYPE)) += DbRow.Value(SQL_COLUMN_TITO_AMOUNT)
        Else
          If (chk_group_by.Checked And opt_date_creation.Checked) Or (chk_group_by.Checked And opt_terminal_creation.Checked) Then
            Call PrintSubTotalRow()
          End If
          m_subtotal_current_termnal = DbRow.Value(SQL_COLUMN_TITO_CREATION_TERMINAL)
          Call ClearSubtotal()
          m_subtotal_values(DbRow.Value(SQL_COLUMN_TITO_TYPE)) += DbRow.Value(SQL_COLUMN_TITO_AMOUNT)
        End If

      ElseIf chk_group_by.Checked And opt_date_creation.Checked Then

        'Calculates the subtotals for each date
        If m_subtotal_current_date = Nothing Then
          m_subtotal_current_date = DbRow.Value(SQL_COLUMN_TITO_CREATION_DATE)
          m_subtotal_current_date = m_subtotal_current_date.Date
          Call ClearSubtotal()
        End If

        _date = DbRow.Value(SQL_COLUMN_TITO_CREATION_DATE)
        _date = _date.Date
        If m_subtotal_current_date.CompareTo(_date) = 0 Then
          m_subtotal_values(DbRow.Value(SQL_COLUMN_TITO_TYPE)) += DbRow.Value(SQL_COLUMN_TITO_AMOUNT)
        Else
          If (chk_group_by.Checked And opt_date_creation.Checked) Or (chk_group_by.Checked And opt_terminal_creation.Checked) Then
            Call PrintSubTotalRow()
          End If
          m_subtotal_current_date = DbRow.Value(SQL_COLUMN_TITO_CREATION_DATE)
          m_subtotal_current_date = m_subtotal_current_date.Date
          Call ClearSubtotal()
          m_subtotal_values(DbRow.Value(SQL_COLUMN_TITO_TYPE)) += DbRow.Value(SQL_COLUMN_TITO_AMOUNT)
        End If

      ElseIf Not chk_group_by.Checked Then
        ' No group, calculate totals
        m_total_values(DbRow.Value(SQL_COLUMN_TITO_TYPE)) += DbRow.Value(SQL_COLUMN_TITO_AMOUNT)

      End If
    End If

    If Not m_detailed AndAlso String.IsNullOrEmpty(ef_ticket_number.Value) Then
      Return False
    End If

    Return True
  End Function ' GUI_CheckOutRowBeforeAdd


  ' PURPOSE: Perform preliminary processing for the grid (reset Totalizators counters)
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    m_subtotal_current_termnal = Nothing
    m_subtotal_current_date = Nothing
    Call ClearSubtotal()
    Call ClearTotal()
    MyBase.GUI_BeforeFirstRow()
    Call EnableTicketAudit()
  End Sub


  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    If (chk_group_by.Checked And opt_date_creation.Checked) Or (chk_group_by.Checked And opt_terminal_creation.Checked) Then
      'Print las subtotal rows
      Call PrintSubTotalRow()
    End If

    'Print the total rows
    Call PrintTotalRow()

    MyBase.GUI_AfterLastRow()
    Call EnableTicketAudit()
  End Sub


  'PURPOSE: Executed just before closing form
  '         
  ' PARAMS:
  '    - INPUT :
  '
  '    - OUTPUT :
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_Closing(ByRef CloseCanceled As Boolean)
    KeyboardMessageFilter.UnregisterHandler(Me, m_barcode_handler)
    MyBase.GUI_Closing(CloseCanceled)
  End Sub

#End Region

#Region "Private Functions"

  ' PURPOSE: Manages barcode received events
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Sub BarcodeEvent(ByVal e As KbdMsgEvent, ByVal Data As Object)

    Try
      If Not Data Is Nothing Then
        Dim _data_read As WSI.Common.Barcode = DirectCast(Data, WSI.Common.Barcode)

        Select Case CType(_data_read.Type, WSI.Common.BarcodeType)
          Case WSI.Common.BarcodeType.Account
            uc_account_sel.TrackData = _data_read.ExternalCode
            Me.ef_ticket_number.TextValue = ""
            Call EnableFilters(True)
            Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

          Case BarcodeType.TitoSecureEnhancedValidation _
             , BarcodeType.TitoStandardValidation _
             , BarcodeType.TitoSystemValidation
            Me.ef_ticket_number.TextValue = _data_read.ExternalCode
            Me.uc_account_sel.TrackData = ""
            Call EnableFilters(False)
            Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

          Case Else
        End Select
      End If
    Catch ex As Exception
    End Try
  End Sub


  ' PURPOSE: Clears Subtotal counters
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub ClearSubtotal()
    Dim _idx As Integer
    m_subtotal_values = Nothing
    ReDim m_subtotal_values(TITO_TICKET_TYPES)
    For _idx = 0 To m_subtotal_values.Length - 1
      m_subtotal_values(_idx) = 0
    Next
  End Sub


  ' PURPOSE: Clears total counters
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub ClearTotal()
    Dim _idx As Integer
    m_total_values = Nothing
    ReDim m_total_values(TITO_TICKET_TYPES)
    For _idx = 0 To m_total_values.Length - 1
      m_total_values(_idx) = 0
    Next
  End Sub


  ' PURPOSE: Print subtotal row for each ticket type. If a ticket type has no value, it's not printed
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub PrintSubTotalRow()
    Dim _row As Integer
    Dim _idx As Integer

    For _idx = 0 To m_subtotal_values.Length - 1
      If m_subtotal_values(_idx) <> 0 Then
        Me.Grid.AddRow()
        _row = Me.Grid.NumRows - 1

        ' Set the column values on group by
        If chk_group_by.Checked Then
          If opt_date_creation.Checked Then
            Me.Grid.Cell(_row, m_grid_column_tito_creation_date).Value = m_subtotal_current_date

          ElseIf opt_terminal_creation.Checked Then
            Me.Grid.Cell(_row, m_grid_column_tito_creation_terminal).Value = m_subtotal_current_termnal

          End If

        End If

        Me.Grid.Cell(_row, m_grid_column_tito_ticket_number).Value = GLB_NLS_GUI_INVOICING.GetString(375)

        Select Case _idx
          Case TITO_TICKET_TYPE.CASHABLE
            Me.Grid.Cell(_row, m_grid_column_tito_type).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125)

          Case TITO_TICKET_TYPE.PROMO_REDEEM
            Me.Grid.Cell(_row, m_grid_column_tito_type).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2701, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3320))

          Case TITO_TICKET_TYPE.PROMO_NONREDEEM
            Me.Grid.Cell(_row, m_grid_column_tito_type).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2701, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3321))

          Case TITO_TICKET_TYPE.HANDPAY
            Me.Grid.Cell(_row, m_grid_column_tito_type).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2685)

          Case TITO_TICKET_TYPE.JACKPOT
            Me.Grid.Cell(_row, m_grid_column_tito_type).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2686)

          Case TITO_TICKET_TYPE.OFFLINE
            Me.Grid.Cell(_row, m_grid_column_tito_type).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3327)

        End Select
        Me.Grid.Cell(_row, m_grid_column_tito_amount).Value = GUI_FormatCurrency(m_subtotal_values(_idx))
        Me.Grid.Row(_row).BackColor() = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
        'calculates Total
        m_total_values(_idx) += m_subtotal_values(_idx)
      End If
    Next
  End Sub


  ' PURPOSE: Print Total row for each ticket type. If a ticket type has no value, it's not printed
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub PrintTotalRow()
    Dim _row As Integer
    Dim _idx As Integer

    For _idx = 0 To m_total_values.Length - 1
      If m_total_values(_idx) <> 0 Then
        Me.Grid.AddRow()
        _row = Me.Grid.NumRows - 1
        Me.Grid.Cell(_row, m_grid_column_tito_ticket_number).Value = GLB_NLS_GUI_INVOICING.GetString(205)
        Select Case _idx
          Case TITO_TICKET_TYPE.CASHABLE
            Me.Grid.Cell(_row, m_grid_column_tito_type).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125)

          Case TITO_TICKET_TYPE.PROMO_REDEEM
            Me.Grid.Cell(_row, m_grid_column_tito_type).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2701, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3320))

          Case TITO_TICKET_TYPE.PROMO_NONREDEEM
            Me.Grid.Cell(_row, m_grid_column_tito_type).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2701, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3321))

          Case TITO_TICKET_TYPE.HANDPAY
            Me.Grid.Cell(_row, m_grid_column_tito_type).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2685)

          Case TITO_TICKET_TYPE.JACKPOT
            Me.Grid.Cell(_row, m_grid_column_tito_type).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2686)

          Case TITO_TICKET_TYPE.OFFLINE
            Me.Grid.Cell(_row, m_grid_column_tito_type).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3327)
        End Select
        Me.Grid.Cell(_row, m_grid_column_tito_amount).Value = GUI_FormatCurrency(m_total_values(_idx))
        Me.Grid.Row(_row).BackColor() = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
      End If
    Next
  End Sub

  ' PURPOSE: Set the column order depending of grouped report or not.
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GetColumnsOrder()

    If chk_group_by.Checked Then

      If opt_date_creation.Checked Then

        m_grid_column_tito_ticket_number = GRID_COLUMN_TITO_BY_DATE_TICKET_NUMBER
        m_grid_column_tito_machine_number = GRID_COLUMN_TITO_BY_DATE_MACHINE_NUMBER
        m_grid_column_tito_type = GRID_COLUMN_TITO_BY_DATE_TYPE
        m_grid_column_tito_associated_promotion = GRID_COLUMN_TITO_BY_DATE_ASSOCIATED_PROMOTION
        m_grid_column_tito_amt0 = GRID_COLUMN_TITO_BY_DATE_AMT0
        m_grid_column_tito_amount = GRID_COLUMN_TITO_BY_DATE_AMOUNT
        m_grid_column_tito_state = GRID_COLUMN_TITO_BY_DATE_STATE
        m_grid_column_tito_creation_terminal = GRID_COLUMN_TITO_BY_DATE_CREATION_TERMINAL
        m_grid_column_tito_user_creation = GRID_COLUMN_TITO_BY_DATE_USER_CREATION
        m_grid_column_tito_creation_date = GRID_COLUMN_TITO_BY_DATE_CREATION_DATE
        m_grid_column_tito_cancel_or_redeem_terminal = GRID_COLUMN_TITO_BY_DATE_CANCEL_OR_REDEEM_TERMINAL
        m_grid_column_tito_cancel_or_redeem_date = GRID_COLUMN_TITO_BY_DATE_CANCEL_OR_REDEEM_DATE
        m_grid_column_tito_user_cancel_or_redeem = GRID_COLUMN_TITO_BY_DATE_USER_CANCEL_OR_REDEEM
        m_grid_column_tito_expiration_date = GRID_COLUMN_TITO_BY_DATE_EXPIRATION_DATE

        Return
      ElseIf opt_terminal_creation.Checked Then

        m_grid_column_tito_ticket_number = GRID_COLUMN_TITO_BY_TERM_TICKET_NUMBER
        m_grid_column_tito_machine_number = GRID_COLUMN_TITO_BY_TERM_MACHINE_NUMBER
        m_grid_column_tito_type = GRID_COLUMN_TITO_BY_TERM_TYPE
        m_grid_column_tito_associated_promotion = GRID_COLUMN_TITO_BY_TERM_ASSOCIATED_PROMOTION
        m_grid_column_tito_amt0 = GRID_COLUMN_TITO_BY_TERM_AMT0
        m_grid_column_tito_amount = GRID_COLUMN_TITO_BY_TERM_AMOUNT
        m_grid_column_tito_state = GRID_COLUMN_TITO_BY_TERM_STATE
        m_grid_column_tito_creation_terminal = GRID_COLUMN_TITO_BY_TERM_CREATION_TERMINAL
        m_grid_column_tito_user_creation = GRID_COLUMN_TITO_BY_TERM_USER_CREATION
        m_grid_column_tito_creation_date = GRID_COLUMN_TITO_BY_TERM_CREATION_DATE
        m_grid_column_tito_cancel_or_redeem_terminal = GRID_COLUMN_TITO_BY_TERM_CANCEL_OR_REDEEM_TERMINAL
        m_grid_column_tito_cancel_or_redeem_date = GRID_COLUMN_TITO_BY_TERM_CANCEL_OR_REDEEM_DATE
        m_grid_column_tito_user_cancel_or_redeem = GRID_COLUMN_TITO_BY_TERM_USER_CANCEL_OR_REDEEM
        m_grid_column_tito_expiration_date = GRID_COLUMN_TITO_BY_TERM_EXPIRATION_DATE

        Return
      End If

    End If

    m_grid_column_tito_ticket_number = GRID_COLUMN_TITO_NO_GROUP_TICKET_NUMBER
    m_grid_column_tito_machine_number = GRID_COLUMN_TITO_NO_GROUP_MACHINE_NUMBER
    m_grid_column_tito_type = GRID_COLUMN_TITO_NO_GROUP_TYPE
    m_grid_column_tito_associated_promotion = GRID_COLUMN_TITO_NO_GROUP_ASSOCIATED_PROMOTION
    m_grid_column_tito_amt0 = GRID_COLUMN_TITO_NO_GROUP_AMT0
    m_grid_column_tito_amount = GRID_COLUMN_TITO_NO_GROUP_AMOUNT
    m_grid_column_tito_state = GRID_COLUMN_TITO_NO_GROUP_STATE
    m_grid_column_tito_creation_terminal = GRID_COLUMN_TITO_NO_GROUP_CREATION_TERMINAL
    m_grid_column_tito_user_creation = GRID_COLUMN_TITO_NO_GROUP_USER_CREATION
    m_grid_column_tito_creation_date = GRID_COLUMN_TITO_NO_GROUP_CREATION_DATE
    m_grid_column_tito_cancel_or_redeem_terminal = GRID_COLUMN_TITO_NO_GROUP_CANCEL_OR_REDEEM_TERMINAL
    m_grid_column_tito_cancel_or_redeem_date = GRID_COLUMN_TITO_NO_GROUP_CANCEL_OR_REDEEM_DATE
    m_grid_column_tito_user_cancel_or_redeem = GRID_COLUMN_TITO_NO_GROUP_USER_CANCEL_OR_REDEEM
    m_grid_column_tito_expiration_date = GRID_COLUMN_TITO_NO_GROUP_EXPIRATION_DATE

  End Sub

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    Call GetColumnsOrder()

    With Me.Grid
      Call .Init(GRID_COLUMNS_COUNT, GRID_HEADERS_COUNT)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' EMPTY COLUMN 
      Call StyleSheetEmptyColumnSelector(GRID_HEADERS_COUNT)

      ' TICKET NUMBER
      .Column(m_grid_column_tito_ticket_number).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2109)
      .Column(m_grid_column_tito_ticket_number).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2110)
      .Column(m_grid_column_tito_ticket_number).Width = GRID_WIDTH_TITO_TICKET_NUMBER
      .Column(m_grid_column_tito_ticket_number).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(m_grid_column_tito_ticket_number).ColDataType = uc_grid.CLASS_COL_DATA.ENUM_COLDATATYPE.FLEX_DT_STRING

      ' MACHINE NUMBER
      .Column(m_grid_column_tito_machine_number).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2109)
      .Column(m_grid_column_tito_machine_number).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2955)
      .Column(m_grid_column_tito_machine_number).Width = GRID_WIDTH_TITO_MACHINE_NUMBER
      .Column(m_grid_column_tito_machine_number).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' TYPE
      .Column(m_grid_column_tito_type).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2109)
      .Column(m_grid_column_tito_type).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2112)
      .Column(m_grid_column_tito_type).Width = GRID_WIDTH_TITO_TYPE
      .Column(m_grid_column_tito_type).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' ASSOCIATED PROMOTION
      .Column(m_grid_column_tito_associated_promotion).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2109)
      .Column(m_grid_column_tito_associated_promotion).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2122)
      .Column(m_grid_column_tito_associated_promotion).Width = GRID_WIDTH_TITO_ASSOCIATED_PROMOTION
      .Column(m_grid_column_tito_associated_promotion).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' AMT0
      .Column(m_grid_column_tito_amt0).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2109)
      If (WSI.Common.Misc.IsFloorDualCurrencyEnabled) Then
        .Column(m_grid_column_tito_amt0).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7104) 'Created amount
        .Column(m_grid_column_tito_amt0).Width = GRID_WIDTH_TITO_AMT0
        .Column(m_grid_column_tito_amt0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Else
        .Column(m_grid_column_tito_amt0).Header(1).Text = "" 'Created amount
        .Column(m_grid_column_tito_amt0).Width = 0 'GRID_WIDTH_TITO_AMT0
      End If

      ' AMOUNT
      .Column(m_grid_column_tito_amount).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2109)
      .Column(m_grid_column_tito_amount).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3372)
      .Column(m_grid_column_tito_amount).Width = GRID_WIDTH_TITO_AMOUNT
      .Column(m_grid_column_tito_amount).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' STATE 
      .Column(m_grid_column_tito_state).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2109)
      .Column(m_grid_column_tito_state).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2117)
      .Column(m_grid_column_tito_state).Width = GRID_WIDTH_TITO_STATE
      .Column(m_grid_column_tito_state).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' GENERATION TERMINAL
      .Column(m_grid_column_tito_creation_terminal).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2108)
      .Column(m_grid_column_tito_creation_terminal).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2106)
      .Column(m_grid_column_tito_creation_terminal).Width = GRID_WIDTH_TITO_CREATION_TERMINAL
      .Column(m_grid_column_tito_creation_terminal).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' CREATION USER
      .Column(m_grid_column_tito_user_creation).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2108)
      .Column(m_grid_column_tito_user_creation).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2107)
      .Column(m_grid_column_tito_user_creation).Width = GRID_WIDTH_TITO_USER_CREATION
      .Column(m_grid_column_tito_user_creation).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' CREATION DATE
      .Column(m_grid_column_tito_creation_date).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2108)
      .Column(m_grid_column_tito_creation_date).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2118)
      .Column(m_grid_column_tito_creation_date).Width = GRID_WIDTH_TITO_CREATION_DATE
      .Column(m_grid_column_tito_creation_date).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' CANCEL/REDEEM TERMINAL
      .Column(m_grid_column_tito_cancel_or_redeem_terminal).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2120)
      .Column(m_grid_column_tito_cancel_or_redeem_terminal).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2106)
      .Column(m_grid_column_tito_cancel_or_redeem_terminal).Width = GRID_WIDTH_TITO_CANCEL_OR_REDEEM_TERMINAL
      .Column(m_grid_column_tito_cancel_or_redeem_terminal).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' DATE REDEEMED/CANCELED
      .Column(m_grid_column_tito_cancel_or_redeem_date).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2120)
      .Column(m_grid_column_tito_cancel_or_redeem_date).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2118)
      .Column(m_grid_column_tito_cancel_or_redeem_date).Width = GRID_WIDTH_TITO_CANCEL_OR_REDEEM_DATE
      .Column(m_grid_column_tito_cancel_or_redeem_date).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' CANCEL/REDEEM USER 
      .Column(m_grid_column_tito_user_cancel_or_redeem).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2116)
      .Column(m_grid_column_tito_user_cancel_or_redeem).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2120)
      .Column(m_grid_column_tito_user_cancel_or_redeem).Width = 0
      .Column(m_grid_column_tito_user_cancel_or_redeem).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' DATE EXPIRATION
      .Column(m_grid_column_tito_expiration_date).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2123)
      .Column(m_grid_column_tito_expiration_date).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2118)
      .Column(m_grid_column_tito_expiration_date).Width = GRID_WIDTH_TITO_EXPIRATION_DATE
      .Column(m_grid_column_tito_expiration_date).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With

  End Sub

  ' PURPOSE: Set default values to the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub SetDefaultValues()
    Dim _today_opening As Date

    _today_opening = WSI.Common.Misc.TodayOpening()

    ' Dates
    gb_date_filter.Enabled = True
    ds_from_to.FromDate = _today_opening
    ds_from_to.FromDateSelected = True
    ds_from_to.ToDate = _today_opening.AddDays(1)
    ds_from_to.ToDateSelected = True
    ds_from_to.ClosingTime = _today_opening.Hour
    opt_creation.Checked = True
    opt_cancelation.Checked = False
    opt_expiration.Checked = False

    ' Ticket
    ef_ticket_number.Value = ""
    chk_redeemable_ticket.Checked = False
    chk_promo_redeem_ticket.Checked = False
    chk_promo_nonredeem_ticket.Checked = False
    chk_jackpot.Checked = False
    chk_handpay.Checked = False
    chk_offline.Checked = False
    chk_redeemable_ticket.Enabled = True
    chk_promo_redeem_ticket.Enabled = True
    chk_promo_nonredeem_ticket.Enabled = True
    chk_jackpot.Enabled = True
    chk_handpay.Enabled = True
    chk_offline.Enabled = True

    ' Status
    gb_state.Enabled = True
    chk_valid.Checked = False
    chk_expired.Checked = False
    chk_pending_print.Checked = False
    chk_redeemed.Checked = False
    chk_expired_redeemed.Checked = False
    chk_discarded.Checked = False
    chk_canceled.Checked = False
    chk_pending_cancel.Checked = False
    chk_pending_payment.Checked = False
    chk_pending_payment_after_expiration.Checked = False
    chk_swap_chips.Checked = False 'EOR 08-MAR-2016 Reset CheckBox Swap Chips

    gb_machine_type.Enabled = True
    ' Cashier
    gb_cashiers.Enabled = True
    chk_cashiers.Checked = True
    Call UncheckAllCahiers()

    ' Terminals
    uc_terminals_list.Enabled = True
    chk_terminals.Checked = True
    Call Me.uc_terminals_list.SetDefaultValues()

    ' Options
    gb_group_by.Enabled = True
    chk_group_by.Checked = False
    chk_detailed_report.Checked = True
    chk_detailed_report.Enabled = False
    opt_date_creation.Checked = True
    opt_date_creation.Enabled = False
    opt_terminal_creation.Enabled = False
    opt_terminal_creation.Checked = False

    ' Account
    uc_account_sel.Enabled = True
    Call Me.uc_account_sel.Clear()

  End Sub ' SetDefaultValues

  ' PURPOSE: Get the sql 'where' statement
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - an string with the 'where' statement
  Private Function GetSqlWhere() As String

    Dim _str_where As String
    Dim _terminals_list As String
    Dim _state As String
    Dim _ticket_type As String
    Dim _associated_promotion As String
    Dim _cashiers_list As String
    Dim _cashiers_and_terminals As String
    Dim _str_where_account As String
    Dim _str_temp As String
    Dim _selected_terminals As Long()
    Dim _date_selected As Boolean
    Dim _ticket_number As Int64

    _str_where = ""
    _state = ""
    _ticket_type = ""
    _terminals_list = ""
    _associated_promotion = ""
    _cashiers_list = ""
    _cashiers_and_terminals = ""
    _date_selected = ds_from_to.FromDateSelected Or ds_from_to.ToDateSelected
    _ticket_number = 0

    'if there is an especific validation number, the rest of filter are discarded
    If ef_ticket_number.TextValue.Trim <> "" AndAlso Int64.TryParse(ef_ticket_number.Value, _ticket_number) Then

      Return " WHERE TI_VALIDATION_NUMBER = " & _ticket_number.ToString()
    End If

    'date filter
    If opt_creation.Checked Then
      _str_where = ds_from_to.GetSqlFilterCondition("TI_CREATED_DATETIME")
    End If

    If opt_cancelation.Checked AndAlso _date_selected Then
      If _str_where.Length > 0 Then
        _str_where = _str_where & " OR " & ds_from_to.GetSqlFilterCondition("TI_LAST_ACTION_DATETIME")
      Else
        _str_where = ds_from_to.GetSqlFilterCondition("TI_LAST_ACTION_DATETIME")
      End If
    End If

    If opt_expiration.Checked AndAlso _date_selected Then
      If _str_where.Length > 0 Then
        _str_where = _str_where & " OR " & ds_from_to.GetSqlFilterCondition("TI_EXPIRATION_DATETIME")
      Else
        _str_where = ds_from_to.GetSqlFilterCondition("TI_EXPIRATION_DATETIME")
      End If
    End If

    If _str_where.Length > 0 Then
      _str_where = "(" & _str_where & ") "
    End If

    'State filters
    If chk_valid.Checked Then
      _state = " TI_STATUS = " & WSI.Common.TITO_TICKET_STATUS.VALID
    End If

    If chk_canceled.Checked Then
      If _state.Length > 0 Then
        _state = _state & " OR TI_STATUS = " & WSI.Common.TITO_TICKET_STATUS.CANCELED
      Else
        _state = _state & " TI_STATUS = " & WSI.Common.TITO_TICKET_STATUS.CANCELED
      End If
    End If

    If chk_pending_cancel.Checked Then
      If _state.Length > 0 Then
        _state = _state & " OR TI_STATUS = " & WSI.Common.TITO_TICKET_STATUS.PENDING_CANCEL
      Else
        _state = _state & " TI_STATUS = " & WSI.Common.TITO_TICKET_STATUS.PENDING_CANCEL
      End If
    End If

    If chk_redeemed.Checked Then
      If _state.Length > 0 Then
        _state = _state & " OR TI_STATUS = " & WSI.Common.TITO_TICKET_STATUS.REDEEMED
      Else
        _state = _state & " TI_STATUS = " & WSI.Common.TITO_TICKET_STATUS.REDEEMED
      End If
    End If

    If chk_expired_redeemed.Checked Then
      If _state.Length > 0 Then
        _state = _state & " OR TI_STATUS = " & WSI.Common.TITO_TICKET_STATUS.PAID_AFTER_EXPIRATION
      Else
        _state = _state & " TI_STATUS = " & WSI.Common.TITO_TICKET_STATUS.PAID_AFTER_EXPIRATION
      End If
    End If

    If chk_expired.Checked Then
      If _state.Length > 0 Then
        _state = _state & " OR TI_STATUS = " & WSI.Common.TITO_TICKET_STATUS.EXPIRED
      Else
        _state = _state & " TI_STATUS = " & WSI.Common.TITO_TICKET_STATUS.EXPIRED
      End If
    End If

    If chk_pending_print.Checked Then
      If _state.Length > 0 Then
        _state = _state & " OR TI_STATUS = " & WSI.Common.TITO_TICKET_STATUS.PENDING_PRINT
      Else
        _state = _state & " TI_STATUS = " & WSI.Common.TITO_TICKET_STATUS.PENDING_PRINT
      End If
    End If

    If chk_discarded.Checked Then
      If _state.Length > 0 Then
        _state = _state & " OR TI_STATUS = " & WSI.Common.TITO_TICKET_STATUS.DISCARDED
      Else
        _state = _state & " TI_STATUS = " & WSI.Common.TITO_TICKET_STATUS.DISCARDED
      End If
    End If

    'EOR 08-MAR-2016 Filter for Swap Chips
    If chk_swap_chips.Checked Then
      If _state.Length > 0 Then
        _state = _state & " OR TI_STATUS = " & WSI.Common.TITO_TICKET_STATUS.SWAP_FOR_CHIPS
      Else
        _state = _state & " TI_STATUS = " & WSI.Common.TITO_TICKET_STATUS.SWAP_FOR_CHIPS
      End If
    End If

    If chk_pending_payment.Checked Then
      If _state.Length > 0 Then
        _state = _state & " OR TI_STATUS = " & TITO_TICKET_STATUS.PENDING_PAYMENT
      Else
        _state = " TI_STATUS = " & TITO_TICKET_STATUS.PENDING_PAYMENT
      End If
    End If

    If chk_pending_payment_after_expiration.Checked Then
      If _state.Length > 0 Then
        _state = _state & " OR TI_STATUS = " & TITO_TICKET_STATUS.PENDING_PAYMENT_AFTER_EXPIRATION
      Else
        _state = " TI_STATUS = " & TITO_TICKET_STATUS.PENDING_PAYMENT_AFTER_EXPIRATION
      End If
    End If

    If _state.Length > 0 Then
      _str_where = (IIf(_str_where.Length > 0, _str_where & " AND ", _str_where))
      _str_where = _str_where & "(" & _state & ") "
    End If

    'Ticket Type filters
    If chk_redeemable_ticket.Checked Then
      If _ticket_type.Length > 0 Then
        _ticket_type = _ticket_type & " OR TI_TYPE_ID = " & TITO_TICKET_TYPE.CASHABLE
      Else
        _ticket_type = _ticket_type & " TI_TYPE_ID = " & TITO_TICKET_TYPE.CASHABLE
      End If
    End If

    If chk_promo_redeem_ticket.Checked Then
      If _ticket_type.Length > 0 Then
        _ticket_type = _ticket_type & " OR TI_TYPE_ID = " & TITO_TICKET_TYPE.PROMO_REDEEM
      Else
        _ticket_type = " TI_TYPE_ID = " & TITO_TICKET_TYPE.PROMO_REDEEM
      End If
    End If

    If chk_promo_nonredeem_ticket.Checked Then
      If _ticket_type.Length > 0 Then
        _ticket_type = _ticket_type & " OR TI_TYPE_ID = " & TITO_TICKET_TYPE.PROMO_NONREDEEM
      Else
        _ticket_type = " TI_TYPE_ID = " & TITO_TICKET_TYPE.PROMO_NONREDEEM
      End If
    End If

    If chk_handpay.Checked Then
      If _ticket_type.Length > 0 Then
        _ticket_type = _ticket_type & " OR TI_TYPE_ID = " & TITO_TICKET_TYPE.HANDPAY
      Else
        _ticket_type = " TI_TYPE_ID = " & TITO_TICKET_TYPE.HANDPAY
      End If
    End If

    If chk_jackpot.Checked Then
      If _ticket_type.Length > 0 Then
        _ticket_type = _ticket_type & " OR TI_TYPE_ID = " & TITO_TICKET_TYPE.JACKPOT
      Else
        _ticket_type = " TI_TYPE_ID = " & TITO_TICKET_TYPE.JACKPOT
      End If
    End If

    If chk_offline.Checked Then
      If _ticket_type.Length > 0 Then
        _ticket_type = _ticket_type & " OR TI_TYPE_ID = " & TITO_TICKET_TYPE.OFFLINE
      Else
        _ticket_type = " TI_TYPE_ID = " & TITO_TICKET_TYPE.OFFLINE
      End If
    End If

    If _ticket_type.Length > 0 Then
      _str_where = (IIf(_str_where.Length > 0, _str_where & " AND ", _str_where))
      _str_where = _str_where & "(" & _ticket_type & ") "
    End If

    'filter cashiers
    If m_search_cashiers Then
      _cashiers_list = GetCashiersSelected(GRID_CASHIER_ID)

      If _cashiers_list.Length > 0 Then

        _cashiers_and_terminals = IIf(m_created, " TI_CREATED_TERMINAL_ID IN ( " & _cashiers_list & ") ", String.Empty) & _
                                  IIf(m_created And m_cancelled, " OR ", String.Empty) & _
                                  IIf(m_cancelled, " TI_LAST_ACTION_TERMINAL_ID IN  ( " & _cashiers_list & ") ", String.Empty)

      Else

        ' All cashiers by type
        _cashiers_and_terminals = IIf(m_created, " TI_CREATED_TERMINAL_TYPE IN (0,2) ", String.Empty) & _
                                  IIf(m_created And m_cancelled, " OR ", String.Empty) & _
                                  IIf(m_cancelled, " TI_LAST_ACTION_TERMINAL_TYPE = 0 ", String.Empty)

      End If

    End If

    'filter terminals
    If m_search_terminals Then
      _selected_terminals = Me.uc_terminals_list.GetTerminalIdListSelected()

      If Not Me.uc_terminals_list.AllSelectedChecked AndAlso Me.uc_terminals_list.TerminalListHasValues Then

        For Each _terminal_id As Long In _selected_terminals
          _terminals_list &= _terminal_id & ","
        Next

        _terminals_list = _terminals_list.Substring(0, (_terminals_list.Length - 1))

        _cashiers_and_terminals &= IIf(_cashiers_and_terminals.Length > 0, " OR ", String.Empty) & _
                                   IIf(m_created, " TI_CREATED_TERMINAL_ID IN ( " & _terminals_list & ") ", String.Empty) & _
                                   IIf(m_created And m_cancelled, " OR  ", String.Empty) & _
                                   IIf(m_cancelled, " TI_LAST_ACTION_TERMINAL_ID IN ( " & _terminals_list & ") ", String.Empty)
      Else
        ' All terminals by type
        _cashiers_and_terminals &= IIf(_cashiers_and_terminals.Length > 0, " OR ", String.Empty) & _
                                  IIf(m_created, " TI_CREATED_TERMINAL_TYPE = 1 ", String.Empty) & _
                                  IIf(m_created And m_cancelled, " OR ", String.Empty) & _
                                  IIf(m_cancelled, " TI_LAST_ACTION_TERMINAL_TYPE = 1 ", String.Empty)

      End If

    End If

    ' Group the cashiers and terminals filters
    If (_cashiers_and_terminals.Length > 0) Then
      _cashiers_and_terminals = "(" & _cashiers_and_terminals & ")"
    Else
      ' If no data in filter assume to show all cashiers and terminals with valid types (0, 1)
      If m_search_cashiers And m_search_terminals Then
        _cashiers_and_terminals = IIf(m_created, " TI_CREATED_TERMINAL_TYPE IN (0, 1) ", String.Empty) & _
                                   IIf(m_created And m_cancelled, " OR ", String.Empty) & _
                                   IIf(m_cancelled, " TI_LAST_ACTION_TERMINAL_TYPE IN (0, 1) ", String.Empty)
      End If

    End If

    _str_where_account = Nothing

    ' JAB 14-11-2013: Using "InitExtendedQuery" function to show Accounts with ac_type equal to 'ACCOUNT_VIRTUAL_CASHIER' or 'ACCOUNT_VIRTUAL_TERMINAL'
    Me.uc_account_sel.InitExtendedQuery(True)
    _str_where_account = Me.uc_account_sel.GetFilterSQL()

    If Not String.IsNullOrEmpty(_str_where_account) Then

      _str_where &= IIf(_str_where.Length > 0, " AND ( ", " ( ")
      _str_temp = _str_where_account.Replace("AC_ACCOUNT_ID", "ACCOUNTS_CREATION.AC_ACCOUNT_ID")
      _str_temp = _str_temp.Replace("AC_TRACK_DATA", "ACCOUNTS_CREATION.AC_TRACK_DATA")
      _str_temp = _str_temp.Replace("AC_HOLDER_NAME", "ACCOUNTS_CREATION.AC_HOLDER_NAME")
      _str_temp = _str_temp.Replace("AC_TYPE", "ACCOUNTS_CREATION.AC_TYPE")
      _str_where &= _str_temp
      _str_where &= ") "
    End If

    If _cashiers_and_terminals.Length > 0 Then
      _str_where = IIf(_str_where.Length > 0, _str_where & "AND (" & _cashiers_and_terminals & ") ", _str_where & " (" & _cashiers_and_terminals & ") ")
    End If

    Return IIf(_str_where.Length > 0, _str_where.Insert(0, " Where "), "")

  End Function


  Private Sub GUI_GetSqlQueryCashiers()

    Dim _sb As StringBuilder
    Dim _adapter As SqlDataAdapter
    Dim _table As DataTable

    _adapter = New SqlDataAdapter()
    _table = New DataTable()
    _sb = New StringBuilder()

    Try
      Using _db_trx As DB_TRX = New DB_TRX()
        _sb.AppendLine("  SELECT   CT_CASHIER_ID              ")
        _sb.AppendLine("          ,CT_NAME                    ")
        _sb.AppendLine("    FROM   CASHIER_TERMINALS          ")
        _sb.AppendLine("   WHERE   CT_TERMINAL_ID IS NULL     ")
        _sb.AppendLine("     AND   CT_GAMING_TABLE_ID IS NULL ")
        _sb.AppendLine("ORDER BY   CT_NAME                    ")
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _adapter = New SqlDataAdapter(_cmd)
          _adapter.Fill(_table)
          Call GUI_SetupRowsCashiersGrid(_table)
        End Using
      End Using
    Catch ex As Exception
    End Try
  End Sub

  Private Sub UncheckAllCahiers()

    Dim idx_rows As Integer

    For idx_rows = 0 To dg_cashiers.NumRows - 1
      dg_cashiers.Cell(idx_rows, GRID_CASHIER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next

  End Sub

#End Region

#Region "Private methods"

  ' PURPOSE: Layout the empty column selector
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub StyleSheetEmptyColumnSelector(Optional ByVal HeaderRows As Integer = 1)

    With Me.Grid

      ' Empty column
      For _Cnt As Integer = 0 To HeaderRows
        .Column(GRID_COLUMN_SELECTOR).Header(_Cnt).Text = String.Empty
      Next _Cnt
      .Column(GRID_COLUMN_SELECTOR).Width = GRID_WIDTH_SELECTOR
      .Column(GRID_COLUMN_SELECTOR).IsColumnPrintable = False
      .Column(GRID_COLUMN_SELECTOR).HighLightWhenSelected = False
    End With

  End Sub

  ' PURPOSE : Enable/Disable the filters
  '
  '  PARAMS :
  '     - INPUT : Boolean indicating if it must enable (true) or disable (false) the filters
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  '     - 
  Private Sub EnableFilters(ByVal EnableFilters As Boolean)
    Me.chk_redeemable_ticket.Enabled = EnableFilters
    Me.chk_promo_redeem_ticket.Enabled = EnableFilters
    Me.chk_promo_nonredeem_ticket.Enabled = EnableFilters
    Me.chk_jackpot.Enabled = EnableFilters
    Me.chk_handpay.Enabled = EnableFilters
    Me.chk_offline.Enabled = EnableFilters
    Me.gb_date_filter.Enabled = EnableFilters
    Me.gb_group_by.Enabled = EnableFilters
    Me.gb_state.Enabled = EnableFilters
    Me.gb_machine_type.Enabled = EnableFilters
    Me.gb_cashiers.Enabled = EnableFilters
    Me.uc_terminals_list.Enabled = EnableFilters
    Me.uc_account_sel.Enabled = EnableFilters

    'If Not EnableFilters Then
    '  Me.dg_cashiers.ClearSelection()
    '  Me.UncheckAllCahiers()
    '  Me.uc_pr_list.SetDefaultValues()
    'End If

  End Sub ' EnableFilters

#End Region

#Region "Public Functions"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE: Define all Grid Account Alias columns
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheetCashiers()

    With Me.dg_cashiers
      Call .Init(GRID_CASHIER_COLUMNS, GRID_CASHIER_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' Hidden colums
      .Column(GRID_CASHIER_ID).Header(1).Text = ""
      .Column(GRID_CASHIER_ID).WidthFixed = 0

      ' GRID_COL_CHECKBOX
      .Column(GRID_CASHIER_COLUMN_CHECKED).Header(1).Text = ""
      .Column(GRID_CASHIER_COLUMN_CHECKED).WidthFixed = GRID_CASHIER_WIDTH_COLUMN_CHECKED
      .Column(GRID_CASHIER_COLUMN_CHECKED).Fixed = True
      .Column(GRID_CASHIER_COLUMN_CHECKED).Editable = True
      .Column(GRID_CASHIER_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_CASHIER_COLUMN_DESCRIPTION).Header(1).Text = ""
      .Column(GRID_CASHIER_COLUMN_DESCRIPTION).WidthFixed = GRID_CASHIER_WIDTH_COLUMN_DESCRIPTION
      .Column(GRID_CASHIER_COLUMN_DESCRIPTION).Fixed = True
      .Column(GRID_CASHIER_COLUMN_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub

  ' PURPOSE: Fill the grid with the bank account alias
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_SetupRowsCashiersGrid(ByVal Table As DataTable)

    Dim _actual_row As Int32

    _actual_row = 0

    For Each r As DataRow In Table.Rows
      dg_cashiers.AddRow()
      Me.dg_cashiers.Cell(_actual_row, 0).Value = r(0)
      Me.dg_cashiers.Cell(_actual_row, 2).Value = r(1)

      _actual_row += 1
    Next

  End Sub ' GUI_SetupRowCashierGrid

  ' PURPOSE: get the cashiers selected
  '
  '  PARAMS:
  '     - INPUT:
  '           - ColumnNeeded : It could be the Id column or alias column
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - it could be a list of Ids or a list of alias
  Private Function GetCashiersSelected(ByVal ColumnNeeded As Int16) As String

    Dim _idx_row As Integer
    Dim _bank_list As String
    Dim _all As Boolean
    Dim _num_elements As Integer

    _bank_list = ""
    _all = True
    _num_elements = 0

    For _idx_row = 0 To Me.dg_cashiers.NumRows - 1
      If dg_cashiers.Cell(_idx_row, GRID_CASHIER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        If _bank_list.Length = 0 Then
          _bank_list = dg_cashiers.Cell(_idx_row, ColumnNeeded).Value
        Else
          _bank_list = _bank_list & ", " & dg_cashiers.Cell(_idx_row, ColumnNeeded).Value
        End If
        _num_elements += 1
      Else
        _all = False
      End If

    Next

    If _all = True Then
      If ColumnNeeded = GRID_CASHIER_COLUMN_DESCRIPTION Then
        _bank_list = GLB_NLS_GUI_ALARMS.GetString(277)
      Else
        _bank_list = ""
      End If
    Else
      If ColumnNeeded = GRID_CASHIER_COLUMN_DESCRIPTION AndAlso _num_elements > MAX_FILTER_REPORT Then
        _bank_list = GLB_NLS_GUI_CONTROLS.GetString(291)
      End If
    End If

    Return _bank_list

  End Function ' GetCashiersSelected

#End Region

#Region "Events"

  Private Sub bt_pick_all_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_check_all.Click
    Dim idx_rows As Integer

    For idx_rows = 0 To dg_cashiers.NumRows - 1
      dg_cashiers.Cell(idx_rows, GRID_CASHIER_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    Next

  End Sub


  Private Sub bt_uncheck_all_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_uncheck_all.Click

    Call UncheckAllCahiers()

  End Sub

  Private Sub chb_cashier_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_cashiers.CheckedChanged

    If chk_cashiers.Checked Then

      Me.dg_cashiers.Enabled = True
      Me.btn_check_all.Enabled = True
      Me.btn_uncheck_all.Enabled = True
      Me.gb_cashiers.Enabled = True

    Else

      Me.dg_cashiers.Enabled = False
      Me.btn_uncheck_all.Enabled = False
      Me.btn_check_all.Enabled = False
      Me.gb_cashiers.Enabled = False

      Call UncheckAllCahiers()

    End If

  End Sub

  Private Sub chb_terminals_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminals.CheckedChanged

    If chk_terminals.Checked Then
      Me.uc_terminals_list.Enabled = True
    Else
      Me.uc_terminals_list.Enabled = False
    End If

  End Sub

  Private Sub chk_group_by_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_group_by.CheckedChanged

    ' Enable or dissable options 
    opt_date_creation.Enabled = chk_group_by.Checked
    opt_terminal_creation.Enabled = chk_group_by.Checked
    ' Enable or dissable Detailed report option
    chk_detailed_report.Enabled = chk_group_by.Checked
    EnableTicketAudit()
  End Sub

  Private Sub opt_expiration_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_expiration.CheckedChanged
    If opt_expiration.Checked Then
      chk_cashiers.Checked = False
      chk_cashiers.Enabled = False
      chk_terminals.Checked = False
      chk_terminals.Enabled = False
    Else
      chk_cashiers.Enabled = True
      chk_terminals.Enabled = True
    End If
  End Sub

  Private Sub opt_cancelation_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_cancelation.CheckedChanged
    If opt_cancelation.Checked Then
      chk_valid.Enabled = False
      chk_expired.Enabled = False
      chk_valid.Checked = False
      chk_expired.Checked = False
    Else
      chk_valid.Enabled = True
      chk_expired.Enabled = True
    End If
  End Sub

  Private Sub ef_ticket_number_Changed() Handles ef_ticket_number.EntryFieldValueChanged
    Dim _enable_filters As Boolean

    _enable_filters = String.IsNullOrEmpty(ef_ticket_number.TextValue)

    Call EnableFilters(_enable_filters)

  End Sub

#End Region

  Private Sub frm_tito_reports_sel_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
    KeyboardMessageFilter.RegisterHandler(Me, BarcodeType.FilterAnyBarcode, m_barcode_handler)
  End Sub

  Private Sub chk_detailed_report_CheckedChanged(sender As Object, e As EventArgs) Handles chk_detailed_report.CheckedChanged
    EnableTicketAudit()
  End Sub

  Private Sub EnableTicketAudit()
    Dim _result As Boolean
    _result = (chk_group_by.Checked And chk_detailed_report.Checked) Or (Not chk_group_by.Checked)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = _result
  End Sub




End Class