<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_tito_terminals_configuration
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.uc_terminals = New GUI_Controls.uc_provider
    Me.gb_status = New System.Windows.Forms.GroupBox
    Me.chk_retired = New System.Windows.Forms.CheckBox
    Me.chk_inactive = New System.Windows.Forms.CheckBox
    Me.chk_active = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Controls.Add(Me.uc_terminals)
    Me.panel_filter.Size = New System.Drawing.Size(848, 190)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_terminals, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 194)
    Me.panel_data.Size = New System.Drawing.Size(848, 294)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(842, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(842, 4)
    '
    'uc_terminals
    '
    Me.uc_terminals.Location = New System.Drawing.Point(282, 3)
    Me.uc_terminals.Name = "uc_terminals"
    Me.uc_terminals.Size = New System.Drawing.Size(338, 194)
    Me.uc_terminals.TabIndex = 11
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.chk_retired)
    Me.gb_status.Controls.Add(Me.chk_inactive)
    Me.gb_status.Controls.Add(Me.chk_active)
    Me.gb_status.Location = New System.Drawing.Point(6, 6)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(275, 94)
    Me.gb_status.TabIndex = 13
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'chk_retired
    '
    Me.chk_retired.AutoSize = True
    Me.chk_retired.Location = New System.Drawing.Point(6, 67)
    Me.chk_retired.Name = "chk_retired"
    Me.chk_retired.Size = New System.Drawing.Size(74, 17)
    Me.chk_retired.TabIndex = 2
    Me.chk_retired.Text = "xRetired"
    Me.chk_retired.UseVisualStyleBackColor = True
    '
    'chk_inactive
    '
    Me.chk_inactive.AutoSize = True
    Me.chk_inactive.Location = New System.Drawing.Point(7, 44)
    Me.chk_inactive.Name = "chk_inactive"
    Me.chk_inactive.Size = New System.Drawing.Size(79, 17)
    Me.chk_inactive.TabIndex = 1
    Me.chk_inactive.Text = "xInactive"
    Me.chk_inactive.UseVisualStyleBackColor = True
    '
    'chk_active
    '
    Me.chk_active.AutoSize = True
    Me.chk_active.Location = New System.Drawing.Point(7, 21)
    Me.chk_active.Name = "chk_active"
    Me.chk_active.Size = New System.Drawing.Size(68, 17)
    Me.chk_active.TabIndex = 0
    Me.chk_active.Text = "xActive"
    Me.chk_active.UseVisualStyleBackColor = True
    '
    'frm_tito_terminals_configuration
    '
    Me.ClientSize = New System.Drawing.Size(856, 492)
    Me.Name = "frm_tito_terminals_configuration"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_terminals As GUI_Controls.uc_provider
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_retired As System.Windows.Forms.CheckBox
  Friend WithEvents chk_inactive As System.Windows.Forms.CheckBox
  Friend WithEvents chk_active As System.Windows.Forms.CheckBox

End Class
