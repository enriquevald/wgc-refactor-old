'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_bills_tickets_balance_report_sel
' DESCRIPTION:   .
' AUTHOR:        Humberto Braojos
' CREATION DATE: 21-06-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 13-AUG-2013  HBB    Initial version
' 24-OCT-2013  JRM    Turned form from base sel to base_print, added an extra grid organized in TabControl, clean up, refactoring
' 29-NOV-2013  NMR    Removed use of TI_XXX_CASHIER_SESSION
' 02-DEC-2013  LJM    Removed the override of GUI_ButtonClick. Not needed at all.
' 26-FEB-2014  JRM    Cleaned commented code from file
' 15-SEP-2015  DCS    Backlog Item 3707 - Coin Collection
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports WSI.Common
Imports System.Data.SqlClient
Imports WSI.Common.TITO

Public Class frm_tito_bills_tickets_balance_report_sel
  Inherits frm_base_print

#Region " Constants "
  ' Bills Grid Columns
  Private Const BILLS_GRID_COLUMN_SENTINEL As Integer = 0
  Private Const BILLS_GRID_COLUMN_TERMINAL As Integer = 1
  Private Const BILLS_GRID_COLUMN_FACE_VALUE As Integer = 2
  Private Const BILLS_GRID_COLUMN_NUM_COLLECTED As Integer = 3
  Private Const BILLS_GRID_COLUMN_SUM_COLLECTED As Integer = 4
  Private Const BILLS_GRID_COLUMN_NUM_EXPECTED As Integer = 5
  Private Const BILLS_GRID_COLUMN_SUM_EXPECTED As Integer = 6
  Private Const BILLS_GRID_COLUMN_NUM_MISMATCH As Integer = 7
  Private Const BILLS_GRID_COLUMN_SUM_MISMATCH As Integer = 8

  'Bills columuns totals
  Private Const BILLS_GRID_HEADER_ROWS As Integer = 2
  Private Const BILLS_GRID_COLUMNS As Integer = 9

  ' Tickets Grid Columns
  Private Const TICKET_GRID_COLUMN_SENTINEL As Integer = 0
  Private Const TICKET_GRID_COLUMN_TERMINAL As Integer = 1
  Private Const TICKET_GRID_COLUMN_TICKET_NUMBER As Integer = 2
  Private Const TICKET_GRID_COLUMN_SUM_EXPECTED As Integer = 3
  Private Const TICKET_GRID_COLUMN_NUM_MISMATCH As Integer = 4
  Private Const TICKET_GRID_COLUMN_SUM_MISMATCH As Integer = 5

  ' Tickets columns totals
  Private Const TICKET_GRID_HEADER_ROWS As Integer = 1
  Private Const TICKET_GRID_COLUMNS As Integer = 6

  ' Sql Columns
  Private Const SQL_COLUMN_TICKET_OR_BILL As Integer = 0
  Private Const SQL_COLUMN_TERMINAL_NAME = 1
  Private Const SQL_COLUMN_FACE_VALUE As Integer = 2
  Private Const SQL_COLUMN_THEORETICAL_COUNT As Integer = 3
  Private Const SQL_COLUMN_REAL_COUNT As Integer = 4
  Private Const SQL_COLUMN_TICKET_NUMBER As Integer = 5
  Private Const SQL_COLUMN_TICKET_COLLECTED As Int32 = 6
  Private Const SQL_COLUMN_TICKET_VALUE As Int32 = 7
  Private Const SQL_COLUMN_TICKET_THEORETICAL_SESSION = 8
  Private Const SQL_COLUMN_TICKET_REAL_SESSION = 9

  ' counters configuration
  Private Const COLOR_GAP_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  Private Const COLOR_GAP_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_PENDING_BACK = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
  Private Const COLOR_PENDING_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_COMPLETED_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_COMPLETED_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00

  ' counters
  Private Const COUNTER_COMPLETED As Integer = 1
  Private Const COUNTER_PENDING As Integer = 2
  Private Const COUNTER_GAP As Integer = 3
  Private Const MAX_COUNTERS As Integer = 4

#End Region ' Constants

#Region " Enums "
  Enum ENUM_TAB_MODE
    TICKETS = 1
    BILLS = 0
  End Enum

#End Region 'Enums

#Region " Members "

  Private m_cashier_session_id As Long
  Private m_cashier_session_name As String
  Private m_bills_counter_list(MAX_COUNTERS) As Integer
  Private m_tickets_counter_list(MAX_COUNTERS) As Integer
  Private m_collection_by_terminal As Boolean
  Private m_status As String
  Private m_first_time As Boolean = True
  Private m_sql_dataset As DataSet
  Private m_sql_conn As SqlConnection
  Private m_sql_adap As SqlDataAdapter
  Private m_sql_commandbuilder As System.Data.SqlClient.SqlCommandBuilder
  Private m_tab_mode As ENUM_TAB_MODE
  Private m_print_datetime As DateTime
  Private m_visible_header As Hashtable
  Private m_loaded_tables As Boolean()
  Private m_ds As DataSet
  Private m_tables_show As New ArrayList()
  Private m_excel_data As GUI_Reports.CLASS_EXCEL_DATA
  Private m_print_data As New GUI_Reports.CLASS_PRINT_DATA()
  Private m_data_grid As DataGrid

#End Region ' Members

#Region "Overrides Functions"

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Notes gap report
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2871)

    ' Buttons()
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_EXCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(27)
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_PRINT).Visible = False
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_EXCEL).Enabled = True
    Me.GUI_Button(frm_base_print.ENUM_BUTTON.BUTTON_PRINT).Enabled = True

    ' Status
    Me.gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1971)
    Me.chk_completed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1972)
    Me.chk_pending.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1973)
    Me.chk_gap.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1974)

    Me.lbl_color_completed.BackColor = GetColor(COLOR_COMPLETED_BACK)
    Me.lbl_color_pending.BackColor = GetColor(COLOR_PENDING_BACK)
    Me.lbl_color_gap.BackColor = GetColor(COLOR_GAP_BACK)

    ' tabs
    Me.tab_tickets.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2269)
    Me.tab_bills.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2266)

    ' Set cashier session name
    Me.lbl_cs_name.Value = m_cashier_session_name

    ' Define grid columns for both datagrids
    Call GUI_StyleSheetForTickets()
    Call GUI_StyleSheetForBills()

    ' Set filter default values
    Call SetDefaultValues()

    ' Performs a first search
    Call GUI_ExecuteQuery()

    m_first_time = False

  End Sub ' GUI_InitControl

  ' PURPOSE: Perform final processing for the grid data
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Sub GUI_AfterLastRow()

    'Update(counters)
    dg_tickets.Counter(COUNTER_COMPLETED).Value = m_tickets_counter_list(COUNTER_COMPLETED)
    dg_tickets.Counter(COUNTER_PENDING).Value = m_tickets_counter_list(COUNTER_PENDING)
    dg_tickets.Counter(COUNTER_GAP).Value = m_tickets_counter_list(COUNTER_GAP)

    dg_bills.Counter(COUNTER_COMPLETED).Value = m_bills_counter_list(COUNTER_COMPLETED)
    dg_bills.Counter(COUNTER_PENDING).Value = m_bills_counter_list(COUNTER_PENDING)
    dg_bills.Counter(COUNTER_GAP).Value = m_bills_counter_list(COUNTER_GAP)

  End Sub ' GUI_AfterLastRow

  Protected Function GUI_FilterGetSqlQuery() As String
    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    _sb.AppendLine("    SELECT   0                                                                                     ") ' this "0" means that the item is a bill
    _sb.AppendLine("           , ISNULL(TERMINAL_NAME_1, TERMINAL_NAME_2)                                              ")
    _sb.AppendLine("           , ISNULL(THEORETICAL_AMOUNT, REAL_AMOUNT)                                               ")
    _sb.AppendLine("           , ISNULL(THEORETICAL_COUNTS,0)                                                          ")
    _sb.AppendLine("           , ISNULL(REAL_COUNT,0)                                                                  ")
    _sb.AppendLine("           , '0'                                                                                   ")
    _sb.AppendLine("           , 0                                                                                     ")
    _sb.AppendLine("           , 0                                                                                     ")
    _sb.AppendLine("           , 0                                                                                     ")
    _sb.AppendLine("           , 0                                                                                     ")
    _sb.AppendLine("             FROM (SELECT   TE_NAME  AS TERMINAL_NAME_1                                            ")
    _sb.AppendLine("                          , CTM_DENOMINATION AS THEORETICAL_AMOUNT                                 ")
    _sb.AppendLine("                          , CTM_QUANTITY AS THEORETICAL_COUNTS                                     ")
    _sb.AppendLine("                          , CTM_SESSION_ID AS SESSION                                              ")
    _sb.AppendLine("                     FROM   CASHIER_TERMINAL_MONEY                                                 ")
    _sb.AppendLine("               INNER JOIN   TERMINALS ON TE_TERMINAL_ID = CTM_TERMINAL_ID                          ")
    _sb.AppendLine("                    WHERE   CTM_SESSION_ID = " & m_cashier_session_id.ToString() & "               ")
    _sb.AppendLine("                 GROUP BY   CTM_DENOMINATION, CTM_QUANTITY, CTM_SESSION_ID, TE_NAME) TABLA1        ")
    _sb.AppendLine("                FULL JOIN                                                                          ")
    _sb.AppendLine("                  (SELECT   TE_NAME AS TERMINAL_NAME_2                                             ")
    _sb.AppendLine("                          , SUM(MCD_NUM_COLLECTED) AS REAL_COUNT                                   ")
    _sb.AppendLine("                          , MCD_FACE_VALUE AS REAL_AMOUNT                                          ")
    _sb.AppendLine("                          , MC_CASHIER_SESSION_ID                                                  ")
    _sb.AppendLine("                     FROM   MONEY_COLLECTION_DETAILS                                               ")
    _sb.AppendLine("               INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID              ")
    _sb.AppendLine("               INNER JOIN   TERMINALS ON MC_TERMINAL_ID  = TE_TERMINAL_ID                          ")
    _sb.AppendLine("                    WHERE   MC_CASHIER_SESSION_ID = " & m_cashier_session_id.ToString())
    _sb.AppendLine("                 GROUP BY   MCD_FACE_VALUE, MC_CASHIER_SESSION_ID, TE_NAME) TABLA2                 ")
    _sb.AppendLine("        ON   TABLA1.THEORETICAL_AMOUNT = TABLA2.REAL_AMOUNT                                        ")
    _sb.AppendLine(GetSqlWhere())
    _sb.AppendLine("     UNION                                                                                         ")
    _sb.AppendLine("    SELECT   1                                                                                     ") ' this "1" means that the item is a ticket
    _sb.AppendLine("           , TE_NAME                                                                               ")
    _sb.AppendLine("           , 0                                                                                     ")
    _sb.AppendLine("           , 0                                                                                     ")
    _sb.AppendLine("           , 0                                                                                     ")
    _sb.AppendLine("           , TI_VALIDATION_NUMBER                                                                  ")
    _sb.AppendLine("           , ISNULL(TI_COLLECTED, 0)                                                               ")
    _sb.AppendLine("           , TI_AMOUNT                                                                             ")
    _sb.AppendLine("           , 0                                                                                     ")
    _sb.AppendLine("           , MC_CASHIER_SESSION_ID                                                                 ")
    _sb.AppendLine("      FROM   TICKETS                                                                               ")
    _sb.AppendLine("INNER JOIN   TERMINALS ON TE_TERMINAL_ID = TI_LAST_ACTION_TERMINAL_ID                              ")
    _sb.AppendLine(" LEFT JOIN   MONEY_COLLECTIONS ON TI_MONEY_COLLECTION_ID = MC_COLLECTION_ID                        ")
    _sb.AppendLine("     WHERE   (MC_CASHIER_SESSION_ID = " & m_cashier_session_id.ToString())
    _sb.AppendLine("       AND   TI_COLLECTED = 1)")

    Return _sb.ToString()

  End Function

  ' PURPOSE: Operations to be performed to generate the excel file
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ExcelResultList()
    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA
    Dim _prev_cursor As Windows.Forms.Cursor
    Dim _report_layout As frm_base_sel.ExcelReportLayout
    Dim _sheet_layout As frm_base_sel.ExcelReportLayout.SheetLayout
    Dim _uc_grid_def As frm_base_sel.ExcelReportLayout.SheetLayout.UcGridDefinition

    _prev_cursor = Me.Cursor

    Cursor = Cursors.WaitCursor

    Try

      _print_data = New GUI_Reports.CLASS_PRINT_DATA()

      Call GUI_ReportFilter(_print_data)

      _report_layout = New frm_base_sel.ExcelReportLayout()

      With _report_layout
        .PrintData = _print_data
        .PrintDateTime = m_print_datetime

        _sheet_layout = New frm_base_sel.ExcelReportLayout.SheetLayout()
        _sheet_layout.Title = Me.Text

        For Each _tab_page As TabPage In Me.tab_totals.TabPages
          _uc_grid_def = New frm_base_sel.ExcelReportLayout.SheetLayout.UcGridDefinition()
          _uc_grid_def.Title = _tab_page.Text
          _uc_grid_def.UcGrid = _tab_page.Controls(0)

          If (_uc_grid_def.UcGrid.NumRows > 0) Then
            _sheet_layout.ListOfUcGrids.Add(_uc_grid_def)
          End If
        Next
        .ListOfSheets.Add(_sheet_layout)

      End With
      _report_layout.GUI_GenerateExcel()

    Catch ex As Exception

    Finally
      Cursor = _prev_cursor
    End Try
  End Sub ' GUI_ExcelResultList

  ' PURPOSE: Executa SQL query and populate both grids
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ExecuteQuery()
    m_data_grid = New DataGrid()

    Try
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Check the filter
      If Not GUI_FilterCheck() Then
        Return
      End If

      ' Clear datagrids
      GUI_ClearGrid()

      ' Execute the query        
      Call ExecuteQuery()

    Catch exception As Exception
      Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, "TitoTicketsAndBillsReport", "GUI_ExecuteQuery", _
                            "", "", "", exception.Message)

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub 'GUI_ExecuteQuery
#End Region

#Region " Private Functions"

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Me.chk_completed.Checked = True
    Me.chk_pending.Checked = True
    Me.chk_gap.Checked = True

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String
    Dim _str_where As String = ""

    ' Filter status
    If Me.chk_completed.Checked Then
      _str_where = " REAL_COUNT = THEORETICAL_COUNTS "
    End If

    If Me.chk_pending.Checked Then
      If _str_where <> "" Then
        _str_where &= " OR ISNULL(REAL_COUNT, 0) < THEORETICAL_COUNTS "
      Else
        _str_where = " ISNULL(REAL_COUNT, 0) < THEORETICAL_COUNTS "
      End If
    End If

    If Me.chk_gap.Checked Then
      If _str_where <> "" Then
        _str_where &= " OR REAL_COUNT > ISNULL(THEORETICAL_COUNTS, 0) "
      Else
        _str_where = " REAL_COUNT > ISNULL(THEORETICAL_COUNTS, 0) "
      End If
    End If

    If Not String.IsNullOrEmpty(_str_where) Then
      _str_where = " WHERE " & _str_where
    End If

    Return _str_where

  End Function ' GetSqlWhere

  ' PURPOSE: Clear both datagrids and counters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_ClearGrid()

    Me.dg_tickets.Clear()
    m_tickets_counter_list(COUNTER_COMPLETED) = 0
    m_tickets_counter_list(COUNTER_PENDING) = 0
    m_tickets_counter_list(COUNTER_GAP) = 0

    Me.dg_bills.Clear()
    m_bills_counter_list(COUNTER_COMPLETED) = 0
    m_bills_counter_list(COUNTER_PENDING) = 0
    m_bills_counter_list(COUNTER_GAP) = 0

  End Sub 'GUI_ClearGrid

  ' PURPOSE: Execute sql
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub ExecuteQuery()

    Dim _sql_query As String
    Dim _idx_row As Integer
    Dim _class_row As frm_base_sel.CLASS_DB_ROW

    _idx_row = 0

    ' Initialize DataView for database
    Try
      m_sql_conn = Nothing
      m_sql_conn = NewSQLConnection()

      If Not (m_sql_conn Is Nothing) Then
        ' Connected
        If m_sql_conn.State <> ConnectionState.Open Then
          m_sql_conn.Open()
        End If

        If Not m_sql_conn.State = ConnectionState.Open Then
          Exit Try
        End If
      End If

      m_sql_dataset = New DataSet

      ' Build Query -------------------------------------------------
      _sql_query = GUI_FilterGetSqlQuery()

      If Not IsNothing(m_sql_adap) Then
        m_sql_adap.Dispose()
        m_sql_adap = Nothing
      End If

      m_sql_adap = New SqlDataAdapter(_sql_query, m_sql_conn)
      m_sql_adap.FillSchema(m_sql_dataset, SchemaType.Source)
      m_sql_adap.Fill(m_sql_dataset)

      While _idx_row < m_sql_dataset.Tables.Item(0).Rows.Count

        _class_row = New frm_base_sel.CLASS_DB_ROW(m_sql_dataset.Tables.Item(0).Rows(_idx_row))

        GUI_SetupTicketsRow(_class_row)
        GUI_SetupBillsRow(_class_row)

        _idx_row = _idx_row + 1
      End While


      Call GUI_AfterLastRow()
    Catch ex As Exception
      ' Do nothing
      Debug.WriteLine(ex.Message)

    Finally
    End Try
  End Sub

  ' PURPOSE: Tab change event
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub TabControl1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tab_totals.SelectedIndexChanged
    m_tab_mode = IIf(tab_totals.SelectedIndex = 0, ENUM_TAB_MODE.TICKETS, ENUM_TAB_MODE.BILLS)
  End Sub

  ' PURPOSE: Define all Ticket Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheetForTickets()

    With Me.dg_tickets
      Call .Init(TICKET_GRID_COLUMNS, TICKET_GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Completed Counter
      .Counter(COUNTER_COMPLETED).Visible = True
      .Counter(COUNTER_COMPLETED).BackColor = GetColor(COLOR_COMPLETED_BACK)
      .Counter(COUNTER_COMPLETED).ForeColor = GetColor(COLOR_COMPLETED_FORE)

      ' Pending Counter
      .Counter(COUNTER_PENDING).Visible = True
      .Counter(COUNTER_PENDING).BackColor = GetColor(COLOR_PENDING_BACK)
      .Counter(COUNTER_PENDING).ForeColor = GetColor(COLOR_PENDING_FORE)

      ' Gap Counter
      .Counter(COUNTER_GAP).Visible = True
      .Counter(COUNTER_GAP).BackColor = GetColor(COLOR_GAP_BACK)
      .Counter(COUNTER_GAP).ForeColor = GetColor(COLOR_GAP_FORE)

      ' SENTINEL
      .Column(TICKET_GRID_COLUMN_SENTINEL).Header(0).Text = ""
      .Column(TICKET_GRID_COLUMN_SENTINEL).Width = 200

      ' TERMINAL
      .Column(TICKET_GRID_COLUMN_TERMINAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1968)
      .Column(TICKET_GRID_COLUMN_TERMINAL).Width = 3000
      .Column(TICKET_GRID_COLUMN_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'TICKET NUMBER
      .Column(TICKET_GRID_COLUMN_TICKET_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2109)
      .Column(TICKET_GRID_COLUMN_TICKET_NUMBER).Width = 3000
      .Column(TICKET_GRID_COLUMN_TICKET_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' SUM NOTES EXPECTED
      .Column(TICKET_GRID_COLUMN_SUM_EXPECTED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2260)
      .Column(TICKET_GRID_COLUMN_SUM_EXPECTED).Width = 1300
      .Column(TICKET_GRID_COLUMN_SUM_EXPECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' NUM NOTES MISMATCH
      .Column(TICKET_GRID_COLUMN_NUM_MISMATCH).Header(0).Text = ""
      .Column(TICKET_GRID_COLUMN_NUM_MISMATCH).Width = 0
      .Column(TICKET_GRID_COLUMN_NUM_MISMATCH).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' SUM NOTES MISMATCH
      .Column(TICKET_GRID_COLUMN_SUM_MISMATCH).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1970)
      .Column(TICKET_GRID_COLUMN_SUM_MISMATCH).Width = 1300
      .Column(TICKET_GRID_COLUMN_SUM_MISMATCH).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With
  End Sub ' GUI_StyleSheetForTickets

  ' PURPOSE: Define all Bills Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheetForBills()

    With Me.dg_bills
      Call .Init(BILLS_GRID_COLUMNS, BILLS_GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Completed Counter
      .Counter(COUNTER_COMPLETED).Visible = True
      .Counter(COUNTER_COMPLETED).BackColor = GetColor(COLOR_COMPLETED_BACK)
      .Counter(COUNTER_COMPLETED).ForeColor = GetColor(COLOR_COMPLETED_FORE)

      ' Pending Counter
      .Counter(COUNTER_PENDING).Visible = True
      .Counter(COUNTER_PENDING).BackColor = GetColor(COLOR_PENDING_BACK)
      .Counter(COUNTER_PENDING).ForeColor = GetColor(COLOR_PENDING_FORE)

      ' Gap Counter
      .Counter(COUNTER_GAP).Visible = True
      .Counter(COUNTER_GAP).BackColor = GetColor(COLOR_GAP_BACK)
      .Counter(COUNTER_GAP).ForeColor = GetColor(COLOR_GAP_FORE)

      ' SENTINEL
      .Column(BILLS_GRID_COLUMN_SENTINEL).Header(0).Text = ""
      .Column(BILLS_GRID_COLUMN_SENTINEL).Header(1).Text = ""
      .Column(BILLS_GRID_COLUMN_SENTINEL).Width = 200

      ' TERMINAL
      .Column(BILLS_GRID_COLUMN_TERMINAL).Header(0).Text = ""
      .Column(BILLS_GRID_COLUMN_TERMINAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1968)
      .Column(BILLS_GRID_COLUMN_TERMINAL).Width = 3000
      .Column(BILLS_GRID_COLUMN_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' NOTE FACE VALUE 
      .Column(BILLS_GRID_COLUMN_FACE_VALUE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1967)
      .Column(BILLS_GRID_COLUMN_FACE_VALUE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(576)
      .Column(BILLS_GRID_COLUMN_FACE_VALUE).Width = 1200
      .Column(BILLS_GRID_COLUMN_FACE_VALUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' NUM NOTES COLLECTED
      .Column(BILLS_GRID_COLUMN_NUM_COLLECTED).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(498)
      .Column(BILLS_GRID_COLUMN_NUM_COLLECTED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1969)
      .Column(BILLS_GRID_COLUMN_NUM_COLLECTED).Width = 1200
      .Column(BILLS_GRID_COLUMN_NUM_COLLECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' SUM NOTES COLLECTED
      .Column(BILLS_GRID_COLUMN_SUM_COLLECTED).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(498)
      .Column(BILLS_GRID_COLUMN_SUM_COLLECTED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1970)
      .Column(BILLS_GRID_COLUMN_SUM_COLLECTED).Width = 1300
      .Column(BILLS_GRID_COLUMN_SUM_COLLECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' NUM NOTES EXPECTED
      .Column(BILLS_GRID_COLUMN_NUM_EXPECTED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(645)
      .Column(BILLS_GRID_COLUMN_NUM_EXPECTED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1969)
      .Column(BILLS_GRID_COLUMN_NUM_EXPECTED).Width = 1200
      .Column(BILLS_GRID_COLUMN_NUM_EXPECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' SUM NOTES EXPECTED
      .Column(BILLS_GRID_COLUMN_SUM_EXPECTED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(645)
      .Column(BILLS_GRID_COLUMN_SUM_EXPECTED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1970)
      .Column(BILLS_GRID_COLUMN_SUM_EXPECTED).Width = 1300
      .Column(BILLS_GRID_COLUMN_SUM_EXPECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' NUM NOTES MISMATCH
      .Column(BILLS_GRID_COLUMN_NUM_MISMATCH).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(499)
      .Column(BILLS_GRID_COLUMN_NUM_MISMATCH).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1969)
      .Column(BILLS_GRID_COLUMN_NUM_MISMATCH).Width = 1200
      .Column(BILLS_GRID_COLUMN_NUM_MISMATCH).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' SUM NOTES MISMATCH
      .Column(BILLS_GRID_COLUMN_SUM_MISMATCH).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(499)
      .Column(BILLS_GRID_COLUMN_SUM_MISMATCH).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1970)
      .Column(BILLS_GRID_COLUMN_SUM_MISMATCH).Width = 1300
      .Column(BILLS_GRID_COLUMN_SUM_MISMATCH).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With
  End Sub ' GUI_StyleSheetForBills

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset
#End Region

#Region "Public Functions"

  ' PURPOSE : Sets the values of a row inthe bills grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Function GUI_SetupBillsRow(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _face_value As Double
    Dim _real_count As Int32
    Dim _theoretical_count As Int32
    Dim _real_tickets_count As Int32
    Dim _theoretical_tickets_count As Int32
    Dim _num_mismatch As Integer
    Dim _ticket_value As Double
    Dim _row_source As Integer
    Dim _last_row_idx As Integer

    _face_value = DbRow.Value(SQL_COLUMN_FACE_VALUE)
    _real_count = DbRow.Value(SQL_COLUMN_REAL_COUNT)
    _theoretical_count = DbRow.Value(SQL_COLUMN_THEORETICAL_COUNT)
    _ticket_value = DbRow.Value(SQL_COLUMN_TICKET_VALUE)
    _real_tickets_count = 0
    _theoretical_tickets_count = 0
    _row_source = DbRow.Value(SQL_COLUMN_TICKET_OR_BILL)

    If (_row_source = ENUM_TAB_MODE.TICKETS) Then
      Return False
    End If

    Me.dg_bills.AddRow()

    _last_row_idx = Me.dg_bills.NumRows - 1

    If DbRow.Value(SQL_COLUMN_TICKET_OR_BILL) = ENUM_TAB_MODE.BILLS Then
      Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_TERMINAL).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
      Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_FACE_VALUE).Value = GUI_FormatCurrency(_face_value)
      Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_NUM_COLLECTED).Value = _real_count
      Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_SUM_COLLECTED).Value = GUI_FormatCurrency(_real_count * _face_value)
      Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_NUM_EXPECTED).Value = _theoretical_count
      Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_SUM_EXPECTED).Value = GUI_FormatCurrency(_theoretical_count * _face_value)

      _num_mismatch = _real_count - _theoretical_count

      If _num_mismatch <> 0 Then
        Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_NUM_MISMATCH).Value = _num_mismatch
        Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_SUM_MISMATCH).Value = GUI_FormatCurrency((_num_mismatch) * _face_value)
      End If

    Else
      Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_TERMINAL).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
      Me.dg_bills.Cell(_last_row_idx, TICKET_GRID_COLUMN_TICKET_NUMBER).Value = DbRow.Value(SQL_COLUMN_TICKET_NUMBER)

      If Not IsDBNull(DbRow.Value(SQL_COLUMN_TICKET_REAL_SESSION)) AndAlso DbRow.Value(SQL_COLUMN_TICKET_REAL_SESSION) = m_cashier_session_id Then
        If DbRow.Value(SQL_COLUMN_TICKET_COLLECTED) = 1 Then
          Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_NUM_COLLECTED).Value = 1
          Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_SUM_COLLECTED).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TICKET_VALUE))
          _real_tickets_count = 1
        End If
      Else
        Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_NUM_COLLECTED).Value = 0
        Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_SUM_COLLECTED).Value = GUI_FormatCurrency(0)

      End If

      If DbRow.Value(SQL_COLUMN_TICKET_THEORETICAL_SESSION) = m_cashier_session_id Then
        Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_NUM_EXPECTED).Value = 1
        Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_SUM_EXPECTED).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TICKET_VALUE))
        _theoretical_tickets_count = 1
      Else
        Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_NUM_EXPECTED).Value = 0
        Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_SUM_EXPECTED).Value = GUI_FormatCurrency(0)
      End If

      _num_mismatch = _real_tickets_count - _theoretical_tickets_count

      If _num_mismatch <> 0 Then
        Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_NUM_MISMATCH).Value = _num_mismatch
        Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_SUM_MISMATCH).Value = GUI_FormatCurrency((_num_mismatch) * _ticket_value)
      End If

      If Not Me.chk_completed.Checked AndAlso (chk_gap.Checked Or chk_pending.Checked) AndAlso _num_mismatch = 0 Then
        Return False
      End If
      If Not chk_gap.Checked AndAlso (chk_completed.Checked Or chk_pending.Checked) AndAlso _num_mismatch > 0 Then
        Return False
      End If
      If Not chk_pending.Checked AndAlso (chk_completed.Checked Or chk_gap.Checked) AndAlso _num_mismatch < 0 Then
        Return False
      End If
    End If

    Select Case _num_mismatch
      Case Is = 0
        Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_SUM_MISMATCH).BackColor = GetColor(COLOR_COMPLETED_BACK)
        Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_SUM_MISMATCH).ForeColor = GetColor(COLOR_COMPLETED_FORE)
        m_bills_counter_list(COUNTER_COMPLETED) = m_bills_counter_list(COUNTER_COMPLETED) + 1
      Case Is < 0
        Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_SUM_MISMATCH).BackColor = GetColor(COLOR_PENDING_BACK)
        Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_SUM_MISMATCH).ForeColor = GetColor(COLOR_PENDING_FORE)
        m_bills_counter_list(COUNTER_PENDING) = m_bills_counter_list(COUNTER_PENDING) + 1
      Case Is > 0
        Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_SUM_MISMATCH).BackColor = GetColor(COLOR_GAP_BACK)
        Me.dg_bills.Cell(_last_row_idx, BILLS_GRID_COLUMN_SUM_MISMATCH).ForeColor = GetColor(COLOR_GAP_FORE)
        m_bills_counter_list(COUNTER_GAP) = m_bills_counter_list(COUNTER_GAP) + 1
    End Select

    Return True

  End Function ' GUI_SetupBillsRow

  ' PURPOSE : Sets the values of a row in tickets grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Function GUI_SetupTicketsRow(ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean ' GUI_SetupTicketsRow

    Dim _face_value As Double
    Dim _real_count As Int32
    Dim _theoretical_count As Int32
    Dim _is_collected As Int32
    Dim _ticket_value As Double
    Dim _row_source As Integer
    Dim _last_row_idx As Integer

    _face_value = DbRow.Value(SQL_COLUMN_FACE_VALUE)
    _real_count = DbRow.Value(SQL_COLUMN_REAL_COUNT)
    _theoretical_count = DbRow.Value(SQL_COLUMN_THEORETICAL_COUNT)
    _ticket_value = DbRow.Value(SQL_COLUMN_TICKET_VALUE)
    _is_collected = DbRow.Value(SQL_COLUMN_TICKET_COLLECTED)
    _row_source = DbRow.Value(SQL_COLUMN_TICKET_OR_BILL)

    If (_row_source = ENUM_TAB_MODE.BILLS) Then
      Return False
    End If

    Me.dg_tickets.AddRow()

    _last_row_idx = Me.dg_tickets.NumRows - 1
    ' JRM TODO: Change Constant for ENUM
    Me.dg_tickets.Cell(_last_row_idx, TICKET_GRID_COLUMN_TERMINAL).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
    Me.dg_tickets.Cell(_last_row_idx, TICKET_GRID_COLUMN_TICKET_NUMBER).Value = ValidationNumberManager.FormatValidationNumber(DbRow.Value(SQL_COLUMN_TICKET_NUMBER), 1, False)
    Me.dg_tickets.Cell(_last_row_idx, TICKET_GRID_COLUMN_SUM_EXPECTED).Value = GUI_FormatCurrency(_ticket_value)
    Me.dg_tickets.Cell(_last_row_idx, TICKET_GRID_COLUMN_SUM_MISMATCH).Value = IIf(_is_collected = 0, GUI_FormatCurrency(_ticket_value.ToString()), "")

    Select Case _is_collected
      Case Is = 1
        Me.dg_tickets.Cell(_last_row_idx, TICKET_GRID_COLUMN_SUM_MISMATCH).BackColor = GetColor(COLOR_COMPLETED_BACK)
        Me.dg_tickets.Cell(_last_row_idx, TICKET_GRID_COLUMN_SUM_MISMATCH).ForeColor = GetColor(COLOR_COMPLETED_FORE)
        m_tickets_counter_list(COUNTER_COMPLETED) = m_bills_counter_list(COUNTER_COMPLETED) + 1
      Case Is = 0
        Me.dg_tickets.Cell(_last_row_idx, TICKET_GRID_COLUMN_SUM_MISMATCH).BackColor = GetColor(COLOR_PENDING_BACK)
        Me.dg_tickets.Cell(_last_row_idx, TICKET_GRID_COLUMN_SUM_MISMATCH).ForeColor = GetColor(COLOR_PENDING_FORE)

        m_tickets_counter_list(COUNTER_PENDING) = m_bills_counter_list(COUNTER_PENDING) + 1

    End Select

    Return True

  End Function
  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowModalForEdit(ByVal session_id As Integer, ByVal session_name As String)

    Me.m_cashier_session_id = session_id
    Me.m_cashier_session_name = session_name
    Me.m_collection_by_terminal = False

    Me.Show()

  End Sub ' ShowModalForEdit
#End Region


  Public Sub New()

    ' This call is required by the designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub
End Class