'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_main_tito.vb
' DESCRIPTION:   Tito main.
' AUTHOR:        Humberto Braojos Bosch
' CREATION DATE: 23-JUL-2013
'
' REVISION HISTORY: 
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 23-JUL-2013  HBB    Initial version
' 23-AUG-2013  JRM    Code revision and refactoring
' 14-OCT-2013  LEM    Check Read permission for frm_tito_stackers_sel and frm_tito_params_edit in Main Menu
' 19-NOV-2013  DRV    Added MB Movements
' 22-NOV-2013  ACM    Added Terminal Sas Meters
' 02-DEC-2013  RMS    Updated menu options from frm_main
'                     Added app version to form title
' 04-DEC-2013  AMF    Remove FORM_MOBILE_BANK_MOVEMENTS
' 16-DEC-2013  JMM    Added bonuses report
' -------------------------------------------------------------------
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common

Public Class frm_main_tito
  Inherits GUI_Controls.frm_base_mdi

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub
  Friend WithEvents lbl_main_title As GUI_Controls.uc_text_field

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.lbl_main_title = New GUI_Controls.uc_text_field
    Me.SuspendLayout()
    '
    'lbl_main_title
    '
    Me.lbl_main_title.IsReadOnly = True
    Me.lbl_main_title.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_main_title.LabelForeColor = System.Drawing.Color.Blue
    Me.lbl_main_title.Location = New System.Drawing.Point(282, 19)
    Me.lbl_main_title.Name = "lbl_main_title"
    Me.lbl_main_title.Size = New System.Drawing.Size(66, 24)
    Me.lbl_main_title.SufixText = "Sufix Text"
    Me.lbl_main_title.SufixTextVisible = True
    Me.lbl_main_title.TabIndex = 1
    Me.lbl_main_title.Visible = False
    '
    'frm_main
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(496, 272)
    Me.Controls.Add(Me.lbl_main_title)
    Me.Name = "frm_main"
    Me.Text = "xWigos GUI"
    Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    Me.Controls.SetChildIndex(Me.lbl_main_title, 0)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region "Menu - System"

  Dim m_profiles_item As CLASS_MENU_ITEM
  Dim m_terminals As CLASS_MENU_ITEM
  Dim m_terminals_3gs As CLASS_MENU_ITEM
  Dim m_pending_terminals As CLASS_MENU_ITEM
  Dim m_terminal_game_relation As CLASS_MENU_ITEM
  Dim m_providers As CLASS_MENU_ITEM
  Dim m_configuration As CLASS_MENU_ITEM
  Dim m_cashier_configuration As CLASS_MENU_ITEM
  Dim m_mailing_programming As CLASS_MENU_ITEM
  Dim m_currencies_accepted As CLASS_MENU_ITEM
  Dim m_password_configuration As CLASS_MENU_ITEM
  Dim m_cash_by_service As CLASS_MENU_ITEM
  Dim m_payment_orders_config As CLASS_MENU_ITEM
  Dim m_player_edit_config As CLASS_MENU_ITEM
  Dim m_operation_schedule As CLASS_MENU_ITEM
  Dim m_currencies As CLASS_MENU_ITEM
  Dim m_machine_alias As CLASS_MENU_ITEM
  Dim m_general_params As CLASS_MENU_ITEM
  Dim m_service_monitor As CLASS_MENU_ITEM
  Dim m_area_bank As CLASS_MENU_ITEM
  Dim m_import As CLASS_MENU_ITEM
  Dim m_accounts_import As CLASS_MENU_ITEM
  Dim m_terminals_group As CLASS_MENU_ITEM
  Dim m_auditor_event_history As CLASS_MENU_ITEM
  Dim m_auditor As CLASS_MENU_ITEM
  Dim m_event_history As CLASS_MENU_ITEM
  Dim m_alarms As CLASS_MENU_ITEM
  Dim m_site_jackpot As CLASS_MENU_ITEM
  Dim m_site_jackpot_configuration As CLASS_MENU_ITEM
  Dim m_site_jackpot_history As CLASS_MENU_ITEM
  Dim m_class_II_jackpot As CLASS_MENU_ITEM
  Dim m_jackpot_configuration As CLASS_MENU_ITEM
  Dim m_jackpot_promo_messages As CLASS_MENU_ITEM
  Dim m_jackpot_history As CLASS_MENU_ITEM
  Dim m_jackpot_monitor As CLASS_MENU_ITEM
  Dim m_download_versions As CLASS_MENU_ITEM
  Dim m_misc_sw_build As CLASS_MENU_ITEM
  Dim m_licenses As CLASS_MENU_ITEM
  Dim m_magnetic_card_writer As CLASS_MENU_ITEM
  Dim m_statistics_terminals As CLASS_MENU_ITEM
  Dim m_statistics_date As CLASS_MENU_ITEM
  Dim m_statistics_games As CLASS_MENU_ITEM
  Dim m_statistics_hours_group As CLASS_MENU_ITEM
  Dim m_statistics_hours As CLASS_MENU_ITEM
  Dim m_date_provider_terminal As CLASS_MENU_ITEM
  Dim m_capacity_by_provider As CLASS_MENU_ITEM
  Dim m_customer_base As CLASS_MENU_ITEM
  Dim m_play_preferences As CLASS_MENU_ITEM
  Dim m_machine_ocupation As CLASS_MENU_ITEM
  Dim m_played_by_customer_and_provider As CLASS_MENU_ITEM
  Dim m_productivity_reports As CLASS_MENU_ITEM
  Dim m_segmentation_reports As CLASS_MENU_ITEM
  Dim m_detailed_awards As CLASS_MENU_ITEM
  Dim m_stats_manual_adjustment As CLASS_MENU_ITEM
  Dim m_plays_sessions As CLASS_MENU_ITEM
  Dim m_sessions As CLASS_MENU_ITEM
  Dim m_plays_counter As CLASS_MENU_ITEM
  Dim m_commands As CLASS_MENU_ITEM
  Dim m_commands_send As CLASS_MENU_ITEM
  Dim m_commands_history As CLASS_MENU_ITEM
  Dim m_terminals_status As CLASS_MENU_ITEM
  Dim m_terminals_without_activity As CLASS_MENU_ITEM
  Dim m_skipped_meters_report As CLASS_MENU_ITEM
  Dim m_peru_frames As CLASS_MENU_ITEM
  Dim m_accounts_menu As CLASS_MENU_ITEM
  Dim m_card_summary As CLASS_MENU_ITEM
  Dim m_card_movements As CLASS_MENU_ITEM
  Dim m_expired_credits As CLASS_MENU_ITEM
  Dim m_manual_assignment_report As CLASS_MENU_ITEM
  Dim m_cashier_sessions As CLASS_MENU_ITEM
  Dim m_cashier_movements As CLASS_MENU_ITEM
  Dim m_cash_summary As CLASS_MENU_ITEM
  Dim m_taxes_resume As CLASS_MENU_ITEM
  Dim m_provider_activity As CLASS_MENU_ITEM
  Dim m_hand_pays As CLASS_MENU_ITEM
  Dim m_taxes_report As CLASS_MENU_ITEM
  Dim m_statistics_quadrature As CLASS_MENU_ITEM
  Dim m_provider_cash_activity As CLASS_MENU_ITEM
  Dim m_transfer_quadrature As CLASS_MENU_ITEM
  Dim m_export As CLASS_MENU_ITEM
  Dim m_voucher_browser As CLASS_MENU_ITEM
  Dim m_prize_taxes_report As CLASS_MENU_ITEM
  Dim m_note_acceptor As CLASS_MENU_ITEM
  Dim m_accepted_notes As CLASS_MENU_ITEM
  Dim m_money_pending_transfer As CLASS_MENU_ITEM
  Dim m_money_collections As CLASS_MENU_ITEM
  Dim m_tickets_menu As CLASS_MENU_ITEM
  Dim m_tickets_report As CLASS_MENU_ITEM
  Dim m_retreats_report As CLASS_MENU_ITEM
  Dim m_service_charge_report As CLASS_MENU_ITEM
  Dim m_payment_orders As CLASS_MENU_ITEM
  Dim m_currency_exchange_audit As CLASS_MENU_ITEM
  Dim m_draws As CLASS_MENU_ITEM
  Dim m_promotions As CLASS_MENU_ITEM
  Dim m_periodical_promotions As CLASS_MENU_ITEM
  Dim m_preassigned_promotions As CLASS_MENU_ITEM
  Dim m_promotion_report As CLASS_MENU_ITEM
  Dim m_promotion_delivered_report As CLASS_MENU_ITEM
  Dim m_advertisement_campaing As CLASS_MENU_ITEM
  Dim m_gifts As CLASS_MENU_ITEM
  Dim m_gifts_catalog As CLASS_MENU_ITEM
  Dim m_gifts_history As CLASS_MENU_ITEM
  Dim m_recommendation_report As CLASS_MENU_ITEM
  Dim m_provider_excluded As CLASS_MENU_ITEM
  Dim m_flags As CLASS_MENU_ITEM
  Dim m_account_flags As CLASS_MENU_ITEM
  Dim m_report_draw_tickets As CLASS_MENU_ITEM
  Dim m_tito_params As CLASS_MENU_ITEM
  Dim m_stacker_collection_sel As CLASS_MENU_ITEM
  Dim m_circulating_cash As CLASS_MENU_ITEM
  Dim m_stacker_sel As CLASS_MENU_ITEM
  Dim m_ticket_tito_report As CLASS_MENU_ITEM
  Dim m_terminals_configuration As CLASS_MENU_ITEM
  Dim m_points_to_credit As CLASS_MENU_ITEM
  Dim m_points_assignment As CLASS_MENU_ITEM
  Dim m_terminal_sas_meters_sel As CLASS_MENU_ITEM
  Dim m_tito_collection_balance_report As CLASS_MENU_ITEM
  Dim m_tito_summary_sel As CLASS_MENU_ITEM
  Dim m_bonuses_report As CLASS_MENU_ITEM

  ' B�veda
  Dim m_CAGE_menu As CLASS_MENU_ITEM
  Dim m_CAGE_amounts As CLASS_MENU_ITEM
  Dim m_CAGE_control As CLASS_MENU_ITEM
  Dim m_CAGE_new As CLASS_MENU_ITEM
  Dim m_CAGE_Source_Target As CLASS_MENU_ITEM
  Dim m_CAGE_Sessions As CLASS_MENU_ITEM

  Dim m_account_card_changes As CLASS_MENU_ITEM

  ' Promobox
  Dim m_ads_configuration As CLASS_MENU_ITEM

  ' Money Laundering
  Dim m_money_laundering_config As CLASS_MENU_ITEM
  Dim m_money_laundering_report As CLASS_MENU_ITEM
  Dim m_money_laundering_report_prize As CLASS_MENU_ITEM
  Dim m_money_laundering_report_recharge As CLASS_MENU_ITEM

  ' Cashdesk Draws
  Dim m_cashdesk_draws_config As CLASS_MENU_ITEM
  Dim m_cashdesk_draws_report As CLASS_MENU_ITEM
  Dim m_cashdesk_draws_summary As CLASS_MENU_ITEM

  ' MultiSite monitor
  Dim m_multiSite_config As CLASS_MENU_ITEM
  Dim m_multisite_monitor As CLASS_MENU_ITEM

#End Region

#Region " Must Override Methods: frm_base_mdi "

  ' PURPOSE: Set initial GUI permissions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '       - Perm: Permissions set.
  '
  ' RETURNS:
  Protected Overrides Sub GUI_Permissions(ByRef Perm As GUI_Controls.CLASS_GUI_USER.TYPE_PERMISSIONS)
    Perm.Read = True
    Perm.Write = False
    Perm.Delete = False
    Perm.Execute = False

  End Sub ' GUI_Permissions

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_MAIN

    ' TJG 01-FEB-2005
    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_Auditor.GUI_Auditor.ico"))

    '------------------------------------------------
    ' XVV 13/04/2007
    ' Translate tu BASE Form
    ' Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "WigosGUI.ico"))
    '------------------------------------------------

    '------------------------------------------------
    'XVV 13/04/2007
    'Call Base Form proc
    Call MyBase.GUI_SetFormId()
    '------------------------------------------------

  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Initialize control to keep form title. A control is used to manage event update
    lbl_main_title.Value = ""

    'PROFILES CONFIGURAION
    m_profiles_item.Status = CurrentUser.Permissions(m_profiles_item.FormId).Read

    'TERMINALS
    m_terminals.Status = CurrentUser.Permissions(m_terminals.FormId).Read

    'TERMINALS 3GS
    m_terminals_3gs.Status = CurrentUser.Permissions(m_terminals_3gs.FormId).Read

    'PENDING TERMINALS
    m_pending_terminals.Status = CurrentUser.Permissions(m_pending_terminals.FormId).Read

    'TERMINAL GAME RELATION
    m_terminal_game_relation.Status = CurrentUser.Permissions(m_terminal_game_relation.FormId).Read

    'PROVIDERS
    m_providers.Status = CurrentUser.Permissions(m_terminal_game_relation.FormId).Read

    'CONFIGURATION MENU
    m_configuration.Status = CurrentUser.Permissions(m_configuration.FormId).Read

    'CASHIER CONFIGURATION
    m_cashier_configuration.Status = CurrentUser.Permissions(m_cashier_configuration.FormId).Read

    'MAILING PROGRAMMING
    m_mailing_programming.Status = CurrentUser.Permissions(m_mailing_programming.FormId).Read

    'CURRENCIES
    m_currencies_accepted.Status = CurrentUser.Permissions(m_currencies_accepted.FormId).Read

    'PASSWORD CONFIGURATION
    m_password_configuration.Status = CurrentUser.Permissions(m_password_configuration.FormId).Read

    'CASH BY SERVICE
    m_cash_by_service.Status = CurrentUser.Permissions(m_cash_by_service.FormId).Read

    'OPERATION SCHEDULE
    m_operation_schedule.Status = CurrentUser.Permissions(m_operation_schedule.FormId).Read

    'CURRENCIES
    m_currencies.Status = CurrentUser.Permissions(m_currencies.FormId).Read

    'TERMINALS ALIAS
    m_machine_alias.Status = CurrentUser.Permissions(m_machine_alias.FormId).Read

    'GENERAL PARAMS
    m_general_params.Status = CurrentUser.Permissions(m_general_params.FormId).Read

    'SERVICE MONITOR
    m_service_monitor.Status = CurrentUser.Permissions(m_service_monitor.FormId).Read

    'AREA ISLAND
    m_area_bank.Status = CurrentUser.Permissions(m_area_bank.FormId).Read

    'IMPORT
    m_import.Status = CurrentUser.Permissions(m_import.FormId).Read

    'IMPORT ACCOUNT
    m_accounts_import.Status = CurrentUser.Permissions(m_accounts_import.FormId).Read

    'TERMINALS GROUP
    m_terminals_group.Status = CurrentUser.Permissions(m_terminals_group.FormId).Read

    'AUDITOR
    m_auditor.Status = CurrentUser.Permissions(m_auditor.FormId).Read

    'EVENT HISTORY
    m_event_history.Status = CurrentUser.Permissions(m_event_history.FormId).Read
    m_alarms.Status = CurrentUser.Permissions(m_alarms.FormId).Read

    'SITE JACKOT
    m_site_jackpot.Status = CurrentUser.Permissions(m_site_jackpot.FormId).Read

    'SITE JACKPOT CONFIGURATION
    m_site_jackpot_configuration.Status = CurrentUser.Permissions(m_site_jackpot_configuration.FormId).Read

    'JACKPOT CONFIGURATION 
    m_jackpot_configuration.Status = CurrentUser.Permissions(m_jackpot_configuration.FormId).Read

    'JACKPOT PROMO MESSAGES
    m_jackpot_promo_messages.Status = CurrentUser.Permissions(m_jackpot_promo_messages.FormId).Read

    'JACKPOT HISTORY
    m_jackpot_history.Status = CurrentUser.Permissions(m_jackpot_history.FormId).Read

    'JACKPOT MONITOR
    m_jackpot_monitor.Status = CurrentUser.Permissions(m_jackpot_monitor.FormId).Read

    'DOWNLOAD VERSION
    m_download_versions.Status = CurrentUser.Permissions(m_download_versions.FormId).Read

    'BUILD
    m_misc_sw_build.Status = CurrentUser.Permissions(m_misc_sw_build.FormId).Read

    'LICENCES
    m_licenses.Status = CurrentUser.Permissions(m_licenses.FormId).Read

    'MAGNETIC CARD WRITER
    If GLB_CurrentUser.IsSuperUser Then
      m_magnetic_card_writer.Status = CurrentUser.Permissions(m_magnetic_card_writer.FormId).Read
    End If
    'STATISTICS BY TERMINAL
    m_statistics_terminals.Status = CurrentUser.Permissions(m_statistics_terminals.FormId).Read

    'STATISTICS BY DATE
    m_statistics_date.Status = CurrentUser.Permissions(m_statistics_date.FormId).Read

    'STATISTICS BY GAME
    m_statistics_games.Status = CurrentUser.Permissions(m_statistics_games.FormId).Read

    'STATISTIC GROUP BY HOUR
    m_statistics_hours_group.Status = CurrentUser.Permissions(m_statistics_hours_group.FormId).Read

    'STATISTIC BY HOUR
    m_statistics_hours.Status = CurrentUser.Permissions(m_statistics_hours.FormId).Read

    'STATISTICS BY DATE, TERMINAL AND PROVIDER
    m_date_provider_terminal.Status = CurrentUser.Permissions(m_date_provider_terminal.FormId).Read

    'CAPACITY BY PROVIDER
    m_capacity_by_provider.Status = CurrentUser.Permissions(m_capacity_by_provider.FormId).Read

    'CUSTOMER BASE
    m_customer_base.Status = CurrentUser.Permissions(m_customer_base.FormId).Read

    'PLAY PREFERENCE
    m_play_preferences.Status = CurrentUser.Permissions(m_play_preferences.FormId).Read

    'MACHINE OCCUPATION
    m_machine_ocupation.Status = CurrentUser.Permissions(m_machine_ocupation.FormId).Read

    'PLAYED BY CUSTOMER AND PROVIDER
    m_played_by_customer_and_provider.Status = CurrentUser.Permissions(m_played_by_customer_and_provider.FormId).Read

    'PRODUCTIVITY REPORT
    m_productivity_reports.Status = CurrentUser.Permissions(m_productivity_reports.FormId).Read

    'SEGMENTATION REPORT
    m_segmentation_reports.Status = CurrentUser.Permissions(m_segmentation_reports.FormId).Read

    'DETAILED AWARDS
    m_detailed_awards.Status = CurrentUser.Permissions(m_segmentation_reports.FormId).Read

    'STATICTICS MANUAL ADJUSTMENT
    m_stats_manual_adjustment.Status = CurrentUser.Permissions(m_stats_manual_adjustment.FormId).Read

    'PLAY SESSIONS
    m_plays_sessions.Status = CurrentUser.Permissions(m_plays_sessions.FormId).Read

    'SESSIONS
    m_sessions.Status = CurrentUser.Permissions(m_stats_manual_adjustment.FormId).Read

    'PLAYS COUNTER
    m_plays_counter.Status = CurrentUser.Permissions(m_plays_counter.FormId).Read

    'COMMAND SEND
    m_commands_send.Status = CurrentUser.Permissions(m_commands_send.FormId).Read

    'COMMAND HISTORY
    m_commands_history.Status = CurrentUser.Permissions(m_commands_history.FormId).Read

    'TERMINAL STATUS
    m_terminals_status.Status = CurrentUser.Permissions(m_terminals_status.FormId).Read

    ' TERMINALS WITHOUT ACTIVITY
    m_terminals_without_activity.Status = CurrentUser.Permissions(m_terminals_without_activity.FormId).Read

    'PERU FRAMES
    If WSI.Common.Misc.ReadGeneralParams("ExternalProtocol", "Enabled") Then
      m_peru_frames.Status = CurrentUser.Permissions(m_terminals_without_activity.FormId).Read
    End If

    'EXPIRED CREDIT
    m_expired_credits.Status = CurrentUser.Permissions(m_expired_credits.FormId).Read

    'ACCCOUNT SUMMARY
    m_card_summary.Status = CurrentUser.Permissions(m_card_summary.FormId).Read

    'ACCOUNT MOVEMENT
    m_card_movements.Status = CurrentUser.Permissions(m_card_summary.FormId).Read

    'MUANUAL ASSIGNMENT REPORT
    m_manual_assignment_report.Status = CurrentUser.Permissions(m_manual_assignment_report.FormId).Read

    'CASHIER SESSION
    m_cashier_sessions.Status = CurrentUser.Permissions(m_cashier_sessions.FormId).Read

    'CASHIER MOVEMENTS
    m_cashier_movements.Status = CurrentUser.Permissions(m_cashier_movements.FormId).Read

    'CASH SUMMARY
    m_cash_summary.Status = CurrentUser.Permissions(m_cash_summary.FormId).Read

    'TAXES RESUME
    m_taxes_resume.Status = CurrentUser.Permissions(m_taxes_resume.FormId).Read

    'PROVIDER ACTIVITY
    m_provider_activity.Status = CurrentUser.Permissions(m_provider_activity.FormId).Read

    'HAND PAYS
    m_hand_pays.Status = CurrentUser.Permissions(m_provider_activity.FormId).Read

    'TAXES REPORT
    m_taxes_report.Status = CurrentUser.Permissions(m_taxes_report.FormId).Read

    'STATISTICS QUADRATURE
    m_statistics_quadrature.Status = CurrentUser.Permissions(m_statistics_quadrature.FormId).Read

    'POVIDER AND CASH ACTIVITY
    m_provider_cash_activity.Status = CurrentUser.Permissions(m_provider_cash_activity.FormId).Read

    'TRANSFER QUADRATURE
    m_transfer_quadrature.Status = CurrentUser.Permissions(m_transfer_quadrature.FormId).Read

    'EXPORT...
    m_export.Status = CurrentUser.Permissions(m_transfer_quadrature.FormId).Read

    'VOUCHER BROWSER
    m_voucher_browser.Status = CurrentUser.Permissions(m_voucher_browser.FormId).Read

    'TAXES OVER AWARDS
    m_prize_taxes_report.Status = CurrentUser.Permissions(m_prize_taxes_report.FormId).Read

    If WSI.Common.Misc.ReadGeneralParams("NoteAcceptor", "Enabled") Then
      'NOTE ACCEPTOR
      m_note_acceptor.Status = CurrentUser.Permissions(m_note_acceptor.FormId).Read

      'ACCEPTED NOTES
      m_accepted_notes.Status = CurrentUser.Permissions(m_accepted_notes.FormId).Read

      'MONEY PENDING TRANSFER
      m_money_pending_transfer.Status = CurrentUser.Permissions(m_money_pending_transfer.FormId).Read

      'MONEY COLLECTIONS
      m_money_collections.Status = CurrentUser.Permissions(m_money_collections.FormId).Read
    End If

    'TICKET REPORT
    m_tickets_report.Status = CurrentUser.Permissions(m_tickets_report.FormId).Read

    'RETREATS REPORT
    m_retreats_report.Status = CurrentUser.Permissions(m_retreats_report.FormId).Read

    'SERVICE CHARGE
    m_service_charge_report.Status = CurrentUser.Permissions(m_service_charge_report.FormId).Read

    'PAYMENT ORDER
    m_payment_orders.Status = CurrentUser.Permissions(m_payment_orders.FormId).Read

    'Currency Exchange Audit
    m_currency_exchange_audit.Status = CurrentUser.Permissions(m_payment_orders.FormId).Read

    'DRAWS
    m_draws.Status = CurrentUser.Permissions(m_draws.FormId).Read

    'PROMOTION
    m_promotions.Status = CurrentUser.Permissions(m_promotions.FormId).Read

    'PERIODICAL PROMOTION
    m_periodical_promotions.Status = CurrentUser.Permissions(m_periodical_promotions.FormId).Read

    'PREASSIGNED PROMOTION
    m_preassigned_promotions.Status = CurrentUser.Permissions(m_preassigned_promotions.FormId).Read

    'PROMOTION REPORT
    m_promotion_report.Status = CurrentUser.Permissions(m_promotion_report.FormId).Read

    'PROMOTION DELIVERED REPORT
    m_promotion_delivered_report.Status = CurrentUser.Permissions(m_promotion_delivered_report.FormId).Read

    'ADVERTISEMENT CAMPAING
    m_advertisement_campaing.Status = CurrentUser.Permissions(m_advertisement_campaing.FormId).Read

    ' PROMOBOX
    m_ads_configuration.Status = CurrentUser.Permissions(m_ads_configuration.FormId).Read

    'GIFTS
    m_gifts.Status = CurrentUser.Permissions(m_gifts.FormId).Read

    'GIFTS CATALOG
    m_gifts_catalog.Status = CurrentUser.Permissions(m_gifts_catalog.FormId).Read

    'GIFTS HISTORY
    m_gifts_history.Status = CurrentUser.Permissions(m_gifts_history.FormId).Read

    'RECOMMENDATION
#If DEBUG Then
    m_recommendation_report.Status = CurrentUser.Permissions(m_recommendation_report.FormId).Read
#End If

    'PROVIDER EXCLUDES FROM NON REDEEMABLE CREDIT
    m_provider_excluded.Status = CurrentUser.Permissions(m_provider_excluded.FormId).Read

    'FLAGS
    m_flags.Status = CurrentUser.Permissions(m_flags.FormId).Read

    'ACCOUNT FLAGS
    m_account_flags.Status = CurrentUser.Permissions(m_account_flags.FormId).Read

    'REPORT DRAW TICKET
    m_report_draw_tickets.Status = CurrentUser.Permissions(m_report_draw_tickets.FormId).Read

    'DRAW CASHDESK REPORT
    m_cashdesk_draws_report.Status = CurrentUser.Permissions(m_cashdesk_draws_report.FormId).Read

    ' DRAW CASH DESK SUMMARY
    m_cashdesk_draws_summary.Status = CurrentUser.Permissions(m_cashdesk_draws_summary.FormId).Read

    'TITO CONFIGURATION
    m_tito_params.Status = CurrentUser.Permissions(m_tito_params.FormId).Read

    'STACKER COLLECTION
    m_stacker_collection_sel.Status = CurrentUser.Permissions(m_stacker_collection_sel.FormId).Read

    'TITO CURRENCY
    ' m_tito_currencies.Status = CurrentUser.Permissions(m_tito_currencies.FormId).Read

    'CIRCULATING CASH
    m_circulating_cash.Status = CurrentUser.Permissions(m_circulating_cash.FormId).Read

    'STACKER SELECTION
    m_stacker_sel.Status = CurrentUser.Permissions(m_stacker_sel.FormId).Read

    'TICKET REPORT
    m_ticket_tito_report.Status = CurrentUser.Permissions(m_ticket_tito_report.FormId).Read

    'REDEMPTION TABLES
    m_points_to_credit.Status = CurrentUser.Permissions(m_points_to_credit.FormId).Read

    'POINTS ASSIGNMENT
    m_points_assignment.Status = CurrentUser.Permissions(m_points_assignment.FormId).Read

    'TERMINALS CONFIGURATION
#If DEBUG Then
    'm_terminals_configuration.Status = CurrentUser.Permissions(m_terminals_configuration.FormId).Read
#End If

    ' PAYMENT ORDERS CONFIG
    m_payment_orders_config.Status = CurrentUser.Permissions(m_payment_orders_config.FormId).Read

    ' SKIPPED METERS REPORT
    m_skipped_meters_report.Status = CurrentUser.Permissions(m_skipped_meters_report.FormId).Read

    ' CAGE / VAULT - (BOVEDA)
    If TITO.Utils.IsCageEnabled Then
      m_CAGE_amounts.Status = CurrentUser.Permissions(m_CAGE_amounts.FormId).Read
      m_CAGE_control.Status = CurrentUser.Permissions(m_CAGE_control.FormId).Read
      m_CAGE_new.Status = CurrentUser.Permissions(m_CAGE_new.FormId).Read
      m_CAGE_Sessions.Status = CurrentUser.Permissions(m_CAGE_Sessions.FormId).Read
      m_CAGE_Source_Target.Status = CurrentUser.Permissions(m_CAGE_Source_Target.FormId).Read
    End If



    ' PLAYER EDIT
    m_player_edit_config.Status = CurrentUser.Permissions(m_player_edit_config.FormId).Read

    ' ACCOUNT CARD CHANGES
    m_account_card_changes.Status = CurrentUser.Permissions(m_account_card_changes.FormId).Read

    ' MONEY LAUNDERING
    m_money_laundering_config.Status = CurrentUser.Permissions(m_money_laundering_config.FormId).Read
    m_money_laundering_report_prize.Status = CurrentUser.Permissions(m_money_laundering_report_prize.FormId).Read
    m_money_laundering_report_recharge.Status = CurrentUser.Permissions(m_money_laundering_report_recharge.FormId).Read

    ' MULTISTE CONFIG
    m_multiSite_config.Status = CurrentUser.Permissions(m_multiSite_config.FormId).Read

    If GeneralParam.GetBoolean("Site", "MultiSiteMember") Then
      m_multisite_monitor.Status = CurrentUser.Permissions(m_multisite_monitor.FormId).Read
    End If

    If Not GeneralParam.GetBoolean("Site", "Configured") Then
      m_site_config_Click(Me, Nothing)
    End If

    ' CASHDESK DRAWS CONFIG
    m_cashdesk_draws_config.Status = CurrentUser.Permissions(m_cashdesk_draws_config.FormId).Read

  End Sub

  ' PURPOSE: First screen activation actions:
  '           - Set form title
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_FirstActivation()

    ' Retrieve Site Identifier and Name from database just once
    'GLB_SiteId = GetSiteId()
    'GLB_SiteName = GetSiteName()

    SetMainTitle()

  End Sub ' GUI_FirstActivation

  ' PURPOSE: Menu creation
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_CreateMenu()
    Dim _cls_menu As CLASS_MENU_ITEM
    'Dim _import_menu As CLASS_MENU_ITEM

    '
    ' SYSTEM MENU
    '
    _cls_menu = m_mnu.AddItem(GLB_NLS_GUI_AUDITOR.Id(453), ENUM_FORM.FORM_MAIN)
    m_profiles_item = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_CONFIGURATION.Id(467), ENUM_FORM.FORM_USER_PROFILE_SEL, AddressOf m_profiles_item_Click)
    m_terminals = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_AUDITOR.Id(458), ENUM_FORM.FORM_TERMINALS_SELECTION, AddressOf m_terminals_Click)
    m_terminals_3gs = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_AUDITOR.Id(461), ENUM_FORM.FORM_TERMINALS_3GS_EDIT, AddressOf m_terminals_3gs_Click)
    m_pending_terminals = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_AUDITOR.Id(463), ENUM_FORM.FORM_TERMINALS_PENDING, AddressOf m_pending_terminals_Click)
    m_terminal_game_relation = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_CONFIGURATION.Id(468), ENUM_FORM.FORM_TERMINAL_GAME_RELATION, AddressOf m_terminal_game_relation_Click)
    m_providers = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1916), ENUM_FORM.FORM_PROVIDERS_GAMES_SEL, AddressOf m_providers_Click)
    m_configuration = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1063), ENUM_FORM.FORM_CURRENCIES)
    m_cashier_configuration = m_mnu.AddItem(m_configuration, GLB_NLS_GUI_CONFIGURATION.Id(451), ENUM_FORM.FORM_CASHIER_CONFIGURATION, AddressOf m_cashier_configuration_Click)
    m_ads_configuration = m_mnu.AddItem(m_configuration, GLB_NLS_GUI_PLAYER_TRACKING.Id(722), ENUM_FORM.FORM_ADS_CONFIGURATION, AddressOf m_ads_configuration_Click)
    m_mailing_programming = m_mnu.AddItem(m_configuration, GLB_NLS_GUI_STATISTICS.Id(451), ENUM_FORM.FORM_MAILING_PROGRAMMING_SEL, AddressOf m_mailing_programming_Click)
    m_currencies_accepted = m_mnu.AddItem(m_configuration, GLB_NLS_GUI_PLAYER_TRACKING.Id(1056), ENUM_FORM.FORM_CURRENCIES, AddressOf m_currencies_accepted_Click)
    m_password_configuration = m_mnu.AddItem(m_configuration, GLB_NLS_GUI_PLAYER_TRACKING.Id(1175), ENUM_FORM.FORM_PASSWORD_CONFIGURATION, AddressOf m_password_configuration_Click)
    m_cash_by_service = m_mnu.AddItem(m_configuration, GLB_NLS_GUI_PLAYER_TRACKING.Id(1294), ENUM_FORM.FORM_CASH_SERVICE, AddressOf m_cash_sevice_Click)
    m_payment_orders_config = m_mnu.AddItem(m_configuration, GLB_NLS_GUI_PLAYER_TRACKING.Id(1563), ENUM_FORM.FORM_PAYMENT_ORDER_CONFIG, AddressOf m_payment_orders_config_Click)
    m_player_edit_config = m_mnu.AddItem(m_configuration, GLB_NLS_GUI_PLAYER_TRACKING.Id(2478), ENUM_FORM.FORM_PLAYER_EDIT_CONFIG, AddressOf m_Player_edit_config_Click)
    m_operation_schedule = m_mnu.AddItem(m_configuration, GLB_NLS_GUI_PLAYER_TRACKING.Id(1663), ENUM_FORM.FORM_OPERATIONS_SCHEDULE, AddressOf m_operation_schedule_Click)
    m_multiSite_config = m_mnu.AddItem(m_configuration, GLB_NLS_GUI_PLAYER_TRACKING.Id(1773), ENUM_FORM.FORM_MULTISITE_CONFIG, AddressOf m_site_config_Click)
    m_money_laundering_config = m_mnu.AddItem(m_configuration, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2399, Accounts.getAMLName()), ENUM_FORM.FORM_MONEY_LAUNDERING_CONFIG, AddressOf m_Money_laundering_config_Click)
    m_cashdesk_draws_config = m_mnu.AddItem(m_configuration, GLB_NLS_GUI_PLAYER_TRACKING.Id(2283), ENUM_FORM.FORM_CASHDESK_DRAWS_CONFIGURATION, AddressOf m_Cashdesk_draws_configuration_click)

    m_currencies = m_mnu.AddItem(m_configuration, GLB_NLS_GUI_PLAYER_TRACKING.Id(2429), ENUM_FORM.FORM_CURRENCY_CONFIGURATION, AddressOf m_currencies_Click)
    m_machine_alias = m_mnu.AddItem(m_configuration, GLB_NLS_GUI_PLAYER_TRACKING.Id(1811), ENUM_FORM.FORM_MACHINE_ALIAS, AddressOf m_machine_alias_Click)
    'Add TITO Configuration
    m_tito_params = m_mnu.AddItem(m_configuration, GLB_NLS_GUI_PLAYER_TRACKING.Id(2303), ENUM_FORM.FORM_TITO_PARAMS_EDIT, AddressOf m_tito_params_Click)
    m_general_params = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_AUDITOR.Id(460), ENUM_FORM.FORM_GENERAL_PARAMS, AddressOf m_general_params_Click)
    m_service_monitor = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_SYSTEM_MONITOR.Id(451), ENUM_FORM.FORM_SERVICE_MONITOR, AddressOf m_service_monitor_Click)
    m_area_bank = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_CONTROLS.Id(425), ENUM_FORM.FORM_AREA_BANK, AddressOf m_area_bank_Click)
    m_terminals_group = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_CONTROLS.Id(460), ENUM_FORM.FORM_GROUPS_SEL, AddressOf m_terminals_group_Click)
    m_import = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1238), ENUM_FORM.FORM_CURRENCIES, Nothing)
    m_accounts_import = m_mnu.AddItem(m_import, GLB_NLS_GUI_PLAYER_TRACKING.Id(1237), ENUM_FORM.FORM_ACCOUNTS_IMPORT, AddressOf m_accounts_import_Click)
    m_mnu.AddBreak(_cls_menu)
    m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_AUDITOR.Id(452), 0, AddressOf m_Exit_Click)

    '
    ' AUDIT MENU
    '
    _cls_menu = m_mnu.AddItem(GLB_NLS_GUI_AUDITOR.Id(459), ENUM_FORM.FORM_MAIN)
    m_auditor = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_AUDITOR.Id(451), ENUM_FORM.FORM_GUI_AUDITOR, AddressOf m_auditor_Click)

    '
    ' EVENTS MENU
    '
    _cls_menu = m_mnu.AddItem(GLB_NLS_GUI_AUDITOR.Id(455), ENUM_FORM.FORM_MAIN)
    m_event_history = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_AUDITOR.Id(457), ENUM_FORM.FORM_EVENTS_HISTORY, AddressOf m_event_history_Click)
    m_alarms = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_ALARMS.Id(454), ENUM_FORM.FORM_ALARMS, AddressOf m_alarms_Click)

    '
    ' STATISTICS MENU
    '
    _cls_menu = m_mnu.AddItem(GLB_NLS_GUI_STATISTICS.Id(201), ENUM_FORM.FORM_MAIN)
    m_statistics_terminals = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_STATISTICS.Id(350), ENUM_FORM.FORM_STATISTICS_TERMINALS, AddressOf m_Statistics_terminals_Click)
    m_statistics_date = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_STATISTICS.Id(352), ENUM_FORM.FORM_STATISTICS_DATES, AddressOf m_statistics_date_Click)
    m_statistics_games = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_STATISTICS.Id(351), ENUM_FORM.FORM_STATISTICS_GAMES, AddressOf m_Statistics_games_Click)
    m_statistics_hours_group = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_STATISTICS.Id(365), ENUM_FORM.FORM_STATISTICS_HOURS_GROUP, AddressOf m_statistics_hours_group_Click)
    m_statistics_hours = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_STATISTICS.Id(353), ENUM_FORM.FORM_STATISTICS_HOURS, AddressOf m_statistics_hours_Click)
    m_date_provider_terminal = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_INVOICING.Id(457), ENUM_FORM.FORM_DATE_TERMINAL, AddressOf m_date_provider_terminal_Click)
    m_mnu.AddBreak(_cls_menu)
    m_capacity_by_provider = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_AUDITOR.Id(470), ENUM_FORM.FORM_CAPACITY, AddressOf m_capacity_by_provider_Click)
    m_customer_base = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(551), ENUM_FORM.FORM_CUSTOMER_BASE, AddressOf m_customer_base_Click)
    m_play_preferences = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_STATISTICS.Id(499), ENUM_FORM.FORM_PLAY_PREFERENCES, AddressOf m_play_preferences_Click)
    m_machine_ocupation = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_STATISTICS.Id(444), ENUM_FORM.FORM_MACHINE_OCCUPATION, AddressOf m_machine_ocupation_Click)
    m_played_by_customer_and_provider = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(570), ENUM_FORM.FORM_CUSTOMER_SEGMENTATION, AddressOf m_played_by_customer_and_provider_Click)
    m_productivity_reports = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_STATISTICS.Id(270), ENUM_FORM.FORM_PRODUCTIVITY_REPORTS, AddressOf m_productivity_reports_Click)
    m_segmentation_reports = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_STATISTICS.Id(244), ENUM_FORM.FORM_SEGMENTATION_REPORTS, AddressOf m_segmentation_reports_Click)
    m_detailed_awards = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_STATISTICS.Id(489), ENUM_FORM.FORM_DETAILED_PRIZES, AddressOf m_detailed_awards_Click)
    m_stats_manual_adjustment = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1371), ENUM_FORM.FORM_STATS_MANUAL_ADJUSTMENT, AddressOf m_stats_manual_adjustment_Click)

    '
    ' PLAYS MENU
    '
    _cls_menu = m_mnu.AddItem(GLB_NLS_GUI_STATISTICS.Id(354), ENUM_FORM.FORM_MAIN)
    m_plays_sessions = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_STATISTICS.Id(357), ENUM_FORM.FORM_PLAYS_SESSIONS, AddressOf m_plays_sessions_Click)
    m_points_assignment = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2814), ENUM_FORM.FORM_POINTS_ASSIGNMENT, AddressOf m_points_assignment_Click)

    '
    ' MONITOR MENU
    '
    _cls_menu = m_mnu.AddItem(GLB_NLS_GUI_STATISTICS.Id(356), ENUM_FORM.FORM_MAIN)
    m_sessions = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_STATISTICS.Id(355), ENUM_FORM.FORM_WS_SESSIONS, AddressOf m_sessions_Click)
    m_plays_counter = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_STATISTICS.Id(395), ENUM_FORM.FORM_GAME_METERS, AddressOf m_plays_counter_Click)
    m_commands = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_CONTROLS.Id(451), ENUM_FORM.FORM_COMMANDS_SEND, Nothing)
    m_commands_send = m_mnu.AddItem(m_commands, GLB_NLS_GUI_CONTROLS.Id(452), ENUM_FORM.FORM_COMMANDS_SEND, AddressOf m_commands_send_Click)
    m_commands_history = m_mnu.AddItem(m_commands, GLB_NLS_GUI_CONTROLS.Id(453), ENUM_FORM.FORM_COMMANDS_HISTORY, AddressOf m_commands_history_Click)
    m_terminals_status = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_SW_DOWNLOAD.Id(453), ENUM_FORM.FORM_TERMINALS_BY_BUILD, AddressOf m_terminal_status_Click)
    m_terminals_without_activity = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_INVOICING.Id(495), ENUM_FORM.FORM_TERMINALS_WITHOUT_ACTIVITY, AddressOf m_Terminal_without_activity_Click)
    m_skipped_meters_report = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2937), ENUM_FORM.FORM_SKIPPED_METERS_REPORT, AddressOf m_skipped_meters_report_Click)
    If WSI.Common.Misc.ReadGeneralParams("ExternalProtocol", "Enabled") Then
      m_peru_frames = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1083), ENUM_FORM.FORM_PERU_FRAMES, AddressOf m_peru_frames_Click)
    End If
    If GeneralParam.GetBoolean("Site", "MultiSiteMember") Then
      m_multisite_monitor = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1787), ENUM_FORM.FORM_MULTISITE_MONITOR, AddressOf m_multisite_status_Click)
    End If

    '
    ' ACCOUNTING MENU
    '
    _cls_menu = m_mnu.AddItem(GLB_NLS_GUI_INVOICING.Id(451), ENUM_FORM.FORM_MAIN)
    m_expired_credits = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_INVOICING.Id(469), ENUM_FORM.FORM_EXPIRED_CREDITS, AddressOf m_expired_credits_Click)
    m_accounts_menu = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1237), ENUM_FORM.FORM_ACCOUNT_SUMMARY)
    m_card_summary = m_mnu.AddItem(m_accounts_menu, GLB_NLS_GUI_INVOICING.Id(455), ENUM_FORM.FORM_ACCOUNT_SUMMARY, AddressOf m_card_summary_Click)
    m_card_movements = m_mnu.AddItem(m_accounts_menu, GLB_NLS_GUI_INVOICING.Id(456), ENUM_FORM.FORM_ACCOUNT_MOVEMENTS, AddressOf m_card_movements_Click)
    m_account_card_changes = m_mnu.AddItem(m_accounts_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2804), ENUM_FORM.FORM_ACCOUNT_CARD_CHANGES, AddressOf m_Account_Card_Changes_Click)
    m_manual_assignment_report = m_mnu.AddItem(m_accounts_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1257), ENUM_FORM.FORM_ACCOUNT_MOVEMENTS_DETAILS, AddressOf m_manual_assignment_report_Click)
    m_cashier_sessions = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_INVOICING.Id(453), ENUM_FORM.FORM_CASHIER_SESSIONS, AddressOf m_cashier_sessions_Click)
    m_cashier_movements = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_INVOICING.Id(454), ENUM_FORM.FORM_CASHIER_MOVS, AddressOf m_cashier_movements_Click)
    m_cash_summary = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1632), ENUM_FORM.FORM_CASH_SUMMARY, AddressOf m_cash_summary_Click)
    m_taxes_resume = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_INVOICING.Id(452), ENUM_FORM.FORM_ACCT_TAXES, AddressOf m_taxes_resume_Click)
    m_provider_activity = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_INVOICING.Id(465), ENUM_FORM.FORM_PROVIDER_DAY_TERMINAL, AddressOf m_provider_activity_Click)
    m_hand_pays = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_INVOICING.Id(481), ENUM_FORM.FORM_HANDPAYS, AddressOf m_hand_pays_Click)
    m_taxes_report = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_INVOICING.Id(346), ENUM_FORM.FORM_TAXES_REPORT, AddressOf m_taxes_report_Click)
    m_statistics_quadrature = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_CONFIGURATION.Id(482), ENUM_FORM.FORM_SITE_GAP_REPORT, AddressOf m_statistics_quadrature_Click)
    m_provider_cash_activity = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_CONFIGURATION.Id(486), ENUM_FORM.FORM_CASH_REPORT, AddressOf m_provider_cash_activity_Click)
    m_transfer_quadrature = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_STATISTICS.Id(266), ENUM_FORM.FORM_TF_GAP_REPORT, AddressOf m_transfer_quadrature_Click)
    m_export = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_STATISTICS.Id(480), ENUM_FORM.FORM_REPORTS, AddressOf m_export_click)
    m_voucher_browser = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(606), ENUM_FORM.FORM_VOUCHER_BROWSER, AddressOf m_voucher_browser_Click)
    m_prize_taxes_report = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_INVOICING.Id(449), ENUM_FORM.FORM_PRIZE_TAXES_REPORT, AddressOf m_prize_taxes_Click)
    If WSI.Common.Misc.ReadGeneralParams("NoteAcceptor", "Enabled") Then
      m_note_acceptor = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_ALARMS.Id(296), ENUM_FORM.FORM_TERMINAL_MONEY)
      m_accepted_notes = m_mnu.AddItem(m_note_acceptor, GLB_NLS_GUI_INVOICING.Id(448), ENUM_FORM.FORM_TERMINAL_MONEY, AddressOf m_accepted_notes_Click)
      m_money_pending_transfer = m_mnu.AddItem(m_note_acceptor, GLB_NLS_GUI_INVOICING.Id(35), ENUM_FORM.FORM_MONEY_PENDING_TRANSFER, AddressOf m_money_pending_transfer_Click)
      m_money_collections = m_mnu.AddItem(m_note_acceptor, GLB_NLS_GUI_PLAYER_TRACKING.Id(641), ENUM_FORM.FORM_MONEY_COLLECTIONS, AddressOf m_money_collections_Click)
    End If
    m_tickets_menu = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_INVOICING.Id(382), ENUM_FORM.FORM_TICKETS_REPORT, Nothing)
    m_tickets_report = m_mnu.AddItem(m_tickets_menu, GLB_NLS_GUI_INVOICING.Id(383), ENUM_FORM.FORM_TICKETS_REPORT, AddressOf m_tickets_report_Click)
    m_retreats_report = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1360), ENUM_FORM.FORM_RETREATS_REPORT, AddressOf m_retreats_report_Click)
    m_service_charge_report = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2214), ENUM_FORM.FORM_SERVICE_CHARGE_REPORT, AddressOf m_Service_Charge_report_Click)
    m_payment_orders = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1563), ENUM_FORM.FORM_PAYMENT_ORDER_SEL, AddressOf m_payment_orders_Click)
    m_currency_exchange_audit = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2082), ENUM_FORM.FORM_CURRENCY_EXCHANGE_AUDIT, AddressOf m_currency_exchange_audit_Click)
    m_money_laundering_report = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2409, Accounts.getAMLName()), ENUM_FORM.FORM_MONEY_LAUNDERING_REPORT_PRIZE)
    m_money_laundering_report_prize = m_mnu.AddItem(m_money_laundering_report, GLB_NLS_GUI_PLAYER_TRACKING.Id(2522), ENUM_FORM.FORM_MONEY_LAUNDERING_REPORT_PRIZE, AddressOf m_Money_laundering_report_prize_Click)
    m_money_laundering_report_recharge = m_mnu.AddItem(m_money_laundering_report, GLB_NLS_GUI_PLAYER_TRACKING.Id(2521), ENUM_FORM.FORM_MONEY_LAUNDERING_REPORT_RECHARGE, AddressOf m_Money_laundering_report_recharge_Click)

    ' 
    ' PROMOTIONS MENU // RCI 10-MAY-2010
    '
    _cls_menu = m_mnu.AddItem(GLB_NLS_GUI_PLAYER_TRACKING.Id(461), ENUM_FORM.FORM_MAIN)
    m_draws = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(463), ENUM_FORM.FORM_DRAWS, AddressOf m_draws_Click)
    m_promotions = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(462), ENUM_FORM.FORM_PROMOTIONS, AddressOf m_promotions_Click)
    m_periodical_promotions = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(438), ENUM_FORM.FORM_PROMOTIONS, AddressOf m_periodical_promotions_Click)
    m_preassigned_promotions = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1236), ENUM_FORM.FORM_PROMOTIONS, AddressOf m_preassigned_promotions_Click)
    m_promotion_report = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(808), ENUM_FORM.FORM_PROMOTION_REPORTS, AddressOf m_promotion_reports_Click)
    m_promotion_delivered_report = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1271), ENUM_FORM.FORM_PROMOTIONS_DELIVERED_REPORT, AddressOf m_promotions_delivered_report_Click)
    m_advertisement_campaing = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(690), ENUM_FORM.FORM_ADVERTISEMENT, AddressOf m_advertisement_campaing_Click)
    m_gifts = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(464), ENUM_FORM.FORM_GIFTS_SEL, Nothing)
    m_gifts_catalog = m_mnu.AddItem(m_gifts, GLB_NLS_GUI_PLAYER_TRACKING.Id(465), ENUM_FORM.FORM_GIFTS_SEL, AddressOf m_gifts_catalog_Click)
    m_gifts_history = m_mnu.AddItem(m_gifts, GLB_NLS_GUI_PLAYER_TRACKING.Id(466), ENUM_FORM.FORM_GIFTS_HISTORY, AddressOf m_gifts_history_Click)
    m_points_to_credit = m_mnu.AddItem(m_gifts, GLB_NLS_GUI_PLAYER_TRACKING.Id(3334), ENUM_FORM.FORM_POINTS_TO_CREDITS, AddressOf m_points_to_credit_Click)

#If DEBUG Then
    m_recommendation_report = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_STATISTICS.Id(493), ENUM_FORM.FORM_RECOMMENDATION_REPORT, AddressOf m_recommendation_report_Click)
#End If
    m_provider_excluded = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1054), ENUM_FORM.FORM_PROVIDER_DATA, AddressOf m_provider_excluded_Click)
    m_flags = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1258), ENUM_FORM.FORM_FLAGS_SEL, AddressOf m_flags_Click)
    m_account_flags = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1259), ENUM_FORM.FORM_ACCOUNT_FLAGS, AddressOf m_account_flags_Click)
    m_report_draw_tickets = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1810), ENUM_FORM.FORM_REPORT_DRAW_TICKET, AddressOf m_report_draw_tickets_Click)
    m_cashdesk_draws_report = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2245), ENUM_FORM.FORM_DRAWS_CASHDESK_DETAIL, AddressOf m_draws_cashdesk_report_click)
    m_cashdesk_draws_summary = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2364), ENUM_FORM.FORM_DRAWS_CASHDESK_SUMMARY, AddressOf m_draws_cashdesk_sumary_click)


    ' 
    ' JACKPOT
    '
    _cls_menu = m_mnu.AddItem(GLB_NLS_GUI_CLASS_II.Id(453), ENUM_FORM.FORM_MAIN)
    m_site_jackpot = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_JACKPOT_MGR.Id(466), ENUM_FORM.FORM_SITE_JACKPOT_CONFIGURATION)
    m_site_jackpot_configuration = m_mnu.AddItem(m_site_jackpot, GLB_NLS_GUI_JACKPOT_MGR.Id(468), ENUM_FORM.FORM_SITE_JACKPOT_CONFIGURATION, AddressOf m_site_jackpot_configuration_Click)
    m_site_jackpot_history = m_mnu.AddItem(m_site_jackpot, GLB_NLS_GUI_JACKPOT_MGR.Id(469), ENUM_FORM.FORM_SITE_JACKPOT_HISTORY, AddressOf m_site_jackpot_history_Click)
    m_class_II_jackpot = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_JACKPOT_MGR.Id(467), ENUM_FORM.FORM_CLASS_II_JACKPOT_CONFIGURATION)
    m_jackpot_configuration = m_mnu.AddItem(m_class_II_jackpot, GLB_NLS_GUI_CLASS_II.Id(454), ENUM_FORM.FORM_CLASS_II_JACKPOT_CONFIGURATION, AddressOf m_jackpot_configuration_Click)
    m_jackpot_promo_messages = m_mnu.AddItem(m_class_II_jackpot, GLB_NLS_GUI_CLASS_II.Id(459), ENUM_FORM.FORM_CLASS_II_JACKPOT_PROMO_MESSAGE, AddressOf m_jackpot_promo_messages_Click)
    m_jackpot_history = m_mnu.AddItem(m_class_II_jackpot, GLB_NLS_GUI_CLASS_II.Id(455), ENUM_FORM.FORM_CLASS_II_JACKPOT_HISTORY, AddressOf m_jackpot_history_Click)
    m_jackpot_monitor = m_mnu.AddItem(m_class_II_jackpot, GLB_NLS_GUI_CLASS_II.Id(456), ENUM_FORM.FORM_CLASS_II_JACKPOT_MONITOR, AddressOf m_jackpot_monitor_Click)

    ' CAGE / VAULT - (BOVEDA)
    If TITO.Utils.IsCageEnabled Then
      m_CAGE_menu = m_mnu.AddItem(GLB_NLS_GUI_PLAYER_TRACKING.Id(2876), ENUM_FORM.FORM_CAGE)
      m_CAGE_amounts = m_mnu.AddItem(m_CAGE_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(3000), ENUM_FORM.FORM_CAGE_AMOUNTS, AddressOf m_CAGE_amounts_Click)
      m_CAGE_Source_Target = m_mnu.AddItem(m_CAGE_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(3376), ENUM_FORM.FORM_CAGE_SOURCE_TARGET, AddressOf m_CAGE_Source_Target_new_Click)
      m_CAGE_Sessions = m_mnu.AddItem(m_CAGE_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(3379), ENUM_FORM.FORM_CAGE_SESSIONS, AddressOf m_CAGE_Sessions_Click)
      m_CAGE_control = m_mnu.AddItem(m_CAGE_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2999), ENUM_FORM.FORM_CAGE_CONTROL, AddressOf m_CAGE_control_Click)
      m_CAGE_new = m_mnu.AddItem(m_CAGE_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(3001), ENUM_FORM.FORM_CAGE_CONTROL, AddressOf m_CAGE_new_Click)

      'Add TITO menu
      _cls_menu = m_mnu.AddItem(m_CAGE_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2303), ENUM_FORM.FORM_MAIN)

      'Add Circulating Cash form
      m_circulating_cash = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(3322), ENUM_FORM.FORM_CASH_TITO_SEL, AddressOf m_circulating_cash_Click)

      'Add ticket report form
      m_ticket_tito_report = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2124), ENUM_FORM.FORM_TITO_TICKET_REPORT, AddressOf m_ticket_report_Click)

      'Add Stacker sel form
      m_stacker_sel = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2162), ENUM_FORM.FORM_STACKERS_SEL, AddressOf m_stacker_sel_Click)

      'Add Stacker Collection Selection form
      m_stacker_collection_sel = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2222), ENUM_FORM.FORM_STACKER_COLLECTION_SEL, AddressOf m_stacker_collection_sel_Click)

      ' ACM 22-NOV-2013 Add Terminal Sas Meters sel form
      m_terminal_sas_meters_sel = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1401), ENUM_FORM.FORM_TERMINAL_SAS_METERS_SEL, AddressOf m_terminal_sas_meters_sel_Click)

      ' JML 04-DEC-2013
      m_tito_collection_balance_report = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(3335), ENUM_FORM.FORM_TITO_COLLECTION_BALANCE_REPORT, AddressOf m_tito_collection_balance_report_Click)

      ' ACM 13-DEC-2013 Add tito summary sel form
      m_tito_summary_sel = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(1403), ENUM_FORM.FORM_TITO_SUMMARY_SEL, AddressOf m_tito_summary_sel_Click)

      '' JMM 16-DEC-2013 Add tito summary sel form
      'm_bonuses_report = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(3389), ENUM_FORM.FORM_BONUSES, AddressOf m_bonuses_report_Click)

    End If

    ' 
    ' SW DOWNLOAD MENU -- 451 "SW Download" // TJG 05-SEP-2008
    '
    _cls_menu = m_mnu.AddItem(GLB_NLS_GUI_SW_DOWNLOAD.Id(451), ENUM_FORM.FORM_MAIN)
    m_download_versions = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_SW_DOWNLOAD.Id(452), ENUM_FORM.FORM_SW_DOWNLOAD_VERSIONS, AddressOf m_sw_download_versions_Click)
    m_misc_sw_build = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_SW_DOWNLOAD.Id(454), ENUM_FORM.FORM_MISC_SW_BY_BUILD, AddressOf m_misc_sw_build_Click)
    m_licenses = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_SW_DOWNLOAD.Id(455), ENUM_FORM.FORM_LICENCE_VERSIONS, AddressOf m_license_Click)

    '
    ' TOOLS MENU -- 457 "Herramientas"
    '
    If GLB_CurrentUser.IsSuperUser Then
      _cls_menu = m_mnu.AddItem(GLB_NLS_GUI_CLASS_II.Id(457), ENUM_FORM.FORM_MAIN)
      m_magnetic_card_writer = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_CLASS_II.Id(458), ENUM_FORM.FORM_CARD_RECORD, AddressOf m_magnetic_card_writer_Click)
    End If


#If DEBUG Then
    'm_terminals_configuration = m_mnu.AddItem(_cls_menu, GLB_NLS_GUI_PLAYER_TRACKING.Id(2698), ENUM_FORM.FORM_TERMINALS_CONFIGURATION, AddressOf m_terminals_configuration_Click)
#End If

  End Sub 'GUI_CreateMenu


  ' PURPOSE: GUI Exit actions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_Exit()

    MyBase.GUI_Exit()

    'logout the user
    Call GLB_CurrentUser.Logout()

    ' Exit All threads ...
    Environment.Exit(0)

  End Sub ' GUI_Exit

#End Region

#Region " Public Functions "

  Public Sub ShowMainWindow()

    Me.Display(True)

  End Sub
#End Region

#Region " Private Functions"
  Private Sub SetMainTitle()

    Dim site_id As String
    Dim site_name As String

    lbl_main_title.Value = GLB_NLS_GUI_AUDITOR.GetString(100) & Space(2) & "[" & WGDB.GetAppVersion() & "]"

    site_id = GetSiteId()
    site_name = GetSiteName()

    If CurrentUser.IsLogged Then
      lbl_main_title.Value = lbl_main_title.Value & " - " & CurrentUser.Name & "@" & GLB_DbServerName
      If GLB_DbServerName <> "" Then
        lbl_main_title.Value = lbl_main_title.Value & " - @" & GLB_DbServerName
      End If
    End If

    Me.Text = lbl_main_title.Value

  End Sub ' SetMainTitle

#End Region

#Region "Events"

  Private Sub m_profiles_item_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_user_profiles_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    _frm = New frm_user_profiles_sel
    Call _frm.ShowForEdit(Me)
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_terminals_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_terminals

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_terminals
    Call _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_terminals_3gs_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_terminals_3gs_edit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    _frm = New frm_terminals_3gs_edit
    Call _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_pending_terminals_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_terminals_pending

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    _frm = New frm_terminals_pending
    Call _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_terminal_game_relation_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_terminal_game_relation

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_terminal_game_relation
    Call _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_providers_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_providers_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_providers_sel()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_cashier_configuration_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cashier_configuration

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_cashier_configuration
    _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_mailing_programming_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_mailing_programming_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_mailing_programming_sel
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_currencies_accepted_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_currencies

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_currencies
    _frm.ShowEditItem(Nothing)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_password_configuration_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_password_configuration

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_password_configuration()
    _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_cash_sevice_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_service_charge_edit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_service_charge_edit()
    _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_operation_schedule_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_operations_schedule

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_operations_schedule()
    _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_machine_alias_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_machine_alias

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    _frm = New frm_machine_alias()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_general_params_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_genaral_params

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_genaral_params
    Call _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_service_monitor_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_service_monitor

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_service_monitor
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_area_bank_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_area_bank_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_area_bank_sel
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_accounts_import_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_accounts_import

    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    _frm = New frm_accounts_import
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_terminals_group_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_groups_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_groups_sel
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Exit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    Me.Close()
    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_auditor_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_auditor

    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    _frm = New frm_auditor

    Call _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_event_history_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_events_history

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_events_history
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_alarms_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_alarms

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_alarms
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Statistics_terminals_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_statistics_terminals

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_statistics_terminals
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_statistics_date_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_statistics_general

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_statistics_general
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Statistics_games_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_statistics_games

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_statistics_games
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_statistics_hours_group_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_statistics_hours_group

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_statistics_hours_group
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_statistics_hours_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_statistics_hours_accumulated

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_statistics_hours_accumulated
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_date_provider_terminal_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_sales_date_terminal

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_sales_date_terminal
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_capacity_by_provider_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_capacity

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_capacity
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_customer_base_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_customer_base

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_customer_base
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_play_preferences_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_play_preferences

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_play_preferences()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_machine_ocupation_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_machine_occupation

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_machine_occupation
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_played_by_customer_and_provider_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_customer_segmentation

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_customer_segmentation
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_productivity_reports_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_productivity_reports

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_productivity_reports
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_segmentation_reports_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_segmentation_reports

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_segmentation_reports
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_detailed_awards_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_detailed_prizes

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_detailed_prizes
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_stats_manual_adjustment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_stats_manual_adjustment

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    _frm = New frm_stats_manual_adjustment()
    Call _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_plays_sessions_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_plays_sessions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_plays_sessions
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_points_assignment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_plays_sessions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_plays_sessions(ENUM_FORM.FORM_POINTS_ASSIGNMENT)
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_sessions_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_wcp_sessions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_wcp_sessions
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_plays_counter_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_game_meters

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_game_meters
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_commands_send_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_command_send

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_command_send
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_commands_history_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_command_history

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_command_history
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_terminal_status_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_terminal_sw_version

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_terminal_sw_version
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Terminal_without_activity_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_terminals_without_activity

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_terminals_without_activity
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_peru_frames_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_peru_frames

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_peru_frames()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_expired_credits_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_expired_credits

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_expired_credits
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_card_summary_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_account_summary

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_account_summary
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_card_movements_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_account_movements

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_account_movements
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_manual_assignment_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_account_movements

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_account_movements(ENUM_FORM.FORM_ACCOUNT_MOVEMENTS_DETAILS)
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_cashier_sessions_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cashier_sessions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_cashier_sessions
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_cashier_movements_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cashier_movements

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_cashier_movements
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_cash_summary_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_reports

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_reports(ENUM_FORM.FORM_CASH_SUMMARY)

    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_taxes_resume_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_acct_taxes

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_acct_taxes
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_provider_activity_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_provider_day_terminal

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_provider_day_terminal
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_hand_pays_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_handpays

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_handpays
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_taxes_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_taxes_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_taxes_report
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_statistics_quadrature_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_statistics_providers

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_statistics_providers
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_provider_cash_activity_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cash_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_cash_report
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_voucher_browser_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_voucher_browser

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_voucher_browser
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_transfer_quadrature_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_comparation_meters

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_comparation_meters
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_export_click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_reports

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_reports
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_prize_taxes_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_prize_taxes_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_prize_taxes_report
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_accepted_notes_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_terminal_money

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_terminal_money
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_money_pending_transfer_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_money_pending_transfer

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_money_pending_transfer
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_tickets_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_tickets_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_tickets_report
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_retreats_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_retreats_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_retreats_report
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Service_Charge_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_retreats_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_retreats_report(ENUM_FORM.FORM_SERVICE_CHARGE_REPORT)
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_payment_orders_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_payment_order_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_payment_order_sel()

    Call _frm.ShowForEdit(Me)

  End Sub

  Private Sub m_currency_exchange_audit_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_currency_exchange_audit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    _frm = New frm_currency_exchange_audit()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_draws_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_draws_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_draws_sel
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_promotions_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_promotions_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_promotions_sel
    _frm.ShowForEdit(Me, frm_promotions_sel.ENUM_PROMO_TYPE_MODE.TYPE_MANUAL)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_periodical_promotions_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_promotions_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_promotions_sel
    _frm.ShowForEdit(Me, frm_promotions_sel.ENUM_PROMO_TYPE_MODE.TYPE_AUTOMATIC)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_preassigned_promotions_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_promotions_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_promotions_sel
    _frm.ShowForEdit(Me, frm_promotions_sel.ENUM_PROMO_TYPE_MODE.TYPE_PREASSIGNED)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_promotion_reports_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_automatic_promotions_reports

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_automatic_promotions_reports
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_promotions_delivered_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_promotions_delivered_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_promotions_delivered_report
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_advertisement_campaing_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_advertisement

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_advertisement()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_gifts_catalog_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_gift_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_gift_sel
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_gifts_history_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_gift_history

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_gift_history
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_recommendation_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_recommendation_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_recommendation_report
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_provider_excluded_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_provider_data

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_provider_data
    _frm.ShowEditItem(Nothing)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_flags_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_flags_sel

    Windows.Forms.Cursor.Current = Cursors.Default

    _frm = New frm_flags_sel
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_account_flags_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_account_flags

    Windows.Forms.Cursor.Current = Cursors.Default

    _frm = New frm_account_flags
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_report_draw_tickets_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_report_draw_tickets

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    _frm = New frm_report_draw_tickets
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_draws_cashdesk_report_click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_draws_cashdesk_detail_sel

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    _frm = New frm_draws_cashdesk_detail_sel
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_draws_cashdesk_sumary_click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_draws_cashdesk_summary_sel

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    _frm = New frm_draws_cashdesk_summary_sel
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_site_jackpot_configuration_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_site_jackpot_configuration

    Windows.Forms.Cursor.Current = Cursors.Default

    _frm = New frm_site_jackpot_configuration
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_site_jackpot_history_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_site_jackpot_history

    Windows.Forms.Cursor.Current = Cursors.Default

    _frm = New frm_site_jackpot_history
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_jackpot_configuration_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_jackpot_configuration

    Windows.Forms.Cursor.Current = Cursors.Default

    _frm = New frm_jackpot_configuration
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_jackpot_promo_messages_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_jackpot_promo_messages

    Windows.Forms.Cursor.Current = Cursors.Default

    _frm = New frm_jackpot_promo_messages
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_jackpot_history_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_jackpot_history

    Windows.Forms.Cursor.Current = Cursors.Default

    _frm = New frm_jackpot_history
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub


  Private Sub m_jackpot_monitor_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_jackpot_monitor

    Windows.Forms.Cursor.Current = Cursors.Default

    _frm = New frm_jackpot_monitor
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_sw_download_versions_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_sw_package_versions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_sw_package_versions
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_misc_sw_build_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_misc_sw_version

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_misc_sw_version
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_license_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_licence_versions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_licence_versions
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_magnetic_card_writer_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_card_writer

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_card_writer
    _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub



  ' PURPOSE: Create and show a form's instance.
  '
  '  PARAMS:
  '     - INPUT:
  '         - sender: Object.
  '         - e:      System.EventArgs.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub m_tito_params_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_tito_params_edit
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    _frm = New frm_tito_params_edit()
    _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_stacker_collection_sel_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_stacker_collections_sel
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_stacker_collections_sel()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_circulating_cash_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_tito_cash_sel
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_tito_cash_sel()
    _frm.Show(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_stacker_sel_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_tito_stackers_sel
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_tito_stackers_sel()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_ticket_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim _frm As frm_tito_reports_sel
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_tito_reports_sel()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_currencies_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_currency_configuration
    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_currency_configuration()
    _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_points_to_credit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_points_to_credit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_points_to_credit
    _frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_terminal_sas_meters_sel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_terminal_sas_meters_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_terminal_sas_meters_sel()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_tito_summary_sel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_tito_summary_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_tito_summary_sel()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' RMS 02-DEC-2013

  Private Sub m_payment_orders_config_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_payment_orders_config

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    frm = New frm_payment_orders_config()

    Call frm.ShowForConfig()

  End Sub

  Private Sub m_skipped_meters_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_skipped_meters_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_skipped_meters_report
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' XMS 11-DEC-2013: Add new Form to Cage Menu
  Private Sub m_CAGE_Source_Target_new_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cage_source_target_edit

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_cage_source_target_edit
    _frm.ShowEditItem(Nothing)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  ' JAB 06-NOV-2013: Cage
  Private Sub m_CAGE_Sessions_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cage_sessions

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_cage_sessions()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_CAGE_amounts_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cage_amounts

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_cage_amounts
    _frm.ShowEditItem(Nothing)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_CAGE_control_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cage_movements_sel

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    _frm = New frm_cage_movements_sel
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_CAGE_new_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_cage_control
    Dim _cage_enabled As Boolean

    _cage_enabled = TITO.Utils.IsCageEnabled

    If Not _cage_enabled Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3026), ENUM_MB_TYPE.MB_TYPE_WARNING)
    Else
      Windows.Forms.Cursor.Current = Cursors.WaitCursor

      ' Create instance
      _frm = New frm_cage_control
      _frm.ShowNewItem()

      Windows.Forms.Cursor.Current = Cursors.Default
    End If
  End Sub

  Private Sub m_money_collections_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_money_collections

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_money_collections
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  Private Sub m_Player_edit_config_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_player_edit_config

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_player_edit_config()
    frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Account_Card_Changes_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_account_card_changes

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_account_card_changes
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_ads_configuration_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_ads_configuration

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_ads_configuration()
    'frm.ShowForEdit(Me)
    frm.ShowEditItem(Me)


    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_site_config_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_multisite_configuration

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_multisite_configuration()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Money_laundering_config_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim frm As frm_money_laundering_config

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_money_laundering_config()
    frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Cashdesk_draws_configuration_click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_cashdesk_draws_configuration_edit

    Windows.Forms.Cursor.Current = Cursors.Default

    ' Create instance
    frm = New frm_cashdesk_draws_configuration_edit
    frm.ShowEditItem(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_multisite_status_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_multisite_monitor

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance 
    frm = New frm_multisite_monitor()
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Money_laundering_report_prize_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_money_laundering_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_money_laundering_report(ENUM_FORM.FORM_MONEY_LAUNDERING_REPORT_PRIZE)
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  Private Sub m_Money_laundering_report_recharge_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Dim frm As frm_money_laundering_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    ' Create instance
    frm = New frm_money_laundering_report(ENUM_FORM.FORM_MONEY_LAUNDERING_REPORT_RECHARGE)
    frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default

  End Sub

  'JML 04-DEC-2013
  Private Sub m_tito_collection_balance_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_tito_collection_balance_report

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_tito_collection_balance_report()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

  'JMM 16-DEC-2013
  Private Sub m_bonuses_report_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim _frm As frm_bonuses

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    _frm = New frm_bonuses()
    _frm.ShowForEdit(Me)

    Windows.Forms.Cursor.Current = Cursors.Default
  End Sub

#End Region

End Class
