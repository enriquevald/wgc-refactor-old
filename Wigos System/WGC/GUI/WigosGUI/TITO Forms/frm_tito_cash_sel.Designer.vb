<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_tito_cash_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.uc_dates = New GUI_Controls.uc_daily_session_selector
    Me.gb_group_by = New System.Windows.Forms.GroupBox
    Me.lbl_group_by = New System.Windows.Forms.Label
    Me.opt_provider_day = New System.Windows.Forms.RadioButton
    Me.opt_day_terminal = New System.Windows.Forms.RadioButton
    Me.opt_terminal_day = New System.Windows.Forms.RadioButton
    Me.opt_day = New System.Windows.Forms.RadioButton
    Me.opt_terminal = New System.Windows.Forms.RadioButton
    Me.opt_provider = New System.Windows.Forms.RadioButton
    Me.chk_actual_state = New System.Windows.Forms.CheckBox
    Me.chk_show_without_activity = New System.Windows.Forms.CheckBox
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_group_by.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chk_show_without_activity)
    Me.panel_filter.Controls.Add(Me.chk_actual_state)
    Me.panel_filter.Controls.Add(Me.gb_group_by)
    Me.panel_filter.Controls.Add(Me.uc_dates)
    Me.panel_filter.Controls.Add(Me.uc_pr_list)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1197, 209)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_pr_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_dates, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_group_by, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_actual_state, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chk_show_without_activity, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 213)
    Me.panel_data.Size = New System.Drawing.Size(1197, 422)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1191, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1191, 4)
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(297, 3)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(329, 188)
    Me.uc_pr_list.TabIndex = 3
    '
    'uc_dates
    '
    Me.uc_dates.ClosingTime = 0
    Me.uc_dates.ClosingTimeEnabled = True
    Me.uc_dates.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dates.FromDateSelected = True
    Me.uc_dates.Location = New System.Drawing.Point(6, 6)
    Me.uc_dates.Name = "uc_dates"
    Me.uc_dates.Size = New System.Drawing.Size(285, 83)
    Me.uc_dates.TabIndex = 1
    Me.uc_dates.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_dates.ToDateSelected = True
    '
    'gb_group_by
    '
    Me.gb_group_by.Controls.Add(Me.lbl_group_by)
    Me.gb_group_by.Controls.Add(Me.opt_provider_day)
    Me.gb_group_by.Controls.Add(Me.opt_day_terminal)
    Me.gb_group_by.Controls.Add(Me.opt_terminal_day)
    Me.gb_group_by.Controls.Add(Me.opt_day)
    Me.gb_group_by.Controls.Add(Me.opt_terminal)
    Me.gb_group_by.Controls.Add(Me.opt_provider)
    Me.gb_group_by.Location = New System.Drawing.Point(6, 95)
    Me.gb_group_by.Name = "gb_group_by"
    Me.gb_group_by.Size = New System.Drawing.Size(285, 108)
    Me.gb_group_by.TabIndex = 2
    Me.gb_group_by.TabStop = False
    Me.gb_group_by.Text = "xOptions"
    '
    'lbl_group_by
    '
    Me.lbl_group_by.AutoSize = True
    Me.lbl_group_by.Location = New System.Drawing.Point(6, 17)
    Me.lbl_group_by.Name = "lbl_group_by"
    Me.lbl_group_by.Size = New System.Drawing.Size(64, 13)
    Me.lbl_group_by.TabIndex = 0
    Me.lbl_group_by.Text = "xGroupBy"
    '
    'opt_provider_day
    '
    Me.opt_provider_day.AutoSize = True
    Me.opt_provider_day.Location = New System.Drawing.Point(130, 57)
    Me.opt_provider_day.Name = "opt_provider_day"
    Me.opt_provider_day.Size = New System.Drawing.Size(130, 17)
    Me.opt_provider_day.TabIndex = 5
    Me.opt_provider_day.TabStop = True
    Me.opt_provider_day.Text = "xProvider and day"
    Me.opt_provider_day.UseVisualStyleBackColor = True
    '
    'opt_day_terminal
    '
    Me.opt_day_terminal.AutoSize = True
    Me.opt_day_terminal.Location = New System.Drawing.Point(130, 79)
    Me.opt_day_terminal.Name = "opt_day_terminal"
    Me.opt_day_terminal.Size = New System.Drawing.Size(131, 17)
    Me.opt_day_terminal.TabIndex = 6
    Me.opt_day_terminal.Text = "xDay and terminal"
    Me.opt_day_terminal.UseVisualStyleBackColor = True
    '
    'opt_terminal_day
    '
    Me.opt_terminal_day.AutoSize = True
    Me.opt_terminal_day.Location = New System.Drawing.Point(130, 35)
    Me.opt_terminal_day.Name = "opt_terminal_day"
    Me.opt_terminal_day.Size = New System.Drawing.Size(132, 17)
    Me.opt_terminal_day.TabIndex = 4
    Me.opt_terminal_day.TabStop = True
    Me.opt_terminal_day.Text = "xTerminal and day"
    Me.opt_terminal_day.UseVisualStyleBackColor = True
    '
    'opt_day
    '
    Me.opt_day.AutoSize = True
    Me.opt_day.Location = New System.Drawing.Point(9, 79)
    Me.opt_day.Name = "opt_day"
    Me.opt_day.Size = New System.Drawing.Size(55, 17)
    Me.opt_day.TabIndex = 3
    Me.opt_day.Text = "xDay"
    Me.opt_day.UseVisualStyleBackColor = True
    '
    'opt_terminal
    '
    Me.opt_terminal.AutoSize = True
    Me.opt_terminal.Location = New System.Drawing.Point(9, 35)
    Me.opt_terminal.Name = "opt_terminal"
    Me.opt_terminal.Size = New System.Drawing.Size(82, 17)
    Me.opt_terminal.TabIndex = 1
    Me.opt_terminal.TabStop = True
    Me.opt_terminal.Text = "xTerminal"
    Me.opt_terminal.UseVisualStyleBackColor = True
    '
    'opt_provider
    '
    Me.opt_provider.AutoSize = True
    Me.opt_provider.Location = New System.Drawing.Point(9, 57)
    Me.opt_provider.Name = "opt_provider"
    Me.opt_provider.Size = New System.Drawing.Size(80, 17)
    Me.opt_provider.TabIndex = 2
    Me.opt_provider.TabStop = True
    Me.opt_provider.Text = "xProvider"
    Me.opt_provider.UseVisualStyleBackColor = True
    '
    'chk_actual_state
    '
    Me.chk_actual_state.AutoSize = True
    Me.chk_actual_state.Location = New System.Drawing.Point(301, 189)
    Me.chk_actual_state.Name = "chk_actual_state"
    Me.chk_actual_state.Size = New System.Drawing.Size(109, 17)
    Me.chk_actual_state.TabIndex = 4
    Me.chk_actual_state.Text = "xShow current"
    Me.chk_actual_state.UseVisualStyleBackColor = True
    '
    'chk_show_without_activity
    '
    Me.chk_show_without_activity.AutoSize = True
    Me.chk_show_without_activity.Enabled = False
    Me.chk_show_without_activity.Location = New System.Drawing.Point(444, 189)
    Me.chk_show_without_activity.Name = "chk_show_without_activity"
    Me.chk_show_without_activity.Size = New System.Drawing.Size(154, 17)
    Me.chk_show_without_activity.TabIndex = 11
    Me.chk_show_without_activity.Text = "xShow without activity"
    Me.chk_show_without_activity.UseVisualStyleBackColor = True
    '
    'frm_tito_cash_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1207, 639)
    Me.Name = "frm_tito_cash_sel"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_cash_tito_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_group_by.ResumeLayout(False)
    Me.gb_group_by.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents uc_dates As GUI_Controls.uc_daily_session_selector
  Friend WithEvents gb_group_by As System.Windows.Forms.GroupBox
  Friend WithEvents opt_terminal As System.Windows.Forms.RadioButton
  Friend WithEvents opt_provider As System.Windows.Forms.RadioButton
  Friend WithEvents opt_day As System.Windows.Forms.RadioButton
  Friend WithEvents chk_actual_state As System.Windows.Forms.CheckBox
  Friend WithEvents opt_day_terminal As System.Windows.Forms.RadioButton
  Friend WithEvents opt_terminal_day As System.Windows.Forms.RadioButton
  Friend WithEvents opt_provider_day As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_group_by As System.Windows.Forms.Label
  Friend WithEvents chk_show_without_activity As System.Windows.Forms.CheckBox
End Class
