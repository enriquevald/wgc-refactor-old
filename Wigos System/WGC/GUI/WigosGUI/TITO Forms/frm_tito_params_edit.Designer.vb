<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_tito_params_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_tito_params_edit))
    Me.gb_tickets = New System.Windows.Forms.GroupBox()
    Me.ef_min_denomination_multiple = New GUI_Controls.uc_entry_field()
    Me.lbl_allow_truncate_info = New System.Windows.Forms.Label()
    Me.chk_allow_truncate = New System.Windows.Forms.CheckBox()
    Me.ef_min_allowed_ticket_in = New GUI_Controls.uc_entry_field()
    Me.ef_stacker_capacity = New GUI_Controls.uc_entry_field()
    Me.lbl_max_creation_info = New System.Windows.Forms.Label()
    Me.ef_max_allowed_creation = New GUI_Controls.uc_entry_field()
    Me.chk_promotional_redemption = New System.Windows.Forms.CheckBox()
    Me.chk_cashable_redemption = New System.Windows.Forms.CheckBox()
    Me.chk_allow_emission = New System.Windows.Forms.CheckBox()
    Me.ef_max_allowed_ATM_reedem = New GUI_Controls.uc_entry_field()
    Me.gb_income = New System.Windows.Forms.GroupBox()
    Me.chk_ticket_collection_cashier = New System.Windows.Forms.CheckBox()
    Me.chk_ticket_collection_terminal = New System.Windows.Forms.CheckBox()
    Me.cmb_detail_collection_level = New GUI_Controls.uc_combo()
    Me.gb_ticket_personalization = New System.Windows.Forms.GroupBox()
    Me.ef_cashable_ticket_title = New GUI_Controls.uc_entry_field()
    Me.ef_promo_non_redim_ticket_title = New GUI_Controls.uc_entry_field()
    Me.ef_location = New GUI_Controls.uc_entry_field()
    Me.ef_promo_redim_ticket_title = New GUI_Controls.uc_entry_field()
    Me.ef_debit_ticket_title = New GUI_Controls.uc_entry_field()
    Me.ef_address_2 = New GUI_Controls.uc_entry_field()
    Me.ef_address_1 = New GUI_Controls.uc_entry_field()
    Me.ef_tickets_limit_before_prize = New GUI_Controls.uc_entry_field()
    Me.gb_appearance = New System.Windows.Forms.GroupBox()
    Me.ef_shown_numbers = New GUI_Controls.uc_entry_field()
    Me.chk_hide_validation_number = New System.Windows.Forms.CheckBox()
    Me.gb_expiration_dates = New System.Windows.Forms.GroupBox()
    Me.ef_ticket_promotional_redeemable_expiration_days = New GUI_Controls.uc_entry_field()
    Me.ef_cashable_ticket_expiration_days = New GUI_Controls.uc_entry_field()
    Me.ef_ticket_promotional_nonredeemable_expiration_days = New GUI_Controls.uc_entry_field()
    Me.gb_prizes = New System.Windows.Forms.GroupBox()
    Me.lbl_also_applies_to_chips = New System.Windows.Forms.Label()
    Me.ef_jackpots_limit_before_prize = New GUI_Controls.uc_entry_field()
    Me.panel_data.SuspendLayout()
    Me.gb_tickets.SuspendLayout()
    Me.gb_income.SuspendLayout()
    Me.gb_ticket_personalization.SuspendLayout()
    Me.gb_appearance.SuspendLayout()
    Me.gb_expiration_dates.SuspendLayout()
    Me.gb_prizes.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_prizes)
    Me.panel_data.Controls.Add(Me.gb_expiration_dates)
    Me.panel_data.Controls.Add(Me.gb_appearance)
    Me.panel_data.Controls.Add(Me.gb_ticket_personalization)
    Me.panel_data.Controls.Add(Me.gb_tickets)
    Me.panel_data.Controls.Add(Me.gb_income)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(702, 668)
    '
    'gb_tickets
    '
    Me.gb_tickets.Controls.Add(Me.ef_min_denomination_multiple)
    Me.gb_tickets.Controls.Add(Me.lbl_allow_truncate_info)
    Me.gb_tickets.Controls.Add(Me.chk_allow_truncate)
    Me.gb_tickets.Controls.Add(Me.ef_min_allowed_ticket_in)
    Me.gb_tickets.Controls.Add(Me.ef_stacker_capacity)
    Me.gb_tickets.Controls.Add(Me.lbl_max_creation_info)
    Me.gb_tickets.Controls.Add(Me.ef_max_allowed_creation)
    Me.gb_tickets.Controls.Add(Me.chk_promotional_redemption)
    Me.gb_tickets.Controls.Add(Me.chk_cashable_redemption)
    Me.gb_tickets.Controls.Add(Me.chk_allow_emission)
    Me.gb_tickets.Controls.Add(Me.ef_max_allowed_ATM_reedem)
    Me.gb_tickets.Location = New System.Drawing.Point(7, 5)
    Me.gb_tickets.Name = "gb_tickets"
    Me.gb_tickets.Size = New System.Drawing.Size(386, 305)
    Me.gb_tickets.TabIndex = 0
    Me.gb_tickets.TabStop = False
    Me.gb_tickets.Text = "xTickets"
    '
    'ef_min_denomination_multiple
    '
    Me.ef_min_denomination_multiple.DoubleValue = 0.0R
    Me.ef_min_denomination_multiple.IntegerValue = 0
    Me.ef_min_denomination_multiple.IsReadOnly = False
    Me.ef_min_denomination_multiple.Location = New System.Drawing.Point(6, 234)
    Me.ef_min_denomination_multiple.Name = "ef_min_denomination_multiple"
    Me.ef_min_denomination_multiple.PlaceHolder = Nothing
    Me.ef_min_denomination_multiple.Size = New System.Drawing.Size(361, 25)
    Me.ef_min_denomination_multiple.SufixText = "Sufix Text"
    Me.ef_min_denomination_multiple.SufixTextVisible = True
    Me.ef_min_denomination_multiple.TabIndex = 8
    Me.ef_min_denomination_multiple.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_min_denomination_multiple.TextValue = ""
    Me.ef_min_denomination_multiple.TextWidth = 175
    Me.ef_min_denomination_multiple.Value = ""
    Me.ef_min_denomination_multiple.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_allow_truncate_info
    '
    Me.lbl_allow_truncate_info.AutoSize = True
    Me.lbl_allow_truncate_info.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_allow_truncate_info.ForeColor = System.Drawing.Color.Navy
    Me.lbl_allow_truncate_info.Location = New System.Drawing.Point(10, 282)
    Me.lbl_allow_truncate_info.Name = "lbl_allow_truncate_info"
    Me.lbl_allow_truncate_info.RightToLeft = System.Windows.Forms.RightToLeft.No
    Me.lbl_allow_truncate_info.Size = New System.Drawing.Size(42, 14)
    Me.lbl_allow_truncate_info.TabIndex = 9
    Me.lbl_allow_truncate_info.Text = "xInfo"
    '
    'chk_allow_truncate
    '
    Me.chk_allow_truncate.AutoSize = True
    Me.chk_allow_truncate.Location = New System.Drawing.Point(10, 218)
    Me.chk_allow_truncate.Name = "chk_allow_truncate"
    Me.chk_allow_truncate.Size = New System.Drawing.Size(106, 17)
    Me.chk_allow_truncate.TabIndex = 7
    Me.chk_allow_truncate.Text = "xAllowTrucate"
    Me.chk_allow_truncate.UseVisualStyleBackColor = True
    '
    'ef_min_allowed_ticket_in
    '
    Me.ef_min_allowed_ticket_in.DoubleValue = 0.0R
    Me.ef_min_allowed_ticket_in.IntegerValue = 0
    Me.ef_min_allowed_ticket_in.IsReadOnly = False
    Me.ef_min_allowed_ticket_in.Location = New System.Drawing.Point(6, 47)
    Me.ef_min_allowed_ticket_in.Name = "ef_min_allowed_ticket_in"
    Me.ef_min_allowed_ticket_in.PlaceHolder = Nothing
    Me.ef_min_allowed_ticket_in.Size = New System.Drawing.Size(361, 24)
    Me.ef_min_allowed_ticket_in.SufixText = "Sufix Text"
    Me.ef_min_allowed_ticket_in.SufixTextVisible = True
    Me.ef_min_allowed_ticket_in.TabIndex = 1
    Me.ef_min_allowed_ticket_in.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_min_allowed_ticket_in.TextValue = ""
    Me.ef_min_allowed_ticket_in.TextWidth = 175
    Me.ef_min_allowed_ticket_in.Value = ""
    Me.ef_min_allowed_ticket_in.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_stacker_capacity
    '
    Me.ef_stacker_capacity.DoubleValue = 0.0R
    Me.ef_stacker_capacity.IntegerValue = 0
    Me.ef_stacker_capacity.IsReadOnly = False
    Me.ef_stacker_capacity.Location = New System.Drawing.Point(6, 101)
    Me.ef_stacker_capacity.Name = "ef_stacker_capacity"
    Me.ef_stacker_capacity.PlaceHolder = Nothing
    Me.ef_stacker_capacity.Size = New System.Drawing.Size(361, 24)
    Me.ef_stacker_capacity.SufixText = "Sufix Text"
    Me.ef_stacker_capacity.SufixTextVisible = True
    Me.ef_stacker_capacity.TabIndex = 3
    Me.ef_stacker_capacity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_stacker_capacity.TextValue = ""
    Me.ef_stacker_capacity.TextWidth = 175
    Me.ef_stacker_capacity.Value = ""
    Me.ef_stacker_capacity.ValueForeColor = System.Drawing.Color.Blue
    '
    'lbl_max_creation_info
    '
    Me.lbl_max_creation_info.AutoSize = True
    Me.lbl_max_creation_info.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_max_creation_info.ForeColor = System.Drawing.Color.Navy
    Me.lbl_max_creation_info.Location = New System.Drawing.Point(10, 261)
    Me.lbl_max_creation_info.Name = "lbl_max_creation_info"
    Me.lbl_max_creation_info.RightToLeft = System.Windows.Forms.RightToLeft.No
    Me.lbl_max_creation_info.Size = New System.Drawing.Size(42, 14)
    Me.lbl_max_creation_info.TabIndex = 6
    Me.lbl_max_creation_info.Text = "xInfo"
    '
    'ef_max_allowed_creation
    '
    Me.ef_max_allowed_creation.DoubleValue = 0.0R
    Me.ef_max_allowed_creation.IntegerValue = 0
    Me.ef_max_allowed_creation.IsReadOnly = False
    Me.ef_max_allowed_creation.Location = New System.Drawing.Point(6, 74)
    Me.ef_max_allowed_creation.Name = "ef_max_allowed_creation"
    Me.ef_max_allowed_creation.PlaceHolder = Nothing
    Me.ef_max_allowed_creation.Size = New System.Drawing.Size(361, 24)
    Me.ef_max_allowed_creation.SufixText = "Sufix Text"
    Me.ef_max_allowed_creation.SufixTextVisible = True
    Me.ef_max_allowed_creation.TabIndex = 2
    Me.ef_max_allowed_creation.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_max_allowed_creation.TextValue = ""
    Me.ef_max_allowed_creation.TextWidth = 175
    Me.ef_max_allowed_creation.Value = ""
    Me.ef_max_allowed_creation.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_promotional_redemption
    '
    Me.chk_promotional_redemption.AutoSize = True
    Me.chk_promotional_redemption.Location = New System.Drawing.Point(10, 195)
    Me.chk_promotional_redemption.Name = "chk_promotional_redemption"
    Me.chk_promotional_redemption.Size = New System.Drawing.Size(169, 17)
    Me.chk_promotional_redemption.TabIndex = 6
    Me.chk_promotional_redemption.Text = "xPromotionalRedemption"
    Me.chk_promotional_redemption.UseVisualStyleBackColor = True
    '
    'chk_cashable_redemption
    '
    Me.chk_cashable_redemption.AutoSize = True
    Me.chk_cashable_redemption.Location = New System.Drawing.Point(10, 171)
    Me.chk_cashable_redemption.Name = "chk_cashable_redemption"
    Me.chk_cashable_redemption.Size = New System.Drawing.Size(154, 17)
    Me.chk_cashable_redemption.TabIndex = 5
    Me.chk_cashable_redemption.Text = "xCashableRedemption"
    Me.chk_cashable_redemption.UseVisualStyleBackColor = True
    '
    'chk_allow_emission
    '
    Me.chk_allow_emission.AutoSize = True
    Me.chk_allow_emission.Location = New System.Drawing.Point(10, 147)
    Me.chk_allow_emission.Name = "chk_allow_emission"
    Me.chk_allow_emission.Size = New System.Drawing.Size(83, 17)
    Me.chk_allow_emission.TabIndex = 4
    Me.chk_allow_emission.Text = "xEmission"
    Me.chk_allow_emission.UseVisualStyleBackColor = True
    '
    'ef_max_allowed_ATM_reedem
    '
    Me.ef_max_allowed_ATM_reedem.DoubleValue = 0.0R
    Me.ef_max_allowed_ATM_reedem.IntegerValue = 0
    Me.ef_max_allowed_ATM_reedem.IsReadOnly = False
    Me.ef_max_allowed_ATM_reedem.Location = New System.Drawing.Point(6, 20)
    Me.ef_max_allowed_ATM_reedem.Name = "ef_max_allowed_ATM_reedem"
    Me.ef_max_allowed_ATM_reedem.PlaceHolder = Nothing
    Me.ef_max_allowed_ATM_reedem.Size = New System.Drawing.Size(361, 24)
    Me.ef_max_allowed_ATM_reedem.SufixText = "Sufix Text"
    Me.ef_max_allowed_ATM_reedem.SufixTextVisible = True
    Me.ef_max_allowed_ATM_reedem.TabIndex = 0
    Me.ef_max_allowed_ATM_reedem.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_max_allowed_ATM_reedem.TextValue = ""
    Me.ef_max_allowed_ATM_reedem.TextWidth = 175
    Me.ef_max_allowed_ATM_reedem.Value = ""
    Me.ef_max_allowed_ATM_reedem.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_income
    '
    Me.gb_income.Controls.Add(Me.chk_ticket_collection_cashier)
    Me.gb_income.Controls.Add(Me.chk_ticket_collection_terminal)
    Me.gb_income.Controls.Add(Me.cmb_detail_collection_level)
    Me.gb_income.Location = New System.Drawing.Point(399, 5)
    Me.gb_income.Name = "gb_income"
    Me.gb_income.Size = New System.Drawing.Size(291, 122)
    Me.gb_income.TabIndex = 1
    Me.gb_income.TabStop = False
    Me.gb_income.Text = "xIncome"
    '
    'chk_ticket_collection_cashier
    '
    Me.chk_ticket_collection_cashier.Location = New System.Drawing.Point(8, 81)
    Me.chk_ticket_collection_cashier.Name = "chk_ticket_collection_cashier"
    Me.chk_ticket_collection_cashier.Size = New System.Drawing.Size(228, 23)
    Me.chk_ticket_collection_cashier.TabIndex = 2
    Me.chk_ticket_collection_cashier.Text = "xAllowTicketRedeemtionInCashier"
    Me.chk_ticket_collection_cashier.UseVisualStyleBackColor = True
    '
    'chk_ticket_collection_terminal
    '
    Me.chk_ticket_collection_terminal.Location = New System.Drawing.Point(8, 57)
    Me.chk_ticket_collection_terminal.Name = "chk_ticket_collection_terminal"
    Me.chk_ticket_collection_terminal.Size = New System.Drawing.Size(228, 23)
    Me.chk_ticket_collection_terminal.TabIndex = 1
    Me.chk_ticket_collection_terminal.Text = "xAllowTicketDeliver"
    Me.chk_ticket_collection_terminal.UseVisualStyleBackColor = True
    '
    'cmb_detail_collection_level
    '
    Me.cmb_detail_collection_level.AllowUnlistedValues = False
    Me.cmb_detail_collection_level.AutoCompleteMode = False
    Me.cmb_detail_collection_level.IsReadOnly = True
    Me.cmb_detail_collection_level.Location = New System.Drawing.Point(13, 20)
    Me.cmb_detail_collection_level.Name = "cmb_detail_collection_level"
    Me.cmb_detail_collection_level.SelectedIndex = -1
    Me.cmb_detail_collection_level.Size = New System.Drawing.Size(230, 24)
    Me.cmb_detail_collection_level.SufixText = "Sufix Text"
    Me.cmb_detail_collection_level.SufixTextVisible = True
    Me.cmb_detail_collection_level.TabIndex = 0
    Me.cmb_detail_collection_level.TextCombo = Nothing
    Me.cmb_detail_collection_level.TextWidth = 110
    '
    'gb_ticket_personalization
    '
    Me.gb_ticket_personalization.Controls.Add(Me.ef_cashable_ticket_title)
    Me.gb_ticket_personalization.Controls.Add(Me.ef_promo_non_redim_ticket_title)
    Me.gb_ticket_personalization.Controls.Add(Me.ef_location)
    Me.gb_ticket_personalization.Controls.Add(Me.ef_promo_redim_ticket_title)
    Me.gb_ticket_personalization.Controls.Add(Me.ef_debit_ticket_title)
    Me.gb_ticket_personalization.Controls.Add(Me.ef_address_2)
    Me.gb_ticket_personalization.Controls.Add(Me.ef_address_1)
    Me.gb_ticket_personalization.Location = New System.Drawing.Point(7, 421)
    Me.gb_ticket_personalization.Name = "gb_ticket_personalization"
    Me.gb_ticket_personalization.Size = New System.Drawing.Size(682, 239)
    Me.gb_ticket_personalization.TabIndex = 5
    Me.gb_ticket_personalization.TabStop = False
    Me.gb_ticket_personalization.Text = "xTicketPersonalization"
    '
    'ef_cashable_ticket_title
    '
    Me.ef_cashable_ticket_title.DoubleValue = 0.0R
    Me.ef_cashable_ticket_title.IntegerValue = 0
    Me.ef_cashable_ticket_title.IsReadOnly = False
    Me.ef_cashable_ticket_title.Location = New System.Drawing.Point(29, 79)
    Me.ef_cashable_ticket_title.Name = "ef_cashable_ticket_title"
    Me.ef_cashable_ticket_title.PlaceHolder = Nothing
    Me.ef_cashable_ticket_title.Size = New System.Drawing.Size(417, 24)
    Me.ef_cashable_ticket_title.SufixText = "Sufix Text"
    Me.ef_cashable_ticket_title.SufixTextVisible = True
    Me.ef_cashable_ticket_title.TabIndex = 2
    Me.ef_cashable_ticket_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cashable_ticket_title.TextValue = ""
    Me.ef_cashable_ticket_title.TextVisible = False
    Me.ef_cashable_ticket_title.TextWidth = 230
    Me.ef_cashable_ticket_title.Value = ""
    Me.ef_cashable_ticket_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_promo_non_redim_ticket_title
    '
    Me.ef_promo_non_redim_ticket_title.DoubleValue = 0.0R
    Me.ef_promo_non_redim_ticket_title.IntegerValue = 0
    Me.ef_promo_non_redim_ticket_title.IsReadOnly = False
    Me.ef_promo_non_redim_ticket_title.Location = New System.Drawing.Point(29, 50)
    Me.ef_promo_non_redim_ticket_title.Name = "ef_promo_non_redim_ticket_title"
    Me.ef_promo_non_redim_ticket_title.PlaceHolder = Nothing
    Me.ef_promo_non_redim_ticket_title.Size = New System.Drawing.Size(417, 24)
    Me.ef_promo_non_redim_ticket_title.SufixText = "Sufix Text"
    Me.ef_promo_non_redim_ticket_title.SufixTextVisible = True
    Me.ef_promo_non_redim_ticket_title.TabIndex = 1
    Me.ef_promo_non_redim_ticket_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_promo_non_redim_ticket_title.TextValue = ""
    Me.ef_promo_non_redim_ticket_title.TextVisible = False
    Me.ef_promo_non_redim_ticket_title.TextWidth = 230
    Me.ef_promo_non_redim_ticket_title.Value = ""
    Me.ef_promo_non_redim_ticket_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_location
    '
    Me.ef_location.DoubleValue = 0.0R
    Me.ef_location.IntegerValue = 0
    Me.ef_location.IsReadOnly = False
    Me.ef_location.Location = New System.Drawing.Point(29, 139)
    Me.ef_location.Name = "ef_location"
    Me.ef_location.PlaceHolder = Nothing
    Me.ef_location.Size = New System.Drawing.Size(537, 24)
    Me.ef_location.SufixText = "Sufix Text"
    Me.ef_location.SufixTextVisible = True
    Me.ef_location.TabIndex = 4
    Me.ef_location.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_location.TextValue = ""
    Me.ef_location.TextVisible = False
    Me.ef_location.TextWidth = 230
    Me.ef_location.Value = ""
    Me.ef_location.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_promo_redim_ticket_title
    '
    Me.ef_promo_redim_ticket_title.DoubleValue = 0.0R
    Me.ef_promo_redim_ticket_title.IntegerValue = 0
    Me.ef_promo_redim_ticket_title.IsReadOnly = False
    Me.ef_promo_redim_ticket_title.Location = New System.Drawing.Point(29, 20)
    Me.ef_promo_redim_ticket_title.Name = "ef_promo_redim_ticket_title"
    Me.ef_promo_redim_ticket_title.PlaceHolder = Nothing
    Me.ef_promo_redim_ticket_title.Size = New System.Drawing.Size(417, 24)
    Me.ef_promo_redim_ticket_title.SufixText = "Sufix Text"
    Me.ef_promo_redim_ticket_title.SufixTextVisible = True
    Me.ef_promo_redim_ticket_title.TabIndex = 0
    Me.ef_promo_redim_ticket_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_promo_redim_ticket_title.TextValue = ""
    Me.ef_promo_redim_ticket_title.TextVisible = False
    Me.ef_promo_redim_ticket_title.TextWidth = 230
    Me.ef_promo_redim_ticket_title.Value = ""
    Me.ef_promo_redim_ticket_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_debit_ticket_title
    '
    Me.ef_debit_ticket_title.DoubleValue = 0.0R
    Me.ef_debit_ticket_title.IntegerValue = 0
    Me.ef_debit_ticket_title.IsReadOnly = False
    Me.ef_debit_ticket_title.Location = New System.Drawing.Point(29, 109)
    Me.ef_debit_ticket_title.Name = "ef_debit_ticket_title"
    Me.ef_debit_ticket_title.PlaceHolder = Nothing
    Me.ef_debit_ticket_title.Size = New System.Drawing.Size(417, 24)
    Me.ef_debit_ticket_title.SufixText = "Sufix Text"
    Me.ef_debit_ticket_title.SufixTextVisible = True
    Me.ef_debit_ticket_title.TabIndex = 3
    Me.ef_debit_ticket_title.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_debit_ticket_title.TextValue = ""
    Me.ef_debit_ticket_title.TextVisible = False
    Me.ef_debit_ticket_title.TextWidth = 230
    Me.ef_debit_ticket_title.Value = ""
    Me.ef_debit_ticket_title.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_address_2
    '
    Me.ef_address_2.DoubleValue = 0.0R
    Me.ef_address_2.IntegerValue = 0
    Me.ef_address_2.IsReadOnly = False
    Me.ef_address_2.Location = New System.Drawing.Point(29, 200)
    Me.ef_address_2.Name = "ef_address_2"
    Me.ef_address_2.PlaceHolder = Nothing
    Me.ef_address_2.Size = New System.Drawing.Size(537, 24)
    Me.ef_address_2.SufixText = "Sufix Text"
    Me.ef_address_2.SufixTextVisible = True
    Me.ef_address_2.TabIndex = 6
    Me.ef_address_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_address_2.TextValue = ""
    Me.ef_address_2.TextVisible = False
    Me.ef_address_2.TextWidth = 230
    Me.ef_address_2.Value = ""
    Me.ef_address_2.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_address_1
    '
    Me.ef_address_1.DoubleValue = 0.0R
    Me.ef_address_1.IntegerValue = 0
    Me.ef_address_1.IsReadOnly = False
    Me.ef_address_1.Location = New System.Drawing.Point(29, 169)
    Me.ef_address_1.Name = "ef_address_1"
    Me.ef_address_1.PlaceHolder = Nothing
    Me.ef_address_1.Size = New System.Drawing.Size(537, 24)
    Me.ef_address_1.SufixText = "Sufix Text"
    Me.ef_address_1.SufixTextVisible = True
    Me.ef_address_1.TabIndex = 5
    Me.ef_address_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_address_1.TextValue = ""
    Me.ef_address_1.TextVisible = False
    Me.ef_address_1.TextWidth = 230
    Me.ef_address_1.Value = ""
    Me.ef_address_1.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_tickets_limit_before_prize
    '
    Me.ef_tickets_limit_before_prize.DoubleValue = 0.0R
    Me.ef_tickets_limit_before_prize.IntegerValue = 0
    Me.ef_tickets_limit_before_prize.IsReadOnly = False
    Me.ef_tickets_limit_before_prize.Location = New System.Drawing.Point(12, 20)
    Me.ef_tickets_limit_before_prize.Name = "ef_tickets_limit_before_prize"
    Me.ef_tickets_limit_before_prize.PlaceHolder = Nothing
    Me.ef_tickets_limit_before_prize.Size = New System.Drawing.Size(604, 24)
    Me.ef_tickets_limit_before_prize.SufixText = "Sufix Text"
    Me.ef_tickets_limit_before_prize.SufixTextVisible = True
    Me.ef_tickets_limit_before_prize.TabIndex = 0
    Me.ef_tickets_limit_before_prize.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_tickets_limit_before_prize.TextValue = ""
    Me.ef_tickets_limit_before_prize.TextWidth = 450
    Me.ef_tickets_limit_before_prize.Value = ""
    Me.ef_tickets_limit_before_prize.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_appearance
    '
    Me.gb_appearance.Controls.Add(Me.ef_shown_numbers)
    Me.gb_appearance.Controls.Add(Me.chk_hide_validation_number)
    Me.gb_appearance.Location = New System.Drawing.Point(399, 263)
    Me.gb_appearance.Name = "gb_appearance"
    Me.gb_appearance.Size = New System.Drawing.Size(290, 47)
    Me.gb_appearance.TabIndex = 3
    Me.gb_appearance.TabStop = False
    Me.gb_appearance.Text = "xAppearance"
    '
    'ef_shown_numbers
    '
    Me.ef_shown_numbers.DoubleValue = 0.0R
    Me.ef_shown_numbers.IntegerValue = 0
    Me.ef_shown_numbers.IsReadOnly = False
    Me.ef_shown_numbers.Location = New System.Drawing.Point(57, 17)
    Me.ef_shown_numbers.Name = "ef_shown_numbers"
    Me.ef_shown_numbers.PlaceHolder = Nothing
    Me.ef_shown_numbers.Size = New System.Drawing.Size(226, 24)
    Me.ef_shown_numbers.SufixText = "digitos"
    Me.ef_shown_numbers.SufixTextVisible = True
    Me.ef_shown_numbers.SufixTextWidth = 55
    Me.ef_shown_numbers.TabIndex = 1
    Me.ef_shown_numbers.Tag = ""
    Me.ef_shown_numbers.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_shown_numbers.TextValue = ""
    Me.ef_shown_numbers.TextWidth = 120
    Me.ef_shown_numbers.Value = ""
    Me.ef_shown_numbers.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_hide_validation_number
    '
    Me.chk_hide_validation_number.AutoSize = True
    Me.chk_hide_validation_number.Location = New System.Drawing.Point(10, 24)
    Me.chk_hide_validation_number.Name = "chk_hide_validation_number"
    Me.chk_hide_validation_number.Size = New System.Drawing.Size(15, 14)
    Me.chk_hide_validation_number.TabIndex = 0
    Me.chk_hide_validation_number.UseVisualStyleBackColor = True
    '
    'gb_expiration_dates
    '
    Me.gb_expiration_dates.Controls.Add(Me.ef_ticket_promotional_redeemable_expiration_days)
    Me.gb_expiration_dates.Controls.Add(Me.ef_cashable_ticket_expiration_days)
    Me.gb_expiration_dates.Controls.Add(Me.ef_ticket_promotional_nonredeemable_expiration_days)
    Me.gb_expiration_dates.Location = New System.Drawing.Point(399, 135)
    Me.gb_expiration_dates.Name = "gb_expiration_dates"
    Me.gb_expiration_dates.Size = New System.Drawing.Size(290, 126)
    Me.gb_expiration_dates.TabIndex = 2
    Me.gb_expiration_dates.TabStop = False
    Me.gb_expiration_dates.Text = "xExpirationDate"
    '
    'ef_ticket_promotional_redeemable_expiration_days
    '
    Me.ef_ticket_promotional_redeemable_expiration_days.DoubleValue = 0.0R
    Me.ef_ticket_promotional_redeemable_expiration_days.IntegerValue = 0
    Me.ef_ticket_promotional_redeemable_expiration_days.IsReadOnly = False
    Me.ef_ticket_promotional_redeemable_expiration_days.Location = New System.Drawing.Point(8, 50)
    Me.ef_ticket_promotional_redeemable_expiration_days.Name = "ef_ticket_promotional_redeemable_expiration_days"
    Me.ef_ticket_promotional_redeemable_expiration_days.PlaceHolder = Nothing
    Me.ef_ticket_promotional_redeemable_expiration_days.Size = New System.Drawing.Size(261, 24)
    Me.ef_ticket_promotional_redeemable_expiration_days.SufixText = "Sufix Text"
    Me.ef_ticket_promotional_redeemable_expiration_days.SufixTextVisible = True
    Me.ef_ticket_promotional_redeemable_expiration_days.TabIndex = 1
    Me.ef_ticket_promotional_redeemable_expiration_days.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_ticket_promotional_redeemable_expiration_days.TextValue = ""
    Me.ef_ticket_promotional_redeemable_expiration_days.TextWidth = 200
    Me.ef_ticket_promotional_redeemable_expiration_days.Value = ""
    Me.ef_ticket_promotional_redeemable_expiration_days.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_cashable_ticket_expiration_days
    '
    Me.ef_cashable_ticket_expiration_days.DoubleValue = 0.0R
    Me.ef_cashable_ticket_expiration_days.IntegerValue = 0
    Me.ef_cashable_ticket_expiration_days.IsReadOnly = False
    Me.ef_cashable_ticket_expiration_days.Location = New System.Drawing.Point(8, 20)
    Me.ef_cashable_ticket_expiration_days.Name = "ef_cashable_ticket_expiration_days"
    Me.ef_cashable_ticket_expiration_days.PlaceHolder = Nothing
    Me.ef_cashable_ticket_expiration_days.Size = New System.Drawing.Size(261, 24)
    Me.ef_cashable_ticket_expiration_days.SufixText = "Sufix Text"
    Me.ef_cashable_ticket_expiration_days.SufixTextVisible = True
    Me.ef_cashable_ticket_expiration_days.TabIndex = 0
    Me.ef_cashable_ticket_expiration_days.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cashable_ticket_expiration_days.TextValue = ""
    Me.ef_cashable_ticket_expiration_days.TextWidth = 200
    Me.ef_cashable_ticket_expiration_days.Value = ""
    Me.ef_cashable_ticket_expiration_days.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_ticket_promotional_nonredeemable_expiration_days
    '
    Me.ef_ticket_promotional_nonredeemable_expiration_days.DoubleValue = 0.0R
    Me.ef_ticket_promotional_nonredeemable_expiration_days.IntegerValue = 0
    Me.ef_ticket_promotional_nonredeemable_expiration_days.IsReadOnly = False
    Me.ef_ticket_promotional_nonredeemable_expiration_days.Location = New System.Drawing.Point(8, 81)
    Me.ef_ticket_promotional_nonredeemable_expiration_days.Name = "ef_ticket_promotional_nonredeemable_expiration_days"
    Me.ef_ticket_promotional_nonredeemable_expiration_days.PlaceHolder = Nothing
    Me.ef_ticket_promotional_nonredeemable_expiration_days.Size = New System.Drawing.Size(261, 24)
    Me.ef_ticket_promotional_nonredeemable_expiration_days.SufixText = "Sufix Text"
    Me.ef_ticket_promotional_nonredeemable_expiration_days.SufixTextVisible = True
    Me.ef_ticket_promotional_nonredeemable_expiration_days.TabIndex = 2
    Me.ef_ticket_promotional_nonredeemable_expiration_days.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_ticket_promotional_nonredeemable_expiration_days.TextValue = ""
    Me.ef_ticket_promotional_nonredeemable_expiration_days.TextWidth = 200
    Me.ef_ticket_promotional_nonredeemable_expiration_days.Value = ""
    Me.ef_ticket_promotional_nonredeemable_expiration_days.ValueForeColor = System.Drawing.Color.Blue
    '
    'gb_prizes
    '
    Me.gb_prizes.Controls.Add(Me.lbl_also_applies_to_chips)
    Me.gb_prizes.Controls.Add(Me.ef_jackpots_limit_before_prize)
    Me.gb_prizes.Controls.Add(Me.ef_tickets_limit_before_prize)
    Me.gb_prizes.Location = New System.Drawing.Point(7, 312)
    Me.gb_prizes.Name = "gb_prizes"
    Me.gb_prizes.Size = New System.Drawing.Size(682, 97)
    Me.gb_prizes.TabIndex = 4
    Me.gb_prizes.TabStop = False
    Me.gb_prizes.Text = "xPrizes"
    '
    'lbl_also_applies_to_chips
    '
    Me.lbl_also_applies_to_chips.AutoSize = True
    Me.lbl_also_applies_to_chips.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_also_applies_to_chips.ForeColor = System.Drawing.Color.Navy
    Me.lbl_also_applies_to_chips.Location = New System.Drawing.Point(10, 75)
    Me.lbl_also_applies_to_chips.Name = "lbl_also_applies_to_chips"
    Me.lbl_also_applies_to_chips.RightToLeft = System.Windows.Forms.RightToLeft.No
    Me.lbl_also_applies_to_chips.Size = New System.Drawing.Size(77, 14)
    Me.lbl_also_applies_to_chips.TabIndex = 2
    Me.lbl_also_applies_to_chips.Text = "xInfoChips"
    Me.lbl_also_applies_to_chips.Visible = False
    '
    'ef_jackpots_limit_before_prize
    '
    Me.ef_jackpots_limit_before_prize.DoubleValue = 0.0R
    Me.ef_jackpots_limit_before_prize.IntegerValue = 0
    Me.ef_jackpots_limit_before_prize.IsReadOnly = False
    Me.ef_jackpots_limit_before_prize.Location = New System.Drawing.Point(12, 48)
    Me.ef_jackpots_limit_before_prize.Name = "ef_jackpots_limit_before_prize"
    Me.ef_jackpots_limit_before_prize.PlaceHolder = Nothing
    Me.ef_jackpots_limit_before_prize.Size = New System.Drawing.Size(604, 24)
    Me.ef_jackpots_limit_before_prize.SufixText = "Sufix Text"
    Me.ef_jackpots_limit_before_prize.SufixTextVisible = True
    Me.ef_jackpots_limit_before_prize.TabIndex = 1
    Me.ef_jackpots_limit_before_prize.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_jackpots_limit_before_prize.TextValue = ""
    Me.ef_jackpots_limit_before_prize.TextWidth = 450
    Me.ef_jackpots_limit_before_prize.Value = ""
    Me.ef_jackpots_limit_before_prize.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_tito_params_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(806, 679)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Name = "frm_tito_params_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_tito_params_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gb_tickets.ResumeLayout(False)
    Me.gb_tickets.PerformLayout()
    Me.gb_income.ResumeLayout(False)
    Me.gb_ticket_personalization.ResumeLayout(False)
    Me.gb_appearance.ResumeLayout(False)
    Me.gb_appearance.PerformLayout()
    Me.gb_expiration_dates.ResumeLayout(False)
    Me.gb_prizes.ResumeLayout(False)
    Me.gb_prizes.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_tickets As System.Windows.Forms.GroupBox
  Friend WithEvents ef_max_allowed_ATM_reedem As GUI_Controls.uc_entry_field
  Friend WithEvents gb_income As System.Windows.Forms.GroupBox
  Friend WithEvents cmb_detail_collection_level As GUI_Controls.uc_combo
  Friend WithEvents gb_ticket_personalization As System.Windows.Forms.GroupBox
  Friend WithEvents ef_promo_redim_ticket_title As GUI_Controls.uc_entry_field
  Friend WithEvents ef_location As GUI_Controls.uc_entry_field
  Friend WithEvents ef_address_1 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_address_2 As GUI_Controls.uc_entry_field
  Friend WithEvents ef_debit_ticket_title As GUI_Controls.uc_entry_field
  Friend WithEvents chk_ticket_collection_terminal As System.Windows.Forms.CheckBox
  Friend WithEvents chk_promotional_redemption As System.Windows.Forms.CheckBox
  Friend WithEvents chk_cashable_redemption As System.Windows.Forms.CheckBox
  Friend WithEvents chk_allow_emission As System.Windows.Forms.CheckBox
  Friend WithEvents ef_max_allowed_creation As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_max_creation_info As System.Windows.Forms.Label
  Friend WithEvents gb_appearance As System.Windows.Forms.GroupBox
  Friend WithEvents chk_hide_validation_number As System.Windows.Forms.CheckBox
  Friend WithEvents ef_shown_numbers As GUI_Controls.uc_entry_field
  Friend WithEvents ef_cashable_ticket_title As GUI_Controls.uc_entry_field
  Friend WithEvents ef_promo_non_redim_ticket_title As GUI_Controls.uc_entry_field
  Friend WithEvents chk_ticket_collection_cashier As System.Windows.Forms.CheckBox
  Friend WithEvents gb_expiration_dates As System.Windows.Forms.GroupBox
  Friend WithEvents ef_tickets_limit_before_prize As GUI_Controls.uc_entry_field
  Friend WithEvents ef_ticket_promotional_redeemable_expiration_days As GUI_Controls.uc_entry_field
  Friend WithEvents ef_cashable_ticket_expiration_days As GUI_Controls.uc_entry_field
  Friend WithEvents ef_ticket_promotional_nonredeemable_expiration_days As GUI_Controls.uc_entry_field
  Friend WithEvents ef_stacker_capacity As GUI_Controls.uc_entry_field
  Friend WithEvents gb_prizes As System.Windows.Forms.GroupBox
  Friend WithEvents ef_jackpots_limit_before_prize As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_also_applies_to_chips As System.Windows.Forms.Label
  Friend WithEvents chk_allow_truncate As System.Windows.Forms.CheckBox
  Friend WithEvents ef_min_allowed_ticket_in As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_allow_truncate_info As System.Windows.Forms.Label
  Friend WithEvents ef_min_denomination_multiple As GUI_Controls.uc_entry_field
End Class
