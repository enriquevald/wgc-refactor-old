'-------------------------------------------------------------------
' Copyright © 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_terminals_configuration_edit
'
' DESCRIPTION : Edit Terminal Configuration
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 09-OCT-2013  DRV    Initial version
' 11-OCT-2013  DRV    Deleted de Validation Type entry field
' 14-OCT-2013  DRV    Added Allowed Bills checkBox
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Data
Public Class frm_tito_terminals_configuration_edit
  Inherits GUI_Controls.frm_base_edit

#Region " Constants "
#End Region ' Constants

#Region " Members "

  Private m_input_terminal_id As Integer
  Private m_input_terminal_name As String

#End Region ' Members

#Region " Overrides "

  ' PURPOSE : Initializes the form id.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = 0

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE : Form controls initialization.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS  :

  Protected Overrides Sub GUI_InitControls()
    ' Required by the base class
    Call MyBase.GUI_InitControls()


    ' Set form Name
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2720)                        ' 2720 "Terminal SAS Editor"

    Me.uc_terminal_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1097)       ' 1097 "Terminal"

    Me.chk_allow_creation.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2717)     ' 2717 "Allow ticket creation"
    Me.chk_allow_cashable.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2718)     ' 2718 "Allow cashable redemption"
    Me.chk_allow_promotional.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2719)  ' 2719 "Allow promotional redemption"
    Me.chk_allow_bills.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2723)        ' 2723 "Allow Bills

    ' Delete button is only available when editing, not while creating
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False

    If Not Permissions.Write Then
      chk_allow_cashable.Enabled = False
      chk_allow_creation.Enabled = False
      chk_allow_promotional.Enabled = False
      chk_allow_bills.Enabled = False
    End If


  End Sub ' GUI_InitControls

  ' PURPOSE : Validate the data presented on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE : Set initial data on the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _terminal_configuration As cls_tito_terminal_configuration
    _terminal_configuration = DbReadObject

    uc_terminal_name.Value = _terminal_configuration.TerminalName

    chk_allow_cashable.Checked = _terminal_configuration.AllowCashable
    chk_allow_creation.Checked = _terminal_configuration.AllowTicketCreation
    chk_allow_promotional.Checked = _terminal_configuration.AllowPromotionTickets
    chk_allow_bills.Checked = _terminal_configuration.AllowBills

  End Sub ' GUI_SetScreenData

  ' PURPOSE : Get data from the screen.
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_GetScreenData()

    Dim _terminal_configuration As cls_tito_terminal_configuration

    _terminal_configuration = DbEditedObject

    _terminal_configuration.AllowCashable = chk_allow_cashable.Checked
    _terminal_configuration.AllowPromotionTickets = chk_allow_promotional.Checked
    _terminal_configuration.AllowTicketCreation = chk_allow_creation.Checked
    _terminal_configuration.AllowBills = chk_allow_bills.Checked

  End Sub ' GUI_GetScreenData

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim _terminal_configuration As cls_tito_terminal_configuration
    Dim _aux_nls As String
    'Dim _rc As mdl_NLS.ENUM_MB_RESULT

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New cls_tito_terminal_configuration
        _terminal_configuration = DbEditedObject
        _terminal_configuration.TerminalId = 0
        DbStatus = ENUM_STATUS.STATUS_OK

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 2721 "Se ha producido un error al leer la configuración del terminal %1."
          _terminal_configuration = Me.DbEditedObject
          _aux_nls = m_input_terminal_name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2721), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE

        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          ' 2722 "Se ha producido un error al modificar la configuración del terminal %1."
          _terminal_configuration = Me.DbEditedObject
          _aux_nls = _terminal_configuration.TerminalName
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2722), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _aux_nls)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE


      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE


      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE : Manage permissions.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)
  End Sub ' GUI_Permissions

  ' PURPOSE : Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :

  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.chk_allow_creation

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE : Init form in new mode
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  '
  Public Overloads Sub ShowNewItem()

  End Sub ' ShowNewItem

  ' PURPOSE : Init form in edit mode
  '
  '  PARAMS :
  '     - INPUT :
  '       - UserId
  '       - Username
  '
  '     - OUTPUT :
  '
  ' RETURNS :

  Public Overloads Sub ShowEditItem(ByVal TerminalId As Integer, ByVal TerminalName As String)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT

    Me.DbObjectId = TerminalId
    Me.m_input_terminal_name = TerminalName

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)
    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If
  End Sub ' ShowEditItem

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Return False
  End Function

#End Region ' Overrides

#Region " Private "
#End Region ' Private

#Region " Events "
#End Region ' Events

End Class
