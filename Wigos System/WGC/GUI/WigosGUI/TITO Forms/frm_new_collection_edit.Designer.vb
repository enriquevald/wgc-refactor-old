<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_new_collection_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.ef_employee = New GUI_Controls.uc_entry_field
    Me.ef_terminal = New GUI_Controls.uc_entry_field
    Me.gb_collection_params = New System.Windows.Forms.GroupBox
    Me.lbl_notes = New System.Windows.Forms.Label
    Me.tb_notes = New System.Windows.Forms.TextBox
    Me.lb_stacker_id = New System.Windows.Forms.Label
    Me.cmb_stacker_id = New System.Windows.Forms.ComboBox
    Me.ef_collection_date = New GUI_Controls.uc_entry_field
    Me.ef_extraction_date = New GUI_Controls.uc_entry_field
    Me.ef_insertion_date = New GUI_Controls.uc_entry_field
    Me.btn_start_collection = New GUI_Controls.uc_button
    Me.uc_pr_list = New GUI_Controls.uc_provider
    Me.gb_theoretical_values = New System.Windows.Forms.GroupBox
    Me.ef_theoretical_collected_in_notes = New GUI_Controls.uc_entry_field
    Me.ef_theoretical_collected_in_tickets = New GUI_Controls.uc_entry_field
    Me.ef_theoretical_total_collected = New GUI_Controls.uc_entry_field
    Me.gb_real_values = New System.Windows.Forms.GroupBox
    Me.ef_real_recolected_in_tickets = New GUI_Controls.uc_entry_field
    Me.ef_real_recolected_in_notes = New GUI_Controls.uc_entry_field
    Me.ef_real_total_recollected = New GUI_Controls.uc_entry_field
    Me.gb_status = New System.Windows.Forms.GroupBox
    Me.lbl_error = New System.Windows.Forms.Label
    Me.tf_error = New GUI_Controls.uc_text_field
    Me.lbl_ok = New System.Windows.Forms.Label
    Me.tf_ok = New GUI_Controls.uc_text_field
    Me.tb_collections = New System.Windows.Forms.TabControl
    Me.tab_billetes = New System.Windows.Forms.TabPage
    Me.dg_collection_bills = New GUI_Controls.uc_grid
    Me.tab_ticket = New System.Windows.Forms.TabPage
    Me.gb_status1 = New System.Windows.Forms.GroupBox
    Me.lbl_error1 = New System.Windows.Forms.Label
    Me.tf_error1 = New GUI_Controls.uc_text_field
    Me.lbl_ok1 = New System.Windows.Forms.Label
    Me.tf_ok1 = New GUI_Controls.uc_text_field
    Me.dg_collection_tickets = New GUI_Controls.uc_grid
    Me.ef_ticket = New GUI_Controls.uc_entry_field
    Me.btn_save_tickets = New GUI_Controls.uc_button
    Me.Label1 = New System.Windows.Forms.Label
    Me.Uc_text_field1 = New GUI_Controls.uc_text_field
    Me.Label2 = New System.Windows.Forms.Label
    Me.Uc_text_field2 = New GUI_Controls.uc_text_field
    Me.Label3 = New System.Windows.Forms.Label
    Me.Uc_text_field3 = New GUI_Controls.uc_text_field
    Me.panel_data.SuspendLayout()
    Me.gb_collection_params.SuspendLayout()
    Me.gb_theoretical_values.SuspendLayout()
    Me.gb_real_values.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.tb_collections.SuspendLayout()
    Me.tab_billetes.SuspendLayout()
    Me.tab_ticket.SuspendLayout()
    Me.gb_status1.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.tb_collections)
    Me.panel_data.Controls.Add(Me.gb_theoretical_values)
    Me.panel_data.Controls.Add(Me.gb_real_values)
    Me.panel_data.Controls.Add(Me.gb_collection_params)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(668, 671)
    '
    'ef_employee
    '
    Me.ef_employee.DoubleValue = 0
    Me.ef_employee.IntegerValue = 0
    Me.ef_employee.IsReadOnly = False
    Me.ef_employee.Location = New System.Drawing.Point(270, 84)
    Me.ef_employee.Name = "ef_employee"
    Me.ef_employee.Size = New System.Drawing.Size(270, 24)
    Me.ef_employee.SufixText = "Sufix Text"
    Me.ef_employee.SufixTextVisible = True
    Me.ef_employee.TabIndex = 5
    Me.ef_employee.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_employee.TextValue = ""
    Me.ef_employee.TextWidth = 100
    Me.ef_employee.Value = ""
    '
    'ef_terminal
    '
    Me.ef_terminal.DoubleValue = 0
    Me.ef_terminal.IntegerValue = 0
    Me.ef_terminal.IsReadOnly = False
    Me.ef_terminal.Location = New System.Drawing.Point(270, 54)
    Me.ef_terminal.Name = "ef_terminal"
    Me.ef_terminal.Size = New System.Drawing.Size(270, 24)
    Me.ef_terminal.SufixText = "Sufix Text"
    Me.ef_terminal.SufixTextVisible = True
    Me.ef_terminal.TabIndex = 6
    Me.ef_terminal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_terminal.TextValue = ""
    Me.ef_terminal.TextWidth = 100
    Me.ef_terminal.Value = ""
    '
    'gb_collection_params
    '
    Me.gb_collection_params.Controls.Add(Me.lbl_notes)
    Me.gb_collection_params.Controls.Add(Me.tb_notes)
    Me.gb_collection_params.Controls.Add(Me.lb_stacker_id)
    Me.gb_collection_params.Controls.Add(Me.cmb_stacker_id)
    Me.gb_collection_params.Controls.Add(Me.ef_collection_date)
    Me.gb_collection_params.Controls.Add(Me.ef_extraction_date)
    Me.gb_collection_params.Controls.Add(Me.ef_insertion_date)
    Me.gb_collection_params.Controls.Add(Me.btn_start_collection)
    Me.gb_collection_params.Controls.Add(Me.ef_employee)
    Me.gb_collection_params.Controls.Add(Me.ef_terminal)
    Me.gb_collection_params.Controls.Add(Me.uc_pr_list)
    Me.gb_collection_params.Location = New System.Drawing.Point(7, 9)
    Me.gb_collection_params.Name = "gb_collection_params"
    Me.gb_collection_params.Size = New System.Drawing.Size(553, 252)
    Me.gb_collection_params.TabIndex = 0
    Me.gb_collection_params.TabStop = False
    Me.gb_collection_params.Text = "xParametros"
    '
    'lbl_notes
    '
    Me.lbl_notes.AutoSize = True
    Me.lbl_notes.Location = New System.Drawing.Point(328, 177)
    Me.lbl_notes.Name = "lbl_notes"
    Me.lbl_notes.Size = New System.Drawing.Size(46, 13)
    Me.lbl_notes.TabIndex = 13
    Me.lbl_notes.Text = "xNotes"
    '
    'tb_notes
    '
    Me.tb_notes.Location = New System.Drawing.Point(373, 174)
    Me.tb_notes.MaxLength = 199
    Me.tb_notes.Multiline = True
    Me.tb_notes.Name = "tb_notes"
    Me.tb_notes.Size = New System.Drawing.Size(167, 69)
    Me.tb_notes.TabIndex = 12
    '
    'lb_stacker_id
    '
    Me.lb_stacker_id.AutoSize = True
    Me.lb_stacker_id.Location = New System.Drawing.Point(35, 23)
    Me.lb_stacker_id.Name = "lb_stacker_id"
    Me.lb_stacker_id.Size = New System.Drawing.Size(70, 13)
    Me.lb_stacker_id.TabIndex = 11
    Me.lb_stacker_id.Text = "xStackerId"
    '
    'cmb_stacker_id
    '
    Me.cmb_stacker_id.FormattingEnabled = True
    Me.cmb_stacker_id.Location = New System.Drawing.Point(112, 20)
    Me.cmb_stacker_id.Name = "cmb_stacker_id"
    Me.cmb_stacker_id.Size = New System.Drawing.Size(149, 21)
    Me.cmb_stacker_id.TabIndex = 1
    '
    'ef_collection_date
    '
    Me.ef_collection_date.DoubleValue = 0
    Me.ef_collection_date.IntegerValue = 0
    Me.ef_collection_date.IsReadOnly = False
    Me.ef_collection_date.Location = New System.Drawing.Point(270, 171)
    Me.ef_collection_date.Name = "ef_collection_date"
    Me.ef_collection_date.Size = New System.Drawing.Size(270, 24)
    Me.ef_collection_date.SufixText = "Sufix Text"
    Me.ef_collection_date.SufixTextVisible = True
    Me.ef_collection_date.TabIndex = 1
    Me.ef_collection_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_collection_date.TextValue = ""
    Me.ef_collection_date.TextWidth = 100
    Me.ef_collection_date.Value = ""
    '
    'ef_extraction_date
    '
    Me.ef_extraction_date.DoubleValue = 0
    Me.ef_extraction_date.IntegerValue = 0
    Me.ef_extraction_date.IsReadOnly = False
    Me.ef_extraction_date.Location = New System.Drawing.Point(270, 114)
    Me.ef_extraction_date.Name = "ef_extraction_date"
    Me.ef_extraction_date.Size = New System.Drawing.Size(270, 24)
    Me.ef_extraction_date.SufixText = "Sufix Text"
    Me.ef_extraction_date.SufixTextVisible = True
    Me.ef_extraction_date.TabIndex = 4
    Me.ef_extraction_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_extraction_date.TextValue = ""
    Me.ef_extraction_date.TextWidth = 100
    Me.ef_extraction_date.Value = ""
    '
    'ef_insertion_date
    '
    Me.ef_insertion_date.DoubleValue = 0
    Me.ef_insertion_date.IntegerValue = 0
    Me.ef_insertion_date.IsReadOnly = False
    Me.ef_insertion_date.Location = New System.Drawing.Point(270, 141)
    Me.ef_insertion_date.Name = "ef_insertion_date"
    Me.ef_insertion_date.Size = New System.Drawing.Size(270, 24)
    Me.ef_insertion_date.SufixText = "Sufix Text"
    Me.ef_insertion_date.SufixTextVisible = True
    Me.ef_insertion_date.TabIndex = 3
    Me.ef_insertion_date.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_insertion_date.TextValue = ""
    Me.ef_insertion_date.TextWidth = 100
    Me.ef_insertion_date.Value = ""
    '
    'btn_start_collection
    '
    Me.btn_start_collection.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_start_collection.Location = New System.Drawing.Point(446, 15)
    Me.btn_start_collection.Name = "btn_start_collection"
    Me.btn_start_collection.Size = New System.Drawing.Size(90, 30)
    Me.btn_start_collection.TabIndex = 2
    Me.btn_start_collection.ToolTipped = False
    Me.btn_start_collection.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    Me.btn_start_collection.Visible = False
    '
    'uc_pr_list
    '
    Me.uc_pr_list.Location = New System.Drawing.Point(12, 42)
    Me.uc_pr_list.Name = "uc_pr_list"
    Me.uc_pr_list.Size = New System.Drawing.Size(304, 188)
    Me.uc_pr_list.TabIndex = 8
    '
    'gb_theoretical_values
    '
    Me.gb_theoretical_values.Controls.Add(Me.ef_theoretical_collected_in_notes)
    Me.gb_theoretical_values.Controls.Add(Me.ef_theoretical_collected_in_tickets)
    Me.gb_theoretical_values.Controls.Add(Me.ef_theoretical_total_collected)
    Me.gb_theoretical_values.Location = New System.Drawing.Point(335, 266)
    Me.gb_theoretical_values.Name = "gb_theoretical_values"
    Me.gb_theoretical_values.Size = New System.Drawing.Size(323, 80)
    Me.gb_theoretical_values.TabIndex = 1
    Me.gb_theoretical_values.TabStop = False
    Me.gb_theoretical_values.Text = "xValores Teoricos"
    '
    'ef_theoretical_collected_in_notes
    '
    Me.ef_theoretical_collected_in_notes.DoubleValue = 0
    Me.ef_theoretical_collected_in_notes.IntegerValue = 0
    Me.ef_theoretical_collected_in_notes.IsReadOnly = False
    Me.ef_theoretical_collected_in_notes.Location = New System.Drawing.Point(6, 16)
    Me.ef_theoretical_collected_in_notes.Name = "ef_theoretical_collected_in_notes"
    Me.ef_theoretical_collected_in_notes.Size = New System.Drawing.Size(144, 24)
    Me.ef_theoretical_collected_in_notes.SufixText = "Sufix Text"
    Me.ef_theoretical_collected_in_notes.SufixTextVisible = True
    Me.ef_theoretical_collected_in_notes.TabIndex = 1
    Me.ef_theoretical_collected_in_notes.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_theoretical_collected_in_notes.TextValue = ""
    Me.ef_theoretical_collected_in_notes.TextWidth = 45
    Me.ef_theoretical_collected_in_notes.Value = ""
    '
    'ef_theoretical_collected_in_tickets
    '
    Me.ef_theoretical_collected_in_tickets.DoubleValue = 0
    Me.ef_theoretical_collected_in_tickets.IntegerValue = 0
    Me.ef_theoretical_collected_in_tickets.IsReadOnly = False
    Me.ef_theoretical_collected_in_tickets.Location = New System.Drawing.Point(173, 17)
    Me.ef_theoretical_collected_in_tickets.Name = "ef_theoretical_collected_in_tickets"
    Me.ef_theoretical_collected_in_tickets.Size = New System.Drawing.Size(144, 24)
    Me.ef_theoretical_collected_in_tickets.SufixText = "Sufix Text"
    Me.ef_theoretical_collected_in_tickets.SufixTextVisible = True
    Me.ef_theoretical_collected_in_tickets.TabIndex = 2
    Me.ef_theoretical_collected_in_tickets.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_theoretical_collected_in_tickets.TextValue = ""
    Me.ef_theoretical_collected_in_tickets.TextWidth = 45
    Me.ef_theoretical_collected_in_tickets.Value = ""
    '
    'ef_theoretical_total_collected
    '
    Me.ef_theoretical_total_collected.DoubleValue = 0
    Me.ef_theoretical_total_collected.IntegerValue = 0
    Me.ef_theoretical_total_collected.IsReadOnly = False
    Me.ef_theoretical_total_collected.Location = New System.Drawing.Point(6, 47)
    Me.ef_theoretical_total_collected.Name = "ef_theoretical_total_collected"
    Me.ef_theoretical_total_collected.Size = New System.Drawing.Size(144, 24)
    Me.ef_theoretical_total_collected.SufixText = "Sufix Text"
    Me.ef_theoretical_total_collected.SufixTextVisible = True
    Me.ef_theoretical_total_collected.TabIndex = 0
    Me.ef_theoretical_total_collected.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_theoretical_total_collected.TextValue = ""
    Me.ef_theoretical_total_collected.TextWidth = 45
    Me.ef_theoretical_total_collected.Value = ""
    '
    'gb_real_values
    '
    Me.gb_real_values.Controls.Add(Me.ef_real_recolected_in_tickets)
    Me.gb_real_values.Controls.Add(Me.ef_real_recolected_in_notes)
    Me.gb_real_values.Controls.Add(Me.ef_real_total_recollected)
    Me.gb_real_values.Location = New System.Drawing.Point(7, 266)
    Me.gb_real_values.Name = "gb_real_values"
    Me.gb_real_values.Size = New System.Drawing.Size(323, 80)
    Me.gb_real_values.TabIndex = 0
    Me.gb_real_values.TabStop = False
    Me.gb_real_values.Text = "xValores Reales"
    '
    'ef_real_recolected_in_tickets
    '
    Me.ef_real_recolected_in_tickets.DoubleValue = 0
    Me.ef_real_recolected_in_tickets.IntegerValue = 0
    Me.ef_real_recolected_in_tickets.IsReadOnly = False
    Me.ef_real_recolected_in_tickets.Location = New System.Drawing.Point(173, 17)
    Me.ef_real_recolected_in_tickets.Name = "ef_real_recolected_in_tickets"
    Me.ef_real_recolected_in_tickets.Size = New System.Drawing.Size(144, 24)
    Me.ef_real_recolected_in_tickets.SufixText = "Sufix Text"
    Me.ef_real_recolected_in_tickets.SufixTextVisible = True
    Me.ef_real_recolected_in_tickets.TabIndex = 2
    Me.ef_real_recolected_in_tickets.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_real_recolected_in_tickets.TextValue = ""
    Me.ef_real_recolected_in_tickets.TextWidth = 45
    Me.ef_real_recolected_in_tickets.Value = ""
    '
    'ef_real_recolected_in_notes
    '
    Me.ef_real_recolected_in_notes.DoubleValue = 0
    Me.ef_real_recolected_in_notes.IntegerValue = 0
    Me.ef_real_recolected_in_notes.IsReadOnly = False
    Me.ef_real_recolected_in_notes.Location = New System.Drawing.Point(6, 17)
    Me.ef_real_recolected_in_notes.Name = "ef_real_recolected_in_notes"
    Me.ef_real_recolected_in_notes.Size = New System.Drawing.Size(144, 24)
    Me.ef_real_recolected_in_notes.SufixText = "Sufix Text"
    Me.ef_real_recolected_in_notes.SufixTextVisible = True
    Me.ef_real_recolected_in_notes.TabIndex = 1
    Me.ef_real_recolected_in_notes.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_real_recolected_in_notes.TextValue = ""
    Me.ef_real_recolected_in_notes.TextWidth = 45
    Me.ef_real_recolected_in_notes.Value = ""
    '
    'ef_real_total_recollected
    '
    Me.ef_real_total_recollected.DoubleValue = 0
    Me.ef_real_total_recollected.IntegerValue = 0
    Me.ef_real_total_recollected.IsReadOnly = False
    Me.ef_real_total_recollected.Location = New System.Drawing.Point(6, 47)
    Me.ef_real_total_recollected.Name = "ef_real_total_recollected"
    Me.ef_real_total_recollected.Size = New System.Drawing.Size(144, 24)
    Me.ef_real_total_recollected.SufixText = "Sufix Text"
    Me.ef_real_total_recollected.SufixTextVisible = True
    Me.ef_real_total_recollected.TabIndex = 0
    Me.ef_real_total_recollected.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_real_total_recollected.TextValue = ""
    Me.ef_real_total_recollected.TextWidth = 45
    Me.ef_real_total_recollected.Value = ""
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.lbl_error)
    Me.gb_status.Controls.Add(Me.tf_error)
    Me.gb_status.Controls.Add(Me.lbl_ok)
    Me.gb_status.Controls.Add(Me.tf_ok)
    Me.gb_status.Location = New System.Drawing.Point(508, 6)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(123, 70)
    Me.gb_status.TabIndex = 110
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'lbl_error
    '
    Me.lbl_error.BackColor = System.Drawing.SystemColors.GrayText
    Me.lbl_error.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_error.Location = New System.Drawing.Point(10, 44)
    Me.lbl_error.Name = "lbl_error"
    Me.lbl_error.Size = New System.Drawing.Size(16, 16)
    Me.lbl_error.TabIndex = 110
    '
    'tf_error
    '
    Me.tf_error.IsReadOnly = True
    Me.tf_error.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_error.LabelForeColor = System.Drawing.Color.Black
    Me.tf_error.Location = New System.Drawing.Point(4, 40)
    Me.tf_error.Name = "tf_error"
    Me.tf_error.Size = New System.Drawing.Size(116, 24)
    Me.tf_error.SufixText = "Sufix Text"
    Me.tf_error.SufixTextVisible = True
    Me.tf_error.TabIndex = 109
    Me.tf_error.TabStop = False
    Me.tf_error.TextVisible = False
    Me.tf_error.TextWidth = 30
    Me.tf_error.Value = "xInactive"
    '
    'lbl_ok
    '
    Me.lbl_ok.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_ok.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_ok.Location = New System.Drawing.Point(10, 19)
    Me.lbl_ok.Name = "lbl_ok"
    Me.lbl_ok.Size = New System.Drawing.Size(16, 16)
    Me.lbl_ok.TabIndex = 106
    '
    'tf_ok
    '
    Me.tf_ok.IsReadOnly = True
    Me.tf_ok.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_ok.LabelForeColor = System.Drawing.Color.Black
    Me.tf_ok.Location = New System.Drawing.Point(4, 15)
    Me.tf_ok.Name = "tf_ok"
    Me.tf_ok.Size = New System.Drawing.Size(114, 24)
    Me.tf_ok.SufixText = "Sufix Text"
    Me.tf_ok.SufixTextVisible = True
    Me.tf_ok.TabIndex = 104
    Me.tf_ok.TabStop = False
    Me.tf_ok.TextVisible = False
    Me.tf_ok.TextWidth = 30
    Me.tf_ok.Value = "xRunning"
    '
    'tb_collections
    '
    Me.tb_collections.Controls.Add(Me.tab_billetes)
    Me.tb_collections.Controls.Add(Me.tab_ticket)
    Me.tb_collections.Location = New System.Drawing.Point(7, 352)
    Me.tb_collections.Name = "tb_collections"
    Me.tb_collections.SelectedIndex = 0
    Me.tb_collections.Size = New System.Drawing.Size(645, 312)
    Me.tb_collections.TabIndex = 1
    '
    'tab_billetes
    '
    Me.tab_billetes.Controls.Add(Me.dg_collection_bills)
    Me.tab_billetes.Controls.Add(Me.gb_status)
    Me.tab_billetes.Location = New System.Drawing.Point(4, 22)
    Me.tab_billetes.Name = "tab_billetes"
    Me.tab_billetes.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_billetes.Size = New System.Drawing.Size(637, 286)
    Me.tab_billetes.TabIndex = 0
    Me.tab_billetes.Text = "TabPage1"
    Me.tab_billetes.UseVisualStyleBackColor = True
    '
    'dg_collection_bills
    '
    Me.dg_collection_bills.CurrentCol = -1
    Me.dg_collection_bills.CurrentRow = -1
    Me.dg_collection_bills.Location = New System.Drawing.Point(6, 6)
    Me.dg_collection_bills.Name = "dg_collection_bills"
    Me.dg_collection_bills.PanelRightVisible = False
    Me.dg_collection_bills.Redraw = True
    Me.dg_collection_bills.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_collection_bills.Size = New System.Drawing.Size(496, 277)
    Me.dg_collection_bills.Sortable = False
    Me.dg_collection_bills.SortAscending = True
    Me.dg_collection_bills.SortByCol = 0
    Me.dg_collection_bills.TabIndex = 118
    Me.dg_collection_bills.ToolTipped = True
    Me.dg_collection_bills.TopRow = -2
    '
    'tab_ticket
    '
    Me.tab_ticket.Controls.Add(Me.gb_status1)
    Me.tab_ticket.Controls.Add(Me.dg_collection_tickets)
    Me.tab_ticket.Controls.Add(Me.ef_ticket)
    Me.tab_ticket.Controls.Add(Me.btn_save_tickets)
    Me.tab_ticket.Location = New System.Drawing.Point(4, 22)
    Me.tab_ticket.Name = "tab_ticket"
    Me.tab_ticket.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_ticket.Size = New System.Drawing.Size(637, 286)
    Me.tab_ticket.TabIndex = 1
    Me.tab_ticket.Text = "TabPage2"
    Me.tab_ticket.UseVisualStyleBackColor = True
    '
    'gb_status1
    '
    Me.gb_status1.Controls.Add(Me.lbl_error1)
    Me.gb_status1.Controls.Add(Me.tf_error1)
    Me.gb_status1.Controls.Add(Me.lbl_ok1)
    Me.gb_status1.Controls.Add(Me.tf_ok1)
    Me.gb_status1.Location = New System.Drawing.Point(508, 6)
    Me.gb_status1.Name = "gb_status1"
    Me.gb_status1.Size = New System.Drawing.Size(123, 70)
    Me.gb_status1.TabIndex = 111
    Me.gb_status1.TabStop = False
    Me.gb_status1.Text = "xStatus"
    '
    'lbl_error1
    '
    Me.lbl_error1.BackColor = System.Drawing.SystemColors.GrayText
    Me.lbl_error1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_error1.Location = New System.Drawing.Point(10, 44)
    Me.lbl_error1.Name = "lbl_error1"
    Me.lbl_error1.Size = New System.Drawing.Size(16, 16)
    Me.lbl_error1.TabIndex = 110
    '
    'tf_error1
    '
    Me.tf_error1.IsReadOnly = True
    Me.tf_error1.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_error1.LabelForeColor = System.Drawing.Color.Black
    Me.tf_error1.Location = New System.Drawing.Point(4, 40)
    Me.tf_error1.Name = "tf_error1"
    Me.tf_error1.Size = New System.Drawing.Size(116, 24)
    Me.tf_error1.SufixText = "Sufix Text"
    Me.tf_error1.TabIndex = 109
    Me.tf_error1.TabStop = False
    Me.tf_error1.TextVisible = False
    Me.tf_error1.TextWidth = 30
    Me.tf_error1.Value = "xInactive"
    '
    'lbl_ok1
    '
    Me.lbl_ok1.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_ok1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_ok1.Location = New System.Drawing.Point(10, 19)
    Me.lbl_ok1.Name = "lbl_ok1"
    Me.lbl_ok1.Size = New System.Drawing.Size(16, 16)
    Me.lbl_ok1.TabIndex = 106
    '
    'tf_ok1
    '
    Me.tf_ok1.IsReadOnly = True
    Me.tf_ok1.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.tf_ok1.LabelForeColor = System.Drawing.Color.Black
    Me.tf_ok1.Location = New System.Drawing.Point(4, 15)
    Me.tf_ok1.Name = "tf_ok1"
    Me.tf_ok1.Size = New System.Drawing.Size(114, 24)
    Me.tf_ok1.SufixText = "Sufix Text"
    Me.tf_ok1.TabIndex = 104
    Me.tf_ok1.TabStop = False
    Me.tf_ok1.TextVisible = False
    Me.tf_ok1.TextWidth = 30
    Me.tf_ok1.Value = "xRunning"
    '
    'dg_collection_tickets
    '
    Me.dg_collection_tickets.CurrentCol = -1
    Me.dg_collection_tickets.CurrentRow = -1
    Me.dg_collection_tickets.Location = New System.Drawing.Point(6, 41)
    Me.dg_collection_tickets.Name = "dg_collection_tickets"
    Me.dg_collection_tickets.PanelRightVisible = False
    Me.dg_collection_tickets.Redraw = True
    Me.dg_collection_tickets.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_collection_tickets.Size = New System.Drawing.Size(496, 242)
    Me.dg_collection_tickets.Sortable = False
    Me.dg_collection_tickets.SortAscending = True
    Me.dg_collection_tickets.SortByCol = 0
    Me.dg_collection_tickets.TabIndex = 2
    Me.dg_collection_tickets.ToolTipped = True
    Me.dg_collection_tickets.TopRow = -2
    '
    'ef_ticket
    '
    Me.ef_ticket.DoubleValue = 0
    Me.ef_ticket.IntegerValue = 0
    Me.ef_ticket.IsReadOnly = False
    Me.ef_ticket.Location = New System.Drawing.Point(6, 6)
    Me.ef_ticket.Name = "ef_ticket"
    Me.ef_ticket.Size = New System.Drawing.Size(400, 25)
    Me.ef_ticket.SufixText = "Sufix Text"
    Me.ef_ticket.TabIndex = 0
    Me.ef_ticket.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_ticket.TextValue = ""
    Me.ef_ticket.TextVisible = False
    Me.ef_ticket.Value = ""
    '
    'btn_save_tickets
    '
    Me.btn_save_tickets.DialogResult = System.Windows.Forms.DialogResult.None
    Me.btn_save_tickets.Location = New System.Drawing.Point(412, 6)
    Me.btn_save_tickets.Name = "btn_save_tickets"
    Me.btn_save_tickets.Size = New System.Drawing.Size(90, 30)
    Me.btn_save_tickets.TabIndex = 1
    Me.btn_save_tickets.ToolTipped = False
    Me.btn_save_tickets.Type = GUI_Controls.uc_button.ENUM_BUTTON_TYPE.NORMAL
    '
    'Label1
    '
    Me.Label1.BackColor = System.Drawing.SystemColors.GrayText
    Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label1.Location = New System.Drawing.Point(10, 63)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(16, 16)
    Me.Label1.TabIndex = 110
    '
    'Uc_text_field1
    '
    Me.Uc_text_field1.IsReadOnly = True
    Me.Uc_text_field1.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.Uc_text_field1.LabelForeColor = System.Drawing.Color.Black
    Me.Uc_text_field1.Location = New System.Drawing.Point(4, 59)
    Me.Uc_text_field1.Name = "Uc_text_field1"
    Me.Uc_text_field1.Size = New System.Drawing.Size(116, 24)
    Me.Uc_text_field1.SufixText = "Sufix Text"
    Me.Uc_text_field1.SufixTextVisible = True
    Me.Uc_text_field1.TabIndex = 109
    Me.Uc_text_field1.TabStop = False
    Me.Uc_text_field1.TextVisible = False
    Me.Uc_text_field1.TextWidth = 30
    Me.Uc_text_field1.Value = "xInactive"
    '
    'Label2
    '
    Me.Label2.BackColor = System.Drawing.SystemColors.GrayText
    Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label2.Location = New System.Drawing.Point(10, 41)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(16, 16)
    Me.Label2.TabIndex = 107
    '
    'Uc_text_field2
    '
    Me.Uc_text_field2.IsReadOnly = True
    Me.Uc_text_field2.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.Uc_text_field2.LabelForeColor = System.Drawing.Color.Black
    Me.Uc_text_field2.Location = New System.Drawing.Point(4, 37)
    Me.Uc_text_field2.Name = "Uc_text_field2"
    Me.Uc_text_field2.Size = New System.Drawing.Size(116, 24)
    Me.Uc_text_field2.SufixText = "Sufix Text"
    Me.Uc_text_field2.SufixTextVisible = True
    Me.Uc_text_field2.TabIndex = 108
    Me.Uc_text_field2.TabStop = False
    Me.Uc_text_field2.TextVisible = False
    Me.Uc_text_field2.TextWidth = 30
    Me.Uc_text_field2.Value = "xStandby"
    '
    'Label3
    '
    Me.Label3.BackColor = System.Drawing.SystemColors.Window
    Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label3.Location = New System.Drawing.Point(10, 19)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(16, 16)
    Me.Label3.TabIndex = 106
    '
    'Uc_text_field3
    '
    Me.Uc_text_field3.IsReadOnly = True
    Me.Uc_text_field3.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.Uc_text_field3.LabelForeColor = System.Drawing.Color.Black
    Me.Uc_text_field3.Location = New System.Drawing.Point(4, 15)
    Me.Uc_text_field3.Name = "Uc_text_field3"
    Me.Uc_text_field3.Size = New System.Drawing.Size(114, 24)
    Me.Uc_text_field3.SufixText = "Sufix Text"
    Me.Uc_text_field3.SufixTextVisible = True
    Me.Uc_text_field3.TabIndex = 104
    Me.Uc_text_field3.TabStop = False
    Me.Uc_text_field3.TextVisible = False
    Me.Uc_text_field3.TextWidth = 30
    Me.Uc_text_field3.Value = "xRunning"
    '
    'frm_new_collection_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(772, 682)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Name = "frm_new_collection_edit"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_new_collection_edit"
    Me.panel_data.ResumeLayout(False)
    Me.gb_collection_params.ResumeLayout(False)
    Me.gb_collection_params.PerformLayout()
    Me.gb_theoretical_values.ResumeLayout(False)
    Me.gb_real_values.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.tb_collections.ResumeLayout(False)
    Me.tab_billetes.ResumeLayout(False)
    Me.tab_ticket.ResumeLayout(False)
    Me.gb_status1.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_terminal As GUI_Controls.uc_entry_field
  Friend WithEvents ef_employee As GUI_Controls.uc_entry_field
  Friend WithEvents gb_collection_params As System.Windows.Forms.GroupBox
  Friend WithEvents gb_theoretical_values As System.Windows.Forms.GroupBox
  Friend WithEvents gb_real_values As System.Windows.Forms.GroupBox
  Friend WithEvents ef_real_recolected_in_notes As GUI_Controls.uc_entry_field
  Friend WithEvents ef_real_total_recollected As GUI_Controls.uc_entry_field
  Friend WithEvents ef_real_recolected_in_tickets As GUI_Controls.uc_entry_field
  Friend WithEvents ef_theoretical_total_collected As GUI_Controls.uc_entry_field
  Friend WithEvents ef_theoretical_collected_in_tickets As GUI_Controls.uc_entry_field
  Friend WithEvents ef_theoretical_collected_in_notes As GUI_Controls.uc_entry_field
  Friend WithEvents btn_start_collection As GUI_Controls.uc_button
  Friend WithEvents ef_insertion_date As GUI_Controls.uc_entry_field
  Friend WithEvents ef_extraction_date As GUI_Controls.uc_entry_field
  Friend WithEvents ef_collection_date As GUI_Controls.uc_entry_field
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_error As System.Windows.Forms.Label
  Friend WithEvents tf_error As GUI_Controls.uc_text_field
  Friend WithEvents lbl_ok As System.Windows.Forms.Label
  Friend WithEvents tf_ok As GUI_Controls.uc_text_field
  Friend WithEvents tb_collections As System.Windows.Forms.TabControl
  Friend WithEvents tab_billetes As System.Windows.Forms.TabPage
  Friend WithEvents tab_ticket As System.Windows.Forms.TabPage
  Friend WithEvents btn_save_tickets As GUI_Controls.uc_button
  Friend WithEvents ef_ticket As GUI_Controls.uc_entry_field
  Friend WithEvents dg_collection_bills As GUI_Controls.uc_grid
  Friend WithEvents dg_collection_tickets As GUI_Controls.uc_grid
  Friend WithEvents gb_status1 As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_error1 As System.Windows.Forms.Label
  Friend WithEvents tf_error1 As GUI_Controls.uc_text_field
  Friend WithEvents lbl_ok1 As System.Windows.Forms.Label
  Friend WithEvents tf_ok1 As GUI_Controls.uc_text_field
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents Uc_text_field1 As GUI_Controls.uc_text_field
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents Uc_text_field2 As GUI_Controls.uc_text_field
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents Uc_text_field3 As GUI_Controls.uc_text_field
  Friend WithEvents uc_pr_list As GUI_Controls.uc_provider
  Friend WithEvents cmb_stacker_id As System.Windows.Forms.ComboBox
  Friend WithEvents lb_stacker_id As System.Windows.Forms.Label
  Friend WithEvents lbl_notes As System.Windows.Forms.Label
  Friend WithEvents tb_notes As System.Windows.Forms.TextBox
End Class
