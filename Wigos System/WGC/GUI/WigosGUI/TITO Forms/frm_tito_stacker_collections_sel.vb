'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_tito_stacker_collections_sel
' DESCRIPTION:   This screen allows to view the stacker collections.
' AUTHOR:        Francesc Borrell
' CREATION DATE: 01-JUL-2013
'
' REVISION HISTORY:
'

' Date         Author Description
' -----------  ------ -----------------------------------------------
' 01-JUL-2013  FBA    Initial version
' 20-AUG-2013  JRM    Completing the "Selection" functionallity
' 01-OCT-2013  JFC    Adding new groupbox "Collection state"
' 14-OCT-2013  DRV    Added another header row to the grid, and changed how is data shown, also added another status to filter 
' 04-DEC-2013  AMF    Use mdl_tito.CollectionStatusAsString
' 18-DEC-2013  ACM    Fixed Bug #WIGOSTITO-864. Set only Sas-Host terminals to terminals filter control.
' 08-JAN-2014  JPJ    Query and code optimization.
' 10-FEB-2014  DRV    Removed all references to frm_cashier_collection_edit, and removed unused functions
' 25-FEB-2014  ICS & FBA  Added support for terminal collection with cage movement
' 27-FEB-2014  LEM    Fixed Bug WIGOSTITO-1097: Wrong message to confirm changes on closed collections view
' 12-MAR-2014  ICS    Fixed Bug WIG-706: Wrong total bills amount in stacker collection form
' 17-MAR-2014  JCA    Fixed Bug WIG-741: Promobox or Sys-Acceptor Collections can not open when is collected
' 18-MAR-2014  DRV    Fixed Bug WIGOSTITO-1148: "Open with activity" status was not shown and filter wasn't working
' 19-MAR-2014  JCA    Fixed Bug WIGOSTITO-1152: The general parameter is ignored "Cage - Cage Collection.Default Session"
' 20-MAR-2014  DRV    Added totalisator, changed colors for money collection status, and added new status 
' 20-MAR-2014  JAB    Added functionality to print cage movement details.
' 26-MAR-2014  DRV    Fixed Bug WIG-767: Exception on select totalisator row
' 27-MAR-2014  ICS    Fixed Bug #WIGOSTITO-1168. The filter 'Some' requires selecting at least one
' 02-APR-2014  CCG    Fixed Bug WIG-791: Don't show the cage sessi�n form when GP Cage.Collection.DefaultCageSession = 2 and there is only 1 cash cage sessi�n opened.
' 19-MAY-2014  RCI & DRV Fixed Bug WIG-921
' 20-MAY-2014  HBB    Fixeg Bug WIGOSTITO-1227: it cant be filtered collections without stacker. It is added an option "Without Stacker" in stackers list
' 05-JUN-2014  JBC    Fixed Bug WIG-1006: Change request to see all values from bills and tickets.
' 30-SEP-2014  LEM    Fixed Bug WIG-1340: Option "Show terminal location" do not work correctly.
' 22-OCT-2014  LEM    Use FORM_CAGE_SHOW_EXPECTED_AMOUNTS permission to Show/Hide expected/difference columns reports.
' 16-JAN-2014  ANM    Disable controls when selection is filling, and enable them again when finish filling
' 30-MAR-2015  ANM    Calling GetProviderIdListSelected always returns a query
' 11-MAY-2015  DLL    Fixed Bug WIG-2301: Status filter is incorrect
' 15-MAY-2015  YNM    Fixed Bug WIG-2301: Unhandled exception when the total row is selected
' 10-JUL-2015  DRV    Fixed Bug WIG-2457: Not filtering by FloorId on Massive collection
' 15-SEP-2015  DCS    Backlog Item 3707 - Coin Collection
' 04-NOV-2015  MPO    Bug 6085: Money collection status wrong for coin expected
' 05-NOV-2015  MPO    Bug 6224: Filter status wrong
' 27-NOV-2016  OVS    Modified button tag (Cancel -> Exit)
' 11-SEP-2017  RGR    Bug 29696:WIGOS-3728 Minor errors in Collection History screen
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports WSI.Common
Imports System.Data.SqlClient

#Region "Enums"
Public Enum ENUM_SUBTOTAL_SHOW_TYPE
  SHOW_SUBTOTAL_BY_PROVIDER
  SHOW_SUBTOTAL_BY_DATE
  SHOW_SUBTOTAL_BY_TERMINAL
End Enum
#End Region

Public Class frm_stacker_collections_sel
  Inherits frm_base_sel


#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_STACKER_COLLECTION_ID As Int32 = 0
  Private Const SQL_COLUMN_COLLECTION_EMPLOYEE As Int32 = 1
  Private Const SQL_COLUMN_STACKER_IDENTIFIER As Int32 = 2
  Private Const SQL_COLUMN_PROVIDER As Int32 = 3
  Private Const SQL_COLUMN_TERMINAL As Int32 = 4
  Private Const SQL_COLUMN_BILLS_EXPECTED_AMOUNT As Int32 = 5
  Private Const SQL_COLUMN_BILLS_COLLECTED_AMOUNT As Int32 = 6
  Private Const SQL_COLUMN_COINS_EXPECTED_AMOUNT As Int32 = 7 'TITO
  Private Const SQL_COLUMN_COINS_COLLECTED_AMOUNT As Int32 = 8 'TITO
  Private Const SQL_COLUMN_TICKETS_EXPECTED_AMOUNT As Int32 = 9 'TITO
  Private Const SQL_COLUMN_TICKETS_COLLECTED_AMOUNT As Int32 = 10 'TITO  
  Private Const SQL_COLUMN_BALANCE_STATE As Int32 = 11
  Private Const SQL_COLUMN_EXTRACTION_DATETIME As Int32 = 12
  Private Const SQL_COLUMN_MC_DATETIME As Int32 = 13
  Private Const SQL_COLUMN_COLLECTION_DATETIME As Int32 = 14
  Private Const SQL_COLUMN_TERMINAL_TYPE As Int32 = 15
  Private Const SQL_COLUMN_MC_CASHIER_SESSION_ID As Int32 = 16
  Private Const SQL_COLUMN_TERMINAL_ID As Int32 = 17
  Private Const SQL_COLUMN_CAGE_SESSION_DATE As Int32 = 18

  ' DB Columns for combos
  Private Const SQL_COLUMN_ID As Int32 = 0
  Private Const SQL_COLUMN_NAME As Int32 = 1

  Private TERMINAL_DATA_COLUMNS As Int32

  ' Grid configuration
  Private GRID_COLUMNS As Int32
  Private Const GRID_HEADER_ROWS As Int32 = 2

  ' Grid Columns
  Private GRID_COLUMN_SELECT As Int32
  Private GRID_COLUMN_STACKER_COLLECTION_START_DATE As Int32
  Private GRID_COLUMN_STACKER_EXTRACTION_DATE As Int32
  Private GRID_COLUMN_STACKER_COLLECTION_DATE As Int32
  Private GRID_COLUMN_CAGE_SESSION_DATE As Int32
  Private GRID_COLUMN_COLLECTION_EMPLOYEE As Int32
  Private GRID_COLUMN_BALANCE_STATUS As Int32
  Private GRID_COLUMN_STACKER_ID As Int32
  Private GRID_COLUMN_STACKER_COLLECTION_ID As Int32
  Private GRID_INIT_TERMINAL_DATA As Int32
  Private GRID_COLUMN_TERMINAL_PROVIDER As Int32
  Private GRID_COLUMN_TERMINAL As Int32
  Private GRID_COLUMN_BILLS_EXPECTED_AMOUNT As Int32
  Private GRID_COLUMN_BILLS_COLLECTED_AMOUNT As Int32
  Private GRID_COLUMN_BILLS_DIFERENCE As Int32
  Private GRID_COLUMN_BILLS_PERCENT As Int32
  Private GRID_COLUMN_TICKETS_EXPECTED_AMOUNT As Int32
  Private GRID_COLUMN_TICKETS_COLLECTED_AMOUNT As Int32
  Private GRID_COLUMN_TICKETS_DIFERENCE As Int32
  Private GRID_COLUMN_TICKETS_PERCENT As Int32
  Private GRID_COLUMN_COINS_EXPECTED_AMOUNT As Int32
  Private GRID_COLUMN_COINS_COLLECTED_AMOUNT As Int32
  Private GRID_COLUMN_COINS_DIFERENCE As Int32
  Private GRID_COLUMN_COINS_PERCENT As Int32
  Private GRID_COLUMN_TERMINAL_TYPE As Int32
  Private GRID_COLUMN_MC_CASHIER_SESSION_ID As Int32

  ' Columns Width
  Private Const GRID_COLUMN_WIDTH_SELECT As Int32 = 200
  Private Const GRID_WIDTH_COLLECTION_ID As Int32 = 0
  Private Const GRID_WIDTH_COLUMN_TERMINAL_TYPE As Int32 = 0
  Private Const GRID_WIDTH_COLLECTION_DATE As Int32 = 2000
  Private Const GRID_WIDTH_COLLECTION_DATE_DETAILED As Int32 = 2500
  Private Const GRID_COLUMN_WIDTH_COLLECTION_EMPLOYEE As Int32 = 1200
  Private Const GRID_WIDTH_STACKER_ID As Int32 = 1100
  Private Const GRID_WIDTH_TERMINAL As Int32 = 1500
  '  Private Const GRID_WIDTH_TERMINAL_TYPE As Integer = 0 'de momento no por qu� s�lo hay un tipo de terminal
  Private Const GRID_WIDTH_NUMBER_OF_BILLS As Integer = 1000
  Private Const GRID_WIDTH_BILLS_TOTAL_AMOUNT As Int32 = 1300
  Private Const GRID_WIDTH_NUMBER_OF_COINS As Int32 = 1000
  Private Const GRID_WIDTH_COINS_TOTAL_AMOUNT As Int32 = 1300
  Private Const GRID_WIDTH_NUMBER_OF_TICKETS As Int32 = 1000
  Private Const GRID_WIDTH_TICKETS_TOTAL_AMOUNT As Int32 = 1300
  Private Const GRID_WIDTH_BALANCE_STATE As Int32 = 2500
  Private Const GRID_WIDTH_TERMINAL_PROVIDER As Int32 = 1500
  Private Const GRID_WIDTH_STACKER_EXTRACTION_DATE As Int32 = 2000
  Private Const GRID_WIDTH_CAGE_SESSION_DATE As Int32 = 2600
  Private Const GRID_WIDTH_COLLECTION_EMPLOYE As Int32 = 1800

  ' Text format columns
  Private Const EXCEL_COLUMN_COLLECTION_EMPLOYEE As Integer = 2

  ' Stackers Grid
  Private Const GRID_STACKERS_HEADER_ROWS As Int32 = 0
  Private Const GRID_STACKERS_COLUMNS As Int32 = 2
  Private Const GRID_STACKERS_COLUMN_CHECKED As Int32 = 0
  Private Const GRID_STACKERS_COLUMN_ID As Int32 = 1
  Private Const GRID_STACKER_FIRST_ROW_WITHOUT_STACKER = 0

  'Stackers Grid Width
  Private Const GRID_STACKERS_WIDTH_COLUMN_DESCRIPTION As Int32 = 2700
  Private Const GRID_STACKERS_WIDTH_COLUMN_CHECKED As Int32 = 300

  ' Stackers Sql columns
  Private Const SQL_COLUMN_STACKER_ID As Int32 = 0


  Private Const COUNTER_OPEN As Int32 = 1
  Private Const COUNTER_OPEN_WITH_ACTIVITY As Int32 = 2
  Private Const COUNTER_PENDING As Int32 = 3
  Private Const COUNTER_BALANCED As Int32 = 4
  Private Const COUNTER_IMBALANCE As Int32 = 5

  Private Const TICKET_DENOMINATION As Int32 = -200


#End Region ' Constants

#Region " Members "

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String
  Private m_stacker_id As String
  Private m_employee_id As String
  Private m_terminals As String
  Private m_status As String
  Private m_options As String
  Private m_cashier_session_id As Long
  Private m_cashier_session_name As String
  Private m_floor_id As String
  Private m_stackers As String
  Private m_total_bill_expected_amount As Decimal
  Private m_total_bill_collected_amount As Decimal
  Private m_total_coin_expected_amount As Decimal
  Private m_total_coin_collected_amount As Decimal
  Private m_total_ticket_expected_amount As Decimal
  Private m_total_ticket_collected_amount As Decimal

  Private m_subtotal_bill_expected_amount As Decimal
  Private m_subtotal_bill_collected_amount As Decimal
  Private m_subtotal_coin_expected_amount As Decimal
  Private m_subtotal_coin_collected_amount As Decimal
  Private m_subtotal_ticket_expected_amount As Decimal
  Private m_subtotal_ticket_collected_amount As Decimal
  Private m_subtotal_date_since As DateTime
  Private m_subtotal_date_until As DateTime
  Private m_subtotal_provider As String
  Private m_subtotal_terminal As String
  Private m_terminal_report_type As ReportType = ReportType.Provider
  Private m_refresh_grid As Boolean = False

  Private m_show_expected_data As Boolean
  Private m_group As Boolean
  Private m_group_by_date As Boolean
  Private m_group_by_provider As Boolean
  Private m_show_details As Boolean
  Private m_index_of_date_to_calculate_subtotal As Int32
  Private m_index_to_print_subtotal_date As Int32
  Private m_insert_last_row As Boolean

  Public m_selected_money_collection As Int64 = -1
  Public m_show_terminal_location As Boolean

#End Region ' Members 

#Region " Overrides "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_STACKER_COLLECTION_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE : Initialize every form control
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2222)

    Me.m_show_expected_data = CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_SHOW_EXPECTED_AMOUNTS).Read

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2884)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False ' TODO: desactivado; debe hacerse desde frm_cashier_session_detail
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Enabled = Me.Permissions.Write And False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1) 'PBI 19515
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2191)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False

    ' Employee
    Me.gb_user.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4577)
    Me.opt_one_employee.Text = GLB_NLS_GUI_ALARMS.GetString(276)
    Me.opt_all_employee.Text = GLB_NLS_GUI_ALARMS.GetString(277)
    Me.cmb_employee.IsReadOnly = False

    ' Dates
    gb_date_filter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2118)
    opt_insertion_datetime.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5734)
    opt_collection_datetime.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5735)
    opt_cage_session_datetime.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5715)
    opt_insertion_datetime.Checked = True

    ' Stacker 
    Me.gb_stacker.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2199)
    dg_stackers.PanelRightVisible = False
    dg_stackers.Sortable = False
    dg_stackers.TopRow = -2

    bt_check_all.Text = GLB_NLS_GUI_ALARMS.GetString(452)
    bt_uncheck_all.Text = GLB_NLS_GUI_ALARMS.GetString(453)

    'state
    Me.gb_collections_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2231)
    Me.chk_inserted_validated.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2742)
    Me.chk_pending_collection.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2743)
    Me.chk_collection_status_correct.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2232)
    Me.chk_collection_status_incorrect.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2233)
    Me.chk_open_with_activity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4737)

    'Colors
    Me.lbl_open.BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
    Me.lbl_pending.BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLUE_01)
    Me.lbl_balanced.BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_GREEN_00)
    Me.lbl_imbalance.BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_00)
    Me.lbl_open_with_activity.BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_GREY_03)

    ' Fill up uc_provider
    Dim _terminal_types() As Integer = Nothing
    _terminal_types = WSI.Common.Misc.AcceptorTerminalTypeList()

    Call Me.uc_provider.Init(_terminal_types)

    ' Fill combo boxes 
    Call SetCombo(Me.cmb_employee, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS WHERE GU_USER_TYPE = 0 ORDER BY GU_USERNAME")
    ' Call SetCombo(Me.cmb_stacker, "SELECT ST_STACKER_ID, ST_STACKER_ID FROM STACKERS ORDER BY ST_MODEL")

    ' Define layout of all Main Grid Columns
    Call GUI_StyleSheet()
    If TITO.Utils.IsTitoMode() Then
      Call GUI_StyleSheetStackers()
      Call GUI_GetSqlQueryStackers()
      Me.chk_terminal_location.Location = New Point(Me.uc_provider.Location.X + Me.uc_provider.Width + 5, Me.chk_terminal_location.Location.Y)
      Me.gb_group_by.Location = New Point(Me.chk_terminal_location.Location.X, Me.chk_terminal_location.Location.Y + 20)
    Else
      Me.gb_stacker.Visible = False
      Me.gb_user.Location = New Point(Me.gb_collections_status.Location.X, Me.gb_collections_status.Location.Y)
      Me.gb_user.Width = gb_date_filter.Width
      Me.gb_collections_status.Location = New Point(Me.gb_stacker.Location.X, Me.gb_stacker.Location.Y)
      Me.uc_provider.Location = New Point(Me.gb_stacker.Location.X + Me.gb_collections_status.Size.Width + 2, Me.uc_provider.Location.Y)
      Me.panel_filter.Size = New Size(Me.panel_filter.Size.Width, 235)
      Me.chk_terminal_location.Location = New Point(Me.gb_collections_status.Location.X + 7, Me.gb_collections_status.Location.Y + Me.gb_collections_status.Height + 5)
      Me.cmb_employee.Width = gb_user.Location.X + gb_user.Width - cmb_employee.Location.X - 10
      Me.gb_group_by.Height = Me.uc_provider.Height - 5
      Me.gb_group_by.Location = New Point(Me.uc_provider.Location.X + Me.uc_provider.Width + 5, Me.gb_date_filter.Location.Y)
    End If

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    ' Group by groupbox
    gb_group_by.Text = GLB_NLS_GUI_INVOICING.GetString(369)
    chk_group_by.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2990)
    opt_group_by_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2251)
    opt_provider.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(469)
    chk_show_detail.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5721)
    opt_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5730)

    'ds_from_to.ShowTitle = False

    opt_group_by_day.Enabled = False
    opt_provider.Enabled = False
    opt_terminal.Enabled = False
    opt_group_by_day.Checked = True
    chk_show_detail.Enabled = False
    chk_show_detail.Checked = True
    m_group_by_date = False
    m_group_by_provider = False
    m_subtotal_date_since = Nothing
    m_subtotal_date_until = Nothing

    opt_insertion_datetime.Enabled = True
    opt_collection_datetime.Enabled = False
    opt_cage_session_datetime.Enabled = False
    m_show_terminal_location = False

    ' Set filter default values
    Call GUI_FilterReset()

    If Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING Then
      Call SetNothingModeForMoneyCollectionSelection()
    End If

  End Sub ' GUI_InitControls

  ' PURPOSE : Initialize all form filters with their default values
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub 'Gui_FilterReset

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sb As StringBuilder
    Dim _order_by_date As String

    _order_by_date = ""
    If opt_insertion_datetime.Checked Then
      _order_by_date = " MC_DATETIME DESC"
    ElseIf opt_collection_datetime.Checked Then
      _order_by_date = " MC_COLLECTION_DATETIME DESC"
    Else
      _order_by_date = " CGS_WORKING_DAY DESC"
    End If

    _sb = New StringBuilder()
    _sb.AppendLine("    SELECT   MC_COLLECTION_ID           ")
    _sb.AppendLine(",            GU_USERNAME                ")
    _sb.AppendLine(",            MC_STACKER_ID              ")
    _sb.AppendLine(",            TE_PROVIDER_ID             ")
    _sb.AppendLine(",            TE_NAME                    ")
    _sb.AppendLine(",            MC_EXPECTED_BILL_AMOUNT    ")
    _sb.AppendLine(",            MC_COLLECTED_BILL_AMOUNT   ")
    _sb.AppendLine(",            MC_EXPECTED_COIN_AMOUNT    ")
    _sb.AppendLine(",            MC_COLLECTED_COIN_AMOUNT   ")
    _sb.AppendLine(",            MC_EXPECTED_TICKET_AMOUNT  ")
    _sb.AppendLine(",            MC_COLLECTED_TICKET_AMOUNT ")
    _sb.AppendLine(",            CASE WHEN MC_STATUS = 0 AND (ISNULL(MC_EXPECTED_BILL_COUNT,0) + ISNULL(MC_EXPECTED_COIN_AMOUNT,0) + ISNULL(MC_EXPECTED_TICKET_COUNT,0)) >0 THEN 4 ELSE MC_STATUS END AS MC_STATUS ")
    _sb.AppendLine(",            MC_EXTRACTION_DATETIME     ")
    _sb.AppendLine(",            MC_DATETIME                ")
    _sb.AppendLine(",            MC_COLLECTION_DATETIME     ")
    _sb.AppendLine(",            TE_TERMINAL_TYPE           ")
    _sb.AppendLine(",            MC_CASHIER_SESSION_ID      ")
    _sb.AppendLine(" ,           MC_TERMINAL_ID             ")

    If opt_cage_session_datetime.Checked OrElse opt_collection_datetime.Checked Then
      _sb.AppendLine(" ,           CGS_WORKING_DAY          ")
      _sb.AppendLine("      FROM   MONEY_COLLECTIONS          ")
      _sb.AppendLine("INNER JOIN   CAGE_MOVEMENTS ON MC_CASHIER_SESSION_ID = CGM_CASHIER_SESSION_ID")
      _sb.AppendLine("INNER JOIN   CAGE_SESSIONS  ON CGM_CAGE_SESSION_ID = CGS_CAGE_SESSION_ID")
    Else
      _sb.AppendLine("    FROM   MONEY_COLLECTIONS          ")
    End If

    _sb.AppendLine(" LEFT JOIN   TERMINALS ON MONEY_COLLECTIONS.MC_TERMINAL_ID = TERMINALS.TE_TERMINAL_ID    ")
    _sb.AppendLine(" LEFT JOIN   GUI_USERS ON MONEY_COLLECTIONS.MC_USER_ID = GUI_USERS.GU_USER_ID            ")

    _sb.AppendLine(GetSqlWhere())

    If chk_group_by.Checked Then
      If opt_provider.Checked Then
        _sb.AppendLine(" ORDER BY   TE_PROVIDER_ID ASC, " & _order_by_date)
      ElseIf opt_terminal.Checked Then
        _sb.AppendLine(" ORDER BY   TE_NAME ASC,  " & _order_by_date)
      Else
        _sb.AppendLine(" ORDER BY " & _order_by_date)
      End If
    Else
      _sb.AppendLine(" ORDER BY " & _order_by_date)
    End If

    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()

    m_total_bill_collected_amount = 0
    m_total_bill_expected_amount = 0
    m_total_ticket_collected_amount = 0
    m_total_ticket_expected_amount = 0
    m_total_coin_collected_amount = 0
    m_total_coin_expected_amount = 0

    If m_refresh_grid Then
      Call GUI_StyleSheet()
    End If
    If opt_group_by_day.Enabled AndAlso opt_group_by_day.Checked Then
      Me.Grid.Column(GRID_COLUMN_STACKER_COLLECTION_START_DATE).Width = GRID_WIDTH_COLLECTION_DATE_DETAILED
      Me.Grid.Column(GRID_COLUMN_STACKER_EXTRACTION_DATE).Width = GRID_WIDTH_COLLECTION_DATE_DETAILED
    Else
      Me.Grid.Column(GRID_COLUMN_STACKER_COLLECTION_START_DATE).Width = GRID_WIDTH_COLLECTION_DATE
      Me.Grid.Column(GRID_COLUMN_STACKER_COLLECTION_START_DATE).Width = GRID_WIDTH_COLLECTION_DATE
    End If

    m_refresh_grid = False

  End Sub

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS : 
  '     - True (the row should be added) or False (the row can not be added)
  '
  '   NOTES :
  '
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _blank_row As Boolean
    Dim _cell_color As Color
    Dim _amount_expected As Decimal
    Dim _amount_collected As Decimal
    Dim _status As TITO_MONEY_COLLECTION_STATUS
    Dim _terminal_data As List(Of Object)

    _blank_row = False
    _amount_expected = 0
    _status = TITO_MONEY_COLLECTION_STATUS.NONE

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_STACKER_COLLECTION_ID)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STACKER_COLLECTION_ID).Value = DbRow.Value(SQL_COLUMN_STACKER_COLLECTION_ID)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_COLLECTION_EMPLOYEE)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COLLECTION_EMPLOYEE).Value = DbRow.Value(SQL_COLUMN_COLLECTION_EMPLOYEE)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_STACKER_IDENTIFIER)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STACKER_ID).Value = DbRow.Value(SQL_COLUMN_STACKER_IDENTIFIER)
    End If

    ' Terminal Report
    _terminal_data = TerminalReport.GetReportDataList(DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_terminal_report_type)
    For _idx As Int32 = 0 To _terminal_data.Count - 1
      If Not _terminal_data(_idx) Is Nothing AndAlso Not _terminal_data(_idx) Is DBNull.Value Then
        Me.Grid.Cell(RowIndex, GRID_INIT_TERMINAL_DATA + _idx).Value = _terminal_data(_idx)
      End If
    Next

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TERMINAL_TYPE)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_TYPE).Value = DbRow.Value(SQL_COLUMN_TERMINAL_TYPE)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_MC_CASHIER_SESSION_ID)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_MC_CASHIER_SESSION_ID).Value = DbRow.Value(SQL_COLUMN_MC_CASHIER_SESSION_ID)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_BALANCE_STATE)) Then
      _status = DbRow.Value(SQL_COLUMN_BALANCE_STATE)
      If DbRow.Value(SQL_COLUMN_BALANCE_STATE) = TITO_MONEY_COLLECTION_STATUS.OPEN Then
        _blank_row = True
      End If
    Else
      _status = TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_BALANCE_STATUS).Value = mdl_tito.CollectionStatusAsString(IIf(IsDBNull(DbRow.Value(SQL_COLUMN_BALANCE_STATE)), TITO_MONEY_COLLECTION_STATUS.NONE, DbRow.Value(SQL_COLUMN_BALANCE_STATE)))

    _cell_color = PrintCollectionStatus(IIf(IsDBNull(DbRow.Value(SQL_COLUMN_BALANCE_STATE)), TITO_MONEY_COLLECTION_STATUS.NONE, DbRow.Value(SQL_COLUMN_BALANCE_STATE)))

    Me.Grid.Cell(RowIndex, GRID_COLUMN_BALANCE_STATUS).BackColor = _cell_color

    If _cell_color = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BALANCE_STATUS).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BALANCE_STATUS).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_MC_DATETIME)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STACKER_COLLECTION_START_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_MC_DATETIME), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_EXTRACTION_DATETIME)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STACKER_EXTRACTION_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_EXTRACTION_DATETIME), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_COLLECTION_DATETIME)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STACKER_COLLECTION_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_COLLECTION_DATETIME), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If (opt_cage_session_datetime.Checked OrElse opt_collection_datetime.Checked) AndAlso Not IsDBNull(DbRow.Value(SQL_COLUMN_CAGE_SESSION_DATE)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_CAGE_SESSION_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_CAGE_SESSION_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)
    End If

    'BILLS COLLECTED AMOUNT
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_BILLS_COLLECTED_AMOUNT)) And Not _blank_row Then
      If _status = TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE Or _status = TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_BILLS_COLLECTED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_BILLS_COLLECTED_AMOUNT))
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_BILLS_COLLECTED_AMOUNT).Value = String.Empty
      End If
      _amount_collected = DbRow.Value(SQL_COLUMN_BILLS_COLLECTED_AMOUNT)
      m_total_bill_collected_amount += DbRow.Value(SQL_COLUMN_BILLS_COLLECTED_AMOUNT)
    Else
      _amount_collected = 0
      If _blank_row Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_BILLS_COLLECTED_AMOUNT).Value = ""
      ElseIf _status = TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE Or _status = TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_BILLS_COLLECTED_AMOUNT).Value = GUI_FormatCurrency(0)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_BILLS_COLLECTED_AMOUNT).Value = ""
      End If
    End If

    'BILLS EXPECTED AMOUNT
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_BILLS_EXPECTED_AMOUNT)) And Not _blank_row Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BILLS_EXPECTED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_BILLS_EXPECTED_AMOUNT))
      _amount_expected = DbRow.Value(SQL_COLUMN_BILLS_EXPECTED_AMOUNT)
      m_total_bill_expected_amount += DbRow.Value(SQL_COLUMN_BILLS_EXPECTED_AMOUNT)
    Else
      _amount_expected = 0
      If _blank_row Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_BILLS_EXPECTED_AMOUNT).Value = ""
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_BILLS_EXPECTED_AMOUNT).Value = GUI_FormatCurrency(0)
      End If
    End If

    'BILLS DIFFENRECE
    If _status = TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE Or _status = TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BILLS_DIFERENCE).Value = GUI_FormatCurrency(Decimal.Subtract(_amount_collected, _amount_expected))
    End If


    'BILLS PERCENT
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BILLS_PERCENT).Value = GetPercentFromValues(_amount_expected, _amount_collected, _status)

    _amount_expected = 0
    _amount_collected = 0

    ' COINS COLLECTED AMOUNT
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_COINS_COLLECTED_AMOUNT)) And Not _blank_row Then
      If _status = TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE Or _status = TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_COINS_COLLECTED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COINS_COLLECTED_AMOUNT))
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_COINS_COLLECTED_AMOUNT).Value = String.Empty
      End If
      _amount_collected = DbRow.Value(SQL_COLUMN_COINS_COLLECTED_AMOUNT)
      m_total_coin_collected_amount += DbRow.Value(SQL_COLUMN_COINS_COLLECTED_AMOUNT)
    Else
      _amount_collected = 0
      If _blank_row Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_COINS_COLLECTED_AMOUNT).Value = ""
      ElseIf _status = TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE Or _status = TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_COINS_COLLECTED_AMOUNT).Value = GUI_FormatCurrency(0)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_COINS_COLLECTED_AMOUNT).Value = ""
      End If
    End If

    ' COINS EXPECTED AMOUNT
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_COINS_EXPECTED_AMOUNT)) And Not _blank_row Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COINS_EXPECTED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_COINS_EXPECTED_AMOUNT))
      _amount_expected = DbRow.Value(SQL_COLUMN_COINS_EXPECTED_AMOUNT)
      m_total_coin_expected_amount += DbRow.Value(SQL_COLUMN_COINS_EXPECTED_AMOUNT)
    Else
      _amount_expected = 0
      If _blank_row Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_COINS_EXPECTED_AMOUNT).Value = ""
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_COINS_EXPECTED_AMOUNT).Value = GUI_FormatCurrency(0)
      End If
    End If

    ' COINS DIFFENRECE
    If _status = TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE Or _status = TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COINS_DIFERENCE).Value = GUI_FormatCurrency(Decimal.Subtract(_amount_collected, _amount_expected))
    End If

    'COINS PERCENT
    Me.Grid.Cell(RowIndex, GRID_COLUMN_COINS_PERCENT).Value = GetPercentFromValues(_amount_expected, _amount_collected, _status)

    _amount_expected = 0
    _amount_collected = 0

    'TICKETS COLLECTED AMOUNT
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TICKETS_COLLECTED_AMOUNT)) And Not _blank_row Then
      If _status = TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE Or _status = TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TICKETS_COLLECTED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TICKETS_COLLECTED_AMOUNT))
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TICKETS_COLLECTED_AMOUNT).Value = String.Empty
      End If
      _amount_collected = DbRow.Value(SQL_COLUMN_TICKETS_COLLECTED_AMOUNT)
      m_total_ticket_collected_amount += DbRow.Value(SQL_COLUMN_TICKETS_COLLECTED_AMOUNT)
    Else
      _amount_collected = 0
      If _blank_row Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TICKETS_COLLECTED_AMOUNT).Value = ""
      ElseIf _status = TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE Or _status = TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TICKETS_COLLECTED_AMOUNT).Value = GUI_FormatCurrency(0)
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TICKETS_COLLECTED_AMOUNT).Value = ""
      End If
    End If

    'TICKETS EXPECTED AMOUNT
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TICKETS_EXPECTED_AMOUNT)) And Not _blank_row Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TICKETS_EXPECTED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TICKETS_EXPECTED_AMOUNT))
      _amount_expected = DbRow.Value(SQL_COLUMN_TICKETS_EXPECTED_AMOUNT)
      m_total_ticket_expected_amount += DbRow.Value(SQL_COLUMN_TICKETS_EXPECTED_AMOUNT)
    Else
      _amount_expected = 0
      If _blank_row Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TICKETS_EXPECTED_AMOUNT).Value = ""
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TICKETS_EXPECTED_AMOUNT).Value = GUI_FormatCurrency(0)
      End If
    End If

    'TICKETS DIFFENRECE
    If _status = TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE Or _status = TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TICKETS_DIFERENCE).Value = GUI_FormatCurrency(Decimal.Subtract(_amount_collected, _amount_expected))
    End If

    'TICKETS PERCENT
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TICKETS_PERCENT).Value = GetPercentFromValues(_amount_expected, _amount_collected, _status)

    Return True

  End Function ' GUI_SetupRow


  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Dim _idx_row As Integer

    If chk_group_by.Checked AndAlso m_insert_last_row Then
      If opt_group_by_day.Checked Then
        Call PrintSubTotalRow(ENUM_SUBTOTAL_SHOW_TYPE.SHOW_SUBTOTAL_BY_DATE, m_index_to_print_subtotal_date)
      ElseIf opt_provider.Checked Then
        Call PrintSubTotalRow(ENUM_SUBTOTAL_SHOW_TYPE.SHOW_SUBTOTAL_BY_PROVIDER)
      Else
        Call PrintSubTotalRow(ENUM_SUBTOTAL_SHOW_TYPE.SHOW_SUBTOTAL_BY_TERMINAL)
      End If
    End If

    _idx_row = Me.Grid.NumRows

    Me.Grid.AddRow()

    Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL_PROVIDER).Value = GLB_NLS_GUI_INVOICING.GetString(205)

    Me.Grid.Cell(_idx_row, GRID_COLUMN_BILLS_COLLECTED_AMOUNT).Value = GUI_FormatCurrency(m_total_bill_collected_amount, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_BILLS_EXPECTED_AMOUNT).Value = GUI_FormatCurrency(m_total_bill_expected_amount, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_COINS_COLLECTED_AMOUNT).Value = GUI_FormatCurrency(m_total_coin_collected_amount, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_COINS_EXPECTED_AMOUNT).Value = GUI_FormatCurrency(m_total_coin_expected_amount, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TICKETS_COLLECTED_AMOUNT).Value = GUI_FormatCurrency(m_total_ticket_collected_amount, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TICKETS_EXPECTED_AMOUNT).Value = GUI_FormatCurrency(m_total_ticket_expected_amount, 2)

    Me.Grid.Cell(_idx_row, GRID_COLUMN_BILLS_DIFERENCE).Value = GUI_FormatCurrency(m_total_bill_collected_amount - m_total_bill_expected_amount, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_BILLS_PERCENT).Value = GetPercentFromValues(m_total_bill_expected_amount, m_total_bill_collected_amount, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_COINS_DIFERENCE).Value = GUI_FormatCurrency(m_total_coin_collected_amount - m_total_coin_expected_amount, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_COINS_PERCENT).Value = GetPercentFromValues(m_total_coin_expected_amount, m_total_coin_collected_amount, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TICKETS_DIFERENCE).Value = GUI_FormatCurrency(m_total_ticket_collected_amount - m_total_ticket_expected_amount, 2)
    Me.Grid.Cell(_idx_row, GRID_COLUMN_TICKETS_PERCENT).Value = GetPercentFromValues(m_total_ticket_expected_amount, m_total_ticket_collected_amount, 2)

    Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    m_subtotal_bill_collected_amount = 0
    m_subtotal_bill_expected_amount = 0
    m_subtotal_coin_collected_amount = 0
    m_subtotal_coin_expected_amount = 0
    m_subtotal_ticket_collected_amount = 0
    m_subtotal_ticket_expected_amount = 0
    m_subtotal_provider = ""
    m_subtotal_terminal = ""
    m_subtotal_date_since = Nothing
    m_subtotal_date_until = Nothing

  End Sub

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    If Me.ds_from_to.FromDateSelected And Me.ds_from_to.ToDateSelected Then
      If Me.ds_from_to.FromDate > Me.ds_from_to.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.ds_from_to.Focus()

        Return False
      End If
    End If

    Return True

  End Function ' GUI_FilterCheck

  ' PURPOSE: Activated when a row of the grid is double clicked or selected, so it can be edited
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Int32
    Dim _frm_edit_cagecontrol As frm_cage_control
    Dim _status As String
    Dim _stacker_id As Long
    Dim _collection_id As Long
    Dim _terminal_name As String
    Dim _cls_cage_control As CLASS_CAGE_CONTROL
    Dim _cage_session_id As Long
    Dim _cage_movement_id As Long
    Dim _terminal_type As Long
    Dim _mb_user_type As MB_USER_TYPE
    Dim _gui_user_type As GU_USER_TYPE
    Dim _cashier_session_id As Long
    Dim _frm_cage_sessions As frm_cage_sessions
    Dim _cage_control_open_mode As CLASS_CAGE_CONTROL.OPEN_MODE

    _stacker_id = -1
    _terminal_name = ""
    _collection_id = -1
    _cls_cage_control = New CLASS_CAGE_CONTROL

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If Not Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled Then
      Return
    End If

    'get collection id
    If Me.Grid.Cell(_idx_row, GRID_COLUMN_STACKER_COLLECTION_ID).Value <> "" Then
      _collection_id = CLng(Me.Grid.Cell(_idx_row, GRID_COLUMN_STACKER_COLLECTION_ID).Value)
    Else
      Return
    End If

    'Get stacker id
    If Me.Grid.Cell(_idx_row, GRID_COLUMN_STACKER_ID).Value <> "" Then
      _stacker_id = CLng(Me.Grid.Cell(_idx_row, GRID_COLUMN_STACKER_ID).Value)
    End If

    'Get terminal name
    If Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL).Value <> "" Then
      _terminal_name = Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL).Value
    End If

    If Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL_TYPE).Value <> "" Then
      _terminal_type = CLng(Me.Grid.Cell(_idx_row, GRID_COLUMN_TERMINAL_TYPE).Value)
    End If

    If Me.Grid.Cell(_idx_row, GRID_COLUMN_MC_CASHIER_SESSION_ID).Value <> "" Then
      _cashier_session_id = CLng(Me.Grid.Cell(_idx_row, GRID_COLUMN_MC_CASHIER_SESSION_ID).Value)
    End If

    ' Get the Draw ID and Name, and launch the editor

    _status = Me.Grid.Cell(_idx_row, GRID_COLUMN_BALANCE_STATUS).Value

    Cashier.GetMobileBankAndGUIUserType(_terminal_type, _mb_user_type, _gui_user_type)

    ' It controls whether the cashier session is not (active or pending collection or active with activity)==>  Is closing balance or imbalance
    If Not (_status.Equals(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2742)) Or _
            _status.Equals(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4737)) Or _
            _status.Equals(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2171))) Then

      _cage_session_id = _cls_cage_control.GetCageSessionByMoneyCollection(_collection_id)
      _frm_edit_cagecontrol = New frm_cage_control(ENUM_FORM.FORM_STACKER_COLLECTION)

      If _gui_user_type = GU_USER_TYPE.SYS_TITO Then
        Call _frm_edit_cagecontrol.ShowEditItem(_stacker_id, _collection_id, False, _cage_session_id, True, _
                                                CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT, , , True)
      Else
        Call _frm_edit_cagecontrol.ShowEditItem(_stacker_id, _collection_id, False, _cage_session_id, True, _
                                                CLASS_CAGE_CONTROL.OPEN_MODE.CANCEL_MOVEMENT_FROM_PROMOBOX_OR_ACCEPTOR, _
                                                _cashier_session_id, _gui_user_type, True)
      End If
    Else

      If _status.Equals(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2171)) Then
        If Cage.GetNumberOfOpenedCages() < 1 Then
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(3378), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Else
          _frm_edit_cagecontrol = New frm_cage_control(ENUM_FORM.FORM_STACKER_COLLECTION)

          If Cage.GetCageMovement(_collection_id, _cage_movement_id) Then

            _cage_control_open_mode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT
            If _gui_user_type <> GU_USER_TYPE.SYS_TITO Then
              _cage_control_open_mode = CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_FROM_PROMOBOX_OR_ACCEPTOR
            End If

            Call _frm_edit_cagecontrol.ShowEditItem(_cage_movement_id, _cage_control_open_mode, , , _gui_user_type)
          Else

            If _cage_session_id = 0 Then
              Select Case Cage.CollectionDefaultCageSession()
                Case Cage.DefaultCageSession.FirstCageSessionOpened _
                   , Cage.DefaultCageSession.LastCageSessionOpened
                  If Not Cage.GetCurrentCageSessionId(_cage_session_id, Cage.CollectionDefaultCageSession()) Then
                    NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5014), ENUM_MB_TYPE.MB_TYPE_ERROR)
                    Return
                  End If

                Case Cage.DefaultCageSession.ChooseCageSession
                  If Cage.GetNumberOfOpenedCages() = 1 Then
                    ' True Or False doesn't matter, because we just want the Cash Cage session opened and there is only one.
                    Call Cage.GetCurrentCageSessionId(_cage_session_id, True)
                  Else
                    If Not CurrentUser.Permissions(ENUM_FORM.FORM_CAGE_SESSIONS).Read Then
                      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4373), ENUM_MB_TYPE.MB_TYPE_WARNING)

                      Return
                    End If

                    _frm_cage_sessions = New frm_cage_sessions()
                    Call _frm_cage_sessions.ShowForEdit(_cage_session_id)
                    Select Case _cage_session_id
                      Case -1
                        ' DRV 16-MAY-14: Returns -1 when none cage session has been selected
                        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5014), ENUM_MB_TYPE.MB_TYPE_ERROR)
                        Return

                      Case 0
                        ' DRV: 16-MAY-14: Returns 0 when the form hasn't been loaded (no user permissions)
                        Return

                    End Select
                  End If

                Case Else
                  NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(5014), ENUM_MB_TYPE.MB_TYPE_ERROR)
                  Return

              End Select
            End If

            If Not Cage.IsCageSessionOpen(_cage_session_id) Then
              NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4706), ENUM_MB_TYPE.MB_TYPE_ERROR)
              Return
            End If

            If TITO.Utils.IsTitoMode() Then
              Call _frm_edit_cagecontrol.ShowEditItem(_stacker_id, _collection_id, True, _cage_session_id, True, CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_MOVEMENT)
            Else
              Call _frm_edit_cagecontrol.ShowEditItem(_stacker_id, _collection_id, True, _cage_session_id, True, _
                                                          CLASS_CAGE_CONTROL.OPEN_MODE.RECOLECT_FROM_PROMOBOX_OR_ACCEPTOR, _cashier_session_id, _gui_user_type)
            End If
          End If
        End If
      Else
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2861), _
                            ENUM_MB_TYPE.MB_TYPE_INFO, _
                            ENUM_MB_BTN.MB_BTN_OK, _
                            ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
      End If

    End If

    Call Me.Grid.Focus()

  End Sub 'GUI_EditSelectedItem

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        If Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING Then
          Call GetSelectedMoneyCollection()
        Else
          Call GUI_EditSelectedItem()
        End If

      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call EnableControlsDuringSelection(False)
        Call MyBase.GUI_ButtonClick(ButtonId)
        Call EnableControlsDuringSelection(True)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub    ' GUI_ButtonClick


  ' PURPOSE : Checks out the values of a db row before adding it to the grid
  '           We use it here to add total data before adding "normal" data.
  '
  '  PARAMS :
  '     - INPUT :
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the db row should be added to the grid
  '     - False: the db row should NOT be added to the grid
  '
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean
    Dim _show_normal_row As Boolean
    Dim _subtotal_date_since As DateTime
    Dim _reset_subtotal_values As Boolean

    _show_normal_row = True
    _reset_subtotal_values = True

    If chk_group_by.Checked Then
      _show_normal_row = chk_show_detail.Checked

      If opt_group_by_day.Checked Then
        Call GetIndexDateSubtotal() ' calculate index to get the date that has to be shown and the column to print subtotal
        If DbRow.Value(m_index_of_date_to_calculate_subtotal) IsNot DBNull.Value Then
          If m_subtotal_date_since = Nothing Then
            _subtotal_date_since = CType(DbRow.Value(m_index_of_date_to_calculate_subtotal), Date).Date
            m_subtotal_date_since = New DateTime(_subtotal_date_since.Year, _subtotal_date_since.Month, _subtotal_date_since.Day, ds_from_to.ClosingTime, 0, 0)
            m_subtotal_date_until = m_subtotal_date_since.AddDays(1)

          ElseIf m_subtotal_date_since <= CType(DbRow.Value(m_index_of_date_to_calculate_subtotal), Date) _
                                  AndAlso m_subtotal_date_until >= CType(DbRow.Value(m_index_of_date_to_calculate_subtotal), Date) Then
            _reset_subtotal_values = False
          Else
            Call PrintSubTotalRow(ENUM_SUBTOTAL_SHOW_TYPE.SHOW_SUBTOTAL_BY_DATE, m_index_to_print_subtotal_date)
            _subtotal_date_since = CType(DbRow.Value(m_index_of_date_to_calculate_subtotal), Date).Date
            m_subtotal_date_since = New DateTime(_subtotal_date_since.Year, _subtotal_date_since.Month, _subtotal_date_since.Day, ds_from_to.ClosingTime, 0, 0)
            m_subtotal_date_until = m_subtotal_date_since.AddDays(1)
          End If
        End If
      ElseIf opt_provider.Checked AndAlso Not String.IsNullOrEmpty(DbRow.Value(SQL_COLUMN_PROVIDER).ToString()) Then
        If String.IsNullOrEmpty(m_subtotal_provider) Then
          m_subtotal_provider = DbRow.Value(SQL_COLUMN_PROVIDER).ToString()
        ElseIf m_subtotal_provider = DbRow.Value(SQL_COLUMN_PROVIDER).ToString() Then
          _reset_subtotal_values = False
        Else
          Call PrintSubTotalRow(ENUM_SUBTOTAL_SHOW_TYPE.SHOW_SUBTOTAL_BY_PROVIDER)
          m_subtotal_provider = DbRow.Value(SQL_COLUMN_PROVIDER).ToString()
        End If
      ElseIf Not String.IsNullOrEmpty(DbRow.Value(SQL_COLUMN_TERMINAL).ToString()) Then
        If String.IsNullOrEmpty(m_subtotal_terminal) Then
          m_subtotal_terminal = DbRow.Value(SQL_COLUMN_TERMINAL).ToString()
        ElseIf m_subtotal_terminal = DbRow.Value(SQL_COLUMN_TERMINAL).ToString() Then
          _reset_subtotal_values = False
        Else
          Call PrintSubTotalRow(ENUM_SUBTOTAL_SHOW_TYPE.SHOW_SUBTOTAL_BY_TERMINAL)
          m_subtotal_terminal = DbRow.Value(SQL_COLUMN_TERMINAL).ToString()
        End If
      End If
      Call UpdateSubtotals(DbRow, _reset_subtotal_values)
    End If
    Return _show_normal_row
  End Function


#Region " GUI Reports "

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid

      Call MyBase.GUI_ReportParams(ExcelData)

      ExcelData.SetColumnFormat(EXCEL_COLUMN_COLLECTION_EMPLOYEE, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)

    End With

  End Sub

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.FilterHeaderWidth(1) = 2000
    PrintData.FilterHeaderWidth(2) = 2000
    PrintData.FilterHeaderWidth(3) = 2000

    If opt_insertion_datetime.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5739) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5737), m_date_from)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5739) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5738), m_date_to)
    ElseIf opt_collection_datetime.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5740) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5737), m_date_from)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5740) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5738), m_date_to)
    ElseIf opt_cage_session_datetime.Checked Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5741) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5737), m_date_from)
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5741) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(5738), m_date_to)
    End If


    'status
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2231), m_status)

    'collection user
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4577), m_employee_id)

    'terminals
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)

    'stackers
    If TITO.Utils.IsTitoMode() Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2199), m_stackers)
    End If

    If m_cashier_session_id > 0 Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(629), m_cashier_session_name)
    End If

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237),
                        IIf(m_show_terminal_location, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))

    PrintData.SetFilter(GLB_NLS_GUI_INVOICING.GetString(369), m_options)
    If Not String.IsNullOrEmpty(m_options) Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5721), IIf(m_show_details, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))
    End If


  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""
    m_stacker_id = ""
    m_employee_id = ""
    m_terminals = ""
    m_status = ""
    m_options = ""
    m_show_details = False
    m_insert_last_row = False

    ' Date 
    If Me.ds_from_to.FromDateSelected Then
      m_date_from = GUI_FormatDate(ds_from_to.FromDate.AddHours(ds_from_to.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.ds_from_to.ToDateSelected Then
      m_date_to = GUI_FormatDate(ds_from_to.ToDate.AddHours(ds_from_to.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Employee
    If Me.opt_one_employee.Checked Then
      m_employee_id = Me.cmb_employee.TextValue
    Else
      m_employee_id = Me.opt_all_employee.Text
    End If

    ' collection balance status

    If Me.chk_inserted_validated.Checked Then
      m_status = Me.chk_inserted_validated.Text
    End If

    If Me.chk_open_with_activity.Checked Then
      If Not String.IsNullOrEmpty(m_status) Then
        m_status += ", "
      End If
      m_status += Me.chk_open_with_activity.Text
    End If

    If Me.chk_pending_collection.Checked Then
      If Not String.IsNullOrEmpty(m_status) Then
        m_status += ", "
      End If
      m_status += Me.chk_pending_collection.Text
    End If

    If Me.chk_collection_status_incorrect.Checked Then
      If Not String.IsNullOrEmpty(m_status) Then
        m_status += ", "
      End If
      m_status += Me.chk_collection_status_incorrect.Text
    End If

    If Me.chk_collection_status_correct.Checked Then
      If Not String.IsNullOrEmpty(m_status) Then
        m_status += ", "
      End If
      m_status += Me.chk_collection_status_correct.Text
    End If

    ' Providers - Terminals
    m_terminals = Me.uc_provider.GetTerminalReportText()

    m_show_terminal_location = Me.chk_terminal_location.Checked

    ' Options
    If Me.chk_group_by.Checked Then
      m_options = chk_group_by.Text
      If opt_group_by_day.Checked Then
        m_options = m_options & " " & opt_group_by_day.Text
      ElseIf opt_provider.Checked Then
        m_options = m_options & " " & opt_provider.Text
      Else
        m_options = m_options & " " & opt_terminal.Text
      End If
      m_show_details = Me.chk_show_detail.Checked

    End If

    Me.Grid.Column(GRID_COLUMN_CAGE_SESSION_DATE).Fixed = False
    Me.Grid.Column(GRID_COLUMN_CAGE_SESSION_DATE).Width = IIf(opt_cage_session_datetime.Checked Or opt_collection_datetime.Checked, GRID_WIDTH_CAGE_SESSION_DATE, 0)

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region 'Overrides

#Region " Private Functions and Methods "

  ' PURPOSE : Define layout of all Main Grid Columns
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Private Sub GUI_StyleSheet()
    Dim _terminal_columns As List(Of ColumnSettings)

    Call GridColumnReIndex()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = True

      .Counter(COUNTER_OPEN).Visible = True
      .Counter(COUNTER_OPEN).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
      .Counter(COUNTER_OPEN).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      .Counter(COUNTER_OPEN_WITH_ACTIVITY).Visible = True
      .Counter(COUNTER_OPEN_WITH_ACTIVITY).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_03)
      .Counter(COUNTER_OPEN_WITH_ACTIVITY).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      .Counter(COUNTER_PENDING).Visible = True
      .Counter(COUNTER_PENDING).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLUE_01)
      .Counter(COUNTER_PENDING).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      .Counter(COUNTER_BALANCED).Visible = True
      .Counter(COUNTER_BALANCED).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_GREEN_00)
      .Counter(COUNTER_BALANCED).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_BLACK_00)
      .Counter(COUNTER_IMBALANCE).Visible = True
      .Counter(COUNTER_IMBALANCE).BackColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_RED_00)
      .Counter(COUNTER_IMBALANCE).ForeColor = GetColor(ControlColor.ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

      ' SELECT
      .Column(GRID_COLUMN_SELECT).Header(0).Text = ""
      .Column(GRID_COLUMN_SELECT).Header(1).Text = ""
      .Column(GRID_COLUMN_SELECT).Width = GRID_COLUMN_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False
      .Column(GRID_COLUMN_SELECT).HighLightWhenSelected = False

      ' STACKER COLLECTION START DATE
      .Column(GRID_COLUMN_STACKER_COLLECTION_START_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4612)
      .Column(GRID_COLUMN_STACKER_COLLECTION_START_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2251)
      .Column(GRID_COLUMN_STACKER_COLLECTION_START_DATE).Width = GRID_WIDTH_COLLECTION_DATE
      .Column(GRID_COLUMN_STACKER_COLLECTION_START_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' STACKER EXTRACTION DATE
      .Column(GRID_COLUMN_STACKER_EXTRACTION_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4612)
      .Column(GRID_COLUMN_STACKER_EXTRACTION_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2263)
      .Column(GRID_COLUMN_STACKER_EXTRACTION_DATE).Width = GRID_WIDTH_STACKER_EXTRACTION_DATE
      .Column(GRID_COLUMN_STACKER_EXTRACTION_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' STACKER COLLECTION DATE
      .Column(GRID_COLUMN_STACKER_COLLECTION_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4612)
      .Column(GRID_COLUMN_STACKER_COLLECTION_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2206)
      .Column(GRID_COLUMN_STACKER_COLLECTION_DATE).Width = GRID_WIDTH_CAGE_SESSION_DATE
      .Column(GRID_COLUMN_STACKER_COLLECTION_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' CAGE SESSION DATE
      .Column(GRID_COLUMN_CAGE_SESSION_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4612)
      .Column(GRID_COLUMN_CAGE_SESSION_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5715)
      .Column(GRID_COLUMN_CAGE_SESSION_DATE).Width = IIf(opt_cage_session_datetime.Checked, GRID_WIDTH_STACKER_EXTRACTION_DATE, 0)
      .Column(GRID_COLUMN_CAGE_SESSION_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' COLLECTION EMPLOYEE
      .Column(GRID_COLUMN_COLLECTION_EMPLOYEE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4612)
      .Column(GRID_COLUMN_COLLECTION_EMPLOYEE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2253)
      .Column(GRID_COLUMN_COLLECTION_EMPLOYEE).Width = GRID_WIDTH_COLLECTION_EMPLOYE
      .Column(GRID_COLUMN_COLLECTION_EMPLOYEE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' BALANCE STATUS
      .Column(GRID_COLUMN_BALANCE_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4612)
      .Column(GRID_COLUMN_BALANCE_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2231)
      .Column(GRID_COLUMN_BALANCE_STATUS).Width = GRID_WIDTH_BALANCE_STATE
      .Column(GRID_COLUMN_BALANCE_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' STACKER ID
      .Column(GRID_COLUMN_STACKER_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4612)
      .Column(GRID_COLUMN_STACKER_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2252)
      .Column(GRID_COLUMN_STACKER_ID).Width = 1
      .Column(GRID_COLUMN_STACKER_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      If TITO.Utils.IsTitoMode Then
        .Column(GRID_COLUMN_STACKER_ID).Width = GRID_WIDTH_STACKER_ID
      End If

      '  Terminal Report 
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1097)
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
        If _terminal_columns(_idx).Column = ReportColumn.Provider Then
          GRID_COLUMN_TERMINAL_PROVIDER = GRID_INIT_TERMINAL_DATA + _idx
        End If
        If _terminal_columns(_idx).Column = ReportColumn.Terminal Then
          GRID_COLUMN_TERMINAL = GRID_INIT_TERMINAL_DATA + _idx
        End If
      Next

      ' TERMINAL TYPE
      .Column(GRID_COLUMN_TERMINAL_TYPE).Width = GRID_WIDTH_COLUMN_TERMINAL_TYPE

      ' CASHIER SESSION ID
      .Column(GRID_COLUMN_MC_CASHIER_SESSION_ID).Width = GRID_WIDTH_COLUMN_TERMINAL_TYPE

      ' BILLS EXPECTED AMOUNT
      .Column(GRID_COLUMN_BILLS_EXPECTED_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2294)
      .Column(GRID_COLUMN_BILLS_EXPECTED_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(645)
      .Column(GRID_COLUMN_BILLS_EXPECTED_AMOUNT).Width = IIf(m_show_expected_data, GRID_WIDTH_BILLS_TOTAL_AMOUNT, 0)
      .Column(GRID_COLUMN_BILLS_EXPECTED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' BILLS COLLECTED AMOUNT
      .Column(GRID_COLUMN_BILLS_COLLECTED_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2294)
      .Column(GRID_COLUMN_BILLS_COLLECTED_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2264)
      .Column(GRID_COLUMN_BILLS_COLLECTED_AMOUNT).Width = GRID_WIDTH_BILLS_TOTAL_AMOUNT
      .Column(GRID_COLUMN_BILLS_COLLECTED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'BILLS DIFFERENCE
      .Column(GRID_COLUMN_BILLS_DIFERENCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2294)
      .Column(GRID_COLUMN_BILLS_DIFERENCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2610)
      .Column(GRID_COLUMN_BILLS_DIFERENCE).Width = IIf(m_show_expected_data, GRID_WIDTH_BILLS_TOTAL_AMOUNT, 0)
      .Column(GRID_COLUMN_BILLS_DIFERENCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'BILLS PERCENT
      .Column(GRID_COLUMN_BILLS_PERCENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2294)
      .Column(GRID_COLUMN_BILLS_PERCENT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5045)
      .Column(GRID_COLUMN_BILLS_PERCENT).Width = IIf(m_show_expected_data, GRID_WIDTH_NUMBER_OF_BILLS, 0)
      .Column(GRID_COLUMN_BILLS_PERCENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' COINS EXPECTED AMOUNT
      .Column(GRID_COLUMN_COINS_EXPECTED_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5558)
      .Column(GRID_COLUMN_COINS_EXPECTED_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(645)
      .Column(GRID_COLUMN_COINS_EXPECTED_AMOUNT).Width = 1
      .Column(GRID_COLUMN_COINS_EXPECTED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      If TITO.Utils.IsTitoMode() Then
        .Column(GRID_COLUMN_COINS_EXPECTED_AMOUNT).Width = IIf(m_show_expected_data, GRID_WIDTH_COINS_TOTAL_AMOUNT, 0)
      End If

      ' COINS COLLECTED AMOUNT
      .Column(GRID_COLUMN_COINS_COLLECTED_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5558)
      .Column(GRID_COLUMN_COINS_COLLECTED_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2264)
      .Column(GRID_COLUMN_COINS_COLLECTED_AMOUNT).Width = 1
      .Column(GRID_COLUMN_COINS_COLLECTED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      If TITO.Utils.IsTitoMode() Then
        .Column(GRID_COLUMN_COINS_COLLECTED_AMOUNT).Width = GRID_WIDTH_COINS_TOTAL_AMOUNT
      End If

      ' COINS DIFFERENCE
      .Column(GRID_COLUMN_COINS_DIFERENCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5558)
      .Column(GRID_COLUMN_COINS_DIFERENCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2610)
      .Column(GRID_COLUMN_COINS_DIFERENCE).Width = 1
      .Column(GRID_COLUMN_COINS_DIFERENCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      If TITO.Utils.IsTitoMode() Then
        .Column(GRID_COLUMN_COINS_DIFERENCE).Width = IIf(m_show_expected_data, GRID_WIDTH_COINS_TOTAL_AMOUNT, 0)
      End If

      ' COINS PERCENT
      .Column(GRID_COLUMN_COINS_PERCENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5558)
      .Column(GRID_COLUMN_COINS_PERCENT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5045)
      .Column(GRID_COLUMN_COINS_PERCENT).Width = 1
      .Column(GRID_COLUMN_COINS_PERCENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      If TITO.Utils.IsTitoMode() Then
        .Column(GRID_COLUMN_COINS_PERCENT).Width = IIf(m_show_expected_data, GRID_WIDTH_NUMBER_OF_COINS, 0)
      End If

      ' TICKETS EXPECTED AMOUNT
      .Column(GRID_COLUMN_TICKETS_EXPECTED_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2295)
      .Column(GRID_COLUMN_TICKETS_EXPECTED_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(645)
      .Column(GRID_COLUMN_TICKETS_EXPECTED_AMOUNT).Width = 1
      .Column(GRID_COLUMN_TICKETS_EXPECTED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      If TITO.Utils.IsTitoMode() Then
        .Column(GRID_COLUMN_TICKETS_EXPECTED_AMOUNT).Width = IIf(m_show_expected_data, GRID_WIDTH_TICKETS_TOTAL_AMOUNT, 0)
      End If

      ' TICKETS COLLECTED AMOUNT
      .Column(GRID_COLUMN_TICKETS_COLLECTED_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2295)
      .Column(GRID_COLUMN_TICKETS_COLLECTED_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2264)
      .Column(GRID_COLUMN_TICKETS_COLLECTED_AMOUNT).Width = 1
      .Column(GRID_COLUMN_TICKETS_COLLECTED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      If TITO.Utils.IsTitoMode() Then
        .Column(GRID_COLUMN_TICKETS_COLLECTED_AMOUNT).Width = GRID_WIDTH_TICKETS_TOTAL_AMOUNT
      End If

      'TICKETS DIFFERENCE
      .Column(GRID_COLUMN_TICKETS_DIFERENCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2295)
      .Column(GRID_COLUMN_TICKETS_DIFERENCE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2610)
      .Column(GRID_COLUMN_TICKETS_DIFERENCE).Width = 1
      .Column(GRID_COLUMN_TICKETS_DIFERENCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      If TITO.Utils.IsTitoMode() Then
        .Column(GRID_COLUMN_TICKETS_DIFERENCE).Width = IIf(m_show_expected_data, GRID_WIDTH_TICKETS_TOTAL_AMOUNT, 0)
      End If

      'TICKETS PERCENT
      .Column(GRID_COLUMN_TICKETS_PERCENT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2295)
      .Column(GRID_COLUMN_TICKETS_PERCENT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5045)
      .Column(GRID_COLUMN_TICKETS_PERCENT).Width = 1
      .Column(GRID_COLUMN_TICKETS_PERCENT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      If TITO.Utils.IsTitoMode() Then
        .Column(GRID_COLUMN_TICKETS_PERCENT).Width = IIf(m_show_expected_data, GRID_WIDTH_NUMBER_OF_TICKETS, 0)
      End If

      ' STACKER COLLECTION ID
#If DEBUG Then
      .Column(GRID_COLUMN_STACKER_COLLECTION_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4612)
      .Column(GRID_COLUMN_STACKER_COLLECTION_ID).Header(1).Text = "Collection Id"
      .Column(GRID_COLUMN_STACKER_COLLECTION_ID).Width = 1300
      .Column(GRID_COLUMN_STACKER_COLLECTION_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
#Else
      .Column(GRID_COLUMN_STACKER_COLLECTION_ID).Width = SQL_COLUMN_STACKER_COLLECTION_ID
#End If

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Call SetDefaultDate()

    '' Dates
    'ds_from_to.FromDateSelected = False
    'ds_from_to.ToDateSelected = False
    'ds_from_to.Enabled = False

    '' Terminal
    Call Me.uc_provider.SetDefaultValues()

    ''employee
    Me.opt_all_employee.Checked = True
    Me.cmb_employee.Enabled = False

    'Status
    Me.chk_inserted_validated.Checked = False
    Me.chk_open_with_activity.Checked = True
    Me.chk_pending_collection.Checked = False
    Me.chk_collection_status_correct.Checked = False
    Me.chk_collection_status_incorrect.Checked = False
    Me.chk_inserted_validated.Enabled = True
    Me.chk_pending_collection.Enabled = True
    Me.chk_open_with_activity.Enabled = True

    '' Balance
    Me.chk_inserted_validated.Checked = False
    Me.chk_pending_collection.Checked = False
    Me.chk_collection_status_correct.Checked = False
    Me.chk_collection_status_incorrect.Checked = False

    Me.chk_terminal_location.Checked = False

    Call UncheckAllStackers()

    Me.chk_group_by.Checked = False
    Me.opt_group_by_day.Checked = True
    Me.chk_show_detail.Checked = True

  End Sub ' SetDefaultValues

  ' PURPOSE: Sets the defaul date values to the daily_session_selector control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultDate()

    Dim _today_opening As Date

    _today_opening = WSI.Common.Misc.TodayOpening()

    ds_from_to.FromDate = _today_opening.AddDays(-1)
    ds_from_to.FromDateSelected = True
    ds_from_to.ToDate = _today_opening
    ds_from_to.ToDateSelected = True
    ds_from_to.ClosingTime = _today_opening.Hour

  End Sub ' SetDefaultDate

  Private Function GetPercentFromValues(ByVal ExpectedAmount As Decimal, ByVal CollectedAmount As Decimal, ByVal Status As Int64) As String
    Dim _result As Decimal

    _result = 0

    If Status <> TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE And Status <> TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED Then

      Return String.Empty

    ElseIf ExpectedAmount = 0 Then

      If CollectedAmount > 0 Then

        Return GUI_FormatNumber(100, 2) & "%"
      End If

      If CollectedAmount = 0 Then

        Return GUI_FormatNumber(0, 2) & "%"
      End If

    End If

    _result = Decimal.Subtract(CollectedAmount, ExpectedAmount)

    Return GUI_FormatNumber(Decimal.Divide(_result, ExpectedAmount) * 100, 2) & "%"
  End Function

  ' PURPOSE: Get the sql 'where' statement
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - an string with the 'where' statement
  Private Function GetSqlWhere() As String

    Dim _str_where As String
    Dim _account_filter As String
    Dim _terminal_filter As String
    Dim _state_filter As String
    Dim _stacker_id_filter As String
    Dim _stackers_list As String

    _str_where = ""
    _account_filter = ""
    _terminal_filter = ""
    _state_filter = ""
    _stacker_id_filter = ""

    'Filter Date
    If opt_insertion_datetime.Checked Then
      _str_where = ds_from_to.GetSqlFilterCondition("MC_DATETIME")
      If _str_where.Length > 0 Then
        _str_where = " AND (" & _str_where & ") "
      End If
    ElseIf opt_collection_datetime.Checked Then
      _str_where = ds_from_to.GetSqlFilterCondition("MC_COLLECTION_DATETIME")
      If _str_where.Length > 0 Then
        _str_where = " AND (" & _str_where & " OR MC_COLLECTION_DATETIME IS NULL) "
      End If
    ElseIf opt_cage_session_datetime.Checked Then
      _str_where = ds_from_to.GetSqlFilterCondition("CGS_WORKING_DAY")
      If _str_where.Length > 0 Then
        _str_where = " AND (" & _str_where & " ) "
      End If
    End If
    'If _str_where.Length > 0 Then
    '  _str_where = " AND (" & _str_where & " OR MC_DATETIME IS NULL) " ' TODO: Don't have any sense
    'End If

    'Filter Employee
    If Me.opt_one_employee.Checked Then
      _account_filter = " GU_USER_ID = " & Me.cmb_employee.Value
    End If

    If _account_filter.Length > 0 Then
      _str_where = _str_where & " AND (" & _account_filter & ") "
    End If

    ' Filter Terminals
    If Me.uc_provider.OneTerminalChecked Then
      _terminal_filter = " TE_TERMINAL_ID IN " & Me.uc_provider.GetProviderIdListSelected()
    ElseIf Me.uc_provider.SeveralTerminalsChecked Then
      _terminal_filter = " TE_TERMINAL_ID IN " & Me.uc_provider.GetProviderIdListSelected()
    Else 'If Me.uc_provider.AllSelectedChecked Then
      _terminal_filter = " TE_TERMINAL_ID IN " & Me.uc_provider.GetProviderIdListSelected() '_terminal_filter = ""
    End If

    If _terminal_filter.Length > 0 Then
      _str_where = _str_where & " AND (" & _terminal_filter & ") "
    End If

    'Floor ID filter
    If Not String.IsNullOrEmpty(m_floor_id) AndAlso Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING Then
      _str_where = _str_where & " AND TE_FLOOR_ID = '" & m_floor_id & "' "
    End If

    ' Stacker filters
    If TITO.Utils.IsTitoMode() Then
      _stackers_list = ""
      If String.IsNullOrEmpty(m_floor_id) Then
        _stackers_list = GetStackersSelected(GRID_STACKERS_COLUMN_ID)
      End If

      If _stackers_list.Length > 0 Then
        _str_where = _str_where & " AND " & _stackers_list
      End If
    End If

    'State filters
    If Me.chk_inserted_validated.Checked Then
      If Not String.IsNullOrEmpty(_state_filter) Then
        _state_filter &= " OR "
      End If
      _state_filter &= " ( MC_STATUS = " & TITO_MONEY_COLLECTION_STATUS.OPEN & " AND ( ISNULL(MC_EXPECTED_BILL_COUNT, 0) + ISNULL(MC_EXPECTED_COIN_AMOUNT,0) + ISNULL(MC_EXPECTED_TICKET_COUNT, 0) = 0  ) ) "
    End If

    If Me.chk_open_with_activity.Checked Then
      If Not String.IsNullOrEmpty(_state_filter) Then
        _state_filter &= " OR "
      End If
      _state_filter &= " ( MC_STATUS = " & TITO_MONEY_COLLECTION_STATUS.OPEN & " AND ( ISNULL(MC_EXPECTED_BILL_COUNT, 0) + ISNULL(MC_EXPECTED_COIN_AMOUNT,0) + ISNULL(MC_EXPECTED_TICKET_COUNT, 0) > 0  ) ) "
    End If

    If Me.chk_pending_collection.Checked Then
      If Not String.IsNullOrEmpty(_state_filter) Then
        _state_filter &= " OR "
      End If
      _state_filter &= " MC_STATUS = " & TITO_MONEY_COLLECTION_STATUS.PENDING
    End If

    If Me.chk_collection_status_incorrect.Checked Then
      If Not String.IsNullOrEmpty(_state_filter) Then
        _state_filter &= " OR "
      End If
      _state_filter &= " MC_STATUS = " & TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE
    End If

    If Me.chk_collection_status_correct.Checked Then
      If Not String.IsNullOrEmpty(_state_filter) Then
        _state_filter += " OR "
      End If
      _state_filter += " MC_STATUS = " & TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED
    End If

    If _state_filter.Length = 0 AndAlso Not opt_insertion_datetime.Checked Then
      _state_filter += " MC_STATUS IN( " & TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED & "," & TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE & ")"
    End If

    If _state_filter.Length > 0 Then
      _str_where &= " AND (" & _state_filter & ") "
    End If

    ' cashier session filter
    If m_cashier_session_id > 0 Then
      _str_where &= " AND (MC_CASHIER_SESSION_ID = " & m_cashier_session_id & ") "
    End If

    If Not String.IsNullOrEmpty(_str_where.ToString()) Then
      _str_where = _str_where.Remove(0, 4) ' Remove first " AND"
      _str_where = _str_where.Insert(0, " WHERE")
    End If

    Return _str_where

  End Function  'GetSqlWhere

  Private Sub UncheckAllStackers()

    Dim idx_rows As Integer

    For idx_rows = 0 To dg_stackers.NumRows - 1
      dg_stackers.Cell(idx_rows, GRID_STACKERS_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_UNCHECKED
    Next

  End Sub

  Private Sub GUI_GetSqlQueryStackers()

    Dim _sb As StringBuilder
    Dim _adapter As SqlDataAdapter
    Dim _table As DataTable

    _adapter = New SqlDataAdapter()
    _table = New DataTable()
    _sb = New StringBuilder()

    Try
      Using _db_trx As DB_TRX = New DB_TRX()
        _sb.AppendLine("  SELECT   ST_STACKER_ID   ")
        _sb.AppendLine("    FROM   STACKERS        ")
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _adapter = New SqlDataAdapter(_cmd)
          _adapter.Fill(_table)
          Call GUI_SetupRowsStackersGrid(_table)
        End Using
      End Using
    Catch ex As Exception
    End Try

  End Sub

  ' PURPOSE: Fill the grid with the stackers validation number
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_SetupRowsStackersGrid(ByVal Table As DataTable)

    Dim _actual_row As Int32

    dg_stackers.AddRow()
    Me.dg_stackers.Cell(GRID_STACKER_FIRST_ROW_WITHOUT_STACKER, GRID_STACKERS_COLUMN_ID).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4988)

    _actual_row = 1

    For Each r As DataRow In Table.Rows
      dg_stackers.AddRow()
      Me.dg_stackers.Cell(_actual_row, GRID_STACKERS_COLUMN_ID).Value = r(SQL_COLUMN_STACKER_ID)

      _actual_row += 1
    Next

  End Sub ' GUI_SetupRowCashierGrid

  ' PURPOSE: Define all Grid Account Alias columns
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheetStackers()

    With Me.dg_stackers
      Call .Init(GRID_STACKERS_COLUMNS, GRID_STACKERS_HEADER_ROWS, True)
      .Counter(0).Visible = False
      .Sortable = False

      ' GRID_COL_CHECKBOX
      .Column(GRID_STACKERS_COLUMN_CHECKED).Header(1).Text = ""
      .Column(GRID_STACKERS_COLUMN_CHECKED).WidthFixed = GRID_STACKERS_WIDTH_COLUMN_CHECKED
      .Column(GRID_STACKERS_COLUMN_CHECKED).Fixed = True
      .Column(GRID_STACKERS_COLUMN_CHECKED).Editable = True
      .Column(GRID_STACKERS_COLUMN_CHECKED).EditionControl.Type = uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE.CONTROL_TYPE_CHECK_BOX

      ' GRID_COL_DESCRIPTION
      .Column(GRID_STACKERS_COLUMN_ID).Header(1).Text = ""
      .Column(GRID_STACKERS_COLUMN_ID).WidthFixed = GRID_STACKERS_WIDTH_COLUMN_DESCRIPTION
      .Column(GRID_STACKERS_COLUMN_ID).Fixed = True
      .Column(GRID_STACKERS_COLUMN_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

    End With

  End Sub

  ' PURPOSE: Enable button in selected row
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_RowSelectedEvent(ByVal SelectedRow As Integer)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = Not String.IsNullOrEmpty(Me.Grid.Cell(SelectedRow, GRID_COLUMN_STACKER_COLLECTION_ID).Value)

  End Sub

  ' PURPOSE: get the cashiers selected
  '
  '  PARAMS:
  '     - INPUT:
  '           - ColumnNeeded : It could be the Id column or alias column
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - it could be a list of Ids or a list of alias
  Private Function GetStackersSelected(ByVal ColumnNeeded As Int16) As String

    Dim _idx_row As Integer
    Dim _all As Boolean
    Dim _at_least_one As Boolean
    Dim _stackers_list As String

    m_stackers = ""
    _stackers_list = ""
    _all = True
    _at_least_one = False

    For _idx_row = 1 To Me.dg_stackers.NumRows - 1
      If dg_stackers.Cell(_idx_row, GRID_STACKERS_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
        If _stackers_list.Length = 0 Then
          _stackers_list = dg_stackers.Cell(_idx_row, ColumnNeeded).Value
        Else
          _stackers_list = _stackers_list & ", " & dg_stackers.Cell(_idx_row, ColumnNeeded).Value
        End If
        _at_least_one = True
      Else
        _all = False
      End If

    Next

    ' the option collection without stacker is selected
    If dg_stackers.Cell(GRID_STACKER_FIRST_ROW_WITHOUT_STACKER, GRID_STACKERS_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED Then
      If _stackers_list.Length = 0 Then
        _stackers_list = " MC_STACKER_ID IS NULL "
        _at_least_one = True
      Else
        _stackers_list = "( MC_STACKER_ID IN (" & _stackers_list & ") OR " & " MC_STACKER_ID IS NULL )"
      End If
    ElseIf _stackers_list.Length > 0 Then
      _stackers_list = "MC_STACKER_ID IN (" & _stackers_list & ")"
      _all = False
    End If

    If _all = True Then
      m_stackers = GLB_NLS_GUI_ALARMS.GetString(277)
      _stackers_list = ""
    End If

    If _all = False AndAlso _at_least_one = True Then
      m_stackers = GLB_NLS_GUI_CONTROLS.GetString(291)
    End If

    Return _stackers_list

  End Function ' GetCashiersSelected


  ' PURPOSE: Return a string representation of TITO_MONEY_COLLECTION_STATUS
  '
  ' PARAMS:
  '   - INPUT:
  '     - Model: TITO_MONEY_COLLECTION_STATUS
  '
  ' RETURNS:
  '     - String
  '
  Public Function PrintCollectionStatus(ByVal Status As TITO_MONEY_COLLECTION_STATUS) As Color
    Dim _color_status As Color

    _color_status = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

    Select Case Status
      Case TITO_MONEY_COLLECTION_STATUS.NONE
        _color_status = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)

      Case TITO_MONEY_COLLECTION_STATUS.OPEN
        _color_status = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
        Me.Grid.Counter(COUNTER_OPEN).Value += 1

      Case TITO_MONEY_COLLECTION_STATUS.PENDING
        _color_status = GetColor(ENUM_GUI_COLOR.GUI_COLOR_BLUE_01)
        Me.Grid.Counter(COUNTER_PENDING).Value += 1

      Case TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE
        _color_status = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
        Me.Grid.Counter(COUNTER_IMBALANCE).Value += 1

      Case TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED
        _color_status = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREEN_00)
        Me.Grid.Counter(COUNTER_BALANCED).Value += 1

      Case 4 'DRV: Status Active with activity (Only used in frm_tito_sacker_collections_sel
        _color_status = GetColor(ENUM_GUI_COLOR.GUI_COLOR_GREY_03)
        Me.Grid.Counter(COUNTER_OPEN_WITH_ACTIVITY).Value += 1

      Case Else
        _color_status = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
    End Select

    Return _color_status

  End Function

  Private Sub GridColumnReIndex()
    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_COLUMN_SELECT = 0
    GRID_COLUMN_STACKER_COLLECTION_START_DATE = 1
    GRID_COLUMN_STACKER_EXTRACTION_DATE = 2
    GRID_COLUMN_STACKER_COLLECTION_DATE = 3
    GRID_COLUMN_CAGE_SESSION_DATE = 4
    GRID_COLUMN_COLLECTION_EMPLOYEE = 5
    GRID_COLUMN_BALANCE_STATUS = 6
    GRID_COLUMN_STACKER_ID = 7
    GRID_COLUMN_STACKER_COLLECTION_ID = 8
    GRID_INIT_TERMINAL_DATA = 9
    GRID_COLUMN_TERMINAL_PROVIDER = GRID_INIT_TERMINAL_DATA
    GRID_COLUMN_TERMINAL = GRID_INIT_TERMINAL_DATA + 1
    GRID_COLUMN_BILLS_COLLECTED_AMOUNT = TERMINAL_DATA_COLUMNS + 9
    GRID_COLUMN_BILLS_EXPECTED_AMOUNT = TERMINAL_DATA_COLUMNS + 10
    GRID_COLUMN_BILLS_DIFERENCE = TERMINAL_DATA_COLUMNS + 11
    GRID_COLUMN_BILLS_PERCENT = TERMINAL_DATA_COLUMNS + 12
    GRID_COLUMN_COINS_COLLECTED_AMOUNT = TERMINAL_DATA_COLUMNS + 13
    GRID_COLUMN_COINS_EXPECTED_AMOUNT = TERMINAL_DATA_COLUMNS + 14
    GRID_COLUMN_COINS_DIFERENCE = TERMINAL_DATA_COLUMNS + 15
    GRID_COLUMN_COINS_PERCENT = TERMINAL_DATA_COLUMNS + 16
    GRID_COLUMN_TICKETS_COLLECTED_AMOUNT = TERMINAL_DATA_COLUMNS + 17
    GRID_COLUMN_TICKETS_EXPECTED_AMOUNT = TERMINAL_DATA_COLUMNS + 18
    GRID_COLUMN_TICKETS_DIFERENCE = TERMINAL_DATA_COLUMNS + 19
    GRID_COLUMN_TICKETS_PERCENT = TERMINAL_DATA_COLUMNS + 20
    GRID_COLUMN_TERMINAL_TYPE = TERMINAL_DATA_COLUMNS + 21
    GRID_COLUMN_MC_CASHIER_SESSION_ID = TERMINAL_DATA_COLUMNS + 22

    GRID_COLUMNS = TERMINAL_DATA_COLUMNS + 23
  End Sub

  Private Sub PrintSubTotalRow(ByVal ShowBy As ENUM_SUBTOTAL_SHOW_TYPE, Optional ByVal IndexShowDate As Int32 = 0)
    Dim _row As Int32

    Me.Grid.AddRow()
    _row = Me.Grid.NumRows - 1
    Select Case ShowBy
      Case ENUM_SUBTOTAL_SHOW_TYPE.SHOW_SUBTOTAL_BY_PROVIDER
        If chk_show_detail.Checked Then
          Me.Grid.Cell(_row, GRID_COLUMN_TERMINAL).Value = GLB_NLS_GUI_INVOICING.GetString(375)
          Me.Grid.Cell(_row, GRID_COLUMN_TERMINAL_PROVIDER).Value = m_subtotal_provider
        Else
          Me.Grid.Cell(_row, GRID_COLUMN_STACKER_ID).Value = GLB_NLS_GUI_INVOICING.GetString(375)
          Me.Grid.Cell(_row, GRID_COLUMN_TERMINAL_PROVIDER).Value = m_subtotal_provider
        End If
      Case ENUM_SUBTOTAL_SHOW_TYPE.SHOW_SUBTOTAL_BY_TERMINAL
        If chk_show_detail.Checked Then
          Me.Grid.Cell(_row, GRID_COLUMN_TERMINAL_PROVIDER).Value = GLB_NLS_GUI_INVOICING.GetString(375)
          Me.Grid.Cell(_row, GRID_COLUMN_TERMINAL).Value = m_subtotal_terminal
        Else
          Me.Grid.Cell(_row, GRID_COLUMN_STACKER_ID).Value = GLB_NLS_GUI_INVOICING.GetString(375)
          Me.Grid.Cell(_row, GRID_COLUMN_TERMINAL).Value = m_subtotal_terminal
        End If
      Case ENUM_SUBTOTAL_SHOW_TYPE.SHOW_SUBTOTAL_BY_DATE
        Me.Grid.Cell(_row, GRID_COLUMN_TERMINAL).Value = GLB_NLS_GUI_INVOICING.GetString(375)
        Me.Grid.Cell(_row, IndexShowDate).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5733) & ": " & m_subtotal_date_since.Date
        ' Me.Grid.Cell(_row, GRID_COLUMN_STACKER_EXTRACTION_DATE).Value = "Hasta: " & m_subtotal_date_until


    End Select


    Me.Grid.Cell(_row, GRID_COLUMN_BILLS_EXPECTED_AMOUNT).Value = GUI_FormatCurrency(m_subtotal_bill_expected_amount, 2)
    Me.Grid.Cell(_row, GRID_COLUMN_BILLS_COLLECTED_AMOUNT).Value = GUI_FormatCurrency(m_subtotal_bill_collected_amount, 2)
    Me.Grid.Cell(_row, GRID_COLUMN_BILLS_DIFERENCE).Value = GUI_FormatCurrency(m_subtotal_bill_collected_amount - m_subtotal_bill_expected_amount, 2)
    Me.Grid.Cell(_row, GRID_COLUMN_BILLS_PERCENT).Value = GetPercentFromValues(m_subtotal_bill_expected_amount, m_subtotal_bill_collected_amount, 2)
    Me.Grid.Cell(_row, GRID_COLUMN_COINS_EXPECTED_AMOUNT).Value = GUI_FormatCurrency(m_subtotal_coin_expected_amount, 2)
    Me.Grid.Cell(_row, GRID_COLUMN_COINS_COLLECTED_AMOUNT).Value = GUI_FormatCurrency(m_subtotal_coin_collected_amount, 2)
    Me.Grid.Cell(_row, GRID_COLUMN_COINS_DIFERENCE).Value = GUI_FormatCurrency(m_subtotal_coin_collected_amount - m_subtotal_coin_expected_amount, 2)
    Me.Grid.Cell(_row, GRID_COLUMN_COINS_PERCENT).Value = GetPercentFromValues(m_subtotal_coin_expected_amount, m_subtotal_coin_collected_amount, 2)
    Me.Grid.Cell(_row, GRID_COLUMN_TICKETS_EXPECTED_AMOUNT).Value = GUI_FormatCurrency(m_subtotal_ticket_expected_amount, 2)
    Me.Grid.Cell(_row, GRID_COLUMN_TICKETS_COLLECTED_AMOUNT).Value = GUI_FormatCurrency(m_subtotal_ticket_collected_amount, 2)
    Me.Grid.Cell(_row, GRID_COLUMN_TICKETS_DIFERENCE).Value = GUI_FormatCurrency(m_subtotal_ticket_collected_amount - m_subtotal_ticket_expected_amount, 2)
    Me.Grid.Cell(_row, GRID_COLUMN_TICKETS_PERCENT).Value = GetPercentFromValues(m_subtotal_ticket_expected_amount, m_subtotal_ticket_collected_amount, 2)

    Me.Grid.Row(_row).BackColor() = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    'if details arent not showen, the total cant be calculate because , the total is calculate in method GUI_SetUpRow
    If chk_group_by.Checked AndAlso Not chk_show_detail.Checked Then
      m_total_bill_expected_amount = m_total_bill_expected_amount + m_subtotal_bill_expected_amount
      m_total_bill_collected_amount = m_total_bill_collected_amount + m_subtotal_bill_collected_amount
      m_total_coin_expected_amount = m_total_coin_expected_amount + m_subtotal_coin_expected_amount
      m_total_coin_collected_amount = m_total_coin_collected_amount + m_subtotal_coin_collected_amount
      m_total_ticket_expected_amount = m_total_ticket_expected_amount + m_subtotal_ticket_expected_amount
      m_total_ticket_collected_amount = m_total_ticket_collected_amount + m_subtotal_ticket_collected_amount
    End If

  End Sub

  Private Sub UpdateSubtotals(ByVal DbRow As CLASS_DB_ROW, ByVal ResetValues As Boolean)
    Dim _subtotal_bill_expected_amount As Decimal
    Dim _subtotal_bill_collected_amount As Decimal
    Dim _subtotal_ticket_expected_amount As Decimal
    Dim _subtotal_ticket_collected_amount As Decimal
    Dim _subtotal_coin_expected_amount As Decimal
    Dim _subtotal_coin_collected_amount As Decimal


    m_insert_last_row = True
    If DbRow.Value(SQL_COLUMN_BILLS_EXPECTED_AMOUNT) IsNot DBNull.Value AndAlso Not Decimal.TryParse(DbRow.Value(SQL_COLUMN_BILLS_EXPECTED_AMOUNT), _subtotal_bill_expected_amount) Then
      _subtotal_bill_expected_amount = 0
    End If
    If DbRow.Value(SQL_COLUMN_BILLS_COLLECTED_AMOUNT) IsNot DBNull.Value AndAlso Not Decimal.TryParse(DbRow.Value(SQL_COLUMN_BILLS_COLLECTED_AMOUNT), _subtotal_bill_collected_amount) Then
      _subtotal_bill_collected_amount = 0
    End If
    If DbRow.Value(SQL_COLUMN_COINS_EXPECTED_AMOUNT) IsNot DBNull.Value AndAlso Not Decimal.TryParse(DbRow.Value(SQL_COLUMN_COINS_EXPECTED_AMOUNT), _subtotal_coin_expected_amount) Then
      _subtotal_coin_expected_amount = 0
    End If
    If DbRow.Value(SQL_COLUMN_COINS_COLLECTED_AMOUNT) IsNot DBNull.Value AndAlso Not Decimal.TryParse(DbRow.Value(SQL_COLUMN_COINS_COLLECTED_AMOUNT), _subtotal_coin_collected_amount) Then
      _subtotal_coin_collected_amount = 0
    End If
    If DbRow.Value(SQL_COLUMN_TICKETS_EXPECTED_AMOUNT) IsNot DBNull.Value AndAlso Not Decimal.TryParse(DbRow.Value(SQL_COLUMN_TICKETS_EXPECTED_AMOUNT), _subtotal_ticket_expected_amount) Then
      _subtotal_ticket_expected_amount = 0
    End If
    If DbRow.Value(SQL_COLUMN_TICKETS_COLLECTED_AMOUNT) IsNot DBNull.Value AndAlso Not Decimal.TryParse(DbRow.Value(SQL_COLUMN_TICKETS_COLLECTED_AMOUNT), _subtotal_ticket_collected_amount) Then
      _subtotal_ticket_collected_amount = 0
    End If

    If ResetValues Then
      m_subtotal_bill_expected_amount = _subtotal_bill_expected_amount
      m_subtotal_bill_collected_amount = _subtotal_bill_collected_amount
      m_subtotal_coin_expected_amount = _subtotal_coin_expected_amount
      m_subtotal_coin_collected_amount = _subtotal_coin_collected_amount
      m_subtotal_ticket_expected_amount = _subtotal_ticket_expected_amount
      m_subtotal_ticket_collected_amount = _subtotal_ticket_collected_amount
    Else
      m_subtotal_bill_expected_amount = m_subtotal_bill_expected_amount + _subtotal_bill_expected_amount
      m_subtotal_bill_collected_amount = m_subtotal_bill_collected_amount + _subtotal_bill_collected_amount
      m_subtotal_coin_expected_amount = m_subtotal_coin_expected_amount + _subtotal_coin_expected_amount
      m_subtotal_coin_collected_amount = m_subtotal_coin_collected_amount + _subtotal_coin_collected_amount
      m_subtotal_ticket_expected_amount = m_subtotal_ticket_expected_amount + _subtotal_ticket_expected_amount
      m_subtotal_ticket_collected_amount = m_subtotal_ticket_collected_amount + _subtotal_ticket_collected_amount
    End If

  End Sub

  Private Function GetIndexDateSubtotal() As Int32

    If opt_insertion_datetime.Checked Then
      m_index_of_date_to_calculate_subtotal = SQL_COLUMN_MC_DATETIME
      m_index_to_print_subtotal_date = GRID_COLUMN_STACKER_COLLECTION_START_DATE
    ElseIf opt_collection_datetime.Checked Then
      m_index_of_date_to_calculate_subtotal = SQL_COLUMN_COLLECTION_DATETIME
      m_index_to_print_subtotal_date = GRID_COLUMN_STACKER_COLLECTION_DATE
    Else
      m_index_of_date_to_calculate_subtotal = SQL_COLUMN_CAGE_SESSION_DATE
      m_index_to_print_subtotal_date = GRID_COLUMN_CAGE_SESSION_DATE
    End If

  End Function

  Private Sub EnableControlsDuringSelection(ByVal pbEnable As Boolean)

    'disable frames to avoid errors during the selection
    gb_date_filter.Enabled = pbEnable
    gb_stacker.Enabled = pbEnable
    uc_provider.Enabled = pbEnable
    gb_user.Enabled = pbEnable
    gb_group_by.Enabled = pbEnable
    chk_terminal_location.Enabled = pbEnable
    gb_collections_status.Enabled = pbEnable

  End Sub

  Private Sub SetNothingModeForMoneyCollectionSelection()
    gb_collections_status.Enabled = False
    gb_date_filter.Enabled = False
    gb_group_by.Enabled = False
    gb_stacker.Enabled = False
    gb_user.Enabled = False
    chk_terminal_location.Enabled = False
    uc_provider.Enabled = False

    chk_pending_collection.Checked = True
    chk_collection_status_correct.Checked = False
    chk_collection_status_incorrect.Checked = False
    chk_inserted_validated.Checked = False
    chk_open_with_activity.Checked = False
    chk_group_by.Checked = False
    chk_show_detail.Checked = False
    chk_terminal_location.Checked = False

    ds_from_to.FromDateSelected = False
    ds_from_to.ToDateSelected = False

    Call SetSpecifiedStacker(m_stacker_id)

    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_RESET).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_INFO).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_3).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_EXCEL).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_PRINT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Enabled = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(723)   ' Seleccionar
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Enabled = True


    ' Search
    Call MyBase.GUI_ButtonClick(frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY)

    Call EnableControlsDuringSelection(False)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Enabled = False

  End Sub

  Private Sub SetSpecifiedStacker(ByVal StackerId As String)
    Dim _row As Int32

    If (String.IsNullOrEmpty(StackerId) OrElse StackerId = 0) AndAlso TITO.Utils.IsTitoMode() Then
      dg_stackers.Cell(0, GRID_STACKERS_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED

      Return
    End If
    For _row = 1 To dg_stackers.NumRows
      If dg_stackers.Cell(_row, GRID_STACKERS_COLUMN_ID).Value = m_stacker_id Then
        dg_stackers.Cell(_row, GRID_STACKERS_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED

        Return
      End If
    Next

  End Sub

  Private Sub GetSelectedMoneyCollection()
    Dim _idx_row As Integer

    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    'LMRS 30/09/2014 WIG-1339
    Me.m_selected_money_collection = -1
    If Me.Grid.NumRows > 0 AndAlso Not Me.Grid.Cell(_idx_row, GRID_COLUMN_STACKER_COLLECTION_ID).Value = "" Then
      Me.m_selected_money_collection = Me.Grid.Cell(_idx_row, GRID_COLUMN_STACKER_COLLECTION_ID).Value
      Call Me.Close()
    End If
  End Sub

#End Region 'privates functions and methods

#Region " Public Functions"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    m_cashier_session_id = 0
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION
    Me.MdiParent = MdiParent

    Call Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE: Opens dialog with default settings for View mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  Public Sub ShowForView(ByVal FloorId As String, ByVal StackerId As Int64)

    m_stacker_id = StackerId
    m_floor_id = FloorId
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    Call Me.Display(True)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Events"

  Private Sub opt_one_employee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_one_employee.Click

    Me.cmb_employee.Enabled = True
    Me.chk_open_with_activity.Enabled = False
    Me.chk_open_with_activity.Checked = False
    Me.chk_inserted_validated.Enabled = False
    Me.chk_inserted_validated.Checked = False
    Me.chk_pending_collection.Enabled = False
    Me.chk_pending_collection.Checked = False

  End Sub ' opt_one_employee_Click

  Private Sub opt_all_employee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_employee.Click
    Dim _enable_open_state As Boolean
    _enable_open_state = opt_insertion_datetime.Checked Or Not opt_insertion_datetime.Enabled

    Me.cmb_employee.Enabled = False
    Me.chk_inserted_validated.Enabled = _enable_open_state
    Me.chk_open_with_activity.Enabled = _enable_open_state
    Me.chk_pending_collection.Enabled = _enable_open_state
  End Sub ' opt_all_employee_Click

  Private Sub bt_check_all_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_check_all.Click
    Dim idx_rows As Integer

    For idx_rows = 0 To dg_stackers.NumRows - 1
      dg_stackers.Cell(idx_rows, GRID_STACKERS_COLUMN_CHECKED).Value = uc_grid.GRID_CHK_CHECKED
    Next

  End Sub

  Private Sub bt_uncheck_all_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_uncheck_all.Click
    Call UncheckAllStackers()
  End Sub

  Private Sub chk_status_check_changed(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_pending_collection.CheckedChanged, chk_open_with_activity.CheckedChanged, chk_inserted_validated.CheckedChanged, chk_collection_status_incorrect.CheckedChanged, chk_collection_status_correct.CheckedChanged
    If (chk_inserted_validated.Checked Or chk_open_with_activity.Checked) And Not (chk_collection_status_correct.Checked Or chk_collection_status_incorrect.Checked Or chk_pending_collection.Checked) Then
      ds_from_to.FromDateSelected = False
      ds_from_to.ToDateSelected = False
      opt_collection_datetime.Enabled = False
      opt_cage_session_datetime.Enabled = False
    Else
      opt_collection_datetime.Enabled = True
      opt_cage_session_datetime.Enabled = True
      ds_from_to.Enabled = True
    End If
  End Sub

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location.CheckedChanged
    If chk_terminal_location.Checked Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If

    m_refresh_grid = True
  End Sub

  Private Sub chk_group_by_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_group_by.CheckedChanged

    opt_group_by_day.Enabled = chk_group_by.Checked
    opt_provider.Enabled = chk_group_by.Checked
    opt_terminal.Enabled = chk_group_by.Checked
    chk_show_detail.Enabled = chk_group_by.Checked

  End Sub

  Private Sub opt_date_creation_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_group_by_day.CheckedChanged
    m_group_by_date = opt_group_by_day.Checked
    m_group_by_provider = Not m_group_by_date
  End Sub

  Private Sub opt_collection_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_collection_datetime.CheckedChanged
    Call DisableChecks(opt_collection_datetime.Checked)
  End Sub

  Private Sub opt_cage_session_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_cage_session_datetime.CheckedChanged
    Call DisableChecks(opt_cage_session_datetime.Checked)
  End Sub
  Private Sub DisableChecks(ByVal Status As Boolean)
    chk_inserted_validated.Enabled = Not Status
    chk_open_with_activity.Enabled = Not Status
    chk_pending_collection.Enabled = Not Status
    If Status Then
      chk_inserted_validated.Checked = False
      chk_open_with_activity.Checked = False
      chk_pending_collection.Checked = False
    End If
  End Sub

  Private Sub opt_insertion_datetime_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles opt_insertion_datetime.MouseClick
    opt_group_by_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2251)
  End Sub

  Private Sub opt_collection_datetime_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles opt_collection_datetime.MouseClick
    opt_group_by_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2206)
  End Sub

  Private Sub opt_cage_session_datetime_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles opt_cage_session_datetime.MouseClick
    opt_group_by_day.Text = opt_cage_session_datetime.Text
  End Sub

#End Region ' Events



End Class