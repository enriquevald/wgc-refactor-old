'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_tito_stacker_history_sel
' DESCRIPTION:   
' AUTHOR:        David Rigal
' CREATION DATE: 12-Nov-2013
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 08-NOV-2013 DRV    First Release
' 03-DEC-2013 AMF    Use mdl_tito.CollectionStatusAsString
' 09-DEC-2013 AMF    Reorder grid columns
' 20-JAN-2013 DRV    Fixed Bug: WIGOSTITO-997 Realiza la b�squeda al abrir la pantalla
' 25-MAR-2014 ICS    Fixed Bug #WIGOSTITO-1167 The hour on the date filter in Excel is not correct
'-------------------------------------------------------------------
Option Explicit On
Option Strict Off

Imports System.Text
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common

Public Class frm_tito_stacker_history_sel
  Inherits GUI_Controls.frm_base_sel

#Region " Members "

  ' al introducir datos en la tabla, se insertar� un prefijo num�rico para conservar el orden
  ' ese prefijo se deber� quitar al momento de enviarlo a las rutinas de impresi�n
  Dim m_date_from As String
  Dim m_date_to As String
  Dim m_stacker_id As Int64


#End Region ' Members

#Region " Constants "

  ' Grid configuration
  Private Const GRID_COLUMNS_COUNT As Integer = 19
  Private Const GRID_HEADER_ROWS As Integer = 2

  Private Const GRID_COLUMN_SELECT As Integer = 0
  Private Const GRID_COLUMN_INSERTED_DATE As Integer = 1
  Private Const GRID_COLUMN_TERMINAL As Integer = 2
  Private Const GRID_COLUMN_STATUS As Integer = 3
  Private Const GRID_COLUMN_EXTRACTION_DATE As Integer = 4
  Private Const GRID_COLUMN_COLLECT_MONEY_DATE As Integer = 5
  Private Const GRID_COLUMN_EMPLOYEE_COLLECTION As Integer = 6
  Private Const GRID_COLUMN_NOTES As Integer = 7
  Private Const GRID_COLUMN_EXPECTED_BILLS_COUNT As Integer = 8
  Private Const GRID_COLUMN_EXPECTED_BILLS_SUM As Integer = 9
  Private Const GRID_COLUMN_BILLS_READ_COUNT As Integer = 10
  Private Const GRID_COLUMN_BILLS_READ_AMOUNT As Integer = 11
  Private Const GRID_COLUMN_EXPECTED_COINS_AMOUNT As Integer = 12
  Private Const GRID_COLUMN_COLLECTED_COINS_AMOUNT As Integer = 13
  Private Const GRID_COLUMN_EXPECTED_TICKETS_COUNT As Integer = 14
  Private Const GRID_COLUMN_EXPECTED_TICKETS_SUM As Integer = 15
  Private Const GRID_COLUMN_TICKETS_READ_COUNT As Integer = 16
  Private Const GRID_COLUMN_TICKETS_READ_AMOUNT As Integer = 17
  Private Const GRID_COLUMN_STACKER_COLLECTION_ID As Integer = 18


  ' QUERY columns
  Private Const SQL_COLUMN_SELECT As Integer = 0
  Private Const SQL_COLUMN_ID As Integer = 1
  Private Const SQL_COLUMN_TERMINAL As Integer = 2
  Private Const SQL_COLUMN_MC_STATUS As Integer = 3
  Private Const SQL_COLUMN_EMPLOYEE_COLLECTION As Integer = 4
  Private Const SQL_COLUMN_EXPECTED_BILLS_COUNT As Integer = 5
  Private Const SQL_COLUMN_EXPECTED_BILLS_AMOUNT As Integer = 6
  Private Const SQL_COLUMN_COLLECTED_BILLS_COUNT As Integer = 7
  Private Const SQL_COLUMN_COLLECTED_BILLS_AMOUNT As Integer = 8
  Private Const SQL_COLUMN_EXPECTED_COINS_AMOUNT As Integer = 9
  Private Const SQL_COLUMN_COLLECTED_COINS_AMOUNT As Integer = 10
  Private Const SQL_COLUMN_EXPECTED_TICKETS_COUNT As Integer = 11
  Private Const SQL_COLUMN_EXPECTED_TICKETS_AMOUNT As Integer = 12
  Private Const SQL_COLUMN_COLLECTED_TICKETS_COUNT As Integer = 13
  Private Const SQL_COLUMN_COLLECTED_TICKETS_AMOUNT As Integer = 14
  Private Const SQL_COLUMN_COLLECT_MONEY_DATE As Integer = 15
  Private Const SQL_COLUMN_INSERTED_DATE As Integer = 16
  Private Const SQL_COLUMN_EXTRACTION_DATE As Integer = 17
  Private Const SQL_COLUMN_NOTES As Integer = 18
  

  Private Const SQL_COLUMN_STACKER_COLLECTION_ID As Integer = SQL_COLUMN_SELECT

  Private Const GRID_COLUMN_SELECT_WIDTH As Integer = 200
  Private Const GRID_COLUMN_TERMINAL_WIDTH As Integer = 2200
  Private Const GRID_COLUMN_STATUS_WIDTH As Integer = 2200
  Private Const GRID_COLUMN_INSERTED_DATE_WIDTH As Integer = 2000
  Private Const GRID_COLUMN_EXTRACTION_DATE_WIDTH As Integer = 2000
  Private Const GRID_COLUMN_EXPECTED_BILLS_COUNT_WIDTH As Integer = 1600
  Private Const GRID_COLUMN_EXPECTED_BILLS_SUM_WIDTH As Integer = 1800
  Private Const GRID_COLUMN_BILLS_READ_COUNT_WIDTH As Integer = 1600
  Private Const GRID_COLUMN_BILLS_READ_AMOUNT_WIDTH As Integer = 1800
  Private Const GRID_COLUMN_COINS_AMOUNT_WIDTH As Integer = 1850
  Private Const GRID_COLUMN_EXPECTED_TICKETS_COUNT_WIDTH As Integer = 1600
  Private Const GRID_COLUMN_EXPECTED_TICKETS_SUM_WIDTH As Integer = 1800
  Private Const GRID_COLUMN_TICKETS_READ_COUNT_WIDTH As Integer = 1600
  Private Const GRID_COLUMN_TICKETS_READ_AMOUNT_WIDTH As Integer = 1800
  Private Const GRID_COLUMN_COLLECT_MONEY_DATE_WIDTH As Integer = 2000
  Private Const GRID_COLUMN_EMPLOYEE_COLLECTION_WIDTH As Integer = 2200
  Private Const GRID_COLUMN_NOTES_WIDTH As Integer = 2600
  Private Const GRID_COLUMN_STACKER_COLLECTION_ID_WIDTH As Integer = 0

  Private Const CLOSE_AND_BALANCED = 3
  Private Const CLOSE_AND_UNBALANCED = 2
  Private Const COUNTER_INFO As Integer = 1
  Private Const COUNTER_WARNING As Integer = 2

  Private Const COLOR_ERROR_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  Private Const COLOR_ERROR_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
   Private Const COLOR_INFO_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_INFO_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00

#End Region ' Constants

#Region " Private Functions and Methods "

  ' PURPOSE : Define layout of all Main Grid Columns
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS_COUNT, GRID_HEADER_ROWS)

      .Counter(COUNTER_INFO).Visible = True
      .Counter(COUNTER_INFO).BackColor = GetColor(COLOR_INFO_BACK)
      .Counter(COUNTER_INFO).ForeColor = GetColor(COLOR_INFO_FORE)

      .Counter(COUNTER_WARNING).Visible = True
      .Counter(COUNTER_WARNING).BackColor = GetColor(COLOR_ERROR_BACK)
      .Counter(COUNTER_WARNING).ForeColor = GetColor(COLOR_ERROR_FORE)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = False

      .Column(GRID_COLUMN_SELECT).Header(0).Text = ""
      .Column(GRID_COLUMN_SELECT).Header(1).Text = ""
      .Column(GRID_COLUMN_SELECT).Width = GRID_COLUMN_SELECT_WIDTH
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False
      .Column(GRID_COLUMN_SELECT).HighLightWhenSelected = False

      .Column(GRID_COLUMN_TERMINAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2121)
      .Column(GRID_COLUMN_TERMINAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(492)
      .Column(GRID_COLUMN_TERMINAL).Width = GRID_COLUMN_TERMINAL_WIDTH

      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2121)
      .Column(GRID_COLUMN_STATUS).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)
      .Column(GRID_COLUMN_STATUS).Width = GRID_COLUMN_STATUS_WIDTH
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_INSERTED_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2121)
      .Column(GRID_COLUMN_INSERTED_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2186)
      .Column(GRID_COLUMN_INSERTED_DATE).Width = GRID_COLUMN_INSERTED_DATE_WIDTH
      .Column(GRID_COLUMN_INSERTED_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_EXTRACTION_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2121)
      .Column(GRID_COLUMN_EXTRACTION_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2353)
      .Column(GRID_COLUMN_EXTRACTION_DATE).Width = GRID_COLUMN_EXTRACTION_DATE_WIDTH
      .Column(GRID_COLUMN_EXTRACTION_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_COLLECT_MONEY_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2121)
      .Column(GRID_COLUMN_COLLECT_MONEY_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2206)
      .Column(GRID_COLUMN_COLLECT_MONEY_DATE).Width = GRID_COLUMN_COLLECT_MONEY_DATE_WIDTH
      .Column(GRID_COLUMN_COLLECT_MONEY_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_EMPLOYEE_COLLECTION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2121)
      .Column(GRID_COLUMN_EMPLOYEE_COLLECTION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2352)
      .Column(GRID_COLUMN_EMPLOYEE_COLLECTION).Width = GRID_COLUMN_EMPLOYEE_COLLECTION_WIDTH

      .Column(GRID_COLUMN_NOTES).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2121)
      .Column(GRID_COLUMN_NOTES).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2188)
      .Column(GRID_COLUMN_NOTES).Width = GRID_COLUMN_NOTES_WIDTH

      .Column(GRID_COLUMN_EXPECTED_BILLS_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2914)
      .Column(GRID_COLUMN_EXPECTED_BILLS_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2915)
      .Column(GRID_COLUMN_EXPECTED_BILLS_COUNT).Width = GRID_COLUMN_EXPECTED_BILLS_COUNT_WIDTH

      .Column(GRID_COLUMN_EXPECTED_BILLS_SUM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2914)
      .Column(GRID_COLUMN_EXPECTED_BILLS_SUM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2916)
      .Column(GRID_COLUMN_EXPECTED_BILLS_SUM).Width = GRID_COLUMN_EXPECTED_BILLS_SUM_WIDTH

      .Column(GRID_COLUMN_BILLS_READ_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2913)
      .Column(GRID_COLUMN_BILLS_READ_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2915)
      .Column(GRID_COLUMN_BILLS_READ_COUNT).Width = GRID_COLUMN_BILLS_READ_COUNT_WIDTH

      .Column(GRID_COLUMN_BILLS_READ_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2913)
      .Column(GRID_COLUMN_BILLS_READ_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2916)
      .Column(GRID_COLUMN_BILLS_READ_AMOUNT).Width = GRID_COLUMN_BILLS_READ_AMOUNT_WIDTH

      .Column(GRID_COLUMN_EXPECTED_COINS_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6710)
      .Column(GRID_COLUMN_EXPECTED_COINS_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2916) & " "
      .Column(GRID_COLUMN_EXPECTED_COINS_AMOUNT).Width = GRID_COLUMN_COINS_AMOUNT_WIDTH

      .Column(GRID_COLUMN_COLLECTED_COINS_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6711)
      .Column(GRID_COLUMN_COLLECTED_COINS_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2916)
      .Column(GRID_COLUMN_COLLECTED_COINS_AMOUNT).Width = GRID_COLUMN_COINS_AMOUNT_WIDTH

      .Column(GRID_COLUMN_EXPECTED_TICKETS_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2918)
      .Column(GRID_COLUMN_EXPECTED_TICKETS_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2915)
      .Column(GRID_COLUMN_EXPECTED_TICKETS_COUNT).Width = GRID_COLUMN_EXPECTED_TICKETS_COUNT_WIDTH

      .Column(GRID_COLUMN_EXPECTED_TICKETS_SUM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2918)
      .Column(GRID_COLUMN_EXPECTED_TICKETS_SUM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2916)
      .Column(GRID_COLUMN_EXPECTED_TICKETS_SUM).Width = GRID_COLUMN_EXPECTED_TICKETS_SUM_WIDTH

      .Column(GRID_COLUMN_TICKETS_READ_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2917)
      .Column(GRID_COLUMN_TICKETS_READ_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2915)
      .Column(GRID_COLUMN_TICKETS_READ_COUNT).Width = GRID_COLUMN_TICKETS_READ_COUNT_WIDTH

      .Column(GRID_COLUMN_TICKETS_READ_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2917)
      .Column(GRID_COLUMN_TICKETS_READ_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2916)
      .Column(GRID_COLUMN_TICKETS_READ_AMOUNT).Width = GRID_COLUMN_TICKETS_READ_AMOUNT_WIDTH

      .Column(GRID_COLUMN_STACKER_COLLECTION_ID).Header(0).Text = ""
      .Column(GRID_COLUMN_STACKER_COLLECTION_ID).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2188)
      .Column(GRID_COLUMN_STACKER_COLLECTION_ID).Width = GRID_COLUMN_STACKER_COLLECTION_ID_WIDTH
    End With

  End Sub ' GUI_StyleSheet

#End Region

#Region " OVERRIDES "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = 0 ' ENUM_FORM.FORM_STACKERS_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4557)
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.uc_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2186)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False

    ' Entry Field Stacker
    Me.ef_stacker.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2180)
    Me.ef_stacker.TextValue = m_stacker_id.ToString()
    Me.ef_stacker.Enabled = False

    Call GUI_StyleSheet()

    Call GUI_FilterReset()


    Me.Grid.Focus()
  End Sub ' GUI_InitControls


  Protected Overrides Sub GUI_FilterReset()

    Dim _final_time As Date
    Dim _closing_time As Integer

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(Now.Year, Now.Month, Now.Day, _closing_time, 0, 0)
    _final_time = _final_time.AddDays(1)
    uc_date.ToDate = _final_time.Date
    uc_date.ToDateSelected = True
    uc_date.FromDate = _final_time.AddDays(-2)
    uc_date.FromDateSelected = True
    uc_date.ClosingTime = _closing_time

  End Sub ' GUI_FilterReset

  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _str_condition As String
    Dim _str_sql_builder As StringBuilder

    _str_sql_builder = New StringBuilder()

    _str_sql_builder.AppendLine("   SELECT   MC.MC_COLLECTION_ID ")
    _str_sql_builder.AppendLine("          , MC.MC_STACKER_ID   ")
    _str_sql_builder.AppendLine("          , ISNULL(TE.TE_NAME, '')   ")
    _str_sql_builder.AppendLine("          , MC.MC_STATUS ")
    _str_sql_builder.AppendLine("          , ISNULL(USR1.GU_USERNAME, '' ) AS USER_NM ")
    _str_sql_builder.AppendLine("          , ISNULL(MC.MC_EXPECTED_BILL_COUNT, 0) ")
    _str_sql_builder.AppendLine("          , ISNULL(MC.MC_EXPECTED_BILL_AMOUNT, 0) ")
    _str_sql_builder.AppendLine("          , ISNULL(MC.MC_COLLECTED_BILL_COUNT, 0) ")
    _str_sql_builder.AppendLine("          , ISNULL(MC.MC_COLLECTED_BILL_AMOUNT, 0) ")
    _str_sql_builder.AppendLine("          , ISNULL(MC.MC_EXPECTED_COIN_AMOUNT, 0) ")
    _str_sql_builder.AppendLine("          , ISNULL(MC.MC_COLLECTED_COIN_AMOUNT, 0) ")
    _str_sql_builder.AppendLine("          , ISNULL(MC.MC_EXPECTED_TICKET_COUNT, 0) ")
    _str_sql_builder.AppendLine("          , ISNULL(MC.MC_EXPECTED_TICKET_AMOUNT, 0) ")
    _str_sql_builder.AppendLine("          , ISNULL(MC.MC_COLLECTED_TICKET_COUNT, 0) ")
    _str_sql_builder.AppendLine("          , ISNULL(MC.MC_COLLECTED_TICKET_AMOUNT, 0) ")
    _str_sql_builder.AppendLine("          , MC.MC_COLLECTION_DATETIME  ")
    _str_sql_builder.AppendLine("          , MC.MC_DATETIME  ")
    _str_sql_builder.AppendLine("          , MC.MC_EXTRACTION_DATETIME  ")
    _str_sql_builder.AppendLine("          , ISNULL(MC.MC_NOTES, '')  ")
    _str_sql_builder.AppendLine("     FROM   MONEY_COLLECTIONS AS MC  ")
    _str_sql_builder.AppendLine("            LEFT JOIN TERMINALS AS TE  ON MC.MC_TERMINAL_ID = TE.TE_TERMINAL_ID   ")
    _str_sql_builder.AppendLine("            LEFT JOIN GUI_USERS AS USR1 ON MC.MC_USER_ID = USR1.GU_USER_ID  ")
    _str_sql_builder.AppendLine("    WHERE   MC.MC_COLLECTION_DATETIME IS NOT NULL ")
    _str_sql_builder.AppendLine("      AND   MC.MC_STACKER_ID = " & m_stacker_id.ToString())

    ' Se filtra por fecha de inserci�n
    _str_condition = uc_date.GetSqlFilterCondition("MC.MC_DATETIME")

    If Not String.IsNullOrEmpty(_str_condition) Then
      _str_sql_builder.AppendLine("   AND    ")
      _str_sql_builder.AppendLine("(" & _str_condition & ")")
    End If

    _str_sql_builder.AppendLine(" ORDER BY   MC.MC_DATETIME")

    Return _str_sql_builder.ToString()

  End Function ' GUI_FilterGetSqlQuery

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    With Me.Grid
      .Cell(RowIndex, GRID_COLUMN_TERMINAL).Value = DbRow.Value(SQL_COLUMN_TERMINAL)
      .Cell(RowIndex, GRID_COLUMN_STATUS).Value = mdl_tito.CollectionStatusAsString(IIf(IsDBNull(DbRow.Value(SQL_COLUMN_MC_STATUS)), TITO_MONEY_COLLECTION_STATUS.NONE, DbRow.Value(SQL_COLUMN_MC_STATUS)))
      .Cell(RowIndex, GRID_COLUMN_INSERTED_DATE).Value = mdl_tito.GetFormatedDate(DbRow.Value(SQL_COLUMN_INSERTED_DATE))
      .Cell(RowIndex, GRID_COLUMN_EXTRACTION_DATE).Value = mdl_tito.GetFormatedDate(DbRow.Value(SQL_COLUMN_EXTRACTION_DATE))
      .Cell(RowIndex, GRID_COLUMN_EXPECTED_BILLS_COUNT).Value = mdl_tito.GetValueForGrid(DbRow.Value(SQL_COLUMN_EXPECTED_BILLS_COUNT))
      .Cell(RowIndex, GRID_COLUMN_EXPECTED_BILLS_SUM).Value = mdl_tito.GetFormatedCurrency(DbRow.Value(SQL_COLUMN_EXPECTED_BILLS_AMOUNT))
      .Cell(RowIndex, GRID_COLUMN_BILLS_READ_COUNT).Value = mdl_tito.GetValueForGrid(DbRow.Value(SQL_COLUMN_COLLECTED_BILLS_COUNT))
      .Cell(RowIndex, GRID_COLUMN_BILLS_READ_AMOUNT).Value = mdl_tito.GetFormatedCurrency(DbRow.Value(SQL_COLUMN_COLLECTED_BILLS_AMOUNT))
      .Cell(RowIndex, GRID_COLUMN_EXPECTED_COINS_AMOUNT).Value = mdl_tito.GetFormatedCurrency(DbRow.Value(SQL_COLUMN_EXPECTED_COINS_AMOUNT))
      .Cell(RowIndex, GRID_COLUMN_COLLECTED_COINS_AMOUNT).Value = mdl_tito.GetFormatedCurrency(DbRow.Value(SQL_COLUMN_COLLECTED_COINS_AMOUNT))
      .Cell(RowIndex, GRID_COLUMN_EXPECTED_TICKETS_COUNT).Value = mdl_tito.GetValueForGrid(DbRow.Value(SQL_COLUMN_EXPECTED_TICKETS_COUNT))
      .Cell(RowIndex, GRID_COLUMN_EXPECTED_TICKETS_SUM).Value = mdl_tito.GetFormatedCurrency(DbRow.Value(SQL_COLUMN_EXPECTED_TICKETS_AMOUNT))
      .Cell(RowIndex, GRID_COLUMN_TICKETS_READ_COUNT).Value = mdl_tito.GetValueForGrid(DbRow.Value(SQL_COLUMN_COLLECTED_TICKETS_COUNT))
      .Cell(RowIndex, GRID_COLUMN_TICKETS_READ_AMOUNT).Value = mdl_tito.GetFormatedCurrency(DbRow.Value(SQL_COLUMN_COLLECTED_TICKETS_AMOUNT))
      .Cell(RowIndex, GRID_COLUMN_COLLECT_MONEY_DATE).Value = mdl_tito.GetFormatedDate(DbRow.Value(SQL_COLUMN_COLLECT_MONEY_DATE))
      .Cell(RowIndex, GRID_COLUMN_EMPLOYEE_COLLECTION).Value = mdl_tito.GetValueForGrid(DbRow.Value(SQL_COLUMN_EMPLOYEE_COLLECTION))
      .Cell(RowIndex, GRID_COLUMN_NOTES).Value = mdl_tito.GetValueForGrid(DbRow.Value(SQL_COLUMN_NOTES))
      .Cell(RowIndex, GRID_COLUMN_STACKER_COLLECTION_ID).Value = mdl_tito.GetValueForGrid(DbRow.Value(SQL_COLUMN_STACKER_COLLECTION_ID))

      Select Case DbRow.Value(SQL_COLUMN_MC_STATUS)
        Case CLOSE_AND_BALANCED
          Me.Grid.Counter(COUNTER_INFO).Value += 1

        Case CLOSE_AND_UNBALANCED
          .Cell(RowIndex, GRID_COLUMN_STATUS).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_RED_00)
          .Cell(RowIndex, GRID_COLUMN_STATUS).ForeColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_WHITE_00)
          Me.Grid.Counter(COUNTER_WARNING).Value += 1

      End Select

    End With

    Return True

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_ReportUpdateFilters()

    If uc_date.FromDateSelected Then
      m_date_from = GUI_FormatDate(uc_date.FromDate.AddHours(uc_date.ClosingTime), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Else
      m_date_from = ""
    End If

    If uc_date.ToDateSelected Then
      m_date_to = GUI_FormatDate(uc_date.ToDate.AddHours(uc_date.ClosingTime), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Else
      m_date_to = ""
    End If

  End Sub ' GUI_ReportUpdateFilters

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    'dates
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2186) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(297), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2186) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(298), m_date_to)

    PrintData.FilterHeaderWidth(1) = 2000
    PrintData.FilterHeaderWidth(2) = 2500
    PrintData.FilterHeaderWidth(3) = 2500
    PrintData.FilterValueWidth(2) = 2100
    PrintData.FilterValueWidth(4) = 2100

  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

  End Sub ' GUI_ReportParams

  ' PURPOSE: Opens dialog with data from the setted stacker
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, ByVal stacker_id As Int64)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION

    Me.m_stacker_id = stacker_id

    Me.MdiParent = MdiParent
    Me.Display(False)


  End Sub ' ShowForEdit

#End Region ' OVERRIDES

#Region " Public Functions "


#End Region ' Public Functions

  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub

End Class
