<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_TITO_promotion_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Me.gb_enabled = New System.Windows.Forms.GroupBox
    Me.opt_disabled = New System.Windows.Forms.RadioButton
    Me.opt_enabled = New System.Windows.Forms.RadioButton
    Me.ef_promo_name = New GUI_Controls.uc_entry_field
    Me.gb_won_lock = New System.Windows.Forms.GroupBox
    Me.chk_won_lock = New System.Windows.Forms.CheckBox
    Me.lbl_won_lock = New GUI_Controls.uc_text_field
    Me.ef_won_lock = New GUI_Controls.uc_entry_field
    Me.tab_limit = New System.Windows.Forms.TabControl
    Me.tab_by_customer = New System.Windows.Forms.TabPage
    Me.ef_monthly_limit = New GUI_Controls.uc_entry_field
    Me.lbl_monthly_limit = New GUI_Controls.uc_text_field
    Me.ef_daily_limit = New GUI_Controls.uc_entry_field
    Me.lbl_daily_limit = New GUI_Controls.uc_text_field
    Me.tab_by_promotion = New System.Windows.Forms.TabPage
    Me.lbl_promotion_global_limit = New GUI_Controls.uc_text_field
    Me.ef_promotion_limit_global = New GUI_Controls.uc_entry_field
    Me.ef_promotion_limit_month = New GUI_Controls.uc_entry_field
    Me.lbl_promotion_monthly_limit = New GUI_Controls.uc_text_field
    Me.ef_promotion_limit_day = New GUI_Controls.uc_entry_field
    Me.lbl_promotion_daily_limit = New GUI_Controls.uc_text_field
    Me.uc_promo_schedule = New GUI_Controls.uc_schedule
    Me.tab_filters = New System.Windows.Forms.TabControl
    Me.tab_gender = New System.Windows.Forms.TabPage
    Me.opt_gender_male = New System.Windows.Forms.RadioButton
    Me.opt_gender_female = New System.Windows.Forms.RadioButton
    Me.chk_by_gender = New System.Windows.Forms.CheckBox
    Me.tab_birth_date = New System.Windows.Forms.TabPage
    Me.opt_birth_date_only = New System.Windows.Forms.RadioButton
    Me.opt_birth_month_only = New System.Windows.Forms.RadioButton
    Me.chk_by_birth_date = New System.Windows.Forms.CheckBox
    Me.tab_level = New System.Windows.Forms.TabPage
    Me.chk_level_anonymous = New System.Windows.Forms.CheckBox
    Me.chk_level_04 = New System.Windows.Forms.CheckBox
    Me.chk_level_03 = New System.Windows.Forms.CheckBox
    Me.chk_level_02 = New System.Windows.Forms.CheckBox
    Me.chk_level_01 = New System.Windows.Forms.CheckBox
    Me.chk_by_level = New System.Windows.Forms.CheckBox
    Me.tab_freq = New System.Windows.Forms.TabPage
    Me.lbl_freq_filter_02 = New GUI_Controls.uc_text_field
    Me.lbl_freq_filter_01 = New GUI_Controls.uc_text_field
    Me.ef_freq_min_cash_in = New GUI_Controls.uc_entry_field
    Me.ef_freq_min_days = New GUI_Controls.uc_entry_field
    Me.ef_freq_last_days = New GUI_Controls.uc_entry_field
    Me.chk_by_freq = New System.Windows.Forms.CheckBox
    Me.tab_flags_required = New System.Windows.Forms.TabPage
    Me.dg_flags_required = New GUI_Controls.uc_grid
    Me.chk_by_flag = New System.Windows.Forms.CheckBox
    Me.gb_test = New System.Windows.Forms.GroupBox
    Me.ef_test_spent = New GUI_Controls.uc_entry_field
    Me.lbl_test_result = New GUI_Controls.uc_text_field
    Me.ef_test_num_tokens = New GUI_Controls.uc_entry_field
    Me.ef_test_cash_in = New GUI_Controls.uc_entry_field
    Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
    Me.chk_special_permission = New System.Windows.Forms.CheckBox
    Me.gb_special_permission = New System.Windows.Forms.GroupBox
    Me.lbl_not_applicable_mb1 = New System.Windows.Forms.Label
    Me.opt_special_permission_b = New System.Windows.Forms.RadioButton
    Me.opt_special_permission_a = New System.Windows.Forms.RadioButton
    Me.ef_min_cash_in = New GUI_Controls.uc_entry_field
    Me.ef_min_cash_in_reward = New GUI_Controls.uc_entry_field
    Me.lbl_cash_in_promotion = New GUI_Controls.uc_text_field
    Me.lbl_min_cash_in_promotion = New GUI_Controls.uc_text_field
    Me.ef_cash_in_reward = New GUI_Controls.uc_entry_field
    Me.ef_cash_in = New GUI_Controls.uc_entry_field
    Me.gb_credit_type = New System.Windows.Forms.GroupBox
    Me.opt_point = New System.Windows.Forms.RadioButton
    Me.opt_credit_redeemable = New System.Windows.Forms.RadioButton
    Me.opt_credit_non_redeemable = New System.Windows.Forms.RadioButton
    Me.tabPromotions = New System.Windows.Forms.TabControl
    Me.tab_chash_in = New System.Windows.Forms.TabPage
    Me.tab_spent = New System.Windows.Forms.TabPage
    Me.ef_min_spent = New GUI_Controls.uc_entry_field
    Me.ef_min_spent_reward = New GUI_Controls.uc_entry_field
    Me.ef_spent = New GUI_Controls.uc_entry_field
    Me.lbl_spent_promotion = New GUI_Controls.uc_text_field
    Me.ef_spent_reward = New GUI_Controls.uc_entry_field
    Me.lbl_min_spent_promotion = New GUI_Controls.uc_text_field
    Me.tab_tokens = New System.Windows.Forms.TabPage
    Me.ef_num_tokens = New GUI_Controls.uc_entry_field
    Me.lbl_token_reward = New GUI_Controls.uc_text_field
    Me.ef_token_reward = New GUI_Controls.uc_entry_field
    Me.ef_token_name = New GUI_Controls.uc_entry_field
    Me.tab_flags_awarded = New System.Windows.Forms.TabPage
    Me.dg_flags_awarded = New GUI_Controls.uc_grid
    Me.chk_flags_awarded = New System.Windows.Forms.CheckBox
    Me.uc_provider_filter1 = New GUI_Controls.uc_provider_filter
    Me.lbl_icon = New System.Windows.Forms.Label
    Me.lbl_image = New System.Windows.Forms.Label
    Me.img_image = New GUI_Controls.uc_image
    Me.img_icon = New GUI_Controls.uc_image
    Me.lbl_icon_optimun_size = New System.Windows.Forms.Label
    Me.lbl_image_optimun_size = New System.Windows.Forms.Label
    Me.tc_expirarion_ticketfooter = New System.Windows.Forms.TabControl
    Me.tp_expiration = New System.Windows.Forms.TabPage
    Me.pa_expiration = New System.Windows.Forms.Panel
    Me.dtp_expiration_limit = New GUI_Controls.uc_date_picker
    Me.lbl_credits_non_redeemable = New GUI_Controls.uc_text_field
    Me.chk_expiration_limit = New System.Windows.Forms.CheckBox
    Me.ef_expiration_days = New GUI_Controls.uc_entry_field
    Me.ef_expiration_hours = New GUI_Controls.uc_entry_field
    Me.opt_expiration_hours = New System.Windows.Forms.RadioButton
    Me.opt_expiration_days = New System.Windows.Forms.RadioButton
    Me.tp_ticketfooter = New System.Windows.Forms.TabPage
    Me.tb_ticket_footer = New System.Windows.Forms.TextBox
    Me.cmb_categories = New GUI_Controls.uc_combo
    Me.chk_visible_on_PromoBOX = New System.Windows.Forms.CheckBox
    Me.gb_tito = New System.Windows.Forms.GroupBox
    Me.ef_generated_tickets_tito = New GUI_Controls.uc_entry_field
    Me.chk_allow_tickets_tito = New System.Windows.Forms.CheckBox
    Me.ef_quantity_tickets_tito = New GUI_Controls.uc_entry_field
    Me.panel_data.SuspendLayout()
    Me.gb_enabled.SuspendLayout()
    Me.gb_won_lock.SuspendLayout()
    Me.tab_limit.SuspendLayout()
    Me.tab_by_customer.SuspendLayout()
    Me.tab_by_promotion.SuspendLayout()
    Me.tab_filters.SuspendLayout()
    Me.tab_gender.SuspendLayout()
    Me.tab_birth_date.SuspendLayout()
    Me.tab_level.SuspendLayout()
    Me.tab_freq.SuspendLayout()
    Me.tab_flags_required.SuspendLayout()
    Me.gb_test.SuspendLayout()
    Me.gb_special_permission.SuspendLayout()
    Me.gb_credit_type.SuspendLayout()
    Me.tabPromotions.SuspendLayout()
    Me.tab_chash_in.SuspendLayout()
    Me.tab_spent.SuspendLayout()
    Me.tab_tokens.SuspendLayout()
    Me.tab_flags_awarded.SuspendLayout()
    Me.tc_expirarion_ticketfooter.SuspendLayout()
    Me.tp_expiration.SuspendLayout()
    Me.pa_expiration.SuspendLayout()
    Me.tp_ticketfooter.SuspendLayout()
    Me.gb_tito.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.gb_tito)
    Me.panel_data.Controls.Add(Me.chk_visible_on_PromoBOX)
    Me.panel_data.Controls.Add(Me.cmb_categories)
    Me.panel_data.Controls.Add(Me.tc_expirarion_ticketfooter)
    Me.panel_data.Controls.Add(Me.lbl_image_optimun_size)
    Me.panel_data.Controls.Add(Me.img_icon)
    Me.panel_data.Controls.Add(Me.img_image)
    Me.panel_data.Controls.Add(Me.lbl_image)
    Me.panel_data.Controls.Add(Me.lbl_icon)
    Me.panel_data.Controls.Add(Me.uc_provider_filter1)
    Me.panel_data.Controls.Add(Me.tabPromotions)
    Me.panel_data.Controls.Add(Me.tab_limit)
    Me.panel_data.Controls.Add(Me.gb_credit_type)
    Me.panel_data.Controls.Add(Me.gb_won_lock)
    Me.panel_data.Controls.Add(Me.tab_filters)
    Me.panel_data.Controls.Add(Me.gb_special_permission)
    Me.panel_data.Controls.Add(Me.ef_promo_name)
    Me.panel_data.Controls.Add(Me.gb_test)
    Me.panel_data.Controls.Add(Me.gb_enabled)
    Me.panel_data.Controls.Add(Me.uc_promo_schedule)
    Me.panel_data.Controls.Add(Me.lbl_icon_optimun_size)
    Me.panel_data.Location = New System.Drawing.Point(5, 4)
    Me.panel_data.Size = New System.Drawing.Size(981, 762)
    '
    'gb_enabled
    '
    Me.gb_enabled.Controls.Add(Me.opt_disabled)
    Me.gb_enabled.Controls.Add(Me.opt_enabled)
    Me.gb_enabled.Location = New System.Drawing.Point(211, 58)
    Me.gb_enabled.Name = "gb_enabled"
    Me.gb_enabled.Size = New System.Drawing.Size(194, 68)
    Me.gb_enabled.TabIndex = 3
    Me.gb_enabled.TabStop = False
    Me.gb_enabled.Text = "xEnabled"
    '
    'opt_disabled
    '
    Me.opt_disabled.Location = New System.Drawing.Point(15, 43)
    Me.opt_disabled.Name = "opt_disabled"
    Me.opt_disabled.Size = New System.Drawing.Size(70, 16)
    Me.opt_disabled.TabIndex = 1
    Me.opt_disabled.Text = "xNo"
    '
    'opt_enabled
    '
    Me.opt_enabled.Location = New System.Drawing.Point(15, 21)
    Me.opt_enabled.Name = "opt_enabled"
    Me.opt_enabled.Size = New System.Drawing.Size(70, 16)
    Me.opt_enabled.TabIndex = 0
    Me.opt_enabled.Text = "xYes"
    '
    'ef_promo_name
    '
    Me.ef_promo_name.DoubleValue = 0
    Me.ef_promo_name.IntegerValue = 0
    Me.ef_promo_name.IsReadOnly = False
    Me.ef_promo_name.Location = New System.Drawing.Point(12, 5)
    Me.ef_promo_name.Name = "ef_promo_name"
    Me.ef_promo_name.Size = New System.Drawing.Size(382, 24)
    Me.ef_promo_name.SufixText = "Sufix Text"
    Me.ef_promo_name.SufixTextVisible = True
    Me.ef_promo_name.TabIndex = 0
    Me.ef_promo_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_promo_name.TextValue = ""
    Me.ef_promo_name.TextWidth = 87
    Me.ef_promo_name.Value = ""
    '
    'gb_won_lock
    '
    Me.gb_won_lock.Controls.Add(Me.chk_won_lock)
    Me.gb_won_lock.Controls.Add(Me.lbl_won_lock)
    Me.gb_won_lock.Controls.Add(Me.ef_won_lock)
    Me.gb_won_lock.Location = New System.Drawing.Point(416, 551)
    Me.gb_won_lock.Name = "gb_won_lock"
    Me.gb_won_lock.Size = New System.Drawing.Size(513, 67)
    Me.gb_won_lock.TabIndex = 18
    Me.gb_won_lock.TabStop = False
    Me.gb_won_lock.Text = "xLock"
    '
    'chk_won_lock
    '
    Me.chk_won_lock.AutoSize = True
    Me.chk_won_lock.Location = New System.Drawing.Point(16, 21)
    Me.chk_won_lock.Name = "chk_won_lock"
    Me.chk_won_lock.Size = New System.Drawing.Size(15, 14)
    Me.chk_won_lock.TabIndex = 4
    Me.chk_won_lock.UseVisualStyleBackColor = True
    '
    'lbl_won_lock
    '
    Me.lbl_won_lock.AutoSize = True
    Me.lbl_won_lock.IsReadOnly = True
    Me.lbl_won_lock.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_won_lock.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_won_lock.Location = New System.Drawing.Point(6, 39)
    Me.lbl_won_lock.Name = "lbl_won_lock"
    Me.lbl_won_lock.Size = New System.Drawing.Size(478, 24)
    Me.lbl_won_lock.SufixText = "Sufix Text"
    Me.lbl_won_lock.SufixTextVisible = True
    Me.lbl_won_lock.TabIndex = 6
    Me.lbl_won_lock.TabStop = False
    Me.lbl_won_lock.TextWidth = 0
    Me.lbl_won_lock.Value = "xUser has to grow [] times the promotion amount to cash out prize"
    '
    'ef_won_lock
    '
    Me.ef_won_lock.DoubleValue = 0
    Me.ef_won_lock.IntegerValue = 0
    Me.ef_won_lock.IsReadOnly = False
    Me.ef_won_lock.Location = New System.Drawing.Point(49, 15)
    Me.ef_won_lock.Name = "ef_won_lock"
    Me.ef_won_lock.OnlyUpperCase = True
    Me.ef_won_lock.Size = New System.Drawing.Size(160, 24)
    Me.ef_won_lock.SufixText = "Sufix Text"
    Me.ef_won_lock.SufixTextVisible = True
    Me.ef_won_lock.TabIndex = 5
    Me.ef_won_lock.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_won_lock.TextValue = ""
    Me.ef_won_lock.TextWidth = 70
    Me.ef_won_lock.Value = ""
    '
    'tab_limit
    '
    Me.tab_limit.Controls.Add(Me.tab_by_customer)
    Me.tab_limit.Controls.Add(Me.tab_by_promotion)
    Me.tab_limit.Location = New System.Drawing.Point(416, 408)
    Me.tab_limit.Name = "tab_limit"
    Me.tab_limit.SelectedIndex = 0
    Me.tab_limit.Size = New System.Drawing.Size(513, 137)
    Me.tab_limit.TabIndex = 17
    '
    'tab_by_customer
    '
    Me.tab_by_customer.Controls.Add(Me.ef_monthly_limit)
    Me.tab_by_customer.Controls.Add(Me.lbl_monthly_limit)
    Me.tab_by_customer.Controls.Add(Me.ef_daily_limit)
    Me.tab_by_customer.Controls.Add(Me.lbl_daily_limit)
    Me.tab_by_customer.Location = New System.Drawing.Point(4, 22)
    Me.tab_by_customer.Name = "tab_by_customer"
    Me.tab_by_customer.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_by_customer.Size = New System.Drawing.Size(505, 111)
    Me.tab_by_customer.TabIndex = 0
    Me.tab_by_customer.Text = "xLimitCustomer"
    Me.tab_by_customer.UseVisualStyleBackColor = True
    '
    'ef_monthly_limit
    '
    Me.ef_monthly_limit.DoubleValue = 0
    Me.ef_monthly_limit.IntegerValue = 0
    Me.ef_monthly_limit.IsReadOnly = False
    Me.ef_monthly_limit.Location = New System.Drawing.Point(6, 57)
    Me.ef_monthly_limit.Name = "ef_monthly_limit"
    Me.ef_monthly_limit.Size = New System.Drawing.Size(200, 24)
    Me.ef_monthly_limit.SufixText = "Sufix Text"
    Me.ef_monthly_limit.SufixTextVisible = True
    Me.ef_monthly_limit.TabIndex = 2
    Me.ef_monthly_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_monthly_limit.TextValue = ""
    Me.ef_monthly_limit.TextVisible = False
    Me.ef_monthly_limit.TextWidth = 110
    Me.ef_monthly_limit.Value = ""
    '
    'lbl_monthly_limit
    '
    Me.lbl_monthly_limit.AutoSize = True
    Me.lbl_monthly_limit.IsReadOnly = True
    Me.lbl_monthly_limit.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_monthly_limit.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_monthly_limit.Location = New System.Drawing.Point(5, 81)
    Me.lbl_monthly_limit.Name = "lbl_monthly_limit"
    Me.lbl_monthly_limit.Size = New System.Drawing.Size(411, 24)
    Me.lbl_monthly_limit.SufixText = "Sufix Text"
    Me.lbl_monthly_limit.SufixTextVisible = True
    Me.lbl_monthly_limit.TabIndex = 3
    Me.lbl_monthly_limit.TabStop = False
    Me.lbl_monthly_limit.TextVisible = False
    Me.lbl_monthly_limit.TextWidth = 0
    Me.lbl_monthly_limit.Value = "x[] Maximum per customer, per month"
    '
    'ef_daily_limit
    '
    Me.ef_daily_limit.DoubleValue = 0
    Me.ef_daily_limit.IntegerValue = 0
    Me.ef_daily_limit.IsReadOnly = False
    Me.ef_daily_limit.Location = New System.Drawing.Point(6, 6)
    Me.ef_daily_limit.Name = "ef_daily_limit"
    Me.ef_daily_limit.Size = New System.Drawing.Size(200, 24)
    Me.ef_daily_limit.SufixText = "Sufix Text"
    Me.ef_daily_limit.SufixTextVisible = True
    Me.ef_daily_limit.TabIndex = 0
    Me.ef_daily_limit.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_daily_limit.TextValue = ""
    Me.ef_daily_limit.TextVisible = False
    Me.ef_daily_limit.TextWidth = 110
    Me.ef_daily_limit.Value = ""
    '
    'lbl_daily_limit
    '
    Me.lbl_daily_limit.AutoSize = True
    Me.lbl_daily_limit.IsReadOnly = True
    Me.lbl_daily_limit.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_daily_limit.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_daily_limit.Location = New System.Drawing.Point(6, 31)
    Me.lbl_daily_limit.Name = "lbl_daily_limit"
    Me.lbl_daily_limit.Size = New System.Drawing.Size(411, 24)
    Me.lbl_daily_limit.SufixText = "Sufix Text"
    Me.lbl_daily_limit.SufixTextVisible = True
    Me.lbl_daily_limit.TabIndex = 1
    Me.lbl_daily_limit.TabStop = False
    Me.lbl_daily_limit.TextVisible = False
    Me.lbl_daily_limit.TextWidth = 0
    Me.lbl_daily_limit.Value = "x[] Maximum per customer, per day"
    '
    'tab_by_promotion
    '
    Me.tab_by_promotion.Controls.Add(Me.lbl_promotion_global_limit)
    Me.tab_by_promotion.Controls.Add(Me.ef_promotion_limit_global)
    Me.tab_by_promotion.Controls.Add(Me.ef_promotion_limit_month)
    Me.tab_by_promotion.Controls.Add(Me.lbl_promotion_monthly_limit)
    Me.tab_by_promotion.Controls.Add(Me.ef_promotion_limit_day)
    Me.tab_by_promotion.Controls.Add(Me.lbl_promotion_daily_limit)
    Me.tab_by_promotion.Location = New System.Drawing.Point(4, 22)
    Me.tab_by_promotion.Name = "tab_by_promotion"
    Me.tab_by_promotion.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_by_promotion.Size = New System.Drawing.Size(505, 111)
    Me.tab_by_promotion.TabIndex = 2
    Me.tab_by_promotion.Text = "xLimitPromotion"
    Me.tab_by_promotion.UseVisualStyleBackColor = True
    '
    'lbl_promotion_global_limit
    '
    Me.lbl_promotion_global_limit.AutoSize = True
    Me.lbl_promotion_global_limit.IsReadOnly = True
    Me.lbl_promotion_global_limit.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_RIGTH
    Me.lbl_promotion_global_limit.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_promotion_global_limit.Location = New System.Drawing.Point(354, 37)
    Me.lbl_promotion_global_limit.Name = "lbl_promotion_global_limit"
    Me.lbl_promotion_global_limit.Size = New System.Drawing.Size(145, 24)
    Me.lbl_promotion_global_limit.SufixText = "Sufix Text"
    Me.lbl_promotion_global_limit.TabIndex = 5
    Me.lbl_promotion_global_limit.TabStop = False
    Me.lbl_promotion_global_limit.TextVisible = False
    Me.lbl_promotion_global_limit.TextWidth = 0
    Me.lbl_promotion_global_limit.Value = "x[] Maximun per promotion, global"
    '
    'ef_promotion_limit_global
    '
    Me.ef_promotion_limit_global.DoubleValue = 0
    Me.ef_promotion_limit_global.IntegerValue = 0
    Me.ef_promotion_limit_global.IsReadOnly = False
    Me.ef_promotion_limit_global.Location = New System.Drawing.Point(299, 6)
    Me.ef_promotion_limit_global.Name = "ef_promotion_limit_global"
    Me.ef_promotion_limit_global.Size = New System.Drawing.Size(200, 24)
    Me.ef_promotion_limit_global.SufixText = "Sufix Text"
    Me.ef_promotion_limit_global.TabIndex = 4
    Me.ef_promotion_limit_global.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_promotion_limit_global.TextValue = ""
    Me.ef_promotion_limit_global.TextVisible = False
    Me.ef_promotion_limit_global.TextWidth = 110
    Me.ef_promotion_limit_global.Value = ""
    '
    'ef_promotion_limit_month
    '
    Me.ef_promotion_limit_month.DoubleValue = 0
    Me.ef_promotion_limit_month.IntegerValue = 0
    Me.ef_promotion_limit_month.IsReadOnly = False
    Me.ef_promotion_limit_month.Location = New System.Drawing.Point(6, 57)
    Me.ef_promotion_limit_month.Name = "ef_promotion_limit_month"
    Me.ef_promotion_limit_month.Size = New System.Drawing.Size(200, 24)
    Me.ef_promotion_limit_month.SufixText = "Sufix Text"
    Me.ef_promotion_limit_month.TabIndex = 2
    Me.ef_promotion_limit_month.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_promotion_limit_month.TextValue = ""
    Me.ef_promotion_limit_month.TextVisible = False
    Me.ef_promotion_limit_month.TextWidth = 110
    Me.ef_promotion_limit_month.Value = ""
    '
    'lbl_promotion_monthly_limit
    '
    Me.lbl_promotion_monthly_limit.AutoSize = True
    Me.lbl_promotion_monthly_limit.IsReadOnly = True
    Me.lbl_promotion_monthly_limit.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_promotion_monthly_limit.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_promotion_monthly_limit.Location = New System.Drawing.Point(5, 81)
    Me.lbl_promotion_monthly_limit.Name = "lbl_promotion_monthly_limit"
    Me.lbl_promotion_monthly_limit.Size = New System.Drawing.Size(411, 24)
    Me.lbl_promotion_monthly_limit.SufixText = "Sufix Text"
    Me.lbl_promotion_monthly_limit.TabIndex = 3
    Me.lbl_promotion_monthly_limit.TabStop = False
    Me.lbl_promotion_monthly_limit.TextVisible = False
    Me.lbl_promotion_monthly_limit.TextWidth = 0
    Me.lbl_promotion_monthly_limit.Value = "x[] Maximum per promotion, per month"
    '
    'ef_promotion_limit_day
    '
    Me.ef_promotion_limit_day.DoubleValue = 0
    Me.ef_promotion_limit_day.IntegerValue = 0
    Me.ef_promotion_limit_day.IsReadOnly = False
    Me.ef_promotion_limit_day.Location = New System.Drawing.Point(6, 6)
    Me.ef_promotion_limit_day.Name = "ef_promotion_limit_day"
    Me.ef_promotion_limit_day.Size = New System.Drawing.Size(200, 24)
    Me.ef_promotion_limit_day.SufixText = "Sufix Text"
    Me.ef_promotion_limit_day.TabIndex = 0
    Me.ef_promotion_limit_day.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_promotion_limit_day.TextValue = ""
    Me.ef_promotion_limit_day.TextVisible = False
    Me.ef_promotion_limit_day.TextWidth = 110
    Me.ef_promotion_limit_day.Value = ""
    '
    'lbl_promotion_daily_limit
    '
    Me.lbl_promotion_daily_limit.AutoSize = True
    Me.lbl_promotion_daily_limit.IsReadOnly = True
    Me.lbl_promotion_daily_limit.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_promotion_daily_limit.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_promotion_daily_limit.Location = New System.Drawing.Point(6, 31)
    Me.lbl_promotion_daily_limit.Name = "lbl_promotion_daily_limit"
    Me.lbl_promotion_daily_limit.Size = New System.Drawing.Size(411, 24)
    Me.lbl_promotion_daily_limit.SufixText = "Sufix Text"
    Me.lbl_promotion_daily_limit.TabIndex = 1
    Me.lbl_promotion_daily_limit.TabStop = False
    Me.lbl_promotion_daily_limit.TextVisible = False
    Me.lbl_promotion_daily_limit.TextWidth = 0
    Me.lbl_promotion_daily_limit.Value = "x[] Maximum per promotion, per day"
    '
    'uc_promo_schedule
    '
    Me.uc_promo_schedule.DateFrom = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_promo_schedule.DateTo = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.uc_promo_schedule.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.uc_promo_schedule.Location = New System.Drawing.Point(4, 145)
    Me.uc_promo_schedule.Name = "uc_promo_schedule"
    Me.uc_promo_schedule.SecondTime = False
    Me.uc_promo_schedule.SecondTimeFrom = 0
    Me.uc_promo_schedule.SecondTimeTo = 0
    Me.uc_promo_schedule.Size = New System.Drawing.Size(410, 239)
    Me.uc_promo_schedule.TabIndex = 5
    Me.uc_promo_schedule.TimeFrom = 0
    Me.uc_promo_schedule.TimeTo = 0
    Me.uc_promo_schedule.Weekday = 127
    '
    'tab_filters
    '
    Me.tab_filters.Controls.Add(Me.tab_gender)
    Me.tab_filters.Controls.Add(Me.tab_birth_date)
    Me.tab_filters.Controls.Add(Me.tab_level)
    Me.tab_filters.Controls.Add(Me.tab_freq)
    Me.tab_filters.Controls.Add(Me.tab_flags_required)
    Me.tab_filters.Location = New System.Drawing.Point(8, 387)
    Me.tab_filters.Name = "tab_filters"
    Me.tab_filters.SelectedIndex = 0
    Me.tab_filters.Size = New System.Drawing.Size(397, 188)
    Me.tab_filters.TabIndex = 6
    '
    'tab_gender
    '
    Me.tab_gender.Controls.Add(Me.opt_gender_male)
    Me.tab_gender.Controls.Add(Me.opt_gender_female)
    Me.tab_gender.Controls.Add(Me.chk_by_gender)
    Me.tab_gender.Location = New System.Drawing.Point(4, 22)
    Me.tab_gender.Name = "tab_gender"
    Me.tab_gender.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_gender.Size = New System.Drawing.Size(389, 162)
    Me.tab_gender.TabIndex = 0
    Me.tab_gender.Text = "xBy Gender"
    Me.tab_gender.UseVisualStyleBackColor = True
    '
    'opt_gender_male
    '
    Me.opt_gender_male.Location = New System.Drawing.Point(26, 29)
    Me.opt_gender_male.Name = "opt_gender_male"
    Me.opt_gender_male.Size = New System.Drawing.Size(314, 17)
    Me.opt_gender_male.TabIndex = 1
    Me.opt_gender_male.Text = "xApplies to Men Only"
    '
    'opt_gender_female
    '
    Me.opt_gender_female.Location = New System.Drawing.Point(26, 52)
    Me.opt_gender_female.Name = "opt_gender_female"
    Me.opt_gender_female.Size = New System.Drawing.Size(314, 17)
    Me.opt_gender_female.TabIndex = 2
    Me.opt_gender_female.Text = "xApplies to Women Only"
    '
    'chk_by_gender
    '
    Me.chk_by_gender.AutoSize = True
    Me.chk_by_gender.Location = New System.Drawing.Point(6, 6)
    Me.chk_by_gender.Name = "chk_by_gender"
    Me.chk_by_gender.Size = New System.Drawing.Size(68, 17)
    Me.chk_by_gender.TabIndex = 0
    Me.chk_by_gender.Text = "xActive"
    Me.chk_by_gender.UseVisualStyleBackColor = True
    '
    'tab_birth_date
    '
    Me.tab_birth_date.Controls.Add(Me.opt_birth_date_only)
    Me.tab_birth_date.Controls.Add(Me.opt_birth_month_only)
    Me.tab_birth_date.Controls.Add(Me.chk_by_birth_date)
    Me.tab_birth_date.Location = New System.Drawing.Point(4, 22)
    Me.tab_birth_date.Name = "tab_birth_date"
    Me.tab_birth_date.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_birth_date.Size = New System.Drawing.Size(389, 162)
    Me.tab_birth_date.TabIndex = 1
    Me.tab_birth_date.Text = "xBy Birth Date"
    Me.tab_birth_date.UseVisualStyleBackColor = True
    '
    'opt_birth_date_only
    '
    Me.opt_birth_date_only.Location = New System.Drawing.Point(26, 52)
    Me.opt_birth_date_only.Name = "opt_birth_date_only"
    Me.opt_birth_date_only.Size = New System.Drawing.Size(344, 17)
    Me.opt_birth_date_only.TabIndex = 2
    Me.opt_birth_date_only.Text = "xBirth day and Month"
    '
    'opt_birth_month_only
    '
    Me.opt_birth_month_only.Location = New System.Drawing.Point(26, 29)
    Me.opt_birth_month_only.Name = "opt_birth_month_only"
    Me.opt_birth_month_only.Size = New System.Drawing.Size(344, 17)
    Me.opt_birth_month_only.TabIndex = 1
    Me.opt_birth_month_only.Text = "xAny day within birthday's month"
    '
    'chk_by_birth_date
    '
    Me.chk_by_birth_date.AutoSize = True
    Me.chk_by_birth_date.Location = New System.Drawing.Point(6, 6)
    Me.chk_by_birth_date.Name = "chk_by_birth_date"
    Me.chk_by_birth_date.Size = New System.Drawing.Size(68, 17)
    Me.chk_by_birth_date.TabIndex = 0
    Me.chk_by_birth_date.Text = "xActive"
    Me.chk_by_birth_date.UseVisualStyleBackColor = True
    '
    'tab_level
    '
    Me.tab_level.Controls.Add(Me.chk_level_anonymous)
    Me.tab_level.Controls.Add(Me.chk_level_04)
    Me.tab_level.Controls.Add(Me.chk_level_03)
    Me.tab_level.Controls.Add(Me.chk_level_02)
    Me.tab_level.Controls.Add(Me.chk_level_01)
    Me.tab_level.Controls.Add(Me.chk_by_level)
    Me.tab_level.Location = New System.Drawing.Point(4, 22)
    Me.tab_level.Name = "tab_level"
    Me.tab_level.Size = New System.Drawing.Size(389, 162)
    Me.tab_level.TabIndex = 2
    Me.tab_level.Text = "xBy Level"
    Me.tab_level.UseVisualStyleBackColor = True
    '
    'chk_level_anonymous
    '
    Me.chk_level_anonymous.AutoSize = True
    Me.chk_level_anonymous.Location = New System.Drawing.Point(26, 29)
    Me.chk_level_anonymous.Name = "chk_level_anonymous"
    Me.chk_level_anonymous.Size = New System.Drawing.Size(100, 17)
    Me.chk_level_anonymous.TabIndex = 1
    Me.chk_level_anonymous.Text = "xAnonymous"
    Me.chk_level_anonymous.UseVisualStyleBackColor = True
    '
    'chk_level_04
    '
    Me.chk_level_04.AutoSize = True
    Me.chk_level_04.Location = New System.Drawing.Point(26, 121)
    Me.chk_level_04.Name = "chk_level_04"
    Me.chk_level_04.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_04.TabIndex = 5
    Me.chk_level_04.Text = "xLevel 04"
    Me.chk_level_04.UseVisualStyleBackColor = True
    '
    'chk_level_03
    '
    Me.chk_level_03.AutoSize = True
    Me.chk_level_03.Location = New System.Drawing.Point(26, 98)
    Me.chk_level_03.Name = "chk_level_03"
    Me.chk_level_03.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_03.TabIndex = 4
    Me.chk_level_03.Text = "xLevel 03"
    Me.chk_level_03.UseVisualStyleBackColor = True
    '
    'chk_level_02
    '
    Me.chk_level_02.AutoSize = True
    Me.chk_level_02.Location = New System.Drawing.Point(26, 75)
    Me.chk_level_02.Name = "chk_level_02"
    Me.chk_level_02.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_02.TabIndex = 3
    Me.chk_level_02.Text = "xLevel 02"
    Me.chk_level_02.UseVisualStyleBackColor = True
    '
    'chk_level_01
    '
    Me.chk_level_01.AutoSize = True
    Me.chk_level_01.Location = New System.Drawing.Point(26, 52)
    Me.chk_level_01.Name = "chk_level_01"
    Me.chk_level_01.Size = New System.Drawing.Size(81, 17)
    Me.chk_level_01.TabIndex = 2
    Me.chk_level_01.Text = "xLevel 01"
    Me.chk_level_01.UseVisualStyleBackColor = True
    '
    'chk_by_level
    '
    Me.chk_by_level.AutoSize = True
    Me.chk_by_level.Location = New System.Drawing.Point(6, 6)
    Me.chk_by_level.Name = "chk_by_level"
    Me.chk_by_level.Size = New System.Drawing.Size(68, 17)
    Me.chk_by_level.TabIndex = 0
    Me.chk_by_level.Text = "xActive"
    Me.chk_by_level.UseVisualStyleBackColor = True
    '
    'tab_freq
    '
    Me.tab_freq.Controls.Add(Me.lbl_freq_filter_02)
    Me.tab_freq.Controls.Add(Me.lbl_freq_filter_01)
    Me.tab_freq.Controls.Add(Me.ef_freq_min_cash_in)
    Me.tab_freq.Controls.Add(Me.ef_freq_min_days)
    Me.tab_freq.Controls.Add(Me.ef_freq_last_days)
    Me.tab_freq.Controls.Add(Me.chk_by_freq)
    Me.tab_freq.Location = New System.Drawing.Point(4, 22)
    Me.tab_freq.Name = "tab_freq"
    Me.tab_freq.Size = New System.Drawing.Size(389, 162)
    Me.tab_freq.TabIndex = 3
    Me.tab_freq.Text = "xBy Freq"
    Me.tab_freq.UseVisualStyleBackColor = True
    '
    'lbl_freq_filter_02
    '
    Me.lbl_freq_filter_02.AutoSize = True
    Me.lbl_freq_filter_02.IsReadOnly = True
    Me.lbl_freq_filter_02.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_freq_filter_02.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_freq_filter_02.Location = New System.Drawing.Point(9, 134)
    Me.lbl_freq_filter_02.Name = "lbl_freq_filter_02"
    Me.lbl_freq_filter_02.Size = New System.Drawing.Size(364, 24)
    Me.lbl_freq_filter_02.SufixText = "Sufix Text"
    Me.lbl_freq_filter_02.TabIndex = 5
    Me.lbl_freq_filter_02.TabStop = False
    Me.lbl_freq_filter_02.TextVisible = False
    Me.lbl_freq_filter_02.TextWidth = 0
    Me.lbl_freq_filter_02.Value = "xrecharging minimum [] per day"
    '
    'lbl_freq_filter_01
    '
    Me.lbl_freq_filter_01.AutoSize = True
    Me.lbl_freq_filter_01.IsReadOnly = True
    Me.lbl_freq_filter_01.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_freq_filter_01.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_freq_filter_01.Location = New System.Drawing.Point(9, 110)
    Me.lbl_freq_filter_01.Name = "lbl_freq_filter_01"
    Me.lbl_freq_filter_01.Size = New System.Drawing.Size(364, 24)
    Me.lbl_freq_filter_01.SufixText = "Sufix Text"
    Me.lbl_freq_filter_01.TabIndex = 4
    Me.lbl_freq_filter_01.TabStop = False
    Me.lbl_freq_filter_01.TextVisible = False
    Me.lbl_freq_filter_01.TextWidth = 0
    Me.lbl_freq_filter_01.Value = "xIn the last [] days, he has come at least [] days"
    '
    'ef_freq_min_cash_in
    '
    Me.ef_freq_min_cash_in.DoubleValue = 0
    Me.ef_freq_min_cash_in.IntegerValue = 0
    Me.ef_freq_min_cash_in.IsReadOnly = False
    Me.ef_freq_min_cash_in.Location = New System.Drawing.Point(9, 81)
    Me.ef_freq_min_cash_in.Name = "ef_freq_min_cash_in"
    Me.ef_freq_min_cash_in.Size = New System.Drawing.Size(200, 24)
    Me.ef_freq_min_cash_in.SufixText = "Sufix Text"
    Me.ef_freq_min_cash_in.TabIndex = 3
    Me.ef_freq_min_cash_in.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_freq_min_cash_in.TextValue = ""
    Me.ef_freq_min_cash_in.TextVisible = False
    Me.ef_freq_min_cash_in.TextWidth = 110
    Me.ef_freq_min_cash_in.Value = ""
    '
    'ef_freq_min_days
    '
    Me.ef_freq_min_days.DoubleValue = 0
    Me.ef_freq_min_days.IntegerValue = 0
    Me.ef_freq_min_days.IsReadOnly = False
    Me.ef_freq_min_days.Location = New System.Drawing.Point(9, 54)
    Me.ef_freq_min_days.Name = "ef_freq_min_days"
    Me.ef_freq_min_days.Size = New System.Drawing.Size(150, 24)
    Me.ef_freq_min_days.SufixText = "Sufix Text"
    Me.ef_freq_min_days.TabIndex = 2
    Me.ef_freq_min_days.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_freq_min_days.TextValue = ""
    Me.ef_freq_min_days.TextVisible = False
    Me.ef_freq_min_days.TextWidth = 110
    Me.ef_freq_min_days.Value = ""
    '
    'ef_freq_last_days
    '
    Me.ef_freq_last_days.DoubleValue = 0
    Me.ef_freq_last_days.IntegerValue = 0
    Me.ef_freq_last_days.IsReadOnly = False
    Me.ef_freq_last_days.Location = New System.Drawing.Point(9, 26)
    Me.ef_freq_last_days.Name = "ef_freq_last_days"
    Me.ef_freq_last_days.Size = New System.Drawing.Size(150, 24)
    Me.ef_freq_last_days.SufixText = "Sufix Text"
    Me.ef_freq_last_days.TabIndex = 1
    Me.ef_freq_last_days.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_freq_last_days.TextValue = ""
    Me.ef_freq_last_days.TextVisible = False
    Me.ef_freq_last_days.TextWidth = 110
    Me.ef_freq_last_days.Value = ""
    '
    'chk_by_freq
    '
    Me.chk_by_freq.AutoSize = True
    Me.chk_by_freq.Location = New System.Drawing.Point(6, 6)
    Me.chk_by_freq.Name = "chk_by_freq"
    Me.chk_by_freq.Size = New System.Drawing.Size(68, 17)
    Me.chk_by_freq.TabIndex = 0
    Me.chk_by_freq.Text = "xActive"
    Me.chk_by_freq.UseVisualStyleBackColor = True
    '
    'tab_flags_required
    '
    Me.tab_flags_required.Controls.Add(Me.dg_flags_required)
    Me.tab_flags_required.Controls.Add(Me.chk_by_flag)
    Me.tab_flags_required.Location = New System.Drawing.Point(4, 22)
    Me.tab_flags_required.Name = "tab_flags_required"
    Me.tab_flags_required.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_flags_required.Size = New System.Drawing.Size(389, 162)
    Me.tab_flags_required.TabIndex = 4
    Me.tab_flags_required.Text = "xBy Flags"
    Me.tab_flags_required.UseVisualStyleBackColor = True
    '
    'dg_flags_required
    '
    Me.dg_flags_required.CurrentCol = -1
    Me.dg_flags_required.CurrentRow = -1
    Me.dg_flags_required.Location = New System.Drawing.Point(26, 29)
    Me.dg_flags_required.Name = "dg_flags_required"
    Me.dg_flags_required.PanelRightVisible = False
    Me.dg_flags_required.Redraw = True
    Me.dg_flags_required.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_flags_required.Size = New System.Drawing.Size(342, 116)
    Me.dg_flags_required.Sortable = False
    Me.dg_flags_required.SortAscending = True
    Me.dg_flags_required.SortByCol = 0
    Me.dg_flags_required.TabIndex = 3
    Me.dg_flags_required.ToolTipped = True
    Me.dg_flags_required.TopRow = -2
    '
    'chk_by_flag
    '
    Me.chk_by_flag.AutoSize = True
    Me.chk_by_flag.Location = New System.Drawing.Point(6, 6)
    Me.chk_by_flag.Name = "chk_by_flag"
    Me.chk_by_flag.Size = New System.Drawing.Size(68, 17)
    Me.chk_by_flag.TabIndex = 0
    Me.chk_by_flag.Text = "xActive"
    Me.chk_by_flag.UseVisualStyleBackColor = True
    '
    'gb_test
    '
    Me.gb_test.Controls.Add(Me.ef_test_spent)
    Me.gb_test.Controls.Add(Me.lbl_test_result)
    Me.gb_test.Controls.Add(Me.ef_test_num_tokens)
    Me.gb_test.Controls.Add(Me.ef_test_cash_in)
    Me.gb_test.Location = New System.Drawing.Point(416, 625)
    Me.gb_test.Name = "gb_test"
    Me.gb_test.Size = New System.Drawing.Size(513, 73)
    Me.gb_test.TabIndex = 19
    Me.gb_test.TabStop = False
    Me.gb_test.Text = "xTest"
    '
    'ef_test_spent
    '
    Me.ef_test_spent.DoubleValue = 0
    Me.ef_test_spent.IntegerValue = 0
    Me.ef_test_spent.IsReadOnly = False
    Me.ef_test_spent.Location = New System.Drawing.Point(182, 19)
    Me.ef_test_spent.Name = "ef_test_spent"
    Me.ef_test_spent.Size = New System.Drawing.Size(170, 24)
    Me.ef_test_spent.SufixText = "Sufix Text"
    Me.ef_test_spent.SufixTextVisible = True
    Me.ef_test_spent.TabIndex = 1
    Me.ef_test_spent.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_test_spent.TextValue = ""
    Me.ef_test_spent.Value = ""
    '
    'lbl_test_result
    '
    Me.lbl_test_result.AutoSize = True
    Me.lbl_test_result.IsReadOnly = True
    Me.lbl_test_result.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_test_result.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_test_result.Location = New System.Drawing.Point(14, 42)
    Me.lbl_test_result.Name = "lbl_test_result"
    Me.lbl_test_result.Size = New System.Drawing.Size(350, 24)
    Me.lbl_test_result.SufixText = "Sufix Text"
    Me.lbl_test_result.SufixTextVisible = True
    Me.lbl_test_result.TabIndex = 3
    Me.lbl_test_result.TabStop = False
    Me.lbl_test_result.TextWidth = 0
    Me.lbl_test_result.Value = "xUser gets [] non_redeemable"
    '
    'ef_test_num_tokens
    '
    Me.ef_test_num_tokens.DoubleValue = 0
    Me.ef_test_num_tokens.IntegerValue = 0
    Me.ef_test_num_tokens.IsReadOnly = False
    Me.ef_test_num_tokens.Location = New System.Drawing.Point(358, 19)
    Me.ef_test_num_tokens.Name = "ef_test_num_tokens"
    Me.ef_test_num_tokens.Size = New System.Drawing.Size(120, 24)
    Me.ef_test_num_tokens.SufixText = "Sufix Text"
    Me.ef_test_num_tokens.SufixTextVisible = True
    Me.ef_test_num_tokens.TabIndex = 2
    Me.ef_test_num_tokens.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_test_num_tokens.TextValue = ""
    Me.ef_test_num_tokens.Value = ""
    '
    'ef_test_cash_in
    '
    Me.ef_test_cash_in.DoubleValue = 0
    Me.ef_test_cash_in.IntegerValue = 0
    Me.ef_test_cash_in.IsReadOnly = False
    Me.ef_test_cash_in.Location = New System.Drawing.Point(6, 19)
    Me.ef_test_cash_in.Name = "ef_test_cash_in"
    Me.ef_test_cash_in.Size = New System.Drawing.Size(170, 24)
    Me.ef_test_cash_in.SufixText = "Sufix Text"
    Me.ef_test_cash_in.SufixTextVisible = True
    Me.ef_test_cash_in.TabIndex = 0
    Me.ef_test_cash_in.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_test_cash_in.TextValue = ""
    Me.ef_test_cash_in.Value = ""
    '
    'Timer1
    '
    '
    'chk_special_permission
    '
    Me.chk_special_permission.AutoSize = True
    Me.chk_special_permission.Location = New System.Drawing.Point(6, 20)
    Me.chk_special_permission.Name = "chk_special_permission"
    Me.chk_special_permission.Size = New System.Drawing.Size(68, 17)
    Me.chk_special_permission.TabIndex = 0
    Me.chk_special_permission.Text = "xActive"
    Me.chk_special_permission.UseVisualStyleBackColor = True
    '
    'gb_special_permission
    '
    Me.gb_special_permission.Controls.Add(Me.lbl_not_applicable_mb1)
    Me.gb_special_permission.Controls.Add(Me.opt_special_permission_b)
    Me.gb_special_permission.Controls.Add(Me.opt_special_permission_a)
    Me.gb_special_permission.Controls.Add(Me.chk_special_permission)
    Me.gb_special_permission.Location = New System.Drawing.Point(416, 15)
    Me.gb_special_permission.Name = "gb_special_permission"
    Me.gb_special_permission.Size = New System.Drawing.Size(188, 141)
    Me.gb_special_permission.TabIndex = 8
    Me.gb_special_permission.TabStop = False
    Me.gb_special_permission.Text = "xSpecial Permission"
    '
    'lbl_not_applicable_mb1
    '
    Me.lbl_not_applicable_mb1.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_not_applicable_mb1.ForeColor = System.Drawing.Color.Red
    Me.lbl_not_applicable_mb1.Location = New System.Drawing.Point(5, 88)
    Me.lbl_not_applicable_mb1.Name = "lbl_not_applicable_mb1"
    Me.lbl_not_applicable_mb1.Size = New System.Drawing.Size(179, 49)
    Me.lbl_not_applicable_mb1.TabIndex = 14
    Me.lbl_not_applicable_mb1.Text = "xNot applicable via " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Mobile Bank"
    Me.lbl_not_applicable_mb1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'opt_special_permission_b
    '
    Me.opt_special_permission_b.Location = New System.Drawing.Point(26, 65)
    Me.opt_special_permission_b.Name = "opt_special_permission_b"
    Me.opt_special_permission_b.Size = New System.Drawing.Size(116, 16)
    Me.opt_special_permission_b.TabIndex = 2
    Me.opt_special_permission_b.Text = "xPermission B"
    '
    'opt_special_permission_a
    '
    Me.opt_special_permission_a.Location = New System.Drawing.Point(26, 43)
    Me.opt_special_permission_a.Name = "opt_special_permission_a"
    Me.opt_special_permission_a.Size = New System.Drawing.Size(116, 16)
    Me.opt_special_permission_a.TabIndex = 0
    Me.opt_special_permission_a.Text = "xPermission A"
    '
    'ef_min_cash_in
    '
    Me.ef_min_cash_in.DoubleValue = 0
    Me.ef_min_cash_in.IntegerValue = 0
    Me.ef_min_cash_in.IsReadOnly = False
    Me.ef_min_cash_in.Location = New System.Drawing.Point(6, 6)
    Me.ef_min_cash_in.Name = "ef_min_cash_in"
    Me.ef_min_cash_in.Size = New System.Drawing.Size(200, 24)
    Me.ef_min_cash_in.SufixText = "Sufix Text"
    Me.ef_min_cash_in.SufixTextVisible = True
    Me.ef_min_cash_in.TabIndex = 0
    Me.ef_min_cash_in.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_min_cash_in.TextValue = ""
    Me.ef_min_cash_in.TextVisible = False
    Me.ef_min_cash_in.TextWidth = 110
    Me.ef_min_cash_in.Value = ""
    '
    'ef_min_cash_in_reward
    '
    Me.ef_min_cash_in_reward.DoubleValue = 0
    Me.ef_min_cash_in_reward.IntegerValue = 0
    Me.ef_min_cash_in_reward.IsReadOnly = False
    Me.ef_min_cash_in_reward.Location = New System.Drawing.Point(217, 6)
    Me.ef_min_cash_in_reward.Name = "ef_min_cash_in_reward"
    Me.ef_min_cash_in_reward.OnlyUpperCase = True
    Me.ef_min_cash_in_reward.Size = New System.Drawing.Size(200, 24)
    Me.ef_min_cash_in_reward.SufixText = "Sufix Text"
    Me.ef_min_cash_in_reward.SufixTextVisible = True
    Me.ef_min_cash_in_reward.TabIndex = 1
    Me.ef_min_cash_in_reward.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_min_cash_in_reward.TextValue = ""
    Me.ef_min_cash_in_reward.TextVisible = False
    Me.ef_min_cash_in_reward.TextWidth = 110
    Me.ef_min_cash_in_reward.Value = ""
    '
    'lbl_cash_in_promotion
    '
    Me.lbl_cash_in_promotion.AutoSize = True
    Me.lbl_cash_in_promotion.IsReadOnly = True
    Me.lbl_cash_in_promotion.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_cash_in_promotion.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_cash_in_promotion.Location = New System.Drawing.Point(6, 84)
    Me.lbl_cash_in_promotion.Name = "lbl_cash_in_promotion"
    Me.lbl_cash_in_promotion.Size = New System.Drawing.Size(477, 24)
    Me.lbl_cash_in_promotion.SufixText = "Sufix Text"
    Me.lbl_cash_in_promotion.SufixTextVisible = True
    Me.lbl_cash_in_promotion.TabIndex = 4
    Me.lbl_cash_in_promotion.TabStop = False
    Me.lbl_cash_in_promotion.TextVisible = False
    Me.lbl_cash_in_promotion.TextWidth = 0
    Me.lbl_cash_in_promotion.Value = "xUser gets [] non-redeemable for every [] cashed in"
    '
    'lbl_min_cash_in_promotion
    '
    Me.lbl_min_cash_in_promotion.AutoSize = True
    Me.lbl_min_cash_in_promotion.IsReadOnly = True
    Me.lbl_min_cash_in_promotion.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_min_cash_in_promotion.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_min_cash_in_promotion.Location = New System.Drawing.Point(6, 30)
    Me.lbl_min_cash_in_promotion.Name = "lbl_min_cash_in_promotion"
    Me.lbl_min_cash_in_promotion.Size = New System.Drawing.Size(477, 24)
    Me.lbl_min_cash_in_promotion.SufixText = "Sufix Text"
    Me.lbl_min_cash_in_promotion.SufixTextVisible = True
    Me.lbl_min_cash_in_promotion.TabIndex = 2
    Me.lbl_min_cash_in_promotion.TabStop = False
    Me.lbl_min_cash_in_promotion.TextVisible = False
    Me.lbl_min_cash_in_promotion.TextWidth = 0
    Me.lbl_min_cash_in_promotion.Value = "xUser gets [] non-redeemable for the minimum cash in of []"
    '
    'ef_cash_in_reward
    '
    Me.ef_cash_in_reward.DoubleValue = 0
    Me.ef_cash_in_reward.IntegerValue = 0
    Me.ef_cash_in_reward.IsReadOnly = False
    Me.ef_cash_in_reward.Location = New System.Drawing.Point(217, 60)
    Me.ef_cash_in_reward.Name = "ef_cash_in_reward"
    Me.ef_cash_in_reward.OnlyUpperCase = True
    Me.ef_cash_in_reward.Size = New System.Drawing.Size(200, 24)
    Me.ef_cash_in_reward.SufixText = "Sufix Text"
    Me.ef_cash_in_reward.SufixTextVisible = True
    Me.ef_cash_in_reward.TabIndex = 5
    Me.ef_cash_in_reward.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cash_in_reward.TextValue = ""
    Me.ef_cash_in_reward.TextVisible = False
    Me.ef_cash_in_reward.TextWidth = 110
    Me.ef_cash_in_reward.Value = ""
    '
    'ef_cash_in
    '
    Me.ef_cash_in.DoubleValue = 0
    Me.ef_cash_in.IntegerValue = 0
    Me.ef_cash_in.IsReadOnly = False
    Me.ef_cash_in.Location = New System.Drawing.Point(5, 60)
    Me.ef_cash_in.Name = "ef_cash_in"
    Me.ef_cash_in.Size = New System.Drawing.Size(200, 24)
    Me.ef_cash_in.SufixText = "Sufix Text"
    Me.ef_cash_in.SufixTextVisible = True
    Me.ef_cash_in.TabIndex = 3
    Me.ef_cash_in.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_cash_in.TextValue = ""
    Me.ef_cash_in.TextVisible = False
    Me.ef_cash_in.TextWidth = 110
    Me.ef_cash_in.Value = ""
    '
    'gb_credit_type
    '
    Me.gb_credit_type.Controls.Add(Me.opt_point)
    Me.gb_credit_type.Controls.Add(Me.opt_credit_redeemable)
    Me.gb_credit_type.Controls.Add(Me.opt_credit_non_redeemable)
    Me.gb_credit_type.Location = New System.Drawing.Point(8, 58)
    Me.gb_credit_type.Name = "gb_credit_type"
    Me.gb_credit_type.Size = New System.Drawing.Size(197, 90)
    Me.gb_credit_type.TabIndex = 2
    Me.gb_credit_type.TabStop = False
    Me.gb_credit_type.Text = "xType"
    '
    'opt_point
    '
    Me.opt_point.Location = New System.Drawing.Point(15, 63)
    Me.opt_point.Name = "opt_point"
    Me.opt_point.Size = New System.Drawing.Size(135, 16)
    Me.opt_point.TabIndex = 3
    Me.opt_point.Text = "xPoint"
    '
    'opt_credit_redeemable
    '
    Me.opt_credit_redeemable.Location = New System.Drawing.Point(15, 40)
    Me.opt_credit_redeemable.Name = "opt_credit_redeemable"
    Me.opt_credit_redeemable.Size = New System.Drawing.Size(135, 16)
    Me.opt_credit_redeemable.TabIndex = 2
    Me.opt_credit_redeemable.Text = "xRedeemable"
    '
    'opt_credit_non_redeemable
    '
    Me.opt_credit_non_redeemable.Location = New System.Drawing.Point(15, 18)
    Me.opt_credit_non_redeemable.Name = "opt_credit_non_redeemable"
    Me.opt_credit_non_redeemable.Size = New System.Drawing.Size(135, 16)
    Me.opt_credit_non_redeemable.TabIndex = 1
    Me.opt_credit_non_redeemable.Text = "xNon-Redeemable"
    '
    'tabPromotions
    '
    Me.tabPromotions.Controls.Add(Me.tab_chash_in)
    Me.tabPromotions.Controls.Add(Me.tab_spent)
    Me.tabPromotions.Controls.Add(Me.tab_tokens)
    Me.tabPromotions.Controls.Add(Me.tab_flags_awarded)
    Me.tabPromotions.Location = New System.Drawing.Point(416, 209)
    Me.tabPromotions.Name = "tabPromotions"
    Me.tabPromotions.SelectedIndex = 0
    Me.tabPromotions.Size = New System.Drawing.Size(513, 193)
    Me.tabPromotions.TabIndex = 15
    '
    'tab_chash_in
    '
    Me.tab_chash_in.Controls.Add(Me.ef_min_cash_in)
    Me.tab_chash_in.Controls.Add(Me.ef_min_cash_in_reward)
    Me.tab_chash_in.Controls.Add(Me.lbl_min_cash_in_promotion)
    Me.tab_chash_in.Controls.Add(Me.ef_cash_in)
    Me.tab_chash_in.Controls.Add(Me.ef_cash_in_reward)
    Me.tab_chash_in.Controls.Add(Me.lbl_cash_in_promotion)
    Me.tab_chash_in.Location = New System.Drawing.Point(4, 22)
    Me.tab_chash_in.Name = "tab_chash_in"
    Me.tab_chash_in.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_chash_in.Size = New System.Drawing.Size(505, 167)
    Me.tab_chash_in.TabIndex = 0
    Me.tab_chash_in.Text = "xChasIn"
    Me.tab_chash_in.UseVisualStyleBackColor = True
    '
    'tab_spent
    '
    Me.tab_spent.Controls.Add(Me.ef_min_spent)
    Me.tab_spent.Controls.Add(Me.ef_min_spent_reward)
    Me.tab_spent.Controls.Add(Me.ef_spent)
    Me.tab_spent.Controls.Add(Me.lbl_spent_promotion)
    Me.tab_spent.Controls.Add(Me.ef_spent_reward)
    Me.tab_spent.Controls.Add(Me.lbl_min_spent_promotion)
    Me.tab_spent.Location = New System.Drawing.Point(4, 22)
    Me.tab_spent.Name = "tab_spent"
    Me.tab_spent.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_spent.Size = New System.Drawing.Size(505, 167)
    Me.tab_spent.TabIndex = 1
    Me.tab_spent.Text = "xSpent"
    Me.tab_spent.UseVisualStyleBackColor = True
    '
    'ef_min_spent
    '
    Me.ef_min_spent.DoubleValue = 0
    Me.ef_min_spent.IntegerValue = 0
    Me.ef_min_spent.IsReadOnly = False
    Me.ef_min_spent.Location = New System.Drawing.Point(6, 6)
    Me.ef_min_spent.Name = "ef_min_spent"
    Me.ef_min_spent.Size = New System.Drawing.Size(200, 24)
    Me.ef_min_spent.SufixText = "Sufix Text"
    Me.ef_min_spent.TabIndex = 6
    Me.ef_min_spent.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_min_spent.TextValue = ""
    Me.ef_min_spent.TextVisible = False
    Me.ef_min_spent.TextWidth = 110
    Me.ef_min_spent.Value = ""
    '
    'ef_min_spent_reward
    '
    Me.ef_min_spent_reward.DoubleValue = 0
    Me.ef_min_spent_reward.IntegerValue = 0
    Me.ef_min_spent_reward.IsReadOnly = False
    Me.ef_min_spent_reward.Location = New System.Drawing.Point(217, 6)
    Me.ef_min_spent_reward.Name = "ef_min_spent_reward"
    Me.ef_min_spent_reward.OnlyUpperCase = True
    Me.ef_min_spent_reward.Size = New System.Drawing.Size(200, 24)
    Me.ef_min_spent_reward.SufixText = "Sufix Text"
    Me.ef_min_spent_reward.TabIndex = 7
    Me.ef_min_spent_reward.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_min_spent_reward.TextValue = ""
    Me.ef_min_spent_reward.TextVisible = False
    Me.ef_min_spent_reward.TextWidth = 110
    Me.ef_min_spent_reward.Value = ""
    '
    'ef_spent
    '
    Me.ef_spent.DoubleValue = 0
    Me.ef_spent.IntegerValue = 0
    Me.ef_spent.IsReadOnly = False
    Me.ef_spent.Location = New System.Drawing.Point(5, 60)
    Me.ef_spent.Name = "ef_spent"
    Me.ef_spent.Size = New System.Drawing.Size(200, 24)
    Me.ef_spent.SufixText = "Sufix Text"
    Me.ef_spent.TabIndex = 9
    Me.ef_spent.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_spent.TextValue = ""
    Me.ef_spent.TextVisible = False
    Me.ef_spent.TextWidth = 110
    Me.ef_spent.Value = ""
    '
    'lbl_spent_promotion
    '
    Me.lbl_spent_promotion.AutoSize = True
    Me.lbl_spent_promotion.IsReadOnly = True
    Me.lbl_spent_promotion.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_spent_promotion.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_spent_promotion.Location = New System.Drawing.Point(6, 84)
    Me.lbl_spent_promotion.Name = "lbl_spent_promotion"
    Me.lbl_spent_promotion.Size = New System.Drawing.Size(477, 24)
    Me.lbl_spent_promotion.SufixText = "Sufix Text"
    Me.lbl_spent_promotion.TabIndex = 11
    Me.lbl_spent_promotion.TabStop = False
    Me.lbl_spent_promotion.TextVisible = False
    Me.lbl_spent_promotion.TextWidth = 0
    Me.lbl_spent_promotion.Value = "xUser gets [] non-redeemable for every [] spent"
    '
    'ef_spent_reward
    '
    Me.ef_spent_reward.DoubleValue = 0
    Me.ef_spent_reward.IntegerValue = 0
    Me.ef_spent_reward.IsReadOnly = False
    Me.ef_spent_reward.Location = New System.Drawing.Point(217, 60)
    Me.ef_spent_reward.Name = "ef_spent_reward"
    Me.ef_spent_reward.OnlyUpperCase = True
    Me.ef_spent_reward.Size = New System.Drawing.Size(200, 24)
    Me.ef_spent_reward.SufixText = "Sufix Text"
    Me.ef_spent_reward.TabIndex = 10
    Me.ef_spent_reward.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_spent_reward.TextValue = ""
    Me.ef_spent_reward.TextVisible = False
    Me.ef_spent_reward.TextWidth = 110
    Me.ef_spent_reward.Value = ""
    '
    'lbl_min_spent_promotion
    '
    Me.lbl_min_spent_promotion.AutoSize = True
    Me.lbl_min_spent_promotion.IsReadOnly = True
    Me.lbl_min_spent_promotion.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_min_spent_promotion.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_min_spent_promotion.Location = New System.Drawing.Point(6, 30)
    Me.lbl_min_spent_promotion.Name = "lbl_min_spent_promotion"
    Me.lbl_min_spent_promotion.Size = New System.Drawing.Size(477, 24)
    Me.lbl_min_spent_promotion.SufixText = "Sufix Text"
    Me.lbl_min_spent_promotion.TabIndex = 8
    Me.lbl_min_spent_promotion.TabStop = False
    Me.lbl_min_spent_promotion.TextVisible = False
    Me.lbl_min_spent_promotion.TextWidth = 0
    Me.lbl_min_spent_promotion.Value = "xUser gets [] non-redeemable for the minimum spent of []"
    '
    'tab_tokens
    '
    Me.tab_tokens.Controls.Add(Me.ef_num_tokens)
    Me.tab_tokens.Controls.Add(Me.lbl_token_reward)
    Me.tab_tokens.Controls.Add(Me.ef_token_reward)
    Me.tab_tokens.Controls.Add(Me.ef_token_name)
    Me.tab_tokens.Location = New System.Drawing.Point(4, 22)
    Me.tab_tokens.Name = "tab_tokens"
    Me.tab_tokens.Size = New System.Drawing.Size(505, 167)
    Me.tab_tokens.TabIndex = 2
    Me.tab_tokens.Text = "xTokens"
    Me.tab_tokens.UseVisualStyleBackColor = True
    '
    'ef_num_tokens
    '
    Me.ef_num_tokens.DoubleValue = 0
    Me.ef_num_tokens.IntegerValue = 0
    Me.ef_num_tokens.IsReadOnly = False
    Me.ef_num_tokens.Location = New System.Drawing.Point(6, 6)
    Me.ef_num_tokens.Name = "ef_num_tokens"
    Me.ef_num_tokens.Size = New System.Drawing.Size(150, 24)
    Me.ef_num_tokens.SufixText = "Sufix Text"
    Me.ef_num_tokens.TabIndex = 4
    Me.ef_num_tokens.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_num_tokens.TextValue = ""
    Me.ef_num_tokens.TextVisible = False
    Me.ef_num_tokens.TextWidth = 110
    Me.ef_num_tokens.Value = ""
    '
    'lbl_token_reward
    '
    Me.lbl_token_reward.AutoSize = True
    Me.lbl_token_reward.IsReadOnly = True
    Me.lbl_token_reward.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_token_reward.LabelForeColor = System.Drawing.SystemColors.HotTrack
    Me.lbl_token_reward.Location = New System.Drawing.Point(3, 71)
    Me.lbl_token_reward.Name = "lbl_token_reward"
    Me.lbl_token_reward.Size = New System.Drawing.Size(477, 24)
    Me.lbl_token_reward.SufixText = "Sufix Text"
    Me.lbl_token_reward.TabIndex = 7
    Me.lbl_token_reward.TabStop = False
    Me.lbl_token_reward.TextVisible = False
    Me.lbl_token_reward.TextWidth = 0
    Me.lbl_token_reward.Value = "xUser gets [] non-redeemable for every [] [token name]"
    '
    'ef_token_reward
    '
    Me.ef_token_reward.DoubleValue = 0
    Me.ef_token_reward.IntegerValue = 0
    Me.ef_token_reward.IsReadOnly = False
    Me.ef_token_reward.Location = New System.Drawing.Point(217, 6)
    Me.ef_token_reward.Name = "ef_token_reward"
    Me.ef_token_reward.OnlyUpperCase = True
    Me.ef_token_reward.Size = New System.Drawing.Size(200, 24)
    Me.ef_token_reward.SufixText = "Sufix Text"
    Me.ef_token_reward.TabIndex = 5
    Me.ef_token_reward.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_token_reward.TextValue = ""
    Me.ef_token_reward.TextVisible = False
    Me.ef_token_reward.TextWidth = 110
    Me.ef_token_reward.Value = ""
    '
    'ef_token_name
    '
    Me.ef_token_name.DoubleValue = 0
    Me.ef_token_name.IntegerValue = 0
    Me.ef_token_name.IsReadOnly = False
    Me.ef_token_name.Location = New System.Drawing.Point(6, 41)
    Me.ef_token_name.Name = "ef_token_name"
    Me.ef_token_name.Size = New System.Drawing.Size(411, 24)
    Me.ef_token_name.SufixText = "Sufix Text"
    Me.ef_token_name.TabIndex = 6
    Me.ef_token_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_token_name.TextValue = ""
    Me.ef_token_name.TextVisible = False
    Me.ef_token_name.TextWidth = 110
    Me.ef_token_name.Value = ""
    '
    'tab_flags_awarded
    '
    Me.tab_flags_awarded.Controls.Add(Me.dg_flags_awarded)
    Me.tab_flags_awarded.Controls.Add(Me.chk_flags_awarded)
    Me.tab_flags_awarded.Location = New System.Drawing.Point(4, 22)
    Me.tab_flags_awarded.Name = "tab_flags_awarded"
    Me.tab_flags_awarded.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_flags_awarded.Size = New System.Drawing.Size(505, 167)
    Me.tab_flags_awarded.TabIndex = 3
    Me.tab_flags_awarded.Text = "xFlags"
    Me.tab_flags_awarded.UseVisualStyleBackColor = True
    '
    'dg_flags_awarded
    '
    Me.dg_flags_awarded.CurrentCol = -1
    Me.dg_flags_awarded.CurrentRow = -1
    Me.dg_flags_awarded.Location = New System.Drawing.Point(26, 29)
    Me.dg_flags_awarded.Name = "dg_flags_awarded"
    Me.dg_flags_awarded.PanelRightVisible = False
    Me.dg_flags_awarded.Redraw = True
    Me.dg_flags_awarded.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_flags_awarded.Size = New System.Drawing.Size(342, 116)
    Me.dg_flags_awarded.Sortable = False
    Me.dg_flags_awarded.SortAscending = True
    Me.dg_flags_awarded.SortByCol = 0
    Me.dg_flags_awarded.TabIndex = 5
    Me.dg_flags_awarded.ToolTipped = True
    Me.dg_flags_awarded.TopRow = -2
    '
    'chk_flags_awarded
    '
    Me.chk_flags_awarded.AutoSize = True
    Me.chk_flags_awarded.Location = New System.Drawing.Point(6, 6)
    Me.chk_flags_awarded.Name = "chk_flags_awarded"
    Me.chk_flags_awarded.Size = New System.Drawing.Size(68, 17)
    Me.chk_flags_awarded.TabIndex = 4
    Me.chk_flags_awarded.Text = "xActive"
    Me.chk_flags_awarded.UseVisualStyleBackColor = True
    '
    'uc_provider_filter1
    '
    Me.uc_provider_filter1.AllowCollapse = True
    Me.uc_provider_filter1.Collapsed = True
    Me.uc_provider_filter1.Location = New System.Drawing.Point(531, 343)
    Me.uc_provider_filter1.Name = "uc_provider_filter1"
    Me.uc_provider_filter1.Size = New System.Drawing.Size(283, 48)
    Me.uc_provider_filter1.TabIndex = 16
    Me.uc_provider_filter1.Visible = False
    '
    'lbl_icon
    '
    Me.lbl_icon.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_icon.ForeColor = System.Drawing.Color.Black
    Me.lbl_icon.Location = New System.Drawing.Point(634, 32)
    Me.lbl_icon.Name = "lbl_icon"
    Me.lbl_icon.Size = New System.Drawing.Size(103, 15)
    Me.lbl_icon.TabIndex = 9
    Me.lbl_icon.Text = "xIcon"
    Me.lbl_icon.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_image
    '
    Me.lbl_image.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.lbl_image.ForeColor = System.Drawing.Color.Black
    Me.lbl_image.Location = New System.Drawing.Point(766, 5)
    Me.lbl_image.Name = "lbl_image"
    Me.lbl_image.Size = New System.Drawing.Size(205, 18)
    Me.lbl_image.TabIndex = 12
    Me.lbl_image.Text = "xImage"
    Me.lbl_image.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'img_image
    '
    Me.img_image.AutoSize = True
    Me.img_image.ButtonDeleteEnabled = True
    Me.img_image.FreeResize = False
    Me.img_image.Image = Nothing
    Me.img_image.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.img_image.ImageLayout = System.Windows.Forms.ImageLayout.Center
    Me.img_image.Location = New System.Drawing.Point(777, 39)
    Me.img_image.Margin = New System.Windows.Forms.Padding(0)
    Me.img_image.Name = "img_image"
    Me.img_image.Size = New System.Drawing.Size(181, 173)
    Me.img_image.TabIndex = 14
    '
    'img_icon
    '
    Me.img_icon.AutoSize = True
    Me.img_icon.ButtonDeleteEnabled = True
    Me.img_icon.FreeResize = False
    Me.img_icon.Image = Nothing
    Me.img_icon.ImageInfoFont = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.img_icon.ImageLayout = System.Windows.Forms.ImageLayout.Center
    Me.img_icon.Location = New System.Drawing.Point(612, 65)
    Me.img_icon.Margin = New System.Windows.Forms.Padding(0)
    Me.img_icon.Name = "img_icon"
    Me.img_icon.Size = New System.Drawing.Size(150, 102)
    Me.img_icon.TabIndex = 11
    '
    'lbl_icon_optimun_size
    '
    Me.lbl_icon_optimun_size.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_icon_optimun_size.Location = New System.Drawing.Point(593, 45)
    Me.lbl_icon_optimun_size.Name = "lbl_icon_optimun_size"
    Me.lbl_icon_optimun_size.Size = New System.Drawing.Size(185, 18)
    Me.lbl_icon_optimun_size.TabIndex = 10
    Me.lbl_icon_optimun_size.Text = "xOptimunSize"
    Me.lbl_icon_optimun_size.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lbl_image_optimun_size
    '
    Me.lbl_image_optimun_size.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lbl_image_optimun_size.Location = New System.Drawing.Point(767, 21)
    Me.lbl_image_optimun_size.Name = "lbl_image_optimun_size"
    Me.lbl_image_optimun_size.Size = New System.Drawing.Size(205, 15)
    Me.lbl_image_optimun_size.TabIndex = 13
    Me.lbl_image_optimun_size.Text = "xOptimunSize"
    Me.lbl_image_optimun_size.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'tc_expirarion_ticketfooter
    '
    Me.tc_expirarion_ticketfooter.Controls.Add(Me.tp_expiration)
    Me.tc_expirarion_ticketfooter.Controls.Add(Me.tp_ticketfooter)
    Me.tc_expirarion_ticketfooter.Location = New System.Drawing.Point(8, 581)
    Me.tc_expirarion_ticketfooter.Name = "tc_expirarion_ticketfooter"
    Me.tc_expirarion_ticketfooter.SelectedIndex = 0
    Me.tc_expirarion_ticketfooter.Size = New System.Drawing.Size(397, 139)
    Me.tc_expirarion_ticketfooter.TabIndex = 7
    Me.tc_expirarion_ticketfooter.Tag = ""
    '
    'tp_expiration
    '
    Me.tp_expiration.Controls.Add(Me.pa_expiration)
    Me.tp_expiration.Location = New System.Drawing.Point(4, 22)
    Me.tp_expiration.Name = "tp_expiration"
    Me.tp_expiration.Padding = New System.Windows.Forms.Padding(3)
    Me.tp_expiration.Size = New System.Drawing.Size(389, 113)
    Me.tp_expiration.TabIndex = 0
    Me.tp_expiration.Text = "xExpiration"
    Me.tp_expiration.UseVisualStyleBackColor = True
    '
    'pa_expiration
    '
    Me.pa_expiration.Controls.Add(Me.dtp_expiration_limit)
    Me.pa_expiration.Controls.Add(Me.lbl_credits_non_redeemable)
    Me.pa_expiration.Controls.Add(Me.chk_expiration_limit)
    Me.pa_expiration.Controls.Add(Me.ef_expiration_days)
    Me.pa_expiration.Controls.Add(Me.ef_expiration_hours)
    Me.pa_expiration.Controls.Add(Me.opt_expiration_hours)
    Me.pa_expiration.Controls.Add(Me.opt_expiration_days)
    Me.pa_expiration.Location = New System.Drawing.Point(-1, 1)
    Me.pa_expiration.Name = "pa_expiration"
    Me.pa_expiration.Size = New System.Drawing.Size(391, 117)
    Me.pa_expiration.TabIndex = 0
    '
    'dtp_expiration_limit
    '
    Me.dtp_expiration_limit.Checked = True
    Me.dtp_expiration_limit.IsReadOnly = False
    Me.dtp_expiration_limit.Location = New System.Drawing.Point(191, 84)
    Me.dtp_expiration_limit.Name = "dtp_expiration_limit"
    Me.dtp_expiration_limit.ShowCheckBox = False
    Me.dtp_expiration_limit.ShowUpDown = False
    Me.dtp_expiration_limit.Size = New System.Drawing.Size(147, 24)
    Me.dtp_expiration_limit.SufixText = "Sufix Text"
    Me.dtp_expiration_limit.SufixTextVisible = True
    Me.dtp_expiration_limit.TabIndex = 23
    Me.dtp_expiration_limit.TextWidth = 0
    Me.dtp_expiration_limit.Value = New Date(2013, 4, 11, 0, 0, 0, 0)
    '
    'lbl_credits_non_redeemable
    '
    Me.lbl_credits_non_redeemable.IsReadOnly = True
    Me.lbl_credits_non_redeemable.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_credits_non_redeemable.LabelForeColor = System.Drawing.Color.Black
    Me.lbl_credits_non_redeemable.Location = New System.Drawing.Point(17, 5)
    Me.lbl_credits_non_redeemable.Name = "lbl_credits_non_redeemable"
    Me.lbl_credits_non_redeemable.Size = New System.Drawing.Size(200, 24)
    Me.lbl_credits_non_redeemable.SufixText = "Sufix Text"
    Me.lbl_credits_non_redeemable.SufixTextVisible = True
    Me.lbl_credits_non_redeemable.TabIndex = 22
    Me.lbl_credits_non_redeemable.TextWidth = 0
    Me.lbl_credits_non_redeemable.Value = "xCredits non-redeemable"
    '
    'chk_expiration_limit
    '
    Me.chk_expiration_limit.AutoSize = True
    Me.chk_expiration_limit.Location = New System.Drawing.Point(17, 88)
    Me.chk_expiration_limit.Name = "chk_expiration_limit"
    Me.chk_expiration_limit.Size = New System.Drawing.Size(114, 17)
    Me.chk_expiration_limit.TabIndex = 21
    Me.chk_expiration_limit.Text = "xNot valid after"
    Me.chk_expiration_limit.UseVisualStyleBackColor = True
    '
    'ef_expiration_days
    '
    Me.ef_expiration_days.DoubleValue = 0
    Me.ef_expiration_days.IntegerValue = 0
    Me.ef_expiration_days.IsReadOnly = False
    Me.ef_expiration_days.Location = New System.Drawing.Point(118, 30)
    Me.ef_expiration_days.Name = "ef_expiration_days"
    Me.ef_expiration_days.Size = New System.Drawing.Size(260, 24)
    Me.ef_expiration_days.SufixText = "xday(s), at closing time (hh:mi)"
    Me.ef_expiration_days.SufixTextVisible = True
    Me.ef_expiration_days.SufixTextWidth = 220
    Me.ef_expiration_days.TabIndex = 20
    Me.ef_expiration_days.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_expiration_days.TextValue = ""
    Me.ef_expiration_days.TextVisible = False
    Me.ef_expiration_days.TextWidth = 0
    Me.ef_expiration_days.Value = ""
    '
    'ef_expiration_hours
    '
    Me.ef_expiration_hours.DoubleValue = 0
    Me.ef_expiration_hours.IntegerValue = 0
    Me.ef_expiration_hours.IsReadOnly = False
    Me.ef_expiration_hours.Location = New System.Drawing.Point(118, 58)
    Me.ef_expiration_hours.Name = "ef_expiration_hours"
    Me.ef_expiration_hours.Size = New System.Drawing.Size(260, 24)
    Me.ef_expiration_hours.SufixText = "xhour(s) from cash in"
    Me.ef_expiration_hours.SufixTextVisible = True
    Me.ef_expiration_hours.SufixTextWidth = 220
    Me.ef_expiration_hours.TabIndex = 18
    Me.ef_expiration_hours.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_expiration_hours.TextValue = ""
    Me.ef_expiration_hours.TextVisible = False
    Me.ef_expiration_hours.TextWidth = 0
    Me.ef_expiration_hours.Value = ""
    '
    'opt_expiration_hours
    '
    Me.opt_expiration_hours.Location = New System.Drawing.Point(17, 62)
    Me.opt_expiration_hours.Name = "opt_expiration_hours"
    Me.opt_expiration_hours.Size = New System.Drawing.Size(108, 20)
    Me.opt_expiration_hours.TabIndex = 17
    Me.opt_expiration_hours.Text = "xAfter"
    '
    'opt_expiration_days
    '
    Me.opt_expiration_days.Location = New System.Drawing.Point(17, 34)
    Me.opt_expiration_days.Name = "opt_expiration_days"
    Me.opt_expiration_days.Size = New System.Drawing.Size(108, 20)
    Me.opt_expiration_days.TabIndex = 16
    Me.opt_expiration_days.Text = "xAfter"
    '
    'tp_ticketfooter
    '
    Me.tp_ticketfooter.Controls.Add(Me.tb_ticket_footer)
    Me.tp_ticketfooter.Location = New System.Drawing.Point(4, 22)
    Me.tp_ticketfooter.Name = "tp_ticketfooter"
    Me.tp_ticketfooter.Padding = New System.Windows.Forms.Padding(3)
    Me.tp_ticketfooter.Size = New System.Drawing.Size(389, 113)
    Me.tp_ticketfooter.TabIndex = 1
    Me.tp_ticketfooter.Text = "xTicketFooter"
    Me.tp_ticketfooter.UseVisualStyleBackColor = True
    '
    'tb_ticket_footer
    '
    Me.tb_ticket_footer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.tb_ticket_footer.Location = New System.Drawing.Point(25, 16)
    Me.tb_ticket_footer.MaxLength = 250
    Me.tb_ticket_footer.Multiline = True
    Me.tb_ticket_footer.Name = "tb_ticket_footer"
    Me.tb_ticket_footer.Size = New System.Drawing.Size(342, 85)
    Me.tb_ticket_footer.TabIndex = 0
    '
    'cmb_categories
    '
    Me.cmb_categories.IsReadOnly = False
    Me.cmb_categories.Location = New System.Drawing.Point(12, 32)
    Me.cmb_categories.Name = "cmb_categories"
    Me.cmb_categories.SelectedIndex = -1
    Me.cmb_categories.Size = New System.Drawing.Size(268, 24)
    Me.cmb_categories.SufixText = "Sufix Text"
    Me.cmb_categories.SufixTextVisible = True
    Me.cmb_categories.TabIndex = 1
    Me.cmb_categories.TextWidth = 87
    '
    'chk_visible_on_PromoBOX
    '
    Me.chk_visible_on_PromoBOX.AutoSize = True
    Me.chk_visible_on_PromoBOX.Location = New System.Drawing.Point(226, 131)
    Me.chk_visible_on_PromoBOX.Name = "chk_visible_on_PromoBOX"
    Me.chk_visible_on_PromoBOX.Size = New System.Drawing.Size(148, 17)
    Me.chk_visible_on_PromoBOX.TabIndex = 4
    Me.chk_visible_on_PromoBOX.Text = "xVisibleOnPromoBOX"
    Me.chk_visible_on_PromoBOX.UseVisualStyleBackColor = True
    '
    'gb_tito
    '
    Me.gb_tito.Controls.Add(Me.ef_generated_tickets_tito)
    Me.gb_tito.Controls.Add(Me.chk_allow_tickets_tito)
    Me.gb_tito.Controls.Add(Me.ef_quantity_tickets_tito)
    Me.gb_tito.Location = New System.Drawing.Point(416, 704)
    Me.gb_tito.Name = "gb_tito"
    Me.gb_tito.Size = New System.Drawing.Size(513, 49)
    Me.gb_tito.TabIndex = 20
    Me.gb_tito.TabStop = False
    Me.gb_tito.Text = "xTITO"
    '
    'ef_generated_tickets_tito
    '
    Me.ef_generated_tickets_tito.DoubleValue = 0
    Me.ef_generated_tickets_tito.IntegerValue = 0
    Me.ef_generated_tickets_tito.IsReadOnly = False
    Me.ef_generated_tickets_tito.Location = New System.Drawing.Point(327, 18)
    Me.ef_generated_tickets_tito.Name = "ef_generated_tickets_tito"
    Me.ef_generated_tickets_tito.OnlyUpperCase = True
    Me.ef_generated_tickets_tito.Size = New System.Drawing.Size(169, 25)
    Me.ef_generated_tickets_tito.SufixText = "Sufix Text"
    Me.ef_generated_tickets_tito.SufixTextVisible = True
    Me.ef_generated_tickets_tito.TabIndex = 6
    Me.ef_generated_tickets_tito.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_generated_tickets_tito.TextValue = ""
    Me.ef_generated_tickets_tito.TextWidth = 110
    Me.ef_generated_tickets_tito.Value = ""
    '
    'chk_allow_tickets_tito
    '
    Me.chk_allow_tickets_tito.AutoSize = True
    Me.chk_allow_tickets_tito.Location = New System.Drawing.Point(16, 22)
    Me.chk_allow_tickets_tito.Name = "chk_allow_tickets_tito"
    Me.chk_allow_tickets_tito.Size = New System.Drawing.Size(131, 17)
    Me.chk_allow_tickets_tito.TabIndex = 4
    Me.chk_allow_tickets_tito.Text = "xAllowTicketsTITO"
    Me.chk_allow_tickets_tito.UseVisualStyleBackColor = True
    '
    'ef_quantity_tickets_tito
    '
    Me.ef_quantity_tickets_tito.DoubleValue = 0
    Me.ef_quantity_tickets_tito.IntegerValue = 0
    Me.ef_quantity_tickets_tito.IsReadOnly = False
    Me.ef_quantity_tickets_tito.Location = New System.Drawing.Point(147, 18)
    Me.ef_quantity_tickets_tito.Name = "ef_quantity_tickets_tito"
    Me.ef_quantity_tickets_tito.OnlyUpperCase = True
    Me.ef_quantity_tickets_tito.Size = New System.Drawing.Size(174, 25)
    Me.ef_quantity_tickets_tito.SufixText = "Sufix Text"
    Me.ef_quantity_tickets_tito.SufixTextVisible = True
    Me.ef_quantity_tickets_tito.TabIndex = 5
    Me.ef_quantity_tickets_tito.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_quantity_tickets_tito.TextValue = ""
    Me.ef_quantity_tickets_tito.TextWidth = 110
    Me.ef_quantity_tickets_tito.Value = ""
    '
    'frm_TITO_promotion_edit
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1084, 773)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_TITO_promotion_edit"
    Me.Text = "frm_promotion_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.gb_enabled.ResumeLayout(False)
    Me.gb_won_lock.ResumeLayout(False)
    Me.gb_won_lock.PerformLayout()
    Me.tab_limit.ResumeLayout(False)
    Me.tab_by_customer.ResumeLayout(False)
    Me.tab_by_customer.PerformLayout()
    Me.tab_by_promotion.ResumeLayout(False)
    Me.tab_by_promotion.PerformLayout()
    Me.tab_filters.ResumeLayout(False)
    Me.tab_gender.ResumeLayout(False)
    Me.tab_gender.PerformLayout()
    Me.tab_birth_date.ResumeLayout(False)
    Me.tab_birth_date.PerformLayout()
    Me.tab_level.ResumeLayout(False)
    Me.tab_level.PerformLayout()
    Me.tab_freq.ResumeLayout(False)
    Me.tab_freq.PerformLayout()
    Me.tab_flags_required.ResumeLayout(False)
    Me.tab_flags_required.PerformLayout()
    Me.gb_test.ResumeLayout(False)
    Me.gb_test.PerformLayout()
    Me.gb_special_permission.ResumeLayout(False)
    Me.gb_special_permission.PerformLayout()
    Me.gb_credit_type.ResumeLayout(False)
    Me.tabPromotions.ResumeLayout(False)
    Me.tab_chash_in.ResumeLayout(False)
    Me.tab_chash_in.PerformLayout()
    Me.tab_spent.ResumeLayout(False)
    Me.tab_spent.PerformLayout()
    Me.tab_tokens.ResumeLayout(False)
    Me.tab_tokens.PerformLayout()
    Me.tab_flags_awarded.ResumeLayout(False)
    Me.tab_flags_awarded.PerformLayout()
    Me.tc_expirarion_ticketfooter.ResumeLayout(False)
    Me.tp_expiration.ResumeLayout(False)
    Me.pa_expiration.ResumeLayout(False)
    Me.pa_expiration.PerformLayout()
    Me.tp_ticketfooter.ResumeLayout(False)
    Me.tp_ticketfooter.PerformLayout()
    Me.gb_tito.ResumeLayout(False)
    Me.gb_tito.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_enabled As System.Windows.Forms.GroupBox
  Friend WithEvents opt_disabled As System.Windows.Forms.RadioButton
  Friend WithEvents opt_enabled As System.Windows.Forms.RadioButton
  Friend WithEvents ef_promo_name As GUI_Controls.uc_entry_field
  Friend WithEvents gb_won_lock As System.Windows.Forms.GroupBox
  Friend WithEvents uc_promo_schedule As GUI_Controls.uc_schedule
  Friend WithEvents opt_gender_female As System.Windows.Forms.RadioButton
  Friend WithEvents opt_gender_male As System.Windows.Forms.RadioButton
  Friend WithEvents chk_by_gender As System.Windows.Forms.CheckBox
  Friend WithEvents chk_by_birth_date As System.Windows.Forms.CheckBox
  Friend WithEvents opt_birth_month_only As System.Windows.Forms.RadioButton
  Friend WithEvents opt_birth_date_only As System.Windows.Forms.RadioButton
  Friend WithEvents lbl_daily_limit As GUI_Controls.uc_text_field
  Friend WithEvents ef_daily_limit As GUI_Controls.uc_entry_field
  Friend WithEvents gb_test As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_test_result As GUI_Controls.uc_text_field
  Friend WithEvents ef_test_num_tokens As GUI_Controls.uc_entry_field
  Friend WithEvents ef_test_cash_in As GUI_Controls.uc_entry_field
  Friend WithEvents Timer1 As System.Windows.Forms.Timer
  Friend WithEvents ef_won_lock As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_won_lock As GUI_Controls.uc_text_field
  Friend WithEvents chk_won_lock As System.Windows.Forms.CheckBox
  Friend WithEvents tab_filters As System.Windows.Forms.TabControl
  Friend WithEvents tab_gender As System.Windows.Forms.TabPage
  Friend WithEvents tab_birth_date As System.Windows.Forms.TabPage
  Friend WithEvents tab_level As System.Windows.Forms.TabPage
  Friend WithEvents chk_by_level As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_03 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_02 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_01 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_04 As System.Windows.Forms.CheckBox
  Friend WithEvents chk_special_permission As System.Windows.Forms.CheckBox
  Friend WithEvents chk_level_anonymous As System.Windows.Forms.CheckBox
  Friend WithEvents gb_special_permission As System.Windows.Forms.GroupBox
  Friend WithEvents opt_special_permission_b As System.Windows.Forms.RadioButton
  Friend WithEvents opt_special_permission_a As System.Windows.Forms.RadioButton
  Friend WithEvents tab_freq As System.Windows.Forms.TabPage
  Friend WithEvents ef_freq_last_days As GUI_Controls.uc_entry_field
  Friend WithEvents chk_by_freq As System.Windows.Forms.CheckBox
  Friend WithEvents ef_freq_min_cash_in As GUI_Controls.uc_entry_field
  Friend WithEvents ef_freq_min_days As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_freq_filter_01 As GUI_Controls.uc_text_field
  Friend WithEvents lbl_freq_filter_02 As GUI_Controls.uc_text_field
  Friend WithEvents ef_test_spent As GUI_Controls.uc_entry_field
  Friend WithEvents ef_min_cash_in As GUI_Controls.uc_entry_field
  Friend WithEvents ef_min_cash_in_reward As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_cash_in_promotion As GUI_Controls.uc_text_field
  Friend WithEvents lbl_min_cash_in_promotion As GUI_Controls.uc_text_field
  Friend WithEvents ef_cash_in_reward As GUI_Controls.uc_entry_field
  Friend WithEvents ef_cash_in As GUI_Controls.uc_entry_field
  Friend WithEvents gb_credit_type As System.Windows.Forms.GroupBox
  Friend WithEvents opt_credit_redeemable As System.Windows.Forms.RadioButton
  Friend WithEvents opt_credit_non_redeemable As System.Windows.Forms.RadioButton
  Friend WithEvents tab_limit As System.Windows.Forms.TabControl
  Friend WithEvents tab_by_customer As System.Windows.Forms.TabPage
  Friend WithEvents tab_by_promotion As System.Windows.Forms.TabPage
  Friend WithEvents ef_promotion_limit_month As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_promotion_monthly_limit As GUI_Controls.uc_text_field
  Friend WithEvents ef_promotion_limit_day As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_promotion_daily_limit As GUI_Controls.uc_text_field
  Friend WithEvents ef_monthly_limit As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_monthly_limit As GUI_Controls.uc_text_field
  Friend WithEvents tabPromotions As System.Windows.Forms.TabControl
  Friend WithEvents tab_chash_in As System.Windows.Forms.TabPage
  Friend WithEvents tab_spent As System.Windows.Forms.TabPage
  Friend WithEvents ef_min_spent As GUI_Controls.uc_entry_field
  Friend WithEvents ef_min_spent_reward As GUI_Controls.uc_entry_field
  Friend WithEvents ef_spent As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_spent_promotion As GUI_Controls.uc_text_field
  Friend WithEvents ef_spent_reward As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_min_spent_promotion As GUI_Controls.uc_text_field
  Friend WithEvents tab_tokens As System.Windows.Forms.TabPage
  Friend WithEvents ef_num_tokens As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_token_reward As GUI_Controls.uc_text_field
  Friend WithEvents ef_token_reward As GUI_Controls.uc_entry_field
  Friend WithEvents ef_token_name As GUI_Controls.uc_entry_field
  Friend WithEvents uc_provider_filter1 As GUI_Controls.uc_provider_filter
  Friend WithEvents lbl_not_applicable_mb1 As System.Windows.Forms.Label
  Friend WithEvents lbl_image As System.Windows.Forms.Label
  Friend WithEvents lbl_icon As System.Windows.Forms.Label
  Friend WithEvents img_image As GUI_Controls.uc_image
  Friend WithEvents img_icon As GUI_Controls.uc_image
  Friend WithEvents opt_point As System.Windows.Forms.RadioButton
  Friend WithEvents ef_promotion_limit_global As GUI_Controls.uc_entry_field
  Friend WithEvents lbl_promotion_global_limit As GUI_Controls.uc_text_field
  Friend WithEvents lbl_image_optimun_size As System.Windows.Forms.Label
  Friend WithEvents lbl_icon_optimun_size As System.Windows.Forms.Label
  Friend WithEvents tc_expirarion_ticketfooter As System.Windows.Forms.TabControl
  Friend WithEvents tp_expiration As System.Windows.Forms.TabPage
  Friend WithEvents tp_ticketfooter As System.Windows.Forms.TabPage
  Friend WithEvents tb_ticket_footer As System.Windows.Forms.TextBox
  Friend WithEvents pa_expiration As System.Windows.Forms.Panel
  Friend WithEvents ef_expiration_days As GUI_Controls.uc_entry_field
  Friend WithEvents ef_expiration_hours As GUI_Controls.uc_entry_field
  Friend WithEvents opt_expiration_hours As System.Windows.Forms.RadioButton
  Friend WithEvents opt_expiration_days As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_categories As GUI_Controls.uc_combo
  Friend WithEvents tab_flags_required As System.Windows.Forms.TabPage
  Friend WithEvents chk_by_flag As System.Windows.Forms.CheckBox
  Friend WithEvents dg_flags_required As GUI_Controls.uc_grid
  Friend WithEvents tab_flags_awarded As System.Windows.Forms.TabPage
  Friend WithEvents chk_flags_awarded As System.Windows.Forms.CheckBox
  Friend WithEvents dg_flags_awarded As GUI_Controls.uc_grid
  Friend WithEvents chk_visible_on_PromoBOX As System.Windows.Forms.CheckBox
  Friend WithEvents chk_expiration_limit As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_credits_non_redeemable As GUI_Controls.uc_text_field
  Friend WithEvents dtp_expiration_limit As GUI_Controls.uc_date_picker
  Friend WithEvents gb_tito As System.Windows.Forms.GroupBox
  Friend WithEvents chk_allow_tickets_tito As System.Windows.Forms.CheckBox
  Friend WithEvents ef_quantity_tickets_tito As GUI_Controls.uc_entry_field
  Friend WithEvents ef_generated_tickets_tito As GUI_Controls.uc_entry_field
End Class
