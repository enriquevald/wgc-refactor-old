<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_tito_reports_sel
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.ds_from_to = New GUI_Controls.uc_daily_session_selector()
    Me.gb_state = New System.Windows.Forms.GroupBox()
    Me.chk_pending_cancel = New System.Windows.Forms.CheckBox()
    Me.chk_pending_print = New System.Windows.Forms.CheckBox()
    Me.chk_swap_chips = New System.Windows.Forms.CheckBox()
    Me.chk_pending_payment_after_expiration = New System.Windows.Forms.CheckBox()
    Me.chk_pending_payment = New System.Windows.Forms.CheckBox()
    Me.chk_expired_redeemed = New System.Windows.Forms.CheckBox()
    Me.chk_discarded = New System.Windows.Forms.CheckBox()
    Me.chk_canceled = New System.Windows.Forms.CheckBox()
    Me.chk_redeemed = New System.Windows.Forms.CheckBox()
    Me.chk_expired = New System.Windows.Forms.CheckBox()
    Me.chk_valid = New System.Windows.Forms.CheckBox()
    Me.uc_terminals_list = New GUI_Controls.uc_provider()
    Me.gb_ticket_type = New System.Windows.Forms.GroupBox()
    Me.chk_offline = New System.Windows.Forms.CheckBox()
    Me.ef_ticket_number = New GUI_Controls.uc_entry_field()
    Me.chk_handpay = New System.Windows.Forms.CheckBox()
    Me.chk_jackpot = New System.Windows.Forms.CheckBox()
    Me.chk_promo_nonredeem_ticket = New System.Windows.Forms.CheckBox()
    Me.chk_promo_redeem_ticket = New System.Windows.Forms.CheckBox()
    Me.chk_redeemable_ticket = New System.Windows.Forms.CheckBox()
    Me.gb_date_filter = New System.Windows.Forms.GroupBox()
    Me.opt_expiration = New System.Windows.Forms.RadioButton()
    Me.opt_cancelation = New System.Windows.Forms.RadioButton()
    Me.opt_creation = New System.Windows.Forms.RadioButton()
    Me.gb_cashiers = New System.Windows.Forms.GroupBox()
    Me.btn_uncheck_all = New System.Windows.Forms.Button()
    Me.btn_check_all = New System.Windows.Forms.Button()
    Me.dg_cashiers = New GUI_Controls.uc_grid()
    Me.uc_account_sel = New GUI_Controls.uc_account_sel()
    Me.gb_machine_type = New System.Windows.Forms.GroupBox()
    Me.chk_terminals = New System.Windows.Forms.CheckBox()
    Me.chk_cashiers = New System.Windows.Forms.CheckBox()
    Me.gb_group_by = New System.Windows.Forms.GroupBox()
    Me.chk_detailed_report = New System.Windows.Forms.CheckBox()
    Me.opt_terminal_creation = New System.Windows.Forms.RadioButton()
    Me.opt_date_creation = New System.Windows.Forms.RadioButton()
    Me.chk_group_by = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_state.SuspendLayout()
    Me.gb_ticket_type.SuspendLayout()
    Me.gb_date_filter.SuspendLayout()
    Me.gb_cashiers.SuspendLayout()
    Me.gb_machine_type.SuspendLayout()
    Me.gb_group_by.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_group_by)
    Me.panel_filter.Controls.Add(Me.gb_machine_type)
    Me.panel_filter.Controls.Add(Me.uc_account_sel)
    Me.panel_filter.Controls.Add(Me.gb_cashiers)
    Me.panel_filter.Controls.Add(Me.gb_date_filter)
    Me.panel_filter.Controls.Add(Me.gb_state)
    Me.panel_filter.Controls.Add(Me.uc_terminals_list)
    Me.panel_filter.Controls.Add(Me.gb_ticket_type)
    Me.panel_filter.Size = New System.Drawing.Size(1357, 330)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_ticket_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_terminals_list, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_state, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date_filter, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_cashiers, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_account_sel, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_machine_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_group_by, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 334)
    Me.panel_data.Size = New System.Drawing.Size(1357, 424)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1351, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1351, 4)
    '
    'ds_from_to
    '
    Me.ds_from_to.ClosingTime = 0
    Me.ds_from_to.ClosingTimeEnabled = True
    Me.ds_from_to.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.ds_from_to.FromDateSelected = True
    Me.ds_from_to.Location = New System.Drawing.Point(3, 71)
    Me.ds_from_to.Name = "ds_from_to"
    Me.ds_from_to.ShowBorder = False
    Me.ds_from_to.Size = New System.Drawing.Size(250, 76)
    Me.ds_from_to.TabIndex = 0
    Me.ds_from_to.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.ds_from_to.ToDateSelected = True
    '
    'gb_state
    '
    Me.gb_state.Controls.Add(Me.chk_pending_cancel)
    Me.gb_state.Controls.Add(Me.chk_pending_print)
    Me.gb_state.Controls.Add(Me.chk_swap_chips)
    Me.gb_state.Controls.Add(Me.chk_pending_payment_after_expiration)
    Me.gb_state.Controls.Add(Me.chk_pending_payment)
    Me.gb_state.Controls.Add(Me.chk_expired_redeemed)
    Me.gb_state.Controls.Add(Me.chk_discarded)
    Me.gb_state.Controls.Add(Me.chk_canceled)
    Me.gb_state.Controls.Add(Me.chk_redeemed)
    Me.gb_state.Controls.Add(Me.chk_expired)
    Me.gb_state.Controls.Add(Me.chk_valid)
    Me.gb_state.Location = New System.Drawing.Point(7, 164)
    Me.gb_state.Name = "gb_state"
    Me.gb_state.Size = New System.Drawing.Size(417, 159)
    Me.gb_state.TabIndex = 4
    Me.gb_state.TabStop = False
    Me.gb_state.Text = "xState"
    '
    'chk_pending_cancel
    '
    Me.chk_pending_cancel.AutoSize = True
    Me.chk_pending_cancel.Location = New System.Drawing.Point(7, 62)
    Me.chk_pending_cancel.Name = "chk_pending_cancel"
    Me.chk_pending_cancel.Size = New System.Drawing.Size(117, 17)
    Me.chk_pending_cancel.TabIndex = 9
    Me.chk_pending_cancel.Text = "xPendingCancel"
    Me.chk_pending_cancel.UseVisualStyleBackColor = True
    '
    'chk_pending_print
    '
    Me.chk_pending_print.AutoSize = True
    Me.chk_pending_print.Location = New System.Drawing.Point(7, 20)
    Me.chk_pending_print.Name = "chk_pending_print"
    Me.chk_pending_print.Size = New System.Drawing.Size(104, 17)
    Me.chk_pending_print.TabIndex = 8
    Me.chk_pending_print.Text = "xPendingPrint"
    Me.chk_pending_print.UseVisualStyleBackColor = True
    '
    'chk_swap_chips
    '
    Me.chk_swap_chips.AutoSize = True
    Me.chk_swap_chips.Location = New System.Drawing.Point(208, 104)
    Me.chk_swap_chips.Name = "chk_swap_chips"
    Me.chk_swap_chips.Size = New System.Drawing.Size(109, 17)
    Me.chk_swap_chips.TabIndex = 6
    Me.chk_swap_chips.Text = "xSwap - Chips"
    Me.chk_swap_chips.UseVisualStyleBackColor = True
    '
    'chk_pending_payment_after_expiration
    '
    Me.chk_pending_payment_after_expiration.AutoSize = True
    Me.chk_pending_payment_after_expiration.Location = New System.Drawing.Point(208, 41)
    Me.chk_pending_payment_after_expiration.Name = "chk_pending_payment_after_expiration"
    Me.chk_pending_payment_after_expiration.Size = New System.Drawing.Size(177, 17)
    Me.chk_pending_payment_after_expiration.TabIndex = 7
    Me.chk_pending_payment_after_expiration.Text = "xPendingPaymentAfterExp"
    Me.chk_pending_payment_after_expiration.UseVisualStyleBackColor = True
    '
    'chk_pending_payment
    '
    Me.chk_pending_payment.AutoSize = True
    Me.chk_pending_payment.Location = New System.Drawing.Point(208, 20)
    Me.chk_pending_payment.Name = "chk_pending_payment"
    Me.chk_pending_payment.Size = New System.Drawing.Size(128, 17)
    Me.chk_pending_payment.TabIndex = 6
    Me.chk_pending_payment.Text = "xPendingPayment"
    Me.chk_pending_payment.UseVisualStyleBackColor = True
    '
    'chk_expired_redeemed
    '
    Me.chk_expired_redeemed.AutoSize = True
    Me.chk_expired_redeemed.Location = New System.Drawing.Point(7, 125)
    Me.chk_expired_redeemed.Name = "chk_expired_redeemed"
    Me.chk_expired_redeemed.Size = New System.Drawing.Size(142, 17)
    Me.chk_expired_redeemed.TabIndex = 5
    Me.chk_expired_redeemed.Text = "xPaidAfterExpiration"
    Me.chk_expired_redeemed.UseVisualStyleBackColor = True
    '
    'chk_discarded
    '
    Me.chk_discarded.AutoSize = True
    Me.chk_discarded.Location = New System.Drawing.Point(208, 83)
    Me.chk_discarded.Name = "chk_discarded"
    Me.chk_discarded.Size = New System.Drawing.Size(90, 17)
    Me.chk_discarded.TabIndex = 4
    Me.chk_discarded.Text = "xDiscarded"
    Me.chk_discarded.UseVisualStyleBackColor = True
    '
    'chk_canceled
    '
    Me.chk_canceled.AutoSize = True
    Me.chk_canceled.Location = New System.Drawing.Point(7, 83)
    Me.chk_canceled.Name = "chk_canceled"
    Me.chk_canceled.Size = New System.Drawing.Size(86, 17)
    Me.chk_canceled.TabIndex = 2
    Me.chk_canceled.Text = "xCanceled"
    Me.chk_canceled.UseVisualStyleBackColor = True
    '
    'chk_redeemed
    '
    Me.chk_redeemed.AutoSize = True
    Me.chk_redeemed.Location = New System.Drawing.Point(7, 104)
    Me.chk_redeemed.Name = "chk_redeemed"
    Me.chk_redeemed.Size = New System.Drawing.Size(80, 17)
    Me.chk_redeemed.TabIndex = 1
    Me.chk_redeemed.Text = "xRedeem"
    Me.chk_redeemed.UseVisualStyleBackColor = True
    '
    'chk_expired
    '
    Me.chk_expired.AutoSize = True
    Me.chk_expired.Location = New System.Drawing.Point(208, 62)
    Me.chk_expired.Name = "chk_expired"
    Me.chk_expired.Size = New System.Drawing.Size(76, 17)
    Me.chk_expired.TabIndex = 3
    Me.chk_expired.Text = "xExpired"
    Me.chk_expired.UseVisualStyleBackColor = True
    '
    'chk_valid
    '
    Me.chk_valid.AutoSize = True
    Me.chk_valid.Location = New System.Drawing.Point(7, 41)
    Me.chk_valid.Name = "chk_valid"
    Me.chk_valid.Size = New System.Drawing.Size(60, 17)
    Me.chk_valid.TabIndex = 0
    Me.chk_valid.Text = "xValid"
    Me.chk_valid.UseVisualStyleBackColor = True
    '
    'uc_terminals_list
    '
    Me.uc_terminals_list.FilterByCurrency = False
    Me.uc_terminals_list.Location = New System.Drawing.Point(933, 2)
    Me.uc_terminals_list.Name = "uc_terminals_list"
    Me.uc_terminals_list.Size = New System.Drawing.Size(304, 187)
    Me.uc_terminals_list.TabIndex = 7
    Me.uc_terminals_list.TerminalListHasValues = False
    '
    'gb_ticket_type
    '
    Me.gb_ticket_type.Controls.Add(Me.chk_offline)
    Me.gb_ticket_type.Controls.Add(Me.ef_ticket_number)
    Me.gb_ticket_type.Controls.Add(Me.chk_handpay)
    Me.gb_ticket_type.Controls.Add(Me.chk_jackpot)
    Me.gb_ticket_type.Controls.Add(Me.chk_promo_nonredeem_ticket)
    Me.gb_ticket_type.Controls.Add(Me.chk_promo_redeem_ticket)
    Me.gb_ticket_type.Controls.Add(Me.chk_redeemable_ticket)
    Me.gb_ticket_type.Location = New System.Drawing.Point(430, 6)
    Me.gb_ticket_type.Name = "gb_ticket_type"
    Me.gb_ticket_type.Size = New System.Drawing.Size(210, 195)
    Me.gb_ticket_type.TabIndex = 3
    Me.gb_ticket_type.TabStop = False
    Me.gb_ticket_type.Text = "xTicket Type"
    '
    'chk_offline
    '
    Me.chk_offline.AutoSize = True
    Me.chk_offline.Location = New System.Drawing.Point(9, 167)
    Me.chk_offline.Name = "chk_offline"
    Me.chk_offline.Size = New System.Drawing.Size(70, 17)
    Me.chk_offline.TabIndex = 6
    Me.chk_offline.Text = "xOffline"
    Me.chk_offline.UseVisualStyleBackColor = True
    '
    'ef_ticket_number
    '
    Me.ef_ticket_number.DoubleValue = 0.0R
    Me.ef_ticket_number.IntegerValue = 0
    Me.ef_ticket_number.IsReadOnly = False
    Me.ef_ticket_number.Location = New System.Drawing.Point(9, 20)
    Me.ef_ticket_number.Name = "ef_ticket_number"
    Me.ef_ticket_number.PlaceHolder = Nothing
    Me.ef_ticket_number.Size = New System.Drawing.Size(192, 24)
    Me.ef_ticket_number.SufixText = "Sufix Text"
    Me.ef_ticket_number.SufixTextVisible = True
    Me.ef_ticket_number.TabIndex = 0
    Me.ef_ticket_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_ticket_number.TextValue = ""
    Me.ef_ticket_number.TextWidth = 50
    Me.ef_ticket_number.Value = ""
    Me.ef_ticket_number.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_handpay
    '
    Me.chk_handpay.AutoSize = True
    Me.chk_handpay.Location = New System.Drawing.Point(9, 144)
    Me.chk_handpay.Name = "chk_handpay"
    Me.chk_handpay.Size = New System.Drawing.Size(83, 17)
    Me.chk_handpay.TabIndex = 5
    Me.chk_handpay.Text = "xHandPay"
    Me.chk_handpay.UseVisualStyleBackColor = True
    '
    'chk_jackpot
    '
    Me.chk_jackpot.AutoSize = True
    Me.chk_jackpot.Location = New System.Drawing.Point(9, 121)
    Me.chk_jackpot.Name = "chk_jackpot"
    Me.chk_jackpot.Size = New System.Drawing.Size(76, 17)
    Me.chk_jackpot.TabIndex = 4
    Me.chk_jackpot.Text = "xJackpot"
    Me.chk_jackpot.UseVisualStyleBackColor = True
    '
    'chk_promo_nonredeem_ticket
    '
    Me.chk_promo_nonredeem_ticket.AutoSize = True
    Me.chk_promo_nonredeem_ticket.Location = New System.Drawing.Point(9, 98)
    Me.chk_promo_nonredeem_ticket.Name = "chk_promo_nonredeem_ticket"
    Me.chk_promo_nonredeem_ticket.Size = New System.Drawing.Size(107, 17)
    Me.chk_promo_nonredeem_ticket.TabIndex = 3
    Me.chk_promo_nonredeem_ticket.Text = "xPlayableOnly"
    Me.chk_promo_nonredeem_ticket.UseVisualStyleBackColor = True
    '
    'chk_promo_redeem_ticket
    '
    Me.chk_promo_redeem_ticket.AutoSize = True
    Me.chk_promo_redeem_ticket.Location = New System.Drawing.Point(9, 75)
    Me.chk_promo_redeem_ticket.Name = "chk_promo_redeem_ticket"
    Me.chk_promo_redeem_ticket.Size = New System.Drawing.Size(196, 17)
    Me.chk_promo_redeem_ticket.TabIndex = 2
    Me.chk_promo_redeem_ticket.Text = "Promotional Non-Redeemable"
    Me.chk_promo_redeem_ticket.UseVisualStyleBackColor = True
    '
    'chk_redeemable_ticket
    '
    Me.chk_redeemable_ticket.AutoSize = True
    Me.chk_redeemable_ticket.Location = New System.Drawing.Point(9, 52)
    Me.chk_redeemable_ticket.Name = "chk_redeemable_ticket"
    Me.chk_redeemable_ticket.Size = New System.Drawing.Size(104, 17)
    Me.chk_redeemable_ticket.TabIndex = 1
    Me.chk_redeemable_ticket.Text = "xRedeemable"
    Me.chk_redeemable_ticket.UseVisualStyleBackColor = True
    '
    'gb_date_filter
    '
    Me.gb_date_filter.Controls.Add(Me.opt_expiration)
    Me.gb_date_filter.Controls.Add(Me.opt_cancelation)
    Me.gb_date_filter.Controls.Add(Me.opt_creation)
    Me.gb_date_filter.Controls.Add(Me.ds_from_to)
    Me.gb_date_filter.Location = New System.Drawing.Point(9, 6)
    Me.gb_date_filter.Name = "gb_date_filter"
    Me.gb_date_filter.Size = New System.Drawing.Size(415, 152)
    Me.gb_date_filter.TabIndex = 0
    Me.gb_date_filter.TabStop = False
    Me.gb_date_filter.Text = "xFiltro fecha"
    '
    'opt_expiration
    '
    Me.opt_expiration.AutoSize = True
    Me.opt_expiration.Location = New System.Drawing.Point(6, 63)
    Me.opt_expiration.Name = "opt_expiration"
    Me.opt_expiration.Size = New System.Drawing.Size(89, 17)
    Me.opt_expiration.TabIndex = 6
    Me.opt_expiration.TabStop = True
    Me.opt_expiration.Text = "xExpiration"
    Me.opt_expiration.UseVisualStyleBackColor = True
    '
    'opt_cancelation
    '
    Me.opt_cancelation.AutoSize = True
    Me.opt_cancelation.Location = New System.Drawing.Point(6, 40)
    Me.opt_cancelation.Name = "opt_cancelation"
    Me.opt_cancelation.Size = New System.Drawing.Size(136, 17)
    Me.opt_cancelation.TabIndex = 5
    Me.opt_cancelation.TabStop = True
    Me.opt_cancelation.Text = "xPlayed/Redeemed"
    Me.opt_cancelation.UseVisualStyleBackColor = True
    '
    'opt_creation
    '
    Me.opt_creation.AutoSize = True
    Me.opt_creation.Location = New System.Drawing.Point(6, 17)
    Me.opt_creation.Name = "opt_creation"
    Me.opt_creation.Size = New System.Drawing.Size(81, 17)
    Me.opt_creation.TabIndex = 4
    Me.opt_creation.TabStop = True
    Me.opt_creation.Text = "xCreation"
    Me.opt_creation.UseVisualStyleBackColor = True
    '
    'gb_cashiers
    '
    Me.gb_cashiers.Controls.Add(Me.btn_uncheck_all)
    Me.gb_cashiers.Controls.Add(Me.btn_check_all)
    Me.gb_cashiers.Controls.Add(Me.dg_cashiers)
    Me.gb_cashiers.Location = New System.Drawing.Point(651, 58)
    Me.gb_cashiers.Name = "gb_cashiers"
    Me.gb_cashiers.Size = New System.Drawing.Size(277, 238)
    Me.gb_cashiers.TabIndex = 6
    Me.gb_cashiers.TabStop = False
    Me.gb_cashiers.Text = "xCashiers"
    '
    'btn_uncheck_all
    '
    Me.btn_uncheck_all.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btn_uncheck_all.Location = New System.Drawing.Point(123, 202)
    Me.btn_uncheck_all.Name = "btn_uncheck_all"
    Me.btn_uncheck_all.Size = New System.Drawing.Size(115, 29)
    Me.btn_uncheck_all.TabIndex = 2
    Me.btn_uncheck_all.Text = "xDesmarcar todas"
    Me.btn_uncheck_all.UseVisualStyleBackColor = True
    '
    'btn_check_all
    '
    Me.btn_check_all.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.btn_check_all.Location = New System.Drawing.Point(17, 202)
    Me.btn_check_all.Name = "btn_check_all"
    Me.btn_check_all.Size = New System.Drawing.Size(100, 29)
    Me.btn_check_all.TabIndex = 1
    Me.btn_check_all.Text = "xMarcar Todas"
    Me.btn_check_all.UseVisualStyleBackColor = True
    '
    'dg_cashiers
    '
    Me.dg_cashiers.CurrentCol = -1
    Me.dg_cashiers.CurrentRow = -1
    Me.dg_cashiers.EditableCellBackColor = System.Drawing.Color.Empty
    Me.dg_cashiers.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.dg_cashiers.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.dg_cashiers.Location = New System.Drawing.Point(17, 20)
    Me.dg_cashiers.Name = "dg_cashiers"
    Me.dg_cashiers.PanelRightVisible = True
    Me.dg_cashiers.Redraw = True
    Me.dg_cashiers.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_cashiers.Size = New System.Drawing.Size(265, 176)
    Me.dg_cashiers.Sortable = False
    Me.dg_cashiers.SortAscending = True
    Me.dg_cashiers.SortByCol = 0
    Me.dg_cashiers.TabIndex = 0
    Me.dg_cashiers.ToolTipped = True
    Me.dg_cashiers.TopRow = -2
    Me.dg_cashiers.WordWrap = False
    '
    'uc_account_sel
    '
    Me.uc_account_sel.Account = ""
    Me.uc_account_sel.AccountText = ""
    Me.uc_account_sel.Anon = False
    Me.uc_account_sel.AutoSize = True
    Me.uc_account_sel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.uc_account_sel.BirthDate = New Date(CType(0, Long))
    Me.uc_account_sel.DisabledHolder = False
    Me.uc_account_sel.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.uc_account_sel.Holder = ""
    Me.uc_account_sel.Location = New System.Drawing.Point(933, 188)
    Me.uc_account_sel.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
    Me.uc_account_sel.MassiveSearchNumbers = ""
    Me.uc_account_sel.MassiveSearchNumbersToEdit = ""
    Me.uc_account_sel.MassiveSearchType = 0
    Me.uc_account_sel.Name = "uc_account_sel"
    Me.uc_account_sel.SearchTrackDataAsInternal = True
    Me.uc_account_sel.ShowMassiveSearch = False
    Me.uc_account_sel.ShowVipClients = True
    Me.uc_account_sel.Size = New System.Drawing.Size(307, 134)
    Me.uc_account_sel.TabIndex = 8
    Me.uc_account_sel.Telephone = ""
    Me.uc_account_sel.TrackData = ""
    Me.uc_account_sel.Vip = False
    Me.uc_account_sel.WeddingDate = New Date(CType(0, Long))
    '
    'gb_machine_type
    '
    Me.gb_machine_type.Controls.Add(Me.chk_terminals)
    Me.gb_machine_type.Controls.Add(Me.chk_cashiers)
    Me.gb_machine_type.Location = New System.Drawing.Point(650, 6)
    Me.gb_machine_type.Name = "gb_machine_type"
    Me.gb_machine_type.Size = New System.Drawing.Size(278, 48)
    Me.gb_machine_type.TabIndex = 5
    Me.gb_machine_type.TabStop = False
    Me.gb_machine_type.Text = "xMachineType"
    '
    'chk_terminals
    '
    Me.chk_terminals.AutoSize = True
    Me.chk_terminals.Location = New System.Drawing.Point(174, 21)
    Me.chk_terminals.Name = "chk_terminals"
    Me.chk_terminals.Size = New System.Drawing.Size(88, 17)
    Me.chk_terminals.TabIndex = 1
    Me.chk_terminals.Text = "xTerminals"
    Me.chk_terminals.UseVisualStyleBackColor = True
    '
    'chk_cashiers
    '
    Me.chk_cashiers.AutoSize = True
    Me.chk_cashiers.Location = New System.Drawing.Point(16, 21)
    Me.chk_cashiers.Name = "chk_cashiers"
    Me.chk_cashiers.Size = New System.Drawing.Size(77, 17)
    Me.chk_cashiers.TabIndex = 0
    Me.chk_cashiers.Text = "xCashier"
    Me.chk_cashiers.UseVisualStyleBackColor = True
    '
    'gb_group_by
    '
    Me.gb_group_by.Controls.Add(Me.chk_detailed_report)
    Me.gb_group_by.Controls.Add(Me.opt_terminal_creation)
    Me.gb_group_by.Controls.Add(Me.opt_date_creation)
    Me.gb_group_by.Controls.Add(Me.chk_group_by)
    Me.gb_group_by.Location = New System.Drawing.Point(431, 207)
    Me.gb_group_by.Name = "gb_group_by"
    Me.gb_group_by.Size = New System.Drawing.Size(209, 116)
    Me.gb_group_by.TabIndex = 1
    Me.gb_group_by.TabStop = False
    Me.gb_group_by.Text = "xOptions"
    '
    'chk_detailed_report
    '
    Me.chk_detailed_report.AutoSize = True
    Me.chk_detailed_report.Enabled = False
    Me.chk_detailed_report.Location = New System.Drawing.Point(5, 93)
    Me.chk_detailed_report.Name = "chk_detailed_report"
    Me.chk_detailed_report.Size = New System.Drawing.Size(80, 17)
    Me.chk_detailed_report.TabIndex = 2
    Me.chk_detailed_report.Text = "xDetailed"
    Me.chk_detailed_report.UseVisualStyleBackColor = True
    '
    'opt_terminal_creation
    '
    Me.opt_terminal_creation.AutoSize = True
    Me.opt_terminal_creation.Location = New System.Drawing.Point(6, 65)
    Me.opt_terminal_creation.Name = "opt_terminal_creation"
    Me.opt_terminal_creation.Size = New System.Drawing.Size(114, 17)
    Me.opt_terminal_creation.TabIndex = 2
    Me.opt_terminal_creation.TabStop = True
    Me.opt_terminal_creation.Text = "xCreationTerm."
    Me.opt_terminal_creation.UseVisualStyleBackColor = True
    '
    'opt_date_creation
    '
    Me.opt_date_creation.AutoSize = True
    Me.opt_date_creation.Location = New System.Drawing.Point(6, 43)
    Me.opt_date_creation.Name = "opt_date_creation"
    Me.opt_date_creation.Size = New System.Drawing.Size(108, 17)
    Me.opt_date_creation.TabIndex = 1
    Me.opt_date_creation.TabStop = True
    Me.opt_date_creation.Text = "xCreationDate"
    Me.opt_date_creation.UseVisualStyleBackColor = True
    '
    'chk_group_by
    '
    Me.chk_group_by.AutoSize = True
    Me.chk_group_by.Location = New System.Drawing.Point(6, 20)
    Me.chk_group_by.Name = "chk_group_by"
    Me.chk_group_by.Size = New System.Drawing.Size(83, 17)
    Me.chk_group_by.TabIndex = 0
    Me.chk_group_by.Text = "xGroupBy"
    Me.chk_group_by.UseVisualStyleBackColor = True
    '
    'frm_tito_reports_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1365, 762)
    Me.Name = "frm_tito_reports_sel"
    Me.Text = "ticket_reports"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_state.ResumeLayout(False)
    Me.gb_state.PerformLayout()
    Me.gb_ticket_type.ResumeLayout(False)
    Me.gb_ticket_type.PerformLayout()
    Me.gb_date_filter.ResumeLayout(False)
    Me.gb_date_filter.PerformLayout()
    Me.gb_cashiers.ResumeLayout(False)
    Me.gb_machine_type.ResumeLayout(False)
    Me.gb_machine_type.PerformLayout()
    Me.gb_group_by.ResumeLayout(False)
    Me.gb_group_by.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents gb_state As System.Windows.Forms.GroupBox
  Friend WithEvents ds_from_to As GUI_Controls.uc_daily_session_selector
  Friend WithEvents chk_redeemed As System.Windows.Forms.CheckBox
  Friend WithEvents chk_expired As System.Windows.Forms.CheckBox
  Friend WithEvents chk_valid As System.Windows.Forms.CheckBox
  Friend WithEvents uc_terminals_list As GUI_Controls.uc_provider
  Friend WithEvents gb_ticket_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_promo_nonredeem_ticket As System.Windows.Forms.CheckBox
  Friend WithEvents chk_promo_redeem_ticket As System.Windows.Forms.CheckBox
  Friend WithEvents chk_redeemable_ticket As System.Windows.Forms.CheckBox
  Friend WithEvents gb_date_filter As System.Windows.Forms.GroupBox
  Friend WithEvents gb_cashiers As System.Windows.Forms.GroupBox
  Friend WithEvents dg_cashiers As GUI_Controls.uc_grid
  Friend WithEvents btn_uncheck_all As System.Windows.Forms.Button
  Friend WithEvents btn_check_all As System.Windows.Forms.Button
  Friend WithEvents chk_canceled As System.Windows.Forms.CheckBox
  Friend WithEvents uc_account_sel As GUI_Controls.uc_account_sel
  Friend WithEvents chk_handpay As System.Windows.Forms.CheckBox
  Friend WithEvents chk_jackpot As System.Windows.Forms.CheckBox
  Friend WithEvents gb_machine_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_terminals As System.Windows.Forms.CheckBox
  Friend WithEvents chk_cashiers As System.Windows.Forms.CheckBox
  Friend WithEvents ef_ticket_number As GUI_Controls.uc_entry_field
  Friend WithEvents chk_discarded As System.Windows.Forms.CheckBox
  Friend WithEvents gb_group_by As System.Windows.Forms.GroupBox
  Friend WithEvents opt_terminal_creation As System.Windows.Forms.RadioButton
  Friend WithEvents opt_date_creation As System.Windows.Forms.RadioButton
  Friend WithEvents chk_group_by As System.Windows.Forms.CheckBox
  Friend WithEvents chk_detailed_report As System.Windows.Forms.CheckBox
  Friend WithEvents chk_offline As System.Windows.Forms.CheckBox
  Friend WithEvents opt_expiration As System.Windows.Forms.RadioButton
  Friend WithEvents opt_cancelation As System.Windows.Forms.RadioButton
  Friend WithEvents opt_creation As System.Windows.Forms.RadioButton
  Friend WithEvents chk_expired_redeemed As System.Windows.Forms.CheckBox
  Friend WithEvents chk_pending_payment_after_expiration As System.Windows.Forms.CheckBox
  Friend WithEvents chk_pending_payment As System.Windows.Forms.CheckBox
  Friend WithEvents chk_swap_chips As System.Windows.Forms.CheckBox
  Friend WithEvents chk_pending_print As System.Windows.Forms.CheckBox
  Friend WithEvents chk_pending_cancel As System.Windows.Forms.CheckBox

End Class
