'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_new_collection
' DESCRIPTION:   
' AUTHOR:        Humberto Braojos
' CREATION DATE: 02-Aug-2012
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -----------------------------------------------
' 02-JUL-2013 HBB    First Release
' 21-AUG-2013 JRM    Adjusted form to new fields in table, refactored several methods
' 18-DEC-2013 ACM    Fixed Bug #WIGOSTITO-864. Set only Sas-Host terminals to terminals filter control.
' 24-DEC-2013 JPJ    Modified grid size and take into account selection when tab is changed
'-------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common.Misc
Imports System.Data.SqlClient
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common.Users
Imports GUI_Reports.PrintDataset
Imports System.Text
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports GUI_Controls.frm_base_sel
Imports WSI.Common.TITO
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE

Public Class frm_new_collection_edit
  Inherits frm_base_edit

#Region "Contants"

  Const GRID_BILLS_COLUMNS_COUNT As Int32 = 4
  Const GRID_TICKETS_COLUMNS_COUNT As Int32 = 5
  Const GRID_HEADERS_COUNT As Int32 = 2

  'Index Columns for tickets
  Const GRID_COLUMN_TICKET_NUMBER_CORRECT_FORMAT As Int32 = 0
  Const GRID_COLUMN_TICKET_AMOUNT As Int32 = 1
  Const GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF As Int32 = 2
  Const GRID_COLUMN_TICKET_NUMBER_ORIGINAL_FORMAT As Int32 = 3
  Const GRID_COLUMN_TICKET_ID As Int32 = 4

  'Index Columns for bills
  Const GRID_COLUMN_BILL_DENOMINATION As Int32 = 0
  Const GRID_COLUMN_BILL_REAL_COUNT As Int32 = 1
  Const GRID_COLUMN_BILL_THEO_COUNT As Int32 = 2
  Const GRID_COLUMN_BILL_REAL_VS_THEO_DIFF As Int32 = 3

  'Width FOR COLLECTION_DETAILS
  Const GRID_WIDTH_TICKET_OR_NOTE As Int32 = 0
  Const GRID_WIDTH_TICKET_VALIDATION_NUMBER As Int32 = 3510
  Const GRID_WIDTH_TICKET_AMOUNT As Int32 = 2535
  Const GRID_WIDTH_TICKET_VALIDATION_TYPE As Int32 = 0
  Const GRID_WIDTH_TICKET_ID As Int32 = 0

  'Const GRID_WIDTH_TICKET_INSERTED As Int32 = 0
  Const GRID_WIDTH_BILL_DENOMINATION As Int32 = 2130
  Const GRID_WIDTH_BILL_REAL_COUNT As Int32 = 1980
  Const GRID_WIDTH_BILL_THEO_COUNT As Int32 = 1935
  Const GRID_WIDTH_BILL_REAL_VS_THEO_DIFF As Int32 = 1320

  ''SQL COLUMNS FOR COLLECTION DETAILS
  'Const SQL_COLUMN_TICKET_OR_NOTE As Int32 = 0
  'Const SQL_COLUMN_COLLECTION_ID As Int32 = 1
  'Const SQL_COLUMN_TICKET_ID As Int32 = 2
  'Const SQL_COLUMN_TICKET_AMOUNT As Int32 = 3
  'Const SQL_COLUMN_BILL_DENOMINATION As Int32 = 4
  'Const SQL_COLUMN_BILL_REAL_COUNT As Int32 = 5
  'Const SQL_COLUMN_BILL_THEO_COUNT As Int32 = 6
  'Const SQL_COLUMN_BILL_THEO_VS_REAL_DIFF As Int32 = 7

  'SQL COLUMNS
  Const SQL_COLUMN_TICKET_OR_NOTE_1 As Int32 = 0
  Const SQL_COLUMN_TICKET_NUMBER As Int32 = 1
  Const SQL_COLUMN_TICKET_THEORETICAL_VALUE As Int32 = 2
  Const SQL_COLUMN_DENOMINATION As Int32 = 3
  Const SQL_COLUMN_THEORETICAL_QUANTITY As Int32 = 4
  Const SQL_COLUMN_TICKET_ID = 5

  'SQL COLUMNS FOR COLLECTION QUERY
  Const SQL_COLUMN_COLLECTION_TERMINAL_ID As Int32 = 0
  Const SQL_COLUMN_COLLECTION_TERMINAL_NAME As Int32 = 1
  Const SQL_COLUMN_COLLECTION_INSERTED_DATE_TIME As Int32 = 2
  Const SQL_COLUMN_COLLECTION_EXTRACTION_DATE_TIME As Int32 = 3
  Const SQL_COLUMN_COLLECTION_TICKETS_COUNT As Int32 = 4
  Const SQL_COLUMN_COLLECTION_AMOUNT_IN_TICKETS As Int32 = 5
  Const SQL_COLUMN_COLLECTION_BILLS_COUNT As Int32 = 6
  Const SQL_COLUMN_COLLECTION_AMOUNT_IN_BILLS As Int32 = 7

  'Ticket or note
  Const IS_TICKET = 0
  Const IS_TICKET_INSERTED = 1
  Const IS_NOTE = 2

  'ticket exist or not
  Const TICKETDONTBELONG = -1
  Const TICKETDONTEXIST = -2
  Const TICKETNUMBERREPEATED = 2

  'Counters
  Private Const COUNTER_TOTAL_ITEMS As Integer = 0
  Private Const COUNTER_INFO As Integer = 1
  Private Const COUNTER_WARNING As Integer = 2
  Private Const COUNTER_ERROR As Integer = 3
  Private Const MAX_COUNTERS As Integer = 4

  'Counters colors
  Private Const COLOR_ERROR_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  Private Const COLOR_ERROR_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_WARNING_BACK = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
  Private Const COLOR_WARNING_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_INFO_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_INFO_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_ALL_ITEMS_BACK = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_ALL_ITEMS_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00

#End Region

#Region "Members"

  Private m_bills_count As Int32
  Private m_tickets_count As Int32
  Private m_collected_in_tickets As Double
  Private m_collected_in_notes As Double
  Private m_stacker_id As Int64
  Private m_theoretical_amount As Double
  Private m_theoretical_notes_count As Int32
  Private m_theoretical_tickets_count As Int32
  Private m_detail_collection_level As Int32
  Private m_new_collecion_mode As Boolean
  Private m_session_id As Int64
  Private m_terminal_id As Int32
  Private m_terminal_name As String
  Private m_old_value As Int32
  Private m_collection_refused As Boolean
  Private m_collection_started As Boolean
  Private m_collection_id As Int64
  Public TICKET_ID_OF_REPEATED_NUMBER As Int64

#End Region

#Region "Overrides functions"

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_NEW_COLLECTION
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    MyBase.GUI_InitControls()

    Dim _terminal_id_list(0) As Int64

    _terminal_id_list(0) = m_terminal_id

    m_detail_collection_level = GeneralParam.GetInt32("TITO", "NoteAcCollectionType", -1)
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2272)

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_STATISTICS.GetString(2)
      Me.GUI_Button(ENUM_BUTTON.BUTTON_OK).Visible = False
    End If

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_CONTROLS.GetString(27)

    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = False

    ' Color code explanation
    Me.lbl_ok.BackColor = GetColor(COLOR_INFO_BACK)
    Me.lbl_ok1.BackColor = GetColor(COLOR_INFO_BACK)
    Me.lbl_error.BackColor = GetColor(COLOR_ERROR_BACK)
    Me.lbl_error1.BackColor = GetColor(COLOR_ERROR_BACK)

    'setting text to the entry fields
    Me.gb_collection_params.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2256)
    Me.ef_collection_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2330)
    Me.ef_insertion_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2251)
    Me.ef_extraction_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2263)
    Me.lb_stacker_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2252)
    Me.ef_employee.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2224)
    Me.ef_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2254)
    Me.lbl_notes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2255)
    Me.ef_ticket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2259)
    Me.gb_real_values.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2836)
    Me.gb_theoretical_values.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2837)
    Me.ef_real_total_recollected.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340)
    Me.ef_real_recolected_in_notes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2266)
    Me.ef_real_recolected_in_tickets.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2269)
    Me.gb_theoretical_values.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2837)
    Me.ef_theoretical_total_collected.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1340)
    Me.ef_theoretical_collected_in_notes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2266)
    Me.ef_theoretical_collected_in_tickets.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2269)
    Me.btn_save_tickets.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2262)
    Me.btn_start_collection.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2257)
    Me.tab_billetes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2266)
    Me.tab_ticket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2269)

    'setting properties to the entry fields
    Me.ef_ticket.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 18)
    Me.ef_real_total_recollected.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    Me.ef_theoretical_total_collected.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY)
    Me.ef_real_recolected_in_tickets.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    Me.ef_real_recolected_in_notes.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8, 2)
    Me.ef_theoretical_collected_in_tickets.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY)
    Me.ef_theoretical_collected_in_notes.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY)

    'making the entry fields read only
    Me.ef_employee.IsReadOnly = True
    Me.ef_theoretical_collected_in_notes.IsReadOnly = True
    Me.ef_theoretical_collected_in_tickets.IsReadOnly = True
    Me.ef_theoretical_total_collected.IsReadOnly = True
    Me.ef_terminal.IsReadOnly = True
    Me.ef_real_total_recollected.IsReadOnly = True
    Me.ef_real_recolected_in_notes.IsReadOnly = True
    Me.ef_real_recolected_in_tickets.IsReadOnly = True
    Me.ef_collection_date.IsReadOnly = True
    Me.ef_insertion_date.IsReadOnly = True
    Me.ef_extraction_date.IsReadOnly = True
    'Me.tb_notes.BackColor = Me.ef_extraction_date.BackColor 'Necessary to change the forecolor property
    'Me.tb_notes.ForeColor = Color.Blue
    ' Me.tb_notes.ReadOnly = False
    uc_pr_list.Enabled = False

    'making the entry fields editable
    Me.ef_ticket.IsReadOnly = False
    Me.cmb_stacker_id.Enabled = False

    Me.ef_ticket.TextVisible = True

    'Call Me.btn_start_collection.Focus()
    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      Call Me.dg_collection_bills.SelectFirstRow(True)
      Call Me.dg_collection_bills.Focus()
      Me.ActiveControl = Me.dg_collection_bills

    End If

    Me.gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1971)
    Me.gb_status1.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1971)
    Me.tf_ok.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1972)
    Me.tf_ok1.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1972)
    Me.tf_error.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1974)
    Me.tf_error1.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1974)

    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      ef_collection_date.Visible = False
      'ef_notes.Location = ef_collection_date.Location
    Else
      dg_collection_bills.Enabled = False
      Me.uc_pr_list.Visible = False
      Me.ef_terminal.Location = New Point(12, 54)
      Me.ef_employee.Location = New Point(12, 84)
      Me.ef_extraction_date.Location = New Point(357, 84)
      Me.ef_insertion_date.Location = New Point(357, 54)
      Me.ef_collection_date.Location = New Point(357, 114)
      Me.lbl_notes.Location = New Point(75, 144)
      Me.tb_notes.Size = New Size(511, 34)
      Me.tb_notes.Location = New Point(113, 144)
      Me.gb_collection_params.Size = New Size(651, 185)
      Me.gb_real_values.Location = New Point(7, 196)
      Me.gb_theoretical_values.Location = New Point(335, 196)
      Me.tb_collections.Location = New Point(7, 282)
      Me.Size = New Size(778, 644)

    End If

    GUI_StyleSheet()

    ' setting properties to uc_provider control
    Dim _terminal_types() As Integer = Nothing
    ReDim Preserve _terminal_types(0 To 0)
    _terminal_types(0) = TerminalTypes.SAS_HOST

    Call Me.uc_pr_list.Init(_terminal_types, uc_provider.UC_FILTER_TYPE.ONLY_ONE_TERMINAL)
    Me.uc_pr_list.SetTerminalIdListSelected(_terminal_id_list)
    Me.uc_pr_list.ExpandSelectedTerminal()


  End Sub

  Protected Overrides Sub GUI_SetInitialFocus()

    'If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
    '  Me.ActiveControl = Me.cmb_stacker_id
    'Else
    '  Me.ActiveControl = Me.gb_collection_params
    'End If

  End Sub 'GUI_SetInitialFocus

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    If Not m_collection_started Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2279), _
                                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                      ENUM_MB_BTN.MB_BTN_OK)
      cmb_stacker_id.Focus()
      ef_ticket.TextValue = ""

      Return False
    End If

    Return True
  End Function

  ' PURPOSE: It sets the read values from the database to the entry fields
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  'Note: If DbReadObject has some properties = -1, it means that there were errors reading the database
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _new_collection As NEW_COLLECTION
    Dim _idx As Int32
    Dim _ticket_inserted As Boolean             ' to specify if the ticket denomination has been inserted
    Dim _denomination_inserted As Boolean       ' to specify if the current denomination has been inserted

    _new_collection = DbReadObject
    _idx = 0
    _ticket_inserted = False
    _denomination_inserted = False
    _new_collection.m_new_collecion_mode = Me.m_new_collecion_mode
    Try

      If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT AndAlso Not m_new_collecion_mode Then

        cmb_stacker_id.Enabled = False
        'uc_pr_list.Enabled = False
        btn_start_collection.Enabled = False
        ef_ticket.Enabled = False
        tb_notes.ReadOnly = True
        tb_notes.BackColor = Me.ef_extraction_date.BackColor 'Necessary to change the forecolor property
        tb_notes.ForeColor = Color.Blue
        btn_save_tickets.Enabled = False
        ef_collection_date.Visible = True
        btn_start_collection.Visible = False

        If Not _new_collection.CollectionDate.IsNull Then
          ef_collection_date.TextValue = GUI_FormatDate(_new_collection.CollectionDate.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
        End If

        'cmb_stacker_id.Items.Insert(0, _new_collection.StackerId)
        cmb_stacker_id.Items.Add(IIf(_new_collection.StackerId = 0, "", _new_collection.StackerId))
        cmb_stacker_id.SelectedIndex = 0
        ef_real_recolected_in_notes.TextValue = GUI_FormatCurrency(_new_collection.RealBillsSum)
        ef_real_recolected_in_tickets.TextValue = GUI_FormatCurrency(_new_collection.RealTicketSum)
        ef_real_total_recollected.TextValue = GUI_FormatCurrency(_new_collection.RealBillsSum + _new_collection.RealTicketSum)
        ef_insertion_date.TextValue = GUI_FormatDate(_new_collection.InsertionDate.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
        ef_theoretical_collected_in_notes.TextValue = GUI_FormatCurrency(_new_collection.TheoreticalBillsSum)
        ef_theoretical_collected_in_tickets.TextValue = GUI_FormatCurrency(_new_collection.TheoreticalTicketSum)
        ef_theoretical_total_collected.TextValue = GUI_FormatCurrency(_new_collection.TheoreticalBillsSum + _new_collection.TheoreticalTicketSum)
        Call SetupDataGrid(_new_collection, True)
        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = ((Me.dg_collection_bills.NumRows > 0) Or (Me.dg_collection_tickets.NumRows > 0))

      Else
        GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = False

        If GeneralParam.GetInt32("TITO", "NoteAcCollectionType", -1) > 0 Then
          ef_theoretical_collected_in_notes.TextValue = GUI_FormatCurrency(_new_collection.TheoreticalBillsSum)
          ef_theoretical_collected_in_tickets.TextValue = GUI_FormatCurrency(_new_collection.TheoreticalTicketSum)
          ef_theoretical_total_collected.TextValue = GUI_FormatCurrency(_new_collection.TheoreticalBillsSum + _new_collection.TheoreticalTicketSum)
          If WSI.Common.GeneralParam.GetInt32("TITO", "TicketsCollection") = 0 Then
            ef_real_recolected_in_tickets.TextValue = GUI_FormatCurrency(_new_collection.TheoreticalTicketSum)
            m_collected_in_tickets = _new_collection.TheoreticalTicketSum
            m_tickets_count = _new_collection.TheoreticalTicketCount
          End If
        End If

        ef_ticket.Enabled = (WSI.Common.GeneralParam.GetInt32("TITO", "TicketsCollection") <> 0)
        btn_save_tickets.Enabled = (WSI.Common.GeneralParam.GetInt32("TITO", "TicketsCollection") <> 0)

        Call SetupDataGrid(_new_collection, False)

        Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
        ef_collection_date.Visible = False

      End If
      If Not _new_collection.ExtractionDate.IsNull Then
        ef_extraction_date.TextValue = GUI_FormatDate(_new_collection.ExtractionDate.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      End If
      If Not _new_collection.InsertionDate.IsNull Then
        ef_insertion_date.TextValue = GUI_FormatDate(_new_collection.InsertionDate.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
      End If
      ef_terminal.TextValue = _new_collection.TerminalName
      ef_employee.TextValue = _new_collection.UserName
      tb_notes.Text = _new_collection.Notes
      m_collection_id = _new_collection.MoneyCollectionId

      If dg_collection_bills.NumRows > 0 Then
        Me.dg_collection_bills.Row(0).IsSelected = True
        Me.dg_collection_bills.CurrentRow = 0
        Call Me.dg_collection_bills.RepaintRow(0)


      End If

    Catch
    End Try

  End Sub

  ' PURPOSE : Database overridable operations. 
  '           Define specific DB operation for this form.
  '
  '  PARAMS :
  '     - INPUT :
  '     - OUTPUT :
  '
  ' RETURNS :
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Dim _new_collection As NEW_COLLECTION

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New NEW_COLLECTION()
        _new_collection = DbEditedObject
        _new_collection.CollectionDate = New TYPE_DATE_TIME()
        _new_collection.CollectionDate.IsNull = True
        _new_collection.InsertionDate = New TYPE_DATE_TIME()
        _new_collection.ExtractionDate = New TYPE_DATE_TIME()
        _new_collection.CollectionDetails = New List(Of NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)()
        _new_collection.m_new_collecion_mode = Me.m_new_collecion_mode
        _new_collection.UserId = CurrentUser.Id
        _new_collection.UserName = CurrentUser.Name
        _new_collection.StackerId = m_stacker_id
        _new_collection.SessionId = m_session_id
        _new_collection.TerminalId = m_terminal_id
        _new_collection.MoneyCollectionId = m_collection_id

      Case ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE
        DbReadObject = DbEditedObject.Duplicate()

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE

        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2368), _
                      ENUM_MB_TYPE.MB_TYPE_WARNING, _
                      ENUM_MB_BTN.MB_BTN_YES_NO, , _
                      GetStringToMessageBox(DbEditedObject)) = ENUM_MB_RESULT.MB_RESULT_NO Then
          DbStatus = ENUM_STATUS.STATUS_ERROR 'the update has been rejected
          m_collection_refused = True

          Return
        Else
          DbStatus = ENUM_STATUS.STATUS_OK

        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        If DbStatus = ENUM_STATUS.STATUS_OK Then 'UPDATE_REQUIRED
          Call MyBase.GUI_DB_Operation(DbOperation)
          Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT _
         , ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE

        If DbStatus <> ENUM_STATUS.STATUS_ERROR Then
          Call ExcelPrintDetails(True, True)
        ElseIf Not m_collection_refused Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2535), _
                ENUM_MB_TYPE.MB_TYPE_ERROR, _
                ENUM_MB_BTN.MB_BTN_OK, _
                ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_AUDIT_UPDATE
        DbEditedObject.AuditorData.Notify(CurrentUser.GuiId, _
                                            CurrentUser.Id, _
                                            CurrentUser.Name, _
                                            CLASS_AUDITOR_DATA.ENUM_AUDITOR_OPERATIONS.INSERT, _
                                            0, _
                                            DbReadObject.AuditorData)

      Case ENUM_DB_OPERATION.DB_OPERATION_READ
        Call MyBase.GUI_DB_Operation(DbOperation)
        If DbStatus = ENUM_STATUS.STATUS_ERROR Then
          cmb_stacker_id.Text = ""
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select
  End Sub

  ' PURPOSE: It gets the values from the entry field and set them to a PASSWORD_CONFIGURATION object
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim _new_collection As NEW_COLLECTION
    Dim _new_collection_details_list As List(Of NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)

    _new_collection_details_list = New List(Of NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)
    _new_collection = DbEditedObject

    If dg_collection_bills.NumRows > 0 Then
      _new_collection.m_new_collecion_mode = Me.m_new_collecion_mode

      If Not String.IsNullOrEmpty(ef_real_recolected_in_notes.Value) Then
        _new_collection.RealBillsSum = CDbl(ef_real_recolected_in_notes.Value)
        _new_collection.RealBillsCount = m_bills_count
      Else
        _new_collection.RealBillsCount = 0
        _new_collection.RealBillsSum = 0.0
      End If

      _new_collection.RealTicketSum = m_collected_in_tickets
      _new_collection.RealTicketCount = m_tickets_count

      _new_collection.Notes = tb_notes.Text

      If Me.m_new_collecion_mode Then
        If Math.Round(_new_collection.TheoreticalBillsSum + _new_collection.TheoreticalTicketSum, 2) = Math.Round(_new_collection.RealBillsSum + _new_collection.RealTicketSum, 2) AndAlso m_bills_count = _new_collection.TheoreticalBillsCount AndAlso _new_collection.RealTicketCount = _new_collection.TheoreticalTicketCount Then
          _new_collection.Balanced = True
        Else
          _new_collection.Balanced = False
        End If
      End If
    ElseIf m_stacker_id > 0 Then
      _new_collection.StackerId = m_stacker_id
      _new_collection.Notes = ""
    End If
    _new_collection.CollectionDate.Value = WGDB.Now
    _new_collection.CollectionDetails = FillDetailsList()

    For Each _detail As NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS In _new_collection.CollectionDetails
      _new_collection.Balanced = (_new_collection.Balanced And _detail.matched)
    Next

    Try

    Catch

    End Try

  End Sub

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '       - DrawId
  '       - DrawName
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowEditItem(ByVal StackerCollectionId As Int64)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.m_new_collecion_mode = False
    Me.DbObjectId = StackerCollectionId
    'ef_ticket.Enabled = False

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Me.Display(True)
      btn_start_collection.Focus()
    End If



  End Sub

  ' PURPOSE: Init form in new mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowNewItem()

    ' Set the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW
    Me.m_new_collecion_mode = True
    Me.m_session_id = -1
    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

      btn_start_collection.Focus()
    End If
    Me.Display(True)

  End Sub 'ShowNewItem

  ' PURPOSE: Init form in new mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowNewItem(ByVal StackerId As Int64, Optional ByVal CashierSessionId As Int64 = -1, _
                                   Optional ByVal TerminalName As String = "", Optional ByVal CollectionId As Int64 = -1)

    Dim _terminal_id As Int64
    ' Set the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW
    Me.m_new_collecion_mode = True
    Me.m_session_id = CashierSessionId
    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    If StackerId <> -1 Then
      m_stacker_id = StackerId
    Else
      Call GetStackerId()
    End If

    If CollectionId <> -1 Then
      m_collection_id = CollectionId
    End If

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

      If m_stacker_id > 0 Then
        cmb_stacker_id.Text = m_stacker_id.ToString()
      End If

      _terminal_id = GetTerminalId(TerminalName)

      If _terminal_id > 0 Then
        m_terminal_id = _terminal_id
      End If

    End If

    StartNewCollection()

    Me.Display(True)
    'btn_start_collection.Focus()
    dg_collection_bills.Focus()

  End Sub 'ShowNewItem

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Select Case ButtonId
      'Excel button
      Case ENUM_BUTTON.BUTTON_CUSTOM_2
        Call ExcelPrintDetails(False, False)
        Call MyBase.AuditFormClick(ButtonId)
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

  Protected Overrides Function GUI_GetAuditFormType(ByVal ButtonId As ENUM_BUTTON) As AUDIT_FLAGS
    Dim _form_type As AUDIT_FLAGS

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_CUSTOM_2
        _form_type = AUDIT_FLAGS.EXCEL

      Case Else
        _form_type = MyBase.GUI_GetAuditFormType(ButtonId)
    End Select

    Return _form_type
  End Function

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean
    If e.KeyChar = Chr(Keys.Enter) Then

      Select Case Me.ActiveControl.Name
        Case cmb_stacker_id.Name
          Call StartNewCollection()
        Case ef_ticket.Name
          Call btn_save_tickets_ClickEvent()
        Case (dg_collection_bills.Name)
          Call dg_collection_bills.KeyPressed(sender, e)
          m_old_value = CInt(dg_collection_bills.Value(dg_collection_bills.CurrentRow, dg_collection_bills.CurrentCol))
        Case Else
          MyBase.GUI_KeyPress(sender, e)
      End Select

      Return True
    End If

  End Function ' GUI_ButtonClick

#End Region

#Region "Public functions"

  'End Sub
  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS : 
  '     - True (the row should be added) or False (the row can not be added)
  '
  '   NOTES :
  '
  Public Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal CollectionDetail As NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS, ByVal EditMode As Boolean) As Boolean
    Dim _idxBackgroundColor As Int16
    Dim _idxForeColor As Int16

    If CollectionDetail.ticket_validation_number > 0 AndAlso (EditMode OrElse GeneralParam.GetInt32("TITO", "NoteAcCollectionType", -1) = 2) Then
      dg_collection_tickets.AddRow()

      'setting the validation number to the cell
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_NUMBER_CORRECT_FORMAT).Value = ValidationNumberManager.FormatValidationNumber(CollectionDetail.ticket_validation_number, CollectionDetail.ticket_validation_type, False)
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_NUMBER_ORIGINAL_FORMAT).Value = CollectionDetail.ticket_validation_number
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_AMOUNT).Value = GUI_FormatCurrency(CollectionDetail.ticket_amount)
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_ID).Value = CollectionDetail.ticket_id
      ' Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_AMOUNT).Value

      'the ticket collection could be taken as they are all collected, so they will appear as collected since the begining of the collection
      'the difference column will be in blank
      If (WSI.Common.GeneralParam.GetInt32("TITO", "TicketsCollection") = 0 AndAlso Not CollectionDetail.ticket_collected) OrElse CollectionDetail.ticket_collected Then
        _idxBackgroundColor = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
        _idxForeColor = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
        Me.dg_collection_bills.Counter(COUNTER_INFO).Value += 1

      Else
        _idxBackgroundColor = ENUM_GUI_COLOR.GUI_COLOR_RED_00
        _idxForeColor = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
        Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = -1
        dg_collection_tickets.Counter(COUNTER_WARNING).Value += 1
      End If

      dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).BackColor = GetColor(_idxBackgroundColor)
      dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).ForeColor = GetColor(_idxForeColor)
    End If

    If CollectionDetail.bill_denomination > 0 Then
      dg_collection_bills.AddRow()
      Me.dg_collection_bills.Cell(dg_collection_bills.NumRows - 1, GRID_COLUMN_BILL_DENOMINATION).Value = GUI_FormatCurrency(CollectionDetail.bill_denomination)
      Me.dg_collection_bills.Cell(dg_collection_bills.NumRows - 1, GRID_COLUMN_BILL_REAL_COUNT).Value = CollectionDetail.bill_real_count
      Me.dg_collection_bills.Cell(dg_collection_bills.NumRows - 1, GRID_COLUMN_BILL_THEO_COUNT).Value = CollectionDetail.bill_theoretical_count
      Me.dg_collection_bills.Cell(dg_collection_bills.NumRows - 1, GRID_COLUMN_BILL_REAL_VS_THEO_DIFF).Value = _
      IIf(CollectionDetail.bill_real_vs_theo_count_diff <> 0, CollectionDetail.bill_real_vs_theo_count_diff, "")

      Select Case CollectionDetail.bill_real_vs_theo_count_diff
        Case Is = 0
          _idxBackgroundColor = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
          _idxForeColor = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
        Case Else
          _idxBackgroundColor = ENUM_GUI_COLOR.GUI_COLOR_RED_00
          _idxForeColor = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
      End Select
      dg_collection_bills.Cell(dg_collection_bills.NumRows - 1, GRID_COLUMN_BILL_REAL_VS_THEO_DIFF).BackColor = GetColor(_idxBackgroundColor)
      dg_collection_bills.Cell(dg_collection_bills.NumRows - 1, GRID_COLUMN_BILL_REAL_VS_THEO_DIFF).ForeColor = GetColor(_idxForeColor)
    End If

    Return True
  End Function ' GUI_SetupRow

#End Region

#Region "Private functions"

  Private Sub GetStackerId()
    Dim _sql_str As StringBuilder
    Dim _table As DataTable

    _table = New DataTable()
    _sql_str = New StringBuilder()

    _sql_str.AppendLine(" SELECT   MC_STACKER_ID                            ")
    _sql_str.AppendLine("        , MC_TERMINAL_ID                           ")
    _sql_str.AppendLine("   FROM   MONEY_COLLECTIONS                        ")
    _sql_str.AppendLine("  WHERE   MC_CASHIER_SESSION_ID = @pCashierSession ")

    Try
      Using _db_trx As New DB_TRX()
        Using _cmd As SqlCommand = New SqlCommand(_sql_str.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pCashierSession", SqlDbType.BigInt).Value = m_session_id
          Using _sql_da As New SqlDataAdapter(_cmd)
            _db_trx.Fill(_sql_da, _table)

          End Using
        End Using
      End Using

      If _table.Rows.Count > 0 Then
        If _table.Rows(0).Item(0) IsNot DBNull.Value Then
          m_stacker_id = _table.Rows(0).Item(0)
        End If
        If _table.Rows(0).Item(1) Then
          m_terminal_id = _table.Rows(0).Item(1)
        End If
      End If

    Catch _ex As Exception
      m_stacker_id = -1
    End Try
  End Sub

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    With Me.dg_collection_bills

      Call .Init(GRID_BILLS_COLUMNS_COUNT, GRID_HEADERS_COUNT, True)

      .Counter(COUNTER_TOTAL_ITEMS).Visible = True
      .Counter(COUNTER_TOTAL_ITEMS).BackColor = GetColor(COLOR_ALL_ITEMS_BACK)
      .Counter(COUNTER_TOTAL_ITEMS).ForeColor = GetColor(COLOR_ALL_ITEMS_FORE)

      .Counter(COUNTER_INFO).Visible = True
      .Counter(COUNTER_INFO).BackColor = GetColor(COLOR_INFO_BACK)
      .Counter(COUNTER_INFO).ForeColor = GetColor(COLOR_INFO_FORE)

      .Counter(COUNTER_WARNING).Visible = True
      .Counter(COUNTER_WARNING).BackColor = GetColor(COLOR_WARNING_BACK)
      .Counter(COUNTER_WARNING).ForeColor = GetColor(COLOR_WARNING_FORE)

      .Counter(COUNTER_ERROR).Visible = True
      .Counter(COUNTER_ERROR).BackColor = GetColor(COLOR_ERROR_BACK)
      .Counter(COUNTER_ERROR).ForeColor = GetColor(COLOR_ERROR_FORE)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      '.Counter(0).Visible = False
      .Sortable = False

      ' Denomination
      .Column(GRID_COLUMN_BILL_DENOMINATION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2266)
      .Column(GRID_COLUMN_BILL_DENOMINATION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2270)
      .Column(GRID_COLUMN_BILL_DENOMINATION).Width = GRID_WIDTH_BILL_DENOMINATION
      .Column(GRID_COLUMN_BILL_DENOMINATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' .Column(GRID_COLUMN_BILL_DENOMINATION).HighLightWhenSelected = True

      ' Real quantity
      .Column(GRID_COLUMN_BILL_REAL_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2266)
      .Column(GRID_COLUMN_BILL_REAL_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2836)
      .Column(GRID_COLUMN_BILL_REAL_COUNT).Width = GRID_WIDTH_BILL_REAL_COUNT
      .Column(GRID_COLUMN_BILL_REAL_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_BILL_REAL_COUNT).Editable = True
      .Column(GRID_COLUMN_BILL_REAL_COUNT).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_BILL_REAL_COUNT).EditionControl.EntryField.IsReadOnly = False
      .Column(GRID_COLUMN_BILL_REAL_COUNT).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 3)
      .Column(GRID_COLUMN_BILL_REAL_COUNT).EditionControl.EntryField.Font = New Font("Curier New", 10, FontStyle.Bold)
      ' .Column(GRID_COLUMN_BILL_REAL_COUNT).HighLightWhenSelected = True

      ' Theoretical quantity
      .Column(GRID_COLUMN_BILL_THEO_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2266)
      .Column(GRID_COLUMN_BILL_THEO_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2837)
      .Column(GRID_COLUMN_BILL_THEO_COUNT).Width = GRID_WIDTH_BILL_THEO_COUNT
      .Column(GRID_COLUMN_BILL_THEO_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_BILL_THEO_COUNT).HighLightWhenSelected = True

      ' Balanced
      .Column(GRID_COLUMN_BILL_REAL_VS_THEO_DIFF).Header(0).Text = "" 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(2274)
      .Column(GRID_COLUMN_BILL_REAL_VS_THEO_DIFF).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2278)
      .Column(GRID_COLUMN_BILL_REAL_VS_THEO_DIFF).Width = GRID_WIDTH_BILL_REAL_VS_THEO_DIFF
      .Column(GRID_COLUMN_BILL_REAL_VS_THEO_DIFF).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      ' .Column(GRID_COLUMN_BILL_REAL_VS_THEO_DIFF).HighLightWhenSelected = True

    End With

    With Me.dg_collection_tickets

      Call .Init(GRID_TICKETS_COLUMNS_COUNT, GRID_HEADERS_COUNT)

      .Counter(COUNTER_TOTAL_ITEMS).Visible = False
      .Counter(COUNTER_TOTAL_ITEMS).BackColor = GetColor(COLOR_ALL_ITEMS_BACK)
      .Counter(COUNTER_TOTAL_ITEMS).ForeColor = GetColor(COLOR_ALL_ITEMS_FORE)

      .Counter(COUNTER_INFO).Visible = False
      .Counter(COUNTER_INFO).BackColor = GetColor(COLOR_INFO_BACK)
      .Counter(COUNTER_INFO).ForeColor = GetColor(COLOR_INFO_FORE)

      .Counter(COUNTER_WARNING).Visible = False
      .Counter(COUNTER_WARNING).BackColor = GetColor(COLOR_WARNING_BACK)
      .Counter(COUNTER_WARNING).ForeColor = GetColor(COLOR_WARNING_FORE)

      .Counter(COUNTER_ERROR).Visible = False
      .Counter(COUNTER_ERROR).BackColor = GetColor(COLOR_ERROR_BACK)
      .Counter(COUNTER_ERROR).ForeColor = GetColor(COLOR_ERROR_FORE)

      ' Ticket number
      .Column(GRID_COLUMN_TICKET_NUMBER_CORRECT_FORMAT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2269)
      .Column(GRID_COLUMN_TICKET_NUMBER_CORRECT_FORMAT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2259)
      .Column(GRID_COLUMN_TICKET_NUMBER_CORRECT_FORMAT).Width = GRID_WIDTH_TICKET_VALIDATION_NUMBER
      .Column(GRID_COLUMN_TICKET_NUMBER_CORRECT_FORMAT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Ticket value
      .Column(GRID_COLUMN_TICKET_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2269)
      .Column(GRID_COLUMN_TICKET_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2260)
      .Column(GRID_COLUMN_TICKET_AMOUNT).Width = GRID_WIDTH_TICKET_AMOUNT
      .Column(GRID_COLUMN_TICKET_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Balanced
      .Column(GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Header(0).Text = "" 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(2274)
      .Column(GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2278)
      .Column(GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Width = GRID_WIDTH_BILL_REAL_VS_THEO_DIFF
      .Column(GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      'Validation Type
      .Column(GRID_COLUMN_TICKET_NUMBER_ORIGINAL_FORMAT).Width = GRID_WIDTH_TICKET_VALIDATION_TYPE

      ' Ticket Id
      .Column(GRID_COLUMN_TICKET_ID).Width = GRID_WIDTH_TICKET_ID

    End With

    'Hidding the theoretical and difference columns when the form is open in read mode
    If Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW AndAlso GeneralParam.GetInt32("TITO", "NoteAcCollectionType", -1) <> 2 Then
      Me.dg_collection_tickets.Column(GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Width = 0
      Me.dg_collection_bills.Column(GRID_COLUMN_BILL_REAL_VS_THEO_DIFF).Width = 0
      Me.dg_collection_bills.Column(GRID_COLUMN_BILL_THEO_COUNT).Width = 0

    End If

  End Sub

  Private Function SearchRowOfTicket(ByVal Number As Int64, ByVal Column As Int32) As Int32

    Dim _idx As Int32

    For _idx = 0 To dg_collection_tickets.NumRows - 1
      If Number = dg_collection_tickets.Cell(_idx, Column).Value Then
        Return _idx
      End If
    Next

    Return -1

  End Function

  Private Sub StartNewCollection()

    dg_collection_bills.Clear()
    dg_collection_tickets.Clear()

    m_collection_started = True
    Me.ef_real_recolected_in_notes.TextValue = ""
    Me.ef_real_recolected_in_tickets.TextValue = ""
    Me.ef_real_total_recollected.TextValue = ""
    m_bills_count = 0
    m_collected_in_notes = 0
    m_tickets_count = 0
    m_collected_in_tickets = 0

    Dim _terminal_id As Long()

    If m_collection_id > 0 OrElse m_session_id > 0 Then

      tb_notes.BackColor = Color.White
      tb_notes.ForeColor = Color.Black
      tb_notes.ReadOnly = False
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

      If DbStatus = ENUM_STATUS.STATUS_OK Then
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
        'MyBase.GUI_FirstActivation()
      End If
      Return
    End If

    If cmb_stacker_id.Text = "" Then
      _terminal_id = uc_pr_list.GetTerminalIdListSelected()
      If Not (_terminal_id Is Nothing) Then
        m_terminal_id = _terminal_id.GetValue(0)
        Call FillStackersListByTerminal(m_terminal_id)

      End If

      Select Case cmb_stacker_id.Items.Count
        Case 0
          'in this case, the collection doesn't have a stacker but it does have a terminal associated
          If TerminalHasPendingCollection(m_terminal_id) Then

            tb_notes.BackColor = Color.White
            tb_notes.ForeColor = Color.Black
            tb_notes.ReadOnly = False
            Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
            Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
            Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
            Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

            If DbStatus = ENUM_STATUS.STATUS_OK Then
              Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
              MyBase.GUI_FirstActivation()
            End If

          Else
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2273), _
                                                           mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                                           ENUM_MB_BTN.MB_BTN_OK, , _
                                                           ef_ticket.TextValue)
            cmb_stacker_id.Focus()
            cmb_stacker_id.Text = ""
          End If

          Return

        Case 1
          cmb_stacker_id.SelectedIndex = 0
          If Not Int64.TryParse(cmb_stacker_id.Text, m_stacker_id) Then
            NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2273), _
                                                         mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                                         ENUM_MB_BTN.MB_BTN_OK, , _
                                                         ef_ticket.TextValue)
            cmb_stacker_id.Focus()
            cmb_stacker_id.Text = ""

            Return
          End If

        Case Is > 1
          cmb_stacker_id.SelectedIndex = -1
          NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2880), _
                                                           mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                                           ENUM_MB_BTN.MB_BTN_OK, , m_terminal_name)

          cmb_stacker_id.Focus()
          Return

      End Select

    ElseIf Not Int64.TryParse(cmb_stacker_id.Text, m_stacker_id) Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2273), _
                                                   mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                                   ENUM_MB_BTN.MB_BTN_OK, , _
                                                   ef_ticket.TextValue)
      cmb_stacker_id.Focus()
      cmb_stacker_id.Text = ""

      Return
    End If


    If (m_stacker_id > 0) Then

      tb_notes.BackColor = Color.White
      tb_notes.ForeColor = Color.Black
      tb_notes.ReadOnly = False
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

      If DbStatus = ENUM_STATUS.STATUS_OK Then
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
        MyBase.GUI_FirstActivation()
      End If

    End If

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = dg_collection_bills.NumRows > 0

    btn_save_tickets.Enabled = (WSI.Common.GeneralParam.GetInt32("TITO", "TicketsCollection") = 1)
    ef_ticket.IsReadOnly = (WSI.Common.GeneralParam.GetInt32("TITO", "TicketsCollection") <> 1)

  End Sub

  Private Sub AddTicketToCollection()

    Dim _row As Int32
    Dim _ticket_value As Double
    Dim _source_name As String
    Dim _description As String
    Dim _validation_number As Int64

    _ticket_value = TicketAmount(ef_ticket.Value)
    _validation_number = 0

    Select Case (_ticket_value)
      Case TICKETDONTEXIST
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2280), _
                                          mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                          ENUM_MB_BTN.MB_BTN_OK, , ef_ticket.Value)
        ef_ticket.TextValue = ""
        Return

      Case TICKETDONTBELONG
        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(4435), _
                                          mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                          ENUM_MB_BTN.MB_BTN_OK, , ef_ticket.Value)
        ef_ticket.TextValue = ""
        Return

      Case TICKETNUMBERREPEATED
        Dim _frm_select_ticket As frm_TITO_select_ticket
        _frm_select_ticket = New frm_TITO_select_ticket()
        _frm_select_ticket.ShowForEdit(Me.MdiParent, ef_ticket.Value)

    End Select

    'If _ticket_value < 0 Then
    '  NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2280), _
    '                                  mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
    '                                  ENUM_MB_BTN.MB_BTN_OK)
    '  ef_ticket.TextValue = ""
    '  Return

    'End If
    'If TICKET_ID_OF_REPEATED_NUMBER = 0 AndAlso _ticket_value <= 0 Then
    '  Return
    'Else
    If TICKET_ID_OF_REPEATED_NUMBER > 0 Then
      _row = SearchRowOfTicket(TICKET_ID_OF_REPEATED_NUMBER, GRID_COLUMN_TICKET_ID)
    Else
      If Int64.TryParse(ef_ticket.Value, _validation_number) Then
        _row = SearchRowOfTicket(_validation_number, GRID_COLUMN_TICKET_NUMBER_ORIGINAL_FORMAT)
      End If
    End If

    'if row = -1 it means that there is no theoretical ticket with this number
    If _row >= 0 Then
      If dg_collection_tickets.Cell(_row, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = "-1" Then
        dg_collection_tickets.Cell(_row, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = ""
        m_tickets_count = m_tickets_count + 1
        m_collected_in_tickets = m_collected_in_tickets + CDbl(_ticket_value)

        'Call SetColumnMismatch(_row, "", ENUM_GUI_COLOR.GUI_COLOR_GREEN_00)
        Call UpdateDifferenceAndColor(_row, 0, False)

      ElseIf dg_collection_tickets.Cell(_row, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = "" Then

        'generate an alarm if the ticket has been inserted before
        Using _db_trx As DB_TRX = New DB_TRX()
          _source_name = GLB_CurrentUser.Name & "@" & Environment.MachineName
          _description = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2263, ef_ticket.TextValue)

          If Alarm.Register(AlarmSourceCode.User, GLB_CurrentUser.Id, _source_name, _
                                AlarmCode.User_CashierSessionClosedFromGUI, _
                                _description, AlarmSeverity.Info, DateTime.MinValue, _db_trx.SqlTransaction) Then
            _db_trx.Commit()
          End If
        End Using

        NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2274), _
                                        mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, _
                                        ENUM_MB_BTN.MB_BTN_OK, , _
                                        ef_ticket.TextValue)
        ef_ticket.TextValue = ""
        Return

      End If

    ElseIf (GeneralParam.GetInt32("TITO", "NoteAcCollectionType", -1) <> 2 AndAlso _ticket_value > 0) Then
      dg_collection_tickets.AddRow()
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_NUMBER_CORRECT_FORMAT).Value = ValidationNumberManager.FormatValidationNumber(ef_ticket.TextValue, 1, False)
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_NUMBER_ORIGINAL_FORMAT).Value = ef_ticket.TextValue
      Me.dg_collection_tickets.Cell(dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_AMOUNT).Value = GUI_FormatCurrency(_ticket_value)
      m_tickets_count = m_tickets_count + 1
      m_collected_in_tickets = m_collected_in_tickets + CDbl(_ticket_value)

    End If

    Me.ef_real_recolected_in_tickets.TextValue = GUI_FormatCurrency(m_collected_in_tickets)
    Me.ef_real_total_recollected.TextValue = GUI_FormatCurrency(m_collected_in_tickets + m_collected_in_notes)

    ef_ticket.TextValue = ""

  End Sub

  Private Sub AddNoteToCollection(ByVal Row As Int32, ByVal Column As Int32)

    Dim _current_denomination As Double

    _current_denomination = GUI_ParseCurrency(dg_collection_bills.Cell(Row, GRID_COLUMN_BILL_DENOMINATION).Value)


    m_collected_in_notes = m_collected_in_notes + _current_denomination * GUI_ParseNumber(dg_collection_bills.Value(Row, Column)) - _current_denomination * GUI_ParseCurrency(m_old_value)
    m_bills_count = m_bills_count + GUI_ParseNumber(dg_collection_bills.Cell(Row, GRID_COLUMN_BILL_REAL_COUNT).Value) - GUI_ParseCurrency(m_old_value) ' GUI_ParseNumber(dg_collection_bills.Value(Row, Column))

    Call UpdateDifferenceAndColor(Row, CInt(dg_collection_bills.Cell(Row, GRID_COLUMN_BILL_REAL_COUNT).Value) - CInt(dg_collection_bills.Cell(Row, GRID_COLUMN_BILL_THEO_COUNT).Value), True)

    Me.ef_real_recolected_in_notes.TextValue = GUI_FormatCurrency(m_collected_in_notes)
    Me.ef_real_total_recollected.TextValue = GUI_FormatCurrency(m_collected_in_tickets + m_collected_in_notes)

    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = dg_collection_bills.NumRows > 0

  End Sub

  Private Function FillDetailsList() As List(Of NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)

    Dim _current_collection_detail As NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS
    Dim _list_of_collection_details As List(Of NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)
    Dim _idx As Int32

    _list_of_collection_details = New List(Of NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS)()

    For _idx = 0 To dg_collection_bills.NumRows - 1
      _current_collection_detail = New NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS()

      If dg_collection_bills.Value(_idx, GRID_COLUMN_BILL_REAL_COUNT) <> "" AndAlso (dg_collection_bills.Value(_idx, GRID_COLUMN_BILL_REAL_COUNT) <> 0 OrElse dg_collection_bills.Value(_idx, GRID_COLUMN_BILL_THEO_COUNT) <> 0) Then
        _current_collection_detail.bill_denomination = GUI_ParseCurrency(dg_collection_bills.Value(_idx, GRID_COLUMN_BILL_DENOMINATION))
        _current_collection_detail.bill_real_count = GUI_FormatNumber(dg_collection_bills.Value(_idx, GRID_COLUMN_BILL_REAL_COUNT))
        _current_collection_detail.bill_theoretical_count = IIf(dg_collection_bills.Value(_idx, GRID_COLUMN_BILL_THEO_COUNT) <> "", GUI_FormatNumber(dg_collection_bills.Value(_idx, GRID_COLUMN_BILL_THEO_COUNT)), 0)
        _current_collection_detail.type = 2 ' GUI_FormatNumber(dg_collection_bills.Value(_idx, GRID_COLUMN_TICKET_OR_NOTE))
        _current_collection_detail.matched = (dg_collection_bills.Value(_idx, GRID_COLUMN_BILL_REAL_COUNT) = dg_collection_bills.Value(_idx, GRID_COLUMN_BILL_THEO_COUNT)) '(dg_collection.Value(_idx, GRID_COLUMN_MISMATCH) = uc_grid.GRID_CHK_CHECKED)
        _list_of_collection_details.Add(_current_collection_detail)

      Else
        Continue For
      End If

    Next

    For _idx = 0 To dg_collection_tickets.NumRows - 1
      If dg_collection_tickets.Value(_idx, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF) <> "-1" Then
        _current_collection_detail = New NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS()
        _current_collection_detail.ticket_validation_number = dg_collection_tickets.Value(_idx, GRID_COLUMN_TICKET_NUMBER_ORIGINAL_FORMAT)
        _current_collection_detail.ticket_id = dg_collection_tickets.Value(_idx, GRID_COLUMN_TICKET_ID)
        _current_collection_detail.ticket_amount = GUI_ParseCurrency(dg_collection_tickets.Value(_idx, GRID_COLUMN_TICKET_AMOUNT))
        _current_collection_detail.matched = (dg_collection_tickets.Value(_idx, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF) = "")
        _current_collection_detail.type = 1 ' GUI_FormatNumber(dg_collection_bills.Value(_idx, GRID_COLUMN_TICKET_OR_NOTE))
        _list_of_collection_details.Add(_current_collection_detail)

      End If
    Next

    Return _list_of_collection_details

  End Function

  Private Function TicketAmount(ByVal TicketNumber As String) As Double
    Dim _sb As StringBuilder
    Dim _obj As Object
    Dim _ticket_number As Int64

    If TicketNumber = "" Or Not Int64.TryParse(TicketNumber, _ticket_number) Then
      Return -1
    End If

    _sb = New StringBuilder()

    _sb.AppendLine("DECLARE   @pCountOfTickets AS INT                           ")
    _sb.AppendLine(" SELECT   @pCountOfTickets = COUNT(*)                       ")
    _sb.AppendLine("   FROM   tickets                                           ")
    _sb.AppendLine("  WHERE   ti_validation_number = @pTicketNumber             ")
    _sb.AppendLine("     IF   @pCountOfTickets = 1                              ")
    _sb.AppendLine("          (SELECT   TI_AMOUNT                               ")
    _sb.AppendLine("             FROM   TICKETS                                 ")
    _sb.AppendLine("            WHERE   TI_VALIDATION_NUMBER = @pTicketNumber   ")
    _sb.AppendLine("              AND   TI_MONEY_COLLECTION_ID = @pCollectionId)")
    _sb.AppendLine("  ELSE IF   @pCountOfTickets > 1                              ")
    _sb.AppendLine("          SELECT    2                                          ")
    _sb.AppendLine("   ELSE                                                     ")
    _sb.AppendLine("          SELECT   -2                                       ")

    Try
      Using _db_trx As New DB_TRX()
        Using _sql_cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _sql_cmd.Parameters.Add("@pTicketNumber", SqlDbType.BigInt).Value = TicketNumber
          _sql_cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = m_collection_id '_stacker_id
          _obj = _sql_cmd.ExecuteScalar

          If Not IsNothing(_obj) AndAlso IsNumeric(_obj) Then
            Return CDbl(_obj)
          End If
        End Using
      End Using
    Catch ex As Exception
    End Try

    Return -1

  End Function

  Private Function GetStringToMessageBox(ByVal NewCollection As NEW_COLLECTION)

    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    _sb.AppendLine("")
    '_sb.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2368))
    _sb.AppendLine("")
    _sb.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2369, GUI_FormatCurrency(m_collected_in_notes)))
    If WSI.Common.GeneralParam.GetInt32("TITO", "TicketsCollection") = 0 Then
      _sb.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2370, ef_theoretical_collected_in_tickets.TextValue))
    Else
      _sb.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2370, GUI_FormatCurrency(NewCollection.RealTicketSum)))
    End If

    If Math.Round(NewCollection.TheoreticalBillsSum + NewCollection.TheoreticalTicketSum, 2) = Math.Round(NewCollection.RealBillsSum + NewCollection.RealTicketSum, 2) AndAlso NewCollection.RealBillsCount = NewCollection.TheoreticalBillsCount AndAlso NewCollection.RealTicketCount = NewCollection.TheoreticalTicketCount Then
      _sb.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2371, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2372)))
    Else
      _sb.AppendLine(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2371, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2373)))
    End If

    Return _sb.ToString()
  End Function

  ' PURPOSE: show differences in Table Details, testing configuration
  '
  '  PARAMS:
  '     - INPUT:
  '       - IdxRow: row index to draw
  '       - Value: numeric valur to set
  '       - IdxColor: cell color
  '
  '     - OUTPUT:
  '
  Private Sub SetColumnMismatch(ByVal IdxRow As Int16, ByVal Value As Object, ByVal IdxColor As Int16)

    If Not Me.m_new_collecion_mode Or (m_detail_collection_level = 2) Then
      dg_collection_bills.Cell(IdxRow, GRID_COLUMN_BILL_REAL_VS_THEO_DIFF).Value = Value
      dg_collection_bills.Cell(IdxRow, GRID_COLUMN_BILL_REAL_VS_THEO_DIFF).BackColor = GetColor(IdxColor)
    End If

  End Sub 'SetColumnMismatch

  ' PURPOSE: Generate Excel report with Table Details
  '
  '  PARAMS:
  '     - INPUT:
  '       - Title: report tittle
  '       - Grid: source grid
  '
  '     - OUTPUT:
  '
  'Protected Sub ExcelWithDetails(ByVal Title As String, ByRef Grid As GUI_Controls.uc_grid)
  '  Dim _print_datetime As Date
  '  Dim _excel_data As GUI_Reports.CLASS_EXCEL_DATA = Nothing
  '  Dim _print_data As GUI_Reports.CLASS_PRINT_DATA

  '  Windows.Forms.Cursor.Current = Cursors.WaitCursor

  '  Try
  '    _excel_data = New GUI_Reports.CLASS_EXCEL_DATA()
  '    If _excel_data.IsNull() Then
  '      Return
  '    End If

  '    _print_datetime = GUI_GetDateTime()

  '    If Not _excel_data.Cancelled Then
  '      Call frm_stacker_collections_sel.GUI_ReportParamsX(Grid, _excel_data, Title, _print_datetime)
  '    End If

  '    If Not _excel_data.Cancelled Then
  '      _print_data = LoadReportFilters()
  '      _excel_data.UpdateReportFilters(_print_data.Filter.ItemArray)
  '      _print_data = Nothing
  '    End If

  '    If Not _excel_data.Cancelled Then
  '      Call frm_stacker_collections_sel.GUI_ReportDataX(Grid, _excel_data)
  '    End If

  '    If Not _excel_data.Cancelled Then
  '      Call _excel_data.Preview()
  '    End If

  '  Catch _ex As Exception
  '    Debug.WriteLine(_ex.Message)
  '    Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2213), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
  '  Finally
  '    If _excel_data Is Nothing Then
  '    Else
  '      _excel_data.Dispose()
  '      _excel_data = Nothing
  '    End If
  '    Windows.Forms.Cursor.Current = Cursors.Default
  '  End Try

  '  For Each _tab_page As TabPage In tb_collections.TabPages

  '  Next

  'End Sub ' ExcelWithDetails

  ' PURPOSE: Print report with Table Details
  '
  '  PARAMS:
  '     - INPUT:
  '       - Title: report tittle
  '       - Grid: source grid
  '
  '     - OUTPUT:
  '
  Private Sub ExcelPrintDetails(Optional ByVal ToPrint As Boolean = False, Optional ByVal WithPreview As Boolean = False)

    Dim _print_excel_data As GUI_Reports.CLASS_PRINT_DATA
    Dim _report_layout As ExcelReportLayout
    Dim _sheet_layout As ExcelReportLayout.SheetLayout
    Dim _uc_grid_def As ExcelReportLayout.SheetLayout.UcGridDefinition
    Dim _new_collection As NEW_COLLECTION

    '_new_collection = DbReadObject
    _new_collection = DbEditedObject

    'GUI_FormatDate(PrintDateTime, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    Try
      _print_excel_data = New GUI_Reports.CLASS_PRINT_DATA()
      _print_excel_data = LoadReportFilters()
      _report_layout = New ExcelReportLayout()

      With _report_layout
        .PrintData = _print_excel_data
        .PrintDateTime = frm_base_sel.GUI_GetPrintDateTime()
        _sheet_layout = New ExcelReportLayout.SheetLayout()
        _sheet_layout.Title = Me.FormTitle
        _sheet_layout.Name = Me.Text

        Call AddTotalizator(_new_collection)

        If dg_collection_bills.NumRows > 0 Then
          _uc_grid_def = New ExcelReportLayout.SheetLayout.UcGridDefinition()
          _uc_grid_def.Title = tb_collections.TabPages(0).Text
          _uc_grid_def.UcGrid = tb_collections.TabPages(0).Controls("dg_collection_bills")
          _sheet_layout.ListOfUcGrids.Add(_uc_grid_def)
        End If

        If dg_collection_tickets.NumRows > 0 Then
          _uc_grid_def = New ExcelReportLayout.SheetLayout.UcGridDefinition()
          _uc_grid_def.Title = tb_collections.TabPages(1).Text
          _uc_grid_def.UcGrid = tb_collections.TabPages(1).Controls("dg_collection_tickets")
          _sheet_layout.ListOfUcGrids.Add(_uc_grid_def)
        End If

        .ListOfSheets.Add(_sheet_layout)
      End With

      Call _report_layout.GUI_GenerateExcel()

      Call RemoveTotalizator()
    Catch

      Call RemoveTotalizator()
    End Try

  End Sub ' PrintDetails

  ' PURPOSE: Save the selected filter for print/excel
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  Private Function LoadReportFilters()

    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA

    _print_data = New GUI_Reports.CLASS_PRINT_DATA()

    _print_data.SetFilter(lb_stacker_id.Text, cmb_stacker_id.Text)
    _print_data.SetFilter(ef_insertion_date.Text, ef_insertion_date.Value)
    _print_data.SetFilter(ef_extraction_date.Text, ef_extraction_date.Value)
    _print_data.SetFilter(ef_collection_date.Text, ef_collection_date.Value)
    _print_data.SetFilter(ef_employee.Text, ef_employee.Value)

    _print_data.SetFilter(ef_terminal.Text, ef_terminal.Value)
    _print_data.SetFilter(lbl_notes.Text, tb_notes.Text)
    _print_data.SetFilter(gb_real_values.Text, ef_real_total_recollected.TextValue)

    _print_data.SetFilter(gb_theoretical_values.Text, ef_theoretical_total_collected.TextValue)

    _print_data.FilterHeaderWidth(1) = 2000
    _print_data.FilterHeaderWidth(2) = 2500

    Return _print_data

  End Function ' LoadReportFilters

  Private Sub SetupDataGrid(ByVal MoneyCollection As NEW_COLLECTION, ByVal EditMode As Boolean)
    Dim _idx As Int32

    _idx = 0

    dg_collection_bills.Counter(COUNTER_TOTAL_ITEMS).Value = MoneyCollection.CollectionDetails.Count

    For Each _detail As NEW_COLLECTION.TYPE_MONEY_COLLECTION_DETAILS In MoneyCollection.CollectionDetails
      Call GUI_SetupRow(_idx, _detail, EditMode)

      _idx = _idx + 1
    Next

  End Sub 'SetupDataGrid

  Private Sub UpdateDifferenceAndColor(ByVal RowIndex As Int32, ByVal NewDiff As Int32, ByVal IsBill As Boolean)

    Dim _idxBackgroundColor As Int16
    Dim _idxForeColor As Int16

    _idxBackgroundColor = IIf(NewDiff = 0, ENUM_GUI_COLOR.GUI_COLOR_WHITE_00, ENUM_GUI_COLOR.GUI_COLOR_RED_02)
    _idxForeColor = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00


    If RowIndex >= 0 Then
      If IsBill Then
        dg_collection_bills.Cell(RowIndex, GRID_COLUMN_BILL_REAL_VS_THEO_DIFF).Value = IIf(NewDiff = 0, "", NewDiff)
        dg_collection_bills.Cell(RowIndex, GRID_COLUMN_BILL_REAL_VS_THEO_DIFF).BackColor = GetColor(_idxBackgroundColor)
        dg_collection_bills.Cell(RowIndex, GRID_COLUMN_BILL_REAL_VS_THEO_DIFF).ForeColor = GetColor(_idxForeColor)
      Else
        dg_collection_tickets.Cell(RowIndex, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).Value = IIf(NewDiff = 0, "", NewDiff)
        dg_collection_tickets.Cell(RowIndex, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).BackColor = GetColor(_idxBackgroundColor)
        dg_collection_tickets.Cell(RowIndex, GRID_COLUMN_TICKET_REAL_VS_THEO_DIFF).ForeColor = GetColor(_idxForeColor)
      End If

    End If

  End Sub

  ' PURPOSE: Adds Totalizator for the grids
  '
  '  PARAMS:
  '     - INPUT:
  '       - Collection: DbReadObject
  '
  '     - OUTPUT:
  '
  Private Sub AddTotalizator(ByVal Collection As NEW_COLLECTION)

    If Collection.RealBillsSum <> 0 Or Collection.TheoreticalBillsSum <> 0 Then
      Me.dg_collection_bills.AddRow()
      Me.dg_collection_bills.Cell(Me.dg_collection_bills.NumRows - 1, GRID_COLUMN_BILL_DENOMINATION).Value = GLB_NLS_GUI_INVOICING.GetString(205)
      Me.dg_collection_bills.Cell(Me.dg_collection_bills.NumRows - 1, GRID_COLUMN_BILL_REAL_COUNT).Value = Collection.RealBillsCount
      Me.dg_collection_bills.Cell(Me.dg_collection_bills.NumRows - 1, GRID_COLUMN_BILL_THEO_COUNT).Value = GUI_FormatCurrency(Collection.RealBillsSum)
      Me.dg_collection_bills.Row(Me.dg_collection_bills.NumRows - 1).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    End If

    If Collection.RealTicketSum <> 0 Or Collection.TheoreticalTicketSum Then
      Me.dg_collection_tickets.AddRow()
      Me.dg_collection_tickets.Cell(Me.dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_NUMBER_CORRECT_FORMAT).Value = GLB_NLS_GUI_INVOICING.GetString(205)
      Me.dg_collection_tickets.Cell(Me.dg_collection_tickets.NumRows - 1, GRID_COLUMN_TICKET_AMOUNT).Value = GUI_FormatCurrency(Collection.RealTicketSum)
      Me.dg_collection_tickets.Row(Me.dg_collection_tickets.NumRows - 1).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    End If

  End Sub 'AddTotalizator

  ' PURPOSE: Remove the totalizator rows from the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  Private Sub RemoveTotalizator()
    If Me.dg_collection_bills.NumRows <> 0 Then
      Me.dg_collection_bills.DeleteRow(Me.dg_collection_bills.NumRows - 1)
    End If
    If Me.dg_collection_tickets.NumRows <> 0 Then
      Me.dg_collection_tickets.DeleteRow(Me.dg_collection_tickets.NumRows - 1)
    End If
  End Sub 'RemoveTotalizator

  Private Function IsDenominationInserted(ByVal Denomination As String) As Boolean

    Dim _idx As Int32

    For _idx = 0 To dg_collection_bills.NumRows - 1
      If dg_collection_bills.Cell(_idx, GRID_COLUMN_BILL_DENOMINATION).Value = Denomination Then
        Return True
      End If
    Next

    Return False
  End Function

  Private Sub FillStackersListByTerminal(ByVal TerminalId)

    Dim _sb As StringBuilder
    Dim _table As DataTable
    Dim _idx As Int32

    _sb = New StringBuilder()
    _table = New DataTable()
    _idx = 0

    _sb.AppendLine("    SELECT   MC_STACKER_ID                             ")
    _sb.AppendLine("      FROM   MONEY_COLLECTIONS                         ")
    _sb.AppendLine("INNER JOIN   STACKERS ON MC_STACKER_ID = ST_STACKER_ID ")
    _sb.AppendLine("     WHERE   MC_TERMINAL_ID = @pTerminalId             ")
    _sb.AppendLine("       AND   MC_STATUS = @pStatus                      ")

    Try
      Using _db_trx As New DB_TRX()
        Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId
          _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.PENDING
          Using _sql_da As New SqlDataAdapter(_cmd)
            _db_trx.Fill(_sql_da, _table)

          End Using
        End Using

        cmb_stacker_id.Items.Clear()
        For Each _row As DataRow In _table.Rows
          cmb_stacker_id.Items.Insert(_idx, _row(0))
          _idx = _idx + 1
        Next

        WSI.Common.Misc.GetTerminalName(m_terminal_id, m_terminal_name, _db_trx.SqlTransaction) 'getting the terminal name
      End Using
    Catch _ex As Exception
    End Try
  End Sub

  Private Function TerminalHasPendingCollection(ByVal TerminalId As Int64) As Boolean

    Dim _sb As StringBuilder
    Dim _value As Object

    _sb = New StringBuilder()

    _sb.AppendLine("    SELECT   COUNT(*)                       ")
    _sb.AppendLine("      FROM   MONEY_COLLECTIONS              ")
    _sb.AppendLine("     WHERE   MC_TERMINAL_ID = @pTerminalId  ")
    _sb.AppendLine("       AND   MC_STATUS = @pStatus           ")

    Using _db_trx As New DB_TRX()
      Using _cmd As New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
        _cmd.Parameters.Add("@pTerminalId", SqlDbType.Int).Value = TerminalId
        _cmd.Parameters.Add("@pStatus", SqlDbType.Int).Value = TITO_MONEY_COLLECTION_STATUS.PENDING
        _value = _cmd.ExecuteScalar()

        Return CInt(_value) > 0

      End Using
    End Using

  End Function

  Private Function GetTerminalId(ByVal TerminalName As String) As Integer
    Dim _sb As StringBuilder
    Dim _obj As Object

    _sb = New StringBuilder()

    Try
      Using _db_trx As New WSI.Common.DB_TRX()
        _sb.AppendLine("SELECT   TE_TERMINAL_ID           ")
        _sb.AppendLine("  FROM   TERMINALS                ")
        _sb.AppendLine(" WHERE   TE_TYPE = 1              ")
        _sb.AppendLine("   AND   TE_NAME = @pTerminalName ")

        Using _cmd As New SqlCommand(_sb.ToString())
          _cmd.Parameters.Add("@pTerminalName", SqlDbType.NVarChar).Value = TerminalName
          _obj = _db_trx.ExecuteScalar(_cmd)

          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            Return CInt(_obj)
          End If

        End Using
      End Using

    Catch _ex As Exception
    End Try

    Return 0
  End Function ' GetTerminalId

#End Region

#Region "Events"

  Private Sub bt_start_collection_ClickEvent() Handles btn_start_collection.ClickEvent
    StartNewCollection()

  End Sub

  Private Sub btn_save_tickets_ClickEvent() Handles btn_save_tickets.ClickEvent
    If Not m_collection_started Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2279), _
                                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                      ENUM_MB_BTN.MB_BTN_OK)
      cmb_stacker_id.Focus()
      Return
    End If

    AddTicketToCollection()
  End Sub

  Private Sub btn_save_notes_group_ClickEvent()

    If m_stacker_id = 0 Then
      NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2279), _
                                      mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, _
                                      ENUM_MB_BTN.MB_BTN_OK)
      cmb_stacker_id.Focus()
      Return
    End If

  End Sub

  Private Sub tb_collections_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tb_collections.MouseClick

    If tb_collections.SelectedTab().Name = tab_ticket.Name AndAlso Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT Then
      ef_ticket.Focus()
    End If
  End Sub

  Private Sub dg_collection_bills_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_collection_bills.CellDataChangedEvent

    If Me.dg_collection_bills.Value(Row, Column) = "" Then
      Me.dg_collection_bills.Value(Row, Column) = 0
    Else
      AddNoteToCollection(Row, Column)
    End If

    If Row < dg_collection_bills.NumRows - 1 Then

      Me.dg_collection_bills.CurrentRow = Row + 1
      Me.dg_collection_bills.Row(Row).IsSelected = False
      Me.dg_collection_bills.Row(Row + 1).IsSelected = True
      ' Call dg_collection_bills.SelectFirstRow(False)
      Call Me.dg_collection_bills.RepaintRow(Row + 1)
      Call Me.dg_collection_bills.RepaintRow(Row)

    End If

  End Sub

  Private Sub dg_collection_bills_RowSelectedEvent(ByVal SelectedRow As System.Int32) Handles dg_collection_bills.RowSelectedEvent

    m_old_value = CInt(dg_collection_bills.Value(dg_collection_bills.CurrentRow, dg_collection_bills.CurrentCol))

  End Sub

  Private Sub tb_collections_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tb_collections.Click

    If Me.tb_collections.SelectedIndex = 0 AndAlso dg_collection_bills.NumRows > 0 Then
      Me.dg_collection_bills.Row(dg_collection_bills.CurrentRow).IsSelected = True
      Call Me.dg_collection_bills.RepaintRow(dg_collection_bills.CurrentRow)
      Call Me.dg_collection_bills.Focus()
      Me.ActiveControl = Me.dg_collection_bills
    End If

  End Sub

#End Region

End Class