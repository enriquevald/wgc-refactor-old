'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_promotion_auto_edit
' DESCRIPTION:   New or Edit Automatic Promotion
' AUTHOR:        Javier Molina
' CREATION DATE: 30-MAY-2012
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 30-MAY-2012  JMM    Initial version
' 21-JUN-2012  JCM    Added support schedule common type
' 01-AUG-2012  MPO    Added new logic credit type 
' 22-AUG-2012  DDM    Added check. Can not apply points promotions 
'                     in anonymous accounts
' 12-SEP-2012  HBB    Added a textbox to specify a ticket footer
' 25-SEP-2012  QMP    Added Promotion Category
' 24-APR-2013  SMN    Hide Player Tracking data if exists an External Loyalty
' 11-JUN-2013  RRR    Copy the data from a selected promotion to a new one
' 19-JUN-2013  RRR    Fixed Bug #878: Error adding images to copied promotions
' 23-AUG-2013  JRM    Code revision  
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports GUI_CommonOperations.CLASS_BASE
Imports WSI.Common
Imports System.Text

Public Class frm_TITO_promotion_auto_edit
  Inherits frm_base_edit

#Region " Constants "

  Private Const LEN_PROMO_NAME As Integer = 20
  Private Const PM_LEVEL_FILTER As Integer = 31 ' 0x00011111

#End Region ' Constants

#Region " Members "

  Private m_promotion_copied_id As Long
  Private m_promotion_copy_operation As Boolean
  Private m_delete_operation As Boolean
  Private m_continue_operation As Boolean

#End Region ' Members

#Region " Overrides "

  ' PURPOSE: Initializes the form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = 0

    Call MyBase.GUI_SetFormId()
  End Sub ' GUI_SetFormId

  ' PURPOSE: Form controls initialization.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    'Dim _promo_item As CLASS_TITO_PROMOTION

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    'checks if the app is running in tito mode
    If Not GeneralParam.GetBoolean("TITO", "TITOMode") Then
      Me.gb_tito.Visible = False
    End If

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(820)

    ' Hide the delete button if ScreenMode is New
    If Me.ScreenMode = frm_base_edit.ENUM_SCREEN_MODE.MODE_NEW Then
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Else
      GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Visible = True
    End If

    ' Name
    ef_promo_name.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253)
    ef_promo_name.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, LEN_PROMO_NAME)

    ' Promotion type
    gb_auto_promotion_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(779)
    opt_per_level.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(439)
    opt_min_played.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(441)
    opt_birthday.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(442)

    ' Credit Type
    gb_credit_type.Text = GLB_NLS_GUI_INVOICING.GetString(336)
    opt_credit_non_redeemable.Text = GLB_NLS_GUI_INVOICING.GetString(338)
    opt_credit_redeemable.Text = GLB_NLS_GUI_INVOICING.GetString(337)
    opt_point.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354)

    ' Enabled control
    gb_enabled.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(260)
    opt_enabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(264)
    opt_disabled.Text = GLB_NLS_GUI_CONFIGURATION.GetString(265)

    ' Availability Dates
    Me.dtp_from.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(295)
    Me.dtp_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(296)

    ' Level
    gb_level.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(443)
    lbl_by_level.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(407)
    chk_level_anonymous.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(405)
    chk_level_01.Text = GetCashierPlayerTrackingData("Level01.Name")

    If chk_level_01.Text = "" Then
      chk_level_01.Text = GLB_NLS_GUI_CONFIGURATION.GetString(289)
    End If

    chk_level_02.Text = GetCashierPlayerTrackingData("Level02.Name")

    If chk_level_02.Text = "" Then
      chk_level_02.Text = GLB_NLS_GUI_CONFIGURATION.GetString(290)
    End If

    chk_level_03.Text = GetCashierPlayerTrackingData("Level03.Name")

    If chk_level_03.Text = "" Then
      chk_level_03.Text = GLB_NLS_GUI_CONFIGURATION.GetString(291)
    End If

    chk_level_04.Text = GetCashierPlayerTrackingData("Level04.Name")

    If chk_level_04.Text = "" Then
      chk_level_04.Text = GLB_NLS_GUI_CONFIGURATION.GetString(332)
    End If

    ' Applies
    gb_applies.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(445)

    ef_expiration_days.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(448) ' 448 "Expires after"
    ef_expiration_days.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(444)
    ef_expiration_days.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 3)
    ef_months_day.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 2, 0)
    ef_months_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(446)
    ef_months_day.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(447)

    lbl_birthday.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(217)

    ' Played rewards
    ' Minimum played
    ef_min_played.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(420)
    ef_min_played.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 8)
    ef_min_played_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
    ef_min_played_reward.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 7)
    ' Additional played
    opt_multiplier.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(226)

    ef_played.Text = ""
    ef_played.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 6)
    ef_played_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
    ef_played_reward.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)

    opt_pct.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)

    ef_percentage_reward.Text = ""
    ef_percentage_reward.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5, 2)
    ef_percentage_reward.SufixText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(821)

    ' Notes
    lbl_notes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1107)

    opt_credit_non_redeemable.Checked = True

    ' Promotion Categories
    Me.cmb_categories.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1336)
    Me.cmb_categories.Add(PromotionCategories.Categories())

    Me.Timer.Interval = 1000
    Me.Timer.Start()

    gb_ticket_footer_automatic_promotion.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1284)

    ' promotion TITO
    Me.gb_tito.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2303)
    Me.chk_allow_tickets_tito.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2355)
    Me.ef_quantity_tickets_tito.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2354)
    Me.ef_quantity_tickets_tito.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5)
    Me.ef_generated_tickets_tito.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2356)
    Me.ef_generated_tickets_tito.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5)
    Me.ef_generated_tickets_tito.Enabled = False

  End Sub ' GUI_InitControls

  ' PURPOSE: Validate the data presented on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Function GUI_IsScreenDataOk() As Boolean

    Dim _nls_param1 As String
    Dim _promo_item As CLASS_TITO_PROMOTION
    Dim _months_day As Double

    _promo_item = DbReadObject

    ' Name
    If ef_promo_name.Value = "" Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(261)
      Call NLS_MsgBox(GLB_NLS_GUI_CONFIGURATION.Id(101), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_promo_name.Focus()

      Return False
    End If

    ' Point and level anonymous
    'If opt_point.Checked AndAlso chk_level_anonymous.Checked Then
    '  Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1264), ENUM_MB_TYPE.MB_TYPE_WARNING)
    '  Call chk_level_anonymous.Focus()

    '  Return False

    'End If

    ' Expiration dates range
    If dtp_from.Value > dtp_to.Value Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call dtp_to.Focus()

      Return False
    End If

    If ((chk_level_anonymous.Checked = False) Or (opt_point.Checked = True) Or (opt_birthday.Checked = True)) _
         And chk_level_01.Checked = False _
         And chk_level_02.Checked = False _
         And chk_level_03.Checked = False _
         And chk_level_04.Checked = False Then

      ' 158 "A level must be selected."
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(158), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call chk_level_anonymous.Focus()

      Return False
    End If

    ' Applies on
    If opt_per_level.Checked Or _
       opt_min_played.Checked Then

      _months_day = GUI_ParseNumber(ef_months_day.Value)

      If _months_day = 0 Or _
         _months_day > 31 Then

        '156 "You should enter a month's day when the promotion will be applied"
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(156), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call ef_months_day.Focus()

        Return False

      End If
    End If

    'If ef_min_played_reward.Value <> "" AndAlso GUI_ParseCurrency(ef_min_played_reward.Value) = 0 Then
    '  Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_min_played_reward.Text)
    '  Call ef_min_played_reward.Focus()

    '  Return False
    'End If


    Select Case True

      Case opt_per_level.Checked

        If GUI_ParseCurrency(ef_min_played_reward.Value) <= 0 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(119), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
          Call ef_min_played_reward.Focus()

          Return False
        End If

      Case opt_min_played.Checked

        If opt_multiplier.Checked Then

          If ((GUI_ParseCurrency(ef_min_played_reward.Value) + GUI_ParseCurrency(ef_played_reward.Value)) = 0) Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(119), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
            Call ef_played_reward.Focus()

            Return False
          End If

          If (ef_played.Value <> "" And ef_played_reward.Value = "") Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(119), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
            Call ef_played_reward.Focus()

            Return False
          End If

          If (ef_played.Value = "" And ef_played_reward.Value <> "") Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(119), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
            Call ef_played.Focus()

            Return False
          End If

          If ef_played.Value <> "" AndAlso GUI_ParseCurrency(ef_played.Value) = 0 Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , opt_multiplier.Text)
            Call ef_played.Focus()

            Return False
          End If

          If ef_played_reward.Value <> "" AndAlso GUI_ParseCurrency(ef_played_reward.Value) = 0 Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_played_reward.Text)
            Call ef_played_reward.Focus()

            Return False
          End If


        ElseIf opt_pct.Checked Then

          If ((GUI_ParseCurrency(ef_min_played_reward.Value) + GUI_ParseCurrency(ef_percentage_reward.Value)) = 0) Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
            Call ef_percentage_reward.Focus()

            Return False
          End If

          If ef_percentage_reward.Value <> "" AndAlso GUI_ParseNumber(ef_percentage_reward.Value) = 0 Then
            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , opt_pct.Text & " " & ef_percentage_reward.SufixText)
            Call ef_percentage_reward.Focus()

            Return False
          End If

        End If

      Case opt_birthday.Checked

        If GUI_ParseCurrency(ef_min_played_reward.Value) <= 0 Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
          Call ef_min_played_reward.Focus()

          Return False
        End If

    End Select

    ' Providers (if listed or not listed must have at least one selected)
    If Not uc_provider_filter.CheckAtLeastOneSelected() Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(566)
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(118), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call uc_provider_filter.Focus()

      Return False
    End If

    ' Expiration 
    If GUI_ParseNumber(ef_expiration_days.Value) <= 0 Then
      _nls_param1 = GLB_NLS_GUI_PLAYER_TRACKING.GetString(223)
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(121), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , _nls_param1)
      Call ef_expiration_days.Focus()

      Return False
    End If

    If opt_min_played.Checked Then
      If GUI_ParseCurrency(ef_min_played.Value) = 0 Then
        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1154), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, ENUM_MB_BTN.MB_BTN_YES_NO) = ENUM_MB_RESULT.MB_RESULT_NO Then
          ef_min_played.Focus()

          Return False
        End If
      End If
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  ' PURPOSE: Get data from the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_GetScreenData()

    Dim _promo_item As CLASS_TITO_PROMOTION
    Dim _aux_pct As Double

    _promo_item = DbEditedObject
    _promo_item.Name = ef_promo_name.Value
    _promo_item.Enabled = opt_enabled.Checked
    _promo_item.WonLockEnabled = False
    _promo_item.WonLock = 0

    If opt_per_level.Checked Then
      _promo_item.Type = Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL
    ElseIf opt_min_played.Checked Then
      _promo_item.Type = Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED
    ElseIf opt_birthday.Checked Then
      _promo_item.Type = Promotion.PROMOTION_TYPE.PERIODIC_PER_BIRTHDAY
    Else
      ' None type radiobutton checked so promotion set as default type
      _promo_item.Type = Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL

      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                      GLB_ApplicationName, _
                      "frm_promotion_auto_edit.GUI_GetScreenData", _
                      "", _
                      "", _
                      "", _
                      "No type radio button selected.")
    End If

    _promo_item.SpecialPermission = CLASS_TITO_PROMOTION.ENUM_SPECIAL_PERMISSION.NOT_SET
    _promo_item.DateStart = dtp_from.Value
    _promo_item.DateFinish = dtp_to.Value
    _promo_item.ScheduleWeekday = ScheduleDay.GetScheduleDayValue(GUI_ParseNumber(Me.ef_months_day.Value), _promo_item.Type)
    _promo_item.Schedule1TimeFrom = 0
    _promo_item.Schedule1TimeTo = 0
    _promo_item.Schedule2Enabled = 0
    _promo_item.Schedule2TimeFrom = 0
    _promo_item.Schedule2TimeTo = 0
    _promo_item.GenderFilter = CLASS_TITO_PROMOTION.ENUM_GENDER_FILTER.NOT_SET
    _promo_item.BirthdayFilter = CLASS_TITO_PROMOTION.ENUM_BIRTHDAY_FILTER.NOT_SET

    If opt_birthday.Checked Then
      _promo_item.BirthdayFilter = CLASS_TITO_PROMOTION.ENUM_BIRTHDAY_FILTER.DAY_ONLY
    End If

    _promo_item.LevelFilter = GetLevelFilter()
    _promo_item.FreqFilterLastDays = 0
    _promo_item.FreqFilterMinDays = 0
    _promo_item.FreqFilterMinCashIn = 0
    _promo_item.ExpirationType = CLASS_TITO_PROMOTION.ENUM_EXPIRATION_TYPE.DAYS
    _promo_item.ExpirationValue = GUI_ParseNumber(ef_expiration_days.Value)
    _promo_item.MinCashIn = 0
    _promo_item.MinCashInReward = 0
    _promo_item.CashIn = 0
    _promo_item.CashInReward = 0
    _promo_item.MinSpent = 0
    _promo_item.MinSpentReward = 0
    _promo_item.Spent = 0
    _promo_item.SpentReward = 0
    _promo_item.MinPlayedReward = GUI_ParseCurrency(Me.ef_min_played_reward.Value)

    If _promo_item.Type = Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED Then

      _promo_item.MinPlayed = GUI_ParseCurrency(Me.ef_min_played.Value)

      If opt_multiplier.Checked Then

        _promo_item.Played = GUI_ParseCurrency(Me.ef_played.Value)
        _promo_item.PlayedReward = GUI_ParseCurrency(Me.ef_played_reward.Value)

      ElseIf opt_pct.Checked Then

        _aux_pct = GUI_ParseNumber(Me.ef_percentage_reward.Value)

        If _aux_pct > 0 Then
          _promo_item.Played = -100
          _promo_item.PlayedReward = _aux_pct * -1
        Else
          _promo_item.Played = 0
          _promo_item.PlayedReward = 0
        End If

      Else

        _promo_item.Played = 0
        _promo_item.PlayedReward = 0

      End If

    Else

      _promo_item.MinPlayed = 0
      _promo_item.Played = 0
      _promo_item.PlayedReward = 0

    End If

    ' Get the checked providers from the control and copy them in the promo_item.ProviderList.
    uc_provider_filter.GetProvidersFromControl(_promo_item.ProviderList)

    _promo_item.NumTokens = 0
    _promo_item.TokenReward = 0
    _promo_item.TokenName = ""
    _promo_item.DailyLimit = 0
    _promo_item.MonthlyLimit = 0
    _promo_item.GlobalDailyLimit = 0
    _promo_item.GlobalMonthlyLimit = 0

    If opt_credit_non_redeemable.Checked Then
      _promo_item.CreditType = WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1
    ElseIf opt_credit_redeemable.Checked Then
      _promo_item.CreditType = WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
    ElseIf opt_point.Checked Then
      _promo_item.CreditType = WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.POINT
    Else
      _promo_item.CreditType = WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1
    End If

    'getting the ticket footer text
    _promo_item.TicketFooter = tb_ticket_footer_automatic_promotion.Text
    _promo_item.CategoryId = cmb_categories.Value

    'TITO
    _promo_item.AllowTickets = Me.chk_allow_tickets_tito.Checked
    _promo_item.TicketQuantity = Me.ef_quantity_tickets_tito.Value
    _promo_item.GeneratedTickets = Me.ef_generated_tickets_tito.Value

  End Sub ' GUI_GetScreenData

  ' PURPOSE: Set initial data on the screen.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _promo_item As CLASS_TITO_PROMOTION
    Dim _months_day As Integer

    _promo_item = DbReadObject

    ef_promo_name.Value = _promo_item.Name

    dtp_from.Value = _promo_item.DateStart
    dtp_to.Value = _promo_item.DateFinish

    If _promo_item.Enabled Then
      opt_enabled.Checked = True
      opt_disabled.Checked = False
    Else
      opt_enabled.Checked = False
      opt_disabled.Checked = True
    End If

    Select Case _promo_item.CreditType
      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR2, WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1
        opt_credit_non_redeemable.Checked = True
      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.POINT
        opt_point.Checked = True
      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE
        opt_credit_redeemable.Checked = True
      Case Else
        opt_credit_non_redeemable.Checked = True
    End Select

    _months_day = ScheduleDay.GetMonthDay(_promo_item.ScheduleWeekday)

    If _months_day > 0 And _months_day <> ScheduleDay.SCHEDULE_DAY_MASK.TYPE_NONE Then
      ef_months_day.Value = _months_day
    End If

    ' Birthday Filter
    Select Case _promo_item.BirthdayFilter
      Case CLASS_TITO_PROMOTION.ENUM_BIRTHDAY_FILTER.NOT_SET
        opt_birthday.Checked = False
        lbl_birthday.Visible = False

      Case CLASS_TITO_PROMOTION.ENUM_BIRTHDAY_FILTER.DAY_ONLY
        opt_birthday.Checked = True
        lbl_birthday.Visible = True

      Case CLASS_TITO_PROMOTION.ENUM_BIRTHDAY_FILTER.WHOLE_MONTH
        ' ENUM_BIRTHDAY_FILTER.WHOLE_MONTH not supported on periodical promotions
        ' Managed as ENUM_BIRTHDAY_FILTER.NOT_SET
        opt_birthday.Checked = False
        lbl_birthday.Visible = False

        Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                GLB_ApplicationName, _
                "frm_promotion_auto_edit.GUI_SetScreenData", _
                "", _
                "", _
                "", _
                "ENUM_BIRTHDAY_FILTER.WHOLE_MONTH found on a periodical promotion.")

    End Select

    ' Level Filter
    SetLevelFilter(_promo_item.LevelFilter)

    Select Case _promo_item.Type
      Case Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL
        opt_per_level.Checked = True
      Case Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED
        opt_min_played.Checked = True
      Case Promotion.PROMOTION_TYPE.PERIODIC_PER_BIRTHDAY
        opt_birthday.Checked = True
      Case Else
        ' Read a non periodical promotion type
        ' Managed as PROMOTION_TYPE.PERIODIC_PER_LEVEL

        opt_per_level.Checked = True

        Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                GLB_ApplicationName, _
                "frm_promotion_auto_edit.GUI_SetScreenData", _
                "", _
                "", _
                "", _
                "Read a non periodical promotion type.")
    End Select

    ' Expiration type and value
    Select Case _promo_item.ExpirationType

      Case CLASS_TITO_PROMOTION.ENUM_EXPIRATION_TYPE.DAYS
        If _promo_item.ExpirationValue > 0 Then
          ef_expiration_days.Value = _promo_item.ExpirationValue
        End If

      Case Else ' All periodical promotions managed as ENUM_EXPIRATION_TYPE.DAYS
        If _promo_item.ExpirationValue > 0 Then
          ef_expiration_days.Value = _promo_item.ExpirationValue
        End If

    End Select

    If (Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT) Or (Me.m_promotion_copy_operation) Then

      If _promo_item.MinPlayed > 0 Then
        ef_min_played.Value = _promo_item.MinPlayed
      End If
      If _promo_item.MinPlayedReward > 0 Then
        ef_min_played_reward.Value = _promo_item.MinPlayedReward
      End If

      If _promo_item.Played > 0 Then
        opt_multiplier.Checked = True
        ef_played.Value = _promo_item.Played
      ElseIf _promo_item.Played = -100 Then
        opt_pct.Checked = True
        ef_played.Value = ""
        ef_played_reward.Value = ""
        ef_percentage_reward.Value = _promo_item.PlayedReward * -1
      End If

      If _promo_item.PlayedReward > 0 Then
        ef_played_reward.Value = _promo_item.PlayedReward
      End If
    End If

    ' Set the provider filter control with the data from the promo_item.ProviderList.
    uc_provider_filter.SetProviderList(_promo_item.ProviderList)

    'HBB: Initialize ticket_footer field
    tb_ticket_footer_automatic_promotion.Text = _promo_item.TicketFooter

    cmb_categories.Value = _promo_item.CategoryId

    ' Initialize text labels that use values from entry fields
    Call RefreshLabelTexts()

    'TITO
    Me.chk_allow_tickets_tito.Checked = _promo_item.AllowTickets
    Me.ef_quantity_tickets_tito.Value = _promo_item.TicketQuantity
    Me.ef_generated_tickets_tito.Value = _promo_item.GeneratedTickets ' NumberOfGeneratedTickets(_promo_item.PromotionId)

  End Sub ' GUI_SetScreenData

  ' PURPOSE: Database overridable operations. 
  '          Define specific DB operation for this form.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)

    Dim _promo_class As CLASS_TITO_PROMOTION
    Dim _nls_param1 As String
    Dim _rc As mdl_NLS.ENUM_MB_RESULT
    Dim _aux_date As DateTime

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_TITO_PROMOTION
        _promo_class = DbEditedObject
        _promo_class.PromotionId = 0
        _promo_class.Enabled = True
        _promo_class.SpecialPermission = CLASS_TITO_PROMOTION.ENUM_SPECIAL_PERMISSION.NOT_SET
        _promo_class.Type = Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL
        _promo_class.CreditType = ACCOUNT_PROMO_CREDIT_TYPE.NR1
        _aux_date = Now.AddMonths(1)
        _promo_class.DateStart = New DateTime(_aux_date.Year, _aux_date.Month, 1, GetDefaultClosingTime(), 0, 0)
        _promo_class.DateFinish = _promo_class.DateStart.AddMonths(1)
        _promo_class.ScheduleWeekday = ScheduleDay.GetScheduleDayValue(0, _promo_class.Type)
        _promo_class.Schedule1TimeFrom = 0     ' 00:00:00
        _promo_class.Schedule1TimeTo = 0       ' 00:00:00
        _promo_class.Schedule2Enabled = False
        _promo_class.Schedule2TimeFrom = 0     ' 00:00:00
        _promo_class.Schedule2TimeTo = 0       ' 00:00:00
        _promo_class.GenderFilter = CLASS_TITO_PROMOTION.ENUM_GENDER_FILTER.NOT_SET
        _promo_class.BirthdayFilter = CLASS_TITO_PROMOTION.ENUM_BIRTHDAY_FILTER.NOT_SET
        _promo_class.LevelFilter = 0
        _promo_class.ExpirationType = CLASS_TITO_PROMOTION.ENUM_EXPIRATION_TYPE.DAYS

        Select Case Me.ScreenMode
          Case ENUM_SCREEN_MODE.MODE_NEW
            _promo_class.PromoScreenMode = CLASS_TITO_PROMOTION.ENUM_PROMO_SCREEN_MODE.MODE_NEW
          Case ENUM_SCREEN_MODE.MODE_EDIT
            _promo_class.PromoScreenMode = CLASS_TITO_PROMOTION.ENUM_PROMO_SCREEN_MODE.MODE_EDIT
        End Select

        DbStatus = ENUM_STATUS.STATUS_OK

        If Me.m_promotion_copied_id > 0 And Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
          If _promo_class.DB_Read(Me.m_promotion_copied_id, 0) = ENUM_STATUS.STATUS_OK Then
            Me.m_promotion_copy_operation = True
            _promo_class.PromotionId = 0
            _promo_class.Name = String.Empty
            _promo_class.ResetImages()
          End If
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          _promo_class = Me.DbEditedObject
          _nls_param1 = "" 'm_input_promo_name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(104), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT

        m_continue_operation = True
        _promo_class = Me.DbEditedObject

        If (_promo_class.Type = Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL Or _
             _promo_class.Type = Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED) And _
           Not DbReadObject.AuditorData.IsEqual(DbEditedObject.AuditorData) Then

          If ScheduleDay.GetMonthDay(_promo_class.ScheduleWeekday) > 28 Then

            '157 "You have entered day %1 as the month day on the promotion will be applied.  \nSome months the promotions won't be applied.  \n\n�Do you want to continue?"
            If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(157), _
                          ENUM_MB_TYPE.MB_TYPE_WARNING, _
                          ENUM_MB_BTN.MB_BTN_YES_NO, _
                          , _
                          ef_months_day.Value) = ENUM_MB_RESULT.MB_RESULT_NO Then

              m_continue_operation = False
            End If
          Else
            Call MyBase.GUI_DB_Operation(DbOperation)
          End If
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_INSERT
        If m_continue_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK And m_continue_operation Then
          _promo_class = Me.DbEditedObject
          _nls_param1 = _promo_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(105), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE

        m_continue_operation = True
        _promo_class = Me.DbEditedObject

        If (_promo_class.Type = Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL Or _
             _promo_class.Type = Promotion.PROMOTION_TYPE.PERIODIC_PER_LEVEL_AND_MIN_PLAYED) And _
           Not DbReadObject.AuditorData.IsEqual(DbEditedObject.AuditorData) Then

          If ScheduleDay.GetMonthDay(_promo_class.ScheduleWeekday) > 28 Then

            '157 "You have entered day %1 as the month day on the promotion will be applied.  \nSome months the promotions won't be applied.  \n\n�Do you want to continue?"
            If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(157), _
                          ENUM_MB_TYPE.MB_TYPE_WARNING, _
                          ENUM_MB_BTN.MB_BTN_YES_NO, _
                          , _
                          ef_months_day.Value) = ENUM_MB_RESULT.MB_RESULT_NO Then

              m_continue_operation = False
            End If
          Else
            Call MyBase.GUI_DB_Operation(DbOperation)
          End If
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        If m_continue_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK And m_continue_operation Then
          _promo_class = Me.DbEditedObject
          _nls_param1 = _promo_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(106), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus = ENUM_STATUS.STATUS_DEPENDENCIES And m_delete_operation Then
          _promo_class = Me.DbEditedObject
          _nls_param1 = _promo_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(103), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1)

        ElseIf Me.DbStatus <> ENUM_STATUS.STATUS_OK And m_delete_operation Then
          _promo_class = Me.DbEditedObject
          _nls_param1 = _promo_class.Name
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(107), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          _nls_param1)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_BEFORE_DELETE
        m_delete_operation = False
        _promo_class = Me.DbEditedObject
        _nls_param1 = _promo_class.Name
        _rc = NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(108), _
                        ENUM_MB_TYPE.MB_TYPE_WARNING, _
                        ENUM_MB_BTN.MB_BTN_YES_NO, _
                        ENUM_MB_DEF_BTN.MB_DEF_BTN_2, _
                        _nls_param1)
        If _rc = ENUM_MB_RESULT.MB_RESULT_YES Then
          m_delete_operation = True
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_DELETE
        If m_delete_operation Then
          Call MyBase.GUI_DB_Operation(DbOperation)
        Else
          DbStatus = ENUM_STATUS.STATUS_ERROR
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  ' PURPOSE: Manage permissions.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_Permissions(ByRef AndPerm As CLASS_GUI_USER.TYPE_PERMISSIONS)

  End Sub ' GUI_Permissions

  ' PURPOSE: Define the control which have the focus when the form is initially shown.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetInitialFocus()

    Me.ActiveControl = Me.ef_promo_name

  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Init form in new mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowNewItem(Optional ByVal PromotionCopiedId As Long = 0)

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    Me.m_promotion_copy_operation = False
    Me.m_promotion_copied_id = PromotionCopiedId

    DbObjectId = Nothing
    DbStatus = ENUM_STATUS.STATUS_OK

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call Me.Display(True)
    End If

  End Sub ' ShowNewItem

  ' PURPOSE: Init form in edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '       - UserId
  '       - Username
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overloads Sub ShowEditItem(ByVal PromoId As Integer, ByVal PromoName As String)
    'Dim _promo_item As CLASS_TITO_PROMOTION

    ' Sets the screen mode
    Me.ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    Me.m_promotion_copy_operation = False
    Me.m_promotion_copied_id = 0
    Me.DbObjectId = PromoId
    '    Me.m_input_promo_name = PromoName

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If


    Call Me.Display(True)

  End Sub ' ShowEditItem

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter)
        If Me.tb_ticket_footer_automatic_promotion.ContainsFocus Then

          Return True
        End If

      Case Else
        Return MyBase.GUI_KeyPress(sender, e)
    End Select

    ' The key Enter event is done in tb_ticket_footer_automatic_promotion

  End Function ' GUI_KeyPress

#End Region ' Overrides

#Region " Private Functions "

  Private Sub SetLevelFilter(ByVal LevelFilter As Integer)
    Dim _str_binary As String
    Dim _bit_array As Char()

    _str_binary = Convert.ToString(LevelFilter, 2)
    _str_binary = New String("0", 5 - _str_binary.Length) + _str_binary
    _bit_array = _str_binary.ToCharArray()

    If (_bit_array(4) = "0") Then chk_level_anonymous.Checked = False Else chk_level_anonymous.Checked = True ' Anonymous Player
    If (_bit_array(3) = "0") Then chk_level_01.Checked = False Else chk_level_01.Checked = True ' Level 01
    If (_bit_array(2) = "0") Then chk_level_02.Checked = False Else chk_level_02.Checked = True ' Level 02
    If (_bit_array(1) = "0") Then chk_level_03.Checked = False Else chk_level_03.Checked = True ' Level 03
    If (_bit_array(0) = "0") Then chk_level_04.Checked = False Else chk_level_04.Checked = True ' Level 04

  End Sub ' SetLevelFilter

  Private Function GetLevelFilter() As Integer
    Dim _bit_array As Char()

    _bit_array = New String("00000").ToCharArray()

    If (Not opt_birthday.Checked) And (Not opt_point.Checked) Then
      _bit_array(4) = Convert.ToInt32(chk_level_anonymous.Checked).ToString()  ' Anonymous Player
    End If

    _bit_array(3) = Convert.ToInt32(chk_level_01.Checked).ToString()  ' Level 01
    _bit_array(2) = Convert.ToInt32(chk_level_02.Checked).ToString()  ' Level 02
    _bit_array(1) = Convert.ToInt32(chk_level_03.Checked).ToString()  ' Level 03
    _bit_array(0) = Convert.ToInt32(chk_level_04.Checked).ToString()  ' Level 04

    Return Convert.ToInt32(_bit_array, 2)

  End Function ' GetLevelFilter

  ' PURPOSE: Refreshes label texts according to user inputs
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Private Sub RefreshLabelTexts()
    Dim _empty_placeholder As String
    Dim _str_promo_credit_type As String
    Dim _min_played_reward As String
    Dim _played_reward As String

    _empty_placeholder = "[ ]"

    If Me.opt_credit_redeemable.Checked Then
      _str_promo_credit_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(518)
    ElseIf Me.opt_credit_non_redeemable.Checked Then
      _str_promo_credit_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(517)
    ElseIf Me.opt_point.Checked Then
      _str_promo_credit_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354).ToLower
    Else
      _str_promo_credit_type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(517)
    End If

    '
    ' Initialize Rewards vars
    '
    If Me.opt_point.Checked Then
      _min_played_reward = GUI_FormatNumber(GUI_ParseCurrency(ef_min_played_reward.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
      _played_reward = GUI_FormatNumber(GUI_ParseCurrency(ef_played_reward.Value), ENUM_GROUP_DIGITS.GROUP_DIGITS_FALSE)
    Else
      _min_played_reward = GUI_FormatCurrency(GUI_ParseCurrency(ef_min_played_reward.Value))
      _played_reward = GUI_FormatCurrency(GUI_ParseCurrency(ef_played_reward.Value))
    End If

    '
    ' Rewards
    '
    If Me.opt_min_played.Checked Then

      If GUI_ParseCurrency(ef_min_played_reward.Value) > 0 Or GUI_ParseCurrency(ef_min_played.Value) > 0 Then
        lbl_min_played_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(449, _min_played_reward, _
                                                                              GUI_FormatCurrency(GUI_ParseCurrency(ef_min_played.Value)), _
                                                                              _str_promo_credit_type)
      Else
        lbl_min_played_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(449, _empty_placeholder, _empty_placeholder, _str_promo_credit_type)
      End If

      If Me.opt_multiplier.Checked Then

        If GUI_ParseCurrency(ef_played_reward.Value) > 0 Or GUI_ParseCurrency(ef_played.Value) > 0 Then
          lbl_played_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(450, _played_reward, _
                                                                            GUI_FormatCurrency(GUI_ParseCurrency(ef_played.Value)), _
                                                                            _str_promo_credit_type)
        Else
          lbl_played_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(450, _empty_placeholder, _empty_placeholder, _str_promo_credit_type)
        End If

      ElseIf Me.opt_pct.Checked Then

        If GUI_ParseNumber(ef_percentage_reward.Value) > 0 Then
          lbl_played_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(823, GUI_FormatNumber(GUI_ParseCurrency(ef_percentage_reward.Value)), _
                                                                              _str_promo_credit_type)
        Else
          lbl_played_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(823, _empty_placeholder, _str_promo_credit_type)

        End If
      Else

        lbl_played_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(450, _empty_placeholder, _empty_placeholder, _str_promo_credit_type)

      End If

    Else
      If GUI_ParseCurrency(ef_min_played_reward.Value) > 0 Then
        lbl_min_played_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(822, _min_played_reward, _
                                                                              _str_promo_credit_type)
      Else
        lbl_min_played_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(822, _empty_placeholder, _str_promo_credit_type)
      End If

      lbl_played_promotion.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(450, _empty_placeholder, _empty_placeholder, _str_promo_credit_type)

    End If

  End Sub ' RefreshLabelTexts

  Private Sub SwitchCredit(ByVal CreditType As WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE)

    Select Case CreditType
      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.NR1, ACCOUNT_PROMO_CREDIT_TYPE.NR2

        ef_min_played_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_played_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        opt_pct.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        Me.chk_level_anonymous.Enabled = Not opt_birthday.Checked

      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.POINT

        ef_min_played_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354)
        ef_played_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354)
        opt_pct.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(354)
        Me.chk_level_anonymous.Enabled = False

      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE

        ef_min_played_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
        ef_played_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
        opt_pct.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(250)
        Me.chk_level_anonymous.Enabled = Not opt_birthday.Checked

      Case Else

        ef_min_played_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        ef_played_reward.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)
        opt_pct.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(224)

    End Select

    Dim ls_ef As List(Of GUI_Controls.uc_entry_field)
    Dim _value As Object

    _value = Nothing

    ls_ef = New List(Of GUI_Controls.uc_entry_field)
    ls_ef.Add(ef_min_played_reward)
    ls_ef.Add(ef_played_reward)

    Select Case CreditType

      Case WSI.Common.ACCOUNT_PROMO_CREDIT_TYPE.POINT

        For Each _uc As uc_entry_field In ls_ef
          _value = _uc.Value
          _uc.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_NUMBER, 5)
          _uc.Value = _value
        Next

      Case Else

        For Each _uc As uc_entry_field In ls_ef
          _value = _uc.Value
          _uc.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_MONEY, 5)
          _uc.Value = _value
        Next

    End Select

  End Sub

  ' PURPOSE: Returns the number of tickets generated for this promotion
  '
  '    - INPUT:
  '     
  '    - OUTPUT:
  '
  ' RETURNS:
  '       - The number of tickets generated for this promotion
  '
  Private Function NumberOfGeneratedTickets(ByVal pPromotionID As Integer) As Integer

    Dim _sb As StringBuilder
    Dim _obj As Object

    NumberOfGeneratedTickets = 0

    _sb = New StringBuilder()
    Try
      Using _trx As New DB_TRX()
        _sb.AppendLine("SELECT   COUNT(*)                                     ")
        _sb.AppendLine("  FROM   TICKETS                                      ")
        _sb.AppendLine(" WHERE   TI_PROMOTION_ID = @pIDPromo       ")

        Using _cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(_sb.ToString(), _trx.SqlTransaction.Connection, _trx.SqlTransaction)
          _cmd.Parameters.Add("@pIDPromo", SqlDbType.NVarChar).Value = pPromotionID
          _obj = _cmd.ExecuteScalar()
          If _obj IsNot Nothing AndAlso _obj IsNot DBNull.Value Then
            Integer.TryParse(CInt(_obj), NumberOfGeneratedTickets)
          End If

          Return NumberOfGeneratedTickets
        End Using
      End Using
    Catch _ex As Exception
      Return 0
    End Try

  End Function   'NumberOfGeneratedTickets

#End Region ' Private Functions

#Region " Events "

  ' PURPOSE: Asure that the provider control is closed when the focus go to another control
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub uc_provider_filter_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uc_provider_filter.Leave
    uc_provider_filter.Collapsed = True
  End Sub

  ' PURPOSE: Timer operation to refresh the test area.
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Private Sub Timer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer.Tick
    Call RefreshLabelTexts()
  End Sub

  Private Sub opt_credit_non_redeemable_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_credit_non_redeemable.CheckedChanged

    Me.SwitchCredit(ACCOUNT_PROMO_CREDIT_TYPE.NR1)

  End Sub

  Private Sub opt_credit_redeemable_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_credit_redeemable.CheckedChanged

    Me.SwitchCredit(ACCOUNT_PROMO_CREDIT_TYPE.REDEEMABLE)

  End Sub

  Private Sub opt_point_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_point.CheckedChanged

    Me.SwitchCredit(ACCOUNT_PROMO_CREDIT_TYPE.POINT)

  End Sub

  Private Sub opt_promotion_type_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_per_level.CheckedChanged, opt_min_played.CheckedChanged, opt_birthday.CheckedChanged

    Dim _opt As RadioButton

    _opt = sender

    Select Case _opt.Handle
      Case Me.opt_per_level.Handle
        Me.ef_months_day.Visible = True
        Me.lbl_birthday.Visible = False

        Me.opt_multiplier.Enabled = False
        Me.opt_multiplier.Checked = False
        Me.opt_pct.Enabled = False
        Me.opt_pct.Checked = False

        Me.ef_min_played.Enabled = False
        Me.ef_min_played_reward.Enabled = True
        Me.ef_played.Enabled = False
        Me.ef_played_reward.Enabled = False
        Me.ef_percentage_reward.Enabled = False

        Me.uc_provider_filter.Enabled = False

        Me.chk_level_anonymous.Enabled = Not opt_point.Checked

      Case Me.opt_min_played.Handle
        Me.ef_months_day.Visible = True
        Me.lbl_birthday.Visible = False

        Me.opt_multiplier.Enabled = True
        Me.opt_multiplier.Checked = True
        Me.opt_pct.Enabled = True
        Me.opt_pct.Checked = False

        Me.ef_min_played.Enabled = True
        Me.ef_min_played_reward.Enabled = True
        Me.ef_played.Enabled = True
        Me.ef_played_reward.Enabled = True
        Me.ef_percentage_reward.Enabled = False

        Me.uc_provider_filter.Enabled = True

        Me.chk_level_anonymous.Enabled = Not opt_point.Checked

      Case Me.opt_birthday.Handle
        Me.ef_months_day.Visible = False
        Me.lbl_birthday.Visible = True

        Me.opt_multiplier.Enabled = False
        Me.opt_multiplier.Checked = False
        Me.opt_pct.Enabled = False
        Me.opt_pct.Checked = False

        Me.ef_min_played.Enabled = False
        Me.ef_min_played_reward.Enabled = True
        Me.ef_played.Enabled = False
        Me.ef_played_reward.Enabled = False
        Me.ef_percentage_reward.Enabled = False

        Me.uc_provider_filter.Enabled = False

        Me.chk_level_anonymous.Enabled = False

    End Select
  End Sub

  Private Sub opt_multiplier_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_multiplier.CheckedChanged
    Me.ef_played.Enabled = True
    Me.ef_played_reward.Enabled = True
    Me.ef_percentage_reward.Enabled = False
  End Sub

  Private Sub opt_pct_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_pct.CheckedChanged
    Me.ef_played.Enabled = False
    Me.ef_played_reward.Enabled = False
    Me.ef_percentage_reward.Enabled = True
  End Sub

#End Region ' Events


End Class