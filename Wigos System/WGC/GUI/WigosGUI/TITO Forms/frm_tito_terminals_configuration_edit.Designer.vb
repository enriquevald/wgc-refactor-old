<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_tito_terminals_configuration_edit
  Inherits GUI_Controls.frm_base_edit

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.uc_terminal_name = New GUI_Controls.uc_entry_field
    Me.chk_allow_cashable = New System.Windows.Forms.CheckBox
    Me.chk_allow_promotional = New System.Windows.Forms.CheckBox
    Me.chk_allow_creation = New System.Windows.Forms.CheckBox
    Me.chk_allow_bills = New System.Windows.Forms.CheckBox
    Me.panel_data.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_data
    '
    Me.panel_data.Controls.Add(Me.chk_allow_bills)
    Me.panel_data.Controls.Add(Me.chk_allow_creation)
    Me.panel_data.Controls.Add(Me.chk_allow_promotional)
    Me.panel_data.Controls.Add(Me.chk_allow_cashable)
    Me.panel_data.Controls.Add(Me.uc_terminal_name)
    Me.panel_data.Size = New System.Drawing.Size(263, 129)
    '
    'uc_terminal_name
    '
    Me.uc_terminal_name.DoubleValue = 0
    Me.uc_terminal_name.IntegerValue = 0
    Me.uc_terminal_name.IsReadOnly = True
    Me.uc_terminal_name.Location = New System.Drawing.Point(3, 4)
    Me.uc_terminal_name.Name = "uc_terminal_name"
    Me.uc_terminal_name.Size = New System.Drawing.Size(248, 24)
    Me.uc_terminal_name.SufixText = "Sufix Text"
    Me.uc_terminal_name.SufixTextVisible = True
    Me.uc_terminal_name.TabIndex = 1
    Me.uc_terminal_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.uc_terminal_name.TextValue = ""
    Me.uc_terminal_name.Value = ""
    '
    'chk_allow_cashable
    '
    Me.chk_allow_cashable.AutoSize = True
    Me.chk_allow_cashable.Location = New System.Drawing.Point(3, 34)
    Me.chk_allow_cashable.Name = "chk_allow_cashable"
    Me.chk_allow_cashable.Size = New System.Drawing.Size(116, 17)
    Me.chk_allow_cashable.TabIndex = 3
    Me.chk_allow_cashable.Text = "xAllowCashable"
    Me.chk_allow_cashable.UseVisualStyleBackColor = True
    '
    'chk_allow_promotional
    '
    Me.chk_allow_promotional.AutoSize = True
    Me.chk_allow_promotional.Location = New System.Drawing.Point(3, 57)
    Me.chk_allow_promotional.Name = "chk_allow_promotional"
    Me.chk_allow_promotional.Size = New System.Drawing.Size(131, 17)
    Me.chk_allow_promotional.TabIndex = 4
    Me.chk_allow_promotional.Text = "xAllowPromotional"
    Me.chk_allow_promotional.UseVisualStyleBackColor = True
    '
    'chk_allow_creation
    '
    Me.chk_allow_creation.AutoSize = True
    Me.chk_allow_creation.Location = New System.Drawing.Point(3, 80)
    Me.chk_allow_creation.Name = "chk_allow_creation"
    Me.chk_allow_creation.Size = New System.Drawing.Size(112, 17)
    Me.chk_allow_creation.TabIndex = 5
    Me.chk_allow_creation.Text = "xAllowCreation"
    Me.chk_allow_creation.UseVisualStyleBackColor = True
    '
    'chk_allow_bills
    '
    Me.chk_allow_bills.AutoSize = True
    Me.chk_allow_bills.Location = New System.Drawing.Point(3, 103)
    Me.chk_allow_bills.Name = "chk_allow_bills"
    Me.chk_allow_bills.Size = New System.Drawing.Size(112, 17)
    Me.chk_allow_bills.TabIndex = 6
    Me.chk_allow_bills.Text = "xAllowCreation"
    Me.chk_allow_bills.UseVisualStyleBackColor = True
    '
    'frm_tito_terminals_configuration_edit
    '
    Me.ClientSize = New System.Drawing.Size(365, 138)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Name = "frm_tito_terminals_configuration_edit"
    Me.panel_data.ResumeLayout(False)
    Me.panel_data.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents chk_allow_cashable As System.Windows.Forms.CheckBox
  Friend WithEvents uc_terminal_name As GUI_Controls.uc_entry_field
  Friend WithEvents chk_allow_creation As System.Windows.Forms.CheckBox
  Friend WithEvents chk_allow_promotional As System.Windows.Forms.CheckBox
  Friend WithEvents chk_allow_bills As System.Windows.Forms.CheckBox

End Class
