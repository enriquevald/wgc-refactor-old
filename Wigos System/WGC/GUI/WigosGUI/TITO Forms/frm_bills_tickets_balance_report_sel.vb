'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_bills_tickets_balance_report_sel
' DESCRIPTION:   .
' AUTHOR:        Humberto Braojos
' CREATION DATE: 21-06-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 13-AUG-2013  HBB    Initial version

'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports WSI.Common
Imports System.Data.SqlClient

Public Class frm_bills_tickets_balance_report_sel
  Inherits frm_base_sel

#Region " Constants "
  ' Grid Columns
  Private Const GRID_COLUMN_SENTINEL As Integer = 0
  Private Const GRID_COLUMN_TERMINAL As Integer = 1
  Private Const GRID_COLUMN_TICKET_NUMBER As Int32 = 2
  Private Const GRID_COLUMN_FACE_VALUE As Integer = 3
  Private Const GRID_COLUMN_NUM_COLLECTED As Integer = 4
  Private Const GRID_COLUMN_SUM_COLLECTED As Integer = 5
  Private Const GRID_COLUMN_NUM_EXPECTED As Integer = 6
  Private Const GRID_COLUMN_SUM_EXPECTED As Integer = 7
  Private Const GRID_COLUMN_NUM_MISMATCH As Integer = 8
  Private Const GRID_COLUMN_SUM_MISMATCH As Integer = 9

  Private Const GRID_HEADER_ROWS As Integer = 2
  Private Const GRID_COLUMNS As Integer = 10

  ' Sql Columns
  Private Const SQL_COLUMN_TICKET_OR_BILL As Integer = 0
  Private Const SQL_COLUMN_TERMINAL_NAME = 1
  Private Const SQL_COLUMN_FACE_VALUE As Integer = 2
  Private Const SQL_COLUMN_THEORETICAL_COUNT As Integer = 3
  Private Const SQL_COLUMN_REAL_COUNT As Integer = 4
  Private Const SQL_COLUMN_TICKET_NUMBER As Integer = 5
  Private Const SQL_COLUMN_TICKET_COLLECTED As Int32 = 6
  Private Const SQL_COLUMN_TICKET_VALUE As Int32 = 7
  Private Const SQL_COLUMN_TICKET_THEORETICAL_SESSION = 8
  Private Const SQL_COLUMN_TICKET_REAL_SESSION = 9

  Private Const COLOR_GAP_BACK = ENUM_GUI_COLOR.GUI_COLOR_RED_02
  Private Const COLOR_GAP_FORE = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_PENDING_BACK = ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00
  Private Const COLOR_PENDING_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00
  Private Const COLOR_COMPLETED_BACK = ENUM_GUI_COLOR.GUI_COLOR_WHITE_00
  Private Const COLOR_COMPLETED_FORE = ENUM_GUI_COLOR.GUI_COLOR_BLACK_00

  ' counters
  Private Const COUNTER_COMPLETED As Integer = 1
  Private Const COUNTER_PENDING As Integer = 2
  Private Const COUNTER_GAP As Integer = 3
  Private Const MAX_COUNTERS As Integer = 4



#End Region ' Constants

#Region " Members "

  Private m_cashier_session_id As Long
  Private m_cashier_session_name As String
  Private m_counter_list(MAX_COUNTERS) As Integer
  Private m_collection_by_terminal As Boolean
  Private m_status As String
  Private m_first_time As Boolean = True

#End Region ' Members

#Region "Overrides Functions"

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Notes gap report
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1966)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_INVOICING.GetString(1)

    ' Status
    Me.gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1971)
    Me.chk_completed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1972)
    Me.chk_pending.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1973)
    Me.chk_gap.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1974)

    Me.lbl_color_completed.BackColor = GetColor(COLOR_COMPLETED_BACK)
    Me.lbl_color_pending.BackColor = GetColor(COLOR_PENDING_BACK)
    Me.lbl_color_gap.BackColor = GetColor(COLOR_GAP_BACK)

    ' Set cashier session name
    Me.lbl_cs_name.Value = m_cashier_session_name

    ' Define grid columns
    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    ' Performs a first search
    Call GUI_ExecuteQuery()

    m_first_time = False

  End Sub ' GUI_InitControl

  ' PURPOSE: Define all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  '
  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' Completed Counter
      .Counter(COUNTER_COMPLETED).Visible = True
      .Counter(COUNTER_COMPLETED).BackColor = GetColor(COLOR_COMPLETED_BACK)
      .Counter(COUNTER_COMPLETED).ForeColor = GetColor(COLOR_COMPLETED_FORE)

      ' Pending Counter
      .Counter(COUNTER_PENDING).Visible = True
      .Counter(COUNTER_PENDING).BackColor = GetColor(COLOR_PENDING_BACK)
      .Counter(COUNTER_PENDING).ForeColor = GetColor(COLOR_PENDING_FORE)

      ' Gap Counter
      .Counter(COUNTER_GAP).Visible = True
      .Counter(COUNTER_GAP).BackColor = GetColor(COLOR_GAP_BACK)
      .Counter(COUNTER_GAP).ForeColor = GetColor(COLOR_GAP_FORE)

      '.Sortable = True

      ' SENTINEL
      .Column(GRID_COLUMN_SENTINEL).Header(0).Text = ""
      .Column(GRID_COLUMN_SENTINEL).Header(1).Text = ""
      .Column(GRID_COLUMN_SENTINEL).Width = 200

      ' TERMINAL
      .Column(GRID_COLUMN_TERMINAL).Header(0).Text = "" ' GLB_NLS_GUI_PLAYER_TRACKING.GetString(576)
      .Column(GRID_COLUMN_TERMINAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1968)
      .Column(GRID_COLUMN_TERMINAL).Width = 1700
      .Column(GRID_COLUMN_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'TICKET NUMBER
      .Column(GRID_COLUMN_TICKET_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1967)
      .Column(GRID_COLUMN_TICKET_NUMBER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2109)
      .Column(GRID_COLUMN_TICKET_NUMBER).Width = 1500
      .Column(GRID_COLUMN_TICKET_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' NOTE FACE VALUE 
      .Column(GRID_COLUMN_FACE_VALUE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1967)
      .Column(GRID_COLUMN_FACE_VALUE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(576)
      .Column(GRID_COLUMN_FACE_VALUE).Width = 1200
      .Column(GRID_COLUMN_FACE_VALUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' NUM NOTES COLLECTED
      .Column(GRID_COLUMN_NUM_COLLECTED).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(498)
      .Column(GRID_COLUMN_NUM_COLLECTED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1969)
      .Column(GRID_COLUMN_NUM_COLLECTED).Width = 1200
      .Column(GRID_COLUMN_NUM_COLLECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' SUM NOTES COLLECTED
      .Column(GRID_COLUMN_SUM_COLLECTED).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(498)
      .Column(GRID_COLUMN_SUM_COLLECTED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1970)
      .Column(GRID_COLUMN_SUM_COLLECTED).Width = 1300
      .Column(GRID_COLUMN_SUM_COLLECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' NUM NOTES EXPECTED
      .Column(GRID_COLUMN_NUM_EXPECTED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(645)
      .Column(GRID_COLUMN_NUM_EXPECTED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1969)
      .Column(GRID_COLUMN_NUM_EXPECTED).Width = 1200
      .Column(GRID_COLUMN_NUM_EXPECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' SUM NOTES EXPECTED
      .Column(GRID_COLUMN_SUM_EXPECTED).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(645)
      .Column(GRID_COLUMN_SUM_EXPECTED).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1970)
      .Column(GRID_COLUMN_SUM_EXPECTED).Width = 1300
      .Column(GRID_COLUMN_SUM_EXPECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' NUM NOTES MISMATCH
      .Column(GRID_COLUMN_NUM_MISMATCH).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(499)
      .Column(GRID_COLUMN_NUM_MISMATCH).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1969)
      .Column(GRID_COLUMN_NUM_MISMATCH).Width = 1200
      .Column(GRID_COLUMN_NUM_MISMATCH).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' SUM NOTES MISMATCH
      .Column(GRID_COLUMN_SUM_MISMATCH).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(499)
      .Column(GRID_COLUMN_SUM_MISMATCH).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1970)
      .Column(GRID_COLUMN_SUM_MISMATCH).Width = 1300
      .Column(GRID_COLUMN_SUM_MISMATCH).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

    End With
  End Sub ' GUI_StyleSheet

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    Call GUI_StyleSheet()

    ' Reset counters
    m_counter_list(COUNTER_COMPLETED) = 0
    m_counter_list(COUNTER_PENDING) = 0
    m_counter_list(COUNTER_GAP) = 0

    ' Reset collection by terminal flag
    m_collection_by_terminal = False

  End Sub ' GUI_BeforeFirsRow

  ' PURPOSE: Perform final processing for the grid data
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()

    ' Update counters
    Grid.Counter(COUNTER_COMPLETED).Value = m_counter_list(COUNTER_COMPLETED)
    Grid.Counter(COUNTER_PENDING).Value = m_counter_list(COUNTER_PENDING)
    Grid.Counter(COUNTER_GAP).Value = m_counter_list(COUNTER_GAP)

    ' Show terminal column if there is a collection by terminal
    'With Me.Grid
    '  If (m_collection_by_terminal) Then
    '    .Column(GRID_COLUMN_TERMINAL).Width = 2000
    '  Else
    '    .Column(GRID_COLUMN_TERMINAL).Width = 0
    '  End If
    'End With

  End Sub ' GUI_AfterLastRow

  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sb As StringBuilder

    _sb = New StringBuilder()
    'Dim _args As String()

    '_args = Environment.GetCommandLineArgs()

    'GLB_GuiMode = ENUM_GUI.WIGOS_GUI
    'If _args.Length > 1 AndAlso _args(1).StartsWith("TITO") Then

    'End If

    _sb.AppendLine("    SELECT   0                                                                                     ") ' this "0" means that the item is a bill
    _sb.AppendLine("           , ISNULL(TERMINAL_NAME_1, TERMINAL_NAME_2)                                              ")
    _sb.AppendLine("           , ISNULL(THEORETICAL_AMOUNT, REAL_AMOUNT)                                               ")
    _sb.AppendLine("           , ISNULL(THEORETICAL_COUNTS,0)                                                          ")
    _sb.AppendLine("           , ISNULL(REAL_COUNT,0)                                                                  ")
    _sb.AppendLine("           , '0'                                                                                   ")
    _sb.AppendLine("           , 0                                                                                     ")
    _sb.AppendLine("           , 0                                                                                     ")
    _sb.AppendLine("           , 0                                                                                     ")
    _sb.AppendLine("           , 0                                                                                     ")
    _sb.AppendLine("             FROM (SELECT   TE_NAME  AS TERMINAL_NAME_1                                            ")
    _sb.AppendLine("                          , TM_AMOUNT AS THEORETICAL_AMOUNT                                        ")
    _sb.AppendLine("                          , COUNT(TM_AMOUNT) AS THEORETICAL_COUNTS                                 ")
    _sb.AppendLine("                          , TM_CASHIER_SESSION_ID AS SESSION                                       ")
    _sb.AppendLine("                     FROM   TERMINAL_MONEY                                                         ")
    _sb.AppendLine("               INNER JOIN   TERMINALS ON TE_TERMINAL_ID = TM_TERMINAL_ID                           ")
    _sb.AppendLine("                    WHERE   TM_CASHIER_SESSION_ID = " & m_cashier_session_id.ToString() & "        ")
    _sb.AppendLine("                 GROUP BY   TM_AMOUNT, TM_CASHIER_SESSION_ID, TE_NAME) TABLA1                      ")
    _sb.AppendLine("                FULL JOIN                                                                          ")
    _sb.AppendLine("                  (SELECT   TE_NAME AS TERMINAL_NAME_2                                             ")
    _sb.AppendLine("                          , SUM(MCD_NUM_COLLECTED) AS REAL_COUNT                                   ")
    _sb.AppendLine("                          , MCD_FACE_VALUE AS REAL_AMOUNT                                          ")
    _sb.AppendLine("                          , MC_CASHIER_SESSION_ID                                                  ")
    _sb.AppendLine("                     FROM   MONEY_COLLECTION_DETAILS                                               ")
    _sb.AppendLine("               INNER JOIN   MONEY_COLLECTIONS ON MCD_COLLECTION_ID = MC_COLLECTION_ID              ")
    _sb.AppendLine("               INNER JOIN   TERMINALS ON MC_TERMINAL_ID  = TE_TERMINAL_ID                          ")
    _sb.AppendLine("                    WHERE   MC_CASHIER_SESSION_ID = " & m_cashier_session_id.ToString())
    _sb.AppendLine("                 GROUP BY   MCD_FACE_VALUE, MC_CASHIER_SESSION_ID, TE_NAME) TABLA2                 ")
    _sb.AppendLine("        ON   TABLA1.THEORETICAL_AMOUNT = TABLA2.REAL_AMOUNT                                        ")
    _sb.AppendLine(GetSqlWhere())
    _sb.AppendLine("     UNION                                                                                         ")
    _sb.AppendLine("    SELECT   1                                                                                     ") ' this "1" means that the item is a ticket
    _sb.AppendLine("           , TE_NAME                                                                               ")
    _sb.AppendLine("           , 0                                                                                     ")
    _sb.AppendLine("           , 0                                                                                     ")
    _sb.AppendLine("           , 0                                                                                     ")
    _sb.AppendLine("           , TI_VALIDATION_NUMBER                                                                  ")
    _sb.AppendLine("           , ISNULL(TI_COLLECTED, 0)                                                               ")
    _sb.AppendLine("           , TI_AMOUNT                                                                             ")
    _sb.AppendLine("           , TI_LAST_ACTION_CASHIER_SESSION                                                                 ")
    _sb.AppendLine("           , MC_CASHIER_SESSION_ID                                                                 ")
    _sb.AppendLine("      FROM   TICKETS                                                                               ")
    _sb.AppendLine("INNER JOIN   TERMINALS ON TE_TERMINAL_ID = TI_LAST_ACTION_TERMINAL_ID                              ")
    _sb.AppendLine(" LEFT JOIN   MONEY_COLLECTIONS ON TI_MONEY_COLLECTION_ID = MC_COLLECTION_ID                        ")
    _sb.AppendLine("     WHERE   TI_LAST_ACTION_CASHIER_SESSION = " & m_cashier_session_id.ToString() & " OR (MC_CASHIER_SESSION_ID = " & m_cashier_session_id.ToString() & " AND TI_COLLECTED = 1)")

    Return _sb.ToString()

  End Function

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean

    Dim _face_value As Double
    Dim _real_count As Int32
    Dim _theoretical_count As Int32
    Dim _real_tickets_count As Int32
    Dim _theoretical_tickets_count As Int32
    Dim _num_mismatch As Integer
    Dim _ticket_value As Double

    _face_value = DbRow.Value(SQL_COLUMN_FACE_VALUE)
    _real_count = DbRow.Value(SQL_COLUMN_REAL_COUNT)
    _theoretical_count = DbRow.Value(SQL_COLUMN_THEORETICAL_COUNT)
    _ticket_value = DbRow.Value(SQL_COLUMN_TICKET_VALUE)
    _real_tickets_count = 0
    _theoretical_tickets_count = 0

    If DbRow.Value(SQL_COLUMN_TICKET_OR_BILL) = 0 Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FACE_VALUE).Value = GUI_FormatCurrency(_face_value)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_COLLECTED).Value = _real_count
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_COLLECTED).Value = GUI_FormatCurrency(_real_count * _face_value)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_EXPECTED).Value = _theoretical_count
      Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_EXPECTED).Value = GUI_FormatCurrency(_theoretical_count * _face_value)
      _num_mismatch = _real_count - _theoretical_count
      If _num_mismatch <> 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_MISMATCH).Value = _num_mismatch
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_MISMATCH).Value = GUI_FormatCurrency((_num_mismatch) * _face_value)
      End If
    Else
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TICKET_NUMBER).Value = DbRow.Value(SQL_COLUMN_TICKET_NUMBER)

      If Not IsDBNull(DbRow.Value(SQL_COLUMN_TICKET_REAL_SESSION)) AndAlso DbRow.Value(SQL_COLUMN_TICKET_REAL_SESSION) = m_cashier_session_id Then
        If DbRow.Value(SQL_COLUMN_TICKET_COLLECTED) = 1 Then
          Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_COLLECTED).Value = 1
          Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_COLLECTED).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TICKET_VALUE))
          _real_tickets_count = 1
        End If
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_COLLECTED).Value = 0
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_COLLECTED).Value = GUI_FormatCurrency(0)

      End If

      If DbRow.Value(SQL_COLUMN_TICKET_THEORETICAL_SESSION) = m_cashier_session_id Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_EXPECTED).Value = 1
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_EXPECTED).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TICKET_VALUE))
        _theoretical_tickets_count = 1
      Else
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_EXPECTED).Value = 0
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_EXPECTED).Value = GUI_FormatCurrency(0)
      End If

      _num_mismatch = _real_tickets_count - _theoretical_tickets_count
      If _num_mismatch <> 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_NUM_MISMATCH).Value = _num_mismatch
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_MISMATCH).Value = GUI_FormatCurrency((_num_mismatch) * _ticket_value)
      End If

      If Not Me.chk_completed.Checked AndAlso (chk_gap.Checked Or chk_pending.Checked) AndAlso _num_mismatch = 0 Then
        Return False
      End If
      If Not chk_gap.Checked AndAlso (chk_completed.Checked Or chk_pending.Checked) AndAlso _num_mismatch > 0 Then
        Return False
      End If
      If Not chk_pending.Checked AndAlso (chk_completed.Checked Or chk_gap.Checked) AndAlso _num_mismatch < 0 Then
        Return False
      End If
      End If

      Select Case _num_mismatch
        Case Is = 0
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_MISMATCH).BackColor = GetColor(COLOR_COMPLETED_BACK)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_MISMATCH).ForeColor = GetColor(COLOR_COMPLETED_FORE)
          m_counter_list(COUNTER_COMPLETED) = m_counter_list(COUNTER_COMPLETED) + 1
        Case Is < 0
          Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_MISMATCH).BackColor = GetColor(COLOR_PENDING_BACK)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_MISMATCH).ForeColor = GetColor(COLOR_PENDING_FORE)
          m_counter_list(COUNTER_PENDING) = m_counter_list(COUNTER_PENDING) + 1
        Case Is > 0
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_MISMATCH).BackColor = GetColor(COLOR_GAP_BACK)
        Me.Grid.Cell(RowIndex, GRID_COLUMN_SUM_MISMATCH).ForeColor = GetColor(COLOR_GAP_FORE)
          m_counter_list(COUNTER_GAP) = m_counter_list(COUNTER_GAP) + 1
      End Select

      Return True

  End Function

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(629), Me.m_cashier_session_name)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1971), m_status)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 4000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    Dim _all As Boolean
    Dim _no_one As Boolean

    m_status = ""
    _all = True
    _no_one = True

    If chk_completed.Checked Then
      m_status = m_status & chk_completed.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_pending.Checked Then
      m_status = m_status & chk_pending.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_gap.Checked Then
      m_status = m_status & chk_gap.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If m_status.Length <> 0 Then
      m_status = Strings.Left(m_status, Len(m_status) - 2)
    End If

    If _no_one Or _all Then
      m_status = GLB_NLS_GUI_ALARMS.GetString(277)
    End If


  End Sub ' GUI_ReportUpdateFilters()

#End Region

#Region " Private Functions"

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Me.chk_completed.Checked = True
    Me.chk_pending.Checked = True
    Me.chk_gap.Checked = True

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to build SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String
    'WHERE                                         

    Dim _str_where As String = ""

    ' Filter status
    If Me.chk_completed.Checked Then
      _str_where = " REAL_COUNT = THEORETICAL_COUNTS "
    End If

    If Me.chk_pending.Checked Then
      If _str_where <> "" Then
        _str_where &= " OR ISNULL(REAL_COUNT, 0) < THEORETICAL_COUNTS "
      Else
        _str_where = " ISNULL(REAL_COUNT, 0) < THEORETICAL_COUNTS "
      End If
    End If

    If Me.chk_gap.Checked Then
      If _str_where <> "" Then
        _str_where &= " OR REAL_COUNT > ISNULL(THEORETICAL_COUNTS, 0) "
      Else
        _str_where = " REAL_COUNT > ISNULL(THEORETICAL_COUNTS, 0) "
      End If
    End If

    If Not String.IsNullOrEmpty(_str_where) Then
      _str_where = " WHERE " & _str_where
    End If

    Return _str_where

  End Function ' GetSqlWhere

#End Region

#Region "Public Functions"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Sub ShowModalForEdit(ByVal session_id As Integer, ByVal session_name As String)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING

    Me.m_cashier_session_id = session_id
    Me.m_cashier_session_name = session_name
    Me.m_collection_by_terminal = False

    ' Me.Display(True)
    Me.Show()

  End Sub ' ShowForEdit
#End Region

End Class