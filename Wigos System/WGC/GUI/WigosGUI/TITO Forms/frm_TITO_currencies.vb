'-------------------------------------------------------------------
' Copyright � 2012 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME : frm_TITO_currencies.vb
'
' DESCRIPTION : Currencies for NoteAcceptors 
' 
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 11-JUL-2013  FBA    Initial version  
' 23-AUG-2013  JRM    Code revision and refactoring
' 29-AUG-2013  NMR    Fixed error while saving TITO currency rejected
'--------------------------------------------------------------------
Option Strict Off
Option Explicit On

#Region "Imports"

Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls
Imports GUI_Controls.CLASS_FILTER.ENUM_FORMAT
Imports GUI_Controls.uc_grid
Imports GUI_Controls.uc_grid.CLASS_BUTTON.ENUM_BUTTON_TYPE
Imports GUI_Controls.uc_grid.CLASS_COL_DATA.CLASS_CONTROL.ENUM_CONTROL_TYPE
Imports WSI.Common
Imports System.Runtime.InteropServices
Imports System.Windows.Forms.UserControl

#End Region           ' Imports

Public Class frm_TITO_currencies
  Inherits frm_base_edit

#Region " Constants "

  Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_CUR_CURRENCY_ALLOWED As Integer = 1
  Private Const GRID_COLUMN_CUR_CURRENCY_TITO_ALLOWED As Integer = 2
  Private Const GRID_COLUMN_CUR_CURRENCY_ISO_CODE As Integer = 3
  Private Const GRID_COLUMN_CUR_CURRENCY_NAME As Integer = 4
  Private Const GRID_COLUMN_CUR_CURRENCY_ALIAS_1 As Integer = 5
  Private Const GRID_COLUMN_CUR_CURRENCY_ALIAS_2 As Integer = 6
  Private Const GRID_COLUMNS_CUR As Integer = 7
  Private Const GRID_HEADER_ROWS_CUR As Integer = 1
  Private Const GRID_WIDTH_INDEX As Integer = 150
  Private Const GRID_WIDTH_CUR_CURRENCY_ISO_CODE As Integer = 900
  Private Const GRID_WIDTH_CUR_CURRENCY_ALLOWED As Integer = 1100
  Private Const GRID_WIDTH_CUR_CURRENCY_NAME As Integer = 2300
  Private Const GRID_WIDTH_CUR_CURRENCY_ALIAS_1 As Integer = 1100
  Private Const GRID_WIDTH_CUR_CURRENCY_ALIAS_2 As Integer = 1100
  Private Const GRID_WIDTH_CUR_CURRENCY_TITO_ALLOWED As Integer = 1500

  'Private Const GRID_COLUMN_INDEX As Integer = 0
  Private Const GRID_COLUMN_BILL_CURRENCY_ISO_CODE As Integer = 1
  Private Const GRID_COLUMN_BILL_CURRENCY_TYPE As Integer = 2
  Private Const GRID_COLUMN_BILL_CURRENCY_DENOMINATION As Integer = 3
  Private Const GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE As Integer = 4
  Private Const GRID_COLUMN_BILL_CURRENCY_REJECTED As Integer = 5
  Private Const GRID_COLUMN_BILL_CURRENCY_EDITABLE As Integer = 6       '0-No editable, 1-Editable, 8-Deleted
  Private Const GRID_COLUMN_BILL_CURRENCY_TITO_REJECTED As Integer = 7
  Private Const GRID_COLUMNS_BILL As Integer = 8
  Private Const GRID_HEADER_ROWS_BILL As Integer = 1

  'Private Const GRID_WIDTH_INDEX As Integer = 150
  Private Const GRID_WIDTH_BILL_CURRENCY_ISO_CODE As Integer = 1000
  Private Const GRID_WIDTH_BILL_CURRENCY_TYPE As Integer = 1200
  Private Const GRID_WIDTH_BILL_CURRENCY_DENOMINATION As Integer = 1740
  Private Const GRID_WIDTH_BILL_CURRENCY_ENABLED As Integer = 1200
  Private Const GRID_WIDTH_BILL_CURRENCY_TITO_ENABLED As Integer = 1500

  Private Const MIN_CURRENCIES_ALLOWED As Integer = 1
  Private Const MAX_CURRENCIES_ALLOWED As Integer = 2
  Private Const MIN_CURRENCIES_ALLOWED_TITO As Integer = 1
  Private Const MAX_CURRENCIES_ALLOWED_TITO As Integer = 1

#End Region       ' Constants

#Region " Members "

  ' For return currencies selected 
  Private CurrenciesSelected() As Integer
  Private m_currency_selected As String = ""

#End Region         ' Members

#Region "Overrides"

  ' PURPOSE: Sets the proper form identifier
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  'RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = 0

    ' Set the form icon from the embedded resource (ICO file must be built in the project as "Embedded Resource")
    ' Call could be made as GetManifestResourceStream("GUI_JackpotManager.GUI_JackpotManager.ico"))
    ' Me.Icon = New Icon(System.Reflection.Assembly.GetExecutingAssembly.GetManifestResourceStream(Me.GetType(), "GUI_JackpotManager.ico"))

    Call MyBase.GUI_SetFormId()

  End Sub               ' GUI_SetFormId

  ' PURPOSE: Initializes form controls
  '         
  ' PARAMS:
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_InitControls()
    ' Initialize parent control, required
    Call MyBase.GUI_InitControls()

    ' - Form
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1056)

    ' Enable / Disable controls
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Enabled = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_DELETE).Visible = False
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL).Enabled = True

    Me.btn_add.Enabled = Me.Permissions.Write
    Me.btn_del.Enabled = Me.Permissions.Write

    Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_OK).Text = GLB_NLS_GUI_CONTROLS.GetString(1)

    Me.btn_add.Text = GLB_NLS_GUI_CONTROLS.GetString(12)
    Me.btn_del.Text = GLB_NLS_GUI_CONTROLS.GetString(11)

    Me.gb_bills.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1059)
    Me.l_remarks_c.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1072)
    Me.l_remarks_b.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1065)

    Me.dg_currencies.SelectionMode = SELECTION_MODE.SELECTION_MODE_SINGLE

    ' Player Tracking
    Call GUI_StyleSheet()

  End Sub         ' GUI_InitControls

  Protected Overrides Sub GUI_SetInitialFocus()
    Me.ActiveControl = Me.GUI_Button(frm_base_edit.ENUM_BUTTON.BUTTON_CANCEL)
  End Sub      ' GUI_SetInitialFocus

  Protected Overrides Sub GUI_GetScreenData()

    Dim currency_config_edit As CLASS_TITO_CURRENCY
    Dim currency_config_read As CLASS_TITO_CURRENCY

    Try

      currency_config_edit = Me.DbEditedObject
      currency_config_read = Me.DbReadObject

      GetScreenCurrencies(currency_config_edit.DataCurrency)
      GetScreenCurrDenomin(currency_config_edit.DataCurrencyDenomination)

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, Me.Name, "GUI_GetScreenData", ex.Message)
    Finally

    End Try

  End Sub        ' GUI_GetScreenData

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim currency_data As CLASS_TITO_CURRENCY

    Try
      currency_data = Me.DbReadObject

      SetScreenCurrencies(currency_data.DataCurrency)
      SetScreenCurrDenomin(currency_data.DataCurrencyDenomination)

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            Me.Name, _
                            "GUI_SetScreenData", _
                            ex.Message)

      ' 137 "Exception error has been found: \n%1"
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

    Finally

    End Try

  End Sub        ' GUI_SetScreenData

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As GUI_Controls.frm_base_edit.ENUM_DB_OPERATION)

    Dim currency_data As CLASS_TITO_CURRENCY
    ' add here new currency class objects, once per control

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_TITO_CURRENCY
        currency_data = DbEditedObject
        ' add here new currency data, once per control

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(105), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case frm_base_edit.ENUM_DB_OPERATION.DB_OPERATION_AFTER_DELETE
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_JACKPOT_MGR.Id(106), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub         ' GUI_DB_Operation

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _count As Integer = 0
    Dim _count_TITO As Integer = 0

    For _idx_row As Integer = 0 To Me.dg_currencies.NumRows - 1
      If GridValueToBool(Me.dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ALLOWED).Value) Then
        _count += 1
      End If
      If GridValueToBool(Me.dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_TITO_ALLOWED).Value) Then
        _count_TITO += 1
      End If
    Next

    If _count < MIN_CURRENCIES_ALLOWED Or _count > MAX_CURRENCIES_ALLOWED  Then
      Call dg_currencies.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1066), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

      Return False
    End If

    If _count_TITO < MIN_CURRENCIES_ALLOWED_TITO Or _count_TITO > MAX_CURRENCIES_ALLOWED_TITO Then
      Call dg_currencies.Focus()
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2375), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

      Return False
    End If

    For _idx_row As Integer = 0 To Me.dg_bills.NumRows - 1
      If Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Value <= 0 Then
        Call dg_bills.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1067), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      End If
    Next

    Dim currency_data As CLASS_TITO_CURRENCY
    Call GUI_GetScreenData()
    currency_data = Me.DbEditedObject

    For Each _currency As DataRow In currency_data.DataCurrencyDenomination.Rows
      ' _currency(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_EDITABLE) <> DataRowState.Deleted, is equals not marked for deletion
      If _currency.RowState <> DataRowState.Deleted _
         AndAlso _currency(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_DENOMINATION) = 0 _
         AndAlso _currency(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_EDITABLE) <> DataRowState.Deleted Then

        Call dg_bills.Focus()
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1067), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

        Return False
      End If
    Next

    '     - Bills Check
    If Not IsScreenDataOkBills() Then

      Return False
    End If

    Return True

  End Function  ' GUI_IsScreenDataOk

  Protected Overrides Function GUI_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) As Boolean

    Select Case e.KeyChar
      Case Chr(Keys.Enter)

        If Me.dg_currencies.ContainsFocus Then
          Me.dg_currencies.KeyPressed(sender, e)
        End If

        If Me.dg_bills.ContainsFocus Then
          Me.dg_bills.KeyPressed(sender, e)
        End If

        Return True
      Case Chr(Keys.Escape)
        Return MyBase.GUI_KeyPress(sender, e)
      Case Else
    End Select

    ' The keypress event is done in uc_grid control

  End Function        ' GUI_KeyPress

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_edit.ENUM_BUTTON)
    'If ButtonId = ENUM_BUTTON.BUTTON_CUSTOM_0 Then
    If ButtonId = ENUM_BUTTON.BUTTON_OK Then
      ApplyButton()
    Else
      MyBase.GUI_ButtonClick(ButtonId)
    End If

  End Sub          ' GUI_ButtonClick

#End Region         ' Overrides

#Region "Private Functions"

  Private Sub ApplyButton()

    If Not Permissions.Write Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Exit Sub
    End If

    'added to change the focus from the date picker
    Me.ActiveControl = Me.AcceptButton

    ' Check the data on the screen
    If Not GUI_IsScreenDataOk() Then
      Exit Sub
    End If

    Call GUI_GetScreenData()

    If Not DbReadObject.AuditorData.IsEqual(DbEditedObject.AuditorData) Then

      ' Save data when some option has been changed, added or deleted in the form
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE)
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AUDIT_UPDATE)

      If DbStatus = ENUM_STATUS.STATUS_OK Then
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_UPDATE)
      End If

      If DbStatus = ENUM_STATUS.STATUS_OK Then
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE)
      End If

      If DbStatus = ENUM_STATUS.STATUS_OK Then
        Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
      End If

    End If

    Call GUI_Exit()

  End Sub              ' ApplyButton

  ' PURPOSE: Define all Grid Points Multiplier Per currency Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    With Me.dg_currencies
      Call .Init(GRID_COLUMNS_CUR, GRID_HEADER_ROWS_CUR, True)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header.Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Currency enable
      .Column(GRID_COLUMN_CUR_CURRENCY_ALLOWED).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1058)
      .Column(GRID_COLUMN_CUR_CURRENCY_ALLOWED).Width = GRID_WIDTH_CUR_CURRENCY_ALLOWED
      .Column(GRID_COLUMN_CUR_CURRENCY_ALLOWED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_CUR_CURRENCY_ALLOWED).IsMerged = False
      .Column(GRID_COLUMN_CUR_CURRENCY_ALLOWED).Editable = True
      .Column(GRID_COLUMN_CUR_CURRENCY_ALLOWED).EditionControl.Type = CONTROL_TYPE_CHECK_BOX

      ' Currency Id
      .Column(GRID_COLUMN_CUR_CURRENCY_ISO_CODE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1057)
      .Column(GRID_COLUMN_CUR_CURRENCY_ISO_CODE).Width = GRID_WIDTH_CUR_CURRENCY_ISO_CODE
      .Column(GRID_COLUMN_CUR_CURRENCY_ISO_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Description
      .Column(GRID_COLUMN_CUR_CURRENCY_NAME).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1069)
      .Column(GRID_COLUMN_CUR_CURRENCY_NAME).Width = GRID_WIDTH_CUR_CURRENCY_NAME
      .Column(GRID_COLUMN_CUR_CURRENCY_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_CUR_CURRENCY_NAME).IsMerged = False
      .Column(GRID_COLUMN_CUR_CURRENCY_NAME).Editable = True
      .Column(GRID_COLUMN_CUR_CURRENCY_NAME).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_CUR_CURRENCY_NAME).EditionControl.EntryField.SetFilter(FORMAT_TEXT, 50)


      ' Alias 1
      .Column(GRID_COLUMN_CUR_CURRENCY_ALIAS_1).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1070)
      .Column(GRID_COLUMN_CUR_CURRENCY_ALIAS_1).Width = GRID_WIDTH_CUR_CURRENCY_ALIAS_1
      .Column(GRID_COLUMN_CUR_CURRENCY_ALIAS_1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      If Permissions.Execute Then
        .Column(GRID_COLUMN_CUR_CURRENCY_ALIAS_1).IsMerged = False
        .Column(GRID_COLUMN_CUR_CURRENCY_ALIAS_1).Editable = True
        .Column(GRID_COLUMN_CUR_CURRENCY_ALIAS_1).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_CUR_CURRENCY_ALIAS_1).EditionControl.EntryField.SetFilter(FORMAT_TEXT, 3)
      End If

      ' Alias 2
      .Column(GRID_COLUMN_CUR_CURRENCY_ALIAS_2).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1071)
      .Column(GRID_COLUMN_CUR_CURRENCY_ALIAS_2).Width = GRID_WIDTH_CUR_CURRENCY_ALIAS_2
      .Column(GRID_COLUMN_CUR_CURRENCY_ALIAS_2).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      If Permissions.Execute Then
        .Column(GRID_COLUMN_CUR_CURRENCY_ALIAS_2).IsMerged = False
        .Column(GRID_COLUMN_CUR_CURRENCY_ALIAS_2).Editable = True
        .Column(GRID_COLUMN_CUR_CURRENCY_ALIAS_2).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
        .Column(GRID_COLUMN_CUR_CURRENCY_ALIAS_2).EditionControl.EntryField.SetFilter(FORMAT_TEXT, 3)
      End If

      ' TITO Currency enable
      .Column(GRID_COLUMN_CUR_CURRENCY_TITO_ALLOWED).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2350)
      .Column(GRID_COLUMN_CUR_CURRENCY_TITO_ALLOWED).Width = GRID_WIDTH_CUR_CURRENCY_TITO_ALLOWED
      .Column(GRID_COLUMN_CUR_CURRENCY_TITO_ALLOWED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_CUR_CURRENCY_TITO_ALLOWED).IsMerged = False
      .Column(GRID_COLUMN_CUR_CURRENCY_TITO_ALLOWED).Editable = True
      .Column(GRID_COLUMN_CUR_CURRENCY_TITO_ALLOWED).EditionControl.Type = CONTROL_TYPE_CHECK_BOX

    End With

    With Me.dg_bills
      Call .Init(GRID_COLUMNS_BILL, GRID_HEADER_ROWS_BILL, True)

      ' Index
      .Column(GRID_COLUMN_INDEX).Header.Text = " "
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' Currency Id
      .Column(GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1057)
      .Column(GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Width = GRID_WIDTH_BILL_CURRENCY_ISO_CODE
      .Column(GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Currency type
      .Column(GRID_COLUMN_BILL_CURRENCY_TYPE).Header.Text = ""
      .Column(GRID_COLUMN_BILL_CURRENCY_TYPE).Width = 0
      .Column(GRID_COLUMN_BILL_CURRENCY_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Currency value
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Header.Text = ""
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Width = 0
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Currency value
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1060)
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Width = GRID_WIDTH_BILL_CURRENCY_DENOMINATION
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).IsMerged = False
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Editable = True
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).EditionControl.Type = CONTROL_TYPE_ENTRY_FIELD
      .Column(GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).EditionControl.EntryField.SetFilter(FORMAT_NUMBER, 10, 0)

      ' Currency enable
      .Column(GRID_COLUMN_BILL_CURRENCY_REJECTED).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1061)
      .Column(GRID_COLUMN_BILL_CURRENCY_REJECTED).Width = GRID_WIDTH_BILL_CURRENCY_ENABLED
      .Column(GRID_COLUMN_BILL_CURRENCY_REJECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_BILL_CURRENCY_REJECTED).IsMerged = False
      .Column(GRID_COLUMN_BILL_CURRENCY_REJECTED).Editable = True
      .Column(GRID_COLUMN_BILL_CURRENCY_REJECTED).EditionControl.Type = CONTROL_TYPE_CHECK_BOX

      ' Editable value row
      .Column(GRID_COLUMN_BILL_CURRENCY_EDITABLE).Header.Text = ""
      .Column(GRID_COLUMN_BILL_CURRENCY_EDITABLE).Width = 0
      .Column(GRID_COLUMN_BILL_CURRENCY_EDITABLE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' TITO BILL Currency enable
      .Column(GRID_COLUMN_BILL_CURRENCY_TITO_REJECTED).Header.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2351)
      .Column(GRID_COLUMN_BILL_CURRENCY_TITO_REJECTED).Width = GRID_WIDTH_BILL_CURRENCY_TITO_ENABLED
      .Column(GRID_COLUMN_BILL_CURRENCY_TITO_REJECTED).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER
      .Column(GRID_COLUMN_BILL_CURRENCY_TITO_REJECTED).IsMerged = False
      .Column(GRID_COLUMN_BILL_CURRENCY_TITO_REJECTED).Editable = True
      .Column(GRID_COLUMN_BILL_CURRENCY_TITO_REJECTED).EditionControl.Type = CONTROL_TYPE_CHECK_BOX

    End With

    'checks if the app is running in tito mode
    If Not GeneralParam.GetBoolean("TITO", "TITOMode") Then
      Me.dg_currencies.Column(GRID_COLUMN_CUR_CURRENCY_TITO_ALLOWED).Width = 0
      Me.dg_bills.Column(GRID_COLUMN_BILL_CURRENCY_TITO_REJECTED).Width = 0
    End If

  End Sub           ' GUI_StyleSheet

  ' PURPOSE: Update the currencies DataTable with the allowed value from uc_grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - currencies As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GetScreenCurrencies(ByVal Currencies As DataTable)
    Dim _currencies() As DataRow

    For _idx_row As Integer = 0 To Me.dg_currencies.NumRows - 1
      _currencies = Currencies.Select("CUR_ISO_CODE = '" & Me.dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ISO_CODE).Value & "' ")

      If _currencies.Length = 1 Then
        If _currencies(0)(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_ALLOWED) <> GridValueToBool(Me.dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ALLOWED).Value) Then
          _currencies(0)(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_ALLOWED) = GridValueToBool(Me.dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ALLOWED).Value)
        End If

        If _currencies(0)(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_NAME) <> Me.dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_NAME).Value Then
          _currencies(0)(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_NAME) = Me.dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_NAME).Value
        End If

        If _currencies(0)(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_ALIAS_1) <> Me.dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ALIAS_1).Value Then
          _currencies(0)(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_ALIAS_1) = Me.dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ALIAS_1).Value
        End If

        If _currencies(0)(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_ALIAS_2) <> Me.dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ALIAS_2).Value Then
          _currencies(0)(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_ALIAS_2) = Me.dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ALIAS_2).Value
        End If

        If _currencies(0)(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_TITO_ALLOWED) <> GridValueToBool(Me.dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_TITO_ALLOWED).Value) Then
          _currencies(0)(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_TITO_ALLOWED) = GridValueToBool(Me.dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_TITO_ALLOWED).Value)
        End If
      End If

    Next

  End Sub      ' GetScreenCurrencies

  ' PURPOSE: Update the currencies denominations DataTable with the rejected value from uc_grid 
  '
  '  PARAMS:
  '     - INPUT:
  '           - currencies denominations As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GetScreenCurrDenomin(ByVal CurrDenomination As DataTable)
    Dim _cur_denomi() As DataRow

    For _idx_row As Integer = 0 To Me.dg_bills.NumRows - 1
      _cur_denomi = CurrDenomination.Select("    CUD_ISO_CODE = '" & Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Value & "' " & _
                                    "AND CUD_TYPE = " & Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_TYPE).Value & " " & _
                                    "AND CUD_DENOMINATION = " & GUI_LocalNumberToDBNumber(Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Value) & " " & _
                                    "AND EDITABLE_ROW <> " & DataRowState.Deleted & " ")

      If _cur_denomi.Length >= 1 Then
        If _cur_denomi(_cur_denomi.Length - 1)(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_REJECTED) <> GridValueToBool(Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_REJECTED).Value) Then
          _cur_denomi(_cur_denomi.Length - 1)(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_REJECTED) = GridValueToBool(Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_REJECTED).Value)
        End If

        If _cur_denomi(_cur_denomi.Length - 1)(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_DENOMINATION) <> Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Value Then
          _cur_denomi(_cur_denomi.Length - 1)(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_DENOMINATION) = Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Value
        End If

        If _cur_denomi(_cur_denomi.Length - 1)(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_TITO_REJECTED) <> GridValueToBool(Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_TITO_REJECTED).Value) Then
          _cur_denomi(_cur_denomi.Length - 1)(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_TITO_REJECTED) = GridValueToBool(Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_TITO_REJECTED).Value)
        End If

        Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Value = CDec(Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Value)
      End If

    Next

  End Sub     ' GetScreenCurrDenomin

  ' PURPOSE: Set the uc_grid s from the screen data with the values from the currencys DataTable.
  '
  '  PARAMS:
  '     - INPUT:
  '           - currencys As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetScreenCurrencies(ByVal currencies As DataTable)
    Dim _idx_row As Integer
    Dim _idx_row_selected As Integer = -1
    ' TODO: ask about this tripple assigment 
    dg_currencies.Redraw = False
    dg_currencies.Clear()
    dg_currencies.Redraw = True
    dg_currencies.Redraw = False

    Try
      For Each _currency As DataRow In currencies.Rows
        dg_currencies.AddRow()
        _idx_row = dg_currencies.NumRows - 1

        dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ISO_CODE).Value = _currency(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_ISO_CODE)
        dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ALLOWED).Value = BoolToGridValue(_currency(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_ALLOWED))
        dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_TITO_ALLOWED).Value = BoolToGridValue(_currency(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_TITO_ALLOWED))

        If IsDBNull(_currency(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_NAME)) Then
          dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_NAME).Value = ""
        Else
          dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_NAME).Value = _currency(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_NAME)
        End If

        If IsDBNull(_currency(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_ALIAS_1)) Then
          dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ALIAS_1).Value = ""
        Else
          dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ALIAS_1).Value = _currency(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_ALIAS_1).ToString()
        End If

        If IsDBNull(_currency(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_ALIAS_2)) Then
          dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ALIAS_2).Value = ""
        Else
          dg_currencies.Cell(_idx_row, GRID_COLUMN_CUR_CURRENCY_ALIAS_2).Value = _currency(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_ALIAS_2).ToString()
        End If

        If _currency(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_ALLOWED) = True And _idx_row_selected = -1 Then
          _idx_row_selected = _idx_row
        End If

        If _currency(CLASS_TITO_CURRENCY.SQL_COLUMN_CUR_CURRENCY_TITO_ALLOWED) = True And _idx_row_selected = -1 Then
          _idx_row_selected = _idx_row
        End If
      Next

    Finally

      dg_currencies.AddRow()
      _idx_row = dg_currencies.NumRows - 1
      Me.dg_currencies.Row(_idx_row).Height = 0

      If dg_currencies.NumRows > 0 And m_currency_selected = "" Then
        Me.dg_currencies.SelectFirstRow(True)
        Call dg_currencies_RowSelectedEvent(0)
      End If

      If _idx_row_selected >= 0 And m_currency_selected <> "" Then
        Me.dg_currencies.IsSelected(0) = False
        Me.dg_currencies.IsSelected(_idx_row_selected) = True

        Call dg_currencies_RowSelectedEvent(_idx_row_selected)
      End If

      dg_currencies.Redraw = True
    End Try

  End Sub      ' SetScreenCurrencies

  ' PURPOSE: Set the uc_grid s from the screen data with the values from the currencys denomination DataTable.
  '
  '  PARAMS:
  '     - INPUT:
  '           - currencys denomination As DataTable
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetScreenCurrDenomin(ByVal currencies As DataTable)
    Dim _idx_row As Integer

    dg_bills.Redraw = False
    dg_bills.Clear()
    dg_bills.Redraw = True

    dg_bills.Redraw = False

    Try
      For Each _currency As DataRow In currencies.Rows
        ' _currency(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_EDITABLE) <> DataRowState.Deleted, is equals not marked for deletion
        If _currency.RowState <> DataRowState.Deleted _
           AndAlso _currency(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_ISO_CODE) = m_currency_selected _
           AndAlso _currency(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_EDITABLE) <> DataRowState.Deleted Then

          dg_bills.AddRow()
          _idx_row = dg_bills.NumRows - 1

          dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Value = _currency(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_ISO_CODE)
          dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_TYPE).Value = GUI_FormatNumber(_currency(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_TYPE), 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
          dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Value = _currency(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_DENOMINATION)
          dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Value = GUI_FormatNumber(_currency(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_DENOMINATION), 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
          dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_REJECTED).Value = BoolToGridValue(_currency(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_REJECTED))
          dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_TITO_REJECTED).Value = BoolToGridValue(_currency(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_TITO_REJECTED))
          dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_EDITABLE).Value = _currency(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_EDITABLE)

          If _currency(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_EDITABLE) = 1 Then
            dg_bills.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)
          End If

        End If
      Next

    Finally
      dg_bills.Redraw = True
    End Try

  End Sub     ' SetScreenCurrDenomin

  ' PURPOSE:  Cast from boolean to uc_grid checked
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function BoolToGridValue(ByVal enabled As Boolean) As String
    If (enabled) Then
      Return uc_grid.GRID_CHK_CHECKED
    Else
      Return uc_grid.GRID_CHK_UNCHECKED
    End If
  End Function     ' BoolToGridValue

  ' PURPOSE:  Cast from uc_grid checked to boolean
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GridValueToBool(ByVal enabled As String) As Boolean

    If (enabled.Equals(uc_grid.GRID_CHK_CHECKED) Or enabled.Equals(uc_grid.GRID_CHK_CHECKED_DISABLED)) Then
      Return True
    Else
      Return False
    End If

  End Function     ' GridValueToBool

  Private Sub InsertBillRow()
    Dim _idx_row As Integer
    Dim _currency_config_edit As CLASS_TITO_CURRENCY
    Dim _array_value(5) As String

    If Not Permissions.Write Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Exit Sub
    End If

    If dg_bills.NumRows < 10 Then

      dg_bills.AddRow()
      _idx_row = dg_bills.NumRows - 1

      dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Value = m_currency_selected
      dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_TYPE).Value = 0
      dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Value = 0
      dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Value = GUI_FormatNumber(0, 0, ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT)
      dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_REJECTED).Value = BoolToGridValue(True)
      dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_TITO_REJECTED).Value = BoolToGridValue(True)
      dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_EDITABLE).Value = 1
      dg_bills.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

      _array_value(0) = Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Value
      _array_value(1) = Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_TYPE).Value
      _array_value(2) = Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE).Value
      _array_value(3) = GridValueToBool(Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_REJECTED).Value)
      _array_value(4) = Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_EDITABLE).Value
      _array_value(5) = GridValueToBool(Me.dg_bills.Cell(_idx_row, GRID_COLUMN_BILL_CURRENCY_TITO_REJECTED).Value)

      _currency_config_edit = Me.DbEditedObject
      _currency_config_edit.DataCurrencyDenomination.Rows.Add(_array_value)

      Call GUI_GetScreenData()

    End If

  End Sub            ' InsertBillRow

  Private Sub DeleteBillRow()
    Dim _currency_config_edit As CLASS_TITO_CURRENCY
    Dim _cur_denomi() As DataRow

    Erase CurrenciesSelected

    If Not Permissions.Delete Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(109), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

      Exit Sub
    End If

    CurrenciesSelected = Me.dg_bills.SelectedRows
    If IsNothing(CurrenciesSelected) Then
      Exit Sub
    End If

    _currency_config_edit = Me.DbEditedObject
    GetScreenCurrDenomin(_currency_config_edit.DataCurrencyDenomination)

    For _idx_row As Integer = 0 To CurrenciesSelected.Length - 1

      _cur_denomi = _currency_config_edit.DataCurrencyDenomination.Select("    CUD_ISO_CODE = '" & Me.dg_bills.Cell(CurrenciesSelected(_idx_row), GRID_COLUMN_BILL_CURRENCY_ISO_CODE).Value & "' " & _
                                    "AND CUD_TYPE = " & Me.dg_bills.Cell(CurrenciesSelected(_idx_row), GRID_COLUMN_BILL_CURRENCY_TYPE).Value & " " & _
                                    "AND CUD_DENOMINATION = " & Me.dg_bills.Cell(CurrenciesSelected(_idx_row), GRID_COLUMN_BILL_CURRENCY_DENOMINATION).Value & " " & _
                                    "AND EDITABLE_ROW = " & Me.dg_bills.Cell(CurrenciesSelected(_idx_row), GRID_COLUMN_BILL_CURRENCY_EDITABLE).Value & " " & _
                                    "AND EDITABLE_ROW <> " & DataRowState.Deleted & " ")

      If _cur_denomi.Length = 1 Or _cur_denomi(0).RowState = DataRowState.Added Then
        If _cur_denomi(0)(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_EDITABLE) = 0 Then
          ' Mark for delete
          _cur_denomi(0)(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_EDITABLE) = DataRowState.Deleted
        Else
          _cur_denomi(0).Delete()
        End If
      End If

      Me.dg_bills.IsSelected(CurrenciesSelected(_idx_row)) = False
      Me.dg_bills.DeleteRowFast(CurrenciesSelected(_idx_row))

      Call GUI_GetScreenData()

      Exit Sub

    Next

  End Sub            ' DeleteBillRow

  Private Sub ReadData()

    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_CREATE)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_BEFORE_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_READ)
    Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ)

    If DbStatus = ENUM_STATUS.STATUS_OK Then
      Call GUI_DB_Operation(ENUM_DB_OPERATION.DB_OPERATION_DUPLICATE)
    End If

  End Sub                 ' ReadData

  ' PURPOSE: Check bills values have a valid value. 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Boolean. True: all ok. False: otherwise.
  Private Function IsScreenDataOkBills() As Boolean

    Dim _currency_data_1 As CLASS_TITO_CURRENCY
    Dim _currency_data_2 As CLASS_TITO_CURRENCY
    Dim _idx_row_top As Integer
    Dim _idx_row As Integer

    Call GUI_GetScreenData()
    _currency_data_1 = Me.DbEditedObject
    _currency_data_2 = Me.DbEditedObject

    _idx_row_top = 0
    _idx_row = 0

    For Each _currency_1 As DataRow In _currency_data_1.DataCurrencyDenomination.Rows
      For Each _currency_2 As DataRow In _currency_data_1.DataCurrencyDenomination.Rows

        If _idx_row_top <> _idx_row _
          And _currency_1(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_ISO_CODE) = _currency_2(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_ISO_CODE) _
          And _currency_1(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_TYPE) = _currency_2(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_TYPE) _
          And _currency_1(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_DENOMINATION) = _currency_2(CLASS_TITO_CURRENCY.SQL_COLUMN_BILL_CURRENCY_DENOMINATION) Then

          Call dg_currencies.Focus()
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1068), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR)

          Return False
        End If

        _idx_row += 1
      Next

      _idx_row_top += 1
      _idx_row = 0
    Next

    Return True
  End Function ' IsScreenDataOkBills

#End Region ' Private Functions

#Region "Events"

  Private Sub dg_currencies_RowSelectedEvent(ByVal SelectedRow As System.Int32) Handles dg_currencies.RowSelectedEvent

    Dim _currency_data As CLASS_TITO_CURRENCY

    If SelectedRow >= 0 Then
      m_currency_selected = dg_currencies.Cell(SelectedRow, GRID_COLUMN_CUR_CURRENCY_ISO_CODE).Value
    End If

    Try

      Call GUI_GetScreenData()

      _currency_data = Me.DbEditedObject

      SetScreenCurrDenomin(_currency_data.DataCurrencyDenomination)

    Catch ex As Exception
      Call Common_LoggerMsg(ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                            Me.Name, _
                            "dg_currencies_RowSelectedEvent", _
                            ex.Message)

      ' 137 "Exception error has been found: \n%1"
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(137), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, , , ex.Message)

    Finally

    End Try

  End Sub      ' dg_currencies_RowSelectedEvent

  Private Sub btn_add_ClickEvent() Handles btn_add.ClickEvent
    InsertBillRow()

  End Sub                  ' btn_add_ClickEvent

  Private Sub btn_del_ClickEvent() Handles btn_del.ClickEvent
    Dim _num_row As Integer

    If Not (Me.dg_bills.SelectedRows Is Nothing) Then
      _num_row = Me.dg_bills.SelectedRows.Length
      For _idx_row_max As Integer = 0 To _num_row
        DeleteBillRow()
      Next
    End If

  End Sub                  ' btn_del_ClickEvent

  Private Sub dg_bill_BeforeStartEditionEvent(ByVal Row As System.Int32, ByVal Col As System.Int32, ByRef Cancel As System.Boolean) Handles dg_bills.BeforeStartEditionEvent

    If Col = GRID_COLUMN_BILL_CURRENCY_DENOMINATION_VALUE Then
      If dg_bills.Cell(Row, GRID_COLUMN_BILL_CURRENCY_EDITABLE).Value = 0 Then
        Cancel = True
      End If
    End If

  End Sub     ' dg_bill_BeforeStartEditionEvent

  Private Sub dg_currencies_CellDataChangedEvent(ByVal Row As System.Int32, ByVal Column As System.Int32) Handles dg_currencies.CellDataChangedEvent

    If Column = GRID_COLUMN_CUR_CURRENCY_ALIAS_1 Or Column = GRID_COLUMN_CUR_CURRENCY_ALIAS_2 Then
      Me.dg_currencies.Cell(Row, Column).Value = Me.dg_currencies.Cell(Row, Column).Value.ToUpper()
    End If

  End Sub  ' dg_currencies_CellDataChangedEvent

#End Region            ' Events

End Class   ' FORM_CURRENCIES
