﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_tito_payment_report_sel
    Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.cmb_terminal = New GUI_Controls.uc_combo_one_all()
    Me.gpb_Types = New System.Windows.Forms.GroupBox()
    Me.opt_all = New System.Windows.Forms.RadioButton()
    Me.opt_some = New System.Windows.Forms.RadioButton()
    Me.sgd_Types = New GUI_Controls.uc_grid()
    Me.cmb_usrpay = New GUI_Controls.uc_combo_one_all()
    Me.cmb_usrallow = New GUI_Controls.uc_combo_one_all()
    Me.gb_date = New System.Windows.Forms.GroupBox()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.gpb_transaction = New System.Windows.Forms.GroupBox()
    Me.chk_with_tax = New System.Windows.Forms.CheckBox()
    Me.chk_without_tax = New System.Windows.Forms.CheckBox()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gpb_Types.SuspendLayout()
    Me.gb_date.SuspendLayout()
    Me.gpb_transaction.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gpb_transaction)
    Me.panel_filter.Controls.Add(Me.gb_date)
    Me.panel_filter.Controls.Add(Me.cmb_usrallow)
    Me.panel_filter.Controls.Add(Me.cmb_usrpay)
    Me.panel_filter.Controls.Add(Me.gpb_Types)
    Me.panel_filter.Controls.Add(Me.cmb_terminal)
    Me.panel_filter.Size = New System.Drawing.Size(1295, 180)
    Me.panel_filter.Controls.SetChildIndex(Me.cmb_terminal, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gpb_Types, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.cmb_usrpay, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.cmb_usrallow, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gpb_transaction, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 184)
    Me.panel_data.Size = New System.Drawing.Size(1295, 473)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1289, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1289, 4)
    '
    'cmb_terminal
    '
    Me.cmb_terminal.AutoSize = True
    Me.cmb_terminal.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.cmb_terminal.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.cmb_terminal.Location = New System.Drawing.Point(6, 90)
    Me.cmb_terminal.Name = "cmb_terminal"
    Me.cmb_terminal.SelectOne = -1
    Me.cmb_terminal.Size = New System.Drawing.Size(306, 81)
    Me.cmb_terminal.TabIndex = 1
    '
    'gpb_Types
    '
    Me.gpb_Types.Controls.Add(Me.opt_all)
    Me.gpb_Types.Controls.Add(Me.opt_some)
    Me.gpb_Types.Controls.Add(Me.sgd_Types)
    Me.gpb_Types.Location = New System.Drawing.Point(318, 8)
    Me.gpb_Types.Name = "gpb_Types"
    Me.gpb_Types.Size = New System.Drawing.Size(407, 164)
    Me.gpb_Types.TabIndex = 2
    Me.gpb_Types.TabStop = False
    Me.gpb_Types.Text = "xMovType"
    '
    'opt_all
    '
    Me.opt_all.AutoSize = True
    Me.opt_all.Checked = True
    Me.opt_all.Location = New System.Drawing.Point(5, 43)
    Me.opt_all.Name = "opt_all"
    Me.opt_all.Size = New System.Drawing.Size(46, 17)
    Me.opt_all.TabIndex = 1
    Me.opt_all.TabStop = True
    Me.opt_all.Text = "xAll"
    Me.opt_all.UseVisualStyleBackColor = True
    '
    'opt_some
    '
    Me.opt_some.AutoSize = True
    Me.opt_some.Location = New System.Drawing.Point(6, 20)
    Me.opt_some.Name = "opt_some"
    Me.opt_some.Size = New System.Drawing.Size(65, 17)
    Me.opt_some.TabIndex = 0
    Me.opt_some.Text = "xSome"
    Me.opt_some.UseVisualStyleBackColor = True
    '
    'sgd_Types
    '
    Me.sgd_Types.CurrentCol = -1
    Me.sgd_Types.CurrentRow = -1
    Me.sgd_Types.EditableCellBackColor = System.Drawing.Color.Empty
    Me.sgd_Types.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.sgd_Types.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.sgd_Types.Location = New System.Drawing.Point(77, 20)
    Me.sgd_Types.Name = "sgd_Types"
    Me.sgd_Types.PanelRightVisible = False
    Me.sgd_Types.Redraw = True
    Me.sgd_Types.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.sgd_Types.Size = New System.Drawing.Size(323, 137)
    Me.sgd_Types.Sortable = False
    Me.sgd_Types.SortAscending = True
    Me.sgd_Types.SortByCol = 0
    Me.sgd_Types.TabIndex = 2
    Me.sgd_Types.ToolTipped = True
    Me.sgd_Types.TopRow = -2
    '
    'cmb_usrpay
    '
    Me.cmb_usrpay.AutoSize = True
    Me.cmb_usrpay.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.cmb_usrpay.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.cmb_usrpay.Location = New System.Drawing.Point(731, 8)
    Me.cmb_usrpay.Name = "cmb_usrpay"
    Me.cmb_usrpay.SelectOne = -1
    Me.cmb_usrpay.Size = New System.Drawing.Size(306, 81)
    Me.cmb_usrpay.TabIndex = 3
    '
    'cmb_usrallow
    '
    Me.cmb_usrallow.AutoSize = True
    Me.cmb_usrallow.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.cmb_usrallow.Font = New System.Drawing.Font("Verdana", 8.25!)
    Me.cmb_usrallow.Location = New System.Drawing.Point(731, 91)
    Me.cmb_usrallow.Name = "cmb_usrallow"
    Me.cmb_usrallow.SelectOne = -1
    Me.cmb_usrallow.Size = New System.Drawing.Size(306, 81)
    Me.cmb_usrallow.TabIndex = 4
    '
    'gb_date
    '
    Me.gb_date.Controls.Add(Me.dtp_to)
    Me.gb_date.Controls.Add(Me.dtp_from)
    Me.gb_date.Location = New System.Drawing.Point(6, 8)
    Me.gb_date.Name = "gb_date"
    Me.gb_date.Size = New System.Drawing.Size(306, 80)
    Me.gb_date.TabIndex = 0
    Me.gb_date.TabStop = False
    Me.gb_date.Text = "xDate"
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(6, 45)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = True
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(276, 24)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 1
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(6, 17)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = True
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(276, 24)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 0
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gpb_transaction
    '
    Me.gpb_transaction.Controls.Add(Me.chk_without_tax)
    Me.gpb_transaction.Controls.Add(Me.chk_with_tax)
    Me.gpb_transaction.Location = New System.Drawing.Point(1043, 8)
    Me.gpb_transaction.Name = "gpb_transaction"
    Me.gpb_transaction.Size = New System.Drawing.Size(153, 81)
    Me.gpb_transaction.TabIndex = 5
    Me.gpb_transaction.TabStop = False
    Me.gpb_transaction.Text = "xTransaction"
    '
    'chk_with_tax
    '
    Me.chk_with_tax.AutoSize = True
    Me.chk_with_tax.Location = New System.Drawing.Point(15, 24)
    Me.chk_with_tax.Name = "chk_with_tax"
    Me.chk_with_tax.Size = New System.Drawing.Size(58, 17)
    Me.chk_with_tax.TabIndex = 0
    Me.chk_with_tax.Text = "xWith"
    Me.chk_with_tax.UseVisualStyleBackColor = True
    '
    'chk_without_tax
    '
    Me.chk_without_tax.AutoSize = True
    Me.chk_without_tax.Location = New System.Drawing.Point(15, 52)
    Me.chk_without_tax.Name = "chk_without_tax"
    Me.chk_without_tax.Size = New System.Drawing.Size(76, 17)
    Me.chk_without_tax.TabIndex = 1
    Me.chk_without_tax.Text = "xWithout"
    Me.chk_without_tax.UseVisualStyleBackColor = True
    '
    'frm_tito_payment_report_sel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1303, 661)
    Me.Name = "frm_tito_payment_report_sel"
    Me.Text = "frm_tito_payment_report_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gpb_Types.ResumeLayout(False)
    Me.gpb_Types.PerformLayout()
    Me.gb_date.ResumeLayout(False)
    Me.gpb_transaction.ResumeLayout(False)
    Me.gpb_transaction.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents cmb_terminal As GUI_Controls.uc_combo_one_all
  Friend WithEvents gpb_Types As System.Windows.Forms.GroupBox
  Friend WithEvents sgd_Types As GUI_Controls.uc_grid
  Friend WithEvents cmb_usrallow As GUI_Controls.uc_combo_one_all
  Friend WithEvents cmb_usrpay As GUI_Controls.uc_combo_one_all
  Friend WithEvents opt_all As System.Windows.Forms.RadioButton
  Friend WithEvents opt_some As System.Windows.Forms.RadioButton
  Friend WithEvents gb_date As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents gpb_transaction As System.Windows.Forms.GroupBox
  Friend WithEvents chk_without_tax As System.Windows.Forms.CheckBox
  Friend WithEvents chk_with_tax As System.Windows.Forms.CheckBox
End Class
