'--------------------------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'--------------------------------------------------------------------------------------
'
'   MODULE NAME :  frm_cash_tito_sel.vb
'
'   DESCRIPTION :  Total Cash in site (TITO)
'
'        AUTHOR :  Nelson Madrigal Reyes
'
' CREATION DATE :  02-JUL-2013
'
' REVISION HISTORY
'
' Date         Author Description
' -----------  ------ ------------------------------------------------------------------
' 02-JUL-2013  NMR    Initial version
' 23-AUG-2013  JRM    Code revision and refactoring
' 06-SEP-2013  NMR    Fixed error: JIRA-WIGOSTITO-90
' 16-SEP-2013  NMR    Fixed error: JIRA-WIGOSTITO-140
' 07-NOV-2013  DRV    Fixed. JIRA-WIGOSTITO-555
' 02-DEC-2013  ICS & JPJ  Adding details and group by options.
' 09-DEC-2013  JPJ    Adding secondary group by options.
' 10-DEC-2013  JPJ    Adding group by provider and date, grid counters and range of dates in group by.
' 12-DEC-2013  ACM    Fixed report filter.
' 18-DEC-2013  ACM    Fixed Bug #WIGOSTITO-864. Set only Sas-Host terminals to terminals filter control.
' 23-DEC-2013  JPJ    Changed query so it doesn't show pending to collect only what is in the machine.
' 07-JAN-2013  ICS    Changed query to use money collection datetime instead of tickets creation datetime for Ticket IN
' 17-JAN-2014  DHA    Fixed Bug #WIGOSTITO-993. Excel/report format corrected for date columns
' 25-FEB-2014  DRV    Fixed Bug #WIGOSTITO-1087. Counting tickets canceled wigos GUI
' 17-MAR-2014  DRV    Added new money collection columns and simplified query
' 26-MAR-2014  ICS    Fixed Bug #WIGOSTITO-1168. The filter 'Some' requires selecting at least one
' 27-MAR-2014  DLL    Fixed Bug #WIGOSTITO-1173. The filter status and type when all selected shows ALL
' 14-MAY-2014  JFC    Fixed Bug #WIG-920. Some columns are wrong aligned
' 28-NOV-2014  SGB    Fixed Bug #WIG-1770 Error us filter 'show actual state' & error in total partial results
' 02-JUL-2015  YNM    Fixed Bug #WIG-2520: Totals calculation is incorrect
' 15-SEP-2015  DCS    Backlog Item 3707 - Coin Collection
' 15-DEC-2015  JPJ    Fixed Bug 7181: Totals don't add up what they should
' 06-MAR-2017  RAB    Bug 23528: Export and print of the form flow of terminals in cash not shows all the terminals.
'---------------------------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports System.Text
Imports System.Data.SqlClient
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_Controls
Imports WSI.Common

Public Class frm_tito_cash_sel
  Inherits GUI_Controls.frm_base_sel

#Region " Enum "

  ' Indicates the current selected group by option
  Private Enum GROUP_OPTION
    GROUP_OPTION_TERMINAL = 0
    GROUP_OPTION_PROV = 1
    GROUP_OPTION_DAY = 2
    GROUP_OPTION_DAY_TERMINAL = 3
    GROUP_OPTION_TERMINAL_DAY = 4
    GROUP_OPTION_PROV_DAY = 5
  End Enum

#End Region ' Enum

#Region " Constants "

  ' Grid configuration
  Private Const GRID_STATIC_COLUMNS_COUNT As Integer = 10
  Private Const GRID_HEADER_ROWS As Integer = 2

  ' Columns Index
  Private Const START_COLUMN_MONEY As Integer = 2
  Private Const GRID_COLUMN_START_PRINT As Integer = 1
  Private Const GRID_COLUMN_SELECT As Integer = 0
  Private GRID_COLUMN_DATE_SINCE As Integer = 1
  Private GRID_COLUMN_DATE_UNTIL As Integer = 2
  Private GRID_COLUMN_TERMINAL_PROV As Integer = 3
  Private GRID_COLUMN_TERMINAL_NAME As Integer = 4
  Private Const GRID_COLUMN_TERM_REDIM As Integer = 5
  Private Const GRID_COLUMN_TERM_NOREDIM As Integer = 6
  Private Const GRID_COLUMN_BILLS_TOTAL As Integer = 7
  Private Const GRID_COLUMN_BILLS_COUNT As Integer = 8
  Private Const GRID_COLUMN_FIRST_BILL As Integer = 9
  Private GRID_COLUMN_COINS_AMOUNT As Integer = 10
  Private GRID_COLUMN_CASH_AMOUNT As Integer = 11

  ' SQL Columns
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 0
  Private SQL_COLUMN_TERMINAL_PROV As Integer = 1
  Private SQL_COLUMN_TERMINAL_NAME As Integer = 2
  Private SQL_COLUMN_DATE_SINCE As Integer = 3
  Private SQL_COLUMN_DATE_UNTIL As Integer = 4
  Private Const SQL_COLUMN_TERM_REDIM As Integer = 5
  Private Const SQL_COLUMN_TERM_NOREDIM As Integer = 6
  Private Const SQL_COLUMN_BILLS_TOTAL As Integer = 7
  Private Const SQL_COLUMN_BILLS_COUNT As Integer = 8
  Private Const SQL_COLUMN_BILL_FACE_VALUE As Integer = 9
  Private Const SQL_COLUMN_BILL_COUNT As Integer = 10
  Private Const SQL_COLUMN_CURRENCY_TYPE As Integer = 11
  Private Const SQL_COLUMN_COINS_TOTAL As Integer = 12

  ' Columns Width
  Private Const GRID_COLUMN_WIDTH_SELECT As Integer = 200
  Private Const GRID_COLUMN_WIDTH_TERMINAL As Integer = 2200
  Private Const GRID_COLUMN_WIDTH_PROVIDER As Integer = 2200
  Private Const GRID_COLUMN_WIDTH_TERM_REDIM As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_TERM_NOREDIM As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_BILLS_TOTAL As Integer = 1500
  Private Const GRID_COLUMN_WIDTH_BILLS_COUNT As Integer = 1420
  Private Const GRID_COLUMN_WIDTH_DATE As Integer = 1700
  Private Const GRID_COLUMN_WIDTH_COINS_TOTAL As Integer = 2000

  Private Const GRID_COUNTER_SUBTOTALS As Integer = 1 ' Grid counters

  Private Const SQL_FACE_VALUE_INDEX As Int32 = 0

#End Region ' Constants

#Region " Structure " ' Structure

  ''' <summary>
  ''' Structure and public methods
  ''' </summary>
  ''' <remarks></remarks>
  Private Structure CashFlowByTerminal
    Public terminal_id As Integer
    Public terminal_provider As String
    Public terminal_name As String
    Public date_since As DateTime
    Public date_until As DateTime
    Public ticket_in_re As Decimal
    Public ticket_in_nre As Decimal
    Public collected_bills_total As Integer
    Public collected_bills_quantity As Integer
    Public collected_bills As Dictionary(Of Integer, Integer)
    Public collected_coins_total As Decimal
    Public collected_cash_total As Decimal
    Public first_row_terminal As Boolean

    ''' <summary>
    ''' Init all propieties from cashflowbyterminal object
    ''' </summary>
    ''' <param name="BillTypes">Definition of facevalue bills</param>
    ''' <remarks></remarks>
    Public Sub Init(ByVal BillTypes As DataTable)
      terminal_id = 0
      terminal_provider = String.Empty
      terminal_name = String.Empty
      date_since = Nothing
      date_until = Nothing
      ticket_in_re = 0D
      ticket_in_nre = 0D
      collected_bills_total = 0
      collected_bills_quantity = 0

      collected_bills = New Dictionary(Of Integer, Integer)
      For Each _bill_type As DataRow In BillTypes.Rows
        collected_bills.Add(_bill_type("FACEVALUE"), 0)
      Next

      collected_coins_total = 0D
      collected_cash_total = 0D
      first_row_terminal = True
    End Sub

    ''' <summary>
    ''' Only set a specific propieties, the others propieties init values
    ''' </summary>
    ''' <param name="TerminalProvider">Terminal name provider value</param>
    ''' <param name="TerminalName">Terminal name value</param>
    ''' <param name="DateSince">Date since value</param>
    ''' <param name="TerminalId">Terminal ID value</param>
    ''' <param name="BillTypes">All facevalue bills types values</param>
    ''' <remarks></remarks>
    Public Sub Init(ByVal TerminalProvider As String, ByVal TerminalName As String, ByVal DateSince As DateTime, ByVal TerminalId As Integer, ByVal BillTypes As DataTable)
      Init(BillTypes)
      terminal_provider = TerminalProvider
      terminal_name = TerminalName
      date_since = DateSince
      terminal_id = TerminalId
      first_row_terminal = False
    End Sub

    ''' <summary>
    ''' Accumulate from object totals values
    ''' </summary>
    ''' <param name="CashFlowByTerminalToSum">Values to sum to object</param>
    ''' <param name="WithoutBillsDenominations">Determines if row is total or subtotal</param>
    ''' <remarks></remarks>
    Public Sub AccumulateToTotals(ByVal CashFlowByTerminalToSum As CashFlowByTerminal, ByVal WithBillsDenominations As Boolean)
      ticket_in_re += CashFlowByTerminalToSum.ticket_in_re
      ticket_in_nre += CashFlowByTerminalToSum.ticket_in_nre
      collected_bills_quantity += CashFlowByTerminalToSum.collected_bills_quantity
      collected_bills_total += CashFlowByTerminalToSum.collected_bills_total

      If (WithBillsDenominations) Then
        For Each _collected_bill As KeyValuePair(Of Integer, Integer) In CashFlowByTerminalToSum.collected_bills
          collected_bills(_collected_bill.Key) += (_collected_bill.Key * _collected_bill.Value)
        Next
      End If

      collected_coins_total += CashFlowByTerminalToSum.collected_coins_total
      collected_cash_total += CashFlowByTerminalToSum.collected_cash_total
    End Sub

  End Structure

#End Region

#Region " Members"

  Private m_filter_params As New Hashtable()        ' Hashtable containing the filter elements.
  Private m_grid_total_columns As Integer           ' Number of grid columns.
  Private m_bill_types As DataTable                 ' Contains the different bill types.
  Private m_bill_index As Hashtable
  Private m_group_by_column As GROUP_OPTION         ' Indicates the column to group by.

  Private m_cash_flow As New CashFlowByTerminal            ' Accumulate all values from specific filter
  Private m_cash_flow_subtotals As New CashFlowByTerminal  ' Accumulate subtotal values
  Private m_cash_flow_totals As New CashFlowByTerminal     ' Accumulate total values

  Private m_subtotal_rows As Integer                       ' Determine total subtotal rows
  Private m_subtotal_print As Boolean                      ' Determine if actual row is due print

#End Region ' Members 

#Region " Private Functions and Methods "

  ' PURPOSE : Define layout of all Main Grid Columns
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  Private Sub GUI_StyleSheet()
    Dim _idx As Integer
    Dim _tickets_in_machine As String

    m_grid_total_columns = GRID_STATIC_COLUMNS_COUNT + m_bill_types.Rows.Count + 1

    m_bill_index = New Hashtable()

    With Me.Grid
      Call .Init(m_grid_total_columns, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = False

      'Dinamic grid columns are set here
      If opt_terminal.Checked OrElse Me.opt_terminal_day.Checked Then
        GRID_COLUMN_TERMINAL_NAME = 2
        GRID_COLUMN_TERMINAL_PROV = 1
        GRID_COLUMN_DATE_SINCE = 3
        GRID_COLUMN_DATE_UNTIL = 4
        SQL_COLUMN_TERMINAL_NAME = 2
        SQL_COLUMN_TERMINAL_PROV = 1
        SQL_COLUMN_DATE_SINCE = 3
        SQL_COLUMN_DATE_UNTIL = 4
      End If
      If opt_provider.Checked OrElse opt_provider_day.Checked Then
        GRID_COLUMN_DATE_SINCE = 3
        GRID_COLUMN_DATE_UNTIL = 4
        GRID_COLUMN_TERMINAL_PROV = 1
        GRID_COLUMN_TERMINAL_NAME = 2
        SQL_COLUMN_DATE_SINCE = 3
        SQL_COLUMN_DATE_UNTIL = 4
        SQL_COLUMN_TERMINAL_PROV = 1
        SQL_COLUMN_TERMINAL_NAME = 2
      End If
      If opt_day.Checked OrElse Me.opt_day_terminal.Checked Then
        GRID_COLUMN_DATE_SINCE = 1
        GRID_COLUMN_DATE_UNTIL = 2
        GRID_COLUMN_TERMINAL_PROV = 3
        GRID_COLUMN_TERMINAL_NAME = 4
        SQL_COLUMN_DATE_SINCE = 1
        SQL_COLUMN_DATE_UNTIL = 2
        SQL_COLUMN_TERMINAL_PROV = 3
        SQL_COLUMN_TERMINAL_NAME = 4
      End If

      ' INDEX
      .Column(GRID_COLUMN_SELECT).Header(0).Text = ""
      .Column(GRID_COLUMN_SELECT).Header(1).Text = ""
      .Column(GRID_COLUMN_SELECT).Width = GRID_COLUMN_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False
      .Column(GRID_COLUMN_SELECT).HighLightWhenSelected = False

      ' Date from
      .Column(GRID_COLUMN_DATE_SINCE).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_COLUMN_DATE_SINCE).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(202)
      .Column(GRID_COLUMN_DATE_SINCE).Width = GRID_COLUMN_WIDTH_DATE
      .Column(GRID_COLUMN_DATE_SINCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Date to
      .Column(GRID_COLUMN_DATE_UNTIL).Header(0).Text = GLB_NLS_GUI_INVOICING.GetString(201)
      .Column(GRID_COLUMN_DATE_UNTIL).Header(1).Text = GLB_NLS_GUI_INVOICING.GetString(203)
      .Column(GRID_COLUMN_DATE_UNTIL).Width = GRID_COLUMN_WIDTH_DATE
      .Column(GRID_COLUMN_DATE_UNTIL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Terminal provider
      .Column(GRID_COLUMN_TERMINAL_PROV).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(492)
      .Column(GRID_COLUMN_TERMINAL_PROV).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3317)
      .Column(GRID_COLUMN_TERMINAL_PROV).Width = GRID_COLUMN_WIDTH_TERMINAL
      .Column(GRID_COLUMN_TERMINAL_PROV).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Terminal name
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(492)
      .Column(GRID_COLUMN_TERMINAL_NAME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(253)
      .Column(GRID_COLUMN_TERMINAL_NAME).Width = GRID_COLUMN_WIDTH_TERMINAL
      .Column(GRID_COLUMN_TERMINAL_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' Ticket In Redeemable
      .Column(GRID_COLUMN_TERM_REDIM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2310)
      .Column(GRID_COLUMN_TERM_REDIM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125)
      .Column(GRID_COLUMN_TERM_REDIM).Width = GRID_COLUMN_WIDTH_TERM_REDIM
      .Column(GRID_COLUMN_TERM_REDIM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Ticket In Non-redeemable
      .Column(GRID_COLUMN_TERM_NOREDIM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2310)
      .Column(GRID_COLUMN_TERM_NOREDIM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2126)
      .Column(GRID_COLUMN_TERM_NOREDIM).Width = GRID_COLUMN_WIDTH_TERM_NOREDIM
      .Column(GRID_COLUMN_TERM_NOREDIM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      If chk_actual_state.Checked Then
        ' Bills in Terminal
        _tickets_in_machine = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2208)
      Else
        ' Collected bills
        _tickets_in_machine = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4468)
      End If

      ' Total
      .Column(GRID_COLUMN_BILLS_TOTAL).Header(0).Text = _tickets_in_machine
      .Column(GRID_COLUMN_BILLS_TOTAL).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1573)
      .Column(GRID_COLUMN_BILLS_TOTAL).Width = GRID_COLUMN_WIDTH_BILLS_TOTAL
      .Column(GRID_COLUMN_BILLS_TOTAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Quantity
      .Column(GRID_COLUMN_BILLS_COUNT).Header(0).Text = _tickets_in_machine
      .Column(GRID_COLUMN_BILLS_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2265)
      .Column(GRID_COLUMN_BILLS_COUNT).Width = GRID_COLUMN_WIDTH_BILLS_COUNT
      .Column(GRID_COLUMN_BILLS_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Bill type
      For _idx = 0 To m_bill_types.Rows.Count - 1
        .Column(GRID_COLUMN_FIRST_BILL + _idx).Header(0).Text = _tickets_in_machine
        .Column(GRID_COLUMN_FIRST_BILL + _idx).Header(1).Text = GUI_FormatCurrency(m_bill_types.Rows(_idx).Item(SQL_FACE_VALUE_INDEX), 0)
        .Column(GRID_COLUMN_FIRST_BILL + _idx).Width = GRID_COLUMN_WIDTH_BILLS_COUNT
        .Column(GRID_COLUMN_FIRST_BILL + _idx).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

        m_bill_index.Add(GUI_FormatCurrency(m_bill_types.Rows(_idx).Item(SQL_FACE_VALUE_INDEX)), GRID_COLUMN_FIRST_BILL + _idx)
      Next



      ' Total Coins
      If chk_actual_state.Checked Then
        ' Coins in Terminal
        .Column(GRID_COLUMN_COINS_AMOUNT + _idx - 1).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6690)
      Else
        ' Collected Coins
        .Column(GRID_COLUMN_COINS_AMOUNT + _idx - 1).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5695)
      End If
      .Column(GRID_COLUMN_COINS_AMOUNT + _idx - 1).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1573)
      .Column(GRID_COLUMN_COINS_AMOUNT + _idx - 1).Width = GRID_COLUMN_WIDTH_COINS_TOTAL
      .Column(GRID_COLUMN_COINS_AMOUNT + _idx - 1).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Total Cash
      If chk_actual_state.Checked Then
        ' Cash in Terminal
        .Column(GRID_COLUMN_COINS_AMOUNT + _idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6691)
      Else
        ' Collected Cash
        .Column(GRID_COLUMN_COINS_AMOUNT + _idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5716)
      End If
      .Column(GRID_COLUMN_COINS_AMOUNT + _idx).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1573) & " "
      .Column(GRID_COLUMN_COINS_AMOUNT + _idx).Width = GRID_COLUMN_WIDTH_COINS_TOTAL
      .Column(GRID_COLUMN_COINS_AMOUNT + _idx).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      Select Case m_group_by_column
        Case GROUP_OPTION.GROUP_OPTION_PROV
          .Column(GRID_COLUMN_TERMINAL_NAME).Width = 0
        Case GROUP_OPTION.GROUP_OPTION_DAY
          .Column(GRID_COLUMN_TERMINAL_NAME).Width = 0
          .Column(GRID_COLUMN_TERMINAL_PROV).Width = 0
      End Select

      If chk_actual_state.Checked Then
        .Column(GRID_COLUMN_DATE_SINCE).Width = 0
        .Column(GRID_COLUMN_DATE_UNTIL).Width = 0
      End If

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT :
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT :
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base.ENUM_BUTTON)

    If ButtonId = ENUM_BUTTON.BUTTON_FILTER_APPLY Then
      If opt_terminal.Checked Then m_group_by_column = GROUP_OPTION.GROUP_OPTION_TERMINAL
      If opt_provider.Checked Then m_group_by_column = GROUP_OPTION.GROUP_OPTION_PROV
      If opt_provider_day.Checked Then m_group_by_column = GROUP_OPTION.GROUP_OPTION_PROV_DAY
      If opt_day.Checked Then m_group_by_column = GROUP_OPTION.GROUP_OPTION_DAY
      If opt_day_terminal.Checked Then m_group_by_column = GROUP_OPTION.GROUP_OPTION_DAY_TERMINAL
      If opt_terminal_day.Checked Then m_group_by_column = GROUP_OPTION.GROUP_OPTION_TERMINAL_DAY

      Call GUI_StyleSheet()
    End If

    Call MyBase.GUI_ButtonClick(ButtonId)

  End Sub ' GUI_ButtonClick

  ' PURPOSE : Return a condition to filter by ColumnName param
  '
  '  PARAMS :
  '     - INPUT :
  '           - ColumnName: name of BD column to filter by
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - Composed string for filter
  '
  Private Function GetSelectedTerminals(ByVal ColumnName As String) As String
    Dim _result As String
    Dim _terminals() As Long
    Dim _select_all_terminals As Boolean

    _select_all_terminals = uc_pr_list.AllSelectedChecked
    _result = " te_terminal_id = te_terminal_id"

    If _select_all_terminals = False Then
      _terminals = uc_pr_list.GetTerminalIdListSelected()

      If uc_pr_list.TerminalListHasValues Then
        _result = String.Empty
        For Each _idx As Long In _terminals
          _result &= IIf(_result.Length > 0, ",", "")
          _result &= _idx.ToString()
        Next
        If Not String.IsNullOrEmpty(_result) Then
          _result = ColumnName & " IN (" & _result & ")"
        End If
      End If
    End If

    Return _result

  End Function ' GetSelectedTerminals

  ' PURPOSE: Add new filter item form export/print purposes
  '
  '  PARAMS:
  '     - INPUT :
  '       - Label: string with label to print
  '       - Value: text value to print
  '
  Private Sub AddFilterItem(ByVal Label As String, ByVal Value As String)

    m_filter_params.Add(m_filter_params.Count, Label & ";" & Value)

  End Sub ' AddFilterItem

  ' PURPOSE: Gets all Bill types from the current currency
  '
  '  PARAMS:
  '     - INPUT:
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub GetBillTypes()
    Dim _sb As StringBuilder

    _sb = New StringBuilder()
    _sb.AppendLine("  SELECT   DISTINCT  X.FACEVALUE                                       ")
    _sb.AppendLine("    FROM   (                                                           ")
    _sb.AppendLine("           SELECT   CTM_DENOMINATION AS FACEVALUE                      ")
    _sb.AppendLine("             FROM   CASHIER_TERMINAL_MONEY                             ")
    _sb.AppendLine("            UNION                                                      ")
    _sb.AppendLine("           SELECT   MCD_FACE_VALUE AS FACEVALUE                        ")
    _sb.AppendLine("             FROM   MONEY_COLLECTION_DETAILS  AS FACEVALUE             ")
    _sb.AppendLine("            WHERE   MCD_FACE_VALUE > 0                                 ")
    _sb.AppendLine("                    AND MCD_NUM_COLLECTED > 0                          ")
    _sb.AppendLine("                    AND MCD_CAGE_CURRENCY_TYPE = 0 ) AS X              ")
    _sb.AppendLine("ORDER BY   X.FACEVALUE                                                 ")

    m_bill_types = GUI_GetTableUsingSQL(_sb.ToString, 100)

  End Sub ' GetBillTypes

  ''' <summary>
  ''' Print total and subtotal rows
  ''' </summary>
  ''' <param name="IsTotalRow">Determine if row is total or subtotal</param>
  ''' <param name="CashFlowTotal">Values from print</param>
  ''' <remarks></remarks>
  Private Sub PrintTotalsRow(ByVal IsTotalRow As Boolean, ByVal CashFlowTotal As CashFlowByTerminal)
    Dim _idx_bill_denomination_col As Integer
    Dim _actual_row As Integer
    _idx_bill_denomination_col = 0
    _actual_row = Me.Grid.NumRows

    Me.Grid.AddRow()

    'Print values of total row
    Me.Grid.Cell(_actual_row, GRID_COLUMN_TERM_REDIM).Value = GUI_FormatCurrency(CashFlowTotal.ticket_in_re, 2)
    Me.Grid.Cell(_actual_row, GRID_COLUMN_TERM_NOREDIM).Value = GUI_FormatCurrency(CashFlowTotal.ticket_in_nre, 2)
    Me.Grid.Cell(_actual_row, GRID_COLUMN_BILLS_TOTAL).Value = GUI_FormatCurrency(CashFlowTotal.collected_bills_total, 2)
    Me.Grid.Cell(_actual_row, GRID_COLUMN_BILLS_COUNT).Value = GUI_FormatNumber(CashFlowTotal.collected_bills_quantity, 0)

    For Each _collected_bill_value As Integer In CashFlowTotal.collected_bills.Values
      Me.Grid.Cell(_actual_row, GRID_COLUMN_FIRST_BILL + _idx_bill_denomination_col).Value = _collected_bill_value
      _idx_bill_denomination_col += 1
    Next

    Me.Grid.Cell(_actual_row, GRID_COLUMN_COINS_AMOUNT + (m_bill_types.Rows.Count - 1)).Value = GUI_FormatCurrency(CashFlowTotal.collected_coins_total, 2)
    Me.Grid.Cell(_actual_row, GRID_COLUMN_CASH_AMOUNT + (m_bill_types.Rows.Count - 1)).Value = GUI_FormatCurrency(CashFlowTotal.collected_cash_total, 2)

    'Print color row
    For _idx_bill_denomination_col = GRID_COLUMN_SELECT To m_grid_total_columns - 1
      Me.Grid.Cell(_actual_row, _idx_bill_denomination_col).BackColor = IIf(IsTotalRow, GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00), GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01))
      If (_idx_bill_denomination_col >= GRID_COLUMN_FIRST_BILL AndAlso _idx_bill_denomination_col < GRID_COLUMN_FIRST_BILL + m_bill_types.Rows.Count) Then
        If (IsTotalRow) Then
          Me.Grid.Cell(_actual_row, _idx_bill_denomination_col).Value = GUI_FormatNumber(Me.Grid.Cell(_actual_row, _idx_bill_denomination_col).Value, 0)
          If (Me.Grid.Cell(_actual_row, _idx_bill_denomination_col).Value = 0) Then
            Me.Grid.Column(_idx_bill_denomination_col).Width = 0
          End If
        Else
          Me.Grid.Column(_idx_bill_denomination_col).Width = 0
        End If
      End If
    Next

    'Print label 'Subtotal' or 'Total'
    If (opt_terminal.Checked OrElse opt_terminal_day.Checked) Then
      Me.Grid.Cell(_actual_row, GRID_COLUMN_TERMINAL_NAME).Value = IIf(IsTotalRow, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1047), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046))
    ElseIf (opt_provider.Checked OrElse opt_provider_day.Checked) Then
      Me.Grid.Cell(_actual_row, GRID_COLUMN_TERMINAL_PROV).Value = IIf(IsTotalRow, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1047), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046))
    ElseIf (opt_day.Checked OrElse opt_day_terminal.Checked) Then
      Me.Grid.Cell(_actual_row, GRID_COLUMN_DATE_SINCE).Value = IIf(IsTotalRow, GLB_NLS_GUI_PLAYER_TRACKING.GetString(1047), GLB_NLS_GUI_PLAYER_TRACKING.GetString(1046))
    End If
  End Sub

  ''' <summary>Sum current DbRow values to object member</summary>
  ''' <param name="DbRow">Current DbRow</param>
  ''' <remarks></remarks>
  Private Sub AccumulateRow(ByVal DbRow As CLASS_DB_ROW)
    'Always accumulated the last date until.
    m_cash_flow.date_until = DbRow.Value(SQL_COLUMN_DATE_UNTIL)

    m_cash_flow.ticket_in_re += DbRow.Value(SQL_COLUMN_TERM_REDIM)
    m_cash_flow.ticket_in_nre += DbRow.Value(SQL_COLUMN_TERM_NOREDIM)

    If (DbRow.Value(SQL_COLUMN_CURRENCY_TYPE) = CageCurrencyType.Bill) Then
      For Each _bill_key As Integer In m_cash_flow.collected_bills.Keys
        If (DbRow.Value(SQL_COLUMN_BILL_FACE_VALUE) = _bill_key) Then
          m_cash_flow.collected_bills(_bill_key) += DbRow.Value(SQL_COLUMN_BILL_COUNT)
          Exit For
        End If
      Next

      m_cash_flow.collected_bills_quantity += DbRow.Value(SQL_COLUMN_BILL_COUNT)
      m_cash_flow.collected_bills_total += DbRow.Value(SQL_COLUMN_BILL_FACE_VALUE) * DbRow.Value(SQL_COLUMN_BILL_COUNT)
    ElseIf (DbRow.Value(SQL_COLUMN_CURRENCY_TYPE) = CageCurrencyType.Coin) Then
      m_cash_flow.collected_coins_total += DbRow.Value(SQL_COLUMN_BILL_FACE_VALUE) * DbRow.Value(SQL_COLUMN_BILL_COUNT)
    End If

    m_cash_flow.collected_cash_total = m_cash_flow.collected_bills_total + m_cash_flow.collected_coins_total
    m_cash_flow.terminal_id = DbRow.Value(SQL_COLUMN_TERMINAL_ID)

  End Sub

#End Region ' Private Functions and Methods

#Region " OVERRIDES "

  ''' <summary>
  ''' Sets the values of a row
  ''' </summary>
  ''' <param name="RowIndex">Current row index</param>
  ''' <param name="DbRow">Current DbRow</param>
  ''' <returns>If he paints the row</returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As CLASS_DB_ROW) As Boolean
    Dim _idx_bill_denomination_col As Integer
    _idx_bill_denomination_col = 0

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_PROV).Value = m_cash_flow.terminal_provider
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = m_cash_flow.terminal_name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_SINCE).Value = GUI_FormatDate(m_cash_flow.date_since, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE_UNTIL).Value = GUI_FormatDate(m_cash_flow.date_until, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)

    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERM_REDIM).Value = GUI_FormatCurrency(m_cash_flow.ticket_in_re, 2)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERM_NOREDIM).Value = GUI_FormatCurrency(m_cash_flow.ticket_in_nre, 2)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BILLS_TOTAL).Value = GUI_FormatCurrency(m_cash_flow.collected_bills_total, 2)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_BILLS_COUNT).Value = GUI_FormatNumber(m_cash_flow.collected_bills_quantity, 0)

    For Each _collected_bill_value As Integer In m_cash_flow.collected_bills.Values
      Me.Grid.Cell(RowIndex, GRID_COLUMN_FIRST_BILL + _idx_bill_denomination_col).Value = _collected_bill_value
      _idx_bill_denomination_col += 1
    Next

    Me.Grid.Cell(RowIndex, GRID_COLUMN_COINS_AMOUNT + (m_bill_types.Rows.Count - 1)).Value = GUI_FormatCurrency(m_cash_flow.collected_coins_total, 2)
    Me.Grid.Cell(RowIndex, GRID_COLUMN_CASH_AMOUNT + (m_bill_types.Rows.Count - 1)).Value = GUI_FormatCurrency(m_cash_flow.collected_cash_total, 2)

    If (m_subtotal_print) Then
      PrintTotalsRow(False, m_cash_flow_subtotals)
      m_subtotal_print = False
      m_subtotal_rows += 1
      m_cash_flow_subtotals.Init(m_bill_types)
    End If

    If Not (DbRow Is Nothing) Then
      m_cash_flow.Init(DbRow.Value(SQL_COLUMN_TERMINAL_PROV), DbRow.Value(SQL_COLUMN_TERMINAL_NAME), DbRow.Value(SQL_COLUMN_DATE_SINCE), DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_bill_types)
      AccumulateRow(DbRow)
    End If

    Return True
  End Function ' GUI_SetupRow

  ' PURPOSE: GUI form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_CASH_TITO_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize base_print form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3323)
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION

    chk_actual_state.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3363)
    chk_show_without_activity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4738)

    ' Group by group box
    gb_group_by.Text = GLB_NLS_GUI_INVOICING.GetString(369)
    lbl_group_by.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2420)
    opt_provider.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1922)
    opt_provider_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3364)
    opt_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2992)
    opt_terminal_day.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3361)
    opt_day.Text = GLB_NLS_GUI_INVOICING.GetString(401)
    opt_day_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3362)

    opt_provider.Enabled = True
    opt_provider_day.Enabled = True
    opt_terminal.Enabled = True
    opt_terminal_day.Enabled = True
    opt_day.Enabled = True
    opt_day_terminal.Enabled = True

    ' Hide/Show some controls
    GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False
    GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = False

    Me.Grid.Counter(GRID_COUNTER_SUBTOTALS).Visible = True
    Me.Grid.Counter(GRID_COUNTER_SUBTOTALS).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_01)

    Call GetBillTypes()
    ' Define layout of all Main Grid Columns
    Call GUI_StyleSheet()

    Dim _terminal_types() As Integer = Nothing
    ReDim Preserve _terminal_types(0 To 0)
    _terminal_types(0) = TerminalTypes.SAS_HOST

    Call uc_pr_list.Init(_terminal_types)

    ' Set filter default values
    Call GUI_FilterReset()

    ' Date time and other filters
    uc_dates.ClosingTimeEnabled = False
    uc_dates.Init(GLB_NLS_GUI_PLAYER_TRACKING.Id(268))

  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()
    Dim _closing_time As Integer
    Dim _initial_time As Date

    _closing_time = GetDefaultClosingTime()
    _initial_time = New DateTime(Now.Year, Now.Month, Now.Day, _closing_time, 0, 0)

    If _initial_time > WGDB.Now Then
      _initial_time = _initial_time.AddDays(-1)
    End If

    uc_dates.ToDate = _initial_time.AddDays(1)
    uc_dates.ToDateSelected = True
    uc_dates.FromDate = _initial_time
    uc_dates.FromDateSelected = True
    uc_dates.ClosingTime = _closing_time

    opt_terminal.Checked = True
    opt_day.Enabled = Not chk_actual_state.Checked
    chk_actual_state.Checked = True

    chk_actual_state.Checked = False

    Call uc_pr_list.SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean
    Dim _result As Boolean

    _result = True

    ' se controla el rango de fechas
    If uc_dates.FromDateSelected And uc_dates.ToDateSelected Then
      If uc_dates.FromDate > uc_dates.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        _result = False
      End If
    End If

    Return _result

  End Function 'GUI_FilterCheck

  ' PURPOSE : Build an SQL query from conditions set in the filters
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - SQL query text ready to send to the database
  '
  '   NOTES :
  '
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    ' JBP 29-NOV-2013: Refactoring.
    Dim _sb As StringBuilder
    Dim _type_redeem As String
    Dim _type_no_redeem As String
    Dim _type_handpay As String
    Dim _type_jackpot As String
    Dim _type_cashable As String
    Dim _init_hour As Integer
    Dim _str_date_time As String
    Dim _str_since As String
    Dim _str_until As String

    _sb = New StringBuilder()

    _type_cashable = CShort(TITO_TICKET_TYPE.CASHABLE).ToString()
    _type_redeem = CShort(TITO_TICKET_TYPE.PROMO_REDEEM).ToString()
    _type_no_redeem = CShort(TITO_TICKET_TYPE.PROMO_NONREDEEM).ToString()
    _type_handpay = CShort(TITO_TICKET_TYPE.HANDPAY).ToString()
    _type_jackpot = CShort(TITO_TICKET_TYPE.JACKPOT).ToString()

    _init_hour = WSI.Common.Misc.TodayOpening().Hour
    _str_date_time = "01-01-2007 " + _init_hour.ToString("00") + ":00:00"
    _str_since = "DATEADD(DAY, DATEDIFF(HOUR,'" & _str_date_time & "',T.DateDay)/24, '" + _str_date_time + "')"
    _str_until = "DATEADD(HOUR, 24, " & _str_since & ")"

    '-- TICKET IN
    _sb.AppendLine("SELECT   T.TerminalId				      AS TerminalId")
    ' TODO: ....
    If Me.opt_terminal.Checked OrElse Me.opt_terminal_day.Checked Then
      _sb.AppendLine("  	   , T.Provider				        AS Provider")
      _sb.AppendLine("  	   , T.Name						        AS Name")
      _sb.AppendLine("       , " & _str_since & "       AS Since")
      _sb.AppendLine("  	   , " & _str_until & "       AS Until")

    End If
    If Me.opt_provider.Checked OrElse opt_provider_day.Checked Then
      _sb.AppendLine("  	   , T.Provider				        AS Provider")
      _sb.AppendLine("  	   , T.Name						        AS Name")
      _sb.AppendLine("       , " & _str_since & "       AS Since")
      _sb.AppendLine("  	   , " & _str_until & "       AS Until")

    End If
    If Me.opt_day.Checked OrElse Me.opt_day_terminal.Checked Then
      _sb.AppendLine("       , " & _str_since & "       AS Since")
      _sb.AppendLine("  	   , " & _str_until & "       AS Until")
      _sb.AppendLine("  	   , T.Provider				        AS Provider")
      _sb.AppendLine("  	   , T.Name						        AS Name")

    End If

    _sb.AppendLine("  	   , (T.AmountRedeem)		      AS AmountRedeem")
    _sb.AppendLine("  	   , (T.AmountNoredeem)		    AS AmountNoredeem")
    _sb.AppendLine("	     , (T.AmountBn)				      AS AmountBn")
    _sb.AppendLine("  	   , (T.CountBn)				      AS CountBn")
    _sb.AppendLine("  	   , (T.FaceValue)            AS FaceValue")
    _sb.AppendLine("  	   , (T.BillCount)            AS BillCount")
    _sb.AppendLine("  	   , (T.CurrencyType)         AS CurrencyType")
    _sb.AppendLine("  	   , (T.AmountCoin)           AS AmountCoin")

    _sb.AppendLine("  FROM (")
    _sb.AppendLine("  		  SELECT    TE.TE_PROVIDER_ID AS Provider")
    _sb.AppendLine("				        , TE.TE_NAME AS Name")
    If chk_actual_state.Checked Then
      _sb.AppendLine("				        , ISNULL(MC_EXPECTED_RE_TICKET_AMOUNT + MC_EXPECTED_PROMO_RE_TICKET_AMOUNT, 0) AS AmountRedeem")
      _sb.AppendLine("			  	      , ISNULL(MC_EXPECTED_PROMO_NR_TICKET_AMOUNT, 0) AS AmountNoredeem")
      _sb.AppendLine("			  	      , ISNULL(MC_EXPECTED_BILL_AMOUNT, 0) AmountBn")
      _sb.AppendLine("			  	      , ISNULL(MC_EXPECTED_BILL_COUNT, 0) CountBn")
    Else
      _sb.AppendLine("				        , ISNULL(MC_COLLECTED_RE_TICKET_AMOUNT + MC_COLLECTED_PROMO_RE_TICKET_AMOUNT, 0) AS AmountRedeem")
      _sb.AppendLine("			  	      , ISNULL(MC_COLLECTED_PROMO_NR_TICKET_AMOUNT, 0) AS AmountNoredeem")
      _sb.AppendLine("			  	      , ISNULL(MC_COLLECTED_BILL_AMOUNT, 0) AmountBn")
      _sb.AppendLine("			  	      , ISNULL(MC_COLLECTED_BILL_COUNT, 0) CountBn")
    End If
    _sb.AppendLine("			  	      , '0' FaceValue")
    _sb.AppendLine("			  	      , '0' BillCount")
    _sb.AppendLine("			  	      , MC.MC_TERMINAL_ID TerminalId")
    _sb.AppendLine("				        , MC.MC_DATETIME AS DateDay")
    _sb.AppendLine("				        , '-1' AS CurrencyType")
    If chk_actual_state.Checked Then
      _sb.AppendLine("				        , ISNULL(MC_EXPECTED_COIN_AMOUNT, 0) AS AmountCoin")
    Else
      _sb.AppendLine("				        , ISNULL(MC_COLLECTED_COIN_AMOUNT, 0) AS AmountCoin")
    End If
    _sb.AppendLine("			    FROM	  MONEY_COLLECTIONS			MC")
    _sb.AppendLine("		 LEFT JOIN    TERMINALS		       TE ON (TE.TE_TERMINAL_ID         = MC.MC_TERMINAL_ID)")

    If chk_actual_state.Checked Then
      _sb.AppendLine("         WHERE    (MC.MC_STATUS = " & CShort(TITO_MONEY_COLLECTION_STATUS.OPEN).ToString() & ")")
    Else
      _sb.AppendLine("         WHERE     MC.MC_STATUS IN ( " & CShort(TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED).ToString() & " , " & CShort(TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE).ToString() & ")")
      _sb.AppendLine("			     AND     MC.MC_TERMINAL_ID IS NOT NULL")
      If uc_dates.FromDateSelected OrElse uc_dates.ToDateSelected Then
        _sb.AppendLine("         AND   " & uc_dates.GetSqlFilterCondition("MC.MC_DATETIME"))
      End If
    End If

    _sb.AppendLine("			       AND    " & GetSelectedTerminals("TE.TE_TERMINAL_ID"))

    _sb.AppendLine("		 UNION ALL")

    '-- BILLETES
    If chk_actual_state.Checked Then
      _sb.AppendLine("		    SELECT    TE.TE_PROVIDER_ID AS Provider")
      _sb.AppendLine("				        , TE.TE_NAME AS Name")
      _sb.AppendLine("				        , '0' AmountRedeem")
      _sb.AppendLine("				        , '0' AmountNoredeem")
      _sb.AppendLine("				        , '0' AmountBn")
      _sb.AppendLine("				        , '0' CountBn")
      _sb.AppendLine("			  	      , CTM.CTM_DENOMINATION FaceValue")
      _sb.AppendLine("			  	      , CTM.CTM_QUANTITY BillCount")
      _sb.AppendLine("				        , MC.MC_TERMINAL_ID	TerminalId")
      _sb.AppendLine("				        , MC.MC_DATETIME AS DateDay")
      _sb.AppendLine("				        , '0' AS CurrencyType")
      _sb.AppendLine("				        , 0 AS AmountCoin")
      _sb.AppendLine("			    FROM	  CASHIER_TERMINAL_MONEY  CTM")
      _sb.AppendLine("		 LEFT JOIN    MONEY_COLLECTIONS	       MC ON (CTM.CTM_MONEY_COLLECTION_ID = MC.MC_COLLECTION_ID)")
      _sb.AppendLine("		 LEFT JOIN    TERMINALS			           TE ON ( TE.TE_TERMINAL_ID		      = MC.MC_TERMINAL_ID)")

      _sb.AppendLine("         WHERE      (MC.MC_STATUS = " & CShort(TITO_MONEY_COLLECTION_STATUS.OPEN).ToString() & ")")
      _sb.AppendLine("           AND    " & GetSelectedTerminals("TE.TE_TERMINAL_ID"))
      _sb.AppendLine("		 UNION                                ")
      _sb.AppendLine("		    SELECT    TE.TE_PROVIDER_ID AS Provider")
      _sb.AppendLine("				        , TE.TE_NAME AS Name")
      _sb.AppendLine("				        , '0' AmountRedeem")
      _sb.AppendLine("				        , '0' AmountNoredeem")
      _sb.AppendLine("				        , '0' AmountBn")
      _sb.AppendLine("				        , '0' CountBn")
      _sb.AppendLine("			  	      , MCD.MCD_FACE_VALUE FaceValue")
      _sb.AppendLine("			  	      , MCD.MCD_NUM_COLLECTED BillCount")
      _sb.AppendLine("				        , MC.MC_TERMINAL_ID	TerminalId")
      _sb.AppendLine("				        , MC.MC_DATETIME AS DateDay")
      _sb.AppendLine("				        , MCD.MCD_CAGE_CURRENCY_TYPE AS CurrencyType")
      _sb.AppendLine("				        , ISNULL(MC_EXPECTED_COIN_AMOUNT, 0) AS AmountCoin")
      _sb.AppendLine("			    FROM	  MONEY_COLLECTION_DETAILS  MCD")
      _sb.AppendLine("		 LEFT JOIN    MONEY_COLLECTIONS	         MC ON (MCD.MCD_COLLECTION_ID = MC.MC_COLLECTION_ID)")
      _sb.AppendLine("		 LEFT JOIN    TERMINALS			             TE ON ( TE.TE_TERMINAL_ID	  = MC.MC_TERMINAL_ID)")
      _sb.AppendLine("         WHERE   " & GetSelectedTerminals("TE.TE_TERMINAL_ID"))
      _sb.AppendLine("           AND   MCD.MCD_FACE_VALUE > 0")

      If chk_actual_state.Checked Then
        _sb.AppendLine("         AND    MC.MC_STATUS = " & CShort(TITO_MONEY_COLLECTION_STATUS.OPEN).ToString())
      Else
        _sb.AppendLine("           AND     MC.MC_STATUS IN ( " & CShort(TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED).ToString() & " , " & CShort(TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE).ToString() & ")")
        _sb.AppendLine("			     AND     MC.MC_TERMINAL_ID IS NOT NULL")
        If uc_dates.FromDateSelected OrElse uc_dates.ToDateSelected Then
          _sb.AppendLine("         AND   " & uc_dates.GetSqlFilterCondition("MC.MC_DATETIME"))
        End If
      End If



    Else
      _sb.AppendLine("		    SELECT    TE.TE_PROVIDER_ID AS Provider")
      _sb.AppendLine("				        , TE.TE_NAME AS Name")
      _sb.AppendLine("				        , '0' AmountRedeem")
      _sb.AppendLine("				        , '0' AmountNoredeem")
      _sb.AppendLine("				        , '0' AmountBn")
      _sb.AppendLine("				        , '0' CountBn")
      _sb.AppendLine("			  	      , MCD.MCD_FACE_VALUE FaceValue")
      _sb.AppendLine("			  	      , MCD.MCD_NUM_COLLECTED BillCount")
      _sb.AppendLine("				        , MC.MC_TERMINAL_ID	TerminalId")
      _sb.AppendLine("				        , MC.MC_DATETIME AS DateDay")
      _sb.AppendLine("				        , MCD.MCD_CAGE_CURRENCY_TYPE AS CurrencyType")
      _sb.AppendLine("				        , ISNULL(MC_COLLECTED_COIN_AMOUNT, 0) AS AmountCoin")
      _sb.AppendLine("			    FROM	  MONEY_COLLECTION_DETAILS  MCD")
      _sb.AppendLine("		 LEFT JOIN    MONEY_COLLECTIONS	         MC ON (MCD.MCD_COLLECTION_ID = MC.MC_COLLECTION_ID)")
      _sb.AppendLine("		 LEFT JOIN    TERMINALS			             TE ON ( TE.TE_TERMINAL_ID	  = MC.MC_TERMINAL_ID)")
      _sb.AppendLine("         WHERE   " & GetSelectedTerminals("TE.TE_TERMINAL_ID"))
      _sb.AppendLine("           AND   MCD.MCD_FACE_VALUE > 0")

      If chk_actual_state.Checked Then
        _sb.AppendLine("         AND    MC.MC_STATUS = " & CShort(TITO_MONEY_COLLECTION_STATUS.OPEN).ToString())
      Else
        _sb.AppendLine("           AND     MC.MC_STATUS IN ( " & CShort(TITO_MONEY_COLLECTION_STATUS.CLOSED_BALANCED).ToString() & " , " & CShort(TITO_MONEY_COLLECTION_STATUS.CLOSED_IN_IMBALANCE).ToString() & ")")
        _sb.AppendLine("			     AND     MC.MC_TERMINAL_ID IS NOT NULL")
        If uc_dates.FromDateSelected OrElse uc_dates.ToDateSelected Then
          _sb.AppendLine("         AND   " & uc_dates.GetSqlFilterCondition("MC.MC_DATETIME"))
        End If
      End If
    End If

    _sb.AppendLine("    ) AS  T")

    'HBB if true just show those rows with activity
    If chk_actual_state.Checked AndAlso chk_show_without_activity.Checked Then
      _sb.AppendLine("WHERE T.AmountRedeem > 0 OR T.AmountNoredeem > 0 OR t.CountBn > 0 OR T.BillCount > 0 OR T.AmountCoin > 0")
    End If

    If Me.opt_terminal.Checked OrElse Me.opt_terminal_day.Checked Then
      ' Terminal group by
      _sb.AppendLine("ORDER BY T.Name, Since, T.Provider, T.TerminalId, T.AmountBn")
    End If
    If Me.opt_provider.Checked OrElse opt_provider_day.Checked Then
      ' Provider group by
      _sb.AppendLine("ORDER BY T.Provider, Since, T.Name, T.TerminalId, T.AmountBn")
    End If
    If Me.opt_day.Checked OrElse Me.opt_day_terminal.Checked Then
      ' Date group by
      _sb.AppendLine("ORDER BY Since, T.Name, T.TerminalId, T.Provider, T.AmountBn")
    End If

    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE: Perform preliminary processing for the grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_BeforeFirstRow()
    m_subtotal_rows = 0
    m_subtotal_print = False

    m_cash_flow.Init(m_bill_types)
    m_cash_flow_subtotals.Init(m_bill_types)
    m_cash_flow_totals.Init(m_bill_types)

  End Sub ' GUI_BeforeFirstRow

  ''' <summary>
  ''' Performs various comparisons for determines if current row is equal to SQL row and if print subtotal row
  ''' </summary>
  ''' <param name="DbRow">Current DbRow from SQL Query</param>
  ''' <param name="CurrentRowEqualSQL">Determines if current row is equal to SQL row</param>
  ''' <remarks></remarks>
  Private Sub CompareCurrentRowToSQLRow(ByVal DbRow As CLASS_DB_ROW, ByRef CurrentRowEqualSQL As Boolean)
    Select Case m_group_by_column
      Case GROUP_OPTION.GROUP_OPTION_TERMINAL,
           GROUP_OPTION.GROUP_OPTION_TERMINAL_DAY

        If (m_cash_flow.first_row_terminal) Then
          m_cash_flow.Init(DbRow.Value(SQL_COLUMN_TERMINAL_PROV), DbRow.Value(SQL_COLUMN_TERMINAL_NAME), DbRow.Value(SQL_COLUMN_DATE_SINCE), DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_bill_types)
        End If
        CurrentRowEqualSQL = IIf(DbRow.Value(SQL_COLUMN_TERMINAL_ID) = m_cash_flow.terminal_id, True, False)
        m_subtotal_print = m_group_by_column = GROUP_OPTION.GROUP_OPTION_TERMINAL_DAY AndAlso Not CurrentRowEqualSQL

      Case GROUP_OPTION.GROUP_OPTION_PROV,
           GROUP_OPTION.GROUP_OPTION_PROV_DAY

        If (m_cash_flow.first_row_terminal) Then
          m_cash_flow.Init(DbRow.Value(SQL_COLUMN_TERMINAL_PROV), DbRow.Value(SQL_COLUMN_TERMINAL_NAME), DbRow.Value(SQL_COLUMN_DATE_SINCE), DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_bill_types)
        End If
        CurrentRowEqualSQL = IIf(DbRow.Value(SQL_COLUMN_TERMINAL_PROV) = m_cash_flow.terminal_provider, True, False)
        m_subtotal_print = m_group_by_column = GROUP_OPTION.GROUP_OPTION_PROV_DAY AndAlso Not CurrentRowEqualSQL

      Case GROUP_OPTION.GROUP_OPTION_DAY,
           GROUP_OPTION.GROUP_OPTION_DAY_TERMINAL

        If (m_cash_flow.first_row_terminal) Then
          m_cash_flow.Init(DbRow.Value(SQL_COLUMN_TERMINAL_PROV), DbRow.Value(SQL_COLUMN_TERMINAL_NAME), DbRow.Value(SQL_COLUMN_DATE_SINCE), DbRow.Value(SQL_COLUMN_TERMINAL_ID), m_bill_types)
        End If

        If (m_cash_flow.date_until <> Nothing) Then
          If (DbRow.Value(SQL_COLUMN_DATE_SINCE) = m_cash_flow.date_since AndAlso DbRow.Value(SQL_COLUMN_DATE_UNTIL) = m_cash_flow.date_until) Then
            CurrentRowEqualSQL = True
          Else
            CurrentRowEqualSQL = False
          End If
        Else
          CurrentRowEqualSQL = True
        End If

        m_subtotal_print = m_group_by_column = GROUP_OPTION.GROUP_OPTION_DAY_TERMINAL AndAlso Not CurrentRowEqualSQL

    End Select

    If (m_group_by_column = GROUP_OPTION.GROUP_OPTION_TERMINAL_DAY OrElse m_group_by_column = GROUP_OPTION.GROUP_OPTION_DAY_TERMINAL) Then
      If (m_cash_flow.date_until <> Nothing) Then
        If (DbRow.Value(SQL_COLUMN_TERMINAL_ID) = m_cash_flow.terminal_id AndAlso (DbRow.Value(SQL_COLUMN_DATE_SINCE) = m_cash_flow.date_since AndAlso DbRow.Value(SQL_COLUMN_DATE_UNTIL) = m_cash_flow.date_until)) Then
          CurrentRowEqualSQL = True
        Else
          CurrentRowEqualSQL = False
        End If
      Else
        CurrentRowEqualSQL = True
      End If
    ElseIf (m_group_by_column = GROUP_OPTION.GROUP_OPTION_PROV_DAY) Then
      If (m_cash_flow.date_until <> Nothing) Then
        If (DbRow.Value(SQL_COLUMN_TERMINAL_PROV) = m_cash_flow.terminal_provider AndAlso (DbRow.Value(SQL_COLUMN_DATE_SINCE) = m_cash_flow.date_since AndAlso DbRow.Value(SQL_COLUMN_DATE_UNTIL) = m_cash_flow.date_until)) Then
          CurrentRowEqualSQL = True
        Else
          CurrentRowEqualSQL = False
        End If
      Else
        CurrentRowEqualSQL = True
      End If
    End If

  End Sub

  ''' <summary>
  ''' Checks out the DB row before adding it to the grid
  ''' </summary>
  ''' <param name="DbRow">Current DbRow</param>
  ''' <returns>Always False</returns>
  ''' <remarks></remarks>
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean
    Dim _is_current_row_equal_sql As Boolean
    Dim _print_row As Boolean
    _is_current_row_equal_sql = True
    _print_row = False

    CompareCurrentRowToSQLRow(DbRow, _is_current_row_equal_sql)

    If (_is_current_row_equal_sql) Then
      AccumulateRow(DbRow)
      _print_row = False
    Else
      'This accumulated in GUI_SetupRow
      _print_row = True
    End If

    If (_print_row) Then
      m_cash_flow_subtotals.AccumulateToTotals(m_cash_flow, False)
      m_cash_flow_totals.AccumulateToTotals(m_cash_flow, True)
    End If

    Return _print_row
  End Function

  ' PURPOSE: Perform final processing for the grid data (totalizator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    'Print last row
    If (m_cash_flow.terminal_id <> 0) Then
      Me.Grid.AddRow()
      m_cash_flow_subtotals.AccumulateToTotals(m_cash_flow, False)
      m_cash_flow_totals.AccumulateToTotals(m_cash_flow, True)
      GUI_SetupRow(Me.Grid.NumRows - 1, Nothing)
    End If


    If (Me.Grid.NumRows > 0) Then
      If (ShowSubtotals()) Then
        PrintTotalsRow(False, m_cash_flow_subtotals)
        m_subtotal_rows += 1
      End If

      PrintTotalsRow(True, m_cash_flow_totals)
      Me.Grid.Counter(GRID_COUNTER_SUBTOTALS).Value = m_subtotal_rows
    End If

  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()
    Dim _terminal As String
    Dim _date_temp As String
    Dim _selected_cashiers As String
    Dim _checked_counter As Integer
    Dim _groupby As String

    m_filter_params.Clear()
    _groupby = ""

    ' Dates
    If Not chk_actual_state.Checked Then
      _date_temp = IIf(uc_dates.FromDateSelected, mdl_tito.GetFormatedDate(uc_dates.FromDate.AddHours(uc_dates.ClosingTime)), "")
      Call AddFilterItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(297), _date_temp)
      _date_temp = IIf(uc_dates.ToDateSelected, mdl_tito.GetFormatedDate(uc_dates.ToDate.AddHours(uc_dates.ClosingTime)), "")
      Call AddFilterItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(298), _date_temp)
    End If

    Call AddFilterItem(chk_actual_state.Text, IIf(chk_actual_state.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))
    If chk_actual_state.Checked Then
      Call AddFilterItem(chk_show_without_activity.Text, IIf(chk_show_without_activity.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))
    End If

    If Me.opt_terminal.Checked Then
      _groupby = Me.opt_terminal.Text
    End If
    If opt_terminal_day.Checked Then
      _groupby = Me.opt_terminal_day.Text
    End If
    If opt_provider.Checked Then
      _groupby = Me.opt_provider.Text
    End If
    If opt_provider_day.Checked Then
      _groupby = Me.opt_provider_day.Text
    End If
    If opt_day.Checked Then
      _groupby = Me.opt_day.Text
    End If
    If opt_day_terminal.Checked Then
      _groupby = Me.opt_day_terminal.Text
    End If

    Call AddFilterItem(Me.lbl_group_by.Text, _groupby)

    'Details
    Call AddFilterItem("", "")
    Call AddFilterItem("", "")

    ' Cashiers
    _checked_counter = 0
    _selected_cashiers = ""

    Call AddFilterItem("", "")
    Call AddFilterItem("", "")
    Call AddFilterItem("", "")
    Call AddFilterItem("", "")

    ' Terminals 
    _terminal = Me.uc_pr_list.GetTerminalReportText()

    Call AddFilterItem(GLB_NLS_GUI_PLAYER_TRACKING.GetString(592), _terminal)

  End Sub ' GUI_ReportUpdateFilters

  ' PURPOSE: Get reports from print information and update report filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    Dim _text As String
    Dim _keys() As Integer
    Dim _values() As String

    ReDim _keys(m_filter_params.Count - 1)

    m_filter_params.Keys.CopyTo(_keys, 0)
    Array.Sort(_keys)

    For Each _key As Integer In _keys
      _text = m_filter_params(_key)
      _values = _text.Split(";")

      If String.IsNullOrEmpty(_values(0)) Then
        PrintData.SetFilter("", "", True)
      Else
        PrintData.SetFilter(_values(0), _values(1))
      End If
    Next

    PrintData.FilterHeaderWidth(1) = 2000
    PrintData.FilterHeaderWidth(2) = 2500
    PrintData.FilterHeaderWidth(3) = 2500
    PrintData.FilterValueWidth(2) = 2100
    PrintData.FilterValueWidth(4) = 2100

  End Sub ' GUI_ReportFilter

#End Region ' OVERRIDES

#Region " Public functions and methods "

  ' TODO: tbd
  Public Overloads Sub Show(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent

    Call GUI_Init()
    'TODO:
    'Me.Display(False)
    Call Show()

  End Sub ' Show

#End Region ' Public functions and methods

#Region " Events "

  ' PURPOSE: Controls the selected information
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    
  Private Sub chk_Actual_State_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_actual_state.CheckedChanged

    uc_dates.Enabled = Not chk_actual_state.Checked
    opt_day.Enabled = Not chk_actual_state.Checked
    opt_terminal_day.Enabled = IIf(Not chk_actual_state.Checked, True, False)
    opt_day.Enabled = IIf(Not chk_actual_state.Checked, True, False)
    opt_day_terminal.Enabled = IIf(Not chk_actual_state.Checked, True, False)
    opt_provider_day.Enabled = IIf(Not chk_actual_state.Checked, True, False)
    chk_show_without_activity.Enabled = chk_actual_state.Checked
    chk_show_without_activity.Checked = False

    If chk_actual_state.Checked Then
      If opt_day.Checked OrElse opt_day_terminal.Checked OrElse opt_terminal_day.Checked OrElse opt_provider_day.Checked Then
        opt_terminal.Checked = True
      End If
    End If

  End Sub ' chk_Actual_State_CheckedChanged

  ' PURPOSE: Indicates whether subtotals must be shown in the grid or not
  '
  '    - INPUT:
  '
  '    - OUTPUT:
  '
  ' RETURNS:
  '    
  Private Function ShowSubtotals() As Boolean
    Dim _show_subtotals As Boolean

    _show_subtotals = False

    If opt_day_terminal.Checked OrElse opt_terminal_day.Checked OrElse opt_provider_day.Checked Then
      _show_subtotals = True
    End If

    Return _show_subtotals

  End Function ' ShowSubtotals

#End Region ' Events

End Class