<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_stacker_collections_sel
  Inherits GUI_Controls.frm_base_sel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.uc_provider = New GUI_Controls.uc_provider
    Me.gb_collections_status = New System.Windows.Forms.GroupBox
    Me.chk_collection_status_not_closed = New System.Windows.Forms.CheckBox
    Me.chk_collection_status_incorrect = New System.Windows.Forms.CheckBox
    Me.chk_collection_status_correct = New System.Windows.Forms.CheckBox
    Me.ds_from_to = New GUI_Controls.uc_daily_session_selector
    Me.gb_employee = New System.Windows.Forms.GroupBox
    Me.opt_one_employee = New System.Windows.Forms.RadioButton
    Me.opt_all_employee = New System.Windows.Forms.RadioButton
    Me.cmb_employee = New GUI_Controls.uc_combo
    Me.gb_stacker = New System.Windows.Forms.GroupBox
    Me.opt_none_stackers = New System.Windows.Forms.RadioButton
    Me.opt_one_stacker = New System.Windows.Forms.RadioButton
    Me.opt_all_stackers = New System.Windows.Forms.RadioButton
    Me.cmb_stacker = New GUI_Controls.uc_combo
    Me.lb_cashier_session = New System.Windows.Forms.Label
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_collections_status.SuspendLayout()
    Me.gb_employee.SuspendLayout()
    Me.gb_stacker.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lb_cashier_session)
    Me.panel_filter.Controls.Add(Me.gb_stacker)
    Me.panel_filter.Controls.Add(Me.gb_employee)
    Me.panel_filter.Controls.Add(Me.ds_from_to)
    Me.panel_filter.Controls.Add(Me.gb_collections_status)
    Me.panel_filter.Controls.Add(Me.uc_provider)
    Me.panel_filter.Size = New System.Drawing.Size(993, 199)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_provider, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_collections_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.ds_from_to, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_employee, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_stacker, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lb_cashier_session, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 203)
    Me.panel_data.Size = New System.Drawing.Size(993, 387)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(987, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(987, 4)
    '
    'uc_provider
    '
    Me.uc_provider.Location = New System.Drawing.Point(510, 2)
    Me.uc_provider.Name = "uc_provider"
    Me.uc_provider.Size = New System.Drawing.Size(392, 183)
    Me.uc_provider.TabIndex = 4
    '
    'gb_collections_status
    '
    Me.gb_collections_status.Controls.Add(Me.chk_collection_status_not_closed)
    Me.gb_collections_status.Controls.Add(Me.chk_collection_status_incorrect)
    Me.gb_collections_status.Controls.Add(Me.chk_collection_status_correct)
    Me.gb_collections_status.Location = New System.Drawing.Point(6, 97)
    Me.gb_collections_status.Name = "gb_collections_status"
    Me.gb_collections_status.Size = New System.Drawing.Size(266, 89)
    Me.gb_collections_status.TabIndex = 1
    Me.gb_collections_status.TabStop = False
    Me.gb_collections_status.Text = "xCollectionsState"
    '
    'chk_collection_status_not_closed
    '
    Me.chk_collection_status_not_closed.AutoSize = True
    Me.chk_collection_status_not_closed.Location = New System.Drawing.Point(23, 21)
    Me.chk_collection_status_not_closed.Name = "chk_collection_status_not_closed"
    Me.chk_collection_status_not_closed.Size = New System.Drawing.Size(91, 17)
    Me.chk_collection_status_not_closed.TabIndex = 1
    Me.chk_collection_status_not_closed.Text = "xNotClosed"
    Me.chk_collection_status_not_closed.UseVisualStyleBackColor = True
    '
    'chk_collection_status_incorrect
    '
    Me.chk_collection_status_incorrect.AutoSize = True
    Me.chk_collection_status_incorrect.Location = New System.Drawing.Point(23, 63)
    Me.chk_collection_status_incorrect.Name = "chk_collection_status_incorrect"
    Me.chk_collection_status_incorrect.Size = New System.Drawing.Size(85, 17)
    Me.chk_collection_status_incorrect.TabIndex = 16
    Me.chk_collection_status_incorrect.Text = "xIncorrect"
    Me.chk_collection_status_incorrect.UseVisualStyleBackColor = True
    '
    'chk_collection_status_correct
    '
    Me.chk_collection_status_correct.AutoSize = True
    Me.chk_collection_status_correct.Location = New System.Drawing.Point(23, 42)
    Me.chk_collection_status_correct.Name = "chk_collection_status_correct"
    Me.chk_collection_status_correct.Size = New System.Drawing.Size(76, 17)
    Me.chk_collection_status_correct.TabIndex = 15
    Me.chk_collection_status_correct.Text = "xCorrect"
    Me.chk_collection_status_correct.UseVisualStyleBackColor = True
    '
    'ds_from_to
    '
    Me.ds_from_to.ClosingTime = 0
    Me.ds_from_to.ClosingTimeEnabled = False
    Me.ds_from_to.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.ds_from_to.FromDateSelected = True
    Me.ds_from_to.Location = New System.Drawing.Point(6, 6)
    Me.ds_from_to.Name = "ds_from_to"
    Me.ds_from_to.Size = New System.Drawing.Size(266, 84)
    Me.ds_from_to.TabIndex = 0
    Me.ds_from_to.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.ds_from_to.ToDateSelected = True
    '
    'gb_employee
    '
    Me.gb_employee.Controls.Add(Me.opt_one_employee)
    Me.gb_employee.Controls.Add(Me.opt_all_employee)
    Me.gb_employee.Controls.Add(Me.cmb_employee)
    Me.gb_employee.Location = New System.Drawing.Point(279, 6)
    Me.gb_employee.Name = "gb_employee"
    Me.gb_employee.Size = New System.Drawing.Size(225, 84)
    Me.gb_employee.TabIndex = 2
    Me.gb_employee.TabStop = False
    Me.gb_employee.Text = "xEmployee"
    '
    'opt_one_employee
    '
    Me.opt_one_employee.Location = New System.Drawing.Point(6, 20)
    Me.opt_one_employee.Name = "opt_one_employee"
    Me.opt_one_employee.Size = New System.Drawing.Size(72, 24)
    Me.opt_one_employee.TabIndex = 17
    Me.opt_one_employee.Text = "xOne"
    '
    'opt_all_employee
    '
    Me.opt_all_employee.Location = New System.Drawing.Point(6, 52)
    Me.opt_all_employee.Name = "opt_all_employee"
    Me.opt_all_employee.Size = New System.Drawing.Size(64, 24)
    Me.opt_all_employee.TabIndex = 19
    Me.opt_all_employee.Text = "xAll"
    '
    'cmb_employee
    '
    Me.cmb_employee.IsReadOnly = False
    Me.cmb_employee.Location = New System.Drawing.Point(78, 20)
    Me.cmb_employee.Name = "cmb_employee"
    Me.cmb_employee.SelectedIndex = -1
    Me.cmb_employee.Size = New System.Drawing.Size(139, 24)
    Me.cmb_employee.SufixText = "Sufix Text"
    Me.cmb_employee.SufixTextVisible = True
    Me.cmb_employee.TabIndex = 18
    Me.cmb_employee.TextVisible = False
    Me.cmb_employee.TextWidth = 0
    '
    'gb_stacker
    '
    Me.gb_stacker.Controls.Add(Me.opt_none_stackers)
    Me.gb_stacker.Controls.Add(Me.opt_one_stacker)
    Me.gb_stacker.Controls.Add(Me.opt_all_stackers)
    Me.gb_stacker.Controls.Add(Me.cmb_stacker)
    Me.gb_stacker.Location = New System.Drawing.Point(279, 96)
    Me.gb_stacker.Name = "gb_stacker"
    Me.gb_stacker.Size = New System.Drawing.Size(225, 89)
    Me.gb_stacker.TabIndex = 3
    Me.gb_stacker.TabStop = False
    Me.gb_stacker.Text = "xStacker"
    '
    'opt_none_stackers
    '
    Me.opt_none_stackers.AutoSize = True
    Me.opt_none_stackers.Location = New System.Drawing.Point(6, 64)
    Me.opt_none_stackers.Name = "opt_none_stackers"
    Me.opt_none_stackers.Size = New System.Drawing.Size(61, 17)
    Me.opt_none_stackers.TabIndex = 23
    Me.opt_none_stackers.TabStop = True
    Me.opt_none_stackers.Text = "xNone"
    Me.opt_none_stackers.UseVisualStyleBackColor = True
    '
    'opt_one_stacker
    '
    Me.opt_one_stacker.Location = New System.Drawing.Point(6, 16)
    Me.opt_one_stacker.Name = "opt_one_stacker"
    Me.opt_one_stacker.Size = New System.Drawing.Size(72, 17)
    Me.opt_one_stacker.TabIndex = 20
    Me.opt_one_stacker.Text = "xOne"
    '
    'opt_all_stackers
    '
    Me.opt_all_stackers.Location = New System.Drawing.Point(6, 40)
    Me.opt_all_stackers.Name = "opt_all_stackers"
    Me.opt_all_stackers.Size = New System.Drawing.Size(64, 17)
    Me.opt_all_stackers.TabIndex = 22
    Me.opt_all_stackers.Text = "xAll"
    '
    'cmb_stacker
    '
    Me.cmb_stacker.IsReadOnly = False
    Me.cmb_stacker.Location = New System.Drawing.Point(78, 12)
    Me.cmb_stacker.Name = "cmb_stacker"
    Me.cmb_stacker.SelectedIndex = -1
    Me.cmb_stacker.Size = New System.Drawing.Size(139, 24)
    Me.cmb_stacker.SufixText = "Sufix Text"
    Me.cmb_stacker.SufixTextVisible = True
    Me.cmb_stacker.TabIndex = 21
    Me.cmb_stacker.TextVisible = False
    Me.cmb_stacker.TextWidth = 0
    '
    'lb_cashier_session
    '
    Me.lb_cashier_session.AutoSize = True
    Me.lb_cashier_session.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lb_cashier_session.Location = New System.Drawing.Point(3, 179)
    Me.lb_cashier_session.Name = "lb_cashier_session"
    Me.lb_cashier_session.Size = New System.Drawing.Size(106, 13)
    Me.lb_cashier_session.TabIndex = 11
    Me.lb_cashier_session.Text = "xCashier Session"
    Me.lb_cashier_session.Visible = False
    '
    'frm_stacker_collections_sel
    '
    Me.ClientSize = New System.Drawing.Size(1001, 594)
    Me.Name = "frm_stacker_collections_sel"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_collections_status.ResumeLayout(False)
    Me.gb_collections_status.PerformLayout()
    Me.gb_employee.ResumeLayout(False)
    Me.gb_stacker.ResumeLayout(False)
    Me.gb_stacker.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents uc_provider As GUI_Controls.uc_provider
  Friend WithEvents gb_collections_status As System.Windows.Forms.GroupBox
  Friend WithEvents chk_collection_status_incorrect As System.Windows.Forms.CheckBox
  Friend WithEvents chk_collection_status_correct As System.Windows.Forms.CheckBox
  Friend WithEvents ds_from_to As GUI_Controls.uc_daily_session_selector
  Friend WithEvents gb_employee As System.Windows.Forms.GroupBox
  Friend WithEvents opt_one_employee As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_employee As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_employee As GUI_Controls.uc_combo
  Friend WithEvents gb_stacker As System.Windows.Forms.GroupBox
  Friend WithEvents opt_one_stacker As System.Windows.Forms.RadioButton
  Friend WithEvents opt_all_stackers As System.Windows.Forms.RadioButton
  Friend WithEvents opt_none_stackers As System.Windows.Forms.RadioButton
  Friend WithEvents cmb_stacker As GUI_Controls.uc_combo
  Friend WithEvents lb_cashier_session As System.Windows.Forms.Label
  Friend WithEvents chk_collection_status_not_closed As System.Windows.Forms.CheckBox
End Class
