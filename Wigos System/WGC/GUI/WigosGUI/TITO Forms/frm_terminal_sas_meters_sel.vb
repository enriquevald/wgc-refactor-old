'-------------------------------------------------------------------
' Copyright © 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_terminal_sas_meters_sel
' DESCRIPTION:   SAS Counters
' AUTHOR:        Adrián Cuartas
' CREATION DATE: 22-NOV-2013
'
' REVISION HISTORY:
'
' Date        Author Description
' ----------  ------ -------------------------------------------------------------------------------------
' 22-NOV-2013 ACM    First Release
' 03-DEC-2013 AMF    Resize uc_checked_list_groups
' 09-DEC-2013 AMF    Groups changes
' 18-DEC-2013 ACM    Fixed Bug #WIGOSTITO-864. Set only Sas-Host terminals to terminals filter control.
' 19-DEC-2013 JML    Change NLSs & Add Tooltip 
' 19-DEC-2013 JPJ    Changed Excel alignment and width of columns
' 21-MAR-2014 JBC    Added checkbox to hide terminal results without activity. Query optimized
' 26-MAR-2014 ICS    Fixed Bug #WIGOSTITO-1168. The filter 'Some' requires selecting at least one
' 27-MAR-2014 DLL    Fixed Bug #WIGOSTITO-1173. The filter Counters incorrect text
' 09-APR-2014 DHA    Fixed Bug #WIGOSTITO-1173. Modified Counter/Dates filters, incorrect text
' 11-APR-2014 LEM    Fixed Bug WIGOSTITO-1198: No Activity filter shows rows with all increments equals cero on Meters by type checked
' 22-APR-2014 HBB    Add Filter First Time
' 23-APR-2014 LEM    Group Meters name use SMG_NAME field from SAS_METERS_GROUPS (Not NLS)
' 28-APR-2014 LEM    Meter 0x000C only visible in WASS mode
' 28-APR-2014 LEM    Fixed Bug WIG-862: Error searching by meter type in WASS mode.
' 07-MAY-2014 HBB    The column headers are taken from another field in DB. If te field is null, the header is teaken from NLs
' 14-MAY-2014 JFC    Fixed Bug WIG-917: The names of the meters filter groups are in English
' 03-SEP-2014 LEM    Added functionality to show terminal location data.
' 26-SEP-2014 LEM & JML Fixed Bug WIG-1312: Error searching with several filter options.
' 29-OCT-2014 SGB    Fixed Bug WIG-1597: Display final value according counter if type is first register
' 12-NOV-2014 SGB    Fixed Bug WIG-1683: Optimized SQL query
' 22-DIC-2014 SGB    Fixed Bug WIG-1811: Orber by actual for date in query
' 28-JAN-2015 ANM    Added id floor column in provider report type
' 02-FEB-2015 SGB    Fixed Bug WIG-1945: Set equal order terminals and dates
' 09-FEB-2015 DRV    Fixed Bug WIG-2000: Error filtering by date.
' 16-MAR-2015 YNM    Fixed Bug WIG-2151: Add Date From and Date To on Grid result when meter Type is Daily and Hourly
' 25-MAR-2015 MPO &RCI    Fixed Bug WIG-2151: The WCP Service must always save the "From" date. Adapt the form with that.
' 25-MAR-2015 RCI    Fixed Bug WIG-2000: As now WCP Service always saves the "From" date, the changes made for WIG-2000 must be reverted.
' 25-MAR-2015 RCI    Fixed Bug WIG-2151: Remove DateTo from Grid. Show as before.
' 25-MAR-2015 RCI    Fixed Bug WIG-1810: chk_hide_no_activity doesn't work.
' 20-APR-2015 MPO    Fixed Bug WIG-2224: Index out of range.
' 14-DEC-2015 ETP    Fixed Bug-7649 Modified Min width for meter columns and default values for meters filter.
' 14-DEC-2015 ETP    Product Backlog Item 4690: Gaming Hall: added SystemMode() call for compativility.
' 16-DEC-2015 MPO    Fixed Bug 7766: TSM denominations ins't necessary in the query
' 26-FEB-2016 DDS    Fixed Bug 10023: Filter by terminal was not working
' 09-MAY-2016 RAB    PBI 12627:AFIP Client: Recovery accounting denomination
' 24-MAY-2016 RAB    PBI 13462:Review Sprint 24 (Accounting Denomination)
' 12-JUL-2016 RAB    Bug 15251:AFIP - GUI: Historical of counters does not correctly report initial, Final, increase
' 26-JUL-2016 FGB    PBI 14470:AFIP Client: Meters photo -> Include new meter types (SAS Denom Change, Service RAM Clear, Machine RAM Clear)
' 02-SEP-2016 FGB    Fixed BUG 16405: Meter history chk_hide_no_activity "Exclude rows without activity" doesn't work properly
' 27-OCT-2016 JML    Fixed Bug 19727: Response time out, when consulting counters history
' 12-ENE-2017 JML    Fixed Bug 22451: Meters history - Timeout
' 13-ENE-2017 FGB    Fixed Bug 22752: Meters history - Does not correctly show the value of column 'Tipo' when filtering by Rollover.
' 19-ENE-2017 JML    Fixed Bug 22451: Meters history - Timeout
' 10-FEB-2017 MS     Bug 24283:Reportes sin informar correctamente la denominación contable
' 17-MAR-2017 JML    Fixed Bug 25794: GUI - Meters history - Timeout
' 05-APR-2017 EOR    Fixed Bug 26404: Report Meter history version V03.05.0037 
' 25-ABR-2017 LTC    Fixed Bug 26725: Meters history - Timeout
' 12-MAY-2017 LTC    Fixed Bug 26404: Error Report Meter history version V03.05.0037
' 22-MAY-2017 FAV    Fixed Bug 27325:Pantalla "Histórico de Contadores" no muestra resultados
' 07-JUN-2017 JMM    WIGOS-2240: Pantalla "Histórico de Contadores" no muestra resultados
' 08-AUG-2017 RGR    Fixed Bug 29261:WIGOS-3987 - Error in Meter history screen --> "Time Out"
' 19-SEP-2017 ETP    PBI 29621: AGG Data transfer [WIGOS-4458]
' 21-SEP-2017 ETP    Bug 29867: [WIGOS-5318] No results and search button inactive after "Show Terminal location" checked in "Meters History Advanced" screen
' 26-SEP-2017 JML    Fixed Bug 29915:WIGOS-5308 [Ticket #8909] Histórico de Contadores [Denominación Contable] V03.06.0023
' 08-NOV-2017 RAB    Bug 30620:WIGOS-6383 [Ticket #10109] Issue in Meter History - v03.06.0035
' 13-NOV-2017 RAB    Bug 30620:WIGOS-6383 [Ticket #10109] Issue in Meter History - v03.06.0035
' 20-NOV-2017 OMC    PBI 30857: Enable Monitor mode for multisite.
' 20-DEC-2017 DPC    Bug 31131:[WIGOS-7125]: Historical meters screen does not work
' 05-FEB-2017 JML    Fixed Bug 31454:WIGOS-7151 [Ticket #11223] Fallo en uso de filtro en Histórico de Contadores V03.06.0040
' 26-MAR-2017 JML    Fixed Bug 32060:WIGOS-9455 AGG-Multisite- Error with the meters for the same id on different sites
' 25-MAY-2018 MS     PBI 32784: [WIGOS-12153] [Ticket #14466] Fallo – Histórico de Contadores Avanzado duplica contadores - V03.08.0001
' 28-MAY-2018 LQB    Fixed Bug xxxxx:WIGOS-12154 [Ticket #14467] Fallo - Histórico de Contadores - Fila Total
' 29-MAY-2018 RGR    Bug 32832: WIGOS-12152 [Ticket #14465] Fallo - Histórico de contadores Avanzados – Monitor - Version V03.08.0001
' 31-MAY-2018 LQB    Bug 32861: WIGOS-12152 [Ticket #14465] Fallo - Histórico de contadores Avanzados – Monitor - Version V03.08.0001
'---------------------------------------------------------------------------------------------------------

Option Explicit On
Option Strict Off

#Region "Imports"

Imports System.Text
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions


#End Region

Public Class frm_terminal_sas_meters_sel
  Inherits GUI_Controls.frm_base_sel

#Region "Constants"
  Private m_terminal_report_type As ReportType = ReportType.Provider

  Private TERMINAL_DATA_COLUMNS As Int32

  Private GRID_COLUMNS_COUNT As Integer
  Private Const GRID_HEADERS_COUNT As Integer = 2

  Private Const SQL_OFFSET_METER_COLUMNS As Integer = 8 'Offset index for meter values SQL and Grid ' LTC 2017-05-12

  'Index Columns
  Private Const GRID_COLUMN_SELECT As Integer = 0
  Private Const GRID_COLUMN_SITE As Integer = 1
  Private Const GRID_COLUMN_DATE As Integer = 2
  Private Const GRID_COLUMN_METERS_TYPE As Integer = 3
  Private Const GRID_COLUMN_TERMINAL_ID As Integer = 4
  Private Const GRID_INIT_TERMINAL_DATA As Integer = 5
  Private Const GRID_COLUMN_TERMINAL_PROVIDER As Integer = 5
  Private Const GRID_COLUMN_TERMINAL_NAME As Integer = 6
  Private Const GRID_COLUMN_TERMINAL_FLOOR As Integer = 7
  Private Const GRID_COLUMN_TERMINAL_ACCOUNT_DENOMINATION As Integer = 8
  Private Const GRID_COLUMN_TERMINAL_AREA As Integer = 9
  Private Const GRID_COLUMN_TERMINAL_BANK As Integer = 10
  Private Const GRID_COLUMN_TERMINAL_POSITION As Integer = 11

  'Width
  Private Const GRID_WIDTH_SELECT As Integer = 200
  Private Const GRID_WIDTH_SITE As Integer = 850
  Private Const GRID_WIDTH_DATE As Integer = 2150
  Private Const GRID_WIDTH_METERS_TYPE As Integer = 2600
  Private Const GRID_WIDTH_TERMINAL_ID As Integer = 1000
  Private Const GRID_WIDTH_TERMINAL_PROVIDER As Integer = 2000
  Private Const GRID_WIDTH_TERMINAL_NAME As Integer = 2200
  Private Const GRID_WIDTH_COUNTER As Integer = 1500
  Private Const GRID_WIDTH_COUNTER_TYPE As Integer = 1660

  'SQL Columns
  Private Const SQL_COLUMN_DATE As Integer = 0
  Private Const SQL_COLUMN_TERMINAL_ID As Integer = 1
  Private Const SQL_COLUMN_TERMINAL_PROVIDER As Integer = 2
  Private Const SQL_COLUMN_TERMINAL_NAME As Integer = 3
  Private Const SQL_COLUMN_TERMINAL_ISO_CODE As Integer = 4
  Private Const SQL_COLUMN_TERMINAL_FLOOR As Integer = 5
  Private Const SQL_COLUMN_TERMINAL_AREA As Integer = 6
  Private Const SQL_COLUMN_TERMINAL_BANK As Integer = 7
  Private Const SQL_COLUMN_TERMINAL_POSITION As Integer = 8
  Private Const SQL_COLUMN_METERS_TYPE As Integer = 9
  Private Const SQL_COLUMN_TERMINAL_ACCOUNT_DENOMINATION As Integer = 10
  Private Const SQL_COLUMN_GROUP_ID As Integer = 11
  Private Const SQL_COLUMN_SITE As Integer = 12

  'First column in the SQL that has a meter value
  Private Const SQL_FIRST_METER_COLUMN_ADVANCED As Integer = 13
  Private Const SQL_FIRST_METER_COLUMN_SIMPLIFIED As Integer = 15

  'To query SiteId in list of uc_sites_sel sites.
  Private Const SQL_COLUMN_SITE_ID As Integer = 0

  Private Const VISIBLE_CODE_LENGTH = 5
  Private Const SEPARATOR = " - "

  Private Const EVENT_START_BASE As Int32 = &H10000
  Private Const NLS_EVENTS As Int32 = 4000
  Private Const WIDTH_START_VALUE As Int32 = 1800
  Private Const LENGHT_START_VALUE As Int32 = 17
  Private Const FIXED_INITIAL_COLUMNS As Int32 = 5

  ' Excel columns
  Private Const EXCEL_COLUMN_SITE_ID As Integer = 0

  ' Timer interval to refresh meters (in seconds)
  Private METERS_REFRESH_TIME As Integer = 5

#End Region

#Region " Enums "

  Public Enum ENUM_FORM_MODE

    ADVANCED_MODE = 0
    SIMPLIFIED_MODE = 1

  End Enum

#End Region ' Enums

#Region "Members"

  Private m_date_from As String
  Private m_date_to As String
  Private m_terminals As String
  Private m_order_by As String
  Private m_meter_types As String
  Private m_groups As String
  Private m_sites As String = ""
  Private m_selected_meters As List(Of MetersGroup)
  Private m_all_meters As List(Of MetersGroup)
  Private m_sum_total As List(Of Double)
  Private m_tool_tip As List(Of String)
  Private m_total_columns As Integer
  Private m_multiplier_columns As Integer
  Private m_system_mode As WSI.Common.SYSTEM_MODE

  Private m_form_mode As ENUM_FORM_MODE
  Dim m_is_center As Boolean

  Private m_first_time As Boolean = True
  Private m_monitor_enabled As Boolean = False
  Private m_init As Boolean = True


#End Region

#Region "Properties"
#End Region

#Region "Overrides"

  ' PURPOSE: GUI form id.
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: None
  '
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TERMINAL_SAS_METERS_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialize every form control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    m_system_mode = WSI.Common.Misc.SystemMode()

    Me.pnl_filters_standard.Visible = (Me.m_form_mode = ENUM_FORM_MODE.ADVANCED_MODE)
    Me.pnl_filters_standard.Enabled = (Me.m_form_mode = ENUM_FORM_MODE.ADVANCED_MODE)

    Me.pnl_filters_simplified.Visible = (Me.m_form_mode = ENUM_FORM_MODE.SIMPLIFIED_MODE)
    Me.pnl_filters_simplified.Enabled = (Me.m_form_mode = ENUM_FORM_MODE.SIMPLIFIED_MODE)

    ' Form title 
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1401) '1401 "Histórico de contadores 
    If Me.m_form_mode = ENUM_FORM_MODE.ADVANCED_MODE Then
      If Not (m_is_center) Then
        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8316) '8316 "Histórico de contadores avanzado"
      Else
        Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2973) '2973 "Contadores"
      End If
    End If

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False

    ' Counters
    gb_counter_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2419)
    opt_currents.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4925)
    opt_types.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2577)

    chk_daily.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2500)
    chk_hourly.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2416)
    chk_reset.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2418)
    chk_rollover.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2417)
    chk_first_time.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4861)
    chk_denom_change.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7439)

    ' Dates
    gb_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2118)
    gb_date_simplified.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2118)
    dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    dtp_from_simplified.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    dtp_to_simplified.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    dtp_from_simplified.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    dtp_to_simplified.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Order By
    gb_order_by.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(792)
    opt_date.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1574)
    opt_terminal.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1968)

    ' Groups
    uc_checked_list_groups.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4928)
    uc_checked_list_groups.btn_check_all.Width = 100
    uc_checked_list_groups.btn_uncheck_all.Width = 115
    uc_checked_list_groups.btn_uncheck_all.Location = New System.Drawing.Point(117, 138)

    uc_checked_list_groups_simplified.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4928)
    uc_checked_list_groups_simplified.btn_check_all.Width = 100
    uc_checked_list_groups_simplified.btn_uncheck_all.Width = 115
    uc_checked_list_groups_simplified.btn_uncheck_all.Location = New System.Drawing.Point(117, 163)

    Me.chk_hide_no_activity.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4757)
    Me.chk_hide_no_activity_simplified.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4757)

    ' Simplified reports
    Me.opt_hourly_simplified.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8317)          '8317 "Hourly - 24h"
    Me.opt_daily_one_month_simplified.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8318) '8318 "Daily - 1 month"
    Me.opt_daily_one_day_simplified.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8319)   '8319 "Daily - 1 day"

    Me.chk_show_events_simplified.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8365)     '8365 "Show events"

    ' Terminals
    Dim _terminal_types() As Integer = Nothing

    ' The 3GS terminals must be showed only if the S2S feature is enabled
    If WSI.Common.Misc.IsWS2SEnabled Then
      ReDim Preserve _terminal_types(0 To 2)
      _terminal_types(0) = TerminalTypes.SAS_HOST
      _terminal_types(1) = TerminalTypes.OFFLINE
      _terminal_types(2) = TerminalTypes.T3GS
    Else
      ReDim Preserve _terminal_types(0 To 1)
      _terminal_types(0) = TerminalTypes.SAS_HOST
      _terminal_types(1) = TerminalTypes.OFFLINE
    End If

    ' Simplified report
    Me.gb_report_simplified.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6971) '6971 "Report"

    If m_is_center AndAlso WSI.Common.Misc.IsAGGEnabled Then
      Call Me.uc_provider.Init(_terminal_types, , , True)
      Call Me.uc_provider_simplified.Init(_terminal_types, , , True)
    Else
      Call Me.uc_provider.Init(_terminal_types)
      Call Me.uc_provider_simplified.Init(_terminal_types)
    End If

    ' Mode
    Me.gb_mode.Text = GLB_NLS_GUI_ALARMS.GetString(203)
    Me.opt_monitor_mode.Text = GLB_NLS_GUI_ALARMS.GetString(204)
    Me.opt_history_mode.Text = GLB_NLS_GUI_ALARMS.GetString(205)

    ' Currency
    Me.gb_currency.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(8800)
    Me.opt_currency.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5558)
    Me.opt_credits.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3334)

    ' Update info
    Me.tf_last_update.Text = GLB_NLS_GUI_SW_DOWNLOAD.GetString(324)
    Me.tf_last_update.Value = "0"
    Me.tf_last_update.SufixText = GLB_NLS_GUI_JACKPOT_MGR.GetString(310)
    Me.tf_last_update.Visible = False

    Call Me.uc_provider_simplified_ValueChangedEvent()

    m_all_meters = LoadMetersByGroup()

    Me.chk_terminal_location.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)
    Me.chk_terminal_location_simplified.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5237)

    Me.uc_sites_sel.Visible = m_is_center And m_form_mode = ENUM_FORM_MODE.ADVANCED_MODE
    If (uc_sites_sel.Visible) Then
      Me.GUI_InitControlsMultisite()
    End If

    If Me.m_is_center Then
      Me.gb_mode.Location = New Point(Me.gb_mode.Location.X - 90, Me.gb_mode.Location.Y)
      Me.gb_currency.Location = New Point(Me.gb_currency.Location.X - 90, Me.gb_currency.Location.Y)
      Me.tf_last_update.Location = New Point(Me.tf_last_update.Location.X - 240, Me.tf_last_update.Location.Y + 40)
    Else
      Me.gb_mode.Location = New Point(Me.gb_mode.Location.X - 10, Me.gb_mode.Location.Y)
      Me.gb_currency.Location = New Point(Me.gb_currency.Location.X - 10, Me.gb_currency.Location.Y)
    End If

    Me.tf_last_update.BringToFront()

    Call SetDefaultValues()

    Call GUI_StyleSheet()

    Me.tmr_monitor.Interval = GeneralParam.GetInt32("WigosGUI", "CageMovements.MonitorRefreshTime", METERS_REFRESH_TIME, 5, 1800) * 1000


  End Sub ' GUI_InitControls

  ' PURPOSE: Initialize all form filters with their default values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub ' GUI_FilterReset

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' Dates selection 
    If Me.dtp_from.Checked And Me.dtp_to.Checked Then
      If Me.dtp_from.Value > Me.dtp_to.Value Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.dtp_to.Focus()

        Return False
      End If
    End If

    If m_is_center Then
      If Not uc_sites_sel.FilterCheckSites() Then

        Return False
      End If
    End If

    Return True

  End Function ' GUI_FilterCheck

  ' PURPOSE : Sets initial focus
  '          
  '    - INPUT:
  '
  '    - OUTPUT:
  '              
  ' RETURNS: None
  '          
  Protected Overrides Sub GUI_SetInitialFocus()
    If Me.m_form_mode = ENUM_FORM_MODE.ADVANCED_MODE Then
      Me.ActiveControl = Me.opt_currents
    End If
  End Sub ' GUI_SetInitialFocus

  ' PURPOSE: Returns the text with provided meter types filter for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetMeterTypesFilterText() As String
    Dim _meter_types_list As List(Of String)
    _meter_types_list = New List(Of String)

    If chk_hourly.Checked Then _meter_types_list.Add(chk_hourly.Text)
    If chk_daily.Checked Then _meter_types_list.Add(chk_daily.Text)
    If chk_reset.Checked Then _meter_types_list.Add(chk_reset.Text)
    If chk_rollover.Checked Then _meter_types_list.Add(chk_rollover.Text)
    If chk_first_time.Checked Then _meter_types_list.Add(chk_first_time.Text)
    If chk_denom_change.Checked Then _meter_types_list.Add(chk_denom_change.Text)
    If Me.opt_hourly_simplified.Checked Then _meter_types_list.Add(chk_hourly.Text)
    If Me.opt_daily_one_month_simplified.Checked Then _meter_types_list.Add(chk_daily.Text)
    If Me.opt_daily_one_day_simplified.Checked Then _meter_types_list.Add(chk_daily.Text)

    Return String.Join(", ", _meter_types_list.ToArray())
  End Function

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    Dim _meter_group As String
    Dim _meter_type As String
    Dim _currency_filter As String

    m_total_columns = 0
    m_date_from = String.Empty
    m_date_to = String.Empty
    m_meter_types = String.Empty
    m_groups = String.Empty
    m_order_by = String.Empty
    _meter_group = String.Empty
    _meter_type = String.Empty
    _currency_filter = String.Empty

    If Me.m_form_mode = ENUM_FORM_MODE.ADVANCED_MODE Then
      If Me.dtp_from.Checked Then
        m_date_from = GUI_FormatDate(Me.dtp_from.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If

      If Me.dtp_to.Checked Then
        m_date_to = GUI_FormatDate(Me.dtp_to.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      End If

      ' Terminals 
      m_terminals = Me.uc_provider.GetTerminalReportText()

      ' Meter types
      If opt_currents.Checked Then
        _meter_group = String.Format("{0}", opt_currents.Text)
      Else
        _meter_group = String.Format("{0}", opt_types.Text)
      End If
    Else
      m_date_from = GUI_FormatDate(Me.dtp_from_simplified.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
      m_date_to = GUI_FormatDate(Me.dtp_to_simplified.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

      ' Terminals 
      m_terminals = Me.uc_provider_simplified.GetTerminalReportText()

      _meter_group = String.Format("{0}", opt_types.Text)
    End If

    _meter_type = GetMeterTypesFilterText()
    If (Not String.IsNullOrEmpty(_meter_type)) Then
      m_meter_types = String.Format("{0} - {1}", _meter_group, _meter_type)
    Else
      m_meter_types = _meter_group
    End If

    If Me.m_form_mode = ENUM_FORM_MODE.ADVANCED_MODE Then
      ' Groups
      If uc_checked_list_groups.SelectedIndexes.Length = uc_checked_list_groups.Count() Then
        m_groups = GLB_NLS_GUI_AUDITOR.GetString(263) 'All
      Else
        m_groups = uc_checked_list_groups.SelectedValuesList()
      End If

      ' Order By
      If opt_date.Checked Then
        m_order_by = opt_date.Text
      Else
        m_order_by = opt_terminal.Text
      End If
    Else
      ' Groups
      If uc_checked_list_groups_simplified.SelectedIndexes.Length = uc_checked_list_groups_simplified.Count() Then
        m_groups = GLB_NLS_GUI_AUDITOR.GetString(263) 'All
      Else
        m_groups = uc_checked_list_groups_simplified.SelectedValuesList()
      End If
    End If

    'Calculate total columns
    For Each _meters_group As MetersGroup In m_selected_meters
      m_total_columns += _meters_group.MeterList.Count
    Next

    If (m_is_center) Then
      m_sites = uc_sites_sel.GetSitesIdListSelectedToPrint()
    End If

    'Currency filter
    If opt_currency.Checked Then
      _currency_filter = String.Format("{0}", opt_currency.Text)
    Else
      _currency_filter = String.Format("{0}", opt_credits.Text)
    End If


  End Sub ' GUI_ReportUpdateFilters



  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ' Set specific column formats.
    ExcelData.SetColumnFormat(EXCEL_COLUMN_SITE_ID, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)

  End Sub


  ' PURPOSE: Get reports from print information and update report filters
  '
  '  PARAMS:
  '     - INPUT:
  '         - PrintData As GUI_Reports.CLASS_PRINT_DATA
  '     - OUTPUT:
  '         - None
  ' RETURNS:
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    ' Dates
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1328), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(204) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1329), m_date_to)

    ' Terminals
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)

    ' Counter types
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2419), m_meter_types)

    ' Groups
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2420), m_groups)

    'Order by
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2421), m_order_by)

    If m_is_center Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(1927), m_sites)
    End If

    'Hide terminals without activity
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4757), IIf(Me.chk_hide_no_activity.Checked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Get the defined Query Type
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - ENUM_QUERY_TYPE
  '
  Protected Overrides Function GUI_GetQueryType() As ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function ' GUI_GetQueryType

  ' PURPOSE: Executes the query with a command
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS: 
  '     - 
  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _row As DataRow
    Dim _rows() As DataRow
    Dim db_row As CLASS_DB_ROW
    Dim count As Integer
    Dim idx_row As Integer
    Dim _query_as_datatable As DataTable


    Call Me.GUI_StyleSheet()
    Call Me.GUI_BeforeFirstRow()

    _query_as_datatable = GUI_GetTableUsingCommand(GUI_GetSqlCommand(), Integer.MaxValue)

    If _query_as_datatable Is Nothing Then
      Me.Grid.Clear()
      Return
    End If

    If _query_as_datatable.Rows.Count = 0 Then
      Me.Grid.Clear()
      Return
    End If

    If (Not IsNothing(_query_as_datatable)) Then

      If (_query_as_datatable.Rows.Count > 0) Then

        _rows = FillAndSortDatatableWithTerminalData(_query_as_datatable)

        For Each _row In _rows

          Try
            db_row = New CLASS_DB_ROW(_row)

            If GUI_CheckOutRowBeforeAdd(db_row) Then
              ' Add the db row to the grid
              Me.Grid.AddRow()
              count = count + 1
              idx_row = Me.Grid.NumRows - 1

              If Not GUI_SetupRow(idx_row, db_row) Then
                ' The row can not be added
                Me.Grid.DeleteRowFast(idx_row)
                count = count - 1
              End If
            End If

          Catch ex As OutOfMemoryException
            Throw ex

          Catch exception As Exception
            Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(124), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
            Call Trace.WriteLine(exception.ToString())
            Call Common_LoggerMsg(mdl_log.ENUM_LOG_MSG.LOG_EXCEPTION_ERROR, _
                                  "frm_terminal_sas_meters_sel", _
                                  "GUI_SetupRow", _
                                  exception.Message)
            Exit For
          End Try

          db_row = Nothing

          If Not GUI_DoEvents(Me.Grid) Then
            Exit Sub
          End If

        Next
      End If
    Else
      MyBase.m_user_canceled_data_shows = True

    End If

    GUI_AfterLastRow()

  End Sub
  ' PURPOSE: Build an SQL Command query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL Command query ready to send to the database
  Protected Overrides Function GUI_GetSqlCommand() As SqlCommand

    Dim _sql_command As SqlCommand
    Dim _sb As StringBuilder
    Dim _filter_terminal As String
    Dim _filter_meter_code As String
    Dim _str_filter_site As String

    _sql_command = New SqlCommand()
    _sb = New StringBuilder()

    If opt_types.Checked Or Me.m_form_mode = ENUM_FORM_MODE.SIMPLIFIED_MODE Then

      _sql_command.CommandTimeout = 5 * 60 ' 5 minutes

      'TERMINAL_SAS_METERS_HISTORY

      _sb.AppendLine("     SELECT  (CASE WHEN TSMH_GROUP_ID IS NULL        ")
      _sb.AppendLine("                   THEN TSMH_DATETIME                ")
      _sb.AppendLine("                   ELSE TSMH_CREATED_DATETIME        ")
      _sb.AppendLine("             END                                     ")
      _sb.AppendLine("             ) AS TSMH_DATETIME                      ")
      _sb.AppendLine("           , TSMH_TERMINAL_ID                        ")
      _sb.AppendLine("           , ''               AS TE_PROVIDER         ")
      _sb.AppendLine("           , ''               AS TE_NAME             ")
      _sb.AppendLine("           , TE_ISO_CODE                             ")
      _sb.AppendLine("           , ''               AS TE_FLOOR_ID         ")
      _sb.AppendLine("           , ''               AS TE_AREA             ")
      _sb.AppendLine("           , ''               AS TE_BANK             ")
      _sb.AppendLine("           , ''               AS TE_POSITION         ")
      _sb.AppendLine("           , TSMH_TYPE                               ")
      _sb.AppendLine("           , TSMH_SAS_ACCOUNTING_DENOM               ")
      _sb.AppendLine("           , TSMH_GROUP_ID                           ")
      If Not (m_is_center) Then
        _sb.AppendLine("           , 0 as TSMH_SITE_ID                     ")
      Else
        _sb.AppendLine("           , TSMH_SITE_ID                          ")
      End If


      'Generate column for each meter
      For Each _meters_group As MetersGroup In m_selected_meters
        For Each _meter As Meter In _meters_group.MeterList
          If Me.m_form_mode = ENUM_FORM_MODE.ADVANCED_MODE Or Me.opt_currents.Checked = False Then
            _sb.AppendFormat("     , SUM(CASE WHEN (TSMH_METER_CODE = {0})    THEN TSMH_METER_INI_VALUE   ELSE NULL END)    AS A{1}{0}a ", _meter.Code, _meters_group.Id)
            _sb.AppendLine("")
            _sb.AppendFormat("     , SUM(CASE WHEN (TSMH_METER_CODE = {0})    THEN TSMH_METER_FIN_VALUE   ELSE NULL END)    AS A{1}{0}b ", _meter.Code, _meters_group.Id)
            _sb.AppendLine("")
            _sb.AppendFormat("     , SUM(CASE WHEN (TSMH_METER_CODE = {0})    THEN TSMH_METER_INCREMENT   ELSE NULL END)    AS A{1}{0}c ", _meter.Code, _meters_group.Id)
            _sb.AppendLine("")
          Else
            _sb.AppendFormat("     , SUM(CASE WHEN (TSMH_METER_CODE = {0})    THEN TSMH_METER_FIN_VALUE   ELSE NULL END)    AS A{1}{0}b ", _meter.Code, _meters_group.Id)
            _sb.AppendLine("")
            _sb.AppendFormat("     , SUM(CASE WHEN (TSMH_METER_CODE = {0})    THEN TSMH_METER_INCREMENT   ELSE NULL END)    AS A{1}{0}c ", _meter.Code, _meters_group.Id)
            _sb.AppendLine("")
          End If
        Next
      Next

      _sb.AppendLine("  FROM TERMINAL_SAS_METERS_HISTORY WITH(INDEX(IX_tsmh_datetime_type))               ")
      _sb.AppendLine("  LEFT JOIN  TERMINALS  ON TE_TERMINAL_ID = TSMH_TERMINAL_ID                        ")  ' LTC 25-ABR-2017
      _sb.AppendLine(GetSqlWhereMeters())

      If (m_is_center) Then
        _sb.AppendLine(String.Format("   AND TE_SITE_ID = TSMH_SITE_ID "))
        _str_filter_site = uc_sites_sel.GetSitesIdListSelected()

        If (_str_filter_site <> String.Empty) Then
          _sb.AppendLine(String.Format("   AND  TSMH_SITE_ID IN ({0})", _str_filter_site))
        End If
      End If

      If m_selected_meters.Count > 0 Then
        _filter_meter_code = "AND TSMH_METER_CODE IN  ( "
        For Each _meters_group As MetersGroup In m_selected_meters
          For Each _meter As Meter In _meters_group.MeterList
            _filter_meter_code += _meter.Code.ToString() + ", "
          Next
        Next
        _filter_meter_code = _filter_meter_code.Substring(0, _filter_meter_code.Length - 2) + " ) "
        _sb.AppendLine(_filter_meter_code)
      End If

      _sb.AppendLine(GetSqlAndTerminals())
      _sb.AppendLine("   GROUP BY                      ")
      If (m_is_center) Then
        _sb.AppendLine("  TSMH_SITE_ID ")
        _sb.AppendLine("           , ")
      End If
      _sb.AppendLine("             (CASE WHEN TSMH_GROUP_ID IS NULL ")
      _sb.AppendLine("                   THEN TSMH_DATETIME         ")
      _sb.AppendLine("                   ELSE TSMH_CREATED_DATETIME ")
      _sb.AppendLine("             END)                             ")
      _sb.AppendLine("           , TSMH_TERMINAL_ID, TSMH_TYPE, TSMH_SAS_ACCOUNTING_DENOM, TE_ISO_CODE, TSMH_GROUP_ID ")
    Else
      ' TERMINAL_SAS_METERS

      _sb.AppendLine("    SELECT   MAX(TSM_LAST_REPORTED) AS TSM_DATETIME ")
      _sb.AppendLine("           , TSM_TERMINAL_ID ")
      _sb.AppendLine("           , '' AS TE_PROVIDER ")
      _sb.AppendLine("           , '' AS TE_NAME ")
      _sb.AppendLine("           , TE_ISO_CODE ")
      _sb.AppendLine("           , '' AS TE_FLOOR_ID ")
      _sb.AppendLine("           , '' AS TE_AREA")
      _sb.AppendLine("           , '' AS TE_BANK")
      _sb.AppendLine("           , '' AS TE_POSITION")
      _sb.AppendLine("           , 0 AS TSMH_TYPE ")
      _sb.AppendLine("           , TSM_SAS_ACCOUNTING_DENOM ")
      _sb.AppendLine("           , 0 AS TSMH_GROUP_ID ")
      If Not (m_is_center) Then
        _sb.AppendLine("           , 0 as TSM_SITE_ID            ")
      Else
        _sb.AppendLine("           , TSM_SITE_ID                  ")
      End If

      'Generate column for each meter
      For Each _meters_group As MetersGroup In m_selected_meters
        For Each _meter As Meter In _meters_group.MeterList
          _sb.AppendFormat("     , SUM(CASE WHEN (TSM_METER_CODE = {0})     THEN TSM_METER_VALUE        ELSE NULL END)    AS A{1}{0}b ", _meter.Code, _meters_group.Id)
          _sb.AppendLine("")
          _sb.AppendFormat("     , SUM(CASE WHEN (TSM_METER_CODE = {0})     THEN TSM_DELTA_VALUE        ELSE NULL END)    AS A{1}{0}c ", _meter.Code, _meters_group.Id)
          _sb.AppendLine("")
        Next
      Next

      _sb.AppendLine("       FROM  TERMINAL_SAS_METERS ")

      If Not m_is_center Then
        _sb.AppendLine(" INNER JOIN  TERMINALS ON TERMINALS.TE_TERMINAL_ID = TERMINAL_SAS_METERS.TSM_TERMINAL_ID")
      Else
        _str_filter_site = uc_sites_sel.GetSitesIdListSelected()

        If (_str_filter_site <> String.Empty) Then
          _sb.AppendLine(String.Format(" INNER JOIN  TERMINALS ON TERMINALS.TE_TERMINAL_ID = TERMINAL_SAS_METERS.TSM_TERMINAL_ID "))
          _sb.AppendLine(String.Format("                      AND TERMINALS.TE_SITE_ID = TERMINAL_SAS_METERS.TSM_SITE_ID "))
          _sb.AppendLine(String.Format("                      AND TE_SITE_ID IN ({0})", _str_filter_site))
        End If
      End If
      _sb.AppendLine(GetSqlWhereMeters())

      ' Terminal filter
      _filter_terminal = Me.GetTerminalsFilter()
      If (Not String.IsNullOrEmpty(_filter_terminal)) Then
        _sb.AppendLine(String.Format("        AND   {0} ", _filter_terminal))
      End If

      If (m_is_center) Then
        _str_filter_site = uc_sites_sel.GetSitesIdListSelected()

        If (_str_filter_site <> String.Empty) Then
          _sb.AppendLine(String.Format("        AND  TSM_SITE_ID IN ({0})", _str_filter_site))
        End If
      End If

      _sb.AppendLine("  GROUP BY ")
      If (m_is_center) Then
        _sb.AppendLine("  TSM_SITE_ID ")
        _sb.AppendLine("           , ")
      End If
      _sb.Append("TSM_TERMINAL_ID          ")
      _sb.AppendLine("           , TSM_SAS_ACCOUNTING_DENOM ")
      _sb.AppendLine("           , TE_ISO_CODE              ")
    End If

    _sql_command.CommandText = _sb.ToString()

    Return _sql_command
  End Function ' GUI_GetSqlCommand

  ''' <summary>
  ''' Checks if we have to add the row if we are showing by currents meters
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckIfAddRowByCurrents(ByVal DbRow As CLASS_DB_ROW) As Boolean
    Dim _add_row As Boolean
    Dim _sql_column_index As Integer
    Dim _first_meter_column As Integer

    If Me.m_form_mode = ENUM_FORM_MODE.ADVANCED_MODE Then
      _first_meter_column = SQL_FIRST_METER_COLUMN_ADVANCED
    Else
      _first_meter_column = SQL_FIRST_METER_COLUMN_SIMPLIFIED
    End If

    _add_row = False

    For _idx_column As Integer = 0 To (m_total_columns * m_multiplier_columns) - 1
      _sql_column_index = _first_meter_column + (_idx_column * m_multiplier_columns)

      If (Not DbRow.IsNull(_sql_column_index)) AndAlso DbRow.Value(_sql_column_index) > 0 Then
        'opt_currents only has a column (current)
        _add_row = True
        Exit For
      End If
    Next

    Return _add_row
  End Function

  ''' <summary>
  ''' Checks if we have to add the row if we are showing by type meter
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckIfAddRowByType(ByVal DbRow As CLASS_DB_ROW) As Boolean
    Dim _add_row As Boolean
    Dim _grid_column_index As Integer
    Dim _sql_column_index As Integer
    Dim _increment_header_text As String
    Dim _meters_type As ENUM_SAS_METER_HISTORY

    _add_row = False
    _increment_header_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4796) '4796 -> Increment header
    _meters_type = DbRow.Value(SQL_COLUMN_METERS_TYPE)

    If _meters_type <> ENUM_SAS_METER_HISTORY.TSMH_HOURLY And _meters_type <> ENUM_SAS_METER_HISTORY.TSMH_MINCETUR_DAILY_REPORT Then
      Return True
    End If

    For _idx_column As Integer = 0 To (m_total_columns * m_multiplier_columns) - 1
      _sql_column_index = SQL_FIRST_METER_COLUMN_ADVANCED + _idx_column

      If (Not DbRow.IsNull(_sql_column_index)) AndAlso DbRow.Value(_sql_column_index) > 0 Then
        _grid_column_index = GRID_COLUMNS_COUNT + _idx_column

        'If column it is a column increment and has value, add row
        If Grid.Column(_grid_column_index).Header(1).Text = _increment_header_text Then
          _add_row = True
          Exit For
        End If
      End If
    Next

    Return _add_row
  End Function

  ''' <summary>
  ''' Returns if thr have to add the row to the grid
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function CheckIfAddRow(ByVal DbRow As CLASS_DB_ROW) As Boolean
    Dim _add_row As Boolean

    If (opt_currents.Checked) Then
      _add_row = CheckIfAddRowByCurrents(DbRow)
    Else
      _add_row = CheckIfAddRowByType(DbRow)
    End If

    Return _add_row
  End Function

  ''' <summary>
  ''' Select format of currency
  ''' </summary>
  ''' <param name="IsCenter"></param>
  ''' <param name="Value"></param>
  ''' <returns></returns>
  ''' <remarks></remarks>
  Private Function SelectTypeCurrency(ByVal IsCenter As Boolean, ByVal Value As Double) As String
    If IsCenter = True Then
      Return GUI_MultiCurrencyValue_Format(Value)
    Else
      Return GUI_FormatCurrency(Value, 2, ENUM_GROUP_DIGITS.GROUP_DIGITS_TRUE)
    End If
  End Function ' SelectTypeCurrency

  ' PURPOSE : Checks out the DB row before adding it to the grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - DataRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the data row can be added) or False (the data row can not be added)
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean
    Dim _add_row As Boolean

    _add_row = False

    Try
      Select Case DbRow.Value(SQL_COLUMN_METERS_TYPE)

        Case ENUM_SAS_METER_HISTORY.TSMH_METER_DISCARDED _
           , ENUM_SAS_METER_HISTORY.TSMH_METER_FIRST_TIME _
           , ENUM_SAS_METER_HISTORY.TSMH_METER_RESET _
           , ENUM_SAS_METER_HISTORY.TSMH_METER_ROLLOVER _
           , ENUM_SAS_METER_HISTORY.TSMH_METER_SAS_ACCOUNT_DENOM_CHANGE

          _add_row = True

        Case Else
          If (chk_hide_no_activity.Checked And Me.m_form_mode = ENUM_FORM_MODE.ADVANCED_MODE) _
             Or (chk_hide_no_activity_simplified.Checked And Me.m_form_mode = ENUM_FORM_MODE.SIMPLIFIED_MODE) Then
            _add_row = CheckIfAddRow(DbRow)
          Else
            _add_row = True
          End If
      End Select

    Catch _ex As Exception
      Log.Exception(_ex)
      _add_row = False
    End Try

    Return _add_row
  End Function ' GUI_CheckOutRowBeforeAdd

  ' PURPOSE: Call GUI_ButtonClick with the Cursor in Wait Mode. Finally restore Cursor to Default.
  '
  '  PARAMS:
  '     - INPUT:
  '           - ButtonId As ENUM_BUTTON
  '     - OUTPUT:
  '
  ' RETURNS:
  '     -
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call GUI_StyleSheet()
        GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Enabled = False
        GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Enabled = False
    End Select

    Call MyBase.GUI_ButtonClick(ButtonId)

  End Sub ' GUI_ButtonClick

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : True (the row should be added) or False (the row can not be added)
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _grid_column_index As Integer
    Dim _sql_column_index As Integer
    Dim _empty_history_meter As Boolean
    Dim _meters_type As ENUM_SAS_METER_HISTORY

    _grid_column_index = GRID_COLUMNS_COUNT
    _sql_column_index = GRID_COLUMNS_COUNT + (SQL_OFFSET_METER_COLUMNS - TERMINAL_DATA_COLUMNS)

    If (IsDBNull(DbRow.Value(SQL_COLUMN_TERMINAL_ID))) Then
      Return False
    End If

    _meters_type = DbRow.Value(SQL_COLUMN_METERS_TYPE)

    'site
    Me.Grid.Cell(RowIndex, GRID_COLUMN_SITE).Value = String.Format("{0:000}", DbRow.Value(SQL_COLUMN_SITE))

    ' Date
    Me.Grid.Cell(RowIndex, GRID_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    ' Meters Type
    Me.Grid.Cell(RowIndex, GRID_COLUMN_METERS_TYPE).Value = GetMetersTypeAsString(_meters_type)

    ' Terminal Id
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ID).Value = DbRow.Value(SQL_COLUMN_TERMINAL_ID)

    ' Terminal Provider
    If DbRow.Value(SQL_COLUMN_TERMINAL_PROVIDER).Equals(DBNull.Value) Then : Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_PROVIDER).Value = String.Empty : Else : Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_PROVIDER).Value = DbRow.Value(SQL_COLUMN_TERMINAL_PROVIDER) : End If

    ' Terminal Name
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_NAME).Value = DbRow.Value(SQL_COLUMN_TERMINAL_NAME)

    ' Terminal Floor
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_FLOOR).Value = DbRow.Value(SQL_COLUMN_TERMINAL_FLOOR)

    ' Accounting denomination
    Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_ACCOUNT_DENOMINATION).Value = IIf(String.IsNullOrEmpty(DbRow.Value(SQL_COLUMN_TERMINAL_ACCOUNT_DENOMINATION).ToString()) _
                                                                                  OrElse DbRow.Value(SQL_COLUMN_TERMINAL_ACCOUNT_DENOMINATION) <= 0,
                                                                                  AUDIT_NONE_STRING, String.Format("{0:N2}", DbRow.Value(SQL_COLUMN_TERMINAL_ACCOUNT_DENOMINATION)) & " " & DbRow.Value(SQL_COLUMN_TERMINAL_ISO_CODE))

    If (chk_terminal_location.Checked And Me.m_form_mode = ENUM_FORM_MODE.ADVANCED_MODE) _
      Or (chk_terminal_location_simplified.Checked And Me.m_form_mode = ENUM_FORM_MODE.SIMPLIFIED_MODE) Then
      'LOCATION
      ' Area
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_AREA).Value = DbRow.Value(SQL_COLUMN_TERMINAL_AREA)

      ' Bank
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_BANK).Value = DbRow.Value(SQL_COLUMN_TERMINAL_BANK)

      ' Position
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL_POSITION).Value = Convert.ToString(DbRow.Value(SQL_COLUMN_TERMINAL_POSITION))

    End If

    'Counters
    For Each _meters_group As MetersGroup In m_selected_meters
      For Each _meter As Meter In _meters_group.MeterList
        _empty_history_meter = True
        ' Is Null?
        If (Not IsDBNull(DbRow.Value(_sql_column_index))) AndAlso (Not IsNothing(DbRow.Value(_sql_column_index))) Then
          'if sas meter is first time and its value is 0 the cell must be shown in blank
          If _meters_type <> ENUM_SAS_METER_HISTORY.TSMH_METER_FIRST_TIME OrElse _
             (_meters_type = ENUM_SAS_METER_HISTORY.TSMH_METER_FIRST_TIME AndAlso DbRow.Value(_sql_column_index) <> 0) Then
            _empty_history_meter = False
            'currency mode
            If (Me.opt_currency.Checked Or Me.m_form_mode = ENUM_FORM_MODE.SIMPLIFIED_MODE) AndAlso Not Me.opt_credits.Checked Then
              If (Not IsDBNull(DbRow.Value(SQL_COLUMN_TERMINAL_ACCOUNT_DENOMINATION))) Then
                Me.Grid.Cell(RowIndex, _grid_column_index).Value = FormatMeterValue(_meter.Code, DbRow.Value(_sql_column_index), DbRow.Value(SQL_COLUMN_TERMINAL_ACCOUNT_DENOMINATION)) 'currency
              Else
                Me.Grid.Cell(RowIndex, _grid_column_index).Value = FormatMeterValue(_meter.Code, DbRow.Value(_sql_column_index), -1.0) 'currency
              End If
              ' credits mode
            ElseIf Not Me.opt_currency.Checked AndAlso Me.opt_credits.Checked Then
              Me.Grid.Cell(RowIndex, _grid_column_index).Value = DbRow.Value(_sql_column_index) 'credits
            End If

          End If
        End If

        If opt_currents.Checked Then

          _grid_column_index += 1
          _sql_column_index += 1
          ' Is Null?
          If (Not IsDBNull(DbRow.Value(_sql_column_index))) AndAlso (Not IsNothing(DbRow.Value(_sql_column_index))) Then
            'if sas meter is first time and its value is 0 the cell must be shown in blank
            _empty_history_meter = False

            ' currency mode
            If Me.opt_currency.Checked AndAlso Not Me.opt_credits.Checked Then
              If (Not IsDBNull(DbRow.Value(SQL_COLUMN_TERMINAL_ACCOUNT_DENOMINATION))) Then
                Me.Grid.Cell(RowIndex, _grid_column_index).Value = FormatMeterValue(_meter.Code, DbRow.Value(_sql_column_index), DbRow.Value(SQL_COLUMN_TERMINAL_ACCOUNT_DENOMINATION)) 'currency
              Else
                Me.Grid.Cell(RowIndex, _grid_column_index).Value = FormatMeterValue(_meter.Code, DbRow.Value(_sql_column_index), -1.0) 'currency
              End If
              ' credits mode
            ElseIf Not Me.opt_currency.Checked AndAlso Me.opt_credits.Checked Then
              Me.Grid.Cell(RowIndex, _grid_column_index).Value = DbRow.Value(_sql_column_index) 'credits
            End If

          End If

        Else
          _grid_column_index += 1
          _sql_column_index += 1
          ' Is Null?
          If (Not IsDBNull(DbRow.Value(_sql_column_index))) AndAlso (Not IsNothing(DbRow.Value(_sql_column_index))) Then
            'if sas meter is first time and its value is 0 the cell must be shown in blank
            _empty_history_meter = False

            ' currency mode
            If Me.opt_currency.Checked AndAlso Not Me.opt_credits.Checked Then
              If (Not IsDBNull(DbRow.Value(SQL_COLUMN_TERMINAL_ACCOUNT_DENOMINATION))) Then
                Me.Grid.Cell(RowIndex, _grid_column_index).Value = FormatMeterValue(_meter.Code, DbRow.Value(_sql_column_index), DbRow.Value(SQL_COLUMN_TERMINAL_ACCOUNT_DENOMINATION)) 'currency
              Else
                Me.Grid.Cell(RowIndex, _grid_column_index).Value = FormatMeterValue(_meter.Code, DbRow.Value(_sql_column_index), -1.0) 'currency
              End If
              ' credits mode
            ElseIf Not Me.opt_currency.Checked AndAlso Me.opt_credits.Checked Then
              Me.Grid.Cell(RowIndex, _grid_column_index).Value = DbRow.Value(_sql_column_index) 'credits
            End If

            'Me.Grid.Cell(RowIndex, _grid_column_index).Value = FormatMeterValue(_meter.Code, DbRow.Value(_sql_column_index))  
          End If
          _grid_column_index += 1
          _sql_column_index += 1
          ' Is Null?
          If (Not IsDBNull(DbRow.Value(_sql_column_index))) AndAlso (Not IsNothing(DbRow.Value(_sql_column_index))) Then
            'if sas meter is 'first_time' and its value is 0 the cell must be shown in blank
            If _meters_type <> ENUM_SAS_METER_HISTORY.TSMH_METER_FIRST_TIME OrElse _
             (_meters_type = ENUM_SAS_METER_HISTORY.TSMH_METER_FIRST_TIME AndAlso DbRow.Value(_sql_column_index) <> 0) Then
              _empty_history_meter = False
              'currency mode 
              If Me.opt_currency.Checked AndAlso Not Me.opt_credits.Checked Then
                If (Not IsDBNull(DbRow.Value(SQL_COLUMN_TERMINAL_ACCOUNT_DENOMINATION))) Then
                  Me.Grid.Cell(RowIndex, _grid_column_index).Value = FormatMeterValue(_meter.Code, DbRow.Value(_sql_column_index), DbRow.Value(SQL_COLUMN_TERMINAL_ACCOUNT_DENOMINATION)) 'currency
                Else
                  Me.Grid.Cell(RowIndex, _grid_column_index).Value = FormatMeterValue(_meter.Code, DbRow.Value(_sql_column_index), -1.0) 'currency
                End If
                ' credits mode
              ElseIf Not Me.opt_currency.Checked AndAlso Me.opt_credits.Checked Then
                Me.Grid.Cell(RowIndex, _grid_column_index).Value = DbRow.Value(_sql_column_index) 'credits
              End If

              'Me.Grid.Cell(RowIndex, _grid_column_index).Value = FormatMeterValue(_meter.Code, DbRow.Value(_sql_column_index))  
            End If
          End If

          'SUM Total
          If (Not IsDBNull(DbRow.Value(_sql_column_index))) Then
            Me.m_sum_total(_grid_column_index - GRID_COLUMNS_COUNT) += DbRow.Value(_sql_column_index)
          End If

        End If

        _grid_column_index += 1
        _sql_column_index += 1
      Next
    Next

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Print totalizator row in the data grid
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    If Not opt_currents.Checked And Me.m_form_mode = ENUM_FORM_MODE.SIMPLIFIED_MODE And opt_daily_one_month_simplified.Checked Then
      Dim _idx_row As Integer
      Dim _column_index As Integer

      _column_index = GRID_COLUMNS_COUNT + 2
      _idx_row = Me.Grid.NumRows

      Me.Grid.AddRow()

      ' Label - TOTAL:
      Me.Grid.Cell(_idx_row, GRID_COLUMN_DATE).Value = GLB_NLS_GUI_INVOICING.GetString(205)

      ' Total - Points
      'Counters
      For Each _meters_group As MetersGroup In m_selected_meters
        For Each _meter As Meter In _meters_group.MeterList

          If Me.opt_currency.Checked And Not Me.opt_credits.Checked Then
            Me.Grid.Cell(_idx_row, _column_index).Value = FormatMeterValue(_meter.Code, Me.m_sum_total(_column_index - GRID_COLUMNS_COUNT), -1.0)
          ElseIf Me.opt_credits.Checked And Not Me.opt_currency.Checked Then
            Me.Grid.Cell(_idx_row, _column_index).Value = Me.m_sum_total(_column_index - GRID_COLUMNS_COUNT)
          End If

          _column_index += 3
        Next
      Next

      ' Color Row
      Me.Grid.Row(_idx_row).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)
    End If
  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Set the tool tip text for a given row and column
  '
  '  PARAMS:
  '     - INPUT:
  '           - RowIndex As Integer
  '           - ColIndex As Integer
  '     - OUTPUT:
  '           - ToolTipTxt As String
  '
  ' RETURNS:
  Protected Overrides Sub GUI_SetToolTipText(ByVal RowIndex As Integer, _
                                             ByVal ColIndex As Integer, _
                                             ByRef ToolTipTxt As String)

    If ColIndex < GRID_COLUMNS_COUNT Then

      Return
    End If

    ToolTipTxt = m_tool_tip(ColIndex - GRID_COLUMNS_COUNT)

    If RowIndex = -3 Then
      ToolTipTxt = ToolTipTxt.Replace(Grid.Column(ColIndex).Header(1).Text, "").Trim()
    End If

  End Sub ' GUI_SetToolTipText

#End Region

#Region "Public Functions"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - MdiParent
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    ShowForEdit(MdiParent, ENUM_FORM_MODE.ADVANCED_MODE)

  End Sub ' ShowForEdit

  ' PURPOSE: Opens dialog with default settings for edit mode choosing between standard mode & simplified mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - MdiParent
  '           - FormMode
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None

  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, ByVal FormMode As ENUM_FORM_MODE)

    Me.m_form_mode = FormMode

    m_is_center = GeneralParam.GetBoolean("MultiSite", "IsCenter", False)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

  Function DeDupeString(ByVal sInput As String, Optional ByVal sDelimiter As String = ",") As String

    Dim varSection As Object
    Dim sTemp As String

    'For Each varSection In Split(sInput, sDelimiter)
    For Each varSection In Strings.Split(sInput, sDelimiter)
      If InStr(1, sDelimiter & sTemp & sDelimiter, sDelimiter & varSection & sDelimiter, vbTextCompare) = 0 Then
        sTemp = sTemp & sDelimiter & varSection
      End If
    Next varSection

    DeDupeString = Mid(sTemp, Len(sDelimiter) + 1)

  End Function

#End Region

#Region "Private Functions"

  ' PURPOSE: Sets the defaul date values to the daily_session_selector control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Dim _today_opening As Date

    _today_opening = WSI.Common.Misc.TodayOpening()

    ' Dates
    dtp_from.Value = _today_opening
    dtp_from_simplified.Value = _today_opening
    dtp_to.Value = _today_opening.AddDays(1)
    dtp_to_simplified.Value = _today_opening.AddDays(1)

    gb_date.Enabled = False
    dtp_from.Checked = False
    dtp_to.Checked = False

    dtp_to_simplified.Enabled = False

    ' Order By
    Me.opt_terminal.Checked = True

    chk_daily.Checked = False
    chk_daily.Enabled = False
    chk_hourly.Checked = False
    chk_hourly.Enabled = False
    chk_reset.Checked = False
    chk_reset.Enabled = False
    chk_rollover.Checked = False
    chk_rollover.Enabled = False
    chk_first_time.Checked = False
    chk_first_time.Enabled = False
    chk_denom_change.Checked = False
    chk_denom_change.Enabled = False
    Me.chk_hide_no_activity.Checked = False
    Me.chk_hide_no_activity_simplified.Checked = False
    Me.opt_history_mode.Checked = True

    ' Terminals
    Call Me.uc_provider.SetDefaultValues()
    Call Me.uc_provider_simplified.SetDefaultValues()

    ' Groups
    Call uc_checked_list_groups.Clear()
    Call uc_checked_list_groups_simplified.Clear()
    Call FillGroups()

    Call Me.uc_checked_list_groups.SetDefaultValue(False)
    Call Me.uc_checked_list_groups.SetValuesByArray(SetDefaultGroups())
    Call Me.uc_checked_list_groups.CollapseAll()

    Call Me.uc_checked_list_groups_simplified.SetDefaultValue(False)
    Call Me.uc_checked_list_groups_simplified.SetValuesByArray(SetDefaultGroups())
    Call Me.uc_checked_list_groups_simplified.CollapseAll()

    Me.chk_terminal_location.Checked = False
    Me.chk_terminal_location_simplified.Checked = False
    Me.chk_show_events_simplified.Checked = False
    Me.opt_currents.Checked = True

    If Me.m_form_mode = ENUM_FORM_MODE.SIMPLIFIED_MODE Then
      Me.opt_types.Checked = True
      Me.opt_hourly_simplified.Checked = False
      Me.opt_daily_one_month_simplified.Checked = False
      Me.opt_daily_one_day_simplified.Checked = True
      Me.chk_hourly.Checked = False
      Me.chk_daily.Checked = True
    Else
      ' Counters
      gb_date.Enabled = False

      Call Me.uc_provider_simplified_ValueChangedEvent()
    End If

    Me.uc_sites_sel.SetDefaultValues(False)


    If WSI.Common.Misc.IsAGGEnabled() AndAlso m_is_center Then
      'Set default value of uc_sites_sel here!
      Dim _dt_sites As DataTable = Me.uc_sites_sel.GetSitesDataTable()
      If Not _dt_sites Is Nothing AndAlso Not _dt_sites.Rows.Count = 0 Then
        Me.uc_sites_sel.SetSitesIdListSelected(_dt_sites.Rows(0).Item(SQL_COLUMN_SITE_ID))
      End If
    End If

    If WSI.Common.Misc.IsAGGEnabled() Then
      gb_currency.Visible = True
      opt_credits.Checked = True
      opt_currency.Checked = False

    Else
      gb_currency.Visible = False
      opt_credits.Checked = False
      opt_currency.Checked = True
    End If


  End Sub ' SetDefaultValues


  ' PURPOSE: Sets the default meter groups by system mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - Default groups 
  Private Function SetDefaultGroups() As Integer()
    Dim _defaultGroups As List(Of Integer)
    _defaultGroups = New List(Of Integer)

    Select Case WSI.Common.Misc.SystemMode()

      Case SYSTEM_MODE.CASHLESS
        _defaultGroups.Add(10004) ' Played - Won

      Case SYSTEM_MODE.TITO  'Tito Mode
        _defaultGroups.Add(10001) ' Tickets
        _defaultGroups.Add(10002) ' Bills And Coins
        _defaultGroups.Add(10004) ' Played - Won

      Case SYSTEM_MODE.GAMINGHALL 'Gaming Hall Mode
        _defaultGroups.Add(10005) ' Gaming Hall

      Case Else
        Log.Warning("SetDefaultGroups: Not Group Selected")

    End Select

    Return _defaultGroups.ToArray()
  End Function

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()
    Dim _str_sql As StringBuilder
    Dim _dt_counters As DataTable
    Dim _lst_counters As List(Of String)
    Dim _total_counters As Integer
    Dim _column_index As Integer
    Dim _group_prefix As String
    Dim _large_description As StringBuilder
    Dim _dummy As Boolean
    Dim _header0_width As Int32
    Dim _header1_width As Int32
    Dim _terminal_columns As List(Of ColumnSettings)
    Dim NoDupes As String

    Call GridColumnReIndex()

    m_selected_meters = New List(Of MetersGroup)

    _column_index = GRID_COLUMNS_COUNT
    _str_sql = New StringBuilder()
    _dt_counters = New DataTable()
    _total_counters = 0
    _lst_counters = New List(Of String)

    If Me.m_form_mode = ENUM_FORM_MODE.ADVANCED_MODE Then
      If uc_checked_list_groups.SelectedIndexes.Length > 0 Then
        NoDupes = DeDupeString(uc_checked_list_groups.SelectedIndexesListLevel2)
        m_selected_meters = SelectMetersByGroup(m_all_meters, uc_checked_list_groups.SelectedIndexesList, NoDupes)
      End If
    Else
      If uc_checked_list_groups_simplified.SelectedIndexes.Length > 0 Then
        m_selected_meters = SelectMetersByGroup(m_all_meters, uc_checked_list_groups_simplified.SelectedIndexesList, uc_checked_list_groups_simplified.SelectedIndexesListLevel2)
      End If
    End If

    For Each _meters_group As MetersGroup In m_selected_meters
      _total_counters = _total_counters + _meters_group.MeterList.Count
    Next

    m_sum_total = New List(Of Double)
    If opt_currents.Checked Then
      m_multiplier_columns = 2
    Else
      m_multiplier_columns = 3
    End If

    m_tool_tip = New List(Of String)

    With Me.Grid
      Call .Init(GRID_COLUMNS_COUNT + _total_counters * m_multiplier_columns, GRID_HEADERS_COUNT)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      .Column(GRID_COLUMN_SELECT).Header(0).Text = ""
      .Column(GRID_COLUMN_SELECT).Header(1).Text = ""
      .Column(GRID_COLUMN_SELECT).Width = GRID_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False
      .Column(GRID_COLUMN_SELECT).HighLightWhenSelected = False

      If Not (m_is_center) Then
        .Column(GRID_COLUMN_SITE).Header(0).Text = ""
        .Column(GRID_COLUMN_SITE).Header(1).Text = ""
        .Column(GRID_COLUMN_SITE).Width = 10
        .Column(GRID_COLUMN_SITE).HighLightWhenSelected = False
        .Column(GRID_COLUMN_SITE).IsColumnPrintable = False
      Else
        .Column(GRID_COLUMN_SITE).Header(0).Text = ""
        .Column(GRID_COLUMN_SITE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1927) ' SiteId
        .Column(GRID_COLUMN_SITE).Width = GRID_WIDTH_SITE
        .Column(GRID_COLUMN_SITE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      End If

      ' Date
      .Column(GRID_COLUMN_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3339)
      If opt_types.Checked Then
        .Column(GRID_COLUMN_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(268)
      Else
        .Column(GRID_COLUMN_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1754)
      End If
      .Column(GRID_COLUMN_DATE).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Meters Type
      .Column(GRID_COLUMN_METERS_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3339)
      .Column(GRID_COLUMN_METERS_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254)
      .Column(GRID_COLUMN_METERS_TYPE).Width = IIf(opt_types.Checked Or chk_show_events_simplified.Checked, GRID_WIDTH_METERS_TYPE, 0)
      .Column(GRID_COLUMN_METERS_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Terminal Id
      .Column(GRID_COLUMN_TERMINAL_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1968) ' Terminal
      .Column(GRID_COLUMN_TERMINAL_ID).Header(1).Text = "" ' GLB_NLS_GUI_PLAYER_TRACKING.GetString(2110)
      .Column(GRID_COLUMN_TERMINAL_ID).Width = 0 'GRID_WIDTH_TERMINAL_ID
      .Column(GRID_COLUMN_TERMINAL_ID).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      '  Terminal Report
      _terminal_columns = TerminalReport.GetColumnStyles(m_terminal_report_type)
      For _idx As Int32 = 0 To _terminal_columns.Count - 1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1968) ' Terminal
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Header(1).Text = _terminal_columns(_idx).Header1
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Width = _terminal_columns(_idx).Width
        .Column(GRID_INIT_TERMINAL_DATA + _idx).Alignment = _terminal_columns(_idx).Alignment
      Next

      'Counters
      _dummy = True
      Dim g As Graphics

      For Each _meters_group As MetersGroup In m_selected_meters
        For Each _meter As Meter In _meters_group.MeterList
          _group_prefix = _meters_group.Type
          _large_description = New StringBuilder()
          _large_description.AppendLine(_meter.CatalogName)
          _large_description.AppendLine(_group_prefix & " " & VisibleMeterCode(_meter.HexCode))
          _large_description.AppendLine("SAS: " & _meter.Description)
          _header0_width = (WIDTH_START_VALUE * _meter.CatalogName.Length) / LENGHT_START_VALUE 'the header0 width is calculated
          _header0_width = Math.Max(GRID_WIDTH_COUNTER_TYPE, _header0_width)

          If opt_currents.Checked Then
            .Column(_column_index).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4793) 'Current
            If _dummy Then
              .Column(_column_index).Header(1).Text &= " "
            End If
            _dummy = Not _dummy
            .Column(_column_index).Header(0).Text = _meter.CatalogName
            m_tool_tip.Add(_large_description.ToString())
            g = Me.CreateGraphics()
            .Column(_column_index).Width = _header0_width
            .Column(_column_index).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
            .Column(_column_index).Header(0).ExcelAlignment = uc_grid.CLASS_CELL_DATA.ENUM_ALIGN.ALIGN_LEFT
            _column_index += 1

            ' Increment
            .Column(_column_index).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4796) ' Increment
            .Column(_column_index).Header(0).Text = _meter.CatalogName
            m_tool_tip.Add(_large_description.ToString())
            .Column(_column_index).Width = 0
            .Column(_column_index).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
            .Column(_column_index).Header(0).ExcelAlignment = uc_grid.CLASS_CELL_DATA.ENUM_ALIGN.ALIGN_LEFT
            _column_index += 1

          Else
            _header1_width = Math.Max(GRID_WIDTH_COUNTER_TYPE, _header0_width / 3) ' max between header0 / 3 and header 1
            ' Initial
            .Column(_column_index).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4794) ' Initial
            .Column(_column_index).Header(0).Text = _meter.CatalogName '
            m_tool_tip.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4794) & " " & _large_description.ToString())
            .Column(_column_index).Width = _header1_width
            .Column(_column_index).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
            .Column(_column_index).Header(0).ExcelAlignment = uc_grid.CLASS_CELL_DATA.ENUM_ALIGN.ALIGN_LEFT
            _column_index += 1

            ' Final
            .Column(_column_index).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4795) ' Final
            .Column(_column_index).Header(0).Text = _meter.CatalogName
            m_tool_tip.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4795) & " " & _large_description.ToString())
            .Column(_column_index).Width = _header1_width
            .Column(_column_index).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
            .Column(_column_index).Header(0).ExcelAlignment = uc_grid.CLASS_CELL_DATA.ENUM_ALIGN.ALIGN_LEFT
            _column_index += 1

            ' Increment
            .Column(_column_index).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4796) ' Increment
            .Column(_column_index).Header(0).Text = _meter.CatalogName
            m_tool_tip.Add(GLB_NLS_GUI_PLAYER_TRACKING.GetString(4796) & " " & _large_description.ToString())
            .Column(_column_index).Width = _header1_width
            .Column(_column_index).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
            .Column(_column_index).Header(0).ExcelAlignment = uc_grid.CLASS_CELL_DATA.ENUM_ALIGN.ALIGN_LEFT
            _column_index += 1

            ' Add registers for totalize
            m_sum_total.Add(0)
            m_sum_total.Add(0)
            m_sum_total.Add(0)
          End If
        Next
      Next

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Gets Meter Type filter
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT: new filter
  '
  ' RETURNS:
  '
  Private Function GetMeterTypeFilter() As String
    Dim _lst_meter_types As List(Of String)
    Dim _str_meter_types As String
    Dim _str_where As String
    Dim _gp_show_extended_meter_type As Integer

    _lst_meter_types = New List(Of String)
    _str_meter_types = String.Empty
    _str_where = String.Empty

    ' Get Mask that says which meter type to display
    _gp_show_extended_meter_type = GeneralParam.GetInt64("AFIPClient", "ShowExtendedMeterType", 3)

    ' Meter type
    If chk_hourly.Checked Then _lst_meter_types.Add(CType(ENUM_SAS_METER_HISTORY.TSMH_HOURLY, String))
    If chk_daily.Checked Then _lst_meter_types.Add(CType(ENUM_SAS_METER_HISTORY.TSMH_MINCETUR_DAILY_REPORT, String))

    If chk_reset.Checked Then
      ' Get mask to find which meter type 
      If ((_gp_show_extended_meter_type And ENUM_SHOW_EXTENDED_METER_TYPE.SEMT_RESET_TYPE_10) = ENUM_SHOW_EXTENDED_METER_TYPE.SEMT_RESET_TYPE_10) Then _lst_meter_types.Add(CType(ENUM_SAS_METER_HISTORY.TSMH_METER_RESET, String))
      If ((_gp_show_extended_meter_type And ENUM_SHOW_EXTENDED_METER_TYPE.SEMT_SERVICE_RAM_CLEAR) = ENUM_SHOW_EXTENDED_METER_TYPE.SEMT_SERVICE_RAM_CLEAR) Then _lst_meter_types.Add(CType(ENUM_SAS_METER_HISTORY.TSMH_METER_SERVICE_RAM_CLEAR, String))
      If ((_gp_show_extended_meter_type And ENUM_SHOW_EXTENDED_METER_TYPE.SEMT_MACHINE_RAM_CLEAR) = ENUM_SHOW_EXTENDED_METER_TYPE.SEMT_MACHINE_RAM_CLEAR) Then _lst_meter_types.Add(CType(ENUM_SAS_METER_HISTORY.TSMH_METER_MACHINE_RAM_CLEAR, String))
      If ((_gp_show_extended_meter_type And ENUM_SHOW_EXTENDED_METER_TYPE.SEMT_GROUP_SERVICE_RAM_CLEAR) = ENUM_SHOW_EXTENDED_METER_TYPE.SEMT_GROUP_SERVICE_RAM_CLEAR) Then _lst_meter_types.Add(CType(ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_SERVICE_RAM_CLEAR, String))
      If ((_gp_show_extended_meter_type And ENUM_SHOW_EXTENDED_METER_TYPE.SEMT_GROUP_MACHINE_RAM_CLEAR) = ENUM_SHOW_EXTENDED_METER_TYPE.SEMT_GROUP_MACHINE_RAM_CLEAR) Then _lst_meter_types.Add(CType(ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_MACHINE_RAM_CLEAR, String))
    End If

    If chk_rollover.Checked Then
      ' Get mask to find which meter type 
      If ((_gp_show_extended_meter_type And ENUM_SHOW_EXTENDED_METER_TYPE.SEMT_ROLLOVER_TYPE_11) = ENUM_SHOW_EXTENDED_METER_TYPE.SEMT_ROLLOVER_TYPE_11) Then _lst_meter_types.Add(CType(ENUM_SAS_METER_HISTORY.TSMH_METER_ROLLOVER, String))
      If ((_gp_show_extended_meter_type And ENUM_SHOW_EXTENDED_METER_TYPE.SEMT_GROUP_ROLLOVER) = ENUM_SHOW_EXTENDED_METER_TYPE.SEMT_GROUP_ROLLOVER) Then _lst_meter_types.Add(CType(ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_ROLLOVER, String))
    End If

    ' Adding first time option
    If chk_first_time.Checked Then _lst_meter_types.Add(CType(ENUM_SAS_METER_HISTORY.TSMH_METER_FIRST_TIME, String))

    ' FGB 20-JUL-2016 Add Denom Change
    If chk_denom_change.Checked Then
      If ((_gp_show_extended_meter_type And ENUM_SHOW_EXTENDED_METER_TYPE.SEMT_DENOM_CHANGE) = ENUM_SHOW_EXTENDED_METER_TYPE.SEMT_DENOM_CHANGE) Then _lst_meter_types.Add(CType(ENUM_SAS_METER_HISTORY.TSMH_METER_SAS_ACCOUNT_DENOM_CHANGE, String))
      If ((_gp_show_extended_meter_type And ENUM_SHOW_EXTENDED_METER_TYPE.SEMT_GROUP_DENOM_CHANGE) = ENUM_SHOW_EXTENDED_METER_TYPE.SEMT_GROUP_DENOM_CHANGE) Then _lst_meter_types.Add(CType(ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_SAS_ACCOUNT_DENOM_CHANGE, String))
    End If

    ' Get Meter type filter
    _str_meter_types = String.Join(", ", _lst_meter_types.ToArray())

    If _str_meter_types.Length > 0 Then
      _str_where = String.Format("TSMH_TYPE IN ({0})", _str_meter_types)
    End If

    Return _str_where
  End Function

  ' PURPOSE: Get the sql 'where' statement for meters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - an string with the 'where' statement
  Private Function GetSqlWhereMeters() As String

    Dim _str_meter_type_filter As String
    Dim _filters_list As List(Of String)

    _str_meter_type_filter = String.Empty
    _filters_list = New List(Of String)

    If Me.opt_types.Checked Or Me.m_form_mode = ENUM_FORM_MODE.SIMPLIFIED_MODE Then
      If Me.m_form_mode = ENUM_FORM_MODE.SIMPLIFIED_MODE Then
        ' Date From filter
        If Me.dtp_from_simplified.Checked Then
          _filters_list.Add(String.Format("TSMH_DATETIME >= {0}", GUI_FormatDateDB(dtp_from_simplified.Value)))
        End If

        ' Date To filter
        If Me.dtp_to_simplified.Checked Then
          _filters_list.Add(String.Format("TSMH_DATETIME < {0}", GUI_FormatDateDB(dtp_to_simplified.Value)))
        End If
      Else
        ' Date From filter
        If Me.dtp_from.Checked Then
          _filters_list.Add(String.Format("TSMH_DATETIME >= {0}", GUI_FormatDateDB(dtp_from.Value)))
        End If

        ' Date To filter
        If Me.dtp_to.Checked Then
          _filters_list.Add(String.Format("TSMH_DATETIME < {0}", GUI_FormatDateDB(dtp_to.Value)))
        End If
      End If
      ' Account denomination
      _filters_list.Add("TSMH_SAS_ACCOUNTING_DENOM IS NOT NULL")
      ' Meter Type filter
      _str_meter_type_filter = GetMeterTypeFilter()
      If (Not String.IsNullOrEmpty(_str_meter_type_filter.Trim())) Then
        _filters_list.Add(_str_meter_type_filter)
      End If
    Else
      ' Account denomination
      _filters_list.Add("TSM_SAS_ACCOUNTING_DENOM IS NOT NULL")
      _filters_list.Add("TSM_DENOMINATION = 0.0 ")
    End If

    Return GetFiltersListText(_filters_list)

  End Function ' GetSqlWhere

  ' PURPOSE: Get the sql 'where' statement for Terminals
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - an string with the 'where' statement
  Private Function GetSqlWhereTerminals() As String

    Dim _str_terminal_filter As String

    _str_terminal_filter = String.Empty

    ' Terminals filter
    _str_terminal_filter = GetTerminalsFilter()
    If (Not String.IsNullOrEmpty(_str_terminal_filter.Trim())) Then
      _str_terminal_filter = "WHERE " & _str_terminal_filter
    End If

    Return _str_terminal_filter

  End Function ' GetSqlWhere


  Private Function GetSqlAndTerminals() As String

    Dim _str_terminal_filter As String

    _str_terminal_filter = String.Empty

    ' Terminals filter
    _str_terminal_filter = GetTerminalsFilter()
    If (Not String.IsNullOrEmpty(_str_terminal_filter.Trim())) Then
      _str_terminal_filter = "AND " & _str_terminal_filter
    End If

    Return _str_terminal_filter

  End Function ' GetSqlWhere

  ' PURPOSE: Gets text of the list of filters
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT: new filter
  '
  ' RETURNS:
  '
  Private Function GetFiltersListText(FilterList As List(Of String)) As String
    Dim _str_where As StringBuilder
    _str_where = New StringBuilder()

    For _index As Integer = 0 To (FilterList.Count - 1)
      If (_index = 0) Then
        _str_where.AppendLine(" WHERE " & FilterList(_index))
      Else
        _str_where.AppendLine(" AND " & FilterList(_index))
      End If
    Next

    Return _str_where.ToString()
  End Function

  ' PURPOSE: Gets Terminal filter
  '
  '  PARAMS:
  '     - INPUT: 
  '
  '     - OUTPUT: new filter
  '
  ' RETURNS:
  '
  Private Function GetTerminalsFilter() As String
    Dim _str_terminals As String
    Dim _str_filter As String

    _str_terminals = String.Empty
    _str_filter = String.Empty

    If Me.m_form_mode = ENUM_FORM_MODE.ADVANCED_MODE Then
      'BUG 22451: If alwways return a filter for terminals, the select is more quickly
      If opt_types.Checked Then
        ' TERMINAL_SAS_METERS_HISTORY
        _str_terminals = Me.uc_provider.GetMasterIdListSelected()
      Else
        ' TERMINAL_SAS_METERS
        _str_terminals = Me.uc_provider.GetProviderIdListSelected()

      End If

      If _str_terminals.Length > 0 Then
        If opt_types.Checked Then
          ' TERMINAL_SAS_METERS_HISTORY
          _str_filter = _str_terminals
        Else
          ' TERMINAL_SAS_METERS
          _str_filter = String.Format("TSM_TERMINAL_ID IN ({0})", _str_terminals)
        End If
      End If
    Else
      ' TERMINAL_SAS_METERS_HISTORY
      _str_filter = Me.uc_provider_simplified.GetMasterIdListSelected()
    End If

    Return _str_filter

  End Function

  ' PURPOSE: Fill uc_checked_list control
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Private Sub FillGroups()
    Dim _group_name As String
    Dim _exists As Boolean

    ' Fill CheckedList control with groups
    Call Me.uc_checked_list_groups.Clear()
    Call Me.uc_checked_list_groups.ReDraw(False)

    For Each _group As MetersGroup In m_all_meters
      If Not _group.GroupName.Equals("IPLYC") OrElse (WSI.Common.Misc.IsAGGEnabled And _group.GroupName.Equals("IPLYC")) Then
        _group_name = GLB_NLS_GUI_PLAYER_TRACKING.GetString(_exists, 3080 + _group.Id - 10000)
        If Not _exists Then
          _group_name = _group.GroupName
        End If
        uc_checked_list_groups.Add(1, _group.Id, _group_name)
        uc_checked_list_groups_simplified.Add(1, _group.Id, _group_name)
        For Each _meter As Meter In _group.MeterList
          uc_checked_list_groups.Add(2, _meter.Code, "  " & VisibleMeterCode(_meter.HexCode) & SEPARATOR & _meter.CatalogName)
          uc_checked_list_groups_simplified.Add(2, _meter.Code, "  " & VisibleMeterCode(_meter.HexCode) & SEPARATOR & _meter.CatalogName)
        Next
      End If
    Next

    If Me.uc_checked_list_groups.Count > 0 Then
      Call Me.uc_checked_list_groups.ColumnWidth(3, 6500)
      Call Me.uc_checked_list_groups.CurrentRow(0)
      Call Me.uc_checked_list_groups.ReDraw(True)

      Call Me.uc_checked_list_groups_simplified.ColumnWidth(3, 6500)
      Call Me.uc_checked_list_groups_simplified.CurrentRow(0)
      Call Me.uc_checked_list_groups_simplified.ReDraw(True)
    End If

  End Sub ' FillGroups

  ' PURPOSE: Return fill datatable in dataRow()
  '
  ' PARAMS:
  '   - INPUT:
  '     - Datatable
  '
  ' RETURNS:
  '     - DataRow()
  '
  Private Function FillAndSortDatatableWithTerminalData(ByRef datatable As DataTable) As DataRow()

    Dim _column_terminal As String
    Dim _sort_by_terminal As String
    Dim _sort_by_datetime As String
    Dim _terminal_id As Int32
    Dim _site_id As Int32
    Dim _terminal_data As List(Of Object)
    Dim _row As DataRow
    Dim _rows() As DataRow
    Dim _sort As String
    Dim _unknown_text As String
    Dim _column_site As String

    If datatable.Columns.Contains("TSMH_TERMINAL_ID") And datatable.Columns.Contains("TSMH_DATETIME") Then
      _sort_by_terminal = "TE_PROVIDER, TE_NAME, TSMH_DATETIME DESC"
      _sort_by_datetime = "TSMH_DATETIME DESC, TE_PROVIDER, TE_NAME"
      _column_terminal = "TSMH_TERMINAL_ID"
      _column_site = "TSMH_SITE_ID"
    Else
      _sort_by_terminal = "TE_PROVIDER, TE_NAME"
      _sort_by_datetime = "TSM_DATETIME DESC, TE_PROVIDER, TE_NAME"
      _column_terminal = "TSM_TERMINAL_ID"
      _column_site = "TSM_SITE_ID"
    End If

    ' Get Unknown text
    _unknown_text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4650)

    ' Get rows without terminal name
    _rows = datatable.Select("TE_NAME = ''")

    While _rows.Length > 0
      _terminal_id = _rows(0)(_column_terminal)
      _rows = datatable.Select(_column_terminal & " = " & _terminal_id.ToString())
      _site_id = _rows(0)(_column_site)

      ' Get terminal info
      _terminal_data = TerminalReport.GetReportDataList(_site_id, _terminal_id, m_terminal_report_type)

      For Each _row In _rows
        If _terminal_data.Count > 0 Then
          _row(SQL_COLUMN_TERMINAL_PROVIDER) = _terminal_data(0)
          _row(SQL_COLUMN_TERMINAL_NAME) = _terminal_data(1)
          _row(SQL_COLUMN_TERMINAL_FLOOR) = _terminal_data(2)

          If (chk_terminal_location.Checked And Me.m_form_mode = ENUM_FORM_MODE.ADVANCED_MODE) _
            Or (chk_terminal_location_simplified.Checked And Me.m_form_mode = ENUM_FORM_MODE.SIMPLIFIED_MODE) Then
            _row(SQL_COLUMN_TERMINAL_AREA) = _terminal_data(4)
            _row(SQL_COLUMN_TERMINAL_BANK) = _terminal_data(5)
            _row(SQL_COLUMN_TERMINAL_POSITION) = _terminal_data(6)
          End If

        Else
          'Terminal info not found, set "Unknown" text
          _row(SQL_COLUMN_TERMINAL_PROVIDER) = _unknown_text
          _row(SQL_COLUMN_TERMINAL_NAME) = _unknown_text
          _row(SQL_COLUMN_TERMINAL_FLOOR) = _unknown_text
          _row(SQL_COLUMN_TERMINAL_AREA) = _unknown_text
          _row(SQL_COLUMN_TERMINAL_BANK) = _unknown_text
          _row(SQL_COLUMN_TERMINAL_POSITION) = _unknown_text

        End If
      Next

      _rows = datatable.Select("TE_NAME = ''")
    End While

    ' Sort rows
    _sort = IIf(opt_terminal.Checked, _sort_by_terminal, _sort_by_datetime)
    _rows = datatable.Select("", _sort)

    Return _rows

  End Function

  ' PURPOSE: Return a string representation of ENUM_SAS_METER_HISTORY
  '
  ' PARAMS:
  '   - INPUT:
  '     - Model: ENUM_SAS_METER_HISTORY
  '
  ' RETURNS:
  '     - String
  '
  Public Function GetMetersTypeAsString(ByVal Type As ENUM_SAS_METER_HISTORY) As String

    Dim _status_str As String

    'FGB  13-ENE-2017 - BUG 22752: Meters history - Does not correctly show the value of column 'Tipo' when filtering by Rollover.
    Select Case Type
      Case ENUM_SAS_METER_HISTORY.NONE
        _status_str = String.Empty

      Case ENUM_SAS_METER_HISTORY.TSMH_HOURLY
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2416)

      Case ENUM_SAS_METER_HISTORY.TSMH_MINCETUR_DAILY_REPORT
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2500)

      Case ENUM_SAS_METER_HISTORY.TSMH_METER_FIRST_TIME
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4861)

      Case ENUM_SAS_METER_HISTORY.TSMH_METER_RESET, ENUM_SAS_METER_HISTORY.TSMH_METER_SERVICE_RAM_CLEAR, ENUM_SAS_METER_HISTORY.TSMH_METER_MACHINE_RAM_CLEAR
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2418) 'Reset

      Case ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_SERVICE_RAM_CLEAR, ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_MACHINE_RAM_CLEAR
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7441) 'Reset (Detailed)

      Case ENUM_SAS_METER_HISTORY.TSMH_METER_ROLLOVER
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2417) 'Rollover

      Case ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_ROLLOVER
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7442) 'Rollover (Detailed)

      Case ENUM_SAS_METER_HISTORY.TSMH_METER_SAS_ACCOUNT_DENOM_CHANGE
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7439) 'Denom change

      Case ENUM_SAS_METER_HISTORY.TSMH_METER_GROUP_SAS_ACCOUNT_DENOM_CHANGE
        _status_str = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7440) 'Denom change (Detailed)

      Case Else
        _status_str = String.Empty
    End Select

    Return _status_str

  End Function ' GetMetersTypeAsString

  Private Function VisibleMeterCode(ByVal HexCode As String) As String
    Dim _code As String

    _code = HexCode.PadLeft(5, "0").Trim()
    _code = _code.Substring(_code.Length - VISIBLE_CODE_LENGTH)

    Return _code
  End Function

  Private Function SelectMetersByGroup(ByVal FullMetersGroupList As List(Of MetersGroup), ByVal FilterGroupListId As String, ByVal FilterMeterListId As String) As List(Of MetersGroup)
    Dim _group_list_id As List(Of String)
    Dim _meter_list_id As List(Of String)
    Dim _meter_list_added_id As List(Of Integer)
    Dim _select_list As List(Of MetersGroup)
    Dim _new_group As MetersGroup
    Dim _new_meter As Meter

    _select_list = New List(Of MetersGroup)
    _group_list_id = New List(Of String)(FilterGroupListId.Replace(" ", "").Split(","))
    _meter_list_id = New List(Of String)(FilterMeterListId.Replace(" ", "").Split(","))
    _meter_list_added_id = New List(Of Integer)

    For Each _group As MetersGroup In FullMetersGroupList
      If _group_list_id.Contains(_group.Id.ToString()) Then
        _new_group = New MetersGroup()
        _new_group.Id = _group.Id
        _new_group.GroupName = _group.GroupName
        _new_group.Type = _group.Type
        _new_group.MeterList = New List(Of Meter)
        For Each _meter As Meter In _group.MeterList
          _new_meter = New Meter()
          _new_meter.Code = _meter.Code
          _new_meter.HexCode = _meter.HexCode
          _new_meter.Description = _meter.Description
          _new_meter.CatalogName = _meter.CatalogName
          If _meter_list_id.Contains(_meter.Code.ToString()) And Not _meter_list_added_id.Contains(_meter.Code) Then

            _new_group.MeterList.Add(_new_meter)
            _meter_list_added_id.Add(_meter.Code)
          End If
        Next
        If _new_group.MeterList.Count > 0 Then
          _select_list.Add(_new_group)
        End If
      End If
    Next

    Return _select_list
  End Function

  Private Function LoadMetersByGroup() As List(Of MetersGroup)
    Dim _meter_group_list As List(Of MetersGroup)
    Dim _str_sql As StringBuilder
    Dim _dt_counters As DataTable
    Dim _group_id As Integer
    Dim _new_meter As Meter
    Dim _new_meters_group As MetersGroup

    _meter_group_list = New List(Of MetersGroup)
    _str_sql = New StringBuilder()
    _dt_counters = New DataTable()
    _group_id = 0

    Try

      Using _db_trx As DB_TRX = New DB_TRX()

        _str_sql.AppendLine("  SELECT   SMCG_GROUP_ID                       ")
        _str_sql.AppendLine("         , SMCG_METER_CODE                     ")
        _str_sql.AppendLine("         , SMC_DESCRIPTION                     ")
        _str_sql.AppendLine("         , SMG_NAME                            ")
        _str_sql.AppendLine("         , ISNULL(SMC_NAME, '') AS CATALOG_NAME")
        _str_sql.AppendLine("    FROM   SAS_METERS_CATALOG_PER_GROUP ")
        _str_sql.AppendLine("   INNER   JOIN SAS_METERS_CATALOG ON SMC_METER_CODE = SMCG_METER_CODE ")
        _str_sql.AppendLine("   INNER   JOIN SAS_METERS_GROUPS ON SMCG_GROUP_ID = SMG_GROUP_ID ")
        _str_sql.AppendLine("   WHERE   SMCG_GROUP_ID > 10000 ")
        _str_sql.AppendLine("ORDER BY   SMCG_GROUP_ID ")

        Using _sql_cmd As SqlCommand = New SqlCommand(_str_sql.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          Using _sql_adapter As SqlDataAdapter = New SqlDataAdapter(_sql_cmd)

            If _sql_adapter.Fill(_dt_counters) > 0 Then
              For Each _row As DataRow In _dt_counters.Rows

                ' LEM 28-APR-2014: Meter 0x000C only visible in WASS mode.
                If m_system_mode <> SYSTEM_MODE.WASS And _row(1) = &HC Then
                  Continue For
                End If

                If _group_id <> CInt(_row(0)) Then
                  _group_id = _row(0) ' GroupId

                  _new_meters_group = New MetersGroup()
                  _new_meters_group.MeterList = New List(Of Meter)
                  _new_meters_group.Id = _group_id
                  _new_meters_group.GroupName = _row(3) 'GroupName
                  _new_meters_group.Type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3482) ' Meter

                  _meter_group_list.Add(_new_meters_group)
                End If

                _new_meter = New Meter()
                _new_meter.Code = _row(1) ' MeterCode
                _new_meter.HexCode = Hex(_new_meter.Code)
                _new_meter.Description = _row(2) ' MeterDescription

                '_new_meter.CatalogName = _row(4) ' Catalog Name

                If _new_meter.Code > EVENT_START_BASE Then
                  _new_meter.CatalogName = IIf(String.IsNullOrEmpty(_row(4)), GLB_NLS_GUI_PLAYER_TRACKING.GetString((_new_meter.Code - EVENT_START_BASE) + NLS_EVENTS), _row(4)) ' Catalog Name
                  _meter_group_list(_meter_group_list.Count - 1).Type = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3483) ' Event
                Else
                  _new_meter.CatalogName = IIf(String.IsNullOrEmpty(_row(4)),
                                               WSI.Common.Resource.String(String.Format("STR_SAS_METER_DESC_{0:00000}", _new_meter.Code)), _row(4))
                End If

                _meter_group_list(_meter_group_list.Count - 1).MeterList.Add(_new_meter)

              Next
            End If
          End Using
        End Using
      End Using

    Catch _ex As Exception
      Log.Exception(_ex)
    End Try

    Return _meter_group_list
  End Function

  Private Function FormatMeterValue(ByVal MeterCode As Int32, ByVal MeterValue As Double, ByVal AccountDenomination As Double) As String
    Dim _value As String

    Select Case MeterCode
      Case &H0, &H1, &H2, &H3, &H4, &H8, &H9, &HA, &HB, &HC, &HD, &HE _
         , &HF, &H10, &H15, &H16, &H17, &H18, &H19, &H1A, &H1B, &H1C, &H1D, &H1E _
         , &H1F, &H20, &H21, &H22, &H23, &H24, &H27, &H28, &H29, &H2A, &H2B, &H2C _
         , &H2D, &H2E, &H2F, &H30, &H31, &H32, &H33, &H34, &H3F, &H58, &H63, &H6E _
         , &H80, &H82, &H84, &H86, &H88, &H8A, &H8C, &H8E, &H90, &H92, &HA0, &HA2 _
         , &HA4, &HA6, &HA8, &HAA, &HAC, &HAE, &HB0, &HB8, &HBA, &HBC, &H1000

        If AccountDenomination <> -1.0 Then
          _value = SelectTypeCurrency(m_is_center, MeterValue * 0.01)
        Else
          _value = SelectTypeCurrency(m_is_center, MeterValue / 100.0)
        End If

      Case Else

        _value = GUI_FormatNumber(MeterValue, 0)
    End Select


    Return _value
  End Function

  Private Sub GridColumnReIndex()

    TERMINAL_DATA_COLUMNS = TerminalReport.NumColumns(m_terminal_report_type)

    GRID_COLUMNS_COUNT = TERMINAL_DATA_COLUMNS + FIXED_INITIAL_COLUMNS

  End Sub

  Private Sub UpdateDateTo_Simplified()
    Dim _date_from As DateTime
    Dim _date_to As DateTime

    _date_from = Me.dtp_from_simplified.Value

    If Me.opt_hourly_simplified.Checked Then
      _date_to = _date_from.AddHours(24)
    ElseIf Me.opt_daily_one_month_simplified.Checked Then
      _date_to = _date_from.AddMonths(1)
    ElseIf Me.opt_daily_one_day_simplified.Checked Then
      _date_to = _date_from.AddDays(1)
    End If

    Me.dtp_to_simplified.Value = _date_to
  End Sub


  ' PURPOSE: Enable/Disable mode related controls
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub EnableMonitorOrHistory()

    If opt_monitor_mode.Checked Then
      gb_date.Enabled = False
      m_monitor_enabled = True
      Me.Grid.Sortable = False
      m_first_time = True

      If GUI_FilterCheck() Then
        Call tmr_monitor.Start()
        Me.Grid.Redraw = False
        tf_last_update.Visible = True
        Call tmr_monitor_Tick(Nothing, Nothing)
        Me.Grid.Redraw = True
        Call Me.GUI_ReportUpdateFilters()

      End If

    ElseIf opt_history_mode.Checked AndAlso gb_mode.Enabled Then
      gb_date.Enabled = True
      m_monitor_enabled = False
      Me.Grid.Sortable = True
      m_first_time = False
      Call tmr_monitor.Stop()
    End If

    m_init = False

  End Sub ' EnableMonitorOrHistory
#End Region


#Region "Events"

  Private Sub opt_currents_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_currents.CheckedChanged
    If opt_currents.Checked Then
      ' TERMINAL_SAS_METERS
      chk_daily.Checked = False
      chk_daily.Enabled = False
      chk_hourly.Checked = False
      chk_hourly.Enabled = False
      chk_reset.Checked = False
      chk_reset.Enabled = False
      chk_rollover.Checked = False
      chk_rollover.Enabled = False
      chk_first_time.Checked = False
      chk_first_time.Enabled = False
      chk_denom_change.Checked = False
      chk_denom_change.Enabled = False
      opt_terminal.Checked = True
      gb_date.Enabled = False
      dtp_from.Checked = False
      dtp_to.Checked = False
      chk_hide_no_activity.Enabled = True
    Else
      ' TERMINAL_SAS_METERS_HISTORY
      gb_date.Enabled = True
      dtp_from.Checked = True
      dtp_to.Checked = True
    End If
  End Sub ' opt_currents_CheckedChanged

  Private Sub opt_types_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_types.CheckedChanged
    If opt_types.Checked Then
      chk_daily.Enabled = True
      chk_hourly.Enabled = True
      chk_reset.Enabled = True
      chk_rollover.Enabled = True
      chk_first_time.Enabled = True
      chk_denom_change.Enabled = True
      chk_hide_no_activity.Enabled = True

      opt_date.Checked = True
    End If
  End Sub ' opt_types_CheckedChanged

  Private Sub CheckActivityMeterTypeOptions()
    If (chk_first_time.Checked OrElse chk_rollover.Checked OrElse chk_reset.Checked OrElse chk_denom_change.Checked) AndAlso Not (chk_daily.Checked OrElse chk_hourly.Checked OrElse opt_currents.Checked) Then
      chk_hide_no_activity.Enabled = False
      chk_hide_no_activity.Checked = False
    Else
      chk_hide_no_activity.Enabled = True
    End If
  End Sub

  Private Sub chk_reset_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Call CheckActivityMeterTypeOptions()
  End Sub

  Private Sub chk_rollover_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Call CheckActivityMeterTypeOptions()
  End Sub
  Private Sub chk_first_time_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Call CheckActivityMeterTypeOptions()
  End Sub

  Private Sub chk_denom_change_CheckedChanged(sender As Object, e As EventArgs)
    Call CheckActivityMeterTypeOptions()
  End Sub

  Public Sub CheckDailyOrHourlyMeterTypeOptions()
    If Not (chk_first_time.Checked OrElse chk_rollover.Checked OrElse chk_reset.Checked OrElse chk_denom_change.Checked) OrElse chk_daily.Checked OrElse chk_hourly.Checked OrElse opt_currents.Checked Then
      chk_hide_no_activity.Enabled = True
    Else
      chk_hide_no_activity.Enabled = False
      chk_hide_no_activity.Checked = False
    End If
  End Sub

  Private Sub chk_daily_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_daily.CheckedChanged
    Call CheckDailyOrHourlyMeterTypeOptions()
  End Sub

  Private Sub chk_hourly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_hourly.CheckedChanged
    Call CheckDailyOrHourlyMeterTypeOptions()
  End Sub

  Private Sub chk_terminal_location_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_terminal_location_simplified.CheckedChanged, chk_terminal_location.CheckedChanged
    If (chk_terminal_location.Checked And Me.m_form_mode = ENUM_FORM_MODE.ADVANCED_MODE) _
      Or (chk_terminal_location_simplified.Checked And Me.m_form_mode = ENUM_FORM_MODE.SIMPLIFIED_MODE) Then
      m_terminal_report_type = ReportType.Provider + ReportType.Location
    Else
      m_terminal_report_type = ReportType.Provider
    End If
  End Sub

  Private Sub uc_provider_simplified_ValueChangedEvent() Handles uc_provider_simplified.ValueChangedEvent

    If Not opt_daily_one_month_simplified.Checked Then
      Me.opt_currents.Checked = True
    End If

    If Me.uc_provider_simplified.AllSelectedChecked Or Me.uc_provider_simplified.SeveralTerminalsChecked Then
      Me.opt_hourly_simplified.Enabled = False
      Me.opt_daily_one_month_simplified.Enabled = False
      Me.opt_daily_one_day_simplified.Checked = True
    Else
      Me.opt_hourly_simplified.Enabled = True
      Me.opt_daily_one_month_simplified.Enabled = True
    End If

    If opt_daily_one_day_simplified.Checked Then
      Me.chk_daily.Checked = True
      Me.chk_hourly.Checked = False
    ElseIf opt_daily_one_month_simplified.Checked Then
      Me.chk_daily.Checked = True
      Me.chk_hourly.Checked = False
    ElseIf opt_hourly_simplified.Checked Then
      Me.chk_daily.Checked = False
      Me.chk_hourly.Checked = True
    End If

  End Sub

  ''' <summary>
  ''' Init MS controls.
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub GUI_InitControlsMultisite()

    Me.ClientSize = New System.Drawing.Size(1224, 676)

    If WSI.Common.Misc.IsAGGEnabled() AndAlso m_is_center Then
      Me.Size = New Size(Me.Size.Width + 150, Me.Size.Height)
    End If

    Me.panel_data.Location = New System.Drawing.Point(5, 231)
    Me.panel_data.Size = New System.Drawing.Size(1214, 431)

    Me.panel_filter.Size = New System.Drawing.Size(1214, 227)

    Me.pnl_filters_standard.Location = New System.Drawing.Point(1, 1)
    Me.pnl_filters_standard.Size = New System.Drawing.Size(1120, 226)

    Me.chk_terminal_location.Location = New System.Drawing.Point(778, 184)
    Me.chk_hide_no_activity.Location = New System.Drawing.Point(778, 206)

    Me.gb_date.Location = New System.Drawing.Point(152, 138)
    Me.gb_date.Size = New System.Drawing.Size(284, 80)

    Me.uc_checked_list_groups.Location = New System.Drawing.Point(444, 5)
    Me.uc_checked_list_groups.m_resize_width = 325
    Me.uc_checked_list_groups.Size = New System.Drawing.Size(325, 173)

    Me.gb_order_by.Location = New System.Drawing.Point(444, 179)
    Me.gb_order_by.Size = New System.Drawing.Size(325, 39)
    Me.opt_terminal.Location = New System.Drawing.Point(219, 15)

    Me.opt_date.Location = New System.Drawing.Point(18, 16)

    Me.uc_provider.Location = New System.Drawing.Point(775, 1)
    Me.uc_provider.Size = New System.Drawing.Size(325, 187)

    Me.gb_counter_type.Location = New System.Drawing.Point(3, 4)
    Me.gb_counter_type.Size = New System.Drawing.Size(143, 214)

    Me.uc_sites_sel.Size = New System.Drawing.Size(287, 130)
    Me.uc_sites_sel.Location = New System.Drawing.Point(152, 2)
    Me.uc_sites_sel.Init()
    Me.uc_sites_sel.SetDefaultValues(False)

    Me.uc_sites_sel.TabIndex = 1
    Me.gb_date.TabIndex = 2
    Me.uc_checked_list_groups.TabIndex = 3
    Me.gb_order_by.TabIndex = 4
    Me.uc_provider.TabIndex = 5
    Me.chk_terminal_location.TabIndex = 6
    Me.chk_hide_no_activity.TabIndex = 7



  End Sub ' GUI_InitControlsMultisite


  Private Sub opt_hourly_simplified_CheckedChanged(sender As Object, e As EventArgs) Handles opt_hourly_simplified.CheckedChanged
    UpdateDateTo_Simplified()

    Me.opt_currents.Checked = True
    Me.chk_hourly.Checked = True
    Me.chk_daily.Checked = False

    Me.chk_show_events_simplified.Enabled = True

    Me.dtp_from_simplified.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to_simplified.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
  End Sub

  Private Sub opt_daily_one_month_simplified_CheckedChanged(sender As Object, e As EventArgs) Handles opt_daily_one_month_simplified.CheckedChanged
    Dim _aux_date As DateTime

    UpdateDateTo_Simplified()

    Me.opt_currents.Checked = False
    Me.chk_hourly.Checked = False
    Me.chk_daily.Checked = True

    Me.chk_show_events_simplified.Checked = False
    Me.chk_show_events_simplified.Enabled = False

    Me.dtp_from_simplified.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Me.dtp_to_simplified.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    _aux_date = New DateTime(Me.dtp_from_simplified.Value.Year, Me.dtp_from_simplified.Value.Month, Me.dtp_from_simplified.Value.Day, GetDefaultClosingTime(), 0, 0)
    Me.dtp_from_simplified.Value = _aux_date

    _aux_date = New DateTime(Me.dtp_to_simplified.Value.Year, Me.dtp_to_simplified.Value.Month, Me.dtp_to_simplified.Value.Day, GetDefaultClosingTime(), 0, 0)
    Me.dtp_to_simplified.Value = _aux_date
  End Sub

  Private Sub opt_daily_one_day_simplified_CheckedChanged(sender As Object, e As EventArgs) Handles opt_daily_one_day_simplified.CheckedChanged
    Dim _aux_date As DateTime

    UpdateDateTo_Simplified()

    Me.opt_currents.Checked = True
    Me.chk_hourly.Checked = False
    Me.chk_daily.Checked = True

    Me.chk_show_events_simplified.Checked = False
    Me.chk_show_events_simplified.Enabled = False

    Me.dtp_from_simplified.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)
    Me.dtp_to_simplified.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_TIME_NONE)

    _aux_date = New DateTime(Me.dtp_from_simplified.Value.Year, Me.dtp_from_simplified.Value.Month, Me.dtp_from_simplified.Value.Day, GetDefaultClosingTime(), 0, 0)
    Me.dtp_from_simplified.Value = _aux_date

    _aux_date = New DateTime(Me.dtp_to_simplified.Value.Year, Me.dtp_to_simplified.Value.Month, Me.dtp_to_simplified.Value.Day, GetDefaultClosingTime(), 0, 0)
    Me.dtp_to_simplified.Value = _aux_date
  End Sub

  Private Sub dtp_from_simplified_DatePickerValueChanged() Handles dtp_from_simplified.DatePickerValueChanged
    UpdateDateTo_Simplified()
  End Sub

  Private Sub chk_show_events_simplified_CheckedChanged(sender As Object, e As EventArgs) Handles chk_show_events_simplified.CheckedChanged
    Me.chk_reset.Checked = Me.chk_show_events_simplified.Checked
    Me.chk_rollover.Checked = Me.chk_show_events_simplified.Checked
    Me.chk_first_time.Checked = Me.chk_show_events_simplified.Checked
    Me.chk_denom_change.Checked = Me.chk_show_events_simplified.Checked
  End Sub

  Private Sub opt_monitor_mode_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_monitor_mode.CheckedChanged
    If opt_monitor_mode.Checked Then
      Call EnableMonitorOrHistory()
    Else
      tf_last_update.Visible = False
    End If
  End Sub ' opt_monitor_mode_CheckedChanged

  Private Sub opt_history_mode_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_history_mode.CheckedChanged
    If opt_history_mode.Checked Then
      Call EnableMonitorOrHistory()
    End If
  End Sub ' opt_history_mode_CheckedChanged

  Private Sub tmr_monitor_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmr_monitor.Tick
    If opt_monitor_mode.Checked Then
      Call GUI_ExecuteQueryCustom()
      Me.tf_last_update.Value = GUI_FormatDate(WGDB.Now, ModuleDateTimeFormats.ENUM_FORMAT_DATE.FORMAT_DATE_NONE, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If
  End Sub ' tmr_monitor_Tick


#End Region

End Class ' frm_terminal_sas_meters_sel

Public Class MetersGroup
  Public Id As Int32
  Public GroupName As String
  Public Type As String
  Public MeterList As List(Of Meter)
End Class

Public Class Meter
  Public Code As Int32
  Public HexCode As String
  Public Description As String
  Public CatalogName As String
End Class

'Public Class MeterCompare
'  Implements IEqualityComparer(Of MetersGroup)

'  Public Function Equals1(x As MetersGroup, y As MetersGroup) As Boolean Implements IEqualityComparer(Of MetersGroup).Equals
'    If x Is y Then Return True
'    If x Is Nothing OrElse y Is Nothing Then Return False

'    Return x.MeterList(0).Code = y.MeterList(0).Code

'  End Function

'  Public Function GetHashCode1(obj As MetersGroup) As Integer Implements IEqualityComparer(Of MetersGroup).GetHashCode
'    If obj Is Nothing Then Return 0


'  End Function
'End Class

