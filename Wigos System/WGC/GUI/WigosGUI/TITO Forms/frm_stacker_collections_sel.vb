'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_stacker_collections_sel
' DESCRIPTION:   This screen allows to view the stacker collections.
' AUTHOR:        Francesc Borrell
' CREATION DATE: 01-JUL-2013
'
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 01-JUL-2013  FBA    Initial version
' 20-AUG-2013  JRM    Completing the "Selection" functionallity
' 01-OCT-2013  JFC    Adding new groupbox "Collection state"
'--------------------------------------------------------------------
Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports WSI.Common
Imports System.Data.SqlClient

Public Class frm_stacker_collections_sel
  Inherits frm_base_sel

#Region " Constants "

  ' DB Columns
  Private Const SQL_COLUMN_STACKER_COLLECTION_ID As Integer = 0
  Private Const SQL_COLUMN_STACKER_COLLECTION_DATE As Integer = 1
  Private Const SQL_COLUMN_COLLECTION_EMPLOYEE As Integer = 2
  Private Const SQL_COLUMN_STACKER_IDENTIFIER As Integer = 3
  Private Const SQL_COLUMN_TERMINAL As Integer = 4
  Private Const SQL_COLUMN_PROVIDER As Integer = 5

  '  Private Const SQL_COLUMN_TERMINAL_TYPE As Integer = 4
  Private Const SQL_COLUMN_NUMBER_OF_BILLS As Integer = 6
  Private Const SQL_COLUMN_BILLS_TOTAL_AMOUNT As Integer = 7
  Private Const SQL_COLUMN_NUMBER_OF_TICKETS As Integer = 8
  Private Const SQL_COLUMN_TICKETS_TOTAL_AMOUNT As Integer = 9
  Private Const SQL_COLUMN_BALANCE_STATE As Integer = 10

  ' DB Columns for combos
  Private Const SQL_COLUMN_ID As Integer = 0
  Private Const SQL_COLUMN_NAME As Integer = 1

  ' Grid configuration
  Private Const GRID_COLUMNS As Integer = 10
  Private Const GRID_HEADER_ROWS As Integer = 1

  ' Grid Columns
  Private Const GRID_COLUMN_STACKER_COLLECTION_ID As Integer = 0
  Private Const GRID_COLUMN_STACKER_COLLECTION_DATE As Integer = 1
  Private Const GRID_COLUMN_COLLECTION_EMPLOYEE As Integer = 2
  Private Const GRID_COLUMN_STACKER_IDENTIFIER As Integer = 3
  Private Const GRID_COLUMN_TERMINAL As Integer = 4
  'Private Const GRID_COLUMN_TERMINAL_TYPE As Integer = 4
  Private Const GRID_COLUMN_NUMBER_OF_BILLS As Integer = 5
  Private Const GRID_COLUMN_BILLS_TOTAL_AMOUNT As Integer = 6
  Private Const GRID_COLUMN_NUMBER_OF_TICKETS As Integer = 7
  Private Const GRID_COLUMN_TICKETS_TOTAL_AMOUNT As Integer = 8
  Private Const GRID_COLUMN_BALANCE_STATE As Integer = 9

  ' Columns Width
  Private Const GRID_WIDTH_COLLECTION_ID As Integer = 0
  Private Const GRID_WIDTH_COLLECTION_DATE As Integer = 2000
  Private Const GRID_COLUMN_WIDTH_COLLECTION_EMPLOYEE As Integer = 1200
  Private Const GRID_COLUMN_WIDTH_STACKER_IDENTIFIER As Integer = 1100
  Private Const GRID_WIDTH_TERMINAL As Integer = 2500
  '  Private Const GRID_WIDTH_TERMINAL_TYPE As Integer = 0 'de momento no por qu� s�lo hay un tipo de terminal
  Private Const GRID_WIDTH_NUMBER_OF_BILLS As Integer = 1500
  Private Const GRID_WIDTH_BILLS_TOTAL_AMOUNT As Integer = 2000
  Private Const GRID_WIDTH_NUMBER_OF_TICKETS As Integer = 1500
  Private Const GRID_WIDTH_TICKETS_TOTAL_AMOUNT As Integer = 2000
  Private Const GRID_WIDTH_BALANCE_STATE As Integer = 1800

  ' Text format columns
  Private Const EXCEL_COLUMN_FILTERS As Integer = 2
  Private Const EXCEL_COLUMN_NUMBER_OF_BILLS As Integer = 8
  Private Const EXCEL_COLUMN_NUMBER_OF_TICKETS As Integer = 10

#End Region ' Constants

#Region " Members "

  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String
  Private m_stacker_id As String
  Private m_employee_id As String
  Private m_terminals As String
  Private m_status As String
  Private m_cashier_session_id As Long
  Private m_cashier_session_name As String

#End Region ' Members

#Region " Overrides "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_STACKER_COLLECTION_SEL

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE : Initialize every form control
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Form title
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2222)

    ' Buttons
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_SELECT).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Visible = False ' TODO: desactivado; debe hacerse desde frm_cashier_session_detail
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_NEW).Enabled = Me.Permissions.Write And False
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6)
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2191)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = False


    ' Employee
    Me.gb_employee.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2224)
    Me.opt_one_employee.Text = GLB_NLS_GUI_ALARMS.GetString(276)
    Me.opt_all_employee.Text = GLB_NLS_GUI_ALARMS.GetString(277)
    Me.cmb_employee.IsReadOnly = False

    ' Dates
    Me.ds_from_to.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2330)

    ' Stacker 
    Me.gb_stacker.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2199)
    Me.opt_one_stacker.Text = GLB_NLS_GUI_ALARMS.GetString(276)
    Me.opt_all_stackers.Text = GLB_NLS_GUI_ALARMS.GetString(277)
    Me.opt_none_stackers.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)
    Me.cmb_stacker.IsReadOnly = False

    'state
    Me.gb_collections_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2231)
    Me.chk_collection_status_not_closed.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2677)
    Me.chk_collection_status_correct.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2232)
    Me.chk_collection_status_incorrect.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2233)

    ' Fill up uc_provider
    Call Me.uc_provider.Init(WSI.Common.Misc.GamingTerminalTypeList())

    ' Fill combo boxes 
    Call SetCombo(Me.cmb_employee, "SELECT GU_USER_ID, GU_USERNAME FROM GUI_USERS ORDER BY GU_USERNAME")
    Call SetCombo(Me.cmb_stacker, "SELECT ST_STACKER_ID, ST_STACKER_ID FROM STACKERS ORDER BY ST_MODEL")

    ' Define layout of all Main Grid Columns
    Call GUI_StyleSheet()

    ' Set filter default values
    Call GUI_FilterReset()

    If m_cashier_session_id > 0 Then
      lb_cashier_session.Visible = True
      lb_cashier_session.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(629) & " : " & m_cashier_session_name
    End If

  End Sub ' GUI_InitControls



  ' PURPOSE : Initialize all form filters with their default values
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Protected Overrides Sub GUI_FilterReset()

    Call SetDefaultValues()

  End Sub 'Gui_FilterReset

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String

    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    _sb.AppendLine("    SELECT   MC_COLLECTION_ID                                                              ")
    _sb.AppendLine("           , MC_COLLECTION_DATETIME                                                        ")
    _sb.AppendLine("           , GU_USERNAME                                                                  ")
    _sb.AppendLine("           , MC_STACKER_ID                                                                 ")
    _sb.AppendLine("           , TE_PROVIDER_ID                                                                ")
    _sb.AppendLine("           , TE_NAME                                                                       ")
    _sb.AppendLine("           , (SELECT   SUM(MCD_NUM_COLLECTED) ")
    _sb.AppendLine("                FROM   MONEY_COLLECTION_DETAILS ")
    _sb.AppendLine("               WHERE   MCD_COLLECTION_ID = MC_COLLECTION_ID )")
    _sb.AppendLine("        AS   COLLECTED_BILLS  ") 'TODO MC_COLLECTED_BILLS check frm_money_collections query!
    _sb.AppendLine("           , (SELECT   SUM(MCD_FACE_VALUE * MCD_NUM_COLLECTED) ")
    _sb.AppendLine("                FROM   MONEY_COLLECTION_DETAILS ")
    _sb.AppendLine("               WHERE   MCD_COLLECTION_ID = MC_COLLECTION_ID)")
    _sb.AppendLine("        AS   BILLS_AMOUNT  ") 'TODO MC_BILLS_AMOUNT        
    _sb.AppendLine("           , (SELECT   COUNT(*) ")
    _sb.AppendLine("                FROM   TICKETS ")
    _sb.AppendLine("               WHERE   TI_MONEY_COLLECTION_ID = MC_COLLECTION_ID) ")
    _sb.AppendLine("        AS   COLLECTED_TICKETS   ") 'TODO MC_COLLECTED_TICKETS
    _sb.AppendLine("           , (SELECT   SUM(ti_amount) ")
    _sb.AppendLine("                FROM   tickets ")
    _sb.AppendLine("               WHERE  ti_money_collection_id = MC_COLLECTION_ID)")
    _sb.AppendLine("        AS   TICHETS_AMOUNT ") 'TODO MC_TICKETS_AMOUNT
    _sb.AppendLine("           , MC_BALANCED                                                                     ")
    _sb.AppendLine("      FROM   MONEY_COLLECTIONS                                                              ")
    _sb.AppendLine(" LEFT JOIN   STACKERS ON MONEY_COLLECTIONS.MC_STACKER_ID = STACKERS.ST_STACKER_ID           ")
    _sb.AppendLine(" LEFT JOIN   TERMINALS ON MONEY_COLLECTIONS.MC_TERMINAL_ID = TERMINALS.TE_TERMINAL_ID       ")
    _sb.AppendLine(" LEFT JOIN   GUI_USERS ON MONEY_COLLECTIONS.MC_USER_ID = GUI_USERS.GU_USER_ID               ")
    _sb.AppendLine(GetSqlWhere())
    _sb.AppendLine("  ORDER BY   MC_COLLECTION_DATETIME DESC                                                                   ")

    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS : 
  '     - True (the row should be added) or False (the row can not be added)
  '
  '   NOTES :
  '
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _str_cuadratura As String
    _str_cuadratura = ""

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_STACKER_COLLECTION_ID)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STACKER_COLLECTION_ID).Value = DbRow.Value(SQL_COLUMN_STACKER_COLLECTION_ID)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_STACKER_COLLECTION_DATE)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STACKER_COLLECTION_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_STACKER_COLLECTION_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    'comprueba que no sea NULL
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_COLLECTION_EMPLOYEE)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_COLLECTION_EMPLOYEE).Value = DbRow.Value(SQL_COLUMN_COLLECTION_EMPLOYEE)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_STACKER_IDENTIFIER)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_STACKER_IDENTIFIER).Value = DbRow.Value(SQL_COLUMN_STACKER_IDENTIFIER)
    End If

    'comprueba que no sea NULL
    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TERMINAL)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TERMINAL).Value = DbRow.Value(SQL_COLUMN_TERMINAL) & " / " & DbRow.Value(SQL_COLUMN_PROVIDER)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_NUMBER_OF_BILLS)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUMBER_OF_BILLS).Value = DbRow.Value(SQL_COLUMN_NUMBER_OF_BILLS)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_BILLS_TOTAL_AMOUNT)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_BILLS_TOTAL_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_BILLS_TOTAL_AMOUNT))
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_NUMBER_OF_TICKETS)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_NUMBER_OF_TICKETS).Value = DbRow.Value(SQL_COLUMN_NUMBER_OF_TICKETS)
    End If

    If Not IsDBNull(DbRow.Value(SQL_COLUMN_TICKETS_TOTAL_AMOUNT)) Then
      Me.Grid.Cell(RowIndex, GRID_COLUMN_TICKETS_TOTAL_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_TICKETS_TOTAL_AMOUNT))
    End If

    If IsDBNull(DbRow.Value(SQL_COLUMN_BALANCE_STATE)) Then
      _str_cuadratura = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2677)
    ElseIf DbRow.Value(SQL_COLUMN_BALANCE_STATE) = False Then
      _str_cuadratura = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2233)
    ElseIf DbRow.Value(SQL_COLUMN_BALANCE_STATE) = True Then
      _str_cuadratura = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2232)
    End If

    Me.Grid.Cell(RowIndex, GRID_COLUMN_BALANCE_STATE).Value = _str_cuadratura

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Check for consistency values provided for every filter
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - TRUE: filter values are accepted
  '     - FALSE: filter values are not accepted
  Protected Overrides Function GUI_FilterCheck() As Boolean

    If Me.ds_from_to.FromDateSelected And Me.ds_from_to.ToDateSelected Then
      If Me.ds_from_to.FromDate > Me.ds_from_to.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.ds_from_to.Focus()

        Return False
      End If
    End If

    If uc_provider.opt_several_types.Checked And (uc_provider.GetTerminalIdListSelected() Is Nothing) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1411), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      Return False
    End If

    Return True

  End Function ' GUI_FilterCheck

  ' PURPOSE: Activated when a row of the grid is double clicked or selected, so it can be edited
  '  PARAMS:
  '     - INPUT:
  '               
  '     - OUTPUT:
  '          
  ' RETURNS:
  '     - none
  Protected Overrides Sub GUI_EditSelectedItem()

    Dim _idx_row As Int32
    Dim _sc_id As Int64
    Dim _frm_edit As frm_new_collection_edit

    ' Search the first row selected
    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If _idx_row = Me.Grid.NumRows Then
      Return
    End If

    ' Get the Draw ID and Name, and launch the editor
    _sc_id = Me.Grid.Cell(_idx_row, GRID_COLUMN_STACKER_COLLECTION_ID).Value
    _frm_edit = New frm_new_collection_edit

    Call _frm_edit.ShowEditItem(_sc_id)

    _frm_edit = Nothing

    Call Me.Grid.Focus()

  End Sub 'GUI_EditSelectedItem

  ' PURPOSE: Process button actions in order to branch to a child screen
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)

    Select Case ButtonId

      Case frm_base_sel.ENUM_BUTTON.BUTTON_SELECT
        Call GUI_EditSelectedItem()

        'Case frm_base_sel.ENUM_BUTTON.BUTTON_NEW
        'Call GUI_ShowNewCollectionForm()
      Case frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1
        'llamar a al funcion delete
        GUI_DeleteMoneyCollection(0)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Perform final processing for the grid data (totalisator row)
  '
  '  PARAMS:
  '     - INPUT:
  ' 
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_AfterLastRow()
    Me.GUI_Button(frm_base_sel.ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = (Me.Grid.NumRows > 0) AndAlso Me.Permissions.Delete
  End Sub


#Region " GUI Reports "

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:

  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    With Me.Grid

      Call MyBase.GUI_ReportParams(ExcelData)

      ExcelData.SetColumnFormat(EXCEL_COLUMN_FILTERS, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
      ExcelData.SetColumnFormat(EXCEL_COLUMN_NUMBER_OF_BILLS, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)
      ExcelData.SetColumnFormat(EXCEL_COLUMN_NUMBER_OF_TICKETS, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT)

    End With
  End Sub

  ' PURPOSE: Set proper values for form filters being sent to the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.FilterHeaderWidth(1) = 2000
    PrintData.FilterHeaderWidth(2) = 2000
    PrintData.FilterHeaderWidth(3) = 2000

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2331), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2332), m_date_to)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    'status
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2231), m_status)

    'employee
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2224), m_employee_id)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)
    PrintData.SetFilter("", "", True)

    'idstacker
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2230), m_stacker_id)

    'terminals
    PrintData.SetFilter(GLB_NLS_GUI_STATISTICS.GetString(470), m_terminals)

    If m_cashier_session_id > 0 Then
      PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(629), m_cashier_session_name)
    End If

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Set texts corresponding to the provided filter values for the report
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_ReportUpdateFilters()

    m_date_from = ""
    m_date_to = ""
    m_stacker_id = ""
    m_employee_id = ""
    m_terminals = ""
    m_status = ""

    ' Date 
    If Me.ds_from_to.FromDateSelected Then
      m_date_from = GUI_FormatDate(ds_from_to.FromDate.AddHours(ds_from_to.ClosingTime), _
                                   ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                   ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    If Me.ds_from_to.ToDateSelected Then
      m_date_to = GUI_FormatDate(ds_from_to.ToDate.AddHours(ds_from_to.ClosingTime), _
                                 ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                 ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' Stacker Identifier
    If Me.opt_none_stackers.Checked Then
      m_stacker_id = GLB_NLS_GUI_PLAYER_TRACKING.GetString(501)
    End If

    If Me.opt_one_stacker.Checked Then
      m_stacker_id = Me.cmb_stacker.TextValue
    End If

    If Me.opt_all_stackers.Checked Then
      m_stacker_id = Me.opt_all_stackers.Text
    End If

    ' Employee
    If Me.opt_one_employee.Checked Then
      m_employee_id = Me.cmb_employee.TextValue
    Else
      m_employee_id = Me.opt_all_employee.Text
    End If

    ' collection balance status

    If Me.chk_collection_status_not_closed.Checked Then
      m_status = Me.chk_collection_status_not_closed.Text
    End If

    If Me.chk_collection_status_incorrect.Checked Then
      If Not String.IsNullOrEmpty(m_status) Then
        m_status += ", "
      End If
      m_status += Me.chk_collection_status_incorrect.Text
    End If

    If Me.chk_collection_status_correct.Checked Then
      If Not String.IsNullOrEmpty(m_status) Then
        m_status += ", "
      End If
      m_status += Me.chk_collection_status_correct.Text
    End If

    ' Providers - Terminals
    If Me.uc_provider.opt_all_types.Checked Then
      m_terminals = Me.uc_provider.opt_all_types.Text
    ElseIf Me.uc_provider.opt_several_types.Checked Then
      m_terminals = Me.uc_provider.opt_several_types.Text
    Else
      m_terminals = Me.uc_provider.cmb_terminal.TextValue
    End If

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region 'Overrides

#Region " Private Functions and Methods "

  ' PURPOSE : Define layout of all Main Grid Columns
  '
  '  PARAMS :
  '     - INPUT :
  '           - None
  '     - OUTPUT :
  '           - None
  '
  ' RETURNS :
  '     - None
  '
  '   NOTES :
  '
  Private Sub GUI_StyleSheet()

    With Me.Grid
      Call .Init(GRID_COLUMNS, GRID_HEADER_ROWS)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = False

      .Column(GRID_COLUMN_STACKER_COLLECTION_ID).Width = SQL_COLUMN_STACKER_COLLECTION_ID

      .Column(GRID_COLUMN_STACKER_COLLECTION_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2223)
      .Column(GRID_COLUMN_STACKER_COLLECTION_DATE).Width = GRID_WIDTH_COLLECTION_DATE
      .Column(GRID_COLUMN_STACKER_COLLECTION_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_COLLECTION_EMPLOYEE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2224)
      .Column(GRID_COLUMN_COLLECTION_EMPLOYEE).Width = GRID_COLUMN_WIDTH_COLLECTION_EMPLOYEE
      .Column(GRID_COLUMN_COLLECTION_EMPLOYEE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_STACKER_IDENTIFIER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2230)
      .Column(GRID_COLUMN_STACKER_IDENTIFIER).Width = GRID_COLUMN_WIDTH_STACKER_IDENTIFIER
      .Column(GRID_COLUMN_STACKER_IDENTIFIER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_COLUMN_TERMINAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2225)
      .Column(GRID_COLUMN_TERMINAL).Width = GRID_WIDTH_TERMINAL
      .Column(GRID_COLUMN_TERMINAL).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      .Column(GRID_COLUMN_NUMBER_OF_BILLS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2226)
      .Column(GRID_COLUMN_NUMBER_OF_BILLS).Width = GRID_WIDTH_NUMBER_OF_BILLS
      .Column(GRID_COLUMN_NUMBER_OF_BILLS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_COLUMN_BILLS_TOTAL_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2227)
      .Column(GRID_COLUMN_BILLS_TOTAL_AMOUNT).Width = GRID_WIDTH_BILLS_TOTAL_AMOUNT
      .Column(GRID_COLUMN_BILLS_TOTAL_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_COLUMN_NUMBER_OF_TICKETS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2228)
      .Column(GRID_COLUMN_NUMBER_OF_TICKETS).Width = GRID_WIDTH_NUMBER_OF_TICKETS
      .Column(GRID_COLUMN_NUMBER_OF_TICKETS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_COLUMN_TICKETS_TOTAL_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2229)
      .Column(GRID_COLUMN_TICKETS_TOTAL_AMOUNT).Width = GRID_WIDTH_TICKETS_TOTAL_AMOUNT
      .Column(GRID_COLUMN_TICKETS_TOTAL_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      .Column(GRID_COLUMN_BALANCE_STATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2231)
      .Column(GRID_COLUMN_BALANCE_STATE).Width = GRID_WIDTH_BALANCE_STATE
      .Column(GRID_COLUMN_BALANCE_STATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

    Call SetDefaultDate()
    '' Stacker
    Me.opt_all_stackers.Checked = True
    Me.cmb_stacker.Enabled = False

    '' Terminal
    Call Me.uc_provider.SetDefaultValues()

    ''employee
    Me.opt_all_employee.Checked = True
    Me.cmb_employee.Enabled = False

    '' Balance
    Me.chk_collection_status_not_closed.Checked = False
    Me.chk_collection_status_correct.Checked = False
    Me.chk_collection_status_incorrect.Checked = False

  End Sub ' SetDefaultValues

  ' PURPOSE: Sets the defaul date values to the daily_session_selector control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultDate()

    Dim _today_opening As Date

    _today_opening = WSI.Common.Misc.TodayOpening()

    ds_from_to.FromDate = _today_opening
    ds_from_to.FromDateSelected = True
    ds_from_to.ToDate = _today_opening.AddDays(1)
    ds_from_to.ToDateSelected = True
    ds_from_to.ClosingTime = _today_opening.Hour

  End Sub ' SetDefaultDate

  ' PURPOSE: Get the sql 'where' statement
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - an string with the 'where' statement
  Private Function GetSqlWhere() As String

    Dim _str_where As String
    Dim _account_filter As String
    Dim _terminal_filter As String
    Dim _state_filter As String
    Dim _stacker_id_filter As String

    _str_where = ""
    _account_filter = ""
    _terminal_filter = ""
    _state_filter = ""
    _stacker_id_filter = ""

    'Filter Date
    _str_where = ds_from_to.GetSqlFilterCondition("MC_COLLECTION_DATETIME")

    If _str_where.Length > 0 Then
      If chk_collection_status_not_closed.Checked Or (Not chk_collection_status_not_closed.Checked And Not chk_collection_status_correct.Checked And Not chk_collection_status_incorrect.Checked) Then
        _str_where = " AND (" & _str_where & " OR MC_COLLECTION_DATETIME IS NULL) "
      Else
        _str_where = " AND (" & _str_where & ")"
      End If


    End If

    'Filter Employee
    If Me.opt_one_employee.Checked Then
      _account_filter = " GU_USER_ID = " & Me.cmb_employee.Value
    End If

    If _account_filter.Length > 0 Then
      _str_where = _str_where & " AND (" & _account_filter & ") "
    End If

    ' Filter Terminals
    If Me.uc_provider.opt_one_terminal.Checked Then
      _terminal_filter = " TE_TERMINAL_ID = " & Me.uc_provider.GetProviderIdListSelected()
    ElseIf Me.uc_provider.opt_several_types.Checked Then
      _terminal_filter = " TE_TERMINAL_ID IN " & Me.uc_provider.GetProviderIdListSelected()
    ElseIf Me.uc_provider.opt_all_types.Checked Then
      _terminal_filter = " TE_TERMINAL_ID IN " & Me.uc_provider.GetProviderIdListSelected() '_terminal_filter = ""
    End If

    If _terminal_filter.Length > 0 Then
      _str_where = _str_where & " AND (" & _terminal_filter & ") "
    End If

    ' Stacker filters
    If Me.opt_one_stacker.Checked Then
      _stacker_id_filter = " MC_STACKER_ID = " & Me.cmb_stacker.Value
    End If

    If Me.opt_none_stackers.Checked Then
      _stacker_id_filter = " MC_STACKER_ID IS NULL "
    End If

    If _stacker_id_filter.Length > 0 Then
      _str_where &= " AND (" & _stacker_id_filter & ") "
    End If

    'State filters
    If Me.chk_collection_status_not_closed.Checked Then
      _state_filter = " MC_BALANCED IS NULL "
    End If

    If Me.chk_collection_status_incorrect.Checked Then
      If Not String.IsNullOrEmpty(_state_filter) Then
        _state_filter += " OR "
      End If
      _state_filter += " MC_BALANCED = 0 "
    End If

    If Me.chk_collection_status_correct.Checked Then
      If Not String.IsNullOrEmpty(_state_filter) Then
        _state_filter += " OR "
      End If
      _state_filter += " MC_BALANCED = 1 "
    End If

    If _state_filter.Length > 0 Then
      _str_where &= " AND (" & _state_filter & ") "
    End If

    ' cashier session filter
    If m_cashier_session_id > 0 Then
      _str_where &= " AND (MC_CASHIER_SESSION_ID = " & m_cashier_session_id & ") "
    End If

    If Not String.IsNullOrEmpty(_str_where.ToString()) Then
      _str_where = _str_where.Remove(0, 4) ' Remove first " AND"
      _str_where = _str_where.Insert(0, " WHERE")
    End If

    Return _str_where

  End Function  'GetSqlWhere

  ' PURPOSE : Adds a new collection to the system
  '
  '  PARAMS :
  '     - INPUT :
  '
  '     - OUTPUT :
  '
  ' RETURNS :
  Private Sub GUI_ShowNewCollectionForm()
    Dim frm_edit As frm_new_collection_edit

    frm_edit = New frm_new_collection_edit
    Call frm_edit.ShowNewItem()

    frm_edit = Nothing
    Call Me.Grid.Focus()

  End Sub ' GUI_ShowNewCollectionForm

  Private Sub GUI_DeleteMoneyCollection(ByVal RowSelected As Int32)

    Dim _collection_id As Int64
    Dim _idx_row As Int32

    For _idx_row = 0 To Me.Grid.NumRows - 1
      If Me.Grid.Row(_idx_row).IsSelected Then
        Exit For
      End If
    Next

    If Grid.SelectedRows.Length < 1 Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(2040), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

    ElseIf Not IsDBNull(Me.Grid.Value(_idx_row, SQL_COLUMN_ID)) Then
      _collection_id = Me.Grid.Value(_idx_row, SQL_COLUMN_ID)

      Using _db_trx As New DB_TRX()
        If Not CkeckCollectionToDelete(_db_trx.SqlTransaction, _collection_id) Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2678), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)

        Else
          If DeleteCollection(_db_trx.SqlTransaction, _collection_id) = 1 Then
            If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2679), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_YES Then
              _db_trx.Commit()
              Me.Grid.DeleteRow(_idx_row)
            Else
              _db_trx.Rollback()
            End If
          Else
            _db_trx.Rollback()
          End If
        End If
      End Using
    End If

  End Sub

  Private Function CkeckCollectionToDelete(ByVal Transaction As SqlTransaction, ByVal CollectionId As Int64) As Boolean

    Dim _sb As StringBuilder
    Dim _table As DataTable

    _table = New DataTable()
    _sb = New StringBuilder()

    _sb.AppendLine("DECLARE   @id_stacker AS BIGINT                                                                     ")
    _sb.AppendLine("DECLARE   @bills_count AS INT                                                                       ")
    _sb.AppendLine("DECLARE   @tickets_count AS INT                                                                     ")
    _sb.AppendLine(" SELECT   @id_stacker = MC_STACKER_ID FROM MONEY_COLLECTIONS WHERE MC_COLLECTION_ID = @pCollectionId")
    _sb.AppendLine(" SELECT   @bills_count = COUNT(*) FROM TERMINAL_MONEY WHERE TM_COLLECTION_ID = @pCollectionId       ")
    _sb.AppendLine(" SELECT   @tickets_count = COUNT(*) FROM TICKETS WHERE TI_MONEY_COLLECTION_ID = @pCollectionId      ")
    _sb.AppendLine(" SELECT   @id_stacker, @bills_count, @tickets_count                                                 ")

    Using _cmd As New SqlCommand(_sb.ToString(), Transaction.Connection, Transaction)
      _cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = CollectionId

      Using _sql_da As New SqlDataAdapter(_cmd)
        _sql_da.Fill(_table)
      End Using
    End Using

    Return (IsDBNull(_table.Rows(0).Item(0)) AndAlso _table.Rows(0).Item(1) = 0 AndAlso _table.Rows(0).Item(2) = 0)
  End Function

  Private Function DeleteCollection(ByVal Transaction As SqlTransaction, ByVal CollectionId As Int64) As Int32

    Dim _sb As StringBuilder

    _sb = New StringBuilder()

    _sb.AppendLine("DELETE                                   ")
    _sb.AppendLine("   FROM MONEY_COLLECTIONS                 ")
    _sb.AppendLine("  WHERE MC_COLLECTION_ID = @pCollectionId ")
    Try
      Using _cmd As New SqlCommand(_sb.ToString(), Transaction.Connection, Transaction)
        _cmd.Parameters.Add("@pCollectionId", SqlDbType.BigInt).Value = CollectionId
        Return _cmd.ExecuteNonQuery()

      End Using

    Catch _exc As SqlException
      Return 0
    End Try

  End Function

#End Region 'privates functions and methods

#Region " Public Functions"

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    m_cashier_session_id = 0
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.MdiParent = MdiParent

    Call Me.Display(False)

  End Sub ' ShowForEdit

  ' PURPOSE: Opens dialog with default settings for View mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  Public Sub ShowForView(ByVal CashierSessionId As Long, ByVal CashierSessionName As String)

    m_cashier_session_id = CashierSessionId
    m_cashier_session_name = CashierSessionName
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION

    Call Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Events"

  Private Sub opt_one_employee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_one_employee.Click

    Me.cmb_employee.Enabled = True

  End Sub ' opt_one_employee_Click

  Private Sub opt_all_employee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_employee.Click

    Me.cmb_employee.Enabled = False

  End Sub ' opt_all_employee_Click

  Private Sub opt_one_stacker_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_one_stacker.Click

    Me.cmb_stacker.Enabled = True

  End Sub ' opt_one_stacker_Click

  Private Sub opt_all_stackers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles opt_all_stackers.Click

    Me.cmb_stacker.Enabled = False

  End Sub ' opt_all_stackers_Click

#End Region ' Events

End Class