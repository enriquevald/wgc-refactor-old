'-------------------------------------------------------------------
' Copyright � 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME : frm_ticket_audits_sel_unique
'   DESCRIPTION : Listado de registros de auditor�a de tickets - formulario child
'                 De este formulario heredan 2: uno que se lanza desde dentro de frm_tito_reports_sel y el otro desde el menu principal
'        AUTHOR : Jorge Concheyro
' CREATION DATE : 25-OCT-2016
'
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 25-OCT-2016  JRC    Initial version. PBI 19580:TITO: Pantalla de Log-Seguimiento de Tickets (Cambios Review)
' 07-FEB-2017  DHA    Bug 24309: Add Issued Amount column for Floor Dual Currency
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.Data.SqlClient
Imports WSI.Common
Imports GUI_Controls.frm_base_print
Imports GUI_Controls.frm_base_sel
Imports WSI.Common.TITO


Public Class frm_ticket_audits_sel_unique
  Inherits frm_ticket_audits_base

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub
  Friend WithEvents gb_TicketData As System.Windows.Forms.GroupBox
  Friend WithEvents ef_TicketNumberValue As GUI_Controls.uc_entry_field
  Friend WithEvents ef_TicketStatusValue As GUI_Controls.uc_entry_field
  Friend WithEvents ef_TicketAmountValue As GUI_Controls.uc_entry_field
  Friend WithEvents ef_TicketTypeValue As GUI_Controls.uc_entry_field
  Friend WithEvents ef_TicketTerminalValue As GUI_Controls.uc_entry_field
  Friend WithEvents ef_CreationAccountValue As GUI_Controls.uc_entry_field
  Friend WithEvents ef_CreationDateValue As GUI_Controls.uc_entry_field

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.gb_TicketData = New System.Windows.Forms.GroupBox()
    Me.ef_CreationDateValue = New GUI_Controls.uc_entry_field()
    Me.ef_CreationAccountValue = New GUI_Controls.uc_entry_field()
    Me.ef_TicketTerminalValue = New GUI_Controls.uc_entry_field()
    Me.ef_TicketTypeValue = New GUI_Controls.uc_entry_field()
    Me.ef_TicketAmountValue = New GUI_Controls.uc_entry_field()
    Me.ef_TicketStatusValue = New GUI_Controls.uc_entry_field()
    Me.ef_TicketNumberValue = New GUI_Controls.uc_entry_field()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_TicketData.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_TicketData)
    Me.panel_filter.Size = New System.Drawing.Size(1318, 150)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_TicketData, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 154)
    Me.panel_data.Size = New System.Drawing.Size(1318, 245)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1312, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1312, 4)
    '
    'gb_TicketData
    '
    Me.gb_TicketData.Controls.Add(Me.ef_CreationDateValue)
    Me.gb_TicketData.Controls.Add(Me.ef_CreationAccountValue)
    Me.gb_TicketData.Controls.Add(Me.ef_TicketTerminalValue)
    Me.gb_TicketData.Controls.Add(Me.ef_TicketTypeValue)
    Me.gb_TicketData.Controls.Add(Me.ef_TicketAmountValue)
    Me.gb_TicketData.Controls.Add(Me.ef_TicketStatusValue)
    Me.gb_TicketData.Controls.Add(Me.ef_TicketNumberValue)
    Me.gb_TicketData.Location = New System.Drawing.Point(9, 7)
    Me.gb_TicketData.Name = "gb_TicketData"
    Me.gb_TicketData.Size = New System.Drawing.Size(827, 129)
    Me.gb_TicketData.TabIndex = 18
    Me.gb_TicketData.TabStop = False
    Me.gb_TicketData.Text = "xTicketData"
    '
    'ef_CreationDateValue
    '
    Me.ef_CreationDateValue.DoubleValue = 0.0R
    Me.ef_CreationDateValue.IntegerValue = 0
    Me.ef_CreationDateValue.IsReadOnly = True
    Me.ef_CreationDateValue.Location = New System.Drawing.Point(442, 69)
    Me.ef_CreationDateValue.Name = "ef_CreationDateValue"
    Me.ef_CreationDateValue.PlaceHolder = Nothing
    Me.ef_CreationDateValue.Size = New System.Drawing.Size(368, 24)
    Me.ef_CreationDateValue.SufixText = "Sufix Text"
    Me.ef_CreationDateValue.SufixTextVisible = True
    Me.ef_CreationDateValue.TabIndex = 25
    Me.ef_CreationDateValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_CreationDateValue.TextValue = ""
    Me.ef_CreationDateValue.TextWidth = 150
    Me.ef_CreationDateValue.Value = ""
    Me.ef_CreationDateValue.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_CreationAccountValue
    '
    Me.ef_CreationAccountValue.DoubleValue = 0.0R
    Me.ef_CreationAccountValue.IntegerValue = 0
    Me.ef_CreationAccountValue.IsReadOnly = True
    Me.ef_CreationAccountValue.Location = New System.Drawing.Point(442, 43)
    Me.ef_CreationAccountValue.Name = "ef_CreationAccountValue"
    Me.ef_CreationAccountValue.PlaceHolder = Nothing
    Me.ef_CreationAccountValue.Size = New System.Drawing.Size(368, 24)
    Me.ef_CreationAccountValue.SufixText = "Sufix Text"
    Me.ef_CreationAccountValue.SufixTextVisible = True
    Me.ef_CreationAccountValue.TabIndex = 25
    Me.ef_CreationAccountValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_CreationAccountValue.TextValue = ""
    Me.ef_CreationAccountValue.TextWidth = 150
    Me.ef_CreationAccountValue.Value = ""
    Me.ef_CreationAccountValue.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_TicketTerminalValue
    '
    Me.ef_TicketTerminalValue.DoubleValue = 0.0R
    Me.ef_TicketTerminalValue.IntegerValue = 0
    Me.ef_TicketTerminalValue.IsReadOnly = True
    Me.ef_TicketTerminalValue.Location = New System.Drawing.Point(442, 17)
    Me.ef_TicketTerminalValue.Name = "ef_TicketTerminalValue"
    Me.ef_TicketTerminalValue.PlaceHolder = Nothing
    Me.ef_TicketTerminalValue.Size = New System.Drawing.Size(368, 24)
    Me.ef_TicketTerminalValue.SufixText = "Sufix Text"
    Me.ef_TicketTerminalValue.SufixTextVisible = True
    Me.ef_TicketTerminalValue.TabIndex = 25
    Me.ef_TicketTerminalValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_TicketTerminalValue.TextValue = ""
    Me.ef_TicketTerminalValue.TextWidth = 150
    Me.ef_TicketTerminalValue.Value = ""
    Me.ef_TicketTerminalValue.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_TicketTypeValue
    '
    Me.ef_TicketTypeValue.DoubleValue = 0.0R
    Me.ef_TicketTypeValue.IntegerValue = 0
    Me.ef_TicketTypeValue.IsReadOnly = True
    Me.ef_TicketTypeValue.Location = New System.Drawing.Point(43, 95)
    Me.ef_TicketTypeValue.Name = "ef_TicketTypeValue"
    Me.ef_TicketTypeValue.PlaceHolder = Nothing
    Me.ef_TicketTypeValue.Size = New System.Drawing.Size(368, 24)
    Me.ef_TicketTypeValue.SufixText = "Sufix Text"
    Me.ef_TicketTypeValue.SufixTextVisible = True
    Me.ef_TicketTypeValue.TabIndex = 25
    Me.ef_TicketTypeValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_TicketTypeValue.TextValue = ""
    Me.ef_TicketTypeValue.TextWidth = 150
    Me.ef_TicketTypeValue.Value = ""
    Me.ef_TicketTypeValue.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_TicketAmountValue
    '
    Me.ef_TicketAmountValue.DoubleValue = 0.0R
    Me.ef_TicketAmountValue.IntegerValue = 0
    Me.ef_TicketAmountValue.IsReadOnly = True
    Me.ef_TicketAmountValue.Location = New System.Drawing.Point(43, 69)
    Me.ef_TicketAmountValue.Name = "ef_TicketAmountValue"
    Me.ef_TicketAmountValue.PlaceHolder = Nothing
    Me.ef_TicketAmountValue.Size = New System.Drawing.Size(368, 24)
    Me.ef_TicketAmountValue.SufixText = "Sufix Text"
    Me.ef_TicketAmountValue.SufixTextVisible = True
    Me.ef_TicketAmountValue.TabIndex = 25
    Me.ef_TicketAmountValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_TicketAmountValue.TextValue = ""
    Me.ef_TicketAmountValue.TextWidth = 150
    Me.ef_TicketAmountValue.Value = ""
    Me.ef_TicketAmountValue.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_TicketStatusValue
    '
    Me.ef_TicketStatusValue.DoubleValue = 0.0R
    Me.ef_TicketStatusValue.IntegerValue = 0
    Me.ef_TicketStatusValue.IsReadOnly = True
    Me.ef_TicketStatusValue.Location = New System.Drawing.Point(43, 43)
    Me.ef_TicketStatusValue.Name = "ef_TicketStatusValue"
    Me.ef_TicketStatusValue.PlaceHolder = Nothing
    Me.ef_TicketStatusValue.Size = New System.Drawing.Size(368, 24)
    Me.ef_TicketStatusValue.SufixText = "Sufix Text"
    Me.ef_TicketStatusValue.SufixTextVisible = True
    Me.ef_TicketStatusValue.TabIndex = 25
    Me.ef_TicketStatusValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_TicketStatusValue.TextValue = ""
    Me.ef_TicketStatusValue.TextWidth = 150
    Me.ef_TicketStatusValue.Value = ""
    Me.ef_TicketStatusValue.ValueForeColor = System.Drawing.Color.Blue
    '
    'ef_TicketNumberValue
    '
    Me.ef_TicketNumberValue.DoubleValue = 0.0R
    Me.ef_TicketNumberValue.IntegerValue = 0
    Me.ef_TicketNumberValue.IsReadOnly = True
    Me.ef_TicketNumberValue.Location = New System.Drawing.Point(43, 17)
    Me.ef_TicketNumberValue.Name = "ef_TicketNumberValue"
    Me.ef_TicketNumberValue.PlaceHolder = Nothing
    Me.ef_TicketNumberValue.Size = New System.Drawing.Size(368, 24)
    Me.ef_TicketNumberValue.SufixText = "Sufix Text"
    Me.ef_TicketNumberValue.SufixTextVisible = True
    Me.ef_TicketNumberValue.TabIndex = 25
    Me.ef_TicketNumberValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_TicketNumberValue.TextValue = ""
    Me.ef_TicketNumberValue.TextWidth = 150
    Me.ef_TicketNumberValue.Value = ""
    Me.ef_TicketNumberValue.ValueForeColor = System.Drawing.Color.Blue
    '
    'frm_ticket_audits_sel_unique
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1326, 403)
    Me.Name = "frm_ticket_audits_sel_unique"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_TicketData.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

#End Region ' Constants



#Region " Members "

  Private m_print_datetime As Date
  Private m_ticket_number As String
  Private m_ticket_type As String
  Private m_ticket_status As String
  Private m_ticket_amount As String
  Private m_ticket_terminal As String
  Private m_ticket_creation_date_time As String
  Private m_ticket_account As String


#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()
    ' Asi deber�a ser pero para unificar los permisos
    'Me.FormId = ENUM_FORM.FORM_TICKET_AUDIT_SEL_UNIQUE
    'Le hacemos creer que es el otro form, que es el que tiene creado el permiso
    Me.FormId = ENUM_FORM.FORM_TICKET_AUDIT_SEL_SEVERAL

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialites controls settings
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7674)


    ' Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_FILTER_RESET).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = True

    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(6268)


    gb_TicketData.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2136)

    ef_TicketNumberValue.IsReadOnly = True
    ef_TicketStatusValue.IsReadOnly = True
    ef_TicketAmountValue.IsReadOnly = True
    ef_TicketTypeValue.IsReadOnly = True
    ef_TicketTerminalValue.IsReadOnly = True
    ef_CreationAccountValue.IsReadOnly = True
    ef_CreationDateValue.IsReadOnly = True

    ef_TicketNumberValue.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7664)
    ef_TicketStatusValue.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7666)
    ef_TicketAmountValue.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7665)
    ef_TicketTypeValue.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2112)
    ef_TicketTerminalValue.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7668)
    ef_CreationAccountValue.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7669)
    ef_CreationDateValue.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(455)

    ef_TicketNumberValue.Value = m_ticket_number
    ef_TicketStatusValue.Value = m_ticket_status
    ef_TicketAmountValue.Value = m_ticket_amount
    ef_TicketTypeValue.Value = m_ticket_type
    ef_TicketTerminalValue.Value = m_ticket_terminal
    ef_CreationDateValue.Value = m_ticket_creation_date_time

    If String.IsNullOrEmpty(m_ticket_account) Then
      ef_CreationAccountValue.Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(405)
    Else
      ef_CreationAccountValue.Value = m_ticket_account
    End If

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    ' 5 minutes timeout
    Call GUI_SetQueryTimeout(300)

    Call GUI_ButtonClick(ENUM_BUTTON.BUTTON_FILTER_APPLY)

  End Sub ' GUI_InitControls

  ' PURPOSE: Resets filters values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset


  ' PURPOSE: Checks filter's values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Function GUI_FilterCheck() As Boolean

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sb As System.Text.StringBuilder

    _sb = New System.Text.StringBuilder()

    _sb.AppendLine(MyBase.GUI_FilterGetSqlQuery())
    _sb.AppendLine(GetSqlWhere())
    _sb.AppendLine("    ORDER BY   TIA_VALIDATION_NUMBER, TIA_INSERT_DATE                                                                                                                ")



    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery



  ' PURPOSE: Sets initial focus
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  

  Protected Overrides Sub GUI_SetInitialFocus()
  End Sub 'GUI_SetInitialFocus


#Region " GUI Reports "

  ' PURPOSE: Sets report filters values
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7664), m_ticket_number)
    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 3000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Sets report filters values
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData 
  '           - FirstColIndex  
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Sets report filters values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_ReportUpdateFilters()


  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region  ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overloads Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, ByVal TicketValidationNumber As String, ByVal TicketStatus As String, ByVal TicketAmount As String _
                                   , ByVal TicketType As String, ByVal TicketTerminal As String, ByVal TicketCreationDateTime As String, ByVal TicketAccount As String)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    m_ticket_number = TicketValidationNumber
    m_ticket_status = TicketStatus
    m_ticket_amount = TicketAmount
    m_ticket_type = TicketType
    m_ticket_terminal = TicketTerminal
    m_ticket_creation_date_time = TicketCreationDateTime
    m_ticket_account = TicketAccount

    Me.Display(False)
  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_StyleSheet()

    MyBase.GUI_StyleSheet()
    With Me.Grid
      ' ticket number
      .Column(GRID_COLUMN_VALIDATION_NUMBER).Width = 0

      ' Created amount
      .Column(GRID_COLUMN_AMOUNT_AMT0).Width = 0

      ' amount
      .Column(GRID_COLUMN_AMOUNT).Width = 0

      ' type
      .Column(GRID_COLUMN_TITO_TYCKET_TYPE).Width = 0

      '.Column(GRID_COLUMN_CREATED_DATETIME).Width = 0
      '.Column(GRID_COLUMN_EXPIRATION_DATETIME).Width = 0
      '.Column(GRID_COLUMN_LAST_ACTION_DATETIME).Width = 0


    End With
  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

  End Sub ' SetDefaultValues

  ' PURPOSE: Get Sql WHERE to buil SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim _str_where As String = ""
    Dim _where_added As Boolean
    Dim _ticket_type As String

    _where_added = False
    _ticket_type = ""


    If _where_added = False Then
      _str_where = _str_where & " WHERE "
      _where_added = True
    Else
      _str_where = _str_where & " AND "
    End If

    _str_where = _str_where & " TIA_VALIDATION_NUMBER = " & m_ticket_number



    If _ticket_type.Length > 0 Then
      _str_where = (IIf(_str_where.Length > 0, _str_where & " AND ", _str_where))
      _str_where = _str_where & "(" & _ticket_type & ") "
    End If



    Return _str_where
  End Function ' GetSqlWhere


#End Region  ' Private Functions

#Region "Events"

#End Region ' Events

End Class
