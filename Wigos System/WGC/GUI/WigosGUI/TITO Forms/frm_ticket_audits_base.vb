'-------------------------------------------------------------------
' Copyright � 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME : frm_ticket_audits_base
'   DESCRIPTION : Listado de registros de auditor�a de tickets - formulario base
'                 De este formulario heredan 2: uno que se lanza desde dentro de frm_tito_reports_sel y el otro desde el menu principal
'        AUTHOR : Jorge Concheyro
' CREATION DATE : 25-OCT-2016
'
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 25-OCT-2016  JRC    Initial version. PBI 19580:TITO: Pantalla de Log-Seguimiento de Tickets (Cambios Review)
' 07-FEB-2017  DHA    Bug 24309: Add Issued Amount column for Floor Dual Currency
' 19-SEP-2017  RAB    Bug 29837:WIGOS-4012 Accounting - TITO tickets audit does not show account creator in all movements when are created from a Cashier
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.Data.SqlClient
Imports WSI.Common
Imports GUI_Controls.frm_base_print
Imports GUI_Controls.frm_base_sel
Imports WSI.Common.TITO


Public Class frm_ticket_audits_base
  Inherits frm_base_sel

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents lb_session_id As System.Windows.Forms.Label
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.lb_session_id = New System.Windows.Forms.Label()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lb_session_id)
    Me.panel_filter.Size = New System.Drawing.Size(1116, 216)
    Me.panel_filter.Controls.SetChildIndex(Me.lb_session_id, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 220)
    Me.panel_data.Size = New System.Drawing.Size(1116, 303)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1110, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1110, 4)
    '
    'lb_session_id
    '
    Me.lb_session_id.AutoSize = True
    Me.lb_session_id.Location = New System.Drawing.Point(34, 93)
    Me.lb_session_id.Name = "lb_session_id"
    Me.lb_session_id.Size = New System.Drawing.Size(0, 13)
    Me.lb_session_id.TabIndex = 6
    '
    'frm_ticket_audits_base
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1124, 527)
    Me.Name = "frm_ticket_audits_base"
    Me.Text = "frm_ticket_audits"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "

  Protected Const GRID_COLUMNS As Integer = 10
  Protected Const GRID_HEADER_ROWS As Integer = 2

  'DB Columns

  Protected Const SQL_COLUMN_AUDIT_DATE As Integer = 0
  Protected Const SQL_COLUMN_AMOUNT_AMT0 As Integer = 1
  Protected Const SQL_COLUMN_AMOUNT_CUR0 As Integer = 2
  Protected Const SQL_COLUMN_AMOUNT As Integer = 3
  Protected Const SQL_COLUMN_TITO_TICKET_TYPE As Integer = 4
  Protected Const SQL_COLUMN_TITO_OLD_STATE As Integer = 5
  Protected Const SQL_COLUMN_TITO_STATE As Integer = 6
  Protected Const SQL_COLUMN_TERMINAL_CREATION As Integer = 7
  Protected Const SQL_COLUMN_ACCOUNT_CREATION As Integer = 8
  Protected Const SQL_COLUMN_COLLECTED_STATUS = 9
  Protected Const SQL_COLUMN_EXPIRATION_DATE_TIME = 10
  Protected Const SQL_COLUMN_CAGE_SESSION_NAME = 11
  Protected Const SQL_COLUMN_REJECT_REASON_EGM = 12
  Protected Const SQL_COLUMN_REJECT_REASON_WCP = 13
  Protected Const SQL_COLUMN_VALIDATION_NUMBER = 14

  'Grid Columns
  Protected Const GRID_COLUMN_INDEX As Integer = 0
  Protected Const GRID_COLUMN_AUDIT_DATE As Integer = 1
  Protected Const GRID_COLUMN_VALIDATION_NUMBER As Integer = 2

  Protected Const GRID_COLUMN_AMOUNT_AMT0 As Integer = 3
  Protected Const GRID_COLUMN_AMOUNT As Integer = 4
  Protected Const GRID_COLUMN_TITO_TYCKET_TYPE As Integer = 5
  Protected Const GRID_COLUMN_OLD_STATE As Integer = 6
  Protected Const GRID_COLUMN_STATE As Integer = 7
  Protected Const GRID_COLUMN_DESCRIPTION = 8
  Protected Const GRID_COLUMN_REJECT_REASON_EGM = 9




  ' Width
  Protected Const GRID_WIDTH_INDEX As Integer = 200
  Protected Const GRID_WIDTH_AUDIT_DATE As Integer = 2280
  Protected Const GRID_WIDTH_VALIDATION_NUMBER As Integer = 2000

  Protected Const GRID_WIDTH_CREATED_DATETIME As Integer = 2280
  Protected Const GRID_WIDTH_LAST_ACTION_DATETIME As Integer = 2280
  Protected Const GRID_WIDTH_EXPIRATION_DATETIME As Integer = 2280

  Protected Const GRID_WIDTH_TITO_AMT0 As Int32 = 1900
  Protected Const GRID_WIDTH_AMOUNT As Integer = 1100
  Protected Const GRID_WIDTH_TITO_TYCKET_TYPE As Integer = 1605
  Protected Const GRID_WIDTH_STATE As Integer = 2865
  Protected Const GRID_WIDTH_DESCRIPTION = 6000
  Protected Const GRID_WIDTH_REJECT_REASON = 12090

#End Region ' Constants



#Region " Members "

  Private m_print_datetime As Date
  Private m_ticket_number As String
  Private m_ticket_type As String


  ' For report filters 
  Private m_date_from As String
  Private m_date_to As String

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TICKET_AUDIT_SEL

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialites controls settings
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_InitControls()


    Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7674)


    ' Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_FILTER_RESET).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = True

    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

    ' 5 minutes timeout
    Call GUI_SetQueryTimeout(300)



  End Sub ' GUI_InitControls

  ' PURPOSE: Resets filters values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()

  End Sub ' GUI_FilterReset


  ' PURPOSE: Checks filter's values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Function GUI_FilterCheck() As Boolean

    Return True
  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sb As System.Text.StringBuilder
    Dim _nat_curr As String

    _nat_curr = GeneralParam.GetString("RegionalOptions", "CurrencyISOCode", "   ")

    _sb = New System.Text.StringBuilder()

    _sb.AppendLine("    	SELECT                                                                                                                                                ")
    _sb.AppendLine("		            TIA_INSERT_DATE                                                                                                                             ")
    _sb.AppendLine("		          , ISNULL(TI_AMT0, TI_AMOUNT) AS TI_AMT0                                                                                                       ")
    _sb.AppendLine("		          , ISNULL(TI_CUR0, '" & _nat_curr & "') AS TI_CUR0                                                                                             ")
    _sb.AppendLine("  	          , TIA_AMOUNT                                                                                                                                  ")
    _sb.AppendLine("	  	        , TIA_TYPE_ID                                                                                                                                 ")
    _sb.AppendLine("              , TIA_OLD_STATUS                                                                                                                              ")
    _sb.AppendLine("              , TIA_STATUS                                                                                                                                  ")
    _sb.AppendLine("	            , CASE WHEN tia_last_action_terminal_id IS NULL                                                                                               ")
    _sb.AppendLine("                THEN ISNULL((SELECT TE_NAME FROM terminals WHERE te_terminal_id = TIA_CREATED_TERMINAL_ID),                                                 ")
    _sb.AppendLine("                		        (SELECT CT_NAME FROM cashier_terminals WHERE ct_cashier_id = TIA_CREATED_TERMINAL_ID))                                          ")
    _sb.AppendLine("	              ELSE ISNULL((SELECT TE_NAME FROM terminals WHERE te_terminal_id = TIA_LAST_ACTION_TERMINAL_ID),                                             ")
    _sb.AppendLine("                            (SELECT CT_NAME FROM cashier_terminals WHERE ct_cashier_id = ISNULL(TIA_LAST_ACTION_TERMINAL_ID, tia_created_terminal_id)))     ")
    _sb.AppendLine("                END AS TE_TERMINAL_NAME                                                                                                                     ")
    _sb.AppendLine("  	          , ACCOUNTS.AC_HOLDER_NAME                                                                                                                     ")
    _sb.AppendLine("  	          , TIA_COLLECTED                                                                                                                               ")
    _sb.AppendLine("              , TIA_EXPIRATION_DATETIME                                                                                                                     ")
    _sb.AppendLine("              , CAGE_SESSIONS.CGS_SESSION_NAME                                                                                                              ")
    _sb.AppendLine("              , TIA_REJECT_REASON_EGM                                                                                                                       ")
    _sb.AppendLine("              , TIA_REJECT_REASON_WCP                                                                                                                       ")
    _sb.AppendLine("              , TIA_VALIDATION_NUMBER                                                                                                                       ")

    _sb.AppendLine("       FROM TICKETS_AUDIT_STATUS_CHANGE                                                                                                                     ")
    _sb.AppendLine(" INNER JOIN   TICKETS ON TIA_TICKET_ID = TI_TICKET_ID                                                                                                       ")
    _sb.AppendLine("	LEFT JOIN   ACCOUNTS ON ACCOUNTS.AC_ACCOUNT_ID = TICKETS_AUDIT_STATUS_CHANGE.TIA_CREATED_ACCOUNT_ID                                                       ")
    _sb.AppendLine("	LEFT JOIN   CAGE_MOVEMENTS ON TICKETS_AUDIT_STATUS_CHANGE.TIA_CAGE_MOVEMENT_ID = CAGE_MOVEMENTS.CGM_MOVEMENT_ID                                           ")
    _sb.AppendLine("	LEFT JOIN   CAGE_SESSIONS ON CAGE_SESSIONS.CGS_CAGE_SESSION_ID = CAGE_MOVEMENTS.CGM_CAGE_SESSION_ID                                                       ")



    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery

  ' PURPOSE : Sets the values of a row in the data grid
  '
  '  PARAMS :
  '     - INPUT :
  '           - RowIndex
  '           - DbRow
  '
  '     - OUTPUT :
  '
  ' RETURNS : 
  '     - True: the row could be added
  '     - False: the row could not be added
  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _terminal As String
    Dim _account As String
    Dim _error_msg_wcp As String
    Dim _error_msg_sashost As String
    _terminal = ""
    _account = ""
    _error_msg_wcp = ""
    _error_msg_sashost = ""



    With Me.Grid

      'audit date 
      .Cell(RowIndex, GRID_COLUMN_AUDIT_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_AUDIT_DATE), _
                                                                            ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                            ENUM_FORMAT_TIME.FORMAT_HHMMSS)


      'ticket number
      .Cell(RowIndex, GRID_COLUMN_VALIDATION_NUMBER).Value = DbRow.Value(SQL_COLUMN_VALIDATION_NUMBER)

      'ticket amount created amount
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_AMOUNT_AMT0)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_AMOUNT_AMT0).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_AMOUNT_AMT0), ENUM_GROUP_DIGITS.GROUP_DIGITS_DEFAULT) & " " & DbRow.Value(SQL_COLUMN_AMOUNT_CUR0)
      End If

      'ticket amount
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_AMOUNT)) Then
        .Cell(RowIndex, GRID_COLUMN_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_AMOUNT))
      End If


      'TITO ticket type
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_TITO_TICKET_TYPE)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_TITO_TYCKET_TYPE).Value = TicketTypeAsString(DbRow.Value(SQL_COLUMN_TITO_TICKET_TYPE))
      End If

      'ticket previous status
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_TITO_OLD_STATE)) Then
        .Cell(RowIndex, GRID_COLUMN_OLD_STATE).Value = TicketStatusAsString(DbRow.Value(SQL_COLUMN_TITO_OLD_STATE))
      End If

      'ticket actual status                            
      If Not IsDBNull(DbRow.Value(SQL_COLUMN_TITO_STATE)) Then
        .Cell(RowIndex, GRID_COLUMN_STATE).Value = TicketStatusAsString(DbRow.Value(SQL_COLUMN_TITO_STATE))
      End If

      'description    

      _terminal = GLB_NLS_GUI_PLAYER_TRACKING.GetString(592) & ": " & DbRow.Value(SQL_COLUMN_TERMINAL_CREATION)

      If Not IsDBNull(DbRow.Value(SQL_COLUMN_ACCOUNT_CREATION)) Then
        _account = GLB_NLS_GUI_PLAYER_TRACKING.GetString(1202) & ": " & DbRow.Value(SQL_COLUMN_ACCOUNT_CREATION)
      Else
        _account = GLB_NLS_GUI_PLAYER_TRACKING.GetString(405)
      End If


      Select Case (DbRow.Value(SQL_COLUMN_TITO_STATE))
        Case WSI.Common.TITO_TICKET_STATUS.PENDING_PRINT,
             WSI.Common.TITO_TICKET_STATUS.VALID,
             WSI.Common.TITO_TICKET_STATUS.CANCELED,
             WSI.Common.TITO_TICKET_STATUS.PENDING_CANCEL,
             WSI.Common.TITO_TICKET_STATUS.DISCARDED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_DESCRIPTION).Value = _terminal & ", " & _account
        Case WSI.Common.TITO_TICKET_STATUS.EXPIRED
          Me.Grid.Cell(RowIndex, GRID_COLUMN_DESCRIPTION).Value = _terminal & ", " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(437) & ": " & GUI_FormatDate(DbRow.Value(SQL_COLUMN_EXPIRATION_DATE_TIME), _
                                                                            ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, _
                                                                            ENUM_FORMAT_TIME.FORMAT_HHMMSS)
        Case Else
          Me.Grid.Cell(RowIndex, GRID_COLUMN_DESCRIPTION).Value = _terminal
      End Select


      If Not IsDBNull(DbRow.Value(SQL_COLUMN_CAGE_SESSION_NAME)) Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_DESCRIPTION).Value = Me.Grid.Cell(RowIndex, GRID_COLUMN_DESCRIPTION).Value & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(3388) & ": " & DbRow.Value(SQL_COLUMN_CAGE_SESSION_NAME)
      End If

      If (Not IsDBNull(DbRow.Value(SQL_COLUMN_REJECT_REASON_EGM))) AndAlso Convert.ToInt32(DbRow.Value(SQL_COLUMN_REJECT_REASON_EGM)) > 2 Then    'los 3 primeros no se muestran porque son OK
        _error_msg_sashost = "EGM" + ": " + Hex(Convert.ToInt32(DbRow.Value(SQL_COLUMN_REJECT_REASON_EGM))) + " " +
        SasHostRejectReasonAsString(DbRow.Value(SQL_COLUMN_REJECT_REASON_EGM))
      End If

      If (Not IsDBNull(DbRow.Value(SQL_COLUMN_REJECT_REASON_WCP))) AndAlso Convert.ToInt32(DbRow.Value(SQL_COLUMN_REJECT_REASON_WCP)) > 2 Then    'los 3 primeros no se muestran porque son OK
        _error_msg_wcp = "WCP" + ": " + Hex(Convert.ToInt32(DbRow.Value(SQL_COLUMN_REJECT_REASON_WCP))) + " " +
        WcpRejectReasonAsString(DbRow.Value(SQL_COLUMN_REJECT_REASON_WCP))
      End If

      If _error_msg_wcp.Length > 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_REJECT_REASON_EGM).Value = _error_msg_wcp
      End If
      If _error_msg_sashost.Length > 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_REJECT_REASON_EGM).Value = _error_msg_sashost
      End If
      If _error_msg_sashost.Length > 0 And _error_msg_wcp.Length > 0 Then
        Me.Grid.Cell(RowIndex, GRID_COLUMN_REJECT_REASON_EGM).Value = _error_msg_wcp + " ; " + _error_msg_sashost
      End If






    End With

    Return True

  End Function ' GUI_SetupRow

  ' PURPOSE: Sets initial focus
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  

  Protected Overrides Sub GUI_SetInitialFocus()

  End Sub 'GUI_SetInitialFocus


#Region " GUI Reports "

  ' PURPOSE: Sets report filters values
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7664), m_ticket_number)

    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 3000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Sets report filters values
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData 
  '           - FirstColIndex  
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_ReportParams(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA, _
                                           Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(PrintData)

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

  End Sub ' GUI_ReportParams

  ' PURPOSE: Sets report filters values
  '
  '  PARAMS:
  '     - INPUT:
  '           - ExcelData 
  '           - FirstColIndex  
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)
    Call MyBase.GUI_ReportParams(ExcelData)
    ExcelData.SetColumnFormat(1, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.TEXT) 'ticket number

  End Sub



  ' PURPOSE: Sets report filters values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_ReportUpdateFilters()

  End Sub ' GUI_ReportUpdateFilters

#End Region ' GUI Reports

#End Region  ' Overrides

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window, ByVal TicketValidationNumber As String)
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_SELECTION
    m_ticket_number = TicketValidationNumber
    Me.MdiParent = MdiParent
    Me.Display(False)
  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overridable Sub GUI_StyleSheet()
    Dim _total_columns As Integer

    _total_columns = GRID_COLUMNS

    With Me.Grid
      Call .Init(_total_columns, GRID_HEADER_ROWS)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE


      ' Index
      .Column(GRID_COLUMN_INDEX).Header(0).Text = ""
      .Column(GRID_COLUMN_INDEX).Header(1).Text = ""
      .Column(GRID_COLUMN_INDEX).Width = GRID_WIDTH_INDEX
      .Column(GRID_COLUMN_INDEX).HighLightWhenSelected = False
      .Column(GRID_COLUMN_INDEX).IsColumnPrintable = False

      ' audit date
      .Column(GRID_COLUMN_AUDIT_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7675)
      .Column(GRID_COLUMN_AUDIT_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7663)
      .Column(GRID_COLUMN_AUDIT_DATE).Width = GRID_WIDTH_AUDIT_DATE
      .Column(GRID_COLUMN_AUDIT_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' ticket number
      .Column(GRID_COLUMN_VALIDATION_NUMBER).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7670)
      .Column(GRID_COLUMN_VALIDATION_NUMBER).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7664)
      .Column(GRID_COLUMN_VALIDATION_NUMBER).Width = GRID_WIDTH_VALIDATION_NUMBER
      .Column(GRID_COLUMN_VALIDATION_NUMBER).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT
      .Column(GRID_COLUMN_VALIDATION_NUMBER).ColDataType = uc_grid.CLASS_COL_DATA.ENUM_COLDATATYPE.FLEX_DT_STRING


      ' creation date
      '.Column(GRID_COLUMN_CREATED_DATETIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7670)
      '.Column(GRID_COLUMN_CREATED_DATETIME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2108)
      '.Column(GRID_COLUMN_CREATED_DATETIME).Width = GRID_WIDTH_CREATED_DATETIME
      '.Column(GRID_COLUMN_CREATED_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' expiration date
      '.Column(GRID_COLUMN_EXPIRATION_DATETIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7670)
      '.Column(GRID_COLUMN_EXPIRATION_DATETIME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2123)
      '.Column(GRID_COLUMN_EXPIRATION_DATETIME).Width = GRID_WIDTH_EXPIRATION_DATETIME
      '.Column(GRID_COLUMN_EXPIRATION_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' expiration date
      '.Column(GRID_COLUMN_LAST_ACTION_DATETIME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7670)
      '.Column(GRID_COLUMN_LAST_ACTION_DATETIME).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2120)
      '.Column(GRID_COLUMN_LAST_ACTION_DATETIME).Width = GRID_WIDTH_LAST_ACTION_DATETIME
      '.Column(GRID_COLUMN_LAST_ACTION_DATETIME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Created Amount AMT0
      .Column(GRID_COLUMN_AMOUNT_AMT0).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7670)
      If (WSI.Common.Misc.IsFloorDualCurrencyEnabled) Then
        .Column(GRID_COLUMN_AMOUNT_AMT0).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7104) 'Created amount
        .Column(GRID_COLUMN_AMOUNT_AMT0).Width = GRID_WIDTH_TITO_AMT0
        .Column(GRID_COLUMN_AMOUNT_AMT0).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
      Else
        .Column(GRID_COLUMN_AMOUNT_AMT0).Header(1).Text = "" 'Created amount
        .Column(GRID_COLUMN_AMOUNT_AMT0).Width = 0 'GRID_WIDTH_TITO_AMT0
      End If

      ' amount
      .Column(GRID_COLUMN_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7670)
      .Column(GRID_COLUMN_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7665)
      .Column(GRID_COLUMN_AMOUNT).Width = GRID_WIDTH_AMOUNT
      .Column(GRID_COLUMN_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT


      ' type
      .Column(GRID_COLUMN_TITO_TYCKET_TYPE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7670)
      .Column(GRID_COLUMN_TITO_TYCKET_TYPE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(254)
      .Column(GRID_COLUMN_TITO_TYCKET_TYPE).Width = GRID_WIDTH_TITO_TYCKET_TYPE
      .Column(GRID_COLUMN_TITO_TYCKET_TYPE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT


      ' old state
      .Column(GRID_COLUMN_OLD_STATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7670)
      .Column(GRID_COLUMN_OLD_STATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7702)
      .Column(GRID_COLUMN_OLD_STATE).Width = GRID_WIDTH_STATE
      .Column(GRID_COLUMN_OLD_STATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT




      ' state
      .Column(GRID_COLUMN_STATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7670)
      .Column(GRID_COLUMN_STATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7703) '//TODO
      .Column(GRID_COLUMN_STATE).Width = GRID_WIDTH_STATE
      .Column(GRID_COLUMN_STATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      ' description
      .Column(GRID_COLUMN_DESCRIPTION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7670)
      .Column(GRID_COLUMN_DESCRIPTION).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(688)
      .Column(GRID_COLUMN_DESCRIPTION).Width = GRID_WIDTH_DESCRIPTION
      .Column(GRID_COLUMN_DESCRIPTION).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT


      ' description
      .Column(GRID_COLUMN_REJECT_REASON_EGM).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7670)
      .Column(GRID_COLUMN_REJECT_REASON_EGM).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7704) '//TODO
      .Column(GRID_COLUMN_REJECT_REASON_EGM).Width = GRID_WIDTH_REJECT_REASON
      .Column(GRID_COLUMN_REJECT_REASON_EGM).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT





    End With
  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()

  End Sub ' SetDefaultValues







#End Region  ' Private Functions

#Region "Events"

#End Region ' Events

End Class
