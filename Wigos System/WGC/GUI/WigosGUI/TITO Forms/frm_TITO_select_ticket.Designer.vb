<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_TITO_select_ticket
  Inherits GUI_Controls.frm_base_print

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.ef_validation_number = New GUI_Controls.uc_entry_field()
    Me.gr_tickets = New GUI_Controls.uc_grid()
    Me.panel_grids.SuspendLayout()
    Me.panel_filter.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_grids
    '
    Me.panel_grids.Controls.Add(Me.gr_tickets)
    Me.panel_grids.Location = New System.Drawing.Point(4, 80)
    Me.panel_grids.Size = New System.Drawing.Size(736, 157)
    Me.panel_grids.Controls.SetChildIndex(Me.gr_tickets, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.panel_buttons, 0)
    '
    'panel_buttons
    '
    Me.panel_buttons.Location = New System.Drawing.Point(648, 0)
    Me.panel_buttons.Size = New System.Drawing.Size(88, 157)
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.ef_validation_number)
    Me.panel_filter.Size = New System.Drawing.Size(736, 59)
    Me.panel_filter.Controls.SetChildIndex(Me.ef_validation_number, 0)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(4, 63)
    Me.pn_separator_line.Size = New System.Drawing.Size(736, 17)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(736, 4)
    '
    'ef_validation_number
    '
    Me.ef_validation_number.DoubleValue = 0.0R
    Me.ef_validation_number.IntegerValue = 0
    Me.ef_validation_number.IsReadOnly = False
    Me.ef_validation_number.Location = New System.Drawing.Point(-15, 16)
    Me.ef_validation_number.Name = "ef_validation_number"
    Me.ef_validation_number.PlaceHolder = Nothing
    Me.ef_validation_number.Size = New System.Drawing.Size(285, 24)
    Me.ef_validation_number.SufixText = "Sufix Text"
    Me.ef_validation_number.SufixTextVisible = True
    Me.ef_validation_number.TabIndex = 11
    Me.ef_validation_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
    Me.ef_validation_number.TextValue = ""
    Me.ef_validation_number.Value = ""
    Me.ef_validation_number.ValueForeColor = System.Drawing.Color.Blue
    '
    'gr_tickets
    '
    Me.gr_tickets.CurrentCol = -1
    Me.gr_tickets.CurrentRow = -1
    Me.gr_tickets.EditableCellBackColor = System.Drawing.Color.Empty
    Me.gr_tickets.EditableCellBorderColor = System.Drawing.Color.Empty
    Me.gr_tickets.EditableCellShowMode = GUI_Controls.uc_grid.GRID_SHOW_EDIT_MODE.SHOW_NONE
    Me.gr_tickets.Location = New System.Drawing.Point(16, 6)
    Me.gr_tickets.Name = "gr_tickets"
    Me.gr_tickets.PanelRightVisible = False
    Me.gr_tickets.Redraw = True
    Me.gr_tickets.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.gr_tickets.Size = New System.Drawing.Size(597, 148)
    Me.gr_tickets.Sortable = False
    Me.gr_tickets.SortAscending = True
    Me.gr_tickets.SortByCol = 0
    Me.gr_tickets.TabIndex = 9
    Me.gr_tickets.ToolTipped = True
    Me.gr_tickets.TopRow = -2
    Me.gr_tickets.WordWrap = False
    '
    'frm_TITO_select_ticket
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(744, 241)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frm_TITO_select_ticket"
    Me.Text = "frm_TITO_select_ticket"
    Me.panel_grids.ResumeLayout(False)
    Me.panel_filter.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ef_validation_number As GUI_Controls.uc_entry_field
  Friend WithEvents gr_tickets As GUI_Controls.uc_grid
End Class
