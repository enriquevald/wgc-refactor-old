<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_tito_bills_tickets_balance_report_sel
  Inherits GUI_Controls.frm_base_print

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.lbl_cs_name = New GUI_Controls.uc_text_field
    Me.gb_status = New System.Windows.Forms.GroupBox
    Me.lbl_color_completed = New System.Windows.Forms.Label
    Me.lbl_color_pending = New System.Windows.Forms.Label
    Me.chk_completed = New System.Windows.Forms.CheckBox
    Me.lbl_color_gap = New System.Windows.Forms.Label
    Me.chk_pending = New System.Windows.Forms.CheckBox
    Me.chk_gap = New System.Windows.Forms.CheckBox
    Me.tab_totals = New System.Windows.Forms.TabControl
    Me.tab_tickets = New System.Windows.Forms.TabPage
    Me.dg_tickets = New GUI_Controls.uc_grid
    Me.tab_bills = New System.Windows.Forms.TabPage
    Me.dg_bills = New GUI_Controls.uc_grid
    Me.panel_grids.SuspendLayout()
    Me.panel_filter.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_status.SuspendLayout()
    Me.tab_totals.SuspendLayout()
    Me.tab_tickets.SuspendLayout()
    Me.tab_bills.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_grids
    '
    Me.panel_grids.Controls.Add(Me.tab_totals)
    Me.panel_grids.Location = New System.Drawing.Point(4, 127)
    Me.panel_grids.Size = New System.Drawing.Size(980, 460)
    Me.panel_grids.Controls.SetChildIndex(Me.tab_totals, 0)
    Me.panel_grids.Controls.SetChildIndex(Me.panel_buttons, 0)
    '
    'panel_buttons
    '
    Me.panel_buttons.Location = New System.Drawing.Point(892, 0)
    Me.panel_buttons.Size = New System.Drawing.Size(88, 460)
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.lbl_cs_name)
    Me.panel_filter.Controls.Add(Me.gb_status)
    Me.panel_filter.Size = New System.Drawing.Size(980, 100)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.lbl_cs_name, 0)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Location = New System.Drawing.Point(4, 104)
    Me.pn_separator_line.Size = New System.Drawing.Size(980, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(980, 4)
    '
    'lbl_cs_name
    '
    Me.lbl_cs_name.IsReadOnly = True
    Me.lbl_cs_name.LabelAlign = GUI_Controls.uc_text_field.ENUM_ALIGN.ALIGN_LEFT
    Me.lbl_cs_name.LabelForeColor = System.Drawing.SystemColors.ControlText
    Me.lbl_cs_name.Location = New System.Drawing.Point(156, 12)
    Me.lbl_cs_name.Name = "lbl_cs_name"
    Me.lbl_cs_name.Size = New System.Drawing.Size(297, 24)
    Me.lbl_cs_name.SufixText = "Sufix Text"
    Me.lbl_cs_name.SufixTextVisible = True
    Me.lbl_cs_name.TabIndex = 12
    Me.lbl_cs_name.TextWidth = 0
    Me.lbl_cs_name.Value = "xCashier Session Name"
    '
    'gb_status
    '
    Me.gb_status.Controls.Add(Me.lbl_color_completed)
    Me.gb_status.Controls.Add(Me.lbl_color_pending)
    Me.gb_status.Controls.Add(Me.chk_completed)
    Me.gb_status.Controls.Add(Me.lbl_color_gap)
    Me.gb_status.Controls.Add(Me.chk_pending)
    Me.gb_status.Controls.Add(Me.chk_gap)
    Me.gb_status.Location = New System.Drawing.Point(6, 2)
    Me.gb_status.Name = "gb_status"
    Me.gb_status.Size = New System.Drawing.Size(144, 88)
    Me.gb_status.TabIndex = 11
    Me.gb_status.TabStop = False
    Me.gb_status.Text = "xStatus"
    '
    'lbl_color_completed
    '
    Me.lbl_color_completed.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_completed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_completed.Location = New System.Drawing.Point(6, 18)
    Me.lbl_color_completed.Name = "lbl_color_completed"
    Me.lbl_color_completed.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_completed.TabIndex = 0
    '
    'lbl_color_pending
    '
    Me.lbl_color_pending.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_pending.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_pending.Location = New System.Drawing.Point(6, 40)
    Me.lbl_color_pending.Name = "lbl_color_pending"
    Me.lbl_color_pending.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_pending.TabIndex = 2
    '
    'chk_completed
    '
    Me.chk_completed.AutoSize = True
    Me.chk_completed.Location = New System.Drawing.Point(27, 18)
    Me.chk_completed.Name = "chk_completed"
    Me.chk_completed.Size = New System.Drawing.Size(95, 17)
    Me.chk_completed.TabIndex = 1
    Me.chk_completed.Text = "xCompleted"
    Me.chk_completed.UseVisualStyleBackColor = True
    '
    'lbl_color_gap
    '
    Me.lbl_color_gap.BackColor = System.Drawing.SystemColors.Window
    Me.lbl_color_gap.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lbl_color_gap.Location = New System.Drawing.Point(6, 63)
    Me.lbl_color_gap.Name = "lbl_color_gap"
    Me.lbl_color_gap.Size = New System.Drawing.Size(16, 16)
    Me.lbl_color_gap.TabIndex = 4
    '
    'chk_pending
    '
    Me.chk_pending.AutoSize = True
    Me.chk_pending.Location = New System.Drawing.Point(27, 40)
    Me.chk_pending.Name = "chk_pending"
    Me.chk_pending.Size = New System.Drawing.Size(78, 17)
    Me.chk_pending.TabIndex = 3
    Me.chk_pending.Text = "xPending"
    Me.chk_pending.UseVisualStyleBackColor = True
    '
    'chk_gap
    '
    Me.chk_gap.AutoSize = True
    Me.chk_gap.Location = New System.Drawing.Point(27, 63)
    Me.chk_gap.Name = "chk_gap"
    Me.chk_gap.Size = New System.Drawing.Size(56, 17)
    Me.chk_gap.TabIndex = 5
    Me.chk_gap.Text = "xGap"
    Me.chk_gap.UseVisualStyleBackColor = True
    '
    'tab_totals
    '
    Me.tab_totals.Controls.Add(Me.tab_tickets)
    Me.tab_totals.Controls.Add(Me.tab_bills)
    Me.tab_totals.Location = New System.Drawing.Point(0, 7)
    Me.tab_totals.Name = "tab_totals"
    Me.tab_totals.SelectedIndex = 0
    Me.tab_totals.Size = New System.Drawing.Size(893, 449)
    Me.tab_totals.TabIndex = 9
    '
    'tab_tickets
    '
    Me.tab_tickets.Controls.Add(Me.dg_tickets)
    Me.tab_tickets.Location = New System.Drawing.Point(4, 22)
    Me.tab_tickets.Name = "tab_tickets"
    Me.tab_tickets.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_tickets.Size = New System.Drawing.Size(885, 423)
    Me.tab_tickets.TabIndex = 0
    Me.tab_tickets.Text = "xTickets"
    Me.tab_tickets.UseVisualStyleBackColor = True
    '
    'dg_tickets
    '
    Me.dg_tickets.CurrentCol = -1
    Me.dg_tickets.CurrentRow = -1
    Me.dg_tickets.Location = New System.Drawing.Point(0, 0)
    Me.dg_tickets.Name = "dg_tickets"
    Me.dg_tickets.PanelRightVisible = True
    Me.dg_tickets.Redraw = True
    Me.dg_tickets.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_tickets.Size = New System.Drawing.Size(875, 420)
    Me.dg_tickets.Sortable = False
    Me.dg_tickets.SortAscending = True
    Me.dg_tickets.SortByCol = 0
    Me.dg_tickets.TabIndex = 1
    Me.dg_tickets.ToolTipped = True
    Me.dg_tickets.TopRow = -2
    '
    'tab_bills
    '
    Me.tab_bills.Controls.Add(Me.dg_bills)
    Me.tab_bills.Location = New System.Drawing.Point(4, 22)
    Me.tab_bills.Name = "tab_bills"
    Me.tab_bills.Padding = New System.Windows.Forms.Padding(3)
    Me.tab_bills.Size = New System.Drawing.Size(885, 423)
    Me.tab_bills.TabIndex = 1
    Me.tab_bills.Text = "xBills"
    Me.tab_bills.UseVisualStyleBackColor = True
    '
    'dg_bills
    '
    Me.dg_bills.CurrentCol = -1
    Me.dg_bills.CurrentRow = -1
    Me.dg_bills.Location = New System.Drawing.Point(0, 0)
    Me.dg_bills.Name = "dg_bills"
    Me.dg_bills.PanelRightVisible = True
    Me.dg_bills.Redraw = True
    Me.dg_bills.SelectionMode = GUI_Controls.uc_grid.SELECTION_MODE.SELECTION_MODE_MULTIPLE
    Me.dg_bills.Size = New System.Drawing.Size(875, 420)
    Me.dg_bills.Sortable = False
    Me.dg_bills.SortAscending = True
    Me.dg_bills.SortByCol = 0
    Me.dg_bills.TabIndex = 2
    Me.dg_bills.ToolTipped = True
    Me.dg_bills.TopRow = -2
    '
    'frm_tito_bills_tickets_balance_report_sel
    '
    Me.ClientSize = New System.Drawing.Size(988, 591)
    Me.Name = "frm_tito_bills_tickets_balance_report_sel"
    Me.panel_grids.ResumeLayout(False)
    Me.panel_filter.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_status.ResumeLayout(False)
    Me.gb_status.PerformLayout()
    Me.tab_totals.ResumeLayout(False)
    Me.tab_tickets.ResumeLayout(False)
    Me.tab_bills.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents lbl_cs_name As GUI_Controls.uc_text_field
  Friend WithEvents gb_status As System.Windows.Forms.GroupBox
  Friend WithEvents lbl_color_completed As System.Windows.Forms.Label
  Friend WithEvents lbl_color_pending As System.Windows.Forms.Label
  Friend WithEvents chk_completed As System.Windows.Forms.CheckBox
  Friend WithEvents lbl_color_gap As System.Windows.Forms.Label
  Friend WithEvents chk_pending As System.Windows.Forms.CheckBox
  Friend WithEvents chk_gap As System.Windows.Forms.CheckBox
  Friend WithEvents tab_totals As System.Windows.Forms.TabControl
  Friend WithEvents tab_tickets As System.Windows.Forms.TabPage
  Friend WithEvents tab_bills As System.Windows.Forms.TabPage
  Friend WithEvents dg_tickets As GUI_Controls.uc_grid
  Friend WithEvents dg_bills As GUI_Controls.uc_grid

End Class
