<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_tito_collection_balance_report
  Inherits GUI_Controls.frm_base_sel

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.ds_from_to = New GUI_Controls.uc_daily_session_selector
    Me.uc_provider = New GUI_Controls.uc_provider
    Me.cb_show_all_denominations = New System.Windows.Forms.CheckBox
    Me.cb_show_only_out_of_balance = New System.Windows.Forms.CheckBox
    Me.cb_terminal_location = New System.Windows.Forms.CheckBox
    Me.gb_group_by = New System.Windows.Forms.GroupBox
    Me.opt_terminal = New System.Windows.Forms.RadioButton
    Me.chk_show_detail = New System.Windows.Forms.CheckBox
    Me.opt_provider = New System.Windows.Forms.RadioButton
    Me.opt_group_by_day = New System.Windows.Forms.RadioButton
    Me.chk_group_by = New System.Windows.Forms.CheckBox
    Me.gb_date_filter = New System.Windows.Forms.GroupBox
    Me.opt_cage_session_datetime = New System.Windows.Forms.RadioButton
    Me.opt_collection_datetime = New System.Windows.Forms.RadioButton
    Me.opt_insertion_datetime = New System.Windows.Forms.RadioButton
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_group_by.SuspendLayout()
    Me.gb_date_filter.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.gb_date_filter)
    Me.panel_filter.Controls.Add(Me.gb_group_by)
    Me.panel_filter.Controls.Add(Me.cb_terminal_location)
    Me.panel_filter.Controls.Add(Me.cb_show_only_out_of_balance)
    Me.panel_filter.Controls.Add(Me.cb_show_all_denominations)
    Me.panel_filter.Controls.Add(Me.uc_provider)
    Me.panel_filter.Location = New System.Drawing.Point(5, 4)
    Me.panel_filter.Size = New System.Drawing.Size(1242, 240)
    Me.panel_filter.Controls.SetChildIndex(Me.uc_provider, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.cb_show_all_denominations, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.cb_show_only_out_of_balance, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.cb_terminal_location, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_group_by, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date_filter, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(5, 244)
    Me.panel_data.Size = New System.Drawing.Size(1242, 363)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1236, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1236, 4)
    '
    'ds_from_to
    '
    Me.ds_from_to.ClosingTime = 0
    Me.ds_from_to.ClosingTimeEnabled = True
    Me.ds_from_to.FromDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.ds_from_to.FromDateSelected = True
    Me.ds_from_to.Location = New System.Drawing.Point(6, 74)
    Me.ds_from_to.Name = "ds_from_to"
    Me.ds_from_to.ShowBorder = False
    Me.ds_from_to.Size = New System.Drawing.Size(249, 80)
    Me.ds_from_to.TabIndex = 3
    Me.ds_from_to.ToDate = New Date(2007, 1, 1, 0, 0, 0, 0)
    Me.ds_from_to.ToDateSelected = True
    '
    'uc_provider
    '
    Me.uc_provider.Location = New System.Drawing.Point(272, 2)
    Me.uc_provider.Name = "uc_provider"
    Me.uc_provider.Size = New System.Drawing.Size(328, 158)
    Me.uc_provider.TabIndex = 4
    '
    'cb_show_all_denominations
    '
    Me.cb_show_all_denominations.AutoSize = True
    Me.cb_show_all_denominations.Location = New System.Drawing.Point(9, 193)
    Me.cb_show_all_denominations.Name = "cb_show_all_denominations"
    Me.cb_show_all_denominations.Size = New System.Drawing.Size(161, 17)
    Me.cb_show_all_denominations.TabIndex = 2
    Me.cb_show_all_denominations.Text = "Show all denominations"
    Me.cb_show_all_denominations.UseVisualStyleBackColor = True
    '
    'cb_show_only_out_of_balance
    '
    Me.cb_show_only_out_of_balance.AutoSize = True
    Me.cb_show_only_out_of_balance.Location = New System.Drawing.Point(9, 170)
    Me.cb_show_only_out_of_balance.Name = "cb_show_only_out_of_balance"
    Me.cb_show_only_out_of_balance.Size = New System.Drawing.Size(170, 17)
    Me.cb_show_only_out_of_balance.TabIndex = 1
    Me.cb_show_only_out_of_balance.Text = "Show only out of balance"
    Me.cb_show_only_out_of_balance.UseVisualStyleBackColor = True
    '
    'cb_terminal_location
    '
    Me.cb_terminal_location.Location = New System.Drawing.Point(9, 216)
    Me.cb_terminal_location.Name = "cb_terminal_location"
    Me.cb_terminal_location.Size = New System.Drawing.Size(235, 16)
    Me.cb_terminal_location.TabIndex = 3
    Me.cb_terminal_location.Text = "Show terminals location"
    '
    'gb_group_by
    '
    Me.gb_group_by.Controls.Add(Me.opt_terminal)
    Me.gb_group_by.Controls.Add(Me.chk_show_detail)
    Me.gb_group_by.Controls.Add(Me.opt_provider)
    Me.gb_group_by.Controls.Add(Me.opt_group_by_day)
    Me.gb_group_by.Controls.Add(Me.chk_group_by)
    Me.gb_group_by.Location = New System.Drawing.Point(613, 6)
    Me.gb_group_by.Name = "gb_group_by"
    Me.gb_group_by.Size = New System.Drawing.Size(223, 154)
    Me.gb_group_by.TabIndex = 5
    Me.gb_group_by.TabStop = False
    Me.gb_group_by.Text = "xOptions"
    '
    'opt_terminal
    '
    Me.opt_terminal.AutoSize = True
    Me.opt_terminal.Location = New System.Drawing.Point(27, 101)
    Me.opt_terminal.Name = "opt_terminal"
    Me.opt_terminal.Size = New System.Drawing.Size(82, 17)
    Me.opt_terminal.TabIndex = 3
    Me.opt_terminal.TabStop = True
    Me.opt_terminal.Text = "xTerminal"
    Me.opt_terminal.UseVisualStyleBackColor = True
    '
    'chk_show_detail
    '
    Me.chk_show_detail.AutoSize = True
    Me.chk_show_detail.Location = New System.Drawing.Point(27, 128)
    Me.chk_show_detail.Name = "chk_show_detail"
    Me.chk_show_detail.Size = New System.Drawing.Size(98, 17)
    Me.chk_show_detail.TabIndex = 4
    Me.chk_show_detail.Text = "Show details"
    Me.chk_show_detail.UseVisualStyleBackColor = True
    '
    'opt_provider
    '
    Me.opt_provider.AutoSize = True
    Me.opt_provider.Location = New System.Drawing.Point(27, 74)
    Me.opt_provider.Name = "opt_provider"
    Me.opt_provider.Size = New System.Drawing.Size(80, 17)
    Me.opt_provider.TabIndex = 2
    Me.opt_provider.TabStop = True
    Me.opt_provider.Text = "xProvider"
    Me.opt_provider.UseVisualStyleBackColor = True
    '
    'opt_group_by_day
    '
    Me.opt_group_by_day.AutoSize = True
    Me.opt_group_by_day.Location = New System.Drawing.Point(27, 47)
    Me.opt_group_by_day.Name = "opt_group_by_day"
    Me.opt_group_by_day.Size = New System.Drawing.Size(108, 17)
    Me.opt_group_by_day.TabIndex = 1
    Me.opt_group_by_day.TabStop = True
    Me.opt_group_by_day.Text = "xCreationDate"
    Me.opt_group_by_day.UseVisualStyleBackColor = True
    '
    'chk_group_by
    '
    Me.chk_group_by.AutoSize = True
    Me.chk_group_by.Location = New System.Drawing.Point(6, 20)
    Me.chk_group_by.Name = "chk_group_by"
    Me.chk_group_by.Size = New System.Drawing.Size(83, 17)
    Me.chk_group_by.TabIndex = 0
    Me.chk_group_by.Text = "xGroupBy"
    Me.chk_group_by.UseVisualStyleBackColor = True
    '
    'gb_date_filter
    '
    Me.gb_date_filter.Controls.Add(Me.opt_cage_session_datetime)
    Me.gb_date_filter.Controls.Add(Me.opt_collection_datetime)
    Me.gb_date_filter.Controls.Add(Me.opt_insertion_datetime)
    Me.gb_date_filter.Controls.Add(Me.ds_from_to)
    Me.gb_date_filter.Location = New System.Drawing.Point(6, 6)
    Me.gb_date_filter.Name = "gb_date_filter"
    Me.gb_date_filter.Size = New System.Drawing.Size(260, 156)
    Me.gb_date_filter.TabIndex = 0
    Me.gb_date_filter.TabStop = False
    Me.gb_date_filter.Text = "xDateFilter"
    '
    'opt_cage_session_datetime
    '
    Me.opt_cage_session_datetime.AutoSize = True
    Me.opt_cage_session_datetime.Location = New System.Drawing.Point(6, 65)
    Me.opt_cage_session_datetime.Name = "opt_cage_session_datetime"
    Me.opt_cage_session_datetime.Size = New System.Drawing.Size(106, 17)
    Me.opt_cage_session_datetime.TabIndex = 2
    Me.opt_cage_session_datetime.TabStop = True
    Me.opt_cage_session_datetime.Text = "xCageSession"
    Me.opt_cage_session_datetime.UseVisualStyleBackColor = True
    '
    'opt_collection_datetime
    '
    Me.opt_collection_datetime.AutoSize = True
    Me.opt_collection_datetime.Location = New System.Drawing.Point(6, 42)
    Me.opt_collection_datetime.Name = "opt_collection_datetime"
    Me.opt_collection_datetime.Size = New System.Drawing.Size(88, 17)
    Me.opt_collection_datetime.TabIndex = 1
    Me.opt_collection_datetime.TabStop = True
    Me.opt_collection_datetime.Text = "xCollection"
    Me.opt_collection_datetime.UseVisualStyleBackColor = True
    '
    'opt_insertion_datetime
    '
    Me.opt_insertion_datetime.AutoSize = True
    Me.opt_insertion_datetime.Location = New System.Drawing.Point(6, 19)
    Me.opt_insertion_datetime.Name = "opt_insertion_datetime"
    Me.opt_insertion_datetime.Size = New System.Drawing.Size(83, 17)
    Me.opt_insertion_datetime.TabIndex = 0
    Me.opt_insertion_datetime.TabStop = True
    Me.opt_insertion_datetime.Text = "xInsertion"
    Me.opt_insertion_datetime.UseVisualStyleBackColor = True
    '
    'frm_tito_collection_balance_report
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1252, 611)
    Me.Name = "frm_tito_collection_balance_report"
    Me.Padding = New System.Windows.Forms.Padding(5, 4, 5, 4)
    Me.Text = "frm_tito_collection_report"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_group_by.ResumeLayout(False)
    Me.gb_group_by.PerformLayout()
    Me.gb_date_filter.ResumeLayout(False)
    Me.gb_date_filter.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents ds_from_to As GUI_Controls.uc_daily_session_selector
  Friend WithEvents cb_show_all_denominations As System.Windows.Forms.CheckBox
  Friend WithEvents uc_provider As GUI_Controls.uc_provider
  Friend WithEvents cb_show_only_out_of_balance As System.Windows.Forms.CheckBox
  Friend WithEvents cb_terminal_location As System.Windows.Forms.CheckBox
  Friend WithEvents gb_group_by As System.Windows.Forms.GroupBox
  Friend WithEvents chk_show_detail As System.Windows.Forms.CheckBox
  Friend WithEvents opt_provider As System.Windows.Forms.RadioButton
  Friend WithEvents opt_group_by_day As System.Windows.Forms.RadioButton
  Friend WithEvents chk_group_by As System.Windows.Forms.CheckBox
  Friend WithEvents opt_terminal As System.Windows.Forms.RadioButton
  Friend WithEvents gb_date_filter As System.Windows.Forms.GroupBox
  Friend WithEvents opt_cage_session_datetime As System.Windows.Forms.RadioButton
  Friend WithEvents opt_collection_datetime As System.Windows.Forms.RadioButton
  Friend WithEvents opt_insertion_datetime As System.Windows.Forms.RadioButton
End Class
