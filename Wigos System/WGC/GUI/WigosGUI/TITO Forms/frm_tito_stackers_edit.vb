'--------------------------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'--------------------------------------------------------------------------------------
'
'   MODULE NAME :  frm_stackers_edit
'
'   DESCRIPTION :  Stackers editor
'
'        AUTHOR :  Nelson Madrigal Reyes
'
' CREATION DATE :  25-JUN-2013
'
' REVISION HISTORY
'
' Date         Author Description
' -----------  ------ ------------------------------------------------------------------
' 25-JUN-2013  NMR    Initial version
' 23-AUG-2013  JRM    Code revision and refactoring
' 28-AUG-2013  NMR    Fixed error ocurred while changing stacker status
' 03-OCT-2013  LEM    WIGOSTITO-317: 
'                     Added "Available" to Status groupbox
'                     Shows warning message if terminal are modified
'                     Column "Status" on Collection History grid shows balanced info.
'                     Before update stacker info, update/insert collection info for terminal selected
'                     After update stacker info, update stacker replaced status as "Available".
'                     Don't allows more than one stacker "Inserted Pending" for a terminal.
' 11-OCT-2013  LEM    Execute permission form must be enabled to change status to available or to change the terminal if current status is Inserted Validated or Collection Pending
' 22-OCT-2013  RRR    Added preassigned terminal field.
'---------------------------------------------------------------------------------------


'
'




Option Explicit On
Option Strict Off

Imports System.Text
Imports GUI_CommonMisc
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_Controls
Imports WSI.Common

Public Class frm_tito_stackers_edit
  Inherits GUI_Controls.frm_base_edit

#Region " Constants "

  ' Grid configuration
  Private Const GRID_COLUMNS_COUNT As Integer = 17
  Private Const GRID_HEADER_ROWS As Integer = 1

  Private Const GRID_COLUMN_SELECT As Integer = 0
  Private Const GRID_COLUMN_ID As Integer = 1
  Private Const GRID_COLUMN_TERMINAL As Integer = 2
  Private Const GRID_COLUMN_STATUS As Integer = 3
  Private Const GRID_COLUMN_EMPLOYEE_COLLECTION As Integer = 4
  Private Const GRID_COLUMN_READ_MONEY As Integer = 5
  Private Const GRID_COLUMN_EXPECTED_MONEY As Integer = 6
  Private Const GRID_COLUMN_BILLS_READ As Integer = 7
  Private Const GRID_COLUMN_BILLS_AMOUNT As Integer = 8
  Private Const GRID_COLUMN_TICKETS_READ As Integer = 9
  Private Const GRID_COLUMN_TICKETS_AMOUNT As Integer = 10
  Private Const GRID_COLUMN_COLLECT_MONEY_DATE As Integer = 11
  Private Const GRID_COLUMN_INSERTED_DATE As Integer = 12
  Private Const GRID_COLUMN_EMPLOYEE_INSERTION As Integer = 13
  Private Const GRID_COLUMN_EXTRACTION_DATE As Integer = 14
  Private Const GRID_COLUMN_NOTES As Integer = 15
  Private Const GRID_COLUMN_STACKER_COLLECTION_ID As Integer = 16

  ' QUERY columns
  Private Const SQL_COLUMN_SELECT As Integer = 0
  Private Const SQL_COLUMN_ID As Integer = 1
  Private Const SQL_COLUMN_TERMINAL As Integer = 2
  Private Const SQL_COLUMN_BALANCED As Integer = 3
  Private Const SQL_COLUMN_EMPLOYEE_COLLECTION As Integer = 4
  Private Const SQL_COLUMN_READ_MONEY As Integer = 5
  Private Const SQL_COLUMN_EXPECTED_MONEY As Integer = 6
  Private Const SQL_COLUMN_BILLS_READ As Integer = 7
  Private Const SQL_COLUMN_BILLS_AMOUNT As Integer = 8
  Private Const SQL_COLUMN_TICKETS_READ As Integer = 9
  Private Const SQL_COLUMN_TICKETS_AMOUNT As Integer = 10
  Private Const SQL_COLUMN_COLLECT_MONEY_DATE As Integer = 11
  Private Const SQL_COLUMN_INSERTED_DATE As Integer = 12
  Private Const SQL_COLUMN_EMPLOYEE_INSERTION As Integer = 13
  Private Const SQL_COLUMN_EXTRACTION_DATE As Integer = 14
  Private Const SQL_COLUMN_NOTES As Integer = 15

  Private Const SQL_COLUMN_STACKER_COLLECTION_ID As Integer = SQL_COLUMN_SELECT

  Private Const GRID_COLUMN_WIDTH_SELECT As Integer = 200
  Private Const GRID_COLUMN_WIDTH_ID As Integer = 1000
  Private Const GRID_COLUMN_TERMINAL_WIDTH As Integer = 2200
  Private Const GRID_COLUMN_STATUS_WIDTH As Integer = 1800
  Private Const GRID_COLUMN_EMPLOYEE_COLLECTION_WIDTH As Integer = 2200
  Private Const GRID_COLUMN_READ_MONEY_WIDTH As Integer = 1600
  Private Const GRID_COLUMN_EXPECTED_MONEY_WIDTH As Integer = 1600
  Private Const GRID_COLUMN_BILLS_READ_WIDTH As Integer = 1600
  Private Const GRID_COLUMN_BILLS_AMOUNT_WIDTH As Integer = 1800
  Private Const GRID_COLUMN_TICKETS_READ_WIDTH As Integer = 1600
  Private Const GRID_COLUMN_TICKETS_AMOUNT_WIDTH As Integer = 1800
  Private Const GRID_COLUMN_COLLECT_MONEY_DATE_WIDTH As Integer = 2000
  Private Const GRID_COLUMN_INSERTED_DATE_WIDTH As Integer = 2000
  Private Const GRID_COLUMN_EMPLOYEE_INSERTION_WIDTH As Integer = 2200
  Private Const GRID_COLUMN_EXTRACTION_DATE_WIDTH As Integer = 2000
  Private Const GRID_COLUMN_NOTES_WIDTH As Integer = 2600

#End Region 'constants

#Region " Members "

  Private m_update_collection_id As Int64
  Private m_extract_stacker As Boolean
  Private m_insert_collection As Boolean
  Private m_pre_ass_provider As Int64
  Private m_pre_ass_terminal As mdl_tito.SmartTerminal
  Private m_assigned_term As mdl_tito.SmartTerminal
  Private m_inserted_term As mdl_tito.SmartTerminal
  Private m_stacker_unlinked_id As Int64
  Private m_update_money_collection_status As ENUM_CONFIGURATION_RC


#End Region ' Members

#Region " Overrides "

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_STACKERS_EDIT
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    ' Required by the base class
    Call MyBase.GUI_InitControls()

    ' Texts translation
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2179) 'Edici�n de Stacker
    ef_stacker_id.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2180) 'Identificador
    ef_model.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2181) 'Modelo"
    chk_deactivated.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2167) 'Desactivado
    lbl_preassigned.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2182) 'Prov. preasignado
    lbl_preassigned_term.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2822) 'Term. preasignado
    ef_stacker_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262) 'Estado
    lbl_inserted.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2185) 'Prov. insertado

    gb_status.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262) 'Estado
    opt_available.Text = mdl_tito.StackerStatusAsString(TITO_STACKER_STATUS.AVAILABLE)
    opt_inserted_pending.Text = mdl_tito.StackerStatusAsString(TITO_STACKER_STATUS.INSERTED_PENDING)
    opt_inserted_validated.Text = mdl_tito.StackerStatusAsString(TITO_STACKER_STATUS.INSERTED_VALIDATED)
    opt_collection_pending.Text = mdl_tito.StackerStatusAsString(TITO_STACKER_STATUS.COLLECTION_PENDING)

    lbl_notes.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2188) 'Notas
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_CONTROLS.GetString(9) 'Seleccionar
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Text = GLB_NLS_GUI_CONTROLS.GetString(27) 'Excel
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Text = GLB_NLS_GUI_CONTROLS.GetString(5) 'Imprimir
    lbl_warning.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2669) 'Warning ...
    lbl_grid_title.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2670) 'Collection History
    lbl_grid_title.Visible = True

    ' Activate/Show controls
    ef_stacker_id.Enabled = ScreenMode = ENUM_SCREEN_MODE.MODE_NEW

    ' Delete button is only available when editing, not while creating
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Visible = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Visible = True

    ef_stacker_id.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 20)
    ef_model.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_TEXT, 50)

    uc_dates.Init(GLB_NLS_GUI_INVOICING.Id(201))
    uc_dates.ClosingTimeEnabled = False
    uc_dates.Enabled = ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT
    btn_search_collections.Text = GLB_NLS_GUI_CONTROLS.GetString(8) ' Buscar
    btn_search_collections.Width = 120
    btn_search_collections.Height = 25
    btn_search_collections.Enabled = uc_dates.Enabled

    ' iniciar la tabla de los detalles
    Call GUI_StyleSheet()
    Call ef_stacker_id.Focus()
    Call GUI_FilterReset()

  End Sub ' GUI_InitControls

  Protected Overrides Function GUI_IsScreenDataOk() As Boolean
    Dim _stacker As CLASS_STACKER
    Dim _status_sel As TITO_STACKER_STATUS

    m_update_collection_id = 0
    m_extract_stacker = False
    m_insert_collection = False

    _stacker = DbEditedObject
    _status_sel = GetStatusSelected()

    m_pre_ass_provider = mdl_tito.GetProviderId(cmb_preassigned_prov.Text)
    m_inserted_term = mdl_tito.GetSmartTerminal(cmb_inserted_term.Text)
    m_pre_ass_terminal = mdl_tito.GetSmartTerminal(cmb_preassigned_term.Text)

    If _stacker.Status = TITO_STACKER_STATUS.DISABLED Or _
       _stacker.Status = TITO_STACKER_STATUS.DEACTIVATED Then

      Return True
    End If

    ' Cambio de estado sin seleccionar un terminal
    If _status_sel <> TITO_STACKER_STATUS.AVAILABLE Then
      If _stacker.Status <> _status_sel And m_inserted_term.terminal_id < 1 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2241), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , mdl_tito.StackerStatusAsString(_status_sel))
        Call cmb_inserted_term.Focus()

        Return False
      End If
    End If

    ' Insertado Pendiente sin seleccionar un terminal
    If _status_sel = TITO_STACKER_STATUS.INSERTED_PENDING And m_inserted_term.terminal_id < 1 Then
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2242), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) <> ENUM_MB_RESULT.MB_RESULT_YES Then

        Return False
      End If

      opt_available.Checked = True
    End If

    ' Stacker Id no v�lido al crear
    If ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      If Not ef_stacker_id.ValidateFormat() Or Int64.Parse(ef_stacker_id.Value) <= 0 Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2196), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , GLB_NLS_GUI_PLAYER_TRACKING.GetString(2180))
        Call ef_stacker_id.Focus()

        Return False
      End If
    End If

    ' Modelo vac�o
    If String.IsNullOrEmpty(ef_model.Value) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2197), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING, , , ef_model.Value)
      Call ef_model.Focus()

      Return False
    End If

    ' RRR 22-OCT-2013
    ' The preassigned terminal doesn't belongs to preassigned provider
    Dim _compare_providers As Boolean
    ' If preassigned terminal and preassigned provided are defined
    _compare_providers = (m_pre_ass_provider > 0 And m_pre_ass_terminal.terminal_id > 0)
    ' If values have been modified
    _compare_providers = _compare_providers And (m_pre_ass_terminal.terminal_id <> _stacker.PreassignedTerminal Or m_pre_ass_provider <> _stacker.PreassignedProvider)
    ' If the preassigned terminal doesn't belongs to preassigned provider
    _compare_providers = _compare_providers AndAlso (m_pre_ass_provider <> m_pre_ass_terminal.provider_id)

    If _compare_providers Then
      If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2823), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) <> ENUM_MB_RESULT.MB_RESULT_YES Then

        Return False
      End If
    End If

    If m_inserted_term.terminal_id > 0 Then

      ' Ya existe un stacker insertado en ese terminal
      If _status_sel <> TITO_STACKER_STATUS.COLLECTION_PENDING AndAlso mdl_tito.ExistsDuplicateTerminalStacker(_stacker.StackerId, m_inserted_term.terminal_id) Then
        Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2667), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_WARNING)
        cmb_inserted_term.Focus()

        Return False
      End If

      ' El terminal de inserci�n no pertenece al provider preasignado
      If m_pre_ass_provider <> _stacker.PreassignedProvider Or m_inserted_term.terminal_id <> _stacker.InsertedTerminal Then
        If m_pre_ass_provider >= 0 AndAlso m_pre_ass_provider <> m_inserted_term.provider_id AndAlso Not opt_collection_pending.Checked Then
          If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2240), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) <> ENUM_MB_RESULT.MB_RESULT_YES Then

            Return False
          End If
        End If
      End If

      ' RRR 22-OCT-2013
      ' The preassigned terminal is not the same as the insertion terminal
      Dim _compare_terminals As Boolean
      ' If preassigned terminal is defined
      _compare_terminals = (m_pre_ass_terminal.terminal_id > 0)
      ' If the preassigned terminal of insertion terminal have been modified
      _compare_terminals = _compare_terminals And (m_pre_ass_terminal.terminal_id <> _stacker.PreassignedTerminal Or m_inserted_term.terminal_id <> _stacker.InsertedTerminal)
      ' If the preassigned terminal is not the same as the insertion terminal
      _compare_terminals = _compare_terminals AndAlso (m_pre_ass_terminal.terminal_id <> m_inserted_term.terminal_id)

      If _compare_terminals Then
        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2824), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) <> ENUM_MB_RESULT.MB_RESULT_YES Then

          Return False
        End If
      End If


      ' Al validar el stacker se crea la recaudaci�n
      If _status_sel = TITO_STACKER_STATUS.INSERTED_VALIDATED And _stacker.Status = TITO_STACKER_STATUS.INSERTED_PENDING Then
        m_insert_collection = True
      End If

      ' Se verifica que se desea extraer el stacker al pasar a Pendiente Entrega
      If _status_sel = TITO_STACKER_STATUS.COLLECTION_PENDING And _stacker.Status <> TITO_STACKER_STATUS.COLLECTION_PENDING Then
        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2357), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) <> ENUM_MB_RESULT.MB_RESULT_YES Then

          Return False
        End If

        m_extract_stacker = True

      End If
    End If

    Return True

  End Function ' GUI_IsScreenDataOk

  Protected Overrides Sub GUI_SetScreenData(ByRef SqlCtx As Integer)

    Dim _stacker As CLASS_STACKER
    Dim _table As DataTable
    Dim _str_sql_builder As StringBuilder

    _stacker = DbReadObject

    ef_stacker_id.Value = _stacker.StackerId
    ef_model.Value = _stacker.Model

    RefreshScreenByStatus(_stacker.Status, False)

    'ef_stacker_status.Value = mdl_tito.StackerStatusAsString(_stacker.Status)
    txt_notes.Text = _stacker.Notes

    _str_sql_builder = New StringBuilder()
    _str_sql_builder.AppendLine("   SELECT   TE_TERMINAL_ID AS ID           ")
    _str_sql_builder.AppendLine("          , TE_NAME        AS NAME         ")
    _str_sql_builder.AppendLine("     FROM   TERMINALS                      ")
    _str_sql_builder.AppendLine("    WHERE  TE_TERMINAL_TYPE IN ( " + WSI.Common.Misc.GamingTerminalTypeListToString() + ") ")
    _str_sql_builder.AppendLine(" ORDER BY   TE_NAME                        ")
    _table = GUI_GetTableUsingSQL(_str_sql_builder.ToString(), 5000)
    Call mdl_tito.FillComboFromTable(_stacker.InsertedTerminal, cmb_inserted_term, _table)

    _str_sql_builder = New StringBuilder()
    _str_sql_builder.AppendLine("  SELECT   PV_ID    AS ID   ")
    _str_sql_builder.AppendLine("         , PV_NAME  AS NAME ")
    _str_sql_builder.AppendLine("    FROM   PROVIDERS        ")
    _str_sql_builder.AppendLine("ORDER BY   PV_NAME          ")
    _table = GUI_GetTableUsingSQL(_str_sql_builder.ToString(), 5000)
    Call mdl_tito.FillComboFromTable(_stacker.PreassignedProvider, cmb_preassigned_prov, _table)

    _str_sql_builder = New StringBuilder()
    _str_sql_builder.AppendLine("   SELECT   TE_TERMINAL_ID AS ID           ")
    _str_sql_builder.AppendLine("          , TE_NAME        AS NAME         ")
    _str_sql_builder.AppendLine("     FROM   TERMINALS                      ")
    _str_sql_builder.AppendLine("    WHERE  TE_TERMINAL_TYPE IN ( " + WSI.Common.Misc.GamingTerminalTypeListToString() + ") ")
    _str_sql_builder.AppendLine(" ORDER BY   TE_NAME                        ")
    _table = GUI_GetTableUsingSQL(_str_sql_builder.ToString(), 5000)
    Call mdl_tito.FillComboFromTable(_stacker.PreassignedTerminal, cmb_preassigned_term, _table)


    ' Fill table with details
    Call FillTableDetails(_stacker.StackerId)

  End Sub ' GUI_SetScreenData

  Protected Overrides Sub GUI_GetScreenData()
    Dim _stacker As CLASS_STACKER
    Dim _stacker_original As CLASS_STACKER

    m_inserted_term = mdl_tito.GetSmartTerminal(cmb_inserted_term.Text)
    m_pre_ass_provider = mdl_tito.GetProviderId(cmb_preassigned_prov.Text)
    m_pre_ass_terminal = mdl_tito.GetSmartTerminal(cmb_preassigned_term.Text)

    _stacker = DbEditedObject
    _stacker_original = DbReadObject

    If ScreenMode = ENUM_SCREEN_MODE.MODE_NEW Then
      _stacker.StackerId = ef_stacker_id.Value
    End If

    _stacker.Model = ef_model.Value
    _stacker.Status = GetStatusSelected()
    _stacker.InsertedTerminal = m_inserted_term.terminal_id
    _stacker.PreassignedProvider = m_pre_ass_provider
    _stacker.PreassignedTerminal = m_pre_ass_terminal.terminal_id
    _stacker.Notes = txt_notes.Text

  End Sub ' GUI_GetScreenData

  Protected Overrides Sub GUI_DB_Operation(ByVal DbOperation As ENUM_DB_OPERATION)
    Dim _stacker_edit As CLASS_STACKER
    Dim _stacker_read As CLASS_STACKER
    Dim _mc_stacker_status As TITO_STACKER_STATUS

    _stacker_edit = New CLASS_STACKER

    Select Case DbOperation
      Case ENUM_DB_OPERATION.DB_OPERATION_CREATE
        DbEditedObject = New CLASS_STACKER()

        _stacker_edit = DbEditedObject
        _stacker_edit.StackerId = 0

        DbStatus = ENUM_STATUS.STATUS_OK

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_READ
        If Me.DbStatus <> ENUM_STATUS.STATUS_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2349), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          DbObjectId.ToString())
        End If

      Case ENUM_DB_OPERATION.DB_OPERATION_UPDATE
        If m_update_money_collection_status <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(1853), _
                     ENUM_MB_TYPE.MB_TYPE_ERROR, _
                     ENUM_MB_BTN.MB_BTN_OK)
          DbStatus = ENUM_STATUS.STATUS_ERROR

          Return
        End If

        Call MyBase.GUI_DB_Operation(DbOperation)

      Case ENUM_DB_OPERATION.DB_OPERATION_BEFORE_UPDATE _
         , ENUM_DB_OPERATION.DB_OPERATION_BEFORE_INSERT

        _stacker_read = DbReadObject
        _stacker_edit = DbEditedObject

        If _stacker_edit.Status >= TITO_STACKER_STATUS.AVAILABLE Then
          ' Insert or Update Money Collections
          m_update_money_collection_status = mdl_tito.UpdateMoneyCollection(_stacker_edit, m_stacker_unlinked_id, _mc_stacker_status, m_insert_collection)
          If m_update_money_collection_status <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then

            Return
          End If

          If _mc_stacker_status <> TITO_STACKER_STATUS.DISABLED AndAlso _stacker_edit.Status <> _mc_stacker_status Then
            _stacker_edit.Status = _mc_stacker_status
          End If

          ' Se marca el stacker como extra�do 
          If m_extract_stacker AndAlso _mc_stacker_status <> TITO_STACKER_STATUS.COLLECTION_PENDING Then
            m_update_collection_id = mdl_tito.ExistsPendingCollection(_stacker_edit, True, True)
            If m_update_collection_id > 0 Then
              _stacker_edit.Status = TITO_STACKER_STATUS.COLLECTION_PENDING
              If Not mdl_tito.ExtractStackerCollection(m_update_collection_id) Then
                m_update_money_collection_status = ENUM_CONFIGURATION_RC.CONFIGURATION_ERROR

                Return
              End If
            End If
          End If

          Call MyBase.GUI_DB_Operation(DbOperation)

        End If

        m_update_money_collection_status = ENUM_CONFIGURATION_RC.CONFIGURATION_OK

      Case ENUM_DB_OPERATION.DB_OPERATION_AFTER_INSERT _
         , ENUM_DB_OPERATION.DB_OPERATION_AFTER_UPDATE

        If Me.DbStatus = ENUM_STATUS.STATUS_OK Then
          If m_stacker_unlinked_id > 0 Then
            _stacker_read = New CLASS_STACKER()
            _stacker_read.DB_Read(m_stacker_unlinked_id, 0)
            _stacker_read.Status = TITO_STACKER_STATUS.AVAILABLE
            _stacker_read.InsertedTerminal = 0
            _stacker_read.DB_Update(0)

            Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2668), _
                            ENUM_MB_TYPE.MB_TYPE_WARNING, _
                            ENUM_MB_BTN.MB_BTN_OK, , _
                            m_stacker_unlinked_id.ToString())

            Alarm.Register(AlarmSourceCode.User, GLB_CurrentUser.Id, GLB_CurrentUser.Name & "@" & Environment.MachineName, _
                      AlarmCode.User_StackerCollectionUnlinked, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2668, m_stacker_unlinked_id.ToString()))
          End If
        End If

        If Me.DbStatus = ENUM_STATUS.STATUS_DUPLICATE_KEY Then
          _stacker_edit = DbEditedObject
          Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2605), _
                          ENUM_MB_TYPE.MB_TYPE_ERROR, _
                          ENUM_MB_BTN.MB_BTN_OK, _
                          ENUM_MB_DEF_BTN.MB_DEF_BTN_1, _
                          ef_stacker_id.Text, " '" & _stacker_edit.StackerId & "'")
        End If

      Case Else
        Call MyBase.GUI_DB_Operation(DbOperation)

    End Select

  End Sub ' GUI_DB_Operation

  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Select Case ButtonId
      Case ENUM_BUTTON.BUTTON_DELETE
        If NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2198), ENUM_MB_TYPE.MB_TYPE_WARNING, ENUM_MB_BTN.MB_BTN_YES_NO, ENUM_MB_DEF_BTN.MB_DEF_BTN_2) = ENUM_MB_RESULT.MB_RESULT_YES Then
          chk_deactivated.Enabled = False
          chk_deactivated.Checked = True
          Call MyBase.GUI_ButtonClick(ENUM_BUTTON.BUTTON_OK)
        End If

        ' Detalle de la recaudaci�n
      Case ENUM_BUTTON.BUTTON_CUSTOM_0
        Call ViewCollectionDetails()

        'Excel button
      Case ENUM_BUTTON.BUTTON_CUSTOM_1
        Call ExcelWithDetails(FormTitle, grid_collection_history)

        'Imprimir button
      Case ENUM_BUTTON.BUTTON_CUSTOM_2
        Call PrintDetails(FormTitle, grid_collection_history)

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick

#End Region 'Overrides

#Region " Private Functions and Methods "

  Private Function GetStatusSelected() As TITO_STACKER_STATUS
    If chk_deactivated.Checked Then

      Return IIf(chk_deactivated.Enabled, TITO_STACKER_STATUS.DEACTIVATED, TITO_STACKER_STATUS.DISABLED)
    ElseIf opt_inserted_pending.Checked Then

      Return TITO_STACKER_STATUS.INSERTED_PENDING
    ElseIf opt_inserted_validated.Checked Then

      Return TITO_STACKER_STATUS.INSERTED_VALIDATED
    ElseIf opt_collection_pending.Checked Then

      Return TITO_STACKER_STATUS.COLLECTION_PENDING
    End If

    Return TITO_STACKER_STATUS.AVAILABLE
  End Function

  'Private Sub AddNewMoneyCollection(ByVal Stacker As CLASS_STACKER)
  '  Dim _transaction_result As ENUM_CONFIGURATION_RC

  '  _transaction_result = mdl_tito.InsertMoneyCollection(Stacker)

  '  If _transaction_result = mdl_tito.InsertMoneyCollection(Stacker) <> ENUM_CONFIGURATION_RC.CONFIGURATION_OK Then

  '  End If
  'End Sub

  ' PURPOSE: Fill table with details values
  '
  ' PARAMS:
  '   - INPUT:
  '
  '   - OUTPUT: None
  '
  Private Sub GUI_StyleSheet()

    With grid_collection_history
      Call .Init(GRID_COLUMNS_COUNT, GRID_HEADER_ROWS)

      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE
      .Sortable = False

      .Column(GRID_COLUMN_SELECT).Header(0).Text = ""
      .Column(GRID_COLUMN_SELECT).Width = GRID_COLUMN_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False

      .Column(GRID_COLUMN_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2199)
      .Column(GRID_COLUMN_ID).Width = GRID_COLUMN_WIDTH_ID

      .Column(GRID_COLUMN_TERMINAL).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(492)
      .Column(GRID_COLUMN_TERMINAL).Width = GRID_COLUMN_TERMINAL_WIDTH

      .Column(GRID_COLUMN_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(262)
      .Column(GRID_COLUMN_STATUS).Width = GRID_COLUMN_STATUS_WIDTH
      .Column(GRID_COLUMN_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_EMPLOYEE_COLLECTION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2352)
      .Column(GRID_COLUMN_EMPLOYEE_COLLECTION).Width = GRID_COLUMN_EMPLOYEE_COLLECTION_WIDTH

      .Column(GRID_COLUMN_READ_MONEY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2204)
      .Column(GRID_COLUMN_READ_MONEY).Width = GRID_COLUMN_READ_MONEY_WIDTH

      .Column(GRID_COLUMN_EXPECTED_MONEY).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2205)
      .Column(GRID_COLUMN_EXPECTED_MONEY).Width = GRID_COLUMN_EXPECTED_MONEY_WIDTH

      .Column(GRID_COLUMN_BILLS_READ).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2207)
      .Column(GRID_COLUMN_BILLS_READ).Width = GRID_COLUMN_BILLS_READ_WIDTH

      .Column(GRID_COLUMN_BILLS_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2208)
      .Column(GRID_COLUMN_BILLS_AMOUNT).Width = GRID_COLUMN_BILLS_AMOUNT_WIDTH

      .Column(GRID_COLUMN_TICKETS_READ).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2209)
      .Column(GRID_COLUMN_TICKETS_READ).Width = GRID_COLUMN_TICKETS_READ_WIDTH

      .Column(GRID_COLUMN_TICKETS_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2210)
      .Column(GRID_COLUMN_TICKETS_AMOUNT).Width = GRID_COLUMN_TICKETS_AMOUNT_WIDTH

      .Column(GRID_COLUMN_COLLECT_MONEY_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2206)
      .Column(GRID_COLUMN_COLLECT_MONEY_DATE).Width = GRID_COLUMN_COLLECT_MONEY_DATE_WIDTH
      .Column(GRID_COLUMN_COLLECT_MONEY_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_INSERTED_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2186)
      .Column(GRID_COLUMN_INSERTED_DATE).Width = GRID_COLUMN_INSERTED_DATE_WIDTH
      .Column(GRID_COLUMN_INSERTED_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_EXTRACTION_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2353)
      .Column(GRID_COLUMN_EXTRACTION_DATE).Width = GRID_COLUMN_EXTRACTION_DATE_WIDTH
      .Column(GRID_COLUMN_EXTRACTION_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      .Column(GRID_COLUMN_EMPLOYEE_INSERTION).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2187)
      .Column(GRID_COLUMN_EMPLOYEE_INSERTION).Width = GRID_COLUMN_EMPLOYEE_INSERTION_WIDTH

      .Column(GRID_COLUMN_NOTES).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2188)
      .Column(GRID_COLUMN_NOTES).Width = GRID_COLUMN_NOTES_WIDTH

      .Column(GRID_COLUMN_STACKER_COLLECTION_ID).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2188)
      .Column(GRID_COLUMN_STACKER_COLLECTION_ID).Width = 0
    End With

  End Sub ' GUI_StyleSheet

  ' PURPOSE: Fill table details
  '
  ' PARAMS:
  '   - INPUT:
  '     - StackerId: identifier of stacker
  '
  Private Sub FillTableDetails(ByVal StackerId As Int64)

    Dim _sql_query As String
    Dim _table As DataTable = Nothing
    Dim _row As DataRow
    Dim _db_row As frm_tito_stackers_sel.CLASS_DB_ROW
    Dim _idx_row As Integer

    grid_collection_history.Redraw = False
    Call grid_collection_history.Clear()

    _idx_row = -1
    _sql_query = GetSqlQueryDetails(StackerId)

    Try
      _table = GUI_GetTableUsingSQL(_sql_query, Integer.MaxValue)

      For Each _row In _table.Rows
        _db_row = New frm_tito_stackers_sel.CLASS_DB_ROW(_row)

        grid_collection_history.AddRow()
        _idx_row = grid_collection_history.NumRows - 1

        With grid_collection_history
          .Cell(_idx_row, GRID_COLUMN_ID).Value = _db_row.Value(SQL_COLUMN_ID)
          .Cell(_idx_row, GRID_COLUMN_TERMINAL).Value = _db_row.Value(SQL_COLUMN_TERMINAL)

          If Not _db_row.IsNull(GRID_COLUMN_STATUS) Then
            If _db_row.Value(GRID_COLUMN_STATUS) Then
              .Cell(_idx_row, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2211)
            Else
              .Cell(_idx_row, GRID_COLUMN_STATUS).Value = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2212)
            End If
          End If

          .Cell(_idx_row, GRID_COLUMN_EMPLOYEE_COLLECTION).Value = mdl_tito.GetValueForGrid(_db_row.Value(SQL_COLUMN_EMPLOYEE_COLLECTION))
          .Cell(_idx_row, GRID_COLUMN_READ_MONEY).Value = mdl_tito.GetValueForGrid(_db_row.Value(SQL_COLUMN_READ_MONEY))
          .Cell(_idx_row, GRID_COLUMN_EXPECTED_MONEY).Value = mdl_tito.GetFormatedCurrency(_db_row.Value(SQL_COLUMN_EXPECTED_MONEY))
          .Cell(_idx_row, GRID_COLUMN_BILLS_READ).Value = mdl_tito.GetValueForGrid(_db_row.Value(SQL_COLUMN_BILLS_READ))
          .Cell(_idx_row, GRID_COLUMN_BILLS_AMOUNT).Value = mdl_tito.GetFormatedCurrency(_db_row.Value(SQL_COLUMN_BILLS_AMOUNT))
          .Cell(_idx_row, GRID_COLUMN_TICKETS_READ).Value = mdl_tito.GetValueForGrid(_db_row.Value(SQL_COLUMN_TICKETS_READ))
          .Cell(_idx_row, GRID_COLUMN_TICKETS_AMOUNT).Value = mdl_tito.GetFormatedCurrency(_db_row.Value(SQL_COLUMN_TICKETS_AMOUNT))
          .Cell(_idx_row, GRID_COLUMN_COLLECT_MONEY_DATE).Value = mdl_tito.GetFormatedDate(_db_row.Value(SQL_COLUMN_COLLECT_MONEY_DATE))
          .Cell(_idx_row, GRID_COLUMN_INSERTED_DATE).Value = mdl_tito.GetFormatedDate(_db_row.Value(SQL_COLUMN_INSERTED_DATE))
          .Cell(_idx_row, GRID_COLUMN_EMPLOYEE_INSERTION).Value = mdl_tito.GetValueForGrid(_db_row.Value(SQL_COLUMN_EMPLOYEE_INSERTION))
          .Cell(_idx_row, GRID_COLUMN_EXTRACTION_DATE).Value = mdl_tito.GetFormatedDate(_db_row.Value(SQL_COLUMN_EXTRACTION_DATE))
          .Cell(_idx_row, GRID_COLUMN_NOTES).Value = mdl_tito.GetValueForGrid(_db_row.Value(SQL_COLUMN_NOTES))
          .Cell(_idx_row, GRID_COLUMN_STACKER_COLLECTION_ID).Value = mdl_tito.GetValueForGrid(_db_row.Value(SQL_COLUMN_STACKER_COLLECTION_ID))
        End With
      Next

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
    End Try

    grid_collection_history.SelectFirstRow(True)

    grid_collection_history.Redraw = True
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Enabled = _table.Rows.Count > 0
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_1).Enabled = _table.Rows.Count > 0
    GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_2).Enabled = _table.Rows.Count > 0

  End Sub ' FillTableWithDetails

  ' PURPOSE: Return a string with query for details search
  '
  ' PARAMS:
  '   - INPUT:
  '     - StackerId: identifier of stacker
  '
  ' RETURNS:
  '     ID: if Terminal with name exists
  '     0 : otherwise
  Private Function GetSqlQueryDetails(ByVal StackerId As Int64) As String

    Dim _str_condition As String
    Dim _str_sql_builder As StringBuilder

    _str_sql_builder = New StringBuilder()

    _str_sql_builder.AppendLine("   SELECT   MC.MC_COLLECTION_ID ")
    _str_sql_builder.AppendLine("          , MC.MC_STACKER_ID   ")
    _str_sql_builder.AppendLine("          , ISNULL(TE.TE_NAME, '')   ")
    _str_sql_builder.AppendLine("          , MC.MC_BALANCED ")
    _str_sql_builder.AppendLine("          , ISNULL(USR1.GU_USERNAME, '' ) AS USER_NM ")
    _str_sql_builder.AppendLine("          , ISNULL(TT_TABLE.TM_MONEY_COUNT, 0) AS THEO_MONEY_COUNT  ")
    _str_sql_builder.AppendLine("          , ISNULL(TS_TABLE.TM_MONEY_SUM, 0) AS THEO_MONEY_COUNT   ")
    _str_sql_builder.AppendLine("          , ISNULL(MCD_TABLE.MCD_BILLS_COUNT, 0) AS REAL_BILLS_COUNT  ")
    _str_sql_builder.AppendLine("          , ISNULL(MCD_TABLE.MCD_BILLS_SUM, 0) AS REAL_BILLS_SUM    ")
    _str_sql_builder.AppendLine("          , ISNULL(TI_TABLE.TI_TICKET_COUNT, 0) AS REAL_TICKETS_COUNT  ")
    _str_sql_builder.AppendLine("          , ISNULL(TI_TABLE.TI_TICKET_SUM, 0) AS REAL_TICKETS_SUM    ")
    _str_sql_builder.AppendLine("          , MC.MC_COLLECTION_DATETIME  ")
    _str_sql_builder.AppendLine("          , MC.MC_DATETIME  ")
    _str_sql_builder.AppendLine("          , ISNULL(USR2.GU_USERNAME, '') AS INSERTED_USER_NAME ")
    _str_sql_builder.AppendLine("          , MC.MC_EXTRACTION_DATETIME  ")
    _str_sql_builder.AppendLine("          , ISNULL(MC.MC_NOTES, '')  ")
    _str_sql_builder.AppendLine("     FROM   MONEY_COLLECTIONS AS MC  ")
    _str_sql_builder.AppendLine("            LEFT JOIN TERMINALS AS TE  ON MC.MC_TERMINAL_ID = TE.TE_TERMINAL_ID   ")
    _str_sql_builder.AppendLine("            LEFT JOIN GUI_USERS AS USR1 ON MC.MC_USER_ID = USR1.GU_USER_ID  ")
    _str_sql_builder.AppendLine("            LEFT JOIN GUI_USERS AS USR2 ON MC.MC_INSERTED_USER_ID = USR2.GU_USER_ID  ")
    _str_sql_builder.AppendLine("            LEFT JOIN ")
    _str_sql_builder.AppendLine("            (    SELECT   COUNT(*) AS TM_MONEY_COUNT    ")
    _str_sql_builder.AppendLine("                        , TM_COLLECTION_ID  ")
    _str_sql_builder.AppendLine("                   FROM   TERMINAL_MONEY    ")
    _str_sql_builder.AppendLine("               GROUP BY   TERMINAL_MONEY.TM_COLLECTION_ID   ")
    _str_sql_builder.AppendLine("            ) TT_TABLE    ")
    _str_sql_builder.AppendLine("       ON   TT_TABLE.TM_COLLECTION_ID = MC.MC_COLLECTION_ID   ")
    _str_sql_builder.AppendLine("             LEFT JOIN  ")
    _str_sql_builder.AppendLine("           (    SELECT   SUM(TM_AMOUNT) AS TM_MONEY_SUM  ")
    _str_sql_builder.AppendLine("                       , TM_COLLECTION_ID   ")
    _str_sql_builder.AppendLine("                  FROM   TERMINAL_MONEY  ")
    _str_sql_builder.AppendLine("              GROUP BY   TERMINAL_MONEY.TM_COLLECTION_ID   ")
    _str_sql_builder.AppendLine("            ) TS_TABLE    ")
    _str_sql_builder.AppendLine("       ON   TS_TABLE.TM_COLLECTION_ID = MC.MC_COLLECTION_ID   ")
    _str_sql_builder.AppendLine("            LEFT JOIN ")
    _str_sql_builder.AppendLine("               (SELECT  SUM(MCD_NUM_COLLECTED) AS MCD_BILLS_COUNT   ")
    _str_sql_builder.AppendLine("                      , SUM(MCD_FACE_VALUE * MCD_NUM_COLLECTED) AS MCD_BILLS_SUM  ")
    _str_sql_builder.AppendLine("                      , MCD_COLLECTION_ID   ")
    _str_sql_builder.AppendLine("                 FROM   MONEY_COLLECTION_DETAILS  ")
    _str_sql_builder.AppendLine("             GROUP BY   MONEY_COLLECTION_DETAILS.MCD_COLLECTION_ID    ")
    _str_sql_builder.AppendLine("           ) MCD_TABLE    ")
    _str_sql_builder.AppendLine("       ON   MCD_TABLE.MCD_COLLECTION_ID = MC.MC_COLLECTION_ID ")
    _str_sql_builder.AppendLine("            LEFT JOIN ")
    _str_sql_builder.AppendLine("            (   SELECT   COUNT(TI_TICKET_ID) AS TI_TICKET_COUNT   ")
    _str_sql_builder.AppendLine("                       , SUM(TI_AMOUNT) AS TI_TICKET_SUM  ")
    _str_sql_builder.AppendLine("                        , TI_MONEY_COLLECTION_ID   ")
    _str_sql_builder.AppendLine("                   FROM   TICKETS    ")
    _str_sql_builder.AppendLine("                  WHERE   TICKETS.TI_COLLECTED = 1   ")
    _str_sql_builder.AppendLine("               GROUP BY   TICKETS.TI_MONEY_COLLECTION_ID   ")
    _str_sql_builder.AppendLine("             ) TI_TABLE   ")
    _str_sql_builder.AppendLine("        ON   TI_TABLE.TI_MONEY_COLLECTION_ID = MC_COLLECTION_ID  ")
    _str_sql_builder.AppendLine("     WHERE   MC.MC_COLLECTION_DATETIME IS NOT NULL ")
    _str_sql_builder.AppendLine("       AND   MC.MC_STACKER_ID = " & StackerId.ToString())


    'las fechas para filtrar los stackers_collections, dependen del momento de inserci�n y
    'del momento de recaudaci�n
    _str_condition = uc_dates.GetSqlFilterCondition("MC.MC_DATETIME")

    If Not String.IsNullOrEmpty(_str_condition) Then
      _str_sql_builder.AppendLine("   AND    (")
      _str_sql_builder.AppendLine("(" & _str_condition & ")")
      _str_sql_builder.AppendLine("           OR ")
      _str_sql_builder.AppendLine("(" & uc_dates.GetSqlFilterCondition("MC.MC_EXTRACTION_DATETIME") & ")")
      _str_sql_builder.AppendLine("           )")
    End If

    Return _str_sql_builder.ToString()

  End Function ' GetSqlQueryDetails

  ' PURPOSE: Call Detail collection Viewer
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  Protected Sub ViewCollectionDetails()

    Dim _idx_row As Int32
    Dim _id_stacker_collection As Int64
    Dim _frm_edit As frm_new_collection_edit

    If grid_collection_history.SelectedRows() Is Nothing Then

      Return
    End If

    If grid_collection_history.SelectedRows().Length > 0 Then
      _idx_row = grid_collection_history.SelectedRows(0)
      _id_stacker_collection = grid_collection_history.Cell(_idx_row, GRID_COLUMN_STACKER_COLLECTION_ID).Value

      _frm_edit = New frm_new_collection_edit
      Call _frm_edit.ShowEditItem(_id_stacker_collection)
      _frm_edit = Nothing
      Call grid_collection_history.Focus()
    End If

  End Sub

  ' PURPOSE: Generate Excel report with Table Details
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  Protected Sub ExcelWithDetails(ByVal Title As String, ByRef Grid As GUI_Controls.uc_grid)

    Dim _print_datetime As Date
    Dim _excel_data As GUI_Reports.CLASS_EXCEL_DATA = Nothing
    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA

    Windows.Forms.Cursor.Current = Cursors.WaitCursor

    Try
      _excel_data = New GUI_Reports.CLASS_EXCEL_DATA()
      If _excel_data.IsNull() Then
        Return
      End If

      _print_datetime = GUI_GetDateTime()

      If Not _excel_data.Cancelled Then
        Call frm_tito_stackers_sel.GUI_ReportParamsX(Grid, _excel_data, Title, _print_datetime)
      End If

      If Not _excel_data.Cancelled Then
        _print_data = LoadReportFilters()
        _excel_data.UpdateReportFilters(_print_data.Filter.ItemArray)
        _print_data = Nothing
      End If

      If Not _excel_data.Cancelled Then
        Call frm_tito_stackers_sel.GUI_ReportDataX(Grid, _excel_data)
      End If

      If Not _excel_data.Cancelled Then
        Call _excel_data.Preview()
      End If

    Catch _ex As Exception
      Debug.WriteLine(_ex.Message)
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(2213), ENUM_MB_TYPE.MB_TYPE_ERROR, ENUM_MB_BTN.MB_BTN_OK, ENUM_MB_DEF_BTN.MB_DEF_BTN_1)
    Finally
      If _excel_data Is Nothing Then
      Else
        _excel_data.Dispose()
        _excel_data = Nothing
      End If
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub ' ExcelWithDetails

  ' PURPOSE: Print report with Table Details
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  Private Sub PrintDetails(ByVal Title As String, ByRef Grid As GUI_Controls.uc_grid)

    Dim _print_datetime As Date
    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA

    Windows.Forms.Cursor.Current = Cursors.WaitCursor
    Try

      _print_data = LoadReportFilters()
      _print_datetime = GUI_GetDateTime()

      Call frm_tito_stackers_sel.GUI_ReportParamsX(Grid, _print_data, Title, _print_datetime)
      Call frm_tito_stackers_sel.GUI_ReportDataX(Grid, _print_data)
      Call _print_data.Preview()

    Finally
      Windows.Forms.Cursor.Current = Cursors.Default
    End Try

  End Sub ' PrintDetails

  ' PURPOSE: Save the selected filter for print/excel
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  Private Function LoadReportFilters()

    Dim _string_tmp As String
    Dim _print_data As GUI_Reports.CLASS_PRINT_DATA

    _print_data = New GUI_Reports.CLASS_PRINT_DATA()

    _print_data.SetFilter(ef_stacker_id.Text, ef_stacker_id.Value)
    _print_data.SetFilter(ef_model.Text, ef_model.Value)
    _print_data.SetFilter(chk_deactivated.Text, IIf(chk_deactivated.Checked, _
                                                    GLB_NLS_GUI_PLAYER_TRACKING.GetString(278), _
                                                    GLB_NLS_GUI_PLAYER_TRACKING.GetString(279)))
    _print_data.SetFilter(lbl_inserted.Text, cmb_inserted_term.Text)

    _print_data.SetFilter(lbl_preassigned.Text, cmb_preassigned_prov.Text)
    _print_data.SetFilter(ef_stacker_status.Text, ef_stacker_status.Value)

    ' Filter detail Dates 
    _string_tmp = IIf(uc_dates.FromDateSelected, mdl_tito.GetFormatedDate(uc_dates.FromDate.AddHours(uc_dates.ClosingTime)), "")
    _print_data.SetFilter(uc_dates.Text & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(297), _string_tmp)
    _string_tmp = IIf(uc_dates.ToDateSelected, mdl_tito.GetFormatedDate(uc_dates.ToDate.AddHours(uc_dates.ClosingTime)), "")
    _print_data.SetFilter(uc_dates.Text & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(298), _string_tmp)

    ' TODO: activar si es necesario
    '_print_data.SetFilter(lbl_notes.Text, txt_notes.Text)

    _print_data.FilterHeaderWidth(1) = 2000
    _print_data.FilterHeaderWidth(2) = 2500

    Return _print_data

  End Function ' LoadReportFilters

  Private Sub GUI_FilterReset()

    Dim _closing_time As Integer
    Dim _final_time As Date

    _closing_time = GetDefaultClosingTime()
    _final_time = New DateTime(Now.Year, Now.Month, Now.Day, _closing_time, 0, 0)
    _final_time = _final_time.AddDays(1)
    uc_dates.ToDate = _final_time.Date
    uc_dates.ToDateSelected = True
    uc_dates.FromDate = _final_time.AddDays(-2)
    uc_dates.FromDateSelected = True
    uc_dates.ClosingTime = _closing_time

  End Sub ' GUI_FilterReset

  Private Sub RefreshScreenByStatus(ByVal Status As TITO_STACKER_STATUS, ByVal IgnoreDeactivatedCheck As Boolean)

    ef_stacker_status.Value = mdl_tito.StackerStatusAsString(Status)
    GUI_Button(ENUM_BUTTON.BUTTON_DELETE).Enabled = (ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT And _
                                                     Status = TITO_STACKER_STATUS.AVAILABLE) And Me.Permissions.Delete

    If Status = TITO_STACKER_STATUS.DISABLED Then
      cmb_preassigned_prov.Enabled = False
      cmb_inserted_term.Enabled = False
      cmb_inserted_term.SelectedIndex = -1
      gb_status.Enabled = False
      chk_deactivated.Enabled = False
      ef_model.Enabled = False
      txt_notes.Enabled = False
      chk_deactivated.Checked = True

      Return
    End If

    If Status = TITO_STACKER_STATUS.DEACTIVATED Then
      cmb_preassigned_prov.Enabled = False
      cmb_inserted_term.Enabled = False
      cmb_inserted_term.SelectedIndex = -1
      gb_status.Enabled = False
      chk_deactivated.Enabled = True
      If Not IgnoreDeactivatedCheck Then
        chk_deactivated.Checked = True
      End If

      Return
    End If

    chk_deactivated.Enabled = (ScreenMode = ENUM_SCREEN_MODE.MODE_EDIT And _
                               Status < TITO_STACKER_STATUS.INSERTED_PENDING)

    cmb_preassigned_prov.Enabled = True
    cmb_inserted_term.Enabled = (Status < TITO_STACKER_STATUS.INSERTED_VALIDATED Or Me.Permissions.Execute) 'Execute permission must be enabled to change the terminal
    gb_status.Enabled = True

    opt_available.Enabled = (Status < TITO_STACKER_STATUS.INSERTED_VALIDATED Or Me.Permissions.Execute) 'Execute permission must be enabled to change status to available

    opt_inserted_pending.Enabled = (Status = TITO_STACKER_STATUS.AVAILABLE Or _
                                    Status = TITO_STACKER_STATUS.INSERTED_PENDING)
    opt_inserted_validated.Enabled = (Status = TITO_STACKER_STATUS.INSERTED_PENDING Or _
                                      Status = TITO_STACKER_STATUS.INSERTED_VALIDATED)
    opt_collection_pending.Enabled = (Status = TITO_STACKER_STATUS.INSERTED_VALIDATED Or _
                                      Status = TITO_STACKER_STATUS.COLLECTION_PENDING)

    opt_available.Checked = (Status = TITO_STACKER_STATUS.AVAILABLE)
    opt_inserted_pending.Checked = (Status = TITO_STACKER_STATUS.INSERTED_PENDING)
    opt_inserted_validated.Checked = (Status = TITO_STACKER_STATUS.INSERTED_VALIDATED)
    opt_collection_pending.Checked = (Status = TITO_STACKER_STATUS.COLLECTION_PENDING)

  End Sub

#End Region ' Private Functions and Methods

#Region " Events "

  Private Sub cmb_inserted_term_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmb_inserted_term.SelectedIndexChanged
    Dim _stacker As CLASS_STACKER

    lbl_warning.Visible = False

    If cmb_inserted_term.Enabled AndAlso cmb_inserted_term.SelectedIndex > 0 Then
      _stacker = DbEditedObject
      lbl_warning.Visible = (cmb_inserted_term.SelectedValue <> _stacker.InsertedTerminal)
    End If

  End Sub

  Private Sub cb_deactivated_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk_deactivated.CheckedChanged

    Dim _stacker As CLASS_STACKER

    _stacker = DbEditedObject

    If chk_deactivated.Checked Then
      _stacker.Status = IIf(chk_deactivated.Enabled, TITO_STACKER_STATUS.DEACTIVATED, TITO_STACKER_STATUS.DISABLED)
    Else
      _stacker.Status = TITO_STACKER_STATUS.AVAILABLE
    End If

    RefreshScreenByStatus(_stacker.Status, True)

  End Sub

  Private Sub btn_search_collections_ClickEvent() Handles btn_search_collections.ClickEvent

    Dim _stacker As CLASS_STACKER

    ' se controla el rango de fechas
    If uc_dates.FromDateSelected And uc_dates.ToDateSelected Then
      If uc_dates.FromDate > uc_dates.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Return
      End If
    End If

    _stacker = DbEditedObject
    Call FillTableDetails(_stacker.StackerId)

    If grid_collection_history.NumRows = 0 Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(119), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_INFO, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
    End If

  End Sub

  Private Sub grid_details_ButtonClickEvent() Handles grid_collection_history.DataSelectedEvent

    ViewCollectionDetails()

  End Sub

  Private Sub opt_available_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opt_available.CheckedChanged
    cmb_inserted_term.Enabled = Not opt_available.Checked
    cmb_inserted_term.SelectedIndex = -1
  End Sub

#End Region 'Events

End Class