'--------------------------------------------------------------------------------------
' Copyright � 2014 Win Systems Ltd.
'--------------------------------------------------------------------------------------
'
'   MODULE NAME :  frm_ticket_out_report_sel.vb
'
'   DESCRIPTION :  Ticket Out Report
'
'        AUTHOR :  David Rigal Vall
'
' CREATION DATE :  18-DEC-2014
'
' REVISION HISTORY
'
' Date         Author Description
' -----------  ------ ------------------------------------------------------------------
' 18-DEC-2014  DRV    Initial version
' 12-JAN-2015  SGB    Fixed Bug WIG-1916: Format column date in export excel
'---------------------------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports System.Text
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports WSI.Common.TITO
Imports WSI.Common
Imports System.Data

Public Class frm_ticket_out_report_sel
  Inherits GUI_Controls.frm_base_sel

#Region " Members "

  Dim m_ini_date As DateTime
  Dim m_fin_date As DateTime
  Dim m_total_row As TotalRow

#End Region ' Members

#Region " Constants "

  Private Const GRID_COLUMNS_COUNT As Integer = 14
  Private Const GRID_HEADERS_COUNT As Integer = 2

  ' Text format columns
  Private Const EXCEL_COLUMN_DATE As Integer = 0

  ' Index Grid Columns
  Private Const GRID_COLUMN_SELECT As Integer = 0
  Private Const GRID_COLUMN_DATE As Integer = 1
  Private Const GRID_COLUMN_PRE_CREATED_COUNT As Integer = 2
  Private Const GRID_COLUMN_PRE_CREATED_AMOUNT As Integer = 3
  Private Const GRID_COLUMN_PRE_CASHED_COUNT As Integer = 4
  Private Const GRID_COLUMN_PRE_CASHED_AMOUNT As Integer = 5
  Private Const GRID_COLUMN_PRE_EXPIRED_COUNT As Integer = 6
  Private Const GRID_COLUMN_PRE_EXPIRED_AMOUNT As Integer = 7
  Private Const GRID_COLUMN_PRE_VALID_COUNT As Integer = 8
  Private Const GRID_COLUMN_PRE_VALID_AMOUNT As Integer = 9
  Private Const GRID_COLUMN_CUR_NOT_CASHED_COUNT As Integer = 10
  Private Const GRID_COLUMN_CUR_NOT_CASHED_AMOUNT As Integer = 11
  Private Const GRID_COLUMN_CUR_VALID_COUNT As Integer = 12
  Private Const GRID_COLUMN_CUR_VALID_AMOUNT As Integer = 13

  ' Grid With
  Private Const GRID_WIDTH_SELECT As Integer = 200
  Private Const GRID_WIDTH_DATE As Integer = 1200
  Private Const GRID_WIDTH_PRE_CREATED_COUNT As Integer = 1800
  Private Const GRID_WIDTH_PRE_CREATED_AMOUNT As Integer = 1700
  Private Const GRID_WIDTH_PRE_CASHED_COUNT As Integer = 1800
  Private Const GRID_WIDTH_PRE_CASHED_AMOUNT As Integer = 1800
  Private Const GRID_WIDTH_PRE_EXPIRED_COUNT As Integer = 1800
  Private Const GRID_WIDTH_PRE_EXPIRED_AMOUNT As Integer = 1700
  Private Const GRID_WIDTH_PRE_VALID_COUNT As Integer = 1500
  Private Const GRID_WIDTH_PRE_VALID_AMOUNT As Integer = 1400
  Private Const GRID_WIDTH_CUR_NOT_CASHED_COUNT As Integer = 2100
  Private Const GRID_WIDTH_CUR_NOT_CASHED_AMOUNT As Integer = 2000
  Private Const GRID_WIDTH_CUR_VALID_COUNT As Integer = 1500
  Private Const GRID_WIDTH_CUR_VALID_AMOUNT As Integer = 1400

  ' SQL Columns
  Private Const SQL_COLUMN_DATE As Integer = 0
  Private Const SQL_COLUMN_PRE_CREATED_COUNT As Integer = 1
  Private Const SQL_COLUMN_PRE_CREATED_AMOUNT As Integer = 2
  Private Const SQL_COLUMN_PRE_CASHED_COUNT As Integer = 3
  Private Const SQL_COLUMN_PRE_CASHED_AMOUNT As Integer = 4
  Private Const SQL_COLUMN_PRE_EXPIRED_COUNT As Integer = 5
  Private Const SQL_COLUMN_PRE_EXPIRED_AMOUNT As Integer = 6
  Private Const SQL_COLUMN_CUR_NOT_CASHED_COUNT As Integer = 7
  Private Const SQL_COLUMN_CUR_NOT_CASHED_AMOUNT As Integer = 8
  Private Const SQL_COLUMN_CUR_EXPIRED_COUNT As Integer = 9
  Private Const SQL_COLUMN_CUR_EXPIRED_AMOUNT As Integer = 10

#End Region ' Constants

#Region " STRUCTS "
  Private Structure TotalRow
    Public PreCreatedCount As Int32
    Public PreCreatedAmount As Decimal
    Public PreCashedCount As Int32
    Public PreCashedAmount As Decimal
    Public PreExpiredCount As Int32
    Public PreExpiredAmount As Decimal
    Public PreValidCount As Int32
    Public PreValidAmount As Decimal
    Public CurNotCashedCount As Int32
    Public CurNotCashedAmount As Decimal
    Public CurValidCount As Int32
    Public CurValidAmount As Decimal

    Public Sub Init()
      PreCreatedCount = 0
      PreCreatedAmount = 0
      PreCashedCount = 0
      PreCashedAmount = 0
      PreExpiredCount = 0
      PreExpiredAmount = 0
      PreValidCount = 0
      PreValidAmount = 0
      CurNotCashedCount = 0
      CurNotCashedAmount = 0
      CurValidCount = 0
      CurValidAmount = 0
    End Sub
  End Structure
#End Region

#Region " Private Functions and Methods "

#End Region

#Region " OVERRIDES "
  ' PURPOSE : Define layout of all Main Grid Columns
  '
  '  PARAMS :
  '     - INPUT :
  '           - None 
  '     - OUTPUT :
  '           - None
  '
  Private Sub GUI_StyleSheet()
    With Me.Grid
      Call .Init(GRID_COLUMNS_COUNT, GRID_HEADERS_COUNT)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      .Column(GRID_COLUMN_SELECT).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_SELECT).Header(1).Text = String.Empty
      .Column(GRID_COLUMN_SELECT).Width = GRID_WIDTH_SELECT
      .Column(GRID_COLUMN_SELECT).IsColumnPrintable = False
      .Column(GRID_COLUMN_SELECT).HighLightWhenSelected = False

      ' Date
      .Column(GRID_COLUMN_DATE).Header(0).Text = String.Empty
      .Column(GRID_COLUMN_DATE).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5807)
      .Column(GRID_COLUMN_DATE).Width = GRID_WIDTH_DATE
      .Column(GRID_COLUMN_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' Previous Day Created Ticket Count
      .Column(GRID_COLUMN_PRE_CREATED_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5806)
      .Column(GRID_COLUMN_PRE_CREATED_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5808)
      .Column(GRID_COLUMN_PRE_CREATED_COUNT).Width = IIf(chk_Show_Count.Checked, GRID_WIDTH_PRE_CREATED_COUNT, 0)
      .Column(GRID_COLUMN_PRE_CREATED_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Previous Day Created Ticket Amount
      .Column(GRID_COLUMN_PRE_CREATED_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5806)
      .Column(GRID_COLUMN_PRE_CREATED_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5809)
      .Column(GRID_COLUMN_PRE_CREATED_AMOUNT).Width = GRID_WIDTH_PRE_CREATED_AMOUNT
      .Column(GRID_COLUMN_PRE_CREATED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Previous Day Total Ticket Cashed Count
      .Column(GRID_COLUMN_PRE_CASHED_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5806)
      .Column(GRID_COLUMN_PRE_CASHED_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5810)
      .Column(GRID_COLUMN_PRE_CASHED_COUNT).Width = IIf(chk_Show_Count.Checked, GRID_WIDTH_PRE_CASHED_COUNT, 0)
      .Column(GRID_COLUMN_PRE_CASHED_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Previous Day Total Ticket Cashed Amount
      .Column(GRID_COLUMN_PRE_CASHED_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5806)
      .Column(GRID_COLUMN_PRE_CASHED_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5811)
      .Column(GRID_COLUMN_PRE_CASHED_AMOUNT).Width = GRID_WIDTH_PRE_CASHED_AMOUNT
      .Column(GRID_COLUMN_PRE_CASHED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Previous Day Total Ticket Expired Count
      .Column(GRID_COLUMN_PRE_EXPIRED_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5806)
      .Column(GRID_COLUMN_PRE_EXPIRED_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5812)
      .Column(GRID_COLUMN_PRE_EXPIRED_COUNT).Width = IIf(chk_Show_Count.Checked, GRID_WIDTH_PRE_EXPIRED_COUNT, 0)
      .Column(GRID_COLUMN_PRE_EXPIRED_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Previous Day Total Ticket Expired Amount
      .Column(GRID_COLUMN_PRE_EXPIRED_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5806)
      .Column(GRID_COLUMN_PRE_EXPIRED_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5813)
      .Column(GRID_COLUMN_PRE_EXPIRED_AMOUNT).Width = GRID_WIDTH_PRE_EXPIRED_AMOUNT
      .Column(GRID_COLUMN_PRE_EXPIRED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Previous Day Total Valid Ticket Count
      .Column(GRID_COLUMN_PRE_VALID_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5806)
      .Column(GRID_COLUMN_PRE_VALID_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5814)
      .Column(GRID_COLUMN_PRE_VALID_COUNT).Width = IIf(chk_Show_Count.Checked, GRID_WIDTH_PRE_VALID_COUNT, 0)
      .Column(GRID_COLUMN_PRE_VALID_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Previous Day Total Valid Ticket Amount
      .Column(GRID_COLUMN_PRE_VALID_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5806)
      .Column(GRID_COLUMN_PRE_VALID_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5815)
      .Column(GRID_COLUMN_PRE_VALID_AMOUNT).Width = GRID_WIDTH_PRE_VALID_AMOUNT
      .Column(GRID_COLUMN_PRE_VALID_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Previous Day Total Not Cashed Ticket Count
      .Column(GRID_COLUMN_CUR_NOT_CASHED_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5807)
      .Column(GRID_COLUMN_CUR_NOT_CASHED_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5816)
      .Column(GRID_COLUMN_CUR_NOT_CASHED_COUNT).Width = IIf(chk_Show_Count.Checked, GRID_WIDTH_CUR_NOT_CASHED_COUNT, 0)
      .Column(GRID_COLUMN_CUR_NOT_CASHED_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Previous Day Total Not Cashed Ticket Amount
      .Column(GRID_COLUMN_CUR_NOT_CASHED_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5807)
      .Column(GRID_COLUMN_CUR_NOT_CASHED_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5817)
      .Column(GRID_COLUMN_CUR_NOT_CASHED_AMOUNT).Width = GRID_WIDTH_CUR_NOT_CASHED_AMOUNT
      .Column(GRID_COLUMN_CUR_NOT_CASHED_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Previous Day Total Valid Ticket Count
      .Column(GRID_COLUMN_CUR_VALID_COUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5807)
      .Column(GRID_COLUMN_CUR_VALID_COUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5814)
      .Column(GRID_COLUMN_CUR_VALID_COUNT).Width = IIf(chk_Show_Count.Checked, GRID_WIDTH_CUR_VALID_COUNT, 0)
      .Column(GRID_COLUMN_CUR_VALID_COUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' Previous Day Total Valid Ticket Amount
      .Column(GRID_COLUMN_CUR_VALID_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5807)
      .Column(GRID_COLUMN_CUR_VALID_AMOUNT).Header(1).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5815)
      .Column(GRID_COLUMN_CUR_VALID_AMOUNT).Width = GRID_WIDTH_CUR_VALID_AMOUNT
      .Column(GRID_COLUMN_CUR_VALID_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT
    End With

  End Sub ' GUI_StyleSheet

  Protected Overrides Function GUI_GetQueryType() As GUI_Controls.frm_base_sel.ENUM_QUERY_TYPE
    Return ENUM_QUERY_TYPE.QUERY_CUSTOM
  End Function

  Protected Overrides Function GUI_FilterCheck() As Boolean
    ' Dates selection 
    If Me.uc_dsl.FromDateSelected And Me.uc_dsl.ToDateSelected Then
      If Me.uc_dsl.FromDate > Me.uc_dsl.ToDate Then
        Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
        Call Me.uc_dsl.Focus()

        Return False
      End If
    End If

    If uc_dsl.ToDateSelected Then
      m_fin_date = uc_dsl.ToDate
      m_fin_date = m_fin_date.AddMinutes(GeneralParam.GetInt32("WigosGUI", "ClosingTimeMinutes"))
      m_fin_date = m_fin_date.AddHours(GeneralParam.GetInt32("WigosGUI", "ClosingTime"))
    Else
      m_fin_date = DateTime.MaxValue
    End If

    If uc_dsl.FromDateSelected Then
      m_ini_date = uc_dsl.FromDate
      m_ini_date = m_ini_date.AddMinutes(GeneralParam.GetInt32("WigosGUI", "ClosingTimeMinutes"))
      m_ini_date = m_ini_date.AddHours(GeneralParam.GetInt32("WigosGUI", "ClosingTime"))
    Else
      m_ini_date = DateTime.MinValue
    End If

    Return True
  End Function


  Protected Overrides Sub GUI_ExecuteQueryCustom()
    Dim _table As DataTable
    Dim _row_idx As Int32
    Dim _db_row As CLASS_DB_ROW

    _table = New DataTable()
    _row_idx = 0

    If Not TicketsReports.GetTicketOutReport(m_ini_date, m_fin_date, _table) Then
      Call NLS_MsgBox(GLB_NLS_GUI_CONTROLS.Id(123), mdl_NLS.ENUM_MB_TYPE.MB_TYPE_ERROR, mdl_NLS.ENUM_MB_BTN.MB_BTN_OK)
      MyBase.m_user_canceled_data_shows = True

      Return
    End If

    Call GUI_StyleSheet()
    Call GUI_BeforeFirstRow()

    For Each _row As DataRow In _table.Rows
      _db_row = New CLASS_DB_ROW(_row)

      If GUI_CheckOutRowBeforeAdd(_db_row) Then
        _row_idx = Me.Grid.AddRow()
        Call GUI_SetupRow(_row_idx, _db_row)
      End If
    Next

    Call GUI_AfterLastRow()
  End Sub

  Public Overrides Sub GUI_SetFormId()
    Me.FormId = ENUM_FORM.FORM_TICKET_OUT_REPORT
  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    ' Form title 
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5805)
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_EDITION
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False

    ' Date
    uc_dsl.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5807)

    Me.chk_Show_Count.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5818)

    Call SetDefaultDate()
    Call GUI_StyleSheet()

  End Sub ' GUI_InitControls

  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultDate()
    chk_Show_Count.Checked = False
  End Sub ' GUI_FilterReset

  ' PURPOSE: Get report parameters and headers
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Protected Overrides Sub GUI_ReportParams(ByVal ExcelData As GUI_Reports.CLASS_EXCEL_DATA, Optional ByVal FirstColIndex As Integer = 0)

    Call MyBase.GUI_ReportParams(ExcelData)

    ExcelData.SetColumnFormat(EXCEL_COLUMN_DATE, GUI_Reports.CLASS_EXCEL_DATA.EXCEL_FORMAT.DATE)

  End Sub

  Public Overrides Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                         ByVal DbRow As GUI_Controls.frm_base_sel.CLASS_DB_ROW) As Boolean
    Dim _aux_count As Int32
    Dim _aux_amount As Decimal

    Grid.Cell(RowIndex, GRID_COLUMN_DATE).Value = GUI_FormatDate(DbRow.Value(SQL_COLUMN_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT)

    Grid.Cell(RowIndex, GRID_COLUMN_PRE_CREATED_COUNT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_PRE_CREATED_COUNT), 0)
    Grid.Cell(RowIndex, GRID_COLUMN_PRE_CREATED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PRE_CREATED_AMOUNT))
    m_total_row.PreCreatedCount += DbRow.Value(SQL_COLUMN_PRE_CREATED_COUNT)
    m_total_row.PreCreatedAmount += DbRow.Value(SQL_COLUMN_PRE_CREATED_AMOUNT)

    Grid.Cell(RowIndex, GRID_COLUMN_PRE_CASHED_COUNT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_PRE_CASHED_COUNT), 0)
    Grid.Cell(RowIndex, GRID_COLUMN_PRE_CASHED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PRE_CASHED_AMOUNT))
    m_total_row.PreCashedCount += DbRow.Value(SQL_COLUMN_PRE_CASHED_COUNT)
    m_total_row.PreCashedAmount += DbRow.Value(SQL_COLUMN_PRE_CASHED_AMOUNT)

    Grid.Cell(RowIndex, GRID_COLUMN_PRE_EXPIRED_COUNT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_PRE_EXPIRED_COUNT), 0)
    Grid.Cell(RowIndex, GRID_COLUMN_PRE_EXPIRED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_PRE_EXPIRED_AMOUNT))
    m_total_row.PreExpiredCount += DbRow.Value(SQL_COLUMN_PRE_EXPIRED_COUNT)
    m_total_row.PreExpiredAmount += DbRow.Value(SQL_COLUMN_PRE_EXPIRED_AMOUNT)

    _aux_count = DbRow.Value(SQL_COLUMN_PRE_CREATED_COUNT) - DbRow.Value(SQL_COLUMN_PRE_CASHED_COUNT) - DbRow.Value(SQL_COLUMN_PRE_EXPIRED_COUNT)
    _aux_amount = DbRow.Value(SQL_COLUMN_PRE_CREATED_AMOUNT) - DbRow.Value(SQL_COLUMN_PRE_CASHED_AMOUNT) - DbRow.Value(SQL_COLUMN_PRE_EXPIRED_AMOUNT)
    Grid.Cell(RowIndex, GRID_COLUMN_PRE_VALID_COUNT).Value = GUI_FormatNumber(_aux_count, 0)
    Grid.Cell(RowIndex, GRID_COLUMN_PRE_VALID_AMOUNT).Value = GUI_FormatCurrency(_aux_amount)
    m_total_row.PreValidCount += _aux_count
    m_total_row.PreValidAmount += _aux_amount

    Grid.Cell(RowIndex, GRID_COLUMN_CUR_NOT_CASHED_COUNT).Value = GUI_FormatNumber(DbRow.Value(SQL_COLUMN_CUR_NOT_CASHED_COUNT), 0)
    Grid.Cell(RowIndex, GRID_COLUMN_CUR_NOT_CASHED_AMOUNT).Value = GUI_FormatCurrency(DbRow.Value(SQL_COLUMN_CUR_NOT_CASHED_AMOUNT))
    m_total_row.CurNotCashedCount += DbRow.Value(SQL_COLUMN_CUR_NOT_CASHED_COUNT)
    m_total_row.CurNotCashedAmount += DbRow.Value(SQL_COLUMN_CUR_NOT_CASHED_AMOUNT)

    _aux_count = DbRow.Value(SQL_COLUMN_CUR_NOT_CASHED_COUNT) - DbRow.Value(SQL_COLUMN_CUR_EXPIRED_COUNT)
    _aux_amount = DbRow.Value(SQL_COLUMN_CUR_NOT_CASHED_AMOUNT) - DbRow.Value(SQL_COLUMN_CUR_EXPIRED_AMOUNT)
    Grid.Cell(RowIndex, GRID_COLUMN_CUR_VALID_COUNT).Value = GUI_FormatNumber(_aux_count, 0)
    Grid.Cell(RowIndex, GRID_COLUMN_CUR_VALID_AMOUNT).Value = GUI_FormatCurrency(_aux_amount)
    m_total_row.CurValidCount += _aux_count
    m_total_row.CurValidAmount += _aux_amount

  End Function ' GUI_SetupRow

  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5807) & " " & GLB_NLS_GUI_AUDITOR.GetString(257), IIf(m_ini_date <> DateTime.MinValue, m_ini_date, "---"))
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(5807) & " " & GLB_NLS_GUI_AUDITOR.GetString(258), IIf(m_fin_date <> DateTime.MaxValue, m_fin_date, "---"))
  End Sub ' GUI_ReportFilter

  Protected Overrides Sub GUI_BeforeFirstRow()
    m_total_row.Init()
  End Sub

  Protected Overrides Sub GUI_AfterLastRow()
    Dim _row_idx As Int32

    _row_idx = Me.Grid.AddRow()

    Grid.Cell(_row_idx, GRID_COLUMN_DATE).Value = GLB_NLS_GUI_STATISTICS.GetString(203)

    Grid.Cell(_row_idx, GRID_COLUMN_PRE_CREATED_COUNT).Value = GUI_FormatNumber(m_total_row.PreCreatedCount, 0)
    Grid.Cell(_row_idx, GRID_COLUMN_PRE_CREATED_AMOUNT).Value = GUI_FormatCurrency(m_total_row.PreCreatedAmount)

    Grid.Cell(_row_idx, GRID_COLUMN_PRE_CASHED_COUNT).Value = GUI_FormatNumber(m_total_row.PreCashedCount, 0)
    Grid.Cell(_row_idx, GRID_COLUMN_PRE_CASHED_AMOUNT).Value = GUI_FormatCurrency(m_total_row.PreCashedAmount)

    Grid.Cell(_row_idx, GRID_COLUMN_PRE_EXPIRED_COUNT).Value = GUI_FormatNumber(m_total_row.PreExpiredCount, 0)
    Grid.Cell(_row_idx, GRID_COLUMN_PRE_EXPIRED_AMOUNT).Value = GUI_FormatCurrency(m_total_row.PreExpiredAmount)

    Grid.Cell(_row_idx, GRID_COLUMN_PRE_VALID_COUNT).Value = GUI_FormatNumber(m_total_row.PreValidCount, 0)
    Grid.Cell(_row_idx, GRID_COLUMN_PRE_VALID_AMOUNT).Value = GUI_FormatCurrency(m_total_row.PreValidAmount)

    Grid.Cell(_row_idx, GRID_COLUMN_CUR_NOT_CASHED_COUNT).Value = GUI_FormatNumber(m_total_row.CurNotCashedCount, 0)
    Grid.Cell(_row_idx, GRID_COLUMN_CUR_NOT_CASHED_AMOUNT).Value = GUI_FormatCurrency(m_total_row.CurNotCashedAmount)

    Grid.Cell(_row_idx, GRID_COLUMN_CUR_VALID_COUNT).Value = GUI_FormatNumber(m_total_row.CurValidCount, 0)
    Grid.Cell(_row_idx, GRID_COLUMN_CUR_VALID_AMOUNT).Value = GUI_FormatCurrency(m_total_row.CurValidAmount)

    Me.Grid.Row(_row_idx).BackColor = GetColor(ENUM_GUI_COLOR.GUI_COLOR_YELLOW_00)

    MyBase.GUI_AfterLastRow()
  End Sub ' GUI_AfterLastRow

  ' PURPOSE: Sets the defaul date values to the daily_session_selector control
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultDate()

    Dim _today_opening As Date

    _today_opening = WSI.Common.Misc.TodayOpening()

    uc_dsl.FromDate = _today_opening
    uc_dsl.FromDateSelected = True
    uc_dsl.ToDate = _today_opening.AddDays(1)
    uc_dsl.ToDateSelected = True
    uc_dsl.ClosingTime = _today_opening.Hour

  End Sub ' SetDefaultDate


#End Region ' OVERRIDES

#Region " Public Functions "

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)

    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.MdiParent = MdiParent
    Me.Display(False)

  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Events "

#End Region

End Class
