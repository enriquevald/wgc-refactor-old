'-------------------------------------------------------------------
' Copyright � 2016 Win Systems Ltd.
'-------------------------------------------------------------------
'
'   MODULE NAME : frm_ticket_audits_sel_several
'   DESCRIPTION : Listado de registros de auditor�a de tickets - formulario grande
'                 De este formulario heredan 2: uno que se lanza desde dentro de frm_tito_reports_sel y el otro desde el menu principal
'        AUTHOR : Jorge Concheyro
' CREATION DATE : 25-OCT-2016
'
'
' REVISION HISTORY :
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 25-OCT-2016  JRC    Initial version. PBI 19580:TITO: Pantalla de Log-Seguimiento de Tickets (Cambios Review)
' 11-APR-2017  RAB    PBI 26539: MES10 Ticket validation - Prevent the use of dealer copy tickets (GUI)
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off

Imports GUI_Controls
Imports GUI_CommonOperations
Imports GUI_CommonOperations.CLASS_BASE
Imports GUI_CommonMisc
Imports System.Data.SqlClient
Imports WSI.Common
Imports GUI_Controls.frm_base_print
Imports GUI_Controls.frm_base_sel

Public Class frm_ticket_audits_sel_several
  Inherits frm_ticket_audits_base

#Region " Windows Form Designer generated code "

  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub
  Friend WithEvents gb_date_filter As System.Windows.Forms.GroupBox
  Friend WithEvents dtp_from As GUI_Controls.uc_date_picker
  Friend WithEvents dtp_to As GUI_Controls.uc_date_picker
  Friend WithEvents gb_ticket_type As System.Windows.Forms.GroupBox
  Friend WithEvents chk_offline As System.Windows.Forms.CheckBox
  Friend WithEvents ef_ticket_number As GUI_Controls.uc_entry_field
  Friend WithEvents chk_handpay As System.Windows.Forms.CheckBox
  Friend WithEvents chk_jackpot As System.Windows.Forms.CheckBox
  Friend WithEvents chk_promo_nonredeem_ticket As System.Windows.Forms.CheckBox
  Friend WithEvents chk_promo_redeem_ticket As System.Windows.Forms.CheckBox
  Friend WithEvents chk_redeemable_ticket As System.Windows.Forms.CheckBox
  Friend WithEvents chklst_previous_status As GUI_Controls.uc_checked_list
  Friend WithEvents chklst_actual_status As GUI_Controls.uc_checked_list

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.gb_date_filter = New System.Windows.Forms.GroupBox()
    Me.dtp_from = New GUI_Controls.uc_date_picker()
    Me.dtp_to = New GUI_Controls.uc_date_picker()
    Me.gb_ticket_type = New System.Windows.Forms.GroupBox()
    Me.chk_offline = New System.Windows.Forms.CheckBox()
    Me.ef_ticket_number = New GUI_Controls.uc_entry_field()
    Me.chk_handpay = New System.Windows.Forms.CheckBox()
    Me.chk_jackpot = New System.Windows.Forms.CheckBox()
    Me.chk_promo_nonredeem_ticket = New System.Windows.Forms.CheckBox()
    Me.chk_promo_redeem_ticket = New System.Windows.Forms.CheckBox()
    Me.chk_redeemable_ticket = New System.Windows.Forms.CheckBox()
    Me.chklst_previous_status = New GUI_Controls.uc_checked_list()
    Me.chklst_actual_status = New GUI_Controls.uc_checked_list()
    Me.panel_filter.SuspendLayout()
    Me.panel_data.SuspendLayout()
    Me.pn_separator_line.SuspendLayout()
    Me.gb_date_filter.SuspendLayout()
    Me.gb_ticket_type.SuspendLayout()
    Me.SuspendLayout()
    '
    'panel_filter
    '
    Me.panel_filter.Controls.Add(Me.chklst_actual_status)
    Me.panel_filter.Controls.Add(Me.chklst_previous_status)
    Me.panel_filter.Controls.Add(Me.gb_ticket_type)
    Me.panel_filter.Controls.Add(Me.gb_date_filter)
    Me.panel_filter.Size = New System.Drawing.Size(1318, 268)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_date_filter, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.gb_ticket_type, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chklst_previous_status, 0)
    Me.panel_filter.Controls.SetChildIndex(Me.chklst_actual_status, 0)
    '
    'panel_data
    '
    Me.panel_data.Location = New System.Drawing.Point(4, 272)
    Me.panel_data.Size = New System.Drawing.Size(1318, 405)
    '
    'pn_separator_line
    '
    Me.pn_separator_line.Size = New System.Drawing.Size(1312, 23)
    '
    'pn_line
    '
    Me.pn_line.Size = New System.Drawing.Size(1312, 4)
    '
    'gb_date_filter
    '
    Me.gb_date_filter.Controls.Add(Me.dtp_from)
    Me.gb_date_filter.Controls.Add(Me.dtp_to)
    Me.gb_date_filter.Location = New System.Drawing.Point(10, 16)
    Me.gb_date_filter.Name = "gb_date_filter"
    Me.gb_date_filter.Size = New System.Drawing.Size(262, 92)
    Me.gb_date_filter.TabIndex = 0
    Me.gb_date_filter.TabStop = False
    Me.gb_date_filter.Text = "xFiltro fecha"
    '
    'dtp_from
    '
    Me.dtp_from.Checked = True
    Me.dtp_from.IsReadOnly = False
    Me.dtp_from.Location = New System.Drawing.Point(10, 20)
    Me.dtp_from.Name = "dtp_from"
    Me.dtp_from.ShowCheckBox = False
    Me.dtp_from.ShowUpDown = False
    Me.dtp_from.Size = New System.Drawing.Size(240, 25)
    Me.dtp_from.SufixText = "Sufix Text"
    Me.dtp_from.SufixTextVisible = True
    Me.dtp_from.TabIndex = 3
    Me.dtp_from.TextWidth = 50
    Me.dtp_from.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'dtp_to
    '
    Me.dtp_to.Checked = True
    Me.dtp_to.IsReadOnly = False
    Me.dtp_to.Location = New System.Drawing.Point(10, 52)
    Me.dtp_to.Name = "dtp_to"
    Me.dtp_to.ShowCheckBox = False
    Me.dtp_to.ShowUpDown = False
    Me.dtp_to.Size = New System.Drawing.Size(240, 25)
    Me.dtp_to.SufixText = "Sufix Text"
    Me.dtp_to.SufixTextVisible = True
    Me.dtp_to.TabIndex = 4
    Me.dtp_to.TextWidth = 50
    Me.dtp_to.Value = New Date(2007, 1, 1, 0, 0, 0, 0)
    '
    'gb_ticket_type
    '
    Me.gb_ticket_type.Controls.Add(Me.chk_offline)
    Me.gb_ticket_type.Controls.Add(Me.ef_ticket_number)
    Me.gb_ticket_type.Controls.Add(Me.chk_handpay)
    Me.gb_ticket_type.Controls.Add(Me.chk_jackpot)
    Me.gb_ticket_type.Controls.Add(Me.chk_promo_nonredeem_ticket)
    Me.gb_ticket_type.Controls.Add(Me.chk_promo_redeem_ticket)
    Me.gb_ticket_type.Controls.Add(Me.chk_redeemable_ticket)
    Me.gb_ticket_type.Location = New System.Drawing.Point(282, 16)
    Me.gb_ticket_type.Name = "gb_ticket_type"
    Me.gb_ticket_type.Size = New System.Drawing.Size(210, 195)
    Me.gb_ticket_type.TabIndex = 1
    Me.gb_ticket_type.TabStop = False
    Me.gb_ticket_type.Text = "xTicket Type"
    '
    'chk_offline
    '
    Me.chk_offline.AutoSize = True
    Me.chk_offline.Location = New System.Drawing.Point(9, 167)
    Me.chk_offline.Name = "chk_offline"
    Me.chk_offline.Size = New System.Drawing.Size(70, 17)
    Me.chk_offline.TabIndex = 6
    Me.chk_offline.Text = "xOffline"
    Me.chk_offline.UseVisualStyleBackColor = True
    '
    'ef_ticket_number
    '
    Me.ef_ticket_number.DoubleValue = 0.0R
    Me.ef_ticket_number.IntegerValue = 0
    Me.ef_ticket_number.IsReadOnly = False
    Me.ef_ticket_number.Location = New System.Drawing.Point(9, 20)
    Me.ef_ticket_number.Name = "ef_ticket_number"
    Me.ef_ticket_number.PlaceHolder = Nothing
    Me.ef_ticket_number.Size = New System.Drawing.Size(192, 24)
    Me.ef_ticket_number.SufixText = "Sufix Text"
    Me.ef_ticket_number.SufixTextVisible = True
    Me.ef_ticket_number.TabIndex = 0
    Me.ef_ticket_number.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.ef_ticket_number.TextValue = ""
    Me.ef_ticket_number.TextWidth = 50
    Me.ef_ticket_number.Value = ""
    Me.ef_ticket_number.ValueForeColor = System.Drawing.Color.Blue
    '
    'chk_handpay
    '
    Me.chk_handpay.AutoSize = True
    Me.chk_handpay.Location = New System.Drawing.Point(9, 144)
    Me.chk_handpay.Name = "chk_handpay"
    Me.chk_handpay.Size = New System.Drawing.Size(83, 17)
    Me.chk_handpay.TabIndex = 5
    Me.chk_handpay.Text = "xHandPay"
    Me.chk_handpay.UseVisualStyleBackColor = True
    '
    'chk_jackpot
    '
    Me.chk_jackpot.AutoSize = True
    Me.chk_jackpot.Location = New System.Drawing.Point(9, 121)
    Me.chk_jackpot.Name = "chk_jackpot"
    Me.chk_jackpot.Size = New System.Drawing.Size(76, 17)
    Me.chk_jackpot.TabIndex = 4
    Me.chk_jackpot.Text = "xJackpot"
    Me.chk_jackpot.UseVisualStyleBackColor = True
    '
    'chk_promo_nonredeem_ticket
    '
    Me.chk_promo_nonredeem_ticket.AutoSize = True
    Me.chk_promo_nonredeem_ticket.Location = New System.Drawing.Point(9, 98)
    Me.chk_promo_nonredeem_ticket.Name = "chk_promo_nonredeem_ticket"
    Me.chk_promo_nonredeem_ticket.Size = New System.Drawing.Size(107, 17)
    Me.chk_promo_nonredeem_ticket.TabIndex = 3
    Me.chk_promo_nonredeem_ticket.Text = "xPlayableOnly"
    Me.chk_promo_nonredeem_ticket.UseVisualStyleBackColor = True
    '
    'chk_promo_redeem_ticket
    '
    Me.chk_promo_redeem_ticket.AutoSize = True
    Me.chk_promo_redeem_ticket.Location = New System.Drawing.Point(9, 75)
    Me.chk_promo_redeem_ticket.Name = "chk_promo_redeem_ticket"
    Me.chk_promo_redeem_ticket.Size = New System.Drawing.Size(196, 17)
    Me.chk_promo_redeem_ticket.TabIndex = 2
    Me.chk_promo_redeem_ticket.Text = "Promotional Non-Redeemable"
    Me.chk_promo_redeem_ticket.UseVisualStyleBackColor = True
    '
    'chk_redeemable_ticket
    '
    Me.chk_redeemable_ticket.AutoSize = True
    Me.chk_redeemable_ticket.Location = New System.Drawing.Point(9, 52)
    Me.chk_redeemable_ticket.Name = "chk_redeemable_ticket"
    Me.chk_redeemable_ticket.Size = New System.Drawing.Size(104, 17)
    Me.chk_redeemable_ticket.TabIndex = 1
    Me.chk_redeemable_ticket.Text = "xRedeemable"
    Me.chk_redeemable_ticket.UseVisualStyleBackColor = True
    '
    'chklst_previous_status
    '
    Me.chklst_previous_status.GroupBoxText = "xPreviousStatus"
    Me.chklst_previous_status.Location = New System.Drawing.Point(504, 16)
    Me.chklst_previous_status.m_resize_width = 306
    Me.chklst_previous_status.multiChoice = True
    Me.chklst_previous_status.Name = "chklst_previous_status"
    Me.chklst_previous_status.SelectedIndexes = New Integer(-1) {}
    Me.chklst_previous_status.SelectedIndexesList = ""
    Me.chklst_previous_status.SelectedIndexesListLevel2 = ""
    Me.chklst_previous_status.SelectedValuesArray = New String(-1) {}
    Me.chklst_previous_status.SelectedValuesList = ""
    Me.chklst_previous_status.SetLevels = 2
    Me.chklst_previous_status.Size = New System.Drawing.Size(306, 236)
    Me.chklst_previous_status.TabIndex = 4
    Me.chklst_previous_status.ValuesArray = New String(-1) {}
    '
    'chklst_actual_status
    '
    Me.chklst_actual_status.GroupBoxText = "xActualStatus"
    Me.chklst_actual_status.Location = New System.Drawing.Point(822, 16)
    Me.chklst_actual_status.m_resize_width = 306
    Me.chklst_actual_status.multiChoice = True
    Me.chklst_actual_status.Name = "chklst_actual_status"
    Me.chklst_actual_status.SelectedIndexes = New Integer(-1) {}
    Me.chklst_actual_status.SelectedIndexesList = ""
    Me.chklst_actual_status.SelectedIndexesListLevel2 = ""
    Me.chklst_actual_status.SelectedValuesArray = New String(-1) {}
    Me.chklst_actual_status.SelectedValuesList = ""
    Me.chklst_actual_status.SetLevels = 2
    Me.chklst_actual_status.Size = New System.Drawing.Size(306, 236)
    Me.chklst_actual_status.TabIndex = 5
    Me.chklst_actual_status.ValuesArray = New String(-1) {}
    '
    'frm_ticket_audits_sel_several
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
    Me.ClientSize = New System.Drawing.Size(1326, 681)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
    Me.Name = "frm_ticket_audits_sel_several"
    Me.panel_filter.ResumeLayout(False)
    Me.panel_filter.PerformLayout()
    Me.panel_data.ResumeLayout(False)
    Me.pn_separator_line.ResumeLayout(False)
    Me.gb_date_filter.ResumeLayout(False)
    Me.gb_ticket_type.ResumeLayout(False)
    Me.gb_ticket_type.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Constants "
#End Region ' Constants

#Region " Members "

  Private m_date_from As String
  Private m_date_to As String
  Private m_filter_by_date As String

  Private m_ticket_number As String
  Private m_ticket_type As String
  Private m_old_status As String
  Private m_new_status As String

#End Region ' Members

#Region " OVERRIDES "

  ' PURPOSE: Establish Form Id, according to the enumeration in the application
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TICKET_AUDIT_SEL_SEVERAL

    'Call Base Form proc
    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  ' PURPOSE: Initialites controls settings
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()

    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7674)

    ' Buttons
    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_INFO).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_FILTER_RESET).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = True

    GUI_Button(ENUM_BUTTON.BUTTON_CANCEL).Text = GLB_NLS_GUI_CONTROLS.GetString(10)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7472)
    Me.GUI_Button(ENUM_BUTTON.BUTTON_CUSTOM_0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7467)

    'date filter
    gb_date_filter.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7663) 'GLB_NLS_GUI_PLAYER_TRACKING.GetString(2118)
    Me.dtp_from.Text = GLB_NLS_GUI_AUDITOR.GetString(257)
    Me.dtp_to.Text = GLB_NLS_GUI_AUDITOR.GetString(258)
    Me.dtp_from.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    Me.dtp_to.SetFormat(ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)

    'ticket filter
    gb_ticket_type.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2134)
    ef_ticket_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(795)
    ef_ticket_number.SetFilter(CLASS_FILTER.ENUM_FORMAT.FORMAT_RAW_NUMBER, 18)
    chk_redeemable_ticket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125)
    chk_promo_redeem_ticket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2701, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125))
    chk_promo_nonredeem_ticket.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2701, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2126))
    chk_handpay.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2685)
    chk_jackpot.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2686)
    chk_offline.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(3327)

    'status
    chklst_previous_status.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7702)
    chklst_actual_status.GroupBoxText = GLB_NLS_GUI_PLAYER_TRACKING.GetString(7703)

    LoadListTicketStatus()
    chklst_previous_status.btn_uncheck_all.PerformClick()
    chklst_previous_status.Enabled = False
    chklst_previous_status.ResizeGrid()

    chklst_actual_status.btn_uncheck_all.PerformClick()
    chklst_actual_status.Enabled = False
    chklst_actual_status.ResizeGrid()

    Call GUI_StyleSheet()

    ' Set filter default values
    Call SetDefaultValues()

  End Sub ' GUI_InitControls

  ' PURPOSE: Resets filters values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_FilterReset()
    Call SetDefaultValues()
  End Sub ' GUI_FilterReset


  ' PURPOSE: Checks filter's values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Function GUI_FilterCheck() As Boolean

    ' The interval between from_date and to_date must be less than 31 days
    If Me.dtp_to.Value > Me.dtp_from.Value.AddDays(31) Then
      Call NLS_MsgBox(GLB_NLS_GUI_PLAYER_TRACKING.Id(587), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.dtp_to.Focus()
      Return False
    End If

    If Me.dtp_from.Value > Me.dtp_to.Value Then
      Call NLS_MsgBox(GLB_NLS_GUI_AUDITOR.Id(101), ENUM_MB_TYPE.MB_TYPE_WARNING)
      Call Me.dtp_from.Focus()
      Return False
    End If

    Return True

  End Function ' GUI_FilterCheck

  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Overrides Function GUI_FilterGetSqlQuery() As String
    Dim _sb As System.Text.StringBuilder

    _sb = New System.Text.StringBuilder()

    _sb.AppendLine(MyBase.GUI_FilterGetSqlQuery())

    _sb.AppendLine(GetSqlWhere())
    _sb.AppendLine("    ORDER BY   TIA_VALIDATION_NUMBER, TIA_INSERT_DATE    ")

    Return _sb.ToString()

  End Function ' GUI_FilterGetSqlQuery

#Region " GUI Reports "

  ' PURPOSE: Sets report filters values
  '
  '  PARAMS:
  '     - INPUT:
  '           - PrintData 
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_ReportFilter(ByVal PrintData As GUI_Reports.CLASS_PRINT_DATA)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7664), m_ticket_number)
    PrintData.FilterValueWidth(1) = 3000
    PrintData.FilterValueWidth(2) = 3000

    PrintData.Settings.Orientation = GUI_Reports.CLASS_PRINT_DATA.CLASS_SETTINGS.ENUM_ORIENTATION.ORIENTATION_LANDSCAPE

    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7663) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1328), m_date_from)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7663) & " " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(1329), m_date_to)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7702), m_old_status)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(7703), m_new_status)
    PrintData.SetFilter(GLB_NLS_GUI_PLAYER_TRACKING.GetString(2134), m_ticket_type)
    PrintData.FilterHeaderWidth(2) = 2000

  End Sub ' GUI_ReportFilter

  ' PURPOSE: Sets report filters values
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None  
  Protected Overrides Sub GUI_ReportUpdateFilters()
    Dim _no_one As Boolean
    Dim _all As Boolean

    m_date_from = ""
    m_date_to = ""
    m_filter_by_date = ""
    m_old_status = ""
    m_new_status = ""
    m_ticket_type = ""
    m_ticket_number = ""

    'if there is a validation number defined the other filters must to be discarded
    If ef_ticket_number.Value <> "" Then
      m_ticket_number = Me.ef_ticket_number.TextValue
      Return
    End If

    'date from
    If Me.dtp_from.Checked Then
      m_date_from = GUI_FormatDate(Me.dtp_from.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    'date to
    If Me.dtp_to.Checked Then
      m_date_to = GUI_FormatDate(Me.dtp_to.Value, ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMMSS)
    End If

    ' state 
    _no_one = True
    _all = True

    If Not chklst_previous_status.IsAllOrNoneSelected Then
      m_old_status = chklst_previous_status.SelectedValuesList
    Else
      m_old_status = GLB_NLS_GUI_ALARMS.GetString(277)
    End If

    If Not chklst_actual_status.IsAllOrNoneSelected Then
      m_new_status = chklst_actual_status.SelectedValuesList
    Else
      m_new_status = GLB_NLS_GUI_ALARMS.GetString(277)
    End If

    'ticket type
    _no_one = True
    _all = True

    If chk_redeemable_ticket.Checked Then
      m_ticket_type = m_ticket_type & Me.chk_redeemable_ticket.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_promo_redeem_ticket.Checked Then
      m_ticket_type = m_ticket_type & Me.chk_promo_redeem_ticket.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_promo_nonredeem_ticket.Checked Then
      m_ticket_type = m_ticket_type & Me.chk_promo_nonredeem_ticket.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_jackpot.Checked Then
      m_ticket_type = m_ticket_type & Me.chk_jackpot.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_handpay.Checked Then
      m_ticket_type = m_ticket_type & Me.chk_handpay.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If chk_offline.Checked Then
      m_ticket_type = m_ticket_type & Me.chk_offline.Text & ", "
      _no_one = False
    Else
      _all = False
    End If

    If m_ticket_type.Length <> 0 Then
      m_ticket_type = Strings.Left(m_ticket_type, Len(m_ticket_type) - 2)
    End If

    If _no_one Or _all Then
      m_ticket_type = GLB_NLS_GUI_ALARMS.GetString(277)
    End If

  End Sub ' GUI_ReportUpdateFilters

  ''' <summary>
  ''' Checks out the values of a db row before adding it to the grid
  ''' </summary>
  ''' <param name="DbRow"></param>
  ''' <returns> - True: the db row should be added to the grid
  '''           - False: the db row should NOT be added to the grid
  '''</returns>
  Public Overrides Function GUI_CheckOutRowBeforeAdd(ByVal DbRow As CLASS_DB_ROW) As Boolean
    If (DbRow.Value(SQL_COLUMN_TITO_TICKET_TYPE) = TITO_TICKET_TYPE.CHIPS_DEALER_COPY) Then
      Return False
    End If
    Return True
  End Function

#End Region ' GUI Reports

#End Region  ' Overrides

#Region " Public Functions "

  ' PURPOSE: Call GUI_ButtonClick with the Cursor in Wait Mode. Finally restore Cursor to Default.
  '
  '  PARAMS:
  '     - INPUT:
  '           - ButtonId As ENUM_BUTTON
  '     - OUTPUT:
  '           -
  '
  ' RETURNS:
  '     -
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As GUI_Controls.frm_base_sel.ENUM_BUTTON)
    Select Case ButtonId
      Case frm_base_sel.ENUM_BUTTON.BUTTON_FILTER_APPLY
        Call GUI_StyleSheet()
        Call MyBase.GUI_ButtonClick(ButtonId)
      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select
  End Sub ' GUI_ButtonClick

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  Public Overloads Sub ShowForEdit(ByVal MdiParent As System.Windows.Forms.IWin32Window)
    Me.MdiParent = MdiParent
    Me.ScreenMode = ENUM_SCREEN_SELECT_MODE.SSM_NOTHING
    Me.Display(False)
  End Sub ' ShowForEdit

#End Region ' Public Functions

#Region " Private Functions "

  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Protected Overrides Sub GUI_StyleSheet()
    MyBase.GUI_StyleSheet()
  End Sub ' GUI_StyleSheet

  ' PURPOSE: Set default values to filters
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub SetDefaultValues()
    Dim _today_opening As Date
    _today_opening = WSI.Common.Misc.TodayOpening()

    chklst_previous_status.Enabled = True
    chklst_previous_status.btn_uncheck_all.PerformClick()
    chklst_actual_status.Enabled = True
    chklst_actual_status.btn_uncheck_all.PerformClick()

    ' Ticket
    ef_ticket_number.Value = ""
    chk_redeemable_ticket.Checked = False
    chk_promo_redeem_ticket.Checked = False
    chk_promo_nonredeem_ticket.Checked = False
    chk_jackpot.Checked = False
    chk_handpay.Checked = False
    chk_offline.Checked = False
    chk_redeemable_ticket.Enabled = True
    chk_promo_redeem_ticket.Enabled = True
    chk_promo_nonredeem_ticket.Enabled = True
    chk_jackpot.Enabled = True
    chk_handpay.Enabled = True
    chk_offline.Enabled = True

    gb_date_filter.Enabled = True
    dtp_from.Value = _today_opening
    dtp_to.Value = _today_opening.AddDays(1)
    dtp_from.Checked = True
    dtp_to.Checked = True

  End Sub ' SetDefaultValues


  ''' <summary>
  ''' LoadListBlockType
  ''' </summary>
  ''' <remarks></remarks>
  Private Sub LoadListTicketStatus()
    chklst_previous_status.Clear()
    chklst_previous_status.Add(WSI.Common.TITO_TICKET_STATUS.PENDING_PRINT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7648))
    chklst_previous_status.Add(WSI.Common.TITO_TICKET_STATUS.VALID, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3010))
    chklst_previous_status.Add(WSI.Common.TITO_TICKET_STATUS.PENDING_CANCEL, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7672))
    chklst_previous_status.Add(WSI.Common.TITO_TICKET_STATUS.CANCELED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7671))
    chklst_previous_status.Add(WSI.Common.TITO_TICKET_STATUS.REDEEMED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2138))
    chklst_previous_status.Add(WSI.Common.TITO_TICKET_STATUS.PAID_AFTER_EXPIRATION, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2138) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2137))
    chklst_previous_status.Add(WSI.Common.TITO_TICKET_STATUS.PENDING_PAYMENT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7278))
    chklst_previous_status.Add(WSI.Common.TITO_TICKET_STATUS.PENDING_PAYMENT_AFTER_EXPIRATION, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7279))
    chklst_previous_status.Add(WSI.Common.TITO_TICKET_STATUS.EXPIRED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2137))
    chklst_previous_status.Add(WSI.Common.TITO_TICKET_STATUS.DISCARDED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2989))
    chklst_previous_status.Add(WSI.Common.TITO_TICKET_STATUS.SWAP_FOR_CHIPS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7145)) 'EOR 08-MAR-2016 Type State

    chklst_actual_status.Clear()
    chklst_actual_status.Add(WSI.Common.TITO_TICKET_STATUS.PENDING_PRINT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7648))
    chklst_actual_status.Add(WSI.Common.TITO_TICKET_STATUS.VALID, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3010))
    chklst_actual_status.Add(WSI.Common.TITO_TICKET_STATUS.PENDING_CANCEL, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7672))
    chklst_actual_status.Add(WSI.Common.TITO_TICKET_STATUS.CANCELED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7671))
    chklst_actual_status.Add(WSI.Common.TITO_TICKET_STATUS.REDEEMED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2138))
    chklst_actual_status.Add(WSI.Common.TITO_TICKET_STATUS.PAID_AFTER_EXPIRATION, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2138) & " - " & GLB_NLS_GUI_PLAYER_TRACKING.GetString(2137))
    chklst_actual_status.Add(WSI.Common.TITO_TICKET_STATUS.PENDING_PAYMENT, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7278))
    chklst_actual_status.Add(WSI.Common.TITO_TICKET_STATUS.PENDING_PAYMENT_AFTER_EXPIRATION, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7279))
    chklst_actual_status.Add(WSI.Common.TITO_TICKET_STATUS.EXPIRED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2137))
    chklst_actual_status.Add(WSI.Common.TITO_TICKET_STATUS.DISCARDED, GLB_NLS_GUI_PLAYER_TRACKING.GetString(2989))
    chklst_actual_status.Add(WSI.Common.TITO_TICKET_STATUS.SWAP_FOR_CHIPS, GLB_NLS_GUI_PLAYER_TRACKING.GetString(7145)) 'EOR 08-MAR-2016 Type State

  End Sub ' LoadListBlockType


  ' PURPOSE: Get Sql WHERE to buil SQL Query
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Function GetSqlWhere() As String

    Dim _str_where As String = ""
    Dim _date_filter As String = ""
    Dim _where_added As Boolean
    Dim _ticket_type As String
    Dim _ticket_number As Int64
    Dim _old_status_filter As String
    Dim _new_status_filter As String

    _where_added = False
    _ticket_type = ""
    _date_filter = ""
    _old_status_filter = ""
    _new_status_filter = ""

    'if there is an especific validation number, the rest of filter are discarded
    If ef_ticket_number.TextValue.Trim <> "" AndAlso Int64.TryParse(ef_ticket_number.Value, _ticket_number) Then

      Return " WHERE TIA_VALIDATION_NUMBER = " & _ticket_number.ToString()
    End If

    _date_filter = GetSqlFilterCondition("TIA_INSERT_DATE")

    'Ticket Type filters
    If chk_redeemable_ticket.Checked Then
      _ticket_type = _ticket_type & " TIA_TYPE_ID = " & TITO_TICKET_TYPE.CASHABLE
    End If

    If chk_promo_redeem_ticket.Checked Then
      If _ticket_type.Length > 0 Then
        _ticket_type = _ticket_type & " OR TIA_TYPE_ID = " & TITO_TICKET_TYPE.PROMO_REDEEM
      Else
        _ticket_type = " TIA_TYPE_ID = " & TITO_TICKET_TYPE.PROMO_REDEEM
      End If
    End If

    If chk_promo_nonredeem_ticket.Checked Then
      If _ticket_type.Length > 0 Then
        _ticket_type = _ticket_type & " OR TIA_TYPE_ID = " & TITO_TICKET_TYPE.PROMO_NONREDEEM
      Else
        _ticket_type = " TIA_TYPE_ID = " & TITO_TICKET_TYPE.PROMO_NONREDEEM
      End If
    End If

    If chk_handpay.Checked Then
      If _ticket_type.Length > 0 Then
        _ticket_type = _ticket_type & " OR TIA_TYPE_ID = " & TITO_TICKET_TYPE.HANDPAY
      Else
        _ticket_type = " TIA_TYPE_ID = " & TITO_TICKET_TYPE.HANDPAY
      End If
    End If

    If chk_jackpot.Checked Then
      If _ticket_type.Length > 0 Then
        _ticket_type = _ticket_type & " OR TIA_TYPE_ID = " & TITO_TICKET_TYPE.JACKPOT
      Else
        _ticket_type = " TIA_TYPE_ID = " & TITO_TICKET_TYPE.JACKPOT
      End If
    End If

    If chk_offline.Checked Then
      If _ticket_type.Length > 0 Then
        _ticket_type = _ticket_type & " OR TIA_TYPE_ID = " & TITO_TICKET_TYPE.OFFLINE
      Else
        _ticket_type = " TIA_TYPE_ID = " & TITO_TICKET_TYPE.OFFLINE
      End If
    End If

    If Not chklst_previous_status.IsAllOrNoneSelected Then
      _old_status_filter = chklst_previous_status.SelectedIndexesList
    End If

    If Not chklst_actual_status.IsAllOrNoneSelected Then
      _new_status_filter = chklst_actual_status.SelectedIndexesList
    End If

    If (_ticket_type.Length > 0) Or (_date_filter.Length > 0) Or (_old_status_filter.Length > 0) Or (_new_status_filter.Length > 0) Then
      _str_where = "WHERE "
    End If

    If _date_filter.Length > 0 Then
      _str_where = _str_where & _date_filter
      _where_added = True
    End If

    If _ticket_type.Length > 0 Then
      _str_where = (IIf(_where_added, _str_where & " AND ", _str_where))
      _str_where = _str_where & "(" & _ticket_type & ") "
      _where_added = True
    End If

    If _old_status_filter.Length > 0 Then
      _str_where = (IIf(_where_added, _str_where & " AND ", _str_where))
      _str_where = _str_where & " TIA_OLD_STATUS IN (" & _old_status_filter & ") "
      _where_added = True
    End If

    If _new_status_filter.Length > 0 Then
      _str_where = (IIf(_where_added, _str_where & " AND ", _str_where))
      _str_where = _str_where & " TIA_STATUS IN (" & _new_status_filter & ") "
      _where_added = True
    End If

    Return _str_where
  End Function ' GetSqlWhere

  Public Function GetSqlFilterCondition(ByVal FilterField As String) As String
    Dim sql_filter As String = ""

    If FilterField.Length > 0 Then
      If Me.dtp_from.Checked Then
        sql_filter = "(" & FilterField & " >= " & GUI_FormatDateDB(Me.dtp_from.Value) & ")"
      End If

      If Me.dtp_to.Checked Then
        If sql_filter.Length > 0 Then
          sql_filter = sql_filter & " AND "
        End If
        sql_filter = sql_filter & "(" & FilterField & " < " & GUI_FormatDateDB(Me.dtp_to.Value) & ")"
      End If
    End If

    Return sql_filter
  End Function ' GetSqlFilterCondition

#End Region  ' Private Functions

#Region "Events"

#End Region ' Events

  Private Sub ef_ticket_number_EntryFieldValueChanged() Handles ef_ticket_number.EntryFieldValueChanged
    Dim _enable_filters As Boolean

    _enable_filters = String.IsNullOrEmpty(ef_ticket_number.TextValue)

    Call EnableFilters(_enable_filters)

  End Sub

  Private Sub EnableFilters(ByVal EnableFilters As Boolean)
    Me.chk_redeemable_ticket.Enabled = EnableFilters
    Me.chk_promo_redeem_ticket.Enabled = EnableFilters
    Me.chk_promo_nonredeem_ticket.Enabled = EnableFilters
    Me.chk_jackpot.Enabled = EnableFilters
    Me.chk_handpay.Enabled = EnableFilters
    Me.chk_offline.Enabled = EnableFilters
    Me.gb_date_filter.Enabled = EnableFilters
    Me.chklst_actual_status.Enabled = EnableFilters
    Me.chklst_previous_status.Enabled = EnableFilters

  End Sub ' EnableFilters

End Class
