'-------------------------------------------------------------------
' Copyright � 2013 Win Systems Ltd.
'-------------------------------------------------------------------
'
' MODULE NAME:   frm_TITO_select_ticket
' DESCRIPTION:   Payment Order configuration form
' AUTHOR:        Humberto Braojos
' CREATION DATE: 15-JAN-2014
' 
' REVISION HISTORY:
'
' Date         Author Description
' -----------  ------ -----------------------------------------------
' 15-JAN-2014  HBB    Initial version
' 28-JUL-2014  SGB    Introduce date of create, sequence and status columns in datagrid
' 06-OCT-2016  JRC    PBI 18259:TITO: Nuevo estado "Pending Print" en tickets - Mostrar nuevo estado en pantallas (GUI y Cashier)
'--------------------------------------------------------------------

Option Explicit On
Option Strict Off
Imports GUI_CommonOperations
Imports GUI_CommonMisc
Imports GUI_Controls
Imports System.Text
Imports WSI.Common
Imports System.Data.SqlClient
Imports WSI.Common.TITO
Imports System.Windows.Forms

Public Class frm_TITO_select_ticket
  Inherits frm_base_print
  ' Filter members
  Private m_validation_number As Int64
  Private m_selected_id As Int64
  Private m_selected_amount As Decimal
  Private m_selected_index As Int32
  Private m_selected_type As TITO_TICKET_TYPE

#Region "Constants"

  Const GRID_COLUMNS_COUNT As Int32 = 7
  Const GRID_HEADERS_COUNT As Int32 = 1

  ' Grid Columns
  Private Const GRID_COLUMN_TICKET_ID As Integer = 0
  Private Const GRID_COLUMN_TICKET_AMOUNT As Integer = 1
  Private Const GRID_COLUMN_TICKET_TYPE_NAME As Integer = 2
  Private Const GRID_COLUMN_TICKET_TYPE_ID As Integer = 3
  Private Const GRID_COLUMN_TICKET_DATE As Integer = 4
  Private Const GRID_COLUMN_TICKET_SEQUENCE As Integer = 5
  Private Const GRID_COLUMN_TICKET_STATUS As Integer = 6

  ' Width
  Private Const GRID_WIDTH_TICKET_ID As Integer = 0
  Private Const GRID_WIDTH_TICKET_AMOUNT As Integer = 1300
  Private Const GRID_WIDTH_TICKET_TYPE As Integer = 1650
  Private Const GRID_WIDTH_TICKET_DATE As Integer = 1800
  Private Const GRID_WIDTH_TICKET_SEQUENCE As Integer = 1000
  Private Const GRID_WIDTH_TICKET_STATUS As Integer = 2865
  Private Const GRID_WIDTH_TICKET_TYPE_ID As Integer = 0

  ' DB Columns
  Private Const SQL_COLUMN_TICKET_ID As Integer = 0
  Private Const SQL_COLUMN_TICKET_AMOUNT As Integer = 1
  Private Const SQL_COLUMN_TICKET_TYPE As Integer = 2
  Private Const SQL_COLUMN_TICKET_DATE As Integer = 3
  Private Const SQL_COLUMN_TICKET_SEQUENCE As Integer = 4
  Private Const SQL_COLUMN_TICKET_STATUS As Integer = 5

#End Region

#Region "Properties"

  Public ReadOnly Property SelectedTicketId() As Int64
    Get
      Return m_selected_id
    End Get
  End Property

  Public ReadOnly Property SelectedTicketAmount() As Decimal
    Get
      Return m_selected_amount
    End Get
  End Property

  Public ReadOnly Property SelectedTicketType() As TITO_TICKET_TYPE
    Get
      Return m_selected_type
    End Get
  End Property

  Public ReadOnly Property SelectedTicketIndex() As Int32
    Get
      Return m_selected_index
    End Get
  End Property

#End Region

  Public Overrides Sub GUI_SetFormId()

    Me.FormId = ENUM_FORM.FORM_TITO_SELECT_TICKET

    Call MyBase.GUI_SetFormId()

  End Sub ' GUI_SetFormId

  Protected Overrides Sub GUI_InitControls()

    Call MyBase.GUI_InitControls()
    Me.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(4466)

    Me.GUI_Button(ENUM_BUTTON.BUTTON_SELECT).Enabled = True
    Me.GUI_Button(ENUM_BUTTON.BUTTON_FILTER_APPLY).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_FILTER_RESET).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_EXCEL).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_NEW).Visible = False
    Me.GUI_Button(ENUM_BUTTON.BUTTON_PRINT).Visible = False
    Me.ef_validation_number.IsReadOnly = True
    Me.ef_validation_number.Value = m_validation_number
    Me.ef_validation_number.Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2259)

    Call GUI_StyleSheet()
    Call GUI_FilterGetSqlQuery()

  End Sub ' GUI_InitControls

  ' PURPOSE: Manage buttons pressed.
  '
  '  PARAMS:
  '     - INPUT:
  '         - ButtonId: Id. of the button clicked.
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '
  Protected Overrides Sub GUI_ButtonClick(ByVal ButtonId As ENUM_BUTTON)

    Select Case ButtonId

      Case ENUM_BUTTON.BUTTON_SELECT
        If gr_tickets.CurrentRow >= 0 Then
          m_selected_index = gr_tickets.CurrentRow
          m_selected_id = GUI_ParseNumber(gr_tickets.Cell(m_selected_index, GRID_COLUMN_TICKET_ID).Value)
          m_selected_amount = GUI_ParseCurrency(gr_tickets.Cell(m_selected_index, GRID_COLUMN_TICKET_AMOUNT).Value)
          m_selected_type = GUI_ParseNumber(gr_tickets.Cell(m_selected_index, GRID_COLUMN_TICKET_TYPE_ID).Value)
          Me.Close()
        End If

      Case ENUM_BUTTON.BUTTON_CANCEL
        m_selected_id = 0
        m_selected_amount = 0
        m_selected_index = -1

      Case Else
        Call MyBase.GUI_ButtonClick(ButtonId)
    End Select

  End Sub ' GUI_ButtonClick


#Region "overrides functions"
  ' PURPOSE: Build an SQL query from conditions set in the filters
  '
  '  PARAMS:
  '     - INPUT:
  '
  '     - OUTPUT:
  '
  ' RETURNS:
  '     - SQL query text ready to send to the database
  Protected Sub GUI_FilterGetSqlQuery()

    Dim _sb As StringBuilder
    Dim _table As DataTable
    Dim _row As DataRow
    Dim _idx As Int32

    _table = New DataTable()
    _idx = 0

    _sb = New StringBuilder()
    _sb.AppendLine("SELECT   TI_TICKET_ID ")
    _sb.AppendLine("       , TI_AMOUNT ")
    _sb.AppendLine("       , TI_TYPE_ID ")
    _sb.AppendLine("       , TI_CREATED_DATETIME ")
    _sb.AppendLine("       , TI_MACHINE_NUMBER ")
    _sb.AppendLine("       , TI_STATUS ")
    _sb.AppendLine("  FROM   TICKETS                                   ")
    _sb.AppendLine(" WHERE   TI_VALIDATION_NUMBER = " & m_validation_number.ToString())
    _sb.AppendLine("   AND   TI_COLLECTED_MONEY_COLLECTION IS NULL           ")
    _sb.AppendLine("ORDER BY TI_TICKET_ID DESC           ")


    Try
      Using _db_trx As New DB_TRX()
        Using _cmd As SqlCommand = New SqlCommand(_sb.ToString(), _db_trx.SqlTransaction.Connection, _db_trx.SqlTransaction)
          Using _sql_da As New SqlDataAdapter(_cmd)
            _db_trx.Fill(_sql_da, _table)

          End Using
        End Using
      End Using

      For Each _row In _table.Rows
        Call GUI_SetupRow(_idx, _row)
        _idx = _idx + 1
      Next

      If Me.gr_tickets.NumRows > 0 Then
        Me.gr_tickets.CurrentRow = 0
        Me.gr_tickets.SelectFirstRow(True)
      End If

    Catch _ex As Exception
    End Try

  End Sub ' GUI_FilterGetSqlQuery

#End Region

  ' PURPOSE: Opens dialog with default settings for edit mode
  '
  '  PARAMS:
  '     - INPUT:
  '           - none
  '
  '     - OUTPUT:
  '           - none
  '
  ' RETURNS:
  '     - none
  Public Function ShowForSelect(ByVal MdiParent As System.Windows.Forms.IWin32Window, ByVal ValidationNumber As String) As DialogResult

    m_selected_id = 0
    m_selected_amount = 0
    m_selected_index = 0
    m_validation_number = 0

    If Not Int64.TryParse(ValidationNumber, m_validation_number) Then

      Return DialogResult.Abort
    End If

    Me.Display(True)

    Return IIf(m_selected_index = -1, DialogResult.Cancel, DialogResult.OK)

  End Function ' ShowForEdit

#Region "private functions"
  ' PURPOSE: Define layout of all Main Grid Columns 
  '
  '  PARAMS:
  '     - INPUT:
  '           - None
  '     - OUTPUT:
  '           - None
  '
  ' RETURNS:
  '     - None
  Private Sub GUI_StyleSheet()

    ' Call GetColumnsOrder()

    With Me.gr_tickets
      Call .Init(GRID_COLUMNS_COUNT, GRID_HEADERS_COUNT)
      .SelectionMode = uc_grid.SELECTION_MODE.SELECTION_MODE_SINGLE

      ' TICKET NUMBER

      .Column(GRID_COLUMN_TICKET_ID).Width = GRID_WIDTH_TICKET_ID

      ' AMOUNT
      .Column(GRID_COLUMN_TICKET_AMOUNT).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2260)
      .Column(GRID_COLUMN_TICKET_AMOUNT).Width = GRID_WIDTH_TICKET_AMOUNT
      .Column(GRID_COLUMN_TICKET_AMOUNT).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' ASSOCIATED PROMOTION
      .Column(GRID_COLUMN_TICKET_TYPE_NAME).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2112)
      .Column(GRID_COLUMN_TICKET_TYPE_NAME).Width = GRID_WIDTH_TICKET_TYPE
      .Column(GRID_COLUMN_TICKET_TYPE_NAME).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT

      'TICKET TYPE ID
      .Column(GRID_COLUMN_TICKET_TYPE_ID).Width = GRID_WIDTH_TICKET_TYPE_ID

      ' DATE
      .Column(GRID_COLUMN_TICKET_DATE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(5239)
      .Column(GRID_COLUMN_TICKET_DATE).Width = GRID_WIDTH_TICKET_DATE
      .Column(GRID_COLUMN_TICKET_DATE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_CENTER

      ' SEQUENCE
      .Column(GRID_COLUMN_TICKET_SEQUENCE).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2955)
      .Column(GRID_COLUMN_TICKET_SEQUENCE).Width = GRID_WIDTH_TICKET_SEQUENCE
      .Column(GRID_COLUMN_TICKET_SEQUENCE).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_RIGHT

      ' STATUS
      .Column(GRID_COLUMN_TICKET_STATUS).Header(0).Text = GLB_NLS_GUI_PLAYER_TRACKING.GetString(2117)
      .Column(GRID_COLUMN_TICKET_STATUS).Width = GRID_WIDTH_TICKET_STATUS
      .Column(GRID_COLUMN_TICKET_STATUS).Alignment = uc_grid.CLASS_COL_DATA.ENUM_ALIGN.ALIGN_LEFT


    End With

  End Sub

#End Region

  Public Function GUI_SetupRow(ByVal RowIndex As Integer, _
                                        ByVal DbRow As DataRow) As Boolean

    Me.gr_tickets.AddRow()
    Me.gr_tickets.Cell(RowIndex, GRID_COLUMN_TICKET_ID).Value = DbRow.Item(SQL_COLUMN_TICKET_ID)
    Me.gr_tickets.Cell(RowIndex, GRID_COLUMN_TICKET_AMOUNT).Value = GUI_FormatCurrency(DbRow.Item(SQL_COLUMN_TICKET_AMOUNT), 2)
    Me.gr_tickets.Cell(RowIndex, GRID_COLUMN_TICKET_TYPE_NAME).Value = GetTicketTypeString(DbRow.Item(SQL_COLUMN_TICKET_TYPE))
    Me.gr_tickets.Cell(RowIndex, GRID_COLUMN_TICKET_TYPE_ID).Value = DbRow.Item(SQL_COLUMN_TICKET_TYPE)
    Me.gr_tickets.Cell(RowIndex, GRID_COLUMN_TICKET_DATE).Value = GUI_FormatDate(DbRow.Item(SQL_COLUMN_TICKET_DATE), ENUM_FORMAT_DATE.FORMAT_DATE_SHORT, ENUM_FORMAT_TIME.FORMAT_HHMM)
    Me.gr_tickets.Cell(RowIndex, GRID_COLUMN_TICKET_SEQUENCE).Value = DbRow.Item(SQL_COLUMN_TICKET_SEQUENCE)

    'status
    Me.gr_tickets.Cell(RowIndex, GRID_COLUMN_TICKET_STATUS).Value = TicketStatusAsString(DbRow.Item(SQL_COLUMN_TICKET_STATUS))

    Return True
  End Function ' GUI_SetupRow

  Private Function GetTicketTypeString(ByVal TicketType As Int32) As String

    Select Case TicketType
      Case 0

        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(2125)
      Case 1

        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(2701, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3320))
      Case 2

        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(2701, GLB_NLS_GUI_PLAYER_TRACKING.GetString(3321))
      Case 3

        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(2685)
      Case 4

        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(2686)
      Case 5

        Return GLB_NLS_GUI_PLAYER_TRACKING.GetString(3327)
      Case Else

        Return ""

    End Select
  End Function



  Private Sub gr_tickets_DataSelectedEvent() Handles gr_tickets.DataSelectedEvent

    GUI_ButtonClick(ENUM_BUTTON.BUTTON_SELECT)

  End Sub
End Class